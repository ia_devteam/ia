#include "var/tmp/sensor.h"
/*    walk.c
 *
 *    Copyright (C) 1991, 1992, 1993, 1994, 1995, 1997, 1998, 1999,
 *    2000, 2001, 2002, 2005 by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 */

#include "EXTERN.h"
#include "a2p.h"
#include "util.h"

bool exitval = FALSE;
bool realexit = FALSE;
bool saw_getline = FALSE;
bool subretnum = FALSE;
bool saw_FNR = FALSE;
bool saw_argv0 = FALSE;
bool saw_fh = FALSE;
int maxtmp = 0;
const char *lparen;
const char *rparen;
const char *limit;
STR *subs;
STR *curargs = NULL;

static void addsemi ( STR *str );
static void emit_split ( STR *str, int level );
static void fixtab ( STR *str, int lvl );
static void numericize ( int node );
static void tab ( STR *str, int lvl );

int prewalk ( int numit, int level, int node, int *numericptr );
STR * walk ( int useval, int level, int node, int *numericptr, int minprec );
#ifdef NETWARE
char *savestr(char *str);
char *cpytill(register char *to, register char *from, register int delim);
char *instr(char *big, const char *little);
#endif

STR *
walk(int useval, int level, register int node, int *numericptr, int minprec)
{
    SensorCall();register int len;
    register STR *str;
    register int type;
    register int i;
    register STR *tmpstr;
    STR *tmp2str;
    STR *tmp3str;
    char *t;
    char *d, *s;
    int numarg;
    int numeric = FALSE;
    STR *fstr;
    int prec = P_MAX;		/* assume no parens needed */

    SensorCall();if (!node) {
	SensorCall();*numericptr = 0;
	{STR * ReplaceReturn = str_make(""); SensorCall(); return ReplaceReturn;}
    }
    SensorCall();type = ops[node].ival;
    len = type >> 8;
    type &= 255;
    SensorCall();switch (type) {
    case OPROG:
	SensorCall();arymax = 0;
	SensorCall();if (namelist) {
	    SensorCall();while (isALPHA(*namelist)) {
		SensorCall();for (d = tokenbuf,s=namelist;
		  isALPHA(*s) || isDIGIT(*s) || *s == '_';
		  *d++ = *s++) ;
		SensorCall();*d = '\0';
		SensorCall();while (*s && !isALPHA(*s)) {/*29*/SensorCall();s++;/*30*/}
		SensorCall();namelist = s;
		nameary[++arymax] = savestr(tokenbuf);
	    }
	}
	SensorCall();if (maxfld < arymax)
	    {/*31*/SensorCall();maxfld = arymax;/*32*/}
	SensorCall();opens = str_new(0);
	subs = str_new(0);
	str = walk(0,level,ops[node+1].ival,&numarg,P_MIN);
	SensorCall();if (do_split && need_entire && !absmaxfld)
	    split_to_array = TRUE;
	SensorCall();if (fswitch && !const_FS)
	    {/*33*/SensorCall();const_FS = fswitch;/*34*/}
	SensorCall();if (saw_FS > 1 || saw_RS)
	    {/*35*/SensorCall();const_FS = 0;/*36*/}
	SensorCall();if (saw_ORS && need_entire)
	    do_chop = TRUE;
	SensorCall();if (fswitch) {
	    SensorCall();str_cat(str,"$FS = '");
	    SensorCall();if (strchr("*+?.[]()|^$\\",fswitch))
		{/*37*/SensorCall();str_cat(str,"\\");/*38*/}
	    SensorCall();sprintf(tokenbuf,"%c",fswitch);
	    str_cat(str,tokenbuf);
	    str_cat(str,"';\t\t# field separator from -F switch\n");
	}
	else {/*39*/SensorCall();if (saw_FS && !const_FS) {
	    SensorCall();str_cat(str,"$FS = ' ';\t\t# set field separator\n");
	;/*40*/}}
	SensorCall();if (saw_OFS) {
	    SensorCall();str_cat(str,"$, = ' ';\t\t# set output field separator\n");
	}
	SensorCall();if (saw_ORS) {
	    SensorCall();str_cat(str,"$\\ = \"\\n\";\t\t# set output record separator\n");
	}
	SensorCall();if (saw_argv0) {
	    SensorCall();str_cat(str,"$ARGV0 = $0;\t\t# remember what we ran as\n");
	}
	SensorCall();if (str->str_cur > 20)
	    {/*41*/SensorCall();str_cat(str,"\n");/*42*/}
	SensorCall();if (ops[node+2].ival) {
	    SensorCall();str_scat(str,fstr=walk(0,level,ops[node+2].ival,&numarg,P_MIN));
	    str_free(fstr);
	    str_cat(str,"\n\n");
	}
	SensorCall();fstr = walk(0,level+1,ops[node+3].ival,&numarg,P_MIN);
	SensorCall();if (*fstr->str_ptr) {
	    SensorCall();if (saw_line_op)
		{/*43*/SensorCall();str_cat(str,"line: ");/*44*/}
	    SensorCall();str_cat(str,"while (<>) {\n");
	    tab(str,++level);
	    SensorCall();if (saw_FS && !const_FS)
		do_chop = TRUE;
	    SensorCall();if (do_chop) {
		SensorCall();str_cat(str,"chomp;\t# strip record separator\n");
		tab(str,level);
	    }
	    SensorCall();if (do_split)
		{/*45*/SensorCall();emit_split(str,level);/*46*/}
	    SensorCall();str_scat(str,fstr);
	    str_free(fstr);
	    fixtab(str,--level);
	    str_cat(str,"}\n");
	    SensorCall();if (saw_FNR)
		{/*47*/SensorCall();str_cat(str,"continue {\n    $FNRbase = $. if eof;\n}\n");/*48*/}
	}
	else {/*49*/SensorCall();if (old_awk)
	    {/*51*/SensorCall();str_cat(str,"while (<>) { }		# (no line actions)\n");/*52*/}/*50*/}
	SensorCall();if (ops[node+4].ival) {
	    SensorCall();realexit = TRUE;
	    str_cat(str,"\n");
	    tab(str,level);
	    str_scat(str,fstr=walk(0,level,ops[node+4].ival,&numarg,P_MIN));
	    str_free(fstr);
	    str_cat(str,"\n");
	}
	SensorCall();if (exitval)
	    {/*53*/SensorCall();str_cat(str,"exit $ExitValue;\n");/*54*/}
	SensorCall();if (subs->str_ptr) {
	    SensorCall();str_cat(str,"\n");
	    str_scat(str,subs);
	}
	SensorCall();if (saw_getline) {
	    SensorCall();for (len = 0; len < 4; len++) {
		SensorCall();if (saw_getline & (1 << len)) {
		    SensorCall();sprintf(tokenbuf,"\nsub Getline%d {\n",len);
		    str_cat(str, tokenbuf);
		    SensorCall();if (len & 2) {
			SensorCall();if (do_fancy_opens)
			    {/*55*/SensorCall();str_cat(str,"    &Pick('',@_);\n");/*56*/}
			else
			    {/*57*/SensorCall();str_cat(str,"    ($fh) = @_;\n");/*58*/}
		    }
		    else {
			SensorCall();if (saw_FNR)
			    {/*59*/SensorCall();str_cat(str,"    $FNRbase = $. if eof;\n");/*60*/}
		    }
		    SensorCall();if (len & 1)
			{/*61*/SensorCall();str_cat(str,"    local($_);\n");/*62*/}
		    SensorCall();if (len & 2)
			{/*63*/SensorCall();str_cat(str,
			  "    if ($getline_ok = (($_ = <$fh>) ne ''))");/*64*/}
		    else
			{/*65*/SensorCall();str_cat(str,
			  "    if ($getline_ok = (($_ = <>) ne ''))");/*66*/}
		    SensorCall();str_cat(str, " {\n");
		    level += 2;
		    tab(str,level);
		    i = 0;
		    SensorCall();if (do_chop) {
			SensorCall();i++;
			str_cat(str,"chomp;\t# strip record separator\n");
			tab(str,level);
		    }
		    SensorCall();if (do_split && !(len & 1)) {
			SensorCall();i++;
			emit_split(str,level);
		    }
		    SensorCall();if (!i)
			{/*67*/SensorCall();str_cat(str,";\n");/*68*/}
		    SensorCall();fixtab(str,--level);
		    str_cat(str,"}\n    $_;\n}\n");
		    --level;
		}
	    }
	}
	SensorCall();if (do_fancy_opens) {
	    SensorCall();str_cat(str,"\n\
sub Pick {\n\
    local($mode,$name,$pipe) = @_;\n\
    $fh = $name;\n\
    open($name,$mode.$name.$pipe) unless $opened{$name}++;\n\
}\n\
");
	}
	SensorCall();break;
    case OHUNKS:
	SensorCall();str = walk(0,level,ops[node+1].ival,&numarg,P_MIN);
	str_scat(str,fstr=walk(0,level,ops[node+2].ival,&numarg,P_MIN));
	str_free(fstr);
	SensorCall();if (len == 3) {
	    SensorCall();str_scat(str,fstr=walk(0,level,ops[node+3].ival,&numarg,P_MIN));
	    str_free(fstr);
	}
	else {
	}
	SensorCall();break;
    case ORANGE:
	SensorCall();prec = P_DOTDOT;
	str = walk(1,level,ops[node+1].ival,&numarg,prec+1);
	str_cat(str," .. ");
	str_scat(str,fstr=walk(1,level,ops[node+2].ival,&numarg,prec+1));
	str_free(fstr);
	SensorCall();break;
    case OPAT:
	SensorCall();goto def;
    case OREGEX:
	SensorCall();str = str_new(0);
	str_set(str,"/");
	tmpstr=walk(0,level,ops[node+1].ival,&numarg,P_MIN);
	/* translate \nnn to [\nnn] */
	SensorCall();for (s = tmpstr->str_ptr, d = tokenbuf; *s; s++, d++) {
	    SensorCall();if (*s == '\\' && isDIGIT(s[1]) && isDIGIT(s[2]) && isDIGIT(s[3])){
		SensorCall();*d++ = '[';
		*d++ = *s++;
		*d++ = *s++;
		*d++ = *s++;
		*d++ = *s;
		*d = ']';
	    }
	    else
		{/*69*/SensorCall();*d = *s;/*70*/}
	}
	SensorCall();*d = '\0';
	SensorCall();for (d=tokenbuf; *d; d++)
           {/*71*/SensorCall();*d += (char)128;/*72*/}
	SensorCall();str_cat(str,tokenbuf);
	str_free(tmpstr);
	str_cat(str,"/");
	SensorCall();break;
    case OHUNK:
	SensorCall();if (len == 1) {
	    SensorCall();str = str_new(0);
	    str = walk(0,level,oper1(OPRINT,0),&numarg,P_MIN);
	    str_cat(str," if ");
	    str_scat(str,fstr=walk(0,level,ops[node+1].ival,&numarg,P_MIN));
	    str_free(fstr);
	    str_cat(str,";");
	}
	else {
	    SensorCall();tmpstr = walk(0,level,ops[node+1].ival,&numarg,P_MIN);
	    SensorCall();if (*tmpstr->str_ptr) {
		SensorCall();str = str_new(0);
		str_set(str,"if (");
		str_scat(str,tmpstr);
		str_cat(str,") {\n");
		tab(str,++level);
		str_scat(str,fstr=walk(0,level,ops[node+2].ival,&numarg,P_MIN));
		str_free(fstr);
		fixtab(str,--level);
		str_cat(str,"}\n");
		tab(str,level);
	    }
	    else {
		SensorCall();str = walk(0,level,ops[node+2].ival,&numarg,P_MIN);
	    }
	}
	SensorCall();break;
    case OPPAREN:
	SensorCall();str = str_new(0);
	str_set(str,"(");
	str_scat(str,fstr=walk(useval != 0,level,ops[node+1].ival,&numarg,P_MIN));
	str_free(fstr);
	str_cat(str,")");
	SensorCall();break;
    case OPANDAND:
	SensorCall();prec = P_ANDAND;
	str = walk(1,level,ops[node+1].ival,&numarg,prec);
	str_cat(str," && ");
	str_scat(str,fstr=walk(1,level,ops[node+2].ival,&numarg,prec+1));
	str_free(fstr);
	str_scat(str,fstr=walk(1,level,ops[node+3].ival,&numarg,prec+1));
	str_free(fstr);
	SensorCall();break;
    case OPOROR:
	SensorCall();prec = P_OROR;
	str = walk(1,level,ops[node+1].ival,&numarg,prec);
	str_cat(str," || ");
	str_scat(str,fstr=walk(1,level,ops[node+2].ival,&numarg,prec+1));
	str_free(fstr);
	str_scat(str,fstr=walk(1,level,ops[node+3].ival,&numarg,prec+1));
	str_free(fstr);
	SensorCall();break;
    case OPNOT:
	SensorCall();prec = P_UNARY;
	str = str_new(0);
	str_set(str,"!");
	str_scat(str,fstr=walk(1,level,ops[node+1].ival,&numarg,prec));
	str_free(fstr);
	SensorCall();break;
    case OCOND:
	SensorCall();prec = P_COND;
	str = walk(1,level,ops[node+1].ival,&numarg,prec);
	str_cat(str," ? ");
	str_scat(str,fstr=walk(1,level,ops[node+2].ival,&numarg,prec+1));
	str_free(fstr);
	str_cat(str," : ");
	str_scat(str,fstr=walk(1,level,ops[node+3].ival,&numarg,prec+1));
	str_free(fstr);
	SensorCall();break;
    case OCPAREN:
	SensorCall();str = str_new(0);
	str_set(str,"(");
	str_scat(str,fstr=walk(useval != 0,level,ops[node+1].ival,&numarg,P_MIN));
	str_free(fstr);
	numeric |= numarg;
	str_cat(str,")");
	SensorCall();break;
    case OCANDAND:
	SensorCall();prec = P_ANDAND;
	str = walk(1,level,ops[node+1].ival,&numarg,prec);
	numeric = 1;
	str_cat(str," && ");
	str_scat(str,fstr=walk(1,level,ops[node+2].ival,&numarg,prec+1));
	str_free(fstr);
	str_scat(str,fstr=walk(1,level,ops[node+3].ival,&numarg,prec+1));
	str_free(fstr);
	SensorCall();break;
    case OCOROR:
	SensorCall();prec = P_OROR;
	str = walk(1,level,ops[node+1].ival,&numarg,prec);
	numeric = 1;
	str_cat(str," || ");
	str_scat(str,fstr=walk(1,level,ops[node+2].ival,&numarg,prec+1));
	str_free(fstr);
	str_scat(str,fstr=walk(1,level,ops[node+3].ival,&numarg,prec+1));
	str_free(fstr);
	SensorCall();break;
    case OCNOT:
	SensorCall();prec = P_UNARY;
	str = str_new(0);
	str_set(str,"!");
	str_scat(str,fstr=walk(1,level,ops[node+1].ival,&numarg,prec));
	str_free(fstr);
	numeric = 1;
	SensorCall();break;
    case ORELOP:
	SensorCall();prec = P_REL;
	str = walk(1,level,ops[node+2].ival,&numarg,prec+1);
	numeric |= numarg;
	tmpstr = walk(0,level,ops[node+1].ival,&numarg,P_MIN);
	tmp2str = walk(1,level,ops[node+3].ival,&numarg,prec+1);
	numeric |= numarg;
	SensorCall();if (!numeric ||
	 (!numarg && (*tmp2str->str_ptr == '"' || *tmp2str->str_ptr == '\''))) {
	    SensorCall();t = tmpstr->str_ptr;
	    SensorCall();if (strEQ(t,"=="))
		{/*73*/SensorCall();str_set(tmpstr,"eq");/*74*/}
	    else {/*75*/SensorCall();if (strEQ(t,"!="))
		{/*77*/SensorCall();str_set(tmpstr,"ne");/*78*/}
	    else {/*79*/SensorCall();if (strEQ(t,"<"))
		{/*81*/SensorCall();str_set(tmpstr,"lt");/*82*/}
	    else {/*83*/SensorCall();if (strEQ(t,"<="))
		{/*85*/SensorCall();str_set(tmpstr,"le");/*86*/}
	    else {/*87*/SensorCall();if (strEQ(t,">"))
		{/*89*/SensorCall();str_set(tmpstr,"gt");/*90*/}
	    else {/*91*/SensorCall();if (strEQ(t,">="))
		{/*93*/SensorCall();str_set(tmpstr,"ge");/*94*/}/*92*/}/*88*/}/*84*/}/*80*/}/*76*/}
	    SensorCall();if (!strchr(tmpstr->str_ptr,'\'') && !strchr(tmpstr->str_ptr,'"') &&
	      !strchr(tmp2str->str_ptr,'\'') && !strchr(tmp2str->str_ptr,'"') )
		{/*95*/SensorCall();numeric |= 2;/*96*/}
	}
	SensorCall();if (numeric & 2) {
	    SensorCall();if (numeric & 1)		/* numeric is very good guess */
		{/*97*/SensorCall();str_cat(str," ");/*98*/}
	    else
		{/*99*/SensorCall();str_cat(str,"\377");/*100*/}
	    SensorCall();numeric = 1;
	}
	else
	    {/*101*/SensorCall();str_cat(str," ");/*102*/}
	SensorCall();str_scat(str,tmpstr);
	str_free(tmpstr);
	str_cat(str," ");
	str_scat(str,tmp2str);
	str_free(tmp2str);
	numeric = 1;
	SensorCall();break;
    case ORPAREN:
	SensorCall();str = str_new(0);
	str_set(str,"(");
	str_scat(str,fstr=walk(useval != 0,level,ops[node+1].ival,&numarg,P_MIN));
	str_free(fstr);
	numeric |= numarg;
	str_cat(str,")");
	SensorCall();break;
    case OMATCHOP:
	SensorCall();prec = P_MATCH;
	str = walk(1,level,ops[node+2].ival,&numarg,prec+1);
	str_cat(str," ");
	tmpstr = walk(0,level,ops[node+1].ival,&numarg,P_MIN);
	SensorCall();if (strEQ(tmpstr->str_ptr,"~"))
	    {/*103*/SensorCall();str_cat(str,"=~");/*104*/}
	else {
	    SensorCall();str_scat(str,tmpstr);
	    str_free(tmpstr);
	}
	SensorCall();str_cat(str," ");
	str_scat(str,fstr=walk(1,level,ops[node+3].ival,&numarg,prec+1));
	str_free(fstr);
	numeric = 1;
	SensorCall();break;
    case OMPAREN:
	SensorCall();str = str_new(0);
	str_set(str,"(");
	str_scat(str,
	  fstr=walk(useval != 0,level,ops[node+1].ival,&numarg,P_MIN));
	str_free(fstr);
	numeric |= numarg;
	str_cat(str,")");
	SensorCall();break;
    case OCONCAT:
	SensorCall();prec = P_ADD;
	type = ops[ops[node+1].ival].ival & 255;
	str = walk(1,level,ops[node+1].ival,&numarg,prec+(type != OCONCAT));
	str_cat(str," . ");
	type = ops[ops[node+2].ival].ival & 255;
	str_scat(str,
	  fstr=walk(1,level,ops[node+2].ival,&numarg,prec+(type != OCONCAT)));
	str_free(fstr);
	SensorCall();break;
    case OASSIGN:
	SensorCall();prec = P_ASSIGN;
	str = walk(0,level,ops[node+2].ival,&numarg,prec+1);
	str_cat(str," ");
	tmpstr = walk(0,level,ops[node+1].ival,&numarg,P_MIN);
	str_scat(str,tmpstr);
	SensorCall();if (str_len(tmpstr) > 1)
	    {/*105*/SensorCall();numeric = 1;/*106*/}
	SensorCall();str_free(tmpstr);
	str_cat(str," ");
	str_scat(str,fstr=walk(1,level,ops[node+3].ival,&numarg,prec));
	str_free(fstr);
	numeric |= numarg;
	SensorCall();if (strEQ(str->str_ptr,"$/ = ''"))
	    {/*107*/SensorCall();str_set(str, "$/ = \"\\n\\n\"");/*108*/}
	SensorCall();break;
    case OADD:
	SensorCall();prec = P_ADD;
	str = walk(1,level,ops[node+1].ival,&numarg,prec);
	str_cat(str," + ");
	str_scat(str,fstr=walk(1,level,ops[node+2].ival,&numarg,prec+1));
	str_free(fstr);
	numeric = 1;
	SensorCall();break;
    case OSUBTRACT:
	SensorCall();prec = P_ADD;
	str = walk(1,level,ops[node+1].ival,&numarg,prec);
	str_cat(str," - ");
	str_scat(str,fstr=walk(1,level,ops[node+2].ival,&numarg,prec+1));
	str_free(fstr);
	numeric = 1;
	SensorCall();break;
    case OMULT:
	SensorCall();prec = P_MUL;
	str = walk(1,level,ops[node+1].ival,&numarg,prec);
	str_cat(str," * ");
	str_scat(str,fstr=walk(1,level,ops[node+2].ival,&numarg,prec+1));
	str_free(fstr);
	numeric = 1;
	SensorCall();break;
    case ODIV:
	SensorCall();prec = P_MUL;
	str = walk(1,level,ops[node+1].ival,&numarg,prec);
	str_cat(str," / ");
	str_scat(str,fstr=walk(1,level,ops[node+2].ival,&numarg,prec+1));
	str_free(fstr);
	numeric = 1;
	SensorCall();break;
    case OPOW:
	SensorCall();prec = P_POW;
	str = walk(1,level,ops[node+1].ival,&numarg,prec+1);
	str_cat(str," ** ");
	str_scat(str,fstr=walk(1,level,ops[node+2].ival,&numarg,prec));
	str_free(fstr);
	numeric = 1;
	SensorCall();break;
    case OMOD:
	SensorCall();prec = P_MUL;
	str = walk(1,level,ops[node+1].ival,&numarg,prec);
	str_cat(str," % ");
	str_scat(str,fstr=walk(1,level,ops[node+2].ival,&numarg,prec+1));
	str_free(fstr);
	numeric = 1;
	SensorCall();break;
    case OPOSTINCR:
	SensorCall();prec = P_AUTO;
	str = walk(1,level,ops[node+1].ival,&numarg,prec+1);
	str_cat(str,"++");
	numeric = 1;
	SensorCall();break;
    case OPOSTDECR:
	SensorCall();prec = P_AUTO;
	str = walk(1,level,ops[node+1].ival,&numarg,prec+1);
	str_cat(str,"--");
	numeric = 1;
	SensorCall();break;
    case OPREINCR:
	SensorCall();prec = P_AUTO;
	str = str_new(0);
	str_set(str,"++");
	str_scat(str,fstr=walk(1,level,ops[node+1].ival,&numarg,prec+1));
	str_free(fstr);
	numeric = 1;
	SensorCall();break;
    case OPREDECR:
	SensorCall();prec = P_AUTO;
	str = str_new(0);
	str_set(str,"--");
	str_scat(str,fstr=walk(1,level,ops[node+1].ival,&numarg,prec+1));
	str_free(fstr);
	numeric = 1;
	SensorCall();break;
    case OUMINUS:
	SensorCall();prec = P_UNARY;
	str = str_new(0);
	str_set(str,"-");
	str_scat(str,fstr=walk(1,level,ops[node+1].ival,&numarg,prec));
	str_free(fstr);
	numeric = 1;
	SensorCall();break;
    case OUPLUS:
	SensorCall();numeric = 1;
	SensorCall();goto def;
    case OPAREN:
	SensorCall();str = str_new(0);
	str_set(str,"(");
	str_scat(str,
	  fstr=walk(useval != 0,level,ops[node+1].ival,&numarg,P_MIN));
	str_free(fstr);
	str_cat(str,")");
	numeric |= numarg;
	SensorCall();break;
    case OGETLINE:
	SensorCall();str = str_new(0);
	SensorCall();if (useval)
	    {/*109*/SensorCall();str_cat(str,"(");/*110*/}
	SensorCall();if (len > 0) {
	    SensorCall();str_scat(str,fstr=walk(1,level,ops[node+1].ival,&numarg,P_MIN));
	    SensorCall();if (!*fstr->str_ptr) {
		SensorCall();str_cat(str,"$_");
		len = 2;		/* a legal fiction */
	    }
	    SensorCall();str_free(fstr);
	}
	else
	    {/*111*/SensorCall();str_cat(str,"$_");/*112*/}
	SensorCall();if (len > 1) {
	    SensorCall();tmpstr=walk(1,level,ops[node+3].ival,&numarg,P_MIN);
	    fstr=walk(1,level,ops[node+2].ival,&numarg,P_MIN);
	    SensorCall();if (!do_fancy_opens) {
		SensorCall();t = tmpstr->str_ptr;
		SensorCall();if (*t == '"' || *t == '\'')
		    {/*113*/SensorCall();t = cpytill(tokenbuf,t+1,*t);/*114*/}
		else
		    fatal("Internal error: OGETLINE %s", t);
		SensorCall();d = savestr(t);
		s = savestr(tokenbuf);
		SensorCall();for (t = tokenbuf; *t; t++) {
		    SensorCall();*t &= 127;
		    SensorCall();if (isLOWER(*t))
			*t = toUPPER(*t);
		    SensorCall();if (!isALPHA(*t) && !isDIGIT(*t))
			{/*115*/SensorCall();*t = '_';/*116*/}
		}
		SensorCall();if (!strchr(tokenbuf,'_'))
		    {/*117*/SensorCall();strcpy(t,"_FH");/*118*/}
		SensorCall();tmp3str = hfetch(symtab,tokenbuf);
		SensorCall();if (!tmp3str) {
		    SensorCall();do_opens = TRUE;
		    str_cat(opens,"open(");
		    str_cat(opens,tokenbuf);
		    str_cat(opens,", ");
		    d[1] = '\0';
		    str_cat(opens,d);
		    str_cat(opens,tmpstr->str_ptr+1);
		    opens->str_cur--;
		    SensorCall();if (*fstr->str_ptr == '|')
			{/*119*/SensorCall();str_cat(opens,"|");/*120*/}
		    SensorCall();str_cat(opens,d);
		    SensorCall();if (*fstr->str_ptr == '|')
			{/*121*/SensorCall();str_cat(opens,") || die 'Cannot pipe from \"");/*122*/}
		    else
			{/*123*/SensorCall();str_cat(opens,") || die 'Cannot open file \"");/*124*/}
		    SensorCall();if (*d == '"')
			{/*125*/SensorCall();str_cat(opens,"'.\"");/*126*/}
		    SensorCall();str_cat(opens,s);
		    SensorCall();if (*d == '"')
			{/*127*/SensorCall();str_cat(opens,"\".'");/*128*/}
		    SensorCall();str_cat(opens,"\".';\n");
		    hstore(symtab,tokenbuf,str_make("x"));
		}
		SensorCall();safefree(s);
		safefree(d);
		str_set(tmpstr,"'");
		str_cat(tmpstr,tokenbuf);
		str_cat(tmpstr,"'");
	    }
	    SensorCall();if (*fstr->str_ptr == '|')
		{/*129*/SensorCall();str_cat(tmpstr,", '|'");/*130*/}
	    SensorCall();str_free(fstr);
	}
	else
	    {/*131*/SensorCall();tmpstr = str_make("");/*132*/}
	SensorCall();sprintf(tokenbuf," = &Getline%d(%s)",len,tmpstr->str_ptr);
	str_cat(str,tokenbuf); 
	str_free(tmpstr);
	SensorCall();if (useval)
	    {/*133*/SensorCall();str_cat(str,",$getline_ok)");/*134*/}
	SensorCall();saw_getline |= 1 << len;
	SensorCall();break;
    case OSPRINTF:
	SensorCall();str = str_new(0);
	str_set(str,"sprintf(");
	str_scat(str,fstr=walk(1,level,ops[node+1].ival,&numarg,P_MIN));
	str_free(fstr);
	str_cat(str,")");
	SensorCall();break;
    case OSUBSTR:
	SensorCall();str = str_new(0);
	str_set(str,"substr(");
	str_scat(str,fstr=walk(1,level,ops[node+1].ival,&numarg,P_COMMA+1));
	str_free(fstr);
	str_cat(str,", (");
	str_scat(str,fstr=walk(1,level,ops[node+2].ival,&numarg,P_COMMA+1));
	str_free(fstr);
	str_cat(str,")-1");
	SensorCall();if (len == 3) {
	    SensorCall();str_cat(str,", ");
	    str_scat(str,fstr=walk(1,level,ops[node+3].ival,&numarg,P_COMMA+1));
	    str_free(fstr);
	}
	SensorCall();str_cat(str,")");
	SensorCall();break;
    case OSTRING:
	SensorCall();str = str_new(0);
	str_set(str,ops[node+1].cval);
	SensorCall();break;
    case OSPLIT:
	SensorCall();str = str_new(0);
	limit = ", -1)";
	numeric = 1;
	tmpstr = walk(1,level,ops[node+2].ival,&numarg,P_MIN);
	SensorCall();if (useval)
	    {/*135*/SensorCall();str_set(str,"(@");/*136*/}
	else
	    {/*137*/SensorCall();str_set(str,"@");/*138*/}
	SensorCall();str_scat(str,tmpstr);
	str_cat(str," = split(");
	SensorCall();if (len == 3) {
	    SensorCall();fstr = walk(1,level,ops[node+3].ival,&numarg,P_COMMA+1);
	    SensorCall();if (str_len(fstr) == 3 && *fstr->str_ptr == '\'') {
		SensorCall();i = fstr->str_ptr[1] & 127;
		SensorCall();if (strchr("*+?.[]()|^$\\",i))
		    {/*139*/SensorCall();sprintf(tokenbuf,"/\\%c/",i);/*140*/}
		else {/*141*/SensorCall();if (i == ' ')
		    {/*143*/SensorCall();sprintf(tokenbuf,"' '");/*144*/}
		else
		    {/*145*/SensorCall();sprintf(tokenbuf,"/%c/",i);/*146*/}/*142*/}
		SensorCall();str_cat(str,tokenbuf);
	    }
	    else
		{/*147*/SensorCall();str_scat(str,fstr);/*148*/}
	    SensorCall();str_free(fstr);
	}
	else {/*149*/SensorCall();if (const_FS) {
	    SensorCall();sprintf(tokenbuf,"/[%c\\n]/",const_FS);
	    str_cat(str,tokenbuf);
	}
	else {/*151*/SensorCall();if (saw_FS)
	    {/*153*/SensorCall();str_cat(str,"$FS");/*154*/}
	else {
	    SensorCall();str_cat(str,"' '");
	    limit = ")";
	;/*152*/}/*150*/}}
	SensorCall();str_cat(str,", ");
	str_scat(str,fstr=walk(1,level,ops[node+1].ival,&numarg,P_COMMA+1));
	str_free(fstr);
	str_cat(str,limit);
	SensorCall();if (useval) {
	    SensorCall();str_cat(str,")");
	}
	SensorCall();str_free(tmpstr);
	SensorCall();break;
    case OINDEX:
	SensorCall();str = str_new(0);
	str_set(str,"(1+index(");
	str_scat(str,fstr=walk(1,level,ops[node+1].ival,&numarg,P_COMMA+1));
	str_free(fstr);
	str_cat(str,", ");
	str_scat(str,fstr=walk(1,level,ops[node+2].ival,&numarg,P_COMMA+1));
	str_free(fstr);
	str_cat(str,"))");
	numeric = 1;
	SensorCall();break;
    case OMATCH:
	SensorCall();str = str_new(0);
	prec = P_ANDAND;
	str_scat(str,fstr=walk(1,level,ops[node+1].ival,&numarg,P_MATCH+1));
	str_free(fstr);
	str_cat(str," =~ ");
	str_scat(str,fstr=walk(1,level,ops[node+2].ival,&numarg,P_MATCH+1));
	str_free(fstr);
	str_cat(str," ? scalar($RLENGTH = length($&), $RSTART = length($`)+1) : 0");
	numeric = 1;
	SensorCall();break;
    case OUSERDEF:
	SensorCall();str = str_new(0);
	subretnum = FALSE;
	fstr=walk(1,level-1,ops[node+2].ival,&numarg,P_MIN);
	curargs = str_new(0);
	str_sset(curargs,fstr);
	str_cat(curargs,",");
	tmp2str=walk(1,level,ops[node+5].ival,&numarg,P_MIN);
	str_free(curargs);
	curargs = NULL;
	level--;
	subretnum |= numarg;
	s = NULL;
	t = tmp2str->str_ptr;
	SensorCall();while ((t = instr(t,"return ")))
	    {/*155*/SensorCall();s = t++;/*156*/}
	SensorCall();if (s) {
	    SensorCall();i = 0;
	    SensorCall();for (t = s+7; *t; t++) {
		SensorCall();if (*t == ';' || *t == '}')
		    {/*157*/SensorCall();i++;/*158*/}
	    }
	    SensorCall();if (i == 1) {
		SensorCall();strcpy(s,s+7);
		tmp2str->str_cur -= 7;
	    }
	}
	SensorCall();str_set(str,"\n");
	tab(str,level);
	str_cat(str,"sub ");
	str_scat(str,tmpstr=walk(1,level,ops[node+1].ival,&numarg,P_MIN));
	str_cat(str," {\n");
	tab(str,++level);
	SensorCall();if (fstr->str_cur) {
	    SensorCall();str_cat(str,"local(");
	    str_scat(str,fstr);
	    str_cat(str,") = @_;");
	}
	SensorCall();str_free(fstr);
	str_scat(str,fstr=walk(1,level,ops[node+3].ival,&numarg,P_MIN));
	str_free(fstr);
	fixtab(str,level);
	str_scat(str,fstr=walk(1,level,ops[node+4].ival,&numarg,P_MIN));
	str_free(fstr);
	fixtab(str,level);
	str_scat(str,tmp2str);
	str_free(tmp2str);
	fixtab(str,--level);
	str_cat(str,"}\n");
	tab(str,level);
	str_scat(subs,str);
	str_set(str,"");
	str_cat(tmpstr,"(");
	tmp2str = str_new(0);
	SensorCall();if (subretnum)
	    {/*159*/SensorCall();str_set(tmp2str,"1");/*160*/}
	SensorCall();hstore(symtab,tmpstr->str_ptr,tmp2str);
	str_free(tmpstr);
	level++;
	SensorCall();break;
    case ORETURN:
	SensorCall();str = str_new(0);
	SensorCall();if (len > 0) {
	    SensorCall();str_cat(str,"return ");
	    str_scat(str,fstr=walk(1,level,ops[node+1].ival,&numarg,P_UNI+1));
	    str_free(fstr);
	    SensorCall();if (numarg)
		subretnum = TRUE;
	}
	else
	    {/*161*/SensorCall();str_cat(str,"return");/*162*/}
	SensorCall();break;
    case OUSERFUN:
	SensorCall();str = str_new(0);
	str_set(str,"&");
	str_scat(str,fstr=walk(1,level,ops[node+1].ival,&numarg,P_MIN));
	str_free(fstr);
	str_cat(str,"(");
	tmpstr = hfetch(symtab,str->str_ptr+3);
	SensorCall();if (tmpstr && tmpstr->str_ptr)
	    {/*163*/SensorCall();numeric |= atoi(tmpstr->str_ptr);/*164*/}
	SensorCall();str_scat(str,fstr=walk(1,level,ops[node+2].ival,&numarg,P_MIN));
	str_free(fstr);
	str_cat(str,")");
	SensorCall();break;
    case OGSUB:
    case OSUB: {
	SensorCall();int gsub = type == OGSUB ? 1 : 0;
	str = str_new(0);
	tmpstr = str_new(0);
	i = 0;
	SensorCall();if (len == 3) {
	    SensorCall();tmpstr = walk(1,level,ops[node+3].ival,&numarg,P_MATCH+1);
	    SensorCall();if (strNE(tmpstr->str_ptr,"$_")) {
		SensorCall();str_cat(tmpstr, " =~ s");
		i++;
	    }
	    else
		{/*165*/SensorCall();str_set(tmpstr, "s");/*166*/}
	}
	else
	    {/*167*/SensorCall();str_set(tmpstr, "s");/*168*/}
	SensorCall();type = ops[ops[node+2].ival].ival;
	len = type >> 8;
	type &= 255;
	tmp3str = str_new(0);
	{
	  const char *s;
	  SensorCall();if (type == OSTR) {
	    SensorCall();tmp2str=walk(1,level,ops[ops[node+2].ival+1].ival,&numarg,P_MIN);
	    SensorCall();for (t = tmp2str->str_ptr, d=tokenbuf; *t; d++,t++) {
		SensorCall();if (*t == '&')
                   {/*169*/SensorCall();*d++ = '$' + (char)128;/*170*/}
		else {/*171*/SensorCall();if (*t == '$' || *t == '/')
                   {/*173*/SensorCall();*d++ = '\\' + (char)128;/*174*/}/*172*/}
		SensorCall();*d = *t + 128;
	    }
	    SensorCall();*d = '\0';
	    str_set(tmp2str,tokenbuf);
	    s = (gsub ? "/g" : "/");
	  }
	  else {
	    SensorCall();tmp2str=walk(1,level,ops[node+2].ival,&numarg,P_MIN);
	    str_set(tmp3str,"($s_ = '\"'.(");
	    str_scat(tmp3str,tmp2str);
	    str_cat(tmp3str,").'\"') =~ s/&/\\$&/g, ");
	    str_set(tmp2str,"eval $s_");
	    s = (gsub ? "/ge" : "/e");
	    i++;
	  }
	  SensorCall();str_cat(tmp2str,s);
	}
	type = ops[ops[node+1].ival].ival;
	len = type >> 8;
	type &= 255;
	fstr=walk(1,level,ops[node+1].ival,&numarg,P_MIN);
	SensorCall();if (type == OREGEX) {
	    SensorCall();if (useval && i)
		{/*175*/SensorCall();str_cat(str,"(");/*176*/}
	    SensorCall();str_scat(str,tmp3str);
	    str_scat(str,tmpstr);
	    str_scat(str,fstr);
	    str_scat(str,tmp2str);
	}
	else {/*177*/SensorCall();if ((type == OFLD && !split_to_array) || (type == OVAR && len == 1)) {
	    SensorCall();if (useval && i)
		{/*179*/SensorCall();str_cat(str,"(");/*180*/}
	    SensorCall();str_scat(str,tmp3str);
	    str_scat(str,tmpstr);
	    str_cat(str,"/");
	    str_scat(str,fstr);
	    str_cat(str,"/");
	    str_scat(str,tmp2str);
	}
	else {
	    SensorCall();i++;
	    SensorCall();if (useval)
		{/*181*/SensorCall();str_cat(str,"(");/*182*/}
	    SensorCall();str_cat(str,"$s = ");
	    str_scat(str,fstr);
	    str_cat(str,", ");
	    str_scat(str,tmp3str);
	    str_scat(str,tmpstr);
	    str_cat(str,"/$s/");
	    str_scat(str,tmp2str);
	;/*178*/}}
	SensorCall();if (useval && i)
	    {/*183*/SensorCall();str_cat(str,")");/*184*/}
	SensorCall();str_free(fstr);
	str_free(tmpstr);
	str_free(tmp2str);
	str_free(tmp3str);
	numeric = 1;
	SensorCall();break; }
    case ONUM:
	SensorCall();str = walk(1,level,ops[node+1].ival,&numarg,P_MIN);
	numeric = 1;
	SensorCall();break;
    case OSTR:
	SensorCall();tmpstr = walk(1,level,ops[node+1].ival,&numarg,P_MIN);
	{
	  const char *s = "'";
	  SensorCall();for (t = tmpstr->str_ptr, d=tokenbuf; *t; d++,t++) {
	    SensorCall();if (*t == '\'')
		{/*185*/SensorCall();s = "\"";/*186*/}
	    else {/*187*/SensorCall();if (*t == '\\') {
		SensorCall();s = "\"";
		*d++ = *t++ + 128;
		SensorCall();switch (*t) {
		case '\\': case '"': case 'n': case 't': case '$':
		    SensorCall();break;
		default:	/* hide this from perl */
                   SensorCall();*d++ = '\\' + (char)128;
		}
	    ;/*188*/}}
	    SensorCall();*d = *t + 128;
	  }
	  SensorCall();*d = '\0';
	  str = str_new(0);
	  str_set(str,s);
	  str_cat(str,tokenbuf);
	  str_free(tmpstr);
	  str_cat(str,s);
	}
	SensorCall();break;
    case ODEFINED:
	SensorCall();prec = P_UNI;
	str = str_new(0);
	str_set(str,"defined $");
	SensorCall();goto addvar;
    case ODELETE:
	SensorCall();str = str_new(0);
	str_set(str,"delete $");
	SensorCall();goto addvar;
    case OSTAR:
	SensorCall();str = str_new(0);
	str_set(str,"*");
	SensorCall();goto addvar;
    case OVAR:
	SensorCall();str = str_new(0);
	str_set(str,"$");
      addvar:
	str_scat(str,tmpstr=walk(1,level,ops[node+1].ival,&numarg,P_MIN));
	SensorCall();if (len == 1) {
	    SensorCall();tmp2str = hfetch(symtab,tmpstr->str_ptr);
	    SensorCall();if (tmp2str && atoi(tmp2str->str_ptr))
		{/*189*/SensorCall();numeric = 2;/*190*/}
	    SensorCall();if (strEQ(str->str_ptr,"$FNR")) {
		SensorCall();numeric = 1;
		saw_FNR++;
		str_set(str,"($.-$FNRbase)");
	    }
	    else {/*191*/SensorCall();if (strEQ(str->str_ptr,"$NR")) {
		SensorCall();numeric = 1;
		str_set(str,"$.");
	    }
	    else {/*193*/SensorCall();if (strEQ(str->str_ptr,"$NF")) {
		SensorCall();numeric = 1;
		str_set(str,"($#Fld+1)");
	    }
	    else {/*195*/SensorCall();if (strEQ(str->str_ptr,"$0"))
		{/*197*/SensorCall();str_set(str,"$_");/*198*/}
	    else {/*199*/SensorCall();if (strEQ(str->str_ptr,"$ARGC"))
		{/*201*/SensorCall();str_set(str,"($#ARGV+2)");/*202*/}/*200*/}/*196*/}/*194*/}/*192*/}
	}
	else {
#ifdef NOTDEF
	    if (curargs) {
		sprintf(tokenbuf,"$%s,",tmpstr->str_ptr);
	???	if (instr(curargs->str_ptr,tokenbuf))
		    str_cat(str,"\377");	/* can't translate yet */
	    }
#endif
	    SensorCall();str_cat(tmpstr,"[]");
	    tmp2str = hfetch(symtab,tmpstr->str_ptr);
	    SensorCall();if (tmp2str && atoi(tmp2str->str_ptr))
		{/*203*/SensorCall();str_cat(str,"[(");/*204*/}
	    else
		{/*205*/SensorCall();str_cat(str,"{");/*206*/}
	    SensorCall();str_scat(str,fstr=walk(1,level,ops[node+2].ival,&numarg,P_MIN));
	    str_free(fstr);
	    SensorCall();if (strEQ(str->str_ptr,"$ARGV[0")) {
		SensorCall();str_set(str,"$ARGV0");
		saw_argv0++;
	    }
	    else {
		SensorCall();if (tmp2str && atoi(tmp2str->str_ptr))
		    {/*207*/SensorCall();strcpy(tokenbuf,")-1]");/*208*/}
		else
		    {/*209*/SensorCall();strcpy(tokenbuf,"}");/*210*/}
               SensorCall();*tokenbuf += (char)128;
		str_cat(str,tokenbuf);
	    }
	}
	SensorCall();str_free(tmpstr);
	SensorCall();break;
    case OFLD:
	SensorCall();str = str_new(0);
	SensorCall();if (split_to_array) {
	    SensorCall();str_set(str,"$Fld");
	    str_cat(str,"[(");
	    str_scat(str,fstr=walk(1,level,ops[node+1].ival,&numarg,P_MIN));
	    str_free(fstr);
	    str_cat(str,")-1]");
	}
	else {
	    SensorCall();i = atoi(walk(1,level,ops[node+1].ival,&numarg,P_MIN)->str_ptr);
	    SensorCall();if (i <= arymax)
		{/*211*/SensorCall();sprintf(tokenbuf,"$%s",nameary[i]);/*212*/}
	    else
		{/*213*/SensorCall();sprintf(tokenbuf,"$Fld%d",i);/*214*/}
	    SensorCall();str_set(str,tokenbuf);
	}
	SensorCall();break;
    case OVFLD:
	SensorCall();str = str_new(0);
	str_set(str,"$Fld[");
	i = ops[node+1].ival;
	SensorCall();if ((ops[i].ival & 255) == OPAREN)
	    {/*215*/SensorCall();i = ops[i+1].ival;/*216*/}
	SensorCall();tmpstr=walk(1,level,i,&numarg,P_MIN);
	str_scat(str,tmpstr);
	str_free(tmpstr);
	str_cat(str,"]");
	SensorCall();break;
    case OJUNK:
	SensorCall();goto def;
    case OSNEWLINE:
	SensorCall();str = str_new(2);
	str_set(str,";\n");
	tab(str,level);
	SensorCall();break;
    case ONEWLINE:
	SensorCall();str = str_new(1);
	str_set(str,"\n");
	tab(str,level);
	SensorCall();break;
    case OSCOMMENT:
	SensorCall();str = str_new(0);
	str_set(str,";");
	tmpstr = walk(0,level,ops[node+1].ival,&numarg,P_MIN);
	SensorCall();for (s = tmpstr->str_ptr; *s && *s != '\n'; s++)
           {/*217*/SensorCall();*s += (char)128;/*218*/}
	SensorCall();str_scat(str,tmpstr);
	str_free(tmpstr);
	tab(str,level);
	SensorCall();break;
    case OCOMMENT:
	SensorCall();str = str_new(0);
	tmpstr = walk(0,level,ops[node+1].ival,&numarg,P_MIN);
	SensorCall();for (s = tmpstr->str_ptr; *s && *s != '\n'; s++)
           {/*219*/SensorCall();*s += (char)128;/*220*/}
	SensorCall();str_scat(str,tmpstr);
	str_free(tmpstr);
	tab(str,level);
	SensorCall();break;
    case OCOMMA:
	SensorCall();prec = P_COMMA;
	str = walk(1,level,ops[node+1].ival,&numarg,prec);
	str_cat(str,", ");
	str_scat(str,fstr=walk(1,level,ops[node+2].ival,&numarg,P_MIN));
	str_free(fstr);
	str_scat(str,fstr=walk(1,level,ops[node+3].ival,&numarg,prec+1));
	str_free(fstr);
	SensorCall();break;
    case OSEMICOLON:
	SensorCall();str = str_new(1);
	str_set(str,";\n");
	tab(str,level);
	SensorCall();break;
    case OSTATES:
	SensorCall();str = walk(0,level,ops[node+1].ival,&numarg,P_MIN);
	str_scat(str,fstr=walk(0,level,ops[node+2].ival,&numarg,P_MIN));
	str_free(fstr);
	SensorCall();break;
    case OSTATE:
	SensorCall();str = str_new(0);
	SensorCall();if (len >= 1) {
	    SensorCall();str_scat(str,fstr=walk(0,level,ops[node+1].ival,&numarg,P_MIN));
	    str_free(fstr);
	    SensorCall();if (len >= 2) {
		SensorCall();tmpstr = walk(0,level,ops[node+2].ival,&numarg,P_MIN);
		SensorCall();if (*tmpstr->str_ptr == ';') {
		    SensorCall();addsemi(str);
		    str_cat(str,tmpstr->str_ptr+1);
		}
		SensorCall();str_free(tmpstr);
	    }
	}
	SensorCall();break;
    case OCLOSE:
	SensorCall();str = str_make("close(");
	tmpstr = walk(1,level,ops[node+1].ival,&numarg,P_MIN);
	SensorCall();if (!do_fancy_opens) {
	    SensorCall();t = tmpstr->str_ptr;
	    SensorCall();if (*t == '"' || *t == '\'')
		{/*221*/SensorCall();t = cpytill(tokenbuf,t+1,*t);/*222*/}
	    else
		fatal("Internal error: OCLOSE %s",t);
	    SensorCall();s = savestr(tokenbuf);
	    SensorCall();for (t = tokenbuf; *t; t++) {
		SensorCall();*t &= 127;
		SensorCall();if (isLOWER(*t))
		    *t = toUPPER(*t);
		SensorCall();if (!isALPHA(*t) && !isDIGIT(*t))
		    {/*223*/SensorCall();*t = '_';/*224*/}
	    }
	    SensorCall();if (!strchr(tokenbuf,'_'))
		{/*225*/SensorCall();strcpy(t,"_FH");/*226*/}
	    SensorCall();str_free(tmpstr);
	    safefree(s);
	    str_set(str,"close ");
	    str_cat(str,tokenbuf);
	}
	else {
	    SensorCall();sprintf(tokenbuf,"delete $opened{%s} && close(%s)",
	       tmpstr->str_ptr, tmpstr->str_ptr);
	    str_free(tmpstr);
	    str_set(str,tokenbuf);
	}
	SensorCall();break;
    case OPRINTF:
    case OPRINT:
	SensorCall();lparen = "";	/* set to parens if necessary */
	rparen = "";
	str = str_new(0);
	SensorCall();if (len == 3) {		/* output redirection */
	    SensorCall();tmpstr = walk(1,level,ops[node+3].ival,&numarg,P_MIN);
	    tmp2str = walk(1,level,ops[node+2].ival,&numarg,P_MIN);
	    SensorCall();if (!do_fancy_opens) {
		SensorCall();t = tmpstr->str_ptr;
		SensorCall();if (*t == '"' || *t == '\'')
		    {/*227*/SensorCall();t = cpytill(tokenbuf,t+1,*t);/*228*/}
		else
		    fatal("Internal error: OPRINT");
		SensorCall();d = savestr(t);
		s = savestr(tokenbuf);
		SensorCall();for (t = tokenbuf; *t; t++) {
		    SensorCall();*t &= 127;
		    SensorCall();if (isLOWER(*t))
			*t = toUPPER(*t);
		    SensorCall();if (!isALPHA(*t) && !isDIGIT(*t))
			{/*229*/SensorCall();*t = '_';/*230*/}
		}
		SensorCall();if (!strchr(tokenbuf,'_'))
		    {/*231*/SensorCall();strcpy(t,"_FH");/*232*/}
		SensorCall();tmp3str = hfetch(symtab,tokenbuf);
		SensorCall();if (!tmp3str) {
		    SensorCall();str_cat(opens,"open(");
		    str_cat(opens,tokenbuf);
		    str_cat(opens,", ");
		    d[1] = '\0';
		    str_cat(opens,d);
		    str_scat(opens,tmp2str);
		    str_cat(opens,tmpstr->str_ptr+1);
		    SensorCall();if (*tmp2str->str_ptr == '|')
			{/*233*/SensorCall();str_cat(opens,") || die 'Cannot pipe to \"");/*234*/}
		    else
			{/*235*/SensorCall();str_cat(opens,") || die 'Cannot create file \"");/*236*/}
		    SensorCall();if (*d == '"')
			{/*237*/SensorCall();str_cat(opens,"'.\"");/*238*/}
		    SensorCall();str_cat(opens,s);
		    SensorCall();if (*d == '"')
			{/*239*/SensorCall();str_cat(opens,"\".'");/*240*/}
		    SensorCall();str_cat(opens,"\".';\n");
		    hstore(symtab,tokenbuf,str_make("x"));
		}
		SensorCall();str_free(tmpstr);
		str_free(tmp2str);
		safefree(s);
		safefree(d);
	    }
	    else {
		SensorCall();sprintf(tokenbuf,"&Pick('%s', %s) &&\n",
		   tmp2str->str_ptr, tmpstr->str_ptr);
		str_cat(str,tokenbuf);
		tab(str,level+1);
		strcpy(tokenbuf,"$fh");
		str_free(tmpstr);
		str_free(tmp2str);
		lparen = "(";
		rparen = ")";
	    }
	}
	else
	    {/*241*/SensorCall();strcpy(tokenbuf,"");/*242*/}
	SensorCall();str_cat(str,lparen);	/* may be null */
	SensorCall();if (type == OPRINTF)
	    {/*243*/SensorCall();str_cat(str,"printf");/*244*/}
	else
	    {/*245*/SensorCall();str_cat(str,"print");/*246*/}
	SensorCall();saw_fh = 0;
	SensorCall();if (len == 3 || do_fancy_opens) {
	    SensorCall();if (*tokenbuf) {
		SensorCall();str_cat(str," ");
		saw_fh = 1;
	    }
	    SensorCall();str_cat(str,tokenbuf);
	}
	SensorCall();tmpstr = walk(1+(type==OPRINT),level,ops[node+1].ival,&numarg,P_MIN);
	SensorCall();if (!*tmpstr->str_ptr && lval_field) {
	    SensorCall();const char *t = (saw_OFS ? "$," : "' '");
	    SensorCall();if (split_to_array) {
		SensorCall();sprintf(tokenbuf,"join(%s,@Fld)",t);
		str_cat(tmpstr,tokenbuf);
	    }
	    else {
		SensorCall();for (i = 1; i < maxfld; i++) {
		    SensorCall();if (i <= arymax)
			{/*247*/SensorCall();sprintf(tokenbuf,"$%s, ",nameary[i]);/*248*/}
		    else
			{/*249*/SensorCall();sprintf(tokenbuf,"$Fld%d, ",i);/*250*/}
		    SensorCall();str_cat(tmpstr,tokenbuf);
		}
		SensorCall();if (maxfld <= arymax)
		    {/*251*/SensorCall();sprintf(tokenbuf,"$%s",nameary[maxfld]);/*252*/}
		else
		    {/*253*/SensorCall();sprintf(tokenbuf,"$Fld%d",maxfld);/*254*/}
		SensorCall();str_cat(tmpstr,tokenbuf);
	    }
	}
	SensorCall();if (*tmpstr->str_ptr) {
	    SensorCall();str_cat(str," ");
	    SensorCall();if (!saw_fh && *tmpstr->str_ptr == '(') {
		SensorCall();str_cat(str,"(");
		str_scat(str,tmpstr);
		str_cat(str,")");
	    }
	    else
		{/*255*/SensorCall();str_scat(str,tmpstr);/*256*/}
	}
	else {
	    SensorCall();str_cat(str," $_");
	}
	SensorCall();str_cat(str,rparen);	/* may be null */
	str_free(tmpstr);
	SensorCall();break;
    case ORAND:
	SensorCall();str = str_make("rand(1)");
	SensorCall();break;
    case OSRAND:
	SensorCall();str = str_make("srand(");
	SensorCall();goto maybe0;
    case OATAN2:
	SensorCall();str = str_make("atan2(");
	SensorCall();goto maybe0;
    case OSIN:
	SensorCall();str = str_make("sin(");
	SensorCall();goto maybe0;
    case OCOS:
	SensorCall();str = str_make("cos(");
	SensorCall();goto maybe0;
    case OSYSTEM:
	SensorCall();str = str_make("system(");
	SensorCall();goto maybe0;
    case OLENGTH:
	SensorCall();str = str_make("length(");
	SensorCall();goto maybe0;
    case OLOG:
	SensorCall();str = str_make("log(");
	SensorCall();goto maybe0;
    case OEXP:
	SensorCall();str = str_make("exp(");
	SensorCall();goto maybe0;
    case OSQRT:
	SensorCall();str = str_make("sqrt(");
	SensorCall();goto maybe0;
    case OINT:
	SensorCall();str = str_make("int(");
      maybe0:
	numeric = 1;
	SensorCall();if (len > 0)
	    {/*257*/SensorCall();tmpstr = walk(1,level,ops[node+1].ival,&numarg,P_MIN);/*258*/}
	else
	    {/*259*/SensorCall();tmpstr = str_new(0);/*260*/}
	SensorCall();if (!tmpstr->str_ptr || !*tmpstr->str_ptr) {
	    SensorCall();if (lval_field) {
		SensorCall();const char *t = (saw_OFS ? "$," : "' '");
		SensorCall();if (split_to_array) {
		    SensorCall();sprintf(tokenbuf,"join(%s,@Fld)",t);
		    str_cat(tmpstr,tokenbuf);
		}
		else {
		    SensorCall();sprintf(tokenbuf,"join(%s, ",t);
		    str_cat(tmpstr,tokenbuf);
		    SensorCall();for (i = 1; i < maxfld; i++) {
			SensorCall();if (i <= arymax)
			    {/*261*/SensorCall();sprintf(tokenbuf,"$%s,",nameary[i]);/*262*/}
			else
			    {/*263*/SensorCall();sprintf(tokenbuf,"$Fld%d,",i);/*264*/}
			SensorCall();str_cat(tmpstr,tokenbuf);
		    }
		    SensorCall();if (maxfld <= arymax)
			{/*265*/SensorCall();sprintf(tokenbuf,"$%s)",nameary[maxfld]);/*266*/}
		    else
			{/*267*/SensorCall();sprintf(tokenbuf,"$Fld%d)",maxfld);/*268*/}
		    SensorCall();str_cat(tmpstr,tokenbuf);
		}
	    }
	    else
		{/*269*/SensorCall();str_cat(tmpstr,"$_");/*270*/}
	}
	SensorCall();if (strEQ(tmpstr->str_ptr,"$_")) {
	    SensorCall();if (type == OLENGTH && !do_chop) {
		SensorCall();str = str_make("(length(");
		str_cat(tmpstr,") - 1");
	    }
	}
	SensorCall();str_scat(str,tmpstr);
	str_free(tmpstr);
	str_cat(str,")");
	SensorCall();break;
    case OBREAK:
	SensorCall();str = str_new(0);
	str_set(str,"last");
	SensorCall();break;
    case ONEXT:
	SensorCall();str = str_new(0);
	str_set(str,"next line");
	SensorCall();break;
    case OEXIT:
	SensorCall();str = str_new(0);
	SensorCall();if (realexit) {
	    SensorCall();prec = P_UNI;
	    str_set(str,"exit");
	    SensorCall();if (len == 1) {
		SensorCall();str_cat(str," ");
		exitval = TRUE;
		str_scat(str,
		  fstr=walk(1,level,ops[node+1].ival,&numarg,prec+1));
		str_free(fstr);
	    }
	}
	else {
	    SensorCall();if (len == 1) {
		SensorCall();str_set(str,"$ExitValue = ");
		exitval = TRUE;
		str_scat(str,
		  fstr=walk(1,level,ops[node+1].ival,&numarg,P_ASSIGN));
		str_free(fstr);
		str_cat(str,"; ");
	    }
	    SensorCall();str_cat(str,"last line");
	}
	SensorCall();break;
    case OCONTINUE:
	SensorCall();str = str_new(0);
	str_set(str,"next");
	SensorCall();break;
    case OREDIR:
	SensorCall();goto def;
    case OIF:
	SensorCall();str = str_new(0);
	str_set(str,"if (");
	str_scat(str,fstr=walk(1,level,ops[node+1].ival,&numarg,P_MIN));
	str_free(fstr);
	str_cat(str,") ");
	str_scat(str,fstr=walk(0,level,ops[node+2].ival,&numarg,P_MIN));
	str_free(fstr);
	SensorCall();if (len == 3) {
	    SensorCall();i = ops[node+3].ival;
	    SensorCall();if (i) {
		SensorCall();if ((ops[i].ival & 255) == OBLOCK) {
		    SensorCall();i = ops[i+1].ival;
		    SensorCall();if (i) {
			SensorCall();if ((ops[i].ival & 255) != OIF)
			    {/*271*/SensorCall();i = 0;/*272*/}
		    }
		}
		else
		    {/*273*/SensorCall();i = 0;/*274*/}
	    }
	    SensorCall();if (i) {
		SensorCall();str_cat(str,"els");
		str_scat(str,fstr=walk(0,level,i,&numarg,P_MIN));
		str_free(fstr);
	    }
	    else {
		SensorCall();str_cat(str,"else ");
		str_scat(str,fstr=walk(0,level,ops[node+3].ival,&numarg,P_MIN));
		str_free(fstr);
	    }
	}
	SensorCall();break;
    case OWHILE:
	SensorCall();str = str_new(0);
	str_set(str,"while (");
	str_scat(str,fstr=walk(1,level,ops[node+1].ival,&numarg,P_MIN));
	str_free(fstr);
	str_cat(str,") ");
	str_scat(str,fstr=walk(0,level,ops[node+2].ival,&numarg,P_MIN));
	str_free(fstr);
	SensorCall();break;
    case ODO:
	SensorCall();str = str_new(0);
	str_set(str,"do ");
	str_scat(str,fstr=walk(1,level,ops[node+1].ival,&numarg,P_MIN));
	str_free(fstr);
	SensorCall();if (str->str_ptr[str->str_cur - 1] == '\n')
	    {/*275*/SensorCall();--str->str_cur;/*276*/}
	SensorCall();str_cat(str," while (");
	str_scat(str,fstr=walk(0,level,ops[node+2].ival,&numarg,P_MIN));
	str_free(fstr);
	str_cat(str,");");
	SensorCall();break;
    case OFOR:
	SensorCall();str = str_new(0);
	str_set(str,"for (");
	str_scat(str,tmpstr=walk(1,level,ops[node+1].ival,&numarg,P_MIN));
	i = numarg;
	SensorCall();if (i) {
	    SensorCall();t = s = tmpstr->str_ptr;
	    SensorCall();while (isALPHA(*t) || isDIGIT(*t) || *t == '$' || *t == '_')
		{/*277*/SensorCall();t++;/*278*/}
	    SensorCall();i = t - s;
	    SensorCall();if (i < 2)
		{/*279*/SensorCall();i = 0;/*280*/}
	}
	SensorCall();str_cat(str,"; ");
	fstr=walk(1,level,ops[node+2].ival,&numarg,P_MIN);
	SensorCall();if (i && (t = strchr(fstr->str_ptr,0377))) {
	    SensorCall();if (strnEQ(fstr->str_ptr,s,i))
		{/*281*/SensorCall();*t = ' ';/*282*/}
	}
	SensorCall();str_scat(str,fstr);
	str_free(fstr);
	str_free(tmpstr);
	str_cat(str,"; ");
	str_scat(str,fstr=walk(1,level,ops[node+3].ival,&numarg,P_MIN));
	str_free(fstr);
	str_cat(str,") ");
	str_scat(str,fstr=walk(0,level,ops[node+4].ival,&numarg,P_MIN));
	str_free(fstr);
	SensorCall();break;
    case OFORIN:
	SensorCall();tmpstr = walk(0,level,ops[node+1].ival,&numarg,P_MIN);
	d = strchr(tmpstr->str_ptr,'$');
	SensorCall();if (!d)
	    fatal("Illegal for loop: %s",tmpstr->str_ptr);
	SensorCall();s = strchr(d,'{');
	SensorCall();if (!s)
	    {/*283*/SensorCall();s = strchr(d,'[');/*284*/}
	SensorCall();if (!s)
	    fatal("Illegal for loop: %s",d);
	SensorCall();*s++ = '\0';
	SensorCall();for (t = s; (i = *t); t++) {
	    SensorCall();i &= 127;
	    SensorCall();if (i == '}' || i == ']')
		{/*285*/SensorCall();break;/*286*/}
	}
	SensorCall();if (*t)
	    {/*287*/SensorCall();*t = '\0';/*288*/}
	SensorCall();str = str_new(0);
	str_set(str,d+1);
	str_cat(str,"[]");
	tmp2str = hfetch(symtab,str->str_ptr);
	SensorCall();if (tmp2str && atoi(tmp2str->str_ptr)) {
	    SensorCall();sprintf(tokenbuf,
	      "foreach %s (0 .. $#%s) ",
	      s,
	      d+1);
	}
	else {
	    SensorCall();sprintf(tokenbuf,
	      "foreach %s (keys %%%s) ",
	      s,
	      d+1);
	}
	SensorCall();str_set(str,tokenbuf);
	str_scat(str,fstr=walk(0,level,ops[node+2].ival,&numarg,P_MIN));
	str_free(fstr);
	str_free(tmpstr);
	SensorCall();break;
    case OBLOCK:
	SensorCall();str = str_new(0);
	str_set(str,"{");
	SensorCall();if (len >= 2 && ops[node+2].ival) {
	    SensorCall();str_scat(str,fstr=walk(0,level,ops[node+2].ival,&numarg,P_MIN));
	    str_free(fstr);
	}
	SensorCall();fixtab(str,++level);
	str_scat(str,fstr=walk(0,level,ops[node+1].ival,&numarg,P_MIN));
	str_free(fstr);
	addsemi(str);
	fixtab(str,--level);
	str_cat(str,"}\n");
	tab(str,level);
	SensorCall();if (len >= 3) {
	    SensorCall();str_scat(str,fstr=walk(0,level,ops[node+3].ival,&numarg,P_MIN));
	    str_free(fstr);
	}
	SensorCall();break;
    default:
      def:
	SensorCall();if (len) {
	    SensorCall();if (len > 5)
		fatal("Garbage length in walk");
	    SensorCall();str = walk(0,level,ops[node+1].ival,&numarg,P_MIN);
	    SensorCall();for (i = 2; i<= len; i++) {
		SensorCall();str_scat(str,fstr=walk(0,level,ops[node+i].ival,&numarg,P_MIN));
		str_free(fstr);
	    }
	}
	else {
	    SensorCall();str = NULL;
	}
	SensorCall();break;
    }
    SensorCall();if (!str)
	{/*289*/SensorCall();str = str_new(0);/*290*/}

    SensorCall();if (useval && prec < minprec) {		/* need parens? */
	SensorCall();fstr = str_new(str->str_cur+2);
	str_nset(fstr,"(",1);
	str_scat(fstr,str);
	str_ncat(fstr,")",1);
	str_free(str);
	str = fstr;
    }

    SensorCall();*numericptr = numeric;
#ifdef DEBUGGING
    if (debug & 4) {
	printf("%3d %5d %15s %d %4d ",level,node,opname[type],len,str->str_cur);
	for (t = str->str_ptr; *t && t - str->str_ptr < 40; t++)
	    if (*t == '\n')
		printf("\\n");
	    else if (*t == '\t')
		printf("\\t");
	    else
		putchar(*t);
	putchar('\n');
    }
#endif
    {STR * ReplaceReturn = str; SensorCall(); return ReplaceReturn;}
}

static void
tab(register STR *str, register int lvl)
{
    SensorCall();while (lvl > 1) {
	SensorCall();str_cat(str,"\t");
	lvl -= 2;
    }
    SensorCall();if (lvl)
	{/*25*/SensorCall();str_cat(str,"    ");/*26*/}
SensorCall();}

static void
fixtab(register STR *str, register int lvl)
{
    SensorCall();register char *s;

    /* strip trailing white space */

    s = str->str_ptr+str->str_cur - 1;
    SensorCall();while (s >= str->str_ptr && (*s == ' ' || *s == '\t' || *s == '\n'))
	{/*21*/SensorCall();s--;/*22*/}
    SensorCall();s[1] = '\0';
    str->str_cur = s + 1 - str->str_ptr;
    SensorCall();if (s >= str->str_ptr && *s != '\n')
	{/*23*/SensorCall();str_cat(str,"\n");/*24*/}

    SensorCall();tab(str,lvl);
SensorCall();}

static void
addsemi(register STR *str)
{
    SensorCall();register char *s;

    s = str->str_ptr+str->str_cur - 1;
    SensorCall();while (s >= str->str_ptr && (*s == ' ' || *s == '\t' || *s == '\n'))
	{/*1*/SensorCall();s--;/*2*/}
    SensorCall();if (s >= str->str_ptr && *s != ';' && *s != '}')
	{/*3*/SensorCall();str_cat(str,";");/*4*/}
SensorCall();}

static void
emit_split(register STR *str, int level)
{
    SensorCall();register int i;

    SensorCall();if (split_to_array)
	{/*5*/SensorCall();str_cat(str,"@Fld");/*6*/}
    else {
	SensorCall();str_cat(str,"(");
	SensorCall();for (i = 1; i < maxfld; i++) {
	    SensorCall();if (i <= arymax)
		{/*7*/SensorCall();sprintf(tokenbuf,"$%s,",nameary[i]);/*8*/}
	    else
		{/*9*/SensorCall();sprintf(tokenbuf,"$Fld%d,",i);/*10*/}
	    SensorCall();str_cat(str,tokenbuf);
	}
	SensorCall();if (maxfld <= arymax)
	    {/*11*/SensorCall();sprintf(tokenbuf,"$%s)",nameary[maxfld]);/*12*/}
	else
	    {/*13*/SensorCall();sprintf(tokenbuf,"$Fld%d)",maxfld);/*14*/}
	SensorCall();str_cat(str,tokenbuf);
    }
    SensorCall();if (const_FS) {
	SensorCall();sprintf(tokenbuf," = split(/[%c\\n]/, $_, -1);\n",const_FS);
	str_cat(str,tokenbuf);
    }
    else {/*15*/SensorCall();if (saw_FS)
	{/*17*/SensorCall();str_cat(str," = split($FS, $_, -1);\n");/*18*/}
    else
	{/*19*/SensorCall();str_cat(str," = split(' ', $_, -1);\n");/*20*/}/*16*/}
    SensorCall();tab(str,level);
SensorCall();}

int
prewalk(int numit, int level, register int node, int *numericptr)
{
    SensorCall();register int len;
    register int type;
    register int i;
    int numarg;
    int numeric = FALSE;
    STR *tmpstr;
    STR *tmp2str;

    SensorCall();if (!node) {
	SensorCall();*numericptr = 0;
	{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
    }
    SensorCall();type = ops[node].ival;
    len = type >> 8;
    type &= 255;
    SensorCall();switch (type) {
    case OPROG:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	SensorCall();if (ops[node+2].ival) {
	    SensorCall();prewalk(0,level,ops[node+2].ival,&numarg);
	}
	SensorCall();++level;
	prewalk(0,level,ops[node+3].ival,&numarg);
	--level;
	SensorCall();if (ops[node+3].ival) {
	    SensorCall();prewalk(0,level,ops[node+4].ival,&numarg);
	}
	SensorCall();break;
    case OHUNKS:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	prewalk(0,level,ops[node+2].ival,&numarg);
	SensorCall();if (len == 3) {
	    SensorCall();prewalk(0,level,ops[node+3].ival,&numarg);
	}
	SensorCall();break;
    case ORANGE:
	SensorCall();prewalk(1,level,ops[node+1].ival,&numarg);
	prewalk(1,level,ops[node+2].ival,&numarg);
	SensorCall();break;
    case OPAT:
	SensorCall();goto def;
    case OREGEX:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	SensorCall();break;
    case OHUNK:
	SensorCall();if (len == 1) {
	    SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	}
	else {
	    SensorCall();i = prewalk(0,level,ops[node+1].ival,&numarg);
	    SensorCall();if (i) {
		SensorCall();++level;
		prewalk(0,level,ops[node+2].ival,&numarg);
		--level;
	    }
	    else {
		SensorCall();prewalk(0,level,ops[node+2].ival,&numarg);
	    }
	}
	SensorCall();break;
    case OPPAREN:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	SensorCall();break;
    case OPANDAND:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	prewalk(0,level,ops[node+2].ival,&numarg);
	SensorCall();break;
    case OPOROR:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	prewalk(0,level,ops[node+2].ival,&numarg);
	SensorCall();break;
    case OPNOT:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	SensorCall();break;
    case OCPAREN:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	numeric |= numarg;
	SensorCall();break;
    case OCANDAND:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	numeric = 1;
	prewalk(0,level,ops[node+2].ival,&numarg);
	SensorCall();break;
    case OCOROR:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	numeric = 1;
	prewalk(0,level,ops[node+2].ival,&numarg);
	SensorCall();break;
    case OCNOT:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	numeric = 1;
	SensorCall();break;
    case ORELOP:
	SensorCall();prewalk(0,level,ops[node+2].ival,&numarg);
	numeric |= numarg;
	prewalk(0,level,ops[node+1].ival,&numarg);
	prewalk(0,level,ops[node+3].ival,&numarg);
	numeric |= numarg;
	numeric = 1;
	SensorCall();break;
    case ORPAREN:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	numeric |= numarg;
	SensorCall();break;
    case OMATCHOP:
	SensorCall();prewalk(0,level,ops[node+2].ival,&numarg);
	prewalk(0,level,ops[node+1].ival,&numarg);
	prewalk(0,level,ops[node+3].ival,&numarg);
	numeric = 1;
	SensorCall();break;
    case OMPAREN:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	numeric |= numarg;
	SensorCall();break;
    case OCONCAT:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	prewalk(0,level,ops[node+2].ival,&numarg);
	SensorCall();break;
    case OASSIGN:
	SensorCall();prewalk(0,level,ops[node+2].ival,&numarg);
	prewalk(0,level,ops[node+1].ival,&numarg);
	prewalk(0,level,ops[node+3].ival,&numarg);
	SensorCall();if (numarg || strlen(ops[ops[node+1].ival+1].cval) > (Size_t)1) {
	    SensorCall();numericize(ops[node+2].ival);
	    SensorCall();if (!numarg)
		{/*291*/SensorCall();numericize(ops[node+3].ival);/*292*/}
	}
	SensorCall();numeric |= numarg;
	SensorCall();break;
    case OADD:
	SensorCall();prewalk(1,level,ops[node+1].ival,&numarg);
	prewalk(1,level,ops[node+2].ival,&numarg);
	numeric = 1;
	SensorCall();break;
    case OSUBTRACT:
	SensorCall();prewalk(1,level,ops[node+1].ival,&numarg);
	prewalk(1,level,ops[node+2].ival,&numarg);
	numeric = 1;
	SensorCall();break;
    case OMULT:
	SensorCall();prewalk(1,level,ops[node+1].ival,&numarg);
	prewalk(1,level,ops[node+2].ival,&numarg);
	numeric = 1;
	SensorCall();break;
    case ODIV:
	SensorCall();prewalk(1,level,ops[node+1].ival,&numarg);
	prewalk(1,level,ops[node+2].ival,&numarg);
	numeric = 1;
	SensorCall();break;
    case OPOW:
	SensorCall();prewalk(1,level,ops[node+1].ival,&numarg);
	prewalk(1,level,ops[node+2].ival,&numarg);
	numeric = 1;
	SensorCall();break;
    case OMOD:
	SensorCall();prewalk(1,level,ops[node+1].ival,&numarg);
	prewalk(1,level,ops[node+2].ival,&numarg);
	numeric = 1;
	SensorCall();break;
    case OPOSTINCR:
	SensorCall();prewalk(1,level,ops[node+1].ival,&numarg);
	numeric = 1;
	SensorCall();break;
    case OPOSTDECR:
	SensorCall();prewalk(1,level,ops[node+1].ival,&numarg);
	numeric = 1;
	SensorCall();break;
    case OPREINCR:
	SensorCall();prewalk(1,level,ops[node+1].ival,&numarg);
	numeric = 1;
	SensorCall();break;
    case OPREDECR:
	SensorCall();prewalk(1,level,ops[node+1].ival,&numarg);
	numeric = 1;
	SensorCall();break;
    case OUMINUS:
	SensorCall();prewalk(1,level,ops[node+1].ival,&numarg);
	numeric = 1;
	SensorCall();break;
    case OUPLUS:
	SensorCall();prewalk(1,level,ops[node+1].ival,&numarg);
	numeric = 1;
	SensorCall();break;
    case OPAREN:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	numeric |= numarg;
	SensorCall();break;
    case OGETLINE:
	SensorCall();break;
    case OSPRINTF:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	SensorCall();break;
    case OSUBSTR:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	prewalk(1,level,ops[node+2].ival,&numarg);
	SensorCall();if (len == 3) {
	    SensorCall();prewalk(1,level,ops[node+3].ival,&numarg);
	}
	SensorCall();break;
    case OSTRING:
	SensorCall();break;
    case OSPLIT:
	SensorCall();numeric = 1;
	prewalk(0,level,ops[node+2].ival,&numarg);
	SensorCall();if (len == 3)
	    {/*293*/SensorCall();prewalk(0,level,ops[node+3].ival,&numarg);/*294*/}
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	SensorCall();break;
    case OINDEX:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	prewalk(0,level,ops[node+2].ival,&numarg);
	numeric = 1;
	SensorCall();break;
    case OMATCH:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	prewalk(0,level,ops[node+2].ival,&numarg);
	numeric = 1;
	SensorCall();break;
    case OUSERDEF:
	SensorCall();subretnum = FALSE;
	--level;
	tmpstr = walk(0,level,ops[node+1].ival,&numarg,P_MIN);
	++level;
	prewalk(0,level,ops[node+2].ival,&numarg);
	prewalk(0,level,ops[node+4].ival,&numarg);
	prewalk(0,level,ops[node+5].ival,&numarg);
	--level;
	str_cat(tmpstr,"(");
	tmp2str = str_new(0);
	SensorCall();if (subretnum || numarg)
	    {/*295*/SensorCall();str_set(tmp2str,"1");/*296*/}
	SensorCall();hstore(symtab,tmpstr->str_ptr,tmp2str);
	str_free(tmpstr);
	level++;
	SensorCall();break;
    case ORETURN:
	SensorCall();if (len > 0) {
	    SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	    SensorCall();if (numarg)
		subretnum = TRUE;
	}
	SensorCall();break;
    case OUSERFUN:
	SensorCall();tmp2str = str_new(0);
	str_scat(tmp2str,tmpstr=walk(1,level,ops[node+1].ival,&numarg,P_MIN));
	fixrargs(tmpstr->str_ptr,ops[node+2].ival,0);
	str_free(tmpstr);
	str_cat(tmp2str,"(");
	tmpstr = hfetch(symtab,tmp2str->str_ptr);
	SensorCall();if (tmpstr && tmpstr->str_ptr)
	    {/*297*/SensorCall();numeric |= atoi(tmpstr->str_ptr);/*298*/}
	SensorCall();prewalk(0,level,ops[node+2].ival,&numarg);
	str_free(tmp2str);
	SensorCall();break;
    case OGSUB:
    case OSUB:
	SensorCall();if (len >= 3)
	    {/*299*/SensorCall();prewalk(0,level,ops[node+3].ival,&numarg);/*300*/}
	SensorCall();prewalk(0,level,ops[ops[node+2].ival+1].ival,&numarg);
	prewalk(0,level,ops[node+1].ival,&numarg);
	numeric = 1;
	SensorCall();break;
    case ONUM:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	numeric = 1;
	SensorCall();break;
    case OSTR:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	SensorCall();break;
    case ODEFINED:
    case ODELETE:
    case OSTAR:
    case OVAR:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	SensorCall();if (len == 1) {
	    SensorCall();if (numit)
		{/*301*/SensorCall();numericize(node);/*302*/}
	}
	else {
	    SensorCall();prewalk(0,level,ops[node+2].ival,&numarg);
	}
	SensorCall();break;
    case OFLD:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	SensorCall();break;
    case OVFLD:
	SensorCall();i = ops[node+1].ival;
	prewalk(0,level,i,&numarg);
	SensorCall();break;
    case OJUNK:
	SensorCall();goto def;
    case OSNEWLINE:
	SensorCall();break;
    case ONEWLINE:
	SensorCall();break;
    case OSCOMMENT:
	SensorCall();break;
    case OCOMMENT:
	SensorCall();break;
    case OCOMMA:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	prewalk(0,level,ops[node+2].ival,&numarg);
	prewalk(0,level,ops[node+3].ival,&numarg);
	SensorCall();break;
    case OSEMICOLON:
	SensorCall();break;
    case OSTATES:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	prewalk(0,level,ops[node+2].ival,&numarg);
	SensorCall();break;
    case OSTATE:
	SensorCall();if (len >= 1) {
	    SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	    SensorCall();if (len >= 2) {
		SensorCall();prewalk(0,level,ops[node+2].ival,&numarg);
	    }
	}
	SensorCall();break;
    case OCLOSE:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	SensorCall();break;
    case OPRINTF:
    case OPRINT:
	SensorCall();if (len == 3) {		/* output redirection */
	    SensorCall();prewalk(0,level,ops[node+3].ival,&numarg);
	    prewalk(0,level,ops[node+2].ival,&numarg);
	}
	SensorCall();prewalk(0+(type==OPRINT),level,ops[node+1].ival,&numarg);
	SensorCall();break;
    case ORAND:
	SensorCall();break;
    case OSRAND:
	SensorCall();goto maybe0;
    case OATAN2:
	SensorCall();goto maybe0;
    case OSIN:
	SensorCall();goto maybe0;
    case OCOS:
	SensorCall();goto maybe0;
    case OSYSTEM:
	SensorCall();goto maybe0;
    case OLENGTH:
	SensorCall();goto maybe0;
    case OLOG:
	SensorCall();goto maybe0;
    case OEXP:
	SensorCall();goto maybe0;
    case OSQRT:
	SensorCall();goto maybe0;
    case OINT:
      maybe0:
	SensorCall();numeric = 1;
	SensorCall();if (len > 0)
	    {/*303*/SensorCall();prewalk(type != OLENGTH && type != OSYSTEM,
	      level,ops[node+1].ival,&numarg);/*304*/}
	SensorCall();break;
    case OBREAK:
	SensorCall();break;
    case ONEXT:
	SensorCall();break;
    case OEXIT:
	SensorCall();if (len == 1) {
	    SensorCall();prewalk(1,level,ops[node+1].ival,&numarg);
	}
	SensorCall();break;
    case OCONTINUE:
	SensorCall();break;
    case OREDIR:
	SensorCall();goto def;
    case OIF:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	prewalk(0,level,ops[node+2].ival,&numarg);
	SensorCall();if (len == 3) {
	    SensorCall();prewalk(0,level,ops[node+3].ival,&numarg);
	}
	SensorCall();break;
    case OWHILE:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	prewalk(0,level,ops[node+2].ival,&numarg);
	SensorCall();break;
    case OFOR:
	SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	prewalk(0,level,ops[node+2].ival,&numarg);
	prewalk(0,level,ops[node+3].ival,&numarg);
	prewalk(0,level,ops[node+4].ival,&numarg);
	SensorCall();break;
    case OFORIN:
	SensorCall();prewalk(0,level,ops[node+2].ival,&numarg);
	prewalk(0,level,ops[node+1].ival,&numarg);
	SensorCall();break;
    case OBLOCK:
	SensorCall();if (len == 2) {
	    SensorCall();prewalk(0,level,ops[node+2].ival,&numarg);
	}
	SensorCall();++level;
	prewalk(0,level,ops[node+1].ival,&numarg);
	--level;
	SensorCall();break;
    default:
      def:
	SensorCall();if (len) {
	    SensorCall();if (len > 5)
		fatal("Garbage length in prewalk");
	    SensorCall();prewalk(0,level,ops[node+1].ival,&numarg);
	    SensorCall();for (i = 2; i<= len; i++) {
		SensorCall();prewalk(0,level,ops[node+i].ival,&numarg);
	    }
	}
	SensorCall();break;
    }
    SensorCall();*numericptr = numeric;
    {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

static void
numericize(register int node)
{
    SensorCall();register int len;
    register int type;
    STR *tmpstr;
    STR *tmp2str;
    int numarg;

    type = ops[node].ival;
    len = type >> 8;
    type &= 255;
    SensorCall();if (type == OVAR && len == 1) {
	SensorCall();tmpstr=walk(0,0,ops[node+1].ival,&numarg,P_MIN);
	tmp2str = str_make("1");
	hstore(symtab,tmpstr->str_ptr,tmp2str);
    }
SensorCall();}
