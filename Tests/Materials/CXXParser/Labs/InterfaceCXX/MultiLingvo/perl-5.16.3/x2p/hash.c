#include "var/tmp/sensor.h"
/*    hash.c
 *
 *    Copyright (C) 1991, 1992, 1993, 1994, 1995, 1999, 2000, 2001, 2002,
 *    2005 by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 */

#include <stdio.h>
#include "EXTERN.h"
#include "a2p.h"
#include "util.h"

#ifdef NETWARE
char *savestr(char *str);
#endif

STR *
hfetch(register HASH *tb, char *key)
{
    SensorCall();register char *s;
    register int i;
    register int hash;
    register HENT *entry;

    SensorCall();if (!tb)
	return NULL;
    SensorCall();for (s=key,		i=0,	hash = 0;
      /* while */ *s;
	 s++,		i++,	hash *= 5) {
	SensorCall();hash += *s * coeff[i];
    }
    SensorCall();entry = tb->tbl_array[hash & tb->tbl_max];
    SensorCall();for (; entry; entry = entry->hent_next) {
	SensorCall();if (entry->hent_hash != hash)		/* strings can't be equal */
	    {/*1*/SensorCall();continue;/*2*/}
	SensorCall();if (strNE(entry->hent_key,key))	/* is this it? */
	    {/*3*/SensorCall();continue;/*4*/}
	{STR * ReplaceReturn = entry->hent_val; SensorCall(); return ReplaceReturn;}
    }
    {STR * ReplaceReturn = NULL; SensorCall(); return ReplaceReturn;}
}

bool
hstore(register HASH *tb, char *key, STR *val)
{
    SensorCall();register char *s;
    register int i;
    register int hash;
    register HENT *entry;
    register HENT **oentry;

    SensorCall();if (!tb)
	return FALSE;
    SensorCall();for (s=key,		i=0,	hash = 0;
      /* while */ *s;
	 s++,		i++,	hash *= 5) {
	SensorCall();hash += *s * coeff[i];
    }

    SensorCall();oentry = &(tb->tbl_array[hash & tb->tbl_max]);
    i = 1;

    SensorCall();for (entry = *oentry; entry; i=0, entry = entry->hent_next) {
	SensorCall();if (entry->hent_hash != hash)		/* strings can't be equal */
	    {/*13*/SensorCall();continue;/*14*/}
	SensorCall();if (strNE(entry->hent_key,key))	/* is this it? */
	    {/*15*/SensorCall();continue;/*16*/}
	/*NOSTRICT*/
	SensorCall();safefree(entry->hent_val);
	entry->hent_val = val;
	{_Bool  ReplaceReturn = TRUE; SensorCall(); return ReplaceReturn;}
    }
    /*NOSTRICT*/
    SensorCall();entry = (HENT*) safemalloc(sizeof(HENT));

    entry->hent_key = savestr(key);
    entry->hent_val = val;
    entry->hent_hash = hash;
    entry->hent_next = *oentry;
    *oentry = entry;

    SensorCall();if (i) {				/* initial entry? */
	SensorCall();tb->tbl_fill++;
	SensorCall();if ((tb->tbl_fill * 100 / (tb->tbl_max + 1)) > FILLPCT)
	    {/*17*/SensorCall();hsplit(tb);/*18*/}
    }

    {_Bool  ReplaceReturn = FALSE; SensorCall(); return ReplaceReturn;}
}

void
hsplit(HASH *tb)
{
    SensorCall();const int oldsize = tb->tbl_max + 1;
    register int newsize = oldsize * 2;
    register int i;
    register HENT **a;
    register HENT **b;
    register HENT *entry;
    register HENT **oentry;

    a = (HENT**) saferealloc((char*)tb->tbl_array, newsize * sizeof(HENT*));
    memset(&a[oldsize], 0, oldsize * sizeof(HENT*)); /* zero second half */
    tb->tbl_max = --newsize;
    tb->tbl_array = a;

    SensorCall();for (i=0; i<oldsize; i++,a++) {
	SensorCall();if (!*a)				/* non-existent */
	    {/*5*/SensorCall();continue;/*6*/}
	SensorCall();b = a+oldsize;
	SensorCall();for (oentry = a, entry = *a; entry; entry = *oentry) {
	    SensorCall();if ((entry->hent_hash & newsize) != i) {
		SensorCall();*oentry = entry->hent_next;
		entry->hent_next = *b;
		SensorCall();if (!*b)
		    {/*7*/SensorCall();tb->tbl_fill++;/*8*/}
		SensorCall();*b = entry;
		SensorCall();continue;
	    }
	    else
		{/*9*/SensorCall();oentry = &entry->hent_next;/*10*/}
	}
	SensorCall();if (!*a)				/* everything moved */
	    {/*11*/SensorCall();tb->tbl_fill--;/*12*/}
    }
SensorCall();}

HASH *
hnew(void)
{
    SensorCall();register HASH *tb = (HASH*)safemalloc(sizeof(HASH));

    tb->tbl_array = (HENT**) safemalloc(8 * sizeof(HENT*));
    tb->tbl_fill = 0;
    tb->tbl_max = 7;
    hiterinit(tb);	/* so each() will start off right */
    memset(tb->tbl_array, 0, 8 * sizeof(HENT*));
    {HASH * ReplaceReturn = tb; SensorCall(); return ReplaceReturn;}
}

int
hiterinit(register HASH *tb)
{
    SensorCall();tb->tbl_riter = -1;
    tb->tbl_eiter = (HENT*)NULL;
    {int  ReplaceReturn = tb->tbl_fill; SensorCall(); return ReplaceReturn;}
}
