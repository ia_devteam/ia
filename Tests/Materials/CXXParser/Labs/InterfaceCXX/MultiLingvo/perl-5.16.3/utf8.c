#include "var/tmp/sensor.h"
/*    utf8.c
 *
 *    Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008
 *    by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 *
 */

/*
 * 'What a fix!' said Sam.  'That's the one place in all the lands we've ever
 *  heard of that we don't want to see any closer; and that's the one place
 *  we're trying to get to!  And that's just where we can't get, nohow.'
 *
 *     [p.603 of _The Lord of the Rings_, IV/I: "The Taming of Sméagol"]
 *
 * 'Well do I understand your speech,' he answered in the same language;
 * 'yet few strangers do so.  Why then do you not speak in the Common Tongue,
 *  as is the custom in the West, if you wish to be answered?'
 *                           --Gandalf, addressing Théoden's door wardens
 *
 *     [p.508 of _The Lord of the Rings_, III/vi: "The King of the Golden Hall"]
 *
 * ...the travellers perceived that the floor was paved with stones of many
 * hues; branching runes and strange devices intertwined beneath their feet.
 *
 *     [p.512 of _The Lord of the Rings_, III/vi: "The King of the Golden Hall"]
 */

#include "EXTERN.h"
#define PERL_IN_UTF8_C
#include "perl.h"

#ifndef EBCDIC
/* Separate prototypes needed because in ASCII systems these are
 * usually macros but they still are compiled as code, too. */
PERL_CALLCONV UV	Perl_utf8n_to_uvchr(pTHX_ const U8 *s, STRLEN curlen, STRLEN *retlen, U32 flags);
PERL_CALLCONV U8*	Perl_uvchr_to_utf8(pTHX_ U8 *d, UV uv);
#endif

static const char unees[] =
    "Malformed UTF-8 character (unexpected end of string)";

/*
=head1 Unicode Support

This file contains various utility functions for manipulating UTF8-encoded
strings. For the uninitiated, this is a method of representing arbitrary
Unicode characters as a variable number of bytes, in such a way that
characters in the ASCII range are unmodified, and a zero byte never appears
within non-zero characters.

=cut
*/

/*
=for apidoc is_ascii_string

Returns true if the first C<len> bytes of the string C<s> are the same whether
or not the string is encoded in UTF-8 (or UTF-EBCDIC on EBCDIC machines).  That
is, if they are invariant.  On ASCII-ish machines, only ASCII characters
fit this definition, hence the function's name.

If C<len> is 0, it will be calculated using C<strlen(s)>.  

See also L</is_utf8_string>(), L</is_utf8_string_loclen>(), and L</is_utf8_string_loc>().

=cut
*/

bool
Perl_is_ascii_string(const U8 *s, STRLEN len)
{
    SensorCall(27859);const U8* const send = s + (len ? len : strlen((const char *)s));
    const U8* x = s;

    PERL_ARGS_ASSERT_IS_ASCII_STRING;

    SensorCall(27862);for (; x < send; ++x) {
	SensorCall(27860);if (!UTF8_IS_INVARIANT(*x))
	    {/*33*/SensorCall(27861);break;/*34*/}
    }

    {_Bool  ReplaceReturn1269 = x == send; SensorCall(27863); return ReplaceReturn1269;}
}

/*
=for apidoc uvuni_to_utf8_flags

Adds the UTF-8 representation of the code point C<uv> to the end
of the string C<d>; C<d> should have at least C<UTF8_MAXBYTES+1> free
bytes available. The return value is the pointer to the byte after the
end of the new character. In other words,

    d = uvuni_to_utf8_flags(d, uv, flags);

or, in most cases,

    d = uvuni_to_utf8(d, uv);

(which is equivalent to)

    d = uvuni_to_utf8_flags(d, uv, 0);

This is the recommended Unicode-aware way of saying

    *(d++) = uv;

This function will convert to UTF-8 (and not warn) even code points that aren't
legal Unicode or are problematic, unless C<flags> contains one or more of the
following flags:

If C<uv> is a Unicode surrogate code point and UNICODE_WARN_SURROGATE is set,
the function will raise a warning, provided UTF8 warnings are enabled.  If instead
UNICODE_DISALLOW_SURROGATE is set, the function will fail and return NULL.
If both flags are set, the function will both warn and return NULL.

The UNICODE_WARN_NONCHAR and UNICODE_DISALLOW_NONCHAR flags correspondingly
affect how the function handles a Unicode non-character.  And, likewise for the
UNICODE_WARN_SUPER and UNICODE_DISALLOW_SUPER flags, and code points that are
above the Unicode maximum of 0x10FFFF.  Code points above 0x7FFF_FFFF (which are
even less portable) can be warned and/or disallowed even if other above-Unicode
code points are accepted by the UNICODE_WARN_FE_FF and UNICODE_DISALLOW_FE_FF
flags.

And finally, the flag UNICODE_WARN_ILLEGAL_INTERCHANGE selects all four of the
above WARN flags; and UNICODE_DISALLOW_ILLEGAL_INTERCHANGE selects all four
DISALLOW flags.


=cut
*/

U8 *
Perl_uvuni_to_utf8_flags(pTHX_ U8 *d, UV uv, UV flags)
{
    PERL_ARGS_ASSERT_UVUNI_TO_UTF8_FLAGS;

    SensorCall(27864);if (ckWARN4_d(WARN_UTF8, WARN_SURROGATE, WARN_NON_UNICODE, WARN_NONCHAR)) {
	SensorCall(27865);if (UNICODE_IS_SURROGATE(uv)) {
	    SensorCall(27866);if (flags & UNICODE_WARN_SURROGATE) {
		SensorCall(27867);Perl_ck_warner_d(aTHX_ packWARN(WARN_SURROGATE),
					    "UTF-16 surrogate U+%04"UVXf, uv);
	    }
	    SensorCall(27869);if (flags & UNICODE_DISALLOW_SURROGATE) {
		{U8 * ReplaceReturn1268 = NULL; SensorCall(27868); return ReplaceReturn1268;}
	    }
	}
	else {/*99*/SensorCall(27870);if (UNICODE_IS_SUPER(uv)) {
	    SensorCall(27871);if (flags & UNICODE_WARN_SUPER
		|| (UNICODE_IS_FE_FF(uv) && (flags & UNICODE_WARN_FE_FF)))
	    {
		SensorCall(27872);Perl_ck_warner_d(aTHX_ packWARN(WARN_NON_UNICODE),
			  "Code point 0x%04"UVXf" is not Unicode, may not be portable", uv);
	    }
	    SensorCall(27874);if (flags & UNICODE_DISALLOW_SUPER
		|| (UNICODE_IS_FE_FF(uv) && (flags & UNICODE_DISALLOW_FE_FF)))
	    {
		{U8 * ReplaceReturn1267 = NULL; SensorCall(27873); return ReplaceReturn1267;}
	    }
	}
	else {/*101*/SensorCall(27875);if (UNICODE_IS_NONCHAR(uv)) {
	    SensorCall(27876);if (flags & UNICODE_WARN_NONCHAR) {
		SensorCall(27877);Perl_ck_warner_d(aTHX_ packWARN(WARN_NONCHAR),
		 "Unicode non-character U+%04"UVXf" is illegal for open interchange",
		 uv);
	    }
	    SensorCall(27879);if (flags & UNICODE_DISALLOW_NONCHAR) {
		{U8 * ReplaceReturn1266 = NULL; SensorCall(27878); return ReplaceReturn1266;}
	    }
	;/*102*/}/*100*/}}
    }
    SensorCall(27882);if (UNI_IS_INVARIANT(uv)) {
	SensorCall(27880);*d++ = (U8)UTF_TO_NATIVE(uv);
	{U8 * ReplaceReturn1265 = d; SensorCall(27881); return ReplaceReturn1265;}
    }
#if defined(EBCDIC)
    else {
	STRLEN len  = UNISKIP(uv);
	U8 *p = d+len-1;
	while (p > d) {
	    *p-- = (U8)UTF_TO_NATIVE((uv & UTF_CONTINUATION_MASK) | UTF_CONTINUATION_MARK);
	    uv >>= UTF_ACCUMULATION_SHIFT;
	}
	*p = (U8)UTF_TO_NATIVE((uv & UTF_START_MASK(len)) | UTF_START_MARK(len));
	return d+len;
    }
#else /* Non loop style */
    SensorCall(27885);if (uv < 0x800) {
	SensorCall(27883);*d++ = (U8)(( uv >>  6)         | 0xc0);
	*d++ = (U8)(( uv        & 0x3f) | 0x80);
	{U8 * ReplaceReturn1264 = d; SensorCall(27884); return ReplaceReturn1264;}
    }
    SensorCall(27888);if (uv < 0x10000) {
	SensorCall(27886);*d++ = (U8)(( uv >> 12)         | 0xe0);
	*d++ = (U8)(((uv >>  6) & 0x3f) | 0x80);
	*d++ = (U8)(( uv        & 0x3f) | 0x80);
	{U8 * ReplaceReturn1263 = d; SensorCall(27887); return ReplaceReturn1263;}
    }
    SensorCall(27891);if (uv < 0x200000) {
	SensorCall(27889);*d++ = (U8)(( uv >> 18)         | 0xf0);
	*d++ = (U8)(((uv >> 12) & 0x3f) | 0x80);
	*d++ = (U8)(((uv >>  6) & 0x3f) | 0x80);
	*d++ = (U8)(( uv        & 0x3f) | 0x80);
	{U8 * ReplaceReturn1262 = d; SensorCall(27890); return ReplaceReturn1262;}
    }
    SensorCall(27894);if (uv < 0x4000000) {
	SensorCall(27892);*d++ = (U8)(( uv >> 24)         | 0xf8);
	*d++ = (U8)(((uv >> 18) & 0x3f) | 0x80);
	*d++ = (U8)(((uv >> 12) & 0x3f) | 0x80);
	*d++ = (U8)(((uv >>  6) & 0x3f) | 0x80);
	*d++ = (U8)(( uv        & 0x3f) | 0x80);
	{U8 * ReplaceReturn1261 = d; SensorCall(27893); return ReplaceReturn1261;}
    }
    SensorCall(27897);if (uv < 0x80000000) {
	SensorCall(27895);*d++ = (U8)(( uv >> 30)         | 0xfc);
	*d++ = (U8)(((uv >> 24) & 0x3f) | 0x80);
	*d++ = (U8)(((uv >> 18) & 0x3f) | 0x80);
	*d++ = (U8)(((uv >> 12) & 0x3f) | 0x80);
	*d++ = (U8)(((uv >>  6) & 0x3f) | 0x80);
	*d++ = (U8)(( uv        & 0x3f) | 0x80);
	{U8 * ReplaceReturn1260 = d; SensorCall(27896); return ReplaceReturn1260;}
    }
#ifdef HAS_QUAD
    SensorCall(27900);if (uv < UTF8_QUAD_MAX)
#endif
    {
	SensorCall(27898);*d++ =                            0xfe;	/* Can't match U+FEFF! */
	*d++ = (U8)(((uv >> 30) & 0x3f) | 0x80);
	*d++ = (U8)(((uv >> 24) & 0x3f) | 0x80);
	*d++ = (U8)(((uv >> 18) & 0x3f) | 0x80);
	*d++ = (U8)(((uv >> 12) & 0x3f) | 0x80);
	*d++ = (U8)(((uv >>  6) & 0x3f) | 0x80);
	*d++ = (U8)(( uv        & 0x3f) | 0x80);
	{U8 * ReplaceReturn1259 = d; SensorCall(27899); return ReplaceReturn1259;}
    }
#ifdef HAS_QUAD
    {
	SensorCall(27901);*d++ =                            0xff;		/* Can't match U+FFFE! */
	*d++ =                            0x80;		/* 6 Reserved bits */
	*d++ = (U8)(((uv >> 60) & 0x0f) | 0x80);	/* 2 Reserved bits */
	*d++ = (U8)(((uv >> 54) & 0x3f) | 0x80);
	*d++ = (U8)(((uv >> 48) & 0x3f) | 0x80);
	*d++ = (U8)(((uv >> 42) & 0x3f) | 0x80);
	*d++ = (U8)(((uv >> 36) & 0x3f) | 0x80);
	*d++ = (U8)(((uv >> 30) & 0x3f) | 0x80);
	*d++ = (U8)(((uv >> 24) & 0x3f) | 0x80);
	*d++ = (U8)(((uv >> 18) & 0x3f) | 0x80);
	*d++ = (U8)(((uv >> 12) & 0x3f) | 0x80);
	*d++ = (U8)(((uv >>  6) & 0x3f) | 0x80);
	*d++ = (U8)(( uv        & 0x3f) | 0x80);
	{U8 * ReplaceReturn1258 = d; SensorCall(27902); return ReplaceReturn1258;}
    }
#endif
#endif /* Loop style */
}

/*

Tests if the first C<len> bytes of string C<s> form a valid UTF-8
character.  Note that an INVARIANT (i.e. ASCII) character is a valid
UTF-8 character.  The number of bytes in the UTF-8 character
will be returned if it is valid, otherwise 0.

This is the "slow" version as opposed to the "fast" version which is
the "unrolled" IS_UTF8_CHAR().  E.g. for t/uni/class.t the speed
difference is a factor of 2 to 3.  For lengths (UTF8SKIP(s)) of four
or less you should use the IS_UTF8_CHAR(), for lengths of five or more
you should use the _slow().  In practice this means that the _slow()
will be used very rarely, since the maximum Unicode code point (as of
Unicode 4.1) is U+10FFFF, which encodes in UTF-8 to four bytes.  Only
the "Perl extended UTF-8" (the infamous 'v-strings') will encode into
five bytes or more.

=cut */
STATIC STRLEN
S_is_utf8_char_slow(const U8 *s, const STRLEN len)
{
    dTHX;   /* The function called below requires thread context */

    STRLEN actual_len;

    PERL_ARGS_ASSERT_IS_UTF8_CHAR_SLOW;

    utf8n_to_uvuni(s, len, &actual_len, UTF8_CHECK_ONLY);

    {STRLEN  ReplaceReturn1257 = (actual_len == (STRLEN) -1) ? 0 : actual_len; SensorCall(27903); return ReplaceReturn1257;}
}

/*
=for apidoc is_utf8_char_buf

Returns the number of bytes that comprise the first UTF-8 encoded character in
buffer C<buf>.  C<buf_end> should point to one position beyond the end of the
buffer.  0 is returned if C<buf> does not point to a complete, valid UTF-8
encoded character.

Note that an INVARIANT character (i.e. ASCII on non-EBCDIC
machines) is a valid UTF-8 character.

=cut */

STRLEN
Perl_is_utf8_char_buf(const U8 *buf, const U8* buf_end)
{

    SensorCall(27904);STRLEN len;

    PERL_ARGS_ASSERT_IS_UTF8_CHAR_BUF;

    SensorCall(27906);if (buf_end <= buf) {
	{STRLEN  ReplaceReturn1256 = 0; SensorCall(27905); return ReplaceReturn1256;}
    }

    SensorCall(27907);len = buf_end - buf;
    SensorCall(27909);if (len > UTF8SKIP(buf)) {
	SensorCall(27908);len = UTF8SKIP(buf);
    }

#ifdef IS_UTF8_CHAR
    SensorCall(27911);if (IS_UTF8_CHAR_FAST(len))
        {/*35*/{STRLEN  ReplaceReturn1255 = IS_UTF8_CHAR(buf, len) ? len : 0; SensorCall(27910); return ReplaceReturn1255;}/*36*/}
#endif /* #ifdef IS_UTF8_CHAR */
    {STRLEN  ReplaceReturn1254 = is_utf8_char_slow(buf, len); SensorCall(27912); return ReplaceReturn1254;}
}

/*
=for apidoc is_utf8_char

DEPRECATED!

Tests if some arbitrary number of bytes begins in a valid UTF-8
character.  Note that an INVARIANT (i.e. ASCII on non-EBCDIC machines)
character is a valid UTF-8 character.  The actual number of bytes in the UTF-8
character will be returned if it is valid, otherwise 0.

This function is deprecated due to the possibility that malformed input could
cause reading beyond the end of the input buffer.  Use L</is_utf8_char_buf>
instead.

=cut */

STRLEN
Perl_is_utf8_char(const U8 *s)
{
    PERL_ARGS_ASSERT_IS_UTF8_CHAR;

    /* Assumes we have enough space, which is why this is deprecated */
    {STRLEN  ReplaceReturn1253 = is_utf8_char_buf(s, s + UTF8SKIP(s)); SensorCall(27913); return ReplaceReturn1253;}
}


/*
=for apidoc is_utf8_string

Returns true if the first C<len> bytes of string C<s> form a valid
UTF-8 string, false otherwise.  If C<len> is 0, it will be calculated
using C<strlen(s)> (which means if you use this option, that C<s> has to have a
terminating NUL byte).  Note that all characters being ASCII constitute 'a
valid UTF-8 string'.

See also L</is_ascii_string>(), L</is_utf8_string_loclen>(), and L</is_utf8_string_loc>().

=cut
*/

bool
Perl_is_utf8_string(const U8 *s, STRLEN len)
{
    SensorCall(27914);const U8* const send = s + (len ? len : strlen((const char *)s));
    const U8* x = s;

    PERL_ARGS_ASSERT_IS_UTF8_STRING;

    SensorCall(27926);while (x < send) {
	 /* Inline the easy bits of is_utf8_char() here for speed... */
	 SensorCall(27915);if (UTF8_IS_INVARIANT(*x)) {
	    SensorCall(27916);x++;
	 }
	 else {/*37*/SensorCall(27917);if (!UTF8_IS_START(*x))
	     return FALSE;
	 else {
	      /* ... and call is_utf8_char() only if really needed. */
	     SensorCall(27918);const STRLEN c = UTF8SKIP(x);
	     const U8* const next_char_ptr = x + c;

	     SensorCall(27920);if (next_char_ptr > send) {
		 {_Bool  ReplaceReturn1252 = FALSE; SensorCall(27919); return ReplaceReturn1252;}
	     }

	     SensorCall(27924);if (IS_UTF8_CHAR_FAST(c)) {
	         SensorCall(27921);if (!IS_UTF8_CHAR(x, c))
		     return FALSE;
	     }
	     else {/*39*/SensorCall(27922);if (! is_utf8_char_slow(x, c)) {
		 {_Bool  ReplaceReturn1251 = FALSE; SensorCall(27923); return ReplaceReturn1251;}
	     ;/*40*/}}
	     SensorCall(27925);x = next_char_ptr;
	 ;/*38*/}}
    }

    {_Bool  ReplaceReturn1250 = TRUE; SensorCall(27927); return ReplaceReturn1250;}
}

/*
Implemented as a macro in utf8.h

=for apidoc is_utf8_string_loc

Like L</is_utf8_string> but stores the location of the failure (in the
case of "utf8ness failure") or the location C<s>+C<len> (in the case of
"utf8ness success") in the C<ep>.

See also L</is_utf8_string_loclen>() and L</is_utf8_string>().

=for apidoc is_utf8_string_loclen

Like L</is_utf8_string>() but stores the location of the failure (in the
case of "utf8ness failure") or the location C<s>+C<len> (in the case of
"utf8ness success") in the C<ep>, and the number of UTF-8
encoded characters in the C<el>.

See also L</is_utf8_string_loc>() and L</is_utf8_string>().

=cut
*/

bool
Perl_is_utf8_string_loclen(const U8 *s, STRLEN len, const U8 **ep, STRLEN *el)
{
    SensorCall(27928);const U8* const send = s + (len ? len : strlen((const char *)s));
    const U8* x = s;
    STRLEN c;
    STRLEN outlen = 0;

    PERL_ARGS_ASSERT_IS_UTF8_STRING_LOCLEN;

    SensorCall(27944);while (x < send) {
	 SensorCall(27929);const U8* next_char_ptr;

	 /* Inline the easy bits of is_utf8_char() here for speed... */
	 SensorCall(27942);if (UTF8_IS_INVARIANT(*x))
	     {/*41*/SensorCall(27930);next_char_ptr = x + 1;/*42*/}
	 else {/*43*/SensorCall(27931);if (!UTF8_IS_START(*x))
	     {/*45*/SensorCall(27932);goto out;/*46*/}
	 else {
	     /* ... and call is_utf8_char() only if really needed. */
	     SensorCall(27933);c = UTF8SKIP(x);
	     next_char_ptr = c + x;
	     SensorCall(27935);if (next_char_ptr > send) {
		 SensorCall(27934);goto out;
	     }
	     SensorCall(27939);if (IS_UTF8_CHAR_FAST(c)) {
	         SensorCall(27936);if (!IS_UTF8_CHAR(x, c))
		     {/*47*/SensorCall(27937);c = 0;/*48*/}
	     } else
	         {/*49*/SensorCall(27938);c = is_utf8_char_slow(x, c);/*50*/}
	     SensorCall(27941);if (!c)
	         {/*51*/SensorCall(27940);goto out;/*52*/}
	 ;/*44*/}}
         SensorCall(27943);x = next_char_ptr;
	 outlen++;
    }

 out:
    SensorCall(27946);if (el)
        {/*53*/SensorCall(27945);*el = outlen;/*54*/}

    SensorCall(27948);if (ep)
        {/*55*/SensorCall(27947);*ep = x;/*56*/}
    {_Bool  ReplaceReturn1249 = (x == send); SensorCall(27949); return ReplaceReturn1249;}
}

/*

=for apidoc utf8n_to_uvuni

Bottom level UTF-8 decode routine.
Returns the code point value of the first character in the string C<s>,
which is assumed to be in UTF-8 (or UTF-EBCDIC) encoding, and no longer than
C<curlen> bytes; C<*retlen> (if C<retlen> isn't NULL) will be set to
the length, in bytes, of that character.

The value of C<flags> determines the behavior when C<s> does not point to a
well-formed UTF-8 character.  If C<flags> is 0, when a malformation is found,
zero is returned and C<*retlen> is set so that (S<C<s> + C<*retlen>>) is the
next possible position in C<s> that could begin a non-malformed character.
Also, if UTF-8 warnings haven't been lexically disabled, a warning is raised.

Various ALLOW flags can be set in C<flags> to allow (and not warn on)
individual types of malformations, such as the sequence being overlong (that
is, when there is a shorter sequence that can express the same code point;
overlong sequences are expressly forbidden in the UTF-8 standard due to
potential security issues).  Another malformation example is the first byte of
a character not being a legal first byte.  See F<utf8.h> for the list of such
flags.  For allowed 0 length strings, this function returns 0; for allowed
overlong sequences, the computed code point is returned; for all other allowed
malformations, the Unicode REPLACEMENT CHARACTER is returned, as these have no
determinable reasonable value.

The UTF8_CHECK_ONLY flag overrides the behavior when a non-allowed (by other
flags) malformation is found.  If this flag is set, the routine assumes that
the caller will raise a warning, and this function will silently just set
C<retlen> to C<-1> and return zero.

Certain code points are considered problematic.  These are Unicode surrogates,
Unicode non-characters, and code points above the Unicode maximum of 0x10FFFF.
By default these are considered regular code points, but certain situations
warrant special handling for them.  If C<flags> contains
UTF8_DISALLOW_ILLEGAL_INTERCHANGE, all three classes are treated as
malformations and handled as such.  The flags UTF8_DISALLOW_SURROGATE,
UTF8_DISALLOW_NONCHAR, and UTF8_DISALLOW_SUPER (meaning above the legal Unicode
maximum) can be set to disallow these categories individually.

The flags UTF8_WARN_ILLEGAL_INTERCHANGE, UTF8_WARN_SURROGATE,
UTF8_WARN_NONCHAR, and UTF8_WARN_SUPER will cause warning messages to be raised
for their respective categories, but otherwise the code points are considered
valid (not malformations).  To get a category to both be treated as a
malformation and raise a warning, specify both the WARN and DISALLOW flags.
(But note that warnings are not raised if lexically disabled nor if
UTF8_CHECK_ONLY is also specified.)

Very large code points (above 0x7FFF_FFFF) are considered more problematic than
the others that are above the Unicode legal maximum.  There are several
reasons: they requre at least 32 bits to represent them on ASCII platforms, are
not representable at all on EBCDIC platforms, and the original UTF-8
specification never went above this number (the current 0x10FFFF limit was
imposed later).  (The smaller ones, those that fit into 32 bits, are
representable by a UV on ASCII platforms, but not by an IV, which means that
the number of operations that can be performed on them is quite restricted.)
The UTF-8 encoding on ASCII platforms for these large code points begins with a
byte containing 0xFE or 0xFF.  The UTF8_DISALLOW_FE_FF flag will cause them to
be treated as malformations, while allowing smaller above-Unicode code points.
(Of course UTF8_DISALLOW_SUPER will treat all above-Unicode code points,
including these, as malformations.) Similarly, UTF8_WARN_FE_FF acts just like
the other WARN flags, but applies just to these code points.

All other code points corresponding to Unicode characters, including private
use and those yet to be assigned, are never considered malformed and never
warn.

Most code should use L</utf8_to_uvchr_buf>() rather than call this directly.

=cut
*/

UV
Perl_utf8n_to_uvuni(pTHX_ const U8 *s, STRLEN curlen, STRLEN *retlen, U32 flags)
{
SensorCall(27950);    dVAR;
    const U8 * const s0 = s;
    U8 overflow_byte = '\0';	/* Save byte in case of overflow */
    U8 * send;
    UV uv = *s;
    STRLEN expectlen;
    SV* sv = NULL;
    UV outlier_ret = 0;	/* return value when input is in error or problematic
			 */
    UV pack_warn = 0;	/* Save result of packWARN() for later */
    bool unexpected_non_continuation = FALSE;
    bool overflowed = FALSE;
    bool do_overlong_test = TRUE;   /* May have to skip this test */

    const char* const malformed_text = "Malformed UTF-8 character";

    PERL_ARGS_ASSERT_UTF8N_TO_UVUNI;

    /* The order of malformation tests here is important.  We should consume as
     * few bytes as possible in order to not skip any valid character.  This is
     * required by the Unicode Standard (section 3.9 of Unicode 6.0); see also
     * http://unicode.org/reports/tr36 for more discussion as to why.  For
     * example, once we've done a UTF8SKIP, we can tell the expected number of
     * bytes, and could fail right off the bat if the input parameters indicate
     * that there are too few available.  But it could be that just that first
     * byte is garbled, and the intended character occupies fewer bytes.  If we
     * blindly assumed that the first byte is correct, and skipped based on
     * that number, we could skip over a valid input character.  So instead, we
     * always examine the sequence byte-by-byte.
     *
     * We also should not consume too few bytes, otherwise someone could inject
     * things.  For example, an input could be deliberately designed to
     * overflow, and if this code bailed out immediately upon discovering that,
     * returning to the caller *retlen pointing to the very next byte (one
     * which is actually part of of the overflowing sequence), that could look
     * legitimate to the caller, which could discard the initial partial
     * sequence and process the rest, inappropriately */

    /* Zero length strings, if allowed, of necessity are zero */
    SensorCall(27958);if (curlen == 0) {
	SensorCall(27951);if (retlen) {
	    SensorCall(27952);*retlen = 0;
	}

	SensorCall(27954);if (flags & UTF8_ALLOW_EMPTY) {
	    {UV  ReplaceReturn1248 = 0; SensorCall(27953); return ReplaceReturn1248;}
	}
	SensorCall(27956);if (! (flags & UTF8_CHECK_ONLY)) {
	    SensorCall(27955);sv = sv_2mortal(Perl_newSVpvf(aTHX_ "%s (empty string)", malformed_text));
	}
	SensorCall(27957);goto malformed;
    }

    SensorCall(27959);expectlen = UTF8SKIP(s);

    /* A well-formed UTF-8 character, as the vast majority of calls to this
     * function will be for, has this expected length.  For efficiency, set
     * things up here to return it.  It will be overriden only in those rare
     * cases where a malformation is found */
    SensorCall(27961);if (retlen) {
	SensorCall(27960);*retlen = expectlen;
    }

    /* An invariant is trivially well-formed */
    SensorCall(27963);if (UTF8_IS_INVARIANT(uv)) {
	{UV  ReplaceReturn1247 = (UV) (NATIVE_TO_UTF(*s)); SensorCall(27962); return ReplaceReturn1247;}
    }

    /* A continuation character can't start a valid sequence */
    SensorCall(27972);if (UTF8_IS_CONTINUATION(uv)) {
	SensorCall(27964);if (flags & UTF8_ALLOW_CONTINUATION) {
	    SensorCall(27965);if (retlen) {
		SensorCall(27966);*retlen = 1;
	    }
	    {UV  ReplaceReturn1246 = UNICODE_REPLACEMENT; SensorCall(27967); return ReplaceReturn1246;}
	}

	SensorCall(27969);if (! (flags & UTF8_CHECK_ONLY)) {
	    SensorCall(27968);sv = sv_2mortal(Perl_newSVpvf(aTHX_ "%s (unexpected continuation byte 0x%02x, with no preceding start byte)", malformed_text, *s0));
	}
	SensorCall(27970);curlen = 1;
	SensorCall(27971);goto malformed;
    }

#ifdef EBCDIC
    uv = NATIVE_TO_UTF(uv);
#endif

    /* Here is not a continuation byte, nor an invariant.  The only thing left
     * is a start byte (possibly for an overlong) */

    /* Remove the leading bits that indicate the number of bytes in the
     * character's whole UTF-8 sequence, leaving just the bits that are part of
     * the value */
    SensorCall(27973);uv &= UTF_START_MASK(expectlen);

    /* Now, loop through the remaining bytes in the character's sequence,
     * accumulating each into the working value as we go.  Be sure to not look
     * past the end of the input string */
    send =  (U8*) s0 + ((expectlen <= curlen) ? expectlen : curlen);

    SensorCall(27980);for (s = s0 + 1; s < send; s++) {
	SensorCall(27974);if (UTF8_IS_CONTINUATION(*s)) {
#ifndef EBCDIC	/* Can't overflow in EBCDIC */
	    SensorCall(27975);if (uv & UTF_ACCUMULATION_OVERFLOW_MASK) {

		/* The original implementors viewed this malformation as more
		 * serious than the others (though I, khw, don't understand
		 * why, since other malformations also give very very wrong
		 * results), so there is no way to turn off checking for it.
		 * Set a flag, but keep going in the loop, so that we absorb
		 * the rest of the bytes that comprise the character. */
		SensorCall(27976);overflowed = TRUE;
		overflow_byte = *s; /* Save for warning message's use */
	    }
#endif
	    SensorCall(27977);uv = UTF8_ACCUMULATE(uv, *s);
	}
	else {
	    /* Here, found a non-continuation before processing all expected
	     * bytes.  This byte begins a new character, so quit, even if
	     * allowing this malformation. */
	    SensorCall(27978);unexpected_non_continuation = TRUE;
	    SensorCall(27979);break;
	}
    } /* End of loop through the character's bytes */

    /* Save how many bytes were actually in the character */
    SensorCall(27981);curlen = s - s0;

    /* The loop above finds two types of malformations: non-continuation and/or
     * overflow.  The non-continuation malformation is really a too-short
     * malformation, as it means that the current character ended before it was
     * expected to (being terminated prematurely by the beginning of the next
     * character, whereas in the too-short malformation there just are too few
     * bytes available to hold the character.  In both cases, the check below
     * that we have found the expected number of bytes would fail if executed.)
     * Thus the non-continuation malformation is really unnecessary, being a
     * subset of the too-short malformation.  But there may be existing
     * applications that are expecting the non-continuation type, so we retain
     * it, and return it in preference to the too-short malformation.  (If this
     * code were being written from scratch, the two types might be collapsed
     * into one.)  I, khw, am also giving priority to returning the
     * non-continuation and too-short malformations over overflow when multiple
     * ones are present.  I don't know of any real reason to prefer one over
     * the other, except that it seems to me that multiple-byte errors trumps
     * errors from a single byte */
    SensorCall(27999);if (unexpected_non_continuation) {
	SensorCall(27982);if (!(flags & UTF8_ALLOW_NON_CONTINUATION)) {
	    SensorCall(27983);if (! (flags & UTF8_CHECK_ONLY)) {
		SensorCall(27984);if (curlen == 1) {
		    SensorCall(27985);sv = sv_2mortal(Perl_newSVpvf(aTHX_ "%s (unexpected non-continuation byte 0x%02x, immediately after start byte 0x%02x)", malformed_text, *s, *s0));
		}
		else {
		    SensorCall(27986);sv = sv_2mortal(Perl_newSVpvf(aTHX_ "%s (unexpected non-continuation byte 0x%02x, %d bytes after start byte 0x%02x, expected %d bytes)", malformed_text, *s, (int) curlen, *s0, (int)expectlen));
		}
	    }
	    SensorCall(27987);goto malformed;
	}
	SensorCall(27988);uv = UNICODE_REPLACEMENT;

	/* Skip testing for overlongs, as the REPLACEMENT may not be the same
	 * as what the original expectations were. */
	do_overlong_test = FALSE;
	SensorCall(27990);if (retlen) {
	    SensorCall(27989);*retlen = curlen;
	}
    }
    else {/*87*/SensorCall(27991);if (curlen < expectlen) {
	SensorCall(27992);if (! (flags & UTF8_ALLOW_SHORT)) {
	    SensorCall(27993);if (! (flags & UTF8_CHECK_ONLY)) {
		SensorCall(27994);sv = sv_2mortal(Perl_newSVpvf(aTHX_ "%s (%d byte%s, need %d, after start byte 0x%02x)", malformed_text, (int)curlen, curlen == 1 ? "" : "s", (int)expectlen, *s0));
	    }
	    SensorCall(27995);goto malformed;
	}
	SensorCall(27996);uv = UNICODE_REPLACEMENT;
	do_overlong_test = FALSE;
	SensorCall(27998);if (retlen) {
	    SensorCall(27997);*retlen = curlen;
	}
    ;/*88*/}}

#ifndef EBCDIC	/* EBCDIC allows FE, FF, can't overflow */
    SensorCall(28004);if ((*s0 & 0xFE) == 0xFE	/* matches both FE, FF */
	&& (flags & (UTF8_WARN_FE_FF|UTF8_DISALLOW_FE_FF)))
    {
	/* By adding UTF8_CHECK_ONLY to the test, we avoid unnecessary
	 * generation of the sv, since no warnings are raised under CHECK */
	SensorCall(28000);if ((flags & (UTF8_WARN_FE_FF|UTF8_CHECK_ONLY)) == UTF8_WARN_FE_FF
	    && ckWARN_d(WARN_UTF8))
	{
	    SensorCall(28001);sv = sv_2mortal(Perl_newSVpvf(aTHX_ "%s Code point beginning with byte 0x%02X is not Unicode, and not portable", malformed_text, *s0));
	    pack_warn = packWARN(WARN_UTF8);
	}
	SensorCall(28003);if (flags & UTF8_DISALLOW_FE_FF) {
	    SensorCall(28002);goto malformed;
	}
    }
    SensorCall(28007);if (overflowed) {

	/* If the first byte is FF, it will overflow a 32-bit word.  If the
	 * first byte is FE, it will overflow a signed 32-bit word.  The
	 * above preserves backward compatibility, since its message was used
	 * in earlier versions of this code in preference to overflow */
	SensorCall(28005);sv = sv_2mortal(Perl_newSVpvf(aTHX_ "%s (overflow at byte 0x%02x, after start byte 0x%02x)", malformed_text, overflow_byte, *s0));
	SensorCall(28006);goto malformed;
    }
#endif

    SensorCall(28011);if (do_overlong_test
	&& expectlen > (STRLEN)UNISKIP(uv)
	&& ! (flags & UTF8_ALLOW_LONG))
    {
	/* The overlong malformation has lower precedence than the others.
	 * Note that if this malformation is allowed, we return the actual
	 * value, instead of the replacement character.  This is because this
	 * value is actually well-defined. */
	SensorCall(28008);if (! (flags & UTF8_CHECK_ONLY)) {
	    SensorCall(28009);sv = sv_2mortal(Perl_newSVpvf(aTHX_ "%s (%d byte%s, need %d, after start byte 0x%02x)", malformed_text, (int)expectlen, expectlen == 1 ? "": "s", UNISKIP(uv), *s0));
	}
	SensorCall(28010);goto malformed;
    }

    /* Here, the input is considered to be well-formed , but could be a
     * problematic code point that is not allowed by the input parameters. */
    SensorCall(28030);if (uv >= UNICODE_SURROGATE_FIRST /* isn't problematic if < this */
	&& (flags & (UTF8_DISALLOW_ILLEGAL_INTERCHANGE
		     |UTF8_WARN_ILLEGAL_INTERCHANGE)))
    {
	SensorCall(28012);if (UNICODE_IS_SURROGATE(uv)) {
	    SensorCall(28013);if ((flags & (UTF8_WARN_SURROGATE|UTF8_CHECK_ONLY)) == UTF8_WARN_SURROGATE
		&& ckWARN2_d(WARN_UTF8, WARN_SURROGATE))
	    {
		SensorCall(28014);sv = sv_2mortal(Perl_newSVpvf(aTHX_ "UTF-16 surrogate U+%04"UVXf"", uv));
		pack_warn = packWARN2(WARN_UTF8, WARN_SURROGATE);
	    }
	    SensorCall(28016);if (flags & UTF8_DISALLOW_SURROGATE) {
		SensorCall(28015);goto disallowed;
	    }
	}
	else {/*89*/SensorCall(28017);if (UNICODE_IS_NONCHAR(uv)) {
	    SensorCall(28018);if ((flags & (UTF8_WARN_NONCHAR|UTF8_CHECK_ONLY)) == UTF8_WARN_NONCHAR
		&& ckWARN2_d(WARN_UTF8, WARN_NONCHAR))
	    {
		SensorCall(28019);sv = sv_2mortal(Perl_newSVpvf(aTHX_ "Unicode non-character U+%04"UVXf" is illegal for open interchange", uv));
		pack_warn = packWARN2(WARN_UTF8, WARN_NONCHAR);
	    }
	    SensorCall(28021);if (flags & UTF8_DISALLOW_NONCHAR) {
		SensorCall(28020);goto disallowed;
	    }
	}
	else {/*91*/SensorCall(28022);if ((uv > PERL_UNICODE_MAX)) {
	    SensorCall(28023);if ((flags & (UTF8_WARN_SUPER|UTF8_CHECK_ONLY)) == UTF8_WARN_SUPER
		&& ckWARN2_d(WARN_UTF8, WARN_NON_UNICODE))
	    {
		SensorCall(28024);sv = sv_2mortal(Perl_newSVpvf(aTHX_ "Code point 0x%04"UVXf" is not Unicode, may not be portable", uv));
		pack_warn = packWARN2(WARN_UTF8, WARN_NON_UNICODE);
	    }
	    SensorCall(28026);if (flags & UTF8_DISALLOW_SUPER) {
		SensorCall(28025);goto disallowed;
	    }
	;/*92*/}/*90*/}}

	SensorCall(28029);if (sv) {
	    SensorCall(28027);outlier_ret = uv;
	    SensorCall(28028);goto do_warn;
	}

	/* Here, this is not considered a malformed character, so drop through
	 * to return it */
    }

    {UV  ReplaceReturn1245 = uv; SensorCall(28031); return ReplaceReturn1245;}

    /* There are three cases which get to beyond this point.  In all 3 cases:
     * <sv>	    if not null points to a string to print as a warning.
     * <curlen>	    is what <*retlen> should be set to if UTF8_CHECK_ONLY isn't
     *		    set.
     * <outlier_ret> is what return value to use if UTF8_CHECK_ONLY isn't set.
     *		    This is done by initializing it to 0, and changing it only
     *		    for case 1).
     * The 3 cases are:
     * 1)   The input is valid but problematic, and to be warned about.  The
     *	    return value is the resultant code point; <*retlen> is set to
     *	    <curlen>, the number of bytes that comprise the code point.
     *	    <pack_warn> contains the result of packWARN() for the warning
     *	    types.  The entry point for this case is the label <do_warn>;
     * 2)   The input is a valid code point but disallowed by the parameters to
     *	    this function.  The return value is 0.  If UTF8_CHECK_ONLY is set,
     *	    <*relen> is -1; otherwise it is <curlen>, the number of bytes that
     *	    comprise the code point.  <pack_warn> contains the result of
     *	    packWARN() for the warning types.  The entry point for this case is
     *	    the label <disallowed>.
     * 3)   The input is malformed.  The return value is 0.  If UTF8_CHECK_ONLY
     *	    is set, <*relen> is -1; otherwise it is <curlen>, the number of
     *	    bytes that comprise the malformation.  All such malformations are
     *	    assumed to be warning type <utf8>.  The entry point for this case
     *	    is the label <malformed>.
     */

malformed:

    if (sv && ckWARN_d(WARN_UTF8)) {
	pack_warn = packWARN(WARN_UTF8);
    }

disallowed:

    if (flags & UTF8_CHECK_ONLY) {
	if (retlen)
	    {/*93*/*retlen = ((STRLEN) -1);/*94*/}
	return 0;
    }

do_warn:

    if (pack_warn) {	/* <pack_warn> was initialized to 0, and changed only
			   if warnings are to be raised. */
	const char * const string = SvPVX_const(sv);

	if (PL_op)
	    {/*95*/Perl_warner(aTHX_ pack_warn, "%s in %s", string,  OP_DESC(PL_op));/*96*/}
	else
	    {/*97*/Perl_warner(aTHX_ pack_warn, "%s", string);/*98*/}
    }

    if (retlen) {
	*retlen = curlen;
    }

    return outlier_ret;
}

/*
=for apidoc utf8_to_uvchr_buf

Returns the native code point of the first character in the string C<s> which
is assumed to be in UTF-8 encoding; C<send> points to 1 beyond the end of C<s>.
C<*retlen> will be set to the length, in bytes, of that character.

If C<s> does not point to a well-formed UTF-8 character and UTF8 warnings are
enabled, zero is returned and C<*retlen> is set (if C<retlen> isn't
NULL) to -1.  If those warnings are off, the computed value if well-defined (or
the Unicode REPLACEMENT CHARACTER, if not) is silently returned, and C<*retlen>
is set (if C<retlen> isn't NULL) so that (S<C<s> + C<*retlen>>) is the
next possible position in C<s> that could begin a non-malformed character.
See L</utf8n_to_uvuni> for details on when the REPLACEMENT CHARACTER is returned.

=cut
*/


UV
Perl_utf8_to_uvchr_buf(pTHX_ const U8 *s, const U8 *send, STRLEN *retlen)
{
    PERL_ARGS_ASSERT_UTF8_TO_UVCHR_BUF;

    assert(s < send);

    {UV  ReplaceReturn1244 = utf8n_to_uvchr(s, send - s, retlen,
			  ckWARN_d(WARN_UTF8) ? 0 : UTF8_ALLOW_ANY); SensorCall(28032); return ReplaceReturn1244;}
}

/* Like L</utf8_to_uvchr_buf>(), but should only be called when it is known that
 * there are no malformations in the input UTF-8 string C<s>.  Currently, some
 * malformations are checked for, but this checking likely will be removed in
 * the future */

UV
Perl_valid_utf8_to_uvchr(pTHX_ const U8 *s, STRLEN *retlen)
{
    PERL_ARGS_ASSERT_VALID_UTF8_TO_UVCHR;

    {UV  ReplaceReturn1243 = utf8_to_uvchr_buf(s, s + UTF8_MAXBYTES, retlen); SensorCall(28033); return ReplaceReturn1243;}
}

/*
=for apidoc utf8_to_uvchr

DEPRECATED!

Returns the native code point of the first character in the string C<s>
which is assumed to be in UTF-8 encoding; C<retlen> will be set to the
length, in bytes, of that character.

Some, but not all, UTF-8 malformations are detected, and in fact, some
malformed input could cause reading beyond the end of the input buffer, which
is why this function is deprecated.  Use L</utf8_to_uvchr_buf> instead.

If C<s> points to one of the detected malformations, and UTF8 warnings are
enabled, zero is returned and C<*retlen> is set (if C<retlen> isn't
NULL) to -1.  If those warnings are off, the computed value if well-defined (or
the Unicode REPLACEMENT CHARACTER, if not) is silently returned, and C<*retlen>
is set (if C<retlen> isn't NULL) so that (S<C<s> + C<*retlen>>) is the
next possible position in C<s> that could begin a non-malformed character.
See L</utf8n_to_uvuni> for details on when the REPLACEMENT CHARACTER is returned.

=cut
*/

UV
Perl_utf8_to_uvchr(pTHX_ const U8 *s, STRLEN *retlen)
{
    PERL_ARGS_ASSERT_UTF8_TO_UVCHR;

    {UV  ReplaceReturn1242 = valid_utf8_to_uvchr(s, retlen); SensorCall(28034); return ReplaceReturn1242;}
}

/*
=for apidoc utf8_to_uvuni_buf

Returns the Unicode code point of the first character in the string C<s> which
is assumed to be in UTF-8 encoding; C<send> points to 1 beyond the end of C<s>.
C<retlen> will be set to the length, in bytes, of that character.

This function should only be used when the returned UV is considered
an index into the Unicode semantic tables (e.g. swashes).

If C<s> does not point to a well-formed UTF-8 character and UTF8 warnings are
enabled, zero is returned and C<*retlen> is set (if C<retlen> isn't
NULL) to -1.  If those warnings are off, the computed value if well-defined (or
the Unicode REPLACEMENT CHARACTER, if not) is silently returned, and C<*retlen>
is set (if C<retlen> isn't NULL) so that (S<C<s> + C<*retlen>>) is the
next possible position in C<s> that could begin a non-malformed character.
See L</utf8n_to_uvuni> for details on when the REPLACEMENT CHARACTER is returned.

=cut
*/

UV
Perl_utf8_to_uvuni_buf(pTHX_ const U8 *s, const U8 *send, STRLEN *retlen)
{
    PERL_ARGS_ASSERT_UTF8_TO_UVUNI_BUF;

    assert(send > s);

    /* Call the low level routine asking for checks */
    {UV  ReplaceReturn1241 = Perl_utf8n_to_uvuni(aTHX_ s, send -s, retlen,
			       ckWARN_d(WARN_UTF8) ? 0 : UTF8_ALLOW_ANY); SensorCall(28035); return ReplaceReturn1241;}
}

/* Like L</utf8_to_uvuni_buf>(), but should only be called when it is known that
 * there are no malformations in the input UTF-8 string C<s>.  Currently, some
 * malformations are checked for, but this checking likely will be removed in
 * the future */

UV
Perl_valid_utf8_to_uvuni(pTHX_ const U8 *s, STRLEN *retlen)
{
    PERL_ARGS_ASSERT_VALID_UTF8_TO_UVUNI;

    {UV  ReplaceReturn1240 = utf8_to_uvuni_buf(s, s + UTF8_MAXBYTES, retlen); SensorCall(28036); return ReplaceReturn1240;}
}

/*
=for apidoc utf8_to_uvuni

DEPRECATED!

Returns the Unicode code point of the first character in the string C<s>
which is assumed to be in UTF-8 encoding; C<retlen> will be set to the
length, in bytes, of that character.

This function should only be used when the returned UV is considered
an index into the Unicode semantic tables (e.g. swashes).

Some, but not all, UTF-8 malformations are detected, and in fact, some
malformed input could cause reading beyond the end of the input buffer, which
is why this function is deprecated.  Use L</utf8_to_uvuni_buf> instead.

If C<s> points to one of the detected malformations, and UTF8 warnings are
enabled, zero is returned and C<*retlen> is set (if C<retlen> doesn't point to
NULL) to -1.  If those warnings are off, the computed value if well-defined (or
the Unicode REPLACEMENT CHARACTER, if not) is silently returned, and C<*retlen>
is set (if C<retlen> isn't NULL) so that (S<C<s> + C<*retlen>>) is the
next possible position in C<s> that could begin a non-malformed character.
See L</utf8n_to_uvuni> for details on when the REPLACEMENT CHARACTER is returned.

=cut
*/

UV
Perl_utf8_to_uvuni(pTHX_ const U8 *s, STRLEN *retlen)
{
    PERL_ARGS_ASSERT_UTF8_TO_UVUNI;

    {UV  ReplaceReturn1239 = valid_utf8_to_uvuni(s, retlen); SensorCall(28037); return ReplaceReturn1239;}
}

/*
=for apidoc utf8_length

Return the length of the UTF-8 char encoded string C<s> in characters.
Stops at C<e> (inclusive).  If C<e E<lt> s> or if the scan would end
up past C<e>, croaks.

=cut
*/

STRLEN
Perl_utf8_length(pTHX_ const U8 *s, const U8 *e)
{
SensorCall(28038);    dVAR;
    STRLEN len = 0;

    PERL_ARGS_ASSERT_UTF8_LENGTH;

    /* Note: cannot use UTF8_IS_...() too eagerly here since e.g.
     * the bitops (especially ~) can create illegal UTF-8.
     * In other words: in Perl UTF-8 is not just for Unicode. */

    SensorCall(28040);if (e < s)
	{/*79*/SensorCall(28039);goto warn_and_return;/*80*/}
    SensorCall(28044);while (s < e) {
	SensorCall(28041);if (!UTF8_IS_INVARIANT(*s))
	    s += UTF8SKIP(s);
	else
	    {/*81*/SensorCall(28042);s++;/*82*/}
	SensorCall(28043);len++;
    }

    SensorCall(28049);if (e != s) {
	SensorCall(28045);len--;
        warn_and_return:
	SensorCall(28048);if (PL_op)
	    {/*83*/SensorCall(28046);Perl_ck_warner_d(aTHX_ packWARN(WARN_UTF8),
			     "%s in %s", unees, OP_DESC(PL_op));/*84*/}
	else
	    {/*85*/SensorCall(28047);Perl_ck_warner_d(aTHX_ packWARN(WARN_UTF8), "%s", unees);/*86*/}
    }

    {STRLEN  ReplaceReturn1238 = len; SensorCall(28050); return ReplaceReturn1238;}
}

/*
=for apidoc utf8_distance

Returns the number of UTF-8 characters between the UTF-8 pointers C<a>
and C<b>.

WARNING: use only if you *know* that the pointers point inside the
same UTF-8 buffer.

=cut
*/

IV
Perl_utf8_distance(pTHX_ const U8 *a, const U8 *b)
{
    PERL_ARGS_ASSERT_UTF8_DISTANCE;

    {IV  ReplaceReturn1237 = (a < b) ? -1 * (IV) utf8_length(a, b) : (IV) utf8_length(b, a); SensorCall(28051); return ReplaceReturn1237;}
}

/*
=for apidoc utf8_hop

Return the UTF-8 pointer C<s> displaced by C<off> characters, either
forward or backward.

WARNING: do not use the following unless you *know* C<off> is within
the UTF-8 data pointed to by C<s> *and* that on entry C<s> is aligned
on the first byte of character or just after the last byte of a character.

=cut
*/

U8 *
Perl_utf8_hop(pTHX_ const U8 *s, I32 off)
{
SensorCall(28052);    PERL_ARGS_ASSERT_UTF8_HOP;

    PERL_UNUSED_CONTEXT;
    /* Note: cannot use UTF8_IS_...() too eagerly here since e.g
     * the bitops (especially ~) can create illegal UTF-8.
     * In other words: in Perl UTF-8 is not just for Unicode. */

    SensorCall(28058);if (off >= 0) {
	SensorCall(28053);while (off--)
	    s += UTF8SKIP(s);
    }
    else {
	SensorCall(28054);while (off++) {
	    SensorCall(28055);s--;
	    SensorCall(28057);while (UTF8_IS_CONTINUATION(*s))
		{/*77*/SensorCall(28056);s--;/*78*/}
	}
    }
    {U8 * ReplaceReturn1236 = (U8 *)s; SensorCall(28059); return ReplaceReturn1236;}
}

/*
=for apidoc bytes_cmp_utf8

Compares the sequence of characters (stored as octets) in C<b>, C<blen> with the
sequence of characters (stored as UTF-8) in C<u>, C<ulen>. Returns 0 if they are
equal, -1 or -2 if the first string is less than the second string, +1 or +2
if the first string is greater than the second string.

-1 or +1 is returned if the shorter string was identical to the start of the
longer string. -2 or +2 is returned if the was a difference between characters
within the strings.

=cut
*/

int
Perl_bytes_cmp_utf8(pTHX_ const U8 *b, STRLEN blen, const U8 *u, STRLEN ulen)
{
    SensorCall(28060);const U8 *const bend = b + blen;
    const U8 *const uend = u + ulen;

    PERL_ARGS_ASSERT_BYTES_CMP_UTF8;

    PERL_UNUSED_CONTEXT;

    SensorCall(28078);while (b < bend && u < uend) {
        SensorCall(28061);U8 c = *u++;
	SensorCall(28074);if (!UTF8_IS_INVARIANT(c)) {
	    SensorCall(28062);if (UTF8_IS_DOWNGRADEABLE_START(c)) {
		SensorCall(28063);if (u < uend) {
		    SensorCall(28064);U8 c1 = *u++;
		    SensorCall(28068);if (UTF8_IS_CONTINUATION(c1)) {
			SensorCall(28065);c = UNI_TO_NATIVE(TWO_BYTE_UTF8_TO_UNI(c, c1));
		    } else {
			SensorCall(28066);Perl_ck_warner_d(aTHX_ packWARN(WARN_UTF8),
					 "Malformed UTF-8 character "
					 "(unexpected non-continuation byte 0x%02x"
					 ", immediately after start byte 0x%02x)"
					 /* Dear diag.t, it's in the pod.  */
					 "%s%s", c1, c,
					 PL_op ? " in " : "",
					 PL_op ? OP_DESC(PL_op) : "");
			{int  ReplaceReturn1235 = -2; SensorCall(28067); return ReplaceReturn1235;}
		    }
		} else {
		    SensorCall(28069);if (PL_op)
			{/*9*/SensorCall(28070);Perl_ck_warner_d(aTHX_ packWARN(WARN_UTF8),
					 "%s in %s", unees, OP_DESC(PL_op));/*10*/}
		    else
			{/*11*/SensorCall(28071);Perl_ck_warner_d(aTHX_ packWARN(WARN_UTF8), "%s", unees);/*12*/}
		    {int  ReplaceReturn1234 = -2; SensorCall(28072); return ReplaceReturn1234;} /* Really want to return undef :-)  */
		}
	    } else {
		{int  ReplaceReturn1233 = -2; SensorCall(28073); return ReplaceReturn1233;}
	    }
	}
	SensorCall(28076);if (*b != c) {
	    {int  ReplaceReturn1232 = *b < c ? -2 : +2; SensorCall(28075); return ReplaceReturn1232;}
	}
	SensorCall(28077);++b;
    }

    SensorCall(28080);if (b == bend && u == uend)
	{/*13*/{int  ReplaceReturn1231 = 0; SensorCall(28079); return ReplaceReturn1231;}/*14*/}

    {int  ReplaceReturn1230 = b < bend ? +1 : -1; SensorCall(28081); return ReplaceReturn1230;}
}

/*
=for apidoc utf8_to_bytes

Converts a string C<s> of length C<len> from UTF-8 into native byte encoding.
Unlike L</bytes_to_utf8>, this over-writes the original string, and
updates C<len> to contain the new length.
Returns zero on failure, setting C<len> to -1.

If you need a copy of the string, see L</bytes_from_utf8>.

=cut
*/

U8 *
Perl_utf8_to_bytes(pTHX_ U8 *s, STRLEN *len)
{
    SensorCall(28082);U8 * const save = s;
    U8 * const send = s + *len;
    U8 *d;

    PERL_ARGS_ASSERT_UTF8_TO_BYTES;

    /* ensure valid UTF-8 and chars < 256 before updating string */
    SensorCall(28087);while (s < send) {
        SensorCall(28083);U8 c = *s++;

        SensorCall(28086);if (!UTF8_IS_INVARIANT(c) &&
            (!UTF8_IS_DOWNGRADEABLE_START(c) || (s >= send)
	     || !(c = *s++) || !UTF8_IS_CONTINUATION(c))) {
            SensorCall(28084);*len = ((STRLEN) -1);
            {U8 * ReplaceReturn1229 = 0; SensorCall(28085); return ReplaceReturn1229;}
        }
    }

    SensorCall(28088);d = s = save;
    SensorCall(28090);while (s < send) {
        SensorCall(28089);STRLEN ulen;
        *d++ = (U8)utf8_to_uvchr_buf(s, send, &ulen);
        s += ulen;
    }
    SensorCall(28091);*d = '\0';
    *len = d - save;
    {U8 * ReplaceReturn1228 = save; SensorCall(28092); return ReplaceReturn1228;}
}

/*
=for apidoc bytes_from_utf8

Converts a string C<s> of length C<len> from UTF-8 into native byte encoding.
Unlike L</utf8_to_bytes> but like L</bytes_to_utf8>, returns a pointer to
the newly-created string, and updates C<len> to contain the new
length.  Returns the original string if no conversion occurs, C<len>
is unchanged. Do nothing if C<is_utf8> points to 0. Sets C<is_utf8> to
0 if C<s> is converted or consisted entirely of characters that are invariant
in utf8 (i.e., US-ASCII on non-EBCDIC machines).

=cut
*/

U8 *
Perl_bytes_from_utf8(pTHX_ const U8 *s, STRLEN *len, bool *is_utf8)
{
    SensorCall(28093);U8 *d;
    const U8 *start = s;
    const U8 *send;
    I32 count = 0;

    PERL_ARGS_ASSERT_BYTES_FROM_UTF8;

    PERL_UNUSED_CONTEXT;
    SensorCall(28095);if (!*is_utf8)
        {/*15*/{U8 * ReplaceReturn1227 = (U8 *)start; SensorCall(28094); return ReplaceReturn1227;}/*16*/}

    /* ensure valid UTF-8 and chars < 256 before converting string */
    SensorCall(28101);for (send = s + *len; s < send;) {
        SensorCall(28096);U8 c = *s++;
	SensorCall(28100);if (!UTF8_IS_INVARIANT(c)) {
	    SensorCall(28097);if (UTF8_IS_DOWNGRADEABLE_START(c) && s < send &&
                (c = *s++) && UTF8_IS_CONTINUATION(c))
		{/*17*/SensorCall(28098);count++;/*18*/}
	    else
                {/*19*/{U8 * ReplaceReturn1226 = (U8 *)start; SensorCall(28099); return ReplaceReturn1226;}/*20*/}
	}
    }

    SensorCall(28102);*is_utf8 = FALSE;

    Newx(d, (*len) - count + 1, U8);
    s = start; start = d;
    SensorCall(28107);while (s < send) {
	SensorCall(28103);U8 c = *s++;
	SensorCall(28105);if (!UTF8_IS_INVARIANT(c)) {
	    /* Then it is two-byte encoded */
	    SensorCall(28104);c = UNI_TO_NATIVE(TWO_BYTE_UTF8_TO_UNI(c, *s++));
	}
	SensorCall(28106);*d++ = c;
    }
    SensorCall(28108);*d = '\0';
    *len = d - start;
    {U8 * ReplaceReturn1225 = (U8 *)start; SensorCall(28109); return ReplaceReturn1225;}
}

/*
=for apidoc bytes_to_utf8

Converts a string C<s> of length C<len> bytes from the native encoding into
UTF-8.
Returns a pointer to the newly-created string, and sets C<len> to
reflect the new length in bytes.

A NUL character will be written after the end of the string.

If you want to convert to UTF-8 from encodings other than
the native (Latin1 or EBCDIC),
see L</sv_recode_to_utf8>().

=cut
*/

/* This logic is duplicated in sv_catpvn_flags, so any bug fixes will
   likewise need duplication. */

U8*
Perl_bytes_to_utf8(pTHX_ const U8 *s, STRLEN *len)
{
    SensorCall(28110);const U8 * const send = s + (*len);
    U8 *d;
    U8 *dst;

    PERL_ARGS_ASSERT_BYTES_TO_UTF8;
    PERL_UNUSED_CONTEXT;

    Newx(d, (*len) * 2 + 1, U8);
    dst = d;

    SensorCall(28114);while (s < send) {
        SensorCall(28111);const UV uv = NATIVE_TO_ASCII(*s++);
        SensorCall(28113);if (UNI_IS_INVARIANT(uv))
            *d++ = (U8)UTF_TO_NATIVE(uv);
        else {
            SensorCall(28112);*d++ = (U8)UTF8_EIGHT_BIT_HI(uv);
            *d++ = (U8)UTF8_EIGHT_BIT_LO(uv);
        }
    }
    SensorCall(28115);*d = '\0';
    *len = d-dst;
    {U8 * ReplaceReturn1224 = dst; SensorCall(28116); return ReplaceReturn1224;}
}

/*
 * Convert native (big-endian) or reversed (little-endian) UTF-16 to UTF-8.
 *
 * Destination must be pre-extended to 3/2 source.  Do not use in-place.
 * We optimize for native, for obvious reasons. */

U8*
Perl_utf16_to_utf8(pTHX_ U8* p, U8* d, I32 bytelen, I32 *newlen)
{
    SensorCall(28117);U8* pend;
    U8* dstart = d;

    PERL_ARGS_ASSERT_UTF16_TO_UTF8;

    SensorCall(28119);if (bytelen & 1)
	{/*69*/SensorCall(28118);Perl_croak(aTHX_ "panic: utf16_to_utf8: odd bytelen %"UVuf, (UV)bytelen);/*70*/}

    SensorCall(28120);pend = p + bytelen;

    SensorCall(28142);while (p < pend) {
	SensorCall(28121);UV uv = (p[0] << 8) + p[1]; /* UTF-16BE */
	p += 2;
	SensorCall(28124);if (uv < 0x80) {
#ifdef EBCDIC
	    *d++ = UNI_TO_NATIVE(uv);
#else
	    SensorCall(28122);*d++ = (U8)uv;
#endif
	    SensorCall(28123);continue;
	}
	SensorCall(28127);if (uv < 0x800) {
	    SensorCall(28125);*d++ = (U8)(( uv >>  6)         | 0xc0);
	    *d++ = (U8)(( uv        & 0x3f) | 0x80);
	    SensorCall(28126);continue;
	}
	SensorCall(28136);if (uv >= 0xd800 && uv <= 0xdbff) {	/* surrogates */
	    SensorCall(28128);if (p >= pend) {
		SensorCall(28129);Perl_croak(aTHX_ "Malformed UTF-16 surrogate");
	    } else {
		SensorCall(28130);UV low = (p[0] << 8) + p[1];
		p += 2;
		SensorCall(28132);if (low < 0xdc00 || low > 0xdfff)
		    {/*71*/SensorCall(28131);Perl_croak(aTHX_ "Malformed UTF-16 surrogate");/*72*/}
		SensorCall(28133);uv = ((uv - 0xd800) << 10) + (low - 0xdc00) + 0x10000;
	    }
	} else {/*73*/SensorCall(28134);if (uv >= 0xdc00 && uv <= 0xdfff) {
	    SensorCall(28135);Perl_croak(aTHX_ "Malformed UTF-16 surrogate");
	;/*74*/}}
	SensorCall(28141);if (uv < 0x10000) {
	    SensorCall(28137);*d++ = (U8)(( uv >> 12)         | 0xe0);
	    *d++ = (U8)(((uv >>  6) & 0x3f) | 0x80);
	    *d++ = (U8)(( uv        & 0x3f) | 0x80);
	    SensorCall(28138);continue;
	}
	else {
	    SensorCall(28139);*d++ = (U8)(( uv >> 18)         | 0xf0);
	    *d++ = (U8)(((uv >> 12) & 0x3f) | 0x80);
	    *d++ = (U8)(((uv >>  6) & 0x3f) | 0x80);
	    *d++ = (U8)(( uv        & 0x3f) | 0x80);
	    SensorCall(28140);continue;
	}
    }
    SensorCall(28143);*newlen = d - dstart;
    {U8 * ReplaceReturn1223 = d; SensorCall(28144); return ReplaceReturn1223;}
}

/* Note: this one is slightly destructive of the source. */

U8*
Perl_utf16_to_utf8_reversed(pTHX_ U8* p, U8* d, I32 bytelen, I32 *newlen)
{
    SensorCall(28145);U8* s = (U8*)p;
    U8* const send = s + bytelen;

    PERL_ARGS_ASSERT_UTF16_TO_UTF8_REVERSED;

    SensorCall(28147);if (bytelen & 1)
	{/*75*/SensorCall(28146);Perl_croak(aTHX_ "panic: utf16_to_utf8_reversed: odd bytelen %"UVuf,
		   (UV)bytelen);/*76*/}

    SensorCall(28149);while (s < send) {
	SensorCall(28148);const U8 tmp = s[0];
	s[0] = s[1];
	s[1] = tmp;
	s += 2;
    }
    {U8 * ReplaceReturn1222 = utf16_to_utf8(p, d, bytelen, newlen); SensorCall(28150); return ReplaceReturn1222;}
}

/* for now these are all defined (inefficiently) in terms of the utf8 versions.
 * Note that the macros in handy.h that call these short-circuit calling them
 * for Latin-1 range inputs */

bool
Perl_is_uni_alnum(pTHX_ UV c)
{
    SensorCall(28151);U8 tmpbuf[UTF8_MAXBYTES+1];
    uvchr_to_utf8(tmpbuf, c);
    {_Bool  ReplaceReturn1221 = is_utf8_alnum(tmpbuf); SensorCall(28152); return ReplaceReturn1221;}
}

bool
Perl_is_uni_idfirst(pTHX_ UV c)
{
    SensorCall(28153);U8 tmpbuf[UTF8_MAXBYTES+1];
    uvchr_to_utf8(tmpbuf, c);
    {_Bool  ReplaceReturn1220 = is_utf8_idfirst(tmpbuf); SensorCall(28154); return ReplaceReturn1220;}
}

bool
Perl_is_uni_alpha(pTHX_ UV c)
{
    SensorCall(28155);U8 tmpbuf[UTF8_MAXBYTES+1];
    uvchr_to_utf8(tmpbuf, c);
    {_Bool  ReplaceReturn1219 = is_utf8_alpha(tmpbuf); SensorCall(28156); return ReplaceReturn1219;}
}

bool
Perl_is_uni_ascii(pTHX_ UV c)
{
    {_Bool  ReplaceReturn1218 = isASCII(c); SensorCall(28157); return ReplaceReturn1218;}
}

bool
Perl_is_uni_space(pTHX_ UV c)
{
    SensorCall(28158);U8 tmpbuf[UTF8_MAXBYTES+1];
    uvchr_to_utf8(tmpbuf, c);
    {_Bool  ReplaceReturn1217 = is_utf8_space(tmpbuf); SensorCall(28159); return ReplaceReturn1217;}
}

bool
Perl_is_uni_digit(pTHX_ UV c)
{
    SensorCall(28160);U8 tmpbuf[UTF8_MAXBYTES+1];
    uvchr_to_utf8(tmpbuf, c);
    {_Bool  ReplaceReturn1216 = is_utf8_digit(tmpbuf); SensorCall(28161); return ReplaceReturn1216;}
}

bool
Perl_is_uni_upper(pTHX_ UV c)
{
    SensorCall(28162);U8 tmpbuf[UTF8_MAXBYTES+1];
    uvchr_to_utf8(tmpbuf, c);
    {_Bool  ReplaceReturn1215 = is_utf8_upper(tmpbuf); SensorCall(28163); return ReplaceReturn1215;}
}

bool
Perl_is_uni_lower(pTHX_ UV c)
{
    SensorCall(28164);U8 tmpbuf[UTF8_MAXBYTES+1];
    uvchr_to_utf8(tmpbuf, c);
    {_Bool  ReplaceReturn1214 = is_utf8_lower(tmpbuf); SensorCall(28165); return ReplaceReturn1214;}
}

bool
Perl_is_uni_cntrl(pTHX_ UV c)
{
    {_Bool  ReplaceReturn1213 = isCNTRL_L1(c); SensorCall(28166); return ReplaceReturn1213;}
}

bool
Perl_is_uni_graph(pTHX_ UV c)
{
    SensorCall(28167);U8 tmpbuf[UTF8_MAXBYTES+1];
    uvchr_to_utf8(tmpbuf, c);
    {_Bool  ReplaceReturn1212 = is_utf8_graph(tmpbuf); SensorCall(28168); return ReplaceReturn1212;}
}

bool
Perl_is_uni_print(pTHX_ UV c)
{
    SensorCall(28169);U8 tmpbuf[UTF8_MAXBYTES+1];
    uvchr_to_utf8(tmpbuf, c);
    {_Bool  ReplaceReturn1211 = is_utf8_print(tmpbuf); SensorCall(28170); return ReplaceReturn1211;}
}

bool
Perl_is_uni_punct(pTHX_ UV c)
{
    SensorCall(28171);U8 tmpbuf[UTF8_MAXBYTES+1];
    uvchr_to_utf8(tmpbuf, c);
    {_Bool  ReplaceReturn1210 = is_utf8_punct(tmpbuf); SensorCall(28172); return ReplaceReturn1210;}
}

bool
Perl_is_uni_xdigit(pTHX_ UV c)
{
    SensorCall(28173);U8 tmpbuf[UTF8_MAXBYTES_CASE+1];
    uvchr_to_utf8(tmpbuf, c);
    {_Bool  ReplaceReturn1209 = is_utf8_xdigit(tmpbuf); SensorCall(28174); return ReplaceReturn1209;}
}

UV
Perl__to_upper_title_latin1(pTHX_ const U8 c, U8* p, STRLEN *lenp, const char S_or_s)
{
    /* We have the latin1-range values compiled into the core, so just use
     * those, converting the result to utf8.  The only difference between upper
     * and title case in this range is that LATIN_SMALL_LETTER_SHARP_S is
     * either "SS" or "Ss".  Which one to use is passed into the routine in
     * 'S_or_s' to avoid a test */

    SensorCall(28175);UV converted = toUPPER_LATIN1_MOD(c);

    PERL_ARGS_ASSERT__TO_UPPER_TITLE_LATIN1;

    assert(S_or_s == 'S' || S_or_s == 's');

    SensorCall(28178);if (UNI_IS_INVARIANT(converted)) { /* No difference between the two for
					  characters in this range */
	SensorCall(28176);*p = (U8) converted;
	*lenp = 1;
	{UV  ReplaceReturn1208 = converted; SensorCall(28177); return ReplaceReturn1208;}
    }

    /* toUPPER_LATIN1_MOD gives the correct results except for three outliers,
     * which it maps to one of them, so as to only have to have one check for
     * it in the main case */
    SensorCall(28187);if (UNLIKELY(converted == LATIN_SMALL_LETTER_Y_WITH_DIAERESIS)) {
	SensorCall(28179);switch (c) {
	    case LATIN_SMALL_LETTER_Y_WITH_DIAERESIS:
		SensorCall(28180);converted = LATIN_CAPITAL_LETTER_Y_WITH_DIAERESIS;
		SensorCall(28181);break;
	    case MICRO_SIGN:
		SensorCall(28182);converted = GREEK_CAPITAL_LETTER_MU;
		SensorCall(28183);break;
	    case LATIN_SMALL_LETTER_SHARP_S:
		SensorCall(28184);*(p)++ = 'S';
		*p = S_or_s;
		*lenp = 2;
		{UV  ReplaceReturn1207 = 'S'; SensorCall(28185); return ReplaceReturn1207;}
	    default:
		SensorCall(28186);Perl_croak(aTHX_ "panic: to_upper_title_latin1 did not expect '%c' to map to '%c'", c, LATIN_SMALL_LETTER_Y_WITH_DIAERESIS);
		/* NOTREACHED */
	}
    }

    SensorCall(28188);*(p)++ = UTF8_TWO_BYTE_HI(converted);
    *p = UTF8_TWO_BYTE_LO(converted);
    *lenp = 2;

    {UV  ReplaceReturn1206 = converted; SensorCall(28189); return ReplaceReturn1206;}
}

/* Call the function to convert a UTF-8 encoded character to the specified case.
 * Note that there may be more than one character in the result.
 * INP is a pointer to the first byte of the input character
 * OUTP will be set to the first byte of the string of changed characters.  It
 *	needs to have space for UTF8_MAXBYTES_CASE+1 bytes
 * LENP will be set to the length in bytes of the string of changed characters
 *
 * The functions return the ordinal of the first character in the string of OUTP */
#define CALL_UPPER_CASE(INP, OUTP, LENP) Perl_to_utf8_case(aTHX_ INP, OUTP, LENP, &PL_utf8_toupper, "ToUc", "utf8::ToSpecUc")
#define CALL_TITLE_CASE(INP, OUTP, LENP) Perl_to_utf8_case(aTHX_ INP, OUTP, LENP, &PL_utf8_totitle, "ToTc", "utf8::ToSpecTc")
#define CALL_LOWER_CASE(INP, OUTP, LENP) Perl_to_utf8_case(aTHX_ INP, OUTP, LENP, &PL_utf8_tolower, "ToLc", "utf8::ToSpecLc")

/* This additionally has the input parameter SPECIALS, which if non-zero will
 * cause this to use the SPECIALS hash for folding (meaning get full case
 * folding); otherwise, when zero, this implies a simple case fold */
#define CALL_FOLD_CASE(INP, OUTP, LENP, SPECIALS) Perl_to_utf8_case(aTHX_ INP, OUTP, LENP, &PL_utf8_tofold, "ToCf", (SPECIALS) ? "utf8::ToSpecCf" : NULL)

UV
Perl_to_uni_upper(pTHX_ UV c, U8* p, STRLEN *lenp)
{
SensorCall(28190);    dVAR;

    /* Convert the Unicode character whose ordinal is <c> to its uppercase
     * version and store that in UTF-8 in <p> and its length in bytes in <lenp>.
     * Note that the <p> needs to be at least UTF8_MAXBYTES_CASE+1 bytes since
     * the changed version may be longer than the original character.
     *
     * The ordinal of the first character of the changed version is returned
     * (but note, as explained above, that there may be more.) */

    PERL_ARGS_ASSERT_TO_UNI_UPPER;

    SensorCall(28192);if (c < 256) {
	{UV  ReplaceReturn1205 = _to_upper_title_latin1((U8) c, p, lenp, 'S'); SensorCall(28191); return ReplaceReturn1205;}
    }

    uvchr_to_utf8(p, c);
    {UV  ReplaceReturn1204 = CALL_UPPER_CASE(p, p, lenp); SensorCall(28193); return ReplaceReturn1204;}
}

UV
Perl_to_uni_title(pTHX_ UV c, U8* p, STRLEN *lenp)
{
SensorCall(28194);    dVAR;

    PERL_ARGS_ASSERT_TO_UNI_TITLE;

    SensorCall(28196);if (c < 256) {
	{UV  ReplaceReturn1203 = _to_upper_title_latin1((U8) c, p, lenp, 's'); SensorCall(28195); return ReplaceReturn1203;}
    }

    uvchr_to_utf8(p, c);
    {UV  ReplaceReturn1202 = CALL_TITLE_CASE(p, p, lenp); SensorCall(28197); return ReplaceReturn1202;}
}

STATIC U8
S_to_lower_latin1(pTHX_ const U8 c, U8* p, STRLEN *lenp)
{
    /* We have the latin1-range values compiled into the core, so just use
     * those, converting the result to utf8.  Since the result is always just
     * one character, we allow <p> to be NULL */

    SensorCall(28198);U8 converted = toLOWER_LATIN1(c);

    SensorCall(28202);if (p != NULL) {
	SensorCall(28199);if (UNI_IS_INVARIANT(converted)) {
	    SensorCall(28200);*p = converted;
	    *lenp = 1;
	}
	else {
	    SensorCall(28201);*p = UTF8_TWO_BYTE_HI(converted);
	    *(p+1) = UTF8_TWO_BYTE_LO(converted);
	    *lenp = 2;
	}
    }
    {U8  ReplaceReturn1201 = converted; SensorCall(28203); return ReplaceReturn1201;}
}

UV
Perl_to_uni_lower(pTHX_ UV c, U8* p, STRLEN *lenp)
{
SensorCall(28204);    dVAR;

    PERL_ARGS_ASSERT_TO_UNI_LOWER;

    SensorCall(28206);if (c < 256) {
	{UV  ReplaceReturn1200 = to_lower_latin1((U8) c, p, lenp); SensorCall(28205); return ReplaceReturn1200;}
    }

    uvchr_to_utf8(p, c);
    {UV  ReplaceReturn1199 = CALL_LOWER_CASE(p, p, lenp); SensorCall(28207); return ReplaceReturn1199;}
}

UV
Perl__to_fold_latin1(pTHX_ const U8 c, U8* p, STRLEN *lenp, const bool flags)
{
    /* Corresponds to to_lower_latin1(), <flags> is TRUE if to use full case
     * folding */

    SensorCall(28208);UV converted;

    PERL_ARGS_ASSERT__TO_FOLD_LATIN1;

    SensorCall(28214);if (c == MICRO_SIGN) {
	SensorCall(28209);converted = GREEK_SMALL_LETTER_MU;
    }
    else {/*159*/SensorCall(28210);if (flags && c == LATIN_SMALL_LETTER_SHARP_S) {
	SensorCall(28211);*(p)++ = 's';
	*p = 's';
	*lenp = 2;
	{UV  ReplaceReturn1198 = 's'; SensorCall(28212); return ReplaceReturn1198;}
    }
    else { /* In this range the fold of all other characters is their lower
              case */
	SensorCall(28213);converted = toLOWER_LATIN1(c);
    ;/*160*/}}

    SensorCall(28217);if (UNI_IS_INVARIANT(converted)) {
	SensorCall(28215);*p = (U8) converted;
	*lenp = 1;
    }
    else {
	SensorCall(28216);*(p)++ = UTF8_TWO_BYTE_HI(converted);
	*p = UTF8_TWO_BYTE_LO(converted);
	*lenp = 2;
    }

    {UV  ReplaceReturn1197 = converted; SensorCall(28218); return ReplaceReturn1197;}
}

UV
Perl__to_uni_fold_flags(pTHX_ UV c, U8* p, STRLEN *lenp, const bool flags)
{

    /* Not currently externally documented, and subject to change, <flags> is
     * TRUE iff full folding is to be used */

    PERL_ARGS_ASSERT__TO_UNI_FOLD_FLAGS;

    SensorCall(28219);if (c < 256) {
	{UV  ReplaceReturn1196 = _to_fold_latin1((U8) c, p, lenp, flags); SensorCall(28220); return ReplaceReturn1196;}
    }

    uvchr_to_utf8(p, c);
    {UV  ReplaceReturn1195 = CALL_FOLD_CASE(p, p, lenp, flags); SensorCall(28221); return ReplaceReturn1195;}
}

/* for now these all assume no locale info available for Unicode > 255; and
 * the corresponding macros in handy.h (like isALNUM_LC_uvchr) should have been
 * called instead, so that these don't get called for < 255 */

bool
Perl_is_uni_alnum_lc(pTHX_ UV c)
{
    {_Bool  ReplaceReturn1194 = is_uni_alnum(c); SensorCall(28222); return ReplaceReturn1194;}	/* XXX no locale support yet */
}

bool
Perl_is_uni_idfirst_lc(pTHX_ UV c)
{
    {_Bool  ReplaceReturn1193 = is_uni_idfirst(c); SensorCall(28223); return ReplaceReturn1193;}	/* XXX no locale support yet */
}

bool
Perl_is_uni_alpha_lc(pTHX_ UV c)
{
    {_Bool  ReplaceReturn1192 = is_uni_alpha(c); SensorCall(28224); return ReplaceReturn1192;}	/* XXX no locale support yet */
}

bool
Perl_is_uni_ascii_lc(pTHX_ UV c)
{
    {_Bool  ReplaceReturn1191 = is_uni_ascii(c); SensorCall(28225); return ReplaceReturn1191;}	/* XXX no locale support yet */
}

bool
Perl_is_uni_space_lc(pTHX_ UV c)
{
    {_Bool  ReplaceReturn1190 = is_uni_space(c); SensorCall(28226); return ReplaceReturn1190;}	/* XXX no locale support yet */
}

bool
Perl_is_uni_digit_lc(pTHX_ UV c)
{
    {_Bool  ReplaceReturn1189 = is_uni_digit(c); SensorCall(28227); return ReplaceReturn1189;}	/* XXX no locale support yet */
}

bool
Perl_is_uni_upper_lc(pTHX_ UV c)
{
    {_Bool  ReplaceReturn1188 = is_uni_upper(c); SensorCall(28228); return ReplaceReturn1188;}	/* XXX no locale support yet */
}

bool
Perl_is_uni_lower_lc(pTHX_ UV c)
{
    {_Bool  ReplaceReturn1187 = is_uni_lower(c); SensorCall(28229); return ReplaceReturn1187;}	/* XXX no locale support yet */
}

bool
Perl_is_uni_cntrl_lc(pTHX_ UV c)
{
    {_Bool  ReplaceReturn1186 = is_uni_cntrl(c); SensorCall(28230); return ReplaceReturn1186;}	/* XXX no locale support yet */
}

bool
Perl_is_uni_graph_lc(pTHX_ UV c)
{
    {_Bool  ReplaceReturn1185 = is_uni_graph(c); SensorCall(28231); return ReplaceReturn1185;}	/* XXX no locale support yet */
}

bool
Perl_is_uni_print_lc(pTHX_ UV c)
{
    {_Bool  ReplaceReturn1184 = is_uni_print(c); SensorCall(28232); return ReplaceReturn1184;}	/* XXX no locale support yet */
}

bool
Perl_is_uni_punct_lc(pTHX_ UV c)
{
    {_Bool  ReplaceReturn1183 = is_uni_punct(c); SensorCall(28233); return ReplaceReturn1183;}	/* XXX no locale support yet */
}

bool
Perl_is_uni_xdigit_lc(pTHX_ UV c)
{
    {_Bool  ReplaceReturn1182 = is_uni_xdigit(c); SensorCall(28234); return ReplaceReturn1182;}	/* XXX no locale support yet */
}

U32
Perl_to_uni_upper_lc(pTHX_ U32 c)
{
    /* XXX returns only the first character -- do not use XXX */
    /* XXX no locale support yet */
    SensorCall(28235);STRLEN len;
    U8 tmpbuf[UTF8_MAXBYTES_CASE+1];
    {U32  ReplaceReturn1181 = (U32)to_uni_upper(c, tmpbuf, &len); SensorCall(28236); return ReplaceReturn1181;}
}

U32
Perl_to_uni_title_lc(pTHX_ U32 c)
{
    /* XXX returns only the first character XXX -- do not use XXX */
    /* XXX no locale support yet */
    SensorCall(28237);STRLEN len;
    U8 tmpbuf[UTF8_MAXBYTES_CASE+1];
    {U32  ReplaceReturn1180 = (U32)to_uni_title(c, tmpbuf, &len); SensorCall(28238); return ReplaceReturn1180;}
}

U32
Perl_to_uni_lower_lc(pTHX_ U32 c)
{
    /* XXX returns only the first character -- do not use XXX */
    /* XXX no locale support yet */
    SensorCall(28239);STRLEN len;
    U8 tmpbuf[UTF8_MAXBYTES_CASE+1];
    {U32  ReplaceReturn1179 = (U32)to_uni_lower(c, tmpbuf, &len); SensorCall(28240); return ReplaceReturn1179;}
}

static bool
S_is_utf8_common(pTHX_ const U8 *const p, SV **swash,
		 const char *const swashname)
{
SensorCall(28241);    /* returns a boolean giving whether or not the UTF8-encoded character that
     * starts at <p> is in the swash indicated by <swashname>.  <swash>
     * contains a pointer to where the swash indicated by <swashname>
     * is to be stored; which this routine will do, so that future calls will
     * look at <*swash> and only generate a swash if it is not null
     *
     * Note that it is assumed that the buffer length of <p> is enough to
     * contain all the bytes that comprise the character.  Thus, <*p> should
     * have been checked before this call for mal-formedness enough to assure
     * that. */

    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_COMMON;

    /* The API should have included a length for the UTF-8 character in <p>,
     * but it doesn't.  We therefor assume that p has been validated at least
     * as far as there being enough bytes available in it to accommodate the
     * character without reading beyond the end, and pass that number on to the
     * validating routine */
    SensorCall(28242);if (!is_utf8_char_buf(p, p + UTF8SKIP(p)))
	return FALSE;
    SensorCall(28243);if (!*swash)
	*swash = swash_init("utf8", swashname, &PL_sv_undef, 1, 0);
    {_Bool  ReplaceReturn1178 = swash_fetch(*swash, p, TRUE) != 0; SensorCall(28244); return ReplaceReturn1178;}
}

bool
Perl_is_utf8_alnum(pTHX_ const U8 *p)
{
SensorCall(28245);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_ALNUM;

    /* NOTE: "IsWord", not "IsAlnum", since Alnum is a true
     * descendant of isalnum(3), in other words, it doesn't
     * contain the '_'. --jhi */
    {_Bool  ReplaceReturn1177 = is_utf8_common(p, &PL_utf8_alnum, "IsWord"); SensorCall(28246); return ReplaceReturn1177;}
}

bool
Perl_is_utf8_idfirst(pTHX_ const U8 *p) /* The naming is historical. */
{
SensorCall(28247);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_IDFIRST;

    SensorCall(28248);if (*p == '_')
	return TRUE;
    /* is_utf8_idstart would be more logical. */
    {_Bool  ReplaceReturn1176 = is_utf8_common(p, &PL_utf8_idstart, "IdStart"); SensorCall(28249); return ReplaceReturn1176;}
}

bool
Perl_is_utf8_xidfirst(pTHX_ const U8 *p) /* The naming is historical. */
{
SensorCall(28250);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_XIDFIRST;

    SensorCall(28251);if (*p == '_')
	return TRUE;
    /* is_utf8_idstart would be more logical. */
    {_Bool  ReplaceReturn1175 = is_utf8_common(p, &PL_utf8_xidstart, "XIdStart"); SensorCall(28252); return ReplaceReturn1175;}
}

bool
Perl__is_utf8__perl_idstart(pTHX_ const U8 *p)
{
SensorCall(28253);    dVAR;

    PERL_ARGS_ASSERT__IS_UTF8__PERL_IDSTART;

    {_Bool  ReplaceReturn1174 = is_utf8_common(p, &PL_utf8_perl_idstart, "_Perl_IDStart"); SensorCall(28254); return ReplaceReturn1174;}
}

bool
Perl_is_utf8_idcont(pTHX_ const U8 *p)
{
SensorCall(28255);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_IDCONT;

    {_Bool  ReplaceReturn1173 = is_utf8_common(p, &PL_utf8_idcont, "IdContinue"); SensorCall(28256); return ReplaceReturn1173;}
}

bool
Perl_is_utf8_xidcont(pTHX_ const U8 *p)
{
SensorCall(28257);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_XIDCONT;

    {_Bool  ReplaceReturn1172 = is_utf8_common(p, &PL_utf8_idcont, "XIdContinue"); SensorCall(28258); return ReplaceReturn1172;}
}

bool
Perl_is_utf8_alpha(pTHX_ const U8 *p)
{
SensorCall(28259);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_ALPHA;

    {_Bool  ReplaceReturn1171 = is_utf8_common(p, &PL_utf8_alpha, "IsAlpha"); SensorCall(28260); return ReplaceReturn1171;}
}

bool
Perl_is_utf8_ascii(pTHX_ const U8 *p)
{
SensorCall(28261);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_ASCII;

    /* ASCII characters are the same whether in utf8 or not.  So the macro
     * works on both utf8 and non-utf8 representations. */
    {_Bool  ReplaceReturn1170 = isASCII(*p); SensorCall(28262); return ReplaceReturn1170;}
}

bool
Perl_is_utf8_space(pTHX_ const U8 *p)
{
SensorCall(28263);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_SPACE;

    {_Bool  ReplaceReturn1169 = is_utf8_common(p, &PL_utf8_space, "IsXPerlSpace"); SensorCall(28264); return ReplaceReturn1169;}
}

bool
Perl_is_utf8_perl_space(pTHX_ const U8 *p)
{
SensorCall(28265);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_PERL_SPACE;

    /* Only true if is an ASCII space-like character, and ASCII is invariant
     * under utf8, so can just use the macro */
    {_Bool  ReplaceReturn1168 = isSPACE_A(*p); SensorCall(28266); return ReplaceReturn1168;}
}

bool
Perl_is_utf8_perl_word(pTHX_ const U8 *p)
{
SensorCall(28267);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_PERL_WORD;

    /* Only true if is an ASCII word character, and ASCII is invariant
     * under utf8, so can just use the macro */
    {_Bool  ReplaceReturn1167 = isWORDCHAR_A(*p); SensorCall(28268); return ReplaceReturn1167;}
}

bool
Perl_is_utf8_digit(pTHX_ const U8 *p)
{
SensorCall(28269);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_DIGIT;

    {_Bool  ReplaceReturn1166 = is_utf8_common(p, &PL_utf8_digit, "IsDigit"); SensorCall(28270); return ReplaceReturn1166;}
}

bool
Perl_is_utf8_posix_digit(pTHX_ const U8 *p)
{
SensorCall(28271);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_POSIX_DIGIT;

    /* Only true if is an ASCII digit character, and ASCII is invariant
     * under utf8, so can just use the macro */
    {_Bool  ReplaceReturn1165 = isDIGIT_A(*p); SensorCall(28272); return ReplaceReturn1165;}
}

bool
Perl_is_utf8_upper(pTHX_ const U8 *p)
{
SensorCall(28273);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_UPPER;

    {_Bool  ReplaceReturn1164 = is_utf8_common(p, &PL_utf8_upper, "IsUppercase"); SensorCall(28274); return ReplaceReturn1164;}
}

bool
Perl_is_utf8_lower(pTHX_ const U8 *p)
{
SensorCall(28275);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_LOWER;

    {_Bool  ReplaceReturn1163 = is_utf8_common(p, &PL_utf8_lower, "IsLowercase"); SensorCall(28276); return ReplaceReturn1163;}
}

bool
Perl_is_utf8_cntrl(pTHX_ const U8 *p)
{
SensorCall(28277);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_CNTRL;

    SensorCall(28279);if (isASCII(*p)) {
	{_Bool  ReplaceReturn1162 = isCNTRL_A(*p); SensorCall(28278); return ReplaceReturn1162;}
    }

    /* All controls are in Latin1 */
    SensorCall(28281);if (! UTF8_IS_DOWNGRADEABLE_START(*p)) {
	{_Bool  ReplaceReturn1161 = 0; SensorCall(28280); return ReplaceReturn1161;}
    }
    {_Bool  ReplaceReturn1160 = isCNTRL_L1(TWO_BYTE_UTF8_TO_UNI(*p, *(p+1))); SensorCall(28282); return ReplaceReturn1160;}
}

bool
Perl_is_utf8_graph(pTHX_ const U8 *p)
{
SensorCall(28283);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_GRAPH;

    {_Bool  ReplaceReturn1159 = is_utf8_common(p, &PL_utf8_graph, "IsGraph"); SensorCall(28284); return ReplaceReturn1159;}
}

bool
Perl_is_utf8_print(pTHX_ const U8 *p)
{
SensorCall(28285);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_PRINT;

    {_Bool  ReplaceReturn1158 = is_utf8_common(p, &PL_utf8_print, "IsPrint"); SensorCall(28286); return ReplaceReturn1158;}
}

bool
Perl_is_utf8_punct(pTHX_ const U8 *p)
{
SensorCall(28287);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_PUNCT;

    {_Bool  ReplaceReturn1157 = is_utf8_common(p, &PL_utf8_punct, "IsPunct"); SensorCall(28288); return ReplaceReturn1157;}
}

bool
Perl_is_utf8_xdigit(pTHX_ const U8 *p)
{
SensorCall(28289);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_XDIGIT;

    {_Bool  ReplaceReturn1156 = is_utf8_common(p, &PL_utf8_xdigit, "IsXDigit"); SensorCall(28290); return ReplaceReturn1156;}
}

bool
Perl_is_utf8_mark(pTHX_ const U8 *p)
{
SensorCall(28291);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_MARK;

    {_Bool  ReplaceReturn1155 = is_utf8_common(p, &PL_utf8_mark, "IsM"); SensorCall(28292); return ReplaceReturn1155;}
}

bool
Perl_is_utf8_X_begin(pTHX_ const U8 *p)
{
SensorCall(28293);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_X_BEGIN;

    {_Bool  ReplaceReturn1154 = is_utf8_common(p, &PL_utf8_X_begin, "_X_Begin"); SensorCall(28294); return ReplaceReturn1154;}
}

bool
Perl_is_utf8_X_extend(pTHX_ const U8 *p)
{
SensorCall(28295);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_X_EXTEND;

    {_Bool  ReplaceReturn1153 = is_utf8_common(p, &PL_utf8_X_extend, "_X_Extend"); SensorCall(28296); return ReplaceReturn1153;}
}

bool
Perl_is_utf8_X_prepend(pTHX_ const U8 *p)
{
SensorCall(28297);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_X_PREPEND;

    {_Bool  ReplaceReturn1152 = is_utf8_common(p, &PL_utf8_X_prepend, "GCB=Prepend"); SensorCall(28298); return ReplaceReturn1152;}
}

bool
Perl_is_utf8_X_non_hangul(pTHX_ const U8 *p)
{
SensorCall(28299);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_X_NON_HANGUL;

    {_Bool  ReplaceReturn1151 = is_utf8_common(p, &PL_utf8_X_non_hangul, "HST=Not_Applicable"); SensorCall(28300); return ReplaceReturn1151;}
}

bool
Perl_is_utf8_X_L(pTHX_ const U8 *p)
{
SensorCall(28301);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_X_L;

    {_Bool  ReplaceReturn1150 = is_utf8_common(p, &PL_utf8_X_L, "GCB=L"); SensorCall(28302); return ReplaceReturn1150;}
}

bool
Perl_is_utf8_X_LV(pTHX_ const U8 *p)
{
SensorCall(28303);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_X_LV;

    {_Bool  ReplaceReturn1149 = is_utf8_common(p, &PL_utf8_X_LV, "GCB=LV"); SensorCall(28304); return ReplaceReturn1149;}
}

bool
Perl_is_utf8_X_LVT(pTHX_ const U8 *p)
{
SensorCall(28305);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_X_LVT;

    {_Bool  ReplaceReturn1148 = is_utf8_common(p, &PL_utf8_X_LVT, "GCB=LVT"); SensorCall(28306); return ReplaceReturn1148;}
}

bool
Perl_is_utf8_X_T(pTHX_ const U8 *p)
{
SensorCall(28307);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_X_T;

    {_Bool  ReplaceReturn1147 = is_utf8_common(p, &PL_utf8_X_T, "GCB=T"); SensorCall(28308); return ReplaceReturn1147;}
}

bool
Perl_is_utf8_X_V(pTHX_ const U8 *p)
{
SensorCall(28309);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_X_V;

    {_Bool  ReplaceReturn1146 = is_utf8_common(p, &PL_utf8_X_V, "GCB=V"); SensorCall(28310); return ReplaceReturn1146;}
}

bool
Perl_is_utf8_X_LV_LVT_V(pTHX_ const U8 *p)
{
SensorCall(28311);    dVAR;

    PERL_ARGS_ASSERT_IS_UTF8_X_LV_LVT_V;

    {_Bool  ReplaceReturn1145 = is_utf8_common(p, &PL_utf8_X_LV_LVT_V, "_X_LV_LVT_V"); SensorCall(28312); return ReplaceReturn1145;}
}

bool
Perl__is_utf8_quotemeta(pTHX_ const U8 *p)
{
SensorCall(28313);    /* For exclusive use of pp_quotemeta() */

    dVAR;

    PERL_ARGS_ASSERT__IS_UTF8_QUOTEMETA;

    {_Bool  ReplaceReturn1144 = is_utf8_common(p, &PL_utf8_quotemeta, "_Perl_Quotemeta"); SensorCall(28314); return ReplaceReturn1144;}
}

/*
=for apidoc to_utf8_case

The C<p> contains the pointer to the UTF-8 string encoding
the character that is being converted.  This routine assumes that the character
at C<p> is well-formed.

The C<ustrp> is a pointer to the character buffer to put the
conversion result to.  The C<lenp> is a pointer to the length
of the result.

The C<swashp> is a pointer to the swash to use.

Both the special and normal mappings are stored in F<lib/unicore/To/Foo.pl>,
and loaded by SWASHNEW, using F<lib/utf8_heavy.pl>.  The C<special> (usually,
but not always, a multicharacter mapping), is tried first.

The C<special> is a string like "utf8::ToSpecLower", which means the
hash %utf8::ToSpecLower.  The access to the hash is through
Perl_to_utf8_case().

The C<normal> is a string like "ToLower" which means the swash
%utf8::ToLower.

=cut */

UV
Perl_to_utf8_case(pTHX_ const U8 *p, U8* ustrp, STRLEN *lenp,
			SV **swashp, const char *normal, const char *special)
{
SensorCall(28315);    dVAR;
    U8 tmpbuf[UTF8_MAXBYTES_CASE+1];
    STRLEN len = 0;
    const UV uv0 = valid_utf8_to_uvchr(p, NULL);
    /* The NATIVE_TO_UNI() and UNI_TO_NATIVE() mappings
     * are necessary in EBCDIC, they are redundant no-ops
     * in ASCII-ish platforms, and hopefully optimized away. */
    const UV uv1 = NATIVE_TO_UNI(uv0);

    PERL_ARGS_ASSERT_TO_UTF8_CASE;

    /* Note that swash_fetch() doesn't output warnings for these because it
     * assumes we will */
    SensorCall(28322);if (uv1 >= UNICODE_SURROGATE_FIRST) {
	SensorCall(28316);if (uv1 <= UNICODE_SURROGATE_LAST) {
	    SensorCall(28317);if (ckWARN_d(WARN_SURROGATE)) {
		SensorCall(28318);const char* desc = (PL_op) ? OP_DESC(PL_op) : normal;
		Perl_warner(aTHX_ packWARN(WARN_SURROGATE),
		    "Operation \"%s\" returns its argument for UTF-16 surrogate U+%04"UVXf"", desc, uv1);
	    }
	}
	else {/*61*/SensorCall(28319);if (UNICODE_IS_SUPER(uv1)) {
	    SensorCall(28320);if (ckWARN_d(WARN_NON_UNICODE)) {
		SensorCall(28321);const char* desc = (PL_op) ? OP_DESC(PL_op) : normal;
		Perl_warner(aTHX_ packWARN(WARN_NON_UNICODE),
		    "Operation \"%s\" returns its argument for non-Unicode code point 0x%04"UVXf"", desc, uv1);
	    }
	;/*62*/}}

	/* Note that non-characters are perfectly legal, so no warning should
	 * be given */
    }

    uvuni_to_utf8(tmpbuf, uv1);

    SensorCall(28323);if (!*swashp) /* load on-demand */
         *swashp = swash_init("utf8", normal, &PL_sv_undef, 4, 0);

    SensorCall(28329);if (special) {
         /* It might be "special" (sometimes, but not always,
	  * a multicharacter mapping) */
	 SensorCall(28324);HV * const hv = get_hv(special, 0);
	 SV **svp;

	 SensorCall(28328);if (hv &&
	     (svp = hv_fetch(hv, (const char*)tmpbuf, UNISKIP(uv1), FALSE)) &&
	     (*svp)) {
	     SensorCall(28325);const char *s;

	      s = SvPV_const(*svp, len);
	      SensorCall(28327);if (len == 1)
		   {/*63*/SensorCall(28326);len = uvuni_to_utf8(ustrp, NATIVE_TO_UNI(*(U8*)s)) - ustrp;/*64*/}
	      else {
#ifdef EBCDIC
		   /* If we have EBCDIC we need to remap the characters
		    * since any characters in the low 256 are Unicode
		    * code points, not EBCDIC. */
		   U8 *t = (U8*)s, *tend = t + len, *d;
		
		   d = tmpbuf;
		   if (SvUTF8(*svp)) {
			STRLEN tlen = 0;
			
			while (t < tend) {
			     const UV c = utf8_to_uvchr_buf(t, tend, &tlen);
			     if (tlen > 0) {
				  d = uvchr_to_utf8(d, UNI_TO_NATIVE(c));
				  t += tlen;
			     }
			     else
				  break;
			}
		   }
		   else {
			while (t < tend) {
			     d = uvchr_to_utf8(d, UNI_TO_NATIVE(*t));
			     t++;
			}
		   }
		   len = d - tmpbuf;
		   Copy(tmpbuf, ustrp, len, U8);
#else
		   Copy(s, ustrp, len, U8);
#endif
	      }
	 }
    }

    SensorCall(28333);if (!len && *swashp) {
	SensorCall(28330);const UV uv2 = swash_fetch(*swashp, tmpbuf, TRUE);

	 SensorCall(28332);if (uv2) {
	      /* It was "normal" (a single character mapping). */
	      SensorCall(28331);const UV uv3 = UNI_TO_NATIVE(uv2);
	      len = uvchr_to_utf8(ustrp, uv3) - ustrp;
	 }
    }

    SensorCall(28335);if (!len) /* Neither: just copy.  In other words, there was no mapping
		 defined, which means that the code point maps to itself */
	 {/*65*/SensorCall(28334);len = uvchr_to_utf8(ustrp, uv0) - ustrp;/*66*/}

    SensorCall(28337);if (lenp)
	 {/*67*/SensorCall(28336);*lenp = len;/*68*/}

    {UV  ReplaceReturn1143 = len ? valid_utf8_to_uvchr(ustrp, 0) : 0; SensorCall(28338); return ReplaceReturn1143;}
}

STATIC UV
S_check_locale_boundary_crossing(pTHX_ const U8* const p, const UV result, U8* const ustrp, STRLEN *lenp)
{
    /* This is called when changing the case of a utf8-encoded character above
     * the Latin1 range, and the operation is in locale.  If the result
     * contains a character that crosses the 255/256 boundary, disallow the
     * change, and return the original code point.  See L<perlfunc/lc> for why;
     *
     * p	points to the original string whose case was changed; assumed
     *          by this routine to be well-formed
     * result	the code point of the first character in the changed-case string
     * ustrp	points to the changed-case string (<result> represents its first char)
     * lenp	points to the length of <ustrp> */

    SensorCall(28339);UV original;    /* To store the first code point of <p> */

    PERL_ARGS_ASSERT_CHECK_LOCALE_BOUNDARY_CROSSING;

    assert(! UTF8_IS_INVARIANT(*p) && ! UTF8_IS_DOWNGRADEABLE_START(*p));

    /* We know immediately if the first character in the string crosses the
     * boundary, so can skip */
    SensorCall(28346);if (result > 255) {

	/* Look at every character in the result; if any cross the
	* boundary, the whole thing is disallowed */
	SensorCall(28340);U8* s = ustrp + UTF8SKIP(ustrp);
	U8* e = ustrp + *lenp;
	SensorCall(28344);while (s < e) {
	    SensorCall(28341);if (UTF8_IS_INVARIANT(*s) || UTF8_IS_DOWNGRADEABLE_START(*s))
	    {
		SensorCall(28342);goto bad_crossing;
	    }
	    SensorCall(28343);s += UTF8SKIP(s);
	}

	/* Here, no characters crossed, result is ok as-is */
	{UV  ReplaceReturn1142 = result; SensorCall(28345); return ReplaceReturn1142;}
    }

bad_crossing:

    /* Failed, have to return the original */
    SensorCall(28347);original = valid_utf8_to_uvchr(p, lenp);
    Copy(p, ustrp, *lenp, char);
    {UV  ReplaceReturn1141 = original; SensorCall(28348); return ReplaceReturn1141;}
}

/*
=for apidoc to_utf8_upper

Convert the UTF-8 encoded character at C<p> to its uppercase version and
store that in UTF-8 in C<ustrp> and its length in bytes in C<lenp>.  Note
that the ustrp needs to be at least UTF8_MAXBYTES_CASE+1 bytes since
the uppercase version may be longer than the original character.

The first character of the uppercased version is returned
(but note, as explained above, that there may be more.)

The character at C<p> is assumed by this routine to be well-formed.

=cut */

/* Not currently externally documented, and subject to change:
 * <flags> is set iff locale semantics are to be used for code points < 256
 * <tainted_ptr> if non-null, *tainted_ptr will be set TRUE iff locale rules
 *		 were used in the calculation; otherwise unchanged. */

UV
Perl__to_utf8_upper_flags(pTHX_ const U8 *p, U8* ustrp, STRLEN *lenp, const bool flags, bool* tainted_ptr)
{
SensorCall(28349);    dVAR;

    UV result;

    PERL_ARGS_ASSERT__TO_UTF8_UPPER_FLAGS;

    SensorCall(28361);if (UTF8_IS_INVARIANT(*p)) {
	SensorCall(28350);if (flags) {
	    SensorCall(28351);result = toUPPER_LC(*p);
	}
	else {
	    {UV  ReplaceReturn1140 = _to_upper_title_latin1(*p, ustrp, lenp, 'S'); SensorCall(28352); return ReplaceReturn1140;}
	}
    }
    else {/*7*/SensorCall(28353);if UTF8_IS_DOWNGRADEABLE_START(*p) {
	SensorCall(28354);if (flags) {
	    SensorCall(28355);result = toUPPER_LC(TWO_BYTE_UTF8_TO_UNI(*p, *(p+1)));
	}
	else {
	    {UV  ReplaceReturn1139 = _to_upper_title_latin1(TWO_BYTE_UTF8_TO_UNI(*p, *(p+1)),
				          ustrp, lenp, 'S'); SensorCall(28356); return ReplaceReturn1139;}
	}
    }
    else {  /* utf8, ord above 255 */
	SensorCall(28357);result = CALL_UPPER_CASE(p, ustrp, lenp);

	SensorCall(28359);if (flags) {
	    SensorCall(28358);result = check_locale_boundary_crossing(p, result, ustrp, lenp);
	}
	{UV  ReplaceReturn1138 = result; SensorCall(28360); return ReplaceReturn1138;}
    ;/*8*/}}

    /* Here, used locale rules.  Convert back to utf8 */
    SensorCall(28364);if (UTF8_IS_INVARIANT(result)) {
	SensorCall(28362);*ustrp = (U8) result;
	*lenp = 1;
    }
    else {
	SensorCall(28363);*ustrp = UTF8_EIGHT_BIT_HI(result);
	*(ustrp + 1) = UTF8_EIGHT_BIT_LO(result);
	*lenp = 2;
    }

    SensorCall(28366);if (tainted_ptr) {
	SensorCall(28365);*tainted_ptr = TRUE;
    }
    {UV  ReplaceReturn1137 = result; SensorCall(28367); return ReplaceReturn1137;}
}

/*
=for apidoc to_utf8_title

Convert the UTF-8 encoded character at C<p> to its titlecase version and
store that in UTF-8 in C<ustrp> and its length in bytes in C<lenp>.  Note
that the C<ustrp> needs to be at least UTF8_MAXBYTES_CASE+1 bytes since the
titlecase version may be longer than the original character.

The first character of the titlecased version is returned
(but note, as explained above, that there may be more.)

The character at C<p> is assumed by this routine to be well-formed.

=cut */

/* Not currently externally documented, and subject to change:
 * <flags> is set iff locale semantics are to be used for code points < 256
 *	   Since titlecase is not defined in POSIX, uppercase is used instead
 *	   for these/
 * <tainted_ptr> if non-null, *tainted_ptr will be set TRUE iff locale rules
 *		 were used in the calculation; otherwise unchanged. */

UV
Perl__to_utf8_title_flags(pTHX_ const U8 *p, U8* ustrp, STRLEN *lenp, const bool flags, bool* tainted_ptr)
{
SensorCall(28368);    dVAR;

    UV result;

    PERL_ARGS_ASSERT__TO_UTF8_TITLE_FLAGS;

    SensorCall(28380);if (UTF8_IS_INVARIANT(*p)) {
	SensorCall(28369);if (flags) {
	    SensorCall(28370);result = toUPPER_LC(*p);
	}
	else {
	    {UV  ReplaceReturn1136 = _to_upper_title_latin1(*p, ustrp, lenp, 's'); SensorCall(28371); return ReplaceReturn1136;}
	}
    }
    else {/*5*/SensorCall(28372);if UTF8_IS_DOWNGRADEABLE_START(*p) {
	SensorCall(28373);if (flags) {
	    SensorCall(28374);result = toUPPER_LC(TWO_BYTE_UTF8_TO_UNI(*p, *(p+1)));
	}
	else {
	    {UV  ReplaceReturn1135 = _to_upper_title_latin1(TWO_BYTE_UTF8_TO_UNI(*p, *(p+1)),
				          ustrp, lenp, 's'); SensorCall(28375); return ReplaceReturn1135;}
	}
    }
    else {  /* utf8, ord above 255 */
	SensorCall(28376);result = CALL_TITLE_CASE(p, ustrp, lenp);

	SensorCall(28378);if (flags) {
	    SensorCall(28377);result = check_locale_boundary_crossing(p, result, ustrp, lenp);
	}
	{UV  ReplaceReturn1134 = result; SensorCall(28379); return ReplaceReturn1134;}
    ;/*6*/}}

    /* Here, used locale rules.  Convert back to utf8 */
    SensorCall(28383);if (UTF8_IS_INVARIANT(result)) {
	SensorCall(28381);*ustrp = (U8) result;
	*lenp = 1;
    }
    else {
	SensorCall(28382);*ustrp = UTF8_EIGHT_BIT_HI(result);
	*(ustrp + 1) = UTF8_EIGHT_BIT_LO(result);
	*lenp = 2;
    }

    SensorCall(28385);if (tainted_ptr) {
	SensorCall(28384);*tainted_ptr = TRUE;
    }
    {UV  ReplaceReturn1133 = result; SensorCall(28386); return ReplaceReturn1133;}
}

/*
=for apidoc to_utf8_lower

Convert the UTF-8 encoded character at C<p> to its lowercase version and
store that in UTF-8 in ustrp and its length in bytes in C<lenp>.  Note
that the C<ustrp> needs to be at least UTF8_MAXBYTES_CASE+1 bytes since the
lowercase version may be longer than the original character.

The first character of the lowercased version is returned
(but note, as explained above, that there may be more.)

The character at C<p> is assumed by this routine to be well-formed.

=cut */

/* Not currently externally documented, and subject to change:
 * <flags> is set iff locale semantics are to be used for code points < 256
 * <tainted_ptr> if non-null, *tainted_ptr will be set TRUE iff locale rules
 *		 were used in the calculation; otherwise unchanged. */

UV
Perl__to_utf8_lower_flags(pTHX_ const U8 *p, U8* ustrp, STRLEN *lenp, const bool flags, bool* tainted_ptr)
{
    SensorCall(28387);UV result;

    dVAR;

    PERL_ARGS_ASSERT__TO_UTF8_LOWER_FLAGS;

    SensorCall(28399);if (UTF8_IS_INVARIANT(*p)) {
	SensorCall(28388);if (flags) {
	    SensorCall(28389);result = toLOWER_LC(*p);
	}
	else {
	    {UV  ReplaceReturn1132 = to_lower_latin1(*p, ustrp, lenp); SensorCall(28390); return ReplaceReturn1132;}
	}
    }
    else {/*3*/SensorCall(28391);if UTF8_IS_DOWNGRADEABLE_START(*p) {
	SensorCall(28392);if (flags) {
	    SensorCall(28393);result = toLOWER_LC(TWO_BYTE_UTF8_TO_UNI(*p, *(p+1)));
	}
	else {
	    {UV  ReplaceReturn1131 = to_lower_latin1(TWO_BYTE_UTF8_TO_UNI(*p, *(p+1)),
		                   ustrp, lenp); SensorCall(28394); return ReplaceReturn1131;}
	}
    }
    else {  /* utf8, ord above 255 */
	SensorCall(28395);result = CALL_LOWER_CASE(p, ustrp, lenp);

	SensorCall(28397);if (flags) {
	    SensorCall(28396);result = check_locale_boundary_crossing(p, result, ustrp, lenp);
	}

	{UV  ReplaceReturn1130 = result; SensorCall(28398); return ReplaceReturn1130;}
    ;/*4*/}}

    /* Here, used locale rules.  Convert back to utf8 */
    SensorCall(28402);if (UTF8_IS_INVARIANT(result)) {
	SensorCall(28400);*ustrp = (U8) result;
	*lenp = 1;
    }
    else {
	SensorCall(28401);*ustrp = UTF8_EIGHT_BIT_HI(result);
	*(ustrp + 1) = UTF8_EIGHT_BIT_LO(result);
	*lenp = 2;
    }

    SensorCall(28404);if (tainted_ptr) {
	SensorCall(28403);*tainted_ptr = TRUE;
    }
    {UV  ReplaceReturn1129 = result; SensorCall(28405); return ReplaceReturn1129;}
}

/*
=for apidoc to_utf8_fold

Convert the UTF-8 encoded character at C<p> to its foldcase version and
store that in UTF-8 in C<ustrp> and its length in bytes in C<lenp>.  Note
that the C<ustrp> needs to be at least UTF8_MAXBYTES_CASE+1 bytes since the
foldcase version may be longer than the original character (up to
three characters).

The first character of the foldcased version is returned
(but note, as explained above, that there may be more.)

The character at C<p> is assumed by this routine to be well-formed.

=cut */

/* Not currently externally documented, and subject to change,
 * in <flags>
 *	bit FOLD_FLAGS_LOCALE is set iff locale semantics are to be used for code
 *			      points < 256.  Since foldcase is not defined in
 *			      POSIX, lowercase is used instead
 *      bit FOLD_FLAGS_FULL   is set iff full case folds are to be used;
 *			      otherwise simple folds
 * <tainted_ptr> if non-null, *tainted_ptr will be set TRUE iff locale rules
 *		 were used in the calculation; otherwise unchanged. */

UV
Perl__to_utf8_fold_flags(pTHX_ const U8 *p, U8* ustrp, STRLEN *lenp, U8 flags, bool* tainted_ptr)
{
SensorCall(28406);    dVAR;

    UV result;

    PERL_ARGS_ASSERT__TO_UTF8_FOLD_FLAGS;

    SensorCall(28418);if (UTF8_IS_INVARIANT(*p)) {
	SensorCall(28407);if (flags & FOLD_FLAGS_LOCALE) {
	    SensorCall(28408);result = toLOWER_LC(*p);
	}
	else {
	    {UV  ReplaceReturn1128 = _to_fold_latin1(*p, ustrp, lenp,
		                   cBOOL(flags & FOLD_FLAGS_FULL)); SensorCall(28409); return ReplaceReturn1128;}
	}
    }
    else {/*1*/SensorCall(28410);if UTF8_IS_DOWNGRADEABLE_START(*p) {
	SensorCall(28411);if (flags & FOLD_FLAGS_LOCALE) {
	    SensorCall(28412);result = toLOWER_LC(TWO_BYTE_UTF8_TO_UNI(*p, *(p+1)));
	}
	else {
	    {UV  ReplaceReturn1127 = _to_fold_latin1(TWO_BYTE_UTF8_TO_UNI(*p, *(p+1)),
		                   ustrp, lenp, cBOOL(flags & FOLD_FLAGS_FULL)); SensorCall(28413); return ReplaceReturn1127;}
	}
    }
    else {  /* utf8, ord above 255 */
	SensorCall(28414);result = CALL_FOLD_CASE(p, ustrp, lenp, flags);

	SensorCall(28416);if ((flags & FOLD_FLAGS_LOCALE)) {
	    SensorCall(28415);result = check_locale_boundary_crossing(p, result, ustrp, lenp);
	}

	{UV  ReplaceReturn1126 = result; SensorCall(28417); return ReplaceReturn1126;}
    ;/*2*/}}

    /* Here, used locale rules.  Convert back to utf8 */
    SensorCall(28421);if (UTF8_IS_INVARIANT(result)) {
	SensorCall(28419);*ustrp = (U8) result;
	*lenp = 1;
    }
    else {
	SensorCall(28420);*ustrp = UTF8_EIGHT_BIT_HI(result);
	*(ustrp + 1) = UTF8_EIGHT_BIT_LO(result);
	*lenp = 2;
    }

    SensorCall(28423);if (tainted_ptr) {
	SensorCall(28422);*tainted_ptr = TRUE;
    }
    {UV  ReplaceReturn1125 = result; SensorCall(28424); return ReplaceReturn1125;}
}

/* Note:
 * Returns a "swash" which is a hash described in utf8.c:Perl_swash_fetch().
 * C<pkg> is a pointer to a package name for SWASHNEW, should be "utf8".
 * For other parameters, see utf8::SWASHNEW in lib/utf8_heavy.pl.
 */

SV*
Perl_swash_init(pTHX_ const char* pkg, const char* name, SV *listsv, I32 minbits, I32 none)
{
    PERL_ARGS_ASSERT_SWASH_INIT;

    /* Returns a copy of a swash initiated by the called function.  This is the
     * public interface, and returning a copy prevents others from doing
     * mischief on the original */

    {SV * ReplaceReturn1124 = newSVsv(_core_swash_init(pkg, name, listsv, minbits, none, FALSE, NULL, FALSE)); SensorCall(28425); return ReplaceReturn1124;}
}

SV*
Perl__core_swash_init(pTHX_ const char* pkg, const char* name, SV *listsv, I32 minbits, I32 none, bool return_if_undef, SV* invlist, bool passed_in_invlist_has_user_defined_property)
{
SensorCall(28426);    /* Initialize and return a swash, creating it if necessary.  It does this
     * by calling utf8_heavy.pl in the general case.
     *
     * This interface should only be used by functions that won't destroy or
     * adversely change the swash, as doing so affects all other uses of the
     * swash in the program; the general public should use 'Perl_swash_init'
     * instead.
     *
     * pkg  is the name of the package that <name> should be in.
     * name is the name of the swash to find.  Typically it is a Unicode
     *	    property name, including user-defined ones
     * listsv is a string to initialize the swash with.  It must be of the form
     *	    documented as the subroutine return value in
     *	    L<perlunicode/User-Defined Character Properties>
     * minbits is the number of bits required to represent each data element.
     *	    It is '1' for binary properties.
     * none I (khw) do not understand this one, but it is used only in tr///.
     * return_if_undef is TRUE if the routine shouldn't croak if it can't find
     *	    the requested property
     * invlist is an inversion list to initialize the swash with (or NULL)
     * has_user_defined_property is TRUE if <invlist> has some component that
     *      came from a user-defined property
     *
     * Thus there are three possible inputs to find the swash: <name>,
     * <listsv>, and <invlist>.  At least one must be specified.  The result
     * will be the union of the specified ones, although <listsv>'s various
     * actions can intersect, etc. what <name> gives.
     *
     * <invlist> is only valid for binary properties */

    dVAR;
    SV* retval = &PL_sv_undef;

    assert(listsv != &PL_sv_undef || strNE(name, "") || invlist);
    assert(! invlist || minbits == 1);

    /* If data was passed in to go out to utf8_heavy to find the swash of, do
     * so */
    SensorCall(28443);if (listsv != &PL_sv_undef || strNE(name, "")) {
	dSP;
	SensorCall(28427);const size_t pkg_len = strlen(pkg);
	const size_t name_len = strlen(name);
	HV * const stash = gv_stashpvn(pkg, pkg_len, 0);
	SV* errsv_save;
	GV *method;

	PERL_ARGS_ASSERT__CORE_SWASH_INIT;

	PUSHSTACKi(PERLSI_MAGIC);
	ENTER;
	SAVEHINTS();
	save_re_context();
	SensorCall(28428);if (PL_parser && PL_parser->error_count)
	    SAVEI8(PL_parser->error_count), PL_parser->error_count = 0;
	SensorCall(28429);method = gv_fetchmeth(stash, "SWASHNEW", 8, -1);
	SensorCall(28432);if (!method) {	/* demand load utf8 */
	    ENTER;
	    SensorCall(28430);errsv_save = newSVsv(ERRSV);
	    /* It is assumed that callers of this routine are not passing in
	     * any user derived data.  */
	    /* Need to do this after save_re_context() as it will set
	     * PL_tainted to 1 while saving $1 etc (see the code after getrx:
	     * in Perl_magic_get).  Even line to create errsv_save can turn on
	     * PL_tainted.  */
	    SAVEBOOL(PL_tainted);
	    PL_tainted = 0;
	    Perl_load_module(aTHX_ PERL_LOADMOD_NOIMPORT, newSVpvn(pkg,pkg_len),
			     NULL);
	    SensorCall(28431);if (!SvTRUE(ERRSV))
		sv_setsv(ERRSV, errsv_save);
	    SvREFCNT_dec(errsv_save);
	    LEAVE;
	}
	SPAGAIN;
	PUSHMARK(SP);
	EXTEND(SP,5);
	mPUSHp(pkg, pkg_len);
	mPUSHp(name, name_len);
	PUSHs(listsv);
	mPUSHi(minbits);
	mPUSHi(none);
	PUTBACK;
	SensorCall(28433);errsv_save = newSVsv(ERRSV);
	/* If we already have a pointer to the method, no need to use
	 * call_method() to repeat the lookup.  */
	SensorCall(28435);if (method ? call_sv(MUTABLE_SV(method), G_SCALAR)
	    : call_sv(newSVpvs_flags("SWASHNEW", SVs_TEMP), G_SCALAR | G_METHOD))
	{
	    SensorCall(28434);retval = *PL_stack_sp--;
	    SvREFCNT_inc(retval);
	}
	SensorCall(28436);if (!SvTRUE(ERRSV))
	    sv_setsv(ERRSV, errsv_save);
	SvREFCNT_dec(errsv_save);
	LEAVE;
	POPSTACK;
	SensorCall(28437);if (IN_PERL_COMPILETIME) {
	    CopHINTS_set(PL_curcop, PL_hints);
	}
	SensorCall(28442);if (!SvROK(retval) || SvTYPE(SvRV(retval)) != SVt_PVHV) {
	    SensorCall(28438);if (SvPOK(retval))

		/* If caller wants to handle missing properties, let them */
		{/*103*/SensorCall(28439);if (return_if_undef) {
		    {SV * ReplaceReturn1123 = NULL; SensorCall(28440); return ReplaceReturn1123;}
		;/*104*/}}
		SensorCall(28441);Perl_croak(aTHX_
			   "Can't find Unicode property definition \"%"SVf"\"",
			   SVfARG(retval));
	    Perl_croak(aTHX_ "SWASHNEW didn't return an HV ref");
	}
    } /* End of calling the module to find the swash */

    /* Make sure there is an inversion list for binary properties */
    SensorCall(28460);if (minbits == 1) {
	SensorCall(28444);SV** swash_invlistsvp = NULL;
	SV* swash_invlist = NULL;
	bool invlist_in_swash_is_valid = FALSE;
	HV* swash_hv = NULL;

        /* If this operation fetched a swash, get its already existing
         * inversion list or create one for it */
	SensorCall(28449);if (retval != &PL_sv_undef) {
	    SensorCall(28445);swash_hv = MUTABLE_HV(SvRV(retval));

	    swash_invlistsvp = hv_fetchs(swash_hv, "INVLIST", FALSE);
	    SensorCall(28448);if (swash_invlistsvp) {
		SensorCall(28446);swash_invlist = *swash_invlistsvp;
		invlist_in_swash_is_valid = TRUE;
	    }
	    else {
		SensorCall(28447);swash_invlist = _swash_to_invlist(retval);
	    }
	}

	/* If an inversion list was passed in, have to include it */
	SensorCall(28456);if (invlist) {

            /* Any fetched swash will by now have an inversion list in it;
             * otherwise <swash_invlist>  will be NULL, indicating that we
             * didn't fetch a swash */
	    SensorCall(28450);if (swash_invlist) {

		/* Add the passed-in inversion list, which invalidates the one
		 * already stored in the swash */
		SensorCall(28451);invlist_in_swash_is_valid = FALSE;
		_invlist_union(invlist, swash_invlist, &swash_invlist);
	    }
	    else {

		/* Here, there is no swash already.  Set up a minimal one */
		SensorCall(28452);swash_hv = newHV();
		retval = newRV_inc(MUTABLE_SV(swash_hv));
		swash_invlist = invlist;
	    }

            SensorCall(28455);if (passed_in_invlist_has_user_defined_property) {
                SensorCall(28453);if (! hv_stores(swash_hv, "USER_DEFINED", newSVuv(1))) {
                    SensorCall(28454);Perl_croak(aTHX_ "panic: hv_store() unexpectedly failed");
                }
            }
	}

        /* Here, we have computed the union of all the passed-in data.  It may
         * be that there was an inversion list in the swash which didn't get
         * touched; otherwise save the one computed one */
	SensorCall(28459);if (! invlist_in_swash_is_valid) {
	    SensorCall(28457);if (! hv_stores(MUTABLE_HV(SvRV(retval)), "INVLIST", swash_invlist))
            {
		SensorCall(28458);Perl_croak(aTHX_ "panic: hv_store() unexpectedly failed");
	    }
	}
    }

    {SV * ReplaceReturn1122 = retval; SensorCall(28461); return ReplaceReturn1122;}
}


/* This API is wrong for special case conversions since we may need to
 * return several Unicode characters for a single Unicode character
 * (see lib/unicore/SpecCase.txt) The SWASHGET in lib/utf8_heavy.pl is
 * the lower-level routine, and it is similarly broken for returning
 * multiple values.  --jhi
 * For those, you should use to_utf8_case() instead */
/* Now SWASHGET is recasted into S_swatch_get in this file. */

/* Note:
 * Returns the value of property/mapping C<swash> for the first character
 * of the string C<ptr>. If C<do_utf8> is true, the string C<ptr> is
 * assumed to be in utf8. If C<do_utf8> is false, the string C<ptr> is
 * assumed to be in native 8-bit encoding. Caches the swatch in C<swash>.
 *
 * A "swash" is a hash which contains initially the keys/values set up by
 * SWASHNEW.  The purpose is to be able to completely represent a Unicode
 * property for all possible code points.  Things are stored in a compact form
 * (see utf8_heavy.pl) so that calculation is required to find the actual
 * property value for a given code point.  As code points are looked up, new
 * key/value pairs are added to the hash, so that the calculation doesn't have
 * to ever be re-done.  Further, each calculation is done, not just for the
 * desired one, but for a whole block of code points adjacent to that one.
 * For binary properties on ASCII machines, the block is usually for 64 code
 * points, starting with a code point evenly divisible by 64.  Thus if the
 * property value for code point 257 is requested, the code goes out and
 * calculates the property values for all 64 code points between 256 and 319,
 * and stores these as a single 64-bit long bit vector, called a "swatch",
 * under the key for code point 256.  The key is the UTF-8 encoding for code
 * point 256, minus the final byte.  Thus, if the length of the UTF-8 encoding
 * for a code point is 13 bytes, the key will be 12 bytes long.  If the value
 * for code point 258 is then requested, this code realizes that it would be
 * stored under the key for 256, and would find that value and extract the
 * relevant bit, offset from 256.
 *
 * Non-binary properties are stored in as many bits as necessary to represent
 * their values (32 currently, though the code is more general than that), not
 * as single bits, but the principal is the same: the value for each key is a
 * vector that encompasses the property values for all code points whose UTF-8
 * representations are represented by the key.  That is, for all code points
 * whose UTF-8 representations are length N bytes, and the key is the first N-1
 * bytes of that.
 */
UV
Perl_swash_fetch(pTHX_ SV *swash, const U8 *ptr, bool do_utf8)
{
SensorCall(28462);    dVAR;
    HV *const hv = MUTABLE_HV(SvRV(swash));
    U32 klen;
    U32 off;
    STRLEN slen;
    STRLEN needents;
    const U8 *tmps = NULL;
    U32 bit;
    SV *swatch;
    U8 tmputf8[2];
    const UV c = NATIVE_TO_ASCII(*ptr);

    PERL_ARGS_ASSERT_SWASH_FETCH;

    /* Convert to utf8 if not already */
    SensorCall(28464);if (!do_utf8 && !UNI_IS_INVARIANT(c)) {
	SensorCall(28463);tmputf8[0] = (U8)UTF8_EIGHT_BIT_HI(c);
	tmputf8[1] = (U8)UTF8_EIGHT_BIT_LO(c);
	ptr = tmputf8;
    }
    /* Given a UTF-X encoded char 0xAA..0xYY,0xZZ
     * then the "swatch" is a vec() for all the chars which start
     * with 0xAA..0xYY
     * So the key in the hash (klen) is length of encoded char -1
     */
    SensorCall(28465);klen = UTF8SKIP(ptr) - 1;
    off  = ptr[klen];

    SensorCall(28468);if (klen == 0) {
      /* If char is invariant then swatch is for all the invariant chars
       * In both UTF-8 and UTF-8-MOD that happens to be UTF_CONTINUATION_MARK
       */
	SensorCall(28466);needents = UTF_CONTINUATION_MARK;
	off      = NATIVE_TO_UTF(ptr[klen]);
    }
    else {
      /* If char is encoded then swatch is for the prefix */
	SensorCall(28467);needents = (1 << UTF_ACCUMULATION_SHIFT);
	off      = NATIVE_TO_UTF(ptr[klen]) & UTF_CONTINUATION_MASK;
    }

    /*
     * This single-entry cache saves about 1/3 of the utf8 overhead in test
     * suite.  (That is, only 7-8% overall over just a hash cache.  Still,
     * it's nothing to sniff at.)  Pity we usually come through at least
     * two function calls to get here...
     *
     * NB: this code assumes that swatches are never modified, once generated!
     */

    SensorCall(28478);if (hv   == PL_last_swash_hv &&
	klen == PL_last_swash_klen &&
	(!klen || memEQ((char *)ptr, (char *)PL_last_swash_key, klen)) )
    {
	SensorCall(28469);tmps = PL_last_swash_tmps;
	slen = PL_last_swash_slen;
    }
    else {
	/* Try our second-level swatch cache, kept in a hash. */
	SensorCall(28470);SV** svp = hv_fetch(hv, (const char*)ptr, klen, FALSE);

	/* If not cached, generate it via swatch_get */
	SensorCall(28476);if (!svp || !SvPOK(*svp)
		 || !(tmps = (const U8*)SvPV_const(*svp, slen))) {
	    /* We use utf8n_to_uvuni() as we want an index into
	       Unicode tables, not a native character number.
	     */
	    SensorCall(28471);const UV code_point = utf8n_to_uvuni(ptr, UTF8_MAXBYTES, 0,
					   ckWARN(WARN_UTF8) ?
					   0 : UTF8_ALLOW_ANY);
	    swatch = swatch_get(swash,
		    /* On EBCDIC & ~(0xA0-1) isn't a useful thing to do */
				(klen) ? (code_point & ~((UV)needents - 1)) : 0,
				needents);

	    SensorCall(28472);if (IN_PERL_COMPILETIME)
		CopHINTS_set(PL_curcop, PL_hints);

	    SensorCall(28473);svp = hv_store(hv, (const char *)ptr, klen, swatch, 0);

	    SensorCall(28475);if (!svp || !(tmps = (U8*)SvPV(*svp, slen))
		     || (slen << 3) < needents)
		{/*59*/SensorCall(28474);Perl_croak(aTHX_ "panic: swash_fetch got improper swatch, "
			   "svp=%p, tmps=%p, slen=%"UVuf", needents=%"UVuf,
			   svp, tmps, (UV)slen, (UV)needents);/*60*/}
	}

	PL_last_swash_hv = hv;
	assert(klen <= sizeof(PL_last_swash_key));
	PL_last_swash_klen = (U8)klen;
	/* FIXME change interpvar.h?  */
	PL_last_swash_tmps = (U8 *) tmps;
	PL_last_swash_slen = slen;
	SensorCall(28477);if (klen)
	    Copy(ptr, PL_last_swash_key, klen, U8);
    }

    SensorCall(28484);if (UTF8_IS_SUPER(ptr) && ckWARN_d(WARN_NON_UNICODE)) {
	SensorCall(28479);SV** const bitssvp = hv_fetchs(hv, "BITS", FALSE);

	/* This outputs warnings for binary properties only, assuming that
	 * to_utf8_case() will output any for non-binary.  Also, surrogates
	 * aren't checked for, as that would warn on things like /\p{Gc=Cs}/ */

	SensorCall(28483);if (! bitssvp || SvUV(*bitssvp) == 1) {
	    /* User-defined properties can silently match above-Unicode */
	    SensorCall(28480);SV** const user_defined_svp = hv_fetchs(hv, "USER_DEFINED", FALSE);
	    SensorCall(28482);if (! user_defined_svp || ! SvUV(*user_defined_svp)) {
		SensorCall(28481);const UV code_point = utf8n_to_uvuni(ptr, UTF8_MAXBYTES, 0, 0);
		Perl_warner(aTHX_ packWARN(WARN_NON_UNICODE),
		    "Code point 0x%04"UVXf" is not Unicode, all \\p{} matches fail; all \\P{} matches succeed", code_point);
	    }
	}
    }

    SensorCall(28492);switch ((int)((slen << 3) / needents)) {
    case 1:
	SensorCall(28485);bit = 1 << (off & 7);
	off >>= 3;
	{UV  ReplaceReturn1121 = (tmps[off] & bit) != 0; SensorCall(28486); return ReplaceReturn1121;}
    case 8:
	{UV  ReplaceReturn1120 = tmps[off]; SensorCall(28487); return ReplaceReturn1120;}
    case 16:
	SensorCall(28488);off <<= 1;
	{UV  ReplaceReturn1119 = (tmps[off] << 8) + tmps[off + 1] ; SensorCall(28489); return ReplaceReturn1119;}
    case 32:
	SensorCall(28490);off <<= 2;
	{UV  ReplaceReturn1118 = (tmps[off] << 24) + (tmps[off+1] << 16) + (tmps[off+2] << 8) + tmps[off + 3] ; SensorCall(28491); return ReplaceReturn1118;}
    }
    SensorCall(28493);Perl_croak(aTHX_ "panic: swash_fetch got swatch of unexpected bit width, "
	       "slen=%"UVuf", needents=%"UVuf, (UV)slen, (UV)needents);
    NORETURN_FUNCTION_END;
SensorCall(28494);}

/* Read a single line of the main body of the swash input text.  These are of
 * the form:
 * 0053	0056	0073
 * where each number is hex.  The first two numbers form the minimum and
 * maximum of a range, and the third is the value associated with the range.
 * Not all swashes should have a third number
 *
 * On input: l	  points to the beginning of the line to be examined; it points
 *		  to somewhere in the string of the whole input text, and is
 *		  terminated by a \n or the null string terminator.
 *	     lend   points to the null terminator of that string
 *	     wants_value    is non-zero if the swash expects a third number
 *	     typestr is the name of the swash's mapping, like 'ToLower'
 * On output: *min, *max, and *val are set to the values read from the line.
 *	      returns a pointer just beyond the line examined.  If there was no
 *	      valid min number on the line, returns lend+1
 */

STATIC U8*
S_swash_scan_list_line(pTHX_ U8* l, U8* const lend, UV* min, UV* max, UV* val,
			     const bool wants_value, const U8* const typestr)
{
    SensorCall(28495);const int  typeto  = typestr[0] == 'T' && typestr[1] == 'o';
    STRLEN numlen;	    /* Length of the number */
    I32 flags = PERL_SCAN_SILENT_ILLDIGIT
		| PERL_SCAN_DISALLOW_PREFIX
		| PERL_SCAN_SILENT_NON_PORTABLE;

    /* nl points to the next \n in the scan */
    U8* const nl = (U8*)memchr(l, '\n', lend - l);

    /* Get the first number on the line: the range minimum */
    numlen = lend - l;
    *min = grok_hex((char *)l, &numlen, &flags, NULL);
    SensorCall(28500);if (numlen)	    /* If found a hex number, position past it */
	{/*161*/SensorCall(28496);l += numlen;/*162*/}
    else {/*163*/SensorCall(28497);if (nl) {	    /* Else, go handle next line, if any */
	{U8 * ReplaceReturn1117 = nl + 1; SensorCall(28498); return ReplaceReturn1117;}	/* 1 is length of "\n" */
    }
    else {		/* Else, no next line */
	{U8 * ReplaceReturn1116 = lend + 1; SensorCall(28499); return ReplaceReturn1116;}	/* to LIST's end at which \n is not found */
    ;/*164*/}}

    /* The max range value follows, separated by a BLANK */
    SensorCall(28524);if (isBLANK(*l)) {
	SensorCall(28501);++l;
	flags = PERL_SCAN_SILENT_ILLDIGIT
		| PERL_SCAN_DISALLOW_PREFIX
		| PERL_SCAN_SILENT_NON_PORTABLE;
	numlen = lend - l;
	*max = grok_hex((char *)l, &numlen, &flags, NULL);
	SensorCall(28504);if (numlen)
	    {/*165*/SensorCall(28502);l += numlen;/*166*/}
	else    /* If no value here, it is a single element range */
	    {/*167*/SensorCall(28503);*max = *min;/*168*/}

	/* Non-binary tables have a third entry: what the first element of the
	 * range maps to */
	SensorCall(28517);if (wants_value) {
	    SensorCall(28505);if (isBLANK(*l)) {
		SensorCall(28506);++l;

		/* The ToLc, etc table mappings are not in hex, and must be
		 * corrected by adding the code point to them */
		SensorCall(28512);if (typeto) {
		    SensorCall(28507);char *after_strtol = (char *) lend;
		    *val = Strtol((char *)l, &after_strtol, 10);
		    l = (U8 *) after_strtol;
		}
		else { /* Other tables are in hex, and are the correct result
			  without tweaking */
		    SensorCall(28508);flags = PERL_SCAN_SILENT_ILLDIGIT
			| PERL_SCAN_DISALLOW_PREFIX
			| PERL_SCAN_SILENT_NON_PORTABLE;
		    numlen = lend - l;
		    *val = grok_hex((char *)l, &numlen, &flags, NULL);
		    SensorCall(28511);if (numlen)
			{/*169*/SensorCall(28509);l += numlen;/*170*/}
		    else
			{/*171*/SensorCall(28510);*val = 0;/*172*/}
		}
	    }
	    else {
		SensorCall(28513);*val = 0;
		SensorCall(28515);if (typeto) {
		    /* diag_listed_as: To%s: illegal mapping '%s' */
		    SensorCall(28514);Perl_croak(aTHX_ "%s: illegal mapping '%s'",
				     typestr, l);
		}
	    }
	}
	else
	    {/*173*/SensorCall(28516);*val = 0;/*174*/} /* bits == 1, then any val should be ignored */
    }
    else { /* Nothing following range min, should be single element with no
	      mapping expected */
	SensorCall(28518);*max = *min;
	SensorCall(28523);if (wants_value) {
	    SensorCall(28519);*val = 0;
	    SensorCall(28521);if (typeto) {
		/* diag_listed_as: To%s: illegal mapping '%s' */
		SensorCall(28520);Perl_croak(aTHX_ "%s: illegal mapping '%s'", typestr, l);
	    }
	}
	else
	    {/*175*/SensorCall(28522);*val = 0;/*176*/} /* bits == 1, then val should be ignored */
    }

    /* Position to next line if any, or EOF */
    SensorCall(28527);if (nl)
	{/*177*/SensorCall(28525);l = nl + 1;/*178*/}
    else
	{/*179*/SensorCall(28526);l = lend;/*180*/}

    {U8 * ReplaceReturn1115 = l; SensorCall(28528); return ReplaceReturn1115;}
}

/* Note:
 * Returns a swatch (a bit vector string) for a code point sequence
 * that starts from the value C<start> and comprises the number C<span>.
 * A C<swash> must be an object created by SWASHNEW (see lib/utf8_heavy.pl).
 * Should be used via swash_fetch, which will cache the swatch in C<swash>.
 */
STATIC SV*
S_swatch_get(pTHX_ SV* swash, UV start, UV span)
{
    SensorCall(28529);SV *swatch;
    U8 *l, *lend, *x, *xend, *s, *send;
    STRLEN lcur, xcur, scur;
    HV *const hv = MUTABLE_HV(SvRV(swash));
    SV** const invlistsvp = hv_fetchs(hv, "INVLIST", FALSE);

    SV** listsvp = NULL; /* The string containing the main body of the table */
    SV** extssvp = NULL;
    SV** invert_it_svp = NULL;
    U8* typestr = NULL;
    STRLEN bits;
    STRLEN octets; /* if bits == 1, then octets == 0 */
    UV  none;
    UV  end = start + span;

    SensorCall(28532);if (invlistsvp == NULL) {
        SensorCall(28530);SV** const bitssvp = hv_fetchs(hv, "BITS", FALSE);
        SV** const nonesvp = hv_fetchs(hv, "NONE", FALSE);
        SV** const typesvp = hv_fetchs(hv, "TYPE", FALSE);
        extssvp = hv_fetchs(hv, "EXTRAS", FALSE);
        listsvp = hv_fetchs(hv, "LIST", FALSE);
        invert_it_svp = hv_fetchs(hv, "INVERT_IT", FALSE);

	bits  = SvUV(*bitssvp);
	none  = SvUV(*nonesvp);
	typestr = (U8*)SvPV_nolen(*typesvp);
    }
    else {
	SensorCall(28531);bits = 1;
	none = 0;
    }
    SensorCall(28533);octets = bits >> 3; /* if bits == 1, then octets == 0 */

    PERL_ARGS_ASSERT_SWATCH_GET;

    SensorCall(28535);if (bits != 1 && bits != 8 && bits != 16 && bits != 32) {
	SensorCall(28534);Perl_croak(aTHX_ "panic: swatch_get doesn't expect bits %"UVuf,
						 (UV)bits);
    }

    /* If overflowed, use the max possible */
    SensorCall(28537);if (end < start) {
	SensorCall(28536);end = UV_MAX;
	span = end - start;
    }

    /* create and initialize $swatch */
    SensorCall(28538);scur   = octets ? (span * octets) : (span + 7) / 8;
    swatch = newSV(scur);
    SvPOK_on(swatch);
    s = (U8*)SvPVX(swatch);
    SensorCall(28549);if (octets && none) {
	SensorCall(28539);const U8* const e = s + scur;
	SensorCall(28546);while (s < e) {
	    SensorCall(28540);if (bits == 8)
		{/*107*/SensorCall(28541);*s++ = (U8)(none & 0xff);/*108*/}
	    else {/*109*/SensorCall(28542);if (bits == 16) {
		SensorCall(28543);*s++ = (U8)((none >>  8) & 0xff);
		*s++ = (U8)( none        & 0xff);
	    }
	    else {/*111*/SensorCall(28544);if (bits == 32) {
		SensorCall(28545);*s++ = (U8)((none >> 24) & 0xff);
		*s++ = (U8)((none >> 16) & 0xff);
		*s++ = (U8)((none >>  8) & 0xff);
		*s++ = (U8)( none        & 0xff);
	    ;/*112*/}/*110*/}}
	}
	SensorCall(28547);*s = '\0';
    }
    else {
	SensorCall(28548);(void)memzero((U8*)s, scur + 1);
    }
    SvCUR_set(swatch, scur);
    SensorCall(28550);s = (U8*)SvPVX(swatch);

    SensorCall(28552);if (invlistsvp) {	/* If has an inversion list set up use that */
	_invlist_populate_swatch(*invlistsvp, start, end, s);
        {SV * ReplaceReturn1114 = swatch; SensorCall(28551); return ReplaceReturn1114;}
    }

    /* read $swash->{LIST} */
    SensorCall(28553);l = (U8*)SvPV(*listsvp, lcur);
    lend = l + lcur;
    SensorCall(28581);while (l < lend) {
	SensorCall(28554);UV min, max, val, upper;
	l = S_swash_scan_list_line(aTHX_ l, lend, &min, &max, &val,
					 cBOOL(octets), typestr);
	SensorCall(28556);if (l > lend) {
	    SensorCall(28555);break;
	}

	/* If looking for something beyond this range, go try the next one */
	SensorCall(28558);if (max < start)
	    {/*113*/SensorCall(28557);continue;/*114*/}

	/* <end> is generally 1 beyond where we want to set things, but at the
	 * platform's infinity, where we can't go any higher, we want to
	 * include the code point at <end> */
        SensorCall(28559);upper = (max < end)
                ? max
                : (max != UV_MAX || end != UV_MAX)
                  ? end - 1
                  : end;

	SensorCall(28580);if (octets) {
	    SensorCall(28560);UV key;
	    SensorCall(28564);if (min < start) {
		SensorCall(28561);if (!none || val < none) {
		    SensorCall(28562);val += start - min;
		}
		SensorCall(28563);min = start;
	    }
	    SensorCall(28574);for (key = min; key <= upper; key++) {
		SensorCall(28565);STRLEN offset;
		/* offset must be non-negative (start <= min <= key < end) */
		offset = octets * (key - start);
		SensorCall(28571);if (bits == 8)
		    {/*115*/SensorCall(28566);s[offset] = (U8)(val & 0xff);/*116*/}
		else {/*117*/SensorCall(28567);if (bits == 16) {
		    SensorCall(28568);s[offset    ] = (U8)((val >>  8) & 0xff);
		    s[offset + 1] = (U8)( val        & 0xff);
		}
		else {/*119*/SensorCall(28569);if (bits == 32) {
		    SensorCall(28570);s[offset    ] = (U8)((val >> 24) & 0xff);
		    s[offset + 1] = (U8)((val >> 16) & 0xff);
		    s[offset + 2] = (U8)((val >>  8) & 0xff);
		    s[offset + 3] = (U8)( val        & 0xff);
		;/*120*/}/*118*/}}

		SensorCall(28573);if (!none || val < none)
		    {/*121*/SensorCall(28572);++val;/*122*/}
	    }
	}
	else { /* bits == 1, then val should be ignored */
	    SensorCall(28575);UV key;
	    SensorCall(28577);if (min < start)
		{/*123*/SensorCall(28576);min = start;/*124*/}

	    SensorCall(28579);for (key = min; key <= upper; key++) {
		SensorCall(28578);const STRLEN offset = (STRLEN)(key - start);
		s[offset >> 3] |= 1 << (offset & 7);
	    }
	}
    } /* while */

    /* Invert if the data says it should be.  Assumes that bits == 1 */
    SensorCall(28586);if (invert_it_svp && SvUV(*invert_it_svp)) {

	/* Unicode properties should come with all bits above PERL_UNICODE_MAX
	 * be 0, and their inversion should also be 0, as we don't succeed any
	 * Unicode property matches for non-Unicode code points */
	SensorCall(28582);if (start <= PERL_UNICODE_MAX) {

	    /* The code below assumes that we never cross the
	     * Unicode/above-Unicode boundary in a range, as otherwise we would
	     * have to figure out where to stop flipping the bits.  Since this
	     * boundary is divisible by a large power of 2, and swatches comes
	     * in small powers of 2, this should be a valid assumption */
	    assert(start + span - 1 <= PERL_UNICODE_MAX);

	    SensorCall(28583);send = s + scur;
	    SensorCall(28585);while (s < send) {
		SensorCall(28584);*s = ~(*s);
		s++;
	    }
	}
    }

    /* read $swash->{EXTRAS}
     * This code also copied to swash_to_invlist() below */
    SensorCall(28587);x = (U8*)SvPV(*extssvp, xcur);
    xend = x + xcur;
    SensorCall(28649);while (x < xend) {
	SensorCall(28588);STRLEN namelen;
	U8 *namestr;
	SV** othersvp;
	HV* otherhv;
	STRLEN otherbits;
	SV **otherbitssvp, *other;
	U8 *s, *o, *nl;
	STRLEN slen, olen;

	const U8 opc = *x++;
	SensorCall(28590);if (opc == '\n')
	    {/*125*/SensorCall(28589);continue;/*126*/}

	SensorCall(28591);nl = (U8*)memchr(x, '\n', xend - x);

	SensorCall(28597);if (opc != '-' && opc != '+' && opc != '!' && opc != '&') {
	    SensorCall(28592);if (nl) {
		SensorCall(28593);x = nl + 1; /* 1 is length of "\n" */
		SensorCall(28594);continue;
	    }
	    else {
		SensorCall(28595);x = xend; /* to EXTRAS' end at which \n is not found */
		SensorCall(28596);break;
	    }
	}

	SensorCall(28598);namestr = x;
	SensorCall(28601);if (nl) {
	    SensorCall(28599);namelen = nl - namestr;
	    x = nl + 1;
	}
	else {
	    SensorCall(28600);namelen = xend - namestr;
	    x = xend;
	}

	SensorCall(28602);othersvp = hv_fetch(hv, (char *)namestr, namelen, FALSE);
	otherhv = MUTABLE_HV(SvRV(*othersvp));
	otherbitssvp = hv_fetchs(otherhv, "BITS", FALSE);
	otherbits = (STRLEN)SvUV(*otherbitssvp);
	SensorCall(28604);if (bits < otherbits)
	    {/*127*/SensorCall(28603);Perl_croak(aTHX_ "panic: swatch_get found swatch size mismatch, "
		       "bits=%"UVuf", otherbits=%"UVuf, (UV)bits, (UV)otherbits);/*128*/}

	/* The "other" swatch must be destroyed after. */
	SensorCall(28605);other = swatch_get(*othersvp, start, span);
	o = (U8*)SvPV(other, olen);

	SensorCall(28607);if (!olen)
	    {/*129*/SensorCall(28606);Perl_croak(aTHX_ "panic: swatch_get got improper swatch");/*130*/}

	SensorCall(28608);s = (U8*)SvPV(swatch, slen);
	SensorCall(28648);if (bits == 1 && otherbits == 1) {
	    SensorCall(28609);if (slen != olen)
		{/*131*/SensorCall(28610);Perl_croak(aTHX_ "panic: swatch_get found swatch length "
			   "mismatch, slen=%"UVuf", olen=%"UVuf,
			   (UV)slen, (UV)olen);/*132*/}

	    SensorCall(28624);switch (opc) {
	    case '+':
		SensorCall(28611);while (slen--)
		    {/*133*/SensorCall(28612);*s++ |= *o++;/*134*/}
		SensorCall(28613);break;
	    case '!':
		SensorCall(28614);while (slen--)
		    {/*135*/SensorCall(28615);*s++ |= ~*o++;/*136*/}
		SensorCall(28616);break;
	    case '-':
		SensorCall(28617);while (slen--)
		    {/*137*/SensorCall(28618);*s++ &= ~*o++;/*138*/}
		SensorCall(28619);break;
	    case '&':
		SensorCall(28620);while (slen--)
		    {/*139*/SensorCall(28621);*s++ &= *o++;/*140*/}
		SensorCall(28622);break;
	    default:
		SensorCall(28623);break;
	    }
	}
	else {
	    SensorCall(28625);STRLEN otheroctets = otherbits >> 3;
	    STRLEN offset = 0;
	    U8* const send = s + slen;

	    SensorCall(28647);while (s < send) {
		SensorCall(28626);UV otherval = 0;

		SensorCall(28631);if (otherbits == 1) {
		    SensorCall(28627);otherval = (o[offset >> 3] >> (offset & 7)) & 1;
		    ++offset;
		}
		else {
		    SensorCall(28628);STRLEN vlen = otheroctets;
		    otherval = *o++;
		    SensorCall(28630);while (--vlen) {
			SensorCall(28629);otherval <<= 8;
			otherval |= *o++;
		    }
		}

		SensorCall(28640);if (opc == '+' && otherval)
		    NOOP;   /* replace with otherval */
		else {/*141*/SensorCall(28632);if (opc == '!' && !otherval)
		    {/*143*/SensorCall(28633);otherval = 1;/*144*/}
		else {/*145*/SensorCall(28634);if (opc == '-' && otherval)
		    {/*147*/SensorCall(28635);otherval = 0;/*148*/}
		else {/*149*/SensorCall(28636);if (opc == '&' && !otherval)
		    {/*151*/SensorCall(28637);otherval = 0;/*152*/}
		else {
		    SensorCall(28638);s += octets; /* no replacement */
		    SensorCall(28639);continue;
		;/*150*/}/*146*/}/*142*/}}

		SensorCall(28646);if (bits == 8)
		    {/*153*/SensorCall(28641);*s++ = (U8)( otherval & 0xff);/*154*/}
		else {/*155*/SensorCall(28642);if (bits == 16) {
		    SensorCall(28643);*s++ = (U8)((otherval >>  8) & 0xff);
		    *s++ = (U8)( otherval        & 0xff);
		}
		else {/*157*/SensorCall(28644);if (bits == 32) {
		    SensorCall(28645);*s++ = (U8)((otherval >> 24) & 0xff);
		    *s++ = (U8)((otherval >> 16) & 0xff);
		    *s++ = (U8)((otherval >>  8) & 0xff);
		    *s++ = (U8)( otherval        & 0xff);
		;/*158*/}/*156*/}}
	    }
	}
	sv_free(other); /* through with it! */
    } /* while */
    {SV * ReplaceReturn1113 = swatch; SensorCall(28650); return ReplaceReturn1113;}
}

HV*
Perl__swash_inversion_hash(pTHX_ SV* const swash)
{

   /* Subject to change or removal.  For use only in one place in regcomp.c.
    * Can't be used on a property that is subject to user override, as it
    * relies on the value of SPECIALS in the swash which would be set by
    * utf8_heavy.pl to the hash in the non-overriden file, and hence is not set
    * for overridden properties
    *
    * Returns a hash which is the inversion and closure of a swash mapping.
    * For example, consider the input lines:
    * 004B		006B
    * 004C		006C
    * 212A		006B
    *
    * The returned hash would have two keys, the utf8 for 006B and the utf8 for
    * 006C.  The value for each key is an array.  For 006C, the array would
    * have a two elements, the utf8 for itself, and for 004C.  For 006B, there
    * would be three elements in its array, the utf8 for 006B, 004B and 212A.
    *
    * Essentially, for any code point, it gives all the code points that map to
    * it, or the list of 'froms' for that point.
    *
    * Currently it ignores any additions or deletions from other swashes,
    * looking at just the main body of the swash, and if there are SPECIALS
    * in the swash, at that hash
    *
    * The specials hash can be extra code points, and most likely consists of
    * maps from single code points to multiple ones (each expressed as a string
    * of utf8 characters).   This function currently returns only 1-1 mappings.
    * However consider this possible input in the specials hash:
    * "\xEF\xAC\x85" => "\x{0073}\x{0074}",         # U+FB05 => 0073 0074
    * "\xEF\xAC\x86" => "\x{0073}\x{0074}",         # U+FB06 => 0073 0074
    *
    * Both FB05 and FB06 map to the same multi-char sequence, which we don't
    * currently handle.  But it also means that FB05 and FB06 are equivalent in
    * a 1-1 mapping which we should handle, and this relationship may not be in
    * the main table.  Therefore this function examines all the multi-char
    * sequences and adds the 1-1 mappings that come out of that.  */

    SensorCall(28651);U8 *l, *lend;
    STRLEN lcur;
    HV *const hv = MUTABLE_HV(SvRV(swash));

    /* The string containing the main body of the table */
    SV** const listsvp = hv_fetchs(hv, "LIST", FALSE);

    SV** const typesvp = hv_fetchs(hv, "TYPE", FALSE);
    SV** const bitssvp = hv_fetchs(hv, "BITS", FALSE);
    SV** const nonesvp = hv_fetchs(hv, "NONE", FALSE);
    /*SV** const extssvp = hv_fetchs(hv, "EXTRAS", FALSE);*/
    const U8* const typestr = (U8*)SvPV_nolen(*typesvp);
    const STRLEN bits  = SvUV(*bitssvp);
    const STRLEN octets = bits >> 3; /* if bits == 1, then octets == 0 */
    const UV     none  = SvUV(*nonesvp);
    SV **specials_p = hv_fetchs(hv, "SPECIALS", 0);

    HV* ret = newHV();

    PERL_ARGS_ASSERT__SWASH_INVERSION_HASH;

    /* Must have at least 8 bits to get the mappings */
    SensorCall(28653);if (bits != 8 && bits != 16 && bits != 32) {
	SensorCall(28652);Perl_croak(aTHX_ "panic: swash_inversion_hash doesn't expect bits %"UVuf,
						 (UV)bits);
    }

    SensorCall(28679);if (specials_p) { /* It might be "special" (sometimes, but not always, a
			mapping to more than one character */

	/* Construct an inverse mapping hash for the specials */
	SensorCall(28654);HV * const specials_hv = MUTABLE_HV(SvRV(*specials_p));
	HV * specials_inverse = newHV();
	char *char_from; /* the lhs of the map */
	I32 from_len;   /* its byte length */
	char *char_to;  /* the rhs of the map */
	I32 to_len;	/* its byte length */
	SV *sv_to;	/* and in a sv */
	AV* from_list;  /* list of things that map to each 'to' */

	hv_iterinit(specials_hv);

	/* The keys are the characters (in utf8) that map to the corresponding
	 * utf8 string value.  Iterate through the list creating the inverse
	 * list. */
	SensorCall(28663);while ((sv_to = hv_iternextsv(specials_hv, &char_from, &from_len))) {
	    SensorCall(28655);SV** listp;
	    SensorCall(28657);if (! SvPOK(sv_to)) {
		SensorCall(28656);Perl_croak(aTHX_ "panic: value returned from hv_iternextsv() "
			   "unexpectedly is not a string, flags=%lu",
			   (unsigned long)SvFLAGS(sv_to));
	    }
	    /*DEBUG_U(PerlIO_printf(Perl_debug_log, "Found mapping from %"UVXf", First char of to is %"UVXf"\n", valid_utf8_to_uvchr((U8*) char_from, 0), valid_utf8_to_uvchr((U8*) SvPVX(sv_to), 0)));*/

	    /* Each key in the inverse list is a mapped-to value, and the key's
	     * hash value is a list of the strings (each in utf8) that map to
	     * it.  Those strings are all one character long */
	    SensorCall(28662);if ((listp = hv_fetch(specials_inverse,
				    SvPVX(sv_to),
				    SvCUR(sv_to), 0)))
	    {
		SensorCall(28658);from_list = (AV*) *listp;
	    }
	    else { /* No entry yet for it: create one */
		SensorCall(28659);from_list = newAV();
		SensorCall(28661);if (! hv_store(specials_inverse,
				SvPVX(sv_to),
				SvCUR(sv_to),
				(SV*) from_list, 0))
		{
		    SensorCall(28660);Perl_croak(aTHX_ "panic: hv_store() unexpectedly failed");
		}
	    }

	    /* Here have the list associated with this 'to' (perhaps newly
	     * created and empty).  Just add to it.  Note that we ASSUME that
	     * the input is guaranteed to not have duplications, so we don't
	     * check for that.  Duplications just slow down execution time. */
	    av_push(from_list, newSVpvn_utf8(char_from, from_len, TRUE));
	}

	/* Here, 'specials_inverse' contains the inverse mapping.  Go through
	 * it looking for cases like the FB05/FB06 examples above.  There would
	 * be an entry in the hash like
	*	'st' => [ FB05, FB06 ]
	* In this example we will create two lists that get stored in the
	* returned hash, 'ret':
	*	FB05 => [ FB05, FB06 ]
	*	FB06 => [ FB05, FB06 ]
	*
	* Note that there is nothing to do if the array only has one element.
	* (In the normal 1-1 case handled below, we don't have to worry about
	* two lists, as everything gets tied to the single list that is
	* generated for the single character 'to'.  But here, we are omitting
	* that list, ('st' in the example), so must have multiple lists.) */
	SensorCall(28678);while ((from_list = (AV *) hv_iternextsv(specials_inverse,
						 &char_to, &to_len)))
	{
	    SensorCall(28664);if (av_len(from_list) > 0) {
		SensorCall(28665);int i;

		/* We iterate over all combinations of i,j to place each code
		 * point on each list */
		SensorCall(28677);for (i = 0; i <= av_len(from_list); i++) {
		    SensorCall(28666);int j;
		    AV* i_list = newAV();
		    SV** entryp = av_fetch(from_list, i, FALSE);
		    SensorCall(28668);if (entryp == NULL) {
			SensorCall(28667);Perl_croak(aTHX_ "panic: av_fetch() unexpectedly failed");
		    }
		    SensorCall(28670);if (hv_fetch(ret, SvPVX(*entryp), SvCUR(*entryp), FALSE)) {
			SensorCall(28669);Perl_croak(aTHX_ "panic: unexpected entry for %s", SvPVX(*entryp));
		    }
		    SensorCall(28672);if (! hv_store(ret, SvPVX(*entryp), SvCUR(*entryp),
				   (SV*) i_list, FALSE))
		    {
			SensorCall(28671);Perl_croak(aTHX_ "panic: hv_store() unexpectedly failed");
		    }

		    /* For debugging: UV u = valid_utf8_to_uvchr((U8*) SvPVX(*entryp), 0);*/
		    SensorCall(28676);for (j = 0; j <= av_len(from_list); j++) {
			SensorCall(28673);entryp = av_fetch(from_list, j, FALSE);
			SensorCall(28675);if (entryp == NULL) {
			    SensorCall(28674);Perl_croak(aTHX_ "panic: av_fetch() unexpectedly failed");
			}

			/* When i==j this adds itself to the list */
			av_push(i_list, newSVuv(utf8_to_uvchr_buf(
					(U8*) SvPVX(*entryp),
					(U8*) SvPVX(*entryp) + SvCUR(*entryp),
					0)));
			/*DEBUG_U(PerlIO_printf(Perl_debug_log, "Adding %"UVXf" to list for %"UVXf"\n", valid_utf8_to_uvchr((U8*) SvPVX(*entryp), 0), u));*/
		    }
		}
	    }
	}
	SvREFCNT_dec(specials_inverse); /* done with it */
    } /* End of specials */

    /* read $swash->{LIST} */
    SensorCall(28680);l = (U8*)SvPV(*listsvp, lcur);
    lend = l + lcur;

    /* Go through each input line */
    SensorCall(28706);while (l < lend) {
	SensorCall(28681);UV min, max, val;
	UV inverse;
	l = S_swash_scan_list_line(aTHX_ l, lend, &min, &max, &val,
					 cBOOL(octets), typestr);
	SensorCall(28683);if (l > lend) {
	    SensorCall(28682);break;
	}

	/* Each element in the range is to be inverted */
	SensorCall(28705);for (inverse = min; inverse <= max; inverse++) {
	    SensorCall(28684);AV* list;
	    SV** listp;
	    IV i;
	    bool found_key = FALSE;
	    bool found_inverse = FALSE;

	    /* The key is the inverse mapping */
	    char key[UTF8_MAXBYTES+1];
	    char* key_end = (char *) uvuni_to_utf8((U8*) key, val);
	    STRLEN key_len = key_end - key;

	    /* Get the list for the map */
	    SensorCall(28689);if ((listp = hv_fetch(ret, key, key_len, FALSE))) {
		SensorCall(28685);list = (AV*) *listp;
	    }
	    else { /* No entry yet for it: create one */
		SensorCall(28686);list = newAV();
		SensorCall(28688);if (! hv_store(ret, key, key_len, (SV*) list, FALSE)) {
		    SensorCall(28687);Perl_croak(aTHX_ "panic: hv_store() unexpectedly failed");
		}
	    }

	    /* Look through list to see if this inverse mapping already is
	     * listed, or if there is a mapping to itself already */
	    SensorCall(28700);for (i = 0; i <= av_len(list); i++) {
		SensorCall(28690);SV** entryp = av_fetch(list, i, FALSE);
		SV* entry;
		SensorCall(28692);if (entryp == NULL) {
		    SensorCall(28691);Perl_croak(aTHX_ "panic: av_fetch() unexpectedly failed");
		}
		SensorCall(28693);entry = *entryp;
		/*DEBUG_U(PerlIO_printf(Perl_debug_log, "list for %"UVXf" contains %"UVXf"\n", val, SvUV(entry)));*/
		SensorCall(28695);if (SvUV(entry) == val) {
		    SensorCall(28694);found_key = TRUE;
		}
		SensorCall(28697);if (SvUV(entry) == inverse) {
		    SensorCall(28696);found_inverse = TRUE;
		}

		/* No need to continue searching if found everything we are
		 * looking for */
		SensorCall(28699);if (found_key && found_inverse) {
		    SensorCall(28698);break;
		}
	    }

	    /* Make sure there is a mapping to itself on the list */
	    SensorCall(28701);if (! found_key) {
		av_push(list, newSVuv(val));
		/*DEBUG_U(PerlIO_printf(Perl_debug_log, "Adding %"UVXf" to list for %"UVXf"\n", val, val));*/
	    }


	    /* Simply add the value to the list */
	    SensorCall(28702);if (! found_inverse) {
		av_push(list, newSVuv(inverse));
		/*DEBUG_U(PerlIO_printf(Perl_debug_log, "Adding %"UVXf" to list for %"UVXf"\n", inverse, val));*/
	    }

	    /* swatch_get() increments the value of val for each element in the
	     * range.  That makes more compact tables possible.  You can
	     * express the capitalization, for example, of all consecutive
	     * letters with a single line: 0061\t007A\t0041 This maps 0061 to
	     * 0041, 0062 to 0042, etc.  I (khw) have never understood 'none',
	     * and it's not documented; it appears to be used only in
	     * implementing tr//; I copied the semantics from swatch_get(), just
	     * in case */
	    SensorCall(28704);if (!none || val < none) {
		SensorCall(28703);++val;
	    }
	}
    }

    {HV * ReplaceReturn1112 = ret; SensorCall(28707); return ReplaceReturn1112;}
}

SV*
Perl__swash_to_invlist(pTHX_ SV* const swash)
{

   /* Subject to change or removal.  For use only in one place in regcomp.c */

    SensorCall(28708);U8 *l, *lend;
    char *loc;
    STRLEN lcur;
    HV *const hv = MUTABLE_HV(SvRV(swash));
    UV elements = 0;    /* Number of elements in the inversion list */
    U8 empty[] = "";

    /* The string containing the main body of the table */
    SV** const listsvp = hv_fetchs(hv, "LIST", FALSE);
    SV** const typesvp = hv_fetchs(hv, "TYPE", FALSE);
    SV** const bitssvp = hv_fetchs(hv, "BITS", FALSE);
    SV** const extssvp = hv_fetchs(hv, "EXTRAS", FALSE);
    SV** const invert_it_svp = hv_fetchs(hv, "INVERT_IT", FALSE);

    const U8* const typestr = (U8*)SvPV_nolen(*typesvp);
    const STRLEN bits  = SvUV(*bitssvp);
    const STRLEN octets = bits >> 3; /* if bits == 1, then octets == 0 */
    U8 *x, *xend;
    STRLEN xcur;

    SV* invlist;

    PERL_ARGS_ASSERT__SWASH_TO_INVLIST;

    /* read $swash->{LIST} */
    SensorCall(28711);if (SvPOK(*listsvp)) {
	SensorCall(28709);l = (U8*)SvPV(*listsvp, lcur);
    }
    else {
	/* LIST legitimately doesn't contain a string during compilation phases
	 * of Perl itself, before the Unicode tables are generated.  In this
	 * case, just fake things up by creating an empty list */
	SensorCall(28710);l = empty;
	lcur = 0;
    }
    SensorCall(28712);loc = (char *) l;
    lend = l + lcur;

    /* Scan the input to count the number of lines to preallocate array size
     * based on worst possible case, which is each line in the input creates 2
     * elements in the inversion list: 1) the beginning of a range in the list;
     * 2) the beginning of a range not in the list.  */
    SensorCall(28714);while ((loc = (strchr(loc, '\n'))) != NULL) {
	SensorCall(28713);elements += 2;
	loc++;
    }

    /* If the ending is somehow corrupt and isn't a new line, add another
     * element for the final range that isn't in the inversion list */
    SensorCall(28716);if (! (*lend == '\n'
	|| (*lend == '\0' && (lcur == 0 || *(lend - 1) == '\n'))))
    {
	SensorCall(28715);elements++;
    }

    SensorCall(28717);invlist = _new_invlist(elements);

    /* Now go through the input again, adding each range to the list */
    SensorCall(28722);while (l < lend) {
	SensorCall(28718);UV start, end;
	UV val;		/* Not used by this function */

	l = S_swash_scan_list_line(aTHX_ l, lend, &start, &end, &val,
					 cBOOL(octets), typestr);

	SensorCall(28720);if (l > lend) {
	    SensorCall(28719);break;
	}

	SensorCall(28721);invlist = _add_range_to_invlist(invlist, start, end);
    }

    /* Invert if the data says it should be */
    SensorCall(28723);if (invert_it_svp && SvUV(*invert_it_svp)) {
	_invlist_invert_prop(invlist);
    }

    /* This code is copied from swatch_get()
     * read $swash->{EXTRAS} */
    SensorCall(28724);x = (U8*)SvPV(*extssvp, xcur);
    xend = x + xcur;
    SensorCall(28749);while (x < xend) {
	SensorCall(28725);STRLEN namelen;
	U8 *namestr;
	SV** othersvp;
	HV* otherhv;
	STRLEN otherbits;
	SV **otherbitssvp, *other;
	U8 *nl;

	const U8 opc = *x++;
	SensorCall(28727);if (opc == '\n')
	    {/*105*/SensorCall(28726);continue;/*106*/}

	SensorCall(28728);nl = (U8*)memchr(x, '\n', xend - x);

	SensorCall(28734);if (opc != '-' && opc != '+' && opc != '!' && opc != '&') {
	    SensorCall(28729);if (nl) {
		SensorCall(28730);x = nl + 1; /* 1 is length of "\n" */
		SensorCall(28731);continue;
	    }
	    else {
		SensorCall(28732);x = xend; /* to EXTRAS' end at which \n is not found */
		SensorCall(28733);break;
	    }
	}

	SensorCall(28735);namestr = x;
	SensorCall(28738);if (nl) {
	    SensorCall(28736);namelen = nl - namestr;
	    x = nl + 1;
	}
	else {
	    SensorCall(28737);namelen = xend - namestr;
	    x = xend;
	}

	SensorCall(28739);othersvp = hv_fetch(hv, (char *)namestr, namelen, FALSE);
	otherhv = MUTABLE_HV(SvRV(*othersvp));
	otherbitssvp = hv_fetchs(otherhv, "BITS", FALSE);
	otherbits = (STRLEN)SvUV(*otherbitssvp);

	SensorCall(28741);if (bits != otherbits || bits != 1) {
	    SensorCall(28740);Perl_croak(aTHX_ "panic: _swash_to_invlist only operates on boolean "
		       "properties, bits=%"UVuf", otherbits=%"UVuf,
		       (UV)bits, (UV)otherbits);
	}

	/* The "other" swatch must be destroyed after. */
	SensorCall(28742);other = _swash_to_invlist((SV *)*othersvp);

	/* End of code copied from swatch_get() */
	SensorCall(28748);switch (opc) {
	case '+':
	    _invlist_union(invlist, other, &invlist);
	    SensorCall(28743);break;
	case '!':
	    _invlist_invert(other);
	    _invlist_union(invlist, other, &invlist);
	    SensorCall(28744);break;
	case '-':
	    _invlist_subtract(invlist, other, &invlist);
	    SensorCall(28745);break;
	case '&':
	    _invlist_intersection(invlist, other, &invlist);
	    SensorCall(28746);break;
	default:
	    SensorCall(28747);break;
	}
	sv_free(other); /* through with it! */
    }

    {SV * ReplaceReturn1111 = invlist; SensorCall(28750); return ReplaceReturn1111;}
}

/*
=for apidoc uvchr_to_utf8

Adds the UTF-8 representation of the Native code point C<uv> to the end
of the string C<d>; C<d> should have at least C<UTF8_MAXBYTES+1> free
bytes available. The return value is the pointer to the byte after the
end of the new character. In other words,

    d = uvchr_to_utf8(d, uv);

is the recommended wide native character-aware way of saying

    *(d++) = uv;

=cut
*/

/* On ASCII machines this is normally a macro but we want a
   real function in case XS code wants it
*/
U8 *
Perl_uvchr_to_utf8(pTHX_ U8 *d, UV uv)
{
    PERL_ARGS_ASSERT_UVCHR_TO_UTF8;

    {U8 * ReplaceReturn1110 = Perl_uvuni_to_utf8_flags(aTHX_ d, NATIVE_TO_UNI(uv), 0); SensorCall(28751); return ReplaceReturn1110;}
}

U8 *
Perl_uvchr_to_utf8_flags(pTHX_ U8 *d, UV uv, UV flags)
{
    PERL_ARGS_ASSERT_UVCHR_TO_UTF8_FLAGS;

    {U8 * ReplaceReturn1109 = Perl_uvuni_to_utf8_flags(aTHX_ d, NATIVE_TO_UNI(uv), flags); SensorCall(28752); return ReplaceReturn1109;}
}

/*
=for apidoc utf8n_to_uvchr

Returns the native character value of the first character in the string
C<s>
which is assumed to be in UTF-8 encoding; C<retlen> will be set to the
length, in bytes, of that character.

C<length> and C<flags> are the same as L</utf8n_to_uvuni>().

=cut
*/
/* On ASCII machines this is normally a macro but we want
   a real function in case XS code wants it
*/
UV
Perl_utf8n_to_uvchr(pTHX_ const U8 *s, STRLEN curlen, STRLEN *retlen,
U32 flags)
{
    SensorCall(28753);const UV uv = Perl_utf8n_to_uvuni(aTHX_ s, curlen, retlen, flags);

    PERL_ARGS_ASSERT_UTF8N_TO_UVCHR;

    {UV  ReplaceReturn1108 = UNI_TO_NATIVE(uv); SensorCall(28754); return ReplaceReturn1108;}
}

bool
Perl_check_utf8_print(pTHX_ register const U8* s, const STRLEN len)
{
    /* May change: warns if surrogates, non-character code points, or
     * non-Unicode code points are in s which has length len bytes.  Returns
     * TRUE if none found; FALSE otherwise.  The only other validity check is
     * to make sure that this won't exceed the string's length */

    SensorCall(28755);const U8* const e = s + len;
    bool ok = TRUE;

    PERL_ARGS_ASSERT_CHECK_UTF8_PRINT;

    SensorCall(28770);while (s < e) {
	SensorCall(28756);if (UTF8SKIP(s) > len) {
	    SensorCall(28757);Perl_ck_warner_d(aTHX_ packWARN(WARN_UTF8),
			   "%s in %s", unees, PL_op ? OP_DESC(PL_op) : "print");
	    {_Bool  ReplaceReturn1107 = FALSE; SensorCall(28758); return ReplaceReturn1107;}
	}
	SensorCall(28768);if (UNLIKELY(*s >= UTF8_FIRST_PROBLEMATIC_CODE_POINT_FIRST_BYTE)) {
	    SensorCall(28759);STRLEN char_len;
	    SensorCall(28767);if (UTF8_IS_SUPER(s)) {
		SensorCall(28760);if (ckWARN_d(WARN_NON_UNICODE)) {
		    SensorCall(28761);UV uv = utf8_to_uvchr_buf(s, e, &char_len);
		    Perl_warner(aTHX_ packWARN(WARN_NON_UNICODE),
			"Code point 0x%04"UVXf" is not Unicode, may not be portable", uv);
		    ok = FALSE;
		}
	    }
	    else {/*21*/SensorCall(28762);if (UTF8_IS_SURROGATE(s)) {
		SensorCall(28763);if (ckWARN_d(WARN_SURROGATE)) {
		    SensorCall(28764);UV uv = utf8_to_uvchr_buf(s, e, &char_len);
		    Perl_warner(aTHX_ packWARN(WARN_SURROGATE),
			"Unicode surrogate U+%04"UVXf" is illegal in UTF-8", uv);
		    ok = FALSE;
		}
	    }
	    else {/*23*/SensorCall(28765);if
		((UTF8_IS_NONCHAR_GIVEN_THAT_NON_SUPER_AND_GE_PROBLEMATIC(s))
		 && (ckWARN_d(WARN_NONCHAR)))
	    {
		SensorCall(28766);UV uv = utf8_to_uvchr_buf(s, e, &char_len);
		Perl_warner(aTHX_ packWARN(WARN_NONCHAR),
		    "Unicode non-character U+%04"UVXf" is illegal for open interchange", uv);
		ok = FALSE;
	    ;/*24*/}/*22*/}}
	}
	SensorCall(28769);s += UTF8SKIP(s);
    }

    {_Bool  ReplaceReturn1106 = ok; SensorCall(28771); return ReplaceReturn1106;}
}

/*
=for apidoc pv_uni_display

Build to the scalar C<dsv> a displayable version of the string C<spv>,
length C<len>, the displayable version being at most C<pvlim> bytes long
(if longer, the rest is truncated and "..." will be appended).

The C<flags> argument can have UNI_DISPLAY_ISPRINT set to display
isPRINT()able characters as themselves, UNI_DISPLAY_BACKSLASH
to display the \\[nrfta\\] as the backslashed versions (like '\n')
(UNI_DISPLAY_BACKSLASH is preferred over UNI_DISPLAY_ISPRINT for \\).
UNI_DISPLAY_QQ (and its alias UNI_DISPLAY_REGEX) have both
UNI_DISPLAY_BACKSLASH and UNI_DISPLAY_ISPRINT turned on.

The pointer to the PV of the C<dsv> is returned.

=cut */
char *
Perl_pv_uni_display(pTHX_ SV *dsv, const U8 *spv, STRLEN len, STRLEN pvlim, UV flags)
{
    SensorCall(28772);int truncated = 0;
    const char *s, *e;

    PERL_ARGS_ASSERT_PV_UNI_DISPLAY;

    sv_setpvs(dsv, "");
    SvUTF8_off(dsv);
    SensorCall(28801);for (s = (const char *)spv, e = s + len; s < e; s += UTF8SKIP(s)) {
	 SensorCall(28773);UV u;
	  /* This serves double duty as a flag and a character to print after
	     a \ when flags & UNI_DISPLAY_BACKSLASH is true.
	  */
	 char ok = 0;

	 SensorCall(28776);if (pvlim && SvCUR(dsv) >= pvlim) {
	      SensorCall(28774);truncated++;
	      SensorCall(28775);break;
	 }
	 SensorCall(28777);u = utf8_to_uvchr_buf((U8*)s, (U8*)e, 0);
	 SensorCall(28798);if (u < 256) {
	     SensorCall(28778);const unsigned char c = (unsigned char)u & 0xFF;
	     SensorCall(28795);if (flags & UNI_DISPLAY_BACKSLASH) {
	         SensorCall(28779);switch (c) {
		 case '\n':
		     SensorCall(28780);ok = 'n'; SensorCall(28781);break;
		 case '\r':
		     SensorCall(28782);ok = 'r'; SensorCall(28783);break;
		 case '\t':
		     SensorCall(28784);ok = 't'; SensorCall(28785);break;
		 case '\f':
		     SensorCall(28786);ok = 'f'; SensorCall(28787);break;
		 case '\a':
		     SensorCall(28788);ok = 'a'; SensorCall(28789);break;
		 case '\\':
		     SensorCall(28790);ok = '\\'; SensorCall(28791);break;
		 default: SensorCall(28792);break;
		 }
		 SensorCall(28794);if (ok) {
		     SensorCall(28793);const char string = ok;
		     sv_catpvs(dsv, "\\");
		     sv_catpvn(dsv, &string, 1);
		 }
	     }
	     /* isPRINT() is the locale-blind version. */
	     SensorCall(28797);if (!ok && (flags & UNI_DISPLAY_ISPRINT) && isPRINT(c)) {
		 SensorCall(28796);const char string = c;
		 sv_catpvn(dsv, &string, 1);
		 ok = 1;
	     }
	 }
	 SensorCall(28800);if (!ok)
	     {/*57*/SensorCall(28799);Perl_sv_catpvf(aTHX_ dsv, "\\x{%"UVxf"}", u);/*58*/}
    }
    SensorCall(28802);if (truncated)
	 sv_catpvs(dsv, "...");

    {char * ReplaceReturn1105 = SvPVX(dsv); SensorCall(28803); return ReplaceReturn1105;}
}

/*
=for apidoc sv_uni_display

Build to the scalar C<dsv> a displayable version of the scalar C<sv>,
the displayable version being at most C<pvlim> bytes long
(if longer, the rest is truncated and "..." will be appended).

The C<flags> argument is as in L</pv_uni_display>().

The pointer to the PV of the C<dsv> is returned.

=cut
*/
char *
Perl_sv_uni_display(pTHX_ SV *dsv, SV *ssv, STRLEN pvlim, UV flags)
{
    PERL_ARGS_ASSERT_SV_UNI_DISPLAY;

     {char * ReplaceReturn1104 = Perl_pv_uni_display(aTHX_ dsv, (const U8*)SvPVX_const(ssv),
				SvCUR(ssv), pvlim, flags); SensorCall(28804); return ReplaceReturn1104;}
}

/*
=for apidoc foldEQ_utf8

Returns true if the leading portions of the strings C<s1> and C<s2> (either or both
of which may be in UTF-8) are the same case-insensitively; false otherwise.
How far into the strings to compare is determined by other input parameters.

If C<u1> is true, the string C<s1> is assumed to be in UTF-8-encoded Unicode;
otherwise it is assumed to be in native 8-bit encoding.  Correspondingly for C<u2>
with respect to C<s2>.

If the byte length C<l1> is non-zero, it says how far into C<s1> to check for fold
equality.  In other words, C<s1>+C<l1> will be used as a goal to reach.  The
scan will not be considered to be a match unless the goal is reached, and
scanning won't continue past that goal.  Correspondingly for C<l2> with respect to
C<s2>.

If C<pe1> is non-NULL and the pointer it points to is not NULL, that pointer is
considered an end pointer beyond which scanning of C<s1> will not continue under
any circumstances.  This means that if both C<l1> and C<pe1> are specified, and
C<pe1>
is less than C<s1>+C<l1>, the match will never be successful because it can
never
get as far as its goal (and in fact is asserted against).  Correspondingly for
C<pe2> with respect to C<s2>.

At least one of C<s1> and C<s2> must have a goal (at least one of C<l1> and
C<l2> must be non-zero), and if both do, both have to be
reached for a successful match.   Also, if the fold of a character is multiple
characters, all of them must be matched (see tr21 reference below for
'folding').

Upon a successful match, if C<pe1> is non-NULL,
it will be set to point to the beginning of the I<next> character of C<s1>
beyond what was matched.  Correspondingly for C<pe2> and C<s2>.

For case-insensitiveness, the "casefolding" of Unicode is used
instead of upper/lowercasing both the characters, see
L<http://www.unicode.org/unicode/reports/tr21/> (Case Mappings).

=cut */

/* A flags parameter has been added which may change, and hence isn't
 * externally documented.  Currently it is:
 *  0 for as-documented above
 *  FOLDEQ_UTF8_NOMIX_ASCII meaning that if a non-ASCII character folds to an
			    ASCII one, to not match
 *  FOLDEQ_UTF8_LOCALE	    meaning that locale rules are to be used for code
 *			    points below 256; unicode rules for above 255; and
 *			    folds that cross those boundaries are disallowed,
 *			    like the NOMIX_ASCII option
 *  FOLDEQ_S1_ALREADY_FOLDED s1 has already been folded before calling this
 *                           routine.  This allows that step to be skipped.
 *  FOLDEQ_S2_ALREADY_FOLDED   Similarly.
 */
I32
Perl_foldEQ_utf8_flags(pTHX_ const char *s1, char **pe1, register UV l1, bool u1, const char *s2, char **pe2, register UV l2, bool u2, U32 flags)
{
SensorCall(28805);    dVAR;
    register const U8 *p1  = (const U8*)s1; /* Point to current char */
    register const U8 *p2  = (const U8*)s2;
    register const U8 *g1 = NULL;       /* goal for s1 */
    register const U8 *g2 = NULL;
    register const U8 *e1 = NULL;       /* Don't scan s1 past this */
    register U8 *f1 = NULL;             /* Point to current folded */
    register const U8 *e2 = NULL;
    register U8 *f2 = NULL;
    STRLEN n1 = 0, n2 = 0;              /* Number of bytes in current char */
    U8 foldbuf1[UTF8_MAXBYTES_CASE+1];
    U8 foldbuf2[UTF8_MAXBYTES_CASE+1];

    PERL_ARGS_ASSERT_FOLDEQ_UTF8_FLAGS;

    /* The algorithm requires that input with the flags on the first line of
     * the assert not be pre-folded. */
    assert( ! ((flags & (FOLDEQ_UTF8_NOMIX_ASCII | FOLDEQ_UTF8_LOCALE))
	&& (flags & (FOLDEQ_S1_ALREADY_FOLDED | FOLDEQ_S2_ALREADY_FOLDED))));

    SensorCall(28807);if (pe1) {
        SensorCall(28806);e1 = *(U8**)pe1;
    }

    SensorCall(28809);if (l1) {
        SensorCall(28808);g1 = (const U8*)s1 + l1;
    }

    SensorCall(28811);if (pe2) {
        SensorCall(28810);e2 = *(U8**)pe2;
    }

    SensorCall(28813);if (l2) {
        SensorCall(28812);g2 = (const U8*)s2 + l2;
    }

    /* Must have at least one goal */
    assert(g1 || g2);

    SensorCall(28815);if (g1) {

        /* Will never match if goal is out-of-bounds */
        assert(! e1  || e1 >= g1);

        /* Here, there isn't an end pointer, or it is beyond the goal.  We
        * only go as far as the goal */
        SensorCall(28814);e1 = g1;
    }
    else {
	assert(e1);    /* Must have an end for looking at s1 */
    }

    /* Same for goal for s2 */
    SensorCall(28817);if (g2) {
        assert(! e2  || e2 >= g2);
        SensorCall(28816);e2 = g2;
    }
    else {
	assert(e2);
    }

    /* If both operands are already folded, we could just do a memEQ on the
     * whole strings at once, but it would be better if the caller realized
     * this and didn't even call us */

    /* Look through both strings, a character at a time */
    SensorCall(28861);while (p1 < e1 && p2 < e2) {

        /* If at the beginning of a new character in s1, get its fold to use
	 * and the length of the fold.  (exception: locale rules just get the
	 * character to a single byte) */
        SensorCall(28818);if (n1 == 0) {
	    SensorCall(28819);if (flags & FOLDEQ_S1_ALREADY_FOLDED) {
		SensorCall(28820);f1 = (U8 *) p1;
		n1 = UTF8SKIP(f1);
	    }

	    else {
		/* If in locale matching, we use two sets of rules, depending
		 * on if the code point is above or below 255.  Here, we test
		 * for and handle locale rules */
		SensorCall(28821);if ((flags & FOLDEQ_UTF8_LOCALE)
		    && (! u1 || UTF8_IS_INVARIANT(*p1)
			|| UTF8_IS_DOWNGRADEABLE_START(*p1)))
		{
		    /* There is no mixing of code points above and below 255. */
		    SensorCall(28822);if (u2 && (! UTF8_IS_INVARIANT(*p2)
			&& ! UTF8_IS_DOWNGRADEABLE_START(*p2)))
		    {
			{I32  ReplaceReturn1103 = 0; SensorCall(28823); return ReplaceReturn1103;}
		    }

		    /* We handle locale rules by converting, if necessary, the
		     * code point to a single byte. */
		    SensorCall(28826);if (! u1 || UTF8_IS_INVARIANT(*p1)) {
			SensorCall(28824);*foldbuf1 = *p1;
		    }
		    else {
			SensorCall(28825);*foldbuf1 = TWO_BYTE_UTF8_TO_UNI(*p1, *(p1 + 1));
		    }
		    SensorCall(28827);n1 = 1;
		}
		else {/*25*/SensorCall(28828);if (isASCII(*p1)) {    /* Note, that here won't be both
					       ASCII and using locale rules */

		    /* If trying to mix non- with ASCII, and not supposed to,
		     * fail */
		    SensorCall(28829);if ((flags & FOLDEQ_UTF8_NOMIX_ASCII) && ! isASCII(*p2)) {
			{I32  ReplaceReturn1102 = 0; SensorCall(28830); return ReplaceReturn1102;}
		    }
		    SensorCall(28831);n1 = 1;
		    *foldbuf1 = toLOWER(*p1);   /* Folds in the ASCII range are
						   just lowercased */
		}
		else {/*27*/SensorCall(28832);if (u1) {
		    to_utf8_fold(p1, foldbuf1, &n1);
		}
		else {  /* Not utf8, get utf8 fold */
		    to_uni_fold(NATIVE_TO_UNI(*p1), foldbuf1, &n1);
		;/*28*/}/*26*/}}
		SensorCall(28833);f1 = foldbuf1;
	    }
        }

        SensorCall(28851);if (n2 == 0) {    /* Same for s2 */
	    SensorCall(28834);if (flags & FOLDEQ_S2_ALREADY_FOLDED) {
		SensorCall(28835);f2 = (U8 *) p2;
		n2 = UTF8SKIP(f2);
	    }
	    else {
		SensorCall(28836);if ((flags & FOLDEQ_UTF8_LOCALE)
		    && (! u2 || UTF8_IS_INVARIANT(*p2) || UTF8_IS_DOWNGRADEABLE_START(*p2)))
		{
		    /* Here, the next char in s2 is < 256.  We've already
		     * worked on s1, and if it isn't also < 256, can't match */
		    SensorCall(28837);if (u1 && (! UTF8_IS_INVARIANT(*p1)
			&& ! UTF8_IS_DOWNGRADEABLE_START(*p1)))
		    {
			{I32  ReplaceReturn1101 = 0; SensorCall(28838); return ReplaceReturn1101;}
		    }
		    SensorCall(28841);if (! u2 || UTF8_IS_INVARIANT(*p2)) {
			SensorCall(28839);*foldbuf2 = *p2;
		    }
		    else {
			SensorCall(28840);*foldbuf2 = TWO_BYTE_UTF8_TO_UNI(*p2, *(p2 + 1));
		    }

		    /* Use another function to handle locale rules.  We've made
		     * sure that both characters to compare are single bytes */
		    SensorCall(28843);if (! foldEQ_locale((char *) f1, (char *) foldbuf2, 1)) {
			{I32  ReplaceReturn1100 = 0; SensorCall(28842); return ReplaceReturn1100;}
		    }
		    SensorCall(28844);n1 = n2 = 0;
		}
		else {/*29*/SensorCall(28845);if (isASCII(*p2)) {
		    SensorCall(28846);if ((flags & FOLDEQ_UTF8_NOMIX_ASCII) && ! isASCII(*p1)) {
			{I32  ReplaceReturn1099 = 0; SensorCall(28847); return ReplaceReturn1099;}
		    }
		    SensorCall(28848);n2 = 1;
		    *foldbuf2 = toLOWER(*p2);
		}
		else {/*31*/SensorCall(28849);if (u2) {
		    to_utf8_fold(p2, foldbuf2, &n2);
		}
		else {
		    to_uni_fold(NATIVE_TO_UNI(*p2), foldbuf2, &n2);
		;/*32*/}/*30*/}}
		SensorCall(28850);f2 = foldbuf2;
	    }
        }

	/* Here f1 and f2 point to the beginning of the strings to compare.
	 * These strings are the folds of the next character from each input
	 * string, stored in utf8. */

        /* While there is more to look for in both folds, see if they
        * continue to match */
        SensorCall(28856);while (n1 && n2) {
            SensorCall(28852);U8 fold_length = UTF8SKIP(f1);
            SensorCall(28854);if (fold_length != UTF8SKIP(f2)
                || (fold_length == 1 && *f1 != *f2) /* Short circuit memNE
                                                       function call for single
                                                       byte */
                || memNE((char*)f1, (char*)f2, fold_length))
            {
                {I32  ReplaceReturn1098 = 0; SensorCall(28853); return ReplaceReturn1098;} /* mismatch */
            }

            /* Here, they matched, advance past them */
            SensorCall(28855);n1 -= fold_length;
            f1 += fold_length;
            n2 -= fold_length;
            f2 += fold_length;
        }

        /* When reach the end of any fold, advance the input past it */
        SensorCall(28858);if (n1 == 0) {
            SensorCall(28857);p1 += u1 ? UTF8SKIP(p1) : 1;
        }
        SensorCall(28860);if (n2 == 0) {
            SensorCall(28859);p2 += u2 ? UTF8SKIP(p2) : 1;
        }
    } /* End of loop through both strings */

    /* A match is defined by each scan that specified an explicit length
    * reaching its final goal, and the other not having matched a partial
    * character (which can happen when the fold of a character is more than one
    * character). */
    SensorCall(28863);if (! ((g1 == 0 || p1 == g1) && (g2 == 0 || p2 == g2)) || n1 || n2) {
        {I32  ReplaceReturn1097 = 0; SensorCall(28862); return ReplaceReturn1097;}
    }

    /* Successful match.  Set output pointers */
    SensorCall(28865);if (pe1) {
        SensorCall(28864);*pe1 = (char*)p1;
    }
    SensorCall(28867);if (pe2) {
        SensorCall(28866);*pe2 = (char*)p2;
    }
    {I32  ReplaceReturn1096 = 1; SensorCall(28868); return ReplaceReturn1096;}
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 *
 * ex: set ts=8 sts=4 sw=4 noet:
 */
