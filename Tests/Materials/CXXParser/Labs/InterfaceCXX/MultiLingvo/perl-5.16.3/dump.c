#include "var/tmp/sensor.h"
/*    dump.c
 *
 *    Copyright (C) 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000,
 *    2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008 by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 *
 */

/*
 *  'You have talked long in your sleep, Frodo,' said Gandalf gently, 'and
 *   it has not been hard for me to read your mind and memory.'
 *
 *     [p.220 of _The Lord of the Rings_, II/i: "Many Meetings"]
 */

/* This file contains utility routines to dump the contents of SV and OP
 * structures, as used by command-line options like -Dt and -Dx, and
 * by Devel::Peek.
 *
 * It also holds the debugging version of the  runops function.
 */

#include "EXTERN.h"
#define PERL_IN_DUMP_C
#include "perl.h"
#include "regcomp.h"
#include "proto.h"


static const char* const svtypenames[SVt_LAST] = {
    "NULL",
    "BIND",
    "IV",
    "NV",
    "PV",
    "PVIV",
    "PVNV",
    "PVMG",
    "REGEXP",
    "PVGV",
    "PVLV",
    "PVAV",
    "PVHV",
    "PVCV",
    "PVFM",
    "PVIO"
};


static const char* const svshorttypenames[SVt_LAST] = {
    "UNDEF",
    "BIND",
    "IV",
    "NV",
    "PV",
    "PVIV",
    "PVNV",
    "PVMG",
    "REGEXP",
    "GV",
    "PVLV",
    "AV",
    "HV",
    "CV",
    "FM",
    "IO"
};

struct flag_to_name {
    U32 flag;
    const char *name;
};

static void
S_append_flags(pTHX_ SV *sv, U32 flags, const struct flag_to_name *start,
	       const struct flag_to_name *const end)
{
    SensorCall(1392);do {
	SensorCall(1393);if (flags & start->flag)
	    sv_catpv(sv, start->name);
    } while (++start < end);
SensorCall(1394);}

#define append_flags(sv, f, flags) \
    S_append_flags(aTHX_ (sv), (f), (flags), C_ARRAY_END(flags))



void
Perl_dump_indent(pTHX_ I32 level, PerlIO *file, const char* pat, ...)
{
    SensorCall(1395);va_list args;
    PERL_ARGS_ASSERT_DUMP_INDENT;
    va_start(args, pat);
    dump_vindent(level, file, pat, &args);
    va_end(args);
}

void
Perl_dump_vindent(pTHX_ I32 level, PerlIO *file, const char* pat, va_list *args)
{
SensorCall(1396);    dVAR;
    PERL_ARGS_ASSERT_DUMP_VINDENT;
    PerlIO_printf(file, "%*s", (int)(level*PL_dumpindent), "");
    PerlIO_vprintf(file, pat, *args);
SensorCall(1397);}

void
Perl_dump_all(pTHX)
{
SensorCall(1398);    dump_all_perl(FALSE);
SensorCall(1399);}

void
Perl_dump_all_perl(pTHX_ bool justperl)
{
SensorCall(1400);
    dVAR;
    PerlIO_setlinebuf(Perl_debug_log);
    SensorCall(1401);if (PL_main_root)
	op_dump(PL_main_root);
    dump_packsubs_perl(PL_defstash, justperl);
}

void
Perl_dump_packsubs(pTHX_ const HV *stash)
{
SensorCall(1402);    PERL_ARGS_ASSERT_DUMP_PACKSUBS;
    dump_packsubs_perl(stash, FALSE);
SensorCall(1403);}

void
Perl_dump_packsubs_perl(pTHX_ const HV *stash, bool justperl)
{
SensorCall(1404);    dVAR;
    I32	i;

    PERL_ARGS_ASSERT_DUMP_PACKSUBS_PERL;

    SensorCall(1406);if (!HvARRAY(stash))
	{/*1*/SensorCall(1405);return;/*2*/}
    SensorCall(1417);for (i = 0; i <= (I32) HvMAX(stash); i++) {
        SensorCall(1407);const HE *entry;
	SensorCall(1416);for (entry = HvARRAY(stash)[i]; entry; entry = HeNEXT(entry)) {
	    SensorCall(1408);const GV * const gv = (const GV *)HeVAL(entry);
	    SensorCall(1410);if (SvTYPE(gv) != SVt_PVGV || !GvGP(gv))
		{/*3*/SensorCall(1409);continue;/*4*/}
	    SensorCall(1411);if (GvCVu(gv))
		dump_sub_perl(gv, justperl);
	    SensorCall(1412);if (GvFORM(gv))
		dump_form(gv);
	    SensorCall(1415);if (HeKEY(entry)[HeKLEN(entry)-1] == ':') {
		SensorCall(1413);const HV * const hv = GvHV(gv);
		SensorCall(1414);if (hv && (hv != PL_defstash))
		    dump_packsubs_perl(hv, justperl); /* nested package */
	    }
	}
    }
SensorCall(1418);}

void
Perl_dump_sub(pTHX_ const GV *gv)
{
SensorCall(1419);    PERL_ARGS_ASSERT_DUMP_SUB;
    dump_sub_perl(gv, FALSE);
SensorCall(1420);}

void
Perl_dump_sub_perl(pTHX_ const GV *gv, bool justperl)
{
    SensorCall(1421);SV * sv;

    PERL_ARGS_ASSERT_DUMP_SUB_PERL;

    SensorCall(1423);if (justperl && (CvISXSUB(GvCV(gv)) || !CvROOT(GvCV(gv))))
	{/*5*/SensorCall(1422);return;/*6*/}

    SensorCall(1424);sv = sv_newmortal();
    gv_fullname3(sv, gv, NULL);
    Perl_dump_indent(aTHX_ 0, Perl_debug_log, "\nSUB %s = ", SvPVX_const(sv));
    SensorCall(1428);if (CvISXSUB(GvCV(gv)))
	{/*7*/SensorCall(1425);Perl_dump_indent(aTHX_ 0, Perl_debug_log, "(xsub 0x%"UVxf" %d)\n",
	    PTR2UV(CvXSUB(GvCV(gv))),
	    (int)CvXSUBANY(GvCV(gv)).any_i32);/*8*/}
    else {/*9*/SensorCall(1426);if (CvROOT(GvCV(gv)))
	op_dump(CvROOT(GvCV(gv)));
    else
	{/*11*/SensorCall(1427);Perl_dump_indent(aTHX_ 0, Perl_debug_log, "<undef>\n");/*12*/}/*10*/}
SensorCall(1429);}

void
Perl_dump_form(pTHX_ const GV *gv)
{
    SensorCall(1430);SV * const sv = sv_newmortal();

    PERL_ARGS_ASSERT_DUMP_FORM;

    gv_fullname3(sv, gv, NULL);
    Perl_dump_indent(aTHX_ 0, Perl_debug_log, "\nFORMAT %s = ", SvPVX_const(sv));
    SensorCall(1432);if (CvROOT(GvFORM(gv)))
	op_dump(CvROOT(GvFORM(gv)));
    else
	{/*13*/SensorCall(1431);Perl_dump_indent(aTHX_ 0, Perl_debug_log, "<undef>\n");/*14*/}
SensorCall(1433);}

void
Perl_dump_eval(pTHX)
{
SensorCall(1434);    dVAR;
    op_dump(PL_eval_root);
}


/*
=for apidoc pv_escape

Escapes at most the first "count" chars of pv and puts the results into
dsv such that the size of the escaped string will not exceed "max" chars
and will not contain any incomplete escape sequences.

If flags contains PERL_PV_ESCAPE_QUOTE then any double quotes in the string
will also be escaped.

Normally the SV will be cleared before the escaped string is prepared,
but when PERL_PV_ESCAPE_NOCLEAR is set this will not occur.

If PERL_PV_ESCAPE_UNI is set then the input string is treated as Unicode,
if PERL_PV_ESCAPE_UNI_DETECT is set then the input string is scanned
using C<is_utf8_string()> to determine if it is Unicode.

If PERL_PV_ESCAPE_ALL is set then all input chars will be output
using C<\x01F1> style escapes, otherwise if PERL_PV_ESCAPE_NONASCII is set, only
chars above 127 will be escaped using this style; otherwise, only chars above
255 will be so escaped; other non printable chars will use octal or
common escaped patterns like C<\n>. Otherwise, if PERL_PV_ESCAPE_NOBACKSLASH
then all chars below 255 will be treated as printable and
will be output as literals.

If PERL_PV_ESCAPE_FIRSTCHAR is set then only the first char of the
string will be escaped, regardless of max. If the output is to be in hex,
then it will be returned as a plain hex
sequence. Thus the output will either be a single char,
an octal escape sequence, a special escape like C<\n> or a hex value.

If PERL_PV_ESCAPE_RE is set then the escape char used will be a '%' and
not a '\\'. This is because regexes very often contain backslashed
sequences, whereas '%' is not a particularly common character in patterns.

Returns a pointer to the escaped text as held by dsv.

=cut
*/
#define PV_ESCAPE_OCTBUFSIZE 32

char *
Perl_pv_escape( pTHX_ SV *dsv, char const * const str, 
                const STRLEN count, const STRLEN max, 
                STRLEN * const escaped, const U32 flags ) 
{
    SensorCall(1435);const char esc = (flags & PERL_PV_ESCAPE_RE) ? '%' : '\\';
    const char dq = (flags & PERL_PV_ESCAPE_QUOTE) ? '"' : esc;
    char octbuf[PV_ESCAPE_OCTBUFSIZE] = "%123456789ABCDF";
    STRLEN wrote = 0;    /* chars written so far */
    STRLEN chsize = 0;   /* size of data to be written */
    STRLEN readsize = 1; /* size of data just read */
    bool isuni= flags & PERL_PV_ESCAPE_UNI ? 1 : 0; /* is this Unicode */
    const char *pv  = str;
    const char * const end = pv + count; /* end of string */
    octbuf[0] = esc;

    PERL_ARGS_ASSERT_PV_ESCAPE;

    SensorCall(1436);if (!(flags & PERL_PV_ESCAPE_NOCLEAR)) {
	    /* This won't alter the UTF-8 flag */
	    sv_setpvs(dsv, "");
    }
    
    SensorCall(1438);if ((flags & PERL_PV_ESCAPE_UNI_DETECT) && is_utf8_string((U8*)pv, count))
        {/*15*/SensorCall(1437);isuni = 1;/*16*/}
    
    SensorCall(1474);for ( ; (pv < end && (!max || (wrote < max))) ; pv += readsize ) {
        SensorCall(1439);const UV u= (isuni) ? utf8_to_uvchr_buf((U8*)pv, (U8*) end, &readsize) : (U8)*pv;
        const U8 c = (U8)u & 0xFF;
        
        SensorCall(1466);if ( ( u > 255 )
	  || (flags & PERL_PV_ESCAPE_ALL)
	  || (( u > 127 ) && (flags & PERL_PV_ESCAPE_NONASCII)))
	{
            SensorCall(1440);if (flags & PERL_PV_ESCAPE_FIRSTCHAR) 
                chsize = my_snprintf( octbuf, PV_ESCAPE_OCTBUFSIZE, 
                                      "%"UVxf, u);
            else
                chsize = my_snprintf( octbuf, PV_ESCAPE_OCTBUFSIZE, 
                                      "%cx{%"UVxf"}", esc, u);
        } else {/*17*/SensorCall(1441);if (flags & PERL_PV_ESCAPE_NOBACKSLASH) {
            SensorCall(1442);chsize = 1;            
        } else {         
            SensorCall(1443);if ( (c == dq) || (c == esc) || !isPRINT(c) ) {
	        SensorCall(1444);chsize = 2;
                SensorCall(1464);switch (c) {
                
		case '\\' : /* fallthrough */
		case '%'  : SensorCall(1445);if ( c == esc )  {
		                SensorCall(1446);octbuf[1] = esc;  
		            } else {
		                SensorCall(1447);chsize = 1;
		            }
		            SensorCall(1448);break;
		case '\v' : SensorCall(1449);octbuf[1] = 'v';  SensorCall(1450);break;
		case '\t' : SensorCall(1451);octbuf[1] = 't';  SensorCall(1452);break;
		case '\r' : SensorCall(1453);octbuf[1] = 'r';  SensorCall(1454);break;
		case '\n' : SensorCall(1455);octbuf[1] = 'n';  SensorCall(1456);break;
		case '\f' : SensorCall(1457);octbuf[1] = 'f';  SensorCall(1458);break;
                case '"'  : 
                        SensorCall(1459);if ( dq == '"' ) 
				{/*19*/SensorCall(1460);octbuf[1] = '"';/*20*/}
                        else 
                            {/*21*/SensorCall(1461);chsize = 1;/*22*/}
                        SensorCall(1462);break;
		default:
                        SensorCall(1463);if ( (pv+readsize < end) && isDIGIT((U8)*(pv+readsize)) )
                            chsize = my_snprintf( octbuf, PV_ESCAPE_OCTBUFSIZE, 
                                                  "%c%03o", esc, c);
			else
                            chsize = my_snprintf( octbuf, PV_ESCAPE_OCTBUFSIZE, 
                                                  "%c%o", esc, c);
                }
            } else {
                SensorCall(1465);chsize = 1;
            }
	;/*18*/}}
	SensorCall(1471);if ( max && (wrote + chsize > max) ) {
	    SensorCall(1467);break;
        } else {/*23*/SensorCall(1468);if (chsize > 1) {
            sv_catpvn(dsv, octbuf, chsize);
            SensorCall(1469);wrote += chsize;
	} else {
	    /* If PERL_PV_ESCAPE_NOBACKSLASH is set then bytes in the range
	       128-255 can be appended raw to the dsv. If dsv happens to be
	       UTF-8 then we need catpvf to upgrade them for us.
	       Or add a new API call sv_catpvc(). Think about that name, and
	       how to keep it clear that it's unlike the s of catpvs, which is
	       really an array octets, not a string.  */
            SensorCall(1470);Perl_sv_catpvf( aTHX_ dsv, "%c", c);
	    wrote++;
	;/*24*/}}
        SensorCall(1473);if ( flags & PERL_PV_ESCAPE_FIRSTCHAR ) 
            {/*25*/SensorCall(1472);break;/*26*/}
    }
    SensorCall(1476);if (escaped != NULL)
        {/*27*/SensorCall(1475);*escaped= pv - str;/*28*/}
    {char * ReplaceReturn2111 = SvPVX(dsv); SensorCall(1477); return ReplaceReturn2111;}
}
/*
=for apidoc pv_pretty

Converts a string into something presentable, handling escaping via
pv_escape() and supporting quoting and ellipses.

If the PERL_PV_PRETTY_QUOTE flag is set then the result will be 
double quoted with any double quotes in the string escaped. Otherwise
if the PERL_PV_PRETTY_LTGT flag is set then the result be wrapped in
angle brackets. 

If the PERL_PV_PRETTY_ELLIPSES flag is set and not all characters in
string were output then an ellipsis C<...> will be appended to the
string. Note that this happens AFTER it has been quoted.

If start_color is non-null then it will be inserted after the opening
quote (if there is one) but before the escaped text. If end_color
is non-null then it will be inserted after the escaped text but before
any quotes or ellipses.

Returns a pointer to the prettified text as held by dsv.

=cut           
*/

char *
Perl_pv_pretty( pTHX_ SV *dsv, char const * const str, const STRLEN count, 
  const STRLEN max, char const * const start_color, char const * const end_color, 
  const U32 flags ) 
{
    SensorCall(1478);const U8 dq = (flags & PERL_PV_PRETTY_QUOTE) ? '"' : '%';
    STRLEN escaped;
 
    PERL_ARGS_ASSERT_PV_PRETTY;
   
    SensorCall(1479);if (!(flags & PERL_PV_PRETTY_NOCLEAR)) {
	    /* This won't alter the UTF-8 flag */
	    sv_setpvs(dsv, "");
    }

    SensorCall(1480);if ( dq == '"' )
        sv_catpvs(dsv, "\"");
    else if ( flags & PERL_PV_PRETTY_LTGT )
        sv_catpvs(dsv, "<");
        
    SensorCall(1481);if ( start_color != NULL ) 
        sv_catpv(dsv, start_color);
    
    pv_escape( dsv, str, count, max, &escaped, flags | PERL_PV_ESCAPE_NOCLEAR );    
    
    SensorCall(1482);if ( end_color != NULL ) 
        sv_catpv(dsv, end_color);

    SensorCall(1483);if ( dq == '"' ) 
	sv_catpvs( dsv, "\"");
    else if ( flags & PERL_PV_PRETTY_LTGT )
        sv_catpvs(dsv, ">");         
    
    SensorCall(1484);if ( (flags & PERL_PV_PRETTY_ELLIPSES) && ( escaped < count ) )
	    sv_catpvs(dsv, "...");
 
    {char * ReplaceReturn2110 = SvPVX(dsv); SensorCall(1485); return ReplaceReturn2110;}
}

/*
=for apidoc pv_display

Similar to

  pv_escape(dsv,pv,cur,pvlim,PERL_PV_ESCAPE_QUOTE);

except that an additional "\0" will be appended to the string when
len > cur and pv[cur] is "\0".

Note that the final string may be up to 7 chars longer than pvlim.

=cut
*/

char *
Perl_pv_display(pTHX_ SV *dsv, const char *pv, STRLEN cur, STRLEN len, STRLEN pvlim)
{
SensorCall(1486);    PERL_ARGS_ASSERT_PV_DISPLAY;

    pv_pretty( dsv, pv, cur, pvlim, NULL, NULL, PERL_PV_PRETTY_DUMP);
    SensorCall(1487);if (len > cur && pv[cur] == '\0')
            sv_catpvs( dsv, "\\0");
    {char * ReplaceReturn2109 = SvPVX(dsv); SensorCall(1488); return ReplaceReturn2109;}
}

char *
Perl_sv_peek(pTHX_ SV *sv)
{
SensorCall(1489);    dVAR;
    SV * const t = sv_newmortal();
    int unref = 0;
    U32 type;

    sv_setpvs(t, "");
  retry:
    SensorCall(1515);if (!sv) {
	sv_catpv(t, "VOID");
	SensorCall(1490);goto finish;
    }
    else {/*29*/SensorCall(1491);if (sv == (const SV *)0x55555555 || SvTYPE(sv) == 'U') {
	sv_catpv(t, "WILD");
	SensorCall(1492);goto finish;
    }
    else {/*31*/SensorCall(1493);if (sv == &PL_sv_undef || sv == &PL_sv_no || sv == &PL_sv_yes || sv == &PL_sv_placeholder) {
	SensorCall(1494);if (sv == &PL_sv_undef) {
	    sv_catpv(t, "SV_UNDEF");
	    SensorCall(1496);if (!(SvFLAGS(sv) & (SVf_OK|SVf_OOK|SVs_OBJECT|
				 SVs_GMG|SVs_SMG|SVs_RMG)) &&
		SvREADONLY(sv))
		{/*33*/SensorCall(1495);goto finish;/*34*/}
	}
	else {/*35*/SensorCall(1497);if (sv == &PL_sv_no) {
	    sv_catpv(t, "SV_NO");
	    SensorCall(1499);if (!(SvFLAGS(sv) & (SVf_ROK|SVf_OOK|SVs_OBJECT|
				 SVs_GMG|SVs_SMG|SVs_RMG)) &&
		!(~SvFLAGS(sv) & (SVf_POK|SVf_NOK|SVf_READONLY|
				  SVp_POK|SVp_NOK)) &&
		SvCUR(sv) == 0 &&
		SvNVX(sv) == 0.0)
		{/*37*/SensorCall(1498);goto finish;/*38*/}
	}
	else {/*39*/SensorCall(1500);if (sv == &PL_sv_yes) {
	    sv_catpv(t, "SV_YES");
	    SensorCall(1502);if (!(SvFLAGS(sv) & (SVf_ROK|SVf_OOK|SVs_OBJECT|
				 SVs_GMG|SVs_SMG|SVs_RMG)) &&
		!(~SvFLAGS(sv) & (SVf_POK|SVf_NOK|SVf_READONLY|
				  SVp_POK|SVp_NOK)) &&
		SvCUR(sv) == 1 &&
		SvPVX_const(sv) && *SvPVX_const(sv) == '1' &&
		SvNVX(sv) == 1.0)
		{/*41*/SensorCall(1501);goto finish;/*42*/}
	}
	else {
	    sv_catpv(t, "SV_PLACEHOLDER");
	    SensorCall(1504);if (!(SvFLAGS(sv) & (SVf_OK|SVf_OOK|SVs_OBJECT|
				 SVs_GMG|SVs_SMG|SVs_RMG)) &&
		SvREADONLY(sv))
		{/*43*/SensorCall(1503);goto finish;/*44*/}
	;/*40*/}/*36*/}}
	sv_catpv(t, ":");
    }
    else {/*45*/SensorCall(1505);if (SvREFCNT(sv) == 0) {
	sv_catpv(t, "(");
	SensorCall(1506);unref++;
    }
    else {/*47*/SensorCall(1507);if (DEBUG_R_TEST_) {
	SensorCall(1508);int is_tmp = 0;
	I32 ix;
	/* is this SV on the tmps stack? */
	SensorCall(1512);for (ix=PL_tmps_ix; ix>=0; ix--) {
	    SensorCall(1509);if (PL_tmps_stack[ix] == sv) {
		SensorCall(1510);is_tmp = 1;
		SensorCall(1511);break;
	    }
	}
	SensorCall(1514);if (SvREFCNT(sv) > 1)
	    {/*49*/SensorCall(1513);Perl_sv_catpvf(aTHX_ t, "<%"UVuf"%s>", (UV)SvREFCNT(sv),
		    is_tmp ? "T" : "");/*50*/}
	else if (is_tmp)
	    sv_catpv(t, "<T>");
    ;/*48*/}/*46*/}/*32*/}/*30*/}}

    SensorCall(1521);if (SvROK(sv)) {
	sv_catpv(t, "\\");
	SensorCall(1518);if (SvCUR(t) + unref > 10) {
	    SvCUR_set(t, unref + 3);
	    SensorCall(1516);*SvEND(t) = '\0';
	    sv_catpv(t, "...");
	    SensorCall(1517);goto finish;
	}
	SensorCall(1519);sv = SvRV(sv);
	SensorCall(1520);goto retry;
    }
    SensorCall(1522);type = SvTYPE(sv);
    SensorCall(1529);if (type == SVt_PVCV) {
	SensorCall(1523);Perl_sv_catpvf(aTHX_ t, "CV(%s)", CvGV(sv) ? GvNAME(CvGV(sv)) : "");
	SensorCall(1524);goto finish;
    } else {/*51*/SensorCall(1525);if (type < SVt_LAST) {
	sv_catpv(t, svshorttypenames[type]);

	SensorCall(1527);if (type == SVt_NULL)
	    {/*53*/SensorCall(1526);goto finish;/*54*/}
    } else {
	sv_catpv(t, "FREED");
	SensorCall(1528);goto finish;
    ;/*52*/}}

    SensorCall(1537);if (SvPOKp(sv)) {
	SensorCall(1530);if (!SvPVX_const(sv))
	    sv_catpv(t, "(null)");
	else {
	    SensorCall(1531);SV * const tmp = newSVpvs("");
	    sv_catpv(t, "(");
	    SensorCall(1533);if (SvOOK(sv)) {
		SensorCall(1532);STRLEN delta;
		SvOOK_offset(sv, delta);
		Perl_sv_catpvf(aTHX_ t, "[%s]", pv_display(tmp, SvPVX_const(sv)-delta, delta, 0, 127));
	    }
	    SensorCall(1534);Perl_sv_catpvf(aTHX_ t, "%s)", pv_display(tmp, SvPVX_const(sv), SvCUR(sv), SvLEN(sv), 127));
	    SensorCall(1536);if (SvUTF8(sv))
		{/*55*/SensorCall(1535);Perl_sv_catpvf(aTHX_ t, " [UTF8 \"%s\"]",
			       sv_uni_display(tmp, sv, 6 * SvCUR(sv),
					      UNI_DISPLAY_QQ));/*56*/}
	    SvREFCNT_dec(tmp);
	}
    }
    else if (SvNOKp(sv)) {
	STORE_NUMERIC_LOCAL_SET_STANDARD();
	Perl_sv_catpvf(aTHX_ t, "(%"NVgf")",SvNVX(sv));
	RESTORE_NUMERIC_LOCAL();
    }
    else if (SvIOKp(sv)) {
	if (SvIsUV(sv))
	    {/*57*/Perl_sv_catpvf(aTHX_ t, "(%"UVuf")", (UV)SvUVX(sv));/*58*/}
	else
            {/*59*/Perl_sv_catpvf(aTHX_ t, "(%"IVdf")", (IV)SvIVX(sv));/*60*/}
    }
    else
	sv_catpv(t, "()");

  finish:
    SensorCall(1538);while (unref--)
	sv_catpv(t, ")");
    SensorCall(1539);if (PL_tainting && SvTAINTED(sv))
	sv_catpv(t, " [tainted]");
    {char * ReplaceReturn2108 = SvPV_nolen(t); SensorCall(1540); return ReplaceReturn2108;}
}

void
Perl_do_pmop_dump(pTHX_ I32 level, PerlIO *file, const PMOP *pm)
{
    SensorCall(1541);char ch;

    PERL_ARGS_ASSERT_DO_PMOP_DUMP;

    SensorCall(1544);if (!pm) {
	SensorCall(1542);Perl_dump_indent(aTHX_ level, file, "{}\n");
	SensorCall(1543);return;
    }
    SensorCall(1545);Perl_dump_indent(aTHX_ level, file, "{\n");
    level++;
    SensorCall(1548);if (pm->op_pmflags & PMf_ONCE)
	{/*61*/SensorCall(1546);ch = '?';/*62*/}
    else
	{/*63*/SensorCall(1547);ch = '/';/*64*/}
    SensorCall(1551);if (PM_GETRE(pm))
	{/*65*/SensorCall(1549);Perl_dump_indent(aTHX_ level, file, "PMf_PRE %c%s%c%s\n",
	     ch, RX_PRECOMP(PM_GETRE(pm)), ch,
	     (pm->op_private & OPpRUNTIME) ? " (RUNTIME)" : "");/*66*/}
    else
	{/*67*/SensorCall(1550);Perl_dump_indent(aTHX_ level, file, "PMf_PRE (RUNTIME)\n");/*68*/}
    SensorCall(1553);if (pm->op_type != OP_PUSHRE && pm->op_pmreplrootu.op_pmreplroot) {
	SensorCall(1552);Perl_dump_indent(aTHX_ level, file, "PMf_REPL = ");
	op_dump(pm->op_pmreplrootu.op_pmreplroot);
    }
    SensorCall(1555);if (pm->op_pmflags || (PM_GETRE(pm) && RX_CHECK_SUBSTR(PM_GETRE(pm)))) {
	SensorCall(1554);SV * const tmpsv = pm_description(pm);
	Perl_dump_indent(aTHX_ level, file, "PMFLAGS = (%s)\n", SvCUR(tmpsv) ? SvPVX_const(tmpsv) + 1 : "");
	SvREFCNT_dec(tmpsv);
    }

    SensorCall(1556);Perl_dump_indent(aTHX_ level-1, file, "}\n");
SensorCall(1557);}

const struct flag_to_name pmflags_flags_names[] = {
    {PMf_CONST, ",CONST"},
    {PMf_KEEP, ",KEEP"},
    {PMf_GLOBAL, ",GLOBAL"},
    {PMf_CONTINUE, ",CONTINUE"},
    {PMf_RETAINT, ",RETAINT"},
    {PMf_EVAL, ",EVAL"},
    {PMf_NONDESTRUCT, ",NONDESTRUCT"},
};

static SV *
S_pm_description(pTHX_ const PMOP *pm)
{
    SensorCall(1558);SV * const desc = newSVpvs("");
    const REGEXP * const regex = PM_GETRE(pm);
    const U32 pmflags = pm->op_pmflags;

    PERL_ARGS_ASSERT_PM_DESCRIPTION;

    SensorCall(1559);if (pmflags & PMf_ONCE)
	sv_catpv(desc, ",ONCE");
#ifdef USE_ITHREADS
    SensorCall(1560);if (SvREADONLY(PL_regex_pad[pm->op_pmoffset]))
        sv_catpv(desc, ":USED");
#else
    if (pmflags & PMf_USED)
        sv_catpv(desc, ":USED");
#endif

    SensorCall(1566);if (regex) {
        SensorCall(1561);if (RX_EXTFLAGS(regex) & RXf_TAINTED)
            sv_catpv(desc, ",TAINTED");
        SensorCall(1564);if (RX_CHECK_SUBSTR(regex)) {
            SensorCall(1562);if (!(RX_EXTFLAGS(regex) & RXf_NOSCAN))
                sv_catpv(desc, ",SCANFIRST");
            SensorCall(1563);if (RX_EXTFLAGS(regex) & RXf_CHECK_ALL)
                sv_catpv(desc, ",ALL");
        }
        SensorCall(1565);if (RX_EXTFLAGS(regex) & RXf_SKIPWHITE)
            sv_catpv(desc, ",SKIPWHITE");
    }

    append_flags(desc, pmflags, pmflags_flags_names);
    {SV * ReplaceReturn2107 = desc; SensorCall(1567); return ReplaceReturn2107;}
}

void
Perl_pmop_dump(pTHX_ PMOP *pm)
{
SensorCall(1568);    do_pmop_dump(0, Perl_debug_log, pm);
SensorCall(1569);}

/* Return a unique integer to represent the address of op o.
 * If it already exists in PL_op_sequence, just return it;
 * otherwise add it.
 *  *** Note that this isn't thread-safe */

STATIC UV
S_sequence_num(pTHX_ const OP *o)
{
SensorCall(1570);    dVAR;
    SV     *op,
          **seq;
    const char *key;
    STRLEN  len;
    SensorCall(1572);if (!o)
	{/*69*/{UV  ReplaceReturn2106 = 0; SensorCall(1571); return ReplaceReturn2106;}/*70*/}
    SensorCall(1573);op = newSVuv(PTR2UV(o));
    sv_2mortal(op);
    key = SvPV_const(op, len);
    SensorCall(1574);if (!PL_op_sequence)
	PL_op_sequence = newHV();
    SensorCall(1575);seq = hv_fetch(PL_op_sequence, key, len, 0);
    SensorCall(1576);if (seq)
	return SvUV(*seq);
    SensorCall(1577);(void)hv_store(PL_op_sequence, key, len, newSVuv(++PL_op_seq), 0);
    {UV  ReplaceReturn2105 = PL_op_seq; SensorCall(1578); return ReplaceReturn2105;}
}

const struct flag_to_name op_flags_names[] = {
    {OPf_KIDS, ",KIDS"},
    {OPf_PARENS, ",PARENS"},
    {OPf_REF, ",REF"},
    {OPf_MOD, ",MOD"},
    {OPf_STACKED, ",STACKED"},
    {OPf_SPECIAL, ",SPECIAL"}
};

const struct flag_to_name op_trans_names[] = {
    {OPpTRANS_FROM_UTF, ",FROM_UTF"},
    {OPpTRANS_TO_UTF, ",TO_UTF"},
    {OPpTRANS_IDENTICAL, ",IDENTICAL"},
    {OPpTRANS_SQUASH, ",SQUASH"},
    {OPpTRANS_COMPLEMENT, ",COMPLEMENT"},
    {OPpTRANS_GROWS, ",GROWS"},
    {OPpTRANS_DELETE, ",DELETE"}
};

const struct flag_to_name op_entersub_names[] = {
    {OPpENTERSUB_DB, ",DB"},
    {OPpENTERSUB_HASTARG, ",HASTARG"},
    {OPpENTERSUB_AMPER, ",AMPER"},
    {OPpENTERSUB_NOPAREN, ",NOPAREN"},
    {OPpENTERSUB_INARGS, ",INARGS"}
};

const struct flag_to_name op_const_names[] = {
    {OPpCONST_NOVER, ",NOVER"},
    {OPpCONST_SHORTCIRCUIT, ",SHORTCIRCUIT"},
    {OPpCONST_STRICT, ",STRICT"},
    {OPpCONST_ENTERED, ",ENTERED"},
    {OPpCONST_BARE, ",BARE"},
    {OPpCONST_WARNING, ",WARNING"}
};

const struct flag_to_name op_sort_names[] = {
    {OPpSORT_NUMERIC, ",NUMERIC"},
    {OPpSORT_INTEGER, ",INTEGER"},
    {OPpSORT_REVERSE, ",REVERSE"},
    {OPpSORT_INPLACE, ",INPLACE"},
    {OPpSORT_DESCEND, ",DESCEND"},
    {OPpSORT_QSORT, ",QSORT"},
    {OPpSORT_STABLE, ",STABLE"}
};

const struct flag_to_name op_open_names[] = {
    {OPpOPEN_IN_RAW, ",IN_RAW"},
    {OPpOPEN_IN_CRLF, ",IN_CRLF"},
    {OPpOPEN_OUT_RAW, ",OUT_RAW"},
    {OPpOPEN_OUT_CRLF, ",OUT_CRLF"}
};

const struct flag_to_name op_exit_names[] = {
    {OPpEXIT_VMSISH, ",EXIT_VMSISH"},
    {OPpHUSH_VMSISH, ",HUSH_VMSISH"}
};

#define OP_PRIVATE_ONCE(op, flag, name) \
    const struct flag_to_name CAT2(op, _names)[] = {	\
	{(flag), (name)} \
    }

OP_PRIVATE_ONCE(op_aassign, OPpASSIGN_COMMON, ",COMMON");
OP_PRIVATE_ONCE(op_leavesub, OPpREFCOUNTED, ",REFCOUNTED");
OP_PRIVATE_ONCE(op_sassign, OPpASSIGN_BACKWARDS, ",BACKWARDS");
OP_PRIVATE_ONCE(op_repeat, OPpREPEAT_DOLIST, ",DOLIST");
OP_PRIVATE_ONCE(op_reverse, OPpREVERSE_INPLACE, ",INPLACE");
OP_PRIVATE_ONCE(op_rv2cv, OPpLVAL_INTRO, ",INTRO");
OP_PRIVATE_ONCE(op_flip, OPpFLIP_LINENUM, ",LINENUM");
OP_PRIVATE_ONCE(op_gv, OPpEARLY_CV, ",EARLY_CV");
OP_PRIVATE_ONCE(op_list, OPpLIST_GUESSED, ",GUESSED");
OP_PRIVATE_ONCE(op_delete, OPpSLICE, ",SLICE");
OP_PRIVATE_ONCE(op_exists, OPpEXISTS_SUB, ",EXISTS_SUB");
OP_PRIVATE_ONCE(op_die, OPpHUSH_VMSISH, ",HUSH_VMSISH");

struct op_private_by_op {
    U16 op_type;
    U16 len;
    const struct flag_to_name *start;
};

const struct op_private_by_op op_private_names[] = {
    {OP_LEAVESUB, C_ARRAY_LENGTH(op_leavesub_names), op_leavesub_names },
    {OP_LEAVE, C_ARRAY_LENGTH(op_leavesub_names), op_leavesub_names },
    {OP_LEAVESUBLV, C_ARRAY_LENGTH(op_leavesub_names), op_leavesub_names },
    {OP_LEAVEWRITE, C_ARRAY_LENGTH(op_leavesub_names), op_leavesub_names },
    {OP_AASSIGN, C_ARRAY_LENGTH(op_aassign_names), op_aassign_names },
    {OP_DIE, C_ARRAY_LENGTH(op_die_names), op_die_names },
    {OP_DELETE, C_ARRAY_LENGTH(op_delete_names), op_delete_names },
    {OP_EXISTS, C_ARRAY_LENGTH(op_exists_names), op_exists_names },
    {OP_EXIT, C_ARRAY_LENGTH(op_exit_names), op_exit_names },
    {OP_FLIP, C_ARRAY_LENGTH(op_flip_names), op_flip_names },
    {OP_FLOP, C_ARRAY_LENGTH(op_flip_names), op_flip_names },
    {OP_GV, C_ARRAY_LENGTH(op_gv_names), op_gv_names },
    {OP_LIST, C_ARRAY_LENGTH(op_list_names), op_list_names },
    {OP_SASSIGN, C_ARRAY_LENGTH(op_sassign_names), op_sassign_names },
    {OP_REPEAT, C_ARRAY_LENGTH(op_repeat_names), op_repeat_names },
    {OP_RV2CV, C_ARRAY_LENGTH(op_rv2cv_names), op_rv2cv_names },
    {OP_TRANS, C_ARRAY_LENGTH(op_trans_names), op_trans_names },
    {OP_CONST, C_ARRAY_LENGTH(op_const_names), op_const_names },
    {OP_SORT, C_ARRAY_LENGTH(op_sort_names), op_sort_names },
    {OP_OPEN, C_ARRAY_LENGTH(op_open_names), op_open_names },
    {OP_BACKTICK, C_ARRAY_LENGTH(op_open_names), op_open_names }
};

static bool
S_op_private_to_names(pTHX_ SV *tmpsv, U32 optype, U32 op_private) {
    SensorCall(1579);const struct op_private_by_op *start = op_private_names;
    const struct op_private_by_op *const end
	= op_private_names + C_ARRAY_LENGTH(op_private_names);

    /* This is a linear search, but no worse than the code that it replaced.
       It's debugging code - size is more important than speed.  */
    SensorCall(1583);do {
	SensorCall(1580);if (optype == start->op_type) {
	    SensorCall(1581);S_append_flags(aTHX_ tmpsv, op_private, start->start,
			   start->start + start->len);
	    {_Bool  ReplaceReturn2104 = TRUE; SensorCall(1582); return ReplaceReturn2104;}
	}
    } while (++start < end);
    {_Bool  ReplaceReturn2103 = FALSE; SensorCall(1584); return ReplaceReturn2103;}
}

void
Perl_do_op_dump(pTHX_ I32 level, PerlIO *file, const OP *o)
{
SensorCall(1585);    dVAR;
    UV      seq;
    const OPCODE optype = o->op_type;

    PERL_ARGS_ASSERT_DO_OP_DUMP;

    Perl_dump_indent(aTHX_ level, file, "{\n");
    level++;
    seq = sequence_num(o);
    SensorCall(1588);if (seq)
	{/*71*/SensorCall(1586);PerlIO_printf(file, "%-4"UVuf, seq);/*72*/}
    else
	{/*73*/SensorCall(1587);PerlIO_printf(file, "????");/*74*/}
    SensorCall(1589);PerlIO_printf(file,
		  "%*sTYPE = %s  ===> ",
		  (int)(PL_dumpindent*level-4), "", OP_NAME(o));
    SensorCall(1592);if (o->op_next)
	{/*75*/SensorCall(1590);PerlIO_printf(file,
			o->op_type == OP_NULL ? "(%"UVuf")\n" : "%"UVuf"\n",
				sequence_num(o->op_next));/*76*/}
    else
	{/*77*/SensorCall(1591);PerlIO_printf(file, "NULL\n");/*78*/}
    SensorCall(1603);if (o->op_targ) {
	SensorCall(1593);if (optype == OP_NULL) {
	    SensorCall(1594);Perl_dump_indent(aTHX_ level, file, "  (was %s)\n", PL_op_name[o->op_targ]);
	    SensorCall(1601);if (o->op_targ == OP_NEXTSTATE) {
		SensorCall(1595);if (CopLINE(cCOPo))
		    {/*79*/SensorCall(1596);Perl_dump_indent(aTHX_ level, file, "LINE = %"UVuf"\n",
				     (UV)CopLINE(cCOPo));/*80*/}
		SensorCall(1598);if (CopSTASHPV(cCOPo))
		    {/*81*/SensorCall(1597);Perl_dump_indent(aTHX_ level, file, "PACKAGE = \"%s\"\n",
				     CopSTASHPV(cCOPo));/*82*/}
		SensorCall(1600);if (CopLABEL(cCOPo))
		    {/*83*/SensorCall(1599);Perl_dump_indent(aTHX_ level, file, "LABEL = \"%s\"\n",
				     CopLABEL(cCOPo));/*84*/}
	    }
	}
	else
	    {/*85*/SensorCall(1602);Perl_dump_indent(aTHX_ level, file, "TARG = %ld\n", (long)o->op_targ);/*86*/}
    }
#ifdef DUMPADDR
    Perl_dump_indent(aTHX_ level, file, "ADDR = 0x%"UVxf" => 0x%"UVxf"\n", (UV)o, (UV)o->op_next);
#endif
    SensorCall(1614);if (o->op_flags || o->op_latefree || o->op_latefreed || o->op_attached) {
	SensorCall(1604);SV * const tmpsv = newSVpvs("");
	SensorCall(1609);switch (o->op_flags & OPf_WANT) {
	case OPf_WANT_VOID:
	    sv_catpv(tmpsv, ",VOID");
	    SensorCall(1605);break;
	case OPf_WANT_SCALAR:
	    sv_catpv(tmpsv, ",SCALAR");
	    SensorCall(1606);break;
	case OPf_WANT_LIST:
	    sv_catpv(tmpsv, ",LIST");
	    SensorCall(1607);break;
	default:
	    sv_catpv(tmpsv, ",UNKNOWN");
	    SensorCall(1608);break;
	}
	append_flags(tmpsv, o->op_flags, op_flags_names);
	SensorCall(1610);if (o->op_latefree)
	    sv_catpv(tmpsv, ",LATEFREE");
	SensorCall(1611);if (o->op_latefreed)
	    sv_catpv(tmpsv, ",LATEFREED");
	SensorCall(1612);if (o->op_attached)
	    sv_catpv(tmpsv, ",ATTACHED");
	SensorCall(1613);Perl_dump_indent(aTHX_ level, file, "FLAGS = (%s)\n", SvCUR(tmpsv) ? SvPVX_const(tmpsv) + 1 : "");
	SvREFCNT_dec(tmpsv);
    }
    SensorCall(1636);if (o->op_private) {
	SensorCall(1615);SV * const tmpsv = newSVpvs("");
	SensorCall(1632);if (PL_opargs[optype] & OA_TARGLEX) {
	    SensorCall(1616);if (o->op_private & OPpTARGET_MY)
		sv_catpv(tmpsv, ",TARGET_MY");
	}
	else {/*87*/SensorCall(1617);if (optype == OP_ENTERSUB ||
	    optype == OP_RV2SV ||
	    optype == OP_GVSV ||
	    optype == OP_RV2AV ||
	    optype == OP_RV2HV ||
	    optype == OP_RV2GV ||
	    optype == OP_AELEM ||
	    optype == OP_HELEM )
	{
	    SensorCall(1618);if (optype == OP_ENTERSUB) {
		append_flags(tmpsv, o->op_private, op_entersub_names);
	    }
	    else {
		SensorCall(1619);switch (o->op_private & OPpDEREF) {
		case OPpDEREF_SV:
		    sv_catpv(tmpsv, ",SV");
		    SensorCall(1620);break;
		case OPpDEREF_AV:
		    sv_catpv(tmpsv, ",AV");
		    SensorCall(1621);break;
		case OPpDEREF_HV:
		    sv_catpv(tmpsv, ",HV");
		    SensorCall(1622);break;
		}
		SensorCall(1623);if (o->op_private & OPpMAYBE_LVSUB)
		    sv_catpv(tmpsv, ",MAYBE_LVSUB");
	    }

	    SensorCall(1627);if (optype == OP_AELEM || optype == OP_HELEM) {
		SensorCall(1624);if (o->op_private & OPpLVAL_DEFER)
		    sv_catpv(tmpsv, ",LVAL_DEFER");
	    }
	    else {
		SensorCall(1625);if (o->op_private & HINT_STRICT_REFS)
		    sv_catpv(tmpsv, ",STRICT_REFS");
		SensorCall(1626);if (o->op_private & OPpOUR_INTRO)
		    sv_catpv(tmpsv, ",OUR_INTRO");
	    }
	}
	else {/*89*/SensorCall(1628);if (S_op_private_to_names(aTHX_ tmpsv, optype, o->op_private)) {
	}
	else {/*91*/SensorCall(1629);if (PL_check[optype] != Perl_ck_ftst) {
	    SensorCall(1630);if (OP_IS_FILETEST_ACCESS(o->op_type) && o->op_private & OPpFT_ACCESS)
		sv_catpv(tmpsv, ",FT_ACCESS");
	    SensorCall(1631);if (o->op_private & OPpFT_STACKED)
		sv_catpv(tmpsv, ",FT_STACKED");
	;/*92*/}/*90*/}/*88*/}}
	SensorCall(1633);if (o->op_flags & OPf_MOD && o->op_private & OPpLVAL_INTRO)
	    sv_catpv(tmpsv, ",INTRO");
	SensorCall(1635);if (SvCUR(tmpsv))
	    {/*93*/SensorCall(1634);Perl_dump_indent(aTHX_ level, file, "PRIVATE = (%s)\n", SvPVX_const(tmpsv) + 1);/*94*/}
	SvREFCNT_dec(tmpsv);
    }

#ifdef PERL_MAD
    if (PL_madskills && o->op_madprop) {
	SV * const tmpsv = newSVpvs("");
	MADPROP* mp = o->op_madprop;
	Perl_dump_indent(aTHX_ level, file, "MADPROPS = {\n");
	level++;
	while (mp) {
	    const char tmp = mp->mad_key;
	    sv_setpvs(tmpsv,"'");
	    if (tmp)
		sv_catpvn(tmpsv, &tmp, 1);
	    sv_catpv(tmpsv, "'=");
	    switch (mp->mad_type) {
	    case MAD_NULL:
		sv_catpv(tmpsv, "NULL");
		Perl_dump_indent(aTHX_ level, file, "%s\n", SvPVX(tmpsv));
		break;
	    case MAD_PV:
		sv_catpv(tmpsv, "<");
		sv_catpvn(tmpsv, (char*)mp->mad_val, mp->mad_vlen);
		sv_catpv(tmpsv, ">");
		Perl_dump_indent(aTHX_ level, file, "%s\n", SvPVX(tmpsv));
		break;
	    case MAD_OP:
		if ((OP*)mp->mad_val) {
		    Perl_dump_indent(aTHX_ level, file, "%s\n", SvPVX(tmpsv));
		    do_op_dump(level, file, (OP*)mp->mad_val);
		}
		break;
	    default:
		sv_catpv(tmpsv, "(UNK)");
		Perl_dump_indent(aTHX_ level, file, "%s\n", SvPVX(tmpsv));
		break;
	    }
	    mp = mp->mad_next;
	}
	level--;
	Perl_dump_indent(aTHX_ level, file, "}\n");

	SvREFCNT_dec(tmpsv);
    }
#endif

    SensorCall(1670);switch (optype) {
    case OP_AELEMFAST:
    case OP_GVSV:
    case OP_GV:
#ifdef USE_ITHREADS
	SensorCall(1637);Perl_dump_indent(aTHX_ level, file, "PADIX = %" IVdf "\n", (IV)cPADOPo->op_padix);
#else
	if ( ! (o->op_flags & OPf_SPECIAL)) { /* not lexical */
	    if (cSVOPo->op_sv) {
		SV * const tmpsv = newSV(0);
		ENTER;
		SAVEFREESV(tmpsv);
#ifdef PERL_MAD
		/* FIXME - is this making unwarranted assumptions about the
		   UTF-8 cleanliness of the dump file handle?  */
		SvUTF8_on(tmpsv);
#endif
		gv_fullname3(tmpsv, MUTABLE_GV(cSVOPo->op_sv), NULL);
		Perl_dump_indent(aTHX_ level, file, "GV = %s\n",
				 SvPV_nolen_const(tmpsv));
		LEAVE;
	    }
	    else
		Perl_dump_indent(aTHX_ level, file, "GV = NULL\n");
	}
#endif
	SensorCall(1638);break;
    case OP_CONST:
    case OP_HINTSEVAL:
    case OP_METHOD_NAMED:
#ifndef USE_ITHREADS
	/* with ITHREADS, consts are stored in the pad, and the right pad
	 * may not be active here, so skip */
	Perl_dump_indent(aTHX_ level, file, "SV = %s\n", SvPEEK(cSVOPo_sv));
#endif
	SensorCall(1639);break;
    case OP_NEXTSTATE:
    case OP_DBSTATE:
	SensorCall(1640);if (CopLINE(cCOPo))
	    {/*95*/SensorCall(1641);Perl_dump_indent(aTHX_ level, file, "LINE = %"UVuf"\n",
			     (UV)CopLINE(cCOPo));/*96*/}
	SensorCall(1643);if (CopSTASHPV(cCOPo))
	    {/*97*/SensorCall(1642);Perl_dump_indent(aTHX_ level, file, "PACKAGE = \"%s\"\n",
			     CopSTASHPV(cCOPo));/*98*/}
	SensorCall(1645);if (CopLABEL(cCOPo))
	    {/*99*/SensorCall(1644);Perl_dump_indent(aTHX_ level, file, "LABEL = \"%s\"\n",
			     CopLABEL(cCOPo));/*100*/}
	SensorCall(1646);break;
    case OP_ENTERLOOP:
	SensorCall(1647);Perl_dump_indent(aTHX_ level, file, "REDO ===> ");
	SensorCall(1650);if (cLOOPo->op_redoop)
	    {/*101*/SensorCall(1648);PerlIO_printf(file, "%"UVuf"\n", sequence_num(cLOOPo->op_redoop));/*102*/}
	else
	    {/*103*/SensorCall(1649);PerlIO_printf(file, "DONE\n");/*104*/}
	SensorCall(1651);Perl_dump_indent(aTHX_ level, file, "NEXT ===> ");
	SensorCall(1654);if (cLOOPo->op_nextop)
	    {/*105*/SensorCall(1652);PerlIO_printf(file, "%"UVuf"\n", sequence_num(cLOOPo->op_nextop));/*106*/}
	else
	    {/*107*/SensorCall(1653);PerlIO_printf(file, "DONE\n");/*108*/}
	SensorCall(1655);Perl_dump_indent(aTHX_ level, file, "LAST ===> ");
	SensorCall(1658);if (cLOOPo->op_lastop)
	    {/*109*/SensorCall(1656);PerlIO_printf(file, "%"UVuf"\n", sequence_num(cLOOPo->op_lastop));/*110*/}
	else
	    {/*111*/SensorCall(1657);PerlIO_printf(file, "DONE\n");/*112*/}
	SensorCall(1659);break;
    case OP_COND_EXPR:
    case OP_RANGE:
    case OP_MAPWHILE:
    case OP_GREPWHILE:
    case OP_OR:
    case OP_AND:
	SensorCall(1660);Perl_dump_indent(aTHX_ level, file, "OTHER ===> ");
	SensorCall(1663);if (cLOGOPo->op_other)
	    {/*113*/SensorCall(1661);PerlIO_printf(file, "%"UVuf"\n", sequence_num(cLOGOPo->op_other));/*114*/}
	else
	    {/*115*/SensorCall(1662);PerlIO_printf(file, "DONE\n");/*116*/}
	SensorCall(1664);break;
    case OP_PUSHRE:
    case OP_MATCH:
    case OP_QR:
    case OP_SUBST:
	do_pmop_dump(level, file, cPMOPo);
	SensorCall(1665);break;
    case OP_LEAVE:
    case OP_LEAVEEVAL:
    case OP_LEAVESUB:
    case OP_LEAVESUBLV:
    case OP_LEAVEWRITE:
    case OP_SCOPE:
	SensorCall(1666);if (o->op_private & OPpREFCOUNTED)
	    {/*117*/SensorCall(1667);Perl_dump_indent(aTHX_ level, file, "REFCNT = %"UVuf"\n", (UV)o->op_targ);/*118*/}
	SensorCall(1668);break;
    default:
	SensorCall(1669);break;
    }
    SensorCall(1673);if (o->op_flags & OPf_KIDS) {
	SensorCall(1671);OP *kid;
	SensorCall(1672);for (kid = cUNOPo->op_first; kid; kid = kid->op_sibling)
	    do_op_dump(level, file, kid);
    }
    SensorCall(1674);Perl_dump_indent(aTHX_ level-1, file, "}\n");
SensorCall(1675);}

void
Perl_op_dump(pTHX_ const OP *o)
{
SensorCall(1676);    PERL_ARGS_ASSERT_OP_DUMP;
    do_op_dump(0, Perl_debug_log, o);
SensorCall(1677);}

void
Perl_gv_dump(pTHX_ GV *gv)
{
    SensorCall(1678);SV *sv;

    PERL_ARGS_ASSERT_GV_DUMP;

    SensorCall(1681);if (!gv) {
	SensorCall(1679);PerlIO_printf(Perl_debug_log, "{}\n");
	SensorCall(1680);return;
    }
    SensorCall(1682);sv = sv_newmortal();
    PerlIO_printf(Perl_debug_log, "{\n");
    gv_fullname3(sv, gv, NULL);
    Perl_dump_indent(aTHX_ 1, Perl_debug_log, "GV_NAME = %s", SvPVX_const(sv));
    SensorCall(1684);if (gv != GvEGV(gv)) {
	gv_efullname3(sv, GvEGV(gv), NULL);
	SensorCall(1683);Perl_dump_indent(aTHX_ 1, Perl_debug_log, "-> %s", SvPVX_const(sv));
    }
    SensorCall(1685);PerlIO_putc(Perl_debug_log, '\n');
    Perl_dump_indent(aTHX_ 0, Perl_debug_log, "}\n");
SensorCall(1686);}


/* map magic types to the symbolic names
 * (with the PERL_MAGIC_ prefixed stripped)
 */

static const struct { const char type; const char *name; } magic_names[] = {
#include "mg_names.c"
	/* this null string terminates the list */
	{ 0,                         NULL },
};

void
Perl_do_magic_dump(pTHX_ I32 level, PerlIO *file, const MAGIC *mg, I32 nest, I32 maxnest, bool dumpops, STRLEN pvlim)
{
    PERL_ARGS_ASSERT_DO_MAGIC_DUMP;

    SensorCall(1687);for (; mg; mg = mg->mg_moremagic) {
 	SensorCall(1688);Perl_dump_indent(aTHX_ level, file,
			 "  MAGIC = 0x%"UVxf"\n", PTR2UV(mg));
 	SensorCall(1694);if (mg->mg_virtual) {
            SensorCall(1689);const MGVTBL * const v = mg->mg_virtual;
	    SensorCall(1692);if (v >= PL_magic_vtables
		&& v < PL_magic_vtables + magic_vtable_max) {
		SensorCall(1690);const U32 i = v - PL_magic_vtables;
	        Perl_dump_indent(aTHX_ level, file, "    MG_VIRTUAL = &PL_vtbl_%s\n", PL_magic_vtable_names[i]);
	    }
	    else
	        {/*119*/SensorCall(1691);Perl_dump_indent(aTHX_ level, file, "    MG_VIRTUAL = 0x%"UVxf"\n", PTR2UV(v));/*120*/}
        }
	else
	    {/*121*/SensorCall(1693);Perl_dump_indent(aTHX_ level, file, "    MG_VIRTUAL = 0\n");/*122*/}

	SensorCall(1696);if (mg->mg_private)
	    {/*123*/SensorCall(1695);Perl_dump_indent(aTHX_ level, file, "    MG_PRIVATE = %d\n", mg->mg_private);/*124*/}

	{
	    SensorCall(1697);int n;
	    const char *name = NULL;
	    SensorCall(1701);for (n = 0; magic_names[n].name; n++) {
		SensorCall(1698);if (mg->mg_type == magic_names[n].type) {
		    SensorCall(1699);name = magic_names[n].name;
		    SensorCall(1700);break;
		}
	    }
	    SensorCall(1704);if (name)
		{/*125*/SensorCall(1702);Perl_dump_indent(aTHX_ level, file,
				"    MG_TYPE = PERL_MAGIC_%s\n", name);/*126*/}
	    else
		{/*127*/SensorCall(1703);Perl_dump_indent(aTHX_ level, file,
				"    MG_TYPE = UNKNOWN(\\%o)\n", mg->mg_type);/*128*/}
	}

        SensorCall(1720);if (mg->mg_flags) {
            SensorCall(1705);Perl_dump_indent(aTHX_ level, file, "    MG_FLAGS = 0x%02X\n", mg->mg_flags);
	    SensorCall(1707);if (mg->mg_type == PERL_MAGIC_envelem &&
		mg->mg_flags & MGf_TAINTEDDIR)
	        {/*129*/SensorCall(1706);Perl_dump_indent(aTHX_ level, file, "      TAINTEDDIR\n");/*130*/}
	    SensorCall(1709);if (mg->mg_type == PERL_MAGIC_regex_global &&
		mg->mg_flags & MGf_MINMATCH)
	        {/*131*/SensorCall(1708);Perl_dump_indent(aTHX_ level, file, "      MINMATCH\n");/*132*/}
	    SensorCall(1711);if (mg->mg_flags & MGf_REFCOUNTED)
	        {/*133*/SensorCall(1710);Perl_dump_indent(aTHX_ level, file, "      REFCOUNTED\n");/*134*/}
            SensorCall(1713);if (mg->mg_flags & MGf_GSKIP)
	        {/*135*/SensorCall(1712);Perl_dump_indent(aTHX_ level, file, "      GSKIP\n");/*136*/}
	    SensorCall(1715);if (mg->mg_flags & MGf_COPY)
	        {/*137*/SensorCall(1714);Perl_dump_indent(aTHX_ level, file, "      COPY\n");/*138*/}
	    SensorCall(1717);if (mg->mg_flags & MGf_DUP)
	        {/*139*/SensorCall(1716);Perl_dump_indent(aTHX_ level, file, "      DUP\n");/*140*/}
	    SensorCall(1719);if (mg->mg_flags & MGf_LOCAL)
	        {/*141*/SensorCall(1718);Perl_dump_indent(aTHX_ level, file, "      LOCAL\n");/*142*/}
        }
	SensorCall(1725);if (mg->mg_obj) {
	    SensorCall(1721);Perl_dump_indent(aTHX_ level, file, "    MG_OBJ = 0x%"UVxf"\n",
	        PTR2UV(mg->mg_obj));
            SensorCall(1723);if (mg->mg_type == PERL_MAGIC_qr) {
		SensorCall(1722);REGEXP* const re = (REGEXP *)mg->mg_obj;
		SV * const dsv = sv_newmortal();
                const char * const s
		    = pv_pretty(dsv, RX_WRAPPED(re), RX_WRAPLEN(re),
                    60, NULL, NULL,
                    ( PERL_PV_PRETTY_QUOTE | PERL_PV_ESCAPE_RE | PERL_PV_PRETTY_ELLIPSES |
                    (RX_UTF8(re) ? PERL_PV_ESCAPE_UNI : 0))
                );
		Perl_dump_indent(aTHX_ level+1, file, "    PAT = %s\n", s);
		Perl_dump_indent(aTHX_ level+1, file, "    REFCNT = %"IVdf"\n",
			(IV)RX_REFCNT(re));
            }
            SensorCall(1724);if (mg->mg_flags & MGf_REFCOUNTED)
		do_sv_dump(level+2, file, mg->mg_obj, nest+1, maxnest, dumpops, pvlim); /* MG is already +1 */
	}
        SensorCall(1727);if (mg->mg_len)
	    {/*143*/SensorCall(1726);Perl_dump_indent(aTHX_ level, file, "    MG_LEN = %ld\n", (long)mg->mg_len);/*144*/}
        SensorCall(1738);if (mg->mg_ptr) {
	    SensorCall(1728);Perl_dump_indent(aTHX_ level, file, "    MG_PTR = 0x%"UVxf, PTR2UV(mg->mg_ptr));
	    SensorCall(1736);if (mg->mg_len >= 0) {
		SensorCall(1729);if (mg->mg_type != PERL_MAGIC_utf8) {
		    SensorCall(1730);SV * const sv = newSVpvs("");
		    PerlIO_printf(file, " %s", pv_display(sv, mg->mg_ptr, mg->mg_len, 0, pvlim));
		    SvREFCNT_dec(sv);
		}
            }
	    else {/*145*/SensorCall(1731);if (mg->mg_len == HEf_SVKEY) {
		SensorCall(1732);PerlIO_puts(file, " => HEf_SVKEY\n");
		do_sv_dump(level+2, file, MUTABLE_SV(((mg)->mg_ptr)), nest+1,
			   maxnest, dumpops, pvlim); /* MG is already +1 */
		SensorCall(1733);continue;
	    }
	    else {/*147*/SensorCall(1734);if (mg->mg_len == -1 && mg->mg_type == PERL_MAGIC_utf8);
	    else
		{/*151*/SensorCall(1735);PerlIO_puts(
		  file,
		 " ???? - " __FILE__
		 " does not know how to handle this MG_LEN"
		);/*152*/}/*148*/}/*146*/}
            SensorCall(1737);PerlIO_putc(file, '\n');
        }
	SensorCall(1744);if (mg->mg_type == PERL_MAGIC_utf8) {
	    SensorCall(1739);const STRLEN * const cache = (STRLEN *) mg->mg_ptr;
	    SensorCall(1743);if (cache) {
		SensorCall(1740);IV i;
		SensorCall(1742);for (i = 0; i < PERL_MAGIC_UTF8_CACHESIZE; i++)
		    {/*153*/SensorCall(1741);Perl_dump_indent(aTHX_ level, file,
				     "      %2"IVdf": %"UVuf" -> %"UVuf"\n",
				     i,
				     (UV)cache[i * 2],
				     (UV)cache[i * 2 + 1]);/*154*/}
	    }
	}
    }
SensorCall(1745);}

void
Perl_magic_dump(pTHX_ const MAGIC *mg)
{
SensorCall(1746);    do_magic_dump(0, Perl_debug_log, mg, 0, 0, FALSE, 0);
SensorCall(1747);}

void
Perl_do_hv_dump(pTHX_ I32 level, PerlIO *file, const char *name, HV *sv)
{
    SensorCall(1748);const char *hvname;

    PERL_ARGS_ASSERT_DO_HV_DUMP;

    Perl_dump_indent(aTHX_ level, file, "%s = 0x%"UVxf, name, PTR2UV(sv));
    SensorCall(1751);if (sv && (hvname = HvNAME_get(sv)))
    {
	/* we have to use pv_display and HvNAMELEN_get() so that we display the real package
           name which quite legally could contain insane things like tabs, newlines, nulls or
           other scary crap - this should produce sane results - except maybe for unicode package
           names - but we will wait for someone to file a bug on that - demerphq */
        SensorCall(1749);SV * const tmpsv = newSVpvs("");
        PerlIO_printf(file, "\t%s\n", pv_display(tmpsv, hvname, HvNAMELEN_get(sv), 0, 1024));
    }
    else
	{/*155*/SensorCall(1750);PerlIO_putc(file, '\n');/*156*/}
SensorCall(1752);}

void
Perl_do_gv_dump(pTHX_ I32 level, PerlIO *file, const char *name, GV *sv)
{
    PERL_ARGS_ASSERT_DO_GV_DUMP;

    SensorCall(1753);Perl_dump_indent(aTHX_ level, file, "%s = 0x%"UVxf, name, PTR2UV(sv));
    SensorCall(1756);if (sv && GvNAME(sv))
	{/*157*/SensorCall(1754);PerlIO_printf(file, "\t\"%s\"\n", GvNAME(sv));/*158*/}
    else
	{/*159*/SensorCall(1755);PerlIO_putc(file, '\n');/*160*/}
SensorCall(1757);}

void
Perl_do_gvgv_dump(pTHX_ I32 level, PerlIO *file, const char *name, GV *sv)
{
    PERL_ARGS_ASSERT_DO_GVGV_DUMP;

    SensorCall(1758);Perl_dump_indent(aTHX_ level, file, "%s = 0x%"UVxf, name, PTR2UV(sv));
    SensorCall(1764);if (sv && GvNAME(sv)) {
	SensorCall(1759);const char *hvname;
	PerlIO_printf(file, "\t\"");
	SensorCall(1761);if (GvSTASH(sv) && (hvname = HvNAME_get(GvSTASH(sv))))
	    {/*161*/SensorCall(1760);PerlIO_printf(file, "%s\" :: \"", hvname);/*162*/}
	SensorCall(1762);PerlIO_printf(file, "%s\"\n", GvNAME(sv));
    }
    else
	{/*163*/SensorCall(1763);PerlIO_putc(file, '\n');/*164*/}
SensorCall(1765);}

const struct flag_to_name first_sv_flags_names[] = {
    {SVs_TEMP, "TEMP,"},
    {SVs_OBJECT, "OBJECT,"},
    {SVs_GMG, "GMG,"},
    {SVs_SMG, "SMG,"},
    {SVs_RMG, "RMG,"},
    {SVf_IOK, "IOK,"},
    {SVf_NOK, "NOK,"},
    {SVf_POK, "POK,"}
};

const struct flag_to_name second_sv_flags_names[] = {
    {SVf_OOK, "OOK,"},
    {SVf_FAKE, "FAKE,"},
    {SVf_READONLY, "READONLY,"},
    {SVf_BREAK, "BREAK,"},
    {SVf_AMAGIC, "OVERLOAD,"},
    {SVp_IOK, "pIOK,"},
    {SVp_NOK, "pNOK,"},
    {SVp_POK, "pPOK,"}
};

const struct flag_to_name cv_flags_names[] = {
    {CVf_ANON, "ANON,"},
    {CVf_UNIQUE, "UNIQUE,"},
    {CVf_CLONE, "CLONE,"},
    {CVf_CLONED, "CLONED,"},
    {CVf_CONST, "CONST,"},
    {CVf_NODEBUG, "NODEBUG,"},
    {CVf_LVALUE, "LVALUE,"},
    {CVf_METHOD, "METHOD,"},
    {CVf_WEAKOUTSIDE, "WEAKOUTSIDE,"},
    {CVf_CVGV_RC, "CVGV_RC,"},
    {CVf_DYNFILE, "DYNFILE,"},
    {CVf_AUTOLOAD, "AUTOLOAD,"},
    {CVf_ISXSUB, "ISXSUB,"}
};

const struct flag_to_name hv_flags_names[] = {
    {SVphv_SHAREKEYS, "SHAREKEYS,"},
    {SVphv_LAZYDEL, "LAZYDEL,"},
    {SVphv_HASKFLAGS, "HASKFLAGS,"},
    {SVphv_REHASH, "REHASH,"},
    {SVphv_CLONEABLE, "CLONEABLE,"}
};

const struct flag_to_name gp_flags_names[] = {
    {GVf_INTRO, "INTRO,"},
    {GVf_MULTI, "MULTI,"},
    {GVf_ASSUMECV, "ASSUMECV,"},
    {GVf_IN_PAD, "IN_PAD,"}
};

const struct flag_to_name gp_flags_imported_names[] = {
    {GVf_IMPORTED_SV, " SV"},
    {GVf_IMPORTED_AV, " AV"},
    {GVf_IMPORTED_HV, " HV"},
    {GVf_IMPORTED_CV, " CV"},
};

const struct flag_to_name regexp_flags_names[] = {
    {RXf_PMf_MULTILINE,   "PMf_MULTILINE,"},
    {RXf_PMf_SINGLELINE,  "PMf_SINGLELINE,"},
    {RXf_PMf_FOLD,        "PMf_FOLD,"},
    {RXf_PMf_EXTENDED,    "PMf_EXTENDED,"},
    {RXf_PMf_KEEPCOPY,    "PMf_KEEPCOPY,"},
    {RXf_ANCH_BOL,        "ANCH_BOL,"},
    {RXf_ANCH_MBOL,       "ANCH_MBOL,"},
    {RXf_ANCH_SBOL,       "ANCH_SBOL,"},
    {RXf_ANCH_GPOS,       "ANCH_GPOS,"},
    {RXf_GPOS_SEEN,       "GPOS_SEEN,"},
    {RXf_GPOS_FLOAT,      "GPOS_FLOAT,"},
    {RXf_LOOKBEHIND_SEEN, "LOOKBEHIND_SEEN,"},
    {RXf_EVAL_SEEN,       "EVAL_SEEN,"},
    {RXf_CANY_SEEN,       "CANY_SEEN,"},
    {RXf_NOSCAN,          "NOSCAN,"},
    {RXf_CHECK_ALL,       "CHECK_ALL,"},
    {RXf_MATCH_UTF8,      "MATCH_UTF8,"},
    {RXf_USE_INTUIT_NOML, "USE_INTUIT_NOML,"},
    {RXf_USE_INTUIT_ML,   "USE_INTUIT_ML,"},
    {RXf_INTUIT_TAIL,     "INTUIT_TAIL,"},
    {RXf_SPLIT,           "SPLIT,"},
    {RXf_COPY_DONE,       "COPY_DONE,"},
    {RXf_TAINTED_SEEN,    "TAINTED_SEEN,"},
    {RXf_TAINTED,         "TAINTED,"},
    {RXf_START_ONLY,      "START_ONLY,"},
    {RXf_SKIPWHITE,       "SKIPWHITE,"},
    {RXf_WHITE,           "WHITE,"},
    {RXf_NULL,            "NULL,"},
};

void
Perl_do_sv_dump(pTHX_ I32 level, PerlIO *file, SV *sv, I32 nest, I32 maxnest, bool dumpops, STRLEN pvlim)
{
SensorCall(1766);    dVAR;
    SV *d;
    const char *s;
    U32 flags;
    U32 type;

    PERL_ARGS_ASSERT_DO_SV_DUMP;

    SensorCall(1769);if (!sv) {
	SensorCall(1767);Perl_dump_indent(aTHX_ level, file, "SV = 0\n");
	SensorCall(1768);return;
    }

    SensorCall(1770);flags = SvFLAGS(sv);
    type = SvTYPE(sv);

    /* process general SV flags */

    d = Perl_newSVpvf(aTHX_
		   "(0x%"UVxf") at 0x%"UVxf"\n%*s  REFCNT = %"IVdf"\n%*s  FLAGS = (",
		   PTR2UV(SvANY(sv)), PTR2UV(sv),
		   (int)(PL_dumpindent*level), "", (IV)SvREFCNT(sv),
		   (int)(PL_dumpindent*level), "");

    SensorCall(1772);if (!((flags & SVpad_NAME) == SVpad_NAME
	  && (type == SVt_PVMG || type == SVt_PVNV))) {
	SensorCall(1771);if ((flags & SVs_PADMY) && (flags & SVs_PADSTALE))
	    sv_catpv(d, "PADSTALE,");
    }
    SensorCall(1775);if (!((flags & SVpad_NAME) == SVpad_NAME && type == SVt_PVMG)) {
	SensorCall(1773);if (!(flags & SVs_PADMY) && (flags & SVs_PADTMP))
	    sv_catpv(d, "PADTMP,");
	SensorCall(1774);if (flags & SVs_PADMY)	sv_catpv(d, "PADMY,");
    }
    append_flags(d, flags, first_sv_flags_names);
    SensorCall(1777);if (flags & SVf_ROK)  {	
    				sv_catpv(d, "ROK,");
	SensorCall(1776);if (SvWEAKREF(sv))	sv_catpv(d, "WEAKREF,");
    }
    append_flags(d, flags, second_sv_flags_names);
    SensorCall(1779);if (flags & SVp_SCREAM && type != SVt_PVHV && !isGV_with_GP(sv)) {
	SensorCall(1778);if (SvPCS_IMPORTED(sv))
				sv_catpv(d, "PCS_IMPORTED,");
	else
				sv_catpv(d, "SCREAM,");
    }

    /* process type-specific SV flags */

    SensorCall(1795);switch (type) {
    case SVt_PVCV:
    case SVt_PVFM:
	append_flags(d, CvFLAGS(sv), cv_flags_names);
	SensorCall(1780);break;
    case SVt_PVHV:
	append_flags(d, flags, hv_flags_names);
	SensorCall(1781);break;
    case SVt_PVGV:
    case SVt_PVLV:
	SensorCall(1782);if (isGV_with_GP(sv)) {
	    append_flags(d, GvFLAGS(sv), gp_flags_names);
	}
	SensorCall(1784);if (isGV_with_GP(sv) && GvIMPORTED(sv)) {
	    sv_catpv(d, "IMPORT");
	    SensorCall(1783);if (GvIMPORTED(sv) == GVf_IMPORTED)
		sv_catpv(d, "ALL,");
	    else {
		sv_catpv(d, "(");
		append_flags(d, GvFLAGS(sv), gp_flags_imported_names);
		sv_catpv(d, " ),");
	    }
	}
	/* FALL THROUGH */
    default:
    evaled_or_uv:
	SensorCall(1785);if (SvEVALED(sv))	sv_catpv(d, "EVALED,");
	SensorCall(1786);if (SvIsUV(sv) && !(flags & SVf_ROK))	sv_catpv(d, "IsUV,");
	SensorCall(1787);break;
    case SVt_PVMG:
	SensorCall(1788);if (SvTAIL(sv))		sv_catpv(d, "TAIL,");
	SensorCall(1789);if (SvVALID(sv))	sv_catpv(d, "VALID,");
	SensorCall(1790);if (SvPAD_TYPED(sv))	sv_catpv(d, "TYPED,");
	SensorCall(1791);if (SvPAD_OUR(sv))	sv_catpv(d, "OUR,");
	/* FALL THROUGH */
    case SVt_PVNV:
	SensorCall(1792);if (SvPAD_STATE(sv))	sv_catpv(d, "STATE,");
	SensorCall(1793);goto evaled_or_uv;
    case SVt_PVAV:
	SensorCall(1794);break;
    }
    /* SVphv_SHAREKEYS is also 0x20000000 */
    SensorCall(1796);if ((type != SVt_PVHV) && SvUTF8(sv))
        sv_catpv(d, "UTF8");

    SensorCall(1797);if (*(SvEND(d) - 1) == ',') {
        SvCUR_set(d, SvCUR(d) - 1);
	SvPVX(d)[SvCUR(d)] = '\0';
    }
    sv_catpv(d, ")");
    SensorCall(1798);s = SvPVX_const(d);

    /* dump initial SV details */

#ifdef DEBUG_LEAKING_SCALARS
    Perl_dump_indent(aTHX_ level, file,
	"ALLOCATED at %s:%d %s %s (parent 0x%"UVxf"); serial %"UVuf"\n",
	sv->sv_debug_file ? sv->sv_debug_file : "(unknown)",
	sv->sv_debug_line,
	sv->sv_debug_inpad ? "for" : "by",
	sv->sv_debug_optype ? PL_op_name[sv->sv_debug_optype]: "(none)",
	PTR2UV(sv->sv_debug_parent),
	sv->sv_debug_serial
    );
#endif
    Perl_dump_indent(aTHX_ level, file, "SV = ");

    /* Dump SV type */

    SensorCall(1804);if (type < SVt_LAST) {
	SensorCall(1799);PerlIO_printf(file, "%s%s\n", svtypenames[type], s);

	SensorCall(1801);if (type ==  SVt_NULL) {
	    SvREFCNT_dec(d);
	    SensorCall(1800);return;
	}
    } else {
	SensorCall(1802);PerlIO_printf(file, "UNKNOWN(0x%"UVxf") %s\n", (UV)type, s);
	SvREFCNT_dec(d);
	SensorCall(1803);return;
    }

    /* Dump general SV fields */

    SensorCall(1809);if ((type >= SVt_PVIV && type != SVt_PVAV && type != SVt_PVHV
	 && type != SVt_PVCV && type != SVt_PVFM && type != SVt_PVIO
	 && type != SVt_REGEXP && !isGV_with_GP(sv) && !SvVALID(sv))
	|| (type == SVt_IV && !SvROK(sv))) {
	SensorCall(1805);if (SvIsUV(sv)
#ifdef PERL_OLD_COPY_ON_WRITE
	               || SvIsCOW(sv)
#endif
	                             )
	    {/*165*/SensorCall(1806);Perl_dump_indent(aTHX_ level, file, "  UV = %"UVuf, (UV)SvUVX(sv));/*166*/}
	else
	    {/*167*/SensorCall(1807);Perl_dump_indent(aTHX_ level, file, "  IV = %"IVdf, (IV)SvIVX(sv));/*168*/}
#ifdef PERL_OLD_COPY_ON_WRITE
	if (SvIsCOW_shared_hash(sv))
	    PerlIO_printf(file, "  (HASH)");
	else if (SvIsCOW_normal(sv))
	    PerlIO_printf(file, "  (COW from 0x%"UVxf")", (UV)SvUVX(sv));
#endif
	SensorCall(1808);PerlIO_putc(file, '\n');
    }

    SensorCall(1813);if ((type == SVt_PVNV || type == SVt_PVMG)
	&& (SvFLAGS(sv) & SVpad_NAME) == SVpad_NAME) {
	SensorCall(1810);Perl_dump_indent(aTHX_ level, file, "  COP_LOW = %"UVuf"\n",
			 (UV) COP_SEQ_RANGE_LOW(sv));
	Perl_dump_indent(aTHX_ level, file, "  COP_HIGH = %"UVuf"\n",
			 (UV) COP_SEQ_RANGE_HIGH(sv));
    } else {/*169*/SensorCall(1811);if ((type >= SVt_PVNV && type != SVt_PVAV && type != SVt_PVHV
		&& type != SVt_PVCV && type != SVt_PVFM  && type != SVt_REGEXP
		&& type != SVt_PVIO && !isGV_with_GP(sv) && !SvVALID(sv))
	       || type == SVt_NV) {
	STORE_NUMERIC_LOCAL_SET_STANDARD();
	/* %Vg doesn't work? --jhi */
#ifdef USE_LONG_DOUBLE
	Perl_dump_indent(aTHX_ level, file, "  NV = %.*" PERL_PRIgldbl "\n", LDBL_DIG, SvNVX(sv));
#else
	SensorCall(1812);Perl_dump_indent(aTHX_ level, file, "  NV = %.*g\n", DBL_DIG, SvNVX(sv));
#endif
	RESTORE_NUMERIC_LOCAL();
    ;/*170*/}}

    SensorCall(1816);if (SvROK(sv)) {
	SensorCall(1814);Perl_dump_indent(aTHX_ level, file, "  RV = 0x%"UVxf"\n", PTR2UV(SvRV(sv)));
	SensorCall(1815);if (nest < maxnest)
	    do_sv_dump(level+1, file, SvRV(sv), nest+1, maxnest, dumpops, pvlim);
    }

    SensorCall(1818);if (type < SVt_PV) {
	SvREFCNT_dec(d);
	SensorCall(1817);return;
    }

    SensorCall(1832);if ((type <= SVt_PVLV && !isGV_with_GP(sv)) || type == SVt_PVFM) {
	SensorCall(1819);if (SvPVX_const(sv)) {
	    SensorCall(1820);STRLEN delta;
	    SensorCall(1823);if (SvOOK(sv)) {
		SvOOK_offset(sv, delta);
		SensorCall(1821);Perl_dump_indent(aTHX_ level, file,"  OFFSET = %"UVuf"\n",
				 (UV) delta);
	    } else {
		SensorCall(1822);delta = 0;
	    }
	    SensorCall(1824);Perl_dump_indent(aTHX_ level, file,"  PV = 0x%"UVxf" ", PTR2UV(SvPVX_const(sv)));
	    SensorCall(1826);if (SvOOK(sv)) {
		SensorCall(1825);PerlIO_printf(file, "( %s . ) ",
			      pv_display(d, SvPVX_const(sv) - delta, delta, 0,
					 pvlim));
	    }
	    SensorCall(1827);PerlIO_printf(file, "%s", pv_display(d, SvPVX_const(sv), SvCUR(sv), SvLEN(sv), pvlim));
	    SensorCall(1829);if (SvUTF8(sv)) /* the 6?  \x{....} */
	        {/*171*/SensorCall(1828);PerlIO_printf(file, " [UTF8 \"%s\"]", sv_uni_display(d, sv, 6 * SvCUR(sv), UNI_DISPLAY_QQ));/*172*/}
	    SensorCall(1830);PerlIO_printf(file, "\n");
	    Perl_dump_indent(aTHX_ level, file, "  CUR = %"IVdf"\n", (IV)SvCUR(sv));
	    Perl_dump_indent(aTHX_ level, file, "  LEN = %"IVdf"\n", (IV)SvLEN(sv));
	}
	else
	    {/*173*/SensorCall(1831);Perl_dump_indent(aTHX_ level, file, "  PV = 0\n");/*174*/}
    }

    SensorCall(1840);if (type >= SVt_PVMG) {
	SensorCall(1833);if (type == SVt_PVMG && SvPAD_OUR(sv)) {
	    SensorCall(1834);HV * const ost = SvOURSTASH(sv);
	    SensorCall(1835);if (ost)
		do_hv_dump(level, file, "  OURSTASH", ost);
	} else {
	    SensorCall(1836);if (SvMAGIC(sv))
		do_magic_dump(level, file, SvMAGIC(sv), nest+1, maxnest, dumpops, pvlim);
	}
	SensorCall(1837);if (SvSTASH(sv))
	    do_hv_dump(level, file, "  STASH", SvSTASH(sv));

	SensorCall(1839);if ((type == SVt_PVMG || type == SVt_PVLV) && SvVALID(sv)) {
	    SensorCall(1838);Perl_dump_indent(aTHX_ level, file, "  RARE = %u\n", (U8)BmRARE(sv));
	    Perl_dump_indent(aTHX_ level, file, "  PREVIOUS = %"UVuf"\n", (UV)BmPREVIOUS(sv));
	    Perl_dump_indent(aTHX_ level, file, "  USEFUL = %"IVdf"\n", (IV)BmUSEFUL(sv));
	}
    }

    /* Dump type-specific SV fields */

    SensorCall(1985);switch (type) {
    case SVt_PVAV:
	SensorCall(1841);Perl_dump_indent(aTHX_ level, file, "  ARRAY = 0x%"UVxf, PTR2UV(AvARRAY(sv)));
	SensorCall(1844);if (AvARRAY(sv) != AvALLOC(sv)) {
	    SensorCall(1842);PerlIO_printf(file, " (offset=%"IVdf")\n", (IV)(AvARRAY(sv) - AvALLOC(sv)));
	    Perl_dump_indent(aTHX_ level, file, "  ALLOC = 0x%"UVxf"\n", PTR2UV(AvALLOC(sv)));
	}
	else
	    {/*175*/SensorCall(1843);PerlIO_putc(file, '\n');/*176*/}
	SensorCall(1845);Perl_dump_indent(aTHX_ level, file, "  FILL = %"IVdf"\n", (IV)AvFILLp(sv));
	Perl_dump_indent(aTHX_ level, file, "  MAX = %"IVdf"\n", (IV)AvMAX(sv));
	Perl_dump_indent(aTHX_ level, file, "  ARYLEN = 0x%"UVxf"\n", SvMAGIC(sv) ? PTR2UV(AvARYLEN(sv)) : 0);
	sv_setpvs(d, "");
	SensorCall(1846);if (AvREAL(sv))	sv_catpv(d, ",REAL");
	SensorCall(1847);if (AvREIFY(sv))	sv_catpv(d, ",REIFY");
	SensorCall(1848);Perl_dump_indent(aTHX_ level, file, "  FLAGS = (%s)\n",
			 SvCUR(d) ? SvPVX_const(d) + 1 : "");
	SensorCall(1853);if (nest < maxnest && av_len(MUTABLE_AV(sv)) >= 0) {
	    SensorCall(1849);int count;
	    SensorCall(1852);for (count = 0; count <=  av_len(MUTABLE_AV(sv)) && count < maxnest; count++) {
		SensorCall(1850);SV** const elt = av_fetch(MUTABLE_AV(sv),count,0);

		Perl_dump_indent(aTHX_ level + 1, file, "Elt No. %"IVdf"\n", (IV)count);
		SensorCall(1851);if (elt)
		    do_sv_dump(level+1, file, *elt, nest+1, maxnest, dumpops, pvlim);
	    }
	}
	SensorCall(1854);break;
    case SVt_PVHV:
	SensorCall(1855);Perl_dump_indent(aTHX_ level, file, "  ARRAY = 0x%"UVxf, PTR2UV(HvARRAY(sv)));
	SensorCall(1875);if (HvARRAY(sv) && HvUSEDKEYS(sv)) {
	    /* Show distribution of HEs in the ARRAY */
	    SensorCall(1856);int freq[200];
#define FREQ_MAX ((int)(sizeof freq / sizeof freq[0] - 1))
	    int i;
	    int max = 0;
	    U32 pow2 = 2, keys = HvUSEDKEYS(sv);
	    NV theoret, sum = 0;

	    PerlIO_printf(file, "  (");
	    Zero(freq, FREQ_MAX + 1, int);
	    SensorCall(1863);for (i = 0; (STRLEN)i <= HvMAX(sv); i++) {
		SensorCall(1857);HE* h;
		int count = 0;
                SensorCall(1859);for (h = HvARRAY(sv)[i]; h; h = HeNEXT(h))
		    {/*177*/SensorCall(1858);count++;/*178*/}
		if (count > FREQ_MAX)
		    count = FREQ_MAX;
	        SensorCall(1860);freq[count]++;
	        SensorCall(1862);if (max < count)
		    {/*181*/SensorCall(1861);max = count;/*182*/}
	    }
	    SensorCall(1868);for (i = 0; i <= max; i++) {
		SensorCall(1864);if (freq[i]) {
		    SensorCall(1865);PerlIO_printf(file, "%d%s:%d", i,
				  (i == FREQ_MAX) ? "+" : "",
				  freq[i]);
		    SensorCall(1867);if (i != max)
			{/*183*/SensorCall(1866);PerlIO_printf(file, ", ");/*184*/}
		}
            }
	    SensorCall(1869);PerlIO_putc(file, ')');
	    /* The "quality" of a hash is defined as the total number of
	       comparisons needed to access every element once, relative
	       to the expected number needed for a random hash.

	       The total number of comparisons is equal to the sum of
	       the squares of the number of entries in each bucket.
	       For a random hash of n keys into k buckets, the expected
	       value is
				n + n(n-1)/2k
	    */

	    SensorCall(1871);for (i = max; i > 0; i--) { /* Precision: count down. */
		SensorCall(1870);sum += freq[i] * i * i;
            }
	    SensorCall(1873);while ((keys = keys >> 1))
		{/*185*/SensorCall(1872);pow2 = pow2 << 1;/*186*/}
	    SensorCall(1874);theoret = HvUSEDKEYS(sv);
	    theoret += theoret * (theoret-1)/pow2;
	    PerlIO_putc(file, '\n');
	    Perl_dump_indent(aTHX_ level, file, "  hash quality = %.1"NVff"%%", theoret/sum*100);
	}
	SensorCall(1876);PerlIO_putc(file, '\n');
	Perl_dump_indent(aTHX_ level, file, "  KEYS = %"IVdf"\n", (IV)HvUSEDKEYS(sv));
	Perl_dump_indent(aTHX_ level, file, "  FILL = %"IVdf"\n", (IV)HvFILL(sv));
	Perl_dump_indent(aTHX_ level, file, "  MAX = %"IVdf"\n", (IV)HvMAX(sv));
	Perl_dump_indent(aTHX_ level, file, "  RITER = %"IVdf"\n", (IV)HvRITER_get(sv));
	Perl_dump_indent(aTHX_ level, file, "  EITER = 0x%"UVxf"\n", PTR2UV(HvEITER_get(sv)));
	{
	    MAGIC * const mg = mg_find(sv, PERL_MAGIC_symtab);
	    SensorCall(1878);if (mg && mg->mg_obj) {
		SensorCall(1877);Perl_dump_indent(aTHX_ level, file, "  PMROOT = 0x%"UVxf"\n", PTR2UV(mg->mg_obj));
	    }
	}
	{
	    SensorCall(1879);const char * const hvname = HvNAME_get(sv);
	    SensorCall(1881);if (hvname)
		{/*187*/SensorCall(1880);Perl_dump_indent(aTHX_ level, file, "  NAME = \"%s\"\n", hvname);/*188*/}
	}
	SensorCall(1906);if (SvOOK(sv)) {
	    SensorCall(1882);AV * const backrefs
		= *Perl_hv_backreferences_p(aTHX_ MUTABLE_HV(sv));
	    struct mro_meta * const meta = HvAUX(sv)->xhv_mro_meta;
	    SensorCall(1884);if (HvAUX(sv)->xhv_name_count)
		{/*189*/SensorCall(1883);Perl_dump_indent(aTHX_
		 level, file, "  NAMECOUNT = %"IVdf"\n",
		 (IV)HvAUX(sv)->xhv_name_count
		);/*190*/}
	    SensorCall(1893);if (HvAUX(sv)->xhv_name_u.xhvnameu_name && HvENAME_HEK_NN(sv)) {
		SensorCall(1885);const I32 count = HvAUX(sv)->xhv_name_count;
		SensorCall(1892);if (count) {
		    SensorCall(1886);SV * const names = newSVpvs_flags("", SVs_TEMP);
		    /* The starting point is the first element if count is
		       positive and the second element if count is negative. */
		    HEK *const *hekp = HvAUX(sv)->xhv_name_u.xhvnameu_names
			+ (count < 0 ? 1 : 0);
		    HEK *const *const endp = HvAUX(sv)->xhv_name_u.xhvnameu_names
			+ (count < 0 ? -count : count);
		    SensorCall(1889);while (hekp < endp) {
			SensorCall(1887);if (*hekp) {
			    sv_catpvs(names, ", \"");
			    sv_catpvn(names, HEK_KEY(*hekp), HEK_LEN(*hekp));
			    sv_catpvs(names, "\"");
			} else {
			    /* This should never happen. */
			    sv_catpvs(names, ", (null)");
			}
			SensorCall(1888);++hekp;
		    }
		    SensorCall(1890);Perl_dump_indent(aTHX_
		     level, file, "  ENAME = %s\n", SvPV_nolen(names)+2
		    );
		}
		else
		    {/*191*/SensorCall(1891);Perl_dump_indent(aTHX_
		     level, file, "  ENAME = \"%s\"\n", HvENAME_get(sv)
		    );/*192*/}
	    }
	    SensorCall(1895);if (backrefs) {
		SensorCall(1894);Perl_dump_indent(aTHX_ level, file, "  BACKREFS = 0x%"UVxf"\n",
				 PTR2UV(backrefs));
		do_sv_dump(level+1, file, MUTABLE_SV(backrefs), nest+1, maxnest,
			   dumpops, pvlim);
	    }
	    SensorCall(1905);if (meta) {
		/* FIXME - mro_algs kflags can signal a UTF-8 name.  */
		SensorCall(1896);Perl_dump_indent(aTHX_ level, file, "  MRO_WHICH = \"%.*s\" (0x%"UVxf")\n",
				 (int)meta->mro_which->length,
				 meta->mro_which->name,
				 PTR2UV(meta->mro_which));
		Perl_dump_indent(aTHX_ level, file, "  CACHE_GEN = 0x%"UVxf"\n",
				 (UV)meta->cache_gen);
		Perl_dump_indent(aTHX_ level, file, "  PKG_GEN = 0x%"UVxf"\n",
				 (UV)meta->pkg_gen);
		SensorCall(1898);if (meta->mro_linear_all) {
		    SensorCall(1897);Perl_dump_indent(aTHX_ level, file, "  MRO_LINEAR_ALL = 0x%"UVxf"\n",
				 PTR2UV(meta->mro_linear_all));
		do_sv_dump(level+1, file, MUTABLE_SV(meta->mro_linear_all), nest+1, maxnest,
			   dumpops, pvlim);
		}
		SensorCall(1900);if (meta->mro_linear_current) {
		    SensorCall(1899);Perl_dump_indent(aTHX_ level, file, "  MRO_LINEAR_CURRENT = 0x%"UVxf"\n",
				 PTR2UV(meta->mro_linear_current));
		do_sv_dump(level+1, file, MUTABLE_SV(meta->mro_linear_current), nest+1, maxnest,
			   dumpops, pvlim);
		}
		SensorCall(1902);if (meta->mro_nextmethod) {
		    SensorCall(1901);Perl_dump_indent(aTHX_ level, file, "  MRO_NEXTMETHOD = 0x%"UVxf"\n",
				 PTR2UV(meta->mro_nextmethod));
		do_sv_dump(level+1, file, MUTABLE_SV(meta->mro_nextmethod), nest+1, maxnest,
			   dumpops, pvlim);
		}
		SensorCall(1904);if (meta->isa) {
		    SensorCall(1903);Perl_dump_indent(aTHX_ level, file, "  ISA = 0x%"UVxf"\n",
				 PTR2UV(meta->isa));
		do_sv_dump(level+1, file, MUTABLE_SV(meta->isa), nest+1, maxnest,
			   dumpops, pvlim);
		}
	    }
	}
	SensorCall(1923);if (nest < maxnest) {
	    SensorCall(1907);HV * const hv = MUTABLE_HV(sv);
	    STRLEN i;
	    HE *he;

	    SensorCall(1922);if (HvARRAY(hv)) {
		SensorCall(1908);int count = maxnest - nest;
		SensorCall(1921);for (i=0; i <= HvMAX(hv); i++) {
		    SensorCall(1909);for (he = HvARRAY(hv)[i]; he; he = HeNEXT(he)) {
			SensorCall(1910);U32 hash;
			SV * keysv;
			const char * keypv;
			SV * elt;
		STRLEN len;

			SensorCall(1912);if (count-- <= 0) {/*193*/SensorCall(1911);goto DONEHV;/*194*/}

			SensorCall(1913);hash = HeHASH(he);
			keysv = hv_iterkeysv(he);
			keypv = SvPV_const(keysv, len);
			elt = HeVAL(he);

		Perl_dump_indent(aTHX_ level+1, file, "Elt %s ", pv_display(d, keypv, len, 0, pvlim));
		SensorCall(1915);if (SvUTF8(keysv))
		    {/*195*/SensorCall(1914);PerlIO_printf(file, "[UTF8 \"%s\"] ", sv_uni_display(d, keysv, 6 * SvCUR(keysv), UNI_DISPLAY_QQ));/*196*/}
			SensorCall(1917);if (HvEITER_get(hv) == he)
			    {/*197*/SensorCall(1916);PerlIO_printf(file, "[CURRENT] ");/*198*/}
		SensorCall(1919);if (HeKREHASH(he))
		    {/*199*/SensorCall(1918);PerlIO_printf(file, "[REHASH] ");/*200*/}
			SensorCall(1920);PerlIO_printf(file, "HASH = 0x%"UVxf"\n", (UV) hash);
		do_sv_dump(level+1, file, elt, nest+1, maxnest, dumpops, pvlim);
	    }
		}
	      DONEHV:;
	    }
	}
	SensorCall(1924);break;

    case SVt_PVCV:
	SensorCall(1925);if (CvAUTOLOAD(sv)) {
	    SensorCall(1926);STRLEN len;
	    const char *const name =  SvPV_const(sv, len);
	    Perl_dump_indent(aTHX_ level, file, "  AUTOLOAD = \"%.*s\"\n",
			     (int) len, name);
	}
	SensorCall(1928);if (SvPOK(sv)) {
	    SensorCall(1927);Perl_dump_indent(aTHX_ level, file, "  PROTOTYPE = \"%.*s\"\n",
			     (int) CvPROTOLEN(sv), CvPROTO(sv));
	}
	/* FALL THROUGH */
    case SVt_PVFM:
	do_hv_dump(level, file, "  COMP_STASH", CvSTASH(sv));
	SensorCall(1937);if (!CvISXSUB(sv)) {
	    SensorCall(1929);if (CvSTART(sv)) {
		SensorCall(1930);Perl_dump_indent(aTHX_ level, file,
				 "  START = 0x%"UVxf" ===> %"IVdf"\n",
				 PTR2UV(CvSTART(sv)),
				 (IV)sequence_num(CvSTART(sv)));
	    }
	    SensorCall(1931);Perl_dump_indent(aTHX_ level, file, "  ROOT = 0x%"UVxf"\n",
			     PTR2UV(CvROOT(sv)));
	    SensorCall(1932);if (CvROOT(sv) && dumpops) {
		do_op_dump(level+1, file, CvROOT(sv));
	    }
	} else {
	    SensorCall(1933);SV * const constant = cv_const_sv((const CV *)sv);

	    Perl_dump_indent(aTHX_ level, file, "  XSUB = 0x%"UVxf"\n", PTR2UV(CvXSUB(sv)));

	    SensorCall(1936);if (constant) {
		SensorCall(1934);Perl_dump_indent(aTHX_ level, file, "  XSUBANY = 0x%"UVxf
				 " (CONST SV)\n",
				 PTR2UV(CvXSUBANY(sv).any_ptr));
		do_sv_dump(level+1, file, constant, nest+1, maxnest, dumpops,
			   pvlim);
	    } else {
		SensorCall(1935);Perl_dump_indent(aTHX_ level, file, "  XSUBANY = %"IVdf"\n",
				 (IV)CvXSUBANY(sv).any_i32);
	    }
	}
 	do_gvgv_dump(level, file, "  GVGV::GV", CvGV(sv));
	SensorCall(1938);Perl_dump_indent(aTHX_ level, file, "  FILE = \"%s\"\n", CvFILE(sv));
	SensorCall(1940);if (type == SVt_PVCV)
	    {/*201*/SensorCall(1939);Perl_dump_indent(aTHX_ level, file, "  DEPTH = %"IVdf"\n", (IV)CvDEPTH(sv));/*202*/}
	SensorCall(1941);Perl_dump_indent(aTHX_ level, file, "  FLAGS = 0x%"UVxf"\n", (UV)CvFLAGS(sv));
	Perl_dump_indent(aTHX_ level, file, "  OUTSIDE_SEQ = %"UVuf"\n", (UV)CvOUTSIDE_SEQ(sv));
	SensorCall(1943);if (type == SVt_PVFM)
	    {/*203*/SensorCall(1942);Perl_dump_indent(aTHX_ level, file, "  LINES = %"IVdf"\n", (IV)FmLINES(sv));/*204*/}
	SensorCall(1944);Perl_dump_indent(aTHX_ level, file, "  PADLIST = 0x%"UVxf"\n", PTR2UV(CvPADLIST(sv)));
	SensorCall(1945);if (nest < maxnest) {
	    do_dump_pad(level+1, file, CvPADLIST(sv), 0);
	}
	{
	    SensorCall(1946);const CV * const outside = CvOUTSIDE(sv);
	    Perl_dump_indent(aTHX_ level, file, "  OUTSIDE = 0x%"UVxf" (%s)\n",
			PTR2UV(outside),
			(!outside ? "null"
			 : CvANON(outside) ? "ANON"
			 : (outside == PL_main_cv) ? "MAIN"
			 : CvUNIQUE(outside) ? "UNIQUE"
			 : CvGV(outside) ? GvNAME(CvGV(outside)) : "UNDEFINED"));
	}
	SensorCall(1947);if (nest < maxnest && (CvCLONE(sv) || CvCLONED(sv)))
	    do_sv_dump(level+1, file, MUTABLE_SV(CvOUTSIDE(sv)), nest+1, maxnest, dumpops, pvlim);
	SensorCall(1948);break;

    case SVt_PVGV:
    case SVt_PVLV:
	SensorCall(1949);if (type == SVt_PVLV) {
	    SensorCall(1950);Perl_dump_indent(aTHX_ level, file, "  TYPE = %c\n", LvTYPE(sv));
	    Perl_dump_indent(aTHX_ level, file, "  TARGOFF = %"IVdf"\n", (IV)LvTARGOFF(sv));
	    Perl_dump_indent(aTHX_ level, file, "  TARGLEN = %"IVdf"\n", (IV)LvTARGLEN(sv));
	    Perl_dump_indent(aTHX_ level, file, "  TARG = 0x%"UVxf"\n", PTR2UV(LvTARG(sv)));
	    Perl_dump_indent(aTHX_ level, file, "  FLAGS = %"IVdf"\n", (IV)LvFLAGS(sv));
	    SensorCall(1951);if (LvTYPE(sv) != 't' && LvTYPE(sv) != 'T')
		do_sv_dump(level+1, file, LvTARG(sv), nest+1, maxnest,
		    dumpops, pvlim);
	}
	SensorCall(1953);if (!isGV_with_GP(sv))
	    {/*205*/SensorCall(1952);break;/*206*/}
	SensorCall(1954);Perl_dump_indent(aTHX_ level, file, "  NAME = \"%s\"\n", GvNAME(sv));
	Perl_dump_indent(aTHX_ level, file, "  NAMELEN = %"IVdf"\n", (IV)GvNAMELEN(sv));
	do_hv_dump (level, file, "  GvSTASH", GvSTASH(sv));
	Perl_dump_indent(aTHX_ level, file, "  GP = 0x%"UVxf"\n", PTR2UV(GvGP(sv)));
	SensorCall(1956);if (!GvGP(sv))
	    {/*207*/SensorCall(1955);break;/*208*/}
	SensorCall(1957);Perl_dump_indent(aTHX_ level, file, "    SV = 0x%"UVxf"\n", PTR2UV(GvSV(sv)));
	Perl_dump_indent(aTHX_ level, file, "    REFCNT = %"IVdf"\n", (IV)GvREFCNT(sv));
	Perl_dump_indent(aTHX_ level, file, "    IO = 0x%"UVxf"\n", PTR2UV(GvIOp(sv)));
	Perl_dump_indent(aTHX_ level, file, "    FORM = 0x%"UVxf"  \n", PTR2UV(GvFORM(sv)));
	Perl_dump_indent(aTHX_ level, file, "    AV = 0x%"UVxf"\n", PTR2UV(GvAV(sv)));
	Perl_dump_indent(aTHX_ level, file, "    HV = 0x%"UVxf"\n", PTR2UV(GvHV(sv)));
	Perl_dump_indent(aTHX_ level, file, "    CV = 0x%"UVxf"\n", PTR2UV(GvCV(sv)));
	Perl_dump_indent(aTHX_ level, file, "    CVGEN = 0x%"UVxf"\n", (UV)GvCVGEN(sv));
	Perl_dump_indent(aTHX_ level, file, "    LINE = %"IVdf"\n", (IV)GvLINE(sv));
	Perl_dump_indent(aTHX_ level, file, "    FILE = \"%s\"\n", GvFILE(sv));
	Perl_dump_indent(aTHX_ level, file, "    FLAGS = 0x%"UVxf"\n", (UV)GvFLAGS(sv));
	do_gv_dump (level, file, "    EGV", GvEGV(sv));
	SensorCall(1958);break;
    case SVt_PVIO:
	SensorCall(1959);Perl_dump_indent(aTHX_ level, file, "  IFP = 0x%"UVxf"\n", PTR2UV(IoIFP(sv)));
	Perl_dump_indent(aTHX_ level, file, "  OFP = 0x%"UVxf"\n", PTR2UV(IoOFP(sv)));
	Perl_dump_indent(aTHX_ level, file, "  DIRP = 0x%"UVxf"\n", PTR2UV(IoDIRP(sv)));
	Perl_dump_indent(aTHX_ level, file, "  LINES = %"IVdf"\n", (IV)IoLINES(sv));
	Perl_dump_indent(aTHX_ level, file, "  PAGE = %"IVdf"\n", (IV)IoPAGE(sv));
	Perl_dump_indent(aTHX_ level, file, "  PAGE_LEN = %"IVdf"\n", (IV)IoPAGE_LEN(sv));
	Perl_dump_indent(aTHX_ level, file, "  LINES_LEFT = %"IVdf"\n", (IV)IoLINES_LEFT(sv));
        SensorCall(1961);if (IoTOP_NAME(sv))
            {/*209*/SensorCall(1960);Perl_dump_indent(aTHX_ level, file, "  TOP_NAME = \"%s\"\n", IoTOP_NAME(sv));/*210*/}
	SensorCall(1963);if (!IoTOP_GV(sv) || SvTYPE(IoTOP_GV(sv)) == SVt_PVGV)
	    do_gv_dump (level, file, "  TOP_GV", IoTOP_GV(sv));
	else {
	    SensorCall(1962);Perl_dump_indent(aTHX_ level, file, "  TOP_GV = 0x%"UVxf"\n",
			     PTR2UV(IoTOP_GV(sv)));
	    do_sv_dump (level+1, file, MUTABLE_SV(IoTOP_GV(sv)), nest+1,
			maxnest, dumpops, pvlim);
	}
	/* Source filters hide things that are not GVs in these three, so let's
	   be careful out there.  */
        SensorCall(1965);if (IoFMT_NAME(sv))
            {/*211*/SensorCall(1964);Perl_dump_indent(aTHX_ level, file, "  FMT_NAME = \"%s\"\n", IoFMT_NAME(sv));/*212*/}
	SensorCall(1967);if (!IoFMT_GV(sv) || SvTYPE(IoFMT_GV(sv)) == SVt_PVGV)
	    do_gv_dump (level, file, "  FMT_GV", IoFMT_GV(sv));
	else {
	    SensorCall(1966);Perl_dump_indent(aTHX_ level, file, "  FMT_GV = 0x%"UVxf"\n",
			     PTR2UV(IoFMT_GV(sv)));
	    do_sv_dump (level+1, file, MUTABLE_SV(IoFMT_GV(sv)), nest+1,
			maxnest, dumpops, pvlim);
	}
        SensorCall(1969);if (IoBOTTOM_NAME(sv))
            {/*213*/SensorCall(1968);Perl_dump_indent(aTHX_ level, file, "  BOTTOM_NAME = \"%s\"\n", IoBOTTOM_NAME(sv));/*214*/}
	SensorCall(1971);if (!IoBOTTOM_GV(sv) || SvTYPE(IoBOTTOM_GV(sv)) == SVt_PVGV)
	    do_gv_dump (level, file, "  BOTTOM_GV", IoBOTTOM_GV(sv));
	else {
	    SensorCall(1970);Perl_dump_indent(aTHX_ level, file, "  BOTTOM_GV = 0x%"UVxf"\n",
			     PTR2UV(IoBOTTOM_GV(sv)));
	    do_sv_dump (level+1, file, MUTABLE_SV(IoBOTTOM_GV(sv)), nest+1,
			maxnest, dumpops, pvlim);
	}
	SensorCall(1974);if (isPRINT(IoTYPE(sv)))
            {/*215*/SensorCall(1972);Perl_dump_indent(aTHX_ level, file, "  TYPE = '%c'\n", IoTYPE(sv));/*216*/}
	else
            {/*217*/SensorCall(1973);Perl_dump_indent(aTHX_ level, file, "  TYPE = '\\%o'\n", IoTYPE(sv));/*218*/}
	SensorCall(1975);Perl_dump_indent(aTHX_ level, file, "  FLAGS = 0x%"UVxf"\n", (UV)IoFLAGS(sv));
	SensorCall(1976);break;
    case SVt_REGEXP:
	{
	    SensorCall(1977);struct regexp * const r = (struct regexp *)SvANY(sv);
	    flags = RX_EXTFLAGS((REGEXP*)sv);
	    sv_setpv(d,"");
	    append_flags(d, flags, regexp_flags_names);
	    SensorCall(1978);if (*(SvEND(d) - 1) == ',') {
		SvCUR_set(d, SvCUR(d) - 1);
		SvPVX(d)[SvCUR(d)] = '\0';
	    }
	    SensorCall(1979);Perl_dump_indent(aTHX_ level, file, "  EXTFLAGS = 0x%"UVxf" (%s)\n",
				(UV)flags, SvPVX_const(d));
	    Perl_dump_indent(aTHX_ level, file, "  INTFLAGS = 0x%"UVxf"\n",
				(UV)(r->intflags));
	    Perl_dump_indent(aTHX_ level, file, "  NPARENS = %"UVuf"\n",
				(UV)(r->nparens));
	    Perl_dump_indent(aTHX_ level, file, "  LASTPAREN = %"UVuf"\n",
				(UV)(r->lastparen));
	    Perl_dump_indent(aTHX_ level, file, "  LASTCLOSEPAREN = %"UVuf"\n",
				(UV)(r->lastcloseparen));
	    Perl_dump_indent(aTHX_ level, file, "  MINLEN = %"IVdf"\n",
				(IV)(r->minlen));
	    Perl_dump_indent(aTHX_ level, file, "  MINLENRET = %"IVdf"\n",
				(IV)(r->minlenret));
	    Perl_dump_indent(aTHX_ level, file, "  GOFS = %"UVuf"\n",
				(UV)(r->gofs));
	    Perl_dump_indent(aTHX_ level, file, "  PRE_PREFIX = %"UVuf"\n",
				(UV)(r->pre_prefix));
	    Perl_dump_indent(aTHX_ level, file, "  SEEN_EVALS = %"UVuf"\n",
				(UV)(r->seen_evals));
	    Perl_dump_indent(aTHX_ level, file, "  SUBLEN = %"IVdf"\n",
				(IV)(r->sublen));
	    SensorCall(1982);if (r->subbeg)
		{/*219*/SensorCall(1980);Perl_dump_indent(aTHX_ level, file, "  SUBBEG = 0x%"UVxf" %s\n",
			    PTR2UV(r->subbeg),
			    pv_display(d, r->subbeg, r->sublen, 50, pvlim));/*220*/}
	    else
		{/*221*/SensorCall(1981);Perl_dump_indent(aTHX_ level, file, "  SUBBEG = 0x0\n");/*222*/}
	    SensorCall(1983);Perl_dump_indent(aTHX_ level, file, "  ENGINE = 0x%"UVxf"\n",
				PTR2UV(r->engine));
	    Perl_dump_indent(aTHX_ level, file, "  MOTHER_RE = 0x%"UVxf"\n",
				PTR2UV(r->mother_re));
	    Perl_dump_indent(aTHX_ level, file, "  PAREN_NAMES = 0x%"UVxf"\n",
				PTR2UV(r->paren_names));
	    Perl_dump_indent(aTHX_ level, file, "  SUBSTRS = 0x%"UVxf"\n",
				PTR2UV(r->substrs));
	    Perl_dump_indent(aTHX_ level, file, "  PPRIVATE = 0x%"UVxf"\n",
				PTR2UV(r->pprivate));
	    Perl_dump_indent(aTHX_ level, file, "  OFFS = 0x%"UVxf"\n",
				PTR2UV(r->offs));
#ifdef PERL_OLD_COPY_ON_WRITE
	    Perl_dump_indent(aTHX_ level, file, "  SAVED_COPY = 0x%"UVxf"\n",
				PTR2UV(r->saved_copy));
#endif
	}
	SensorCall(1984);break;
    }
    SvREFCNT_dec(d);
}

void
Perl_sv_dump(pTHX_ SV *sv)
{
SensorCall(1986);    dVAR;

    PERL_ARGS_ASSERT_SV_DUMP;

    SensorCall(1987);if (SvROK(sv))
	do_sv_dump(0, Perl_debug_log, sv, 0, 4, 0, 0);
    else
	do_sv_dump(0, Perl_debug_log, sv, 0, 0, 0, 0);
SensorCall(1988);}

int
Perl_runops_debug(pTHX)
{
SensorCall(1989);    dVAR;
    SensorCall(1992);if (!PL_op) {
	SensorCall(1990);Perl_ck_warner_d(aTHX_ packWARN(WARN_DEBUGGING), "NULL OP IN RUN");
	{int  ReplaceReturn2102 = 0; SensorCall(1991); return ReplaceReturn2102;}
    }

    DEBUG_l(Perl_deb(aTHX_ "Entering new RUNOPS level\n"));
    SensorCall(2001);do {
	SensorCall(1993);if (PL_debug) {
	    SensorCall(1994);if (PL_watchaddr && (*PL_watchaddr != PL_watchok))
		{/*223*/SensorCall(1995);PerlIO_printf(Perl_debug_log,
			      "WARNING: %"UVxf" changed from %"UVxf" to %"UVxf"\n",
			      PTR2UV(PL_watchaddr), PTR2UV(PL_watchok),
			      PTR2UV(*PL_watchaddr));/*224*/}
	    SensorCall(1998);if (DEBUG_s_TEST_) {
		SensorCall(1996);if (DEBUG_v_TEST_) {
		    SensorCall(1997);PerlIO_printf(Perl_debug_log, "\n");
		    deb_stack_all();
		}
		else
		    debstack();
	    }


	    SensorCall(1999);if (DEBUG_t_TEST_) debop(PL_op);
	    SensorCall(2000);if (DEBUG_P_TEST_) debprof(PL_op);
	}
    } while ((PL_op = PL_op->op_ppaddr(aTHX)));
    DEBUG_l(Perl_deb(aTHX_ "leaving RUNOPS level\n"));
    PERL_ASYNC_CHECK();

    TAINT_NOT;
    {int  ReplaceReturn2101 = 0; SensorCall(2002); return ReplaceReturn2101;}
}

I32
Perl_debop(pTHX_ const OP *o)
{
SensorCall(2003);    dVAR;

    PERL_ARGS_ASSERT_DEBOP;

    SensorCall(2005);if (CopSTASH_eq(PL_curcop, PL_debstash) && !DEBUG_J_TEST_)
	{/*225*/{I32  ReplaceReturn2100 = 0; SensorCall(2004); return ReplaceReturn2100;}/*226*/}

    SensorCall(2006);Perl_deb(aTHX_ "%s", OP_NAME(o));
    SensorCall(2022);switch (o->op_type) {
    case OP_CONST:
    case OP_HINTSEVAL:
	/* With ITHREADS, consts are stored in the pad, and the right pad
	 * may not be active here, so check.
	 * Looks like only during compiling the pads are illegal.
	 */
#ifdef USE_ITHREADS
	SensorCall(2007);if ((((SVOP*)o)->op_sv) || !IN_PERL_COMPILETIME)
#endif
	    {/*227*/SensorCall(2008);PerlIO_printf(Perl_debug_log, "(%s)", SvPEEK(cSVOPo_sv));/*228*/}
	SensorCall(2009);break;
    case OP_GVSV:
    case OP_GV:
	SensorCall(2010);if (cGVOPo_gv) {
	    SensorCall(2011);SV * const sv = newSV(0);
#ifdef PERL_MAD
	    /* FIXME - is this making unwarranted assumptions about the
	       UTF-8 cleanliness of the dump file handle?  */
	    SvUTF8_on(sv);
#endif
	    gv_fullname3(sv, cGVOPo_gv, NULL);
	    PerlIO_printf(Perl_debug_log, "(%s)", SvPV_nolen_const(sv));
	    SvREFCNT_dec(sv);
	}
	else
	    {/*229*/SensorCall(2012);PerlIO_printf(Perl_debug_log, "(NULL)");/*230*/}
	SensorCall(2013);break;
    case OP_PADSV:
    case OP_PADAV:
    case OP_PADHV:
	{
	/* print the lexical's name */
	SensorCall(2014);CV * const cv = deb_curcv(cxstack_ix);
	SV *sv;
        SensorCall(2016);if (cv) {
	    SensorCall(2015);AV * const padlist = CvPADLIST(cv);
            AV * const comppad = MUTABLE_AV(*av_fetch(padlist, 0, FALSE));
            sv = *av_fetch(comppad, o->op_targ, FALSE);
        } else
            sv = NULL;
        SensorCall(2019);if (sv)
	    {/*231*/SensorCall(2017);PerlIO_printf(Perl_debug_log, "(%s)", SvPV_nolen_const(sv));/*232*/}
        else
	    {/*233*/SensorCall(2018);PerlIO_printf(Perl_debug_log, "[%"UVuf"]", (UV)o->op_targ);/*234*/}
	}
        SensorCall(2020);break;
    default:
	SensorCall(2021);break;
    }
    SensorCall(2023);PerlIO_printf(Perl_debug_log, "\n");
    {I32  ReplaceReturn2099 = 0; SensorCall(2024); return ReplaceReturn2099;}
}

STATIC CV*
S_deb_curcv(pTHX_ const I32 ix)
{
SensorCall(2025);    dVAR;
    const PERL_CONTEXT * const cx = &cxstack[ix];
    SensorCall(2027);if (CxTYPE(cx) == CXt_SUB || CxTYPE(cx) == CXt_FORMAT)
        {/*235*/{CV * ReplaceReturn2098 = cx->blk_sub.cv; SensorCall(2026); return ReplaceReturn2098;}/*236*/}
    else if (CxTYPE(cx) == CXt_EVAL && !CxTRYBLOCK(cx))
        {/*237*/return cx->blk_eval.cv;/*238*/}
    else if (ix == 0 && PL_curstackinfo->si_type == PERLSI_MAIN)
        return PL_main_cv;
    else if (ix <= 0)
        return NULL;
    else
        return deb_curcv(ix - 1);
SensorCall(2028);}

void
Perl_watch(pTHX_ char **addr)
{
SensorCall(2029);    dVAR;

    PERL_ARGS_ASSERT_WATCH;

    PL_watchaddr = addr;
    PL_watchok = *addr;
    PerlIO_printf(Perl_debug_log, "WATCHING, %"UVxf" is currently %"UVxf"\n",
	PTR2UV(PL_watchaddr), PTR2UV(PL_watchok));
SensorCall(2030);}

STATIC void
S_debprof(pTHX_ const OP *o)
{
SensorCall(2031);    dVAR;

    PERL_ARGS_ASSERT_DEBPROF;

    SensorCall(2033);if (!DEBUG_J_TEST_ && CopSTASH_eq(PL_curcop, PL_debstash))
	{/*239*/SensorCall(2032);return;/*240*/}
    SensorCall(2034);if (!PL_profiledata)
	Newxz(PL_profiledata, MAXO, U32);
    SensorCall(2035);++PL_profiledata[o->op_type];
SensorCall(2036);}

void
Perl_debprofdump(pTHX)
{
SensorCall(2037);    dVAR;
    unsigned i;
    SensorCall(2039);if (!PL_profiledata)
	{/*241*/SensorCall(2038);return;/*242*/}
    SensorCall(2042);for (i = 0; i < MAXO; i++) {
	SensorCall(2040);if (PL_profiledata[i])
	    {/*243*/SensorCall(2041);PerlIO_printf(Perl_debug_log,
			  "%5lu %s\n", (unsigned long)PL_profiledata[i],
                                       PL_op_name[i]);/*244*/}
    }
SensorCall(2043);}

#ifdef PERL_MAD
/*
 *    XML variants of most of the above routines
 */

STATIC void
S_xmldump_attr(pTHX_ I32 level, PerlIO *file, const char* pat, ...)
{
    va_list args;

    PERL_ARGS_ASSERT_XMLDUMP_ATTR;

    PerlIO_printf(file, "\n    ");
    va_start(args, pat);
    xmldump_vindent(level, file, pat, &args);
    va_end(args);
}


void
Perl_xmldump_indent(pTHX_ I32 level, PerlIO *file, const char* pat, ...)
{
    va_list args;
    PERL_ARGS_ASSERT_XMLDUMP_INDENT;
    va_start(args, pat);
    xmldump_vindent(level, file, pat, &args);
    va_end(args);
}

void
Perl_xmldump_vindent(pTHX_ I32 level, PerlIO *file, const char* pat, va_list *args)
{
    PERL_ARGS_ASSERT_XMLDUMP_VINDENT;

    PerlIO_printf(file, "%*s", (int)(level*PL_dumpindent), "");
    PerlIO_vprintf(file, pat, *args);
}

void
Perl_xmldump_all(pTHX)
{
    xmldump_all_perl(FALSE);
}

void
Perl_xmldump_all_perl(pTHX_ bool justperl PERL_UNUSED_DECL)
{
    PerlIO_setlinebuf(PL_xmlfp);
    if (PL_main_root)
	op_xmldump(PL_main_root);
    /* someday we might call this, when it outputs XML: */
    /* xmldump_packsubs_perl(PL_defstash, justperl); */
    if (PL_xmlfp != (PerlIO*)PerlIO_stdout())
	PerlIO_close(PL_xmlfp);
    PL_xmlfp = 0;
}

void
Perl_xmldump_packsubs(pTHX_ const HV *stash)
{
    PERL_ARGS_ASSERT_XMLDUMP_PACKSUBS;
    xmldump_packsubs_perl(stash, FALSE);
}

void
Perl_xmldump_packsubs_perl(pTHX_ const HV *stash, bool justperl)
{
    I32	i;
    HE	*entry;

    PERL_ARGS_ASSERT_XMLDUMP_PACKSUBS_PERL;

    if (!HvARRAY(stash))
	return;
    for (i = 0; i <= (I32) HvMAX(stash); i++) {
	for (entry = HvARRAY(stash)[i]; entry; entry = HeNEXT(entry)) {
	    GV *gv = MUTABLE_GV(HeVAL(entry));
	    HV *hv;
	    if (SvTYPE(gv) != SVt_PVGV || !GvGP(gv))
		continue;
	    if (GvCVu(gv))
		xmldump_sub_perl(gv, justperl);
	    if (GvFORM(gv))
		xmldump_form(gv);
	    if (HeKEY(entry)[HeKLEN(entry)-1] == ':'
		&& (hv = GvHV(gv)) && hv != PL_defstash)
		xmldump_packsubs_perl(hv, justperl);	/* nested package */
	}
    }
}

void
Perl_xmldump_sub(pTHX_ const GV *gv)
{
    PERL_ARGS_ASSERT_XMLDUMP_SUB;
    xmldump_sub_perl(gv, FALSE);
}

void
Perl_xmldump_sub_perl(pTHX_ const GV *gv, bool justperl)
{
    SV * sv;

    PERL_ARGS_ASSERT_XMLDUMP_SUB_PERL;

    if (justperl && (CvISXSUB(GvCV(gv)) || !CvROOT(GvCV(gv))))
	return;

    sv = sv_newmortal();
    gv_fullname3(sv, gv, NULL);
    Perl_xmldump_indent(aTHX_ 0, PL_xmlfp, "\nSUB %s = ", SvPVX(sv));
    if (CvXSUB(GvCV(gv)))
	Perl_xmldump_indent(aTHX_ 0, PL_xmlfp, "(xsub 0x%"UVxf" %d)\n",
	    PTR2UV(CvXSUB(GvCV(gv))),
	    (int)CvXSUBANY(GvCV(gv)).any_i32);
    else if (CvROOT(GvCV(gv)))
	op_xmldump(CvROOT(GvCV(gv)));
    else
	Perl_xmldump_indent(aTHX_ 0, PL_xmlfp, "<undef>\n");
}

void
Perl_xmldump_form(pTHX_ const GV *gv)
{
    SV * const sv = sv_newmortal();

    PERL_ARGS_ASSERT_XMLDUMP_FORM;

    gv_fullname3(sv, gv, NULL);
    Perl_xmldump_indent(aTHX_ 0, PL_xmlfp, "\nFORMAT %s = ", SvPVX(sv));
    if (CvROOT(GvFORM(gv)))
	op_xmldump(CvROOT(GvFORM(gv)));
    else
	Perl_xmldump_indent(aTHX_ 0, PL_xmlfp, "<undef>\n");
}

void
Perl_xmldump_eval(pTHX)
{
    op_xmldump(PL_eval_root);
}

char *
Perl_sv_catxmlsv(pTHX_ SV *dsv, SV *ssv)
{
    PERL_ARGS_ASSERT_SV_CATXMLSV;
    return sv_catxmlpvn(dsv, SvPVX(ssv), SvCUR(ssv), SvUTF8(ssv));
}

char *
Perl_sv_catxmlpv(pTHX_ SV *dsv, const char *pv, int utf8)
{
    PERL_ARGS_ASSERT_SV_CATXMLPV;
    return sv_catxmlpvn(dsv, pv, strlen(pv), utf8);
}

char *
Perl_sv_catxmlpvn(pTHX_ SV *dsv, const char *pv, STRLEN len, int utf8)
{
    unsigned int c;
    const char * const e = pv + len;
    const char * const start = pv;
    STRLEN dsvcur;
    STRLEN cl;

    PERL_ARGS_ASSERT_SV_CATXMLPVN;

    sv_catpvs(dsv,"");
    dsvcur = SvCUR(dsv);	/* in case we have to restart */

  retry:
    while (pv < e) {
	if (utf8) {
	    c = utf8_to_uvchr_buf((U8*)pv, (U8*)e, &cl);
	    if (cl == 0) {
		SvCUR(dsv) = dsvcur;
		pv = start;
		utf8 = 0;
		goto retry;
	    }
	}
	else
	    c = (*pv & 255);

	switch (c) {
	case 0x00:
	case 0x01:
	case 0x02:
	case 0x03:
	case 0x04:
	case 0x05:
	case 0x06:
	case 0x07:
	case 0x08:
	case 0x0b:
	case 0x0c:
	case 0x0e:
	case 0x0f:
	case 0x10:
	case 0x11:
	case 0x12:
	case 0x13:
	case 0x14:
	case 0x15:
	case 0x16:
	case 0x17:
	case 0x18:
	case 0x19:
	case 0x1a:
	case 0x1b:
	case 0x1c:
	case 0x1d:
	case 0x1e:
	case 0x1f:
	case 0x7f:
	case 0x80:
	case 0x81:
	case 0x82:
	case 0x83:
	case 0x84:
	case 0x86:
	case 0x87:
	case 0x88:
	case 0x89:
	case 0x90:
	case 0x91:
	case 0x92:
	case 0x93:
	case 0x94:
	case 0x95:
	case 0x96:
	case 0x97:
	case 0x98:
	case 0x99:
	case 0x9a:
	case 0x9b:
	case 0x9c:
	case 0x9d:
	case 0x9e:
	case 0x9f:
	    Perl_sv_catpvf(aTHX_ dsv, "STUPIDXML(#x%X)", c);
	    break;
	case '<':
	    sv_catpvs(dsv, "&lt;");
	    break;
	case '>':
	    sv_catpvs(dsv, "&gt;");
	    break;
	case '&':
	    sv_catpvs(dsv, "&amp;");
	    break;
	case '"':
	    sv_catpvs(dsv, "&#34;");
	    break;
	default:
	    if (c < 0xD800) {
		if (c < 32 || c > 127) {
		    Perl_sv_catpvf(aTHX_ dsv, "&#x%X;", c);
		}
		else {
		    const char string = (char) c;
		    sv_catpvn(dsv, &string, 1);
		}
		break;
	    }
	    if ((c >= 0xD800 && c <= 0xDB7F) ||
		(c >= 0xDC00 && c <= 0xDFFF) ||
		(c >= 0xFFF0 && c <= 0xFFFF) ||
		 c > 0x10ffff)
		Perl_sv_catpvf(aTHX_ dsv, "STUPIDXML(#x%X)", c);
	    else
		Perl_sv_catpvf(aTHX_ dsv, "&#x%X;", c);
	}

	if (utf8)
	    pv += UTF8SKIP(pv);
	else
	    pv++;
    }

    return SvPVX(dsv);
}

char *
Perl_sv_xmlpeek(pTHX_ SV *sv)
{
    SV * const t = sv_newmortal();
    STRLEN n_a;
    int unref = 0;

    PERL_ARGS_ASSERT_SV_XMLPEEK;

    sv_utf8_upgrade(t);
    sv_setpvs(t, "");
    /* retry: */
    if (!sv) {
	sv_catpv(t, "VOID=\"\"");
	goto finish;
    }
    else if (sv == (const SV *)0x55555555 || SvTYPE(sv) == 'U') {
	sv_catpv(t, "WILD=\"\"");
	goto finish;
    }
    else if (sv == &PL_sv_undef || sv == &PL_sv_no || sv == &PL_sv_yes || sv == &PL_sv_placeholder) {
	if (sv == &PL_sv_undef) {
	    sv_catpv(t, "SV_UNDEF=\"1\"");
	    if (!(SvFLAGS(sv) & (SVf_OK|SVf_OOK|SVs_OBJECT|
				 SVs_GMG|SVs_SMG|SVs_RMG)) &&
		SvREADONLY(sv))
		goto finish;
	}
	else if (sv == &PL_sv_no) {
	    sv_catpv(t, "SV_NO=\"1\"");
	    if (!(SvFLAGS(sv) & (SVf_ROK|SVf_OOK|SVs_OBJECT|
				 SVs_GMG|SVs_SMG|SVs_RMG)) &&
		!(~SvFLAGS(sv) & (SVf_POK|SVf_NOK|SVf_READONLY|
				  SVp_POK|SVp_NOK)) &&
		SvCUR(sv) == 0 &&
		SvNVX(sv) == 0.0)
		goto finish;
	}
	else if (sv == &PL_sv_yes) {
	    sv_catpv(t, "SV_YES=\"1\"");
	    if (!(SvFLAGS(sv) & (SVf_ROK|SVf_OOK|SVs_OBJECT|
				 SVs_GMG|SVs_SMG|SVs_RMG)) &&
		!(~SvFLAGS(sv) & (SVf_POK|SVf_NOK|SVf_READONLY|
				  SVp_POK|SVp_NOK)) &&
		SvCUR(sv) == 1 &&
		SvPVX(sv) && *SvPVX(sv) == '1' &&
		SvNVX(sv) == 1.0)
		goto finish;
	}
	else {
	    sv_catpv(t, "SV_PLACEHOLDER=\"1\"");
	    if (!(SvFLAGS(sv) & (SVf_OK|SVf_OOK|SVs_OBJECT|
				 SVs_GMG|SVs_SMG|SVs_RMG)) &&
		SvREADONLY(sv))
		goto finish;
	}
	sv_catpv(t, " XXX=\"\" ");
    }
    else if (SvREFCNT(sv) == 0) {
	sv_catpv(t, " refcnt=\"0\"");
	unref++;
    }
    else if (DEBUG_R_TEST_) {
	int is_tmp = 0;
	I32 ix;
	/* is this SV on the tmps stack? */
	for (ix=PL_tmps_ix; ix>=0; ix--) {
	    if (PL_tmps_stack[ix] == sv) {
		is_tmp = 1;
		break;
	    }
	}
	if (SvREFCNT(sv) > 1)
	    Perl_sv_catpvf(aTHX_ t, " DRT=\"<%"UVuf"%s>\"", (UV)SvREFCNT(sv),
		    is_tmp ? "T" : "");
	else if (is_tmp)
	    sv_catpv(t, " DRT=\"<T>\"");
    }

    if (SvROK(sv)) {
	sv_catpv(t, " ROK=\"\"");
    }
    switch (SvTYPE(sv)) {
    default:
	sv_catpv(t, " FREED=\"1\"");
	goto finish;

    case SVt_NULL:
	sv_catpv(t, " UNDEF=\"1\"");
	goto finish;
    case SVt_IV:
	sv_catpv(t, " IV=\"");
	break;
    case SVt_NV:
	sv_catpv(t, " NV=\"");
	break;
    case SVt_PV:
	sv_catpv(t, " PV=\"");
	break;
    case SVt_PVIV:
	sv_catpv(t, " PVIV=\"");
	break;
    case SVt_PVNV:
	sv_catpv(t, " PVNV=\"");
	break;
    case SVt_PVMG:
	sv_catpv(t, " PVMG=\"");
	break;
    case SVt_PVLV:
	sv_catpv(t, " PVLV=\"");
	break;
    case SVt_PVAV:
	sv_catpv(t, " AV=\"");
	break;
    case SVt_PVHV:
	sv_catpv(t, " HV=\"");
	break;
    case SVt_PVCV:
	if (CvGV(sv))
	    Perl_sv_catpvf(aTHX_ t, " CV=\"(%s)\"", GvNAME(CvGV(sv)));
	else
	    sv_catpv(t, " CV=\"()\"");
	goto finish;
    case SVt_PVGV:
	sv_catpv(t, " GV=\"");
	break;
    case SVt_BIND:
	sv_catpv(t, " BIND=\"");
	break;
    case SVt_REGEXP:
	sv_catpv(t, " REGEXP=\"");
	break;
    case SVt_PVFM:
	sv_catpv(t, " FM=\"");
	break;
    case SVt_PVIO:
	sv_catpv(t, " IO=\"");
	break;
    }

    if (SvPOKp(sv)) {
	if (SvPVX(sv)) {
	    sv_catxmlsv(t, sv);
	}
    }
    else if (SvNOKp(sv)) {
	STORE_NUMERIC_LOCAL_SET_STANDARD();
	Perl_sv_catpvf(aTHX_ t, "%"NVgf"",SvNVX(sv));
	RESTORE_NUMERIC_LOCAL();
    }
    else if (SvIOKp(sv)) {
	if (SvIsUV(sv))
	    Perl_sv_catpvf(aTHX_ t, "%"UVuf"", (UV)SvUVX(sv));
	else
            Perl_sv_catpvf(aTHX_ t, "%"IVdf"", (IV)SvIVX(sv));
    }
    else
	sv_catpv(t, "");
    sv_catpv(t, "\"");

  finish:
    while (unref--)
	sv_catpv(t, ")");
    return SvPV(t, n_a);
}

void
Perl_do_pmop_xmldump(pTHX_ I32 level, PerlIO *file, const PMOP *pm)
{
    PERL_ARGS_ASSERT_DO_PMOP_XMLDUMP;

    if (!pm) {
	Perl_xmldump_indent(aTHX_ level, file, "<pmop/>\n");
	return;
    }
    Perl_xmldump_indent(aTHX_ level, file, "<pmop \n");
    level++;
    if (PM_GETRE(pm)) {
	REGEXP *const r = PM_GETRE(pm);
	SV * const tmpsv = newSVpvn_utf8("", 0, TRUE);
	sv_catxmlsv(tmpsv, MUTABLE_SV(r));
	Perl_xmldump_indent(aTHX_ level, file, "pre=\"%s\"\n",
	     SvPVX(tmpsv));
	SvREFCNT_dec(tmpsv);
	Perl_xmldump_indent(aTHX_ level, file, "when=\"%s\"\n",
	     (pm->op_private & OPpRUNTIME) ? "RUN" : "COMP");
    }
    else
	Perl_xmldump_indent(aTHX_ level, file, "pre=\"\" when=\"RUN\"\n");
    if (pm->op_pmflags || (PM_GETRE(pm) && RX_CHECK_SUBSTR(PM_GETRE(pm)))) {
	SV * const tmpsv = pm_description(pm);
	Perl_xmldump_indent(aTHX_ level, file, "pmflags=\"%s\"\n", SvCUR(tmpsv) ? SvPVX(tmpsv) + 1 : "");
	SvREFCNT_dec(tmpsv);
    }

    level--;
    if (pm->op_type != OP_PUSHRE && pm->op_pmreplrootu.op_pmreplroot) {
	Perl_xmldump_indent(aTHX_ level, file, ">\n");
	Perl_xmldump_indent(aTHX_ level+1, file, "<pm_repl>\n");
	do_op_xmldump(level+2, file, pm->op_pmreplrootu.op_pmreplroot);
	Perl_xmldump_indent(aTHX_ level+1, file, "</pm_repl>\n");
	Perl_xmldump_indent(aTHX_ level, file, "</pmop>\n");
    }
    else
	Perl_xmldump_indent(aTHX_ level, file, "/>\n");
}

void
Perl_pmop_xmldump(pTHX_ const PMOP *pm)
{
    do_pmop_xmldump(0, PL_xmlfp, pm);
}

void
Perl_do_op_xmldump(pTHX_ I32 level, PerlIO *file, const OP *o)
{
    UV      seq;
    int     contents = 0;

    PERL_ARGS_ASSERT_DO_OP_XMLDUMP;

    if (!o)
	return;
    seq = sequence_num(o);
    Perl_xmldump_indent(aTHX_ level, file,
	"<op_%s seq=\"%"UVuf" -> ",
	     OP_NAME(o),
	              seq);
    level++;
    if (o->op_next)
	PerlIO_printf(file, seq ? "%"UVuf"\"" : "(%"UVuf")\"",
		      sequence_num(o->op_next));
    else
	PerlIO_printf(file, "DONE\"");

    if (o->op_targ) {
	if (o->op_type == OP_NULL)
	{
	    PerlIO_printf(file, " was=\"%s\"", PL_op_name[o->op_targ]);
	    if (o->op_targ == OP_NEXTSTATE)
	    {
		if (CopLINE(cCOPo))
		    PerlIO_printf(file, " line=\"%"UVuf"\"",
				     (UV)CopLINE(cCOPo));
		if (CopSTASHPV(cCOPo))
		    PerlIO_printf(file, " package=\"%s\"",
				     CopSTASHPV(cCOPo));
		if (CopLABEL(cCOPo))
		    PerlIO_printf(file, " label=\"%s\"",
				     CopLABEL(cCOPo));
	    }
	}
	else
	    PerlIO_printf(file, " targ=\"%ld\"", (long)o->op_targ);
    }
#ifdef DUMPADDR
    PerlIO_printf(file, " addr=\"0x%"UVxf" => 0x%"UVxf"\"", (UV)o, (UV)o->op_next);
#endif
    if (o->op_flags) {
	SV * const tmpsv = newSVpvs("");
	switch (o->op_flags & OPf_WANT) {
	case OPf_WANT_VOID:
	    sv_catpv(tmpsv, ",VOID");
	    break;
	case OPf_WANT_SCALAR:
	    sv_catpv(tmpsv, ",SCALAR");
	    break;
	case OPf_WANT_LIST:
	    sv_catpv(tmpsv, ",LIST");
	    break;
	default:
	    sv_catpv(tmpsv, ",UNKNOWN");
	    break;
	}
	if (o->op_flags & OPf_KIDS)
	    sv_catpv(tmpsv, ",KIDS");
	if (o->op_flags & OPf_PARENS)
	    sv_catpv(tmpsv, ",PARENS");
	if (o->op_flags & OPf_STACKED)
	    sv_catpv(tmpsv, ",STACKED");
	if (o->op_flags & OPf_REF)
	    sv_catpv(tmpsv, ",REF");
	if (o->op_flags & OPf_MOD)
	    sv_catpv(tmpsv, ",MOD");
	if (o->op_flags & OPf_SPECIAL)
	    sv_catpv(tmpsv, ",SPECIAL");
	PerlIO_printf(file, " flags=\"%s\"", SvCUR(tmpsv) ? SvPVX(tmpsv) + 1 : "");
	SvREFCNT_dec(tmpsv);
    }
    if (o->op_private) {
	SV * const tmpsv = newSVpvs("");
	if (PL_opargs[o->op_type] & OA_TARGLEX) {
	    if (o->op_private & OPpTARGET_MY)
		sv_catpv(tmpsv, ",TARGET_MY");
	}
	else if (o->op_type == OP_LEAVESUB ||
		 o->op_type == OP_LEAVE ||
		 o->op_type == OP_LEAVESUBLV ||
		 o->op_type == OP_LEAVEWRITE) {
	    if (o->op_private & OPpREFCOUNTED)
		sv_catpv(tmpsv, ",REFCOUNTED");
	}
        else if (o->op_type == OP_AASSIGN) {
	    if (o->op_private & OPpASSIGN_COMMON)
		sv_catpv(tmpsv, ",COMMON");
	}
	else if (o->op_type == OP_SASSIGN) {
	    if (o->op_private & OPpASSIGN_BACKWARDS)
		sv_catpv(tmpsv, ",BACKWARDS");
	}
	else if (o->op_type == OP_TRANS) {
	    if (o->op_private & OPpTRANS_SQUASH)
		sv_catpv(tmpsv, ",SQUASH");
	    if (o->op_private & OPpTRANS_DELETE)
		sv_catpv(tmpsv, ",DELETE");
	    if (o->op_private & OPpTRANS_COMPLEMENT)
		sv_catpv(tmpsv, ",COMPLEMENT");
	    if (o->op_private & OPpTRANS_IDENTICAL)
		sv_catpv(tmpsv, ",IDENTICAL");
	    if (o->op_private & OPpTRANS_GROWS)
		sv_catpv(tmpsv, ",GROWS");
	}
	else if (o->op_type == OP_REPEAT) {
	    if (o->op_private & OPpREPEAT_DOLIST)
		sv_catpv(tmpsv, ",DOLIST");
	}
	else if (o->op_type == OP_ENTERSUB ||
		 o->op_type == OP_RV2SV ||
		 o->op_type == OP_GVSV ||
		 o->op_type == OP_RV2AV ||
		 o->op_type == OP_RV2HV ||
		 o->op_type == OP_RV2GV ||
		 o->op_type == OP_AELEM ||
		 o->op_type == OP_HELEM )
	{
	    if (o->op_type == OP_ENTERSUB) {
		if (o->op_private & OPpENTERSUB_AMPER)
		    sv_catpv(tmpsv, ",AMPER");
		if (o->op_private & OPpENTERSUB_DB)
		    sv_catpv(tmpsv, ",DB");
		if (o->op_private & OPpENTERSUB_HASTARG)
		    sv_catpv(tmpsv, ",HASTARG");
		if (o->op_private & OPpENTERSUB_NOPAREN)
		    sv_catpv(tmpsv, ",NOPAREN");
		if (o->op_private & OPpENTERSUB_INARGS)
		    sv_catpv(tmpsv, ",INARGS");
	    }
	    else {
		switch (o->op_private & OPpDEREF) {
	    case OPpDEREF_SV:
		sv_catpv(tmpsv, ",SV");
		break;
	    case OPpDEREF_AV:
		sv_catpv(tmpsv, ",AV");
		break;
	    case OPpDEREF_HV:
		sv_catpv(tmpsv, ",HV");
		break;
	    }
		if (o->op_private & OPpMAYBE_LVSUB)
		    sv_catpv(tmpsv, ",MAYBE_LVSUB");
	    }
	    if (o->op_type == OP_AELEM || o->op_type == OP_HELEM) {
		if (o->op_private & OPpLVAL_DEFER)
		    sv_catpv(tmpsv, ",LVAL_DEFER");
	    }
	    else {
		if (o->op_private & HINT_STRICT_REFS)
		    sv_catpv(tmpsv, ",STRICT_REFS");
		if (o->op_private & OPpOUR_INTRO)
		    sv_catpv(tmpsv, ",OUR_INTRO");
	    }
	}
	else if (o->op_type == OP_CONST) {
	    if (o->op_private & OPpCONST_BARE)
		sv_catpv(tmpsv, ",BARE");
	    if (o->op_private & OPpCONST_STRICT)
		sv_catpv(tmpsv, ",STRICT");
	    if (o->op_private & OPpCONST_WARNING)
		sv_catpv(tmpsv, ",WARNING");
	    if (o->op_private & OPpCONST_ENTERED)
		sv_catpv(tmpsv, ",ENTERED");
	}
	else if (o->op_type == OP_FLIP) {
	    if (o->op_private & OPpFLIP_LINENUM)
		sv_catpv(tmpsv, ",LINENUM");
	}
	else if (o->op_type == OP_FLOP) {
	    if (o->op_private & OPpFLIP_LINENUM)
		sv_catpv(tmpsv, ",LINENUM");
	}
	else if (o->op_type == OP_RV2CV) {
	    if (o->op_private & OPpLVAL_INTRO)
		sv_catpv(tmpsv, ",INTRO");
	}
	else if (o->op_type == OP_GV) {
	    if (o->op_private & OPpEARLY_CV)
		sv_catpv(tmpsv, ",EARLY_CV");
	}
	else if (o->op_type == OP_LIST) {
	    if (o->op_private & OPpLIST_GUESSED)
		sv_catpv(tmpsv, ",GUESSED");
	}
	else if (o->op_type == OP_DELETE) {
	    if (o->op_private & OPpSLICE)
		sv_catpv(tmpsv, ",SLICE");
	}
	else if (o->op_type == OP_EXISTS) {
	    if (o->op_private & OPpEXISTS_SUB)
		sv_catpv(tmpsv, ",EXISTS_SUB");
	}
	else if (o->op_type == OP_SORT) {
	    if (o->op_private & OPpSORT_NUMERIC)
		sv_catpv(tmpsv, ",NUMERIC");
	    if (o->op_private & OPpSORT_INTEGER)
		sv_catpv(tmpsv, ",INTEGER");
	    if (o->op_private & OPpSORT_REVERSE)
		sv_catpv(tmpsv, ",REVERSE");
	}
	else if (o->op_type == OP_OPEN || o->op_type == OP_BACKTICK) {
	    if (o->op_private & OPpOPEN_IN_RAW)
		sv_catpv(tmpsv, ",IN_RAW");
	    if (o->op_private & OPpOPEN_IN_CRLF)
		sv_catpv(tmpsv, ",IN_CRLF");
	    if (o->op_private & OPpOPEN_OUT_RAW)
		sv_catpv(tmpsv, ",OUT_RAW");
	    if (o->op_private & OPpOPEN_OUT_CRLF)
		sv_catpv(tmpsv, ",OUT_CRLF");
	}
	else if (o->op_type == OP_EXIT) {
	    if (o->op_private & OPpEXIT_VMSISH)
		sv_catpv(tmpsv, ",EXIT_VMSISH");
	    if (o->op_private & OPpHUSH_VMSISH)
		sv_catpv(tmpsv, ",HUSH_VMSISH");
	}
	else if (o->op_type == OP_DIE) {
	    if (o->op_private & OPpHUSH_VMSISH)
		sv_catpv(tmpsv, ",HUSH_VMSISH");
	}
	else if (PL_check[o->op_type] != Perl_ck_ftst) {
	    if (OP_IS_FILETEST_ACCESS(o->op_type) && o->op_private & OPpFT_ACCESS)
		sv_catpv(tmpsv, ",FT_ACCESS");
	    if (o->op_private & OPpFT_STACKED)
		sv_catpv(tmpsv, ",FT_STACKED");
	}
	if (o->op_flags & OPf_MOD && o->op_private & OPpLVAL_INTRO)
	    sv_catpv(tmpsv, ",INTRO");
	if (SvCUR(tmpsv))
	    S_xmldump_attr(aTHX_ level, file, "private=\"%s\"", SvPVX(tmpsv) + 1);
	SvREFCNT_dec(tmpsv);
    }

    switch (o->op_type) {
    case OP_AELEMFAST:
	if (o->op_flags & OPf_SPECIAL) {
	    break;
	}
    case OP_GVSV:
    case OP_GV:
#ifdef USE_ITHREADS
	S_xmldump_attr(aTHX_ level, file, "padix=\"%" IVdf "\"", (IV)cPADOPo->op_padix);
#else
	if (cSVOPo->op_sv) {
	    SV * const tmpsv1 = newSVpvn_utf8(NULL, 0, TRUE);
	    SV * const tmpsv2 = newSVpvn_utf8("", 0, TRUE);
	    char *s;
	    STRLEN len;
	    ENTER;
	    SAVEFREESV(tmpsv1);
	    SAVEFREESV(tmpsv2);
	    gv_fullname3(tmpsv1, MUTABLE_GV(cSVOPo->op_sv), NULL);
	    s = SvPV(tmpsv1,len);
	    sv_catxmlpvn(tmpsv2, s, len, 1);
	    S_xmldump_attr(aTHX_ level, file, "gv=\"%s\"", SvPV(tmpsv2, len));
	    LEAVE;
	}
	else
	    S_xmldump_attr(aTHX_ level, file, "gv=\"NULL\"");
#endif
	break;
    case OP_CONST:
    case OP_HINTSEVAL:
    case OP_METHOD_NAMED:
#ifndef USE_ITHREADS
	/* with ITHREADS, consts are stored in the pad, and the right pad
	 * may not be active here, so skip */
	S_xmldump_attr(aTHX_ level, file, "%s", sv_xmlpeek(cSVOPo_sv));
#endif
	break;
    case OP_ANONCODE:
	if (!contents) {
	    contents = 1;
	    PerlIO_printf(file, ">\n");
	}
	do_op_xmldump(level+1, file, CvROOT(cSVOPo_sv));
	break;
    case OP_NEXTSTATE:
    case OP_DBSTATE:
	if (CopLINE(cCOPo))
	    S_xmldump_attr(aTHX_ level, file, "line=\"%"UVuf"\"",
			     (UV)CopLINE(cCOPo));
	if (CopSTASHPV(cCOPo))
	    S_xmldump_attr(aTHX_ level, file, "package=\"%s\"",
			     CopSTASHPV(cCOPo));
	if (CopLABEL(cCOPo))
	    S_xmldump_attr(aTHX_ level, file, "label=\"%s\"",
			     CopLABEL(cCOPo));
	break;
    case OP_ENTERLOOP:
	S_xmldump_attr(aTHX_ level, file, "redo=\"");
	if (cLOOPo->op_redoop)
	    PerlIO_printf(file, "%"UVuf"\"", sequence_num(cLOOPo->op_redoop));
	else
	    PerlIO_printf(file, "DONE\"");
	S_xmldump_attr(aTHX_ level, file, "next=\"");
	if (cLOOPo->op_nextop)
	    PerlIO_printf(file, "%"UVuf"\"", sequence_num(cLOOPo->op_nextop));
	else
	    PerlIO_printf(file, "DONE\"");
	S_xmldump_attr(aTHX_ level, file, "last=\"");
	if (cLOOPo->op_lastop)
	    PerlIO_printf(file, "%"UVuf"\"", sequence_num(cLOOPo->op_lastop));
	else
	    PerlIO_printf(file, "DONE\"");
	break;
    case OP_COND_EXPR:
    case OP_RANGE:
    case OP_MAPWHILE:
    case OP_GREPWHILE:
    case OP_OR:
    case OP_AND:
	S_xmldump_attr(aTHX_ level, file, "other=\"");
	if (cLOGOPo->op_other)
	    PerlIO_printf(file, "%"UVuf"\"", sequence_num(cLOGOPo->op_other));
	else
	    PerlIO_printf(file, "DONE\"");
	break;
    case OP_LEAVE:
    case OP_LEAVEEVAL:
    case OP_LEAVESUB:
    case OP_LEAVESUBLV:
    case OP_LEAVEWRITE:
    case OP_SCOPE:
	if (o->op_private & OPpREFCOUNTED)
	    S_xmldump_attr(aTHX_ level, file, "refcnt=\"%"UVuf"\"", (UV)o->op_targ);
	break;
    default:
	break;
    }

    if (PL_madskills && o->op_madprop) {
	char prevkey = '\0';
	SV * const tmpsv = newSVpvn_utf8("", 0, TRUE);
	const MADPROP* mp = o->op_madprop;

	if (!contents) {
	    contents = 1;
	    PerlIO_printf(file, ">\n");
	}
	Perl_xmldump_indent(aTHX_ level, file, "<madprops>\n");
	level++;
	while (mp) {
	    char tmp = mp->mad_key;
	    sv_setpvs(tmpsv,"\"");
	    if (tmp)
		sv_catxmlpvn(tmpsv, &tmp, 1, 0);
	    if ((tmp == '_') || (tmp == '#')) /* '_' '#' whitespace belong to the previous token. */
		sv_catxmlpvn(tmpsv, &prevkey, 1, 0);
	    else
		prevkey = tmp;
	    sv_catpv(tmpsv, "\"");
	    switch (mp->mad_type) {
	    case MAD_NULL:
		sv_catpv(tmpsv, "NULL");
		Perl_xmldump_indent(aTHX_ level, file, "<mad_null key=%s/>\n", SvPVX(tmpsv));
		break;
	    case MAD_PV:
		sv_catpv(tmpsv, " val=\"");
		sv_catxmlpvn(tmpsv, (char*)mp->mad_val, mp->mad_vlen,1);
		sv_catpv(tmpsv, "\"");
		Perl_xmldump_indent(aTHX_ level, file, "<mad_pv key=%s/>\n", SvPVX(tmpsv));
		break;
	    case MAD_SV:
		sv_catpv(tmpsv, " val=\"");
		sv_catxmlsv(tmpsv, MUTABLE_SV(mp->mad_val));
		sv_catpv(tmpsv, "\"");
		Perl_xmldump_indent(aTHX_ level, file, "<mad_sv key=%s/>\n", SvPVX(tmpsv));
		break;
	    case MAD_OP:
		if ((OP*)mp->mad_val) {
		    Perl_xmldump_indent(aTHX_ level, file, "<mad_op key=%s>\n", SvPVX(tmpsv));
		    do_op_xmldump(level+1, file, (OP*)mp->mad_val);
		    Perl_xmldump_indent(aTHX_ level, file, "</mad_op>\n");
		}
		break;
	    default:
		Perl_xmldump_indent(aTHX_ level, file, "<mad_unk key=%s/>\n", SvPVX(tmpsv));
		break;
	    }
	    mp = mp->mad_next;
	}
	level--;
	Perl_xmldump_indent(aTHX_ level, file, "</madprops>\n");

	SvREFCNT_dec(tmpsv);
    }

    switch (o->op_type) {
    case OP_PUSHRE:
    case OP_MATCH:
    case OP_QR:
    case OP_SUBST:
	if (!contents) {
	    contents = 1;
	    PerlIO_printf(file, ">\n");
	}
	do_pmop_xmldump(level, file, cPMOPo);
	break;
    default:
	break;
    }

    if (o->op_flags & OPf_KIDS) {
	OP *kid;
	if (!contents) {
	    contents = 1;
	    PerlIO_printf(file, ">\n");
	}
	for (kid = cUNOPo->op_first; kid; kid = kid->op_sibling)
	    do_op_xmldump(level, file, kid);
    }

    if (contents)
	Perl_xmldump_indent(aTHX_ level-1, file, "</op_%s>\n", OP_NAME(o));
    else
	PerlIO_printf(file, " />\n");
}

void
Perl_op_xmldump(pTHX_ const OP *o)
{
    PERL_ARGS_ASSERT_OP_XMLDUMP;

    do_op_xmldump(0, PL_xmlfp, o);
}
#endif

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 *
 * ex: set ts=8 sts=4 sw=4 noet:
 */
