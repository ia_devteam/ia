#include "var/tmp/sensor.h"
/*
 * Copyright (c) 1989, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * Guido van Rossum.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)glob.c	8.3 (Berkeley) 10/13/93";
/* most changes between the version above and the one below have been ported:
static char sscsid[]=  "$OpenBSD: glob.c,v 1.8.10.1 2001/04/10 jason Exp $";
 */
#endif /* LIBC_SCCS and not lint */

/*
 * glob(3) -- a superset of the one defined in POSIX 1003.2.
 *
 * The [!...] convention to negate a range is supported (SysV, Posix, ksh).
 *
 * Optional extra services, controlled by flags not defined by POSIX:
 *
 * GLOB_QUOTE:
 *	Escaping convention: \ inhibits any special meaning the following
 *	character might have (except \ at end of string is retained).
 * GLOB_MAGCHAR:
 *	Set in gl_flags if pattern contained a globbing character.
 * GLOB_NOMAGIC:
 *	Same as GLOB_NOCHECK, but it will only append pattern if it did
 *	not contain any magic characters.  [Used in csh style globbing]
 * GLOB_ALTDIRFUNC:
 *	Use alternately specified directory access functions.
 * GLOB_TILDE:
 *	expand ~user/foo to the /home/dir/of/user/foo
 * GLOB_BRACE:
 *	expand {1,2}{a,b} to 1a 1b 2a 2b
 * gl_matchc:
 *	Number of matches in the current invocation of glob.
 * GLOB_ALPHASORT:
 *	sort alphabetically like csh (case doesn't matter) instead of in ASCII
 *	order
 */

#include <EXTERN.h>
#include <perl.h>
#include <XSUB.h>

#include "bsd_glob.h"
#ifdef I_PWD
#	include <pwd.h>
#else
#if defined(HAS_PASSWD) && !defined(VMS)
	struct passwd *getpwnam(char *);
	struct passwd *getpwuid(Uid_t);
#endif
#endif

#ifndef MAXPATHLEN
#  ifdef PATH_MAX
#    define	MAXPATHLEN	PATH_MAX
#  else
#    define	MAXPATHLEN	1024
#  endif
#endif

#ifdef I_LIMITS
#include <limits.h>
#endif

#ifndef ARG_MAX
#  ifdef _SC_ARG_MAX
#    define		ARG_MAX		(sysconf(_SC_ARG_MAX))
#  else
#    ifdef _POSIX_ARG_MAX
#      define		ARG_MAX		_POSIX_ARG_MAX
#    else
#      ifdef WIN32
#        define	ARG_MAX		14500	/* from VC's limits.h */
#      else
#        define	ARG_MAX		4096	/* from POSIX, be conservative */
#      endif
#    endif
#  endif
#endif

#define	BG_DOLLAR	'$'
#define	BG_DOT		'.'
#define	BG_EOS		'\0'
#define	BG_LBRACKET	'['
#define	BG_NOT		'!'
#define	BG_QUESTION	'?'
#define	BG_QUOTE	'\\'
#define	BG_RANGE	'-'
#define	BG_RBRACKET	']'
#define	BG_SEP	'/'
#ifdef DOSISH
#define BG_SEP2		'\\'
#endif
#define	BG_STAR		'*'
#define	BG_TILDE	'~'
#define	BG_UNDERSCORE	'_'
#define	BG_LBRACE	'{'
#define	BG_RBRACE	'}'
#define	BG_SLASH	'/'
#define	BG_COMMA	','

#ifndef GLOB_DEBUG

#define	M_QUOTE		0x8000
#define	M_PROTECT	0x4000
#define	M_MASK		0xffff
#define	M_ASCII		0x00ff

typedef U16 Char;

#else

#define	M_QUOTE		0x80
#define	M_PROTECT	0x40
#define	M_MASK		0xff
#define	M_ASCII		0x7f

typedef U8 Char;

#endif /* !GLOB_DEBUG */


#define	CHAR(c)		((Char)((c)&M_ASCII))
#define	META(c)		((Char)((c)|M_QUOTE))
#define	M_ALL		META('*')
#define	M_END		META(']')
#define	M_NOT		META('!')
#define	M_ONE		META('?')
#define	M_RNG		META('-')
#define	M_SET		META('[')
#define	ismeta(c)	(((c)&M_QUOTE) != 0)


static int	 compare(const void *, const void *);
static int	 ci_compare(const void *, const void *);
static int	 g_Ctoc(const Char *, char *, STRLEN);
static int	 g_lstat(Char *, Stat_t *, glob_t *);
static DIR	*g_opendir(Char *, glob_t *);
static Char	*g_strchr(Char *, int);
static int	 g_stat(Char *, Stat_t *, glob_t *);
static int	 glob0(const Char *, glob_t *);
static int	 glob1(Char *, Char *, glob_t *, size_t *);
static int	 glob2(Char *, Char *, Char *, Char *, Char *, Char *,
		       glob_t *, size_t *);
static int	 glob3(Char *, Char *, Char *, Char *, Char *, Char *,
		       Char *, Char *, glob_t *, size_t *);
static int	 globextend(const Char *, glob_t *, size_t *);
static const Char *
		 globtilde(const Char *, Char *, size_t, glob_t *);
static int	 globexp1(const Char *, glob_t *);
static int	 globexp2(const Char *, const Char *, glob_t *, int *);
static int	 match(Char *, Char *, Char *, int);
#ifdef GLOB_DEBUG
static void	 qprintf(const char *, Char *);
#endif /* GLOB_DEBUG */

#ifdef PERL_IMPLICIT_CONTEXT
static Direntry_t *	my_readdir(DIR*);

static Direntry_t *
my_readdir(DIR *d)
{
#ifndef NETWARE
    {__IASTRUCT__ dirent * ReplaceReturn = PerlDir_read(d); SensorCall(); return ReplaceReturn;}
#else
    return (DIR *)PerlDir_read(d);
#endif
}
#else

/* ReliantUNIX (OS formerly known as SINIX) defines readdir
 * in LFS-mode to be a 64-bit version of readdir.  */

#   ifdef sinix
static Direntry_t *    my_readdir(DIR*);

static Direntry_t *
my_readdir(DIR *d)
{
    return readdir(d);
}
#   else

#       define	my_readdir	readdir

#   endif

#endif

int
bsd_glob(const char *pattern, int flags,
	 int (*errfunc)(const char *, int), glob_t *pglob)
{
	SensorCall();const U8 *patnext;
	int c;
	Char *bufnext, *bufend, patbuf[MAXPATHLEN];
	patnext = (U8 *) pattern;
	/* TODO: GLOB_APPEND / GLOB_DOOFFS aren't supported yet */
#if 0
	if (!(flags & GLOB_APPEND)) {
		pglob->gl_pathc = 0;
		pglob->gl_pathv = NULL;
		if (!(flags & GLOB_DOOFFS))
			pglob->gl_offs = 0;
	}
#else
	pglob->gl_pathc = 0;
	pglob->gl_pathv = NULL;
	pglob->gl_offs = 0;
#endif
	pglob->gl_flags = flags & ~GLOB_MAGCHAR;
	pglob->gl_errfunc = errfunc;
	pglob->gl_matchc = 0;

	bufnext = patbuf;
	bufend = bufnext + MAXPATHLEN - 1;
#ifdef DOSISH
	/* Nasty hack to treat patterns like "C:*" correctly. In this
	 * case, the * should match any file in the current directory
	 * on the C: drive. However, the glob code does not treat the
	 * colon specially, so it looks for files beginning "C:" in
	 * the current directory. To fix this, change the pattern to
	 * add an explicit "./" at the start (just after the drive
	 * letter and colon - ie change to "C:./").
	 */
	if (isalpha(pattern[0]) && pattern[1] == ':' &&
	    pattern[2] != BG_SEP && pattern[2] != BG_SEP2 &&
	    bufend - bufnext > 4) {
		*bufnext++ = pattern[0];
		*bufnext++ = ':';
		*bufnext++ = '.';
		*bufnext++ = BG_SEP;
		patnext += 2;
	}
#endif

	SensorCall();if (flags & GLOB_QUOTE) {
		/* Protect the quoted characters. */
		SensorCall();while (bufnext < bufend && (c = *patnext++) != BG_EOS)
			{/*1*/SensorCall();if (c == BG_QUOTE) {
#ifdef DOSISH
				    /* To avoid backslashitis on Win32,
				     * we only treat \ as a quoting character
				     * if it precedes one of the
				     * metacharacters []-{}~\
				     */
				if ((c = *patnext++) != '[' && c != ']' &&
				    c != '-' && c != '{' && c != '}' &&
				    c != '~' && c != '\\') {
#else
				SensorCall();if ((c = *patnext++) == BG_EOS) {
#endif
					SensorCall();c = BG_QUOTE;
					--patnext;
				}
				SensorCall();*bufnext++ = c | M_PROTECT;
			} else
				{/*3*/SensorCall();*bufnext++ = c;/*4*/}/*2*/}
	} else
		{/*5*/SensorCall();while (bufnext < bufend && (c = *patnext++) != BG_EOS)
			{/*7*/SensorCall();*bufnext++ = c;/*8*/}/*6*/}
	SensorCall();*bufnext = BG_EOS;

	SensorCall();if (flags & GLOB_BRACE)
	    {/*9*/{int  ReplaceReturn = globexp1(patbuf, pglob); SensorCall(); return ReplaceReturn;}/*10*/}
	else
	    {/*11*/{int  ReplaceReturn = glob0(patbuf, pglob); SensorCall(); return ReplaceReturn;}/*12*/}
SensorCall();}

/*
 * Expand recursively a glob {} pattern. When there is no more expansion
 * invoke the standard globbing routine to glob the rest of the magic
 * characters
 */
static int
globexp1(const Char *pattern, glob_t *pglob)
{
	SensorCall();const Char* ptr = pattern;
	int rv;

	/* Protect a single {}, for find(1), like csh */
	SensorCall();if (pattern[0] == BG_LBRACE && pattern[1] == BG_RBRACE && pattern[2] == BG_EOS)
		{/*95*/{int  ReplaceReturn = glob0(pattern, pglob); SensorCall(); return ReplaceReturn;}/*96*/}

	SensorCall();while ((ptr = (const Char *) g_strchr((Char *) ptr, BG_LBRACE)) != NULL)
		{/*97*/SensorCall();if (!globexp2(ptr, pattern, pglob, &rv))
			{/*99*/{int  ReplaceReturn = rv; SensorCall(); return ReplaceReturn;}/*100*/}/*98*/}

	{int  ReplaceReturn = glob0(pattern, pglob); SensorCall(); return ReplaceReturn;}
}


/*
 * Recursive brace globbing helper. Tries to expand a single brace.
 * If it succeeds then it invokes globexp1 with the new pattern.
 * If it fails then it tries to glob the rest of the pattern and returns.
 */
static int
globexp2(const Char *ptr, const Char *pattern,
	 glob_t *pglob, int *rv)
{
	SensorCall();int     i;
	Char   *lm, *ls;
	const Char *pe, *pm, *pm1, *pl;
	Char    patbuf[MAXPATHLEN];

	/* copy part up to the brace */
	SensorCall();for (lm = patbuf, pm = pattern; pm != ptr; *lm++ = *pm++)
		;
	SensorCall();*lm = BG_EOS;
	ls = lm;

	/* Find the balanced brace */
	SensorCall();for (i = 0, pe = ++ptr; *pe; pe++)
		{/*103*/SensorCall();if (*pe == BG_LBRACKET) {
			/* Ignore everything between [] */
			SensorCall();for (pm = pe++; *pe != BG_RBRACKET && *pe != BG_EOS; pe++)
				;
			SensorCall();if (*pe == BG_EOS) {
				/*
				 * We could not find a matching BG_RBRACKET.
				 * Ignore and just look for BG_RBRACE
				 */
				SensorCall();pe = pm;
			}
		} else {/*107*/SensorCall();if (*pe == BG_LBRACE)
			{/*109*/SensorCall();i++;/*110*/}
		else {/*111*/SensorCall();if (*pe == BG_RBRACE) {
			SensorCall();if (i == 0)
				{/*113*/SensorCall();break;/*114*/}
			SensorCall();i--;
		;/*112*/}/*108*/}/*104*/}}

	/* Non matching braces; just glob the pattern */
	SensorCall();if (i != 0 || *pe == BG_EOS) {
		SensorCall();*rv = glob0(patbuf, pglob);
		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}

	SensorCall();for (i = 0, pl = pm = ptr; pm <= pe; pm++) {
		SensorCall();switch (*pm) {
		case BG_LBRACKET:
			/* Ignore everything between [] */
			SensorCall();for (pm1 = pm++; *pm != BG_RBRACKET && *pm != BG_EOS; pm++)
				;
			SensorCall();if (*pm == BG_EOS) {
				/*
				 * We could not find a matching BG_RBRACKET.
				 * Ignore and just look for BG_RBRACE
				 */
				SensorCall();pm = pm1;
			}
			SensorCall();break;

		case BG_LBRACE:
			SensorCall();i++;
			SensorCall();break;

		case BG_RBRACE:
			SensorCall();if (i) {
				SensorCall();i--;
				SensorCall();break;
			}
			/* FALLTHROUGH */
		case BG_COMMA:
			SensorCall();if (i && *pm == BG_COMMA)
				{/*117*/SensorCall();break;/*118*/}
			else {
				/* Append the current string */
				SensorCall();for (lm = ls; (pl < pm); *lm++ = *pl++)
					;

				/*
				 * Append the rest of the pattern after the
				 * closing brace
				 */
				SensorCall();for (pl = pe + 1; (*lm++ = *pl++) != BG_EOS; )
					;

				/* Expand the current pattern */
#ifdef GLOB_DEBUG
				qprintf("globexp2:", patbuf);
#endif /* GLOB_DEBUG */
				SensorCall();*rv = globexp1(patbuf, pglob);

				/* move after the comma, to the next string */
				pl = pm + 1;
			}
			SensorCall();break;

		default:
			SensorCall();break;
		}
	}
	SensorCall();*rv = 0;
	{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}



/*
 * expand tilde from the passwd file.
 */
static const Char *
globtilde(const Char *pattern, Char *patbuf, size_t patbuf_len, glob_t *pglob)
{
	SensorCall();char *h;
	const Char *p;
	Char *b, *eb;

	SensorCall();if (*pattern != BG_TILDE || !(pglob->gl_flags & GLOB_TILDE))
		{/*79*/{const Char * ReplaceReturn = pattern; SensorCall(); return ReplaceReturn;}/*80*/}

	/* Copy up to the end of the string or / */
	SensorCall();eb = &patbuf[patbuf_len - 1];
	SensorCall();for (p = pattern + 1, h = (char *) patbuf;
	     h < (char*)eb && *p && *p != BG_SLASH; *h++ = (char)*p++)
		;

	SensorCall();*h = BG_EOS;

#if 0
	if (h == (char *)eb)
		return what;
#endif

	SensorCall();if (((char *) patbuf)[0] == BG_EOS) {
		/*
		 * handle a plain ~ or ~/ by expanding $HOME
		 * first and then trying the password file
		 * or $USERPROFILE on DOSISH systems
		 */
		SensorCall();if ((h = getenv("HOME")) == NULL) {
#ifdef HAS_PASSWD
			SensorCall();struct passwd *pwd;
			SensorCall();if ((pwd = getpwuid(getuid())) == NULL)
				{/*83*/{const Char * ReplaceReturn = pattern; SensorCall(); return ReplaceReturn;}/*84*/}
			else
				{/*85*/SensorCall();h = pwd->pw_dir;/*86*/}
#elif DOSISH
			/*
			 * When no passwd file, fallback to the USERPROFILE
			 * environment variable on DOSish systems.
			 */
			if ((h = getenv("USERPROFILE")) == NULL) {
			    return pattern;
			}
#else
                        return pattern;
#endif
		}
	} else {
		/*
		 * Expand a ~user
		 */
#ifdef HAS_PASSWD
		SensorCall();struct passwd *pwd;
		SensorCall();if ((pwd = getpwnam((char*) patbuf)) == NULL)
			{/*87*/{const Char * ReplaceReturn = pattern; SensorCall(); return ReplaceReturn;}/*88*/}
		else
			{/*89*/SensorCall();h = pwd->pw_dir;/*90*/}
#else
                return pattern;
#endif
	}

	/* Copy the home directory */
	SensorCall();for (b = patbuf; b < eb && *h; *b++ = *h++)
		;

	/* Append the rest of the pattern */
	SensorCall();while (b < eb && (*b++ = *p++) != BG_EOS)
		;
	SensorCall();*b = BG_EOS;

	{const Char * ReplaceReturn = patbuf; SensorCall(); return ReplaceReturn;}
}


/*
 * The main glob() routine: compiles the pattern (optionally processing
 * quotes), calls glob1() to do the real pattern matching, and finally
 * sorts the list (unless unsorted operation is requested).  Returns 0
 * if things went well, nonzero if errors occurred.  It is not an error
 * to find no matches.
 */
static int
glob0(const Char *pattern, glob_t *pglob)
{
	SensorCall();const Char *qpat, *qpatnext;
	int c, err, oldflags, oldpathc;
	Char *bufnext, patbuf[MAXPATHLEN];
	size_t limit = 0;

	qpat = globtilde(pattern, patbuf, MAXPATHLEN, pglob);
	qpatnext = qpat;
	oldflags = pglob->gl_flags;
	oldpathc = pglob->gl_pathc;
	bufnext = patbuf;

	/* We don't need to check for buffer overflow any more. */
	SensorCall();while ((c = *qpatnext++) != BG_EOS) {
		SensorCall();switch (c) {
		case BG_LBRACKET:
			SensorCall();c = *qpatnext;
			SensorCall();if (c == BG_NOT)
				{/*33*/SensorCall();++qpatnext;/*34*/}
			SensorCall();if (*qpatnext == BG_EOS ||
			    g_strchr((Char *) qpatnext+1, BG_RBRACKET) == NULL) {
				SensorCall();*bufnext++ = BG_LBRACKET;
				SensorCall();if (c == BG_NOT)
					{/*35*/SensorCall();--qpatnext;/*36*/}
				SensorCall();break;
			}
			SensorCall();*bufnext++ = M_SET;
			if (c == BG_NOT)
				*bufnext++ = M_NOT;
			SensorCall();c = *qpatnext++;
			SensorCall();do {
				SensorCall();*bufnext++ = CHAR(c);
				SensorCall();if (*qpatnext == BG_RANGE &&
				    (c = qpatnext[1]) != BG_RBRACKET) {
					SensorCall();*bufnext++ = M_RNG;
					*bufnext++ = CHAR(c);
					qpatnext += 2;
				}
			} while ((c = *qpatnext++) != BG_RBRACKET);
			SensorCall();pglob->gl_flags |= GLOB_MAGCHAR;
			*bufnext++ = M_END;
			SensorCall();break;
		case BG_QUESTION:
			SensorCall();pglob->gl_flags |= GLOB_MAGCHAR;
			*bufnext++ = M_ONE;
			SensorCall();break;
		case BG_STAR:
			SensorCall();pglob->gl_flags |= GLOB_MAGCHAR;
			/* collapse adjacent stars to one,
			 * to avoid exponential behavior
			 */
			if (bufnext == patbuf || bufnext[-1] != M_ALL)
				*bufnext++ = M_ALL;
			SensorCall();break;
		default:
			SensorCall();*bufnext++ = CHAR(c);
			SensorCall();break;
		}
	}
	SensorCall();*bufnext = BG_EOS;
#ifdef GLOB_DEBUG
	qprintf("glob0:", patbuf);
#endif /* GLOB_DEBUG */

	SensorCall();if ((err = glob1(patbuf, patbuf+MAXPATHLEN-1, pglob, &limit)) != 0) {
		SensorCall();pglob->gl_flags = oldflags;
		SensorCall();return(err);
	}

	/*
	 * If there was no match we are going to append the pattern
	 * if GLOB_NOCHECK was specified or if GLOB_NOMAGIC was specified
	 * and the pattern did not contain any magic characters
	 * GLOB_NOMAGIC is there just for compatibility with csh.
	 */
	SensorCall();if (pglob->gl_pathc == oldpathc &&
	    ((pglob->gl_flags & GLOB_NOCHECK) ||
	      ((pglob->gl_flags & GLOB_NOMAGIC) &&
	       !(pglob->gl_flags & GLOB_MAGCHAR))))
	{
#ifdef GLOB_DEBUG
		printf("calling globextend from glob0\n");
#endif /* GLOB_DEBUG */
		SensorCall();pglob->gl_flags = oldflags;
		SensorCall();return(globextend(qpat, pglob, &limit));
        }
	else {/*41*/SensorCall();if (!(pglob->gl_flags & GLOB_NOSORT))
		{/*43*/SensorCall();qsort(pglob->gl_pathv + pglob->gl_offs + oldpathc,
		    pglob->gl_pathc - oldpathc, sizeof(char *),
		    (pglob->gl_flags & (GLOB_ALPHASORT|GLOB_NOCASE))
			? ci_compare : compare);/*44*/}/*42*/}
	SensorCall();pglob->gl_flags = oldflags;
	SensorCall();return(0);
}

static int
ci_compare(const void *p, const void *q)
{
	SensorCall();const char *pp = *(const char **)p;
	const char *qq = *(const char **)q;
	int ci;
	SensorCall();while (*pp && *qq) {
		SensorCall();if (toLOWER(*pp) != toLOWER(*qq))
			{/*13*/SensorCall();break;/*14*/}
		SensorCall();++pp;
		++qq;
	}
	SensorCall();ci = toLOWER(*pp) - toLOWER(*qq);
	SensorCall();if (ci == 0)
		{/*15*/{int  ReplaceReturn = compare(p, q); SensorCall(); return ReplaceReturn;}/*16*/}
	{int  ReplaceReturn = ci; SensorCall(); return ReplaceReturn;}
}

static int
compare(const void *p, const void *q)
{
	SensorCall();return(strcmp(*(char **)p, *(char **)q));
}

static int
glob1(Char *pattern, Char *pattern_last, glob_t *pglob, size_t *limitp)
{
	SensorCall();Char pathbuf[MAXPATHLEN];

	/* A null pathname is invalid -- POSIX 1003.1 sect. 2.4. */
	SensorCall();if (*pattern == BG_EOS)
		{/*45*/SensorCall();return(0);/*46*/}
	SensorCall();return(glob2(pathbuf, pathbuf+MAXPATHLEN-1,
		     pathbuf, pathbuf+MAXPATHLEN-1,
		     pattern, pattern_last, pglob, limitp));
}

/*
 * The functions glob2 and glob3 are mutually recursive; there is one level
 * of recursion for each segment in the pattern that contains one or more
 * meta characters.
 */
static int
glob2(Char *pathbuf, Char *pathbuf_last, Char *pathend, Char *pathend_last,
      Char *pattern, Char *pattern_last, glob_t *pglob, size_t *limitp)
{
SensorCall();	Stat_t sb;
	Char *p, *q;
	int anymeta;

	/*
	 * Loop over pattern segments until end of pattern or until
	 * segment with meta character found.
	 */
	SensorCall();for (anymeta = 0;;) {
		SensorCall();if (*pattern == BG_EOS) {		/* End of pattern? */
			SensorCall();*pathend = BG_EOS;
			SensorCall();if (g_lstat(pathbuf, &sb, pglob))
				{/*47*/SensorCall();return(0);/*48*/}

			SensorCall();if (((pglob->gl_flags & GLOB_MARK) &&
			    pathend[-1] != BG_SEP
#ifdef DOSISH
			    && pathend[-1] != BG_SEP2
#endif
			    ) && (S_ISDIR(sb.st_mode) ||
				  (S_ISLNK(sb.st_mode) &&
			    (g_stat(pathbuf, &sb, pglob) == 0) &&
			    S_ISDIR(sb.st_mode)))) {
				SensorCall();if (pathend+1 > pathend_last)
					{/*49*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*50*/}
				SensorCall();*pathend++ = BG_SEP;
				*pathend = BG_EOS;
			}
			SensorCall();++pglob->gl_matchc;
#ifdef GLOB_DEBUG
                        printf("calling globextend from glob2\n");
#endif /* GLOB_DEBUG */
			SensorCall();return(globextend(pathbuf, pglob, limitp));
		}

		/* Find end of next segment, copy tentatively to pathend. */
		SensorCall();q = pathend;
		p = pattern;
		SensorCall();while (*p != BG_EOS && *p != BG_SEP
#ifdef DOSISH
		       && *p != BG_SEP2
#endif
		       ) {
			SensorCall();if (ismeta(*p))
				{/*51*/SensorCall();anymeta = 1;/*52*/}
			SensorCall();if (q+1 > pathend_last)
				{/*53*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*54*/}
			SensorCall();*q++ = *p++;
		}

		SensorCall();if (!anymeta) {		/* No expansion, do next segment. */
			SensorCall();pathend = q;
			pattern = p;
			SensorCall();while (*pattern == BG_SEP
#ifdef DOSISH
			       || *pattern == BG_SEP2
#endif
			       ) {
				SensorCall();if (pathend+1 > pathend_last)
					{/*55*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*56*/}
				SensorCall();*pathend++ = *pattern++;
			}
		} else
			/* Need expansion, recurse. */
			{/*57*/SensorCall();return(glob3(pathbuf, pathbuf_last, pathend,
				     pathend_last, pattern, pattern_last,
				     p, pattern_last, pglob, limitp));/*58*/}
	}
	/* NOTREACHED */
SensorCall();}

static int
glob3(Char *pathbuf, Char *pathbuf_last, Char *pathend, Char *pathend_last,
      Char *pattern, Char *pattern_last,
      Char *restpattern, Char *restpattern_last, glob_t *pglob, size_t *limitp)
{
	SensorCall();register Direntry_t *dp;
	DIR *dirp;
	int err;
	int nocase;
	char buf[MAXPATHLEN];

	/*
	 * The readdirfunc declaration can't be prototyped, because it is
	 * assigned, below, to two functions which are prototyped in glob.h
	 * and dirent.h as taking pointers to differently typed opaque
	 * structures.
	 */
	Direntry_t *(*readdirfunc)(DIR*);

	SensorCall();if (pathend > pathend_last)
		{/*59*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*60*/}
	SensorCall();*pathend = BG_EOS;
	errno = 0;

#ifdef VMS
        {
		Char *q = pathend;
		if (q - pathbuf > 5) {
			q -= 5;
			if (q[0] == '.' &&
			    tolower(q[1]) == 'd' && tolower(q[2]) == 'i' &&
			    tolower(q[3]) == 'r' && q[4] == '/')
			{
				q[0] = '/';
				q[1] = BG_EOS;
				pathend = q+1;
			}
		}
        }
#endif

	SensorCall();if ((dirp = g_opendir(pathbuf, pglob)) == NULL) {
		/* TODO: don't call for ENOENT or ENOTDIR? */
		SensorCall();if (pglob->gl_errfunc) {
			SensorCall();if (g_Ctoc(pathbuf, buf, sizeof(buf)))
				{/*61*/{int  ReplaceReturn = (GLOB_ABEND); SensorCall(); return ReplaceReturn;}/*62*/}
			SensorCall();if (pglob->gl_errfunc(buf, errno) ||
			    (pglob->gl_flags & GLOB_ERR))
				{/*63*/{int  ReplaceReturn = (GLOB_ABEND); SensorCall(); return ReplaceReturn;}/*64*/}
		}
		SensorCall();return(0);
	}

	SensorCall();err = 0;
	nocase = ((pglob->gl_flags & GLOB_NOCASE) != 0);

	/* Search directory for matching names. */
	SensorCall();if (pglob->gl_flags & GLOB_ALTDIRFUNC)
		{/*65*/SensorCall();readdirfunc = (Direntry_t *(*)(DIR *))pglob->gl_readdir;/*66*/}
	else
		{/*67*/SensorCall();readdirfunc = (Direntry_t *(*)(DIR *))my_readdir;/*68*/}
	SensorCall();while ((dp = (*readdirfunc)(dirp))) {
		SensorCall();register U8 *sc;
		register Char *dc;

		/* Initial BG_DOT must be matched literally. */
		SensorCall();if (dp->d_name[0] == BG_DOT && *pattern != BG_DOT)
			{/*69*/SensorCall();continue;/*70*/}
		SensorCall();dc = pathend;
		sc = (U8 *) dp->d_name;
		SensorCall();while (dc < pathend_last && (*dc++ = *sc++) != BG_EOS)
			;
		SensorCall();if (dc >= pathend_last) {
			SensorCall();*dc = BG_EOS;
			err = 1;
			SensorCall();break;
		}

		SensorCall();if (!match(pathend, pattern, restpattern, nocase)) {
			SensorCall();*pathend = BG_EOS;
			SensorCall();continue;
		}
		SensorCall();err = glob2(pathbuf, pathbuf_last, --dc, pathend_last,
			    restpattern, restpattern_last, pglob, limitp);
		SensorCall();if (err)
			{/*73*/SensorCall();break;/*74*/}
	}

	SensorCall();if (pglob->gl_flags & GLOB_ALTDIRFUNC)
		{/*75*/SensorCall();(*pglob->gl_closedir)(dirp);/*76*/}
	else
		PerlDir_close(dirp);
	SensorCall();return(err);
}


/*
 * Extend the gl_pathv member of a glob_t structure to accommodate a new item,
 * add the new item, and update gl_pathc.
 *
 * This assumes the BSD realloc, which only copies the block when its size
 * crosses a power-of-two boundary; for v7 realloc, this would cause quadratic
 * behavior.
 *
 * Return 0 if new item added, error code if memory couldn't be allocated.
 *
 * Invariant of the glob_t structure:
 *	Either gl_pathc is zero and gl_pathv is NULL; or gl_pathc > 0 and
 *	gl_pathv points to (gl_offs + gl_pathc + 1) items.
 */
static int
globextend(const Char *path, glob_t *pglob, size_t *limitp)
{
	SensorCall();register char **pathv;
	register int i;
	STRLEN newsize, len;
	char *copy;
	const Char *p;

#ifdef GLOB_DEBUG
	printf("Adding ");
        for (p = path; *p; p++)
                (void)printf("%c", CHAR(*p));
        printf("\n");
#endif /* GLOB_DEBUG */

	newsize = sizeof(*pathv) * (2 + pglob->gl_pathc + pglob->gl_offs);
	SensorCall();if (pglob->gl_pathv)
		pathv = Renew(pglob->gl_pathv,newsize,char*);
	else
		Newx(pathv,newsize,char*);
	SensorCall();if (pathv == NULL) {
		SensorCall();if (pglob->gl_pathv) {
			Safefree(pglob->gl_pathv);
			SensorCall();pglob->gl_pathv = NULL;
		}
		SensorCall();return(GLOB_NOSPACE);
	}

	SensorCall();if (pglob->gl_pathv == NULL && pglob->gl_offs > 0) {
		/* first time around -- clear initial gl_offs items */
		SensorCall();pathv += pglob->gl_offs;
		SensorCall();for (i = pglob->gl_offs; --i >= 0; )
			*--pathv = NULL;
	}
	SensorCall();pglob->gl_pathv = pathv;

	SensorCall();for (p = path; *p++;)
		;
	SensorCall();len = (STRLEN)(p - path);
	*limitp += len;
	Newx(copy, p-path, char);
	SensorCall();if (copy != NULL) {
		SensorCall();if (g_Ctoc(path, copy, len)) {
			Safefree(copy);
			SensorCall();return(GLOB_NOSPACE);
		}
		SensorCall();pathv[pglob->gl_offs + pglob->gl_pathc++] = copy;
	}
	SensorCall();pathv[pglob->gl_offs + pglob->gl_pathc] = NULL;

	SensorCall();if ((pglob->gl_flags & GLOB_LIMIT) &&
	    newsize + *limitp >= ARG_MAX) {
		errno = 0;
		SensorCall();return(GLOB_NOSPACE);
	}

	SensorCall();return(copy == NULL ? GLOB_NOSPACE : 0);
}


/*
 * pattern matching function for filenames.  Each occurrence of the *
 * pattern causes a recursion level.
 */
static int
match(register Char *name, register Char *pat, register Char *patend, int nocase)
{
	SensorCall();int ok, negate_range;
	Char c, k;

	SensorCall();while (pat < patend) {
		SensorCall();c = *pat++;
		SensorCall();switch (c & M_MASK) {
		case M_ALL:
			SensorCall();if (pat == patend)
				{/*123*/SensorCall();return(1);/*124*/}
			SensorCall();do
			    {/*125*/SensorCall();if (match(name, pat, patend, nocase))
				    {/*127*/SensorCall();return(1);/*128*/}/*126*/}
			while (*name++ != BG_EOS)
				;
			SensorCall();return(0);
		case M_ONE:
			SensorCall();if (*name++ == BG_EOS)
				{/*129*/SensorCall();return(0);/*130*/}
			SensorCall();break;
		case M_SET:
			SensorCall();ok = 0;
			SensorCall();if ((k = *name++) == BG_EOS)
				{/*131*/SensorCall();return(0);/*132*/}
			SensorCall();if ((negate_range = ((*pat & M_MASK) == M_NOT)) != BG_EOS)
				{/*133*/SensorCall();++pat;/*134*/}
			SensorCall();while (((c = *pat++) & M_MASK) != M_END)
				{/*135*/SensorCall();if ((*pat & M_MASK) == M_RNG) {
					SensorCall();if (nocase) {
						SensorCall();if (tolower(c) <= tolower(k) && tolower(k) <= tolower(pat[1]))
							{/*137*/SensorCall();ok = 1;/*138*/}
					} else {
						SensorCall();if (c <= k && k <= pat[1])
							{/*139*/SensorCall();ok = 1;/*140*/}
					}
					SensorCall();pat += 2;
				} else {/*141*/SensorCall();if (nocase ? (tolower(c) == tolower(k)) : (c == k))
					{/*143*/SensorCall();ok = 1;/*144*/}/*142*/}/*136*/}
			SensorCall();if (ok == negate_range)
				{/*145*/SensorCall();return(0);/*146*/}
			SensorCall();break;
		default:
			SensorCall();k = *name++;
			SensorCall();if (nocase ? (tolower(k) != tolower(c)) : (k != c))
				{/*147*/SensorCall();return(0);/*148*/}
			SensorCall();break;
		}
	}
	SensorCall();return(*name == BG_EOS);
}

/* Free allocated data belonging to a glob_t structure. */
void
bsd_globfree(glob_t *pglob)
{
	SensorCall();register int i;
	register char **pp;

	SensorCall();if (pglob->gl_pathv != NULL) {
		SensorCall();pp = pglob->gl_pathv + pglob->gl_offs;
		SensorCall();for (i = pglob->gl_pathc; i--; ++pp)
			if (*pp)
				Safefree(*pp);
		Safefree(pglob->gl_pathv);
		SensorCall();pglob->gl_pathv = NULL;
	}
SensorCall();}

static DIR *
g_opendir(register Char *str, glob_t *pglob)
{
	SensorCall();char buf[MAXPATHLEN];

	SensorCall();if (!*str) {
		my_strlcpy(buf, ".", sizeof(buf));
	} else {
		SensorCall();if (g_Ctoc(str, buf, sizeof(buf)))
			{/*23*/SensorCall();return(NULL);/*24*/}
	}

	SensorCall();if (pglob->gl_flags & GLOB_ALTDIRFUNC)
		{/*25*/SensorCall();return((DIR*)(*pglob->gl_opendir)(buf));/*26*/}

	SensorCall();return(PerlDir_open(buf));
}

static int
g_lstat(register Char *fn, Stat_t *sb, glob_t *pglob)
{
	SensorCall();char buf[MAXPATHLEN];

	SensorCall();if (g_Ctoc(fn, buf, sizeof(buf)))
		{/*19*/SensorCall();return(-1);/*20*/}
	SensorCall();if (pglob->gl_flags & GLOB_ALTDIRFUNC)
		{/*21*/SensorCall();return((*pglob->gl_lstat)(buf, sb));/*22*/}
#ifdef HAS_LSTAT
	SensorCall();return(PerlLIO_lstat(buf, sb));
#else
	return(PerlLIO_stat(buf, sb));
#endif /* HAS_LSTAT */
}

static int
g_stat(register Char *fn, Stat_t *sb, glob_t *pglob)
{
	SensorCall();char buf[MAXPATHLEN];

	SensorCall();if (g_Ctoc(fn, buf, sizeof(buf)))
		{/*29*/SensorCall();return(-1);/*30*/}
	SensorCall();if (pglob->gl_flags & GLOB_ALTDIRFUNC)
		{/*31*/SensorCall();return((*pglob->gl_stat)(buf, sb));/*32*/}
	SensorCall();return(PerlLIO_stat(buf, sb));
}

static Char *
g_strchr(Char *str, int ch)
{
	SensorCall();do {
		SensorCall();if (*str == ch)
			{/*27*/{Char * ReplaceReturn = (str); SensorCall(); return ReplaceReturn;}/*28*/}
	} while (*str++);
	{Char * ReplaceReturn = (NULL); SensorCall(); return ReplaceReturn;}
}

static int
g_Ctoc(register const Char *str, char *buf, STRLEN len)
{
	SensorCall();while (len--) {
		SensorCall();if ((*buf++ = (char)*str++) == BG_EOS)
			{/*17*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*18*/}
	}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

#ifdef GLOB_DEBUG
static void
qprintf(const char *str, register Char *s)
{
	register Char *p;

	(void)printf("%s:\n", str);
	for (p = s; *p; p++)
		(void)printf("%c", CHAR(*p));
	(void)printf("\n");
	for (p = s; *p; p++)
		(void)printf("%c", *p & M_PROTECT ? '"' : ' ');
	(void)printf("\n");
	for (p = s; *p; p++)
		(void)printf("%c", ismeta(*p) ? '_' : ' ');
	(void)printf("\n");
}
#endif /* GLOB_DEBUG */
