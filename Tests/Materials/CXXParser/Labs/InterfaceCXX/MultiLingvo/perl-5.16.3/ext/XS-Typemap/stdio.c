#include "var/tmp/sensor.h"

/* This provides a test of STDIO and emulates a library that
   has been built outside of the PerlIO system and therefore is
   built using FILE* rather than PerlIO * (a common occurrence
   for XS).

   Use a separate file to make sure we are not contaminated by
   PerlIO.
*/

#include <stdio.h>

/* Open a file for write */
FILE * xsfopen ( const char * path ) {
  SensorCall();FILE * stream;
  stream = fopen( path, "w");
  {FILE * ReplaceReturn = stream; SensorCall(); return ReplaceReturn;}
}

int xsfclose ( FILE * stream ) {
  {int  ReplaceReturn = fclose( stream ); SensorCall(); return ReplaceReturn;}
}


int xsfprintf ( FILE * stream, const char * text ) {
  {int  ReplaceReturn = fprintf( stream, "%s", text ); SensorCall(); return ReplaceReturn;}
}

