#include "var/tmp/sensor.h"
/*
 * sdbm - ndbm work-alike hashed database library
 * based on Per-Aake Larson's Dynamic Hashing algorithms. BIT 18 (1978).
 * author: oz@nexus.yorku.ca
 * status: public domain.
 *
 * core routines
 */

#include "INTERN.h"
#include "config.h"
#ifdef WIN32
#include "io.h"
#endif
#include "sdbm.h"
#include "tune.h"
#include "pair.h"

#ifdef I_FCNTL
# include <fcntl.h>
#endif
#ifdef I_SYS_FILE
# include <sys/file.h>
#endif

#ifdef I_STRING
# ifndef __ultrix__
#  include <string.h>
# endif
#else
# include <strings.h>
#endif

/*
 * externals
 */

#include <errno.h> /* See notes in perl.h about avoiding
			extern int errno; */

extern Malloc_t malloc proto((MEM_SIZE));
extern Free_t free proto((Malloc_t));

/*
 * forward
 */
static int getdbit proto((DBM *, long));
static int setdbit proto((DBM *, long));
static int getpage proto((DBM *, long));
static datum getnext proto((DBM *));
static int makroom proto((DBM *, long, int));

/*
 * useful macros
 */
#define bad(x)		((x).dptr == NULL || (x).dsize < 0)
#define exhash(item)	sdbm_hash((item).dptr, (item).dsize)
#define ioerr(db)	((db)->flags |= DBM_IOERR)

#define OFF_PAG(off)	(long) (off) * PBLKSIZ
#define OFF_DIR(off)	(long) (off) * DBLKSIZ

static const long masks[] = {
	000000000000, 000000000001, 000000000003, 000000000007,
	000000000017, 000000000037, 000000000077, 000000000177,
	000000000377, 000000000777, 000000001777, 000000003777,
	000000007777, 000000017777, 000000037777, 000000077777,
	000000177777, 000000377777, 000000777777, 000001777777,
	000003777777, 000007777777, 000017777777, 000037777777,
	000077777777, 000177777777, 000377777777, 000777777777,
	001777777777, 003777777777, 007777777777, 017777777777
};

DBM *
sdbm_open(register char *file, register int flags, register int mode)
{
	SensorCall();register DBM *db;
	register char *dirname;
	register char *pagname;
	size_t filelen;
	const size_t dirfext_size = sizeof(DIRFEXT "");
	const size_t pagfext_size = sizeof(PAGFEXT "");

	SensorCall();if (file == NULL || !*file)
		return errno = EINVAL, (DBM *) NULL;
/*
 * need space for two separate filenames
 */
	SensorCall();filelen = strlen(file);

	SensorCall();if ((dirname = (char *) malloc(filelen + dirfext_size
				       + filelen + pagfext_size)) == NULL)
		return errno = ENOMEM, (DBM *) NULL;
/*
 * build the file names
 */
	SensorCall();memcpy(dirname, file, filelen);
	memcpy(dirname + filelen, DIRFEXT, dirfext_size);
	pagname = dirname + filelen + dirfext_size;
	memcpy(pagname, file, filelen);
	memcpy(pagname + filelen, PAGFEXT, pagfext_size);

	db = sdbm_prep(dirname, pagname, flags, mode);
	free((char *) dirname);
	{DBM * ReplaceReturn = db; SensorCall(); return ReplaceReturn;}
}

DBM *
sdbm_prep(char *dirname, char *pagname, int flags, int mode)
{
	SensorCall();register DBM *db;
	struct stat dstat;

	SensorCall();if ((db = (DBM *) malloc(sizeof(DBM))) == NULL)
		return errno = ENOMEM, (DBM *) NULL;

        SensorCall();db->flags = 0;
        db->hmask = 0;
        db->blkptr = 0;
        db->keyptr = 0;
/*
 * adjust user flags so that WRONLY becomes RDWR, 
 * as required by this package. Also set our internal
 * flag for RDONLY if needed.
 */
	SensorCall();if (flags & O_WRONLY)
		flags = (flags & ~O_WRONLY) | O_RDWR;

	else if ((flags & 03) == O_RDONLY)
		db->flags = DBM_RDONLY;
/*
 * open the files in sequence, and stat the dirfile.
 * If we fail anywhere, undo everything, return NULL.
 */
#if defined(OS2) || defined(MSDOS) || defined(WIN32) || defined(__CYGWIN__)
	flags |= O_BINARY;
#	endif
	SensorCall();if ((db->pagf = open(pagname, flags, mode)) > -1) {
		SensorCall();if ((db->dirf = open(dirname, flags, mode)) > -1) {
/*
 * need the dirfile size to establish max bit number.
 */
			SensorCall();if (fstat(db->dirf, &dstat) == 0) {
/*
 * zero size: either a fresh database, or one with a single,
 * unsplit data page: dirpage is all zeros.
 */
				SensorCall();db->dirbno = (!dstat.st_size) ? 0 : -1;
				db->pagbno = -1;
				db->maxbno = dstat.st_size * BYTESIZ;

				(void) memset(db->pagbuf, 0, PBLKSIZ);
				(void) memset(db->dirbuf, 0, DBLKSIZ);
			/*
			 * success
			 */
				{DBM * ReplaceReturn = db; SensorCall(); return ReplaceReturn;}
			}
			SensorCall();(void) close(db->dirf);
		}
		SensorCall();(void) close(db->pagf);
	}
	SensorCall();free((char *) db);
	{DBM * ReplaceReturn = (DBM *) NULL; SensorCall(); return ReplaceReturn;}
}

void
sdbm_close(register DBM *db)
{
	SensorCall();if (db == NULL)
		errno = EINVAL;
	else {
		SensorCall();(void) close(db->dirf);
		(void) close(db->pagf);
		free((char *) db);
	}
SensorCall();}

datum
sdbm_fetch(register DBM *db, datum key)
{
	SensorCall();if (db == NULL || bad(key))
		{/*1*/{datum  ReplaceReturn = errno = EINVAL, nullitem; SensorCall(); return ReplaceReturn;}/*2*/}

	SensorCall();if (getpage(db, exhash(key)))
		{/*3*/{datum  ReplaceReturn = getpair(db->pagbuf, key); SensorCall(); return ReplaceReturn;}/*4*/}

	{datum  ReplaceReturn = ioerr(db), nullitem; SensorCall(); return ReplaceReturn;}
}

int
sdbm_exists(register DBM *db, datum key)
{
	SensorCall();if (db == NULL || bad(key))
		{/*37*/{int  ReplaceReturn = errno = EINVAL, -1; SensorCall(); return ReplaceReturn;}/*38*/}

	SensorCall();if (getpage(db, exhash(key)))
		{/*39*/{int  ReplaceReturn = exipair(db->pagbuf, key); SensorCall(); return ReplaceReturn;}/*40*/}

	{int  ReplaceReturn = ioerr(db), -1; SensorCall(); return ReplaceReturn;}
}

int
sdbm_delete(register DBM *db, datum key)
{
	SensorCall();if (db == NULL || bad(key))
		{/*5*/{int  ReplaceReturn = errno = EINVAL, -1; SensorCall(); return ReplaceReturn;}/*6*/}
	SensorCall();if (sdbm_rdonly(db))
		{/*7*/{int  ReplaceReturn = errno = EPERM, -1; SensorCall(); return ReplaceReturn;}/*8*/}

	SensorCall();if (getpage(db, exhash(key))) {
		SensorCall();if (!delpair(db->pagbuf, key))
			{/*9*/{int  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}/*10*/}
/*
 * update the page file
 */
		SensorCall();if (lseek(db->pagf, OFF_PAG(db->pagbno), SEEK_SET) < 0
		    || write(db->pagf, db->pagbuf, PBLKSIZ) < 0)
			{/*11*/{int  ReplaceReturn = ioerr(db), -1; SensorCall(); return ReplaceReturn;}/*12*/}

		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}

	{int  ReplaceReturn = ioerr(db), -1; SensorCall(); return ReplaceReturn;}
}

int
sdbm_store(register DBM *db, datum key, datum val, int flags)
{
	SensorCall();int need;
	register long hash;

	SensorCall();if (db == NULL || bad(key))
		{/*13*/{int  ReplaceReturn = errno = EINVAL, -1; SensorCall(); return ReplaceReturn;}/*14*/}
	SensorCall();if (sdbm_rdonly(db))
		{/*15*/{int  ReplaceReturn = errno = EPERM, -1; SensorCall(); return ReplaceReturn;}/*16*/}

	SensorCall();need = key.dsize + val.dsize;
/*
 * is the pair too big (or too small) for this database ??
 */
	SensorCall();if (need < 0 || need > PAIRMAX)
		{/*17*/{int  ReplaceReturn = errno = EINVAL, -1; SensorCall(); return ReplaceReturn;}/*18*/}

	SensorCall();if (getpage(db, (hash = exhash(key)))) {
/*
 * if we need to replace, delete the key/data pair
 * first. If it is not there, ignore.
 */
		SensorCall();if (flags == DBM_REPLACE)
			{/*19*/SensorCall();(void) delpair(db->pagbuf, key);/*20*/}
#ifdef SEEDUPS
		else {/*21*/SensorCall();if (duppair(db->pagbuf, key))
			{/*23*/{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}/*24*/}/*22*/}
#endif
/*
 * if we do not have enough room, we have to split.
 */
		SensorCall();if (!fitpair(db->pagbuf, need))
			{/*25*/SensorCall();if (!makroom(db, hash, need))
				{/*27*/{int  ReplaceReturn = ioerr(db), -1; SensorCall(); return ReplaceReturn;}/*28*/}/*26*/}
/*
 * we have enough room or split is successful. insert the key,
 * and update the page file.
 */
		SensorCall();(void) putpair(db->pagbuf, key, val);

		SensorCall();if (lseek(db->pagf, OFF_PAG(db->pagbno), SEEK_SET) < 0
		    || write(db->pagf, db->pagbuf, PBLKSIZ) < 0)
			{/*29*/{int  ReplaceReturn = ioerr(db), -1; SensorCall(); return ReplaceReturn;}/*30*/}
	/*
	 * success
	 */
		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}

	{int  ReplaceReturn = ioerr(db), -1; SensorCall(); return ReplaceReturn;}
}

/*
 * makroom - make room by splitting the overfull page
 * this routine will attempt to make room for SPLTMAX times before
 * giving up.
 */
static int
makroom(register DBM *db, long int hash, int need)
{
	SensorCall();long newp;
	char twin[PBLKSIZ];
#if defined(DOSISH) || defined(WIN32)
	char zer[PBLKSIZ];
	long oldtail;
#endif
	char *pag = db->pagbuf;
	char *New = twin;
	register int smax = SPLTMAX;

	SensorCall();do {
/*
 * split the current page
 */
		SensorCall();(void) splpage(pag, New, db->hmask + 1);
/*
 * address of the new page
 */
		newp = (hash & db->hmask) | (db->hmask + 1);

/*
 * write delay, read avoidance/cache shuffle:
 * select the page for incoming pair: if key is to go to the new page,
 * write out the previous one, and copy the new one over, thus making
 * it the current page. If not, simply write the new page, and we are
 * still looking at the page of interest. current page is not updated
 * here, as sdbm_store will do so, after it inserts the incoming pair.
 */

#if defined(DOSISH) || defined(WIN32)
		/*
		 * Fill hole with 0 if made it.
		 * (hole is NOT read as 0)
		 */
		oldtail = lseek(db->pagf, 0L, SEEK_END);
		memset(zer, 0, PBLKSIZ);
		while (OFF_PAG(newp) > oldtail) {
			if (lseek(db->pagf, 0L, SEEK_END) < 0 ||
			    write(db->pagf, zer, PBLKSIZ) < 0) {

				return 0;
			}
			oldtail += PBLKSIZ;
		}
#endif
		SensorCall();if (hash & (db->hmask + 1)) {
			SensorCall();if (lseek(db->pagf, OFF_PAG(db->pagbno), SEEK_SET) < 0
			    || write(db->pagf, db->pagbuf, PBLKSIZ) < 0)
				{/*67*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*68*/}
			SensorCall();db->pagbno = newp;
			(void) memcpy(pag, New, PBLKSIZ);
		}
		else {/*69*/SensorCall();if (lseek(db->pagf, OFF_PAG(newp), SEEK_SET) < 0
			 || write(db->pagf, New, PBLKSIZ) < 0)
			{/*71*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*72*/}/*70*/}

		SensorCall();if (!setdbit(db, db->curbit))
			{/*73*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*74*/}
/*
 * see if we have enough room now
 */
		SensorCall();if (fitpair(pag, need))
			{/*75*/{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}/*76*/}
/*
 * try again... update curbit and hmask as getpage would have
 * done. because of our update of the current page, we do not
 * need to read in anything. BUT we have to write the current
 * [deferred] page out, as the window of failure is too great.
 */
		SensorCall();db->curbit = 2 * db->curbit +
			((hash & (db->hmask + 1)) ? 2 : 1);
		db->hmask |= db->hmask + 1;

		SensorCall();if (lseek(db->pagf, OFF_PAG(db->pagbno), SEEK_SET) < 0
		    || write(db->pagf, db->pagbuf, PBLKSIZ) < 0)
			{/*77*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*78*/}

	} while (--smax);
/*
 * if we are here, this is real bad news. After SPLTMAX splits,
 * we still cannot fit the key. say goodnight.
 */
#ifdef BADMESS
	SensorCall();(void) write(2, "sdbm: cannot insert after SPLTMAX attempts.\n", 44);
#endif
	{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}

}

/*
 * the following two routines will break if
 * deletions aren't taken into account. (ndbm bug)
 */
datum
sdbm_firstkey(register DBM *db)
{
	SensorCall();if (db == NULL)
		{/*31*/{datum  ReplaceReturn = errno = EINVAL, nullitem; SensorCall(); return ReplaceReturn;}/*32*/}
/*
 * start at page 0
 */
	SensorCall();if (lseek(db->pagf, OFF_PAG(0), SEEK_SET) < 0
	    || read(db->pagf, db->pagbuf, PBLKSIZ) < 0)
		{/*33*/{datum  ReplaceReturn = ioerr(db), nullitem; SensorCall(); return ReplaceReturn;}/*34*/}
	SensorCall();db->pagbno = 0;
	db->blkptr = 0;
	db->keyptr = 0;

	{datum  ReplaceReturn = getnext(db); SensorCall(); return ReplaceReturn;}
}

datum
sdbm_nextkey(register DBM *db)
{
	SensorCall();if (db == NULL)
		{/*35*/{datum  ReplaceReturn = errno = EINVAL, nullitem; SensorCall(); return ReplaceReturn;}/*36*/}
	{datum  ReplaceReturn = getnext(db); SensorCall(); return ReplaceReturn;}
}

/*
 * all important binary trie traversal
 */
static int
getpage(register DBM *db, register long int hash)
{
	SensorCall();register int hbit;
	register long dbit;
	register long pagb;

	dbit = 0;
	hbit = 0;
	SensorCall();while (dbit < db->maxbno && getdbit(db, dbit))
		{/*51*/SensorCall();dbit = 2 * dbit + ((hash & (1 << hbit++)) ? 2 : 1);/*52*/}

	debug(("dbit: %d...", dbit));

	SensorCall();db->curbit = dbit;
	db->hmask = masks[hbit];

	pagb = hash & db->hmask;
/*
 * see if the block we need is already in memory.
 * note: this lookaside cache has about 10% hit rate.
 */
	SensorCall();if (pagb != db->pagbno) { 
/*
 * note: here, we assume a "hole" is read as 0s.
 * if not, must zero pagbuf first.
 */
		SensorCall();if (lseek(db->pagf, OFF_PAG(pagb), SEEK_SET) < 0
		    || read(db->pagf, db->pagbuf, PBLKSIZ) < 0)
			{/*53*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*54*/}
		SensorCall();if (!chkpage(db->pagbuf))
			{/*55*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*56*/}
		SensorCall();db->pagbno = pagb;

		debug(("pag read: %d\n", pagb));
	}
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

static int
getdbit(register DBM *db, register long int dbit)
{
	SensorCall();register long c;
	register long dirb;

	c = dbit / BYTESIZ;
	dirb = c / DBLKSIZ;

	SensorCall();if (dirb != db->dirbno) {
		SensorCall();int got;
		SensorCall();if (lseek(db->dirf, OFF_DIR(dirb), SEEK_SET) < 0
		    || (got=read(db->dirf, db->dirbuf, DBLKSIZ)) < 0)
			{/*41*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*42*/}
		SensorCall();if (got==0) 
			{/*43*/SensorCall();memset(db->dirbuf,0,DBLKSIZ);/*44*/}
		SensorCall();db->dirbno = dirb;

		debug(("dir read: %d\n", dirb));
	}

	{int  ReplaceReturn = db->dirbuf[c % DBLKSIZ] & (1 << dbit % BYTESIZ); SensorCall(); return ReplaceReturn;}
}

static int
setdbit(register DBM *db, register long int dbit)
{
	SensorCall();register long c;
	register long dirb;

	c = dbit / BYTESIZ;
	dirb = c / DBLKSIZ;

	SensorCall();if (dirb != db->dirbno) {
		SensorCall();int got;
		SensorCall();if (lseek(db->dirf, OFF_DIR(dirb), SEEK_SET) < 0
		    || (got=read(db->dirf, db->dirbuf, DBLKSIZ)) < 0)
			{/*45*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*46*/}
		SensorCall();if (got==0) 
			{/*47*/SensorCall();memset(db->dirbuf,0,DBLKSIZ);/*48*/}
		SensorCall();db->dirbno = dirb;

		debug(("dir read: %d\n", dirb));
	}

	SensorCall();db->dirbuf[c % DBLKSIZ] |= (1 << dbit % BYTESIZ);

#if 0
	if (dbit >= db->maxbno)
		db->maxbno += DBLKSIZ * BYTESIZ;
#else
	SensorCall();if (OFF_DIR((dirb+1))*BYTESIZ > db->maxbno) 
		db->maxbno=OFF_DIR((dirb+1))*BYTESIZ;
#endif

	SensorCall();if (lseek(db->dirf, OFF_DIR(dirb), SEEK_SET) < 0
	    || write(db->dirf, db->dirbuf, DBLKSIZ) < 0)
		{/*49*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*50*/}

	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

/*
 * getnext - get the next key in the page, and if done with
 * the page, try the next page in sequence
 */
static datum
getnext(register DBM *db)
{
	SensorCall();datum key;

	SensorCall();for (;;) {
		SensorCall();db->keyptr++;
		key = getnkey(db->pagbuf, db->keyptr);
		SensorCall();if (key.dptr != NULL)
			{/*57*/{datum  ReplaceReturn = key; SensorCall(); return ReplaceReturn;}/*58*/}
/*
 * we either run out, or there is nothing on this page..
 * try the next one... If we lost our position on the
 * file, we will have to seek.
 */
		SensorCall();db->keyptr = 0;
		SensorCall();if (db->pagbno != db->blkptr++)
			{/*59*/SensorCall();if (lseek(db->pagf, OFF_PAG(db->blkptr), SEEK_SET) < 0)
				{/*61*/SensorCall();break;/*62*/}/*60*/}
		SensorCall();db->pagbno = db->blkptr;
		SensorCall();if (read(db->pagf, db->pagbuf, PBLKSIZ) <= 0)
			{/*63*/SensorCall();break;/*64*/}
		SensorCall();if (!chkpage(db->pagbuf))
			{/*65*/SensorCall();break;/*66*/}
	}

	{datum  ReplaceReturn = ioerr(db), nullitem; SensorCall(); return ReplaceReturn;}
}

