#include "var/tmp/sensor.h"
/*    doop.c
 *
 *    Copyright (C) 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000,
 *    2001, 2002, 2004, 2005, 2006, 2007, 2008, 2009 by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 *
 */

/*
 *  'So that was the job I felt I had to do when I started,' thought Sam.
 *
 *     [p.934 of _The Lord of the Rings_, VI/iii: "Mount Doom"]
 */

/* This file contains some common functions needed to carry out certain
 * ops. For example both pp_schomp() and pp_chomp() - scalar and array
 * chomp operations - call the function do_chomp() found in this file.
 */

#include "EXTERN.h"
#define PERL_IN_DOOP_C
#include "perl.h"

#ifndef PERL_MICRO
#include <signal.h>
#endif

STATIC I32
S_do_trans_simple(pTHX_ SV * const sv)
{
SensorCall();    dVAR;
    I32 matches = 0;
    STRLEN len;
    U8 *s = (U8*)SvPV_nomg(sv,len);
    U8 * const send = s+len;
    const short * const tbl = (short*)cPVOP->op_pv;

    PERL_ARGS_ASSERT_DO_TRANS_SIMPLE;

    SensorCall();if (!tbl)
	{/*185*/SensorCall();Perl_croak(aTHX_ "panic: do_trans_simple line %d",__LINE__);/*186*/}

    /* First, take care of non-UTF-8 input strings, because they're easy */
    SensorCall();if (!SvUTF8(sv)) {
	SensorCall();while (s < send) {
	    SensorCall();const I32 ch = tbl[*s];
	    SensorCall();if (ch >= 0) {
		SensorCall();matches++;
		*s = (U8)ch;
	    }
	    SensorCall();s++;
	}
	SvSETMAGIC(sv);
    }
    else {
	SensorCall();const I32 grows = PL_op->op_private & OPpTRANS_GROWS;
	U8 *d;
	U8 *dstart;

	/* Allow for expansion: $_="a".chr(400); tr/a/\xFE/, FE needs encoding */
	SensorCall();if (grows)
	    Newx(d, len*2+1, U8);
	else
	    {/*187*/SensorCall();d = s;/*188*/}
	SensorCall();dstart = d;
	SensorCall();while (s < send) {
	    SensorCall();STRLEN ulen;
	    I32 ch;

	    /* Need to check this, otherwise 128..255 won't match */
	    const UV c = utf8n_to_uvchr(s, send - s, &ulen, UTF8_ALLOW_DEFAULT);
	    SensorCall();if (c < 0x100 && (ch = tbl[c]) >= 0) {
		SensorCall();matches++;
		d = uvchr_to_utf8(d, ch);
		s += ulen;
	    }
	    else { /* No match -> copy */
		Move(s, d, ulen, U8);
		SensorCall();d += ulen;
		s += ulen;
	    }
	}
	SensorCall();if (grows) {
	    sv_setpvn(sv, (char*)dstart, d - dstart);
	    Safefree(dstart);
	}
	else {
	    SensorCall();*d = '\0';
	    SvCUR_set(sv, d - dstart);
	}
	SvUTF8_on(sv);
	SvSETMAGIC(sv);
    }
    {I32  ReplaceReturn = matches; SensorCall(); return ReplaceReturn;}
}

STATIC I32
S_do_trans_count(pTHX_ SV * const sv)
{
SensorCall();    dVAR;
    STRLEN len;
    const U8 *s = (const U8*)SvPV_nomg_const(sv, len);
    const U8 * const send = s + len;
    I32 matches = 0;
    const short * const tbl = (short*)cPVOP->op_pv;

    PERL_ARGS_ASSERT_DO_TRANS_COUNT;

    SensorCall();if (!tbl)
	{/*173*/SensorCall();Perl_croak(aTHX_ "panic: do_trans_count line %d",__LINE__);/*174*/}

    SensorCall();if (!SvUTF8(sv)) {
	SensorCall();while (s < send) {
            SensorCall();if (tbl[*s++] >= 0)
                {/*175*/SensorCall();matches++;/*176*/}
	}
    }
    else {
	SensorCall();const I32 complement = PL_op->op_private & OPpTRANS_COMPLEMENT;
	SensorCall();while (s < send) {
	    SensorCall();STRLEN ulen;
	    const UV c = utf8n_to_uvchr(s, send - s, &ulen, UTF8_ALLOW_DEFAULT);
	    SensorCall();if (c < 0x100) {
		SensorCall();if (tbl[c] >= 0)
		    {/*177*/SensorCall();matches++;/*178*/}
	    } else {/*179*/SensorCall();if (complement)
		{/*181*/SensorCall();matches++;/*182*/}/*180*/}
	    SensorCall();s += ulen;
	}
    }

    {I32  ReplaceReturn = matches; SensorCall(); return ReplaceReturn;}
}

STATIC I32
S_do_trans_complex(pTHX_ SV * const sv)
{
SensorCall();    dVAR;
    STRLEN len;
    U8 *s = (U8*)SvPV_nomg(sv, len);
    U8 * const send = s+len;
    I32 matches = 0;
    const short * const tbl = (short*)cPVOP->op_pv;

    PERL_ARGS_ASSERT_DO_TRANS_COMPLEX;

    SensorCall();if (!tbl)
	{/*121*/SensorCall();Perl_croak(aTHX_ "panic: do_trans_complex line %d",__LINE__);/*122*/}

    SensorCall();if (!SvUTF8(sv)) {
	SensorCall();U8 *d = s;
	U8 * const dstart = d;

	SensorCall();if (PL_op->op_private & OPpTRANS_SQUASH) {
	    SensorCall();const U8* p = send;
	    SensorCall();while (s < send) {
		SensorCall();const I32 ch = tbl[*s];
		SensorCall();if (ch >= 0) {
		    SensorCall();*d = (U8)ch;
		    matches++;
		    SensorCall();if (p != d - 1 || *p != *d)
			{/*123*/SensorCall();p = d++;/*124*/}
		}
		else {/*125*/SensorCall();if (ch == -1)	/* -1 is unmapped character */
		    {/*127*/SensorCall();*d++ = *s;/*128*/}	
		else {/*129*/SensorCall();if (ch == -2)	/* -2 is delete character */
		    {/*131*/SensorCall();matches++;/*132*/}/*130*/}/*126*/}
		SensorCall();s++;
	    }
	}
	else {
	    SensorCall();while (s < send) {
		SensorCall();const I32 ch = tbl[*s];
		SensorCall();if (ch >= 0) {
		    SensorCall();matches++;
		    *d++ = (U8)ch;
		}
		else {/*133*/SensorCall();if (ch == -1)	/* -1 is unmapped character */
		    {/*135*/SensorCall();*d++ = *s;/*136*/}
		else {/*137*/SensorCall();if (ch == -2)      /* -2 is delete character */
		    {/*139*/SensorCall();matches++;/*140*/}/*138*/}/*134*/}
		SensorCall();s++;
	    }
	}
	SensorCall();*d = '\0';
	SvCUR_set(sv, d - dstart);
    }
    else { /* is utf8 */
	SensorCall();const I32 complement = PL_op->op_private & OPpTRANS_COMPLEMENT;
	const I32 grows = PL_op->op_private & OPpTRANS_GROWS;
	const I32 del = PL_op->op_private & OPpTRANS_DELETE;
	U8 *d;
	U8 *dstart;
	STRLEN rlen = 0;

	SensorCall();if (grows)
	    Newx(d, len*2+1, U8);
	else
	    {/*141*/SensorCall();d = s;/*142*/}
	SensorCall();dstart = d;
	SensorCall();if (complement && !del)
	    {/*143*/SensorCall();rlen = tbl[0x100];/*144*/}

	SensorCall();if (PL_op->op_private & OPpTRANS_SQUASH) {
	    SensorCall();UV pch = 0xfeedface;
	    SensorCall();while (s < send) {
		SensorCall();STRLEN len;
		const UV comp = utf8n_to_uvchr(s, send - s, &len,
					       UTF8_ALLOW_DEFAULT);
		I32 ch;

		SensorCall();if (comp > 0xff) {
		    SensorCall();if (!complement) {
			Move(s, d, len, U8);
			SensorCall();d += len;
		    }
		    else {
			SensorCall();matches++;
			SensorCall();if (!del) {
			    SensorCall();ch = (rlen == 0) ? (I32)comp :
				(comp - 0x100 < rlen) ?
				tbl[comp+1] : tbl[0x100+rlen];
			    SensorCall();if ((UV)ch != pch) {
				SensorCall();d = uvchr_to_utf8(d, ch);
				pch = (UV)ch;
			    }
			    SensorCall();s += len;
			    SensorCall();continue;
			}
		    }
		}
		else {/*145*/SensorCall();if ((ch = tbl[comp]) >= 0) {
		    SensorCall();matches++;
		    SensorCall();if ((UV)ch != pch) {
		        SensorCall();d = uvchr_to_utf8(d, ch);
		        pch = (UV)ch;
		    }
		    SensorCall();s += len;
		    SensorCall();continue;
		}
		else {/*147*/SensorCall();if (ch == -1) {	/* -1 is unmapped character */
		    Move(s, d, len, U8);
		    SensorCall();d += len;
		}
		else {/*149*/SensorCall();if (ch == -2)      /* -2 is delete character */
		    {/*151*/SensorCall();matches++;/*152*/}/*150*/}/*148*/}/*146*/}
		SensorCall();s += len;
		pch = 0xfeedface;
	    }
	}
	else {
	    SensorCall();while (s < send) {
		SensorCall();STRLEN len;
		const UV comp = utf8n_to_uvchr(s, send - s, &len,
					       UTF8_ALLOW_DEFAULT);
		I32 ch;
		SensorCall();if (comp > 0xff) {
		    SensorCall();if (!complement) {
			Move(s, d, len, U8);
			SensorCall();d += len;
		    }
		    else {
			SensorCall();matches++;
			SensorCall();if (!del) {
			    SensorCall();if (comp - 0x100 < rlen)
				d = uvchr_to_utf8(d, tbl[comp+1]);
			    else
				d = uvchr_to_utf8(d, tbl[0x100+rlen]);
			}
		    }
		}
		else {/*153*/SensorCall();if ((ch = tbl[comp]) >= 0) {
		    SensorCall();d = uvchr_to_utf8(d, ch);
		    matches++;
		}
		else {/*155*/SensorCall();if (ch == -1) {	/* -1 is unmapped character */
		    Move(s, d, len, U8);
		    SensorCall();d += len;
		}
		else {/*157*/SensorCall();if (ch == -2)      /* -2 is delete character */
		    {/*159*/SensorCall();matches++;/*160*/}/*158*/}/*156*/}/*154*/}
		SensorCall();s += len;
	    }
	}
	SensorCall();if (grows) {
	    sv_setpvn(sv, (char*)dstart, d - dstart);
	    Safefree(dstart);
	}
	else {
	    SensorCall();*d = '\0';
	    SvCUR_set(sv, d - dstart);
	}
	SvUTF8_on(sv);
    }
    SvSETMAGIC(sv);
    {I32  ReplaceReturn = matches; SensorCall(); return ReplaceReturn;}
}

STATIC I32
S_do_trans_simple_utf8(pTHX_ SV * const sv)
{
SensorCall();    dVAR;
    U8 *s;
    U8 *send;
    U8 *d;
    U8 *start;
    U8 *dstart, *dend;
    I32 matches = 0;
    const I32 grows = PL_op->op_private & OPpTRANS_GROWS;
    STRLEN len;
    SV* const  rv =
#ifdef USE_ITHREADS
		    PAD_SVl(cPADOP->op_padix);
#else
		    MUTABLE_SV(cSVOP->op_sv);
#endif
    HV* const  hv = MUTABLE_HV(SvRV(rv));
    SV* const * svp = hv_fetchs(hv, "NONE", FALSE);
    const UV none = svp ? SvUV(*svp) : 0x7fffffff;
    const UV extra = none + 1;
    UV final = 0;
    U8 hibit = 0;

    PERL_ARGS_ASSERT_DO_TRANS_SIMPLE_UTF8;

    s = (U8*)SvPV_nomg(sv, len);
    SensorCall();if (!SvUTF8(sv)) {
	SensorCall();const U8 *t = s;
	const U8 * const e = s + len;
	SensorCall();while (t < e) {
	    SensorCall();const U8 ch = *t++;
	    hibit = !NATIVE_IS_INVARIANT(ch);
	    SensorCall();if (hibit) {
		SensorCall();s = bytes_to_utf8(s, &len);
		SensorCall();break;
	    }
	}
    }
    SensorCall();send = s + len;
    start = s;

    svp = hv_fetchs(hv, "FINAL", FALSE);
    SensorCall();if (svp)
	final = SvUV(*svp);

    SensorCall();if (grows) {
	/* d needs to be bigger than s, in case e.g. upgrading is required */
	Newx(d, len * 3 + UTF8_MAXBYTES, U8);
	SensorCall();dend = d + len * 3;
	dstart = d;
    }
    else {
	SensorCall();dstart = d = s;
	dend = d + len;
    }

    SensorCall();while (s < send) {
	SensorCall();const UV uv = swash_fetch(rv, s, TRUE);
	SensorCall();if (uv < none) {
	    SensorCall();s += UTF8SKIP(s);
	    matches++;
	    d = uvuni_to_utf8(d, uv);
	}
	else if (uv == none) {
	    const int i = UTF8SKIP(s);
	    Move(s, d, i, U8);
	    d += i;
	    s += i;
	}
	else if (uv == extra) {
	    s += UTF8SKIP(s);
	    matches++;
	    d = uvuni_to_utf8(d, final);
	}
	else
	    s += UTF8SKIP(s);

	SensorCall();if (d > dend) {
	    SensorCall();const STRLEN clen = d - dstart;
	    const STRLEN nlen = dend - dstart + len + UTF8_MAXBYTES;
	    SensorCall();if (!grows)
		{/*189*/SensorCall();Perl_croak(aTHX_ "panic: do_trans_simple_utf8 line %d",__LINE__);/*190*/}
	    Renew(dstart, nlen + UTF8_MAXBYTES, U8);
	    SensorCall();d = dstart + clen;
	    dend = dstart + nlen;
	}
    }
    SensorCall();if (grows || hibit) {
	sv_setpvn(sv, (char*)dstart, d - dstart);
	Safefree(dstart);
	SensorCall();if (grows && hibit)
	    Safefree(start);
    }
    else {
	SensorCall();*d = '\0';
	SvCUR_set(sv, d - dstart);
    }
    SvSETMAGIC(sv);
    SvUTF8_on(sv);

    {I32  ReplaceReturn = matches; SensorCall(); return ReplaceReturn;}
}

STATIC I32
S_do_trans_count_utf8(pTHX_ SV * const sv)
{
SensorCall();    dVAR;
    const U8 *s;
    const U8 *start = NULL;
    const U8 *send;
    I32 matches = 0;
    STRLEN len;
    SV* const  rv =
#ifdef USE_ITHREADS
		    PAD_SVl(cPADOP->op_padix);
#else
		    MUTABLE_SV(cSVOP->op_sv);
#endif
    HV* const hv = MUTABLE_HV(SvRV(rv));
    SV* const * const svp = hv_fetchs(hv, "NONE", FALSE);
    const UV none = svp ? SvUV(*svp) : 0x7fffffff;
    const UV extra = none + 1;
    U8 hibit = 0;

    PERL_ARGS_ASSERT_DO_TRANS_COUNT_UTF8;

    s = (const U8*)SvPV_nomg_const(sv, len);
    SensorCall();if (!SvUTF8(sv)) {
	SensorCall();const U8 *t = s;
	const U8 * const e = s + len;
	SensorCall();while (t < e) {
	    SensorCall();const U8 ch = *t++;
	    hibit = !NATIVE_IS_INVARIANT(ch);
	    SensorCall();if (hibit) {
		SensorCall();start = s = bytes_to_utf8(s, &len);
		SensorCall();break;
	    }
	}
    }
    SensorCall();send = s + len;

    SensorCall();while (s < send) {
	SensorCall();const UV uv = swash_fetch(rv, s, TRUE);
	SensorCall();if (uv < none || uv == extra)
	    {/*183*/SensorCall();matches++;/*184*/}
	SensorCall();s += UTF8SKIP(s);
    }
    SensorCall();if (hibit)
        Safefree(start);

    {I32  ReplaceReturn = matches; SensorCall(); return ReplaceReturn;}
}

STATIC I32
S_do_trans_complex_utf8(pTHX_ SV * const sv)
{
SensorCall();    dVAR;
    U8 *start, *send;
    U8 *d;
    I32 matches = 0;
    const I32 squash   = PL_op->op_private & OPpTRANS_SQUASH;
    const I32 del      = PL_op->op_private & OPpTRANS_DELETE;
    const I32 grows    = PL_op->op_private & OPpTRANS_GROWS;
    SV* const  rv =
#ifdef USE_ITHREADS
		    PAD_SVl(cPADOP->op_padix);
#else
		    MUTABLE_SV(cSVOP->op_sv);
#endif
    HV * const hv = MUTABLE_HV(SvRV(rv));
    SV * const *svp = hv_fetchs(hv, "NONE", FALSE);
    const UV none = svp ? SvUV(*svp) : 0x7fffffff;
    const UV extra = none + 1;
    UV final = 0;
    bool havefinal = FALSE;
    STRLEN len;
    U8 *dstart, *dend;
    U8 hibit = 0;
    U8 *s = (U8*)SvPV_nomg(sv, len);

    PERL_ARGS_ASSERT_DO_TRANS_COMPLEX_UTF8;

    SensorCall();if (!SvUTF8(sv)) {
	SensorCall();const U8 *t = s;
	const U8 * const e = s + len;
	SensorCall();while (t < e) {
	    SensorCall();const U8 ch = *t++;
	    hibit = !NATIVE_IS_INVARIANT(ch);
	    SensorCall();if (hibit) {
		SensorCall();s = bytes_to_utf8(s, &len);
		SensorCall();break;
	    }
	}
    }
    SensorCall();send = s + len;
    start = s;

    svp = hv_fetchs(hv, "FINAL", FALSE);
    SensorCall();if (svp) {
	SensorCall();final = SvUV(*svp);
	havefinal = TRUE;
    }

    SensorCall();if (grows) {
	/* d needs to be bigger than s, in case e.g. upgrading is required */
	Newx(d, len * 3 + UTF8_MAXBYTES, U8);
	SensorCall();dend = d + len * 3;
	dstart = d;
    }
    else {
	SensorCall();dstart = d = s;
	dend = d + len;
    }

    SensorCall();if (squash) {
	SensorCall();UV puv = 0xfeedface;
	SensorCall();while (s < send) {
	    SensorCall();UV uv = swash_fetch(rv, s, TRUE);
	
	    SensorCall();if (d > dend) {
		SensorCall();const STRLEN clen = d - dstart;
		const STRLEN nlen = dend - dstart + len + UTF8_MAXBYTES;
		SensorCall();if (!grows)
		    {/*161*/SensorCall();Perl_croak(aTHX_ "panic: do_trans_complex_utf8 line %d",__LINE__);/*162*/}
		Renew(dstart, nlen + UTF8_MAXBYTES, U8);
		SensorCall();d = dstart + clen;
		dend = dstart + nlen;
	    }
	    SensorCall();if (uv < none) {
		SensorCall();matches++;
		s += UTF8SKIP(s);
		SensorCall();if (uv != puv) {
		    SensorCall();d = uvuni_to_utf8(d, uv);
		    puv = uv;
		}
		SensorCall();continue;
	    }
	    else {/*163*/SensorCall();if (uv == none) {	/* "none" is unmapped character */
		SensorCall();const int i = UTF8SKIP(s);
		Move(s, d, i, U8);
		d += i;
		s += i;
		puv = 0xfeedface;
		SensorCall();continue;
	    }
	    else {/*165*/SensorCall();if (uv == extra && !del) {
		SensorCall();matches++;
		SensorCall();if (havefinal) {
		    SensorCall();s += UTF8SKIP(s);
		    SensorCall();if (puv != final) {
			SensorCall();d = uvuni_to_utf8(d, final);
			puv = final;
		    }
		}
		else {
		    SensorCall();STRLEN len;
		    uv = utf8n_to_uvuni(s, send - s, &len, UTF8_ALLOW_DEFAULT);
		    SensorCall();if (uv != puv) {
			Move(s, d, len, U8);
			SensorCall();d += len;
			puv = uv;
		    }
		    SensorCall();s += len;
		}
		SensorCall();continue;
	    ;/*166*/}/*164*/}}
	    SensorCall();matches++;			/* "none+1" is delete character */
	    s += UTF8SKIP(s);
	}
    }
    else {
	SensorCall();while (s < send) {
	    SensorCall();const UV uv = swash_fetch(rv, s, TRUE);
	    SensorCall();if (d > dend) {
	        SensorCall();const STRLEN clen = d - dstart;
		const STRLEN nlen = dend - dstart + len + UTF8_MAXBYTES;
		SensorCall();if (!grows)
		    {/*167*/SensorCall();Perl_croak(aTHX_ "panic: do_trans_complex_utf8 line %d",__LINE__);/*168*/}
		Renew(dstart, nlen + UTF8_MAXBYTES, U8);
		SensorCall();d = dstart + clen;
		dend = dstart + nlen;
	    }
	    SensorCall();if (uv < none) {
		SensorCall();matches++;
		s += UTF8SKIP(s);
		d = uvuni_to_utf8(d, uv);
		SensorCall();continue;
	    }
	    else {/*169*/SensorCall();if (uv == none) {	/* "none" is unmapped character */
		SensorCall();const int i = UTF8SKIP(s);
		Move(s, d, i, U8);
		d += i;
		s += i;
		SensorCall();continue;
	    }
	    else {/*171*/SensorCall();if (uv == extra && !del) {
		SensorCall();matches++;
		s += UTF8SKIP(s);
		d = uvuni_to_utf8(d, final);
		SensorCall();continue;
	    ;/*172*/}/*170*/}}
	    SensorCall();matches++;			/* "none+1" is delete character */
	    s += UTF8SKIP(s);
	}
    }
    SensorCall();if (grows || hibit) {
	sv_setpvn(sv, (char*)dstart, d - dstart);
	Safefree(dstart);
	SensorCall();if (grows && hibit)
	    Safefree(start);
    }
    else {
	SensorCall();*d = '\0';
	SvCUR_set(sv, d - dstart);
    }
    SvUTF8_on(sv);
    SvSETMAGIC(sv);

    {I32  ReplaceReturn = matches; SensorCall(); return ReplaceReturn;}
}

I32
Perl_do_trans(pTHX_ SV *sv)
{
SensorCall();    dVAR;
    STRLEN len;
    const I32 hasutf = (PL_op->op_private &
                    (OPpTRANS_FROM_UTF|OPpTRANS_TO_UTF));

    PERL_ARGS_ASSERT_DO_TRANS;

    SensorCall();if (SvREADONLY(sv) && !(PL_op->op_private & OPpTRANS_IDENTICAL)) {
        SensorCall();if (SvIsCOW(sv))
            sv_force_normal_flags(sv, 0);
        SensorCall();if (SvREADONLY(sv))
            {/*1*/SensorCall();Perl_croak_no_modify(aTHX);/*2*/}
    }
    SensorCall();(void)SvPV_const(sv, len);
    SensorCall();if (!len)
	{/*3*/{I32  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*4*/}
    SensorCall();if (!(PL_op->op_private & OPpTRANS_IDENTICAL)) {
	SensorCall();if (!SvPOKp(sv))
	    (void)SvPV_force_nomg(sv, len);
	SensorCall();(void)SvPOK_only_UTF8(sv);
    }

    DEBUG_t( Perl_deb(aTHX_ "2.TBL\n"));

    SensorCall();switch (PL_op->op_private & ~hasutf & (
		OPpTRANS_FROM_UTF|OPpTRANS_TO_UTF|OPpTRANS_IDENTICAL|
		OPpTRANS_SQUASH|OPpTRANS_DELETE|OPpTRANS_COMPLEMENT)) {
    case 0:
	SensorCall();if (hasutf)
	    return do_trans_simple_utf8(sv);
	else
	    return do_trans_simple(sv);

    case OPpTRANS_IDENTICAL:
    case OPpTRANS_IDENTICAL|OPpTRANS_COMPLEMENT:
	SensorCall();if (hasutf)
	    return do_trans_count_utf8(sv);
	else
	    return do_trans_count(sv);

    default:
	SensorCall();if (hasutf)
	    return do_trans_complex_utf8(sv);
	else
	    return do_trans_complex(sv);
    }
SensorCall();}

void
Perl_do_join(pTHX_ register SV *sv, SV *delim, register SV **mark, register SV **sp)
{
SensorCall();    dVAR;
    SV ** const oldmark = mark;
    register I32 items = sp - mark;
    register STRLEN len;
    STRLEN delimlen;

    PERL_ARGS_ASSERT_DO_JOIN;

    (void) SvPV_const(delim, delimlen); /* stringify and get the delimlen */
    /* SvCUR assumes it's SvPOK() and woe betide you if it's not. */

    mark++;
    len = (items > 0 ? (delimlen * (items - 1) ) : 0);
    SvUPGRADE(sv, SVt_PV);
    SensorCall();if (SvLEN(sv) < len + items) {	/* current length is way too short */
	SensorCall();while (items-- > 0) {
	    SensorCall();if (*mark && !SvGAMAGIC(*mark) && SvOK(*mark)) {
		SensorCall();STRLEN tmplen;
		SvPV_const(*mark, tmplen);
		len += tmplen;
	    }
	    SensorCall();mark++;
	}
	SvGROW(sv, len + 1);		/* so try to pre-extend */

	SensorCall();mark = oldmark;
	items = sp - mark;
	++mark;
    }

    sv_setpvs(sv, "");
    /* sv_setpv retains old UTF8ness [perl #24846] */
    SvUTF8_off(sv);

    SensorCall();if (PL_tainting && SvMAGICAL(sv))
	SvTAINTED_off(sv);

    SensorCall();if (items-- > 0) {
	SensorCall();if (*mark)
	    sv_catsv(sv, *mark);
	SensorCall();mark++;
    }

    SensorCall();if (delimlen) {
	SensorCall();for (; items > 0; items--,mark++) {
	    sv_catsv(sv,delim);
	    sv_catsv(sv,*mark);
	}
    }
    else {
	SensorCall();for (; items > 0; items--,mark++)
	    sv_catsv(sv,*mark);
    }
    SvSETMAGIC(sv);
}

void
Perl_do_sprintf(pTHX_ SV *sv, I32 len, SV **sarg)
{
SensorCall();    dVAR;
    STRLEN patlen;
    const char * const pat = SvPV_const(*sarg, patlen);
    bool do_taint = FALSE;

    PERL_ARGS_ASSERT_DO_SPRINTF;

    SensorCall();if (SvTAINTED(*sarg))
	TAINT_PROPER(
		(PL_op && PL_op->op_type < OP_max)
		    ? (PL_op->op_type == OP_PRTF)
			? "printf"
			: PL_op_name[PL_op->op_type]
		    : "(unknown)"
	);
    SvUTF8_off(sv);
    SensorCall();if (DO_UTF8(*sarg))
        SvUTF8_on(sv);
    sv_vsetpvfn(sv, pat, patlen, NULL, sarg + 1, len - 1, &do_taint);
    SvSETMAGIC(sv);
    SensorCall();if (do_taint)
	SvTAINTED_on(sv);
SensorCall();}

/* currently converts input to bytes if possible, but doesn't sweat failure */
UV
Perl_do_vecget(pTHX_ SV *sv, I32 offset, I32 size)
{
SensorCall();    dVAR;
    STRLEN srclen, len, uoffset, bitoffs = 0;
    const unsigned char *s = (const unsigned char *) SvPV_const(sv, srclen);
    UV retnum = 0;

    PERL_ARGS_ASSERT_DO_VECGET;

    SensorCall();if (offset < 0)
	{/*5*/{UV  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*6*/}
    SensorCall();if (size < 1 || (size & (size-1))) /* size < 1 or not a power of two */
	{/*7*/SensorCall();Perl_croak(aTHX_ "Illegal number of bits in vec");/*8*/}

    SensorCall();if (SvUTF8(sv))
	{/*9*/SensorCall();(void) Perl_sv_utf8_downgrade(aTHX_ sv, TRUE);/*10*/}

    SensorCall();if (size < 8) {
	SensorCall();bitoffs = ((offset%8)*size)%8;
	uoffset = offset/(8/size);
    }
    else {/*11*/SensorCall();if (size > 8)
	{/*13*/SensorCall();uoffset = offset*(size/8);/*14*/}
    else
	{/*15*/SensorCall();uoffset = offset;/*16*/}/*12*/}

    SensorCall();len = uoffset + (bitoffs + size + 7)/8;	/* required number of bytes */
    SensorCall();if (len > srclen) {
	SensorCall();if (size <= 8)
	    {/*17*/SensorCall();retnum = 0;/*18*/}
	else {
	    SensorCall();if (size == 16) {
		SensorCall();if (uoffset >= srclen)
		    {/*19*/SensorCall();retnum = 0;/*20*/}
		else
		    {/*21*/SensorCall();retnum = (UV) s[uoffset] <<  8;/*22*/}
	    }
	    else {/*23*/SensorCall();if (size == 32) {
		SensorCall();if (uoffset >= srclen)
		    {/*25*/SensorCall();retnum = 0;/*26*/}
		else {/*27*/SensorCall();if (uoffset + 1 >= srclen)
		    {/*29*/SensorCall();retnum =
			((UV) s[uoffset    ] << 24);/*30*/}
		else {/*31*/SensorCall();if (uoffset + 2 >= srclen)
		    {/*33*/SensorCall();retnum =
			((UV) s[uoffset    ] << 24) +
			((UV) s[uoffset + 1] << 16);/*34*/}
		else
		    {/*35*/SensorCall();retnum =
			((UV) s[uoffset    ] << 24) +
			((UV) s[uoffset + 1] << 16) +
			(     s[uoffset + 2] <<  8);/*36*/}/*32*/}/*28*/}
	    }
#ifdef UV_IS_QUAD
	    else {/*37*/SensorCall();if (size == 64) {
		SensorCall();Perl_ck_warner(aTHX_ packWARN(WARN_PORTABLE),
			       "Bit vector size > 32 non-portable");
		SensorCall();if (uoffset >= srclen)
		    {/*39*/SensorCall();retnum = 0;/*40*/}
		else {/*41*/SensorCall();if (uoffset + 1 >= srclen)
		    {/*43*/SensorCall();retnum =
			(UV) s[uoffset     ] << 56;/*44*/}
		else {/*45*/SensorCall();if (uoffset + 2 >= srclen)
		    {/*47*/SensorCall();retnum =
			((UV) s[uoffset    ] << 56) +
			((UV) s[uoffset + 1] << 48);/*48*/}
		else {/*49*/SensorCall();if (uoffset + 3 >= srclen)
		    {/*51*/SensorCall();retnum =
			((UV) s[uoffset    ] << 56) +
			((UV) s[uoffset + 1] << 48) +
			((UV) s[uoffset + 2] << 40);/*52*/}
		else {/*53*/SensorCall();if (uoffset + 4 >= srclen)
		    {/*55*/SensorCall();retnum =
			((UV) s[uoffset    ] << 56) +
			((UV) s[uoffset + 1] << 48) +
			((UV) s[uoffset + 2] << 40) +
			((UV) s[uoffset + 3] << 32);/*56*/}
		else {/*57*/SensorCall();if (uoffset + 5 >= srclen)
		    {/*59*/SensorCall();retnum =
			((UV) s[uoffset    ] << 56) +
			((UV) s[uoffset + 1] << 48) +
			((UV) s[uoffset + 2] << 40) +
			((UV) s[uoffset + 3] << 32) +
			(     s[uoffset + 4] << 24);/*60*/}
		else {/*61*/SensorCall();if (uoffset + 6 >= srclen)
		    {/*63*/SensorCall();retnum =
			((UV) s[uoffset    ] << 56) +
			((UV) s[uoffset + 1] << 48) +
			((UV) s[uoffset + 2] << 40) +
			((UV) s[uoffset + 3] << 32) +
			((UV) s[uoffset + 4] << 24) +
			((UV) s[uoffset + 5] << 16);/*64*/}
		else
		    {/*65*/SensorCall();retnum =
			((UV) s[uoffset    ] << 56) +
			((UV) s[uoffset + 1] << 48) +
			((UV) s[uoffset + 2] << 40) +
			((UV) s[uoffset + 3] << 32) +
			((UV) s[uoffset + 4] << 24) +
			((UV) s[uoffset + 5] << 16) +
			(     s[uoffset + 6] <<  8);/*66*/}/*62*/}/*58*/}/*54*/}/*50*/}/*46*/}/*42*/}
	    ;/*38*/}/*24*/}}
#endif
	}
    }
    else {/*67*/SensorCall();if (size < 8)
	{/*69*/SensorCall();retnum = (s[uoffset] >> bitoffs) & ((1 << size) - 1);/*70*/}
    else {
	SensorCall();if (size == 8)
	    {/*71*/SensorCall();retnum = s[uoffset];/*72*/}
	else {/*73*/SensorCall();if (size == 16)
	    {/*75*/SensorCall();retnum =
		((UV) s[uoffset] <<      8) +
		      s[uoffset + 1];/*76*/}
	else {/*77*/SensorCall();if (size == 32)
	    {/*79*/SensorCall();retnum =
		((UV) s[uoffset    ] << 24) +
		((UV) s[uoffset + 1] << 16) +
		(     s[uoffset + 2] <<  8) +
		      s[uoffset + 3];/*80*/}
#ifdef UV_IS_QUAD
	else {/*81*/SensorCall();if (size == 64) {
	    SensorCall();Perl_ck_warner(aTHX_ packWARN(WARN_PORTABLE),
			   "Bit vector size > 32 non-portable");
	    retnum =
		((UV) s[uoffset    ] << 56) +
		((UV) s[uoffset + 1] << 48) +
		((UV) s[uoffset + 2] << 40) +
		((UV) s[uoffset + 3] << 32) +
		((UV) s[uoffset + 4] << 24) +
		((UV) s[uoffset + 5] << 16) +
		(     s[uoffset + 6] <<  8) +
		      s[uoffset + 7];
	;/*82*/}/*78*/}/*74*/}}
#endif
    ;/*68*/}}

    {UV  ReplaceReturn = retnum; SensorCall(); return ReplaceReturn;}
}

/* currently converts input to bytes if possible but doesn't sweat failures,
 * although it does ensure that the string it clobbers is not marked as
 * utf8-valid any more
 */
void
Perl_do_vecset(pTHX_ SV *sv)
{
SensorCall();    dVAR;
    register I32 offset, bitoffs = 0;
    register I32 size;
    register unsigned char *s;
    register UV lval;
    I32 mask;
    STRLEN targlen;
    STRLEN len;
    SV * const targ = LvTARG(sv);

    PERL_ARGS_ASSERT_DO_VECSET;

    SensorCall();if (!targ)
	{/*83*/SensorCall();return;/*84*/}
    SensorCall();s = (unsigned char*)SvPV_force(targ, targlen);
    SensorCall();if (SvUTF8(targ)) {
	/* This is handled by the SvPOK_only below...
	if (!Perl_sv_utf8_downgrade(aTHX_ targ, TRUE))
	    SvUTF8_off(targ);
	 */
	SensorCall();(void) Perl_sv_utf8_downgrade(aTHX_ targ, TRUE);
    }

    SensorCall();(void)SvPOK_only(targ);
    lval = SvUV(sv);
    offset = LvTARGOFF(sv);
    SensorCall();if (offset < 0)
	{/*85*/SensorCall();Perl_croak(aTHX_ "Negative offset to vec in lvalue context");/*86*/}
    SensorCall();size = LvTARGLEN(sv);
    SensorCall();if (size < 1 || (size & (size-1))) /* size < 1 or not a power of two */
	{/*87*/SensorCall();Perl_croak(aTHX_ "Illegal number of bits in vec");/*88*/}

    SensorCall();if (size < 8) {
	SensorCall();bitoffs = ((offset%8)*size)%8;
	offset /= 8/size;
    }
    else {/*89*/SensorCall();if (size > 8)
	{/*91*/SensorCall();offset *= size/8;/*92*/}/*90*/}

    SensorCall();len = offset + (bitoffs + size + 7)/8;	/* required number of bytes */
    SensorCall();if (len > targlen) {
	SensorCall();s = (unsigned char*)SvGROW(targ, len + 1);
	(void)memzero((char *)(s + targlen), len - targlen + 1);
	SvCUR_set(targ, len);
    }

    SensorCall();if (size < 8) {
	SensorCall();mask = (1 << size) - 1;
	lval &= mask;
	s[offset] &= ~(mask << bitoffs);
	s[offset] |= lval << bitoffs;
    }
    else {
	SensorCall();if (size == 8)
	    {/*93*/SensorCall();s[offset  ] = (U8)( lval        & 0xff);/*94*/}
	else {/*95*/SensorCall();if (size == 16) {
	    SensorCall();s[offset  ] = (U8)((lval >>  8) & 0xff);
	    s[offset+1] = (U8)( lval        & 0xff);
	}
	else {/*97*/SensorCall();if (size == 32) {
	    SensorCall();s[offset  ] = (U8)((lval >> 24) & 0xff);
	    s[offset+1] = (U8)((lval >> 16) & 0xff);
	    s[offset+2] = (U8)((lval >>  8) & 0xff);
	    s[offset+3] = (U8)( lval        & 0xff);
	}
#ifdef UV_IS_QUAD
	else {/*99*/SensorCall();if (size == 64) {
	    SensorCall();Perl_ck_warner(aTHX_ packWARN(WARN_PORTABLE),
			   "Bit vector size > 32 non-portable");
	    s[offset  ] = (U8)((lval >> 56) & 0xff);
	    s[offset+1] = (U8)((lval >> 48) & 0xff);
	    s[offset+2] = (U8)((lval >> 40) & 0xff);
	    s[offset+3] = (U8)((lval >> 32) & 0xff);
	    s[offset+4] = (U8)((lval >> 24) & 0xff);
	    s[offset+5] = (U8)((lval >> 16) & 0xff);
	    s[offset+6] = (U8)((lval >>  8) & 0xff);
	    s[offset+7] = (U8)( lval        & 0xff);
	;/*100*/}/*98*/}/*96*/}}
#endif
    }
    SvSETMAGIC(targ);
}

void
Perl_do_vop(pTHX_ I32 optype, SV *sv, SV *left, SV *right)
{
SensorCall();    dVAR;
#ifdef LIBERAL
    register long *dl;
    register long *ll;
    register long *rl;
#endif
    register char *dc;
    STRLEN leftlen;
    STRLEN rightlen;
    register const char *lc;
    register const char *rc;
    register STRLEN len;
    STRLEN lensave;
    const char *lsave;
    const char *rsave;
    bool left_utf;
    bool right_utf;
    STRLEN needlen = 0;

    PERL_ARGS_ASSERT_DO_VOP;

    SensorCall();if (sv != left || (optype != OP_BIT_AND && !SvOK(sv)))
	sv_setpvs(sv, "");	/* avoid undef warning on |= and ^= */
    SensorCall();if (sv == left) {
	SensorCall();lsave = lc = SvPV_force_nomg(left, leftlen);
    }
    else {
	SensorCall();lsave = lc = SvPV_nomg_const(left, leftlen);
	SvPV_force_nomg_nolen(sv);
    }
    SensorCall();rsave = rc = SvPV_nomg_const(right, rightlen);

    /* This need to come after SvPV to ensure that string overloading has
       fired off.  */

    left_utf = DO_UTF8(left);
    right_utf = DO_UTF8(right);

    SensorCall();if (left_utf && !right_utf) {
	/* Avoid triggering overloading again by using temporaries.
	   Maybe there should be a variant of sv_utf8_upgrade that takes pvn
	*/
	SensorCall();right = newSVpvn_flags(rsave, rightlen, SVs_TEMP);
	sv_utf8_upgrade(right);
	rsave = rc = SvPV_nomg_const(right, rightlen);
	right_utf = TRUE;
    }
    else {/*101*/SensorCall();if (!left_utf && right_utf) {
	SensorCall();left = newSVpvn_flags(lsave, leftlen, SVs_TEMP);
	sv_utf8_upgrade(left);
	lsave = lc = SvPV_nomg_const(left, leftlen);
	left_utf = TRUE;
    ;/*102*/}}

    SensorCall();len = leftlen < rightlen ? leftlen : rightlen;
    lensave = len;
    SvCUR_set(sv, len);
    (void)SvPOK_only(sv);
    SensorCall();if ((left_utf || right_utf) && (sv == left || sv == right)) {
	SensorCall();needlen = optype == OP_BIT_AND ? len : leftlen + rightlen;
	Newxz(dc, needlen + 1, char);
    }
    else {/*103*/SensorCall();if (SvOK(sv) || SvTYPE(sv) > SVt_PVMG) {
	SensorCall();dc = SvPV_force_nomg_nolen(sv);
	SensorCall();if (SvLEN(sv) < len + 1) {
	    SensorCall();dc = SvGROW(sv, len + 1);
	    (void)memzero(dc + SvCUR(sv), len - SvCUR(sv) + 1);
	}
	SensorCall();if (optype != OP_BIT_AND && (left_utf || right_utf))
	    dc = SvGROW(sv, leftlen + rightlen + 1);
    }
    else {
	SensorCall();needlen = optype == OP_BIT_AND
		    ? len : (leftlen > rightlen ? leftlen : rightlen);
	Newxz(dc, needlen + 1, char);
	sv_usepvn_flags(sv, dc, needlen, SV_HAS_TRAILING_NUL);
	dc = SvPVX(sv);		/* sv_usepvn() calls Renew() */
    ;/*104*/}}
    SensorCall();if (left_utf || right_utf) {
	SensorCall();UV duc, luc, ruc;
	char *dcorig = dc;
	char *dcsave = NULL;
	STRLEN lulen = leftlen;
	STRLEN rulen = rightlen;
	STRLEN ulen;

	SensorCall();switch (optype) {
	case OP_BIT_AND:
	    SensorCall();while (lulen && rulen) {
		SensorCall();luc = utf8n_to_uvchr((U8*)lc, lulen, &ulen, UTF8_ALLOW_ANYUV);
		lc += ulen;
		lulen -= ulen;
		ruc = utf8n_to_uvchr((U8*)rc, rulen, &ulen, UTF8_ALLOW_ANYUV);
		rc += ulen;
		rulen -= ulen;
		duc = luc & ruc;
		dc = (char*)uvchr_to_utf8((U8*)dc, duc);
	    }
	    SensorCall();if (sv == left || sv == right)
		(void)sv_usepvn(sv, dcorig, needlen);
	    SvCUR_set(sv, dc - dcorig);
	    SensorCall();break;
	case OP_BIT_XOR:
	    SensorCall();while (lulen && rulen) {
		SensorCall();luc = utf8n_to_uvchr((U8*)lc, lulen, &ulen, UTF8_ALLOW_ANYUV);
		lc += ulen;
		lulen -= ulen;
		ruc = utf8n_to_uvchr((U8*)rc, rulen, &ulen, UTF8_ALLOW_ANYUV);
		rc += ulen;
		rulen -= ulen;
		duc = luc ^ ruc;
		dc = (char*)uvchr_to_utf8((U8*)dc, duc);
	    }
	    SensorCall();goto mop_up_utf;
	case OP_BIT_OR:
	    SensorCall();while (lulen && rulen) {
		SensorCall();luc = utf8n_to_uvchr((U8*)lc, lulen, &ulen, UTF8_ALLOW_ANYUV);
		lc += ulen;
		lulen -= ulen;
		ruc = utf8n_to_uvchr((U8*)rc, rulen, &ulen, UTF8_ALLOW_ANYUV);
		rc += ulen;
		rulen -= ulen;
		duc = luc | ruc;
		dc = (char*)uvchr_to_utf8((U8*)dc, duc);
	    }
	  mop_up_utf:
	    SensorCall();if (rulen)
		dcsave = savepvn(rc, rulen);
	    else if (lulen)
		dcsave = savepvn(lc, lulen);
	    SensorCall();if (sv == left || sv == right)
		(void)sv_usepvn(sv, dcorig, needlen); /* Uses Renew(). */
	    SvCUR_set(sv, dc - dcorig);
	    SensorCall();if (rulen)
		sv_catpvn(sv, dcsave, rulen);
	    else {/*105*/SensorCall();if (lulen)
		sv_catpvn(sv, dcsave, lulen);
	    else
		{/*107*/SensorCall();*SvEND(sv) = '\0';/*108*/}/*106*/}
	    Safefree(dcsave);
	    SensorCall();break;
	default:
	    SensorCall();if (sv == left || sv == right)
		Safefree(dcorig);
	    SensorCall();Perl_croak(aTHX_ "panic: do_vop called for op %u (%s)",
			(unsigned)optype, PL_op_name[optype]);
	}
	SvUTF8_on(sv);
	SensorCall();goto finish;
    }
    else
#ifdef LIBERAL
    {/*109*/SensorCall();if (len >= sizeof(long)*4 &&
	!((unsigned long)dc % sizeof(long)) &&
	!((unsigned long)lc % sizeof(long)) &&
	!((unsigned long)rc % sizeof(long)))	/* It's almost always aligned... */
    {
	SensorCall();const STRLEN remainder = len % (sizeof(long)*4);
	len /= (sizeof(long)*4);

	dl = (long*)dc;
	ll = (long*)lc;
	rl = (long*)rc;

	SensorCall();switch (optype) {
	case OP_BIT_AND:
	    SensorCall();while (len--) {
		SensorCall();*dl++ = *ll++ & *rl++;
		*dl++ = *ll++ & *rl++;
		*dl++ = *ll++ & *rl++;
		*dl++ = *ll++ & *rl++;
	    }
	    SensorCall();break;
	case OP_BIT_XOR:
	    SensorCall();while (len--) {
		SensorCall();*dl++ = *ll++ ^ *rl++;
		*dl++ = *ll++ ^ *rl++;
		*dl++ = *ll++ ^ *rl++;
		*dl++ = *ll++ ^ *rl++;
	    }
	    SensorCall();break;
	case OP_BIT_OR:
	    SensorCall();while (len--) {
		SensorCall();*dl++ = *ll++ | *rl++;
		*dl++ = *ll++ | *rl++;
		*dl++ = *ll++ | *rl++;
		*dl++ = *ll++ | *rl++;
	    }
	}

	SensorCall();dc = (char*)dl;
	lc = (char*)ll;
	rc = (char*)rl;

	len = remainder;
    ;/*110*/}}
#endif
    {
	SensorCall();switch (optype) {
	case OP_BIT_AND:
	    SensorCall();while (len--)
		{/*111*/SensorCall();*dc++ = *lc++ & *rc++;/*112*/}
	    SensorCall();*dc = '\0';
	    SensorCall();break;
	case OP_BIT_XOR:
	    SensorCall();while (len--)
		{/*113*/SensorCall();*dc++ = *lc++ ^ *rc++;/*114*/}
	    SensorCall();goto mop_up;
	case OP_BIT_OR:
	    SensorCall();while (len--)
		{/*115*/SensorCall();*dc++ = *lc++ | *rc++;/*116*/}
	  mop_up:
	    SensorCall();len = lensave;
	    SensorCall();if (rightlen > len)
		sv_catpvn(sv, rsave + len, rightlen - len);
	    else {/*117*/SensorCall();if (leftlen > (STRLEN)len)
		sv_catpvn(sv, lsave + len, leftlen - len);
	    else
		{/*119*/SensorCall();*SvEND(sv) = '\0';/*120*/}/*118*/}
	    SensorCall();break;
	}
    }
finish:
    SvTAINT(sv);
}

OP *
Perl_do_kv(pTHX)
{
SensorCall();    dVAR;
    dSP;
    HV * const keys = MUTABLE_HV(POPs);
    register HE *entry;
    const I32 gimme = GIMME_V;
    const I32 dokv =     (PL_op->op_type == OP_RV2HV || PL_op->op_type == OP_PADHV);
    /* op_type is OP_RKEYS/OP_RVALUES if pp_rkeys delegated to here */
    const I32 dokeys =   dokv || (PL_op->op_type == OP_KEYS || PL_op->op_type == OP_RKEYS);
    const I32 dovalues = dokv || (PL_op->op_type == OP_VALUES || PL_op->op_type == OP_RVALUES);

    (void)hv_iterinit(keys);	/* always reset iterator regardless */

    SensorCall();if (gimme == G_VOID)
	RETURN;

    SensorCall();if (gimme == G_SCALAR) {
	SensorCall();if (PL_op->op_flags & OPf_MOD || LVRET) {	/* lvalue */
	    SensorCall();SV * const ret = sv_2mortal(newSV_type(SVt_PVLV));  /* Not TARG RT#67838 */
	    sv_magic(ret, NULL, PERL_MAGIC_nkeys, NULL, 0);
	    LvTYPE(ret) = 'k';
	    LvTARG(ret) = SvREFCNT_inc_simple(keys);
	    PUSHs(ret);
	}
	else {
	    SensorCall();IV i;
	    dTARGET;

	    SensorCall();if (! SvTIED_mg((const SV *)keys, PERL_MAGIC_tied) ) {
		SensorCall();i = HvUSEDKEYS(keys);
	    }
	    else {
		SensorCall();i = 0;
		SensorCall();while (hv_iternext(keys)) {/*191*/SensorCall();i++;/*192*/}
	    }
	    PUSHi( i );
	}
	RETURN;
    }

    EXTEND(SP, HvUSEDKEYS(keys) * (dokeys + dovalues));

    PUTBACK;	/* hv_iternext and hv_iterval might clobber stack_sp */
    SensorCall();while ((entry = hv_iternext(keys))) {
	SPAGAIN;
	SensorCall();if (dokeys) {
	    SensorCall();SV* const sv = hv_iterkeysv(entry);
	    XPUSHs(sv);	/* won't clobber stack_sp */
	}
	SensorCall();if (dovalues) {
	    SensorCall();SV *tmpstr;
	    PUTBACK;
	    tmpstr = hv_iterval(keys,entry);
	    DEBUG_H(Perl_sv_setpvf(aTHX_ tmpstr, "%lu%%%d=%lu",
			    (unsigned long)HeHASH(entry),
			    (int)HvMAX(keys)+1,
			    (unsigned long)(HeHASH(entry) & HvMAX(keys))));
	    SPAGAIN;
	    XPUSHs(tmpstr);
	}
	PUTBACK;
    }
    {OP * ReplaceReturn = NORMAL; SensorCall(); return ReplaceReturn;}
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 *
 * ex: set ts=8 sts=4 sw=4 noet:
 */
