#include "var/tmp/sensor.h"
/*    doio.c
 *
 *    Copyright (C) 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000,
 *    2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008 by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 *
 */

/*
 *  Far below them they saw the white waters pour into a foaming bowl, and
 *  then swirl darkly about a deep oval basin in the rocks, until they found
 *  their way out again through a narrow gate, and flowed away, fuming and
 *  chattering, into calmer and more level reaches.
 *
 *     [p.684 of _The Lord of the Rings_, IV/vi: "The Forbidden Pool"]
 */

/* This file contains functions that do the actual I/O on behalf of ops.
 * For example, pp_print() calls the do_print() function in this file for
 * each argument needing printing.
 */

#include "EXTERN.h"
#define PERL_IN_DOIO_C
#include "perl.h"

#if defined(HAS_MSG) || defined(HAS_SEM) || defined(HAS_SHM)
#ifndef HAS_SEM
#include <sys/ipc.h>
#endif
#ifdef HAS_MSG
#include <sys/msg.h>
#endif
#ifdef HAS_SHM
#include <sys/shm.h>
# ifndef HAS_SHMAT_PROTOTYPE
    extern Shmat_t shmat (int, char *, int);
# endif
#endif
#endif

#ifdef I_UTIME
#  if defined(_MSC_VER) || defined(__MINGW32__)
#    include <sys/utime.h>
#  else
#    include <utime.h>
#  endif
#endif

#ifdef O_EXCL
#  define OPEN_EXCL O_EXCL
#else
#  define OPEN_EXCL 0
#endif

#define PERL_MODE_MAX 8
#define PERL_FLAGS_MAX 10

#include <signal.h>

bool
Perl_do_openn(pTHX_ GV *gv, register const char *oname, I32 len, int as_raw,
	      int rawmode, int rawperm, PerlIO *supplied_fp, SV **svp,
	      I32 num_svs)
{
SensorCall();    dVAR;
    register IO * const io = GvIOn(gv);
    PerlIO *saveifp = NULL;
    PerlIO *saveofp = NULL;
    int savefd = -1;
    char savetype = IoTYPE_CLOSED;
    int writing = 0;
    PerlIO *fp;
    int fd;
    int result;
    bool was_fdopen = FALSE;
    bool in_raw = 0, in_crlf = 0, out_raw = 0, out_crlf = 0;
    char *type  = NULL;
    char mode[PERL_MODE_MAX];	/* file mode ("r\0", "rb\0", "ab\0" etc.) */
    SV *namesv;

    PERL_ARGS_ASSERT_DO_OPENN;

    Zero(mode,sizeof(mode),char);
    PL_forkprocess = 1;		/* assume true if no fork */

    /* Collect default raw/crlf info from the op */
    SensorCall();if (PL_op && PL_op->op_type == OP_OPEN) {
	/* set up IO layers */
	SensorCall();const U8 flags = PL_op->op_private;
	in_raw = (flags & OPpOPEN_IN_RAW);
	in_crlf = (flags & OPpOPEN_IN_CRLF);
	out_raw = (flags & OPpOPEN_OUT_RAW);
	out_crlf = (flags & OPpOPEN_OUT_CRLF);
    }

    /* If currently open - close before we re-open */
    SensorCall();if (IoIFP(io)) {
	SensorCall();fd = PerlIO_fileno(IoIFP(io));
	SensorCall();if (IoTYPE(io) == IoTYPE_STD) {
	    /* This is a clone of one of STD* handles */
	    SensorCall();result = 0;
	}
	else if (fd >= 0 && fd <= PL_maxsysfd) {
	    /* This is one of the original STD* handles */
	    saveifp  = IoIFP(io);
	    saveofp  = IoOFP(io);
	    savetype = IoTYPE(io);
	    savefd   = fd;
	    result   = 0;
	}
	else if (IoTYPE(io) == IoTYPE_PIPE)
	    result = PerlProc_pclose(IoIFP(io));
	else if (IoIFP(io) != IoOFP(io)) {
	    if (IoOFP(io)) {
		result = PerlIO_close(IoOFP(io));
		PerlIO_close(IoIFP(io)); /* clear stdio, fd already closed */
	    }
	    else
		result = PerlIO_close(IoIFP(io));
	}
	else
	    result = PerlIO_close(IoIFP(io));
	SensorCall();if (result == EOF && fd > PL_maxsysfd) {
	    /* Why is this not Perl_warn*() call ? */
	    SensorCall();PerlIO_printf(Perl_error_log,
		"Warning: unable to close filehandle %"HEKf" properly.\n",
		 HEKfARG(GvENAME_HEK(gv))
	    );
	}
	IoOFP(io) = IoIFP(io) = NULL;
    }

    SensorCall();if (as_raw) {
        /* sysopen style args, i.e. integer mode and permissions */
	SensorCall();STRLEN ix = 0;
	const int appendtrunc =
	     0
#ifdef O_APPEND	/* Not fully portable. */
	     |O_APPEND
#endif
#ifdef O_TRUNC	/* Not fully portable. */
	     |O_TRUNC
#endif
	     ;
	const int modifyingmode = O_WRONLY|O_RDWR|O_CREAT|appendtrunc;
	int ismodifying;

	SensorCall();if (num_svs != 0) {
	    SensorCall();Perl_croak(aTHX_ "panic: sysopen with multiple args, num_svs=%ld",
		       (long) num_svs);
	}
	/* It's not always

	   O_RDONLY 0
	   O_WRONLY 1
	   O_RDWR   2

	   It might be (in OS/390 and Mac OS Classic it is)

	   O_WRONLY 1
	   O_RDONLY 2
	   O_RDWR   3

	   This means that simple & with O_RDWR would look
	   like O_RDONLY is present.  Therefore we have to
	   be more careful.
	*/
	SensorCall();if ((ismodifying = (rawmode & modifyingmode))) {
	     SensorCall();if ((ismodifying & O_WRONLY) == O_WRONLY ||
		 (ismodifying & O_RDWR)   == O_RDWR   ||
		 (ismodifying & (O_CREAT|appendtrunc)))
		  TAINT_PROPER("sysopen");
	}
	SensorCall();mode[ix++] = IoTYPE_NUMERIC; /* Marker to openn to use numeric "sysopen" */

#if defined(USE_64_BIT_RAWIO) && defined(O_LARGEFILE)
	rawmode |= O_LARGEFILE;	/* Transparently largefiley. */
#endif

        IoTYPE(io) = PerlIO_intmode2str(rawmode, &mode[ix], &writing);

	namesv = newSVpvn_flags(oname, len, SVs_TEMP);
	num_svs = 1;
	svp = &namesv;
	type = NULL;
	fp = PerlIO_openn(aTHX_ type, mode, -1, rawmode, rawperm, NULL, num_svs, svp);
    }
    else {
	/* Regular (non-sys) open */
	SensorCall();char *name;
	STRLEN olen = len;
	char *tend;
	int dodup = 0;

	type = savepvn(oname, len);
	tend = type+len;
	SAVEFREEPV(type);

        /* Lose leading and trailing white space */
	SensorCall();while (isSPACE(*type))
	    {/*35*/SensorCall();type++;/*36*/}
        SensorCall();while (tend > type && isSPACE(tend[-1]))
	    {/*37*/SensorCall();*--tend = '\0';/*38*/}

	SensorCall();if (num_svs) {
	    /* New style explicit name, type is just mode and layer info */
#ifdef USE_STDIO
	    if (SvROK(*svp) && !strchr(oname,'&')) {
		if (ckWARN(WARN_IO))
		    Perl_warner(aTHX_ packWARN(WARN_IO),
			    "Can't open a reference");
		SETERRNO(EINVAL, LIB_INVARG);
		goto say_false;
	    }
#endif /* USE_STDIO */
	    SensorCall();name = (SvOK(*svp) || SvGMAGICAL(*svp)) ?
			savesvpv (*svp) : savepvs ("");
	    SAVEFREEPV(name);
	}
	else {
	    SensorCall();name = type;
	    len  = tend-type;
	}
	IoTYPE(io) = *type;
	SensorCall();if ((*type == IoTYPE_RDWR) && /* scary */
           (*(type+1) == IoTYPE_RDONLY || *(type+1) == IoTYPE_WRONLY) &&
	    ((!num_svs || (tend > type+1 && tend[-1] != IoTYPE_PIPE)))) {
	    TAINT_PROPER("open");
	    SensorCall();mode[1] = *type++;
	    writing = 1;
	}

	SensorCall();if (*type == IoTYPE_PIPE) {
	    SensorCall();if (num_svs) {
		SensorCall();if (type[1] != IoTYPE_STD) {
	          unknown_open_mode:
		    SensorCall();Perl_croak(aTHX_ "Unknown open() mode '%.*s'", (int)olen, oname);
		}
		SensorCall();type++;
	    }
	    SensorCall();do {
		SensorCall();type++;
	    } while (isSPACE(*type));
	    SensorCall();if (!num_svs) {
		SensorCall();name = type;
		len = tend-type;
	    }
	    SensorCall();if (*name == '\0') {
		/* command is missing 19990114 */
		SensorCall();if (ckWARN(WARN_PIPE))
		    {/*39*/SensorCall();Perl_warner(aTHX_ packWARN(WARN_PIPE), "Missing command in piped open");/*40*/}
		errno = EPIPE;
		SensorCall();goto say_false;
	    }
	    SensorCall();if (!(*name == '-' && name[1] == '\0') || num_svs)
		TAINT_ENV();
	    TAINT_PROPER("piped open");
	    SensorCall();if (!num_svs && name[len-1] == '|') {
		SensorCall();name[--len] = '\0' ;
		SensorCall();if (ckWARN(WARN_PIPE))
		    {/*41*/SensorCall();Perl_warner(aTHX_ packWARN(WARN_PIPE), "Can't open bidirectional pipe");/*42*/}
	    }
	    SensorCall();mode[0] = 'w';
	    writing = 1;
            SensorCall();if (out_raw)
		{/*43*/SensorCall();mode[1] = 'b';/*44*/}
            else {/*45*/SensorCall();if (out_crlf)
		{/*47*/SensorCall();mode[1] = 't';/*48*/}/*46*/}
	    SensorCall();if (num_svs > 1) {
		SensorCall();fp = PerlProc_popen_list(mode, num_svs, svp);
	    }
	    else {
		SensorCall();fp = PerlProc_popen(name,mode);
	    }
	    SensorCall();if (num_svs) {
		SensorCall();if (*type) {
		    SensorCall();if (PerlIO_apply_layers(aTHX_ fp, mode, type) != 0) {
			SensorCall();goto say_false;
		    }
		}
	    }
	} /* IoTYPE_PIPE */
	else {/*49*/SensorCall();if (*type == IoTYPE_WRONLY) {
	    TAINT_PROPER("open");
	    SensorCall();type++;
	    SensorCall();if (*type == IoTYPE_WRONLY) {
		/* Two IoTYPE_WRONLYs in a row make for an IoTYPE_APPEND. */
		SensorCall();mode[0] = IoTYPE(io) = IoTYPE_APPEND;
		type++;
	    }
	    else {
		SensorCall();mode[0] = 'w';
	    }
	    SensorCall();writing = 1;

            SensorCall();if (out_raw)
		{/*51*/SensorCall();mode[1] = 'b';/*52*/}
            else {/*53*/SensorCall();if (out_crlf)
		{/*55*/SensorCall();mode[1] = 't';/*56*/}/*54*/}
	    SensorCall();if (*type == '&') {
	      duplicity:
		SensorCall();dodup = PERLIO_DUP_FD;
		type++;
		SensorCall();if (*type == '=') {
		    SensorCall();dodup = 0;
		    type++;
		}
		SensorCall();if (!num_svs && !*type && supplied_fp) {
		    /* "<+&" etc. is used by typemaps */
		    SensorCall();fp = supplied_fp;
		}
		else {
		    PerlIO *that_fp = NULL;
		    SensorCall();if (num_svs > 1) {
			/* diag_listed_as: More than one argument to '%s' open */
			SensorCall();Perl_croak(aTHX_ "More than one argument to '%c&' open",IoTYPE(io));
		    }
		    SensorCall();while (isSPACE(*type))
			{/*57*/SensorCall();type++;/*58*/}
		    SensorCall();if (num_svs && (SvIOK(*svp) || (SvPOK(*svp) && looks_like_number(*svp)))) {
			SensorCall();fd = SvUV(*svp);
			num_svs = 0;
		    }
		    else {/*59*/SensorCall();if (isDIGIT(*type)) {
			SensorCall();fd = atoi(type);
		    }
		    else {
			SensorCall();const IO* thatio;
			SensorCall();if (num_svs) {
			    SensorCall();thatio = sv_2io(*svp);
			}
			else {
			    SensorCall();GV * const thatgv = gv_fetchpvn_flags(type, tend - type,
						       0, SVt_PVIO);
			    thatio = GvIO(thatgv);
			}
			SensorCall();if (!thatio) {
#ifdef EINVAL
			    SETERRNO(EINVAL,SS_IVCHAN);
#endif
			    SensorCall();goto say_false;
			}
			SensorCall();if ((that_fp = IoIFP(thatio))) {
			    /* Flush stdio buffer before dup. --mjd
			     * Unfortunately SEEK_CURing 0 seems to
			     * be optimized away on most platforms;
			     * only Solaris and Linux seem to flush
			     * on that. --jhi */
#ifdef USE_SFIO
			    /* sfio fails to clear error on next
			       sfwrite, contrary to documentation.
			       -- Nicholas Clark */
			    if (PerlIO_seek(that_fp, 0, SEEK_CUR) == -1)
				PerlIO_clearerr(that_fp);
#endif
			    /* On the other hand, do all platforms
			     * take gracefully to flushing a read-only
			     * filehandle?  Perhaps we should do
			     * fsetpos(src)+fgetpos(dst)?  --nik */
			    PerlIO_flush(that_fp);
			    SensorCall();fd = PerlIO_fileno(that_fp);
			    /* When dup()ing STDIN, STDOUT or STDERR
			     * explicitly set appropriate access mode */
			    SensorCall();if (that_fp == PerlIO_stdout()
				|| that_fp == PerlIO_stderr())
			        IoTYPE(io) = IoTYPE_WRONLY;
			    else if (that_fp == PerlIO_stdin())
                                IoTYPE(io) = IoTYPE_RDONLY;
			    /* When dup()ing a socket, say result is
			     * one as well */
			    else if (IoTYPE(thatio) == IoTYPE_SOCKET)
				IoTYPE(io) = IoTYPE_SOCKET;
			}
			else
			    {/*61*/SensorCall();fd = -1;/*62*/}
		    ;/*60*/}}
		    SensorCall();if (!num_svs)
			type = NULL;
		    SensorCall();if (that_fp) {
			SensorCall();fp = PerlIO_fdupopen(aTHX_ that_fp, NULL, dodup);
		    }
		    else {
			SensorCall();if (dodup)
			    fd = PerlLIO_dup(fd);
			else
			    was_fdopen = TRUE;
			SensorCall();if (!(fp = PerlIO_openn(aTHX_ type,mode,fd,0,0,NULL,num_svs,svp))) {
			    SensorCall();if (dodup && fd >= 0)
				PerlLIO_close(fd);
			}
		    }
		}
	    } /* & */
	    else {
		SensorCall();while (isSPACE(*type))
		    {/*63*/SensorCall();type++;/*64*/}
		SensorCall();if (*type == IoTYPE_STD && (!type[1] || isSPACE(type[1]) || type[1] == ':')) {
		    SensorCall();type++;
		    fp = PerlIO_stdout();
		    IoTYPE(io) = IoTYPE_STD;
		    SensorCall();if (num_svs > 1) {
			/* diag_listed_as: More than one argument to '%s' open */
			SensorCall();Perl_croak(aTHX_ "More than one argument to '>%c' open",IoTYPE_STD);
		    }
		}
		else  {
		    SensorCall();if (!num_svs) {
			SensorCall();namesv = newSVpvn_flags(type, tend - type, SVs_TEMP);
			num_svs = 1;
			svp = &namesv;
		        type = NULL;
		    }
		    SensorCall();fp = PerlIO_openn(aTHX_ type,mode,-1,0,0,NULL,num_svs,svp);
		}
	    } /* !& */
	    SensorCall();if (!fp && type && *type && *type != ':' && !isIDFIRST(*type))
	       {/*65*/SensorCall();goto unknown_open_mode;/*66*/}
	} /* IoTYPE_WRONLY */
	else {/*67*/SensorCall();if (*type == IoTYPE_RDONLY) {
	    SensorCall();do {
		SensorCall();type++;
	    } while (isSPACE(*type));
	    SensorCall();mode[0] = 'r';
            SensorCall();if (in_raw)
		{/*69*/SensorCall();mode[1] = 'b';/*70*/}
            else {/*71*/SensorCall();if (in_crlf)
		{/*73*/SensorCall();mode[1] = 't';/*74*/}/*72*/}
	    SensorCall();if (*type == '&') {
		SensorCall();goto duplicity;
	    }
	    SensorCall();if (*type == IoTYPE_STD && (!type[1] || isSPACE(type[1]) || type[1] == ':')) {
		SensorCall();type++;
		fp = PerlIO_stdin();
		IoTYPE(io) = IoTYPE_STD;
		SensorCall();if (num_svs > 1) {
		    /* diag_listed_as: More than one argument to '%s' open */
		    SensorCall();Perl_croak(aTHX_ "More than one argument to '<%c' open",IoTYPE_STD);
		}
	    }
	    else {
		SensorCall();if (!num_svs) {
		    SensorCall();namesv = newSVpvn_flags(type, tend - type, SVs_TEMP);
		    num_svs = 1;
		    svp = &namesv;
		    type = NULL;
		}
		SensorCall();fp = PerlIO_openn(aTHX_ type,mode,-1,0,0,NULL,num_svs,svp);
	    }
	    SensorCall();if (!fp && type && *type && *type != ':' && !isIDFIRST(*type))
	       {/*75*/SensorCall();goto unknown_open_mode;/*76*/}
	} /* IoTYPE_RDONLY */
	else {/*77*/SensorCall();if ((num_svs && /* '-|...' or '...|' */
		  type[0] == IoTYPE_STD && type[1] == IoTYPE_PIPE) ||
	         (!num_svs && tend > type+1 && tend[-1] == IoTYPE_PIPE)) {
	    SensorCall();if (num_svs) {
		SensorCall();type += 2;   /* skip over '-|' */
	    }
	    else {
		SensorCall();*--tend = '\0';
		SensorCall();while (tend > type && isSPACE(tend[-1]))
		    {/*79*/SensorCall();*--tend = '\0';/*80*/}
		SensorCall();for (; isSPACE(*type); type++)
		    ;
		SensorCall();name = type;
	        len  = tend-type;
	    }
	    SensorCall();if (*name == '\0') {
		/* command is missing 19990114 */
		SensorCall();if (ckWARN(WARN_PIPE))
		    {/*83*/SensorCall();Perl_warner(aTHX_ packWARN(WARN_PIPE), "Missing command in piped open");/*84*/}
		errno = EPIPE;
		SensorCall();goto say_false;
	    }
	    SensorCall();if (!(*name == '-' && name[1] == '\0') || num_svs)
		TAINT_ENV();
	    TAINT_PROPER("piped open");
	    SensorCall();mode[0] = 'r';

            SensorCall();if (in_raw)
		{/*85*/SensorCall();mode[1] = 'b';/*86*/}
            else {/*87*/SensorCall();if (in_crlf)
		{/*89*/SensorCall();mode[1] = 't';/*90*/}/*88*/}

	    SensorCall();if (num_svs > 1) {
		SensorCall();fp = PerlProc_popen_list(mode,num_svs,svp);
	    }
	    else {
		SensorCall();fp = PerlProc_popen(name,mode);
	    }
	    IoTYPE(io) = IoTYPE_PIPE;
	    SensorCall();if (num_svs) {
		SensorCall();while (isSPACE(*type))
		    {/*91*/SensorCall();type++;/*92*/}
		SensorCall();if (*type) {
		    SensorCall();if (PerlIO_apply_layers(aTHX_ fp, mode, type) != 0) {
			SensorCall();goto say_false;
		    }
		}
	    }
	}
	else { /* layer(Args) */
	    SensorCall();if (num_svs)
		{/*93*/SensorCall();goto unknown_open_mode;/*94*/}
	    SensorCall();name = type;
	    IoTYPE(io) = IoTYPE_RDONLY;
	    SensorCall();for (; isSPACE(*name); name++)
		;
	    SensorCall();mode[0] = 'r';

            SensorCall();if (in_raw)
		{/*97*/SensorCall();mode[1] = 'b';/*98*/}
            else {/*99*/SensorCall();if (in_crlf)
		{/*101*/SensorCall();mode[1] = 't';/*102*/}/*100*/}

	    SensorCall();if (*name == '-' && name[1] == '\0') {
		SensorCall();fp = PerlIO_stdin();
		IoTYPE(io) = IoTYPE_STD;
	    }
	    else {
		SensorCall();if (!num_svs) {
		    SensorCall();namesv = newSVpvn_flags(type, tend - type, SVs_TEMP);
		    num_svs = 1;
		    svp = &namesv;
		    type = NULL;
		}
		SensorCall();fp = PerlIO_openn(aTHX_ type,mode,-1,0,0,NULL,num_svs,svp);
	    }
	;/*78*/}/*68*/}/*50*/}}
    }
    SensorCall();if (!fp) {
	SensorCall();if (IoTYPE(io) == IoTYPE_RDONLY && ckWARN(WARN_NEWLINE)
	    && strchr(oname, '\n')
	    
	)
	    {/*103*/SensorCall();Perl_warner(aTHX_ packWARN(WARN_NEWLINE), PL_warn_nl, "open");/*104*/}
	SensorCall();goto say_false;
    }

    SensorCall();if (ckWARN(WARN_IO)) {
	SensorCall();if ((IoTYPE(io) == IoTYPE_RDONLY) &&
	    (fp == PerlIO_stdout() || fp == PerlIO_stderr())) {
		SensorCall();Perl_warner(aTHX_ packWARN(WARN_IO),
			    "Filehandle STD%s reopened as %"HEKf
			    " only for input",
			    ((fp == PerlIO_stdout()) ? "OUT" : "ERR"),
			    HEKfARG(GvENAME_HEK(gv)));
	}
	else {/*105*/SensorCall();if ((IoTYPE(io) == IoTYPE_WRONLY) && fp == PerlIO_stdin()) {
		SensorCall();Perl_warner(aTHX_ packWARN(WARN_IO),
		    "Filehandle STDIN reopened as %"HEKf" only for output",
		     HEKfARG(GvENAME_HEK(gv))
		);
	;/*106*/}}
    }

    SensorCall();fd = PerlIO_fileno(fp);
    /* If there is no fd (e.g. PerlIO::scalar) assume it isn't a
     * socket - this covers PerlIO::scalar - otherwise unless we "know" the
     * type probe for socket-ness.
     */
    SensorCall();if (IoTYPE(io) && IoTYPE(io) != IoTYPE_PIPE && IoTYPE(io) != IoTYPE_STD && fd >= 0) {
	SensorCall();if (PerlLIO_fstat(fd,&PL_statbuf) < 0) {
	    /* If PerlIO claims to have fd we had better be able to fstat() it. */
	    SensorCall();(void) PerlIO_close(fp);
	    SensorCall();goto say_false;
	}
#ifndef PERL_MICRO
	SensorCall();if (S_ISSOCK(PL_statbuf.st_mode))
	    IoTYPE(io) = IoTYPE_SOCKET;	/* in case a socket was passed in to us */
#ifdef HAS_SOCKET
	else {/*107*/SensorCall();if (
#ifdef S_IFMT
	    !(PL_statbuf.st_mode & S_IFMT)
#else
	    !PL_statbuf.st_mode
#endif
	    && IoTYPE(io) != IoTYPE_WRONLY  /* Dups of STD* filehandles already have */
	    && IoTYPE(io) != IoTYPE_RDONLY  /* type so they aren't marked as sockets */
	) {				    /* on OS's that return 0 on fstat()ed pipe */
	     SensorCall();char tmpbuf[256];
	     Sock_size_t buflen = sizeof tmpbuf;
	     SensorCall();if (PerlSock_getsockname(fd, (struct sockaddr *)tmpbuf, &buflen) >= 0
		      || errno != ENOTSOCK)
		    IoTYPE(io) = IoTYPE_SOCKET; /* some OS's return 0 on fstat()ed socket */
				                /* but some return 0 for streams too, sigh */
	;/*108*/}}
#endif /* HAS_SOCKET */
#endif /* !PERL_MICRO */
    }

    /* Eeek - FIXME !!!
     * If this is a standard handle we discard all the layer stuff
     * and just dup the fd into whatever was on the handle before !
     */

    SensorCall();if (saveifp) {		/* must use old fp? */
        /* If fd is less that PL_maxsysfd i.e. STDIN..STDERR
           then dup the new fileno down
         */
	SensorCall();if (saveofp) {
	    PerlIO_flush(saveofp);	/* emulate PerlIO_close() */
	    SensorCall();if (saveofp != saveifp) {	/* was a socket? */
		PerlIO_close(saveofp);
	    }
	}
	SensorCall();if (savefd != fd) {
	    /* Still a small can-of-worms here if (say) PerlIO::scalar
	       is assigned to (say) STDOUT - for now let dup2() fail
	       and provide the error
	     */
	    SensorCall();if (PerlLIO_dup2(fd, savefd) < 0) {
		SensorCall();(void)PerlIO_close(fp);
		SensorCall();goto say_false;
	    }
#ifdef VMS
	    if (savefd != PerlIO_fileno(PerlIO_stdin())) {
                char newname[FILENAME_MAX+1];
                if (PerlIO_getname(fp, newname)) {
                    if (fd == PerlIO_fileno(PerlIO_stdout()))
                        Perl_vmssetuserlnm(aTHX_ "SYS$OUTPUT", newname);
                    if (fd == PerlIO_fileno(PerlIO_stderr()))
                        Perl_vmssetuserlnm(aTHX_ "SYS$ERROR",  newname);
                }
	    }
#endif

#if !defined(WIN32)
           /* PL_fdpid isn't used on Windows, so avoid this useless work.
            * XXX Probably the same for a lot of other places. */
            {
                Pid_t pid;
                SensorCall();SV *sv;

                sv = *av_fetch(PL_fdpid,fd,TRUE);
                SvUPGRADE(sv, SVt_IV);
                pid = SvIVX(sv);
                SvIV_set(sv, 0);
                sv = *av_fetch(PL_fdpid,savefd,TRUE);
                SvUPGRADE(sv, SVt_IV);
                SvIV_set(sv, pid);
            }
#endif

	    SensorCall();if (was_fdopen) {
                /* need to close fp without closing underlying fd */
                SensorCall();int ofd = PerlIO_fileno(fp);
                int dupfd = PerlLIO_dup(ofd);
#if defined(HAS_FCNTL) && defined(F_SETFD)
		/* Assume if we have F_SETFD we have F_GETFD */
                int coe = fcntl(ofd,F_GETFD);
#endif
                PerlIO_close(fp);
                PerlLIO_dup2(dupfd,ofd);
#if defined(HAS_FCNTL) && defined(F_SETFD)
		/* The dup trick has lost close-on-exec on ofd */
		fcntl(ofd,F_SETFD, coe);
#endif
                PerlLIO_close(dupfd);
	    }
            else
		PerlIO_close(fp);
	}
	SensorCall();fp = saveifp;
	PerlIO_clearerr(fp);
	fd = PerlIO_fileno(fp);
    }
#if defined(HAS_FCNTL) && defined(F_SETFD)
    SensorCall();if (fd >= 0) {
	dSAVE_ERRNO;
	SensorCall();fcntl(fd,F_SETFD,fd > PL_maxsysfd); /* can change errno */
	RESTORE_ERRNO;
    }
#endif
    IoIFP(io) = fp;

    IoFLAGS(io) &= ~IOf_NOLINE;
    SensorCall();if (writing) {
	SensorCall();if (IoTYPE(io) == IoTYPE_SOCKET
	    || (IoTYPE(io) == IoTYPE_WRONLY && fd >= 0 && S_ISCHR(PL_statbuf.st_mode)) ) {
	    SensorCall();char *s = mode;
	    SensorCall();if (*s == IoTYPE_IMPLICIT || *s == IoTYPE_NUMERIC)
	      {/*109*/SensorCall();s++;/*110*/}
	    SensorCall();*s = 'w';
	    SensorCall();if (!(IoOFP(io) = PerlIO_openn(aTHX_ type,s,fd,0,0,NULL,0,svp))) {
		PerlIO_close(fp);
		IoIFP(io) = NULL;
		SensorCall();goto say_false;
	    }
	}
	else
	    IoOFP(io) = fp;
    }
    {_Bool  ReplaceReturn = TRUE; SensorCall(); return ReplaceReturn;}

say_false:
    IoIFP(io) = saveifp;
    IoOFP(io) = saveofp;
    IoTYPE(io) = savetype;
    return FALSE;
}

PerlIO *
Perl_nextargv(pTHX_ register GV *gv)
{
SensorCall();    dVAR;
    register SV *sv;
#ifndef FLEXFILENAMES
    int filedev;
    int fileino;
#endif
    Uid_t fileuid;
    Gid_t filegid;
    IO * const io = GvIOp(gv);

    PERL_ARGS_ASSERT_NEXTARGV;

    SensorCall();if (!PL_argvoutgv)
	PL_argvoutgv = gv_fetchpvs("ARGVOUT", GV_ADD|GV_NOTQUAL, SVt_PVIO);
    SensorCall();if (io && (IoFLAGS(io) & IOf_ARGV) && (IoFLAGS(io) & IOf_START)) {
	IoFLAGS(io) &= ~IOf_START;
	SensorCall();if (PL_inplace) {
	    assert(PL_defoutgv);
	    SensorCall();Perl_av_create_and_push(aTHX_ &PL_argvout_stack,
				    SvREFCNT_inc_simple_NN(PL_defoutgv));
	}
    }
    SensorCall();if (PL_filemode & (S_ISUID|S_ISGID)) {
	PerlIO_flush(IoIFP(GvIOn(PL_argvoutgv)));  /* chmod must follow last write */
#ifdef HAS_FCHMOD
	SensorCall();if (PL_lastfd != -1)
	    {/*139*/SensorCall();(void)fchmod(PL_lastfd,PL_filemode);/*140*/}
#else
	(void)PerlLIO_chmod(PL_oldname,PL_filemode);
#endif
    }
    PL_lastfd = -1;
    PL_filemode = 0;
    SensorCall();if (!GvAV(gv))
	return NULL;
    SensorCall();while (av_len(GvAV(gv)) >= 0) {
	SensorCall();STRLEN oldlen;
	sv = av_shift(GvAV(gv));
	SAVEFREESV(sv);
	sv_setsv(GvSVn(gv),sv);
	SvSETMAGIC(GvSV(gv));
	PL_oldname = SvPVx(GvSV(gv), oldlen);
	SensorCall();if (do_open(gv,PL_oldname,oldlen,PL_inplace!=0,O_RDONLY,0,NULL)) {
	    SensorCall();if (PL_inplace) {
		TAINT_PROPER("inplace open");
		SensorCall();if (oldlen == 1 && *PL_oldname == '-') {
		    setdefout(gv_fetchpvs("STDOUT", GV_ADD|GV_NOTQUAL,
					  SVt_PVIO));
		    {PerlIO * ReplaceReturn = IoIFP(GvIOp(gv)); SensorCall(); return ReplaceReturn;}
		}
#ifndef FLEXFILENAMES
		filedev = PL_statbuf.st_dev;
		fileino = PL_statbuf.st_ino;
#endif
		PL_filemode = PL_statbuf.st_mode;
		SensorCall();fileuid = PL_statbuf.st_uid;
		filegid = PL_statbuf.st_gid;
		SensorCall();if (!S_ISREG(PL_filemode)) {
		    SensorCall();Perl_ck_warner_d(aTHX_ packWARN(WARN_INPLACE),
				     "Can't do inplace edit: %s is not a regular file",
				     PL_oldname );
		    do_close(gv,FALSE);
		    SensorCall();continue;
		}
		SensorCall();if (*PL_inplace && strNE(PL_inplace, "*")) {
		    SensorCall();const char *star = strchr(PL_inplace, '*');
		    SensorCall();if (star) {
			SensorCall();const char *begin = PL_inplace;
			sv_setpvs(sv, "");
			SensorCall();do {
			    sv_catpvn(sv, begin, star - begin);
			    sv_catpvn(sv, PL_oldname, oldlen);
			    SensorCall();begin = ++star;
			} while ((star = strchr(begin, '*')));
			SensorCall();if (*begin)
			    sv_catpv(sv,begin);
		    }
		    else {
			sv_catpv(sv,PL_inplace);
		    }
#ifndef FLEXFILENAMES
		    if ((PerlLIO_stat(SvPVX_const(sv),&PL_statbuf) >= 0
			 && PL_statbuf.st_dev == filedev
			 && PL_statbuf.st_ino == fileino)
#ifdef DJGPP
			|| ((_djstat_fail_bits & _STFAIL_TRUENAME)!=0)
#endif
                      )
		    {
			Perl_ck_warner_d(aTHX_ packWARN(WARN_INPLACE),
					 "Can't do inplace edit: %"SVf" would not be unique",
					 SVfARG(sv));
			do_close(gv,FALSE);
			continue;
		    }
#endif
#ifdef HAS_RENAME
#if !defined(DOSISH) && !defined(__CYGWIN__) && !defined(EPOC)
		    SensorCall();if (PerlLIO_rename(PL_oldname,SvPVX_const(sv)) < 0) {
			SensorCall();Perl_ck_warner_d(aTHX_ packWARN(WARN_INPLACE),
					 "Can't rename %s to %"SVf": %s, skipping file",
					 PL_oldname, SVfARG(sv), Strerror(errno));
			do_close(gv,FALSE);
			SensorCall();continue;
		    }
#else
		    do_close(gv,FALSE);
		    (void)PerlLIO_unlink(SvPVX_const(sv));
		    (void)PerlLIO_rename(PL_oldname,SvPVX_const(sv));
		    do_open(gv,(char*)SvPVX_const(sv),SvCUR(sv),TRUE,O_RDONLY,0,NULL);
#endif /* DOSISH */
#else
		    (void)UNLINK(SvPVX_const(sv));
		    if (link(PL_oldname,SvPVX_const(sv)) < 0) {
			Perl_ck_warner_d(aTHX_ packWARN(WARN_INPLACE),
					 "Can't rename %s to %"SVf": %s, skipping file",
					 PL_oldname, SVfARG(sv), Strerror(errno) );
			do_close(gv,FALSE);
			continue;
		    }
		    (void)UNLINK(PL_oldname);
#endif
		}
		else {
#if !defined(DOSISH) && !defined(AMIGAOS)
#  ifndef VMS  /* Don't delete; use automatic file versioning */
		    SensorCall();if (UNLINK(PL_oldname) < 0) {
			SensorCall();Perl_ck_warner_d(aTHX_ packWARN(WARN_INPLACE),
					 "Can't remove %s: %s, skipping file",
					 PL_oldname, Strerror(errno) );
			do_close(gv,FALSE);
			SensorCall();continue;
		    }
#  endif
#else
		    Perl_croak(aTHX_ "Can't do inplace edit without backup");
#endif
		}

		sv_setpvn(sv,PL_oldname,oldlen);
		SETERRNO(0,0);		/* in case sprintf set errno */
		SensorCall();if (!Perl_do_openn(aTHX_ PL_argvoutgv, (char*)SvPVX_const(sv),
				   SvCUR(sv), TRUE,
#ifdef VMS
				   O_WRONLY|O_CREAT|O_TRUNC,0,
#else
				   O_WRONLY|O_CREAT|OPEN_EXCL,0600,
#endif
				   NULL, NULL, 0)) {
		    SensorCall();Perl_ck_warner_d(aTHX_ packWARN(WARN_INPLACE), "Can't do inplace edit on %s: %s",
				     PL_oldname, Strerror(errno) );
		    do_close(gv,FALSE);
		    SensorCall();continue;
		}
		setdefout(PL_argvoutgv);
		PL_lastfd = PerlIO_fileno(IoIFP(GvIOp(PL_argvoutgv)));
		SensorCall();(void)PerlLIO_fstat(PL_lastfd,&PL_statbuf);
#ifdef HAS_FCHMOD
		(void)fchmod(PL_lastfd,PL_filemode);
#else
		(void)PerlLIO_chmod(PL_oldname,PL_filemode);
#endif
		SensorCall();if (fileuid != PL_statbuf.st_uid || filegid != PL_statbuf.st_gid) {
#ifdef HAS_FCHOWN
		    SensorCall();(void)fchown(PL_lastfd,fileuid,filegid);
#else
#ifdef HAS_CHOWN
		    (void)PerlLIO_chown(PL_oldname,fileuid,filegid);
#endif
#endif
		}
	    }
	    {PerlIO * ReplaceReturn = IoIFP(GvIOp(gv)); SensorCall(); return ReplaceReturn;}
	}
	else {
	    SensorCall();if (ckWARN_d(WARN_INPLACE)) {
		SensorCall();const int eno = errno;
		SensorCall();if (PerlLIO_stat(PL_oldname, &PL_statbuf) >= 0
		    && !S_ISREG(PL_statbuf.st_mode))	
		{
		    SensorCall();Perl_warner(aTHX_ packWARN(WARN_INPLACE),
				"Can't do inplace edit: %s is not a regular file",
				PL_oldname);
		}
		else
		    {/*141*/SensorCall();Perl_warner(aTHX_ packWARN(WARN_INPLACE), "Can't open %s: %s",
				PL_oldname, Strerror(eno));/*142*/}
	    }
	}
    }
    SensorCall();if (io && (IoFLAGS(io) & IOf_ARGV))
	IoFLAGS(io) |= IOf_START;
    SensorCall();if (PL_inplace) {
	SensorCall();(void)do_close(PL_argvoutgv,FALSE);
	SensorCall();if (io && (IoFLAGS(io) & IOf_ARGV)
	    && PL_argvout_stack && AvFILLp(PL_argvout_stack) >= 0)
	{
	    SensorCall();GV * const oldout = MUTABLE_GV(av_pop(PL_argvout_stack));
	    setdefout(oldout);
	    SvREFCNT_dec(oldout);
	    {PerlIO * ReplaceReturn = NULL; SensorCall(); return ReplaceReturn;}
	}
	setdefout(gv_fetchpvs("STDOUT", GV_ADD|GV_NOTQUAL, SVt_PVIO));
    }
    {PerlIO * ReplaceReturn = NULL; SensorCall(); return ReplaceReturn;}
}

/* explicit renamed to avoid C++ conflict    -- kja */
bool
Perl_do_close(pTHX_ GV *gv, bool not_implicit)
{
SensorCall();    dVAR;
    bool retval;
    IO *io;

    SensorCall();if (!gv)
	gv = PL_argvgv;
    SensorCall();if (!gv || !isGV_with_GP(gv)) {
	SensorCall();if (not_implicit)
	    SETERRNO(EBADF,SS_IVCHAN);
	{_Bool  ReplaceReturn = FALSE; SensorCall(); return ReplaceReturn;}
    }
    SensorCall();io = GvIO(gv);
    SensorCall();if (!io) {		/* never opened */
	SensorCall();if (not_implicit) {
	    report_evil_fh(gv);
	    SETERRNO(EBADF,SS_IVCHAN);
	}
	{_Bool  ReplaceReturn = FALSE; SensorCall(); return ReplaceReturn;}
    }
    SensorCall();retval = io_close(io, not_implicit);
    SensorCall();if (not_implicit) {
	IoLINES(io) = 0;
	IoPAGE(io) = 0;
	IoLINES_LEFT(io) = IoPAGE_LEN(io);
    }
    IoTYPE(io) = IoTYPE_CLOSED;
    {_Bool  ReplaceReturn = retval; SensorCall(); return ReplaceReturn;}
}

bool
Perl_io_close(pTHX_ IO *io, bool not_implicit)
{
SensorCall();    dVAR;
    bool retval = FALSE;

    PERL_ARGS_ASSERT_IO_CLOSE;

    SensorCall();if (IoIFP(io)) {
	SensorCall();if (IoTYPE(io) == IoTYPE_PIPE) {
	    SensorCall();const int status = PerlProc_pclose(IoIFP(io));
	    SensorCall();if (not_implicit) {
		STATUS_NATIVE_CHILD_SET(status);
		SensorCall();retval = (STATUS_UNIX == 0);
	    }
	    else {
		SensorCall();retval = (status != -1);
	    }
	}
	else {/*119*/SensorCall();if (IoTYPE(io) == IoTYPE_STD)
	    retval = TRUE;
	else {
	    SensorCall();if (IoOFP(io) && IoOFP(io) != IoIFP(io)) {		/* a socket */
		SensorCall();const bool prev_err = PerlIO_error(IoOFP(io));
		retval = (PerlIO_close(IoOFP(io)) != EOF && !prev_err);
		PerlIO_close(IoIFP(io));	/* clear stdio, fd already closed */
	    }
	    else {
		SensorCall();const bool prev_err = PerlIO_error(IoIFP(io));
		retval = (PerlIO_close(IoIFP(io)) != EOF && !prev_err);
	    }
	;/*120*/}}
	IoOFP(io) = IoIFP(io) = NULL;
    }
    else {/*121*/SensorCall();if (not_implicit) {
	SETERRNO(EBADF,SS_IVCHAN);
    ;/*122*/}}

    {_Bool  ReplaceReturn = retval; SensorCall(); return ReplaceReturn;}
}

bool
Perl_do_eof(pTHX_ GV *gv)
{
SensorCall();    dVAR;
    register IO * const io = GvIO(gv);

    PERL_ARGS_ASSERT_DO_EOF;

    SensorCall();if (!io)
	return TRUE;
    else if (IoTYPE(io) == IoTYPE_WRONLY)
	report_wrongway_fh(gv, '>');

    SensorCall();while (IoIFP(io)) {
        SensorCall();if (PerlIO_has_cntptr(IoIFP(io))) {	/* (the code works without this) */
	    SensorCall();if (PerlIO_get_cnt(IoIFP(io)) > 0)	/* cheat a little, since */
		return FALSE;			/* this is the most usual case */
        }

	{
	     /* getc and ungetc can stomp on errno */
	    dSAVE_ERRNO;
	    SensorCall();const int ch = PerlIO_getc(IoIFP(io));
	    SensorCall();if (ch != EOF) {
		SensorCall();(void)PerlIO_ungetc(IoIFP(io),ch);
		RESTORE_ERRNO;
		{_Bool  ReplaceReturn = FALSE; SensorCall(); return ReplaceReturn;}
	    }
	    RESTORE_ERRNO;
	}

        SensorCall();if (PerlIO_has_cntptr(IoIFP(io)) && PerlIO_canset_cnt(IoIFP(io))) {
	    SensorCall();if (PerlIO_get_cnt(IoIFP(io)) < -1)
		PerlIO_set_cnt(IoIFP(io),-1);
	}
	SensorCall();if (PL_op->op_flags & OPf_SPECIAL) { /* not necessarily a real EOF yet? */
	    SensorCall();if (gv != PL_argvgv || !nextargv(gv))	/* get another fp handy */
		return TRUE;
	}
	else
	    return TRUE;		/* normal fp, definitely end of file */
    }
    {_Bool  ReplaceReturn = TRUE; SensorCall(); return ReplaceReturn;}
}

Off_t
Perl_do_tell(pTHX_ GV *gv)
{
SensorCall();    dVAR;
    IO *const io = GvIO(gv);
    register PerlIO *fp;

    PERL_ARGS_ASSERT_DO_TELL;

    SensorCall();if (io && (fp = IoIFP(io))) {
#ifdef ULTRIX_STDIO_BOTCH
	if (PerlIO_eof(fp))
	    (void)PerlIO_seek(fp, 0L, 2);	/* ultrix 1.2 workaround */
#endif
	{off64_t  ReplaceReturn = PerlIO_tell(fp); SensorCall(); return ReplaceReturn;}
    }
    report_evil_fh(gv);
    SETERRNO(EBADF,RMS_IFI);
    {off64_t  ReplaceReturn = (Off_t)-1; SensorCall(); return ReplaceReturn;}
}

bool
Perl_do_seek(pTHX_ GV *gv, Off_t pos, int whence)
{
SensorCall();    dVAR;
    IO *const io = GvIO(gv);
    register PerlIO *fp;

    SensorCall();if (io && (fp = IoIFP(io))) {
#ifdef ULTRIX_STDIO_BOTCH
	if (PerlIO_eof(fp))
	    (void)PerlIO_seek(fp, 0L, 2);	/* ultrix 1.2 workaround */
#endif
	{_Bool  ReplaceReturn = PerlIO_seek(fp, pos, whence) >= 0; SensorCall(); return ReplaceReturn;}
    }
    report_evil_fh(gv);
    SETERRNO(EBADF,RMS_IFI);
    {_Bool  ReplaceReturn = FALSE; SensorCall(); return ReplaceReturn;}
}

Off_t
Perl_do_sysseek(pTHX_ GV *gv, Off_t pos, int whence)
{
SensorCall();    dVAR;
    IO *const io = GvIO(gv);
    register PerlIO *fp;

    PERL_ARGS_ASSERT_DO_SYSSEEK;

    SensorCall();if (io && (fp = IoIFP(io)))
	return PerlLIO_lseek(PerlIO_fileno(fp), pos, whence);
    report_evil_fh(gv);
    SETERRNO(EBADF,RMS_IFI);
    {off64_t  ReplaceReturn = (Off_t)-1; SensorCall(); return ReplaceReturn;}
}

int
Perl_mode_from_discipline(pTHX_ const char *s, STRLEN len)
{
    SensorCall();int mode = O_BINARY;
    SensorCall();if (s) {
	SensorCall();while (*s) {
	    SensorCall();if (*s == ':') {
		SensorCall();switch (s[1]) {
		case 'r':
		    SensorCall();if (s[2] == 'a' && s[3] == 'w'
			&& (!s[4] || s[4] == ':' || isSPACE(s[4])))
		    {
			SensorCall();mode = O_BINARY;
			s += 4;
			len -= 4;
			SensorCall();break;
		    }
		    /* FALL THROUGH */
		case 'c':
		    SensorCall();if (s[2] == 'r' && s[3] == 'l' && s[4] == 'f'
			&& (!s[5] || s[5] == ':' || isSPACE(s[5])))
		    {
			SensorCall();mode = O_TEXT;
			s += 5;
			len -= 5;
			SensorCall();break;
		    }
		    /* FALL THROUGH */
		default:
		    SensorCall();goto fail_discipline;
		}
	    }
	    else {/*123*/SensorCall();if (isSPACE(*s)) {
		SensorCall();++s;
		--len;
	    }
	    else {
		SensorCall();const char *end;
fail_discipline:
		end = strchr(s+1, ':');
		SensorCall();if (!end)
		    {/*125*/SensorCall();end = s+len;/*126*/}
#ifndef PERLIO_LAYERS
		Perl_croak(aTHX_ "IO layers (like '%.*s') unavailable", end-s, s);
#else
		SensorCall();len -= end-s;
		s = end;
#endif
	    ;/*124*/}}
	}
    }
    {int  ReplaceReturn = mode; SensorCall(); return ReplaceReturn;}
}

#if !defined(HAS_TRUNCATE) && !defined(HAS_CHSIZE)
I32
my_chsize(int fd, Off_t length)
{
#ifdef F_FREESP
	/* code courtesy of William Kucharski */
#define HAS_CHSIZE

    Stat_t filebuf;

    if (PerlLIO_fstat(fd, &filebuf) < 0)
	return -1;

    if (filebuf.st_size < length) {

	/* extend file length */

	if ((PerlLIO_lseek(fd, (length - 1), 0)) < 0)
	    return -1;

	/* write a "0" byte */

	if ((PerlLIO_write(fd, "", 1)) != 1)
	    return -1;
    }
    else {
	/* truncate length */
	struct flock fl;
	fl.l_whence = 0;
	fl.l_len = 0;
	fl.l_start = length;
	fl.l_type = F_WRLCK;    /* write lock on file space */

	/*
	* This relies on the UNDOCUMENTED F_FREESP argument to
	* fcntl(2), which truncates the file so that it ends at the
	* position indicated by fl.l_start.
	*
	* Will minor miracles never cease?
	*/

	if (fcntl(fd, F_FREESP, &fl) < 0)
	    return -1;

    }
    return 0;
#else
    Perl_croak_nocontext("truncate not implemented");
#endif /* F_FREESP */
    return -1;
}
#endif /* !HAS_TRUNCATE && !HAS_CHSIZE */

bool
Perl_do_print(pTHX_ register SV *sv, PerlIO *fp)
{
SensorCall();    dVAR;

    PERL_ARGS_ASSERT_DO_PRINT;

    /* assuming fp is checked earlier */
    SensorCall();if (!sv)
	return TRUE;
    SensorCall();if (SvTYPE(sv) == SVt_IV && SvIOK(sv)) {
	assert(!SvGMAGICAL(sv));
	SensorCall();if (SvIsUV(sv))
	    {/*111*/SensorCall();PerlIO_printf(fp, "%"UVuf, (UV)SvUVX(sv));/*112*/}
	else
	    {/*113*/SensorCall();PerlIO_printf(fp, "%"IVdf, (IV)SvIVX(sv));/*114*/}
	{_Bool  ReplaceReturn = !PerlIO_error(fp); SensorCall(); return ReplaceReturn;}
    }
    else {
	SensorCall();STRLEN len;
	/* Do this first to trigger any overloading.  */
	const char *tmps = SvPV_const(sv, len);
	U8 *tmpbuf = NULL;
	bool happy = TRUE;

	SensorCall();if (PerlIO_isutf8(fp)) {
	    SensorCall();if (!SvUTF8(sv)) {
		/* We don't modify the original scalar.  */
		SensorCall();tmpbuf = bytes_to_utf8((const U8*) tmps, &len);
		tmps = (char *) tmpbuf;
	    }
	    else {/*115*/SensorCall();if (ckWARN4_d(WARN_UTF8, WARN_SURROGATE, WARN_NON_UNICODE, WARN_NONCHAR)) {
		SensorCall();(void) check_utf8_print((const U8*) tmps, len);
	    ;/*116*/}}
	}
	else {/*117*/SensorCall();if (DO_UTF8(sv)) {
	    SensorCall();STRLEN tmplen = len;
	    bool utf8 = TRUE;
	    U8 * const result = bytes_from_utf8((const U8*) tmps, &tmplen, &utf8);
	    SensorCall();if (!utf8) {
		SensorCall();tmpbuf = result;
		tmps = (char *) tmpbuf;
		len = tmplen;
	    }
	    else {
		assert((char *)result == tmps);
		SensorCall();Perl_ck_warner_d(aTHX_ packWARN(WARN_UTF8),
				 "Wide character in %s",
				   PL_op ? OP_DESC(PL_op) : "print"
				);
		    /* Could also check that isn't one of the things to avoid
		     * in utf8 by using check_utf8_print(), but not doing so,
		     * since the stream isn't a UTF8 stream */
	    }
	;/*118*/}}
	/* To detect whether the process is about to overstep its
	 * filesize limit we would need getrlimit().  We could then
	 * also transparently raise the limit with setrlimit() --
	 * but only until the system hard limit/the filesystem limit,
	 * at which we would get EPERM.  Note that when using buffered
	 * io the write failure can be delayed until the flush/close. --jhi */
	SensorCall();if (len && (PerlIO_write(fp,tmps,len) == 0))
	    happy = FALSE;
	Safefree(tmpbuf);
	{_Bool  ReplaceReturn = happy ? !PerlIO_error(fp) : FALSE; SensorCall(); return ReplaceReturn;}
    }
SensorCall();}

I32
Perl_my_stat_flags(pTHX_ const U32 flags)
{
SensorCall();    dVAR;
    dSP;
    IO *io;
    GV* gv;

    SensorCall();if (PL_op->op_flags & OPf_REF) {
	EXTEND(SP,1);
	SensorCall();gv = cGVOP_gv;
      do_fstat:
        SensorCall();if (gv == PL_defgv)
            return PL_laststatval;
	SensorCall();io = GvIO(gv);
        do_fstat_have_io:
        PL_laststype = OP_STAT;
        PL_statgv = gv ? gv : (GV *)io;
        sv_setpvs(PL_statname, "");
        SensorCall();if(io) {
	    SensorCall();if (IoIFP(io)) {
	        {I32  ReplaceReturn = (PL_laststatval = PerlLIO_fstat(PerlIO_fileno(IoIFP(io)), &PL_statcache)); SensorCall(); return ReplaceReturn;}
            } else {/*133*/SensorCall();if (IoDIRP(io)) {
                {I32  ReplaceReturn = (PL_laststatval = PerlLIO_fstat(my_dirfd(IoDIRP(io)), &PL_statcache)); SensorCall(); return ReplaceReturn;}
            ;/*134*/}}
        }
	PL_laststatval = -1;
	report_evil_fh(gv);
	{I32  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}
    }
    else {
      SensorCall();SV* const sv = PL_op->op_private & OPpFT_STACKING ? TOPs : POPs;
      PUTBACK;
      SensorCall();if ((PL_op->op_private & (OPpFT_STACKED|OPpFT_AFTER_t))
	     == OPpFT_STACKED)
	return PL_laststatval;
      else {
	SensorCall();const char *s;
	STRLEN len;
	SensorCall();if ((gv = MAYBE_DEREF_GV_flags(sv,flags))) {
	    SensorCall();goto do_fstat;
	}
        else {/*135*/SensorCall();if (SvROK(sv) && SvTYPE(SvRV(sv)) == SVt_PVIO) {
            SensorCall();io = MUTABLE_IO(SvRV(sv));
	    gv = NULL;
            SensorCall();goto do_fstat_have_io;
        ;/*136*/}}

	SensorCall();s = SvPV_flags_const(sv, len, flags);
	PL_statgv = NULL;
	sv_setpvn(PL_statname, s, len);
	s = SvPVX_const(PL_statname);		/* s now NUL-terminated */
	PL_laststype = OP_STAT;
	PL_laststatval = PerlLIO_stat(s, &PL_statcache);
	SensorCall();if (PL_laststatval < 0 && ckWARN(WARN_NEWLINE) && strchr(s, '\n'))
	    {/*137*/SensorCall();Perl_warner(aTHX_ packWARN(WARN_NEWLINE), PL_warn_nl, "stat");/*138*/}
	{I32  ReplaceReturn = PL_laststatval; SensorCall(); return ReplaceReturn;}
      }
    }
SensorCall();}


I32
Perl_my_lstat_flags(pTHX_ const U32 flags)
{
SensorCall();    dVAR;
    static const char no_prev_lstat[] = "The stat preceding -l _ wasn't an lstat";
    dSP;
    SV *sv;
    const char *file;
    SensorCall();if (PL_op->op_flags & OPf_REF) {
	EXTEND(SP,1);
	SensorCall();if (cGVOP_gv == PL_defgv) {
	    SensorCall();if (PL_laststype != OP_LSTAT)
		{/*127*/SensorCall();Perl_croak(aTHX_ no_prev_lstat);/*128*/}
	    {I32  ReplaceReturn = PL_laststatval; SensorCall(); return ReplaceReturn;}
	}
	PL_laststatval = -1;
	SensorCall();if (ckWARN(WARN_IO)) {
	    SensorCall();Perl_warner(aTHX_ packWARN(WARN_IO),
		 	     "Use of -l on filehandle %"HEKf,
			      HEKfARG(GvENAME_HEK(cGVOP_gv)));
	}
	{I32  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}
    }
    SensorCall();sv = PL_op->op_private & OPpFT_STACKING ? TOPs : POPs;
    PUTBACK;
    SensorCall();if ((PL_op->op_private & (OPpFT_STACKED|OPpFT_AFTER_t))
	     == OPpFT_STACKED) {
      SensorCall();if (PL_laststype != OP_LSTAT)
	{/*129*/SensorCall();Perl_croak(aTHX_ no_prev_lstat);/*130*/}
      {I32  ReplaceReturn = PL_laststatval; SensorCall(); return ReplaceReturn;}
    } 

    PL_laststype = OP_LSTAT;
    PL_statgv = NULL;
    SensorCall();file = SvPV_flags_const_nolen(sv, flags);
    sv_setpv(PL_statname,file);
    PL_laststatval = PerlLIO_lstat(file,&PL_statcache);
    SensorCall();if (PL_laststatval < 0 && ckWARN(WARN_NEWLINE) && strchr(file, '\n'))
	{/*131*/SensorCall();Perl_warner(aTHX_ packWARN(WARN_NEWLINE), PL_warn_nl, "lstat");/*132*/}
    {I32  ReplaceReturn = PL_laststatval; SensorCall(); return ReplaceReturn;}
}

static void
S_exec_failed(pTHX_ const char *cmd, int fd, int do_report)
{
    SensorCall();const int e = errno;
    PERL_ARGS_ASSERT_EXEC_FAILED;
    SensorCall();if (ckWARN(WARN_EXEC))
	{/*187*/SensorCall();Perl_warner(aTHX_ packWARN(WARN_EXEC), "Can't exec \"%s\": %s",
		    cmd, Strerror(e));/*188*/}
    SensorCall();if (do_report) {
	PerlLIO_write(fd, (void*)&e, sizeof(int));
	PerlLIO_close(fd);
    }
SensorCall();}

bool
Perl_do_aexec5(pTHX_ SV *really, register SV **mark, register SV **sp,
	       int fd, int do_report)
{
SensorCall();    dVAR;
    PERL_ARGS_ASSERT_DO_AEXEC5;
#if defined(__SYMBIAN32__) || defined(__LIBCATAMOUNT__)
    Perl_croak(aTHX_ "exec? I'm not *that* kind of operating system");
#else
    SensorCall();if (sp > mark) {
	SensorCall();const char **a;
	const char *tmps = NULL;
	Newx(PL_Argv, sp - mark + 1, const char*);
	a = PL_Argv;

	SensorCall();while (++mark <= sp) {
	    SensorCall();if (*mark)
		*a++ = SvPV_nolen_const(*mark);
	    else
		{/*33*/SensorCall();*a++ = "";/*34*/}
	}
	SensorCall();*a = NULL;
	SensorCall();if (really)
	    tmps = SvPV_nolen_const(really);
	SensorCall();if ((!really && *PL_Argv[0] != '/') ||
	    (really && *tmps != '/'))		/* will execvp use PATH? */
	    TAINT_ENV();		/* testing IFS here is overkill, probably */
	PERL_FPU_PRE_EXEC
	SensorCall();if (really && *tmps)
	    PerlProc_execvp(tmps,EXEC_ARGV_CAST(PL_Argv));
	else
	    PerlProc_execvp(PL_Argv[0],EXEC_ARGV_CAST(PL_Argv));
	PERL_FPU_POST_EXEC
 	SensorCall();S_exec_failed(aTHX_ (really ? tmps : PL_Argv[0]), fd, do_report);
    }
    do_execfree();
#endif
    {_Bool  ReplaceReturn = FALSE; SensorCall(); return ReplaceReturn;}
}

void
Perl_do_execfree(pTHX)
{
SensorCall();    dVAR;
    Safefree(PL_Argv);
    PL_Argv = NULL;
    Safefree(PL_Cmd);
    PL_Cmd = NULL;
}

#ifdef PERL_DEFAULT_DO_EXEC3_IMPLEMENTATION

bool
Perl_do_exec3(pTHX_ const char *incmd, int fd, int do_report)
{
SensorCall();    dVAR;
    register const char **a;
    register char *s;
    char *buf;
    char *cmd;
    /* Make a copy so we can change it */
    const Size_t cmdlen = strlen(incmd) + 1;

    PERL_ARGS_ASSERT_DO_EXEC3;

    Newx(buf, cmdlen, char);
    cmd = buf;
    memcpy(cmd, incmd, cmdlen);

    SensorCall();while (*cmd && isSPACE(*cmd))
	{/*143*/SensorCall();cmd++;/*144*/}

    /* save an extra exec if possible */

#ifdef CSH
    {
        SensorCall();char flags[PERL_FLAGS_MAX];
	SensorCall();if (strnEQ(cmd,PL_cshname,PL_cshlen) &&
	    strnEQ(cmd+PL_cshlen," -c",3)) {
          my_strlcpy(flags, "-c", PERL_FLAGS_MAX);
	  SensorCall();s = cmd+PL_cshlen+3;
	  SensorCall();if (*s == 'f') {
	      SensorCall();s++;
              my_strlcat(flags, "f", PERL_FLAGS_MAX - 2);
	  }
	  SensorCall();if (*s == ' ')
	      {/*145*/SensorCall();s++;/*146*/}
	  SensorCall();if (*s++ == '\'') {
	      SensorCall();char * const ncmd = s;

	      SensorCall();while (*s)
		  {/*147*/SensorCall();s++;/*148*/}
	      SensorCall();if (s[-1] == '\n')
		  {/*149*/SensorCall();*--s = '\0';/*150*/}
	      SensorCall();if (s[-1] == '\'') {
		  SensorCall();*--s = '\0';
		  PERL_FPU_PRE_EXEC
		  PerlProc_execl(PL_cshname, "csh", flags, ncmd, (char*)NULL);
		  PERL_FPU_POST_EXEC
		  *s = '\'';
 		  S_exec_failed(aTHX_ PL_cshname, fd, do_report);
		  Safefree(buf);
		  {_Bool  ReplaceReturn = FALSE; SensorCall(); return ReplaceReturn;}
	      }
	  }
	}
    }
#endif /* CSH */

    /* see if there are shell metacharacters in it */

    SensorCall();if (*cmd == '.' && isSPACE(cmd[1]))
	{/*151*/SensorCall();goto doshell;/*152*/}

    SensorCall();if (strnEQ(cmd,"exec",4) && isSPACE(cmd[4]))
	{/*153*/SensorCall();goto doshell;/*154*/}

    SensorCall();s = cmd;
    SensorCall();while (isALNUM(*s))
	{/*155*/SensorCall();s++;/*156*/}	/* catch VAR=val gizmo */
    SensorCall();if (*s == '=')
	{/*157*/SensorCall();goto doshell;/*158*/}

    SensorCall();for (s = cmd; *s; s++) {
	SensorCall();if (*s != ' ' && !isALPHA(*s) &&
	    strchr("$&*(){}[]'\";\\|?<>~`\n",*s)) {
	    SensorCall();if (*s == '\n' && !s[1]) {
		SensorCall();*s = '\0';
		SensorCall();break;
	    }
	    /* handle the 2>&1 construct at the end */
	    SensorCall();if (*s == '>' && s[1] == '&' && s[2] == '1'
		&& s > cmd + 1 && s[-1] == '2' && isSPACE(s[-2])
		&& (!s[3] || isSPACE(s[3])))
	    {
                SensorCall();const char *t = s + 3;

		SensorCall();while (*t && isSPACE(*t))
		    {/*159*/SensorCall();++t;/*160*/}
		SensorCall();if (!*t && (PerlLIO_dup2(1,2) != -1)) {
		    SensorCall();s[-2] = '\0';
		    SensorCall();break;
		}
	    }
	  doshell:
	    PERL_FPU_PRE_EXEC
	    PerlProc_execl(PL_sh_path, "sh", "-c", cmd, (char *)NULL);
	    PERL_FPU_POST_EXEC
 	    SensorCall();S_exec_failed(aTHX_ PL_sh_path, fd, do_report);
	    Safefree(buf);
	    {_Bool  ReplaceReturn = FALSE; SensorCall(); return ReplaceReturn;}
	}
    }

    Newx(PL_Argv, (s - cmd) / 2 + 2, const char*);
    PL_Cmd = savepvn(cmd, s-cmd);
    SensorCall();a = PL_Argv;
    SensorCall();for (s = PL_Cmd; *s;) {
	SensorCall();while (isSPACE(*s))
	    {/*161*/SensorCall();s++;/*162*/}
	SensorCall();if (*s)
	    {/*163*/SensorCall();*(a++) = s;/*164*/}
	SensorCall();while (*s && !isSPACE(*s))
	    {/*165*/SensorCall();s++;/*166*/}
	SensorCall();if (*s)
	    {/*167*/SensorCall();*s++ = '\0';/*168*/}
    }
    SensorCall();*a = NULL;
    SensorCall();if (PL_Argv[0]) {
	PERL_FPU_PRE_EXEC
	PerlProc_execvp(PL_Argv[0],EXEC_ARGV_CAST(PL_Argv));
	PERL_FPU_POST_EXEC
	SensorCall();if (errno == ENOEXEC) {		/* for system V NIH syndrome */
	    do_execfree();
	    SensorCall();goto doshell;
	}
 	SensorCall();S_exec_failed(aTHX_ PL_Argv[0], fd, do_report);
    }
    do_execfree();
    Safefree(buf);
    {_Bool  ReplaceReturn = FALSE; SensorCall(); return ReplaceReturn;}
}

#endif /* OS2 || WIN32 */

I32
Perl_apply(pTHX_ I32 type, register SV **mark, register SV **sp)
{
SensorCall();    dVAR;
    register I32 val;
    register I32 tot = 0;
    const char *const what = PL_op_name[type];
    const char *s;
    STRLEN len;
    SV ** const oldmark = mark;

    PERL_ARGS_ASSERT_APPLY;

    /* Doing this ahead of the switch statement preserves the old behaviour,
       where attempting to use kill as a taint test test would fail on
       platforms where kill was not defined.  */
#ifndef HAS_KILL
    if (type == OP_KILL)
	Perl_die(aTHX_ PL_no_func, what);
#endif
#ifndef HAS_CHOWN
    if (type == OP_CHOWN)
	Perl_die(aTHX_ PL_no_func, what);
#endif


#define APPLY_TAINT_PROPER() \
    STMT_START {							\
	if (PL_tainted) { TAINT_PROPER(what); }				\
    } STMT_END

    /* This is a first heuristic; it doesn't catch tainting magic. */
    SensorCall();if (PL_tainting) {
	SensorCall();while (++mark <= sp) {
	    SensorCall();if (SvTAINTED(*mark)) {
		TAINT;
		SensorCall();break;
	    }
	}
	SensorCall();mark = oldmark;
    }
    SensorCall();switch (type) {
    case OP_CHMOD:
	APPLY_TAINT_PROPER();
	SensorCall();if (++mark <= sp) {
	    SensorCall();val = SvIV(*mark);
	    APPLY_TAINT_PROPER();
	    tot = sp - mark;
	    SensorCall();while (++mark <= sp) {
                SensorCall();GV* gv;
                SensorCall();if ((gv = MAYBE_DEREF_GV(*mark))) {
		    SensorCall();if (GvIO(gv) && IoIFP(GvIOp(gv))) {
#ifdef HAS_FCHMOD
			APPLY_TAINT_PROPER();
			SensorCall();if (fchmod(PerlIO_fileno(IoIFP(GvIOn(gv))), val))
			    {/*1*/SensorCall();tot--;/*2*/}
#else
			Perl_die(aTHX_ PL_no_func, "fchmod");
#endif
		    }
		    else {
			SensorCall();tot--;
		    }
		}
		else {
		    SensorCall();const char *name = SvPV_nomg_const_nolen(*mark);
		    APPLY_TAINT_PROPER();
		    SensorCall();if (PerlLIO_chmod(name, val))
			{/*3*/SensorCall();tot--;/*4*/}
		}
	    }
	}
	SensorCall();break;
#ifdef HAS_CHOWN
    case OP_CHOWN:
	APPLY_TAINT_PROPER();
	SensorCall();if (sp - mark > 2) {
            SensorCall();register I32 val2;
	    val = SvIVx(*++mark);
	    val2 = SvIVx(*++mark);
	    APPLY_TAINT_PROPER();
	    tot = sp - mark;
	    SensorCall();while (++mark <= sp) {
                SensorCall();GV* gv;
		SensorCall();if ((gv = MAYBE_DEREF_GV(*mark))) {
		    SensorCall();if (GvIO(gv) && IoIFP(GvIOp(gv))) {
#ifdef HAS_FCHOWN
			APPLY_TAINT_PROPER();
			SensorCall();if (fchown(PerlIO_fileno(IoIFP(GvIOn(gv))), val, val2))
			    {/*5*/SensorCall();tot--;/*6*/}
#else
			Perl_die(aTHX_ PL_no_func, "fchown");
#endif
		    }
		    else {
			SensorCall();tot--;
		    }
		}
		else {
		    SensorCall();const char *name = SvPV_nomg_const_nolen(*mark);
		    APPLY_TAINT_PROPER();
		    SensorCall();if (PerlLIO_chown(name, val, val2))
			{/*7*/SensorCall();tot--;/*8*/}
		}
	    }
	}
	SensorCall();break;
#endif
/*
XXX Should we make lchown() directly available from perl?
For now, we'll let Configure test for HAS_LCHOWN, but do
nothing in the core.
    --AD  5/1998
*/
#ifdef HAS_KILL
    case OP_KILL:
	APPLY_TAINT_PROPER();
	SensorCall();if (mark == sp)
	    {/*9*/SensorCall();break;/*10*/}
	SensorCall();s = SvPVx_const(*++mark, len);
	SensorCall();if (isALPHA(*s)) {
	    SensorCall();if (*s == 'S' && s[1] == 'I' && s[2] == 'G') {
		SensorCall();s += 3;
                len -= 3;
            }
           SensorCall();if ((val = whichsig_pvn(s, len)) < 0)
               {/*11*/SensorCall();Perl_croak(aTHX_ "Unrecognized signal name \"%"SVf"\"", SVfARG(*mark));/*12*/}
	}
	else
	    val = SvIV(*mark);
	APPLY_TAINT_PROPER();
	SensorCall();tot = sp - mark;
#ifdef VMS
	/* kill() doesn't do process groups (job trees?) under VMS */
	if (val < 0) val = -val;
	if (val == SIGKILL) {
#	    include <starlet.h>
	    /* Use native sys$delprc() to insure that target process is
	     * deleted; supervisor-mode images don't pay attention to
	     * CRTL's emulation of Unix-style signals and kill()
	     */
	    while (++mark <= sp) {
		I32 proc;
		register unsigned long int __vmssts;
		SvGETMAGIC(*mark);
		if (!(SvIOK(*mark) || SvNOK(*mark) || looks_like_number(*mark)))
		    Perl_croak(aTHX_ "Can't kill a non-numeric process ID");
		proc = SvIV_nomg(*mark);
		APPLY_TAINT_PROPER();
		if (!((__vmssts = sys$delprc(&proc,0)) & 1)) {
		    tot--;
		    switch (__vmssts) {
			case SS$_NONEXPR:
			case SS$_NOSUCHNODE:
			    SETERRNO(ESRCH,__vmssts);
			    break;
			case SS$_NOPRIV:
			    SETERRNO(EPERM,__vmssts);
			    break;
			default:
			    SETERRNO(EVMSERR,__vmssts);
		    }
		}
	    }
	    PERL_ASYNC_CHECK();
	    break;
	}
#endif
	SensorCall();if (val < 0) {
	    SensorCall();val = -val;
	    SensorCall();while (++mark <= sp) {
		SensorCall();I32 proc;
		SvGETMAGIC(*mark);
		SensorCall();if (!(SvIOK(*mark) || SvNOK(*mark) || looks_like_number(*mark)))
		    {/*13*/SensorCall();Perl_croak(aTHX_ "Can't kill a non-numeric process ID");/*14*/}
		SensorCall();proc = SvIV_nomg(*mark);
		APPLY_TAINT_PROPER();
#ifdef HAS_KILLPG
		SensorCall();if (PerlProc_killpg(proc,val))	/* BSD */
#else
		if (PerlProc_kill(-proc,val))	/* SYSV */
#endif
		    {/*15*/SensorCall();tot--;/*16*/}
	    }
	}
	else {
	    SensorCall();while (++mark <= sp) {
		SensorCall();I32 proc;
		SvGETMAGIC(*mark);
		SensorCall();if (!(SvIOK(*mark) || SvNOK(*mark) || looks_like_number(*mark)))
		    {/*17*/SensorCall();Perl_croak(aTHX_ "Can't kill a non-numeric process ID");/*18*/}
		SensorCall();proc = SvIV_nomg(*mark);
		APPLY_TAINT_PROPER();
		SensorCall();if (PerlProc_kill(proc, val))
		    {/*19*/SensorCall();tot--;/*20*/}
	    }
	}
	PERL_ASYNC_CHECK();
	SensorCall();break;
#endif
    case OP_UNLINK:
	APPLY_TAINT_PROPER();
	SensorCall();tot = sp - mark;
	SensorCall();while (++mark <= sp) {
	    SensorCall();s = SvPV_nolen_const(*mark);
	    APPLY_TAINT_PROPER();
	    SensorCall();if (PerlProc_geteuid() || PL_unsafe) {
		SensorCall();if (UNLINK(s))
		    {/*21*/SensorCall();tot--;/*22*/}
	    }
	    else {	/* don't let root wipe out directories without -U */
		SensorCall();if (PerlLIO_lstat(s,&PL_statbuf) < 0 || S_ISDIR(PL_statbuf.st_mode))
		    {/*23*/SensorCall();tot--;/*24*/}
		else {
		    SensorCall();if (UNLINK(s))
			{/*25*/SensorCall();tot--;/*26*/}
		}
	    }
	}
	SensorCall();break;
#if defined(HAS_UTIME) || defined(HAS_FUTIMES)
    case OP_UTIME:
	APPLY_TAINT_PROPER();
	SensorCall();if (sp - mark > 2) {
#if defined(HAS_FUTIMES)
	    SensorCall();struct timeval utbuf[2];
	    void *utbufp = utbuf;
#elif defined(I_UTIME) || defined(VMS)
	    struct utimbuf utbuf;
	    struct utimbuf *utbufp = &utbuf;
#else
	    struct {
		Time_t	actime;
		Time_t	modtime;
	    } utbuf;
	    void *utbufp = &utbuf;
#endif

	   SV* const accessed = *++mark;
	   SV* const modified = *++mark;

           /* Be like C, and if both times are undefined, let the C
            * library figure out what to do.  This usually means
            * "current time". */

           SensorCall();if ( accessed == &PL_sv_undef && modified == &PL_sv_undef )
                utbufp = NULL;
           else {
                Zero(&utbuf, sizeof utbuf, char);
#ifdef HAS_FUTIMES
		SensorCall();utbuf[0].tv_sec = (long)SvIV(accessed);  /* time accessed */
		utbuf[0].tv_usec = 0;
		utbuf[1].tv_sec = (long)SvIV(modified);  /* time modified */
		utbuf[1].tv_usec = 0;
#elif defined(BIG_TIME)
                utbuf.actime = (Time_t)SvNV(accessed);  /* time accessed */
                utbuf.modtime = (Time_t)SvNV(modified); /* time modified */
#else
                utbuf.actime = (Time_t)SvIV(accessed);  /* time accessed */
                utbuf.modtime = (Time_t)SvIV(modified); /* time modified */
#endif
            }
	    APPLY_TAINT_PROPER();
	    SensorCall();tot = sp - mark;
	    SensorCall();while (++mark <= sp) {
                SensorCall();GV* gv;
                SensorCall();if ((gv = MAYBE_DEREF_GV(*mark))) {
		    SensorCall();if (GvIO(gv) && IoIFP(GvIOp(gv))) {
#ifdef HAS_FUTIMES
			APPLY_TAINT_PROPER();
			SensorCall();if (futimes(PerlIO_fileno(IoIFP(GvIOn(gv))),
                            (struct timeval *) utbufp))
			    {/*27*/SensorCall();tot--;/*28*/}
#else
			Perl_die(aTHX_ PL_no_func, "futimes");
#endif
		    }
		    else {
			SensorCall();tot--;
		    }
		}
		else {
		    SensorCall();const char * const name = SvPV_nomg_const_nolen(*mark);
		    APPLY_TAINT_PROPER();
#ifdef HAS_FUTIMES
		    SensorCall();if (utimes(name, (struct timeval *)utbufp))
#else
		    if (PerlLIO_utime(name, utbufp))
#endif
			{/*29*/SensorCall();tot--;/*30*/}
		}

	    }
	}
	else
	    {/*31*/SensorCall();tot = 0;/*32*/}
	SensorCall();break;
#endif
    }
    {I32  ReplaceReturn = tot; SensorCall(); return ReplaceReturn;}

#undef APPLY_TAINT_PROPER
}

/* Do the permissions allow some operation?  Assumes statcache already set. */
#ifndef VMS /* VMS' cando is in vms.c */
bool
Perl_cando(pTHX_ Mode_t mode, bool effective, register const Stat_t *statbufp)
/* effective is a flag, true for EUID, or for checking if the effective gid
 *  is in the list of groups returned from getgroups().
 */
{
SensorCall();    dVAR;

    PERL_ARGS_ASSERT_CANDO;

#ifdef DOSISH
    /* [Comments and code from Len Reed]
     * MS-DOS "user" is similar to UNIX's "superuser," but can't write
     * to write-protected files.  The execute permission bit is set
     * by the Microsoft C library stat() function for the following:
     *		.exe files
     *		.com files
     *		.bat files
     *		directories
     * All files and directories are readable.
     * Directories and special files, e.g. "CON", cannot be
     * write-protected.
     * [Comment by Tom Dinger -- a directory can have the write-protect
     *		bit set in the file system, but DOS permits changes to
     *		the directory anyway.  In addition, all bets are off
     *		here for networked software, such as Novell and
     *		Sun's PC-NFS.]
     */

     /* Atari stat() does pretty much the same thing. we set x_bit_set_in_stat
      * too so it will actually look into the files for magic numbers
      */
     return (mode & statbufp->st_mode) ? TRUE : FALSE;

#else /* ! DOSISH */
# ifdef __CYGWIN__
    if (ingroup(544,effective)) {     /* member of Administrators */
# else
    SensorCall();if ((effective ? PerlProc_geteuid() : PerlProc_getuid()) == 0) {	/* root is special */
# endif
	SensorCall();if (mode == S_IXUSR) {
	    SensorCall();if (statbufp->st_mode & 0111 || S_ISDIR(statbufp->st_mode))
		return TRUE;
	}
	else
	    return TRUE;		/* root reads and writes anything */
	{_Bool  ReplaceReturn = FALSE; SensorCall(); return ReplaceReturn;}
    }
    SensorCall();if (statbufp->st_uid == (effective ? PerlProc_geteuid() : PerlProc_getuid()) ) {
	SensorCall();if (statbufp->st_mode & mode)
	    return TRUE;	/* ok as "user" */
    }
    else if (ingroup(statbufp->st_gid,effective)) {
	if (statbufp->st_mode & mode >> 3)
	    return TRUE;	/* ok as "group" */
    }
    else if (statbufp->st_mode & mode >> 6)
	return TRUE;	/* ok as "other" */
    {_Bool  ReplaceReturn = FALSE; SensorCall(); return ReplaceReturn;}
#endif /* ! DOSISH */
}
#endif /* ! VMS */

static bool
S_ingroup(pTHX_ Gid_t testgid, bool effective)
{
SensorCall();    dVAR;
    SensorCall();if (testgid == (effective ? PerlProc_getegid() : PerlProc_getgid()))
	return TRUE;
#ifdef HAS_GETGROUPS
    {
	Groups_t *gary = NULL;
	SensorCall();I32 anum;
        bool rc = FALSE;

	anum = getgroups(0, gary);
        Newx(gary, anum, Groups_t);
        anum = getgroups(anum, gary);
	SensorCall();while (--anum >= 0)
	    {/*189*/SensorCall();if (gary[anum] == testgid) {
                SensorCall();rc = TRUE;
                SensorCall();break;
            ;/*190*/}}

        Safefree(gary);
        {_Bool  ReplaceReturn = rc; SensorCall(); return ReplaceReturn;}
    }
#else
    return FALSE;
#endif
}

#if defined(HAS_MSG) || defined(HAS_SEM) || defined(HAS_SHM)

I32
Perl_do_ipcget(pTHX_ I32 optype, SV **mark, SV **sp)
{
SensorCall();    dVAR;
    const key_t key = (key_t)SvNVx(*++mark);
    SV *nsv = optype == OP_MSGGET ? NULL : *++mark;
    const I32 flags = SvIVx(*++mark);

    PERL_ARGS_ASSERT_DO_IPCGET;
    PERL_UNUSED_ARG(sp);

    SETERRNO(0,0);
    SensorCall();switch (optype)
    {
#ifdef HAS_MSG
    case OP_MSGGET:
	{I32  ReplaceReturn = msgget(key, flags); SensorCall(); return ReplaceReturn;}
#endif
#ifdef HAS_SEM
    case OP_SEMGET:
	{I32  ReplaceReturn = semget(key, (int) SvIV(nsv), flags); SensorCall(); return ReplaceReturn;}
#endif
#ifdef HAS_SHM
    case OP_SHMGET:
	{I32  ReplaceReturn = shmget(key, (size_t) SvUV(nsv), flags); SensorCall(); return ReplaceReturn;}
#endif
#if !defined(HAS_MSG) || !defined(HAS_SEM) || !defined(HAS_SHM)
    default:
        /* diag_listed_as: msg%s not implemented */
	Perl_croak(aTHX_ "%s not implemented", PL_op_desc[optype]);
#endif
    }
    {I32  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}			/* should never happen */
}

I32
Perl_do_ipcctl(pTHX_ I32 optype, SV **mark, SV **sp)
{
SensorCall();    dVAR;
    char *a;
    I32 ret = -1;
    const I32 id  = SvIVx(*++mark);
#ifdef Semctl
    const I32 n   = (optype == OP_SEMCTL) ? SvIVx(*++mark) : 0;
#endif
    const I32 cmd = SvIVx(*++mark);
    SV * const astr = *++mark;
    STRLEN infosize = 0;
    I32 getinfo = (cmd == IPC_STAT);

    PERL_ARGS_ASSERT_DO_IPCCTL;
    PERL_UNUSED_ARG(sp);

    SensorCall();switch (optype)
    {
#ifdef HAS_MSG
    case OP_MSGCTL:
	SensorCall();if (cmd == IPC_STAT || cmd == IPC_SET)
	    {/*169*/SensorCall();infosize = sizeof(struct msqid_ds);/*170*/}
	SensorCall();break;
#endif
#ifdef HAS_SHM
    case OP_SHMCTL:
	SensorCall();if (cmd == IPC_STAT || cmd == IPC_SET)
	    {/*171*/SensorCall();infosize = sizeof(struct shmid_ds);/*172*/}
	SensorCall();break;
#endif
#ifdef HAS_SEM
    case OP_SEMCTL:
#ifdef Semctl
	SensorCall();if (cmd == IPC_STAT || cmd == IPC_SET)
	    {/*173*/SensorCall();infosize = sizeof(struct semid_ds);/*174*/}
	else {/*175*/SensorCall();if (cmd == GETALL || cmd == SETALL)
	{
	    SensorCall();struct semid_ds semds;
	    union semun semun;
#ifdef EXTRA_F_IN_SEMUN_BUF
            semun.buff = &semds;
#else
            semun.buf = &semds;
#endif
	    getinfo = (cmd == GETALL);
	    SensorCall();if (Semctl(id, 0, IPC_STAT, semun) == -1)
		{/*177*/{I32  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}/*178*/}
	    SensorCall();infosize = semds.sem_nsems * sizeof(short);
		/* "short" is technically wrong but much more portable
		   than guessing about u_?short(_t)? */
	;/*176*/}}
#else
        /* diag_listed_as: sem%s not implemented */
	Perl_croak(aTHX_ "%s not implemented", PL_op_desc[optype]);
#endif
	SensorCall();break;
#endif
#if !defined(HAS_MSG) || !defined(HAS_SEM) || !defined(HAS_SHM)
    default:
        /* diag_listed_as: shm%s not implemented */
	Perl_croak(aTHX_ "%s not implemented", PL_op_desc[optype]);
#endif
    }

    SensorCall();if (infosize)
    {
	SensorCall();if (getinfo)
	{
	    SvPV_force_nolen(astr);
	    SensorCall();a = SvGROW(astr, infosize+1);
	}
	else
	{
	    SensorCall();STRLEN len;
	    a = SvPV(astr, len);
	    SensorCall();if (len != infosize)
		{/*179*/SensorCall();Perl_croak(aTHX_ "Bad arg length for %s, is %lu, should be %ld",
		      PL_op_desc[optype],
		      (unsigned long)len,
		      (long)infosize);/*180*/}
	}
    }
    else
    {
	SensorCall();const IV i = SvIV(astr);
	a = INT2PTR(char *,i);		/* ouch */
    }
    SETERRNO(0,0);
    SensorCall();switch (optype)
    {
#ifdef HAS_MSG
    case OP_MSGCTL:
	SensorCall();ret = msgctl(id, cmd, (struct msqid_ds *)a);
	SensorCall();break;
#endif
#ifdef HAS_SEM
    case OP_SEMCTL: {
#ifdef Semctl
            SensorCall();union semun unsemds;

#ifdef EXTRA_F_IN_SEMUN_BUF
            unsemds.buff = (struct semid_ds *)a;
#else
            unsemds.buf = (struct semid_ds *)a;
#endif
	    ret = Semctl(id, n, cmd, unsemds);
#else
	    /* diag_listed_as: sem%s not implemented */
	    Perl_croak(aTHX_ "%s not implemented", PL_op_desc[optype]);
#endif
        }
	SensorCall();break;
#endif
#ifdef HAS_SHM
    case OP_SHMCTL:
	SensorCall();ret = shmctl(id, cmd, (struct shmid_ds *)a);
	SensorCall();break;
#endif
    }
    SensorCall();if (getinfo && ret >= 0) {
	SvCUR_set(astr, infosize);
	SensorCall();*SvEND(astr) = '\0';
	SvSETMAGIC(astr);
    }
    {I32  ReplaceReturn = ret; SensorCall(); return ReplaceReturn;}
}

I32
Perl_do_msgsnd(pTHX_ SV **mark, SV **sp)
{
SensorCall();    dVAR;
#ifdef HAS_MSG
    STRLEN len;
    const I32 id = SvIVx(*++mark);
    SV * const mstr = *++mark;
    const I32 flags = SvIVx(*++mark);
    const char * const mbuf = SvPV_const(mstr, len);
    const I32 msize = len - sizeof(long);

    PERL_ARGS_ASSERT_DO_MSGSND;
    PERL_UNUSED_ARG(sp);

    SensorCall();if (msize < 0)
	{/*181*/SensorCall();Perl_croak(aTHX_ "Arg too short for msgsnd");/*182*/}
    SETERRNO(0,0);
    {I32  ReplaceReturn = msgsnd(id, (struct msgbuf *)mbuf, msize, flags); SensorCall(); return ReplaceReturn;}
#else
    PERL_UNUSED_ARG(sp);
    PERL_UNUSED_ARG(mark);
    /* diag_listed_as: msg%s not implemented */
    Perl_croak(aTHX_ "msgsnd not implemented");
#endif
}

I32
Perl_do_msgrcv(pTHX_ SV **mark, SV **sp)
{
SensorCall();
#ifdef HAS_MSG
    dVAR;
    char *mbuf;
    long mtype;
    I32 msize, flags, ret;
    const I32 id = SvIVx(*++mark);
    SV * const mstr = *++mark;

    PERL_ARGS_ASSERT_DO_MSGRCV;
    PERL_UNUSED_ARG(sp);

    /* suppress warning when reading into undef var --jhi */
    SensorCall();if (! SvOK(mstr))
	sv_setpvs(mstr, "");
    SensorCall();msize = SvIVx(*++mark);
    mtype = (long)SvIVx(*++mark);
    flags = SvIVx(*++mark);
    SvPV_force_nolen(mstr);
    mbuf = SvGROW(mstr, sizeof(long)+msize+1);

    SETERRNO(0,0);
    ret = msgrcv(id, (struct msgbuf *)mbuf, msize, mtype, flags);
    SensorCall();if (ret >= 0) {
	SvCUR_set(mstr, sizeof(long)+ret);
	SensorCall();*SvEND(mstr) = '\0';
#ifndef INCOMPLETE_TAINTS
	/* who knows who has been playing with this message? */
	SvTAINTED_on(mstr);
#endif
    }
    {I32  ReplaceReturn = ret; SensorCall(); return ReplaceReturn;}
#else
    PERL_UNUSED_ARG(sp);
    PERL_UNUSED_ARG(mark);
    /* diag_listed_as: msg%s not implemented */
    Perl_croak(aTHX_ "msgrcv not implemented");
#endif
}

I32
Perl_do_semop(pTHX_ SV **mark, SV **sp)
{
SensorCall();
#ifdef HAS_SEM
    dVAR;
    STRLEN opsize;
    const I32 id = SvIVx(*++mark);
    SV * const opstr = *++mark;
    const char * const opbuf = SvPV_const(opstr, opsize);

    PERL_ARGS_ASSERT_DO_SEMOP;
    PERL_UNUSED_ARG(sp);

    SensorCall();if (opsize < 3 * SHORTSIZE
	|| (opsize % (3 * SHORTSIZE))) {
	SETERRNO(EINVAL,LIB_INVARG);
	{I32  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}
    }
    SETERRNO(0,0);
    /* We can't assume that sizeof(struct sembuf) == 3 * sizeof(short). */
    {
        SensorCall();const int nsops  = opsize / (3 * sizeof (short));
        int i      = nsops;
        short * const ops = (short *) opbuf;
        short *o   = ops;
        struct sembuf *temps, *t;
        I32 result;

        Newx (temps, nsops, struct sembuf);
        t = temps;
        SensorCall();while (i--) {
            SensorCall();t->sem_num = *o++;
            t->sem_op  = *o++;
            t->sem_flg = *o++;
            t++;
        }
        SensorCall();result = semop(id, temps, nsops);
        t = temps;
        o = ops;
        i = nsops;
        SensorCall();while (i--) {
            SensorCall();*o++ = t->sem_num;
            *o++ = t->sem_op;
            *o++ = t->sem_flg;
            t++;
        }
        Safefree(temps);
        {I32  ReplaceReturn = result; SensorCall(); return ReplaceReturn;}
    }
#else
    /* diag_listed_as: sem%s not implemented */
    Perl_croak(aTHX_ "semop not implemented");
#endif
}

I32
Perl_do_shmio(pTHX_ I32 optype, SV **mark, SV **sp)
{
SensorCall();
#ifdef HAS_SHM
    dVAR;
    char *shm;
    struct shmid_ds shmds;
    const I32 id = SvIVx(*++mark);
    SV * const mstr = *++mark;
    const I32 mpos = SvIVx(*++mark);
    const I32 msize = SvIVx(*++mark);

    PERL_ARGS_ASSERT_DO_SHMIO;
    PERL_UNUSED_ARG(sp);

    SETERRNO(0,0);
    SensorCall();if (shmctl(id, IPC_STAT, &shmds) == -1)
	{/*183*/{I32  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}/*184*/}
    SensorCall();if (mpos < 0 || msize < 0
	|| (size_t)mpos + msize > (size_t)shmds.shm_segsz) {
	SETERRNO(EFAULT,SS_ACCVIO);		/* can't do as caller requested */
	{I32  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}
    }
    SensorCall();shm = (char *)shmat(id, NULL, (optype == OP_SHMREAD) ? SHM_RDONLY : 0);
    SensorCall();if (shm == (char *)-1)	/* I hate System V IPC, I really do */
	{/*185*/{I32  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}/*186*/}
    SensorCall();if (optype == OP_SHMREAD) {
	SensorCall();char *mbuf;
	/* suppress warning when reading into undef var (tchrist 3/Mar/00) */
	SensorCall();if (! SvOK(mstr))
	    sv_setpvs(mstr, "");
	SvUPGRADE(mstr, SVt_PV);
	SvPOK_only(mstr);
	SensorCall();mbuf = SvGROW(mstr, (STRLEN)msize+1);

	Copy(shm + mpos, mbuf, msize, char);
	SvCUR_set(mstr, msize);
	*SvEND(mstr) = '\0';
	SvSETMAGIC(mstr);
#ifndef INCOMPLETE_TAINTS
	/* who knows who has been playing with this shared memory? */
	SvTAINTED_on(mstr);
#endif
    }
    else {
	SensorCall();STRLEN len;

	const char *mbuf = SvPV_const(mstr, len);
	const I32 n = ((I32)len > msize) ? msize : (I32)len;
	Copy(mbuf, shm + mpos, n, char);
	SensorCall();if (n < msize)
	    memzero(shm + mpos + n, msize - n);
    }
    {I32  ReplaceReturn = shmdt(shm); SensorCall(); return ReplaceReturn;}
#else
    /* diag_listed_as: shm%s not implemented */
    Perl_croak(aTHX_ "shm I/O not implemented");
#endif
}

#endif /* SYSV IPC */

/*
=head1 IO Functions

=for apidoc start_glob

Function called by C<do_readline> to spawn a glob (or do the glob inside
perl on VMS). This code used to be inline, but now perl uses C<File::Glob>
this glob starter is only used by miniperl during the build process.
Moving it away shrinks pp_hot.c; shrinking pp_hot.c helps speed perl up.

=cut
*/

PerlIO *
Perl_start_glob (pTHX_ SV *tmpglob, IO *io)
{
SensorCall();    dVAR;
    SV * const tmpcmd = newSV(0);
    PerlIO *fp;

    PERL_ARGS_ASSERT_START_GLOB;

    ENTER;
    SAVEFREESV(tmpcmd);
#ifdef VMS /* expand the wildcards right here, rather than opening a pipe, */
           /* since spawning off a process is a real performance hit */

PerlIO * 
Perl_vms_start_glob
   (pTHX_ SV *tmpglob,
    IO *io);

    fp = Perl_vms_start_glob(aTHX_ tmpglob, io);

#else /* !VMS */
#ifdef DOSISH
#ifdef OS2
    sv_setpv(tmpcmd, "for a in ");
    sv_catsv(tmpcmd, tmpglob);
    sv_catpv(tmpcmd, "; do echo \"$a\\0\\c\"; done |");
#else
#ifdef DJGPP
    sv_setpv(tmpcmd, "/dev/dosglob/"); /* File System Extension */
    sv_catsv(tmpcmd, tmpglob);
#else
    sv_setpv(tmpcmd, "perlglob ");
    sv_catsv(tmpcmd, tmpglob);
    sv_catpv(tmpcmd, " |");
#endif /* !DJGPP */
#endif /* !OS2 */
#else /* !DOSISH */
#if defined(CSH)
    sv_setpvn(tmpcmd, PL_cshname, PL_cshlen);
    sv_catpv(tmpcmd, " -cf 'set nonomatch; glob ");
    sv_catsv(tmpcmd, tmpglob);
    sv_catpv(tmpcmd, "' 2>/dev/null |");
#else
    sv_setpv(tmpcmd, "echo ");
    sv_catsv(tmpcmd, tmpglob);
#if 'z' - 'a' == 25
    sv_catpv(tmpcmd, "|tr -s ' \t\f\r' '\\012\\012\\012\\012'|");
#else
    sv_catpv(tmpcmd, "|tr -s ' \t\f\r' '\\n\\n\\n\\n'|");
#endif
#endif /* !CSH */
#endif /* !DOSISH */
    {
	GV * const envgv = gv_fetchpvs("ENV", 0, SVt_PVHV);
	SV ** const home = hv_fetchs(GvHV(envgv), "HOME", 0);
	SV ** const path = hv_fetchs(GvHV(envgv), "PATH", 0);
	SensorCall();if (home && *home) SvGETMAGIC(*home);
	SensorCall();if (path && *path) SvGETMAGIC(*path);
	save_hash(gv_fetchpvs("ENV", 0, SVt_PVHV));
	SensorCall();if (home && *home) SvSETMAGIC(*home);
	SensorCall();if (path && *path) SvSETMAGIC(*path);
    }
    SensorCall();(void)do_open(PL_last_in_gv, (char*)SvPVX_const(tmpcmd), SvCUR(tmpcmd),
		  FALSE, O_RDONLY, 0, NULL);
    fp = IoIFP(io);
#endif /* !VMS */
    LEAVE;
    {PerlIO * ReplaceReturn = fp; SensorCall(); return ReplaceReturn;}
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 *
 * ex: set ts=8 sts=4 sw=4 noet:
 */
