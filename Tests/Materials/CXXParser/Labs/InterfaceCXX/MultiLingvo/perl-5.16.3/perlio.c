#include "var/tmp/sensor.h"
/*
 * perlio.c
 * Copyright (c) 1996-2006, Nick Ing-Simmons
 * Copyright (c) 2006, 2007, 2008, 2009, 2010, 2011 Larry Wall and others
 *
 * You may distribute under the terms of either the GNU General Public License
 * or the Artistic License, as specified in the README file.
 */

/*
 * Hour after hour for nearly three weary days he had jogged up and down,
 * over passes, and through long dales, and across many streams.
 *
 *     [pp.791-792 of _The Lord of the Rings_, V/iii: "The Muster of Rohan"]
 */

/* This file contains the functions needed to implement PerlIO, which
 * is Perl's private replacement for the C stdio library. This is used
 * by default unless you compile with -Uuseperlio or run with
 * PERLIO=:stdio (but don't do this unless you know what you're doing)
 */

/*
 * If we have ActivePerl-like PERL_IMPLICIT_SYS then we need a dTHX to get
 * at the dispatch tables, even when we do not need it for other reasons.
 * Invent a dSYS macro to abstract this out
 */
#ifdef PERL_IMPLICIT_SYS
#define dSYS dTHX
#else
#define dSYS dNOOP
#endif

#define VOIDUSED 1
#ifdef PERL_MICRO
#   include "uconfig.h"
#else
#   ifndef USE_CROSS_COMPILE
#       include "config.h"
#   else
#       include "xconfig.h"
#   endif
#endif

#define PERLIO_NOT_STDIO 0
#if !defined(PERLIO_IS_STDIO) && !defined(USE_SFIO)
/*
 * #define PerlIO FILE
 */
#endif
/*
 * This file provides those parts of PerlIO abstraction
 * which are not #defined in perlio.h.
 * Which these are depends on various Configure #ifdef's
 */

#include "EXTERN.h"
#define PERL_IN_PERLIO_C
#include "perl.h"

#ifdef PERL_IMPLICIT_CONTEXT
#undef dSYS
#define dSYS dTHX
#endif

#include "XSUB.h"

#ifdef __Lynx__
/* Missing proto on LynxOS */
int mkstemp(char*);
#endif

#define PerlIO_lockcnt(f) (((PerlIOl*)(f))->head->flags)

/* Call the callback or PerlIOBase, and return failure. */
#define Perl_PerlIO_or_Base(f, callback, base, failure, args) 	\
	if (PerlIOValid(f)) {					\
		const PerlIO_funcs * const tab = PerlIOBase(f)->tab;\
		if (tab && tab->callback)			\
			return (*tab->callback) args;		\
		else						\
			return PerlIOBase_ ## base args;	\
	}							\
	else							\
		SETERRNO(EBADF, SS_IVCHAN);			\
	return failure

/* Call the callback or fail, and return failure. */
#define Perl_PerlIO_or_fail(f, callback, failure, args) 	\
	if (PerlIOValid(f)) {					\
		const PerlIO_funcs * const tab = PerlIOBase(f)->tab;\
		if (tab && tab->callback)			\
			return (*tab->callback) args;		\
		SETERRNO(EINVAL, LIB_INVARG);			\
	}							\
	else							\
		SETERRNO(EBADF, SS_IVCHAN);			\
	return failure

/* Call the callback or PerlIOBase, and be void. */
#define Perl_PerlIO_or_Base_void(f, callback, base, args) 	\
	if (PerlIOValid(f)) {					\
		const PerlIO_funcs * const tab = PerlIOBase(f)->tab;\
		if (tab && tab->callback)			\
			(*tab->callback) args;			\
		else						\
			PerlIOBase_ ## base args;		\
	}							\
	else							\
		SETERRNO(EBADF, SS_IVCHAN)

/* Call the callback or fail, and be void. */
#define Perl_PerlIO_or_fail_void(f, callback, args) 		\
	if (PerlIOValid(f)) {					\
		const PerlIO_funcs * const tab = PerlIOBase(f)->tab;\
		if (tab && tab->callback)			\
			(*tab->callback) args;			\
		else						\
			SETERRNO(EINVAL, LIB_INVARG);		\
	}							\
	else							\
		SETERRNO(EBADF, SS_IVCHAN)

#if defined(__osf__) && _XOPEN_SOURCE < 500
extern int   fseeko(FILE *, off_t, int);
extern off_t ftello(FILE *);
#endif

#ifndef USE_SFIO

EXTERN_C int perlsio_binmode(FILE *fp, int iotype, int mode);

int
perlsio_binmode(FILE *fp, int iotype, int mode)
{
    /*
     * This used to be contents of do_binmode in doio.c
     */
#ifdef DOSISH
#  if defined(atarist)
    PERL_UNUSED_ARG(iotype);
    if (!fflush(fp)) {
        if (mode & O_BINARY)
            ((FILE *) fp)->_flag |= _IOBIN;
        else
            ((FILE *) fp)->_flag &= ~_IOBIN;
        return 1;
    }
    return 0;
#  else
    dTHX;
    PERL_UNUSED_ARG(iotype);
#ifdef NETWARE
    if (PerlLIO_setmode(fp, mode) != -1) {
#else
    if (PerlLIO_setmode(fileno(fp), mode) != -1) {
#endif
        return 1;
    }
    else
        return 0;
#  endif
#else
#  if defined(USEMYBINMODE)
    dTHX;
#    if defined(__CYGWIN__)
    PERL_UNUSED_ARG(iotype);
#    endif
    if (my_binmode(fp, iotype, mode) != FALSE)
        return 1;
    else
        return 0;
#  else
    PERL_UNUSED_ARG(fp);
    PERL_UNUSED_ARG(iotype);
    PERL_UNUSED_ARG(mode);
    {int  ReplaceReturn1574 = 1; SensorCall(10075); return ReplaceReturn1574;}
#  endif
#endif
}
#endif /* sfio */

#ifndef O_ACCMODE
#define O_ACCMODE 3             /* Assume traditional implementation */
#endif

int
PerlIO_intmode2str(int rawmode, char *mode, int *writing)
{
    SensorCall(10076);const int result = rawmode & O_ACCMODE;
    int ix = 0;
    int ptype;
    SensorCall(10083);switch (result) {
    case O_RDONLY:
	SensorCall(10077);ptype = IoTYPE_RDONLY;
	SensorCall(10078);break;
    case O_WRONLY:
	SensorCall(10079);ptype = IoTYPE_WRONLY;
	SensorCall(10080);break;
    case O_RDWR:
    default:
	SensorCall(10081);ptype = IoTYPE_RDWR;
	SensorCall(10082);break;
    }
    SensorCall(10085);if (writing)
	{/*39*/SensorCall(10084);*writing = (result != O_RDONLY);/*40*/}

    SensorCall(10094);if (result == O_RDONLY) {
	SensorCall(10086);mode[ix++] = 'r';
    }
#ifdef O_APPEND
    else {/*41*/SensorCall(10087);if (rawmode & O_APPEND) {
	SensorCall(10088);mode[ix++] = 'a';
	SensorCall(10090);if (result != O_WRONLY)
	    {/*43*/SensorCall(10089);mode[ix++] = '+';/*44*/}
    }
#endif
    else {
	SensorCall(10091);if (result == O_WRONLY)
	    {/*45*/SensorCall(10092);mode[ix++] = 'w';/*46*/}
	else {
	    SensorCall(10093);mode[ix++] = 'r';
	    mode[ix++] = '+';
	}
    ;/*42*/}}
    SensorCall(10096);if (rawmode & O_BINARY)
	{/*47*/SensorCall(10095);mode[ix++] = 'b';/*48*/}
    SensorCall(10097);mode[ix] = '\0';
    {int  ReplaceReturn1573 = ptype; SensorCall(10098); return ReplaceReturn1573;}
}

#ifndef PERLIO_LAYERS
int
PerlIO_apply_layers(pTHX_ PerlIO *f, const char *mode, const char *names)
{
    if (!names || !*names
        || strEQ(names, ":crlf")
        || strEQ(names, ":raw")
        || strEQ(names, ":bytes")
       ) {
	return 0;
    }
    Perl_croak(aTHX_ "Cannot apply \"%s\" in non-PerlIO perl", names);
    /*
     * NOTREACHED
     */
    return -1;
}

void
PerlIO_destruct(pTHX)
{
}

int
PerlIO_binmode(pTHX_ PerlIO *fp, int iotype, int mode, const char *names)
{
#ifdef USE_SFIO
    PERL_UNUSED_ARG(iotype);
    PERL_UNUSED_ARG(mode);
    PERL_UNUSED_ARG(names);
    return 1;
#else
    return perlsio_binmode(fp, iotype, mode);
#endif
}

PerlIO *
PerlIO_fdupopen(pTHX_ PerlIO *f, CLONE_PARAMS *param, int flags)
{
#if defined(PERL_MICRO) || defined(__SYMBIAN32__)
    return NULL;
#else
#ifdef PERL_IMPLICIT_SYS
    return PerlSIO_fdupopen(f);
#else
#ifdef WIN32
    return win32_fdupopen(f);
#else
    if (f) {
	const int fd = PerlLIO_dup(PerlIO_fileno(f));
	if (fd >= 0) {
	    char mode[8];
#ifdef DJGPP
	    const int omode = djgpp_get_stream_mode(f);
#else
	    const int omode = fcntl(fd, F_GETFL);
#endif
	    PerlIO_intmode2str(omode,mode,NULL);
	    /* the r+ is a hack */
	    return PerlIO_fdopen(fd, mode);
	}
	return NULL;
    }
    else {
	SETERRNO(EBADF, SS_IVCHAN);
    }
#endif
    return NULL;
#endif
#endif
}


/*
 * De-mux PerlIO_openn() into fdopen, freopen and fopen type entries
 */

PerlIO *
PerlIO_openn(pTHX_ const char *layers, const char *mode, int fd,
	     int imode, int perm, PerlIO *old, int narg, SV **args)
{
    if (narg) {
	if (narg > 1) {
	    Perl_croak(aTHX_ "More than one argument to open");
	}
	if (*args == &PL_sv_undef)
	    return PerlIO_tmpfile();
	else {
	    const char *name = SvPV_nolen_const(*args);
	    if (*mode == IoTYPE_NUMERIC) {
		fd = PerlLIO_open3(name, imode, perm);
		if (fd >= 0)
		    return PerlIO_fdopen(fd, mode + 1);
	    }
	    else if (old) {
		return PerlIO_reopen(name, mode, old);
	    }
	    else {
		return PerlIO_open(name, mode);
	    }
	}
    }
    else {
	return PerlIO_fdopen(fd, (char *) mode);
    }
    return NULL;
}

XS(XS_PerlIO__Layer__find)
{
    dXSARGS;
    if (items < 2)
	Perl_croak(aTHX_ "Usage class->find(name[,load])");
    else {
	const char * const name = SvPV_nolen_const(ST(1));
	ST(0) = (strEQ(name, "crlf")
		 || strEQ(name, "raw")) ? &PL_sv_yes : &PL_sv_undef;
	XSRETURN(1);
    }
}


void
Perl_boot_core_PerlIO(pTHX)
{
    newXS("PerlIO::Layer::find", XS_PerlIO__Layer__find, __FILE__);
}

#endif


#ifdef PERLIO_IS_STDIO

void
PerlIO_init(pTHX)
{
    PERL_UNUSED_CONTEXT;
    /*
     * Does nothing (yet) except force this file to be included in perl
     * binary. That allows this file to force inclusion of other functions
     * that may be required by loadable extensions e.g. for
     * FileHandle::tmpfile
     */
}

#undef PerlIO_tmpfile
PerlIO *
PerlIO_tmpfile(void)
{
    return tmpfile();
}

#else                           /* PERLIO_IS_STDIO */

#ifdef USE_SFIO

#undef HAS_FSETPOS
#undef HAS_FGETPOS

/*
 * This section is just to make sure these functions get pulled in from
 * libsfio.a
 */

#undef PerlIO_tmpfile
PerlIO *
PerlIO_tmpfile(void)
{
    return sftmp(0);
}

void
PerlIO_init(pTHX)
{
    PERL_UNUSED_CONTEXT;
    /*
     * Force this file to be included in perl binary. Which allows this
     * file to force inclusion of other functions that may be required by
     * loadable extensions e.g. for FileHandle::tmpfile
     */

    /*
     * Hack sfio does its own 'autoflush' on stdout in common cases. Flush
     * results in a lot of lseek()s to regular files and lot of small
     * writes to pipes.
     */
    sfset(sfstdout, SF_SHARE, 0);
}

/* This is not the reverse of PerlIO_exportFILE(), PerlIO_releaseFILE() is. */
PerlIO *
PerlIO_importFILE(FILE *stdio, const char *mode)
{
    const int fd = fileno(stdio);
    if (!mode || !*mode) {
	mode = "r+";
    }
    return PerlIO_fdopen(fd, mode);
}

FILE *
PerlIO_findFILE(PerlIO *pio)
{
    const int fd = PerlIO_fileno(pio);
    FILE * const f = fdopen(fd, "r+");
    PerlIO_flush(pio);
    if (!f && errno == EINVAL)
	f = fdopen(fd, "w");
    if (!f && errno == EINVAL)
	f = fdopen(fd, "r");
    return f;
}


#else                           /* USE_SFIO */
/*======================================================================================*/
/*
 * Implement all the PerlIO interface ourselves.
 */

#include "perliol.h"

void
PerlIO_debug(const char *fmt, ...)
{
    SensorCall(10099);va_list ap;
    dSYS;
    va_start(ap, fmt);
    SensorCall(10105);if (!PL_perlio_debug_fd) {
	SensorCall(10100);if (!PL_tainting &&
	    PerlProc_getuid() == PerlProc_geteuid() &&
	    PerlProc_getgid() == PerlProc_getegid()) {
	    SensorCall(10101);const char * const s = PerlEnv_getenv("PERLIO_DEBUG");
	    SensorCall(10103);if (s && *s)
		PL_perlio_debug_fd
		    = PerlLIO_open3(s, O_WRONLY | O_CREAT | O_APPEND, 0666);
	    else
		{/*53*/SensorCall(10102);PL_perlio_debug_fd = -1;/*54*/}
	} else {
	    /* tainting or set*id, so ignore the environment, and ensure we
	       skip these tests next time through.  */
	    SensorCall(10104);PL_perlio_debug_fd = -1;
	}
    }
    SensorCall(10107);if (PL_perlio_debug_fd > 0) {
	dTHX;
#ifdef USE_ITHREADS
	SensorCall(10106);const char * const s = CopFILE(PL_curcop);
	/* Use fixed buffer as sv_catpvf etc. needs SVs */
	char buffer[1024];
	const STRLEN len1 = my_snprintf(buffer, sizeof(buffer), "%.40s:%" IVdf " ", s ? s : "(none)", (IV) CopLINE(PL_curcop));
	const STRLEN len2 = my_vsnprintf(buffer + len1, sizeof(buffer) - len1, fmt, ap);
	PerlLIO_write(PL_perlio_debug_fd, buffer, len1 + len2);
#else
	const char *s = CopFILE(PL_curcop);
	STRLEN len;
	SV * const sv = Perl_newSVpvf(aTHX_ "%s:%" IVdf " ", s ? s : "(none)",
				      (IV) CopLINE(PL_curcop));
	Perl_sv_vcatpvf(aTHX_ sv, fmt, &ap);

	s = SvPV_const(sv, len);
	PerlLIO_write(PL_perlio_debug_fd, s, len);
	SvREFCNT_dec(sv);
#endif
    }
    va_end(ap);
}

/*--------------------------------------------------------------------------------------*/

/*
 * Inner level routines
 */

/* check that the head field of each layer points back to the head */

#ifdef DEBUGGING
#  define VERIFY_HEAD(f) PerlIO_verify_head(aTHX_ f)
static void
PerlIO_verify_head(pTHX_ PerlIO *f)
{
    PerlIOl *head, *p;
    int seen = 0;
    if (!PerlIOValid(f))
	return;
    p = head = PerlIOBase(f)->head;
    assert(p);
    do {
	assert(p->head == head);
	if (p == (PerlIOl*)f)
	    seen = 1;
	p = p->next;
    } while (p);
    assert(seen);
}
#else
#  define VERIFY_HEAD(f)
#endif


/*
 * Table of pointers to the PerlIO structs (malloc'ed)
 */
#define PERLIO_TABLE_SIZE 64

static void
PerlIO_init_table(pTHX)
{
    SensorCall(10108);if (PL_perlio)
	{/*243*/SensorCall(10109);return;/*244*/}
    Newxz(PL_perlio, PERLIO_TABLE_SIZE, PerlIOl);
}



PerlIO *
PerlIO_allocate(pTHX)
{
SensorCall(10110);    dVAR;
    /*
     * Find a free slot in the table, allocating new table as necessary
     */
    PerlIOl **last;
    PerlIOl *f;
    last = &PL_perlio;
    SensorCall(10116);while ((f = *last)) {
	SensorCall(10111);int i;
	last = (PerlIOl **) (f);
	SensorCall(10115);for (i = 1; i < PERLIO_TABLE_SIZE; i++) {
	    SensorCall(10112);if (!((++f)->next)) {
		SensorCall(10113);f->flags = 0; /* lockcnt */
		f->tab = NULL;
		f->head = f;
		{PerlIO * ReplaceReturn1572 = (PerlIO *)f; SensorCall(10114); return ReplaceReturn1572;}
	    }
	}
    }
    Newxz(f,PERLIO_TABLE_SIZE,PerlIOl);
    SensorCall(10118);if (!f) {
	{PerlIO * ReplaceReturn1571 = NULL; SensorCall(10117); return ReplaceReturn1571;}
    }
    SensorCall(10119);*last = (PerlIOl*) f++;
    f->flags = 0; /* lockcnt */
    f->tab = NULL;
    f->head = f;
    {PerlIO * ReplaceReturn1570 = (PerlIO*) f; SensorCall(10120); return ReplaceReturn1570;}
}

#undef PerlIO_fdupopen
PerlIO *
PerlIO_fdupopen(pTHX_ PerlIO *f, CLONE_PARAMS *param, int flags)
{
    SensorCall(10121);if (PerlIOValid(f)) {
	SensorCall(10122);const PerlIO_funcs * const tab = PerlIOBase(f)->tab;
	PerlIO_debug("fdupopen f=%p param=%p\n",(void*)f,(void*)param);
	SensorCall(10125);if (tab && tab->Dup)
	     {/*29*/{PerlIO * ReplaceReturn1569 = (*tab->Dup)(aTHX_ PerlIO_allocate(aTHX), f, param, flags); SensorCall(10123); return ReplaceReturn1569;}/*30*/}
	else {
	     {PerlIO * ReplaceReturn1568 = PerlIOBase_dup(aTHX_ PerlIO_allocate(aTHX), f, param, flags); SensorCall(10124); return ReplaceReturn1568;}
	}
    }
    else
	 SETERRNO(EBADF, SS_IVCHAN);

    {PerlIO * ReplaceReturn1567 = NULL; SensorCall(10126); return ReplaceReturn1567;}
}

void
PerlIO_cleantable(pTHX_ PerlIOl **tablep)
{
    SensorCall(10127);PerlIOl * const table = *tablep;
    SensorCall(10133);if (table) {
	SensorCall(10128);int i;
	PerlIO_cleantable(aTHX_(PerlIOl **) & (table[0]));
	SensorCall(10131);for (i = PERLIO_TABLE_SIZE - 1; i > 0; i--) {
	    SensorCall(10129);PerlIOl * const f = table + i;
	    SensorCall(10130);if (f->next) {
		PerlIO_close(&(f->next));
	    }
	}
	Safefree(table);
	SensorCall(10132);*tablep = NULL;
    }
SensorCall(10134);}


PerlIO_list_t *
PerlIO_list_alloc(pTHX)
{
    SensorCall(10135);PerlIO_list_t *list;
    PERL_UNUSED_CONTEXT;
    Newxz(list, 1, PerlIO_list_t);
    list->refcnt = 1;
    {PerlIO_list_t * ReplaceReturn1566 = list; SensorCall(10136); return ReplaceReturn1566;}
}

void
PerlIO_list_free(pTHX_ PerlIO_list_t *list)
{
    SensorCall(10137);if (list) {
	SensorCall(10138);if (--list->refcnt == 0) {
	    SensorCall(10139);if (list->array) {
		SensorCall(10140);IV i;
		SensorCall(10141);for (i = 0; i < list->cur; i++)
		    SvREFCNT_dec(list->array[i].arg);
		Safefree(list->array);
	    }
	    Safefree(list);
	}
    }
SensorCall(10142);}

void
PerlIO_list_push(pTHX_ PerlIO_list_t *list, PerlIO_funcs *funcs, SV *arg)
{
SensorCall(10143);    dVAR;
    PerlIO_pair_t *p;
    PERL_UNUSED_CONTEXT;

    SensorCall(10146);if (list->cur >= list->len) {
	SensorCall(10144);list->len += 8;
	SensorCall(10145);if (list->array)
	    Renew(list->array, list->len, PerlIO_pair_t);
	else
	    Newx(list->array, list->len, PerlIO_pair_t);
    }
    SensorCall(10147);p = &(list->array[list->cur++]);
    p->funcs = funcs;
    SensorCall(10148);if ((p->arg = arg)) {
	SvREFCNT_inc_simple_void_NN(arg);
    }
SensorCall(10149);}

PerlIO_list_t *
PerlIO_clone_list(pTHX_ PerlIO_list_t *proto, CLONE_PARAMS *param)
{
    SensorCall(10150);PerlIO_list_t *list = NULL;
    SensorCall(10156);if (proto) {
	SensorCall(10151);int i;
	list = PerlIO_list_alloc(aTHX);
	SensorCall(10155);for (i=0; i < proto->cur; i++) {
	    SensorCall(10152);SV *arg = proto->array[i].arg;
#ifdef sv_dup
	    SensorCall(10153);if (arg && param)
		arg = sv_dup(arg, param);
#else
	    PERL_UNUSED_ARG(param);
#endif
	    SensorCall(10154);PerlIO_list_push(aTHX_ list, proto->array[i].funcs, arg);
	}
    }
    {PerlIO_list_t * ReplaceReturn1565 = list; SensorCall(10157); return ReplaceReturn1565;}
}

void
PerlIO_clone(pTHX_ PerlInterpreter *proto, CLONE_PARAMS *param)
{
#ifdef USE_ITHREADS
    SensorCall(10158);PerlIOl **table = &proto->Iperlio;
    PerlIOl *f;
    PL_perlio = NULL;
    PL_known_layers = PerlIO_clone_list(aTHX_ proto->Iknown_layers, param);
    PL_def_layerlist = PerlIO_clone_list(aTHX_ proto->Idef_layerlist, param);
    PerlIO_init_table(aTHX);
    PerlIO_debug("Clone %p from %p\n",(void*)aTHX,(void*)proto);
    SensorCall(10164);while ((f = *table)) {
	    SensorCall(10159);int i;
	    table = (PerlIOl **) (f++);
	    SensorCall(10163);for (i = 1; i < PERLIO_TABLE_SIZE; i++) {
		SensorCall(10160);if (f->next) {
		    SensorCall(10161);(void) fp_dup(&(f->next), 0, param);
		}
		SensorCall(10162);f++;
	    }
	}
#else
    PERL_UNUSED_CONTEXT;
    PERL_UNUSED_ARG(proto);
    PERL_UNUSED_ARG(param);
#endif
SensorCall(10165);}

void
PerlIO_destruct(pTHX)
{
SensorCall(10166);    dVAR;
    PerlIOl **table = &PL_perlio;
    PerlIOl *f;
#ifdef USE_ITHREADS
    PerlIO_debug("Destruct %p\n",(void*)aTHX);
#endif
    SensorCall(10175);while ((f = *table)) {
	SensorCall(10167);int i;
	table = (PerlIOl **) (f++);
	SensorCall(10174);for (i = 1; i < PERLIO_TABLE_SIZE; i++) {
SensorCall(10168);	    PerlIO *x = &(f->next);
	    const PerlIOl *l;
	    SensorCall(10172);while ((l = *x)) {
		SensorCall(10169);if (l->tab && l->tab->kind & PERLIO_K_DESTRUCT) {
		    SensorCall(10170);PerlIO_debug("Destruct popping %s\n", l->tab->name);
		    PerlIO_flush(x);
		    PerlIO_pop(aTHX_ x);
		}
		else {
		    SensorCall(10171);x = PerlIONext(x);
		}
	    }
	    SensorCall(10173);f++;
	}
    }
SensorCall(10176);}

void
PerlIO_pop(pTHX_ PerlIO *f)
{
    SensorCall(10177);const PerlIOl *l = *f;
    VERIFY_HEAD(f);
    SensorCall(10184);if (l) {
	SensorCall(10178);PerlIO_debug("PerlIO_pop f=%p %s\n", (void*)f,
			    l->tab ? l->tab->name : "(Null)");
	SensorCall(10181);if (l->tab && l->tab->Popped) {
	    /*
	     * If popped returns non-zero do not free its layer structure
	     * it has either done so itself, or it is shared and still in
	     * use
	     */
	    SensorCall(10179);if ((*l->tab->Popped) (aTHX_ f) != 0)
		{/*5*/SensorCall(10180);return;/*6*/}
	}
	SensorCall(10183);if (PerlIO_lockcnt(f)) {
	    /* we're in use; defer freeing the structure */
	    PerlIOBase(f)->flags = PERLIO_F_CLEARED;
	    PerlIOBase(f)->tab = NULL;
	}
	else {
	    SensorCall(10182);*f = l->next;
	    Safefree(l);
	}

    }
SensorCall(10185);}

/* Return as an array the stack of layers on a filehandle.  Note that
 * the stack is returned top-first in the array, and there are three
 * times as many array elements as there are layers in the stack: the
 * first element of a layer triplet is the name, the second one is the
 * arguments, and the third one is the flags. */

AV *
PerlIO_get_layers(pTHX_ PerlIO *f)
{
SensorCall(10186);    dVAR;
    AV * const av = newAV();

    SensorCall(10190);if (PerlIOValid(f)) {
	SensorCall(10187);PerlIOl *l = PerlIOBase(f);

	SensorCall(10189);while (l) {
	    /* There is some collusion in the implementation of
	       XS_PerlIO_get_layers - it knows that name and flags are
	       generated as fresh SVs here, and takes advantage of that to
	       "copy" them by taking a reference. If it changes here, it needs
	       to change there too.  */
	    SensorCall(10188);SV * const name = l->tab && l->tab->name ?
	    newSVpv(l->tab->name, 0) : &PL_sv_undef;
	    SV * const arg = l->tab && l->tab->Getarg ?
	    (*l->tab->Getarg)(aTHX_ &l, 0, 0) : &PL_sv_undef;
	    av_push(av, name);
	    av_push(av, arg);
	    av_push(av, newSViv((IV)l->flags));
	    l = l->next;
	}
    }

    {AV * ReplaceReturn1564 = av; SensorCall(10191); return ReplaceReturn1564;}
}

/*--------------------------------------------------------------------------------------*/
/*
 * XS Interface for perl code
 */

PerlIO_funcs *
PerlIO_find_layer(pTHX_ const char *name, STRLEN len, int load)
{
SensorCall(10192);    dVAR;
    IV i;
    SensorCall(10194);if ((SSize_t) len <= 0)
	{/*1*/SensorCall(10193);len = strlen(name);/*2*/}
    SensorCall(10199);for (i = 0; i < PL_known_layers->cur; i++) {
	SensorCall(10195);PerlIO_funcs * const f = PL_known_layers->array[i].funcs;
	SensorCall(10198);if (memEQ(f->name, name, len) && f->name[len] == 0) {
	    SensorCall(10196);PerlIO_debug("%.*s => %p\n", (int) len, name, (void*)f);
	    {PerlIO_funcs * ReplaceReturn1563 = f; SensorCall(10197); return ReplaceReturn1563;}
	}
    }
    SensorCall(10207);if (load && PL_subname && PL_def_layerlist
	&& PL_def_layerlist->cur >= 2) {
	SensorCall(10200);if (PL_in_load_module) {
	    SensorCall(10201);Perl_croak(aTHX_ "Recursive call to Perl_load_module in PerlIO_find_layer");
	    {PerlIO_funcs * ReplaceReturn1562 = NULL; SensorCall(10202); return ReplaceReturn1562;}
	} else {
	    SensorCall(10203);SV * const pkgsv = newSVpvs("PerlIO");
	    SV * const layer = newSVpvn(name, len);
	    CV * const cv    = get_cvs("PerlIO::Layer::NoWarnings", 0);
	    ENTER;
	    SAVEBOOL(PL_in_load_module);
	    SensorCall(10204);if (cv) {
		SAVEGENERICSV(PL_warnhook);
		PL_warnhook = MUTABLE_SV((SvREFCNT_inc_simple_NN(cv)));
	    }
	    PL_in_load_module = TRUE;
	    /*
	     * The two SVs are magically freed by load_module
	     */
	    SensorCall(10205);Perl_load_module(aTHX_ 0, pkgsv, NULL, layer, NULL);
	    LEAVE;
	    {PerlIO_funcs * ReplaceReturn1561 = PerlIO_find_layer(aTHX_ name, len, 0); SensorCall(10206); return ReplaceReturn1561;}
	}
    }
    SensorCall(10208);PerlIO_debug("Cannot find %.*s\n", (int) len, name);
    {PerlIO_funcs * ReplaceReturn1560 = NULL; SensorCall(10209); return ReplaceReturn1560;}
}

#ifdef USE_ATTRIBUTES_FOR_PERLIO

static int
perlio_mg_set(pTHX_ SV *sv, MAGIC *mg)
{
    if (SvROK(sv)) {
	IO * const io = GvIOn(MUTABLE_GV(SvRV(sv)));
	PerlIO * const ifp = IoIFP(io);
	PerlIO * const ofp = IoOFP(io);
	Perl_warn(aTHX_ "set %" SVf " %p %p %p",
		  SVfARG(sv), (void*)io, (void*)ifp, (void*)ofp);
    }
    return 0;
}

static int
perlio_mg_get(pTHX_ SV *sv, MAGIC *mg)
{
    if (SvROK(sv)) {
	IO * const io = GvIOn(MUTABLE_GV(SvRV(sv)));
	PerlIO * const ifp = IoIFP(io);
	PerlIO * const ofp = IoOFP(io);
	Perl_warn(aTHX_ "get %" SVf " %p %p %p",
		  SVfARG(sv), (void*)io, (void*)ifp, (void*)ofp);
    }
    return 0;
}

static int
perlio_mg_clear(pTHX_ SV *sv, MAGIC *mg)
{
    Perl_warn(aTHX_ "clear %" SVf, SVfARG(sv));
    return 0;
}

static int
perlio_mg_free(pTHX_ SV *sv, MAGIC *mg)
{
    Perl_warn(aTHX_ "free %" SVf, SVfARG(sv));
    return 0;
}

MGVTBL perlio_vtab = {
    perlio_mg_get,
    perlio_mg_set,
    NULL,                       /* len */
    perlio_mg_clear,
    perlio_mg_free
};

XS(XS_io_MODIFY_SCALAR_ATTRIBUTES)
{
    dXSARGS;
    SV * const sv = SvRV(ST(1));
    AV * const av = newAV();
    MAGIC *mg;
    int count = 0;
    int i;
    sv_magic(sv, MUTABLE_SV(av), PERL_MAGIC_ext, NULL, 0);
    SvRMAGICAL_off(sv);
    mg = mg_find(sv, PERL_MAGIC_ext);
    mg->mg_virtual = &perlio_vtab;
    mg_magical(sv);
    Perl_warn(aTHX_ "attrib %" SVf, SVfARG(sv));
    for (i = 2; i < items; i++) {
	STRLEN len;
	const char * const name = SvPV_const(ST(i), len);
	SV * const layer = PerlIO_find_layer(aTHX_ name, len, 1);
	if (layer) {
	    av_push(av, SvREFCNT_inc_simple_NN(layer));
	}
	else {
	    ST(count) = ST(i);
	    count++;
	}
    }
    SvREFCNT_dec(av);
    XSRETURN(count);
}

#endif                          /* USE_ATTIBUTES_FOR_PERLIO */

SV *
PerlIO_tab_sv(pTHX_ PerlIO_funcs *tab)
{
    SensorCall(10210);HV * const stash = gv_stashpvs("PerlIO::Layer", GV_ADD);
    SV * const sv = sv_bless(newRV_noinc(newSViv(PTR2IV(tab))), stash);
    {SV * ReplaceReturn1559 = sv; SensorCall(10211); return ReplaceReturn1559;}
}

XS(XS_PerlIO__Layer__NoWarnings)
{
SensorCall(10212);    /* This is used as a %SIG{__WARN__} handler to suppress warnings
       during loading of layers.
     */
    dVAR;
    dXSARGS;
    PERL_UNUSED_ARG(cv);
    SensorCall(10214);if (items)
    	{/*245*/SensorCall(10213);PerlIO_debug("warning:%s\n",SvPV_nolen_const(ST(0)));/*246*/}
    XSRETURN(0);
}

XS(XS_PerlIO__Layer__find)
{
SensorCall(10215);    dVAR;
    dXSARGS;
    PERL_UNUSED_ARG(cv);
    SensorCall(10218);if (items < 2)
	{/*247*/SensorCall(10216);Perl_croak(aTHX_ "Usage class->find(name[,load])");/*248*/}
    else {
	SensorCall(10217);STRLEN len;
	const char * const name = SvPV_const(ST(1), len);
	const bool load = (items > 2) ? SvTRUE(ST(2)) : 0;
	PerlIO_funcs * const layer = PerlIO_find_layer(aTHX_ name, len, load);
	ST(0) =
	    (layer) ? sv_2mortal(PerlIO_tab_sv(aTHX_ layer)) :
	    &PL_sv_undef;
	XSRETURN(1);
    }
SensorCall(10219);}

void
PerlIO_define_layer(pTHX_ PerlIO_funcs *tab)
{
SensorCall(10220);    dVAR;
    SensorCall(10221);if (!PL_known_layers)
	PL_known_layers = PerlIO_list_alloc(aTHX);
    SensorCall(10222);PerlIO_list_push(aTHX_ PL_known_layers, tab, NULL);
    PerlIO_debug("define %s %p\n", tab->name, (void*)tab);
SensorCall(10223);}

int
PerlIO_parse_layers(pTHX_ PerlIO_list_t *av, const char *names)
{
SensorCall(10224);    dVAR;
    SensorCall(10260);if (names) {
	SensorCall(10225);const char *s = names;
	SensorCall(10259);while (*s) {
	    SensorCall(10226);while (isSPACE(*s) || *s == ':')
		{/*109*/SensorCall(10227);s++;/*110*/}
	    SensorCall(10258);if (*s) {
		SensorCall(10228);STRLEN llen = 0;
		const char *e = s;
		const char *as = NULL;
		STRLEN alen = 0;
		SensorCall(10231);if (!isIDFIRST(*s)) {
		    /*
		     * Message is consistent with how attribute lists are
		     * passed. Even though this means "foo : : bar" is
		     * seen as an invalid separator character.
		     */
		    SensorCall(10229);const char q = ((*s == '\'') ? '"' : '\'');
		    Perl_ck_warner(aTHX_ packWARN(WARN_LAYER),
				   "Invalid separator character %c%c%c in PerlIO layer specification %s",
				   q, *s, q, s);
		    SETERRNO(EINVAL, LIB_INVARG);
		    {int  ReplaceReturn1558 = -1; SensorCall(10230); return ReplaceReturn1558;}
		}
		SensorCall(10233);do {
		    SensorCall(10232);e++;
		} while (isALNUM(*e));
		SensorCall(10234);llen = e - s;
		SensorCall(10248);if (*e == '(') {
		    SensorCall(10235);int nesting = 1;
		    as = ++e;
		    SensorCall(10247);while (nesting) {
			SensorCall(10236);switch (*e++) {
			case ')':
			    SensorCall(10237);if (--nesting == 0)
				{/*111*/SensorCall(10238);alen = (e - 1) - as;/*112*/}
			    SensorCall(10239);break;
			case '(':
			    SensorCall(10240);++nesting;
			    SensorCall(10241);break;
			case '\\':
			    /*
			     * It's a nul terminated string, not allowed
			     * to \ the terminating null. Anything other
			     * character is passed over.
			     */
			    SensorCall(10242);if (*e++) {
				SensorCall(10243);break;
			    }
			    /*
			     * Drop through
			     */
			case '\0':
			    SensorCall(10244);e--;
			    Perl_ck_warner(aTHX_ packWARN(WARN_LAYER),
					   "Argument list not closed for PerlIO layer \"%.*s\"",
					   (int) (e - s), s);
			    {int  ReplaceReturn1557 = -1; SensorCall(10245); return ReplaceReturn1557;}
			default:
			    /*
			     * boring.
			     */
			    SensorCall(10246);break;
			}
		    }
		}
		SensorCall(10256);if (e > s) {
		    SensorCall(10249);PerlIO_funcs * const layer =
			PerlIO_find_layer(aTHX_ s, llen, 1);
		    SensorCall(10255);if (layer) {
			SensorCall(10250);SV *arg = NULL;
			SensorCall(10251);if (as)
			    arg = newSVpvn(as, alen);
			SensorCall(10252);PerlIO_list_push(aTHX_ av, layer,
					 (arg) ? arg : &PL_sv_undef);
			SvREFCNT_dec(arg);
		    }
		    else {
			SensorCall(10253);Perl_ck_warner(aTHX_ packWARN(WARN_LAYER), "Unknown PerlIO layer \"%.*s\"",
				       (int) llen, s);
			{int  ReplaceReturn1556 = -1; SensorCall(10254); return ReplaceReturn1556;}
		    }
		}
		SensorCall(10257);s = e;
	    }
	}
    }
    {int  ReplaceReturn1555 = 0; SensorCall(10261); return ReplaceReturn1555;}
}

void
PerlIO_default_buffer(pTHX_ PerlIO_list_t *av)
{
SensorCall(10262);    dVAR;
    PERLIO_FUNCS_DECL(*tab) = &PerlIO_perlio;
#ifdef PERLIO_USING_CRLF
    tab = &PerlIO_crlf;
#else
    SensorCall(10264);if (PerlIO_stdio.Set_ptrcnt)
	{/*115*/SensorCall(10263);tab = &PerlIO_stdio;/*116*/}
#endif
    SensorCall(10265);PerlIO_debug("Pushing %s\n", tab->name);
    PerlIO_list_push(aTHX_ av, PerlIO_find_layer(aTHX_ tab->name, 0, 0),
		     &PL_sv_undef);
SensorCall(10266);}

SV *
PerlIO_arg_fetch(PerlIO_list_t *av, IV n)
{
    {SV * ReplaceReturn1554 = av->array[n].arg; SensorCall(10267); return ReplaceReturn1554;}
}

PerlIO_funcs *
PerlIO_layer_fetch(pTHX_ PerlIO_list_t *av, IV n, PerlIO_funcs *def)
{
    SensorCall(10268);if (n >= 0 && n < av->cur) {
	SensorCall(10269);PerlIO_debug("Layer %" IVdf " is %s\n", n,
		     av->array[n].funcs->name);
	{PerlIO_funcs * ReplaceReturn1553 = av->array[n].funcs; SensorCall(10270); return ReplaceReturn1553;}
    }
    SensorCall(10272);if (!def)
	{/*113*/SensorCall(10271);Perl_croak(aTHX_ "panic: PerlIO layer array corrupt");/*114*/}
    {PerlIO_funcs * ReplaceReturn1552 = def; SensorCall(10273); return ReplaceReturn1552;}
}

IV
PerlIOPop_pushed(pTHX_ PerlIO *f, const char *mode, SV *arg, PerlIO_funcs *tab)
{
    PERL_UNUSED_ARG(mode);
    PERL_UNUSED_ARG(arg);
    PERL_UNUSED_ARG(tab);
    SensorCall(10276);if (PerlIOValid(f)) {
	PerlIO_flush(f);
	SensorCall(10274);PerlIO_pop(aTHX_ f);
	{IV  ReplaceReturn1551 = 0; SensorCall(10275); return ReplaceReturn1551;}
    }
    {IV  ReplaceReturn1550 = -1; SensorCall(10277); return ReplaceReturn1550;}
}

PERLIO_FUNCS_DECL(PerlIO_remove) = {
    sizeof(PerlIO_funcs),
    "pop",
    0,
    PERLIO_K_DUMMY | PERLIO_K_UTF8,
    PerlIOPop_pushed,
    NULL,
    PerlIOBase_open,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,                       /* flush */
    NULL,                       /* fill */
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,                       /* get_base */
    NULL,                       /* get_bufsiz */
    NULL,                       /* get_ptr */
    NULL,                       /* get_cnt */
    NULL,                       /* set_ptrcnt */
};

PerlIO_list_t *
PerlIO_default_layers(pTHX)
{
SensorCall(10278);    dVAR;
    SensorCall(10283);if (!PL_def_layerlist) {
	SensorCall(10279);const char * const s = (PL_tainting) ? NULL : PerlEnv_getenv("PERLIO");
	PERLIO_FUNCS_DECL(*osLayer) = &PerlIO_unix;
	PL_def_layerlist = PerlIO_list_alloc(aTHX);
	PerlIO_define_layer(aTHX_ PERLIO_FUNCS_CAST(&PerlIO_unix));
#if defined(WIN32)
	PerlIO_define_layer(aTHX_ PERLIO_FUNCS_CAST(&PerlIO_win32));
#if 0
	osLayer = &PerlIO_win32;
#endif
#endif
	PerlIO_define_layer(aTHX_ PERLIO_FUNCS_CAST(&PerlIO_raw));
	PerlIO_define_layer(aTHX_ PERLIO_FUNCS_CAST(&PerlIO_perlio));
	PerlIO_define_layer(aTHX_ PERLIO_FUNCS_CAST(&PerlIO_stdio));
	PerlIO_define_layer(aTHX_ PERLIO_FUNCS_CAST(&PerlIO_crlf));
	PerlIO_define_layer(aTHX_ PERLIO_FUNCS_CAST(&PerlIO_utf8));
	PerlIO_define_layer(aTHX_ PERLIO_FUNCS_CAST(&PerlIO_remove));
	PerlIO_define_layer(aTHX_ PERLIO_FUNCS_CAST(&PerlIO_byte));
	PerlIO_list_push(aTHX_ PL_def_layerlist,
			 PerlIO_find_layer(aTHX_ osLayer->name, 0, 0),
			 &PL_sv_undef);
	SensorCall(10282);if (s) {
	    SensorCall(10280);PerlIO_parse_layers(aTHX_ PL_def_layerlist, s);
	}
	else {
	    SensorCall(10281);PerlIO_default_buffer(aTHX_ PL_def_layerlist);
	}
    }
    SensorCall(10285);if (PL_def_layerlist->cur < 2) {
	SensorCall(10284);PerlIO_default_buffer(aTHX_ PL_def_layerlist);
    }
    {PerlIO_list_t * ReplaceReturn1549 = PL_def_layerlist; SensorCall(10286); return ReplaceReturn1549;}
}

void
Perl_boot_core_PerlIO(pTHX)
{
#ifdef USE_ATTRIBUTES_FOR_PERLIO
    newXS("io::MODIFY_SCALAR_ATTRIBUTES", XS_io_MODIFY_SCALAR_ATTRIBUTES,
	  __FILE__);
#endif
    newXS("PerlIO::Layer::find", XS_PerlIO__Layer__find, __FILE__);
    newXS("PerlIO::Layer::NoWarnings", XS_PerlIO__Layer__NoWarnings, __FILE__);
}

PerlIO_funcs *
PerlIO_default_layer(pTHX_ I32 n)
{
SensorCall(10287);    dVAR;
    PerlIO_list_t * const av = PerlIO_default_layers(aTHX);
    SensorCall(10289);if (n < 0)
	{/*127*/SensorCall(10288);n += av->cur;/*128*/}
    {PerlIO_funcs * ReplaceReturn1548 = PerlIO_layer_fetch(aTHX_ av, n, PERLIO_FUNCS_CAST(&PerlIO_stdio)); SensorCall(10290); return ReplaceReturn1548;}
}

#define PerlIO_default_top() PerlIO_default_layer(aTHX_ -1)
#define PerlIO_default_btm() PerlIO_default_layer(aTHX_ 0)

void
PerlIO_stdstreams(pTHX)
{
SensorCall(10291);    dVAR;
    SensorCall(10293);if (!PL_perlio) {
	SensorCall(10292);PerlIO_init_table(aTHX);
	PerlIO_fdopen(0, "Ir" PERLIO_STDTEXT);
	PerlIO_fdopen(1, "Iw" PERLIO_STDTEXT);
	PerlIO_fdopen(2, "Iw" PERLIO_STDTEXT);
    }
SensorCall(10294);}

PerlIO *
PerlIO_push(pTHX_ PerlIO *f, PERLIO_FUNCS_DECL(*tab), const char *mode, SV *arg)
{
    VERIFY_HEAD(f);
    SensorCall(10295);if (tab->fsize != sizeof(PerlIO_funcs)) {
	SensorCall(10296);Perl_croak( aTHX_
	    "%s (%"UVuf") does not match %s (%"UVuf")",
	    "PerlIO layer function table size", (UV)tab->fsize,
	    "size expected by this perl", (UV)sizeof(PerlIO_funcs) );
    }
    SensorCall(10311);if (tab->size) {
	SensorCall(10297);PerlIOl *l;
	SensorCall(10299);if (tab->size < sizeof(PerlIOl)) {
	    SensorCall(10298);Perl_croak( aTHX_
		"%s (%"UVuf") smaller than %s (%"UVuf")",
		"PerlIO layer instance size", (UV)tab->size,
		"size expected by this perl", (UV)sizeof(PerlIOl) );
	}
	/* Real layer with a data area */
	SensorCall(10306);if (f) {
	    SensorCall(10300);char *temp;
	    Newxz(temp, tab->size, char);
	    l = (PerlIOl*)temp;
	    SensorCall(10305);if (l) {
		SensorCall(10301);l->next = *f;
		l->tab = (PerlIO_funcs*) tab;
		l->head = ((PerlIOl*)f)->head;
		*f = l;
		PerlIO_debug("PerlIO_push f=%p %s %s %p\n",
			     (void*)f, tab->name,
			     (mode) ? mode : "(Null)", (void*)arg);
		SensorCall(10304);if (*l->tab->Pushed &&
		    (*l->tab->Pushed)
		      (aTHX_ f, mode, arg, (PerlIO_funcs*) tab) != 0) {
		    SensorCall(10302);PerlIO_pop(aTHX_ f);
		    {PerlIO * ReplaceReturn1547 = NULL; SensorCall(10303); return ReplaceReturn1547;}
		}
	    }
	    else
		return NULL;
	}
    }
    else {/*3*/SensorCall(10307);if (f) {
	/* Pseudo-layer where push does its own stack adjust */
	SensorCall(10308);PerlIO_debug("PerlIO_push f=%p %s %s %p\n", (void*)f, tab->name,
		     (mode) ? mode : "(Null)", (void*)arg);
	SensorCall(10310);if (tab->Pushed &&
	    (*tab->Pushed) (aTHX_ f, mode, arg, (PerlIO_funcs*) tab) != 0) {
	     {PerlIO * ReplaceReturn1546 = NULL; SensorCall(10309); return ReplaceReturn1546;}
	}
    ;/*4*/}}
    {PerlIO * ReplaceReturn1545 = f; SensorCall(10312); return ReplaceReturn1545;}
}

PerlIO *
PerlIOBase_open(pTHX_ PerlIO_funcs *self, PerlIO_list_t *layers,
	       IV n, const char *mode, int fd, int imode, int perm,
	       PerlIO *old, int narg, SV **args)
{
    SensorCall(10313);PerlIO_funcs * const tab = PerlIO_layer_fetch(aTHX_ layers, n - 1, PerlIO_default_layer(aTHX_ 0));
    SensorCall(10318);if (tab && tab->Open) {
SensorCall(10314);	PerlIO* ret = (*tab->Open)(aTHX_ tab, layers, n - 1, mode, fd, imode, perm, old, narg, args);
	SensorCall(10316);if (ret && PerlIO_push(aTHX_ ret, self, mode, PerlIOArg) == NULL) {
	    PerlIO_close(ret);
	    {PerlIO * ReplaceReturn1544 = NULL; SensorCall(10315); return ReplaceReturn1544;}
	}
	{PerlIO * ReplaceReturn1543 = ret; SensorCall(10317); return ReplaceReturn1543;}
    }
    SETERRNO(EINVAL, LIB_INVARG);
    {PerlIO * ReplaceReturn1542 = NULL; SensorCall(10319); return ReplaceReturn1542;}
}

IV
PerlIOBase_binmode(pTHX_ PerlIO *f)
{
   SensorCall(10320);if (PerlIOValid(f)) {
	/* Is layer suitable for raw stream ? */
	SensorCall(10321);if (PerlIOBase(f)->tab && PerlIOBase(f)->tab->kind & PERLIO_K_RAW) {
	    /* Yes - turn off UTF-8-ness, to undo UTF-8 locale effects */
	    PerlIOBase(f)->flags &= ~PERLIO_F_UTF8;
	}
	else {
	    /* Not suitable - pop it */
	    SensorCall(10322);PerlIO_pop(aTHX_ f);
	}
	{IV  ReplaceReturn1541 = 0; SensorCall(10323); return ReplaceReturn1541;}
   }
   {IV  ReplaceReturn1540 = -1; SensorCall(10324); return ReplaceReturn1540;}
}

IV
PerlIORaw_pushed(pTHX_ PerlIO *f, const char *mode, SV *arg, PerlIO_funcs *tab)
{
    PERL_UNUSED_ARG(mode);
    PERL_UNUSED_ARG(arg);
    PERL_UNUSED_ARG(tab);

    SensorCall(10336);if (PerlIOValid(f)) {
	PerlIO *t;
	SensorCall(10325);const PerlIOl *l;
	PerlIO_flush(f);
	/*
	 * Strip all layers that are not suitable for a raw stream
	 */
	t = f;
	SensorCall(10332);while (t && (l = *t)) {
	    SensorCall(10326);if (l->tab && l->tab->Binmode) {
		/* Has a handler - normal case */
		SensorCall(10327);if ((*l->tab->Binmode)(aTHX_ t) == 0) {
		    SensorCall(10328);if (*t == l) {
			/* Layer still there - move down a layer */
			SensorCall(10329);t = PerlIONext(t);
		    }
		}
		else {
		    {IV  ReplaceReturn1539 = -1; SensorCall(10330); return ReplaceReturn1539;}
		}
	    }
	    else {
		/* No handler - pop it */
		SensorCall(10331);PerlIO_pop(aTHX_ t);
	    }
	}
	SensorCall(10335);if (PerlIOValid(f)) {
	    SensorCall(10333);PerlIO_debug(":raw f=%p :%s\n", (void*)f,
		PerlIOBase(f)->tab ? PerlIOBase(f)->tab->name : "(Null)");
	    {IV  ReplaceReturn1538 = 0; SensorCall(10334); return ReplaceReturn1538;}
	}
    }
    {IV  ReplaceReturn1537 = -1; SensorCall(10337); return ReplaceReturn1537;}
}

int
PerlIO_apply_layera(pTHX_ PerlIO *f, const char *mode,
		    PerlIO_list_t *layers, IV n, IV max)
{
    SensorCall(10338);int code = 0;
    SensorCall(10345);while (n < max) {
	SensorCall(10339);PerlIO_funcs * const tab = PerlIO_layer_fetch(aTHX_ layers, n, NULL);
	SensorCall(10343);if (tab) {
	    SensorCall(10340);if (!PerlIO_push(aTHX_ f, tab, mode, PerlIOArg)) {
		SensorCall(10341);code = -1;
		SensorCall(10342);break;
	    }
	}
	SensorCall(10344);n++;
    }
    {int  ReplaceReturn1536 = code; SensorCall(10346); return ReplaceReturn1536;}
}

int
PerlIO_apply_layers(pTHX_ PerlIO *f, const char *mode, const char *names)
{
    SensorCall(10347);int code = 0;
    ENTER;
    save_scalar(PL_errgv);
    SensorCall(10352);if (f && names) {
	SensorCall(10348);PerlIO_list_t * const layers = PerlIO_list_alloc(aTHX);
	code = PerlIO_parse_layers(aTHX_ layers, names);
	SensorCall(10350);if (code == 0) {
	    SensorCall(10349);code = PerlIO_apply_layera(aTHX_ f, mode, layers, 0, layers->cur);
	}
	SensorCall(10351);PerlIO_list_free(aTHX_ layers);
    }
    LEAVE;
    {int  ReplaceReturn1535 = code; SensorCall(10353); return ReplaceReturn1535;}
}


/*--------------------------------------------------------------------------------------*/
/*
 * Given the abstraction above the public API functions
 */

int
PerlIO_binmode(pTHX_ PerlIO *f, int iotype, int mode, const char *names)
{
    SensorCall(10354);PerlIO_debug("PerlIO_binmode f=%p %s %c %x %s\n", (void*)f,
                 (PerlIOBase(f) && PerlIOBase(f)->tab) ?
		       	PerlIOBase(f)->tab->name : "(Null)",
                 iotype, mode, (names) ? names : "(Null)");

    SensorCall(10357);if (names) {
	/* Do not flush etc. if (e.g.) switching encodings.
	   if a pushed layer knows it needs to flush lower layers
	   (for example :unix which is never going to call them)
	   it can do the flush when it is pushed.
	 */
	{int  ReplaceReturn1534 = PerlIO_apply_layers(aTHX_ f, NULL, names) == 0 ? TRUE : FALSE; SensorCall(10355); return ReplaceReturn1534;}
    }
    else {
	/* Fake 5.6 legacy of using this call to turn ON O_TEXT */
#ifdef PERLIO_USING_CRLF
	/* Legacy binmode only has meaning if O_TEXT has a value distinct from
	   O_BINARY so we can look for it in mode.
	 */
	if (!(mode & O_BINARY)) {
	    /* Text mode */
	    /* FIXME?: Looking down the layer stack seems wrong,
	       but is a way of reaching past (say) an encoding layer
	       to flip CRLF-ness of the layer(s) below
	     */
	    while (*f) {
		/* Perhaps we should turn on bottom-most aware layer
		   e.g. Ilya's idea that UNIX TTY could serve
		 */
		if (PerlIOBase(f)->tab &&
		    PerlIOBase(f)->tab->kind & PERLIO_K_CANCRLF)
		{
		    if (!(PerlIOBase(f)->flags & PERLIO_F_CRLF)) {
			/* Not in text mode - flush any pending stuff and flip it */
			PerlIO_flush(f);
			PerlIOBase(f)->flags |= PERLIO_F_CRLF;
		    }
		    /* Only need to turn it on in one layer so we are done */
		    return TRUE;
		}
		f = PerlIONext(f);
	    }
	    /* Not finding a CRLF aware layer presumably means we are binary
	       which is not what was requested - so we failed
	       We _could_ push :crlf layer but so could caller
	     */
	    return FALSE;
	}
#endif
	/* Legacy binmode is now _defined_ as being equivalent to pushing :raw
	   So code that used to be here is now in PerlIORaw_pushed().
	 */
	{int  ReplaceReturn1533 = PerlIO_push(aTHX_ f, PERLIO_FUNCS_CAST(&PerlIO_raw), NULL, NULL) ? TRUE : FALSE; SensorCall(10356); return ReplaceReturn1533;}
    }
SensorCall(10358);}

int
PerlIO__close(pTHX_ PerlIO *f)
{
    SensorCall(10359);if (PerlIOValid(f)) {
	SensorCall(10360);PerlIO_funcs * const tab = PerlIOBase(f)->tab;
	SensorCall(10363);if (tab && tab->Close)
	    {/*117*/{int  ReplaceReturn1532 = (*tab->Close)(aTHX_ f); SensorCall(10361); return ReplaceReturn1532;}/*118*/}
	else
	    {/*119*/{int  ReplaceReturn1531 = PerlIOBase_close(aTHX_ f); SensorCall(10362); return ReplaceReturn1531;}/*120*/}
    }
    else {
	SETERRNO(EBADF, SS_IVCHAN);
	{int  ReplaceReturn1530 = -1; SensorCall(10364); return ReplaceReturn1530;}
    }
SensorCall(10365);}

int
Perl_PerlIO_close(pTHX_ PerlIO *f)
{
    SensorCall(10366);const int code = PerlIO__close(aTHX_ f);
    SensorCall(10369);while (PerlIOValid(f)) {
	SensorCall(10367);PerlIO_pop(aTHX_ f);
	SensorCall(10368);if (PerlIO_lockcnt(f))
	    /* we're in use; the 'pop' deferred freeing the structure */
	    f = PerlIONext(f);
    }
    {int  ReplaceReturn1529 = code; SensorCall(10370); return ReplaceReturn1529;}
}

int
Perl_PerlIO_fileno(pTHX_ PerlIO *f)
{
SensorCall(10371);    dVAR;
     Perl_PerlIO_or_Base(f, Fileno, fileno, -1, (aTHX_ f));
}


static PerlIO_funcs *
PerlIO_layer_from_ref(pTHX_ SV *sv)
{
SensorCall(10372);    dVAR;
    /*
     * For any scalar type load the handler which is bundled with perl
     */
    SensorCall(10377);if (SvTYPE(sv) < SVt_PVAV && (!isGV_with_GP(sv) || SvFAKE(sv))) {
	SensorCall(10373);PerlIO_funcs *f = PerlIO_find_layer(aTHX_ STR_WITH_LEN("scalar"), 1);
	/* This isn't supposed to happen, since PerlIO::scalar is core,
	 * but could happen anyway in smaller installs or with PAR */
	SensorCall(10375);if (!f)
	    /* diag_listed_as: Unknown PerlIO layer "%s" */
	    {/*249*/SensorCall(10374);Perl_ck_warner(aTHX_ packWARN(WARN_LAYER), "Unknown PerlIO layer \"scalar\"");/*250*/}
	{PerlIO_funcs * ReplaceReturn1528 = f; SensorCall(10376); return ReplaceReturn1528;}
    }

    /*
     * For other types allow if layer is known but don't try and load it
     */
    SensorCall(10383);switch (SvTYPE(sv)) {
    case SVt_PVAV:
	{PerlIO_funcs * ReplaceReturn1527 = PerlIO_find_layer(aTHX_ STR_WITH_LEN("Array"), 0); SensorCall(10378); return ReplaceReturn1527;}
    case SVt_PVHV:
	{PerlIO_funcs * ReplaceReturn1526 = PerlIO_find_layer(aTHX_ STR_WITH_LEN("Hash"), 0); SensorCall(10379); return ReplaceReturn1526;}
    case SVt_PVCV:
	{PerlIO_funcs * ReplaceReturn1525 = PerlIO_find_layer(aTHX_ STR_WITH_LEN("Code"), 0); SensorCall(10380); return ReplaceReturn1525;}
    case SVt_PVGV:
	{PerlIO_funcs * ReplaceReturn1524 = PerlIO_find_layer(aTHX_ STR_WITH_LEN("Glob"), 0); SensorCall(10381); return ReplaceReturn1524;}
    default:
	{PerlIO_funcs * ReplaceReturn1523 = NULL; SensorCall(10382); return ReplaceReturn1523;}
    }
SensorCall(10384);}

PerlIO_list_t *
PerlIO_resolve_layers(pTHX_ const char *layers,
		      const char *mode, int narg, SV **args)
{
SensorCall(10385);    dVAR;
    PerlIO_list_t *def = PerlIO_default_layers(aTHX);
    int incdef = 1;
    SensorCall(10387);if (!PL_perlio)
	{/*121*/SensorCall(10386);PerlIO_stdstreams(aTHX);/*122*/}
    SensorCall(10393);if (narg) {
	SensorCall(10388);SV * const arg = *args;
	/*
	 * If it is a reference but not an object see if we have a handler
	 * for it
	 */
	SensorCall(10392);if (SvROK(arg) && !sv_isobject(arg)) {
	    SensorCall(10389);PerlIO_funcs * const handler = PerlIO_layer_from_ref(aTHX_ SvRV(arg));
	    SensorCall(10391);if (handler) {
		SensorCall(10390);def = PerlIO_list_alloc(aTHX);
		PerlIO_list_push(aTHX_ def, handler, &PL_sv_undef);
		incdef = 0;
	    }
	    /*
	     * Don't fail if handler cannot be found :via(...) etc. may do
	     * something sensible else we will just stringfy and open
	     * resulting string.
	     */
	}
    }
    SensorCall(10395);if (!layers || !*layers)
	{/*123*/SensorCall(10394);layers = Perl_PerlIO_context_layers(aTHX_ mode);/*124*/}
    SensorCall(10407);if (layers && *layers) {
	SensorCall(10396);PerlIO_list_t *av;
	SensorCall(10399);if (incdef) {
	    SensorCall(10397);av = PerlIO_clone_list(aTHX_ def, NULL);
	}
	else {
	    SensorCall(10398);av = def;
	}
	SensorCall(10403);if (PerlIO_parse_layers(aTHX_ av, layers) == 0) {
	     {PerlIO_list_t * ReplaceReturn1522 = av; SensorCall(10400); return ReplaceReturn1522;}
	}
	else {
	    SensorCall(10401);PerlIO_list_free(aTHX_ av);
	    {PerlIO_list_t * ReplaceReturn1521 = NULL; SensorCall(10402); return ReplaceReturn1521;}
	}
    }
    else {
	SensorCall(10404);if (incdef)
	    {/*125*/SensorCall(10405);def->refcnt++;/*126*/}
	{PerlIO_list_t * ReplaceReturn1520 = def; SensorCall(10406); return ReplaceReturn1520;}
    }
SensorCall(10408);}

PerlIO *
PerlIO_openn(pTHX_ const char *layers, const char *mode, int fd,
	     int imode, int perm, PerlIO *f, int narg, SV **args)
{
SensorCall(10409);    dVAR;
    SensorCall(10445);if (!f && narg == 1 && *args == &PL_sv_undef) {
	SensorCall(10410);if ((f = PerlIO_tmpfile())) {
	    SensorCall(10411);if (!layers || !*layers)
		{/*7*/SensorCall(10412);layers = Perl_PerlIO_context_layers(aTHX_ mode);/*8*/}
	    SensorCall(10414);if (layers && *layers)
		{/*9*/SensorCall(10413);PerlIO_apply_layers(aTHX_ f, mode, layers);/*10*/}
	}
    }
    else {
	SensorCall(10415);PerlIO_list_t *layera;
	IV n;
	PerlIO_funcs *tab = NULL;
	SensorCall(10425);if (PerlIOValid(f)) {
	    /*
	     * This is "reopen" - it is not tested as perl does not use it
	     * yet
	     */
	    SensorCall(10416);PerlIOl *l = *f;
	    layera = PerlIO_list_alloc(aTHX);
	    SensorCall(10421);while (l) {
		SensorCall(10417);SV *arg = NULL;
		SensorCall(10419);if (l->tab && l->tab->Getarg)
		    {/*11*/SensorCall(10418);arg = (*l->tab->Getarg) (aTHX_ &l, NULL, 0);/*12*/}
		SensorCall(10420);PerlIO_list_push(aTHX_ layera, l->tab,
				 (arg) ? arg : &PL_sv_undef);
		SvREFCNT_dec(arg);
		l = *PerlIONext(&l);
	    }
	}
	else {
	    SensorCall(10422);layera = PerlIO_resolve_layers(aTHX_ layers, mode, narg, args);
	    SensorCall(10424);if (!layera) {
		{PerlIO * ReplaceReturn1519 = NULL; SensorCall(10423); return ReplaceReturn1519;}
	    }
	}
	/*
	 * Start at "top" of layer stack
	 */
	SensorCall(10426);n = layera->cur - 1;
	SensorCall(10432);while (n >= 0) {
	    SensorCall(10427);PerlIO_funcs * const t = PerlIO_layer_fetch(aTHX_ layera, n, NULL);
	    SensorCall(10430);if (t && t->Open) {
		SensorCall(10428);tab = t;
		SensorCall(10429);break;
	    }
	    SensorCall(10431);n--;
	}
	SensorCall(10443);if (tab) {
	    /*
	     * Found that layer 'n' can do opens - call it
	     */
	    SensorCall(10433);if (narg > 1 && !(tab->kind & PERLIO_K_MULTIARG)) {
		SensorCall(10434);Perl_croak(aTHX_ "More than one argument to open(,':%s')",tab->name);
	    }
	    SensorCall(10435);PerlIO_debug("openn(%s,'%s','%s',%d,%x,%o,%p,%d,%p)\n",
			 tab->name, layers ? layers : "(Null)", mode, fd,
			 imode, perm, (void*)f, narg, (void*)args);
	    SensorCall(10438);if (tab->Open)
		 {/*13*/SensorCall(10436);f = (*tab->Open) (aTHX_ tab, layera, n, mode, fd, imode, perm,
				   f, narg, args);/*14*/}
	    else {
		 SETERRNO(EINVAL, LIB_INVARG);
		 SensorCall(10437);f = NULL;
	    }
	    SensorCall(10442);if (f) {
		SensorCall(10439);if (n + 1 < layera->cur) {
		    /*
		     * More layers above the one that we used to open -
		     * apply them now
		     */
		    SensorCall(10440);if (PerlIO_apply_layera(aTHX_ f, mode, layera, n + 1, layera->cur) != 0) {
			/* If pushing layers fails close the file */
			PerlIO_close(f);
			SensorCall(10441);f = NULL;
		    }
		}
	    }
	}
	SensorCall(10444);PerlIO_list_free(aTHX_ layera);
    }
    {PerlIO * ReplaceReturn1518 = f; SensorCall(10446); return ReplaceReturn1518;}
}


SSize_t
Perl_PerlIO_read(pTHX_ PerlIO *f, void *vbuf, Size_t count)
{
SensorCall(10447);     PERL_ARGS_ASSERT_PERLIO_READ;

     Perl_PerlIO_or_Base(f, Read, read, -1, (aTHX_ f, vbuf, count));
}

SSize_t
Perl_PerlIO_unread(pTHX_ PerlIO *f, const void *vbuf, Size_t count)
{
SensorCall(10448);     PERL_ARGS_ASSERT_PERLIO_UNREAD;

     Perl_PerlIO_or_Base(f, Unread, unread, -1, (aTHX_ f, vbuf, count));
}

SSize_t
Perl_PerlIO_write(pTHX_ PerlIO *f, const void *vbuf, Size_t count)
{
SensorCall(10449);     PERL_ARGS_ASSERT_PERLIO_WRITE;

     Perl_PerlIO_or_fail(f, Write, -1, (aTHX_ f, vbuf, count));
}

int
Perl_PerlIO_seek(pTHX_ PerlIO *f, Off_t offset, int whence)
{
SensorCall(10450);     Perl_PerlIO_or_fail(f, Seek, -1, (aTHX_ f, offset, whence));
}

Off_t
Perl_PerlIO_tell(pTHX_ PerlIO *f)
{
SensorCall(10451);     Perl_PerlIO_or_fail(f, Tell, -1, (aTHX_ f));
}

int
Perl_PerlIO_flush(pTHX_ PerlIO *f)
{
SensorCall(10452);    dVAR;
    SensorCall(10468);if (f) {
	SensorCall(10453);if (*f) {
	    SensorCall(10454);const PerlIO_funcs *tab = PerlIOBase(f)->tab;

	    SensorCall(10457);if (tab && tab->Flush)
		{/*75*/{int  ReplaceReturn1517 = (*tab->Flush) (aTHX_ f); SensorCall(10455); return ReplaceReturn1517;}/*76*/}
	    else
		 {/*77*/{int  ReplaceReturn1516 = 0; SensorCall(10456); return ReplaceReturn1516;}/*78*/} /* If no Flush defined, silently succeed. */
	}
	else {
	    SensorCall(10458);PerlIO_debug("Cannot flush f=%p\n", (void*)f);
	    SETERRNO(EBADF, SS_IVCHAN);
	    {int  ReplaceReturn1515 = -1; SensorCall(10459); return ReplaceReturn1515;}
	}
    }
    else {
	/*
	 * Is it good API design to do flush-all on NULL, a potentially
	 * erroneous input? Maybe some magical value (PerlIO*
	 * PERLIO_FLUSH_ALL = (PerlIO*)-1;)? Yes, stdio does similar
	 * things on fflush(NULL), but should we be bound by their design
	 * decisions? --jhi
	 */
	SensorCall(10460);PerlIOl **table = &PL_perlio;
	PerlIOl *ff;
	int code = 0;
	SensorCall(10466);while ((ff = *table)) {
	    SensorCall(10461);int i;
	    table = (PerlIOl **) (ff++);
	    SensorCall(10465);for (i = 1; i < PERLIO_TABLE_SIZE; i++) {
		SensorCall(10462);if (ff->next && PerlIO_flush(&(ff->next)) != 0)
		    {/*79*/SensorCall(10463);code = -1;/*80*/}
		SensorCall(10464);ff++;
	    }
	}
	{int  ReplaceReturn1514 = code; SensorCall(10467); return ReplaceReturn1514;}
    }
SensorCall(10469);}

void
PerlIOBase_flush_linebuf(pTHX)
{
SensorCall(10470);    dVAR;
    PerlIOl **table = &PL_perlio;
    PerlIOl *f;
    SensorCall(10475);while ((f = *table)) {
	SensorCall(10471);int i;
	table = (PerlIOl **) (f++);
	SensorCall(10474);for (i = 1; i < PERLIO_TABLE_SIZE; i++) {
	    SensorCall(10472);if (f->next
		&& (PerlIOBase(&(f->next))->
		    flags & (PERLIO_F_LINEBUF | PERLIO_F_CANWRITE))
		== (PERLIO_F_LINEBUF | PERLIO_F_CANWRITE))
		PerlIO_flush(&(f->next));
	    SensorCall(10473);f++;
	}
    }
SensorCall(10476);}

int
Perl_PerlIO_fill(pTHX_ PerlIO *f)
{
SensorCall(10477);     Perl_PerlIO_or_fail(f, Fill, -1, (aTHX_ f));
}

int
PerlIO_isutf8(PerlIO *f)
{
     SensorCall(10478);if (PerlIOValid(f))
	  {/*37*/{int  ReplaceReturn1513 = (PerlIOBase(f)->flags & PERLIO_F_UTF8) != 0; SensorCall(10479); return ReplaceReturn1513;}/*38*/}
     else
	  SETERRNO(EBADF, SS_IVCHAN);

     {int  ReplaceReturn1512 = -1; SensorCall(10480); return ReplaceReturn1512;}
}

int
Perl_PerlIO_eof(pTHX_ PerlIO *f)
{
SensorCall(10481);     Perl_PerlIO_or_Base(f, Eof, eof, -1, (aTHX_ f));
}

int
Perl_PerlIO_error(pTHX_ PerlIO *f)
{
SensorCall(10482);     Perl_PerlIO_or_Base(f, Error, error, -1, (aTHX_ f));
}

void
Perl_PerlIO_clearerr(pTHX_ PerlIO *f)
{
SensorCall(10483);     Perl_PerlIO_or_Base_void(f, Clearerr, clearerr, (aTHX_ f));
}

void
Perl_PerlIO_setlinebuf(pTHX_ PerlIO *f)
{
SensorCall(10484);     Perl_PerlIO_or_Base_void(f, Setlinebuf, setlinebuf, (aTHX_ f));
}

int
PerlIO_has_base(PerlIO *f)
{
     SensorCall(10485);if (PerlIOValid(f)) {
	  SensorCall(10486);const PerlIO_funcs * const tab = PerlIOBase(f)->tab;

	  SensorCall(10488);if (tab)
	       {/*21*/{int  ReplaceReturn1511 = (tab->Get_base != NULL); SensorCall(10487); return ReplaceReturn1511;}/*22*/}
     }

     {int  ReplaceReturn1510 = 0; SensorCall(10489); return ReplaceReturn1510;}
}

int
PerlIO_fast_gets(PerlIO *f)
{
    SensorCall(10490);if (PerlIOValid(f)) {
	 SensorCall(10491);if (PerlIOBase(f)->flags & PERLIO_F_FASTGETS) {
	     SensorCall(10492);const PerlIO_funcs * const tab = PerlIOBase(f)->tab;

	     SensorCall(10494);if (tab)
		  {/*25*/{int  ReplaceReturn1509 = (tab->Set_ptrcnt != NULL); SensorCall(10493); return ReplaceReturn1509;}/*26*/}
	 }
    }

    {int  ReplaceReturn1508 = 0; SensorCall(10495); return ReplaceReturn1508;}
}

int
PerlIO_has_cntptr(PerlIO *f)
{
    SensorCall(10496);if (PerlIOValid(f)) {
	SensorCall(10497);const PerlIO_funcs * const tab = PerlIOBase(f)->tab;

	SensorCall(10499);if (tab)
	     {/*23*/{int  ReplaceReturn1507 = (tab->Get_ptr != NULL && tab->Get_cnt != NULL); SensorCall(10498); return ReplaceReturn1507;}/*24*/}
    }

    {int  ReplaceReturn1506 = 0; SensorCall(10500); return ReplaceReturn1506;}
}

int
PerlIO_canset_cnt(PerlIO *f)
{
    SensorCall(10501);if (PerlIOValid(f)) {
	  SensorCall(10502);const PerlIO_funcs * const tab = PerlIOBase(f)->tab;

	  SensorCall(10504);if (tab)
	       {/*27*/{int  ReplaceReturn1505 = (tab->Set_ptrcnt != NULL); SensorCall(10503); return ReplaceReturn1505;}/*28*/}
    }

    {int  ReplaceReturn1504 = 0; SensorCall(10505); return ReplaceReturn1504;}
}

STDCHAR *
Perl_PerlIO_get_base(pTHX_ PerlIO *f)
{
SensorCall(10506);     Perl_PerlIO_or_fail(f, Get_base, NULL, (aTHX_ f));
}

int
Perl_PerlIO_get_bufsiz(pTHX_ PerlIO *f)
{
SensorCall(10507);     Perl_PerlIO_or_fail(f, Get_bufsiz, -1, (aTHX_ f));
}

STDCHAR *
Perl_PerlIO_get_ptr(pTHX_ PerlIO *f)
{
SensorCall(10508);     Perl_PerlIO_or_fail(f, Get_ptr, NULL, (aTHX_ f));
}

int
Perl_PerlIO_get_cnt(pTHX_ PerlIO *f)
{
SensorCall(10509);     Perl_PerlIO_or_fail(f, Get_cnt, -1, (aTHX_ f));
}

void
Perl_PerlIO_set_cnt(pTHX_ PerlIO *f, int cnt)
{
SensorCall(10510);     Perl_PerlIO_or_fail_void(f, Set_ptrcnt, (aTHX_ f, NULL, cnt));
}

void
Perl_PerlIO_set_ptrcnt(pTHX_ PerlIO *f, STDCHAR * ptr, int cnt)
{
SensorCall(10511);     Perl_PerlIO_or_fail_void(f, Set_ptrcnt, (aTHX_ f, ptr, cnt));
}


/*--------------------------------------------------------------------------------------*/
/*
 * utf8 and raw dummy layers
 */

IV
PerlIOUtf8_pushed(pTHX_ PerlIO *f, const char *mode, SV *arg, PerlIO_funcs *tab)
{
    PERL_UNUSED_CONTEXT;
    PERL_UNUSED_ARG(mode);
    PERL_UNUSED_ARG(arg);
    SensorCall(10514);if (PerlIOValid(f)) {
	SensorCall(10512);if (tab && tab->kind & PERLIO_K_UTF8)
	    PerlIOBase(f)->flags |= PERLIO_F_UTF8;
	else
	    PerlIOBase(f)->flags &= ~PERLIO_F_UTF8;
	{IV  ReplaceReturn1503 = 0; SensorCall(10513); return ReplaceReturn1503;}
    }
    {IV  ReplaceReturn1502 = -1; SensorCall(10515); return ReplaceReturn1502;}
}

PERLIO_FUNCS_DECL(PerlIO_utf8) = {
    sizeof(PerlIO_funcs),
    "utf8",
    0,
    PERLIO_K_DUMMY | PERLIO_K_UTF8 | PERLIO_K_MULTIARG,
    PerlIOUtf8_pushed,
    NULL,
    PerlIOBase_open,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,                       /* flush */
    NULL,                       /* fill */
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,                       /* get_base */
    NULL,                       /* get_bufsiz */
    NULL,                       /* get_ptr */
    NULL,                       /* get_cnt */
    NULL,                       /* set_ptrcnt */
};

PERLIO_FUNCS_DECL(PerlIO_byte) = {
    sizeof(PerlIO_funcs),
    "bytes",
    0,
    PERLIO_K_DUMMY | PERLIO_K_MULTIARG,
    PerlIOUtf8_pushed,
    NULL,
    PerlIOBase_open,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,                       /* flush */
    NULL,                       /* fill */
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,                       /* get_base */
    NULL,                       /* get_bufsiz */
    NULL,                       /* get_ptr */
    NULL,                       /* get_cnt */
    NULL,                       /* set_ptrcnt */
};

PERLIO_FUNCS_DECL(PerlIO_raw) = {
    sizeof(PerlIO_funcs),
    "raw",
    0,
    PERLIO_K_DUMMY,
    PerlIORaw_pushed,
    PerlIOBase_popped,
    PerlIOBase_open,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,                       /* flush */
    NULL,                       /* fill */
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,                       /* get_base */
    NULL,                       /* get_bufsiz */
    NULL,                       /* get_ptr */
    NULL,                       /* get_cnt */
    NULL,                       /* set_ptrcnt */
};
/*--------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------*/
/*
 * "Methods" of the "base class"
 */

IV
PerlIOBase_fileno(pTHX_ PerlIO *f)
{
    {IV  ReplaceReturn1501 = PerlIOValid(f) ? PerlIO_fileno(PerlIONext(f)) : -1; SensorCall(10516); return ReplaceReturn1501;}
}

char *
PerlIO_modestr(PerlIO * f, char *buf)
{
    SensorCall(10517);char *s = buf;
    SensorCall(10531);if (PerlIOValid(f)) {
	SensorCall(10518);const IV flags = PerlIOBase(f)->flags;
	SensorCall(10530);if (flags & PERLIO_F_APPEND) {
	    SensorCall(10519);*s++ = 'a';
	    SensorCall(10521);if (flags & PERLIO_F_CANREAD) {
		SensorCall(10520);*s++ = '+';
	    }
	}
	else {/*31*/SensorCall(10522);if (flags & PERLIO_F_CANREAD) {
	    SensorCall(10523);*s++ = 'r';
	    SensorCall(10525);if (flags & PERLIO_F_CANWRITE)
		{/*33*/SensorCall(10524);*s++ = '+';/*34*/}
	}
	else {/*35*/SensorCall(10526);if (flags & PERLIO_F_CANWRITE) {
	    SensorCall(10527);*s++ = 'w';
	    SensorCall(10529);if (flags & PERLIO_F_CANREAD) {
		SensorCall(10528);*s++ = '+';
	    }
	;/*36*/}/*32*/}}
#ifdef PERLIO_USING_CRLF
	if (!(flags & PERLIO_F_CRLF))
	    *s++ = 'b';
#endif
    }
    SensorCall(10532);*s = '\0';
    {char * ReplaceReturn1500 = buf; SensorCall(10533); return ReplaceReturn1500;}
}


IV
PerlIOBase_pushed(pTHX_ PerlIO *f, const char *mode, SV *arg, PerlIO_funcs *tab)
{
    SensorCall(10534);PerlIOl * const l = PerlIOBase(f);
    PERL_UNUSED_CONTEXT;
    PERL_UNUSED_ARG(arg);

    l->flags &= ~(PERLIO_F_CANREAD | PERLIO_F_CANWRITE |
		  PERLIO_F_TRUNCATE | PERLIO_F_APPEND);
    SensorCall(10535);if (tab && tab->Set_ptrcnt != NULL)
	l->flags |= PERLIO_F_FASTGETS;
    SensorCall(10557);if (mode) {
	SensorCall(10536);if (*mode == IoTYPE_NUMERIC || *mode == IoTYPE_IMPLICIT)
	    {/*137*/SensorCall(10537);mode++;/*138*/}
	SensorCall(10545);switch (*mode++) {
	case 'r':
	    SensorCall(10538);l->flags |= PERLIO_F_CANREAD;
	    SensorCall(10539);break;
	case 'a':
	    SensorCall(10540);l->flags |= PERLIO_F_APPEND | PERLIO_F_CANWRITE;
	    SensorCall(10541);break;
	case 'w':
	    SensorCall(10542);l->flags |= PERLIO_F_TRUNCATE | PERLIO_F_CANWRITE;
	    SensorCall(10543);break;
	default:
	    SETERRNO(EINVAL, LIB_INVARG);
	    {IV  ReplaceReturn1499 = -1; SensorCall(10544); return ReplaceReturn1499;}
	}
	SensorCall(10554);while (*mode) {
	    SensorCall(10546);switch (*mode++) {
	    case '+':
		SensorCall(10547);l->flags |= PERLIO_F_CANREAD | PERLIO_F_CANWRITE;
		SensorCall(10548);break;
	    case 'b':
		SensorCall(10549);l->flags &= ~PERLIO_F_CRLF;
		SensorCall(10550);break;
	    case 't':
		SensorCall(10551);l->flags |= PERLIO_F_CRLF;
		SensorCall(10552);break;
	    default:
		SETERRNO(EINVAL, LIB_INVARG);
		{IV  ReplaceReturn1498 = -1; SensorCall(10553); return ReplaceReturn1498;}
	    }
	}
    }
    else {
	SensorCall(10555);if (l->next) {
	    SensorCall(10556);l->flags |= l->next->flags &
		(PERLIO_F_CANREAD | PERLIO_F_CANWRITE | PERLIO_F_TRUNCATE |
		 PERLIO_F_APPEND);
	}
    }
#if 0
    PerlIO_debug("PerlIOBase_pushed f=%p %s %s fl=%08" UVxf " (%s)\n",
		 (void*)f, PerlIOBase(f)->tab->name, (omode) ? omode : "(Null)",
		 l->flags, PerlIO_modestr(f, temp));
#endif
    {IV  ReplaceReturn1497 = 0; SensorCall(10558); return ReplaceReturn1497;}
}

IV
PerlIOBase_popped(pTHX_ PerlIO *f)
{
    PERL_UNUSED_CONTEXT;
    PERL_UNUSED_ARG(f);
    {IV  ReplaceReturn1496 = 0; SensorCall(10559); return ReplaceReturn1496;}
}

SSize_t
PerlIOBase_unread(pTHX_ PerlIO *f, const void *vbuf, Size_t count)
{
    /*
     * Save the position as current head considers it
     */
    SensorCall(10560);const Off_t old = PerlIO_tell(f);
    PerlIO_push(aTHX_ f, PERLIO_FUNCS_CAST(&PerlIO_pending), "r", NULL);
    PerlIOSelf(f, PerlIOBuf)->posn = old;
    {ssize_t  ReplaceReturn1495 = PerlIOBuf_unread(aTHX_ f, vbuf, count); SensorCall(10561); return ReplaceReturn1495;}
}

SSize_t
PerlIOBase_read(pTHX_ PerlIO *f, void *vbuf, Size_t count)
{
SensorCall(10562);    STDCHAR *buf = (STDCHAR *) vbuf;
    SensorCall(10576);if (f) {
        SensorCall(10563);if (!(PerlIOBase(f)->flags & PERLIO_F_CANREAD)) {
	    PerlIOBase(f)->flags |= PERLIO_F_ERROR;
	    SETERRNO(EBADF, SS_IVCHAN);
	    {ssize_t  ReplaceReturn1494 = 0; SensorCall(10564); return ReplaceReturn1494;}
	}
	SensorCall(10574);while (count > 0) {
	 get_cnt:
	  {
	    SSize_t avail = PerlIO_get_cnt(f);
	    SSize_t take = 0;
	    SensorCall(10566);if (avail > 0)
		{/*139*/SensorCall(10565);take = (((SSize_t) count >= 0) && ((SSize_t)count < avail)) ? (SSize_t)count : avail;/*140*/}
	    SensorCall(10570);if (take > 0) {
		STDCHAR *ptr = PerlIO_get_ptr(f);
		Copy(ptr, buf, take, STDCHAR);
		PerlIO_set_ptrcnt(f, ptr + take, (avail -= take));
		SensorCall(10567);count -= take;
		buf += take;
		SensorCall(10569);if (avail == 0)		/* set_ptrcnt could have reset avail */
		    {/*141*/SensorCall(10568);goto get_cnt;/*142*/}
	    }
	    SensorCall(10573);if (count > 0 && avail <= 0) {
		SensorCall(10571);if (PerlIO_fill(f) != 0)
		    {/*143*/SensorCall(10572);break;/*144*/}
	    }
	  }
	}
	{ssize_t  ReplaceReturn1493 = (buf - (STDCHAR *) vbuf); SensorCall(10575); return ReplaceReturn1493;}
    }
    {ssize_t  ReplaceReturn1492 = 0; SensorCall(10577); return ReplaceReturn1492;}
}

IV
PerlIOBase_noop_ok(pTHX_ PerlIO *f)
{
    PERL_UNUSED_CONTEXT;
    PERL_UNUSED_ARG(f);
    {IV  ReplaceReturn1491 = 0; SensorCall(10578); return ReplaceReturn1491;}
}

IV
PerlIOBase_noop_fail(pTHX_ PerlIO *f)
{
    PERL_UNUSED_CONTEXT;
    PERL_UNUSED_ARG(f);
    {IV  ReplaceReturn1490 = -1; SensorCall(10579); return ReplaceReturn1490;}
}

IV
PerlIOBase_close(pTHX_ PerlIO *f)
{
    SensorCall(10580);IV code = -1;
    SensorCall(10589);if (PerlIOValid(f)) {
	PerlIO *n = PerlIONext(f);
	SensorCall(10581);code = PerlIO_flush(f);
	PerlIOBase(f)->flags &=
	   ~(PERLIO_F_CANREAD | PERLIO_F_CANWRITE | PERLIO_F_OPEN);
	SensorCall(10588);while (PerlIOValid(n)) {
	    SensorCall(10582);const PerlIO_funcs * const tab = PerlIOBase(n)->tab;
	    SensorCall(10586);if (tab && tab->Close) {
		SensorCall(10583);if ((*tab->Close)(aTHX_ n) != 0)
		    {/*129*/SensorCall(10584);code = -1;/*130*/}
		SensorCall(10585);break;
	    }
	    else {
		PerlIOBase(n)->flags &=
		    ~(PERLIO_F_CANREAD | PERLIO_F_CANWRITE | PERLIO_F_OPEN);
	    }
	    SensorCall(10587);n = PerlIONext(n);
	}
    }
    else {
	SETERRNO(EBADF, SS_IVCHAN);
    }
    {IV  ReplaceReturn1489 = code; SensorCall(10590); return ReplaceReturn1489;}
}

IV
PerlIOBase_eof(pTHX_ PerlIO *f)
{
SensorCall(10591);    PERL_UNUSED_CONTEXT;
    SensorCall(10593);if (PerlIOValid(f)) {
	{IV  ReplaceReturn1488 = (PerlIOBase(f)->flags & PERLIO_F_EOF) != 0; SensorCall(10592); return ReplaceReturn1488;}
    }
    {IV  ReplaceReturn1487 = 1; SensorCall(10594); return ReplaceReturn1487;}
}

IV
PerlIOBase_error(pTHX_ PerlIO *f)
{
SensorCall(10595);    PERL_UNUSED_CONTEXT;
    SensorCall(10597);if (PerlIOValid(f)) {
	{IV  ReplaceReturn1486 = (PerlIOBase(f)->flags & PERLIO_F_ERROR) != 0; SensorCall(10596); return ReplaceReturn1486;}
    }
    {IV  ReplaceReturn1485 = 1; SensorCall(10598); return ReplaceReturn1485;}
}

void
PerlIOBase_clearerr(pTHX_ PerlIO *f)
{
    SensorCall(10599);if (PerlIOValid(f)) {
	PerlIO * const n = PerlIONext(f);
	PerlIOBase(f)->flags &= ~(PERLIO_F_ERROR | PERLIO_F_EOF);
	SensorCall(10600);if (PerlIOValid(n))
	    PerlIO_clearerr(n);
    }
SensorCall(10601);}

void
PerlIOBase_setlinebuf(pTHX_ PerlIO *f)
{
SensorCall(10602);    PERL_UNUSED_CONTEXT;
    SensorCall(10603);if (PerlIOValid(f)) {
	PerlIOBase(f)->flags |= PERLIO_F_LINEBUF;
    }
SensorCall(10604);}

SV *
PerlIO_sv_dup(pTHX_ SV *arg, CLONE_PARAMS *param)
{
    SensorCall(10605);if (!arg)
	return NULL;
#ifdef sv_dup
    SensorCall(10609);if (param) {
	SensorCall(10606);arg = sv_dup(arg, param);
	SvREFCNT_inc_simple_void_NN(arg);
	{SV * ReplaceReturn1484 = arg; SensorCall(10607); return ReplaceReturn1484;}
    }
    else {
	{SV * ReplaceReturn1483 = newSVsv(arg); SensorCall(10608); return ReplaceReturn1483;}
    }
#else
    PERL_UNUSED_ARG(param);
    return newSVsv(arg);
#endif
SensorCall(10610);}

PerlIO *
PerlIOBase_dup(pTHX_ PerlIO *f, PerlIO *o, CLONE_PARAMS *param, int flags)
{
SensorCall(10611);    PerlIO * const nexto = PerlIONext(o);
    SensorCall(10616);if (PerlIOValid(nexto)) {
	SensorCall(10612);const PerlIO_funcs * const tab = PerlIOBase(nexto)->tab;
	SensorCall(10615);if (tab && tab->Dup)
	    {/*131*/SensorCall(10613);f = (*tab->Dup)(aTHX_ f, nexto, param, flags);/*132*/}
	else
	    {/*133*/SensorCall(10614);f = PerlIOBase_dup(aTHX_ f, nexto, param, flags);/*134*/}
    }
    SensorCall(10622);if (f) {
	SensorCall(10617);PerlIO_funcs * const self = PerlIOBase(o)->tab;
	SV *arg = NULL;
	char buf[8];
	PerlIO_debug("PerlIOBase_dup %s f=%p o=%p param=%p\n",
		     self ? self->name : "(Null)",
		     (void*)f, (void*)o, (void*)param);
	SensorCall(10619);if (self && self->Getarg)
	    {/*135*/SensorCall(10618);arg = (*self->Getarg)(aTHX_ o, param, flags);/*136*/}
	SensorCall(10620);f = PerlIO_push(aTHX_ f, self, PerlIO_modestr(o,buf), arg);
	SensorCall(10621);if (PerlIOBase(o)->flags & PERLIO_F_UTF8)
	    PerlIOBase(f)->flags |= PERLIO_F_UTF8;
	SvREFCNT_dec(arg);
    }
    {PerlIO * ReplaceReturn1482 = f; SensorCall(10623); return ReplaceReturn1482;}
}

/* PL_perlio_fd_refcnt[] is in intrpvar.h */

/* Must be called with PL_perlio_mutex locked. */
static void
S_more_refcounted_fds(pTHX_ const int new_fd) {
SensorCall(10624);    dVAR;
    const int old_max = PL_perlio_fd_refcnt_size;
    const int new_max = 16 + (new_fd & ~15);
    int *new_array;

    PerlIO_debug("More fds - old=%d, need %d, new=%d\n",
		 old_max, new_fd, new_max);

    SensorCall(10626);if (new_fd < old_max) {
	SensorCall(10625);return;
    }

    assert (new_max > new_fd);

    /* Use plain realloc() since we need this memory to be really
     * global and visible to all the interpreters and/or threads. */
    SensorCall(10627);new_array = (int*) realloc(PL_perlio_fd_refcnt, new_max * sizeof(int));

    SensorCall(10628);if (!new_array) {
#ifdef USE_ITHREADS
	MUTEX_UNLOCK(&PL_perlio_mutex);
#endif
	/* Can't use PerlIO to write as it allocates memory */
	PerlLIO_write(PerlIO_fileno(Perl_error_log),
		      PL_no_mem, strlen(PL_no_mem));
	my_exit(1);
    }

    SensorCall(10629);PL_perlio_fd_refcnt_size = new_max;
    PL_perlio_fd_refcnt = new_array;

    PerlIO_debug("Zeroing %p, %d\n",
		 (void*)(new_array + old_max),
		 new_max - old_max);

    Zero(new_array + old_max, new_max - old_max, int);
}


void
PerlIO_init(pTHX)
{
SensorCall(10630);    /* MUTEX_INIT(&PL_perlio_mutex) is done in PERL_SYS_INIT3(). */
    PERL_UNUSED_CONTEXT;
SensorCall(10631);}

void
PerlIOUnix_refcnt_inc(int fd)
{
    dTHX;
    SensorCall(10639);if (fd >= 0) {
	dVAR;

#ifdef USE_ITHREADS
	MUTEX_LOCK(&PL_perlio_mutex);
#endif
	SensorCall(10633);if (fd >= PL_perlio_fd_refcnt_size)
	    {/*235*/SensorCall(10632);S_more_refcounted_fds(aTHX_ fd);/*236*/}

	SensorCall(10634);PL_perlio_fd_refcnt[fd]++;
	SensorCall(10636);if (PL_perlio_fd_refcnt[fd] <= 0) {
	    /* diag_listed_as: refcnt_inc: fd %d%s */
	    SensorCall(10635);Perl_croak(aTHX_ "refcnt_inc: fd %d: %d <= 0\n",
		       fd, PL_perlio_fd_refcnt[fd]);
	}
	SensorCall(10637);PerlIO_debug("refcnt_inc: fd %d refcnt=%d\n",
		     fd, PL_perlio_fd_refcnt[fd]);

#ifdef USE_ITHREADS
	MUTEX_UNLOCK(&PL_perlio_mutex);
#endif
    } else {
	/* diag_listed_as: refcnt_inc: fd %d%s */
	SensorCall(10638);Perl_croak(aTHX_ "refcnt_inc: fd %d < 0\n", fd);
    }
SensorCall(10640);}

int
PerlIOUnix_refcnt_dec(int fd)
{
    dTHX;
    int cnt = 0;
    SensorCall(10647);if (fd >= 0) {
	dVAR;
#ifdef USE_ITHREADS
	MUTEX_LOCK(&PL_perlio_mutex);
#endif
	SensorCall(10642);if (fd >= PL_perlio_fd_refcnt_size) {
	    /* diag_listed_as: refcnt_dec: fd %d%s */
	    SensorCall(10641);Perl_croak(aTHX_ "refcnt_dec: fd %d >= refcnt_size %d\n",
		       fd, PL_perlio_fd_refcnt_size);
	}
	SensorCall(10644);if (PL_perlio_fd_refcnt[fd] <= 0) {
	    /* diag_listed_as: refcnt_dec: fd %d%s */
	    SensorCall(10643);Perl_croak(aTHX_ "refcnt_dec: fd %d: %d <= 0\n",
		       fd, PL_perlio_fd_refcnt[fd]);
	}
	SensorCall(10645);cnt = --PL_perlio_fd_refcnt[fd];
	PerlIO_debug("refcnt_dec: fd %d refcnt=%d\n", fd, cnt);
#ifdef USE_ITHREADS
	MUTEX_UNLOCK(&PL_perlio_mutex);
#endif
    } else {
	/* diag_listed_as: refcnt_dec: fd %d%s */
	SensorCall(10646);Perl_croak(aTHX_ "refcnt_dec: fd %d < 0\n", fd);
    }
    {int  ReplaceReturn1481 = cnt; SensorCall(10648); return ReplaceReturn1481;}
}

int
PerlIOUnix_refcnt(int fd)
{
    dTHX;
    int cnt = 0;
    SensorCall(10655);if (fd >= 0) {
	dVAR;
#ifdef USE_ITHREADS
	MUTEX_LOCK(&PL_perlio_mutex);
#endif
	SensorCall(10650);if (fd >= PL_perlio_fd_refcnt_size) {
	    /* diag_listed_as: refcnt: fd %d%s */
	    SensorCall(10649);Perl_croak(aTHX_ "refcnt: fd %d >= refcnt_size %d\n",
		       fd, PL_perlio_fd_refcnt_size);
	}
	SensorCall(10652);if (PL_perlio_fd_refcnt[fd] <= 0) {
	    /* diag_listed_as: refcnt: fd %d%s */
	    SensorCall(10651);Perl_croak(aTHX_ "refcnt: fd %d: %d <= 0\n",
		       fd, PL_perlio_fd_refcnt[fd]);
	}
	SensorCall(10653);cnt = PL_perlio_fd_refcnt[fd];
#ifdef USE_ITHREADS
	MUTEX_UNLOCK(&PL_perlio_mutex);
#endif
    } else {
	/* diag_listed_as: refcnt: fd %d%s */
	SensorCall(10654);Perl_croak(aTHX_ "refcnt: fd %d < 0\n", fd);
    }
    {int  ReplaceReturn1480 = cnt; SensorCall(10656); return ReplaceReturn1480;}
}

void
PerlIO_cleanup(pTHX)
{
SensorCall(10657);    dVAR;
    int i;
#ifdef USE_ITHREADS
    PerlIO_debug("Cleanup layers for %p\n",(void*)aTHX);
#else
    PerlIO_debug("Cleanup layers\n");
#endif

    /* Raise STDIN..STDERR refcount so we don't close them */
    SensorCall(10659);for (i=0; i < 3; i++)
	{/*49*/SensorCall(10658);PerlIOUnix_refcnt_inc(i);/*50*/}
    SensorCall(10660);PerlIO_cleantable(aTHX_ &PL_perlio);
    /* Restore STDIN..STDERR refcount */
    SensorCall(10662);for (i=0; i < 3; i++)
	{/*51*/SensorCall(10661);PerlIOUnix_refcnt_dec(i);/*52*/}

    SensorCall(10664);if (PL_known_layers) {
	SensorCall(10663);PerlIO_list_free(aTHX_ PL_known_layers);
	PL_known_layers = NULL;
    }
    SensorCall(10666);if (PL_def_layerlist) {
	SensorCall(10665);PerlIO_list_free(aTHX_ PL_def_layerlist);
	PL_def_layerlist = NULL;
    }
SensorCall(10667);}

void PerlIO_teardown(void) /* Call only from PERL_SYS_TERM(). */
{
SensorCall(10668);    dVAR;
#if 0
/* XXX we can't rely on an interpreter being present at this late stage,
   XXX so we can't use a function like PerlLIO_write that relies on one
   being present (at least in win32) :-(.
   Disable for now.
*/
#ifdef DEBUGGING
    {
	/* By now all filehandles should have been closed, so any
	 * stray (non-STD-)filehandles indicate *possible* (PerlIO)
	 * errors. */
#define PERLIO_TEARDOWN_MESSAGE_BUF_SIZE 64
#define PERLIO_TEARDOWN_MESSAGE_FD 2
	char buf[PERLIO_TEARDOWN_MESSAGE_BUF_SIZE];
	int i;
	for (i = 3; i < PL_perlio_fd_refcnt_size; i++) {
	    if (PL_perlio_fd_refcnt[i]) {
		const STRLEN len =
		    my_snprintf(buf, sizeof(buf),
				"PerlIO_teardown: fd %d refcnt=%d\n",
				i, PL_perlio_fd_refcnt[i]);
		PerlLIO_write(PERLIO_TEARDOWN_MESSAGE_FD, buf, len);
	    }
	}
    }
#endif
#endif
    /* Not bothering with PL_perlio_mutex since by now
     * all the interpreters are gone. */
    SensorCall(10670);if (PL_perlio_fd_refcnt_size /* Assuming initial size of zero. */
        && PL_perlio_fd_refcnt) {
	SensorCall(10669);free(PL_perlio_fd_refcnt); /* To match realloc() in S_more_refcounted_fds(). */
	PL_perlio_fd_refcnt = NULL;
	PL_perlio_fd_refcnt_size = 0;
    }
SensorCall(10671);}

/*--------------------------------------------------------------------------------------*/
/*
 * Bottom-most level for UNIX-like case
 */

typedef struct {
    struct _PerlIO base;        /* The generic part */
    int fd;                     /* UNIX like file descriptor */
    int oflags;                 /* open/fcntl flags */
} PerlIOUnix;

static void
S_lockcnt_dec(pTHX_ const void* f)
{
SensorCall(10672);    PerlIO_lockcnt((PerlIO*)f)--;
}


/* call the signal handler, and if that handler happens to clear
 * this handle, free what we can and return true */

static bool
S_perlio_async_run(pTHX_ PerlIO* f) {
    ENTER;
    SAVEDESTRUCTOR_X(S_lockcnt_dec, (void*)f);
    PerlIO_lockcnt(f)++;
    PERL_ASYNC_CHECK();
    SensorCall(10674);if ( !(PerlIOBase(f)->flags & PERLIO_F_CLEARED) ) {
	LEAVE;
	{_Bool  ReplaceReturn1479 = 0; SensorCall(10673); return ReplaceReturn1479;}
    }
    /* we've just run some perl-level code that could have done
     * anything, including closing the file or clearing this layer.
     * If so, free any lower layers that have already been
     * cleared, then return an error. */
    SensorCall(10676);while (PerlIOValid(f) &&
	    (PerlIOBase(f)->flags & PERLIO_F_CLEARED))
    {
	SensorCall(10675);const PerlIOl *l = *f;
	*f = l->next;
	Safefree(l);
    }
    LEAVE;
    {_Bool  ReplaceReturn1478 = 1; SensorCall(10677); return ReplaceReturn1478;}
}

int
PerlIOUnix_oflags(const char *mode)
{
    SensorCall(10678);int oflags = -1;
    SensorCall(10680);if (*mode == IoTYPE_IMPLICIT || *mode == IoTYPE_NUMERIC)
	{/*217*/SensorCall(10679);mode++;/*218*/}
    SensorCall(10693);switch (*mode) {
    case 'r':
	SensorCall(10681);oflags = O_RDONLY;
	SensorCall(10683);if (*++mode == '+') {
	    SensorCall(10682);oflags = O_RDWR;
	    mode++;
	}
	SensorCall(10684);break;

    case 'w':
	SensorCall(10685);oflags = O_CREAT | O_TRUNC;
	SensorCall(10687);if (*++mode == '+') {
	    SensorCall(10686);oflags |= O_RDWR;
	    mode++;
	}
	else
	    oflags |= O_WRONLY;
	SensorCall(10688);break;

    case 'a':
	SensorCall(10689);oflags = O_CREAT | O_APPEND;
	SensorCall(10691);if (*++mode == '+') {
	    SensorCall(10690);oflags |= O_RDWR;
	    mode++;
	}
	else
	    oflags |= O_WRONLY;
	SensorCall(10692);break;
    }
    SensorCall(10697);if (*mode == 'b') {
	SensorCall(10694);oflags |= O_BINARY;
	oflags &= ~O_TEXT;
	mode++;
    }
    else {/*219*/SensorCall(10695);if (*mode == 't') {
	SensorCall(10696);oflags |= O_TEXT;
	oflags &= ~O_BINARY;
	mode++;
    ;/*220*/}}
    /*
     * Always open in binary mode
     */
    SensorCall(10698);oflags |= O_BINARY;
    SensorCall(10700);if (*mode || oflags == -1) {
	SETERRNO(EINVAL, LIB_INVARG);
	SensorCall(10699);oflags = -1;
    }
    {int  ReplaceReturn1477 = oflags; SensorCall(10701); return ReplaceReturn1477;}
}

IV
PerlIOUnix_fileno(pTHX_ PerlIO *f)
{
SensorCall(10702);    PERL_UNUSED_CONTEXT;
    {IV  ReplaceReturn1476 = PerlIOSelf(f, PerlIOUnix)->fd; SensorCall(10703); return ReplaceReturn1476;}
}

static void
PerlIOUnix_setfd(pTHX_ PerlIO *f, int fd, int imode)
{
    SensorCall(10704);PerlIOUnix * const s = PerlIOSelf(f, PerlIOUnix);
#if defined(WIN32)
    Stat_t st;
    if (PerlLIO_fstat(fd, &st) == 0) {
	if (!S_ISREG(st.st_mode)) {
	    PerlIO_debug("%d is not regular file\n",fd);
    	    PerlIOBase(f)->flags |= PERLIO_F_NOTREG;
	}
	else {
	    PerlIO_debug("%d _is_ a regular file\n",fd);
	}
    }
#endif
    s->fd = fd;
    s->oflags = imode;
    PerlIOUnix_refcnt_inc(fd);
    PERL_UNUSED_CONTEXT;
}

IV
PerlIOUnix_pushed(pTHX_ PerlIO *f, const char *mode, SV *arg, PerlIO_funcs *tab)
{
    SensorCall(10705);IV code = PerlIOBase_pushed(aTHX_ f, mode, arg, tab);
    SensorCall(10707);if (*PerlIONext(f)) {
	/* We never call down so do any pending stuff now */
	PerlIO_flush(PerlIONext(f));
	/*
	 * XXX could (or should) we retrieve the oflags from the open file
	 * handle rather than believing the "mode" we are passed in? XXX
	 * Should the value on NULL mode be 0 or -1?
	 */
        SensorCall(10706);PerlIOUnix_setfd(aTHX_ f, PerlIO_fileno(PerlIONext(f)),
                         mode ? PerlIOUnix_oflags(mode) : -1);
    }
    PerlIOBase(f)->flags |= PERLIO_F_OPEN;

    {IV  ReplaceReturn1475 = code; SensorCall(10708); return ReplaceReturn1475;}
}

IV
PerlIOUnix_seek(pTHX_ PerlIO *f, Off_t offset, int whence)
{
    SensorCall(10709);const int fd = PerlIOSelf(f, PerlIOUnix)->fd;
    Off_t new_loc;
    PERL_UNUSED_CONTEXT;
    SensorCall(10711);if (PerlIOBase(f)->flags & PERLIO_F_NOTREG) {
#ifdef  ESPIPE
	SETERRNO(ESPIPE, LIB_INVARG);
#else
	SETERRNO(EINVAL, LIB_INVARG);
#endif
	{IV  ReplaceReturn1474 = -1; SensorCall(10710); return ReplaceReturn1474;}
    }
    SensorCall(10712);new_loc = PerlLIO_lseek(fd, offset, whence);
    SensorCall(10714);if (new_loc == (Off_t) - 1)
	{/*237*/{IV  ReplaceReturn1473 = -1; SensorCall(10713); return ReplaceReturn1473;}/*238*/}
    PerlIOBase(f)->flags &= ~PERLIO_F_EOF;
    {IV  ReplaceReturn1472 = 0; SensorCall(10715); return ReplaceReturn1472;}
}

PerlIO *
PerlIOUnix_open(pTHX_ PerlIO_funcs *self, PerlIO_list_t *layers,
		IV n, const char *mode, int fd, int imode,
		int perm, PerlIO *f, int narg, SV **args)
{
    SensorCall(10716);if (PerlIOValid(f)) {
	SensorCall(10717);if (PerlIOBase(f)->tab && PerlIOBase(f)->flags & PERLIO_F_OPEN)
	    {/*221*/SensorCall(10718);(*PerlIOBase(f)->tab->Close)(aTHX_ f);/*222*/}
    }
    SensorCall(10724);if (narg > 0) {
	SensorCall(10719);if (*mode == IoTYPE_NUMERIC)
	    {/*223*/SensorCall(10720);mode++;/*224*/}
	else {
	    SensorCall(10721);imode = PerlIOUnix_oflags(mode);
#ifdef VMS
	    perm = 0777; /* preserve RMS defaults, ACL inheritance, etc. */
#else
	    perm = 0666;
#endif
	}
	SensorCall(10723);if (imode != -1) {
	    SensorCall(10722);const char *path = SvPV_nolen_const(*args);
	    fd = PerlLIO_open3(path, imode, perm);
	}
    }
    SensorCall(10738);if (fd >= 0) {
	SensorCall(10725);if (*mode == IoTYPE_IMPLICIT)
	    {/*225*/SensorCall(10726);mode++;/*226*/}
	SensorCall(10728);if (!f) {
	    SensorCall(10727);f = PerlIO_allocate(aTHX);
	}
	SensorCall(10731);if (!PerlIOValid(f)) {
	    SensorCall(10729);if (!(f = PerlIO_push(aTHX_ f, self, mode, PerlIOArg))) {
		{PerlIO * ReplaceReturn1471 = NULL; SensorCall(10730); return ReplaceReturn1471;}
	    }
	}
        SensorCall(10732);PerlIOUnix_setfd(aTHX_ f, fd, imode);
	PerlIOBase(f)->flags |= PERLIO_F_OPEN;
	SensorCall(10734);if (*mode == IoTYPE_APPEND)
	    {/*227*/SensorCall(10733);PerlIOUnix_seek(aTHX_ f, 0, SEEK_END);/*228*/}
	{PerlIO * ReplaceReturn1470 = f; SensorCall(10735); return ReplaceReturn1470;}
    }
    else {
	SensorCall(10736);if (f) {
	    NOOP;
	    /*
	     * FIXME: pop layers ???
	     */
	}
	{PerlIO * ReplaceReturn1469 = NULL; SensorCall(10737); return ReplaceReturn1469;}
    }
SensorCall(10739);}

PerlIO *
PerlIOUnix_dup(pTHX_ PerlIO *f, PerlIO *o, CLONE_PARAMS *param, int flags)
{
    SensorCall(10740);const PerlIOUnix * const os = PerlIOSelf(o, PerlIOUnix);
    int fd = os->fd;
    SensorCall(10742);if (flags & PERLIO_DUP_FD) {
	SensorCall(10741);fd = PerlLIO_dup(fd);
    }
    SensorCall(10747);if (fd >= 0) {
	SensorCall(10743);f = PerlIOBase_dup(aTHX_ f, o, param, flags);
	SensorCall(10746);if (f) {
	    /* If all went well overwrite fd in dup'ed lay with the dup()'ed fd */
	    SensorCall(10744);PerlIOUnix_setfd(aTHX_ f, fd, os->oflags);
	    {PerlIO * ReplaceReturn1468 = f; SensorCall(10745); return ReplaceReturn1468;}
	}
    }
    {PerlIO * ReplaceReturn1467 = NULL; SensorCall(10748); return ReplaceReturn1467;}
}


SSize_t
PerlIOUnix_read(pTHX_ PerlIO *f, void *vbuf, Size_t count)
{
SensorCall(10749);    dVAR;
    int fd;
    SensorCall(10751);if (PerlIO_lockcnt(f)) /* in use: abort ungracefully */
	{/*229*/{ssize_t  ReplaceReturn1466 = -1; SensorCall(10750); return ReplaceReturn1466;}/*230*/}
    SensorCall(10752);fd = PerlIOSelf(f, PerlIOUnix)->fd;
#ifdef PERLIO_STD_SPECIAL
    if (fd == 0)
        return PERLIO_STD_IN(fd, vbuf, count);
#endif
    SensorCall(10754);if (!(PerlIOBase(f)->flags & PERLIO_F_CANREAD) ||
         PerlIOBase(f)->flags & (PERLIO_F_EOF|PERLIO_F_ERROR)) {
	{ssize_t  ReplaceReturn1465 = 0; SensorCall(10753); return ReplaceReturn1465;}
    }
    SensorCall(10763);while (1) {
	SensorCall(10755);const SSize_t len = PerlLIO_read(fd, vbuf, count);
	SensorCall(10760);if (len >= 0 || errno != EINTR) {
	    SensorCall(10756);if (len < 0) {
		SensorCall(10757);if (errno != EAGAIN) {
		    PerlIOBase(f)->flags |= PERLIO_F_ERROR;
		}
	    }
	    else {/*231*/SensorCall(10758);if (len == 0 && count != 0) {
		PerlIOBase(f)->flags |= PERLIO_F_EOF;
		SETERRNO(0,0);
	    ;/*232*/}}
	    {ssize_t  ReplaceReturn1464 = len; SensorCall(10759); return ReplaceReturn1464;}
	}
	/* EINTR */
	SensorCall(10762);if (PL_sig_pending && S_perlio_async_run(aTHX_ f))
	    {/*233*/{ssize_t  ReplaceReturn1463 = -1; SensorCall(10761); return ReplaceReturn1463;}/*234*/}
    }
    /*NOTREACHED*/
SensorCall(10764);}

SSize_t
PerlIOUnix_write(pTHX_ PerlIO *f, const void *vbuf, Size_t count)
{
SensorCall(10765);    dVAR;
    int fd;
    SensorCall(10767);if (PerlIO_lockcnt(f)) /* in use: abort ungracefully */
	{/*239*/{ssize_t  ReplaceReturn1462 = -1; SensorCall(10766); return ReplaceReturn1462;}/*240*/}
    SensorCall(10768);fd = PerlIOSelf(f, PerlIOUnix)->fd;
#ifdef PERLIO_STD_SPECIAL
    if (fd == 1 || fd == 2)
        return PERLIO_STD_OUT(fd, vbuf, count);
#endif
    SensorCall(10776);while (1) {
	SensorCall(10769);const SSize_t len = PerlLIO_write(fd, vbuf, count);
	SensorCall(10773);if (len >= 0 || errno != EINTR) {
	    SensorCall(10770);if (len < 0) {
		SensorCall(10771);if (errno != EAGAIN) {
		    PerlIOBase(f)->flags |= PERLIO_F_ERROR;
		}
	    }
	    {ssize_t  ReplaceReturn1461 = len; SensorCall(10772); return ReplaceReturn1461;}
	}
	/* EINTR */
	SensorCall(10775);if (PL_sig_pending && S_perlio_async_run(aTHX_ f))
	    {/*241*/{ssize_t  ReplaceReturn1460 = -1; SensorCall(10774); return ReplaceReturn1460;}/*242*/}
    }
    /*NOTREACHED*/
SensorCall(10777);}

Off_t
PerlIOUnix_tell(pTHX_ PerlIO *f)
{
SensorCall(10778);    PERL_UNUSED_CONTEXT;

    {off64_t  ReplaceReturn1459 = PerlLIO_lseek(PerlIOSelf(f, PerlIOUnix)->fd, 0, SEEK_CUR); SensorCall(10779); return ReplaceReturn1459;}
}


IV
PerlIOUnix_close(pTHX_ PerlIO *f)
{
SensorCall(10780);    dVAR;
    const int fd = PerlIOSelf(f, PerlIOUnix)->fd;
    int code = 0;
    SensorCall(10784);if (PerlIOBase(f)->flags & PERLIO_F_OPEN) {
	SensorCall(10781);if (PerlIOUnix_refcnt_dec(fd) > 0) {
	    PerlIOBase(f)->flags &= ~PERLIO_F_OPEN;
	    {IV  ReplaceReturn1458 = 0; SensorCall(10782); return ReplaceReturn1458;}
	}
    }
    else {
	SETERRNO(EBADF,SS_IVCHAN);
	{IV  ReplaceReturn1457 = -1; SensorCall(10783); return ReplaceReturn1457;}
    }
    SensorCall(10790);while (PerlLIO_close(fd) != 0) {
	SensorCall(10785);if (errno != EINTR) {
	    SensorCall(10786);code = -1;
	    SensorCall(10787);break;
	}
	/* EINTR */
	SensorCall(10789);if (PL_sig_pending && S_perlio_async_run(aTHX_ f))
	    {/*215*/{IV  ReplaceReturn1456 = -1; SensorCall(10788); return ReplaceReturn1456;}/*216*/}
    }
    SensorCall(10791);if (code == 0) {
	PerlIOBase(f)->flags &= ~PERLIO_F_OPEN;
    }
    {IV  ReplaceReturn1455 = code; SensorCall(10792); return ReplaceReturn1455;}
}

PERLIO_FUNCS_DECL(PerlIO_unix) = {
    sizeof(PerlIO_funcs),
    "unix",
    sizeof(PerlIOUnix),
    PERLIO_K_RAW,
    PerlIOUnix_pushed,
    PerlIOBase_popped,
    PerlIOUnix_open,
    PerlIOBase_binmode,         /* binmode */
    NULL,
    PerlIOUnix_fileno,
    PerlIOUnix_dup,
    PerlIOUnix_read,
    PerlIOBase_unread,
    PerlIOUnix_write,
    PerlIOUnix_seek,
    PerlIOUnix_tell,
    PerlIOUnix_close,
    PerlIOBase_noop_ok,         /* flush */
    PerlIOBase_noop_fail,       /* fill */
    PerlIOBase_eof,
    PerlIOBase_error,
    PerlIOBase_clearerr,
    PerlIOBase_setlinebuf,
    NULL,                       /* get_base */
    NULL,                       /* get_bufsiz */
    NULL,                       /* get_ptr */
    NULL,                       /* get_cnt */
    NULL,                       /* set_ptrcnt */
};

/*--------------------------------------------------------------------------------------*/
/*
 * stdio as a layer
 */

#if defined(VMS) && !defined(STDIO_BUFFER_WRITABLE)
/* perl5.8 - This ensures the last minute VMS ungetc fix is not
   broken by the last second glibc 2.3 fix
 */
#define STDIO_BUFFER_WRITABLE
#endif


typedef struct {
    struct _PerlIO base;
    FILE *stdio;                /* The stream */
} PerlIOStdio;

IV
PerlIOStdio_fileno(pTHX_ PerlIO *f)
{
SensorCall(10793);    PERL_UNUSED_CONTEXT;

    SensorCall(10796);if (PerlIOValid(f)) {
	SensorCall(10794);FILE * const s = PerlIOSelf(f, PerlIOStdio)->stdio;
	SensorCall(10795);if (s)
	    return PerlSIO_fileno(s);
    }
    errno = EBADF;
    {IV  ReplaceReturn1454 = -1; SensorCall(10797); return ReplaceReturn1454;}
}

char *
PerlIOStdio_mode(const char *mode, char *tmode)
{
    SensorCall(10798);char * const ret = tmode;
    SensorCall(10801);if (mode) {
	SensorCall(10799);while (*mode) {
	    SensorCall(10800);*tmode++ = *mode++;
	}
    }
#if defined(PERLIO_USING_CRLF) || defined(__CYGWIN__)
    *tmode++ = 'b';
#endif
    SensorCall(10802);*tmode = '\0';
    {char * ReplaceReturn1453 = ret; SensorCall(10803); return ReplaceReturn1453;}
}

IV
PerlIOStdio_pushed(pTHX_ PerlIO *f, const char *mode, SV *arg, PerlIO_funcs *tab)
{
SensorCall(10804);    PerlIO *n;
    SensorCall(10812);if (PerlIOValid(f) && PerlIOValid(n = PerlIONext(f))) {
	SensorCall(10805);PerlIO_funcs * const toptab = PerlIOBase(n)->tab;
        SensorCall(10811);if (toptab == tab) {
	    /* Top is already stdio - pop self (duplicate) and use original */
	    SensorCall(10806);PerlIO_pop(aTHX_ f);
	    {IV  ReplaceReturn1452 = 0; SensorCall(10807); return ReplaceReturn1452;}
	} else {
	    SensorCall(10808);const int fd = PerlIO_fileno(n);
	    char tmode[8];
	    FILE *stdio;
	    SensorCall(10810);if (fd >= 0 && (stdio  = PerlSIO_fdopen(fd,
			    mode = PerlIOStdio_mode(mode, tmode)))) {
		PerlIOSelf(f, PerlIOStdio)->stdio = stdio;
	    	/* We never call down so do any pending stuff now */
	    	PerlIO_flush(PerlIONext(f));
	    }
	    else {
		{IV  ReplaceReturn1451 = -1; SensorCall(10809); return ReplaceReturn1451;}
	    }
        }
    }
    {IV  ReplaceReturn1450 = PerlIOBase_pushed(aTHX_ f, mode, arg, tab); SensorCall(10813); return ReplaceReturn1450;}
}


PerlIO *
PerlIO_importFILE(FILE *stdio, const char *mode)
{
    dTHX;
    PerlIO *f = NULL;
    SensorCall(10826);if (stdio) {
	SensorCall(10814);PerlIOStdio *s;
	SensorCall(10823);if (!mode || !*mode) {
	    /* We need to probe to see how we can open the stream
	       so start with read/write and then try write and read
	       we dup() so that we can fclose without loosing the fd.

	       Note that the errno value set by a failing fdopen
	       varies between stdio implementations.
	     */
	    SensorCall(10815);const int fd = PerlLIO_dup(fileno(stdio));
	    FILE *f2 = PerlSIO_fdopen(fd, (mode = "r+"));
	    SensorCall(10817);if (!f2) {
		SensorCall(10816);f2 = PerlSIO_fdopen(fd, (mode = "w"));
	    }
	    SensorCall(10819);if (!f2) {
		SensorCall(10818);f2 = PerlSIO_fdopen(fd, (mode = "r"));
	    }
	    SensorCall(10821);if (!f2) {
		/* Don't seem to be able to open */
		PerlLIO_close(fd);
		{PerlIO * ReplaceReturn1449 = f; SensorCall(10820); return ReplaceReturn1449;}
	    }
	    SensorCall(10822);fclose(f2);
	}
	SensorCall(10825);if ((f = PerlIO_push(aTHX_(f = PerlIO_allocate(aTHX)), PERLIO_FUNCS_CAST(&PerlIO_stdio), mode, NULL))) {
	    SensorCall(10824);s = PerlIOSelf(f, PerlIOStdio);
	    s->stdio = stdio;
	    PerlIOUnix_refcnt_inc(fileno(stdio));
	}
    }
    {PerlIO * ReplaceReturn1448 = f; SensorCall(10827); return ReplaceReturn1448;}
}

PerlIO *
PerlIOStdio_open(pTHX_ PerlIO_funcs *self, PerlIO_list_t *layers,
		 IV n, const char *mode, int fd, int imode,
		 int perm, PerlIO *f, int narg, SV **args)
{
    SensorCall(10828);char tmode[8];
    SensorCall(10867);if (PerlIOValid(f)) {
	SensorCall(10829);const char * const path = SvPV_nolen_const(*args);
	PerlIOStdio * const s = PerlIOSelf(f, PerlIOStdio);
	FILE *stdio;
	PerlIOUnix_refcnt_dec(fileno(s->stdio));
	stdio = PerlSIO_freopen(path, (mode = PerlIOStdio_mode(mode, tmode)),
			    s->stdio);
	SensorCall(10830);if (!s->stdio)
	    return NULL;
	SensorCall(10831);s->stdio = stdio;
	PerlIOUnix_refcnt_inc(fileno(s->stdio));
	{PerlIO * ReplaceReturn1447 = f; SensorCall(10832); return ReplaceReturn1447;}
    }
    else {
	SensorCall(10833);if (narg > 0) {
	    SensorCall(10834);const char * const path = SvPV_nolen_const(*args);
	    SensorCall(10847);if (*mode == IoTYPE_NUMERIC) {
		SensorCall(10835);mode++;
		fd = PerlLIO_open3(path, imode, perm);
	    }
	    else {
	        SensorCall(10836);FILE *stdio;
	        bool appended = FALSE;
#ifdef __CYGWIN__
		/* Cygwin wants its 'b' early. */
		appended = TRUE;
		mode = PerlIOStdio_mode(mode, tmode);
#endif
		stdio = PerlSIO_fopen(path, mode);
		SensorCall(10846);if (stdio) {
		    SensorCall(10837);if (!f) {
			SensorCall(10838);f = PerlIO_allocate(aTHX);
		    }
		    SensorCall(10840);if (!appended)
		        {/*199*/SensorCall(10839);mode = PerlIOStdio_mode(mode, tmode);/*200*/}
		    SensorCall(10841);f = PerlIO_push(aTHX_ f, self, mode, PerlIOArg);
		    SensorCall(10843);if (f) {
			PerlIOSelf(f, PerlIOStdio)->stdio = stdio;
			SensorCall(10842);PerlIOUnix_refcnt_inc(fileno(stdio));
		    } else {
			PerlSIO_fclose(stdio);
		    }
		    {PerlIO * ReplaceReturn1446 = f; SensorCall(10844); return ReplaceReturn1446;}
		}
		else {
		    {PerlIO * ReplaceReturn1445 = NULL; SensorCall(10845); return ReplaceReturn1445;}
		}
	    }
	}
	SensorCall(10866);if (fd >= 0) {
	    SensorCall(10848);FILE *stdio = NULL;
	    int init = 0;
	    SensorCall(10850);if (*mode == IoTYPE_IMPLICIT) {
		SensorCall(10849);init = 1;
		mode++;
	    }
	    SensorCall(10859);if (init) {
		SensorCall(10851);switch (fd) {
		case 0:
		    SensorCall(10852);stdio = PerlSIO_stdin;
		    SensorCall(10853);break;
		case 1:
		    SensorCall(10854);stdio = PerlSIO_stdout;
		    SensorCall(10855);break;
		case 2:
		    SensorCall(10856);stdio = PerlSIO_stderr;
		    SensorCall(10857);break;
		}
	    }
	    else {
		SensorCall(10858);stdio = PerlSIO_fdopen(fd, mode =
				       PerlIOStdio_mode(mode, tmode));
	    }
	    SensorCall(10865);if (stdio) {
		SensorCall(10860);if (!f) {
		    SensorCall(10861);f = PerlIO_allocate(aTHX);
		}
		SensorCall(10863);if ((f = PerlIO_push(aTHX_ f, self, mode, PerlIOArg))) {
		    PerlIOSelf(f, PerlIOStdio)->stdio = stdio;
		    SensorCall(10862);PerlIOUnix_refcnt_inc(fileno(stdio));
		}
		{PerlIO * ReplaceReturn1444 = f; SensorCall(10864); return ReplaceReturn1444;}
	    }
	}
    }
    {PerlIO * ReplaceReturn1443 = NULL; SensorCall(10868); return ReplaceReturn1443;}
}

PerlIO *
PerlIOStdio_dup(pTHX_ PerlIO *f, PerlIO *o, CLONE_PARAMS *param, int flags)
{
    /* This assumes no layers underneath - which is what
       happens, but is not how I remember it. NI-S 2001/10/16
     */
    SensorCall(10869);if ((f = PerlIOBase_dup(aTHX_ f, o, param, flags))) {
	SensorCall(10870);FILE *stdio = PerlIOSelf(o, PerlIOStdio)->stdio;
	const int fd = fileno(stdio);
	char mode[8];
	SensorCall(10875);if (flags & PERLIO_DUP_FD) {
	    SensorCall(10871);const int dfd = PerlLIO_dup(fileno(stdio));
	    SensorCall(10874);if (dfd >= 0) {
		SensorCall(10872);stdio = PerlSIO_fdopen(dfd, PerlIO_modestr(o,mode));
		SensorCall(10873);goto set_this;
	    }
	    else {
		NOOP;
		/* FIXME: To avoid messy error recovery if dup fails
		   re-use the existing stdio as though flag was not set
		 */
	    }
	}
    	SensorCall(10876);stdio = PerlSIO_fdopen(fd, PerlIO_modestr(o,mode));
    set_this:
	PerlIOSelf(f, PerlIOStdio)->stdio = stdio;
        SensorCall(10878);if(stdio) {
	    SensorCall(10877);PerlIOUnix_refcnt_inc(fileno(stdio));
        }
    }
    {PerlIO * ReplaceReturn1442 = f; SensorCall(10879); return ReplaceReturn1442;}
}

static int
PerlIOStdio_invalidate_fileno(pTHX_ FILE *f)
{
SensorCall(10880);    PERL_UNUSED_CONTEXT;

    /* XXX this could use PerlIO_canset_fileno() and
     * PerlIO_set_fileno() support from Configure
     */
#  if defined(__UCLIBC__)
    /* uClibc must come before glibc because it defines __GLIBC__ as well. */
    f->__filedes = -1;
    return 1;
#  elif defined(__GLIBC__)
    /* There may be a better way for GLIBC:
    	- libio.h defines a flag to not close() on cleanup
     */	
    f->_fileno = -1;
    {int  ReplaceReturn1441 = 1; SensorCall(10881); return ReplaceReturn1441;}
#  elif defined(__sun__)
    PERL_UNUSED_ARG(f);
    return 0;
#  elif defined(__hpux)
    f->__fileH = 0xff;
    f->__fileL = 0xff;
    return 1;
   /* Next one ->_file seems to be a reasonable fallback, i.e. if
      your platform does not have special entry try this one.
      [For OSF only have confirmation for Tru64 (alpha)
      but assume other OSFs will be similar.]
    */
#  elif defined(_AIX) || defined(__osf__) || defined(__irix__)
    f->_file = -1;
    return 1;
#  elif defined(__FreeBSD__)
    /* There may be a better way on FreeBSD:
        - we could insert a dummy func in the _close function entry
	f->_close = (int (*)(void *)) dummy_close;
     */
    f->_file = -1;
    return 1;
#  elif defined(__OpenBSD__)
    /* There may be a better way on OpenBSD:
        - we could insert a dummy func in the _close function entry
	f->_close = (int (*)(void *)) dummy_close;
     */
    f->_file = -1;
    return 1;
#  elif defined(__EMX__)
    /* f->_flags &= ~_IOOPEN; */	/* Will leak stream->_buffer */
    f->_handle = -1;
    return 1;
#  elif defined(__CYGWIN__)
    /* There may be a better way on CYGWIN:
        - we could insert a dummy func in the _close function entry
	f->_close = (int (*)(void *)) dummy_close;
     */
    f->_file = -1;
    return 1;
#  elif defined(WIN32)
#    if defined(UNDER_CE)
    /* WIN_CE does not have access to FILE internals, it hardly has FILE
       structure at all
     */
#    else
    f->_file = -1;
#    endif
    return 1;
#  else
#if 0
    /* Sarathy's code did this - we fall back to a dup/dup2 hack
       (which isn't thread safe) instead
     */
#    error "Don't know how to set FILE.fileno on your platform"
#endif
    PERL_UNUSED_ARG(f);
    return 0;
#  endif
}

IV
PerlIOStdio_close(pTHX_ PerlIO *f)
{
    SensorCall(10882);FILE * const stdio = PerlIOSelf(f, PerlIOStdio)->stdio;
    SensorCall(10900);if (!stdio) {
	errno = EBADF;
	{IV  ReplaceReturn1440 = -1; SensorCall(10883); return ReplaceReturn1440;}
    }
    else {
        SensorCall(10884);const int fd = fileno(stdio);
	int invalidate = 0;
	IV result = 0;
	int dupfd = -1;
	dSAVEDERRNO;
#ifdef USE_ITHREADS
	dVAR;
#endif
#ifdef SOCKS5_VERSION_NAME
    	/* Socks lib overrides close() but stdio isn't linked to
	   that library (though we are) - so we must call close()
	   on sockets on stdio's behalf.
	 */
    	int optval;
    	Sock_size_t optlen = sizeof(int);
	if (getsockopt(fd, SOL_SOCKET, SO_TYPE, (void *) &optval, &optlen) == 0)
	    invalidate = 1;
#endif
	/* Test for -1, as *BSD stdio (at least) on fclose sets the FILE* such
	   that a subsequent fileno() on it returns -1. Don't want to croak()
	   from within PerlIOUnix_refcnt_dec() if some buggy caller code is
	   trying to close an already closed handle which somehow it still has
	   a reference to. (via.xs, I'm looking at you).  */
	SensorCall(10886);if (fd != -1 && PerlIOUnix_refcnt_dec(fd) > 0) {
	    /* File descriptor still in use */
	    SensorCall(10885);invalidate = 1;
	}
	SensorCall(10894);if (invalidate) {
	    /* For STD* handles, don't close stdio, since we shared the FILE *, too. */
	    SensorCall(10887);if (stdio == stdin) /* Some stdios are buggy fflush-ing inputs */
		{/*191*/{IV  ReplaceReturn1439 = 0; SensorCall(10888); return ReplaceReturn1439;}/*192*/}
	    SensorCall(10889);if (stdio == stdout || stdio == stderr)
		return PerlIO_flush(f);
            /* Tricky - must fclose(stdio) to free memory but not close(fd)
	       Use Sarathy's trick from maint-5.6 to invalidate the
	       fileno slot of the FILE *
	    */
	    SensorCall(10890);result = PerlIO_flush(f);
	    SAVE_ERRNO;
	    invalidate = PerlIOStdio_invalidate_fileno(aTHX_ stdio);
	    SensorCall(10893);if (!invalidate) {
#ifdef USE_ITHREADS
		MUTEX_LOCK(&PL_perlio_mutex);
		/* Right. We need a mutex here because for a brief while we
		   will have the situation that fd is actually closed. Hence if
		   a second thread were to get into this block, its dup() would
		   likely return our fd as its dupfd. (after all, it is closed)
		   Then if we get to the dup2() first, we blat the fd back
		   (messing up its temporary as a side effect) only for it to
		   then close its dupfd (== our fd) in its close(dupfd) */

		/* There is, of course, a race condition, that any other thread
		   trying to input/output/whatever on this fd will be stuffed
		   for the duration of this little manoeuvrer. Perhaps we
		   should hold an IO mutex for the duration of every IO
		   operation if we know that invalidate doesn't work on this
		   platform, but that would suck, and could kill performance.

		   Except that correctness trumps speed.
		   Advice from klortho #11912. */
#endif
		SensorCall(10891);dupfd = PerlLIO_dup(fd);
#ifdef USE_ITHREADS
		SensorCall(10892);if (dupfd < 0) {
		    MUTEX_UNLOCK(&PL_perlio_mutex);
		    /* Oh cXap. This isn't going to go well. Not sure if we can
		       recover from here, or if closing this particular FILE *
		       is a good idea now.  */
		}
#endif
	    }
	} else {
	    SAVE_ERRNO;   /* This is here only to silence compiler warnings */
	}
        SensorCall(10895);result = PerlSIO_fclose(stdio);
	/* We treat error from stdio as success if we invalidated
	   errno may NOT be expected EBADF
	 */
	SensorCall(10897);if (invalidate && result != 0) {
	    RESTORE_ERRNO;
	    SensorCall(10896);result = 0;
	}
#ifdef SOCKS5_VERSION_NAME
	/* in SOCKS' case, let close() determine return value */
	result = close(fd);
#endif
	SensorCall(10898);if (dupfd >= 0) {
	    PerlLIO_dup2(dupfd,fd);
	    PerlLIO_close(dupfd);
#ifdef USE_ITHREADS
	    MUTEX_UNLOCK(&PL_perlio_mutex);
#endif
	}
	{IV  ReplaceReturn1438 = result; SensorCall(10899); return ReplaceReturn1438;}
    }
SensorCall(10901);}

SSize_t
PerlIOStdio_read(pTHX_ PerlIO *f, void *vbuf, Size_t count)
{
SensorCall(10902);    dVAR;
    FILE * s;
    SSize_t got = 0;
    SensorCall(10904);if (PerlIO_lockcnt(f)) /* in use: abort ungracefully */
	{/*201*/{ssize_t  ReplaceReturn1437 = -1; SensorCall(10903); return ReplaceReturn1437;}/*202*/}
    SensorCall(10905);s = PerlIOSelf(f, PerlIOStdio)->stdio;
    SensorCall(10916);for (;;) {
	SensorCall(10906);if (count == 1) {
SensorCall(10907);	    STDCHAR *buf = (STDCHAR *) vbuf;
	    /*
	     * Perl is expecting PerlIO_getc() to fill the buffer Linux's
	     * stdio does not do that for fread()
	     */
	    const int ch = PerlSIO_fgetc(s);
	    SensorCall(10909);if (ch != EOF) {
		SensorCall(10908);*buf = ch;
		got = 1;
	    }
	}
	else
	    got = PerlSIO_fread(vbuf, 1, count, s);
	SensorCall(10911);if (got == 0 && PerlSIO_ferror(s))
	    {/*203*/SensorCall(10910);got = -1;/*204*/}
	SensorCall(10913);if (got >= 0 || errno != EINTR)
	    {/*205*/SensorCall(10912);break;/*206*/}
	SensorCall(10915);if (PL_sig_pending && S_perlio_async_run(aTHX_ f))
	    {/*207*/{ssize_t  ReplaceReturn1436 = -1; SensorCall(10914); return ReplaceReturn1436;}/*208*/}
	SETERRNO(0,0);	/* just in case */
    }
    {ssize_t  ReplaceReturn1435 = got; SensorCall(10917); return ReplaceReturn1435;}
}

SSize_t
PerlIOStdio_unread(pTHX_ PerlIO *f, const void *vbuf, Size_t count)
{
SensorCall(10918);    SSize_t unread = 0;
    FILE * const s = PerlIOSelf(f, PerlIOStdio)->stdio;

#ifdef STDIO_BUFFER_WRITABLE
    if (PerlIO_fast_gets(f) && PerlIO_has_base(f)) {
	STDCHAR *buf = ((STDCHAR *) vbuf) + count;
	STDCHAR *base = PerlIO_get_base(f);
	SSize_t cnt   = PerlIO_get_cnt(f);
	STDCHAR *ptr  = PerlIO_get_ptr(f);
	SSize_t avail = ptr - base;
	if (avail > 0) {
	    if (avail > count) {
		avail = count;
	    }
	    ptr -= avail;
	    Move(buf-avail,ptr,avail,STDCHAR);
	    count -= avail;
	    unread += avail;
	    PerlIO_set_ptrcnt(f,ptr,cnt+avail);
	    if (PerlSIO_feof(s) && unread >= 0)
		PerlSIO_clearerr(s);
	}
    }
    else
#endif
    SensorCall(10928);if (PerlIO_has_cntptr(f)) {
	/* We can get pointer to buffer but not its base
	   Do ungetc() but check chars are ending up in the
	   buffer
	 */
SensorCall(10919);	STDCHAR *eptr = (STDCHAR*)PerlSIO_get_ptr(s);
	STDCHAR *buf = ((STDCHAR *) vbuf) + count;
	SensorCall(10927);while (count > 0) {
	    SensorCall(10920);const int ch = *--buf & 0xFF;
	    SensorCall(10922);if (ungetc(ch,s) != ch) {
		/* ungetc did not work */
		SensorCall(10921);break;
	    }
	    SensorCall(10925);if ((STDCHAR*)PerlSIO_get_ptr(s) != --eptr || ((*eptr & 0xFF) != ch)) {
		/* Did not change pointer as expected */
		SensorCall(10923);fgetc(s);  /* get char back again */
		SensorCall(10924);break;
	    }
	    /* It worked ! */
	    SensorCall(10926);count--;
	    unread++;
	}
    }

    SensorCall(10930);if (count > 0) {
	SensorCall(10929);unread += PerlIOBase_unread(aTHX_ f, vbuf, count);
    }
    {ssize_t  ReplaceReturn1434 = unread; SensorCall(10931); return ReplaceReturn1434;}
}

SSize_t
PerlIOStdio_write(pTHX_ PerlIO *f, const void *vbuf, Size_t count)
{
SensorCall(10932);    dVAR;
    SSize_t got;
    SensorCall(10934);if (PerlIO_lockcnt(f)) /* in use: abort ungracefully */
	{/*209*/{ssize_t  ReplaceReturn1433 = -1; SensorCall(10933); return ReplaceReturn1433;}/*210*/}
    SensorCall(10940);for (;;) {
	SensorCall(10935);got = PerlSIO_fwrite(vbuf, 1, count,
			      PerlIOSelf(f, PerlIOStdio)->stdio);
	SensorCall(10937);if (got >= 0 || errno != EINTR)
	    {/*211*/SensorCall(10936);break;/*212*/}
	SensorCall(10939);if (PL_sig_pending && S_perlio_async_run(aTHX_ f))
	    {/*213*/{ssize_t  ReplaceReturn1432 = -1; SensorCall(10938); return ReplaceReturn1432;}/*214*/}
	SETERRNO(0,0);	/* just in case */
    }
    {ssize_t  ReplaceReturn1431 = got; SensorCall(10941); return ReplaceReturn1431;}
}

IV
PerlIOStdio_seek(pTHX_ PerlIO *f, Off_t offset, int whence)
{
    SensorCall(10942);FILE * const stdio = PerlIOSelf(f, PerlIOStdio)->stdio;
    PERL_UNUSED_CONTEXT;

    {IV  ReplaceReturn1430 = PerlSIO_fseek(stdio, offset, whence); SensorCall(10943); return ReplaceReturn1430;}
}

Off_t
PerlIOStdio_tell(pTHX_ PerlIO *f)
{
    SensorCall(10944);FILE * const stdio = PerlIOSelf(f, PerlIOStdio)->stdio;
    PERL_UNUSED_CONTEXT;

    {off64_t  ReplaceReturn1429 = PerlSIO_ftell(stdio); SensorCall(10945); return ReplaceReturn1429;}
}

IV
PerlIOStdio_flush(pTHX_ PerlIO *f)
{
    SensorCall(10946);FILE * const stdio = PerlIOSelf(f, PerlIOStdio)->stdio;
    PERL_UNUSED_CONTEXT;

    SensorCall(10948);if (PerlIOBase(f)->flags & PERLIO_F_CANWRITE) {
	{IV  ReplaceReturn1428 = PerlSIO_fflush(stdio); SensorCall(10947); return ReplaceReturn1428;}
    }
    else {
	NOOP;
#if 0
	/*
	 * FIXME: This discards ungetc() and pre-read stuff which is not
	 * right if this is just a "sync" from a layer above Suspect right
	 * design is to do _this_ but not have layer above flush this
	 * layer read-to-read
	 */
	/*
	 * Not writeable - sync by attempting a seek
	 */
	dSAVE_ERRNO;
	if (PerlSIO_fseek(stdio, (Off_t) 0, SEEK_CUR) != 0)
	    RESTORE_ERRNO;
#endif
    }
    {IV  ReplaceReturn1427 = 0; SensorCall(10949); return ReplaceReturn1427;}
}

IV
PerlIOStdio_eof(pTHX_ PerlIO *f)
{
SensorCall(10950);    PERL_UNUSED_CONTEXT;

    {IV  ReplaceReturn1426 = PerlSIO_feof(PerlIOSelf(f, PerlIOStdio)->stdio); SensorCall(10951); return ReplaceReturn1426;}
}

IV
PerlIOStdio_error(pTHX_ PerlIO *f)
{
SensorCall(10952);    PERL_UNUSED_CONTEXT;

    {IV  ReplaceReturn1425 = PerlSIO_ferror(PerlIOSelf(f, PerlIOStdio)->stdio); SensorCall(10953); return ReplaceReturn1425;}
}

void
PerlIOStdio_clearerr(pTHX_ PerlIO *f)
{
SensorCall(10954);    PERL_UNUSED_CONTEXT;

    PerlSIO_clearerr(PerlIOSelf(f, PerlIOStdio)->stdio);
}

void
PerlIOStdio_setlinebuf(pTHX_ PerlIO *f)
{
SensorCall(10955);    PERL_UNUSED_CONTEXT;

#ifdef HAS_SETLINEBUF
    PerlSIO_setlinebuf(PerlIOSelf(f, PerlIOStdio)->stdio);
#else
    PerlSIO_setvbuf(PerlIOSelf(f, PerlIOStdio)->stdio, NULL, _IOLBF, 0);
#endif
}

#ifdef FILE_base
STDCHAR *
PerlIOStdio_get_base(pTHX_ PerlIO *f)
{
    SensorCall(10956);FILE * const stdio = PerlIOSelf(f, PerlIOStdio)->stdio;
    {char * ReplaceReturn1424 = (STDCHAR*)PerlSIO_get_base(stdio); SensorCall(10957); return ReplaceReturn1424;}
}

Size_t
PerlIOStdio_get_bufsiz(pTHX_ PerlIO *f)
{
    SensorCall(10958);FILE * const stdio = PerlIOSelf(f, PerlIOStdio)->stdio;
    {size_t  ReplaceReturn1423 = PerlSIO_get_bufsiz(stdio); SensorCall(10959); return ReplaceReturn1423;}
}
#endif

#ifdef USE_STDIO_PTR
STDCHAR *
PerlIOStdio_get_ptr(pTHX_ PerlIO *f)
{
    SensorCall(10960);FILE * const stdio = PerlIOSelf(f, PerlIOStdio)->stdio;
    {char * ReplaceReturn1422 = (STDCHAR*)PerlSIO_get_ptr(stdio); SensorCall(10961); return ReplaceReturn1422;}
}

SSize_t
PerlIOStdio_get_cnt(pTHX_ PerlIO *f)
{
    SensorCall(10962);FILE * const stdio = PerlIOSelf(f, PerlIOStdio)->stdio;
    {ssize_t  ReplaceReturn1421 = PerlSIO_get_cnt(stdio); SensorCall(10963); return ReplaceReturn1421;}
}

void
PerlIOStdio_set_ptrcnt(pTHX_ PerlIO *f, STDCHAR * ptr, SSize_t cnt)
{
    SensorCall(10964);FILE * const stdio = PerlIOSelf(f, PerlIOStdio)->stdio;
    SensorCall(10966);if (ptr != NULL) {
#ifdef STDIO_PTR_LVALUE
	PerlSIO_set_ptr(stdio, ptr); /* LHS STDCHAR* cast non-portable */
#ifdef STDIO_PTR_LVAL_SETS_CNT
	assert(PerlSIO_get_cnt(stdio) == (cnt));
#endif
#if (!defined(STDIO_PTR_LVAL_NOCHANGE_CNT))
	/*
	 * Setting ptr _does_ change cnt - we are done
	 */
	SensorCall(10965);return;
#endif
#else                           /* STDIO_PTR_LVALUE */
	PerlProc_abort();
#endif                          /* STDIO_PTR_LVALUE */
    }
    /*
     * Now (or only) set cnt
     */
#ifdef STDIO_CNT_LVALUE
    PerlSIO_set_cnt(stdio, cnt);
#else                           /* STDIO_CNT_LVALUE */
#if (defined(STDIO_PTR_LVALUE) && defined(STDIO_PTR_LVAL_SETS_CNT))
    PerlSIO_set_ptr(stdio,
		    PerlSIO_get_ptr(stdio) + (PerlSIO_get_cnt(stdio) -
					      cnt));
#else                           /* STDIO_PTR_LVAL_SETS_CNT */
    PerlProc_abort();
#endif                          /* STDIO_PTR_LVAL_SETS_CNT */
#endif                          /* STDIO_CNT_LVALUE */
}


#endif

IV
PerlIOStdio_fill(pTHX_ PerlIO *f)
{
    SensorCall(10967);FILE * stdio;
    int c;
    PERL_UNUSED_CONTEXT;
    SensorCall(10969);if (PerlIO_lockcnt(f)) /* in use: abort ungracefully */
	{/*193*/{IV  ReplaceReturn1420 = -1; SensorCall(10968); return ReplaceReturn1420;}/*194*/}
    SensorCall(10970);stdio = PerlIOSelf(f, PerlIOStdio)->stdio;

    /*
     * fflush()ing read-only streams can cause trouble on some stdio-s
     */
    SensorCall(10972);if ((PerlIOBase(f)->flags & PERLIO_F_CANWRITE)) {
	SensorCall(10971);if (PerlSIO_fflush(stdio) != 0)
	    return EOF;
    }
    SensorCall(10979);for (;;) {
	SensorCall(10973);c = PerlSIO_fgetc(stdio);
	SensorCall(10975);if (c != EOF)
	    {/*195*/SensorCall(10974);break;/*196*/}
	SensorCall(10976);if (! PerlSIO_ferror(stdio) || errno != EINTR)
	    return EOF;
	SensorCall(10978);if (PL_sig_pending && S_perlio_async_run(aTHX_ f))
	    {/*197*/{IV  ReplaceReturn1419 = -1; SensorCall(10977); return ReplaceReturn1419;}/*198*/}
	SETERRNO(0,0);
    }

#if (defined(STDIO_PTR_LVALUE) && (defined(STDIO_CNT_LVALUE) || defined(STDIO_PTR_LVAL_SETS_CNT)))

#ifdef STDIO_BUFFER_WRITABLE
    if (PerlIO_fast_gets(f) && PerlIO_has_base(f)) {
	/* Fake ungetc() to the real buffer in case system's ungetc
	   goes elsewhere
	 */
	STDCHAR *base = (STDCHAR*)PerlSIO_get_base(stdio);
	SSize_t cnt   = PerlSIO_get_cnt(stdio);
	STDCHAR *ptr  = (STDCHAR*)PerlSIO_get_ptr(stdio);
	if (ptr == base+1) {
	    *--ptr = (STDCHAR) c;
	    PerlIOStdio_set_ptrcnt(aTHX_ f,ptr,cnt+1);
	    if (PerlSIO_feof(stdio))
		PerlSIO_clearerr(stdio);
	    return 0;
	}
    }
    else
#endif
    SensorCall(10983);if (PerlIO_has_cntptr(f)) {
SensorCall(10980);	STDCHAR ch = c;
	SensorCall(10982);if (PerlIOStdio_unread(aTHX_ f,&ch,1) == 1) {
	    {IV  ReplaceReturn1418 = 0; SensorCall(10981); return ReplaceReturn1418;}
	}
    }
#endif

#if defined(VMS)
    /* An ungetc()d char is handled separately from the regular
     * buffer, so we stuff it in the buffer ourselves.
     * Should never get called as should hit code above
     */
    *(--((*stdio)->_ptr)) = (unsigned char) c;
    (*stdio)->_cnt++;
#else
    /* If buffer snoop scheme above fails fall back to
       using ungetc().
     */
    SensorCall(10984);if (PerlSIO_ungetc(c, stdio) != c)
	return EOF;
#endif
    {IV  ReplaceReturn1417 = 0; SensorCall(10985); return ReplaceReturn1417;}
}



PERLIO_FUNCS_DECL(PerlIO_stdio) = {
    sizeof(PerlIO_funcs),
    "stdio",
    sizeof(PerlIOStdio),
    PERLIO_K_BUFFERED|PERLIO_K_RAW,
    PerlIOStdio_pushed,
    PerlIOBase_popped,
    PerlIOStdio_open,
    PerlIOBase_binmode,         /* binmode */
    NULL,
    PerlIOStdio_fileno,
    PerlIOStdio_dup,
    PerlIOStdio_read,
    PerlIOStdio_unread,
    PerlIOStdio_write,
    PerlIOStdio_seek,
    PerlIOStdio_tell,
    PerlIOStdio_close,
    PerlIOStdio_flush,
    PerlIOStdio_fill,
    PerlIOStdio_eof,
    PerlIOStdio_error,
    PerlIOStdio_clearerr,
    PerlIOStdio_setlinebuf,
#ifdef FILE_base
    PerlIOStdio_get_base,
    PerlIOStdio_get_bufsiz,
#else
    NULL,
    NULL,
#endif
#ifdef USE_STDIO_PTR
    PerlIOStdio_get_ptr,
    PerlIOStdio_get_cnt,
#   if defined(HAS_FAST_STDIO) && defined(USE_FAST_STDIO)
    PerlIOStdio_set_ptrcnt,
#   else
    NULL,
#   endif /* HAS_FAST_STDIO && USE_FAST_STDIO */
#else
    NULL,
    NULL,
    NULL,
#endif /* USE_STDIO_PTR */
};

/* Note that calls to PerlIO_exportFILE() are reversed using
 * PerlIO_releaseFILE(), not importFILE. */
FILE *
PerlIO_exportFILE(PerlIO * f, const char *mode)
{
    dTHX;
    FILE *stdio = NULL;
    SensorCall(10995);if (PerlIOValid(f)) {
	SensorCall(10986);char buf[8];
	PerlIO_flush(f);
	SensorCall(10988);if (!mode || !*mode) {
	    SensorCall(10987);mode = PerlIO_modestr(f, buf);
	}
	SensorCall(10989);stdio = PerlSIO_fdopen(PerlIO_fileno(f), mode);
	SensorCall(10994);if (stdio) {
	    SensorCall(10990);PerlIOl *l = *f;
	    PerlIO *f2;
	    /* De-link any lower layers so new :stdio sticks */
	    *f = NULL;
	    SensorCall(10993);if ((f2 = PerlIO_push(aTHX_ f, PERLIO_FUNCS_CAST(&PerlIO_stdio), buf, NULL))) {
		SensorCall(10991);PerlIOStdio *s = PerlIOSelf((f = f2), PerlIOStdio);
		s->stdio = stdio;
		PerlIOUnix_refcnt_inc(fileno(stdio));
		/* Link previous lower layers under new one */
		*PerlIONext(f) = l;
	    }
	    else {
		/* restore layers list */
		SensorCall(10992);*f = l;
	    }
	}
    }
    {FILE * ReplaceReturn1416 = stdio; SensorCall(10996); return ReplaceReturn1416;}
}


FILE *
PerlIO_findFILE(PerlIO *f)
{
    SensorCall(10997);PerlIOl *l = *f;
    FILE *stdio;
    SensorCall(11002);while (l) {
	SensorCall(10998);if (l->tab == &PerlIO_stdio) {
	    SensorCall(10999);PerlIOStdio *s = PerlIOSelf(&l, PerlIOStdio);
	    {FILE * ReplaceReturn1415 = s->stdio; SensorCall(11000); return ReplaceReturn1415;}
	}
	SensorCall(11001);l = *PerlIONext(&l);
    }
    /* Uses fallback "mode" via PerlIO_modestr() in PerlIO_exportFILE */
    /* However, we're not really exporting a FILE * to someone else (who
       becomes responsible for closing it, or calling PerlIO_releaseFILE())
       So we need to undo its reference count increase on the underlying file
       descriptor. We have to do this, because if the loop above returns you
       the FILE *, then *it* didn't increase any reference count. So there's
       only one way to be consistent. */
    SensorCall(11003);stdio = PerlIO_exportFILE(f, NULL);
    SensorCall(11007);if (stdio) {
	SensorCall(11004);const int fd = fileno(stdio);
	SensorCall(11006);if (fd >= 0)
	    {/*17*/SensorCall(11005);PerlIOUnix_refcnt_dec(fd);/*18*/}
    }
    {FILE * ReplaceReturn1414 = stdio; SensorCall(11008); return ReplaceReturn1414;}
}

/* Use this to reverse PerlIO_exportFILE calls. */
void
PerlIO_releaseFILE(PerlIO *p, FILE *f)
{
SensorCall(11009);    dVAR;
    PerlIOl *l;
    SensorCall(11019);while ((l = *p)) {
	SensorCall(11010);if (l->tab == &PerlIO_stdio) {
	    SensorCall(11011);PerlIOStdio *s = PerlIOSelf(&l, PerlIOStdio);
	    SensorCall(11017);if (s->stdio == f) {
		dTHX;
		SensorCall(11012);const int fd = fileno(f);
		SensorCall(11014);if (fd >= 0)
		    {/*19*/SensorCall(11013);PerlIOUnix_refcnt_dec(fd);/*20*/}
		SensorCall(11015);PerlIO_pop(aTHX_ p);
		SensorCall(11016);return;
	    }
	}
	SensorCall(11018);p = PerlIONext(p);
    }
    SensorCall(11020);return;
}

/*--------------------------------------------------------------------------------------*/
/*
 * perlio buffer layer
 */

IV
PerlIOBuf_pushed(pTHX_ PerlIO *f, const char *mode, SV *arg, PerlIO_funcs *tab)
{
    SensorCall(11021);PerlIOBuf *b = PerlIOSelf(f, PerlIOBuf);
    const int fd = PerlIO_fileno(f);
    SensorCall(11022);if (fd >= 0 && PerlLIO_isatty(fd)) {
	PerlIOBase(f)->flags |= PERLIO_F_LINEBUF | PERLIO_F_TTY;
    }
    SensorCall(11026);if (*PerlIONext(f)) {
	SensorCall(11023);const Off_t posn = PerlIO_tell(PerlIONext(f));
	SensorCall(11025);if (posn != (Off_t) - 1) {
	    SensorCall(11024);b->posn = posn;
	}
    }
    {IV  ReplaceReturn1413 = PerlIOBase_pushed(aTHX_ f, mode, arg, tab); SensorCall(11027); return ReplaceReturn1413;}
}

PerlIO *
PerlIOBuf_open(pTHX_ PerlIO_funcs *self, PerlIO_list_t *layers,
	       IV n, const char *mode, int fd, int imode, int perm,
	       PerlIO *f, int narg, SV **args)
{
    SensorCall(11028);if (PerlIOValid(f)) {
	PerlIO *next = PerlIONext(f);
	SensorCall(11029);PerlIO_funcs *tab =
	     PerlIO_layer_fetch(aTHX_ layers, n - 1, PerlIOBase(next)->tab);
	SensorCall(11031);if (tab && tab->Open)
	     {/*161*/SensorCall(11030);next =
		  (*tab->Open)(aTHX_ tab, layers, n - 1, mode, fd, imode, perm,
			       next, narg, args);/*162*/}
	SensorCall(11033);if (!next || (*PerlIOBase(f)->tab->Pushed) (aTHX_ f, mode, PerlIOArg, self) != 0) {
	    {PerlIO * ReplaceReturn1412 = NULL; SensorCall(11032); return ReplaceReturn1412;}
	}
    }
    else {
	SensorCall(11034);PerlIO_funcs *tab = PerlIO_layer_fetch(aTHX_ layers, n - 1, PerlIO_default_btm());
	int init = 0;
	SensorCall(11036);if (*mode == IoTYPE_IMPLICIT) {
	    SensorCall(11035);init = 1;
	    /*
	     * mode++;
	     */
	}
	SensorCall(11038);if (tab && tab->Open)
	     {/*163*/SensorCall(11037);f = (*tab->Open)(aTHX_ tab, layers, n - 1, mode, fd, imode, perm,
			      f, narg, args);/*164*/}
	else
	     SETERRNO(EINVAL, LIB_INVARG);
	SensorCall(11043);if (f) {
	    SensorCall(11039);if (PerlIO_push(aTHX_ f, self, mode, PerlIOArg) == 0) {
		/*
		 * if push fails during open, open fails. close will pop us.
		 */
		PerlIO_close (f);
		{PerlIO * ReplaceReturn1411 = NULL; SensorCall(11040); return ReplaceReturn1411;}
	    } else {
		SensorCall(11041);fd = PerlIO_fileno(f);
		SensorCall(11042);if (init && fd == 2) {
		    /*
		     * Initial stderr is unbuffered
		     */
		    PerlIOBase(f)->flags |= PERLIO_F_UNBUF;
		}
#ifdef PERLIO_USING_CRLF
#  ifdef PERLIO_IS_BINMODE_FD
		if (PERLIO_IS_BINMODE_FD(fd))
		    PerlIO_binmode(aTHX_ f,  '<'/*not used*/, O_BINARY, NULL);
		else
#  endif
		/*
		 * do something about failing setmode()? --jhi
		 */
		PerlLIO_setmode(fd, O_BINARY);
#endif
#ifdef VMS
#include <rms.h>
		/* Enable line buffering with record-oriented regular files
		 * so we don't introduce an extraneous record boundary when
		 * the buffer fills up.
		 */
		if (PerlIOBase(f)->flags & PERLIO_F_CANWRITE) {
		    Stat_t st;
		    if (PerlLIO_fstat(fd, &st) == 0
		        && S_ISREG(st.st_mode)
		        && (st.st_fab_rfm == FAB$C_VAR 
			    || st.st_fab_rfm == FAB$C_VFC)) {
			PerlIOBase(f)->flags |= PERLIO_F_LINEBUF;
		    }
		}
#endif
	    }
	}
    }
    {PerlIO * ReplaceReturn1410 = f; SensorCall(11044); return ReplaceReturn1410;}
}

/*
 * This "flush" is akin to sfio's sync in that it handles files in either
 * read or write state.  For write state, we put the postponed data through
 * the next layers.  For read state, we seek() the next layers to the
 * offset given by current position in the buffer, and discard the buffer
 * state (XXXX supposed to be for seek()able buffers only, but now it is done
 * in any case?).  Then the pass the stick further in chain.
 */
IV
PerlIOBuf_flush(pTHX_ PerlIO *f)
{
    SensorCall(11045);PerlIOBuf * const b = PerlIOSelf(f, PerlIOBuf);
    int code = 0;
    PerlIO *n = PerlIONext(f);
    SensorCall(11061);if (PerlIOBase(f)->flags & PERLIO_F_WRBUF) {
	/*
	 * write() the buffer
	 */
	SensorCall(11046);const STDCHAR *buf = b->buf;
	const STDCHAR *p = buf;
	SensorCall(11052);while (p < b->ptr) {
	    SSize_t count = PerlIO_write(n, p, b->ptr - p);
	    SensorCall(11051);if (count > 0) {
		SensorCall(11047);p += count;
	    }
	    else {/*153*/SensorCall(11048);if (count < 0 || PerlIO_error(n)) {
		PerlIOBase(f)->flags |= PERLIO_F_ERROR;
		SensorCall(11049);code = -1;
		SensorCall(11050);break;
	    ;/*154*/}}
	}
	SensorCall(11053);b->posn += (p - buf);
    }
    else {/*155*/SensorCall(11054);if (PerlIOBase(f)->flags & PERLIO_F_RDBUF) {
	STDCHAR *buf = PerlIO_get_base(f);
	/*
	 * Note position change
	 */
	SensorCall(11055);b->posn += (b->ptr - buf);
	SensorCall(11060);if (b->ptr < b->end) {
	    /* We did not consume all of it - try and seek downstream to
	       our logical position
	     */
	    SensorCall(11056);if (PerlIOValid(n) && PerlIO_seek(n, b->posn, SEEK_SET) == 0) {
		/* Reload n as some layers may pop themselves on seek */
		SensorCall(11057);b->posn = PerlIO_tell(n = PerlIONext(f));
	    }
	    else {
		/* Seek failed (e.g. pipe or tty). Do NOT clear buffer or pre-read
		   data is lost for good - so return saying "ok" having undone
		   the position adjust
		 */
		SensorCall(11058);b->posn -= (b->ptr - buf);
		{IV  ReplaceReturn1409 = code; SensorCall(11059); return ReplaceReturn1409;}
	    }
	}
    ;/*156*/}}
    SensorCall(11062);b->ptr = b->end = b->buf;
    PerlIOBase(f)->flags &= ~(PERLIO_F_RDBUF | PERLIO_F_WRBUF);
    /* We check for Valid because of dubious decision to make PerlIO_flush(NULL) flush all */
    SensorCall(11064);if (PerlIOValid(n) && PerlIO_flush(n) != 0)
	{/*157*/SensorCall(11063);code = -1;/*158*/}
    {IV  ReplaceReturn1408 = code; SensorCall(11065); return ReplaceReturn1408;}
}

/* This discards the content of the buffer after b->ptr, and rereads
 * the buffer from the position off in the layer downstream; here off
 * is at offset corresponding to b->ptr - b->buf.
 */
IV
PerlIOBuf_fill(pTHX_ PerlIO *f)
{
    SensorCall(11066);PerlIOBuf * const b = PerlIOSelf(f, PerlIOBuf);
    PerlIO *n = PerlIONext(f);
    SSize_t avail;
    /*
     * Down-stream flush is defined not to loose read data so is harmless.
     * we would not normally be fill'ing if there was data left in anycase.
     */
    SensorCall(11068);if (PerlIO_flush(f) != 0)	/* XXXX Check that its seek() succeeded?! */
	{/*145*/{IV  ReplaceReturn1407 = -1; SensorCall(11067); return ReplaceReturn1407;}/*146*/}
    SensorCall(11070);if (PerlIOBase(f)->flags & PERLIO_F_TTY)
	{/*147*/SensorCall(11069);PerlIOBase_flush_linebuf(aTHX);/*148*/}

    SensorCall(11071);if (!b->buf)
	PerlIO_get_base(f);     /* allocate via vtable */

    assert(b->buf); /* The b->buf does get allocated via the vtable system. */

    SensorCall(11072);b->ptr = b->end = b->buf;

    SensorCall(11074);if (!PerlIOValid(n)) {
	PerlIOBase(f)->flags |= PERLIO_F_EOF;
	{IV  ReplaceReturn1406 = -1; SensorCall(11073); return ReplaceReturn1406;}
    }

    SensorCall(11086);if (PerlIO_fast_gets(n)) {
	/*
	 * Layer below is also buffered. We do _NOT_ want to call its
	 * ->Read() because that will loop till it gets what we asked for
	 * which may hang on a pipe etc. Instead take anything it has to
	 * hand, or ask it to fill _once_.
	 */
	SensorCall(11075);avail = PerlIO_get_cnt(n);
	SensorCall(11080);if (avail <= 0) {
	    SensorCall(11076);avail = PerlIO_fill(n);
	    SensorCall(11079);if (avail == 0)
		avail = PerlIO_get_cnt(n);
	    else {
		SensorCall(11077);if (!PerlIO_error(n) && PerlIO_eof(n))
		    {/*149*/SensorCall(11078);avail = 0;/*150*/}
	    }
	}
	SensorCall(11084);if (avail > 0) {
	    STDCHAR *ptr = PerlIO_get_ptr(n);
	    SensorCall(11081);const SSize_t cnt = avail;
	    SensorCall(11083);if (avail > (SSize_t)b->bufsiz)
		{/*151*/SensorCall(11082);avail = b->bufsiz;/*152*/}
	    Copy(ptr, b->buf, avail, STDCHAR);
	    PerlIO_set_ptrcnt(n, ptr + avail, cnt - avail);
	}
    }
    else {
	SensorCall(11085);avail = PerlIO_read(n, b->ptr, b->bufsiz);
    }
    SensorCall(11089);if (avail <= 0) {
	SensorCall(11087);if (avail == 0)
	    PerlIOBase(f)->flags |= PERLIO_F_EOF;
	else
	    PerlIOBase(f)->flags |= PERLIO_F_ERROR;
	{IV  ReplaceReturn1405 = -1; SensorCall(11088); return ReplaceReturn1405;}
    }
    SensorCall(11090);b->end = b->buf + avail;
    PerlIOBase(f)->flags |= PERLIO_F_RDBUF;
    {IV  ReplaceReturn1404 = 0; SensorCall(11091); return ReplaceReturn1404;}
}

SSize_t
PerlIOBuf_read(pTHX_ PerlIO *f, void *vbuf, Size_t count)
{
    SensorCall(11092);if (PerlIOValid(f)) {
        SensorCall(11093);const PerlIOBuf * const b = PerlIOSelf(f, PerlIOBuf);
	SensorCall(11094);if (!b->ptr)
	    PerlIO_get_base(f);
	{ssize_t  ReplaceReturn1403 = PerlIOBase_read(aTHX_ f, vbuf, count); SensorCall(11095); return ReplaceReturn1403;}
    }
    {ssize_t  ReplaceReturn1402 = 0; SensorCall(11096); return ReplaceReturn1402;}
}

SSize_t
PerlIOBuf_unread(pTHX_ PerlIO *f, const void *vbuf, Size_t count)
{
    SensorCall(11097);const STDCHAR *buf = (const STDCHAR *) vbuf + count;
    PerlIOBuf * const b = PerlIOSelf(f, PerlIOBuf);
    SSize_t unread = 0;
    SSize_t avail;
    SensorCall(11098);if (PerlIOBase(f)->flags & PERLIO_F_WRBUF)
	PerlIO_flush(f);
    SensorCall(11099);if (!b->buf)
	PerlIO_get_base(f);
    SensorCall(11109);if (b->buf) {
	SensorCall(11100);if (PerlIOBase(f)->flags & PERLIO_F_RDBUF) {
	    /*
	     * Buffer is already a read buffer, we can overwrite any chars
	     * which have been read back to buffer start
	     */
	    SensorCall(11101);avail = (b->ptr - b->buf);
	}
	else {
	    /*
	     * Buffer is idle, set it up so whole buffer is available for
	     * unread
	     */
	    SensorCall(11102);avail = b->bufsiz;
	    b->end = b->buf + avail;
	    b->ptr = b->end;
	    PerlIOBase(f)->flags |= PERLIO_F_RDBUF;
	    /*
	     * Buffer extends _back_ from where we are now
	     */
	    b->posn -= b->bufsiz;
	}
	SensorCall(11104);if ((SSize_t) count >= 0 && avail > (SSize_t) count) {
	    /*
	     * If we have space for more than count, just move count
	     */
	    SensorCall(11103);avail = count;
	}
	SensorCall(11108);if (avail > 0) {
	    SensorCall(11105);b->ptr -= avail;
	    buf -= avail;
	    /*
	     * In simple stdio-like ungetc() case chars will be already
	     * there
	     */
	    SensorCall(11106);if (buf != b->ptr) {
		Copy(buf, b->ptr, avail, STDCHAR);
	    }
	    SensorCall(11107);count -= avail;
	    unread += avail;
	    PerlIOBase(f)->flags &= ~PERLIO_F_EOF;
	}
    }
    SensorCall(11111);if (count > 0) {
	SensorCall(11110);unread += PerlIOBase_unread(aTHX_ f, vbuf, count);
    }
    {ssize_t  ReplaceReturn1401 = unread; SensorCall(11112); return ReplaceReturn1401;}
}

SSize_t
PerlIOBuf_write(pTHX_ PerlIO *f, const void *vbuf, Size_t count)
{
    SensorCall(11113);PerlIOBuf * const b = PerlIOSelf(f, PerlIOBuf);
    const STDCHAR *buf = (const STDCHAR *) vbuf;
    const STDCHAR *flushptr = buf;
    Size_t written = 0;
    SensorCall(11114);if (!b->buf)
	PerlIO_get_base(f);
    SensorCall(11116);if (!(PerlIOBase(f)->flags & PERLIO_F_CANWRITE))
	{/*165*/{ssize_t  ReplaceReturn1400 = 0; SensorCall(11115); return ReplaceReturn1400;}/*166*/}
    SensorCall(11119);if (PerlIOBase(f)->flags & PERLIO_F_RDBUF) {
    	SensorCall(11117);if (PerlIO_flush(f) != 0) {
	    {ssize_t  ReplaceReturn1399 = 0; SensorCall(11118); return ReplaceReturn1399;}
	}
    }	
    SensorCall(11123);if (PerlIOBase(f)->flags & PERLIO_F_LINEBUF) {
	SensorCall(11120);flushptr = buf + count;
	SensorCall(11122);while (flushptr > buf && *(flushptr - 1) != '\n')
	    {/*167*/SensorCall(11121);--flushptr;/*168*/}
    }
    SensorCall(11135);while (count > 0) {
SensorCall(11124);	SSize_t avail = b->bufsiz - (b->ptr - b->buf);
	SensorCall(11126);if ((SSize_t) count >= 0 && (SSize_t) count < avail)
	    {/*169*/SensorCall(11125);avail = count;/*170*/}
	SensorCall(11128);if (flushptr > buf && flushptr <= buf + avail)
	    {/*171*/SensorCall(11127);avail = flushptr - buf;/*172*/}
	PerlIOBase(f)->flags |= PERLIO_F_WRBUF;
	SensorCall(11131);if (avail) {
	    Copy(buf, b->ptr, avail, STDCHAR);
	    SensorCall(11129);count -= avail;
	    buf += avail;
	    written += avail;
	    b->ptr += avail;
	    SensorCall(11130);if (buf == flushptr)
		PerlIO_flush(f);
	}
	SensorCall(11134);if (b->ptr >= (b->buf + b->bufsiz))
	    {/*173*/SensorCall(11132);if (PerlIO_flush(f) == -1)
		{/*175*/{ssize_t  ReplaceReturn1398 = -1; SensorCall(11133); return ReplaceReturn1398;}/*176*/}/*174*/}
    }
    SensorCall(11136);if (PerlIOBase(f)->flags & PERLIO_F_UNBUF)
	PerlIO_flush(f);
    {ssize_t  ReplaceReturn1397 = written; SensorCall(11137); return ReplaceReturn1397;}
}

IV
PerlIOBuf_seek(pTHX_ PerlIO *f, Off_t offset, int whence)
{
    SensorCall(11138);IV code;
    SensorCall(11142);if ((code = PerlIO_flush(f)) == 0) {
	PerlIOBase(f)->flags &= ~PERLIO_F_EOF;
	SensorCall(11139);code = PerlIO_seek(PerlIONext(f), offset, whence);
	SensorCall(11141);if (code == 0) {
	    SensorCall(11140);PerlIOBuf *b = PerlIOSelf(f, PerlIOBuf);
	    b->posn = PerlIO_tell(PerlIONext(f));
	}
    }
    {IV  ReplaceReturn1396 = code; SensorCall(11143); return ReplaceReturn1396;}
}

Off_t
PerlIOBuf_tell(pTHX_ PerlIO *f)
{
    SensorCall(11144);PerlIOBuf * const b = PerlIOSelf(f, PerlIOBuf);
    /*
     * b->posn is file position where b->buf was read, or will be written
     */
    Off_t posn = b->posn;
    SensorCall(11146);if ((PerlIOBase(f)->flags & PERLIO_F_APPEND) &&
        (PerlIOBase(f)->flags & PERLIO_F_WRBUF)) {
#if 1
    	/* As O_APPEND files are normally shared in some sense it is better
	   to flush :
	 */  	
	PerlIO_flush(f);
#else	
        /* when file is NOT shared then this is sufficient */
	PerlIO_seek(PerlIONext(f),0, SEEK_END);
#endif
	SensorCall(11145);posn = b->posn = PerlIO_tell(PerlIONext(f));
    }
    SensorCall(11148);if (b->buf) {
	/*
	 * If buffer is valid adjust position by amount in buffer
	 */
	SensorCall(11147);posn += (b->ptr - b->buf);
    }
    {off64_t  ReplaceReturn1395 = posn; SensorCall(11149); return ReplaceReturn1395;}
}

IV
PerlIOBuf_popped(pTHX_ PerlIO *f)
{
    SensorCall(11150);const IV code = PerlIOBase_popped(aTHX_ f);
    PerlIOBuf * const b = PerlIOSelf(f, PerlIOBuf);
    SensorCall(11151);if (b->buf && b->buf != (STDCHAR *) & b->oneword) {
	Safefree(b->buf);
    }
    SensorCall(11152);b->ptr = b->end = b->buf = NULL;
    PerlIOBase(f)->flags &= ~(PERLIO_F_RDBUF | PERLIO_F_WRBUF);
    {IV  ReplaceReturn1394 = code; SensorCall(11153); return ReplaceReturn1394;}
}

IV
PerlIOBuf_close(pTHX_ PerlIO *f)
{
    SensorCall(11154);const IV code = PerlIOBase_close(aTHX_ f);
    PerlIOBuf * const b = PerlIOSelf(f, PerlIOBuf);
    SensorCall(11155);if (b->buf && b->buf != (STDCHAR *) & b->oneword) {
	Safefree(b->buf);
    }
    SensorCall(11156);b->ptr = b->end = b->buf = NULL;
    PerlIOBase(f)->flags &= ~(PERLIO_F_RDBUF | PERLIO_F_WRBUF);
    {IV  ReplaceReturn1393 = code; SensorCall(11157); return ReplaceReturn1393;}
}

STDCHAR *
PerlIOBuf_get_ptr(pTHX_ PerlIO *f)
{
    SensorCall(11158);const PerlIOBuf * const b = PerlIOSelf(f, PerlIOBuf);
    SensorCall(11159);if (!b->buf)
	PerlIO_get_base(f);
    {char * ReplaceReturn1392 = b->ptr; SensorCall(11160); return ReplaceReturn1392;}
}

SSize_t
PerlIOBuf_get_cnt(pTHX_ PerlIO *f)
{
    SensorCall(11161);const PerlIOBuf * const b = PerlIOSelf(f, PerlIOBuf);
    SensorCall(11162);if (!b->buf)
	PerlIO_get_base(f);
    SensorCall(11164);if (PerlIOBase(f)->flags & PERLIO_F_RDBUF)
	{/*159*/{ssize_t  ReplaceReturn1391 = (b->end - b->ptr); SensorCall(11163); return ReplaceReturn1391;}/*160*/}
    {ssize_t  ReplaceReturn1390 = 0; SensorCall(11165); return ReplaceReturn1390;}
}

STDCHAR *
PerlIOBuf_get_base(pTHX_ PerlIO *f)
{
    SensorCall(11166);PerlIOBuf * const b = PerlIOSelf(f, PerlIOBuf);
    PERL_UNUSED_CONTEXT;

    SensorCall(11171);if (!b->buf) {
	SensorCall(11167);if (!b->bufsiz)
	    b->bufsiz = PERLIOBUF_DEFAULT_BUFSIZ;
	Newxz(b->buf,b->bufsiz, STDCHAR);
	SensorCall(11169);if (!b->buf) {
	    SensorCall(11168);b->buf = (STDCHAR *) & b->oneword;
	    b->bufsiz = sizeof(b->oneword);
	}
	SensorCall(11170);b->end = b->ptr = b->buf;
    }
    {char * ReplaceReturn1389 = b->buf; SensorCall(11172); return ReplaceReturn1389;}
}

Size_t
PerlIOBuf_bufsiz(pTHX_ PerlIO *f)
{
    SensorCall(11173);const PerlIOBuf * const b = PerlIOSelf(f, PerlIOBuf);
    SensorCall(11174);if (!b->buf)
	PerlIO_get_base(f);
    {size_t  ReplaceReturn1388 = (b->end - b->buf); SensorCall(11175); return ReplaceReturn1388;}
}

void
PerlIOBuf_set_ptrcnt(pTHX_ PerlIO *f, STDCHAR * ptr, SSize_t cnt)
{
    SensorCall(11176);PerlIOBuf * const b = PerlIOSelf(f, PerlIOBuf);
#ifndef DEBUGGING
    PERL_UNUSED_ARG(cnt);
#endif
    SensorCall(11177);if (!b->buf)
	PerlIO_get_base(f);
    SensorCall(11178);b->ptr = ptr;
    assert(PerlIO_get_cnt(f) == cnt);
    assert(b->ptr >= b->buf);
    PerlIOBase(f)->flags |= PERLIO_F_RDBUF;
}

PerlIO *
PerlIOBuf_dup(pTHX_ PerlIO *f, PerlIO *o, CLONE_PARAMS *param, int flags)
{
 {PerlIO * ReplaceReturn1387 = PerlIOBase_dup(aTHX_ f, o, param, flags); SensorCall(11179); return ReplaceReturn1387;}
}



PERLIO_FUNCS_DECL(PerlIO_perlio) = {
    sizeof(PerlIO_funcs),
    "perlio",
    sizeof(PerlIOBuf),
    PERLIO_K_BUFFERED|PERLIO_K_RAW,
    PerlIOBuf_pushed,
    PerlIOBuf_popped,
    PerlIOBuf_open,
    PerlIOBase_binmode,         /* binmode */
    NULL,
    PerlIOBase_fileno,
    PerlIOBuf_dup,
    PerlIOBuf_read,
    PerlIOBuf_unread,
    PerlIOBuf_write,
    PerlIOBuf_seek,
    PerlIOBuf_tell,
    PerlIOBuf_close,
    PerlIOBuf_flush,
    PerlIOBuf_fill,
    PerlIOBase_eof,
    PerlIOBase_error,
    PerlIOBase_clearerr,
    PerlIOBase_setlinebuf,
    PerlIOBuf_get_base,
    PerlIOBuf_bufsiz,
    PerlIOBuf_get_ptr,
    PerlIOBuf_get_cnt,
    PerlIOBuf_set_ptrcnt,
};

/*--------------------------------------------------------------------------------------*/
/*
 * Temp layer to hold unread chars when cannot do it any other way
 */

IV
PerlIOPending_fill(pTHX_ PerlIO *f)
{
SensorCall(11180);    /*
     * Should never happen
     */
    PerlIO_flush(f);
    {IV  ReplaceReturn1386 = 0; SensorCall(11181); return ReplaceReturn1386;}
}

IV
PerlIOPending_close(pTHX_ PerlIO *f)
{
SensorCall(11182);    /*
     * A tad tricky - flush pops us, then we close new top
     */
    PerlIO_flush(f);
    {IV  ReplaceReturn1385 = PerlIO_close(f); SensorCall(11183); return ReplaceReturn1385;}
}

IV
PerlIOPending_seek(pTHX_ PerlIO *f, Off_t offset, int whence)
{
SensorCall(11184);    /*
     * A tad tricky - flush pops us, then we seek new top
     */
    PerlIO_flush(f);
    {IV  ReplaceReturn1384 = PerlIO_seek(f, offset, whence); SensorCall(11185); return ReplaceReturn1384;}
}


IV
PerlIOPending_flush(pTHX_ PerlIO *f)
{
    SensorCall(11186);PerlIOBuf * const b = PerlIOSelf(f, PerlIOBuf);
    SensorCall(11188);if (b->buf && b->buf != (STDCHAR *) & b->oneword) {
	Safefree(b->buf);
	SensorCall(11187);b->buf = NULL;
    }
    SensorCall(11189);PerlIO_pop(aTHX_ f);
    {IV  ReplaceReturn1383 = 0; SensorCall(11190); return ReplaceReturn1383;}
}

void
PerlIOPending_set_ptrcnt(pTHX_ PerlIO *f, STDCHAR * ptr, SSize_t cnt)
{
    SensorCall(11191);if (cnt <= 0) {
	PerlIO_flush(f);
    }
    else {
	SensorCall(11192);PerlIOBuf_set_ptrcnt(aTHX_ f, ptr, cnt);
    }
SensorCall(11193);}

IV
PerlIOPending_pushed(pTHX_ PerlIO *f, const char *mode, SV *arg, PerlIO_funcs *tab)
{
    SensorCall(11194);const IV code = PerlIOBase_pushed(aTHX_ f, mode, arg, tab);
    PerlIOl * const l = PerlIOBase(f);
    /*
     * Our PerlIO_fast_gets must match what we are pushed on, or sv_gets()
     * etc. get muddled when it changes mid-string when we auto-pop.
     */
    l->flags = (l->flags & ~(PERLIO_F_FASTGETS | PERLIO_F_UTF8)) |
	(PerlIOBase(PerlIONext(f))->
	 flags & (PERLIO_F_FASTGETS | PERLIO_F_UTF8));
    {IV  ReplaceReturn1382 = code; SensorCall(11195); return ReplaceReturn1382;}
}

SSize_t
PerlIOPending_read(pTHX_ PerlIO *f, void *vbuf, Size_t count)
{
SensorCall(11196);    SSize_t avail = PerlIO_get_cnt(f);
    SSize_t got = 0;
    SensorCall(11198);if ((SSize_t) count >= 0 && (SSize_t)count < avail)
	{/*185*/SensorCall(11197);avail = count;/*186*/}
    SensorCall(11200);if (avail > 0)
	{/*187*/SensorCall(11199);got = PerlIOBuf_read(aTHX_ f, vbuf, avail);/*188*/}
    SensorCall(11204);if (got >= 0 && got < (SSize_t)count) {
	SensorCall(11201);const SSize_t more =
	    PerlIO_read(f, ((STDCHAR *) vbuf) + got, count - got);
	SensorCall(11203);if (more >= 0 || got == 0)
	    {/*189*/SensorCall(11202);got += more;/*190*/}
    }
    {ssize_t  ReplaceReturn1381 = got; SensorCall(11205); return ReplaceReturn1381;}
}

PERLIO_FUNCS_DECL(PerlIO_pending) = {
    sizeof(PerlIO_funcs),
    "pending",
    sizeof(PerlIOBuf),
    PERLIO_K_BUFFERED|PERLIO_K_RAW,  /* not sure about RAW here */
    PerlIOPending_pushed,
    PerlIOBuf_popped,
    NULL,
    PerlIOBase_binmode,         /* binmode */
    NULL,
    PerlIOBase_fileno,
    PerlIOBuf_dup,
    PerlIOPending_read,
    PerlIOBuf_unread,
    PerlIOBuf_write,
    PerlIOPending_seek,
    PerlIOBuf_tell,
    PerlIOPending_close,
    PerlIOPending_flush,
    PerlIOPending_fill,
    PerlIOBase_eof,
    PerlIOBase_error,
    PerlIOBase_clearerr,
    PerlIOBase_setlinebuf,
    PerlIOBuf_get_base,
    PerlIOBuf_bufsiz,
    PerlIOBuf_get_ptr,
    PerlIOBuf_get_cnt,
    PerlIOPending_set_ptrcnt,
};



/*--------------------------------------------------------------------------------------*/
/*
 * crlf - translation On read translate CR,LF to "\n" we do this by
 * overriding ptr/cnt entries to hand back a line at a time and keeping a
 * record of which nl we "lied" about. On write translate "\n" to CR,LF
 *
 * c->nl points on the first byte of CR LF pair when it is temporarily
 * replaced by LF, or to the last CR of the buffer.  In the former case
 * the caller thinks that the buffer ends at c->nl + 1, in the latter
 * that it ends at c->nl; these two cases can be distinguished by
 * *c->nl.  c->nl is set during _getcnt() call, and unset during
 * _unread() and _flush() calls.
 * It only matters for read operations.
 */

typedef struct {
    PerlIOBuf base;             /* PerlIOBuf stuff */
    STDCHAR *nl;                /* Position of crlf we "lied" about in the
				 * buffer */
} PerlIOCrlf;

/* Inherit the PERLIO_F_UTF8 flag from previous layer.
 * Otherwise the :crlf layer would always revert back to
 * raw mode.
 */
static void
S_inherit_utf8_flag(PerlIO *f)
{
SensorCall(11206);    PerlIO *g = PerlIONext(f);
    SensorCall(11208);if (PerlIOValid(g)) {
	SensorCall(11207);if (PerlIOBase(g)->flags & PERLIO_F_UTF8) {
	    PerlIOBase(f)->flags |= PERLIO_F_UTF8;
	}
    }
SensorCall(11209);}

IV
PerlIOCrlf_pushed(pTHX_ PerlIO *f, const char *mode, SV *arg, PerlIO_funcs *tab)
{
    SensorCall(11210);IV code;
    PerlIOBase(f)->flags |= PERLIO_F_CRLF;
    code = PerlIOBuf_pushed(aTHX_ f, mode, arg, tab);
#if 0
    PerlIO_debug("PerlIOCrlf_pushed f=%p %s %s fl=%08" UVxf "\n",
		 (void*)f, PerlIOBase(f)->tab->name, (mode) ? mode : "(Null)",
		 PerlIOBase(f)->flags);
#endif
    {
      /* If the old top layer is a CRLF layer, reactivate it (if
       * necessary) and remove this new layer from the stack */
	 PerlIO *g = PerlIONext(f);
	 SensorCall(11216);if (PerlIOValid(g)) {
	      SensorCall(11211);PerlIOl *b = PerlIOBase(g);
	      SensorCall(11215);if (b && b->tab == &PerlIO_crlf) {
		   SensorCall(11212);if (!(b->flags & PERLIO_F_CRLF))
			b->flags |= PERLIO_F_CRLF;
		   SensorCall(11213);S_inherit_utf8_flag(g);
		   PerlIO_pop(aTHX_ f);
		   {IV  ReplaceReturn1380 = code; SensorCall(11214); return ReplaceReturn1380;}
	      }
	 }
    }
    SensorCall(11217);S_inherit_utf8_flag(f);
    {IV  ReplaceReturn1379 = code; SensorCall(11218); return ReplaceReturn1379;}
}


SSize_t
PerlIOCrlf_unread(pTHX_ PerlIO *f, const void *vbuf, Size_t count)
{
    SensorCall(11219);PerlIOCrlf * const c = PerlIOSelf(f, PerlIOCrlf);
    SensorCall(11221);if (c->nl) {	/* XXXX Shouldn't it be done only if b->ptr > c->nl? */
	SensorCall(11220);*(c->nl) = 0xd;
	c->nl = NULL;
    }
    SensorCall(11237);if (!(PerlIOBase(f)->flags & PERLIO_F_CRLF))
	{/*251*/{ssize_t  ReplaceReturn1378 = PerlIOBuf_unread(aTHX_ f, vbuf, count); SensorCall(11222); return ReplaceReturn1378;}/*252*/}
    else {
	SensorCall(11223);const STDCHAR *buf = (const STDCHAR *) vbuf + count;
	PerlIOBuf *b = PerlIOSelf(f, PerlIOBuf);
	SSize_t unread = 0;
	SensorCall(11224);if (PerlIOBase(f)->flags & PERLIO_F_WRBUF)
	    PerlIO_flush(f);
	SensorCall(11225);if (!b->buf)
	    PerlIO_get_base(f);
	SensorCall(11235);if (b->buf) {
	    SensorCall(11226);if (!(PerlIOBase(f)->flags & PERLIO_F_RDBUF)) {
		SensorCall(11227);b->end = b->ptr = b->buf + b->bufsiz;
		PerlIOBase(f)->flags |= PERLIO_F_RDBUF;
		b->posn -= b->bufsiz;
	    }
	    SensorCall(11234);while (count > 0 && b->ptr > b->buf) {
		SensorCall(11228);const int ch = *--buf;
		SensorCall(11233);if (ch == '\n') {
		    SensorCall(11229);if (b->ptr - 2 >= b->buf) {
			SensorCall(11230);*--(b->ptr) = 0xa;
			*--(b->ptr) = 0xd;
			unread++;
			count--;
		    }
		    else {
		    /* If b->ptr - 1 == b->buf, we are undoing reading 0xa */
			SensorCall(11231);*--(b->ptr) = 0xa;	/* Works even if 0xa == '\r' */
			unread++;
			count--;
		    }
		}
		else {
		    SensorCall(11232);*--(b->ptr) = ch;
		    unread++;
		    count--;
		}
	    }
	}
	{ssize_t  ReplaceReturn1377 = unread; SensorCall(11236); return ReplaceReturn1377;}
    }
SensorCall(11238);}

/* XXXX This code assumes that buffer size >=2, but does not check it... */
SSize_t
PerlIOCrlf_get_cnt(pTHX_ PerlIO *f)
{
    SensorCall(11239);PerlIOBuf * const b = PerlIOSelf(f, PerlIOBuf);
    SensorCall(11240);if (!b->buf)
	PerlIO_get_base(f);
    SensorCall(11259);if (PerlIOBase(f)->flags & PERLIO_F_RDBUF) {
	SensorCall(11241);PerlIOCrlf * const c = PerlIOSelf(f, PerlIOCrlf);
	SensorCall(11257);if ((PerlIOBase(f)->flags & PERLIO_F_CRLF) && (!c->nl || *c->nl == 0xd)) {
SensorCall(11242);	    STDCHAR *nl = (c->nl) ? c->nl : b->ptr;
	  scan:
	    SensorCall(11244);while (nl < b->end && *nl != 0xd)
		{/*177*/SensorCall(11243);nl++;/*178*/}
	    SensorCall(11256);if (nl < b->end && *nl == 0xd) {
	      test:
		SensorCall(11245);if (nl + 1 < b->end) {
		    SensorCall(11246);if (nl[1] == 0xa) {
			SensorCall(11247);*nl = '\n';
			c->nl = nl;
		    }
		    else {
			/*
			 * Not CR,LF but just CR
			 */
			SensorCall(11248);nl++;
			SensorCall(11249);goto scan;
		    }
		}
		else {
		    /*
		     * Blast - found CR as last char in buffer
		     */

		    SensorCall(11250);if (b->ptr < nl) {
			/*
			 * They may not care, defer work as long as
			 * possible
			 */
			SensorCall(11251);c->nl = nl;
			{ssize_t  ReplaceReturn1376 = (nl - b->ptr); SensorCall(11252); return ReplaceReturn1376;}
		    }
		    else {
			SensorCall(11253);int code;
			b->ptr++;       /* say we have read it as far as
					 * flush() is concerned */
			b->buf++;       /* Leave space in front of buffer */
			/* Note as we have moved buf up flush's
			   posn += ptr-buf
			   will naturally make posn point at CR
			 */
			b->bufsiz--;    /* Buffer is thus smaller */
			code = PerlIO_fill(f);  /* Fetch some more */
			b->bufsiz++;    /* Restore size for next time */
			b->buf--;       /* Point at space */
			b->ptr = nl = b->buf;   /* Which is what we hand
						 * off */
			*nl = 0xd;      /* Fill in the CR */
			SensorCall(11255);if (code == 0)
			    {/*179*/SensorCall(11254);goto test;/*180*/}  /* fill() call worked */
			/*
			 * CR at EOF - just fall through
			 */
			/* Should we clear EOF though ??? */
		    }
		}
	    }
	}
	{ssize_t  ReplaceReturn1375 = (((c->nl) ? (c->nl + 1) : b->end) - b->ptr); SensorCall(11258); return ReplaceReturn1375;}
    }
    {ssize_t  ReplaceReturn1374 = 0; SensorCall(11260); return ReplaceReturn1374;}
}

void
PerlIOCrlf_set_ptrcnt(pTHX_ PerlIO *f, STDCHAR * ptr, SSize_t cnt)
{
    SensorCall(11261);PerlIOBuf * const b = PerlIOSelf(f, PerlIOBuf);
    PerlIOCrlf * const c = PerlIOSelf(f, PerlIOCrlf);
    SensorCall(11262);if (!b->buf)
	PerlIO_get_base(f);
    SensorCall(11269);if (!ptr) {
	SensorCall(11263);if (c->nl) {
	    SensorCall(11264);ptr = c->nl + 1;
	    SensorCall(11266);if (ptr == b->end && *c->nl == 0xd) {
		/* Deferred CR at end of buffer case - we lied about count */
		SensorCall(11265);ptr--;
	    }
	}
	else {
	    SensorCall(11267);ptr = b->end;
	}
	SensorCall(11268);ptr -= cnt;
    }
    else {
	NOOP;
#if 0
	/*
	 * Test code - delete when it works ...
	 */
	IV flags = PerlIOBase(f)->flags;
	STDCHAR *chk = (c->nl) ? (c->nl+1) : b->end;
	if (ptr+cnt == c->nl && c->nl+1 == b->end && *c->nl == 0xd) {
	  /* Deferred CR at end of buffer case - we lied about count */
	  chk--;
	}
	chk -= cnt;

	if (ptr != chk ) {
	    Perl_croak(aTHX_ "ptr wrong %p != %p fl=%08" UVxf
		       " nl=%p e=%p for %d", (void*)ptr, (void*)chk,
		       flags, c->nl, b->end, cnt);
	}
#endif
    }
    SensorCall(11272);if (c->nl) {
	SensorCall(11270);if (ptr > c->nl) {
	    /*
	     * They have taken what we lied about
	     */
	    SensorCall(11271);*(c->nl) = 0xd;
	    c->nl = NULL;
	    ptr++;
	}
    }
    SensorCall(11273);b->ptr = ptr;
    PerlIOBase(f)->flags |= PERLIO_F_RDBUF;
}

SSize_t
PerlIOCrlf_write(pTHX_ PerlIO *f, const void *vbuf, Size_t count)
{
    SensorCall(11274);if (!(PerlIOBase(f)->flags & PERLIO_F_CRLF))
	{/*181*/{ssize_t  ReplaceReturn1373 = PerlIOBuf_write(aTHX_ f, vbuf, count); SensorCall(11275); return ReplaceReturn1373;}/*182*/}
    else {
	SensorCall(11276);PerlIOBuf * const b = PerlIOSelf(f, PerlIOBuf);
	const STDCHAR *buf = (const STDCHAR *) vbuf;
	const STDCHAR * const ebuf = buf + count;
	SensorCall(11277);if (!b->buf)
	    PerlIO_get_base(f);
	SensorCall(11279);if (!(PerlIOBase(f)->flags & PERLIO_F_CANWRITE))
	    {/*183*/{ssize_t  ReplaceReturn1372 = 0; SensorCall(11278); return ReplaceReturn1372;}/*184*/}
	SensorCall(11291);while (buf < ebuf) {
	    SensorCall(11280);const STDCHAR * const eptr = b->buf + b->bufsiz;
	    PerlIOBase(f)->flags |= PERLIO_F_WRBUF;
	    SensorCall(11290);while (buf < ebuf && b->ptr < eptr) {
		SensorCall(11281);if (*buf == '\n') {
		    SensorCall(11282);if ((b->ptr + 2) > eptr) {
			/*
			 * Not room for both
			 */
			PerlIO_flush(f);
			SensorCall(11283);break;
		    }
		    else {
			SensorCall(11284);*(b->ptr)++ = 0xd;      /* CR */
			*(b->ptr)++ = 0xa;      /* LF */
			buf++;
			SensorCall(11286);if (PerlIOBase(f)->flags & PERLIO_F_LINEBUF) {
			    PerlIO_flush(f);
			    SensorCall(11285);break;
			}
		    }
		}
		else {
		    SensorCall(11287);*(b->ptr)++ = *buf++;
		}
		SensorCall(11289);if (b->ptr >= eptr) {
		    PerlIO_flush(f);
		    SensorCall(11288);break;
		}
	    }
	}
	SensorCall(11292);if (PerlIOBase(f)->flags & PERLIO_F_UNBUF)
	    PerlIO_flush(f);
	{ssize_t  ReplaceReturn1371 = (buf - (STDCHAR *) vbuf); SensorCall(11293); return ReplaceReturn1371;}
    }
SensorCall(11294);}

IV
PerlIOCrlf_flush(pTHX_ PerlIO *f)
{
    SensorCall(11295);PerlIOCrlf * const c = PerlIOSelf(f, PerlIOCrlf);
    SensorCall(11297);if (c->nl) {
	SensorCall(11296);*(c->nl) = 0xd;
	c->nl = NULL;
    }
    {IV  ReplaceReturn1370 = PerlIOBuf_flush(aTHX_ f); SensorCall(11298); return ReplaceReturn1370;}
}

IV
PerlIOCrlf_binmode(pTHX_ PerlIO *f)
{
    SensorCall(11299);if ((PerlIOBase(f)->flags & PERLIO_F_CRLF)) {
	/* In text mode - flush any pending stuff and flip it */
	PerlIOBase(f)->flags &= ~PERLIO_F_CRLF;
#ifndef PERLIO_USING_CRLF
	/* CRLF is unusual case - if this is just the :crlf layer pop it */
	SensorCall(11300);PerlIO_pop(aTHX_ f);
#endif
    }
    {IV  ReplaceReturn1369 = 0; SensorCall(11301); return ReplaceReturn1369;}
}

PERLIO_FUNCS_DECL(PerlIO_crlf) = {
    sizeof(PerlIO_funcs),
    "crlf",
    sizeof(PerlIOCrlf),
    PERLIO_K_BUFFERED | PERLIO_K_CANCRLF | PERLIO_K_RAW,
    PerlIOCrlf_pushed,
    PerlIOBuf_popped,         /* popped */
    PerlIOBuf_open,
    PerlIOCrlf_binmode,       /* binmode */
    NULL,
    PerlIOBase_fileno,
    PerlIOBuf_dup,
    PerlIOBuf_read,             /* generic read works with ptr/cnt lies */
    PerlIOCrlf_unread,          /* Put CR,LF in buffer for each '\n' */
    PerlIOCrlf_write,           /* Put CR,LF in buffer for each '\n' */
    PerlIOBuf_seek,
    PerlIOBuf_tell,
    PerlIOBuf_close,
    PerlIOCrlf_flush,
    PerlIOBuf_fill,
    PerlIOBase_eof,
    PerlIOBase_error,
    PerlIOBase_clearerr,
    PerlIOBase_setlinebuf,
    PerlIOBuf_get_base,
    PerlIOBuf_bufsiz,
    PerlIOBuf_get_ptr,
    PerlIOCrlf_get_cnt,
    PerlIOCrlf_set_ptrcnt,
};

PerlIO *
Perl_PerlIO_stdin(pTHX)
{
SensorCall(11302);    dVAR;
    SensorCall(11304);if (!PL_perlio) {
	SensorCall(11303);PerlIO_stdstreams(aTHX);
    }
    {PerlIO * ReplaceReturn1368 = (PerlIO*)&PL_perlio[1]; SensorCall(11305); return ReplaceReturn1368;}
}

PerlIO *
Perl_PerlIO_stdout(pTHX)
{
SensorCall(11306);    dVAR;
    SensorCall(11308);if (!PL_perlio) {
	SensorCall(11307);PerlIO_stdstreams(aTHX);
    }
    {PerlIO * ReplaceReturn1367 = (PerlIO*)&PL_perlio[2]; SensorCall(11309); return ReplaceReturn1367;}
}

PerlIO *
Perl_PerlIO_stderr(pTHX)
{
SensorCall(11310);    dVAR;
    SensorCall(11312);if (!PL_perlio) {
	SensorCall(11311);PerlIO_stdstreams(aTHX);
    }
    {PerlIO * ReplaceReturn1366 = (PerlIO*)&PL_perlio[3]; SensorCall(11313); return ReplaceReturn1366;}
}

/*--------------------------------------------------------------------------------------*/

char *
PerlIO_getname(PerlIO *f, char *buf)
{
    dTHX;
#ifdef VMS
    char *name = NULL;
    bool exported = FALSE;
    FILE *stdio = PerlIOSelf(f, PerlIOStdio)->stdio;
    if (!stdio) {
	stdio = PerlIO_exportFILE(f,0);
	exported = TRUE;
    }
    if (stdio) {
	name = fgetname(stdio, buf);
	if (exported) PerlIO_releaseFILE(f,stdio);
    }
    return name;
#else
    PERL_UNUSED_ARG(f);
    PERL_UNUSED_ARG(buf);
    Perl_croak(aTHX_ "Don't know how to get file name");
    {char * ReplaceReturn1365 = NULL; SensorCall(11314); return ReplaceReturn1365;}
#endif
}


/*--------------------------------------------------------------------------------------*/
/*
 * Functions which can be called on any kind of PerlIO implemented in
 * terms of above
 */

#undef PerlIO_fdopen
PerlIO *
PerlIO_fdopen(int fd, const char *mode)
{
    dTHX;
    {PerlIO * ReplaceReturn1364 = PerlIO_openn(aTHX_ NULL, mode, fd, 0, 0, NULL, 0, NULL); SensorCall(11315); return ReplaceReturn1364;}
}

#undef PerlIO_open
PerlIO *
PerlIO_open(const char *path, const char *mode)
{
    dTHX;
    SV *name = sv_2mortal(newSVpv(path, 0));
    {PerlIO * ReplaceReturn1363 = PerlIO_openn(aTHX_ NULL, mode, -1, 0, 0, NULL, 1, &name); SensorCall(11316); return ReplaceReturn1363;}
}

#undef Perlio_reopen
PerlIO *
PerlIO_reopen(const char *path, const char *mode, PerlIO *f)
{
    dTHX;
    SV *name = sv_2mortal(newSVpv(path,0));
    {PerlIO * ReplaceReturn1362 = PerlIO_openn(aTHX_ NULL, mode, -1, 0, 0, f, 1, &name); SensorCall(11317); return ReplaceReturn1362;}
}

#undef PerlIO_getc
int
PerlIO_getc(PerlIO *f)
{
    dTHX;
    STDCHAR buf[1];
    SensorCall(11319);if ( 1 == PerlIO_read(f, buf, 1) ) {
	{int  ReplaceReturn1361 = (unsigned char) buf[0]; SensorCall(11318); return ReplaceReturn1361;}
    }
    {int  ReplaceReturn1360 = EOF; SensorCall(11320); return ReplaceReturn1360;}
}

#undef PerlIO_ungetc
int
PerlIO_ungetc(PerlIO *f, int ch)
{
    dTHX;
    SensorCall(11324);if (ch != EOF) {
SensorCall(11321);	STDCHAR buf = ch;
	SensorCall(11323);if (PerlIO_unread(f, &buf, 1) == 1)
	    {/*15*/{int  ReplaceReturn1359 = ch; SensorCall(11322); return ReplaceReturn1359;}/*16*/}
    }
    {int  ReplaceReturn1358 = EOF; SensorCall(11325); return ReplaceReturn1358;}
}

#undef PerlIO_putc
int
PerlIO_putc(PerlIO *f, int ch)
{
    dTHX;
    STDCHAR buf = ch;
    {int  ReplaceReturn1357 = PerlIO_write(f, &buf, 1); SensorCall(11326); return ReplaceReturn1357;}
}

#undef PerlIO_puts
int
PerlIO_puts(PerlIO *f, const char *s)
{
    dTHX;
    {int  ReplaceReturn1356 = PerlIO_write(f, s, strlen(s)); SensorCall(11327); return ReplaceReturn1356;}
}

#undef PerlIO_rewind
void
PerlIO_rewind(PerlIO *f)
{
    dTHX;
    PerlIO_seek(f, (Off_t) 0, SEEK_SET);
    PerlIO_clearerr(f);
}

#undef PerlIO_vprintf
int
PerlIO_vprintf(PerlIO *f, const char *fmt, va_list ap)
{
    dTHX;
    SV * sv;
    const char *s;
    STRLEN len;
    SSize_t wrote;
#ifdef NEED_VA_COPY
    va_list apc;
    Perl_va_copy(ap, apc);
    sv = vnewSVpvf(fmt, &apc);
#else
    sv = vnewSVpvf(fmt, &ap);
#endif
    s = SvPV_const(sv, len);
    wrote = PerlIO_write(f, s, len);
    SvREFCNT_dec(sv);
    {int  ReplaceReturn1355 = wrote; SensorCall(11328); return ReplaceReturn1355;}
}

#undef PerlIO_printf
int
PerlIO_printf(PerlIO *f, const char *fmt, ...)
{
    SensorCall(11329);va_list ap;
    int result;
    va_start(ap, fmt);
    result = PerlIO_vprintf(f, fmt, ap);
    va_end(ap);
    {int  ReplaceReturn1354 = result; SensorCall(11330); return ReplaceReturn1354;}
}

#undef PerlIO_stdoutf
int
PerlIO_stdoutf(const char *fmt, ...)
{
    dTHX;
    va_list ap;
    int result;
    va_start(ap, fmt);
    result = PerlIO_vprintf(PerlIO_stdout(), fmt, ap);
    va_end(ap);
    {int  ReplaceReturn1353 = result; SensorCall(11331); return ReplaceReturn1353;}
}

#undef PerlIO_tmpfile
PerlIO *
PerlIO_tmpfile(void)
{
     dTHX;
     PerlIO *f = NULL;
#ifdef WIN32
     const int fd = win32_tmpfd();
     if (fd >= 0)
	  f = PerlIO_fdopen(fd, "w+b");
#else /* WIN32 */
#    if defined(HAS_MKSTEMP) && ! defined(VMS) && ! defined(OS2)
     int fd = -1;
     char tempname[] = "/tmp/PerlIO_XXXXXX";
     const char * const tmpdir = PL_tainting ? NULL : PerlEnv_getenv("TMPDIR");
     SV * sv = NULL;
     /*
      * I have no idea how portable mkstemp() is ... NI-S
      */
     SensorCall(11333);if (tmpdir && *tmpdir) {
	 /* if TMPDIR is set and not empty, we try that first */
	 SensorCall(11332);sv = newSVpv(tmpdir, 0);
	 sv_catpv(sv, tempname + 4);
	 fd = mkstemp(SvPVX(sv));
     }
     SensorCall(11335);if (fd < 0) {
	 SensorCall(11334);sv = NULL;
	 /* else we try /tmp */
	 fd = mkstemp(tempname);
     }
     SensorCall(11338);if (fd >= 0) {
	  SensorCall(11336);f = PerlIO_fdopen(fd, "w+");
	  SensorCall(11337);if (f)
	       PerlIOBase(f)->flags |= PERLIO_F_TEMP;
	  PerlLIO_unlink(sv ? SvPVX_const(sv) : tempname);
     }
     SvREFCNT_dec(sv);
#    else	/* !HAS_MKSTEMP, fallback to stdio tmpfile(). */
     FILE * const stdio = PerlSIO_tmpfile();

     if (stdio)
	  f = PerlIO_fdopen(fileno(stdio), "w+");

#    endif /* else HAS_MKSTEMP */
#endif /* else WIN32 */
     {PerlIO * ReplaceReturn1352 = f; SensorCall(11339); return ReplaceReturn1352;}
}

#undef HAS_FSETPOS
#undef HAS_FGETPOS

#endif                          /* USE_SFIO */
#endif                          /* PERLIO_IS_STDIO */

/*======================================================================================*/
/*
 * Now some functions in terms of above which may be needed even if we are
 * not in true PerlIO mode
 */
const char *
Perl_PerlIO_context_layers(pTHX_ const char *mode)
{
SensorCall(11340);    dVAR;
    const char *direction = NULL;
    SV *layers;
    /*
     * Need to supply default layer info from open.pm
     */

    SensorCall(11341);if (!PL_curcop)
	return NULL;

    SensorCall(11346);if (mode && mode[0] != 'r') {
	SensorCall(11342);if (PL_curcop->cop_hints & HINT_LEXICAL_IO_OUT)
	    {/*55*/SensorCall(11343);direction = "open>";/*56*/}
    } else {
	SensorCall(11344);if (PL_curcop->cop_hints & HINT_LEXICAL_IO_IN)
	    {/*57*/SensorCall(11345);direction = "open<";/*58*/}
    }
    SensorCall(11347);if (!direction)
	return NULL;

    SensorCall(11348);layers = cop_hints_fetch_pvn(PL_curcop, direction, 5, 0, 0);

    assert(layers);
    {const char * ReplaceReturn1351 = SvOK(layers) ? SvPV_nolen_const(layers) : NULL; SensorCall(11349); return ReplaceReturn1351;}
}


#ifndef HAS_FSETPOS
#undef PerlIO_setpos
int
PerlIO_setpos(PerlIO *f, SV *pos)
{
    dTHX;
    SensorCall(11352);if (SvOK(pos)) {
	SensorCall(11350);STRLEN len;
	const Off_t * const posn = (Off_t *) SvPV(pos, len);
	SensorCall(11351);if (f && len == sizeof(Off_t))
	    return PerlIO_seek(f, *posn, SEEK_SET);
    }
    SETERRNO(EINVAL, SS_IVCHAN);
    {int  ReplaceReturn1350 = -1; SensorCall(11353); return ReplaceReturn1350;}
}
#else
#undef PerlIO_setpos
int
PerlIO_setpos(PerlIO *f, SV *pos)
{
    dTHX;
    if (SvOK(pos)) {
	STRLEN len;
	Fpos_t * const fpos = (Fpos_t *) SvPV(pos, len);
	if (f && len == sizeof(Fpos_t)) {
#if defined(USE_64_BIT_STDIO) && defined(USE_FSETPOS64)
	    return fsetpos64(f, fpos);
#else
	    return fsetpos(f, fpos);
#endif
	}
    }
    SETERRNO(EINVAL, SS_IVCHAN);
    return -1;
}
#endif

#ifndef HAS_FGETPOS
#undef PerlIO_getpos
int
PerlIO_getpos(PerlIO *f, SV *pos)
{
    dTHX;
    Off_t posn = PerlIO_tell(f);
    sv_setpvn(pos, (char *) &posn, sizeof(posn));
    {int  ReplaceReturn1349 = (posn == (Off_t) - 1) ? -1 : 0; SensorCall(11354); return ReplaceReturn1349;}
}
#else
#undef PerlIO_getpos
int
PerlIO_getpos(PerlIO *f, SV *pos)
{
    dTHX;
    Fpos_t fpos;
    int code;
#if defined(USE_64_BIT_STDIO) && defined(USE_FSETPOS64)
    code = fgetpos64(f, &fpos);
#else
    code = fgetpos(f, &fpos);
#endif
    sv_setpvn(pos, (char *) &fpos, sizeof(fpos));
    return code;
}
#endif

#if (defined(PERLIO_IS_STDIO) || !defined(USE_SFIO)) && !defined(HAS_VPRINTF)

int
vprintf(char *pat, char *args)
{
    _doprnt(pat, args, stdout);
    return 0;                   /* wrong, but perl doesn't use the return
				 * value */
}

int
vfprintf(FILE *fd, char *pat, char *args)
{
    _doprnt(pat, args, fd);
    return 0;                   /* wrong, but perl doesn't use the return
				 * value */
}

#endif

#ifndef PerlIO_vsprintf
int
PerlIO_vsprintf(char *s, int n, const char *fmt, va_list ap)
{
    dTHX; 
    const int val = my_vsnprintf(s, n > 0 ? n : 0, fmt, ap);
    PERL_UNUSED_CONTEXT;

#ifndef PERL_MY_VSNPRINTF_GUARDED
    if (val < 0 || (n > 0 ? val >= n : 0)) {
	Perl_croak(aTHX_ "panic: my_vsnprintf overflow in PerlIO_vsprintf\n");
    }
#endif
    {int  ReplaceReturn1348 = val; SensorCall(11355); return ReplaceReturn1348;}
}
#endif

#ifndef PerlIO_sprintf
int
PerlIO_sprintf(char *s, int n, const char *fmt, ...)
{
    SensorCall(11356);va_list ap;
    int result;
    va_start(ap, fmt);
    result = PerlIO_vsprintf(s, n, fmt, ap);
    va_end(ap);
    {int  ReplaceReturn1347 = result; SensorCall(11357); return ReplaceReturn1347;}
}
#endif

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 *
 * ex: set ts=8 sts=4 sw=4 noet:
 */
