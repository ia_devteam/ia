#include "var/tmp/sensor.h"
/*    pad.c
 *
 *    Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
 *    by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 */

/*
 *  'Anyway: there was this Mr. Frodo left an orphan and stranded, as you
 *   might say, among those queer Bucklanders, being brought up anyhow in
 *   Brandy Hall.  A regular warren, by all accounts.  Old Master Gorbadoc
 *   never had fewer than a couple of hundred relations in the place.
 *   Mr. Bilbo never did a kinder deed than when he brought the lad back
 *   to live among decent folk.'                           --the Gaffer
 *
 *     [p.23 of _The Lord of the Rings_, I/i: "A Long-Expected Party"]
 */

/* XXX DAPM
 * As of Sept 2002, this file is new and may be in a state of flux for
 * a while. I've marked things I intent to come back and look at further
 * with an 'XXX DAPM' comment.
 */

/*
=head1 Pad Data Structures

=for apidoc Amx|PADLIST *|CvPADLIST|CV *cv

CV's can have CvPADLIST(cv) set to point to an AV.  This is the CV's
scratchpad, which stores lexical variables and opcode temporary and
per-thread values.

For these purposes "forms" are a kind-of CV, eval""s are too (except they're
not callable at will and are always thrown away after the eval"" is done
executing). Require'd files are simply evals without any outer lexical
scope.

XSUBs don't have CvPADLIST set - dXSTARG fetches values from PL_curpad,
but that is really the callers pad (a slot of which is allocated by
every entersub).

The CvPADLIST AV has the REFCNT of its component items managed "manually"
(mostly in pad.c) rather than by normal av.c rules.  So we turn off AvREAL
just before freeing it, to let av.c know not to touch the entries.
The items in the AV are not SVs as for a normal AV, but other AVs:

0'th Entry of the CvPADLIST is an AV which represents the "names" or rather
the "static type information" for lexicals.

The CvDEPTH'th entry of CvPADLIST AV is an AV which is the stack frame at that
depth of recursion into the CV.
The 0'th slot of a frame AV is an AV which is @_.
other entries are storage for variables and op targets.

Iterating over the names AV iterates over all possible pad
items. Pad slots that are SVs_PADTMP (targets/GVs/constants) end up having
&PL_sv_undef "names" (see pad_alloc()).

Only my/our variable (SVs_PADMY/SVs_PADOUR) slots get valid names.
The rest are op targets/GVs/constants which are statically allocated
or resolved at compile time.  These don't have names by which they
can be looked up from Perl code at run time through eval"" like
my/our variables can be.  Since they can't be looked up by "name"
but only by their index allocated at compile time (which is usually
in PL_op->op_targ), wasting a name SV for them doesn't make sense.

The SVs in the names AV have their PV being the name of the variable.
xlow+1..xhigh inclusive in the NV union is a range of cop_seq numbers for
which the name is valid (accessed through the macros COP_SEQ_RANGE_LOW and
_HIGH).  During compilation, these fields may hold the special value
PERL_PADSEQ_INTRO to indicate various stages:

   COP_SEQ_RANGE_LOW        _HIGH
   -----------------        -----
   PERL_PADSEQ_INTRO            0   variable not yet introduced:   { my ($x
   valid-seq#   PERL_PADSEQ_INTRO   variable in scope:             { my ($x)
   valid-seq#          valid-seq#   compilation of scope complete: { my ($x) }

For typed lexicals name SV is SVt_PVMG and SvSTASH
points at the type.  For C<our> lexicals, the type is also SVt_PVMG, with the
SvOURSTASH slot pointing at the stash of the associated global (so that
duplicate C<our> declarations in the same package can be detected).  SvUVX is
sometimes hijacked to store the generation number during compilation.

If SvFAKE is set on the name SV, then that slot in the frame AV is
a REFCNT'ed reference to a lexical from "outside". In this case,
the name SV does not use xlow and xhigh to store a cop_seq range, since it is
in scope throughout. Instead xhigh stores some flags containing info about
the real lexical (is it declared in an anon, and is it capable of being
instantiated multiple times?), and for fake ANONs, xlow contains the index
within the parent's pad where the lexical's value is stored, to make
cloning quicker.

If the 'name' is '&' the corresponding entry in frame AV
is a CV representing a possible closure.
(SvFAKE and name of '&' is not a meaningful combination currently but could
become so if C<my sub foo {}> is implemented.)

Note that formats are treated as anon subs, and are cloned each time
write is called (if necessary).

The flag SVs_PADSTALE is cleared on lexicals each time the my() is executed,
and set on scope exit. This allows the 'Variable $x is not available' warning
to be generated in evals, such as 

    { my $x = 1; sub f { eval '$x'} } f();

For state vars, SVs_PADSTALE is overloaded to mean 'not yet initialised'

=for apidoc AmxU|AV *|PL_comppad_name

During compilation, this points to the array containing the names part
of the pad for the currently-compiling code.

=for apidoc AmxU|AV *|PL_comppad

During compilation, this points to the array containing the values
part of the pad for the currently-compiling code.  (At runtime a CV may
have many such value arrays; at compile time just one is constructed.)
At runtime, this points to the array containing the currently-relevant
values for the pad for the currently-executing code.

=for apidoc AmxU|SV **|PL_curpad

Points directly to the body of the L</PL_comppad> array.
(I.e., this is C<AvARRAY(PL_comppad)>.)

=cut
*/


#include "EXTERN.h"
#define PERL_IN_PAD_C
#include "perl.h"
#include "keywords.h"

#define COP_SEQ_RANGE_LOW_set(sv,val)		\
  STMT_START { ((XPVNV*)SvANY(sv))->xnv_u.xpad_cop_seq.xlow = (val); } STMT_END
#define COP_SEQ_RANGE_HIGH_set(sv,val)		\
  STMT_START { ((XPVNV*)SvANY(sv))->xnv_u.xpad_cop_seq.xhigh = (val); } STMT_END

#define PARENT_PAD_INDEX_set(sv,val)		\
  STMT_START { ((XPVNV*)SvANY(sv))->xnv_u.xpad_cop_seq.xlow = (val); } STMT_END
#define PARENT_FAKELEX_FLAGS_set(sv,val)	\
  STMT_START { ((XPVNV*)SvANY(sv))->xnv_u.xpad_cop_seq.xhigh = (val); } STMT_END

/*
=for apidoc mx|void|pad_peg|const char *s

When PERL_MAD is enabled, this is a small no-op function that gets called
at the start of each pad-related function.  It can be breakpointed to
track all pad operations.  The parameter is a string indicating the type
of pad operation being performed.

=cut
*/

#ifdef PERL_MAD
void pad_peg(const char* s) {
    static int pegcnt; /* XXX not threadsafe */
    PERL_UNUSED_ARG(s);

    PERL_ARGS_ASSERT_PAD_PEG;

    pegcnt++;
}
#endif

/*
This is basically sv_eq_flags() in sv.c, but we avoid the magic
and bytes checking.
*/

static bool
sv_eq_pvn_flags(pTHX_ const SV *sv, const char* pv, const STRLEN pvlen, const U32 flags) {
    SensorCall(9000);if ( (SvUTF8(sv) & SVf_UTF8 ) != (flags & SVf_UTF8) ) {
        SensorCall(9001);const char *pv1 = SvPVX_const(sv);
        STRLEN cur1     = SvCUR(sv);
        const char *pv2 = pv;
        STRLEN cur2     = pvlen;
	SensorCall(9006);if (PL_encoding) {
              SensorCall(9002);SV* svrecode = NULL;
	      SensorCall(9005);if (SvUTF8(sv)) {
		   SensorCall(9003);svrecode = newSVpvn(pv2, cur2);
		   sv_recode_to_utf8(svrecode, PL_encoding);
		   pv2      = SvPV_const(svrecode, cur2);
	      }
	      else {
		   SensorCall(9004);svrecode = newSVpvn(pv1, cur1);
		   sv_recode_to_utf8(svrecode, PL_encoding);
		   pv1      = SvPV_const(svrecode, cur1);
	      }
              SvREFCNT_dec(svrecode);
        }
        SensorCall(9009);if (flags & SVf_UTF8)
            {/*83*/{_Bool  ReplaceReturn2164 = (bytes_cmp_utf8(
                        (const U8*)pv1, cur1,
		        (const U8*)pv2, cur2) == 0); SensorCall(9007); return ReplaceReturn2164;}/*84*/}
        else
            {/*85*/{_Bool  ReplaceReturn2163 = (bytes_cmp_utf8(
                        (const U8*)pv2, cur2,
		        (const U8*)pv1, cur1) == 0); SensorCall(9008); return ReplaceReturn2163;}/*86*/}
    }
    else
        {/*87*/{_Bool  ReplaceReturn2162 = ((SvPVX_const(sv) == pv)
                    || memEQ(SvPVX_const(sv), pv, pvlen)); SensorCall(9010); return ReplaceReturn2162;}/*88*/}
SensorCall(9011);}


/*
=for apidoc Am|PADLIST *|pad_new|int flags

Create a new padlist, updating the global variables for the
currently-compiling padlist to point to the new padlist.  The following
flags can be OR'ed together:

    padnew_CLONE	this pad is for a cloned CV
    padnew_SAVE		save old globals on the save stack
    padnew_SAVESUB	also save extra stuff for start of sub

=cut
*/

PADLIST *
Perl_pad_new(pTHX_ int flags)
{
SensorCall(9012);    dVAR;
    AV *padlist, *padname, *pad;
    SV **ary;

    ASSERT_CURPAD_LEGAL("pad_new");

    /* XXX DAPM really need a new SAVEt_PAD which restores all or most
     * vars (based on flags) rather than storing vals + addresses for
     * each individually. Also see pad_block_start.
     * XXX DAPM Try to see whether all these conditionals are required
     */

    /* save existing state, ... */

    SensorCall(9015);if (flags & padnew_SAVE) {
	SAVECOMPPAD();
	SAVESPTR(PL_comppad_name);
	SensorCall(9014);if (! (flags & padnew_CLONE)) {
	    SAVEI32(PL_padix);
	    SAVEI32(PL_comppad_name_fill);
	    SAVEI32(PL_min_intro_pending);
	    SAVEI32(PL_max_intro_pending);
	    SAVEBOOL(PL_cv_has_eval);
	    SensorCall(9013);if (flags & padnew_SAVESUB) {
		SAVEBOOL(PL_pad_reset_pending);
	    }
	}
    }
    /* XXX DAPM interestingly, PL_comppad_name_floor never seems to be
     * saved - check at some pt that this is okay */

    /* ... create new pad ... */

    SensorCall(9016);padlist	= newAV();
    padname	= newAV();
    pad		= newAV();

    SensorCall(9018);if (flags & padnew_CLONE) {
	/* XXX DAPM  I dont know why cv_clone needs it
	 * doing differently yet - perhaps this separate branch can be
	 * dispensed with eventually ???
	 */

        SensorCall(9017);AV * const a0 = newAV();			/* will be @_ */
	av_store(pad, 0, MUTABLE_SV(a0));
	AvREIFY_only(a0);
    }
    else {
	av_store(pad, 0, NULL);
    }

    /* Most subroutines never recurse, hence only need 2 entries in the padlist
       array - names, and depth=1.  The default for av_store() is to allocate
       0..3, and even an explicit call to av_extend() with <3 will be rounded
       up, so we inline the allocation of the array here.  */
    Newx(ary, 2, SV*);
    AvFILLp(padlist) = 1;
    AvMAX(padlist) = 1;
    AvALLOC(padlist) = ary;
    AvARRAY(padlist) = ary;
    SensorCall(9019);ary[0] = MUTABLE_SV(padname);
    ary[1] = MUTABLE_SV(pad);

    /* ... then update state variables */

    PL_comppad_name	= padname;
    PL_comppad		= pad;
    PL_curpad		= AvARRAY(pad);

    SensorCall(9020);if (! (flags & padnew_CLONE)) {
	PL_comppad_name_fill = 0;
	PL_min_intro_pending = 0;
	PL_padix	     = 0;
	PL_cv_has_eval	     = 0;
    }

    DEBUG_X(PerlIO_printf(Perl_debug_log,
	  "Pad 0x%"UVxf"[0x%"UVxf"] new:       compcv=0x%"UVxf
	      " name=0x%"UVxf" flags=0x%"UVxf"\n",
	  PTR2UV(PL_comppad), PTR2UV(PL_curpad), PTR2UV(PL_compcv),
	      PTR2UV(padname), (UV)flags
	)
    );

    {PADLIST * ReplaceReturn2161 = (PADLIST*)padlist; SensorCall(9021); return ReplaceReturn2161;}
}


/*
=head1 Embedding Functions

=for apidoc cv_undef

Clear out all the active components of a CV. This can happen either
by an explicit C<undef &foo>, or by the reference count going to zero.
In the former case, we keep the CvOUTSIDE pointer, so that any anonymous
children can still follow the full lexical scope chain.

=cut
*/

void
Perl_cv_undef(pTHX_ CV *cv)
{
SensorCall(9022);    dVAR;
    const PADLIST *padlist = CvPADLIST(cv);

    PERL_ARGS_ASSERT_CV_UNDEF;

    DEBUG_X(PerlIO_printf(Perl_debug_log,
	  "CV undef: cv=0x%"UVxf" comppad=0x%"UVxf"\n",
	    PTR2UV(cv), PTR2UV(PL_comppad))
    );

    SensorCall(9023);if (CvFILE(cv) && CvDYNFILE(cv)) {
	Safefree(CvFILE(cv));
    }
    CvFILE(cv) = NULL;

    SensorCall(9026);if (!CvISXSUB(cv) && CvROOT(cv)) {
	SensorCall(9024);if (SvTYPE(cv) == SVt_PVCV && CvDEPTH(cv))
	    {/*5*/SensorCall(9025);Perl_croak(aTHX_ "Can't undef active subroutine");/*6*/}
	ENTER;

	PAD_SAVE_SETNULLPAD();

	op_free(CvROOT(cv));
	CvROOT(cv) = NULL;
	CvSTART(cv) = NULL;
	LEAVE;
    }
    SvPOK_off(MUTABLE_SV(cv));		/* forget prototype */
    CvGV_set(cv, NULL);

    /* This statement and the subsequence if block was pad_undef().  */
    pad_peg("pad_undef");

    SensorCall(9045);if (padlist && !SvIS_FREED(padlist) /* may be during global destruction */
	) {
	SensorCall(9027);I32 ix;

	/* Free the padlist associated with a CV.
	   If parts of it happen to be current, we null the relevant PL_*pad*
	   global vars so that we don't have any dangling references left.
	   We also repoint the CvOUTSIDE of any about-to-be-orphaned inner
	   subs to the outer of this cv.  */

	DEBUG_X(PerlIO_printf(Perl_debug_log,
			      "Pad undef: cv=0x%"UVxf" padlist=0x%"UVxf" comppad=0x%"UVxf"\n",
			      PTR2UV(cv), PTR2UV(padlist), PTR2UV(PL_comppad))
		);

	/* detach any '&' anon children in the pad; if afterwards they
	 * are still live, fix up their CvOUTSIDEs to point to our outside,
	 * bypassing us. */
	/* XXX DAPM for efficiency, we should only do this if we know we have
	 * children, or integrate this loop with general cleanup */

	SensorCall(9037);if (PL_phase != PERL_PHASE_DESTRUCT) { /* don't bother during global destruction */
	    SensorCall(9028);CV * const outercv = CvOUTSIDE(cv);
	    const U32 seq = CvOUTSIDE_SEQ(cv);
	    AV *  const comppad_name = MUTABLE_AV(AvARRAY(padlist)[0]);
	    SV ** const namepad = AvARRAY(comppad_name);
	    AV *  const comppad = MUTABLE_AV(AvARRAY(padlist)[1]);
	    SV ** const curpad = AvARRAY(comppad);
	    SensorCall(9036);for (ix = AvFILLp(comppad_name); ix > 0; ix--) {
		SensorCall(9029);SV * const namesv = namepad[ix];
		SensorCall(9035);if (namesv && namesv != &PL_sv_undef
		    && *SvPVX_const(namesv) == '&')
		    {
			SensorCall(9030);CV * const innercv = MUTABLE_CV(curpad[ix]);
			U32 inner_rc = SvREFCNT(innercv);
			assert(inner_rc);
			assert(SvTYPE(innercv) != SVt_PVFM);
			namepad[ix] = NULL;
			SvREFCNT_dec(namesv);

			SensorCall(9032);if (SvREFCNT(comppad) < 2) { /* allow for /(?{ sub{} })/  */
			    SensorCall(9031);curpad[ix] = NULL;
			    SvREFCNT_dec(innercv);
			    inner_rc--;
			}

			/* in use, not just a prototype */
			SensorCall(9034);if (inner_rc && (CvOUTSIDE(innercv) == cv)) {
			    assert(CvWEAKOUTSIDE(innercv));
			    /* don't relink to grandfather if he's being freed */
			    SensorCall(9033);if (outercv && SvREFCNT(outercv)) {
				CvWEAKOUTSIDE_off(innercv);
				CvOUTSIDE(innercv) = outercv;
				CvOUTSIDE_SEQ(innercv) = seq;
				SvREFCNT_inc_simple_void_NN(outercv);
			    }
			    else {
				CvOUTSIDE(innercv) = NULL;
			    }
			}
		    }
	    }
	}

	SensorCall(9038);ix = AvFILLp(padlist);
	SensorCall(9042);while (ix > 0) {
	    SensorCall(9039);SV* const sv = AvARRAY(padlist)[ix--];
	    SensorCall(9041);if (sv) {
		SensorCall(9040);if (sv == (const SV *)PL_comppad) {
		    PL_comppad = NULL;
		    PL_curpad = NULL;
		}
		SvREFCNT_dec(sv);
	    }
	}
	{
	    SensorCall(9043);SV *const sv = AvARRAY(padlist)[0];
	    SensorCall(9044);if (sv == (const SV *)PL_comppad_name)
		PL_comppad_name = NULL;
	    SvREFCNT_dec(sv);
	}
	AvREAL_off(CvPADLIST(cv));
	SvREFCNT_dec(MUTABLE_SV(CvPADLIST(cv)));
	CvPADLIST(cv) = NULL;
    }


    /* remove CvOUTSIDE unless this is an undef rather than a free */
    SensorCall(9047);if (!SvREFCNT(cv) && CvOUTSIDE(cv)) {
	SensorCall(9046);if (!CvWEAKOUTSIDE(cv))
	    SvREFCNT_dec(CvOUTSIDE(cv));
	CvOUTSIDE(cv) = NULL;
    }
    SensorCall(9048);if (CvCONST(cv)) {
	SvREFCNT_dec(MUTABLE_SV(CvXSUBANY(cv).any_ptr));
	CvCONST_off(cv);
    }
    SensorCall(9049);if (CvISXSUB(cv) && CvXSUB(cv)) {
	CvXSUB(cv) = NULL;
    }
    /* delete all flags except WEAKOUTSIDE and CVGV_RC, which indicate the
     * ref status of CvOUTSIDE and CvGV, and ANON, which pp_entersub uses
     * to choose an error message */
    CvFLAGS(cv) &= (CVf_WEAKOUTSIDE|CVf_CVGV_RC|CVf_ANON);
}

/*
=for apidoc m|PADOFFSET|pad_alloc_name|SV *namesv|U32 flags|HV *typestash|HV *ourstash

Allocates a place in the currently-compiling
pad (via L<perlapi/pad_alloc>) and
then stores a name for that entry.  I<namesv> is adopted and becomes the
name entry; it must already contain the name string and be sufficiently
upgraded.  I<typestash> and I<ourstash> and the C<padadd_STATE> flag get
added to I<namesv>.  None of the other
processing of L<perlapi/pad_add_name_pvn>
is done.  Returns the offset of the allocated pad slot.

=cut
*/

static PADOFFSET
S_pad_alloc_name(pTHX_ SV *namesv, U32 flags, HV *typestash, HV *ourstash)
{
SensorCall(9050);    dVAR;
    const PADOFFSET offset = pad_alloc(OP_PADSV, SVs_PADMY);

    PERL_ARGS_ASSERT_PAD_ALLOC_NAME;

    ASSERT_CURPAD_ACTIVE("pad_alloc_name");

    SensorCall(9051);if (typestash) {
	assert(SvTYPE(namesv) == SVt_PVMG);
	SvPAD_TYPED_on(namesv);
	SvSTASH_set(namesv, MUTABLE_HV(SvREFCNT_inc_simple_NN(MUTABLE_SV(typestash))));
    }
    SensorCall(9053);if (ourstash) {
	SvPAD_OUR_on(namesv);
	SvOURSTASH_set(namesv, ourstash);
	SvREFCNT_inc_simple_void_NN(ourstash);
    }
    else {/*55*/SensorCall(9052);if (flags & padadd_STATE) {
	SvPAD_STATE_on(namesv);
    ;/*56*/}}

    av_store(PL_comppad_name, offset, namesv);
    {PADOFFSET  ReplaceReturn2160 = offset; SensorCall(9054); return ReplaceReturn2160;}
}

/*
=for apidoc Am|PADOFFSET|pad_add_name_pvn|const char *namepv|STRLEN namelen|U32 flags|HV *typestash|HV *ourstash

Allocates a place in the currently-compiling pad for a named lexical
variable.  Stores the name and other metadata in the name part of the
pad, and makes preparations to manage the variable's lexical scoping.
Returns the offset of the allocated pad slot.

I<namepv>/I<namelen> specify the variable's name, including leading sigil.
If I<typestash> is non-null, the name is for a typed lexical, and this
identifies the type.  If I<ourstash> is non-null, it's a lexical reference
to a package variable, and this identifies the package.  The following
flags can be OR'ed together:

    padadd_OUR          redundantly specifies if it's a package var
    padadd_STATE        variable will retain value persistently
    padadd_NO_DUP_CHECK skip check for lexical shadowing

=cut
*/

PADOFFSET
Perl_pad_add_name_pvn(pTHX_ const char *namepv, STRLEN namelen,
		U32 flags, HV *typestash, HV *ourstash)
{
SensorCall(9055);    dVAR;
    PADOFFSET offset;
    SV *namesv;
    bool is_utf8;

    PERL_ARGS_ASSERT_PAD_ADD_NAME_PVN;

    SensorCall(9057);if (flags & ~(padadd_OUR|padadd_STATE|padadd_NO_DUP_CHECK|padadd_UTF8_NAME))
	{/*13*/SensorCall(9056);Perl_croak(aTHX_ "panic: pad_add_name_pvn illegal flag bits 0x%" UVxf,
		   (UV)flags);/*14*/}

    SensorCall(9058);namesv = newSV_type((ourstash || typestash) ? SVt_PVMG : SVt_PVNV);
    
    SensorCall(9060);if ((is_utf8 = ((flags & padadd_UTF8_NAME) != 0))) {
        SensorCall(9059);namepv = (const char*)bytes_from_utf8((U8*)namepv, &namelen, &is_utf8);
    }

    sv_setpvn(namesv, namepv, namelen);

    SensorCall(9062);if (is_utf8) {
        SensorCall(9061);flags |= padadd_UTF8_NAME;
        SvUTF8_on(namesv);
    }
    else
        flags &= ~padadd_UTF8_NAME;

    SensorCall(9063);if ((flags & padadd_NO_DUP_CHECK) == 0) {
	/* check for duplicate declaration */
	pad_check_dup(namesv, flags & padadd_OUR, ourstash);
    }

    SensorCall(9064);offset = pad_alloc_name(namesv, flags & ~padadd_UTF8_NAME, typestash, ourstash);

    /* not yet introduced */
    COP_SEQ_RANGE_LOW_set(namesv, PERL_PADSEQ_INTRO);
    COP_SEQ_RANGE_HIGH_set(namesv, 0);

    SensorCall(9065);if (!PL_min_intro_pending)
	PL_min_intro_pending = offset;
    PL_max_intro_pending = offset;
    /* if it's not a simple scalar, replace with an AV or HV */
    assert(SvTYPE(PL_curpad[offset]) == SVt_NULL);
    assert(SvREFCNT(PL_curpad[offset]) == 1);
    SensorCall(9066);if (namelen != 0 && *namepv == '@')
	sv_upgrade(PL_curpad[offset], SVt_PVAV);
    else if (namelen != 0 && *namepv == '%')
	sv_upgrade(PL_curpad[offset], SVt_PVHV);
    assert(SvPADMY(PL_curpad[offset]));
    DEBUG_Xv(PerlIO_printf(Perl_debug_log,
			   "Pad addname: %ld \"%s\" new lex=0x%"UVxf"\n",
			   (long)offset, SvPVX(namesv),
			   PTR2UV(PL_curpad[offset])));

    {PADOFFSET  ReplaceReturn2159 = offset; SensorCall(9067); return ReplaceReturn2159;}
}

/*
=for apidoc Am|PADOFFSET|pad_add_name_pv|const char *name|U32 flags|HV *typestash|HV *ourstash

Exactly like L</pad_add_name_pvn>, but takes a nul-terminated string
instead of a string/length pair.

=cut
*/

PADOFFSET
Perl_pad_add_name_pv(pTHX_ const char *name,
		     const U32 flags, HV *typestash, HV *ourstash)
{
    PERL_ARGS_ASSERT_PAD_ADD_NAME_PV;
    {PADOFFSET  ReplaceReturn2158 = pad_add_name_pvn(name, strlen(name), flags, typestash, ourstash); SensorCall(9068); return ReplaceReturn2158;}
}

/*
=for apidoc Am|PADOFFSET|pad_add_name_sv|SV *name|U32 flags|HV *typestash|HV *ourstash

Exactly like L</pad_add_name_pvn>, but takes the name string in the form
of an SV instead of a string/length pair.

=cut
*/

PADOFFSET
Perl_pad_add_name_sv(pTHX_ SV *name, U32 flags, HV *typestash, HV *ourstash)
{
    SensorCall(9069);char *namepv;
    STRLEN namelen;
    PERL_ARGS_ASSERT_PAD_ADD_NAME_SV;
    namepv = SvPV(name, namelen);
    SensorCall(9070);if (SvUTF8(name))
        flags |= padadd_UTF8_NAME;
    {PADOFFSET  ReplaceReturn2157 = pad_add_name_pvn(namepv, namelen, flags, typestash, ourstash); SensorCall(9071); return ReplaceReturn2157;}
}

/*
=for apidoc Amx|PADOFFSET|pad_alloc|I32 optype|U32 tmptype

Allocates a place in the currently-compiling pad,
returning the offset of the allocated pad slot.
No name is initially attached to the pad slot.
I<tmptype> is a set of flags indicating the kind of pad entry required,
which will be set in the value SV for the allocated pad entry:

    SVs_PADMY    named lexical variable ("my", "our", "state")
    SVs_PADTMP   unnamed temporary store

I<optype> should be an opcode indicating the type of operation that the
pad entry is to support.  This doesn't affect operational semantics,
but is used for debugging.

=cut
*/

/* XXX DAPM integrate alloc(), add_name() and add_anon(),
 * or at least rationalise ??? */
/* And flag whether the incoming name is UTF8 or 8 bit?
   Could do this either with the +ve/-ve hack of the HV code, or expanding
   the flag bits. Either way, this makes proper Unicode safe pad support.
   NWC
*/

PADOFFSET
Perl_pad_alloc(pTHX_ I32 optype, U32 tmptype)
{
SensorCall(9072);    dVAR;
    SV *sv;
    I32 retval;

    PERL_UNUSED_ARG(optype);
    ASSERT_CURPAD_ACTIVE("pad_alloc");

    SensorCall(9074);if (AvARRAY(PL_comppad) != PL_curpad)
	{/*15*/SensorCall(9073);Perl_croak(aTHX_ "panic: pad_alloc, %p!=%p",
		   AvARRAY(PL_comppad), PL_curpad);/*16*/}
    SensorCall(9075);if (PL_pad_reset_pending)
	pad_reset();
    SensorCall(9085);if (tmptype & SVs_PADMY) {
	/* For a my, simply push a null SV onto the end of PL_comppad. */
	SensorCall(9076);sv = *av_fetch(PL_comppad, AvFILLp(PL_comppad) + 1, TRUE);
	retval = AvFILLp(PL_comppad);
    }
    else {
	/* For a tmp, scan the pad from PL_padix upwards
	 * for a slot which has no name and no active value.
	 */
	SensorCall(9077);SV * const * const names = AvARRAY(PL_comppad_name);
        const SSize_t names_fill = AvFILLp(PL_comppad_name);
	SensorCall(9083);for (;;) {
	    /*
	     * "foreach" index vars temporarily become aliases to non-"my"
	     * values.  Thus we must skip, not just pad values that are
	     * marked as current pad values, but also those with names.
	     */
	    /* HVDS why copy to sv here? we don't seem to use it */
	    SensorCall(9078);if (++PL_padix <= names_fill &&
		   (sv = names[PL_padix]) && sv != &PL_sv_undef)
		{/*17*/SensorCall(9079);continue;/*18*/}
	    SensorCall(9080);sv = *av_fetch(PL_comppad, PL_padix, TRUE);
	    SensorCall(9082);if (!(SvFLAGS(sv) & (SVs_PADTMP | SVs_PADMY)) &&
		!IS_PADGV(sv) && !IS_PADCONST(sv))
		{/*19*/SensorCall(9081);break;/*20*/}
	}
	SensorCall(9084);retval = PL_padix;
    }
    SvFLAGS(sv) |= tmptype;
    PL_curpad = AvARRAY(PL_comppad);

    DEBUG_X(PerlIO_printf(Perl_debug_log,
	  "Pad 0x%"UVxf"[0x%"UVxf"] alloc:   %ld for %s\n",
	  PTR2UV(PL_comppad), PTR2UV(PL_curpad), (long) retval,
	  PL_op_name[optype]));
#ifdef DEBUG_LEAKING_SCALARS
    sv->sv_debug_optype = optype;
    sv->sv_debug_inpad = 1;
#endif
    {PADOFFSET  ReplaceReturn2156 = (PADOFFSET)retval; SensorCall(9086); return ReplaceReturn2156;}
}

/*
=for apidoc Am|PADOFFSET|pad_add_anon|CV *func|I32 optype

Allocates a place in the currently-compiling pad (via L</pad_alloc>)
for an anonymous function that is lexically scoped inside the
currently-compiling function.
The function I<func> is linked into the pad, and its C<CvOUTSIDE> link
to the outer scope is weakened to avoid a reference loop.

I<optype> should be an opcode indicating the type of operation that the
pad entry is to support.  This doesn't affect operational semantics,
but is used for debugging.

=cut
*/

PADOFFSET
Perl_pad_add_anon(pTHX_ CV* func, I32 optype)
{
SensorCall(9087);    dVAR;
    PADOFFSET ix;
    SV* const name = newSV_type(SVt_PVNV);

    PERL_ARGS_ASSERT_PAD_ADD_ANON;

    pad_peg("add_anon");
    sv_setpvs(name, "&");
    /* These two aren't used; just make sure they're not equal to
     * PERL_PADSEQ_INTRO */
    COP_SEQ_RANGE_LOW_set(name, 0);
    COP_SEQ_RANGE_HIGH_set(name, 0);
    ix = pad_alloc(optype, SVs_PADMY);
    av_store(PL_comppad_name, ix, name);
    /* XXX DAPM use PL_curpad[] ? */
    SensorCall(9089);if (SvTYPE(func) == SVt_PVCV || !CvOUTSIDE(func))
	av_store(PL_comppad, ix, (SV*)func);
    else {
	SensorCall(9088);SV *rv = newRV_inc((SV *)func);
	sv_rvweaken(rv);
	assert (SvTYPE(func) == SVt_PVFM);
	av_store(PL_comppad, ix, rv);
    }
    SvPADMY_on((SV*)func);

    /* to avoid ref loops, we never have parent + child referencing each
     * other simultaneously */
    SensorCall(9090);if (CvOUTSIDE(func) && SvTYPE(func) == SVt_PVCV) {
	assert(!CvWEAKOUTSIDE(func));
	CvWEAKOUTSIDE_on(func);
	SvREFCNT_dec(CvOUTSIDE(func));
    }
    {PADOFFSET  ReplaceReturn2155 = ix; SensorCall(9091); return ReplaceReturn2155;}
}

/*
=for apidoc pad_check_dup

Check for duplicate declarations: report any of:

     * a my in the current scope with the same name;
     * an our (anywhere in the pad) with the same name and the
       same stash as C<ourstash>

C<is_our> indicates that the name to check is an 'our' declaration.

=cut
*/

STATIC void
S_pad_check_dup(pTHX_ SV *name, U32 flags, const HV *ourstash)
{
SensorCall(9092);    dVAR;
    SV		**svp;
    PADOFFSET	top, off;
    const U32	is_our = flags & padadd_OUR;

    PERL_ARGS_ASSERT_PAD_CHECK_DUP;

    ASSERT_CURPAD_ACTIVE("pad_check_dup");

    assert((flags & ~padadd_OUR) == 0);

    SensorCall(9094);if (AvFILLp(PL_comppad_name) < 0 || !ckWARN(WARN_MISC))
	{/*57*/SensorCall(9093);return;/*58*/} /* nothing to check */

    SensorCall(9095);svp = AvARRAY(PL_comppad_name);
    top = AvFILLp(PL_comppad_name);
    /* check the current scope */
    /* XXX DAPM - why the (I32) cast - shouldn't we ensure they're the same
     * type ? */
    SensorCall(9102);for (off = top; (I32)off > PL_comppad_name_floor; off--) {
	SensorCall(9096);SV * const sv = svp[off];
	SensorCall(9101);if (sv
	    && sv != &PL_sv_undef
	    && !SvFAKE(sv)
	    && (   COP_SEQ_RANGE_LOW(sv)  == PERL_PADSEQ_INTRO
		|| COP_SEQ_RANGE_HIGH(sv) == PERL_PADSEQ_INTRO)
	    && sv_eq(name, sv))
	{
	    SensorCall(9097);if (is_our && (SvPAD_OUR(sv)))
		{/*59*/SensorCall(9098);break;/*60*/} /* "our" masking "our" */
	    SensorCall(9099);Perl_warner(aTHX_ packWARN(WARN_MISC),
		"\"%s\" variable %"SVf" masks earlier declaration in same %s",
		(is_our ? "our" : PL_parser->in_my == KEY_my ? "my" : "state"),
		sv,
		(COP_SEQ_RANGE_HIGH(sv) == PERL_PADSEQ_INTRO
		    ? "scope" : "statement"));
	    --off;
	    SensorCall(9100);break;
	}
    }
    /* check the rest of the pad */
    SensorCall(9111);if (is_our) {
	SensorCall(9103);while (off > 0) {
	    SensorCall(9104);SV * const sv = svp[off];
	    SensorCall(9109);if (sv
		&& sv != &PL_sv_undef
		&& !SvFAKE(sv)
		&& (   COP_SEQ_RANGE_LOW(sv)  == PERL_PADSEQ_INTRO
		    || COP_SEQ_RANGE_HIGH(sv) == PERL_PADSEQ_INTRO)
		&& SvOURSTASH(sv) == ourstash
		&& sv_eq(name, sv))
	    {
		SensorCall(9105);Perl_warner(aTHX_ packWARN(WARN_MISC),
		    "\"our\" variable %"SVf" redeclared", sv);
		SensorCall(9107);if ((I32)off <= PL_comppad_name_floor)
		    {/*61*/SensorCall(9106);Perl_warner(aTHX_ packWARN(WARN_MISC),
			"\t(Did you mean \"local\" instead of \"our\"?)\n");/*62*/}
		SensorCall(9108);break;
	    }
	    SensorCall(9110);--off;
	}
    }
SensorCall(9112);}


/*
=for apidoc Am|PADOFFSET|pad_findmy_pvn|const char *namepv|STRLEN namelen|U32 flags

Given the name of a lexical variable, find its position in the
currently-compiling pad.
I<namepv>/I<namelen> specify the variable's name, including leading sigil.
I<flags> is reserved and must be zero.
If it is not in the current pad but appears in the pad of any lexically
enclosing scope, then a pseudo-entry for it is added in the current pad.
Returns the offset in the current pad,
or C<NOT_IN_PAD> if no such lexical is in scope.

=cut
*/

PADOFFSET
Perl_pad_findmy_pvn(pTHX_ const char *namepv, STRLEN namelen, U32 flags)
{
SensorCall(9113);    dVAR;
    SV *out_sv;
    int out_flags;
    I32 offset;
    const AV *nameav;
    SV **name_svp;

    PERL_ARGS_ASSERT_PAD_FINDMY_PVN;

    pad_peg("pad_findmy_pvn");

    SensorCall(9115);if (flags & ~padadd_UTF8_NAME)
	{/*21*/SensorCall(9114);Perl_croak(aTHX_ "panic: pad_findmy_pvn illegal flag bits 0x%" UVxf,
		   (UV)flags);/*22*/}

    SensorCall(9119);if (flags & padadd_UTF8_NAME) {
        bool is_utf8 = TRUE;
        SensorCall(9116);namepv = (const char*)bytes_from_utf8((U8*)namepv, &namelen, &is_utf8);

        SensorCall(9118);if (is_utf8)
            flags |= padadd_UTF8_NAME;
        else {
            SensorCall(9117);flags &= ~padadd_UTF8_NAME;
            SAVEFREEPV(namepv);
        }
    }

    SensorCall(9120);offset = pad_findlex(namepv, namelen, flags,
                PL_compcv, PL_cop_seqmax, 1, NULL, &out_sv, &out_flags);
    SensorCall(9122);if ((PADOFFSET)offset != NOT_IN_PAD) 
	{/*23*/{PADOFFSET  ReplaceReturn2154 = offset; SensorCall(9121); return ReplaceReturn2154;}/*24*/}

    /* look for an our that's being introduced; this allows
     *    our $foo = 0 unless defined $foo;
     * to not give a warning. (Yes, this is a hack) */

    SensorCall(9123);nameav = MUTABLE_AV(AvARRAY(CvPADLIST(PL_compcv))[0]);
    name_svp = AvARRAY(nameav);
    SensorCall(9127);for (offset = AvFILLp(nameav); offset > 0; offset--) {
        SensorCall(9124);const SV * const namesv = name_svp[offset];
	SensorCall(9126);if (namesv && namesv != &PL_sv_undef
	    && !SvFAKE(namesv)
	    && (SvPAD_OUR(namesv))
	    && SvCUR(namesv) == namelen
            && sv_eq_pvn_flags(aTHX_ namesv, namepv, namelen,
                                flags & padadd_UTF8_NAME ? SVf_UTF8 : 0 )
	    && COP_SEQ_RANGE_LOW(namesv) == PERL_PADSEQ_INTRO
	)
	    {/*25*/{PADOFFSET  ReplaceReturn2153 = offset; SensorCall(9125); return ReplaceReturn2153;}/*26*/}
    }
    {PADOFFSET  ReplaceReturn2152 = NOT_IN_PAD; SensorCall(9128); return ReplaceReturn2152;}
}

/*
=for apidoc Am|PADOFFSET|pad_findmy_pv|const char *name|U32 flags

Exactly like L</pad_findmy_pvn>, but takes a nul-terminated string
instead of a string/length pair.

=cut
*/

PADOFFSET
Perl_pad_findmy_pv(pTHX_ const char *name, U32 flags)
{
    PERL_ARGS_ASSERT_PAD_FINDMY_PV;
    {PADOFFSET  ReplaceReturn2151 = pad_findmy_pvn(name, strlen(name), flags); SensorCall(9129); return ReplaceReturn2151;}
}

/*
=for apidoc Am|PADOFFSET|pad_findmy_sv|SV *name|U32 flags

Exactly like L</pad_findmy_pvn>, but takes the name string in the form
of an SV instead of a string/length pair.

=cut
*/

PADOFFSET
Perl_pad_findmy_sv(pTHX_ SV *name, U32 flags)
{
    SensorCall(9130);char *namepv;
    STRLEN namelen;
    PERL_ARGS_ASSERT_PAD_FINDMY_SV;
    namepv = SvPV(name, namelen);
    SensorCall(9131);if (SvUTF8(name))
        flags |= padadd_UTF8_NAME;
    {PADOFFSET  ReplaceReturn2150 = pad_findmy_pvn(namepv, namelen, flags); SensorCall(9132); return ReplaceReturn2150;}
}

/*
=for apidoc Amp|PADOFFSET|find_rundefsvoffset

Find the position of the lexical C<$_> in the pad of the
currently-executing function.  Returns the offset in the current pad,
or C<NOT_IN_PAD> if there is no lexical C<$_> in scope (in which case
the global one should be used instead).
L</find_rundefsv> is likely to be more convenient.

=cut
*/

PADOFFSET
Perl_find_rundefsvoffset(pTHX)
{
SensorCall(9133);    dVAR;
    SV *out_sv;
    int out_flags;
    {PADOFFSET  ReplaceReturn2149 = pad_findlex("$_", 2, 0, find_runcv(NULL), PL_curcop->cop_seq, 1,
	    NULL, &out_sv, &out_flags); SensorCall(9134); return ReplaceReturn2149;}
}

/*
=for apidoc Am|SV *|find_rundefsv

Find and return the variable that is named C<$_> in the lexical scope
of the currently-executing function.  This may be a lexical C<$_>,
or will otherwise be the global one.

=cut
*/

SV *
Perl_find_rundefsv(pTHX)
{
    SensorCall(9135);SV *namesv;
    int flags;
    PADOFFSET po;

    po = pad_findlex("$_", 2, 0, find_runcv(NULL), PL_curcop->cop_seq, 1,
	    NULL, &namesv, &flags);

    SensorCall(9136);if (po == NOT_IN_PAD || SvPAD_OUR(namesv))
	return DEFSV;

    {SV * ReplaceReturn2148 = PAD_SVl(po); SensorCall(9137); return ReplaceReturn2148;}
}

SV *
Perl_find_rundefsv2(pTHX_ CV *cv, U32 seq)
{
    SensorCall(9138);SV *namesv;
    int flags;
    PADOFFSET po;

    PERL_ARGS_ASSERT_FIND_RUNDEFSV2;

    po = pad_findlex("$_", 2, 0, cv, seq, 1,
	    NULL, &namesv, &flags);

    SensorCall(9139);if (po == NOT_IN_PAD || SvPAD_OUR(namesv))
	return DEFSV;

    {SV * ReplaceReturn2147 = AvARRAY((PAD*) (AvARRAY(CvPADLIST(cv))[CvDEPTH(cv)]))[po]; SensorCall(9140); return ReplaceReturn2147;}
}

/*
=for apidoc m|PADOFFSET|pad_findlex|const char *namepv|STRLEN namelen|U32 flags|const CV* cv|U32 seq|int warn|SV** out_capture|SV** out_name_sv|int *out_flags

Find a named lexical anywhere in a chain of nested pads. Add fake entries
in the inner pads if it's found in an outer one.

Returns the offset in the bottom pad of the lex or the fake lex.
cv is the CV in which to start the search, and seq is the current cop_seq
to match against. If warn is true, print appropriate warnings.  The out_*
vars return values, and so are pointers to where the returned values
should be stored. out_capture, if non-null, requests that the innermost
instance of the lexical is captured; out_name_sv is set to the innermost
matched namesv or fake namesv; out_flags returns the flags normally
associated with the IVX field of a fake namesv.

Note that pad_findlex() is recursive; it recurses up the chain of CVs,
then comes back down, adding fake entries as it goes. It has to be this way
because fake namesvs in anon protoypes have to store in xlow the index into
the parent pad.

=cut
*/

/* the CV has finished being compiled. This is not a sufficient test for
 * all CVs (eg XSUBs), but suffices for the CVs found in a lexical chain */
#define CvCOMPILED(cv)	CvROOT(cv)

/* the CV does late binding of its lexicals */
#define CvLATE(cv) (CvANON(cv) || SvTYPE(cv) == SVt_PVFM)


STATIC PADOFFSET
S_pad_findlex(pTHX_ const char *namepv, STRLEN namelen, U32 flags, const CV* cv, U32 seq,
	int warn, SV** out_capture, SV** out_name_sv, int *out_flags)
{
SensorCall(9141);    dVAR;
    I32 offset, new_offset;
    SV *new_capture;
    SV **new_capturep;
    const AV * const padlist = CvPADLIST(cv);

    PERL_ARGS_ASSERT_PAD_FINDLEX;

    SensorCall(9143);if (flags & ~padadd_UTF8_NAME)
	{/*63*/SensorCall(9142);Perl_croak(aTHX_ "panic: pad_findlex illegal flag bits 0x%" UVxf,
		   (UV)flags);/*64*/}

    SensorCall(9144);*out_flags = 0;

    DEBUG_Xv(PerlIO_printf(Perl_debug_log,
	"Pad findlex cv=0x%"UVxf" searching \"%.*s\" seq=%d%s\n",
			   PTR2UV(cv), (int)namelen, namepv, (int)seq,
	out_capture ? " capturing" : "" ));

    /* first, search this pad */

    SensorCall(9183);if (padlist) { /* not an undef CV */
	SensorCall(9145);I32 fake_offset = 0;
        const AV * const nameav = MUTABLE_AV(AvARRAY(padlist)[0]);
	SV * const * const name_svp = AvARRAY(nameav);

	SensorCall(9158);for (offset = AvFILLp(nameav); offset > 0; offset--) {
            SensorCall(9146);const SV * const namesv = name_svp[offset];
	    SensorCall(9157);if (namesv && namesv != &PL_sv_undef
		    && SvCUR(namesv) == namelen
                    && sv_eq_pvn_flags(aTHX_ namesv, namepv, namelen,
                                    flags & padadd_UTF8_NAME ? SVf_UTF8 : 0))
	    {
		SensorCall(9147);if (SvFAKE(namesv)) {
		    SensorCall(9148);fake_offset = offset; /* in case we don't find a real one */
		    SensorCall(9149);continue;
		}
		/* is seq within the range _LOW to _HIGH ?
		 * This is complicated by the fact that PL_cop_seqmax
		 * may have wrapped around at some point */
		SensorCall(9151);if (COP_SEQ_RANGE_LOW(namesv) == PERL_PADSEQ_INTRO)
		    {/*65*/SensorCall(9150);continue;/*66*/} /* not yet introduced */

		SensorCall(9156);if (COP_SEQ_RANGE_HIGH(namesv) == PERL_PADSEQ_INTRO) {
		    /* in compiling scope */
		    SensorCall(9152);if (
			(seq >  COP_SEQ_RANGE_LOW(namesv))
			? (seq - COP_SEQ_RANGE_LOW(namesv) < (U32_MAX >> 1))
			: (COP_SEQ_RANGE_LOW(namesv) - seq > (U32_MAX >> 1))
		    )
		       {/*67*/SensorCall(9153);break;/*68*/}
		}
		else {/*69*/SensorCall(9154);if (
		    (COP_SEQ_RANGE_LOW(namesv) > COP_SEQ_RANGE_HIGH(namesv))
		    ?
			(  seq >  COP_SEQ_RANGE_LOW(namesv)
			|| seq <= COP_SEQ_RANGE_HIGH(namesv))

		    :    (  seq >  COP_SEQ_RANGE_LOW(namesv)
			 && seq <= COP_SEQ_RANGE_HIGH(namesv))
		)
		{/*71*/SensorCall(9155);break;/*72*/}/*70*/}
	    }
	}

	SensorCall(9182);if (offset > 0 || fake_offset > 0 ) { /* a match! */
	    SensorCall(9159);if (offset > 0) { /* not fake */
		SensorCall(9160);fake_offset = 0;
		*out_name_sv = name_svp[offset]; /* return the namesv */

		/* set PAD_FAKELEX_MULTI if this lex can have multiple
		 * instances. For now, we just test !CvUNIQUE(cv), but
		 * ideally, we should detect my's declared within loops
		 * etc - this would allow a wider range of 'not stayed
		 * shared' warnings. We also treated already-compiled
		 * lexes as not multi as viewed from evals. */

		*out_flags = CvANON(cv) ?
			PAD_FAKELEX_ANON :
			    (!CvUNIQUE(cv) && ! CvCOMPILED(cv))
				? PAD_FAKELEX_MULTI : 0;

		DEBUG_Xv(PerlIO_printf(Perl_debug_log,
		    "Pad findlex cv=0x%"UVxf" matched: offset=%ld (%lu,%lu)\n",
		    PTR2UV(cv), (long)offset,
		    (unsigned long)COP_SEQ_RANGE_LOW(*out_name_sv),
		    (unsigned long)COP_SEQ_RANGE_HIGH(*out_name_sv)));
	    }
	    else { /* fake match */
		SensorCall(9161);offset = fake_offset;
		*out_name_sv = name_svp[offset]; /* return the namesv */
		*out_flags = PARENT_FAKELEX_FLAGS(*out_name_sv);
		DEBUG_Xv(PerlIO_printf(Perl_debug_log,
		    "Pad findlex cv=0x%"UVxf" matched: offset=%ld flags=0x%lx index=%lu\n",
		    PTR2UV(cv), (long)offset, (unsigned long)*out_flags,
		    (unsigned long) PARENT_PAD_INDEX(*out_name_sv) 
		));
	    }

	    /* return the lex? */

	    SensorCall(9180);if (out_capture) {

		/* our ? */
		SensorCall(9162);if (SvPAD_OUR(*out_name_sv)) {
		    SensorCall(9163);*out_capture = NULL;
		    {PADOFFSET  ReplaceReturn2146 = offset; SensorCall(9164); return ReplaceReturn2146;}
		}

		/* trying to capture from an anon prototype? */
		SensorCall(9177);if (CvCOMPILED(cv)
			? CvANON(cv) && CvCLONE(cv) && !CvCLONED(cv)
			: *out_flags & PAD_FAKELEX_ANON)
		{
		    SensorCall(9165);if (warn)
			{/*73*/SensorCall(9166);Perl_ck_warner(aTHX_ packWARN(WARN_CLOSURE),
				       "Variable \"%"SVf"\" is not available",
                                       newSVpvn_flags(namepv, namelen,
                                           SVs_TEMP |
                                           (flags & padadd_UTF8_NAME ? SVf_UTF8 : 0)));/*74*/}

		    SensorCall(9167);*out_capture = NULL;
		}

		/* real value */
		else {
		    SensorCall(9168);int newwarn = warn;
		    SensorCall(9170);if (!CvCOMPILED(cv) && (*out_flags & PAD_FAKELEX_MULTI)
			 && !SvPAD_STATE(name_svp[offset])
			 && warn && ckWARN(WARN_CLOSURE)) {
			SensorCall(9169);newwarn = 0;
			Perl_warner(aTHX_ packWARN(WARN_CLOSURE),
			    "Variable \"%"SVf"\" will not stay shared",
                            newSVpvn_flags(namepv, namelen,
                                SVs_TEMP |
                                (flags & padadd_UTF8_NAME ? SVf_UTF8 : 0)));
		    }

		    SensorCall(9173);if (fake_offset && CvANON(cv)
			    && CvCLONE(cv) &&!CvCLONED(cv))
		    {
			SensorCall(9171);SV *n;
			/* not yet caught - look further up */
			DEBUG_Xv(PerlIO_printf(Perl_debug_log,
			    "Pad findlex cv=0x%"UVxf" chasing lex in outer pad\n",
			    PTR2UV(cv)));
			n = *out_name_sv;
			(void) pad_findlex(namepv, namelen, flags, CvOUTSIDE(cv),
			    CvOUTSIDE_SEQ(cv),
			    newwarn, out_capture, out_name_sv, out_flags);
			*out_name_sv = n;
			{PADOFFSET  ReplaceReturn2145 = offset; SensorCall(9172); return ReplaceReturn2145;}
		    }

		    SensorCall(9174);*out_capture = AvARRAY(MUTABLE_AV(AvARRAY(padlist)[
				    CvDEPTH(cv) ? CvDEPTH(cv) : 1]))[offset];
		    DEBUG_Xv(PerlIO_printf(Perl_debug_log,
			"Pad findlex cv=0x%"UVxf" found lex=0x%"UVxf"\n",
			PTR2UV(cv), PTR2UV(*out_capture)));

		    SensorCall(9176);if (SvPADSTALE(*out_capture)
			&& !SvPAD_STATE(name_svp[offset]))
		    {
			SensorCall(9175);Perl_ck_warner(aTHX_ packWARN(WARN_CLOSURE),
				       "Variable \"%"SVf"\" is not available",
                                       newSVpvn_flags(namepv, namelen,
                                           SVs_TEMP |
                                           (flags & padadd_UTF8_NAME ? SVf_UTF8 : 0)));
			*out_capture = NULL;
		    }
		}
		SensorCall(9179);if (!*out_capture) {
		    SensorCall(9178);if (namelen != 0 && *namepv == '@')
			*out_capture = sv_2mortal(MUTABLE_SV(newAV()));
		    else if (namelen != 0 && *namepv == '%')
			*out_capture = sv_2mortal(MUTABLE_SV(newHV()));
		    else
			*out_capture = sv_newmortal();
		}
	    }

	    {PADOFFSET  ReplaceReturn2144 = offset; SensorCall(9181); return ReplaceReturn2144;}
	}
    }

    /* it's not in this pad - try above */

    SensorCall(9184);if (!CvOUTSIDE(cv))
	return NOT_IN_PAD;

    /* out_capture non-null means caller wants us to capture lex; in
     * addition we capture ourselves unless it's an ANON/format */
    SensorCall(9185);new_capturep = out_capture ? out_capture :
		CvLATE(cv) ? NULL : &new_capture;

    offset = pad_findlex(namepv, namelen, flags, CvOUTSIDE(cv), CvOUTSIDE_SEQ(cv), 1,
		new_capturep, out_name_sv, out_flags);
    SensorCall(9186);if ((PADOFFSET)offset == NOT_IN_PAD)
	return NOT_IN_PAD;

    /* found in an outer CV. Add appropriate fake entry to this pad */

    /* don't add new fake entries (via eval) to CVs that we have already
     * finished compiling, or to undef CVs */
    SensorCall(9188);if (CvCOMPILED(cv) || !padlist)
	{/*75*/{PADOFFSET  ReplaceReturn2143 = 0; SensorCall(9187); return ReplaceReturn2143;}/*76*/} /* this dummy (and invalid) value isnt used by the caller */

    {
	/* This relies on sv_setsv_flags() upgrading the destination to the same
	   type as the source, independent of the flags set, and on it being
	   "good" and only copying flag bits and pointers that it understands.
	*/
	SensorCall(9189);SV *new_namesv = newSVsv(*out_name_sv);
	AV *  const ocomppad_name = PL_comppad_name;
	PAD * const ocomppad = PL_comppad;
	PL_comppad_name = MUTABLE_AV(AvARRAY(padlist)[0]);
	PL_comppad = MUTABLE_AV(AvARRAY(padlist)[1]);
	PL_curpad = AvARRAY(PL_comppad);

	new_offset
	    = pad_alloc_name(new_namesv,
			      (SvPAD_STATE(*out_name_sv) ? padadd_STATE : 0),
			      SvPAD_TYPED(*out_name_sv)
			      ? SvSTASH(*out_name_sv) : NULL,
			      SvOURSTASH(*out_name_sv)
			      );

	SvFAKE_on(new_namesv);
	DEBUG_Xv(PerlIO_printf(Perl_debug_log,
			       "Pad addname: %ld \"%.*s\" FAKE\n",
			       (long)new_offset,
			       (int) SvCUR(new_namesv), SvPVX(new_namesv)));
	PARENT_FAKELEX_FLAGS_set(new_namesv, *out_flags);

	PARENT_PAD_INDEX_set(new_namesv, 0);
	SensorCall(9191);if (SvPAD_OUR(new_namesv)) {
	    NOOP;   /* do nothing */
	}
	else {/*77*/SensorCall(9190);if (CvLATE(cv)) {
	    /* delayed creation - just note the offset within parent pad */
	    PARENT_PAD_INDEX_set(new_namesv, offset);
	    CvCLONE_on(cv);
	}
	else {
	    /* immediate creation - capture outer value right now */
	    av_store(PL_comppad, new_offset, SvREFCNT_inc(*new_capturep));
	    DEBUG_Xv(PerlIO_printf(Perl_debug_log,
		"Pad findlex cv=0x%"UVxf" saved captured sv 0x%"UVxf" at offset %ld\n",
		PTR2UV(cv), PTR2UV(*new_capturep), (long)new_offset));
	;/*78*/}}
	SensorCall(9192);*out_name_sv = new_namesv;
	*out_flags = PARENT_FAKELEX_FLAGS(new_namesv);

	PL_comppad_name = ocomppad_name;
	PL_comppad = ocomppad;
	PL_curpad = ocomppad ? AvARRAY(ocomppad) : NULL;
    }
    {PADOFFSET  ReplaceReturn2142 = new_offset; SensorCall(9193); return ReplaceReturn2142;}
}

#ifdef DEBUGGING

/*
=for apidoc Am|SV *|pad_sv|PADOFFSET po

Get the value at offset I<po> in the current (compiling or executing) pad.
Use macro PAD_SV instead of calling this function directly.

=cut
*/

SV *
Perl_pad_sv(pTHX_ PADOFFSET po)
{
    dVAR;
    ASSERT_CURPAD_ACTIVE("pad_sv");

    if (!po)
	Perl_croak(aTHX_ "panic: pad_sv po");
    DEBUG_X(PerlIO_printf(Perl_debug_log,
	"Pad 0x%"UVxf"[0x%"UVxf"] sv:      %ld sv=0x%"UVxf"\n",
	PTR2UV(PL_comppad), PTR2UV(PL_curpad), (long)po, PTR2UV(PL_curpad[po]))
    );
    return PL_curpad[po];
}

/*
=for apidoc Am|void|pad_setsv|PADOFFSET po|SV *sv

Set the value at offset I<po> in the current (compiling or executing) pad.
Use the macro PAD_SETSV() rather than calling this function directly.

=cut
*/

void
Perl_pad_setsv(pTHX_ PADOFFSET po, SV* sv)
{
    dVAR;

    PERL_ARGS_ASSERT_PAD_SETSV;

    ASSERT_CURPAD_ACTIVE("pad_setsv");

    DEBUG_X(PerlIO_printf(Perl_debug_log,
	"Pad 0x%"UVxf"[0x%"UVxf"] setsv:   %ld sv=0x%"UVxf"\n",
	PTR2UV(PL_comppad), PTR2UV(PL_curpad), (long)po, PTR2UV(sv))
    );
    PL_curpad[po] = sv;
}

#endif /* DEBUGGING */

/*
=for apidoc m|void|pad_block_start|int full

Update the pad compilation state variables on entry to a new block

=cut
*/

/* XXX DAPM perhaps:
 * 	- integrate this in general state-saving routine ???
 * 	- combine with the state-saving going on in pad_new ???
 * 	- introduce a new SAVE type that does all this in one go ?
 */

void
Perl_pad_block_start(pTHX_ int full)
{
SensorCall(9194);    dVAR;
    ASSERT_CURPAD_ACTIVE("pad_block_start");
    SAVEI32(PL_comppad_name_floor);
    PL_comppad_name_floor = AvFILLp(PL_comppad_name);
    SensorCall(9195);if (full)
	PL_comppad_name_fill = PL_comppad_name_floor;
    SensorCall(9196);if (PL_comppad_name_floor < 0)
	PL_comppad_name_floor = 0;
    SAVEI32(PL_min_intro_pending);
    SAVEI32(PL_max_intro_pending);
    PL_min_intro_pending = 0;
    SAVEI32(PL_comppad_name_fill);
    SAVEI32(PL_padix_floor);
    PL_padix_floor = PL_padix;
    PL_pad_reset_pending = FALSE;
}

/*
=for apidoc m|U32|intro_my

"Introduce" my variables to visible status.

=cut
*/

U32
Perl_intro_my(pTHX)
{
SensorCall(9197);    dVAR;
    SV **svp;
    I32 i;
    U32 seq;

    ASSERT_CURPAD_ACTIVE("intro_my");
    SensorCall(9198);if (! PL_min_intro_pending)
	return PL_cop_seqmax;

    SensorCall(9199);svp = AvARRAY(PL_comppad_name);
    SensorCall(9202);for (i = PL_min_intro_pending; i <= PL_max_intro_pending; i++) {
	SensorCall(9200);SV * const sv = svp[i];

	SensorCall(9201);if (sv && sv != &PL_sv_undef && !SvFAKE(sv)
	    && COP_SEQ_RANGE_LOW(sv) == PERL_PADSEQ_INTRO)
	{
	    COP_SEQ_RANGE_HIGH_set(sv, PERL_PADSEQ_INTRO); /* Don't know scope end yet. */
	    COP_SEQ_RANGE_LOW_set(sv, PL_cop_seqmax);
	    DEBUG_Xv(PerlIO_printf(Perl_debug_log,
		"Pad intromy: %ld \"%s\", (%lu,%lu)\n",
		(long)i, SvPVX_const(sv),
		(unsigned long)COP_SEQ_RANGE_LOW(sv),
		(unsigned long)COP_SEQ_RANGE_HIGH(sv))
	    );
	}
    }
    SensorCall(9203);seq = PL_cop_seqmax;
    PL_cop_seqmax++;
    SensorCall(9204);if (PL_cop_seqmax == PERL_PADSEQ_INTRO) /* not a legal value */
	PL_cop_seqmax++;
    PL_min_intro_pending = 0;
    PL_comppad_name_fill = PL_max_intro_pending; /* Needn't search higher */
    DEBUG_Xv(PerlIO_printf(Perl_debug_log,
		"Pad intromy: seq -> %ld\n", (long)(PL_cop_seqmax)));

    {U32  ReplaceReturn2141 = seq; SensorCall(9205); return ReplaceReturn2141;}
}

/*
=for apidoc m|void|pad_leavemy

Cleanup at end of scope during compilation: set the max seq number for
lexicals in this scope and warn of any lexicals that never got introduced.

=cut
*/

void
Perl_pad_leavemy(pTHX)
{
SensorCall(9206);    dVAR;
    I32 off;
    SV * const * const svp = AvARRAY(PL_comppad_name);

    PL_pad_reset_pending = FALSE;

    ASSERT_CURPAD_ACTIVE("pad_leavemy");
    SensorCall(9211);if (PL_min_intro_pending && PL_comppad_name_fill < PL_min_intro_pending) {
	SensorCall(9207);for (off = PL_max_intro_pending; off >= PL_min_intro_pending; off--) {
	    SensorCall(9208);const SV * const sv = svp[off];
	    SensorCall(9210);if (sv && sv != &PL_sv_undef && !SvFAKE(sv))
		{/*35*/SensorCall(9209);Perl_ck_warner_d(aTHX_ packWARN(WARN_INTERNAL),
				 "%"SVf" never introduced",
				 SVfARG(sv));/*36*/}
	}
    }
    /* "Deintroduce" my variables that are leaving with this scope. */
    SensorCall(9214);for (off = AvFILLp(PL_comppad_name); off > PL_comppad_name_fill; off--) {
	SensorCall(9212);const SV * const sv = svp[off];
	SensorCall(9213);if (sv && sv != &PL_sv_undef && !SvFAKE(sv)
	    && COP_SEQ_RANGE_HIGH(sv) == PERL_PADSEQ_INTRO)
	{
	    COP_SEQ_RANGE_HIGH_set(sv, PL_cop_seqmax);
	    DEBUG_Xv(PerlIO_printf(Perl_debug_log,
		"Pad leavemy: %ld \"%s\", (%lu,%lu)\n",
		(long)off, SvPVX_const(sv),
		(unsigned long)COP_SEQ_RANGE_LOW(sv),
		(unsigned long)COP_SEQ_RANGE_HIGH(sv))
	    );
	}
    }
    PL_cop_seqmax++;
    SensorCall(9215);if (PL_cop_seqmax == PERL_PADSEQ_INTRO) /* not a legal value */
	PL_cop_seqmax++;
    DEBUG_Xv(PerlIO_printf(Perl_debug_log,
	    "Pad leavemy: seq = %ld\n", (long)PL_cop_seqmax));
SensorCall(9216);}

/*
=for apidoc m|void|pad_swipe|PADOFFSET po|bool refadjust

Abandon the tmp in the current pad at offset po and replace with a
new one.

=cut
*/

void
Perl_pad_swipe(pTHX_ PADOFFSET po, bool refadjust)
{
SensorCall(9217);    dVAR;
    ASSERT_CURPAD_LEGAL("pad_swipe");
    SensorCall(9219);if (!PL_curpad)
	{/*39*/SensorCall(9218);return;/*40*/}
    SensorCall(9221);if (AvARRAY(PL_comppad) != PL_curpad)
	{/*41*/SensorCall(9220);Perl_croak(aTHX_ "panic: pad_swipe curpad, %p!=%p",
		   AvARRAY(PL_comppad), PL_curpad);/*42*/}
    SensorCall(9223);if (!po || ((SSize_t)po) > AvFILLp(PL_comppad))
	{/*43*/SensorCall(9222);Perl_croak(aTHX_ "panic: pad_swipe po=%ld, fill=%ld",
		   (long)po, (long)AvFILLp(PL_comppad));/*44*/}

    DEBUG_X(PerlIO_printf(Perl_debug_log,
		"Pad 0x%"UVxf"[0x%"UVxf"] swipe:   %ld\n",
		PTR2UV(PL_comppad), PTR2UV(PL_curpad), (long)po));

    SensorCall(9224);if (PL_curpad[po])
	SvPADTMP_off(PL_curpad[po]);
    SensorCall(9225);if (refadjust)
	SvREFCNT_dec(PL_curpad[po]);


    /* if pad tmps aren't shared between ops, then there's no need to
     * create a new tmp when an existing op is freed */
#ifdef USE_BROKEN_PAD_RESET
    PL_curpad[po] = newSV(0);
    SvPADTMP_on(PL_curpad[po]);
#else
    PL_curpad[po] = &PL_sv_undef;
#endif
    SensorCall(9226);if ((I32)po < PL_padix)
	PL_padix = po - 1;
SensorCall(9227);}

/*
=for apidoc m|void|pad_reset

Mark all the current temporaries for reuse

=cut
*/

/* XXX pad_reset() is currently disabled because it results in serious bugs.
 * It causes pad temp TARGs to be shared between OPs. Since TARGs are pushed
 * on the stack by OPs that use them, there are several ways to get an alias
 * to  a shared TARG.  Such an alias will change randomly and unpredictably.
 * We avoid doing this until we can think of a Better Way.
 * GSAR 97-10-29 */
static void
S_pad_reset(pTHX)
{
SensorCall(9228);    dVAR;
#ifdef USE_BROKEN_PAD_RESET
    if (AvARRAY(PL_comppad) != PL_curpad)
	Perl_croak(aTHX_ "panic: pad_reset curpad, %p!=%p",
		   AvARRAY(PL_comppad), PL_curpad);

    DEBUG_X(PerlIO_printf(Perl_debug_log,
	    "Pad 0x%"UVxf"[0x%"UVxf"] reset:     padix %ld -> %ld",
	    PTR2UV(PL_comppad), PTR2UV(PL_curpad),
		(long)PL_padix, (long)PL_padix_floor
	    )
    );

    if (!PL_tainting) {	/* Can't mix tainted and non-tainted temporaries. */
        register I32 po;
	for (po = AvMAX(PL_comppad); po > PL_padix_floor; po--) {
	    if (PL_curpad[po] && !SvIMMORTAL(PL_curpad[po]))
		SvPADTMP_off(PL_curpad[po]);
	}
	PL_padix = PL_padix_floor;
    }
#endif
    PL_pad_reset_pending = FALSE;
}

/*
=for apidoc Amx|void|pad_tidy|padtidy_type type

Tidy up a pad at the end of compilation of the code to which it belongs.
Jobs performed here are: remove most stuff from the pads of anonsub
prototypes; give it a @_; mark temporaries as such.  I<type> indicates
the kind of subroutine:

    padtidy_SUB        ordinary subroutine
    padtidy_SUBCLONE   prototype for lexical closure
    padtidy_FORMAT     format

=cut
*/

/* XXX DAPM surely most of this stuff should be done properly
 * at the right time beforehand, rather than going around afterwards
 * cleaning up our mistakes ???
 */

void
Perl_pad_tidy(pTHX_ padtidy_type type)
{
SensorCall(9229);    dVAR;

    ASSERT_CURPAD_ACTIVE("pad_tidy");

    /* If this CV has had any 'eval-capable' ops planted in it
     * (ie it contains eval '...', //ee, /$var/ or /(?{..})/), Then any
     * anon prototypes in the chain of CVs should be marked as cloneable,
     * so that for example the eval's CV in C<< sub { eval '$x' } >> gets
     * the right CvOUTSIDE.
     * If running with -d, *any* sub may potentially have an eval
     * executed within it.
     */

    SensorCall(9235);if (PL_cv_has_eval || PL_perldb) {
        SensorCall(9230);const CV *cv;
	SensorCall(9234);for (cv = PL_compcv ;cv; cv = CvOUTSIDE(cv)) {
	    SensorCall(9231);if (cv != PL_compcv && CvCOMPILED(cv))
		{/*45*/SensorCall(9232);break;/*46*/} /* no need to mark already-compiled code */
	    SensorCall(9233);if (CvANON(cv)) {
		DEBUG_Xv(PerlIO_printf(Perl_debug_log,
		    "Pad clone on cv=0x%"UVxf"\n", PTR2UV(cv)));
		CvCLONE_on(cv);
	    }
	}
    }

    /* extend curpad to match namepad */
    SensorCall(9236);if (AvFILLp(PL_comppad_name) < AvFILLp(PL_comppad))
	av_store(PL_comppad_name, AvFILLp(PL_comppad), NULL);

    SensorCall(9245);if (type == padtidy_SUBCLONE) {
	SensorCall(9237);SV * const * const namep = AvARRAY(PL_comppad_name);
	PADOFFSET ix;

	SensorCall(9242);for (ix = AvFILLp(PL_comppad); ix > 0; ix--) {
	    SensorCall(9238);SV *namesv;

	    SensorCall(9240);if (SvIMMORTAL(PL_curpad[ix]) || IS_PADGV(PL_curpad[ix]) || IS_PADCONST(PL_curpad[ix]))
		{/*47*/SensorCall(9239);continue;/*48*/}
	    /*
	     * The only things that a clonable function needs in its
	     * pad are anonymous subs.
	     * The rest are created anew during cloning.
	     */
	    SensorCall(9241);if (!((namesv = namep[ix]) != NULL &&
		  namesv != &PL_sv_undef &&
		   *SvPVX_const(namesv) == '&'))
	    {
		SvREFCNT_dec(PL_curpad[ix]);
		PL_curpad[ix] = NULL;
	    }
	}
    }
    else {/*49*/SensorCall(9243);if (type == padtidy_SUB) {
	/* XXX DAPM this same bit of code keeps appearing !!! Rationalise? */
	SensorCall(9244);AV * const av = newAV();			/* Will be @_ */
	av_store(PL_comppad, 0, MUTABLE_SV(av));
	AvREIFY_only(av);
    ;/*50*/}}

    SensorCall(9252);if (type == padtidy_SUB || type == padtidy_FORMAT) {
	SensorCall(9246);SV * const * const namep = AvARRAY(PL_comppad_name);
	PADOFFSET ix;
	SensorCall(9251);for (ix = AvFILLp(PL_comppad); ix > 0; ix--) {
	    SensorCall(9247);if (SvIMMORTAL(PL_curpad[ix]) || IS_PADGV(PL_curpad[ix]) || IS_PADCONST(PL_curpad[ix]))
		{/*51*/SensorCall(9248);continue;/*52*/}
	    SensorCall(9250);if (!SvPADMY(PL_curpad[ix])) {
		SvPADTMP_on(PL_curpad[ix]);
	    } else {/*53*/SensorCall(9249);if (!SvFAKE(namep[ix])) {
		/* This is a work around for how the current implementation of
		   ?{ } blocks in regexps interacts with lexicals.

		   One of our lexicals.
		   Can't do this on all lexicals, otherwise sub baz() won't
		   compile in

		   my $foo;

		   sub bar { ++$foo; }

		   sub baz { ++$foo; }

		   because completion of compiling &bar calling pad_tidy()
		   would cause (top level) $foo to be marked as stale, and
		   "no longer available".  */
		SvPADSTALE_on(PL_curpad[ix]);
	    ;/*54*/}}
	}
    }
    PL_curpad = AvARRAY(PL_comppad);
}

/*
=for apidoc m|void|pad_free|PADOFFSET po

Free the SV at offset po in the current pad.

=cut
*/

/* XXX DAPM integrate with pad_swipe ???? */
void
Perl_pad_free(pTHX_ PADOFFSET po)
{
SensorCall(9253);    dVAR;
    ASSERT_CURPAD_LEGAL("pad_free");
    SensorCall(9255);if (!PL_curpad)
	{/*29*/SensorCall(9254);return;/*30*/}
    SensorCall(9257);if (AvARRAY(PL_comppad) != PL_curpad)
	{/*31*/SensorCall(9256);Perl_croak(aTHX_ "panic: pad_free curpad, %p!=%p",
		   AvARRAY(PL_comppad), PL_curpad);/*32*/}
    SensorCall(9259);if (!po)
	{/*33*/SensorCall(9258);Perl_croak(aTHX_ "panic: pad_free po");/*34*/}

    DEBUG_X(PerlIO_printf(Perl_debug_log,
	    "Pad 0x%"UVxf"[0x%"UVxf"] free:    %ld\n",
	    PTR2UV(PL_comppad), PTR2UV(PL_curpad), (long)po)
    );

    SensorCall(9260);if (PL_curpad[po] && PL_curpad[po] != &PL_sv_undef) {
	SvFLAGS(PL_curpad[po]) &= ~SVs_PADTMP; /* also clears SVs_PADSTALE */
    }
    SensorCall(9261);if ((I32)po < PL_padix)
	PL_padix = po - 1;
SensorCall(9262);}

/*
=for apidoc m|void|do_dump_pad|I32 level|PerlIO *file|PADLIST *padlist|int full

Dump the contents of a padlist

=cut
*/

void
Perl_do_dump_pad(pTHX_ I32 level, PerlIO *file, PADLIST *padlist, int full)
{
SensorCall(9263);    dVAR;
    const AV *pad_name;
    const AV *pad;
    SV **pname;
    SV **ppad;
    I32 ix;

    PERL_ARGS_ASSERT_DO_DUMP_PAD;

    SensorCall(9265);if (!padlist) {
	SensorCall(9264);return;
    }
    SensorCall(9266);pad_name = MUTABLE_AV(*av_fetch(MUTABLE_AV(padlist), 0, FALSE));
    pad = MUTABLE_AV(*av_fetch(MUTABLE_AV(padlist), 1, FALSE));
    pname = AvARRAY(pad_name);
    ppad = AvARRAY(pad);
    Perl_dump_indent(aTHX_ level, file,
	    "PADNAME = 0x%"UVxf"(0x%"UVxf") PAD = 0x%"UVxf"(0x%"UVxf")\n",
	    PTR2UV(pad_name), PTR2UV(pname), PTR2UV(pad), PTR2UV(ppad)
    );

    SensorCall(9276);for (ix = 1; ix <= AvFILLp(pad_name); ix++) {
        SensorCall(9267);const SV *namesv = pname[ix];
	SensorCall(9269);if (namesv && namesv == &PL_sv_undef) {
	    SensorCall(9268);namesv = NULL;
	}
	SensorCall(9275);if (namesv) {
	    SensorCall(9270);if (SvFAKE(namesv))
		{/*7*/SensorCall(9271);Perl_dump_indent(aTHX_ level+1, file,
		    "%2d. 0x%"UVxf"<%lu> FAKE \"%s\" flags=0x%lx index=%lu\n",
		    (int) ix,
		    PTR2UV(ppad[ix]),
		    (unsigned long) (ppad[ix] ? SvREFCNT(ppad[ix]) : 0),
		    SvPVX_const(namesv),
		    (unsigned long)PARENT_FAKELEX_FLAGS(namesv),
		    (unsigned long)PARENT_PAD_INDEX(namesv)

		);/*8*/}
	    else
		{/*9*/SensorCall(9272);Perl_dump_indent(aTHX_ level+1, file,
		    "%2d. 0x%"UVxf"<%lu> (%lu,%lu) \"%s\"\n",
		    (int) ix,
		    PTR2UV(ppad[ix]),
		    (unsigned long) (ppad[ix] ? SvREFCNT(ppad[ix]) : 0),
		    (unsigned long)COP_SEQ_RANGE_LOW(namesv),
		    (unsigned long)COP_SEQ_RANGE_HIGH(namesv),
		    SvPVX_const(namesv)
		);/*10*/}
	}
	else {/*11*/SensorCall(9273);if (full) {
	    SensorCall(9274);Perl_dump_indent(aTHX_ level+1, file,
		"%2d. 0x%"UVxf"<%lu>\n",
		(int) ix,
		PTR2UV(ppad[ix]),
		(unsigned long) (ppad[ix] ? SvREFCNT(ppad[ix]) : 0)
	    );
	;/*12*/}}
    }
SensorCall(9277);}

#ifdef DEBUGGING

/*
=for apidoc m|void|cv_dump|CV *cv|const char *title

dump the contents of a CV

=cut
*/

STATIC void
S_cv_dump(pTHX_ const CV *cv, const char *title)
{
    dVAR;
    const CV * const outside = CvOUTSIDE(cv);
    AV* const padlist = CvPADLIST(cv);

    PERL_ARGS_ASSERT_CV_DUMP;

    PerlIO_printf(Perl_debug_log,
		  "  %s: CV=0x%"UVxf" (%s), OUTSIDE=0x%"UVxf" (%s)\n",
		  title,
		  PTR2UV(cv),
		  (CvANON(cv) ? "ANON"
		   : (SvTYPE(cv) == SVt_PVFM) ? "FORMAT"
		   : (cv == PL_main_cv) ? "MAIN"
		   : CvUNIQUE(cv) ? "UNIQUE"
		   : CvGV(cv) ? GvNAME(CvGV(cv)) : "UNDEFINED"),
		  PTR2UV(outside),
		  (!outside ? "null"
		   : CvANON(outside) ? "ANON"
		   : (outside == PL_main_cv) ? "MAIN"
		   : CvUNIQUE(outside) ? "UNIQUE"
		   : CvGV(outside) ? GvNAME(CvGV(outside)) : "UNDEFINED"));

    PerlIO_printf(Perl_debug_log,
		    "    PADLIST = 0x%"UVxf"\n", PTR2UV(padlist));
    do_dump_pad(1, Perl_debug_log, padlist, 1);
}

#endif /* DEBUGGING */

/*
=for apidoc Am|CV *|cv_clone|CV *proto

Clone a CV, making a lexical closure.  I<proto> supplies the prototype
of the function: its code, pad structure, and other attributes.
The prototype is combined with a capture of outer lexicals to which the
code refers, which are taken from the currently-executing instance of
the immediately surrounding code.

=cut
*/

CV *
Perl_cv_clone(pTHX_ CV *proto)
{
SensorCall(9278);    dVAR;
    I32 ix;
    AV* const protopadlist = CvPADLIST(proto);
    const AV *const protopad_name = (const AV *)*av_fetch(protopadlist, 0, FALSE);
    const AV *const protopad = (const AV *)*av_fetch(protopadlist, 1, FALSE);
    SV** const pname = AvARRAY(protopad_name);
    SV** const ppad = AvARRAY(protopad);
    const I32 fname = AvFILLp(protopad_name);
    const I32 fpad = AvFILLp(protopad);
    CV* cv;
    SV** outpad;
    CV* outside;
    long depth;

    PERL_ARGS_ASSERT_CV_CLONE;

    assert(!CvUNIQUE(proto));

    /* Since cloneable anon subs can be nested, CvOUTSIDE may point
     * to a prototype; we instead want the cloned parent who called us.
     * Note that in general for formats, CvOUTSIDE != find_runcv; formats
     * inside closures, however, only work if CvOUTSIDE == find_runcv.
     */

    outside = CvOUTSIDE(proto);
    SensorCall(9279);if (outside && CvCLONE(outside) && ! CvCLONED(outside))
	outside = find_runcv(NULL);
    SensorCall(9280);if (SvTYPE(proto) == SVt_PVFM
     && CvROOT(outside) != CvROOT(CvOUTSIDE(proto)))
	outside = CvOUTSIDE(proto);
    SensorCall(9281);depth = CvDEPTH(outside);
    assert(depth || SvTYPE(proto) == SVt_PVFM);
    SensorCall(9283);if (!depth)
	{/*1*/SensorCall(9282);depth = 1;/*2*/}
    assert(CvPADLIST(outside) || SvTYPE(proto) == SVt_PVFM);

    ENTER;
    SAVESPTR(PL_compcv);

    SensorCall(9284);cv = PL_compcv = MUTABLE_CV(newSV_type(SvTYPE(proto)));
    CvFLAGS(cv) = CvFLAGS(proto) & ~(CVf_CLONE|CVf_WEAKOUTSIDE|CVf_CVGV_RC);
    CvCLONED_on(cv);

    CvFILE(cv)		= CvDYNFILE(proto) ? savepv(CvFILE(proto))
					   : CvFILE(proto);
    CvGV_set(cv,CvGV(proto));
    CvSTASH_set(cv, CvSTASH(proto));
    OP_REFCNT_LOCK;
    CvROOT(cv)		= OpREFCNT_inc(CvROOT(proto));
    OP_REFCNT_UNLOCK;
    CvSTART(cv)		= CvSTART(proto);
    CvOUTSIDE(cv)	= MUTABLE_CV(SvREFCNT_inc_simple(outside));
    CvOUTSIDE_SEQ(cv) = CvOUTSIDE_SEQ(proto);

    SensorCall(9285);if (SvPOK(proto))
	sv_setpvn(MUTABLE_SV(cv), SvPVX_const(proto), SvCUR(proto));

    CvPADLIST(cv) = pad_new(padnew_CLONE|padnew_SAVE);

    av_fill(PL_comppad, fpad);
    SensorCall(9286);for (ix = fname; ix > 0; ix--)
	av_store(PL_comppad_name, ix, SvREFCNT_inc(pname[ix]));

    PL_curpad = AvARRAY(PL_comppad);

    SensorCall(9287);outpad = CvPADLIST(outside)
	? AvARRAY(AvARRAY(CvPADLIST(outside))[depth])
	: NULL;

    SensorCall(9300);for (ix = fpad; ix > 0; ix--) {
	SensorCall(9288);SV* const namesv = (ix <= fname) ? pname[ix] : NULL;
	SV *sv = NULL;
	SensorCall(9299);if (namesv && namesv != &PL_sv_undef) { /* lexical */
	    SensorCall(9289);if (SvFAKE(namesv)) {   /* lexical from outside? */
		/* formats may have an inactive, or even undefined, parent,
		   while my $x if $false can leave an active var marked as
		   stale. And state vars are always available */
		SensorCall(9290);if (!outpad || !(sv = outpad[PARENT_PAD_INDEX(namesv)])
		 || (SvPADSTALE(sv) && !SvPAD_STATE(namesv))) {
		    SensorCall(9291);Perl_ck_warner(aTHX_ packWARN(WARN_CLOSURE),
				   "Variable \"%"SVf"\" is not available", namesv);
		    sv = NULL;
		}
		else 
		    SvREFCNT_inc_simple_void_NN(sv);
	    }
	    SensorCall(9295);if (!sv) {
                SensorCall(9292);const char sigil = SvPVX_const(namesv)[0];
                SensorCall(9293);if (sigil == '&')
		    sv = SvREFCNT_inc(ppad[ix]);
                else if (sigil == '@')
		    sv = MUTABLE_SV(newAV());
                else if (sigil == '%')
		    sv = MUTABLE_SV(newHV());
		else
		    sv = newSV(0);
		SvPADMY_on(sv);
		/* reset the 'assign only once' flag on each state var */
		SensorCall(9294);if (SvPAD_STATE(namesv))
		    SvPADSTALE_on(sv);
	    }
	}
	else {/*3*/SensorCall(9296);if (IS_PADGV(ppad[ix]) || IS_PADCONST(ppad[ix])) {
	    SensorCall(9297);sv = SvREFCNT_inc_NN(ppad[ix]);
	}
	else {
	    SensorCall(9298);sv = newSV(0);
	    SvPADTMP_on(sv);
	;/*4*/}}
	PL_curpad[ix] = sv;
    }

    DEBUG_Xv(
	PerlIO_printf(Perl_debug_log, "\nPad CV clone\n");
	cv_dump(outside, "Outside");
	cv_dump(proto,	 "Proto");
	cv_dump(cv,	 "To");
    );

    LEAVE;

    SensorCall(9304);if (CvCONST(cv)) {
	/* Constant sub () { $x } closing over $x - see lib/constant.pm:
	 * The prototype was marked as a candiate for const-ization,
	 * so try to grab the current const value, and if successful,
	 * turn into a const sub:
	 */
	SensorCall(9301);SV* const const_sv = op_const_sv(CvSTART(cv), cv);
	SensorCall(9303);if (const_sv) {
	    SvREFCNT_dec(cv);
	    SensorCall(9302);cv = newCONSTSUB(CvSTASH(proto), NULL, const_sv);
	}
	else {
	    CvCONST_off(cv);
	}
    }

    {CV * ReplaceReturn2140 = cv; SensorCall(9305); return ReplaceReturn2140;}
}

/*
=for apidoc m|void|pad_fixup_inner_anons|PADLIST *padlist|CV *old_cv|CV *new_cv

For any anon CVs in the pad, change CvOUTSIDE of that CV from
old_cv to new_cv if necessary. Needed when a newly-compiled CV has to be
moved to a pre-existing CV struct.

=cut
*/

void
Perl_pad_fixup_inner_anons(pTHX_ PADLIST *padlist, CV *old_cv, CV *new_cv)
{
SensorCall(9306);    dVAR;
    I32 ix;
    AV * const comppad_name = MUTABLE_AV(AvARRAY(padlist)[0]);
    AV * const comppad = MUTABLE_AV(AvARRAY(padlist)[1]);
    SV ** const namepad = AvARRAY(comppad_name);
    SV ** const curpad = AvARRAY(comppad);

    PERL_ARGS_ASSERT_PAD_FIXUP_INNER_ANONS;
    PERL_UNUSED_ARG(old_cv);

    SensorCall(9315);for (ix = AvFILLp(comppad_name); ix > 0; ix--) {
        SensorCall(9307);const SV * const namesv = namepad[ix];
	SensorCall(9314);if (namesv && namesv != &PL_sv_undef
	    && *SvPVX_const(namesv) == '&')
	{
	  SensorCall(9308);if (SvTYPE(curpad[ix]) == SVt_PVCV) {
	    SensorCall(9309);CV * const innercv = MUTABLE_CV(curpad[ix]);
	    assert(CvWEAKOUTSIDE(innercv));
	    assert(CvOUTSIDE(innercv) == old_cv);
	    CvOUTSIDE(innercv) = new_cv;
	  }
	  else { /* format reference */
	    SensorCall(9310);SV * const rv = curpad[ix];
	    CV *innercv;
	    SensorCall(9312);if (!SvOK(rv)) {/*27*/SensorCall(9311);continue;/*28*/}
	    assert(SvROK(rv));
	    assert(SvWEAKREF(rv));
	    SensorCall(9313);innercv = (CV *)SvRV(rv);
	    assert(!CvWEAKOUTSIDE(innercv));
	    SvREFCNT_dec(CvOUTSIDE(innercv));
	    CvOUTSIDE(innercv) = (CV *)SvREFCNT_inc_simple_NN(new_cv);
	  }
	}
    }
SensorCall(9316);}

/*
=for apidoc m|void|pad_push|PADLIST *padlist|int depth

Push a new pad frame onto the padlist, unless there's already a pad at
this depth, in which case don't bother creating a new one.  Then give
the new pad an @_ in slot zero.

=cut
*/

void
Perl_pad_push(pTHX_ PADLIST *padlist, int depth)
{
SensorCall(9317);    dVAR;

    PERL_ARGS_ASSERT_PAD_PUSH;

    SensorCall(9328);if (depth > AvFILLp(padlist)) {
	SensorCall(9318);SV** const svp = AvARRAY(padlist);
	AV* const newpad = newAV();
	SV** const oldpad = AvARRAY(svp[depth-1]);
	I32 ix = AvFILLp((const AV *)svp[1]);
        const I32 names_fill = AvFILLp((const AV *)svp[0]);
	SV** const names = AvARRAY(svp[0]);
	AV *av;

	SensorCall(9326);for ( ;ix > 0; ix--) {
	    SensorCall(9319);if (names_fill >= ix && names[ix] != &PL_sv_undef) {
		SensorCall(9320);const char sigil = SvPVX_const(names[ix])[0];
		SensorCall(9323);if ((SvFLAGS(names[ix]) & SVf_FAKE)
			|| (SvFLAGS(names[ix]) & SVpad_STATE)
			|| sigil == '&')
		{
		    /* outer lexical or anon code */
		    av_store(newpad, ix, SvREFCNT_inc(oldpad[ix]));
		}
		else {		/* our own lexical */
		    SensorCall(9321);SV *sv; 
		    SensorCall(9322);if (sigil == '@')
			sv = MUTABLE_SV(newAV());
		    else if (sigil == '%')
			sv = MUTABLE_SV(newHV());
		    else
			sv = newSV(0);
		    av_store(newpad, ix, sv);
		    SvPADMY_on(sv);
		}
	    }
	    else {/*37*/SensorCall(9324);if (IS_PADGV(oldpad[ix]) || IS_PADCONST(oldpad[ix])) {
		av_store(newpad, ix, SvREFCNT_inc_NN(oldpad[ix]));
	    }
	    else {
		/* save temporaries on recursion? */
		SensorCall(9325);SV * const sv = newSV(0);
		av_store(newpad, ix, sv);
		SvPADTMP_on(sv);
	    ;/*38*/}}
	}
	SensorCall(9327);av = newAV();
	av_store(newpad, 0, MUTABLE_SV(av));
	AvREIFY_only(av);

	av_store(padlist, depth, MUTABLE_SV(newpad));
	AvFILLp(padlist) = depth;
    }
SensorCall(9329);}

/*
=for apidoc Am|HV *|pad_compname_type|PADOFFSET po

Looks up the type of the lexical variable at position I<po> in the
currently-compiling pad.  If the variable is typed, the stash of the
class to which it is typed is returned.  If not, C<NULL> is returned.

=cut
*/

HV *
Perl_pad_compname_type(pTHX_ const PADOFFSET po)
{
SensorCall(9330);    dVAR;
    SV* const * const av = av_fetch(PL_comppad_name, po, FALSE);
    SensorCall(9332);if ( SvPAD_TYPED(*av) ) {
        {HV * ReplaceReturn2139 = SvSTASH(*av); SensorCall(9331); return ReplaceReturn2139;}
    }
    {HV * ReplaceReturn2138 = NULL; SensorCall(9333); return ReplaceReturn2138;}
}

#if defined(USE_ITHREADS)

#  define av_dup_inc(s,t)	MUTABLE_AV(sv_dup_inc((const SV *)s,t))

/*
=for apidoc m|AV *|padlist_dup|AV *srcpad|CLONE_PARAMS *param

Duplicates a pad.

=cut
*/

AV *
Perl_padlist_dup(pTHX_ AV *srcpad, CLONE_PARAMS *param)
{
    SensorCall(9334);AV *dstpad;
    PERL_ARGS_ASSERT_PADLIST_DUP;

    SensorCall(9335);if (!srcpad)
	return NULL;

    SensorCall(9359);if (param->flags & CLONEf_COPY_STACKS
	|| SvREFCNT(AvARRAY(srcpad)[1]) > 1) {
	SensorCall(9336);dstpad = av_dup_inc(srcpad, param);
	assert (SvREFCNT(AvARRAY(srcpad)[1]) == 1);
    } else {
	/* CvDEPTH() on our subroutine will be set to 0, so there's no need
	   to build anything other than the first level of pads.  */

	SensorCall(9337);I32 ix = AvFILLp((const AV *)AvARRAY(srcpad)[1]);
	AV *pad1;
	const I32 names_fill = AvFILLp((const AV *)(AvARRAY(srcpad)[0]));
	const AV *const srcpad1 = (const AV *) AvARRAY(srcpad)[1];
	SV **oldpad = AvARRAY(srcpad1);
	SV **names;
	SV **pad1a;
	AV *args;
	/* Look for it in the table first, as the padlist may have ended up
	   as an element of @DB::args (or theoretically even @_), so it may
	   may have been cloned already.  It may also be there because of
	   how Perl_sv_compile_2op() "works". :-(   */
	dstpad = (AV*)ptr_table_fetch(PL_ptr_table, srcpad);

	SensorCall(9338);if (dstpad)
	    return (AV *)SvREFCNT_inc_simple_NN(dstpad);

	SensorCall(9339);dstpad = newAV();
	ptr_table_store(PL_ptr_table, srcpad, dstpad);
	av_extend(dstpad, 1);
	AvARRAY(dstpad)[0] = MUTABLE_SV(av_dup_inc(AvARRAY(srcpad)[0], param));
	names = AvARRAY(AvARRAY(dstpad)[0]);

	pad1 = newAV();

	av_extend(pad1, ix);
	AvARRAY(dstpad)[1] = MUTABLE_SV(pad1);
	pad1a = AvARRAY(pad1);
	AvFILLp(dstpad) = 1;

	SensorCall(9358);if (ix > -1) {
	    AvFILLp(pad1) = ix;

	    SensorCall(9355);for ( ;ix > 0; ix--) {
		SensorCall(9340);if (!oldpad[ix]) {
		    SensorCall(9341);pad1a[ix] = NULL;
		} else {/*79*/SensorCall(9342);if (names_fill >= ix && names[ix] != &PL_sv_undef) {
		    SensorCall(9343);const char sigil = SvPVX_const(names[ix])[0];
		    SensorCall(9350);if ((SvFLAGS(names[ix]) & SVf_FAKE)
			|| (SvFLAGS(names[ix]) & SVpad_STATE)
			|| sigil == '&')
			{
			    /* outer lexical or anon code */
			    SensorCall(9344);pad1a[ix] = sv_dup_inc(oldpad[ix], param);
			}
		    else {		/* our own lexical */
			SensorCall(9345);if(SvPADSTALE(oldpad[ix]) && SvREFCNT(oldpad[ix]) > 1) {
			    /* This is a work around for how the current
			       implementation of ?{ } blocks in regexps
			       interacts with lexicals.  */
			    SensorCall(9346);pad1a[ix] = sv_dup_inc(oldpad[ix], param);
			} else {
			    SensorCall(9347);SV *sv; 
			    
			    SensorCall(9348);if (sigil == '@')
				sv = MUTABLE_SV(newAV());
			    else if (sigil == '%')
				sv = MUTABLE_SV(newHV());
			    else
				sv = newSV(0);
			    SensorCall(9349);pad1a[ix] = sv;
			    SvPADMY_on(sv);
			}
		    }
		}
		else {/*81*/SensorCall(9351);if (IS_PADGV(oldpad[ix]) || IS_PADCONST(oldpad[ix])) {
		    SensorCall(9352);pad1a[ix] = sv_dup_inc(oldpad[ix], param);
		}
		else {
		    /* save temporaries on recursion? */
		    SensorCall(9353);SV * const sv = newSV(0);
		    pad1a[ix] = sv;

		    /* SvREFCNT(oldpad[ix]) != 1 for some code in threads.xs
		       FIXTHAT before merging this branch.
		       (And I know how to) */
		    SensorCall(9354);if (SvPADMY(oldpad[ix]))
			SvPADMY_on(sv);
		    else
			SvPADTMP_on(sv);
		;/*82*/}/*80*/}}
	    }

	    SensorCall(9357);if (oldpad[0]) {
		SensorCall(9356);args = newAV();			/* Will be @_ */
		AvREIFY_only(args);
		pad1a[0] = (SV *)args;
	    }
	}
    }

    {AV * ReplaceReturn2137 = dstpad; SensorCall(9360); return ReplaceReturn2137;}
}

#endif /* USE_ITHREADS */

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 *
 * ex: set ts=8 sts=4 sw=4 noet:
 */
