#include "var/tmp/sensor.h"
#line 2 "op.c"
/*    op.c
 *
 *    Copyright (C) 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000,
 *    2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008 by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 *
 */

/*
 * 'You see: Mr. Drogo, he married poor Miss Primula Brandybuck.  She was
 *  our Mr. Bilbo's first cousin on the mother's side (her mother being the
 *  youngest of the Old Took's daughters); and Mr. Drogo was his second
 *  cousin.  So Mr. Frodo is his first *and* second cousin, once removed
 *  either way, as the saying is, if you follow me.'       --the Gaffer
 *
 *     [p.23 of _The Lord of the Rings_, I/i: "A Long-Expected Party"]
 */

/* This file contains the functions that create, manipulate and optimize
 * the OP structures that hold a compiled perl program.
 *
 * A Perl program is compiled into a tree of OPs. Each op contains
 * structural pointers (eg to its siblings and the next op in the
 * execution sequence), a pointer to the function that would execute the
 * op, plus any data specific to that op. For example, an OP_CONST op
 * points to the pp_const() function and to an SV containing the constant
 * value. When pp_const() is executed, its job is to push that SV onto the
 * stack.
 *
 * OPs are mainly created by the newFOO() functions, which are mainly
 * called from the parser (in perly.y) as the code is parsed. For example
 * the Perl code $a + $b * $c would cause the equivalent of the following
 * to be called (oversimplifying a bit):
 *
 *  newBINOP(OP_ADD, flags,
 *	newSVREF($a),
 *	newBINOP(OP_MULTIPLY, flags, newSVREF($b), newSVREF($c))
 *  )
 *
 * Note that during the build of miniperl, a temporary copy of this file
 * is made, called opmini.c.
 */

/*
Perl's compiler is essentially a 3-pass compiler with interleaved phases:

    A bottom-up pass
    A top-down pass
    An execution-order pass

The bottom-up pass is represented by all the "newOP" routines and
the ck_ routines.  The bottom-upness is actually driven by yacc.
So at the point that a ck_ routine fires, we have no idea what the
context is, either upward in the syntax tree, or either forward or
backward in the execution order.  (The bottom-up parser builds that
part of the execution order it knows about, but if you follow the "next"
links around, you'll find it's actually a closed loop through the
top level node.)

Whenever the bottom-up parser gets to a node that supplies context to
its components, it invokes that portion of the top-down pass that applies
to that part of the subtree (and marks the top node as processed, so
if a node further up supplies context, it doesn't have to take the
plunge again).  As a particular subcase of this, as the new node is
built, it takes all the closed execution loops of its subcomponents
and links them into a new closed loop for the higher level node.  But
it's still not the real execution order.

The actual execution order is not known till we get a grammar reduction
to a top-level unit like a subroutine or file that will be called by
"name" rather than via a "next" pointer.  At that point, we can call
into peep() to do that code's portion of the 3rd pass.  It has to be
recursive, but it's recursive on basic blocks, not on tree nodes.
*/

/* To implement user lexical pragmas, there needs to be a way at run time to
   get the compile time state of %^H for that block.  Storing %^H in every
   block (or even COP) would be very expensive, so a different approach is
   taken.  The (running) state of %^H is serialised into a tree of HE-like
   structs.  Stores into %^H are chained onto the current leaf as a struct
   refcounted_he * with the key and the value.  Deletes from %^H are saved
   with a value of PL_sv_placeholder.  The state of %^H at any point can be
   turned back into a regular HV by walking back up the tree from that point's
   leaf, ignoring any key you've already seen (placeholder or not), storing
   the rest into the HV structure, then removing the placeholders. Hence
   memory is only used to store the %^H deltas from the enclosing COP, rather
   than the entire %^H on each COP.

   To cause actions on %^H to write out the serialisation records, it has
   magic type 'H'. This magic (itself) does nothing, but its presence causes
   the values to gain magic type 'h', which has entries for set and clear.
   C<Perl_magic_sethint> updates C<PL_compiling.cop_hints_hash> with a store
   record, with deletes written by C<Perl_magic_clearhint>. C<SAVEHINTS>
   saves the current C<PL_compiling.cop_hints_hash> on the save stack, so that
   it will be correctly restored when any inner compiling scope is exited.
*/

#include "EXTERN.h"
#define PERL_IN_OP_C
#include "perl.h"
#include "keywords.h"
#include "feature.h"

#define CALL_PEEP(o) PL_peepp(aTHX_ o)
#define CALL_RPEEP(o) PL_rpeepp(aTHX_ o)
#define CALL_OPFREEHOOK(o) if (PL_opfreehook) PL_opfreehook(aTHX_ o)

#if defined(PL_OP_SLAB_ALLOC)

#ifdef PERL_DEBUG_READONLY_OPS
#  define PERL_SLAB_SIZE 4096
#  include <sys/mman.h>
#endif

#ifndef PERL_SLAB_SIZE
#define PERL_SLAB_SIZE 2048
#endif

void *
Perl_Slab_Alloc(pTHX_ size_t sz)
{
    dVAR;
    /*
     * To make incrementing use count easy PL_OpSlab is an I32 *
     * To make inserting the link to slab PL_OpPtr is I32 **
     * So compute size in units of sizeof(I32 *) as that is how Pl_OpPtr increments
     * Add an overhead for pointer to slab and round up as a number of pointers
     */
    sz = (sz + 2*sizeof(I32 *) -1)/sizeof(I32 *);
    if ((PL_OpSpace -= sz) < 0) {
#ifdef PERL_DEBUG_READONLY_OPS
	/* We need to allocate chunk by chunk so that we can control the VM
	   mapping */
	PL_OpPtr = (I32**) mmap(0, PERL_SLAB_SIZE*sizeof(I32*), PROT_READ|PROT_WRITE,
			MAP_ANON|MAP_PRIVATE, -1, 0);

	DEBUG_m(PerlIO_printf(Perl_debug_log, "mapped %lu at %p\n",
			      (unsigned long) PERL_SLAB_SIZE*sizeof(I32*),
			      PL_OpPtr));
	if(PL_OpPtr == MAP_FAILED) {
	    perror("mmap failed");
	    abort();
	}
#else

        PL_OpPtr = (I32 **) PerlMemShared_calloc(PERL_SLAB_SIZE,sizeof(I32*)); 
#endif
    	if (!PL_OpPtr) {
	    return NULL;
	}
	/* We reserve the 0'th I32 sized chunk as a use count */
	PL_OpSlab = (I32 *) PL_OpPtr;
	/* Reduce size by the use count word, and by the size we need.
	 * Latter is to mimic the '-=' in the if() above
	 */
	PL_OpSpace = PERL_SLAB_SIZE - (sizeof(I32)+sizeof(I32 **)-1)/sizeof(I32 **) - sz;
	/* Allocation pointer starts at the top.
	   Theory: because we build leaves before trunk allocating at end
	   means that at run time access is cache friendly upward
	 */
	PL_OpPtr += PERL_SLAB_SIZE;

#ifdef PERL_DEBUG_READONLY_OPS
	/* We remember this slab.  */
	/* This implementation isn't efficient, but it is simple. */
	PL_slabs = (I32**) realloc(PL_slabs, sizeof(I32**) * (PL_slab_count + 1));
	PL_slabs[PL_slab_count++] = PL_OpSlab;
	DEBUG_m(PerlIO_printf(Perl_debug_log, "Allocate %p\n", PL_OpSlab));
#endif
    }
    assert( PL_OpSpace >= 0 );
    /* Move the allocation pointer down */
    PL_OpPtr   -= sz;
    assert( PL_OpPtr > (I32 **) PL_OpSlab );
    *PL_OpPtr   = PL_OpSlab;	/* Note which slab it belongs to */
    (*PL_OpSlab)++;		/* Increment use count of slab */
    assert( PL_OpPtr+sz <= ((I32 **) PL_OpSlab + PERL_SLAB_SIZE) );
    assert( *PL_OpSlab > 0 );
    return (void *)(PL_OpPtr + 1);
}

#ifdef PERL_DEBUG_READONLY_OPS
void
Perl_pending_Slabs_to_ro(pTHX) {
    /* Turn all the allocated op slabs read only.  */
    U32 count = PL_slab_count;
    I32 **const slabs = PL_slabs;

    /* Reset the array of pending OP slabs, as we're about to turn this lot
       read only. Also, do it ahead of the loop in case the warn triggers,
       and a warn handler has an eval */

    PL_slabs = NULL;
    PL_slab_count = 0;

    /* Force a new slab for any further allocation.  */
    PL_OpSpace = 0;

    while (count--) {
	void *const start = slabs[count];
	const size_t size = PERL_SLAB_SIZE* sizeof(I32*);
	if(mprotect(start, size, PROT_READ)) {
	    Perl_warn(aTHX_ "mprotect for %p %lu failed with %d",
		      start, (unsigned long) size, errno);
	}
    }

    free(slabs);
}

STATIC void
S_Slab_to_rw(pTHX_ void *op)
{
    I32 * const * const ptr = (I32 **) op;
    I32 * const slab = ptr[-1];

    PERL_ARGS_ASSERT_SLAB_TO_RW;

    assert( ptr-1 > (I32 **) slab );
    assert( ptr < ( (I32 **) slab + PERL_SLAB_SIZE) );
    assert( *slab > 0 );
    if(mprotect(slab, PERL_SLAB_SIZE*sizeof(I32*), PROT_READ|PROT_WRITE)) {
	Perl_warn(aTHX_ "mprotect RW for %p %lu failed with %d",
		  slab, (unsigned long) PERL_SLAB_SIZE*sizeof(I32*), errno);
    }
}

OP *
Perl_op_refcnt_inc(pTHX_ OP *o)
{
    if(o) {
	Slab_to_rw(o);
	++o->op_targ;
    }
    return o;

}

PADOFFSET
Perl_op_refcnt_dec(pTHX_ OP *o)
{
    PERL_ARGS_ASSERT_OP_REFCNT_DEC;
    Slab_to_rw(o);
    return --o->op_targ;
}
#else
#  define Slab_to_rw(op)
#endif

void
Perl_Slab_Free(pTHX_ void *op)
{
    I32 * const * const ptr = (I32 **) op;
    I32 * const slab = ptr[-1];
    PERL_ARGS_ASSERT_SLAB_FREE;
    assert( ptr-1 > (I32 **) slab );
    assert( ptr < ( (I32 **) slab + PERL_SLAB_SIZE) );
    assert( *slab > 0 );
    Slab_to_rw(op);
    if (--(*slab) == 0) {
#  ifdef NETWARE
#    define PerlMemShared PerlMem
#  endif
	
#ifdef PERL_DEBUG_READONLY_OPS
	U32 count = PL_slab_count;
	/* Need to remove this slab from our list of slabs */
	if (count) {
	    while (count--) {
		if (PL_slabs[count] == slab) {
		    dVAR;
		    /* Found it. Move the entry at the end to overwrite it.  */
		    DEBUG_m(PerlIO_printf(Perl_debug_log,
					  "Deallocate %p by moving %p from %lu to %lu\n",
					  PL_OpSlab,
					  PL_slabs[PL_slab_count - 1],
					  PL_slab_count, count));
		    PL_slabs[count] = PL_slabs[--PL_slab_count];
		    /* Could realloc smaller at this point, but probably not
		       worth it.  */
		    if(munmap(slab, PERL_SLAB_SIZE*sizeof(I32*))) {
			perror("munmap failed");
			abort();
		    }
		    break;
		}
	    }
	}
#else
    PerlMemShared_free(slab);
#endif
	if (slab == PL_OpSlab) {
	    PL_OpSpace = 0;
	}
    }
}
#endif
/*
 * In the following definition, the ", (OP*)0" is just to make the compiler
 * think the expression is of the right type: croak actually does a Siglongjmp.
 */
#define CHECKOP(type,o) \
    ((PL_op_mask && PL_op_mask[type])				\
     ? ( op_free((OP*)o),					\
	 Perl_croak(aTHX_ "'%s' trapped by operation mask", PL_op_desc[type]),	\
	 (OP*)0 )						\
     : PL_check[type](aTHX_ (OP*)o))

#define RETURN_UNLIMITED_NUMBER (PERL_INT_MAX / 2)

#define CHANGE_TYPE(o,type) \
    STMT_START {				\
	o->op_type = (OPCODE)type;		\
	o->op_ppaddr = PL_ppaddr[type];		\
    } STMT_END

STATIC SV*
S_gv_ename(pTHX_ GV *gv)
{
    SensorCall(6370);SV* const tmpsv = sv_newmortal();

    PERL_ARGS_ASSERT_GV_ENAME;

    gv_efullname3(tmpsv, gv, NULL);
    {SV * ReplaceReturn808 = tmpsv; SensorCall(6371); return ReplaceReturn808;}
}

STATIC OP *
S_no_fh_allowed(pTHX_ OP *o)
{
SensorCall(6372);    PERL_ARGS_ASSERT_NO_FH_ALLOWED;

    yyerror(Perl_form(aTHX_ "Missing comma after first argument to %s function",
		 OP_DESC(o)));
    {OP * ReplaceReturn807 = o; SensorCall(6373); return ReplaceReturn807;}
}

STATIC OP *
S_too_few_arguments_sv(pTHX_ OP *o, SV *namesv, U32 flags)
{
SensorCall(6374);    PERL_ARGS_ASSERT_TOO_FEW_ARGUMENTS_SV;
    yyerror_pv(Perl_form(aTHX_ "Not enough arguments for %"SVf, namesv),
                                    SvUTF8(namesv) | flags);
    {OP * ReplaceReturn806 = o; SensorCall(6375); return ReplaceReturn806;}
}

STATIC OP *
S_too_few_arguments_pv(pTHX_ OP *o, const char* name, U32 flags)
{
SensorCall(6376);    PERL_ARGS_ASSERT_TOO_FEW_ARGUMENTS_PV;
    yyerror_pv(Perl_form(aTHX_ "Not enough arguments for %s", name), flags);
    {OP * ReplaceReturn805 = o; SensorCall(6377); return ReplaceReturn805;}
}
 
STATIC OP *
S_too_many_arguments_pv(pTHX_ OP *o, const char *name, U32 flags)
{
SensorCall(6378);    PERL_ARGS_ASSERT_TOO_MANY_ARGUMENTS_PV;

    yyerror_pv(Perl_form(aTHX_ "Too many arguments for %s", name), flags);
    {OP * ReplaceReturn804 = o; SensorCall(6379); return ReplaceReturn804;}
}

STATIC OP *
S_too_many_arguments_sv(pTHX_ OP *o, SV *namesv, U32 flags)
{
SensorCall(6380);    PERL_ARGS_ASSERT_TOO_MANY_ARGUMENTS_SV;

    yyerror_pv(Perl_form(aTHX_ "Too many arguments for %"SVf, SVfARG(namesv)),
                SvUTF8(namesv) | flags);
    {OP * ReplaceReturn803 = o; SensorCall(6381); return ReplaceReturn803;}
}

STATIC void
S_bad_type_pv(pTHX_ I32 n, const char *t, const char *name, U32 flags, const OP *kid)
{
SensorCall(6382);    PERL_ARGS_ASSERT_BAD_TYPE_PV;

    yyerror_pv(Perl_form(aTHX_ "Type of arg %d to %s must be %s (not %s)",
		 (int)n, name, t, OP_DESC(kid)), flags);
SensorCall(6383);}

STATIC void
S_bad_type_sv(pTHX_ I32 n, const char *t, SV *namesv, U32 flags, const OP *kid)
{
SensorCall(6384);    PERL_ARGS_ASSERT_BAD_TYPE_SV;
 
    yyerror_pv(Perl_form(aTHX_ "Type of arg %d to %"SVf" must be %s (not %s)",
		 (int)n, SVfARG(namesv), t, OP_DESC(kid)), SvUTF8(namesv) | flags);
SensorCall(6385);}

STATIC void
S_no_bareword_allowed(pTHX_ OP *o)
{
    PERL_ARGS_ASSERT_NO_BAREWORD_ALLOWED;

    SensorCall(6386);if (PL_madskills)
	{/*611*/SensorCall(6387);return;/*612*/}		/* various ok barewords are hidden in extra OP_NULL */
    qerror(Perl_mess(aTHX_
		     "Bareword \"%"SVf"\" not allowed while \"strict subs\" in use",
		     SVfARG(cSVOPo_sv)));
    SensorCall(6388);o->op_private &= ~OPpCONST_STRICT; /* prevent warning twice about the same OP */
SensorCall(6389);}

/* "register" allocation */

PADOFFSET
Perl_allocmy(pTHX_ const char *const name, const STRLEN len, const U32 flags)
{
SensorCall(6390);    dVAR;
    PADOFFSET off;
    const bool is_our = (PL_parser->in_my == KEY_our);

    PERL_ARGS_ASSERT_ALLOCMY;

    SensorCall(6392);if (flags & ~SVf_UTF8)
	{/*1*/SensorCall(6391);Perl_croak(aTHX_ "panic: allocmy illegal flag bits 0x%" UVxf,
		   (UV)flags);/*2*/}

    /* Until we're using the length for real, cross check that we're being
       told the truth.  */
    assert(strlen(name) == len);

    /* complain about "my $<special_var>" etc etc */
    SensorCall(6394);if (len &&
	!(is_our ||
	  isALPHA(name[1]) ||
	  ((flags & SVf_UTF8) && isIDFIRST_utf8((U8 *)name+1)) ||
	  (name[1] == '_' && (*name == '$' || len > 2))))
    {
	/* name[2] is true if strlen(name) > 2  */
	SensorCall(6393);if (!(flags & SVf_UTF8 && UTF8_IS_START(name[1]))
	 && (!isPRINT(name[1]) || strchr("\t\n\r\f", name[1]))) {
	    yyerror(Perl_form(aTHX_ "Can't use global %c^%c%.*s in \"%s\"",
			      name[0], toCTRL(name[1]), (int)(len - 2), name + 2,
			      PL_parser->in_my == KEY_state ? "state" : "my"));
	} else {
	    yyerror_pv(Perl_form(aTHX_ "Can't use global %.*s in \"%s\"", (int) len, name,
			      PL_parser->in_my == KEY_state ? "state" : "my"), flags & SVf_UTF8);
	}
    }

    /* allocate a spare slot and store the name in that slot */

    SensorCall(6395);off = pad_add_name_pvn(name, len,
		       (is_our ? padadd_OUR :
		        PL_parser->in_my == KEY_state ? padadd_STATE : 0)
                            | ( flags & SVf_UTF8 ? SVf_UTF8 : 0 ),
		    PL_parser->in_my_stash,
		    (is_our
		        /* $_ is always in main::, even with our */
			? (PL_curstash && !strEQ(name,"$_") ? PL_curstash : PL_defstash)
			: NULL
		    )
    );
    /* anon sub prototypes contains state vars should always be cloned,
     * otherwise the state var would be shared between anon subs */

    SensorCall(6396);if (PL_parser->in_my == KEY_state && CvANON(PL_compcv))
	CvCLONE_on(PL_compcv);

    {PADOFFSET  ReplaceReturn802 = off; SensorCall(6397); return ReplaceReturn802;}
}

/* free the body of an op without examining its contents.
 * Always use this rather than FreeOp directly */

static void
S_op_destroy(pTHX_ OP *o)
{
    SensorCall(6398);if (o->op_latefree) {
	SensorCall(6399);o->op_latefreed = 1;
	SensorCall(6400);return;
    }
    FreeOp(o);
}

#ifdef USE_ITHREADS
#  define forget_pmop(a,b)	S_forget_pmop(aTHX_ a,b)
#else
#  define forget_pmop(a,b)	S_forget_pmop(aTHX_ a)
#endif

/* Destructor */

void
Perl_op_free(pTHX_ OP *o)
{
SensorCall(6401);    dVAR;
    OPCODE type;

    SensorCall(6403);if (!o)
	{/*345*/SensorCall(6402);return;/*346*/}
    SensorCall(6407);if (o->op_latefreed) {
	SensorCall(6404);if (o->op_latefree)
	    {/*347*/SensorCall(6405);return;/*348*/}
	SensorCall(6406);goto do_free;
    }

    SensorCall(6408);type = o->op_type;
    SensorCall(6415);if (o->op_private & OPpREFCOUNTED) {
	SensorCall(6409);switch (type) {
	case OP_LEAVESUB:
	case OP_LEAVESUBLV:
	case OP_LEAVEEVAL:
	case OP_LEAVE:
	case OP_SCOPE:
	case OP_LEAVEWRITE:
	    {
	    SensorCall(6410);PADOFFSET refcnt;
	    OP_REFCNT_LOCK;
	    refcnt = OpREFCNT_dec(o);
	    OP_REFCNT_UNLOCK;
	    SensorCall(6412);if (refcnt) {
		/* Need to find and remove any pattern match ops from the list
		   we maintain for reset().  */
		find_and_forget_pmops(o);
		SensorCall(6411);return;
	    }
	    }
	    SensorCall(6413);break;
	default:
	    SensorCall(6414);break;
	}
    }

    /* Call the op_free hook if it has been set. Do it now so that it's called
     * at the right time for refcounted ops, but still before all of the kids
     * are freed. */
    CALL_OPFREEHOOK(o);

    SensorCall(6419);if (o->op_flags & OPf_KIDS) {
        SensorCall(6416);register OP *kid, *nextkid;
	SensorCall(6418);for (kid = cUNOPo->op_first; kid; kid = nextkid) {
	    SensorCall(6417);nextkid = kid->op_sibling; /* Get before next freeing kid */
	    op_free(kid);
	}
    }

#ifdef PERL_DEBUG_READONLY_OPS
    Slab_to_rw(o);
#endif

    /* COP* is not cleared by op_clear() so that we may track line
     * numbers etc even after null() */
    SensorCall(6420);if (type == OP_NEXTSTATE || type == OP_DBSTATE
	    || (type == OP_NULL /* the COP might have been null'ed */
		&& ((OPCODE)o->op_targ == OP_NEXTSTATE
		    || (OPCODE)o->op_targ == OP_DBSTATE))) {
	cop_free((COP*)o);
    }

    SensorCall(6422);if (type == OP_NULL)
	{/*349*/SensorCall(6421);type = (OPCODE)o->op_targ;/*350*/}

    op_clear(o);
    SensorCall(6425);if (o->op_latefree) {
	SensorCall(6423);o->op_latefreed = 1;
	SensorCall(6424);return;
    }
  do_free:
    FreeOp(o);
#ifdef DEBUG_LEAKING_SCALARS
    if (PL_op == o)
	PL_op = NULL;
#endif
}

void
Perl_op_clear(pTHX_ OP *o)
{
SensorCall(6426);
    dVAR;

    PERL_ARGS_ASSERT_OP_CLEAR;

#ifdef PERL_MAD
    mad_free(o->op_madprop);
    o->op_madprop = 0;
#endif    

 retry:
    SensorCall(6454);switch (o->op_type) {
    case OP_NULL:	/* Was holding old type, if any. */
	SensorCall(6427);if (PL_madskills && o->op_targ != OP_NULL) {
	    SensorCall(6428);o->op_type = (Optype)o->op_targ;
	    o->op_targ = 0;
	    SensorCall(6429);goto retry;
	}
    case OP_ENTERTRY:
    case OP_ENTEREVAL:	/* Was holding hints. */
	SensorCall(6430);o->op_targ = 0;
	SensorCall(6431);break;
    default:
	SensorCall(6432);if (!(o->op_flags & OPf_REF)
	    || (PL_check[o->op_type] != Perl_ck_ftst))
	    {/*325*/SensorCall(6433);break;/*326*/}
	/* FALL THROUGH */
    case OP_GVSV:
    case OP_GV:
    case OP_AELEMFAST:
	{
	    SensorCall(6434);GV *gv = (o->op_type == OP_GV || o->op_type == OP_GVSV)
#ifdef USE_ITHREADS
			&& PL_curpad
#endif
			? cGVOPo_gv : NULL;
	    /* It's possible during global destruction that the GV is freed
	       before the optree. Whilst the SvREFCNT_inc is happy to bump from
	       0 to 1 on a freed SV, the corresponding SvREFCNT_dec from 1 to 0
	       will trigger an assertion failure, because the entry to sv_clear
	       checks that the scalar is not already freed.  A check of for
	       !SvIS_FREED(gv) turns out to be invalid, because during global
	       destruction the reference count can be forced down to zero
	       (with SVf_BREAK set).  In which case raising to 1 and then
	       dropping to 0 triggers cleanup before it should happen.  I
	       *think* that this might actually be a general, systematic,
	       weakness of the whole idea of SVf_BREAK, in that code *is*
	       allowed to raise and lower references during global destruction,
	       so any *valid* code that happens to do this during global
	       destruction might well trigger premature cleanup.  */
	    bool still_valid = gv && SvREFCNT(gv);

	    SensorCall(6435);if (still_valid)
		SvREFCNT_inc_simple_void(gv);
#ifdef USE_ITHREADS
	    SensorCall(6436);if (cPADOPo->op_padix > 0) {
		/* No GvIN_PAD_off(cGVOPo_gv) here, because other references
		 * may still exist on the pad */
		pad_swipe(cPADOPo->op_padix, TRUE);
		cPADOPo->op_padix = 0;
	    }
#else
	    SvREFCNT_dec(cSVOPo->op_sv);
	    cSVOPo->op_sv = NULL;
#endif
	    SensorCall(6439);if (still_valid) {
		SensorCall(6437);int try_downgrade = SvREFCNT(gv) == 2;
		SvREFCNT_dec(gv);
		SensorCall(6438);if (try_downgrade)
		    gv_try_downgrade(gv);
	    }
	}
	SensorCall(6440);break;
    case OP_METHOD_NAMED:
    case OP_CONST:
    case OP_HINTSEVAL:
	SvREFCNT_dec(cSVOPo->op_sv);
	cSVOPo->op_sv = NULL;
#ifdef USE_ITHREADS
	/** Bug #15654
	  Even if op_clear does a pad_free for the target of the op,
	  pad_free doesn't actually remove the sv that exists in the pad;
	  instead it lives on. This results in that it could be reused as 
	  a target later on when the pad was reallocated.
	**/
        SensorCall(6442);if(o->op_targ) {
          pad_swipe(o->op_targ,1);
          SensorCall(6441);o->op_targ = 0;
        }
#endif
	SensorCall(6443);break;
    case OP_GOTO:
    case OP_NEXT:
    case OP_LAST:
    case OP_REDO:
	SensorCall(6444);if (o->op_flags & (OPf_SPECIAL|OPf_STACKED|OPf_KIDS))
	    {/*327*/SensorCall(6445);break;/*328*/}
	/* FALL THROUGH */
    case OP_TRANS:
    case OP_TRANSR:
	SensorCall(6446);if (o->op_private & (OPpTRANS_FROM_UTF|OPpTRANS_TO_UTF)) {
#ifdef USE_ITHREADS
	    SensorCall(6447);if (cPADOPo->op_padix > 0) {
		pad_swipe(cPADOPo->op_padix, TRUE);
		cPADOPo->op_padix = 0;
	    }
#else
	    SvREFCNT_dec(cSVOPo->op_sv);
	    cSVOPo->op_sv = NULL;
#endif
	}
	else {
	    PerlMemShared_free(cPVOPo->op_pv);
	    cPVOPo->op_pv = NULL;
	}
	SensorCall(6448);break;
    case OP_SUBST:
	op_free(cPMOPo->op_pmreplrootu.op_pmreplroot);
	SensorCall(6449);goto clear_pmop;
    case OP_PUSHRE:
#ifdef USE_ITHREADS
        SensorCall(6450);if (cPMOPo->op_pmreplrootu.op_pmtargetoff) {
	    /* No GvIN_PAD_off here, because other references may still
	     * exist on the pad */
	    pad_swipe(cPMOPo->op_pmreplrootu.op_pmtargetoff, TRUE);
	}
#else
	SvREFCNT_dec(MUTABLE_SV(cPMOPo->op_pmreplrootu.op_pmtargetgv));
#endif
	/* FALL THROUGH */
    case OP_MATCH:
    case OP_QR:
clear_pmop:
	forget_pmop(cPMOPo, 1);
	cPMOPo->op_pmreplrootu.op_pmreplroot = NULL;
        /* we use the same protection as the "SAFE" version of the PM_ macros
         * here since sv_clean_all might release some PMOPs
         * after PL_regex_padav has been cleared
         * and the clearing of PL_regex_padav needs to
         * happen before sv_clean_all
         */
#ifdef USE_ITHREADS
	SensorCall(6452);if(PL_regex_pad) {        /* We could be in destruction */
	    SensorCall(6451);const IV offset = (cPMOPo)->op_pmoffset;
	    ReREFCNT_dec(PM_GETRE(cPMOPo));
	    PL_regex_pad[offset] = &PL_sv_undef;
            sv_catpvn_nomg(PL_regex_pad[0], (const char *)&offset,
			   sizeof(offset));
        }
#else
	ReREFCNT_dec(PM_GETRE(cPMOPo));
	PM_SETRE(cPMOPo, NULL);
#endif

	SensorCall(6453);break;
    }

    SensorCall(6456);if (o->op_targ > 0) {
	pad_free(o->op_targ);
	SensorCall(6455);o->op_targ = 0;
    }
SensorCall(6457);}

STATIC void
S_cop_free(pTHX_ COP* cop)
{
SensorCall(6458);    PERL_ARGS_ASSERT_COP_FREE;

    CopFILE_free(cop);
    CopSTASH_free(cop);
    SensorCall(6459);if (! specialWARN(cop->cop_warnings))
	PerlMemShared_free(cop->cop_warnings);
    cophh_free(CopHINTHASH_get(cop));
}

STATIC void
S_forget_pmop(pTHX_ PMOP *const o
#ifdef USE_ITHREADS
	      , U32 flags
#endif
	      )
{
    SensorCall(6460);HV * const pmstash = PmopSTASH(o);

    PERL_ARGS_ASSERT_FORGET_PMOP;

    SensorCall(6470);if (pmstash && !SvIS_FREED(pmstash) && SvMAGICAL(pmstash)) {
	SensorCall(6461);MAGIC * const mg = mg_find((const SV *)pmstash, PERL_MAGIC_symtab);
	SensorCall(6469);if (mg) {
	    SensorCall(6462);PMOP **const array = (PMOP**) mg->mg_ptr;
	    U32 count = mg->mg_len / sizeof(PMOP**);
	    U32 i = count;

	    SensorCall(6468);while (i--) {
		SensorCall(6463);if (array[i] == o) {
		    /* Found it. Move the entry at the end to overwrite it.  */
		    SensorCall(6464);array[i] = array[--count];
		    mg->mg_len = count * sizeof(PMOP**);
		    /* Could realloc smaller at this point always, but probably
		       not worth it. Probably worth free()ing if we're the
		       last.  */
		    SensorCall(6466);if(!count) {
			Safefree(mg->mg_ptr);
			SensorCall(6465);mg->mg_ptr = NULL;
		    }
		    SensorCall(6467);break;
		}
	    }
	}
    }
    SensorCall(6471);if (PL_curpm == o) 
	PL_curpm = NULL;
#ifdef USE_ITHREADS
    SensorCall(6472);if (flags)
	PmopSTASH_free(o);
#endif
SensorCall(6473);}

STATIC void
S_find_and_forget_pmops(pTHX_ OP *o)
{
    PERL_ARGS_ASSERT_FIND_AND_FORGET_PMOPS;

    SensorCall(6474);if (o->op_flags & OPf_KIDS) {
        SensorCall(6475);OP *kid = cUNOPo->op_first;
	SensorCall(6478);while (kid) {
	    SensorCall(6476);switch (kid->op_type) {
	    case OP_SUBST:
	    case OP_PUSHRE:
	    case OP_MATCH:
	    case OP_QR:
		forget_pmop((PMOP*)kid, 0);
	    }
	    find_and_forget_pmops(kid);
	    SensorCall(6477);kid = kid->op_sibling;
	}
    }
SensorCall(6479);}

void
Perl_op_null(pTHX_ OP *o)
{
SensorCall(6480);    dVAR;

    PERL_ARGS_ASSERT_OP_NULL;

    SensorCall(6482);if (o->op_type == OP_NULL)
	{/*399*/SensorCall(6481);return;/*400*/}
    SensorCall(6483);if (!PL_madskills)
	op_clear(o);
    SensorCall(6484);o->op_targ = o->op_type;
    o->op_type = OP_NULL;
    o->op_ppaddr = PL_ppaddr[OP_NULL];
SensorCall(6485);}

void
Perl_op_refcnt_lock(pTHX)
{
SensorCall(6486);    dVAR;
    PERL_UNUSED_CONTEXT;
    OP_REFCNT_LOCK;
}

void
Perl_op_refcnt_unlock(pTHX)
{
SensorCall(6487);    dVAR;
    PERL_UNUSED_CONTEXT;
    OP_REFCNT_UNLOCK;
}

/* Contextualizers */

/*
=for apidoc Am|OP *|op_contextualize|OP *o|I32 context

Applies a syntactic context to an op tree representing an expression.
I<o> is the op tree, and I<context> must be C<G_SCALAR>, C<G_ARRAY>,
or C<G_VOID> to specify the context to apply.  The modified op tree
is returned.

=cut
*/

OP *
Perl_op_contextualize(pTHX_ OP *o, I32 context)
{
    PERL_ARGS_ASSERT_OP_CONTEXTUALIZE;
    SensorCall(6488);switch (context) {
	case G_SCALAR: {OP * ReplaceReturn801 = scalar(o); SensorCall(6489); return ReplaceReturn801;}
	case G_ARRAY:  {OP * ReplaceReturn800 = list(o); SensorCall(6490); return ReplaceReturn800;}
	case G_VOID:   {OP * ReplaceReturn799 = scalarvoid(o); SensorCall(6491); return ReplaceReturn799;}
	default:
	    SensorCall(6492);Perl_croak(aTHX_ "panic: op_contextualize bad context %ld",
		       (long) context);
	    {OP * ReplaceReturn798 = o; SensorCall(6493); return ReplaceReturn798;}
    }
SensorCall(6494);}

/*
=head1 Optree Manipulation Functions

=for apidoc Am|OP*|op_linklist|OP *o
This function is the implementation of the L</LINKLIST> macro. It should
not be called directly.

=cut
*/

OP *
Perl_op_linklist(pTHX_ OP *o)
{
    SensorCall(6495);OP *first;

    PERL_ARGS_ASSERT_OP_LINKLIST;

    SensorCall(6497);if (o->op_next)
	{/*351*/{OP * ReplaceReturn797 = o->op_next; SensorCall(6496); return ReplaceReturn797;}/*352*/}

    /* establish postfix order */
    SensorCall(6498);first = cUNOPo->op_first;
    SensorCall(6506);if (first) {
        SensorCall(6499);register OP *kid;
	o->op_next = LINKLIST(first);
	kid = first;
	SensorCall(6504);for (;;) {
	    SensorCall(6500);if (kid->op_sibling) {
		SensorCall(6501);kid->op_next = LINKLIST(kid->op_sibling);
		kid = kid->op_sibling;
	    } else {
		SensorCall(6502);kid->op_next = o;
		SensorCall(6503);break;
	    }
	}
    }
    else
	{/*353*/SensorCall(6505);o->op_next = o;/*354*/}

    {OP * ReplaceReturn796 = o->op_next; SensorCall(6507); return ReplaceReturn796;}
}

static OP *
S_scalarkids(pTHX_ OP *o)
{
    SensorCall(6508);if (o && o->op_flags & OPf_KIDS) {
        SensorCall(6509);OP *kid;
	SensorCall(6510);for (kid = cLISTOPo->op_first; kid; kid = kid->op_sibling)
	    scalar(kid);
    }
    {OP * ReplaceReturn795 = o; SensorCall(6511); return ReplaceReturn795;}
}

STATIC OP *
S_scalarboolean(pTHX_ OP *o)
{
SensorCall(6512);    dVAR;

    PERL_ARGS_ASSERT_SCALARBOOLEAN;

    SensorCall(6517);if (o->op_type == OP_SASSIGN && cBINOPo->op_first->op_type == OP_CONST
     && !(cBINOPo->op_first->op_flags & OPf_SPECIAL)) {
	SensorCall(6513);if (ckWARN(WARN_SYNTAX)) {
	    SensorCall(6514);const line_t oldline = CopLINE(PL_curcop);

	    SensorCall(6515);if (PL_parser && PL_parser->copline != NOLINE)
		CopLINE_set(PL_curcop, PL_parser->copline);
	    SensorCall(6516);Perl_warner(aTHX_ packWARN(WARN_SYNTAX), "Found = in conditional, should be ==");
	    CopLINE_set(PL_curcop, oldline);
	}
    }
    {OP * ReplaceReturn794 = scalar(o); SensorCall(6518); return ReplaceReturn794;}
}

OP *
Perl_scalar(pTHX_ OP *o)
{
SensorCall(6519);    dVAR;
    OP *kid;

    /* assumes no premature commitment */
    SensorCall(6521);if (!o || (PL_parser && PL_parser->error_count)
	 || (o->op_flags & OPf_WANT)
	 || o->op_type == OP_RETURN)
    {
	{OP * ReplaceReturn793 = o; SensorCall(6520); return ReplaceReturn793;}
    }

    SensorCall(6522);o->op_flags = (o->op_flags & ~OPf_WANT) | OPf_WANT_SCALAR;

    SensorCall(6539);switch (o->op_type) {
    case OP_REPEAT:
	scalar(cBINOPo->op_first);
	SensorCall(6523);break;
    case OP_OR:
    case OP_AND:
    case OP_COND_EXPR:
	SensorCall(6524);for (kid = cUNOPo->op_first->op_sibling; kid; kid = kid->op_sibling)
	    scalar(kid);
	SensorCall(6525);break;
	/* FALL THROUGH */
    case OP_SPLIT:
    case OP_MATCH:
    case OP_QR:
    case OP_SUBST:
    case OP_NULL:
    default:
	SensorCall(6526);if (o->op_flags & OPf_KIDS) {
	    SensorCall(6527);for (kid = cUNOPo->op_first; kid; kid = kid->op_sibling)
		scalar(kid);
	}
	SensorCall(6528);break;
    case OP_LEAVE:
    case OP_LEAVETRY:
	SensorCall(6529);kid = cLISTOPo->op_first;
	scalar(kid);
	kid = kid->op_sibling;
    do_kids:
	SensorCall(6533);while (kid) {
	    SensorCall(6530);OP *sib = kid->op_sibling;
	    SensorCall(6531);if (sib && kid->op_type != OP_LEAVEWHEN)
		scalarvoid(kid);
	    else
		scalar(kid);
	    SensorCall(6532);kid = sib;
	}
	PL_curcop = &PL_compiling;
	SensorCall(6534);break;
    case OP_SCOPE:
    case OP_LINESEQ:
    case OP_LIST:
	SensorCall(6535);kid = cLISTOPo->op_first;
	SensorCall(6536);goto do_kids;
    case OP_SORT:
	SensorCall(6537);Perl_ck_warner(aTHX_ packWARN(WARN_VOID), "Useless use of sort in scalar context");
	SensorCall(6538);break;
    }
    {OP * ReplaceReturn792 = o; SensorCall(6540); return ReplaceReturn792;}
}

OP *
Perl_scalarvoid(pTHX_ OP *o)
{
SensorCall(6541);    dVAR;
    OP *kid;
    const char* useless = NULL;
    U32 useless_is_utf8 = 0;
    SV* sv;
    U8 want;

    PERL_ARGS_ASSERT_SCALARVOID;

    /* trailing mad null ops don't count as "there" for void processing */
    SensorCall(6546);if (PL_madskills &&
    	o->op_type != OP_NULL &&
	o->op_sibling &&
	o->op_sibling->op_type == OP_NULL)
    {
	SensorCall(6542);OP *sib;
	SensorCall(6543);for (sib = o->op_sibling;
		sib && sib->op_type == OP_NULL;
		sib = sib->op_sibling) ;
	
	SensorCall(6545);if (!sib)
	    {/*475*/{OP * ReplaceReturn791 = o; SensorCall(6544); return ReplaceReturn791;}/*476*/}
    }

    SensorCall(6547);if (o->op_type == OP_NEXTSTATE
	|| o->op_type == OP_DBSTATE
	|| (o->op_type == OP_NULL && (o->op_targ == OP_NEXTSTATE
				      || o->op_targ == OP_DBSTATE)))
	PL_curcop = (COP*)o;		/* for warning below */

    /* assumes no premature commitment */
    SensorCall(6548);want = o->op_flags & OPf_WANT;
    SensorCall(6550);if ((want && want != OPf_WANT_SCALAR)
	 || (PL_parser && PL_parser->error_count)
	 || o->op_type == OP_RETURN || o->op_type == OP_REQUIRE || o->op_type == OP_LEAVEWHEN)
    {
	{OP * ReplaceReturn790 = o; SensorCall(6549); return ReplaceReturn790;}
    }

    SensorCall(6552);if ((o->op_private & OPpTARGET_MY)
	&& (PL_opargs[o->op_type] & OA_TARGLEX))/* OPp share the meaning */
    {
	{OP * ReplaceReturn789 = scalar(o); SensorCall(6551); return ReplaceReturn789;}			/* As if inside SASSIGN */
    }

    SensorCall(6553);o->op_flags = (o->op_flags & ~OPf_WANT) | OPf_WANT_VOID;

    SensorCall(6635);switch (o->op_type) {
    default:
	SensorCall(6554);if (!(PL_opargs[o->op_type] & OA_FOLDCONST))
	    {/*477*/SensorCall(6555);break;/*478*/}
	/* FALL THROUGH */
    case OP_REPEAT:
	SensorCall(6556);if (o->op_flags & OPf_STACKED)
	    {/*479*/SensorCall(6557);break;/*480*/}
	SensorCall(6558);goto func_ops;
    case OP_SUBSTR:
	SensorCall(6559);if (o->op_private == 4)
	    {/*481*/SensorCall(6560);break;/*482*/}
	/* FALL THROUGH */
    case OP_GVSV:
    case OP_WANTARRAY:
    case OP_GV:
    case OP_SMARTMATCH:
    case OP_PADSV:
    case OP_PADAV:
    case OP_PADHV:
    case OP_PADANY:
    case OP_AV2ARYLEN:
    case OP_REF:
    case OP_REFGEN:
    case OP_SREFGEN:
    case OP_DEFINED:
    case OP_HEX:
    case OP_OCT:
    case OP_LENGTH:
    case OP_VEC:
    case OP_INDEX:
    case OP_RINDEX:
    case OP_SPRINTF:
    case OP_AELEM:
    case OP_AELEMFAST:
    case OP_AELEMFAST_LEX:
    case OP_ASLICE:
    case OP_HELEM:
    case OP_HSLICE:
    case OP_UNPACK:
    case OP_PACK:
    case OP_JOIN:
    case OP_LSLICE:
    case OP_ANONLIST:
    case OP_ANONHASH:
    case OP_SORT:
    case OP_REVERSE:
    case OP_RANGE:
    case OP_FLIP:
    case OP_FLOP:
    case OP_CALLER:
    case OP_FILENO:
    case OP_EOF:
    case OP_TELL:
    case OP_GETSOCKNAME:
    case OP_GETPEERNAME:
    case OP_READLINK:
    case OP_TELLDIR:
    case OP_GETPPID:
    case OP_GETPGRP:
    case OP_GETPRIORITY:
    case OP_TIME:
    case OP_TMS:
    case OP_LOCALTIME:
    case OP_GMTIME:
    case OP_GHBYNAME:
    case OP_GHBYADDR:
    case OP_GHOSTENT:
    case OP_GNBYNAME:
    case OP_GNBYADDR:
    case OP_GNETENT:
    case OP_GPBYNAME:
    case OP_GPBYNUMBER:
    case OP_GPROTOENT:
    case OP_GSBYNAME:
    case OP_GSBYPORT:
    case OP_GSERVENT:
    case OP_GPWNAM:
    case OP_GPWUID:
    case OP_GGRNAM:
    case OP_GGRGID:
    case OP_GETLOGIN:
    case OP_PROTOTYPE:
    case OP_RUNCV:
      func_ops:
	SensorCall(6561);if (!(o->op_private & (OPpLVAL_INTRO|OPpOUR_INTRO)))
	    /* Otherwise it's "Useless use of grep iterator" */
	    useless = OP_DESC(o);
	SensorCall(6562);break;

    case OP_SPLIT:
	SensorCall(6563);kid = cLISTOPo->op_first;
	SensorCall(6564);if (kid && kid->op_type == OP_PUSHRE
#ifdef USE_ITHREADS
		&& !((PMOP*)kid)->op_pmreplrootu.op_pmtargetoff)
#else
		&& !((PMOP*)kid)->op_pmreplrootu.op_pmtargetgv)
#endif
	    useless = OP_DESC(o);
	SensorCall(6565);break;

    case OP_NOT:
       SensorCall(6566);kid = cUNOPo->op_first;
       SensorCall(6568);if (kid->op_type != OP_MATCH && kid->op_type != OP_SUBST &&
           kid->op_type != OP_TRANS && kid->op_type != OP_TRANSR) {
	        SensorCall(6567);goto func_ops;
       }
       SensorCall(6569);useless = "negative pattern binding (!~)";
       SensorCall(6570);break;

    case OP_SUBST:
	SensorCall(6571);if (cPMOPo->op_pmflags & PMf_NONDESTRUCT)
	    {/*483*/SensorCall(6572);useless = "non-destructive substitution (s///r)";/*484*/}
	SensorCall(6573);break;

    case OP_TRANSR:
	SensorCall(6574);useless = "non-destructive transliteration (tr///r)";
	SensorCall(6575);break;

    case OP_RV2GV:
    case OP_RV2SV:
    case OP_RV2AV:
    case OP_RV2HV:
	SensorCall(6576);if (!(o->op_private & (OPpLVAL_INTRO|OPpOUR_INTRO)) &&
		(!o->op_sibling || o->op_sibling->op_type != OP_READLINE))
	    {/*485*/SensorCall(6577);useless = "a variable";/*486*/}
	SensorCall(6578);break;

    case OP_CONST:
	SensorCall(6579);sv = cSVOPo_sv;
	SensorCall(6590);if (cSVOPo->op_private & OPpCONST_STRICT)
	    no_bareword_allowed(o);
	else {
	    SensorCall(6580);if (ckWARN(WARN_VOID)) {
		/* don't warn on optimised away booleans, eg 
		 * use constant Foo, 5; Foo || print; */
		SensorCall(6581);if (cSVOPo->op_private & OPpCONST_SHORTCIRCUIT)
		    useless = NULL;
		/* the constants 0 and 1 are permitted as they are
		   conventionally used as dummies in constructs like
		        1 while some_condition_with_side_effects;  */
		else {/*487*/SensorCall(6582);if (SvNIOK(sv) && (SvNV(sv) == 0.0 || SvNV(sv) == 1.0))
		    useless = NULL;
		else {/*489*/SensorCall(6583);if (SvPOK(sv)) {
                  /* perl4's way of mixing documentation and code
                     (before the invention of POD) was based on a
                     trick to mix nroff and perl code. The trick was
                     built upon these three nroff macros being used in
                     void context. The pink camel has the details in
                     the script wrapman near page 319. */
		    SensorCall(6584);const char * const maybe_macro = SvPVX_const(sv);
		    SensorCall(6586);if (strnEQ(maybe_macro, "di", 2) ||
			strnEQ(maybe_macro, "ds", 2) ||
			strnEQ(maybe_macro, "ig", 2))
			    useless = NULL;
		    else {
			SensorCall(6585);SV * const dsv = newSVpvs("");
			SV* msv = sv_2mortal(Perl_newSVpvf(aTHX_
				    "a constant (%s)",
				    pv_pretty(dsv, maybe_macro, SvCUR(sv), 32, NULL, NULL,
					    PERL_PV_PRETTY_DUMP | PERL_PV_ESCAPE_NOCLEAR | PERL_PV_ESCAPE_UNI_DETECT )));
			SvREFCNT_dec(dsv);
			useless = SvPV_nolen(msv);
			useless_is_utf8 = SvUTF8(msv);
		    }
		}
		else {/*491*/SensorCall(6587);if (SvOK(sv)) {
		    SensorCall(6588);SV* msv = sv_2mortal(Perl_newSVpvf(aTHX_
				"a constant (%"SVf")", sv));
		    useless = SvPV_nolen(msv);
		}
		else
		    {/*493*/SensorCall(6589);useless = "a constant (undef)";/*494*/}/*492*/}/*490*/}/*488*/}
	    }
	}
	op_null(o);		/* don't execute or even remember it */
	SensorCall(6591);break;

    case OP_POSTINC:
	SensorCall(6592);o->op_type = OP_PREINC;		/* pre-increment is faster */
	o->op_ppaddr = PL_ppaddr[OP_PREINC];
	SensorCall(6593);break;

    case OP_POSTDEC:
	SensorCall(6594);o->op_type = OP_PREDEC;		/* pre-decrement is faster */
	o->op_ppaddr = PL_ppaddr[OP_PREDEC];
	SensorCall(6595);break;

    case OP_I_POSTINC:
	SensorCall(6596);o->op_type = OP_I_PREINC;	/* pre-increment is faster */
	o->op_ppaddr = PL_ppaddr[OP_I_PREINC];
	SensorCall(6597);break;

    case OP_I_POSTDEC:
	SensorCall(6598);o->op_type = OP_I_PREDEC;	/* pre-decrement is faster */
	o->op_ppaddr = PL_ppaddr[OP_I_PREDEC];
	SensorCall(6599);break;

    case OP_SASSIGN: {
	SensorCall(6600);OP *rv2gv;
	UNOP *refgen, *rv2cv;
	LISTOP *exlist;

	SensorCall(6602);if ((o->op_private & ~OPpASSIGN_BACKWARDS) != 2)
	    {/*495*/SensorCall(6601);break;/*496*/}

	SensorCall(6603);rv2gv = ((BINOP *)o)->op_last;
	SensorCall(6605);if (!rv2gv || rv2gv->op_type != OP_RV2GV)
	    {/*497*/SensorCall(6604);break;/*498*/}

	SensorCall(6606);refgen = (UNOP *)((BINOP *)o)->op_first;

	SensorCall(6608);if (!refgen || refgen->op_type != OP_REFGEN)
	    {/*499*/SensorCall(6607);break;/*500*/}

	SensorCall(6609);exlist = (LISTOP *)refgen->op_first;
	SensorCall(6611);if (!exlist || exlist->op_type != OP_NULL
	    || exlist->op_targ != OP_LIST)
	    {/*501*/SensorCall(6610);break;/*502*/}

	SensorCall(6613);if (exlist->op_first->op_type != OP_PUSHMARK)
	    {/*503*/SensorCall(6612);break;/*504*/}

	SensorCall(6614);rv2cv = (UNOP*)exlist->op_last;

	SensorCall(6616);if (rv2cv->op_type != OP_RV2CV)
	    {/*505*/SensorCall(6615);break;/*506*/}

	assert ((rv2gv->op_private & OPpDONT_INIT_GV) == 0);
	assert ((o->op_private & OPpASSIGN_CV_TO_GV) == 0);
	assert ((rv2cv->op_private & OPpMAY_RETURN_CONSTANT) == 0);

	SensorCall(6617);o->op_private |= OPpASSIGN_CV_TO_GV;
	rv2gv->op_private |= OPpDONT_INIT_GV;
	rv2cv->op_private |= OPpMAY_RETURN_CONSTANT;

	SensorCall(6618);break;
    }

    case OP_AASSIGN: {
	inplace_aassign(o);
	SensorCall(6619);break;
    }

    case OP_OR:
    case OP_AND:
	SensorCall(6620);kid = cLOGOPo->op_first;
	SensorCall(6624);if (kid->op_type == OP_NOT
	    && (kid->op_flags & OPf_KIDS)
	    && !PL_madskills) {
	    SensorCall(6621);if (o->op_type == OP_AND) {
		SensorCall(6622);o->op_type = OP_OR;
		o->op_ppaddr = PL_ppaddr[OP_OR];
	    } else {
		SensorCall(6623);o->op_type = OP_AND;
		o->op_ppaddr = PL_ppaddr[OP_AND];
	    }
	    op_null(kid);
	}

    case OP_DOR:
    case OP_COND_EXPR:
    case OP_ENTERGIVEN:
    case OP_ENTERWHEN:
	SensorCall(6625);for (kid = cUNOPo->op_first->op_sibling; kid; kid = kid->op_sibling)
	    scalarvoid(kid);
	SensorCall(6626);break;

    case OP_NULL:
	SensorCall(6627);if (o->op_flags & OPf_STACKED)
	    {/*507*/SensorCall(6628);break;/*508*/}
	/* FALL THROUGH */
    case OP_NEXTSTATE:
    case OP_DBSTATE:
    case OP_ENTERTRY:
    case OP_ENTER:
	SensorCall(6629);if (!(o->op_flags & OPf_KIDS))
	    {/*509*/SensorCall(6630);break;/*510*/}
	/* FALL THROUGH */
    case OP_SCOPE:
    case OP_LEAVE:
    case OP_LEAVETRY:
    case OP_LEAVELOOP:
    case OP_LINESEQ:
    case OP_LIST:
    case OP_LEAVEGIVEN:
    case OP_LEAVEWHEN:
	SensorCall(6631);for (kid = cLISTOPo->op_first; kid; kid = kid->op_sibling)
	    scalarvoid(kid);
	SensorCall(6632);break;
    case OP_ENTEREVAL:
	scalarkids(o);
	SensorCall(6633);break;
    case OP_SCALAR:
	{OP * ReplaceReturn788 = scalar(o); SensorCall(6634); return ReplaceReturn788;}
    }
    SensorCall(6637);if (useless)
       {/*511*/SensorCall(6636);Perl_ck_warner(aTHX_ packWARN(WARN_VOID), "Useless use of %"SVf" in void context",
                       newSVpvn_flags(useless, strlen(useless),
                            SVs_TEMP | ( useless_is_utf8 ? SVf_UTF8 : 0 )));/*512*/}
    {OP * ReplaceReturn787 = o; SensorCall(6638); return ReplaceReturn787;}
}

static OP *
S_listkids(pTHX_ OP *o)
{
    SensorCall(6639);if (o && o->op_flags & OPf_KIDS) {
        SensorCall(6640);OP *kid;
	SensorCall(6641);for (kid = cLISTOPo->op_first; kid; kid = kid->op_sibling)
	    list(kid);
    }
    {OP * ReplaceReturn786 = o; SensorCall(6642); return ReplaceReturn786;}
}

OP *
Perl_list(pTHX_ OP *o)
{
SensorCall(6643);    dVAR;
    OP *kid;

    /* assumes no premature commitment */
    SensorCall(6645);if (!o || (o->op_flags & OPf_WANT)
	 || (PL_parser && PL_parser->error_count)
	 || o->op_type == OP_RETURN)
    {
	{OP * ReplaceReturn785 = o; SensorCall(6644); return ReplaceReturn785;}
    }

    SensorCall(6647);if ((o->op_private & OPpTARGET_MY)
	&& (PL_opargs[o->op_type] & OA_TARGLEX))/* OPp share the meaning */
    {
	{OP * ReplaceReturn784 = o; SensorCall(6646); return ReplaceReturn784;}				/* As if inside SASSIGN */
    }

    SensorCall(6648);o->op_flags = (o->op_flags & ~OPf_WANT) | OPf_WANT_LIST;

    SensorCall(6665);switch (o->op_type) {
    case OP_FLOP:
    case OP_REPEAT:
	list(cBINOPo->op_first);
	SensorCall(6649);break;
    case OP_OR:
    case OP_AND:
    case OP_COND_EXPR:
	SensorCall(6650);for (kid = cUNOPo->op_first->op_sibling; kid; kid = kid->op_sibling)
	    list(kid);
	SensorCall(6651);break;
    default:
    case OP_MATCH:
    case OP_QR:
    case OP_SUBST:
    case OP_NULL:
	SensorCall(6652);if (!(o->op_flags & OPf_KIDS))
	    {/*225*/SensorCall(6653);break;/*226*/}
	SensorCall(6655);if (!o->op_next && cUNOPo->op_first->op_type == OP_FLOP) {
	    list(cBINOPo->op_first);
	    {OP * ReplaceReturn783 = gen_constant_list(o); SensorCall(6654); return ReplaceReturn783;}
	}
    case OP_LIST:
	listkids(o);
	SensorCall(6656);break;
    case OP_LEAVE:
    case OP_LEAVETRY:
	SensorCall(6657);kid = cLISTOPo->op_first;
	list(kid);
	kid = kid->op_sibling;
    do_kids:
	SensorCall(6661);while (kid) {
	    SensorCall(6658);OP *sib = kid->op_sibling;
	    SensorCall(6659);if (sib && kid->op_type != OP_LEAVEWHEN)
		scalarvoid(kid);
	    else
		list(kid);
	    SensorCall(6660);kid = sib;
	}
	PL_curcop = &PL_compiling;
	SensorCall(6662);break;
    case OP_SCOPE:
    case OP_LINESEQ:
	SensorCall(6663);kid = cLISTOPo->op_first;
	SensorCall(6664);goto do_kids;
    }
    {OP * ReplaceReturn782 = o; SensorCall(6666); return ReplaceReturn782;}
}

static OP *
S_scalarseq(pTHX_ OP *o)
{
SensorCall(6667);    dVAR;
    SensorCall(6675);if (o) {
	SensorCall(6668);const OPCODE type = o->op_type;

	SensorCall(6672);if (type == OP_LINESEQ || type == OP_SCOPE ||
	    type == OP_LEAVE || type == OP_LEAVETRY)
	{
            SensorCall(6669);OP *kid;
	    SensorCall(6671);for (kid = cLISTOPo->op_first; kid; kid = kid->op_sibling) {
		SensorCall(6670);if (kid->op_sibling) {
		    scalarvoid(kid);
		}
	    }
	    PL_curcop = &PL_compiling;
	}
	SensorCall(6673);o->op_flags &= ~OPf_PARENS;
	SensorCall(6674);if (PL_hints & HINT_BLOCK_SCOPE)
	    o->op_flags |= OPf_PARENS;
    }
    else
	o = newOP(OP_STUB, 0);
    {OP * ReplaceReturn781 = o; SensorCall(6676); return ReplaceReturn781;}
}

STATIC OP *
S_modkids(pTHX_ OP *o, I32 type)
{
    SensorCall(6677);if (o && o->op_flags & OPf_KIDS) {
        SensorCall(6678);OP *kid;
	SensorCall(6679);for (kid = cLISTOPo->op_first; kid; kid = kid->op_sibling)
	    op_lvalue(kid, type);
    }
    {OP * ReplaceReturn780 = o; SensorCall(6680); return ReplaceReturn780;}
}

/*
=for apidoc finalize_optree

This function finalizes the optree. Should be called directly after
the complete optree is built. It does some additional
checking which can't be done in the normal ck_xxx functions and makes
the tree thread-safe.

=cut
*/
void
Perl_finalize_optree(pTHX_ OP* o)
{
    PERL_ARGS_ASSERT_FINALIZE_OPTREE;

    ENTER;
    SAVEVPTR(PL_curcop);

    finalize_op(o);

    LEAVE;
}

STATIC void
S_finalize_op(pTHX_ OP* o)
{
    PERL_ARGS_ASSERT_FINALIZE_OP;

#if defined(PERL_MAD) && defined(USE_ITHREADS)
    {
	/* Make sure mad ops are also thread-safe */
	MADPROP *mp = o->op_madprop;
	while (mp) {
	    if (mp->mad_type == MAD_OP && mp->mad_vlen) {
		OP *prop_op = (OP *) mp->mad_val;
		/* We only need "Relocate sv to the pad for thread safety.", but this
		   easiest way to make sure it traverses everything */
		if (prop_op->op_type == OP_CONST)
		    cSVOPx(prop_op)->op_private &= ~OPpCONST_STRICT;
		finalize_op(prop_op);
	    }
	    mp = mp->mad_next;
	}
    }
#endif

    SensorCall(6681);switch (o->op_type) {
    case OP_NEXTSTATE:
    case OP_DBSTATE:
	PL_curcop = ((COP*)o);		/* for warnings */
	SensorCall(6682);break;
    case OP_EXEC:
	SensorCall(6683);if ( o->op_sibling
	    && (o->op_sibling->op_type == OP_NEXTSTATE || o->op_sibling->op_type == OP_DBSTATE)
	    && ckWARN(WARN_SYNTAX))
	    {
		SensorCall(6684);if (o->op_sibling->op_sibling) {
		    SensorCall(6685);const OPCODE type = o->op_sibling->op_sibling->op_type;
		    SensorCall(6687);if (type != OP_EXIT && type != OP_WARN && type != OP_DIE) {
			SensorCall(6686);const line_t oldline = CopLINE(PL_curcop);
			CopLINE_set(PL_curcop, CopLINE((COP*)o->op_sibling));
			Perl_warner(aTHX_ packWARN(WARN_EXEC),
			    "Statement unlikely to be reached");
			Perl_warner(aTHX_ packWARN(WARN_EXEC),
			    "\t(Maybe you meant system() when you said exec()?)\n");
			CopLINE_set(PL_curcop, oldline);
		    }
		}
	    }
	SensorCall(6688);break;

    case OP_GV:
	SensorCall(6689);if ((o->op_private & OPpEARLY_CV) && ckWARN(WARN_PROTOTYPE)) {
	    SensorCall(6690);GV * const gv = cGVOPo_gv;
	    SensorCall(6692);if (SvTYPE(gv) == SVt_PVGV && GvCV(gv) && SvPVX_const(GvCV(gv))) {
		/* XXX could check prototype here instead of just carping */
		SensorCall(6691);SV * const sv = sv_newmortal();
		gv_efullname3(sv, gv, NULL);
		Perl_warner(aTHX_ packWARN(WARN_PROTOTYPE),
		    "%"SVf"() called too early to check prototype",
		    SVfARG(sv));
	    }
	}
	SensorCall(6693);break;

    case OP_CONST:
	SensorCall(6694);if (cSVOPo->op_private & OPpCONST_STRICT)
	    no_bareword_allowed(o);
	/* FALLTHROUGH */
#ifdef USE_ITHREADS
    case OP_HINTSEVAL:
    case OP_METHOD_NAMED:
	/* Relocate sv to the pad for thread safety.
	 * Despite being a "constant", the SV is written to,
	 * for reference counts, sv_upgrade() etc. */
	SensorCall(6695);if (cSVOPo->op_sv) {
	    SensorCall(6696);const PADOFFSET ix = pad_alloc(OP_CONST, SVs_PADTMP);
	    SensorCall(6698);if (o->op_type != OP_METHOD_NAMED &&
		(SvPADTMP(cSVOPo->op_sv) || SvPADMY(cSVOPo->op_sv)))
	    {
		/* If op_sv is already a PADTMP/MY then it is being used by
		 * some pad, so make a copy. */
		sv_setsv(PAD_SVl(ix),cSVOPo->op_sv);
		SvREADONLY_on(PAD_SVl(ix));
		SvREFCNT_dec(cSVOPo->op_sv);
	    }
	    else {/*529*/SensorCall(6697);if (o->op_type != OP_METHOD_NAMED
		&& cSVOPo->op_sv == &PL_sv_undef) {
		/* PL_sv_undef is hack - it's unsafe to store it in the
		   AV that is the pad, because av_fetch treats values of
		   PL_sv_undef as a "free" AV entry and will merrily
		   replace them with a new SV, causing pad_alloc to think
		   that this pad slot is free. (When, clearly, it is not)
		*/
		SvOK_off(PAD_SVl(ix));
		SvPADTMP_on(PAD_SVl(ix));
		SvREADONLY_on(PAD_SVl(ix));
	    }
	    else {
		SvREFCNT_dec(PAD_SVl(ix));
		SvPADTMP_on(cSVOPo->op_sv);
		PAD_SETSV(ix, cSVOPo->op_sv);
		/* XXX I don't know how this isn't readonly already. */
		SvREADONLY_on(PAD_SVl(ix));
	    ;/*530*/}}
	    cSVOPo->op_sv = NULL;
	    SensorCall(6699);o->op_targ = ix;
	}
#endif
	SensorCall(6700);break;

    case OP_HELEM: {
	SensorCall(6701);UNOP *rop;
	SV *lexname;
	GV **fields;
	SV **svp, *sv;
	const char *key = NULL;
	STRLEN keylen;

	SensorCall(6703);if (((BINOP*)o)->op_last->op_type != OP_CONST)
	    {/*531*/SensorCall(6702);break;/*532*/}

	/* Make the CONST have a shared SV */
	SensorCall(6704);svp = cSVOPx_svp(((BINOP*)o)->op_last);
	SensorCall(6706);if ((!SvFAKE(sv = *svp) || !SvREADONLY(sv))
	    && SvTYPE(sv) < SVt_PVMG && !SvROK(sv)) {
	    SensorCall(6705);key = SvPV_const(sv, keylen);
	    lexname = newSVpvn_share(key,
		SvUTF8(sv) ? -(I32)keylen : (I32)keylen,
		0);
	    SvREFCNT_dec(sv);
	    *svp = lexname;
	}

	SensorCall(6708);if ((o->op_private & (OPpLVAL_INTRO)))
	    {/*533*/SensorCall(6707);break;/*534*/}

	SensorCall(6709);rop = (UNOP*)((BINOP*)o)->op_first;
	SensorCall(6711);if (rop->op_type != OP_RV2HV || rop->op_first->op_type != OP_PADSV)
	    {/*535*/SensorCall(6710);break;/*536*/}
	SensorCall(6712);lexname = *av_fetch(PL_comppad_name, rop->op_first->op_targ, TRUE);
	SensorCall(6714);if (!SvPAD_TYPED(lexname))
	    {/*537*/SensorCall(6713);break;/*538*/}
	SensorCall(6715);fields = (GV**)hv_fetchs(SvSTASH(lexname), "FIELDS", FALSE);
	SensorCall(6717);if (!fields || !GvHV(*fields))
	    {/*539*/SensorCall(6716);break;/*540*/}
	SensorCall(6718);key = SvPV_const(*svp, keylen);
	SensorCall(6720);if (!hv_fetch(GvHV(*fields), key,
		SvUTF8(*svp) ? -(I32)keylen : (I32)keylen, FALSE)) {
	    SensorCall(6719);Perl_croak(aTHX_ "No such class field \"%"SVf"\" " 
			   "in variable %"SVf" of type %"HEKf, 
		      SVfARG(*svp), SVfARG(lexname),
                      HEKfARG(HvNAME_HEK(SvSTASH(lexname))));
	}
	SensorCall(6721);break;
    }

    case OP_HSLICE: {
	SensorCall(6722);UNOP *rop;
	SV *lexname;
	GV **fields;
	SV **svp;
	const char *key;
	STRLEN keylen;
	SVOP *first_key_op, *key_op;

	SensorCall(6724);if ((o->op_private & (OPpLVAL_INTRO))
	    /* I bet there's always a pushmark... */
	    || ((LISTOP*)o)->op_first->op_sibling->op_type != OP_LIST)
	    /* hmmm, no optimization if list contains only one key. */
	    {/*541*/SensorCall(6723);break;/*542*/}
	SensorCall(6725);rop = (UNOP*)((LISTOP*)o)->op_last;
	SensorCall(6727);if (rop->op_type != OP_RV2HV)
	    {/*543*/SensorCall(6726);break;/*544*/}
	SensorCall(6732);if (rop->op_first->op_type == OP_PADSV)
	    /* @$hash{qw(keys here)} */
	    {/*545*/SensorCall(6728);rop = (UNOP*)rop->op_first;/*546*/}
	else {
	    /* @{$hash}{qw(keys here)} */
	    SensorCall(6729);if (rop->op_first->op_type == OP_SCOPE
		&& cLISTOPx(rop->op_first)->op_last->op_type == OP_PADSV)
		{
		    SensorCall(6730);rop = (UNOP*)cLISTOPx(rop->op_first)->op_last;
		}
	    else
		{/*547*/SensorCall(6731);break;/*548*/}
	}

	SensorCall(6733);lexname = *av_fetch(PL_comppad_name, rop->op_targ, TRUE);
	SensorCall(6735);if (!SvPAD_TYPED(lexname))
	    {/*549*/SensorCall(6734);break;/*550*/}
	SensorCall(6736);fields = (GV**)hv_fetchs(SvSTASH(lexname), "FIELDS", FALSE);
	SensorCall(6738);if (!fields || !GvHV(*fields))
	    {/*551*/SensorCall(6737);break;/*552*/}
	/* Again guessing that the pushmark can be jumped over.... */
	SensorCall(6739);first_key_op = (SVOP*)((LISTOP*)((LISTOP*)o)->op_first->op_sibling)
	    ->op_first->op_sibling;
	SensorCall(6745);for (key_op = first_key_op; key_op;
	     key_op = (SVOP*)key_op->op_sibling) {
	    SensorCall(6740);if (key_op->op_type != OP_CONST)
		{/*553*/SensorCall(6741);continue;/*554*/}
	    SensorCall(6742);svp = cSVOPx_svp(key_op);
	    key = SvPV_const(*svp, keylen);
	    SensorCall(6744);if (!hv_fetch(GvHV(*fields), key,
		    SvUTF8(*svp) ? -(I32)keylen : (I32)keylen, FALSE)) {
		SensorCall(6743);Perl_croak(aTHX_ "No such class field \"%"SVf"\" " 
			   "in variable %"SVf" of type %"HEKf, 
		      SVfARG(*svp), SVfARG(lexname),
                      HEKfARG(HvNAME_HEK(SvSTASH(lexname))));
	    }
	}
	SensorCall(6746);break;
    }
    case OP_SUBST: {
	SensorCall(6747);if (cPMOPo->op_pmreplrootu.op_pmreplroot)
	    finalize_op(cPMOPo->op_pmreplrootu.op_pmreplroot);
	SensorCall(6748);break;
    }
    default:
	SensorCall(6749);break;
    }

    SensorCall(6752);if (o->op_flags & OPf_KIDS) {
	SensorCall(6750);OP *kid;
	SensorCall(6751);for (kid = cUNOPo->op_first; kid; kid = kid->op_sibling)
	    finalize_op(kid);
    }
SensorCall(6753);}

/*
=for apidoc Amx|OP *|op_lvalue|OP *o|I32 type

Propagate lvalue ("modifiable") context to an op and its children.
I<type> represents the context type, roughly based on the type of op that
would do the modifying, although C<local()> is represented by OP_NULL,
because it has no op type of its own (it is signalled by a flag on
the lvalue op).

This function detects things that can't be modified, such as C<$x+1>, and
generates errors for them. For example, C<$x+1 = 2> would cause it to be
called with an op of type OP_ADD and a C<type> argument of OP_SASSIGN.

It also flags things that need to behave specially in an lvalue context,
such as C<$$x = 5> which might have to vivify a reference in C<$x>.

=cut
*/

OP *
Perl_op_lvalue_flags(pTHX_ OP *o, I32 type, U32 flags)
{
SensorCall(6754);    dVAR;
    OP *kid;
    /* -1 = error on localize, 0 = ignore localize, 1 = ok to localize */
    int localize = -1;

    SensorCall(6756);if (!o || (PL_parser && PL_parser->error_count))
	{/*355*/{OP * ReplaceReturn779 = o; SensorCall(6755); return ReplaceReturn779;}/*356*/}

    SensorCall(6758);if ((o->op_private & OPpTARGET_MY)
	&& (PL_opargs[o->op_type] & OA_TARGLEX))/* OPp share the meaning */
    {
	{OP * ReplaceReturn778 = o; SensorCall(6757); return ReplaceReturn778;}
    }

    assert( (o->op_flags & OPf_WANT) != OPf_WANT_VOID );

    SensorCall(6760);if (type == OP_PRTF || type == OP_SPRINTF) {/*357*/SensorCall(6759);type = OP_ENTERSUB;/*358*/}

    SensorCall(6856);switch (o->op_type) {
    case OP_UNDEF:
	SensorCall(6761);localize = 0;
	PL_modcount++;
	{OP * ReplaceReturn777 = o; SensorCall(6762); return ReplaceReturn777;}
    case OP_STUB:
	SensorCall(6763);if ((o->op_flags & OPf_PARENS) || PL_madskills)
	    {/*359*/SensorCall(6764);break;/*360*/}
	SensorCall(6765);goto nomod;
    case OP_ENTERSUB:
	SensorCall(6766);if ((type == OP_UNDEF || type == OP_REFGEN || type == OP_LOCK) &&
	    !(o->op_flags & OPf_STACKED)) {
	    SensorCall(6767);o->op_type = OP_RV2CV;		/* entersub => rv2cv */
	    /* Both ENTERSUB and RV2CV use this bit, but for different pur-
	       poses, so we need it clear.  */
	    o->op_private &= ~1;
	    o->op_ppaddr = PL_ppaddr[OP_RV2CV];
	    assert(cUNOPo->op_first->op_type == OP_NULL);
	    op_null(((LISTOP*)cUNOPo->op_first)->op_first);/* disable pushmark */
	    SensorCall(6768);break;
	}
	else {				/* lvalue subroutine call */
	    SensorCall(6769);o->op_private |= OPpLVAL_INTRO
	                   |(OPpENTERSUB_INARGS * (type == OP_LEAVESUBLV));
	    PL_modcount = RETURN_UNLIMITED_NUMBER;
	    SensorCall(6793);if (type == OP_GREPSTART || type == OP_ENTERSUB || type == OP_REFGEN) {
		/* Potential lvalue context: */
		SensorCall(6770);o->op_private |= OPpENTERSUB_INARGS;
		SensorCall(6771);break;
	    }
	    else {                      /* Compile-time error message: */
		SensorCall(6772);OP *kid = cUNOPo->op_first;
		CV *cv;

		SensorCall(6776);if (kid->op_type != OP_PUSHMARK) {
		    SensorCall(6773);if (kid->op_type != OP_NULL || kid->op_targ != OP_LIST)
			{/*361*/SensorCall(6774);Perl_croak(aTHX_
				"panic: unexpected lvalue entersub "
				"args: type/targ %ld:%"UVuf,
				(long)kid->op_type, (UV)kid->op_targ);/*362*/}
		    SensorCall(6775);kid = kLISTOP->op_first;
		}
		SensorCall(6778);while (kid->op_sibling)
		    {/*363*/SensorCall(6777);kid = kid->op_sibling;/*364*/}
		SensorCall(6780);if (!(kid->op_type == OP_NULL && kid->op_targ == OP_RV2CV)) {
		    SensorCall(6779);break;	/* Postpone until runtime */
		}

		SensorCall(6781);kid = kUNOP->op_first;
		SensorCall(6783);if (kid->op_type == OP_NULL && kid->op_targ == OP_RV2SV)
		    {/*365*/SensorCall(6782);kid = kUNOP->op_first;/*366*/}
		SensorCall(6785);if (kid->op_type == OP_NULL)
		    {/*367*/SensorCall(6784);Perl_croak(aTHX_
			       "Unexpected constant lvalue entersub "
			       "entry via type/targ %ld:%"UVuf,
			       (long)kid->op_type, (UV)kid->op_targ);/*368*/}
		SensorCall(6787);if (kid->op_type != OP_GV) {
		    SensorCall(6786);break;
		}

		SensorCall(6788);cv = GvCV(kGVOP_gv);
		SensorCall(6790);if (!cv)
		    {/*369*/SensorCall(6789);break;/*370*/}
		SensorCall(6792);if (CvLVALUE(cv))
		    {/*371*/SensorCall(6791);break;/*372*/}
	    }
	}
	/* FALL THROUGH */
    default:
      nomod:
	SensorCall(6794);if (flags & OP_LVALUE_NO_CROAK) return NULL;
	/* grep, foreach, subcalls, refgen */
	SensorCall(6796);if (type == OP_GREPSTART || type == OP_ENTERSUB
	 || type == OP_REFGEN    || type == OP_LEAVESUBLV)
	    {/*373*/SensorCall(6795);break;/*374*/}
	yyerror(Perl_form(aTHX_ "Can't modify %s in %s",
		     (o->op_type == OP_NULL && (o->op_flags & OPf_SPECIAL)
		      ? "do block"
		      : (o->op_type == OP_ENTERSUB
			? "non-lvalue subroutine call"
			: OP_DESC(o))),
		     type ? PL_op_desc[type] : "local"));
	{OP * ReplaceReturn776 = o; SensorCall(6797); return ReplaceReturn776;}

    case OP_PREINC:
    case OP_PREDEC:
    case OP_POW:
    case OP_MULTIPLY:
    case OP_DIVIDE:
    case OP_MODULO:
    case OP_REPEAT:
    case OP_ADD:
    case OP_SUBTRACT:
    case OP_CONCAT:
    case OP_LEFT_SHIFT:
    case OP_RIGHT_SHIFT:
    case OP_BIT_AND:
    case OP_BIT_XOR:
    case OP_BIT_OR:
    case OP_I_MULTIPLY:
    case OP_I_DIVIDE:
    case OP_I_MODULO:
    case OP_I_ADD:
    case OP_I_SUBTRACT:
	SensorCall(6798);if (!(o->op_flags & OPf_STACKED))
	    {/*375*/SensorCall(6799);goto nomod;/*376*/}
	PL_modcount++;
	SensorCall(6800);break;

    case OP_COND_EXPR:
	SensorCall(6801);localize = 1;
	SensorCall(6802);for (kid = cUNOPo->op_first->op_sibling; kid; kid = kid->op_sibling)
	    op_lvalue(kid, type);
	SensorCall(6803);break;

    case OP_RV2AV:
    case OP_RV2HV:
	SensorCall(6804);if (type == OP_REFGEN && o->op_flags & OPf_PARENS) {
           PL_modcount = RETURN_UNLIMITED_NUMBER;
	    {OP * ReplaceReturn775 = o; SensorCall(6805); return ReplaceReturn775;}		/* Treat \(@foo) like ordinary list. */
	}
	/* FALL THROUGH */
    case OP_RV2GV:
	SensorCall(6806);if (scalar_mod_type(o, type))
	    {/*377*/SensorCall(6807);goto nomod;/*378*/}
	ref(cUNOPo->op_first, o->op_type);
	/* FALL THROUGH */
    case OP_ASLICE:
    case OP_HSLICE:
	SensorCall(6808);if (type == OP_LEAVESUBLV)
	    o->op_private |= OPpMAYBE_LVSUB;
	SensorCall(6809);localize = 1;
	/* FALL THROUGH */
    case OP_AASSIGN:
    case OP_NEXTSTATE:
    case OP_DBSTATE:
       PL_modcount = RETURN_UNLIMITED_NUMBER;
	SensorCall(6810);break;
    case OP_AV2ARYLEN:
	PL_hints |= HINT_BLOCK_SCOPE;
	SensorCall(6811);if (type == OP_LEAVESUBLV)
	    o->op_private |= OPpMAYBE_LVSUB;
	PL_modcount++;
	SensorCall(6812);break;
    case OP_RV2SV:
	ref(cUNOPo->op_first, o->op_type);
	SensorCall(6813);localize = 1;
	/* FALL THROUGH */
    case OP_GV:
	PL_hints |= HINT_BLOCK_SCOPE;
    case OP_SASSIGN:
    case OP_ANDASSIGN:
    case OP_ORASSIGN:
    case OP_DORASSIGN:
	PL_modcount++;
	SensorCall(6814);break;

    case OP_AELEMFAST:
    case OP_AELEMFAST_LEX:
	SensorCall(6815);localize = -1;
	PL_modcount++;
	SensorCall(6816);break;

    case OP_PADAV:
    case OP_PADHV:
       PL_modcount = RETURN_UNLIMITED_NUMBER;
	SensorCall(6818);if (type == OP_REFGEN && o->op_flags & OPf_PARENS)
	    {/*379*/{OP * ReplaceReturn774 = o; SensorCall(6817); return ReplaceReturn774;}/*380*/}		/* Treat \(@foo) like ordinary list. */
	SensorCall(6820);if (scalar_mod_type(o, type))
	    {/*381*/SensorCall(6819);goto nomod;/*382*/}
	SensorCall(6821);if (type == OP_LEAVESUBLV)
	    o->op_private |= OPpMAYBE_LVSUB;
	/* FALL THROUGH */
    case OP_PADSV:
	PL_modcount++;
	SensorCall(6823);if (!type) /* local() */
	    {/*383*/SensorCall(6822);Perl_croak(aTHX_ "Can't localize lexical variable %"SVf,
		 PAD_COMPNAME_SV(o->op_targ));/*384*/}
	SensorCall(6824);break;

    case OP_PUSHMARK:
	SensorCall(6825);localize = 0;
	SensorCall(6826);break;

    case OP_KEYS:
    case OP_RKEYS:
	SensorCall(6827);if (type != OP_SASSIGN && type != OP_LEAVESUBLV)
	    {/*385*/SensorCall(6828);goto nomod;/*386*/}
	SensorCall(6829);goto lvalue_func;
    case OP_SUBSTR:
	SensorCall(6830);if (o->op_private == 4) /* don't allow 4 arg substr as lvalue */
	    {/*387*/SensorCall(6831);goto nomod;/*388*/}
	/* FALL THROUGH */
    case OP_POS:
    case OP_VEC:
      lvalue_func:
	SensorCall(6832);if (type == OP_LEAVESUBLV)
	    o->op_private |= OPpMAYBE_LVSUB;
	pad_free(o->op_targ);
	SensorCall(6833);o->op_targ = pad_alloc(o->op_type, SVs_PADMY);
	assert(SvTYPE(PAD_SV(o->op_targ)) == SVt_NULL);
	SensorCall(6834);if (o->op_flags & OPf_KIDS)
	    op_lvalue(cBINOPo->op_first->op_sibling, type);
	SensorCall(6835);break;

    case OP_AELEM:
    case OP_HELEM:
	ref(cBINOPo->op_first, o->op_type);
	SensorCall(6836);if (type == OP_ENTERSUB &&
	     !(o->op_private & (OPpLVAL_INTRO | OPpDEREF)))
	    o->op_private |= OPpLVAL_DEFER;
	SensorCall(6837);if (type == OP_LEAVESUBLV)
	    o->op_private |= OPpMAYBE_LVSUB;
	SensorCall(6838);localize = 1;
	PL_modcount++;
	SensorCall(6839);break;

    case OP_SCOPE:
    case OP_LEAVE:
    case OP_ENTER:
    case OP_LINESEQ:
	SensorCall(6840);localize = 0;
	SensorCall(6841);if (o->op_flags & OPf_KIDS)
	    op_lvalue(cLISTOPo->op_last, type);
	SensorCall(6842);break;

    case OP_NULL:
	SensorCall(6843);localize = 0;
	SensorCall(6847);if (o->op_flags & OPf_SPECIAL)		/* do BLOCK */
	    {/*389*/SensorCall(6844);goto nomod;/*390*/}
	else {/*391*/SensorCall(6845);if (!(o->op_flags & OPf_KIDS))
	    {/*393*/SensorCall(6846);break;/*394*/}/*392*/}
	SensorCall(6849);if (o->op_targ != OP_LIST) {
	    op_lvalue(cBINOPo->op_first, type);
	    SensorCall(6848);break;
	}
	/* FALL THROUGH */
    case OP_LIST:
	SensorCall(6850);localize = 0;
	SensorCall(6851);for (kid = cLISTOPo->op_first; kid; kid = kid->op_sibling)
	    /* elements might be in void context because the list is
	       in scalar context or because they are attribute sub calls */
	    if ( (kid->op_flags & OPf_WANT) != OPf_WANT_VOID )
		op_lvalue(kid, type);
	SensorCall(6852);break;

    case OP_RETURN:
	SensorCall(6853);if (type != OP_LEAVESUBLV)
	    {/*395*/SensorCall(6854);goto nomod;/*396*/}
	SensorCall(6855);break; /* op_lvalue()ing was handled by ck_return() */
    }

    /* [20011101.069] File test operators interpret OPf_REF to mean that
       their argument is a filehandle; thus \stat(".") should not set
       it. AMS 20011102 */
    SensorCall(6858);if (type == OP_REFGEN &&
        PL_check[o->op_type] == Perl_ck_ftst)
        {/*397*/{OP * ReplaceReturn773 = o; SensorCall(6857); return ReplaceReturn773;}/*398*/}

    SensorCall(6859);if (type != OP_LEAVESUBLV)
        o->op_flags |= OPf_MOD;

    SensorCall(6860);if (type == OP_AASSIGN || type == OP_SASSIGN)
	o->op_flags |= OPf_SPECIAL|OPf_REF;
    else if (!type) { /* local() */
	switch (localize) {
	case 1:
	    o->op_private |= OPpLVAL_INTRO;
	    o->op_flags &= ~OPf_SPECIAL;
	    PL_hints |= HINT_BLOCK_SCOPE;
	    break;
	case 0:
	    break;
	case -1:
	    Perl_ck_warner(aTHX_ packWARN(WARN_SYNTAX),
			   "Useless localization of %s", OP_DESC(o));
	}
    }
    else if (type != OP_GREPSTART && type != OP_ENTERSUB
             && type != OP_LEAVESUBLV)
	o->op_flags |= OPf_REF;
    {OP * ReplaceReturn772 = o; SensorCall(6861); return ReplaceReturn772;}
}

STATIC bool
S_scalar_mod_type(const OP *o, I32 type)
{
    assert(o || type != OP_SASSIGN);

    SensorCall(6862);switch (type) {
    case OP_SASSIGN:
	SensorCall(6863);if (o->op_type == OP_RV2GV)
	    return FALSE;
	/* FALL THROUGH */
    case OP_PREINC:
    case OP_PREDEC:
    case OP_POSTINC:
    case OP_POSTDEC:
    case OP_I_PREINC:
    case OP_I_PREDEC:
    case OP_I_POSTINC:
    case OP_I_POSTDEC:
    case OP_POW:
    case OP_MULTIPLY:
    case OP_DIVIDE:
    case OP_MODULO:
    case OP_REPEAT:
    case OP_ADD:
    case OP_SUBTRACT:
    case OP_I_MULTIPLY:
    case OP_I_DIVIDE:
    case OP_I_MODULO:
    case OP_I_ADD:
    case OP_I_SUBTRACT:
    case OP_LEFT_SHIFT:
    case OP_RIGHT_SHIFT:
    case OP_BIT_AND:
    case OP_BIT_XOR:
    case OP_BIT_OR:
    case OP_CONCAT:
    case OP_SUBST:
    case OP_TRANS:
    case OP_TRANSR:
    case OP_READ:
    case OP_SYSREAD:
    case OP_RECV:
    case OP_ANDASSIGN:
    case OP_ORASSIGN:
    case OP_DORASSIGN:
	{_Bool  ReplaceReturn771 = TRUE; SensorCall(6864); return ReplaceReturn771;}
    default:
	{_Bool  ReplaceReturn770 = FALSE; SensorCall(6865); return ReplaceReturn770;}
    }
SensorCall(6866);}

STATIC bool
S_is_handle_constructor(const OP *o, I32 numargs)
{
    PERL_ARGS_ASSERT_IS_HANDLE_CONSTRUCTOR;

    SensorCall(6867);switch (o->op_type) {
    case OP_PIPE_OP:
    case OP_SOCKPAIR:
	SensorCall(6868);if (numargs == 2)
	    return TRUE;
	/* FALL THROUGH */
    case OP_SYSOPEN:
    case OP_OPEN:
    case OP_SELECT:		/* XXX c.f. SelectSaver.pm */
    case OP_SOCKET:
    case OP_OPEN_DIR:
    case OP_ACCEPT:
	SensorCall(6869);if (numargs == 1)
	    return TRUE;
	/* FALLTHROUGH */
    default:
	{_Bool  ReplaceReturn769 = FALSE; SensorCall(6870); return ReplaceReturn769;}
    }
SensorCall(6871);}

static OP *
S_refkids(pTHX_ OP *o, I32 type)
{
    SensorCall(6872);if (o && o->op_flags & OPf_KIDS) {
        SensorCall(6873);OP *kid;
	SensorCall(6874);for (kid = cLISTOPo->op_first; kid; kid = kid->op_sibling)
	    ref(kid, type);
    }
    {OP * ReplaceReturn768 = o; SensorCall(6875); return ReplaceReturn768;}
}

OP *
Perl_doref(pTHX_ OP *o, I32 type, bool set_op_ref)
{
SensorCall(6876);    dVAR;
    OP *kid;

    PERL_ARGS_ASSERT_DOREF;

    SensorCall(6878);if (!o || (PL_parser && PL_parser->error_count))
	{/*217*/{OP * ReplaceReturn767 = o; SensorCall(6877); return ReplaceReturn767;}/*218*/}

    SensorCall(6906);switch (o->op_type) {
    case OP_ENTERSUB:
	SensorCall(6879);if ((type == OP_EXISTS || type == OP_DEFINED) &&
	    !(o->op_flags & OPf_STACKED)) {
	    SensorCall(6880);o->op_type = OP_RV2CV;             /* entersub => rv2cv */
	    o->op_ppaddr = PL_ppaddr[OP_RV2CV];
	    assert(cUNOPo->op_first->op_type == OP_NULL);
	    op_null(((LISTOP*)cUNOPo->op_first)->op_first);	/* disable pushmark */
	    o->op_flags |= OPf_SPECIAL;
	    o->op_private &= ~1;
	}
	else {/*219*/SensorCall(6881);if (type == OP_RV2SV || type == OP_RV2AV || type == OP_RV2HV){
	    SensorCall(6882);o->op_private |= (type == OP_RV2AV ? OPpDEREF_AV
			      : type == OP_RV2HV ? OPpDEREF_HV
			      : OPpDEREF_SV);
	    o->op_flags |= OPf_MOD;
	;/*220*/}}

	SensorCall(6883);break;

    case OP_COND_EXPR:
	SensorCall(6884);for (kid = cUNOPo->op_first->op_sibling; kid; kid = kid->op_sibling)
	    doref(kid, type, set_op_ref);
	SensorCall(6885);break;
    case OP_RV2SV:
	SensorCall(6886);if (type == OP_DEFINED)
	    o->op_flags |= OPf_SPECIAL;		/* don't create GV */
	doref(cUNOPo->op_first, o->op_type, set_op_ref);
	/* FALL THROUGH */
    case OP_PADSV:
	SensorCall(6887);if (type == OP_RV2SV || type == OP_RV2AV || type == OP_RV2HV) {
	    SensorCall(6888);o->op_private |= (type == OP_RV2AV ? OPpDEREF_AV
			      : type == OP_RV2HV ? OPpDEREF_HV
			      : OPpDEREF_SV);
	    o->op_flags |= OPf_MOD;
	}
	SensorCall(6889);break;

    case OP_RV2AV:
    case OP_RV2HV:
	SensorCall(6890);if (set_op_ref)
	    o->op_flags |= OPf_REF;
	/* FALL THROUGH */
    case OP_RV2GV:
	SensorCall(6891);if (type == OP_DEFINED)
	    o->op_flags |= OPf_SPECIAL;		/* don't create GV */
	doref(cUNOPo->op_first, o->op_type, set_op_ref);
	SensorCall(6892);break;

    case OP_PADAV:
    case OP_PADHV:
	SensorCall(6893);if (set_op_ref)
	    o->op_flags |= OPf_REF;
	SensorCall(6894);break;

    case OP_SCALAR:
    case OP_NULL:
	SensorCall(6895);if (!(o->op_flags & OPf_KIDS))
	    {/*221*/SensorCall(6896);break;/*222*/}
	doref(cBINOPo->op_first, type, set_op_ref);
	SensorCall(6897);break;
    case OP_AELEM:
    case OP_HELEM:
	doref(cBINOPo->op_first, o->op_type, set_op_ref);
	SensorCall(6899);if (type == OP_RV2SV || type == OP_RV2AV || type == OP_RV2HV) {
	    SensorCall(6898);o->op_private |= (type == OP_RV2AV ? OPpDEREF_AV
			      : type == OP_RV2HV ? OPpDEREF_HV
			      : OPpDEREF_SV);
	    o->op_flags |= OPf_MOD;
	}
	SensorCall(6900);break;

    case OP_SCOPE:
    case OP_LEAVE:
	SensorCall(6901);set_op_ref = FALSE;
	/* FALL THROUGH */
    case OP_ENTER:
    case OP_LIST:
	SensorCall(6902);if (!(o->op_flags & OPf_KIDS))
	    {/*223*/SensorCall(6903);break;/*224*/}
	doref(cLISTOPo->op_last, type, set_op_ref);
	SensorCall(6904);break;
    default:
	SensorCall(6905);break;
    }
    {OP * ReplaceReturn766 = scalar(o); SensorCall(6907); return ReplaceReturn766;}

}

STATIC OP *
S_dup_attrlist(pTHX_ OP *o)
{
SensorCall(6908);    dVAR;
    OP *rop;

    PERL_ARGS_ASSERT_DUP_ATTRLIST;

    /* An attrlist is either a simple OP_CONST or an OP_LIST with kids,
     * where the first kid is OP_PUSHMARK and the remaining ones
     * are OP_CONST.  We need to push the OP_CONST values.
     */
    SensorCall(6912);if (o->op_type == OP_CONST)
	rop = newSVOP(OP_CONST, o->op_flags, SvREFCNT_inc_NN(cSVOPo->op_sv));
#ifdef PERL_MAD
    else if (o->op_type == OP_NULL)
	rop = NULL;
#endif
    else {
	assert((o->op_type == OP_LIST) && (o->op_flags & OPf_KIDS));
	SensorCall(6909);rop = NULL;
	SensorCall(6911);for (o = cLISTOPo->op_first; o; o=o->op_sibling) {
	    SensorCall(6910);if (o->op_type == OP_CONST)
		rop = op_append_elem(OP_LIST, rop,
				  newSVOP(OP_CONST, o->op_flags,
					  SvREFCNT_inc_NN(cSVOPo->op_sv)));
	}
    }
    {OP * ReplaceReturn765 = rop; SensorCall(6913); return ReplaceReturn765;}
}

STATIC void
S_apply_attrs(pTHX_ HV *stash, SV *target, OP *attrs, bool for_my)
{
SensorCall(6914);    dVAR;

    PERL_ARGS_ASSERT_APPLY_ATTRS;

    /* fake up C<use attributes $pkg,$rv,@attrs> */
    ENTER;		/* need to protect against side-effects of 'use' */

#define ATTRSMODULE "attributes"
#define ATTRSMODULE_PM "attributes.pm"

    SensorCall(6919);if (for_my) {
	/* Don't force the C<use> if we don't need it. */
	SensorCall(6915);SV * const * const svp = hv_fetchs(GvHVn(PL_incgv), ATTRSMODULE_PM, FALSE);
	SensorCall(6917);if (svp && *svp != &PL_sv_undef)
	    NOOP;	/* already in %INC */
	else
	    {/*525*/SensorCall(6916);Perl_load_module(aTHX_ PERL_LOADMOD_NOIMPORT,
			     newSVpvs(ATTRSMODULE), NULL);/*526*/}
    }
    else {
	SensorCall(6918);SV * const stashsv =
	    stash ? newSVhek(HvNAME_HEK(stash)) : &PL_sv_no;
	Perl_load_module(aTHX_ PERL_LOADMOD_IMPORT_OPS,
			 newSVpvs(ATTRSMODULE),
			 NULL,
			 op_prepend_elem(OP_LIST,
				      newSVOP(OP_CONST, 0, stashsv),
				      op_prepend_elem(OP_LIST,
						   newSVOP(OP_CONST, 0,
							   newRV(target)),
						   dup_attrlist(attrs))));
    }
    LEAVE;
}

STATIC void
S_apply_attrs_my(pTHX_ HV *stash, OP *target, OP *attrs, OP **imopsp)
{
SensorCall(6920);    dVAR;
    OP *pack, *imop, *arg;
    SV *meth, *stashsv;

    PERL_ARGS_ASSERT_APPLY_ATTRS_MY;

    SensorCall(6922);if (!attrs)
	{/*527*/SensorCall(6921);return;/*528*/}

    assert(target->op_type == OP_PADSV ||
	   target->op_type == OP_PADHV ||
	   target->op_type == OP_PADAV);

    /* Ensure that attributes.pm is loaded. */
    apply_attrs(stash, PAD_SV(target->op_targ), attrs, TRUE);

    /* Need package name for method call. */
    SensorCall(6923);pack = newSVOP(OP_CONST, 0, newSVpvs(ATTRSMODULE));

    /* Build up the real arg-list. */
    stashsv = stash ? newSVhek(HvNAME_HEK(stash)) : &PL_sv_no;

    arg = newOP(OP_PADSV, 0);
    arg->op_targ = target->op_targ;
    arg = op_prepend_elem(OP_LIST,
		       newSVOP(OP_CONST, 0, stashsv),
		       op_prepend_elem(OP_LIST,
				    newUNOP(OP_REFGEN, 0,
					    op_lvalue(arg, OP_REFGEN)),
				    dup_attrlist(attrs)));

    /* Fake up a method call to import */
    meth = newSVpvs_share("import");
    imop = convert(OP_ENTERSUB, OPf_STACKED|OPf_SPECIAL|OPf_WANT_VOID,
		   op_append_elem(OP_LIST,
			       op_prepend_elem(OP_LIST, pack, list(arg)),
			       newSVOP(OP_METHOD_NAMED, 0, meth)));

    /* Combine the ops. */
    *imopsp = op_append_elem(OP_LIST, *imopsp, imop);
SensorCall(6924);}

/*
=notfor apidoc apply_attrs_string

Attempts to apply a list of attributes specified by the C<attrstr> and
C<len> arguments to the subroutine identified by the C<cv> argument which
is expected to be associated with the package identified by the C<stashpv>
argument (see L<attributes>).  It gets this wrong, though, in that it
does not correctly identify the boundaries of the individual attribute
specifications within C<attrstr>.  This is not really intended for the
public API, but has to be listed here for systems such as AIX which
need an explicit export list for symbols.  (It's called from XS code
in support of the C<ATTRS:> keyword from F<xsubpp>.)  Patches to fix it
to respect attribute syntax properly would be welcome.

=cut
*/

void
Perl_apply_attrs_string(pTHX_ const char *stashpv, CV *cv,
                        const char *attrstr, STRLEN len)
{
    SensorCall(6925);OP *attrs = NULL;

    PERL_ARGS_ASSERT_APPLY_ATTRS_STRING;

    SensorCall(6927);if (!len) {
        SensorCall(6926);len = strlen(attrstr);
    }

    SensorCall(6933);while (len) {
        SensorCall(6928);for (; isSPACE(*attrstr) && len; --len, ++attrstr) ;
        SensorCall(6932);if (len) {
            SensorCall(6929);const char * const sstr = attrstr;
            SensorCall(6930);for (; !isSPACE(*attrstr) && len; --len, ++attrstr) ;
            SensorCall(6931);attrs = op_append_elem(OP_LIST, attrs,
                                newSVOP(OP_CONST, 0,
                                        newSVpvn(sstr, attrstr-sstr)));
        }
    }

    SensorCall(6934);Perl_load_module(aTHX_ PERL_LOADMOD_IMPORT_OPS,
		     newSVpvs(ATTRSMODULE),
                     NULL, op_prepend_elem(OP_LIST,
				  newSVOP(OP_CONST, 0, newSVpv(stashpv,0)),
				  op_prepend_elem(OP_LIST,
					       newSVOP(OP_CONST, 0,
						       newRV(MUTABLE_SV(cv))),
                                               attrs)));
SensorCall(6935);}

STATIC OP *
S_my_kid(pTHX_ OP *o, OP *attrs, OP **imopsp)
{
SensorCall(6936);    dVAR;
    I32 type;
    const bool stately = PL_parser && PL_parser->in_my == KEY_state;

    PERL_ARGS_ASSERT_MY_KID;

    SensorCall(6938);if (!o || (PL_parser && PL_parser->error_count))
	{/*585*/{OP * ReplaceReturn764 = o; SensorCall(6937); return ReplaceReturn764;}/*586*/}

    SensorCall(6939);type = o->op_type;
    SensorCall(6942);if (PL_madskills && type == OP_NULL && o->op_flags & OPf_KIDS) {
	SensorCall(6940);(void)my_kid(cUNOPo->op_first, attrs, imopsp);
	{OP * ReplaceReturn763 = o; SensorCall(6941); return ReplaceReturn763;}
    }

    SensorCall(6959);if (type == OP_LIST) {
        SensorCall(6943);OP *kid;
	SensorCall(6944);for (kid = cLISTOPo->op_first; kid; kid = kid->op_sibling)
	    my_kid(kid, attrs, imopsp);
	{OP * ReplaceReturn762 = o; SensorCall(6945); return ReplaceReturn762;}
    } else {/*587*/SensorCall(6946);if (type == OP_UNDEF
#ifdef PERL_MAD
	       || type == OP_STUB
#endif
	       ) {
	{OP * ReplaceReturn761 = o; SensorCall(6947); return ReplaceReturn761;}
    } else {/*589*/SensorCall(6948);if (type == OP_RV2SV ||	/* "our" declaration */
	       type == OP_RV2AV ||
	       type == OP_RV2HV) { /* XXX does this let anything illegal in? */
	SensorCall(6949);if (cUNOPo->op_first->op_type != OP_GV) { /* MJD 20011224 */
	    yyerror(Perl_form(aTHX_ "Can't declare %s in \"%s\"",
			OP_DESC(o),
			PL_parser->in_my == KEY_our
			    ? "our"
			    : PL_parser->in_my == KEY_state ? "state" : "my"));
	} else {/*591*/SensorCall(6950);if (attrs) {
	    SensorCall(6951);GV * const gv = cGVOPx_gv(cUNOPo->op_first);
	    PL_parser->in_my = FALSE;
	    PL_parser->in_my_stash = NULL;
	    apply_attrs(GvSTASH(gv),
			(type == OP_RV2SV ? GvSV(gv) :
			 type == OP_RV2AV ? MUTABLE_SV(GvAV(gv)) :
			 type == OP_RV2HV ? MUTABLE_SV(GvHV(gv)) : MUTABLE_SV(gv)),
			attrs, FALSE);
	;/*592*/}}
	SensorCall(6952);o->op_private |= OPpOUR_INTRO;
	{OP * ReplaceReturn760 = o; SensorCall(6953); return ReplaceReturn760;}
    }
    else {/*593*/SensorCall(6954);if (type != OP_PADSV &&
	     type != OP_PADAV &&
	     type != OP_PADHV &&
	     type != OP_PUSHMARK)
    {
	yyerror(Perl_form(aTHX_ "Can't declare %s in \"%s\"",
			  OP_DESC(o),
			  PL_parser->in_my == KEY_our
			    ? "our"
			    : PL_parser->in_my == KEY_state ? "state" : "my"));
	{OP * ReplaceReturn759 = o; SensorCall(6955); return ReplaceReturn759;}
    }
    else {/*595*/SensorCall(6956);if (attrs && type != OP_PUSHMARK) {
	SensorCall(6957);HV *stash;

	PL_parser->in_my = FALSE;
	PL_parser->in_my_stash = NULL;

	/* check for C<my Dog $spot> when deciding package */
	stash = PAD_COMPNAME_TYPE(o->op_targ);
	SensorCall(6958);if (!stash)
	    stash = PL_curstash;
	apply_attrs_my(stash, o, attrs, imopsp);
    ;/*596*/}/*594*/}/*590*/}/*588*/}}
    SensorCall(6960);o->op_flags |= OPf_MOD;
    o->op_private |= OPpLVAL_INTRO;
    SensorCall(6961);if (stately)
	o->op_private |= OPpPAD_STATE;
    {OP * ReplaceReturn758 = o; SensorCall(6962); return ReplaceReturn758;}
}

OP *
Perl_my_attrs(pTHX_ OP *o, OP *attrs)
{
SensorCall(6963);    dVAR;
    OP *rops;
    int maybe_scalar = 0;

    PERL_ARGS_ASSERT_MY_ATTRS;

/* [perl #17376]: this appears to be premature, and results in code such as
   C< our(%x); > executing in list mode rather than void mode */
#if 0
    if (o->op_flags & OPf_PARENS)
	list(o);
    else
	maybe_scalar = 1;
#else
    maybe_scalar = 1;
#endif
    SensorCall(6964);if (attrs)
	SAVEFREEOP(attrs);
    SensorCall(6965);rops = NULL;
    o = my_kid(o, attrs, &rops);
    SensorCall(6972);if (rops) {
	SensorCall(6966);if (maybe_scalar && o->op_type == OP_PADSV) {
	    SensorCall(6967);o = scalar(op_append_list(OP_LIST, rops, o));
	    o->op_private |= OPpLVAL_INTRO;
	}
	else {
	    /* The listop in rops might have a pushmark at the beginning,
	       which will mess up list assignment. */
	    SensorCall(6968);LISTOP * const lrops = (LISTOP *)rops; /* for brevity */
	    SensorCall(6970);if (rops->op_type == OP_LIST && 
	        lrops->op_first && lrops->op_first->op_type == OP_PUSHMARK)
	    {
		SensorCall(6969);OP * const pushmark = lrops->op_first;
		lrops->op_first = pushmark->op_sibling;
		op_free(pushmark);
	    }
	    SensorCall(6971);o = op_append_list(OP_LIST, o, rops);
	}
    }
    PL_parser->in_my = FALSE;
    PL_parser->in_my_stash = NULL;
    {OP * ReplaceReturn757 = o; SensorCall(6973); return ReplaceReturn757;}
}

OP *
Perl_sawparens(pTHX_ OP *o)
{
SensorCall(6974);    PERL_UNUSED_CONTEXT;
    SensorCall(6975);if (o)
	o->op_flags |= OPf_PARENS;
    {OP * ReplaceReturn756 = o; SensorCall(6976); return ReplaceReturn756;}
}

OP *
Perl_bind_match(pTHX_ I32 type, OP *left, OP *right)
{
    SensorCall(6977);OP *o;
    bool ismatchop = 0;
    const OPCODE ltype = left->op_type;
    const OPCODE rtype = right->op_type;

    PERL_ARGS_ASSERT_BIND_MATCH;

    SensorCall(6982);if ( (ltype == OP_RV2AV || ltype == OP_RV2HV || ltype == OP_PADAV
	  || ltype == OP_PADHV) && ckWARN(WARN_MISC))
    {
      SensorCall(6978);const char * const desc
	  = PL_op_desc[(
		          rtype == OP_SUBST || rtype == OP_TRANS
		       || rtype == OP_TRANSR
		       )
		       ? (int)rtype : OP_MATCH];
      const bool isary = ltype == OP_RV2AV || ltype == OP_PADAV;
      GV *gv;
      SV * const name =
       (ltype == OP_RV2AV || ltype == OP_RV2HV)
        ?    cUNOPx(left)->op_first->op_type == OP_GV
          && (gv = cGVOPx_gv(cUNOPx(left)->op_first))
              ? varname(gv, isary ? '@' : '%', 0, NULL, 0, 1)
              : NULL
        : varname(
           (GV *)PL_compcv, isary ? '@' : '%', left->op_targ, NULL, 0, 1
          );
      SensorCall(6981);if (name)
	{/*7*/SensorCall(6979);Perl_warner(aTHX_ packWARN(WARN_MISC),
             "Applying %s to %"SVf" will act on scalar(%"SVf")",
             desc, name, name);/*8*/}
      else {
	SensorCall(6980);const char * const sample = (isary
	     ? "@array" : "%hash");
	Perl_warner(aTHX_ packWARN(WARN_MISC),
             "Applying %s to %s will act on scalar(%s)",
             desc, sample, sample);
      }
    }

    SensorCall(6983);if (rtype == OP_CONST &&
	cSVOPx(right)->op_private & OPpCONST_BARE &&
	cSVOPx(right)->op_private & OPpCONST_STRICT)
    {
	no_bareword_allowed(right);
    }

    /* !~ doesn't make sense with /r, so error on it for now */
    SensorCall(6984);if (rtype == OP_SUBST && (cPMOPx(right)->op_pmflags & PMf_NONDESTRUCT) &&
	type == OP_NOT)
	yyerror("Using !~ with s///r doesn't make sense");
    SensorCall(6985);if (rtype == OP_TRANSR && type == OP_NOT)
	yyerror("Using !~ with tr///r doesn't make sense");

    SensorCall(6986);ismatchop = (rtype == OP_MATCH ||
		 rtype == OP_SUBST ||
		 rtype == OP_TRANS || rtype == OP_TRANSR)
	     && !(right->op_flags & OPf_SPECIAL);
    SensorCall(6988);if (ismatchop && right->op_private & OPpTARGET_MY) {
	SensorCall(6987);right->op_targ = 0;
	right->op_private &= ~OPpTARGET_MY;
    }
    SensorCall(6995);if (!(right->op_flags & OPf_STACKED) && ismatchop) {
	SensorCall(6989);OP *newleft;

	right->op_flags |= OPf_STACKED;
	SensorCall(6991);if (rtype != OP_MATCH && rtype != OP_TRANSR &&
            ! (rtype == OP_TRANS &&
               right->op_private & OPpTRANS_IDENTICAL) &&
	    ! (rtype == OP_SUBST &&
	       (cPMOPx(right)->op_pmflags & PMf_NONDESTRUCT)))
	    newleft = op_lvalue(left, rtype);
	else
	    {/*9*/SensorCall(6990);newleft = left;/*10*/}
	SensorCall(6992);if (right->op_type == OP_TRANS || right->op_type == OP_TRANSR)
	    o = newBINOP(OP_NULL, OPf_STACKED, scalar(newleft), right);
	else
	    o = op_prepend_elem(rtype, scalar(newleft), right);
	SensorCall(6993);if (type == OP_NOT)
	    return newUNOP(OP_NOT, 0, scalar(o));
	{OP * ReplaceReturn755 = o; SensorCall(6994); return ReplaceReturn755;}
    }
    else
	return bind_match(type, left,
		pmruntime(newPMOP(OP_MATCH, 0), right, 0));
SensorCall(6996);}

OP *
Perl_invert(pTHX_ OP *o)
{
    SensorCall(6997);if (!o)
	return NULL;
    {OP * ReplaceReturn754 = newUNOP(OP_NOT, OPf_SPECIAL, scalar(o)); SensorCall(6998); return ReplaceReturn754;}
}

/*
=for apidoc Amx|OP *|op_scope|OP *o

Wraps up an op tree with some additional ops so that at runtime a dynamic
scope will be created.  The original ops run in the new dynamic scope,
and then, provided that they exit normally, the scope will be unwound.
The additional ops used to create and unwind the dynamic scope will
normally be an C<enter>/C<leave> pair, but a C<scope> op may be used
instead if the ops are simple enough to not need the full dynamic scope
structure.

=cut
*/

OP *
Perl_op_scope(pTHX_ OP *o)
{
SensorCall(6999);    dVAR;
    SensorCall(7002);if (o) {
	SensorCall(7000);if (o->op_flags & OPf_PARENS || PERLDB_NOOPT || PL_tainting) {
	    SensorCall(7001);o = op_prepend_elem(OP_LINESEQ, newOP(OP_ENTER, 0), o);
	    o->op_type = OP_LEAVE;
	    o->op_ppaddr = PL_ppaddr[OP_LEAVE];
	}
	else if (o->op_type == OP_LINESEQ) {
	    OP *kid;
	    o->op_type = OP_SCOPE;
	    o->op_ppaddr = PL_ppaddr[OP_SCOPE];
	    kid = ((LISTOP*)o)->op_first;
	    if (kid->op_type == OP_NEXTSTATE || kid->op_type == OP_DBSTATE) {
		op_null(kid);

		/* The following deals with things like 'do {1 for 1}' */
		kid = kid->op_sibling;
		if (kid &&
		    (kid->op_type == OP_NEXTSTATE || kid->op_type == OP_DBSTATE))
		    op_null(kid);
	    }
	}
	else
	    o = newLISTOP(OP_SCOPE, 0, o, NULL);
    }
    {OP * ReplaceReturn753 = o; SensorCall(7003); return ReplaceReturn753;}
}

int
Perl_block_start(pTHX_ int full)
{
SensorCall(7004);    dVAR;
    const int retval = PL_savestack_ix;

    pad_block_start(full);
    SAVEHINTS();
    PL_hints &= ~HINT_BLOCK_SCOPE;
    SAVECOMPILEWARNINGS();
    PL_compiling.cop_warnings = DUP_WARNINGS(PL_compiling.cop_warnings);

    CALL_BLOCK_HOOKS(bhk_start, full);

    {int  ReplaceReturn752 = retval; SensorCall(7005); return ReplaceReturn752;}
}

OP*
Perl_block_end(pTHX_ I32 floor, OP *seq)
{
SensorCall(7006);    dVAR;
    const int needblockscope = PL_hints & HINT_BLOCK_SCOPE;
    OP* retval = scalarseq(seq);

    CALL_BLOCK_HOOKS(bhk_pre_end, &retval);

    LEAVE_SCOPE(floor);
    CopHINTS_set(&PL_compiling, PL_hints);
    SensorCall(7007);if (needblockscope)
	PL_hints |= HINT_BLOCK_SCOPE; /* propagate out */
    pad_leavemy();

    CALL_BLOCK_HOOKS(bhk_post_end, &retval);

    {OP * ReplaceReturn751 = retval; SensorCall(7008); return ReplaceReturn751;}
}

/*
=head1 Compile-time scope hooks

=for apidoc Aox||blockhook_register

Register a set of hooks to be called when the Perl lexical scope changes
at compile time. See L<perlguts/"Compile-time scope hooks">.

=cut
*/

void
Perl_blockhook_register(pTHX_ BHK *hk)
{
    PERL_ARGS_ASSERT_BLOCKHOOK_REGISTER;

    SensorCall(7009);Perl_av_create_and_push(aTHX_ &PL_blockhooks, newSViv(PTR2IV(hk)));
SensorCall(7010);}

STATIC OP *
S_newDEFSVOP(pTHX)
{
SensorCall(7011);    dVAR;
    const PADOFFSET offset = pad_findmy_pvs("$_", 0);
    SensorCall(7015);if (offset == NOT_IN_PAD || PAD_COMPNAME_FLAGS_isOUR(offset)) {
	{OP * ReplaceReturn750 = newSVREF(newGVOP(OP_GV, 0, PL_defgv)); SensorCall(7012); return ReplaceReturn750;}
    }
    else {
	SensorCall(7013);OP * const o = newOP(OP_PADSV, 0);
	o->op_targ = offset;
	{OP * ReplaceReturn749 = o; SensorCall(7014); return ReplaceReturn749;}
    }
SensorCall(7016);}

void
Perl_newPROG(pTHX_ OP *o)
{
SensorCall(7017);    dVAR;

    PERL_ARGS_ASSERT_NEWPROG;

    SensorCall(7030);if (PL_in_eval) {
	SensorCall(7018);PERL_CONTEXT *cx;
	I32 i;
	SensorCall(7020);if (PL_eval_root)
		{/*291*/SensorCall(7019);return;/*292*/}
	PL_eval_root = newUNOP(OP_LEAVEEVAL,
			       ((PL_in_eval & EVAL_KEEPERR)
				? OPf_SPECIAL : 0), o);

	SensorCall(7021);cx = &cxstack[cxstack_ix];
	assert(CxTYPE(cx) == CXt_EVAL);

	SensorCall(7022);if ((cx->blk_gimme & G_WANT) == G_VOID)
	    scalarvoid(PL_eval_root);
	else if ((cx->blk_gimme & G_WANT) == G_ARRAY)
	    list(PL_eval_root);
	else
	    scalar(PL_eval_root);

	/* don't use LINKLIST, since PL_eval_root might indirect through
	 * a rather expensive function call and LINKLIST evaluates its
	 * argument more than once */
	PL_eval_start = op_linklist(PL_eval_root);
	PL_eval_root->op_private |= OPpREFCOUNTED;
	OpREFCNT_set(PL_eval_root, 1);
	PL_eval_root->op_next = 0;
	SensorCall(7023);i = PL_savestack_ix;
	SAVEFREEOP(o);
	ENTER;
	CALL_PEEP(PL_eval_start);
	finalize_optree(PL_eval_root);
	LEAVE;
	PL_savestack_ix = i;
    }
    else {
	SensorCall(7024);if (o->op_type == OP_STUB) {
	    PL_comppad_name = 0;
	    PL_compcv = 0;
	    SensorCall(7025);S_op_destroy(aTHX_ o);
	    SensorCall(7026);return;
	}
	PL_main_root = op_scope(sawparens(scalarvoid(o)));
	PL_curcop = &PL_compiling;
	PL_main_start = LINKLIST(PL_main_root);
	PL_main_root->op_private |= OPpREFCOUNTED;
	OpREFCNT_set(PL_main_root, 1);
	PL_main_root->op_next = 0;
	CALL_PEEP(PL_main_start);
	finalize_optree(PL_main_root);
	PL_compcv = 0;

	/* Register with debugger */
	SensorCall(7029);if (PERLDB_INTER) {
	    SensorCall(7027);CV * const cv = get_cvs("DB::postponed", 0);
	    SensorCall(7028);if (cv) {
		dSP;
		PUSHMARK(SP);
		XPUSHs(MUTABLE_SV(CopFILEGV(&PL_compiling)));
		PUTBACK;
		call_sv(MUTABLE_SV(cv), G_DISCARD);
	    }
	}
    }
SensorCall(7031);}

OP *
Perl_localize(pTHX_ OP *o, I32 lex)
{
SensorCall(7032);    dVAR;

    PERL_ARGS_ASSERT_LOCALIZE;

    SensorCall(7047);if (o->op_flags & OPf_PARENS)
/* [perl #17376]: this appears to be premature, and results in code such as
   C< our(%x); > executing in list mode rather than void mode */
#if 0
	list(o);
#else
	NOOP;
#endif
    else {
	SensorCall(7033);if ( PL_parser->bufptr > PL_parser->oldbufptr
	    && PL_parser->bufptr[-1] == ','
	    && ckWARN(WARN_PARENTHESIS))
	{
	    SensorCall(7034);char *s = PL_parser->bufptr;
	    bool sigil = FALSE;

	    /* some heuristics to detect a potential error */
	    SensorCall(7036);while (*s && (strchr(", \t\n", *s)))
		{/*227*/SensorCall(7035);s++;/*228*/}

	    SensorCall(7044);while (1) {
		SensorCall(7037);if (*s && strchr("@$%*", *s) && *++s
		       && (isALNUM(*s) || UTF8_IS_CONTINUED(*s))) {
		    SensorCall(7038);s++;
		    sigil = TRUE;
		    SensorCall(7040);while (*s && (isALNUM(*s) || UTF8_IS_CONTINUED(*s)))
			{/*229*/SensorCall(7039);s++;/*230*/}
		    SensorCall(7042);while (*s && (strchr(", \t\n", *s)))
			{/*231*/SensorCall(7041);s++;/*232*/}
		}
		else
		    {/*233*/SensorCall(7043);break;/*234*/}
	    }
	    SensorCall(7046);if (sigil && (*s == ';' || *s == '=')) {
		SensorCall(7045);Perl_warner(aTHX_ packWARN(WARN_PARENTHESIS),
				"Parentheses missing around \"%s\" list",
				lex
				    ? (PL_parser->in_my == KEY_our
					? "our"
					: PL_parser->in_my == KEY_state
					    ? "state"
					    : "my")
				    : "local");
	    }
	}
    }
    SensorCall(7048);if (lex)
	o = my(o);
    else
	o = op_lvalue(o, OP_NULL);		/* a bit kludgey */
    PL_parser->in_my = FALSE;
    PL_parser->in_my_stash = NULL;
    {OP * ReplaceReturn748 = o; SensorCall(7049); return ReplaceReturn748;}
}

OP *
Perl_jmaybe(pTHX_ OP *o)
{
    PERL_ARGS_ASSERT_JMAYBE;

    SensorCall(7050);if (o->op_type == OP_LIST) {
	SensorCall(7051);OP * const o2
	    = newSVREF(newGVOP(OP_GV, 0, gv_fetchpvs(";", GV_ADD|GV_NOTQUAL, SVt_PV)));
	o = convert(OP_JOIN, 0, op_prepend_elem(OP_LIST, o2, o));
    }
    {OP * ReplaceReturn747 = o; SensorCall(7052); return ReplaceReturn747;}
}

PERL_STATIC_INLINE OP *
S_op_std_init(pTHX_ OP *o)
{
    SensorCall(7053);I32 type = o->op_type;

    PERL_ARGS_ASSERT_OP_STD_INIT;

    SensorCall(7054);if (PL_opargs[type] & OA_RETSCALAR)
	scalar(o);
    SensorCall(7055);if (PL_opargs[type] & OA_TARGET && !o->op_targ)
	o->op_targ = pad_alloc(type, SVs_PADTMP);

    {OP * ReplaceReturn746 = o; SensorCall(7056); return ReplaceReturn746;}
}

PERL_STATIC_INLINE OP *
S_op_integerize(pTHX_ OP *o)
{
    SensorCall(7057);I32 type = o->op_type;

    PERL_ARGS_ASSERT_OP_INTEGERIZE;

    /* integerize op, unless it happens to be C<-foo>.
     * XXX should pp_i_negate() do magic string negation instead? */
    SensorCall(7059);if ((PL_opargs[type] & OA_OTHERINT) && (PL_hints & HINT_INTEGER)
	&& !(type == OP_NEGATE && cUNOPo->op_first->op_type == OP_CONST
	     && (cUNOPo->op_first->op_private & OPpCONST_BARE)))
    {
	dVAR;
	SensorCall(7058);o->op_ppaddr = PL_ppaddr[type = ++(o->op_type)];
    }

    SensorCall(7060);if (type == OP_NEGATE)
	/* XXX might want a ck_negate() for this */
	cUNOPo->op_first->op_private &= ~OPpCONST_STRICT;

    {OP * ReplaceReturn745 = o; SensorCall(7061); return ReplaceReturn745;}
}

static OP *
S_fold_constants(pTHX_ register OP *o)
{
SensorCall(7062);    dVAR;
    register OP * VOL curop;
    OP *newop;
    VOL I32 type = o->op_type;
    SV * VOL sv = NULL;
    int ret = 0;
    I32 oldscope;
    OP *old_next;
    SV * const oldwarnhook = PL_warnhook;
    SV * const olddiehook  = PL_diehook;
    COP not_compiling;
    dJMPENV;

    PERL_ARGS_ASSERT_FOLD_CONSTANTS;

    SensorCall(7064);if (!(PL_opargs[type] & OA_FOLDCONST))
	{/*555*/SensorCall(7063);goto nope;/*556*/}

    SensorCall(7068);switch (type) {
    case OP_UCFIRST:
    case OP_LCFIRST:
    case OP_UC:
    case OP_LC:
    case OP_SLT:
    case OP_SGT:
    case OP_SLE:
    case OP_SGE:
    case OP_SCMP:
    case OP_SPRINTF:
	/* XXX what about the numeric ops? */
	SensorCall(7065);if (IN_LOCALE_COMPILETIME)
	    {/*557*/SensorCall(7066);goto nope;/*558*/}
	SensorCall(7067);break;
    }

    SensorCall(7070);if (PL_parser && PL_parser->error_count)
	{/*559*/SensorCall(7069);goto nope;/*560*/}		/* Don't try to run w/ errors */

    SensorCall(7074);for (curop = LINKLIST(o); curop != o; curop = LINKLIST(curop)) {
	SensorCall(7071);const OPCODE type = curop->op_type;
	SensorCall(7073);if ((type != OP_CONST || (curop->op_private & OPpCONST_BARE)) &&
	    type != OP_LIST &&
	    type != OP_SCALAR &&
	    type != OP_NULL &&
	    type != OP_PUSHMARK)
	{
	    SensorCall(7072);goto nope;
	}
    }

    SensorCall(7075);curop = LINKLIST(o);
    old_next = o->op_next;
    o->op_next = 0;
    PL_op = curop;

    oldscope = PL_scopestack_ix;
    create_eval_scope(G_FAKINGEVAL);

    /* Verify that we don't need to save it:  */
    assert(PL_curcop == &PL_compiling);
    StructCopy(&PL_compiling, &not_compiling, COP);
    PL_curcop = &not_compiling;
    /* The above ensures that we run with all the correct hints of the
       currently compiling COP, but that IN_PERL_RUNTIME is not true. */
    assert(IN_PERL_RUNTIME);
    PL_warnhook = PERL_WARNHOOK_FATAL;
    PL_diehook  = NULL;
    JMPENV_PUSH(ret);

    SensorCall(7083);switch (ret) {
    case 0:
	CALLRUNOPS(aTHX);
	SensorCall(7076);sv = *(PL_stack_sp--);
	SensorCall(7078);if (o->op_targ && sv == PAD_SV(o->op_targ)) {	/* grab pad temp? */
#ifdef PERL_MAD
	    /* Can't simply swipe the SV from the pad, because that relies on
	       the op being freed "real soon now". Under MAD, this doesn't
	       happen (see the #ifdef below).  */
	    sv = newSVsv(sv);
#else
	    pad_swipe(o->op_targ,  FALSE);
#endif
	}
	else {/*561*/SensorCall(7077);if (SvTEMP(sv)) {			/* grab mortal temp? */
	    SvREFCNT_inc_simple_void(sv);
	    SvTEMP_off(sv);
	;/*562*/}}
	SensorCall(7079);break;
    case 3:
	/* Something tried to die.  Abandon constant folding.  */
	/* Pretend the error never happened.  */
	CLEAR_ERRSV();
	SensorCall(7080);o->op_next = old_next;
	SensorCall(7081);break;
    default:
	JMPENV_POP;
	/* Don't expect 1 (setjmp failed) or 2 (something called my_exit)  */
	PL_warnhook = oldwarnhook;
	PL_diehook  = olddiehook;
	/* XXX note that this croak may fail as we've already blown away
	 * the stack - eg any nested evals */
	SensorCall(7082);Perl_croak(aTHX_ "panic: fold_constants JMPENV_PUSH returned %d", ret);
    }
    JMPENV_POP;
    PL_warnhook = oldwarnhook;
    PL_diehook  = olddiehook;
    PL_curcop = &PL_compiling;

    SensorCall(7084);if (PL_scopestack_ix > oldscope)
	delete_eval_scope();

    SensorCall(7086);if (ret)
	{/*563*/SensorCall(7085);goto nope;/*564*/}

#ifndef PERL_MAD
    op_free(o);
#endif
    assert(sv);
    SensorCall(7087);if (type == OP_RV2GV)
	newop = newGVOP(OP_GV, 0, MUTABLE_GV(sv));
    else
	newop = newSVOP(OP_CONST, 0, MUTABLE_SV(sv));
    op_getmad(o,newop,'f');
    {OP * ReplaceReturn744 = newop; SensorCall(7088); return ReplaceReturn744;}

 nope:
    return o;
}

static OP *
S_gen_constant_list(pTHX_ register OP *o)
{
SensorCall(7089);    dVAR;
    register OP *curop;
    const I32 oldtmps_floor = PL_tmps_floor;

    list(o);
    SensorCall(7091);if (PL_parser && PL_parser->error_count)
	{/*565*/{OP * ReplaceReturn743 = o; SensorCall(7090); return ReplaceReturn743;}/*566*/}		/* Don't attempt to run with errors */

    PL_op = curop = LINKLIST(o);
    SensorCall(7092);o->op_next = 0;
    CALL_PEEP(curop);
    Perl_pp_pushmark(aTHX);
    CALLRUNOPS(aTHX);
    PL_op = curop;
    assert (!(curop->op_flags & OPf_SPECIAL));
    assert(curop->op_type == OP_RANGE);
    Perl_pp_anonlist(aTHX);
    PL_tmps_floor = oldtmps_floor;

    o->op_type = OP_RV2AV;
    o->op_ppaddr = PL_ppaddr[OP_RV2AV];
    o->op_flags &= ~OPf_REF;	/* treat \(1..2) like an ordinary list */
    o->op_flags |= OPf_PARENS;	/* and flatten \(1..2,3) */
    o->op_opt = 0;		/* needs to be revisited in rpeep() */
    curop = ((UNOP*)o)->op_first;
    ((UNOP*)o)->op_first = newSVOP(OP_CONST, 0, SvREFCNT_inc_NN(*PL_stack_sp--));
#ifdef PERL_MAD
    op_getmad(curop,o,'O');
#else
    op_free(curop);
#endif
    LINKLIST(o);
    {OP * ReplaceReturn742 = list(o); SensorCall(7093); return ReplaceReturn742;}
}

OP *
Perl_convert(pTHX_ I32 type, I32 flags, OP *o)
{
SensorCall(7094);    dVAR;
    SensorCall(7095);if (type < 0) type = -type, flags |= OPf_SPECIAL;
    SensorCall(7096);if (!o || o->op_type != OP_LIST)
	o = newLISTOP(OP_LIST, 0, o, NULL);
    else
	o->op_flags &= ~OPf_WANT;

    SensorCall(7100);if (!(PL_opargs[type] & OA_MARK))
	op_null(cLISTOPo->op_first);
    else {
	SensorCall(7097);OP * const kid2 = cLISTOPo->op_first->op_sibling;
	SensorCall(7099);if (kid2 && kid2->op_type == OP_COREARGS) {
	    op_null(cLISTOPo->op_first);
	    SensorCall(7098);kid2->op_private |= OPpCOREARGS_PUSHMARK;
	}
    }	

    SensorCall(7101);o->op_type = (OPCODE)type;
    o->op_ppaddr = PL_ppaddr[type];
    o->op_flags |= flags;

    o = CHECKOP(type, o);
    SensorCall(7103);if (o->op_type != (unsigned)type)
	{/*177*/{OP * ReplaceReturn741 = o; SensorCall(7102); return ReplaceReturn741;}/*178*/}

    {OP * ReplaceReturn740 = fold_constants(op_integerize(op_std_init(o))); SensorCall(7104); return ReplaceReturn740;}
}

/*
=head1 Optree Manipulation Functions
*/

/* List constructors */

/*
=for apidoc Am|OP *|op_append_elem|I32 optype|OP *first|OP *last

Append an item to the list of ops contained directly within a list-type
op, returning the lengthened list.  I<first> is the list-type op,
and I<last> is the op to append to the list.  I<optype> specifies the
intended opcode for the list.  If I<first> is not already a list of the
right type, it will be upgraded into one.  If either I<first> or I<last>
is null, the other is returned unchanged.

=cut
*/

OP *
Perl_op_append_elem(pTHX_ I32 type, OP *first, OP *last)
{
    SensorCall(7105);if (!first)
	{/*315*/{OP * ReplaceReturn739 = last; SensorCall(7106); return ReplaceReturn739;}/*316*/}

    SensorCall(7108);if (!last)
	{/*317*/{OP * ReplaceReturn738 = first; SensorCall(7107); return ReplaceReturn738;}/*318*/}

    SensorCall(7110);if (first->op_type != (unsigned)type
	|| (type == OP_LIST && (first->op_flags & OPf_PARENS)))
    {
	{OP * ReplaceReturn737 = newLISTOP(type, 0, first, last); SensorCall(7109); return ReplaceReturn737;}
    }

    SensorCall(7113);if (first->op_flags & OPf_KIDS)
	{/*319*/SensorCall(7111);((LISTOP*)first)->op_last->op_sibling = last;/*320*/}
    else {
	SensorCall(7112);first->op_flags |= OPf_KIDS;
	((LISTOP*)first)->op_first = last;
    }
    SensorCall(7114);((LISTOP*)first)->op_last = last;
    {OP * ReplaceReturn736 = first; SensorCall(7115); return ReplaceReturn736;}
}

/*
=for apidoc Am|OP *|op_append_list|I32 optype|OP *first|OP *last

Concatenate the lists of ops contained directly within two list-type ops,
returning the combined list.  I<first> and I<last> are the list-type ops
to concatenate.  I<optype> specifies the intended opcode for the list.
If either I<first> or I<last> is not already a list of the right type,
it will be upgraded into one.  If either I<first> or I<last> is null,
the other is returned unchanged.

=cut
*/

OP *
Perl_op_append_list(pTHX_ I32 type, OP *first, OP *last)
{
    SensorCall(7116);if (!first)
	{/*321*/{OP * ReplaceReturn735 = last; SensorCall(7117); return ReplaceReturn735;}/*322*/}

    SensorCall(7119);if (!last)
	{/*323*/{OP * ReplaceReturn734 = first; SensorCall(7118); return ReplaceReturn734;}/*324*/}

    SensorCall(7120);if (first->op_type != (unsigned)type)
	return op_prepend_elem(type, first, last);

    SensorCall(7121);if (last->op_type != (unsigned)type)
	return op_append_elem(type, first, last);

    SensorCall(7122);((LISTOP*)first)->op_last->op_sibling = ((LISTOP*)last)->op_first;
    ((LISTOP*)first)->op_last = ((LISTOP*)last)->op_last;
    first->op_flags |= (last->op_flags & OPf_KIDS);

#ifdef PERL_MAD
    if (((LISTOP*)last)->op_first && first->op_madprop) {
	MADPROP *mp = ((LISTOP*)last)->op_first->op_madprop;
	if (mp) {
	    while (mp->mad_next)
		mp = mp->mad_next;
	    mp->mad_next = first->op_madprop;
	}
	else {
	    ((LISTOP*)last)->op_first->op_madprop = first->op_madprop;
	}
    }
    first->op_madprop = last->op_madprop;
    last->op_madprop = 0;
#endif

    S_op_destroy(aTHX_ last);

    {OP * ReplaceReturn733 = first; SensorCall(7123); return ReplaceReturn733;}
}

/*
=for apidoc Am|OP *|op_prepend_elem|I32 optype|OP *first|OP *last

Prepend an item to the list of ops contained directly within a list-type
op, returning the lengthened list.  I<first> is the op to prepend to the
list, and I<last> is the list-type op.  I<optype> specifies the intended
opcode for the list.  If I<last> is not already a list of the right type,
it will be upgraded into one.  If either I<first> or I<last> is null,
the other is returned unchanged.

=cut
*/

OP *
Perl_op_prepend_elem(pTHX_ I32 type, OP *first, OP *last)
{
    SensorCall(7124);if (!first)
	{/*401*/{OP * ReplaceReturn732 = last; SensorCall(7125); return ReplaceReturn732;}/*402*/}

    SensorCall(7127);if (!last)
	{/*403*/{OP * ReplaceReturn731 = first; SensorCall(7126); return ReplaceReturn731;}/*404*/}

    SensorCall(7136);if (last->op_type == (unsigned)type) {
	SensorCall(7128);if (type == OP_LIST) {	/* already a PUSHMARK there */
	    SensorCall(7129);first->op_sibling = ((LISTOP*)last)->op_first->op_sibling;
	    ((LISTOP*)last)->op_first->op_sibling = first;
            SensorCall(7130);if (!(first->op_flags & OPf_PARENS))
                last->op_flags &= ~OPf_PARENS;
	}
	else {
	    SensorCall(7131);if (!(last->op_flags & OPf_KIDS)) {
		SensorCall(7132);((LISTOP*)last)->op_last = first;
		last->op_flags |= OPf_KIDS;
	    }
	    SensorCall(7133);first->op_sibling = ((LISTOP*)last)->op_first;
	    ((LISTOP*)last)->op_first = first;
	}
	SensorCall(7134);last->op_flags |= OPf_KIDS;
	{OP * ReplaceReturn730 = last; SensorCall(7135); return ReplaceReturn730;}
    }

    {OP * ReplaceReturn729 = newLISTOP(type, 0, first, last); SensorCall(7137); return ReplaceReturn729;}
}

/* Constructors */

#ifdef PERL_MAD
 
TOKEN *
Perl_newTOKEN(pTHX_ I32 optype, YYSTYPE lval, MADPROP* madprop)
{
    TOKEN *tk;
    Newxz(tk, 1, TOKEN);
    tk->tk_type = (OPCODE)optype;
    tk->tk_type = 12345;
    tk->tk_lval = lval;
    tk->tk_mad = madprop;
    return tk;
}

void
Perl_token_free(pTHX_ TOKEN* tk)
{
    PERL_ARGS_ASSERT_TOKEN_FREE;

    if (tk->tk_type != 12345)
	return;
    mad_free(tk->tk_mad);
    Safefree(tk);
}

void
Perl_token_getmad(pTHX_ TOKEN* tk, OP* o, char slot)
{
    MADPROP* mp;
    MADPROP* tm;

    PERL_ARGS_ASSERT_TOKEN_GETMAD;

    if (tk->tk_type != 12345) {
	Perl_warner(aTHX_ packWARN(WARN_MISC),
	     "Invalid TOKEN object ignored");
	return;
    }
    tm = tk->tk_mad;
    if (!tm)
	return;

    /* faked up qw list? */
    if (slot == '(' &&
	tm->mad_type == MAD_SV &&
	SvPVX((SV *)tm->mad_val)[0] == 'q')
	    slot = 'x';

    if (o) {
	mp = o->op_madprop;
	if (mp) {
	    for (;;) {
		/* pretend constant fold didn't happen? */
		if (mp->mad_key == 'f' &&
		    (o->op_type == OP_CONST ||
		     o->op_type == OP_GV) )
		{
		    token_getmad(tk,(OP*)mp->mad_val,slot);
		    return;
		}
		if (!mp->mad_next)
		    break;
		mp = mp->mad_next;
	    }
	    mp->mad_next = tm;
	    mp = mp->mad_next;
	}
	else {
	    o->op_madprop = tm;
	    mp = o->op_madprop;
	}
	if (mp->mad_key == 'X')
	    mp->mad_key = slot;	/* just change the first one */

	tk->tk_mad = 0;
    }
    else
	mad_free(tm);
    Safefree(tk);
}

void
Perl_op_getmad_weak(pTHX_ OP* from, OP* o, char slot)
{
    MADPROP* mp;
    if (!from)
	return;
    if (o) {
	mp = o->op_madprop;
	if (mp) {
	    for (;;) {
		/* pretend constant fold didn't happen? */
		if (mp->mad_key == 'f' &&
		    (o->op_type == OP_CONST ||
		     o->op_type == OP_GV) )
		{
		    op_getmad(from,(OP*)mp->mad_val,slot);
		    return;
		}
		if (!mp->mad_next)
		    break;
		mp = mp->mad_next;
	    }
	    mp->mad_next = newMADPROP(slot,MAD_OP,from,0);
	}
	else {
	    o->op_madprop = newMADPROP(slot,MAD_OP,from,0);
	}
    }
}

void
Perl_op_getmad(pTHX_ OP* from, OP* o, char slot)
{
    MADPROP* mp;
    if (!from)
	return;
    if (o) {
	mp = o->op_madprop;
	if (mp) {
	    for (;;) {
		/* pretend constant fold didn't happen? */
		if (mp->mad_key == 'f' &&
		    (o->op_type == OP_CONST ||
		     o->op_type == OP_GV) )
		{
		    op_getmad(from,(OP*)mp->mad_val,slot);
		    return;
		}
		if (!mp->mad_next)
		    break;
		mp = mp->mad_next;
	    }
	    mp->mad_next = newMADPROP(slot,MAD_OP,from,1);
	}
	else {
	    o->op_madprop = newMADPROP(slot,MAD_OP,from,1);
	}
    }
    else {
	PerlIO_printf(PerlIO_stderr(),
		      "DESTROYING op = %0"UVxf"\n", PTR2UV(from));
	op_free(from);
    }
}

void
Perl_prepend_madprops(pTHX_ MADPROP* mp, OP* o, char slot)
{
    MADPROP* tm;
    if (!mp || !o)
	return;
    if (slot)
	mp->mad_key = slot;
    tm = o->op_madprop;
    o->op_madprop = mp;
    for (;;) {
	if (!mp->mad_next)
	    break;
	mp = mp->mad_next;
    }
    mp->mad_next = tm;
}

void
Perl_append_madprops(pTHX_ MADPROP* tm, OP* o, char slot)
{
    if (!o)
	return;
    addmad(tm, &(o->op_madprop), slot);
}

void
Perl_addmad(pTHX_ MADPROP* tm, MADPROP** root, char slot)
{
    MADPROP* mp;
    if (!tm || !root)
	return;
    if (slot)
	tm->mad_key = slot;
    mp = *root;
    if (!mp) {
	*root = tm;
	return;
    }
    for (;;) {
	if (!mp->mad_next)
	    break;
	mp = mp->mad_next;
    }
    mp->mad_next = tm;
}

MADPROP *
Perl_newMADsv(pTHX_ char key, SV* sv)
{
    PERL_ARGS_ASSERT_NEWMADSV;

    return newMADPROP(key, MAD_SV, sv, 0);
}

MADPROP *
Perl_newMADPROP(pTHX_ char key, char type, void* val, I32 vlen)
{
    MADPROP *const mp = (MADPROP *) PerlMemShared_malloc(sizeof(MADPROP));
    mp->mad_next = 0;
    mp->mad_key = key;
    mp->mad_vlen = vlen;
    mp->mad_type = type;
    mp->mad_val = val;
/*    PerlIO_printf(PerlIO_stderr(), "NEW  mp = %0x\n", mp);  */
    return mp;
}

void
Perl_mad_free(pTHX_ MADPROP* mp)
{
/*    PerlIO_printf(PerlIO_stderr(), "FREE mp = %0x\n", mp); */
    if (!mp)
	return;
    if (mp->mad_next)
	mad_free(mp->mad_next);
/*    if (PL_parser && PL_parser->lex_state != LEX_NOTPARSING && mp->mad_vlen)
	PerlIO_printf(PerlIO_stderr(), "DESTROYING '%c'=<%s>\n", mp->mad_key & 255, mp->mad_val); */
    switch (mp->mad_type) {
    case MAD_NULL:
	break;
    case MAD_PV:
	Safefree((char*)mp->mad_val);
	break;
    case MAD_OP:
	if (mp->mad_vlen)	/* vlen holds "strong/weak" boolean */
	    op_free((OP*)mp->mad_val);
	break;
    case MAD_SV:
	sv_free(MUTABLE_SV(mp->mad_val));
	break;
    default:
	PerlIO_printf(PerlIO_stderr(), "Unrecognized mad\n");
	break;
    }
    PerlMemShared_free(mp);
}

#endif

/*
=head1 Optree construction

=for apidoc Am|OP *|newNULLLIST

Constructs, checks, and returns a new C<stub> op, which represents an
empty list expression.

=cut
*/

OP *
Perl_newNULLLIST(pTHX)
{
    {OP * ReplaceReturn728 = newOP(OP_STUB, 0); SensorCall(7138); return ReplaceReturn728;}
}

static OP *
S_force_list(pTHX_ OP *o)
{
    SensorCall(7139);if (!o || o->op_type != OP_LIST)
	o = newLISTOP(OP_LIST, 0, o, NULL);
    op_null(o);
    {OP * ReplaceReturn727 = o; SensorCall(7140); return ReplaceReturn727;}
}

/*
=for apidoc Am|OP *|newLISTOP|I32 type|I32 flags|OP *first|OP *last

Constructs, checks, and returns an op of any list type.  I<type> is
the opcode.  I<flags> gives the eight bits of C<op_flags>, except that
C<OPf_KIDS> will be set automatically if required.  I<first> and I<last>
supply up to two ops to be direct children of the list op; they are
consumed by this function and become part of the constructed op tree.

=cut
*/

OP *
Perl_newLISTOP(pTHX_ I32 type, I32 flags, OP *first, OP *last)
{
SensorCall(7141);    dVAR;
    LISTOP *listop;

    assert((PL_opargs[type] & OA_CLASS_MASK) == OA_LISTOP);

    NewOp(1101, listop, 1, LISTOP);

    listop->op_type = (OPCODE)type;
    listop->op_ppaddr = PL_ppaddr[type];
    SensorCall(7142);if (first || last)
	flags |= OPf_KIDS;
    SensorCall(7143);listop->op_flags = (U8)flags;

    SensorCall(7149);if (!last && first)
	{/*271*/SensorCall(7144);last = first;/*272*/}
    else {/*273*/SensorCall(7145);if (!first && last)
	{/*275*/SensorCall(7146);first = last;/*276*/}
    else {/*277*/SensorCall(7147);if (first)
	{/*279*/SensorCall(7148);first->op_sibling = last;/*280*/}/*278*/}/*274*/}
    SensorCall(7150);listop->op_first = first;
    listop->op_last = last;
    SensorCall(7154);if (type == OP_LIST) {
	SensorCall(7151);OP* const pushop = newOP(OP_PUSHMARK, 0);
	pushop->op_sibling = first;
	listop->op_first = pushop;
	listop->op_flags |= OPf_KIDS;
	SensorCall(7153);if (!last)
	    {/*281*/SensorCall(7152);listop->op_last = pushop;/*282*/}
    }

    {OP * ReplaceReturn726 = CHECKOP(type, listop); SensorCall(7155); return ReplaceReturn726;}
}

/*
=for apidoc Am|OP *|newOP|I32 type|I32 flags

Constructs, checks, and returns an op of any base type (any type that
has no extra fields).  I<type> is the opcode.  I<flags> gives the
eight bits of C<op_flags>, and, shifted up eight bits, the eight bits
of C<op_private>.

=cut
*/

OP *
Perl_newOP(pTHX_ I32 type, I32 flags)
{
SensorCall(7156);    dVAR;
    OP *o;

    SensorCall(7158);if (type == -OP_ENTEREVAL) {
	SensorCall(7157);type = OP_ENTEREVAL;
	flags |= OPpEVAL_BYTES<<8;
    }

    assert((PL_opargs[type] & OA_CLASS_MASK) == OA_BASEOP
	|| (PL_opargs[type] & OA_CLASS_MASK) == OA_BASEOP_OR_UNOP
	|| (PL_opargs[type] & OA_CLASS_MASK) == OA_FILESTATOP
	|| (PL_opargs[type] & OA_CLASS_MASK) == OA_LOOPEXOP);

    NewOp(1101, o, 1, OP);
    SensorCall(7159);o->op_type = (OPCODE)type;
    o->op_ppaddr = PL_ppaddr[type];
    o->op_flags = (U8)flags;
    o->op_latefree = 0;
    o->op_latefreed = 0;
    o->op_attached = 0;

    o->op_next = o;
    o->op_private = (U8)(0 | (flags >> 8));
    SensorCall(7160);if (PL_opargs[type] & OA_RETSCALAR)
	scalar(o);
    SensorCall(7161);if (PL_opargs[type] & OA_TARGET)
	o->op_targ = pad_alloc(type, SVs_PADTMP);
    {OP * ReplaceReturn725 = CHECKOP(type, o); SensorCall(7162); return ReplaceReturn725;}
}

/*
=for apidoc Am|OP *|newUNOP|I32 type|I32 flags|OP *first

Constructs, checks, and returns an op of any unary type.  I<type> is
the opcode.  I<flags> gives the eight bits of C<op_flags>, except that
C<OPf_KIDS> will be set automatically if required, and, shifted up eight
bits, the eight bits of C<op_private>, except that the bit with value 1
is automatically set.  I<first> supplies an optional op to be the direct
child of the unary op; it is consumed by this function and become part
of the constructed op tree.

=cut
*/

OP *
Perl_newUNOP(pTHX_ I32 type, I32 flags, OP *first)
{
SensorCall(7163);    dVAR;
    UNOP *unop;

    SensorCall(7165);if (type == -OP_ENTEREVAL) {
	SensorCall(7164);type = OP_ENTEREVAL;
	flags |= OPpEVAL_BYTES<<8;
    }

    assert((PL_opargs[type] & OA_CLASS_MASK) == OA_UNOP
	|| (PL_opargs[type] & OA_CLASS_MASK) == OA_BASEOP_OR_UNOP
	|| (PL_opargs[type] & OA_CLASS_MASK) == OA_FILESTATOP
	|| (PL_opargs[type] & OA_CLASS_MASK) == OA_LOOPEXOP
	|| type == OP_SASSIGN
	|| type == OP_ENTERTRY
	|| type == OP_NULL );

    SensorCall(7166);if (!first)
	first = newOP(OP_STUB, 0);
    SensorCall(7167);if (PL_opargs[type] & OA_MARK)
	first = force_list(first);

    NewOp(1101, unop, 1, UNOP);
    SensorCall(7168);unop->op_type = (OPCODE)type;
    unop->op_ppaddr = PL_ppaddr[type];
    unop->op_first = first;
    unop->op_flags = (U8)(flags | OPf_KIDS);
    unop->op_private = (U8)(1 | (flags >> 8));
    unop = (UNOP*) CHECKOP(type, unop);
    SensorCall(7170);if (unop->op_next)
	{/*293*/{OP * ReplaceReturn724 = (OP*)unop; SensorCall(7169); return ReplaceReturn724;}/*294*/}

    {OP * ReplaceReturn723 = fold_constants(op_integerize(op_std_init((OP *) unop))); SensorCall(7171); return ReplaceReturn723;}
}

/*
=for apidoc Am|OP *|newBINOP|I32 type|I32 flags|OP *first|OP *last

Constructs, checks, and returns an op of any binary type.  I<type>
is the opcode.  I<flags> gives the eight bits of C<op_flags>, except
that C<OPf_KIDS> will be set automatically, and, shifted up eight bits,
the eight bits of C<op_private>, except that the bit with value 1 or
2 is automatically set as required.  I<first> and I<last> supply up to
two ops to be the direct children of the binary op; they are consumed
by this function and become part of the constructed op tree.

=cut
*/

OP *
Perl_newBINOP(pTHX_ I32 type, I32 flags, OP *first, OP *last)
{
SensorCall(7172);    dVAR;
    BINOP *binop;

    assert((PL_opargs[type] & OA_CLASS_MASK) == OA_BINOP
	|| type == OP_SASSIGN || type == OP_NULL );

    NewOp(1101, binop, 1, BINOP);

    SensorCall(7173);if (!first)
	first = newOP(OP_NULL, 0);

    SensorCall(7174);binop->op_type = (OPCODE)type;
    binop->op_ppaddr = PL_ppaddr[type];
    binop->op_first = first;
    binop->op_flags = (U8)(flags | OPf_KIDS);
    SensorCall(7177);if (!last) {
	SensorCall(7175);last = first;
	binop->op_private = (U8)(1 | (flags >> 8));
    }
    else {
	SensorCall(7176);binop->op_private = (U8)(2 | (flags >> 8));
	first->op_sibling = last;
    }

    SensorCall(7178);binop = (BINOP*)CHECKOP(type, binop);
    SensorCall(7180);if (binop->op_next || binop->op_type != (OPCODE)type)
	{/*259*/{OP * ReplaceReturn722 = (OP*)binop; SensorCall(7179); return ReplaceReturn722;}/*260*/}

    SensorCall(7181);binop->op_last = binop->op_first->op_sibling;

    {OP * ReplaceReturn721 = fold_constants(op_integerize(op_std_init((OP *)binop))); SensorCall(7182); return ReplaceReturn721;}
}

static int uvcompare(const void *a, const void *b)
    __attribute__nonnull__(1)
    __attribute__nonnull__(2)
    __attribute__pure__;
static int uvcompare(const void *a, const void *b)
{
    SensorCall(7183);if (*((const UV *)a) < (*(const UV *)b))
	{/*735*/{int  ReplaceReturn720 = -1; SensorCall(7184); return ReplaceReturn720;}/*736*/}
    SensorCall(7186);if (*((const UV *)a) > (*(const UV *)b))
	{/*737*/{int  ReplaceReturn719 = 1; SensorCall(7185); return ReplaceReturn719;}/*738*/}
    SensorCall(7188);if (*((const UV *)a+1) < (*(const UV *)b+1))
	{/*739*/{int  ReplaceReturn718 = -1; SensorCall(7187); return ReplaceReturn718;}/*740*/}
    SensorCall(7190);if (*((const UV *)a+1) > (*(const UV *)b+1))
	{/*741*/{int  ReplaceReturn717 = 1; SensorCall(7189); return ReplaceReturn717;}/*742*/}
    {int  ReplaceReturn716 = 0; SensorCall(7191); return ReplaceReturn716;}
}

static OP *
S_pmtrans(pTHX_ OP *o, OP *expr, OP *repl)
{
SensorCall(7192);    dVAR;
    SV * const tstr = ((SVOP*)expr)->op_sv;
    SV * const rstr =
#ifdef PERL_MAD
			(repl->op_type == OP_NULL)
			    ? ((SVOP*)((LISTOP*)repl)->op_first)->op_sv :
#endif
			      ((SVOP*)repl)->op_sv;
    STRLEN tlen;
    STRLEN rlen;
    const U8 *t = (U8*)SvPV_const(tstr, tlen);
    const U8 *r = (U8*)SvPV_const(rstr, rlen);
    register I32 i;
    register I32 j;
    I32 grows = 0;
    register short *tbl;

    const I32 complement = o->op_private & OPpTRANS_COMPLEMENT;
    const I32 squash     = o->op_private & OPpTRANS_SQUASH;
    I32 del              = o->op_private & OPpTRANS_DELETE;
    SV* swash;

    PERL_ARGS_ASSERT_PMTRANS;

    PL_hints |= HINT_BLOCK_SCOPE;

    SensorCall(7193);if (SvUTF8(tstr))
        o->op_private |= OPpTRANS_FROM_UTF;

    SensorCall(7194);if (SvUTF8(rstr))
        o->op_private |= OPpTRANS_TO_UTF;

    SensorCall(7269);if (o->op_private & (OPpTRANS_FROM_UTF|OPpTRANS_TO_UTF)) {
	SensorCall(7195);SV* const listsv = newSVpvs("# comment\n");
	SV* transv = NULL;
	const U8* tend = t + tlen;
	const U8* rend = r + rlen;
	STRLEN ulen;
	UV tfirst = 1;
	UV tlast = 0;
	IV tdiff;
	UV rfirst = 1;
	UV rlast = 0;
	IV rdiff;
	IV diff;
	I32 none = 0;
	U32 max = 0;
	I32 bits;
	I32 havefinal = 0;
	U32 final = 0;
	const I32 from_utf  = o->op_private & OPpTRANS_FROM_UTF;
	const I32 to_utf    = o->op_private & OPpTRANS_TO_UTF;
	U8* tsave = NULL;
	U8* rsave = NULL;
	const U32 flags = UTF8_ALLOW_DEFAULT;

	SensorCall(7197);if (!from_utf) {
	    SensorCall(7196);STRLEN len = tlen;
	    t = tsave = bytes_to_utf8(t, &len);
	    tend = t + len;
	}
	SensorCall(7199);if (!to_utf && rlen) {
	    SensorCall(7198);STRLEN len = rlen;
	    r = rsave = bytes_to_utf8(r, &len);
	    rend = r + len;
	}

/* There are several snags with this code on EBCDIC:
   1. 0xFF is a legal UTF-EBCDIC byte (there are no illegal bytes).
   2. scan_const() in toke.c has encoded chars in native encoding which makes
      ranges at least in EBCDIC 0..255 range the bottom odd.
*/

	SensorCall(7220);if (complement) {
	    SensorCall(7200);U8 tmpbuf[UTF8_MAXBYTES+1];
	    UV *cp;
	    UV nextmin = 0;
	    Newx(cp, 2*tlen, UV);
	    i = 0;
	    transv = newSVpvs("");
	    SensorCall(7206);while (t < tend) {
		SensorCall(7201);cp[2*i] = utf8n_to_uvuni(t, tend-t, &ulen, flags);
		t += ulen;
		SensorCall(7204);if (t < tend && NATIVE_TO_UTF(*t) == 0xff) {
		    SensorCall(7202);t++;
		    cp[2*i+1] = utf8n_to_uvuni(t, tend-t, &ulen, flags);
		    t += ulen;
		}
		else {
		 SensorCall(7203);cp[2*i+1] = cp[2*i];
		}
		SensorCall(7205);i++;
	    }
	    SensorCall(7207);qsort(cp, i, 2*sizeof(UV), uvcompare);
	    SensorCall(7216);for (j = 0; j < i; j++) {
		SensorCall(7208);UV  val = cp[2*j];
		diff = val - nextmin;
		SensorCall(7212);if (diff > 0) {
		    SensorCall(7209);t = uvuni_to_utf8(tmpbuf,nextmin);
		    sv_catpvn(transv, (char*)tmpbuf, t - tmpbuf);
		    SensorCall(7211);if (diff > 1) {
			SensorCall(7210);U8  range_mark = UTF_TO_NATIVE(0xff);
			t = uvuni_to_utf8(tmpbuf, val - 1);
			sv_catpvn(transv, (char *)&range_mark, 1);
			sv_catpvn(transv, (char*)tmpbuf, t - tmpbuf);
		    }
	        }
		SensorCall(7213);val = cp[2*j+1];
		SensorCall(7215);if (val >= nextmin)
		    {/*613*/SensorCall(7214);nextmin = val + 1;/*614*/}
	    }
	    SensorCall(7217);t = uvuni_to_utf8(tmpbuf,nextmin);
	    sv_catpvn(transv, (char*)tmpbuf, t - tmpbuf);
	    {
		U8 range_mark = UTF_TO_NATIVE(0xff);
		sv_catpvn(transv, (char *)&range_mark, 1);
	    }
	    t = uvuni_to_utf8(tmpbuf, 0x7fffffff);
	    sv_catpvn(transv, (char*)tmpbuf, t - tmpbuf);
	    t = (const U8*)SvPVX_const(transv);
	    tlen = SvCUR(transv);
	    tend = t + tlen;
	    Safefree(cp);
	}
	else {/*615*/SensorCall(7218);if (!rlen && !del) {
	    SensorCall(7219);r = t; rlen = tlen; rend = tend;
	;/*616*/}}
	SensorCall(7223);if (!squash) {
		SensorCall(7221);if ((!rlen && !del) || t == r ||
		    (tlen == rlen && memEQ((char *)t, (char *)r, tlen)))
		{
		    SensorCall(7222);o->op_private |= OPpTRANS_IDENTICAL;
		}
	}

	SensorCall(7256);while (t < tend || tfirst <= tlast) {
	    /* see if we need more "t" chars */
	    SensorCall(7224);if (tfirst > tlast) {
		SensorCall(7225);tfirst = (I32)utf8n_to_uvuni(t, tend - t, &ulen, flags);
		t += ulen;
		SensorCall(7228);if (t < tend && NATIVE_TO_UTF(*t) == 0xff) {	/* illegal utf8 val indicates range */
		    SensorCall(7226);t++;
		    tlast = (I32)utf8n_to_uvuni(t, tend - t, &ulen, flags);
		    t += ulen;
		}
		else
		    {/*617*/SensorCall(7227);tlast = tfirst;/*618*/}
	    }

	    /* now see if we need more "r" chars */
	    SensorCall(7237);if (rfirst > rlast) {
		SensorCall(7229);if (r < rend) {
		    SensorCall(7230);rfirst = (I32)utf8n_to_uvuni(r, rend - r, &ulen, flags);
		    r += ulen;
		    SensorCall(7233);if (r < rend && NATIVE_TO_UTF(*r) == 0xff) {	/* illegal utf8 val indicates range */
			SensorCall(7231);r++;
			rlast = (I32)utf8n_to_uvuni(r, rend - r, &ulen, flags);
			r += ulen;
		    }
		    else
			{/*619*/SensorCall(7232);rlast = rfirst;/*620*/}
		}
		else {
		    SensorCall(7234);if (!havefinal++)
			{/*621*/SensorCall(7235);final = rlast;/*622*/}
		    SensorCall(7236);rfirst = rlast = 0xffffffff;
		}
	    }

	    /* now see which range will peter our first, if either. */
	    SensorCall(7238);tdiff = tlast - tfirst;
	    rdiff = rlast - rfirst;

	    SensorCall(7241);if (tdiff <= rdiff)
		{/*623*/SensorCall(7239);diff = tdiff;/*624*/}
	    else
		{/*625*/SensorCall(7240);diff = rdiff;/*626*/}

	    SensorCall(7254);if (rfirst == 0xffffffff) {
		SensorCall(7242);diff = tdiff;	/* oops, pretend rdiff is infinite */
		SensorCall(7245);if (diff > 0)
		    {/*627*/SensorCall(7243);Perl_sv_catpvf(aTHX_ listsv, "%04lx\t%04lx\tXXXX\n",
				   (long)tfirst, (long)tlast);/*628*/}
		else
		    {/*629*/SensorCall(7244);Perl_sv_catpvf(aTHX_ listsv, "%04lx\t\tXXXX\n", (long)tfirst);/*630*/}
	    }
	    else {
		SensorCall(7246);if (diff > 0)
		    {/*631*/SensorCall(7247);Perl_sv_catpvf(aTHX_ listsv, "%04lx\t%04lx\t%04lx\n",
				   (long)tfirst, (long)(tfirst + diff),
				   (long)rfirst);/*632*/}
		else
		    {/*633*/SensorCall(7248);Perl_sv_catpvf(aTHX_ listsv, "%04lx\t\t%04lx\n",
				   (long)tfirst, (long)rfirst);/*634*/}

		SensorCall(7250);if (rfirst + diff > max)
		    {/*635*/SensorCall(7249);max = rfirst + diff;/*636*/}
		SensorCall(7252);if (!grows)
		    {/*637*/SensorCall(7251);grows = (tfirst < rfirst &&
			     UNISKIP(tfirst) < UNISKIP(rfirst + diff));/*638*/}
		SensorCall(7253);rfirst += diff + 1;
	    }
	    SensorCall(7255);tfirst += diff + 1;
	}

	SensorCall(7257);none = ++max;
	SensorCall(7259);if (del)
	    {/*639*/SensorCall(7258);del = ++max;/*640*/}

	SensorCall(7264);if (max > 0xffff)
	    {/*641*/SensorCall(7260);bits = 32;/*642*/}
	else {/*643*/SensorCall(7261);if (max > 0xff)
	    {/*645*/SensorCall(7262);bits = 16;/*646*/}
	else
	    {/*647*/SensorCall(7263);bits = 8;/*648*/}/*644*/}

	SensorCall(7265);swash = MUTABLE_SV(swash_init("utf8", "", listsv, bits, none));
#ifdef USE_ITHREADS
	cPADOPo->op_padix = pad_alloc(OP_TRANS, SVs_PADTMP);
	SvREFCNT_dec(PAD_SVl(cPADOPo->op_padix));
	PAD_SETSV(cPADOPo->op_padix, swash);
	SvPADTMP_on(swash);
	SvREADONLY_on(swash);
#else
	cSVOPo->op_sv = swash;
#endif
	SvREFCNT_dec(listsv);
	SvREFCNT_dec(transv);

	SensorCall(7266);if (!del && havefinal && rlen)
	    (void)hv_store(MUTABLE_HV(SvRV(swash)), "FINAL", 5,
			   newSVuv((UV)final), 0);

	SensorCall(7267);if (grows)
	    o->op_private |= OPpTRANS_GROWS;

	Safefree(tsave);
	Safefree(rsave);

#ifdef PERL_MAD
	op_getmad(expr,o,'e');
	op_getmad(repl,o,'r');
#else
	op_free(expr);
	op_free(repl);
#endif
	{OP * ReplaceReturn715 = o; SensorCall(7268); return ReplaceReturn715;}
    }

    SensorCall(7270);tbl = (short*)PerlMemShared_calloc(
	(o->op_private & OPpTRANS_COMPLEMENT) &&
	    !(o->op_private & OPpTRANS_DELETE) ? 258 : 256,
	sizeof(short));
    cPVOPo->op_pv = (char*)tbl;
    SensorCall(7312);if (complement) {
	SensorCall(7271);for (i = 0; i < (I32)tlen; i++)
	    {/*649*/SensorCall(7272);tbl[t[i]] = -1;/*650*/}
	SensorCall(7283);for (i = 0, j = 0; i < 256; i++) {
	    SensorCall(7273);if (!tbl[i]) {
		SensorCall(7274);if (j >= (I32)rlen) {
		    SensorCall(7275);if (del)
			{/*651*/SensorCall(7276);tbl[i] = -2;/*652*/}
		    else {/*653*/SensorCall(7277);if (rlen)
			{/*655*/SensorCall(7278);tbl[i] = r[j-1];/*656*/}
		    else
			{/*657*/SensorCall(7279);tbl[i] = (short)i;/*658*/}/*654*/}
		}
		else {
		    SensorCall(7280);if (i < 128 && r[j] >= 128)
			{/*659*/SensorCall(7281);grows = 1;/*660*/}
		    SensorCall(7282);tbl[i] = r[j++];
		}
	    }
	}
	SensorCall(7293);if (!del) {
	    SensorCall(7284);if (!rlen) {
		SensorCall(7285);j = rlen;
		SensorCall(7286);if (!squash)
		    o->op_private |= OPpTRANS_IDENTICAL;
	    }
	    else {/*661*/SensorCall(7287);if (j >= (I32)rlen)
		{/*663*/SensorCall(7288);j = rlen - 1;/*664*/}
	    else {
		SensorCall(7289);tbl = 
		    (short *)
		    PerlMemShared_realloc(tbl,
					  (0x101+rlen-j) * sizeof(short));
		cPVOPo->op_pv = (char*)tbl;
	    ;/*662*/}}
	    SensorCall(7290);tbl[0x100] = (short)(rlen - j);
	    SensorCall(7292);for (i=0; i < (I32)rlen - j; i++)
		{/*665*/SensorCall(7291);tbl[0x101+i] = r[j+i];/*666*/}
	}
    }
    else {
	SensorCall(7294);if (!rlen && !del) {
	    SensorCall(7295);r = t; rlen = tlen;
	    SensorCall(7296);if (!squash)
		o->op_private |= OPpTRANS_IDENTICAL;
	}
	else {/*667*/SensorCall(7297);if (!squash && rlen == tlen && memEQ((char*)t, (char*)r, tlen)) {
	    SensorCall(7298);o->op_private |= OPpTRANS_IDENTICAL;
	;/*668*/}}
	SensorCall(7300);for (i = 0; i < 256; i++)
	    {/*669*/SensorCall(7299);tbl[i] = -1;/*670*/}
	SensorCall(7311);for (i = 0, j = 0; i < (I32)tlen; i++,j++) {
	    SensorCall(7301);if (j >= (I32)rlen) {
		SensorCall(7302);if (del) {
		    SensorCall(7303);if (tbl[t[i]] == -1)
			{/*671*/SensorCall(7304);tbl[t[i]] = -2;/*672*/}
		    SensorCall(7305);continue;
		}
		SensorCall(7306);--j;
	    }
	    SensorCall(7310);if (tbl[t[i]] == -1) {
		SensorCall(7307);if (t[i] < 128 && r[j] >= 128)
		    {/*673*/SensorCall(7308);grows = 1;/*674*/}
		SensorCall(7309);tbl[t[i]] = r[j];
	    }
	}
    }

    SensorCall(7316);if(del && rlen == tlen) {
	SensorCall(7313);Perl_ck_warner(aTHX_ packWARN(WARN_MISC), "Useless use of /d modifier in transliteration operator"); 
    } else {/*675*/SensorCall(7314);if(rlen > tlen) {
	SensorCall(7315);Perl_ck_warner(aTHX_ packWARN(WARN_MISC), "Replacement list is longer than search list");
    ;/*676*/}}

    SensorCall(7317);if (grows)
	o->op_private |= OPpTRANS_GROWS;
#ifdef PERL_MAD
    op_getmad(expr,o,'e');
    op_getmad(repl,o,'r');
#else
    op_free(expr);
    op_free(repl);
#endif

    {OP * ReplaceReturn714 = o; SensorCall(7318); return ReplaceReturn714;}
}

/*
=for apidoc Am|OP *|newPMOP|I32 type|I32 flags

Constructs, checks, and returns an op of any pattern matching type.
I<type> is the opcode.  I<flags> gives the eight bits of C<op_flags>
and, shifted up eight bits, the eight bits of C<op_private>.

=cut
*/

OP *
Perl_newPMOP(pTHX_ I32 type, I32 flags)
{
SensorCall(7319);    dVAR;
    PMOP *pmop;

    assert((PL_opargs[type] & OA_CLASS_MASK) == OA_PMOP);

    NewOp(1101, pmop, 1, PMOP);
    pmop->op_type = (OPCODE)type;
    pmop->op_ppaddr = PL_ppaddr[type];
    pmop->op_flags = (U8)flags;
    pmop->op_private = (U8)(0 | (flags >> 8));

    SensorCall(7320);if (PL_hints & HINT_RE_TAINT)
	pmop->op_pmflags |= PMf_RETAINT;
    SensorCall(7324);if (IN_LOCALE_COMPILETIME) {
	SensorCall(7321);set_regex_charset(&(pmop->op_pmflags), REGEX_LOCALE_CHARSET);
    }
    else {/*289*/SensorCall(7322);if ((! (PL_hints & HINT_BYTES))
                /* Both UNI_8_BIT and locale :not_characters imply Unicode */
	     && (PL_hints & (HINT_UNI_8_BIT|HINT_LOCALE_NOT_CHARS)))
    {
	SensorCall(7323);set_regex_charset(&(pmop->op_pmflags), REGEX_UNICODE_CHARSET);
    ;/*290*/}}
    SensorCall(7330);if (PL_hints & HINT_RE_FLAGS) {
        SensorCall(7325);SV *reflags = Perl_refcounted_he_fetch_pvn(aTHX_
         PL_compiling.cop_hints_hash, STR_WITH_LEN("reflags"), 0, 0
        );
        SensorCall(7326);if (reflags && SvOK(reflags)) pmop->op_pmflags |= SvIV(reflags);
        SensorCall(7327);reflags = Perl_refcounted_he_fetch_pvn(aTHX_
         PL_compiling.cop_hints_hash, STR_WITH_LEN("reflags_charset"), 0, 0
        );
        SensorCall(7329);if (reflags && SvOK(reflags)) {
            SensorCall(7328);set_regex_charset(&(pmop->op_pmflags), (regex_charset)SvIV(reflags));
        }
    }


#ifdef USE_ITHREADS
    assert(SvPOK(PL_regex_pad[0]));
    SensorCall(7333);if (SvCUR(PL_regex_pad[0])) {
	/* Pop off the "packed" IV from the end.  */
	SensorCall(7331);SV *const repointer_list = PL_regex_pad[0];
	const char *p = SvEND(repointer_list) - sizeof(IV);
	const IV offset = *((IV*)p);

	assert(SvCUR(repointer_list) % sizeof(IV) == 0);

	SvEND_set(repointer_list, p);

	pmop->op_pmoffset = offset;
	/* This slot should be free, so assert this:  */
	assert(PL_regex_pad[offset] == &PL_sv_undef);
    } else {
	SensorCall(7332);SV * const repointer = &PL_sv_undef;
	av_push(PL_regex_padav, repointer);
	pmop->op_pmoffset = av_len(PL_regex_padav);
	PL_regex_pad = AvARRAY(PL_regex_padav);
    }
#endif

    {OP * ReplaceReturn713 = CHECKOP(type, pmop); SensorCall(7334); return ReplaceReturn713;}
}

/* Given some sort of match op o, and an expression expr containing a
 * pattern, either compile expr into a regex and attach it to o (if it's
 * constant), or convert expr into a runtime regcomp op sequence (if it's
 * not)
 *
 * isreg indicates that the pattern is part of a regex construct, eg
 * $x =~ /pattern/ or split /pattern/, as opposed to $x =~ $pattern or
 * split "pattern", which aren't. In the former case, expr will be a list
 * if the pattern contains more than one term (eg /a$b/) or if it contains
 * a replacement, ie s/// or tr///.
 */

OP *
Perl_pmruntime(pTHX_ OP *o, OP *expr, bool isreg)
{
SensorCall(7335);    dVAR;
    PMOP *pm;
    LOGOP *rcop;
    I32 repl_has_vars = 0;
    OP* repl = NULL;
    bool reglist;

    PERL_ARGS_ASSERT_PMRUNTIME;

    SensorCall(7340);if (
        o->op_type == OP_SUBST
     || o->op_type == OP_TRANS || o->op_type == OP_TRANSR
    ) {
	/* last element in list is the replacement; pop it */
	SensorCall(7336);OP* kid;
	repl = cLISTOPx(expr)->op_last;
	kid = cLISTOPx(expr)->op_first;
	SensorCall(7338);while (kid->op_sibling != repl)
	    {/*405*/SensorCall(7337);kid = kid->op_sibling;/*406*/}
	SensorCall(7339);kid->op_sibling = NULL;
	cLISTOPx(expr)->op_last = kid;
    }

    SensorCall(7342);if (isreg && expr->op_type == OP_LIST &&
	cLISTOPx(expr)->op_first->op_sibling == cLISTOPx(expr)->op_last)
    {
	/* convert single element list to element */
	SensorCall(7341);OP* const oe = expr;
	expr = cLISTOPx(oe)->op_first->op_sibling;
	cLISTOPx(oe)->op_first->op_sibling = NULL;
	cLISTOPx(oe)->op_last = NULL;
	op_free(oe);
    }

    SensorCall(7344);if (o->op_type == OP_TRANS || o->op_type == OP_TRANSR) {
	{OP * ReplaceReturn712 = pmtrans(o, expr, repl); SensorCall(7343); return ReplaceReturn712;}
    }

    SensorCall(7345);reglist = isreg && expr->op_type == OP_LIST;
    SensorCall(7346);if (reglist)
	op_null(expr);

    PL_hints |= HINT_BLOCK_SCOPE;
    SensorCall(7347);pm = (PMOP*)o;

    SensorCall(7360);if (expr->op_type == OP_CONST) {
	SensorCall(7348);SV *pat = ((SVOP*)expr)->op_sv;
	U32 pm_flags = pm->op_pmflags & RXf_PMf_COMPILETIME;

	SensorCall(7349);if (o->op_flags & OPf_SPECIAL)
	    pm_flags |= RXf_SPLIT;

	SensorCall(7352);if (DO_UTF8(pat)) {
	    assert (SvUTF8(pat));
	} else {/*407*/SensorCall(7350);if (SvUTF8(pat)) {
	    /* Not doing UTF-8, despite what the SV says. Is this only if we're
	       trapped in use 'bytes'?  */
	    /* Make a copy of the octet sequence, but without the flag on, as
	       the compiler now honours the SvUTF8 flag on pat.  */
	    SensorCall(7351);STRLEN len;
	    const char *const p = SvPV(pat, len);
	    pat = newSVpvn_flags(p, len, SVs_TEMP);
	;/*408*/}}

	PM_SETRE(pm, CALLREGCOMP(pat, pm_flags));

#ifdef PERL_MAD
	op_getmad(expr,(OP*)pm,'e');
#else
	op_free(expr);
#endif
    }
    else {
	SensorCall(7353);if (pm->op_pmflags & PMf_KEEP || !(PL_hints & HINT_RE_EVAL))
	    expr = newUNOP((!(PL_hints & HINT_RE_EVAL)
			    ? OP_REGCRESET
			    : OP_REGCMAYBE),0,expr);

	NewOp(1101, rcop, 1, LOGOP);
	SensorCall(7354);rcop->op_type = OP_REGCOMP;
	rcop->op_ppaddr = PL_ppaddr[OP_REGCOMP];
	rcop->op_first = scalar(expr);
	rcop->op_flags |= OPf_KIDS
			    | ((PL_hints & HINT_RE_EVAL) ? OPf_SPECIAL : 0)
			    | (reglist ? OPf_STACKED : 0);
	rcop->op_private = 1;
	rcop->op_other = o;
	SensorCall(7355);if (reglist)
	    rcop->op_targ = pad_alloc(rcop->op_type, SVs_PADTMP);

	/* /$x/ may cause an eval, since $x might be qr/(?{..})/  */
	SensorCall(7356);if (PL_hints & HINT_RE_EVAL) PL_cv_has_eval = 1;

	/* establish postfix order */
	SensorCall(7359);if (pm->op_pmflags & PMf_KEEP || !(PL_hints & HINT_RE_EVAL)) {
	    LINKLIST(expr);
	    SensorCall(7357);rcop->op_next = expr;
	    ((UNOP*)expr)->op_first->op_next = (OP*)rcop;
	}
	else {
	    SensorCall(7358);rcop->op_next = LINKLIST(expr);
	    expr->op_next = (OP*)rcop;
	}

	op_prepend_elem(o->op_type, scalar((OP*)rcop), o);
    }

    SensorCall(7389);if (repl) {
	SensorCall(7361);OP *curop;
	SensorCall(7383);if (pm->op_pmflags & PMf_EVAL) {
	    SensorCall(7362);curop = NULL;
	    SensorCall(7363);if (CopLINE(PL_curcop) < (line_t)PL_parser->multi_end)
		CopLINE_set(PL_curcop, (line_t)PL_parser->multi_end);
	}
	else {/*409*/SensorCall(7364);if (repl->op_type == OP_CONST)
	    {/*411*/SensorCall(7365);curop = repl;/*412*/}
	else {
	    SensorCall(7366);OP *lastop = NULL;
	    SensorCall(7382);for (curop = LINKLIST(repl); curop!=repl; curop = LINKLIST(curop)) {
		SensorCall(7367);if (curop->op_type == OP_SCOPE
			|| curop->op_type == OP_LEAVE
			|| (PL_opargs[curop->op_type] & OA_DANGEROUS)) {
		    SensorCall(7368);if (curop->op_type == OP_GV) {
			SensorCall(7369);GV * const gv = cGVOPx_gv(curop);
			repl_has_vars = 1;
			SensorCall(7371);if (strchr("&`'123456789+-\016\022", *GvENAME(gv)))
			    {/*413*/SensorCall(7370);break;/*414*/}
		    }
		    else {/*415*/SensorCall(7372);if (curop->op_type == OP_RV2CV)
			{/*417*/SensorCall(7373);break;/*418*/}
		    else {/*419*/SensorCall(7374);if (curop->op_type == OP_RV2SV ||
			     curop->op_type == OP_RV2AV ||
			     curop->op_type == OP_RV2HV ||
			     curop->op_type == OP_RV2GV) {
			SensorCall(7375);if (lastop && lastop->op_type != OP_GV)	/*funny deref?*/
			    {/*421*/SensorCall(7376);break;/*422*/}
		    }
		    else {/*423*/SensorCall(7377);if (curop->op_type == OP_PADSV ||
			     curop->op_type == OP_PADAV ||
			     curop->op_type == OP_PADHV ||
			     curop->op_type == OP_PADANY)
		    {
			SensorCall(7378);repl_has_vars = 1;
		    }
		    else {/*425*/SensorCall(7379);if (curop->op_type == OP_PUSHRE)
			NOOP; /* Okay here, dangerous in newASSIGNOP */
		    else
			{/*427*/SensorCall(7380);break;/*428*/}/*416*/;/*426*/}/*424*/}/*420*/}}
		}
		SensorCall(7381);lastop = curop;
	    }
	;/*410*/}}
	SensorCall(7388);if (curop == repl
	    && !(repl_has_vars
		 && (!PM_GETRE(pm)
		     || RX_EXTFLAGS(PM_GETRE(pm)) & RXf_EVAL_SEEN)))
	{
	    SensorCall(7384);pm->op_pmflags |= PMf_CONST;	/* const for long enough */
	    op_prepend_elem(o->op_type, scalar(repl), o);
	}
	else {
	    SensorCall(7385);if (curop == repl && !PM_GETRE(pm)) { /* Has variables. */
		SensorCall(7386);pm->op_pmflags |= PMf_MAYBE_CONST;
	    }
	    NewOp(1101, rcop, 1, LOGOP);
	    SensorCall(7387);rcop->op_type = OP_SUBSTCONT;
	    rcop->op_ppaddr = PL_ppaddr[OP_SUBSTCONT];
	    rcop->op_first = scalar(repl);
	    rcop->op_flags |= OPf_KIDS;
	    rcop->op_private = 1;
	    rcop->op_other = o;

	    /* establish postfix order */
	    rcop->op_next = LINKLIST(repl);
	    repl->op_next = (OP*)rcop;

	    pm->op_pmreplrootu.op_pmreplroot = scalar((OP*)rcop);
	    assert(!(pm->op_pmflags & PMf_ONCE));
	    pm->op_pmstashstartu.op_pmreplstart = LINKLIST(rcop);
	    rcop->op_next = 0;
	}
    }

    {OP * ReplaceReturn711 = (OP*)pm; SensorCall(7390); return ReplaceReturn711;}
}

/*
=for apidoc Am|OP *|newSVOP|I32 type|I32 flags|SV *sv

Constructs, checks, and returns an op of any type that involves an
embedded SV.  I<type> is the opcode.  I<flags> gives the eight bits
of C<op_flags>.  I<sv> gives the SV to embed in the op; this function
takes ownership of one reference to it.

=cut
*/

OP *
Perl_newSVOP(pTHX_ I32 type, I32 flags, SV *sv)
{
SensorCall(7391);    dVAR;
    SVOP *svop;

    PERL_ARGS_ASSERT_NEWSVOP;

    assert((PL_opargs[type] & OA_CLASS_MASK) == OA_SVOP
	|| (PL_opargs[type] & OA_CLASS_MASK) == OA_PVOP_OR_SVOP
	|| (PL_opargs[type] & OA_CLASS_MASK) == OA_FILESTATOP);

    NewOp(1101, svop, 1, SVOP);
    svop->op_type = (OPCODE)type;
    svop->op_ppaddr = PL_ppaddr[type];
    svop->op_sv = sv;
    svop->op_next = (OP*)svop;
    svop->op_flags = (U8)flags;
    SensorCall(7392);if (PL_opargs[type] & OA_RETSCALAR)
	scalar((OP*)svop);
    SensorCall(7393);if (PL_opargs[type] & OA_TARGET)
	svop->op_targ = pad_alloc(type, SVs_PADTMP);
    {OP * ReplaceReturn710 = CHECKOP(type, svop); SensorCall(7394); return ReplaceReturn710;}
}

#ifdef USE_ITHREADS

/*
=for apidoc Am|OP *|newPADOP|I32 type|I32 flags|SV *sv

Constructs, checks, and returns an op of any type that involves a
reference to a pad element.  I<type> is the opcode.  I<flags> gives the
eight bits of C<op_flags>.  A pad slot is automatically allocated, and
is populated with I<sv>; this function takes ownership of one reference
to it.

This function only exists if Perl has been compiled to use ithreads.

=cut
*/

OP *
Perl_newPADOP(pTHX_ I32 type, I32 flags, SV *sv)
{
SensorCall(7395);    dVAR;
    PADOP *padop;

    PERL_ARGS_ASSERT_NEWPADOP;

    assert((PL_opargs[type] & OA_CLASS_MASK) == OA_SVOP
	|| (PL_opargs[type] & OA_CLASS_MASK) == OA_PVOP_OR_SVOP
	|| (PL_opargs[type] & OA_CLASS_MASK) == OA_FILESTATOP);

    NewOp(1101, padop, 1, PADOP);
    padop->op_type = (OPCODE)type;
    padop->op_ppaddr = PL_ppaddr[type];
    padop->op_padix = pad_alloc(type, SVs_PADTMP);
    SvREFCNT_dec(PAD_SVl(padop->op_padix));
    PAD_SETSV(padop->op_padix, sv);
    assert(sv);
    SvPADTMP_on(sv);
    padop->op_next = (OP*)padop;
    padop->op_flags = (U8)flags;
    SensorCall(7396);if (PL_opargs[type] & OA_RETSCALAR)
	scalar((OP*)padop);
    SensorCall(7397);if (PL_opargs[type] & OA_TARGET)
	padop->op_targ = pad_alloc(type, SVs_PADTMP);
    {OP * ReplaceReturn709 = CHECKOP(type, padop); SensorCall(7398); return ReplaceReturn709;}
}

#endif /* !USE_ITHREADS */

/*
=for apidoc Am|OP *|newGVOP|I32 type|I32 flags|GV *gv

Constructs, checks, and returns an op of any type that involves an
embedded reference to a GV.  I<type> is the opcode.  I<flags> gives the
eight bits of C<op_flags>.  I<gv> identifies the GV that the op should
reference; calling this function does not transfer ownership of any
reference to it.

=cut
*/

OP *
Perl_newGVOP(pTHX_ I32 type, I32 flags, GV *gv)
{
SensorCall(7399);    dVAR;

    PERL_ARGS_ASSERT_NEWGVOP;

#ifdef USE_ITHREADS
    GvIN_PAD_on(gv);
    {OP * ReplaceReturn708 = newPADOP(type, flags, SvREFCNT_inc_simple_NN(gv)); SensorCall(7400); return ReplaceReturn708;}
#else
    return newSVOP(type, flags, SvREFCNT_inc_simple_NN(gv));
#endif
}

/*
=for apidoc Am|OP *|newPVOP|I32 type|I32 flags|char *pv

Constructs, checks, and returns an op of any type that involves an
embedded C-level pointer (PV).  I<type> is the opcode.  I<flags> gives
the eight bits of C<op_flags>.  I<pv> supplies the C-level pointer, which
must have been allocated using L</PerlMemShared_malloc>; the memory will
be freed when the op is destroyed.

=cut
*/

OP *
Perl_newPVOP(pTHX_ I32 type, I32 flags, char *pv)
{
SensorCall(7401);    dVAR;
    const bool utf8 = cBOOL(flags & SVf_UTF8);
    PVOP *pvop;

    flags &= ~SVf_UTF8;

    assert((PL_opargs[type] & OA_CLASS_MASK) == OA_PVOP_OR_SVOP
	|| type == OP_RUNCV
	|| (PL_opargs[type] & OA_CLASS_MASK) == OA_LOOPEXOP);

    NewOp(1101, pvop, 1, PVOP);
    pvop->op_type = (OPCODE)type;
    pvop->op_ppaddr = PL_ppaddr[type];
    pvop->op_pv = pv;
    pvop->op_next = (OP*)pvop;
    pvop->op_flags = (U8)flags;
    pvop->op_private = utf8 ? OPpPV_IS_UTF8 : 0;
    SensorCall(7402);if (PL_opargs[type] & OA_RETSCALAR)
	scalar((OP*)pvop);
    SensorCall(7403);if (PL_opargs[type] & OA_TARGET)
	pvop->op_targ = pad_alloc(type, SVs_PADTMP);
    {OP * ReplaceReturn707 = CHECKOP(type, pvop); SensorCall(7404); return ReplaceReturn707;}
}

#ifdef PERL_MAD
OP*
#else
void
#endif
Perl_package(pTHX_ OP *o)
{
SensorCall(7405);    dVAR;
    SV *const sv = cSVOPo->op_sv;
#ifdef PERL_MAD
    OP *pegop;
#endif

    PERL_ARGS_ASSERT_PACKAGE;

    SAVEGENERICSV(PL_curstash);
    save_item(PL_curstname);

    PL_curstash = (HV *)SvREFCNT_inc(gv_stashsv(sv, GV_ADD));

    sv_setsv(PL_curstname, sv);

    PL_hints |= HINT_BLOCK_SCOPE;
    PL_parser->copline = NOLINE;
    PL_parser->expect = XSTATE;

#ifndef PERL_MAD
    op_free(o);
#else
    if (!PL_madskills) {
	op_free(o);
	return NULL;
    }

    pegop = newOP(OP_NULL,0);
    op_getmad(o,pegop,'P');
    return pegop;
#endif
}

void
Perl_package_version( pTHX_ OP *v )
{
SensorCall(7406);    dVAR;
    U32 savehints = PL_hints;
    PERL_ARGS_ASSERT_PACKAGE_VERSION;
    PL_hints &= ~HINT_STRICT_VARS;
    sv_setsv( GvSV(gv_fetchpvs("VERSION", GV_ADDMULTI, SVt_PV)), cSVOPx(v)->op_sv );
    PL_hints = savehints;
    op_free(v);
}

#ifdef PERL_MAD
OP*
#else
void
#endif
Perl_utilize(pTHX_ int aver, I32 floor, OP *version, OP *idop, OP *arg)
{
SensorCall(7407);    dVAR;
    OP *pack;
    OP *imop;
    OP *veop;
#ifdef PERL_MAD
    OP *pegop = newOP(OP_NULL,0);
#endif
    SV *use_version = NULL;

    PERL_ARGS_ASSERT_UTILIZE;

    SensorCall(7409);if (idop->op_type != OP_CONST)
	{/*517*/SensorCall(7408);Perl_croak(aTHX_ "Module name must be constant");/*518*/}

    SensorCall(7410);if (PL_madskills)
	op_getmad(idop,pegop,'U');

    SensorCall(7411);veop = NULL;

    SensorCall(7420);if (version) {
	SensorCall(7412);SV * const vesv = ((SVOP*)version)->op_sv;

	SensorCall(7413);if (PL_madskills)
	    op_getmad(version,pegop,'V');
	SensorCall(7419);if (!arg && !SvNIOKp(vesv)) {
	    SensorCall(7414);arg = version;
	}
	else {
	    SensorCall(7415);OP *pack;
	    SV *meth;

	    SensorCall(7417);if (version->op_type != OP_CONST || !SvNIOKp(vesv))
		{/*519*/SensorCall(7416);Perl_croak(aTHX_ "Version number must be a constant number");/*520*/}

	    /* Make copy of idop so we don't free it twice */
	    SensorCall(7418);pack = newSVOP(OP_CONST, 0, newSVsv(((SVOP*)idop)->op_sv));

	    /* Fake up a method call to VERSION */
	    meth = newSVpvs_share("VERSION");
	    veop = convert(OP_ENTERSUB, OPf_STACKED|OPf_SPECIAL,
			    op_append_elem(OP_LIST,
					op_prepend_elem(OP_LIST, pack, list(version)),
					newSVOP(OP_METHOD_NAMED, 0, meth)));
	}
    }

    /* Fake up an import/unimport */
    SensorCall(7430);if (arg && arg->op_type == OP_STUB) {
	SensorCall(7421);if (PL_madskills)
	    op_getmad(arg,pegop,'S');
	SensorCall(7422);imop = arg;		/* no import on explicit () */
    }
    else {/*521*/SensorCall(7423);if (SvNIOKp(((SVOP*)idop)->op_sv)) {
	SensorCall(7424);imop = NULL;		/* use 5.0; */
	SensorCall(7426);if (aver)
	    {/*523*/SensorCall(7425);use_version = ((SVOP*)idop)->op_sv;/*524*/}
	else
	    idop->op_private |= OPpCONST_NOVER;
    }
    else {
	SensorCall(7427);SV *meth;

	SensorCall(7428);if (PL_madskills)
	    op_getmad(arg,pegop,'A');

	/* Make copy of idop so we don't free it twice */
	SensorCall(7429);pack = newSVOP(OP_CONST, 0, newSVsv(((SVOP*)idop)->op_sv));

	/* Fake up a method call to import/unimport */
	meth = aver
	    ? newSVpvs_share("import") : newSVpvs_share("unimport");
	imop = convert(OP_ENTERSUB, OPf_STACKED|OPf_SPECIAL,
		       op_append_elem(OP_LIST,
				   op_prepend_elem(OP_LIST, pack, list(arg)),
				   newSVOP(OP_METHOD_NAMED, 0, meth)));
    ;/*522*/}}

    /* Fake up the BEGIN {}, which does its thing immediately. */
    newATTRSUB(floor,
	newSVOP(OP_CONST, 0, newSVpvs_share("BEGIN")),
	NULL,
	NULL,
	op_append_elem(OP_LINESEQ,
	    op_append_elem(OP_LINESEQ,
	        newSTATEOP(0, NULL, newUNOP(OP_REQUIRE, 0, idop)),
	        newSTATEOP(0, NULL, veop)),
	    newSTATEOP(0, NULL, imop) ));

    SensorCall(7439);if (use_version) {
	/* Enable the
	 * feature bundle that corresponds to the required version. */
	SensorCall(7431);use_version = sv_2mortal(new_version(use_version));
	S_enable_feature_bundle(aTHX_ use_version);

	/* If a version >= 5.11.0 is requested, strictures are on by default! */
	SensorCall(7438);if (vcmp(use_version,
		 sv_2mortal(upg_version(newSVnv(5.011000), FALSE))) >= 0) {
	    SensorCall(7432);if (!(PL_hints & HINT_EXPLICIT_STRICT_REFS))
		PL_hints |= HINT_STRICT_REFS;
	    SensorCall(7433);if (!(PL_hints & HINT_EXPLICIT_STRICT_SUBS))
		PL_hints |= HINT_STRICT_SUBS;
	    SensorCall(7434);if (!(PL_hints & HINT_EXPLICIT_STRICT_VARS))
		PL_hints |= HINT_STRICT_VARS;
	}
	/* otherwise they are off */
	else {
	    SensorCall(7435);if (!(PL_hints & HINT_EXPLICIT_STRICT_REFS))
		PL_hints &= ~HINT_STRICT_REFS;
	    SensorCall(7436);if (!(PL_hints & HINT_EXPLICIT_STRICT_SUBS))
		PL_hints &= ~HINT_STRICT_SUBS;
	    SensorCall(7437);if (!(PL_hints & HINT_EXPLICIT_STRICT_VARS))
		PL_hints &= ~HINT_STRICT_VARS;
	}
    }

    /* The "did you use incorrect case?" warning used to be here.
     * The problem is that on case-insensitive filesystems one
     * might get false positives for "use" (and "require"):
     * "use Strict" or "require CARP" will work.  This causes
     * portability problems for the script: in case-strict
     * filesystems the script will stop working.
     *
     * The "incorrect case" warning checked whether "use Foo"
     * imported "Foo" to your namespace, but that is wrong, too:
     * there is no requirement nor promise in the language that
     * a Foo.pm should or would contain anything in package "Foo".
     *
     * There is very little Configure-wise that can be done, either:
     * the case-sensitivity of the build filesystem of Perl does not
     * help in guessing the case-sensitivity of the runtime environment.
     */

    PL_hints |= HINT_BLOCK_SCOPE;
    PL_parser->copline = NOLINE;
    PL_parser->expect = XSTATE;
    PL_cop_seqmax++; /* Purely for B::*'s benefit */
    SensorCall(7440);if (PL_cop_seqmax == PERL_PADSEQ_INTRO) /* not a legal value */
	PL_cop_seqmax++;

#ifdef PERL_MAD
    if (!PL_madskills) {
	/* FIXME - don't allocate pegop if !PL_madskills */
	op_free(pegop);
	return NULL;
    }
    return pegop;
#endif
SensorCall(7441);}

/*
=head1 Embedding Functions

=for apidoc load_module

Loads the module whose name is pointed to by the string part of name.
Note that the actual module name, not its filename, should be given.
Eg, "Foo::Bar" instead of "Foo/Bar.pm".  flags can be any of
PERL_LOADMOD_DENY, PERL_LOADMOD_NOIMPORT, or PERL_LOADMOD_IMPORT_OPS
(or 0 for no flags). ver, if specified and not NULL, provides version semantics
similar to C<use Foo::Bar VERSION>.  The optional trailing SV*
arguments can be used to specify arguments to the module's import()
method, similar to C<use Foo::Bar VERSION LIST>.  They must be
terminated with a final NULL pointer.  Note that this list can only
be omitted when the PERL_LOADMOD_NOIMPORT flag has been used.
Otherwise at least a single NULL pointer to designate the default
import list is required.

The reference count for each specified C<SV*> parameter is decremented.

=cut */

void
Perl_load_module(pTHX_ U32 flags, SV *name, SV *ver, ...)
{
    SensorCall(7442);va_list args;

    PERL_ARGS_ASSERT_LOAD_MODULE;

    va_start(args, ver);
    vload_module(flags, name, ver, &args);
    va_end(args);
}

#ifdef PERL_IMPLICIT_CONTEXT
void
Perl_load_module_nocontext(U32 flags, SV *name, SV *ver, ...)
{
    dTHX;
    va_list args;
    PERL_ARGS_ASSERT_LOAD_MODULE_NOCONTEXT;
    va_start(args, ver);
    vload_module(flags, name, ver, &args);
    va_end(args);
}
#endif

void
Perl_vload_module(pTHX_ U32 flags, SV *name, SV *ver, va_list *args)
{
SensorCall(7443);    dVAR;
    OP *veop, *imop;
    OP * const modname = newSVOP(OP_CONST, 0, name);

    PERL_ARGS_ASSERT_VLOAD_MODULE;

    modname->op_private |= OPpCONST_BARE;
    SensorCall(7445);if (ver) {
	SensorCall(7444);veop = newSVOP(OP_CONST, 0, ver);
    }
    else
	veop = NULL;
    SensorCall(7452);if (flags & PERL_LOADMOD_NOIMPORT) {
	SensorCall(7446);imop = sawparens(newNULLLIST());
    }
    else {/*513*/SensorCall(7447);if (flags & PERL_LOADMOD_IMPORT_OPS) {
	SensorCall(7448);imop = va_arg(*args, OP*);
    }
    else {
	SensorCall(7449);SV *sv;
	imop = NULL;
	sv = va_arg(*args, SV*);
	SensorCall(7451);while (sv) {
	    SensorCall(7450);imop = op_append_elem(OP_LIST, imop, newSVOP(OP_CONST, 0, sv));
	    sv = va_arg(*args, SV*);
	}
    ;/*514*/}}

    /* utilize() fakes up a BEGIN { require ..; import ... }, so make sure
     * that it has a PL_parser to play with while doing that, and also
     * that it doesn't mess with any existing parser, by creating a tmp
     * new parser with lex_start(). This won't actually be used for much,
     * since pp_require() will create another parser for the real work. */

    ENTER;
    SAVEVPTR(PL_curcop);
    lex_start(NULL, NULL, LEX_START_SAME_FILTER);
    utilize(!(flags & PERL_LOADMOD_DENY), start_subparse(FALSE, 0),
	    veop, modname, imop);
    LEAVE;
}

OP *
Perl_dofile(pTHX_ OP *term, I32 force_builtin)
{
SensorCall(7453);    dVAR;
    OP *doop;
    GV *gv = NULL;

    PERL_ARGS_ASSERT_DOFILE;

    SensorCall(7457);if (!force_builtin) {
	SensorCall(7454);gv = gv_fetchpvs("do", GV_NOTQUAL, SVt_PVCV);
	SensorCall(7456);if (!(gv && GvCVu(gv) && GvIMPORTED_CV(gv))) {
	    SensorCall(7455);GV * const * const gvp = (GV**)hv_fetchs(PL_globalstash, "do", FALSE);
	    gv = gvp ? *gvp : NULL;
	}
    }

    SensorCall(7460);if (gv && GvCVu(gv) && GvIMPORTED_CV(gv)) {
	SensorCall(7458);doop = ck_subr(newUNOP(OP_ENTERSUB, OPf_STACKED,
			       op_append_elem(OP_LIST, term,
					   scalar(newUNOP(OP_RV2CV, 0,
							  newGVOP(OP_GV, 0, gv))))));
    }
    else {
	SensorCall(7459);doop = newUNOP(OP_DOFILE, 0, scalar(term));
    }
    {OP * ReplaceReturn706 = doop; SensorCall(7461); return ReplaceReturn706;}
}

/*
=head1 Optree construction

=for apidoc Am|OP *|newSLICEOP|I32 flags|OP *subscript|OP *listval

Constructs, checks, and returns an C<lslice> (list slice) op.  I<flags>
gives the eight bits of C<op_flags>, except that C<OPf_KIDS> will
be set automatically, and, shifted up eight bits, the eight bits of
C<op_private>, except that the bit with value 1 or 2 is automatically
set as required.  I<listval> and I<subscript> supply the parameters of
the slice; they are consumed by this function and become part of the
constructed op tree.

=cut
*/

OP *
Perl_newSLICEOP(pTHX_ I32 flags, OP *subscript, OP *listval)
{
    {OP * ReplaceReturn705 = newBINOP(OP_LSLICE, flags,
	    list(force_list(subscript)),
	    list(force_list(listval)) ); SensorCall(7462); return ReplaceReturn705;}
}

STATIC I32
S_is_list_assignment(pTHX_ register const OP *o)
{
    SensorCall(7463);unsigned type;
    U8 flags;

    SensorCall(7464);if (!o)
	return TRUE;

    SensorCall(7466);if ((o->op_type == OP_NULL) && (o->op_flags & OPf_KIDS))
	{/*583*/SensorCall(7465);o = cUNOPo->op_first;/*584*/}

    SensorCall(7467);flags = o->op_flags;
    type = o->op_type;
    SensorCall(7472);if (type == OP_COND_EXPR) {
        SensorCall(7468);const I32 t = is_list_assignment(cLOGOPo->op_first->op_sibling);
        const I32 f = is_list_assignment(cLOGOPo->op_first->op_sibling->op_sibling);

	SensorCall(7469);if (t && f)
	    return TRUE;
	SensorCall(7470);if (t || f)
	    yyerror("Assignment to both a list and a scalar");
	{I32  ReplaceReturn704 = FALSE; SensorCall(7471); return ReplaceReturn704;}
    }

    SensorCall(7473);if (type == OP_LIST &&
	(flags & OPf_WANT) == OPf_WANT_SCALAR &&
	o->op_private & OPpLVAL_INTRO)
	return FALSE;

    SensorCall(7474);if (type == OP_LIST || flags & OPf_PARENS ||
	type == OP_RV2AV || type == OP_RV2HV ||
	type == OP_ASLICE || type == OP_HSLICE)
	return TRUE;

    SensorCall(7475);if (type == OP_PADAV || type == OP_PADHV)
	return TRUE;

    SensorCall(7476);if (type == OP_RV2SV)
	return FALSE;

    {I32  ReplaceReturn703 = FALSE; SensorCall(7477); return ReplaceReturn703;}
}

/*
  Helper function for newASSIGNOP to detection commonality between the
  lhs and the rhs.  Marks all variables with PL_generation.  If it
  returns TRUE the assignment must be able to handle common variables.
*/
PERL_STATIC_INLINE bool
S_aassign_common_vars(pTHX_ OP* o)
{
    SensorCall(7478);OP *curop;
    SensorCall(7485);for (curop = cUNOPo->op_first; curop; curop=curop->op_sibling) {
	SensorCall(7479);if (PL_opargs[curop->op_type] & OA_DANGEROUS) {
	    SensorCall(7480);if (curop->op_type == OP_GV) {
		SensorCall(7481);GV *gv = cGVOPx_gv(curop);
		SensorCall(7482);if (gv == PL_defgv
		    || (int)GvASSIGN_GENERATION(gv) == PL_generation)
		    return TRUE;
		GvASSIGN_GENERATION_set(gv, PL_generation);
	    }
	    else if (curop->op_type == OP_PADSV ||
		curop->op_type == OP_PADAV ||
		curop->op_type == OP_PADHV ||
		curop->op_type == OP_PADANY)
		{
		    if (PAD_COMPNAME_GEN(curop->op_targ)
			== (STRLEN)PL_generation)
			return TRUE;
		    PAD_COMPNAME_GEN_set(curop->op_targ, PL_generation);

		}
	    else if (curop->op_type == OP_RV2CV)
		return TRUE;
	    else if (curop->op_type == OP_RV2SV ||
		curop->op_type == OP_RV2AV ||
		curop->op_type == OP_RV2HV ||
		curop->op_type == OP_RV2GV) {
		if (cUNOPx(curop)->op_first->op_type != OP_GV)	/* funny deref? */
		    return TRUE;
	    }
	    else if (curop->op_type == OP_PUSHRE) {
#ifdef USE_ITHREADS
		if (((PMOP*)curop)->op_pmreplrootu.op_pmtargetoff) {
		    GV *const gv = MUTABLE_GV(PAD_SVl(((PMOP*)curop)->op_pmreplrootu.op_pmtargetoff));
		    if (gv == PL_defgv
			|| (int)GvASSIGN_GENERATION(gv) == PL_generation)
			return TRUE;
		    GvASSIGN_GENERATION_set(gv, PL_generation);
		}
#else
		GV *const gv
		    = ((PMOP*)curop)->op_pmreplrootu.op_pmtargetgv;
		if (gv) {
		    if (gv == PL_defgv
			|| (int)GvASSIGN_GENERATION(gv) == PL_generation)
			return TRUE;
		    GvASSIGN_GENERATION_set(gv, PL_generation);
		}
#endif
	    }
	    else
		return TRUE;
	}

	SensorCall(7484);if (curop->op_flags & OPf_KIDS) {
	    SensorCall(7483);if (aassign_common_vars(curop))
		return TRUE;
	}
    }
    {_Bool  ReplaceReturn702 = FALSE; SensorCall(7486); return ReplaceReturn702;}
}

/*
=for apidoc Am|OP *|newASSIGNOP|I32 flags|OP *left|I32 optype|OP *right

Constructs, checks, and returns an assignment op.  I<left> and I<right>
supply the parameters of the assignment; they are consumed by this
function and become part of the constructed op tree.

If I<optype> is C<OP_ANDASSIGN>, C<OP_ORASSIGN>, or C<OP_DORASSIGN>, then
a suitable conditional optree is constructed.  If I<optype> is the opcode
of a binary operator, such as C<OP_BIT_OR>, then an op is constructed that
performs the binary operation and assigns the result to the left argument.
Either way, if I<optype> is non-zero then I<flags> has no effect.

If I<optype> is zero, then a plain scalar or list assignment is
constructed.  Which type of assignment it is is automatically determined.
I<flags> gives the eight bits of C<op_flags>, except that C<OPf_KIDS>
will be set automatically, and, shifted up eight bits, the eight bits
of C<op_private>, except that the bit with value 1 or 2 is automatically
set as required.

=cut
*/

OP *
Perl_newASSIGNOP(pTHX_ I32 flags, OP *left, I32 optype, OP *right)
{
SensorCall(7487);    dVAR;
    OP *o;

    SensorCall(7491);if (optype) {
	SensorCall(7488);if (optype == OP_ANDASSIGN || optype == OP_ORASSIGN || optype == OP_DORASSIGN) {
	    {OP * ReplaceReturn701 = newLOGOP(optype, 0,
		op_lvalue(scalar(left), optype),
		newUNOP(OP_SASSIGN, 0, scalar(right))); SensorCall(7489); return ReplaceReturn701;}
	}
	else {
	    {OP * ReplaceReturn700 = newBINOP(optype, OPf_STACKED,
		op_lvalue(scalar(left), optype), scalar(right)); SensorCall(7490); return ReplaceReturn700;}
	}
    }

    SensorCall(7521);if (is_list_assignment(left)) {
	SensorCall(7492);static const char no_list_state[] = "Initialization of state variables"
	    " in list context currently forbidden";
	OP *curop;
	bool maybe_common_vars = TRUE;

	PL_modcount = 0;
	left = op_lvalue(left, OP_AASSIGN);
	curop = list(force_list(left));
	o = newBINOP(OP_AASSIGN, flags, list(force_list(right)), curop);
	o->op_private = (U8)(0 | (flags >> 8));

	SensorCall(7505);if ((left->op_type == OP_LIST
	     || (left->op_type == OP_NULL && left->op_targ == OP_LIST)))
	{
	    SensorCall(7493);OP* lop = ((LISTOP*)left)->op_first;
	    maybe_common_vars = FALSE;
	    SensorCall(7501);while (lop) {
		SensorCall(7494);if (lop->op_type == OP_PADSV ||
		    lop->op_type == OP_PADAV ||
		    lop->op_type == OP_PADHV ||
		    lop->op_type == OP_PADANY) {
		    SensorCall(7495);if (!(lop->op_private & OPpLVAL_INTRO))
			maybe_common_vars = TRUE;

		    SensorCall(7497);if (lop->op_private & OPpPAD_STATE) {
			SensorCall(7496);if (left->op_private & OPpLVAL_INTRO) {
			    /* Each variable in state($a, $b, $c) = ... */
			}
			else {
			    /* Each state variable in
			       (state $a, my $b, our $c, $d, undef) = ... */
			}
			yyerror(no_list_state);
		    } else {
			/* Each my variable in
			   (state $a, my $b, our $c, $d, undef) = ... */
		    }
		} else {/*235*/SensorCall(7498);if (lop->op_type == OP_UNDEF ||
			   lop->op_type == OP_PUSHMARK) {
		    /* undef may be interesting in
		       (state $a, undef, state $c) */
		} else {
		    /* Other ops in the list. */
		    SensorCall(7499);maybe_common_vars = TRUE;
		;/*236*/}}
		SensorCall(7500);lop = lop->op_sibling;
	    }
	}
	else {/*237*/SensorCall(7502);if ((left->op_private & OPpLVAL_INTRO)
		&& (   left->op_type == OP_PADSV
		    || left->op_type == OP_PADAV
		    || left->op_type == OP_PADHV
		    || left->op_type == OP_PADANY))
	{
	    SensorCall(7503);if (left->op_type == OP_PADSV) maybe_common_vars = FALSE;
	    SensorCall(7504);if (left->op_private & OPpPAD_STATE) {
		/* All single variable list context state assignments, hence
		   state ($a) = ...
		   (state $a) = ...
		   state @a = ...
		   state (@a) = ...
		   (state @a) = ...
		   state %a = ...
		   state (%a) = ...
		   (state %a) = ...
		*/
		yyerror(no_list_state);
	    }
	;/*238*/}}

	/* PL_generation sorcery:
	 * an assignment like ($a,$b) = ($c,$d) is easier than
	 * ($a,$b) = ($c,$a), since there is no need for temporary vars.
	 * To detect whether there are common vars, the global var
	 * PL_generation is incremented for each assign op we compile.
	 * Then, while compiling the assign op, we run through all the
	 * variables on both sides of the assignment, setting a spare slot
	 * in each of them to PL_generation. If any of them already have
	 * that value, we know we've got commonality.  We could use a
	 * single bit marker, but then we'd have to make 2 passes, first
	 * to clear the flag, then to test and set it.  To find somewhere
	 * to store these values, evil chicanery is done with SvUVX().
	 */

	SensorCall(7507);if (maybe_common_vars) {
	    PL_generation++;
	    SensorCall(7506);if (aassign_common_vars(o))
		o->op_private |= OPpASSIGN_COMMON;
	    LINKLIST(o);
	}

	SensorCall(7519);if (right && right->op_type == OP_SPLIT && !PL_madskills) {
	    SensorCall(7508);OP* tmpop = ((LISTOP*)right)->op_first;
	    SensorCall(7518);if (tmpop && (tmpop->op_type == OP_PUSHRE)) {
		SensorCall(7509);PMOP * const pm = (PMOP*)tmpop;
		SensorCall(7517);if (left->op_type == OP_RV2AV &&
		    !(left->op_private & OPpLVAL_INTRO) &&
		    !(o->op_private & OPpASSIGN_COMMON) )
		{
		    SensorCall(7510);tmpop = ((UNOP*)left)->op_first;
		    SensorCall(7513);if (tmpop->op_type == OP_GV
#ifdef USE_ITHREADS
			&& !pm->op_pmreplrootu.op_pmtargetoff
#else
			&& !pm->op_pmreplrootu.op_pmtargetgv
#endif
			) {
#ifdef USE_ITHREADS
			SensorCall(7511);pm->op_pmreplrootu.op_pmtargetoff
			    = cPADOPx(tmpop)->op_padix;
			cPADOPx(tmpop)->op_padix = 0;	/* steal it */
#else
			pm->op_pmreplrootu.op_pmtargetgv
			    = MUTABLE_GV(cSVOPx(tmpop)->op_sv);
			cSVOPx(tmpop)->op_sv = NULL;	/* steal it */
#endif
			pm->op_pmflags |= PMf_ONCE;
			tmpop = cUNOPo->op_first;	/* to list (nulled) */
			tmpop = ((UNOP*)tmpop)->op_first; /* to pushmark */
			tmpop->op_sibling = NULL;	/* don't free split */
			right->op_next = tmpop->op_next;  /* fix starting loc */
			op_free(o);			/* blow off assign */
			right->op_flags &= ~OPf_WANT;
				/* "I don't know and I don't care." */
			{OP * ReplaceReturn699 = right; SensorCall(7512); return ReplaceReturn699;}
		    }
		}
		else {
                   SensorCall(7514);if (PL_modcount < RETURN_UNLIMITED_NUMBER &&
		      ((LISTOP*)right)->op_last->op_type == OP_CONST)
		    {
			SensorCall(7515);SV *sv = ((SVOP*)((LISTOP*)right)->op_last)->op_sv;
			SensorCall(7516);if (SvIOK(sv) && SvIVX(sv) == 0)
			    sv_setiv(sv, PL_modcount+1);
		    }
		}
	    }
	}
	{OP * ReplaceReturn698 = o; SensorCall(7520); return ReplaceReturn698;}
    }
    SensorCall(7522);if (!right)
	right = newOP(OP_UNDEF, 0);
    SensorCall(7526);if (right->op_type == OP_READLINE) {
	SensorCall(7523);right->op_flags |= OPf_STACKED;
	{OP * ReplaceReturn697 = newBINOP(OP_NULL, flags, op_lvalue(scalar(left), OP_SASSIGN),
		scalar(right)); SensorCall(7524); return ReplaceReturn697;}
    }
    else {
	SensorCall(7525);o = newBINOP(OP_SASSIGN, flags,
	    scalar(right), op_lvalue(scalar(left), OP_SASSIGN) );
    }
    {OP * ReplaceReturn696 = o; SensorCall(7527); return ReplaceReturn696;}
}

/*
=for apidoc Am|OP *|newSTATEOP|I32 flags|char *label|OP *o

Constructs a state op (COP).  The state op is normally a C<nextstate> op,
but will be a C<dbstate> op if debugging is enabled for currently-compiled
code.  The state op is populated from L</PL_curcop> (or L</PL_compiling>).
If I<label> is non-null, it supplies the name of a label to attach to
the state op; this function takes ownership of the memory pointed at by
I<label>, and will free it.  I<flags> gives the eight bits of C<op_flags>
for the state op.

If I<o> is null, the state op is returned.  Otherwise the state op is
combined with I<o> into a C<lineseq> list op, which is returned.  I<o>
is consumed by this function and becomes part of the returned op tree.

=cut
*/

OP *
Perl_newSTATEOP(pTHX_ I32 flags, char *label, OP *o)
{
SensorCall(7528);    dVAR;
    const U32 seq = intro_my();
    const U32 utf8 = flags & SVf_UTF8;
    register COP *cop;

    flags &= ~SVf_UTF8;

    NewOp(1101, cop, 1, COP);
    SensorCall(7531);if (PERLDB_LINE && CopLINE(PL_curcop) && PL_curstash != PL_debstash) {
	SensorCall(7529);cop->op_type = OP_DBSTATE;
	cop->op_ppaddr = PL_ppaddr[ OP_DBSTATE ];
    }
    else {
	SensorCall(7530);cop->op_type = OP_NEXTSTATE;
	cop->op_ppaddr = PL_ppaddr[ OP_NEXTSTATE ];
    }
    SensorCall(7532);cop->op_flags = (U8)flags;
    CopHINTS_set(cop, PL_hints);
#ifdef NATIVE_HINTS
    cop->op_private |= NATIVE_HINTS;
#endif
    CopHINTS_set(&PL_compiling, CopHINTS_get(cop));
    cop->op_next = (OP*)cop;

    cop->cop_seq = seq;
    cop->cop_warnings = DUP_WARNINGS(PL_curcop->cop_warnings);
    CopHINTHASH_set(cop, cophh_copy(CopHINTHASH_get(PL_curcop)));
    SensorCall(7534);if (label) {
	SensorCall(7533);Perl_cop_store_label(aTHX_ cop, label, strlen(label), utf8);

	PL_hints |= HINT_BLOCK_SCOPE;
	/* It seems that we need to defer freeing this pointer, as other parts
	   of the grammar end up wanting to copy it after this op has been
	   created. */
	SAVEFREEPV(label);
    }

    SensorCall(7536);if (PL_parser && PL_parser->copline == NOLINE)
        CopLINE_set(cop, CopLINE(PL_curcop));
    else {
	CopLINE_set(cop, PL_parser->copline);
	SensorCall(7535);if (PL_parser)
	    PL_parser->copline = NOLINE;
    }
#ifdef USE_ITHREADS
    CopFILE_set(cop, CopFILE(PL_curcop));	/* XXX share in a pvtable? */
#else
    CopFILEGV_set(cop, CopFILEGV(PL_curcop));
#endif
    CopSTASH_set(cop, PL_curstash);

    SensorCall(7542);if ((PERLDB_LINE || PERLDB_SAVESRC) && PL_curstash != PL_debstash) {
	/* this line can have a breakpoint - store the cop in IV */
	SensorCall(7537);AV *av = CopFILEAVx(PL_curcop);
	SensorCall(7541);if (av) {
	    SensorCall(7538);SV * const * const svp = av_fetch(av, (I32)CopLINE(cop), FALSE);
	    SensorCall(7540);if (svp && *svp != &PL_sv_undef ) {
		SensorCall(7539);(void)SvIOK_on(*svp);
		SvIV_set(*svp, PTR2IV(cop));
	    }
	}
    }

    SensorCall(7543);if (flags & OPf_SPECIAL)
	op_null((OP*)cop);
    {OP * ReplaceReturn695 = op_prepend_elem(OP_LINESEQ, (OP*)cop, o); SensorCall(7544); return ReplaceReturn695;}
}

/*
=for apidoc Am|OP *|newLOGOP|I32 type|I32 flags|OP *first|OP *other

Constructs, checks, and returns a logical (flow control) op.  I<type>
is the opcode.  I<flags> gives the eight bits of C<op_flags>, except
that C<OPf_KIDS> will be set automatically, and, shifted up eight bits,
the eight bits of C<op_private>, except that the bit with value 1 is
automatically set.  I<first> supplies the expression controlling the
flow, and I<other> supplies the side (alternate) chain of ops; they are
consumed by this function and become part of the constructed op tree.

=cut
*/

OP *
Perl_newLOGOP(pTHX_ I32 type, I32 flags, OP *first, OP *other)
{
SensorCall(7545);    dVAR;

    PERL_ARGS_ASSERT_NEWLOGOP;

    {OP * ReplaceReturn694 = new_logop(type, flags, &first, &other); SensorCall(7546); return ReplaceReturn694;}
}

STATIC OP *
S_search_const(pTHX_ OP *o)
{
    PERL_ARGS_ASSERT_SEARCH_CONST;

    SensorCall(7547);switch (o->op_type) {
	case OP_CONST:
	    {OP * ReplaceReturn693 = o; SensorCall(7548); return ReplaceReturn693;}
	case OP_NULL:
	    SensorCall(7549);if (o->op_flags & OPf_KIDS)
		return search_const(cUNOPo->op_first);
	    SensorCall(7550);break;
	case OP_LEAVE:
	case OP_SCOPE:
	case OP_LINESEQ:
	{
	    SensorCall(7551);OP *kid;
	    SensorCall(7552);if (!(o->op_flags & OPf_KIDS))
		return NULL;
	    SensorCall(7553);kid = cLISTOPo->op_first;
	    SensorCall(7559);do {
		SensorCall(7554);switch (kid->op_type) {
		    case OP_ENTER:
		    case OP_NULL:
		    case OP_NEXTSTATE:
			SensorCall(7555);kid = kid->op_sibling;
			SensorCall(7556);break;
		    default:
			SensorCall(7557);if (kid != cLISTOPo->op_last)
			    return NULL;
			SensorCall(7558);goto last;
		}
	    } while (kid);
	    SensorCall(7561);if (!kid)
		{/*703*/SensorCall(7560);kid = cLISTOPo->op_last;/*704*/}
last:
	    {OP * ReplaceReturn692 = search_const(kid); SensorCall(7562); return ReplaceReturn692;}
	}
    }

    {OP * ReplaceReturn691 = NULL; SensorCall(7563); return ReplaceReturn691;}
}

STATIC OP *
S_new_logop(pTHX_ I32 type, I32 flags, OP** firstp, OP** otherp)
{
SensorCall(7564);    dVAR;
    LOGOP *logop;
    OP *o;
    OP *first;
    OP *other;
    OP *cstop = NULL;
    int prepend_not = 0;

    PERL_ARGS_ASSERT_NEW_LOGOP;

    first = *firstp;
    other = *otherp;

    SensorCall(7565);if (type == OP_XOR)		/* Not short circuit, but here by precedence. */
	return newBINOP(type, flags, scalar(first), scalar(other));

    assert((PL_opargs[type] & OA_CLASS_MASK) == OA_LOGOP);

    scalarboolean(first);
    /* optimize AND and OR ops that have NOTs as children */
    SensorCall(7572);if (first->op_type == OP_NOT
	&& (first->op_flags & OPf_KIDS)
	&& ((first->op_flags & OPf_SPECIAL) /* unless ($x) { } */
	    || (other->op_type == OP_NOT))  /* if (!$x && !$y) { } */
	&& !PL_madskills) {
	SensorCall(7566);if (type == OP_AND || type == OP_OR) {
	    SensorCall(7567);if (type == OP_AND)
		{/*597*/SensorCall(7568);type = OP_OR;/*598*/}
	    else
		{/*599*/SensorCall(7569);type = OP_AND;/*600*/}
	    op_null(first);
	    SensorCall(7571);if (other->op_type == OP_NOT) { /* !a AND|OR !b => !(a OR|AND b) */
		op_null(other);
		SensorCall(7570);prepend_not = 1; /* prepend a NOT op later */
	    }
	}
    }
    /* search for a constant op that could let us fold the test */
    SensorCall(7605);if ((cstop = search_const(first))) {
	SensorCall(7573);if (cstop->op_private & OPpCONST_STRICT)
	    no_bareword_allowed(cstop);
	else {/*601*/SensorCall(7574);if ((cstop->op_private & OPpCONST_BARE))
		{/*603*/SensorCall(7575);Perl_ck_warner(aTHX_ packWARN(WARN_BAREWORD), "Bareword found in conditional");/*604*/}/*602*/}
	SensorCall(7593);if ((type == OP_AND &&  SvTRUE(((SVOP*)cstop)->op_sv)) ||
	    (type == OP_OR  && !SvTRUE(((SVOP*)cstop)->op_sv)) ||
	    (type == OP_DOR && !SvOK(((SVOP*)cstop)->op_sv))) {
	    SensorCall(7576);*firstp = NULL;
	    SensorCall(7577);if (other->op_type == OP_CONST)
		other->op_private |= OPpCONST_SHORTCIRCUIT;
	    SensorCall(7580);if (PL_madskills) {
		SensorCall(7578);OP *newop = newUNOP(OP_NULL, 0, other);
		op_getmad(first, newop, '1');
		newop->op_targ = type;	/* set "was" field */
		{OP * ReplaceReturn690 = newop; SensorCall(7579); return ReplaceReturn690;}
	    }
	    op_free(first);
	    SensorCall(7581);if (other->op_type == OP_LEAVE)
		other = newUNOP(OP_NULL, OPf_SPECIAL, other);
	    else if (other->op_type == OP_MATCH
	          || other->op_type == OP_SUBST
	          || other->op_type == OP_TRANSR
	          || other->op_type == OP_TRANS)
		/* Mark the op as being unbindable with =~ */
		other->op_flags |= OPf_SPECIAL;
	    {OP * ReplaceReturn689 = other; SensorCall(7582); return ReplaceReturn689;}
	}
	else {
	    /* check for C<my $x if 0>, or C<my($x,$y) if 0> */
	    SensorCall(7583);const OP *o2 = other;
	    SensorCall(7585);if ( ! (o2->op_type == OP_LIST
		    && (( o2 = cUNOPx(o2)->op_first))
		    && o2->op_type == OP_PUSHMARK
		    && (( o2 = o2->op_sibling)) )
	    )
		{/*605*/SensorCall(7584);o2 = other;/*606*/}
	    SensorCall(7587);if ((o2->op_type == OP_PADSV || o2->op_type == OP_PADAV
			|| o2->op_type == OP_PADHV)
		&& o2->op_private & OPpLVAL_INTRO
		&& !(o2->op_private & OPpPAD_STATE))
	    {
		SensorCall(7586);Perl_ck_warner_d(aTHX_ packWARN(WARN_DEPRECATED),
				 "Deprecated use of my() in false conditional");
	    }

	    SensorCall(7588);*otherp = NULL;
	    SensorCall(7589);if (first->op_type == OP_CONST)
		first->op_private |= OPpCONST_SHORTCIRCUIT;
	    SensorCall(7591);if (PL_madskills) {
		SensorCall(7590);first = newUNOP(OP_NULL, 0, first);
		op_getmad(other, first, '2');
		first->op_targ = type;	/* set "was" field */
	    }
	    else
		op_free(other);
	    {OP * ReplaceReturn688 = first; SensorCall(7592); return ReplaceReturn688;}
	}
    }
    else {/*607*/SensorCall(7594);if ((first->op_flags & OPf_KIDS) && type != OP_DOR
	&& ckWARN(WARN_MISC)) /* [#24076] Don't warn for <FH> err FOO. */
    {
	SensorCall(7595);const OP * const k1 = ((UNOP*)first)->op_first;
	const OP * const k2 = k1->op_sibling;
	OPCODE warnop = 0;
	SensorCall(7602);switch (first->op_type)
	{
	case OP_NULL:
	    SensorCall(7596);if (k2 && k2->op_type == OP_READLINE
		  && (k2->op_flags & OPf_STACKED)
		  && ((k1->op_flags & OPf_WANT) == OPf_WANT_SCALAR))
	    {
		SensorCall(7597);warnop = k2->op_type;
	    }
	    SensorCall(7598);break;

	case OP_SASSIGN:
	    SensorCall(7599);if (k1->op_type == OP_READDIR
		  || k1->op_type == OP_GLOB
		  || (k1->op_type == OP_NULL && k1->op_targ == OP_GLOB)
                 || k1->op_type == OP_EACH
                 || k1->op_type == OP_AEACH)
	    {
		SensorCall(7600);warnop = ((k1->op_type == OP_NULL)
			  ? (OPCODE)k1->op_targ : k1->op_type);
	    }
	    SensorCall(7601);break;
	}
	SensorCall(7604);if (warnop) {
	    SensorCall(7603);const line_t oldline = CopLINE(PL_curcop);
	    CopLINE_set(PL_curcop, PL_parser->copline);
	    Perl_warner(aTHX_ packWARN(WARN_MISC),
		 "Value of %s%s can be \"0\"; test with defined()",
		 PL_op_desc[warnop],
		 ((warnop == OP_READLINE || warnop == OP_GLOB)
		  ? " construct" : "() operator"));
	    CopLINE_set(PL_curcop, oldline);
	}
    ;/*608*/}}

    SensorCall(7607);if (!other)
	{/*609*/{OP * ReplaceReturn687 = first; SensorCall(7606); return ReplaceReturn687;}/*610*/}

    SensorCall(7608);if (type == OP_ANDASSIGN || type == OP_ORASSIGN || type == OP_DORASSIGN)
	other->op_private |= OPpASSIGN_BACKWARDS;  /* other is an OP_SASSIGN */

    NewOp(1101, logop, 1, LOGOP);

    SensorCall(7609);logop->op_type = (OPCODE)type;
    logop->op_ppaddr = PL_ppaddr[type];
    logop->op_first = first;
    logop->op_flags = (U8)(flags | OPf_KIDS);
    logop->op_other = LINKLIST(other);
    logop->op_private = (U8)(1 | (flags >> 8));

    /* establish postfix order */
    logop->op_next = LINKLIST(first);
    first->op_next = (OP*)logop;
    first->op_sibling = other;

    CHECKOP(type,logop);

    o = newUNOP(prepend_not ? OP_NOT : OP_NULL, 0, (OP*)logop);
    other->op_next = o;

    {OP * ReplaceReturn686 = o; SensorCall(7610); return ReplaceReturn686;}
}

/*
=for apidoc Am|OP *|newCONDOP|I32 flags|OP *first|OP *trueop|OP *falseop

Constructs, checks, and returns a conditional-expression (C<cond_expr>)
op.  I<flags> gives the eight bits of C<op_flags>, except that C<OPf_KIDS>
will be set automatically, and, shifted up eight bits, the eight bits of
C<op_private>, except that the bit with value 1 is automatically set.
I<first> supplies the expression selecting between the two branches,
and I<trueop> and I<falseop> supply the branches; they are consumed by
this function and become part of the constructed op tree.

=cut
*/

OP *
Perl_newCONDOP(pTHX_ I32 flags, OP *first, OP *trueop, OP *falseop)
{
SensorCall(7611);    dVAR;
    LOGOP *logop;
    OP *start;
    OP *o;
    OP *cstop;

    PERL_ARGS_ASSERT_NEWCONDOP;

    SensorCall(7612);if (!falseop)
	return newLOGOP(OP_AND, 0, first, trueop);
    SensorCall(7613);if (!trueop)
	return newLOGOP(OP_OR, 0, first, falseop);

    scalarboolean(first);
    SensorCall(7620);if ((cstop = search_const(first))) {
	/* Left or right arm of the conditional?  */
	SensorCall(7614);const bool left = SvTRUE(((SVOP*)cstop)->op_sv);
	OP *live = left ? trueop : falseop;
	OP *const dead = left ? falseop : trueop;
        SensorCall(7615);if (cstop->op_private & OPpCONST_BARE &&
	    cstop->op_private & OPpCONST_STRICT) {
	    no_bareword_allowed(cstop);
	}
	SensorCall(7617);if (PL_madskills) {
	    /* This is all dead code when PERL_MAD is not defined.  */
	    SensorCall(7616);live = newUNOP(OP_NULL, 0, live);
	    op_getmad(first, live, 'C');
	    op_getmad(dead, live, left ? 'e' : 't');
	} else {
	    op_free(first);
	    op_free(dead);
	}
	SensorCall(7618);if (live->op_type == OP_LEAVE)
	    live = newUNOP(OP_NULL, OPf_SPECIAL, live);
	else if (live->op_type == OP_MATCH || live->op_type == OP_SUBST
	      || live->op_type == OP_TRANS || live->op_type == OP_TRANSR)
	    /* Mark the op as being unbindable with =~ */
	    live->op_flags |= OPf_SPECIAL;
	{OP * ReplaceReturn685 = live; SensorCall(7619); return ReplaceReturn685;}
    }
    NewOp(1101, logop, 1, LOGOP);
    SensorCall(7621);logop->op_type = OP_COND_EXPR;
    logop->op_ppaddr = PL_ppaddr[OP_COND_EXPR];
    logop->op_first = first;
    logop->op_flags = (U8)(flags | OPf_KIDS);
    logop->op_private = (U8)(1 | (flags >> 8));
    logop->op_other = LINKLIST(trueop);
    logop->op_next = LINKLIST(falseop);

    CHECKOP(OP_COND_EXPR, /* that's logop->op_type */
	    logop);

    /* establish postfix order */
    start = LINKLIST(first);
    first->op_next = (OP*)logop;

    first->op_sibling = trueop;
    trueop->op_sibling = falseop;
    o = newUNOP(OP_NULL, 0, (OP*)logop);

    trueop->op_next = falseop->op_next = o;

    o->op_next = start;
    {OP * ReplaceReturn684 = o; SensorCall(7622); return ReplaceReturn684;}
}

/*
=for apidoc Am|OP *|newRANGE|I32 flags|OP *left|OP *right

Constructs and returns a C<range> op, with subordinate C<flip> and
C<flop> ops.  I<flags> gives the eight bits of C<op_flags> for the
C<flip> op and, shifted up eight bits, the eight bits of C<op_private>
for both the C<flip> and C<range> ops, except that the bit with value
1 is automatically set.  I<left> and I<right> supply the expressions
controlling the endpoints of the range; they are consumed by this function
and become part of the constructed op tree.

=cut
*/

OP *
Perl_newRANGE(pTHX_ I32 flags, OP *left, OP *right)
{
SensorCall(7623);    dVAR;
    LOGOP *range;
    OP *flip;
    OP *flop;
    OP *leftstart;
    OP *o;

    PERL_ARGS_ASSERT_NEWRANGE;

    NewOp(1101, range, 1, LOGOP);

    range->op_type = OP_RANGE;
    range->op_ppaddr = PL_ppaddr[OP_RANGE];
    range->op_first = left;
    range->op_flags = OPf_KIDS;
    leftstart = LINKLIST(left);
    range->op_other = LINKLIST(right);
    range->op_private = (U8)(1 | (flags >> 8));

    left->op_sibling = right;

    range->op_next = (OP*)range;
    flip = newUNOP(OP_FLIP, flags, (OP*)range);
    flop = newUNOP(OP_FLOP, 0, flip);
    o = newUNOP(OP_NULL, 0, flop);
    LINKLIST(flop);
    range->op_next = leftstart;

    left->op_next = flip;
    right->op_next = flop;

    range->op_targ = pad_alloc(OP_RANGE, SVs_PADMY);
    sv_upgrade(PAD_SV(range->op_targ), SVt_PVNV);
    flip->op_targ = pad_alloc(OP_RANGE, SVs_PADMY);
    sv_upgrade(PAD_SV(flip->op_targ), SVt_PVNV);

    flip->op_private =  left->op_type == OP_CONST ? OPpFLIP_LINENUM : 0;
    flop->op_private = right->op_type == OP_CONST ? OPpFLIP_LINENUM : 0;

    /* check barewords before they might be optimized aways */
    SensorCall(7624);if (flip->op_private && cSVOPx(left)->op_private & OPpCONST_STRICT)
	no_bareword_allowed(left);
    SensorCall(7625);if (flop->op_private && cSVOPx(right)->op_private & OPpCONST_STRICT)
	no_bareword_allowed(right);

    SensorCall(7626);flip->op_next = o;
    SensorCall(7627);if (!flip->op_private || !flop->op_private)
	LINKLIST(o);		/* blow off optimizer unless constant */

    {OP * ReplaceReturn683 = o; SensorCall(7628); return ReplaceReturn683;}
}

/*
=for apidoc Am|OP *|newLOOPOP|I32 flags|I32 debuggable|OP *expr|OP *block

Constructs, checks, and returns an op tree expressing a loop.  This is
only a loop in the control flow through the op tree; it does not have
the heavyweight loop structure that allows exiting the loop by C<last>
and suchlike.  I<flags> gives the eight bits of C<op_flags> for the
top-level op, except that some bits will be set automatically as required.
I<expr> supplies the expression controlling loop iteration, and I<block>
supplies the body of the loop; they are consumed by this function and
become part of the constructed op tree.  I<debuggable> is currently
unused and should always be 1.

=cut
*/

OP *
Perl_newLOOPOP(pTHX_ I32 flags, I32 debuggable, OP *expr, OP *block)
{
SensorCall(7629);    dVAR;
    OP* listop;
    OP* o;
    const bool once = block && block->op_flags & OPf_SPECIAL &&
      (block->op_type == OP_ENTERSUB || block->op_type == OP_NULL);

    PERL_UNUSED_ARG(debuggable);

    SensorCall(7641);if (expr) {
	SensorCall(7630);if (once && expr->op_type == OP_CONST && !SvTRUE(((SVOP*)expr)->op_sv))
	    {/*283*/{OP * ReplaceReturn682 = block; SensorCall(7631); return ReplaceReturn682;}/*284*/}	/* do {} while 0 does once */
	SensorCall(7640);if (expr->op_type == OP_READLINE
	    || expr->op_type == OP_READDIR
	    || expr->op_type == OP_GLOB
	    || (expr->op_type == OP_NULL && expr->op_targ == OP_GLOB)) {
	    SensorCall(7632);expr = newUNOP(OP_DEFINED, 0,
		newASSIGNOP(0, newDEFSVOP(), 0, expr) );
	} else {/*285*/SensorCall(7633);if (expr->op_flags & OPf_KIDS) {
	    SensorCall(7634);const OP * const k1 = ((UNOP*)expr)->op_first;
	    const OP * const k2 = k1 ? k1->op_sibling : NULL;
	    SensorCall(7639);switch (expr->op_type) {
	      case OP_NULL:
		SensorCall(7635);if (k2 && (k2->op_type == OP_READLINE || k2->op_type == OP_READDIR)
		      && (k2->op_flags & OPf_STACKED)
		      && ((k1->op_flags & OPf_WANT) == OPf_WANT_SCALAR))
		    expr = newUNOP(OP_DEFINED, 0, expr);
		SensorCall(7636);break;

	      case OP_SASSIGN:
		SensorCall(7637);if (k1 && (k1->op_type == OP_READDIR
		      || k1->op_type == OP_GLOB
		      || (k1->op_type == OP_NULL && k1->op_targ == OP_GLOB)
                     || k1->op_type == OP_EACH
                     || k1->op_type == OP_AEACH))
		    expr = newUNOP(OP_DEFINED, 0, expr);
		SensorCall(7638);break;
	    }
	;/*286*/}}
    }

    /* if block is null, the next op_append_elem() would put UNSTACK, a scalar
     * op, in listop. This is wrong. [perl #27024] */
    SensorCall(7642);if (!block)
	block = newOP(OP_NULL, 0);
    SensorCall(7643);listop = op_append_elem(OP_LINESEQ, block, newOP(OP_UNSTACK, 0));
    o = new_logop(OP_AND, 0, &expr, &listop);

    SensorCall(7644);if (listop)
	((LISTOP*)listop)->op_last->op_next = LINKLIST(o);

    SensorCall(7646);if (once && o != listop)
	{/*287*/SensorCall(7645);o->op_next = ((LOGOP*)cUNOPo->op_first)->op_other;/*288*/}

    SensorCall(7647);if (o == listop)
	o = newUNOP(OP_NULL, 0, o);	/* or do {} while 1 loses outer block */

    SensorCall(7648);o->op_flags |= flags;
    o = op_scope(o);
    o->op_flags |= OPf_SPECIAL;	/* suppress POPBLOCK curpm restoration*/
    {OP * ReplaceReturn681 = o; SensorCall(7649); return ReplaceReturn681;}
}

/*
=for apidoc Am|OP *|newWHILEOP|I32 flags|I32 debuggable|LOOP *loop|OP *expr|OP *block|OP *cont|I32 has_my

Constructs, checks, and returns an op tree expressing a C<while> loop.
This is a heavyweight loop, with structure that allows exiting the loop
by C<last> and suchlike.

I<loop> is an optional preconstructed C<enterloop> op to use in the
loop; if it is null then a suitable op will be constructed automatically.
I<expr> supplies the loop's controlling expression.  I<block> supplies the
main body of the loop, and I<cont> optionally supplies a C<continue> block
that operates as a second half of the body.  All of these optree inputs
are consumed by this function and become part of the constructed op tree.

I<flags> gives the eight bits of C<op_flags> for the C<leaveloop>
op and, shifted up eight bits, the eight bits of C<op_private> for
the C<leaveloop> op, except that (in both cases) some bits will be set
automatically.  I<debuggable> is currently unused and should always be 1.
I<has_my> can be supplied as true to force the
loop body to be enclosed in its own scope.

=cut
*/

OP *
Perl_newWHILEOP(pTHX_ I32 flags, I32 debuggable, LOOP *loop,
	OP *expr, OP *block, OP *cont, I32 has_my)
{
SensorCall(7650);    dVAR;
    OP *redo;
    OP *next = NULL;
    OP *listop;
    OP *o;
    U8 loopflags = 0;

    PERL_UNUSED_ARG(debuggable);

    SensorCall(7660);if (expr) {
	SensorCall(7651);if (expr->op_type == OP_READLINE
         || expr->op_type == OP_READDIR
         || expr->op_type == OP_GLOB
		     || (expr->op_type == OP_NULL && expr->op_targ == OP_GLOB)) {
	    SensorCall(7652);expr = newUNOP(OP_DEFINED, 0,
		newASSIGNOP(0, newDEFSVOP(), 0, expr) );
	} else {/*297*/SensorCall(7653);if (expr->op_flags & OPf_KIDS) {
	    SensorCall(7654);const OP * const k1 = ((UNOP*)expr)->op_first;
	    const OP * const k2 = (k1) ? k1->op_sibling : NULL;
	    SensorCall(7659);switch (expr->op_type) {
	      case OP_NULL:
		SensorCall(7655);if (k2 && (k2->op_type == OP_READLINE || k2->op_type == OP_READDIR)
		      && (k2->op_flags & OPf_STACKED)
		      && ((k1->op_flags & OPf_WANT) == OPf_WANT_SCALAR))
		    expr = newUNOP(OP_DEFINED, 0, expr);
		SensorCall(7656);break;

	      case OP_SASSIGN:
		SensorCall(7657);if (k1 && (k1->op_type == OP_READDIR
		      || k1->op_type == OP_GLOB
		      || (k1->op_type == OP_NULL && k1->op_targ == OP_GLOB)
                     || k1->op_type == OP_EACH
                     || k1->op_type == OP_AEACH))
		    expr = newUNOP(OP_DEFINED, 0, expr);
		SensorCall(7658);break;
	    }
	;/*298*/}}
    }

    SensorCall(7663);if (!block)
	block = newOP(OP_NULL, 0);
    else {/*299*/SensorCall(7661);if (cont || has_my) {
	SensorCall(7662);block = op_scope(block);
    ;/*300*/}}

    SensorCall(7665);if (cont) {
	SensorCall(7664);next = LINKLIST(cont);
    }
    SensorCall(7670);if (expr) {
	SensorCall(7666);OP * const unstack = newOP(OP_UNSTACK, 0);
	SensorCall(7668);if (!next)
	    {/*301*/SensorCall(7667);next = unstack;/*302*/}
	SensorCall(7669);cont = op_append_elem(OP_LINESEQ, cont, unstack);
    }

    assert(block);
    SensorCall(7671);listop = op_append_list(OP_LINESEQ, block, cont);
    assert(listop);
    redo = LINKLIST(listop);

    SensorCall(7678);if (expr) {
	scalar(listop);
	SensorCall(7672);o = new_logop(OP_AND, 0, &expr, &listop);
	SensorCall(7674);if (o == expr && o->op_type == OP_CONST && !SvTRUE(cSVOPo->op_sv)) {
	    op_free(expr);		/* oops, it's a while (0) */
	    op_free((OP*)loop);
	    {OP * ReplaceReturn680 = NULL; SensorCall(7673); return ReplaceReturn680;}		/* listop already freed by new_logop */
	}
	SensorCall(7676);if (listop)
	    {/*303*/SensorCall(7675);((LISTOP*)listop)->op_last->op_next =
		(o == listop ? redo : LINKLIST(o));/*304*/}
    }
    else
	{/*305*/SensorCall(7677);o = listop;/*306*/}

    SensorCall(7680);if (!loop) {
	NewOp(1101,loop,1,LOOP);
	SensorCall(7679);loop->op_type = OP_ENTERLOOP;
	loop->op_ppaddr = PL_ppaddr[OP_ENTERLOOP];
	loop->op_private = 0;
	loop->op_next = (OP*)loop;
    }

    SensorCall(7681);o = newBINOP(OP_LEAVELOOP, 0, (OP*)loop, o);

    loop->op_redoop = redo;
    loop->op_lastop = o;
    o->op_private |= loopflags;

    SensorCall(7684);if (next)
	{/*307*/SensorCall(7682);loop->op_nextop = next;/*308*/}
    else
	{/*309*/SensorCall(7683);loop->op_nextop = o;/*310*/}

    SensorCall(7685);o->op_flags |= flags;
    o->op_private |= (flags >> 8);
    {OP * ReplaceReturn679 = o; SensorCall(7686); return ReplaceReturn679;}
}

/*
=for apidoc Am|OP *|newFOROP|I32 flags|OP *sv|OP *expr|OP *block|OP *cont

Constructs, checks, and returns an op tree expressing a C<foreach>
loop (iteration through a list of values).  This is a heavyweight loop,
with structure that allows exiting the loop by C<last> and suchlike.

I<sv> optionally supplies the variable that will be aliased to each
item in turn; if null, it defaults to C<$_> (either lexical or global).
I<expr> supplies the list of values to iterate over.  I<block> supplies
the main body of the loop, and I<cont> optionally supplies a C<continue>
block that operates as a second half of the body.  All of these optree
inputs are consumed by this function and become part of the constructed
op tree.

I<flags> gives the eight bits of C<op_flags> for the C<leaveloop>
op and, shifted up eight bits, the eight bits of C<op_private> for
the C<leaveloop> op, except that (in both cases) some bits will be set
automatically.

=cut
*/

OP *
Perl_newFOROP(pTHX_ I32 flags, OP *sv, OP *expr, OP *block, OP *cont)
{
SensorCall(7687);    dVAR;
    LOOP *loop;
    OP *wop;
    PADOFFSET padoff = 0;
    I32 iterflags = 0;
    I32 iterpflags = 0;
    OP *madsv = NULL;

    PERL_ARGS_ASSERT_NEWFOROP;

    SensorCall(7706);if (sv) {
	SensorCall(7688);if (sv->op_type == OP_RV2SV) {	/* symbol table variable */
	    SensorCall(7689);iterpflags = sv->op_private & OPpOUR_INTRO; /* for our $x () */
	    sv->op_type = OP_RV2GV;
	    sv->op_ppaddr = PL_ppaddr[OP_RV2GV];

	    /* The op_type check is needed to prevent a possible segfault
	     * if the loop variable is undeclared and 'strict vars' is in
	     * effect. This is illegal but is nonetheless parsed, so we
	     * may reach this point with an OP_CONST where we're expecting
	     * an OP_GV.
	     */
	    SensorCall(7690);if (cUNOPx(sv)->op_first->op_type == OP_GV
	     && cGVOPx_gv(cUNOPx(sv)->op_first) == PL_defgv)
		iterpflags |= OPpITER_DEF;
	}
	else {/*261*/SensorCall(7691);if (sv->op_type == OP_PADSV) { /* private variable */
	    SensorCall(7692);iterpflags = sv->op_private & OPpLVAL_INTRO; /* for my $x () */
	    padoff = sv->op_targ;
	    SensorCall(7695);if (PL_madskills)
		{/*263*/SensorCall(7693);madsv = sv;/*264*/}
	    else {
		SensorCall(7694);sv->op_targ = 0;
		op_free(sv);
	    }
	    SensorCall(7696);sv = NULL;
	}
	else
	    {/*265*/SensorCall(7697);Perl_croak(aTHX_ "Can't use %s for loop variable", PL_op_desc[sv->op_type]);/*266*/}/*262*/}
	SensorCall(7700);if (padoff) {
	    SensorCall(7698);SV *const namesv = PAD_COMPNAME_SV(padoff);
	    STRLEN len;
	    const char *const name = SvPV_const(namesv, len);

	    SensorCall(7699);if (len == 2 && name[0] == '$' && name[1] == '_')
		iterpflags |= OPpITER_DEF;
	}
    }
    else {
        SensorCall(7701);const PADOFFSET offset = pad_findmy_pvs("$_", 0);
	SensorCall(7704);if (offset == NOT_IN_PAD || PAD_COMPNAME_FLAGS_isOUR(offset)) {
	    SensorCall(7702);sv = newGVOP(OP_GV, 0, PL_defgv);
	}
	else {
	    SensorCall(7703);padoff = offset;
	}
	SensorCall(7705);iterpflags |= OPpITER_DEF;
    }
    SensorCall(7711);if (expr->op_type == OP_RV2AV || expr->op_type == OP_PADAV) {
	SensorCall(7707);expr = op_lvalue(force_list(scalar(ref(expr, OP_ITER))), OP_GREPSTART);
	iterflags |= OPf_STACKED;
    }
    else {/*267*/SensorCall(7708);if (expr->op_type == OP_NULL &&
             (expr->op_flags & OPf_KIDS) &&
             ((BINOP*)expr)->op_first->op_type == OP_FLOP)
    {
	/* Basically turn for($x..$y) into the same as for($x,$y), but we
	 * set the STACKED flag to indicate that these values are to be
	 * treated as min/max values by 'pp_iterinit'.
	 */
	SensorCall(7709);const UNOP* const flip = (UNOP*)((UNOP*)((BINOP*)expr)->op_first)->op_first;
	LOGOP* const range = (LOGOP*) flip->op_first;
	OP* const left  = range->op_first;
	OP* const right = left->op_sibling;
	LISTOP* listop;

	range->op_flags &= ~OPf_KIDS;
	range->op_first = NULL;

	listop = (LISTOP*)newLISTOP(OP_LIST, 0, left, right);
	listop->op_first->op_next = range->op_next;
	left->op_next = range->op_other;
	right->op_next = (OP*)listop;
	listop->op_next = listop->op_first;

#ifdef PERL_MAD
	op_getmad(expr,(OP*)listop,'O');
#else
	op_free(expr);
#endif
	expr = (OP*)(listop);
        op_null(expr);
	iterflags |= OPf_STACKED;
    }
    else {
        SensorCall(7710);expr = op_lvalue(force_list(expr), OP_GREPSTART);
    ;/*268*/}}

    SensorCall(7712);loop = (LOOP*)list(convert(OP_ENTERITER, iterflags,
			       op_append_elem(OP_LIST, expr, scalar(sv))));
    assert(!loop->op_next);
    /* for my  $x () sets OPpLVAL_INTRO;
     * for our $x () sets OPpOUR_INTRO */
    loop->op_private = (U8)iterpflags;
#ifdef PL_OP_SLAB_ALLOC
    {
	LOOP *tmp;
	NewOp(1234,tmp,1,LOOP);
	Copy(loop,tmp,1,LISTOP);
	S_op_destroy(aTHX_ (OP*)loop);
	loop = tmp;
    }
#else
    loop = (LOOP*)PerlMemShared_realloc(loop, sizeof(LOOP));
#endif
    loop->op_targ = padoff;
    wop = newWHILEOP(flags, 1, loop, newOP(OP_ITER, 0), block, cont, 0);
    SensorCall(7713);if (madsv)
	op_getmad(madsv, (OP*)loop, 'v');
    {OP * ReplaceReturn678 = wop; SensorCall(7714); return ReplaceReturn678;}
}

/*
=for apidoc Am|OP *|newLOOPEX|I32 type|OP *label

Constructs, checks, and returns a loop-exiting op (such as C<goto>
or C<last>).  I<type> is the opcode.  I<label> supplies the parameter
determining the target of the op; it is consumed by this function and
become part of the constructed op tree.

=cut
*/

OP*
Perl_newLOOPEX(pTHX_ I32 type, OP *label)
{
SensorCall(7715);    dVAR;
    OP *o;

    PERL_ARGS_ASSERT_NEWLOOPEX;

    assert((PL_opargs[type] & OA_CLASS_MASK) == OA_LOOPEXOP);

    SensorCall(7720);if (type != OP_GOTO || label->op_type == OP_CONST) {
	/* "last()" means "last" */
	SensorCall(7716);if (label->op_type == OP_STUB && (label->op_flags & OPf_PARENS))
	    o = newOP(type, OPf_SPECIAL);
	else {
	    SensorCall(7717);o = newPVOP(type,
                        label->op_type == OP_CONST
                            ? SvUTF8(((SVOP*)label)->op_sv)
                            : 0,
                        savesharedpv(label->op_type == OP_CONST
				? SvPV_nolen_const(((SVOP*)label)->op_sv)
				: ""));
	}
#ifdef PERL_MAD
	op_getmad(label,o,'L');
#else
	op_free(label);
#endif
    }
    else {
	/* Check whether it's going to be a goto &function */
	SensorCall(7718);if (label->op_type == OP_ENTERSUB
		&& !(label->op_flags & OPf_STACKED))
	    label = newUNOP(OP_REFGEN, 0, op_lvalue(label, OP_REFGEN));
	SensorCall(7719);o = newUNOP(type, OPf_STACKED, label);
    }
    PL_hints |= HINT_BLOCK_SCOPE;
    {OP * ReplaceReturn677 = o; SensorCall(7721); return ReplaceReturn677;}
}

/* if the condition is a literal array or hash
   (or @{ ... } etc), make a reference to it.
 */
STATIC OP *
S_ref_array_or_hash(pTHX_ OP *cond)
{
    SensorCall(7722);if (cond
    && (cond->op_type == OP_RV2AV
    ||  cond->op_type == OP_PADAV
    ||  cond->op_type == OP_RV2HV
    ||  cond->op_type == OP_PADHV))

	return newUNOP(OP_REFGEN, 0, op_lvalue(cond, OP_REFGEN));

    else {/*699*/SensorCall(7723);if(cond
    && (cond->op_type == OP_ASLICE
    ||  cond->op_type == OP_HSLICE)) {

	/* anonlist now needs a list from this op, was previously used in
	 * scalar context */
	SensorCall(7724);cond->op_flags |= ~(OPf_WANT_SCALAR | OPf_REF);
	cond->op_flags |= OPf_WANT_LIST;

	{OP * ReplaceReturn676 = newANONLIST(op_lvalue(cond, OP_ANONLIST)); SensorCall(7725); return ReplaceReturn676;}
    }

    else
	{/*701*/{OP * ReplaceReturn675 = cond; SensorCall(7726); return ReplaceReturn675;}/*702*/}/*700*/}
SensorCall(7727);}

/* These construct the optree fragments representing given()
   and when() blocks.

   entergiven and enterwhen are LOGOPs; the op_other pointer
   points up to the associated leave op. We need this so we
   can put it in the context and make break/continue work.
   (Also, of course, pp_enterwhen will jump straight to
   op_other if the match fails.)
 */

STATIC OP *
S_newGIVWHENOP(pTHX_ OP *cond, OP *block,
		   I32 enter_opcode, I32 leave_opcode,
		   PADOFFSET entertarg)
{
SensorCall(7728);    dVAR;
    LOGOP *enterop;
    OP *o;

    PERL_ARGS_ASSERT_NEWGIVWHENOP;

    NewOp(1101, enterop, 1, LOGOP);
    enterop->op_type = (Optype)enter_opcode;
    enterop->op_ppaddr = PL_ppaddr[enter_opcode];
    enterop->op_flags =  (U8) OPf_KIDS;
    enterop->op_targ = ((entertarg == NOT_IN_PAD) ? 0 : entertarg);
    enterop->op_private = 0;

    o = newUNOP(leave_opcode, 0, (OP *) enterop);

    SensorCall(7731);if (cond) {
	SensorCall(7729);enterop->op_first = scalar(cond);
	cond->op_sibling = block;

	o->op_next = LINKLIST(cond);
	cond->op_next = (OP *) enterop;
    }
    else {
	/* This is a default {} block */
	SensorCall(7730);enterop->op_first = block;
	enterop->op_flags |= OPf_SPECIAL;
	o      ->op_flags |= OPf_SPECIAL;

	o->op_next = (OP *) enterop;
    }

    CHECKOP(enter_opcode, enterop); /* Currently does nothing, since
    				       entergiven and enterwhen both
    				       use ck_null() */

    enterop->op_next = LINKLIST(block);
    block->op_next = enterop->op_other = o;

    {OP * ReplaceReturn674 = o; SensorCall(7732); return ReplaceReturn674;}
}

/* Does this look like a boolean operation? For these purposes
   a boolean operation is:
     - a subroutine call [*]
     - a logical connective
     - a comparison operator
     - a filetest operator, with the exception of -s -M -A -C
     - defined(), exists() or eof()
     - /$re/ or $foo =~ /$re/
   
   [*] possibly surprising
 */
STATIC bool
S_looks_like_bool(pTHX_ const OP *o)
{
SensorCall(7733);    dVAR;

    PERL_ARGS_ASSERT_LOOKS_LIKE_BOOL;

    SensorCall(7740);switch(o->op_type) {
	case OP_OR:
	case OP_DOR:
	    {_Bool  ReplaceReturn673 = looks_like_bool(cLOGOPo->op_first); SensorCall(7734); return ReplaceReturn673;}

	case OP_AND:
	    {_Bool  ReplaceReturn672 = (
	    	looks_like_bool(cLOGOPo->op_first)
	     && looks_like_bool(cLOGOPo->op_first->op_sibling)); SensorCall(7735); return ReplaceReturn672;}

	case OP_NULL:
	case OP_SCALAR:
	    {_Bool  ReplaceReturn671 = (
		o->op_flags & OPf_KIDS
	    && looks_like_bool(cUNOPo->op_first)); SensorCall(7736); return ReplaceReturn671;}

	case OP_ENTERSUB:

	case OP_NOT:	case OP_XOR:

	case OP_EQ:	case OP_NE:	case OP_LT:
	case OP_GT:	case OP_LE:	case OP_GE:

	case OP_I_EQ:	case OP_I_NE:	case OP_I_LT:
	case OP_I_GT:	case OP_I_LE:	case OP_I_GE:

	case OP_SEQ:	case OP_SNE:	case OP_SLT:
	case OP_SGT:	case OP_SLE:	case OP_SGE:
	
	case OP_SMARTMATCH:
	
	case OP_FTRREAD:  case OP_FTRWRITE: case OP_FTREXEC:
	case OP_FTEREAD:  case OP_FTEWRITE: case OP_FTEEXEC:
	case OP_FTIS:     case OP_FTEOWNED: case OP_FTROWNED:
	case OP_FTZERO:   case OP_FTSOCK:   case OP_FTCHR:
	case OP_FTBLK:    case OP_FTFILE:   case OP_FTDIR:
	case OP_FTPIPE:   case OP_FTLINK:   case OP_FTSUID:
	case OP_FTSGID:   case OP_FTSVTX:   case OP_FTTTY:
	case OP_FTTEXT:   case OP_FTBINARY:
	
	case OP_DEFINED: case OP_EXISTS:
	case OP_MATCH:	 case OP_EOF:

	case OP_FLOP:

	    {_Bool  ReplaceReturn670 = TRUE; SensorCall(7737); return ReplaceReturn670;}
	
	case OP_CONST:
	    /* Detect comparisons that have been optimized away */
	    SensorCall(7738);if (cSVOPo->op_sv == &PL_sv_yes
	    ||  cSVOPo->op_sv == &PL_sv_no)
	    
		return TRUE;
	    else
		return FALSE;

	/* FALL THROUGH */
	default:
	    {_Bool  ReplaceReturn669 = FALSE; SensorCall(7739); return ReplaceReturn669;}
    }
SensorCall(7741);}

/*
=for apidoc Am|OP *|newGIVENOP|OP *cond|OP *block|PADOFFSET defsv_off

Constructs, checks, and returns an op tree expressing a C<given> block.
I<cond> supplies the expression that will be locally assigned to a lexical
variable, and I<block> supplies the body of the C<given> construct; they
are consumed by this function and become part of the constructed op tree.
I<defsv_off> is the pad offset of the scalar lexical variable that will
be affected.

=cut
*/

OP *
Perl_newGIVENOP(pTHX_ OP *cond, OP *block, PADOFFSET defsv_off)
{
SensorCall(7742);    dVAR;
    PERL_ARGS_ASSERT_NEWGIVENOP;
    {OP * ReplaceReturn668 = newGIVWHENOP(
    	ref_array_or_hash(cond),
    	block,
	OP_ENTERGIVEN, OP_LEAVEGIVEN,
	defsv_off); SensorCall(7743); return ReplaceReturn668;}
}

/*
=for apidoc Am|OP *|newWHENOP|OP *cond|OP *block

Constructs, checks, and returns an op tree expressing a C<when> block.
I<cond> supplies the test expression, and I<block> supplies the block
that will be executed if the test evaluates to true; they are consumed
by this function and become part of the constructed op tree.  I<cond>
will be interpreted DWIMically, often as a comparison against C<$_>,
and may be null to generate a C<default> block.

=cut
*/

OP *
Perl_newWHENOP(pTHX_ OP *cond, OP *block)
{
    SensorCall(7744);const bool cond_llb = (!cond || looks_like_bool(cond));
    OP *cond_op;

    PERL_ARGS_ASSERT_NEWWHENOP;

    SensorCall(7747);if (cond_llb)
	{/*295*/SensorCall(7745);cond_op = cond;/*296*/}
    else {
	SensorCall(7746);cond_op = newBINOP(OP_SMARTMATCH, OPf_SPECIAL,
		newDEFSVOP(),
		scalar(ref_array_or_hash(cond)));
    }
    
    {OP * ReplaceReturn667 = newGIVWHENOP(cond_op, block, OP_ENTERWHEN, OP_LEAVEWHEN, 0); SensorCall(7748); return ReplaceReturn667;}
}

void
Perl_cv_ckproto_len_flags(pTHX_ const CV *cv, const GV *gv, const char *p,
		    const STRLEN len, const U32 flags)
{
    SensorCall(7749);const char * const cvp = CvPROTO(cv);
    const STRLEN clen = CvPROTOLEN(cv);

    PERL_ARGS_ASSERT_CV_CKPROTO_LEN_FLAGS;

    SensorCall(7759);if (((!p != !cvp) /* One has prototype, one has not.  */
	|| (p && (
		  (flags & SVf_UTF8) == SvUTF8(cv)
		   ? len != clen || memNE(cvp, p, len)
		   : flags & SVf_UTF8
		      ? bytes_cmp_utf8((const U8 *)cvp, clen,
				       (const U8 *)p, len)
		      : bytes_cmp_utf8((const U8 *)p, len,
				       (const U8 *)cvp, clen)
		 )
	   )
        )
	 && ckWARN_d(WARN_PROTOTYPE)) {
	SensorCall(7750);SV* const msg = sv_newmortal();
	SV* name = NULL;

	SensorCall(7751);if (gv)
	    gv_efullname3(name = sv_newmortal(), gv, NULL);
	sv_setpvs(msg, "Prototype mismatch:");
	SensorCall(7753);if (name)
	    {/*211*/SensorCall(7752);Perl_sv_catpvf(aTHX_ msg, " sub %"SVf, SVfARG(name));/*212*/}
	SensorCall(7755);if (SvPOK(cv))
	    {/*213*/SensorCall(7754);Perl_sv_catpvf(aTHX_ msg, " (%"SVf")",
		SVfARG(newSVpvn_flags(cvp,clen, SvUTF8(cv)|SVs_TEMP))
	    );/*214*/}
	else
	    sv_catpvs(msg, ": none");
	sv_catpvs(msg, " vs ");
	SensorCall(7757);if (p)
	    {/*215*/SensorCall(7756);Perl_sv_catpvf(aTHX_ msg, "(%"SVf")", SVfARG(newSVpvn_flags(p, len, flags | SVs_TEMP)));/*216*/}
	else
	    sv_catpvs(msg, "none");
	SensorCall(7758);Perl_warner(aTHX_ packWARN(WARN_PROTOTYPE), "%"SVf, SVfARG(msg));
    }
SensorCall(7760);}

static void const_sv_xsub(pTHX_ CV* cv);

/*

=head1 Optree Manipulation Functions

=for apidoc cv_const_sv

If C<cv> is a constant sub eligible for inlining. returns the constant
value returned by the sub.  Otherwise, returns NULL.

Constant subs can be created with C<newCONSTSUB> or as described in
L<perlsub/"Constant Functions">.

=cut
*/
SV *
Perl_cv_const_sv(pTHX_ const CV *const cv)
{
SensorCall(7761);    PERL_UNUSED_CONTEXT;
    SensorCall(7762);if (!cv)
	return NULL;
    SensorCall(7763);if (!(SvTYPE(cv) == SVt_PVCV || SvTYPE(cv) == SVt_PVFM))
	return NULL;
    {SV * ReplaceReturn666 = CvCONST(cv) ? MUTABLE_SV(CvXSUBANY(cv).any_ptr) : NULL; SensorCall(7764); return ReplaceReturn666;}
}

/* op_const_sv:  examine an optree to determine whether it's in-lineable.
 * Can be called in 3 ways:
 *
 * !cv
 * 	look for a single OP_CONST with attached value: return the value
 *
 * cv && CvCLONE(cv) && !CvCONST(cv)
 *
 * 	examine the clone prototype, and if contains only a single
 * 	OP_CONST referencing a pad const, or a single PADSV referencing
 * 	an outer lexical, return a non-zero value to indicate the CV is
 * 	a candidate for "constizing" at clone time
 *
 * cv && CvCONST(cv)
 *
 *	We have just cloned an anon prototype that was marked as a const
 *	candidate. Try to grab the current value, and in the case of
 *	PADSV, ignore it if it has multiple references. Return the value.
 */

SV *
Perl_op_const_sv(pTHX_ const OP *o, CV *cv)
{
SensorCall(7765);    dVAR;
    SV *sv = NULL;

    SensorCall(7766);if (PL_madskills)
	return NULL;

    SensorCall(7767);if (!o)
	return NULL;

    SensorCall(7769);if (o->op_type == OP_LINESEQ && cLISTOPo->op_first)
	{/*329*/SensorCall(7768);o = cLISTOPo->op_first->op_sibling;/*330*/}

    SensorCall(7794);for (; o; o = o->op_next) {
	SensorCall(7770);const OPCODE type = o->op_type;

	SensorCall(7772);if (sv && o->op_next == o)
	    {/*331*/{SV * ReplaceReturn665 = sv; SensorCall(7771); return ReplaceReturn665;}/*332*/}
	SensorCall(7777);if (o->op_next != o) {
	    SensorCall(7773);if (type == OP_NEXTSTATE
	     || (type == OP_NULL && !(o->op_flags & OPf_KIDS))
	     || type == OP_PUSHMARK)
		{/*333*/SensorCall(7774);continue;/*334*/}
	    SensorCall(7776);if (type == OP_DBSTATE)
		{/*335*/SensorCall(7775);continue;/*336*/}
	}
	SensorCall(7779);if (type == OP_LEAVESUB || type == OP_RETURN)
	    {/*337*/SensorCall(7778);break;/*338*/}
	SensorCall(7780);if (sv)
	    return NULL;
	SensorCall(7793);if (type == OP_CONST && cSVOPo->op_sv)
	    {/*339*/SensorCall(7781);sv = cSVOPo->op_sv;/*340*/}
	else {/*341*/SensorCall(7782);if (cv && type == OP_CONST) {
	    SensorCall(7783);sv = PAD_BASE_SV(CvPADLIST(cv), o->op_targ);
	    SensorCall(7784);if (!sv)
		return NULL;
	}
	else {/*343*/SensorCall(7785);if (cv && type == OP_PADSV) {
	    SensorCall(7786);if (CvCONST(cv)) { /* newly cloned anon */
		SensorCall(7787);sv = PAD_BASE_SV(CvPADLIST(cv), o->op_targ);
		/* the candidate should have 1 ref from this pad and 1 ref
		 * from the parent */
		SensorCall(7788);if (!sv || SvREFCNT(sv) != 2)
		    return NULL;
		SensorCall(7789);sv = newSVsv(sv);
		SvREADONLY_on(sv);
		{SV * ReplaceReturn664 = sv; SensorCall(7790); return ReplaceReturn664;}
	    }
	    else {
		SensorCall(7791);if (PAD_COMPNAME_FLAGS(o->op_targ) & SVf_FAKE)
		    sv = &PL_sv_undef; /* an arbitrary non-null value */
	    }
	}
	else {
	    {SV * ReplaceReturn663 = NULL; SensorCall(7792); return ReplaceReturn663;}
	;/*344*/}/*342*/}}
    }
    {SV * ReplaceReturn662 = sv; SensorCall(7795); return ReplaceReturn662;}
}

#ifdef PERL_MAD
OP *
#else
void
#endif
Perl_newMYSUB(pTHX_ I32 floor, OP *o, OP *proto, OP *attrs, OP *block)
{
SensorCall(7796);
#if 0
    /* This would be the return value, but the return cannot be reached.  */
    OP* pegop = newOP(OP_NULL, 0);
#endif

    PERL_UNUSED_ARG(floor);

    SensorCall(7797);if (o)
	SAVEFREEOP(o);
    SensorCall(7798);if (proto)
	SAVEFREEOP(proto);
    SensorCall(7799);if (attrs)
	SAVEFREEOP(attrs);
    SensorCall(7800);if (block)
	SAVEFREEOP(block);
    SensorCall(7801);Perl_croak(aTHX_ "\"my sub\" not yet implemented");
#ifdef PERL_MAD
    NORETURN_FUNCTION_END;
#endif
SensorCall(7802);}

CV *
Perl_newATTRSUB(pTHX_ I32 floor, OP *o, OP *proto, OP *attrs, OP *block)
{
    {CV * ReplaceReturn661 = newATTRSUB_flags(floor, o, proto, attrs, block, 0); SensorCall(7803); return ReplaceReturn661;}
}

CV *
Perl_newATTRSUB_flags(pTHX_ I32 floor, OP *o, OP *proto, OP *attrs,
			    OP *block, U32 flags)
{
SensorCall(7804);    dVAR;
    GV *gv;
    const char *ps;
    STRLEN ps_len = 0; /* init it to avoid false uninit warning from icc */
    U32 ps_utf8 = 0;
    register CV *cv = NULL;
    SV *const_sv;
    /* If the subroutine has no body, no attributes, and no builtin attributes
       then it's just a sub declaration, and we may be able to get away with
       storing with a placeholder scalar in the symbol table, rather than a
       full GV and CV.  If anything is present then it will take a full CV to
       store it.  */
    const I32 gv_fetch_flags
	= (block || attrs || (CvFLAGS(PL_compcv) & CVf_BUILTIN_ATTRS)
	   || PL_madskills)
	? GV_ADDMULTI : GV_ADDMULTI | GV_NOINIT;
    STRLEN namlen = 0;
    const bool o_is_gv = flags & 1;
    const char * const name =
	 o ? SvPV_const(o_is_gv ? (SV *)o : cSVOPo->op_sv, namlen) : NULL;
    bool has_name;
    bool name_is_utf8 = o && !o_is_gv && SvUTF8(cSVOPo->op_sv);

    SensorCall(7806);if (proto) {
	assert(proto->op_type == OP_CONST);
	SensorCall(7805);ps = SvPV_const(((SVOP*)proto)->op_sv, ps_len);
        ps_utf8 = SvUTF8(((SVOP*)proto)->op_sv);
    }
    else
	ps = NULL;

    SensorCall(7815);if (o_is_gv) {
	SensorCall(7807);gv = (GV*)o;
	o = NULL;
	has_name = TRUE;
    } else {/*239*/SensorCall(7808);if (name) {
	SensorCall(7809);gv = gv_fetchsv(cSVOPo->op_sv, gv_fetch_flags, SVt_PVCV);
	has_name = TRUE;
    } else {/*241*/SensorCall(7810);if (PERLDB_NAMEANON && CopLINE(PL_curcop)) {
	SensorCall(7811);SV * const sv = sv_newmortal();
	Perl_sv_setpvf(aTHX_ sv, "%s[%s:%"IVdf"]",
		       PL_curstash ? "__ANON__" : "__ANON__::__ANON__",
		       CopFILE(PL_curcop), (IV)CopLINE(PL_curcop));
	gv = gv_fetchsv(sv, gv_fetch_flags, SVt_PVCV);
	has_name = TRUE;
    } else {/*243*/SensorCall(7812);if (PL_curstash) {
	SensorCall(7813);gv = gv_fetchpvs("__ANON__", gv_fetch_flags, SVt_PVCV);
	has_name = FALSE;
    } else {
	SensorCall(7814);gv = gv_fetchpvs("__ANON__::__ANON__", gv_fetch_flags, SVt_PVCV);
	has_name = FALSE;
    ;/*244*/}/*242*/}/*240*/}}

    SensorCall(7819);if (!PL_madskills) {
	SensorCall(7816);if (o)
	    SAVEFREEOP(o);
	SensorCall(7817);if (proto)
	    SAVEFREEOP(proto);
	SensorCall(7818);if (attrs)
	    SAVEFREEOP(attrs);
    }

    SensorCall(7827);if (SvTYPE(gv) != SVt_PVGV) {	/* Maybe prototype now, and had at
					   maximum a prototype before. */
	SensorCall(7820);if (SvTYPE(gv) > SVt_NULL) {
	    SensorCall(7821);if (!SvPOK((const SV *)gv)
		&& !(SvIOK((const SV *)gv) && SvIVX((const SV *)gv) == -1))
	    {
		SensorCall(7822);Perl_ck_warner_d(aTHX_ packWARN(WARN_PROTOTYPE), "Runaway prototype");
	    }
	    cv_ckproto_len_flags((const CV *)gv, NULL, ps, ps_len, ps_utf8);
	}
	SensorCall(7824);if (ps) {
	    sv_setpvn(MUTABLE_SV(gv), ps, ps_len);
            SensorCall(7823);if ( ps_utf8 ) SvUTF8_on(MUTABLE_SV(gv));
        }
	else
	    sv_setiv(MUTABLE_SV(gv), -1);

	SvREFCNT_dec(PL_compcv);
	SensorCall(7825);cv = PL_compcv = NULL;
	SensorCall(7826);goto done;
    }

    SensorCall(7828);cv = (!name || GvCVGEN(gv)) ? NULL : GvCV(gv);

    SensorCall(7829);if (!block || !ps || *ps || attrs
	|| (CvFLAGS(PL_compcv) & CVf_BUILTIN_ATTRS)
#ifdef PERL_MAD
	|| block->op_type == OP_NULL
#endif
	)
	const_sv = NULL;
    else
	const_sv = op_const_sv(block, NULL);

    SensorCall(7845);if (cv) {
        SensorCall(7830);const bool exists = CvROOT(cv) || CvXSUB(cv);

        /* if the subroutine doesn't exist and wasn't pre-declared
         * with a prototype, assume it will be AUTOLOADed,
         * skipping the prototype check
         */
        SensorCall(7831);if (exists || SvPOK(cv))
            cv_ckproto_len_flags(cv, gv, ps, ps_len, ps_utf8);
	/* already defined (or promised)? */
	SensorCall(7844);if (exists || GvASSUMECV(gv)) {
	    SensorCall(7832);if ((!block
#ifdef PERL_MAD
		 || block->op_type == OP_NULL
#endif
		 )) {
		SensorCall(7833);if (CvFLAGS(PL_compcv)) {
		    /* might have had built-in attrs applied */
		    SensorCall(7834);const bool pureperl = !CvISXSUB(cv) && CvROOT(cv);
		    SensorCall(7836);if (CvLVALUE(PL_compcv) && ! CvLVALUE(cv) && pureperl
		     && ckWARN(WARN_MISC))
			{/*245*/SensorCall(7835);Perl_warner(aTHX_ packWARN(WARN_MISC), "lvalue attribute ignored after the subroutine has been defined");/*246*/}
		    CvFLAGS(cv) |=
			(CvFLAGS(PL_compcv) & CVf_BUILTIN_ATTRS
			  & ~(CVf_LVALUE * pureperl));
		}
		SensorCall(7838);if (attrs) {/*247*/SensorCall(7837);goto attrs;/*248*/}
		/* just a "sub foo;" when &foo is already defined */
		SAVEFREESV(PL_compcv);
		SensorCall(7839);goto done;
	    }
	    SensorCall(7843);if (block
#ifdef PERL_MAD
		&& block->op_type != OP_NULL
#endif
		) {
		SensorCall(7840);const line_t oldline = CopLINE(PL_curcop);
		SensorCall(7841);if (PL_parser && PL_parser->copline != NOLINE)
			CopLINE_set(PL_curcop, PL_parser->copline);
		report_redefined_cv(cSVOPo->op_sv, cv, &const_sv);
		CopLINE_set(PL_curcop, oldline);
#ifdef PERL_MAD
		if (!PL_minus_c)	/* keep old one around for madskills */
#endif
		    {
			/* (PL_madskills unset in used file.) */
			SvREFCNT_dec(cv);
		    }
		SensorCall(7842);cv = NULL;
	    }
	}
    }
    SensorCall(7854);if (const_sv) {
	SensorCall(7846);HV *stash;
	SvREFCNT_inc_simple_void_NN(const_sv);
	SensorCall(7848);if (cv) {
	    assert(!CvROOT(cv) && !CvCONST(cv));
	    sv_setpvs(MUTABLE_SV(cv), "");  /* prototype is "" */
	    CvXSUBANY(cv).any_ptr = const_sv;
	    CvXSUB(cv) = const_sv_xsub;
	    CvCONST_on(cv);
	    CvISXSUB_on(cv);
	}
	else {
	    GvCV_set(gv, NULL);
	    SensorCall(7847);cv = newCONSTSUB_flags(
		NULL, name, namlen, name_is_utf8 ? SVf_UTF8 : 0,
		const_sv
	    );
	}
	SensorCall(7849);stash =
            (CvGV(cv) && GvSTASH(CvGV(cv)))
                ? GvSTASH(CvGV(cv))
                : CvSTASH(cv)
                    ? CvSTASH(cv)
                    : PL_curstash;
	SensorCall(7850);if (HvENAME_HEK(stash))
            mro_method_changed_in(stash); /* sub Foo::Bar () { 123 } */
	SensorCall(7852);if (PL_madskills)
	    {/*249*/SensorCall(7851);goto install_block;/*250*/}
	op_free(block);
	SvREFCNT_dec(PL_compcv);
	PL_compcv = NULL;
	SensorCall(7853);goto done;
    }
    SensorCall(7865);if (cv) {				/* must reuse cv if autoloaded */
	/* transfer PL_compcv to cv */
	SensorCall(7855);if (block
#ifdef PERL_MAD
                  && block->op_type != OP_NULL
#endif
	) {
	    SensorCall(7856);cv_flags_t existing_builtin_attrs = CvFLAGS(cv) & CVf_BUILTIN_ATTRS;
	    AV *const temp_av = CvPADLIST(cv);
	    CV *const temp_cv = CvOUTSIDE(cv);

	    assert(!CvWEAKOUTSIDE(cv));
	    assert(!CvCVGV_RC(cv));
	    assert(CvGV(cv) == gv);

	    SvPOK_off(cv);
	    CvFLAGS(cv) = CvFLAGS(PL_compcv) | existing_builtin_attrs;
	    CvOUTSIDE(cv) = CvOUTSIDE(PL_compcv);
	    CvOUTSIDE_SEQ(cv) = CvOUTSIDE_SEQ(PL_compcv);
	    CvPADLIST(cv) = CvPADLIST(PL_compcv);
	    CvOUTSIDE(PL_compcv) = temp_cv;
	    CvPADLIST(PL_compcv) = temp_av;

	    SensorCall(7857);if (CvFILE(cv) && CvDYNFILE(cv)) {
		Safefree(CvFILE(cv));
    }
	    CvFILE_set_from_cop(cv, PL_curcop);
	    CvSTASH_set(cv, PL_curstash);

	    /* inner references to PL_compcv must be fixed up ... */
	    pad_fixup_inner_anons(CvPADLIST(cv), PL_compcv, cv);
	    SensorCall(7858);if (PERLDB_INTER)/* Advice debugger on the new sub. */
	      ++PL_sub_generation;
	}
	else {
	    /* Might have had built-in attributes applied -- propagate them. */
	    CvFLAGS(cv) |= (CvFLAGS(PL_compcv) & CVf_BUILTIN_ATTRS);
	}
	/* ... before we throw it away */
	SvREFCNT_dec(PL_compcv);
	PL_compcv = cv;
    }
    else {
	SensorCall(7859);cv = PL_compcv;
	SensorCall(7864);if (name) {
	    GvCV_set(gv, cv);
	    SensorCall(7862);if (PL_madskills) {
		SensorCall(7860);if (strEQ(name, "import")) {
		    PL_formfeed = MUTABLE_SV(cv);
		    /* diag_listed_as: SKIPME */
		    SensorCall(7861);Perl_warner(aTHX_ packWARN(WARN_VOID), "0x%"UVxf"\n", PTR2UV(cv));
		}
	    }
	    GvCVGEN(gv) = 0;
	    SensorCall(7863);if (HvENAME_HEK(GvSTASH(gv)))
		/* sub Foo::bar { (shift)+1 } */
		mro_method_changed_in(GvSTASH(gv));
	}
    }
    SensorCall(7866);if (!CvGV(cv)) {
	CvGV_set(cv, gv);
	CvFILE_set_from_cop(cv, PL_curcop);
	CvSTASH_set(cv, PL_curstash);
    }

    SensorCall(7868);if (ps) {
	sv_setpvn(MUTABLE_SV(cv), ps, ps_len);
        SensorCall(7867);if ( ps_utf8 ) SvUTF8_on(MUTABLE_SV(cv));
    }

    SensorCall(7877);if (PL_parser && PL_parser->error_count) {
	op_free(block);
	SensorCall(7869);block = NULL;
	SensorCall(7876);if (name) {
	    SensorCall(7870);const char *s = strrchr(name, ':');
	    s = s ? s+1 : name;
	    SensorCall(7875);if (strEQ(s, "BEGIN")) {
		SensorCall(7871);const char not_safe[] =
		    "BEGIN not safe after errors--compilation aborted";
		SensorCall(7874);if (PL_in_eval & EVAL_KEEPERR)
		    {/*251*/SensorCall(7872);Perl_croak(aTHX_ not_safe);/*252*/}
		else {
		    /* force display of errors found but not reported */
		    sv_catpv(ERRSV, not_safe);
		    SensorCall(7873);Perl_croak(aTHX_ "%"SVf, SVfARG(ERRSV));
		}
	    }
	}
    }
 install_block:
    SensorCall(7879);if (!block)
	{/*253*/SensorCall(7878);goto attrs;/*254*/}

    /* If we assign an optree to a PVCV, then we've defined a subroutine that
       the debugger could be able to set a breakpoint in, so signal to
       pp_entereval that it should not throw away any saved lines at scope
       exit.  */
       
    PL_breakable_sub_gen++;
    /* This makes sub {}; work as expected.  */
    SensorCall(7882);if (block->op_type == OP_STUB) {
	    SensorCall(7880);OP* const newblock = newSTATEOP(0, NULL, 0);
#ifdef PERL_MAD
	    op_getmad(block,newblock,'B');
#else
	    op_free(block);
#endif
	    block = newblock;
    }
    else {/*255*/SensorCall(7881);block->op_attached = 1;/*256*/}
    CvROOT(cv) = CvLVALUE(cv)
		   ? newUNOP(OP_LEAVESUBLV, 0,
			     op_lvalue(scalarseq(block), OP_LEAVESUBLV))
		   : newUNOP(OP_LEAVESUB, 0, scalarseq(block));
    CvROOT(cv)->op_private |= OPpREFCOUNTED;
    OpREFCNT_set(CvROOT(cv), 1);
    CvSTART(cv) = LINKLIST(CvROOT(cv));
    CvROOT(cv)->op_next = 0;
    CALL_PEEP(CvSTART(cv));
    finalize_optree(CvROOT(cv));

    /* now that optimizer has done its work, adjust pad values */

    pad_tidy(CvCLONE(cv) ? padtidy_SUBCLONE : padtidy_SUB);

    SensorCall(7884);if (CvCLONE(cv)) {
	assert(!CvCONST(cv));
	SensorCall(7883);if (ps && !*ps && op_const_sv(block, cv))
	    CvCONST_on(cv);
    }

  attrs:
    SensorCall(7886);if (attrs) {
	/* Need to do a C<use attributes $stash_of_cv,\&cv,@attrs>. */
	SensorCall(7885);HV *stash = name && GvSTASH(CvGV(cv)) ? GvSTASH(CvGV(cv)) : PL_curstash;
	apply_attrs(stash, MUTABLE_SV(cv), attrs, FALSE);
    }

    SensorCall(7893);if (block && has_name) {
	SensorCall(7887);if (PERLDB_SUBLINE && PL_curstash != PL_debstash) {
	    SensorCall(7888);SV * const tmpstr = sv_newmortal();
	    GV * const db_postponed = gv_fetchpvs("DB::postponed",
						  GV_ADDMULTI, SVt_PVHV);
	    HV *hv;
	    SV * const sv = Perl_newSVpvf(aTHX_ "%s:%ld-%ld",
					  CopFILE(PL_curcop),
					  (long)PL_subline,
					  (long)CopLINE(PL_curcop));
	    gv_efullname3(tmpstr, gv, NULL);
	    (void)hv_store(GvHV(PL_DBsub), SvPVX_const(tmpstr),
		    SvUTF8(tmpstr) ? -(I32)SvCUR(tmpstr) : (I32)SvCUR(tmpstr), sv, 0);
	    hv = GvHVn(db_postponed);
	    SensorCall(7891);if (HvTOTALKEYS(hv) > 0 && hv_exists(hv, SvPVX_const(tmpstr), SvUTF8(tmpstr) ? -(I32)SvCUR(tmpstr) : (I32)SvCUR(tmpstr))) {
		SensorCall(7889);CV * const pcv = GvCV(db_postponed);
		SensorCall(7890);if (pcv) {
		    dSP;
		    PUSHMARK(SP);
		    XPUSHs(tmpstr);
		    PUTBACK;
		    call_sv(MUTABLE_SV(pcv), G_DISCARD);
		}
	    }
	}

	SensorCall(7892);if (name && ! (PL_parser && PL_parser->error_count))
	    process_special_blocks(name, gv, cv);
    }

  done:
    SensorCall(7894);if (PL_parser)
	PL_parser->copline = NOLINE;
    LEAVE_SCOPE(floor);
    {CV * ReplaceReturn660 = cv; SensorCall(7895); return ReplaceReturn660;}
}

STATIC void
S_process_special_blocks(pTHX_ const char *const fullname, GV *const gv,
			 CV *const cv)
{
    SensorCall(7896);const char *const colon = strrchr(fullname,':');
    const char *const name = colon ? colon + 1 : fullname;

    PERL_ARGS_ASSERT_PROCESS_SPECIAL_BLOCKS;

    SensorCall(7921);if (*name == 'B') {
	SensorCall(7897);if (strEQ(name, "BEGIN")) {
	    SensorCall(7898);const I32 oldscope = PL_scopestack_ix;
	    ENTER;
	    SAVECOPFILE(&PL_compiling);
	    SAVECOPLINE(&PL_compiling);
	    SAVEVPTR(PL_curcop);

	    DEBUG_x( dump_sub(gv) );
	    Perl_av_create_and_push(aTHX_ &PL_beginav, MUTABLE_SV(cv));
	    GvCV_set(gv,0);		/* cv has been hijacked */
	    call_list(oldscope, PL_beginav);

	    CopHINTS_set(&PL_compiling, PL_hints);
	    LEAVE;
	}
	else
	    {/*677*/SensorCall(7899);return;/*678*/}
    } else {
	SensorCall(7900);if (*name == 'E') {
	    SensorCall(7901);if strEQ(name, "END") {
		DEBUG_x( dump_sub(gv) );
		SensorCall(7902);Perl_av_create_and_unshift_one(aTHX_ &PL_endav, MUTABLE_SV(cv));
	    } else
		{/*679*/SensorCall(7903);return;/*680*/}
	} else {/*681*/SensorCall(7904);if (*name == 'U') {
	    SensorCall(7905);if (strEQ(name, "UNITCHECK")) {
		/* It's never too late to run a unitcheck block */
		SensorCall(7906);Perl_av_create_and_unshift_one(aTHX_ &PL_unitcheckav, MUTABLE_SV(cv));
	    }
	    else
		{/*683*/SensorCall(7907);return;/*684*/}
	} else {/*685*/SensorCall(7908);if (*name == 'C') {
	    SensorCall(7909);if (strEQ(name, "CHECK")) {
		SensorCall(7910);if (PL_main_start)
		    /* diag_listed_as: Too late to run %s block */
		    {/*687*/SensorCall(7911);Perl_ck_warner(aTHX_ packWARN(WARN_VOID),
				   "Too late to run CHECK block");/*688*/}
		SensorCall(7912);Perl_av_create_and_unshift_one(aTHX_ &PL_checkav, MUTABLE_SV(cv));
	    }
	    else
		{/*689*/SensorCall(7913);return;/*690*/}
	} else {/*691*/SensorCall(7914);if (*name == 'I') {
	    SensorCall(7915);if (strEQ(name, "INIT")) {
		SensorCall(7916);if (PL_main_start)
		    /* diag_listed_as: Too late to run %s block */
		    {/*693*/SensorCall(7917);Perl_ck_warner(aTHX_ packWARN(WARN_VOID),
				   "Too late to run INIT block");/*694*/}
		SensorCall(7918);Perl_av_create_and_push(aTHX_ &PL_initav, MUTABLE_SV(cv));
	    }
	    else
		{/*695*/SensorCall(7919);return;/*696*/}
	} else
	    {/*697*/SensorCall(7920);/*692*/}/*686*/}/*682*/}return;/*698*/}
	DEBUG_x( dump_sub(gv) );
	GvCV_set(gv,0);		/* cv has been hijacked */
    }
SensorCall(7922);}

/*
=for apidoc newCONSTSUB

See L</newCONSTSUB_flags>.

=cut
*/

CV *
Perl_newCONSTSUB(pTHX_ HV *stash, const char *name, SV *sv)
{
    {CV * ReplaceReturn659 = newCONSTSUB_flags(stash, name, name ? strlen(name) : 0, 0, sv); SensorCall(7923); return ReplaceReturn659;}
}

/*
=for apidoc newCONSTSUB_flags

Creates a constant sub equivalent to Perl C<sub FOO () { 123 }> which is
eligible for inlining at compile-time.

Currently, the only useful value for C<flags> is SVf_UTF8.

Passing NULL for SV creates a constant sub equivalent to C<sub BAR () {}>,
which won't be called if used as a destructor, but will suppress the overhead
of a call to C<AUTOLOAD>.  (This form, however, isn't eligible for inlining at
compile time.)

=cut
*/

CV *
Perl_newCONSTSUB_flags(pTHX_ HV *stash, const char *name, STRLEN len,
                             U32 flags, SV *sv)
{
SensorCall(7924);    dVAR;
    CV* cv;
#ifdef USE_ITHREADS
    const char *const file = CopFILE(PL_curcop);
#else
    SV *const temp_sv = CopFILESV(PL_curcop);
    const char *const file = temp_sv ? SvPV_nolen_const(temp_sv) : NULL;
#endif

    ENTER;

    SensorCall(7925);if (IN_PERL_RUNTIME) {
	/* at runtime, it's not safe to manipulate PL_curcop: it may be
	 * an op shared between threads. Use a non-shared COP for our
	 * dirty work */
	 SAVEVPTR(PL_curcop);
	 SAVECOMPILEWARNINGS();
	 PL_compiling.cop_warnings = DUP_WARNINGS(PL_curcop->cop_warnings);
	 PL_curcop = &PL_compiling;
    }
    SAVECOPLINE(PL_curcop);
    CopLINE_set(PL_curcop, PL_parser ? PL_parser->copline : NOLINE);

    SAVEHINTS();
    PL_hints &= ~HINT_BLOCK_SCOPE;

    SensorCall(7926);if (stash) {
	SAVEGENERICSV(PL_curstash);
	SAVECOPSTASH(PL_curcop);
	PL_curstash = (HV *)SvREFCNT_inc_simple_NN(stash);
	CopSTASH_set(PL_curcop,stash);
    }

    /* file becomes the CvFILE. For an XS, it's usually static storage,
       and so doesn't get free()d.  (It's expected to be from the C pre-
       processor __FILE__ directive). But we need a dynamically allocated one,
       and we need it to get freed.  */
    SensorCall(7927);cv = newXS_len_flags(name, len, const_sv_xsub, file ? file : "", "",
			 &sv, XS_DYNAMIC_FILENAME | flags);
    CvXSUBANY(cv).any_ptr = sv;
    CvCONST_on(cv);

#ifdef USE_ITHREADS
    SensorCall(7928);if (stash)
	CopSTASH_free(PL_curcop);
#endif
    LEAVE;

    {CV * ReplaceReturn658 = cv; SensorCall(7929); return ReplaceReturn658;}
}

CV *
Perl_newXS_flags(pTHX_ const char *name, XSUBADDR_t subaddr,
		 const char *const filename, const char *const proto,
		 U32 flags)
{
    PERL_ARGS_ASSERT_NEWXS_FLAGS;
    {CV * ReplaceReturn657 = newXS_len_flags(
       name, name ? strlen(name) : 0, subaddr, filename, proto, NULL, flags
    ); SensorCall(7930); return ReplaceReturn657;}
}

CV *
Perl_newXS_len_flags(pTHX_ const char *name, STRLEN len,
			   XSUBADDR_t subaddr, const char *const filename,
			   const char *const proto, SV **const_svp,
			   U32 flags)
{
    SensorCall(7931);CV *cv;

    PERL_ARGS_ASSERT_NEWXS_LEN_FLAGS;

    {
        GV * const gv = name
			 ? gv_fetchpvn(
				name,len,GV_ADDMULTI|flags,SVt_PVCV
			   )
			 : gv_fetchpv(
                            (PL_curstash ? "__ANON__" : "__ANON__::__ANON__"),
                            GV_ADDMULTI | flags, SVt_PVCV);
    
        SensorCall(7933);if (!subaddr)
            {/*311*/SensorCall(7932);Perl_croak(aTHX_ "panic: no address for '%s' in '%s'", name, filename);/*312*/}
    
        SensorCall(7941);if ((cv = (name ? GvCV(gv) : NULL))) {
            SensorCall(7934);if (GvCVGEN(gv)) {
                /* just a cached method */
                SvREFCNT_dec(cv);
                SensorCall(7935);cv = NULL;
            }
            else {/*313*/SensorCall(7936);if (CvROOT(cv) || CvXSUB(cv) || GvASSUMECV(gv)) {
                /* already defined (or promised) */
                /* Redundant check that allows us to avoid creating an SV
                   most of the time: */
                SensorCall(7937);if (CvCONST(cv) || ckWARN(WARN_REDEFINE)) {
                    SensorCall(7938);const line_t oldline = CopLINE(PL_curcop);
                    SensorCall(7939);if (PL_parser && PL_parser->copline != NOLINE)
                        CopLINE_set(PL_curcop, PL_parser->copline);
                    report_redefined_cv(newSVpvn_flags(
                                         name,len,(flags&SVf_UTF8)|SVs_TEMP
                                        ),
                                        cv, const_svp);
                    CopLINE_set(PL_curcop, oldline);
                }
                SvREFCNT_dec(cv);
                SensorCall(7940);cv = NULL;
            ;/*314*/}}
        }
    
        SensorCall(7945);if (cv)				/* must reuse cv if autoloaded */
            cv_undef(cv);
        else {
            SensorCall(7942);cv = MUTABLE_CV(newSV_type(SVt_PVCV));
            SensorCall(7944);if (name) {
                GvCV_set(gv,cv);
                GvCVGEN(gv) = 0;
                SensorCall(7943);if (HvENAME_HEK(GvSTASH(gv)))
                    mro_method_changed_in(GvSTASH(gv)); /* newXS */
            }
        }
        SensorCall(7946);if (!name)
            CvANON_on(cv);
        CvGV_set(cv, gv);
        SensorCall(7947);(void)gv_fetchfile(filename);
        CvFILE(cv) = (char *)filename; /* NOTE: not copied, as it is expected to be
                                    an external constant string */
        assert(!CvDYNFILE(cv)); /* cv_undef should have turned it off */
        CvISXSUB_on(cv);
        CvXSUB(cv) = subaddr;
    
        SensorCall(7948);if (name)
            process_special_blocks(name, gv, cv);
    }

    SensorCall(7949);if (flags & XS_DYNAMIC_FILENAME) {
	CvFILE(cv) = savepv(filename);
	CvDYNFILE_on(cv);
    }
    sv_setpv(MUTABLE_SV(cv), proto);
    {CV * ReplaceReturn656 = cv; SensorCall(7950); return ReplaceReturn656;}
}

/*
=for apidoc U||newXS

Used by C<xsubpp> to hook up XSUBs as Perl subs.  I<filename> needs to be
static storage, as it is used directly as CvFILE(), without a copy being made.

=cut
*/

CV *
Perl_newXS(pTHX_ const char *name, XSUBADDR_t subaddr, const char *filename)
{
    PERL_ARGS_ASSERT_NEWXS;
    {CV * ReplaceReturn655 = newXS_flags(name, subaddr, filename, NULL, 0); SensorCall(7951); return ReplaceReturn655;}
}

#ifdef PERL_MAD
OP *
#else
void
#endif
Perl_newFORM(pTHX_ I32 floor, OP *o, OP *block)
{
SensorCall(7952);    dVAR;
    register CV *cv;
#ifdef PERL_MAD
    OP* pegop = newOP(OP_NULL, 0);
#endif

    GV * const gv = o
	? gv_fetchsv(cSVOPo->op_sv, GV_ADD, SVt_PVFM)
	: gv_fetchpvs("STDOUT", GV_ADD|GV_NOTQUAL, SVt_PVFM);

    GvMULTI_on(gv);
    SensorCall(7959);if ((cv = GvFORM(gv))) {
	SensorCall(7953);if (ckWARN(WARN_REDEFINE)) {
	    SensorCall(7954);const line_t oldline = CopLINE(PL_curcop);
	    SensorCall(7955);if (PL_parser && PL_parser->copline != NOLINE)
		CopLINE_set(PL_curcop, PL_parser->copline);
	    SensorCall(7958);if (o) {
		SensorCall(7956);Perl_warner(aTHX_ packWARN(WARN_REDEFINE),
			    "Format %"SVf" redefined", SVfARG(cSVOPo->op_sv));
	    } else {
		/* diag_listed_as: Format %s redefined */
		SensorCall(7957);Perl_warner(aTHX_ packWARN(WARN_REDEFINE),
			    "Format STDOUT redefined");
	    }
	    CopLINE_set(PL_curcop, oldline);
	}
	SvREFCNT_dec(cv);
    }
    SensorCall(7960);cv = PL_compcv;
    GvFORM(gv) = cv;
    CvGV_set(cv, gv);
    CvFILE_set_from_cop(cv, PL_curcop);


    pad_tidy(padtidy_FORMAT);
    CvROOT(cv) = newUNOP(OP_LEAVEWRITE, 0, scalarseq(block));
    CvROOT(cv)->op_private |= OPpREFCOUNTED;
    OpREFCNT_set(CvROOT(cv), 1);
    CvSTART(cv) = LINKLIST(CvROOT(cv));
    CvROOT(cv)->op_next = 0;
    CALL_PEEP(CvSTART(cv));
    finalize_optree(CvROOT(cv));
#ifdef PERL_MAD
    op_getmad(o,pegop,'n');
    op_getmad_weak(block, pegop, 'b');
#else
    op_free(o);
#endif
    SensorCall(7961);if (PL_parser)
	PL_parser->copline = NOLINE;
    LEAVE_SCOPE(floor);
#ifdef PERL_MAD
    return pegop;
#endif
}

OP *
Perl_newANONLIST(pTHX_ OP *o)
{
    {OP * ReplaceReturn654 = convert(OP_ANONLIST, OPf_SPECIAL, o); SensorCall(7962); return ReplaceReturn654;}
}

OP *
Perl_newANONHASH(pTHX_ OP *o)
{
    {OP * ReplaceReturn653 = convert(OP_ANONHASH, OPf_SPECIAL, o); SensorCall(7963); return ReplaceReturn653;}
}

OP *
Perl_newANONSUB(pTHX_ I32 floor, OP *proto, OP *block)
{
    {OP * ReplaceReturn652 = newANONATTRSUB(floor, proto, NULL, block); SensorCall(7964); return ReplaceReturn652;}
}

OP *
Perl_newANONATTRSUB(pTHX_ I32 floor, OP *proto, OP *attrs, OP *block)
{
    {OP * ReplaceReturn651 = newUNOP(OP_REFGEN, 0,
	newSVOP(OP_ANONCODE, 0,
		MUTABLE_SV(newATTRSUB(floor, 0, proto, attrs, block)))); SensorCall(7965); return ReplaceReturn651;}
}

OP *
Perl_oopsAV(pTHX_ OP *o)
{
SensorCall(7966);    dVAR;

    PERL_ARGS_ASSERT_OOPSAV;

    SensorCall(7973);switch (o->op_type) {
    case OP_PADSV:
	SensorCall(7967);o->op_type = OP_PADAV;
	o->op_ppaddr = PL_ppaddr[OP_PADAV];
	{OP * ReplaceReturn650 = ref(o, OP_RV2AV); SensorCall(7968); return ReplaceReturn650;}

    case OP_RV2SV:
	SensorCall(7969);o->op_type = OP_RV2AV;
	o->op_ppaddr = PL_ppaddr[OP_RV2AV];
	ref(o, OP_RV2AV);
	SensorCall(7970);break;

    default:
	SensorCall(7971);Perl_ck_warner_d(aTHX_ packWARN(WARN_INTERNAL), "oops: oopsAV");
	SensorCall(7972);break;
    }
    {OP * ReplaceReturn649 = o; SensorCall(7974); return ReplaceReturn649;}
}

OP *
Perl_oopsHV(pTHX_ OP *o)
{
SensorCall(7975);    dVAR;

    PERL_ARGS_ASSERT_OOPSHV;

    SensorCall(7982);switch (o->op_type) {
    case OP_PADSV:
    case OP_PADAV:
	SensorCall(7976);o->op_type = OP_PADHV;
	o->op_ppaddr = PL_ppaddr[OP_PADHV];
	{OP * ReplaceReturn648 = ref(o, OP_RV2HV); SensorCall(7977); return ReplaceReturn648;}

    case OP_RV2SV:
    case OP_RV2AV:
	SensorCall(7978);o->op_type = OP_RV2HV;
	o->op_ppaddr = PL_ppaddr[OP_RV2HV];
	ref(o, OP_RV2HV);
	SensorCall(7979);break;

    default:
	SensorCall(7980);Perl_ck_warner_d(aTHX_ packWARN(WARN_INTERNAL), "oops: oopsHV");
	SensorCall(7981);break;
    }
    {OP * ReplaceReturn647 = o; SensorCall(7983); return ReplaceReturn647;}
}

OP *
Perl_newAVREF(pTHX_ OP *o)
{
SensorCall(7984);    dVAR;

    PERL_ARGS_ASSERT_NEWAVREF;

    SensorCall(7989);if (o->op_type == OP_PADANY) {
	SensorCall(7985);o->op_type = OP_PADAV;
	o->op_ppaddr = PL_ppaddr[OP_PADAV];
	{OP * ReplaceReturn646 = o; SensorCall(7986); return ReplaceReturn646;}
    }
    else {/*257*/SensorCall(7987);if ((o->op_type == OP_RV2AV || o->op_type == OP_PADAV)) {
	SensorCall(7988);Perl_ck_warner_d(aTHX_ packWARN(WARN_DEPRECATED),
		       "Using an array as a reference is deprecated");
    ;/*258*/}}
    {OP * ReplaceReturn645 = newUNOP(OP_RV2AV, 0, scalar(o)); SensorCall(7990); return ReplaceReturn645;}
}

OP *
Perl_newGVREF(pTHX_ I32 type, OP *o)
{
    SensorCall(7991);if (type == OP_MAPSTART || type == OP_GREPSTART || type == OP_SORT)
	return newUNOP(OP_NULL, 0, o);
    {OP * ReplaceReturn644 = ref(newUNOP(OP_RV2GV, OPf_REF, o), type); SensorCall(7992); return ReplaceReturn644;}
}

OP *
Perl_newHVREF(pTHX_ OP *o)
{
SensorCall(7993);    dVAR;

    PERL_ARGS_ASSERT_NEWHVREF;

    SensorCall(7998);if (o->op_type == OP_PADANY) {
	SensorCall(7994);o->op_type = OP_PADHV;
	o->op_ppaddr = PL_ppaddr[OP_PADHV];
	{OP * ReplaceReturn643 = o; SensorCall(7995); return ReplaceReturn643;}
    }
    else {/*269*/SensorCall(7996);if ((o->op_type == OP_RV2HV || o->op_type == OP_PADHV)) {
	SensorCall(7997);Perl_ck_warner_d(aTHX_ packWARN(WARN_DEPRECATED),
		       "Using a hash as a reference is deprecated");
    ;/*270*/}}
    {OP * ReplaceReturn642 = newUNOP(OP_RV2HV, 0, scalar(o)); SensorCall(7999); return ReplaceReturn642;}
}

OP *
Perl_newCVREF(pTHX_ I32 flags, OP *o)
{
    {OP * ReplaceReturn641 = newUNOP(OP_RV2CV, flags, scalar(o)); SensorCall(8000); return ReplaceReturn641;}
}

OP *
Perl_newSVREF(pTHX_ OP *o)
{
SensorCall(8001);    dVAR;

    PERL_ARGS_ASSERT_NEWSVREF;

    SensorCall(8004);if (o->op_type == OP_PADANY) {
	SensorCall(8002);o->op_type = OP_PADSV;
	o->op_ppaddr = PL_ppaddr[OP_PADSV];
	{OP * ReplaceReturn640 = o; SensorCall(8003); return ReplaceReturn640;}
    }
    {OP * ReplaceReturn639 = newUNOP(OP_RV2SV, 0, scalar(o)); SensorCall(8005); return ReplaceReturn639;}
}

/* Check routines. See the comments at the top of this file for details
 * on when these are called */

OP *
Perl_ck_anoncode(pTHX_ OP *o)
{
SensorCall(8006);    PERL_ARGS_ASSERT_CK_ANONCODE;

    cSVOPo->op_targ = pad_add_anon((CV*)cSVOPo->op_sv, o->op_type);
    SensorCall(8007);if (!PL_madskills)
	cSVOPo->op_sv = NULL;
    {OP * ReplaceReturn638 = o; SensorCall(8008); return ReplaceReturn638;}
}

OP *
Perl_ck_bitop(pTHX_ OP *o)
{
SensorCall(8009);    dVAR;

    PERL_ARGS_ASSERT_CK_BITOP;

    o->op_private = (U8)(PL_hints & HINT_INTEGER);
    SensorCall(8013);if (!(o->op_flags & OPf_STACKED) /* Not an assignment */
	    && (o->op_type == OP_BIT_OR
	     || o->op_type == OP_BIT_AND
	     || o->op_type == OP_BIT_XOR))
    {
	SensorCall(8010);const OP * const left = cBINOPo->op_first;
	const OP * const right = left->op_sibling;
	SensorCall(8012);if ((OP_IS_NUMCOMPARE(left->op_type) &&
		(left->op_flags & OPf_PARENS) == 0) ||
	    (OP_IS_NUMCOMPARE(right->op_type) &&
		(right->op_flags & OPf_PARENS) == 0))
	    {/*11*/SensorCall(8011);Perl_ck_warner(aTHX_ packWARN(WARN_PRECEDENCE),
			   "Possible precedence problem on bitwise %c operator",
			   o->op_type == OP_BIT_OR ? '|'
			   : o->op_type == OP_BIT_AND ? '&' : '^'
			   );/*12*/}
    }
    {OP * ReplaceReturn637 = o; SensorCall(8014); return ReplaceReturn637;}
}

PERL_STATIC_INLINE bool
is_dollar_bracket(pTHX_ const OP * const o)
{
    SensorCall(8015);const OP *kid;
    {_Bool  ReplaceReturn636 = o->op_type == OP_RV2SV && o->op_flags & OPf_KIDS
	&& (kid = cUNOPx(o)->op_first)
	&& kid->op_type == OP_GV
	&& strEQ(GvNAME(cGVOPx_gv(kid)), "["); SensorCall(8016); return ReplaceReturn636;}
}

OP *
Perl_ck_cmp(pTHX_ OP *o)
{
    PERL_ARGS_ASSERT_CK_CMP;
    SensorCall(8017);if (ckWARN(WARN_SYNTAX)) {
	SensorCall(8018);const OP *kid = cUNOPo->op_first;
	SensorCall(8020);if (kid && (
		(
		   is_dollar_bracket(aTHX_ kid)
		&& kid->op_sibling && kid->op_sibling->op_type == OP_CONST
		)
	     || (  kid->op_type == OP_CONST
		&& (kid = kid->op_sibling) && is_dollar_bracket(aTHX_ kid))
	   ))
	    {/*13*/SensorCall(8019);Perl_warner(aTHX_ packWARN(WARN_SYNTAX),
			"$[ used in %s (did you mean $] ?)", OP_DESC(o));/*14*/}
    }
    {OP * ReplaceReturn635 = o; SensorCall(8021); return ReplaceReturn635;}
}

OP *
Perl_ck_concat(pTHX_ OP *o)
{
    SensorCall(8022);const OP * const kid = cUNOPo->op_first;

    PERL_ARGS_ASSERT_CK_CONCAT;
    PERL_UNUSED_CONTEXT;

    SensorCall(8023);if (kid->op_type == OP_CONCAT && !(kid->op_private & OPpTARGET_MY) &&
	    !(kUNOP->op_first->op_flags & OPf_MOD))
        o->op_flags |= OPf_STACKED;
    {OP * ReplaceReturn634 = o; SensorCall(8024); return ReplaceReturn634;}
}

OP *
Perl_ck_spair(pTHX_ OP *o)
{
SensorCall(8025);    dVAR;

    PERL_ARGS_ASSERT_CK_SPAIR;

    SensorCall(8031);if (o->op_flags & OPf_KIDS) {
	SensorCall(8026);OP* newop;
	OP* kid;
	const OPCODE type = o->op_type;
	o = modkids(ck_fun(o), type);
	kid = cUNOPo->op_first;
	newop = kUNOP->op_first->op_sibling;
	SensorCall(8030);if (newop) {
	    SensorCall(8027);const OPCODE type = newop->op_type;
	    SensorCall(8029);if (newop->op_sibling || !(PL_opargs[type] & OA_RETSCALAR) ||
		    type == OP_PADAV || type == OP_PADHV ||
		    type == OP_RV2AV || type == OP_RV2HV)
		{/*159*/{OP * ReplaceReturn633 = o; SensorCall(8028); return ReplaceReturn633;}/*160*/}
	}
#ifdef PERL_MAD
	op_getmad(kUNOP->op_first,newop,'K');
#else
	op_free(kUNOP->op_first);
#endif
	kUNOP->op_first = newop;
    }
    SensorCall(8032);o->op_ppaddr = PL_ppaddr[++o->op_type];
    {OP * ReplaceReturn632 = ck_fun(o); SensorCall(8033); return ReplaceReturn632;}
}

OP *
Perl_ck_delete(pTHX_ OP *o)
{
    PERL_ARGS_ASSERT_CK_DELETE;

    SensorCall(8034);o = ck_fun(o);
    o->op_private = 0;
    SensorCall(8044);if (o->op_flags & OPf_KIDS) {
	SensorCall(8035);OP * const kid = cUNOPo->op_first;
	SensorCall(8042);switch (kid->op_type) {
	case OP_ASLICE:
	    SensorCall(8036);o->op_flags |= OPf_SPECIAL;
	    /* FALL THROUGH */
	case OP_HSLICE:
	    SensorCall(8037);o->op_private |= OPpSLICE;
	    SensorCall(8038);break;
	case OP_AELEM:
	    SensorCall(8039);o->op_flags |= OPf_SPECIAL;
	    /* FALL THROUGH */
	case OP_HELEM:
	    SensorCall(8040);break;
	default:
	    SensorCall(8041);Perl_croak(aTHX_ "%s argument is not a HASH or ARRAY element or slice",
		  OP_DESC(o));
	}
	SensorCall(8043);if (kid->op_private & OPpLVAL_INTRO)
	    o->op_private |= OPpLVAL_INTRO;
	op_null(kid);
    }
    {OP * ReplaceReturn631 = o; SensorCall(8045); return ReplaceReturn631;}
}

OP *
Perl_ck_die(pTHX_ OP *o)
{
    PERL_ARGS_ASSERT_CK_DIE;

#ifdef VMS
    if (VMSISH_HUSHED) o->op_private |= OPpHUSH_VMSISH;
#endif
    {OP * ReplaceReturn630 = ck_fun(o); SensorCall(8046); return ReplaceReturn630;}
}

OP *
Perl_ck_eof(pTHX_ OP *o)
{
SensorCall(8047);    dVAR;

    PERL_ARGS_ASSERT_CK_EOF;

    SensorCall(8053);if (o->op_flags & OPf_KIDS) {
	SensorCall(8048);OP *kid;
	SensorCall(8050);if (cLISTOPo->op_first->op_type == OP_STUB) {
	    SensorCall(8049);OP * const newop
		= newUNOP(o->op_type, OPf_SPECIAL, newGVOP(OP_GV, 0, PL_argvgv));
#ifdef PERL_MAD
	    op_getmad(o,newop,'O');
#else
	    op_free(o);
#endif
	    o = newop;
	}
	SensorCall(8051);o = ck_fun(o);
	kid = cLISTOPo->op_first;
	SensorCall(8052);if (kid->op_type == OP_RV2GV)
	    kid->op_private |= OPpALLOW_FAKE;
    }
    {OP * ReplaceReturn629 = o; SensorCall(8054); return ReplaceReturn629;}
}

OP *
Perl_ck_eval(pTHX_ OP *o)
{
SensorCall(8055);    dVAR;

    PERL_ARGS_ASSERT_CK_EVAL;

    PL_hints |= HINT_BLOCK_SCOPE;
    SensorCall(8063);if (o->op_flags & OPf_KIDS) {
	SensorCall(8056);SVOP * const kid = (SVOP*)cUNOPo->op_first;

	SensorCall(8061);if (!kid) {
	    SensorCall(8057);o->op_flags &= ~OPf_KIDS;
	    op_null(o);
	}
	else {/*75*/SensorCall(8058);if (kid->op_type == OP_LINESEQ || kid->op_type == OP_STUB) {
	    SensorCall(8059);LOGOP *enter;
#ifdef PERL_MAD
	    OP* const oldo = o;
#endif

	    cUNOPo->op_first = 0;
#ifndef PERL_MAD
	    op_free(o);
#endif

	    NewOp(1101, enter, 1, LOGOP);
	    enter->op_type = OP_ENTERTRY;
	    enter->op_ppaddr = PL_ppaddr[OP_ENTERTRY];
	    enter->op_private = 0;

	    /* establish postfix order */
	    enter->op_next = (OP*)enter;

	    o = op_prepend_elem(OP_LINESEQ, (OP*)enter, (OP*)kid);
	    o->op_type = OP_LEAVETRY;
	    o->op_ppaddr = PL_ppaddr[OP_LEAVETRY];
	    enter->op_other = o;
	    op_getmad(oldo,o,'O');
	    {OP * ReplaceReturn628 = o; SensorCall(8060); return ReplaceReturn628;}
	}
	else {
	    scalar((OP*)kid);
	    PL_cv_has_eval = 1;
	;/*76*/}}
    }
    else {
	SensorCall(8062);const U8 priv = o->op_private;
#ifdef PERL_MAD
	OP* const oldo = o;
#else
	op_free(o);
#endif
	o = newUNOP(OP_ENTEREVAL, priv <<8, newDEFSVOP());
	op_getmad(oldo,o,'O');
    }
    SensorCall(8064);o->op_targ = (PADOFFSET)PL_hints;
    SensorCall(8065);if (o->op_private & OPpEVAL_BYTES) o->op_targ &= ~HINT_UTF8;
    SensorCall(8067);if ((PL_hints & HINT_LOCALIZE_HH) != 0
     && !(o->op_private & OPpEVAL_COPHH) && GvHV(PL_hintgv)) {
	/* Store a copy of %^H that pp_entereval can pick up. */
	SensorCall(8066);OP *hhop = newSVOP(OP_HINTSEVAL, 0,
			   MUTABLE_SV(hv_copy_hints_hv(GvHV(PL_hintgv))));
	cUNOPo->op_first->op_sibling = hhop;
	o->op_private |= OPpEVAL_HAS_HH;
    }
    SensorCall(8068);if (!(o->op_private & OPpEVAL_BYTES)
	 && FEATURE_UNIEVAL_IS_ENABLED)
	    o->op_private |= OPpEVAL_UNICODE;
    {OP * ReplaceReturn627 = o; SensorCall(8069); return ReplaceReturn627;}
}

OP *
Perl_ck_exit(pTHX_ OP *o)
{
    PERL_ARGS_ASSERT_CK_EXIT;

#ifdef VMS
    HV * const table = GvHV(PL_hintgv);
    if (table) {
       SV * const * const svp = hv_fetchs(table, "vmsish_exit", FALSE);
       if (svp && *svp && SvTRUE(*svp))
           o->op_private |= OPpEXIT_VMSISH;
    }
    if (VMSISH_HUSHED) o->op_private |= OPpHUSH_VMSISH;
#endif
    {OP * ReplaceReturn626 = ck_fun(o); SensorCall(8070); return ReplaceReturn626;}
}

OP *
Perl_ck_exec(pTHX_ OP *o)
{
    PERL_ARGS_ASSERT_CK_EXEC;

    SensorCall(8071);if (o->op_flags & OPf_STACKED) {
        SensorCall(8072);OP *kid;
	o = ck_fun(o);
	kid = cUNOPo->op_first->op_sibling;
	SensorCall(8073);if (kid->op_type == OP_RV2GV)
	    op_null(kid);
    }
    else
	o = listkids(o);
    {OP * ReplaceReturn625 = o; SensorCall(8074); return ReplaceReturn625;}
}

OP *
Perl_ck_exists(pTHX_ OP *o)
{
SensorCall(8075);    dVAR;

    PERL_ARGS_ASSERT_CK_EXISTS;

    o = ck_fun(o);
    SensorCall(8085);if (o->op_flags & OPf_KIDS) {
	SensorCall(8076);OP * const kid = cUNOPo->op_first;
	SensorCall(8084);if (kid->op_type == OP_ENTERSUB) {
	    SensorCall(8077);(void) ref(kid, o->op_type);
	    SensorCall(8079);if (kid->op_type != OP_RV2CV
			&& !(PL_parser && PL_parser->error_count))
		{/*77*/SensorCall(8078);Perl_croak(aTHX_ "%s argument is not a subroutine name",
			    OP_DESC(o));/*78*/}
	    SensorCall(8080);o->op_private |= OPpEXISTS_SUB;
	}
	else {/*79*/SensorCall(8081);if (kid->op_type == OP_AELEM)
	    o->op_flags |= OPf_SPECIAL;
	else {/*81*/SensorCall(8082);if (kid->op_type != OP_HELEM)
	    {/*83*/SensorCall(8083);Perl_croak(aTHX_ "%s argument is not a HASH or ARRAY element or a subroutine",
		        OP_DESC(o));/*84*/}/*82*/}/*80*/}
	op_null(kid);
    }
    {OP * ReplaceReturn624 = o; SensorCall(8086); return ReplaceReturn624;}
}

OP *
Perl_ck_rvconst(pTHX_ register OP *o)
{
SensorCall(8087);    dVAR;
    SVOP * const kid = (SVOP*)cUNOPo->op_first;

    PERL_ARGS_ASSERT_CK_RVCONST;

    o->op_private |= (PL_hints & HINT_STRICT_REFS);
    SensorCall(8089);if (o->op_type == OP_RV2CV)
	{/*135*/SensorCall(8088);o->op_private &= ~1;/*136*/}

    SensorCall(8127);if (kid->op_type == OP_CONST) {
	SensorCall(8090);int iscv;
	GV *gv;
	SV * const kidsv = kid->op_sv;

	/* Is it a constant from cv_const_sv()? */
	SensorCall(8108);if (SvROK(kidsv) && SvREADONLY(kidsv)) {
	    SensorCall(8091);SV * const rsv = SvRV(kidsv);
	    const svtype type = SvTYPE(rsv);
            const char *badtype = NULL;

	    SensorCall(8104);switch (o->op_type) {
	    case OP_RV2SV:
		SensorCall(8092);if (type > SVt_PVMG)
		    {/*137*/SensorCall(8093);badtype = "a SCALAR";/*138*/}
		SensorCall(8094);break;
	    case OP_RV2AV:
		SensorCall(8095);if (type != SVt_PVAV)
		    {/*139*/SensorCall(8096);badtype = "an ARRAY";/*140*/}
		SensorCall(8097);break;
	    case OP_RV2HV:
		SensorCall(8098);if (type != SVt_PVHV)
		    {/*141*/SensorCall(8099);badtype = "a HASH";/*142*/}
		SensorCall(8100);break;
	    case OP_RV2CV:
		SensorCall(8101);if (type != SVt_PVCV)
		    {/*143*/SensorCall(8102);badtype = "a CODE";/*144*/}
		SensorCall(8103);break;
	    }
	    SensorCall(8106);if (badtype)
		{/*145*/SensorCall(8105);Perl_croak(aTHX_ "Constant is not %s reference", badtype);/*146*/}
	    {OP * ReplaceReturn623 = o; SensorCall(8107); return ReplaceReturn623;}
	}
	SensorCall(8121);if ((o->op_private & HINT_STRICT_REFS) && (kid->op_private & OPpCONST_BARE)) {
	    SensorCall(8109);const char *badthing;
	    SensorCall(8118);switch (o->op_type) {
	    case OP_RV2SV:
		SensorCall(8110);badthing = "a SCALAR";
		SensorCall(8111);break;
	    case OP_RV2AV:
		SensorCall(8112);badthing = "an ARRAY";
		SensorCall(8113);break;
	    case OP_RV2HV:
		SensorCall(8114);badthing = "a HASH";
		SensorCall(8115);break;
	    default:
		SensorCall(8116);badthing = NULL;
		SensorCall(8117);break;
	    }
	    SensorCall(8120);if (badthing)
		{/*147*/SensorCall(8119);Perl_croak(aTHX_
			   "Can't use bareword (\"%"SVf"\") as %s ref while \"strict refs\" in use",
			   SVfARG(kidsv), badthing);/*148*/}
	}
	/*
	 * This is a little tricky.  We only want to add the symbol if we
	 * didn't add it in the lexer.  Otherwise we get duplicate strict
	 * warnings.  But if we didn't add it in the lexer, we must at
	 * least pretend like we wanted to add it even if it existed before,
	 * or we get possible typo warnings.  OPpCONST_ENTERED says
	 * whether the lexer already added THIS instance of this symbol.
	 */
	SensorCall(8122);iscv = (o->op_type == OP_RV2CV) * 2;
	SensorCall(8124);do {
	    SensorCall(8123);gv = gv_fetchsv(kidsv,
		iscv | !(kid->op_private & OPpCONST_ENTERED),
		iscv
		    ? SVt_PVCV
		    : o->op_type == OP_RV2SV
			? SVt_PV
			: o->op_type == OP_RV2AV
			    ? SVt_PVAV
			    : o->op_type == OP_RV2HV
				? SVt_PVHV
				: SVt_PVGV);
	} while (!gv && !(kid->op_private & OPpCONST_ENTERED) && !iscv++);
	SensorCall(8126);if (gv) {
	    SensorCall(8125);kid->op_type = OP_GV;
	    SvREFCNT_dec(kid->op_sv);
#ifdef USE_ITHREADS
	    /* XXX hack: dependence on sizeof(PADOP) <= sizeof(SVOP) */
	    kPADOP->op_padix = pad_alloc(OP_GV, SVs_PADTMP);
	    SvREFCNT_dec(PAD_SVl(kPADOP->op_padix));
	    GvIN_PAD_on(gv);
	    PAD_SETSV(kPADOP->op_padix, MUTABLE_SV(SvREFCNT_inc_simple_NN(gv)));
#else
	    kid->op_sv = SvREFCNT_inc_simple_NN(gv);
#endif
	    kid->op_private = 0;
	    kid->op_ppaddr = PL_ppaddr[OP_GV];
	    /* FAKE globs in the symbol table cause weird bugs (#77810) */
	    SvFAKE_off(gv);
	}
    }
    {OP * ReplaceReturn622 = o; SensorCall(8128); return ReplaceReturn622;}
}

OP *
Perl_ck_ftst(pTHX_ OP *o)
{
SensorCall(8129);    dVAR;
    const I32 type = o->op_type;

    PERL_ARGS_ASSERT_CK_FTST;

    SensorCall(8140);if (o->op_flags & OPf_REF) {
	NOOP;
    }
    else {/*85*/SensorCall(8130);if (o->op_flags & OPf_KIDS && cUNOPo->op_first->op_type != OP_STUB) {
	SensorCall(8131);SVOP * const kid = (SVOP*)cUNOPo->op_first;
	const OPCODE kidtype = kid->op_type;

	SensorCall(8134);if (kidtype == OP_CONST && (kid->op_private & OPpCONST_BARE)) {
	    SensorCall(8132);OP * const newop = newGVOP(type, OPf_REF,
		gv_fetchsv(kid->op_sv, GV_ADD, SVt_PVIO));
#ifdef PERL_MAD
	    op_getmad(o,newop,'O');
#else
	    op_free(o);
#endif
	    {OP * ReplaceReturn621 = newop; SensorCall(8133); return ReplaceReturn621;}
	}
	SensorCall(8135);if ((PL_hints & HINT_FILETEST_ACCESS) && OP_IS_FILETEST_ACCESS(o->op_type))
	    o->op_private |= OPpFT_ACCESS;
	SensorCall(8138);if (PL_check[kidtype] == Perl_ck_ftst
	        && kidtype != OP_STAT && kidtype != OP_LSTAT) {
	    SensorCall(8136);o->op_private |= OPpFT_STACKED;
	    kid->op_private |= OPpFT_STACKING;
	    SensorCall(8137);if (kidtype == OP_FTTTY && (
		   !(kid->op_private & OPpFT_STACKED)
		|| kid->op_private & OPpFT_AFTER_t
	       ))
		o->op_private |= OPpFT_AFTER_t;
	}
    }
    else {
#ifdef PERL_MAD
	OP* const oldo = o;
#else
	op_free(o);
#endif
	SensorCall(8139);if (type == OP_FTTTY)
	    o = newGVOP(type, OPf_REF, PL_stdingv);
	else
	    o = newUNOP(type, 0, newDEFSVOP());
	op_getmad(oldo,o,'O');
    ;/*86*/}}
    {OP * ReplaceReturn620 = o; SensorCall(8141); return ReplaceReturn620;}
}

OP *
Perl_ck_fun(pTHX_ OP *o)
{
SensorCall(8142);    dVAR;
    const int type = o->op_type;
    register I32 oa = PL_opargs[type] >> OASHIFT;

    PERL_ARGS_ASSERT_CK_FUN;

    SensorCall(8144);if (o->op_flags & OPf_STACKED) {
	SensorCall(8143);if ((oa & OA_OPTIONAL) && (oa >> 4) && !((oa >> 4) & OA_OPTIONAL))
	    oa &= ~OA_OPTIONAL;
	else
	    return no_fh_allowed(o);
    }

    SensorCall(8224);if (o->op_flags & OPf_KIDS) {
        SensorCall(8145);OP **tokid = &cLISTOPo->op_first;
        register OP *kid = cLISTOPo->op_first;
        OP *sibl;
        I32 numargs = 0;
	bool seen_optional = FALSE;

	SensorCall(8147);if (kid->op_type == OP_PUSHMARK ||
	    (kid->op_type == OP_NULL && kid->op_targ == OP_PUSHMARK))
	{
	    SensorCall(8146);tokid = &kid->op_sibling;
	    kid = kid->op_sibling;
	}
	SensorCall(8155);if (kid && kid->op_type == OP_COREARGS) {
	    bool optional = FALSE;
	    SensorCall(8151);while (oa) {
		SensorCall(8148);numargs++;
		SensorCall(8149);if (oa & OA_OPTIONAL) optional = TRUE;
		SensorCall(8150);oa = oa >> 4;
	    }
	    SensorCall(8153);if (optional) {/*87*/SensorCall(8152);o->op_private |= numargs;/*88*/}
	    {OP * ReplaceReturn619 = o; SensorCall(8154); return ReplaceReturn619;}
	}

	SensorCall(8219);while (oa) {
	    SensorCall(8156);if (oa & OA_OPTIONAL || (oa & 7) == OA_LIST) {
		SensorCall(8157);if (!kid && !seen_optional && PL_opargs[type] & OA_DEFGV)
		    *tokid = kid = newDEFSVOP();
		SensorCall(8158);seen_optional = TRUE;
	    }
	    SensorCall(8160);if (!kid) {/*89*/SensorCall(8159);break;/*90*/}

	    SensorCall(8161);numargs++;
	    sibl = kid->op_sibling;
#ifdef PERL_MAD
	    if (!sibl && kid->op_type == OP_STUB) {
		numargs--;
		break;
	    }
#endif
	    SensorCall(8217);switch (oa & 7) {
	    case OA_SCALAR:
		/* list seen where single (scalar) arg expected? */
		SensorCall(8162);if (numargs == 1 && !(oa >> 4)
		    && kid->op_type == OP_LIST && type != OP_SCALAR)
		{
		    {OP * ReplaceReturn618 = too_many_arguments_pv(o,PL_op_desc[type], 0); SensorCall(8163); return ReplaceReturn618;}
		}
		scalar(kid);
		SensorCall(8164);break;
	    case OA_LIST:
		SensorCall(8165);if (oa < 16) {
		    SensorCall(8166);kid = 0;
		    SensorCall(8167);continue;
		}
		else
		    list(kid);
		SensorCall(8168);break;
	    case OA_AVREF:
		SensorCall(8169);if ((type == OP_PUSH || type == OP_UNSHIFT)
		    && !kid->op_sibling)
		    {/*91*/SensorCall(8170);Perl_ck_warner(aTHX_ packWARN(WARN_SYNTAX),
				   "Useless use of %s with no values",
				   PL_op_desc[type]);/*92*/}

		SensorCall(8172);if (kid->op_type == OP_CONST &&
		    (kid->op_private & OPpCONST_BARE))
		{
		    SensorCall(8171);OP * const newop = newAVREF(newGVOP(OP_GV, 0,
			gv_fetchsv(((SVOP*)kid)->op_sv, GV_ADD, SVt_PVAV) ));
		    Perl_ck_warner_d(aTHX_ packWARN(WARN_DEPRECATED),
				   "Array @%"SVf" missing the @ in argument %"IVdf" of %s()",
				   SVfARG(((SVOP*)kid)->op_sv), (IV)numargs, PL_op_desc[type]);
#ifdef PERL_MAD
		    op_getmad(kid,newop,'K');
#else
		    op_free(kid);
#endif
		    kid = newop;
		    kid->op_sibling = sibl;
		    *tokid = kid;
		}
		else if (kid->op_type == OP_CONST
		      && (  !SvROK(cSVOPx_sv(kid)) 
		         || SvTYPE(SvRV(cSVOPx_sv(kid))) != SVt_PVAV  )
		        )
		    bad_type_pv(numargs, "array", PL_op_desc[type], 0, kid);
		/* Defer checks to run-time if we have a scalar arg */
		SensorCall(8173);if (kid->op_type == OP_RV2AV || kid->op_type == OP_PADAV)
		    op_lvalue(kid, type);
		else scalar(kid);
		SensorCall(8174);break;
	    case OA_HVREF:
		SensorCall(8175);if (kid->op_type == OP_CONST &&
		    (kid->op_private & OPpCONST_BARE))
		{
		    SensorCall(8176);OP * const newop = newHVREF(newGVOP(OP_GV, 0,
			gv_fetchsv(((SVOP*)kid)->op_sv, GV_ADD, SVt_PVHV) ));
		    Perl_ck_warner_d(aTHX_ packWARN(WARN_DEPRECATED),
				   "Hash %%%"SVf" missing the %% in argument %"IVdf" of %s()",
				   SVfARG(((SVOP*)kid)->op_sv), (IV)numargs, PL_op_desc[type]);
#ifdef PERL_MAD
		    op_getmad(kid,newop,'K');
#else
		    op_free(kid);
#endif
		    kid = newop;
		    kid->op_sibling = sibl;
		    *tokid = kid;
		}
		else if (kid->op_type != OP_RV2HV && kid->op_type != OP_PADHV)
		    bad_type_pv(numargs, "hash", PL_op_desc[type], 0, kid);
		op_lvalue(kid, type);
		SensorCall(8177);break;
	    case OA_CVREF:
		{
		    SensorCall(8178);OP * const newop = newUNOP(OP_NULL, 0, kid);
		    kid->op_sibling = 0;
		    LINKLIST(kid);
		    newop->op_next = newop;
		    kid = newop;
		    kid->op_sibling = sibl;
		    *tokid = kid;
		}
		SensorCall(8179);break;
	    case OA_FILEREF:
		SensorCall(8180);if (kid->op_type != OP_GV && kid->op_type != OP_RV2GV) {
		    SensorCall(8181);if (kid->op_type == OP_CONST &&
			(kid->op_private & OPpCONST_BARE))
		    {
			SensorCall(8182);OP * const newop = newGVOP(OP_GV, 0,
			    gv_fetchsv(((SVOP*)kid)->op_sv, GV_ADD, SVt_PVIO));
			SensorCall(8183);if (!(o->op_private & 1) && /* if not unop */
			    kid == cLISTOPo->op_last)
			    cLISTOPo->op_last = newop;
#ifdef PERL_MAD
			op_getmad(kid,newop,'K');
#else
			op_free(kid);
#endif
			SensorCall(8184);kid = newop;
		    }
		    else {/*93*/SensorCall(8185);if (kid->op_type == OP_READLINE) {
			/* neophyte patrol: open(<FH>), close(<FH>) etc. */
			bad_type_pv(numargs, "HANDLE", OP_DESC(o), 0, kid);
		    }
		    else {
			SensorCall(8186);I32 flags = OPf_SPECIAL;
			I32 priv = 0;
			PADOFFSET targ = 0;

			/* is this op a FH constructor? */
			SensorCall(8212);if (is_handle_constructor(o,numargs)) {
                            SensorCall(8187);const char *name = NULL;
			    STRLEN len = 0;
                            U32 name_utf8 = 0;
			    bool want_dollar = TRUE;

			    flags = 0;
			    /* Set a flag to tell rv2gv to vivify
			     * need to "prove" flag does not mean something
			     * else already - NI-S 1999/05/07
			     */
			    priv = OPpDEREF;
			    SensorCall(8207);if (kid->op_type == OP_PADSV) {
				SensorCall(8188);SV *const namesv
				    = PAD_COMPNAME_SV(kid->op_targ);
				name = SvPV_const(namesv, len);
                                name_utf8 = SvUTF8(namesv);
			    }
			    else {/*95*/SensorCall(8189);if (kid->op_type == OP_RV2SV
				     && kUNOP->op_first->op_type == OP_GV)
			    {
				SensorCall(8190);GV * const gv = cGVOPx_gv(kUNOP->op_first);
				name = GvNAME(gv);
				len = GvNAMELEN(gv);
                                name_utf8 = GvNAMEUTF8(gv) ? SVf_UTF8 : 0;
			    }
			    else {/*97*/SensorCall(8191);if (kid->op_type == OP_AELEM
				     || kid->op_type == OP_HELEM)
			    {
				 SensorCall(8192);OP *firstop;
				 OP *op = ((BINOP*)kid)->op_first;
				 name = NULL;
				 SensorCall(8204);if (op) {
				      SensorCall(8193);SV *tmpstr = NULL;
				      const char * const a =
					   kid->op_type == OP_AELEM ?
					   "[]" : "{}";
				      SensorCall(8201);if (((op->op_type == OP_RV2AV) ||
					   (op->op_type == OP_RV2HV)) &&
					  (firstop = ((UNOP*)op)->op_first) &&
					  (firstop->op_type == OP_GV)) {
					   /* packagevar $a[] or $h{} */
					   SensorCall(8194);GV * const gv = cGVOPx_gv(firstop);
					   SensorCall(8196);if (gv)
						{/*99*/SensorCall(8195);tmpstr =
						     Perl_newSVpvf(aTHX_
								   "%s%c...%c",
								   GvNAME(gv),
								   a[0], a[1]);/*100*/}
				      }
				      else {/*101*/SensorCall(8197);if (op->op_type == OP_PADAV
					       || op->op_type == OP_PADHV) {
					   /* lexicalvar $a[] or $h{} */
					   SensorCall(8198);const char * const padname =
						PAD_COMPNAME_PV(op->op_targ);
					   SensorCall(8200);if (padname)
						{/*103*/SensorCall(8199);tmpstr =
						     Perl_newSVpvf(aTHX_
								   "%s%c...%c",
								   padname + 1,
								   a[0], a[1]);/*104*/}
				      ;/*102*/}}
				      SensorCall(8203);if (tmpstr) {
					   SensorCall(8202);name = SvPV_const(tmpstr, len);
                                           name_utf8 = SvUTF8(tmpstr);
					   sv_2mortal(tmpstr);
				      }
				 }
				 SensorCall(8206);if (!name) {
				      SensorCall(8205);name = "__ANONIO__";
				      len = 10;
				      want_dollar = FALSE;
				 }
				 op_lvalue(kid, type);
			    ;/*98*/}/*96*/}}
			    SensorCall(8211);if (name) {
				SensorCall(8208);SV *namesv;
				targ = pad_alloc(OP_RV2GV, SVs_PADTMP);
				namesv = PAD_SVl(targ);
				SvUPGRADE(namesv, SVt_PV);
				SensorCall(8209);if (want_dollar && *name != '$')
				    sv_setpvs(namesv, "$");
				sv_catpvn(namesv, name, len);
                                SensorCall(8210);if ( name_utf8 ) SvUTF8_on(namesv);
			    }
			}
			SensorCall(8213);kid->op_sibling = 0;
			kid = newUNOP(OP_RV2GV, flags, scalar(kid));
			kid->op_targ = targ;
			kid->op_private |= priv;
		    ;/*94*/}}
		    SensorCall(8214);kid->op_sibling = sibl;
		    *tokid = kid;
		}
		scalar(kid);
		SensorCall(8215);break;
	    case OA_SCALARREF:
		op_lvalue(scalar(kid), type);
		SensorCall(8216);break;
	    }
	    SensorCall(8218);oa >>= 4;
	    tokid = &kid->op_sibling;
	    kid = kid->op_sibling;
	}
#ifdef PERL_MAD
	if (kid && kid->op_type != OP_STUB)
	    return too_many_arguments_pv(o,OP_DESC(o), 0);
	o->op_private |= numargs;
#else
	/* FIXME - should the numargs move as for the PERL_MAD case?  */
	SensorCall(8220);o->op_private |= numargs;
	SensorCall(8221);if (kid)
	    return too_many_arguments_pv(o,OP_DESC(o), 0);
#endif
	listkids(o);
    }
    else {/*105*/SensorCall(8222);if (PL_opargs[type] & OA_DEFGV) {
#ifdef PERL_MAD
	OP *newop = newUNOP(type, 0, newDEFSVOP());
	op_getmad(o,newop,'O');
	return newop;
#else
	/* Ordering of these two is important to keep f_map.t passing.  */
	op_free(o);
	{OP * ReplaceReturn617 = newUNOP(type, 0, newDEFSVOP()); SensorCall(8223); return ReplaceReturn617;}
#endif
    ;/*106*/}}

    SensorCall(8228);if (oa) {
	SensorCall(8225);while (oa & OA_OPTIONAL)
	    {/*107*/SensorCall(8226);oa >>= 4;/*108*/}
	SensorCall(8227);if (oa && oa != OA_LIST)
	    return too_few_arguments_pv(o,OP_DESC(o), 0);
    }
    {OP * ReplaceReturn616 = o; SensorCall(8229); return ReplaceReturn616;}
}

OP *
Perl_ck_glob(pTHX_ OP *o)
{
SensorCall(8230);    dVAR;
    GV *gv;
    const bool core = o->op_flags & OPf_SPECIAL;

    PERL_ARGS_ASSERT_CK_GLOB;

    o = ck_fun(o);
    SensorCall(8231);if ((o->op_flags & OPf_KIDS) && !cLISTOPo->op_first->op_sibling)
	op_append_elem(OP_GLOB, o, newDEFSVOP()); /* glob() => glob($_) */

    SensorCall(8234);if (core) gv = NULL;
    else {/*109*/SensorCall(8232);if (!((gv = gv_fetchpvs("glob", GV_NOTQUAL, SVt_PVCV))
	  && GvCVu(gv) && GvIMPORTED_CV(gv)))
    {
	SensorCall(8233);gv = gv_fetchpvs("CORE::GLOBAL::glob", 0, SVt_PVCV);
    ;/*110*/}}

#if !defined(PERL_EXTERNAL_GLOB)
    SensorCall(8236);if (!(gv && GvCVu(gv) && GvIMPORTED_CV(gv))) {
	ENTER;
	SensorCall(8235);Perl_load_module(aTHX_ PERL_LOADMOD_NOIMPORT,
		newSVpvs("File::Glob"), NULL, NULL, NULL);
	LEAVE;
    }
#endif /* !PERL_EXTERNAL_GLOB */

    SensorCall(8239);if (gv && GvCVu(gv) && GvIMPORTED_CV(gv)) {
	/* convert
	 *     glob
	 *       \ null - const(wildcard)
	 * into
	 *     null
	 *       \ enter
	 *            \ list
	 *                 \ mark - glob - rv2cv
	 *                             |        \ gv(CORE::GLOBAL::glob)
	 *                             |
	 *                              \ null - const(wildcard) - const(ix)
	 */
	SensorCall(8237);o->op_flags |= OPf_SPECIAL;
	o->op_targ = pad_alloc(OP_GLOB, SVs_PADTMP);
	op_append_elem(OP_GLOB, o,
		    newSVOP(OP_CONST, 0, newSViv(PL_glob_index++)));
	o = newLISTOP(OP_LIST, 0, o, NULL);
	o = newUNOP(OP_ENTERSUB, OPf_STACKED,
		    op_append_elem(OP_LIST, o,
				scalar(newUNOP(OP_RV2CV, 0,
					       newGVOP(OP_GV, 0, gv)))));
	o = newUNOP(OP_NULL, 0, ck_subr(o));
	o->op_targ = OP_GLOB; /* hint at what it used to be: eg in newWHILEOP */
	{OP * ReplaceReturn615 = o; SensorCall(8238); return ReplaceReturn615;}
    }
    else o->op_flags &= ~OPf_SPECIAL;
    SensorCall(8240);gv = newGVgen("main");
    gv_IOadd(gv);
#ifndef PERL_EXTERNAL_GLOB
    sv_setiv(GvSVn(gv),PL_glob_index++);
#endif
    op_append_elem(OP_GLOB, o, newGVOP(OP_GV, 0, gv));
    scalarkids(o);
    {OP * ReplaceReturn614 = o; SensorCall(8241); return ReplaceReturn614;}
}

OP *
Perl_ck_grep(pTHX_ OP *o)
{
SensorCall(8242);    dVAR;
    LOGOP *gwop = NULL;
    OP *kid;
    const OPCODE type = o->op_type == OP_GREPSTART ? OP_GREPWHILE : OP_MAPWHILE;
    PADOFFSET offset;

    PERL_ARGS_ASSERT_CK_GREP;

    o->op_ppaddr = PL_ppaddr[OP_GREPSTART];
    /* don't allocate gwop here, as we may leak it if PL_parser->error_count > 0 */

    SensorCall(8248);if (o->op_flags & OPf_STACKED) {
	SensorCall(8243);OP* k;
	o = ck_sort(o);
        kid = cUNOPx(cLISTOPo->op_first->op_sibling)->op_first;
	SensorCall(8244);if (kid->op_type != OP_SCOPE && kid->op_type != OP_LEAVE)
	    return no_fh_allowed(o);
	SensorCall(8246);for (k = kid; k; k = k->op_next) {
	    SensorCall(8245);kid = k;
	}
	NewOp(1101, gwop, 1, LOGOP);
	SensorCall(8247);kid->op_next = (OP*)gwop;
	o->op_flags &= ~OPf_STACKED;
    }
    SensorCall(8249);kid = cLISTOPo->op_first->op_sibling;
    SensorCall(8250);if (type == OP_MAPWHILE)
	list(kid);
    else
	scalar(kid);
    SensorCall(8251);o = ck_fun(o);
    SensorCall(8253);if (PL_parser && PL_parser->error_count)
	{/*111*/{OP * ReplaceReturn613 = o; SensorCall(8252); return ReplaceReturn613;}/*112*/}
    SensorCall(8254);kid = cLISTOPo->op_first->op_sibling;
    SensorCall(8256);if (kid->op_type != OP_NULL)
	{/*113*/SensorCall(8255);Perl_croak(aTHX_ "panic: ck_grep, type=%u", (unsigned) kid->op_type);/*114*/}
    SensorCall(8257);kid = kUNOP->op_first;

    SensorCall(8258);if (!gwop)
	NewOp(1101, gwop, 1, LOGOP);
    SensorCall(8259);gwop->op_type = type;
    gwop->op_ppaddr = PL_ppaddr[type];
    gwop->op_first = listkids(o);
    gwop->op_flags |= OPf_KIDS;
    gwop->op_other = LINKLIST(kid);
    kid->op_next = (OP*)gwop;
    offset = pad_findmy_pvs("$_", 0);
    SensorCall(8262);if (offset == NOT_IN_PAD || PAD_COMPNAME_FLAGS_isOUR(offset)) {
	SensorCall(8260);o->op_private = gwop->op_private = 0;
	gwop->op_targ = pad_alloc(type, SVs_PADTMP);
    }
    else {
	SensorCall(8261);o->op_private = gwop->op_private = OPpGREP_LEX;
	gwop->op_targ = o->op_targ = offset;
    }

    SensorCall(8263);kid = cLISTOPo->op_first->op_sibling;
    SensorCall(8264);if (!kid || !kid->op_sibling)
	return too_few_arguments_pv(o,OP_DESC(o), 0);
    SensorCall(8265);for (kid = kid->op_sibling; kid; kid = kid->op_sibling)
	op_lvalue(kid, OP_GREPSTART);

    {OP * ReplaceReturn612 = (OP*)gwop; SensorCall(8266); return ReplaceReturn612;}
}

OP *
Perl_ck_index(pTHX_ OP *o)
{
    PERL_ARGS_ASSERT_CK_INDEX;

    SensorCall(8267);if (o->op_flags & OPf_KIDS) {
	SensorCall(8268);OP *kid = cLISTOPo->op_first->op_sibling;	/* get past pushmark */
	SensorCall(8270);if (kid)
	    {/*115*/SensorCall(8269);kid = kid->op_sibling;/*116*/}			/* get past "big" */
	SensorCall(8272);if (kid && kid->op_type == OP_CONST) {
	    SensorCall(8271);const bool save_taint = PL_tainted;
	    fbm_compile(((SVOP*)kid)->op_sv, 0);
	    PL_tainted = save_taint;
	}
    }
    {OP * ReplaceReturn611 = ck_fun(o); SensorCall(8273); return ReplaceReturn611;}
}

OP *
Perl_ck_lfun(pTHX_ OP *o)
{
    SensorCall(8274);const OPCODE type = o->op_type;

    PERL_ARGS_ASSERT_CK_LFUN;

    {OP * ReplaceReturn610 = modkids(ck_fun(o), type); SensorCall(8275); return ReplaceReturn610;}
}

OP *
Perl_ck_defined(pTHX_ OP *o)		/* 19990527 MJD */
{
    PERL_ARGS_ASSERT_CK_DEFINED;

    SensorCall(8276);if ((o->op_flags & OPf_KIDS)) {
	SensorCall(8277);switch (cUNOPo->op_first->op_type) {
	case OP_RV2AV:
	case OP_PADAV:
	case OP_AASSIGN:		/* Is this a good idea? */
	    SensorCall(8278);Perl_ck_warner_d(aTHX_ packWARN(WARN_DEPRECATED),
			   "defined(@array) is deprecated");
	    Perl_ck_warner_d(aTHX_ packWARN(WARN_DEPRECATED),
			   "\t(Maybe you should just omit the defined()?)\n");
	SensorCall(8279);break;
	case OP_RV2HV:
	case OP_PADHV:
	    SensorCall(8280);Perl_ck_warner_d(aTHX_ packWARN(WARN_DEPRECATED),
			   "defined(%%hash) is deprecated");
	    Perl_ck_warner_d(aTHX_ packWARN(WARN_DEPRECATED),
			   "\t(Maybe you should just omit the defined()?)\n");
	    SensorCall(8281);break;
	default:
	    /* no warning */
	    SensorCall(8282);break;
	}
    }
    {OP * ReplaceReturn609 = ck_rfun(o); SensorCall(8283); return ReplaceReturn609;}
}

OP *
Perl_ck_readline(pTHX_ OP *o)
{
    PERL_ARGS_ASSERT_CK_READLINE;

    SensorCall(8284);if (o->op_flags & OPf_KIDS) {
	 SensorCall(8285);OP *kid = cLISTOPo->op_first;
	 SensorCall(8286);if (kid->op_type == OP_RV2GV) kid->op_private |= OPpALLOW_FAKE;
    }
    else {
	SensorCall(8287);OP * const newop
	    = newUNOP(OP_READLINE, 0, newGVOP(OP_GV, 0, PL_argvgv));
#ifdef PERL_MAD
	op_getmad(o,newop,'O');
#else
	op_free(o);
#endif
	{OP * ReplaceReturn608 = newop; SensorCall(8288); return ReplaceReturn608;}
    }
    {OP * ReplaceReturn607 = o; SensorCall(8289); return ReplaceReturn607;}
}

OP *
Perl_ck_rfun(pTHX_ OP *o)
{
    SensorCall(8290);const OPCODE type = o->op_type;

    PERL_ARGS_ASSERT_CK_RFUN;

    {OP * ReplaceReturn606 = refkids(ck_fun(o), type); SensorCall(8291); return ReplaceReturn606;}
}

OP *
Perl_ck_listiob(pTHX_ OP *o)
{
    SensorCall(8292);register OP *kid;

    PERL_ARGS_ASSERT_CK_LISTIOB;

    kid = cLISTOPo->op_first;
    SensorCall(8294);if (!kid) {
	SensorCall(8293);o = force_list(o);
	kid = cLISTOPo->op_first;
    }
    SensorCall(8296);if (kid->op_type == OP_PUSHMARK)
	{/*129*/SensorCall(8295);kid = kid->op_sibling;/*130*/}
    SensorCall(8301);if (kid && o->op_flags & OPf_STACKED)
	{/*131*/SensorCall(8297);kid = kid->op_sibling;/*132*/}
    else {/*133*/SensorCall(8298);if (kid && !kid->op_sibling) {		/* print HANDLE; */
	SensorCall(8299);if (kid->op_type == OP_CONST && kid->op_private & OPpCONST_BARE) {
	    SensorCall(8300);o->op_flags |= OPf_STACKED;	/* make it a filehandle */
	    kid = newUNOP(OP_RV2GV, OPf_REF, scalar(kid));
	    cLISTOPo->op_first->op_sibling = kid;
	    cLISTOPo->op_last = kid;
	    kid = kid->op_sibling;
	}
    ;/*134*/}}

    SensorCall(8302);if (!kid)
	op_append_elem(o->op_type, o, newDEFSVOP());

    SensorCall(8303);if (o->op_type == OP_PRTF) return modkids(listkids(o), OP_PRTF);
    {OP * ReplaceReturn605 = listkids(o); SensorCall(8304); return ReplaceReturn605;}
}

OP *
Perl_ck_smartmatch(pTHX_ OP *o)
{
SensorCall(8305);    dVAR;
    PERL_ARGS_ASSERT_CK_SMARTMATCH;
    SensorCall(8311);if (0 == (o->op_flags & OPf_SPECIAL)) {
	SensorCall(8306);OP *first  = cBINOPo->op_first;
	OP *second = first->op_sibling;
	
	/* Implicitly take a reference to an array or hash */
	first->op_sibling = NULL;
	first = cBINOPo->op_first = ref_array_or_hash(first);
	second = first->op_sibling = ref_array_or_hash(second);
	
	/* Implicitly take a reference to a regular expression */
	SensorCall(8308);if (first->op_type == OP_MATCH) {
	    SensorCall(8307);first->op_type = OP_QR;
	    first->op_ppaddr = PL_ppaddr[OP_QR];
	}
	SensorCall(8310);if (second->op_type == OP_MATCH) {
	    SensorCall(8309);second->op_type = OP_QR;
	    second->op_ppaddr = PL_ppaddr[OP_QR];
        }
    }
    
    {OP * ReplaceReturn604 = o; SensorCall(8312); return ReplaceReturn604;}
}


OP *
Perl_ck_sassign(pTHX_ OP *o)
{
SensorCall(8313);    dVAR;
    OP * const kid = cLISTOPo->op_first;

    PERL_ARGS_ASSERT_CK_SASSIGN;

    /* has a disposable target? */
    SensorCall(8318);if ((PL_opargs[kid->op_type] & OA_TARGLEX)
	&& !(kid->op_flags & OPf_STACKED)
	/* Cannot steal the second time! */
	&& !(kid->op_private & OPpTARGET_MY)
	/* Keep the full thing for madskills */
	&& !PL_madskills
	)
    {
	SensorCall(8314);OP * const kkid = kid->op_sibling;

	/* Can just relocate the target. */
	SensorCall(8317);if (kkid && kkid->op_type == OP_PADSV
	    && !(kkid->op_private & OPpLVAL_INTRO))
	{
	    SensorCall(8315);kid->op_targ = kkid->op_targ;
	    kkid->op_targ = 0;
	    /* Now we do not need PADSV and SASSIGN. */
	    kid->op_sibling = o->op_sibling;	/* NULL */
	    cLISTOPo->op_first = NULL;
	    op_free(o);
	    op_free(kkid);
	    kid->op_private |= OPpTARGET_MY;	/* Used for context settings */
	    {OP * ReplaceReturn603 = kid; SensorCall(8316); return ReplaceReturn603;}
	}
    }
    SensorCall(8323);if (kid->op_sibling) {
	SensorCall(8319);OP *kkid = kid->op_sibling;
	/* For state variable assignment, kkid is a list op whose op_last
	   is a padsv. */
	SensorCall(8322);if ((kkid->op_type == OP_PADSV ||
	     (kkid->op_type == OP_LIST &&
	      (kkid = cLISTOPx(kkid)->op_last)->op_type == OP_PADSV
	     )
	    )
		&& (kkid->op_private & OPpLVAL_INTRO)
		&& SvPAD_STATE(*av_fetch(PL_comppad_name, kkid->op_targ, FALSE))) {
	    SensorCall(8320);const PADOFFSET target = kkid->op_targ;
	    OP *const other = newOP(OP_PADSV,
				    kkid->op_flags
				    | ((kkid->op_private & ~OPpLVAL_INTRO) << 8));
	    OP *const first = newOP(OP_NULL, 0);
	    OP *const nullop = newCONDOP(0, first, o, other);
	    OP *const condop = first->op_next;
	    /* hijacking PADSTALE for uninitialized state variables */
	    SvPADSTALE_on(PAD_SVl(target));

	    condop->op_type = OP_ONCE;
	    condop->op_ppaddr = PL_ppaddr[OP_ONCE];
	    condop->op_targ = target;
	    other->op_targ = target;

	    /* Because we change the type of the op here, we will skip the
	       assignment binop->op_last = binop->op_first->op_sibling; at the
	       end of Perl_newBINOP(). So need to do it here. */
	    cBINOPo->op_last = cBINOPo->op_first->op_sibling;

	    {OP * ReplaceReturn602 = nullop; SensorCall(8321); return ReplaceReturn602;}
	}
    }
    {OP * ReplaceReturn601 = o; SensorCall(8324); return ReplaceReturn601;}
}

OP *
Perl_ck_match(pTHX_ OP *o)
{
SensorCall(8325);    dVAR;

    PERL_ARGS_ASSERT_CK_MATCH;

    SensorCall(8329);if (o->op_type != OP_QR && PL_compcv) {
	SensorCall(8326);const PADOFFSET offset = pad_findmy_pvs("$_", 0);
	SensorCall(8328);if (offset != NOT_IN_PAD && !(PAD_COMPNAME_FLAGS_isOUR(offset))) {
	    SensorCall(8327);o->op_targ = offset;
	    o->op_private |= OPpTARGET_MY;
	}
    }
    SensorCall(8330);if (o->op_type == OP_MATCH || o->op_type == OP_QR)
	o->op_private |= OPpRUNTIME;
    {OP * ReplaceReturn600 = o; SensorCall(8331); return ReplaceReturn600;}
}

OP *
Perl_ck_method(pTHX_ OP *o)
{
    SensorCall(8332);OP * const kid = cUNOPo->op_first;

    PERL_ARGS_ASSERT_CK_METHOD;

    SensorCall(8340);if (kid->op_type == OP_CONST) {
	SensorCall(8333);SV* sv = kSVOP->op_sv;
	const char * const method = SvPVX_const(sv);
	SensorCall(8339);if (!(strchr(method, ':') || strchr(method, '\''))) {
	    SensorCall(8334);OP *cmop;
	    SensorCall(8336);if (!SvREADONLY(sv) || !SvFAKE(sv)) {
		SensorCall(8335);sv = newSVpvn_share(method, SvUTF8(sv) ? -(I32)SvCUR(sv) : (I32)SvCUR(sv), 0);
	    }
	    else {
		kSVOP->op_sv = NULL;
	    }
	    SensorCall(8337);cmop = newSVOP(OP_METHOD_NAMED, 0, sv);
#ifdef PERL_MAD
	    op_getmad(o,cmop,'O');
#else
	    op_free(o);
#endif
	    {OP * ReplaceReturn599 = cmop; SensorCall(8338); return ReplaceReturn599;}
	}
    }
    {OP * ReplaceReturn598 = o; SensorCall(8341); return ReplaceReturn598;}
}

OP *
Perl_ck_null(pTHX_ OP *o)
{
SensorCall(8342);    PERL_ARGS_ASSERT_CK_NULL;
    PERL_UNUSED_CONTEXT;
    {OP * ReplaceReturn597 = o; SensorCall(8343); return ReplaceReturn597;}
}

OP *
Perl_ck_open(pTHX_ OP *o)
{
SensorCall(8344);    dVAR;
    HV * const table = GvHV(PL_hintgv);

    PERL_ARGS_ASSERT_CK_OPEN;

    SensorCall(8353);if (table) {
	SensorCall(8345);SV **svp = hv_fetchs(table, "open_IN", FALSE);
	SensorCall(8348);if (svp && *svp) {
	    SensorCall(8346);STRLEN len = 0;
	    const char *d = SvPV_const(*svp, len);
	    const I32 mode = mode_from_discipline(d, len);
	    SensorCall(8347);if (mode & O_BINARY)
		o->op_private |= OPpOPEN_IN_RAW;
	    else if (mode & O_TEXT)
		o->op_private |= OPpOPEN_IN_CRLF;
	}

	SensorCall(8349);svp = hv_fetchs(table, "open_OUT", FALSE);
	SensorCall(8352);if (svp && *svp) {
	    SensorCall(8350);STRLEN len = 0;
	    const char *d = SvPV_const(*svp, len);
	    const I32 mode = mode_from_discipline(d, len);
	    SensorCall(8351);if (mode & O_BINARY)
		o->op_private |= OPpOPEN_OUT_RAW;
	    else if (mode & O_TEXT)
		o->op_private |= OPpOPEN_OUT_CRLF;
	}
    }
    SensorCall(8358);if (o->op_type == OP_BACKTICK) {
	SensorCall(8354);if (!(o->op_flags & OPf_KIDS)) {
	    SensorCall(8355);OP * const newop = newUNOP(OP_BACKTICK, 0, newDEFSVOP());
#ifdef PERL_MAD
	    op_getmad(o,newop,'O');
#else
	    op_free(o);
#endif
	    {OP * ReplaceReturn596 = newop; SensorCall(8356); return ReplaceReturn596;}
	}
	{OP * ReplaceReturn595 = o; SensorCall(8357); return ReplaceReturn595;}
    }
    {
	 /* In case of three-arg dup open remove strictness
	  * from the last arg if it is a bareword. */
	 SensorCall(8359);OP * const first = cLISTOPx(o)->op_first; /* The pushmark. */
	 OP * const last  = cLISTOPx(o)->op_last;  /* The bareword. */
	 OP *oa;
	 const char *mode;

	 SensorCall(8360);if ((last->op_type == OP_CONST) &&		/* The bareword. */
	     (last->op_private & OPpCONST_BARE) &&
	     (last->op_private & OPpCONST_STRICT) &&
	     (oa = first->op_sibling) &&		/* The fh. */
	     (oa = oa->op_sibling) &&			/* The mode. */
	     (oa->op_type == OP_CONST) &&
	     SvPOK(((SVOP*)oa)->op_sv) &&
	     (mode = SvPVX_const(((SVOP*)oa)->op_sv)) &&
	     mode[0] == '>' && mode[1] == '&' &&	/* A dup open. */
	     (last == oa->op_sibling))			/* The bareword. */
	      last->op_private &= ~OPpCONST_STRICT;
    }
    {OP * ReplaceReturn594 = ck_fun(o); SensorCall(8361); return ReplaceReturn594;}
}

OP *
Perl_ck_repeat(pTHX_ OP *o)
{
    PERL_ARGS_ASSERT_CK_REPEAT;

    SensorCall(8362);if (cBINOPo->op_first->op_flags & OPf_PARENS) {
	SensorCall(8363);o->op_private |= OPpREPEAT_DOLIST;
	cBINOPo->op_first = force_list(cBINOPo->op_first);
    }
    else
	scalar(o);
    {OP * ReplaceReturn593 = o; SensorCall(8364); return ReplaceReturn593;}
}

OP *
Perl_ck_require(pTHX_ OP *o)
{
SensorCall(8365);    dVAR;
    GV* gv = NULL;

    PERL_ARGS_ASSERT_CK_REQUIRE;

    SensorCall(8376);if (o->op_flags & OPf_KIDS) {	/* Shall we supply missing .pm? */
	SensorCall(8366);SVOP * const kid = (SVOP*)cUNOPo->op_first;

	SensorCall(8375);if (kid->op_type == OP_CONST && (kid->op_private & OPpCONST_BARE)) {
	    SensorCall(8367);SV * const sv = kid->op_sv;
	    U32 was_readonly = SvREADONLY(sv);
	    char *s;
	    STRLEN len;
	    const char *end;

	    SensorCall(8370);if (was_readonly) {
		SensorCall(8368);if (SvFAKE(sv)) {
		    sv_force_normal_flags(sv, 0);
		    assert(!SvREADONLY(sv));
		    SensorCall(8369);was_readonly = 0;
		} else {
		    SvREADONLY_off(sv);
		}
	    }   

	    SensorCall(8371);s = SvPVX(sv);
	    len = SvCUR(sv);
	    end = s + len;
	    SensorCall(8374);for (; s < end; s++) {
		SensorCall(8372);if (*s == ':' && s[1] == ':') {
		    SensorCall(8373);*s = '/';
		    Move(s+2, s+1, end - s - 1, char);
		    --end;
		}
	    }
	    SvEND_set(sv, end);
	    sv_catpvs(sv, ".pm");
	    SvFLAGS(sv) |= was_readonly;
	}
    }

    SensorCall(8380);if (!(o->op_flags & OPf_SPECIAL)) { /* Wasn't written as CORE::require */
	/* handle override, if any */
	SensorCall(8377);gv = gv_fetchpvs("require", GV_NOTQUAL, SVt_PVCV);
	SensorCall(8379);if (!(gv && GvCVu(gv) && GvIMPORTED_CV(gv))) {
	    SensorCall(8378);GV * const * const gvp = (GV**)hv_fetchs(PL_globalstash, "require", FALSE);
	    gv = gvp ? *gvp : NULL;
	}
    }

    SensorCall(8387);if (gv && GvCVu(gv) && GvIMPORTED_CV(gv)) {
	SensorCall(8381);OP *kid, *newop;
	SensorCall(8384);if (o->op_flags & OPf_KIDS) {
	    SensorCall(8382);kid = cUNOPo->op_first;
	    cUNOPo->op_first = NULL;
	}
	else {
	    SensorCall(8383);kid = newDEFSVOP();
	}
#ifndef PERL_MAD
	op_free(o);
#endif
	SensorCall(8385);newop = ck_subr(newUNOP(OP_ENTERSUB, OPf_STACKED,
				op_append_elem(OP_LIST, kid,
					    scalar(newUNOP(OP_RV2CV, 0,
							   newGVOP(OP_GV, 0,
								   gv))))));
	op_getmad(o,newop,'O');
	{OP * ReplaceReturn592 = newop; SensorCall(8386); return ReplaceReturn592;}
    }

    {OP * ReplaceReturn591 = scalar(ck_fun(o)); SensorCall(8388); return ReplaceReturn591;}
}

OP *
Perl_ck_return(pTHX_ OP *o)
{
SensorCall(8389);    dVAR;
    OP *kid;

    PERL_ARGS_ASSERT_CK_RETURN;

    kid = cLISTOPo->op_first->op_sibling;
    SensorCall(8391);if (CvLVALUE(PL_compcv)) {
	SensorCall(8390);for (; kid; kid = kid->op_sibling)
	    op_lvalue(kid, OP_LEAVESUBLV);
    }

    {OP * ReplaceReturn590 = o; SensorCall(8392); return ReplaceReturn590;}
}

OP *
Perl_ck_select(pTHX_ OP *o)
{
SensorCall(8393);    dVAR;
    OP* kid;

    PERL_ARGS_ASSERT_CK_SELECT;

    SensorCall(8398);if (o->op_flags & OPf_KIDS) {
	SensorCall(8394);kid = cLISTOPo->op_first->op_sibling;	/* get past pushmark */
	SensorCall(8397);if (kid && kid->op_sibling) {
	    SensorCall(8395);o->op_type = OP_SSELECT;
	    o->op_ppaddr = PL_ppaddr[OP_SSELECT];
	    o = ck_fun(o);
	    {OP * ReplaceReturn589 = fold_constants(op_integerize(op_std_init(o))); SensorCall(8396); return ReplaceReturn589;}
	}
    }
    SensorCall(8399);o = ck_fun(o);
    kid = cLISTOPo->op_first->op_sibling;    /* get past pushmark */
    SensorCall(8400);if (kid && kid->op_type == OP_RV2GV)
	kid->op_private &= ~HINT_STRICT_REFS;
    {OP * ReplaceReturn588 = o; SensorCall(8401); return ReplaceReturn588;}
}

OP *
Perl_ck_shift(pTHX_ OP *o)
{
SensorCall(8402);    dVAR;
    const I32 type = o->op_type;

    PERL_ARGS_ASSERT_CK_SHIFT;

    SensorCall(8409);if (!(o->op_flags & OPf_KIDS)) {
	SensorCall(8403);OP *argop;

	SensorCall(8406);if (!CvUNIQUE(PL_compcv)) {
	    SensorCall(8404);o->op_flags |= OPf_SPECIAL;
	    {OP * ReplaceReturn587 = o; SensorCall(8405); return ReplaceReturn587;}
	}

	SensorCall(8407);argop = newUNOP(OP_RV2AV, 0, scalar(newGVOP(OP_GV, 0, PL_argvgv)));
#ifdef PERL_MAD
	{
	    OP * const oldo = o;
	    o = newUNOP(type, 0, scalar(argop));
	    op_getmad(oldo,o,'O');
	    return o;
	}
#else
	op_free(o);
	{OP * ReplaceReturn586 = newUNOP(type, 0, scalar(argop)); SensorCall(8408); return ReplaceReturn586;}
#endif
    }
    {OP * ReplaceReturn585 = scalar(ck_fun(o)); SensorCall(8410); return ReplaceReturn585;}
}

OP *
Perl_ck_sort(pTHX_ OP *o)
{
SensorCall(8411);    dVAR;
    OP *firstkid;

    PERL_ARGS_ASSERT_CK_SORT;

    SensorCall(8419);if (o->op_type == OP_SORT && (PL_hints & HINT_LOCALIZE_HH) != 0) {
	SensorCall(8412);HV * const hinthv = GvHV(PL_hintgv);
	SensorCall(8418);if (hinthv) {
	    SensorCall(8413);SV ** const svp = hv_fetchs(hinthv, "sort", FALSE);
	    SensorCall(8417);if (svp) {
		SensorCall(8414);const I32 sorthints = (I32)SvIV(*svp);
		SensorCall(8415);if ((sorthints & HINT_SORT_QUICKSORT) != 0)
		    o->op_private |= OPpSORT_QSORT;
		SensorCall(8416);if ((sorthints & HINT_SORT_STABLE) != 0)
		    o->op_private |= OPpSORT_STABLE;
	    }
	}
    }

    SensorCall(8420);if (o->op_type == OP_SORT && o->op_flags & OPf_STACKED)
	simplify_sort(o);
    SensorCall(8421);firstkid = cLISTOPo->op_first->op_sibling;		/* get past pushmark */
    SensorCall(8442);if (o->op_flags & OPf_STACKED) {			/* may have been cleared */
	SensorCall(8422);OP *k = NULL;
	OP *kid = cUNOPx(firstkid)->op_first;		/* get past null */

	SensorCall(8440);if (kid->op_type == OP_SCOPE || kid->op_type == OP_LEAVE) {
	    LINKLIST(kid);
	    SensorCall(8434);if (kid->op_type == OP_SCOPE) {
		SensorCall(8423);k = kid->op_next;
		kid->op_next = 0;
	    }
	    else {/*149*/SensorCall(8424);if (kid->op_type == OP_LEAVE) {
		SensorCall(8425);if (o->op_type == OP_SORT) {
		    op_null(kid);			/* wipe out leave */
		    SensorCall(8426);kid->op_next = kid;

		    SensorCall(8431);for (k = kLISTOP->op_first->op_next; k; k = k->op_next) {
			SensorCall(8427);if (k->op_next == kid)
			    {/*151*/SensorCall(8428);k->op_next = 0;/*152*/}
			/* don't descend into loops */
			else {/*153*/SensorCall(8429);if (k->op_type == OP_ENTERLOOP
				 || k->op_type == OP_ENTERITER)
			{
			    SensorCall(8430);k = cLOOPx(k)->op_lastop;
			;/*154*/}}
		    }
		}
		else
		    {/*155*/SensorCall(8432);kid->op_next = 0;/*156*/}		/* just disconnect the leave */
		SensorCall(8433);k = kLISTOP->op_first;
	    ;/*150*/}}
	    CALL_PEEP(k);

	    SensorCall(8435);kid = firstkid;
	    SensorCall(8438);if (o->op_type == OP_SORT) {
		/* provide scalar context for comparison function/block */
		SensorCall(8436);kid = scalar(kid);
		kid->op_next = kid;
	    }
	    else
		{/*157*/SensorCall(8437);kid->op_next = k;/*158*/}
	    SensorCall(8439);o->op_flags |= OPf_SPECIAL;
	}

	SensorCall(8441);firstkid = firstkid->op_sibling;
    }

    /* provide list context for arguments */
    SensorCall(8443);if (o->op_type == OP_SORT)
	list(firstkid);

    {OP * ReplaceReturn584 = o; SensorCall(8444); return ReplaceReturn584;}
}

STATIC void
S_simplify_sort(pTHX_ OP *o)
{
SensorCall(8445);    dVAR;
    register OP *kid = cLISTOPo->op_first->op_sibling;	/* get past pushmark */
    OP *k;
    int descending;
    GV *gv;
    const char *gvname;

    PERL_ARGS_ASSERT_SIMPLIFY_SORT;

    SensorCall(8447);if (!(o->op_flags & OPf_STACKED))
	{/*705*/SensorCall(8446);return;/*706*/}
    GvMULTI_on(gv_fetchpvs("a", GV_ADD|GV_NOTQUAL, SVt_PV));
    GvMULTI_on(gv_fetchpvs("b", GV_ADD|GV_NOTQUAL, SVt_PV));
    SensorCall(8448);kid = kUNOP->op_first;				/* get past null */
    SensorCall(8450);if (kid->op_type != OP_SCOPE)
	{/*707*/SensorCall(8449);return;/*708*/}
    SensorCall(8451);kid = kLISTOP->op_last;				/* get past scope */
    SensorCall(8454);switch(kid->op_type) {
	case OP_NCMP:
	case OP_I_NCMP:
	case OP_SCMP:
	    SensorCall(8452);break;
	default:
	    SensorCall(8453);return;
    }
    SensorCall(8455);k = kid;						/* remember this node*/
    SensorCall(8457);if (kBINOP->op_first->op_type != OP_RV2SV)
	{/*709*/SensorCall(8456);return;/*710*/}
    SensorCall(8458);kid = kBINOP->op_first;				/* get past cmp */
    SensorCall(8460);if (kUNOP->op_first->op_type != OP_GV)
	{/*711*/SensorCall(8459);return;/*712*/}
    SensorCall(8461);kid = kUNOP->op_first;				/* get past rv2sv */
    gv = kGVOP_gv;
    SensorCall(8463);if (GvSTASH(gv) != PL_curstash)
	{/*713*/SensorCall(8462);return;/*714*/}
    SensorCall(8464);gvname = GvNAME(gv);
    SensorCall(8469);if (*gvname == 'a' && gvname[1] == '\0')
	{/*715*/SensorCall(8465);descending = 0;/*716*/}
    else {/*717*/SensorCall(8466);if (*gvname == 'b' && gvname[1] == '\0')
	{/*719*/SensorCall(8467);descending = 1;/*720*/}
    else
	{/*721*/SensorCall(8468);/*718*/}return;/*722*/}

    SensorCall(8470);kid = k;						/* back to cmp */
    SensorCall(8472);if (kBINOP->op_last->op_type != OP_RV2SV)
	{/*723*/SensorCall(8471);return;/*724*/}
    SensorCall(8473);kid = kBINOP->op_last;				/* down to 2nd arg */
    SensorCall(8475);if (kUNOP->op_first->op_type != OP_GV)
	{/*725*/SensorCall(8474);return;/*726*/}
    SensorCall(8476);kid = kUNOP->op_first;				/* get past rv2sv */
    gv = kGVOP_gv;
    SensorCall(8478);if (GvSTASH(gv) != PL_curstash)
	{/*727*/SensorCall(8477);return;/*728*/}
    SensorCall(8479);gvname = GvNAME(gv);
    SensorCall(8481);if ( descending
	 ? !(*gvname == 'a' && gvname[1] == '\0')
	 : !(*gvname == 'b' && gvname[1] == '\0'))
	{/*729*/SensorCall(8480);return;/*730*/}
    SensorCall(8482);o->op_flags &= ~(OPf_STACKED | OPf_SPECIAL);
    SensorCall(8483);if (descending)
	o->op_private |= OPpSORT_DESCEND;
    SensorCall(8484);if (k->op_type == OP_NCMP)
	o->op_private |= OPpSORT_NUMERIC;
    SensorCall(8485);if (k->op_type == OP_I_NCMP)
	o->op_private |= OPpSORT_NUMERIC | OPpSORT_INTEGER;
    SensorCall(8486);kid = cLISTOPo->op_first->op_sibling;
    cLISTOPo->op_first->op_sibling = kid->op_sibling; /* bypass old block */
#ifdef PERL_MAD
    op_getmad(kid,o,'S');			      /* then delete it */
#else
    op_free(kid);				      /* then delete it */
#endif
}

OP *
Perl_ck_split(pTHX_ OP *o)
{
SensorCall(8487);    dVAR;
    register OP *kid;

    PERL_ARGS_ASSERT_CK_SPLIT;

    SensorCall(8488);if (o->op_flags & OPf_STACKED)
	return no_fh_allowed(o);

    SensorCall(8489);kid = cLISTOPo->op_first;
    SensorCall(8491);if (kid->op_type != OP_NULL)
	{/*161*/SensorCall(8490);Perl_croak(aTHX_ "panic: ck_split, type=%u", (unsigned) kid->op_type);/*162*/}
    SensorCall(8492);kid = kid->op_sibling;
    op_free(cLISTOPo->op_first);
    SensorCall(8493);if (kid)
	cLISTOPo->op_first = kid;
    else {
	cLISTOPo->op_first = kid = newSVOP(OP_CONST, 0, newSVpvs(" "));
	cLISTOPo->op_last = kid; /* There was only one element previously */
    }

    SensorCall(8497);if (kid->op_type != OP_MATCH || kid->op_flags & OPf_STACKED) {
	SensorCall(8494);OP * const sibl = kid->op_sibling;
	kid->op_sibling = 0;
	kid = pmruntime( newPMOP(OP_MATCH, OPf_SPECIAL), kid, 0);
	SensorCall(8495);if (cLISTOPo->op_first == cLISTOPo->op_last)
	    cLISTOPo->op_last = kid;
	cLISTOPo->op_first = kid;
	SensorCall(8496);kid->op_sibling = sibl;
    }

    SensorCall(8498);kid->op_type = OP_PUSHRE;
    kid->op_ppaddr = PL_ppaddr[OP_PUSHRE];
    scalar(kid);
    SensorCall(8500);if (((PMOP *)kid)->op_pmflags & PMf_GLOBAL) {
      SensorCall(8499);Perl_ck_warner(aTHX_ packWARN(WARN_REGEXP),
		     "Use of /g modifier is meaningless in split");
    }

    SensorCall(8501);if (!kid->op_sibling)
	op_append_elem(OP_SPLIT, o, newDEFSVOP());

    SensorCall(8502);kid = kid->op_sibling;
    scalar(kid);

    SensorCall(8503);if (!kid->op_sibling)
	op_append_elem(OP_SPLIT, o, newSVOP(OP_CONST, 0, newSViv(0)));
    assert(kid->op_sibling);

    SensorCall(8504);kid = kid->op_sibling;
    scalar(kid);

    SensorCall(8505);if (kid->op_sibling)
	return too_many_arguments_pv(o,OP_DESC(o), 0);

    {OP * ReplaceReturn583 = o; SensorCall(8506); return ReplaceReturn583;}
}

OP *
Perl_ck_join(pTHX_ OP *o)
{
    SensorCall(8507);const OP * const kid = cLISTOPo->op_first->op_sibling;

    PERL_ARGS_ASSERT_CK_JOIN;

    SensorCall(8510);if (kid && kid->op_type == OP_MATCH) {
	SensorCall(8508);if (ckWARN(WARN_SYNTAX)) {
            SensorCall(8509);const REGEXP *re = PM_GETRE(kPMOP);
            const SV *msg = re
                    ? newSVpvn_flags( RX_PRECOMP_const(re), RX_PRELEN(re),
                                            SVs_TEMP | ( RX_UTF8(re) ? SVf_UTF8 : 0 ) )
                    : newSVpvs_flags( "STRING", SVs_TEMP );
	    Perl_warner(aTHX_ packWARN(WARN_SYNTAX),
			"/%"SVf"/ should probably be written as \"%"SVf"\"",
			SVfARG(msg), SVfARG(msg));
	}
    }
    {OP * ReplaceReturn582 = ck_fun(o); SensorCall(8511); return ReplaceReturn582;}
}

/*
=for apidoc Am|CV *|rv2cv_op_cv|OP *cvop|U32 flags

Examines an op, which is expected to identify a subroutine at runtime,
and attempts to determine at compile time which subroutine it identifies.
This is normally used during Perl compilation to determine whether
a prototype can be applied to a function call.  I<cvop> is the op
being considered, normally an C<rv2cv> op.  A pointer to the identified
subroutine is returned, if it could be determined statically, and a null
pointer is returned if it was not possible to determine statically.

Currently, the subroutine can be identified statically if the RV that the
C<rv2cv> is to operate on is provided by a suitable C<gv> or C<const> op.
A C<gv> op is suitable if the GV's CV slot is populated.  A C<const> op is
suitable if the constant value must be an RV pointing to a CV.  Details of
this process may change in future versions of Perl.  If the C<rv2cv> op
has the C<OPpENTERSUB_AMPER> flag set then no attempt is made to identify
the subroutine statically: this flag is used to suppress compile-time
magic on a subroutine call, forcing it to use default runtime behaviour.

If I<flags> has the bit C<RV2CVOPCV_MARK_EARLY> set, then the handling
of a GV reference is modified.  If a GV was examined and its CV slot was
found to be empty, then the C<gv> op has the C<OPpEARLY_CV> flag set.
If the op is not optimised away, and the CV slot is later populated with
a subroutine having a prototype, that flag eventually triggers the warning
"called too early to check prototype".

If I<flags> has the bit C<RV2CVOPCV_RETURN_NAME_GV> set, then instead
of returning a pointer to the subroutine it returns a pointer to the
GV giving the most appropriate name for the subroutine in this context.
Normally this is just the C<CvGV> of the subroutine, but for an anonymous
(C<CvANON>) subroutine that is referenced through a GV it will be the
referencing GV.  The resulting C<GV*> is cast to C<CV*> to be returned.
A null pointer is returned as usual if there is no statically-determinable
subroutine.

=cut
*/

CV *
Perl_rv2cv_op_cv(pTHX_ OP *cvop, U32 flags)
{
    SensorCall(8512);OP *rvop;
    CV *cv;
    GV *gv;
    PERL_ARGS_ASSERT_RV2CV_OP_CV;
    SensorCall(8514);if (flags & ~(RV2CVOPCV_MARK_EARLY|RV2CVOPCV_RETURN_NAME_GV))
	{/*471*/SensorCall(8513);Perl_croak(aTHX_ "panic: rv2cv_op_cv bad flags %x", (unsigned)flags);/*472*/}
    SensorCall(8515);if (cvop->op_type != OP_RV2CV)
	return NULL;
    SensorCall(8516);if (cvop->op_private & OPpENTERSUB_AMPER)
	return NULL;
    SensorCall(8517);if (!(cvop->op_flags & OPf_KIDS))
	return NULL;
    SensorCall(8518);rvop = cUNOPx(cvop)->op_first;
    SensorCall(8529);switch (rvop->op_type) {
	case OP_GV: {
	    SensorCall(8519);gv = cGVOPx_gv(rvop);
	    cv = GvCVu(gv);
	    SensorCall(8522);if (!cv) {
		SensorCall(8520);if (flags & RV2CVOPCV_MARK_EARLY)
		    rvop->op_private |= OPpEARLY_CV;
		{CV * ReplaceReturn581 = NULL; SensorCall(8521); return ReplaceReturn581;}
	    }
	} SensorCall(8523);break;
	case OP_CONST: {
	    SensorCall(8524);SV *rv = cSVOPx_sv(rvop);
	    SensorCall(8525);if (!SvROK(rv))
		return NULL;
	    SensorCall(8526);cv = (CV*)SvRV(rv);
	    gv = NULL;
	} SensorCall(8527);break;
	default: {
	    {CV * ReplaceReturn580 = NULL; SensorCall(8528); return ReplaceReturn580;}
	} break;
    }
    SensorCall(8530);if (SvTYPE((SV*)cv) != SVt_PVCV)
	return NULL;
    SensorCall(8534);if (flags & RV2CVOPCV_RETURN_NAME_GV) {
	SensorCall(8531);if (!CvANON(cv) || !gv)
	    gv = CvGV(cv);
	{CV * ReplaceReturn579 = (CV*)gv; SensorCall(8532); return ReplaceReturn579;}
    } else {
	{CV * ReplaceReturn578 = cv; SensorCall(8533); return ReplaceReturn578;}
    }
SensorCall(8535);}

/*
=for apidoc Am|OP *|ck_entersub_args_list|OP *entersubop

Performs the default fixup of the arguments part of an C<entersub>
op tree.  This consists of applying list context to each of the
argument ops.  This is the standard treatment used on a call marked
with C<&>, or a method call, or a call through a subroutine reference,
or any other call where the callee can't be identified at compile time,
or a call where the callee has no prototype.

=cut
*/

OP *
Perl_ck_entersub_args_list(pTHX_ OP *entersubop)
{
    SensorCall(8536);OP *aop;
    PERL_ARGS_ASSERT_CK_ENTERSUB_ARGS_LIST;
    aop = cUNOPx(entersubop)->op_first;
    SensorCall(8538);if (!aop->op_sibling)
	{/*29*/SensorCall(8537);aop = cUNOPx(aop)->op_first;/*30*/}
    SensorCall(8540);for (aop = aop->op_sibling; aop->op_sibling; aop = aop->op_sibling) {
	SensorCall(8539);if (!(PL_madskills && aop->op_type == OP_STUB)) {
	    list(aop);
	    op_lvalue(aop, OP_ENTERSUB);
	}
    }
    {OP * ReplaceReturn577 = entersubop; SensorCall(8541); return ReplaceReturn577;}
}

/*
=for apidoc Am|OP *|ck_entersub_args_proto|OP *entersubop|GV *namegv|SV *protosv

Performs the fixup of the arguments part of an C<entersub> op tree
based on a subroutine prototype.  This makes various modifications to
the argument ops, from applying context up to inserting C<refgen> ops,
and checking the number and syntactic types of arguments, as directed by
the prototype.  This is the standard treatment used on a subroutine call,
not marked with C<&>, where the callee can be identified at compile time
and has a prototype.

I<protosv> supplies the subroutine prototype to be applied to the call.
It may be a normal defined scalar, of which the string value will be used.
Alternatively, for convenience, it may be a subroutine object (a C<CV*>
that has been cast to C<SV*>) which has a prototype.  The prototype
supplied, in whichever form, does not need to match the actual callee
referenced by the op tree.

If the argument ops disagree with the prototype, for example by having
an unacceptable number of arguments, a valid op tree is returned anyway.
The error is reflected in the parser state, normally resulting in a single
exception at the top level of parsing which covers all the compilation
errors that occurred.  In the error message, the callee is referred to
by the name defined by the I<namegv> parameter.

=cut
*/

OP *
Perl_ck_entersub_args_proto(pTHX_ OP *entersubop, GV *namegv, SV *protosv)
{
    SensorCall(8542);STRLEN proto_len;
    const char *proto, *proto_end;
    OP *aop, *prev, *cvop;
    int optional = 0;
    I32 arg = 0;
    I32 contextclass = 0;
    const char *e = NULL;
    PERL_ARGS_ASSERT_CK_ENTERSUB_ARGS_PROTO;
    SensorCall(8544);if (SvTYPE(protosv) == SVt_PVCV ? !SvPOK(protosv) : !SvOK(protosv))
	{/*31*/SensorCall(8543);Perl_croak(aTHX_ "panic: ck_entersub_args_proto CV with no proto, "
		   "flags=%lx", (unsigned long) SvFLAGS(protosv));/*32*/}
    SensorCall(8545);if (SvTYPE(protosv) == SVt_PVCV)
	 proto = CvPROTO(protosv), proto_len = CvPROTOLEN(protosv);
    else proto = SvPV(protosv, proto_len);
    SensorCall(8546);proto_end = proto + proto_len;
    aop = cUNOPx(entersubop)->op_first;
    SensorCall(8548);if (!aop->op_sibling)
	{/*33*/SensorCall(8547);aop = cUNOPx(aop)->op_first;/*34*/}
    SensorCall(8549);prev = aop;
    aop = aop->op_sibling;
    SensorCall(8550);for (cvop = aop; cvop->op_sibling; cvop = cvop->op_sibling) ;
    SensorCall(8636);while (aop != cvop) {
	SensorCall(8551);OP* o3;
	SensorCall(8554);if (PL_madskills && aop->op_type == OP_STUB) {
	    SensorCall(8552);aop = aop->op_sibling;
	    SensorCall(8553);continue;
	}
	SensorCall(8557);if (PL_madskills && aop->op_type == OP_NULL)
	    {/*37*/SensorCall(8555);o3 = ((UNOP*)aop)->op_first;/*38*/}
	else
	    {/*39*/SensorCall(8556);o3 = aop;/*40*/}

	SensorCall(8558);if (proto >= proto_end)
	    return too_many_arguments_sv(entersubop, gv_ename(namegv), 0);

	SensorCall(8634);switch (*proto) {
	    case ';':
		SensorCall(8559);optional = 1;
		proto++;
		SensorCall(8560);continue;
	    case '_':
		/* _ must be at the end */
		SensorCall(8561);if (proto[1] && !strchr(";@%", proto[1]))
		    {/*41*/SensorCall(8562);goto oops;/*42*/}
	    case '$':
		SensorCall(8563);proto++;
		arg++;
		scalar(aop);
		SensorCall(8564);break;
	    case '%':
	    case '@':
		list(aop);
		SensorCall(8565);arg++;
		SensorCall(8566);break;
	    case '&':
		SensorCall(8567);proto++;
		arg++;
		SensorCall(8568);if (o3->op_type != OP_REFGEN && o3->op_type != OP_UNDEF)
		    bad_type_sv(arg,
			    arg == 1 ? "block or sub {}" : "sub {}",
			    gv_ename(namegv), 0, o3);
		SensorCall(8569);break;
	    case '*':
		/* '*' allows any scalar type, including bareword */
		SensorCall(8570);proto++;
		arg++;
		SensorCall(8581);if (o3->op_type == OP_RV2GV)
		    {/*43*/SensorCall(8571);goto wrapref;/*44*/}	/* autoconvert GLOB -> GLOBref */
		else {/*45*/SensorCall(8572);if (o3->op_type == OP_CONST)
		    o3->op_private &= ~OPpCONST_STRICT;
		else {/*47*/SensorCall(8573);if (o3->op_type == OP_ENTERSUB) {
		    /* accidental subroutine, revert to bareword */
		    SensorCall(8574);OP *gvop = ((UNOP*)o3)->op_first;
		    SensorCall(8580);if (gvop && gvop->op_type == OP_NULL) {
			SensorCall(8575);gvop = ((UNOP*)gvop)->op_first;
			SensorCall(8579);if (gvop) {
			    SensorCall(8576);for (; gvop->op_sibling; gvop = gvop->op_sibling)
				;
			    SensorCall(8578);if (gvop &&
				    (gvop->op_private & OPpENTERSUB_NOPAREN) &&
				    (gvop = ((UNOP*)gvop)->op_first) &&
				    gvop->op_type == OP_GV)
			    {
				SensorCall(8577);GV * const gv = cGVOPx_gv(gvop);
				OP * const sibling = aop->op_sibling;
				SV * const n = newSVpvs("");
#ifdef PERL_MAD
				OP * const oldaop = aop;
#else
				op_free(aop);
#endif
				gv_fullname4(n, gv, "", FALSE);
				aop = newSVOP(OP_CONST, 0, n);
				op_getmad(oldaop,aop,'O');
				prev->op_sibling = aop;
				aop->op_sibling = sibling;
			    }
			}
		    }
		;/*48*/}/*46*/}}
		scalar(aop);
		SensorCall(8582);break;
	    case '+':
		SensorCall(8583);proto++;
		arg++;
		SensorCall(8585);if (o3->op_type == OP_RV2AV ||
		    o3->op_type == OP_PADAV ||
		    o3->op_type == OP_RV2HV ||
		    o3->op_type == OP_PADHV
		) {
		    SensorCall(8584);goto wrapref;
		}
		scalar(aop);
		SensorCall(8586);break;
	    case '[': case ']':
		SensorCall(8587);goto oops;
		SensorCall(8588);break;
	    case '\\':
		SensorCall(8589);proto++;
		arg++;
	    again:
		SensorCall(8627);switch (*proto++) {
		    case '[':
			SensorCall(8590);if (contextclass++ == 0) {
			    SensorCall(8591);e = strchr(proto, ']');
			    SensorCall(8593);if (!e || e == proto)
				{/*51*/SensorCall(8592);goto oops;/*52*/}
			}
			else
			    {/*53*/SensorCall(8594);goto oops;/*54*/}
			SensorCall(8595);goto again;
			SensorCall(8596);break;
		    case ']':
			SensorCall(8597);if (contextclass) {
			    SensorCall(8598);const char *p = proto;
			    const char *const end = proto;
			    contextclass = 0;
			    SensorCall(8601);while (*--p != '[')
				/* \[$] accepts any scalar lvalue */
				{/*55*/SensorCall(8599);if (*p == '$'
				 && Perl_op_lvalue_flags(aTHX_
				     scalar(o3),
				     OP_READ, /* not entersub */
				     OP_LVALUE_NO_CROAK
				    )) {/*57*/SensorCall(8600);goto wrapref;/*58*/}/*56*/}
			    bad_type_sv(arg, Perl_form(aTHX_ "one of %.*s",
					(int)(end - p), p),
				    gv_ename(namegv), 0, o3);
			} else
			    {/*59*/SensorCall(8602);goto oops;/*60*/}
			SensorCall(8603);break;
		    case '*':
			SensorCall(8604);if (o3->op_type == OP_RV2GV)
			    {/*61*/SensorCall(8605);goto wrapref;/*62*/}
			SensorCall(8606);if (!contextclass)
			    bad_type_sv(arg, "symbol", gv_ename(namegv), 0, o3);
			SensorCall(8607);break;
		    case '&':
			SensorCall(8608);if (o3->op_type == OP_ENTERSUB)
			    {/*63*/SensorCall(8609);goto wrapref;/*64*/}
			SensorCall(8610);if (!contextclass)
			    bad_type_sv(arg, "subroutine entry", gv_ename(namegv), 0,
				    o3);
			SensorCall(8611);break;
		    case '$':
			SensorCall(8612);if (o3->op_type == OP_RV2SV ||
				o3->op_type == OP_PADSV ||
				o3->op_type == OP_HELEM ||
				o3->op_type == OP_AELEM)
			    {/*65*/SensorCall(8613);goto wrapref;/*66*/}
			SensorCall(8616);if (!contextclass) {
			    /* \$ accepts any scalar lvalue */
			    SensorCall(8614);if (Perl_op_lvalue_flags(aTHX_
				    scalar(o3),
				    OP_READ,  /* not entersub */
				    OP_LVALUE_NO_CROAK
			       )) {/*67*/SensorCall(8615);goto wrapref;/*68*/}
			    bad_type_sv(arg, "scalar", gv_ename(namegv), 0, o3);
			}
			SensorCall(8617);break;
		    case '@':
			SensorCall(8618);if (o3->op_type == OP_RV2AV ||
				o3->op_type == OP_PADAV)
			    {/*69*/SensorCall(8619);goto wrapref;/*70*/}
			SensorCall(8620);if (!contextclass)
			    bad_type_sv(arg, "array", gv_ename(namegv), 0, o3);
			SensorCall(8621);break;
		    case '%':
			SensorCall(8622);if (o3->op_type == OP_RV2HV ||
				o3->op_type == OP_PADHV)
			    {/*71*/SensorCall(8623);goto wrapref;/*72*/}
			SensorCall(8624);if (!contextclass)
			    bad_type_sv(arg, "hash", gv_ename(namegv), 0, o3);
			SensorCall(8625);break;
		    wrapref:
			{
			    OP* const kid = aop;
			    OP* const sib = kid->op_sibling;
			    kid->op_sibling = 0;
			    aop = newUNOP(OP_REFGEN, 0, kid);
			    aop->op_sibling = sib;
			    prev->op_sibling = aop;
			}
			if (contextclass && e) {
			    proto = e + 1;
			    contextclass = 0;
			}
			break;
		    default: SensorCall(8626);goto oops;
		}
		SensorCall(8629);if (contextclass)
		    {/*73*/SensorCall(8628);goto again;/*74*/}
		SensorCall(8630);break;
	    case ' ':
		SensorCall(8631);proto++;
		SensorCall(8632);continue;
	    default:
	    oops: {
                SensorCall(8633);SV* const tmpsv = sv_newmortal();
                gv_efullname3(tmpsv, namegv, NULL);
		Perl_croak(aTHX_ "Malformed prototype for %"SVf": %"SVf,
			SVfARG(tmpsv), SVfARG(protosv));
            }
	}

	op_lvalue(aop, OP_ENTERSUB);
	SensorCall(8635);prev = aop;
	aop = aop->op_sibling;
    }
    SensorCall(8638);if (aop == cvop && *proto == '_') {
	/* generate an access to $_ */
	SensorCall(8637);aop = newDEFSVOP();
	aop->op_sibling = prev->op_sibling;
	prev->op_sibling = aop; /* instead of cvop */
    }
    SensorCall(8639);if (!optional && proto_end > proto &&
	(*proto != '@' && *proto != '%' && *proto != ';' && *proto != '_'))
	return too_few_arguments_sv(entersubop, gv_ename(namegv), 0);
    {OP * ReplaceReturn576 = entersubop; SensorCall(8640); return ReplaceReturn576;}
}

/*
=for apidoc Am|OP *|ck_entersub_args_proto_or_list|OP *entersubop|GV *namegv|SV *protosv

Performs the fixup of the arguments part of an C<entersub> op tree either
based on a subroutine prototype or using default list-context processing.
This is the standard treatment used on a subroutine call, not marked
with C<&>, where the callee can be identified at compile time.

I<protosv> supplies the subroutine prototype to be applied to the call,
or indicates that there is no prototype.  It may be a normal scalar,
in which case if it is defined then the string value will be used
as a prototype, and if it is undefined then there is no prototype.
Alternatively, for convenience, it may be a subroutine object (a C<CV*>
that has been cast to C<SV*>), of which the prototype will be used if it
has one.  The prototype (or lack thereof) supplied, in whichever form,
does not need to match the actual callee referenced by the op tree.

If the argument ops disagree with the prototype, for example by having
an unacceptable number of arguments, a valid op tree is returned anyway.
The error is reflected in the parser state, normally resulting in a single
exception at the top level of parsing which covers all the compilation
errors that occurred.  In the error message, the callee is referred to
by the name defined by the I<namegv> parameter.

=cut
*/

OP *
Perl_ck_entersub_args_proto_or_list(pTHX_ OP *entersubop,
	GV *namegv, SV *protosv)
{
    PERL_ARGS_ASSERT_CK_ENTERSUB_ARGS_PROTO_OR_LIST;
    SensorCall(8641);if (SvTYPE(protosv) == SVt_PVCV ? SvPOK(protosv) : SvOK(protosv))
	return ck_entersub_args_proto(entersubop, namegv, protosv);
    else
	return ck_entersub_args_list(entersubop);
SensorCall(8642);}

OP *
Perl_ck_entersub_args_core(pTHX_ OP *entersubop, GV *namegv, SV *protosv)
{
    SensorCall(8643);int opnum = SvTYPE(protosv) == SVt_PVCV ? 0 : (int)SvUV(protosv);
    OP *aop = cUNOPx(entersubop)->op_first;

    PERL_ARGS_ASSERT_CK_ENTERSUB_ARGS_CORE;

    SensorCall(8672);if (!opnum) {
	SensorCall(8644);OP *cvop;
	SensorCall(8646);if (!aop->op_sibling)
	    {/*17*/SensorCall(8645);aop = cUNOPx(aop)->op_first;/*18*/}
	SensorCall(8647);aop = aop->op_sibling;
	SensorCall(8648);for (cvop = aop; cvop->op_sibling; cvop = cvop->op_sibling) ;
	SensorCall(8651);if (PL_madskills) {/*21*/SensorCall(8649);while (aop != cvop && aop->op_type == OP_STUB) {
	    SensorCall(8650);aop = aop->op_sibling;
	;/*22*/}}
	SensorCall(8652);if (aop != cvop)
	    (void)too_many_arguments_pv(entersubop, GvNAME(namegv), 0);
	
	op_free(entersubop);
	SensorCall(8656);switch(GvNAME(namegv)[2]) {
	case 'F': {OP * ReplaceReturn575 = newSVOP(OP_CONST, 0,
					newSVpv(CopFILE(PL_curcop),0)); SensorCall(8653); return ReplaceReturn575;}
	case 'L': {OP * ReplaceReturn574 = newSVOP(
	                   OP_CONST, 0,
                           Perl_newSVpvf(aTHX_
	                     "%"IVdf, (IV)CopLINE(PL_curcop)
	                   )
	                 ); SensorCall(8654); return ReplaceReturn574;}
	case 'P': {OP * ReplaceReturn573 = newSVOP(OP_CONST, 0,
	                           (PL_curstash
	                             ? newSVhek(HvNAME_HEK(PL_curstash))
	                             : &PL_sv_undef
	                           )
	                        ); SensorCall(8655); return ReplaceReturn573;}
	}
	assert(0);
    }
    else {
	SensorCall(8657);OP *prev, *cvop;
	U32 flags;
#ifdef PERL_MAD
	bool seenarg = FALSE;
#endif
	SensorCall(8659);if (!aop->op_sibling)
	    {/*23*/SensorCall(8658);aop = cUNOPx(aop)->op_first;/*24*/}
	
	SensorCall(8660);prev = aop;
	aop = aop->op_sibling;
	prev->op_sibling = NULL;
	SensorCall(8661);for (cvop = aop;
	     cvop->op_sibling;
	     prev=cvop, cvop = cvop->op_sibling)
#ifdef PERL_MAD
	    if (PL_madskills && cvop->op_sibling
	     && cvop->op_type != OP_STUB) seenarg = TRUE
#endif
	    ;
	SensorCall(8662);prev->op_sibling = NULL;
	flags = OPf_SPECIAL * !(cvop->op_private & OPpENTERSUB_NOPAREN);
	op_free(cvop);
	SensorCall(8663);if (aop == cvop) aop = NULL;
	op_free(entersubop);

	SensorCall(8665);if (opnum == OP_ENTEREVAL
	 && GvNAMELEN(namegv)==9 && strnEQ(GvNAME(namegv), "evalbytes", 9))
	    {/*27*/SensorCall(8664);flags |= OPpEVAL_BYTES <<8;/*28*/}
	
	SensorCall(8671);switch (PL_opargs[opnum] & OA_CLASS_MASK) {
	case OA_UNOP:
	case OA_BASEOP_OR_UNOP:
	case OA_FILESTATOP:
	    {OP * ReplaceReturn572 = aop ? newUNOP(opnum,flags,aop) : newOP(opnum,flags); SensorCall(8666); return ReplaceReturn572;}
	case OA_BASEOP:
	    SensorCall(8667);if (aop) {
#ifdef PERL_MAD
		if (!PL_madskills || seenarg)
#endif
		    SensorCall(8668);(void)too_many_arguments_pv(aop, GvNAME(namegv), 0);
		op_free(aop);
	    }
	    {OP * ReplaceReturn571 = opnum == OP_RUNCV
		? newPVOP(OP_RUNCV,0,NULL)
		: newOP(opnum,0); SensorCall(8669); return ReplaceReturn571;}
	default:
	    {OP * ReplaceReturn570 = convert(opnum,0,aop); SensorCall(8670); return ReplaceReturn570;}
	}
    }
    assert(0);
    {OP * ReplaceReturn569 = entersubop; SensorCall(8673); return ReplaceReturn569;}
}

/*
=for apidoc Am|void|cv_get_call_checker|CV *cv|Perl_call_checker *ckfun_p|SV **ckobj_p

Retrieves the function that will be used to fix up a call to I<cv>.
Specifically, the function is applied to an C<entersub> op tree for a
subroutine call, not marked with C<&>, where the callee can be identified
at compile time as I<cv>.

The C-level function pointer is returned in I<*ckfun_p>, and an SV
argument for it is returned in I<*ckobj_p>.  The function is intended
to be called in this manner:

    entersubop = (*ckfun_p)(aTHX_ entersubop, namegv, (*ckobj_p));

In this call, I<entersubop> is a pointer to the C<entersub> op,
which may be replaced by the check function, and I<namegv> is a GV
supplying the name that should be used by the check function to refer
to the callee of the C<entersub> op if it needs to emit any diagnostics.
It is permitted to apply the check function in non-standard situations,
such as to a call to a different subroutine or to a method call.

By default, the function is
L<Perl_ck_entersub_args_proto_or_list|/ck_entersub_args_proto_or_list>,
and the SV parameter is I<cv> itself.  This implements standard
prototype processing.  It can be changed, for a particular subroutine,
by L</cv_set_call_checker>.

=cut
*/

void
Perl_cv_get_call_checker(pTHX_ CV *cv, Perl_call_checker *ckfun_p, SV **ckobj_p)
{
    SensorCall(8674);MAGIC *callmg;
    PERL_ARGS_ASSERT_CV_GET_CALL_CHECKER;
    callmg = SvMAGICAL((SV*)cv) ? mg_find((SV*)cv, PERL_MAGIC_checkcall) : NULL;
    SensorCall(8677);if (callmg) {
	SensorCall(8675);*ckfun_p = DPTR2FPTR(Perl_call_checker, callmg->mg_ptr);
	*ckobj_p = callmg->mg_obj;
    } else {
	SensorCall(8676);*ckfun_p = Perl_ck_entersub_args_proto_or_list;
	*ckobj_p = (SV*)cv;
    }
SensorCall(8678);}

/*
=for apidoc Am|void|cv_set_call_checker|CV *cv|Perl_call_checker ckfun|SV *ckobj

Sets the function that will be used to fix up a call to I<cv>.
Specifically, the function is applied to an C<entersub> op tree for a
subroutine call, not marked with C<&>, where the callee can be identified
at compile time as I<cv>.

The C-level function pointer is supplied in I<ckfun>, and an SV argument
for it is supplied in I<ckobj>.  The function is intended to be called
in this manner:

    entersubop = ckfun(aTHX_ entersubop, namegv, ckobj);

In this call, I<entersubop> is a pointer to the C<entersub> op,
which may be replaced by the check function, and I<namegv> is a GV
supplying the name that should be used by the check function to refer
to the callee of the C<entersub> op if it needs to emit any diagnostics.
It is permitted to apply the check function in non-standard situations,
such as to a call to a different subroutine or to a method call.

The current setting for a particular CV can be retrieved by
L</cv_get_call_checker>.

=cut
*/

void
Perl_cv_set_call_checker(pTHX_ CV *cv, Perl_call_checker ckfun, SV *ckobj)
{
    PERL_ARGS_ASSERT_CV_SET_CALL_CHECKER;
    SensorCall(8679);if (ckfun == Perl_ck_entersub_args_proto_or_list && ckobj == (SV*)cv) {
	SensorCall(8680);if (SvMAGICAL((SV*)cv))
	    mg_free_type((SV*)cv, PERL_MAGIC_checkcall);
    } else {
	SensorCall(8681);MAGIC *callmg;
	sv_magic((SV*)cv, &PL_sv_undef, PERL_MAGIC_checkcall, NULL, 0);
	callmg = mg_find((SV*)cv, PERL_MAGIC_checkcall);
	SensorCall(8683);if (callmg->mg_flags & MGf_REFCOUNTED) {
	    SvREFCNT_dec(callmg->mg_obj);
	    SensorCall(8682);callmg->mg_flags &= ~MGf_REFCOUNTED;
	}
	SensorCall(8684);callmg->mg_ptr = FPTR2DPTR(char *, ckfun);
	callmg->mg_obj = ckobj;
	SensorCall(8686);if (ckobj != (SV*)cv) {
	    SvREFCNT_inc_simple_void_NN(ckobj);
	    SensorCall(8685);callmg->mg_flags |= MGf_REFCOUNTED;
	}
    }
SensorCall(8687);}

OP *
Perl_ck_subr(pTHX_ OP *o)
{
    SensorCall(8688);OP *aop, *cvop;
    CV *cv;
    GV *namegv;

    PERL_ARGS_ASSERT_CK_SUBR;

    aop = cUNOPx(o)->op_first;
    SensorCall(8690);if (!aop->op_sibling)
	{/*163*/SensorCall(8689);aop = cUNOPx(aop)->op_first;/*164*/}
    SensorCall(8691);aop = aop->op_sibling;
    SensorCall(8692);for (cvop = aop; cvop->op_sibling; cvop = cvop->op_sibling) ;
    SensorCall(8693);cv = rv2cv_op_cv(cvop, RV2CVOPCV_MARK_EARLY);
    namegv = cv ? (GV*)rv2cv_op_cv(cvop, RV2CVOPCV_RETURN_NAME_GV) : NULL;

    o->op_private &= ~1;
    o->op_private |= OPpENTERSUB_HASTARG;
    o->op_private |= (PL_hints & HINT_STRICT_REFS);
    SensorCall(8694);if (PERLDB_SUB && PL_curstash != PL_debstash)
	o->op_private |= OPpENTERSUB_DB;
    SensorCall(8701);if (cvop->op_type == OP_RV2CV) {
	SensorCall(8695);o->op_private |= (cvop->op_private & OPpENTERSUB_AMPER);
	op_null(cvop);
    } else {/*167*/SensorCall(8696);if (cvop->op_type == OP_METHOD || cvop->op_type == OP_METHOD_NAMED) {
	SensorCall(8697);if (aop->op_type == OP_CONST)
	    aop->op_private &= ~OPpCONST_STRICT;
	else {/*169*/SensorCall(8698);if (aop->op_type == OP_LIST) {
	    SensorCall(8699);OP * const sib = ((UNOP*)aop)->op_first->op_sibling;
	    SensorCall(8700);if (sib && sib->op_type == OP_CONST)
		sib->op_private &= ~OPpCONST_STRICT;
	;/*170*/}}
    ;/*168*/}}

    SensorCall(8705);if (!cv) {
	{OP * ReplaceReturn568 = ck_entersub_args_list(o); SensorCall(8702); return ReplaceReturn568;}
    } else {
	SensorCall(8703);Perl_call_checker ckfun;
	SV *ckobj;
	cv_get_call_checker(cv, &ckfun, &ckobj);
	{OP * ReplaceReturn567 = ckfun(aTHX_ o, namegv, ckobj); SensorCall(8704); return ReplaceReturn567;}
    }
SensorCall(8706);}

OP *
Perl_ck_svconst(pTHX_ OP *o)
{
SensorCall(8707);    PERL_ARGS_ASSERT_CK_SVCONST;
    PERL_UNUSED_CONTEXT;
    SvREADONLY_on(cSVOPo->op_sv);
    {OP * ReplaceReturn566 = o; SensorCall(8708); return ReplaceReturn566;}
}

OP *
Perl_ck_chdir(pTHX_ OP *o)
{
    PERL_ARGS_ASSERT_CK_CHDIR;
    SensorCall(8709);if (o->op_flags & OPf_KIDS) {
	SensorCall(8710);SVOP * const kid = (SVOP*)cUNOPo->op_first;

	SensorCall(8712);if (kid && kid->op_type == OP_CONST &&
	    (kid->op_private & OPpCONST_BARE))
	{
	    SensorCall(8711);o->op_flags |= OPf_SPECIAL;
	    kid->op_private &= ~OPpCONST_STRICT;
	}
    }
    {OP * ReplaceReturn565 = ck_fun(o); SensorCall(8713); return ReplaceReturn565;}
}

OP *
Perl_ck_trunc(pTHX_ OP *o)
{
    PERL_ARGS_ASSERT_CK_TRUNC;

    SensorCall(8714);if (o->op_flags & OPf_KIDS) {
	SensorCall(8715);SVOP *kid = (SVOP*)cUNOPo->op_first;

	SensorCall(8717);if (kid->op_type == OP_NULL)
	    {/*175*/SensorCall(8716);kid = (SVOP*)kid->op_sibling;/*176*/}
	SensorCall(8719);if (kid && kid->op_type == OP_CONST &&
	    (kid->op_private & OPpCONST_BARE))
	{
	    SensorCall(8718);o->op_flags |= OPf_SPECIAL;
	    kid->op_private &= ~OPpCONST_STRICT;
	}
    }
    {OP * ReplaceReturn564 = ck_fun(o); SensorCall(8720); return ReplaceReturn564;}
}

OP *
Perl_ck_substr(pTHX_ OP *o)
{
    PERL_ARGS_ASSERT_CK_SUBSTR;

    SensorCall(8721);o = ck_fun(o);
    SensorCall(8726);if ((o->op_flags & OPf_KIDS) && (o->op_private == 4)) {
	SensorCall(8722);OP *kid = cLISTOPo->op_first;

	SensorCall(8724);if (kid->op_type == OP_NULL)
	    {/*171*/SensorCall(8723);kid = kid->op_sibling;/*172*/}
	SensorCall(8725);if (kid)
	    kid->op_flags |= OPf_MOD;

    }
    {OP * ReplaceReturn563 = o; SensorCall(8727); return ReplaceReturn563;}
}

OP *
Perl_ck_tell(pTHX_ OP *o)
{
    PERL_ARGS_ASSERT_CK_TELL;
    SensorCall(8728);o = ck_fun(o);
    SensorCall(8733);if (o->op_flags & OPf_KIDS) {
     SensorCall(8729);OP *kid = cLISTOPo->op_first;
     SensorCall(8731);if (kid->op_type == OP_NULL && kid->op_sibling) {/*173*/SensorCall(8730);kid = kid->op_sibling;/*174*/}
     SensorCall(8732);if (kid->op_type == OP_RV2GV) kid->op_private |= OPpALLOW_FAKE;
    }
    {OP * ReplaceReturn562 = o; SensorCall(8734); return ReplaceReturn562;}
}

OP *
Perl_ck_each(pTHX_ OP *o)
{
SensorCall(8735);    dVAR;
    OP *kid = o->op_flags & OPf_KIDS ? cUNOPo->op_first : NULL;
    const unsigned orig_type  = o->op_type;
    const unsigned array_type = orig_type == OP_EACH ? OP_AEACH
	                      : orig_type == OP_KEYS ? OP_AKEYS : OP_AVALUES;
    const unsigned ref_type   = orig_type == OP_EACH ? OP_REACH
	                      : orig_type == OP_KEYS ? OP_RKEYS : OP_RVALUES;

    PERL_ARGS_ASSERT_CK_EACH;

    SensorCall(8741);if (kid) {
	SensorCall(8736);switch (kid->op_type) {
	    case OP_PADHV:
	    case OP_RV2HV:
		SensorCall(8737);break;
	    case OP_PADAV:
	    case OP_RV2AV:
		CHANGE_TYPE(o, array_type);
		SensorCall(8738);break;
	    case OP_CONST:
		SensorCall(8739);if (kid->op_private == OPpCONST_BARE
		 || !SvROK(cSVOPx_sv(kid))
		 || (  SvTYPE(SvRV(cSVOPx_sv(kid))) != SVt_PVAV
		    && SvTYPE(SvRV(cSVOPx_sv(kid))) != SVt_PVHV  )
		   )
		    /* we let ck_fun handle it */
		    {/*15*/SensorCall(8740);break;/*16*/}
	    default:
		CHANGE_TYPE(o, ref_type);
		scalar(kid);
	}
    }
    /* if treating as a reference, defer additional checks to runtime */
    {OP * ReplaceReturn561 = o->op_type == ref_type ? o : ck_fun(o); SensorCall(8742); return ReplaceReturn561;}
}

OP *
Perl_ck_length(pTHX_ OP *o)
{
    PERL_ARGS_ASSERT_CK_LENGTH;

    SensorCall(8743);o = ck_fun(o);

    SensorCall(8763);if (ckWARN(WARN_SYNTAX)) {
        SensorCall(8744);const OP *kid = o->op_flags & OPf_KIDS ? cLISTOPo->op_first : NULL;

        SensorCall(8762);if (kid) {
            SensorCall(8745);SV *name = NULL;
            const bool hash = kid->op_type == OP_PADHV
                           || kid->op_type == OP_RV2HV;
            SensorCall(8756);switch (kid->op_type) {
                case OP_PADHV:
                case OP_PADAV:
                    SensorCall(8746);name = varname(
                        (GV *)PL_compcv, hash ? '%' : '@', kid->op_targ,
                        NULL, 0, 1
                    );
                    SensorCall(8747);break;
                case OP_RV2HV:
                case OP_RV2AV:
                    SensorCall(8748);if (cUNOPx(kid)->op_first->op_type != OP_GV) {/*117*/SensorCall(8749);break;/*118*/}
                    {
                        SensorCall(8750);GV *gv = cGVOPx_gv(cUNOPx(kid)->op_first);
                        SensorCall(8752);if (!gv) {/*119*/SensorCall(8751);break;/*120*/}
                        SensorCall(8753);name = varname(gv, hash?'%':'@', 0, NULL, 0, 1);
                    }
                    SensorCall(8754);break;
                default:
                    {OP * ReplaceReturn560 = o; SensorCall(8755); return ReplaceReturn560;}
            }
            SensorCall(8761);if (name)
                {/*121*/SensorCall(8757);Perl_warner(aTHX_ packWARN(WARN_SYNTAX),
                    "length() used on %"SVf" (did you mean \"scalar(%s%"SVf
                    ")\"?)",
                    name, hash ? "keys " : "", name
                );/*122*/}
            else {/*123*/SensorCall(8758);if (hash)
                {/*125*/SensorCall(8759);Perl_warner(aTHX_ packWARN(WARN_SYNTAX),
                    "length() used on %%hash (did you mean \"scalar(keys %%hash)\"?)");/*126*/}
            else
                {/*127*/SensorCall(8760);Perl_warner(aTHX_ packWARN(WARN_SYNTAX),
                    "length() used on @array (did you mean \"scalar(@array)\"?)");/*128*/}/*124*/}
        }
    }

    {OP * ReplaceReturn559 = o; SensorCall(8764); return ReplaceReturn559;}
}

/* caller is supposed to assign the return to the 
   container of the rep_op var */
STATIC OP *
S_opt_scalarhv(pTHX_ OP *rep_op) {
SensorCall(8765);    dVAR;
    UNOP *unop;

    PERL_ARGS_ASSERT_OPT_SCALARHV;

    NewOp(1101, unop, 1, UNOP);
    unop->op_type = (OPCODE)OP_BOOLKEYS;
    unop->op_ppaddr = PL_ppaddr[OP_BOOLKEYS];
    unop->op_flags = (U8)(OPf_WANT_SCALAR | OPf_KIDS );
    unop->op_private = (U8)(1 | ((OPf_WANT_SCALAR | OPf_KIDS) >> 8));
    unop->op_first = rep_op;
    unop->op_next = rep_op->op_next;
    rep_op->op_next = (OP*)unop;
    rep_op->op_flags|=(OPf_REF | OPf_MOD);
    unop->op_sibling = rep_op->op_sibling;
    rep_op->op_sibling = NULL;
    /* unop->op_targ = pad_alloc(OP_BOOLKEYS, SVs_PADTMP); */
    SensorCall(8767);if (rep_op->op_type == OP_PADHV) { 
        SensorCall(8766);rep_op->op_flags &= ~OPf_WANT_SCALAR;
        rep_op->op_flags |= OPf_WANT_LIST;
    }
    {OP * ReplaceReturn558 = (OP*)unop; SensorCall(8768); return ReplaceReturn558;}
}                        

/* Check for in place reverse and sort assignments like "@a = reverse @a"
   and modify the optree to make them work inplace */

STATIC void
S_inplace_aassign(pTHX_ OP *o) {

    SensorCall(8769);OP *modop, *modop_pushmark;
    OP *oright;
    OP *oleft, *oleft_pushmark;

    PERL_ARGS_ASSERT_INPLACE_AASSIGN;

    assert((o->op_flags & OPf_WANT) == OPf_WANT_VOID);

    assert(cUNOPo->op_first->op_type == OP_NULL);
    modop_pushmark = cUNOPx(cUNOPo->op_first)->op_first;
    assert(modop_pushmark->op_type == OP_PUSHMARK);
    modop = modop_pushmark->op_sibling;

    SensorCall(8771);if (modop->op_type != OP_SORT && modop->op_type != OP_REVERSE)
	{/*567*/SensorCall(8770);return;/*568*/}

    /* no other operation except sort/reverse */
    SensorCall(8773);if (modop->op_sibling)
	{/*569*/SensorCall(8772);return;/*570*/}

    assert(cUNOPx(modop)->op_first->op_type == OP_PUSHMARK);
    SensorCall(8775);if (!(oright = cUNOPx(modop)->op_first->op_sibling)) {/*571*/SensorCall(8774);return;/*572*/}

    SensorCall(8777);if (modop->op_flags & OPf_STACKED) {
	/* skip sort subroutine/block */
	assert(oright->op_type == OP_NULL);
	SensorCall(8776);oright = oright->op_sibling;
    }

    assert(cUNOPo->op_first->op_sibling->op_type == OP_NULL);
    SensorCall(8778);oleft_pushmark = cUNOPx(cUNOPo->op_first->op_sibling)->op_first;
    assert(oleft_pushmark->op_type == OP_PUSHMARK);
    oleft = oleft_pushmark->op_sibling;

    /* Check the lhs is an array */
    SensorCall(8780);if (!oleft ||
	(oleft->op_type != OP_RV2AV && oleft->op_type != OP_PADAV)
	|| oleft->op_sibling
	|| (oleft->op_private & OPpLVAL_INTRO)
    )
	{/*573*/SensorCall(8779);return;/*574*/}

    /* Only one thing on the rhs */
    SensorCall(8782);if (oright->op_sibling)
	{/*575*/SensorCall(8781);return;/*576*/}

    /* check the array is the same on both sides */
    SensorCall(8787);if (oleft->op_type == OP_RV2AV) {
	SensorCall(8783);if (oright->op_type != OP_RV2AV
	    || !cUNOPx(oright)->op_first
	    || cUNOPx(oright)->op_first->op_type != OP_GV
	    || cUNOPx(oleft )->op_first->op_type != OP_GV
	    || cGVOPx_gv(cUNOPx(oleft)->op_first) !=
	       cGVOPx_gv(cUNOPx(oright)->op_first)
	)
	    {/*577*/SensorCall(8784);return;/*578*/}
    }
    else {/*579*/SensorCall(8785);if (oright->op_type != OP_PADAV
	|| oright->op_targ != oleft->op_targ
    )
	{/*581*/SensorCall(8786);/*580*/}return;/*582*/}

    /* This actually is an inplace assignment */

    SensorCall(8788);modop->op_private |= OPpSORT_INPLACE;

    /* transfer MODishness etc from LHS arg to RHS arg */
    oright->op_flags = oleft->op_flags;

    /* remove the aassign op and the lhs */
    op_null(o);
    op_null(oleft_pushmark);
    SensorCall(8789);if (oleft->op_type == OP_RV2AV && cUNOPx(oleft)->op_first)
	op_null(cUNOPx(oleft)->op_first);
    op_null(oleft);
}

#define MAX_DEFERRED 4

#define DEFER(o) \
    if (defer_ix == (MAX_DEFERRED-1)) { \
	CALL_RPEEP(defer_queue[defer_base]); \
	defer_base = (defer_base + 1) % MAX_DEFERRED; \
	defer_ix--; \
    } \
    defer_queue[(defer_base + ++defer_ix) % MAX_DEFERRED] = o;

/* A peephole optimizer.  We visit the ops in the order they're to execute.
 * See the comments at the top of this file for more details about when
 * peep() is called */

void
Perl_rpeep(pTHX_ register OP *o)
{
SensorCall(8790);    dVAR;
    register OP* oldop = NULL;
    OP* defer_queue[MAX_DEFERRED]; /* small queue of deferred branches */
    int defer_base = 0;
    int defer_ix = -1;

    SensorCall(8792);if (!o || o->op_opt)
	{/*429*/SensorCall(8791);return;/*430*/}
    ENTER;
    SAVEOP();
    SAVEVPTR(PL_curcop);
    SensorCall(8925);for (;; o = o->op_next) {
	SensorCall(8793);if (o && o->op_opt)
	    o = NULL;
	SensorCall(8795);if (!o) {
	    while (defer_ix >= 0)
		CALL_RPEEP(defer_queue[(defer_base + defer_ix--) % MAX_DEFERRED]);
	    SensorCall(8794);break;
	}

	/* By default, this op has now been optimised. A couple of cases below
	   clear this again.  */
	SensorCall(8796);o->op_opt = 1;
	PL_op = o;
	SensorCall(8923);switch (o->op_type) {
	case OP_DBSTATE:
	    PL_curcop = ((COP*)o);		/* for warnings */
	    SensorCall(8797);break;
	case OP_NEXTSTATE:
	    PL_curcop = ((COP*)o);		/* for warnings */

	    /* Two NEXTSTATEs in a row serve no purpose. Except if they happen
	       to carry two labels. For now, take the easier option, and skip
	       this optimisation if the first NEXTSTATE has a label.  */
	    SensorCall(8803);if (!CopLABEL((COP*)o) && !PERLDB_NOOPT) {
		SensorCall(8798);OP *nextop = o->op_next;
		SensorCall(8800);while (nextop && nextop->op_type == OP_NULL)
		    {/*431*/SensorCall(8799);nextop = nextop->op_next;/*432*/}

		SensorCall(8802);if (nextop && (nextop->op_type == OP_NEXTSTATE)) {
		    SensorCall(8801);COP *firstcop = (COP *)o;
		    COP *secondcop = (COP *)nextop;
		    /* We want the COP pointed to by o (and anything else) to
		       become the next COP down the line.  */
		    cop_free(firstcop);

		    firstcop->op_next = secondcop->op_next;

		    /* Now steal all its pointers, and duplicate the other
		       data.  */
		    firstcop->cop_line = secondcop->cop_line;
#ifdef USE_ITHREADS
		    firstcop->cop_stashpv = secondcop->cop_stashpv;
		    firstcop->cop_stashlen = secondcop->cop_stashlen;
		    firstcop->cop_file = secondcop->cop_file;
#else
		    firstcop->cop_stash = secondcop->cop_stash;
		    firstcop->cop_filegv = secondcop->cop_filegv;
#endif
		    firstcop->cop_hints = secondcop->cop_hints;
		    firstcop->cop_seq = secondcop->cop_seq;
		    firstcop->cop_warnings = secondcop->cop_warnings;
		    firstcop->cop_hints_hash = secondcop->cop_hints_hash;

#ifdef USE_ITHREADS
		    secondcop->cop_stashpv = NULL;
		    secondcop->cop_file = NULL;
#else
		    secondcop->cop_stash = NULL;
		    secondcop->cop_filegv = NULL;
#endif
		    secondcop->cop_warnings = NULL;
		    secondcop->cop_hints_hash = NULL;

		    /* If we use op_null(), and hence leave an ex-COP, some
		       warnings are misreported. For example, the compile-time
		       error in 'use strict; no strict refs;'  */
		    secondcop->op_type = OP_NULL;
		    secondcop->op_ppaddr = PL_ppaddr[OP_NULL];
		}
	    }
	    SensorCall(8804);break;

	case OP_CONCAT:
	    SensorCall(8805);if (o->op_next && o->op_next->op_type == OP_STRINGIFY) {
		SensorCall(8806);if (o->op_next->op_private & OPpTARGET_MY) {
		    SensorCall(8807);if (o->op_flags & OPf_STACKED) /* chained concats */
			{/*433*/SensorCall(8808);break;/*434*/} /* ignore_optimization */
		    else {
			/* assert(PL_opargs[o->op_type] & OA_TARGLEX); */
			SensorCall(8809);o->op_targ = o->op_next->op_targ;
			o->op_next->op_targ = 0;
			o->op_private |= OPpTARGET_MY;
		    }
		}
		op_null(o->op_next);
	    }
	    SensorCall(8810);break;
	case OP_STUB:
	    SensorCall(8811);if ((o->op_flags & OPf_WANT) != OPf_WANT_LIST) {
		SensorCall(8812);break; /* Scalar stub must produce undef.  List stub is noop */
	    }
	    SensorCall(8813);goto nothin;
	case OP_NULL:
	    SensorCall(8814);if (o->op_targ == OP_NEXTSTATE
		|| o->op_targ == OP_DBSTATE)
	    {
		PL_curcop = ((COP*)o);
	    }
	    /* XXX: We avoid setting op_seq here to prevent later calls
	       to rpeep() from mistakenly concluding that optimisation
	       has already occurred. This doesn't fix the real problem,
	       though (See 20010220.007). AMS 20010719 */
	    /* op_seq functionality is now replaced by op_opt */
	    SensorCall(8815);o->op_opt = 0;
	    /* FALL THROUGH */
	case OP_SCALAR:
	case OP_LINESEQ:
	case OP_SCOPE:
	nothin:
	    SensorCall(8816);if (oldop && o->op_next) {
		SensorCall(8817);oldop->op_next = o->op_next;
		o->op_opt = 0;
		SensorCall(8818);continue;
	    }
	    SensorCall(8819);break;

	case OP_PADAV:
	case OP_GV:
	    SensorCall(8820);if (o->op_type == OP_PADAV || o->op_next->op_type == OP_RV2AV) {
		SensorCall(8821);OP* const pop = (o->op_type == OP_PADAV) ?
			    o->op_next : o->op_next->op_next;
		IV i;
		SensorCall(8829);if (pop && pop->op_type == OP_CONST &&
		    ((PL_op = pop->op_next)) &&
		    pop->op_next->op_type == OP_AELEM &&
		    !(pop->op_next->op_private &
		      (OPpLVAL_INTRO|OPpLVAL_DEFER|OPpDEREF|OPpMAYBE_LVSUB)) &&
		    (i = SvIV(((SVOP*)pop)->op_sv)) <= 255 && i >= 0)
		{
		    SensorCall(8822);GV *gv;
		    SensorCall(8823);if (cSVOPx(pop)->op_private & OPpCONST_STRICT)
			no_bareword_allowed(pop);
		    SensorCall(8824);if (o->op_type == OP_GV)
			op_null(o->op_next);
		    op_null(pop->op_next);
		    op_null(pop);
		    SensorCall(8825);o->op_flags |= pop->op_next->op_flags & OPf_MOD;
		    o->op_next = pop->op_next->op_next;
		    o->op_ppaddr = PL_ppaddr[OP_AELEMFAST];
		    o->op_private = (U8)i;
		    SensorCall(8828);if (o->op_type == OP_GV) {
			SensorCall(8826);gv = cGVOPo_gv;
			GvAVn(gv);
			o->op_type = OP_AELEMFAST;
		    }
		    else
			{/*435*/SensorCall(8827);o->op_type = OP_AELEMFAST_LEX;/*436*/}
		}
		SensorCall(8830);break;
	    }

	    SensorCall(8835);if (o->op_next->op_type == OP_RV2SV) {
		SensorCall(8831);if (!(o->op_next->op_private & OPpDEREF)) {
		    op_null(o->op_next);
		    SensorCall(8832);o->op_private |= o->op_next->op_private & (OPpLVAL_INTRO
							       | OPpOUR_INTRO);
		    o->op_next = o->op_next->op_next;
		    o->op_type = OP_GVSV;
		    o->op_ppaddr = PL_ppaddr[OP_GVSV];
		}
	    }
	    else {/*437*/SensorCall(8833);if (o->op_next->op_type == OP_READLINE
		    && o->op_next->op_next->op_type == OP_CONCAT
		    && (o->op_next->op_next->op_flags & OPf_STACKED))
	    {
		/* Turn "$a .= <FH>" into an OP_RCATLINE. AMS 20010917 */
		SensorCall(8834);o->op_type   = OP_RCATLINE;
		o->op_flags |= OPf_STACKED;
		o->op_ppaddr = PL_ppaddr[OP_RCATLINE];
		op_null(o->op_next->op_next);
		op_null(o->op_next);
	    ;/*438*/}}

	    SensorCall(8836);break;
        
        {
            OP *fop;
            OP *sop;
            
        case OP_NOT:
            fop = cUNOP->op_first;
            sop = NULL;
            goto stitch_keys;
            break;

        case OP_AND:
	case OP_OR:
	case OP_DOR:
            fop = cLOGOP->op_first;
            sop = fop->op_sibling;
	    while (cLOGOP->op_other->op_type == OP_NULL)
		cLOGOP->op_other = cLOGOP->op_other->op_next;
	    while (o->op_next && (   o->op_type == o->op_next->op_type
				  || o->op_next->op_type == OP_NULL))
		{/*439*/o->op_next = o->op_next->op_next;/*440*/}
	    DEFER(cLOGOP->op_other);
          
          stitch_keys:	    
	    o->op_opt = 1;
            if ((fop->op_type == OP_PADHV || fop->op_type == OP_RV2HV)
                || ( sop && 
                     (sop->op_type == OP_PADHV || sop->op_type == OP_RV2HV)
                    )
            ){	
                OP * nop = o;
                OP * lop = o;
                if (!((nop->op_flags & OPf_WANT) == OPf_WANT_VOID)) {
                    while (nop && nop->op_next) {
                        switch (nop->op_next->op_type) {
                            case OP_NOT:
                            case OP_AND:
                            case OP_OR:
                            case OP_DOR:
                                lop = nop = nop->op_next;
                                break;
                            case OP_NULL:
                                nop = nop->op_next;
                                break;
                            default:
                                nop = NULL;
                                break;
                        }
                    }            
                }
                if ((lop->op_flags & OPf_WANT) == OPf_WANT_VOID) {
                    if (fop->op_type == OP_PADHV || fop->op_type == OP_RV2HV) 
                        cLOGOP->op_first = opt_scalarhv(fop);
                    if (sop && (sop->op_type == OP_PADHV || sop->op_type == OP_RV2HV)) 
                        cLOGOP->op_first->op_sibling = opt_scalarhv(sop);
                }                                        
            }                  
            
	    
	    break;
	}    
	
	case OP_MAPWHILE:
	case OP_GREPWHILE:
	case OP_ANDASSIGN:
	case OP_ORASSIGN:
	case OP_DORASSIGN:
	case OP_COND_EXPR:
	case OP_RANGE:
	case OP_ONCE:
	    SensorCall(8837);while (cLOGOP->op_other->op_type == OP_NULL)
		cLOGOP->op_other = cLOGOP->op_other->op_next;
	    DEFER(cLOGOP->op_other);
	    SensorCall(8838);break;

	case OP_ENTERLOOP:
	case OP_ENTERITER:
	    SensorCall(8839);while (cLOOP->op_redoop->op_type == OP_NULL)
		cLOOP->op_redoop = cLOOP->op_redoop->op_next;
	    SensorCall(8840);while (cLOOP->op_nextop->op_type == OP_NULL)
		cLOOP->op_nextop = cLOOP->op_nextop->op_next;
	    SensorCall(8841);while (cLOOP->op_lastop->op_type == OP_NULL)
		cLOOP->op_lastop = cLOOP->op_lastop->op_next;
	    /* a while(1) loop doesn't have an op_next that escapes the
	     * loop, so we have to explicitly follow the op_lastop to
	     * process the rest of the code */
	    DEFER(cLOOP->op_lastop);
	    SensorCall(8842);break;

	case OP_SUBST:
	    assert(!(cPMOP->op_pmflags & PMf_ONCE));
	    SensorCall(8843);while (cPMOP->op_pmstashstartu.op_pmreplstart &&
		   cPMOP->op_pmstashstartu.op_pmreplstart->op_type == OP_NULL)
		cPMOP->op_pmstashstartu.op_pmreplstart
		    = cPMOP->op_pmstashstartu.op_pmreplstart->op_next;
	    DEFER(cPMOP->op_pmstashstartu.op_pmreplstart);
	    SensorCall(8844);break;

	case OP_SORT: {
	    /* check that RHS of sort is a single plain array */
	    SensorCall(8845);OP *oright = cUNOPo->op_first;
	    SensorCall(8847);if (!oright || oright->op_type != OP_PUSHMARK)
		{/*441*/SensorCall(8846);break;/*442*/}

	    SensorCall(8849);if (o->op_private & OPpSORT_INPLACE)
		{/*443*/SensorCall(8848);break;/*444*/}

	    /* reverse sort ... can be optimised.  */
	    SensorCall(8855);if (!cUNOPo->op_sibling) {
		/* Nothing follows us on the list. */
		SensorCall(8850);OP * const reverse = o->op_next;

		SensorCall(8854);if (reverse->op_type == OP_REVERSE &&
		    (reverse->op_flags & OPf_WANT) == OPf_WANT_LIST) {
		    SensorCall(8851);OP * const pushmark = cUNOPx(reverse)->op_first;
		    SensorCall(8853);if (pushmark && (pushmark->op_type == OP_PUSHMARK)
			&& (cUNOPx(pushmark)->op_sibling == o)) {
			/* reverse -> pushmark -> sort */
			SensorCall(8852);o->op_private |= OPpSORT_REVERSE;
			op_null(reverse);
			pushmark->op_next = oright->op_next;
			op_null(oright);
		    }
		}
	    }

	    SensorCall(8856);break;
	}

	case OP_REVERSE: {
	    SensorCall(8857);OP *ourmark, *theirmark, *ourlast, *iter, *expushmark, *rv2av;
	    OP *gvop = NULL;
	    LISTOP *enter, *exlist;

	    SensorCall(8859);if (o->op_private & OPpSORT_INPLACE)
		{/*445*/SensorCall(8858);break;/*446*/}

	    SensorCall(8860);enter = (LISTOP *) o->op_next;
	    SensorCall(8862);if (!enter)
		{/*447*/SensorCall(8861);break;/*448*/}
	    SensorCall(8866);if (enter->op_type == OP_NULL) {
		SensorCall(8863);enter = (LISTOP *) enter->op_next;
		SensorCall(8865);if (!enter)
		    {/*449*/SensorCall(8864);break;/*450*/}
	    }
	    /* for $a (...) will have OP_GV then OP_RV2GV here.
	       for (...) just has an OP_GV.  */
	    SensorCall(8874);if (enter->op_type == OP_GV) {
		SensorCall(8867);gvop = (OP *) enter;
		enter = (LISTOP *) enter->op_next;
		SensorCall(8869);if (!enter)
		    {/*451*/SensorCall(8868);break;/*452*/}
		SensorCall(8873);if (enter->op_type == OP_RV2GV) {
		  SensorCall(8870);enter = (LISTOP *) enter->op_next;
		  SensorCall(8872);if (!enter)
		    {/*453*/SensorCall(8871);break;/*454*/}
		}
	    }

	    SensorCall(8876);if (enter->op_type != OP_ENTERITER)
		{/*455*/SensorCall(8875);break;/*456*/}

	    SensorCall(8877);iter = enter->op_next;
	    SensorCall(8879);if (!iter || iter->op_type != OP_ITER)
		{/*457*/SensorCall(8878);break;/*458*/}
	    
	    SensorCall(8880);expushmark = enter->op_first;
	    SensorCall(8882);if (!expushmark || expushmark->op_type != OP_NULL
		|| expushmark->op_targ != OP_PUSHMARK)
		{/*459*/SensorCall(8881);break;/*460*/}

	    SensorCall(8883);exlist = (LISTOP *) expushmark->op_sibling;
	    SensorCall(8885);if (!exlist || exlist->op_type != OP_NULL
		|| exlist->op_targ != OP_LIST)
		{/*461*/SensorCall(8884);break;/*462*/}

	    SensorCall(8887);if (exlist->op_last != o) {
		/* Mmm. Was expecting to point back to this op.  */
		SensorCall(8886);break;
	    }
	    SensorCall(8888);theirmark = exlist->op_first;
	    SensorCall(8890);if (!theirmark || theirmark->op_type != OP_PUSHMARK)
		{/*463*/SensorCall(8889);break;/*464*/}

	    SensorCall(8892);if (theirmark->op_sibling != o) {
		/* There's something between the mark and the reverse, eg
		   for (1, reverse (...))
		   so no go.  */
		SensorCall(8891);break;
	    }

	    SensorCall(8893);ourmark = ((LISTOP *)o)->op_first;
	    SensorCall(8895);if (!ourmark || ourmark->op_type != OP_PUSHMARK)
		{/*465*/SensorCall(8894);break;/*466*/}

	    SensorCall(8896);ourlast = ((LISTOP *)o)->op_last;
	    SensorCall(8898);if (!ourlast || ourlast->op_next != o)
		{/*467*/SensorCall(8897);break;/*468*/}

	    SensorCall(8899);rv2av = ourmark->op_sibling;
	    SensorCall(8901);if (rv2av && rv2av->op_type == OP_RV2AV && rv2av->op_sibling == 0
		&& rv2av->op_flags == (OPf_WANT_LIST | OPf_KIDS)
		&& enter->op_flags == (OPf_WANT_LIST | OPf_KIDS)) {
		/* We're just reversing a single array.  */
		SensorCall(8900);rv2av->op_flags = OPf_WANT_SCALAR | OPf_KIDS | OPf_REF;
		enter->op_flags |= OPf_STACKED;
	    }

	    /* We don't have control over who points to theirmark, so sacrifice
	       ours.  */
	    SensorCall(8902);theirmark->op_next = ourmark->op_next;
	    theirmark->op_flags = ourmark->op_flags;
	    ourlast->op_next = gvop ? gvop : (OP *) enter;
	    op_null(ourmark);
	    op_null(o);
	    enter->op_private |= OPpITER_REVERSED;
	    iter->op_private |= OPpITER_REVERSED;
	    
	    SensorCall(8903);break;
	}

	case OP_QR:
	case OP_MATCH:
	    SensorCall(8904);if (!(cPMOP->op_pmflags & PMf_ONCE)) {
		assert (!cPMOP->op_pmstashstartu.op_pmreplstart);
	    }
	    SensorCall(8905);break;

	case OP_RUNCV:
	    SensorCall(8906);if (!(o->op_private & OPpOFFBYONE) && !CvCLONE(PL_compcv)) {
		SensorCall(8907);SV *sv;
		SensorCall(8909);if (CvEVAL(PL_compcv)) sv = &PL_sv_undef;
		else {
		    SensorCall(8908);sv = newRV((SV *)PL_compcv);
		    sv_rvweaken(sv);
		    SvREADONLY_on(sv);
		}
		SensorCall(8910);o->op_type = OP_CONST;
		o->op_ppaddr = PL_ppaddr[OP_CONST];
		o->op_flags |= OPf_SPECIAL;
		cSVOPo->op_sv = sv;
	    }
	    SensorCall(8911);break;

	case OP_SASSIGN:
	    SensorCall(8912);if (OP_GIMME(o,0) == G_VOID) {
		SensorCall(8913);OP *right = cBINOP->op_first;
		SensorCall(8917);if (right) {
		    SensorCall(8914);OP *left = right->op_sibling;
		    SensorCall(8916);if (left->op_type == OP_SUBSTR
			 && (left->op_private & 7) < 4) {
			op_null(o);
			cBINOP->op_first = left;
			SensorCall(8915);right->op_sibling =
			    cBINOPx(left)->op_first->op_sibling;
			cBINOPx(left)->op_first->op_sibling = right;
			left->op_private |= OPpSUBSTR_REPL_FIRST;
			left->op_flags =
			    (o->op_flags & ~OPf_WANT) | OPf_WANT_VOID;
		    }
		}
	    }
	    SensorCall(8918);break;

	case OP_CUSTOM: {
	    SensorCall(8919);Perl_cpeep_t cpeep = 
		XopENTRY(Perl_custom_op_xop(aTHX_ o), xop_peep);
	    SensorCall(8921);if (cpeep)
		{/*469*/SensorCall(8920);cpeep(aTHX_ o, oldop);/*470*/}
	    SensorCall(8922);break;
	}
	    
	}
	SensorCall(8924);oldop = o;
    }
    LEAVE;
}

void
Perl_peep(pTHX_ register OP *o)
{
SensorCall(8926);    CALL_RPEEP(o);
SensorCall(8927);}

/*
=head1 Custom Operators

=for apidoc Ao||custom_op_xop
Return the XOP structure for a given custom op. This function should be
considered internal to OP_NAME and the other access macros: use them instead.

=cut
*/

const XOP *
Perl_custom_op_xop(pTHX_ const OP *o)
{
    SensorCall(8928);SV *keysv;
    HE *he = NULL;
    XOP *xop;

    static const XOP xop_null = { 0, 0, 0, 0, 0 };

    PERL_ARGS_ASSERT_CUSTOM_OP_XOP;
    assert(o->op_type == OP_CUSTOM);

    /* This is wrong. It assumes a function pointer can be cast to IV,
     * which isn't guaranteed, but this is what the old custom OP code
     * did. In principle it should be safer to Copy the bytes of the
     * pointer into a PV: since the new interface is hidden behind
     * functions, this can be changed later if necessary.  */
    /* Change custom_op_xop if this ever happens */
    keysv = sv_2mortal(newSViv(PTR2IV(o->op_ppaddr)));

    SensorCall(8929);if (PL_custom_ops)
	he = hv_fetch_ent(PL_custom_ops, keysv, 0, 0);

    /* assume noone will have just registered a desc */
    SensorCall(8935);if (!he && PL_custom_op_names &&
	(he = hv_fetch_ent(PL_custom_op_names, keysv, 0, 0))
    ) {
	SensorCall(8930);const char *pv;
	STRLEN l;

	/* XXX does all this need to be shared mem? */
	Newxz(xop, 1, XOP);
	pv = SvPV(HeVAL(he), l);
	XopENTRY_set(xop, xop_name, savepvn(pv, l));
	SensorCall(8932);if (PL_custom_op_descs &&
	    (he = hv_fetch_ent(PL_custom_op_descs, keysv, 0, 0))
	) {
	    SensorCall(8931);pv = SvPV(HeVAL(he), l);
	    XopENTRY_set(xop, xop_desc, savepvn(pv, l));
	}
	SensorCall(8933);Perl_custom_op_register(aTHX_ o->op_ppaddr, xop);
	{const XOP * ReplaceReturn557 = xop; SensorCall(8934); return ReplaceReturn557;}
    }

    SensorCall(8937);if (!he) {/*209*/{const XOP * ReplaceReturn556 = &xop_null; SensorCall(8936); return ReplaceReturn556;}/*210*/}

    SensorCall(8938);xop = INT2PTR(XOP *, SvIV(HeVAL(he)));
    {const XOP * ReplaceReturn555 = xop; SensorCall(8939); return ReplaceReturn555;}
}

/*
=for apidoc Ao||custom_op_register
Register a custom op. See L<perlguts/"Custom Operators">.

=cut
*/

void
Perl_custom_op_register(pTHX_ Perl_ppaddr_t ppaddr, const XOP *xop)
{
    SensorCall(8940);SV *keysv;

    PERL_ARGS_ASSERT_CUSTOM_OP_REGISTER;

    /* see the comment in custom_op_xop */
    keysv = sv_2mortal(newSViv(PTR2IV(ppaddr)));

    SensorCall(8941);if (!PL_custom_ops)
	PL_custom_ops = newHV();

    SensorCall(8943);if (!hv_store_ent(PL_custom_ops, keysv, newSViv(PTR2IV(xop)), 0))
	{/*207*/SensorCall(8942);Perl_croak(aTHX_ "panic: can't register custom OP %s", xop->xop_name);/*208*/}
SensorCall(8944);}

/*
=head1 Functions in file op.c

=for apidoc core_prototype
This function assigns the prototype of the named core function to C<sv>, or
to a new mortal SV if C<sv> is NULL.  It returns the modified C<sv>, or
NULL if the core function has no prototype.  C<code> is a code as returned
by C<keyword()>.  It must be negative and unequal to -KEY_CORE.

=cut
*/

SV *
Perl_core_prototype(pTHX_ SV *sv, const char *name, const int code,
                          int * const opnum)
{
    SensorCall(8945);int i = 0, n = 0, seen_question = 0, defgv = 0;
    I32 oa;
#define MAX_ARGS_OP ((sizeof(I32) - 1) * 2)
    char str[ MAX_ARGS_OP * 2 + 2 ]; /* One ';', one '\0' */
    bool nullret = FALSE;

    PERL_ARGS_ASSERT_CORE_PROTOTYPE;

    assert (code < 0 && code != -KEY_CORE);

    SensorCall(8946);if (!sv) sv = sv_newmortal();

#define retsetpvs(x,y) sv_setpvs(sv, x); if(opnum) *opnum=(y); return sv

    SensorCall(8956);switch (-code) {
    case KEY_and   : case KEY_chop: case KEY_chomp:
    case KEY_cmp   : case KEY_exec: case KEY_eq   :
    case KEY_ge    : case KEY_gt  : case KEY_le   :
    case KEY_lt    : case KEY_ne  : case KEY_or   :
    case KEY_select: case KEY_system: case KEY_x  : case KEY_xor:
	SensorCall(8947);if (!opnum) return NULL; SensorCall(8948);nullret = TRUE; SensorCall(8949);goto findopnum;
    case KEY_keys:    retsetpvs("+", OP_KEYS);
    case KEY_values:  retsetpvs("+", OP_VALUES);
    case KEY_each:    retsetpvs("+", OP_EACH);
    case KEY_push:    retsetpvs("+@", OP_PUSH);
    case KEY_unshift: retsetpvs("+@", OP_UNSHIFT);
    case KEY_pop:     retsetpvs(";+", OP_POP);
    case KEY_shift:   retsetpvs(";+", OP_SHIFT);
    case KEY_splice:
	retsetpvs("+;$$@", OP_SPLICE);
    case KEY___FILE__: case KEY___LINE__: case KEY___PACKAGE__:
	retsetpvs("", 0);
    case KEY_evalbytes:
	SensorCall(8953);name = "entereval"; SensorCall(8954);break;
    case KEY_readpipe:
	SensorCall(8955);name = "backtick";
    }

#undef retsetpvs

  findopnum:
    SensorCall(8963);while (i < MAXO) {	/* The slow way. */
	SensorCall(8957);if (strEQ(name, PL_op_name[i])
	    || strEQ(name, PL_op_desc[i]))
	{
	    SensorCall(8958);if (nullret) { assert(opnum); SensorCall(8959);*opnum = i; {SV * ReplaceReturn554 = NULL; SensorCall(8960); return ReplaceReturn554;} }
	    SensorCall(8961);goto found;
	}
	SensorCall(8962);i++;
    }
    assert(0); {SV * ReplaceReturn553 = NULL; SensorCall(8964); return ReplaceReturn553;}    /* Should not happen... */
  found:
    defgv = PL_opargs[i] & OA_DEFGV;
    oa = PL_opargs[i] >> OASHIFT;
    while (oa) {
	if (oa & OA_OPTIONAL && !seen_question && (
	      !defgv || (oa & (OA_OPTIONAL - 1)) == OA_FILEREF
	)) {
	    seen_question = 1;
	    str[n++] = ';';
	}
	if ((oa & (OA_OPTIONAL - 1)) >= OA_AVREF
	    && (oa & (OA_OPTIONAL - 1)) <= OA_SCALARREF
	    /* But globs are already references (kinda) */
	    && (oa & (OA_OPTIONAL - 1)) != OA_FILEREF
	) {
	    str[n++] = '\\';
	}
	if ((oa & (OA_OPTIONAL - 1)) == OA_SCALARREF
	 && !scalar_mod_type(NULL, i)) {
	    str[n++] = '[';
	    str[n++] = '$';
	    str[n++] = '@';
	    str[n++] = '%';
	    if (i == OP_LOCK) {/*197*/str[n++] = '&';/*198*/}
	    str[n++] = '*';
	    str[n++] = ']';
	}
	else {/*199*/str[n++] = ("?$@@%&*$")[oa & (OA_OPTIONAL - 1)];/*200*/}
	if (oa & OA_OPTIONAL && defgv && str[n-1] == '$') {
	    str[n-1] = '_'; defgv = 0;
	}
	oa = oa >> 4;
    }
    if (code == -KEY_not || code == -KEY_getprotobynumber) {/*201*/str[n++] = ';';/*202*/}
    str[n++] = '\0';
    sv_setpvn(sv, str, n - 1);
    if (opnum) {/*203*/*opnum = i;/*204*/}
    return sv;
}

OP *
Perl_coresub_op(pTHX_ SV * const coreargssv, const int code,
                      const int opnum)
{
    SensorCall(8965);OP * const argop = newSVOP(OP_COREARGS,0,coreargssv);
    OP *o;

    PERL_ARGS_ASSERT_CORESUB_OP;

    SensorCall(8983);switch(opnum) {
    case 0:
	{OP * ReplaceReturn552 = op_append_elem(OP_LINESEQ,
	               argop,
	               newSLICEOP(0,
	                          newSVOP(OP_CONST, 0, newSViv(-code % 3)),
	                          newOP(OP_CALLER,0)
	               )
	       ); SensorCall(8966); return ReplaceReturn552;}
    case OP_SELECT: /* which represents OP_SSELECT as well */
	SensorCall(8967);if (code)
	    return newCONDOP(
	                 0,
	                 newBINOP(OP_GT, 0,
	                          newAVREF(newGVOP(OP_GV, 0, PL_defgv)),
	                          newSVOP(OP_CONST, 0, newSVuv(1))
	                         ),
	                 coresub_op(newSVuv((UV)OP_SSELECT), 0,
	                            OP_SSELECT),
	                 coresub_op(coreargssv, 0, OP_SELECT)
	           );
	/* FALL THROUGH */
    default:
	SensorCall(8968);switch (PL_opargs[opnum] & OA_CLASS_MASK) {
	case OA_BASEOP:
	    {OP * ReplaceReturn551 = op_append_elem(
	                OP_LINESEQ, argop,
	                newOP(opnum,
	                      opnum == OP_WANTARRAY || opnum == OP_RUNCV
	                        ? OPpOFFBYONE << 8 : 0)
	           ); SensorCall(8969); return ReplaceReturn551;}
	case OA_BASEOP_OR_UNOP:
	    SensorCall(8970);if (opnum == OP_ENTEREVAL) {
		SensorCall(8971);o = newUNOP(OP_ENTEREVAL,OPpEVAL_COPHH<<8,argop);
		SensorCall(8972);if (code == -KEY_evalbytes) o->op_private |= OPpEVAL_BYTES;
	    }
	    else o = newUNOP(opnum,0,argop);
	    SensorCall(8974);if (opnum == OP_CALLER) o->op_private |= OPpOFFBYONE;
	    else {
	  onearg:
	      SensorCall(8973);if (is_handle_constructor(o, 1))
		argop->op_private |= OPpCOREARGS_DEREF1;
	    }
	    {OP * ReplaceReturn550 = o; SensorCall(8975); return ReplaceReturn550;}
	default:
	    SensorCall(8976);o = convert(opnum,0,argop);
	    SensorCall(8977);if (is_handle_constructor(o, 2))
		argop->op_private |= OPpCOREARGS_DEREF2;
	    SensorCall(8978);if (scalar_mod_type(NULL, opnum))
		argop->op_private |= OPpCOREARGS_SCALARMOD;
	    SensorCall(8982);if (opnum == OP_SUBSTR) {
		SensorCall(8979);o->op_private |= OPpMAYBE_LVSUB;
		{OP * ReplaceReturn549 = o; SensorCall(8980); return ReplaceReturn549;}
	    }
	    else {/*205*/SensorCall(8981);goto onearg;/*206*/}
	}
    }
SensorCall(8984);}

void
Perl_report_redefined_cv(pTHX_ const SV *name, const CV *old_cv,
			       SV * const *new_const_svp)
{
    SensorCall(8985);const char *hvname;
    bool is_const = !!CvCONST(old_cv);
    SV *old_const_sv = is_const ? cv_const_sv(old_cv) : NULL;

    PERL_ARGS_ASSERT_REPORT_REDEFINED_CV;

    SensorCall(8987);if (is_const && new_const_svp && old_const_sv == *new_const_svp)
	{/*731*/SensorCall(8986);return;/*732*/}
	/* They are 2 constant subroutines generated from
	   the same constant. This probably means that
	   they are really the "same" proxy subroutine
	   instantiated in 2 places. Most likely this is
	   when a constant is exported twice.  Don't warn.
	*/
    SensorCall(8989);if (
	(ckWARN(WARN_REDEFINE)
	 && !(
		CvGV(old_cv) && GvSTASH(CvGV(old_cv))
	     && HvNAMELEN(GvSTASH(CvGV(old_cv))) == 7
	     && (hvname = HvNAME(GvSTASH(CvGV(old_cv))),
		 strEQ(hvname, "autouse"))
	     )
	)
     || (is_const
	 && ckWARN_d(WARN_REDEFINE)
	 && (!new_const_svp || sv_cmp(old_const_sv, *new_const_svp))
	)
    )
	{/*733*/SensorCall(8988);Perl_warner(aTHX_ packWARN(WARN_REDEFINE),
			  is_const
			    ? "Constant subroutine %"SVf" redefined"
			    : "Subroutine %"SVf" redefined",
			  name);/*734*/}
SensorCall(8990);}

/*
=head1 Hook manipulation

These functions provide convenient and thread-safe means of manipulating
hook variables.

=cut
*/

/*
=for apidoc Am|void|wrap_op_checker|Optype opcode|Perl_check_t new_checker|Perl_check_t *old_checker_p

Puts a C function into the chain of check functions for a specified op
type.  This is the preferred way to manipulate the L</PL_check> array.
I<opcode> specifies which type of op is to be affected.  I<new_checker>
is a pointer to the C function that is to be added to that opcode's
check chain, and I<old_checker_p> points to the storage location where a
pointer to the next function in the chain will be stored.  The value of
I<new_pointer> is written into the L</PL_check> array, while the value
previously stored there is written to I<*old_checker_p>.

L</PL_check> is global to an entire process, and a module wishing to
hook op checking may find itself invoked more than once per process,
typically in different threads.  To handle that situation, this function
is idempotent.  The location I<*old_checker_p> must initially (once
per process) contain a null pointer.  A C variable of static duration
(declared at file scope, typically also marked C<static> to give
it internal linkage) will be implicitly initialised appropriately,
if it does not have an explicit initialiser.  This function will only
actually modify the check chain if it finds I<*old_checker_p> to be null.
This function is also thread safe on the small scale.  It uses appropriate
locking to avoid race conditions in accessing L</PL_check>.

When this function is called, the function referenced by I<new_checker>
must be ready to be called, except for I<*old_checker_p> being unfilled.
In a threading situation, I<new_checker> may be called immediately,
even before this function has returned.  I<*old_checker_p> will always
be appropriately set before I<new_checker> is called.  If I<new_checker>
decides not to do anything special with an op that it is given (which
is the usual case for most uses of op check hooking), it must chain the
check function referenced by I<*old_checker_p>.

If you want to influence compilation of calls to a specific subroutine,
then use L</cv_set_call_checker> rather than hooking checking of all
C<entersub> ops.

=cut
*/

void
Perl_wrap_op_checker(pTHX_ Optype opcode,
    Perl_check_t new_checker, Perl_check_t *old_checker_p)
{
SensorCall(8991);    dVAR;

    PERL_ARGS_ASSERT_WRAP_OP_CHECKER;
    SensorCall(8993);if (*old_checker_p) {/*515*/SensorCall(8992);return;/*516*/}
    OP_CHECK_MUTEX_LOCK;
    SensorCall(8995);if (!*old_checker_p) {
	SensorCall(8994);*old_checker_p = PL_check[opcode];
	PL_check[opcode] = new_checker;
    }
    OP_CHECK_MUTEX_UNLOCK;
}

#include "XSUB.h"

/* Efficient sub that returns a constant scalar value. */
static void
const_sv_xsub(pTHX_ CV* cv)
{
SensorCall(8996);    dVAR;
    dXSARGS;
    SV *const sv = MUTABLE_SV(XSANY.any_ptr);
    SensorCall(8997);if (items != 0) {
	NOOP;
#if 0
	/* diag_listed_as: SKIPME */
        Perl_croak(aTHX_ "usage: %s::%s()",
                   HvNAME_get(GvSTASH(CvGV(cv))), GvNAME(CvGV(cv)));
#endif
    }
    SensorCall(8998);if (!sv) {
	XSRETURN(0);
    }
    EXTEND(sp, 1);
    ST(0) = sv;
    XSRETURN(1);
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 *
 * ex: set ts=8 sts=4 sw=4 noet:
 */
