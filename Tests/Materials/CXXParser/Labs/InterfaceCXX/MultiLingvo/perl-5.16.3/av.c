#include "var/tmp/sensor.h"
/*    av.c
 *
 *    Copyright (C) 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000,
 *    2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008 by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 *
 */

/*
 * '...for the Entwives desired order, and plenty, and peace (by which they
 *  meant that things should remain where they had set them).' --Treebeard
 *
 *     [p.476 of _The Lord of the Rings_, III/iv: "Treebeard"]
 */

/*
=head1 Array Manipulation Functions
*/

#include "EXTERN.h"
#define PERL_IN_AV_C
#include "perl.h"

void
Perl_av_reify(pTHX_ AV *av)
{
SensorCall();    dVAR;
    I32 key;

    PERL_ARGS_ASSERT_AV_REIFY;
    assert(SvTYPE(av) == SVt_PVAV);

    SensorCall();if (AvREAL(av))
	{/*21*/SensorCall();return;/*22*/}
#ifdef DEBUGGING
    if (SvTIED_mg((const SV *)av, PERL_MAGIC_tied))
	Perl_ck_warner_d(aTHX_ packWARN(WARN_DEBUGGING), "av_reify called on tied array");
#endif
    SensorCall();key = AvMAX(av) + 1;
    SensorCall();while (key > AvFILLp(av) + 1)
	AvARRAY(av)[--key] = &PL_sv_undef;
    SensorCall();while (key) {
	SensorCall();SV * const sv = AvARRAY(av)[--key];
	assert(sv);
	SensorCall();if (sv != &PL_sv_undef)
	    SvREFCNT_inc_simple_void_NN(sv);
    }
    SensorCall();key = AvARRAY(av) - AvALLOC(av);
    SensorCall();while (key)
	AvALLOC(av)[--key] = &PL_sv_undef;
    AvREIFY_off(av);
    AvREAL_on(av);
}

/*
=for apidoc av_extend

Pre-extend an array.  The C<key> is the index to which the array should be
extended.

=cut
*/

void
Perl_av_extend(pTHX_ AV *av, I32 key)
{
SensorCall();    dVAR;
    MAGIC *mg;

    PERL_ARGS_ASSERT_AV_EXTEND;
    assert(SvTYPE(av) == SVt_PVAV);

    mg = SvTIED_mg((const SV *)av, PERL_MAGIC_tied);
    SensorCall();if (mg) {
	SensorCall();SV *arg1 = sv_newmortal();
	sv_setiv(arg1, (IV)(key + 1));
	Perl_magic_methcall(aTHX_ MUTABLE_SV(av), mg, "EXTEND", G_DISCARD, 1,
			    arg1);
	SensorCall();return;
    }
    SensorCall();if (key > AvMAX(av)) {
	SensorCall();SV** ary;
	I32 tmp;
	I32 newmax;

	SensorCall();if (AvALLOC(av) != AvARRAY(av)) {
	    SensorCall();ary = AvALLOC(av) + AvFILLp(av) + 1;
	    tmp = AvARRAY(av) - AvALLOC(av);
	    Move(AvARRAY(av), AvALLOC(av), AvFILLp(av)+1, SV*);
	    AvMAX(av) += tmp;
	    AvARRAY(av) = AvALLOC(av);
	    SensorCall();if (AvREAL(av)) {
		SensorCall();while (tmp)
		    ary[--tmp] = &PL_sv_undef;
	    }
	    SensorCall();if (key > AvMAX(av) - 10) {
		SensorCall();newmax = key + AvMAX(av);
		SensorCall();goto resize;
	    }
	}
	else {
#ifdef PERL_MALLOC_WRAP
	    SensorCall();static const char oom_array_extend[] =
	      "Out of memory during array extend"; /* Duplicated in pp_hot.c */
#endif

	    SensorCall();if (AvALLOC(av)) {
#if !defined(STRANGE_MALLOC) && !defined(MYMALLOC)
		MEM_SIZE bytes;
		SensorCall();IV itmp;
#endif

#ifdef Perl_safesysmalloc_size
		/* Whilst it would be quite possible to move this logic around
		   (as I did in the SV code), so as to set AvMAX(av) early,
		   based on calling Perl_safesysmalloc_size() immediately after
		   allocation, I'm not convinced that it is a great idea here.
		   In an array we have to loop round setting everything to
		   &PL_sv_undef, which means writing to memory, potentially lots
		   of it, whereas for the SV buffer case we don't touch the
		   "bonus" memory. So there there is no cost in telling the
		   world about it, whereas here we have to do work before we can
		   tell the world about it, and that work involves writing to
		   memory that might never be read. So, I feel, better to keep
		   the current lazy system of only writing to it if our caller
		   has a need for more space. NWC  */
		newmax = Perl_safesysmalloc_size((void*)AvALLOC(av)) /
		    sizeof(const SV *) - 1;

		if (key <= newmax) 
		    goto resized;
#endif 
		newmax = key + AvMAX(av) / 5;
	      resize:
		MEM_WRAP_CHECK_1(newmax+1, SV*, oom_array_extend);
#if defined(STRANGE_MALLOC) || defined(MYMALLOC)
		Renew(AvALLOC(av),newmax+1, SV*);
#else
		bytes = (newmax + 1) * sizeof(const SV *);
#define MALLOC_OVERHEAD 16
		itmp = MALLOC_OVERHEAD;
		SensorCall();while ((MEM_SIZE)(itmp - MALLOC_OVERHEAD) < bytes)
		    {/*11*/SensorCall();itmp += itmp;/*12*/}
		SensorCall();itmp -= MALLOC_OVERHEAD;
		itmp /= sizeof(const SV *);
		assert(itmp > newmax);
		newmax = itmp - 1;
		assert(newmax >= AvMAX(av));
		Newx(ary, newmax+1, SV*);
		Copy(AvALLOC(av), ary, AvMAX(av)+1, SV*);
		Safefree(AvALLOC(av));
		AvALLOC(av) = ary;
#endif
#ifdef Perl_safesysmalloc_size
	      resized:
#endif
		ary = AvALLOC(av) + AvMAX(av) + 1;
		tmp = newmax - AvMAX(av);
		SensorCall();if (av == PL_curstack) {	/* Oops, grew stack (via av_store()?) */
		    PL_stack_sp = AvALLOC(av) + (PL_stack_sp - PL_stack_base);
		    PL_stack_base = AvALLOC(av);
		    PL_stack_max = PL_stack_base + newmax;
		}
	    }
	    else {
		SensorCall();newmax = key < 3 ? 3 : key;
		MEM_WRAP_CHECK_1(newmax+1, SV*, oom_array_extend);
		Newx(AvALLOC(av), newmax+1, SV*);
		ary = AvALLOC(av) + 1;
		tmp = newmax;
		AvALLOC(av)[0] = &PL_sv_undef;	/* For the stacks */
	    }
	    SensorCall();if (AvREAL(av)) {
		SensorCall();while (tmp)
		    ary[--tmp] = &PL_sv_undef;
	    }
	    
	    AvARRAY(av) = AvALLOC(av);
	    AvMAX(av) = newmax;
	}
    }
SensorCall();}

/*
=for apidoc av_fetch

Returns the SV at the specified index in the array.  The C<key> is the
index.  If lval is true, you are guaranteed to get a real SV back (in case
it wasn't real before), which you can then modify.  Check that the return
value is non-null before dereferencing it to a C<SV*>.

See L<perlguts/"Understanding the Magic of Tied Hashes and Arrays"> for
more information on how to use this function on tied arrays. 

The rough perl equivalent is C<$myarray[$idx]>.

=cut
*/

SV**
Perl_av_fetch(pTHX_ register AV *av, I32 key, I32 lval)
{
SensorCall();    dVAR;

    PERL_ARGS_ASSERT_AV_FETCH;
    assert(SvTYPE(av) == SVt_PVAV);

    SensorCall();if (SvRMAGICAL(av)) {
        SensorCall();const MAGIC * const tied_magic
	    = mg_find((const SV *)av, PERL_MAGIC_tied);
        SensorCall();if (tied_magic || mg_find((const SV *)av, PERL_MAGIC_regdata)) {
	    SensorCall();SV *sv;
	    SensorCall();if (key < 0) {
		SensorCall();I32 adjust_index = 1;
		SensorCall();if (tied_magic) {
		    /* Handle negative array indices 20020222 MJD */
		    SensorCall();SV * const * const negative_indices_glob =
			hv_fetch(SvSTASH(SvRV(SvTIED_obj(MUTABLE_SV(av),
							 tied_magic))),
				NEGATIVE_INDICES_VAR, 16, 0);

		    SensorCall();if (negative_indices_glob && SvTRUE(GvSV(*negative_indices_glob)))
			{/*13*/SensorCall();adjust_index = 0;/*14*/}
		}

		SensorCall();if (adjust_index) {
		    SensorCall();key += AvFILL(av) + 1;
		    SensorCall();if (key < 0)
			return NULL;
		}
	    }

            SensorCall();sv = sv_newmortal();
	    sv_upgrade(sv, SVt_PVLV);
	    mg_copy(MUTABLE_SV(av), sv, 0, key);
	    SensorCall();if (!tied_magic) /* for regdata, force leavesub to make copies */
		SvTEMP_off(sv);
	    LvTYPE(sv) = 't';
	    LvTARG(sv) = sv; /* fake (SV**) */
	    {SV ** ReplaceReturn = &(LvTARG(sv)); SensorCall(); return ReplaceReturn;}
        }
    }

    SensorCall();if (key < 0) {
	SensorCall();key += AvFILL(av) + 1;
	SensorCall();if (key < 0)
	    return NULL;
    }

    SensorCall();if (key > AvFILLp(av) || AvARRAY(av)[key] == &PL_sv_undef) {
      emptyness:
	{SV ** ReplaceReturn = lval ? av_store(av,key,newSV(0)) : NULL; SensorCall(); return ReplaceReturn;}
    }

    SensorCall();if (AvREIFY(av)
	     && (!AvARRAY(av)[key]	/* eg. @_ could have freed elts */
		 || SvIS_FREED(AvARRAY(av)[key]))) {
	AvARRAY(av)[key] = &PL_sv_undef;	/* 1/2 reify */
	SensorCall();goto emptyness;
    }
    {SV ** ReplaceReturn = &AvARRAY(av)[key]; SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc av_store

Stores an SV in an array.  The array index is specified as C<key>.  The
return value will be NULL if the operation failed or if the value did not
need to be actually stored within the array (as in the case of tied
arrays). Otherwise, it can be dereferenced
to get the C<SV*> that was stored
there (= C<val>)).

Note that the caller is responsible for suitably incrementing the reference
count of C<val> before the call, and decrementing it if the function
returned NULL.

Approximate Perl equivalent: C<$myarray[$key] = $val;>.

See L<perlguts/"Understanding the Magic of Tied Hashes and Arrays"> for
more information on how to use this function on tied arrays.

=cut
*/

SV**
Perl_av_store(pTHX_ register AV *av, I32 key, SV *val)
{
SensorCall();    dVAR;
    SV** ary;

    PERL_ARGS_ASSERT_AV_STORE;
    assert(SvTYPE(av) == SVt_PVAV);

    /* S_regclass relies on being able to pass in a NULL sv
       (unicode_alternate may be NULL).
    */

    SensorCall();if (!val)
	val = &PL_sv_undef;

    SensorCall();if (SvRMAGICAL(av)) {
        SensorCall();const MAGIC * const tied_magic = mg_find((const SV *)av, PERL_MAGIC_tied);
        SensorCall();if (tied_magic) {
            /* Handle negative array indices 20020222 MJD */
            SensorCall();if (key < 0) {
		bool adjust_index = 1;
		SensorCall();SV * const * const negative_indices_glob =
                    hv_fetch(SvSTASH(SvRV(SvTIED_obj(MUTABLE_SV(av), 
                                                     tied_magic))), 
                             NEGATIVE_INDICES_VAR, 16, 0);
                SensorCall();if (negative_indices_glob
                    && SvTRUE(GvSV(*negative_indices_glob)))
                    {/*25*/SensorCall();adjust_index = 0;/*26*/}
                SensorCall();if (adjust_index) {
                    SensorCall();key += AvFILL(av) + 1;
                    SensorCall();if (key < 0)
                        {/*27*/{SV ** ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*28*/}
                }
            }
	    SensorCall();if (val != &PL_sv_undef) {
		mg_copy(MUTABLE_SV(av), val, 0, key);
	    }
	    {SV ** ReplaceReturn = NULL; SensorCall(); return ReplaceReturn;}
        }
    }


    SensorCall();if (key < 0) {
	SensorCall();key += AvFILL(av) + 1;
	SensorCall();if (key < 0)
	    return NULL;
    }

    SensorCall();if (SvREADONLY(av) && key >= AvFILL(av))
	{/*29*/SensorCall();Perl_croak_no_modify(aTHX);/*30*/}

    SensorCall();if (!AvREAL(av) && AvREIFY(av))
	av_reify(av);
    SensorCall();if (key > AvMAX(av))
	av_extend(av,key);
    SensorCall();ary = AvARRAY(av);
    SensorCall();if (AvFILLp(av) < key) {
	SensorCall();if (!AvREAL(av)) {
	    SensorCall();if (av == PL_curstack && key > PL_stack_sp - PL_stack_base)
		PL_stack_sp = PL_stack_base + key;	/* XPUSH in disguise */
	    SensorCall();do {
		SensorCall();ary[++AvFILLp(av)] = &PL_sv_undef;
	    } while (AvFILLp(av) < key);
	}
	AvFILLp(av) = key;
    }
    else if (AvREAL(av))
	SvREFCNT_dec(ary[key]);
    SensorCall();ary[key] = val;
    SensorCall();if (SvSMAGICAL(av)) {
	SensorCall();const MAGIC *mg = SvMAGIC(av);
	bool set = TRUE;
	SensorCall();for (; mg; mg = mg->mg_moremagic) {
	  SensorCall();if (!isUPPER(mg->mg_type)) {/*31*/SensorCall();continue;/*32*/}
	  SensorCall();if (val != &PL_sv_undef) {
	    sv_magic(val, MUTABLE_SV(av), toLOWER(mg->mg_type), 0, key);
	  }
	  SensorCall();if (PL_delaymagic && mg->mg_type == PERL_MAGIC_isa) {
	    PL_delaymagic |= DM_ARRAY_ISA;
	    SensorCall();set = FALSE;
	  }
	}
	SensorCall();if (set)
	   mg_set(MUTABLE_SV(av));
    }
    {SV ** ReplaceReturn = &ary[key]; SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc av_make

Creates a new AV and populates it with a list of SVs.  The SVs are copied
into the array, so they may be freed after the call to av_make.  The new AV
will have a reference count of 1.

Perl equivalent: C<my @new_array = ($scalar1, $scalar2, $scalar3...);>

=cut
*/

AV *
Perl_av_make(pTHX_ register I32 size, register SV **strp)
{
    SensorCall();register AV * const av = MUTABLE_AV(newSV_type(SVt_PVAV));
    /* sv_upgrade does AvREAL_only()  */
    PERL_ARGS_ASSERT_AV_MAKE;
    assert(SvTYPE(av) == SVt_PVAV);

    SensorCall();if (size) {		/* "defined" was returning undef for size==0 anyway. */
        SensorCall();register SV** ary;
        register I32 i;
	Newx(ary,size,SV*);
	AvALLOC(av) = ary;
	AvARRAY(av) = ary;
	AvFILLp(av) = AvMAX(av) = size - 1;
	SensorCall();for (i = 0; i < size; i++) {
	    assert (*strp);

	    /* Don't let sv_setsv swipe, since our source array might
	       have multiple references to the same temp scalar (e.g.
	       from a list slice) */

	    SensorCall();ary[i] = newSV(0);
	    sv_setsv_flags(ary[i], *strp,
			   SV_GMAGIC|SV_DO_COW_SVSETSV|SV_NOSTEAL);
	    strp++;
	}
    }
    {AV * ReplaceReturn = av; SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc av_clear

Clears an array, making it empty.  Does not free the memory the av uses to
store its list of scalars.  If any destructors are triggered as a result,
the av itself may be freed when this function returns.

Perl equivalent: C<@myarray = ();>.

=cut
*/

void
Perl_av_clear(pTHX_ register AV *av)
{
SensorCall();    dVAR;
    I32 extra;
    bool real;

    PERL_ARGS_ASSERT_AV_CLEAR;
    assert(SvTYPE(av) == SVt_PVAV);

#ifdef DEBUGGING
    if (SvREFCNT(av) == 0) {
	Perl_ck_warner_d(aTHX_ packWARN(WARN_DEBUGGING), "Attempt to clear deleted array");
    }
#endif

    SensorCall();if (SvREADONLY(av))
	{/*1*/SensorCall();Perl_croak_no_modify(aTHX);/*2*/}

    /* Give any tie a chance to cleanup first */
    SensorCall();if (SvRMAGICAL(av)) {
	SensorCall();const MAGIC* const mg = SvMAGIC(av);
	SensorCall();if (PL_delaymagic && mg && mg->mg_type == PERL_MAGIC_isa)
	    PL_delaymagic |= DM_ARRAY_ISA;
        else
	    mg_clear(MUTABLE_SV(av)); 
    }

    SensorCall();if (AvMAX(av) < 0)
	{/*3*/SensorCall();return;/*4*/}

    SensorCall();if ((real = !!AvREAL(av))) {
	SensorCall();SV** const ary = AvARRAY(av);
	I32 index = AvFILLp(av) + 1;
	ENTER;
	SAVEFREESV(SvREFCNT_inc_simple_NN(av));
	SensorCall();while (index) {
	    SensorCall();SV * const sv = ary[--index];
	    /* undef the slot before freeing the value, because a
	     * destructor might try to modify this array */
	    ary[index] = &PL_sv_undef;
	    SvREFCNT_dec(sv);
	}
    }
    SensorCall();extra = AvARRAY(av) - AvALLOC(av);
    SensorCall();if (extra) {
	AvMAX(av) += extra;
	AvARRAY(av) = AvALLOC(av);
    }
    AvFILLp(av) = -1;
    SensorCall();if (real) LEAVE;
SensorCall();}

/*
=for apidoc av_undef

Undefines the array.  Frees the memory used by the av to store its list of
scalars.  If any destructors are triggered as a result, the av itself may
be freed.

=cut
*/

void
Perl_av_undef(pTHX_ register AV *av)
{
SensorCall();    bool real;

    PERL_ARGS_ASSERT_AV_UNDEF;
    assert(SvTYPE(av) == SVt_PVAV);

    /* Give any tie a chance to cleanup first */
    SensorCall();if (SvTIED_mg((const SV *)av, PERL_MAGIC_tied)) 
	av_fill(av, -1);

    SensorCall();if ((real = !!AvREAL(av))) {
	SensorCall();register I32 key = AvFILLp(av) + 1;
	ENTER;
	SAVEFREESV(SvREFCNT_inc_simple_NN(av));
	SensorCall();while (key)
	    SvREFCNT_dec(AvARRAY(av)[--key]);
    }

    Safefree(AvALLOC(av));
    AvALLOC(av) = NULL;
    AvARRAY(av) = NULL;
    AvMAX(av) = AvFILLp(av) = -1;

    SensorCall();if(SvRMAGICAL(av)) mg_clear(MUTABLE_SV(av));
    SensorCall();if(real) LEAVE;
SensorCall();}

/*

=for apidoc av_create_and_push

Push an SV onto the end of the array, creating the array if necessary.
A small internal helper function to remove a commonly duplicated idiom.

=cut
*/

void
Perl_av_create_and_push(pTHX_ AV **const avp, SV *const val)
{
    PERL_ARGS_ASSERT_AV_CREATE_AND_PUSH;

    SensorCall();if (!*avp)
	*avp = newAV();
    av_push(*avp, val);
}

/*
=for apidoc av_push

Pushes an SV onto the end of the array.  The array will grow automatically
to accommodate the addition.  This takes ownership of one reference count.

Perl equivalent: C<push @myarray, $elem;>.

=cut
*/

void
Perl_av_push(pTHX_ register AV *av, SV *val)
{SensorCall();             
    dVAR;
    MAGIC *mg;

    PERL_ARGS_ASSERT_AV_PUSH;
    assert(SvTYPE(av) == SVt_PVAV);

    SensorCall();if (SvREADONLY(av))
	{/*19*/SensorCall();Perl_croak_no_modify(aTHX);/*20*/}

    SensorCall();if ((mg = SvTIED_mg((const SV *)av, PERL_MAGIC_tied))) {
	SensorCall();Perl_magic_methcall(aTHX_ MUTABLE_SV(av), mg, "PUSH", G_DISCARD, 1,
			    val);
	SensorCall();return;
    }
    av_store(av,AvFILLp(av)+1,val);
}

/*
=for apidoc av_pop

Pops an SV off the end of the array.  Returns C<&PL_sv_undef> if the array
is empty.

Perl equivalent: C<pop(@myarray);>

=cut
*/

SV *
Perl_av_pop(pTHX_ register AV *av)
{
SensorCall();    dVAR;
    SV *retval;
    MAGIC* mg;

    PERL_ARGS_ASSERT_AV_POP;
    assert(SvTYPE(av) == SVt_PVAV);

    SensorCall();if (SvREADONLY(av))
	{/*17*/SensorCall();Perl_croak_no_modify(aTHX);/*18*/}
    SensorCall();if ((mg = SvTIED_mg((const SV *)av, PERL_MAGIC_tied))) {
	SensorCall();retval = Perl_magic_methcall(aTHX_ MUTABLE_SV(av), mg, "POP", 0, 0);
	SensorCall();if (retval)
	    retval = newSVsv(retval);
	{SV * ReplaceReturn = retval; SensorCall(); return ReplaceReturn;}
    }
    SensorCall();if (AvFILL(av) < 0)
	return &PL_sv_undef;
    SensorCall();retval = AvARRAY(av)[AvFILLp(av)];
    AvARRAY(av)[AvFILLp(av)--] = &PL_sv_undef;
    SensorCall();if (SvSMAGICAL(av))
	mg_set(MUTABLE_SV(av));
    {SV * ReplaceReturn = retval; SensorCall(); return ReplaceReturn;}
}

/*

=for apidoc av_create_and_unshift_one

Unshifts an SV onto the beginning of the array, creating the array if
necessary.
A small internal helper function to remove a commonly duplicated idiom.

=cut
*/

SV **
Perl_av_create_and_unshift_one(pTHX_ AV **const avp, SV *const val)
{
    PERL_ARGS_ASSERT_AV_CREATE_AND_UNSHIFT_ONE;

    SensorCall();if (!*avp)
	*avp = newAV();
    av_unshift(*avp, 1);
    {SV ** ReplaceReturn = av_store(*avp, 0, val); SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc av_unshift

Unshift the given number of C<undef> values onto the beginning of the
array.  The array will grow automatically to accommodate the addition.  You
must then use C<av_store> to assign values to these new elements.

Perl equivalent: C<unshift @myarray, ( (undef) x $n );>
    
=cut
*/

void
Perl_av_unshift(pTHX_ register AV *av, register I32 num)
{
SensorCall();    dVAR;
    register I32 i;
    MAGIC* mg;

    PERL_ARGS_ASSERT_AV_UNSHIFT;
    assert(SvTYPE(av) == SVt_PVAV);

    SensorCall();if (SvREADONLY(av))
	{/*33*/SensorCall();Perl_croak_no_modify(aTHX);/*34*/}

    SensorCall();if ((mg = SvTIED_mg((const SV *)av, PERL_MAGIC_tied))) {
	SensorCall();Perl_magic_methcall(aTHX_ MUTABLE_SV(av), mg, "UNSHIFT",
			    G_DISCARD | G_UNDEF_FILL, num);
	SensorCall();return;
    }

    SensorCall();if (num <= 0)
      {/*35*/SensorCall();return;/*36*/}
    SensorCall();if (!AvREAL(av) && AvREIFY(av))
	av_reify(av);
    SensorCall();i = AvARRAY(av) - AvALLOC(av);
    SensorCall();if (i) {
	SensorCall();if (i > num)
	    {/*37*/SensorCall();i = num;/*38*/}
	SensorCall();num -= i;
    
	AvMAX(av) += i;
	AvFILLp(av) += i;
	AvARRAY(av) = AvARRAY(av) - i;
    }
    SensorCall();if (num) {
	SensorCall();register SV **ary;
	const I32 i = AvFILLp(av);
	/* Create extra elements */
	const I32 slide = i > 0 ? i : 0;
	num += slide;
	av_extend(av, i + num);
	AvFILLp(av) += num;
	ary = AvARRAY(av);
	Move(ary, ary + num, i + 1, SV*);
	SensorCall();do {
	    SensorCall();ary[--num] = &PL_sv_undef;
	} while (num);
	/* Make extra elements into a buffer */
	AvMAX(av) -= slide;
	AvFILLp(av) -= slide;
	AvARRAY(av) = AvARRAY(av) + slide;
    }
SensorCall();}

/*
=for apidoc av_shift

Shifts an SV off the beginning of the
array.  Returns C<&PL_sv_undef> if the 
array is empty.

Perl equivalent: C<shift(@myarray);>

=cut
*/

SV *
Perl_av_shift(pTHX_ register AV *av)
{
SensorCall();    dVAR;
    SV *retval;
    MAGIC* mg;

    PERL_ARGS_ASSERT_AV_SHIFT;
    assert(SvTYPE(av) == SVt_PVAV);

    SensorCall();if (SvREADONLY(av))
	{/*23*/SensorCall();Perl_croak_no_modify(aTHX);/*24*/}
    SensorCall();if ((mg = SvTIED_mg((const SV *)av, PERL_MAGIC_tied))) {
	SensorCall();retval = Perl_magic_methcall(aTHX_ MUTABLE_SV(av), mg, "SHIFT", 0, 0);
	SensorCall();if (retval)
	    retval = newSVsv(retval);
	{SV * ReplaceReturn = retval; SensorCall(); return ReplaceReturn;}
    }
    SensorCall();if (AvFILL(av) < 0)
      return &PL_sv_undef;
    SensorCall();retval = *AvARRAY(av);
    SensorCall();if (AvREAL(av))
	*AvARRAY(av) = &PL_sv_undef;
    AvARRAY(av) = AvARRAY(av) + 1;
    AvMAX(av)--;
    AvFILLp(av)--;
    SensorCall();if (SvSMAGICAL(av))
	mg_set(MUTABLE_SV(av));
    {SV * ReplaceReturn = retval; SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc av_len

Returns the highest index in the array.  The number of elements in the
array is C<av_len(av) + 1>.  Returns -1 if the array is empty.

The Perl equivalent for this is C<$#myarray>.

=cut
*/

I32
Perl_av_len(pTHX_ AV *av)
{
    PERL_ARGS_ASSERT_AV_LEN;
    assert(SvTYPE(av) == SVt_PVAV);

    {I32  ReplaceReturn = AvFILL(av); SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc av_fill

Set the highest index in the array to the given number, equivalent to
Perl's C<$#array = $fill;>.

The number of elements in the an array will be C<fill + 1> after
av_fill() returns.  If the array was previously shorter, then the
additional elements appended are set to C<PL_sv_undef>.  If the array
was longer, then the excess elements are freed.  C<av_fill(av, -1)> is
the same as C<av_clear(av)>.

=cut
*/
void
Perl_av_fill(pTHX_ register AV *av, I32 fill)
{
SensorCall();    dVAR;
    MAGIC *mg;

    PERL_ARGS_ASSERT_AV_FILL;
    assert(SvTYPE(av) == SVt_PVAV);

    SensorCall();if (fill < 0)
	{/*15*/SensorCall();fill = -1;/*16*/}
    SensorCall();if ((mg = SvTIED_mg((const SV *)av, PERL_MAGIC_tied))) {
	SensorCall();SV *arg1 = sv_newmortal();
	sv_setiv(arg1, (IV)(fill + 1));
	Perl_magic_methcall(aTHX_ MUTABLE_SV(av), mg, "STORESIZE", G_DISCARD,
			    1, arg1);
	SensorCall();return;
    }
    SensorCall();if (fill <= AvMAX(av)) {
	SensorCall();I32 key = AvFILLp(av);
	SV** const ary = AvARRAY(av);

	SensorCall();if (AvREAL(av)) {
	    SensorCall();while (key > fill) {
		SvREFCNT_dec(ary[key]);
		SensorCall();ary[key--] = &PL_sv_undef;
	    }
	}
	else {
	    SensorCall();while (key < fill)
		ary[++key] = &PL_sv_undef;
	}
	    
	AvFILLp(av) = fill;
	SensorCall();if (SvSMAGICAL(av))
	    mg_set(MUTABLE_SV(av));
    }
    else
	(void)av_store(av,fill,&PL_sv_undef);
SensorCall();}

/*
=for apidoc av_delete

Deletes the element indexed by C<key> from the array, makes the element mortal,
and returns it.  If C<flags> equals C<G_DISCARD>, the element is freed and null
is returned.  Perl equivalent: C<my $elem = delete($myarray[$idx]);> for the
non-C<G_DISCARD> version and a void-context C<delete($myarray[$idx]);> for the
C<G_DISCARD> version.

=cut
*/
SV *
Perl_av_delete(pTHX_ AV *av, I32 key, I32 flags)
{
SensorCall();    dVAR;
    SV *sv;

    PERL_ARGS_ASSERT_AV_DELETE;
    assert(SvTYPE(av) == SVt_PVAV);

    SensorCall();if (SvREADONLY(av))
	{/*5*/SensorCall();Perl_croak_no_modify(aTHX);/*6*/}

    SensorCall();if (SvRMAGICAL(av)) {
        SensorCall();const MAGIC * const tied_magic
	    = mg_find((const SV *)av, PERL_MAGIC_tied);
        SensorCall();if ((tied_magic || mg_find((const SV *)av, PERL_MAGIC_regdata))) {
            /* Handle negative array indices 20020222 MJD */
            SensorCall();SV **svp;
            SensorCall();if (key < 0) {
                SensorCall();unsigned adjust_index = 1;
                SensorCall();if (tied_magic) {
		    SensorCall();SV * const * const negative_indices_glob =
                        hv_fetch(SvSTASH(SvRV(SvTIED_obj(MUTABLE_SV(av), 
                                                         tied_magic))), 
                                 NEGATIVE_INDICES_VAR, 16, 0);
                    SensorCall();if (negative_indices_glob
                        && SvTRUE(GvSV(*negative_indices_glob)))
                        {/*7*/SensorCall();adjust_index = 0;/*8*/}
                }
                SensorCall();if (adjust_index) {
                    SensorCall();key += AvFILL(av) + 1;
                    SensorCall();if (key < 0)
			return NULL;
                }
            }
            SensorCall();svp = av_fetch(av, key, TRUE);
            SensorCall();if (svp) {
                SensorCall();sv = *svp;
                mg_clear(sv);
                SensorCall();if (mg_find(sv, PERL_MAGIC_tiedelem)) {
                    sv_unmagic(sv, PERL_MAGIC_tiedelem); /* No longer an element */
                    {SV * ReplaceReturn = sv; SensorCall(); return ReplaceReturn;}
                }
		{SV * ReplaceReturn = NULL; SensorCall(); return ReplaceReturn;}
            }
        }
    }

    SensorCall();if (key < 0) {
	SensorCall();key += AvFILL(av) + 1;
	SensorCall();if (key < 0)
	    return NULL;
    }

    SensorCall();if (key > AvFILLp(av))
	return NULL;
    else {
	SensorCall();if (!AvREAL(av) && AvREIFY(av))
	    av_reify(av);
	SensorCall();sv = AvARRAY(av)[key];
	SensorCall();if (key == AvFILLp(av)) {
	    AvARRAY(av)[key] = &PL_sv_undef;
	    SensorCall();do {
		AvFILLp(av)--;
	    } while (--key >= 0 && AvARRAY(av)[key] == &PL_sv_undef);
	}
	else
	    AvARRAY(av)[key] = &PL_sv_undef;
	SensorCall();if (SvSMAGICAL(av))
	    mg_set(MUTABLE_SV(av));
    }
    SensorCall();if (flags & G_DISCARD) {
	SvREFCNT_dec(sv);
	SensorCall();sv = NULL;
    }
    else if (AvREAL(av))
	sv = sv_2mortal(sv);
    {SV * ReplaceReturn = sv; SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc av_exists

Returns true if the element indexed by C<key> has been initialized.

This relies on the fact that uninitialized array elements are set to
C<&PL_sv_undef>.

Perl equivalent: C<exists($myarray[$key])>.

=cut
*/
bool
Perl_av_exists(pTHX_ AV *av, I32 key)
{
SensorCall();    dVAR;
    PERL_ARGS_ASSERT_AV_EXISTS;
    assert(SvTYPE(av) == SVt_PVAV);

    SensorCall();if (SvRMAGICAL(av)) {
        SensorCall();const MAGIC * const tied_magic
	    = mg_find((const SV *)av, PERL_MAGIC_tied);
        const MAGIC * const regdata_magic
            = mg_find((const SV *)av, PERL_MAGIC_regdata);
        SensorCall();if (tied_magic || regdata_magic) {
	    SensorCall();SV * const sv = sv_newmortal();
            MAGIC *mg;
            /* Handle negative array indices 20020222 MJD */
            SensorCall();if (key < 0) {
                SensorCall();unsigned adjust_index = 1;
                SensorCall();if (tied_magic) {
		    SensorCall();SV * const * const negative_indices_glob =
                        hv_fetch(SvSTASH(SvRV(SvTIED_obj(MUTABLE_SV(av), 
                                                         tied_magic))), 
                                 NEGATIVE_INDICES_VAR, 16, 0);
                    SensorCall();if (negative_indices_glob
                        && SvTRUE(GvSV(*negative_indices_glob)))
                        {/*9*/SensorCall();adjust_index = 0;/*10*/}
                }
                SensorCall();if (adjust_index) {
                    SensorCall();key += AvFILL(av) + 1;
                    SensorCall();if (key < 0)
                        return FALSE;
                    else
                        return TRUE;
                }
            }

            SensorCall();if(key >= 0 && regdata_magic) {
                SensorCall();if (key <= AvFILL(av))
                    return TRUE;
                else
                    return FALSE;
            }

            mg_copy(MUTABLE_SV(av), sv, 0, key);
            SensorCall();mg = mg_find(sv, PERL_MAGIC_tiedelem);
            SensorCall();if (mg) {
                magic_existspack(sv, mg);
                {_Bool  ReplaceReturn = cBOOL(SvTRUE(sv)); SensorCall(); return ReplaceReturn;}
            }

        }
    }

    SensorCall();if (key < 0) {
	SensorCall();key += AvFILL(av) + 1;
	SensorCall();if (key < 0)
	    return FALSE;
    }

    SensorCall();if (key <= AvFILLp(av) && AvARRAY(av)[key] != &PL_sv_undef
	&& AvARRAY(av)[key])
    {
	{_Bool  ReplaceReturn = TRUE; SensorCall(); return ReplaceReturn;}
    }
    else
	return FALSE;
SensorCall();}

static MAGIC *
S_get_aux_mg(pTHX_ AV *av) {
SensorCall();    dVAR;
    MAGIC *mg;

    PERL_ARGS_ASSERT_GET_AUX_MG;
    assert(SvTYPE(av) == SVt_PVAV);

    mg = mg_find((const SV *)av, PERL_MAGIC_arylen_p);

    SensorCall();if (!mg) {
	SensorCall();mg = sv_magicext(MUTABLE_SV(av), 0, PERL_MAGIC_arylen_p,
			 &PL_vtbl_arylen_p, 0, 0);
	assert(mg);
	/* sv_magicext won't set this for us because we pass in a NULL obj  */
	mg->mg_flags |= MGf_REFCOUNTED;
    }
    {MAGIC * ReplaceReturn = mg; SensorCall(); return ReplaceReturn;}
}

SV **
Perl_av_arylen_p(pTHX_ AV *av) {
    SensorCall();MAGIC *const mg = get_aux_mg(av);

    PERL_ARGS_ASSERT_AV_ARYLEN_P;
    assert(SvTYPE(av) == SVt_PVAV);

    {SV ** ReplaceReturn = &(mg->mg_obj); SensorCall(); return ReplaceReturn;}
}

IV *
Perl_av_iter_p(pTHX_ AV *av) {
    SensorCall();MAGIC *const mg = get_aux_mg(av);

    PERL_ARGS_ASSERT_AV_ITER_P;
    assert(SvTYPE(av) == SVt_PVAV);

#if IVSIZE == I32SIZE
    return (IV *)&(mg->mg_len);
#else
    SensorCall();if (!mg->mg_ptr) {
	SensorCall();IV *temp;
	mg->mg_len = IVSIZE;
	Newxz(temp, 1, IV);
	mg->mg_ptr = (char *) temp;
    }
    {IV * ReplaceReturn = (IV *)mg->mg_ptr; SensorCall(); return ReplaceReturn;}
#endif
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 *
 * ex: set ts=8 sts=4 sw=4 noet:
 */
