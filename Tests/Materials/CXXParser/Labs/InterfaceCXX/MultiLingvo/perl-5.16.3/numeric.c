#include "var/tmp/sensor.h"
/*    numeric.c
 *
 *    Copyright (C) 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001,
 *    2002, 2003, 2004, 2005, 2006, 2007, 2008 by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 *
 */

/*
 * "That only makes eleven (plus one mislaid) and not fourteen,
 *  unless wizards count differently to other people."  --Beorn
 *
 *     [p.115 of _The Hobbit_: "Queer Lodgings"]
 */

/*
=head1 Numeric functions

This file contains all the stuff needed by perl for manipulating numeric
values, including such things as replacements for the OS's atof() function

=cut

*/

#include "EXTERN.h"
#define PERL_IN_NUMERIC_C
#include "perl.h"

U32
Perl_cast_ulong(pTHX_ NV f)
{
SensorCall();    PERL_UNUSED_CONTEXT;
  SensorCall();if (f < 0.0)
    {/*5*/{U32  ReplaceReturn = f < I32_MIN ? (U32) I32_MIN : (U32)(I32) f; SensorCall(); return ReplaceReturn;}/*6*/}
  SensorCall();if (f < U32_MAX_P1) {
#if CASTFLAGS & 2
    if (f < U32_MAX_P1_HALF)
      return (U32) f;
    f -= U32_MAX_P1_HALF;
    return ((U32) f) | (1 + U32_MAX >> 1);
#else
    {U32  ReplaceReturn = (U32) f; SensorCall(); return ReplaceReturn;}
#endif
  }
  {U32  ReplaceReturn = f > 0 ? U32_MAX : 0 /* NaN */; SensorCall(); return ReplaceReturn;}
}

I32
Perl_cast_i32(pTHX_ NV f)
{
SensorCall();    PERL_UNUSED_CONTEXT;
  SensorCall();if (f < I32_MAX_P1)
    {/*1*/{I32  ReplaceReturn = f < I32_MIN ? I32_MIN : (I32) f; SensorCall(); return ReplaceReturn;}/*2*/}
  SensorCall();if (f < U32_MAX_P1) {
#if CASTFLAGS & 2
    if (f < U32_MAX_P1_HALF)
      return (I32)(U32) f;
    f -= U32_MAX_P1_HALF;
    return (I32)(((U32) f) | (1 + U32_MAX >> 1));
#else
    {I32  ReplaceReturn = (I32)(U32) f; SensorCall(); return ReplaceReturn;}
#endif
  }
  {I32  ReplaceReturn = f > 0 ? (I32)U32_MAX : 0 /* NaN */; SensorCall(); return ReplaceReturn;}
}

IV
Perl_cast_iv(pTHX_ NV f)
{
SensorCall();    PERL_UNUSED_CONTEXT;
  SensorCall();if (f < IV_MAX_P1)
    {/*3*/{IV  ReplaceReturn = f < IV_MIN ? IV_MIN : (IV) f; SensorCall(); return ReplaceReturn;}/*4*/}
  SensorCall();if (f < UV_MAX_P1) {
#if CASTFLAGS & 2
    /* For future flexibility allowing for sizeof(UV) >= sizeof(IV)  */
    if (f < UV_MAX_P1_HALF)
      return (IV)(UV) f;
    f -= UV_MAX_P1_HALF;
    return (IV)(((UV) f) | (1 + UV_MAX >> 1));
#else
    {IV  ReplaceReturn = (IV)(UV) f; SensorCall(); return ReplaceReturn;}
#endif
  }
  {IV  ReplaceReturn = f > 0 ? (IV)UV_MAX : 0 /* NaN */; SensorCall(); return ReplaceReturn;}
}

UV
Perl_cast_uv(pTHX_ NV f)
{
SensorCall();    PERL_UNUSED_CONTEXT;
  SensorCall();if (f < 0.0)
    {/*7*/{UV  ReplaceReturn = f < IV_MIN ? (UV) IV_MIN : (UV)(IV) f; SensorCall(); return ReplaceReturn;}/*8*/}
  SensorCall();if (f < UV_MAX_P1) {
#if CASTFLAGS & 2
    if (f < UV_MAX_P1_HALF)
      return (UV) f;
    f -= UV_MAX_P1_HALF;
    return ((UV) f) | (1 + UV_MAX >> 1);
#else
    {UV  ReplaceReturn = (UV) f; SensorCall(); return ReplaceReturn;}
#endif
  }
  {UV  ReplaceReturn = f > 0 ? UV_MAX : 0 /* NaN */; SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc grok_bin

converts a string representing a binary number to numeric form.

On entry I<start> and I<*len> give the string to scan, I<*flags> gives
conversion flags, and I<result> should be NULL or a pointer to an NV.
The scan stops at the end of the string, or the first invalid character.
Unless C<PERL_SCAN_SILENT_ILLDIGIT> is set in I<*flags>, encountering an
invalid character will also trigger a warning.
On return I<*len> is set to the length of the scanned string,
and I<*flags> gives output flags.

If the value is <= C<UV_MAX> it is returned as a UV, the output flags are clear,
and nothing is written to I<*result>. If the value is > UV_MAX C<grok_bin>
returns UV_MAX, sets C<PERL_SCAN_GREATER_THAN_UV_MAX> in the output flags,
and writes the value to I<*result> (or the value is discarded if I<result>
is NULL).

The binary number may optionally be prefixed with "0b" or "b" unless
C<PERL_SCAN_DISALLOW_PREFIX> is set in I<*flags> on entry. If
C<PERL_SCAN_ALLOW_UNDERSCORES> is set in I<*flags> then the binary
number may use '_' characters to separate digits.

=cut

Not documented yet because experimental is C<PERL_SCAN_SILENT_NON_PORTABLE
which suppresses any message for non-portable numbers that are still valid
on this platform.
 */

UV
Perl_grok_bin(pTHX_ const char *start, STRLEN *len_p, I32 *flags, NV *result)
{
    SensorCall();const char *s = start;
    STRLEN len = *len_p;
    UV value = 0;
    NV value_nv = 0;

    const UV max_div_2 = UV_MAX / 2;
    const bool allow_underscores = cBOOL(*flags & PERL_SCAN_ALLOW_UNDERSCORES);
    bool overflowed = FALSE;
    char bit;

    PERL_ARGS_ASSERT_GROK_BIN;

    SensorCall();if (!(*flags & PERL_SCAN_DISALLOW_PREFIX)) {
        /* strip off leading b or 0b.
           for compatibility silently suffer "b" and "0b" as valid binary
           numbers. */
        SensorCall();if (len >= 1) {
            SensorCall();if (s[0] == 'b' || s[0] == 'B') {
                SensorCall();s++;
                len--;
            }
            else {/*9*/SensorCall();if (len >= 2 && s[0] == '0' && (s[1] == 'b' || s[1] == 'B')) {
                SensorCall();s+=2;
                len-=2;
            ;/*10*/}}
        }
    }

    SensorCall();for (; len-- && (bit = *s); s++) {
        SensorCall();if (bit == '0' || bit == '1') {
            /* Write it in this wonky order with a goto to attempt to get the
               compiler to make the common case integer-only loop pretty tight.
               With gcc seems to be much straighter code than old scan_bin.  */
          redo:
            SensorCall();if (!overflowed) {
                SensorCall();if (value <= max_div_2) {
                    SensorCall();value = (value << 1) | (bit - '0');
                    SensorCall();continue;
                }
                /* Bah. We're just overflowed.  */
		/* diag_listed_as: Integer overflow in %s number */
		SensorCall();Perl_ck_warner_d(aTHX_ packWARN(WARN_OVERFLOW),
				 "Integer overflow in binary number");
                overflowed = TRUE;
                value_nv = (NV) value;
            }
            SensorCall();value_nv *= 2.0;
	    /* If an NV has not enough bits in its mantissa to
	     * represent a UV this summing of small low-order numbers
	     * is a waste of time (because the NV cannot preserve
	     * the low-order bits anyway): we could just remember when
	     * did we overflow and in the end just multiply value_nv by the
	     * right amount. */
            value_nv += (NV)(bit - '0');
            SensorCall();continue;
        }
        SensorCall();if (bit == '_' && len && allow_underscores && (bit = s[1])
            && (bit == '0' || bit == '1'))
	    {
		SensorCall();--len;
		++s;
                SensorCall();goto redo;
	    }
        SensorCall();if (!(*flags & PERL_SCAN_SILENT_ILLDIGIT))
            {/*11*/SensorCall();Perl_ck_warner(aTHX_ packWARN(WARN_DIGIT),
			   "Illegal binary digit '%c' ignored", *s);/*12*/}
        SensorCall();break;
    }
    
    SensorCall();if (   ( overflowed && value_nv > 4294967295.0)
#if UVSIZE > 4
	|| (!overflowed && value > 0xffffffff
	    && ! (*flags & PERL_SCAN_SILENT_NON_PORTABLE))
#endif
	) {
	SensorCall();Perl_ck_warner(aTHX_ packWARN(WARN_PORTABLE),
		       "Binary number > 0b11111111111111111111111111111111 non-portable");
    }
    SensorCall();*len_p = s - start;
    SensorCall();if (!overflowed) {
        SensorCall();*flags = 0;
        {UV  ReplaceReturn = value; SensorCall(); return ReplaceReturn;}
    }
    SensorCall();*flags = PERL_SCAN_GREATER_THAN_UV_MAX;
    SensorCall();if (result)
        {/*13*/SensorCall();*result = value_nv;/*14*/}
    {UV  ReplaceReturn = UV_MAX; SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc grok_hex

converts a string representing a hex number to numeric form.

On entry I<start> and I<*len> give the string to scan, I<*flags> gives
conversion flags, and I<result> should be NULL or a pointer to an NV.
The scan stops at the end of the string, or the first invalid character.
Unless C<PERL_SCAN_SILENT_ILLDIGIT> is set in I<*flags>, encountering an
invalid character will also trigger a warning.
On return I<*len> is set to the length of the scanned string,
and I<*flags> gives output flags.

If the value is <= UV_MAX it is returned as a UV, the output flags are clear,
and nothing is written to I<*result>. If the value is > UV_MAX C<grok_hex>
returns UV_MAX, sets C<PERL_SCAN_GREATER_THAN_UV_MAX> in the output flags,
and writes the value to I<*result> (or the value is discarded if I<result>
is NULL).

The hex number may optionally be prefixed with "0x" or "x" unless
C<PERL_SCAN_DISALLOW_PREFIX> is set in I<*flags> on entry. If
C<PERL_SCAN_ALLOW_UNDERSCORES> is set in I<*flags> then the hex
number may use '_' characters to separate digits.

=cut

Not documented yet because experimental is C<PERL_SCAN_SILENT_NON_PORTABLE
which suppresses any message for non-portable numbers that are still valid
on this platform.
 */

UV
Perl_grok_hex(pTHX_ const char *start, STRLEN *len_p, I32 *flags, NV *result)
{
SensorCall();    dVAR;
    const char *s = start;
    STRLEN len = *len_p;
    UV value = 0;
    NV value_nv = 0;
    const UV max_div_16 = UV_MAX / 16;
    const bool allow_underscores = cBOOL(*flags & PERL_SCAN_ALLOW_UNDERSCORES);
    bool overflowed = FALSE;

    PERL_ARGS_ASSERT_GROK_HEX;

    SensorCall();if (!(*flags & PERL_SCAN_DISALLOW_PREFIX)) {
        /* strip off leading x or 0x.
           for compatibility silently suffer "x" and "0x" as valid hex numbers.
        */
        SensorCall();if (len >= 1) {
            SensorCall();if (s[0] == 'x' || s[0] == 'X') {
                SensorCall();s++;
                len--;
            }
            else {/*15*/SensorCall();if (len >= 2 && s[0] == '0' && (s[1] == 'x' || s[1] == 'X')) {
                SensorCall();s+=2;
                len-=2;
            ;/*16*/}}
        }
    }

    SensorCall();for (; len-- && *s; s++) {
	SensorCall();const char *hexdigit = strchr(PL_hexdigit, *s);
        SensorCall();if (hexdigit) {
            /* Write it in this wonky order with a goto to attempt to get the
               compiler to make the common case integer-only loop pretty tight.
               With gcc seems to be much straighter code than old scan_hex.  */
          redo:
            SensorCall();if (!overflowed) {
                SensorCall();if (value <= max_div_16) {
                    SensorCall();value = (value << 4) | ((hexdigit - PL_hexdigit) & 15);
                    SensorCall();continue;
                }
                /* Bah. We're just overflowed.  */
		/* diag_listed_as: Integer overflow in %s number */
		SensorCall();Perl_ck_warner_d(aTHX_ packWARN(WARN_OVERFLOW),
				 "Integer overflow in hexadecimal number");
                overflowed = TRUE;
                value_nv = (NV) value;
            }
            SensorCall();value_nv *= 16.0;
	    /* If an NV has not enough bits in its mantissa to
	     * represent a UV this summing of small low-order numbers
	     * is a waste of time (because the NV cannot preserve
	     * the low-order bits anyway): we could just remember when
	     * did we overflow and in the end just multiply value_nv by the
	     * right amount of 16-tuples. */
            value_nv += (NV)((hexdigit - PL_hexdigit) & 15);
            SensorCall();continue;
        }
        SensorCall();if (*s == '_' && len && allow_underscores && s[1]
		&& (hexdigit = strchr(PL_hexdigit, s[1])))
	    {
		SensorCall();--len;
		++s;
                SensorCall();goto redo;
	    }
        SensorCall();if (!(*flags & PERL_SCAN_SILENT_ILLDIGIT))
            {/*17*/SensorCall();Perl_ck_warner(aTHX_ packWARN(WARN_DIGIT),
                        "Illegal hexadecimal digit '%c' ignored", *s);/*18*/}
        SensorCall();break;
    }
    
    SensorCall();if (   ( overflowed && value_nv > 4294967295.0)
#if UVSIZE > 4
	|| (!overflowed && value > 0xffffffff
	    && ! (*flags & PERL_SCAN_SILENT_NON_PORTABLE))
#endif
	) {
	SensorCall();Perl_ck_warner(aTHX_ packWARN(WARN_PORTABLE),
		       "Hexadecimal number > 0xffffffff non-portable");
    }
    SensorCall();*len_p = s - start;
    SensorCall();if (!overflowed) {
        SensorCall();*flags = 0;
        {UV  ReplaceReturn = value; SensorCall(); return ReplaceReturn;}
    }
    SensorCall();*flags = PERL_SCAN_GREATER_THAN_UV_MAX;
    SensorCall();if (result)
        {/*19*/SensorCall();*result = value_nv;/*20*/}
    {UV  ReplaceReturn = UV_MAX; SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc grok_oct

converts a string representing an octal number to numeric form.

On entry I<start> and I<*len> give the string to scan, I<*flags> gives
conversion flags, and I<result> should be NULL or a pointer to an NV.
The scan stops at the end of the string, or the first invalid character.
Unless C<PERL_SCAN_SILENT_ILLDIGIT> is set in I<*flags>, encountering an
8 or 9 will also trigger a warning.
On return I<*len> is set to the length of the scanned string,
and I<*flags> gives output flags.

If the value is <= UV_MAX it is returned as a UV, the output flags are clear,
and nothing is written to I<*result>. If the value is > UV_MAX C<grok_oct>
returns UV_MAX, sets C<PERL_SCAN_GREATER_THAN_UV_MAX> in the output flags,
and writes the value to I<*result> (or the value is discarded if I<result>
is NULL).

If C<PERL_SCAN_ALLOW_UNDERSCORES> is set in I<*flags> then the octal
number may use '_' characters to separate digits.

=cut

Not documented yet because experimental is C<PERL_SCAN_SILENT_NON_PORTABLE
which suppresses any message for non-portable numbers that are still valid
on this platform.
 */

UV
Perl_grok_oct(pTHX_ const char *start, STRLEN *len_p, I32 *flags, NV *result)
{
    SensorCall();const char *s = start;
    STRLEN len = *len_p;
    UV value = 0;
    NV value_nv = 0;
    const UV max_div_8 = UV_MAX / 8;
    const bool allow_underscores = cBOOL(*flags & PERL_SCAN_ALLOW_UNDERSCORES);
    bool overflowed = FALSE;

    PERL_ARGS_ASSERT_GROK_OCT;

    SensorCall();for (; len-- && *s; s++) {
         /* gcc 2.95 optimiser not smart enough to figure that this subtraction
            out front allows slicker code.  */
        SensorCall();int digit = *s - '0';
        SensorCall();if (digit >= 0 && digit <= 7) {
            /* Write it in this wonky order with a goto to attempt to get the
               compiler to make the common case integer-only loop pretty tight.
            */
          redo:
            SensorCall();if (!overflowed) {
                SensorCall();if (value <= max_div_8) {
                    SensorCall();value = (value << 3) | digit;
                    SensorCall();continue;
                }
                /* Bah. We're just overflowed.  */
		/* diag_listed_as: Integer overflow in %s number */
		SensorCall();Perl_ck_warner_d(aTHX_ packWARN(WARN_OVERFLOW),
			       "Integer overflow in octal number");
                overflowed = TRUE;
                value_nv = (NV) value;
            }
            SensorCall();value_nv *= 8.0;
	    /* If an NV has not enough bits in its mantissa to
	     * represent a UV this summing of small low-order numbers
	     * is a waste of time (because the NV cannot preserve
	     * the low-order bits anyway): we could just remember when
	     * did we overflow and in the end just multiply value_nv by the
	     * right amount of 8-tuples. */
            value_nv += (NV)digit;
            SensorCall();continue;
        }
        SensorCall();if (digit == ('_' - '0') && len && allow_underscores
            && (digit = s[1] - '0') && (digit >= 0 && digit <= 7))
	    {
		SensorCall();--len;
		++s;
                SensorCall();goto redo;
	    }
        /* Allow \octal to work the DWIM way (that is, stop scanning
         * as soon as non-octal characters are seen, complain only if
         * someone seems to want to use the digits eight and nine). */
        SensorCall();if (digit == 8 || digit == 9) {
            SensorCall();if (!(*flags & PERL_SCAN_SILENT_ILLDIGIT))
                {/*79*/SensorCall();Perl_ck_warner(aTHX_ packWARN(WARN_DIGIT),
			       "Illegal octal digit '%c' ignored", *s);/*80*/}
        }
        SensorCall();break;
    }
    
    SensorCall();if (   ( overflowed && value_nv > 4294967295.0)
#if UVSIZE > 4
	|| (!overflowed && value > 0xffffffff
	    && ! (*flags & PERL_SCAN_SILENT_NON_PORTABLE))
#endif
	) {
	SensorCall();Perl_ck_warner(aTHX_ packWARN(WARN_PORTABLE),
		       "Octal number > 037777777777 non-portable");
    }
    SensorCall();*len_p = s - start;
    SensorCall();if (!overflowed) {
        SensorCall();*flags = 0;
        {UV  ReplaceReturn = value; SensorCall(); return ReplaceReturn;}
    }
    SensorCall();*flags = PERL_SCAN_GREATER_THAN_UV_MAX;
    SensorCall();if (result)
        {/*81*/SensorCall();*result = value_nv;/*82*/}
    {UV  ReplaceReturn = UV_MAX; SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc scan_bin

For backwards compatibility. Use C<grok_bin> instead.

=for apidoc scan_hex

For backwards compatibility. Use C<grok_hex> instead.

=for apidoc scan_oct

For backwards compatibility. Use C<grok_oct> instead.

=cut
 */

NV
Perl_scan_bin(pTHX_ const char *start, STRLEN len, STRLEN *retlen)
{
    SensorCall();NV rnv;
    I32 flags = *retlen ? PERL_SCAN_ALLOW_UNDERSCORES : 0;
    const UV ruv = grok_bin (start, &len, &flags, &rnv);

    PERL_ARGS_ASSERT_SCAN_BIN;

    *retlen = len;
    {NV  ReplaceReturn = (flags & PERL_SCAN_GREATER_THAN_UV_MAX) ? rnv : (NV)ruv; SensorCall(); return ReplaceReturn;}
}

NV
Perl_scan_oct(pTHX_ const char *start, STRLEN len, STRLEN *retlen)
{
    SensorCall();NV rnv;
    I32 flags = *retlen ? PERL_SCAN_ALLOW_UNDERSCORES : 0;
    const UV ruv = grok_oct (start, &len, &flags, &rnv);

    PERL_ARGS_ASSERT_SCAN_OCT;

    *retlen = len;
    {NV  ReplaceReturn = (flags & PERL_SCAN_GREATER_THAN_UV_MAX) ? rnv : (NV)ruv; SensorCall(); return ReplaceReturn;}
}

NV
Perl_scan_hex(pTHX_ const char *start, STRLEN len, STRLEN *retlen)
{
    SensorCall();NV rnv;
    I32 flags = *retlen ? PERL_SCAN_ALLOW_UNDERSCORES : 0;
    const UV ruv = grok_hex (start, &len, &flags, &rnv);

    PERL_ARGS_ASSERT_SCAN_HEX;

    *retlen = len;
    {NV  ReplaceReturn = (flags & PERL_SCAN_GREATER_THAN_UV_MAX) ? rnv : (NV)ruv; SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc grok_numeric_radix

Scan and skip for a numeric decimal separator (radix).

=cut
 */
bool
Perl_grok_numeric_radix(pTHX_ const char **sp, const char *send)
{
SensorCall();
#ifdef USE_LOCALE_NUMERIC
    dVAR;

    PERL_ARGS_ASSERT_GROK_NUMERIC_RADIX;

    SensorCall();if (PL_numeric_radix_sv && IN_SOME_LOCALE_FORM) {
        SensorCall();STRLEN len;
        const char * const radix = SvPV(PL_numeric_radix_sv, len);
        SensorCall();if (*sp + len <= send && memEQ(*sp, radix, len)) {
            SensorCall();*sp += len;
            {_Bool  ReplaceReturn = TRUE; SensorCall(); return ReplaceReturn;} 
        }
    }
    /* always try "." if numeric radix didn't match because
     * we may have data from different locales mixed */
#endif

    PERL_ARGS_ASSERT_GROK_NUMERIC_RADIX;

    SensorCall();if (*sp < send && **sp == '.') {
        SensorCall();++*sp;
        {_Bool  ReplaceReturn = TRUE; SensorCall(); return ReplaceReturn;}
    }
    {_Bool  ReplaceReturn = FALSE; SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc grok_number

Recognise (or not) a number.  The type of the number is returned
(0 if unrecognised), otherwise it is a bit-ORed combination of
IS_NUMBER_IN_UV, IS_NUMBER_GREATER_THAN_UV_MAX, IS_NUMBER_NOT_INT,
IS_NUMBER_NEG, IS_NUMBER_INFINITY, IS_NUMBER_NAN (defined in perl.h).

If the value of the number can fit an in UV, it is returned in the *valuep
IS_NUMBER_IN_UV will be set to indicate that *valuep is valid, IS_NUMBER_IN_UV
will never be set unless *valuep is valid, but *valuep may have been assigned
to during processing even though IS_NUMBER_IN_UV is not set on return.
If valuep is NULL, IS_NUMBER_IN_UV will be set for the same cases as when
valuep is non-NULL, but no actual assignment (or SEGV) will occur.

IS_NUMBER_NOT_INT will be set with IS_NUMBER_IN_UV if trailing decimals were
seen (in which case *valuep gives the true value truncated to an integer), and
IS_NUMBER_NEG if the number is negative (in which case *valuep holds the
absolute value).  IS_NUMBER_IN_UV is not set if e notation was used or the
number is larger than a UV.

=cut
 */
int
Perl_grok_number(pTHX_ const char *pv, STRLEN len, UV *valuep)
{
  SensorCall();const char *s = pv;
  const char * const send = pv + len;
  const UV max_div_10 = UV_MAX / 10;
  const char max_mod_10 = UV_MAX % 10;
  int numtype = 0;
  int sawinf = 0;
  int sawnan = 0;

  PERL_ARGS_ASSERT_GROK_NUMBER;

  SensorCall();while (s < send && isSPACE(*s))
    {/*21*/SensorCall();s++;/*22*/}
  SensorCall();if (s == send) {
    {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
  } else {/*23*/SensorCall();if (*s == '-') {
    SensorCall();s++;
    numtype = IS_NUMBER_NEG;
  }
  else {/*25*/SensorCall();if (*s == '+')
  {/*27*/SensorCall();s++;/*28*/}/*26*/}/*24*/}

  SensorCall();if (s == send)
    {/*29*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*30*/}

  /* next must be digit or the radix separator or beginning of infinity */
  SensorCall();if (isDIGIT(*s)) {
    /* UVs are at least 32 bits, so the first 9 decimal digits cannot
       overflow.  */
    SensorCall();UV value = *s - '0';
    /* This construction seems to be more optimiser friendly.
       (without it gcc does the isDIGIT test and the *s - '0' separately)
       With it gcc on arm is managing 6 instructions (6 cycles) per digit.
       In theory the optimiser could deduce how far to unroll the loop
       before checking for overflow.  */
    SensorCall();if (++s < send) {
      SensorCall();int digit = *s - '0';
      SensorCall();if (digit >= 0 && digit <= 9) {
        SensorCall();value = value * 10 + digit;
        SensorCall();if (++s < send) {
          SensorCall();digit = *s - '0';
          SensorCall();if (digit >= 0 && digit <= 9) {
            SensorCall();value = value * 10 + digit;
            SensorCall();if (++s < send) {
              SensorCall();digit = *s - '0';
              SensorCall();if (digit >= 0 && digit <= 9) {
                SensorCall();value = value * 10 + digit;
		SensorCall();if (++s < send) {
                  SensorCall();digit = *s - '0';
                  SensorCall();if (digit >= 0 && digit <= 9) {
                    SensorCall();value = value * 10 + digit;
                    SensorCall();if (++s < send) {
                      SensorCall();digit = *s - '0';
                      SensorCall();if (digit >= 0 && digit <= 9) {
                        SensorCall();value = value * 10 + digit;
                        SensorCall();if (++s < send) {
                          SensorCall();digit = *s - '0';
                          SensorCall();if (digit >= 0 && digit <= 9) {
                            SensorCall();value = value * 10 + digit;
                            SensorCall();if (++s < send) {
                              SensorCall();digit = *s - '0';
                              SensorCall();if (digit >= 0 && digit <= 9) {
                                SensorCall();value = value * 10 + digit;
                                SensorCall();if (++s < send) {
                                  SensorCall();digit = *s - '0';
                                  SensorCall();if (digit >= 0 && digit <= 9) {
                                    SensorCall();value = value * 10 + digit;
                                    SensorCall();if (++s < send) {
                                      /* Now got 9 digits, so need to check
                                         each time for overflow.  */
                                      SensorCall();digit = *s - '0';
                                      SensorCall();while (digit >= 0 && digit <= 9
                                             && (value < max_div_10
                                                 || (value == max_div_10
                                                     && digit <= max_mod_10))) {
                                        SensorCall();value = value * 10 + digit;
                                        SensorCall();if (++s < send)
                                          {/*31*/SensorCall();digit = *s - '0';/*32*/}
                                        else
                                          {/*33*/SensorCall();break;/*34*/}
                                      }
                                      SensorCall();if (digit >= 0 && digit <= 9
                                          && (s < send)) {
                                        /* value overflowed.
                                           skip the remaining digits, don't
                                           worry about setting *valuep.  */
                                        SensorCall();do {
                                          SensorCall();s++;
                                        } while (s < send && isDIGIT(*s));
                                        SensorCall();numtype |=
                                          IS_NUMBER_GREATER_THAN_UV_MAX;
                                        SensorCall();goto skip_value;
                                      }
                                    }
                                  }
				}
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
	}
      }
    }
    SensorCall();numtype |= IS_NUMBER_IN_UV;
    SensorCall();if (valuep)
      {/*35*/SensorCall();*valuep = value;/*36*/}

  skip_value:
    SensorCall();if (GROK_NUMERIC_RADIX(&s, send)) {
      SensorCall();numtype |= IS_NUMBER_NOT_INT;
      SensorCall();while (s < send && isDIGIT(*s))  /* optional digits after the radix */
        {/*37*/SensorCall();s++;/*38*/}
    }
  }
  else {/*39*/SensorCall();if (GROK_NUMERIC_RADIX(&s, send)) {
    SensorCall();numtype |= IS_NUMBER_NOT_INT | IS_NUMBER_IN_UV; /* valuep assigned below */
    /* no digits before the radix means we need digits after it */
    SensorCall();if (s < send && isDIGIT(*s)) {
      SensorCall();do {
        SensorCall();s++;
      } while (s < send && isDIGIT(*s));
      SensorCall();if (valuep) {
        /* integer approximation is valid - it's 0.  */
        SensorCall();*valuep = 0;
      }
    }
    else
      {/*41*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*42*/}
  } else {/*43*/SensorCall();if (*s == 'I' || *s == 'i') {
    SensorCall();s++; SensorCall();if (s == send || (*s != 'N' && *s != 'n')) {/*45*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*46*/}
    SensorCall();s++; SensorCall();if (s == send || (*s != 'F' && *s != 'f')) {/*47*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*48*/}
    SensorCall();s++; SensorCall();if (s < send && (*s == 'I' || *s == 'i')) {
      SensorCall();s++; SensorCall();if (s == send || (*s != 'N' && *s != 'n')) {/*49*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*50*/}
      SensorCall();s++; SensorCall();if (s == send || (*s != 'I' && *s != 'i')) {/*51*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*52*/}
      SensorCall();s++; SensorCall();if (s == send || (*s != 'T' && *s != 't')) {/*53*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*54*/}
      SensorCall();s++; SensorCall();if (s == send || (*s != 'Y' && *s != 'y')) {/*55*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*56*/}
      SensorCall();s++;
    }
    SensorCall();sawinf = 1;
  } else {/*57*/SensorCall();if (*s == 'N' || *s == 'n') {
    /* XXX TODO: There are signaling NaNs and quiet NaNs. */
    SensorCall();s++; SensorCall();if (s == send || (*s != 'A' && *s != 'a')) {/*59*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*60*/}
    SensorCall();s++; SensorCall();if (s == send || (*s != 'N' && *s != 'n')) {/*61*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*62*/}
    SensorCall();s++;
    sawnan = 1;
  } else
    {/*63*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*64*/}/*58*/}/*44*/}/*40*/}

  SensorCall();if (sawinf) {
    SensorCall();numtype &= IS_NUMBER_NEG; /* Keep track of sign  */
    numtype |= IS_NUMBER_INFINITY | IS_NUMBER_NOT_INT;
  } else {/*65*/SensorCall();if (sawnan) {
    SensorCall();numtype &= IS_NUMBER_NEG; /* Keep track of sign  */
    numtype |= IS_NUMBER_NAN | IS_NUMBER_NOT_INT;
  } else {/*67*/SensorCall();if (s < send) {
    /* we can have an optional exponent part */
    SensorCall();if (*s == 'e' || *s == 'E') {
      /* The only flag we keep is sign.  Blow away any "it's UV"  */
      SensorCall();numtype &= IS_NUMBER_NEG;
      numtype |= IS_NUMBER_NOT_INT;
      s++;
      SensorCall();if (s < send && (*s == '-' || *s == '+'))
        {/*69*/SensorCall();s++;/*70*/}
      SensorCall();if (s < send && isDIGIT(*s)) {
        SensorCall();do {
          SensorCall();s++;
        } while (s < send && isDIGIT(*s));
      }
      else
      {/*71*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*72*/}
    }
  ;/*68*/}/*66*/}}
  SensorCall();while (s < send && isSPACE(*s))
    {/*73*/SensorCall();s++;/*74*/}
  SensorCall();if (s >= send)
    {/*75*/{int  ReplaceReturn = numtype; SensorCall(); return ReplaceReturn;}/*76*/}
  SensorCall();if (len == 10 && memEQ(pv, "0 but true", 10)) {
    SensorCall();if (valuep)
      {/*77*/SensorCall();*valuep = 0;/*78*/}
    {int  ReplaceReturn = IS_NUMBER_IN_UV; SensorCall(); return ReplaceReturn;}
  }
  {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

STATIC NV
S_mulexp10(NV value, I32 exponent)
{
    SensorCall();NV result = 1.0;
    NV power = 10.0;
    bool negative = 0;
    I32 bit;

    SensorCall();if (exponent == 0)
	{/*99*/{NV  ReplaceReturn = value; SensorCall(); return ReplaceReturn;}/*100*/}
    SensorCall();if (value == 0)
	{/*101*/{NV  ReplaceReturn = (NV)0; SensorCall(); return ReplaceReturn;}/*102*/}

    /* On OpenVMS VAX we by default use the D_FLOAT double format,
     * and that format does not have *easy* capabilities [1] for
     * overflowing doubles 'silently' as IEEE fp does.  We also need 
     * to support G_FLOAT on both VAX and Alpha, and though the exponent 
     * range is much larger than D_FLOAT it still doesn't do silent 
     * overflow.  Therefore we need to detect early whether we would 
     * overflow (this is the behaviour of the native string-to-float 
     * conversion routines, and therefore of native applications, too).
     *
     * [1] Trying to establish a condition handler to trap floating point
     *     exceptions is not a good idea. */

    /* In UNICOS and in certain Cray models (such as T90) there is no
     * IEEE fp, and no way at all from C to catch fp overflows gracefully.
     * There is something you can do if you are willing to use some
     * inline assembler: the instruction is called DFI-- but that will
     * disable *all* floating point interrupts, a little bit too large
     * a hammer.  Therefore we need to catch potential overflows before
     * it's too late. */

#if ((defined(VMS) && !defined(__IEEE_FP)) || defined(_UNICOS)) && defined(NV_MAX_10_EXP)
    STMT_START {
	const NV exp_v = log10(value);
	if (exponent >= NV_MAX_10_EXP || exponent + exp_v >= NV_MAX_10_EXP)
	    return NV_MAX;
	if (exponent < 0) {
	    if (-(exponent + exp_v) >= NV_MAX_10_EXP)
		return 0.0;
	    while (-exponent >= NV_MAX_10_EXP) {
		/* combination does not overflow, but 10^(-exponent) does */
		value /= 10;
		++exponent;
	    }
	}
    } STMT_END;
#endif

    SensorCall();if (exponent < 0) {
	SensorCall();negative = 1;
	exponent = -exponent;
    }
    SensorCall();for (bit = 1; exponent; bit <<= 1) {
	SensorCall();if (exponent & bit) {
	    SensorCall();exponent ^= bit;
	    result *= power;
	    /* Floating point exceptions are supposed to be turned off,
	     *  but if we're obviously done, don't risk another iteration.  
	     */
	     SensorCall();if (exponent == 0) {/*103*/SensorCall();break;/*104*/}
	}
	SensorCall();power *= power;
    }
    {NV  ReplaceReturn = negative ? value / result : value * result; SensorCall(); return ReplaceReturn;}
}

NV
Perl_my_atof(pTHX_ const char* s)
{
    SensorCall();NV x = 0.0;
#ifdef USE_LOCALE_NUMERIC
    dVAR;

    PERL_ARGS_ASSERT_MY_ATOF;

    SensorCall();if (PL_numeric_local && PL_numeric_radix_sv && IN_SOME_LOCALE_FORM) {
        SensorCall();char *standard = NULL, *local = NULL;
        bool use_standard_radix;

        standard = strchr(s, '.');
        local = strstr(s, SvPV_nolen(PL_numeric_radix_sv));

        use_standard_radix = standard && (!local || standard < local);

        SensorCall();if (use_standard_radix)
            SET_NUMERIC_STANDARD();

        Perl_atof2(s, x);

        SensorCall();if (use_standard_radix)
            SET_NUMERIC_LOCAL();
    }
    else
	Perl_atof2(s, x);
#else
    Perl_atof2(s, x);
#endif
    {NV  ReplaceReturn = x; SensorCall(); return ReplaceReturn;}
}

char*
Perl_my_atof2(pTHX_ const char* orig, NV* value)
{
    SensorCall();NV result[3] = {0.0, 0.0, 0.0};
    const char* s = orig;
#ifdef USE_PERL_ATOF
    UV accumulator[2] = {0,0};	/* before/after dp */
    bool negative = 0;
    const char* send = s + strlen(orig) - 1;
    bool seen_digit = 0;
    I32 exp_adjust[2] = {0,0};
    I32 exp_acc[2] = {-1, -1};
    /* the current exponent adjust for the accumulators */
    I32 exponent = 0;
    I32	seen_dp  = 0;
    I32 digit = 0;
    I32 old_digit = 0;
    I32 sig_digits = 0; /* noof significant digits seen so far */

    PERL_ARGS_ASSERT_MY_ATOF2;

/* There is no point in processing more significant digits
 * than the NV can hold. Note that NV_DIG is a lower-bound value,
 * while we need an upper-bound value. We add 2 to account for this;
 * since it will have been conservative on both the first and last digit.
 * For example a 32-bit mantissa with an exponent of 4 would have
 * exact values in the set
 *               4
 *               8
 *              ..
 *     17179869172
 *     17179869176
 *     17179869180
 *
 * where for the purposes of calculating NV_DIG we would have to discount
 * both the first and last digit, since neither can hold all values from
 * 0..9; but for calculating the value we must examine those two digits.
 */
#ifdef MAX_SIG_DIG_PLUS
    /* It is not necessarily the case that adding 2 to NV_DIG gets all the
       possible digits in a NV, especially if NVs are not IEEE compliant
       (e.g., long doubles on IRIX) - Allen <allens@cpan.org> */
# define MAX_SIG_DIGITS (NV_DIG+MAX_SIG_DIG_PLUS)
#else
# define MAX_SIG_DIGITS (NV_DIG+2)
#endif

/* the max number we can accumulate in a UV, and still safely do 10*N+9 */
#define MAX_ACCUMULATE ( (UV) ((UV_MAX - 9)/10))

    /* leading whitespace */
    SensorCall();while (isSPACE(*s))
	{/*83*/SensorCall();++s;/*84*/}

    /* sign */
    SensorCall();switch (*s) {
	case '-':
	    SensorCall();negative = 1;
	    /* fall through */
	case '+':
	    SensorCall();++s;
    }

    /* punt to strtod for NaN/Inf; if no support for it there, tough luck */

#ifdef HAS_STRTOD
    SensorCall();if (*s == 'n' || *s == 'N' || *s == 'i' || *s == 'I') {
        SensorCall();const char *p = negative ? s - 1 : s;
        char *endp;
        NV rslt;
        rslt = strtod(p, &endp);
        SensorCall();if (endp != p) {
            SensorCall();*value = rslt;
            {char * ReplaceReturn = (char *)endp; SensorCall(); return ReplaceReturn;}
        }
    }
#endif

    /* we accumulate digits into an integer; when this becomes too
     * large, we add the total to NV and start again */

    SensorCall();while (1) {
	SensorCall();if (isDIGIT(*s)) {
	    SensorCall();seen_digit = 1;
	    old_digit = digit;
	    digit = *s++ - '0';
	    SensorCall();if (seen_dp)
		{/*85*/SensorCall();exp_adjust[1]++;/*86*/}

	    /* don't start counting until we see the first significant
	     * digit, eg the 5 in 0.00005... */
	    SensorCall();if (!sig_digits && digit == 0)
		{/*87*/SensorCall();continue;/*88*/}

	    SensorCall();if (++sig_digits > MAX_SIG_DIGITS) {
		/* limits of precision reached */
	        SensorCall();if (digit > 5) {
		    SensorCall();++accumulator[seen_dp];
		} else {/*89*/SensorCall();if (digit == 5) {
		    SensorCall();if (old_digit % 2) { /* round to even - Allen */
			SensorCall();++accumulator[seen_dp];
		    }
		;/*90*/}}
		SensorCall();if (seen_dp) {
		    SensorCall();exp_adjust[1]--;
		} else {
		    SensorCall();exp_adjust[0]++;
		}
		/* skip remaining digits */
		SensorCall();while (isDIGIT(*s)) {
		    SensorCall();++s;
		    SensorCall();if (! seen_dp) {
			SensorCall();exp_adjust[0]++;
		    }
		}
		/* warn of loss of precision? */
	    }
	    else {
		SensorCall();if (accumulator[seen_dp] > MAX_ACCUMULATE) {
		    /* add accumulator to result and start again */
		    SensorCall();result[seen_dp] = S_mulexp10(result[seen_dp],
						 exp_acc[seen_dp])
			+ (NV)accumulator[seen_dp];
		    accumulator[seen_dp] = 0;
		    exp_acc[seen_dp] = 0;
		}
		SensorCall();accumulator[seen_dp] = accumulator[seen_dp] * 10 + digit;
		++exp_acc[seen_dp];
	    }
	}
	else {/*91*/SensorCall();if (!seen_dp && GROK_NUMERIC_RADIX(&s, send)) {
	    SensorCall();seen_dp = 1;
	    SensorCall();if (sig_digits > MAX_SIG_DIGITS) {
		SensorCall();do {
		    SensorCall();++s;
		} while (isDIGIT(*s));
		SensorCall();break;
	    }
	}
	else {
	    SensorCall();break;
	;/*92*/}}
    }

    SensorCall();result[0] = S_mulexp10(result[0], exp_acc[0]) + (NV)accumulator[0];
    SensorCall();if (seen_dp) {
	SensorCall();result[1] = S_mulexp10(result[1], exp_acc[1]) + (NV)accumulator[1];
    }

    SensorCall();if (seen_digit && (*s == 'e' || *s == 'E')) {
	bool expnegative = 0;

	SensorCall();++s;
	SensorCall();switch (*s) {
	    case '-':
		SensorCall();expnegative = 1;
		/* fall through */
	    case '+':
		SensorCall();++s;
	}
	SensorCall();while (isDIGIT(*s))
	    {/*93*/SensorCall();exponent = exponent * 10 + (*s++ - '0');/*94*/}
	SensorCall();if (expnegative)
	    {/*95*/SensorCall();exponent = -exponent;/*96*/}
    }



    /* now apply the exponent */

    SensorCall();if (seen_dp) {
	SensorCall();result[2] = S_mulexp10(result[0],exponent+exp_adjust[0])
		+ S_mulexp10(result[1],exponent-exp_adjust[1]);
    } else {
	SensorCall();result[2] = S_mulexp10(result[0],exponent+exp_adjust[0]);
    }

    /* now apply the sign */
    SensorCall();if (negative)
	{/*97*/SensorCall();result[2] = -result[2];/*98*/}
#endif /* USE_PERL_ATOF */
    SensorCall();*value = result[2];
    {char * ReplaceReturn = (char *)s; SensorCall(); return ReplaceReturn;}
}

#if ! defined(HAS_MODFL) && defined(HAS_AINTL) && defined(HAS_COPYSIGNL)
long double
Perl_my_modfl(long double x, long double *ip)
{
	*ip = aintl(x);
	return (x == *ip ? copysignl(0.0L, x) : x - *ip);
}
#endif

#if ! defined(HAS_FREXPL) && defined(HAS_ILOGBL) && defined(HAS_SCALBNL)
long double
Perl_my_frexpl(long double x, int *e) {
	*e = x == 0.0L ? 0 : ilogbl(x) + 1;
	return (scalbnl(x, -*e));
}
#endif

/*
=for apidoc Perl_signbit

Return a non-zero integer if the sign bit on an NV is set, and 0 if
it is not.  

If Configure detects this system has a signbit() that will work with
our NVs, then we just use it via the #define in perl.h.  Otherwise,
fall back on this implementation.  As a first pass, this gets everything
right except -0.0.  Alas, catching -0.0 is the main use for this function,
so this is not too helpful yet.  Still, at least we have the scaffolding
in place to support other systems, should that prove useful.


Configure notes:  This function is called 'Perl_signbit' instead of a
plain 'signbit' because it is easy to imagine a system having a signbit()
function or macro that doesn't happen to work with our particular choice
of NVs.  We shouldn't just re-#define signbit as Perl_signbit and expect
the standard system headers to be happy.  Also, this is a no-context
function (no pTHX_) because Perl_signbit() is usually re-#defined in
perl.h as a simple macro call to the system's signbit().
Users should just always call Perl_signbit().

=cut
*/
#if !defined(HAS_SIGNBIT)
int
Perl_signbit(NV x) {
    return (x < 0.0) ? 1 : 0;
}
#endif

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 *
 * ex: set ts=8 sts=4 sw=4 noet:
 */
