#include "var/tmp/sensor.h"
/*    hv.c
 *
 *    Copyright (C) 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000,
 *    2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008 by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 *
 */

/*
 *      I sit beside the fire and think
 *          of all that I have seen.
 *                         --Bilbo
 *
 *     [p.278 of _The Lord of the Rings_, II/iii: "The Ring Goes South"]
 */

/* 
=head1 Hash Manipulation Functions

A HV structure represents a Perl hash.  It consists mainly of an array
of pointers, each of which points to a linked list of HE structures.  The
array is indexed by the hash function of the key, so each linked list
represents all the hash entries with the same hash value.  Each HE contains
a pointer to the actual value, plus a pointer to a HEK structure which
holds the key and hash value.

=cut

*/

#include "EXTERN.h"
#define PERL_IN_HV_C
#define PERL_HASH_INTERNAL_ACCESS
#include "perl.h"

#define HV_MAX_LENGTH_BEFORE_REHASH 14
#define SHOULD_DO_HSPLIT(xhv) ((xhv)->xhv_keys > (xhv)->xhv_max) /* HvTOTALKEYS(hv) > HvMAX(hv) */

static const char S_strtab_error[]
    = "Cannot modify shared string table in hv_%s";

#ifdef PURIFY

#define new_HE() (HE*)safemalloc(sizeof(HE))
#define del_HE(p) safefree((char*)p)

#else

STATIC HE*
S_new_he(pTHX)
{
SensorCall(2865);    dVAR;
    HE* he;
    void ** const root = &PL_body_roots[HE_SVSLOT];

    SensorCall(2867);if (!*root)
	{/*163*/SensorCall(2866);Perl_more_bodies(aTHX_ HE_SVSLOT, sizeof(HE), PERL_ARENA_SIZE);/*164*/}
    SensorCall(2868);he = (HE*) *root;
    assert(he);
    *root = HeNEXT(he);
    {HE * ReplaceReturn1663 = he; SensorCall(2869); return ReplaceReturn1663;}
}

#define new_HE() new_he()
#define del_HE(p) \
    STMT_START { \
	HeNEXT(p) = (HE*)(PL_body_roots[HE_SVSLOT]);	\
	PL_body_roots[HE_SVSLOT] = p; \
    } STMT_END



#endif

STATIC HEK *
S_save_hek_flags(const char *str, I32 len, U32 hash, int flags)
{
    SensorCall(2870);const int flags_masked = flags & HVhek_MASK;
    char *k;
    register HEK *hek;

    PERL_ARGS_ASSERT_SAVE_HEK_FLAGS;

    Newx(k, HEK_BASESIZE + len + 2, char);
    hek = (HEK*)k;
    Copy(str, HEK_KEY(hek), len, char);
    HEK_KEY(hek)[len] = 0;
    HEK_LEN(hek) = len;
    HEK_HASH(hek) = hash;
    HEK_FLAGS(hek) = (unsigned char)flags_masked | HVhek_UNSHARED;

    SensorCall(2871);if (flags & HVhek_FREEKEY)
	Safefree(str);
    {HEK * ReplaceReturn1662 = hek; SensorCall(2872); return ReplaceReturn1662;}
}

/* free the pool of temporary HE/HEK pairs returned by hv_fetch_ent
 * for tied hashes */

void
Perl_free_tied_hv_pool(pTHX)
{
SensorCall(2873);    dVAR;
    HE *he = PL_hv_fetch_ent_mh;
    SensorCall(2875);while (he) {
	SensorCall(2874);HE * const ohe = he;
	Safefree(HeKEY_hek(he));
	he = HeNEXT(he);
	del_HE(ohe);
    }
    PL_hv_fetch_ent_mh = NULL;
}

#if defined(USE_ITHREADS)
HEK *
Perl_hek_dup(pTHX_ HEK *source, CLONE_PARAMS* param)
{
    SensorCall(2876);HEK *shared;

    PERL_ARGS_ASSERT_HEK_DUP;
    PERL_UNUSED_ARG(param);

    SensorCall(2877);if (!source)
	return NULL;

    SensorCall(2878);shared = (HEK*)ptr_table_fetch(PL_ptr_table, source);
    SensorCall(2881);if (shared) {
	/* We already shared this hash key.  */
	SensorCall(2879);(void)share_hek_hek(shared);
    }
    else {
	SensorCall(2880);shared
	    = share_hek_flags(HEK_KEY(source), HEK_LEN(source),
			      HEK_HASH(source), HEK_FLAGS(source));
	ptr_table_store(PL_ptr_table, source, shared);
    }
    {HEK * ReplaceReturn1661 = shared; SensorCall(2882); return ReplaceReturn1661;}
}

HE *
Perl_he_dup(pTHX_ const HE *e, bool shared, CLONE_PARAMS* param)
{
    SensorCall(2883);HE *ret;

    PERL_ARGS_ASSERT_HE_DUP;

    SensorCall(2884);if (!e)
	return NULL;
    /* look for it in the table first */
    SensorCall(2885);ret = (HE*)ptr_table_fetch(PL_ptr_table, e);
    SensorCall(2887);if (ret)
	{/*191*/{HE * ReplaceReturn1660 = ret; SensorCall(2886); return ReplaceReturn1660;}/*192*/}

    /* create anew and remember what it is */
    SensorCall(2888);ret = new_HE();
    ptr_table_store(PL_ptr_table, e, ret);

    HeNEXT(ret) = he_dup(HeNEXT(e),shared, param);
    SensorCall(2895);if (HeKLEN(e) == HEf_SVKEY) {
	SensorCall(2889);char *k;
	Newx(k, HEK_BASESIZE + sizeof(const SV *), char);
	HeKEY_hek(ret) = (HEK*)k;
	HeKEY_sv(ret) = sv_dup_inc(HeKEY_sv(e), param);
    }
    else {/*193*/SensorCall(2890);if (shared) {
	/* This is hek_dup inlined, which seems to be important for speed
	   reasons.  */
	SensorCall(2891);HEK * const source = HeKEY_hek(e);
	HEK *shared = (HEK*)ptr_table_fetch(PL_ptr_table, source);

	SensorCall(2894);if (shared) {
	    /* We already shared this hash key.  */
	    SensorCall(2892);(void)share_hek_hek(shared);
	}
	else {
	    SensorCall(2893);shared
		= share_hek_flags(HEK_KEY(source), HEK_LEN(source),
				  HEK_HASH(source), HEK_FLAGS(source));
	    ptr_table_store(PL_ptr_table, source, shared);
	}
	HeKEY_hek(ret) = shared;
    }
    else
	HeKEY_hek(ret) = save_hek_flags(HeKEY(e), HeKLEN(e), HeHASH(e),
                                        HeKFLAGS(e));/*194*/}
    HeVAL(ret) = sv_dup_inc(HeVAL(e), param);
    {HE * ReplaceReturn1659 = ret; SensorCall(2896); return ReplaceReturn1659;}
}
#endif	/* USE_ITHREADS */

static void
S_hv_notallowed(pTHX_ int flags, const char *key, I32 klen,
		const char *msg)
{
    SensorCall(2897);SV * const sv = sv_newmortal();

    PERL_ARGS_ASSERT_HV_NOTALLOWED;

    SensorCall(2898);if (!(flags & HVhek_FREEKEY)) {
	sv_setpvn(sv, key, klen);
    }
    else {
	/* Need to free saved eventually assign to mortal SV */
	/* XXX is this line an error ???:  SV *sv = sv_newmortal(); */
	sv_usepvn(sv, (char *) key, klen);
    }
    SensorCall(2899);if (flags & HVhek_UTF8) {
	SvUTF8_on(sv);
    }
    SensorCall(2900);Perl_croak(aTHX_ msg, SVfARG(sv));
SensorCall(2901);}

/* (klen == HEf_SVKEY) is special for MAGICAL hv entries, meaning key slot
 * contains an SV* */

/*
=for apidoc hv_store

Stores an SV in a hash.  The hash key is specified as C<key> and the
absolute value of C<klen> is the length of the key.  If C<klen> is
negative the key is assumed to be in UTF-8-encoded Unicode.  The
C<hash> parameter is the precomputed hash value; if it is zero then
Perl will compute it.

The return value will be
NULL if the operation failed or if the value did not need to be actually
stored within the hash (as in the case of tied hashes).  Otherwise it can
be dereferenced to get the original C<SV*>.  Note that the caller is
responsible for suitably incrementing the reference count of C<val> before
the call, and decrementing it if the function returned NULL.  Effectively
a successful hv_store takes ownership of one reference to C<val>.  This is
usually what you want; a newly created SV has a reference count of one, so
if all your code does is create SVs then store them in a hash, hv_store
will own the only reference to the new SV, and your code doesn't need to do
anything further to tidy up.  hv_store is not implemented as a call to
hv_store_ent, and does not create a temporary SV for the key, so if your
key data is not already in SV form then use hv_store in preference to
hv_store_ent.

See L<perlguts/"Understanding the Magic of Tied Hashes and Arrays"> for more
information on how to use this function on tied hashes.

=for apidoc hv_store_ent

Stores C<val> in a hash.  The hash key is specified as C<key>.  The C<hash>
parameter is the precomputed hash value; if it is zero then Perl will
compute it.  The return value is the new hash entry so created.  It will be
NULL if the operation failed or if the value did not need to be actually
stored within the hash (as in the case of tied hashes).  Otherwise the
contents of the return value can be accessed using the C<He?> macros
described here.  Note that the caller is responsible for suitably
incrementing the reference count of C<val> before the call, and
decrementing it if the function returned NULL.  Effectively a successful
hv_store_ent takes ownership of one reference to C<val>.  This is
usually what you want; a newly created SV has a reference count of one, so
if all your code does is create SVs then store them in a hash, hv_store
will own the only reference to the new SV, and your code doesn't need to do
anything further to tidy up.  Note that hv_store_ent only reads the C<key>;
unlike C<val> it does not take ownership of it, so maintaining the correct
reference count on C<key> is entirely the caller's responsibility.  hv_store
is not implemented as a call to hv_store_ent, and does not create a temporary
SV for the key, so if your key data is not already in SV form then use
hv_store in preference to hv_store_ent.

See L<perlguts/"Understanding the Magic of Tied Hashes and Arrays"> for more
information on how to use this function on tied hashes.

=for apidoc hv_exists

Returns a boolean indicating whether the specified hash key exists.  The
absolute value of C<klen> is the length of the key.  If C<klen> is
negative the key is assumed to be in UTF-8-encoded Unicode.

=for apidoc hv_fetch

Returns the SV which corresponds to the specified key in the hash.
The absolute value of C<klen> is the length of the key.  If C<klen> is
negative the key is assumed to be in UTF-8-encoded Unicode.  If
C<lval> is set then the fetch will be part of a store.  Check that the
return value is non-null before dereferencing it to an C<SV*>.

See L<perlguts/"Understanding the Magic of Tied Hashes and Arrays"> for more
information on how to use this function on tied hashes.

=for apidoc hv_exists_ent

Returns a boolean indicating whether
the specified hash key exists.  C<hash>
can be a valid precomputed hash value, or 0 to ask for it to be
computed.

=cut
*/

/* returns an HE * structure with the all fields set */
/* note that hent_val will be a mortal sv for MAGICAL hashes */
/*
=for apidoc hv_fetch_ent

Returns the hash entry which corresponds to the specified key in the hash.
C<hash> must be a valid precomputed hash number for the given C<key>, or 0
if you want the function to compute it.  IF C<lval> is set then the fetch
will be part of a store.  Make sure the return value is non-null before
accessing it.  The return value when C<hv> is a tied hash is a pointer to a
static location, so be sure to make a copy of the structure if you need to
store it somewhere.

See L<perlguts/"Understanding the Magic of Tied Hashes and Arrays"> for more
information on how to use this function on tied hashes.

=cut
*/

/* Common code for hv_delete()/hv_exists()/hv_fetch()/hv_store()  */
void *
Perl_hv_common_key_len(pTHX_ HV *hv, const char *key, I32 klen_i32,
		       const int action, SV *val, const U32 hash)
{
    SensorCall(2902);STRLEN klen;
    int flags;

    PERL_ARGS_ASSERT_HV_COMMON_KEY_LEN;

    SensorCall(2905);if (klen_i32 < 0) {
	SensorCall(2903);klen = -klen_i32;
	flags = HVhek_UTF8;
    } else {
	SensorCall(2904);klen = klen_i32;
	flags = 0;
    }
    {void * ReplaceReturn1658 = hv_common(hv, NULL, key, klen, flags, action, val, hash); SensorCall(2906); return ReplaceReturn1658;}
}

void *
Perl_hv_common(pTHX_ HV *hv, SV *keysv, const char *key, STRLEN klen,
	       int flags, int action, SV *val, register U32 hash)
{
SensorCall(2907);    dVAR;
    XPVHV* xhv;
    HE *entry;
    HE **oentry;
    SV *sv;
    bool is_utf8;
    int masked_flags;
    const int return_svp = action & HV_FETCH_JUST_SV;

    SensorCall(2908);if (!hv)
	return NULL;
    SensorCall(2909);if (SvTYPE(hv) == (svtype)SVTYPEMASK)
	return NULL;

    assert(SvTYPE(hv) == SVt_PVHV);

    SensorCall(2918);if (SvSMAGICAL(hv) && SvGMAGICAL(hv) && !(action & HV_DISABLE_UVAR_XKEY)) {
	SensorCall(2910);MAGIC* mg;
	SensorCall(2917);if ((mg = mg_find((const SV *)hv, PERL_MAGIC_uvar))) {
	    SensorCall(2911);struct ufuncs * const uf = (struct ufuncs *)mg->mg_ptr;
	    SensorCall(2916);if (uf->uf_set == NULL) {
		SensorCall(2912);SV* obj = mg->mg_obj;

		SensorCall(2914);if (!keysv) {
		    SensorCall(2913);keysv = newSVpvn_flags(key, klen, SVs_TEMP |
					   ((flags & HVhek_UTF8)
					    ? SVf_UTF8 : 0));
		}
		
		SensorCall(2915);mg->mg_obj = keysv;         /* pass key */
		uf->uf_index = action;      /* pass action */
		magic_getuvar(MUTABLE_SV(hv), mg);
		keysv = mg->mg_obj;         /* may have changed */
		mg->mg_obj = obj;

		/* If the key may have changed, then we need to invalidate
		   any passed-in computed hash value.  */
		hash = 0;
	    }
	}
    }
    SensorCall(2925);if (keysv) {
	SensorCall(2919);if (flags & HVhek_FREEKEY)
	    Safefree(key);
	SensorCall(2920);key = SvPV_const(keysv, klen);
	is_utf8 = (SvUTF8(keysv) != 0);
	SensorCall(2923);if (SvIsCOW_shared_hash(keysv)) {
	    SensorCall(2921);flags = HVhek_KEYCANONICAL | (is_utf8 ? HVhek_UTF8 : 0);
	} else {
	    SensorCall(2922);flags = 0;
	}
    } else {
	SensorCall(2924);is_utf8 = ((flags & HVhek_UTF8) ? TRUE : FALSE);
    }

    SensorCall(2927);if (action & HV_DELETE) {
	{void * ReplaceReturn1657 = (void *) hv_delete_common(hv, keysv, key, klen,
					 flags | (is_utf8 ? HVhek_UTF8 : 0),
					 action, hash); SensorCall(2926); return ReplaceReturn1657;}
    }

    SensorCall(2928);xhv = (XPVHV*)SvANY(hv);
    SensorCall(2961);if (SvMAGICAL(hv)) {
	SensorCall(2929);if (SvRMAGICAL(hv) && !(action & (HV_FETCH_ISSTORE|HV_FETCH_ISEXISTS))) {
	    SensorCall(2930);if (mg_find((const SV *)hv, PERL_MAGIC_tied)
		|| SvGMAGICAL((const SV *)hv))
	    {
		/* FIXME should be able to skimp on the HE/HEK here when
		   HV_FETCH_JUST_SV is true.  */
		SensorCall(2931);if (!keysv) {
		    SensorCall(2932);keysv = newSVpvn_utf8(key, klen, is_utf8);
  		} else {
		    SensorCall(2933);keysv = newSVsv(keysv);
		}
                SensorCall(2934);sv = sv_newmortal();
                mg_copy(MUTABLE_SV(hv), sv, (char *)keysv, HEf_SVKEY);

		/* grab a fake HE/HEK pair from the pool or make a new one */
		entry = PL_hv_fetch_ent_mh;
		SensorCall(2936);if (entry)
		    PL_hv_fetch_ent_mh = HeNEXT(entry);
		else {
		    SensorCall(2935);char *k;
		    entry = new_HE();
		    Newx(k, HEK_BASESIZE + sizeof(const SV *), char);
		    HeKEY_hek(entry) = (HEK*)k;
		}
		HeNEXT(entry) = NULL;
		HeSVKEY_set(entry, keysv);
		HeVAL(entry) = sv;
		sv_upgrade(sv, SVt_PVLV);
		LvTYPE(sv) = 'T';
		 /* so we can free entry when freeing sv */
		LvTARG(sv) = MUTABLE_SV(entry);

		/* XXX remove at some point? */
		SensorCall(2937);if (flags & HVhek_FREEKEY)
		    Safefree(key);

		SensorCall(2939);if (return_svp) {
		    {void * ReplaceReturn1656 = entry ? (void *) &HeVAL(entry) : NULL; SensorCall(2938); return ReplaceReturn1656;}
		}
		{void * ReplaceReturn1655 = (void *) entry; SensorCall(2940); return ReplaceReturn1655;}
	    }
#ifdef ENV_IS_CASELESS
	    else if (mg_find((const SV *)hv, PERL_MAGIC_env)) {
		U32 i;
		for (i = 0; i < klen; ++i)
		    if (isLOWER(key[i])) {
			/* Would be nice if we had a routine to do the
			   copy and upercase in a single pass through.  */
			const char * const nkey = strupr(savepvn(key,klen));
			/* Note that this fetch is for nkey (the uppercased
			   key) whereas the store is for key (the original)  */
			void *result = hv_common(hv, NULL, nkey, klen,
						 HVhek_FREEKEY, /* free nkey */
						 0 /* non-LVAL fetch */
						 | HV_DISABLE_UVAR_XKEY
						 | return_svp,
						 NULL /* no value */,
						 0 /* compute hash */);
			if (!result && (action & HV_FETCH_LVALUE)) {
			    /* This call will free key if necessary.
			       Do it this way to encourage compiler to tail
			       call optimise.  */
			    result = hv_common(hv, keysv, key, klen, flags,
					       HV_FETCH_ISSTORE
					       | HV_DISABLE_UVAR_XKEY
					       | return_svp,
					       newSV(0), hash);
			} else {
			    if (flags & HVhek_FREEKEY)
				Safefree(key);
			}
			return result;
		    }
	    }
#endif
	} /* ISFETCH */
	else {/*7*/SensorCall(2941);if (SvRMAGICAL(hv) && (action & HV_FETCH_ISEXISTS)) {
	    SensorCall(2942);if (mg_find((const SV *)hv, PERL_MAGIC_tied)
		|| SvGMAGICAL((const SV *)hv)) {
		/* I don't understand why hv_exists_ent has svret and sv,
		   whereas hv_exists only had one.  */
		SensorCall(2943);SV * const svret = sv_newmortal();
		sv = sv_newmortal();

		SensorCall(2947);if (keysv || is_utf8) {
		    SensorCall(2944);if (!keysv) {
			SensorCall(2945);keysv = newSVpvn_utf8(key, klen, TRUE);
		    } else {
			SensorCall(2946);keysv = newSVsv(keysv);
		    }
		    mg_copy(MUTABLE_SV(hv), sv, (char *)sv_2mortal(keysv), HEf_SVKEY);
		} else {
		    mg_copy(MUTABLE_SV(hv), sv, key, klen);
		}
		SensorCall(2948);if (flags & HVhek_FREEKEY)
		    Safefree(key);
		magic_existspack(svret, mg_find(sv, PERL_MAGIC_tiedelem));
		/* This cast somewhat evil, but I'm merely using NULL/
		   not NULL to return the boolean exists.
		   And I know hv is not NULL.  */
		{void * ReplaceReturn1654 = SvTRUE(svret) ? (void *)hv : NULL; SensorCall(2949); return ReplaceReturn1654;}
		}
#ifdef ENV_IS_CASELESS
	    else if (mg_find((const SV *)hv, PERL_MAGIC_env)) {
		/* XXX This code isn't UTF8 clean.  */
		char * const keysave = (char * const)key;
		/* Will need to free this, so set FREEKEY flag.  */
		key = savepvn(key,klen);
		key = (const char*)strupr((char*)key);
		is_utf8 = FALSE;
		hash = 0;
		keysv = 0;

		if (flags & HVhek_FREEKEY) {
		    Safefree(keysave);
		}
		flags |= HVhek_FREEKEY;
	    }
#endif
	} /* ISEXISTS */
	else {/*9*/SensorCall(2950);if (action & HV_FETCH_ISSTORE) {
	    bool needs_copy;
	    bool needs_store;
	    hv_magic_check (hv, &needs_copy, &needs_store);
	    SensorCall(2960);if (needs_copy) {
		SensorCall(2951);const bool save_taint = PL_tainted;
		SensorCall(2956);if (keysv || is_utf8) {
		    SensorCall(2952);if (!keysv) {
			SensorCall(2953);keysv = newSVpvn_utf8(key, klen, TRUE);
		    }
		    SensorCall(2954);if (PL_tainting)
			PL_tainted = SvTAINTED(keysv);
		    SensorCall(2955);keysv = sv_2mortal(newSVsv(keysv));
		    mg_copy(MUTABLE_SV(hv), val, (char*)keysv, HEf_SVKEY);
		} else {
		    mg_copy(MUTABLE_SV(hv), val, key, klen);
		}

		TAINT_IF(save_taint);
		SensorCall(2959);if (!needs_store) {
		    SensorCall(2957);if (flags & HVhek_FREEKEY)
			Safefree(key);
		    {void * ReplaceReturn1653 = NULL; SensorCall(2958); return ReplaceReturn1653;}
		}
#ifdef ENV_IS_CASELESS
		else if (mg_find((const SV *)hv, PERL_MAGIC_env)) {
		    /* XXX This code isn't UTF8 clean.  */
		    const char *keysave = key;
		    /* Will need to free this, so set FREEKEY flag.  */
		    key = savepvn(key,klen);
		    key = (const char*)strupr((char*)key);
		    is_utf8 = FALSE;
		    hash = 0;
		    keysv = 0;

		    if (flags & HVhek_FREEKEY) {
			Safefree(keysave);
		    }
		    flags |= HVhek_FREEKEY;
		}
#endif
	    }
	;/*10*/}/*8*/}} /* ISSTORE */
    } /* SvMAGICAL */

    SensorCall(2966);if (!HvARRAY(hv)) {
	SensorCall(2962);if ((action & (HV_FETCH_LVALUE | HV_FETCH_ISSTORE))
#ifdef DYNAMIC_ENV_FETCH  /* if it's an %ENV lookup, we may get it on the fly */
		 || (SvRMAGICAL((const SV *)hv)
		     && mg_find((const SV *)hv, PERL_MAGIC_env))
#endif
								  ) {
	    SensorCall(2963);char *array;
	    Newxz(array,
		 PERL_HV_ARRAY_ALLOC_BYTES(xhv->xhv_max+1 /* HvMAX(hv)+1 */),
		 char);
	    HvARRAY(hv) = (HE**)array;
	}
#ifdef DYNAMIC_ENV_FETCH
	else if (action & HV_FETCH_ISEXISTS) {
	    /* for an %ENV exists, if we do an insert it's by a recursive
	       store call, so avoid creating HvARRAY(hv) right now.  */
	}
#endif
	else {
	    /* XXX remove at some point? */
            SensorCall(2964);if (flags & HVhek_FREEKEY)
                Safefree(key);

	    {void * ReplaceReturn1652 = NULL; SensorCall(2965); return ReplaceReturn1652;}
	}
    }

    SensorCall(2972);if (is_utf8 & !(flags & HVhek_KEYCANONICAL)) {
	SensorCall(2967);char * const keysave = (char *)key;
	key = (char*)bytes_from_utf8((U8*)key, &klen, &is_utf8);
        SensorCall(2968);if (is_utf8)
	    flags |= HVhek_UTF8;
	else
	    flags &= ~HVhek_UTF8;
        SensorCall(2971);if (key != keysave) {
	    SensorCall(2969);if (flags & HVhek_FREEKEY)
		Safefree(keysave);
            SensorCall(2970);flags |= HVhek_WASUTF8 | HVhek_FREEKEY;
	    /* If the caller calculated a hash, it was on the sequence of
	       octets that are the UTF-8 form. We've now changed the sequence
	       of octets stored to that of the equivalent byte representation,
	       so the hash we need is different.  */
	    hash = 0;
	}
    }

    SensorCall(2973);if (HvREHASH(hv) || (!hash && !(keysv && (SvIsCOW_shared_hash(keysv)))))
	PERL_HASH_INTERNAL_(hash, key, klen, HvREHASH(hv));
    else if (!hash)
	hash = SvSHARED_HASH(keysv);

    /* We don't have a pointer to the hv, so we have to replicate the
       flag into every HEK, so that hv_iterkeysv can see it.
       And yes, you do need this even though you are not "storing" because
       you can flip the flags below if doing an lval lookup.  (And that
       was put in to give the semantics Andreas was expecting.)  */
    SensorCall(2974);if (HvREHASH(hv))
	flags |= HVhek_REHASH;

    SensorCall(2975);masked_flags = (flags & HVhek_MASK);

#ifdef DYNAMIC_ENV_FETCH
    if (!HvARRAY(hv)) entry = NULL;
    else
#endif
    {
	entry = (HvARRAY(hv))[hash & (I32) HvMAX(hv)];
    }
    SensorCall(3005);for (; entry; entry = HeNEXT(entry)) {
	SensorCall(2976);if (HeHASH(entry) != hash)		/* strings can't be equal */
	    {/*11*/SensorCall(2977);continue;/*12*/}
	SensorCall(2979);if (HeKLEN(entry) != (I32)klen)
	    {/*13*/SensorCall(2978);continue;/*14*/}
	SensorCall(2981);if (HeKEY(entry) != key && memNE(HeKEY(entry),key,klen))	/* is this it? */
	    {/*15*/SensorCall(2980);continue;/*16*/}
	SensorCall(2983);if ((HeKFLAGS(entry) ^ masked_flags) & HVhek_UTF8)
	    {/*17*/SensorCall(2982);continue;/*18*/}

        SensorCall(3000);if (action & (HV_FETCH_LVALUE|HV_FETCH_ISSTORE)) {
	    SensorCall(2984);if (HeKFLAGS(entry) != masked_flags) {
		/* We match if HVhek_UTF8 bit in our flags and hash key's
		   match.  But if entry was set previously with HVhek_WASUTF8
		   and key now doesn't (or vice versa) then we should change
		   the key's flag, as this is assignment.  */
		SensorCall(2985);if (HvSHAREKEYS(hv)) {
		    /* Need to swap the key we have for a key with the flags we
		       need. As keys are shared we can't just write to the
		       flag, so we share the new one, unshare the old one.  */
		    SensorCall(2986);HEK * const new_hek = share_hek_flags(key, klen, hash,
						   masked_flags);
		    unshare_hek (HeKEY_hek(entry));
		    HeKEY_hek(entry) = new_hek;
		}
		else {/*19*/SensorCall(2987);if (hv == PL_strtab) {
		    /* PL_strtab is usually the only hash without HvSHAREKEYS,
		       so putting this test here is cheap  */
		    SensorCall(2988);if (flags & HVhek_FREEKEY)
			Safefree(key);
		    SensorCall(2989);Perl_croak(aTHX_ S_strtab_error,
			       action & HV_FETCH_LVALUE ? "fetch" : "store");
		}
		else
		    HeKFLAGS(entry) = masked_flags;/*20*/}
		SensorCall(2990);if (masked_flags & HVhek_ENABLEHVKFLAGS)
		    HvHASKFLAGS_on(hv);
	    }
	    SensorCall(2997);if (HeVAL(entry) == &PL_sv_placeholder) {
		/* yes, can store into placeholder slot */
		SensorCall(2991);if (action & HV_FETCH_LVALUE) {
		    SensorCall(2992);if (SvMAGICAL(hv)) {
			/* This preserves behaviour with the old hv_fetch
			   implementation which at this point would bail out
			   with a break; (at "if we find a placeholder, we
			   pretend we haven't found anything")

			   That break mean that if a placeholder were found, it
			   caused a call into hv_store, which in turn would
			   check magic, and if there is no magic end up pretty
			   much back at this point (in hv_store's code).  */
			SensorCall(2993);break;
		    }
		    /* LVAL fetch which actually needs a store.  */
		    SensorCall(2994);val = newSV(0);
		    HvPLACEHOLDERS(hv)--;
		} else {
		    /* store */
		    SensorCall(2995);if (val != &PL_sv_placeholder)
			HvPLACEHOLDERS(hv)--;
		}
		HeVAL(entry) = val;
	    } else {/*21*/SensorCall(2996);if (action & HV_FETCH_ISSTORE) {
		SvREFCNT_dec(HeVAL(entry));
		HeVAL(entry) = val;
	    ;/*22*/}}
	} else {/*23*/SensorCall(2998);if (HeVAL(entry) == &PL_sv_placeholder) {
	    /* if we find a placeholder, we pretend we haven't found
	       anything */
	    SensorCall(2999);break;
	;/*24*/}}
	SensorCall(3001);if (flags & HVhek_FREEKEY)
	    Safefree(key);
	SensorCall(3003);if (return_svp) {
	    {void * ReplaceReturn1651 = entry ? (void *) &HeVAL(entry) : NULL; SensorCall(3002); return ReplaceReturn1651;}
	}
	{void * ReplaceReturn1650 = entry; SensorCall(3004); return ReplaceReturn1650;}
    }
#ifdef DYNAMIC_ENV_FETCH  /* %ENV lookup?  If so, try to fetch the value now */
    if (!(action & HV_FETCH_ISSTORE) 
	&& SvRMAGICAL((const SV *)hv)
	&& mg_find((const SV *)hv, PERL_MAGIC_env)) {
	unsigned long len;
	const char * const env = PerlEnv_ENVgetenv_len(key,&len);
	if (env) {
	    sv = newSVpvn(env,len);
	    SvTAINTED_on(sv);
	    return hv_common(hv, keysv, key, klen, flags,
			     HV_FETCH_ISSTORE|HV_DISABLE_UVAR_XKEY|return_svp,
			     sv, hash);
	}
    }
#endif

    SensorCall(3006);if (!entry && SvREADONLY(hv) && !(action & HV_FETCH_ISEXISTS)) {
	hv_notallowed(flags, key, klen,
			"Attempt to access disallowed key '%"SVf"' in"
			" a restricted hash");
    }
    SensorCall(3009);if (!(action & (HV_FETCH_LVALUE|HV_FETCH_ISSTORE))) {
	/* Not doing some form of store, so return failure.  */
	SensorCall(3007);if (flags & HVhek_FREEKEY)
	    Safefree(key);
	{void * ReplaceReturn1649 = NULL; SensorCall(3008); return ReplaceReturn1649;}
    }
    SensorCall(3013);if (action & HV_FETCH_LVALUE) {
	SensorCall(3010);val = action & HV_FETCH_EMPTY_HE ? NULL : newSV(0);
	SensorCall(3012);if (SvMAGICAL(hv)) {
	    /* At this point the old hv_fetch code would call to hv_store,
	       which in turn might do some tied magic. So we need to make that
	       magic check happen.  */
	    /* gonna assign to this, so it better be there */
	    /* If a fetch-as-store fails on the fetch, then the action is to
	       recurse once into "hv_store". If we didn't do this, then that
	       recursive call would call the key conversion routine again.
	       However, as we replace the original key with the converted
	       key, this would result in a double conversion, which would show
	       up as a bug if the conversion routine is not idempotent.  */
	    {void * ReplaceReturn1648 = hv_common(hv, keysv, key, klen, flags,
			     HV_FETCH_ISSTORE|HV_DISABLE_UVAR_XKEY|return_svp,
			     val, hash); SensorCall(3011); return ReplaceReturn1648;}
	    /* XXX Surely that could leak if the fetch-was-store fails?
	       Just like the hv_fetch.  */
	}
    }

    /* Welcome to hv_store...  */

    SensorCall(3015);if (!HvARRAY(hv)) {
	/* Not sure if we can get here.  I think the only case of oentry being
	   NULL is for %ENV with dynamic env fetch.  But that should disappear
	   with magic in the previous code.  */
	SensorCall(3014);char *array;
	Newxz(array,
	     PERL_HV_ARRAY_ALLOC_BYTES(xhv->xhv_max+1 /* HvMAX(hv)+1 */),
	     char);
	HvARRAY(hv) = (HE**)array;
    }

    SensorCall(3016);oentry = &(HvARRAY(hv))[hash & (I32) xhv->xhv_max];

    entry = new_HE();
    /* share_hek_flags will do the free for us.  This might be considered
       bad API design.  */
    SensorCall(3020);if (HvSHAREKEYS(hv))
	HeKEY_hek(entry) = share_hek_flags(key, klen, hash, flags);
    else {/*25*/SensorCall(3017);if (hv == PL_strtab) {
	/* PL_strtab is usually the only hash without HvSHAREKEYS, so putting
	   this test here is cheap  */
	SensorCall(3018);if (flags & HVhek_FREEKEY)
	    Safefree(key);
	SensorCall(3019);Perl_croak(aTHX_ S_strtab_error,
		   action & HV_FETCH_LVALUE ? "fetch" : "store");
    }
    else                                       /* gotta do the real thing */
	HeKEY_hek(entry) = save_hek_flags(key, klen, hash, flags);/*26*/}
    HeVAL(entry) = val;
    HeNEXT(entry) = *oentry;
    SensorCall(3021);*oentry = entry;

    SensorCall(3022);if (val == &PL_sv_placeholder)
	HvPLACEHOLDERS(hv)++;
    SensorCall(3023);if (masked_flags & HVhek_ENABLEHVKFLAGS)
	HvHASKFLAGS_on(hv);

    SensorCall(3024);xhv->xhv_keys++; /* HvTOTALKEYS(hv)++ */
    SensorCall(3025);if ( SHOULD_DO_HSPLIT(xhv) ) {
        hsplit(hv);
    }

    SensorCall(3027);if (return_svp) {
	{void * ReplaceReturn1647 = entry ? (void *) &HeVAL(entry) : NULL; SensorCall(3026); return ReplaceReturn1647;}
    }
    {void * ReplaceReturn1646 = (void *) entry; SensorCall(3028); return ReplaceReturn1646;}
}

STATIC void
S_hv_magic_check(HV *hv, bool *needs_copy, bool *needs_store)
{
    SensorCall(3029);const MAGIC *mg = SvMAGIC(hv);

    PERL_ARGS_ASSERT_HV_MAGIC_CHECK;

    *needs_copy = FALSE;
    *needs_store = TRUE;
    SensorCall(3036);while (mg) {
	SensorCall(3030);if (isUPPER(mg->mg_type)) {
	    SensorCall(3031);*needs_copy = TRUE;
	    SensorCall(3034);if (mg->mg_type == PERL_MAGIC_tied) {
		SensorCall(3032);*needs_store = FALSE;
		SensorCall(3033);return; /* We've set all there is to set. */
	    }
	}
	SensorCall(3035);mg = mg->mg_moremagic;
    }
SensorCall(3037);}

/*
=for apidoc hv_scalar

Evaluates the hash in scalar context and returns the result. Handles magic when the hash is tied.

=cut
*/

SV *
Perl_hv_scalar(pTHX_ HV *hv)
{
    SensorCall(3038);SV *sv;

    PERL_ARGS_ASSERT_HV_SCALAR;

    SensorCall(3041);if (SvRMAGICAL(hv)) {
	SensorCall(3039);MAGIC * const mg = mg_find((const SV *)hv, PERL_MAGIC_tied);
	SensorCall(3040);if (mg)
	    return magic_scalarpack(hv, mg);
    }

    SensorCall(3042);sv = sv_newmortal();
    SensorCall(3044);if (HvTOTALKEYS((const HV *)hv)) 
        {/*101*/SensorCall(3043);Perl_sv_setpvf(aTHX_ sv, "%ld/%ld",
                (long)HvFILL(hv), (long)HvMAX(hv) + 1);/*102*/}
    else
        sv_setiv(sv, 0);
    
    {SV * ReplaceReturn1645 = sv; SensorCall(3045); return ReplaceReturn1645;}
}

/*
=for apidoc hv_delete

Deletes a key/value pair in the hash.  The value's SV is removed from
the hash, made mortal, and returned to the caller.  The absolute
value of C<klen> is the length of the key.  If C<klen> is negative the
key is assumed to be in UTF-8-encoded Unicode.  The C<flags> value
will normally be zero; if set to G_DISCARD then NULL will be returned.
NULL will also be returned if the key is not found.

=for apidoc hv_delete_ent

Deletes a key/value pair in the hash.  The value SV is removed from the hash,
made mortal, and returned to the caller.  The C<flags> value will normally be
zero; if set to G_DISCARD then NULL will be returned.  NULL will also be
returned if the key is not found.  C<hash> can be a valid precomputed hash
value, or 0 to ask for it to be computed.

=cut
*/

STATIC SV *
S_hv_delete_common(pTHX_ HV *hv, SV *keysv, const char *key, STRLEN klen,
		   int k_flags, I32 d_flags, U32 hash)
{
SensorCall(3046);    dVAR;
    register XPVHV* xhv;
    register HE *entry;
    register HE **oentry;
    bool is_utf8 = (k_flags & HVhek_UTF8) ? TRUE : FALSE;
    int masked_flags;

    SensorCall(3055);if (SvRMAGICAL(hv)) {
	bool needs_copy;
	bool needs_store;
	hv_magic_check (hv, &needs_copy, &needs_store);

	SensorCall(3054);if (needs_copy) {
	    SensorCall(3047);SV *sv;
	    entry = (HE *) hv_common(hv, keysv, key, klen,
				     k_flags & ~HVhek_FREEKEY,
				     HV_FETCH_LVALUE|HV_DISABLE_UVAR_XKEY,
				     NULL, hash);
	    sv = entry ? HeVAL(entry) : NULL;
	    SensorCall(3053);if (sv) {
		SensorCall(3048);if (SvMAGICAL(sv)) {
		    mg_clear(sv);
		}
		SensorCall(3052);if (!needs_store) {
		    SensorCall(3049);if (mg_find(sv, PERL_MAGIC_tiedelem)) {
			/* No longer an element */
			sv_unmagic(sv, PERL_MAGIC_tiedelem);
			{SV * ReplaceReturn1644 = sv; SensorCall(3050); return ReplaceReturn1644;}
		    }		
		    {SV * ReplaceReturn1643 = NULL; SensorCall(3051); return ReplaceReturn1643;}		/* element cannot be deleted */
		}
#ifdef ENV_IS_CASELESS
		else if (mg_find((const SV *)hv, PERL_MAGIC_env)) {
		    /* XXX This code isn't UTF8 clean.  */
		    keysv = newSVpvn_flags(key, klen, SVs_TEMP);
		    if (k_flags & HVhek_FREEKEY) {
			Safefree(key);
		    }
		    key = strupr(SvPVX(keysv));
		    is_utf8 = 0;
		    k_flags = 0;
		    hash = 0;
		}
#endif
	    }
	}
    }
    SensorCall(3056);xhv = (XPVHV*)SvANY(hv);
    SensorCall(3057);if (!HvARRAY(hv))
	return NULL;

    SensorCall(3063);if (is_utf8) {
	SensorCall(3058);const char * const keysave = key;
	key = (char*)bytes_from_utf8((U8*)key, &klen, &is_utf8);

        SensorCall(3059);if (is_utf8)
            k_flags |= HVhek_UTF8;
	else
            k_flags &= ~HVhek_UTF8;
        SensorCall(3062);if (key != keysave) {
	    SensorCall(3060);if (k_flags & HVhek_FREEKEY) {
		/* This shouldn't happen if our caller does what we expect,
		   but strictly the API allows it.  */
		Safefree(keysave);
	    }
	    SensorCall(3061);k_flags |= HVhek_WASUTF8 | HVhek_FREEKEY;
	}
        HvHASKFLAGS_on(MUTABLE_SV(hv));
    }

    SensorCall(3064);if (HvREHASH(hv) || (!hash && !(keysv && (SvIsCOW_shared_hash(keysv)))))
	PERL_HASH_INTERNAL_(hash, key, klen, HvREHASH(hv));
    else if (!hash)
	hash = SvSHARED_HASH(keysv);

    SensorCall(3065);masked_flags = (k_flags & HVhek_MASK);

    oentry = &(HvARRAY(hv))[hash & (I32) HvMAX(hv)];
    entry = *oentry;
    SensorCall(3103);for (; entry; oentry = &HeNEXT(entry), entry = *oentry) {
	SensorCall(3066);SV *sv;
	U8 mro_changes = 0; /* 1 = isa; 2 = package moved */
	GV *gv = NULL;
	HV *stash = NULL;

	SensorCall(3068);if (HeHASH(entry) != hash)		/* strings can't be equal */
	    {/*151*/SensorCall(3067);continue;/*152*/}
	SensorCall(3070);if (HeKLEN(entry) != (I32)klen)
	    {/*153*/SensorCall(3069);continue;/*154*/}
	SensorCall(3072);if (HeKEY(entry) != key && memNE(HeKEY(entry),key,klen))	/* is this it? */
	    {/*155*/SensorCall(3071);continue;/*156*/}
	SensorCall(3074);if ((HeKFLAGS(entry) ^ masked_flags) & HVhek_UTF8)
	    {/*157*/SensorCall(3073);continue;/*158*/}

	SensorCall(3077);if (hv == PL_strtab) {
	    SensorCall(3075);if (k_flags & HVhek_FREEKEY)
		Safefree(key);
	    SensorCall(3076);Perl_croak(aTHX_ S_strtab_error, "delete");
	}

	/* if placeholder is here, it's already been deleted.... */
	SensorCall(3080);if (HeVAL(entry) == &PL_sv_placeholder) {
	    SensorCall(3078);if (k_flags & HVhek_FREEKEY)
		Safefree(key);
	    {SV * ReplaceReturn1642 = NULL; SensorCall(3079); return ReplaceReturn1642;}
	}
	SensorCall(3081);if (SvREADONLY(hv) && HeVAL(entry) && SvREADONLY(HeVAL(entry))
	 && !SvIsCOW(HeVAL(entry))) {
	    hv_notallowed(k_flags, key, klen,
			    "Attempt to delete readonly key '%"SVf"' from"
			    " a restricted hash");
	}
        SensorCall(3082);if (k_flags & HVhek_FREEKEY)
            Safefree(key);

	/* If this is a stash and the key ends with ::, then someone is 
	 * deleting a package.
	 */
	SensorCall(3089);if (HeVAL(entry) && HvENAME_get(hv)) {
		SensorCall(3083);gv = (GV *)HeVAL(entry);
		SensorCall(3084);if (keysv) key = SvPV(keysv, klen);
		SensorCall(3088);if ((
		     (klen > 1 && key[klen-2] == ':' && key[klen-1] == ':')
		      ||
		     (klen == 1 && key[0] == ':')
		    )
		 && (klen != 6 || hv!=PL_defstash || memNE(key,"main::",6))
		 && SvTYPE(gv) == SVt_PVGV && (stash = GvHV((GV *)gv))
		 && HvENAME_get(stash)) {
			/* A previous version of this code checked that the
			 * GV was still in the symbol table by fetching the
			 * GV with its name. That is not necessary (and
			 * sometimes incorrect), as HvENAME cannot be set
			 * on hv if it is not in the symtab. */
			SensorCall(3085);mro_changes = 2;
			/* Hang on to it for a bit. */
			SvREFCNT_inc_simple_void_NN(
			 sv_2mortal((SV *)gv)
			);
		}
		else {/*159*/SensorCall(3086);if (klen == 3 && strnEQ(key, "ISA", 3))
		    {/*161*/SensorCall(3087);mro_changes = 1;/*162*/}/*160*/}
	}

	SensorCall(3090);sv = d_flags & G_DISCARD ? HeVAL(entry) : sv_2mortal(HeVAL(entry));
	HeVAL(entry) = &PL_sv_placeholder;
	SensorCall(3092);if (sv) {
	    /* deletion of method from stash */
	    SensorCall(3091);if (isGV(sv) && isGV_with_GP(sv) && GvCVu(sv)
	     && HvENAME_get(hv))
		mro_method_changed_in(hv);
	}

	/*
	 * If a restricted hash, rather than really deleting the entry, put
	 * a placeholder there. This marks the key as being "approved", so
	 * we can still access via not-really-existing key without raising
	 * an error.
	 */
	SensorCall(3098);if (SvREADONLY(hv))
	    /* We'll be saving this slot, so the number of allocated keys
	     * doesn't go down, but the number placeholders goes up */
	    HvPLACEHOLDERS(hv)++;
	else {
	    SensorCall(3093);*oentry = HeNEXT(entry);
	    SensorCall(3095);if (SvOOK(hv) && entry == HvAUX(hv)->xhv_eiter /* HvEITER(hv) */)
		HvLAZYDEL_on(hv);
	    else {
		SensorCall(3094);if (SvOOK(hv) && HvLAZYDEL(hv) &&
		    entry == HeNEXT(HvAUX(hv)->xhv_eiter))
		    HeNEXT(HvAUX(hv)->xhv_eiter) = HeNEXT(entry);
		hv_free_ent(hv, entry);
	    }
	    SensorCall(3096);xhv->xhv_keys--; /* HvTOTALKEYS(hv)-- */
	    SensorCall(3097);if (xhv->xhv_keys == 0)
	        HvHASKFLAGS_off(hv);
	}

	SensorCall(3100);if (d_flags & G_DISCARD) {
	    SvREFCNT_dec(sv);
	    SensorCall(3099);sv = NULL;
	}

	SensorCall(3101);if (mro_changes == 1) mro_isa_changed_in(hv);
	else if (mro_changes == 2)
	    mro_package_moved(NULL, stash, gv, 1);

	{SV * ReplaceReturn1641 = sv; SensorCall(3102); return ReplaceReturn1641;}
    }
    SensorCall(3104);if (SvREADONLY(hv)) {
	hv_notallowed(k_flags, key, klen,
			"Attempt to delete disallowed key '%"SVf"' from"
			" a restricted hash");
    }

    SensorCall(3105);if (k_flags & HVhek_FREEKEY)
	Safefree(key);
    {SV * ReplaceReturn1640 = NULL; SensorCall(3106); return ReplaceReturn1640;}
}

STATIC void
S_hsplit(pTHX_ HV *hv)
{
SensorCall(3107);    dVAR;
    register XPVHV* const xhv = (XPVHV*)SvANY(hv);
    const I32 oldsize = (I32) xhv->xhv_max+1; /* HvMAX(hv)+1 (sick) */
    register I32 newsize = oldsize * 2;
    register I32 i;
    char *a = (char*) HvARRAY(hv);
    register HE **aep;
    int longest_chain = 0;
    int was_shared;

    PERL_ARGS_ASSERT_HSPLIT;

    /*PerlIO_printf(PerlIO_stderr(), "hsplit called for %p which had %d\n",
      (void*)hv, (int) oldsize);*/

    SensorCall(3108);if (HvPLACEHOLDERS_get(hv) && !SvREADONLY(hv)) {
      /* Can make this clear any placeholders first for non-restricted hashes,
	 even though Storable rebuilds restricted hashes by putting in all the
	 placeholders (first) before turning on the readonly flag, because
	 Storable always pre-splits the hash.  */
      hv_clear_placeholders(hv);
    }
	       
    PL_nomemok = TRUE;
#if defined(STRANGE_MALLOC) || defined(MYMALLOC)
    Renew(a, PERL_HV_ARRAY_ALLOC_BYTES(newsize)
	  + (SvOOK(hv) ? sizeof(struct xpvhv_aux) : 0), char);
    if (!a) {
      PL_nomemok = FALSE;
      return;
    }
    if (SvOOK(hv)) {
	Move(&a[oldsize * sizeof(HE*)], &a[newsize * sizeof(HE*)], 1, struct xpvhv_aux);
    }
#else
    Newx(a, PERL_HV_ARRAY_ALLOC_BYTES(newsize)
	+ (SvOOK(hv) ? sizeof(struct xpvhv_aux) : 0), char);
    SensorCall(3110);if (!a) {
      PL_nomemok = FALSE;
      SensorCall(3109);return;
    }
    Copy(HvARRAY(hv), a, oldsize * sizeof(HE*), char);
    SensorCall(3111);if (SvOOK(hv)) {
	Copy(HvAUX(hv), &a[newsize * sizeof(HE*)], 1, struct xpvhv_aux);
    }
    Safefree(HvARRAY(hv));
#endif

    PL_nomemok = FALSE;
    Zero(&a[oldsize * sizeof(HE*)], (newsize-oldsize) * sizeof(HE*), char);	/* zero 2nd half*/
    SensorCall(3112);xhv->xhv_max = --newsize;	/* HvMAX(hv) = --newsize */
    HvARRAY(hv) = (HE**) a;
    aep = (HE**)a;

    SensorCall(3126);for (i=0; i<oldsize; i++,aep++) {
	SensorCall(3113);int left_length = 0;
	int right_length = 0;
	HE **oentry = aep;
	HE *entry = *aep;
	register HE **bep;

	SensorCall(3115);if (!entry)				/* non-existent */
	    {/*145*/SensorCall(3114);continue;/*146*/}
	SensorCall(3116);bep = aep+oldsize;
	SensorCall(3121);do {
	    SensorCall(3117);if ((HeHASH(entry) & newsize) != (U32)i) {
		SensorCall(3118);*oentry = HeNEXT(entry);
		HeNEXT(entry) = *bep;
		*bep = entry;
		right_length++;
	    }
	    else {
		SensorCall(3119);oentry = &HeNEXT(entry);
		left_length++;
	    }
	    SensorCall(3120);entry = *oentry;
	} while (entry);
	/* I think we don't actually need to keep track of the longest length,
	   merely flag if anything is too long. But for the moment while
	   developing this code I'll track it.  */
	SensorCall(3123);if (left_length > longest_chain)
	    {/*147*/SensorCall(3122);longest_chain = left_length;/*148*/}
	SensorCall(3125);if (right_length > longest_chain)
	    {/*149*/SensorCall(3124);longest_chain = right_length;/*150*/}
    }


    /* Pick your policy for "hashing isn't working" here:  */
    SensorCall(3128);if (longest_chain <= HV_MAX_LENGTH_BEFORE_REHASH /* split worked?  */
	|| HvREHASH(hv)) {
	SensorCall(3127);return;
    }

    SensorCall(3130);if (hv == PL_strtab) {
	/* Urg. Someone is doing something nasty to the string table.
	   Can't win.  */
	SensorCall(3129);return;
    }

    /* Awooga. Awooga. Pathological data.  */
    /*PerlIO_printf(PerlIO_stderr(), "%p %d of %d with %d/%d buckets\n", (void*)hv,
      longest_chain, HvTOTALKEYS(hv), HvFILL(hv),  1+HvMAX(hv));*/

    SensorCall(3131);++newsize;
    Newxz(a, PERL_HV_ARRAY_ALLOC_BYTES(newsize)
	 + (SvOOK(hv) ? sizeof(struct xpvhv_aux) : 0), char);
    SensorCall(3132);if (SvOOK(hv)) {
	Copy(HvAUX(hv), &a[newsize * sizeof(HE*)], 1, struct xpvhv_aux);
    }

    SensorCall(3133);was_shared = HvSHAREKEYS(hv);

    HvSHAREKEYS_off(hv);
    HvREHASH_on(hv);

    aep = HvARRAY(hv);

    SensorCall(3140);for (i=0; i<newsize; i++,aep++) {
	SensorCall(3134);register HE *entry = *aep;
	SensorCall(3139);while (entry) {
	    /* We're going to trash this HE's next pointer when we chain it
	       into the new hash below, so store where we go next.  */
	    SensorCall(3135);HE * const next = HeNEXT(entry);
	    UV hash;
	    HE **bep;

	    /* Rehash it */
	    PERL_HASH_INTERNAL(hash, HeKEY(entry), HeKLEN(entry));

	    SensorCall(3137);if (was_shared) {
		/* Unshare it.  */
		SensorCall(3136);HEK * const new_hek
		    = save_hek_flags(HeKEY(entry), HeKLEN(entry),
				     hash, HeKFLAGS(entry));
		unshare_hek (HeKEY_hek(entry));
		HeKEY_hek(entry) = new_hek;
	    } else {
		/* Not shared, so simply write the new hash in. */
		HeHASH(entry) = hash;
	    }
	    /*PerlIO_printf(PerlIO_stderr(), "%d ", HeKFLAGS(entry));*/
	    HEK_REHASH_on(HeKEY_hek(entry));
	    /*PerlIO_printf(PerlIO_stderr(), "%d\n", HeKFLAGS(entry));*/

	    /* Copy oentry to the correct new chain.  */
	    SensorCall(3138);bep = ((HE**)a) + (hash & (I32) xhv->xhv_max);
	    HeNEXT(entry) = *bep;
	    *bep = entry;

	    entry = next;
	}
    }
    Safefree (HvARRAY(hv));
    HvARRAY(hv) = (HE **)a;
}

void
Perl_hv_ksplit(pTHX_ HV *hv, IV newmax)
{
SensorCall(3141);    dVAR;
    register XPVHV* xhv = (XPVHV*)SvANY(hv);
    const I32 oldsize = (I32) xhv->xhv_max+1; /* HvMAX(hv)+1 (sick) */
    register I32 newsize;
    register I32 i;
    register char *a;
    register HE **aep;

    PERL_ARGS_ASSERT_HV_KSPLIT;

    newsize = (I32) newmax;			/* possible truncation here */
    SensorCall(3143);if (newsize != newmax || newmax <= oldsize)
	{/*73*/SensorCall(3142);return;/*74*/}
    SensorCall(3145);while ((newsize & (1 + ~newsize)) != newsize) {
	SensorCall(3144);newsize &= ~(newsize & (1 + ~newsize));	/* get proper power of 2 */
    }
    SensorCall(3147);if (newsize < newmax)
	{/*75*/SensorCall(3146);newsize *= 2;/*76*/}
    SensorCall(3149);if (newsize < newmax)
	{/*77*/SensorCall(3148);return;/*78*/}					/* overflow detection */

    SensorCall(3150);a = (char *) HvARRAY(hv);
    SensorCall(3154);if (a) {
	PL_nomemok = TRUE;
#if defined(STRANGE_MALLOC) || defined(MYMALLOC)
	Renew(a, PERL_HV_ARRAY_ALLOC_BYTES(newsize)
	      + (SvOOK(hv) ? sizeof(struct xpvhv_aux) : 0), char);
	if (!a) {
	  PL_nomemok = FALSE;
	  return;
	}
	if (SvOOK(hv)) {
	    Copy(&a[oldsize * sizeof(HE*)], &a[newsize * sizeof(HE*)], 1, struct xpvhv_aux);
	}
#else
	Newx(a, PERL_HV_ARRAY_ALLOC_BYTES(newsize)
	    + (SvOOK(hv) ? sizeof(struct xpvhv_aux) : 0), char);
	SensorCall(3152);if (!a) {
	  PL_nomemok = FALSE;
	  SensorCall(3151);return;
	}
	Copy(HvARRAY(hv), a, oldsize * sizeof(HE*), char);
	SensorCall(3153);if (SvOOK(hv)) {
	    Copy(HvAUX(hv), &a[newsize * sizeof(HE*)], 1, struct xpvhv_aux);
	}
	Safefree(HvARRAY(hv));
#endif
	PL_nomemok = FALSE;
	Zero(&a[oldsize * sizeof(HE*)], (newsize-oldsize) * sizeof(HE*), char); /* zero 2nd half*/
    }
    else {
	Newxz(a, PERL_HV_ARRAY_ALLOC_BYTES(newsize), char);
    }
    SensorCall(3155);xhv->xhv_max = --newsize; 	/* HvMAX(hv) = --newsize */
    HvARRAY(hv) = (HE **) a;
    SensorCall(3157);if (!xhv->xhv_keys /* !HvTOTALKEYS(hv) */)	/* skip rest if no entries */
	{/*79*/SensorCall(3156);return;/*80*/}

    SensorCall(3158);aep = (HE**)a;
    SensorCall(3167);for (i=0; i<oldsize; i++,aep++) {
	SensorCall(3159);HE **oentry = aep;
	HE *entry = *aep;

	SensorCall(3161);if (!entry)				/* non-existent */
	    {/*81*/SensorCall(3160);continue;/*82*/}
	SensorCall(3166);do {
	    SensorCall(3162);register I32 j = (HeHASH(entry) & newsize);

	    SensorCall(3164);if (j != i) {
		SensorCall(3163);j -= i;
		*oentry = HeNEXT(entry);
		HeNEXT(entry) = aep[j];
		aep[j] = entry;
	    }
	    else
		oentry = &HeNEXT(entry);
	    SensorCall(3165);entry = *oentry;
	} while (entry);
    }
SensorCall(3168);}

HV *
Perl_newHVhv(pTHX_ HV *ohv)
{
SensorCall(3169);    dVAR;
    HV * const hv = newHV();
    STRLEN hv_max;

    SensorCall(3171);if (!ohv || (!HvTOTALKEYS(ohv) && !SvMAGICAL((const SV *)ohv)))
	{/*107*/{HV * ReplaceReturn1639 = hv; SensorCall(3170); return ReplaceReturn1639;}/*108*/}
    SensorCall(3172);hv_max = HvMAX(ohv);

    SensorCall(3190);if (!SvMAGICAL((const SV *)ohv)) {
	/* It's an ordinary hash, so copy it fast. AMS 20010804 */
	SensorCall(3173);STRLEN i;
	const bool shared = !!HvSHAREKEYS(ohv);
	HE **ents, ** const oents = (HE **)HvARRAY(ohv);
	char *a;
	Newx(a, PERL_HV_ARRAY_ALLOC_BYTES(hv_max+1), char);
	ents = (HE**)a;

	/* In each bucket... */
	SensorCall(3183);for (i = 0; i <= hv_max; i++) {
	    SensorCall(3174);HE *prev = NULL;
	    HE *oent = oents[i];

	    SensorCall(3177);if (!oent) {
		SensorCall(3175);ents[i] = NULL;
		SensorCall(3176);continue;
	    }

	    /* Copy the linked list of entries. */
	    SensorCall(3182);for (; oent; oent = HeNEXT(oent)) {
		SensorCall(3178);const U32 hash   = HeHASH(oent);
		const char * const key = HeKEY(oent);
		const STRLEN len = HeKLEN(oent);
		const int flags  = HeKFLAGS(oent);
		HE * const ent   = new_HE();
		SV *const val    = HeVAL(oent);

		HeVAL(ent) = SvIMMORTAL(val) ? val : newSVsv(val);
		HeKEY_hek(ent)
                    = shared ? share_hek_flags(key, len, hash, flags)
                             :  save_hek_flags(key, len, hash, flags);
		SensorCall(3180);if (prev)
		    HeNEXT(prev) = ent;
		else
		    {/*109*/SensorCall(3179);ents[i] = ent;/*110*/}
		SensorCall(3181);prev = ent;
		HeNEXT(ent) = NULL;
	    }
	}

	HvMAX(hv)   = hv_max;
	HvTOTALKEYS(hv)  = HvTOTALKEYS(ohv);
	HvARRAY(hv) = ents;
    } /* not magical */
    else {
	/* Iterate over ohv, copying keys and values one at a time. */
	SensorCall(3184);HE *entry;
	const I32 riter = HvRITER_get(ohv);
	HE * const eiter = HvEITER_get(ohv);
	STRLEN hv_fill = HvFILL(ohv);

	/* Can we use fewer buckets? (hv_max is always 2^n-1) */
	SensorCall(3186);while (hv_max && hv_max + 1 >= hv_fill * 2)
	    {/*111*/SensorCall(3185);hv_max = hv_max / 2;/*112*/}
	HvMAX(hv) = hv_max;

	hv_iterinit(ohv);
	SensorCall(3189);while ((entry = hv_iternext_flags(ohv, 0))) {
	    SensorCall(3187);SV *val = hv_iterval(ohv,entry);
	    SV * const keysv = HeSVKEY(entry);
	    val = SvIMMORTAL(val) ? val : newSVsv(val);
	    SensorCall(3188);if (keysv)
		(void)hv_store_ent(hv, keysv, val, 0);
	    else
	        (void)hv_store_flags(hv, HeKEY(entry), HeKLEN(entry), val,
				 HeHASH(entry), HeKFLAGS(entry));
	}
	HvRITER_set(ohv, riter);
	HvEITER_set(ohv, eiter);
    }

    {HV * ReplaceReturn1638 = hv; SensorCall(3191); return ReplaceReturn1638;}
}

/*
=for apidoc Am|HV *|hv_copy_hints_hv|HV *ohv

A specialised version of L</newHVhv> for copying C<%^H>.  I<ohv> must be
a pointer to a hash (which may have C<%^H> magic, but should be generally
non-magical), or C<NULL> (interpreted as an empty hash).  The content
of I<ohv> is copied to a new hash, which has the C<%^H>-specific magic
added to it.  A pointer to the new hash is returned.

=cut
*/

HV *
Perl_hv_copy_hints_hv(pTHX_ HV *const ohv)
{
    SensorCall(3192);HV * const hv = newHV();

    SensorCall(3202);if (ohv) {
	SensorCall(3193);STRLEN hv_max = HvMAX(ohv);
	STRLEN hv_fill = HvFILL(ohv);
	HE *entry;
	const I32 riter = HvRITER_get(ohv);
	HE * const eiter = HvEITER_get(ohv);

	ENTER;
	SAVEFREESV(hv);

	SensorCall(3195);while (hv_max && hv_max + 1 >= hv_fill * 2)
	    {/*27*/SensorCall(3194);hv_max = hv_max / 2;/*28*/}
	HvMAX(hv) = hv_max;

	hv_iterinit(ohv);
	SensorCall(3201);while ((entry = hv_iternext_flags(ohv, 0))) {
	    SensorCall(3196);SV *const sv = newSVsv(hv_iterval(ohv,entry));
	    SV *heksv = HeSVKEY(entry);
	    SensorCall(3197);if (!heksv && sv) heksv = newSVhek(HeKEY_hek(entry));
	    SensorCall(3198);if (sv) sv_magic(sv, NULL, PERL_MAGIC_hintselem,
		     (char *)heksv, HEf_SVKEY);
	    SensorCall(3200);if (heksv == HeSVKEY(entry))
		(void)hv_store_ent(hv, heksv, sv, 0);
	    else {
		SensorCall(3199);(void)hv_common(hv, heksv, HeKEY(entry), HeKLEN(entry),
				 HeKFLAGS(entry), HV_FETCH_ISSTORE|HV_FETCH_JUST_SV, sv, HeHASH(entry));
		SvREFCNT_dec(heksv);
	    }
	}
	HvRITER_set(ohv, riter);
	HvEITER_set(ohv, eiter);

	SvREFCNT_inc_simple_void_NN(hv);
	LEAVE;
    }
    hv_magic(hv, NULL, PERL_MAGIC_hints);
    {HV * ReplaceReturn1637 = hv; SensorCall(3203); return ReplaceReturn1637;}
}

/* like hv_free_ent, but returns the SV rather than freeing it */
STATIC SV*
S_hv_free_ent_ret(pTHX_ HV *hv, register HE *entry)
{
SensorCall(3204);    dVAR;
    SV *val;

    PERL_ARGS_ASSERT_HV_FREE_ENT_RET;

    SensorCall(3205);if (!entry)
	return NULL;
    SensorCall(3206);val = HeVAL(entry);
    SensorCall(3207);if (HeKLEN(entry) == HEf_SVKEY) {
	SvREFCNT_dec(HeKEY_sv(entry));
	Safefree(HeKEY_hek(entry));
    }
    else if (HvSHAREKEYS(hv))
	unshare_hek(HeKEY_hek(entry));
    else
	Safefree(HeKEY_hek(entry));
    del_HE(entry);
    {SV * ReplaceReturn1636 = val; SensorCall(3208); return ReplaceReturn1636;}
}


void
Perl_hv_free_ent(pTHX_ HV *hv, register HE *entry)
{
SensorCall(3209);    dVAR;
    SV *val;

    PERL_ARGS_ASSERT_HV_FREE_ENT;

    SensorCall(3211);if (!entry)
	{/*65*/SensorCall(3210);return;/*66*/}
    SensorCall(3212);val = hv_free_ent_ret(hv, entry);
    SvREFCNT_dec(val);
}


void
Perl_hv_delayfree_ent(pTHX_ HV *hv, register HE *entry)
{
SensorCall(3213);    dVAR;

    PERL_ARGS_ASSERT_HV_DELAYFREE_ENT;

    SensorCall(3215);if (!entry)
	{/*29*/SensorCall(3214);return;/*30*/}
    /* SvREFCNT_inc to counter the SvREFCNT_dec in hv_free_ent  */
    sv_2mortal(SvREFCNT_inc(HeVAL(entry)));	/* free between statements */
    SensorCall(3216);if (HeKLEN(entry) == HEf_SVKEY) {
	sv_2mortal(SvREFCNT_inc(HeKEY_sv(entry)));
    }
    hv_free_ent(hv, entry);
}

/*
=for apidoc hv_clear

Frees the all the elements of a hash, leaving it empty.
The XS equivalent of C<%hash = ()>.  See also L</hv_undef>.

If any destructors are triggered as a result, the hv itself may
be freed.

=cut
*/

void
Perl_hv_clear(pTHX_ HV *hv)
{
SensorCall(3217);    dVAR;
    register XPVHV* xhv;
    SensorCall(3219);if (!hv)
	{/*5*/SensorCall(3218);return;/*6*/}

    DEBUG_A(Perl_hv_assert(aTHX_ hv));

    SensorCall(3220);xhv = (XPVHV*)SvANY(hv);

    ENTER;
    SAVEFREESV(SvREFCNT_inc_simple_NN(hv));
    SensorCall(3229);if (SvREADONLY(hv) && HvARRAY(hv) != NULL) {
	/* restricted hash: convert all keys to placeholders */
	SensorCall(3221);STRLEN i;
	SensorCall(3227);for (i = 0; i <= xhv->xhv_max; i++) {
	    SensorCall(3222);HE *entry = (HvARRAY(hv))[i];
	    SensorCall(3226);for (; entry; entry = HeNEXT(entry)) {
		/* not already placeholder */
		SensorCall(3223);if (HeVAL(entry) != &PL_sv_placeholder) {
		    SensorCall(3224);if (HeVAL(entry) && SvREADONLY(HeVAL(entry))
		     && !SvIsCOW(HeVAL(entry))) {
			SensorCall(3225);SV* const keysv = hv_iterkeysv(entry);
			Perl_croak(aTHX_
				   "Attempt to delete readonly key '%"SVf"' from a restricted hash",
				   (void*)keysv);
		    }
		    SvREFCNT_dec(HeVAL(entry));
		    HeVAL(entry) = &PL_sv_placeholder;
		    HvPLACEHOLDERS(hv)++;
		}
	    }
	}
    }
    else {
	hfreeentries(hv);
	HvPLACEHOLDERS_set(hv, 0);

	SensorCall(3228);if (SvRMAGICAL(hv))
	    mg_clear(MUTABLE_SV(hv));

	HvHASKFLAGS_off(hv);
	HvREHASH_off(hv);
    }
    SensorCall(3231);if (SvOOK(hv)) {
        SensorCall(3230);if(HvENAME_get(hv))
            mro_isa_changed_in(hv);
	HvEITER_set(hv, NULL);
    }
    LEAVE;
}

/*
=for apidoc hv_clear_placeholders

Clears any placeholders from a hash.  If a restricted hash has any of its keys
marked as readonly and the key is subsequently deleted, the key is not actually
deleted but is marked by assigning it a value of &PL_sv_placeholder.  This tags
it so it will be ignored by future operations such as iterating over the hash,
but will still allow the hash to have a value reassigned to the key at some
future point.  This function clears any such placeholder keys from the hash.
See Hash::Util::lock_keys() for an example of its use.

=cut
*/

void
Perl_hv_clear_placeholders(pTHX_ HV *hv)
{
SensorCall(3232);    dVAR;
    const U32 items = (U32)HvPLACEHOLDERS_get(hv);

    PERL_ARGS_ASSERT_HV_CLEAR_PLACEHOLDERS;

    SensorCall(3233);if (items)
	clear_placeholders(hv, items);
SensorCall(3234);}

static void
S_clear_placeholders(pTHX_ HV *hv, U32 items)
{
SensorCall(3235);    dVAR;
    I32 i;

    PERL_ARGS_ASSERT_CLEAR_PLACEHOLDERS;

    SensorCall(3237);if (items == 0)
	{/*143*/SensorCall(3236);return;/*144*/}

    SensorCall(3238);i = HvMAX(hv);
    SensorCall(3249);do {
	/* Loop down the linked list heads  */
	SensorCall(3239);HE **oentry = &(HvARRAY(hv))[i];
	HE *entry;

	SensorCall(3248);while ((entry = *oentry)) {
	    SensorCall(3240);if (HeVAL(entry) == &PL_sv_placeholder) {
		SensorCall(3241);*oentry = HeNEXT(entry);
		SensorCall(3243);if (entry == HvEITER_get(hv))
		    HvLAZYDEL_on(hv);
		else {
		    SensorCall(3242);if (SvOOK(hv) && HvLAZYDEL(hv) &&
			entry == HeNEXT(HvAUX(hv)->xhv_eiter))
			HeNEXT(HvAUX(hv)->xhv_eiter) = HeNEXT(entry);
		    hv_free_ent(hv, entry);
		}

		SensorCall(3246);if (--items == 0) {
		    /* Finished.  */
		    HvTOTALKEYS(hv) -= (IV)HvPLACEHOLDERS_get(hv);
		    SensorCall(3244);if (HvUSEDKEYS(hv) == 0)
			HvHASKFLAGS_off(hv);
		    HvPLACEHOLDERS_set(hv, 0);
		    SensorCall(3245);return;
		}
	    } else {
		SensorCall(3247);oentry = &HeNEXT(entry);
	    }
	}
    } while (--i >= 0);
    /* You can't get here, hence assertion should always fail.  */
    assert (items == 0);
    assert (0);
SensorCall(3250);}

STATIC void
S_hfreeentries(pTHX_ HV *hv)
{
    SensorCall(3251);STRLEN index = 0;
    XPVHV * const xhv = (XPVHV*)SvANY(hv);
    SV *sv;

    PERL_ARGS_ASSERT_HFREEENTRIES;

    SensorCall(3252);while ((sv = Perl_hfree_next_entry(aTHX_ hv, &index))||xhv->xhv_keys) {
	SvREFCNT_dec(sv);
    }
SensorCall(3253);}


/* hfree_next_entry()
 * For use only by S_hfreeentries() and sv_clear().
 * Delete the next available HE from hv and return the associated SV.
 * Returns null on empty hash. Nevertheless null is not a reliable
 * indicator that the hash is empty, as the deleted entry may have a
 * null value.
 * indexp is a pointer to the current index into HvARRAY. The index should
 * initially be set to 0. hfree_next_entry() may update it.  */

SV*
Perl_hfree_next_entry(pTHX_ HV *hv, STRLEN *indexp)
{
    SensorCall(3254);struct xpvhv_aux *iter;
    HE *entry;
    HE ** array;
#ifdef DEBUGGING
    STRLEN orig_index = *indexp;
#endif

    PERL_ARGS_ASSERT_HFREE_NEXT_ENTRY;

    SensorCall(3257);if (SvOOK(hv) && ((iter = HvAUX(hv)))
	&& ((entry = iter->xhv_eiter)) )
    {
	/* the iterator may get resurrected after each
	 * destructor call, so check each time */
	SensorCall(3255);if (entry && HvLAZYDEL(hv)) {	/* was deleted earlier? */
	    HvLAZYDEL_off(hv);
	    hv_free_ent(hv, entry);
	    /* warning: at this point HvARRAY may have been
	     * re-allocated, HvMAX changed etc */
	}
	SensorCall(3256);iter->xhv_riter = -1; 	/* HvRITER(hv) = -1 */
	iter->xhv_eiter = NULL;	/* HvEITER(hv) = NULL */
    }

    SensorCall(3258);if (!((XPVHV*)SvANY(hv))->xhv_keys)
	return NULL;

    SensorCall(3259);array = HvARRAY(hv);
    assert(array);
    SensorCall(3262);while ( ! ((entry = array[*indexp])) ) {
	SensorCall(3260);if ((*indexp)++ >= HvMAX(hv))
	    {/*189*/SensorCall(3261);*indexp = 0;/*190*/}
	assert(*indexp != orig_index);
    }
    SensorCall(3263);array[*indexp] = HeNEXT(entry);
    ((XPVHV*) SvANY(hv))->xhv_keys--;

    SensorCall(3266);if (   PL_phase != PERL_PHASE_DESTRUCT && HvENAME(hv)
	&& HeVAL(entry) && isGV(HeVAL(entry))
	&& GvHV(HeVAL(entry)) && HvENAME(GvHV(HeVAL(entry)))
    ) {
	SensorCall(3264);STRLEN klen;
	const char * const key = HePV(entry,klen);
	SensorCall(3265);if ((klen > 1 && key[klen-1]==':' && key[klen-2]==':')
	 || (klen == 1 && key[0] == ':')) {
	    mro_package_moved(
	     NULL, GvHV(HeVAL(entry)),
	     (GV *)HeVAL(entry), 0
	    );
	}
    }
    {SV * ReplaceReturn1635 = hv_free_ent_ret(hv, entry); SensorCall(3267); return ReplaceReturn1635;}
}


/*
=for apidoc hv_undef

Undefines the hash.  The XS equivalent of C<undef(%hash)>.

As well as freeing all the elements of the hash (like hv_clear()), this
also frees any auxiliary data and storage associated with the hash.

If any destructors are triggered as a result, the hv itself may
be freed.

See also L</hv_clear>.

=cut
*/

void
Perl_hv_undef_flags(pTHX_ HV *hv, U32 flags)
{
SensorCall(3268);    dVAR;
    register XPVHV* xhv;
    const char *name;
    const bool save = !!SvREFCNT(hv);

    SensorCall(3270);if (!hv)
	{/*103*/SensorCall(3269);return;/*104*/}
    DEBUG_A(Perl_hv_assert(aTHX_ hv));
    SensorCall(3271);xhv = (XPVHV*)SvANY(hv);

    /* The name must be deleted before the call to hfreeeeentries so that
       CVs are anonymised properly. But the effective name must be pre-
       served until after that call (and only deleted afterwards if the
       call originated from sv_clear). For stashes with one name that is
       both the canonical name and the effective name, hv_name_set has to
       allocate an array for storing the effective name. We can skip that
       during global destruction, as it does not matter where the CVs point
       if they will be freed anyway. */
    /* note that the code following prior to hfreeentries is duplicated
     * in sv_clear(), and changes here should be done there too */
    SensorCall(3273);if (PL_phase != PERL_PHASE_DESTRUCT && (name = HvNAME(hv))) {
        SensorCall(3272);if (PL_stashcache)
	    (void)hv_delete(PL_stashcache, name,
                            HEK_UTF8(HvNAME_HEK(hv)) ? -HvNAMELEN_get(hv) : HvNAMELEN_get(hv),
                            G_DISCARD
                           );
	hv_name_set(hv, NULL, 0, 0);
    }
    SensorCall(3274);if (save) {
	ENTER;
	SAVEFREESV(SvREFCNT_inc_simple_NN(hv));
    }
    hfreeentries(hv);
    SensorCall(3289);if (SvOOK(hv)) {
      SensorCall(3275);struct xpvhv_aux * const aux = HvAUX(hv);
      struct mro_meta *meta;

      SensorCall(3278);if ((name = HvENAME_get(hv))) {
	SensorCall(3276);if (PL_phase != PERL_PHASE_DESTRUCT)
	    mro_isa_changed_in(hv);
        SensorCall(3277);if (PL_stashcache)
	    (void)hv_delete(
	            PL_stashcache, name,
                    HEK_UTF8(HvENAME_HEK(hv)) ? -HvENAMELEN_get(hv) : HvENAMELEN_get(hv),
                    G_DISCARD
	          );
      }

      /* If this call originated from sv_clear, then we must check for
       * effective names that need freeing, as well as the usual name. */
      SensorCall(3279);name = HvNAME(hv);
      SensorCall(3281);if (flags & HV_NAME_SETALL ? !!aux->xhv_name_u.xhvnameu_name : !!name) {
        SensorCall(3280);if (name && PL_stashcache)
	    (void)hv_delete(PL_stashcache, name, (HEK_UTF8(HvNAME_HEK(hv)) ? -HvNAMELEN_get(hv) : HvNAMELEN_get(hv)), G_DISCARD);
	hv_name_set(hv, NULL, 0, flags);
      }
      SensorCall(3287);if((meta = aux->xhv_mro_meta)) {
	SensorCall(3282);if (meta->mro_linear_all) {
	    SvREFCNT_dec(MUTABLE_SV(meta->mro_linear_all));
	    SensorCall(3283);meta->mro_linear_all = NULL;
	    /* This is just acting as a shortcut pointer.  */
	    meta->mro_linear_current = NULL;
	} else {/*105*/SensorCall(3284);if (meta->mro_linear_current) {
	    /* Only the current MRO is stored, so this owns the data.
	     */
	    SvREFCNT_dec(meta->mro_linear_current);
	    SensorCall(3285);meta->mro_linear_current = NULL;
	;/*106*/}}
	SvREFCNT_dec(meta->mro_nextmethod);
	SvREFCNT_dec(meta->isa);
	Safefree(meta);
	SensorCall(3286);aux->xhv_mro_meta = NULL;
      }
      SensorCall(3288);if (!aux->xhv_name_u.xhvnameu_name && ! aux->xhv_backreferences)
	SvFLAGS(hv) &= ~SVf_OOK;
    }
    SensorCall(3291);if (!SvOOK(hv)) {
	Safefree(HvARRAY(hv));
	SensorCall(3290);xhv->xhv_max   = 7;	/* HvMAX(hv) = 7 (it's a normal hash) */
	HvARRAY(hv) = 0;
    }
    /* if we're freeing the HV, the SvMAGIC field has been reused for
     * other purposes, and so there can't be any placeholder magic */
    SensorCall(3292);if (SvREFCNT(hv))
	HvPLACEHOLDERS_set(hv, 0);

    SensorCall(3293);if (SvRMAGICAL(hv))
	mg_clear(MUTABLE_SV(hv));
    SensorCall(3294);if (save) LEAVE;
SensorCall(3295);}

/*
=for apidoc hv_fill

Returns the number of hash buckets that happen to be in use. This function is
wrapped by the macro C<HvFILL>.

Previously this value was stored in the HV structure, rather than being
calculated on demand.

=cut
*/

STRLEN
Perl_hv_fill(pTHX_ HV const *const hv)
{
    SensorCall(3296);STRLEN count = 0;
    HE **ents = HvARRAY(hv);

    PERL_ARGS_ASSERT_HV_FILL;

    SensorCall(3301);if (ents) {
	SensorCall(3297);HE *const *const last = ents + HvMAX(hv);
	count = last + 1 - ents;

	SensorCall(3300);do {
	    SensorCall(3298);if (!*ents)
		{/*63*/SensorCall(3299);--count;/*64*/}
	} while (++ents <= last);
    }
    {STRLEN  ReplaceReturn1634 = count; SensorCall(3302); return ReplaceReturn1634;}
}

static struct xpvhv_aux*
S_hv_auxinit(HV *hv) {
    SensorCall(3303);struct xpvhv_aux *iter;
    char *array;

    PERL_ARGS_ASSERT_HV_AUXINIT;

    SensorCall(3305);if (!HvARRAY(hv)) {
	Newxz(array, PERL_HV_ARRAY_ALLOC_BYTES(HvMAX(hv) + 1)
	    + sizeof(struct xpvhv_aux), char);
    } else {
	SensorCall(3304);array = (char *) HvARRAY(hv);
	Renew(array, PERL_HV_ARRAY_ALLOC_BYTES(HvMAX(hv) + 1)
	      + sizeof(struct xpvhv_aux), char);
    }
    HvARRAY(hv) = (HE**) array;
    SvOOK_on(hv);
    SensorCall(3306);iter = HvAUX(hv);

    iter->xhv_riter = -1; 	/* HvRITER(hv) = -1 */
    iter->xhv_eiter = NULL;	/* HvEITER(hv) = NULL */
    iter->xhv_name_u.xhvnameu_name = 0;
    iter->xhv_name_count = 0;
    iter->xhv_backreferences = 0;
    iter->xhv_mro_meta = NULL;
    {__IASTRUCT__ xpvhv_aux * ReplaceReturn1633 = iter; SensorCall(3307); return ReplaceReturn1633;}
}

/*
=for apidoc hv_iterinit

Prepares a starting point to traverse a hash table.  Returns the number of
keys in the hash (i.e. the same as C<HvUSEDKEYS(hv)>).  The return value is
currently only meaningful for hashes without tie magic.

NOTE: Before version 5.004_65, C<hv_iterinit> used to return the number of
hash buckets that happen to be in use.  If you still need that esoteric
value, you can get it through the macro C<HvFILL(hv)>.


=cut
*/

I32
Perl_hv_iterinit(pTHX_ HV *hv)
{
    PERL_ARGS_ASSERT_HV_ITERINIT;

    /* FIXME: Are we not NULL, or do we croak? Place bets now! */

    SensorCall(3308);if (!hv)
	{/*67*/SensorCall(3309);Perl_croak(aTHX_ "Bad hash");/*68*/}

    SensorCall(3313);if (SvOOK(hv)) {
	SensorCall(3310);struct xpvhv_aux * const iter = HvAUX(hv);
	HE * const entry = iter->xhv_eiter; /* HvEITER(hv) */
	SensorCall(3311);if (entry && HvLAZYDEL(hv)) {	/* was deleted earlier? */
	    HvLAZYDEL_off(hv);
	    hv_free_ent(hv, entry);
	}
	SensorCall(3312);iter->xhv_riter = -1; 	/* HvRITER(hv) = -1 */
	iter->xhv_eiter = NULL; /* HvEITER(hv) = NULL */
    } else {
	hv_auxinit(hv);
    }

    /* used to be xhv->xhv_fill before 5.004_65 */
    {I32  ReplaceReturn1632 = HvTOTALKEYS(hv); SensorCall(3314); return ReplaceReturn1632;}
}

I32 *
Perl_hv_riter_p(pTHX_ HV *hv) {
    SensorCall(3315);struct xpvhv_aux *iter;

    PERL_ARGS_ASSERT_HV_RITER_P;

    SensorCall(3317);if (!hv)
	{/*95*/SensorCall(3316);Perl_croak(aTHX_ "Bad hash");/*96*/}

    SensorCall(3318);iter = SvOOK(hv) ? HvAUX(hv) : hv_auxinit(hv);
    {I32 * ReplaceReturn1631 = &(iter->xhv_riter); SensorCall(3319); return ReplaceReturn1631;}
}

HE **
Perl_hv_eiter_p(pTHX_ HV *hv) {
    SensorCall(3320);struct xpvhv_aux *iter;

    PERL_ARGS_ASSERT_HV_EITER_P;

    SensorCall(3322);if (!hv)
	{/*31*/SensorCall(3321);Perl_croak(aTHX_ "Bad hash");/*32*/}

    SensorCall(3323);iter = SvOOK(hv) ? HvAUX(hv) : hv_auxinit(hv);
    {HE ** ReplaceReturn1630 = &(iter->xhv_eiter); SensorCall(3324); return ReplaceReturn1630;}
}

void
Perl_hv_riter_set(pTHX_ HV *hv, I32 riter) {
    SensorCall(3325);struct xpvhv_aux *iter;

    PERL_ARGS_ASSERT_HV_RITER_SET;

    SensorCall(3327);if (!hv)
	{/*97*/SensorCall(3326);Perl_croak(aTHX_ "Bad hash");/*98*/}

    SensorCall(3332);if (SvOOK(hv)) {
	SensorCall(3328);iter = HvAUX(hv);
    } else {
	SensorCall(3329);if (riter == -1)
	    {/*99*/SensorCall(3330);return;/*100*/}

	SensorCall(3331);iter = hv_auxinit(hv);
    }
    SensorCall(3333);iter->xhv_riter = riter;
SensorCall(3334);}

void
Perl_hv_eiter_set(pTHX_ HV *hv, HE *eiter) {
    SensorCall(3335);struct xpvhv_aux *iter;

    PERL_ARGS_ASSERT_HV_EITER_SET;

    SensorCall(3337);if (!hv)
	{/*33*/SensorCall(3336);Perl_croak(aTHX_ "Bad hash");/*34*/}

    SensorCall(3342);if (SvOOK(hv)) {
	SensorCall(3338);iter = HvAUX(hv);
    } else {
	/* 0 is the default so don't go malloc()ing a new structure just to
	   hold 0.  */
	SensorCall(3339);if (!eiter)
	    {/*35*/SensorCall(3340);return;/*36*/}

	SensorCall(3341);iter = hv_auxinit(hv);
    }
    SensorCall(3343);iter->xhv_eiter = eiter;
SensorCall(3344);}

void
Perl_hv_name_set(pTHX_ HV *hv, const char *name, U32 len, U32 flags)
{
SensorCall(3345);    dVAR;
    struct xpvhv_aux *iter;
    U32 hash;
    HEK **spot;

    PERL_ARGS_ASSERT_HV_NAME_SET;

    SensorCall(3347);if (len > I32_MAX)
	{/*83*/SensorCall(3346);Perl_croak(aTHX_ "panic: hv name too long (%"UVuf")", (UV) len);/*84*/}

    SensorCall(3366);if (SvOOK(hv)) {
	SensorCall(3348);iter = HvAUX(hv);
	SensorCall(3362);if (iter->xhv_name_u.xhvnameu_name) {
	    SensorCall(3349);if(iter->xhv_name_count) {
	      SensorCall(3350);if(flags & HV_NAME_SETALL) {
		SensorCall(3351);HEK ** const name = HvAUX(hv)->xhv_name_u.xhvnameu_names;
		HEK **hekp = name + (
		    iter->xhv_name_count < 0
		     ? -iter->xhv_name_count
		     :  iter->xhv_name_count
		   );
		SensorCall(3352);while(hekp-- > name+1) 
		    unshare_hek_or_pvn(*hekp, 0, 0, 0);
		/* The first elem may be null. */
		SensorCall(3353);if(*name) unshare_hek_or_pvn(*name, 0, 0, 0);
		Safefree(name);
		SensorCall(3354);spot = &iter->xhv_name_u.xhvnameu_name;
		iter->xhv_name_count = 0;
	      }
	      else {
		SensorCall(3355);if(iter->xhv_name_count > 0) {
		    /* shift some things over */
		    Renew(
		     iter->xhv_name_u.xhvnameu_names, iter->xhv_name_count + 1, HEK *
		    );
		    SensorCall(3356);spot = iter->xhv_name_u.xhvnameu_names;
		    spot[iter->xhv_name_count] = spot[1];
		    spot[1] = spot[0];
		    iter->xhv_name_count = -(iter->xhv_name_count + 1);
		}
		else {/*85*/SensorCall(3357);if(*(spot = iter->xhv_name_u.xhvnameu_names)) {
		    unshare_hek_or_pvn(*spot, 0, 0, 0);
		;/*86*/}}
	      }
	    }
	    else {/*87*/SensorCall(3358);if (flags & HV_NAME_SETALL) {
		unshare_hek_or_pvn(iter->xhv_name_u.xhvnameu_name, 0, 0, 0);
		SensorCall(3359);spot = &iter->xhv_name_u.xhvnameu_name;
	    }
	    else {
		SensorCall(3360);HEK * const existing_name = iter->xhv_name_u.xhvnameu_name;
		Newx(iter->xhv_name_u.xhvnameu_names, 2, HEK *);
		iter->xhv_name_count = -2;
		spot = iter->xhv_name_u.xhvnameu_names;
		spot[1] = existing_name;
	    ;/*88*/}}
	}
	else { SensorCall(3361);spot = &iter->xhv_name_u.xhvnameu_name; iter->xhv_name_count = 0; }
    } else {
	SensorCall(3363);if (name == 0)
	    {/*89*/SensorCall(3364);return;/*90*/}

	SensorCall(3365);iter = hv_auxinit(hv);
	spot = &iter->xhv_name_u.xhvnameu_name;
    }
    PERL_HASH(hash, name, len);
    SensorCall(3367);*spot = name ? share_hek(name, flags & SVf_UTF8 ? -(I32)len : (I32)len, hash) : NULL;
SensorCall(3368);}

/*
This is basically sv_eq_flags() in sv.c, but we avoid the magic
and bytes checking.
*/

STATIC I32
hek_eq_pvn_flags(pTHX_ const HEK *hek, const char* pv, const I32 pvlen, const U32 flags) {
    SensorCall(3369);if ( (HEK_UTF8(hek) ? 1 : 0) != (flags & SVf_UTF8 ? 1 : 0) ) {
        SensorCall(3370);if (flags & SVf_UTF8)
            {/*195*/{I32  ReplaceReturn1629 = (bytes_cmp_utf8(
                        (const U8*)HEK_KEY(hek), HEK_LEN(hek),
		        (const U8*)pv, pvlen) == 0); SensorCall(3371); return ReplaceReturn1629;}/*196*/}
        else
            {/*197*/{I32  ReplaceReturn1628 = (bytes_cmp_utf8(
                        (const U8*)pv, pvlen,
		        (const U8*)HEK_KEY(hek), HEK_LEN(hek)) == 0); SensorCall(3372); return ReplaceReturn1628;}/*198*/}
    }
    else
        {/*199*/{I32  ReplaceReturn1627 = HEK_LEN(hek) == pvlen && ((HEK_KEY(hek) == pv)
                    || memEQ(HEK_KEY(hek), pv, pvlen)); SensorCall(3373); return ReplaceReturn1627;}/*200*/}
SensorCall(3374);}

/*
=for apidoc hv_ename_add

Adds a name to a stash's internal list of effective names.  See
C<hv_ename_delete>.

This is called when a stash is assigned to a new location in the symbol
table.

=cut
*/

void
Perl_hv_ename_add(pTHX_ HV *hv, const char *name, U32 len, U32 flags)
{
SensorCall(3375);    dVAR;
    struct xpvhv_aux *aux = SvOOK(hv) ? HvAUX(hv) : hv_auxinit(hv);
    U32 hash;

    PERL_ARGS_ASSERT_HV_ENAME_ADD;

    SensorCall(3377);if (len > I32_MAX)
	{/*37*/SensorCall(3376);Perl_croak(aTHX_ "panic: hv name too long (%"UVuf")", (UV) len);/*38*/}

    PERL_HASH(hash, name, len);

    SensorCall(3392);if (aux->xhv_name_count) {
	SensorCall(3378);HEK ** const xhv_name = aux->xhv_name_u.xhvnameu_names;
	I32 count = aux->xhv_name_count;
	HEK **hekp = xhv_name + (count < 0 ? -count : count);
	SensorCall(3383);while (hekp-- > xhv_name)
	    {/*39*/SensorCall(3379);if (
                 (HEK_UTF8(*hekp) || (flags & SVf_UTF8)) 
                    ? hek_eq_pvn_flags(aTHX_ *hekp, name, (I32)len, flags)
	            : (HEK_LEN(*hekp) == (I32)len && memEQ(HEK_KEY(*hekp), name, len))
               ) {
		SensorCall(3380);if (hekp == xhv_name && count < 0)
		    {/*41*/SensorCall(3381);aux->xhv_name_count = -count;/*42*/}
		SensorCall(3382);return;
	    ;/*40*/}}
	SensorCall(3386);if (count < 0) {/*43*/SensorCall(3384);aux->xhv_name_count--, count = -count;/*44*/}
	else {/*45*/SensorCall(3385);aux->xhv_name_count++;/*46*/}
	Renew(aux->xhv_name_u.xhvnameu_names, count + 1, HEK *);
	SensorCall(3387);(aux->xhv_name_u.xhvnameu_names)[count] = share_hek(name, (flags & SVf_UTF8 ? -(I32)len : (I32)len), hash);
    }
    else {
	SensorCall(3388);HEK *existing_name = aux->xhv_name_u.xhvnameu_name;
	SensorCall(3390);if (
	    existing_name && (
             (HEK_UTF8(existing_name) || (flags & SVf_UTF8))
                ? hek_eq_pvn_flags(aTHX_ existing_name, name, (I32)len, flags)
	        : (HEK_LEN(existing_name) == (I32)len && memEQ(HEK_KEY(existing_name), name, len))
	    )
	) {/*47*/SensorCall(3389);return;/*48*/}
	Newx(aux->xhv_name_u.xhvnameu_names, 2, HEK *);
	SensorCall(3391);aux->xhv_name_count = existing_name ? 2 : -2;
	*aux->xhv_name_u.xhvnameu_names = existing_name;
	(aux->xhv_name_u.xhvnameu_names)[1] = share_hek(name, (flags & SVf_UTF8 ? -(I32)len : (I32)len), hash);
    }
SensorCall(3393);}

/*
=for apidoc hv_ename_delete

Removes a name from a stash's internal list of effective names.  If this is
the name returned by C<HvENAME>, then another name in the list will take
its place (C<HvENAME> will use it).

This is called when a stash is deleted from the symbol table.

=cut
*/

void
Perl_hv_ename_delete(pTHX_ HV *hv, const char *name, U32 len, U32 flags)
{
SensorCall(3394);    dVAR;
    struct xpvhv_aux *aux;

    PERL_ARGS_ASSERT_HV_ENAME_DELETE;

    SensorCall(3396);if (len > I32_MAX)
	{/*49*/SensorCall(3395);Perl_croak(aTHX_ "panic: hv name too long (%"UVuf")", (UV) len);/*50*/}

    SensorCall(3398);if (!SvOOK(hv)) {/*51*/SensorCall(3397);return;/*52*/}

    SensorCall(3399);aux = HvAUX(hv);
    SensorCall(3401);if (!aux->xhv_name_u.xhvnameu_name) {/*53*/SensorCall(3400);return;/*54*/}

    SensorCall(3416);if (aux->xhv_name_count) {
	SensorCall(3402);HEK ** const namep = aux->xhv_name_u.xhvnameu_names;
	I32 const count = aux->xhv_name_count;
	HEK **victim = namep + (count < 0 ? -count : count);
	SensorCall(3411);while (victim-- > namep + 1)
	    {/*55*/SensorCall(3403);if (
             (HEK_UTF8(*victim) || (flags & SVf_UTF8)) 
                ? hek_eq_pvn_flags(aTHX_ *victim, name, (I32)len, flags)
	        : (HEK_LEN(*victim) == (I32)len && memEQ(HEK_KEY(*victim), name, len))
	    ) {
		unshare_hek_or_pvn(*victim, 0, 0, 0);
		SensorCall(3406);if (count < 0) {/*57*/SensorCall(3404);++aux->xhv_name_count;/*58*/}
		else {/*59*/SensorCall(3405);--aux->xhv_name_count;/*60*/}
		SensorCall(3409);if (
		    (aux->xhv_name_count == 1 || aux->xhv_name_count == -1)
		 && !*namep
		) {  /* if there are none left */
		    Safefree(namep);
		    SensorCall(3407);aux->xhv_name_u.xhvnameu_names = NULL;
		    aux->xhv_name_count = 0;
		}
		else {
		    /* Move the last one back to fill the empty slot. It
		       does not matter what order they are in. */
		    SensorCall(3408);*victim = *(namep + (count < 0 ? -count : count) - 1);
		}
		SensorCall(3410);return;
	    ;/*56*/}}
	SensorCall(3413);if (
	    count > 0 && (HEK_UTF8(*namep) || (flags & SVf_UTF8)) 
                ? hek_eq_pvn_flags(aTHX_ *namep, name, (I32)len, flags)
	        : (HEK_LEN(*namep) == (I32)len && memEQ(HEK_KEY(*namep), name, len))
	) {
	    SensorCall(3412);aux->xhv_name_count = -count;
	}
    }
    else {/*61*/SensorCall(3414);if(
        (HEK_UTF8(aux->xhv_name_u.xhvnameu_name) || (flags & SVf_UTF8)) 
                ? hek_eq_pvn_flags(aTHX_ aux->xhv_name_u.xhvnameu_name, name, (I32)len, flags)
	        : (HEK_LEN(aux->xhv_name_u.xhvnameu_name) == (I32)len &&
                            memEQ(HEK_KEY(aux->xhv_name_u.xhvnameu_name), name, len))
    ) {
	SensorCall(3415);HEK * const namehek = aux->xhv_name_u.xhvnameu_name;
	Newx(aux->xhv_name_u.xhvnameu_names, 1, HEK *);
	*aux->xhv_name_u.xhvnameu_names = namehek;
	aux->xhv_name_count = -1;
    ;/*62*/}}
SensorCall(3417);}

AV **
Perl_hv_backreferences_p(pTHX_ HV *hv) {
    SensorCall(3418);struct xpvhv_aux * const iter = SvOOK(hv) ? HvAUX(hv) : hv_auxinit(hv);

    PERL_ARGS_ASSERT_HV_BACKREFERENCES_P;
    PERL_UNUSED_CONTEXT;

    {AV ** ReplaceReturn1626 = &(iter->xhv_backreferences); SensorCall(3419); return ReplaceReturn1626;}
}

void
Perl_hv_kill_backrefs(pTHX_ HV *hv) {
    SensorCall(3420);AV *av;

    PERL_ARGS_ASSERT_HV_KILL_BACKREFS;

    SensorCall(3422);if (!SvOOK(hv))
	{/*141*/SensorCall(3421);return;/*142*/}

    SensorCall(3423);av = HvAUX(hv)->xhv_backreferences;

    SensorCall(3426);if (av) {
	HvAUX(hv)->xhv_backreferences = 0;
	SensorCall(3424);Perl_sv_kill_backrefs(aTHX_ MUTABLE_SV(hv), av);
	SensorCall(3425);if (SvTYPE(av) == SVt_PVAV)
	    SvREFCNT_dec(av);
    }
SensorCall(3427);}

/*
hv_iternext is implemented as a macro in hv.h

=for apidoc hv_iternext

Returns entries from a hash iterator.  See C<hv_iterinit>.

You may call C<hv_delete> or C<hv_delete_ent> on the hash entry that the
iterator currently points to, without losing your place or invalidating your
iterator.  Note that in this case the current entry is deleted from the hash
with your iterator holding the last reference to it.  Your iterator is flagged
to free the entry on the next call to C<hv_iternext>, so you must not discard
your iterator immediately else the entry will leak - call C<hv_iternext> to
trigger the resource deallocation.

=for apidoc hv_iternext_flags

Returns entries from a hash iterator.  See C<hv_iterinit> and C<hv_iternext>.
The C<flags> value will normally be zero; if HV_ITERNEXT_WANTPLACEHOLDERS is
set the placeholders keys (for restricted hashes) will be returned in addition
to normal keys. By default placeholders are automatically skipped over.
Currently a placeholder is implemented with a value that is
C<&PL_sv_placeholder>.  Note that the implementation of placeholders and
restricted hashes may change, and the implementation currently is
insufficiently abstracted for any change to be tidy.

=cut
*/

HE *
Perl_hv_iternext_flags(pTHX_ HV *hv, I32 flags)
{
SensorCall(3428);    dVAR;
    register XPVHV* xhv;
    register HE *entry;
    HE *oldentry;
    MAGIC* mg;
    struct xpvhv_aux *iter;

    PERL_ARGS_ASSERT_HV_ITERNEXT_FLAGS;

    SensorCall(3430);if (!hv)
	{/*69*/SensorCall(3429);Perl_croak(aTHX_ "Bad hash");/*70*/}

    SensorCall(3431);xhv = (XPVHV*)SvANY(hv);

    SensorCall(3432);if (!SvOOK(hv)) {
	/* Too many things (well, pp_each at least) merrily assume that you can
	   call iv_iternext without calling hv_iterinit, so we'll have to deal
	   with it.  */
	hv_iterinit(hv);
    }
    SensorCall(3433);iter = HvAUX(hv);

    oldentry = entry = iter->xhv_eiter; /* HvEITER(hv) */
    SensorCall(3442);if (SvMAGICAL(hv) && SvRMAGICAL(hv)) {
	SensorCall(3434);if ( ( mg = mg_find((const SV *)hv, PERL_MAGIC_tied) ) ) {
            SensorCall(3435);SV * const key = sv_newmortal();
            SensorCall(3437);if (entry) {
                sv_setsv(key, HeSVKEY_force(entry));
                SvREFCNT_dec(HeSVKEY(entry));       /* get rid of previous key */
		HeSVKEY_set(entry, NULL);
            }
            else {
                SensorCall(3436);char *k;
                HEK *hek;

                /* one HE per MAGICAL hash */
                iter->xhv_eiter = entry = new_HE(); /* HvEITER(hv) = new_HE() */
		HvLAZYDEL_on(hv); /* make sure entry gets freed */
                Zero(entry, 1, HE);
                Newxz(k, HEK_BASESIZE + sizeof(const SV *), char);
                hek = (HEK*)k;
                HeKEY_hek(entry) = hek;
                HeKLEN(entry) = HEf_SVKEY;
            }
            magic_nextpack(MUTABLE_SV(hv),mg,key);
            SensorCall(3439);if (SvOK(key)) {
                /* force key to stay around until next time */
                HeSVKEY_set(entry, SvREFCNT_inc_simple_NN(key));
                {HE * ReplaceReturn1625 = entry; SensorCall(3438); return ReplaceReturn1625;}               /* beware, hent_val is not set */
            }
            SvREFCNT_dec(HeVAL(entry));
            Safefree(HeKEY_hek(entry));
            del_HE(entry);
            SensorCall(3440);iter->xhv_eiter = NULL; /* HvEITER(hv) = NULL */
	    HvLAZYDEL_off(hv);
            {HE * ReplaceReturn1624 = NULL; SensorCall(3441); return ReplaceReturn1624;}
        }
    }
#if defined(DYNAMIC_ENV_FETCH) && !defined(__riscos__)  /* set up %ENV for iteration */
    if (!entry && SvRMAGICAL((const SV *)hv)
	&& mg_find((const SV *)hv, PERL_MAGIC_env)) {
	prime_env_iter();
#ifdef VMS
	/* The prime_env_iter() on VMS just loaded up new hash values
	 * so the iteration count needs to be reset back to the beginning
	 */
	hv_iterinit(hv);
	iter = HvAUX(hv);
	oldentry = entry = iter->xhv_eiter; /* HvEITER(hv) */
#endif
    }
#endif

    /* hv_iterinit now ensures this.  */
    assert (HvARRAY(hv));

    /* At start of hash, entry is NULL.  */
    SensorCall(3447);if (entry)
    {
	SensorCall(3443);entry = HeNEXT(entry);
        SensorCall(3446);if (!(flags & HV_ITERNEXT_WANTPLACEHOLDERS)) {
            /*
             * Skip past any placeholders -- don't want to include them in
             * any iteration.
             */
            SensorCall(3444);while (entry && HeVAL(entry) == &PL_sv_placeholder) {
                SensorCall(3445);entry = HeNEXT(entry);
            }
	}
    }

    /* Skip the entire loop if the hash is empty.   */
    SensorCall(3457);if ((flags & HV_ITERNEXT_WANTPLACEHOLDERS)
	? HvTOTALKEYS(hv) : HvUSEDKEYS(hv)) {
	SensorCall(3448);while (!entry) {
	    /* OK. Come to the end of the current list.  Grab the next one.  */

	    SensorCall(3449);iter->xhv_riter++; /* HvRITER(hv)++ */
	    SensorCall(3452);if (iter->xhv_riter > (I32)xhv->xhv_max /* HvRITER(hv) > HvMAX(hv) */) {
		/* There is no next one.  End of the hash.  */
		SensorCall(3450);iter->xhv_riter = -1; /* HvRITER(hv) = -1 */
		SensorCall(3451);break;
	    }
	    SensorCall(3453);entry = (HvARRAY(hv))[iter->xhv_riter];

	    SensorCall(3455);if (!(flags & HV_ITERNEXT_WANTPLACEHOLDERS)) {
		/* If we have an entry, but it's a placeholder, don't count it.
		   Try the next.  */
		SensorCall(3454);while (entry && HeVAL(entry) == &PL_sv_placeholder)
		    entry = HeNEXT(entry);
	    }
	    /* Will loop again if this linked list starts NULL
	       (for HV_ITERNEXT_WANTPLACEHOLDERS)
	       or if we run through it and find only placeholders.  */
	}
    }
    else {/*71*/SensorCall(3456);iter->xhv_riter = -1;/*72*/}

    SensorCall(3458);if (oldentry && HvLAZYDEL(hv)) {		/* was deleted earlier? */
	HvLAZYDEL_off(hv);
	hv_free_ent(hv, oldentry);
    }

    /*if (HvREHASH(hv) && entry && !HeKREHASH(entry))
      PerlIO_printf(PerlIO_stderr(), "Awooga %p %p\n", (void*)hv, (void*)entry);*/

    SensorCall(3459);iter->xhv_eiter = entry; /* HvEITER(hv) = entry */
    {HE * ReplaceReturn1623 = entry; SensorCall(3460); return ReplaceReturn1623;}
}

/*
=for apidoc hv_iterkey

Returns the key from the current position of the hash iterator.  See
C<hv_iterinit>.

=cut
*/

char *
Perl_hv_iterkey(pTHX_ register HE *entry, I32 *retlen)
{
    PERL_ARGS_ASSERT_HV_ITERKEY;

    SensorCall(3461);if (HeKLEN(entry) == HEf_SVKEY) {
	SensorCall(3462);STRLEN len;
	char * const p = SvPV(HeKEY_sv(entry), len);
	*retlen = len;
	{char * ReplaceReturn1622 = p; SensorCall(3463); return ReplaceReturn1622;}
    }
    else {
	SensorCall(3464);*retlen = HeKLEN(entry);
	{char * ReplaceReturn1621 = HeKEY(entry); SensorCall(3465); return ReplaceReturn1621;}
    }
SensorCall(3466);}

/* unlike hv_iterval(), this always returns a mortal copy of the key */
/*
=for apidoc hv_iterkeysv

Returns the key as an C<SV*> from the current position of the hash
iterator.  The return value will always be a mortal copy of the key.  Also
see C<hv_iterinit>.

=cut
*/

SV *
Perl_hv_iterkeysv(pTHX_ register HE *entry)
{
    PERL_ARGS_ASSERT_HV_ITERKEYSV;

    {SV * ReplaceReturn1620 = sv_2mortal(newSVhek(HeKEY_hek(entry))); SensorCall(3467); return ReplaceReturn1620;}
}

/*
=for apidoc hv_iterval

Returns the value from the current position of the hash iterator.  See
C<hv_iterkey>.

=cut
*/

SV *
Perl_hv_iterval(pTHX_ HV *hv, register HE *entry)
{
    PERL_ARGS_ASSERT_HV_ITERVAL;

    SensorCall(3468);if (SvRMAGICAL(hv)) {
	SensorCall(3469);if (mg_find((const SV *)hv, PERL_MAGIC_tied)) {
	    SensorCall(3470);SV* const sv = sv_newmortal();
	    SensorCall(3471);if (HeKLEN(entry) == HEf_SVKEY)
		mg_copy(MUTABLE_SV(hv), sv, (char*)HeKEY_sv(entry), HEf_SVKEY);
	    else
		mg_copy(MUTABLE_SV(hv), sv, HeKEY(entry), HeKLEN(entry));
	    {SV * ReplaceReturn1619 = sv; SensorCall(3472); return ReplaceReturn1619;}
	}
    }
    {SV * ReplaceReturn1618 = HeVAL(entry); SensorCall(3473); return ReplaceReturn1618;}
}

/*
=for apidoc hv_iternextsv

Performs an C<hv_iternext>, C<hv_iterkey>, and C<hv_iterval> in one
operation.

=cut
*/

SV *
Perl_hv_iternextsv(pTHX_ HV *hv, char **key, I32 *retlen)
{
    SensorCall(3474);HE * const he = hv_iternext_flags(hv, 0);

    PERL_ARGS_ASSERT_HV_ITERNEXTSV;

    SensorCall(3475);if (!he)
	return NULL;
    SensorCall(3476);*key = hv_iterkey(he, retlen);
    {SV * ReplaceReturn1617 = hv_iterval(hv, he); SensorCall(3477); return ReplaceReturn1617;}
}

/*

Now a macro in hv.h

=for apidoc hv_magic

Adds magic to a hash.  See C<sv_magic>.

=cut
*/

/* possibly free a shared string if no one has access to it
 * len and hash must both be valid for str.
 */
void
Perl_unsharepvn(pTHX_ const char *str, I32 len, U32 hash)
{
SensorCall(3478);    unshare_hek_or_pvn (NULL, str, len, hash);
SensorCall(3479);}


void
Perl_unshare_hek(pTHX_ HEK *hek)
{
SensorCall(3480);    assert(hek);
    unshare_hek_or_pvn(hek, NULL, 0, 0);
SensorCall(3481);}

/* possibly free a shared string if no one has access to it
   hek if non-NULL takes priority over the other 3, else str, len and hash
   are used.  If so, len and hash must both be valid for str.
 */
STATIC void
S_unshare_hek_or_pvn(pTHX_ const HEK *hek, const char *str, I32 len, U32 hash)
{
SensorCall(3482);    dVAR;
    register XPVHV* xhv;
    HE *entry;
    register HE **oentry;
    bool is_utf8 = FALSE;
    int k_flags = 0;
    const char * const save = str;
    struct shared_he *he = NULL;

    SensorCall(3492);if (hek) {
	/* Find the shared he which is just before us in memory.  */
	SensorCall(3483);he = (struct shared_he *)(((char *)hek)
				  - STRUCT_OFFSET(struct shared_he,
						  shared_he_hek));

	/* Assert that the caller passed us a genuine (or at least consistent)
	   shared hek  */
	assert (he->shared_he_he.hent_hek == hek);

	SensorCall(3486);if (he->shared_he_he.he_valu.hent_refcount - 1) {
	    SensorCall(3484);--he->shared_he_he.he_valu.hent_refcount;
	    SensorCall(3485);return;
	}

        SensorCall(3487);hash = HEK_HASH(hek);
    } else {/*175*/SensorCall(3488);if (len < 0) {
        SensorCall(3489);STRLEN tmplen = -len;
        is_utf8 = TRUE;
        /* See the note in hv_fetch(). --jhi */
        str = (char*)bytes_from_utf8((U8*)str, &tmplen, &is_utf8);
        len = tmplen;
        SensorCall(3490);if (is_utf8)
            k_flags = HVhek_UTF8;
        SensorCall(3491);if (str != save)
            k_flags |= HVhek_WASUTF8 | HVhek_FREEKEY;
    ;/*176*/}}

    /* what follows was the moral equivalent of:
    if ((Svp = hv_fetch(PL_strtab, tmpsv, FALSE, hash))) {
	if (--*Svp == NULL)
	    hv_delete(PL_strtab, str, len, G_DISCARD, hash);
    } */
    SensorCall(3493);xhv = (XPVHV*)SvANY(PL_strtab);
    /* assert(xhv_array != 0) */
    oentry = &(HvARRAY(PL_strtab))[hash & (I32) HvMAX(PL_strtab)];
    SensorCall(3509);if (he) {
	SensorCall(3494);const HE *const he_he = &(he->shared_he_he);
        SensorCall(3497);for (entry = *oentry; entry; oentry = &HeNEXT(entry), entry = *oentry) {
            SensorCall(3495);if (entry == he_he)
                {/*177*/SensorCall(3496);break;/*178*/}
        }
    } else {
        SensorCall(3498);const int flags_masked = k_flags & HVhek_MASK;
        SensorCall(3508);for (entry = *oentry; entry; oentry = &HeNEXT(entry), entry = *oentry) {
            SensorCall(3499);if (HeHASH(entry) != hash)		/* strings can't be equal */
                {/*179*/SensorCall(3500);continue;/*180*/}
            SensorCall(3502);if (HeKLEN(entry) != len)
                {/*181*/SensorCall(3501);continue;/*182*/}
            SensorCall(3504);if (HeKEY(entry) != str && memNE(HeKEY(entry),str,len))	/* is this it? */
                {/*183*/SensorCall(3503);continue;/*184*/}
            SensorCall(3506);if (HeKFLAGS(entry) != flags_masked)
                {/*185*/SensorCall(3505);continue;/*186*/}
            SensorCall(3507);break;
        }
    }

    SensorCall(3512);if (entry) {
        SensorCall(3510);if (--entry->he_valu.hent_refcount == 0) {
            SensorCall(3511);*oentry = HeNEXT(entry);
            Safefree(entry);
            xhv->xhv_keys--; /* HvTOTALKEYS(hv)-- */
        }
    }

    SensorCall(3514);if (!entry)
	{/*187*/SensorCall(3513);Perl_ck_warner_d(aTHX_ packWARN(WARN_INTERNAL),
			 "Attempt to free nonexistent shared string '%s'%s"
			 pTHX__FORMAT,
			 hek ? HEK_KEY(hek) : str,
			 ((k_flags & HVhek_UTF8) ? " (utf8)" : "") pTHX__VALUE);/*188*/}
    SensorCall(3515);if (k_flags & HVhek_FREEKEY)
	Safefree(str);
SensorCall(3516);}

/* get a (constant) string ptr from the global string table
 * string will get added if it is not already there.
 * len and hash must both be valid for str.
 */
HEK *
Perl_share_hek(pTHX_ const char *str, I32 len, register U32 hash)
{
SensorCall(3517);    bool is_utf8 = FALSE;
    int flags = 0;
    const char * const save = str;

    PERL_ARGS_ASSERT_SHARE_HEK;

    SensorCall(3522);if (len < 0) {
      SensorCall(3518);STRLEN tmplen = -len;
      is_utf8 = TRUE;
      /* See the note in hv_fetch(). --jhi */
      str = (char*)bytes_from_utf8((U8*)str, &tmplen, &is_utf8);
      len = tmplen;
      /* If we were able to downgrade here, then than means that we were passed
         in a key which only had chars 0-255, but was utf8 encoded.  */
      SensorCall(3519);if (is_utf8)
          flags = HVhek_UTF8;
      /* If we found we were able to downgrade the string to bytes, then
         we should flag that it needs upgrading on keys or each.  Also flag
         that we need share_hek_flags to free the string.  */
      SensorCall(3521);if (str != save) {
          PERL_HASH(hash, str, len);
          SensorCall(3520);flags |= HVhek_WASUTF8 | HVhek_FREEKEY;
      }
    }

    {HEK * ReplaceReturn1616 = share_hek_flags (str, len, hash, flags); SensorCall(3523); return ReplaceReturn1616;}
}

STATIC HEK *
S_share_hek_flags(pTHX_ const char *str, I32 len, register U32 hash, int flags)
{
SensorCall(3524);    dVAR;
    register HE *entry;
    const int flags_masked = flags & HVhek_MASK;
    const U32 hindex = hash & (I32) HvMAX(PL_strtab);
    register XPVHV * const xhv = (XPVHV*)SvANY(PL_strtab);

    PERL_ARGS_ASSERT_SHARE_HEK_FLAGS;

    /* what follows is the moral equivalent of:

    if (!(Svp = hv_fetch(PL_strtab, str, len, FALSE)))
	hv_store(PL_strtab, str, len, NULL, hash);

	Can't rehash the shared string table, so not sure if it's worth
	counting the number of entries in the linked list
    */

    /* assert(xhv_array != 0) */
    entry = (HvARRAY(PL_strtab))[hindex];
    SensorCall(3534);for (;entry; entry = HeNEXT(entry)) {
	SensorCall(3525);if (HeHASH(entry) != hash)		/* strings can't be equal */
	    {/*165*/SensorCall(3526);continue;/*166*/}
	SensorCall(3528);if (HeKLEN(entry) != len)
	    {/*167*/SensorCall(3527);continue;/*168*/}
	SensorCall(3530);if (HeKEY(entry) != str && memNE(HeKEY(entry),str,len))	/* is this it? */
	    {/*169*/SensorCall(3529);continue;/*170*/}
	SensorCall(3532);if (HeKFLAGS(entry) != flags_masked)
	    {/*171*/SensorCall(3531);continue;/*172*/}
	SensorCall(3533);break;
    }

    SensorCall(3538);if (!entry) {
	/* What used to be head of the list.
	   If this is NULL, then we're the first entry for this slot, which
	   means we need to increate fill.  */
	SensorCall(3535);struct shared_he *new_entry;
	HEK *hek;
	char *k;
	HE **const head = &HvARRAY(PL_strtab)[hindex];
	HE *const next = *head;

	/* We don't actually store a HE from the arena and a regular HEK.
	   Instead we allocate one chunk of memory big enough for both,
	   and put the HEK straight after the HE. This way we can find the
	   HE directly from the HEK.
	*/

	Newx(k, STRUCT_OFFSET(struct shared_he,
				shared_he_hek.hek_key[0]) + len + 2, char);
	new_entry = (struct shared_he *)k;
	entry = &(new_entry->shared_he_he);
	hek = &(new_entry->shared_he_hek);

	Copy(str, HEK_KEY(hek), len, char);
	HEK_KEY(hek)[len] = 0;
	HEK_LEN(hek) = len;
	HEK_HASH(hek) = hash;
	HEK_FLAGS(hek) = (unsigned char)flags_masked;

	/* Still "point" to the HEK, so that other code need not know what
	   we're up to.  */
	HeKEY_hek(entry) = hek;
	entry->he_valu.hent_refcount = 0;
	HeNEXT(entry) = next;
	*head = entry;

	xhv->xhv_keys++; /* HvTOTALKEYS(hv)++ */
	SensorCall(3537);if (!next) {			/* initial entry? */
	} else {/*173*/SensorCall(3536);if ( SHOULD_DO_HSPLIT(xhv) ) {
            hsplit(PL_strtab);
	;/*174*/}}
    }

    SensorCall(3539);++entry->he_valu.hent_refcount;

    SensorCall(3540);if (flags & HVhek_FREEKEY)
	Safefree(str);

    {HEK * ReplaceReturn1615 = HeKEY_hek(entry); SensorCall(3541); return ReplaceReturn1615;}
}

I32 *
Perl_hv_placeholders_p(pTHX_ HV *hv)
{
SensorCall(3542);    dVAR;
    MAGIC *mg = mg_find((const SV *)hv, PERL_MAGIC_rhash);

    PERL_ARGS_ASSERT_HV_PLACEHOLDERS_P;

    SensorCall(3546);if (!mg) {
	SensorCall(3543);mg = sv_magicext(MUTABLE_SV(hv), 0, PERL_MAGIC_rhash, 0, 0, 0);

	SensorCall(3545);if (!mg) {
	    SensorCall(3544);Perl_die(aTHX_ "panic: hv_placeholders_p");
	}
    }
    {I32 * ReplaceReturn1614 = &(mg->mg_len); SensorCall(3547); return ReplaceReturn1614;}
}


I32
Perl_hv_placeholders_get(pTHX_ const HV *hv)
{
SensorCall(3548);    dVAR;
    MAGIC * const mg = mg_find((const SV *)hv, PERL_MAGIC_rhash);

    PERL_ARGS_ASSERT_HV_PLACEHOLDERS_GET;

    {I32  ReplaceReturn1613 = mg ? mg->mg_len : 0; SensorCall(3549); return ReplaceReturn1613;}
}

void
Perl_hv_placeholders_set(pTHX_ HV *hv, I32 ph)
{
SensorCall(3550);    dVAR;
    MAGIC * const mg = mg_find((const SV *)hv, PERL_MAGIC_rhash);

    PERL_ARGS_ASSERT_HV_PLACEHOLDERS_SET;

    SensorCall(3555);if (mg) {
	SensorCall(3551);mg->mg_len = ph;
    } else {/*91*/SensorCall(3552);if (ph) {
	SensorCall(3553);if (!sv_magicext(MUTABLE_SV(hv), 0, PERL_MAGIC_rhash, 0, 0, ph))
	    {/*93*/SensorCall(3554);Perl_die(aTHX_ "panic: hv_placeholders_set");/*94*/}
    ;/*92*/}}
    /* else we don't need to add magic to record 0 placeholders.  */
SensorCall(3556);}

STATIC SV *
S_refcounted_he_value(pTHX_ const struct refcounted_he *he)
{
SensorCall(3557);    dVAR;
    SV *value;

    PERL_ARGS_ASSERT_REFCOUNTED_HE_VALUE;

    SensorCall(3570);switch(he->refcounted_he_data[0] & HVrhek_typemask) {
    case HVrhek_undef:
	SensorCall(3558);value = newSV(0);
	SensorCall(3559);break;
    case HVrhek_delete:
	SensorCall(3560);value = &PL_sv_placeholder;
	SensorCall(3561);break;
    case HVrhek_IV:
	SensorCall(3562);value = newSViv(he->refcounted_he_val.refcounted_he_u_iv);
	SensorCall(3563);break;
    case HVrhek_UV:
	SensorCall(3564);value = newSVuv(he->refcounted_he_val.refcounted_he_u_uv);
	SensorCall(3565);break;
    case HVrhek_PV:
    case HVrhek_PV_UTF8:
	/* Create a string SV that directly points to the bytes in our
	   structure.  */
	SensorCall(3566);value = newSV_type(SVt_PV);
	SvPV_set(value, (char *) he->refcounted_he_data + 1);
	SvCUR_set(value, he->refcounted_he_val.refcounted_he_u_len);
	/* This stops anything trying to free it  */
	SvLEN_set(value, 0);
	SvPOK_on(value);
	SvREADONLY_on(value);
	SensorCall(3567);if ((he->refcounted_he_data[0] & HVrhek_typemask) == HVrhek_PV_UTF8)
	    SvUTF8_on(value);
	SensorCall(3568);break;
    default:
	SensorCall(3569);Perl_croak(aTHX_ "panic: refcounted_he_value bad flags %"UVxf,
		   (UV)he->refcounted_he_data[0]);
    }
    {SV * ReplaceReturn1612 = value; SensorCall(3571); return ReplaceReturn1612;}
}

/*
=for apidoc m|HV *|refcounted_he_chain_2hv|const struct refcounted_he *c|U32 flags

Generates and returns a C<HV *> representing the content of a
C<refcounted_he> chain.
I<flags> is currently unused and must be zero.

=cut
*/
HV *
Perl_refcounted_he_chain_2hv(pTHX_ const struct refcounted_he *chain, U32 flags)
{
SensorCall(3572);    dVAR;
    HV *hv;
    U32 placeholders, max;

    SensorCall(3574);if (flags)
	{/*113*/SensorCall(3573);Perl_croak(aTHX_ "panic: refcounted_he_chain_2hv bad flags %"UVxf,
	    (UV)flags);/*114*/}

    /* We could chase the chain once to get an idea of the number of keys,
       and call ksplit.  But for now we'll make a potentially inefficient
       hash with only 8 entries in its array.  */
    SensorCall(3575);hv = newHV();
    max = HvMAX(hv);
    SensorCall(3577);if (!HvARRAY(hv)) {
	SensorCall(3576);char *array;
	Newxz(array, PERL_HV_ARRAY_ALLOC_BYTES(max + 1), char);
	HvARRAY(hv) = (HE**)array;
    }

    SensorCall(3578);placeholders = 0;
    SensorCall(3589);while (chain) {
#ifdef USE_ITHREADS
	SensorCall(3579);U32 hash = chain->refcounted_he_hash;
#else
	U32 hash = HEK_HASH(chain->refcounted_he_hek);
#endif
	HE **oentry = &((HvARRAY(hv))[hash & max]);
	HE *entry = *oentry;
	SV *value;

	SensorCall(3584);for (; entry; entry = HeNEXT(entry)) {
	    SensorCall(3580);if (HeHASH(entry) == hash) {
		/* We might have a duplicate key here.  If so, entry is older
		   than the key we've already put in the hash, so if they are
		   the same, skip adding entry.  */
#ifdef USE_ITHREADS
		SensorCall(3581);const STRLEN klen = HeKLEN(entry);
		const char *const key = HeKEY(entry);
		SensorCall(3583);if (klen == chain->refcounted_he_keylen
		    && (!!HeKUTF8(entry)
			== !!(chain->refcounted_he_data[0] & HVhek_UTF8))
		    && memEQ(key, REF_HE_KEY(chain), klen))
		    {/*115*/SensorCall(3582);goto next_please;/*116*/}
#else
		if (HeKEY_hek(entry) == chain->refcounted_he_hek)
		    goto next_please;
		if (HeKLEN(entry) == HEK_LEN(chain->refcounted_he_hek)
		    && HeKUTF8(entry) == HEK_UTF8(chain->refcounted_he_hek)
		    && memEQ(HeKEY(entry), HEK_KEY(chain->refcounted_he_hek),
			     HeKLEN(entry)))
		    goto next_please;
#endif
	    }
	}
	assert (!entry);
	SensorCall(3585);entry = new_HE();

#ifdef USE_ITHREADS
	HeKEY_hek(entry)
	    = share_hek_flags(REF_HE_KEY(chain),
			      chain->refcounted_he_keylen,
			      chain->refcounted_he_hash,
			      (chain->refcounted_he_data[0]
			       & (HVhek_UTF8|HVhek_WASUTF8)));
#else
	HeKEY_hek(entry) = share_hek_hek(chain->refcounted_he_hek);
#endif
	value = refcounted_he_value(chain);
	SensorCall(3587);if (value == &PL_sv_placeholder)
	    {/*117*/SensorCall(3586);placeholders++;/*118*/}
	HeVAL(entry) = value;

	/* Link it into the chain.  */
	HeNEXT(entry) = *oentry;
	SensorCall(3588);*oentry = entry;

	HvTOTALKEYS(hv)++;

    next_please:
	chain = chain->refcounted_he_next;
    }

    SensorCall(3590);if (placeholders) {
	clear_placeholders(hv, placeholders);
	HvTOTALKEYS(hv) -= placeholders;
    }

    /* We could check in the loop to see if we encounter any keys with key
       flags, but it's probably not worth it, as this per-hash flag is only
       really meant as an optimisation for things like Storable.  */
    HvHASKFLAGS_on(hv);
    DEBUG_A(Perl_hv_assert(aTHX_ hv));

    {HV * ReplaceReturn1611 = hv; SensorCall(3591); return ReplaceReturn1611;}
}

/*
=for apidoc m|SV *|refcounted_he_fetch_pvn|const struct refcounted_he *chain|const char *keypv|STRLEN keylen|U32 hash|U32 flags

Search along a C<refcounted_he> chain for an entry with the key specified
by I<keypv> and I<keylen>.  If I<flags> has the C<REFCOUNTED_HE_KEY_UTF8>
bit set, the key octets are interpreted as UTF-8, otherwise they
are interpreted as Latin-1.  I<hash> is a precomputed hash of the key
string, or zero if it has not been precomputed.  Returns a mortal scalar
representing the value associated with the key, or C<&PL_sv_placeholder>
if there is no value associated with the key.

=cut
*/

SV *
Perl_refcounted_he_fetch_pvn(pTHX_ const struct refcounted_he *chain,
			 const char *keypv, STRLEN keylen, U32 hash, U32 flags)
{
SensorCall(3592);    dVAR;
    U8 utf8_flag;
    PERL_ARGS_ASSERT_REFCOUNTED_HE_FETCH_PVN;

    SensorCall(3594);if (flags & ~(REFCOUNTED_HE_KEY_UTF8|REFCOUNTED_HE_EXISTS))
	{/*119*/SensorCall(3593);Perl_croak(aTHX_ "panic: refcounted_he_fetch_pvn bad flags %"UVxf,
	    (UV)flags);/*120*/}
    SensorCall(3596);if (!chain)
	{/*121*/{SV * ReplaceReturn1610 = &PL_sv_placeholder; SensorCall(3595); return ReplaceReturn1610;}/*122*/}
    SensorCall(3609);if (flags & REFCOUNTED_HE_KEY_UTF8) {
	/* For searching purposes, canonicalise to Latin-1 where possible. */
	SensorCall(3597);const char *keyend = keypv + keylen, *p;
	STRLEN nonascii_count = 0;
	SensorCall(3603);for (p = keypv; p != keyend; p++) {
	    SensorCall(3598);U8 c = (U8)*p;
	    SensorCall(3602);if (c & 0x80) {
		SensorCall(3599);if (!((c & 0xfe) == 0xc2 && ++p != keyend &&
			    (((U8)*p) & 0xc0) == 0x80))
		    {/*123*/SensorCall(3600);goto canonicalised_key;/*124*/}
		SensorCall(3601);nonascii_count++;
	    }
	}
	SensorCall(3607);if (nonascii_count) {
	    SensorCall(3604);char *q;
	    const char *p = keypv, *keyend = keypv + keylen;
	    keylen -= nonascii_count;
	    Newx(q, keylen, char);
	    SAVEFREEPV(q);
	    keypv = q;
	    SensorCall(3606);for (; p != keyend; p++, q++) {
		SensorCall(3605);U8 c = (U8)*p;
		*q = (char)
		    ((c & 0x80) ? ((c & 0x03) << 6) | (((U8)*++p) & 0x3f) : c);
	    }
	}
	SensorCall(3608);flags &= ~REFCOUNTED_HE_KEY_UTF8;
	canonicalised_key: ;
    }
    SensorCall(3610);utf8_flag = (flags & REFCOUNTED_HE_KEY_UTF8) ? HVhek_UTF8 : 0;
    SensorCall(3611);if (!hash)
	PERL_HASH(hash, keypv, keylen);

    SensorCall(3615);for (; chain; chain = chain->refcounted_he_next) {
	SensorCall(3612);if (
#ifdef USE_ITHREADS
	    hash == chain->refcounted_he_hash &&
	    keylen == chain->refcounted_he_keylen &&
	    memEQ(REF_HE_KEY(chain), keypv, keylen) &&
	    utf8_flag == (chain->refcounted_he_data[0] & HVhek_UTF8)
#else
	    hash == HEK_HASH(chain->refcounted_he_hek) &&
	    keylen == (STRLEN)HEK_LEN(chain->refcounted_he_hek) &&
	    memEQ(HEK_KEY(chain->refcounted_he_hek), keypv, keylen) &&
	    utf8_flag == (HEK_FLAGS(chain->refcounted_he_hek) & HVhek_UTF8)
#endif
	) {
	    SensorCall(3613);if (flags & REFCOUNTED_HE_EXISTS)
		return (chain->refcounted_he_data[0] & HVrhek_typemask)
		    == HVrhek_delete
		    ? NULL : &PL_sv_yes;
	    {SV * ReplaceReturn1609 = sv_2mortal(refcounted_he_value(chain)); SensorCall(3614); return ReplaceReturn1609;}
	}
    }
    {SV * ReplaceReturn1608 = flags & REFCOUNTED_HE_EXISTS ? NULL : &PL_sv_placeholder; SensorCall(3616); return ReplaceReturn1608;}
}

/*
=for apidoc m|SV *|refcounted_he_fetch_pv|const struct refcounted_he *chain|const char *key|U32 hash|U32 flags

Like L</refcounted_he_fetch_pvn>, but takes a nul-terminated string
instead of a string/length pair.

=cut
*/

SV *
Perl_refcounted_he_fetch_pv(pTHX_ const struct refcounted_he *chain,
			 const char *key, U32 hash, U32 flags)
{
    PERL_ARGS_ASSERT_REFCOUNTED_HE_FETCH_PV;
    {SV * ReplaceReturn1607 = refcounted_he_fetch_pvn(chain, key, strlen(key), hash, flags); SensorCall(3617); return ReplaceReturn1607;}
}

/*
=for apidoc m|SV *|refcounted_he_fetch_sv|const struct refcounted_he *chain|SV *key|U32 hash|U32 flags

Like L</refcounted_he_fetch_pvn>, but takes a Perl scalar instead of a
string/length pair.

=cut
*/

SV *
Perl_refcounted_he_fetch_sv(pTHX_ const struct refcounted_he *chain,
			 SV *key, U32 hash, U32 flags)
{
    SensorCall(3618);const char *keypv;
    STRLEN keylen;
    PERL_ARGS_ASSERT_REFCOUNTED_HE_FETCH_SV;
    SensorCall(3620);if (flags & REFCOUNTED_HE_KEY_UTF8)
	{/*125*/SensorCall(3619);Perl_croak(aTHX_ "panic: refcounted_he_fetch_sv bad flags %"UVxf,
	    (UV)flags);/*126*/}
    SensorCall(3621);keypv = SvPV_const(key, keylen);
    SensorCall(3622);if (SvUTF8(key))
	flags |= REFCOUNTED_HE_KEY_UTF8;
    SensorCall(3623);if (!hash && SvIsCOW_shared_hash(key))
	hash = SvSHARED_HASH(key);
    {SV * ReplaceReturn1606 = refcounted_he_fetch_pvn(chain, keypv, keylen, hash, flags); SensorCall(3624); return ReplaceReturn1606;}
}

/*
=for apidoc m|struct refcounted_he *|refcounted_he_new_pvn|struct refcounted_he *parent|const char *keypv|STRLEN keylen|U32 hash|SV *value|U32 flags

Creates a new C<refcounted_he>.  This consists of a single key/value
pair and a reference to an existing C<refcounted_he> chain (which may
be empty), and thus forms a longer chain.  When using the longer chain,
the new key/value pair takes precedence over any entry for the same key
further along the chain.

The new key is specified by I<keypv> and I<keylen>.  If I<flags> has
the C<REFCOUNTED_HE_KEY_UTF8> bit set, the key octets are interpreted
as UTF-8, otherwise they are interpreted as Latin-1.  I<hash> is
a precomputed hash of the key string, or zero if it has not been
precomputed.

I<value> is the scalar value to store for this key.  I<value> is copied
by this function, which thus does not take ownership of any reference
to it, and later changes to the scalar will not be reflected in the
value visible in the C<refcounted_he>.  Complex types of scalar will not
be stored with referential integrity, but will be coerced to strings.
I<value> may be either null or C<&PL_sv_placeholder> to indicate that no
value is to be associated with the key; this, as with any non-null value,
takes precedence over the existence of a value for the key further along
the chain.

I<parent> points to the rest of the C<refcounted_he> chain to be
attached to the new C<refcounted_he>.  This function takes ownership
of one reference to I<parent>, and returns one reference to the new
C<refcounted_he>.

=cut
*/

struct refcounted_he *
Perl_refcounted_he_new_pvn(pTHX_ struct refcounted_he *parent,
	const char *keypv, STRLEN keylen, U32 hash, SV *value, U32 flags)
{
SensorCall(3625);    dVAR;
    STRLEN value_len = 0;
    const char *value_p = NULL;
    bool is_pv;
    char value_type;
    char hekflags;
    STRLEN key_offset = 1;
    struct refcounted_he *he;
    PERL_ARGS_ASSERT_REFCOUNTED_HE_NEW_PVN;

    SensorCall(3634);if (!value || value == &PL_sv_placeholder) {
	SensorCall(3626);value_type = HVrhek_delete;
    } else {/*127*/SensorCall(3627);if (SvPOK(value)) {
	SensorCall(3628);value_type = HVrhek_PV;
    } else {/*129*/SensorCall(3629);if (SvIOK(value)) {
	SensorCall(3630);value_type = SvUOK((const SV *)value) ? HVrhek_UV : HVrhek_IV;
    } else {/*131*/SensorCall(3631);if (!SvOK(value)) {
	SensorCall(3632);value_type = HVrhek_undef;
    } else {
	SensorCall(3633);value_type = HVrhek_PV;
    ;/*132*/}/*130*/}/*128*/}}
    SensorCall(3635);is_pv = value_type == HVrhek_PV;
    SensorCall(3639);if (is_pv) {
	/* Do it this way so that the SvUTF8() test is after the SvPV, in case
	   the value is overloaded, and doesn't yet have the UTF-8flag set.  */
	SensorCall(3636);value_p = SvPV_const(value, value_len);
	SensorCall(3637);if (SvUTF8(value))
	    value_type = HVrhek_PV_UTF8;
	SensorCall(3638);key_offset = value_len + 2;
    }
    SensorCall(3640);hekflags = value_type;

    SensorCall(3653);if (flags & REFCOUNTED_HE_KEY_UTF8) {
	/* Canonicalise to Latin-1 where possible. */
	SensorCall(3641);const char *keyend = keypv + keylen, *p;
	STRLEN nonascii_count = 0;
	SensorCall(3647);for (p = keypv; p != keyend; p++) {
	    SensorCall(3642);U8 c = (U8)*p;
	    SensorCall(3646);if (c & 0x80) {
		SensorCall(3643);if (!((c & 0xfe) == 0xc2 && ++p != keyend &&
			    (((U8)*p) & 0xc0) == 0x80))
		    {/*133*/SensorCall(3644);goto canonicalised_key;/*134*/}
		SensorCall(3645);nonascii_count++;
	    }
	}
	SensorCall(3651);if (nonascii_count) {
	    SensorCall(3648);char *q;
	    const char *p = keypv, *keyend = keypv + keylen;
	    keylen -= nonascii_count;
	    Newx(q, keylen, char);
	    SAVEFREEPV(q);
	    keypv = q;
	    SensorCall(3650);for (; p != keyend; p++, q++) {
		SensorCall(3649);U8 c = (U8)*p;
		*q = (char)
		    ((c & 0x80) ? ((c & 0x03) << 6) | (((U8)*++p) & 0x3f) : c);
	    }
	}
	SensorCall(3652);flags &= ~REFCOUNTED_HE_KEY_UTF8;
	canonicalised_key: ;
    }
    SensorCall(3654);if (flags & REFCOUNTED_HE_KEY_UTF8)
	hekflags |= HVhek_UTF8;
    SensorCall(3655);if (!hash)
	PERL_HASH(hash, keypv, keylen);

#ifdef USE_ITHREADS
    SensorCall(3656);he = (struct refcounted_he*)
	PerlMemShared_malloc(sizeof(struct refcounted_he) - 1
			     + keylen
			     + key_offset);
#else
    he = (struct refcounted_he*)
	PerlMemShared_malloc(sizeof(struct refcounted_he) - 1
			     + key_offset);
#endif

    he->refcounted_he_next = parent;

    SensorCall(3662);if (is_pv) {
	Copy(value_p, he->refcounted_he_data + 1, value_len + 1, char);
	SensorCall(3657);he->refcounted_he_val.refcounted_he_u_len = value_len;
    } else {/*135*/SensorCall(3658);if (value_type == HVrhek_IV) {
	SensorCall(3659);he->refcounted_he_val.refcounted_he_u_iv = SvIVX(value);
    } else {/*137*/SensorCall(3660);if (value_type == HVrhek_UV) {
	SensorCall(3661);he->refcounted_he_val.refcounted_he_u_uv = SvUVX(value);
    ;/*138*/}/*136*/}}

#ifdef USE_ITHREADS
    SensorCall(3663);he->refcounted_he_hash = hash;
    he->refcounted_he_keylen = keylen;
    Copy(keypv, he->refcounted_he_data + key_offset, keylen, char);
#else
    he->refcounted_he_hek = share_hek_flags(keypv, keylen, hash, hekflags);
#endif

    he->refcounted_he_data[0] = hekflags;
    he->refcounted_he_refcnt = 1;

    {__IASTRUCT__ refcounted_he * ReplaceReturn1605 = he; SensorCall(3664); return ReplaceReturn1605;}
}

/*
=for apidoc m|struct refcounted_he *|refcounted_he_new_pv|struct refcounted_he *parent|const char *key|U32 hash|SV *value|U32 flags

Like L</refcounted_he_new_pvn>, but takes a nul-terminated string instead
of a string/length pair.

=cut
*/

struct refcounted_he *
Perl_refcounted_he_new_pv(pTHX_ struct refcounted_he *parent,
	const char *key, U32 hash, SV *value, U32 flags)
{
    PERL_ARGS_ASSERT_REFCOUNTED_HE_NEW_PV;
    {__IASTRUCT__ refcounted_he * ReplaceReturn1604 = refcounted_he_new_pvn(parent, key, strlen(key), hash, value, flags); SensorCall(3665); return ReplaceReturn1604;}
}

/*
=for apidoc m|struct refcounted_he *|refcounted_he_new_sv|struct refcounted_he *parent|SV *key|U32 hash|SV *value|U32 flags

Like L</refcounted_he_new_pvn>, but takes a Perl scalar instead of a
string/length pair.

=cut
*/

struct refcounted_he *
Perl_refcounted_he_new_sv(pTHX_ struct refcounted_he *parent,
	SV *key, U32 hash, SV *value, U32 flags)
{
    SensorCall(3666);const char *keypv;
    STRLEN keylen;
    PERL_ARGS_ASSERT_REFCOUNTED_HE_NEW_SV;
    SensorCall(3668);if (flags & REFCOUNTED_HE_KEY_UTF8)
	{/*139*/SensorCall(3667);Perl_croak(aTHX_ "panic: refcounted_he_new_sv bad flags %"UVxf,
	    (UV)flags);/*140*/}
    SensorCall(3669);keypv = SvPV_const(key, keylen);
    SensorCall(3670);if (SvUTF8(key))
	flags |= REFCOUNTED_HE_KEY_UTF8;
    SensorCall(3671);if (!hash && SvIsCOW_shared_hash(key))
	hash = SvSHARED_HASH(key);
    {__IASTRUCT__ refcounted_he * ReplaceReturn1603 = refcounted_he_new_pvn(parent, keypv, keylen, hash, value, flags); SensorCall(3672); return ReplaceReturn1603;}
}

/*
=for apidoc m|void|refcounted_he_free|struct refcounted_he *he

Decrements the reference count of a C<refcounted_he> by one.  If the
reference count reaches zero the structure's memory is freed, which
(recursively) causes a reduction of its parent C<refcounted_he>'s
reference count.  It is safe to pass a null pointer to this function:
no action occurs in this case.

=cut
*/

void
Perl_refcounted_he_free(pTHX_ struct refcounted_he *he) {
SensorCall(3673);    dVAR;
    PERL_UNUSED_CONTEXT;

    SensorCall(3678);while (he) {
	SensorCall(3674);struct refcounted_he *copy;
	U32 new_count;

	HINTS_REFCNT_LOCK;
	new_count = --he->refcounted_he_refcnt;
	HINTS_REFCNT_UNLOCK;
	
	SensorCall(3676);if (new_count) {
	    SensorCall(3675);return;
	}

#ifndef USE_ITHREADS
	unshare_hek_or_pvn (he->refcounted_he_hek, 0, 0, 0);
#endif
	SensorCall(3677);copy = he;
	he = he->refcounted_he_next;
	PerlMemShared_free(copy);
    }
SensorCall(3679);}

/*
=for apidoc m|struct refcounted_he *|refcounted_he_inc|struct refcounted_he *he

Increment the reference count of a C<refcounted_he>.  The pointer to the
C<refcounted_he> is also returned.  It is safe to pass a null pointer
to this function: no action occurs and a null pointer is returned.

=cut
*/

struct refcounted_he *
Perl_refcounted_he_inc(pTHX_ struct refcounted_he *he)
{
SensorCall(3680);    dVAR;
    SensorCall(3682);if (he) {
	HINTS_REFCNT_LOCK;
	SensorCall(3681);he->refcounted_he_refcnt++;
	HINTS_REFCNT_UNLOCK;
    }
    {__IASTRUCT__ refcounted_he * ReplaceReturn1602 = he; SensorCall(3683); return ReplaceReturn1602;}
}

/*
=for apidoc cop_fetch_label

Returns the label attached to a cop.
The flags pointer may be set to C<SVf_UTF8> or 0.

=cut
*/

/* pp_entereval is aware that labels are stored with a key ':' at the top of
   the linked list.  */
const char *
Perl_cop_fetch_label(pTHX_ COP *const cop, STRLEN *len, U32 *flags) {
    SensorCall(3684);struct refcounted_he *const chain = cop->cop_hints_hash;

    PERL_ARGS_ASSERT_COP_FETCH_LABEL;

    SensorCall(3685);if (!chain)
	return NULL;
#ifdef USE_ITHREADS
    SensorCall(3686);if (chain->refcounted_he_keylen != 1)
	return NULL;
    SensorCall(3687);if (*REF_HE_KEY(chain) != ':')
	return NULL;
#else
    if ((STRLEN)HEK_LEN(chain->refcounted_he_hek) != 1)
	return NULL;
    if (*HEK_KEY(chain->refcounted_he_hek) != ':')
	return NULL;
#endif
    /* Stop anyone trying to really mess us up by adding their own value for
       ':' into %^H  */
    SensorCall(3688);if ((chain->refcounted_he_data[0] & HVrhek_typemask) != HVrhek_PV
	&& (chain->refcounted_he_data[0] & HVrhek_typemask) != HVrhek_PV_UTF8)
	return NULL;

    SensorCall(3690);if (len)
	{/*1*/SensorCall(3689);*len = chain->refcounted_he_val.refcounted_he_u_len;/*2*/}
    SensorCall(3692);if (flags) {
	SensorCall(3691);*flags = ((chain->refcounted_he_data[0] & HVrhek_typemask)
		  == HVrhek_PV_UTF8) ? SVf_UTF8 : 0;
    }
    {const char * ReplaceReturn1601 = chain->refcounted_he_data + 1; SensorCall(3693); return ReplaceReturn1601;}
}

/*
=for apidoc cop_store_label

Save a label into a C<cop_hints_hash>. You need to set flags to C<SVf_UTF8>
for a utf-8 label.

=cut
*/

void
Perl_cop_store_label(pTHX_ COP *const cop, const char *label, STRLEN len,
		     U32 flags)
{
    SensorCall(3694);SV *labelsv;
    PERL_ARGS_ASSERT_COP_STORE_LABEL;

    SensorCall(3696);if (flags & ~(SVf_UTF8))
	{/*3*/SensorCall(3695);Perl_croak(aTHX_ "panic: cop_store_label illegal flag bits 0x%" UVxf,
		   (UV)flags);/*4*/}
    SensorCall(3697);labelsv = newSVpvn_flags(label, len, SVs_TEMP);
    SensorCall(3698);if (flags & SVf_UTF8)
	SvUTF8_on(labelsv);
    SensorCall(3699);cop->cop_hints_hash
	= refcounted_he_new_pvs(cop->cop_hints_hash, ":", labelsv, 0);
SensorCall(3700);}

/*
=for apidoc hv_assert

Check that a hash is in an internally consistent state.

=cut
*/

#ifdef DEBUGGING

void
Perl_hv_assert(pTHX_ HV *hv)
{
    dVAR;
    HE* entry;
    int withflags = 0;
    int placeholders = 0;
    int real = 0;
    int bad = 0;
    const I32 riter = HvRITER_get(hv);
    HE *eiter = HvEITER_get(hv);

    PERL_ARGS_ASSERT_HV_ASSERT;

    (void)hv_iterinit(hv);

    while ((entry = hv_iternext_flags(hv, HV_ITERNEXT_WANTPLACEHOLDERS))) {
	/* sanity check the values */
	if (HeVAL(entry) == &PL_sv_placeholder)
	    placeholders++;
	else
	    real++;
	/* sanity check the keys */
	if (HeSVKEY(entry)) {
	    NOOP;   /* Don't know what to check on SV keys.  */
	} else if (HeKUTF8(entry)) {
	    withflags++;
	    if (HeKWASUTF8(entry)) {
		PerlIO_printf(Perl_debug_log,
			    "hash key has both WASUTF8 and UTF8: '%.*s'\n",
			    (int) HeKLEN(entry),  HeKEY(entry));
		bad = 1;
	    }
	} else if (HeKWASUTF8(entry))
	    withflags++;
    }
    if (!SvTIED_mg((const SV *)hv, PERL_MAGIC_tied)) {
	static const char bad_count[] = "Count %d %s(s), but hash reports %d\n";
	const int nhashkeys = HvUSEDKEYS(hv);
	const int nhashplaceholders = HvPLACEHOLDERS_get(hv);

	if (nhashkeys != real) {
	    PerlIO_printf(Perl_debug_log, bad_count, real, "keys", nhashkeys );
	    bad = 1;
	}
	if (nhashplaceholders != placeholders) {
	    PerlIO_printf(Perl_debug_log, bad_count, placeholders, "placeholder", nhashplaceholders );
	    bad = 1;
	}
    }
    if (withflags && ! HvHASKFLAGS(hv)) {
	PerlIO_printf(Perl_debug_log,
		    "Hash has HASKFLAGS off but I count %d key(s) with flags\n",
		    withflags);
	bad = 1;
    }
    if (bad) {
	sv_dump(MUTABLE_SV(hv));
    }
    HvRITER_set(hv, riter);		/* Restore hash iterator state */
    HvEITER_set(hv, eiter);
}

#endif

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 *
 * ex: set ts=8 sts=4 sw=4 noet:
 */
