#include "var/tmp/sensor.h"
/* Originally this program just generated uudmap.h
   However, when we later wanted to generate bitcount.h, it was easier to
   refactor it and keep the same name, than either alternative - rename it,
   or duplicate all of the Makefile logic for a second program.  */

#include <stdio.h>
#include <stdlib.h>
/* If it turns out that we need to make this conditional on config.sh derived
   values, it might be easier just to rip out the use of strerrer().  */
#include <string.h>
/* If a platform doesn't support errno.h, it's probably so strange that
   "hello world" won't port easily to it.  */
#include <errno.h>

struct mg_data_raw_t {
    unsigned char type;
    const char *value;
    const char *comment;
};

static struct mg_data_raw_t mg_data_raw[] = {
#ifdef WIN32
#  include "..\mg_raw.h"
#else
#  include "mg_raw.h"
#endif
    {0, 0, 0}
};

struct mg_data_t {
    const char *value;
    const char *comment;
};

static struct mg_data_t mg_data[256];

static void
format_mg_data(FILE *out, const void *thing, size_t count) {
  SensorCall();const struct mg_data_t *p = (const struct mg_data_t *)thing;

  SensorCall();while (1) {
      SensorCall();if (p->value) {
	  SensorCall();fprintf(out, "    %s\n    %s", p->comment, p->value);
      } else {
	  SensorCall();fputs("    0", out);
      }
      SensorCall();++p;
      SensorCall();if (!--count)
	  {/*1*/SensorCall();break;/*2*/}
      SensorCall();fputs(",\n", out);
  }
  SensorCall();fputc('\n', out);
SensorCall();}

static void
format_char_block(FILE *out, const void *thing, size_t count) {
  SensorCall();const char *block = (const char *)thing;

  fputs("    ", out);
  SensorCall();while (count--) {
    SensorCall();fprintf(out, "%d", *block);
    block++;
    SensorCall();if (count) {
      SensorCall();fputs(", ", out);
      SensorCall();if (!(count & 15)) {
	SensorCall();fputs("\n    ", out);
      }
    }
  }
  SensorCall();fputc('\n', out);
SensorCall();}

static void
output_to_file(const char *progname, const char *filename,
	       void (format_function)(FILE *out, const void *thing, size_t count),
	       const void *thing, size_t count) {
  SensorCall();FILE *const out = fopen(filename, "w");

  SensorCall();if (!out) {
    SensorCall();fprintf(stderr, "%s: Could not open '%s': %s\n", progname, filename,
	    strerror(errno));
    exit(1);
  }

  SensorCall();fputs("{\n", out);
  format_function(out, thing, count);
  fputs("}\n", out);

  SensorCall();if (fclose(out)) {
    SensorCall();fprintf(stderr, "%s: Could not close '%s': %s\n", progname, filename,
	    strerror(errno));
    exit(1);
  }
SensorCall();}


static const char PL_uuemap[]
= "`!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_";

typedef unsigned char U8;

/* This will ensure it is all zeros.  */
static char PL_uudmap[256];
static char PL_bitcount[256];

int main(int argc, char **argv) {
  SensorCall();size_t i;
  int bits;
  struct mg_data_raw_t *p = mg_data_raw;

  SensorCall();if (argc < 4 || argv[1][0] == '\0' || argv[2][0] == '\0'
      || argv[3][0] == '\0') {
    SensorCall();fprintf(stderr, "Usage: %s uudemap.h bitcount.h mg_data.h\n", argv[0]);
    {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
  }

  SensorCall();for (i = 0; i < sizeof(PL_uuemap) - 1; ++i)
    {/*3*/SensorCall();PL_uudmap[(U8)PL_uuemap[i]] = (char)i;/*4*/}
  /*
   * Because ' ' and '`' map to the same value,
   * we need to decode them both the same.
   */
  SensorCall();PL_uudmap[(U8)' '] = 0;

  output_to_file(argv[0], argv[1], &format_char_block,
		 (const void *)PL_uudmap, sizeof(PL_uudmap));

  SensorCall();for (bits = 1; bits < 256; bits++) {
    SensorCall();if (bits & 1)	{/*5*/SensorCall();PL_bitcount[bits]++;/*6*/}
    SensorCall();if (bits & 2)	{/*7*/SensorCall();PL_bitcount[bits]++;/*8*/}
    SensorCall();if (bits & 4)	{/*9*/SensorCall();PL_bitcount[bits]++;/*10*/}
    SensorCall();if (bits & 8)	{/*11*/SensorCall();PL_bitcount[bits]++;/*12*/}
    SensorCall();if (bits & 16)	{/*13*/SensorCall();PL_bitcount[bits]++;/*14*/}
    SensorCall();if (bits & 32)	{/*15*/SensorCall();PL_bitcount[bits]++;/*16*/}
    SensorCall();if (bits & 64)	{/*17*/SensorCall();PL_bitcount[bits]++;/*18*/}
    SensorCall();if (bits & 128)	{/*19*/SensorCall();PL_bitcount[bits]++;/*20*/}
  }

  SensorCall();output_to_file(argv[0], argv[2], &format_char_block,
		 (const void *)PL_bitcount, sizeof(PL_bitcount));

  SensorCall();while (p->value) {
      SensorCall();mg_data[p->type].value = p->value;
      mg_data[p->type].comment = p->comment;
      ++p;
  }
      
  SensorCall();output_to_file(argv[0], argv[3], &format_mg_data,
		 (const void *)mg_data, sizeof(mg_data)/sizeof(mg_data[0]));

  {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}
