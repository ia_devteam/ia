#include "var/tmp/sensor.h"
#define PERL_constant_NOTFOUND	1
#define PERL_constant_NOTDEF	2
#define PERL_constant_ISIV	3
#define PERL_constant_ISNO	4
#define PERL_constant_ISNV	5
#define PERL_constant_ISPV	6
#define PERL_constant_ISPVN	7
#define PERL_constant_ISSV	8
#define PERL_constant_ISUNDEF	9
#define PERL_constant_ISUV	10
#define PERL_constant_ISYES	11

#ifndef NVTYPE
typedef double NV; /* 5.6 and later define NVTYPE, and typedef NV to it.  */
#endif
#ifndef aTHX_
#define aTHX_ /* 5.6 or later define this for threading support.  */
#endif
#ifndef pTHX_
#define pTHX_ /* 5.6 or later define this for threading support.  */
#endif

static int
constant_7 (pTHX_ const char *name, IV *iv_return) {
  /* When generated this function returned values for the list of names given
     here.  However, subsequent manual editing may have added or removed some.
     OS_CODE Z_ASCII Z_BLOCK Z_ERRNO Z_FIXED Z_TREES */
  /* Offset 6 gives the best switch position.  */
  SensorCall();switch (name[6]) {
  case 'D':
    SensorCall();if (memEQ(name, "Z_FIXE", 6)) {
    /*                     D     */
#ifdef Z_FIXED
      SensorCall();*iv_return = Z_FIXED;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case 'E':
    SensorCall();if (memEQ(name, "OS_COD", 6)) {
    /*                     E     */
#ifdef OS_CODE
      SensorCall();*iv_return = OS_CODE;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case 'I':
    SensorCall();if (memEQ(name, "Z_ASCI", 6)) {
    /*                     I     */
#ifdef Z_ASCII
      SensorCall();*iv_return = Z_ASCII;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case 'K':
    SensorCall();if (memEQ(name, "Z_BLOC", 6)) {
    /*                     K     */
#ifdef Z_BLOCK
      SensorCall();*iv_return = Z_BLOCK;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case 'O':
    SensorCall();if (memEQ(name, "Z_ERRN", 6)) {
    /*                     O     */
#ifdef Z_ERRNO
      SensorCall();*iv_return = Z_ERRNO;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case 'S':
    SensorCall();if (memEQ(name, "Z_TREE", 6)) {
    /*                     S     */
#if ZLIB_VERNUM >= 0x1240
      SensorCall();*iv_return = Z_TREES;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  }
  {int  ReplaceReturn = PERL_constant_NOTFOUND; SensorCall(); return ReplaceReturn;}
}

static int
constant_9 (pTHX_ const char *name, IV *iv_return) {
  /* When generated this function returned values for the list of names given
     here.  However, subsequent manual editing may have added or removed some.
     DEF_WBITS MAX_WBITS Z_UNKNOWN */
  /* Offset 2 gives the best switch position.  */
  SensorCall();switch (name[2]) {
  case 'F':
    SensorCall();if (memEQ(name, "DEF_WBITS", 9)) {
    /*                 ^            */
#ifdef DEF_WBITS
      *iv_return = DEF_WBITS;
      return PERL_constant_ISIV;
#else
      {int  ReplaceReturn = PERL_constant_NOTDEF; SensorCall(); return ReplaceReturn;}
#endif
    }
    SensorCall();break;
  case 'U':
    SensorCall();if (memEQ(name, "Z_UNKNOWN", 9)) {
    /*                 ^            */
#ifdef Z_UNKNOWN
      SensorCall();*iv_return = Z_UNKNOWN;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case 'X':
    SensorCall();if (memEQ(name, "MAX_WBITS", 9)) {
    /*                 ^            */
#ifdef MAX_WBITS
      SensorCall();*iv_return = MAX_WBITS;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  }
  {int  ReplaceReturn = PERL_constant_NOTFOUND; SensorCall(); return ReplaceReturn;}
}

static int
constant_10 (pTHX_ const char *name, IV *iv_return) {
  /* When generated this function returned values for the list of names given
     here.  However, subsequent manual editing may have added or removed some.
     Z_DEFLATED Z_FILTERED Z_NO_FLUSH */
  /* Offset 7 gives the best switch position.  */
  SensorCall();switch (name[7]) {
  case 'R':
    SensorCall();if (memEQ(name, "Z_FILTERED", 10)) {
    /*                      ^         */
#ifdef Z_FILTERED
      SensorCall();*iv_return = Z_FILTERED;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case 'T':
    SensorCall();if (memEQ(name, "Z_DEFLATED", 10)) {
    /*                      ^         */
#ifdef Z_DEFLATED
      SensorCall();*iv_return = Z_DEFLATED;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case 'U':
    SensorCall();if (memEQ(name, "Z_NO_FLUSH", 10)) {
    /*                      ^         */
#ifdef Z_NO_FLUSH
      SensorCall();*iv_return = Z_NO_FLUSH;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  }
  {int  ReplaceReturn = PERL_constant_NOTFOUND; SensorCall(); return ReplaceReturn;}
}

static int
constant_11 (pTHX_ const char *name, IV *iv_return) {
  /* When generated this function returned values for the list of names given
     here.  However, subsequent manual editing may have added or removed some.
     Z_BUF_ERROR Z_MEM_ERROR Z_NEED_DICT */
  /* Offset 4 gives the best switch position.  */
  SensorCall();switch (name[4]) {
  case 'E':
    SensorCall();if (memEQ(name, "Z_NEED_DICT", 11)) {
    /*                   ^             */
#ifdef Z_NEED_DICT
      SensorCall();*iv_return = Z_NEED_DICT;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case 'F':
    SensorCall();if (memEQ(name, "Z_BUF_ERROR", 11)) {
    /*                   ^             */
#ifdef Z_BUF_ERROR
      SensorCall();*iv_return = Z_BUF_ERROR;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case 'M':
    SensorCall();if (memEQ(name, "Z_MEM_ERROR", 11)) {
    /*                   ^             */
#ifdef Z_MEM_ERROR
      SensorCall();*iv_return = Z_MEM_ERROR;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  }
  {int  ReplaceReturn = PERL_constant_NOTFOUND; SensorCall(); return ReplaceReturn;}
}

static int
constant_12 (pTHX_ const char *name, IV *iv_return, const char **pv_return) {
  /* When generated this function returned values for the list of names given
     here.  However, subsequent manual editing may have added or removed some.
     ZLIB_VERSION Z_BEST_SPEED Z_DATA_ERROR Z_FULL_FLUSH Z_STREAM_END
     Z_SYNC_FLUSH */
  /* Offset 4 gives the best switch position.  */
  SensorCall();switch (name[4]) {
  case 'L':
    SensorCall();if (memEQ(name, "Z_FULL_FLUSH", 12)) {
    /*                   ^              */
#ifdef Z_FULL_FLUSH
      SensorCall();*iv_return = Z_FULL_FLUSH;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case 'N':
    SensorCall();if (memEQ(name, "Z_SYNC_FLUSH", 12)) {
    /*                   ^              */
#ifdef Z_SYNC_FLUSH
      SensorCall();*iv_return = Z_SYNC_FLUSH;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case 'R':
    SensorCall();if (memEQ(name, "Z_STREAM_END", 12)) {
    /*                   ^              */
#ifdef Z_STREAM_END
      SensorCall();*iv_return = Z_STREAM_END;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case 'S':
    SensorCall();if (memEQ(name, "Z_BEST_SPEED", 12)) {
    /*                   ^              */
#ifdef Z_BEST_SPEED
      SensorCall();*iv_return = Z_BEST_SPEED;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case 'T':
    SensorCall();if (memEQ(name, "Z_DATA_ERROR", 12)) {
    /*                   ^              */
#ifdef Z_DATA_ERROR
      SensorCall();*iv_return = Z_DATA_ERROR;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case '_':
    SensorCall();if (memEQ(name, "ZLIB_VERSION", 12)) {
    /*                   ^              */
#ifdef ZLIB_VERSION
      SensorCall();*pv_return = ZLIB_VERSION;
      {int  ReplaceReturn = PERL_constant_ISPV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  }
  {int  ReplaceReturn = PERL_constant_NOTFOUND; SensorCall(); return ReplaceReturn;}
}

static int
constant (pTHX_ const char *name, STRLEN len, IV *iv_return, const char **pv_return) {
  /* Initially switch on the length of the name.  */
  /* When generated this function returned values for the list of names given
     in this section of perl code.  Rather than manually editing these functions
     to add or remove constants, which would result in this comment and section
     of code becoming inaccurate, we recommend that you edit this section of
     code, and use it to regenerate a new set of constant functions which you
     then use to replace the originals.

     Regenerate these constant functions by feeding this entire source file to
     perl -x

#!/root/rpmbuild/BUILD/perl-5.16.3/miniperl -w
use ExtUtils::Constant qw (constant_types C_constant XS_constant);

my $types = {map {($_, 1)} qw(IV PV)};
my @names = (qw(DEF_WBITS MAX_MEM_LEVEL MAX_WBITS OS_CODE Z_ASCII
	       Z_BEST_COMPRESSION Z_BEST_SPEED Z_BINARY Z_BLOCK Z_BUF_ERROR
	       Z_DATA_ERROR Z_DEFAULT_COMPRESSION Z_DEFAULT_STRATEGY Z_DEFLATED
	       Z_ERRNO Z_FILTERED Z_FINISH Z_FIXED Z_FULL_FLUSH Z_HUFFMAN_ONLY
	       Z_MEM_ERROR Z_NEED_DICT Z_NO_COMPRESSION Z_NO_FLUSH Z_NULL Z_OK
	       Z_PARTIAL_FLUSH Z_RLE Z_STREAM_END Z_STREAM_ERROR Z_SYNC_FLUSH
	       Z_UNKNOWN Z_VERSION_ERROR),
            {name=>"ZLIB_VERSION", type=>"PV"},
            {name=>"Z_TREES", type=>"IV", macro=>["#if ZLIB_VERNUM >= 0x1240\n", "#endif\n"]});

print constant_types(), "\n"; # macro defs
foreach (C_constant ("Zlib", 'constant', 'IV', $types, undef, 3, @names) ) {
    print $_, "\n"; # C constant subs
}
print "\n#### XS Section:\n";
print XS_constant ("Zlib", $types);
__END__
   */

  SensorCall();switch (len) {
  case 4:
    SensorCall();if (memEQ(name, "Z_OK", 4)) {
#ifdef Z_OK
      SensorCall();*iv_return = Z_OK;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case 5:
    SensorCall();if (memEQ(name, "Z_RLE", 5)) {
#ifdef Z_RLE
      SensorCall();*iv_return = Z_RLE;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case 6:
    SensorCall();if (memEQ(name, "Z_NULL", 6)) {
#ifdef Z_NULL
      SensorCall();*iv_return = Z_NULL;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case 7:
    {int  ReplaceReturn = constant_7 (aTHX_ name, iv_return); SensorCall(); return ReplaceReturn;}
    break;
  case 8:
    /* Names all of length 8.  */
    /* Z_BINARY Z_FINISH */
    /* Offset 6 gives the best switch position.  */
    SensorCall();switch (name[6]) {
    case 'R':
      SensorCall();if (memEQ(name, "Z_BINARY", 8)) {
      /*                     ^       */
#ifdef Z_BINARY
        SensorCall();*iv_return = Z_BINARY;
        {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
        return PERL_constant_NOTDEF;
#endif
      }
      SensorCall();break;
    case 'S':
      SensorCall();if (memEQ(name, "Z_FINISH", 8)) {
      /*                     ^       */
#ifdef Z_FINISH
        SensorCall();*iv_return = Z_FINISH;
        {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
        return PERL_constant_NOTDEF;
#endif
      }
      SensorCall();break;
    }
    SensorCall();break;
  case 9:
    {int  ReplaceReturn = constant_9 (aTHX_ name, iv_return); SensorCall(); return ReplaceReturn;}
    break;
  case 10:
    {int  ReplaceReturn = constant_10 (aTHX_ name, iv_return); SensorCall(); return ReplaceReturn;}
    break;
  case 11:
    {int  ReplaceReturn = constant_11 (aTHX_ name, iv_return); SensorCall(); return ReplaceReturn;}
    break;
  case 12:
    {int  ReplaceReturn = constant_12 (aTHX_ name, iv_return, pv_return); SensorCall(); return ReplaceReturn;}
    break;
  case 13:
    SensorCall();if (memEQ(name, "MAX_MEM_LEVEL", 13)) {
#ifdef MAX_MEM_LEVEL
      SensorCall();*iv_return = MAX_MEM_LEVEL;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case 14:
    /* Names all of length 14.  */
    /* Z_HUFFMAN_ONLY Z_STREAM_ERROR */
    /* Offset 3 gives the best switch position.  */
    SensorCall();switch (name[3]) {
    case 'T':
      SensorCall();if (memEQ(name, "Z_STREAM_ERROR", 14)) {
      /*                  ^                 */
#ifdef Z_STREAM_ERROR
        SensorCall();*iv_return = Z_STREAM_ERROR;
        {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
        return PERL_constant_NOTDEF;
#endif
      }
      SensorCall();break;
    case 'U':
      SensorCall();if (memEQ(name, "Z_HUFFMAN_ONLY", 14)) {
      /*                  ^                 */
#ifdef Z_HUFFMAN_ONLY
        SensorCall();*iv_return = Z_HUFFMAN_ONLY;
        {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
        return PERL_constant_NOTDEF;
#endif
      }
      SensorCall();break;
    }
    SensorCall();break;
  case 15:
    /* Names all of length 15.  */
    /* Z_PARTIAL_FLUSH Z_VERSION_ERROR */
    /* Offset 5 gives the best switch position.  */
    SensorCall();switch (name[5]) {
    case 'S':
      SensorCall();if (memEQ(name, "Z_VERSION_ERROR", 15)) {
      /*                    ^                */
#ifdef Z_VERSION_ERROR
        SensorCall();*iv_return = Z_VERSION_ERROR;
        {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
        return PERL_constant_NOTDEF;
#endif
      }
      SensorCall();break;
    case 'T':
      SensorCall();if (memEQ(name, "Z_PARTIAL_FLUSH", 15)) {
      /*                    ^                */
#ifdef Z_PARTIAL_FLUSH
        SensorCall();*iv_return = Z_PARTIAL_FLUSH;
        {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
        return PERL_constant_NOTDEF;
#endif
      }
      SensorCall();break;
    }
    SensorCall();break;
  case 16:
    SensorCall();if (memEQ(name, "Z_NO_COMPRESSION", 16)) {
#ifdef Z_NO_COMPRESSION
      SensorCall();*iv_return = Z_NO_COMPRESSION;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  case 18:
    /* Names all of length 18.  */
    /* Z_BEST_COMPRESSION Z_DEFAULT_STRATEGY */
    /* Offset 14 gives the best switch position.  */
    SensorCall();switch (name[14]) {
    case 'S':
      SensorCall();if (memEQ(name, "Z_BEST_COMPRESSION", 18)) {
      /*                             ^          */
#ifdef Z_BEST_COMPRESSION
        SensorCall();*iv_return = Z_BEST_COMPRESSION;
        {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
        return PERL_constant_NOTDEF;
#endif
      }
      SensorCall();break;
    case 'T':
      SensorCall();if (memEQ(name, "Z_DEFAULT_STRATEGY", 18)) {
      /*                             ^          */
#ifdef Z_DEFAULT_STRATEGY
        SensorCall();*iv_return = Z_DEFAULT_STRATEGY;
        {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
        return PERL_constant_NOTDEF;
#endif
      }
      SensorCall();break;
    }
    SensorCall();break;
  case 21:
    SensorCall();if (memEQ(name, "Z_DEFAULT_COMPRESSION", 21)) {
#ifdef Z_DEFAULT_COMPRESSION
      SensorCall();*iv_return = Z_DEFAULT_COMPRESSION;
      {int  ReplaceReturn = PERL_constant_ISIV; SensorCall(); return ReplaceReturn;}
#else
      return PERL_constant_NOTDEF;
#endif
    }
    SensorCall();break;
  }
  {int  ReplaceReturn = PERL_constant_NOTFOUND; SensorCall(); return ReplaceReturn;}
}

