#include "var/tmp/sensor.h"
/*
 * hmac.c: routines to compute HMAC-SHA-1/224/256/384/512 digests
 *
 * Ref: FIPS PUB 198 The Keyed-Hash Message Authentication Code
 *
 * Copyright (C) 2003-2012 Mark Shelor, All Rights Reserved
 *
 * Version: 5.71
 * Wed Feb 29 04:06:10 MST 2012
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hmac.h"
#include "sha.h"

/* hmacopen: creates a new HMAC-SHA digest object */
HMAC *hmacopen(int alg, unsigned char *key, unsigned int keylen)
{
	SensorCall();unsigned int i;
	HMAC *h;

	SHA_newz(0, h, 1, HMAC);
	SensorCall();if (h == NULL)
		{/*123*/SensorCall();return(NULL);/*124*/}
	SensorCall();if ((h->isha = shaopen(alg)) == NULL) {
		SHA_free(h);
		SensorCall();return(NULL);
	}
	SensorCall();if ((h->osha = shaopen(alg)) == NULL) {
		SensorCall();shaclose(h->isha);
		SHA_free(h);
		SensorCall();return(NULL);
	}
	SensorCall();if (keylen <= h->osha->blocksize / 8)
		{/*125*/SensorCall();memcpy(h->key, key, keylen);/*126*/}
	else {
		SensorCall();if ((h->ksha = shaopen(alg)) == NULL) {
			SensorCall();shaclose(h->isha);
			shaclose(h->osha);
			SHA_free(h);
			SensorCall();return(NULL);
		}
		SensorCall();shawrite(key, keylen * 8, h->ksha);
		shafinish(h->ksha);
		memcpy(h->key, shadigest(h->ksha), h->ksha->digestlen);
		shaclose(h->ksha);
	}
	SensorCall();for (i = 0; i < h->osha->blocksize / 8; i++)
		{/*127*/SensorCall();h->key[i] ^= 0x5c;/*128*/}
	SensorCall();shawrite(h->key, h->osha->blocksize, h->osha);
	SensorCall();for (i = 0; i < h->isha->blocksize / 8; i++)
		{/*129*/SensorCall();h->key[i] ^= (0x5c ^ 0x36);/*130*/}
	SensorCall();shawrite(h->key, h->isha->blocksize, h->isha);
	memset(h->key, 0, sizeof(h->key));
	SensorCall();return(h);
}

/* hmacwrite: triggers a state update using data in bitstr/bitcnt */
unsigned long hmacwrite(unsigned char *bitstr, unsigned long bitcnt, HMAC *h)
{
	SensorCall();return(shawrite(bitstr, bitcnt, h->isha));
}

/* hmacfinish: computes final digest state */
void hmacfinish(HMAC *h)
{
	SensorCall();shafinish(h->isha);
	shawrite(shadigest(h->isha), h->isha->digestlen * 8, h->osha);
	shaclose(h->isha);
	shafinish(h->osha);
SensorCall();}

/* hmacdigest: returns pointer to digest (binary) */
unsigned char *hmacdigest(HMAC *h)
{
	SensorCall();return(shadigest(h->osha));
}

/* hmachex: returns pointer to digest (hexadecimal) */
char *hmachex(HMAC *h)
{
	SensorCall();return(shahex(h->osha));
}

/* hmacbase64: returns pointer to digest (Base 64) */
char *hmacbase64(HMAC *h)
{
	SensorCall();return(shabase64(h->osha));
}

/* hmacclose: de-allocates digest object */
int hmacclose(HMAC *h)
{
	SensorCall();if (h != NULL) {
		SensorCall();shaclose(h->osha);
		memset(h, 0, sizeof(HMAC));
		SHA_free(h);
	}
	SensorCall();return(0);
}
