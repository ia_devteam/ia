#include "var/tmp/sensor.h"
/*    pp_pack.c
 *
 *    Copyright (C) 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000,
 *    2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008 by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 *
 */

/*
 * He still hopefully carried some of his gear in his pack: a small tinder-box,
 * two small shallow pans, the smaller fitting into the larger; inside them a
 * wooden spoon, a short two-pronged fork and some skewers were stowed; and
 * hidden at the bottom of the pack in a flat wooden box a dwindling treasure,
 * some salt.
 *
 *     [p.653 of _The Lord of the Rings_, IV/iv: "Of Herbs and Stewed Rabbit"]
 */

/* This file contains pp ("push/pop") functions that
 * execute the opcodes that make up a perl program. A typical pp function
 * expects to find its arguments on the stack, and usually pushes its
 * results onto the stack, hence the 'pp' terminology. Each OP structure
 * contains a pointer to the relevant pp_foo() function.
 *
 * This particular file just contains pp_pack() and pp_unpack(). See the
 * other pp*.c files for the rest of the pp_ functions.
 */

#include "EXTERN.h"
#define PERL_IN_PP_PACK_C
#include "perl.h"

/* Types used by pack/unpack */ 
typedef enum {
  e_no_len,     /* no length  */
  e_number,     /* number, [] */
  e_star        /* asterisk   */
} howlen_t;

typedef struct tempsym {
  const char*    patptr;   /* current template char */
  const char*    patend;   /* one after last char   */
  const char*    grpbeg;   /* 1st char of ()-group  */
  const char*    grpend;   /* end of ()-group       */
  I32      code;     /* template code (!<>)   */
  I32      length;   /* length/repeat count   */
  howlen_t howlen;   /* how length is given   */ 
  int      level;    /* () nesting level      */
  U32      flags;    /* /=4, comma=2, pack=1  */
                     /*   and group modifiers */
  STRLEN   strbeg;   /* offset of group start */
  struct tempsym *previous; /* previous group */
} tempsym_t;

#define TEMPSYM_INIT(symptr, p, e, f) \
    STMT_START {	\
	(symptr)->patptr   = (p);	\
	(symptr)->patend   = (e);	\
	(symptr)->grpbeg   = NULL;	\
	(symptr)->grpend   = NULL;	\
	(symptr)->grpend   = NULL;	\
	(symptr)->code     = 0;		\
	(symptr)->length   = 0;		\
	(symptr)->howlen   = e_no_len;	\
	(symptr)->level    = 0;		\
	(symptr)->flags    = (f);	\
	(symptr)->strbeg   = 0;		\
	(symptr)->previous = NULL;	\
   } STMT_END

typedef union {
    NV nv;
    U8 bytes[sizeof(NV)];
} NV_bytes;

#if defined(HAS_LONG_DOUBLE) && defined(USE_LONG_DOUBLE)
typedef union {
    long double ld;
    U8 bytes[sizeof(long double)];
} ld_bytes;
#endif

#if PERL_VERSION >= 9
# define PERL_PACK_CAN_BYTEORDER
# define PERL_PACK_CAN_SHRIEKSIGN
#endif

#ifndef CHAR_BIT
# define CHAR_BIT	8
#endif
/* Maximum number of bytes to which a byte can grow due to upgrade */
#define UTF8_EXPAND	2

/*
 * Offset for integer pack/unpack.
 *
 * On architectures where I16 and I32 aren't really 16 and 32 bits,
 * which for now are all Crays, pack and unpack have to play games.
 */

/*
 * These values are required for portability of pack() output.
 * If they're not right on your machine, then pack() and unpack()
 * wouldn't work right anyway; you'll need to apply the Cray hack.
 * (I'd like to check them with #if, but you can't use sizeof() in
 * the preprocessor.)  --???
 */
/*
    The appropriate SHORTSIZE, INTSIZE, LONGSIZE, and LONGLONGSIZE
    defines are now in config.h.  --Andy Dougherty  April 1998
 */
#define SIZE16 2
#define SIZE32 4

/* CROSSCOMPILE and MULTIARCH are going to affect pp_pack() and pp_unpack().
   --jhi Feb 1999 */

#if U16SIZE > SIZE16 || U32SIZE > SIZE32
#  if BYTEORDER == 0x1234 || BYTEORDER == 0x12345678    /* little-endian */
#    define OFF16(p)	((char*)(p))
#    define OFF32(p)	((char*)(p))
#  else
#    if BYTEORDER == 0x4321 || BYTEORDER == 0x87654321  /* big-endian */
#      define OFF16(p)	((char*)(p) + (sizeof(U16) - SIZE16))
#      define OFF32(p)	((char*)(p) + (sizeof(U32) - SIZE32))
#    else
       ++++ bad cray byte order
#    endif
#  endif
#else
#  define OFF16(p)     ((char *) (p))
#  define OFF32(p)     ((char *) (p))
#endif

/* Only to be used inside a loop (see the break) */
#define SHIFT16(utf8, s, strend, p, datumtype) STMT_START {		\
    if (utf8) {								\
	if (!uni_to_bytes(aTHX_ &(s), strend, OFF16(p), SIZE16, datumtype)) break;	\
    } else {								\
	Copy(s, OFF16(p), SIZE16, char);				\
	(s) += SIZE16;							\
    }									\
} STMT_END

/* Only to be used inside a loop (see the break) */
#define SHIFT32(utf8, s, strend, p, datumtype) STMT_START {		\
    if (utf8) {								\
	if (!uni_to_bytes(aTHX_ &(s), strend, OFF32(p), SIZE32, datumtype)) break;	\
    } else {								\
	Copy(s, OFF32(p), SIZE32, char);				\
	(s) += SIZE32;							\
    }									\
} STMT_END

#define PUSH16(utf8, cur, p) PUSH_BYTES(utf8, cur, OFF16(p), SIZE16)
#define PUSH32(utf8, cur, p) PUSH_BYTES(utf8, cur, OFF32(p), SIZE32)

/* Only to be used inside a loop (see the break) */
#define SHIFT_BYTES(utf8, s, strend, buf, len, datumtype)	\
STMT_START {						\
    if (utf8) {						\
        if (!uni_to_bytes(aTHX_ &s, strend,		\
	  (char *) (buf), len, datumtype)) break;	\
    } else {						\
        Copy(s, (char *) (buf), len, char);		\
        s += len;					\
    }							\
} STMT_END

#define SHIFT_VAR(utf8, s, strend, var, datumtype)	\
       SHIFT_BYTES(utf8, s, strend, &(var), sizeof(var), datumtype)

#define PUSH_VAR(utf8, aptr, var)	\
	PUSH_BYTES(utf8, aptr, &(var), sizeof(var))

/* Avoid stack overflow due to pathological templates. 100 should be plenty. */
#define MAX_SUB_TEMPLATE_LEVEL 100

/* flags (note that type modifiers can also be used as flags!) */
#define FLAG_WAS_UTF8	      0x40
#define FLAG_PARSE_UTF8       0x20	/* Parse as utf8 */
#define FLAG_UNPACK_ONLY_ONE  0x10
#define FLAG_DO_UTF8          0x08	/* The underlying string is utf8 */
#define FLAG_SLASH            0x04
#define FLAG_COMMA            0x02
#define FLAG_PACK             0x01

STATIC SV *
S_mul128(pTHX_ SV *sv, U8 m)
{
  SensorCall(15254);STRLEN          len;
  char           *s = SvPV(sv, len);
  char           *t;

  PERL_ARGS_ASSERT_MUL128;

  SensorCall(15256);if (!strnEQ(s, "0000", 4)) {  /* need to grow sv */
    SensorCall(15255);SV * const tmpNew = newSVpvs("0000000000");

    sv_catsv(tmpNew, sv);
    SvREFCNT_dec(sv);		/* free old sv */
    sv = tmpNew;
    s = SvPV(sv, len);
  }
  SensorCall(15257);t = s + len - 1;
  SensorCall(15259);while (!*t)                   /* trailing '\0'? */
    {/*43*/SensorCall(15258);t--;/*44*/}
  SensorCall(15261);while (t > s) {
    SensorCall(15260);const U32 i = ((*t - '0') << 7) + m;
    *(t--) = '0' + (char)(i % 10);
    m = (char)(i / 10);
  }
  {SV * ReplaceReturn1600 = (sv); SensorCall(15262); return ReplaceReturn1600;}
}

/* Explosives and implosives. */

#if 'I' == 73 && 'J' == 74
/* On an ASCII/ISO kind of system */
#define ISUUCHAR(ch)    ((ch) >= ' ' && (ch) < 'a')
#else
/*
  Some other sort of character set - use memchr() so we don't match
  the null byte.
 */
#define ISUUCHAR(ch)    (memchr(PL_uuemap, (ch), sizeof(PL_uuemap)-1) || (ch) == ' ')
#endif

/* type modifiers */
#define TYPE_IS_SHRIEKING	0x100
#define TYPE_IS_BIG_ENDIAN	0x200
#define TYPE_IS_LITTLE_ENDIAN	0x400
#define TYPE_IS_PACK		0x800
#define TYPE_ENDIANNESS_MASK	(TYPE_IS_BIG_ENDIAN|TYPE_IS_LITTLE_ENDIAN)
#define TYPE_MODIFIERS(t)	((t) & ~0xFF)
#define TYPE_NO_MODIFIERS(t)	((t) & 0xFF)

#ifdef PERL_PACK_CAN_SHRIEKSIGN
# define SHRIEKING_ALLOWED_TYPES "sSiIlLxXnNvV@."
#else
# define SHRIEKING_ALLOWED_TYPES "sSiIlLxX"
#endif

#ifndef PERL_PACK_CAN_BYTEORDER
/* Put "can't" first because it is shorter  */
# define TYPE_ENDIANNESS(t)	0
# define TYPE_NO_ENDIANNESS(t)	(t)

# define ENDIANNESS_ALLOWED_TYPES   ""

# define DO_BO_UNPACK(var, type)
# define DO_BO_PACK(var, type)
# define DO_BO_UNPACK_PTR(var, type, pre_cast, post_cast)
# define DO_BO_PACK_PTR(var, type, pre_cast, post_cast)
# define DO_BO_UNPACK_N(var, type)
# define DO_BO_PACK_N(var, type)
# define DO_BO_UNPACK_P(var)
# define DO_BO_PACK_P(var)
# define DO_BO_UNPACK_PC(var)
# define DO_BO_PACK_PC(var)

#else /* PERL_PACK_CAN_BYTEORDER */

# define TYPE_ENDIANNESS(t)	((t) & TYPE_ENDIANNESS_MASK)
# define TYPE_NO_ENDIANNESS(t)	((t) & ~TYPE_ENDIANNESS_MASK)

# define ENDIANNESS_ALLOWED_TYPES   "sSiIlLqQjJfFdDpP("

# define DO_BO_UNPACK(var, type)                                              \
        STMT_START {                                                          \
          switch (TYPE_ENDIANNESS(datumtype)) {                               \
            case TYPE_IS_BIG_ENDIAN:    var = my_betoh ## type (var); break;  \
            case TYPE_IS_LITTLE_ENDIAN: var = my_letoh ## type (var); break;  \
            default: break;                                                   \
          }                                                                   \
        } STMT_END

# define DO_BO_PACK(var, type)                                                \
        STMT_START {                                                          \
          switch (TYPE_ENDIANNESS(datumtype)) {                               \
            case TYPE_IS_BIG_ENDIAN:    var = my_htobe ## type (var); break;  \
            case TYPE_IS_LITTLE_ENDIAN: var = my_htole ## type (var); break;  \
            default: break;                                                   \
          }                                                                   \
        } STMT_END

# define DO_BO_UNPACK_PTR(var, type, pre_cast, post_cast)                     \
        STMT_START {                                                          \
          switch (TYPE_ENDIANNESS(datumtype)) {                               \
            case TYPE_IS_BIG_ENDIAN:                                          \
              var = (post_cast*) my_betoh ## type ((pre_cast) var);           \
              break;                                                          \
            case TYPE_IS_LITTLE_ENDIAN:                                       \
              var = (post_cast *) my_letoh ## type ((pre_cast) var);          \
              break;                                                          \
            default:                                                          \
              break;                                                          \
          }                                                                   \
        } STMT_END

# define DO_BO_PACK_PTR(var, type, pre_cast, post_cast)                       \
        STMT_START {                                                          \
          switch (TYPE_ENDIANNESS(datumtype)) {                               \
            case TYPE_IS_BIG_ENDIAN:                                          \
              var = (post_cast *) my_htobe ## type ((pre_cast) var);          \
              break;                                                          \
            case TYPE_IS_LITTLE_ENDIAN:                                       \
              var = (post_cast *) my_htole ## type ((pre_cast) var);          \
              break;                                                          \
            default:                                                          \
              break;                                                          \
          }                                                                   \
        } STMT_END

# define BO_CANT_DOIT(action, type)                                           \
        STMT_START {                                                          \
          switch (TYPE_ENDIANNESS(datumtype)) {                               \
             case TYPE_IS_BIG_ENDIAN:                                         \
               Perl_croak(aTHX_ "Can't %s big-endian %ss on this "            \
                                "platform", #action, #type);                  \
               break;                                                         \
             case TYPE_IS_LITTLE_ENDIAN:                                      \
               Perl_croak(aTHX_ "Can't %s little-endian %ss on this "         \
                                "platform", #action, #type);                  \
               break;                                                         \
             default:                                                         \
               break;                                                         \
           }                                                                  \
         } STMT_END

# if PTRSIZE == INTSIZE
#  define DO_BO_UNPACK_P(var)	DO_BO_UNPACK_PTR(var, i, int, void)
#  define DO_BO_PACK_P(var)	DO_BO_PACK_PTR(var, i, int, void)
#  define DO_BO_UNPACK_PC(var)	DO_BO_UNPACK_PTR(var, i, int, char)
#  define DO_BO_PACK_PC(var)	DO_BO_PACK_PTR(var, i, int, char)
# elif PTRSIZE == LONGSIZE
#  if LONGSIZE < IVSIZE && IVSIZE == 8
#   define DO_BO_UNPACK_P(var)	DO_BO_UNPACK_PTR(var, 64, IV, void)
#   define DO_BO_PACK_P(var)	DO_BO_PACK_PTR(var, 64, IV, void)
#   define DO_BO_UNPACK_PC(var)	DO_BO_UNPACK_PTR(var, 64, IV, char)
#   define DO_BO_PACK_PC(var)	DO_BO_PACK_PTR(var, 64, IV, char)
#  else
#   define DO_BO_UNPACK_P(var)	DO_BO_UNPACK_PTR(var, l, IV, void)
#   define DO_BO_PACK_P(var)	DO_BO_PACK_PTR(var, l, IV, void)
#   define DO_BO_UNPACK_PC(var)	DO_BO_UNPACK_PTR(var, l, IV, char)
#   define DO_BO_PACK_PC(var)	DO_BO_PACK_PTR(var, l, IV, char)
#  endif
# elif PTRSIZE == IVSIZE
#  define DO_BO_UNPACK_P(var)	DO_BO_UNPACK_PTR(var, l, IV, void)
#  define DO_BO_PACK_P(var)	DO_BO_PACK_PTR(var, l, IV, void)
#  define DO_BO_UNPACK_PC(var)	DO_BO_UNPACK_PTR(var, l, IV, char)
#  define DO_BO_PACK_PC(var)	DO_BO_PACK_PTR(var, l, IV, char)
# else
#  define DO_BO_UNPACK_P(var)	BO_CANT_DOIT(unpack, pointer)
#  define DO_BO_PACK_P(var)	BO_CANT_DOIT(pack, pointer)
#  define DO_BO_UNPACK_PC(var)	BO_CANT_DOIT(unpack, pointer)
#  define DO_BO_PACK_PC(var)	BO_CANT_DOIT(pack, pointer)
# endif

# if defined(my_htolen) && defined(my_letohn) && \
    defined(my_htoben) && defined(my_betohn)
#  define DO_BO_UNPACK_N(var, type)                                           \
         STMT_START {                                                         \
           switch (TYPE_ENDIANNESS(datumtype)) {                              \
             case TYPE_IS_BIG_ENDIAN:    my_betohn(&var, sizeof(type)); break;\
             case TYPE_IS_LITTLE_ENDIAN: my_letohn(&var, sizeof(type)); break;\
             default: break;                                                  \
           }                                                                  \
         } STMT_END

#  define DO_BO_PACK_N(var, type)                                             \
         STMT_START {                                                         \
           switch (TYPE_ENDIANNESS(datumtype)) {                              \
             case TYPE_IS_BIG_ENDIAN:    my_htoben(&var, sizeof(type)); break;\
             case TYPE_IS_LITTLE_ENDIAN: my_htolen(&var, sizeof(type)); break;\
             default: break;                                                  \
           }                                                                  \
         } STMT_END
# else
#  define DO_BO_UNPACK_N(var, type)	BO_CANT_DOIT(unpack, type)
#  define DO_BO_PACK_N(var, type)	BO_CANT_DOIT(pack, type)
# endif

#endif /* PERL_PACK_CAN_BYTEORDER */

#define PACK_SIZE_CANNOT_CSUM		0x80
#define PACK_SIZE_UNPREDICTABLE		0x40	/* Not a fixed size element */
#define PACK_SIZE_MASK			0x3F

/* These tables are regenerated by genpacksizetables.pl (and then hand pasted
   in).  You're unlikely ever to need to regenerate them.  */

#if TYPE_IS_SHRIEKING != 0x100
   ++++shriek offset should be 256
#endif

typedef U8 packprops_t;
#if 'J'-'I' == 1
/* ASCII */
STATIC const packprops_t packprops[512] = {
    /* normal */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0,
    /* C */ sizeof(unsigned char),
#if defined(HAS_LONG_DOUBLE) && defined(USE_LONG_DOUBLE)
    /* D */ LONG_DOUBLESIZE,
#else
    0,
#endif
    0,
    /* F */ NVSIZE,
    0, 0,
    /* I */ sizeof(unsigned int),
    /* J */ UVSIZE,
    0,
    /* L */ SIZE32,
    0,
    /* N */ SIZE32,
    0, 0,
#if defined(HAS_QUAD)
    /* Q */ sizeof(Uquad_t),
#else
    0,
#endif
    0,
    /* S */ SIZE16,
    0,
    /* U */ sizeof(char) | PACK_SIZE_UNPREDICTABLE,
    /* V */ SIZE32,
    /* W */ sizeof(unsigned char) | PACK_SIZE_UNPREDICTABLE,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* c */ sizeof(char),
    /* d */ sizeof(double),
    0,
    /* f */ sizeof(float),
    0, 0,
    /* i */ sizeof(int),
    /* j */ IVSIZE,
    0,
    /* l */ SIZE32,
    0,
    /* n */ SIZE16,
    0,
    /* p */ sizeof(char *) | PACK_SIZE_CANNOT_CSUM,
#if defined(HAS_QUAD)
    /* q */ sizeof(Quad_t),
#else
    0,
#endif
    0,
    /* s */ SIZE16,
    0, 0,
    /* v */ SIZE16,
    /* w */ sizeof(char) | PACK_SIZE_UNPREDICTABLE | PACK_SIZE_CANNOT_CSUM,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    /* shrieking */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* I */ sizeof(unsigned int),
    0, 0,
    /* L */ sizeof(unsigned long),
    0,
#if defined(PERL_PACK_CAN_SHRIEKSIGN)
    /* N */ SIZE32,
#else
    0,
#endif
    0, 0, 0, 0,
    /* S */ sizeof(unsigned short),
    0, 0,
#if defined(PERL_PACK_CAN_SHRIEKSIGN)
    /* V */ SIZE32,
#else
    0,
#endif
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0,
    /* i */ sizeof(int),
    0, 0,
    /* l */ sizeof(long),
    0,
#if defined(PERL_PACK_CAN_SHRIEKSIGN)
    /* n */ SIZE16,
#else
    0,
#endif
    0, 0, 0, 0,
    /* s */ sizeof(short),
    0, 0,
#if defined(PERL_PACK_CAN_SHRIEKSIGN)
    /* v */ SIZE16,
#else
    0,
#endif
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0
};
#else
/* EBCDIC (or bust) */
STATIC const packprops_t packprops[512] = {
    /* normal */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0,
    /* c */ sizeof(char),
    /* d */ sizeof(double),
    0,
    /* f */ sizeof(float),
    0, 0,
    /* i */ sizeof(int),
    0, 0, 0, 0, 0, 0, 0,
    /* j */ IVSIZE,
    0,
    /* l */ SIZE32,
    0,
    /* n */ SIZE16,
    0,
    /* p */ sizeof(char *) | PACK_SIZE_CANNOT_CSUM,
#if defined(HAS_QUAD)
    /* q */ sizeof(Quad_t),
#else
    0,
#endif
    0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* s */ SIZE16,
    0, 0,
    /* v */ SIZE16,
    /* w */ sizeof(char) | PACK_SIZE_UNPREDICTABLE | PACK_SIZE_CANNOT_CSUM,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* C */ sizeof(unsigned char),
#if defined(HAS_LONG_DOUBLE) && defined(USE_LONG_DOUBLE)
    /* D */ LONG_DOUBLESIZE,
#else
    0,
#endif
    0,
    /* F */ NVSIZE,
    0, 0,
    /* I */ sizeof(unsigned int),
    0, 0, 0, 0, 0, 0, 0,
    /* J */ UVSIZE,
    0,
    /* L */ SIZE32,
    0,
    /* N */ SIZE32,
    0, 0,
#if defined(HAS_QUAD)
    /* Q */ sizeof(Uquad_t),
#else
    0,
#endif
    0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* S */ SIZE16,
    0,
    /* U */ sizeof(char) | PACK_SIZE_UNPREDICTABLE,
    /* V */ SIZE32,
    /* W */ sizeof(unsigned char) | PACK_SIZE_UNPREDICTABLE,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* shrieking */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* i */ sizeof(int),
    0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* l */ sizeof(long),
    0,
#if defined(PERL_PACK_CAN_SHRIEKSIGN)
    /* n */ SIZE16,
#else
    0,
#endif
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* s */ sizeof(short),
    0, 0,
#if defined(PERL_PACK_CAN_SHRIEKSIGN)
    /* v */ SIZE16,
#else
    0,
#endif
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0,
    /* I */ sizeof(unsigned int),
    0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* L */ sizeof(unsigned long),
    0,
#if defined(PERL_PACK_CAN_SHRIEKSIGN)
    /* N */ SIZE32,
#else
    0,
#endif
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* S */ sizeof(unsigned short),
    0, 0,
#if defined(PERL_PACK_CAN_SHRIEKSIGN)
    /* V */ SIZE32,
#else
    0,
#endif
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};
#endif

STATIC U8
uni_to_byte(pTHX_ const char **s, const char *end, I32 datumtype)
{
    SensorCall(15263);STRLEN retlen;
    UV val = utf8n_to_uvchr((U8 *) *s, end-*s, &retlen,
			 ckWARN(WARN_UTF8) ? 0 : UTF8_ALLOW_ANY);
    /* We try to process malformed UTF-8 as much as possible (preferably with
       warnings), but these two mean we make no progress in the string and
       might enter an infinite loop */
    SensorCall(15265);if (retlen == (STRLEN) -1 || retlen == 0)
	{/*587*/SensorCall(15264);Perl_croak(aTHX_ "Malformed UTF-8 string in '%c' format in unpack",
		   (int) TYPE_NO_MODIFIERS(datumtype));/*588*/}
    SensorCall(15267);if (val >= 0x100) {
	SensorCall(15266);Perl_ck_warner(aTHX_ packWARN(WARN_UNPACK),
		       "Character in '%c' format wrapped in unpack",
		       (int) TYPE_NO_MODIFIERS(datumtype));
	val &= 0xff;
    }
    SensorCall(15268);*s += retlen;
    {U8  ReplaceReturn1599 = (U8)val; SensorCall(15269); return ReplaceReturn1599;}
}

#define SHIFT_BYTE(utf8, s, strend, datumtype) ((utf8) ? \
	uni_to_byte(aTHX_ &(s), (strend), (datumtype)) : \
	*(U8 *)(s)++)

STATIC bool
uni_to_bytes(pTHX_ const char **s, const char *end, const char *buf, int buf_len, I32 datumtype)
{
    SensorCall(15270);UV val;
    STRLEN retlen;
    const char *from = *s;
    int bad = 0;
    const U32 flags = ckWARN(WARN_UTF8) ?
	UTF8_CHECK_ONLY : (UTF8_CHECK_ONLY | UTF8_ALLOW_ANY);
    SensorCall(15279);for (;buf_len > 0; buf_len--) {
	SensorCall(15271);if (from >= end) return FALSE;
	SensorCall(15272);val = utf8n_to_uvchr((U8 *) from, end-from, &retlen, flags);
	SensorCall(15275);if (retlen == (STRLEN) -1 || retlen == 0) {
	    SensorCall(15273);from += UTF8SKIP(from);
	    bad |= 1;
	} else {/*589*/SensorCall(15274);from += retlen;/*590*/}
	SensorCall(15277);if (val >= 0x100) {
	    SensorCall(15276);bad |= 2;
	    val &= 0xff;
	}
	SensorCall(15278);*(U8 *)buf++ = (U8)val;
    }
    /* We have enough characters for the buffer. Did we have problems ? */
    SensorCall(15289);if (bad) {
	SensorCall(15280);if (bad & 1) {
	    /* Rewalk the string fragment while warning */
	    SensorCall(15281);const char *ptr;
	    const int flags = ckWARN(WARN_UTF8) ? 0 : UTF8_ALLOW_ANY;
	    SensorCall(15284);for (ptr = *s; ptr < from; ptr += UTF8SKIP(ptr)) {
		SensorCall(15282);if (ptr >= end) {/*591*/SensorCall(15283);break;/*592*/}
		utf8n_to_uvuni((U8 *) ptr, end-ptr, &retlen, flags);
	    }
	    SensorCall(15286);if (from > end) {/*593*/SensorCall(15285);from = end;/*594*/}
	}
	SensorCall(15288);if ((bad & 2))
	    {/*595*/SensorCall(15287);Perl_ck_warner(aTHX_ packWARN(datumtype & TYPE_IS_PACK ?
				       WARN_PACK : WARN_UNPACK),
			   "Character(s) in '%c' format wrapped in %s",
			   (int) TYPE_NO_MODIFIERS(datumtype),
			   datumtype & TYPE_IS_PACK ? "pack" : "unpack");/*596*/}
    }
    SensorCall(15290);*s = from;
    {_Bool  ReplaceReturn1598 = TRUE; SensorCall(15291); return ReplaceReturn1598;}
}

STATIC bool
next_uni_uu(pTHX_ const char **s, const char *end, I32 *out)
{
SensorCall(15292);    dVAR;
    STRLEN retlen;
    const UV val = utf8n_to_uvchr((U8 *) *s, end-*s, &retlen, UTF8_CHECK_ONLY);
    SensorCall(15295);if (val >= 0x100 || !ISUUCHAR(val) ||
	retlen == (STRLEN) -1 || retlen == 0) {
	SensorCall(15293);*out = 0;
	{_Bool  ReplaceReturn1597 = FALSE; SensorCall(15294); return ReplaceReturn1597;}
    }
    SensorCall(15296);*out = PL_uudmap[val] & 077;
    *s += retlen;
    {_Bool  ReplaceReturn1596 = TRUE; SensorCall(15297); return ReplaceReturn1596;}
}

STATIC char *
S_bytes_to_uni(const U8 *start, STRLEN len, char *dest) {
    SensorCall(15298);const U8 * const end = start + len;

    PERL_ARGS_ASSERT_BYTES_TO_UNI;

    SensorCall(15303);while (start < end) {
	SensorCall(15299);const UV uv = NATIVE_TO_ASCII(*start);
	SensorCall(15301);if (UNI_IS_INVARIANT(uv))
	    *dest++ = (char)(U8)UTF_TO_NATIVE(uv);
	else {
	    SensorCall(15300);*dest++ = (char)(U8)UTF8_EIGHT_BIT_HI(uv);
	    *dest++ = (char)(U8)UTF8_EIGHT_BIT_LO(uv);
	}
	SensorCall(15302);start++;
    }
    {char * ReplaceReturn1595 = dest; SensorCall(15304); return ReplaceReturn1595;}
}

#define PUSH_BYTES(utf8, cur, buf, len)				\
STMT_START {							\
    if (utf8)							\
	(cur) = bytes_to_uni((U8 *) buf, len, (cur));		\
    else {							\
	Copy(buf, cur, len, char);				\
	(cur) += (len);						\
    }								\
} STMT_END

#define GROWING(utf8, cat, start, cur, in_len)	\
STMT_START {					\
    STRLEN glen = (in_len);			\
    if (utf8) glen *= UTF8_EXPAND;		\
    if ((cur) + glen >= (start) + SvLEN(cat)) {	\
	(start) = sv_exp_grow(cat, glen);	\
	(cur) = (start) + SvCUR(cat);		\
    }						\
} STMT_END

#define PUSH_GROWING_BYTES(utf8, cat, start, cur, buf, in_len) \
STMT_START {					\
    const STRLEN glen = (in_len);		\
    STRLEN gl = glen;				\
    if (utf8) gl *= UTF8_EXPAND;		\
    if ((cur) + gl >= (start) + SvLEN(cat)) {	\
        *cur = '\0';				\
        SvCUR_set((cat), (cur) - (start));	\
	(start) = sv_exp_grow(cat, gl);		\
	(cur) = (start) + SvCUR(cat);		\
    }						\
    PUSH_BYTES(utf8, cur, buf, glen);		\
} STMT_END

#define PUSH_BYTE(utf8, s, byte)		\
STMT_START {					\
    if (utf8) {					\
	const U8 au8 = (byte);			\
	(s) = bytes_to_uni(&au8, 1, (s));	\
    } else *(U8 *)(s)++ = (byte);		\
} STMT_END

/* Only to be used inside a loop (see the break) */
#define NEXT_UNI_VAL(val, cur, str, end, utf8_flags)		\
STMT_START {							\
    STRLEN retlen;						\
    if (str >= end) break;					\
    val = utf8n_to_uvchr((U8 *) str, end-str, &retlen, utf8_flags);	\
    if (retlen == (STRLEN) -1 || retlen == 0) {			\
	*cur = '\0';						\
	Perl_croak(aTHX_ "Malformed UTF-8 string in pack");	\
    }								\
    str += retlen;						\
} STMT_END

static const char *_action( const tempsym_t* symptr )
{
    {const char * ReplaceReturn1594 = (const char *)(( symptr->flags & FLAG_PACK ) ? "pack" : "unpack"); SensorCall(15305); return ReplaceReturn1594;}
}

/* Returns the sizeof() struct described by pat */
STATIC I32
S_measure_struct(pTHX_ tempsym_t* symptr)
{
    SensorCall(15306);I32 total = 0;

    PERL_ARGS_ASSERT_MEASURE_STRUCT;

    SensorCall(15345);while (next_symbol(symptr)) {
	SensorCall(15307);I32 len;
	int size;

        SensorCall(15312);switch (symptr->howlen) {
	  case e_star:
   	    SensorCall(15308);Perl_croak(aTHX_ "Within []-length '*' not allowed in %s",
                        _action( symptr ) );
            SensorCall(15309);break;
	  default:
	    /* e_no_len and e_number */
	    SensorCall(15310);len = symptr->length;
	    SensorCall(15311);break;
        }

	SensorCall(15313);size = packprops[TYPE_NO_ENDIANNESS(symptr->code)] & PACK_SIZE_MASK;
	SensorCall(15343);if (!size) {
            SensorCall(15314);int star;
	    /* endianness doesn't influence the size of a type */
	    SensorCall(15342);switch(TYPE_NO_ENDIANNESS(symptr->code)) {
	    default:
		SensorCall(15315);Perl_croak(aTHX_ "Invalid type '%c' in %s",
			   (int)TYPE_NO_MODIFIERS(symptr->code),
                           _action( symptr ) );
#ifdef PERL_PACK_CAN_SHRIEKSIGN
	    case '.' | TYPE_IS_SHRIEKING:
	    case '@' | TYPE_IS_SHRIEKING:
#endif
	    case '@':
	    case '.':
	    case '/':
	    case 'U':			/* XXXX Is it correct? */
	    case 'w':
	    case 'u':
		SensorCall(15316);Perl_croak(aTHX_ "Within []-length '%c' not allowed in %s",
			   (int) TYPE_NO_MODIFIERS(symptr->code),
                           _action( symptr ) );
	    case '%':
		SensorCall(15317);size = 0;
		SensorCall(15318);break;
	    case '(':
	    {
		SensorCall(15319);tempsym_t savsym = *symptr;
		symptr->patptr = savsym.grpbeg;
		symptr->patend = savsym.grpend;
		/* XXXX Theoretically, we need to measure many times at
		   different positions, since the subexpression may contain
		   alignment commands, but be not of aligned length.
		   Need to detect this and croak().  */
		size = measure_struct(symptr);
		*symptr = savsym;
		SensorCall(15320);break;
	    }
	    case 'X' | TYPE_IS_SHRIEKING:
		/* XXXX Is this useful?  Then need to treat MEASURE_BACKWARDS.
		 */
		SensorCall(15321);if (!len)		/* Avoid division by 0 */
		    {/*33*/SensorCall(15322);len = 1;/*34*/}
		SensorCall(15323);len = total % len;	/* Assumed: the start is aligned. */
		/* FALL THROUGH */
	    case 'X':
		SensorCall(15324);size = -1;
		SensorCall(15326);if (total < len)
                    {/*35*/SensorCall(15325);Perl_croak(aTHX_ "'X' outside of string in %s", _action( symptr ) );/*36*/}
		SensorCall(15327);break;
	    case 'x' | TYPE_IS_SHRIEKING:
		SensorCall(15328);if (!len)		/* Avoid division by 0 */
		    {/*37*/SensorCall(15329);len = 1;/*38*/}
		SensorCall(15330);star = total % len;	/* Assumed: the start is aligned. */
		SensorCall(15333);if (star)		/* Other portable ways? */
		    {/*39*/SensorCall(15331);len = len - star;/*40*/}
		else
		    {/*41*/SensorCall(15332);len = 0;/*42*/}
		/* FALL THROUGH */
	    case 'x':
	    case 'A':
	    case 'Z':
	    case 'a':
		SensorCall(15334);size = 1;
		SensorCall(15335);break;
	    case 'B':
	    case 'b':
		SensorCall(15336);len = (len + 7)/8;
		size = 1;
		SensorCall(15337);break;
	    case 'H':
	    case 'h':
		SensorCall(15338);len = (len + 1)/2;
		size = 1;
		SensorCall(15339);break;

	    case 'P':
		SensorCall(15340);len = 1;
		size = sizeof(char*);
		SensorCall(15341);break;
	    }
	}
	SensorCall(15344);total += len * size;
    }
    {I32  ReplaceReturn1593 = total; SensorCall(15346); return ReplaceReturn1593;}
}


/* locate matching closing parenthesis or bracket
 * returns char pointer to char after match, or NULL
 */
STATIC const char *
S_group_end(pTHX_ register const char *patptr, register const char *patend, char ender)
{
    PERL_ARGS_ASSERT_GROUP_END;

    SensorCall(15347);while (patptr < patend) {
	SensorCall(15348);const char c = *patptr++;

	SensorCall(15360);if (isSPACE(c))
	    {/*15*/SensorCall(15349);continue;/*16*/}
	else {/*17*/SensorCall(15350);if (c == ender)
	    {/*19*/{const char * ReplaceReturn1592 = patptr-1; SensorCall(15351); return ReplaceReturn1592;}/*20*/}
	else {/*21*/SensorCall(15352);if (c == '#') {
	    SensorCall(15353);while (patptr < patend && *patptr != '\n')
		{/*23*/SensorCall(15354);patptr++;/*24*/}
	    SensorCall(15355);continue;
	} else {/*25*/SensorCall(15356);if (c == '(')
	    {/*27*/SensorCall(15357);patptr = group_end(patptr, patend, ')') + 1;/*28*/}
	else {/*29*/SensorCall(15358);if (c == '[')
	    {/*31*/SensorCall(15359);patptr = group_end(patptr, patend, ']') + 1;/*32*/}/*30*/}/*26*/}/*22*/}/*18*/}
    }
    SensorCall(15361);Perl_croak(aTHX_ "No group ending character '%c' found in template",
               ender);
    {const char * ReplaceReturn1591 = 0; SensorCall(15362); return ReplaceReturn1591;}
}


/* Convert unsigned decimal number to binary.
 * Expects a pointer to the first digit and address of length variable
 * Advances char pointer to 1st non-digit char and returns number
 */
STATIC const char *
S_get_num(pTHX_ register const char *patptr, I32 *lenptr )
{
  SensorCall(15363);I32 len = *patptr++ - '0';

  PERL_ARGS_ASSERT_GET_NUM;

  SensorCall(15367);while (isDIGIT(*patptr)) {
    SensorCall(15364);if (len >= 0x7FFFFFFF/10)
      {/*13*/SensorCall(15365);Perl_croak(aTHX_ "pack/unpack repeat count overflow");/*14*/}
    SensorCall(15366);len = (len * 10) + (*patptr++ - '0');
  }
  SensorCall(15368);*lenptr = len;
  {const char * ReplaceReturn1590 = patptr; SensorCall(15369); return ReplaceReturn1590;}
}

/* The marvellous template parsing routine: Using state stored in *symptr,
 * locates next template code and count
 */
STATIC bool
S_next_symbol(pTHX_ tempsym_t* symptr )
{
  SensorCall(15370);const char* patptr = symptr->patptr;
  const char* const patend = symptr->patend;

  PERL_ARGS_ASSERT_NEXT_SYMBOL;

  symptr->flags &= ~FLAG_SLASH;

  SensorCall(15444);while (patptr < patend) {
    SensorCall(15371);if (isSPACE(*patptr))
      {/*45*/SensorCall(15372);patptr++;/*46*/}
    else {/*47*/SensorCall(15373);if (*patptr == '#') {
      SensorCall(15374);patptr++;
      SensorCall(15376);while (patptr < patend && *patptr != '\n')
	{/*49*/SensorCall(15375);patptr++;/*50*/}
      SensorCall(15378);if (patptr < patend)
	{/*51*/SensorCall(15377);patptr++;/*52*/}
    } else {
      /* We should have found a template code */
      SensorCall(15379);I32 code = *patptr++ & 0xFF;
      U32 inherited_modifiers = 0;

      SensorCall(15383);if (code == ','){ /* grandfather in commas but with a warning */
	SensorCall(15380);if (((symptr->flags & FLAG_COMMA) == 0) && ckWARN(WARN_UNPACK)){
          SensorCall(15381);symptr->flags |= FLAG_COMMA;
	  Perl_warner(aTHX_ packWARN(WARN_UNPACK),
	 	      "Invalid type ',' in %s", _action( symptr ) );
        }
	SensorCall(15382);continue;
      }

      /* for '(', skip to ')' */
      SensorCall(15389);if (code == '(') {
        SensorCall(15384);if( isDIGIT(*patptr) || *patptr == '*' || *patptr == '[' )
          {/*53*/SensorCall(15385);Perl_croak(aTHX_ "()-group starts with a count in %s",
                        _action( symptr ) );/*54*/}
        SensorCall(15386);symptr->grpbeg = patptr;
        patptr = 1 + ( symptr->grpend = group_end(patptr, patend, ')') );
        SensorCall(15388);if( symptr->level >= MAX_SUB_TEMPLATE_LEVEL )
	  {/*55*/SensorCall(15387);Perl_croak(aTHX_ "Too deeply nested ()-groups in %s",
                        _action( symptr ) );/*56*/}
      }

      /* look for group modifiers to inherit */
      SensorCall(15390);if (TYPE_ENDIANNESS(symptr->flags)) {
        if (strchr(ENDIANNESS_ALLOWED_TYPES, TYPE_NO_MODIFIERS(code)))
          inherited_modifiers |= TYPE_ENDIANNESS(symptr->flags);
      }

      /* look for modifiers */
      SensorCall(15412);while (patptr < patend) {
        SensorCall(15391);const char *allowed;
        I32 modifier;
        SensorCall(15400);switch (*patptr) {
          case '!':
            SensorCall(15392);modifier = TYPE_IS_SHRIEKING;
            allowed = SHRIEKING_ALLOWED_TYPES;
            SensorCall(15393);break;
#ifdef PERL_PACK_CAN_BYTEORDER
          case '>':
            SensorCall(15394);modifier = TYPE_IS_BIG_ENDIAN;
            allowed = ENDIANNESS_ALLOWED_TYPES;
            SensorCall(15395);break;
          case '<':
            SensorCall(15396);modifier = TYPE_IS_LITTLE_ENDIAN;
            allowed = ENDIANNESS_ALLOWED_TYPES;
            SensorCall(15397);break;
#endif /* PERL_PACK_CAN_BYTEORDER */
          default:
            SensorCall(15398);allowed = "";
            modifier = 0;
            SensorCall(15399);break;
        }

        SensorCall(15402);if (modifier == 0)
          {/*59*/SensorCall(15401);break;/*60*/}

        SensorCall(15404);if (!strchr(allowed, TYPE_NO_MODIFIERS(code)))
          {/*61*/SensorCall(15403);Perl_croak(aTHX_ "'%c' allowed only after types %s in %s", *patptr,
                        allowed, _action( symptr ) );/*62*/}

        SensorCall(15408);if (TYPE_ENDIANNESS(code | modifier) == TYPE_ENDIANNESS_MASK)
          {/*63*/SensorCall(15405);Perl_croak(aTHX_ "Can't use both '<' and '>' after type '%c' in %s",
                     (int) TYPE_NO_MODIFIERS(code), _action( symptr ) );/*64*/}
        else {/*65*/SensorCall(15406);if (TYPE_ENDIANNESS(code | modifier | inherited_modifiers) ==
                 TYPE_ENDIANNESS_MASK)
          {/*67*/SensorCall(15407);Perl_croak(aTHX_ "Can't use '%c' in a group with different byte-order in %s",
                     *patptr, _action( symptr ) );/*68*/}/*66*/}

        SensorCall(15410);if ((code & modifier)) {
	    SensorCall(15409);Perl_ck_warner(aTHX_ packWARN(WARN_UNPACK),
			   "Duplicate modifier '%c' after '%c' in %s",
			   *patptr, (int) TYPE_NO_MODIFIERS(code),
			   _action( symptr ) );
        }

        SensorCall(15411);code |= modifier;
        patptr++;
      }

      /* inherit modifiers */
      SensorCall(15413);code |= inherited_modifiers;

      /* look for count and/or / */
      SensorCall(15441);if (patptr < patend) {
	SensorCall(15414);if (isDIGIT(*patptr)) {
 	  SensorCall(15415);patptr = get_num( patptr, &symptr->length );
          symptr->howlen = e_number;

        } else {/*69*/SensorCall(15416);if (*patptr == '*') {
          SensorCall(15417);patptr++;
          symptr->howlen = e_star;

        } else {/*71*/SensorCall(15418);if (*patptr == '[') {
          SensorCall(15419);const char* lenptr = ++patptr;
          symptr->howlen = e_number;
          patptr = group_end( patptr, patend, ']' ) + 1;
          /* what kind of [] is it? */
          SensorCall(15424);if (isDIGIT(*lenptr)) {
            SensorCall(15420);lenptr = get_num( lenptr, &symptr->length );
            SensorCall(15422);if( *lenptr != ']' )
              {/*73*/SensorCall(15421);Perl_croak(aTHX_ "Malformed integer in [] in %s",
                            _action( symptr ) );/*74*/}
          } else {
            SensorCall(15423);tempsym_t savsym = *symptr;
            symptr->patend = patptr-1;
            symptr->patptr = lenptr;
            savsym.length = measure_struct(symptr);
            *symptr = savsym;
          }
        } else {
          SensorCall(15425);symptr->howlen = e_no_len;
          symptr->length = 1;
        ;/*72*/}/*70*/}}

        /* try to find / */
        SensorCall(15439);while (patptr < patend) {
          SensorCall(15426);if (isSPACE(*patptr))
            {/*75*/SensorCall(15427);patptr++;/*76*/}
          else {/*77*/SensorCall(15428);if (*patptr == '#') {
            SensorCall(15429);patptr++;
            SensorCall(15431);while (patptr < patend && *patptr != '\n')
	      {/*79*/SensorCall(15430);patptr++;/*80*/}
            SensorCall(15433);if (patptr < patend)
	      {/*81*/SensorCall(15432);patptr++;/*82*/}
          } else {
            SensorCall(15434);if (*patptr == '/') {
              SensorCall(15435);symptr->flags |= FLAG_SLASH;
              patptr++;
              SensorCall(15437);if (patptr < patend &&
                  (isDIGIT(*patptr) || *patptr == '*' || *patptr == '['))
                {/*83*/SensorCall(15436);Perl_croak(aTHX_ "'/' does not take a repeat count in %s",
                            _action( symptr ) );/*84*/}
            }
            SensorCall(15438);break;
	  ;/*78*/}}
	}
      } else {
        /* at end - no count, no / */
        SensorCall(15440);symptr->howlen = e_no_len;
        symptr->length = 1;
      }

      SensorCall(15442);symptr->code = code;
      symptr->patptr = patptr;
      {_Bool  ReplaceReturn1589 = TRUE; SensorCall(15443); return ReplaceReturn1589;}
    ;/*48*/}}
  }
  SensorCall(15445);symptr->patptr = patptr;
  {_Bool  ReplaceReturn1588 = FALSE; SensorCall(15446); return ReplaceReturn1588;}
}

/*
   There is no way to cleanly handle the case where we should process the
   string per byte in its upgraded form while it's really in downgraded form
   (e.g. estimates like strend-s as an upper bound for the number of
   characters left wouldn't work). So if we foresee the need of this
   (pattern starts with U or contains U0), we want to work on the encoded
   version of the string. Users are advised to upgrade their pack string
   themselves if they need to do a lot of unpacks like this on it
*/
STATIC bool
need_utf8(const char *pat, const char *patend)
{
SensorCall(15447);    bool first = TRUE;

    PERL_ARGS_ASSERT_NEED_UTF8;

    SensorCall(15452);while (pat < patend) {
	SensorCall(15448);if (pat[0] == '#') {
	    SensorCall(15449);pat++;
	    pat = (const char *) memchr(pat, '\n', patend-pat);
	    SensorCall(15450);if (!pat) return FALSE;
	} else if (pat[0] == 'U') {
	    if (first || pat[1] == '0') return TRUE;
	} else first = FALSE;
	SensorCall(15451);pat++;
    }
    {_Bool  ReplaceReturn1587 = FALSE; SensorCall(15453); return ReplaceReturn1587;}
}

STATIC char
first_symbol(const char *pat, const char *patend) {
    PERL_ARGS_ASSERT_FIRST_SYMBOL;

    SensorCall(15454);while (pat < patend) {
	SensorCall(15455);if (pat[0] != '#') {/*9*/{char  ReplaceReturn1586 = pat[0]; SensorCall(15456); return ReplaceReturn1586;}/*10*/}
	SensorCall(15457);pat++;
	pat = (const char *) memchr(pat, '\n', patend-pat);
	SensorCall(15459);if (!pat) {/*11*/{char  ReplaceReturn1585 = 0; SensorCall(15458); return ReplaceReturn1585;}/*12*/}
	SensorCall(15460);pat++;
    }
    {char  ReplaceReturn1584 = 0; SensorCall(15461); return ReplaceReturn1584;}
}

/*
=for apidoc unpackstring

The engine implementing unpack() Perl function. C<unpackstring> puts the
extracted list items on the stack and returns the number of elements.
Issue C<PUTBACK> before and C<SPAGAIN> after the call to this function.

=cut */

I32
Perl_unpackstring(pTHX_ const char *pat, const char *patend, const char *s, const char *strend, U32 flags)
{
    SensorCall(15462);tempsym_t sym;

    PERL_ARGS_ASSERT_UNPACKSTRING;

    SensorCall(15466);if (flags & FLAG_DO_UTF8) {/*3*/SensorCall(15463);flags |= FLAG_WAS_UTF8;/*4*/}
    else {/*5*/SensorCall(15464);if (need_utf8(pat, patend)) {
	/* We probably should try to avoid this in case a scalar context call
	   wouldn't get to the "U0" */
	SensorCall(15465);STRLEN len = strend - s;
	s = (char *) bytes_to_utf8((U8 *) s, &len);
	SAVEFREEPV(s);
	strend = s + len;
	flags |= FLAG_DO_UTF8;
    ;/*6*/}}

    if (first_symbol(pat, patend) != 'U' && (flags & FLAG_DO_UTF8))
	flags |= FLAG_PARSE_UTF8;

    TEMPSYM_INIT(&sym, pat, patend, flags);

    {I32  ReplaceReturn1583 = unpack_rec(&sym, s, s, strend, NULL ); SensorCall(15467); return ReplaceReturn1583;}
}

STATIC I32
S_unpack_rec(pTHX_ tempsym_t* symptr, const char *s, const char *strbeg, const char *strend, const char **new_s )
{
SensorCall(15468);    dVAR; dSP;
    SV *sv = NULL;
    const I32 start_sp_offset = SP - PL_stack_base;
    howlen_t howlen;
    I32 checksum = 0;
    UV cuv = 0;
    NV cdouble = 0.0;
    const int bits_in_uv = CHAR_BIT * sizeof(cuv);
    bool beyond = FALSE;
    bool explicit_length;
    const bool unpack_only_one = (symptr->flags & FLAG_UNPACK_ONLY_ONE) != 0;
    bool utf8 = (symptr->flags & FLAG_PARSE_UTF8) ? 1 : 0;

    PERL_ARGS_ASSERT_UNPACK_REC;

    symptr->strbeg = s - strbeg;

    SensorCall(15957);while (next_symbol(symptr)) {
	SensorCall(15469);packprops_t props;
	I32 len;
        I32 datumtype = symptr->code;
	/* do first one only unless in list context
	   / is implemented by unpacking the count, then popping it from the
	   stack, so must check that we're not in the middle of a /  */
        SensorCall(15471);if ( unpack_only_one
	     && (SP - PL_stack_base == start_sp_offset + 1)
	     && (datumtype != '/') )   /* XXX can this be omitted */
            {/*261*/SensorCall(15470);break;/*262*/}

        SensorCall(15476);switch (howlen = symptr->howlen) {
	  case e_star:
	    SensorCall(15472);len = strend - strbeg;	/* long enough */
	    SensorCall(15473);break;
	  default:
	    /* e_no_len and e_number */
	    SensorCall(15474);len = symptr->length;
	    SensorCall(15475);break;
        }

        SensorCall(15477);explicit_length = TRUE;
      redo_switch:
        beyond = s >= strend;

	props = packprops[TYPE_NO_ENDIANNESS(datumtype)];
	SensorCall(15484);if (props) {
	    /* props nonzero means we can process this letter. */
            SensorCall(15478);const long size = props & PACK_SIZE_MASK;
            const long howmany = (strend - s) / size;
	    SensorCall(15480);if (len > howmany)
		{/*263*/SensorCall(15479);len = howmany;/*264*/}

	    SensorCall(15483);if (!checksum || (props & PACK_SIZE_CANNOT_CSUM)) {
		SensorCall(15481);if (len && unpack_only_one) {/*265*/SensorCall(15482);len = 1;/*266*/}
		EXTEND(SP, len);
		EXTEND_MORTAL(len);
	    }
	}

	SensorCall(15930);switch(TYPE_NO_ENDIANNESS(datumtype)) {
	default:
	    SensorCall(15485);Perl_croak(aTHX_ "Invalid type '%c' in unpack", (int)TYPE_NO_MODIFIERS(datumtype) );

	case '%':
	    SensorCall(15486);if (howlen == e_no_len)
		{/*267*/SensorCall(15487);len = 16;/*268*/}		/* len is not specified */
	    SensorCall(15488);checksum = len;
	    cuv = 0;
	    cdouble = 0;
	    SensorCall(15489);continue;
	    break;
	case '(':
	{
            SensorCall(15490);tempsym_t savsym = *symptr;
            const U32 group_modifiers = TYPE_MODIFIERS(datumtype & ~symptr->flags);
	    symptr->flags |= group_modifiers;
            symptr->patend = savsym.grpend;
	    symptr->previous = &savsym;
            symptr->level++;
	    PUTBACK;
	    SensorCall(15492);if (len && unpack_only_one) {/*269*/SensorCall(15491);len = 1;/*270*/}
	    SensorCall(15496);while (len--) {
  	        SensorCall(15493);symptr->patptr = savsym.grpbeg;
		if (utf8) symptr->flags |=  FLAG_PARSE_UTF8;
		else      symptr->flags &= ~FLAG_PARSE_UTF8;
 	        unpack_rec(symptr, s, strbeg, strend, &s);
                SensorCall(15495);if (s == strend && savsym.howlen == e_star)
		    {/*275*/SensorCall(15494);break;/*276*/} /* No way to continue */
	    }
	    SPAGAIN;
            SensorCall(15497);savsym.flags = symptr->flags & ~group_modifiers;
            *symptr = savsym;
	    SensorCall(15498);break;
	}
#ifdef PERL_PACK_CAN_SHRIEKSIGN
	case '.' | TYPE_IS_SHRIEKING:
#endif
	case '.': {
	    SensorCall(15499);const char *from;
	    SV *sv;
#ifdef PERL_PACK_CAN_SHRIEKSIGN
	    const bool u8 = utf8 && !(datumtype & TYPE_IS_SHRIEKING);
#else /* PERL_PACK_CAN_SHRIEKSIGN */
	    const bool u8 = utf8;
#endif
	    SensorCall(15507);if (howlen == e_star) {/*277*/SensorCall(15500);from = strbeg;/*278*/}
	    else {/*279*/SensorCall(15501);if (len <= 0) {/*281*/SensorCall(15502);from = s;/*282*/}
	    else {
		SensorCall(15503);tempsym_t *group = symptr;

		SensorCall(15505);while (--len && group) {/*283*/SensorCall(15504);group = group->previous;/*284*/}
		SensorCall(15506);from = group ? strbeg + group->strbeg : strbeg;
	    ;/*280*/}}
	    SensorCall(15508);sv = from <= s ?
		newSVuv(  u8 ? (UV) utf8_length((const U8*)from, (const U8*)s) : (UV) (s-from)) :
		newSViv(-(u8 ? (IV) utf8_length((const U8*)s, (const U8*)from) : (IV) (from-s)));
	    mXPUSHs(sv);
	    SensorCall(15509);break;
	}
#ifdef PERL_PACK_CAN_SHRIEKSIGN
	case '@' | TYPE_IS_SHRIEKING:
#endif
	case '@':
	    SensorCall(15510);s = strbeg + symptr->strbeg;
#ifdef PERL_PACK_CAN_SHRIEKSIGN
	    SensorCall(15520);if (utf8  && !(datumtype & TYPE_IS_SHRIEKING))
#else /* PERL_PACK_CAN_SHRIEKSIGN */
	    if (utf8)
#endif
	    {
		SensorCall(15511);while (len > 0) {
		    SensorCall(15512);if (s >= strend)
			{/*285*/SensorCall(15513);Perl_croak(aTHX_ "'@' outside of string in unpack");/*286*/}
		    SensorCall(15514);s += UTF8SKIP(s);
		    len--;
		}
		SensorCall(15516);if (s > strend)
		    {/*287*/SensorCall(15515);Perl_croak(aTHX_ "'@' outside of string with malformed UTF-8 in unpack");/*288*/}
	    } else {
		SensorCall(15517);if (strend-s < len)
		    {/*289*/SensorCall(15518);Perl_croak(aTHX_ "'@' outside of string in unpack");/*290*/}
		SensorCall(15519);s += len;
	    }
	    SensorCall(15521);break;
 	case 'X' | TYPE_IS_SHRIEKING:
 	    SensorCall(15522);if (!len)			/* Avoid division by 0 */
 		{/*291*/SensorCall(15523);len = 1;/*292*/}
	    SensorCall(15533);if (utf8) {
		SensorCall(15524);const char *hop, *last;
		I32 l = len;
		hop = last = strbeg;
		SensorCall(15528);while (hop < s) {
		    SensorCall(15525);hop += UTF8SKIP(hop);
		    SensorCall(15527);if (--l == 0) {
			SensorCall(15526);last = hop;
			l = len;
		    }
		}
		SensorCall(15530);if (last > s)
		    {/*293*/SensorCall(15529);Perl_croak(aTHX_ "Malformed UTF-8 string in unpack");/*294*/}
		SensorCall(15531);s = last;
		SensorCall(15532);break;
	    }
	    SensorCall(15534);len = (s - strbeg) % len;
 	    /* FALL THROUGH */
	case 'X':
	    SensorCall(15535);if (utf8) {
		SensorCall(15536);while (len > 0) {
		    SensorCall(15537);if (s <= strbeg)
			{/*295*/SensorCall(15538);Perl_croak(aTHX_ "'X' outside of string in unpack");/*296*/}
		    SensorCall(15541);while (--s, UTF8_IS_CONTINUATION(*s)) {
			SensorCall(15539);if (s <= strbeg)
			    {/*297*/SensorCall(15540);Perl_croak(aTHX_ "'X' outside of string in unpack");/*298*/}
		    }
		    SensorCall(15542);len--;
		}
	    } else {
		SensorCall(15543);if (len > s - strbeg)
		    {/*299*/SensorCall(15544);Perl_croak(aTHX_ "'X' outside of string in unpack" );/*300*/}
		SensorCall(15545);s -= len;
	    }
	    SensorCall(15546);break;
 	case 'x' | TYPE_IS_SHRIEKING: {
            SensorCall(15547);I32 ai32;
 	    SensorCall(15549);if (!len)			/* Avoid division by 0 */
 		{/*301*/SensorCall(15548);len = 1;/*302*/}
	    SensorCall(15552);if (utf8) {/*303*/SensorCall(15550);ai32 = utf8_length((U8 *) strbeg, (U8 *) s) % len;/*304*/}
	    else      {/*305*/SensorCall(15551);ai32 = (s - strbeg)                         % len;/*306*/}
	    SensorCall(15554);if (ai32 == 0) {/*307*/SensorCall(15553);break;/*308*/}
	    SensorCall(15555);len -= ai32;
            }
 	    /* FALL THROUGH */
	case 'x':
	    SensorCall(15556);if (utf8) {
		SensorCall(15557);while (len>0) {
		    SensorCall(15558);if (s >= strend)
			{/*309*/SensorCall(15559);Perl_croak(aTHX_ "'x' outside of string in unpack");/*310*/}
		    SensorCall(15560);s += UTF8SKIP(s);
		    len--;
		}
	    } else {
		SensorCall(15561);if (len > strend - s)
		    {/*311*/SensorCall(15562);Perl_croak(aTHX_ "'x' outside of string in unpack");/*312*/}
		SensorCall(15563);s += len;
	    }
	    SensorCall(15564);break;
	case '/':
	    SensorCall(15565);Perl_croak(aTHX_ "'/' must follow a numeric type in unpack");
            SensorCall(15566);break;
	case 'A':
	case 'Z':
	case 'a':
	    SensorCall(15567);if (checksum) {
		/* Preliminary length estimate is assumed done in 'W' */
		SensorCall(15568);if (len > strend - s) {/*313*/SensorCall(15569);len = strend - s;/*314*/}
		SensorCall(15570);goto W_checksum;
	    }
	    SensorCall(15582);if (utf8) {
		SensorCall(15571);I32 l;
		const char *hop;
		SensorCall(15576);for (l=len, hop=s; l>0; l--, hop += UTF8SKIP(hop)) {
		    SensorCall(15572);if (hop >= strend) {
			SensorCall(15573);if (hop > strend)
			    {/*315*/SensorCall(15574);Perl_croak(aTHX_ "Malformed UTF-8 string in unpack");/*316*/}
			SensorCall(15575);break;
		    }
		}
		SensorCall(15578);if (hop > strend)
		    {/*317*/SensorCall(15577);Perl_croak(aTHX_ "Malformed UTF-8 string in unpack");/*318*/}
		SensorCall(15579);len = hop - s;
	    } else {/*319*/SensorCall(15580);if (len > strend - s)
		{/*321*/SensorCall(15581);len = strend - s;/*322*/}/*320*/}

	    SensorCall(15590);if (datumtype == 'Z') {
		/* 'Z' strips stuff after first null */
		SensorCall(15583);const char *ptr, *end;
		end = s + len;
		SensorCall(15586);for (ptr = s; ptr < end; ptr++) {/*323*/SensorCall(15584);if (*ptr == 0) {/*325*/SensorCall(15585);break;/*326*/}/*324*/}
		SensorCall(15587);sv = newSVpvn(s, ptr-s);
		SensorCall(15589);if (howlen == e_star) /* exact for 'Z*' */
		    {/*327*/SensorCall(15588);len = ptr-s + (ptr != strend ? 1 : 0);/*328*/}
	    } else if (datumtype == 'A') {
		/* 'A' strips both nulls and spaces */
		const char *ptr;
		if (utf8 && (symptr->flags & FLAG_WAS_UTF8)) {
		    for (ptr = s+len-1; ptr >= s; ptr--)
			{/*329*/if (*ptr != 0 && !UTF8_IS_CONTINUATION(*ptr) &&
			    !is_utf8_space((U8 *) ptr)) {/*331*/break;/*332*/}/*330*/}
		    if (ptr >= s) ptr += UTF8SKIP(ptr);
		    else {/*333*/ptr++;/*334*/}
		    if (ptr > s+len)
			{/*335*/Perl_croak(aTHX_ "Malformed UTF-8 string in unpack");/*336*/}
		} else {
		    for (ptr = s+len-1; ptr >= s; ptr--)
			{/*337*/if (*ptr != 0 && !isSPACE(*ptr)) {/*339*/break;/*340*/}/*338*/}
		    ptr++;
		}
		sv = newSVpvn(s, ptr-s);
	    } else sv = newSVpvn(s, len);

	    SensorCall(15592);if (utf8) {
		SvUTF8_on(sv);
		/* Undo any upgrade done due to need_utf8() */
		SensorCall(15591);if (!(symptr->flags & FLAG_WAS_UTF8))
		    sv_utf8_downgrade(sv, 0);
	    }
	    mXPUSHs(sv);
	    SensorCall(15593);s += len;
	    SensorCall(15594);break;
	case 'B':
	case 'b': {
	    SensorCall(15595);char *str;
	    SensorCall(15597);if (howlen == e_star || len > (strend - s) * 8)
		{/*341*/SensorCall(15596);len = (strend - s) * 8;/*342*/}
	    SensorCall(15615);if (checksum) {
		SensorCall(15598);if (utf8)
		    {/*343*/SensorCall(15599);while (len >= 8 && s < strend) {
			SensorCall(15600);cuv += PL_bitcount[uni_to_byte(aTHX_ &s, strend, datumtype)];
			len -= 8;
		    ;/*344*/}}
		else
		    {/*345*/SensorCall(15601);while (len >= 8) {
			SensorCall(15602);cuv += PL_bitcount[*(U8 *)s++];
			len -= 8;
		    ;/*346*/}}
		SensorCall(15613);if (len && s < strend) {
		    SensorCall(15603);U8 bits;
		    bits = SHIFT_BYTE(utf8, s, strend, datumtype);
		    SensorCall(15612);if (datumtype == 'b')
			{/*347*/SensorCall(15604);while (len-- > 0) {
			    SensorCall(15605);if (bits & 1) {/*349*/SensorCall(15606);cuv++;/*350*/}
			    SensorCall(15607);bits >>= 1;
			;/*348*/}}
		    else
			{/*351*/SensorCall(15608);while (len-- > 0) {
			    SensorCall(15609);if (bits & 0x80) {/*353*/SensorCall(15610);cuv++;/*354*/}
			    SensorCall(15611);bits <<= 1;
			;/*352*/}}
		}
		SensorCall(15614);break;
	    }

	    SensorCall(15616);sv = sv_2mortal(newSV(len ? len : 1));
	    SvPOK_on(sv);
	    str = SvPVX(sv);
	    SensorCall(15637);if (datumtype == 'b') {
		SensorCall(15617);U8 bits = 0;
		const I32 ai32 = len;
		SensorCall(15626);for (len = 0; len < ai32; len++) {
		    SensorCall(15618);if (len & 7) {/*355*/SensorCall(15619);bits >>= 1;/*356*/}
		    else {/*357*/SensorCall(15620);if (utf8) {
			SensorCall(15621);if (s >= strend) {/*359*/SensorCall(15622);break;/*360*/}
			SensorCall(15623);bits = uni_to_byte(aTHX_ &s, strend, datumtype);
		    } else {/*361*/SensorCall(15624);bits = *(U8 *) s++;/*362*/}/*358*/}
		    SensorCall(15625);*str++ = bits & 1 ? '1' : '0';
		}
	    } else {
		SensorCall(15627);U8 bits = 0;
		const I32 ai32 = len;
		SensorCall(15636);for (len = 0; len < ai32; len++) {
		    SensorCall(15628);if (len & 7) {/*363*/SensorCall(15629);bits <<= 1;/*364*/}
		    else {/*365*/SensorCall(15630);if (utf8) {
			SensorCall(15631);if (s >= strend) {/*367*/SensorCall(15632);break;/*368*/}
			SensorCall(15633);bits = uni_to_byte(aTHX_ &s, strend, datumtype);
		    } else {/*369*/SensorCall(15634);bits = *(U8 *) s++;/*370*/}/*366*/}
		    SensorCall(15635);*str++ = bits & 0x80 ? '1' : '0';
		}
	    }
	    SensorCall(15638);*str = '\0';
	    SvCUR_set(sv, str - SvPVX_const(sv));
	    XPUSHs(sv);
	    SensorCall(15639);break;
	}
	case 'H':
	case 'h': {
	    SensorCall(15640);char *str = NULL;
	    /* Preliminary length estimate, acceptable for utf8 too */
	    SensorCall(15642);if (howlen == e_star || len > (strend - s) * 2)
		{/*371*/SensorCall(15641);len = (strend - s) * 2;/*372*/}
	    SensorCall(15644);if (!checksum) {
		SensorCall(15643);sv = sv_2mortal(newSV(len ? len : 1));
		SvPOK_on(sv);
		str = SvPVX(sv);
	    }
	    SensorCall(15667);if (datumtype == 'h') {
		SensorCall(15645);U8 bits = 0;
		I32 ai32 = len;
		SensorCall(15655);for (len = 0; len < ai32; len++) {
		    SensorCall(15646);if (len & 1) {/*373*/SensorCall(15647);bits >>= 4;/*374*/}
		    else {/*375*/SensorCall(15648);if (utf8) {
			SensorCall(15649);if (s >= strend) {/*377*/SensorCall(15650);break;/*378*/}
			SensorCall(15651);bits = uni_to_byte(aTHX_ &s, strend, datumtype);
		    } else {/*379*/SensorCall(15652);bits = * (U8 *) s++;/*380*/}/*376*/}
		    SensorCall(15654);if (!checksum)
			{/*381*/SensorCall(15653);*str++ = PL_hexdigit[bits & 15];/*382*/}
		}
	    } else {
		SensorCall(15656);U8 bits = 0;
		const I32 ai32 = len;
		SensorCall(15666);for (len = 0; len < ai32; len++) {
		    SensorCall(15657);if (len & 1) {/*383*/SensorCall(15658);bits <<= 4;/*384*/}
		    else {/*385*/SensorCall(15659);if (utf8) {
			SensorCall(15660);if (s >= strend) {/*387*/SensorCall(15661);break;/*388*/}
			SensorCall(15662);bits = uni_to_byte(aTHX_ &s, strend, datumtype);
		    } else {/*389*/SensorCall(15663);bits = *(U8 *) s++;/*390*/}/*386*/}
		    SensorCall(15665);if (!checksum)
			{/*391*/SensorCall(15664);*str++ = PL_hexdigit[(bits >> 4) & 15];/*392*/}
		}
	    }
	    SensorCall(15669);if (!checksum) {
		SensorCall(15668);*str = '\0';
		SvCUR_set(sv, str - SvPVX_const(sv));
		XPUSHs(sv);
	    }
	    SensorCall(15670);break;
	}
	case 'C':
            SensorCall(15671);if (len == 0) {
                SensorCall(15672);if (explicit_length)
		    /* Switch to "character" mode */
		    {/*393*/SensorCall(15673);utf8 = (symptr->flags & FLAG_DO_UTF8) ? 1 : 0;/*394*/}
		SensorCall(15674);break;
	    }
	    /* FALL THROUGH */
	case 'c':
	    SensorCall(15675);while (len-- > 0 && s < strend) {
		SensorCall(15676);int aint;
		SensorCall(15682);if (utf8)
		  {
		    SensorCall(15677);STRLEN retlen;
		    aint = utf8n_to_uvchr((U8 *) s, strend-s, &retlen,
				 ckWARN(WARN_UTF8) ? 0 : UTF8_ALLOW_ANY);
		    SensorCall(15679);if (retlen == (STRLEN) -1 || retlen == 0)
			{/*395*/SensorCall(15678);Perl_croak(aTHX_ "Malformed UTF-8 string in unpack");/*396*/}
		    SensorCall(15680);s += retlen;
		  }
		else
		  {/*397*/SensorCall(15681);aint = *(U8 *)(s)++;/*398*/}
		SensorCall(15684);if (aint >= 128 && datumtype != 'C')	/* fake up signed chars */
		    {/*399*/SensorCall(15683);aint -= 256;/*400*/}
		SensorCall(15688);if (!checksum)
		    mPUSHi(aint);
		else {/*401*/SensorCall(15685);if (checksum > bits_in_uv)
		    {/*403*/SensorCall(15686);cdouble += (NV)aint;/*404*/}
		else
		    {/*405*/SensorCall(15687);cuv += aint;/*406*/}/*402*/}
	    }
	    SensorCall(15689);break;
	case 'W':
	  W_checksum:
	    SensorCall(15690);if (utf8) {
		SensorCall(15691);while (len-- > 0 && s < strend) {
		    SensorCall(15692);STRLEN retlen;
		    const UV val = utf8n_to_uvchr((U8 *) s, strend-s, &retlen,
					 ckWARN(WARN_UTF8) ? 0 : UTF8_ALLOW_ANY);
		    SensorCall(15694);if (retlen == (STRLEN) -1 || retlen == 0)
			{/*407*/SensorCall(15693);Perl_croak(aTHX_ "Malformed UTF-8 string in unpack");/*408*/}
		    SensorCall(15695);s += retlen;
		    SensorCall(15699);if (!checksum)
			mPUSHu(val);
		    else {/*409*/SensorCall(15696);if (checksum > bits_in_uv)
			{/*411*/SensorCall(15697);cdouble += (NV) val;/*412*/}
		    else
			{/*413*/SensorCall(15698);cuv += val;/*414*/}/*410*/}
		}
	    } else {/*415*/SensorCall(15700);if (!checksum)
		{/*417*/SensorCall(15701);while (len-- > 0) {
		    SensorCall(15702);const U8 ch = *(U8 *) s++;
		    mPUSHu(ch);
	    ;/*418*/}}
	    else {/*419*/SensorCall(15703);if (checksum > bits_in_uv)
		{/*421*/SensorCall(15704);while (len-- > 0) {/*423*/SensorCall(15705);cdouble += (NV) *(U8 *) s++;/*424*/}/*422*/}
	    else
		{/*425*/SensorCall(15706);while (len-- > 0) {/*427*/SensorCall(15707);cuv += *(U8 *) s++;/*428*/}/*426*/}/*420*/}/*416*/}
	    SensorCall(15708);break;
	case 'U':
	    SensorCall(15709);if (len == 0) {
                SensorCall(15710);if (explicit_length && howlen != e_star) {
		    /* Switch to "bytes in UTF-8" mode */
		    SensorCall(15711);if (symptr->flags & FLAG_DO_UTF8) {/*429*/SensorCall(15712);utf8 = 0;/*430*/}
		    else
			/* Should be impossible due to the need_utf8() test */
			{/*431*/SensorCall(15713);Perl_croak(aTHX_ "U0 mode on a byte string");/*432*/}
		}
		SensorCall(15714);break;
	    }
	    SensorCall(15716);if (len > strend - s) {/*433*/SensorCall(15715);len = strend - s;/*434*/}
	    SensorCall(15719);if (!checksum) {
		SensorCall(15717);if (len && unpack_only_one) {/*435*/SensorCall(15718);len = 1;/*436*/}
		EXTEND(SP, len);
		EXTEND_MORTAL(len);
	    }
	    SensorCall(15737);while (len-- > 0 && s < strend) {
		SensorCall(15720);STRLEN retlen;
		UV auv;
		SensorCall(15732);if (utf8) {
		    SensorCall(15721);U8 result[UTF8_MAXLEN];
		    const char *ptr = s;
		    STRLEN len;
		    /* Bug: warns about bad utf8 even if we are short on bytes
		       and will break out of the loop */
		    SensorCall(15723);if (!uni_to_bytes(aTHX_ &ptr, strend, (char *) result, 1,
				      'U'))
			{/*437*/SensorCall(15722);break;/*438*/}
		    SensorCall(15724);len = UTF8SKIP(result);
		    SensorCall(15726);if (!uni_to_bytes(aTHX_ &ptr, strend,
				      (char *) &result[1], len-1, 'U')) {/*439*/SensorCall(15725);break;/*440*/}
		    SensorCall(15727);auv = utf8n_to_uvuni(result, len, &retlen, ckWARN(WARN_UTF8) ? 0 : UTF8_ALLOW_ANYUV);
		    s = ptr;
		} else {
		    SensorCall(15728);auv = utf8n_to_uvuni((U8*)s, strend - s, &retlen, ckWARN(WARN_UTF8) ? 0 : UTF8_ALLOW_ANYUV);
		    SensorCall(15730);if (retlen == (STRLEN) -1 || retlen == 0)
			{/*441*/SensorCall(15729);Perl_croak(aTHX_ "Malformed UTF-8 string in unpack");/*442*/}
		    SensorCall(15731);s += retlen;
		}
		SensorCall(15736);if (!checksum)
		    mPUSHu(auv);
		else {/*443*/SensorCall(15733);if (checksum > bits_in_uv)
		    {/*445*/SensorCall(15734);cdouble += (NV) auv;/*446*/}
		else
		    {/*447*/SensorCall(15735);cuv += auv;/*448*/}/*444*/}
	    }
	    SensorCall(15738);break;
	case 's' | TYPE_IS_SHRIEKING:
#if SHORTSIZE != SIZE16
	    while (len-- > 0) {
		short ashort;
		SHIFT_VAR(utf8, s, strend, ashort, datumtype);
		DO_BO_UNPACK(ashort, s);
		if (!checksum)
		    mPUSHi(ashort);
		else if (checksum > bits_in_uv)
		    cdouble += (NV)ashort;
		else
		    cuv += ashort;
	    }
	    break;
#else
	    /* Fallthrough! */
#endif
	case 's':
	    SensorCall(15739);while (len-- > 0) {
		SensorCall(15740);I16 ai16;

#if U16SIZE > SIZE16
		ai16 = 0;
#endif
		SHIFT16(utf8, s, strend, &ai16, datumtype);
		DO_BO_UNPACK(ai16, 16);
#if U16SIZE > SIZE16
		if (ai16 > 32767)
		    ai16 -= 65536;
#endif
		SensorCall(15744);if (!checksum)
		    mPUSHi(ai16);
		else {/*449*/SensorCall(15741);if (checksum > bits_in_uv)
		    {/*451*/SensorCall(15742);cdouble += (NV)ai16;/*452*/}
		else
		    {/*453*/SensorCall(15743);cuv += ai16;/*454*/}/*450*/}
	    }
	    SensorCall(15745);break;
	case 'S' | TYPE_IS_SHRIEKING:
#if SHORTSIZE != SIZE16
	    while (len-- > 0) {
		unsigned short aushort;
		SHIFT_VAR(utf8, s, strend, aushort, datumtype);
		DO_BO_UNPACK(aushort, s);
		if (!checksum)
		    mPUSHu(aushort);
		else if (checksum > bits_in_uv)
		    cdouble += (NV)aushort;
		else
		    cuv += aushort;
	    }
	    break;
#else
            /* Fallthrough! */
#endif
	case 'v':
	case 'n':
	case 'S':
	    SensorCall(15746);while (len-- > 0) {
		SensorCall(15747);U16 au16;
#if U16SIZE > SIZE16
		au16 = 0;
#endif
		SHIFT16(utf8, s, strend, &au16, datumtype);
		DO_BO_UNPACK(au16, 16);
#ifdef HAS_NTOHS
		SensorCall(15748);if (datumtype == 'n')
		    au16 = PerlSock_ntohs(au16);
#endif
#ifdef HAS_VTOHS
		SensorCall(15750);if (datumtype == 'v')
		    {/*455*/SensorCall(15749);au16 = vtohs(au16);/*456*/}
#endif
		SensorCall(15754);if (!checksum)
		    mPUSHu(au16);
		else {/*457*/SensorCall(15751);if (checksum > bits_in_uv)
		    {/*459*/SensorCall(15752);cdouble += (NV) au16;/*460*/}
		else
		    {/*461*/SensorCall(15753);cuv += au16;/*462*/}/*458*/}
	    }
	    SensorCall(15755);break;
#ifdef PERL_PACK_CAN_SHRIEKSIGN
	case 'v' | TYPE_IS_SHRIEKING:
	case 'n' | TYPE_IS_SHRIEKING:
	    SensorCall(15756);while (len-- > 0) {
		SensorCall(15757);I16 ai16;
# if U16SIZE > SIZE16
		ai16 = 0;
# endif
		SHIFT16(utf8, s, strend, &ai16, datumtype);
# ifdef HAS_NTOHS
		SensorCall(15758);if (datumtype == ('n' | TYPE_IS_SHRIEKING))
		    ai16 = (I16) PerlSock_ntohs((U16) ai16);
# endif /* HAS_NTOHS */
# ifdef HAS_VTOHS
		SensorCall(15760);if (datumtype == ('v' | TYPE_IS_SHRIEKING))
		    {/*463*/SensorCall(15759);ai16 = (I16) vtohs((U16) ai16);/*464*/}
# endif /* HAS_VTOHS */
		SensorCall(15764);if (!checksum)
		    mPUSHi(ai16);
		else {/*465*/SensorCall(15761);if (checksum > bits_in_uv)
		    {/*467*/SensorCall(15762);cdouble += (NV) ai16;/*468*/}
		else
		    {/*469*/SensorCall(15763);cuv += ai16;/*470*/}/*466*/}
	    }
	    SensorCall(15765);break;
#endif /* PERL_PACK_CAN_SHRIEKSIGN */
	case 'i':
	case 'i' | TYPE_IS_SHRIEKING:
	    SensorCall(15766);while (len-- > 0) {
		SensorCall(15767);int aint;
		SHIFT_VAR(utf8, s, strend, aint, datumtype);
		DO_BO_UNPACK(aint, i);
		SensorCall(15771);if (!checksum)
		    mPUSHi(aint);
		else {/*471*/SensorCall(15768);if (checksum > bits_in_uv)
		    {/*473*/SensorCall(15769);cdouble += (NV)aint;/*474*/}
		else
		    {/*475*/SensorCall(15770);cuv += aint;/*476*/}/*472*/}
	    }
	    SensorCall(15772);break;
	case 'I':
	case 'I' | TYPE_IS_SHRIEKING:
	    SensorCall(15773);while (len-- > 0) {
		SensorCall(15774);unsigned int auint;
		SHIFT_VAR(utf8, s, strend, auint, datumtype);
		DO_BO_UNPACK(auint, i);
		SensorCall(15778);if (!checksum)
		    mPUSHu(auint);
		else {/*477*/SensorCall(15775);if (checksum > bits_in_uv)
		    {/*479*/SensorCall(15776);cdouble += (NV)auint;/*480*/}
		else
		    {/*481*/SensorCall(15777);cuv += auint;/*482*/}/*478*/}
	    }
	    SensorCall(15779);break;
	case 'j':
	    SensorCall(15780);while (len-- > 0) {
		SensorCall(15781);IV aiv;
		SHIFT_VAR(utf8, s, strend, aiv, datumtype);
#if IVSIZE == INTSIZE
		DO_BO_UNPACK(aiv, i);
#elif IVSIZE == LONGSIZE
		DO_BO_UNPACK(aiv, l);
#elif defined(HAS_QUAD) && IVSIZE == U64SIZE
		DO_BO_UNPACK(aiv, 64);
#else
		Perl_croak(aTHX_ "'j' not supported on this platform");
#endif
		SensorCall(15785);if (!checksum)
		    mPUSHi(aiv);
		else {/*483*/SensorCall(15782);if (checksum > bits_in_uv)
		    {/*485*/SensorCall(15783);cdouble += (NV)aiv;/*486*/}
		else
		    {/*487*/SensorCall(15784);cuv += aiv;/*488*/}/*484*/}
	    }
	    SensorCall(15786);break;
	case 'J':
	    SensorCall(15787);while (len-- > 0) {
		SensorCall(15788);UV auv;
		SHIFT_VAR(utf8, s, strend, auv, datumtype);
#if IVSIZE == INTSIZE
		DO_BO_UNPACK(auv, i);
#elif IVSIZE == LONGSIZE
		DO_BO_UNPACK(auv, l);
#elif defined(HAS_QUAD) && IVSIZE == U64SIZE
		DO_BO_UNPACK(auv, 64);
#else
		Perl_croak(aTHX_ "'J' not supported on this platform");
#endif
		SensorCall(15792);if (!checksum)
		    mPUSHu(auv);
		else {/*489*/SensorCall(15789);if (checksum > bits_in_uv)
		    {/*491*/SensorCall(15790);cdouble += (NV)auv;/*492*/}
		else
		    {/*493*/SensorCall(15791);cuv += auv;/*494*/}/*490*/}
	    }
	    SensorCall(15793);break;
	case 'l' | TYPE_IS_SHRIEKING:
#if LONGSIZE != SIZE32
	    SensorCall(15794);while (len-- > 0) {
		SensorCall(15795);long along;
		SHIFT_VAR(utf8, s, strend, along, datumtype);
		DO_BO_UNPACK(along, l);
		SensorCall(15799);if (!checksum)
		    mPUSHi(along);
		else {/*495*/SensorCall(15796);if (checksum > bits_in_uv)
		    {/*497*/SensorCall(15797);cdouble += (NV)along;/*498*/}
		else
		    {/*499*/SensorCall(15798);cuv += along;/*500*/}/*496*/}
	    }
	    SensorCall(15800);break;
#else
	    /* Fallthrough! */
#endif
	case 'l':
	    SensorCall(15801);while (len-- > 0) {
		SensorCall(15802);I32 ai32;
#if U32SIZE > SIZE32
		ai32 = 0;
#endif
		SHIFT32(utf8, s, strend, &ai32, datumtype);
		DO_BO_UNPACK(ai32, 32);
#if U32SIZE > SIZE32
		if (ai32 > 2147483647) ai32 -= 4294967296;
#endif
		SensorCall(15806);if (!checksum)
		    mPUSHi(ai32);
		else {/*501*/SensorCall(15803);if (checksum > bits_in_uv)
		    {/*503*/SensorCall(15804);cdouble += (NV)ai32;/*504*/}
		else
		    {/*505*/SensorCall(15805);cuv += ai32;/*506*/}/*502*/}
	    }
	    SensorCall(15807);break;
	case 'L' | TYPE_IS_SHRIEKING:
#if LONGSIZE != SIZE32
	    SensorCall(15808);while (len-- > 0) {
		SensorCall(15809);unsigned long aulong;
		SHIFT_VAR(utf8, s, strend, aulong, datumtype);
		DO_BO_UNPACK(aulong, l);
		SensorCall(15813);if (!checksum)
		    mPUSHu(aulong);
		else {/*507*/SensorCall(15810);if (checksum > bits_in_uv)
		    {/*509*/SensorCall(15811);cdouble += (NV)aulong;/*510*/}
		else
		    {/*511*/SensorCall(15812);cuv += aulong;/*512*/}/*508*/}
	    }
	    SensorCall(15814);break;
#else
            /* Fall through! */
#endif
	case 'V':
	case 'N':
	case 'L':
	    SensorCall(15815);while (len-- > 0) {
		SensorCall(15816);U32 au32;
#if U32SIZE > SIZE32
		au32 = 0;
#endif
		SHIFT32(utf8, s, strend, &au32, datumtype);
		DO_BO_UNPACK(au32, 32);
#ifdef HAS_NTOHL
		SensorCall(15817);if (datumtype == 'N')
		    au32 = PerlSock_ntohl(au32);
#endif
#ifdef HAS_VTOHL
		SensorCall(15819);if (datumtype == 'V')
		    {/*513*/SensorCall(15818);au32 = vtohl(au32);/*514*/}
#endif
		SensorCall(15823);if (!checksum)
		    mPUSHu(au32);
		else {/*515*/SensorCall(15820);if (checksum > bits_in_uv)
		    {/*517*/SensorCall(15821);cdouble += (NV)au32;/*518*/}
		else
		    {/*519*/SensorCall(15822);cuv += au32;/*520*/}/*516*/}
	    }
	    SensorCall(15824);break;
#ifdef PERL_PACK_CAN_SHRIEKSIGN
	case 'V' | TYPE_IS_SHRIEKING:
	case 'N' | TYPE_IS_SHRIEKING:
	    SensorCall(15825);while (len-- > 0) {
		SensorCall(15826);I32 ai32;
# if U32SIZE > SIZE32
		ai32 = 0;
# endif
		SHIFT32(utf8, s, strend, &ai32, datumtype);
# ifdef HAS_NTOHL
		SensorCall(15827);if (datumtype == ('N' | TYPE_IS_SHRIEKING))
		    ai32 = (I32)PerlSock_ntohl((U32)ai32);
# endif
# ifdef HAS_VTOHL
		SensorCall(15829);if (datumtype == ('V' | TYPE_IS_SHRIEKING))
		    {/*521*/SensorCall(15828);ai32 = (I32)vtohl((U32)ai32);/*522*/}
# endif
		SensorCall(15833);if (!checksum)
		    mPUSHi(ai32);
		else {/*523*/SensorCall(15830);if (checksum > bits_in_uv)
		    {/*525*/SensorCall(15831);cdouble += (NV)ai32;/*526*/}
		else
		    {/*527*/SensorCall(15832);cuv += ai32;/*528*/}/*524*/}
	    }
	    SensorCall(15834);break;
#endif /* PERL_PACK_CAN_SHRIEKSIGN */
	case 'p':
	    SensorCall(15835);while (len-- > 0) {
		SensorCall(15836);const char *aptr;
		SHIFT_VAR(utf8, s, strend, aptr, datumtype);
		DO_BO_UNPACK_PC(aptr);
		/* newSVpv generates undef if aptr is NULL */
		mPUSHs(newSVpv(aptr, 0));
	    }
	    SensorCall(15837);break;
	case 'w':
	    {
		SensorCall(15838);UV auv = 0;
		U32 bytes = 0;

		SensorCall(15854);while (len > 0 && s < strend) {
		    SensorCall(15839);U8 ch;
		    ch = SHIFT_BYTE(utf8, s, strend, datumtype);
		    auv = (auv << 7) | (ch & 0x7f);
		    /* UTF8_IS_XXXXX not right here - using constant 0x80 */
		    SensorCall(15842);if (ch < 0x80) {
			SensorCall(15840);bytes = 0;
			mPUSHu(auv);
			len--;
			auv = 0;
			SensorCall(15841);continue;
		    }
		    SensorCall(15853);if (++bytes >= sizeof(UV)) {	/* promote to string */
			SensorCall(15843);const char *t;

			sv = Perl_newSVpvf(aTHX_ "%.*"UVuf, (int)TYPE_DIGITS(UV), auv);
			SensorCall(15848);while (s < strend) {
			    SensorCall(15844);ch = SHIFT_BYTE(utf8, s, strend, datumtype);
			    sv = mul128(sv, (U8)(ch & 0x7f));
			    SensorCall(15847);if (!(ch & 0x80)) {
				SensorCall(15845);bytes = 0;
				SensorCall(15846);break;
			    }
			}
			SensorCall(15849);t = SvPV_nolen_const(sv);
			SensorCall(15851);while (*t == '0')
			    {/*529*/SensorCall(15850);t++;/*530*/}
			sv_chop(sv, t);
			mPUSHs(sv);
			SensorCall(15852);len--;
			auv = 0;
		    }
		}
		SensorCall(15856);if ((s >= strend) && bytes)
		    {/*531*/SensorCall(15855);Perl_croak(aTHX_ "Unterminated compressed integer in unpack");/*532*/}
	    }
	    SensorCall(15857);break;
	case 'P':
	    SensorCall(15858);if (symptr->howlen == e_star)
	        {/*533*/SensorCall(15859);Perl_croak(aTHX_ "'P' must have an explicit size in unpack");/*534*/}
	    EXTEND(SP, 1);
	    SensorCall(15861);if (s + sizeof(char*) <= strend) {
		SensorCall(15860);char *aptr;
		SHIFT_VAR(utf8, s, strend, aptr, datumtype);
		DO_BO_UNPACK_PC(aptr);
		/* newSVpvn generates undef if aptr is NULL */
		PUSHs(newSVpvn_flags(aptr, len, SVs_TEMP));
	    }
	    SensorCall(15862);break;
#ifdef HAS_QUAD
	case 'q':
	    SensorCall(15863);while (len-- > 0) {
		Quad_t aquad;
		SHIFT_VAR(utf8, s, strend, aquad, datumtype);
		DO_BO_UNPACK(aquad, 64);
		SensorCall(15867);if (!checksum)
                    mPUSHs(aquad >= IV_MIN && aquad <= IV_MAX ?
			   newSViv((IV)aquad) : newSVnv((NV)aquad));
		else {/*535*/SensorCall(15864);if (checksum > bits_in_uv)
		    {/*537*/SensorCall(15865);cdouble += (NV)aquad;/*538*/}
		else
		    {/*539*/SensorCall(15866);cuv += aquad;/*540*/}/*536*/}
	    }
	    SensorCall(15868);break;
	case 'Q':
	    SensorCall(15869);while (len-- > 0) {
		Uquad_t auquad;
		SHIFT_VAR(utf8, s, strend, auquad, datumtype);
		DO_BO_UNPACK(auquad, 64);
		SensorCall(15873);if (!checksum)
		    mPUSHs(auquad <= UV_MAX ?
			   newSVuv((UV)auquad) : newSVnv((NV)auquad));
		else {/*541*/SensorCall(15870);if (checksum > bits_in_uv)
		    {/*543*/SensorCall(15871);cdouble += (NV)auquad;/*544*/}
		else
		    {/*545*/SensorCall(15872);cuv += auquad;/*546*/}/*542*/}
	    }
	    SensorCall(15874);break;
#endif /* HAS_QUAD */
	/* float and double added gnb@melba.bby.oz.au 22/11/89 */
	case 'f':
	    SensorCall(15875);while (len-- > 0) {
		SensorCall(15876);float afloat;
		SHIFT_VAR(utf8, s, strend, afloat, datumtype);
		DO_BO_UNPACK_N(afloat, float);
		SensorCall(15878);if (!checksum)
		    mPUSHn(afloat);
		else
		    {/*547*/SensorCall(15877);cdouble += afloat;/*548*/}
	    }
	    SensorCall(15879);break;
	case 'd':
	    SensorCall(15880);while (len-- > 0) {
		SensorCall(15881);double adouble;
		SHIFT_VAR(utf8, s, strend, adouble, datumtype);
		DO_BO_UNPACK_N(adouble, double);
		SensorCall(15883);if (!checksum)
		    mPUSHn(adouble);
		else
		    {/*549*/SensorCall(15882);cdouble += adouble;/*550*/}
	    }
	    SensorCall(15884);break;
	case 'F':
	    SensorCall(15885);while (len-- > 0) {
		SensorCall(15886);NV_bytes anv;
		SHIFT_BYTES(utf8, s, strend, anv.bytes, sizeof(anv.bytes), datumtype);
		DO_BO_UNPACK_N(anv.nv, NV);
		SensorCall(15888);if (!checksum)
		    mPUSHn(anv.nv);
		else
		    {/*551*/SensorCall(15887);cdouble += anv.nv;/*552*/}
	    }
	    SensorCall(15889);break;
#if defined(HAS_LONG_DOUBLE) && defined(USE_LONG_DOUBLE)
	case 'D':
	    while (len-- > 0) {
		ld_bytes aldouble;
		SHIFT_BYTES(utf8, s, strend, aldouble.bytes, sizeof(aldouble.bytes), datumtype);
		DO_BO_UNPACK_N(aldouble.ld, long double);
		if (!checksum)
		    mPUSHn(aldouble.ld);
		else
		    cdouble += aldouble.ld;
	    }
	    break;
#endif
	case 'u':
	    SensorCall(15890);if (!checksum) {
                SensorCall(15891);const STRLEN l = (STRLEN) (strend - s) * 3 / 4;
		sv = sv_2mortal(newSV(l));
		SensorCall(15892);if (l) SvPOK_on(sv);
	    }
	    SensorCall(15927);if (utf8) {
		SensorCall(15893);while (next_uni_uu(aTHX_ &s, strend, &len)) {
		    SensorCall(15894);I32 a, b, c, d;
		    char hunk[3];

		    SensorCall(15898);while (len > 0) {
			SensorCall(15895);next_uni_uu(aTHX_ &s, strend, &a);
			next_uni_uu(aTHX_ &s, strend, &b);
			next_uni_uu(aTHX_ &s, strend, &c);
			next_uni_uu(aTHX_ &s, strend, &d);
			hunk[0] = (char)((a << 2) | (b >> 4));
			hunk[1] = (char)((b << 4) | (c >> 2));
			hunk[2] = (char)((c << 6) | d);
			SensorCall(15896);if (!checksum)
			    sv_catpvn(sv, hunk, (len > 3) ? 3 : len);
			SensorCall(15897);len -= 3;
		    }
		    SensorCall(15904);if (s < strend) {
			SensorCall(15899);if (*s == '\n') {
                            SensorCall(15900);s++;
                        }
			else {
			    /* possible checksum byte */
			    SensorCall(15901);const char *skip = s+UTF8SKIP(s);
			    SensorCall(15903);if (skip < strend && *skip == '\n')
                                {/*553*/SensorCall(15902);s = skip+1;/*554*/}
			}
		    }
		}
	    } else {
		SensorCall(15905);while (s < strend && *s > ' ' && ISUUCHAR(*s)) {
		    SensorCall(15906);I32 a, b, c, d;
		    char hunk[3];

		    len = PL_uudmap[*(U8*)s++] & 077;
		    SensorCall(15922);while (len > 0) {
			SensorCall(15907);if (s < strend && ISUUCHAR(*s))
			    {/*555*/SensorCall(15908);a = PL_uudmap[*(U8*)s++] & 077;/*556*/}
			else
			    {/*557*/SensorCall(15909);a = 0;/*558*/}
			SensorCall(15912);if (s < strend && ISUUCHAR(*s))
			    {/*559*/SensorCall(15910);b = PL_uudmap[*(U8*)s++] & 077;/*560*/}
			else
			    {/*561*/SensorCall(15911);b = 0;/*562*/}
			SensorCall(15915);if (s < strend && ISUUCHAR(*s))
			    {/*563*/SensorCall(15913);c = PL_uudmap[*(U8*)s++] & 077;/*564*/}
			else
			    {/*565*/SensorCall(15914);c = 0;/*566*/}
			SensorCall(15918);if (s < strend && ISUUCHAR(*s))
			    {/*567*/SensorCall(15916);d = PL_uudmap[*(U8*)s++] & 077;/*568*/}
			else
			    {/*569*/SensorCall(15917);d = 0;/*570*/}
			SensorCall(15919);hunk[0] = (char)((a << 2) | (b >> 4));
			hunk[1] = (char)((b << 4) | (c >> 2));
			hunk[2] = (char)((c << 6) | d);
			SensorCall(15920);if (!checksum)
			    sv_catpvn(sv, hunk, (len > 3) ? 3 : len);
			SensorCall(15921);len -= 3;
		    }
		    SensorCall(15926);if (*s == '\n')
			{/*571*/SensorCall(15923);s++;/*572*/}
		    else	/* possible checksum byte */
			{/*573*/SensorCall(15924);if (s + 1 < strend && s[1] == '\n')
			    {/*575*/SensorCall(15925);s += 2;/*576*/}/*574*/}
		}
	    }
	    SensorCall(15928);if (!checksum)
		XPUSHs(sv);
	    SensorCall(15929);break;
	}

	SensorCall(15942);if (checksum) {
	    SensorCall(15931);if (strchr("fFdD", TYPE_NO_MODIFIERS(datumtype)) ||
	      (checksum > bits_in_uv &&
	       strchr("cCsSiIlLnNUWvVqQjJ", TYPE_NO_MODIFIERS(datumtype))) ) {
		SensorCall(15932);NV trouble, anv;

                anv = (NV) (1 << (checksum & 15));
		SensorCall(15934);while (checksum >= 16) {
		    SensorCall(15933);checksum -= 16;
		    anv *= 65536.0;
		}
		SensorCall(15936);while (cdouble < 0.0)
		    {/*577*/SensorCall(15935);cdouble += anv;/*578*/}
		SensorCall(15937);cdouble = Perl_modf(cdouble / anv, &trouble) * anv;
		sv = newSVnv(cdouble);
	    }
	    else {
		SensorCall(15938);if (checksum < bits_in_uv) {
		    SensorCall(15939);UV mask = ((UV)1 << checksum) - 1;
		    cuv &= mask;
		}
		SensorCall(15940);sv = newSVuv(cuv);
	    }
	    mXPUSHs(sv);
	    SensorCall(15941);checksum = 0;
	}

        SensorCall(15956);if (symptr->flags & FLAG_SLASH){
            SensorCall(15943);if (SP - PL_stack_base - start_sp_offset <= 0)
                {/*579*/SensorCall(15944);Perl_croak(aTHX_ "'/' must follow a numeric type in unpack");/*580*/}
            SensorCall(15953);if( next_symbol(symptr) ){
              SensorCall(15945);if( symptr->howlen == e_number )
		{/*581*/SensorCall(15946);Perl_croak(aTHX_ "Count after length/code in unpack" );/*582*/}
              SensorCall(15951);if( beyond ){
         	/* ...end of char buffer then no decent length available */
		SensorCall(15947);Perl_croak(aTHX_ "length/code after end of string in unpack" );
              } else {
         	/* take top of stack (hope it's numeric) */
                SensorCall(15948);len = POPi;
                SensorCall(15950);if( len < 0 )
                    {/*583*/SensorCall(15949);Perl_croak(aTHX_ "Negative '/' count in unpack" );/*584*/}
              }
            } else {
		SensorCall(15952);Perl_croak(aTHX_ "Code missing after '/' in unpack" );
            }
            SensorCall(15954);datumtype = symptr->code;
            explicit_length = FALSE;
	    SensorCall(15955);goto redo_switch;
        }
    }

    SensorCall(15959);if (new_s)
	{/*585*/SensorCall(15958);*new_s = s;/*586*/}
    PUTBACK;
    {I32  ReplaceReturn1582 = SP - PL_stack_base - start_sp_offset; SensorCall(15960); return ReplaceReturn1582;}
}

PP(pp_unpack)
{
SensorCall(15961);    dVAR;
    dSP;
    dPOPPOPssrl;
    I32 gimme = GIMME_V;
    STRLEN llen;
    STRLEN rlen;
    const char *pat = SvPV_const(left,  llen);
    const char *s   = SvPV_const(right, rlen);
    const char *strend = s + rlen;
    const char *patend = pat + llen;
    I32 cnt;

    PUTBACK;
    cnt = unpackstring(pat, patend, s, strend,
		     ((gimme == G_SCALAR) ? FLAG_UNPACK_ONLY_ONE : 0)
		     | (DO_UTF8(right) ? FLAG_DO_UTF8 : 0));

    SPAGAIN;
    SensorCall(15962);if ( !cnt && gimme == G_SCALAR )
       PUSHs(&PL_sv_undef);
    RETURN;
}

STATIC U8 *
doencodes(U8 *h, const char *s, I32 len)
{
    SensorCall(15963);*h++ = PL_uuemap[len];
    SensorCall(15965);while (len > 2) {
	SensorCall(15964);*h++ = PL_uuemap[(077 & (s[0] >> 2))];
	*h++ = PL_uuemap[(077 & (((s[0] << 4) & 060) | ((s[1] >> 4) & 017)))];
	*h++ = PL_uuemap[(077 & (((s[1] << 2) & 074) | ((s[2] >> 6) & 03)))];
	*h++ = PL_uuemap[(077 & (s[2] & 077))];
	s += 3;
	len -= 3;
    }
    SensorCall(15967);if (len > 0) {
        SensorCall(15966);const char r = (len > 1 ? s[1] : '\0');
	*h++ = PL_uuemap[(077 & (s[0] >> 2))];
	*h++ = PL_uuemap[(077 & (((s[0] << 4) & 060) | ((r >> 4) & 017)))];
	*h++ = PL_uuemap[(077 & ((r << 2) & 074))];
	*h++ = PL_uuemap[0];
    }
    SensorCall(15968);*h++ = '\n';
    {U8 * ReplaceReturn1581 = h; SensorCall(15969); return ReplaceReturn1581;}
}

STATIC SV *
S_is_an_int(pTHX_ const char *s, STRLEN l)
{
  SensorCall(15970);SV *result = newSVpvn(s, l);
  char *const result_c = SvPV_nolen(result);	/* convenience */
  char *out = result_c;
  bool skip = 1;
  bool ignore = 0;

  PERL_ARGS_ASSERT_IS_AN_INT;

  SensorCall(15984);while (*s) {
    SensorCall(15971);switch (*s) {
    case ' ':
      SensorCall(15972);break;
    case '+':
      SensorCall(15973);if (!skip) {
	SvREFCNT_dec(result);
	{SV * ReplaceReturn1580 = (NULL); SensorCall(15974); return ReplaceReturn1580;}
      }
      SensorCall(15975);break;
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      SensorCall(15976);skip = 0;
      SensorCall(15978);if (!ignore) {
	SensorCall(15977);*(out++) = *s;
      }
      SensorCall(15979);break;
    case '.':
      SensorCall(15980);ignore = 1;
      SensorCall(15981);break;
    default:
      SvREFCNT_dec(result);
      {SV * ReplaceReturn1579 = (NULL); SensorCall(15982); return ReplaceReturn1579;}
    }
    SensorCall(15983);s++;
  }
  SensorCall(15985);*(out++) = '\0';
  SvCUR_set(result, out - result_c);
  {SV * ReplaceReturn1578 = (result); SensorCall(15986); return ReplaceReturn1578;}
}

/* pnum must be '\0' terminated */
STATIC int
S_div128(pTHX_ SV *pnum, bool *done)
{
    SensorCall(15987);STRLEN len;
    char * const s = SvPV(pnum, len);
    char *t = s;
    int m = 0;

    PERL_ARGS_ASSERT_DIV128;

    *done = 1;
    SensorCall(15992);while (*t) {
	SensorCall(15988);const int i = m * 10 + (*t - '0');
	const int r = (i >> 7); /* r < 10 */
	m = i & 0x7F;
	SensorCall(15990);if (r) {
	    SensorCall(15989);*done = 0;
	}
	SensorCall(15991);*(t++) = '0' + r;
    }
    SensorCall(15993);*(t++) = '\0';
    SvCUR_set(pnum, (STRLEN) (t - s));
    {int  ReplaceReturn1577 = (m); SensorCall(15994); return ReplaceReturn1577;}
}

/*
=for apidoc packlist

The engine implementing pack() Perl function.

=cut
*/

void
Perl_packlist(pTHX_ SV *cat, const char *pat, const char *patend, register SV **beglist, SV **endlist )
{
SensorCall(15995);    dVAR;
    tempsym_t sym;

    PERL_ARGS_ASSERT_PACKLIST;

    TEMPSYM_INIT(&sym, pat, patend, FLAG_PACK);

    /* We're going to do changes through SvPVX(cat). Make sure it's valid.
       Also make sure any UTF8 flag is loaded */
    SvPV_force_nolen(cat);
    if (DO_UTF8(cat))
	sym.flags |= FLAG_PARSE_UTF8 | FLAG_DO_UTF8;

    SensorCall(15996);(void)pack_rec( cat, &sym, beglist, endlist );
SensorCall(15997);}

/* like sv_utf8_upgrade, but also repoint the group start markers */
STATIC void
marked_upgrade(pTHX_ SV *sv, tempsym_t *sym_ptr) {
    SensorCall(15998);STRLEN len;
    tempsym_t *group;
    const char *from_ptr, *from_start, *from_end, **marks, **m;
    char *to_start, *to_ptr;

    SensorCall(16000);if (SvUTF8(sv)) {/*597*/SensorCall(15999);return;/*598*/}

    SensorCall(16001);from_start = SvPVX_const(sv);
    from_end = from_start + SvCUR(sv);
    SensorCall(16004);for (from_ptr = from_start; from_ptr < from_end; from_ptr++)
	{/*599*/SensorCall(16002);if (!NATIVE_IS_INVARIANT(*from_ptr)) {/*601*/SensorCall(16003);break;/*602*/}/*600*/}
    SensorCall(16006);if (from_ptr == from_end) {
	/* Simple case: no character needs to be changed */
	SvUTF8_on(sv);
	SensorCall(16005);return;
    }

    SensorCall(16007);len = (from_end-from_ptr)*UTF8_EXPAND+(from_ptr-from_start)+1;
    Newx(to_start, len, char);
    Copy(from_start, to_start, from_ptr-from_start, char);
    to_ptr = to_start + (from_ptr-from_start);

    Newx(marks, sym_ptr->level+2, const char *);
    SensorCall(16009);for (group=sym_ptr; group; group = group->previous)
	{/*603*/SensorCall(16008);marks[group->level] = from_start + group->strbeg;/*604*/}
    SensorCall(16010);marks[sym_ptr->level+1] = from_end+1;
    SensorCall(16012);for (m = marks; *m < from_ptr; m++)
	{/*605*/SensorCall(16011);*m = to_start + (*m-from_start);/*606*/}

    SensorCall(16016);for (;from_ptr < from_end; from_ptr++) {
	SensorCall(16013);while (*m == from_ptr) {/*607*/SensorCall(16014);*m++ = to_ptr;/*608*/}
	SensorCall(16015);to_ptr = (char *) uvchr_to_utf8((U8 *) to_ptr, *(U8 *) from_ptr);
    }
    SensorCall(16017);*to_ptr = 0;

    SensorCall(16019);while (*m == from_ptr) {/*609*/SensorCall(16018);*m++ = to_ptr;/*610*/}
    SensorCall(16021);if (m != marks + sym_ptr->level+1) {
	Safefree(marks);
	Safefree(to_start);
	SensorCall(16020);Perl_croak(aTHX_ "panic: marks beyond string end, m=%p, marks=%p, "
		   "level=%d", m, marks, sym_ptr->level);
    }
    SensorCall(16023);for (group=sym_ptr; group; group = group->previous)
	{/*611*/SensorCall(16022);group->strbeg = marks[group->level] - to_start;/*612*/}
    Safefree(marks);

    SensorCall(16026);if (SvOOK(sv)) {
	SensorCall(16024);if (SvIVX(sv)) {
	    SvLEN_set(sv, SvLEN(sv) + SvIVX(sv));
	    SensorCall(16025);from_start -= SvIVX(sv);
	    SvIV_set(sv, 0);
	}
	SvFLAGS(sv) &= ~SVf_OOK;
    }
    SensorCall(16027);if (SvLEN(sv) != 0)
	Safefree(from_start);
    SvPV_set(sv, to_start);
    SvCUR_set(sv, to_ptr - to_start);
    SvLEN_set(sv, len);
    SvUTF8_on(sv);
}

/* Exponential string grower. Makes string extension effectively O(n)
   needed says how many extra bytes we need (not counting the final '\0')
   Only grows the string if there is an actual lack of space
*/
STATIC char *
S_sv_exp_grow(pTHX_ SV *sv, STRLEN needed) {
    SensorCall(16028);const STRLEN cur = SvCUR(sv);
    const STRLEN len = SvLEN(sv);
    STRLEN extend;

    PERL_ARGS_ASSERT_SV_EXP_GROW;

    SensorCall(16029);if (len - cur > needed) return SvPVX(sv);
    SensorCall(16030);extend = needed > len ? needed : len;
    {char * ReplaceReturn1576 = SvGROW(sv, len+extend+1); SensorCall(16031); return ReplaceReturn1576;}
}

STATIC
SV **
S_pack_rec(pTHX_ SV *cat, tempsym_t* symptr, SV **beglist, SV **endlist )
{
SensorCall(16032);    dVAR;
    tempsym_t lookahead;
    I32 items  = endlist - beglist;
    bool found = next_symbol(symptr);
    bool utf8 = (symptr->flags & FLAG_PARSE_UTF8) ? 1 : 0;
    bool warn_utf8 = ckWARN(WARN_UTF8);

    PERL_ARGS_ASSERT_PACK_REC;

    SensorCall(16034);if (symptr->level == 0 && found && symptr->code == 'U') {
	SensorCall(16033);marked_upgrade(aTHX_ cat, symptr);
	symptr->flags |= FLAG_DO_UTF8;
	utf8 = 0;
    }
    SensorCall(16035);symptr->strbeg = SvCUR(cat);

    SensorCall(16458);while (found) {
	SensorCall(16036);SV *fromstr;
	STRLEN fromlen;
	I32 len;
	SV *lengthcode = NULL;
        I32 datumtype = symptr->code;
        howlen_t howlen = symptr->howlen;
	char *start = SvPVX(cat);
	char *cur   = start + SvCUR(cat);

#define NEXTFROM (lengthcode ? lengthcode : items-- > 0 ? *beglist++ : &PL_sv_no)

        SensorCall(16041);switch (howlen) {
	  case e_star:
	    SensorCall(16037);len = strchr("@Xxu", TYPE_NO_MODIFIERS(datumtype)) ?
		0 : items;
	    SensorCall(16038);break;
	  default:
	    /* e_no_len and e_number */
	    SensorCall(16039);len = symptr->length;
	    SensorCall(16040);break;
        }

	SensorCall(16045);if (len) {
	    SensorCall(16042);packprops_t props = packprops[TYPE_NO_ENDIANNESS(datumtype)];

	    SensorCall(16044);if (props && !(props & PACK_SIZE_UNPREDICTABLE)) {
		/* We can process this letter. */
		SensorCall(16043);STRLEN size = props & PACK_SIZE_MASK;
		GROWING(utf8, cat, start, cur, (STRLEN) len * size);
	    }
        }

        /* Look ahead for next symbol. Do we have code/code? */
        SensorCall(16046);lookahead = *symptr;
        found = next_symbol(&lookahead);
	SensorCall(16064);if (symptr->flags & FLAG_SLASH) {
	    SensorCall(16047);IV count;
	    SensorCall(16049);if (!found) {/*85*/SensorCall(16048);Perl_croak(aTHX_ "Code missing after '/' in pack");/*86*/}
	    SensorCall(16062);if (strchr("aAZ", lookahead.code)) {
		SensorCall(16050);if (lookahead.howlen == e_number) {/*87*/SensorCall(16051);count = lookahead.length;/*88*/}
		else {
		    SensorCall(16052);if (items > 0) {
			SensorCall(16053);if (SvGAMAGIC(*beglist)) {
			    /* Avoid reading the active data more than once
			       by copying it to a temporary.  */
			    SensorCall(16054);STRLEN len;
			    const char *const pv = SvPV_const(*beglist, len);
			    SV *const temp
				= newSVpvn_flags(pv, len,
						 SVs_TEMP | SvUTF8(*beglist));
			    *beglist = temp;
			}
			SensorCall(16055);count = DO_UTF8(*beglist) ?
			    sv_len_utf8(*beglist) : sv_len(*beglist);
		    }
		    else {/*89*/SensorCall(16056);count = 0;/*90*/}
		    SensorCall(16058);if (lookahead.code == 'Z') {/*91*/SensorCall(16057);count++;/*92*/}
		}
	    } else {
		SensorCall(16059);if (lookahead.howlen == e_number && lookahead.length < items)
		    {/*93*/SensorCall(16060);count = lookahead.length;/*94*/}
		else {/*95*/SensorCall(16061);count = items;/*96*/}
	    }
	    SensorCall(16063);lookahead.howlen = e_number;
	    lookahead.length = count;
	    lengthcode = sv_2mortal(newSViv(count));
	}

	/* Code inside the switch must take care to properly update
	   cat (CUR length and '\0' termination) if it updated *cur and
	   doesn't simply leave using break */
	SensorCall(16456);switch(TYPE_NO_ENDIANNESS(datumtype)) {
	default:
	    SensorCall(16065);Perl_croak(aTHX_ "Invalid type '%c' in pack",
		       (int) TYPE_NO_MODIFIERS(datumtype));
	case '%':
	    SensorCall(16066);Perl_croak(aTHX_ "'%%' may not be used in pack");
	{
	    char *from;
#ifdef PERL_PACK_CAN_SHRIEKSIGN
	case '.' | TYPE_IS_SHRIEKING:
#endif
	case '.':
	    SensorCall(16074);if (howlen == e_star) {/*97*/SensorCall(16067);from = start;/*98*/}
	    else {/*99*/SensorCall(16068);if (len == 0) {/*101*/SensorCall(16069);from = cur;/*102*/}
	    else {
		SensorCall(16070);tempsym_t *group = symptr;

		SensorCall(16072);while (--len && group) {/*103*/SensorCall(16071);group = group->previous;/*104*/}
		SensorCall(16073);from = group ? start + group->strbeg : start;
	    ;/*100*/}}
	    SensorCall(16075);fromstr = NEXTFROM;
	    len = SvIV(fromstr);
	    SensorCall(16076);goto resize;
#ifdef PERL_PACK_CAN_SHRIEKSIGN
	case '@' | TYPE_IS_SHRIEKING:
SensorCall(16077);
#endif
	case '@':
	    from = start + symptr->strbeg;
	  resize:
#ifdef PERL_PACK_CAN_SHRIEKSIGN
	    SensorCall(16098);if (utf8  && !(datumtype & TYPE_IS_SHRIEKING))
#else /* PERL_PACK_CAN_SHRIEKSIGN */
	    if (utf8)
#endif
		{/*105*/SensorCall(16078);if (len >= 0) {
		    SensorCall(16079);while (len && from < cur) {
			SensorCall(16080);from += UTF8SKIP(from);
			len--;
		    }
		    SensorCall(16082);if (from > cur)
			{/*107*/SensorCall(16081);Perl_croak(aTHX_ "Malformed UTF-8 string in pack");/*108*/}
		    SensorCall(16088);if (len) {
			/* Here we know from == cur */
		      grow:
			GROWING(0, cat, start, cur, len);
			Zero(cur, len, char);
			SensorCall(16083);cur += len;
		    } else {/*109*/SensorCall(16084);if (from < cur) {
			SensorCall(16085);len = cur - from;
			SensorCall(16086);goto shrink;
		    } else {/*111*/SensorCall(16087);goto no_change;/*112*/}/*110*/}
		} else {
		    SensorCall(16089);cur = from;
		    len = -len;
		    SensorCall(16090);goto utf8_shrink;
		;/*106*/}}
	    else {
		SensorCall(16091);len -= cur - from;
		SensorCall(16093);if (len > 0) {/*113*/SensorCall(16092);goto grow;/*114*/}
		SensorCall(16095);if (len == 0) {/*115*/SensorCall(16094);goto no_change;/*116*/}
		SensorCall(16096);len = -len;
		SensorCall(16097);goto shrink;
	    }
	    SensorCall(16099);break;
	}
	case '(': {
            SensorCall(16100);tempsym_t savsym = *symptr;
	    U32 group_modifiers = TYPE_MODIFIERS(datumtype & ~symptr->flags);
	    symptr->flags |= group_modifiers;
            symptr->patend = savsym.grpend;
            symptr->level++;
	    symptr->previous = &lookahead;
	    SensorCall(16107);while (len--) {
		SensorCall(16101);U32 was_utf8;
		if (utf8) symptr->flags |=  FLAG_PARSE_UTF8;
		else      symptr->flags &= ~FLAG_PARSE_UTF8;
		SensorCall(16102);was_utf8 = SvUTF8(cat);
  	        symptr->patptr = savsym.grpbeg;
		beglist = pack_rec(cat, symptr, beglist, endlist);
		SensorCall(16104);if (SvUTF8(cat) != was_utf8)
		    /* This had better be an upgrade while in utf8==0 mode */
		    {/*121*/SensorCall(16103);utf8 = 1;/*122*/}

		SensorCall(16106);if (savsym.howlen == e_star && beglist == endlist)
		    {/*123*/SensorCall(16105);break;/*124*/}		/* No way to continue */
	    }
	    SensorCall(16108);items = endlist - beglist;
	    lookahead.flags  = symptr->flags & ~group_modifiers;
	    SensorCall(16109);goto no_change;
	}
	case 'X' | TYPE_IS_SHRIEKING:
	    SensorCall(16110);if (!len)			/* Avoid division by 0 */
		{/*125*/SensorCall(16111);len = 1;/*126*/}
	    SensorCall(16121);if (utf8) {
		SensorCall(16112);char *hop, *last;
		I32 l = len;
		hop = last = start;
		SensorCall(16116);while (hop < cur) {
		    SensorCall(16113);hop += UTF8SKIP(hop);
		    SensorCall(16115);if (--l == 0) {
			SensorCall(16114);last = hop;
			l = len;
		    }
		}
		SensorCall(16118);if (last > cur)
		    {/*127*/SensorCall(16117);Perl_croak(aTHX_ "Malformed UTF-8 string in pack");/*128*/}
		SensorCall(16119);cur = last;
		SensorCall(16120);break;
	    }
	    SensorCall(16122);len = (cur-start) % len;
	    /* FALL THROUGH */
	case 'X':
	    SensorCall(16123);if (utf8) {
		SensorCall(16124);if (len < 1) {/*129*/SensorCall(16125);goto no_change;/*130*/}
	      utf8_shrink:
		SensorCall(16132);while (len > 0) {
		    SensorCall(16126);if (cur <= start)
			{/*131*/SensorCall(16127);Perl_croak(aTHX_ "'%c' outside of string in pack",
				   (int) TYPE_NO_MODIFIERS(datumtype));/*132*/}
		    SensorCall(16130);while (--cur, UTF8_IS_CONTINUATION(*cur)) {
			SensorCall(16128);if (cur <= start)
			    {/*133*/SensorCall(16129);Perl_croak(aTHX_ "'%c' outside of string in pack",
				       (int) TYPE_NO_MODIFIERS(datumtype));/*134*/}
		    }
		    SensorCall(16131);len--;
		}
	    } else {
	      shrink:
		SensorCall(16133);if (cur - start < len)
		    {/*135*/SensorCall(16134);Perl_croak(aTHX_ "'%c' outside of string in pack",
			       (int) TYPE_NO_MODIFIERS(datumtype));/*136*/}
		SensorCall(16135);cur -= len;
	    }
	    SensorCall(16140);if (cur < start+symptr->strbeg) {
		/* Make sure group starts don't point into the void */
		SensorCall(16136);tempsym_t *group;
		const STRLEN length = cur-start;
		SensorCall(16138);for (group = symptr;
		     group && length < group->strbeg;
		     group = group->previous) {/*137*/SensorCall(16137);group->strbeg = length;/*138*/}
		SensorCall(16139);lookahead.strbeg = length;
	    }
	    SensorCall(16141);break;
	case 'x' | TYPE_IS_SHRIEKING: {
	    SensorCall(16142);I32 ai32;
	    SensorCall(16144);if (!len)			/* Avoid division by 0 */
		{/*139*/SensorCall(16143);len = 1;/*140*/}
	    SensorCall(16147);if (utf8) {/*141*/SensorCall(16145);ai32 = utf8_length((U8 *) start, (U8 *) cur) % len;/*142*/}
	    else      {/*143*/SensorCall(16146);ai32 = (cur - start) % len;/*144*/}
	    SensorCall(16149);if (ai32 == 0) {/*145*/SensorCall(16148);goto no_change;/*146*/}
	    SensorCall(16150);len -= ai32;
	}
	/* FALL THROUGH */
	case 'x':
	    SensorCall(16151);goto grow;
	case 'A':
	case 'Z':
	case 'a': {
	    SensorCall(16152);const char *aptr;

	    fromstr = NEXTFROM;
	    aptr = SvPV_const(fromstr, fromlen);
	    SensorCall(16202);if (DO_UTF8(fromstr)) {
                SensorCall(16153);const char *end, *s;

		SensorCall(16155);if (!utf8 && !SvUTF8(cat)) {
		    SensorCall(16154);marked_upgrade(aTHX_ cat, symptr);
		    lookahead.flags |= FLAG_DO_UTF8;
		    lookahead.strbeg = symptr->strbeg;
		    utf8 = 1;
		    start = SvPVX(cat);
		    cur = start + SvCUR(cat);
		}
		SensorCall(16159);if (howlen == e_star) {
		    SensorCall(16156);if (utf8) {/*147*/SensorCall(16157);goto string_copy;/*148*/}
		    SensorCall(16158);len = fromlen+1;
		}
		SensorCall(16160);s = aptr;
		end = aptr + fromlen;
		fromlen = datumtype == 'Z' ? len-1 : len;
		SensorCall(16162);while ((I32) fromlen > 0 && s < end) {
		    SensorCall(16161);s += UTF8SKIP(s);
		    fromlen--;
		}
		SensorCall(16164);if (s > end)
		    {/*149*/SensorCall(16163);Perl_croak(aTHX_ "Malformed UTF-8 string in pack");/*150*/}
		SensorCall(16170);if (utf8) {
		    SensorCall(16165);len = fromlen;
		    SensorCall(16167);if (datumtype == 'Z') {/*151*/SensorCall(16166);len++;/*152*/}
		    SensorCall(16168);fromlen = s-aptr;
		    len += fromlen;

		    SensorCall(16169);goto string_copy;
		}
		SensorCall(16171);fromlen = len - fromlen;
		SensorCall(16173);if (datumtype == 'Z') {/*153*/SensorCall(16172);fromlen--;/*154*/}
		SensorCall(16177);if (howlen == e_star) {
		    SensorCall(16174);len = fromlen;
		    SensorCall(16176);if (datumtype == 'Z') {/*155*/SensorCall(16175);len++;/*156*/}
		}
		GROWING(0, cat, start, cur, len);
		SensorCall(16179);if (!uni_to_bytes(aTHX_ &aptr, end, cur, fromlen,
				  datumtype | TYPE_IS_PACK))
		    {/*157*/SensorCall(16178);Perl_croak(aTHX_ "panic: predicted utf8 length not available, "
			       "for '%c', aptr=%p end=%p cur=%p, fromlen=%"UVuf,
			       (int)datumtype, aptr, end, cur, (UV)fromlen);/*158*/}
		SensorCall(16180);cur += fromlen;
		len -= fromlen;
	    } else {/*159*/SensorCall(16181);if (utf8) {
		SensorCall(16182);if (howlen == e_star) {
		    SensorCall(16183);len = fromlen;
		    SensorCall(16185);if (datumtype == 'Z') {/*161*/SensorCall(16184);len++;/*162*/}
		}
		SensorCall(16189);if (len <= (I32) fromlen) {
		    SensorCall(16186);fromlen = len;
		    SensorCall(16188);if (datumtype == 'Z' && fromlen > 0) {/*163*/SensorCall(16187);fromlen--;/*164*/}
		}
		/* assumes a byte expands to at most UTF8_EXPAND bytes on
		   upgrade, so:
		   expected_length <= from_len*UTF8_EXPAND + (len-from_len) */
		GROWING(0, cat, start, cur, fromlen*(UTF8_EXPAND-1)+len);
		SensorCall(16190);len -= fromlen;
		SensorCall(16192);while (fromlen > 0) {
		    SensorCall(16191);cur = (char *) uvchr_to_utf8((U8 *) cur, * (U8 *) aptr);
		    aptr++;
		    fromlen--;
		}
	    } else {
	      string_copy:
		SensorCall(16193);if (howlen == e_star) {
		    SensorCall(16194);len = fromlen;
		    SensorCall(16196);if (datumtype == 'Z') {/*165*/SensorCall(16195);len++;/*166*/}
		}
		SensorCall(16200);if (len <= (I32) fromlen) {
		    SensorCall(16197);fromlen = len;
		    SensorCall(16199);if (datumtype == 'Z' && fromlen > 0) {/*167*/SensorCall(16198);fromlen--;/*168*/}
		}
		GROWING(0, cat, start, cur, len);
		Copy(aptr, cur, fromlen, char);
		SensorCall(16201);cur += fromlen;
		len -= fromlen;
	    ;/*160*/}}
	    SensorCall(16203);memset(cur, datumtype == 'A' ? ' ' : '\0', len);
	    cur += len;
	    SvTAINT(cat);
	    SensorCall(16204);break;
	}
	case 'B':
	case 'b': {
	    SensorCall(16205);const char *str, *end;
	    I32 l, field_len;
	    U8 bits;
	    bool utf8_source;
	    U32 utf8_flags;

	    fromstr = NEXTFROM;
	    str = SvPV_const(fromstr, fromlen);
	    end = str + fromlen;
	    SensorCall(16208);if (DO_UTF8(fromstr)) {
		SensorCall(16206);utf8_source = TRUE;
		utf8_flags  = warn_utf8 ? 0 : UTF8_ALLOW_ANY;
	    } else {
		SensorCall(16207);utf8_source = FALSE;
		utf8_flags  = 0; /* Unused, but keep compilers happy */
	    }
	    SensorCall(16210);if (howlen == e_star) {/*169*/SensorCall(16209);len = fromlen;/*170*/}
	    SensorCall(16211);field_len = (len+7)/8;
	    GROWING(utf8, cat, start, cur, field_len);
	    SensorCall(16213);if (len > (I32)fromlen) {/*171*/SensorCall(16212);len = fromlen;/*172*/}
	    SensorCall(16214);bits = 0;
	    l = 0;
	    SensorCall(16232);if (datumtype == 'B')
		{/*173*/SensorCall(16215);while (l++ < len) {
		    SensorCall(16216);if (utf8_source) {
			SensorCall(16217);UV val = 0;
			NEXT_UNI_VAL(val, cur, str, end, utf8_flags);
			bits |= val & 1;
		    } else {/*175*/SensorCall(16218);bits |= *str++ & 1;/*176*/}
		    SensorCall(16221);if (l & 7) {/*177*/SensorCall(16219);bits <<= 1;/*178*/}
		    else {
			PUSH_BYTE(utf8, cur, bits);
			SensorCall(16220);bits = 0;
		    }
		;/*174*/}}
	    else
		/* datumtype == 'b' */
		{/*179*/SensorCall(16222);while (l++ < len) {
		    SensorCall(16223);if (utf8_source) {
			SensorCall(16224);UV val = 0;
			NEXT_UNI_VAL(val, cur, str, end, utf8_flags);
			SensorCall(16226);if (val & 1) {/*181*/SensorCall(16225);bits |= 0x80;/*182*/}
		    } else {/*183*/SensorCall(16227);if (*str++ & 1)
			{/*185*/SensorCall(16228);bits |= 0x80;/*186*/}/*184*/}
		    SensorCall(16231);if (l & 7) {/*187*/SensorCall(16229);bits >>= 1;/*188*/}
		    else {
			PUSH_BYTE(utf8, cur, bits);
			SensorCall(16230);bits = 0;
		    }
		;/*180*/}}
	    SensorCall(16233);l--;
	    SensorCall(16238);if (l & 7) {
		SensorCall(16234);if (datumtype == 'B')
		    {/*189*/SensorCall(16235);bits <<= 7 - (l & 7);/*190*/}
		else
		    {/*191*/SensorCall(16236);bits >>= 7 - (l & 7);/*192*/}
		PUSH_BYTE(utf8, cur, bits);
		SensorCall(16237);l += 7;
	    }
	    /* Determine how many chars are left in the requested field */
	    SensorCall(16239);l /= 8;
	    SensorCall(16242);if (howlen == e_star) {/*193*/SensorCall(16240);field_len = 0;/*194*/}
	    else {/*195*/SensorCall(16241);field_len -= l;/*196*/}
	    Zero(cur, field_len, char);
	    SensorCall(16243);cur += field_len;
	    SensorCall(16244);break;
	}
	case 'H':
	case 'h': {
	    SensorCall(16245);const char *str, *end;
	    I32 l, field_len;
	    U8 bits;
	    bool utf8_source;
	    U32 utf8_flags;

	    fromstr = NEXTFROM;
	    str = SvPV_const(fromstr, fromlen);
	    end = str + fromlen;
	    SensorCall(16248);if (DO_UTF8(fromstr)) {
		SensorCall(16246);utf8_source = TRUE;
		utf8_flags  = warn_utf8 ? 0 : UTF8_ALLOW_ANY;
	    } else {
		SensorCall(16247);utf8_source = FALSE;
		utf8_flags  = 0; /* Unused, but keep compilers happy */
	    }
	    SensorCall(16250);if (howlen == e_star) {/*197*/SensorCall(16249);len = fromlen;/*198*/}
	    SensorCall(16251);field_len = (len+1)/2;
	    GROWING(utf8, cat, start, cur, field_len);
	    SensorCall(16253);if (!utf8 && len > (I32)fromlen) {/*199*/SensorCall(16252);len = fromlen;/*200*/}
	    SensorCall(16254);bits = 0;
	    l = 0;
	    SensorCall(16279);if (datumtype == 'H')
		{/*201*/SensorCall(16255);while (l++ < len) {
		    SensorCall(16256);if (utf8_source) {
			SensorCall(16257);UV val = 0;
			NEXT_UNI_VAL(val, cur, str, end, utf8_flags);
			SensorCall(16260);if (val < 256 && isALPHA(val))
			    {/*203*/SensorCall(16258);bits |= (val + 9) & 0xf;/*204*/}
			else
			    {/*205*/SensorCall(16259);bits |= val & 0xf;/*206*/}
		    } else {/*207*/SensorCall(16261);if (isALPHA(*str))
			{/*209*/SensorCall(16262);bits |= (*str++ + 9) & 0xf;/*210*/}
		    else
			{/*211*/SensorCall(16263);bits |= *str++ & 0xf;/*212*/}/*208*/}
		    SensorCall(16266);if (l & 1) {/*213*/SensorCall(16264);bits <<= 4;/*214*/}
		    else {
			PUSH_BYTE(utf8, cur, bits);
			SensorCall(16265);bits = 0;
		    }
		;/*202*/}}
	    else
		{/*215*/SensorCall(16267);while (l++ < len) {
		    SensorCall(16268);if (utf8_source) {
			SensorCall(16269);UV val = 0;
			NEXT_UNI_VAL(val, cur, str, end, utf8_flags);
			SensorCall(16272);if (val < 256 && isALPHA(val))
			    {/*217*/SensorCall(16270);bits |= ((val + 9) & 0xf) << 4;/*218*/}
			else
			    {/*219*/SensorCall(16271);bits |= (val & 0xf) << 4;/*220*/}
		    } else {/*221*/SensorCall(16273);if (isALPHA(*str))
			{/*223*/SensorCall(16274);bits |= ((*str++ + 9) & 0xf) << 4;/*224*/}
		    else
			{/*225*/SensorCall(16275);bits |= (*str++ & 0xf) << 4;/*226*/}/*222*/}
		    SensorCall(16278);if (l & 1) {/*227*/SensorCall(16276);bits >>= 4;/*228*/}
		    else {
			PUSH_BYTE(utf8, cur, bits);
			SensorCall(16277);bits = 0;
		    }
		;/*216*/}}
	    SensorCall(16280);l--;
	    SensorCall(16282);if (l & 1) {
		PUSH_BYTE(utf8, cur, bits);
		SensorCall(16281);l++;
	    }
	    /* Determine how many chars are left in the requested field */
	    SensorCall(16283);l /= 2;
	    SensorCall(16286);if (howlen == e_star) {/*229*/SensorCall(16284);field_len = 0;/*230*/}
	    else {/*231*/SensorCall(16285);field_len -= l;/*232*/}
	    Zero(cur, field_len, char);
	    SensorCall(16287);cur += field_len;
	    SensorCall(16288);break;
	}
	case 'c':
	    SensorCall(16289);while (len-- > 0) {
		SensorCall(16290);IV aiv;
		fromstr = NEXTFROM;
		aiv = SvIV(fromstr);
		SensorCall(16292);if ((-128 > aiv || aiv > 127))
		    {/*233*/SensorCall(16291);Perl_ck_warner(aTHX_ packWARN(WARN_PACK),
				   "Character in 'c' format wrapped in pack");/*234*/}
		PUSH_BYTE(utf8, cur, (U8)(aiv & 0xff));
	    }
	    SensorCall(16293);break;
	case 'C':
	    SensorCall(16294);if (len == 0) {
		SensorCall(16295);utf8 = (symptr->flags & FLAG_DO_UTF8) ? 1 : 0;
		SensorCall(16296);break;
	    }
	    SensorCall(16300);while (len-- > 0) {
		SensorCall(16297);IV aiv;
		fromstr = NEXTFROM;
		aiv = SvIV(fromstr);
		SensorCall(16299);if ((0 > aiv || aiv > 0xff))
		    {/*235*/SensorCall(16298);Perl_ck_warner(aTHX_ packWARN(WARN_PACK),
				   "Character in 'C' format wrapped in pack");/*236*/}
		PUSH_BYTE(utf8, cur, (U8)(aiv & 0xff));
	    }
	    SensorCall(16301);break;
	case 'W': {
	    SensorCall(16302);char *end;
	    U8 in_bytes = (U8)IN_BYTES;

	    end = start+SvLEN(cat)-1;
	    SensorCall(16304);if (utf8) {/*237*/SensorCall(16303);end -= UTF8_MAXLEN-1;/*238*/}
	    SensorCall(16320);while (len-- > 0) {
		SensorCall(16305);UV auv;
		fromstr = NEXTFROM;
		auv = SvUV(fromstr);
		SensorCall(16307);if (in_bytes) {/*239*/SensorCall(16306);auv = auv % 0x100;/*240*/}
		SensorCall(16319);if (utf8) {
		  W_utf8:
		    SensorCall(16308);if (cur > end) {
			SensorCall(16309);*cur = '\0';
			SvCUR_set(cat, cur - start);

			GROWING(0, cat, start, cur, len+UTF8_MAXLEN);
			end = start+SvLEN(cat)-UTF8_MAXLEN;
		    }
		    SensorCall(16310);cur = (char *) uvuni_to_utf8_flags((U8 *) cur,
						       NATIVE_TO_UNI(auv),
						       warn_utf8 ?
						       0 : UNICODE_ALLOW_ANY);
		} else {
		    SensorCall(16311);if (auv >= 0x100) {
			SensorCall(16312);if (!SvUTF8(cat)) {
			    SensorCall(16313);*cur = '\0';
			    SvCUR_set(cat, cur - start);
			    marked_upgrade(aTHX_ cat, symptr);
			    lookahead.flags |= FLAG_DO_UTF8;
			    lookahead.strbeg = symptr->strbeg;
			    utf8 = 1;
			    start = SvPVX(cat);
			    cur = start + SvCUR(cat);
			    end = start+SvLEN(cat)-UTF8_MAXLEN;
			    SensorCall(16314);goto W_utf8;
			}
			SensorCall(16315);Perl_ck_warner(aTHX_ packWARN(WARN_PACK),
				       "Character in 'W' format wrapped in pack");
			auv &= 0xff;
		    }
		    SensorCall(16317);if (cur >= end) {
			SensorCall(16316);*cur = '\0';
			SvCUR_set(cat, cur - start);
			GROWING(0, cat, start, cur, len+1);
			end = start+SvLEN(cat)-1;
		    }
		    SensorCall(16318);*(U8 *) cur++ = (U8)auv;
		}
	    }
	    SensorCall(16321);break;
	}
	case 'U': {
	    SensorCall(16322);char *end;

	    SensorCall(16327);if (len == 0) {
		SensorCall(16323);if (!(symptr->flags & FLAG_DO_UTF8)) {
		    SensorCall(16324);marked_upgrade(aTHX_ cat, symptr);
		    lookahead.flags |= FLAG_DO_UTF8;
		    lookahead.strbeg = symptr->strbeg;
		}
		SensorCall(16325);utf8 = 0;
		SensorCall(16326);goto no_change;
	    }

	    SensorCall(16328);end = start+SvLEN(cat);
	    SensorCall(16329);if (!utf8) end -= UTF8_MAXLEN;
	    SensorCall(16339);while (len-- > 0) {
		SensorCall(16330);UV auv;
		fromstr = NEXTFROM;
		auv = SvUV(fromstr);
		SensorCall(16338);if (utf8) {
		    SensorCall(16331);U8 buffer[UTF8_MAXLEN], *endb;
		    endb = uvuni_to_utf8_flags(buffer, auv,
					       warn_utf8 ?
					       0 : UNICODE_ALLOW_ANY);
		    SensorCall(16333);if (cur+(endb-buffer)*UTF8_EXPAND >= end) {
			SensorCall(16332);*cur = '\0';
			SvCUR_set(cat, cur - start);
			GROWING(0, cat, start, cur,
				len+(endb-buffer)*UTF8_EXPAND);
			end = start+SvLEN(cat);
		    }
		    SensorCall(16334);cur = bytes_to_uni(buffer, endb-buffer, cur);
		} else {
		    SensorCall(16335);if (cur >= end) {
			SensorCall(16336);*cur = '\0';
			SvCUR_set(cat, cur - start);
			GROWING(0, cat, start, cur, len+UTF8_MAXLEN);
			end = start+SvLEN(cat)-UTF8_MAXLEN;
		    }
		    SensorCall(16337);cur = (char *) uvuni_to_utf8_flags((U8 *) cur, auv,
						       warn_utf8 ?
						       0 : UNICODE_ALLOW_ANY);
		}
	    }
	    SensorCall(16340);break;
	}
	/* Float and double added by gnb@melba.bby.oz.au  22/11/89 */
	case 'f':
	    SensorCall(16341);while (len-- > 0) {
		SensorCall(16342);float afloat;
		NV anv;
		fromstr = NEXTFROM;
		anv = SvNV(fromstr);
#ifdef __VOS__
		/* VOS does not automatically map a floating-point overflow
		   during conversion from double to float into infinity, so we
		   do it by hand.  This code should either be generalized for
		   any OS that needs it, or removed if and when VOS implements
		   posix-976 (suggestion to support mapping to infinity).
		   Paul.Green@stratus.com 02-04-02.  */
{
extern const float _float_constants[];
		if (anv > FLT_MAX)
		    afloat = _float_constants[0];   /* single prec. inf. */
		else if (anv < -FLT_MAX)
		    afloat = _float_constants[0];   /* single prec. inf. */
		else afloat = (float) anv;
}
#else /* __VOS__ */
# if defined(VMS) && !defined(__IEEE_FP)
		/* IEEE fp overflow shenanigans are unavailable on VAX and optional
		 * on Alpha; fake it if we don't have them.
		 */
		if (anv > FLT_MAX)
		    afloat = FLT_MAX;
		else if (anv < -FLT_MAX)
		    afloat = -FLT_MAX;
		else afloat = (float)anv;
# else
		afloat = (float)anv;
# endif
#endif /* __VOS__ */
		DO_BO_PACK_N(afloat, float);
		PUSH_VAR(utf8, cur, afloat);
	    }
	    SensorCall(16343);break;
	case 'd':
	    SensorCall(16344);while (len-- > 0) {
		SensorCall(16345);double adouble;
		NV anv;
		fromstr = NEXTFROM;
		anv = SvNV(fromstr);
#ifdef __VOS__
		/* VOS does not automatically map a floating-point overflow
		   during conversion from long double to double into infinity,
		   so we do it by hand.  This code should either be generalized
		   for any OS that needs it, or removed if and when VOS
		   implements posix-976 (suggestion to support mapping to
		   infinity).  Paul.Green@stratus.com 02-04-02.  */
{
extern const double _double_constants[];
		if (anv > DBL_MAX)
		    adouble = _double_constants[0];   /* double prec. inf. */
		else if (anv < -DBL_MAX)
		    adouble = _double_constants[0];   /* double prec. inf. */
		else adouble = (double) anv;
}
#else /* __VOS__ */
# if defined(VMS) && !defined(__IEEE_FP)
		/* IEEE fp overflow shenanigans are unavailable on VAX and optional
		 * on Alpha; fake it if we don't have them.
		 */
		if (anv > DBL_MAX)
		    adouble = DBL_MAX;
		else if (anv < -DBL_MAX)
		    adouble = -DBL_MAX;
		else adouble = (double)anv;
# else
		adouble = (double)anv;
# endif
#endif /* __VOS__ */
		DO_BO_PACK_N(adouble, double);
		PUSH_VAR(utf8, cur, adouble);
	    }
	    SensorCall(16346);break;
	case 'F': {
	    SensorCall(16347);NV_bytes anv;
	    Zero(&anv, 1, NV); /* can be long double with unused bits */
	    SensorCall(16349);while (len-- > 0) {
		SensorCall(16348);fromstr = NEXTFROM;
#ifdef __GNUC__
		/* to work round a gcc/x86 bug; don't use SvNV */
		anv.nv = sv_2nv(fromstr);
#else
		anv.nv = SvNV(fromstr);
#endif
		DO_BO_PACK_N(anv, NV);
		PUSH_BYTES(utf8, cur, anv.bytes, sizeof(anv.bytes));
	    }
	    SensorCall(16350);break;
	}
#if defined(HAS_LONG_DOUBLE) && defined(USE_LONG_DOUBLE)
	case 'D': {
	    ld_bytes aldouble;
	    /* long doubles can have unused bits, which may be nonzero */
	    Zero(&aldouble, 1, long double);
	    while (len-- > 0) {
		fromstr = NEXTFROM;
#  ifdef __GNUC__
		/* to work round a gcc/x86 bug; don't use SvNV */
		aldouble.ld = (long double)sv_2nv(fromstr);
#  else
		aldouble.ld = (long double)SvNV(fromstr);
#  endif
		DO_BO_PACK_N(aldouble, long double);
		PUSH_BYTES(utf8, cur, aldouble.bytes, sizeof(aldouble.bytes));
	    }
	    break;
	}
#endif
#ifdef PERL_PACK_CAN_SHRIEKSIGN
	case 'n' | TYPE_IS_SHRIEKING:
#endif
	case 'n':
	    SensorCall(16351);while (len-- > 0) {
		SensorCall(16352);I16 ai16;
		fromstr = NEXTFROM;
		ai16 = (I16)SvIV(fromstr);
#ifdef HAS_HTONS
		ai16 = PerlSock_htons(ai16);
#endif
		PUSH16(utf8, cur, &ai16);
	    }
	    SensorCall(16353);break;
#ifdef PERL_PACK_CAN_SHRIEKSIGN
	case 'v' | TYPE_IS_SHRIEKING:
#endif
	case 'v':
	    SensorCall(16354);while (len-- > 0) {
		SensorCall(16355);I16 ai16;
		fromstr = NEXTFROM;
		ai16 = (I16)SvIV(fromstr);
#ifdef HAS_HTOVS
		ai16 = htovs(ai16);
#endif
		PUSH16(utf8, cur, &ai16);
	    }
	    SensorCall(16356);break;
        case 'S' | TYPE_IS_SHRIEKING:
#if SHORTSIZE != SIZE16
	    while (len-- > 0) {
		unsigned short aushort;
		fromstr = NEXTFROM;
		aushort = SvUV(fromstr);
		DO_BO_PACK(aushort, s);
		PUSH_VAR(utf8, cur, aushort);
	    }
            break;
#else
            /* Fall through! */
#endif
	case 'S':
	    SensorCall(16357);while (len-- > 0) {
		SensorCall(16358);U16 au16;
		fromstr = NEXTFROM;
		au16 = (U16)SvUV(fromstr);
		DO_BO_PACK(au16, 16);
		PUSH16(utf8, cur, &au16);
	    }
	    SensorCall(16359);break;
	case 's' | TYPE_IS_SHRIEKING:
#if SHORTSIZE != SIZE16
	    while (len-- > 0) {
		short ashort;
		fromstr = NEXTFROM;
		ashort = SvIV(fromstr);
		DO_BO_PACK(ashort, s);
		PUSH_VAR(utf8, cur, ashort);
	    }
            break;
#else
            /* Fall through! */
#endif
	case 's':
	    SensorCall(16360);while (len-- > 0) {
		SensorCall(16361);I16 ai16;
		fromstr = NEXTFROM;
		ai16 = (I16)SvIV(fromstr);
		DO_BO_PACK(ai16, 16);
		PUSH16(utf8, cur, &ai16);
	    }
	    SensorCall(16362);break;
	case 'I':
	case 'I' | TYPE_IS_SHRIEKING:
	    SensorCall(16363);while (len-- > 0) {
		SensorCall(16364);unsigned int auint;
		fromstr = NEXTFROM;
		auint = SvUV(fromstr);
		DO_BO_PACK(auint, i);
		PUSH_VAR(utf8, cur, auint);
	    }
	    SensorCall(16365);break;
	case 'j':
	    SensorCall(16366);while (len-- > 0) {
		SensorCall(16367);IV aiv;
		fromstr = NEXTFROM;
		aiv = SvIV(fromstr);
#if IVSIZE == INTSIZE
		DO_BO_PACK(aiv, i);
#elif IVSIZE == LONGSIZE
		DO_BO_PACK(aiv, l);
#elif defined(HAS_QUAD) && IVSIZE == U64SIZE
		DO_BO_PACK(aiv, 64);
#else
		Perl_croak(aTHX_ "'j' not supported on this platform");
#endif
		PUSH_VAR(utf8, cur, aiv);
	    }
	    SensorCall(16368);break;
	case 'J':
	    SensorCall(16369);while (len-- > 0) {
		SensorCall(16370);UV auv;
		fromstr = NEXTFROM;
		auv = SvUV(fromstr);
#if UVSIZE == INTSIZE
		DO_BO_PACK(auv, i);
#elif UVSIZE == LONGSIZE
		DO_BO_PACK(auv, l);
#elif defined(HAS_QUAD) && UVSIZE == U64SIZE
		DO_BO_PACK(auv, 64);
#else
		Perl_croak(aTHX_ "'J' not supported on this platform");
#endif
		PUSH_VAR(utf8, cur, auv);
	    }
	    SensorCall(16371);break;
	case 'w':
            SensorCall(16372);while (len-- > 0) {
		SensorCall(16373);NV anv;
		fromstr = NEXTFROM;
		anv = SvNV(fromstr);

		SensorCall(16375);if (anv < 0) {
		    SensorCall(16374);*cur = '\0';
		    SvCUR_set(cat, cur - start);
		    Perl_croak(aTHX_ "Cannot compress negative numbers in pack");
		}

                /* 0xFFFFFFFFFFFFFFFF may cast to 18446744073709551616.0,
                   which is == UV_MAX_P1. IOK is fine (instead of UV_only), as
                   any negative IVs will have already been got by the croak()
                   above. IOK is untrue for fractions, so we test them
                   against UV_MAX_P1.  */
		SensorCall(16397);if (SvIOK(fromstr) || anv < UV_MAX_P1) {
		    SensorCall(16376);char   buf[(sizeof(UV)*CHAR_BIT)/7+1];
		    char  *in = buf + sizeof(buf);
		    UV     auv = SvUV(fromstr);

		    SensorCall(16378);do {
			SensorCall(16377);*--in = (char)((auv & 0x7f) | 0x80);
			auv >>= 7;
		    } while (auv);
		    SensorCall(16379);buf[sizeof(buf) - 1] &= 0x7f; /* clear continue bit */
		    PUSH_GROWING_BYTES(utf8, cat, start, cur,
				       in, (buf + sizeof(buf)) - in);
		} else {/*241*/SensorCall(16380);if (SvPOKp(fromstr))
		    {/*243*/SensorCall(16381);goto w_string;/*244*/}
		else {/*245*/SensorCall(16382);if (SvNOKp(fromstr)) {
		    /* 10**NV_MAX_10_EXP is the largest power of 10
		       so 10**(NV_MAX_10_EXP+1) is definitely unrepresentable
		       given 10**(NV_MAX_10_EXP+1) == 128 ** x solve for x:
		       x = (NV_MAX_10_EXP+1) * log (10) / log (128)
		       And with that many bytes only Inf can overflow.
		       Some C compilers are strict about integral constant
		       expressions so we conservatively divide by a slightly
		       smaller integer instead of multiplying by the exact
		       floating-point value.
		    */
#ifdef NV_MAX_10_EXP
		    /* char   buf[1 + (int)((NV_MAX_10_EXP + 1) * 0.47456)]; -- invalid C */
		    SensorCall(16383);char   buf[1 + (int)((NV_MAX_10_EXP + 1) / 2)]; /* valid C */
#else
		    /* char   buf[1 + (int)((308 + 1) * 0.47456)]; -- invalid C */
		    char   buf[1 + (int)((308 + 1) / 2)]; /* valid C */
#endif
		    char  *in = buf + sizeof(buf);

		    anv = Perl_floor(anv);
		    SensorCall(16388);do {
			SensorCall(16384);const NV next = Perl_floor(anv / 128);
			SensorCall(16386);if (in <= buf)  /* this cannot happen ;-) */
			    {/*247*/SensorCall(16385);Perl_croak(aTHX_ "Cannot compress integer in pack");/*248*/}
			SensorCall(16387);*--in = (unsigned char)(anv - (next * 128)) | 0x80;
			anv = next;
		    } while (anv > 0);
		    SensorCall(16389);buf[sizeof(buf) - 1] &= 0x7f; /* clear continue bit */
		    PUSH_GROWING_BYTES(utf8, cat, start, cur,
				       in, (buf + sizeof(buf)) - in);
		} else {
		    SensorCall(16390);const char     *from;
		    char           *result, *in;
		    SV             *norm;
		    STRLEN          len;
		    bool            done;

		  w_string:
		    /* Copy string and check for compliance */
		    from = SvPV_const(fromstr, len);
		    SensorCall(16392);if ((norm = is_an_int(from, len)) == NULL)
			{/*249*/SensorCall(16391);Perl_croak(aTHX_ "Can only compress unsigned integers in pack");/*250*/}

		    Newx(result, len, char);
		    SensorCall(16393);in = result + len;
		    done = FALSE;
		    SensorCall(16395);while (!done) {/*251*/SensorCall(16394);*--in = div128(norm, &done) | 0x80;/*252*/}
		    SensorCall(16396);result[len - 1] &= 0x7F; /* clear continue bit */
		    PUSH_GROWING_BYTES(utf8, cat, start, cur,
				       in, (result + len) - in);
		    Safefree(result);
		    SvREFCNT_dec(norm);	/* free norm */
		;/*246*/}/*242*/}}
	    }
            SensorCall(16398);break;
	case 'i':
	case 'i' | TYPE_IS_SHRIEKING:
	    SensorCall(16399);while (len-- > 0) {
		SensorCall(16400);int aint;
		fromstr = NEXTFROM;
		aint = SvIV(fromstr);
		DO_BO_PACK(aint, i);
		PUSH_VAR(utf8, cur, aint);
	    }
	    SensorCall(16401);break;
#ifdef PERL_PACK_CAN_SHRIEKSIGN
	case 'N' | TYPE_IS_SHRIEKING:
#endif
	case 'N':
	    SensorCall(16402);while (len-- > 0) {
		SensorCall(16403);U32 au32;
		fromstr = NEXTFROM;
		au32 = SvUV(fromstr);
#ifdef HAS_HTONL
		au32 = PerlSock_htonl(au32);
#endif
		PUSH32(utf8, cur, &au32);
	    }
	    SensorCall(16404);break;
#ifdef PERL_PACK_CAN_SHRIEKSIGN
	case 'V' | TYPE_IS_SHRIEKING:
#endif
	case 'V':
	    SensorCall(16405);while (len-- > 0) {
		SensorCall(16406);U32 au32;
		fromstr = NEXTFROM;
		au32 = SvUV(fromstr);
#ifdef HAS_HTOVL
		au32 = htovl(au32);
#endif
		PUSH32(utf8, cur, &au32);
	    }
	    SensorCall(16407);break;
	case 'L' | TYPE_IS_SHRIEKING:
#if LONGSIZE != SIZE32
	    SensorCall(16408);while (len-- > 0) {
		SensorCall(16409);unsigned long aulong;
		fromstr = NEXTFROM;
		aulong = SvUV(fromstr);
		DO_BO_PACK(aulong, l);
		PUSH_VAR(utf8, cur, aulong);
	    }
	    SensorCall(16410);break;
#else
            /* Fall though! */
#endif
	case 'L':
	    SensorCall(16411);while (len-- > 0) {
		SensorCall(16412);U32 au32;
		fromstr = NEXTFROM;
		au32 = SvUV(fromstr);
		DO_BO_PACK(au32, 32);
		PUSH32(utf8, cur, &au32);
	    }
	    SensorCall(16413);break;
	case 'l' | TYPE_IS_SHRIEKING:
#if LONGSIZE != SIZE32
	    SensorCall(16414);while (len-- > 0) {
		SensorCall(16415);long along;
		fromstr = NEXTFROM;
		along = SvIV(fromstr);
		DO_BO_PACK(along, l);
		PUSH_VAR(utf8, cur, along);
	    }
	    SensorCall(16416);break;
#else
            /* Fall though! */
#endif
	case 'l':
            SensorCall(16417);while (len-- > 0) {
		SensorCall(16418);I32 ai32;
		fromstr = NEXTFROM;
		ai32 = SvIV(fromstr);
		DO_BO_PACK(ai32, 32);
		PUSH32(utf8, cur, &ai32);
	    }
	    SensorCall(16419);break;
#ifdef HAS_QUAD
	case 'Q':
	    SensorCall(16420);while (len-- > 0) {
		Uquad_t auquad;
		SensorCall(16421);fromstr = NEXTFROM;
		auquad = (Uquad_t) SvUV(fromstr);
		DO_BO_PACK(auquad, 64);
		PUSH_VAR(utf8, cur, auquad);
	    }
	    SensorCall(16422);break;
	case 'q':
	    SensorCall(16423);while (len-- > 0) {
		Quad_t aquad;
		SensorCall(16424);fromstr = NEXTFROM;
		aquad = (Quad_t)SvIV(fromstr);
		DO_BO_PACK(aquad, 64);
		PUSH_VAR(utf8, cur, aquad);
	    }
	    SensorCall(16425);break;
#endif /* HAS_QUAD */
	case 'P':
	    SensorCall(16426);len = 1;		/* assume SV is correct length */
	    GROWING(utf8, cat, start, cur, sizeof(char *));
	    /* Fall through! */
	case 'p':
	    SensorCall(16427);while (len-- > 0) {
		SensorCall(16428);const char *aptr;

		fromstr = NEXTFROM;
		SvGETMAGIC(fromstr);
		SensorCall(16432);if (!SvOK(fromstr)) aptr = NULL;
		else {
		    /* XXX better yet, could spirit away the string to
		     * a safe spot and hang on to it until the result
		     * of pack() (and all copies of the result) are
		     * gone.
		     */
		    SensorCall(16429);if ((SvTEMP(fromstr) || (SvPADTMP(fromstr) &&
			     !SvREADONLY(fromstr)))) {
			SensorCall(16430);Perl_ck_warner(aTHX_ packWARN(WARN_PACK),
				       "Attempt to pack pointer to temporary value");
		    }
		    SensorCall(16431);if (SvPOK(fromstr) || SvNIOK(fromstr))
			aptr = SvPV_nomg_const_nolen(fromstr);
		    else
			aptr = SvPV_force_flags_nolen(fromstr, 0);
		}
		DO_BO_PACK_PC(aptr);
		PUSH_VAR(utf8, cur, aptr);
	    }
	    SensorCall(16433);break;
	case 'u': {
	    SensorCall(16434);const char *aptr, *aend;
	    bool from_utf8;

	    fromstr = NEXTFROM;
	    SensorCall(16437);if (len <= 2) {/*253*/SensorCall(16435);len = 45;/*254*/}
	    else {/*255*/SensorCall(16436);len = len / 3 * 3;/*256*/}
	    SensorCall(16439);if (len >= 64) {
		SensorCall(16438);Perl_ck_warner(aTHX_ packWARN(WARN_PACK),
			       "Field too wide in 'u' format in pack");
		len = 63;
	    }
	    SensorCall(16440);aptr = SvPV_const(fromstr, fromlen);
	    from_utf8 = DO_UTF8(fromstr);
	    SensorCall(16442);if (from_utf8) {
		SensorCall(16441);aend = aptr + fromlen;
		fromlen = sv_len_utf8(fromstr);
	    } else aend = NULL; /* Unused, but keep compilers happy */
	    GROWING(utf8, cat, start, cur, (fromlen+2) / 3 * 4 + (fromlen+len-1)/len * 2);
	    SensorCall(16454);while (fromlen > 0) {
		SensorCall(16443);U8 *end;
		I32 todo;
		U8 hunk[1+63/3*4+1];

		SensorCall(16446);if ((I32)fromlen > len)
		    {/*257*/SensorCall(16444);todo = len;/*258*/}
		else
		    {/*259*/SensorCall(16445);todo = fromlen;/*260*/}
		SensorCall(16452);if (from_utf8) {
		    SensorCall(16447);char buffer[64];
		    SensorCall(16449);if (!uni_to_bytes(aTHX_ &aptr, aend, buffer, todo,
				      'u' | TYPE_IS_PACK)) {
			SensorCall(16448);*cur = '\0';
			SvCUR_set(cat, cur - start);
			Perl_croak(aTHX_ "panic: string is shorter than advertised, "
				   "aptr=%p, aend=%p, buffer=%p, todo=%ld",
				   aptr, aend, buffer, (long) todo);
		    }
		    SensorCall(16450);end = doencodes(hunk, buffer, todo);
		} else {
		    SensorCall(16451);end = doencodes(hunk, aptr, todo);
		    aptr += todo;
		}
		PUSH_BYTES(utf8, cur, hunk, end-hunk);
		SensorCall(16453);fromlen -= todo;
	    }
	    SensorCall(16455);break;
	}
	}
	SensorCall(16457);*cur = '\0';
	SvCUR_set(cat, cur - start);
      no_change:
	*symptr = lookahead;
    }
    {SV ** ReplaceReturn1575 = beglist; SensorCall(16459); return ReplaceReturn1575;}
}
#undef NEXTFROM


PP(pp_pack)
{
SensorCall(16460);    dVAR; dSP; dMARK; dORIGMARK; dTARGET;
    register SV *cat = TARG;
    STRLEN fromlen;
    SV *pat_sv = *++MARK;
    register const char *pat = SvPV_const(pat_sv, fromlen);
    register const char *patend = pat + fromlen;

    MARK++;
    sv_setpvs(cat, "");
    SvUTF8_off(cat);

    packlist(cat, pat, patend, MARK, SP + 1);

    SvSETMAGIC(cat);
    SP = ORIGMARK;
    PUSHs(cat);
    RETURN;
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 *
 * ex: set ts=8 sts=4 sw=4 noet:
 */
