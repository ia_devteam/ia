#include "var/tmp/sensor.h"
/*    pp_hot.c
 *
 *    Copyright (C) 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000,
 *    2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008 by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 *
 */

/*
 * Then he heard Merry change the note, and up went the Horn-cry of Buckland,
 * shaking the air.
 *
 *                  Awake!  Awake!  Fear, Fire, Foes!  Awake!
 *                               Fire, Foes!  Awake!
 *
 *     [p.1007 of _The Lord of the Rings_, VI/viii: "The Scouring of the Shire"]
 */

/* This file contains 'hot' pp ("push/pop") functions that
 * execute the opcodes that make up a perl program. A typical pp function
 * expects to find its arguments on the stack, and usually pushes its
 * results onto the stack, hence the 'pp' terminology. Each OP structure
 * contains a pointer to the relevant pp_foo() function.
 *
 * By 'hot', we mean common ops whose execution speed is critical.
 * By gathering them together into a single file, we encourage
 * CPU cache hits on hot code. Also it could be taken as a warning not to
 * change any code in this file unless you're sure it won't affect
 * performance.
 */

#include "EXTERN.h"
#define PERL_IN_PP_HOT_C
#include "perl.h"

/* Hot code. */

PP(pp_const)
{
SensorCall(14480);    dVAR;
    dSP;
    XPUSHs(cSVOP_sv);
    RETURN;
}

PP(pp_nextstate)
{
SensorCall(14481);    dVAR;
    PL_curcop = (COP*)PL_op;
    TAINT_NOT;		/* Each statement is presumed innocent */
    PL_stack_sp = PL_stack_base + cxstack[cxstack_ix].blk_oldsp;
    FREETMPS;
    PERL_ASYNC_CHECK();
    {OP * ReplaceReturn2097 = NORMAL; SensorCall(14482); return ReplaceReturn2097;}
}

PP(pp_gvsv)
{
SensorCall(14483);    dVAR;
    dSP;
    EXTEND(SP,1);
    SensorCall(14484);if (PL_op->op_private & OPpLVAL_INTRO)
	PUSHs(save_scalar(cGVOP_gv));
    else
	PUSHs(GvSVn(cGVOP_gv));
    RETURN;
}

PP(pp_null)
{
SensorCall(14485);    dVAR;
    {OP * ReplaceReturn2096 = NORMAL; SensorCall(14486); return ReplaceReturn2096;}
}

/* This is sometimes called directly by pp_coreargs. */
PP(pp_pushmark)
{
    dVAR;
    PUSHMARK(PL_stack_sp);
    {OP * ReplaceReturn2095 = NORMAL; SensorCall(14487); return ReplaceReturn2095;}
}

PP(pp_stringify)
{
SensorCall(14488);    dVAR; dSP; dTARGET;
    sv_copypv(TARG,TOPs);
    SETTARG;
    RETURN;
}

PP(pp_gv)
{
SensorCall(14489);    dVAR; dSP;
    XPUSHs(MUTABLE_SV(cGVOP_gv));
    RETURN;
}

PP(pp_and)
{
SensorCall(14490);    dVAR; dSP;
    PERL_ASYNC_CHECK();
    SensorCall(14492);if (!SvTRUE(TOPs))
	RETURN;
    else {
        SensorCall(14491);if (PL_op->op_type == OP_AND)
	    --SP;
	RETURNOP(cLOGOP->op_other);
    }
SensorCall(14493);}

PP(pp_sassign)
{
SensorCall(14494);    dVAR; dSP; dPOPTOPssrl;

    SensorCall(14496);if (PL_op->op_private & OPpASSIGN_BACKWARDS) {
	SensorCall(14495);SV * const temp = left;
	left = right; right = temp;
    }
    SensorCall(14497);if (PL_tainting && PL_tainted && !SvTAINTED(left))
	TAINT_NOT;
    SensorCall(14509);if (PL_op->op_private & OPpASSIGN_CV_TO_GV) {
	SensorCall(14498);SV * const cv = SvRV(left);
	const U32 cv_type = SvTYPE(cv);
	const bool is_gv = isGV_with_GP(right);
	const bool got_coderef = cv_type == SVt_PVCV || cv_type == SVt_PVFM;

	SensorCall(14499);if (!got_coderef) {
	    assert(SvROK(cv));
	}

	/* Can do the optimisation if right (LVALUE) is not a typeglob,
	   left (RVALUE) is a reference to something, and we're in void
	   context. */
	SensorCall(14503);if (!got_coderef && !is_gv && GIMME_V == G_VOID) {
	    /* Is the target symbol table currently empty?  */
	    SensorCall(14500);GV * const gv = gv_fetchsv_nomg(right, GV_NOINIT, SVt_PVGV);
	    SensorCall(14502);if (SvTYPE(gv) != SVt_PVGV && !SvOK(gv)) {
		/* Good. Create a new proxy constant subroutine in the target.
		   The gv becomes a(nother) reference to the constant.  */
		SensorCall(14501);SV *const value = SvRV(cv);

		SvUPGRADE(MUTABLE_SV(gv), SVt_IV);
		SvPCS_IMPORTED_on(gv);
		SvRV_set(gv, value);
		SvREFCNT_inc_simple_void(value);
		SETs(right);
		RETURN;
	    }
	}

	/* Need to fix things up.  */
	SensorCall(14505);if (!is_gv) {
	    /* Need to fix GV.  */
	    SensorCall(14504);right = MUTABLE_SV(gv_fetchsv_nomg(right,GV_ADD, SVt_PVGV));
	}

	SensorCall(14508);if (!got_coderef) {
	    /* We've been returned a constant rather than a full subroutine,
	       but they expect a subroutine reference to apply.  */
	    SensorCall(14506);if (SvROK(cv)) {
		ENTER_with_name("sassign_coderef");
		SvREFCNT_inc_void(SvRV(cv));
		/* newCONSTSUB takes a reference count on the passed in SV
		   from us.  We set the name to NULL, otherwise we get into
		   all sorts of fun as the reference to our new sub is
		   donated to the GV that we're about to assign to.
		*/
		SvRV_set(left, MUTABLE_SV(newCONSTSUB(GvSTASH(right), NULL,
						      SvRV(cv))));
		SvREFCNT_dec(cv);
		LEAVE_with_name("sassign_coderef");
	    } else {
		/* What can happen for the corner case *{"BONK"} = \&{"BONK"};
		   is that
		   First:   ops for \&{"BONK"}; return us the constant in the
			    symbol table
		   Second:  ops for *{"BONK"} cause that symbol table entry
			    (and our reference to it) to be upgraded from RV
			    to typeblob)
		   Thirdly: We get here. cv is actually PVGV now, and its
			    GvCV() is actually the subroutine we're looking for

		   So change the reference so that it points to the subroutine
		   of that typeglob, as that's what they were after all along.
		*/
		SensorCall(14507);GV *const upgraded = MUTABLE_GV(cv);
		CV *const source = GvCV(upgraded);

		assert(source);
		assert(CvFLAGS(source) & CVf_CONST);

		SvREFCNT_inc_void(source);
		SvREFCNT_dec(upgraded);
		SvRV_set(left, MUTABLE_SV(source));
	    }
	}

    }
    SensorCall(14511);if (
      SvTEMP(right) && !SvSMAGICAL(right) && SvREFCNT(right) == 1 &&
      (!isGV_with_GP(right) || SvFAKE(right)) && ckWARN(WARN_MISC)
    )
	{/*139*/SensorCall(14510);Perl_warner(aTHX_
	    packWARN(WARN_MISC), "Useless assignment to a temporary"
	);/*140*/}
    SvSetMagicSV(right, left);
    SETs(right);
    RETURN;
}

PP(pp_cond_expr)
{
SensorCall(14512);    dVAR; dSP;
    PERL_ASYNC_CHECK();
    SensorCall(14513);if (SvTRUEx(POPs))
	RETURNOP(cLOGOP->op_other);
    else
	RETURNOP(cLOGOP->op_next);
SensorCall(14514);}

PP(pp_unstack)
{
SensorCall(14515);    dVAR;
    PERL_ASYNC_CHECK();
    TAINT_NOT;		/* Each statement is presumed innocent */
    PL_stack_sp = PL_stack_base + cxstack[cxstack_ix].blk_oldsp;
    FREETMPS;
    SensorCall(14517);if (!(PL_op->op_flags & OPf_SPECIAL)) {
	SensorCall(14516);I32 oldsave = PL_scopestack[PL_scopestack_ix - 1];
	LEAVE_SCOPE(oldsave);
    }
    {OP * ReplaceReturn2094 = NORMAL; SensorCall(14518); return ReplaceReturn2094;}
}

PP(pp_concat)
{
SensorCall(14519);  dVAR; dSP; dATARGET; tryAMAGICbin_MG(concat_amg, AMGf_assign);
  {
    dPOPTOPssrl;
    bool lbyte;
    STRLEN rlen;
    const char *rpv = NULL;
    bool rbyte = FALSE;
    bool rcopied = FALSE;

    SensorCall(14521);if (TARG == right && right != left) { /* $r = $l.$r */
	SensorCall(14520);rpv = SvPV_nomg_const(right, rlen);
	rbyte = !DO_UTF8(right);
	right = newSVpvn_flags(rpv, rlen, SVs_TEMP);
	rpv = SvPV_const(right, rlen);	/* no point setting UTF-8 here */
	rcopied = TRUE;
    }

    SensorCall(14528);if (TARG != left) { /* not $l .= $r */
        SensorCall(14522);STRLEN llen;
        const char* const lpv = SvPV_nomg_const(left, llen);
	lbyte = !DO_UTF8(left);
	sv_setpvn(TARG, lpv, llen);
	SensorCall(14523);if (!lbyte)
	    SvUTF8_on(TARG);
	else
	    SvUTF8_off(TARG);
    }
    else { /* $l .= $r */
	SensorCall(14524);if (!SvOK(TARG)) {
	    SensorCall(14525);if (left == right && ckWARN(WARN_UNINITIALIZED)) /* $l .= $l */
		report_uninit(right);
	    sv_setpvs(left, "");
	}
	SensorCall(14526);lbyte = (SvROK(left) && SvTYPE(SvRV(left)) == SVt_REGEXP)
		    ?  !DO_UTF8(SvRV(left)) : !DO_UTF8(left);
	SensorCall(14527);if (IN_BYTES)
	    SvUTF8_off(TARG);
    }

    SensorCall(14531);if (!rcopied) {
	SensorCall(14529);if (left == right)
	    /* $r.$r: do magic twice: tied might return different 2nd time */
	    SvGETMAGIC(right);
	SensorCall(14530);rpv = SvPV_nomg_const(right, rlen);
	rbyte = !DO_UTF8(right);
    }
    SensorCall(14535);if (lbyte != rbyte) {
	/* sv_utf8_upgrade_nomg() may reallocate the stack */
	PUTBACK;
	SensorCall(14534);if (lbyte)
	    sv_utf8_upgrade_nomg(TARG);
	else {
	    SensorCall(14532);if (!rcopied)
		right = newSVpvn_flags(rpv, rlen, SVs_TEMP);
	    sv_utf8_upgrade_nomg(right);
	    SensorCall(14533);rpv = SvPV_nomg_const(right, rlen);
	}
	SPAGAIN;
    }
    sv_catpvn_nomg(TARG, rpv, rlen);

    SETTARG;
    RETURN;
  }
}

PP(pp_padsv)
{
SensorCall(14536);    dVAR; dSP; dTARGET;
    XPUSHs(TARG);
    SensorCall(14539);if (PL_op->op_flags & OPf_MOD) {
	SensorCall(14537);if (PL_op->op_private & OPpLVAL_INTRO)
	    if (!(PL_op->op_private & OPpPAD_STATE))
		SAVECLEARSV(PAD_SVl(PL_op->op_targ));
        SensorCall(14538);if (PL_op->op_private & OPpDEREF) {
	    PUTBACK;
	    TOPs = vivify_ref(TOPs, PL_op->op_private & OPpDEREF);
	    SPAGAIN;
	}
    }
    RETURN;
}

PP(pp_readline)
{
SensorCall(14540);    dVAR;
    dSP;
    SensorCall(14541);if (TOPs) {
	SvGETMAGIC(TOPs);
	tryAMAGICunTARGET(iter_amg, 0, 0);
	PL_last_in_gv = MUTABLE_GV(*PL_stack_sp--);
    }
    else PL_last_in_gv = PL_argvgv, PL_stack_sp--;
    SensorCall(14544);if (!isGV_with_GP(PL_last_in_gv)) {
	SensorCall(14542);if (SvROK(PL_last_in_gv) && isGV_with_GP(SvRV(PL_last_in_gv)))
	    PL_last_in_gv = MUTABLE_GV(SvRV(PL_last_in_gv));
	else {
	    dSP;
	    XPUSHs(MUTABLE_SV(PL_last_in_gv));
	    PUTBACK;
	    SensorCall(14543);Perl_pp_rv2gv(aTHX);
	    PL_last_in_gv = MUTABLE_GV(*PL_stack_sp--);
	}
    }
    {OP * ReplaceReturn2093 = do_readline(); SensorCall(14545); return ReplaceReturn2093;}
}

PP(pp_eq)
{
SensorCall(14546);    dVAR; dSP;
    SV *left, *right;

    tryAMAGICbin_MG(eq_amg, AMGf_set|AMGf_numeric);
    right = POPs;
    left  = TOPs;
    SETs(boolSV(
	(SvIOK_notUV(left) && SvIOK_notUV(right))
	? (SvIVX(left) == SvIVX(right))
	: ( do_ncmp(left, right) == 0)
    ));
    RETURN;
}

PP(pp_preinc)
{
SensorCall(14547);    dVAR; dSP;
    const bool inc =
	PL_op->op_type == OP_PREINC || PL_op->op_type == OP_I_PREINC;
    SensorCall(14549);if (SvTYPE(TOPs) >= SVt_PVAV || (isGV_with_GP(TOPs) && !SvFAKE(TOPs)))
	{/*95*/SensorCall(14548);Perl_croak_no_modify(aTHX);/*96*/}
    SensorCall(14550);if (!SvREADONLY(TOPs) && SvIOK_notUV(TOPs) && !SvNOK(TOPs) && !SvPOK(TOPs)
        && SvIVX(TOPs) != (inc ? IV_MAX : IV_MIN))
    {
	SvIV_set(TOPs, SvIVX(TOPs) + (inc ? 1 : -1));
	SvFLAGS(TOPs) &= ~(SVp_NOK|SVp_POK);
    }
    else /* Do all the PERL_PRESERVE_IVUV conditionals in sv_inc */
	if (inc) sv_inc(TOPs);
	else sv_dec(TOPs);
    SvSETMAGIC(TOPs);
    {OP * ReplaceReturn2092 = NORMAL; SensorCall(14551); return ReplaceReturn2092;}
}

PP(pp_or)
{
SensorCall(14552);    dVAR; dSP;
    PERL_ASYNC_CHECK();
    SensorCall(14554);if (SvTRUE(TOPs))
	RETURN;
    else {
	SensorCall(14553);if (PL_op->op_type == OP_OR)
            --SP;
	RETURNOP(cLOGOP->op_other);
    }
SensorCall(14555);}

PP(pp_defined)
{
SensorCall(14556);    dVAR; dSP;
    register SV* sv;
    bool defined;
    const int op_type = PL_op->op_type;
    const bool is_dor = (op_type == OP_DOR || op_type == OP_DORASSIGN);

    SensorCall(14562);if (is_dor) {
	PERL_ASYNC_CHECK();
        SensorCall(14557);sv = TOPs;
        SensorCall(14559);if (!sv || !SvANY(sv)) {
	    SensorCall(14558);if (op_type == OP_DOR)
		--SP;
            RETURNOP(cLOGOP->op_other);
        }
    }
    else {
	/* OP_DEFINED */
        SensorCall(14560);sv = POPs;
        SensorCall(14561);if (!sv || !SvANY(sv))
            RETPUSHNO;
    }

    SensorCall(14563);defined = FALSE;
    SensorCall(14572);switch (SvTYPE(sv)) {
    case SVt_PVAV:
	SensorCall(14564);if (AvMAX(sv) >= 0 || SvGMAGICAL(sv) || (SvRMAGICAL(sv) && mg_find(sv, PERL_MAGIC_tied)))
	    defined = TRUE;
	SensorCall(14565);break;
    case SVt_PVHV:
	SensorCall(14566);if (HvARRAY(sv) || SvGMAGICAL(sv) || (SvRMAGICAL(sv) && mg_find(sv, PERL_MAGIC_tied)))
	    defined = TRUE;
	SensorCall(14567);break;
    case SVt_PVCV:
	SensorCall(14568);if (CvROOT(sv) || CvXSUB(sv))
	    defined = TRUE;
	SensorCall(14569);break;
    default:
	SvGETMAGIC(sv);
	SensorCall(14570);if (SvOK(sv))
	    defined = TRUE;
	SensorCall(14571);break;
    }

    SensorCall(14575);if (is_dor) {
        SensorCall(14573);if(defined) 
            RETURN; 
        SensorCall(14574);if(op_type == OP_DOR)
            --SP;
        RETURNOP(cLOGOP->op_other);
    }
    /* assuming OP_DEFINED */
    SensorCall(14576);if(defined) 
        RETPUSHYES;
    RETPUSHNO;
}

PP(pp_add)
{
SensorCall(14577);    dVAR; dSP; dATARGET; bool useleft; SV *svl, *svr;
    tryAMAGICbin_MG(add_amg, AMGf_assign|AMGf_numeric);
    svr = TOPs;
    svl = TOPm1s;

    useleft = USE_LEFT(svl);
#ifdef PERL_PRESERVE_IVUV
    /* We must see if we can perform the addition with integers if possible,
       as the integer code detects overflow while the NV code doesn't.
       If either argument hasn't had a numeric conversion yet attempt to get
       the IV. It's important to do this now, rather than just assuming that
       it's not IOK as a PV of "9223372036854775806" may not take well to NV
       addition, and an SV which is NOK, NV=6.0 ought to be coerced to
       integer in case the second argument is IV=9223372036854775806
       We can (now) rely on sv_2iv to do the right thing, only setting the
       public IOK flag if the value in the NV (or PV) slot is truly integer.

       A side effect is that this also aggressively prefers integer maths over
       fp maths for integer values.

       How to detect overflow?

       C 99 section 6.2.6.1 says

       The range of nonnegative values of a signed integer type is a subrange
       of the corresponding unsigned integer type, and the representation of
       the same value in each type is the same. A computation involving
       unsigned operands can never overflow, because a result that cannot be
       represented by the resulting unsigned integer type is reduced modulo
       the number that is one greater than the largest value that can be
       represented by the resulting type.

       (the 9th paragraph)

       which I read as "unsigned ints wrap."

       signed integer overflow seems to be classed as "exception condition"

       If an exceptional condition occurs during the evaluation of an
       expression (that is, if the result is not mathematically defined or not
       in the range of representable values for its type), the behavior is
       undefined.

       (6.5, the 5th paragraph)

       I had assumed that on 2s complement machines signed arithmetic would
       wrap, hence coded pp_add and pp_subtract on the assumption that
       everything perl builds on would be happy.  After much wailing and
       gnashing of teeth it would seem that irix64 knows its ANSI spec well,
       knows that it doesn't need to, and doesn't.  Bah.  Anyway, the all-
       unsigned code below is actually shorter than the old code. :-)
    */

    SvIV_please_nomg(svr);

    SensorCall(14609);if (SvIOK(svr)) {
	/* Unless the left argument is integer in range we are going to have to
	   use NV maths. Hence only attempt to coerce the right argument if
	   we know the left is integer.  */
	SensorCall(14578);register UV auv = 0;
	bool auvok = FALSE;
	bool a_valid = 0;

	SensorCall(14587);if (!useleft) {
	    SensorCall(14579);auv = 0;
	    a_valid = auvok = 1;
	    /* left operand is undef, treat as zero. + 0 is identity,
	       Could SETi or SETu right now, but space optimise by not adding
	       lots of code to speed up what is probably a rarish case.  */
	} else {
	    /* Left operand is defined, so is it IV? */
	    SvIV_please_nomg(svl);
	    SensorCall(14586);if (SvIOK(svl)) {
		SensorCall(14580);if ((auvok = SvUOK(svl)))
		    auv = SvUVX(svl);
		else {
		    SensorCall(14581);register const IV aiv = SvIVX(svl);
		    SensorCall(14584);if (aiv >= 0) {
			SensorCall(14582);auv = aiv;
			auvok = 1;	/* Now acting as a sign flag.  */
		    } else { /* 2s complement assumption for IV_MIN */
			SensorCall(14583);auv = (UV)-aiv;
		    }
		}
		SensorCall(14585);a_valid = 1;
	    }
	}
	SensorCall(14608);if (a_valid) {
	    bool result_good = 0;
	    SensorCall(14588);UV result;
	    register UV buv;
	    bool buvok = SvUOK(svr);
	
	    SensorCall(14593);if (buvok)
		buv = SvUVX(svr);
	    else {
		SensorCall(14589);register const IV biv = SvIVX(svr);
		SensorCall(14592);if (biv >= 0) {
		    SensorCall(14590);buv = biv;
		    buvok = 1;
		} else
		    {/*49*/SensorCall(14591);buv = (UV)-biv;/*50*/}
	    }
	    /* ?uvok if value is >= 0. basically, flagged as UV if it's +ve,
	       else "IV" now, independent of how it came in.
	       if a, b represents positive, A, B negative, a maps to -A etc
	       a + b =>  (a + b)
	       A + b => -(a - b)
	       a + B =>  (a - b)
	       A + B => -(a + b)
	       all UV maths. negate result if A negative.
	       add if signs same, subtract if signs differ. */

	    SensorCall(14604);if (auvok ^ buvok) {
		/* Signs differ.  */
		SensorCall(14594);if (auv >= buv) {
		    SensorCall(14595);result = auv - buv;
		    /* Must get smaller */
		    SensorCall(14597);if (result <= auv)
			{/*51*/SensorCall(14596);result_good = 1;/*52*/}
		} else {
		    SensorCall(14598);result = buv - auv;
		    SensorCall(14600);if (result <= buv) {
			/* result really should be -(auv-buv). as its negation
			   of true value, need to swap our result flag  */
			SensorCall(14599);auvok = !auvok;
			result_good = 1;
		    }
		}
	    } else {
		/* Signs same */
		SensorCall(14601);result = auv + buv;
		SensorCall(14603);if (result >= auv)
		    {/*53*/SensorCall(14602);result_good = 1;/*54*/}
	    }
	    SensorCall(14607);if (result_good) {
		SP--;
		SensorCall(14606);if (auvok)
		    SETu( result );
		else {
		    /* Negate result */
		    SensorCall(14605);if (result <= (UV)IV_MIN)
			SETi( -(IV)result );
		    else {
			/* result valid, but out of range for IV.  */
			SETn( -(NV)result );
		    }
		}
		RETURN;
	    } /* Overflow, drop through to NVs.  */
	}
    }
#endif
    {
	SensorCall(14610);NV value = SvNV_nomg(svr);
	(void)POPs;
	SensorCall(14611);if (!useleft) {
	    /* left operand is undef, treat as zero. + 0.0 is identity. */
	    SETn(value);
	    RETURN;
	}
	SETn( value + SvNV_nomg(svl) );
	RETURN;
    }
}

PP(pp_aelemfast)
{
SensorCall(14612);    dVAR; dSP;
    AV * const av = PL_op->op_type == OP_AELEMFAST_LEX
	? MUTABLE_AV(PAD_SV(PL_op->op_targ)) : GvAVn(cGVOP_gv);
    const U32 lval = PL_op->op_flags & OPf_MOD;
    SV** const svp = av_fetch(av, PL_op->op_private, lval);
    SV *sv = (svp ? *svp : &PL_sv_undef);
    EXTEND(SP, 1);
    SensorCall(14613);if (!lval && SvRMAGICAL(av) && SvGMAGICAL(sv)) /* see note in pp_helem() */
	mg_get(sv);
    PUSHs(sv);
    RETURN;
}

PP(pp_join)
{
SensorCall(14614);    dVAR; dSP; dMARK; dTARGET;
    MARK++;
    do_join(TARG, *MARK, MARK, SP);
    SP = MARK;
    SETs(TARG);
    RETURN;
}

PP(pp_pushre)
{
SensorCall(14615);    dVAR; dSP;
#ifdef DEBUGGING
    /*
     * We ass_u_me that LvTARGOFF() comes first, and that two STRLENs
     * will be enough to hold an OP*.
     */
    SV* const sv = sv_newmortal();
    sv_upgrade(sv, SVt_PVLV);
    LvTYPE(sv) = '/';
    Copy(&PL_op, &LvTARGOFF(sv), 1, OP*);
    XPUSHs(sv);
#else
    XPUSHs(MUTABLE_SV(PL_op));
#endif
    RETURN;
}

/* Oversized hot code. */

PP(pp_print)
{
SensorCall(14616);    dVAR; dSP; dMARK; dORIGMARK;
    register PerlIO *fp;
    MAGIC *mg;
    GV * const gv
	= (PL_op->op_flags & OPf_STACKED) ? MUTABLE_GV(*++MARK) : PL_defoutgv;
    IO *io = GvIO(gv);

    SensorCall(14620);if (io
	&& (mg = SvTIED_mg((const SV *)io, PERL_MAGIC_tiedscalar)))
    {
      had_magic:
	SensorCall(14617);if (MARK == ORIGMARK) {
	    /* If using default handle then we need to make space to
	     * pass object as 1st arg, so move other args up ...
	     */
	    MEXTEND(SP, 1);
	    SensorCall(14618);++MARK;
	    Move(MARK, MARK + 1, (SP - MARK) + 1, SV*);
	    ++SP;
	}
	{OP * ReplaceReturn2091 = Perl_tied_method(aTHX_ "PRINT", mark - 1, MUTABLE_SV(io),
				mg,
				(G_SCALAR | TIED_METHOD_ARGUMENTS_ON_STACK
				 | (PL_op->op_type == OP_SAY
				    ? TIED_METHOD_SAY : 0)), sp - mark); SensorCall(14619); return ReplaceReturn2091;}
    }
    SensorCall(14649);if (!io) {
        SensorCall(14621);if ( gv && GvEGVx(gv) && (io = GvIO(GvEGV(gv)))
	    && (mg = SvTIED_mg((const SV *)io, PERL_MAGIC_tiedscalar)))
            {/*97*/SensorCall(14622);goto had_magic;/*98*/}
	report_evil_fh(gv);
	SETERRNO(EBADF,RMS_IFI);
	SensorCall(14623);goto just_say_no;
    }
    else {/*99*/SensorCall(14624);if (!(fp = IoOFP(io))) {
	SensorCall(14625);if (IoIFP(io))
	    report_wrongway_fh(gv, '<');
	else
	    report_evil_fh(gv);
	SETERRNO(EBADF,IoIFP(io)?RMS_FAC:RMS_IFI);
	SensorCall(14626);goto just_say_no;
    }
    else {
	SensorCall(14627);SV * const ofs = GvSV(PL_ofsgv); /* $, */
	MARK++;
	SensorCall(14637);if (ofs && (SvGMAGICAL(ofs) || SvOK(ofs))) {
	    SensorCall(14628);while (MARK <= SP) {
		SensorCall(14629);if (!do_print(*MARK, fp))
		    {/*101*/SensorCall(14630);break;/*102*/}
		MARK++;
		SensorCall(14633);if (MARK <= SP) {
		    /* don't use 'ofs' here - it may be invalidated by magic callbacks */
		    SensorCall(14631);if (!do_print(GvSV(PL_ofsgv), fp)) {
			MARK--;
			SensorCall(14632);break;
		    }
		}
	    }
	}
	else {
	    SensorCall(14634);while (MARK <= SP) {
		SensorCall(14635);if (!do_print(*MARK, fp))
		    {/*103*/SensorCall(14636);break;/*104*/}
		MARK++;
	    }
	}
	SensorCall(14648);if (MARK <= SP)
	    {/*105*/SensorCall(14638);goto just_say_no;/*106*/}
	else {
	    SensorCall(14639);if (PL_op->op_type == OP_SAY) {
		SensorCall(14640);if (PerlIO_write(fp, "\n", 1) == 0 || PerlIO_error(fp))
		    {/*107*/SensorCall(14641);goto just_say_no;/*108*/}
	    }
            else {/*109*/SensorCall(14642);if (PL_ors_sv && SvOK(PL_ors_sv))
		{/*111*/SensorCall(14643);if (!do_print(PL_ors_sv, fp)) /* $\ */
		    {/*113*/SensorCall(14644);goto just_say_no;/*114*/}/*112*/}/*110*/}

	    SensorCall(14647);if (IoFLAGS(io) & IOf_FLUSH)
		{/*115*/SensorCall(14645);if (PerlIO_flush(fp) == EOF)
		    {/*117*/SensorCall(14646);goto just_say_no;/*118*/}/*116*/}
	}
    ;/*100*/}}
    SP = ORIGMARK;
    XPUSHs(&PL_sv_yes);
    RETURN;

  just_say_no:
    SP = ORIGMARK;
    XPUSHs(&PL_sv_undef);
    RETURN;
}

PP(pp_rv2av)
{
SensorCall(14650);    dVAR; dSP; dTOPss;
    const I32 gimme = GIMME_V;
    static const char an_array[] = "an ARRAY";
    static const char a_hash[] = "a HASH";
    const bool is_pp_rv2av = PL_op->op_type == OP_RV2AV;
    const svtype type = is_pp_rv2av ? SVt_PVAV : SVt_PVHV;

    SvGETMAGIC(sv);
    SensorCall(14681);if (SvROK(sv)) {
	SensorCall(14651);if (SvAMAGIC(sv)) {
	    SensorCall(14652);sv = amagic_deref_call(sv, is_pp_rv2av ? to_av_amg : to_hv_amg);
	}
	SensorCall(14653);sv = SvRV(sv);
	SensorCall(14654);if (SvTYPE(sv) != type)
	    /* diag_listed_as: Not an ARRAY reference */
	    DIE(aTHX_ "Not %s reference", is_pp_rv2av ? an_array : a_hash);
	SensorCall(14662);if (PL_op->op_flags & OPf_REF) {
	    SETs(sv);
	    RETURN;
	}
	else {/*119*/SensorCall(14655);if (PL_op->op_private & OPpMAYBE_LVSUB) {
	  SensorCall(14656);const I32 flags = is_lvalue_sub();
	  SensorCall(14659);if (flags && !(flags & OPpENTERSUB_INARGS)) {
	    SensorCall(14657);if (gimme != G_ARRAY)
		{/*121*/SensorCall(14658);goto croak_cant_return;/*122*/}
	    SETs(sv);
	    RETURN;
	  }
	}
	else {/*123*/SensorCall(14660);if (PL_op->op_flags & OPf_MOD
		&& PL_op->op_private & OPpLVAL_INTRO)
	    {/*125*/SensorCall(14661);Perl_croak(aTHX_ "%s", PL_no_localize_ref);/*126*/}/*124*/}/*120*/}
    }
    else {
	SensorCall(14663);if (SvTYPE(sv) == type) {
	    SensorCall(14664);if (PL_op->op_flags & OPf_REF) {
		SETs(sv);
		RETURN;
	    }
	    else {/*127*/SensorCall(14665);if (LVRET) {
		SensorCall(14666);if (gimme != G_ARRAY)
		    {/*129*/SensorCall(14667);goto croak_cant_return;/*130*/}
		SETs(sv);
		RETURN;
	    ;/*128*/}}
	}
	else {
	    SensorCall(14668);GV *gv;
	
	    SensorCall(14672);if (!isGV_with_GP(sv)) {
		SensorCall(14669);gv = Perl_softref2xv(aTHX_ sv, is_pp_rv2av ? an_array : a_hash,
				     type, &sp);
		SensorCall(14670);if (!gv)
		    RETURN;
	    }
	    else {
		SensorCall(14671);gv = MUTABLE_GV(sv);
	    }
	    SensorCall(14673);sv = is_pp_rv2av ? MUTABLE_SV(GvAVn(gv)) : MUTABLE_SV(GvHVn(gv));
	    SensorCall(14674);if (PL_op->op_private & OPpLVAL_INTRO)
		sv = is_pp_rv2av ? MUTABLE_SV(save_ary(gv)) : MUTABLE_SV(save_hash(gv));
	    SensorCall(14680);if (PL_op->op_flags & OPf_REF) {
		SETs(sv);
		RETURN;
	    }
	    else {/*131*/SensorCall(14675);if (PL_op->op_private & OPpMAYBE_LVSUB) {
	      SensorCall(14676);const I32 flags = is_lvalue_sub();
	      SensorCall(14679);if (flags && !(flags & OPpENTERSUB_INARGS)) {
		SensorCall(14677);if (gimme != G_ARRAY)
		    {/*133*/SensorCall(14678);goto croak_cant_return;/*134*/}
		SETs(sv);
		RETURN;
	      }
	    ;/*132*/}}
	}
    }

    SensorCall(14695);if (is_pp_rv2av) {
	SensorCall(14682);AV *const av = MUTABLE_AV(sv);
	/* The guts of pp_rv2av, with no intending change to preserve history
	   (until such time as we get tools that can do blame annotation across
	   whitespace changes.  */
	SensorCall(14690);if (gimme == G_ARRAY) {
	    SensorCall(14683);const I32 maxarg = AvFILL(av) + 1;
	    (void)POPs;			/* XXXX May be optimized away? */
	    EXTEND(SP, maxarg);
	    SensorCall(14687);if (SvRMAGICAL(av)) {
		SensorCall(14684);U32 i;
		SensorCall(14686);for (i=0; i < (U32)maxarg; i++) {
		    SensorCall(14685);SV ** const svp = av_fetch(av, i, FALSE);
		    /* See note in pp_helem, and bug id #27839 */
		    SP[i+1] = svp
			? SvGMAGICAL(*svp) ? (mg_get(*svp), *svp) : *svp
			: &PL_sv_undef;
		}
	    }
	    else {
		Copy(AvARRAY(av), SP+1, maxarg, SV*);
	    }
	    SP += maxarg;
	}
	else {/*135*/SensorCall(14688);if (gimme == G_SCALAR) {
	    dTARGET;
	    SensorCall(14689);const I32 maxarg = AvFILL(av) + 1;
	    SETi(maxarg);
	;/*136*/}}
    } else {
	/* The guts of pp_rv2hv  */
	SensorCall(14691);if (gimme == G_ARRAY) { /* array wanted */
	    SensorCall(14692);*PL_stack_sp = sv;
	    {OP * ReplaceReturn2090 = Perl_do_kv(aTHX); SensorCall(14693); return ReplaceReturn2090;}
	}
	else {/*137*/SensorCall(14694);if (gimme == G_SCALAR) {
	    dTARGET;
	    TARG = Perl_hv_scalar(aTHX_ MUTABLE_HV(sv));
	    SPAGAIN;
	    SETTARG;
	;/*138*/}}
    }
    RETURN;

 croak_cant_return:
    SensorCall(14696);Perl_croak(aTHX_ "Can't return %s to lvalue scalar context",
	       is_pp_rv2av ? "array" : "hash");
    RETURN;
}

STATIC void
S_do_oddball(pTHX_ HV *hash, SV **relem, SV **firstrelem)
{
SensorCall(14697);    dVAR;

    PERL_ARGS_ASSERT_DO_ODDBALL;

    SensorCall(14709);if (*relem) {
	SensorCall(14698);SV *tmpstr;
        const HE *didstore;

        SensorCall(14704);if (ckWARN(WARN_MISC)) {
	    SensorCall(14699);const char *err;
	    SensorCall(14702);if (relem == firstrelem &&
		SvROK(*relem) &&
		(SvTYPE(SvRV(*relem)) == SVt_PVAV ||
		 SvTYPE(SvRV(*relem)) == SVt_PVHV))
	    {
		SensorCall(14700);err = "Reference found where even-sized list expected";
	    }
	    else
		{/*31*/SensorCall(14701);err = "Odd number of elements in hash assignment";/*32*/}
	    SensorCall(14703);Perl_warner(aTHX_ packWARN(WARN_MISC), "%s", err);
	}

        SensorCall(14705);tmpstr = newSV(0);
        didstore = hv_store_ent(hash,*relem,tmpstr,0);
        SensorCall(14708);if (SvMAGICAL(hash)) {
            SensorCall(14706);if (SvSMAGICAL(tmpstr))
                mg_set(tmpstr);
            SensorCall(14707);if (!didstore)
                sv_2mortal(tmpstr);
        }
        TAINT_NOT;
    }
SensorCall(14710);}

PP(pp_aassign)
{
SensorCall(14711);    dVAR; dSP;
    SV **lastlelem = PL_stack_sp;
    SV **lastrelem = PL_stack_base + POPMARK;
    SV **firstrelem = PL_stack_base + POPMARK + 1;
    SV **firstlelem = lastrelem + 1;

    register SV **relem;
    register SV **lelem;

    register SV *sv;
    register AV *ary;

    I32 gimme;
    HV *hash;
    I32 i;
    int magic;
    int duplicates = 0;
    SV **firsthashrelem = NULL;	/* "= 0" keeps gcc 2.95 quiet  */

    PL_delaymagic = DM_DELAY;		/* catch simultaneous items */
    gimme = GIMME_V;

    /* If there's a common identifier on both sides we have to take
     * special care that assigning the identifier on the left doesn't
     * clobber a value on the right that's used later in the list.
     * Don't bother if LHS is just an empty hash or array.
     */

    SensorCall(14717);if (    (PL_op->op_private & OPpASSIGN_COMMON)
	&&  (
	       firstlelem != lastlelem
	    || ! ((sv = *firstlelem))
	    || SvMAGICAL(sv)
	    || ! (SvTYPE(sv) == SVt_PVAV || SvTYPE(sv) == SVt_PVHV)
	    || (SvTYPE(sv) == SVt_PVAV && AvFILL((AV*)sv) != -1)
	    || (SvTYPE(sv) == SVt_PVHV && HvUSEDKEYS((HV*)sv) != 0)
	    )
    ) {
	EXTEND_MORTAL(lastrelem - firstrelem + 1);
	SensorCall(14716);for (relem = firstrelem; relem <= lastrelem; relem++) {
	    SensorCall(14712);if ((sv = *relem)) {
		TAINT_NOT;	/* Each item is independent */

		/* Dear TODO test in t/op/sort.t, I love you.
		   (It's relying on a panic, not a "semi-panic" from newSVsv()
		   and then an assertion failure below.)  */
		SensorCall(14714);if (SvIS_FREED(sv)) {
		    SensorCall(14713);Perl_croak(aTHX_ "panic: attempt to copy freed scalar %p",
			       (void*)sv);
		}
		/* Specifically *not* sv_mortalcopy(), as that will steal TEMPs,
		   and we need a second copy of a temp here.  */
		SensorCall(14715);*relem = sv_2mortal(newSVsv(sv));
	    }
	}
    }

    SensorCall(14718);relem = firstrelem;
    lelem = firstlelem;
    ary = NULL;
    hash = NULL;

    SensorCall(14755);while (lelem <= lastlelem) {
	TAINT_NOT;		/* Each item stands on its own, taintwise. */
	SensorCall(14719);sv = *lelem++;
	SensorCall(14754);switch (SvTYPE(sv)) {
	case SVt_PVAV:
	    SensorCall(14720);ary = MUTABLE_AV(sv);
	    magic = SvMAGICAL(ary) != 0;
	    ENTER;
	    SAVEFREESV(SvREFCNT_inc_simple_NN(sv));
	    av_clear(ary);
	    av_extend(ary, lastrelem - relem);
	    i = 0;
	    SensorCall(14725);while (relem <= lastrelem) {	/* gobble up all the rest */
		SensorCall(14721);SV **didstore;
		assert(*relem);
		sv = newSV(0);
		sv_setsv(sv, *relem);
		*(relem++) = sv;
		didstore = av_store(ary,i++,sv);
		SensorCall(14724);if (magic) {
		    SensorCall(14722);if (SvSMAGICAL(sv))
			mg_set(sv);
		    SensorCall(14723);if (!didstore)
			sv_2mortal(sv);
		}
		TAINT_NOT;
	    }
	    SensorCall(14726);if (PL_delaymagic & DM_ARRAY_ISA)
		SvSETMAGIC(MUTABLE_SV(ary));
	    LEAVE;
	    SensorCall(14727);break;
	case SVt_PVHV: {				/* normal hash */
		SensorCall(14728);SV *tmpstr;
		SV** topelem = relem;

		hash = MUTABLE_HV(sv);
		magic = SvMAGICAL(hash) != 0;
		ENTER;
		SAVEFREESV(SvREFCNT_inc_simple_NN(sv));
		hv_clear(hash);
		firsthashrelem = relem;

		SensorCall(14741);while (relem < lastrelem) {	/* gobble up all the rest */
		    SensorCall(14729);HE *didstore;
		    sv = *relem ? *relem : &PL_sv_no;
		    relem++;
		    tmpstr = newSV(0);
		    SensorCall(14730);if (*relem)
			sv_setsv(tmpstr,*relem);	/* value */
		    SensorCall(14731);relem++;
		    SensorCall(14736);if (gimme != G_VOID) {
			SensorCall(14732);if (hv_exists_ent(hash, sv, 0))
			    /* key overwrites an existing entry */
			    {/*37*/SensorCall(14733);duplicates += 2;/*38*/}
			else
			{/*39*/SensorCall(14734);if (gimme == G_ARRAY) {
			    /* copy element back: possibly to an earlier
			     * stack location if we encountered dups earlier */
			    SensorCall(14735);*topelem++ = sv;
			    *topelem++ = tmpstr;
			;/*40*/}}
		    }
		    SensorCall(14737);didstore = hv_store_ent(hash,sv,tmpstr,0);
		    SensorCall(14740);if (magic) {
			SensorCall(14738);if (SvSMAGICAL(tmpstr))
			    mg_set(tmpstr);
			SensorCall(14739);if (!didstore)
			    sv_2mortal(tmpstr);
		    }
		    TAINT_NOT;
		}
		SensorCall(14743);if (relem == lastrelem) {
		    do_oddball(hash, relem, firstrelem);
		    SensorCall(14742);relem++;
		}
		LEAVE;
	    }
	    SensorCall(14744);break;
	default:
	    SensorCall(14745);if (SvIMMORTAL(sv)) {
		SensorCall(14746);if (relem <= lastrelem)
		    {/*41*/SensorCall(14747);relem++;/*42*/}
		SensorCall(14748);break;
	    }
	    SensorCall(14752);if (relem <= lastrelem) {
		SensorCall(14749);if (
		  SvTEMP(sv) && !SvSMAGICAL(sv) && SvREFCNT(sv) == 1 &&
		  (!isGV_with_GP(sv) || SvFAKE(sv)) && ckWARN(WARN_MISC)
		)
		    {/*43*/SensorCall(14750);Perl_warner(aTHX_
		       packWARN(WARN_MISC),
		      "Useless assignment to a temporary"
		    );/*44*/}
		sv_setsv(sv, *relem);
		SensorCall(14751);*(relem++) = sv;
	    }
	    else
		sv_setsv(sv, &PL_sv_undef);
	    SvSETMAGIC(sv);
	    SensorCall(14753);break;
	}
    }
    SensorCall(14761);if (PL_delaymagic & ~DM_DELAY) {
	/* Will be used to set PL_tainting below */
	SensorCall(14756);UV tmp_uid  = PerlProc_getuid();
	UV tmp_euid = PerlProc_geteuid();
	UV tmp_gid  = PerlProc_getgid();
	UV tmp_egid = PerlProc_getegid();

	SensorCall(14758);if (PL_delaymagic & DM_UID) {
#ifdef HAS_SETRESUID
	    SensorCall(14757);(void)setresuid((PL_delaymagic & DM_RUID) ? PL_delaymagic_uid  : (Uid_t)-1,
			    (PL_delaymagic & DM_EUID) ? PL_delaymagic_euid : (Uid_t)-1,
			    (Uid_t)-1);
#else
#  ifdef HAS_SETREUID
	    (void)setreuid((PL_delaymagic & DM_RUID) ? PL_delaymagic_uid  : (Uid_t)-1,
			   (PL_delaymagic & DM_EUID) ? PL_delaymagic_euid : (Uid_t)-1);
#  else
#    ifdef HAS_SETRUID
	    if ((PL_delaymagic & DM_UID) == DM_RUID) {
		(void)setruid(PL_delaymagic_uid);
		PL_delaymagic &= ~DM_RUID;
	    }
#    endif /* HAS_SETRUID */
#    ifdef HAS_SETEUID
	    if ((PL_delaymagic & DM_UID) == DM_EUID) {
		(void)seteuid(PL_delaymagic_euid);
		PL_delaymagic &= ~DM_EUID;
	    }
#    endif /* HAS_SETEUID */
	    if (PL_delaymagic & DM_UID) {
		if (PL_delaymagic_uid != PL_delaymagic_euid)
		    DIE(aTHX_ "No setreuid available");
		(void)PerlProc_setuid(PL_delaymagic_uid);
	    }
#  endif /* HAS_SETREUID */
#endif /* HAS_SETRESUID */
	    tmp_uid  = PerlProc_getuid();
	    tmp_euid = PerlProc_geteuid();
	}
	SensorCall(14760);if (PL_delaymagic & DM_GID) {
#ifdef HAS_SETRESGID
	    SensorCall(14759);(void)setresgid((PL_delaymagic & DM_RGID) ? PL_delaymagic_gid  : (Gid_t)-1,
			    (PL_delaymagic & DM_EGID) ? PL_delaymagic_egid : (Gid_t)-1,
			    (Gid_t)-1);
#else
#  ifdef HAS_SETREGID
	    (void)setregid((PL_delaymagic & DM_RGID) ? PL_delaymagic_gid  : (Gid_t)-1,
			   (PL_delaymagic & DM_EGID) ? PL_delaymagic_egid : (Gid_t)-1);
#  else
#    ifdef HAS_SETRGID
	    if ((PL_delaymagic & DM_GID) == DM_RGID) {
		(void)setrgid(PL_delaymagic_gid);
		PL_delaymagic &= ~DM_RGID;
	    }
#    endif /* HAS_SETRGID */
#    ifdef HAS_SETEGID
	    if ((PL_delaymagic & DM_GID) == DM_EGID) {
		(void)setegid(PL_delaymagic_egid);
		PL_delaymagic &= ~DM_EGID;
	    }
#    endif /* HAS_SETEGID */
	    if (PL_delaymagic & DM_GID) {
		if (PL_delaymagic_gid != PL_delaymagic_egid)
		    DIE(aTHX_ "No setregid available");
		(void)PerlProc_setgid(PL_delaymagic_gid);
	    }
#  endif /* HAS_SETREGID */
#endif /* HAS_SETRESGID */
	    tmp_gid  = PerlProc_getgid();
	    tmp_egid = PerlProc_getegid();
	}
	PL_tainting |= (tmp_uid && (tmp_euid != tmp_uid || tmp_egid != tmp_gid));
    }
    PL_delaymagic = 0;

    SensorCall(14771);if (gimme == G_VOID)
	SP = firstrelem - 1;
    else {/*45*/SensorCall(14762);if (gimme == G_SCALAR) {
	dTARGET;
	SP = firstrelem;
	SETi(lastrelem - firstrelem + 1 - duplicates);
    }
    else {
	SensorCall(14763);if (ary)
	    SP = lastrelem;
	else {/*47*/SensorCall(14764);if (hash) {
	    SensorCall(14765);if (duplicates) {
		/* at this point we have removed the duplicate key/value
		 * pairs from the stack, but the remaining values may be
		 * wrong; i.e. with (a 1 a 2 b 3) on the stack we've removed
		 * the (a 2), but the stack now probably contains
		 * (a <freed> b 3), because { hv_save(a,1); hv_save(a,2) }
		 * obliterates the earlier key. So refresh all values. */
		SensorCall(14766);lastrelem -= duplicates;
		relem = firsthashrelem;
		SensorCall(14768);while (relem < lastrelem) {
		    SensorCall(14767);HE *he;
		    sv = *relem++;
		    he = hv_fetch_ent(hash, sv, 0, 0);
		    *relem++ = (he ? HeVAL(he) : &PL_sv_undef);
		}
	    }
	    SP = lastrelem;
	}
	else
	    SP = firstrelem + (lastlelem - firstlelem);/*48*/}
	SensorCall(14769);lelem = firstlelem + (relem - firstrelem);
	SensorCall(14770);while (relem <= SP)
	    *relem++ = (lelem <= lastlelem) ? *lelem++ : &PL_sv_undef;
    ;/*46*/}}

    RETURN;
}

PP(pp_qr)
{
SensorCall(14772);    dVAR; dSP;
    register PMOP * const pm = cPMOP;
    REGEXP * rx = PM_GETRE(pm);
    SV * const pkg = rx ? CALLREG_PACKAGE(rx) : NULL;
    SV * const rv = sv_newmortal();

    SvUPGRADE(rv, SVt_IV);
    /* For a subroutine describing itself as "This is a hacky workaround" I'm
       loathe to use it here, but it seems to be the right fix. Or close.
       The key part appears to be that it's essential for pp_qr to return a new
       object (SV), which implies that there needs to be an effective way to
       generate a new SV from the existing SV that is pre-compiled in the
       optree.  */
    SvRV_set(rv, MUTABLE_SV(reg_temp_copy(NULL, rx)));
    SvROK_on(rv);

    SensorCall(14774);if (pkg) {
	SensorCall(14773);HV *const stash = gv_stashsv(pkg, GV_ADD);
	SvREFCNT_dec(pkg);
	(void)sv_bless(rv, stash);
    }

    SensorCall(14775);if (RX_EXTFLAGS(rx) & RXf_TAINTED) {
        SvTAINTED_on(rv);
        SvTAINTED_on(SvRV(rv));
    }
    XPUSHs(rv);
    RETURN;
}

PP(pp_match)
{
SensorCall(14776);    dVAR; dSP; dTARG;
    register PMOP *pm = cPMOP;
    PMOP *dynpm = pm;
    register const char *t;
    register const char *s;
    const char *strend;
    I32 global;
    U8 r_flags = REXEC_CHECKED;
    const char *truebase;			/* Start of string  */
    register REGEXP *rx = PM_GETRE(pm);
    bool rxtainted;
    const I32 gimme = GIMME;
    STRLEN len;
    I32 minmatch = 0;
    const I32 oldsave = PL_savestack_ix;
    I32 update_minmatch = 1;
    I32 had_zerolen = 0;
    U32 gpos = 0;

    SensorCall(14778);if (PL_op->op_flags & OPf_STACKED)
	TARG = POPs;
    else {/*73*/SensorCall(14777);if (PL_op->op_private & OPpTARGET_MY)
	GETTARGET;
    else {
	TARG = DEFSV;
	EXTEND(SP,1);
    ;/*74*/}}

    PUTBACK;				/* EVAL blocks need stack_sp. */
    /* Skip get-magic if this is a qr// clone, because regcomp has
       already done it. */
    SensorCall(14779);s = ((struct regexp *)SvANY(rx))->mother_re
	 ? SvPV_nomg_const(TARG, len)
	 : SvPV_const(TARG, len);
    SensorCall(14780);if (!s)
	DIE(aTHX_ "panic: pp_match");
    SensorCall(14781);strend = s + len;
    rxtainted = ((RX_EXTFLAGS(rx) & RXf_TAINTED) ||
		 (PL_tainted && (pm->op_pmflags & PMf_RETAINT)));
    TAINT_NOT;

    RX_MATCH_UTF8_set(rx, DO_UTF8(TARG));

    /* PMdf_USED is set after a ?? matches once */
    SensorCall(14783);if (
#ifdef USE_ITHREADS
        SvREADONLY(PL_regex_pad[pm->op_pmoffset])
#else
        pm->op_pmflags & PMf_USED
#endif
    ) {
      failure:
	SensorCall(14782);if (gimme == G_ARRAY)
	    RETURN;
	RETPUSHNO;
    }



    /* empty pattern special-cased to use last successful pattern if possible */
    SensorCall(14785);if (!RX_PRELEN(rx) && PL_curpm) {
	SensorCall(14784);pm = PL_curpm;
	rx = PM_GETRE(pm);
    }

    SensorCall(14787);if (RX_MINLEN(rx) > (I32)len)
	{/*75*/SensorCall(14786);goto failure;/*76*/}

    SensorCall(14788);truebase = t = s;

    /* XXXX What part of this is needed with true \G-support? */
    SensorCall(14798);if ((global = dynpm->op_pmflags & PMf_GLOBAL)) {
	RX_OFFS(rx)[0].start = -1;
	SensorCall(14797);if (SvTYPE(TARG) >= SVt_PVMG && SvMAGIC(TARG)) {
	    SensorCall(14789);MAGIC* const mg = mg_find(TARG, PERL_MAGIC_regex_global);
	    SensorCall(14796);if (mg && mg->mg_len >= 0) {
		SensorCall(14790);if (!(RX_EXTFLAGS(rx) & RXf_GPOS_SEEN))
		    RX_OFFS(rx)[0].end = RX_OFFS(rx)[0].start = mg->mg_len;
		else {/*77*/SensorCall(14791);if (RX_EXTFLAGS(rx) & RXf_ANCH_GPOS) {
		    SensorCall(14792);r_flags |= REXEC_IGNOREPOS;
		    RX_OFFS(rx)[0].end = RX_OFFS(rx)[0].start = mg->mg_len;
		} else {/*79*/SensorCall(14793);if (RX_EXTFLAGS(rx) & RXf_GPOS_FLOAT) 
		    {/*81*/SensorCall(14794);gpos = mg->mg_len;/*82*/}
		else 
		    RX_OFFS(rx)[0].end = RX_OFFS(rx)[0].start = mg->mg_len;/*80*/}/*78*/}
		SensorCall(14795);minmatch = (mg->mg_flags & MGf_MINMATCH) ? RX_GOFS(rx) + 1 : 0;
		update_minmatch = 0;
	    }
	}
    }
    /* XXX: comment out !global get safe $1 vars after a
       match, BUT be aware that this leads to dramatic slowdowns on
       /g matches against large strings.  So far a solution to this problem
       appears to be quite tricky.
       Test for the unsafe vars are TODO for now. */
    SensorCall(14799);if (       (!global && RX_NPARENS(rx))
	    || SvTEMP(TARG) || SvAMAGIC(TARG) || PL_sawampersand
	    || (RX_EXTFLAGS(rx) & (RXf_EVAL_SEEN|RXf_PMf_KEEPCOPY)))
	r_flags |= REXEC_COPY_STR;
    SensorCall(14800);if (SvSCREAM(TARG))
	r_flags |= REXEC_SCREAM;

  play_it_again:
    SensorCall(14806);if (global && RX_OFFS(rx)[0].start != -1) {
	SensorCall(14801);t = s = RX_OFFS(rx)[0].end + truebase - RX_GOFS(rx);
	SensorCall(14803);if ((s + RX_MINLEN(rx)) > strend || s < truebase)
	    {/*83*/SensorCall(14802);goto nope;/*84*/}
	SensorCall(14805);if (update_minmatch++)
	    {/*85*/SensorCall(14804);minmatch = had_zerolen;/*86*/}
    }
    SensorCall(14812);if (RX_EXTFLAGS(rx) & RXf_USE_INTUIT &&
	DO_UTF8(TARG) == (RX_UTF8(rx) != 0)) {
	/* FIXME - can PL_bostr be made const char *?  */
	PL_bostr = (char *)truebase;
	SensorCall(14807);s = CALLREG_INTUIT_START(rx, TARG, (char *)s, (char *)strend, r_flags, NULL);

	SensorCall(14809);if (!s)
	    {/*87*/SensorCall(14808);goto nope;/*88*/}
	SensorCall(14811);if ( (RX_EXTFLAGS(rx) & RXf_CHECK_ALL)
	     && !PL_sawampersand
	     && !(RX_EXTFLAGS(rx) & RXf_PMf_KEEPCOPY)
	     && ((RX_EXTFLAGS(rx) & RXf_NOSCAN)
		 || !((RX_EXTFLAGS(rx) & RXf_INTUIT_TAIL)
		      && (r_flags & REXEC_SCREAM)))
	     && !SvROK(TARG))	/* Cannot trust since INTUIT cannot guess ^ */
	    {/*89*/SensorCall(14810);goto yup;/*90*/}
    }
    SensorCall(14814);if (!CALLREGEXEC(rx, (char*)s, (char *)strend, (char*)truebase,
		     minmatch, TARG, NUM2PTR(void*, gpos), r_flags))
	{/*91*/SensorCall(14813);goto ret_no;/*92*/}

    PL_curpm = pm;
    SensorCall(14815);if (dynpm->op_pmflags & PMf_ONCE) {
#ifdef USE_ITHREADS
	SvREADONLY_on(PL_regex_pad[dynpm->op_pmoffset]);
#else
	dynpm->op_pmflags |= PMf_USED;
#endif
    }

  gotcha:
    SensorCall(14816);if (rxtainted)
	RX_MATCH_TAINTED_on(rx);
    TAINT_IF(RX_MATCH_TAINTED(rx));
    SensorCall(14842);if (gimme == G_ARRAY) {
	SensorCall(14817);const I32 nparens = RX_NPARENS(rx);
	I32 i = (global && !nparens) ? 1 : 0;

	SPAGAIN;			/* EVAL blocks could move the stack. */
	EXTEND(SP, nparens + i);
	EXTEND_MORTAL(nparens + i);
	SensorCall(14822);for (i = !i; i <= nparens; i++) {
	    PUSHs(sv_newmortal());
	    SensorCall(14821);if ((RX_OFFS(rx)[i].start != -1) && RX_OFFS(rx)[i].end != -1 ) {
		SensorCall(14818);const I32 len = RX_OFFS(rx)[i].end - RX_OFFS(rx)[i].start;
		s = RX_OFFS(rx)[i].start + truebase;
	        SensorCall(14819);if (RX_OFFS(rx)[i].end < 0 || RX_OFFS(rx)[i].start < 0 ||
		    len < 0 || len > strend - s)
		    DIE(aTHX_ "panic: pp_match start/end pointers, i=%ld, "
			"start=%ld, end=%ld, s=%p, strend=%p, len=%"UVuf,
			(long) i, (long) RX_OFFS(rx)[i].start,
			(long)RX_OFFS(rx)[i].end, s, strend, (UV) len);
		sv_setpvn(*SP, s, len);
		SensorCall(14820);if (DO_UTF8(TARG) && is_utf8_string((U8*)s, len))
		    SvUTF8_on(*SP);
	    }
	}
	SensorCall(14833);if (global) {
	    SensorCall(14823);if (dynpm->op_pmflags & PMf_CONTINUE) {
		SensorCall(14824);MAGIC* mg = NULL;
		SensorCall(14825);if (SvTYPE(TARG) >= SVt_PVMG && SvMAGIC(TARG))
		    mg = mg_find(TARG, PERL_MAGIC_regex_global);
		SensorCall(14827);if (!mg) {
#ifdef PERL_OLD_COPY_ON_WRITE
		    if (SvIsCOW(TARG))
			sv_force_normal_flags(TARG, 0);
#endif
		    SensorCall(14826);mg = sv_magicext(TARG, NULL, PERL_MAGIC_regex_global,
				     &PL_vtbl_mglob, NULL, 0);
		}
		SensorCall(14830);if (RX_OFFS(rx)[0].start != -1) {
		    SensorCall(14828);mg->mg_len = RX_OFFS(rx)[0].end;
		    SensorCall(14829);if (RX_OFFS(rx)[0].start + RX_GOFS(rx) == (UV)RX_OFFS(rx)[0].end)
			mg->mg_flags |= MGf_MINMATCH;
		    else
			mg->mg_flags &= ~MGf_MINMATCH;
		}
	    }
	    SensorCall(14831);had_zerolen = (RX_OFFS(rx)[0].start != -1
			   && (RX_OFFS(rx)[0].start + RX_GOFS(rx)
			       == (UV)RX_OFFS(rx)[0].end));
	    PUTBACK;			/* EVAL blocks may use stack */
	    r_flags |= REXEC_IGNOREPOS | REXEC_NOT_FIRST;
	    SensorCall(14832);goto play_it_again;
	}
	else if (!nparens)
	    XPUSHs(&PL_sv_yes);
	LEAVE_SCOPE(oldsave);
	RETURN;
    }
    else {
	SensorCall(14834);if (global) {
	    SensorCall(14835);MAGIC* mg;
	    SensorCall(14836);if (SvTYPE(TARG) >= SVt_PVMG && SvMAGIC(TARG))
		mg = mg_find(TARG, PERL_MAGIC_regex_global);
	    else
		mg = NULL;
	    SensorCall(14838);if (!mg) {
#ifdef PERL_OLD_COPY_ON_WRITE
		if (SvIsCOW(TARG))
		    sv_force_normal_flags(TARG, 0);
#endif
		SensorCall(14837);mg = sv_magicext(TARG, NULL, PERL_MAGIC_regex_global,
				 &PL_vtbl_mglob, NULL, 0);
	    }
	    SensorCall(14841);if (RX_OFFS(rx)[0].start != -1) {
		SensorCall(14839);mg->mg_len = RX_OFFS(rx)[0].end;
		SensorCall(14840);if (RX_OFFS(rx)[0].start + RX_GOFS(rx) == (UV)RX_OFFS(rx)[0].end)
		    mg->mg_flags |= MGf_MINMATCH;
		else
		    mg->mg_flags &= ~MGf_MINMATCH;
	    }
	}
	LEAVE_SCOPE(oldsave);
	RETPUSHYES;
    }

yup:					/* Confirmed by INTUIT */
    SensorCall(14843);if (rxtainted)
	RX_MATCH_TAINTED_on(rx);
    TAINT_IF(RX_MATCH_TAINTED(rx));
    PL_curpm = pm;
    SensorCall(14844);if (dynpm->op_pmflags & PMf_ONCE) {
#ifdef USE_ITHREADS
        SvREADONLY_on(PL_regex_pad[dynpm->op_pmoffset]);
#else
        dynpm->op_pmflags |= PMf_USED;
#endif
    }
    SensorCall(14845);if (RX_MATCH_COPIED(rx))
	Safefree(RX_SUBBEG(rx));
    RX_MATCH_COPIED_off(rx);
    RX_SUBBEG(rx) = NULL;
    SensorCall(14849);if (global) {
	/* FIXME - should rx->subbeg be const char *?  */
	RX_SUBBEG(rx) = (char *) truebase;
	RX_OFFS(rx)[0].start = s - truebase;
	SensorCall(14847);if (RX_MATCH_UTF8(rx)) {
	    SensorCall(14846);char * const t = (char*)utf8_hop((U8*)s, RX_MINLENRET(rx));
	    RX_OFFS(rx)[0].end = t - truebase;
	}
	else {
	    RX_OFFS(rx)[0].end = s - truebase + RX_MINLENRET(rx);
	}
	RX_SUBLEN(rx) = strend - truebase;
	SensorCall(14848);goto gotcha;
    }
    SensorCall(14851);if (PL_sawampersand || RX_EXTFLAGS(rx) & RXf_PMf_KEEPCOPY) {
	SensorCall(14850);I32 off;
#ifdef PERL_OLD_COPY_ON_WRITE
	if (SvIsCOW(TARG) || (SvFLAGS(TARG) & CAN_COW_MASK) == CAN_COW_FLAGS) {
	    if (DEBUG_C_TEST) {
		PerlIO_printf(Perl_debug_log,
			      "Copy on write: pp_match $& capture, type %d, truebase=%p, t=%p, difference %d\n",
			      (int) SvTYPE(TARG), (void*)truebase, (void*)t,
			      (int)(t-truebase));
	    }
	    RX_SAVED_COPY(rx) = sv_setsv_cow(RX_SAVED_COPY(rx), TARG);
	    RX_SUBBEG(rx)
		= (char *) SvPVX_const(RX_SAVED_COPY(rx)) + (t - truebase);
	    assert (SvPOKp(RX_SAVED_COPY(rx)));
	} else
#endif
	{

	    RX_SUBBEG(rx) = savepvn(t, strend - t);
#ifdef PERL_OLD_COPY_ON_WRITE
	    RX_SAVED_COPY(rx) = NULL;
#endif
	}
	RX_SUBLEN(rx) = strend - t;
	RX_MATCH_COPIED_on(rx);
	off = RX_OFFS(rx)[0].start = s - t;
	RX_OFFS(rx)[0].end = off + RX_MINLENRET(rx);
    }
    else {			/* startp/endp are used by @- @+. */
	RX_OFFS(rx)[0].start = s - truebase;
	RX_OFFS(rx)[0].end = s - truebase + RX_MINLENRET(rx);
    }
    /* including RX_NPARENS(rx) in the below code seems highly suspicious.
       -dmq */
    RX_NPARENS(rx) = RX_LASTPAREN(rx) = RX_LASTCLOSEPAREN(rx) = 0;	/* used by @-, @+, and $^N */
    LEAVE_SCOPE(oldsave);
    RETPUSHYES;

nope:
ret_no:
    SensorCall(14856);if (global && !(dynpm->op_pmflags & PMf_CONTINUE)) {
	SensorCall(14852);if (SvTYPE(TARG) >= SVt_PVMG && SvMAGIC(TARG)) {
	    SensorCall(14853);MAGIC* const mg = mg_find(TARG, PERL_MAGIC_regex_global);
	    SensorCall(14855);if (mg)
		{/*93*/SensorCall(14854);mg->mg_len = -1;/*94*/}
	}
    }
    LEAVE_SCOPE(oldsave);
    SensorCall(14857);if (gimme == G_ARRAY)
	RETURN;
    RETPUSHNO;
}

OP *
Perl_do_readline(pTHX)
{
SensorCall(14858);    dVAR; dSP; dTARGETSTACKED;
    register SV *sv;
    STRLEN tmplen = 0;
    STRLEN offset;
    PerlIO *fp;
    register IO * const io = GvIO(PL_last_in_gv);
    register const I32 type = PL_op->op_type;
    const I32 gimme = GIMME_V;

    SensorCall(14864);if (io) {
	SensorCall(14859);const MAGIC *const mg = SvTIED_mg((const SV *)io, PERL_MAGIC_tiedscalar);
	SensorCall(14863);if (mg) {
	    SensorCall(14860);Perl_tied_method(aTHX_ "READLINE", SP, MUTABLE_SV(io), mg, gimme, 0);
	    SensorCall(14861);if (gimme == G_SCALAR) {
		SPAGAIN;
		SvSetSV_nosteal(TARG, TOPs);
		SETTARG;
	    }
	    {OP * ReplaceReturn2089 = NORMAL; SensorCall(14862); return ReplaceReturn2089;}
	}
    }
    SensorCall(14865);fp = NULL;
    SensorCall(14880);if (io) {
	SensorCall(14866);fp = IoIFP(io);
	SensorCall(14879);if (!fp) {
	    SensorCall(14867);if (IoFLAGS(io) & IOf_ARGV) {
		SensorCall(14868);if (IoFLAGS(io) & IOf_START) {
		    IoLINES(io) = 0;
		    SensorCall(14871);if (av_len(GvAVn(PL_last_in_gv)) < 0) {
			IoFLAGS(io) &= ~IOf_START;
			do_open(PL_last_in_gv,"-",1,FALSE,O_RDONLY,0,NULL);
			sv_setpvs(GvSVn(PL_last_in_gv), "-");
			SvSETMAGIC(GvSV(PL_last_in_gv));
			SensorCall(14869);fp = IoIFP(io);
			SensorCall(14870);goto have_fp;
		    }
		}
		SensorCall(14872);fp = nextargv(PL_last_in_gv);
		SensorCall(14874);if (!fp) { /* Note: fp != IoIFP(io) */
		    SensorCall(14873);(void)do_close(PL_last_in_gv, FALSE); /* now it does*/
		}
	    }
	    else {/*1*/SensorCall(14875);if (type == OP_GLOB)
		{/*3*/SensorCall(14876);fp = Perl_start_glob(aTHX_ POPs, io);/*4*/}/*2*/}
	}
	else {/*5*/SensorCall(14877);if (type == OP_GLOB)
	    SP--;
	else {/*7*/SensorCall(14878);if (IoTYPE(io) == IoTYPE_WRONLY) {
	    report_wrongway_fh(PL_last_in_gv, '>');
	;/*8*/}/*6*/}}
    }
    SensorCall(14886);if (!fp) {
	SensorCall(14881);if ((!io || !(IoFLAGS(io) & IOf_START))
	    && ckWARN2(WARN_GLOB, WARN_CLOSED))
	{
	    SensorCall(14882);if (type == OP_GLOB)
		{/*9*/SensorCall(14883);Perl_ck_warner_d(aTHX_ packWARN(WARN_GLOB),
			    "glob failed (can't start child: %s)",
			    Strerror(errno));/*10*/}
	    else
		report_evil_fh(PL_last_in_gv);
	}
	SensorCall(14885);if (gimme == G_SCALAR) {
	    /* undef TARG, and push that undefined value */
	    SensorCall(14884);if (type != OP_RCATLINE) {
		SV_CHECK_THINKFIRST_COW_DROP(TARG);
		SvOK_off(TARG);
	    }
	    PUSHTARG;
	}
	RETURN;
    }
  have_fp:
    SensorCall(14899);if (gimme == G_SCALAR) {
	SensorCall(14887);sv = TARG;
	SensorCall(14888);if (type == OP_RCATLINE && SvGMAGICAL(sv))
	    mg_get(sv);
	SensorCall(14891);if (SvROK(sv)) {
	    SensorCall(14889);if (type == OP_RCATLINE)
		SvPV_force_nomg_nolen(sv);
	    else
		sv_unref(sv);
	}
	else {/*11*/SensorCall(14890);if (isGV_with_GP(sv)) {
	    SvPV_force_nomg_nolen(sv);
	;/*12*/}}
	SvUPGRADE(sv, SVt_PV);
	SensorCall(14892);tmplen = SvLEN(sv);	/* remember if already alloced */
	SensorCall(14893);if (!tmplen && !SvREADONLY(sv)) {
            /* try short-buffering it. Please update t/op/readline.t
	     * if you change the growth length.
	     */
	    Sv_Grow(sv, 80);
        }
	SensorCall(14894);offset = 0;
	SensorCall(14897);if (type == OP_RCATLINE && SvOK(sv)) {
	    SensorCall(14895);if (!SvPOK(sv)) {
		SvPV_force_nomg_nolen(sv);
	    }
	    SensorCall(14896);offset = SvCUR(sv);
	}
    }
    else {
	SensorCall(14898);sv = sv_2mortal(newSV(80));
	offset = 0;
    }

    /* This should not be marked tainted if the fp is marked clean */
#define MAYBE_TAINT_LINE(io, sv) \
    if (!(IoFLAGS(io) & IOf_UNTAINT)) { \
	TAINT;				\
	SvTAINTED_on(sv);		\
    }

/* delay EOF state for a snarfed empty file */
#define SNARF_EOF(gimme,rs,io,sv) \
    (gimme != G_SCALAR || SvCUR(sv)					\
     || (IoFLAGS(io) & IOf_NOLINE) || !RsSNARF(rs))

    SensorCall(14935);for (;;) {
	PUTBACK;
	SensorCall(14911);if (!sv_gets(sv, fp, offset)
	    && (type == OP_GLOB
		|| SNARF_EOF(gimme, PL_rs, io, sv)
		|| PerlIO_error(fp)))
	{
	    PerlIO_clearerr(fp);
	    SensorCall(14907);if (IoFLAGS(io) & IOf_ARGV) {
		SensorCall(14900);fp = nextargv(PL_last_in_gv);
		SensorCall(14902);if (fp)
		    {/*13*/SensorCall(14901);continue;/*14*/}
		SensorCall(14903);(void)do_close(PL_last_in_gv, FALSE);
	    }
	    else {/*15*/SensorCall(14904);if (type == OP_GLOB) {
		SensorCall(14905);if (!do_close(PL_last_in_gv, FALSE)) {
		    SensorCall(14906);Perl_ck_warner(aTHX_ packWARN(WARN_GLOB),
				   "glob failed (child exited with status %d%s)",
				   (int)(STATUS_CURRENT >> 8),
				   (STATUS_CURRENT & 0x80) ? ", core dumped" : "");
		}
	    ;/*16*/}}
	    SensorCall(14909);if (gimme == G_SCALAR) {
		SensorCall(14908);if (type != OP_RCATLINE) {
		    SV_CHECK_THINKFIRST_COW_DROP(TARG);
		    SvOK_off(TARG);
		}
		SPAGAIN;
		PUSHTARG;
	    }
	    MAYBE_TAINT_LINE(io, sv);
	    RETURN;
	}
	MAYBE_TAINT_LINE(io, sv);
	IoLINES(io)++;
	IoFLAGS(io) |= IOf_NOLINE;
	SvSETMAGIC(sv);
	SPAGAIN;
	XPUSHs(sv);
	SensorCall(14928);if (type == OP_GLOB) {
	    SensorCall(14912);const char *t1;

	    SensorCall(14916);if (SvCUR(sv) > 0 && SvCUR(PL_rs) > 0) {
		SensorCall(14913);char * const tmps = SvEND(sv) - 1;
		SensorCall(14915);if (*tmps == *SvPVX_const(PL_rs)) {
		    SensorCall(14914);*tmps = '\0';
		    SvCUR_set(sv, SvCUR(sv) - 1);
		}
	    }
	    SensorCall(14919);for (t1 = SvPVX_const(sv); *t1; t1++)
		{/*17*/SensorCall(14917);if (!isALPHA(*t1) && !isDIGIT(*t1) &&
		    strchr("$&*(){}[]'\";\\|?<>~`", *t1))
			{/*19*/SensorCall(14918);break;/*20*/}/*18*/}
	    SensorCall(14922);if (*t1 && PerlLIO_lstat(SvPVX_const(sv), &PL_statbuf) < 0) {
		SensorCall(14920);(void)POPs;		/* Unmatched wildcard?  Chuck it... */
		SensorCall(14921);continue;
	    }
	} else {/*21*/SensorCall(14923);if (SvUTF8(sv)) { /* OP_READLINE, OP_RCATLINE */
	     SensorCall(14924);if (ckWARN(WARN_UTF8)) {
		SensorCall(14925);const U8 * const s = (const U8*)SvPVX_const(sv) + offset;
		const STRLEN len = SvCUR(sv) - offset;
		const U8 *f;

		SensorCall(14927);if (!is_utf8_string_loc(s, len, &f))
		    /* Emulate :encoding(utf8) warning in the same case. */
		    {/*23*/SensorCall(14926);Perl_warner(aTHX_ packWARN(WARN_UTF8),
				"utf8 \"\\x%02X\" does not map to Unicode",
				f < (U8*)SvEND(sv) ? *f : 0);/*24*/}
	     }
	;/*22*/}}
	SensorCall(14934);if (gimme == G_ARRAY) {
	    SensorCall(14929);if (SvLEN(sv) - SvCUR(sv) > 20) {
		SvPV_shrink_to_cur(sv);
	    }
	    SensorCall(14930);sv = sv_2mortal(newSV(80));
	    SensorCall(14931);continue;
	}
	else {/*25*/SensorCall(14932);if (gimme == G_SCALAR && !tmplen && SvLEN(sv) - SvCUR(sv) > 80) {
	    /* try to reclaim a bit of scalar space (only on 1st alloc) */
	    SensorCall(14933);const STRLEN new_len
		= SvCUR(sv) < 60 ? 80 : SvCUR(sv)+40; /* allow some slop */
	    SvPV_renew(sv, new_len);
	;/*26*/}}
	RETURN;
    }
SensorCall(14936);}

PP(pp_helem)
{
SensorCall(14937);    dVAR; dSP;
    HE* he;
    SV **svp;
    SV * const keysv = POPs;
    HV * const hv = MUTABLE_HV(POPs);
    const U32 lval = PL_op->op_flags & OPf_MOD || LVRET;
    const U32 defer = PL_op->op_private & OPpLVAL_DEFER;
    SV *sv;
    const U32 hash = (SvIsCOW_shared_hash(keysv)) ? SvSHARED_HASH(keysv) : 0;
    const bool localizing = PL_op->op_private & OPpLVAL_INTRO;
    bool preeminent = TRUE;

    SensorCall(14938);if (SvTYPE(hv) != SVt_PVHV)
	RETPUSHUNDEF;

    SensorCall(14941);if (localizing) {
	SensorCall(14939);MAGIC *mg;
	HV *stash;

	/* If we can determine whether the element exist,
	 * Try to preserve the existenceness of a tied hash
	 * element by using EXISTS and DELETE if possible.
	 * Fallback to FETCH and STORE otherwise. */
	SensorCall(14940);if (SvCANEXISTDELETE(hv) || mg_find((const SV *)hv, PERL_MAGIC_env))
	    preeminent = hv_exists_ent(hv, keysv, 0);
    }

    SensorCall(14942);he = hv_fetch_ent(hv, keysv, lval && !defer, hash);
    svp = he ? &HeVAL(he) : NULL;
    SensorCall(14950);if (lval) {
	SensorCall(14943);if (!svp || !*svp || *svp == &PL_sv_undef) {
	    SensorCall(14944);SV* lv;
	    SV* key2;
	    SensorCall(14945);if (!defer) {
		DIE(aTHX_ PL_no_helem_sv, SVfARG(keysv));
	    }
	    SensorCall(14946);lv = sv_newmortal();
	    sv_upgrade(lv, SVt_PVLV);
	    LvTYPE(lv) = 'y';
	    sv_magic(lv, key2 = newSVsv(keysv), PERL_MAGIC_defelem, NULL, 0);
	    SvREFCNT_dec(key2);	/* sv_magic() increments refcount */
	    LvTARG(lv) = SvREFCNT_inc_simple(hv);
	    LvTARGLEN(lv) = 1;
	    PUSHs(lv);
	    RETURN;
	}
	SensorCall(14949);if (localizing) {
	    SensorCall(14947);if (HvNAME_get(hv) && isGV(*svp))
		save_gp(MUTABLE_GV(*svp), !(PL_op->op_flags & OPf_SPECIAL));
	    else if (preeminent)
		save_helem_flags(hv, keysv, svp,
		     (PL_op->op_flags & OPf_SPECIAL) ? 0 : SAVEf_SETMAGIC);
	    else
		SAVEHDELETE(hv, keysv);
	}
	else {/*65*/SensorCall(14948);if (PL_op->op_private & OPpDEREF) {
	    PUSHs(vivify_ref(*svp, PL_op->op_private & OPpDEREF));
	    RETURN;
	;/*66*/}}
    }
    SensorCall(14951);sv = (svp && *svp ? *svp : &PL_sv_undef);
    /* Originally this did a conditional C<sv = sv_mortalcopy(sv)>; this
     * was to make C<local $tied{foo} = $tied{foo}> possible.
     * However, it seems no longer to be needed for that purpose, and
     * introduced a new bug: stuff like C<while ($hash{taintedval} =~ /.../g>
     * would loop endlessly since the pos magic is getting set on the
     * mortal copy and lost. However, the copy has the effect of
     * triggering the get magic, and losing it altogether made things like
     * c<$tied{foo};> in void context no longer do get magic, which some
     * code relied on. Also, delayed triggering of magic on @+ and friends
     * meant the original regex may be out of scope by now. So as a
     * compromise, do the get magic here. (The MGf_GSKIP flag will stop it
     * being called too many times). */
    SensorCall(14952);if (!lval && SvRMAGICAL(hv) && SvGMAGICAL(sv))
	mg_get(sv);
    PUSHs(sv);
    RETURN;
}

PP(pp_iter)
{
SensorCall(14953);    dVAR; dSP;
    register PERL_CONTEXT *cx;
    SV *sv, *oldsv;
    SV **itersvp;
    AV *av = NULL; /* used for LOOP_FOR on arrays and the stack */
    bool av_is_stack = FALSE;

    EXTEND(SP, 1);
    cx = &cxstack[cxstack_ix];
    SensorCall(14954);if (!CxTYPE_is_LOOP(cx))
	DIE(aTHX_ "panic: pp_iter, type=%u", CxTYPE(cx));

    SensorCall(14955);itersvp = CxITERVAR(cx);
    SensorCall(14967);if (CxTYPE(cx) == CXt_LOOP_LAZYSV) {
	    /* string increment */
	    SensorCall(14956);SV* cur = cx->blk_loop.state_u.lazysv.cur;
	    SV *end = cx->blk_loop.state_u.lazysv.end;
	    /* If the maximum is !SvOK(), pp_enteriter substitutes PL_sv_no.
	       It has SvPVX of "" and SvCUR of 0, which is what we want.  */
	    STRLEN maxlen = 0;
	    const char *max = SvPV_const(end, maxlen);
	    SensorCall(14960);if (!SvNIOK(cur) && SvCUR(cur) <= maxlen) {
		SensorCall(14957);if (SvREFCNT(*itersvp) == 1 && !SvMAGICAL(*itersvp)) {
		    /* safe to reuse old SV */
		    sv_setsv(*itersvp, cur);
		}
		else
		{
		    /* we need a fresh SV every time so that loop body sees a
		     * completely new SV for closures/references to work as
		     * they used to */
		    SensorCall(14958);oldsv = *itersvp;
		    *itersvp = newSVsv(cur);
		    SvREFCNT_dec(oldsv);
		}
		SensorCall(14959);if (strEQ(SvPVX_const(cur), max))
		    sv_setiv(cur, 0); /* terminate next time */
		else
		    sv_inc(cur);
		RETPUSHYES;
	    }
	    RETPUSHNO;
    }
    else {/*67*/SensorCall(14961);if (CxTYPE(cx) == CXt_LOOP_LAZYIV) {
	/* integer increment */
	SensorCall(14962);if (cx->blk_loop.state_u.lazyiv.cur > cx->blk_loop.state_u.lazyiv.end)
	    RETPUSHNO;

	/* don't risk potential race */
	SensorCall(14964);if (SvREFCNT(*itersvp) == 1 && !SvMAGICAL(*itersvp)) {
	    /* safe to reuse old SV */
	    sv_setiv(*itersvp, cx->blk_loop.state_u.lazyiv.cur++);
	}
	else
	{
	    /* we need a fresh SV every time so that loop body sees a
	     * completely new SV for closures/references to work as they
	     * used to */
	    SensorCall(14963);oldsv = *itersvp;
	    *itersvp = newSViv(cx->blk_loop.state_u.lazyiv.cur++);
	    SvREFCNT_dec(oldsv);
	}

	/* Handle end of range at IV_MAX */
	SensorCall(14966);if ((cx->blk_loop.state_u.lazyiv.cur == IV_MIN) &&
	    (cx->blk_loop.state_u.lazyiv.end == IV_MAX))
	{
	    SensorCall(14965);cx->blk_loop.state_u.lazyiv.cur++;
	    cx->blk_loop.state_u.lazyiv.end++;
	}

	RETPUSHYES;
    ;/*68*/}}

    /* iterate array */
    assert(CxTYPE(cx) == CXt_LOOP_FOR);
    SensorCall(14968);av = cx->blk_loop.state_u.ary.ary;
    SensorCall(14970);if (!av) {
	SensorCall(14969);av_is_stack = TRUE;
	av = PL_curstack;
    }
    SensorCall(14979);if (PL_op->op_private & OPpITER_REVERSED) {
	SensorCall(14971);if (cx->blk_loop.state_u.ary.ix <= (av_is_stack
				    ? cx->blk_loop.resetsp + 1 : 0))
	    RETPUSHNO;

	SensorCall(14974);if (SvMAGICAL(av) || AvREIFY(av)) {
	    SensorCall(14972);SV * const * const svp = av_fetch(av, --cx->blk_loop.state_u.ary.ix, FALSE);
	    sv = svp ? *svp : NULL;
	}
	else {
	    SensorCall(14973);sv = AvARRAY(av)[--cx->blk_loop.state_u.ary.ix];
	}
    }
    else {
	SensorCall(14975);if (cx->blk_loop.state_u.ary.ix >= (av_is_stack ? cx->blk_oldsp :
				    AvFILL(av)))
	    RETPUSHNO;

	SensorCall(14978);if (SvMAGICAL(av) || AvREIFY(av)) {
	    SensorCall(14976);SV * const * const svp = av_fetch(av, ++cx->blk_loop.state_u.ary.ix, FALSE);
	    sv = svp ? *svp : NULL;
	}
	else {
	    SensorCall(14977);sv = AvARRAY(av)[++cx->blk_loop.state_u.ary.ix];
	}
    }

    SensorCall(14981);if (sv && SvIS_FREED(sv)) {
	SensorCall(14980);*itersvp = NULL;
	Perl_croak(aTHX_ "Use of freed value in iteration");
    }

    SensorCall(14982);if (sv) {
	SvTEMP_off(sv);
	SvREFCNT_inc_simple_void_NN(sv);
    }
    else
	sv = &PL_sv_undef;
    SensorCall(14984);if (!av_is_stack && sv == &PL_sv_undef) {
	SensorCall(14983);SV *lv = newSV_type(SVt_PVLV);
	LvTYPE(lv) = 'y';
	sv_magic(lv, NULL, PERL_MAGIC_defelem, NULL, 0);
	LvTARG(lv) = SvREFCNT_inc_simple(av);
	LvTARGOFF(lv) = cx->blk_loop.state_u.ary.ix;
	LvTARGLEN(lv) = (STRLEN)UV_MAX;
	sv = lv;
    }

    SensorCall(14985);oldsv = *itersvp;
    *itersvp = sv;
    SvREFCNT_dec(oldsv);

    RETPUSHYES;
}

/*
A description of how taint works in pattern matching and substitution.

While the pattern is being assembled/concatenated and then compiled,
PL_tainted will get set if any component of the pattern is tainted, e.g.
/.*$tainted/.  At the end of pattern compilation, the RXf_TAINTED flag
is set on the pattern if PL_tainted is set.

When the pattern is copied, e.g. $r = qr/..../, the SV holding the ref to
the pattern is marked as tainted. This means that subsequent usage, such
as /x$r/, will set PL_tainted, and thus RXf_TAINTED, on the new pattern too.

During execution of a pattern, locale-variant ops such as ALNUML set the
local flag RF_tainted. At the end of execution, the engine sets the
RXf_TAINTED_SEEN on the pattern if RF_tainted got set, or clears it
otherwise.

In addition, RXf_TAINTED_SEEN is used post-execution by the get magic code
of $1 et al to indicate whether the returned value should be tainted.
It is the responsibility of the caller of the pattern (i.e. pp_match,
pp_subst etc) to set this flag for any other circumstances where $1 needs
to be tainted.

The taint behaviour of pp_subst (and pp_substcont) is quite complex.

There are three possible sources of taint
    * the source string
    * the pattern (both compile- and run-time, RXf_TAINTED / RXf_TAINTED_SEEN)
    * the replacement string (or expression under /e)
    
There are four destinations of taint and they are affected by the sources
according to the rules below:

    * the return value (not including /r):
	tainted by the source string and pattern, but only for the
	number-of-iterations case; boolean returns aren't tainted;
    * the modified string (or modified copy under /r):
	tainted by the source string, pattern, and replacement strings;
    * $1 et al:
	tainted by the pattern, and under 'use re "taint"', by the source
	string too;
    * PL_taint - i.e. whether subsequent code (e.g. in a /e block) is tainted:
	should always be unset before executing subsequent code.

The overall action of pp_subst is:

    * at the start, set bits in rxtainted indicating the taint status of
	the various sources.

    * After each pattern execution, update the SUBST_TAINT_PAT bit in
	rxtainted if RXf_TAINTED_SEEN has been set, to indicate that the
	pattern has subsequently become tainted via locale ops.

    * If control is being passed to pp_substcont to execute a /e block,
	save rxtainted in the CXt_SUBST block, for future use by
	pp_substcont.

    * Whenever control is being returned to perl code (either by falling
	off the "end" of pp_subst/pp_substcont, or by entering a /e block),
	use the flag bits in rxtainted to make all the appropriate types of
	destination taint visible; e.g. set RXf_TAINTED_SEEN so that $1
	et al will appear tainted.

pp_match is just a simpler version of the above.

*/

PP(pp_subst)
{
SensorCall(14986);    dVAR; dSP; dTARG;
    register PMOP *pm = cPMOP;
    PMOP *rpm = pm;
    register char *s;
    char *strend;
    register char *m;
    const char *c;
    register char *d;
    STRLEN clen;
    I32 iters = 0;
    I32 maxiters;
    register I32 i;
    bool once;
    U8 rxtainted = 0; /* holds various SUBST_TAINT_* flag bits.
			See "how taint works" above */
    char *orig;
    U8 r_flags;
    register REGEXP *rx = PM_GETRE(pm);
    STRLEN len;
    int force_on_match = 0;
    const I32 oldsave = PL_savestack_ix;
    STRLEN slen;
    bool doutf8 = FALSE;
#ifdef PERL_OLD_COPY_ON_WRITE
    bool is_cow;
#endif
    SV *nsv = NULL;
    /* known replacement string? */
    register SV *dstr = (pm->op_pmflags & PMf_CONST) ? POPs : NULL;

    PERL_ASYNC_CHECK();

    SensorCall(14988);if (PL_op->op_flags & OPf_STACKED)
	TARG = POPs;
    else {/*141*/SensorCall(14987);if (PL_op->op_private & OPpTARGET_MY)
	GETTARGET;
    else {
	TARG = DEFSV;
	EXTEND(SP,1);
    ;/*142*/}}

#ifdef PERL_OLD_COPY_ON_WRITE
    /* Awooga. Awooga. "bool" types that are actually char are dangerous,
       because they make integers such as 256 "false".  */
    is_cow = SvIsCOW(TARG) ? TRUE : FALSE;
#else
    SensorCall(14989);if (SvIsCOW(TARG))
	sv_force_normal_flags(TARG,0);
#endif
    SensorCall(14991);if (!(rpm->op_pmflags & PMf_NONDESTRUCT)
#ifdef PERL_OLD_COPY_ON_WRITE
	&& !is_cow
#endif
	&& (SvREADONLY(TARG)
	    || ( ((SvTYPE(TARG) == SVt_PVGV && isGV_with_GP(TARG))
		  || SvTYPE(TARG) > SVt_PVLV)
		 && !(SvTYPE(TARG) == SVt_PVGV && SvFAKE(TARG)))))
	{/*143*/SensorCall(14990);Perl_croak_no_modify(aTHX);/*144*/}
    PUTBACK;

  setup_match:
    SensorCall(14992);s = SvPV_mutable(TARG, len);
    SensorCall(14994);if (!SvPOKp(TARG) || SvTYPE(TARG) == SVt_PVGV)
	{/*145*/SensorCall(14993);force_on_match = 1;/*146*/}

    /* only replace once? */
    SensorCall(14995);once = !(rpm->op_pmflags & PMf_GLOBAL);

    /* See "how taint works" above */
    SensorCall(14997);if (PL_tainting) {
	SensorCall(14996);rxtainted  = (
	    (SvTAINTED(TARG) ? SUBST_TAINT_STR : 0)
	  | ((RX_EXTFLAGS(rx) & RXf_TAINTED) ? SUBST_TAINT_PAT : 0)
	  | ((pm->op_pmflags & PMf_RETAINT) ? SUBST_TAINT_RETAINT : 0)
	  | ((once && !(rpm->op_pmflags & PMf_NONDESTRUCT))
		? SUBST_TAINT_BOOLRET : 0));
	TAINT_NOT;
    }

    RX_MATCH_UTF8_set(rx, DO_UTF8(TARG));

  force_it:
    SensorCall(14998);if (!pm || !s)
	DIE(aTHX_ "panic: pp_subst, pm=%p, s=%p", pm, s);

    SensorCall(14999);strend = s + len;
    slen = RX_MATCH_UTF8(rx) ? utf8_length((U8*)s, (U8*)strend) : len;
    maxiters = 2 * slen + 10;	/* We can match twice at each
				   position, once with zero-length,
				   second time with non-zero. */

    SensorCall(15001);if (!RX_PRELEN(rx) && PL_curpm) {
	SensorCall(15000);pm = PL_curpm;
	rx = PM_GETRE(pm);
    }
    SensorCall(15002);r_flags = (RX_NPARENS(rx) || SvTEMP(TARG) || PL_sawampersand
	    || (RX_EXTFLAGS(rx) & (RXf_EVAL_SEEN|RXf_PMf_KEEPCOPY)) )
	       ? REXEC_COPY_STR : 0;
    SensorCall(15003);if (SvSCREAM(TARG))
	r_flags |= REXEC_SCREAM;

    SensorCall(15004);orig = m = s;
    SensorCall(15008);if (RX_EXTFLAGS(rx) & RXf_USE_INTUIT) {
	PL_bostr = orig;
	SensorCall(15005);s = CALLREG_INTUIT_START(rx, TARG, s, strend, r_flags, NULL);

	SensorCall(15007);if (!s)
	    {/*147*/SensorCall(15006);goto ret_no;/*148*/}
	/* How to do it in subst? */
/*	if ( (RX_EXTFLAGS(rx) & RXf_CHECK_ALL)
	     && !PL_sawampersand
	     && !(RX_EXTFLAGS(rx) & RXf_KEEPCOPY)
	     && ((RX_EXTFLAGS(rx) & RXf_NOSCAN)
		 || !((RX_EXTFLAGS(rx) & RXf_INTUIT_TAIL)
		      && (r_flags & REXEC_SCREAM))))
	    goto yup;
*/
    }

    SensorCall(15009);if (!CALLREGEXEC(rx, s, strend, orig, 0, TARG, NULL,
			 r_flags | REXEC_CHECKED))
    {
      ret_no:
	SPAGAIN;
	PUSHs(rpm->op_pmflags & PMf_NONDESTRUCT ? TARG : &PL_sv_no);
	LEAVE_SCOPE(oldsave);
	RETURN;
    }

    /* known replacement string? */
    SensorCall(15021);if (dstr) {
	SensorCall(15010);if (SvTAINTED(dstr))
	    rxtainted |= SUBST_TAINT_REPL;

	/* Upgrade the source if the replacement is utf8 but the source is not,
	 * but only if it matched; see
	 * http://www.nntp.perl.org/group/perl.perl5.porters/2010/04/msg158809.html
	 */
	SensorCall(15014);if (DO_UTF8(dstr) && ! DO_UTF8(TARG)) {
	    SensorCall(15011);char * const orig_pvx =  SvPVX(TARG);
	    const STRLEN new_len = sv_utf8_upgrade_nomg(TARG);

	    /* If the lengths are the same, the pattern contains only
	     * invariants, can keep going; otherwise, various internal markers
	     * could be off, so redo */
	    SensorCall(15013);if (new_len != len || orig_pvx != SvPVX(TARG)) {
		SensorCall(15012);goto setup_match;
	    }
	}

	/* replacement needing upgrading? */
	SensorCall(15019);if (DO_UTF8(TARG) && !doutf8) {
	     SensorCall(15015);nsv = sv_newmortal();
	     SvSetSV(nsv, dstr);
	     SensorCall(15016);if (PL_encoding)
		  sv_recode_to_utf8(nsv, PL_encoding);
	     else
		  sv_utf8_upgrade(nsv);
	     SensorCall(15017);c = SvPV_const(nsv, clen);
	     doutf8 = TRUE;
	}
	else {
	    SensorCall(15018);c = SvPV_const(dstr, clen);
	    doutf8 = DO_UTF8(dstr);
	}
    }
    else {
	SensorCall(15020);c = NULL;
	doutf8 = FALSE;
    }
    
    /* can do inplace substitution? */
    SensorCall(15077);if (c
#ifdef PERL_OLD_COPY_ON_WRITE
	&& !is_cow
#endif
	&& (I32)clen <= RX_MINLENRET(rx) && (once || !(r_flags & REXEC_COPY_STR))
	&& !(RX_EXTFLAGS(rx) & RXf_LOOKBEHIND_SEEN)
	&& (!doutf8 || SvUTF8(TARG))
	&& !(rpm->op_pmflags & PMf_NONDESTRUCT))
    {

#ifdef PERL_OLD_COPY_ON_WRITE
	if (SvIsCOW(TARG)) {
	    assert (!force_on_match);
	    goto have_a_cow;
	}
#endif
	SensorCall(15022);if (force_on_match) {
	    SensorCall(15023);force_on_match = 0;
	    s = SvPV_force(TARG, len);
	    SensorCall(15024);goto force_it;
	}
	SensorCall(15025);d = s;
	PL_curpm = pm;
	SvSCREAM_off(TARG);	/* disable possible screamer */
	SensorCall(15052);if (once) {
	    SensorCall(15026);if (RX_MATCH_TAINTED(rx)) /* run time pattern taint, eg locale */
		rxtainted |= SUBST_TAINT_PAT;
	    SensorCall(15027);m = orig + RX_OFFS(rx)[0].start;
	    d = orig + RX_OFFS(rx)[0].end;
	    s = orig;
	    SensorCall(15039);if (m - s > strend - d) {  /* faster to shorten from end */
		SensorCall(15028);if (clen) {
		    Copy(c, m, clen, char);
		    SensorCall(15029);m += clen;
		}
		SensorCall(15030);i = strend - d;
		SensorCall(15032);if (i > 0) {
		    Move(d, m, i, char);
		    SensorCall(15031);m += i;
		}
		SensorCall(15033);*m = '\0';
		SvCUR_set(TARG, m - s);
	    }
	    else {/*149*/SensorCall(15034);if ((i = m - s)) {	/* faster from front */
		SensorCall(15035);d -= clen;
		m = d;
		Move(s, d - i, i, char);
		sv_chop(TARG, d-i);
		SensorCall(15036);if (clen)
		    Copy(c, m, clen, char);
	    }
	    else {/*151*/SensorCall(15037);if (clen) {
		SensorCall(15038);d -= clen;
		sv_chop(TARG, d);
		Copy(c, d, clen, char);
	    }
	    else {
		sv_chop(TARG, d);
	    ;/*152*/}/*150*/}}
	    SPAGAIN;
	    PUSHs(&PL_sv_yes);
	}
	else {
	    SensorCall(15040);do {
		SensorCall(15041);if (iters++ > maxiters)
		    DIE(aTHX_ "Substitution loop");
		SensorCall(15042);if (RX_MATCH_TAINTED(rx)) /* run time pattern taint, eg locale */
		    rxtainted |= SUBST_TAINT_PAT;
		SensorCall(15043);m = RX_OFFS(rx)[0].start + orig;
		SensorCall(15046);if ((i = m - s)) {
		    SensorCall(15044);if (s != d)
			Move(s, d, i, char);
		    SensorCall(15045);d += i;
		}
		SensorCall(15048);if (clen) {
		    Copy(c, d, clen, char);
		    SensorCall(15047);d += clen;
		}
		SensorCall(15049);s = RX_OFFS(rx)[0].end + orig;
	    } while (CALLREGEXEC(rx, s, strend, orig, s == m,
				 TARG, NULL,
				 /* don't match same null twice */
				 REXEC_NOT_FIRST|REXEC_IGNOREPOS));
	    SensorCall(15051);if (s != d) {
		SensorCall(15050);i = strend - s;
		SvCUR_set(TARG, d - SvPVX_const(TARG) + i);
		Move(s, d, i+1, char);		/* include the NUL */
	    }
	    SPAGAIN;
	    mPUSHi((I32)iters);
	}
    }
    else {
	SensorCall(15053);if (force_on_match) {
	    SensorCall(15054);force_on_match = 0;
	    SensorCall(15055);if (rpm->op_pmflags & PMf_NONDESTRUCT) {
		/* I feel that it should be possible to avoid this mortal copy
		   given that the code below copies into a new destination.
		   However, I suspect it isn't worth the complexity of
		   unravelling the C<goto force_it> for the small number of
		   cases where it would be viable to drop into the copy code. */
		TARG = sv_2mortal(newSVsv(TARG));
	    }
	    SensorCall(15056);s = SvPV_force(TARG, len);
	    SensorCall(15057);goto force_it;
	}
#ifdef PERL_OLD_COPY_ON_WRITE
      have_a_cow:
#endif
	SensorCall(15058);if (RX_MATCH_TAINTED(rx)) /* run time pattern taint, eg locale */
	    rxtainted |= SUBST_TAINT_PAT;
	SensorCall(15059);dstr = newSVpvn_flags(m, s-m, SVs_TEMP | (DO_UTF8(TARG) ? SVf_UTF8 : 0));
	PL_curpm = pm;
	SensorCall(15061);if (!c) {
	    SensorCall(15060);register PERL_CONTEXT *cx;
	    SPAGAIN;
	    /* note that a whole bunch of local vars are saved here for
	     * use by pp_substcont: here's a list of them in case you're
	     * searching for places in this sub that uses a particular var:
	     * iters maxiters r_flags oldsave rxtainted orig dstr targ
	     * s m strend rx once */
	    PUSHSUBST(cx);
	    RETURNOP(cPMOP->op_pmreplrootu.op_pmreplroot);
	}
	SensorCall(15062);r_flags |= REXEC_IGNOREPOS | REXEC_NOT_FIRST;
	SensorCall(15073);do {
	    SensorCall(15063);if (iters++ > maxiters)
		DIE(aTHX_ "Substitution loop");
	    SensorCall(15064);if (RX_MATCH_TAINTED(rx))
		rxtainted |= SUBST_TAINT_PAT;
	    SensorCall(15066);if (RX_MATCH_COPIED(rx) && RX_SUBBEG(rx) != orig) {
		SensorCall(15065);m = s;
		s = orig;
		orig = RX_SUBBEG(rx);
		s = orig + (m - s);
		strend = s + (strend - m);
	    }
	    SensorCall(15067);m = RX_OFFS(rx)[0].start + orig;
	    SensorCall(15068);if (doutf8 && !SvUTF8(dstr))
		sv_catpvn_utf8_upgrade(dstr, s, m - s, nsv);
            else
		sv_catpvn(dstr, s, m-s);
	    SensorCall(15069);s = RX_OFFS(rx)[0].end + orig;
	    SensorCall(15070);if (clen)
		sv_catpvn(dstr, c, clen);
	    SensorCall(15072);if (once)
		{/*153*/SensorCall(15071);break;/*154*/}
	} while (CALLREGEXEC(rx, s, strend, orig, s == m,
			     TARG, NULL, r_flags));
	SensorCall(15074);if (doutf8 && !DO_UTF8(TARG))
	    sv_catpvn_utf8_upgrade(dstr, s, strend - s, nsv);
	else
	    sv_catpvn(dstr, s, strend - s);

	SensorCall(15076);if (rpm->op_pmflags & PMf_NONDESTRUCT) {
	    /* From here on down we're using the copy, and leaving the original
	       untouched.  */
	    TARG = dstr;
	    SPAGAIN;
	    PUSHs(dstr);
	} else {
#ifdef PERL_OLD_COPY_ON_WRITE
	    /* The match may make the string COW. If so, brilliant, because
	       that's just saved us one malloc, copy and free - the regexp has
	       donated the old buffer, and we malloc an entirely new one, rather
	       than the regexp malloc()ing a buffer and copying our original,
	       only for us to throw it away here during the substitution.  */
	    if (SvIsCOW(TARG)) {
		sv_force_normal_flags(TARG, SV_COW_DROP_PV);
	    } else
#endif
	    {
		SvPV_free(TARG);
	    }
	    SvPV_set(TARG, SvPVX(dstr));
	    SvCUR_set(TARG, SvCUR(dstr));
	    SvLEN_set(TARG, SvLEN(dstr));
	    SensorCall(15075);doutf8 |= DO_UTF8(dstr);
	    SvPV_set(dstr, NULL);

	    SPAGAIN;
	    mPUSHi((I32)iters);
	}
    }

    SensorCall(15080);if (!(rpm->op_pmflags & PMf_NONDESTRUCT)) {
	SensorCall(15078);(void)SvPOK_only_UTF8(TARG);
	SensorCall(15079);if (doutf8)
	    SvUTF8_on(TARG);
    }

    /* See "how taint works" above */
    SensorCall(15084);if (PL_tainting) {
	SensorCall(15081);if ((rxtainted & SUBST_TAINT_PAT) ||
	    ((rxtainted & (SUBST_TAINT_STR|SUBST_TAINT_RETAINT)) ==
				(SUBST_TAINT_STR|SUBST_TAINT_RETAINT))
	)
	    {/*155*/SensorCall(15082);(RX_MATCH_TAINTED_on(rx));/*156*/} /* taint $1 et al */

	SensorCall(15083);if (!(rxtainted & SUBST_TAINT_BOOLRET)
	    && (rxtainted & (SUBST_TAINT_STR|SUBST_TAINT_PAT))
	)
	    SvTAINTED_on(TOPs);  /* taint return value */
	else
	    SvTAINTED_off(TOPs);  /* may have got tainted earlier */

	/* needed for mg_set below */
	PL_tainted =
	  cBOOL(rxtainted & (SUBST_TAINT_STR|SUBST_TAINT_PAT|SUBST_TAINT_REPL));
	SvTAINT(TARG);
    }
    SvSETMAGIC(TARG); /* PL_tainted must be correctly set for this mg_set */
    TAINT_NOT;
    LEAVE_SCOPE(oldsave);
    RETURN;
}

PP(pp_grepwhile)
{
SensorCall(15085);    dVAR; dSP;

    SensorCall(15086);if (SvTRUEx(POPs))
	PL_stack_base[PL_markstack_ptr[-1]++] = PL_stack_base[*PL_markstack_ptr];
    SensorCall(15087);++*PL_markstack_ptr;
    FREETMPS;
    LEAVE_with_name("grep_item");					/* exit inner scope */

    /* All done yet? */
    SensorCall(15095);if (PL_stack_base + *PL_markstack_ptr > SP) {
	SensorCall(15088);I32 items;
	const I32 gimme = GIMME_V;

	LEAVE_with_name("grep");					/* exit outer scope */
	(void)POPMARK;				/* pop src */
	items = --*PL_markstack_ptr - PL_markstack_ptr[-1];
	(void)POPMARK;				/* pop dst */
	SP = PL_stack_base + POPMARK;		/* pop original mark */
	SensorCall(15092);if (gimme == G_SCALAR) {
	    SensorCall(15089);if (PL_op->op_private & OPpGREP_LEX) {
		SensorCall(15090);SV* const sv = sv_newmortal();
		sv_setiv(sv, items);
		PUSHs(sv);
	    }
	    else {
		dTARGET;
		XPUSHi(items);
	    }
	}
	else {/*63*/SensorCall(15091);if (gimme == G_ARRAY)
	    SP += items;/*64*/}
	RETURN;
    }
    else {
	SensorCall(15093);SV *src;

	ENTER_with_name("grep_item");					/* enter inner scope */
	SAVEVPTR(PL_curpm);

	src = PL_stack_base[*PL_markstack_ptr];
	SvTEMP_off(src);
	SensorCall(15094);if (PL_op->op_private & OPpGREP_LEX)
	    PAD_SVl(PL_op->op_targ) = src;
	else
	    DEFSV_set(src);

	RETURNOP(cLOGOP->op_other);
    }
SensorCall(15096);}

PP(pp_leavesub)
{
SensorCall(15097);    dVAR; dSP;
    SV **mark;
    SV **newsp;
    PMOP *newpm;
    I32 gimme;
    register PERL_CONTEXT *cx;
    SV *sv;

    SensorCall(15099);if (CxMULTICALL(&cxstack[cxstack_ix]))
	{/*69*/{OP * ReplaceReturn2088 = 0; SensorCall(15098); return ReplaceReturn2088;}/*70*/}

SensorCall(15100);    POPBLOCK(cx,newpm);
    cxstack_ix++; /* temporarily protect top context */

    TAINT_NOT;
    SensorCall(15111);if (gimme == G_SCALAR) {
	MARK = newsp + 1;
	SensorCall(15106);if (MARK <= SP) {
	    SensorCall(15101);if (cx->blk_sub.cv && CvDEPTH(cx->blk_sub.cv) > 1) {
		SensorCall(15102);if (SvTEMP(TOPs) && SvREFCNT(TOPs) == 1
		     && !SvMAGICAL(TOPs)) {
		    SensorCall(15103);*MARK = SvREFCNT_inc(TOPs);
		    FREETMPS;
		    sv_2mortal(*MARK);
		}
		else {
		    SensorCall(15104);sv = SvREFCNT_inc(TOPs);	/* FREETMPS could clobber it */
		    FREETMPS;
		    *MARK = sv_mortalcopy(sv);
		    SvREFCNT_dec(sv);
		}
	    }
	    else if (SvTEMP(TOPs) && SvREFCNT(TOPs) == 1
		     && !SvMAGICAL(TOPs)) {
		*MARK = TOPs;
	    }
	    else
		*MARK = sv_mortalcopy(TOPs);
	}
	else {
	    MEXTEND(MARK, 0);
	    SensorCall(15105);*MARK = &PL_sv_undef;
	}
	SP = MARK;
    }
    else {/*71*/SensorCall(15107);if (gimme == G_ARRAY) {
	SensorCall(15108);for (MARK = newsp + 1; MARK <= SP; MARK++) {
	    SensorCall(15109);if (!SvTEMP(*MARK) || SvREFCNT(*MARK) != 1
		 || SvMAGICAL(*MARK)) {
		SensorCall(15110);*MARK = sv_mortalcopy(*MARK);
		TAINT_NOT;	/* Each item is independent */
	    }
	}
    ;/*72*/}}
    PUTBACK;

    LEAVE;
    cxstack_ix--;
    POPSUB(cx,sv);	/* Stack values are safe: release CV and @_ ... */
    PL_curpm = newpm;	/* ... and pop $1 et al */

    LEAVESUB(sv);
    {OP * ReplaceReturn2087 = cx->blk_sub.retop; SensorCall(15112); return ReplaceReturn2087;}
}

PP(pp_entersub)
{
SensorCall(15113);    dVAR; dSP; dPOPss;
    GV *gv;
    register CV *cv;
    register PERL_CONTEXT *cx;
    I32 gimme;
    const bool hasargs = (PL_op->op_flags & OPf_STACKED) != 0;

    SensorCall(15114);if (!sv)
	DIE(aTHX_ "Not a CODE reference");
    SensorCall(15137);switch (SvTYPE(sv)) {
	/* This is overwhelming the most common case:  */
    case SVt_PVGV:
      we_have_a_glob:
	SensorCall(15115);if (!(cv = GvCVu((const GV *)sv))) {
	    SensorCall(15116);HV *stash;
	    cv = sv_2cv(sv, &stash, &gv, 0);
	}
	SensorCall(15118);if (!cv) {
	    ENTER;
	    SAVETMPS;
	    SensorCall(15117);goto try_autoload;
	}
	SensorCall(15119);break;
    case SVt_PVLV:
	SensorCall(15120);if(isGV_with_GP(sv)) {/*59*/SensorCall(15121);goto we_have_a_glob;/*60*/}
	/*FALLTHROUGH*/
    default:
	SensorCall(15122);if (sv == &PL_sv_yes) {		/* unfound import, ignore */
	    SensorCall(15123);if (hasargs)
		SP = PL_stack_base + POPMARK;
	    else
		(void)POPMARK;
	    RETURN;
	}
	SvGETMAGIC(sv);
	SensorCall(15131);if (SvROK(sv)) {
	    SensorCall(15124);if (SvAMAGIC(sv)) {
		SensorCall(15125);sv = amagic_deref_call(sv, to_cv_amg);
		/* Don't SPAGAIN here.  */
	    }
	}
	else {
	    SensorCall(15126);const char *sym;
	    STRLEN len;
	    sym = SvPV_nomg_const(sv, len);
	    SensorCall(15127);if (!sym)
		DIE(aTHX_ PL_no_usym, "a subroutine");
	    SensorCall(15128);if (PL_op->op_private & HINT_STRICT_REFS)
		DIE(aTHX_ "Can't use string (\"%" SVf32 "\"%s) as a subroutine ref while \"strict refs\" in use", sv, len>32 ? "..." : "");
	    SensorCall(15129);cv = get_cvn_flags(sym, len, GV_ADD|SvUTF8(sv));
	    SensorCall(15130);break;
	}
	SensorCall(15132);cv = MUTABLE_CV(SvRV(sv));
	SensorCall(15134);if (SvTYPE(cv) == SVt_PVCV)
	    {/*61*/SensorCall(15133);break;/*62*/}
	/* FALL THROUGH */
    case SVt_PVHV:
    case SVt_PVAV:
	DIE(aTHX_ "Not a CODE reference");
	/* This is the second most common case:  */
    case SVt_PVCV:
	SensorCall(15135);cv = MUTABLE_CV(sv);
	SensorCall(15136);break;
    }

    ENTER;
    SAVETMPS;

  retry:
    SensorCall(15138);if (CvCLONE(cv) && ! CvCLONED(cv))
	DIE(aTHX_ "Closure prototype called");
    SensorCall(15148);if (!CvROOT(cv) && !CvXSUB(cv)) {
	SensorCall(15139);GV* autogv;
	SV* sub_name;

	/* anonymous or undef'd function leaves us no recourse */
	SensorCall(15140);if (CvANON(cv) || !(gv = CvGV(cv)))
	    DIE(aTHX_ "Undefined subroutine called");

	/* autoloaded stub? */
	SensorCall(15145);if (cv != GvCV(gv)) {
	    SensorCall(15141);cv = GvCV(gv);
	}
	/* should call AUTOLOAD now? */
	else {
try_autoload:
	    SensorCall(15142);if ((autogv = gv_autoload_pvn(GvSTASH(gv), GvNAME(gv), GvNAMELEN(gv),
				   GvNAMEUTF8(gv) ? SVf_UTF8 : 0)))
	    {
		SensorCall(15143);cv = GvCV(autogv);
	    }
	    /* sorry */
	    else {
		SensorCall(15144);sub_name = sv_newmortal();
		gv_efullname3(sub_name, gv, NULL);
		DIE(aTHX_ "Undefined subroutine &%"SVf" called", SVfARG(sub_name));
	    }
	}
	SensorCall(15146);if (!cv)
	    DIE(aTHX_ "Not a CODE reference");
	SensorCall(15147);goto retry;
    }

    SensorCall(15149);gimme = GIMME_V;
    SensorCall(15157);if ((PL_op->op_private & OPpENTERSUB_DB) && GvCV(PL_DBsub) && !CvNODEBUG(cv)) {
	 SensorCall(15150);Perl_get_db_sub(aTHX_ &sv, cv);
	 SensorCall(15151);if (CvISXSUB(cv))
	     PL_curcopdb = PL_curcop;
         SensorCall(15155);if (CvLVALUE(cv)) {
             /* check for lsub that handles lvalue subroutines */
	     SensorCall(15152);cv = GvCV(gv_HVadd(gv_fetchpvs("DB::lsub", GV_ADDMULTI, SVt_PVHV)));
             /* if lsub not found then fall back to DB::sub */
	     SensorCall(15153);if (!cv) cv = GvCV(PL_DBsub);
         } else {
             SensorCall(15154);cv = GvCV(PL_DBsub);
         }

	SensorCall(15156);if (!cv || (!CvXSUB(cv) && !CvSTART(cv)))
	    DIE(aTHX_ "No DB::sub routine defined");
    }

    SensorCall(15180);if (!(CvISXSUB(cv))) {
	/* This path taken at least 75% of the time   */
	dMARK;
	SensorCall(15158);register I32 items = SP - MARK;
	AV* const padlist = CvPADLIST(cv);
	PUSHBLOCK(cx, CXt_SUB, MARK);
	PUSHSUB(cx);
	cx->blk_sub.retop = PL_op->op_next;
	CvDEPTH(cv)++;
	/* XXX This would be a natural place to set C<PL_compcv = cv> so
	 * that eval'' ops within this sub know the correct lexical space.
	 * Owing the speed considerations, we choose instead to search for
	 * the cv using find_runcv() when calling doeval().
	 */
	SensorCall(15159);if (CvDEPTH(cv) >= 2) {
	    PERL_STACK_OVERFLOW_CHECK();
	    pad_push(padlist, CvDEPTH(cv));
	}
	SAVECOMPPAD();
	PAD_SET_CUR_NOSAVE(padlist, CvDEPTH(cv));
	SensorCall(15169);if (hasargs) {
	    SensorCall(15160);AV *const av = MUTABLE_AV(PAD_SVl(0));
	    SensorCall(15161);if (AvREAL(av)) {
		/* @_ is normally not REAL--this should only ever
		 * happen when DB::sub() calls things that modify @_ */
		av_clear(av);
		AvREAL_off(av);
		AvREIFY_on(av);
	    }
	    SensorCall(15162);cx->blk_sub.savearray = GvAV(PL_defgv);
	    GvAV(PL_defgv) = MUTABLE_AV(SvREFCNT_inc_simple(av));
	    CX_CURPAD_SAVE(cx->blk_sub);
	    cx->blk_sub.argarray = av;
	    ++MARK;

	    SensorCall(15166);if (items > AvMAX(av) + 1) {
		SensorCall(15163);SV **ary = AvALLOC(av);
		SensorCall(15164);if (AvARRAY(av) != ary) {
		    AvMAX(av) += AvARRAY(av) - AvALLOC(av);
		    AvARRAY(av) = ary;
		}
		SensorCall(15165);if (items > AvMAX(av) + 1) {
		    AvMAX(av) = items - 1;
		    Renew(ary,items,SV*);
		    AvALLOC(av) = ary;
		    AvARRAY(av) = ary;
		}
	    }
	    Copy(MARK,AvARRAY(av),items,SV*);
	    AvFILLp(av) = items - 1;
	
	    SensorCall(15168);while (items--) {
		SensorCall(15167);if (*MARK)
		    SvTEMP_off(*MARK);
		MARK++;
	    }
	}
	SensorCall(15170);if ((cx->blk_u16 & OPpENTERSUB_LVAL_MASK) == OPpLVAL_INTRO &&
	    !CvLVALUE(cv))
	    DIE(aTHX_ "Can't modify non-lvalue subroutine call");
	/* warning must come *after* we fully set up the context
	 * stuff so that __WARN__ handlers can safely dounwind()
	 * if they want to
	 */
	SensorCall(15171);if (CvDEPTH(cv) == PERL_SUB_DEPTH_WARN && ckWARN(WARN_RECURSION)
	    && !(PERLDB_SUB && cv == GvCV(PL_DBsub)))
	    sub_crush_depth(cv);
	RETURNOP(CvSTART(cv));
    }
    else {
	SensorCall(15172);I32 markix = TOPMARK;

	PUTBACK;

	SensorCall(15175);if (!hasargs) {
	    /* Need to copy @_ to stack. Alternative may be to
	     * switch stack to @_, and copy return values
	     * back. This would allow popping @_ in XSUB, e.g.. XXXX */
	    SensorCall(15173);AV * const av = GvAV(PL_defgv);
	    const I32 items = AvFILLp(av) + 1;   /* @_ is not tieable */

	    SensorCall(15174);if (items) {
		/* Mark is at the end of the stack. */
		EXTEND(SP, items);
		Copy(AvARRAY(av), SP + 1, items, SV*);
		SP += items;
		PUTBACK ;		
	    }
	}
	/* We assume first XSUB in &DB::sub is the called one. */
	SensorCall(15176);if (PL_curcopdb) {
	    SAVEVPTR(PL_curcop);
	    PL_curcop = PL_curcopdb;
	    PL_curcopdb = NULL;
	}
	/* Do we need to open block here? XXXX */

	/* CvXSUB(cv) must not be NULL because newXS() refuses NULL xsub address */
	assert(CvXSUB(cv));
	CvXSUB(cv)(aTHX_ cv);

	/* Enforce some sanity in scalar context. */
	SensorCall(15178);if (gimme == G_SCALAR && ++markix != PL_stack_sp - PL_stack_base ) {
	    SensorCall(15177);if (markix > PL_stack_sp - PL_stack_base)
		*(PL_stack_base + markix) = &PL_sv_undef;
	    else
		*(PL_stack_base + markix) = *PL_stack_sp;
	    PL_stack_sp = PL_stack_base + markix;
	}
	LEAVE;
	{OP * ReplaceReturn2086 = NORMAL; SensorCall(15179); return ReplaceReturn2086;}
    }
SensorCall(15181);}

void
Perl_sub_crush_depth(pTHX_ CV *cv)
{
    PERL_ARGS_ASSERT_SUB_CRUSH_DEPTH;

    SensorCall(15182);if (CvANON(cv))
	{/*27*/SensorCall(15183);Perl_warner(aTHX_ packWARN(WARN_RECURSION), "Deep recursion on anonymous subroutine");/*28*/}
    else {
	SensorCall(15184);SV* const tmpstr = sv_newmortal();
	gv_efullname3(tmpstr, CvGV(cv), NULL);
	Perl_warner(aTHX_ packWARN(WARN_RECURSION), "Deep recursion on subroutine \"%"SVf"\"",
		    SVfARG(tmpstr));
    }
SensorCall(15185);}

PP(pp_aelem)
{
SensorCall(15186);    dVAR; dSP;
    SV** svp;
    SV* const elemsv = POPs;
    IV elem = SvIV(elemsv);
    AV *const av = MUTABLE_AV(POPs);
    const U32 lval = PL_op->op_flags & OPf_MOD || LVRET;
    const U32 defer = (PL_op->op_private & OPpLVAL_DEFER) && (elem > av_len(av));
    const bool localizing = PL_op->op_private & OPpLVAL_INTRO;
    bool preeminent = TRUE;
    SV *sv;

    SensorCall(15188);if (SvROK(elemsv) && !SvGAMAGIC(elemsv) && ckWARN(WARN_MISC))
	{/*55*/SensorCall(15187);Perl_warner(aTHX_ packWARN(WARN_MISC),
		    "Use of reference \"%"SVf"\" as array index",
		    SVfARG(elemsv));/*56*/}
    SensorCall(15189);if (SvTYPE(av) != SVt_PVAV)
	RETPUSHUNDEF;

    SensorCall(15192);if (localizing) {
	SensorCall(15190);MAGIC *mg;
	HV *stash;

	/* If we can determine whether the element exist,
	 * Try to preserve the existenceness of a tied array
	 * element by using EXISTS and DELETE if possible.
	 * Fallback to FETCH and STORE otherwise. */
	SensorCall(15191);if (SvCANEXISTDELETE(av))
	    preeminent = av_exists(av, elem);
    }

    SensorCall(15193);svp = av_fetch(av, elem, lval && !defer);
    SensorCall(15205);if (lval) {
#ifdef PERL_MALLOC_WRAP
	 SensorCall(15194);if (SvUOK(elemsv)) {
	      SensorCall(15195);const UV uv = SvUV(elemsv);
	      elem = uv > IV_MAX ? IV_MAX : uv;
	 }
	 else if (SvNOK(elemsv))
	      elem = (IV)SvNV(elemsv);
	 SensorCall(15197);if (elem > 0) {
	      SensorCall(15196);static const char oom_array_extend[] =
		"Out of memory during array extend"; /* Duplicated in av.c */
	      MEM_WRAP_CHECK_1(elem,SV*,oom_array_extend);
	 }
#endif
	SensorCall(15201);if (!svp || *svp == &PL_sv_undef) {
	    SensorCall(15198);SV* lv;
	    SensorCall(15199);if (!defer)
		DIE(aTHX_ PL_no_aelem, elem);
	    SensorCall(15200);lv = sv_newmortal();
	    sv_upgrade(lv, SVt_PVLV);
	    LvTYPE(lv) = 'y';
	    sv_magic(lv, NULL, PERL_MAGIC_defelem, NULL, 0);
	    LvTARG(lv) = SvREFCNT_inc_simple(av);
	    LvTARGOFF(lv) = elem;
	    LvTARGLEN(lv) = 1;
	    PUSHs(lv);
	    RETURN;
	}
	SensorCall(15204);if (localizing) {
	    SensorCall(15202);if (preeminent)
		save_aelem(av, elem, svp);
	    else
		SAVEADELETE(av, elem);
	}
	else {/*57*/SensorCall(15203);if (PL_op->op_private & OPpDEREF) {
	    PUSHs(vivify_ref(*svp, PL_op->op_private & OPpDEREF));
	    RETURN;
	;/*58*/}}
    }
    SensorCall(15206);sv = (svp ? *svp : &PL_sv_undef);
    SensorCall(15207);if (!lval && SvRMAGICAL(av) && SvGMAGICAL(sv)) /* see note in pp_helem() */
	mg_get(sv);
    PUSHs(sv);
    RETURN;
}

SV*
Perl_vivify_ref(pTHX_ SV *sv, U32 to_what)
{
SensorCall(15208);    PERL_ARGS_ASSERT_VIVIFY_REF;

    SvGETMAGIC(sv);
    SensorCall(15215);if (!SvOK(sv)) {
	SensorCall(15209);if (SvREADONLY(sv))
	    {/*29*/SensorCall(15210);Perl_croak_no_modify(aTHX);/*30*/}
	prepare_SV_for_RV(sv);
	SensorCall(15214);switch (to_what) {
	case OPpDEREF_SV:
	    SvRV_set(sv, newSV(0));
	    SensorCall(15211);break;
	case OPpDEREF_AV:
	    SvRV_set(sv, MUTABLE_SV(newAV()));
	    SensorCall(15212);break;
	case OPpDEREF_HV:
	    SvRV_set(sv, MUTABLE_SV(newHV()));
	    SensorCall(15213);break;
	}
	SvROK_on(sv);
	SvSETMAGIC(sv);
	SvGETMAGIC(sv);
    }
    SensorCall(15218);if (SvGMAGICAL(sv)) {
	/* copy the sv without magic to prevent magic from being
	   executed twice */
	SensorCall(15216);SV* msv = sv_newmortal();
	sv_setsv_nomg(msv, sv);
	{SV * ReplaceReturn2085 = msv; SensorCall(15217); return ReplaceReturn2085;}
    }
    {SV * ReplaceReturn2084 = sv; SensorCall(15219); return ReplaceReturn2084;}
}

PP(pp_method)
{
SensorCall(15220);    dVAR; dSP;
    SV* const sv = TOPs;

    SensorCall(15223);if (SvROK(sv)) {
	SensorCall(15221);SV* const rsv = SvRV(sv);
	SensorCall(15222);if (SvTYPE(rsv) == SVt_PVCV) {
	    SETs(rsv);
	    RETURN;
	}
    }

    SETs(method_common(sv, NULL));
    RETURN;
}

PP(pp_method_named)
{
SensorCall(15224);    dVAR; dSP;
    SV* const sv = cSVOP_sv;
    U32 hash = SvSHARED_HASH(sv);

    XPUSHs(method_common(sv, &hash));
    RETURN;
}

STATIC SV *
S_method_common(pTHX_ SV* meth, U32* hashp)
{
SensorCall(15225);    dVAR;
    SV* ob;
    GV* gv;
    HV* stash;
    SV *packsv = NULL;
    SV * const sv = *(PL_stack_base + TOPMARK + 1);

    PERL_ARGS_ASSERT_METHOD_COMMON;

    SensorCall(15227);if (!sv)
	{/*33*/SensorCall(15226);Perl_croak(aTHX_ "Can't call method \"%"SVf"\" on an undefined value",
		   SVfARG(meth));/*34*/}

    SvGETMAGIC(sv);
    SensorCall(15243);if (SvROK(sv))
	ob = MUTABLE_SV(SvRV(sv));
    else {
	SensorCall(15228);GV* iogv;
        STRLEN packlen;
        const char * packname = NULL;
	bool packname_is_utf8 = FALSE;

	/* this isn't a reference */
        SensorCall(15233);if(SvOK(sv) && (packname = SvPV_nomg_const(sv, packlen))) {
          SensorCall(15229);const HE* const he =
	    (const HE *)hv_common_key_len(
	      PL_stashcache, packname,
	      packlen * -(packname_is_utf8 = !!SvUTF8(sv)), 0, NULL, 0
	    );
	  
          SensorCall(15232);if (he) { 
            SensorCall(15230);stash = INT2PTR(HV*,SvIV(HeVAL(he)));
            SensorCall(15231);goto fetch;
          }
        }

	SensorCall(15241);if (!SvOK(sv) ||
	    !(packname) ||
	    !(iogv = gv_fetchpvn_flags(
	        packname, packlen, SVf_UTF8 * packname_is_utf8, SVt_PVIO
	     )) ||
	    !(ob=MUTABLE_SV(GvIO(iogv))))
	{
	    /* this isn't the name of a filehandle either */
	    SensorCall(15234);if (!packname ||
		((UTF8_IS_START(*packname) && DO_UTF8(sv))
		    ? !isIDFIRST_utf8((U8*)packname)
		    : !isIDFIRST_L1((U8)*packname)
		))
	    {
		/* diag_listed_as: Can't call method "%s" without a package or object reference */
		SensorCall(15235);Perl_croak(aTHX_ "Can't call method \"%"SVf"\" %s",
			   SVfARG(meth),
			   SvOK(sv) ? "without a package or object reference"
				    : "on an undefined value");
	    }
	    /* assume it's a package name */
	    SensorCall(15236);stash = gv_stashpvn(packname, packlen, packname_is_utf8 ? SVf_UTF8 : 0);
	    SensorCall(15239);if (!stash)
		{/*35*/SensorCall(15237);packsv = sv;/*36*/}
            else {
	        SensorCall(15238);SV* const ref = newSViv(PTR2IV(stash));
	        (void)hv_store(PL_stashcache, packname,
                                packname_is_utf8 ? -(I32)packlen : (I32)packlen, ref, 0);
	    }
	    SensorCall(15240);goto fetch;
	}
	/* it _is_ a filehandle name -- replace with a reference */
	SensorCall(15242);*(PL_stack_base + TOPMARK + 1) = sv_2mortal(newRV(MUTABLE_SV(iogv)));
    }

    /* if we got here, ob should be a reference or a glob */
    SensorCall(15245);if (!ob || !(SvOBJECT(ob)
		 || (SvTYPE(ob) == SVt_PVGV 
		     && isGV_with_GP(ob)
		     && (ob = MUTABLE_SV(GvIO((const GV *)ob)))
		     && SvOBJECT(ob))))
    {
	SensorCall(15244);Perl_croak(aTHX_ "Can't call method \"%"SVf"\" on unblessed reference",
		   SVfARG((SvSCREAM(meth) && strEQ(SvPV_nolen_const(meth),"isa"))
                                        ? newSVpvs_flags("DOES", SVs_TEMP)
                                        : meth));
    }

    SensorCall(15246);stash = SvSTASH(ob);

  fetch:
    /* NOTE: stash may be null, hope hv_fetch_ent and
       gv_fetchmethod can cope (it seems they can) */

    /* shortcut for simple names */
    SensorCall(15251);if (hashp) {
	SensorCall(15247);const HE* const he = hv_fetch_ent(stash, meth, 0, *hashp);
	SensorCall(15250);if (he) {
	    SensorCall(15248);gv = MUTABLE_GV(HeVAL(he));
	    SensorCall(15249);if (isGV(gv) && GvCV(gv) &&
		(!GvCVGEN(gv) || GvCVGEN(gv)
                  == (PL_sub_generation + HvMROMETA(stash)->cache_gen)))
		return MUTABLE_SV(GvCV(gv));
	}
    }

    SensorCall(15252);gv = gv_fetchmethod_sv_flags(stash ? stash : MUTABLE_HV(packsv),
			             meth, GV_AUTOLOAD | GV_CROAK);

    assert(gv);

    {SV * ReplaceReturn2083 = isGV(gv) ? MUTABLE_SV(GvCV(gv)) : MUTABLE_SV(gv); SensorCall(15253); return ReplaceReturn2083;}
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 *
 * ex: set ts=8 sts=4 sw=4 noet:
 */
