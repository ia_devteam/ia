/*
 * Copyright (c) 2007-2011, Lloyd Hilaiel <lloyd@hilaiel.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <yajl/yajl_parse.h>
#include "var/tmp/sensor.h"
#include <yajl/yajl_gen.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <assert.h>

/* memory debugging routines */
typedef struct
{
    unsigned int numFrees;
    unsigned int numMallocs;
    /* XXX: we really need a hash table here with per-allocation
     *      information */
} yajlTestMemoryContext;

/* cast void * into context */
#define TEST_CTX(vptr) ((yajlTestMemoryContext *) (vptr))

static void yajlTestFree(void * ctx, void * ptr)
{
SensorCall(752);    assert(ptr != NULL);
    TEST_CTX(ctx)->numFrees++;
    free(ptr);
}

static void * yajlTestMalloc(void * ctx, size_t sz)
{
SensorCall(753);    assert(sz != 0);
    TEST_CTX(ctx)->numMallocs++;
    {void * ReplaceReturn104 = malloc(sz); SensorCall(754); return ReplaceReturn104;}
}

static void * yajlTestRealloc(void * ctx, void * ptr, size_t sz)
{
    SensorCall(755);if (ptr == NULL) {
        assert(sz != 0);
        TEST_CTX(ctx)->numMallocs++;
    } else {/*1*/SensorCall(756);if (sz == 0) {
        TEST_CTX(ctx)->numFrees++;
    ;/*2*/}}

    {void * ReplaceReturn103 = realloc(ptr, sz); SensorCall(757); return ReplaceReturn103;}
}


/* begin parsing callback routines */
#define BUF_SIZE 2048

static int test_yajl_null(void *ctx)
{
    SensorCall(758);printf("null\n");
    {int  ReplaceReturn102 = 1; SensorCall(759); return ReplaceReturn102;}
}

static int test_yajl_boolean(void * ctx, int boolVal)
{
    SensorCall(760);printf("bool: %s\n", boolVal ? "true" : "false");
    {int  ReplaceReturn101 = 1; SensorCall(761); return ReplaceReturn101;}
}

static int test_yajl_integer(void *ctx, long long integerVal)
{
    SensorCall(762);printf("integer: %lld\n", integerVal);
    {int  ReplaceReturn100 = 1; SensorCall(763); return ReplaceReturn100;}
}

static int test_yajl_double(void *ctx, double doubleVal)
{
    SensorCall(764);printf("double: %g\n", doubleVal);
    {int  ReplaceReturn99 = 1; SensorCall(765); return ReplaceReturn99;}
}

static int test_yajl_string(void *ctx, const unsigned char * stringVal,
                            size_t stringLen)
{
    SensorCall(766);printf("string: '");
    fwrite(stringVal, 1, stringLen, stdout);
    printf("'\n");
    {int  ReplaceReturn98 = 1; SensorCall(767); return ReplaceReturn98;}
}

static int test_yajl_map_key(void *ctx, const unsigned char * stringVal,
                             size_t stringLen)
{
    SensorCall(768);char * str = (char *) malloc(stringLen + 1);
    str[stringLen] = 0;
    memcpy(str, stringVal, stringLen);
    printf("key: '%s'\n", str);
    free(str);
    {int  ReplaceReturn97 = 1; SensorCall(769); return ReplaceReturn97;}
}

static int test_yajl_start_map(void *ctx)
{
    SensorCall(770);printf("map open '{'\n");
    {int  ReplaceReturn96 = 1; SensorCall(771); return ReplaceReturn96;}
}


static int test_yajl_end_map(void *ctx)
{
    SensorCall(772);printf("map close '}'\n");
    {int  ReplaceReturn95 = 1; SensorCall(773); return ReplaceReturn95;}
}

static int test_yajl_start_array(void *ctx)
{
    SensorCall(774);printf("array open '['\n");
    {int  ReplaceReturn94 = 1; SensorCall(775); return ReplaceReturn94;}
}

static int test_yajl_end_array(void *ctx)
{
    SensorCall(776);printf("array close ']'\n");
    {int  ReplaceReturn93 = 1; SensorCall(777); return ReplaceReturn93;}
}

static yajl_callbacks callbacks = {
    test_yajl_null,
    test_yajl_boolean,
    test_yajl_integer,
    test_yajl_double,
    NULL,
    test_yajl_string,
    test_yajl_start_map,
    test_yajl_map_key,
    test_yajl_end_map,
    test_yajl_start_array,
    test_yajl_end_array
};

static void usage(const char * progname)
{
    SensorCall(778);fprintf(stderr,
            "usage:  %s [options]\n"
            "Parse input from stdin as JSON and ouput parsing details "
                                                          "to stdout\n"
            "   -b  set the read buffer size\n"
            "   -c  allow comments\n"
            "   -g  allow *g*arbage after valid JSON text\n"
            "   -m  allows the parser to consume multiple JSON values\n"
            "       from a single string separated by whitespace\n"
            "   -p  partial JSON documents should not cause errors\n",
            progname);
    exit(1);
SensorCall(779);}

int
main(int argc, char ** argv)
{
    SensorCall(780);yajl_handle hand;
    const char * fileName;
    static unsigned char * fileData = NULL;
    size_t bufSize = BUF_SIZE;
    yajl_status stat;
    size_t rd;
    int i, j;

    /* memory allocation debugging: allocate a structure which collects
     * statistics */
    yajlTestMemoryContext memCtx = { 0,0 };

    /* memory allocation debugging: allocate a structure which holds
     * allocation routines */
    yajl_alloc_funcs allocFuncs = {
        yajlTestMalloc,
        yajlTestRealloc,
        yajlTestFree,
        (void *) NULL
    };

    allocFuncs.ctx = (void *) &memCtx;

    /* allocate the parser */
    hand = yajl_alloc(&callbacks, &allocFuncs, NULL);

    /* check arguments.  We expect exactly one! */
    SensorCall(800);for (i=1;i<argc;i++) {
        SensorCall(781);if (!strcmp("-c", argv[i])) {
            SensorCall(782);yajl_config(hand, yajl_allow_comments, 1);
        } else {/*3*/SensorCall(783);if (!strcmp("-b", argv[i])) {
            SensorCall(784);if (++i >= argc) {/*5*/SensorCall(785);usage(argv[0]);/*6*/}

            /* validate integer */
            SensorCall(789);for (j=0;j<(int)strlen(argv[i]);j++) {
                SensorCall(786);if (argv[i][j] <= '9' && argv[i][j] >= '0') {/*7*/SensorCall(787);continue;/*8*/}
                SensorCall(788);fprintf(stderr, "-b requires an integer argument.  '%s' "
                        "is invalid\n", argv[i]);
                usage(argv[0]);
            }

            SensorCall(790);bufSize = atoi(argv[i]);
            SensorCall(792);if (!bufSize) {
                SensorCall(791);fprintf(stderr, "%zu is an invalid buffer size\n",
                        bufSize);
            }
        } else {/*9*/SensorCall(793);if (!strcmp("-g", argv[i])) {
            SensorCall(794);yajl_config(hand, yajl_allow_trailing_garbage, 1);
        } else {/*11*/SensorCall(795);if (!strcmp("-m", argv[i])) {
            SensorCall(796);yajl_config(hand, yajl_allow_multiple_values, 1);
        } else {/*13*/SensorCall(797);if (!strcmp("-p", argv[i])) {
            SensorCall(798);yajl_config(hand, yajl_allow_partial_values, 1);
        } else {
            SensorCall(799);fprintf(stderr, "invalid command line option: '%s'\n",
                    argv[i]);
            usage(argv[0]);
        ;/*14*/}/*12*/}/*10*/}/*4*/}}
    }

    SensorCall(801);fileData = (unsigned char *) malloc(bufSize);

    SensorCall(803);if (fileData == NULL) {
        SensorCall(802);fprintf(stderr,
                "failed to allocate read buffer of %zu bytes, exiting.",
                bufSize);
        yajl_free(hand);
        exit(2);
    }

    SensorCall(804);fileName = argv[argc-1];

    SensorCall(813);for (;;) {
        SensorCall(805);rd = fread((void *) fileData, 1, bufSize, stdin);

        SensorCall(809);if (rd == 0) {
            SensorCall(806);if (!feof(stdin)) {
                SensorCall(807);fprintf(stderr, "error reading from '%s'\n", fileName);
            }
            SensorCall(808);break;
        }
        /* read file data, now pass to parser */
        SensorCall(810);stat = yajl_parse(hand, fileData, rd);

        SensorCall(812);if (stat != yajl_status_ok) {/*15*/SensorCall(811);break;/*16*/}
    }

    SensorCall(814);stat = yajl_complete_parse(hand);
    SensorCall(816);if (stat != yajl_status_ok)
    {
        SensorCall(815);unsigned char * str = yajl_get_error(hand, 0, fileData, rd);
        fflush(stdout);
        fprintf(stderr, "%s", (char *) str);
        yajl_free_error(hand, str);
    }

    SensorCall(817);yajl_free(hand);
    free(fileData);

    /* finally, print out some memory statistics */

/* (lth) only print leaks here, as allocations and frees may vary depending
 *       on read buffer size, causing false failures.
 *
 *  printf("allocations:\t%u\n", memCtx.numMallocs);
 *  printf("frees:\t\t%u\n", memCtx.numFrees);
*/
    fflush(stderr);
    fflush(stdout);
    printf("memory leaks:\t%u\n", memCtx.numMallocs - memCtx.numFrees);

    {int  ReplaceReturn92 = 0; SensorCall(818); return ReplaceReturn92;}
}
