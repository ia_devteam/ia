/*
 * Copyright (c) 2007-2011, Lloyd Hilaiel <lloyd@hilaiel.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "api/yajl_gen.h"
#include "var/tmp/sensor.h"
#include "yajl_buf.h"
#include "yajl_encode.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <stdarg.h>

typedef enum {
    yajl_gen_start,
    yajl_gen_map_start,
    yajl_gen_map_key,
    yajl_gen_map_val,
    yajl_gen_array_start,
    yajl_gen_in_array,
    yajl_gen_complete,
    yajl_gen_error
} yajl_gen_state;

struct yajl_gen_t
{
    unsigned int flags;
    unsigned int depth;
    const char * indentString;
    yajl_gen_state state[YAJL_MAX_DEPTH];
    yajl_print_t print;
    void * ctx; /* yajl_buf */
    /* memory allocation routines */
    yajl_alloc_funcs alloc;
};

int
yajl_gen_config(yajl_gen g, yajl_gen_option opt, ...)
{
    SensorCall(185);int rv = 1;
    va_list ap;
    va_start(ap, opt);

    SensorCall(198);switch(opt) {
        case yajl_gen_beautify:
        case yajl_gen_validate_utf8:
        case yajl_gen_escape_solidus:
            SensorCall(186);if (va_arg(ap, int)) {/*1*/SensorCall(187);g->flags |= opt;/*2*/}
            else {/*3*/SensorCall(188);g->flags &= ~opt;/*4*/}
            SensorCall(189);break;
        case yajl_gen_indent_string: {
            SensorCall(190);const char *indent = va_arg(ap, const char *);
            g->indentString = indent;
            SensorCall(193);for (; *indent; indent++) {
                SensorCall(191);if (*indent != '\n'
                    && *indent != '\v'
                    && *indent != '\f'
                    && *indent != '\t'
                    && *indent != '\r'
                    && *indent != ' ')
                {
                    SensorCall(192);g->indentString = NULL;
                    rv = 0;
                }
            }
            SensorCall(194);break;
        }
        case yajl_gen_print_callback:
            SensorCall(195);yajl_buf_free(g->ctx);
            g->print = va_arg(ap, const yajl_print_t);
            g->ctx = va_arg(ap, void *);
            SensorCall(196);break;
        default:
            SensorCall(197);rv = 0;
    }

    va_end(ap);

    {int  ReplaceReturn91 = rv; SensorCall(199); return ReplaceReturn91;}
}



yajl_gen
yajl_gen_alloc(const yajl_alloc_funcs * afs)
{
    SensorCall(200);yajl_gen g = NULL;
    yajl_alloc_funcs afsBuffer;

    /* first order of business is to set up memory allocation routines */
    SensorCall(204);if (afs != NULL) {
        SensorCall(201);if (afs->malloc == NULL || afs->realloc == NULL || afs->free == NULL)
        {
            {yajl_gen  ReplaceReturn90 = NULL; SensorCall(202); return ReplaceReturn90;}
        }
    } else {
        SensorCall(203);yajl_set_default_alloc_funcs(&afsBuffer);
        afs = &afsBuffer;
    }

    SensorCall(205);g = (yajl_gen) YA_MALLOC(afs, sizeof(struct yajl_gen_t));
    SensorCall(206);if (!g) return NULL;

    SensorCall(207);memset((void *) g, 0, sizeof(struct yajl_gen_t));
    /* copy in pointers to allocation routines */
    memcpy((void *) &(g->alloc), (void *) afs, sizeof(yajl_alloc_funcs));

    g->print = (yajl_print_t)&yajl_buf_append;
    g->ctx = yajl_buf_alloc(&(g->alloc));
    g->indentString = "    ";

    {yajl_gen  ReplaceReturn89 = g; SensorCall(208); return ReplaceReturn89;}
}

void
yajl_gen_free(yajl_gen g)
{
    SensorCall(209);if (g->print == (yajl_print_t)&yajl_buf_append) {/*5*/SensorCall(210);yajl_buf_free((yajl_buf)g->ctx);/*6*/}
    YA_FREE(&(g->alloc), g);
}

#define INSERT_SEP \
    if (g->state[g->depth] == yajl_gen_map_key ||               \
        g->state[g->depth] == yajl_gen_in_array) {              \
        g->print(g->ctx, ",", 1);                               \
        if ((g->flags & yajl_gen_beautify)) g->print(g->ctx, "\n", 1);               \
    } else if (g->state[g->depth] == yajl_gen_map_val) {        \
        g->print(g->ctx, ":", 1);                               \
        if ((g->flags & yajl_gen_beautify)) g->print(g->ctx, " ", 1);                \
   } 

#define INSERT_WHITESPACE                                               \
    if ((g->flags & yajl_gen_beautify)) {                                                    \
        if (g->state[g->depth] != yajl_gen_map_val) {                   \
            unsigned int _i;                                            \
            for (_i=0;_i<g->depth;_i++)                                 \
                g->print(g->ctx,                                        \
                         g->indentString,                               \
                         (unsigned int)strlen(g->indentString));        \
        }                                                               \
    }

#define ENSURE_NOT_KEY \
    if (g->state[g->depth] == yajl_gen_map_key ||       \
        g->state[g->depth] == yajl_gen_map_start)  {    \
        return yajl_gen_keys_must_be_strings;           \
    }                                                   \

/* check that we're not complete, or in error state.  in a valid state
 * to be generating */
#define ENSURE_VALID_STATE \
    if (g->state[g->depth] == yajl_gen_error) {   \
        return yajl_gen_in_error_state;\
    } else if (g->state[g->depth] == yajl_gen_complete) {   \
        return yajl_gen_generation_complete;                \
    }

#define INCREMENT_DEPTH \
    if (++(g->depth) >= YAJL_MAX_DEPTH) return yajl_max_depth_exceeded;

#define DECREMENT_DEPTH \
  if (--(g->depth) >= YAJL_MAX_DEPTH) return yajl_gen_error;

#define APPENDED_ATOM \
    switch (g->state[g->depth]) {                   \
        case yajl_gen_start:                        \
            g->state[g->depth] = yajl_gen_complete; \
            break;                                  \
        case yajl_gen_map_start:                    \
        case yajl_gen_map_key:                      \
            g->state[g->depth] = yajl_gen_map_val;  \
            break;                                  \
        case yajl_gen_array_start:                  \
            g->state[g->depth] = yajl_gen_in_array; \
            break;                                  \
        case yajl_gen_map_val:                      \
            g->state[g->depth] = yajl_gen_map_key;  \
            break;                                  \
        default:                                    \
            break;                                  \
    }                                               \

#define FINAL_NEWLINE                                        \
    if ((g->flags & yajl_gen_beautify) && g->state[g->depth] == yajl_gen_complete) \
        g->print(g->ctx, "\n", 1);

yajl_gen_status
yajl_gen_integer(yajl_gen g, long long int number)
{
    SensorCall(211);char i[32];
    ENSURE_VALID_STATE; ENSURE_NOT_KEY; INSERT_SEP; INSERT_WHITESPACE;
    SensorCall(212);sprintf(i, "%lld", number);
    g->print(g->ctx, i, (unsigned int)strlen(i));
    APPENDED_ATOM;
    FINAL_NEWLINE;
    {yajl_gen_status  ReplaceReturn88 = yajl_gen_status_ok; SensorCall(213); return ReplaceReturn88;}
}

#if defined(_WIN32) || defined(WIN32)
#include <float.h>
#define isnan _isnan
#define isinf !_finite
#endif

yajl_gen_status
yajl_gen_double(yajl_gen g, double number)
{
    SensorCall(214);char i[32];
    ENSURE_VALID_STATE; ENSURE_NOT_KEY; 
    SensorCall(216);if (isnan(number) || isinf(number)) {/*7*/{yajl_gen_status  ReplaceReturn87 = yajl_gen_invalid_number; SensorCall(215); return ReplaceReturn87;}/*8*/}
    INSERT_SEP; INSERT_WHITESPACE;
    SensorCall(217);sprintf(i, "%.20g", number);
    SensorCall(219);if (strspn(i, "0123456789-") == strlen(i)) {
        SensorCall(218);strcat(i, ".0");
    }
    SensorCall(220);g->print(g->ctx, i, (unsigned int)strlen(i));
    APPENDED_ATOM;
    FINAL_NEWLINE;
    {yajl_gen_status  ReplaceReturn86 = yajl_gen_status_ok; SensorCall(221); return ReplaceReturn86;}
}

yajl_gen_status
yajl_gen_number(yajl_gen g, const char * s, size_t l)
{
SensorCall(222);    ENSURE_VALID_STATE; ENSURE_NOT_KEY; INSERT_SEP; INSERT_WHITESPACE;
    SensorCall(223);g->print(g->ctx, s, l);
    APPENDED_ATOM;
    FINAL_NEWLINE;
    {yajl_gen_status  ReplaceReturn85 = yajl_gen_status_ok; SensorCall(224); return ReplaceReturn85;}
}

yajl_gen_status
yajl_gen_string(yajl_gen g, const unsigned char * str,
                size_t len)
{
    // if validation is enabled, check that the string is valid utf8
    // XXX: This checking could be done a little faster, in the same pass as
    // the string encoding
    SensorCall(225);if (g->flags & yajl_gen_validate_utf8) {
        SensorCall(226);if (!yajl_string_validate_utf8(str, len)) {
            {yajl_gen_status  ReplaceReturn84 = yajl_gen_invalid_string; SensorCall(227); return ReplaceReturn84;}
        }
    }
    ENSURE_VALID_STATE; INSERT_SEP; INSERT_WHITESPACE;
    SensorCall(228);g->print(g->ctx, "\"", 1);
    yajl_string_encode(g->print, g->ctx, str, len, g->flags & yajl_gen_escape_solidus);
    g->print(g->ctx, "\"", 1);
    APPENDED_ATOM;
    FINAL_NEWLINE;
    {yajl_gen_status  ReplaceReturn83 = yajl_gen_status_ok; SensorCall(229); return ReplaceReturn83;}
}

yajl_gen_status
yajl_gen_null(yajl_gen g)
{
SensorCall(230);    ENSURE_VALID_STATE; ENSURE_NOT_KEY; INSERT_SEP; INSERT_WHITESPACE;
    SensorCall(231);g->print(g->ctx, "null", strlen("null"));
    APPENDED_ATOM;
    FINAL_NEWLINE;
    {yajl_gen_status  ReplaceReturn82 = yajl_gen_status_ok; SensorCall(232); return ReplaceReturn82;}
}

yajl_gen_status
yajl_gen_bool(yajl_gen g, int boolean)
{
    SensorCall(233);const char * val = boolean ? "true" : "false";

	ENSURE_VALID_STATE; ENSURE_NOT_KEY; INSERT_SEP; INSERT_WHITESPACE;
    SensorCall(234);g->print(g->ctx, val, (unsigned int)strlen(val));
    APPENDED_ATOM;
    FINAL_NEWLINE;
    {yajl_gen_status  ReplaceReturn81 = yajl_gen_status_ok; SensorCall(235); return ReplaceReturn81;}
}

yajl_gen_status
yajl_gen_map_open(yajl_gen g)
{
SensorCall(236);    ENSURE_VALID_STATE; ENSURE_NOT_KEY; INSERT_SEP; INSERT_WHITESPACE;
    INCREMENT_DEPTH; 
    
    SensorCall(237);g->state[g->depth] = yajl_gen_map_start;
    g->print(g->ctx, "{", 1);
    SensorCall(239);if ((g->flags & yajl_gen_beautify)) {/*9*/SensorCall(238);g->print(g->ctx, "\n", 1);/*10*/}
    FINAL_NEWLINE;
    {yajl_gen_status  ReplaceReturn80 = yajl_gen_status_ok; SensorCall(240); return ReplaceReturn80;}
}

yajl_gen_status
yajl_gen_map_close(yajl_gen g)
{
SensorCall(241);    ENSURE_VALID_STATE; 
    DECREMENT_DEPTH;
    
    SensorCall(243);if ((g->flags & yajl_gen_beautify)) {/*11*/SensorCall(242);g->print(g->ctx, "\n", 1);/*12*/}
    APPENDED_ATOM;
    INSERT_WHITESPACE;
    SensorCall(244);g->print(g->ctx, "}", 1);
    FINAL_NEWLINE;
    {yajl_gen_status  ReplaceReturn79 = yajl_gen_status_ok; SensorCall(245); return ReplaceReturn79;}
}

yajl_gen_status
yajl_gen_array_open(yajl_gen g)
{
SensorCall(246);    ENSURE_VALID_STATE; ENSURE_NOT_KEY; INSERT_SEP; INSERT_WHITESPACE;
    INCREMENT_DEPTH; 
    SensorCall(247);g->state[g->depth] = yajl_gen_array_start;
    g->print(g->ctx, "[", 1);
    SensorCall(249);if ((g->flags & yajl_gen_beautify)) {/*13*/SensorCall(248);g->print(g->ctx, "\n", 1);/*14*/}
    FINAL_NEWLINE;
    {yajl_gen_status  ReplaceReturn78 = yajl_gen_status_ok; SensorCall(250); return ReplaceReturn78;}
}

yajl_gen_status
yajl_gen_array_close(yajl_gen g)
{
SensorCall(251);    ENSURE_VALID_STATE;
    DECREMENT_DEPTH;
    SensorCall(253);if ((g->flags & yajl_gen_beautify)) {/*15*/SensorCall(252);g->print(g->ctx, "\n", 1);/*16*/}
    APPENDED_ATOM;
    INSERT_WHITESPACE;
    SensorCall(254);g->print(g->ctx, "]", 1);
    FINAL_NEWLINE;
    {yajl_gen_status  ReplaceReturn77 = yajl_gen_status_ok; SensorCall(255); return ReplaceReturn77;}
}

yajl_gen_status
yajl_gen_get_buf(yajl_gen g, const unsigned char ** buf,
                 size_t * len)
{
    SensorCall(256);if (g->print != (yajl_print_t)&yajl_buf_append) {/*17*/{yajl_gen_status  ReplaceReturn76 = yajl_gen_no_buf; SensorCall(257); return ReplaceReturn76;}/*18*/}
    SensorCall(258);*buf = yajl_buf_data((yajl_buf)g->ctx);
    *len = yajl_buf_len((yajl_buf)g->ctx);
    {yajl_gen_status  ReplaceReturn75 = yajl_gen_status_ok; SensorCall(259); return ReplaceReturn75;}
}

void
yajl_gen_clear(yajl_gen g)
{
    SensorCall(260);if (g->print == (yajl_print_t)&yajl_buf_append) {/*19*/SensorCall(261);yajl_buf_clear((yajl_buf)g->ctx);/*20*/}
SensorCall(262);}
