/*
 * Copyright (c) 2007-2011, Lloyd Hilaiel <lloyd@hilaiel.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "yajl_lex.h"
#include "var/tmp/sensor.h"
#include "yajl_buf.h"

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#ifdef YAJL_LEXER_DEBUG
static const char *
tokToStr(yajl_tok tok) 
{
    switch (tok) {
        case yajl_tok_bool: return "bool";
        case yajl_tok_colon: return "colon";
        case yajl_tok_comma: return "comma";
        case yajl_tok_eof: return "eof";
        case yajl_tok_error: return "error";
        case yajl_tok_left_brace: return "brace";
        case yajl_tok_left_bracket: return "bracket";
        case yajl_tok_null: return "null";
        case yajl_tok_integer: return "integer";
        case yajl_tok_double: return "double";
        case yajl_tok_right_brace: return "brace";
        case yajl_tok_right_bracket: return "bracket";
        case yajl_tok_string: return "string";
        case yajl_tok_string_with_escapes: return "string_with_escapes";
    }
    return "unknown";
}
#endif

/* Impact of the stream parsing feature on the lexer:
 *
 * YAJL support stream parsing.  That is, the ability to parse the first
 * bits of a chunk of JSON before the last bits are available (still on
 * the network or disk).  This makes the lexer more complex.  The
 * responsibility of the lexer is to handle transparently the case where
 * a chunk boundary falls in the middle of a token.  This is
 * accomplished is via a buffer and a character reading abstraction. 
 *
 * Overview of implementation
 *
 * When we lex to end of input string before end of token is hit, we
 * copy all of the input text composing the token into our lexBuf.
 * 
 * Every time we read a character, we do so through the readChar function.
 * readChar's responsibility is to handle pulling all chars from the buffer
 * before pulling chars from input text
 */

struct yajl_lexer_t {
    /* the overal line and char offset into the data */
    size_t lineOff;
    size_t charOff;

    /* error */
    yajl_lex_error error;

    /* a input buffer to handle the case where a token is spread over
     * multiple chunks */ 
    yajl_buf buf;

    /* in the case where we have data in the lexBuf, bufOff holds
     * the current offset into the lexBuf. */
    size_t bufOff;

    /* are we using the lex buf? */
    unsigned int bufInUse;

    /* shall we allow comments? */
    unsigned int allowComments;

    /* shall we validate utf8 inside strings? */
    unsigned int validateUTF8;

    yajl_alloc_funcs * alloc;
};

#define readChar(lxr, txt, off)                      \
    (((lxr)->bufInUse && yajl_buf_len((lxr)->buf) && lxr->bufOff < yajl_buf_len((lxr)->buf)) ? \
     (*((const unsigned char *) yajl_buf_data((lxr)->buf) + ((lxr)->bufOff)++)) : \
     ((txt)[(*(off))++]))

#define unreadChar(lxr, off) ((*(off) > 0) ? (*(off))-- : ((lxr)->bufOff--))

yajl_lexer
yajl_lex_alloc(yajl_alloc_funcs * alloc,
               unsigned int allowComments, unsigned int validateUTF8)
{
    SensorCall(263);yajl_lexer lxr = (yajl_lexer) YA_MALLOC(alloc, sizeof(struct yajl_lexer_t));
    memset((void *) lxr, 0, sizeof(struct yajl_lexer_t));
    lxr->buf = yajl_buf_alloc(alloc);
    lxr->allowComments = allowComments;
    lxr->validateUTF8 = validateUTF8;
    lxr->alloc = alloc;
    {yajl_lexer  ReplaceReturn33 = lxr; SensorCall(264); return ReplaceReturn33;}
}

void
yajl_lex_free(yajl_lexer lxr)
{
    SensorCall(265);yajl_buf_free(lxr->buf);
    YA_FREE(lxr->alloc, lxr);
    SensorCall(266);return;
}

/* a lookup table which lets us quickly determine three things:
 * VEC - valid escaped control char
 * note.  the solidus '/' may be escaped or not.
 * IJC - invalid json char
 * VHC - valid hex char
 * NFP - needs further processing (from a string scanning perspective)
 * NUC - needs utf8 checking when enabled (from a string scanning perspective)
 */
#define VEC 0x01
#define IJC 0x02
#define VHC 0x04
#define NFP 0x08
#define NUC 0x10

static const char charLookupTable[256] =
{
/*00*/ IJC    , IJC    , IJC    , IJC    , IJC    , IJC    , IJC    , IJC    ,
/*08*/ IJC    , IJC    , IJC    , IJC    , IJC    , IJC    , IJC    , IJC    ,
/*10*/ IJC    , IJC    , IJC    , IJC    , IJC    , IJC    , IJC    , IJC    ,
/*18*/ IJC    , IJC    , IJC    , IJC    , IJC    , IJC    , IJC    , IJC    ,

/*20*/ 0      , 0      , NFP|VEC|IJC, 0      , 0      , 0      , 0      , 0      ,
/*28*/ 0      , 0      , 0      , 0      , 0      , 0      , 0      , VEC    ,
/*30*/ VHC    , VHC    , VHC    , VHC    , VHC    , VHC    , VHC    , VHC    ,
/*38*/ VHC    , VHC    , 0      , 0      , 0      , 0      , 0      , 0      ,

/*40*/ 0      , VHC    , VHC    , VHC    , VHC    , VHC    , VHC    , 0      ,
/*48*/ 0      , 0      , 0      , 0      , 0      , 0      , 0      , 0      ,
/*50*/ 0      , 0      , 0      , 0      , 0      , 0      , 0      , 0      ,
/*58*/ 0      , 0      , 0      , 0      , NFP|VEC|IJC, 0      , 0      , 0      ,

/*60*/ 0      , VHC    , VEC|VHC, VHC    , VHC    , VHC    , VEC|VHC, 0      ,
/*68*/ 0      , 0      , 0      , 0      , 0      , 0      , VEC    , 0      ,
/*70*/ 0      , 0      , VEC    , 0      , VEC    , 0      , 0      , 0      ,
/*78*/ 0      , 0      , 0      , 0      , 0      , 0      , 0      , 0      ,

       NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    ,
       NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    ,
       NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    ,
       NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    ,

       NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    ,
       NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    ,
       NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    ,
       NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    ,

       NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    ,
       NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    ,
       NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    ,
       NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    ,

       NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    ,
       NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    ,
       NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    ,
       NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC    , NUC
};

/** process a variable length utf8 encoded codepoint.
 *
 *  returns:
 *    yajl_tok_string - if valid utf8 char was parsed and offset was
 *                      advanced
 *    yajl_tok_eof - if end of input was hit before validation could
 *                   complete
 *    yajl_tok_error - if invalid utf8 was encountered
 * 
 *  NOTE: on error the offset will point to the first char of the
 *  invalid utf8 */
#define UTF8_CHECK_EOF if (*offset >= jsonTextLen) { return yajl_tok_eof; }

static yajl_tok
yajl_lex_utf8_char(yajl_lexer lexer, const unsigned char * jsonText,
                   size_t jsonTextLen, size_t * offset,
                   unsigned char curChar)
{
    SensorCall(267);if (curChar <= 0x7f) {
        /* single byte */
        {yajl_tok  ReplaceReturn32 = yajl_tok_string; SensorCall(268); return ReplaceReturn32;}
    } else {/*7*/SensorCall(269);if ((curChar >> 5) == 0x6) {
        /* two byte */ 
        UTF8_CHECK_EOF;
        SensorCall(270);curChar = readChar(lexer, jsonText, offset);
        SensorCall(272);if ((curChar >> 6) == 0x2) {/*9*/{yajl_tok  ReplaceReturn31 = yajl_tok_string; SensorCall(271); return ReplaceReturn31;}/*10*/}
    } else {/*11*/SensorCall(273);if ((curChar >> 4) == 0x0e) {
        /* three byte */
        UTF8_CHECK_EOF;
        SensorCall(274);curChar = readChar(lexer, jsonText, offset);
        SensorCall(278);if ((curChar >> 6) == 0x2) {
            UTF8_CHECK_EOF;
            SensorCall(275);curChar = readChar(lexer, jsonText, offset);
            SensorCall(277);if ((curChar >> 6) == 0x2) {/*13*/{yajl_tok  ReplaceReturn30 = yajl_tok_string; SensorCall(276); return ReplaceReturn30;}/*14*/}
        }
    } else {/*15*/SensorCall(279);if ((curChar >> 3) == 0x1e) {
        /* four byte */
        UTF8_CHECK_EOF;
        SensorCall(280);curChar = readChar(lexer, jsonText, offset);
        SensorCall(286);if ((curChar >> 6) == 0x2) {
            UTF8_CHECK_EOF;
            SensorCall(281);curChar = readChar(lexer, jsonText, offset);
            SensorCall(285);if ((curChar >> 6) == 0x2) {
                UTF8_CHECK_EOF;
                SensorCall(282);curChar = readChar(lexer, jsonText, offset);
                SensorCall(284);if ((curChar >> 6) == 0x2) {/*17*/{yajl_tok  ReplaceReturn29 = yajl_tok_string; SensorCall(283); return ReplaceReturn29;}/*18*/}
            }
        }
    ;/*16*/}/*12*/}/*8*/}} 

    {yajl_tok  ReplaceReturn28 = yajl_tok_error; SensorCall(287); return ReplaceReturn28;}
}

/* lex a string.  input is the lexer, pointer to beginning of
 * json text, and start of string (offset).
 * a token is returned which has the following meanings:
 * yajl_tok_string: lex of string was successful.  offset points to
 *                  terminating '"'.
 * yajl_tok_eof: end of text was encountered before we could complete
 *               the lex.
 * yajl_tok_error: embedded in the string were unallowable chars.  offset
 *               points to the offending char
 */
#define STR_CHECK_EOF \
if (*offset >= jsonTextLen) { \
   tok = yajl_tok_eof; \
   goto finish_string_lex; \
}

/** scan a string for interesting characters that might need further
 *  review.  return the number of chars that are uninteresting and can
 *  be skipped.
 * (lth) hi world, any thoughts on how to make this routine faster? */
static size_t
yajl_string_scan(const unsigned char * buf, size_t len, int utf8check)
{
    SensorCall(288);unsigned char mask = IJC|NFP|(utf8check ? NUC : 0);
    size_t skip = 0;
    SensorCall(290);while (skip < len && !(charLookupTable[*buf] & mask))
    {
        SensorCall(289);skip++;
        buf++;
    }
    {size_t  ReplaceReturn27 = skip; SensorCall(291); return ReplaceReturn27;}
}

static yajl_tok
yajl_lex_string(yajl_lexer lexer, const unsigned char * jsonText,
                size_t jsonTextLen, size_t * offset)
{
    SensorCall(292);yajl_tok tok = yajl_tok_error;
    int hasEscapes = 0;

    SensorCall(323);for (;;) {
        SensorCall(293);unsigned char curChar;

        /* now jump into a faster scanning routine to skip as much
         * of the buffers as possible */
        {
            const unsigned char * p;
            size_t len;
            
            SensorCall(297);if ((lexer->bufInUse && yajl_buf_len(lexer->buf) &&
                 lexer->bufOff < yajl_buf_len(lexer->buf)))
            {
                SensorCall(294);p = ((const unsigned char *) yajl_buf_data(lexer->buf) +
                     (lexer->bufOff));
                len = yajl_buf_len(lexer->buf) - lexer->bufOff;
                lexer->bufOff += yajl_string_scan(p, len, lexer->validateUTF8);
            }                
            else {/*19*/SensorCall(295);if (*offset < jsonTextLen) 
            {
                SensorCall(296);p = jsonText + *offset;
                len = jsonTextLen - *offset;
                *offset += yajl_string_scan(p, len, lexer->validateUTF8);
            ;/*20*/}}
        }

        STR_CHECK_EOF;

        SensorCall(298);curChar = readChar(lexer, jsonText, offset);

        /* quote terminates */
        SensorCall(322);if (curChar == '"') {
            SensorCall(299);tok = yajl_tok_string;
            SensorCall(300);break;
        }
        /* backslash escapes a set of control chars, */
        else {/*21*/SensorCall(301);if (curChar == '\\') {
            SensorCall(302);hasEscapes = 1;
            STR_CHECK_EOF;

            /* special case \u */
            SensorCall(303);curChar = readChar(lexer, jsonText, offset);
            SensorCall(311);if (curChar == 'u') {
                SensorCall(304);unsigned int i = 0;

                SensorCall(308);for (i=0;i<4;i++) {
                    STR_CHECK_EOF;                
                    SensorCall(305);curChar = readChar(lexer, jsonText, offset);                
                    SensorCall(307);if (!(charLookupTable[curChar] & VHC)) {
                        /* back up to offending char */
                        unreadChar(lexer, offset);
                        lexer->error = yajl_lex_string_invalid_hex_char;
                        SensorCall(306);goto finish_string_lex;
                    }
                }
            } else {/*23*/SensorCall(309);if (!(charLookupTable[curChar] & VEC)) {
                /* back up to offending char */
                unreadChar(lexer, offset);
                lexer->error = yajl_lex_string_invalid_escaped_char;
                SensorCall(310);goto finish_string_lex;                
            ;/*24*/}} 
        }
        /* when not validating UTF8 it's a simple table lookup to determine
         * if the present character is invalid */
        else {/*25*/SensorCall(312);if(charLookupTable[curChar] & IJC) {
            /* back up to offending char */
            unreadChar(lexer, offset);
            lexer->error = yajl_lex_string_invalid_json_char;
            SensorCall(313);goto finish_string_lex;                
        }
        /* when in validate UTF8 mode we need to do some extra work */
        else {/*27*/SensorCall(314);if (lexer->validateUTF8) {
            SensorCall(315);yajl_tok t = yajl_lex_utf8_char(lexer, jsonText, jsonTextLen,
                                            offset, curChar);
            
            SensorCall(321);if (t == yajl_tok_eof) {
                SensorCall(316);tok = yajl_tok_eof;
                SensorCall(317);goto finish_string_lex;
            } else {/*29*/SensorCall(318);if (t == yajl_tok_error) {
                SensorCall(319);lexer->error = yajl_lex_string_invalid_utf8;
                SensorCall(320);goto finish_string_lex;
            ;/*30*/}} 
        ;/*28*/}/*26*/}/*22*/}}
        /* accept it, and move on */ 
    }
  finish_string_lex:
    /* tell our buddy, the parser, wether he needs to process this string
     * again */
    SensorCall(325);if (hasEscapes && tok == yajl_tok_string) {
        SensorCall(324);tok = yajl_tok_string_with_escapes;
    } 

    {yajl_tok  ReplaceReturn26 = tok; SensorCall(326); return ReplaceReturn26;}
}

#define RETURN_IF_EOF if (*offset >= jsonTextLen) return yajl_tok_eof;

static yajl_tok
yajl_lex_number(yajl_lexer lexer, const unsigned char * jsonText,
                size_t jsonTextLen, size_t * offset)
{
    /** XXX: numbers are the only entities in json that we must lex
     *       _beyond_ in order to know that they are complete.  There
     *       is an ambiguous case for integers at EOF. */

    SensorCall(327);unsigned char c;

    yajl_tok tok = yajl_tok_integer;

    RETURN_IF_EOF;    
    SensorCall(328);c = readChar(lexer, jsonText, offset);

    /* optional leading minus */
    SensorCall(330);if (c == '-') {
        RETURN_IF_EOF;    
        SensorCall(329);c = readChar(lexer, jsonText, offset); 
    }

    /* a single zero, or a series of integers */
    SensorCall(336);if (c == '0') {
        RETURN_IF_EOF;    
        SensorCall(331);c = readChar(lexer, jsonText, offset); 
    } else {/*31*/SensorCall(332);if (c >= '1' && c <= '9') {
        SensorCall(333);do {
            RETURN_IF_EOF;    
            SensorCall(334);c = readChar(lexer, jsonText, offset); 
        } while (c >= '0' && c <= '9');
    } else {
        unreadChar(lexer, offset);
        lexer->error = yajl_lex_missing_integer_after_minus;
        {yajl_tok  ReplaceReturn25 = yajl_tok_error; SensorCall(335); return ReplaceReturn25;}
    ;/*32*/}}

    /* optional fraction (indicates this is floating point) */
    SensorCall(345);if (c == '.') {
        SensorCall(337);int numRd = 0;
        
        RETURN_IF_EOF;
        SensorCall(338);c = readChar(lexer, jsonText, offset); 

        SensorCall(341);while (c >= '0' && c <= '9') {
            SensorCall(339);numRd++;
            RETURN_IF_EOF;
            SensorCall(340);c = readChar(lexer, jsonText, offset); 
        } 

        SensorCall(343);if (!numRd) {
            unreadChar(lexer, offset);
            lexer->error = yajl_lex_missing_integer_after_decimal;
            {yajl_tok  ReplaceReturn24 = yajl_tok_error; SensorCall(342); return ReplaceReturn24;}
        }
        SensorCall(344);tok = yajl_tok_double;
    }

    /* optional exponent (indicates this is floating point) */
    SensorCall(354);if (c == 'e' || c == 'E') {
        RETURN_IF_EOF;
        SensorCall(346);c = readChar(lexer, jsonText, offset); 

        /* optional sign */
        SensorCall(348);if (c == '+' || c == '-') {
            RETURN_IF_EOF;
            SensorCall(347);c = readChar(lexer, jsonText, offset); 
        }

        SensorCall(352);if (c >= '0' && c <= '9') {
            SensorCall(349);do {
                RETURN_IF_EOF;
                SensorCall(350);c = readChar(lexer, jsonText, offset); 
            } while (c >= '0' && c <= '9');
        } else {
            unreadChar(lexer, offset);
            lexer->error = yajl_lex_missing_integer_after_exponent;
            {yajl_tok  ReplaceReturn23 = yajl_tok_error; SensorCall(351); return ReplaceReturn23;}
        }
        SensorCall(353);tok = yajl_tok_double;
    }
    
    /* we always go "one too far" */
    unreadChar(lexer, offset);
    
    {yajl_tok  ReplaceReturn22 = tok; SensorCall(355); return ReplaceReturn22;}
}

static yajl_tok
yajl_lex_comment(yajl_lexer lexer, const unsigned char * jsonText,
                 size_t jsonTextLen, size_t * offset)
{
    SensorCall(356);unsigned char c;

    yajl_tok tok = yajl_tok_comment;

    RETURN_IF_EOF;    
    SensorCall(357);c = readChar(lexer, jsonText, offset);

    /* either slash or star expected */
    SensorCall(368);if (c == '/') {
        /* now we throw away until end of line */
        SensorCall(358);do {
            RETURN_IF_EOF;    
            SensorCall(359);c = readChar(lexer, jsonText, offset); 
        } while (c != '\n');
    } else {/*33*/SensorCall(360);if (c == '*') {
        /* now we throw away until end of comment */        
        SensorCall(361);for (;;) {
            RETURN_IF_EOF;    
            SensorCall(362);c = readChar(lexer, jsonText, offset); 
            SensorCall(366);if (c == '*') {
                RETURN_IF_EOF;    
                SensorCall(363);c = readChar(lexer, jsonText, offset);                 
                SensorCall(365);if (c == '/') {
                    SensorCall(364);break;
                } else {
                    unreadChar(lexer, offset);
                }
            }
        }
    } else {
        SensorCall(367);lexer->error = yajl_lex_invalid_char;
        tok = yajl_tok_error;
    ;/*34*/}}
    
    {yajl_tok  ReplaceReturn21 = tok; SensorCall(369); return ReplaceReturn21;}
}

yajl_tok
yajl_lex_lex(yajl_lexer lexer, const unsigned char * jsonText,
             size_t jsonTextLen, size_t * offset,
             const unsigned char ** outBuf, size_t * outLen)
{
    SensorCall(370);yajl_tok tok = yajl_tok_error;
    unsigned char c;
    size_t startOffset = *offset;

    *outBuf = NULL;
    *outLen = 0;

    SensorCall(432);for (;;) {
        assert(*offset <= jsonTextLen);

        SensorCall(373);if (*offset >= jsonTextLen) {
            SensorCall(371);tok = yajl_tok_eof;
            SensorCall(372);goto lexed;
        }

        SensorCall(374);c = readChar(lexer, jsonText, offset);

        SensorCall(431);switch (c) {
            case '{':
                SensorCall(375);tok = yajl_tok_left_bracket;
                SensorCall(376);goto lexed;
            case '}':
                SensorCall(377);tok = yajl_tok_right_bracket;
                SensorCall(378);goto lexed;
            case '[':
                SensorCall(379);tok = yajl_tok_left_brace;
                SensorCall(380);goto lexed;
            case ']':
                SensorCall(381);tok = yajl_tok_right_brace;
                SensorCall(382);goto lexed;
            case ',':
                SensorCall(383);tok = yajl_tok_comma;
                SensorCall(384);goto lexed;
            case ':':
                SensorCall(385);tok = yajl_tok_colon;
                SensorCall(386);goto lexed;
            case '\t': case '\n': case '\v': case '\f': case '\r': case ' ':
                SensorCall(387);startOffset++;
                SensorCall(388);break;
            case 't': {
                SensorCall(389);const char * want = "rue";
                SensorCall(396);do {
                    SensorCall(390);if (*offset >= jsonTextLen) {
                        SensorCall(391);tok = yajl_tok_eof;
                        SensorCall(392);goto lexed;
                    }
                    SensorCall(393);c = readChar(lexer, jsonText, offset);
                    SensorCall(395);if (c != *want) {
                        unreadChar(lexer, offset);
                        lexer->error = yajl_lex_invalid_string;
                        tok = yajl_tok_error;
                        SensorCall(394);goto lexed;
                    }
                } while (*(++want));
                SensorCall(397);tok = yajl_tok_bool;
                SensorCall(398);goto lexed;
            }
            case 'f': {
                SensorCall(399);const char * want = "alse";
                SensorCall(406);do {
                    SensorCall(400);if (*offset >= jsonTextLen) {
                        SensorCall(401);tok = yajl_tok_eof;
                        SensorCall(402);goto lexed;
                    }
                    SensorCall(403);c = readChar(lexer, jsonText, offset);
                    SensorCall(405);if (c != *want) {
                        unreadChar(lexer, offset);
                        lexer->error = yajl_lex_invalid_string;
                        tok = yajl_tok_error;
                        SensorCall(404);goto lexed;
                    }
                } while (*(++want));
                SensorCall(407);tok = yajl_tok_bool;
                SensorCall(408);goto lexed;
            }
            case 'n': {
                SensorCall(409);const char * want = "ull";
                SensorCall(416);do {
                    SensorCall(410);if (*offset >= jsonTextLen) {
                        SensorCall(411);tok = yajl_tok_eof;
                        SensorCall(412);goto lexed;
                    }
                    SensorCall(413);c = readChar(lexer, jsonText, offset);
                    SensorCall(415);if (c != *want) {
                        unreadChar(lexer, offset);
                        lexer->error = yajl_lex_invalid_string;
                        tok = yajl_tok_error;
                        SensorCall(414);goto lexed;
                    }
                } while (*(++want));
                SensorCall(417);tok = yajl_tok_null;
                SensorCall(418);goto lexed;
            }
            case '"': {
                SensorCall(419);tok = yajl_lex_string(lexer, (const unsigned char *) jsonText,
                                      jsonTextLen, offset);
                SensorCall(420);goto lexed;
            }
            case '-':
            case '0': case '1': case '2': case '3': case '4': 
            case '5': case '6': case '7': case '8': case '9': {
                /* integer parsing wants to start from the beginning */
                unreadChar(lexer, offset);
                tok = yajl_lex_number(lexer, (const unsigned char *) jsonText,
                                      jsonTextLen, offset);
                SensorCall(421);goto lexed;
            }
            case '/':
                /* hey, look, a probable comment!  If comments are disabled
                 * it's an error. */
                SensorCall(422);if (!lexer->allowComments) {
                    unreadChar(lexer, offset);
                    lexer->error = yajl_lex_unallowed_comment;
                    tok = yajl_tok_error;
                    SensorCall(423);goto lexed;
                }
                /* if comments are enabled, then we should try to lex
                 * the thing.  possible outcomes are
                 * - successful lex (tok_comment, which means continue),
                 * - malformed comment opening (slash not followed by
                 *   '*' or '/') (tok_error)
                 * - eof hit. (tok_eof) */
                SensorCall(424);tok = yajl_lex_comment(lexer, (const unsigned char *) jsonText,
                                       jsonTextLen, offset);
                SensorCall(427);if (tok == yajl_tok_comment) {
                    /* "error" is silly, but that's the initial
                     * state of tok.  guilty until proven innocent. */  
                    SensorCall(425);tok = yajl_tok_error;
                    yajl_buf_clear(lexer->buf);
                    lexer->bufInUse = 0;
                    startOffset = *offset; 
                    SensorCall(426);break;
                }
                /* hit error or eof, bail */
                SensorCall(428);goto lexed;
            default:
                SensorCall(429);lexer->error = yajl_lex_invalid_char;
                tok = yajl_tok_error;
                SensorCall(430);goto lexed;
        }
    }


  lexed:
    /* need to append to buffer if the buffer is in use or
     * if it's an EOF token */
    SensorCall(440);if (tok == yajl_tok_eof || lexer->bufInUse) {
        SensorCall(433);if (!lexer->bufInUse) {/*1*/SensorCall(434);yajl_buf_clear(lexer->buf);/*2*/}
        SensorCall(435);lexer->bufInUse = 1;
        yajl_buf_append(lexer->buf, jsonText + startOffset, *offset - startOffset);
        lexer->bufOff = 0;
        
        SensorCall(437);if (tok != yajl_tok_eof) {
            SensorCall(436);*outBuf = yajl_buf_data(lexer->buf);
            *outLen = yajl_buf_len(lexer->buf);
            lexer->bufInUse = 0;
        }
    } else {/*3*/SensorCall(438);if (tok != yajl_tok_error) {
        SensorCall(439);*outBuf = jsonText + startOffset;
        *outLen = *offset - startOffset;
    ;/*4*/}}

    /* special case for strings. skip the quotes. */
    SensorCall(442);if (tok == yajl_tok_string || tok == yajl_tok_string_with_escapes)
    {
        assert(*outLen >= 2);
        SensorCall(441);(*outBuf)++;
        *outLen -= 2; 
    }


#ifdef YAJL_LEXER_DEBUG
    if (tok == yajl_tok_error) {
        printf("lexical error: %s\n",
               yajl_lex_error_to_string(yajl_lex_get_error(lexer)));
    } else if (tok == yajl_tok_eof) {
        printf("EOF hit\n");
    } else {
        printf("lexed %s: '", tokToStr(tok));
        fwrite(*outBuf, 1, *outLen, stdout);
        printf("'\n");
    }
#endif

    {yajl_tok  ReplaceReturn20 = tok; SensorCall(443); return ReplaceReturn20;}
}

const char *
yajl_lex_error_to_string(yajl_lex_error error)
{
    SensorCall(444);switch (error) {
        case yajl_lex_e_ok:
            {const char * ReplaceReturn19 = "ok, no error"; SensorCall(445); return ReplaceReturn19;}
        case yajl_lex_string_invalid_utf8:
            {const char * ReplaceReturn18 = "invalid bytes in UTF8 string."; SensorCall(446); return ReplaceReturn18;}
        case yajl_lex_string_invalid_escaped_char:
            {const char * ReplaceReturn17 = "inside a string, '\\' occurs before a character "
                   "which it may not."; SensorCall(447); return ReplaceReturn17;}
        case yajl_lex_string_invalid_json_char:            
            {const char * ReplaceReturn16 = "invalid character inside string."; SensorCall(448); return ReplaceReturn16;}
        case yajl_lex_string_invalid_hex_char:
            {const char * ReplaceReturn15 = "invalid (non-hex) character occurs after '\\u' inside "
                   "string."; SensorCall(449); return ReplaceReturn15;}
        case yajl_lex_invalid_char:
            {const char * ReplaceReturn14 = "invalid char in json text."; SensorCall(450); return ReplaceReturn14;}
        case yajl_lex_invalid_string:
            {const char * ReplaceReturn13 = "invalid string in json text."; SensorCall(451); return ReplaceReturn13;}
        case yajl_lex_missing_integer_after_exponent:
            {const char * ReplaceReturn12 = "malformed number, a digit is required after the exponent."; SensorCall(452); return ReplaceReturn12;}
        case yajl_lex_missing_integer_after_decimal:
            {const char * ReplaceReturn11 = "malformed number, a digit is required after the "
                   "decimal point."; SensorCall(453); return ReplaceReturn11;}
        case yajl_lex_missing_integer_after_minus:
            {const char * ReplaceReturn10 = "malformed number, a digit is required after the "
                   "minus sign."; SensorCall(454); return ReplaceReturn10;}
        case yajl_lex_unallowed_comment:
            {const char * ReplaceReturn9 = "probable comment found in input text, comments are "
                   "not enabled."; SensorCall(455); return ReplaceReturn9;}
    }
    {const char * ReplaceReturn8 = "unknown error code"; SensorCall(456); return ReplaceReturn8;}
}


/** allows access to more specific information about the lexical
 *  error when yajl_lex_lex returns yajl_tok_error. */
yajl_lex_error
yajl_lex_get_error(yajl_lexer lexer)
{
    SensorCall(457);if (lexer == NULL) {/*5*/{yajl_lex_error  ReplaceReturn7 = (yajl_lex_error) -1; SensorCall(458); return ReplaceReturn7;}/*6*/}
    {yajl_lex_error  ReplaceReturn6 = lexer->error; SensorCall(459); return ReplaceReturn6;}
}

size_t yajl_lex_current_line(yajl_lexer lexer)
{
    {size_t  ReplaceReturn5 = lexer->lineOff; SensorCall(460); return ReplaceReturn5;}
}

size_t yajl_lex_current_char(yajl_lexer lexer)
{
    {size_t  ReplaceReturn4 = lexer->charOff; SensorCall(461); return ReplaceReturn4;}
}

yajl_tok yajl_lex_peek(yajl_lexer lexer, const unsigned char * jsonText,
                       size_t jsonTextLen, size_t offset)
{
    SensorCall(462);const unsigned char * outBuf;
    size_t outLen;
    size_t bufLen = yajl_buf_len(lexer->buf);
    size_t bufOff = lexer->bufOff;
    unsigned int bufInUse = lexer->bufInUse;
    yajl_tok tok;
    
    tok = yajl_lex_lex(lexer, jsonText, jsonTextLen, &offset,
                       &outBuf, &outLen);

    lexer->bufOff = bufOff;
    lexer->bufInUse = bufInUse;
    yajl_buf_truncate(lexer->buf, bufLen);
    
    {yajl_tok  ReplaceReturn3 = tok; SensorCall(463); return ReplaceReturn3;}
}
