/*
 * Copyright (c) 2007-2011, Lloyd Hilaiel <lloyd@hilaiel.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "api/yajl_parse.h"
#include "var/tmp/sensor.h"
#include "yajl_lex.h"
#include "yajl_parser.h"
#include "yajl_alloc.h"

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>

const char *
yajl_status_to_string(yajl_status stat)
{
    SensorCall(111);const char * statStr = "unknown";
    SensorCall(118);switch (stat) {
        case yajl_status_ok:
            SensorCall(112);statStr = "ok, no error";
            SensorCall(113);break;
        case yajl_status_client_canceled:
            SensorCall(114);statStr = "client canceled parse";
            SensorCall(115);break;
        case yajl_status_error:
            SensorCall(116);statStr = "parse error";
            SensorCall(117);break;
    }
    {const char * ReplaceReturn123 = statStr; SensorCall(119); return ReplaceReturn123;}
}

yajl_handle
yajl_alloc(const yajl_callbacks * callbacks,
           yajl_alloc_funcs * afs,
           void * ctx)
{
    SensorCall(120);yajl_handle hand = NULL;
    yajl_alloc_funcs afsBuffer;

    /* first order of business is to set up memory allocation routines */
    SensorCall(124);if (afs != NULL) {
        SensorCall(121);if (afs->malloc == NULL || afs->realloc == NULL || afs->free == NULL)
        {
            {yajl_handle  ReplaceReturn122 = NULL; SensorCall(122); return ReplaceReturn122;}
        }
    } else {
        SensorCall(123);yajl_set_default_alloc_funcs(&afsBuffer);
        afs = &afsBuffer;
    }

    SensorCall(125);hand = (yajl_handle) YA_MALLOC(afs, sizeof(struct yajl_handle_t));

    /* copy in pointers to allocation routines */
    memcpy((void *) &(hand->alloc), (void *) afs, sizeof(yajl_alloc_funcs));

    hand->callbacks = callbacks;
    hand->ctx = ctx;
    hand->lexer = NULL; 
    hand->bytesConsumed = 0;
    hand->decodeBuf = yajl_buf_alloc(&(hand->alloc));
    hand->flags	    = 0;
    yajl_bs_init(hand->stateStack, &(hand->alloc));
    yajl_bs_push(hand->stateStack, yajl_state_start);

    {yajl_handle  ReplaceReturn121 = hand; SensorCall(126); return ReplaceReturn121;}
}

int
yajl_config(yajl_handle h, yajl_option opt, ...)
{
    SensorCall(127);int rv = 1;
    va_list ap;
    va_start(ap, opt);

    SensorCall(133);switch(opt) {
        case yajl_allow_comments:
        case yajl_dont_validate_strings:
        case yajl_allow_trailing_garbage:
        case yajl_allow_multiple_values:
        case yajl_allow_partial_values:
            SensorCall(128);if (va_arg(ap, int)) {/*1*/SensorCall(129);h->flags |= opt;/*2*/}
            else {/*3*/SensorCall(130);h->flags &= ~opt;/*4*/}
            SensorCall(131);break;
        default:
            SensorCall(132);rv = 0;
    }
    va_end(ap);

    {int  ReplaceReturn120 = rv; SensorCall(134); return ReplaceReturn120;}
}

void
yajl_free(yajl_handle handle)
{
SensorCall(135);    yajl_bs_free(handle->stateStack);
    SensorCall(136);yajl_buf_free(handle->decodeBuf);
    SensorCall(138);if (handle->lexer) {
        SensorCall(137);yajl_lex_free(handle->lexer);
        handle->lexer = NULL;
    }
    YA_FREE(&(handle->alloc), handle);
}

yajl_status
yajl_parse(yajl_handle hand, const unsigned char * jsonText,
           size_t jsonTextLen)
{
    SensorCall(139);yajl_status status;

    /* lazy allocation of the lexer */
    SensorCall(141);if (hand->lexer == NULL) {
        SensorCall(140);hand->lexer = yajl_lex_alloc(&(hand->alloc),
                                     hand->flags & yajl_allow_comments,
                                     !(hand->flags & yajl_dont_validate_strings));
    }

    SensorCall(142);status = yajl_do_parse(hand, jsonText, jsonTextLen);
    {yajl_status  ReplaceReturn119 = status; SensorCall(143); return ReplaceReturn119;}
}


yajl_status
yajl_complete_parse(yajl_handle hand)
{
    /* The lexer is lazy allocated in the first call to parse.  if parse is
     * never called, then no data was provided to parse at all.  This is a
     * "premature EOF" error unless yajl_allow_partial_values is specified.
     * allocating the lexer now is the simplest possible way to handle this
     * case while preserving all the other semantics of the parser
     * (multiple values, partial values, etc). */
    SensorCall(144);if (hand->lexer == NULL) {
        SensorCall(145);hand->lexer = yajl_lex_alloc(&(hand->alloc),
                                     hand->flags & yajl_allow_comments,
                                     !(hand->flags & yajl_dont_validate_strings));
    }

    {yajl_status  ReplaceReturn118 = yajl_do_finish(hand); SensorCall(146); return ReplaceReturn118;}
}

unsigned char *
yajl_get_error(yajl_handle hand, int verbose,
               const unsigned char * jsonText, size_t jsonTextLen)
{
    {unsigned char * ReplaceReturn117 = yajl_render_error_string(hand, jsonText, jsonTextLen, verbose); SensorCall(147); return ReplaceReturn117;}
}

size_t
yajl_get_bytes_consumed(yajl_handle hand)
{
    SensorCall(148);if (!hand) {/*5*/{size_t  ReplaceReturn116 = 0; SensorCall(149); return ReplaceReturn116;}/*6*/}
    else {/*7*/{size_t  ReplaceReturn115 = hand->bytesConsumed; SensorCall(150); return ReplaceReturn115;}/*8*/}
SensorCall(151);}


void
yajl_free_error(yajl_handle hand, unsigned char * str)
{
SensorCall(152);    /* use memory allocation functions if set */
    YA_FREE(&(hand->alloc), str);
SensorCall(153);}

/* XXX: add utility routines to parse from file */
