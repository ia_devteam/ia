/*
 * Copyright (c) 2010-2011  Florian Forster  <ff at octo.it>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdlib.h>
#include "var/tmp/sensor.h"
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#include "api/yajl_tree.h"
#include "api/yajl_parse.h"

#include "yajl_parser.h"

#if defined(_WIN32) || defined(WIN32)
#define snprintf sprintf_s
#endif

#define STATUS_CONTINUE 1
#define STATUS_ABORT    0

struct stack_elem_s;
typedef struct stack_elem_s stack_elem_t;
struct stack_elem_s
{
    char * key;
    yajl_val value;
    stack_elem_t *next;
};

struct context_s
{
    stack_elem_t *stack;
    yajl_val root;
    char *errbuf;
    size_t errbuf_size;
};
typedef struct context_s context_t;

#define RETURN_ERROR(ctx,retval,...) {                                  \
        if ((ctx)->errbuf != NULL)                                      \
            snprintf ((ctx)->errbuf, (ctx)->errbuf_size, __VA_ARGS__);  \
        return (retval);                                                \
    }

static yajl_val value_alloc (yajl_type type)
{
    SensorCall(626);yajl_val v;

    v = malloc (sizeof (*v));
    SensorCall(628);if (v == NULL) {/*11*/{yajl_val  ReplaceReturn74 = (NULL); SensorCall(627); return ReplaceReturn74;}/*12*/}
    SensorCall(629);memset (v, 0, sizeof (*v));
    v->type = type;

    {yajl_val  ReplaceReturn73 = (v); SensorCall(630); return ReplaceReturn73;}
}

static void yajl_object_free (yajl_val v)
{
    SensorCall(631);size_t i;

    SensorCall(633);if (!YAJL_IS_OBJECT(v)) {/*13*/SensorCall(632);return;/*14*/}

    SensorCall(635);for (i = 0; i < v->u.object.len; i++)
    {
        SensorCall(634);free((char *) v->u.object.keys[i]);
        v->u.object.keys[i] = NULL;
        yajl_tree_free (v->u.object.values[i]);
        v->u.object.values[i] = NULL;
    }

    SensorCall(636);free((void*) v->u.object.keys);
    free(v->u.object.values);
    free(v);
SensorCall(637);}

static void yajl_array_free (yajl_val v)
{
    SensorCall(638);size_t i;

    SensorCall(640);if (!YAJL_IS_ARRAY(v)) {/*15*/SensorCall(639);return;/*16*/}

    SensorCall(642);for (i = 0; i < v->u.array.len; i++)
    {
        SensorCall(641);yajl_tree_free (v->u.array.values[i]);
        v->u.array.values[i] = NULL;
    }

    SensorCall(643);free(v->u.array.values);
    free(v);
SensorCall(644);}

/*
 * Parsing nested objects and arrays is implemented using a stack. When a new
 * object or array starts (a curly or a square opening bracket is read), an
 * appropriate value is pushed on the stack. When the end of the object is
 * reached (an appropriate closing bracket has been read), the value is popped
 * off the stack and added to the enclosing object using "context_add_value".
 */
static int context_push(context_t *ctx, yajl_val v)
{
    SensorCall(645);stack_elem_t *stack;

    stack = malloc (sizeof (*stack));
    SensorCall(646);if (stack == NULL)
        RETURN_ERROR (ctx, ENOMEM, "Out of memory");
    SensorCall(647);memset (stack, 0, sizeof (*stack));

    assert ((ctx->stack == NULL)
            || YAJL_IS_OBJECT (v)
            || YAJL_IS_ARRAY (v));

    stack->value = v;
    stack->next = ctx->stack;
    ctx->stack = stack;

    {int  ReplaceReturn72 = (0); SensorCall(648); return ReplaceReturn72;}
}

static yajl_val context_pop(context_t *ctx)
{
    SensorCall(649);stack_elem_t *stack;
    yajl_val v;

    SensorCall(650);if (ctx->stack == NULL)
        RETURN_ERROR (ctx, NULL, "context_pop: "
                      "Bottom of stack reached prematurely");

    SensorCall(651);stack = ctx->stack;
    ctx->stack = stack->next;

    v = stack->value;

    free (stack);

    {yajl_val  ReplaceReturn71 = (v); SensorCall(652); return ReplaceReturn71;}
}

static int object_add_keyval(context_t *ctx,
                             yajl_val obj, char *key, yajl_val value)
{
    SensorCall(653);const char **tmpk;
    yajl_val *tmpv;

    /* We're checking for NULL in "context_add_value" or its callers. */
    assert (ctx != NULL);
    assert (obj != NULL);
    assert (key != NULL);
    assert (value != NULL);

    /* We're assuring that "obj" is an object in "context_add_value". */
    assert(YAJL_IS_OBJECT(obj));

    tmpk = realloc((void *) obj->u.object.keys, sizeof(*(obj->u.object.keys)) * (obj->u.object.len + 1));
    SensorCall(654);if (tmpk == NULL)
        RETURN_ERROR(ctx, ENOMEM, "Out of memory");
    SensorCall(655);obj->u.object.keys = tmpk;

    tmpv = realloc(obj->u.object.values, sizeof (*obj->u.object.values) * (obj->u.object.len + 1));
    SensorCall(656);if (tmpv == NULL)
        RETURN_ERROR(ctx, ENOMEM, "Out of memory");
    SensorCall(657);obj->u.object.values = tmpv;

    obj->u.object.keys[obj->u.object.len] = key;
    obj->u.object.values[obj->u.object.len] = value;
    obj->u.object.len++;

    {int  ReplaceReturn70 = (0); SensorCall(658); return ReplaceReturn70;}
}

static int array_add_value (context_t *ctx,
                            yajl_val array, yajl_val value)
{
    SensorCall(659);yajl_val *tmp;

    /* We're checking for NULL pointers in "context_add_value" or its
     * callers. */
    assert (ctx != NULL);
    assert (array != NULL);
    assert (value != NULL);

    /* "context_add_value" will only call us with array values. */
    assert(YAJL_IS_ARRAY(array));
    
    tmp = realloc(array->u.array.values,
                  sizeof(*(array->u.array.values)) * (array->u.array.len + 1));
    SensorCall(660);if (tmp == NULL)
        RETURN_ERROR(ctx, ENOMEM, "Out of memory");
    SensorCall(661);array->u.array.values = tmp;
    array->u.array.values[array->u.array.len] = value;
    array->u.array.len++;

    {int  ReplaceReturn69 = 0; SensorCall(662); return ReplaceReturn69;}
}

/*
 * Add a value to the value on top of the stack or the "root" member in the
 * context if the end of the parsing process is reached.
 */
static int context_add_value (context_t *ctx, yajl_val v)
{
    /* We're checking for NULL values in all the calling functions. */
    assert (ctx != NULL);
    assert (v != NULL);

    /*
     * There are three valid states in which this function may be called:
     *   - There is no value on the stack => This is the only value. This is the
     *     last step done when parsing a document. We assign the value to the
     *     "root" member and return.
     *   - The value on the stack is an object. In this case store the key on the
     *     stack or, if the key has already been read, add key and value to the
     *     object.
     *   - The value on the stack is an array. In this case simply add the value
     *     and return.
     */
    SensorCall(674);if (ctx->stack == NULL)
    {
        assert (ctx->root == NULL);
        SensorCall(663);ctx->root = v;
        {int  ReplaceReturn68 = (0); SensorCall(664); return ReplaceReturn68;}
    }
    else {/*17*/SensorCall(665);if (YAJL_IS_OBJECT (ctx->stack->value))
    {
        SensorCall(666);if (ctx->stack->key == NULL)
        {
            SensorCall(667);if (!YAJL_IS_STRING (v))
                RETURN_ERROR (ctx, EINVAL, "context_add_value: "
                              "Object key is not a string (%#04x)",
                              v->type);

            SensorCall(668);ctx->stack->key = v->u.string;
            v->u.string = NULL;
            free(v);
            {int  ReplaceReturn67 = (0); SensorCall(669); return ReplaceReturn67;}
        }
        else /* if (ctx->key != NULL) */
        {
            SensorCall(670);char * key;

            key = ctx->stack->key;
            ctx->stack->key = NULL;
            {int  ReplaceReturn66 = (object_add_keyval (ctx, ctx->stack->value, key, v)); SensorCall(671); return ReplaceReturn66;}
        }
    }
    else {/*19*/SensorCall(672);if (YAJL_IS_ARRAY (ctx->stack->value))
    {
        {int  ReplaceReturn65 = (array_add_value (ctx, ctx->stack->value, v)); SensorCall(673); return ReplaceReturn65;}
    }
    else
    {
        RETURN_ERROR (ctx, EINVAL, "context_add_value: Cannot add value to "
                      "a value of type %#04x (not a composite type)",
                      ctx->stack->value->type);
    ;/*20*/}/*18*/}}
SensorCall(675);}

static int handle_string (void *ctx,
                          const unsigned char *string, size_t string_length)
{
    SensorCall(676);yajl_val v;

    v = value_alloc (yajl_t_string);
    SensorCall(677);if (v == NULL)
        RETURN_ERROR ((context_t *) ctx, STATUS_ABORT, "Out of memory");

    SensorCall(678);v->u.string = malloc (string_length + 1);
    SensorCall(680);if (v->u.string == NULL)
    {
        SensorCall(679);free (v);
        RETURN_ERROR ((context_t *) ctx, STATUS_ABORT, "Out of memory");
    }
    SensorCall(681);memcpy(v->u.string, string, string_length);
    v->u.string[string_length] = 0;

    {int  ReplaceReturn64 = ((context_add_value (ctx, v) == 0) ? STATUS_CONTINUE : STATUS_ABORT); SensorCall(682); return ReplaceReturn64;}
}

static int handle_number (void *ctx, const char *string, size_t string_length)
{
    SensorCall(683);yajl_val v;
    char *endptr;

    v = value_alloc(yajl_t_number);
    SensorCall(684);if (v == NULL)
        RETURN_ERROR((context_t *) ctx, STATUS_ABORT, "Out of memory");

    SensorCall(685);v->u.number.r = malloc(string_length + 1);
    SensorCall(687);if (v->u.number.r == NULL)
    {
        SensorCall(686);free(v);
        RETURN_ERROR((context_t *) ctx, STATUS_ABORT, "Out of memory");
    }
    SensorCall(688);memcpy(v->u.number.r, string, string_length);
    v->u.number.r[string_length] = 0;

    v->u.number.flags = 0;

    errno = 0;
    v->u.number.i = yajl_parse_integer((const unsigned char *) v->u.number.r,
                                       strlen(v->u.number.r));
    SensorCall(689);if (errno == 0)
        v->u.number.flags |= YAJL_NUMBER_INT_VALID;

    SensorCall(690);endptr = NULL;
    errno = 0;
    v->u.number.d = strtod(v->u.number.r, &endptr);
    SensorCall(691);if ((errno == 0) && (endptr != NULL) && (*endptr == 0))
        v->u.number.flags |= YAJL_NUMBER_DOUBLE_VALID;

    {int  ReplaceReturn63 = ((context_add_value(ctx, v) == 0) ? STATUS_CONTINUE : STATUS_ABORT); SensorCall(692); return ReplaceReturn63;}
}

static int handle_start_map (void *ctx)
{
    SensorCall(693);yajl_val v;

    v = value_alloc(yajl_t_object);
    SensorCall(694);if (v == NULL)
        RETURN_ERROR ((context_t *) ctx, STATUS_ABORT, "Out of memory");

    SensorCall(695);v->u.object.keys = NULL;
    v->u.object.values = NULL;
    v->u.object.len = 0;

    {int  ReplaceReturn62 = ((context_push (ctx, v) == 0) ? STATUS_CONTINUE : STATUS_ABORT); SensorCall(696); return ReplaceReturn62;}
}

static int handle_end_map (void *ctx)
{
    SensorCall(697);yajl_val v;

    v = context_pop (ctx);
    SensorCall(699);if (v == NULL)
        {/*21*/{int  ReplaceReturn61 = (STATUS_ABORT); SensorCall(698); return ReplaceReturn61;}/*22*/}

    {int  ReplaceReturn60 = ((context_add_value (ctx, v) == 0) ? STATUS_CONTINUE : STATUS_ABORT); SensorCall(700); return ReplaceReturn60;}
}

static int handle_start_array (void *ctx)
{
    SensorCall(701);yajl_val v;

    v = value_alloc(yajl_t_array);
    SensorCall(702);if (v == NULL)
        RETURN_ERROR ((context_t *) ctx, STATUS_ABORT, "Out of memory");

    SensorCall(703);v->u.array.values = NULL;
    v->u.array.len = 0;

    {int  ReplaceReturn59 = ((context_push (ctx, v) == 0) ? STATUS_CONTINUE : STATUS_ABORT); SensorCall(704); return ReplaceReturn59;}
}

static int handle_end_array (void *ctx)
{
    SensorCall(705);yajl_val v;

    v = context_pop (ctx);
    SensorCall(707);if (v == NULL)
        {/*23*/{int  ReplaceReturn58 = (STATUS_ABORT); SensorCall(706); return ReplaceReturn58;}/*24*/}

    {int  ReplaceReturn57 = ((context_add_value (ctx, v) == 0) ? STATUS_CONTINUE : STATUS_ABORT); SensorCall(708); return ReplaceReturn57;}
}

static int handle_boolean (void *ctx, int boolean_value)
{
    SensorCall(709);yajl_val v;

    v = value_alloc (boolean_value ? yajl_t_true : yajl_t_false);
    SensorCall(710);if (v == NULL)
        RETURN_ERROR ((context_t *) ctx, STATUS_ABORT, "Out of memory");

    {int  ReplaceReturn56 = ((context_add_value (ctx, v) == 0) ? STATUS_CONTINUE : STATUS_ABORT); SensorCall(711); return ReplaceReturn56;}
}

static int handle_null (void *ctx)
{
    SensorCall(712);yajl_val v;

    v = value_alloc (yajl_t_null);
    SensorCall(713);if (v == NULL)
        RETURN_ERROR ((context_t *) ctx, STATUS_ABORT, "Out of memory");

    {int  ReplaceReturn55 = ((context_add_value (ctx, v) == 0) ? STATUS_CONTINUE : STATUS_ABORT); SensorCall(714); return ReplaceReturn55;}
}

/*
 * Public functions
 */
yajl_val yajl_tree_parse (const char *input,
                          char *error_buffer, size_t error_buffer_size)
{
    SensorCall(715);static const yajl_callbacks callbacks =
        {
            /* null        = */ handle_null,
            /* boolean     = */ handle_boolean,
            /* integer     = */ NULL,
            /* double      = */ NULL,
            /* number      = */ handle_number,
            /* string      = */ handle_string,
            /* start map   = */ handle_start_map,
            /* map key     = */ handle_string,
            /* end map     = */ handle_end_map,
            /* start array = */ handle_start_array,
            /* end array   = */ handle_end_array
        };

    yajl_handle handle;
    yajl_status status;
    char * internal_err_str;
	context_t ctx = { NULL, NULL, NULL, 0 };

	ctx.errbuf = error_buffer;
	ctx.errbuf_size = error_buffer_size;

    SensorCall(717);if (error_buffer != NULL)
        {/*1*/SensorCall(716);memset (error_buffer, 0, error_buffer_size);/*2*/}

    SensorCall(718);handle = yajl_alloc (&callbacks, NULL, &ctx);
    yajl_config(handle, yajl_allow_comments, 1);

    status = yajl_parse(handle,
                        (unsigned char *) input,
                        strlen (input));
    status = yajl_complete_parse (handle);
    SensorCall(723);if (status != yajl_status_ok) {
        SensorCall(719);if (error_buffer != NULL && error_buffer_size > 0) {
               SensorCall(720);internal_err_str = (char *) yajl_get_error(handle, 1,
                     (const unsigned char *) input,
                     strlen(input));
             snprintf(error_buffer, error_buffer_size, "%s", internal_err_str);
             YA_FREE(&(handle->alloc), internal_err_str);
        }
        SensorCall(721);yajl_free (handle);
        {yajl_val  ReplaceReturn54 = NULL; SensorCall(722); return ReplaceReturn54;}
    }

    SensorCall(724);yajl_free (handle);
    {yajl_val  ReplaceReturn53 = (ctx.root); SensorCall(725); return ReplaceReturn53;}
}

yajl_val yajl_tree_get(yajl_val n, const char ** path, yajl_type type)
{
    SensorCall(726);if (!path) return NULL;
    SensorCall(736);while (n && *path) {
        SensorCall(727);unsigned int i;
        int len;

        SensorCall(728);if (n->type != yajl_t_object) return NULL;
        SensorCall(729);len = n->u.object.len;
        SensorCall(733);for (i = 0; i < len; i++) {
            SensorCall(730);if (!strcmp(*path, n->u.object.keys[i])) {
                SensorCall(731);n = n->u.object.values[i];
                SensorCall(732);break;
            }
        }
        SensorCall(734);if (i == len) return NULL;
        SensorCall(735);path++;
    }
    SensorCall(737);if (n && type != yajl_t_any && type != n->type) n = NULL;
    {yajl_val  ReplaceReturn52 = n; SensorCall(738); return ReplaceReturn52;}
}

void yajl_tree_free (yajl_val v)
{
    SensorCall(739);if (v == NULL) {/*3*/SensorCall(740);return;/*4*/}

    SensorCall(749);if (YAJL_IS_STRING(v))
    {
        SensorCall(741);free(v->u.string);
        free(v);
    }
    else {/*5*/SensorCall(742);if (YAJL_IS_NUMBER(v))
    {
        SensorCall(743);free(v->u.number.r);
        free(v);
    }
    else {/*7*/SensorCall(744);if (YAJL_GET_OBJECT(v))
    {
        SensorCall(745);yajl_object_free(v);
    }
    else {/*9*/SensorCall(746);if (YAJL_GET_ARRAY(v))
    {
        SensorCall(747);yajl_array_free(v);
    }
    else /* if (yajl_t_true or yajl_t_false or yajl_t_null) */
    {
        SensorCall(748);free(v);
    ;/*10*/}/*8*/}/*6*/}}
SensorCall(750);}
