/*
 * Copyright (c) 2007-2011, Lloyd Hilaiel <lloyd@hilaiel.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "api/yajl_parse.h"
#include "var/tmp/sensor.h"
#include "yajl_lex.h"
#include "yajl_parser.h"
#include "yajl_encode.h"
#include "yajl_bytestack.h"

#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>

#define MAX_VALUE_TO_MULTIPLY ((LLONG_MAX / 10) + (LLONG_MAX % 10))

 /* same semantics as strtol */
long long
yajl_parse_integer(const unsigned char *number, unsigned int length)
{
    SensorCall(464);long long ret  = 0;
    long sign = 1;
    const unsigned char *pos = number;
    SensorCall(466);if (*pos == '-') { SensorCall(465);pos++; sign = -1; }
    SensorCall(468);if (*pos == '+') { SensorCall(467);pos++; }

    SensorCall(477);while (pos < number + length) {
        SensorCall(469);if ( ret > MAX_VALUE_TO_MULTIPLY ) {
            errno = ERANGE;
            {long long  ReplaceReturn51 = sign == 1 ? LLONG_MAX : LLONG_MIN; SensorCall(470); return ReplaceReturn51;}
        }
        SensorCall(471);ret *= 10;
        SensorCall(473);if (LLONG_MAX - ret < (*pos - '0')) {
            errno = ERANGE;
            {long long  ReplaceReturn50 = sign == 1 ? LLONG_MAX : LLONG_MIN; SensorCall(472); return ReplaceReturn50;}
        }
        SensorCall(475);if (*pos < '0' || *pos > '9') {
            errno = ERANGE;
            {long long  ReplaceReturn49 = sign == 1 ? LLONG_MAX : LLONG_MIN; SensorCall(474); return ReplaceReturn49;}
        }
        SensorCall(476);ret += (*pos++ - '0');
    }

    {long long  ReplaceReturn48 = sign * ret; SensorCall(478); return ReplaceReturn48;}
}

unsigned char *
yajl_render_error_string(yajl_handle hand, const unsigned char * jsonText,
                         size_t jsonTextLen, int verbose)
{
    SensorCall(479);size_t offset = hand->bytesConsumed;
    unsigned char * str;
    const char * errorType = NULL;
    const char * errorText = NULL;
    char text[72];
    const char * arrow = "                     (right here) ------^\n";

    SensorCall(484);if (yajl_bs_current(hand->stateStack) == yajl_state_parse_error) {
        SensorCall(480);errorType = "parse";
        errorText = hand->parseError;
    } else {/*21*/SensorCall(481);if (yajl_bs_current(hand->stateStack) == yajl_state_lexical_error) {
        SensorCall(482);errorType = "lexical";
        errorText = yajl_lex_error_to_string(yajl_lex_get_error(hand->lexer));
    } else {
        SensorCall(483);errorType = "unknown";
    ;/*22*/}}

    {
        SensorCall(485);size_t memneeded = 0;
        memneeded += strlen(errorType);
        memneeded += strlen(" error");
        SensorCall(487);if (errorText != NULL) {
            SensorCall(486);memneeded += strlen(": ");
            memneeded += strlen(errorText);
        }
        SensorCall(488);str = (unsigned char *) YA_MALLOC(&(hand->alloc), memneeded + 2);
        SensorCall(489);if (!str) return NULL;
        SensorCall(490);str[0] = 0;
        strcat((char *) str, errorType);
        strcat((char *) str, " error");
        SensorCall(492);if (errorText != NULL) {
            SensorCall(491);strcat((char *) str, ": ");
            strcat((char *) str, errorText);
        }
        SensorCall(493);strcat((char *) str, "\n");
    }

    /* now we append as many spaces as needed to make sure the error
     * falls at char 41, if verbose was specified */
    SensorCall(505);if (verbose) {
        SensorCall(494);size_t start, end, i;
        size_t spacesNeeded;

        spacesNeeded = (offset < 30 ? 40 - offset : 10);
        start = (offset >= 30 ? offset - 30 : 0);
        end = (offset + 30 > jsonTextLen ? jsonTextLen : offset + 30);

        SensorCall(496);for (i=0;i<spacesNeeded;i++) {/*23*/SensorCall(495);text[i] = ' ';/*24*/}

        SensorCall(500);for (;start < end;start++, i++) {
            SensorCall(497);if (jsonText[start] != '\n' && jsonText[start] != '\r')
            {
                SensorCall(498);text[i] = jsonText[start];
            }
            else
            {
                SensorCall(499);text[i] = ' ';
            }
        }
        assert(i <= 71);
        SensorCall(501);text[i++] = '\n';
        text[i] = 0;
        {
            char * newStr = (char *)
                YA_MALLOC(&(hand->alloc), (unsigned int)(strlen((char *) str) +
                                                         strlen((char *) text) +
                                                         strlen(arrow) + 1));
            SensorCall(503);if (newStr) {
                SensorCall(502);newStr[0] = 0;
                strcat((char *) newStr, (char *) str);
                strcat((char *) newStr, text);
                strcat((char *) newStr, arrow);
            }
            YA_FREE(&(hand->alloc), str);
            SensorCall(504);str = (unsigned char *) newStr;
        }
    }
    {unsigned char * ReplaceReturn47 = str; SensorCall(506); return ReplaceReturn47;}
}

/* check for client cancelation */
#define _CC_CHK(x)                                                \
    if (!(x)) {                                                   \
        yajl_bs_set(hand->stateStack, yajl_state_parse_error);    \
        hand->parseError =                                        \
            "client cancelled parse via callback return value";   \
        return yajl_status_client_canceled;                       \
    }


yajl_status
yajl_do_finish(yajl_handle hand)
{
    SensorCall(507);yajl_status stat;
    stat = yajl_do_parse(hand,(const unsigned char *) " ",1);

    SensorCall(509);if (stat != yajl_status_ok) {/*19*/{yajl_status  ReplaceReturn46 = stat; SensorCall(508); return ReplaceReturn46;}/*20*/}

    SensorCall(516);switch(yajl_bs_current(hand->stateStack))
    {
        case yajl_state_parse_error:
        case yajl_state_lexical_error:
            {yajl_status  ReplaceReturn45 = yajl_status_error; SensorCall(510); return ReplaceReturn45;}
        case yajl_state_got_value:
        case yajl_state_parse_complete:
            {yajl_status  ReplaceReturn44 = yajl_status_ok; SensorCall(511); return ReplaceReturn44;}
        default:
            SensorCall(512);if (!(hand->flags & yajl_allow_partial_values))
            {
                yajl_bs_set(hand->stateStack, yajl_state_parse_error);
                SensorCall(513);hand->parseError = "premature EOF";
                {yajl_status  ReplaceReturn43 = yajl_status_error; SensorCall(514); return ReplaceReturn43;}
            }
            {yajl_status  ReplaceReturn42 = yajl_status_ok; SensorCall(515); return ReplaceReturn42;}
    }
SensorCall(517);}

yajl_status
yajl_do_parse(yajl_handle hand, const unsigned char * jsonText,
              size_t jsonTextLen)
{
    SensorCall(518);yajl_tok tok;
    const unsigned char * buf;
    size_t bufLen;
    size_t * offset = &(hand->bytesConsumed);

    *offset = 0;

  around_again:
    SensorCall(623);switch (yajl_bs_current(hand->stateStack)) {
        case yajl_state_parse_complete:
            SensorCall(519);if (hand->flags & yajl_allow_multiple_values) {
                yajl_bs_set(hand->stateStack, yajl_state_got_value);
                SensorCall(520);goto around_again;
            }
            SensorCall(526);if (!(hand->flags & yajl_allow_trailing_garbage)) {
                SensorCall(521);if (*offset != jsonTextLen) {
                    SensorCall(522);tok = yajl_lex_lex(hand->lexer, jsonText, jsonTextLen,
                                       offset, &buf, &bufLen);
                    SensorCall(524);if (tok != yajl_tok_eof) {
                        yajl_bs_set(hand->stateStack, yajl_state_parse_error);
                        SensorCall(523);hand->parseError = "trailing garbage";
                    }
                    SensorCall(525);goto around_again;
                }
            }
            {yajl_status  ReplaceReturn41 = yajl_status_ok; SensorCall(527); return ReplaceReturn41;}
        case yajl_state_lexical_error:
        case yajl_state_parse_error:
            {yajl_status  ReplaceReturn40 = yajl_status_error; SensorCall(528); return ReplaceReturn40;}
        case yajl_state_start:
        case yajl_state_got_value:
        case yajl_state_map_need_val:
        case yajl_state_array_need_val:
        case yajl_state_array_start:  {
            /* for arrays and maps, we advance the state for this
             * depth, then push the state of the next depth.
             * If an error occurs during the parsing of the nesting
             * enitity, the state at this level will not matter.
             * a state that needs pushing will be anything other
             * than state_start */

            SensorCall(529);yajl_state stateToPush = yajl_state_start;

            tok = yajl_lex_lex(hand->lexer, jsonText, jsonTextLen,
                               offset, &buf, &bufLen);

            SensorCall(576);switch (tok) {
                case yajl_tok_eof:
                    {yajl_status  ReplaceReturn39 = yajl_status_ok; SensorCall(530); return ReplaceReturn39;}
                case yajl_tok_error:
                    yajl_bs_set(hand->stateStack, yajl_state_lexical_error);
                    SensorCall(531);goto around_again;
                case yajl_tok_string:
                    SensorCall(532);if (hand->callbacks && hand->callbacks->yajl_string) {
                        _CC_CHK(hand->callbacks->yajl_string(hand->ctx,
                                                             buf, bufLen));
                    }
                    SensorCall(533);break;
                case yajl_tok_string_with_escapes:
                    SensorCall(534);if (hand->callbacks && hand->callbacks->yajl_string) {
                        SensorCall(535);yajl_buf_clear(hand->decodeBuf);
                        yajl_string_decode(hand->decodeBuf, buf, bufLen);
                        _CC_CHK(hand->callbacks->yajl_string(
                                    hand->ctx, yajl_buf_data(hand->decodeBuf),
                                    yajl_buf_len(hand->decodeBuf)));
                    }
                    SensorCall(536);break;
                case yajl_tok_bool:
                    SensorCall(537);if (hand->callbacks && hand->callbacks->yajl_boolean) {
                        _CC_CHK(hand->callbacks->yajl_boolean(hand->ctx,
                                                              *buf == 't'));
                    }
                    SensorCall(538);break;
                case yajl_tok_null:
                    SensorCall(539);if (hand->callbacks && hand->callbacks->yajl_null) {
                        _CC_CHK(hand->callbacks->yajl_null(hand->ctx));
                    }
                    SensorCall(540);break;
                case yajl_tok_left_bracket:
                    SensorCall(541);if (hand->callbacks && hand->callbacks->yajl_start_map) {
                        _CC_CHK(hand->callbacks->yajl_start_map(hand->ctx));
                    }
                    SensorCall(542);stateToPush = yajl_state_map_start;
                    SensorCall(543);break;
                case yajl_tok_left_brace:
                    SensorCall(544);if (hand->callbacks && hand->callbacks->yajl_start_array) {
                        _CC_CHK(hand->callbacks->yajl_start_array(hand->ctx));
                    }
                    SensorCall(545);stateToPush = yajl_state_array_start;
                    SensorCall(546);break;
                case yajl_tok_integer:
                    SensorCall(547);if (hand->callbacks) {
                        SensorCall(548);if (hand->callbacks->yajl_number) {
                            _CC_CHK(hand->callbacks->yajl_number(
                                        hand->ctx,(const char *) buf, bufLen));
                        } else {/*1*/SensorCall(549);if (hand->callbacks->yajl_integer) {
                            SensorCall(550);long long int i = 0;
                            errno = 0;
                            i = yajl_parse_integer(buf, bufLen);
                            SensorCall(556);if ((i == LLONG_MIN || i == LLONG_MAX) &&
                                errno == ERANGE)
                            {
                                yajl_bs_set(hand->stateStack,
                                            yajl_state_parse_error);
                                SensorCall(551);hand->parseError = "integer overflow" ;
                                /* try to restore error offset */
                                SensorCall(554);if (*offset >= bufLen) {/*3*/SensorCall(552);*offset -= bufLen;/*4*/}
                                else {/*5*/SensorCall(553);*offset = 0;/*6*/}
                                SensorCall(555);goto around_again;
                            }
                            _CC_CHK(hand->callbacks->yajl_integer(hand->ctx,
                                                                  i));
                        ;/*2*/}}
                    }
                    SensorCall(557);break;
                case yajl_tok_double:
                    SensorCall(558);if (hand->callbacks) {
                        SensorCall(559);if (hand->callbacks->yajl_number) {
                            _CC_CHK(hand->callbacks->yajl_number(
                                        hand->ctx, (const char *) buf, bufLen));
                        } else {/*7*/SensorCall(560);if (hand->callbacks->yajl_double) {
                            SensorCall(561);double d = 0.0;
                            yajl_buf_clear(hand->decodeBuf);
                            yajl_buf_append(hand->decodeBuf, buf, bufLen);
                            buf = yajl_buf_data(hand->decodeBuf);
                            errno = 0;
                            d = strtod((char *) buf, NULL);
                            SensorCall(567);if ((d == HUGE_VAL || d == -HUGE_VAL) &&
                                errno == ERANGE)
                            {
                                yajl_bs_set(hand->stateStack,
                                            yajl_state_parse_error);
                                SensorCall(562);hand->parseError = "numeric (floating point) "
                                    "overflow";
                                /* try to restore error offset */
                                SensorCall(565);if (*offset >= bufLen) {/*9*/SensorCall(563);*offset -= bufLen;/*10*/}
                                else {/*11*/SensorCall(564);*offset = 0;/*12*/}
                                SensorCall(566);goto around_again;
                            }
                            _CC_CHK(hand->callbacks->yajl_double(hand->ctx,
                                                                 d));
                        ;/*8*/}}
                    }
                    SensorCall(568);break;
                case yajl_tok_right_brace: {
                    SensorCall(569);if (yajl_bs_current(hand->stateStack) ==
                        yajl_state_array_start)
                    {
                        SensorCall(570);if (hand->callbacks &&
                            hand->callbacks->yajl_end_array)
                        {
                            _CC_CHK(hand->callbacks->yajl_end_array(hand->ctx));
                        }
                        yajl_bs_pop(hand->stateStack);
                        SensorCall(571);goto around_again;
                    }
                    /* intentional fall-through */
                }
                case yajl_tok_colon:
                case yajl_tok_comma:
                case yajl_tok_right_bracket:
                    yajl_bs_set(hand->stateStack, yajl_state_parse_error);
                    SensorCall(572);hand->parseError =
                        "unallowed token at this point in JSON text";
                    SensorCall(573);goto around_again;
                default:
                    yajl_bs_set(hand->stateStack, yajl_state_parse_error);
                    SensorCall(574);hand->parseError = "invalid token, internal error";
                    SensorCall(575);goto around_again;
            }
            /* got a value.  transition depends on the state we're in. */
            {
                SensorCall(577);yajl_state s = yajl_bs_current(hand->stateStack);
                SensorCall(579);if (s == yajl_state_start || s == yajl_state_got_value) {
                    yajl_bs_set(hand->stateStack, yajl_state_parse_complete);
                } else {/*13*/SensorCall(578);if (s == yajl_state_map_need_val) {
                    yajl_bs_set(hand->stateStack, yajl_state_map_got_val);
                } else {
                    yajl_bs_set(hand->stateStack, yajl_state_array_got_val);
                ;/*14*/}}
            }
            SensorCall(580);if (stateToPush != yajl_state_start) {
                yajl_bs_push(hand->stateStack, stateToPush);
            }

            SensorCall(581);goto around_again;
        }
        case yajl_state_map_start:
        case yajl_state_map_need_key: {
            /* only difference between these two states is that in
             * start '}' is valid, whereas in need_key, we've parsed
             * a comma, and a string key _must_ follow */
            SensorCall(582);tok = yajl_lex_lex(hand->lexer, jsonText, jsonTextLen,
                               offset, &buf, &bufLen);
            SensorCall(594);switch (tok) {
                case yajl_tok_eof:
                    {yajl_status  ReplaceReturn38 = yajl_status_ok; SensorCall(583); return ReplaceReturn38;}
                case yajl_tok_error:
                    yajl_bs_set(hand->stateStack, yajl_state_lexical_error);
                    SensorCall(584);goto around_again;
                case yajl_tok_string_with_escapes:
                    SensorCall(585);if (hand->callbacks && hand->callbacks->yajl_map_key) {
                        SensorCall(586);yajl_buf_clear(hand->decodeBuf);
                        yajl_string_decode(hand->decodeBuf, buf, bufLen);
                        buf = yajl_buf_data(hand->decodeBuf);
                        bufLen = yajl_buf_len(hand->decodeBuf);
                    }
                    /* intentional fall-through */
                case yajl_tok_string:
                    SensorCall(587);if (hand->callbacks && hand->callbacks->yajl_map_key) {
                        _CC_CHK(hand->callbacks->yajl_map_key(hand->ctx, buf,
                                                              bufLen));
                    }
                    yajl_bs_set(hand->stateStack, yajl_state_map_sep);
                    SensorCall(588);goto around_again;
                case yajl_tok_right_bracket:
                    SensorCall(589);if (yajl_bs_current(hand->stateStack) ==
                        yajl_state_map_start)
                    {
                        SensorCall(590);if (hand->callbacks && hand->callbacks->yajl_end_map) {
                            _CC_CHK(hand->callbacks->yajl_end_map(hand->ctx));
                        }
                        yajl_bs_pop(hand->stateStack);
                        SensorCall(591);goto around_again;
                    }
                default:
                    yajl_bs_set(hand->stateStack, yajl_state_parse_error);
                    SensorCall(592);hand->parseError =
                        "invalid object key (must be a string)"; 
                    SensorCall(593);goto around_again;
            }
        }
        case yajl_state_map_sep: {
            SensorCall(595);tok = yajl_lex_lex(hand->lexer, jsonText, jsonTextLen,
                               offset, &buf, &bufLen);
            SensorCall(601);switch (tok) {
                case yajl_tok_colon:
                    yajl_bs_set(hand->stateStack, yajl_state_map_need_val);
                    SensorCall(596);goto around_again;
                case yajl_tok_eof:
                    {yajl_status  ReplaceReturn37 = yajl_status_ok; SensorCall(597); return ReplaceReturn37;}
                case yajl_tok_error:
                    yajl_bs_set(hand->stateStack, yajl_state_lexical_error);
                    SensorCall(598);goto around_again;
                default:
                    yajl_bs_set(hand->stateStack, yajl_state_parse_error);
                    SensorCall(599);hand->parseError = "object key and value must "
                        "be separated by a colon (':')";
                    SensorCall(600);goto around_again;
            }
        }
        case yajl_state_map_got_val: {
            SensorCall(602);tok = yajl_lex_lex(hand->lexer, jsonText, jsonTextLen,
                               offset, &buf, &bufLen);
            SensorCall(613);switch (tok) {
                case yajl_tok_right_bracket:
                    SensorCall(603);if (hand->callbacks && hand->callbacks->yajl_end_map) {
                        _CC_CHK(hand->callbacks->yajl_end_map(hand->ctx));
                    }
                    yajl_bs_pop(hand->stateStack);
                    SensorCall(604);goto around_again;
                case yajl_tok_comma:
                    yajl_bs_set(hand->stateStack, yajl_state_map_need_key);
                    SensorCall(605);goto around_again;
                case yajl_tok_eof:
                    {yajl_status  ReplaceReturn36 = yajl_status_ok; SensorCall(606); return ReplaceReturn36;}
                case yajl_tok_error:
                    yajl_bs_set(hand->stateStack, yajl_state_lexical_error);
                    SensorCall(607);goto around_again;
                default:
                    yajl_bs_set(hand->stateStack, yajl_state_parse_error);
                    SensorCall(608);hand->parseError = "after key and value, inside map, "
                                       "I expect ',' or '}'";
                    /* try to restore error offset */
                    SensorCall(611);if (*offset >= bufLen) {/*15*/SensorCall(609);*offset -= bufLen;/*16*/}
                    else {/*17*/SensorCall(610);*offset = 0;/*18*/}
                    SensorCall(612);goto around_again;
            }
        }
        case yajl_state_array_got_val: {
            SensorCall(614);tok = yajl_lex_lex(hand->lexer, jsonText, jsonTextLen,
                               offset, &buf, &bufLen);
            SensorCall(622);switch (tok) {
                case yajl_tok_right_brace:
                    SensorCall(615);if (hand->callbacks && hand->callbacks->yajl_end_array) {
                        _CC_CHK(hand->callbacks->yajl_end_array(hand->ctx));
                    }
                    yajl_bs_pop(hand->stateStack);
                    SensorCall(616);goto around_again;
                case yajl_tok_comma:
                    yajl_bs_set(hand->stateStack, yajl_state_array_need_val);
                    SensorCall(617);goto around_again;
                case yajl_tok_eof:
                    {yajl_status  ReplaceReturn35 = yajl_status_ok; SensorCall(618); return ReplaceReturn35;}
                case yajl_tok_error:
                    yajl_bs_set(hand->stateStack, yajl_state_lexical_error);
                    SensorCall(619);goto around_again;
                default:
                    yajl_bs_set(hand->stateStack, yajl_state_parse_error);
                    SensorCall(620);hand->parseError =
                        "after array element, I expect ',' or ']'";
                    SensorCall(621);goto around_again;
            }
        }
    }

    SensorCall(624);abort();
    {yajl_status  ReplaceReturn34 = yajl_status_error; SensorCall(625); return ReplaceReturn34;}
}

