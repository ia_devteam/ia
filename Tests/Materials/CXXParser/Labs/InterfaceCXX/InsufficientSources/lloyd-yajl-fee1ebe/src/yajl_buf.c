/*
 * Copyright (c) 2007-2011, Lloyd Hilaiel <lloyd@hilaiel.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "yajl_buf.h"
#include "var/tmp/sensor.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#define YAJL_BUF_INIT_SIZE 2048

struct yajl_buf_t {
    size_t len;
    size_t used;
    unsigned char * data;
    yajl_alloc_funcs * alloc;
};

static
void yajl_buf_ensure_available(yajl_buf buf, size_t want)
{
    SensorCall(160);size_t need;
    
    assert(buf != NULL);

    /* first call */
    SensorCall(162);if (buf->data == NULL) {
        SensorCall(161);buf->len = YAJL_BUF_INIT_SIZE;
        buf->data = (unsigned char *) YA_MALLOC(buf->alloc, buf->len);
        buf->data[0] = 0;
    }

    SensorCall(163);need = buf->len;

    SensorCall(165);while (want >= (need - buf->used)) {/*3*/SensorCall(164);need <<= 1;/*4*/}

    SensorCall(167);if (need != buf->len) {
        SensorCall(166);buf->data = (unsigned char *) YA_REALLOC(buf->alloc, buf->data, need);
        buf->len = need;
    }
SensorCall(168);}

yajl_buf yajl_buf_alloc(yajl_alloc_funcs * alloc)
{
    SensorCall(169);yajl_buf b = YA_MALLOC(alloc, sizeof(struct yajl_buf_t));
    memset((void *) b, 0, sizeof(struct yajl_buf_t));
    b->alloc = alloc;
    {yajl_buf  ReplaceReturn132 = b; SensorCall(170); return ReplaceReturn132;}
}

void yajl_buf_free(yajl_buf buf)
{
SensorCall(171);    assert(buf != NULL);
    SensorCall(172);if (buf->data) YA_FREE(buf->alloc, buf->data);
    YA_FREE(buf->alloc, buf);
}

void yajl_buf_append(yajl_buf buf, const void * data, size_t len)
{
    SensorCall(173);yajl_buf_ensure_available(buf, len);
    SensorCall(175);if (len > 0) {
        assert(data != NULL);
        SensorCall(174);memcpy(buf->data + buf->used, data, len);
        buf->used += len;
        buf->data[buf->used] = 0;
    }
SensorCall(176);}

void yajl_buf_clear(yajl_buf buf)
{
    SensorCall(177);buf->used = 0;
    SensorCall(179);if (buf->data) {/*1*/SensorCall(178);buf->data[buf->used] = 0;/*2*/}
SensorCall(180);}

const unsigned char * yajl_buf_data(yajl_buf buf)
{
    {const unsigned char * ReplaceReturn131 = buf->data; SensorCall(181); return ReplaceReturn131;}
}

size_t yajl_buf_len(yajl_buf buf)
{
    {size_t  ReplaceReturn130 = buf->used; SensorCall(182); return ReplaceReturn130;}
}

void
yajl_buf_truncate(yajl_buf buf, size_t len)
{
SensorCall(183);    assert(len <= buf->used);
    buf->used = len;
SensorCall(184);}
