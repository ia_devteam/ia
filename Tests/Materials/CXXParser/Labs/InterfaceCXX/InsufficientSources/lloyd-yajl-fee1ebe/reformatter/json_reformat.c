/*
 * Copyright (c) 2007-2011, Lloyd Hilaiel <lloyd@hilaiel.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <yajl/yajl_parse.h>
#include "var/tmp/sensor.h"
#include <yajl/yajl_gen.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int reformat_null(void * ctx)
{
    SensorCall(61);yajl_gen g = (yajl_gen) ctx;
    {int  ReplaceReturn114 = yajl_gen_status_ok == yajl_gen_null(g); SensorCall(62); return ReplaceReturn114;}
}

static int reformat_boolean(void * ctx, int boolean)
{
    SensorCall(63);yajl_gen g = (yajl_gen) ctx;
    {int  ReplaceReturn113 = yajl_gen_status_ok == yajl_gen_bool(g, boolean); SensorCall(64); return ReplaceReturn113;}
}

static int reformat_number(void * ctx, const char * s, size_t l)
{
    SensorCall(65);yajl_gen g = (yajl_gen) ctx;
    {int  ReplaceReturn112 = yajl_gen_status_ok == yajl_gen_number(g, s, l); SensorCall(66); return ReplaceReturn112;}
}

static int reformat_string(void * ctx, const unsigned char * stringVal,
                           size_t stringLen)
{
    SensorCall(67);yajl_gen g = (yajl_gen) ctx;
    {int  ReplaceReturn111 = yajl_gen_status_ok == yajl_gen_string(g, stringVal, stringLen); SensorCall(68); return ReplaceReturn111;}
}

static int reformat_map_key(void * ctx, const unsigned char * stringVal,
                            size_t stringLen)
{
    SensorCall(69);yajl_gen g = (yajl_gen) ctx;
    {int  ReplaceReturn110 = yajl_gen_status_ok == yajl_gen_string(g, stringVal, stringLen); SensorCall(70); return ReplaceReturn110;}
}

static int reformat_start_map(void * ctx)
{
    SensorCall(71);yajl_gen g = (yajl_gen) ctx;
    {int  ReplaceReturn109 = yajl_gen_status_ok == yajl_gen_map_open(g); SensorCall(72); return ReplaceReturn109;}
}


static int reformat_end_map(void * ctx)
{
    SensorCall(73);yajl_gen g = (yajl_gen) ctx;
    {int  ReplaceReturn108 = yajl_gen_status_ok == yajl_gen_map_close(g); SensorCall(74); return ReplaceReturn108;}
}

static int reformat_start_array(void * ctx)
{
    SensorCall(75);yajl_gen g = (yajl_gen) ctx;
    {int  ReplaceReturn107 = yajl_gen_status_ok == yajl_gen_array_open(g); SensorCall(76); return ReplaceReturn107;}
}

static int reformat_end_array(void * ctx)
{
    SensorCall(77);yajl_gen g = (yajl_gen) ctx;
    {int  ReplaceReturn106 = yajl_gen_status_ok == yajl_gen_array_close(g); SensorCall(78); return ReplaceReturn106;}
}

static yajl_callbacks callbacks = {
    reformat_null,
    reformat_boolean,
    NULL,
    NULL,
    reformat_number,
    reformat_string,
    reformat_start_map,
    reformat_map_key,
    reformat_end_map,
    reformat_start_array,
    reformat_end_array
};

static void
usage(const char * progname)
{
    SensorCall(79);fprintf(stderr, "%s: reformat json from stdin\n"
            "usage:  json_reformat [options]\n"
            "    -m minimize json rather than beautify (default)\n"
            "    -u allow invalid UTF8 inside strings during parsing\n"
            "    -e escape any forward slashes (for embedding in HTML)\n",
            progname);
    exit(1);
SensorCall(80);}


int 
main(int argc, char ** argv)
{
    SensorCall(81);yajl_handle hand;
    static unsigned char fileData[65536];
    /* generator config */
    yajl_gen g;
    yajl_status stat;
    size_t rd;
    int retval = 0;
    int a = 1;

    g = yajl_gen_alloc(NULL);
    yajl_gen_config(g, yajl_gen_beautify, 1);
    yajl_gen_config(g, yajl_gen_validate_utf8, 1);

    /* ok.  open file.  let's read and parse */
    hand = yajl_alloc(&callbacks, NULL, (void *) g);
    /* and let's allow comments by default */
    yajl_config(hand, yajl_allow_comments, 1);

    /* check arguments.*/
    SensorCall(93);while ((a < argc) && (argv[a][0] == '-') && (strlen(argv[a]) > 1)) {
        SensorCall(82);unsigned int i;
        SensorCall(91);for ( i=1; i < strlen(argv[a]); i++) {
            SensorCall(83);switch (argv[a][i]) {
                case 'm':
                    SensorCall(84);yajl_gen_config(g, yajl_gen_beautify, 0);
                    SensorCall(85);break;
                case 'u':
                    SensorCall(86);yajl_config(hand, yajl_dont_validate_strings, 1);
                    SensorCall(87);break;
                case 'e':
                    SensorCall(88);yajl_gen_config(g, yajl_gen_escape_solidus, 1);
                    SensorCall(89);break;
                default:
                    SensorCall(90);fprintf(stderr, "unrecognized option: '%c'\n\n",
                            argv[a][i]);
                    usage(argv[0]);
            }
        }
        SensorCall(92);++a;
    }
    SensorCall(95);if (a < argc) {
        SensorCall(94);usage(argv[0]);
    }


    SensorCall(105);for (;;) {
        SensorCall(96);rd = fread((void *) fileData, 1, sizeof(fileData) - 1, stdin);

        SensorCall(100);if (rd == 0) {
            SensorCall(97);if (!feof(stdin)) {
                SensorCall(98);fprintf(stderr, "error on file read.\n");
                retval = 1;
            }
            SensorCall(99);break;
        }
        SensorCall(101);fileData[rd] = 0;

        stat = yajl_parse(hand, fileData, rd);

        SensorCall(103);if (stat != yajl_status_ok) {/*1*/SensorCall(102);break;/*2*/}

        {
            SensorCall(104);const unsigned char * buf;
            size_t len;
            yajl_gen_get_buf(g, &buf, &len);
            fwrite(buf, 1, len, stdout);
            yajl_gen_clear(g);
        }
    }

    SensorCall(106);stat = yajl_complete_parse(hand);

    SensorCall(108);if (stat != yajl_status_ok) {
        SensorCall(107);unsigned char * str = yajl_get_error(hand, 1, fileData, rd);
        fprintf(stderr, "%s", (const char *) str);
        yajl_free_error(hand, str);
        retval = 1;
    }

    SensorCall(109);yajl_gen_free(g);
    yajl_free(hand);

    {int  ReplaceReturn105 = retval; SensorCall(110); return ReplaceReturn105;}
}
