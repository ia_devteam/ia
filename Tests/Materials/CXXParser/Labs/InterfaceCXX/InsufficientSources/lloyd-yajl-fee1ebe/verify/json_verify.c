/*
 * Copyright (c) 2007-2011, Lloyd Hilaiel <lloyd@hilaiel.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <yajl/yajl_parse.h>
#include "var/tmp/sensor.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void
usage(const char * progname)
{
    SensorCall(819);fprintf(stderr, "%s: validate json from stdin\n"
                    "usage: json_verify [options]\n"
                    "    -q quiet mode\n"
                    "    -c allow comments\n"
                    "    -u allow invalid utf8 inside strings\n",
            progname);
    exit(1);
SensorCall(820);}

int
main(int argc, char ** argv)
{
    SensorCall(821);yajl_status stat;
    size_t rd;
    yajl_handle hand;
    static unsigned char fileData[65536];
    int quiet = 0;
    int retval = 0;
    int a = 1;

    /* allocate a parser */
    hand = yajl_alloc(NULL, NULL, NULL);

    /* check arguments.*/
    SensorCall(833);while ((a < argc) && (argv[a][0] == '-') && (strlen(argv[a]) > 1)) {
        SensorCall(822);unsigned int i;
        SensorCall(831);for ( i=1; i < strlen(argv[a]); i++) {
            SensorCall(823);switch (argv[a][i]) {
                case 'q':
                    SensorCall(824);quiet = 1;
                    SensorCall(825);break;
                case 'c':
                    SensorCall(826);yajl_config(hand, yajl_allow_comments, 1);
                    SensorCall(827);break;
                case 'u':
                    SensorCall(828);yajl_config(hand, yajl_dont_validate_strings, 1);
                    SensorCall(829);break;
                default:
                    SensorCall(830);fprintf(stderr, "unrecognized option: '%c'\n\n", argv[a][i]);
                    usage(argv[0]);
            }
        }
        SensorCall(832);++a;
    }
    SensorCall(835);if (a < argc) {
        SensorCall(834);usage(argv[0]);
    }

    SensorCall(846);for (;;) {
        SensorCall(836);rd = fread((void *) fileData, 1, sizeof(fileData) - 1, stdin);

        retval = 0;

        SensorCall(842);if (rd == 0) {
            SensorCall(837);if (!feof(stdin)) {
                SensorCall(838);if (!quiet) {
                    SensorCall(839);fprintf(stderr, "error encountered on file read\n");
                }
                SensorCall(840);retval = 1;
            }
            SensorCall(841);break;
        }
        SensorCall(843);fileData[rd] = 0;

        /* read file data, pass to parser */
        stat = yajl_parse(hand, fileData, rd);

        SensorCall(845);if (stat != yajl_status_ok) {/*1*/SensorCall(844);break;/*2*/}
    }

    /* parse any remaining buffered data */
    SensorCall(847);stat = yajl_complete_parse(hand);

    SensorCall(851);if (stat != yajl_status_ok)
    {
        SensorCall(848);if (!quiet) {
            SensorCall(849);unsigned char * str = yajl_get_error(hand, 1, fileData, rd);
            fprintf(stderr, "%s", (const char *) str);
            yajl_free_error(hand, str);
        }
        SensorCall(850);retval = 1;
    }

    SensorCall(852);yajl_free(hand);

    SensorCall(854);if (!quiet) {
        SensorCall(853);printf("JSON is %s\n", retval ? "invalid" : "valid");
    }

    {int  ReplaceReturn129 = retval; SensorCall(855); return ReplaceReturn129;}
}
