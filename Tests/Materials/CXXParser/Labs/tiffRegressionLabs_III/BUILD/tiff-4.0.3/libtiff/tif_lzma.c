/* $Id: tif_lzma.c,v 1.4 2011-12-22 00:29:29 bfriesen Exp $ */

/*
 * Copyright (c) 2010, Andrey Kiselev <dron@ak4719.spb.edu>
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#include "tiffiop.h"
#include "/var/tmp/sensor.h"
#ifdef LZMA_SUPPORT
/*
 * TIFF Library.
 *
 * LZMA2 Compression Support
 *
 * You need an LZMA2 SDK to link with. See http://tukaani.org/xz/ for details.
 *
 * The codec is derived from ZLIB codec (tif_zip.c).
 */

#include "tif_predict.h"
#include "lzma.h"

#include <stdio.h>

/*
 * State block for each open TIFF file using LZMA2 compression/decompression.
 */
typedef struct {
	TIFFPredictorState predict;
        lzma_stream	stream;
	lzma_filter	filters[LZMA_FILTERS_MAX + 1];
	lzma_options_delta opt_delta;		/* delta filter options */
	lzma_options_lzma opt_lzma;		/* LZMA2 filter options */
	int             preset;			/* compression level */
	lzma_check	check;			/* type of the integrity check */
	int             state;			/* state flags */
#define LSTATE_INIT_DECODE 0x01
#define LSTATE_INIT_ENCODE 0x02

	TIFFVGetMethod  vgetparent;            /* super-class method */
	TIFFVSetMethod  vsetparent;            /* super-class method */
} LZMAState;

#define LState(tif)             ((LZMAState*) (tif)->tif_data)
#define DecoderState(tif)       LState(tif)
#define EncoderState(tif)       LState(tif)

static int LZMAEncode(TIFF* tif, uint8* bp, tmsize_t cc, uint16 s);
static int LZMADecode(TIFF* tif, uint8* op, tmsize_t occ, uint16 s);

static const char *
LZMAStrerror(lzma_ret ret)
{
	SensorCall();switch (ret) {
		case LZMA_OK:
		    {const char * ReplaceReturn = "operation completed successfully"; SensorCall(); return ReplaceReturn;}
		case LZMA_STREAM_END:
		    {const char * ReplaceReturn = "end of stream was reached"; SensorCall(); return ReplaceReturn;}
		case LZMA_NO_CHECK:
		    {const char * ReplaceReturn = "input stream has no integrity check"; SensorCall(); return ReplaceReturn;}
		case LZMA_UNSUPPORTED_CHECK:
		    {const char * ReplaceReturn = "cannot calculate the integrity check"; SensorCall(); return ReplaceReturn;}
		case LZMA_GET_CHECK:
		    {const char * ReplaceReturn = "integrity check type is now available"; SensorCall(); return ReplaceReturn;}
		case LZMA_MEM_ERROR:
		    {const char * ReplaceReturn = "cannot allocate memory"; SensorCall(); return ReplaceReturn;}
		case LZMA_MEMLIMIT_ERROR:
		    {const char * ReplaceReturn = "memory usage limit was reached"; SensorCall(); return ReplaceReturn;}
		case LZMA_FORMAT_ERROR:
		    {const char * ReplaceReturn = "file format not recognized"; SensorCall(); return ReplaceReturn;}
		case LZMA_OPTIONS_ERROR:
		    {const char * ReplaceReturn = "invalid or unsupported options"; SensorCall(); return ReplaceReturn;}
		case LZMA_DATA_ERROR:
		    {const char * ReplaceReturn = "data is corrupt"; SensorCall(); return ReplaceReturn;}
		case LZMA_BUF_ERROR:
		    {const char * ReplaceReturn = "no progress is possible (stream is truncated or corrupt)"; SensorCall(); return ReplaceReturn;}
		case LZMA_PROG_ERROR:
		    {const char * ReplaceReturn = "programming error"; SensorCall(); return ReplaceReturn;}
		default:
		    {const char * ReplaceReturn = "unindentified liblzma error"; SensorCall(); return ReplaceReturn;}
	}
SensorCall();}

static int
LZMAFixupTags(TIFF* tif)
{
	SensorCall();(void) tif;
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

static int
LZMASetupDecode(TIFF* tif)
{
	SensorCall();LZMAState* sp = DecoderState(tif);

	assert(sp != NULL);
        
        /* if we were last encoding, terminate this mode */
	if (sp->state & LSTATE_INIT_ENCODE) {
	    lzma_end(&sp->stream);
	    sp->state = 0;
	}

	sp->state |= LSTATE_INIT_DECODE;
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

/*
 * Setup state for decoding a strip.
 */
static int
LZMAPreDecode(TIFF* tif, uint16 s)
{
	SensorCall();static const char module[] = "LZMAPreDecode";
	LZMAState* sp = DecoderState(tif);
	lzma_ret ret;

	(void) s;
	assert(sp != NULL);

	if( (sp->state & LSTATE_INIT_DECODE) == 0 )
            {/*5*/tif->tif_setupdecode(tif);/*6*/}

	sp->stream.next_in = tif->tif_rawdata;
	sp->stream.avail_in = (size_t) tif->tif_rawcc;
	if ((tmsize_t)sp->stream.avail_in != tif->tif_rawcc) {
		TIFFErrorExt(tif->tif_clientdata, module,
			     "Liblzma cannot deal with buffers this size");
		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}

	/*
	 * Disable memory limit when decoding. UINT64_MAX is a flag to disable
	 * the limit, we are passing (uint64_t)-1 which should be the same.
	 */
	ret = lzma_stream_decoder(&sp->stream, (uint64_t)-1, 0);
	if (ret != LZMA_OK) {
		TIFFErrorExt(tif->tif_clientdata, module,
			     "Error initializing the stream decoder, %s",
			     LZMAStrerror(ret));
		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

static int
LZMADecode(TIFF* tif, uint8* op, tmsize_t occ, uint16 s)
{
	SensorCall();static const char module[] = "LZMADecode";
	LZMAState* sp = DecoderState(tif);

	(void) s;
	assert(sp != NULL);
	assert(sp->state == LSTATE_INIT_DECODE);

        sp->stream.next_in = tif->tif_rawcp;
        sp->stream.avail_in = (size_t) tif->tif_rawcc;

	sp->stream.next_out = op;
	sp->stream.avail_out = (size_t) occ;
	if ((tmsize_t)sp->stream.avail_out != occ) {
		TIFFErrorExt(tif->tif_clientdata, module,
			     "Liblzma cannot deal with buffers this size");
		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}

	do {
		/*
		 * Save the current stream state to properly recover from the
		 * decoding errors later.
		 */
		const uint8_t *next_in = sp->stream.next_in;
		size_t avail_in = sp->stream.avail_in;

		lzma_ret ret = lzma_code(&sp->stream, LZMA_RUN);
		if (ret == LZMA_STREAM_END)
			break;
		if (ret == LZMA_MEMLIMIT_ERROR) {
			lzma_ret r = lzma_stream_decoder(&sp->stream,
							 lzma_memusage(&sp->stream), 0);
			if (r != LZMA_OK) {
				TIFFErrorExt(tif->tif_clientdata, module,
					     "Error initializing the stream decoder, %s",
					     LZMAStrerror(r));
				break;
			}
			sp->stream.next_in = next_in;
			sp->stream.avail_in = avail_in;
			continue;
		}
		if (ret != LZMA_OK) {
			TIFFErrorExt(tif->tif_clientdata, module,
			    "Decoding error at scanline %lu, %s",
			    (unsigned long) tif->tif_row, LZMAStrerror(ret));
			break;
		}
	} while (sp->stream.avail_out > 0);
	if (sp->stream.avail_out != 0) {
		TIFFErrorExt(tif->tif_clientdata, module,
		    "Not enough data at scanline %lu (short %lu bytes)",
		    (unsigned long) tif->tif_row, (unsigned long) sp->stream.avail_out);
		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}

        tif->tif_rawcp = (uint8 *)sp->stream.next_in; /* cast away const */
        tif->tif_rawcc = sp->stream.avail_in;
        
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

static int
LZMASetupEncode(TIFF* tif)
{
	SensorCall();LZMAState* sp = EncoderState(tif);

	assert(sp != NULL);
	if (sp->state & LSTATE_INIT_DECODE) {
		lzma_end(&sp->stream);
		sp->state = 0;
	}

	sp->state |= LSTATE_INIT_ENCODE;
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

/*
 * Reset encoding state at the start of a strip.
 */
static int
LZMAPreEncode(TIFF* tif, uint16 s)
{
	SensorCall();static const char module[] = "LZMAPreEncode";
	LZMAState *sp = EncoderState(tif);

	(void) s;
	assert(sp != NULL);
	if( sp->state != LSTATE_INIT_ENCODE )
            {/*7*/tif->tif_setupencode(tif);/*8*/}

	sp->stream.next_out = tif->tif_rawdata;
	sp->stream.avail_out = (size_t)tif->tif_rawdatasize;
	if ((tmsize_t)sp->stream.avail_out != tif->tif_rawdatasize) {
		TIFFErrorExt(tif->tif_clientdata, module,
			     "Liblzma cannot deal with buffers this size");
		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}
	{int  ReplaceReturn = (lzma_stream_encoder(&sp->stream, sp->filters, sp->check) == LZMA_OK); SensorCall(); return ReplaceReturn;}
}

/*
 * Encode a chunk of pixels.
 */
static int
LZMAEncode(TIFF* tif, uint8* bp, tmsize_t cc, uint16 s)
{
	SensorCall();static const char module[] = "LZMAEncode";
	LZMAState *sp = EncoderState(tif);

	assert(sp != NULL);
	assert(sp->state == LSTATE_INIT_ENCODE);

	(void) s;
	sp->stream.next_in = bp;
	sp->stream.avail_in = (size_t) cc;
	if ((tmsize_t)sp->stream.avail_in != cc) {
		TIFFErrorExt(tif->tif_clientdata, module,
			     "Liblzma cannot deal with buffers this size");
		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}
	do {
		lzma_ret ret = lzma_code(&sp->stream, LZMA_RUN);
		if (ret != LZMA_OK) {
			TIFFErrorExt(tif->tif_clientdata, module,
				"Encoding error at scanline %lu, %s",
				(unsigned long) tif->tif_row, LZMAStrerror(ret));
			{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
		}
		if (sp->stream.avail_out == 0) {
			tif->tif_rawcc = tif->tif_rawdatasize;
			TIFFFlushData1(tif);
			sp->stream.next_out = tif->tif_rawdata;
			sp->stream.avail_out = (size_t)tif->tif_rawdatasize;  /* this is a safe typecast, as check is made already in LZMAPreEncode */
		}
	} while (sp->stream.avail_in > 0);
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

/*
 * Finish off an encoded strip by flushing the last
 * string and tacking on an End Of Information code.
 */
static int
LZMAPostEncode(TIFF* tif)
{
	SensorCall();static const char module[] = "LZMAPostEncode";
	LZMAState *sp = EncoderState(tif);
	lzma_ret ret;

	sp->stream.avail_in = 0;
	do {
		ret = lzma_code(&sp->stream, LZMA_FINISH);
		switch (ret) {
		case LZMA_STREAM_END:
		case LZMA_OK:
			if ((tmsize_t)sp->stream.avail_out != tif->tif_rawdatasize) {
				tif->tif_rawcc =
					tif->tif_rawdatasize - sp->stream.avail_out;
				TIFFFlushData1(tif);
				sp->stream.next_out = tif->tif_rawdata;
				sp->stream.avail_out = (size_t)tif->tif_rawdatasize;  /* this is a safe typecast, as check is made already in ZIPPreEncode */
			}
			break;
		default:
			TIFFErrorExt(tif->tif_clientdata, module, "Liblzma error: %s",
				     LZMAStrerror(ret));
			{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
		}
	} while (ret != LZMA_STREAM_END);
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

static void
LZMACleanup(TIFF* tif)
{
	SensorCall();LZMAState* sp = LState(tif);

	assert(sp != 0);

	(void)TIFFPredictorCleanup(tif);

	tif->tif_tagmethods.vgetfield = sp->vgetparent;
	tif->tif_tagmethods.vsetfield = sp->vsetparent;

	if (sp->state) {
		lzma_end(&sp->stream);
		sp->state = 0;
	}
	_TIFFfree(sp);
	tif->tif_data = NULL;

	_TIFFSetDefaultCompressionState(tif);
SensorCall();}

static int
LZMAVSetField(TIFF* tif, uint32 tag, va_list ap)
{
	SensorCall();static const char module[] = "LZMAVSetField";
	LZMAState* sp = LState(tif);

	switch (tag) {
	case TIFFTAG_LZMAPRESET:
		sp->preset = (int) va_arg(ap, int);
		lzma_lzma_preset(&sp->opt_lzma, sp->preset);
		if (sp->state & LSTATE_INIT_ENCODE) {
			lzma_ret ret = lzma_stream_encoder(&sp->stream,
							   sp->filters,
							   sp->check);
			if (ret != LZMA_OK) {
				TIFFErrorExt(tif->tif_clientdata, module,
					     "Liblzma error: %s",
					     LZMAStrerror(ret));
			}
		}
		{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
	default:
		{int  ReplaceReturn = (*sp->vsetparent)(tif, tag, ap); SensorCall(); return ReplaceReturn;}
	}
	/*NOTREACHED*/
SensorCall();}

static int
LZMAVGetField(TIFF* tif, uint32 tag, va_list ap)
{
	SensorCall();LZMAState* sp = LState(tif);

	switch (tag) {
	case TIFFTAG_LZMAPRESET:
		*va_arg(ap, int*) = sp->preset;
		break;
	default:
		{int  ReplaceReturn = (*sp->vgetparent)(tif, tag, ap); SensorCall(); return ReplaceReturn;}
	}
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

static const TIFFField lzmaFields[] = {
	{ TIFFTAG_LZMAPRESET, 0, 0, TIFF_ANY, 0, TIFF_SETGET_INT, TIFF_SETGET_UNDEFINED,
		FIELD_PSEUDO, TRUE, FALSE, "LZMA2 Compression Preset", NULL },
};

int
TIFFInitLZMA(TIFF* tif, int scheme)
{
	SensorCall();static const char module[] = "TIFFInitLZMA";
	LZMAState* sp;
	lzma_stream tmp_stream = LZMA_STREAM_INIT;

	assert( scheme == COMPRESSION_LZMA );

	/*
	 * Merge codec-specific tag information.
	 */
	if (!_TIFFMergeFields(tif, lzmaFields, TIFFArrayCount(lzmaFields))) {
		TIFFErrorExt(tif->tif_clientdata, module,
			     "Merging LZMA2 codec-specific tags failed");
		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}

	/*
	 * Allocate state block so tag methods have storage to record values.
	 */
	tif->tif_data = (uint8*) _TIFFmalloc(sizeof(LZMAState));
	if (tif->tif_data == NULL)
		goto bad;
	sp = LState(tif);
	memcpy(&sp->stream, &tmp_stream, sizeof(lzma_stream));

	/*
	 * Override parent get/set field methods.
	 */
	sp->vgetparent = tif->tif_tagmethods.vgetfield;
	tif->tif_tagmethods.vgetfield = LZMAVGetField;	/* hook for codec tags */
	sp->vsetparent = tif->tif_tagmethods.vsetfield;
	tif->tif_tagmethods.vsetfield = LZMAVSetField;	/* hook for codec tags */

	/* Default values for codec-specific fields */
	sp->preset = LZMA_PRESET_DEFAULT;		/* default comp. level */
	sp->check = LZMA_CHECK_NONE;
	sp->state = 0;

	/* Data filters. So far we are using delta and LZMA2 filters only. */
	sp->opt_delta.type = LZMA_DELTA_TYPE_BYTE;
	/*
	 * The sample size in bytes seems to be reasonable distance for delta
	 * filter.
	 */
	sp->opt_delta.dist = (tif->tif_dir.td_bitspersample % 8) ?
		1 : tif->tif_dir.td_bitspersample / 8;
	sp->filters[0].id = LZMA_FILTER_DELTA;
	sp->filters[0].options = &sp->opt_delta;

	lzma_lzma_preset(&sp->opt_lzma, sp->preset);
	sp->filters[1].id = LZMA_FILTER_LZMA2;
	sp->filters[1].options = &sp->opt_lzma;

	sp->filters[2].id = LZMA_VLI_UNKNOWN;
	sp->filters[2].options = NULL;

	/*
	 * Install codec methods.
	 */
	tif->tif_fixuptags = LZMAFixupTags;
	tif->tif_setupdecode = LZMASetupDecode;
	tif->tif_predecode = LZMAPreDecode;
	tif->tif_decoderow = LZMADecode;
	tif->tif_decodestrip = LZMADecode;
	tif->tif_decodetile = LZMADecode;
	tif->tif_setupencode = LZMASetupEncode;
	tif->tif_preencode = LZMAPreEncode;
	tif->tif_postencode = LZMAPostEncode;
	tif->tif_encoderow = LZMAEncode;
	tif->tif_encodestrip = LZMAEncode;
	tif->tif_encodetile = LZMAEncode;
	tif->tif_cleanup = LZMACleanup;
	/*
	 * Setup predictor setup.
	 */
	(void) TIFFPredictorInit(tif);
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
bad:
	TIFFErrorExt(tif->tif_clientdata, module,
		     "No space for LZMA2 state block");
	{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}
#endif /* LZMA_SUPORT */

/* vim: set ts=8 sts=8 sw=8 noet: */
