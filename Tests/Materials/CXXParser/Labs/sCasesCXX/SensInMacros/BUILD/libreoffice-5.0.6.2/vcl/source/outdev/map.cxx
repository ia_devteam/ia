/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This file incorporates work covered by the following license notice:
 *
 *   Licensed to the Apache Software Foundation (ASF) under one or more
 *   contributor license agreements. See the NOTICE file distributed
 *   with this work for additional information regarding copyright
 *   ownership. The ASF licenses this file to you under the Apache
 *   License, Version 2.0 (the "License"); you may not use this file
 *   except in compliance with the License. You may obtain a copy of
 *   the License at http://www.apache.org/licenses/LICENSE-2.0 .
 */

#include <sal/config.h>
#include "var/tmp/sensor.h"

#include <cstdlib>
#include <limits.h>

#include <o3tl/numeric.hxx>
#include <tools/bigint.hxx>

#include <vcl/virdev.hxx>
#include <vcl/wrkwin.hxx>
#include <vcl/outdev.hxx>
#include <vcl/cursor.hxx>

#include <svdata.hxx>
#include <window.h>
#include <outdev.h>

#include <basegfx/matrix/b2dhommatrix.hxx>

static int const s_ImplArySize = MAP_PIXEL+1;
static const long aImplNumeratorAry[s_ImplArySize] =
    {    1,   1,   5,  50,    1,   1,  1, 1,  1,    1, 1 };
static const long aImplDenominatorAry[s_ImplArySize] =
     { 2540, 254, 127, 127, 1000, 100, 10, 1, 72, 1440, 1 };

/*
Reduces accuracy until it is a fraction (should become
ctor fraction once); we could also do this with BigInts
*/

static Fraction ImplMakeFraction( long nN1, long nN2, long nD1, long nD2 )
{
    SensorCall();if( nD1 == 0 || nD2 == 0 ) //under these bad circumstances the following while loop will be endless
    {
        DBG_ASSERT(false,"Invalid parameter for ImplMakeFraction");
        {Fraction  ReplaceReturn = Fraction( 1, 1 ); SensorCall(); return ReplaceReturn;}
    }

    SensorCall();long i = 1;

    SensorCall();if ( nN1 < 0 ) { SensorCall();i = -i; nN1 = -nN1; }
    SensorCall();if ( nN2 < 0 ) { SensorCall();i = -i; nN2 = -nN2; }
    SensorCall();if ( nD1 < 0 ) { SensorCall();i = -i; nD1 = -nD1; }
    SensorCall();if ( nD2 < 0 ) { SensorCall();i = -i; nD2 = -nD2; }
    // all positive; i sign

    SensorCall();Fraction aF = Fraction( i*nN1, nD1 ) * Fraction( nN2, nD2 );

    SensorCall();while ( !aF.IsValid() ) {
        SensorCall();if ( nN1 > nN2 )
            {/*1*/SensorCall();nN1 = (nN1 + 1) / 2;/*2*/}
        else
            {/*3*/SensorCall();nN2 = (nN2 + 1) / 2;/*4*/}
        SensorCall();if ( nD1 > nD2 )
            {/*5*/SensorCall();nD1 = (nD1 + 1) / 2;/*6*/}
        else
            {/*7*/SensorCall();nD2 = (nD2 + 1) / 2;/*8*/}

        SensorCall();aF = Fraction( i*nN1, nD1 ) * Fraction( nN2, nD2 );
    }

    SensorCall();aF.ReduceInaccurate(32);
    {Fraction  ReplaceReturn = aF; SensorCall(); return ReplaceReturn;}
}

// Fraction.GetNumerator()
// Fraction.GetDenominator()    > 0
// rOutRes.nPixPerInch?         > 0
// rMapRes.nMapScNum?
// rMapRes.nMapScDenom?         > 0

static void ImplCalcBigIntThreshold( long nDPIX, long nDPIY,
                                     const ImplMapRes& rMapRes,
                                     ImplThresholdRes& rThresRes )
{
    SensorCall();if ( nDPIX && (LONG_MAX / nDPIX < std::abs( rMapRes.mnMapScNumX ) ) ) // #111139# avoid div by zero
    {
        SensorCall();rThresRes.mnThresLogToPixX = 0;
        rThresRes.mnThresPixToLogX = 0;
    }
    else
    {
        // calculate thresholds for BigInt arithmetic
        SensorCall();long    nDenomHalfX = rMapRes.mnMapScDenomX / 2;
        sal_uLong   nDenomX     = rMapRes.mnMapScDenomX;
        long    nProductX   = nDPIX * rMapRes.mnMapScNumX;

        SensorCall();if ( !nProductX )
            rThresRes.mnThresLogToPixX = LONG_MAX;
        else
            {/*9*/SensorCall();rThresRes.mnThresLogToPixX = std::abs( (LONG_MAX - nDenomHalfX) / nProductX );/*10*/}

        SensorCall();if ( !nDenomX )
            rThresRes.mnThresPixToLogX = LONG_MAX;
        else {/*11*/SensorCall();if ( nProductX >= 0 )
            {/*13*/SensorCall();rThresRes.mnThresPixToLogX = (long)(((sal_uLong)LONG_MAX - (sal_uLong)( nProductX/2)) / nDenomX);/*14*/}
        else
            {/*15*/SensorCall();rThresRes.mnThresPixToLogX = (long)(((sal_uLong)LONG_MAX + (sal_uLong)(-nProductX/2)) / nDenomX);/*16*/}/*12*/}
    }

    SensorCall();if ( nDPIY && (LONG_MAX / nDPIY < std::abs( rMapRes.mnMapScNumY ) ) ) // #111139# avoid div by zero
    {
        SensorCall();rThresRes.mnThresLogToPixY = 0;
        rThresRes.mnThresPixToLogY = 0;
    }
    else
    {
        // calculate thresholds for BigInt arithmetic
        SensorCall();long    nDenomHalfY = rMapRes.mnMapScDenomY / 2;
        sal_uLong   nDenomY     = rMapRes.mnMapScDenomY;
        long    nProductY   = nDPIY * rMapRes.mnMapScNumY;

        SensorCall();if ( !nProductY )
            rThresRes.mnThresLogToPixY = LONG_MAX;
        else
            {/*17*/SensorCall();rThresRes.mnThresLogToPixY = std::abs( (LONG_MAX - nDenomHalfY) / nProductY );/*18*/}

        SensorCall();if ( !nDenomY )
            rThresRes.mnThresPixToLogY = LONG_MAX;
        else {/*19*/SensorCall();if ( nProductY >= 0 )
            {/*21*/SensorCall();rThresRes.mnThresPixToLogY = (long)(((sal_uLong)LONG_MAX - (sal_uLong)( nProductY/2)) / nDenomY);/*22*/}
        else
            {/*23*/SensorCall();rThresRes.mnThresPixToLogY = (long)(((sal_uLong)LONG_MAX + (sal_uLong)(-nProductY/2)) / nDenomY);/*24*/}/*20*/}
    }

    SensorCall();rThresRes.mnThresLogToPixX /= 2;
    rThresRes.mnThresLogToPixY /= 2;
    rThresRes.mnThresPixToLogX /= 2;
    rThresRes.mnThresPixToLogY /= 2;
SensorCall();}

static void ImplCalcMapResolution( const MapMode& rMapMode,
                                   long nDPIX, long nDPIY, ImplMapRes& rMapRes )
{
    SensorCall();rMapRes.mfScaleX = 1.0;
    rMapRes.mfScaleY = 1.0;
    SensorCall();switch ( rMapMode.GetMapUnit() )
    {
        case MAP_RELATIVE:
            SensorCall();break;
        case MAP_100TH_MM:
            SensorCall();rMapRes.mnMapScNumX   = 1;
            rMapRes.mnMapScDenomX = 2540;
            rMapRes.mnMapScNumY   = 1;
            rMapRes.mnMapScDenomY = 2540;
            SensorCall();break;
        case MAP_10TH_MM:
            SensorCall();rMapRes.mnMapScNumX   = 1;
            rMapRes.mnMapScDenomX = 254;
            rMapRes.mnMapScNumY   = 1;
            rMapRes.mnMapScDenomY = 254;
            SensorCall();break;
        case MAP_MM:
            SensorCall();rMapRes.mnMapScNumX   = 5;      // 10
            rMapRes.mnMapScDenomX = 127;    // 254
            rMapRes.mnMapScNumY   = 5;      // 10
            rMapRes.mnMapScDenomY = 127;    // 254
            SensorCall();break;
        case MAP_CM:
            SensorCall();rMapRes.mnMapScNumX   = 50;     // 100
            rMapRes.mnMapScDenomX = 127;    // 254
            rMapRes.mnMapScNumY   = 50;     // 100
            rMapRes.mnMapScDenomY = 127;    // 254
            SensorCall();break;
        case MAP_1000TH_INCH:
            SensorCall();rMapRes.mnMapScNumX   = 1;
            rMapRes.mnMapScDenomX = 1000;
            rMapRes.mnMapScNumY   = 1;
            rMapRes.mnMapScDenomY = 1000;
            SensorCall();break;
        case MAP_100TH_INCH:
            SensorCall();rMapRes.mnMapScNumX   = 1;
            rMapRes.mnMapScDenomX = 100;
            rMapRes.mnMapScNumY   = 1;
            rMapRes.mnMapScDenomY = 100;
            SensorCall();break;
        case MAP_10TH_INCH:
            SensorCall();rMapRes.mnMapScNumX   = 1;
            rMapRes.mnMapScDenomX = 10;
            rMapRes.mnMapScNumY   = 1;
            rMapRes.mnMapScDenomY = 10;
            SensorCall();break;
        case MAP_INCH:
            SensorCall();rMapRes.mnMapScNumX   = 1;
            rMapRes.mnMapScDenomX = 1;
            rMapRes.mnMapScNumY   = 1;
            rMapRes.mnMapScDenomY = 1;
            SensorCall();break;
        case MAP_POINT:
            SensorCall();rMapRes.mnMapScNumX   = 1;
            rMapRes.mnMapScDenomX = 72;
            rMapRes.mnMapScNumY   = 1;
            rMapRes.mnMapScDenomY = 72;
            SensorCall();break;
        case MAP_TWIP:
            SensorCall();rMapRes.mnMapScNumX   = 1;
            rMapRes.mnMapScDenomX = 1440;
            rMapRes.mnMapScNumY   = 1;
            rMapRes.mnMapScDenomY = 1440;
            SensorCall();break;
        case MAP_PIXEL:
            SensorCall();rMapRes.mnMapScNumX   = 1;
            rMapRes.mnMapScDenomX = nDPIX;
            rMapRes.mnMapScNumY   = 1;
            rMapRes.mnMapScDenomY = nDPIY;
            SensorCall();break;
        case MAP_SYSFONT:
        case MAP_APPFONT:
            {
            SensorCall();ImplSVData* pSVData = ImplGetSVData();
            SensorCall();if ( !pSVData->maGDIData.mnAppFontX )
            {
                SensorCall();if( pSVData->maWinData.mpFirstFrame )
                    {/*25*/vcl::Window::ImplInitAppFontData( pSVData->maWinData.mpFirstFrame );/*26*/}
                else
                {
                    SensorCall();ScopedVclPtrInstance<WorkWindow> pWin( nullptr, 0 );
                    vcl::Window::ImplInitAppFontData( pWin );
                }
            }
            rMapRes.mnMapScNumX   = pSVData->maGDIData.mnAppFontX;
            SensorCall();rMapRes.mnMapScDenomX = nDPIX * 40;
            rMapRes.mnMapScNumY   = pSVData->maGDIData.mnAppFontY;
            rMapRes.mnMapScDenomY = nDPIY * 80;
            }
            SensorCall();break;
        default:
            OSL_FAIL( "unhandled MapUnit" );
            SensorCall();break;
    }

    SensorCall();Fraction aScaleX = rMapMode.GetScaleX();
    Fraction aScaleY = rMapMode.GetScaleY();

    // set offset according to MapMode
    Point aOrigin = rMapMode.GetOrigin();
    SensorCall();if ( rMapMode.GetMapUnit() != MAP_RELATIVE )
    {
        SensorCall();rMapRes.mnMapOfsX = aOrigin.X();
        rMapRes.mnMapOfsY = aOrigin.Y();
        rMapRes.mfOffsetX = aOrigin.X();
        rMapRes.mfOffsetY = aOrigin.Y();
    }
    else
    {
        SensorCall();if (!aScaleX.GetNumerator() || !aScaleY.GetNumerator())
            {/*27*/SensorCall();throw o3tl::divide_by_zero();/*28*/}

        SensorCall();rMapRes.mfOffsetX *= aScaleX.GetDenominator();
        rMapRes.mfOffsetX /= aScaleX.GetNumerator();
        rMapRes.mfOffsetX += aOrigin.X();
        rMapRes.mfOffsetY *= aScaleY.GetDenominator();
        rMapRes.mfOffsetY /= aScaleY.GetNumerator();
        rMapRes.mfOffsetY += aOrigin.Y();

        BigInt aX( rMapRes.mnMapOfsX );
        aX *= BigInt( aScaleX.GetDenominator() );
        SensorCall();if ( rMapRes.mnMapOfsX >= 0 )
        {
            SensorCall();if ( aScaleX.GetNumerator() >= 0 )
                {/*29*/SensorCall();aX += BigInt( aScaleX.GetNumerator()/2 );/*30*/}
            else
                {/*31*/SensorCall();aX -= BigInt( (aScaleX.GetNumerator()+1)/2 );/*32*/}
        }
        else
        {
            SensorCall();if ( aScaleX.GetNumerator() >= 0 )
                {/*33*/SensorCall();aX -= BigInt( (aScaleX.GetNumerator()-1)/2 );/*34*/}
            else
                {/*35*/SensorCall();aX += BigInt( aScaleX.GetNumerator()/2 );/*36*/}
        }
        SensorCall();aX /= BigInt( aScaleX.GetNumerator() );
        rMapRes.mnMapOfsX = (long)aX + aOrigin.X();
        BigInt aY( rMapRes.mnMapOfsY );
        aY *= BigInt( aScaleY.GetDenominator() );
        SensorCall();if( rMapRes.mnMapOfsY >= 0 )
        {
            SensorCall();if ( aScaleY.GetNumerator() >= 0 )
                {/*37*/SensorCall();aY += BigInt( aScaleY.GetNumerator()/2 );/*38*/}
            else
                {/*39*/SensorCall();aY -= BigInt( (aScaleY.GetNumerator()+1)/2 );/*40*/}
        }
        else
        {
            SensorCall();if ( aScaleY.GetNumerator() >= 0 )
                {/*41*/SensorCall();aY -= BigInt( (aScaleY.GetNumerator()-1)/2 );/*42*/}
            else
                {/*43*/SensorCall();aY += BigInt( aScaleY.GetNumerator()/2 );/*44*/}
        }
        SensorCall();aY /= BigInt( aScaleY.GetNumerator() );
        rMapRes.mnMapOfsY = (long)aY + aOrigin.Y();
    }

    SensorCall();rMapRes.mfScaleX *= (double)rMapRes.mnMapScNumX * (double)aScaleX.GetNumerator() /
        ((double)rMapRes.mnMapScDenomX * (double)aScaleX.GetDenominator());
    rMapRes.mfScaleY *= (double)rMapRes.mnMapScNumY * (double)aScaleY.GetNumerator() /
        ((double)rMapRes.mnMapScDenomY * (double)aScaleY.GetDenominator());

    // calculate scaling factor according to MapMode
    // aTemp? = rMapRes.mnMapSc? * aScale?
    Fraction aTempX = ImplMakeFraction( rMapRes.mnMapScNumX,
                                        aScaleX.GetNumerator(),
                                        rMapRes.mnMapScDenomX,
                                        aScaleX.GetDenominator() );
    Fraction aTempY = ImplMakeFraction( rMapRes.mnMapScNumY,
                                        aScaleY.GetNumerator(),
                                        rMapRes.mnMapScDenomY,
                                        aScaleY.GetDenominator() );
    rMapRes.mnMapScNumX   = aTempX.GetNumerator();
    rMapRes.mnMapScDenomX = aTempX.GetDenominator();
    rMapRes.mnMapScNumY   = aTempY.GetNumerator();
    rMapRes.mnMapScDenomY = aTempY.GetDenominator();
SensorCall();}

inline void ImplCalcMapResolution( const MapMode& rMapMode,
                                   long nDPIX, long nDPIY,
                                   ImplMapRes& rMapRes,
                                   ImplThresholdRes& rThresRes )
{
    SensorCall();ImplCalcMapResolution( rMapMode, nDPIX, nDPIY, rMapRes );
    ImplCalcBigIntThreshold( nDPIX, nDPIY, rMapRes, rThresRes );
SensorCall();}

// #i75163#
void OutputDevice::ImplInvalidateViewTransform()
{
    SensorCall();if(!mpOutDevData)
        {/*45*/SensorCall();return;/*46*/}

    SensorCall();if(mpOutDevData->mpViewTransform)
    {
        SensorCall();delete mpOutDevData->mpViewTransform;
        mpOutDevData->mpViewTransform = NULL;
    }

    SensorCall();if(mpOutDevData->mpInverseViewTransform)
    {
        SensorCall();delete mpOutDevData->mpInverseViewTransform;
        mpOutDevData->mpInverseViewTransform = NULL;
    }
SensorCall();}

static long ImplLogicToPixel( long n, long nDPI, long nMapNum, long nMapDenom,
                              long nThres )
{
    // To "use" it...
    SensorCall();(void) nThres;
#if (SAL_TYPES_SIZEOFLONG < 8)
    if( (+n < nThres) && (-n < nThres) )
    {
       n *= nMapNum * nDPI;
       if( nMapDenom != 1 )
       {
          n = (2 * n) / nMapDenom;
          if( n < 0 ) --n; else ++n;
          n /= 2;
       }
    }
    else
#else
    assert(nMapNum >= 0);
    assert(nDPI > 0);
    assert(nMapNum == 0 || std::abs(n) < std::numeric_limits<long>::max() / nMapNum / nDPI); //detect overflows
#endif
    {
       sal_Int64 n64 = n;
       n64 *= nMapNum;
       n64 *= nDPI;
       SensorCall();if( nMapDenom == 1 )
          {/*47*/SensorCall();n = (long)n64;/*48*/}
       else
       {
          SensorCall();n = (long)(2 * n64 / nMapDenom);
          SensorCall();if( n < 0 ) {/*49*/SensorCall();--n;/*50*/} else {/*51*/SensorCall();++n;/*52*/}
          SensorCall();n /= 2;
       }
    }
    {long  ReplaceReturn = n; SensorCall(); return ReplaceReturn;}
}

static long ImplPixelToLogic( long n, long nDPI, long nMapNum, long nMapDenom,
                              long nThres )
{
    // To "use" it...
   SensorCall();(void) nThres;
   SensorCall();if (nMapNum == 0)
   {
       {long  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
   }
#if (SAL_TYPES_SIZEOFLONG < 8)
    if( (+n < nThres) && (-n < nThres) )
        n = (2 * n * nMapDenom) / (nDPI * nMapNum);
    else
#endif
    {
        SensorCall();sal_Int64 n64 = n;
        n64 *= nMapDenom;
        long nDenom  = nDPI * nMapNum;
        n = (long)(2 * n64 / nDenom);
    }
    SensorCall();if( n < 0 ) {/*53*/SensorCall();--n;/*54*/} else {/*55*/SensorCall();++n;/*56*/}
    {long  ReplaceReturn = (n / 2); SensorCall(); return ReplaceReturn;}
}

long OutputDevice::ImplLogicXToDevicePixel( long nX ) const
{
    SensorCall();if ( !mbMap )
        {/*57*/{long  ReplaceReturn = nX+mnOutOffX; SensorCall(); return ReplaceReturn;}/*58*/}

    {long  ReplaceReturn = ImplLogicToPixel( nX + maMapRes.mnMapOfsX, mnDPIX,
                             maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                             maThresRes.mnThresLogToPixX )+mnOutOffX+mnOutOffOrigX; SensorCall(); return ReplaceReturn;}
}

long OutputDevice::ImplLogicYToDevicePixel( long nY ) const
{
    SensorCall();if ( !mbMap )
        {/*59*/{long  ReplaceReturn = nY+mnOutOffY; SensorCall(); return ReplaceReturn;}/*60*/}

    {long  ReplaceReturn = ImplLogicToPixel( nY + maMapRes.mnMapOfsY, mnDPIY,
                             maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                             maThresRes.mnThresLogToPixY )+mnOutOffY+mnOutOffOrigY; SensorCall(); return ReplaceReturn;}
}

long OutputDevice::ImplLogicWidthToDevicePixel( long nWidth ) const
{
    SensorCall();if ( !mbMap )
        {/*61*/{long  ReplaceReturn = nWidth; SensorCall(); return ReplaceReturn;}/*62*/}

    {long  ReplaceReturn = ImplLogicToPixel( nWidth, mnDPIX,
                             maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                             maThresRes.mnThresLogToPixX ); SensorCall(); return ReplaceReturn;}
}

long OutputDevice::ImplLogicHeightToDevicePixel( long nHeight ) const
{
    SensorCall();if ( !mbMap )
        {/*63*/{long  ReplaceReturn = nHeight; SensorCall(); return ReplaceReturn;}/*64*/}

    {long  ReplaceReturn = ImplLogicToPixel( nHeight, mnDPIY,
                             maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                             maThresRes.mnThresLogToPixY ); SensorCall(); return ReplaceReturn;}
}

float OutputDevice::ImplFloatLogicHeightToDevicePixel( float fLogicHeight) const
{
    SensorCall();if( !mbMap)
        {/*65*/{float  ReplaceReturn = fLogicHeight; SensorCall(); return ReplaceReturn;}/*66*/}
    SensorCall();float fPixelHeight = (fLogicHeight * mnDPIY * maMapRes.mnMapScNumY) / maMapRes.mnMapScDenomY;
    {float  ReplaceReturn = fPixelHeight; SensorCall(); return ReplaceReturn;}
}

long OutputDevice::ImplDevicePixelToLogicWidth( long nWidth ) const
{
    SensorCall();if ( !mbMap )
        {/*67*/{long  ReplaceReturn = nWidth; SensorCall(); return ReplaceReturn;}/*68*/}

    {long  ReplaceReturn = ImplPixelToLogic( nWidth, mnDPIX,
                             maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                             maThresRes.mnThresPixToLogX ); SensorCall(); return ReplaceReturn;}
}

long OutputDevice::ImplDevicePixelToLogicHeight( long nHeight ) const
{
    SensorCall();if ( !mbMap )
        {/*69*/{long  ReplaceReturn = nHeight; SensorCall(); return ReplaceReturn;}/*70*/}

    {long  ReplaceReturn = ImplPixelToLogic( nHeight, mnDPIY,
                             maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                             maThresRes.mnThresPixToLogY ); SensorCall(); return ReplaceReturn;}
}

Point OutputDevice::ImplLogicToDevicePixel( const Point& rLogicPt ) const
{
    SensorCall();if ( !mbMap )
        {/*71*/{Point  ReplaceReturn = Point( rLogicPt.X()+mnOutOffX, rLogicPt.Y()+mnOutOffY ); SensorCall(); return ReplaceReturn;}/*72*/}

    {Point  ReplaceReturn = Point( ImplLogicToPixel( rLogicPt.X() + maMapRes.mnMapOfsX, mnDPIX,
                                    maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                    maThresRes.mnThresLogToPixX )+mnOutOffX+mnOutOffOrigX,
                  ImplLogicToPixel( rLogicPt.Y() + maMapRes.mnMapOfsY, mnDPIY,
                                    maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                    maThresRes.mnThresLogToPixY )+mnOutOffY+mnOutOffOrigY ); SensorCall(); return ReplaceReturn;}
}

Size OutputDevice::ImplLogicToDevicePixel( const Size& rLogicSize ) const
{
    SensorCall();if ( !mbMap )
        {/*73*/{Size  ReplaceReturn = rLogicSize; SensorCall(); return ReplaceReturn;}/*74*/}

    {Size  ReplaceReturn = Size( ImplLogicToPixel( rLogicSize.Width(), mnDPIX,
                                   maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                   maThresRes.mnThresLogToPixX ),
                 ImplLogicToPixel( rLogicSize.Height(), mnDPIY,
                                   maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                   maThresRes.mnThresLogToPixY ) ); SensorCall(); return ReplaceReturn;}
}

Rectangle OutputDevice::ImplLogicToDevicePixel( const Rectangle& rLogicRect ) const
{
    SensorCall();if ( rLogicRect.IsEmpty() )
        {/*75*/{Rectangle  ReplaceReturn = rLogicRect; SensorCall(); return ReplaceReturn;}/*76*/}

    SensorCall();if ( !mbMap )
    {
        {Rectangle  ReplaceReturn = Rectangle( rLogicRect.Left()+mnOutOffX, rLogicRect.Top()+mnOutOffY,
                          rLogicRect.Right()+mnOutOffX, rLogicRect.Bottom()+mnOutOffY ); SensorCall(); return ReplaceReturn;}
    }

    {Rectangle  ReplaceReturn = Rectangle( ImplLogicToPixel( rLogicRect.Left()+maMapRes.mnMapOfsX, mnDPIX,
                                        maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                        maThresRes.mnThresLogToPixX )+mnOutOffX+mnOutOffOrigX,
                      ImplLogicToPixel( rLogicRect.Top()+maMapRes.mnMapOfsY, mnDPIY,
                                        maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                        maThresRes.mnThresLogToPixY )+mnOutOffY+mnOutOffOrigY,
                      ImplLogicToPixel( rLogicRect.Right()+maMapRes.mnMapOfsX, mnDPIX,
                                        maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                        maThresRes.mnThresLogToPixX )+mnOutOffX+mnOutOffOrigX,
                      ImplLogicToPixel( rLogicRect.Bottom()+maMapRes.mnMapOfsY, mnDPIY,
                                        maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                        maThresRes.mnThresLogToPixY )+mnOutOffY+mnOutOffOrigY ); SensorCall(); return ReplaceReturn;}
}

Polygon OutputDevice::ImplLogicToDevicePixel( const Polygon& rLogicPoly ) const
{
    SensorCall();if ( !mbMap && !mnOutOffX && !mnOutOffY )
        {/*77*/{Polygon  ReplaceReturn = rLogicPoly; SensorCall(); return ReplaceReturn;}/*78*/}

    SensorCall();sal_uInt16  i;
    sal_uInt16  nPoints = rLogicPoly.GetSize();
    Polygon aPoly( rLogicPoly );

    // get pointer to Point-array (copy data)
    const Point* pPointAry = aPoly.GetConstPointAry();

    SensorCall();if ( mbMap )
    {
        SensorCall();for ( i = 0; i < nPoints; i++ )
        {
            SensorCall();const Point* pPt = &(pPointAry[i]);
            Point aPt;
            aPt.X() = ImplLogicToPixel( pPt->X()+maMapRes.mnMapOfsX, mnDPIX,
                                        maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                        maThresRes.mnThresLogToPixX )+mnOutOffX+mnOutOffOrigX;
            aPt.Y() = ImplLogicToPixel( pPt->Y()+maMapRes.mnMapOfsY, mnDPIY,
                                        maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                        maThresRes.mnThresLogToPixY )+mnOutOffY+mnOutOffOrigY;
            aPoly[i] = aPt;
        }
    }
    else
    {
        SensorCall();for ( i = 0; i < nPoints; i++ )
        {
            SensorCall();Point aPt = pPointAry[i];
            aPt.X() += mnOutOffX;
            aPt.Y() += mnOutOffY;
            aPoly[i] = aPt;
        }
    }

    {Polygon  ReplaceReturn = aPoly; SensorCall(); return ReplaceReturn;}
}

tools::PolyPolygon OutputDevice::ImplLogicToDevicePixel( const tools::PolyPolygon& rLogicPolyPoly ) const
{
    SensorCall();if ( !mbMap && !mnOutOffX && !mnOutOffY )
        {/*79*/{tools::PolyPolygon  ReplaceReturn = rLogicPolyPoly; SensorCall(); return ReplaceReturn;}/*80*/}

    SensorCall();tools::PolyPolygon aPolyPoly( rLogicPolyPoly );
    sal_uInt16      nPoly = aPolyPoly.Count();
    SensorCall();for( sal_uInt16 i = 0; i < nPoly; i++ )
    {
        SensorCall();Polygon& rPoly = aPolyPoly[i];
        rPoly = ImplLogicToDevicePixel( rPoly );
    }
    {tools::PolyPolygon  ReplaceReturn = aPolyPoly; SensorCall(); return ReplaceReturn;}
}

LineInfo OutputDevice::ImplLogicToDevicePixel( const LineInfo& rLineInfo ) const
{
    SensorCall();LineInfo aInfo( rLineInfo );

    SensorCall();if( aInfo.GetStyle() == LINE_DASH )
    {
        SensorCall();if( aInfo.GetDotCount() && aInfo.GetDotLen() )
            {/*81*/aInfo.SetDotLen( std::max( ImplLogicWidthToDevicePixel( aInfo.GetDotLen() ), 1L ) );/*82*/}
        else
            {/*83*/SensorCall();aInfo.SetDotCount( 0 );/*84*/}

        SensorCall();if( aInfo.GetDashCount() && aInfo.GetDashLen() )
            {/*85*/aInfo.SetDashLen( std::max( ImplLogicWidthToDevicePixel( aInfo.GetDashLen() ), 1L ) );/*86*/}
        else
            {/*87*/SensorCall();aInfo.SetDashCount( 0 );/*88*/}

        SensorCall();aInfo.SetDistance( ImplLogicWidthToDevicePixel( aInfo.GetDistance() ) );

        SensorCall();if( ( !aInfo.GetDashCount() && !aInfo.GetDotCount() ) || !aInfo.GetDistance() )
            {/*89*/SensorCall();aInfo.SetStyle( LINE_SOLID );/*90*/}
    }

    SensorCall();aInfo.SetWidth( ImplLogicWidthToDevicePixel( aInfo.GetWidth() ) );

    {LineInfo  ReplaceReturn = aInfo; SensorCall(); return ReplaceReturn;}
}

Rectangle OutputDevice::ImplDevicePixelToLogic( const Rectangle& rPixelRect ) const
{
    SensorCall();if ( rPixelRect.IsEmpty() )
        {/*91*/{Rectangle  ReplaceReturn = rPixelRect; SensorCall(); return ReplaceReturn;}/*92*/}

    SensorCall();if ( !mbMap )
    {
        {Rectangle  ReplaceReturn = Rectangle( rPixelRect.Left()-mnOutOffX, rPixelRect.Top()-mnOutOffY,
                          rPixelRect.Right()-mnOutOffX, rPixelRect.Bottom()-mnOutOffY ); SensorCall(); return ReplaceReturn;}
    }

    {Rectangle  ReplaceReturn = Rectangle( ImplPixelToLogic( rPixelRect.Left()-mnOutOffX-mnOutOffOrigX, mnDPIX,
                                        maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                        maThresRes.mnThresPixToLogX )-maMapRes.mnMapOfsX,
                      ImplPixelToLogic( rPixelRect.Top()-mnOutOffY-mnOutOffOrigY, mnDPIY,
                                        maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                        maThresRes.mnThresPixToLogY )-maMapRes.mnMapOfsY,
                      ImplPixelToLogic( rPixelRect.Right()-mnOutOffX-mnOutOffOrigX, mnDPIX,
                                        maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                        maThresRes.mnThresPixToLogX )-maMapRes.mnMapOfsX,
                      ImplPixelToLogic( rPixelRect.Bottom()-mnOutOffY-mnOutOffOrigY, mnDPIY,
                                        maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                        maThresRes.mnThresPixToLogY )-maMapRes.mnMapOfsY ); SensorCall(); return ReplaceReturn;}
}

vcl::Region OutputDevice::ImplPixelToDevicePixel( const vcl::Region& rRegion ) const
{
    SensorCall();if ( !mnOutOffX && !mnOutOffY )
        {/*93*/{vcl::Region  ReplaceReturn = rRegion; SensorCall(); return ReplaceReturn;}/*94*/}

    SensorCall();vcl::Region aRegion( rRegion );
    aRegion.Move( mnOutOffX+mnOutOffOrigX, mnOutOffY+mnOutOffOrigY );
    {vcl::Region  ReplaceReturn = aRegion; SensorCall(); return ReplaceReturn;}
}

void OutputDevice::EnableMapMode( bool bEnable )
{
    SensorCall();mbMap = bEnable;

    if( mpAlphaVDev )
        mpAlphaVDev->EnableMapMode( bEnable );
SensorCall();}

void OutputDevice::SetMapMode()
{

    SensorCall();if ( mpMetaFile )
        {/*95*/SensorCall();mpMetaFile->AddAction( new MetaMapModeAction( MapMode() ) );/*96*/}

    SensorCall();if ( mbMap || !maMapMode.IsDefault() )
    {
        SensorCall();mbMap       = false;
        maMapMode   = MapMode();

        // create new objects (clip region are not re-scaled)
        mbNewFont   = true;
        mbInitFont  = true;
        SensorCall();if ( GetOutDevType() == OUTDEV_WINDOW )
        {
            SensorCall();if ( static_cast<vcl::Window*>(this)->mpWindowImpl->mpCursor )
                {/*97*/SensorCall();static_cast<vcl::Window*>(this)->mpWindowImpl->mpCursor->ImplNew();/*98*/}
        }

        // #106426# Adapt logical offset when changing mapmode
        SensorCall();mnOutOffLogicX = mnOutOffOrigX; // no mapping -> equal offsets
        mnOutOffLogicY = mnOutOffOrigY;

        // #i75163#
        ImplInvalidateViewTransform();
    }

    if( mpAlphaVDev )
        mpAlphaVDev->SetMapMode();
SensorCall();}

void OutputDevice::SetMapMode( const MapMode& rNewMapMode )
{

    SensorCall();bool bRelMap = (rNewMapMode.GetMapUnit() == MAP_RELATIVE);

    SensorCall();if ( mpMetaFile )
    {
        SensorCall();mpMetaFile->AddAction( new MetaMapModeAction( rNewMapMode ) );
#ifdef DBG_UTIL
        if ( GetOutDevType() != OUTDEV_PRINTER )
            DBG_ASSERTWARNING( bRelMap, "Please record only relative MapModes!" );
#endif
    }

    // do nothing if MapMode was not changed
    SensorCall();if ( maMapMode == rNewMapMode )
        {/*99*/SensorCall();return;/*100*/}

    if( mpAlphaVDev )
        mpAlphaVDev->SetMapMode( rNewMapMode );

     // if default MapMode calculate nothing
    SensorCall();bool bOldMap = mbMap;
    mbMap = !rNewMapMode.IsDefault();
    SensorCall();if ( mbMap )
    {
        // if only the origin is converted, do not scale new
        SensorCall();if ( (rNewMapMode.GetMapUnit() == maMapMode.GetMapUnit()) &&
             (rNewMapMode.GetScaleX()  == maMapMode.GetScaleX())  &&
             (rNewMapMode.GetScaleY()  == maMapMode.GetScaleY())  &&
             (bOldMap                  == mbMap) )
        {
            // set offset
            SensorCall();Point aOrigin = rNewMapMode.GetOrigin();
            maMapRes.mnMapOfsX = aOrigin.X();
            maMapRes.mnMapOfsY = aOrigin.Y();
            maMapRes.mfOffsetX = aOrigin.X();
            maMapRes.mfOffsetY = aOrigin.Y();
            maMapMode = rNewMapMode;

            // #i75163#
            ImplInvalidateViewTransform();

            SensorCall();return;
        }
        SensorCall();if ( !bOldMap && bRelMap )
        {
            SensorCall();maMapRes.mnMapScNumX    = 1;
            maMapRes.mnMapScNumY    = 1;
            maMapRes.mnMapScDenomX  = mnDPIX;
            maMapRes.mnMapScDenomY  = mnDPIY;
            maMapRes.mnMapOfsX      = 0;
            maMapRes.mnMapOfsY      = 0;
            maMapRes.mfOffsetX = 0.0;
            maMapRes.mfOffsetY = 0.0;
            maMapRes.mfScaleX = (double)1/(double)mnDPIX;
            maMapRes.mfScaleY = (double)1/(double)mnDPIY;
        }

        // calculate new MapMode-resolution
        SensorCall();ImplCalcMapResolution( rNewMapMode, mnDPIX, mnDPIY, maMapRes, maThresRes );
    }

    // set new MapMode
    SensorCall();if ( bRelMap )
    {
        SensorCall();Point aOrigin( maMapRes.mnMapOfsX, maMapRes.mnMapOfsY );
        // aScale? = maMapMode.GetScale?() * rNewMapMode.GetScale?()
        Fraction aScaleX = ImplMakeFraction( maMapMode.GetScaleX().GetNumerator(),
                                             rNewMapMode.GetScaleX().GetNumerator(),
                                             maMapMode.GetScaleX().GetDenominator(),
                                             rNewMapMode.GetScaleX().GetDenominator() );
        Fraction aScaleY = ImplMakeFraction( maMapMode.GetScaleY().GetNumerator(),
                                             rNewMapMode.GetScaleY().GetNumerator(),
                                             maMapMode.GetScaleY().GetDenominator(),
                                             rNewMapMode.GetScaleY().GetDenominator() );
        maMapMode.SetOrigin( aOrigin );
        maMapMode.SetScaleX( aScaleX );
        maMapMode.SetScaleY( aScaleY );
    }
    else
        {/*101*/SensorCall();maMapMode = rNewMapMode;/*102*/}

    // create new objects (clip region are not re-scaled)
    SensorCall();mbNewFont   = true;
    mbInitFont  = true;
    SensorCall();if ( GetOutDevType() == OUTDEV_WINDOW )
    {
        SensorCall();if ( static_cast<vcl::Window*>(this)->mpWindowImpl->mpCursor )
            {/*103*/SensorCall();static_cast<vcl::Window*>(this)->mpWindowImpl->mpCursor->ImplNew();/*104*/}
    }

    // #106426# Adapt logical offset when changing mapmode
    SensorCall();mnOutOffLogicX = ImplPixelToLogic( mnOutOffOrigX, mnDPIX,
                                       maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                       maThresRes.mnThresPixToLogX );
    mnOutOffLogicY = ImplPixelToLogic( mnOutOffOrigY, mnDPIY,
                                       maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                       maThresRes.mnThresPixToLogY );

    // #i75163#
    ImplInvalidateViewTransform();
SensorCall();}

void OutputDevice::SetRelativeMapMode( const MapMode& rNewMapMode )
{
    // do nothing if MapMode did not change
    SensorCall();if ( maMapMode == rNewMapMode )
        {/*105*/SensorCall();return;/*106*/}

    SensorCall();MapUnit eOld = maMapMode.GetMapUnit();
    MapUnit eNew = rNewMapMode.GetMapUnit();

    // a?F = rNewMapMode.GetScale?() / maMapMode.GetScale?()
    Fraction aXF = ImplMakeFraction( rNewMapMode.GetScaleX().GetNumerator(),
                                     maMapMode.GetScaleX().GetDenominator(),
                                     rNewMapMode.GetScaleX().GetDenominator(),
                                     maMapMode.GetScaleX().GetNumerator() );
    Fraction aYF = ImplMakeFraction( rNewMapMode.GetScaleY().GetNumerator(),
                                     maMapMode.GetScaleY().GetDenominator(),
                                     rNewMapMode.GetScaleY().GetDenominator(),
                                     maMapMode.GetScaleY().GetNumerator() );

    Point aPt( LogicToLogic( Point(), NULL, &rNewMapMode ) );
    SensorCall();if ( eNew != eOld )
    {
        SensorCall();if ( eOld > MAP_PIXEL )
        {
            SAL_WARN( "vcl.gdi", "Not implemented MapUnit" );
        }
        else {/*107*/SensorCall();if ( eNew > MAP_PIXEL )
        {
            SAL_WARN( "vcl.gdi", "Not implemented MapUnit" );
        }
        else
        {
            SensorCall();Fraction aF( aImplNumeratorAry[eNew] * aImplDenominatorAry[eOld],
                         aImplNumeratorAry[eOld] * aImplDenominatorAry[eNew] );

            // a?F =  a?F * aF
            aXF = ImplMakeFraction( aXF.GetNumerator(),   aF.GetNumerator(),
                                    aXF.GetDenominator(), aF.GetDenominator() );
            aYF = ImplMakeFraction( aYF.GetNumerator(),   aF.GetNumerator(),
                                    aYF.GetDenominator(), aF.GetDenominator() );
            SensorCall();if ( eOld == MAP_PIXEL )
            {
                SensorCall();aXF *= Fraction( mnDPIX, 1 );
                aYF *= Fraction( mnDPIY, 1 );
            }
            else {/*109*/SensorCall();if ( eNew == MAP_PIXEL )
            {
                SensorCall();aXF *= Fraction( 1, mnDPIX );
                aYF *= Fraction( 1, mnDPIY );
            ;/*110*/}}
        ;/*108*/}}
    }

    SensorCall();MapMode aNewMapMode( MAP_RELATIVE, Point( -aPt.X(), -aPt.Y() ), aXF, aYF );
    SetMapMode( aNewMapMode );

    SensorCall();if ( eNew != eOld )
        {/*111*/SensorCall();maMapMode = rNewMapMode;/*112*/}

    // #106426# Adapt logical offset when changing MapMode
    SensorCall();mnOutOffLogicX = ImplPixelToLogic( mnOutOffOrigX, mnDPIX,
                                       maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                       maThresRes.mnThresPixToLogX );
    mnOutOffLogicY = ImplPixelToLogic( mnOutOffOrigY, mnDPIY,
                                       maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                       maThresRes.mnThresPixToLogY );

    if( mpAlphaVDev )
        mpAlphaVDev->SetRelativeMapMode( rNewMapMode );
SensorCall();}

// #i75163#
basegfx::B2DHomMatrix OutputDevice::GetViewTransformation() const
{
    SensorCall();if(mbMap)
    {
        SensorCall();if(!mpOutDevData->mpViewTransform)
        {
            SensorCall();mpOutDevData->mpViewTransform = new basegfx::B2DHomMatrix;

            const double fScaleFactorX((double)mnDPIX * (double)maMapRes.mnMapScNumX / (double)maMapRes.mnMapScDenomX);
            const double fScaleFactorY((double)mnDPIY * (double)maMapRes.mnMapScNumY / (double)maMapRes.mnMapScDenomY);
            const double fZeroPointX(((double)maMapRes.mnMapOfsX * fScaleFactorX) + (double)mnOutOffOrigX);
            const double fZeroPointY(((double)maMapRes.mnMapOfsY * fScaleFactorY) + (double)mnOutOffOrigY);

            mpOutDevData->mpViewTransform->set(0, 0, fScaleFactorX);
            mpOutDevData->mpViewTransform->set(1, 1, fScaleFactorY);
            mpOutDevData->mpViewTransform->set(0, 2, fZeroPointX);
            mpOutDevData->mpViewTransform->set(1, 2, fZeroPointY);
        }

        {basegfx::B2DHomMatrix  ReplaceReturn = *mpOutDevData->mpViewTransform; SensorCall(); return ReplaceReturn;}
    }
    else
    {
        {basegfx::B2DHomMatrix  ReplaceReturn = basegfx::B2DHomMatrix(); SensorCall(); return ReplaceReturn;}
    }
SensorCall();}

// #i75163#
basegfx::B2DHomMatrix OutputDevice::GetInverseViewTransformation() const
{
    SensorCall();if(mbMap)
    {
        SensorCall();if(!mpOutDevData->mpInverseViewTransform)
        {
            SensorCall();GetViewTransformation();
            mpOutDevData->mpInverseViewTransform = new basegfx::B2DHomMatrix(*mpOutDevData->mpViewTransform);
            mpOutDevData->mpInverseViewTransform->invert();
        }

        {basegfx::B2DHomMatrix  ReplaceReturn = *mpOutDevData->mpInverseViewTransform; SensorCall(); return ReplaceReturn;}
    }
    else
    {
        {basegfx::B2DHomMatrix  ReplaceReturn = basegfx::B2DHomMatrix(); SensorCall(); return ReplaceReturn;}
    }
SensorCall();}

// #i75163#
basegfx::B2DHomMatrix OutputDevice::GetViewTransformation( const MapMode& rMapMode ) const
{
    // #i82615#
    SensorCall();ImplMapRes          aMapRes;
    ImplThresholdRes    aThresRes;
    ImplCalcMapResolution( rMapMode, mnDPIX, mnDPIY, aMapRes, aThresRes );

    basegfx::B2DHomMatrix aTransform;

    const double fScaleFactorX((double)mnDPIX * (double)aMapRes.mnMapScNumX / (double)aMapRes.mnMapScDenomX);
    const double fScaleFactorY((double)mnDPIY * (double)aMapRes.mnMapScNumY / (double)aMapRes.mnMapScDenomY);
    const double fZeroPointX(((double)aMapRes.mnMapOfsX * fScaleFactorX) + (double)mnOutOffOrigX);
    const double fZeroPointY(((double)aMapRes.mnMapOfsY * fScaleFactorY) + (double)mnOutOffOrigY);

    aTransform.set(0, 0, fScaleFactorX);
    aTransform.set(1, 1, fScaleFactorY);
    aTransform.set(0, 2, fZeroPointX);
    aTransform.set(1, 2, fZeroPointY);

    {basegfx::B2DHomMatrix  ReplaceReturn = aTransform; SensorCall(); return ReplaceReturn;}
}

// #i75163#
basegfx::B2DHomMatrix OutputDevice::GetInverseViewTransformation( const MapMode& rMapMode ) const
{
    SensorCall();basegfx::B2DHomMatrix aMatrix( GetViewTransformation( rMapMode ) );
    aMatrix.invert();
    {basegfx::B2DHomMatrix  ReplaceReturn = aMatrix; SensorCall(); return ReplaceReturn;}
}

basegfx::B2DHomMatrix OutputDevice::ImplGetDeviceTransformation() const
{
    SensorCall();basegfx::B2DHomMatrix aTransformation = GetViewTransformation();
    // TODO: is it worth to cache the transformed result?
    SensorCall();if( mnOutOffX || mnOutOffY )
        {/*113*/SensorCall();aTransformation.translate( mnOutOffX, mnOutOffY );/*114*/}
    {basegfx::B2DHomMatrix  ReplaceReturn = aTransformation; SensorCall(); return ReplaceReturn;}
}

Point OutputDevice::LogicToPixel( const Point& rLogicPt ) const
{

    SensorCall();if ( !mbMap )
        {/*115*/{Point  ReplaceReturn = rLogicPt; SensorCall(); return ReplaceReturn;}/*116*/}

    {Point  ReplaceReturn = Point( ImplLogicToPixel( rLogicPt.X() + maMapRes.mnMapOfsX, mnDPIX,
                                    maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                    maThresRes.mnThresLogToPixX )+mnOutOffOrigX,
                  ImplLogicToPixel( rLogicPt.Y() + maMapRes.mnMapOfsY, mnDPIY,
                                    maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                    maThresRes.mnThresLogToPixY )+mnOutOffOrigY ); SensorCall(); return ReplaceReturn;}
}

Size OutputDevice::LogicToPixel( const Size& rLogicSize ) const
{

    SensorCall();if ( !mbMap )
        {/*117*/{Size  ReplaceReturn = rLogicSize; SensorCall(); return ReplaceReturn;}/*118*/}

    {Size  ReplaceReturn = Size( ImplLogicToPixel( rLogicSize.Width(), mnDPIX,
                                   maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                   maThresRes.mnThresLogToPixX ),
                 ImplLogicToPixel( rLogicSize.Height(), mnDPIY,
                                   maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                   maThresRes.mnThresLogToPixY ) ); SensorCall(); return ReplaceReturn;}
}

Rectangle OutputDevice::LogicToPixel( const Rectangle& rLogicRect ) const
{

    SensorCall();if ( !mbMap || rLogicRect.IsEmpty() )
        {/*119*/{Rectangle  ReplaceReturn = rLogicRect; SensorCall(); return ReplaceReturn;}/*120*/}

    {Rectangle  ReplaceReturn = Rectangle( ImplLogicToPixel( rLogicRect.Left() + maMapRes.mnMapOfsX, mnDPIX,
                                        maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                        maThresRes.mnThresLogToPixX )+mnOutOffOrigX,
                      ImplLogicToPixel( rLogicRect.Top() + maMapRes.mnMapOfsY, mnDPIY,
                                        maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                        maThresRes.mnThresLogToPixY )+mnOutOffOrigY,
                      ImplLogicToPixel( rLogicRect.Right() + maMapRes.mnMapOfsX, mnDPIX,
                                        maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                        maThresRes.mnThresLogToPixX )+mnOutOffOrigX,
                      ImplLogicToPixel( rLogicRect.Bottom() + maMapRes.mnMapOfsY, mnDPIY,
                                        maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                        maThresRes.mnThresLogToPixY )+mnOutOffOrigY ); SensorCall(); return ReplaceReturn;}
}

Polygon OutputDevice::LogicToPixel( const Polygon& rLogicPoly ) const
{

    SensorCall();if ( !mbMap )
        {/*121*/{Polygon  ReplaceReturn = rLogicPoly; SensorCall(); return ReplaceReturn;}/*122*/}

    SensorCall();sal_uInt16  i;
    sal_uInt16  nPoints = rLogicPoly.GetSize();
    Polygon aPoly( rLogicPoly );

    // get pointer to Point-array (copy data)
    const Point* pPointAry = aPoly.GetConstPointAry();

    SensorCall();for ( i = 0; i < nPoints; i++ )
    {
        SensorCall();const Point* pPt = &(pPointAry[i]);
        Point aPt;
        aPt.X() = ImplLogicToPixel( pPt->X() + maMapRes.mnMapOfsX, mnDPIX,
                                    maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                    maThresRes.mnThresLogToPixX )+mnOutOffOrigX;
        aPt.Y() = ImplLogicToPixel( pPt->Y() + maMapRes.mnMapOfsY, mnDPIY,
                                    maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                    maThresRes.mnThresLogToPixY )+mnOutOffOrigY;
        aPoly[i] = aPt;
    }

    {Polygon  ReplaceReturn = aPoly; SensorCall(); return ReplaceReturn;}
}

tools::PolyPolygon OutputDevice::LogicToPixel( const tools::PolyPolygon& rLogicPolyPoly ) const
{

    SensorCall();if ( !mbMap )
        {/*123*/{tools::PolyPolygon  ReplaceReturn = rLogicPolyPoly; SensorCall(); return ReplaceReturn;}/*124*/}

    SensorCall();tools::PolyPolygon aPolyPoly( rLogicPolyPoly );
    sal_uInt16      nPoly = aPolyPoly.Count();
    SensorCall();for( sal_uInt16 i = 0; i < nPoly; i++ )
    {
        SensorCall();Polygon& rPoly = aPolyPoly[i];
        rPoly = LogicToPixel( rPoly );
    }
    {tools::PolyPolygon  ReplaceReturn = aPolyPoly; SensorCall(); return ReplaceReturn;}
}

basegfx::B2DPolygon OutputDevice::LogicToPixel( const basegfx::B2DPolygon& rLogicPoly ) const
{
    SensorCall();basegfx::B2DPolygon aTransformedPoly = rLogicPoly;
    const ::basegfx::B2DHomMatrix& rTransformationMatrix = GetViewTransformation();
    aTransformedPoly.transform( rTransformationMatrix );
    {basegfx::B2DPolygon  ReplaceReturn = aTransformedPoly; SensorCall(); return ReplaceReturn;}
}

basegfx::B2DPolyPolygon OutputDevice::LogicToPixel( const basegfx::B2DPolyPolygon& rLogicPolyPoly ) const
{
    SensorCall();basegfx::B2DPolyPolygon aTransformedPoly = rLogicPolyPoly;
    const ::basegfx::B2DHomMatrix& rTransformationMatrix = GetViewTransformation();
    aTransformedPoly.transform( rTransformationMatrix );
    {basegfx::B2DPolyPolygon  ReplaceReturn = aTransformedPoly; SensorCall(); return ReplaceReturn;}
}

vcl::Region OutputDevice::LogicToPixel( const vcl::Region& rLogicRegion ) const
{

    SensorCall();if(!mbMap || rLogicRegion.IsNull() || rLogicRegion.IsEmpty())
    {
        {vcl::Region  ReplaceReturn = rLogicRegion; SensorCall(); return ReplaceReturn;}
    }

    SensorCall();vcl::Region aRegion;

    SensorCall();if(rLogicRegion.getB2DPolyPolygon())
    {
        SensorCall();aRegion = vcl::Region(LogicToPixel(*rLogicRegion.getB2DPolyPolygon()));
    }
    else {/*125*/SensorCall();if(rLogicRegion.getPolyPolygon())
    {
        SensorCall();aRegion = vcl::Region(LogicToPixel(*rLogicRegion.getPolyPolygon()));
    }
    else {/*127*/SensorCall();if(rLogicRegion.getRegionBand())
    {
        SensorCall();RectangleVector aRectangles;
        rLogicRegion.GetRegionRectangles(aRectangles);
        const RectangleVector& rRectangles(aRectangles); // needed to make the '!=' work

        // make reverse run to fill new region bottom-up, this will speed it up due to the used data structuring
        for(RectangleVector::const_reverse_iterator aRectIter(rRectangles.rbegin()); aRectIter != rRectangles.rend(); ++aRectIter)
        {
            aRegion.Union(LogicToPixel(*aRectIter));
        }
    ;/*128*/}/*126*/}}

    {vcl::Region  ReplaceReturn = aRegion; SensorCall(); return ReplaceReturn;}
}

Point OutputDevice::LogicToPixel( const Point& rLogicPt,
                                  const MapMode& rMapMode ) const
{

    SensorCall();if ( rMapMode.IsDefault() )
        {/*129*/{Point  ReplaceReturn = rLogicPt; SensorCall(); return ReplaceReturn;}/*130*/}

    // convert MapMode resolution and convert
    SensorCall();ImplMapRes          aMapRes;
    ImplThresholdRes    aThresRes;
    ImplCalcMapResolution( rMapMode, mnDPIX, mnDPIY, aMapRes, aThresRes );

    {Point  ReplaceReturn = Point( ImplLogicToPixel( rLogicPt.X() + aMapRes.mnMapOfsX, mnDPIX,
                                    aMapRes.mnMapScNumX, aMapRes.mnMapScDenomX,
                                    aThresRes.mnThresLogToPixX )+mnOutOffOrigX,
                  ImplLogicToPixel( rLogicPt.Y() + aMapRes.mnMapOfsY, mnDPIY,
                                    aMapRes.mnMapScNumY, aMapRes.mnMapScDenomY,
                                    aThresRes.mnThresLogToPixY )+mnOutOffOrigY ); SensorCall(); return ReplaceReturn;}
}

Size OutputDevice::LogicToPixel( const Size& rLogicSize,
                                 const MapMode& rMapMode ) const
{

    SensorCall();if ( rMapMode.IsDefault() )
        {/*131*/{Size  ReplaceReturn = rLogicSize; SensorCall(); return ReplaceReturn;}/*132*/}

    // convert MapMode resolution and convert
    SensorCall();ImplMapRes          aMapRes;
    ImplThresholdRes    aThresRes;
    ImplCalcMapResolution( rMapMode, mnDPIX, mnDPIY, aMapRes, aThresRes );

    {Size  ReplaceReturn = Size( ImplLogicToPixel( rLogicSize.Width(), mnDPIX,
                                   aMapRes.mnMapScNumX, aMapRes.mnMapScDenomX,
                                   aThresRes.mnThresLogToPixX ),
                 ImplLogicToPixel( rLogicSize.Height(), mnDPIY,
                                   aMapRes.mnMapScNumY, aMapRes.mnMapScDenomY,
                                   aThresRes.mnThresLogToPixY ) ); SensorCall(); return ReplaceReturn;}
}

Rectangle OutputDevice::LogicToPixel( const Rectangle& rLogicRect,
                                      const MapMode& rMapMode ) const
{

    SensorCall();if ( rMapMode.IsDefault() || rLogicRect.IsEmpty() )
        {/*133*/{Rectangle  ReplaceReturn = rLogicRect; SensorCall(); return ReplaceReturn;}/*134*/}

    // convert MapMode resolution and convert
    SensorCall();ImplMapRes          aMapRes;
    ImplThresholdRes    aThresRes;
    ImplCalcMapResolution( rMapMode, mnDPIX, mnDPIY, aMapRes, aThresRes );

    {Rectangle  ReplaceReturn = Rectangle( ImplLogicToPixel( rLogicRect.Left() + aMapRes.mnMapOfsX, mnDPIX,
                                        aMapRes.mnMapScNumX, aMapRes.mnMapScDenomX,
                                        aThresRes.mnThresLogToPixX )+mnOutOffOrigX,
                      ImplLogicToPixel( rLogicRect.Top() + aMapRes.mnMapOfsY, mnDPIY,
                                        aMapRes.mnMapScNumY, aMapRes.mnMapScDenomY,
                                        aThresRes.mnThresLogToPixY )+mnOutOffOrigY,
                      ImplLogicToPixel( rLogicRect.Right() + aMapRes.mnMapOfsX, mnDPIX,
                                        aMapRes.mnMapScNumX, aMapRes.mnMapScDenomX,
                                        aThresRes.mnThresLogToPixX )+mnOutOffOrigX,
                      ImplLogicToPixel( rLogicRect.Bottom() + aMapRes.mnMapOfsY, mnDPIY,
                                        aMapRes.mnMapScNumY, aMapRes.mnMapScDenomY,
                                        aThresRes.mnThresLogToPixY )+mnOutOffOrigY ); SensorCall(); return ReplaceReturn;}
}

Polygon OutputDevice::LogicToPixel( const Polygon& rLogicPoly,
                                    const MapMode& rMapMode ) const
{

    SensorCall();if ( rMapMode.IsDefault() )
        {/*135*/{Polygon  ReplaceReturn = rLogicPoly; SensorCall(); return ReplaceReturn;}/*136*/}

    // convert MapMode resolution and convert
    SensorCall();ImplMapRes          aMapRes;
    ImplThresholdRes    aThresRes;
    ImplCalcMapResolution( rMapMode, mnDPIX, mnDPIY, aMapRes, aThresRes );

    sal_uInt16  i;
    sal_uInt16  nPoints = rLogicPoly.GetSize();
    Polygon aPoly( rLogicPoly );

    // get pointer to Point-array (copy data)
    const Point* pPointAry = aPoly.GetConstPointAry();

    SensorCall();for ( i = 0; i < nPoints; i++ )
    {
        SensorCall();const Point* pPt = &(pPointAry[i]);
        Point aPt;
        aPt.X() = ImplLogicToPixel( pPt->X() + aMapRes.mnMapOfsX, mnDPIX,
                                    aMapRes.mnMapScNumX, aMapRes.mnMapScDenomX,
                                    aThresRes.mnThresLogToPixX )+mnOutOffOrigX;
        aPt.Y() = ImplLogicToPixel( pPt->Y() + aMapRes.mnMapOfsY, mnDPIY,
                                    aMapRes.mnMapScNumY, aMapRes.mnMapScDenomY,
                                    aThresRes.mnThresLogToPixY )+mnOutOffOrigY;
        aPoly[i] = aPt;
    }

    {Polygon  ReplaceReturn = aPoly; SensorCall(); return ReplaceReturn;}
}

tools::PolyPolygon OutputDevice::LogicToPixel( const tools::PolyPolygon& rLogicPolyPoly,
    const MapMode& rMapMode ) const
{

    SensorCall();if ( rMapMode.IsDefault() )
        {/*137*/{tools::PolyPolygon  ReplaceReturn = rLogicPolyPoly; SensorCall(); return ReplaceReturn;}/*138*/}

    SensorCall();tools::PolyPolygon aPolyPoly( rLogicPolyPoly );
    sal_uInt16              nPoly = aPolyPoly.Count();
    SensorCall();for( sal_uInt16 i = 0; i < nPoly; i++ )
    {
        SensorCall();Polygon& rPoly = aPolyPoly[i];
        rPoly = LogicToPixel( rPoly, rMapMode );
    }
    {tools::PolyPolygon  ReplaceReturn = aPolyPoly; SensorCall(); return ReplaceReturn;}
}

basegfx::B2DPolyPolygon OutputDevice::LogicToPixel( const basegfx::B2DPolyPolygon& rLogicPolyPoly,
                                                    const MapMode& rMapMode ) const
{
    SensorCall();basegfx::B2DPolyPolygon aTransformedPoly = rLogicPolyPoly;
    const ::basegfx::B2DHomMatrix& rTransformationMatrix = GetViewTransformation( rMapMode );
    aTransformedPoly.transform( rTransformationMatrix );
    {basegfx::B2DPolyPolygon  ReplaceReturn = aTransformedPoly; SensorCall(); return ReplaceReturn;}
}

basegfx::B2DPolygon OutputDevice::LogicToPixel( const basegfx::B2DPolygon& rLogicPoly,
                                                const MapMode& rMapMode ) const
{
    SensorCall();basegfx::B2DPolygon aTransformedPoly = rLogicPoly;
    const ::basegfx::B2DHomMatrix& rTransformationMatrix = GetViewTransformation( rMapMode );
    aTransformedPoly.transform( rTransformationMatrix );
    {basegfx::B2DPolygon  ReplaceReturn = aTransformedPoly; SensorCall(); return ReplaceReturn;}
}

vcl::Region OutputDevice::LogicToPixel( const vcl::Region& rLogicRegion, const MapMode& rMapMode ) const
{

    SensorCall();if(rMapMode.IsDefault() || rLogicRegion.IsNull() || rLogicRegion.IsEmpty())
    {
        {vcl::Region  ReplaceReturn = rLogicRegion; SensorCall(); return ReplaceReturn;}
    }

    SensorCall();vcl::Region aRegion;

    SensorCall();if(rLogicRegion.getB2DPolyPolygon())
    {
        SensorCall();aRegion = vcl::Region(LogicToPixel(*rLogicRegion.getB2DPolyPolygon(), rMapMode));
    }
    else {/*139*/SensorCall();if(rLogicRegion.getPolyPolygon())
    {
        SensorCall();aRegion = vcl::Region(LogicToPixel(*rLogicRegion.getPolyPolygon(), rMapMode));
    }
    else {/*141*/SensorCall();if(rLogicRegion.getRegionBand())
    {
        SensorCall();RectangleVector aRectangles;
        rLogicRegion.GetRegionRectangles(aRectangles);
        const RectangleVector& rRectangles(aRectangles); // needed to make the '!=' work

        // make reverse run to fill new region bottom-up, this will speed it up due to the used data structuring
        for(RectangleVector::const_reverse_iterator aRectIter(rRectangles.rbegin()); aRectIter != rRectangles.rend(); ++aRectIter)
        {
            aRegion.Union(LogicToPixel(*aRectIter, rMapMode));
        }
    ;/*142*/}/*140*/}}

    {vcl::Region  ReplaceReturn = aRegion; SensorCall(); return ReplaceReturn;}
}

Point OutputDevice::PixelToLogic( const Point& rDevicePt ) const
{

    SensorCall();if ( !mbMap )
        {/*143*/{Point  ReplaceReturn = rDevicePt; SensorCall(); return ReplaceReturn;}/*144*/}

    {Point  ReplaceReturn = Point( ImplPixelToLogic( rDevicePt.X(), mnDPIX,
                                    maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                    maThresRes.mnThresPixToLogX ) - maMapRes.mnMapOfsX - mnOutOffLogicX,
                  ImplPixelToLogic( rDevicePt.Y(), mnDPIY,
                                    maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                    maThresRes.mnThresPixToLogY ) - maMapRes.mnMapOfsY - mnOutOffLogicY ); SensorCall(); return ReplaceReturn;}
}

Size OutputDevice::PixelToLogic( const Size& rDeviceSize ) const
{

    SensorCall();if ( !mbMap )
        {/*145*/{Size  ReplaceReturn = rDeviceSize; SensorCall(); return ReplaceReturn;}/*146*/}

    {Size  ReplaceReturn = Size( ImplPixelToLogic( rDeviceSize.Width(), mnDPIX,
                                   maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                   maThresRes.mnThresPixToLogX ),
                 ImplPixelToLogic( rDeviceSize.Height(), mnDPIY,
                                   maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                   maThresRes.mnThresPixToLogY ) ); SensorCall(); return ReplaceReturn;}
}

Rectangle OutputDevice::PixelToLogic( const Rectangle& rDeviceRect ) const
{

    SensorCall();if ( !mbMap || rDeviceRect.IsEmpty() )
        {/*147*/{Rectangle  ReplaceReturn = rDeviceRect; SensorCall(); return ReplaceReturn;}/*148*/}

    {Rectangle  ReplaceReturn = Rectangle( ImplPixelToLogic( rDeviceRect.Left(), mnDPIX,
                                        maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                        maThresRes.mnThresPixToLogX ) - maMapRes.mnMapOfsX - mnOutOffLogicX,
                      ImplPixelToLogic( rDeviceRect.Top(), mnDPIY,
                                        maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                        maThresRes.mnThresPixToLogY ) - maMapRes.mnMapOfsY - mnOutOffLogicY,
                      ImplPixelToLogic( rDeviceRect.Right(), mnDPIX,
                                        maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                        maThresRes.mnThresPixToLogX ) - maMapRes.mnMapOfsX - mnOutOffLogicX,
                      ImplPixelToLogic( rDeviceRect.Bottom(), mnDPIY,
                                        maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                        maThresRes.mnThresPixToLogY ) - maMapRes.mnMapOfsY - mnOutOffLogicY ); SensorCall(); return ReplaceReturn;}
}

Polygon OutputDevice::PixelToLogic( const Polygon& rDevicePoly ) const
{

    SensorCall();if ( !mbMap )
        {/*149*/{Polygon  ReplaceReturn = rDevicePoly; SensorCall(); return ReplaceReturn;}/*150*/}

    SensorCall();sal_uInt16  i;
    sal_uInt16  nPoints = rDevicePoly.GetSize();
    Polygon aPoly( rDevicePoly );

    // get pointer to Point-array (copy data)
    const Point* pPointAry = aPoly.GetConstPointAry();

    SensorCall();for ( i = 0; i < nPoints; i++ )
    {
        SensorCall();const Point* pPt = &(pPointAry[i]);
        Point aPt;
        aPt.X() = ImplPixelToLogic( pPt->X(), mnDPIX,
                                    maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                    maThresRes.mnThresPixToLogX ) - maMapRes.mnMapOfsX - mnOutOffLogicX;
        aPt.Y() = ImplPixelToLogic( pPt->Y(), mnDPIY,
                                    maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                    maThresRes.mnThresPixToLogY ) - maMapRes.mnMapOfsY - mnOutOffLogicY;
        aPoly[i] = aPt;
    }

    {Polygon  ReplaceReturn = aPoly; SensorCall(); return ReplaceReturn;}
}

tools::PolyPolygon OutputDevice::PixelToLogic( const tools::PolyPolygon& rDevicePolyPoly ) const
{

    SensorCall();if ( !mbMap )
        {/*151*/{tools::PolyPolygon  ReplaceReturn = rDevicePolyPoly; SensorCall(); return ReplaceReturn;}/*152*/}

    SensorCall();tools::PolyPolygon aPolyPoly( rDevicePolyPoly );
    sal_uInt16      nPoly = aPolyPoly.Count();
    SensorCall();for( sal_uInt16 i = 0; i < nPoly; i++ )
    {
        SensorCall();Polygon& rPoly = aPolyPoly[i];
        rPoly = PixelToLogic( rPoly );
    }
    {tools::PolyPolygon  ReplaceReturn = aPolyPoly; SensorCall(); return ReplaceReturn;}
}

basegfx::B2DPolyPolygon OutputDevice::PixelToLogic( const basegfx::B2DPolyPolygon& rPixelPolyPoly ) const
{
    SensorCall();basegfx::B2DPolyPolygon aTransformedPoly = rPixelPolyPoly;
    const ::basegfx::B2DHomMatrix& rTransformationMatrix = GetInverseViewTransformation();
    aTransformedPoly.transform( rTransformationMatrix );
    {basegfx::B2DPolyPolygon  ReplaceReturn = aTransformedPoly; SensorCall(); return ReplaceReturn;}
}

vcl::Region OutputDevice::PixelToLogic( const vcl::Region& rDeviceRegion ) const
{

    SensorCall();if(!mbMap || rDeviceRegion.IsNull() || rDeviceRegion.IsEmpty())
    {
        {vcl::Region  ReplaceReturn = rDeviceRegion; SensorCall(); return ReplaceReturn;}
    }

    SensorCall();vcl::Region aRegion;

    SensorCall();if(rDeviceRegion.getB2DPolyPolygon())
    {
        SensorCall();aRegion = vcl::Region(PixelToLogic(*rDeviceRegion.getB2DPolyPolygon()));
    }
    else {/*153*/SensorCall();if(rDeviceRegion.getPolyPolygon())
    {
        SensorCall();aRegion = vcl::Region(PixelToLogic(*rDeviceRegion.getPolyPolygon()));
    }
    else {/*155*/SensorCall();if(rDeviceRegion.getRegionBand())
    {
        SensorCall();RectangleVector aRectangles;
        rDeviceRegion.GetRegionRectangles(aRectangles);
        const RectangleVector& rRectangles(aRectangles); // needed to make the '!=' work

        // make reverse run to fill new region bottom-up, this will speed it up due to the used data structuring
        for(RectangleVector::const_reverse_iterator aRectIter(rRectangles.rbegin()); aRectIter != rRectangles.rend(); ++aRectIter)
        {
            aRegion.Union(PixelToLogic(*aRectIter));
        }
    ;/*156*/}/*154*/}}

    {vcl::Region  ReplaceReturn = aRegion; SensorCall(); return ReplaceReturn;}
}

Point OutputDevice::PixelToLogic( const Point& rDevicePt,
                                  const MapMode& rMapMode ) const
{

    // calculate nothing if default-MapMode
    SensorCall();if ( rMapMode.IsDefault() )
        {/*157*/{Point  ReplaceReturn = rDevicePt; SensorCall(); return ReplaceReturn;}/*158*/}

    // calculate MapMode-resolution and convert
    SensorCall();ImplMapRes          aMapRes;
    ImplThresholdRes    aThresRes;
    ImplCalcMapResolution( rMapMode, mnDPIX, mnDPIY, aMapRes, aThresRes );

    {Point  ReplaceReturn = Point( ImplPixelToLogic( rDevicePt.X(), mnDPIX,
                                    aMapRes.mnMapScNumX, aMapRes.mnMapScDenomX,
                                    aThresRes.mnThresPixToLogX ) - aMapRes.mnMapOfsX - mnOutOffLogicX,
                  ImplPixelToLogic( rDevicePt.Y(), mnDPIY,
                                    aMapRes.mnMapScNumY, aMapRes.mnMapScDenomY,
                                    aThresRes.mnThresPixToLogY ) - aMapRes.mnMapOfsY - mnOutOffLogicY ); SensorCall(); return ReplaceReturn;}
}

Size OutputDevice::PixelToLogic( const Size& rDeviceSize,
                                 const MapMode& rMapMode ) const
{

    // calculate nothing if default-MapMode
    SensorCall();if ( rMapMode.IsDefault() )
        {/*159*/{Size  ReplaceReturn = rDeviceSize; SensorCall(); return ReplaceReturn;}/*160*/}

    // calculate MapMode-resolution and convert
    SensorCall();ImplMapRes          aMapRes;
    ImplThresholdRes    aThresRes;
    ImplCalcMapResolution( rMapMode, mnDPIX, mnDPIY, aMapRes, aThresRes );

    {Size  ReplaceReturn = Size( ImplPixelToLogic( rDeviceSize.Width(), mnDPIX,
                                   aMapRes.mnMapScNumX, aMapRes.mnMapScDenomX,
                                   aThresRes.mnThresPixToLogX ),
                 ImplPixelToLogic( rDeviceSize.Height(), mnDPIY,
                                   aMapRes.mnMapScNumY, aMapRes.mnMapScDenomY,
                                   aThresRes.mnThresPixToLogY ) ); SensorCall(); return ReplaceReturn;}
}

Rectangle OutputDevice::PixelToLogic( const Rectangle& rDeviceRect,
                                      const MapMode& rMapMode ) const
{

    // calculate nothing if default-MapMode
    SensorCall();if ( rMapMode.IsDefault() || rDeviceRect.IsEmpty() )
        {/*161*/{Rectangle  ReplaceReturn = rDeviceRect; SensorCall(); return ReplaceReturn;}/*162*/}

    // calculate MapMode-resolution and convert
    SensorCall();ImplMapRes          aMapRes;
    ImplThresholdRes    aThresRes;
    ImplCalcMapResolution( rMapMode, mnDPIX, mnDPIY, aMapRes, aThresRes );

    {Rectangle  ReplaceReturn = Rectangle( ImplPixelToLogic( rDeviceRect.Left(), mnDPIX,
                                        aMapRes.mnMapScNumX, aMapRes.mnMapScDenomX,
                                        aThresRes.mnThresPixToLogX ) - aMapRes.mnMapOfsX - mnOutOffLogicX,
                      ImplPixelToLogic( rDeviceRect.Top(), mnDPIY,
                                        aMapRes.mnMapScNumY, aMapRes.mnMapScDenomY,
                                        aThresRes.mnThresPixToLogY ) - aMapRes.mnMapOfsY - mnOutOffLogicY,
                      ImplPixelToLogic( rDeviceRect.Right(), mnDPIX,
                                        aMapRes.mnMapScNumX, aMapRes.mnMapScDenomX,
                                        aThresRes.mnThresPixToLogX ) - aMapRes.mnMapOfsX - mnOutOffLogicX,
                      ImplPixelToLogic( rDeviceRect.Bottom(), mnDPIY,
                                        aMapRes.mnMapScNumY, aMapRes.mnMapScDenomY,
                                        aThresRes.mnThresPixToLogY ) - aMapRes.mnMapOfsY - mnOutOffLogicY ); SensorCall(); return ReplaceReturn;}
}

Polygon OutputDevice::PixelToLogic( const Polygon& rDevicePoly,
                                    const MapMode& rMapMode ) const
{

    // calculate nothing if default-MapMode
    SensorCall();if ( rMapMode.IsDefault() )
        {/*163*/{Polygon  ReplaceReturn = rDevicePoly; SensorCall(); return ReplaceReturn;}/*164*/}

    // calculate MapMode-resolution and convert
    SensorCall();ImplMapRes          aMapRes;
    ImplThresholdRes    aThresRes;
    ImplCalcMapResolution( rMapMode, mnDPIX, mnDPIY, aMapRes, aThresRes );

    sal_uInt16  i;
    sal_uInt16  nPoints = rDevicePoly.GetSize();
    Polygon aPoly( rDevicePoly );

    // get pointer to Point-array (copy data)
    const Point* pPointAry = aPoly.GetConstPointAry();

    SensorCall();for ( i = 0; i < nPoints; i++ )
    {
        SensorCall();const Point* pPt = &(pPointAry[i]);
        Point aPt;
        aPt.X() = ImplPixelToLogic( pPt->X(), mnDPIX,
                                    aMapRes.mnMapScNumX, aMapRes.mnMapScDenomX,
                                    aThresRes.mnThresPixToLogX ) - aMapRes.mnMapOfsX - mnOutOffLogicX;
        aPt.Y() = ImplPixelToLogic( pPt->Y(), mnDPIY,
                                    aMapRes.mnMapScNumY, aMapRes.mnMapScDenomY,
                                    aThresRes.mnThresPixToLogY ) - aMapRes.mnMapOfsY - mnOutOffLogicY;
        aPoly[i] = aPt;
    }

    {Polygon  ReplaceReturn = aPoly; SensorCall(); return ReplaceReturn;}
}

tools::PolyPolygon OutputDevice::PixelToLogic( const tools::PolyPolygon& rDevicePolyPoly,
    const MapMode& rMapMode ) const
{

    SensorCall();if ( rMapMode.IsDefault() )
        {/*165*/{tools::PolyPolygon  ReplaceReturn = rDevicePolyPoly; SensorCall(); return ReplaceReturn;}/*166*/}

    SensorCall();tools::PolyPolygon aPolyPoly( rDevicePolyPoly );
    sal_uInt16      nPoly = aPolyPoly.Count();
    SensorCall();for( sal_uInt16 i = 0; i < nPoly; i++ )
    {
        SensorCall();Polygon& rPoly = aPolyPoly[i];
        rPoly = PixelToLogic( rPoly, rMapMode );
    }
    {tools::PolyPolygon  ReplaceReturn = aPolyPoly; SensorCall(); return ReplaceReturn;}
}

basegfx::B2DPolygon OutputDevice::PixelToLogic( const basegfx::B2DPolygon& rPixelPoly,
                                                const MapMode& rMapMode ) const
{
    SensorCall();basegfx::B2DPolygon aTransformedPoly = rPixelPoly;
    const ::basegfx::B2DHomMatrix& rTransformationMatrix = GetInverseViewTransformation( rMapMode );
    aTransformedPoly.transform( rTransformationMatrix );
    {basegfx::B2DPolygon  ReplaceReturn = aTransformedPoly; SensorCall(); return ReplaceReturn;}
}

basegfx::B2DPolyPolygon OutputDevice::PixelToLogic( const basegfx::B2DPolyPolygon& rPixelPolyPoly,
                                                    const MapMode& rMapMode ) const
{
    SensorCall();basegfx::B2DPolyPolygon aTransformedPoly = rPixelPolyPoly;
    const ::basegfx::B2DHomMatrix& rTransformationMatrix = GetInverseViewTransformation( rMapMode );
    aTransformedPoly.transform( rTransformationMatrix );
    {basegfx::B2DPolyPolygon  ReplaceReturn = aTransformedPoly; SensorCall(); return ReplaceReturn;}
}

vcl::Region OutputDevice::PixelToLogic( const vcl::Region& rDeviceRegion, const MapMode& rMapMode ) const
{

    SensorCall();if(rMapMode.IsDefault() || rDeviceRegion.IsNull() || rDeviceRegion.IsEmpty())
    {
        {vcl::Region  ReplaceReturn = rDeviceRegion; SensorCall(); return ReplaceReturn;}
    }

    SensorCall();vcl::Region aRegion;

    SensorCall();if(rDeviceRegion.getB2DPolyPolygon())
    {
        SensorCall();aRegion = vcl::Region(PixelToLogic(*rDeviceRegion.getB2DPolyPolygon(), rMapMode));
    }
    else {/*167*/SensorCall();if(rDeviceRegion.getPolyPolygon())
    {
        SensorCall();aRegion = vcl::Region(PixelToLogic(*rDeviceRegion.getPolyPolygon(), rMapMode));
    }
    else {/*169*/SensorCall();if(rDeviceRegion.getRegionBand())
    {
        SensorCall();RectangleVector aRectangles;
        rDeviceRegion.GetRegionRectangles(aRectangles);
        const RectangleVector& rRectangles(aRectangles); // needed to make the '!=' work

        // make reverse run to fill new region bottom-up, this will speed it up due to the used data structuring
        for(RectangleVector::const_reverse_iterator aRectIter(rRectangles.rbegin()); aRectIter != rRectangles.rend(); ++aRectIter)
        {
            aRegion.Union(PixelToLogic(*aRectIter, rMapMode));
        }
    ;/*170*/}/*168*/}}

    {vcl::Region  ReplaceReturn = aRegion; SensorCall(); return ReplaceReturn;}
}

#define ENTER1( rSource, pMapModeSource, pMapModeDest )                 \
    if ( !pMapModeSource )                                              \
        pMapModeSource = &maMapMode;                                    \
    if ( !pMapModeDest )                                                \
        pMapModeDest = &maMapMode;                                      \
    if ( *pMapModeSource == *pMapModeDest )                             \
        return rSource;                                                 \
                                                                        \
    ImplMapRes aMapResSource;                                           \
    aMapResSource.mnMapOfsX          = 0;                               \
    aMapResSource.mnMapOfsY          = 0;                               \
    aMapResSource.mnMapScNumX        = 1;                               \
    aMapResSource.mnMapScNumY        = 1;                               \
    aMapResSource.mnMapScDenomX      = 1;                               \
    aMapResSource.mnMapScDenomY      = 1;                               \
    aMapResSource.mfOffsetX          = 0.0;                             \
    aMapResSource.mfOffsetY          = 0.0;                             \
    aMapResSource.mfScaleX           = 1.0;                             \
    aMapResSource.mfScaleY           = 1.0;                             \
    ImplMapRes aMapResDest(aMapResSource);                              \
                                                                        \
    if ( !mbMap || pMapModeSource != &maMapMode )                       \
    {                                                                   \
        if ( pMapModeSource->GetMapUnit() == MAP_RELATIVE )             \
            aMapResSource = maMapRes;                                   \
        ImplCalcMapResolution( *pMapModeSource,                         \
                               mnDPIX, mnDPIY, aMapResSource );         \
    }                                                                   \
    else                                                                \
        aMapResSource = maMapRes;                                       \
    if ( !mbMap || pMapModeDest != &maMapMode )                         \
    {                                                                   \
        if ( pMapModeDest->GetMapUnit() == MAP_RELATIVE )               \
            aMapResDest = maMapRes;                                     \
        ImplCalcMapResolution( *pMapModeDest,                           \
                               mnDPIX, mnDPIY, aMapResDest );           \
    }                                                                   \
    else                                                                \
        aMapResDest = maMapRes

static void verifyUnitSourceDest( MapUnit eUnitSource, MapUnit eUnitDest )
{
    SensorCall();(void) eUnitSource;
    (void) eUnitDest;
    DBG_ASSERT( eUnitSource != MAP_SYSFONT
                && eUnitSource != MAP_APPFONT
                && eUnitSource != MAP_RELATIVE,
                "Source MapUnit nicht erlaubt" );
    DBG_ASSERT( eUnitDest != MAP_SYSFONT
                && eUnitDest != MAP_APPFONT
                && eUnitDest != MAP_RELATIVE,
                "Destination MapUnit nicht erlaubt" );
    DBG_ASSERTWARNING( eUnitSource != MAP_PIXEL,
                       "MAP_PIXEL mit 72dpi angenaehert" );
    DBG_ASSERTWARNING( eUnitDest != MAP_PIXEL,
                       "MAP_PIXEL mit 72dpi angenaehert" );
}

#define ENTER3( eUnitSource, eUnitDest )                                \
    long nNumerator      = 1;       \
    long nDenominator    = 1;       \
    DBG_ASSERT( eUnitSource < s_ImplArySize, "Invalid source map unit");    \
    DBG_ASSERT( eUnitDest < s_ImplArySize, "Invalid destination map unit"); \
    if( (eUnitSource < s_ImplArySize) && (eUnitDest < s_ImplArySize) )  \
    {   \
        nNumerator   = aImplNumeratorAry[eUnitSource] *             \
                           aImplDenominatorAry[eUnitDest];              \
        nDenominator     = aImplNumeratorAry[eUnitDest] *               \
                           aImplDenominatorAry[eUnitSource];            \
    } \
    if ( eUnitSource == MAP_PIXEL )                                     \
        nDenominator *= 72;                                             \
    else if( eUnitDest == MAP_PIXEL )                                   \
        nNumerator *= 72

#define ENTER4( rMapModeSource, rMapModeDest )                          \
    ImplMapRes aMapResSource;                                           \
    aMapResSource.mnMapOfsX          = 0;                               \
    aMapResSource.mnMapOfsY          = 0;                               \
    aMapResSource.mnMapScNumX        = 1;                               \
    aMapResSource.mnMapScNumY        = 1;                               \
    aMapResSource.mnMapScDenomX      = 1;                               \
    aMapResSource.mnMapScDenomY      = 1;                               \
    aMapResSource.mfOffsetX          = 0.0;                             \
    aMapResSource.mfOffsetY          = 0.0;                             \
    aMapResSource.mfScaleX           = 1.0;                             \
    aMapResSource.mfScaleY           = 1.0;                             \
    ImplMapRes aMapResDest(aMapResSource);                              \
                                                                        \
    ImplCalcMapResolution( rMapModeSource, 72, 72, aMapResSource );     \
    ImplCalcMapResolution( rMapModeDest, 72, 72, aMapResDest )

// return (n1 * n2 * n3) / (n4 * n5)
static long fn5( const long n1,
                 const long n2,
                 const long n3,
                 const long n4,
                 const long n5 )
{
    SensorCall();if ( n1 == 0 || n2 == 0 || n3 == 0 || n4 == 0 || n5 == 0 )
        {/*171*/{long  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*172*/}
    SensorCall();if ( LONG_MAX / std::abs(n2) < std::abs(n3) )
    {
        // a6 is skipped
        SensorCall();BigInt a7 = n2;
        a7 *= n3;
        a7 *= n1;

        SensorCall();if ( LONG_MAX / std::abs(n4) < std::abs(n5) )
        {
            SensorCall();BigInt a8 = n4;
            a8 *= n5;

            BigInt a9 = a8;
            a9 /= 2;
            SensorCall();if ( a7.IsNeg() )
                {/*173*/SensorCall();a7 -= a9;/*174*/}
            else
                {/*175*/SensorCall();a7 += a9;/*176*/}

            SensorCall();a7 /= a8;
        } // of if
        else
        {
            SensorCall();long n8 = n4 * n5;

            SensorCall();if ( a7.IsNeg() )
                {/*177*/SensorCall();a7 -= n8 / 2;/*178*/}
            else
                {/*179*/SensorCall();a7 += n8 / 2;/*180*/}

            SensorCall();a7 /= n8;
        } // of else
        {long  ReplaceReturn = (long)a7; SensorCall(); return ReplaceReturn;}
    } // of if
    else
    {
        SensorCall();long n6 = n2 * n3;

        SensorCall();if ( LONG_MAX / std::abs(n1) < std::abs(n6) )
        {
            SensorCall();BigInt a7 = n1;
            a7 *= n6;

            SensorCall();if ( LONG_MAX / std::abs(n4) < std::abs(n5) )
            {
                SensorCall();BigInt a8 = n4;
                a8 *= n5;

                BigInt a9 = a8;
                a9 /= 2;
                SensorCall();if ( a7.IsNeg() )
                    {/*181*/SensorCall();a7 -= a9;/*182*/}
                else
                    {/*183*/SensorCall();a7 += a9;/*184*/}

                SensorCall();a7 /= a8;
            } // of if
            else
            {
                SensorCall();long n8 = n4 * n5;

                SensorCall();if ( a7.IsNeg() )
                    {/*185*/SensorCall();a7 -= n8 / 2;/*186*/}
                else
                    {/*187*/SensorCall();a7 += n8 / 2;/*188*/}

                SensorCall();a7 /= n8;
            } // of else
            {long  ReplaceReturn = (long)a7; SensorCall(); return ReplaceReturn;}
        } // of if
        else
        {
            SensorCall();long n7 = n1 * n6;

            SensorCall();if ( LONG_MAX / std::abs(n4) < std::abs(n5) )
            {
                SensorCall();BigInt a7 = n7;
                BigInt a8 = n4;
                a8 *= n5;

                BigInt a9 = a8;
                a9 /= 2;
                SensorCall();if ( a7.IsNeg() )
                    {/*189*/SensorCall();a7 -= a9;/*190*/}
                else
                    {/*191*/SensorCall();a7 += a9;/*192*/}

                SensorCall();a7 /= a8;
                {long  ReplaceReturn = (long)a7; SensorCall(); return ReplaceReturn;}
            } // of if
            else
            {
                SensorCall();const long n8 = n4 * n5;
                const long n8_2 = n8 / 2;

                SensorCall();if( n7 < 0 )
                {
                    SensorCall();if( ( n7 - LONG_MIN ) >= n8_2 )
                        {/*193*/SensorCall();n7 -= n8_2;/*194*/}
                }
                else {/*195*/SensorCall();if( ( LONG_MAX - n7 ) >= n8_2 )
                    {/*197*/SensorCall();n7 += n8_2;/*198*/}/*196*/}

                {long  ReplaceReturn = n7 / n8; SensorCall(); return ReplaceReturn;}
            } // of else
        } // of else
    } // of else
SensorCall();}

// return (n1 * n2) / n3
static long fn3( const long n1, const long n2, const long n3 )
{
    SensorCall();if ( n1 == 0 || n2 == 0 || n3 == 0 )
        {/*199*/{long  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*200*/}
    SensorCall();if ( LONG_MAX / std::abs(n1) < std::abs(n2) )
    {
        SensorCall();BigInt a4 = n1;
        a4 *= n2;

        SensorCall();if ( a4.IsNeg() )
            {/*201*/SensorCall();a4 -= n3 / 2;/*202*/}
        else
            {/*203*/SensorCall();a4 += n3 / 2;/*204*/}

        SensorCall();a4 /= n3;
        {long  ReplaceReturn = (long)a4; SensorCall(); return ReplaceReturn;}
    } // of if
    else
    {
        SensorCall();long        n4 = n1 * n2;
        const long  n3_2 = n3 / 2;

        SensorCall();if( n4 < 0 )
        {
            SensorCall();if( ( n4 - LONG_MIN ) >= n3_2 )
                {/*205*/SensorCall();n4 -= n3_2;/*206*/}
        }
        else {/*207*/SensorCall();if( ( LONG_MAX - n4 ) >= n3_2 )
            {/*209*/SensorCall();n4 += n3_2;/*210*/}/*208*/}

        {long  ReplaceReturn = n4 / n3; SensorCall(); return ReplaceReturn;}
    } // of else
SensorCall();}

Point OutputDevice::LogicToLogic( const Point& rPtSource,
                                  const MapMode* pMapModeSource,
                                  const MapMode* pMapModeDest ) const
{
SensorCall();    ENTER1( rPtSource, pMapModeSource, pMapModeDest );

    {Point  ReplaceReturn = Point( fn5( rPtSource.X() + aMapResSource.mnMapOfsX,
                       aMapResSource.mnMapScNumX, aMapResDest.mnMapScDenomX,
                       aMapResSource.mnMapScDenomX, aMapResDest.mnMapScNumX ) -
                  aMapResDest.mnMapOfsX,
                  fn5( rPtSource.Y() + aMapResSource.mnMapOfsY,
                       aMapResSource.mnMapScNumY, aMapResDest.mnMapScDenomY,
                       aMapResSource.mnMapScDenomY, aMapResDest.mnMapScNumY ) -
                  aMapResDest.mnMapOfsY ); SensorCall(); return ReplaceReturn;}
}

Size OutputDevice::LogicToLogic( const Size& rSzSource,
                                 const MapMode* pMapModeSource,
                                 const MapMode* pMapModeDest ) const
{
SensorCall();    ENTER1( rSzSource, pMapModeSource, pMapModeDest );

    {Size  ReplaceReturn = Size( fn5( rSzSource.Width(),
                      aMapResSource.mnMapScNumX, aMapResDest.mnMapScDenomX,
                      aMapResSource.mnMapScDenomX, aMapResDest.mnMapScNumX ),
                 fn5( rSzSource.Height(),
                      aMapResSource.mnMapScNumY, aMapResDest.mnMapScDenomY,
                      aMapResSource.mnMapScDenomY, aMapResDest.mnMapScNumY ) ); SensorCall(); return ReplaceReturn;}
}

Rectangle OutputDevice::LogicToLogic( const Rectangle& rRectSource,
                                      const MapMode* pMapModeSource,
                                      const MapMode* pMapModeDest ) const
{
SensorCall();    ENTER1( rRectSource, pMapModeSource, pMapModeDest );

    {Rectangle  ReplaceReturn = Rectangle( fn5( rRectSource.Left() + aMapResSource.mnMapOfsX,
                           aMapResSource.mnMapScNumX, aMapResDest.mnMapScDenomX,
                           aMapResSource.mnMapScDenomX, aMapResDest.mnMapScNumX ) -
                      aMapResDest.mnMapOfsX,
                      fn5( rRectSource.Top() + aMapResSource.mnMapOfsY,
                           aMapResSource.mnMapScNumY, aMapResDest.mnMapScDenomY,
                           aMapResSource.mnMapScDenomY, aMapResDest.mnMapScNumY ) -
                      aMapResDest.mnMapOfsY,
                      fn5( rRectSource.Right() + aMapResSource.mnMapOfsX,
                           aMapResSource.mnMapScNumX, aMapResDest.mnMapScDenomX,
                           aMapResSource.mnMapScDenomX, aMapResDest.mnMapScNumX ) -
                      aMapResDest.mnMapOfsX,
                      fn5( rRectSource.Bottom() + aMapResSource.mnMapOfsY,
                           aMapResSource.mnMapScNumY, aMapResDest.mnMapScDenomY,
                           aMapResSource.mnMapScDenomY, aMapResDest.mnMapScNumY ) -
                      aMapResDest.mnMapOfsY ); SensorCall(); return ReplaceReturn;}
}

Point OutputDevice::LogicToLogic( const Point& rPtSource,
                                  const MapMode& rMapModeSource,
                                  const MapMode& rMapModeDest )
{
    SensorCall();if ( rMapModeSource == rMapModeDest )
        {/*211*/{Point  ReplaceReturn = rPtSource; SensorCall(); return ReplaceReturn;}/*212*/}

    SensorCall();MapUnit eUnitSource = rMapModeSource.GetMapUnit();
    MapUnit eUnitDest   = rMapModeDest.GetMapUnit();
    verifyUnitSourceDest( eUnitSource, eUnitDest );

    SensorCall();if (rMapModeSource.IsSimple() && rMapModeDest.IsSimple())
    {
        ENTER3( eUnitSource, eUnitDest );

        {Point  ReplaceReturn = Point( fn3( rPtSource.X(), nNumerator, nDenominator ),
                      fn3( rPtSource.Y(), nNumerator, nDenominator ) ); SensorCall(); return ReplaceReturn;}
    }
    else
    {
        ENTER4( rMapModeSource, rMapModeDest );

        {Point  ReplaceReturn = Point( fn5( rPtSource.X() + aMapResSource.mnMapOfsX,
                           aMapResSource.mnMapScNumX, aMapResDest.mnMapScDenomX,
                           aMapResSource.mnMapScDenomX, aMapResDest.mnMapScNumX ) -
                      aMapResDest.mnMapOfsX,
                      fn5( rPtSource.Y() + aMapResSource.mnMapOfsY,
                           aMapResSource.mnMapScNumY, aMapResDest.mnMapScDenomY,
                           aMapResSource.mnMapScDenomY, aMapResDest.mnMapScNumY ) -
                      aMapResDest.mnMapOfsY ); SensorCall(); return ReplaceReturn;}
    }
SensorCall();}

Size OutputDevice::LogicToLogic( const Size& rSzSource,
                                 const MapMode& rMapModeSource,
                                 const MapMode& rMapModeDest )
{
    SensorCall();if ( rMapModeSource == rMapModeDest )
        {/*213*/{Size  ReplaceReturn = rSzSource; SensorCall(); return ReplaceReturn;}/*214*/}

    SensorCall();MapUnit eUnitSource = rMapModeSource.GetMapUnit();
    MapUnit eUnitDest   = rMapModeDest.GetMapUnit();
    verifyUnitSourceDest( eUnitSource, eUnitDest );

    SensorCall();if (rMapModeSource.IsSimple() && rMapModeDest.IsSimple())
    {
        ENTER3( eUnitSource, eUnitDest );

        {Size  ReplaceReturn = Size( fn3( rSzSource.Width(),  nNumerator, nDenominator ),
                     fn3( rSzSource.Height(), nNumerator, nDenominator ) ); SensorCall(); return ReplaceReturn;}
    }
    else
    {
        ENTER4( rMapModeSource, rMapModeDest );

        {Size  ReplaceReturn = Size( fn5( rSzSource.Width(),
                          aMapResSource.mnMapScNumX, aMapResDest.mnMapScDenomX,
                          aMapResSource.mnMapScDenomX, aMapResDest.mnMapScNumX ),
                     fn5( rSzSource.Height(),
                          aMapResSource.mnMapScNumY, aMapResDest.mnMapScDenomY,
                          aMapResSource.mnMapScDenomY, aMapResDest.mnMapScNumY ) ); SensorCall(); return ReplaceReturn;}
    }
SensorCall();}

basegfx::B2DPolygon OutputDevice::LogicToLogic( const basegfx::B2DPolygon& rPolySource,
                                                const MapMode& rMapModeSource,
                                                const MapMode& rMapModeDest )
{
    SensorCall();if(rMapModeSource == rMapModeDest)
    {
        {basegfx::B2DPolygon  ReplaceReturn = rPolySource; SensorCall(); return ReplaceReturn;}
    }

    SensorCall();const basegfx::B2DHomMatrix aTransform(LogicToLogic(rMapModeSource, rMapModeDest));
    basegfx::B2DPolygon aPoly(rPolySource);

    aPoly.transform(aTransform);
    {basegfx::B2DPolygon  ReplaceReturn = aPoly; SensorCall(); return ReplaceReturn;}
}

basegfx::B2DPolyPolygon OutputDevice::LogicToLogic( const basegfx::B2DPolyPolygon& rPolySource,
                                                    const MapMode& rMapModeSource,
                                                    const MapMode& rMapModeDest )
{
    SensorCall();if(rMapModeSource == rMapModeDest)
    {
        {basegfx::B2DPolyPolygon  ReplaceReturn = rPolySource; SensorCall(); return ReplaceReturn;}
    }

    SensorCall();const basegfx::B2DHomMatrix aTransform(LogicToLogic(rMapModeSource, rMapModeDest));
    basegfx::B2DPolyPolygon aPoly(rPolySource);

    aPoly.transform(aTransform);
    {basegfx::B2DPolyPolygon  ReplaceReturn = aPoly; SensorCall(); return ReplaceReturn;}
}

basegfx::B2DHomMatrix OutputDevice::LogicToLogic(const MapMode& rMapModeSource, const MapMode& rMapModeDest)
{
    SensorCall();basegfx::B2DHomMatrix aTransform;

    SensorCall();if(rMapModeSource == rMapModeDest)
    {
        {basegfx::B2DHomMatrix  ReplaceReturn = aTransform; SensorCall(); return ReplaceReturn;}
    }

    SensorCall();MapUnit eUnitSource = rMapModeSource.GetMapUnit();
    MapUnit eUnitDest   = rMapModeDest.GetMapUnit();
    verifyUnitSourceDest(eUnitSource, eUnitDest);

    SensorCall();if (rMapModeSource.IsSimple() && rMapModeDest.IsSimple())
    {
        ENTER3(eUnitSource, eUnitDest);

        SensorCall();const double fScaleFactor((double)nNumerator / (double)nDenominator);
        aTransform.set(0, 0, fScaleFactor);
        aTransform.set(1, 1, fScaleFactor);
    }
    else
    {
        ENTER4(rMapModeSource, rMapModeDest);

        const double fScaleFactorX((double(aMapResSource.mnMapScNumX) * double(aMapResDest.mnMapScDenomX)) / (double(aMapResSource.mnMapScDenomX) * double(aMapResDest.mnMapScNumX)));
        const double fScaleFactorY((double(aMapResSource.mnMapScNumY) * double(aMapResDest.mnMapScDenomY)) / (double(aMapResSource.mnMapScDenomY) * double(aMapResDest.mnMapScNumY)));
        const double fZeroPointX(double(aMapResSource.mnMapOfsX) * fScaleFactorX - double(aMapResDest.mnMapOfsX));
        const double fZeroPointY(double(aMapResSource.mnMapOfsY) * fScaleFactorY - double(aMapResDest.mnMapOfsY));

        aTransform.set(0, 0, fScaleFactorX);
        aTransform.set(1, 1, fScaleFactorY);
        aTransform.set(0, 2, fZeroPointX);
        aTransform.set(1, 2, fZeroPointY);
    }

    {basegfx::B2DHomMatrix  ReplaceReturn = aTransform; SensorCall(); return ReplaceReturn;}
}

Rectangle OutputDevice::LogicToLogic( const Rectangle& rRectSource,
                                      const MapMode& rMapModeSource,
                                      const MapMode& rMapModeDest )
{
    SensorCall();if ( rMapModeSource == rMapModeDest )
        {/*215*/{Rectangle  ReplaceReturn = rRectSource; SensorCall(); return ReplaceReturn;}/*216*/}

    SensorCall();MapUnit eUnitSource = rMapModeSource.GetMapUnit();
    MapUnit eUnitDest   = rMapModeDest.GetMapUnit();
    verifyUnitSourceDest( eUnitSource, eUnitDest );

    SensorCall();if (rMapModeSource.IsSimple() && rMapModeDest.IsSimple())
    {
        ENTER3( eUnitSource, eUnitDest );

        {Rectangle  ReplaceReturn = Rectangle( fn3( rRectSource.Left(), nNumerator, nDenominator ),
                          fn3( rRectSource.Top(), nNumerator, nDenominator ),
                          fn3( rRectSource.Right(), nNumerator, nDenominator ),
                          fn3( rRectSource.Bottom(), nNumerator, nDenominator ) ); SensorCall(); return ReplaceReturn;}
    }
    else
    {
        ENTER4( rMapModeSource, rMapModeDest );

        {Rectangle  ReplaceReturn = Rectangle( fn5( rRectSource.Left() + aMapResSource.mnMapOfsX,
                               aMapResSource.mnMapScNumX, aMapResDest.mnMapScDenomX,
                               aMapResSource.mnMapScDenomX, aMapResDest.mnMapScNumX ) -
                          aMapResDest.mnMapOfsX,
                          fn5( rRectSource.Top() + aMapResSource.mnMapOfsY,
                               aMapResSource.mnMapScNumY, aMapResDest.mnMapScDenomY,
                               aMapResSource.mnMapScDenomY, aMapResDest.mnMapScNumY ) -
                          aMapResDest.mnMapOfsY,
                          fn5( rRectSource.Right() + aMapResSource.mnMapOfsX,
                               aMapResSource.mnMapScNumX, aMapResDest.mnMapScDenomX,
                               aMapResSource.mnMapScDenomX, aMapResDest.mnMapScNumX ) -
                          aMapResDest.mnMapOfsX,
                          fn5( rRectSource.Bottom() + aMapResSource.mnMapOfsY,
                               aMapResSource.mnMapScNumY, aMapResDest.mnMapScDenomY,
                               aMapResSource.mnMapScDenomY, aMapResDest.mnMapScNumY ) -
                          aMapResDest.mnMapOfsY ); SensorCall(); return ReplaceReturn;}
    }
SensorCall();}

long OutputDevice::LogicToLogic( long nLongSource,
                                 MapUnit eUnitSource, MapUnit eUnitDest )
{
    SensorCall();if ( eUnitSource == eUnitDest )
        {/*217*/{long  ReplaceReturn = nLongSource; SensorCall(); return ReplaceReturn;}/*218*/}

    SensorCall();verifyUnitSourceDest( eUnitSource, eUnitDest );
    ENTER3( eUnitSource, eUnitDest );

    {long  ReplaceReturn = fn3( nLongSource, nNumerator, nDenominator ); SensorCall(); return ReplaceReturn;}
}

void OutputDevice::SetPixelOffset( const Size& rOffset )
{
    SensorCall();mnOutOffOrigX  = rOffset.Width();
    mnOutOffOrigY  = rOffset.Height();

    mnOutOffLogicX = ImplPixelToLogic( mnOutOffOrigX, mnDPIX,
                                       maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                                       maThresRes.mnThresPixToLogX );
    mnOutOffLogicY = ImplPixelToLogic( mnOutOffOrigY, mnDPIY,
                                       maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                                       maThresRes.mnThresPixToLogY );

    if( mpAlphaVDev )
        mpAlphaVDev->SetPixelOffset( rOffset );
SensorCall();}


namespace vcl {

long Window::ImplLogicUnitToPixelX( long nX, MapUnit eUnit )
{
    SensorCall();if ( eUnit != MAP_PIXEL )
    {
        SensorCall();ImplFrameData* pFrameData = mpWindowImpl->mpFrameData;

        // shift map unit, then re-calculate
        SensorCall();if ( pFrameData->meMapUnit != eUnit )
        {
            SensorCall();pFrameData->meMapUnit = eUnit;
            ImplCalcMapResolution( MapMode( eUnit ), mnDPIX, mnDPIY,
                                   pFrameData->maMapUnitRes );
        }

        // BigInt is not required, as this function is only used to
        // convert the window position
        SensorCall();nX  = nX * mnDPIX * pFrameData->maMapUnitRes.mnMapScNumX;
        nX += nX >= 0 ?  (pFrameData->maMapUnitRes.mnMapScDenomX/2) :
                        -((pFrameData->maMapUnitRes.mnMapScDenomX-1)/2);
        nX /= pFrameData->maMapUnitRes.mnMapScDenomX;
    }

    {long  ReplaceReturn = nX; SensorCall(); return ReplaceReturn;}
}

long Window::ImplLogicUnitToPixelY( long nY, MapUnit eUnit )
{
    SensorCall();if ( eUnit != MAP_PIXEL )
    {
        SensorCall();ImplFrameData* pFrameData = mpWindowImpl->mpFrameData;

        // shift map unit, then re-calculate
        SensorCall();if ( pFrameData->meMapUnit != eUnit )
        {
            SensorCall();pFrameData->meMapUnit = eUnit;
            ImplCalcMapResolution( MapMode( eUnit ), mnDPIX, mnDPIY,
                                   pFrameData->maMapUnitRes );
        }

        // BigInt is not required, as this function is only used to
        // convert the window position
        SensorCall();nY  = nY * mnDPIY * pFrameData->maMapUnitRes.mnMapScNumY;
        nY += nY >= 0 ?  (pFrameData->maMapUnitRes.mnMapScDenomY/2) :
                        -((pFrameData->maMapUnitRes.mnMapScDenomY-1)/2);
        nY /= pFrameData->maMapUnitRes.mnMapScDenomY;
    }

    {long  ReplaceReturn = nY; SensorCall(); return ReplaceReturn;}
}

} /* namespace vcl */

DeviceCoordinate OutputDevice::LogicWidthToDeviceCoordinate( long nWidth ) const
{
    SensorCall();if ( !mbMap )
        {/*219*/{DeviceCoordinate  ReplaceReturn = (DeviceCoordinate)nWidth; SensorCall(); return ReplaceReturn;}/*220*/}

#if VCL_FLOAT_DEVICE_PIXEL
    return (double)nWidth * maMapRes.mfScaleX * mnDPIX;
#else

    {DeviceCoordinate  ReplaceReturn = ImplLogicToPixel( nWidth, mnDPIX,
                             maMapRes.mnMapScNumX, maMapRes.mnMapScDenomX,
                             maThresRes.mnThresLogToPixX ); SensorCall(); return ReplaceReturn;}
#endif
}

DeviceCoordinate OutputDevice::LogicHeightToDeviceCoordinate( long nHeight ) const
{
    SensorCall();if ( !mbMap )
        {/*221*/{DeviceCoordinate  ReplaceReturn = (DeviceCoordinate)nHeight; SensorCall(); return ReplaceReturn;}/*222*/}
#if VCL_FLOAT_DEVICE_PIXEL
    return (double)nHeight * maMapRes.mfScaleY * mnDPIY;
#else

    {DeviceCoordinate  ReplaceReturn = ImplLogicToPixel( nHeight, mnDPIY,
                             maMapRes.mnMapScNumY, maMapRes.mnMapScDenomY,
                             maThresRes.mnThresLogToPixY ); SensorCall(); return ReplaceReturn;}
#endif
}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
