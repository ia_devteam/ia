/*
 * Copyright (c) 2000-2002,2005 Silicon Graphics, Inc.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it would be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write the Free Software Foundation,
 * Inc.,  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "libxfs_priv.h"
#include "var/tmp/sensor.h"
#include "xfs_fs.h"
#include "xfs_shared.h"
#include "xfs_format.h"
#include "xfs_log_format.h"
#include "xfs_trans_resv.h"
#include "xfs_bit.h"
#include "xfs_mount.h"
#include "xfs_inode.h"
#include "xfs_trans.h"
#include "xfs_btree.h"
#include "xfs_trace.h"
#include "xfs_cksum.h"
#include "xfs_alloc.h"

/*
 * Cursor allocation zone.
 */
kmem_zone_t	*xfs_btree_cur_zone;

/*
 * Btree magic numbers.
 */
static const __uint32_t xfs_magics[2][XFS_BTNUM_MAX] = {
	{ XFS_ABTB_MAGIC, XFS_ABTC_MAGIC, XFS_BMAP_MAGIC, XFS_IBT_MAGIC,
	  XFS_FIBT_MAGIC },
	{ XFS_ABTB_CRC_MAGIC, XFS_ABTC_CRC_MAGIC,
	  XFS_BMAP_CRC_MAGIC, XFS_IBT_CRC_MAGIC, XFS_FIBT_CRC_MAGIC }
};
#define xfs_btree_magic(cur) \
	xfs_magics[!!((cur)->bc_flags & XFS_BTREE_CRC_BLOCKS)][cur->bc_btnum]


STATIC int				/* error (0 or EFSCORRUPTED) */
xfs_btree_check_lblock(
	struct xfs_btree_cur	*cur,	/* btree cursor */
	struct xfs_btree_block	*block,	/* btree long form block pointer */
	int			level,	/* level of the btree block */
	struct xfs_buf		*bp)	/* buffer for block, if any */
{
	Din_Go(1,2048);int			lblock_ok = 1; /* block passes checks */
	struct xfs_mount	*mp;	/* file system mount point */

	mp = cur->bc_mp;

	Din_Go(3,2048);if (xfs_sb_version_hascrc(&mp->m_sb)) {
		Din_Go(2,2048);lblock_ok = lblock_ok &&
			uuid_equal(&block->bb_u.l.bb_uuid,
				   &mp->m_sb.sb_meta_uuid) &&
			block->bb_u.l.bb_blkno == cpu_to_be64(
				bp ? bp->b_bn : XFS_BUF_DADDR_NULL);
	}

	Din_Go(4,2048);lblock_ok = lblock_ok &&
		be32_to_cpu(block->bb_magic) == xfs_btree_magic(cur) &&
		be16_to_cpu(block->bb_level) == level &&
		be16_to_cpu(block->bb_numrecs) <=
			cur->bc_ops->get_maxrecs(cur, level) &&
		block->bb_u.l.bb_leftsib &&
		(block->bb_u.l.bb_leftsib == cpu_to_be64(NULLFSBLOCK) ||
		 XFS_FSB_SANITY_CHECK(mp,
			be64_to_cpu(block->bb_u.l.bb_leftsib))) &&
		block->bb_u.l.bb_rightsib &&
		(block->bb_u.l.bb_rightsib == cpu_to_be64(NULLFSBLOCK) ||
		 XFS_FSB_SANITY_CHECK(mp,
			be64_to_cpu(block->bb_u.l.bb_rightsib)));

	Din_Go(7,2048);if (unlikely(XFS_TEST_ERROR(!lblock_ok, mp,
			XFS_ERRTAG_BTREE_CHECK_LBLOCK,
			XFS_RANDOM_BTREE_CHECK_LBLOCK))) {
		Din_Go(5,2048);if (bp)
			trace_xfs_btree_corrupt(bp, _RET_IP_);
		XFS_ERROR_REPORT(__func__, XFS_ERRLEVEL_LOW, mp);
		{int  ReplaceReturn = -EFSCORRUPTED; Din_Go(6,2048); return ReplaceReturn;}
	}
	{int  ReplaceReturn = 0; Din_Go(8,2048); return ReplaceReturn;}
}

STATIC int				/* error (0 or EFSCORRUPTED) */
xfs_btree_check_sblock(
	struct xfs_btree_cur	*cur,	/* btree cursor */
	struct xfs_btree_block	*block,	/* btree short form block pointer */
	int			level,	/* level of the btree block */
	struct xfs_buf		*bp)	/* buffer containing block */
{
	Din_Go(9,2048);struct xfs_mount	*mp;	/* file system mount point */
	struct xfs_buf		*agbp;	/* buffer for ag. freespace struct */
	struct xfs_agf		*agf;	/* ag. freespace structure */
	xfs_agblock_t		agflen;	/* native ag. freespace length */
	int			sblock_ok = 1; /* block passes checks */

	mp = cur->bc_mp;
	agbp = cur->bc_private.a.agbp;
	agf = XFS_BUF_TO_AGF(agbp);
	agflen = be32_to_cpu(agf->agf_length);

	Din_Go(11,2048);if (xfs_sb_version_hascrc(&mp->m_sb)) {
		Din_Go(10,2048);sblock_ok = sblock_ok &&
			uuid_equal(&block->bb_u.s.bb_uuid,
				   &mp->m_sb.sb_meta_uuid) &&
			block->bb_u.s.bb_blkno == cpu_to_be64(
				bp ? bp->b_bn : XFS_BUF_DADDR_NULL);
	}

	Din_Go(12,2048);sblock_ok = sblock_ok &&
		be32_to_cpu(block->bb_magic) == xfs_btree_magic(cur) &&
		be16_to_cpu(block->bb_level) == level &&
		be16_to_cpu(block->bb_numrecs) <=
			cur->bc_ops->get_maxrecs(cur, level) &&
		(block->bb_u.s.bb_leftsib == cpu_to_be32(NULLAGBLOCK) ||
		 be32_to_cpu(block->bb_u.s.bb_leftsib) < agflen) &&
		block->bb_u.s.bb_leftsib &&
		(block->bb_u.s.bb_rightsib == cpu_to_be32(NULLAGBLOCK) ||
		 be32_to_cpu(block->bb_u.s.bb_rightsib) < agflen) &&
		block->bb_u.s.bb_rightsib;

	Din_Go(15,2048);if (unlikely(XFS_TEST_ERROR(!sblock_ok, mp,
			XFS_ERRTAG_BTREE_CHECK_SBLOCK,
			XFS_RANDOM_BTREE_CHECK_SBLOCK))) {
		Din_Go(13,2048);if (bp)
			trace_xfs_btree_corrupt(bp, _RET_IP_);
		XFS_ERROR_REPORT(__func__, XFS_ERRLEVEL_LOW, mp);
		{int  ReplaceReturn = -EFSCORRUPTED; Din_Go(14,2048); return ReplaceReturn;}
	}
	{int  ReplaceReturn = 0; Din_Go(16,2048); return ReplaceReturn;}
}

/*
 * Debug routine: check that block header is ok.
 */
int
xfs_btree_check_block(
	struct xfs_btree_cur	*cur,	/* btree cursor */
	struct xfs_btree_block	*block,	/* generic btree block pointer */
	int			level,	/* level of the btree block */
	struct xfs_buf		*bp)	/* buffer containing block, if any */
{
	Din_Go(17,2048);if (cur->bc_flags & XFS_BTREE_LONG_PTRS)
		{/*1*/{int  ReplaceReturn = xfs_btree_check_lblock(cur, block, level, bp); Din_Go(18,2048); return ReplaceReturn;}/*2*/}
	else
		{/*3*/{int  ReplaceReturn = xfs_btree_check_sblock(cur, block, level, bp); Din_Go(19,2048); return ReplaceReturn;}/*4*/}
Din_Go(20,2048);}

/*
 * Check that (long) pointer is ok.
 */
int					/* error (0 or EFSCORRUPTED) */
xfs_btree_check_lptr(
	struct xfs_btree_cur	*cur,	/* btree cursor */
	xfs_fsblock_t		bno,	/* btree block disk address */
	int			level)	/* btree block level */
{
Din_Go(21,2048);	XFS_WANT_CORRUPTED_RETURN(cur->bc_mp,
		level > 0 &&
		bno != NULLFSBLOCK &&
		XFS_FSB_SANITY_CHECK(cur->bc_mp, bno));
	{int  ReplaceReturn = 0; Din_Go(22,2048); return ReplaceReturn;}
}

#ifdef DEBUG
/*
 * Check that (short) pointer is ok.
 */
STATIC int				/* error (0 or EFSCORRUPTED) */
xfs_btree_check_sptr(
	struct xfs_btree_cur	*cur,	/* btree cursor */
	xfs_agblock_t		bno,	/* btree block disk address */
	int			level)	/* btree block level */
{
	xfs_agblock_t		agblocks = cur->bc_mp->m_sb.sb_agblocks;

	XFS_WANT_CORRUPTED_RETURN(cur->bc_mp,
		level > 0 &&
		bno != NULLAGBLOCK &&
		bno != 0 &&
		bno < agblocks);
	return 0;
}

/*
 * Check that block ptr is ok.
 */
STATIC int				/* error (0 or EFSCORRUPTED) */
xfs_btree_check_ptr(
	struct xfs_btree_cur	*cur,	/* btree cursor */
	union xfs_btree_ptr	*ptr,	/* btree block disk address */
	int			index,	/* offset from ptr to check */
	int			level)	/* btree block level */
{
	if (cur->bc_flags & XFS_BTREE_LONG_PTRS) {
		return xfs_btree_check_lptr(cur,
				be64_to_cpu((&ptr->l)[index]), level);
	} else {
		return xfs_btree_check_sptr(cur,
				be32_to_cpu((&ptr->s)[index]), level);
	}
}
#endif

/*
 * Calculate CRC on the whole btree block and stuff it into the
 * long-form btree header.
 *
 * Prior to calculting the CRC, pull the LSN out of the buffer log item and put
 * it into the buffer so recovery knows what the last modification was that made
 * it to disk.
 */
void
xfs_btree_lblock_calc_crc(
	struct xfs_buf		*bp)
{
	Din_Go(23,2048);struct xfs_btree_block	*block = XFS_BUF_TO_BLOCK(bp);
	struct xfs_buf_log_item	*bip = bp->b_fspriv;

	Din_Go(25,2048);if (!xfs_sb_version_hascrc(&bp->b_target->bt_mount->m_sb))
		{/*75*/Din_Go(24,2048);return;/*76*/}
	Din_Go(26,2048);if (bip)
		block->bb_u.l.bb_lsn = cpu_to_be64(bip->bli_item.li_lsn);
	Din_Go(27,2048);xfs_buf_update_cksum(bp, XFS_BTREE_LBLOCK_CRC_OFF);
Din_Go(28,2048);}

bool
xfs_btree_lblock_verify_crc(
	struct xfs_buf		*bp)
{
	Din_Go(29,2048);struct xfs_btree_block	*block = XFS_BUF_TO_BLOCK(bp);
	struct xfs_mount	*mp = bp->b_target->bt_mount;

	Din_Go(32,2048);if (xfs_sb_version_hascrc(&bp->b_target->bt_mount->m_sb)) {
		Din_Go(30,2048);if (!xfs_log_check_lsn(mp, be64_to_cpu(block->bb_u.l.bb_lsn)))
			return false;
		{_Bool  ReplaceReturn = xfs_buf_verify_cksum(bp, XFS_BTREE_LBLOCK_CRC_OFF); Din_Go(31,2048); return ReplaceReturn;}
	}

	{_Bool  ReplaceReturn = true; Din_Go(33,2048); return ReplaceReturn;}
}

/*
 * Calculate CRC on the whole btree block and stuff it into the
 * short-form btree header.
 *
 * Prior to calculting the CRC, pull the LSN out of the buffer log item and put
 * it into the buffer so recovery knows what the last modification was that made
 * it to disk.
 */
void
xfs_btree_sblock_calc_crc(
	struct xfs_buf		*bp)
{
	Din_Go(34,2048);struct xfs_btree_block	*block = XFS_BUF_TO_BLOCK(bp);
	struct xfs_buf_log_item	*bip = bp->b_fspriv;

	Din_Go(36,2048);if (!xfs_sb_version_hascrc(&bp->b_target->bt_mount->m_sb))
		{/*77*/Din_Go(35,2048);return;/*78*/}
	Din_Go(37,2048);if (bip)
		block->bb_u.s.bb_lsn = cpu_to_be64(bip->bli_item.li_lsn);
	Din_Go(38,2048);xfs_buf_update_cksum(bp, XFS_BTREE_SBLOCK_CRC_OFF);
Din_Go(39,2048);}

bool
xfs_btree_sblock_verify_crc(
	struct xfs_buf		*bp)
{
	Din_Go(40,2048);struct xfs_btree_block  *block = XFS_BUF_TO_BLOCK(bp);
	struct xfs_mount	*mp = bp->b_target->bt_mount;

	Din_Go(43,2048);if (xfs_sb_version_hascrc(&bp->b_target->bt_mount->m_sb)) {
		Din_Go(41,2048);if (!xfs_log_check_lsn(mp, be64_to_cpu(block->bb_u.s.bb_lsn)))
			return false;
		{_Bool  ReplaceReturn = xfs_buf_verify_cksum(bp, XFS_BTREE_SBLOCK_CRC_OFF); Din_Go(42,2048); return ReplaceReturn;}
	}

	{_Bool  ReplaceReturn = true; Din_Go(44,2048); return ReplaceReturn;}
}

/*
 * Delete the btree cursor.
 */
void
xfs_btree_del_cursor(
	xfs_btree_cur_t	*cur,		/* btree cursor */
	int		error)		/* del because of error */
{
	Din_Go(45,2048);int		i;		/* btree level */

	/*
	 * Clear the buffer pointers, and release the buffers.
	 * If we're doing this in the face of an error, we
	 * need to make sure to inspect all of the entries
	 * in the bc_bufs array for buffers to be unlocked.
	 * This is because some of the btree code works from
	 * level n down to 0, and if we get an error along
	 * the way we won't have initialized all the entries
	 * down to 0.
	 */
	Din_Go(49,2048);for (i = 0; i < cur->bc_nlevels; i++) {
		Din_Go(46,2048);if (cur->bc_bufs[i])
			xfs_trans_brelse(cur->bc_tp, cur->bc_bufs[i]);
		else {/*5*/Din_Go(47,2048);if (!error)
			{/*7*/Din_Go(48,2048);break;/*8*/}/*6*/}
	}
	/*
	 * Can't free a bmap cursor without having dealt with the
	 * allocated indirect blocks' accounting.
	 */
	ASSERT(cur->bc_btnum != XFS_BTNUM_BMAP ||
	       cur->bc_private.b.allocated == 0);
	/*
	 * Free the cursor.
	 */
	Din_Go(50,2048);kmem_zone_free(xfs_btree_cur_zone, cur);
Din_Go(51,2048);}

/*
 * Duplicate the btree cursor.
 * Allocate a new one, copy the record, re-get the buffers.
 */
int					/* error */
xfs_btree_dup_cursor(
	xfs_btree_cur_t	*cur,		/* input cursor */
	xfs_btree_cur_t	**ncur)		/* output cursor */
{
	Din_Go(52,2048);xfs_buf_t	*bp;		/* btree block's buffer pointer */
	int		error;		/* error return value */
	int		i;		/* level number of btree block */
	xfs_mount_t	*mp;		/* mount structure for filesystem */
	xfs_btree_cur_t	*new;		/* new cursor value */
	xfs_trans_t	*tp;		/* transaction pointer, can be NULL */

	tp = cur->bc_tp;
	mp = cur->bc_mp;

	/*
	 * Allocate a new cursor like the old one.
	 */
	new = cur->bc_ops->dup_cursor(cur);

	/*
	 * Copy the record currently in the cursor.
	 */
	new->bc_rec = cur->bc_rec;

	/*
	 * For each level current, re-get the buffer and copy the ptr value.
	 */
	Din_Go(60,2048);for (i = 0; i < new->bc_nlevels; i++) {
		Din_Go(53,2048);new->bc_ptrs[i] = cur->bc_ptrs[i];
		new->bc_ra[i] = cur->bc_ra[i];
		bp = cur->bc_bufs[i];
		Din_Go(58,2048);if (bp) {
			Din_Go(54,2048);error = xfs_trans_read_buf(mp, tp, mp->m_ddev_targp,
						   XFS_BUF_ADDR(bp), mp->m_bsize,
						   0, &bp,
						   cur->bc_ops->buf_ops);
			Din_Go(57,2048);if (error) {
				Din_Go(55,2048);xfs_btree_del_cursor(new, error);
				*ncur = NULL;
				{int  ReplaceReturn = error; Din_Go(56,2048); return ReplaceReturn;}
			}
		}
		Din_Go(59,2048);new->bc_bufs[i] = bp;
	}
	Din_Go(61,2048);*ncur = new;
	{int  ReplaceReturn = 0; Din_Go(62,2048); return ReplaceReturn;}
}

/*
 * XFS btree block layout and addressing:
 *
 * There are two types of blocks in the btree: leaf and non-leaf blocks.
 *
 * The leaf record start with a header then followed by records containing
 * the values.  A non-leaf block also starts with the same header, and
 * then first contains lookup keys followed by an equal number of pointers
 * to the btree blocks at the previous level.
 *
 *		+--------+-------+-------+-------+-------+-------+-------+
 * Leaf:	| header | rec 1 | rec 2 | rec 3 | rec 4 | rec 5 | rec N |
 *		+--------+-------+-------+-------+-------+-------+-------+
 *
 *		+--------+-------+-------+-------+-------+-------+-------+
 * Non-Leaf:	| header | key 1 | key 2 | key N | ptr 1 | ptr 2 | ptr N |
 *		+--------+-------+-------+-------+-------+-------+-------+
 *
 * The header is called struct xfs_btree_block for reasons better left unknown
 * and comes in different versions for short (32bit) and long (64bit) block
 * pointers.  The record and key structures are defined by the btree instances
 * and opaque to the btree core.  The block pointers are simple disk endian
 * integers, available in a short (32bit) and long (64bit) variant.
 *
 * The helpers below calculate the offset of a given record, key or pointer
 * into a btree block (xfs_btree_*_offset) or return a pointer to the given
 * record, key or pointer (xfs_btree_*_addr).  Note that all addressing
 * inside the btree block is done using indices starting at one, not zero!
 */

/*
 * Return size of the btree block header for this btree instance.
 */
static inline size_t xfs_btree_block_len(struct xfs_btree_cur *cur)
{
	Din_Go(63,2048);if (cur->bc_flags & XFS_BTREE_LONG_PTRS) {
		Din_Go(64,2048);if (cur->bc_flags & XFS_BTREE_CRC_BLOCKS)
			return XFS_BTREE_LBLOCK_CRC_LEN;
		{size_t  ReplaceReturn = XFS_BTREE_LBLOCK_LEN; Din_Go(65,2048); return ReplaceReturn;}
	}
	Din_Go(66,2048);if (cur->bc_flags & XFS_BTREE_CRC_BLOCKS)
		return XFS_BTREE_SBLOCK_CRC_LEN;
	{size_t  ReplaceReturn = XFS_BTREE_SBLOCK_LEN; Din_Go(67,2048); return ReplaceReturn;}
}

/*
 * Return size of btree block pointers for this btree instance.
 */
static inline size_t xfs_btree_ptr_len(struct xfs_btree_cur *cur)
{
	{size_t  ReplaceReturn = (cur->bc_flags & XFS_BTREE_LONG_PTRS) ?
		sizeof(__be64) : sizeof(__be32); Din_Go(68,2048); return ReplaceReturn;}
}

/*
 * Calculate offset of the n-th record in a btree block.
 */
STATIC size_t
xfs_btree_rec_offset(
	struct xfs_btree_cur	*cur,
	int			n)
{
	{size_t  ReplaceReturn = xfs_btree_block_len(cur) +
		(n - 1) * cur->bc_ops->rec_len; Din_Go(69,2048); return ReplaceReturn;}
}

/*
 * Calculate offset of the n-th key in a btree block.
 */
STATIC size_t
xfs_btree_key_offset(
	struct xfs_btree_cur	*cur,
	int			n)
{
	{size_t  ReplaceReturn = xfs_btree_block_len(cur) +
		(n - 1) * cur->bc_ops->key_len; Din_Go(70,2048); return ReplaceReturn;}
}

/*
 * Calculate offset of the n-th block pointer in a btree block.
 */
STATIC size_t
xfs_btree_ptr_offset(
	struct xfs_btree_cur	*cur,
	int			n,
	int			level)
{
	{size_t  ReplaceReturn = xfs_btree_block_len(cur) +
		cur->bc_ops->get_maxrecs(cur, level) * cur->bc_ops->key_len +
		(n - 1) * xfs_btree_ptr_len(cur); Din_Go(71,2048); return ReplaceReturn;}
}

/*
 * Return a pointer to the n-th record in the btree block.
 */
STATIC union xfs_btree_rec *
xfs_btree_rec_addr(
	struct xfs_btree_cur	*cur,
	int			n,
	struct xfs_btree_block	*block)
{
	{__IAUNION__ xfs_btree_rec * ReplaceReturn = (union xfs_btree_rec *)
		((char *)block + xfs_btree_rec_offset(cur, n)); Din_Go(72,2048); return ReplaceReturn;}
}

/*
 * Return a pointer to the n-th key in the btree block.
 */
STATIC union xfs_btree_key *
xfs_btree_key_addr(
	struct xfs_btree_cur	*cur,
	int			n,
	struct xfs_btree_block	*block)
{
	{__IAUNION__ xfs_btree_key * ReplaceReturn = (union xfs_btree_key *)
		((char *)block + xfs_btree_key_offset(cur, n)); Din_Go(73,2048); return ReplaceReturn;}
}

/*
 * Return a pointer to the n-th block pointer in the btree block.
 */
STATIC union xfs_btree_ptr *
xfs_btree_ptr_addr(
	struct xfs_btree_cur	*cur,
	int			n,
	struct xfs_btree_block	*block)
{
	Din_Go(74,2048);int			level = xfs_btree_get_level(block);

	ASSERT(block->bb_level != 0);

	{__IAUNION__ xfs_btree_ptr * ReplaceReturn = (union xfs_btree_ptr *)
		((char *)block + xfs_btree_ptr_offset(cur, n, level)); Din_Go(75,2048); return ReplaceReturn;}
}

/*
 * Get the root block which is stored in the inode.
 *
 * For now this btree implementation assumes the btree root is always
 * stored in the if_broot field of an inode fork.
 */
STATIC struct xfs_btree_block *
xfs_btree_get_iroot(
       struct xfs_btree_cur    *cur)
{
       Din_Go(76,2048);struct xfs_ifork        *ifp;

       ifp = XFS_IFORK_PTR(cur->bc_private.b.ip, cur->bc_private.b.whichfork);
       {__IASTRUCT__ xfs_btree_block * ReplaceReturn = (struct xfs_btree_block *)ifp->if_broot; Din_Go(77,2048); return ReplaceReturn;}
}

/*
 * Retrieve the block pointer from the cursor at the given level.
 * This may be an inode btree root or from a buffer.
 */
STATIC struct xfs_btree_block *		/* generic btree block pointer */
xfs_btree_get_block(
	struct xfs_btree_cur	*cur,	/* btree cursor */
	int			level,	/* level in btree */
	struct xfs_buf		**bpp)	/* buffer containing the block */
{
	Din_Go(78,2048);if ((cur->bc_flags & XFS_BTREE_ROOT_IN_INODE) &&
	    (level == cur->bc_nlevels - 1)) {
		Din_Go(79,2048);*bpp = NULL;
		{__IASTRUCT__ xfs_btree_block * ReplaceReturn = xfs_btree_get_iroot(cur); Din_Go(80,2048); return ReplaceReturn;}
	}

	Din_Go(81,2048);*bpp = cur->bc_bufs[level];
	{__IASTRUCT__ xfs_btree_block * ReplaceReturn = XFS_BUF_TO_BLOCK(*bpp); Din_Go(82,2048); return ReplaceReturn;}
}

/*
 * Get a buffer for the block, return it with no data read.
 * Long-form addressing.
 */
xfs_buf_t *				/* buffer for fsbno */
xfs_btree_get_bufl(
	xfs_mount_t	*mp,		/* file system mount point */
	xfs_trans_t	*tp,		/* transaction pointer */
	xfs_fsblock_t	fsbno,		/* file system block number */
	uint		lock)		/* lock flags for get_buf */
{
	Din_Go(83,2048);xfs_daddr_t		d;		/* real disk block address */

	ASSERT(fsbno != NULLFSBLOCK);
	d = XFS_FSB_TO_DADDR(mp, fsbno);
	{__IASTRUCT__ xfs_buf * ReplaceReturn = xfs_trans_get_buf(tp, mp->m_ddev_targp, d, mp->m_bsize, lock); Din_Go(84,2048); return ReplaceReturn;}
}

/*
 * Get a buffer for the block, return it with no data read.
 * Short-form addressing.
 */
xfs_buf_t *				/* buffer for agno/agbno */
xfs_btree_get_bufs(
	xfs_mount_t	*mp,		/* file system mount point */
	xfs_trans_t	*tp,		/* transaction pointer */
	xfs_agnumber_t	agno,		/* allocation group number */
	xfs_agblock_t	agbno,		/* allocation group block number */
	uint		lock)		/* lock flags for get_buf */
{
	Din_Go(85,2048);xfs_daddr_t		d;		/* real disk block address */

	ASSERT(agno != NULLAGNUMBER);
	ASSERT(agbno != NULLAGBLOCK);
	d = XFS_AGB_TO_DADDR(mp, agno, agbno);
	{__IASTRUCT__ xfs_buf * ReplaceReturn = xfs_trans_get_buf(tp, mp->m_ddev_targp, d, mp->m_bsize, lock); Din_Go(86,2048); return ReplaceReturn;}
}

/*
 * Check for the cursor referring to the last block at the given level.
 */
int					/* 1=is last block, 0=not last block */
xfs_btree_islastblock(
	xfs_btree_cur_t		*cur,	/* btree cursor */
	int			level)	/* level to check */
{
	Din_Go(87,2048);struct xfs_btree_block	*block;	/* generic btree block pointer */
	xfs_buf_t		*bp;	/* buffer containing block */

	block = xfs_btree_get_block(cur, level, &bp);
	xfs_btree_check_block(cur, block, level, bp);
	Din_Go(88,2048);if (cur->bc_flags & XFS_BTREE_LONG_PTRS)
		return block->bb_u.l.bb_rightsib == cpu_to_be64(NULLFSBLOCK);
	else
		return block->bb_u.s.bb_rightsib == cpu_to_be32(NULLAGBLOCK);
Din_Go(89,2048);}

/*
 * Change the cursor to point to the first record at the given level.
 * Other levels are unaffected.
 */
STATIC int				/* success=1, failure=0 */
xfs_btree_firstrec(
	xfs_btree_cur_t		*cur,	/* btree cursor */
	int			level)	/* level to change */
{
	Din_Go(90,2048);struct xfs_btree_block	*block;	/* generic btree block pointer */
	xfs_buf_t		*bp;	/* buffer containing block */

	/*
	 * Get the block pointer for this level.
	 */
	block = xfs_btree_get_block(cur, level, &bp);
	xfs_btree_check_block(cur, block, level, bp);
	/*
	 * It's empty, there is no such record.
	 */
	Din_Go(92,2048);if (!block->bb_numrecs)
		{/*79*/{int  ReplaceReturn = 0; Din_Go(91,2048); return ReplaceReturn;}/*80*/}
	/*
	 * Set the ptr value to 1, that's the first record/key.
	 */
	Din_Go(93,2048);cur->bc_ptrs[level] = 1;
	{int  ReplaceReturn = 1; Din_Go(94,2048); return ReplaceReturn;}
}

/*
 * Change the cursor to point to the last record in the current block
 * at the given level.  Other levels are unaffected.
 */
STATIC int				/* success=1, failure=0 */
xfs_btree_lastrec(
	xfs_btree_cur_t		*cur,	/* btree cursor */
	int			level)	/* level to change */
{
	Din_Go(95,2048);struct xfs_btree_block	*block;	/* generic btree block pointer */
	xfs_buf_t		*bp;	/* buffer containing block */

	/*
	 * Get the block pointer for this level.
	 */
	block = xfs_btree_get_block(cur, level, &bp);
	xfs_btree_check_block(cur, block, level, bp);
	/*
	 * It's empty, there is no such record.
	 */
	Din_Go(97,2048);if (!block->bb_numrecs)
		{/*81*/{int  ReplaceReturn = 0; Din_Go(96,2048); return ReplaceReturn;}/*82*/}
	/*
	 * Set the ptr value to numrecs, that's the last record/key.
	 */
	Din_Go(98,2048);cur->bc_ptrs[level] = be16_to_cpu(block->bb_numrecs);
	{int  ReplaceReturn = 1; Din_Go(99,2048); return ReplaceReturn;}
}

/*
 * Compute first and last byte offsets for the fields given.
 * Interprets the offsets table, which contains struct field offsets.
 */
void
xfs_btree_offsets(
	__int64_t	fields,		/* bitmask of fields */
	const short	*offsets,	/* table of field offsets */
	int		nbits,		/* number of bits to inspect */
	int		*first,		/* output: first byte offset */
	int		*last)		/* output: last byte offset */
{
	Din_Go(100,2048);int		i;		/* current bit number */
	__int64_t	imask;		/* mask for current bit number */

	ASSERT(fields != 0);
	/*
	 * Find the lowest bit, so the first byte offset.
	 */
	Din_Go(104,2048);for (i = 0, imask = 1LL; ; i++, imask <<= 1) {
		Din_Go(101,2048);if (imask & fields) {
			Din_Go(102,2048);*first = offsets[i];
			Din_Go(103,2048);break;
		}
	}
	/*
	 * Find the highest bit, so the last byte offset.
	 */
	Din_Go(108,2048);for (i = nbits - 1, imask = 1LL << i; ; i--, imask >>= 1) {
		Din_Go(105,2048);if (imask & fields) {
			Din_Go(106,2048);*last = offsets[i + 1] - 1;
			Din_Go(107,2048);break;
		}
	}
Din_Go(109,2048);}

/*
 * Get a buffer for the block, return it read in.
 * Long-form addressing.
 */
int
xfs_btree_read_bufl(
	struct xfs_mount	*mp,		/* file system mount point */
	struct xfs_trans	*tp,		/* transaction pointer */
	xfs_fsblock_t		fsbno,		/* file system block number */
	uint			lock,		/* lock flags for read_buf */
	struct xfs_buf		**bpp,		/* buffer for fsbno */
	int			refval,		/* ref count value for buffer */
	const struct xfs_buf_ops *ops)
{
	Din_Go(110,2048);struct xfs_buf		*bp;		/* return value */
	xfs_daddr_t		d;		/* real disk block address */
	int			error;

	ASSERT(fsbno != NULLFSBLOCK);
	d = XFS_FSB_TO_DADDR(mp, fsbno);
	error = xfs_trans_read_buf(mp, tp, mp->m_ddev_targp, d,
				   mp->m_bsize, lock, &bp, ops);
	Din_Go(112,2048);if (error)
		{/*9*/{int  ReplaceReturn = error; Din_Go(111,2048); return ReplaceReturn;}/*10*/}
	Din_Go(113,2048);if (bp)
		xfs_buf_set_ref(bp, refval);
	Din_Go(114,2048);*bpp = bp;
	{int  ReplaceReturn = 0; Din_Go(115,2048); return ReplaceReturn;}
}

/*
 * Read-ahead the block, don't wait for it, don't return a buffer.
 * Long-form addressing.
 */
/* ARGSUSED */
void
xfs_btree_reada_bufl(
	struct xfs_mount	*mp,		/* file system mount point */
	xfs_fsblock_t		fsbno,		/* file system block number */
	xfs_extlen_t		count,		/* count of filesystem blocks */
	const struct xfs_buf_ops *ops)
{
	Din_Go(116,2048);xfs_daddr_t		d;

	ASSERT(fsbno != NULLFSBLOCK);
	d = XFS_FSB_TO_DADDR(mp, fsbno);
	xfs_buf_readahead(mp->m_ddev_targp, d, mp->m_bsize * count, ops);
}

/*
 * Read-ahead the block, don't wait for it, don't return a buffer.
 * Short-form addressing.
 */
/* ARGSUSED */
void
xfs_btree_reada_bufs(
	struct xfs_mount	*mp,		/* file system mount point */
	xfs_agnumber_t		agno,		/* allocation group number */
	xfs_agblock_t		agbno,		/* allocation group block number */
	xfs_extlen_t		count,		/* count of filesystem blocks */
	const struct xfs_buf_ops *ops)
{
	Din_Go(117,2048);xfs_daddr_t		d;

	ASSERT(agno != NULLAGNUMBER);
	ASSERT(agbno != NULLAGBLOCK);
	d = XFS_AGB_TO_DADDR(mp, agno, agbno);
	xfs_buf_readahead(mp->m_ddev_targp, d, mp->m_bsize * count, ops);
}

STATIC int
xfs_btree_readahead_lblock(
	struct xfs_btree_cur	*cur,
	int			lr,
	struct xfs_btree_block	*block)
{
	Din_Go(118,2048);int			rval = 0;
	xfs_fsblock_t		left = be64_to_cpu(block->bb_u.l.bb_leftsib);
	xfs_fsblock_t		right = be64_to_cpu(block->bb_u.l.bb_rightsib);

	Din_Go(120,2048);if ((lr & XFS_BTCUR_LEFTRA) && left != NULLFSBLOCK) {
		Din_Go(119,2048);xfs_btree_reada_bufl(cur->bc_mp, left, 1,
				     cur->bc_ops->buf_ops);
		rval++;
	}

	Din_Go(122,2048);if ((lr & XFS_BTCUR_RIGHTRA) && right != NULLFSBLOCK) {
		Din_Go(121,2048);xfs_btree_reada_bufl(cur->bc_mp, right, 1,
				     cur->bc_ops->buf_ops);
		rval++;
	}

	{int  ReplaceReturn = rval; Din_Go(123,2048); return ReplaceReturn;}
}

STATIC int
xfs_btree_readahead_sblock(
	struct xfs_btree_cur	*cur,
	int			lr,
	struct xfs_btree_block *block)
{
	Din_Go(124,2048);int			rval = 0;
	xfs_agblock_t		left = be32_to_cpu(block->bb_u.s.bb_leftsib);
	xfs_agblock_t		right = be32_to_cpu(block->bb_u.s.bb_rightsib);


	Din_Go(126,2048);if ((lr & XFS_BTCUR_LEFTRA) && left != NULLAGBLOCK) {
		Din_Go(125,2048);xfs_btree_reada_bufs(cur->bc_mp, cur->bc_private.a.agno,
				     left, 1, cur->bc_ops->buf_ops);
		rval++;
	}

	Din_Go(128,2048);if ((lr & XFS_BTCUR_RIGHTRA) && right != NULLAGBLOCK) {
		Din_Go(127,2048);xfs_btree_reada_bufs(cur->bc_mp, cur->bc_private.a.agno,
				     right, 1, cur->bc_ops->buf_ops);
		rval++;
	}

	{int  ReplaceReturn = rval; Din_Go(129,2048); return ReplaceReturn;}
}

/*
 * Read-ahead btree blocks, at the given level.
 * Bits in lr are set from XFS_BTCUR_{LEFT,RIGHT}RA.
 */
STATIC int
xfs_btree_readahead(
	struct xfs_btree_cur	*cur,		/* btree cursor */
	int			lev,		/* level in btree */
	int			lr)		/* left/right bits */
{
	Din_Go(130,2048);struct xfs_btree_block	*block;

	/*
	 * No readahead needed if we are at the root level and the
	 * btree root is stored in the inode.
	 */
	Din_Go(132,2048);if ((cur->bc_flags & XFS_BTREE_ROOT_IN_INODE) &&
	    (lev == cur->bc_nlevels - 1))
		{/*83*/{int  ReplaceReturn = 0; Din_Go(131,2048); return ReplaceReturn;}/*84*/}

	Din_Go(134,2048);if ((cur->bc_ra[lev] | lr) == cur->bc_ra[lev])
		{/*85*/{int  ReplaceReturn = 0; Din_Go(133,2048); return ReplaceReturn;}/*86*/}

	Din_Go(135,2048);cur->bc_ra[lev] |= lr;
	block = XFS_BUF_TO_BLOCK(cur->bc_bufs[lev]);

	Din_Go(137,2048);if (cur->bc_flags & XFS_BTREE_LONG_PTRS)
		{/*87*/{int  ReplaceReturn = xfs_btree_readahead_lblock(cur, lr, block); Din_Go(136,2048); return ReplaceReturn;}/*88*/}
	{int  ReplaceReturn = xfs_btree_readahead_sblock(cur, lr, block); Din_Go(138,2048); return ReplaceReturn;}
}

STATIC xfs_daddr_t
xfs_btree_ptr_to_daddr(
	struct xfs_btree_cur	*cur,
	union xfs_btree_ptr	*ptr)
{
	Din_Go(139,2048);if (cur->bc_flags & XFS_BTREE_LONG_PTRS) {
		ASSERT(ptr->l != cpu_to_be64(NULLFSBLOCK));

		{xfs_daddr_t  ReplaceReturn = XFS_FSB_TO_DADDR(cur->bc_mp, be64_to_cpu(ptr->l)); Din_Go(140,2048); return ReplaceReturn;}
	} else {
		ASSERT(cur->bc_private.a.agno != NULLAGNUMBER);
		ASSERT(ptr->s != cpu_to_be32(NULLAGBLOCK));

		{xfs_daddr_t  ReplaceReturn = XFS_AGB_TO_DADDR(cur->bc_mp, cur->bc_private.a.agno,
					be32_to_cpu(ptr->s)); Din_Go(141,2048); return ReplaceReturn;}
	}
Din_Go(142,2048);}

/*
 * Readahead @count btree blocks at the given @ptr location.
 *
 * We don't need to care about long or short form btrees here as we have a
 * method of converting the ptr directly to a daddr available to us.
 */
STATIC void
xfs_btree_readahead_ptr(
	struct xfs_btree_cur	*cur,
	union xfs_btree_ptr	*ptr,
	xfs_extlen_t		count)
{
Din_Go(143,2048);	xfs_buf_readahead(cur->bc_mp->m_ddev_targp,
			  xfs_btree_ptr_to_daddr(cur, ptr),
			  cur->bc_mp->m_bsize * count, cur->bc_ops->buf_ops);
}

/*
 * Set the buffer for level "lev" in the cursor to bp, releasing
 * any previous buffer.
 */
STATIC void
xfs_btree_setbuf(
	xfs_btree_cur_t		*cur,	/* btree cursor */
	int			lev,	/* level in btree */
	xfs_buf_t		*bp)	/* new buffer to set */
{
	Din_Go(144,2048);struct xfs_btree_block	*b;	/* btree block */

	Din_Go(145,2048);if (cur->bc_bufs[lev])
		xfs_trans_brelse(cur->bc_tp, cur->bc_bufs[lev]);
	Din_Go(146,2048);cur->bc_bufs[lev] = bp;
	cur->bc_ra[lev] = 0;

	b = XFS_BUF_TO_BLOCK(bp);
	Din_Go(151,2048);if (cur->bc_flags & XFS_BTREE_LONG_PTRS) {
		Din_Go(147,2048);if (b->bb_u.l.bb_leftsib == cpu_to_be64(NULLFSBLOCK))
			cur->bc_ra[lev] |= XFS_BTCUR_LEFTRA;
		Din_Go(148,2048);if (b->bb_u.l.bb_rightsib == cpu_to_be64(NULLFSBLOCK))
			cur->bc_ra[lev] |= XFS_BTCUR_RIGHTRA;
	} else {
		Din_Go(149,2048);if (b->bb_u.s.bb_leftsib == cpu_to_be32(NULLAGBLOCK))
			cur->bc_ra[lev] |= XFS_BTCUR_LEFTRA;
		Din_Go(150,2048);if (b->bb_u.s.bb_rightsib == cpu_to_be32(NULLAGBLOCK))
			cur->bc_ra[lev] |= XFS_BTCUR_RIGHTRA;
	}
Din_Go(152,2048);}

STATIC int
xfs_btree_ptr_is_null(
	struct xfs_btree_cur	*cur,
	union xfs_btree_ptr	*ptr)
{
	Din_Go(153,2048);if (cur->bc_flags & XFS_BTREE_LONG_PTRS)
		return ptr->l == cpu_to_be64(NULLFSBLOCK);
	else
		return ptr->s == cpu_to_be32(NULLAGBLOCK);
Din_Go(154,2048);}

STATIC void
xfs_btree_set_ptr_null(
	struct xfs_btree_cur	*cur,
	union xfs_btree_ptr	*ptr)
{
	Din_Go(155,2048);if (cur->bc_flags & XFS_BTREE_LONG_PTRS)
		ptr->l = cpu_to_be64(NULLFSBLOCK);
	else
		ptr->s = cpu_to_be32(NULLAGBLOCK);
Din_Go(156,2048);}

/*
 * Get/set/init sibling pointers
 */
STATIC void
xfs_btree_get_sibling(
	struct xfs_btree_cur	*cur,
	struct xfs_btree_block	*block,
	union xfs_btree_ptr	*ptr,
	int			lr)
{
Din_Go(157,2048);	ASSERT(lr == XFS_BB_LEFTSIB || lr == XFS_BB_RIGHTSIB);

	Din_Go(164,2048);if (cur->bc_flags & XFS_BTREE_LONG_PTRS) {
		Din_Go(158,2048);if (lr == XFS_BB_RIGHTSIB)
			{/*89*/Din_Go(159,2048);ptr->l = block->bb_u.l.bb_rightsib;/*90*/}
		else
			{/*91*/Din_Go(160,2048);ptr->l = block->bb_u.l.bb_leftsib;/*92*/}
	} else {
		Din_Go(161,2048);if (lr == XFS_BB_RIGHTSIB)
			{/*93*/Din_Go(162,2048);ptr->s = block->bb_u.s.bb_rightsib;/*94*/}
		else
			{/*95*/Din_Go(163,2048);ptr->s = block->bb_u.s.bb_leftsib;/*96*/}
	}
Din_Go(165,2048);}

STATIC void
xfs_btree_set_sibling(
	struct xfs_btree_cur	*cur,
	struct xfs_btree_block	*block,
	union xfs_btree_ptr	*ptr,
	int			lr)
{
Din_Go(166,2048);	ASSERT(lr == XFS_BB_LEFTSIB || lr == XFS_BB_RIGHTSIB);

	Din_Go(173,2048);if (cur->bc_flags & XFS_BTREE_LONG_PTRS) {
		Din_Go(167,2048);if (lr == XFS_BB_RIGHTSIB)
			{/*97*/Din_Go(168,2048);block->bb_u.l.bb_rightsib = ptr->l;/*98*/}
		else
			{/*99*/Din_Go(169,2048);block->bb_u.l.bb_leftsib = ptr->l;/*100*/}
	} else {
		Din_Go(170,2048);if (lr == XFS_BB_RIGHTSIB)
			{/*101*/Din_Go(171,2048);block->bb_u.s.bb_rightsib = ptr->s;/*102*/}
		else
			{/*103*/Din_Go(172,2048);block->bb_u.s.bb_leftsib = ptr->s;/*104*/}
	}
Din_Go(174,2048);}

void
xfs_btree_init_block_int(
	struct xfs_mount	*mp,
	struct xfs_btree_block	*buf,
	xfs_daddr_t		blkno,
	__u32			magic,
	__u16			level,
	__u16			numrecs,
	__u64			owner,
	unsigned int		flags)
{
	Din_Go(175,2048);buf->bb_magic = cpu_to_be32(magic);
	buf->bb_level = cpu_to_be16(level);
	buf->bb_numrecs = cpu_to_be16(numrecs);

	Din_Go(182,2048);if (flags & XFS_BTREE_LONG_PTRS) {
		Din_Go(176,2048);buf->bb_u.l.bb_leftsib = cpu_to_be64(NULLFSBLOCK);
		buf->bb_u.l.bb_rightsib = cpu_to_be64(NULLFSBLOCK);
		Din_Go(178,2048);if (flags & XFS_BTREE_CRC_BLOCKS) {
			Din_Go(177,2048);buf->bb_u.l.bb_blkno = cpu_to_be64(blkno);
			buf->bb_u.l.bb_owner = cpu_to_be64(owner);
			uuid_copy(&buf->bb_u.l.bb_uuid, &mp->m_sb.sb_meta_uuid);
			buf->bb_u.l.bb_pad = 0;
			buf->bb_u.l.bb_lsn = 0;
		}
	} else {
		/* owner is a 32 bit value on short blocks */
		Din_Go(179,2048);__u32 __owner = (__u32)owner;

		buf->bb_u.s.bb_leftsib = cpu_to_be32(NULLAGBLOCK);
		buf->bb_u.s.bb_rightsib = cpu_to_be32(NULLAGBLOCK);
		Din_Go(181,2048);if (flags & XFS_BTREE_CRC_BLOCKS) {
			Din_Go(180,2048);buf->bb_u.s.bb_blkno = cpu_to_be64(blkno);
			buf->bb_u.s.bb_owner = cpu_to_be32(__owner);
			uuid_copy(&buf->bb_u.s.bb_uuid, &mp->m_sb.sb_meta_uuid);
			buf->bb_u.s.bb_lsn = 0;
		}
	}
Din_Go(183,2048);}

void
xfs_btree_init_block(
	struct xfs_mount *mp,
	struct xfs_buf	*bp,
	__u32		magic,
	__u16		level,
	__u16		numrecs,
	__u64		owner,
	unsigned int	flags)
{
	Din_Go(184,2048);xfs_btree_init_block_int(mp, XFS_BUF_TO_BLOCK(bp), bp->b_bn,
				 magic, level, numrecs, owner, flags);
Din_Go(185,2048);}

STATIC void
xfs_btree_init_block_cur(
	struct xfs_btree_cur	*cur,
	struct xfs_buf		*bp,
	int			level,
	int			numrecs)
{
	Din_Go(186,2048);__u64 owner;

	/*
	 * we can pull the owner from the cursor right now as the different
	 * owners align directly with the pointer size of the btree. This may
	 * change in future, but is safe for current users of the generic btree
	 * code.
	 */
	Din_Go(189,2048);if (cur->bc_flags & XFS_BTREE_LONG_PTRS)
		{/*105*/Din_Go(187,2048);owner = cur->bc_private.b.ip->i_ino;/*106*/}
	else
		{/*107*/Din_Go(188,2048);owner = cur->bc_private.a.agno;/*108*/}

	Din_Go(190,2048);xfs_btree_init_block_int(cur->bc_mp, XFS_BUF_TO_BLOCK(bp), bp->b_bn,
				 xfs_btree_magic(cur), level, numrecs,
				 owner, cur->bc_flags);
Din_Go(191,2048);}

/*
 * Return true if ptr is the last record in the btree and
 * we need to track updates to this record.  The decision
 * will be further refined in the update_lastrec method.
 */
STATIC int
xfs_btree_is_lastrec(
	struct xfs_btree_cur	*cur,
	struct xfs_btree_block	*block,
	int			level)
{
	Din_Go(192,2048);union xfs_btree_ptr	ptr;

	Din_Go(194,2048);if (level > 0)
		{/*109*/{int  ReplaceReturn = 0; Din_Go(193,2048); return ReplaceReturn;}/*110*/}
	Din_Go(196,2048);if (!(cur->bc_flags & XFS_BTREE_LASTREC_UPDATE))
		{/*111*/{int  ReplaceReturn = 0; Din_Go(195,2048); return ReplaceReturn;}/*112*/}

	Din_Go(197,2048);xfs_btree_get_sibling(cur, block, &ptr, XFS_BB_RIGHTSIB);
	Din_Go(199,2048);if (!xfs_btree_ptr_is_null(cur, &ptr))
		{/*113*/{int  ReplaceReturn = 0; Din_Go(198,2048); return ReplaceReturn;}/*114*/}
	{int  ReplaceReturn = 1; Din_Go(200,2048); return ReplaceReturn;}
}

STATIC void
xfs_btree_buf_to_ptr(
	struct xfs_btree_cur	*cur,
	struct xfs_buf		*bp,
	union xfs_btree_ptr	*ptr)
{
	Din_Go(201,2048);if (cur->bc_flags & XFS_BTREE_LONG_PTRS)
		ptr->l = cpu_to_be64(XFS_DADDR_TO_FSB(cur->bc_mp,
					XFS_BUF_ADDR(bp)));
	else {
		Din_Go(202,2048);ptr->s = cpu_to_be32(xfs_daddr_to_agbno(cur->bc_mp,
					XFS_BUF_ADDR(bp)));
	}
Din_Go(203,2048);}

STATIC void
xfs_btree_set_refs(
	struct xfs_btree_cur	*cur,
	struct xfs_buf		*bp)
{
	Din_Go(204,2048);switch (cur->bc_btnum) {
	case XFS_BTNUM_BNO:
	case XFS_BTNUM_CNT:
		xfs_buf_set_ref(bp, XFS_ALLOC_BTREE_REF);
		Din_Go(205,2048);break;
	case XFS_BTNUM_INO:
	case XFS_BTNUM_FINO:
		xfs_buf_set_ref(bp, XFS_INO_BTREE_REF);
		Din_Go(206,2048);break;
	case XFS_BTNUM_BMAP:
		xfs_buf_set_ref(bp, XFS_BMAP_BTREE_REF);
		Din_Go(207,2048);break;
	default:
		ASSERT(0);
	}
Din_Go(208,2048);}

STATIC int
xfs_btree_get_buf_block(
	struct xfs_btree_cur	*cur,
	union xfs_btree_ptr	*ptr,
	int			flags,
	struct xfs_btree_block	**block,
	struct xfs_buf		**bpp)
{
	Din_Go(209,2048);struct xfs_mount	*mp = cur->bc_mp;
	xfs_daddr_t		d;

	/* need to sort out how callers deal with failures first */
	ASSERT(!(flags & XBF_TRYLOCK));

	d = xfs_btree_ptr_to_daddr(cur, ptr);
	*bpp = xfs_trans_get_buf(cur->bc_tp, mp->m_ddev_targp, d,
				 mp->m_bsize, flags);

	Din_Go(210,2048);if (!*bpp)
		return -ENOMEM;

	Din_Go(211,2048);(*bpp)->b_ops = cur->bc_ops->buf_ops;
	*block = XFS_BUF_TO_BLOCK(*bpp);
	{int  ReplaceReturn = 0; Din_Go(212,2048); return ReplaceReturn;}
}

/*
 * Read in the buffer at the given ptr and return the buffer and
 * the block pointer within the buffer.
 */
STATIC int
xfs_btree_read_buf_block(
	struct xfs_btree_cur	*cur,
	union xfs_btree_ptr	*ptr,
	int			flags,
	struct xfs_btree_block	**block,
	struct xfs_buf		**bpp)
{
	Din_Go(213,2048);struct xfs_mount	*mp = cur->bc_mp;
	xfs_daddr_t		d;
	int			error;

	/* need to sort out how callers deal with failures first */
	ASSERT(!(flags & XBF_TRYLOCK));

	d = xfs_btree_ptr_to_daddr(cur, ptr);
	error = xfs_trans_read_buf(mp, cur->bc_tp, mp->m_ddev_targp, d,
				   mp->m_bsize, flags, bpp,
				   cur->bc_ops->buf_ops);
	Din_Go(215,2048);if (error)
		{/*115*/{int  ReplaceReturn = error; Din_Go(214,2048); return ReplaceReturn;}/*116*/}

	Din_Go(216,2048);xfs_btree_set_refs(cur, *bpp);
	*block = XFS_BUF_TO_BLOCK(*bpp);
	{int  ReplaceReturn = 0; Din_Go(217,2048); return ReplaceReturn;}
}

/*
 * Copy keys from one btree block to another.
 */
STATIC void
xfs_btree_copy_keys(
	struct xfs_btree_cur	*cur,
	union xfs_btree_key	*dst_key,
	union xfs_btree_key	*src_key,
	int			numkeys)
{
Din_Go(218,2048);	ASSERT(numkeys >= 0);
	memcpy(dst_key, src_key, numkeys * cur->bc_ops->key_len);
Din_Go(219,2048);}

/*
 * Copy records from one btree block to another.
 */
STATIC void
xfs_btree_copy_recs(
	struct xfs_btree_cur	*cur,
	union xfs_btree_rec	*dst_rec,
	union xfs_btree_rec	*src_rec,
	int			numrecs)
{
Din_Go(220,2048);	ASSERT(numrecs >= 0);
	memcpy(dst_rec, src_rec, numrecs * cur->bc_ops->rec_len);
Din_Go(221,2048);}

/*
 * Copy block pointers from one btree block to another.
 */
STATIC void
xfs_btree_copy_ptrs(
	struct xfs_btree_cur	*cur,
	union xfs_btree_ptr	*dst_ptr,
	union xfs_btree_ptr	*src_ptr,
	int			numptrs)
{
Din_Go(222,2048);	ASSERT(numptrs >= 0);
	memcpy(dst_ptr, src_ptr, numptrs * xfs_btree_ptr_len(cur));
Din_Go(223,2048);}

/*
 * Shift keys one index left/right inside a single btree block.
 */
STATIC void
xfs_btree_shift_keys(
	struct xfs_btree_cur	*cur,
	union xfs_btree_key	*key,
	int			dir,
	int			numkeys)
{
	Din_Go(224,2048);char			*dst_key;

	ASSERT(numkeys >= 0);
	ASSERT(dir == 1 || dir == -1);

	dst_key = (char *)key + (dir * cur->bc_ops->key_len);
	memmove(dst_key, key, numkeys * cur->bc_ops->key_len);
Din_Go(225,2048);}

/*
 * Shift records one index left/right inside a single btree block.
 */
STATIC void
xfs_btree_shift_recs(
	struct xfs_btree_cur	*cur,
	union xfs_btree_rec	*rec,
	int			dir,
	int			numrecs)
{
	Din_Go(226,2048);char			*dst_rec;

	ASSERT(numrecs >= 0);
	ASSERT(dir == 1 || dir == -1);

	dst_rec = (char *)rec + (dir * cur->bc_ops->rec_len);
	memmove(dst_rec, rec, numrecs * cur->bc_ops->rec_len);
Din_Go(227,2048);}

/*
 * Shift block pointers one index left/right inside a single btree block.
 */
STATIC void
xfs_btree_shift_ptrs(
	struct xfs_btree_cur	*cur,
	union xfs_btree_ptr	*ptr,
	int			dir,
	int			numptrs)
{
	Din_Go(228,2048);char			*dst_ptr;

	ASSERT(numptrs >= 0);
	ASSERT(dir == 1 || dir == -1);

	dst_ptr = (char *)ptr + (dir * xfs_btree_ptr_len(cur));
	memmove(dst_ptr, ptr, numptrs * xfs_btree_ptr_len(cur));
Din_Go(229,2048);}

/*
 * Log key values from the btree block.
 */
STATIC void
xfs_btree_log_keys(
	struct xfs_btree_cur	*cur,
	struct xfs_buf		*bp,
	int			first,
	int			last)
{
	XFS_BTREE_TRACE_CURSOR(cur, XBT_ENTRY);
	XFS_BTREE_TRACE_ARGBII(cur, bp, first, last);

	Din_Go(230,2048);if (bp) {
		xfs_trans_buf_set_type(cur->bc_tp, bp, XFS_BLFT_BTREE_BUF);
		xfs_trans_log_buf(cur->bc_tp, bp,
				  xfs_btree_key_offset(cur, first),
				  xfs_btree_key_offset(cur, last + 1) - 1);
	} else {
		xfs_trans_log_inode(cur->bc_tp, cur->bc_private.b.ip,
				xfs_ilog_fbroot(cur->bc_private.b.whichfork));
	}

	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
Din_Go(231,2048);}

/*
 * Log record values from the btree block.
 */
void
xfs_btree_log_recs(
	struct xfs_btree_cur	*cur,
	struct xfs_buf		*bp,
	int			first,
	int			last)
{
	XFS_BTREE_TRACE_CURSOR(cur, XBT_ENTRY);
	XFS_BTREE_TRACE_ARGBII(cur, bp, first, last);

	xfs_trans_buf_set_type(cur->bc_tp, bp, XFS_BLFT_BTREE_BUF);
	xfs_trans_log_buf(cur->bc_tp, bp,
			  xfs_btree_rec_offset(cur, first),
			  xfs_btree_rec_offset(cur, last + 1) - 1);

	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
}

/*
 * Log block pointer fields from a btree block (nonleaf).
 */
STATIC void
xfs_btree_log_ptrs(
	struct xfs_btree_cur	*cur,	/* btree cursor */
	struct xfs_buf		*bp,	/* buffer containing btree block */
	int			first,	/* index of first pointer to log */
	int			last)	/* index of last pointer to log */
{
	XFS_BTREE_TRACE_CURSOR(cur, XBT_ENTRY);
	XFS_BTREE_TRACE_ARGBII(cur, bp, first, last);

	Din_Go(232,2048);if (bp) {
		Din_Go(233,2048);struct xfs_btree_block	*block = XFS_BUF_TO_BLOCK(bp);
		int			level = xfs_btree_get_level(block);

		xfs_trans_buf_set_type(cur->bc_tp, bp, XFS_BLFT_BTREE_BUF);
		xfs_trans_log_buf(cur->bc_tp, bp,
				xfs_btree_ptr_offset(cur, first, level),
				xfs_btree_ptr_offset(cur, last + 1, level) - 1);
	} else {
		xfs_trans_log_inode(cur->bc_tp, cur->bc_private.b.ip,
			xfs_ilog_fbroot(cur->bc_private.b.whichfork));
	}

	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
Din_Go(234,2048);}

/*
 * Log fields from a btree block header.
 */
void
xfs_btree_log_block(
	struct xfs_btree_cur	*cur,	/* btree cursor */
	struct xfs_buf		*bp,	/* buffer containing btree block */
	int			fields)	/* mask of fields: XFS_BB_... */
{
	Din_Go(235,2048);int			first;	/* first byte offset logged */
	int			last;	/* last byte offset logged */
	static const short	soffsets[] = {	/* table of offsets (short) */
		offsetof(struct xfs_btree_block, bb_magic),
		offsetof(struct xfs_btree_block, bb_level),
		offsetof(struct xfs_btree_block, bb_numrecs),
		offsetof(struct xfs_btree_block, bb_u.s.bb_leftsib),
		offsetof(struct xfs_btree_block, bb_u.s.bb_rightsib),
		offsetof(struct xfs_btree_block, bb_u.s.bb_blkno),
		offsetof(struct xfs_btree_block, bb_u.s.bb_lsn),
		offsetof(struct xfs_btree_block, bb_u.s.bb_uuid),
		offsetof(struct xfs_btree_block, bb_u.s.bb_owner),
		offsetof(struct xfs_btree_block, bb_u.s.bb_crc),
		XFS_BTREE_SBLOCK_CRC_LEN
	};
	static const short	loffsets[] = {	/* table of offsets (long) */
		offsetof(struct xfs_btree_block, bb_magic),
		offsetof(struct xfs_btree_block, bb_level),
		offsetof(struct xfs_btree_block, bb_numrecs),
		offsetof(struct xfs_btree_block, bb_u.l.bb_leftsib),
		offsetof(struct xfs_btree_block, bb_u.l.bb_rightsib),
		offsetof(struct xfs_btree_block, bb_u.l.bb_blkno),
		offsetof(struct xfs_btree_block, bb_u.l.bb_lsn),
		offsetof(struct xfs_btree_block, bb_u.l.bb_uuid),
		offsetof(struct xfs_btree_block, bb_u.l.bb_owner),
		offsetof(struct xfs_btree_block, bb_u.l.bb_crc),
		offsetof(struct xfs_btree_block, bb_u.l.bb_pad),
		XFS_BTREE_LBLOCK_CRC_LEN
	};

	XFS_BTREE_TRACE_CURSOR(cur, XBT_ENTRY);
	XFS_BTREE_TRACE_ARGBI(cur, bp, fields);

	Din_Go(242,2048);if (bp) {
		Din_Go(236,2048);int nbits;

		Din_Go(240,2048);if (cur->bc_flags & XFS_BTREE_CRC_BLOCKS) {
			/*
			 * We don't log the CRC when updating a btree
			 * block but instead recreate it during log
			 * recovery.  As the log buffers have checksums
			 * of their own this is safe and avoids logging a crc
			 * update in a lot of places.
			 */
			Din_Go(237,2048);if (fields == XFS_BB_ALL_BITS)
				fields = XFS_BB_ALL_BITS_CRC;
			Din_Go(238,2048);nbits = XFS_BB_NUM_BITS_CRC;
		} else {
			Din_Go(239,2048);nbits = XFS_BB_NUM_BITS;
		}
		Din_Go(241,2048);xfs_btree_offsets(fields,
				  (cur->bc_flags & XFS_BTREE_LONG_PTRS) ?
					loffsets : soffsets,
				  nbits, &first, &last);
		xfs_trans_buf_set_type(cur->bc_tp, bp, XFS_BLFT_BTREE_BUF);
		xfs_trans_log_buf(cur->bc_tp, bp, first, last);
	} else {
		xfs_trans_log_inode(cur->bc_tp, cur->bc_private.b.ip,
			xfs_ilog_fbroot(cur->bc_private.b.whichfork));
	}

	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
Din_Go(243,2048);}

/*
 * Increment cursor by one record at the level.
 * For nonzero levels the leaf-ward information is untouched.
 */
int						/* error */
xfs_btree_increment(
	struct xfs_btree_cur	*cur,
	int			level,
	int			*stat)		/* success/failure */
{
	Din_Go(244,2048);struct xfs_btree_block	*block;
	union xfs_btree_ptr	ptr;
	struct xfs_buf		*bp;
	int			error;		/* error return value */
	int			lev;

	XFS_BTREE_TRACE_CURSOR(cur, XBT_ENTRY);
	XFS_BTREE_TRACE_ARGI(cur, level);

	ASSERT(level < cur->bc_nlevels);

	/* Read-ahead to the right at this level. */
	xfs_btree_readahead(cur, level, XFS_BTCUR_RIGHTRA);

	/* Get a pointer to the btree block. */
	block = xfs_btree_get_block(cur, level, &bp);

#ifdef DEBUG
	error = xfs_btree_check_block(cur, block, level, bp);
	if (error)
		goto error0;
#endif

	/* We're done if we remain in the block after the increment. */
	Din_Go(246,2048);if (++cur->bc_ptrs[level] <= xfs_btree_get_numrecs(block))
		{/*11*/Din_Go(245,2048);goto out1;/*12*/}

	/* Fail if we just went off the right edge of the tree. */
	Din_Go(247,2048);xfs_btree_get_sibling(cur, block, &ptr, XFS_BB_RIGHTSIB);
	Din_Go(249,2048);if (xfs_btree_ptr_is_null(cur, &ptr))
		{/*13*/Din_Go(248,2048);goto out0;/*14*/}

	XFS_BTREE_STATS_INC(cur, increment);

	/*
	 * March up the tree incrementing pointers.
	 * Stop when we don't go off the right edge of a block.
	 */
	Din_Go(254,2048);for (lev = level + 1; lev < cur->bc_nlevels; lev++) {
		Din_Go(250,2048);block = xfs_btree_get_block(cur, lev, &bp);

#ifdef DEBUG
		error = xfs_btree_check_block(cur, block, lev, bp);
		if (error)
			goto error0;
#endif

		Din_Go(252,2048);if (++cur->bc_ptrs[lev] <= xfs_btree_get_numrecs(block))
			{/*15*/Din_Go(251,2048);break;/*16*/}

		/* Read-ahead the right block for the next loop. */
		Din_Go(253,2048);xfs_btree_readahead(cur, lev, XFS_BTCUR_RIGHTRA);
	}

	/*
	 * If we went off the root then we are either seriously
	 * confused or have the tree root in an inode.
	 */
	Din_Go(259,2048);if (lev == cur->bc_nlevels) {
		Din_Go(255,2048);if (cur->bc_flags & XFS_BTREE_ROOT_IN_INODE)
			{/*17*/Din_Go(256,2048);goto out0;/*18*/}
		ASSERT(0);
		Din_Go(257,2048);error = -EFSCORRUPTED;
		Din_Go(258,2048);goto error0;
	}
	ASSERT(lev < cur->bc_nlevels);

	/*
	 * Now walk back down the tree, fixing up the cursor's buffer
	 * pointers and key numbers.
	 */
	Din_Go(264,2048);for (block = xfs_btree_get_block(cur, lev, &bp); lev > level; ) {
		Din_Go(260,2048);union xfs_btree_ptr	*ptrp;

		ptrp = xfs_btree_ptr_addr(cur, cur->bc_ptrs[lev], block);
		--lev;
		error = xfs_btree_read_buf_block(cur, ptrp, 0, &block, &bp);
		Din_Go(262,2048);if (error)
			{/*19*/Din_Go(261,2048);goto error0;/*20*/}

		Din_Go(263,2048);xfs_btree_setbuf(cur, lev, bp);
		cur->bc_ptrs[lev] = 1;
	}
out1:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	Din_Go(265,2048);*stat = 1;
	{int  ReplaceReturn = 0; Din_Go(266,2048); return ReplaceReturn;}

out0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	*stat = 0;
	return 0;

error0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_ERROR);
	return error;
}

/*
 * Decrement cursor by one record at the level.
 * For nonzero levels the leaf-ward information is untouched.
 */
int						/* error */
xfs_btree_decrement(
	struct xfs_btree_cur	*cur,
	int			level,
	int			*stat)		/* success/failure */
{
	Din_Go(267,2048);struct xfs_btree_block	*block;
	xfs_buf_t		*bp;
	int			error;		/* error return value */
	int			lev;
	union xfs_btree_ptr	ptr;

	XFS_BTREE_TRACE_CURSOR(cur, XBT_ENTRY);
	XFS_BTREE_TRACE_ARGI(cur, level);

	ASSERT(level < cur->bc_nlevels);

	/* Read-ahead to the left at this level. */
	xfs_btree_readahead(cur, level, XFS_BTCUR_LEFTRA);

	/* We're done if we remain in the block after the decrement. */
	Din_Go(269,2048);if (--cur->bc_ptrs[level] > 0)
		{/*21*/Din_Go(268,2048);goto out1;/*22*/}

	/* Get a pointer to the btree block. */
	Din_Go(270,2048);block = xfs_btree_get_block(cur, level, &bp);

#ifdef DEBUG
	error = xfs_btree_check_block(cur, block, level, bp);
	if (error)
		goto error0;
#endif

	/* Fail if we just went off the left edge of the tree. */
	xfs_btree_get_sibling(cur, block, &ptr, XFS_BB_LEFTSIB);
	Din_Go(272,2048);if (xfs_btree_ptr_is_null(cur, &ptr))
		{/*23*/Din_Go(271,2048);goto out0;/*24*/}

	XFS_BTREE_STATS_INC(cur, decrement);

	/*
	 * March up the tree decrementing pointers.
	 * Stop when we don't go off the left edge of a block.
	 */
	Din_Go(276,2048);for (lev = level + 1; lev < cur->bc_nlevels; lev++) {
		Din_Go(273,2048);if (--cur->bc_ptrs[lev] > 0)
			{/*25*/Din_Go(274,2048);break;/*26*/}
		/* Read-ahead the left block for the next loop. */
		Din_Go(275,2048);xfs_btree_readahead(cur, lev, XFS_BTCUR_LEFTRA);
	}

	/*
	 * If we went off the root then we are seriously confused.
	 * or the root of the tree is in an inode.
	 */
	Din_Go(281,2048);if (lev == cur->bc_nlevels) {
		Din_Go(277,2048);if (cur->bc_flags & XFS_BTREE_ROOT_IN_INODE)
			{/*27*/Din_Go(278,2048);goto out0;/*28*/}
		ASSERT(0);
		Din_Go(279,2048);error = -EFSCORRUPTED;
		Din_Go(280,2048);goto error0;
	}
	ASSERT(lev < cur->bc_nlevels);

	/*
	 * Now walk back down the tree, fixing up the cursor's buffer
	 * pointers and key numbers.
	 */
	Din_Go(286,2048);for (block = xfs_btree_get_block(cur, lev, &bp); lev > level; ) {
		Din_Go(282,2048);union xfs_btree_ptr	*ptrp;

		ptrp = xfs_btree_ptr_addr(cur, cur->bc_ptrs[lev], block);
		--lev;
		error = xfs_btree_read_buf_block(cur, ptrp, 0, &block, &bp);
		Din_Go(284,2048);if (error)
			{/*29*/Din_Go(283,2048);goto error0;/*30*/}
		Din_Go(285,2048);xfs_btree_setbuf(cur, lev, bp);
		cur->bc_ptrs[lev] = xfs_btree_get_numrecs(block);
	}
out1:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	Din_Go(287,2048);*stat = 1;
	{int  ReplaceReturn = 0; Din_Go(288,2048); return ReplaceReturn;}

out0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	*stat = 0;
	return 0;

error0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_ERROR);
	return error;
}

STATIC int
xfs_btree_lookup_get_block(
	struct xfs_btree_cur	*cur,	/* btree cursor */
	int			level,	/* level in the btree */
	union xfs_btree_ptr	*pp,	/* ptr to btree block */
	struct xfs_btree_block	**blkp) /* return btree block */
{
	Din_Go(289,2048);struct xfs_buf		*bp;	/* buffer pointer for btree block */
	int			error = 0;

	/* special case the root block if in an inode */
	Din_Go(292,2048);if ((cur->bc_flags & XFS_BTREE_ROOT_IN_INODE) &&
	    (level == cur->bc_nlevels - 1)) {
		Din_Go(290,2048);*blkp = xfs_btree_get_iroot(cur);
		{int  ReplaceReturn = 0; Din_Go(291,2048); return ReplaceReturn;}
	}

	/*
	 * If the old buffer at this level for the disk address we are
	 * looking for re-use it.
	 *
	 * Otherwise throw it away and get a new one.
	 */
	Din_Go(293,2048);bp = cur->bc_bufs[level];
	Din_Go(296,2048);if (bp && XFS_BUF_ADDR(bp) == xfs_btree_ptr_to_daddr(cur, pp)) {
		Din_Go(294,2048);*blkp = XFS_BUF_TO_BLOCK(bp);
		{int  ReplaceReturn = 0; Din_Go(295,2048); return ReplaceReturn;}
	}

	Din_Go(297,2048);error = xfs_btree_read_buf_block(cur, pp, 0, blkp, &bp);
	Din_Go(299,2048);if (error)
		{/*117*/{int  ReplaceReturn = error; Din_Go(298,2048); return ReplaceReturn;}/*118*/}

	Din_Go(300,2048);xfs_btree_setbuf(cur, level, bp);
	{int  ReplaceReturn = 0; Din_Go(301,2048); return ReplaceReturn;}
}

/*
 * Get current search key.  For level 0 we don't actually have a key
 * structure so we make one up from the record.  For all other levels
 * we just return the right key.
 */
STATIC union xfs_btree_key *
xfs_lookup_get_search_key(
	struct xfs_btree_cur	*cur,
	int			level,
	int			keyno,
	struct xfs_btree_block	*block,
	union xfs_btree_key	*kp)
{
	Din_Go(302,2048);if (level == 0) {
		Din_Go(303,2048);cur->bc_ops->init_key_from_rec(kp,
				xfs_btree_rec_addr(cur, keyno, block));
		{__IAUNION__ xfs_btree_key * ReplaceReturn = kp; Din_Go(304,2048); return ReplaceReturn;}
	}

	{__IAUNION__ xfs_btree_key * ReplaceReturn = xfs_btree_key_addr(cur, keyno, block); Din_Go(305,2048); return ReplaceReturn;}
}

/*
 * Lookup the record.  The cursor is made to point to it, based on dir.
 * stat is set to 0 if can't find any such record, 1 for success.
 */
int					/* error */
xfs_btree_lookup(
	struct xfs_btree_cur	*cur,	/* btree cursor */
	xfs_lookup_t		dir,	/* <=, ==, or >= */
	int			*stat)	/* success/failure */
{
	Din_Go(306,2048);struct xfs_btree_block	*block;	/* current btree block */
	__int64_t		diff;	/* difference for the current key */
	int			error;	/* error return value */
	int			keyno;	/* current key number */
	int			level;	/* level in the btree */
	union xfs_btree_ptr	*pp;	/* ptr to btree block */
	union xfs_btree_ptr	ptr;	/* ptr to btree block */

	XFS_BTREE_TRACE_CURSOR(cur, XBT_ENTRY);
	XFS_BTREE_TRACE_ARGI(cur, dir);

	XFS_BTREE_STATS_INC(cur, lookup);

	block = NULL;
	keyno = 0;

	/* initialise start pointer from cursor */
	cur->bc_ops->init_ptr_from_cur(cur, &ptr);
	pp = &ptr;

	/*
	 * Iterate over each level in the btree, starting at the root.
	 * For each level above the leaves, find the key we need, based
	 * on the lookup record, then follow the corresponding block
	 * pointer down to the next level.
	 */
	Din_Go(327,2048);for (level = cur->bc_nlevels - 1, diff = 1; level >= 0; level--) {
		/* Get the block we need to do the lookup on. */
		Din_Go(307,2048);error = xfs_btree_lookup_get_block(cur, level, pp, &block);
		Din_Go(309,2048);if (error)
			{/*31*/Din_Go(308,2048);goto error0;/*32*/}

		Din_Go(322,2048);if (diff == 0) {
			/*
			 * If we already had a key match at a higher level, we
			 * know we need to use the first entry in this block.
			 */
			Din_Go(310,2048);keyno = 1;
		} else {
			/* Otherwise search this block. Do a binary search. */

			Din_Go(311,2048);int	high;	/* high entry number */
			int	low;	/* low entry number */

			/* Set low and high entry numbers, 1-based. */
			low = 1;
			high = xfs_btree_get_numrecs(block);
			Din_Go(314,2048);if (!high) {
				/* Block is empty, must be an empty leaf. */
				ASSERT(level == 0 && cur->bc_nlevels == 1);

				Din_Go(312,2048);cur->bc_ptrs[0] = dir != XFS_LOOKUP_LE;
				XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
				*stat = 0;
				{int  ReplaceReturn = 0; Din_Go(313,2048); return ReplaceReturn;}
			}

			/* Binary search the block. */
			Din_Go(321,2048);while (low <= high) {
				Din_Go(315,2048);union xfs_btree_key	key;
				union xfs_btree_key	*kp;

				XFS_BTREE_STATS_INC(cur, compare);

				/* keyno is average of low and high. */
				keyno = (low + high) >> 1;

				/* Get current search key */
				kp = xfs_lookup_get_search_key(cur, level,
						keyno, block, &key);

				/*
				 * Compute difference to get next direction:
				 *  - less than, move right
				 *  - greater than, move left
				 *  - equal, we're done
				 */
				diff = cur->bc_ops->key_diff(cur, kp);
				Din_Go(320,2048);if (diff < 0)
					{/*33*/Din_Go(316,2048);low = keyno + 1;/*34*/}
				else {/*35*/Din_Go(317,2048);if (diff > 0)
					{/*37*/Din_Go(318,2048);high = keyno - 1;/*38*/}
				else
					{/*39*/Din_Go(319,2048);break;/*40*/}/*36*/}
			}
		}

		/*
		 * If there are more levels, set up for the next level
		 * by getting the block number and filling in the cursor.
		 */
		Din_Go(326,2048);if (level > 0) {
			/*
			 * If we moved left, need the previous key number,
			 * unless there isn't one.
			 */
			Din_Go(323,2048);if (diff > 0 && --keyno < 1)
				{/*41*/Din_Go(324,2048);keyno = 1;/*42*/}
			Din_Go(325,2048);pp = xfs_btree_ptr_addr(cur, keyno, block);

#ifdef DEBUG
			error = xfs_btree_check_ptr(cur, pp, 0, level);
			if (error)
				goto error0;
#endif
			cur->bc_ptrs[level] = keyno;
		}
	}

	/* Done with the search. See if we need to adjust the results. */
	Din_Go(337,2048);if (dir != XFS_LOOKUP_LE && diff < 0) {
		Din_Go(328,2048);keyno++;
		/*
		 * If ge search and we went off the end of the block, but it's
		 * not the last block, we're in the wrong block.
		 */
		xfs_btree_get_sibling(cur, block, &ptr, XFS_BB_RIGHTSIB);
		Din_Go(334,2048);if (dir == XFS_LOOKUP_GE &&
		    keyno > xfs_btree_get_numrecs(block) &&
		    !xfs_btree_ptr_is_null(cur, &ptr)) {
			Din_Go(329,2048);int	i;

			cur->bc_ptrs[0] = keyno;
			error = xfs_btree_increment(cur, 0, &i);
			Din_Go(331,2048);if (error)
				{/*43*/Din_Go(330,2048);goto error0;/*44*/}
			XFS_WANT_CORRUPTED_RETURN(cur->bc_mp, i == 1);
			XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
			Din_Go(332,2048);*stat = 1;
			{int  ReplaceReturn = 0; Din_Go(333,2048); return ReplaceReturn;}
		}
	} else {/*45*/Din_Go(335,2048);if (dir == XFS_LOOKUP_LE && diff > 0)
		{/*47*/Din_Go(336,2048);keyno--;/*48*/}/*46*/}
	Din_Go(338,2048);cur->bc_ptrs[0] = keyno;

	/* Return if we succeeded or not. */
	Din_Go(343,2048);if (keyno == 0 || keyno > xfs_btree_get_numrecs(block))
		{/*49*/Din_Go(339,2048);*stat = 0;/*50*/}
	else {/*51*/Din_Go(340,2048);if (dir != XFS_LOOKUP_EQ || diff == 0)
		{/*53*/Din_Go(341,2048);*stat = 1;/*54*/}
	else
		{/*55*/Din_Go(342,2048);*stat = 0;/*56*/}/*52*/}
	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	{int  ReplaceReturn = 0; Din_Go(344,2048); return ReplaceReturn;}

error0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_ERROR);
	return error;
}

/*
 * Update keys at all levels from here to the root along the cursor's path.
 */
STATIC int
xfs_btree_updkey(
	struct xfs_btree_cur	*cur,
	union xfs_btree_key	*keyp,
	int			level)
{
	Din_Go(345,2048);struct xfs_btree_block	*block;
	struct xfs_buf		*bp;
	union xfs_btree_key	*kp;
	int			ptr;

	XFS_BTREE_TRACE_CURSOR(cur, XBT_ENTRY);
	XFS_BTREE_TRACE_ARGIK(cur, level, keyp);

	ASSERT(!(cur->bc_flags & XFS_BTREE_ROOT_IN_INODE) || level >= 1);

	/*
	 * Go up the tree from this level toward the root.
	 * At each level, update the key value to the value input.
	 * Stop when we reach a level where the cursor isn't pointing
	 * at the first entry in the block.
	 */
	Din_Go(347,2048);for (ptr = 1; ptr == 1 && level < cur->bc_nlevels; level++) {
#ifdef DEBUG
		int		error;
#endif
		Din_Go(346,2048);block = xfs_btree_get_block(cur, level, &bp);
#ifdef DEBUG
		error = xfs_btree_check_block(cur, block, level, bp);
		if (error) {
			XFS_BTREE_TRACE_CURSOR(cur, XBT_ERROR);
			return error;
		}
#endif
		ptr = cur->bc_ptrs[level];
		kp = xfs_btree_key_addr(cur, ptr, block);
		xfs_btree_copy_keys(cur, kp, keyp, 1);
		xfs_btree_log_keys(cur, bp, ptr, ptr);
	}

	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	{int  ReplaceReturn = 0; Din_Go(348,2048); return ReplaceReturn;}
}

/*
 * Update the record referred to by cur to the value in the
 * given record. This either works (return 0) or gets an
 * EFSCORRUPTED error.
 */
int
xfs_btree_update(
	struct xfs_btree_cur	*cur,
	union xfs_btree_rec	*rec)
{
	Din_Go(349,2048);struct xfs_btree_block	*block;
	struct xfs_buf		*bp;
	int			error;
	int			ptr;
	union xfs_btree_rec	*rp;

	XFS_BTREE_TRACE_CURSOR(cur, XBT_ENTRY);
	XFS_BTREE_TRACE_ARGR(cur, rec);

	/* Pick up the current block. */
	block = xfs_btree_get_block(cur, 0, &bp);

#ifdef DEBUG
	error = xfs_btree_check_block(cur, block, 0, bp);
	if (error)
		goto error0;
#endif
	/* Get the address of the rec to be updated. */
	ptr = cur->bc_ptrs[0];
	rp = xfs_btree_rec_addr(cur, ptr, block);

	/* Fill in the new contents and log them. */
	xfs_btree_copy_recs(cur, rp, rec, 1);
	xfs_btree_log_recs(cur, bp, ptr, ptr);

	/*
	 * If we are tracking the last record in the tree and
	 * we are at the far right edge of the tree, update it.
	 */
	Din_Go(351,2048);if (xfs_btree_is_lastrec(cur, block, 0)) {
		Din_Go(350,2048);cur->bc_ops->update_lastrec(cur, block, rec,
					    ptr, LASTREC_UPDATE);
	}

	/* Updating first rec in leaf. Pass new key value up to our parent. */
	Din_Go(355,2048);if (ptr == 1) {
		Din_Go(352,2048);union xfs_btree_key	key;

		cur->bc_ops->init_key_from_rec(&key, rec);
		error = xfs_btree_updkey(cur, &key, 1);
		Din_Go(354,2048);if (error)
			{/*57*/Din_Go(353,2048);goto error0;/*58*/}
	}

	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	{int  ReplaceReturn = 0; Din_Go(356,2048); return ReplaceReturn;}

error0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_ERROR);
	return error;
}

/*
 * Move 1 record left from cur/level if possible.
 * Update cur to reflect the new path.
 */
STATIC int					/* error */
xfs_btree_lshift(
	struct xfs_btree_cur	*cur,
	int			level,
	int			*stat)		/* success/failure */
{
	Din_Go(357,2048);union xfs_btree_key	key;		/* btree key */
	struct xfs_buf		*lbp;		/* left buffer pointer */
	struct xfs_btree_block	*left;		/* left btree block */
	int			lrecs;		/* left record count */
	struct xfs_buf		*rbp;		/* right buffer pointer */
	struct xfs_btree_block	*right;		/* right btree block */
	int			rrecs;		/* right record count */
	union xfs_btree_ptr	lptr;		/* left btree pointer */
	union xfs_btree_key	*rkp = NULL;	/* right btree key */
	union xfs_btree_ptr	*rpp = NULL;	/* right address pointer */
	union xfs_btree_rec	*rrp = NULL;	/* right record pointer */
	int			error;		/* error return value */

	XFS_BTREE_TRACE_CURSOR(cur, XBT_ENTRY);
	XFS_BTREE_TRACE_ARGI(cur, level);

	Din_Go(359,2048);if ((cur->bc_flags & XFS_BTREE_ROOT_IN_INODE) &&
	    level == cur->bc_nlevels - 1)
		{/*119*/Din_Go(358,2048);goto out0;/*120*/}

	/* Set up variables for this block as "right". */
	Din_Go(360,2048);right = xfs_btree_get_block(cur, level, &rbp);

#ifdef DEBUG
	error = xfs_btree_check_block(cur, right, level, rbp);
	if (error)
		goto error0;
#endif

	/* If we've got no left sibling then we can't shift an entry left. */
	xfs_btree_get_sibling(cur, right, &lptr, XFS_BB_LEFTSIB);
	Din_Go(362,2048);if (xfs_btree_ptr_is_null(cur, &lptr))
		{/*121*/Din_Go(361,2048);goto out0;/*122*/}

	/*
	 * If the cursor entry is the one that would be moved, don't
	 * do it... it's too complicated.
	 */
	Din_Go(364,2048);if (cur->bc_ptrs[level] <= 1)
		{/*123*/Din_Go(363,2048);goto out0;/*124*/}

	/* Set up the left neighbor as "left". */
	Din_Go(365,2048);error = xfs_btree_read_buf_block(cur, &lptr, 0, &left, &lbp);
	Din_Go(367,2048);if (error)
		{/*125*/Din_Go(366,2048);goto error0;/*126*/}

	/* If it's full, it can't take another entry. */
	Din_Go(368,2048);lrecs = xfs_btree_get_numrecs(left);
	Din_Go(370,2048);if (lrecs == cur->bc_ops->get_maxrecs(cur, level))
		{/*127*/Din_Go(369,2048);goto out0;/*128*/}

	Din_Go(371,2048);rrecs = xfs_btree_get_numrecs(right);

	/*
	 * We add one entry to the left side and remove one for the right side.
	 * Account for it here, the changes will be updated on disk and logged
	 * later.
	 */
	lrecs++;
	rrecs--;

	XFS_BTREE_STATS_INC(cur, lshift);
	XFS_BTREE_STATS_ADD(cur, moves, 1);

	/*
	 * If non-leaf, copy a key and a ptr to the left block.
	 * Log the changes to the left block.
	 */
	Din_Go(374,2048);if (level > 0) {
		/* It's a non-leaf.  Move keys and pointers. */
		Din_Go(372,2048);union xfs_btree_key	*lkp;	/* left btree key */
		union xfs_btree_ptr	*lpp;	/* left address pointer */

		lkp = xfs_btree_key_addr(cur, lrecs, left);
		rkp = xfs_btree_key_addr(cur, 1, right);

		lpp = xfs_btree_ptr_addr(cur, lrecs, left);
		rpp = xfs_btree_ptr_addr(cur, 1, right);
#ifdef DEBUG
		error = xfs_btree_check_ptr(cur, rpp, 0, level);
		if (error)
			goto error0;
#endif
		xfs_btree_copy_keys(cur, lkp, rkp, 1);
		xfs_btree_copy_ptrs(cur, lpp, rpp, 1);

		xfs_btree_log_keys(cur, lbp, lrecs, lrecs);
		xfs_btree_log_ptrs(cur, lbp, lrecs, lrecs);

		ASSERT(cur->bc_ops->keys_inorder(cur,
			xfs_btree_key_addr(cur, lrecs - 1, left), lkp));
	} else {
		/* It's a leaf.  Move records.  */
		Din_Go(373,2048);union xfs_btree_rec	*lrp;	/* left record pointer */

		lrp = xfs_btree_rec_addr(cur, lrecs, left);
		rrp = xfs_btree_rec_addr(cur, 1, right);

		xfs_btree_copy_recs(cur, lrp, rrp, 1);
		xfs_btree_log_recs(cur, lbp, lrecs, lrecs);

		ASSERT(cur->bc_ops->recs_inorder(cur,
			xfs_btree_rec_addr(cur, lrecs - 1, left), lrp));
	}

	Din_Go(375,2048);xfs_btree_set_numrecs(left, lrecs);
	xfs_btree_log_block(cur, lbp, XFS_BB_NUMRECS);

	xfs_btree_set_numrecs(right, rrecs);
	xfs_btree_log_block(cur, rbp, XFS_BB_NUMRECS);

	/*
	 * Slide the contents of right down one entry.
	 */
	XFS_BTREE_STATS_ADD(cur, moves, rrecs - 1);
	Din_Go(378,2048);if (level > 0) {
		/* It's a nonleaf. operate on keys and ptrs */
#ifdef DEBUG
		int			i;		/* loop index */

		for (i = 0; i < rrecs; i++) {
			error = xfs_btree_check_ptr(cur, rpp, i + 1, level);
			if (error)
				goto error0;
		}
#endif
		Din_Go(376,2048);xfs_btree_shift_keys(cur,
				xfs_btree_key_addr(cur, 2, right),
				-1, rrecs);
		xfs_btree_shift_ptrs(cur,
				xfs_btree_ptr_addr(cur, 2, right),
				-1, rrecs);

		xfs_btree_log_keys(cur, rbp, 1, rrecs);
		xfs_btree_log_ptrs(cur, rbp, 1, rrecs);
	} else {
		/* It's a leaf. operate on records */
		Din_Go(377,2048);xfs_btree_shift_recs(cur,
			xfs_btree_rec_addr(cur, 2, right),
			-1, rrecs);
		xfs_btree_log_recs(cur, rbp, 1, rrecs);

		/*
		 * If it's the first record in the block, we'll need a key
		 * structure to pass up to the next level (updkey).
		 */
		cur->bc_ops->init_key_from_rec(&key,
			xfs_btree_rec_addr(cur, 1, right));
		rkp = &key;
	}

	/* Update the parent key values of right. */
	Din_Go(379,2048);error = xfs_btree_updkey(cur, rkp, level + 1);
	Din_Go(381,2048);if (error)
		{/*129*/Din_Go(380,2048);goto error0;/*130*/}

	/* Slide the cursor value left one. */
	Din_Go(382,2048);cur->bc_ptrs[level]--;

	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	*stat = 1;
	{int  ReplaceReturn = 0; Din_Go(383,2048); return ReplaceReturn;}

out0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	*stat = 0;
	return 0;

error0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_ERROR);
	return error;
}

/*
 * Move 1 record right from cur/level if possible.
 * Update cur to reflect the new path.
 */
STATIC int					/* error */
xfs_btree_rshift(
	struct xfs_btree_cur	*cur,
	int			level,
	int			*stat)		/* success/failure */
{
	Din_Go(384,2048);union xfs_btree_key	key;		/* btree key */
	struct xfs_buf		*lbp;		/* left buffer pointer */
	struct xfs_btree_block	*left;		/* left btree block */
	struct xfs_buf		*rbp;		/* right buffer pointer */
	struct xfs_btree_block	*right;		/* right btree block */
	struct xfs_btree_cur	*tcur;		/* temporary btree cursor */
	union xfs_btree_ptr	rptr;		/* right block pointer */
	union xfs_btree_key	*rkp;		/* right btree key */
	int			rrecs;		/* right record count */
	int			lrecs;		/* left record count */
	int			error;		/* error return value */
	int			i;		/* loop counter */

	XFS_BTREE_TRACE_CURSOR(cur, XBT_ENTRY);
	XFS_BTREE_TRACE_ARGI(cur, level);

	Din_Go(386,2048);if ((cur->bc_flags & XFS_BTREE_ROOT_IN_INODE) &&
	    (level == cur->bc_nlevels - 1))
		{/*131*/Din_Go(385,2048);goto out0;/*132*/}

	/* Set up variables for this block as "left". */
	Din_Go(387,2048);left = xfs_btree_get_block(cur, level, &lbp);

#ifdef DEBUG
	error = xfs_btree_check_block(cur, left, level, lbp);
	if (error)
		goto error0;
#endif

	/* If we've got no right sibling then we can't shift an entry right. */
	xfs_btree_get_sibling(cur, left, &rptr, XFS_BB_RIGHTSIB);
	Din_Go(389,2048);if (xfs_btree_ptr_is_null(cur, &rptr))
		{/*133*/Din_Go(388,2048);goto out0;/*134*/}

	/*
	 * If the cursor entry is the one that would be moved, don't
	 * do it... it's too complicated.
	 */
	Din_Go(390,2048);lrecs = xfs_btree_get_numrecs(left);
	Din_Go(392,2048);if (cur->bc_ptrs[level] >= lrecs)
		{/*135*/Din_Go(391,2048);goto out0;/*136*/}

	/* Set up the right neighbor as "right". */
	Din_Go(393,2048);error = xfs_btree_read_buf_block(cur, &rptr, 0, &right, &rbp);
	Din_Go(395,2048);if (error)
		{/*137*/Din_Go(394,2048);goto error0;/*138*/}

	/* If it's full, it can't take another entry. */
	Din_Go(396,2048);rrecs = xfs_btree_get_numrecs(right);
	Din_Go(398,2048);if (rrecs == cur->bc_ops->get_maxrecs(cur, level))
		{/*139*/Din_Go(397,2048);goto out0;/*140*/}

	XFS_BTREE_STATS_INC(cur, rshift);
	XFS_BTREE_STATS_ADD(cur, moves, rrecs);

	/*
	 * Make a hole at the start of the right neighbor block, then
	 * copy the last left block entry to the hole.
	 */
	Din_Go(401,2048);if (level > 0) {
		/* It's a nonleaf. make a hole in the keys and ptrs */
		Din_Go(399,2048);union xfs_btree_key	*lkp;
		union xfs_btree_ptr	*lpp;
		union xfs_btree_ptr	*rpp;

		lkp = xfs_btree_key_addr(cur, lrecs, left);
		lpp = xfs_btree_ptr_addr(cur, lrecs, left);
		rkp = xfs_btree_key_addr(cur, 1, right);
		rpp = xfs_btree_ptr_addr(cur, 1, right);

#ifdef DEBUG
		for (i = rrecs - 1; i >= 0; i--) {
			error = xfs_btree_check_ptr(cur, rpp, i, level);
			if (error)
				goto error0;
		}
#endif

		xfs_btree_shift_keys(cur, rkp, 1, rrecs);
		xfs_btree_shift_ptrs(cur, rpp, 1, rrecs);

#ifdef DEBUG
		error = xfs_btree_check_ptr(cur, lpp, 0, level);
		if (error)
			goto error0;
#endif

		/* Now put the new data in, and log it. */
		xfs_btree_copy_keys(cur, rkp, lkp, 1);
		xfs_btree_copy_ptrs(cur, rpp, lpp, 1);

		xfs_btree_log_keys(cur, rbp, 1, rrecs + 1);
		xfs_btree_log_ptrs(cur, rbp, 1, rrecs + 1);

		ASSERT(cur->bc_ops->keys_inorder(cur, rkp,
			xfs_btree_key_addr(cur, 2, right)));
	} else {
		/* It's a leaf. make a hole in the records */
		Din_Go(400,2048);union xfs_btree_rec	*lrp;
		union xfs_btree_rec	*rrp;

		lrp = xfs_btree_rec_addr(cur, lrecs, left);
		rrp = xfs_btree_rec_addr(cur, 1, right);

		xfs_btree_shift_recs(cur, rrp, 1, rrecs);

		/* Now put the new data in, and log it. */
		xfs_btree_copy_recs(cur, rrp, lrp, 1);
		xfs_btree_log_recs(cur, rbp, 1, rrecs + 1);

		cur->bc_ops->init_key_from_rec(&key, rrp);
		rkp = &key;

		ASSERT(cur->bc_ops->recs_inorder(cur, rrp,
			xfs_btree_rec_addr(cur, 2, right)));
	}

	/*
	 * Decrement and log left's numrecs, bump and log right's numrecs.
	 */
	Din_Go(402,2048);xfs_btree_set_numrecs(left, --lrecs);
	xfs_btree_log_block(cur, lbp, XFS_BB_NUMRECS);

	xfs_btree_set_numrecs(right, ++rrecs);
	xfs_btree_log_block(cur, rbp, XFS_BB_NUMRECS);

	/*
	 * Using a temporary cursor, update the parent key values of the
	 * block on the right.
	 */
	error = xfs_btree_dup_cursor(cur, &tcur);
	Din_Go(404,2048);if (error)
		{/*141*/Din_Go(403,2048);goto error0;/*142*/}
	Din_Go(405,2048);i = xfs_btree_lastrec(tcur, level);
	XFS_WANT_CORRUPTED_GOTO(cur->bc_mp, i == 1, error0);

	error = xfs_btree_increment(tcur, level, &i);
	Din_Go(407,2048);if (error)
		{/*143*/Din_Go(406,2048);goto error1;/*144*/}

	Din_Go(408,2048);error = xfs_btree_updkey(tcur, rkp, level + 1);
	Din_Go(410,2048);if (error)
		{/*145*/Din_Go(409,2048);goto error1;/*146*/}

	Din_Go(411,2048);xfs_btree_del_cursor(tcur, XFS_BTREE_NOERROR);

	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	*stat = 1;
	{int  ReplaceReturn = 0; Din_Go(412,2048); return ReplaceReturn;}

out0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	*stat = 0;
	return 0;

error0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_ERROR);
	return error;

error1:
	XFS_BTREE_TRACE_CURSOR(tcur, XBT_ERROR);
	xfs_btree_del_cursor(tcur, XFS_BTREE_ERROR);
	return error;
}

/*
 * Split cur/level block in half.
 * Return new block number and the key to its first
 * record (to be inserted into parent).
 */
STATIC int					/* error */
__xfs_btree_split(
	struct xfs_btree_cur	*cur,
	int			level,
	union xfs_btree_ptr	*ptrp,
	union xfs_btree_key	*key,
	struct xfs_btree_cur	**curp,
	int			*stat)		/* success/failure */
{
	Din_Go(413,2048);union xfs_btree_ptr	lptr;		/* left sibling block ptr */
	struct xfs_buf		*lbp;		/* left buffer pointer */
	struct xfs_btree_block	*left;		/* left btree block */
	union xfs_btree_ptr	rptr;		/* right sibling block ptr */
	struct xfs_buf		*rbp;		/* right buffer pointer */
	struct xfs_btree_block	*right;		/* right btree block */
	union xfs_btree_ptr	rrptr;		/* right-right sibling ptr */
	struct xfs_buf		*rrbp;		/* right-right buffer pointer */
	struct xfs_btree_block	*rrblock;	/* right-right btree block */
	int			lrecs;
	int			rrecs;
	int			src_index;
	int			error;		/* error return value */
#ifdef DEBUG
	int			i;
#endif

	XFS_BTREE_TRACE_CURSOR(cur, XBT_ENTRY);
	XFS_BTREE_TRACE_ARGIPK(cur, level, *ptrp, key);

	XFS_BTREE_STATS_INC(cur, split);

	/* Set up left block (current one). */
	left = xfs_btree_get_block(cur, level, &lbp);

#ifdef DEBUG
	error = xfs_btree_check_block(cur, left, level, lbp);
	if (error)
		goto error0;
#endif

	xfs_btree_buf_to_ptr(cur, lbp, &lptr);

	/* Allocate the new block. If we can't do it, we're toast. Give up. */
	error = cur->bc_ops->alloc_block(cur, &lptr, &rptr, stat);
	Din_Go(415,2048);if (error)
		{/*147*/Din_Go(414,2048);goto error0;/*148*/}
	Din_Go(417,2048);if (*stat == 0)
		{/*149*/Din_Go(416,2048);goto out0;/*150*/}
	XFS_BTREE_STATS_INC(cur, alloc);

	/* Set up the new block as "right". */
	Din_Go(418,2048);error = xfs_btree_get_buf_block(cur, &rptr, 0, &right, &rbp);
	Din_Go(420,2048);if (error)
		{/*151*/Din_Go(419,2048);goto error0;/*152*/}

	/* Fill in the btree header for the new right block. */
	Din_Go(421,2048);xfs_btree_init_block_cur(cur, rbp, xfs_btree_get_level(left), 0);

	/*
	 * Split the entries between the old and the new block evenly.
	 * Make sure that if there's an odd number of entries now, that
	 * each new block will have the same number of entries.
	 */
	lrecs = xfs_btree_get_numrecs(left);
	rrecs = lrecs / 2;
	Din_Go(423,2048);if ((lrecs & 1) && cur->bc_ptrs[level] <= rrecs + 1)
		{/*153*/Din_Go(422,2048);rrecs++;/*154*/}
	Din_Go(424,2048);src_index = (lrecs - rrecs + 1);

	XFS_BTREE_STATS_ADD(cur, moves, rrecs);

	/*
	 * Copy btree block entries from the left block over to the
	 * new block, the right. Update the right block and log the
	 * changes.
	 */
	Din_Go(427,2048);if (level > 0) {
		/* It's a non-leaf.  Move keys and pointers. */
		Din_Go(425,2048);union xfs_btree_key	*lkp;	/* left btree key */
		union xfs_btree_ptr	*lpp;	/* left address pointer */
		union xfs_btree_key	*rkp;	/* right btree key */
		union xfs_btree_ptr	*rpp;	/* right address pointer */

		lkp = xfs_btree_key_addr(cur, src_index, left);
		lpp = xfs_btree_ptr_addr(cur, src_index, left);
		rkp = xfs_btree_key_addr(cur, 1, right);
		rpp = xfs_btree_ptr_addr(cur, 1, right);

#ifdef DEBUG
		for (i = src_index; i < rrecs; i++) {
			error = xfs_btree_check_ptr(cur, lpp, i, level);
			if (error)
				goto error0;
		}
#endif

		xfs_btree_copy_keys(cur, rkp, lkp, rrecs);
		xfs_btree_copy_ptrs(cur, rpp, lpp, rrecs);

		xfs_btree_log_keys(cur, rbp, 1, rrecs);
		xfs_btree_log_ptrs(cur, rbp, 1, rrecs);

		/* Grab the keys to the entries moved to the right block */
		xfs_btree_copy_keys(cur, key, rkp, 1);
	} else {
		/* It's a leaf.  Move records.  */
		Din_Go(426,2048);union xfs_btree_rec	*lrp;	/* left record pointer */
		union xfs_btree_rec	*rrp;	/* right record pointer */

		lrp = xfs_btree_rec_addr(cur, src_index, left);
		rrp = xfs_btree_rec_addr(cur, 1, right);

		xfs_btree_copy_recs(cur, rrp, lrp, rrecs);
		xfs_btree_log_recs(cur, rbp, 1, rrecs);

		cur->bc_ops->init_key_from_rec(key,
			xfs_btree_rec_addr(cur, 1, right));
	}


	/*
	 * Find the left block number by looking in the buffer.
	 * Adjust numrecs, sibling pointers.
	 */
	Din_Go(428,2048);xfs_btree_get_sibling(cur, left, &rrptr, XFS_BB_RIGHTSIB);
	xfs_btree_set_sibling(cur, right, &rrptr, XFS_BB_RIGHTSIB);
	xfs_btree_set_sibling(cur, right, &lptr, XFS_BB_LEFTSIB);
	xfs_btree_set_sibling(cur, left, &rptr, XFS_BB_RIGHTSIB);

	lrecs -= rrecs;
	xfs_btree_set_numrecs(left, lrecs);
	xfs_btree_set_numrecs(right, xfs_btree_get_numrecs(right) + rrecs);

	xfs_btree_log_block(cur, rbp, XFS_BB_ALL_BITS);
	xfs_btree_log_block(cur, lbp, XFS_BB_NUMRECS | XFS_BB_RIGHTSIB);

	/*
	 * If there's a block to the new block's right, make that block
	 * point back to right instead of to left.
	 */
	Din_Go(433,2048);if (!xfs_btree_ptr_is_null(cur, &rrptr)) {
		Din_Go(429,2048);error = xfs_btree_read_buf_block(cur, &rrptr,
							0, &rrblock, &rrbp);
		Din_Go(431,2048);if (error)
			{/*155*/Din_Go(430,2048);goto error0;/*156*/}
		Din_Go(432,2048);xfs_btree_set_sibling(cur, rrblock, &rptr, XFS_BB_LEFTSIB);
		xfs_btree_log_block(cur, rrbp, XFS_BB_LEFTSIB);
	}
	/*
	 * If the cursor is really in the right block, move it there.
	 * If it's just pointing past the last entry in left, then we'll
	 * insert there, so don't change anything in that case.
	 */
	Din_Go(435,2048);if (cur->bc_ptrs[level] > lrecs + 1) {
		Din_Go(434,2048);xfs_btree_setbuf(cur, level, rbp);
		cur->bc_ptrs[level] -= lrecs;
	}
	/*
	 * If there are more levels, we'll need another cursor which refers
	 * the right block, no matter where this cursor was.
	 */
	Din_Go(440,2048);if (level + 1 < cur->bc_nlevels) {
		Din_Go(436,2048);error = xfs_btree_dup_cursor(cur, curp);
		Din_Go(438,2048);if (error)
			{/*157*/Din_Go(437,2048);goto error0;/*158*/}
		Din_Go(439,2048);(*curp)->bc_ptrs[level + 1]++;
	}
	Din_Go(441,2048);*ptrp = rptr;
	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	*stat = 1;
	{int  ReplaceReturn = 0; Din_Go(442,2048); return ReplaceReturn;}
out0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	*stat = 0;
	return 0;

error0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_ERROR);
	return error;
}

#ifdef __KERNEL__
struct xfs_btree_split_args {
	struct xfs_btree_cur	*cur;
	int			level;
	union xfs_btree_ptr	*ptrp;
	union xfs_btree_key	*key;
	struct xfs_btree_cur	**curp;
	int			*stat;		/* success/failure */
	int			result;
	bool			kswapd;	/* allocation in kswapd context */
	struct completion	*done;
	struct work_struct	work;
};

/*
 * Stack switching interfaces for allocation
 */
static void
xfs_btree_split_worker(
	struct work_struct	*work)
{
	struct xfs_btree_split_args	*args = container_of(work,
						struct xfs_btree_split_args, work);
	unsigned long		pflags;
	unsigned long		new_pflags = PF_FSTRANS;

	/*
	 * we are in a transaction context here, but may also be doing work
	 * in kswapd context, and hence we may need to inherit that state
	 * temporarily to ensure that we don't block waiting for memory reclaim
	 * in any way.
	 */
	if (args->kswapd)
		new_pflags |= PF_MEMALLOC | PF_SWAPWRITE | PF_KSWAPD;

	current_set_flags_nested(&pflags, new_pflags);

	args->result = __xfs_btree_split(args->cur, args->level, args->ptrp,
					 args->key, args->curp, args->stat);
	complete(args->done);

	current_restore_flags_nested(&pflags, new_pflags);
}

/*
 * BMBT split requests often come in with little stack to work on. Push
 * them off to a worker thread so there is lots of stack to use. For the other
 * btree types, just call directly to avoid the context switch overhead here.
 */
STATIC int					/* error */
xfs_btree_split(
	struct xfs_btree_cur	*cur,
	int			level,
	union xfs_btree_ptr	*ptrp,
	union xfs_btree_key	*key,
	struct xfs_btree_cur	**curp,
	int			*stat)		/* success/failure */
{
	struct xfs_btree_split_args	args;
	DECLARE_COMPLETION_ONSTACK(done);

	if (cur->bc_btnum != XFS_BTNUM_BMAP)
		return __xfs_btree_split(cur, level, ptrp, key, curp, stat);

	args.cur = cur;
	args.level = level;
	args.ptrp = ptrp;
	args.key = key;
	args.curp = curp;
	args.stat = stat;
	args.done = &done;
	args.kswapd = current_is_kswapd();
	INIT_WORK_ONSTACK(&args.work, xfs_btree_split_worker);
	queue_work(xfs_alloc_wq, &args.work);
	wait_for_completion(&done);
	destroy_work_on_stack(&args.work);
	return args.result;
}
#else /* !KERNEL */
#define xfs_btree_split	__xfs_btree_split
#endif


/*
 * Copy the old inode root contents into a real block and make the
 * broot point to it.
 */
int						/* error */
xfs_btree_new_iroot(
	struct xfs_btree_cur	*cur,		/* btree cursor */
	int			*logflags,	/* logging flags for inode */
	int			*stat)		/* return status - 0 fail */
{
	Din_Go(443,2048);struct xfs_buf		*cbp;		/* buffer for cblock */
	struct xfs_btree_block	*block;		/* btree block */
	struct xfs_btree_block	*cblock;	/* child btree block */
	union xfs_btree_key	*ckp;		/* child key pointer */
	union xfs_btree_ptr	*cpp;		/* child ptr pointer */
	union xfs_btree_key	*kp;		/* pointer to btree key */
	union xfs_btree_ptr	*pp;		/* pointer to block addr */
	union xfs_btree_ptr	nptr;		/* new block addr */
	int			level;		/* btree level */
	int			error;		/* error return code */
#ifdef DEBUG
	int			i;		/* loop counter */
#endif

	XFS_BTREE_TRACE_CURSOR(cur, XBT_ENTRY);
	XFS_BTREE_STATS_INC(cur, newroot);

	ASSERT(cur->bc_flags & XFS_BTREE_ROOT_IN_INODE);

	level = cur->bc_nlevels - 1;

	block = xfs_btree_get_iroot(cur);
	pp = xfs_btree_ptr_addr(cur, 1, block);

	/* Allocate the new block. If we can't do it, we're toast. Give up. */
	error = cur->bc_ops->alloc_block(cur, pp, &nptr, stat);
	Din_Go(445,2048);if (error)
		{/*59*/Din_Go(444,2048);goto error0;/*60*/}
	Din_Go(447,2048);if (*stat == 0) {
		XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
		{int  ReplaceReturn = 0; Din_Go(446,2048); return ReplaceReturn;}
	}
	XFS_BTREE_STATS_INC(cur, alloc);

	/* Copy the root into a real block. */
	Din_Go(448,2048);error = xfs_btree_get_buf_block(cur, &nptr, 0, &cblock, &cbp);
	Din_Go(450,2048);if (error)
		{/*61*/Din_Go(449,2048);goto error0;/*62*/}

	/*
	 * we can't just memcpy() the root in for CRC enabled btree blocks.
	 * In that case have to also ensure the blkno remains correct
	 */
	Din_Go(451,2048);memcpy(cblock, block, xfs_btree_block_len(cur));
	Din_Go(453,2048);if (cur->bc_flags & XFS_BTREE_CRC_BLOCKS) {
		Din_Go(452,2048);if (cur->bc_flags & XFS_BTREE_LONG_PTRS)
			cblock->bb_u.l.bb_blkno = cpu_to_be64(cbp->b_bn);
		else
			cblock->bb_u.s.bb_blkno = cpu_to_be64(cbp->b_bn);
	}

	Din_Go(454,2048);be16_add_cpu(&block->bb_level, 1);
	xfs_btree_set_numrecs(block, 1);
	cur->bc_nlevels++;
	cur->bc_ptrs[level + 1] = 1;

	kp = xfs_btree_key_addr(cur, 1, block);
	ckp = xfs_btree_key_addr(cur, 1, cblock);
	xfs_btree_copy_keys(cur, ckp, kp, xfs_btree_get_numrecs(cblock));

	cpp = xfs_btree_ptr_addr(cur, 1, cblock);
#ifdef DEBUG
	for (i = 0; i < be16_to_cpu(cblock->bb_numrecs); i++) {
		error = xfs_btree_check_ptr(cur, pp, i, level);
		if (error)
			goto error0;
	}
#endif
	xfs_btree_copy_ptrs(cur, cpp, pp, xfs_btree_get_numrecs(cblock));

#ifdef DEBUG
	error = xfs_btree_check_ptr(cur, &nptr, 0, level);
	if (error)
		goto error0;
#endif
	xfs_btree_copy_ptrs(cur, pp, &nptr, 1);

	xfs_iroot_realloc(cur->bc_private.b.ip,
			  1 - xfs_btree_get_numrecs(cblock),
			  cur->bc_private.b.whichfork);

	xfs_btree_setbuf(cur, level, cbp);

	/*
	 * Do all this logging at the end so that
	 * the root is at the right level.
	 */
	xfs_btree_log_block(cur, cbp, XFS_BB_ALL_BITS);
	xfs_btree_log_keys(cur, cbp, 1, be16_to_cpu(cblock->bb_numrecs));
	xfs_btree_log_ptrs(cur, cbp, 1, be16_to_cpu(cblock->bb_numrecs));

	*logflags |=
		XFS_ILOG_CORE | xfs_ilog_fbroot(cur->bc_private.b.whichfork);
	*stat = 1;
	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	{int  ReplaceReturn = 0; Din_Go(455,2048); return ReplaceReturn;}
error0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_ERROR);
	return error;
}

/*
 * Allocate a new root block, fill it in.
 */
STATIC int				/* error */
xfs_btree_new_root(
	struct xfs_btree_cur	*cur,	/* btree cursor */
	int			*stat)	/* success/failure */
{
	Din_Go(456,2048);struct xfs_btree_block	*block;	/* one half of the old root block */
	struct xfs_buf		*bp;	/* buffer containing block */
	int			error;	/* error return value */
	struct xfs_buf		*lbp;	/* left buffer pointer */
	struct xfs_btree_block	*left;	/* left btree block */
	struct xfs_buf		*nbp;	/* new (root) buffer */
	struct xfs_btree_block	*new;	/* new (root) btree block */
	int			nptr;	/* new value for key index, 1 or 2 */
	struct xfs_buf		*rbp;	/* right buffer pointer */
	struct xfs_btree_block	*right;	/* right btree block */
	union xfs_btree_ptr	rptr;
	union xfs_btree_ptr	lptr;

	XFS_BTREE_TRACE_CURSOR(cur, XBT_ENTRY);
	XFS_BTREE_STATS_INC(cur, newroot);

	/* initialise our start point from the cursor */
	cur->bc_ops->init_ptr_from_cur(cur, &rptr);

	/* Allocate the new block. If we can't do it, we're toast. Give up. */
	error = cur->bc_ops->alloc_block(cur, &rptr, &lptr, stat);
	Din_Go(458,2048);if (error)
		{/*159*/Din_Go(457,2048);goto error0;/*160*/}
	Din_Go(460,2048);if (*stat == 0)
		{/*161*/Din_Go(459,2048);goto out0;/*162*/}
	XFS_BTREE_STATS_INC(cur, alloc);

	/* Set up the new block. */
	Din_Go(461,2048);error = xfs_btree_get_buf_block(cur, &lptr, 0, &new, &nbp);
	Din_Go(463,2048);if (error)
		{/*163*/Din_Go(462,2048);goto error0;/*164*/}

	/* Set the root in the holding structure  increasing the level by 1. */
	Din_Go(464,2048);cur->bc_ops->set_root(cur, &lptr, 1);

	/*
	 * At the previous root level there are now two blocks: the old root,
	 * and the new block generated when it was split.  We don't know which
	 * one the cursor is pointing at, so we set up variables "left" and
	 * "right" for each case.
	 */
	block = xfs_btree_get_block(cur, cur->bc_nlevels - 1, &bp);

#ifdef DEBUG
	error = xfs_btree_check_block(cur, block, cur->bc_nlevels - 1, bp);
	if (error)
		goto error0;
#endif

	xfs_btree_get_sibling(cur, block, &rptr, XFS_BB_RIGHTSIB);
	Din_Go(473,2048);if (!xfs_btree_ptr_is_null(cur, &rptr)) {
		/* Our block is left, pick up the right block. */
		Din_Go(465,2048);lbp = bp;
		xfs_btree_buf_to_ptr(cur, lbp, &lptr);
		left = block;
		error = xfs_btree_read_buf_block(cur, &rptr, 0, &right, &rbp);
		Din_Go(467,2048);if (error)
			{/*165*/Din_Go(466,2048);goto error0;/*166*/}
		Din_Go(468,2048);bp = rbp;
		nptr = 1;
	} else {
		/* Our block is right, pick up the left block. */
		Din_Go(469,2048);rbp = bp;
		xfs_btree_buf_to_ptr(cur, rbp, &rptr);
		right = block;
		xfs_btree_get_sibling(cur, right, &lptr, XFS_BB_LEFTSIB);
		error = xfs_btree_read_buf_block(cur, &lptr, 0, &left, &lbp);
		Din_Go(471,2048);if (error)
			{/*167*/Din_Go(470,2048);goto error0;/*168*/}
		Din_Go(472,2048);bp = lbp;
		nptr = 2;
	}
	/* Fill in the new block's btree header and log it. */
	Din_Go(474,2048);xfs_btree_init_block_cur(cur, nbp, cur->bc_nlevels, 2);
	xfs_btree_log_block(cur, nbp, XFS_BB_ALL_BITS);
	ASSERT(!xfs_btree_ptr_is_null(cur, &lptr) &&
			!xfs_btree_ptr_is_null(cur, &rptr));

	/* Fill in the key data in the new root. */
	Din_Go(477,2048);if (xfs_btree_get_level(left) > 0) {
		Din_Go(475,2048);xfs_btree_copy_keys(cur,
				xfs_btree_key_addr(cur, 1, new),
				xfs_btree_key_addr(cur, 1, left), 1);
		xfs_btree_copy_keys(cur,
				xfs_btree_key_addr(cur, 2, new),
				xfs_btree_key_addr(cur, 1, right), 1);
	} else {
		Din_Go(476,2048);cur->bc_ops->init_key_from_rec(
				xfs_btree_key_addr(cur, 1, new),
				xfs_btree_rec_addr(cur, 1, left));
		cur->bc_ops->init_key_from_rec(
				xfs_btree_key_addr(cur, 2, new),
				xfs_btree_rec_addr(cur, 1, right));
	}
	Din_Go(478,2048);xfs_btree_log_keys(cur, nbp, 1, 2);

	/* Fill in the pointer data in the new root. */
	xfs_btree_copy_ptrs(cur,
		xfs_btree_ptr_addr(cur, 1, new), &lptr, 1);
	xfs_btree_copy_ptrs(cur,
		xfs_btree_ptr_addr(cur, 2, new), &rptr, 1);
	xfs_btree_log_ptrs(cur, nbp, 1, 2);

	/* Fix up the cursor. */
	xfs_btree_setbuf(cur, cur->bc_nlevels, nbp);
	cur->bc_ptrs[cur->bc_nlevels] = nptr;
	cur->bc_nlevels++;
	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	*stat = 1;
	{int  ReplaceReturn = 0; Din_Go(479,2048); return ReplaceReturn;}
error0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_ERROR);
	return error;
out0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	*stat = 0;
	return 0;
}

STATIC int
xfs_btree_make_block_unfull(
	struct xfs_btree_cur	*cur,	/* btree cursor */
	int			level,	/* btree level */
	int			numrecs,/* # of recs in block */
	int			*oindex,/* old tree index */
	int			*index,	/* new tree index */
	union xfs_btree_ptr	*nptr,	/* new btree ptr */
	struct xfs_btree_cur	**ncur,	/* new btree cursor */
	union xfs_btree_rec	*nrec,	/* new record */
	int			*stat)
{
	Din_Go(480,2048);union xfs_btree_key	key;	/* new btree key value */
	int			error = 0;

	Din_Go(488,2048);if ((cur->bc_flags & XFS_BTREE_ROOT_IN_INODE) &&
	    level == cur->bc_nlevels - 1) {
	    	Din_Go(481,2048);struct xfs_inode *ip = cur->bc_private.b.ip;

		Din_Go(486,2048);if (numrecs < cur->bc_ops->get_dmaxrecs(cur, level)) {
			/* A root block that can be made bigger. */
			Din_Go(482,2048);xfs_iroot_realloc(ip, 1, cur->bc_private.b.whichfork);
		} else {
			/* A root block that needs replacing */
			Din_Go(483,2048);int	logflags = 0;

			error = xfs_btree_new_iroot(cur, &logflags, stat);
			Din_Go(485,2048);if (error || *stat == 0)
				{/*169*/{int  ReplaceReturn = error; Din_Go(484,2048); return ReplaceReturn;}/*170*/}

			xfs_trans_log_inode(cur->bc_tp, ip, logflags);
		}

		{int  ReplaceReturn = 0; Din_Go(487,2048); return ReplaceReturn;}
	}

	/* First, try shifting an entry to the right neighbor. */
	Din_Go(489,2048);error = xfs_btree_rshift(cur, level, stat);
	Din_Go(491,2048);if (error || *stat)
		{/*171*/{int  ReplaceReturn = error; Din_Go(490,2048); return ReplaceReturn;}/*172*/}

	/* Next, try shifting an entry to the left neighbor. */
	Din_Go(492,2048);error = xfs_btree_lshift(cur, level, stat);
	Din_Go(494,2048);if (error)
		{/*173*/{int  ReplaceReturn = error; Din_Go(493,2048); return ReplaceReturn;}/*174*/}

	Din_Go(497,2048);if (*stat) {
		Din_Go(495,2048);*oindex = *index = cur->bc_ptrs[level];
		{int  ReplaceReturn = 0; Din_Go(496,2048); return ReplaceReturn;}
	}

	/*
	 * Next, try splitting the current block in half.
	 *
	 * If this works we have to re-set our variables because we
	 * could be in a different block now.
	 */
	Din_Go(498,2048);error = xfs_btree_split(cur, level, nptr, &key, ncur, stat);
	Din_Go(500,2048);if (error || *stat == 0)
		{/*175*/{int  ReplaceReturn = error; Din_Go(499,2048); return ReplaceReturn;}/*176*/}


	Din_Go(501,2048);*index = cur->bc_ptrs[level];
	cur->bc_ops->init_rec_from_key(&key, nrec);
	{int  ReplaceReturn = 0; Din_Go(502,2048); return ReplaceReturn;}
}

/*
 * Insert one record/level.  Return information to the caller
 * allowing the next level up to proceed if necessary.
 */
STATIC int
xfs_btree_insrec(
	struct xfs_btree_cur	*cur,	/* btree cursor */
	int			level,	/* level to insert record at */
	union xfs_btree_ptr	*ptrp,	/* i/o: block number inserted */
	union xfs_btree_rec	*recp,	/* i/o: record data inserted */
	struct xfs_btree_cur	**curp,	/* output: new cursor replacing cur */
	int			*stat)	/* success/failure */
{
	Din_Go(503,2048);struct xfs_btree_block	*block;	/* btree block */
	struct xfs_buf		*bp;	/* buffer for block */
	union xfs_btree_key	key;	/* btree key */
	union xfs_btree_ptr	nptr;	/* new block ptr */
	struct xfs_btree_cur	*ncur;	/* new btree cursor */
	union xfs_btree_rec	nrec;	/* new record count */
	int			optr;	/* old key/record index */
	int			ptr;	/* key/record index */
	int			numrecs;/* number of records */
	int			error;	/* error return value */
#ifdef DEBUG
	int			i;
#endif

	XFS_BTREE_TRACE_CURSOR(cur, XBT_ENTRY);
	XFS_BTREE_TRACE_ARGIPR(cur, level, *ptrp, recp);

	ncur = NULL;

	/*
	 * If we have an external root pointer, and we've made it to the
	 * root level, allocate a new root block and we're done.
	 */
	Din_Go(506,2048);if (!(cur->bc_flags & XFS_BTREE_ROOT_IN_INODE) &&
	    (level >= cur->bc_nlevels)) {
		Din_Go(504,2048);error = xfs_btree_new_root(cur, stat);
		xfs_btree_set_ptr_null(cur, ptrp);

		XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
		{int  ReplaceReturn = error; Din_Go(505,2048); return ReplaceReturn;}
	}

	/* If we're off the left edge, return failure. */
	Din_Go(507,2048);ptr = cur->bc_ptrs[level];
	Din_Go(510,2048);if (ptr == 0) {
		XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
		Din_Go(508,2048);*stat = 0;
		{int  ReplaceReturn = 0; Din_Go(509,2048); return ReplaceReturn;}
	}

	/* Make a key out of the record data to be inserted, and save it. */
	Din_Go(511,2048);cur->bc_ops->init_key_from_rec(&key, recp);

	optr = ptr;

	XFS_BTREE_STATS_INC(cur, insrec);

	/* Get pointers to the btree buffer and block. */
	block = xfs_btree_get_block(cur, level, &bp);
	numrecs = xfs_btree_get_numrecs(block);

#ifdef DEBUG
	error = xfs_btree_check_block(cur, block, level, bp);
	if (error)
		goto error0;

	/* Check that the new entry is being inserted in the right place. */
	if (ptr <= numrecs) {
		if (level == 0) {
			ASSERT(cur->bc_ops->recs_inorder(cur, recp,
				xfs_btree_rec_addr(cur, ptr, block)));
		} else {
			ASSERT(cur->bc_ops->keys_inorder(cur, &key,
				xfs_btree_key_addr(cur, ptr, block)));
		}
	}
#endif

	/*
	 * If the block is full, we can't insert the new entry until we
	 * make the block un-full.
	 */
	xfs_btree_set_ptr_null(cur, &nptr);
	Din_Go(515,2048);if (numrecs == cur->bc_ops->get_maxrecs(cur, level)) {
		Din_Go(512,2048);error = xfs_btree_make_block_unfull(cur, level, numrecs,
					&optr, &ptr, &nptr, &ncur, &nrec, stat);
		Din_Go(514,2048);if (error || *stat == 0)
			{/*177*/Din_Go(513,2048);goto error0;/*178*/}
	}

	/*
	 * The current block may have changed if the block was
	 * previously full and we have just made space in it.
	 */
	Din_Go(516,2048);block = xfs_btree_get_block(cur, level, &bp);
	numrecs = xfs_btree_get_numrecs(block);

#ifdef DEBUG
	error = xfs_btree_check_block(cur, block, level, bp);
	if (error)
		return error;
#endif

	/*
	 * At this point we know there's room for our new entry in the block
	 * we're pointing at.
	 */
	XFS_BTREE_STATS_ADD(cur, moves, numrecs - ptr + 1);

	Din_Go(519,2048);if (level > 0) {
		/* It's a nonleaf. make a hole in the keys and ptrs */
		Din_Go(517,2048);union xfs_btree_key	*kp;
		union xfs_btree_ptr	*pp;

		kp = xfs_btree_key_addr(cur, ptr, block);
		pp = xfs_btree_ptr_addr(cur, ptr, block);

#ifdef DEBUG
		for (i = numrecs - ptr; i >= 0; i--) {
			error = xfs_btree_check_ptr(cur, pp, i, level);
			if (error)
				return error;
		}
#endif

		xfs_btree_shift_keys(cur, kp, 1, numrecs - ptr + 1);
		xfs_btree_shift_ptrs(cur, pp, 1, numrecs - ptr + 1);

#ifdef DEBUG
		error = xfs_btree_check_ptr(cur, ptrp, 0, level);
		if (error)
			goto error0;
#endif

		/* Now put the new data in, bump numrecs and log it. */
		xfs_btree_copy_keys(cur, kp, &key, 1);
		xfs_btree_copy_ptrs(cur, pp, ptrp, 1);
		numrecs++;
		xfs_btree_set_numrecs(block, numrecs);
		xfs_btree_log_ptrs(cur, bp, ptr, numrecs);
		xfs_btree_log_keys(cur, bp, ptr, numrecs);
#ifdef DEBUG
		if (ptr < numrecs) {
			ASSERT(cur->bc_ops->keys_inorder(cur, kp,
				xfs_btree_key_addr(cur, ptr + 1, block)));
		}
#endif
	} else {
		/* It's a leaf. make a hole in the records */
		Din_Go(518,2048);union xfs_btree_rec             *rp;

		rp = xfs_btree_rec_addr(cur, ptr, block);

		xfs_btree_shift_recs(cur, rp, 1, numrecs - ptr + 1);

		/* Now put the new data in, bump numrecs and log it. */
		xfs_btree_copy_recs(cur, rp, recp, 1);
		xfs_btree_set_numrecs(block, ++numrecs);
		xfs_btree_log_recs(cur, bp, ptr, numrecs);
#ifdef DEBUG
		if (ptr < numrecs) {
			ASSERT(cur->bc_ops->recs_inorder(cur, rp,
				xfs_btree_rec_addr(cur, ptr + 1, block)));
		}
#endif
	}

	/* Log the new number of records in the btree header. */
	Din_Go(520,2048);xfs_btree_log_block(cur, bp, XFS_BB_NUMRECS);

	/* If we inserted at the start of a block, update the parents' keys. */
	Din_Go(524,2048);if (optr == 1) {
		Din_Go(521,2048);error = xfs_btree_updkey(cur, &key, level + 1);
		Din_Go(523,2048);if (error)
			{/*179*/Din_Go(522,2048);goto error0;/*180*/}
	}

	/*
	 * If we are tracking the last record in the tree and
	 * we are at the far right edge of the tree, update it.
	 */
	Din_Go(526,2048);if (xfs_btree_is_lastrec(cur, block, level)) {
		Din_Go(525,2048);cur->bc_ops->update_lastrec(cur, block, recp,
					    ptr, LASTREC_INSREC);
	}

	/*
	 * Return the new block number, if any.
	 * If there is one, give back a record value and a cursor too.
	 */
	Din_Go(527,2048);*ptrp = nptr;
	Din_Go(529,2048);if (!xfs_btree_ptr_is_null(cur, &nptr)) {
		Din_Go(528,2048);*recp = nrec;
		*curp = ncur;
	}

	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	Din_Go(530,2048);*stat = 1;
	{int  ReplaceReturn = 0; Din_Go(531,2048); return ReplaceReturn;}

error0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_ERROR);
	return error;
}

/*
 * Insert the record at the point referenced by cur.
 *
 * A multi-level split of the tree on insert will invalidate the original
 * cursor.  All callers of this function should assume that the cursor is
 * no longer valid and revalidate it.
 */
int
xfs_btree_insert(
	struct xfs_btree_cur	*cur,
	int			*stat)
{
	Din_Go(532,2048);int			error;	/* error return value */
	int			i;	/* result value, 0 for failure */
	int			level;	/* current level number in btree */
	union xfs_btree_ptr	nptr;	/* new block number (split result) */
	struct xfs_btree_cur	*ncur;	/* new cursor (split result) */
	struct xfs_btree_cur	*pcur;	/* previous level's cursor */
	union xfs_btree_rec	rec;	/* record to insert */

	level = 0;
	ncur = NULL;
	pcur = cur;

	xfs_btree_set_ptr_null(cur, &nptr);
	cur->bc_ops->init_rec_from_cur(cur, &rec);

	/*
	 * Loop going up the tree, starting at the leaf level.
	 * Stop when we don't get a split block, that must mean that
	 * the insert is finished with this level.
	 */
	Din_Go(545,2048);do {
		/*
		 * Insert nrec/nptr into this level of the tree.
		 * Note if we fail, nptr will be null.
		 */
		Din_Go(533,2048);error = xfs_btree_insrec(pcur, level, &nptr, &rec, &ncur, &i);
		Din_Go(537,2048);if (error) {
			Din_Go(534,2048);if (pcur != cur)
				{/*63*/Din_Go(535,2048);xfs_btree_del_cursor(pcur, XFS_BTREE_ERROR);/*64*/}
			Din_Go(536,2048);goto error0;
		}

		XFS_WANT_CORRUPTED_GOTO(cur->bc_mp, i == 1, error0);
		Din_Go(538,2048);level++;

		/*
		 * See if the cursor we just used is trash.
		 * Can't trash the caller's cursor, but otherwise we should
		 * if ncur is a new cursor or we're about to be done.
		 */
		Din_Go(542,2048);if (pcur != cur &&
		    (ncur || xfs_btree_ptr_is_null(cur, &nptr))) {
			/* Save the state from the cursor before we trash it */
			Din_Go(539,2048);if (cur->bc_ops->update_cursor)
				{/*65*/Din_Go(540,2048);cur->bc_ops->update_cursor(pcur, cur);/*66*/}
			Din_Go(541,2048);cur->bc_nlevels = pcur->bc_nlevels;
			xfs_btree_del_cursor(pcur, XFS_BTREE_NOERROR);
		}
		/* If we got a new cursor, switch to it. */
		Din_Go(544,2048);if (ncur) {
			Din_Go(543,2048);pcur = ncur;
			ncur = NULL;
		}
	} while (!xfs_btree_ptr_is_null(cur, &nptr));

	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	Din_Go(546,2048);*stat = i;
	{int  ReplaceReturn = 0; Din_Go(547,2048); return ReplaceReturn;}
error0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_ERROR);
	return error;
}

/*
 * Try to merge a non-leaf block back into the inode root.
 *
 * Note: the killroot names comes from the fact that we're effectively
 * killing the old root block.  But because we can't just delete the
 * inode we have to copy the single block it was pointing to into the
 * inode.
 */
STATIC int
xfs_btree_kill_iroot(
	struct xfs_btree_cur	*cur)
{
	Din_Go(548,2048);int			whichfork = cur->bc_private.b.whichfork;
	struct xfs_inode	*ip = cur->bc_private.b.ip;
	struct xfs_ifork	*ifp = XFS_IFORK_PTR(ip, whichfork);
	struct xfs_btree_block	*block;
	struct xfs_btree_block	*cblock;
	union xfs_btree_key	*kp;
	union xfs_btree_key	*ckp;
	union xfs_btree_ptr	*pp;
	union xfs_btree_ptr	*cpp;
	struct xfs_buf		*cbp;
	int			level;
	int			index;
	int			numrecs;
#ifdef DEBUG
	union xfs_btree_ptr	ptr;
	int			i;
#endif

	XFS_BTREE_TRACE_CURSOR(cur, XBT_ENTRY);

	ASSERT(cur->bc_flags & XFS_BTREE_ROOT_IN_INODE);
	ASSERT(cur->bc_nlevels > 1);

	/*
	 * Don't deal with the root block needs to be a leaf case.
	 * We're just going to turn the thing back into extents anyway.
	 */
	level = cur->bc_nlevels - 1;
	Din_Go(550,2048);if (level == 1)
		{/*181*/Din_Go(549,2048);goto out0;/*182*/}

	/*
	 * Give up if the root has multiple children.
	 */
	Din_Go(551,2048);block = xfs_btree_get_iroot(cur);
	Din_Go(553,2048);if (xfs_btree_get_numrecs(block) != 1)
		{/*183*/Din_Go(552,2048);goto out0;/*184*/}

	Din_Go(554,2048);cblock = xfs_btree_get_block(cur, level - 1, &cbp);
	numrecs = xfs_btree_get_numrecs(cblock);

	/*
	 * Only do this if the next level will fit.
	 * Then the data must be copied up to the inode,
	 * instead of freeing the root you free the next level.
	 */
	Din_Go(556,2048);if (numrecs > cur->bc_ops->get_dmaxrecs(cur, level))
		{/*185*/Din_Go(555,2048);goto out0;/*186*/}

	XFS_BTREE_STATS_INC(cur, killroot);

#ifdef DEBUG
	xfs_btree_get_sibling(cur, block, &ptr, XFS_BB_LEFTSIB);
	ASSERT(xfs_btree_ptr_is_null(cur, &ptr));
	xfs_btree_get_sibling(cur, block, &ptr, XFS_BB_RIGHTSIB);
	ASSERT(xfs_btree_ptr_is_null(cur, &ptr));
#endif

	Din_Go(557,2048);index = numrecs - cur->bc_ops->get_maxrecs(cur, level);
	Din_Go(559,2048);if (index) {
		Din_Go(558,2048);xfs_iroot_realloc(cur->bc_private.b.ip, index,
				  cur->bc_private.b.whichfork);
		block = ifp->if_broot;
	}

	Din_Go(560,2048);be16_add_cpu(&block->bb_numrecs, index);
	ASSERT(block->bb_numrecs == cblock->bb_numrecs);

	kp = xfs_btree_key_addr(cur, 1, block);
	ckp = xfs_btree_key_addr(cur, 1, cblock);
	xfs_btree_copy_keys(cur, kp, ckp, numrecs);

	pp = xfs_btree_ptr_addr(cur, 1, block);
	cpp = xfs_btree_ptr_addr(cur, 1, cblock);
#ifdef DEBUG
	for (i = 0; i < numrecs; i++) {
		int		error;

		error = xfs_btree_check_ptr(cur, cpp, i, level - 1);
		if (error) {
			XFS_BTREE_TRACE_CURSOR(cur, XBT_ERROR);
			return error;
		}
	}
#endif
	xfs_btree_copy_ptrs(cur, pp, cpp, numrecs);

	cur->bc_ops->free_block(cur, cbp);
	XFS_BTREE_STATS_INC(cur, free);

	cur->bc_bufs[level - 1] = NULL;
	be16_add_cpu(&block->bb_level, -1);
	xfs_trans_log_inode(cur->bc_tp, ip,
		XFS_ILOG_CORE | xfs_ilog_fbroot(cur->bc_private.b.whichfork));
	cur->bc_nlevels--;
out0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	{int  ReplaceReturn = 0; Din_Go(561,2048); return ReplaceReturn;}
}

/*
 * Kill the current root node, and replace it with it's only child node.
 */
STATIC int
xfs_btree_kill_root(
	struct xfs_btree_cur	*cur,
	struct xfs_buf		*bp,
	int			level,
	union xfs_btree_ptr	*newroot)
{
	Din_Go(562,2048);int			error;

	XFS_BTREE_TRACE_CURSOR(cur, XBT_ENTRY);
	XFS_BTREE_STATS_INC(cur, killroot);

	/*
	 * Update the root pointer, decreasing the level by 1 and then
	 * free the old root.
	 */
	cur->bc_ops->set_root(cur, newroot, -1);

	error = cur->bc_ops->free_block(cur, bp);
	Din_Go(564,2048);if (error) {
		XFS_BTREE_TRACE_CURSOR(cur, XBT_ERROR);
		{int  ReplaceReturn = error; Din_Go(563,2048); return ReplaceReturn;}
	}

	XFS_BTREE_STATS_INC(cur, free);

	Din_Go(565,2048);cur->bc_bufs[level] = NULL;
	cur->bc_ra[level] = 0;
	cur->bc_nlevels--;

	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	{int  ReplaceReturn = 0; Din_Go(566,2048); return ReplaceReturn;}
}

STATIC int
xfs_btree_dec_cursor(
	struct xfs_btree_cur	*cur,
	int			level,
	int			*stat)
{
	Din_Go(567,2048);int			error;
	int			i;

	Din_Go(571,2048);if (level > 0) {
		Din_Go(568,2048);error = xfs_btree_decrement(cur, level, &i);
		Din_Go(570,2048);if (error)
			{/*187*/{int  ReplaceReturn = error; Din_Go(569,2048); return ReplaceReturn;}/*188*/}
	}

	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	Din_Go(572,2048);*stat = 1;
	{int  ReplaceReturn = 0; Din_Go(573,2048); return ReplaceReturn;}
}

/*
 * Single level of the btree record deletion routine.
 * Delete record pointed to by cur/level.
 * Remove the record from its block then rebalance the tree.
 * Return 0 for error, 1 for done, 2 to go on to the next level.
 */
STATIC int					/* error */
xfs_btree_delrec(
	struct xfs_btree_cur	*cur,		/* btree cursor */
	int			level,		/* level removing record from */
	int			*stat)		/* fail/done/go-on */
{
	Din_Go(574,2048);struct xfs_btree_block	*block;		/* btree block */
	union xfs_btree_ptr	cptr;		/* current block ptr */
	struct xfs_buf		*bp;		/* buffer for block */
	int			error;		/* error return value */
	int			i;		/* loop counter */
	union xfs_btree_key	key;		/* storage for keyp */
	union xfs_btree_key	*keyp = &key;	/* passed to the next level */
	union xfs_btree_ptr	lptr;		/* left sibling block ptr */
	struct xfs_buf		*lbp;		/* left buffer pointer */
	struct xfs_btree_block	*left;		/* left btree block */
	int			lrecs = 0;	/* left record count */
	int			ptr;		/* key/record index */
	union xfs_btree_ptr	rptr;		/* right sibling block ptr */
	struct xfs_buf		*rbp;		/* right buffer pointer */
	struct xfs_btree_block	*right;		/* right btree block */
	struct xfs_btree_block	*rrblock;	/* right-right btree block */
	struct xfs_buf		*rrbp;		/* right-right buffer pointer */
	int			rrecs = 0;	/* right record count */
	struct xfs_btree_cur	*tcur;		/* temporary btree cursor */
	int			numrecs;	/* temporary numrec count */

	XFS_BTREE_TRACE_CURSOR(cur, XBT_ENTRY);
	XFS_BTREE_TRACE_ARGI(cur, level);

	tcur = NULL;

	/* Get the index of the entry being deleted, check for nothing there. */
	ptr = cur->bc_ptrs[level];
	Din_Go(577,2048);if (ptr == 0) {
		XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
		Din_Go(575,2048);*stat = 0;
		{int  ReplaceReturn = 0; Din_Go(576,2048); return ReplaceReturn;}
	}

	/* Get the buffer & block containing the record or key/ptr. */
	Din_Go(578,2048);block = xfs_btree_get_block(cur, level, &bp);
	numrecs = xfs_btree_get_numrecs(block);

#ifdef DEBUG
	error = xfs_btree_check_block(cur, block, level, bp);
	if (error)
		goto error0;
#endif

	/* Fail if we're off the end of the block. */
	Din_Go(581,2048);if (ptr > numrecs) {
		XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
		Din_Go(579,2048);*stat = 0;
		{int  ReplaceReturn = 0; Din_Go(580,2048); return ReplaceReturn;}
	}

	XFS_BTREE_STATS_INC(cur, delrec);
	XFS_BTREE_STATS_ADD(cur, moves, numrecs - ptr);

	/* Excise the entries being deleted. */
	Din_Go(591,2048);if (level > 0) {
		/* It's a nonleaf. operate on keys and ptrs */
		Din_Go(582,2048);union xfs_btree_key	*lkp;
		union xfs_btree_ptr	*lpp;

		lkp = xfs_btree_key_addr(cur, ptr + 1, block);
		lpp = xfs_btree_ptr_addr(cur, ptr + 1, block);

#ifdef DEBUG
		for (i = 0; i < numrecs - ptr; i++) {
			error = xfs_btree_check_ptr(cur, lpp, i, level);
			if (error)
				goto error0;
		}
#endif

		Din_Go(584,2048);if (ptr < numrecs) {
			Din_Go(583,2048);xfs_btree_shift_keys(cur, lkp, -1, numrecs - ptr);
			xfs_btree_shift_ptrs(cur, lpp, -1, numrecs - ptr);
			xfs_btree_log_keys(cur, bp, ptr, numrecs - 1);
			xfs_btree_log_ptrs(cur, bp, ptr, numrecs - 1);
		}

		/*
		 * If it's the first record in the block, we'll need to pass a
		 * key up to the next level (updkey).
		 */
		Din_Go(586,2048);if (ptr == 1)
			{/*189*/Din_Go(585,2048);keyp = xfs_btree_key_addr(cur, 1, block);/*190*/}
	} else {
		/* It's a leaf. operate on records */
		Din_Go(587,2048);if (ptr < numrecs) {
			Din_Go(588,2048);xfs_btree_shift_recs(cur,
				xfs_btree_rec_addr(cur, ptr + 1, block),
				-1, numrecs - ptr);
			xfs_btree_log_recs(cur, bp, ptr, numrecs - 1);
		}

		/*
		 * If it's the first record in the block, we'll need a key
		 * structure to pass up to the next level (updkey).
		 */
		Din_Go(590,2048);if (ptr == 1) {
			Din_Go(589,2048);cur->bc_ops->init_key_from_rec(&key,
					xfs_btree_rec_addr(cur, 1, block));
			keyp = &key;
		}
	}

	/*
	 * Decrement and log the number of entries in the block.
	 */
	Din_Go(592,2048);xfs_btree_set_numrecs(block, --numrecs);
	xfs_btree_log_block(cur, bp, XFS_BB_NUMRECS);

	/*
	 * If we are tracking the last record in the tree and
	 * we are at the far right edge of the tree, update it.
	 */
	Din_Go(594,2048);if (xfs_btree_is_lastrec(cur, block, level)) {
		Din_Go(593,2048);cur->bc_ops->update_lastrec(cur, block, NULL,
					    ptr, LASTREC_DELREC);
	}

	/*
	 * We're at the root level.  First, shrink the root block in-memory.
	 * Try to get rid of the next level down.  If we can't then there's
	 * nothing left to do.
	 */
	Din_Go(614,2048);if (level == cur->bc_nlevels - 1) {
		Din_Go(595,2048);if (cur->bc_flags & XFS_BTREE_ROOT_IN_INODE) {
			Din_Go(596,2048);xfs_iroot_realloc(cur->bc_private.b.ip, -1,
					  cur->bc_private.b.whichfork);

			error = xfs_btree_kill_iroot(cur);
			Din_Go(598,2048);if (error)
				{/*191*/Din_Go(597,2048);goto error0;/*192*/}

			Din_Go(599,2048);error = xfs_btree_dec_cursor(cur, level, stat);
			Din_Go(601,2048);if (error)
				{/*193*/Din_Go(600,2048);goto error0;/*194*/}
			Din_Go(602,2048);*stat = 1;
			{int  ReplaceReturn = 0; Din_Go(603,2048); return ReplaceReturn;}
		}

		/*
		 * If this is the root level, and there's only one entry left,
		 * and it's NOT the leaf level, then we can get rid of this
		 * level.
		 */
		Din_Go(611,2048);if (numrecs == 1 && level > 0) {
			Din_Go(604,2048);union xfs_btree_ptr	*pp;
			/*
			 * pp is still set to the first pointer in the block.
			 * Make it the new root of the btree.
			 */
			pp = xfs_btree_ptr_addr(cur, 1, block);
			error = xfs_btree_kill_root(cur, bp, level, pp);
			Din_Go(606,2048);if (error)
				{/*195*/Din_Go(605,2048);goto error0;/*196*/}
		} else {/*197*/Din_Go(607,2048);if (level > 0) {
			Din_Go(608,2048);error = xfs_btree_dec_cursor(cur, level, stat);
			Din_Go(610,2048);if (error)
				{/*199*/Din_Go(609,2048);goto error0;/*200*/}
		;/*198*/}}
		Din_Go(612,2048);*stat = 1;
		{int  ReplaceReturn = 0; Din_Go(613,2048); return ReplaceReturn;}
	}

	/*
	 * If we deleted the leftmost entry in the block, update the
	 * key values above us in the tree.
	 */
	Din_Go(618,2048);if (ptr == 1) {
		Din_Go(615,2048);error = xfs_btree_updkey(cur, keyp, level + 1);
		Din_Go(617,2048);if (error)
			{/*201*/Din_Go(616,2048);goto error0;/*202*/}
	}

	/*
	 * If the number of records remaining in the block is at least
	 * the minimum, we're done.
	 */
	Din_Go(623,2048);if (numrecs >= cur->bc_ops->get_minrecs(cur, level)) {
		Din_Go(619,2048);error = xfs_btree_dec_cursor(cur, level, stat);
		Din_Go(621,2048);if (error)
			{/*203*/Din_Go(620,2048);goto error0;/*204*/}
		{int  ReplaceReturn = 0; Din_Go(622,2048); return ReplaceReturn;}
	}

	/*
	 * Otherwise, we have to move some records around to keep the
	 * tree balanced.  Look at the left and right sibling blocks to
	 * see if we can re-balance by moving only one record.
	 */
	Din_Go(624,2048);xfs_btree_get_sibling(cur, block, &rptr, XFS_BB_RIGHTSIB);
	xfs_btree_get_sibling(cur, block, &lptr, XFS_BB_LEFTSIB);

	Din_Go(632,2048);if (cur->bc_flags & XFS_BTREE_ROOT_IN_INODE) {
		/*
		 * One child of root, need to get a chance to copy its contents
		 * into the root and delete it. Can't go up to next level,
		 * there's nothing to delete there.
		 */
		Din_Go(625,2048);if (xfs_btree_ptr_is_null(cur, &rptr) &&
		    xfs_btree_ptr_is_null(cur, &lptr) &&
		    level == cur->bc_nlevels - 2) {
			Din_Go(626,2048);error = xfs_btree_kill_iroot(cur);
			Din_Go(628,2048);if (!error)
				{/*205*/Din_Go(627,2048);error = xfs_btree_dec_cursor(cur, level, stat);/*206*/}
			Din_Go(630,2048);if (error)
				{/*207*/Din_Go(629,2048);goto error0;/*208*/}
			{int  ReplaceReturn = 0; Din_Go(631,2048); return ReplaceReturn;}
		}
	}

	ASSERT(!xfs_btree_ptr_is_null(cur, &rptr) ||
	       !xfs_btree_ptr_is_null(cur, &lptr));

	/*
	 * Duplicate the cursor so our btree manipulations here won't
	 * disrupt the next level up.
	 */
	Din_Go(633,2048);error = xfs_btree_dup_cursor(cur, &tcur);
	Din_Go(635,2048);if (error)
		{/*209*/Din_Go(634,2048);goto error0;/*210*/}

	/*
	 * If there's a right sibling, see if it's ok to shift an entry
	 * out of it.
	 */
	Din_Go(654,2048);if (!xfs_btree_ptr_is_null(cur, &rptr)) {
		/*
		 * Move the temp cursor to the last entry in the next block.
		 * Actually any entry but the first would suffice.
		 */
		Din_Go(636,2048);i = xfs_btree_lastrec(tcur, level);
		XFS_WANT_CORRUPTED_GOTO(cur->bc_mp, i == 1, error0);

		error = xfs_btree_increment(tcur, level, &i);
		Din_Go(638,2048);if (error)
			{/*211*/Din_Go(637,2048);goto error0;/*212*/}
		XFS_WANT_CORRUPTED_GOTO(cur->bc_mp, i == 1, error0);

		Din_Go(639,2048);i = xfs_btree_lastrec(tcur, level);
		XFS_WANT_CORRUPTED_GOTO(cur->bc_mp, i == 1, error0);

		/* Grab a pointer to the block. */
		right = xfs_btree_get_block(tcur, level, &rbp);
#ifdef DEBUG
		error = xfs_btree_check_block(tcur, right, level, rbp);
		if (error)
			goto error0;
#endif
		/* Grab the current block number, for future use. */
		xfs_btree_get_sibling(tcur, right, &cptr, XFS_BB_LEFTSIB);

		/*
		 * If right block is full enough so that removing one entry
		 * won't make it too empty, and left-shifting an entry out
		 * of right to us works, we're done.
		 */
		Din_Go(648,2048);if (xfs_btree_get_numrecs(right) - 1 >=
		    cur->bc_ops->get_minrecs(tcur, level)) {
			Din_Go(640,2048);error = xfs_btree_lshift(tcur, level, &i);
			Din_Go(642,2048);if (error)
				{/*213*/Din_Go(641,2048);goto error0;/*214*/}
			Din_Go(647,2048);if (i) {
				ASSERT(xfs_btree_get_numrecs(block) >=
				       cur->bc_ops->get_minrecs(tcur, level));

				Din_Go(643,2048);xfs_btree_del_cursor(tcur, XFS_BTREE_NOERROR);
				tcur = NULL;

				error = xfs_btree_dec_cursor(cur, level, stat);
				Din_Go(645,2048);if (error)
					{/*215*/Din_Go(644,2048);goto error0;/*216*/}
				{int  ReplaceReturn = 0; Din_Go(646,2048); return ReplaceReturn;}
			}
		}

		/*
		 * Otherwise, grab the number of records in right for
		 * future reference, and fix up the temp cursor to point
		 * to our block again (last record).
		 */
		Din_Go(649,2048);rrecs = xfs_btree_get_numrecs(right);
		Din_Go(653,2048);if (!xfs_btree_ptr_is_null(cur, &lptr)) {
			Din_Go(650,2048);i = xfs_btree_firstrec(tcur, level);
			XFS_WANT_CORRUPTED_GOTO(cur->bc_mp, i == 1, error0);

			error = xfs_btree_decrement(tcur, level, &i);
			Din_Go(652,2048);if (error)
				{/*217*/Din_Go(651,2048);goto error0;/*218*/}
			XFS_WANT_CORRUPTED_GOTO(cur->bc_mp, i == 1, error0);
		}
	}

	/*
	 * If there's a left sibling, see if it's ok to shift an entry
	 * out of it.
	 */
	Din_Go(670,2048);if (!xfs_btree_ptr_is_null(cur, &lptr)) {
		/*
		 * Move the temp cursor to the first entry in the
		 * previous block.
		 */
		Din_Go(655,2048);i = xfs_btree_firstrec(tcur, level);
		XFS_WANT_CORRUPTED_GOTO(cur->bc_mp, i == 1, error0);

		error = xfs_btree_decrement(tcur, level, &i);
		Din_Go(657,2048);if (error)
			{/*219*/Din_Go(656,2048);goto error0;/*220*/}
		Din_Go(658,2048);i = xfs_btree_firstrec(tcur, level);
		XFS_WANT_CORRUPTED_GOTO(cur->bc_mp, i == 1, error0);

		/* Grab a pointer to the block. */
		left = xfs_btree_get_block(tcur, level, &lbp);
#ifdef DEBUG
		error = xfs_btree_check_block(cur, left, level, lbp);
		if (error)
			goto error0;
#endif
		/* Grab the current block number, for future use. */
		xfs_btree_get_sibling(tcur, left, &cptr, XFS_BB_RIGHTSIB);

		/*
		 * If left block is full enough so that removing one entry
		 * won't make it too empty, and right-shifting an entry out
		 * of left to us works, we're done.
		 */
		Din_Go(668,2048);if (xfs_btree_get_numrecs(left) - 1 >=
		    cur->bc_ops->get_minrecs(tcur, level)) {
			Din_Go(659,2048);error = xfs_btree_rshift(tcur, level, &i);
			Din_Go(661,2048);if (error)
				{/*221*/Din_Go(660,2048);goto error0;/*222*/}
			Din_Go(667,2048);if (i) {
				ASSERT(xfs_btree_get_numrecs(block) >=
				       cur->bc_ops->get_minrecs(tcur, level));
				Din_Go(662,2048);xfs_btree_del_cursor(tcur, XFS_BTREE_NOERROR);
				tcur = NULL;
				Din_Go(664,2048);if (level == 0)
					{/*223*/Din_Go(663,2048);cur->bc_ptrs[0]++;/*224*/}
				XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
				Din_Go(665,2048);*stat = 1;
				{int  ReplaceReturn = 0; Din_Go(666,2048); return ReplaceReturn;}
			}
		}

		/*
		 * Otherwise, grab the number of records in right for
		 * future reference.
		 */
		Din_Go(669,2048);lrecs = xfs_btree_get_numrecs(left);
	}

	/* Delete the temp cursor, we're done with it. */
	Din_Go(671,2048);xfs_btree_del_cursor(tcur, XFS_BTREE_NOERROR);
	tcur = NULL;

	/* If here, we need to do a join to keep the tree balanced. */
	ASSERT(!xfs_btree_ptr_is_null(cur, &cptr));

	Din_Go(683,2048);if (!xfs_btree_ptr_is_null(cur, &lptr) &&
	    lrecs + xfs_btree_get_numrecs(block) <=
			cur->bc_ops->get_maxrecs(cur, level)) {
		/*
		 * Set "right" to be the starting block,
		 * "left" to be the left neighbor.
		 */
		Din_Go(672,2048);rptr = cptr;
		right = block;
		rbp = bp;
		error = xfs_btree_read_buf_block(cur, &lptr, 0, &left, &lbp);
		Din_Go(674,2048);if (error)
			{/*225*/Din_Go(673,2048);goto error0;/*226*/}

	/*
	 * If that won't work, see if we can join with the right neighbor block.
	 */
	} else {/*227*/Din_Go(675,2048);if (!xfs_btree_ptr_is_null(cur, &rptr) &&
		   rrecs + xfs_btree_get_numrecs(block) <=
			cur->bc_ops->get_maxrecs(cur, level)) {
		/*
		 * Set "left" to be the starting block,
		 * "right" to be the right neighbor.
		 */
		Din_Go(676,2048);lptr = cptr;
		left = block;
		lbp = bp;
		error = xfs_btree_read_buf_block(cur, &rptr, 0, &right, &rbp);
		Din_Go(678,2048);if (error)
			{/*229*/Din_Go(677,2048);goto error0;/*230*/}

	/*
	 * Otherwise, we can't fix the imbalance.
	 * Just return.  This is probably a logic error, but it's not fatal.
	 */
	} else {
		Din_Go(679,2048);error = xfs_btree_dec_cursor(cur, level, stat);
		Din_Go(681,2048);if (error)
			{/*231*/Din_Go(680,2048);goto error0;/*232*/}
		{int  ReplaceReturn = 0; Din_Go(682,2048); return ReplaceReturn;}
	;/*228*/}}

	Din_Go(684,2048);rrecs = xfs_btree_get_numrecs(right);
	lrecs = xfs_btree_get_numrecs(left);

	/*
	 * We're now going to join "left" and "right" by moving all the stuff
	 * in "right" to "left" and deleting "right".
	 */
	XFS_BTREE_STATS_ADD(cur, moves, rrecs);
	Din_Go(687,2048);if (level > 0) {
		/* It's a non-leaf.  Move keys and pointers. */
		Din_Go(685,2048);union xfs_btree_key	*lkp;	/* left btree key */
		union xfs_btree_ptr	*lpp;	/* left address pointer */
		union xfs_btree_key	*rkp;	/* right btree key */
		union xfs_btree_ptr	*rpp;	/* right address pointer */

		lkp = xfs_btree_key_addr(cur, lrecs + 1, left);
		lpp = xfs_btree_ptr_addr(cur, lrecs + 1, left);
		rkp = xfs_btree_key_addr(cur, 1, right);
		rpp = xfs_btree_ptr_addr(cur, 1, right);
#ifdef DEBUG
		for (i = 1; i < rrecs; i++) {
			error = xfs_btree_check_ptr(cur, rpp, i, level);
			if (error)
				goto error0;
		}
#endif
		xfs_btree_copy_keys(cur, lkp, rkp, rrecs);
		xfs_btree_copy_ptrs(cur, lpp, rpp, rrecs);

		xfs_btree_log_keys(cur, lbp, lrecs + 1, lrecs + rrecs);
		xfs_btree_log_ptrs(cur, lbp, lrecs + 1, lrecs + rrecs);
	} else {
		/* It's a leaf.  Move records.  */
		Din_Go(686,2048);union xfs_btree_rec	*lrp;	/* left record pointer */
		union xfs_btree_rec	*rrp;	/* right record pointer */

		lrp = xfs_btree_rec_addr(cur, lrecs + 1, left);
		rrp = xfs_btree_rec_addr(cur, 1, right);

		xfs_btree_copy_recs(cur, lrp, rrp, rrecs);
		xfs_btree_log_recs(cur, lbp, lrecs + 1, lrecs + rrecs);
	}

	XFS_BTREE_STATS_INC(cur, join);

	/*
	 * Fix up the number of records and right block pointer in the
	 * surviving block, and log it.
	 */
	Din_Go(688,2048);xfs_btree_set_numrecs(left, lrecs + rrecs);
	xfs_btree_get_sibling(cur, right, &cptr, XFS_BB_RIGHTSIB),
	xfs_btree_set_sibling(cur, left, &cptr, XFS_BB_RIGHTSIB);
	xfs_btree_log_block(cur, lbp, XFS_BB_NUMRECS | XFS_BB_RIGHTSIB);

	/* If there is a right sibling, point it to the remaining block. */
	xfs_btree_get_sibling(cur, left, &cptr, XFS_BB_RIGHTSIB);
	Din_Go(693,2048);if (!xfs_btree_ptr_is_null(cur, &cptr)) {
		Din_Go(689,2048);error = xfs_btree_read_buf_block(cur, &cptr, 0, &rrblock, &rrbp);
		Din_Go(691,2048);if (error)
			{/*233*/Din_Go(690,2048);goto error0;/*234*/}
		Din_Go(692,2048);xfs_btree_set_sibling(cur, rrblock, &lptr, XFS_BB_LEFTSIB);
		xfs_btree_log_block(cur, rrbp, XFS_BB_LEFTSIB);
	}

	/* Free the deleted block. */
	Din_Go(694,2048);error = cur->bc_ops->free_block(cur, rbp);
	Din_Go(696,2048);if (error)
		{/*235*/Din_Go(695,2048);goto error0;/*236*/}
	XFS_BTREE_STATS_INC(cur, free);

	/*
	 * If we joined with the left neighbor, set the buffer in the
	 * cursor to the left block, and fix up the index.
	 */
	Din_Go(702,2048);if (bp != lbp) {
		Din_Go(697,2048);cur->bc_bufs[level] = lbp;
		cur->bc_ptrs[level] += lrecs;
		cur->bc_ra[level] = 0;
	}
	/*
	 * If we joined with the right neighbor and there's a level above
	 * us, increment the cursor at that level.
	 */
	else {/*237*/Din_Go(698,2048);if ((cur->bc_flags & XFS_BTREE_ROOT_IN_INODE) ||
		   (level + 1 < cur->bc_nlevels)) {
		Din_Go(699,2048);error = xfs_btree_increment(cur, level + 1, &i);
		Din_Go(701,2048);if (error)
			{/*239*/Din_Go(700,2048);goto error0;/*240*/}
	;/*238*/}}

	/*
	 * Readjust the ptr at this level if it's not a leaf, since it's
	 * still pointing at the deletion point, which makes the cursor
	 * inconsistent.  If this makes the ptr 0, the caller fixes it up.
	 * We can't use decrement because it would change the next level up.
	 */
	Din_Go(704,2048);if (level > 0)
		{/*241*/Din_Go(703,2048);cur->bc_ptrs[level]--;/*242*/}

	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	/* Return value means the next level up has something to do. */
	Din_Go(705,2048);*stat = 2;
	{int  ReplaceReturn = 0; Din_Go(706,2048); return ReplaceReturn;}

error0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_ERROR);
	if (tcur)
		{/*243*/xfs_btree_del_cursor(tcur, XFS_BTREE_ERROR);/*244*/}
	return error;
}

/*
 * Delete the record pointed to by cur.
 * The cursor refers to the place where the record was (could be inserted)
 * when the operation returns.
 */
int					/* error */
xfs_btree_delete(
	struct xfs_btree_cur	*cur,
	int			*stat)	/* success/failure */
{
	Din_Go(707,2048);int			error;	/* error return value */
	int			level;
	int			i;

	XFS_BTREE_TRACE_CURSOR(cur, XBT_ENTRY);

	/*
	 * Go up the tree, starting at leaf level.
	 *
	 * If 2 is returned then a join was done; go to the next level.
	 * Otherwise we are done.
	 */
	Din_Go(711,2048);for (level = 0, i = 2; i == 2; level++) {
		Din_Go(708,2048);error = xfs_btree_delrec(cur, level, &i);
		Din_Go(710,2048);if (error)
			{/*67*/Din_Go(709,2048);goto error0;/*68*/}
	}

	Din_Go(718,2048);if (i == 0) {
		Din_Go(712,2048);for (level = 1; level < cur->bc_nlevels; level++) {
			Din_Go(713,2048);if (cur->bc_ptrs[level] == 0) {
				Din_Go(714,2048);error = xfs_btree_decrement(cur, level, &i);
				Din_Go(716,2048);if (error)
					{/*69*/Din_Go(715,2048);goto error0;/*70*/}
				Din_Go(717,2048);break;
			}
		}
	}

	XFS_BTREE_TRACE_CURSOR(cur, XBT_EXIT);
	Din_Go(719,2048);*stat = i;
	{int  ReplaceReturn = 0; Din_Go(720,2048); return ReplaceReturn;}
error0:
	XFS_BTREE_TRACE_CURSOR(cur, XBT_ERROR);
	return error;
}

/*
 * Get the data from the pointed-to record.
 */
int					/* error */
xfs_btree_get_rec(
	struct xfs_btree_cur	*cur,	/* btree cursor */
	union xfs_btree_rec	**recp,	/* output: btree record */
	int			*stat)	/* output: success/failure */
{
	Din_Go(721,2048);struct xfs_btree_block	*block;	/* btree block */
	struct xfs_buf		*bp;	/* buffer pointer */
	int			ptr;	/* record number */
#ifdef DEBUG
	int			error;	/* error return value */
#endif

	ptr = cur->bc_ptrs[0];
	block = xfs_btree_get_block(cur, 0, &bp);

#ifdef DEBUG
	error = xfs_btree_check_block(cur, block, 0, bp);
	if (error)
		return error;
#endif

	/*
	 * Off the right end or left end, return failure.
	 */
	Din_Go(724,2048);if (ptr > xfs_btree_get_numrecs(block) || ptr <= 0) {
		Din_Go(722,2048);*stat = 0;
		{int  ReplaceReturn = 0; Din_Go(723,2048); return ReplaceReturn;}
	}

	/*
	 * Point to the record and extract its data.
	 */
	Din_Go(725,2048);*recp = xfs_btree_rec_addr(cur, ptr, block);
	*stat = 1;
	{int  ReplaceReturn = 0; Din_Go(726,2048); return ReplaceReturn;}
}

/*
 * Change the owner of a btree.
 *
 * The mechanism we use here is ordered buffer logging. Because we don't know
 * how many buffers were are going to need to modify, we don't really want to
 * have to make transaction reservations for the worst case of every buffer in a
 * full size btree as that may be more space that we can fit in the log....
 *
 * We do the btree walk in the most optimal manner possible - we have sibling
 * pointers so we can just walk all the blocks on each level from left to right
 * in a single pass, and then move to the next level and do the same. We can
 * also do readahead on the sibling pointers to get IO moving more quickly,
 * though for slow disks this is unlikely to make much difference to performance
 * as the amount of CPU work we have to do before moving to the next block is
 * relatively small.
 *
 * For each btree block that we load, modify the owner appropriately, set the
 * buffer as an ordered buffer and log it appropriately. We need to ensure that
 * we mark the region we change dirty so that if the buffer is relogged in
 * a subsequent transaction the changes we make here as an ordered buffer are
 * correctly relogged in that transaction.  If we are in recovery context, then
 * just queue the modified buffer as delayed write buffer so the transaction
 * recovery completion writes the changes to disk.
 */
static int
xfs_btree_block_change_owner(
	struct xfs_btree_cur	*cur,
	int			level,
	__uint64_t		new_owner,
	struct list_head	*buffer_list)
{
	Din_Go(727,2048);struct xfs_btree_block	*block;
	struct xfs_buf		*bp;
	union xfs_btree_ptr     rptr;

	/* do right sibling readahead */
	xfs_btree_readahead(cur, level, XFS_BTCUR_RIGHTRA);

	/* modify the owner */
	block = xfs_btree_get_block(cur, level, &bp);
	Din_Go(728,2048);if (cur->bc_flags & XFS_BTREE_LONG_PTRS)
		block->bb_u.l.bb_owner = cpu_to_be64(new_owner);
	else
		block->bb_u.s.bb_owner = cpu_to_be32(new_owner);

	/*
	 * If the block is a root block hosted in an inode, we might not have a
	 * buffer pointer here and we shouldn't attempt to log the change as the
	 * information is already held in the inode and discarded when the root
	 * block is formatted into the on-disk inode fork. We still change it,
	 * though, so everything is consistent in memory.
	 */
	Din_Go(731,2048);if (bp) {
		Din_Go(729,2048);if (cur->bc_tp) {
			xfs_trans_ordered_buf(cur->bc_tp, bp);
			Din_Go(730,2048);xfs_btree_log_block(cur, bp, XFS_BB_OWNER);
		} else {
			xfs_buf_delwri_queue(bp, buffer_list);
		}
	} else {
		ASSERT(cur->bc_flags & XFS_BTREE_ROOT_IN_INODE);
		ASSERT(level == cur->bc_nlevels - 1);
	}

	/* now read rh sibling block for next iteration */
	Din_Go(732,2048);xfs_btree_get_sibling(cur, block, &rptr, XFS_BB_RIGHTSIB);
	Din_Go(733,2048);if (xfs_btree_ptr_is_null(cur, &rptr))
		return -ENOENT;

	{int  ReplaceReturn = xfs_btree_lookup_get_block(cur, level, &rptr, &block); Din_Go(734,2048); return ReplaceReturn;}
}

int
xfs_btree_change_owner(
	struct xfs_btree_cur	*cur,
	__uint64_t		new_owner,
	struct list_head	*buffer_list)
{
	Din_Go(735,2048);union xfs_btree_ptr     lptr;
	int			level;
	struct xfs_btree_block	*block = NULL;
	int			error = 0;

	cur->bc_ops->init_ptr_from_cur(cur, &lptr);

	/* for each level */
	Din_Go(745,2048);for (level = cur->bc_nlevels - 1; level >= 0; level--) {
		/* grab the left hand block */
		Din_Go(736,2048);error = xfs_btree_lookup_get_block(cur, level, &lptr, &block);
		Din_Go(738,2048);if (error)
			{/*71*/{int  ReplaceReturn = error; Din_Go(737,2048); return ReplaceReturn;}/*72*/}

		/* readahead the left most block for the next level down */
		Din_Go(740,2048);if (level > 0) {
			Din_Go(739,2048);union xfs_btree_ptr     *ptr;

			ptr = xfs_btree_ptr_addr(cur, 1, block);
			xfs_btree_readahead_ptr(cur, ptr, 1);

			/* save for the next iteration of the loop */
			lptr = *ptr;
		}

		/* for each buffer in the level */
		Din_Go(742,2048);do {
			Din_Go(741,2048);error = xfs_btree_block_change_owner(cur, level,
							     new_owner,
							     buffer_list);
		} while (!error);

		Din_Go(744,2048);if (error != -ENOENT)
			{/*73*/{int  ReplaceReturn = error; Din_Go(743,2048); return ReplaceReturn;}/*74*/}
	}

	{int  ReplaceReturn = 0; Din_Go(746,2048); return ReplaceReturn;}
}

/**
 * xfs_btree_sblock_v5hdr_verify() -- verify the v5 fields of a short-format
 *				      btree block
 *
 * @bp: buffer containing the btree block
 * @max_recs: pointer to the m_*_mxr max records field in the xfs mount
 * @pag_max_level: pointer to the per-ag max level field
 */
bool
xfs_btree_sblock_v5hdr_verify(
	struct xfs_buf		*bp)
{
	Din_Go(747,2048);struct xfs_mount	*mp = bp->b_target->bt_mount;
	struct xfs_btree_block	*block = XFS_BUF_TO_BLOCK(bp);
	struct xfs_perag	*pag = bp->b_pag;

	Din_Go(748,2048);if (!xfs_sb_version_hascrc(&mp->m_sb))
		return false;
	Din_Go(749,2048);if (!uuid_equal(&block->bb_u.s.bb_uuid, &mp->m_sb.sb_meta_uuid))
		return false;
	Din_Go(750,2048);if (block->bb_u.s.bb_blkno != cpu_to_be64(bp->b_bn))
		return false;
	Din_Go(751,2048);if (pag && be32_to_cpu(block->bb_u.s.bb_owner) != pag->pag_agno)
		return false;
	{_Bool  ReplaceReturn = true; Din_Go(752,2048); return ReplaceReturn;}
}

/**
 * xfs_btree_sblock_verify() -- verify a short-format btree block
 *
 * @bp: buffer containing the btree block
 * @max_recs: maximum records allowed in this btree node
 */
bool
xfs_btree_sblock_verify(
	struct xfs_buf		*bp,
	unsigned int		max_recs)
{
	Din_Go(753,2048);struct xfs_mount	*mp = bp->b_target->bt_mount;
	struct xfs_btree_block	*block = XFS_BUF_TO_BLOCK(bp);

	/* numrecs verification */
	Din_Go(754,2048);if (be16_to_cpu(block->bb_numrecs) > max_recs)
		return false;

	/* sibling pointer verification */
	Din_Go(755,2048);if (!block->bb_u.s.bb_leftsib ||
	    (be32_to_cpu(block->bb_u.s.bb_leftsib) >= mp->m_sb.sb_agblocks &&
	     block->bb_u.s.bb_leftsib != cpu_to_be32(NULLAGBLOCK)))
		return false;
	Din_Go(756,2048);if (!block->bb_u.s.bb_rightsib ||
	    (be32_to_cpu(block->bb_u.s.bb_rightsib) >= mp->m_sb.sb_agblocks &&
	     block->bb_u.s.bb_rightsib != cpu_to_be32(NULLAGBLOCK)))
		return false;

	{_Bool  ReplaceReturn = true; Din_Go(757,2048); return ReplaceReturn;}
}
