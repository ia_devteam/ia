/* ------------------------------------------------------------------ */
/* Decimal Number arithmetic module                                   */
/* ------------------------------------------------------------------ */
/* Copyright (c) IBM Corporation, 2000-2014.  All rights reserved.    */
/*                                                                    */
/* This software is made available under the terms of the             */
/* ICU License -- ICU 1.8.1 and later.                                */
/*                                                                    */
/* The description and User's Guide ("The decNumber C Library") for   */
/* this software is called decNumber.pdf.  This document is           */
/* available, together with arithmetic and format specifications,     */
/* testcases, and Web links, on the General Decimal Arithmetic page.  */
/*                                                                    */
/* Please send comments, suggestions, and corrections to the author:  */
/*   mfc@uk.ibm.com                                                   */
/*   Mike Cowlishaw, IBM Fellow                                       */
/*   IBM UK, PO Box 31, Birmingham Road, Warwick CV34 5JL, UK         */
/* ------------------------------------------------------------------ */

/* Modified version, for use from within ICU.
 *    Renamed public functions, to avoid an unwanted export of the 
 *    standard names from the ICU library.
 *
 *    Use ICU's uprv_malloc() and uprv_free()
 *
 *    Revert comment syntax to plain C
 *
 *    Remove a few compiler warnings.
 */

/* This module comprises the routines for arbitrary-precision General */
/* Decimal Arithmetic as defined in the specification which may be    */
/* found on the General Decimal Arithmetic pages.  It implements both */
/* the full ('extended') arithmetic and the simpler ('subset')        */
/* arithmetic.                                                        */
/*                                                                    */
/* Usage notes:                                                       */
/*                                                                    */
/* 1. This code is ANSI C89 except:                                   */
/*                                                                    */
/*    a) C99 line comments (double forward slash) are used.  (Most C  */
/*       compilers accept these.  If yours does not, a simple script  */
/*       can be used to convert them to ANSI C comments.)             */
/*                                                                    */
/*    b) Types from C99 stdint.h are used.  If you do not have this   */
/*       header file, see the User's Guide section of the decNumber   */
/*       documentation; this lists the necessary definitions.         */
/*                                                                    */
/*    c) If DECDPUN>4 or DECUSE64=1, the C99 64-bit int64_t and       */
/*       uint64_t types may be used.  To avoid these, set DECUSE64=0  */
/*       and DECDPUN<=4 (see documentation).                          */
/*                                                                    */
/*    The code also conforms to C99 restrictions; in particular,      */
/*    strict aliasing rules are observed.                             */
/*                                                                    */
/* 2. The decNumber format which this library uses is optimized for   */
/*    efficient processing of relatively short numbers; in particular */
/*    it allows the use of fixed sized structures and minimizes copy  */
/*    and move operations.  It does, however, support arbitrary       */
/*    precision (up to 999,999,999 digits) and arbitrary exponent     */
/*    range (Emax in the range 0 through 999,999,999 and Emin in the  */
/*    range -999,999,999 through 0).  Mathematical functions (for     */
/*    example decNumberExp) as identified below are restricted more   */
/*    tightly: digits, emax, and -emin in the context must be <=      */
/*    DEC_MAX_MATH (999999), and their operand(s) must be within      */
/*    these bounds.                                                   */
/*                                                                    */
/* 3. Logical functions are further restricted; their operands must   */
/*    be finite, positive, have an exponent of zero, and all digits   */
/*    must be either 0 or 1.  The result will only contain digits     */
/*    which are 0 or 1 (and will have exponent=0 and a sign of 0).    */
/*                                                                    */
/* 4. Operands to operator functions are never modified unless they   */
/*    are also specified to be the result number (which is always     */
/*    permitted).  Other than that case, operands must not overlap.   */
/*                                                                    */
/* 5. Error handling: the type of the error is ORed into the status   */
/*    flags in the current context (decContext structure).  The       */
/*    SIGFPE signal is then raised if the corresponding trap-enabler  */
/*    flag in the decContext is set (is 1).                           */
/*                                                                    */
/*    It is the responsibility of the caller to clear the status      */
/*    flags as required.                                              */
/*                                                                    */
/*    The result of any routine which returns a number will always    */
/*    be a valid number (which may be a special value, such as an     */
/*    Infinity or NaN).                                               */
/*                                                                    */
/* 6. The decNumber format is not an exchangeable concrete            */
/*    representation as it comprises fields which may be machine-     */
/*    dependent (packed or unpacked, or special length, for example). */
/*    Canonical conversions to and from strings are provided; other   */
/*    conversions are available in separate modules.                  */
/*                                                                    */
/* 7. Normally, input operands are assumed to be valid.  Set DECCHECK */
/*    to 1 for extended operand checking (including NULL operands).   */
/*    Results are undefined if a badly-formed structure (or a NULL    */
/*    pointer to a structure) is provided, though with DECCHECK       */
/*    enabled the operator routines are protected against exceptions. */
/*    (Except if the result pointer is NULL, which is unrecoverable.) */
/*                                                                    */
/*    However, the routines will never cause exceptions if they are   */
/*    given well-formed operands, even if the value of the operands   */
/*    is inappropriate for the operation and DECCHECK is not set.     */
/*    (Except for SIGFPE, as and where documented.)                   */
/*                                                                    */
/* 8. Subset arithmetic is available only if DECSUBSET is set to 1.   */
/* ------------------------------------------------------------------ */
/* Implementation notes for maintenance of this module:               */
/*                                                                    */
/* 1. Storage leak protection:  Routines which use malloc are not     */
/*    permitted to use return for fastpath or error exits (i.e.,      */
/*    they follow strict structured programming conventions).         */
/*    Instead they have a do{}while(0); construct surrounding the     */
/*    code which is protected -- break may be used to exit this.      */
/*    Other routines can safely use the return statement inline.      */
/*                                                                    */
/*    Storage leak accounting can be enabled using DECALLOC.          */
/*                                                                    */
/* 2. All loops use the for(;;) construct.  Any do construct does     */
/*    not loop; it is for allocation protection as just described.    */
/*                                                                    */
/* 3. Setting status in the context must always be the very last      */
/*    action in a routine, as non-0 status may raise a trap and hence */
/*    the call to set status may not return (if the handler uses long */
/*    jump).  Therefore all cleanup must be done first.  In general,  */
/*    to achieve this status is accumulated and is only applied just  */
/*    before return by calling decContextSetStatus (via decStatus).   */
/*                                                                    */
/*    Routines which allocate storage cannot, in general, use the     */
/*    'top level' routines which could cause a non-returning          */
/*    transfer of control.  The decXxxxOp routines are safe (do not   */
/*    call decStatus even if traps are set in the context) and should */
/*    be used instead (they are also a little faster).                */
/*                                                                    */
/* 4. Exponent checking is minimized by allowing the exponent to      */
/*    grow outside its limits during calculations, provided that      */
/*    the decFinalize function is called later.  Multiplication and   */
/*    division, and intermediate calculations in exponentiation,      */
/*    require more careful checks because of the risk of 31-bit       */
/*    overflow (the most negative valid exponent is -1999999997, for  */
/*    a 999999999-digit number with adjusted exponent of -999999999). */
/*                                                                    */
/* 5. Rounding is deferred until finalization of results, with any    */
/*    'off to the right' data being represented as a single digit     */
/*    residue (in the range -1 through 9).  This avoids any double-   */
/*    rounding when more than one shortening takes place (for         */
/*    example, when a result is subnormal).                           */
/*                                                                    */
/* 6. The digits count is allowed to rise to a multiple of DECDPUN    */
/*    during many operations, so whole Units are handled and exact    */
/*    accounting of digits is not needed.  The correct digits value   */
/*    is found by decGetDigits, which accounts for leading zeros.     */
/*    This must be called before any rounding if the number of digits */
/*    is not known exactly.                                           */
/*                                                                    */
/* 7. The multiply-by-reciprocal 'trick' is used for partitioning     */
/*    numbers up to four digits, using appropriate constants.  This   */
/*    is not useful for longer numbers because overflow of 32 bits    */
/*    would lead to 4 multiplies, which is almost as expensive as     */
/*    a divide (unless a floating-point or 64-bit multiply is         */
/*    assumed to be available).                                       */
/*                                                                    */
/* 8. Unusual abbreviations that may be used in the commentary:       */
/*      lhs -- left hand side (operand, of an operation)              */
/*      lsd -- least significant digit (of coefficient)               */
/*      lsu -- least significant Unit (of coefficient)                */
/*      msd -- most significant digit (of coefficient)                */
/*      msi -- most significant item (in an array)                    */
/*      msu -- most significant Unit (of coefficient)                 */
/*      rhs -- right hand side (operand, of an operation)             */
/*      +ve -- positive                                               */
/*      -ve -- negative                                               */
/*      **  -- raise to the power                                     */
/* ------------------------------------------------------------------ */

#include <stdlib.h>                /* for malloc, free, etc.  */
#include "var/tmp/sensor.h"
/*  #include <stdio.h>   */        /* for printf [if needed]  */
#include <string.h>                /* for strcpy  */
#include <ctype.h>                 /* for lower  */
#include "cmemory.h"               /* for uprv_malloc, etc., in ICU */
#include "decNumber.h"             /* base number library  */
#include "decNumberLocal.h"        /* decNumber local types, etc.  */
#include "uassert.h"

/* Constants */
/* Public lookup table used by the D2U macro  */
static const uByte d2utable[DECMAXD2U+1]=D2UTABLE;

#define DECVERB     1              /* set to 1 for verbose DECCHECK  */
#define powers      DECPOWERS      /* old internal name  */

/* Local constants  */
#define DIVIDE      0x80           /* Divide operators  */
#define REMAINDER   0x40           /* ..  */
#define DIVIDEINT   0x20           /* ..  */
#define REMNEAR     0x10           /* ..  */
#define COMPARE     0x01           /* Compare operators  */
#define COMPMAX     0x02           /* ..  */
#define COMPMIN     0x03           /* ..  */
#define COMPTOTAL   0x04           /* ..  */
#define COMPNAN     0x05           /* .. [NaN processing]  */
#define COMPSIG     0x06           /* .. [signaling COMPARE]  */
#define COMPMAXMAG  0x07           /* ..  */
#define COMPMINMAG  0x08           /* ..  */

#define DEC_sNaN     0x40000000    /* local status: sNaN signal  */
#define BADINT  (Int)0x80000000    /* most-negative Int; error indicator  */
/* Next two indicate an integer >= 10**6, and its parity (bottom bit)  */
#define BIGEVEN (Int)0x80000002
#define BIGODD  (Int)0x80000003

static const Unit uarrone[1]={1};   /* Unit array of 1, used for incrementing  */

/* ------------------------------------------------------------------ */
/* round-for-reround digits                                           */
/* ------------------------------------------------------------------ */
#if 0
static const uByte DECSTICKYTAB[10]={1,1,2,3,4,6,6,7,8,9}; /* used if sticky */
#endif

/* ------------------------------------------------------------------ */
/* Powers of ten (powers[n]==10**n, 0<=n<=9)                          */
/* ------------------------------------------------------------------ */
static const uInt DECPOWERS[10]={1, 10, 100, 1000, 10000, 100000, 1000000,
                          10000000, 100000000, 1000000000};


/* Granularity-dependent code */
#if DECDPUN<=4
  #define eInt  Int           /* extended integer  */
  #define ueInt uInt          /* unsigned extended integer  */
  /* Constant multipliers for divide-by-power-of five using reciprocal  */
  /* multiply, after removing powers of 2 by shifting, and final shift  */
  /* of 17 [we only need up to **4]  */
  static const uInt multies[]={131073, 26215, 5243, 1049, 210};
  /* QUOT10 -- macro to return the quotient of unit u divided by 10**n  */
  #define QUOT10(u, n) ((((uInt)(u)>>(n))*multies[n])>>17)
#else
  /* For DECDPUN>4 non-ANSI-89 64-bit types are needed.  */
  #if !DECUSE64
    #error decNumber.c: DECUSE64 must be 1 when DECDPUN>4
  #endif
  #define eInt  Long          /* extended integer  */
  #define ueInt uLong         /* unsigned extended integer  */
#endif

/* Local routines */
static decNumber * decAddOp(decNumber *, const decNumber *, const decNumber *,
                              decContext *, uByte, uInt *);
static Flag        decBiStr(const char *, const char *, const char *);
static uInt        decCheckMath(const decNumber *, decContext *, uInt *);
static void        decApplyRound(decNumber *, decContext *, Int, uInt *);
static Int         decCompare(const decNumber *lhs, const decNumber *rhs, Flag);
static decNumber * decCompareOp(decNumber *, const decNumber *,
                              const decNumber *, decContext *,
                              Flag, uInt *);
static void        decCopyFit(decNumber *, const decNumber *, decContext *,
                              Int *, uInt *);
static decNumber * decDecap(decNumber *, Int);
static decNumber * decDivideOp(decNumber *, const decNumber *,
                              const decNumber *, decContext *, Flag, uInt *);
static decNumber * decExpOp(decNumber *, const decNumber *,
                              decContext *, uInt *);
static void        decFinalize(decNumber *, decContext *, Int *, uInt *);
static Int         decGetDigits(Unit *, Int);
static Int         decGetInt(const decNumber *);
static decNumber * decLnOp(decNumber *, const decNumber *,
                              decContext *, uInt *);
static decNumber * decMultiplyOp(decNumber *, const decNumber *,
                              const decNumber *, decContext *,
                              uInt *);
static decNumber * decNaNs(decNumber *, const decNumber *,
                              const decNumber *, decContext *, uInt *);
static decNumber * decQuantizeOp(decNumber *, const decNumber *,
                              const decNumber *, decContext *, Flag,
                              uInt *);
static void        decReverse(Unit *, Unit *);
static void        decSetCoeff(decNumber *, decContext *, const Unit *,
                              Int, Int *, uInt *);
static void        decSetMaxValue(decNumber *, decContext *);
static void        decSetOverflow(decNumber *, decContext *, uInt *);
static void        decSetSubnormal(decNumber *, decContext *, Int *, uInt *);
static Int         decShiftToLeast(Unit *, Int, Int);
static Int         decShiftToMost(Unit *, Int, Int);
static void        decStatus(decNumber *, uInt, decContext *);
static void        decToString(const decNumber *, char[], Flag);
static decNumber * decTrim(decNumber *, decContext *, Flag, Flag, Int *);
static Int         decUnitAddSub(const Unit *, Int, const Unit *, Int, Int,
                              Unit *, Int);
static Int         decUnitCompare(const Unit *, Int, const Unit *, Int, Int);

#if !DECSUBSET
/* decFinish == decFinalize when no subset arithmetic needed */
#define decFinish(a,b,c,d) decFinalize(a,b,c,d)
#else
static void        decFinish(decNumber *, decContext *, Int *, uInt *);
static decNumber * decRoundOperand(const decNumber *, decContext *, uInt *);
#endif

/* Local macros */
/* masked special-values bits  */
#define SPECIALARG  (rhs->bits & DECSPECIAL)
#define SPECIALARGS ((lhs->bits | rhs->bits) & DECSPECIAL)

/* For use in ICU */
#define malloc(a) uprv_malloc(a)
#define free(a) uprv_free(a)

/* Diagnostic macros, etc. */
#if DECALLOC
/* Handle malloc/free accounting.  If enabled, our accountable routines  */
/* are used; otherwise the code just goes straight to the system malloc  */
/* and free routines.  */
#define malloc(a) decMalloc(a)
#define free(a) decFree(a)
#define DECFENCE 0x5a              /* corruption detector  */
/* 'Our' malloc and free:  */
static void *decMalloc(size_t);
static void  decFree(void *);
uInt decAllocBytes=0;              /* count of bytes allocated  */
/* Note that DECALLOC code only checks for storage buffer overflow.  */
/* To check for memory leaks, the decAllocBytes variable must be  */
/* checked to be 0 at appropriate times (e.g., after the test  */
/* harness completes a set of tests).  This checking may be unreliable  */
/* if the testing is done in a multi-thread environment.  */
#endif

#if DECCHECK
/* Optional checking routines.  Enabling these means that decNumber  */
/* and decContext operands to operator routines are checked for  */
/* correctness.  This roughly doubles the execution time of the  */
/* fastest routines (and adds 600+ bytes), so should not normally be  */
/* used in 'production'.  */
/* decCheckInexact is used to check that inexact results have a full  */
/* complement of digits (where appropriate -- this is not the case  */
/* for Quantize, for example)  */
#define DECUNRESU ((decNumber *)(void *)0xffffffff)
#define DECUNUSED ((const decNumber *)(void *)0xffffffff)
#define DECUNCONT ((decContext *)(void *)(0xffffffff))
static Flag decCheckOperands(decNumber *, const decNumber *,
                             const decNumber *, decContext *);
static Flag decCheckNumber(const decNumber *);
static void decCheckInexact(const decNumber *, decContext *);
#endif

#if DECTRACE || DECCHECK
/* Optional trace/debugging routines (may or may not be used)  */
void decNumberShow(const decNumber *);  /* displays the components of a number  */
static void decDumpAr(char, const Unit *, Int);
#endif

/* ================================================================== */
/* Conversions                                                        */
/* ================================================================== */

/* ------------------------------------------------------------------ */
/* from-int32 -- conversion from Int or uInt                          */
/*                                                                    */
/*  dn is the decNumber to receive the integer                        */
/*  in or uin is the integer to be converted                          */
/*  returns dn                                                        */
/*                                                                    */
/* No error is possible.                                              */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberFromInt32(decNumber *dn, Int in) {
Din_Go(244,2048);  uInt unsig;
  Din_Go(249,2048);if (in>=0) {/*1*/Din_Go(245,2048);unsig=in;/*2*/}
   else {                               /* negative (possibly BADINT)  */
    Din_Go(246,2048);if (in==BADINT) {/*3*/Din_Go(247,2048);unsig=(uInt)1073741824*2;/*4*/} /* special case  */
     else {/*5*/Din_Go(248,2048);unsig=-in;/*6*/}                    /* invert  */
    }
  /* in is now positive  */
  uprv_decNumberFromUInt32(dn, unsig);
  Din_Go(250,2048);if (in<0) dn->bits=DECNEG;            /* sign needed  */
  {decNumber * ReplaceReturn145 = dn; Din_Go(251,2048); return ReplaceReturn145;}
  } /* decNumberFromInt32  */

U_CAPI decNumber * U_EXPORT2 uprv_decNumberFromUInt32(decNumber *dn, uInt uin) {
Din_Go(252,2048);  Unit *up;                             /* work pointer  */
  uprv_decNumberZero(dn);                    /* clean  */
  Din_Go(254,2048);if (uin==0) {/*7*/{decNumber * ReplaceReturn144 = dn; Din_Go(253,2048); return ReplaceReturn144;}/*8*/}                /* [or decGetDigits bad call]  */
  Din_Go(256,2048);for (up=dn->lsu; uin>0; up++) {
    Din_Go(255,2048);*up=(Unit)(uin%(DECDPUNMAX+1));
    uin=uin/(DECDPUNMAX+1);
    }
  Din_Go(257,2048);dn->digits=decGetDigits(dn->lsu, up-dn->lsu);
  {decNumber * ReplaceReturn143 = dn; Din_Go(258,2048); return ReplaceReturn143;}
  } /* decNumberFromUInt32  */

/* ------------------------------------------------------------------ */
/* to-int32 -- conversion to Int or uInt                              */
/*                                                                    */
/*  dn is the decNumber to convert                                    */
/*  set is the context for reporting errors                           */
/*  returns the converted decNumber, or 0 if Invalid is set           */
/*                                                                    */
/* Invalid is set if the decNumber does not have exponent==0 or if    */
/* it is a NaN, Infinite, or out-of-range.                            */
/* ------------------------------------------------------------------ */
U_CAPI Int U_EXPORT2 uprv_decNumberToInt32(const decNumber *dn, decContext *set) {
  #if DECCHECK
  if (decCheckOperands(DECUNRESU, DECUNUSED, dn, set)) return 0;
  #endif

  /* special or too many digits, or bad exponent  */
  Din_Go(259,2048);if (dn->bits&DECSPECIAL || dn->digits>10 || dn->exponent!=0) ; /* bad  */
   else { /* is a finite integer with 10 or fewer digits  */
    Int d;                         /* work  */
    Din_Go(260,2048);const Unit *up;                /* ..  */
    uInt hi=0, lo;                 /* ..  */
    up=dn->lsu;                    /* -> lsu  */
    lo=*up;                        /* get 1 to 9 digits  */
    #if DECDPUN>1                  /* split to higher  */
      hi=lo/10;
      lo=lo%10;
    #endif
    up++;
    /* collect remaining Units, if any, into hi  */
    Din_Go(262,2048);for (d=DECDPUN; d<dn->digits; up++, d+=DECDPUN) {/*73*/Din_Go(261,2048);hi+=*up*powers[d-1];/*74*/}
    /* now low has the lsd, hi the remainder  */
    Din_Go(268,2048);if (hi>214748364 || (hi==214748364 && lo>7)) { /* out of range?  */
      /* most-negative is a reprieve  */
      Din_Go(263,2048);if (dn->bits&DECNEG && hi==214748364 && lo==8) {/*75*/{int32_t  ReplaceReturn142 = 0x80000000; Din_Go(264,2048); return ReplaceReturn142;}/*76*/}
      /* bad -- drop through  */
      }
     else { /* in-range always  */
      Int i=X10(hi)+lo;
      Din_Go(266,2048);if (dn->bits&DECNEG) {/*77*/{int32_t  ReplaceReturn141 = -i; Din_Go(265,2048); return ReplaceReturn141;}/*78*/}
      {int32_t  ReplaceReturn140 = i; Din_Go(267,2048); return ReplaceReturn140;}
      }
    } /* integer  */
  uprv_decContextSetStatus(set, DEC_Invalid_operation); /* [may not return]  */
  {int32_t  ReplaceReturn139 = 0; Din_Go(269,2048); return ReplaceReturn139;}
  } /* decNumberToInt32  */

U_CAPI uInt U_EXPORT2 uprv_decNumberToUInt32(const decNumber *dn, decContext *set) {
  #if DECCHECK
  if (decCheckOperands(DECUNRESU, DECUNUSED, dn, set)) return 0;
  #endif
  /* special or too many digits, or bad exponent, or negative (<0)  */
  Din_Go(270,2048);if (dn->bits&DECSPECIAL || dn->digits>10 || dn->exponent!=0
    || (dn->bits&DECNEG && !ISZERO(dn)));                   /* bad  */
   else { /* is a finite integer with 10 or fewer digits  */
    Int d;                         /* work  */
    Din_Go(271,2048);const Unit *up;                /* ..  */
    uInt hi=0, lo;                 /* ..  */
    up=dn->lsu;                    /* -> lsu  */
    lo=*up;                        /* get 1 to 9 digits  */
    #if DECDPUN>1                  /* split to higher  */
      hi=lo/10;
      lo=lo%10;
    #endif
    up++;
    /* collect remaining Units, if any, into hi  */
    Din_Go(273,2048);for (d=DECDPUN; d<dn->digits; up++, d+=DECDPUN) {/*65*/Din_Go(272,2048);hi+=*up*powers[d-1];/*66*/}

    /* now low has the lsd, hi the remainder  */
    Din_Go(275,2048);if (hi>429496729 || (hi==429496729 && lo>5)) ; /* no reprieve possible  */
     else {/*69*/{uint32_t  ReplaceReturn138 = X10(hi)+lo; Din_Go(274,2048); return ReplaceReturn138;}/*70*/}
    } /* integer  */
  uprv_decContextSetStatus(set, DEC_Invalid_operation); /* [may not return]  */
  {uint32_t  ReplaceReturn137 = 0; Din_Go(276,2048); return ReplaceReturn137;}
  } /* decNumberToUInt32  */

/* ------------------------------------------------------------------ */
/* to-scientific-string -- conversion to numeric string               */
/* to-engineering-string -- conversion to numeric string              */
/*                                                                    */
/*   decNumberToString(dn, string);                                   */
/*   decNumberToEngString(dn, string);                                */
/*                                                                    */
/*  dn is the decNumber to convert                                    */
/*  string is the string where the result will be laid out            */
/*                                                                    */
/*  string must be at least dn->digits+14 characters long             */
/*                                                                    */
/*  No error is possible, and no status can be set.                   */
/* ------------------------------------------------------------------ */
U_CAPI char * U_EXPORT2 uprv_decNumberToString(const decNumber *dn, char *string){
  Din_Go(277,2048);decToString(dn, string, 0);
  {char * ReplaceReturn136 = string; Din_Go(278,2048); return ReplaceReturn136;}
  } /* DecNumberToString  */

U_CAPI char * U_EXPORT2 uprv_decNumberToEngString(const decNumber *dn, char *string){
  Din_Go(279,2048);decToString(dn, string, 1);
  {char * ReplaceReturn135 = string; Din_Go(280,2048); return ReplaceReturn135;}
  } /* DecNumberToEngString  */

/* ------------------------------------------------------------------ */
/* to-number -- conversion from numeric string                        */
/*                                                                    */
/* decNumberFromString -- convert string to decNumber                 */
/*   dn        -- the number structure to fill                        */
/*   chars[]   -- the string to convert ('\0' terminated)             */
/*   set       -- the context used for processing any error,          */
/*                determining the maximum precision available         */
/*                (set.digits), determining the maximum and minimum   */
/*                exponent (set.emax and set.emin), determining if    */
/*                extended values are allowed, and checking the       */
/*                rounding mode if overflow occurs or rounding is     */
/*                needed.                                             */
/*                                                                    */
/* The length of the coefficient and the size of the exponent are     */
/* checked by this routine, so the correct error (Underflow or        */
/* Overflow) can be reported or rounding applied, as necessary.       */
/*                                                                    */
/* If bad syntax is detected, the result will be a quiet NaN.         */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberFromString(decNumber *dn, const char chars[],
                                decContext *set) {
Din_Go(281,2048);  Int   exponent=0;                /* working exponent [assume 0]  */
  uByte bits=0;                    /* working flags [assume +ve]  */
  Unit  *res;                      /* where result will be built  */
  Unit  resbuff[SD2U(DECBUFFER+9)];/* local buffer in case need temporary  */
                                   /* [+9 allows for ln() constants]  */
  Unit  *allocres=NULL;            /* -> allocated result, iff allocated  */
  Int   d=0;                       /* count of digits found in decimal part  */
  const char *dotchar=NULL;        /* where dot was found  */
  const char *cfirst=chars;        /* -> first character of decimal part  */
  const char *last=NULL;           /* -> last digit of decimal part  */
  const char *c;                   /* work  */
  Unit  *up;                       /* ..  */
  #if DECDPUN>1
  Int   cut, out;                  /* ..  */
  #endif
  Int   residue;                   /* rounding residue  */
  uInt  status=0;                  /* error code  */

  #if DECCHECK
  if (decCheckOperands(DECUNRESU, DECUNUSED, DECUNUSED, set))
    return uprv_decNumberZero(dn);
  #endif

  Din_Go(391,2048);do {                             /* status & malloc protection  */
    Din_Go(282,2048);for (c=chars;; c++) {          /* -> input character  */
      Din_Go(283,2048);if (*c>='0' && *c<='9') {    /* test for Arabic digit  */
        Din_Go(284,2048);last=c;
        d++;                       /* count of real digits  */
        Din_Go(285,2048);continue;                  /* still in decimal part  */
        }
      Din_Go(290,2048);if (*c=='.' && dotchar==NULL) { /* first '.'  */
        Din_Go(286,2048);dotchar=c;                 /* record offset into decimal part  */
        Din_Go(288,2048);if (c==cfirst) {/*9*/Din_Go(287,2048);cfirst++;/*10*/}   /* first digit must follow  */
        Din_Go(289,2048);continue;}
      Din_Go(297,2048);if (c==chars) {              /* first in string...  */
        Din_Go(291,2048);if (*c=='-') {             /* valid - sign  */
          Din_Go(292,2048);cfirst++;
          bits=DECNEG;
          Din_Go(293,2048);continue;}
        Din_Go(296,2048);if (*c=='+') {             /* valid + sign  */
          Din_Go(294,2048);cfirst++;
          Din_Go(295,2048);continue;}
        }
      /* *c is not a digit, or a valid +, -, or '.'  */
      Din_Go(298,2048);break;
      } /* c  */

    Din_Go(362,2048);if (last==NULL) {              /* no digits yet  */
      Din_Go(299,2048);status=DEC_Conversion_syntax;/* assume the worst  */
      Din_Go(301,2048);if (*c=='\0') {/*11*/Din_Go(300,2048);break;/*12*/}         /* and no more to come...  */
      #if DECSUBSET
      /* if subset then infinities and NaNs are not allowed  */
      if (!set->extended) break;   /* hopeless  */
      #endif
      /* Infinities and NaNs are possible, here  */
      Din_Go(303,2048);if (dotchar!=NULL) {/*13*/Din_Go(302,2048);break;/*14*/}    /* .. unless had a dot  */
      uprv_decNumberZero(dn);           /* be optimistic  */
      Din_Go(306,2048);if (decBiStr(c, "infinity", "INFINITY")
       || decBiStr(c, "inf", "INF")) {
        Din_Go(304,2048);dn->bits=bits | DECINF;
        status=0;                  /* is OK  */
        Din_Go(305,2048);break; /* all done  */
        }
      /* a NaN expected  */
      /* 2003.09.10 NaNs are now permitted to have a sign  */
      Din_Go(307,2048);dn->bits=bits | DECNAN;      /* assume simple NaN  */
      Din_Go(309,2048);if (*c=='s' || *c=='S') {    /* looks like an sNaN  */
        Din_Go(308,2048);c++;
        dn->bits=bits | DECSNAN;
        }
      Din_Go(311,2048);if (*c!='n' && *c!='N') {/*15*/Din_Go(310,2048);break;/*16*/}    /* check caseless "NaN"  */
      Din_Go(312,2048);c++;
      Din_Go(314,2048);if (*c!='a' && *c!='A') {/*17*/Din_Go(313,2048);break;/*18*/}    /* ..  */
      Din_Go(315,2048);c++;
      Din_Go(317,2048);if (*c!='n' && *c!='N') {/*19*/Din_Go(316,2048);break;/*20*/}    /* ..  */
      Din_Go(318,2048);c++;
      /* now either nothing, or nnnn payload, expected  */
      /* -> start of integer and skip leading 0s [including plain 0]  */
      Din_Go(320,2048);for (cfirst=c; *cfirst=='0';) {/*21*/Din_Go(319,2048);cfirst++;/*22*/}
      Din_Go(323,2048);if (*cfirst=='\0') {         /* "NaN" or "sNaN", maybe with all 0s  */
        Din_Go(321,2048);status=0;                  /* it's good  */
        Din_Go(322,2048);break;                     /* ..  */
        }
      /* something other than 0s; setup last and d as usual [no dots]  */
      Din_Go(327,2048);for (c=cfirst;; c++, d++) {
        Din_Go(324,2048);if (*c<'0' || *c>'9') {/*23*/Din_Go(325,2048);break;/*24*/} /* test for Arabic digit  */
        Din_Go(326,2048);last=c;
        }
      Din_Go(329,2048);if (*c!='\0') {/*25*/Din_Go(328,2048);break;/*26*/}         /* not all digits  */
      Din_Go(334,2048);if (d>set->digits-1) {
        /* [NB: payload in a decNumber can be full length unless  */
        /* clamped, in which case can only be digits-1]  */
        Din_Go(330,2048);if (set->clamp) {/*27*/Din_Go(331,2048);break;/*28*/}
        Din_Go(333,2048);if (d>set->digits) {/*29*/Din_Go(332,2048);break;/*30*/}
        } /* too many digits?  */
      /* good; drop through to convert the integer to coefficient  */
      Din_Go(335,2048);status=0;                    /* syntax is OK  */
      bits=dn->bits;               /* for copy-back  */
      } /* last==NULL  */

     else {/*31*/Din_Go(336,2048);if (*c!='\0') {          /* more to process...  */
      /* had some digits; exponent is only valid sequence now  */
      Flag nege;                   /* 1=negative exponent  */
      Din_Go(337,2048);const char *firstexp;        /* -> first significant exponent digit  */
      status=DEC_Conversion_syntax;/* assume the worst  */
      Din_Go(339,2048);if (*c!='e' && *c!='E') {/*33*/Din_Go(338,2048);break;/*34*/}
      /* Found 'e' or 'E' -- now process explicit exponent */
      /* 1998.07.11: sign no longer required  */
      Din_Go(340,2048);nege=0;
      c++;                         /* to (possible) sign  */
      Din_Go(344,2048);if (*c=='-') {Din_Go(341,2048);nege=1; c++;}
       else {/*35*/Din_Go(342,2048);if (*c=='+') {/*37*/Din_Go(343,2048);c++;/*38*/}/*36*/}
      Din_Go(346,2048);if (*c=='\0') {/*39*/Din_Go(345,2048);break;/*40*/}

      Din_Go(348,2048);for (; *c=='0' && *(c+1)!='\0';) {/*41*/Din_Go(347,2048);c++;/*42*/}  /* strip insignificant zeros  */
      Din_Go(349,2048);firstexp=c;                            /* save exponent digit place  */
      Din_Go(353,2048);for (; ;c++) {
        Din_Go(350,2048);if (*c<'0' || *c>'9') {/*43*/Din_Go(351,2048);break;/*44*/}         /* not a digit  */
        Din_Go(352,2048);exponent=X10(exponent)+(Int)*c-(Int)'0';
        } /* c  */
      /* if not now on a '\0', *c must not be a digit  */
      Din_Go(355,2048);if (*c!='\0') {/*45*/Din_Go(354,2048);break;/*46*/}

      /* (this next test must be after the syntax checks)  */
      /* if it was too long the exponent may have wrapped, so check  */
      /* carefully and set it to a certain overflow if wrap possible  */
      Din_Go(358,2048);if (c>=firstexp+9+1) {
        Din_Go(356,2048);if (c>firstexp+9+1 || *firstexp>'1') {/*47*/Din_Go(357,2048);exponent=DECNUMMAXE*2;/*48*/}
        /* [up to 1999999999 is OK, for example 1E-1000000998]  */
        }
      Din_Go(360,2048);if (nege) {/*49*/Din_Go(359,2048);exponent=-exponent;/*50*/}     /* was negative  */
      Din_Go(361,2048);status=0;                         /* is OK  */
      ;/*32*/}} /* stuff after digits  */

    /* Here when whole string has been inspected; syntax is good  */
    /* cfirst->first digit (never dot), last->last digit (ditto)  */

    /* strip leading zeros/dot [leave final 0 if all 0's]  */
    Din_Go(369,2048);if (*cfirst=='0') {                 /* [cfirst has stepped over .]  */
      Din_Go(363,2048);for (c=cfirst; c<last; c++, cfirst++) {
        Din_Go(364,2048);if (*c=='.') {/*51*/Din_Go(365,2048);continue;/*52*/}          /* ignore dots  */
        Din_Go(367,2048);if (*c!='0') {/*53*/Din_Go(366,2048);break;/*54*/}             /* non-zero found  */
        Din_Go(368,2048);d--;                            /* 0 stripped  */
        } /* c  */
      #if DECSUBSET
      /* make a rapid exit for easy zeros if !extended  */
      if (*cfirst=='0' && !set->extended) {
        uprv_decNumberZero(dn);              /* clean result  */
        break;                          /* [could be return]  */
        }
      #endif
      } /* at least one leading 0  */

    /* Handle decimal point...  */
    Din_Go(371,2048);if (dotchar!=NULL && dotchar<last)  /* non-trailing '.' found?  */
      {/*55*/Din_Go(370,2048);exponent-=(last-dotchar);/*56*/}         /* adjust exponent  */
    /* [we can now ignore the .]  */

    /* OK, the digits string is good.  Assemble in the decNumber, or in  */
    /* a temporary units array if rounding is needed  */
    Din_Go(380,2048);if (d<=set->digits) {/*57*/Din_Go(372,2048);res=dn->lsu;/*58*/}    /* fits into supplied decNumber  */
     else {                             /* rounding needed  */
      Int needbytes=D2U(d)*sizeof(Unit);/* bytes needed  */
      Din_Go(373,2048);res=resbuff;                      /* assume use local buffer  */
      Din_Go(379,2048);if (needbytes>(Int)sizeof(resbuff)) { /* too big for local  */
        Din_Go(374,2048);allocres=(Unit *)malloc(needbytes);
        Din_Go(377,2048);if (allocres==NULL) {Din_Go(375,2048);status|=DEC_Insufficient_storage; Din_Go(376,2048);break;}
        Din_Go(378,2048);res=allocres;
        }
      }
    /* res now -> number lsu, buffer, or allocated storage for Unit array  */

    /* Place the coefficient into the selected Unit array  */
    /* [this is often 70% of the cost of this function when DECDPUN>1]  */
    #if DECDPUN>1
    out=0;                         /* accumulator  */
    up=res+D2U(d)-1;               /* -> msu  */
    cut=d-(up-res)*DECDPUN;        /* digits in top unit  */
    for (c=cfirst;; c++) {         /* along the digits  */
      if (*c=='.') continue;       /* ignore '.' [don't decrement cut]  */
      out=X10(out)+(Int)*c-(Int)'0';
      if (c==last) break;          /* done [never get to trailing '.']  */
      cut--;
      if (cut>0) continue;         /* more for this unit  */
      *up=(Unit)out;               /* write unit  */
      up--;                        /* prepare for unit below..  */
      cut=DECDPUN;                 /* ..  */
      out=0;                       /* ..  */
      } /* c  */
    *up=(Unit)out;                 /* write lsu  */

    #else
    /* DECDPUN==1  */
    Din_Go(381,2048);up=res;                        /* -> lsu  */
    Din_Go(385,2048);for (c=last; c>=cfirst; c--) { /* over each character, from least  */
      Din_Go(382,2048);if (*c=='.') {/*59*/Din_Go(383,2048);continue;/*60*/}       /* ignore . [don't step up]  */
      Din_Go(384,2048);*up=(Unit)((Int)*c-(Int)'0');
      up++;
      } /* c  */
    #endif

    Din_Go(386,2048);dn->bits=bits;
    dn->exponent=exponent;
    dn->digits=d;

    /* if not in number (too long) shorten into the number  */
    Din_Go(390,2048);if (d>set->digits) {
      Din_Go(387,2048);residue=0;
      decSetCoeff(dn, set, res, d, &residue, &status);
      /* always check for overflow or subnormal and round as needed  */
      decFinalize(dn, set, &residue, &status);
      }
     else { /* no rounding, but may still have overflow or subnormal  */
      /* [these tests are just for performance; finalize repeats them]  */
      Din_Go(388,2048);if ((dn->exponent-1<set->emin-dn->digits)
       || (dn->exponent-1>set->emax-set->digits)) {
        Din_Go(389,2048);residue=0;
        decFinalize(dn, set, &residue, &status);
        }
      }
    /* decNumberShow(dn);  */
    } while(0);                         /* [for break]  */

  if (allocres!=NULL) free(allocres);   /* drop any storage used  */
  Din_Go(393,2048);if (status!=0) {/*61*/Din_Go(392,2048);decStatus(dn, status, set);/*62*/}
  {decNumber * ReplaceReturn134 = dn; Din_Go(394,2048); return ReplaceReturn134;}
  } /* decNumberFromString */

/* ================================================================== */
/* Operators                                                          */
/* ================================================================== */

/* ------------------------------------------------------------------ */
/* decNumberAbs -- absolute value operator                            */
/*                                                                    */
/*   This computes C = abs(A)                                         */
/*                                                                    */
/*   res is C, the result.  C may be A                                */
/*   rhs is A                                                         */
/*   set is the context                                               */
/*                                                                    */
/* See also decNumberCopyAbs for a quiet bitwise version of this.     */
/* C must have space for set->digits digits.                          */
/* ------------------------------------------------------------------ */
/* This has the same effect as decNumberPlus unless A is negative,    */
/* in which case it has the same effect as decNumberMinus.            */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberAbs(decNumber *res, const decNumber *rhs,
                         decContext *set) {
  Din_Go(395,2048);decNumber dzero;                      /* for 0  */
  uInt status=0;                        /* accumulator  */

  #if DECCHECK
  if (decCheckOperands(res, DECUNUSED, rhs, set)) return res;
  #endif

  uprv_decNumberZero(&dzero);                /* set 0  */
  dzero.exponent=rhs->exponent;         /* [no coefficient expansion]  */
  decAddOp(res, &dzero, rhs, set, (uByte)(rhs->bits & DECNEG), &status);
  Din_Go(397,2048);if (status!=0) {/*83*/Din_Go(396,2048);decStatus(res, status, set);/*84*/}
  #if DECCHECK
  decCheckInexact(res, set);
  #endif
  {decNumber * ReplaceReturn133 = res; Din_Go(398,2048); return ReplaceReturn133;}
  } /* decNumberAbs  */

/* ------------------------------------------------------------------ */
/* decNumberAdd -- add two Numbers                                    */
/*                                                                    */
/*   This computes C = A + B                                          */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X+X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/* ------------------------------------------------------------------ */
/* This just calls the routine shared with Subtract                   */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberAdd(decNumber *res, const decNumber *lhs,
                         const decNumber *rhs, decContext *set) {
Din_Go(399,2048);  uInt status=0;                        /* accumulator  */
  decAddOp(res, lhs, rhs, set, 0, &status);
  Din_Go(401,2048);if (status!=0) {/*85*/Din_Go(400,2048);decStatus(res, status, set);/*86*/}
  #if DECCHECK
  decCheckInexact(res, set);
  #endif
  {decNumber * ReplaceReturn132 = res; Din_Go(402,2048); return ReplaceReturn132;}
  } /* decNumberAdd  */

/* ------------------------------------------------------------------ */
/* decNumberAnd -- AND two Numbers, digitwise                         */
/*                                                                    */
/*   This computes C = A & B                                          */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X&X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context (used for result length and error report)     */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/*                                                                    */
/* Logical function restrictions apply (see above); a NaN is          */
/* returned with Invalid_operation if a restriction is violated.      */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberAnd(decNumber *res, const decNumber *lhs,
                         const decNumber *rhs, decContext *set) {
  Din_Go(403,2048);const Unit *ua, *ub;                  /* -> operands  */
  const Unit *msua, *msub;              /* -> operand msus  */
  Unit *uc,  *msuc;                     /* -> result and its msu  */
  Int   msudigs;                        /* digits in res msu  */
  #if DECCHECK
  if (decCheckOperands(res, lhs, rhs, set)) return res;
  #endif

  Din_Go(406,2048);if (lhs->exponent!=0 || decNumberIsSpecial(lhs) || decNumberIsNegative(lhs)
   || rhs->exponent!=0 || decNumberIsSpecial(rhs) || decNumberIsNegative(rhs)) {
    Din_Go(404,2048);decStatus(res, DEC_Invalid_operation, set);
    {decNumber * ReplaceReturn131 = res; Din_Go(405,2048); return ReplaceReturn131;}
    }

  /* operands are valid  */
  Din_Go(407,2048);ua=lhs->lsu;                          /* bottom-up  */
  ub=rhs->lsu;                          /* ..  */
  uc=res->lsu;                          /* ..  */
  msua=ua+D2U(lhs->digits)-1;           /* -> msu of lhs  */
  msub=ub+D2U(rhs->digits)-1;           /* -> msu of rhs  */
  msuc=uc+D2U(set->digits)-1;           /* -> msu of result  */
  msudigs=MSUDIGITS(set->digits);       /* [faster than remainder]  */
  Din_Go(426,2048);for (; uc<=msuc; ua++, ub++, uc++) {  /* Unit loop  */
    Unit a, b;                          /* extract units  */
    Din_Go(410,2048);if (ua>msua) {/*87*/Din_Go(408,2048);a=0;/*88*/}
     else {/*89*/Din_Go(409,2048);a=*ua;/*90*/}
    Din_Go(413,2048);if (ub>msub) {/*91*/Din_Go(411,2048);b=0;/*92*/}
     else {/*93*/Din_Go(412,2048);b=*ub;/*94*/}
    Din_Go(414,2048);*uc=0;                              /* can now write back  */
    Din_Go(425,2048);if (a|b) {                          /* maybe 1 bits to examine  */
      Int i, j;
      Din_Go(415,2048);*uc=0;                            /* can now write back  */
      /* This loop could be unrolled and/or use BIN2BCD tables  */
      Din_Go(424,2048);for (i=0; i<DECDPUN; i++) {
        Din_Go(416,2048);if (a&b&1) {/*95*/Din_Go(417,2048);*uc=*uc+(Unit)powers[i];/*96*/}  /* effect AND  */
        Din_Go(418,2048);j=a%10;
        a=a/10;
        j|=b%10;
        b=b/10;
        Din_Go(421,2048);if (j>1) {
          Din_Go(419,2048);decStatus(res, DEC_Invalid_operation, set);
          {decNumber * ReplaceReturn130 = res; Din_Go(420,2048); return ReplaceReturn130;}
          }
        Din_Go(423,2048);if (uc==msuc && i==msudigs-1) {/*97*/Din_Go(422,2048);break;/*98*/} /* just did final digit  */
        } /* each digit  */
      } /* both OK  */
    } /* each unit  */
  /* [here uc-1 is the msu of the result]  */
  Din_Go(427,2048);res->digits=decGetDigits(res->lsu, uc-res->lsu);
  res->exponent=0;                      /* integer  */
  res->bits=0;                          /* sign=0  */
  {decNumber * ReplaceReturn129 = res; Din_Go(428,2048); return ReplaceReturn129;}  /* [no status to set]  */
  } /* decNumberAnd  */

/* ------------------------------------------------------------------ */
/* decNumberCompare -- compare two Numbers                            */
/*                                                                    */
/*   This computes C = A ? B                                          */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X?X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for one digit (or NaN).                          */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberCompare(decNumber *res, const decNumber *lhs,
                             const decNumber *rhs, decContext *set) {
Din_Go(429,2048);  uInt status=0;                        /* accumulator  */
  decCompareOp(res, lhs, rhs, set, COMPARE, &status);
  Din_Go(431,2048);if (status!=0) {/*99*/Din_Go(430,2048);decStatus(res, status, set);/*100*/}
  {decNumber * ReplaceReturn128 = res; Din_Go(432,2048); return ReplaceReturn128;}
  } /* decNumberCompare  */

/* ------------------------------------------------------------------ */
/* decNumberCompareSignal -- compare, signalling on all NaNs          */
/*                                                                    */
/*   This computes C = A ? B                                          */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X?X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for one digit (or NaN).                          */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberCompareSignal(decNumber *res, const decNumber *lhs,
                                   const decNumber *rhs, decContext *set) {
Din_Go(433,2048);  uInt status=0;                        /* accumulator  */
  decCompareOp(res, lhs, rhs, set, COMPSIG, &status);
  Din_Go(435,2048);if (status!=0) {/*101*/Din_Go(434,2048);decStatus(res, status, set);/*102*/}
  {decNumber * ReplaceReturn127 = res; Din_Go(436,2048); return ReplaceReturn127;}
  } /* decNumberCompareSignal  */

/* ------------------------------------------------------------------ */
/* decNumberCompareTotal -- compare two Numbers, using total ordering */
/*                                                                    */
/*   This computes C = A ? B, under total ordering                    */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X?X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for one digit; the result will always be one of  */
/* -1, 0, or 1.                                                       */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberCompareTotal(decNumber *res, const decNumber *lhs,
                                  const decNumber *rhs, decContext *set) {
Din_Go(437,2048);  uInt status=0;                        /* accumulator  */
  decCompareOp(res, lhs, rhs, set, COMPTOTAL, &status);
  Din_Go(439,2048);if (status!=0) {/*103*/Din_Go(438,2048);decStatus(res, status, set);/*104*/}
  {decNumber * ReplaceReturn126 = res; Din_Go(440,2048); return ReplaceReturn126;}
  } /* decNumberCompareTotal  */

/* ------------------------------------------------------------------ */
/* decNumberCompareTotalMag -- compare, total ordering of magnitudes  */
/*                                                                    */
/*   This computes C = |A| ? |B|, under total ordering                */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X?X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for one digit; the result will always be one of  */
/* -1, 0, or 1.                                                       */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberCompareTotalMag(decNumber *res, const decNumber *lhs,
                                     const decNumber *rhs, decContext *set) {
  uInt status=0;                   /* accumulator  */
  uInt needbytes;                  /* for space calculations  */
  decNumber bufa[D2N(DECBUFFER+1)];/* +1 in case DECBUFFER=0  */
  decNumber *allocbufa=NULL;       /* -> allocated bufa, iff allocated  */
  decNumber bufb[D2N(DECBUFFER+1)];
  decNumber *allocbufb=NULL;       /* -> allocated bufb, iff allocated  */
  decNumber *a, *b;                /* temporary pointers  */

  #if DECCHECK
  if (decCheckOperands(res, lhs, rhs, set)) return res;
  #endif

  Din_Go(460,2048);do {                                  /* protect allocated storage  */
    /* if either is negative, take a copy and absolute  */
    Din_Go(441,2048);if (decNumberIsNegative(lhs)) {     /* lhs<0  */
      Din_Go(442,2048);a=bufa;
      needbytes=sizeof(decNumber)+(D2U(lhs->digits)-1)*sizeof(Unit);
      Din_Go(448,2048);if (needbytes>sizeof(bufa)) {     /* need malloc space  */
        Din_Go(443,2048);allocbufa=(decNumber *)malloc(needbytes);
        Din_Go(446,2048);if (allocbufa==NULL) {          /* hopeless -- abandon  */
          Din_Go(444,2048);status|=DEC_Insufficient_storage;
          Din_Go(445,2048);break;}
        Din_Go(447,2048);a=allocbufa;                    /* use the allocated space  */
        }
      uprv_decNumberCopy(a, lhs);            /* copy content  */
      Din_Go(449,2048);a->bits&=~DECNEG;                 /* .. and clear the sign  */
      lhs=a;                            /* use copy from here on  */
      }
    Din_Go(458,2048);if (decNumberIsNegative(rhs)) {     /* rhs<0  */
      Din_Go(450,2048);b=bufb;
      needbytes=sizeof(decNumber)+(D2U(rhs->digits)-1)*sizeof(Unit);
      Din_Go(456,2048);if (needbytes>sizeof(bufb)) {     /* need malloc space  */
        Din_Go(451,2048);allocbufb=(decNumber *)malloc(needbytes);
        Din_Go(454,2048);if (allocbufb==NULL) {          /* hopeless -- abandon  */
          Din_Go(452,2048);status|=DEC_Insufficient_storage;
          Din_Go(453,2048);break;}
        Din_Go(455,2048);b=allocbufb;                    /* use the allocated space  */
        }
      uprv_decNumberCopy(b, rhs);            /* copy content  */
      Din_Go(457,2048);b->bits&=~DECNEG;                 /* .. and clear the sign  */
      rhs=b;                            /* use copy from here on  */
      }
    Din_Go(459,2048);decCompareOp(res, lhs, rhs, set, COMPTOTAL, &status);
    } while(0);                         /* end protected  */

  if (allocbufa!=NULL) free(allocbufa); /* drop any storage used  */
  if (allocbufb!=NULL) free(allocbufb); /* ..  */
  Din_Go(462,2048);if (status!=0) {/*105*/Din_Go(461,2048);decStatus(res, status, set);/*106*/}
  {decNumber * ReplaceReturn125 = res; Din_Go(463,2048); return ReplaceReturn125;}
  } /* decNumberCompareTotalMag  */

/* ------------------------------------------------------------------ */
/* decNumberDivide -- divide one number by another                    */
/*                                                                    */
/*   This computes C = A / B                                          */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X/X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberDivide(decNumber *res, const decNumber *lhs,
                            const decNumber *rhs, decContext *set) {
Din_Go(464,2048);  uInt status=0;                        /* accumulator  */
  decDivideOp(res, lhs, rhs, set, DIVIDE, &status);
  Din_Go(466,2048);if (status!=0) {/*107*/Din_Go(465,2048);decStatus(res, status, set);/*108*/}
  #if DECCHECK
  decCheckInexact(res, set);
  #endif
  {decNumber * ReplaceReturn124 = res; Din_Go(467,2048); return ReplaceReturn124;}
  } /* decNumberDivide  */

/* ------------------------------------------------------------------ */
/* decNumberDivideInteger -- divide and return integer quotient       */
/*                                                                    */
/*   This computes C = A # B, where # is the integer divide operator  */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X#X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberDivideInteger(decNumber *res, const decNumber *lhs,
                                   const decNumber *rhs, decContext *set) {
Din_Go(468,2048);  uInt status=0;                        /* accumulator  */
  decDivideOp(res, lhs, rhs, set, DIVIDEINT, &status);
  Din_Go(470,2048);if (status!=0) {/*109*/Din_Go(469,2048);decStatus(res, status, set);/*110*/}
  {decNumber * ReplaceReturn123 = res; Din_Go(471,2048); return ReplaceReturn123;}
  } /* decNumberDivideInteger  */

/* ------------------------------------------------------------------ */
/* decNumberExp -- exponentiation                                     */
/*                                                                    */
/*   This computes C = exp(A)                                         */
/*                                                                    */
/*   res is C, the result.  C may be A                                */
/*   rhs is A                                                         */
/*   set is the context; note that rounding mode has no effect        */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/*                                                                    */
/* Mathematical function restrictions apply (see above); a NaN is     */
/* returned with Invalid_operation if a restriction is violated.      */
/*                                                                    */
/* Finite results will always be full precision and Inexact, except   */
/* when A is a zero or -Infinity (giving 1 or 0 respectively).        */
/*                                                                    */
/* An Inexact result is rounded using DEC_ROUND_HALF_EVEN; it will    */
/* almost always be correctly rounded, but may be up to 1 ulp in      */
/* error in rare cases.                                               */
/* ------------------------------------------------------------------ */
/* This is a wrapper for decExpOp which can handle the slightly wider */
/* (double) range needed by Ln (which has to be able to calculate     */
/* exp(-a) where a can be the tiniest number (Ntiny).                 */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberExp(decNumber *res, const decNumber *rhs,
                         decContext *set) {
Din_Go(472,2048);  uInt status=0;                        /* accumulator  */
  #if DECSUBSET
  decNumber *allocrhs=NULL;        /* non-NULL if rounded rhs allocated  */
  #endif

  #if DECCHECK
  if (decCheckOperands(res, DECUNUSED, rhs, set)) return res;
  #endif

  /* Check restrictions; these restrictions ensure that if h=8 (see  */
  /* decExpOp) then the result will either overflow or underflow to 0.  */
  /* Other math functions restrict the input range, too, for inverses.  */
  /* If not violated then carry out the operation.  */
  Din_Go(475,2048);if (!decCheckMath(rhs, set, &status)) {/*111*/Din_Go(473,2048);do { /* protect allocation  */
    #if DECSUBSET
    if (!set->extended) {
      /* reduce operand and set lostDigits status, as needed  */
      if (rhs->digits>set->digits) {
        allocrhs=decRoundOperand(rhs, set, &status);
        if (allocrhs==NULL) break;
        rhs=allocrhs;
        }
      }
    #endif
    Din_Go(474,2048);decExpOp(res, rhs, set, &status);
    } while(0);/*112*/}                         /* end protected  */

  #if DECSUBSET
  if (allocrhs !=NULL) free(allocrhs);  /* drop any storage used  */
  #endif
  /* apply significant status  */
  Din_Go(477,2048);if (status!=0) {/*113*/Din_Go(476,2048);decStatus(res, status, set);/*114*/}
  #if DECCHECK
  decCheckInexact(res, set);
  #endif
  {decNumber * ReplaceReturn122 = res; Din_Go(478,2048); return ReplaceReturn122;}
  } /* decNumberExp  */

/* ------------------------------------------------------------------ */
/* decNumberFMA -- fused multiply add                                 */
/*                                                                    */
/*   This computes D = (A * B) + C with only one rounding             */
/*                                                                    */
/*   res is D, the result.  D may be A or B or C (e.g., X=FMA(X,X,X)) */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   fhs is C [far hand side]                                         */
/*   set is the context                                               */
/*                                                                    */
/* Mathematical function restrictions apply (see above); a NaN is     */
/* returned with Invalid_operation if a restriction is violated.      */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberFMA(decNumber *res, const decNumber *lhs,
                         const decNumber *rhs, const decNumber *fhs,
                         decContext *set) {
Din_Go(479,2048);  uInt status=0;                   /* accumulator  */
  decContext dcmul;                /* context for the multiplication  */
  uInt needbytes;                  /* for space calculations  */
  decNumber bufa[D2N(DECBUFFER*2+1)];
  decNumber *allocbufa=NULL;       /* -> allocated bufa, iff allocated  */
  decNumber *acc;                  /* accumulator pointer  */
  decNumber dzero;                 /* work  */

  #if DECCHECK
  if (decCheckOperands(res, lhs, rhs, set)) return res;
  if (decCheckOperands(res, fhs, DECUNUSED, set)) return res;
  #endif

  Din_Go(496,2048);do {                                  /* protect allocated storage  */
    #if DECSUBSET
    if (!set->extended) {               /* [undefined if subset]  */
      status|=DEC_Invalid_operation;
      break;}
    #endif
    /* Check math restrictions [these ensure no overflow or underflow]  */
    Din_Go(480,2048);if ((!decNumberIsSpecial(lhs) && decCheckMath(lhs, set, &status))
     || (!decNumberIsSpecial(rhs) && decCheckMath(rhs, set, &status))
     || (!decNumberIsSpecial(fhs) && decCheckMath(fhs, set, &status))) {/*115*/Din_Go(481,2048);break;/*116*/}
    /* set up context for multiply  */
    Din_Go(482,2048);dcmul=*set;
    dcmul.digits=lhs->digits+rhs->digits; /* just enough  */
    /* [The above may be an over-estimate for subset arithmetic, but that's OK]  */
    dcmul.emax=DEC_MAX_EMAX;            /* effectively unbounded ..  */
    dcmul.emin=DEC_MIN_EMIN;            /* [thanks to Math restrictions]  */
    /* set up decNumber space to receive the result of the multiply  */
    acc=bufa;                           /* may fit  */
    needbytes=sizeof(decNumber)+(D2U(dcmul.digits)-1)*sizeof(Unit);
    Din_Go(488,2048);if (needbytes>sizeof(bufa)) {       /* need malloc space  */
      Din_Go(483,2048);allocbufa=(decNumber *)malloc(needbytes);
      Din_Go(486,2048);if (allocbufa==NULL) {            /* hopeless -- abandon  */
        Din_Go(484,2048);status|=DEC_Insufficient_storage;
        Din_Go(485,2048);break;}
      Din_Go(487,2048);acc=allocbufa;                    /* use the allocated space  */
      }
    /* multiply with extended range and necessary precision  */
    /*printf("emin=%ld\n", dcmul.emin);  */
    Din_Go(489,2048);decMultiplyOp(acc, lhs, rhs, &dcmul, &status);
    /* Only Invalid operation (from sNaN or Inf * 0) is possible in  */
    /* status; if either is seen than ignore fhs (in case it is  */
    /* another sNaN) and set acc to NaN unless we had an sNaN  */
    /* [decMultiplyOp leaves that to caller]  */
    /* Note sNaN has to go through addOp to shorten payload if  */
    /* necessary  */
    Din_Go(494,2048);if ((status&DEC_Invalid_operation)!=0) {
      Din_Go(490,2048);if (!(status&DEC_sNaN)) {         /* but be true invalid  */
        uprv_decNumberZero(res);             /* acc not yet set  */
        Din_Go(491,2048);res->bits=DECNAN;
        Din_Go(492,2048);break;
        }
      uprv_decNumberZero(&dzero);            /* make 0 (any non-NaN would do)  */
      Din_Go(493,2048);fhs=&dzero;                       /* use that  */
      }
    #if DECCHECK
     else { /* multiply was OK  */
      if (status!=0) printf("Status=%08lx after FMA multiply\n", (LI)status);
      }
    #endif
    /* add the third operand and result -> res, and all is done  */
    Din_Go(495,2048);decAddOp(res, acc, fhs, set, 0, &status);
    } while(0);                         /* end protected  */

  if (allocbufa!=NULL) free(allocbufa); /* drop any storage used  */
  Din_Go(498,2048);if (status!=0) {/*117*/Din_Go(497,2048);decStatus(res, status, set);/*118*/}
  #if DECCHECK
  decCheckInexact(res, set);
  #endif
  {decNumber * ReplaceReturn121 = res; Din_Go(499,2048); return ReplaceReturn121;}
  } /* decNumberFMA  */

/* ------------------------------------------------------------------ */
/* decNumberInvert -- invert a Number, digitwise                      */
/*                                                                    */
/*   This computes C = ~A                                             */
/*                                                                    */
/*   res is C, the result.  C may be A (e.g., X=~X)                   */
/*   rhs is A                                                         */
/*   set is the context (used for result length and error report)     */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/*                                                                    */
/* Logical function restrictions apply (see above); a NaN is          */
/* returned with Invalid_operation if a restriction is violated.      */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberInvert(decNumber *res, const decNumber *rhs,
                            decContext *set) {
  Din_Go(500,2048);const Unit *ua, *msua;                /* -> operand and its msu  */
  Unit  *uc, *msuc;                     /* -> result and its msu  */
  Int   msudigs;                        /* digits in res msu  */
  #if DECCHECK
  if (decCheckOperands(res, DECUNUSED, rhs, set)) return res;
  #endif

  Din_Go(503,2048);if (rhs->exponent!=0 || decNumberIsSpecial(rhs) || decNumberIsNegative(rhs)) {
    Din_Go(501,2048);decStatus(res, DEC_Invalid_operation, set);
    {decNumber * ReplaceReturn120 = res; Din_Go(502,2048); return ReplaceReturn120;}
    }
  /* operand is valid  */
  Din_Go(504,2048);ua=rhs->lsu;                          /* bottom-up  */
  uc=res->lsu;                          /* ..  */
  msua=ua+D2U(rhs->digits)-1;           /* -> msu of rhs  */
  msuc=uc+D2U(set->digits)-1;           /* -> msu of result  */
  msudigs=MSUDIGITS(set->digits);       /* [faster than remainder]  */
  Din_Go(518,2048);for (; uc<=msuc; ua++, uc++) {        /* Unit loop  */
    Unit a;                             /* extract unit  */
    Int  i, j;                          /* work  */
    Din_Go(507,2048);if (ua>msua) {/*119*/Din_Go(505,2048);a=0;/*120*/}
     else {/*121*/Din_Go(506,2048);a=*ua;/*122*/}
    Din_Go(508,2048);*uc=0;                              /* can now write back  */
    /* always need to examine all bits in rhs  */
    /* This loop could be unrolled and/or use BIN2BCD tables  */
    Din_Go(517,2048);for (i=0; i<DECDPUN; i++) {
      Din_Go(509,2048);if ((~a)&1) {/*123*/Din_Go(510,2048);*uc=*uc+(Unit)powers[i];/*124*/}   /* effect INVERT  */
      Din_Go(511,2048);j=a%10;
      a=a/10;
      Din_Go(514,2048);if (j>1) {
        Din_Go(512,2048);decStatus(res, DEC_Invalid_operation, set);
        {decNumber * ReplaceReturn119 = res; Din_Go(513,2048); return ReplaceReturn119;}
        }
      Din_Go(516,2048);if (uc==msuc && i==msudigs-1) {/*125*/Din_Go(515,2048);break;/*126*/}   /* just did final digit  */
      } /* each digit  */
    } /* each unit  */
  /* [here uc-1 is the msu of the result]  */
  Din_Go(519,2048);res->digits=decGetDigits(res->lsu, uc-res->lsu);
  res->exponent=0;                      /* integer  */
  res->bits=0;                          /* sign=0  */
  {decNumber * ReplaceReturn118 = res; Din_Go(520,2048); return ReplaceReturn118;}  /* [no status to set]  */
  } /* decNumberInvert  */

/* ------------------------------------------------------------------ */
/* decNumberLn -- natural logarithm                                   */
/*                                                                    */
/*   This computes C = ln(A)                                          */
/*                                                                    */
/*   res is C, the result.  C may be A                                */
/*   rhs is A                                                         */
/*   set is the context; note that rounding mode has no effect        */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/*                                                                    */
/* Notable cases:                                                     */
/*   A<0 -> Invalid                                                   */
/*   A=0 -> -Infinity (Exact)                                         */
/*   A=+Infinity -> +Infinity (Exact)                                 */
/*   A=1 exactly -> 0 (Exact)                                         */
/*                                                                    */
/* Mathematical function restrictions apply (see above); a NaN is     */
/* returned with Invalid_operation if a restriction is violated.      */
/*                                                                    */
/* An Inexact result is rounded using DEC_ROUND_HALF_EVEN; it will    */
/* almost always be correctly rounded, but may be up to 1 ulp in      */
/* error in rare cases.                                               */
/* ------------------------------------------------------------------ */
/* This is a wrapper for decLnOp which can handle the slightly wider  */
/* (+11) range needed by Ln, Log10, etc. (which may have to be able   */
/* to calculate at p+e+2).                                            */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberLn(decNumber *res, const decNumber *rhs,
                        decContext *set) {
Din_Go(521,2048);  uInt status=0;                   /* accumulator  */
  #if DECSUBSET
  decNumber *allocrhs=NULL;        /* non-NULL if rounded rhs allocated  */
  #endif

  #if DECCHECK
  if (decCheckOperands(res, DECUNUSED, rhs, set)) return res;
  #endif

  /* Check restrictions; this is a math function; if not violated  */
  /* then carry out the operation.  */
  Din_Go(524,2048);if (!decCheckMath(rhs, set, &status)) {/*127*/Din_Go(522,2048);do { /* protect allocation  */
    #if DECSUBSET
    if (!set->extended) {
      /* reduce operand and set lostDigits status, as needed  */
      if (rhs->digits>set->digits) {
        allocrhs=decRoundOperand(rhs, set, &status);
        if (allocrhs==NULL) break;
        rhs=allocrhs;
        }
      /* special check in subset for rhs=0  */
      if (ISZERO(rhs)) {                /* +/- zeros -> error  */
        status|=DEC_Invalid_operation;
        break;}
      } /* extended=0  */
    #endif
    Din_Go(523,2048);decLnOp(res, rhs, set, &status);
    } while(0);/*128*/}                         /* end protected  */

  #if DECSUBSET
  if (allocrhs !=NULL) free(allocrhs);  /* drop any storage used  */
  #endif
  /* apply significant status  */
  Din_Go(526,2048);if (status!=0) {/*129*/Din_Go(525,2048);decStatus(res, status, set);/*130*/}
  #if DECCHECK
  decCheckInexact(res, set);
  #endif
  {decNumber * ReplaceReturn117 = res; Din_Go(527,2048); return ReplaceReturn117;}
  } /* decNumberLn  */

/* ------------------------------------------------------------------ */
/* decNumberLogB - get adjusted exponent, by 754 rules                */
/*                                                                    */
/*   This computes C = adjustedexponent(A)                            */
/*                                                                    */
/*   res is C, the result.  C may be A                                */
/*   rhs is A                                                         */
/*   set is the context, used only for digits and status              */
/*                                                                    */
/* C must have space for 10 digits (A might have 10**9 digits and     */
/* an exponent of +999999999, or one digit and an exponent of         */
/* -1999999999).                                                      */
/*                                                                    */
/* This returns the adjusted exponent of A after (in theory) padding  */
/* with zeros on the right to set->digits digits while keeping the    */
/* same value.  The exponent is not limited by emin/emax.             */
/*                                                                    */
/* Notable cases:                                                     */
/*   A<0 -> Use |A|                                                   */
/*   A=0 -> -Infinity (Division by zero)                              */
/*   A=Infinite -> +Infinity (Exact)                                  */
/*   A=1 exactly -> 0 (Exact)                                         */
/*   NaNs are propagated as usual                                     */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberLogB(decNumber *res, const decNumber *rhs,
                          decContext *set) {
Din_Go(528,2048);  uInt status=0;                   /* accumulator  */

  #if DECCHECK
  if (decCheckOperands(res, DECUNUSED, rhs, set)) return res;
  #endif

  /* NaNs as usual; Infinities return +Infinity; 0->oops  */
  Din_Go(534,2048);if (decNumberIsNaN(rhs)) {/*131*/Din_Go(529,2048);decNaNs(res, rhs, NULL, set, &status);/*132*/}
   else {/*133*/Din_Go(530,2048);if (decNumberIsInfinite(rhs)) uprv_decNumberCopyAbs(res, rhs);
   else {/*135*/Din_Go(531,2048);if (decNumberIsZero(rhs)) {
    uprv_decNumberZero(res);                 /* prepare for Infinity  */
    Din_Go(532,2048);res->bits=DECNEG|DECINF;            /* -Infinity  */
    status|=DEC_Division_by_zero;       /* as per 754  */
    }
   else { /* finite non-zero  */
Din_Go(533,2048);    Int ae=rhs->exponent+rhs->digits-1; /* adjusted exponent  */
    uprv_decNumberFromInt32(res, ae);        /* lay it out  */
    ;/*136*/}/*134*/}}

  Din_Go(536,2048);if (status!=0) {/*137*/Din_Go(535,2048);decStatus(res, status, set);/*138*/}
  {decNumber * ReplaceReturn116 = res; Din_Go(537,2048); return ReplaceReturn116;}
  } /* decNumberLogB  */

/* ------------------------------------------------------------------ */
/* decNumberLog10 -- logarithm in base 10                             */
/*                                                                    */
/*   This computes C = log10(A)                                       */
/*                                                                    */
/*   res is C, the result.  C may be A                                */
/*   rhs is A                                                         */
/*   set is the context; note that rounding mode has no effect        */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/*                                                                    */
/* Notable cases:                                                     */
/*   A<0 -> Invalid                                                   */
/*   A=0 -> -Infinity (Exact)                                         */
/*   A=+Infinity -> +Infinity (Exact)                                 */
/*   A=10**n (if n is an integer) -> n (Exact)                        */
/*                                                                    */
/* Mathematical function restrictions apply (see above); a NaN is     */
/* returned with Invalid_operation if a restriction is violated.      */
/*                                                                    */
/* An Inexact result is rounded using DEC_ROUND_HALF_EVEN; it will    */
/* almost always be correctly rounded, but may be up to 1 ulp in      */
/* error in rare cases.                                               */
/* ------------------------------------------------------------------ */
/* This calculates ln(A)/ln(10) using appropriate precision.  For     */
/* ln(A) this is the max(p, rhs->digits + t) + 3, where p is the      */
/* requested digits and t is the number of digits in the exponent     */
/* (maximum 6).  For ln(10) it is p + 3; this is often handled by the */
/* fastpath in decLnOp.  The final division is done to the requested  */
/* precision.                                                         */
/* ------------------------------------------------------------------ */
#if defined(__clang__) || U_GCC_MAJOR_MINOR >= 406
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Warray-bounds"
#endif
U_CAPI decNumber * U_EXPORT2 uprv_decNumberLog10(decNumber *res, const decNumber *rhs,
                          decContext *set) {
  uInt status=0, ignore=0;         /* status accumulators  */
  uInt needbytes;                  /* for space calculations  */
  Int p;                           /* working precision  */
  Int t;                           /* digits in exponent of A  */

  /* buffers for a and b working decimals  */
  /* (adjustment calculator, same size)  */
  decNumber bufa[D2N(DECBUFFER+2)];
  decNumber *allocbufa=NULL;       /* -> allocated bufa, iff allocated  */
  decNumber *a=bufa;               /* temporary a  */
  decNumber bufb[D2N(DECBUFFER+2)];
  decNumber *allocbufb=NULL;       /* -> allocated bufb, iff allocated  */
  decNumber *b=bufb;               /* temporary b  */
  decNumber bufw[D2N(10)];         /* working 2-10 digit number  */
  decNumber *w=bufw;               /* ..  */
  #if DECSUBSET
  decNumber *allocrhs=NULL;        /* non-NULL if rounded rhs allocated  */
  #endif

  decContext aset;                 /* working context  */

  #if DECCHECK
  if (decCheckOperands(res, DECUNUSED, rhs, set)) return res;
  #endif

  /* Check restrictions; this is a math function; if not violated  */
  /* then carry out the operation.  */
  Din_Go(564,2048);if (!decCheckMath(rhs, set, &status)) {/*139*/Din_Go(538,2048);do { /* protect malloc  */
    #if DECSUBSET
    if (!set->extended) {
      /* reduce operand and set lostDigits status, as needed  */
      if (rhs->digits>set->digits) {
        allocrhs=decRoundOperand(rhs, set, &status);
        if (allocrhs==NULL) break;
        rhs=allocrhs;
        }
      /* special check in subset for rhs=0  */
      if (ISZERO(rhs)) {                /* +/- zeros -> error  */
        status|=DEC_Invalid_operation;
        break;}
      } /* extended=0  */
    #endif

    uprv_decContextDefault(&aset, DEC_INIT_DECIMAL64); /* clean context  */

    /* handle exact powers of 10; only check if +ve finite  */
    Din_Go(543,2048);if (!(rhs->bits&(DECNEG|DECSPECIAL)) && !ISZERO(rhs)) {
      Int residue=0;               /* (no residue)  */
      uInt copystat=0;             /* clean status  */

      /* round to a single digit...  */
      Din_Go(539,2048);aset.digits=1;
      decCopyFit(w, rhs, &aset, &residue, &copystat); /* copy & shorten  */
      /* if exact and the digit is 1, rhs is a power of 10  */
      Din_Go(542,2048);if (!(copystat&DEC_Inexact) && w->lsu[0]==1) {
        /* the exponent, conveniently, is the power of 10; making  */
        /* this the result needs a little care as it might not fit,  */
        /* so first convert it into the working number, and then move  */
        /* to res  */
        uprv_decNumberFromInt32(w, w->exponent);
        Din_Go(540,2048);residue=0;
        decCopyFit(res, w, set, &residue, &status); /* copy & round  */
        decFinish(res, set, &residue, &status);     /* cleanup/set flags  */
        Din_Go(541,2048);break;
        } /* not a power of 10  */
      } /* not a candidate for exact  */

    /* simplify the information-content calculation to use 'total  */
    /* number of digits in a, including exponent' as compared to the  */
    /* requested digits, as increasing this will only rarely cost an  */
    /* iteration in ln(a) anyway  */
    Din_Go(544,2048);t=6;                                /* it can never be >6  */

    /* allocate space when needed...  */
    p=(rhs->digits+t>set->digits?rhs->digits+t:set->digits)+3;
    needbytes=sizeof(decNumber)+(D2U(p)-1)*sizeof(Unit);
    Din_Go(550,2048);if (needbytes>sizeof(bufa)) {       /* need malloc space  */
      Din_Go(545,2048);allocbufa=(decNumber *)malloc(needbytes);
      Din_Go(548,2048);if (allocbufa==NULL) {            /* hopeless -- abandon  */
        Din_Go(546,2048);status|=DEC_Insufficient_storage;
        Din_Go(547,2048);break;}
      Din_Go(549,2048);a=allocbufa;                      /* use the allocated space  */
      }
    Din_Go(551,2048);aset.digits=p;                      /* as calculated  */
    aset.emax=DEC_MAX_MATH;             /* usual bounds  */
    aset.emin=-DEC_MAX_MATH;            /* ..  */
    aset.clamp=0;                       /* and no concrete format  */
    decLnOp(a, rhs, &aset, &status);    /* a=ln(rhs)  */

    /* skip the division if the result so far is infinite, NaN, or  */
    /* zero, or there was an error; note NaN from sNaN needs copy  */
    Din_Go(553,2048);if (status&DEC_NaNs && !(status&DEC_sNaN)) {/*141*/Din_Go(552,2048);break;/*142*/}
    Din_Go(555,2048);if (a->bits&DECSPECIAL || ISZERO(a)) {
      uprv_decNumberCopy(res, a);            /* [will fit]  */
      Din_Go(554,2048);break;}

    /* for ln(10) an extra 3 digits of precision are needed  */
    Din_Go(556,2048);p=set->digits+3;
    needbytes=sizeof(decNumber)+(D2U(p)-1)*sizeof(Unit);
    Din_Go(562,2048);if (needbytes>sizeof(bufb)) {       /* need malloc space  */
      Din_Go(557,2048);allocbufb=(decNumber *)malloc(needbytes);
      Din_Go(560,2048);if (allocbufb==NULL) {            /* hopeless -- abandon  */
        Din_Go(558,2048);status|=DEC_Insufficient_storage;
        Din_Go(559,2048);break;}
      Din_Go(561,2048);b=allocbufb;                      /* use the allocated space  */
      }
    uprv_decNumberZero(w);                   /* set up 10...  */
    #if DECDPUN==1
    Din_Go(563,2048);w->lsu[1]=1; w->lsu[0]=0;           /* ..  */
    #else
    w->lsu[0]=10;                       /* ..  */
    #endif
    w->digits=2;                        /* ..  */

    aset.digits=p;
    decLnOp(b, w, &aset, &ignore);      /* b=ln(10)  */

    aset.digits=set->digits;            /* for final divide  */
    decDivideOp(res, a, b, &aset, DIVIDE, &status); /* into result  */
    } while(0);/*140*/}                         /* [for break]  */

  if (allocbufa!=NULL) free(allocbufa); /* drop any storage used  */
  if (allocbufb!=NULL) free(allocbufb); /* ..  */
  #if DECSUBSET
  if (allocrhs !=NULL) free(allocrhs);  /* ..  */
  #endif
  /* apply significant status  */
  Din_Go(566,2048);if (status!=0) {/*143*/Din_Go(565,2048);decStatus(res, status, set);/*144*/}
  #if DECCHECK
  decCheckInexact(res, set);
  #endif
  {decNumber * ReplaceReturn115 = res; Din_Go(567,2048); return ReplaceReturn115;}
  } /* decNumberLog10  */
#if defined(__clang__) || U_GCC_MAJOR_MINOR >= 406
#pragma GCC diagnostic pop
#endif

/* ------------------------------------------------------------------ */
/* decNumberMax -- compare two Numbers and return the maximum         */
/*                                                                    */
/*   This computes C = A ? B, returning the maximum by 754 rules      */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X?X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberMax(decNumber *res, const decNumber *lhs,
                         const decNumber *rhs, decContext *set) {
Din_Go(568,2048);  uInt status=0;                        /* accumulator  */
  decCompareOp(res, lhs, rhs, set, COMPMAX, &status);
  Din_Go(570,2048);if (status!=0) {/*145*/Din_Go(569,2048);decStatus(res, status, set);/*146*/}
  #if DECCHECK
  decCheckInexact(res, set);
  #endif
  {decNumber * ReplaceReturn114 = res; Din_Go(571,2048); return ReplaceReturn114;}
  } /* decNumberMax  */

/* ------------------------------------------------------------------ */
/* decNumberMaxMag -- compare and return the maximum by magnitude     */
/*                                                                    */
/*   This computes C = A ? B, returning the maximum by 754 rules      */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X?X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberMaxMag(decNumber *res, const decNumber *lhs,
                         const decNumber *rhs, decContext *set) {
Din_Go(572,2048);  uInt status=0;                        /* accumulator  */
  decCompareOp(res, lhs, rhs, set, COMPMAXMAG, &status);
  Din_Go(574,2048);if (status!=0) {/*147*/Din_Go(573,2048);decStatus(res, status, set);/*148*/}
  #if DECCHECK
  decCheckInexact(res, set);
  #endif
  {decNumber * ReplaceReturn113 = res; Din_Go(575,2048); return ReplaceReturn113;}
  } /* decNumberMaxMag  */

/* ------------------------------------------------------------------ */
/* decNumberMin -- compare two Numbers and return the minimum         */
/*                                                                    */
/*   This computes C = A ? B, returning the minimum by 754 rules      */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X?X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberMin(decNumber *res, const decNumber *lhs,
                         const decNumber *rhs, decContext *set) {
Din_Go(576,2048);  uInt status=0;                        /* accumulator  */
  decCompareOp(res, lhs, rhs, set, COMPMIN, &status);
  Din_Go(578,2048);if (status!=0) {/*149*/Din_Go(577,2048);decStatus(res, status, set);/*150*/}
  #if DECCHECK
  decCheckInexact(res, set);
  #endif
  {decNumber * ReplaceReturn112 = res; Din_Go(579,2048); return ReplaceReturn112;}
  } /* decNumberMin  */

/* ------------------------------------------------------------------ */
/* decNumberMinMag -- compare and return the minimum by magnitude     */
/*                                                                    */
/*   This computes C = A ? B, returning the minimum by 754 rules      */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X?X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberMinMag(decNumber *res, const decNumber *lhs,
                         const decNumber *rhs, decContext *set) {
Din_Go(580,2048);  uInt status=0;                        /* accumulator  */
  decCompareOp(res, lhs, rhs, set, COMPMINMAG, &status);
  Din_Go(582,2048);if (status!=0) {/*151*/Din_Go(581,2048);decStatus(res, status, set);/*152*/}
  #if DECCHECK
  decCheckInexact(res, set);
  #endif
  {decNumber * ReplaceReturn111 = res; Din_Go(583,2048); return ReplaceReturn111;}
  } /* decNumberMinMag  */

/* ------------------------------------------------------------------ */
/* decNumberMinus -- prefix minus operator                            */
/*                                                                    */
/*   This computes C = 0 - A                                          */
/*                                                                    */
/*   res is C, the result.  C may be A                                */
/*   rhs is A                                                         */
/*   set is the context                                               */
/*                                                                    */
/* See also decNumberCopyNegate for a quiet bitwise version of this.  */
/* C must have space for set->digits digits.                          */
/* ------------------------------------------------------------------ */
/* Simply use AddOp for the subtract, which will do the necessary.    */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberMinus(decNumber *res, const decNumber *rhs,
                           decContext *set) {
  Din_Go(584,2048);decNumber dzero;
  uInt status=0;                        /* accumulator  */

  #if DECCHECK
  if (decCheckOperands(res, DECUNUSED, rhs, set)) return res;
  #endif

  uprv_decNumberZero(&dzero);                /* make 0  */
  dzero.exponent=rhs->exponent;         /* [no coefficient expansion]  */
  decAddOp(res, &dzero, rhs, set, DECNEG, &status);
  Din_Go(586,2048);if (status!=0) {/*153*/Din_Go(585,2048);decStatus(res, status, set);/*154*/}
  #if DECCHECK
  decCheckInexact(res, set);
  #endif
  {decNumber * ReplaceReturn110 = res; Din_Go(587,2048); return ReplaceReturn110;}
  } /* decNumberMinus  */

/* ------------------------------------------------------------------ */
/* decNumberNextMinus -- next towards -Infinity                       */
/*                                                                    */
/*   This computes C = A - infinitesimal, rounded towards -Infinity   */
/*                                                                    */
/*   res is C, the result.  C may be A                                */
/*   rhs is A                                                         */
/*   set is the context                                               */
/*                                                                    */
/* This is a generalization of 754 NextDown.                          */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberNextMinus(decNumber *res, const decNumber *rhs,
                               decContext *set) {
  Din_Go(588,2048);decNumber dtiny;                           /* constant  */
  decContext workset=*set;                   /* work  */
  uInt status=0;                             /* accumulator  */
  #if DECCHECK
  if (decCheckOperands(res, DECUNUSED, rhs, set)) return res;
  #endif

  /* +Infinity is the special case  */
  Din_Go(591,2048);if ((rhs->bits&(DECINF|DECNEG))==DECINF) {
    Din_Go(589,2048);decSetMaxValue(res, set);                /* is +ve  */
    /* there is no status to set  */
    {decNumber * ReplaceReturn109 = res; Din_Go(590,2048); return ReplaceReturn109;}
    }
  uprv_decNumberZero(&dtiny);                     /* start with 0  */
  Din_Go(592,2048);dtiny.lsu[0]=1;                            /* make number that is ..  */
  dtiny.exponent=DEC_MIN_EMIN-1;             /* .. smaller than tiniest  */
  workset.round=DEC_ROUND_FLOOR;
  decAddOp(res, rhs, &dtiny, &workset, DECNEG, &status);
  status&=DEC_Invalid_operation|DEC_sNaN;    /* only sNaN Invalid please  */
  Din_Go(594,2048);if (status!=0) {/*275*/Din_Go(593,2048);decStatus(res, status, set);/*276*/}
  {decNumber * ReplaceReturn108 = res; Din_Go(595,2048); return ReplaceReturn108;}
  } /* decNumberNextMinus  */

/* ------------------------------------------------------------------ */
/* decNumberNextPlus -- next towards +Infinity                        */
/*                                                                    */
/*   This computes C = A + infinitesimal, rounded towards +Infinity   */
/*                                                                    */
/*   res is C, the result.  C may be A                                */
/*   rhs is A                                                         */
/*   set is the context                                               */
/*                                                                    */
/* This is a generalization of 754 NextUp.                            */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberNextPlus(decNumber *res, const decNumber *rhs,
                              decContext *set) {
  Din_Go(596,2048);decNumber dtiny;                           /* constant  */
  decContext workset=*set;                   /* work  */
  uInt status=0;                             /* accumulator  */
  #if DECCHECK
  if (decCheckOperands(res, DECUNUSED, rhs, set)) return res;
  #endif

  /* -Infinity is the special case  */
  Din_Go(599,2048);if ((rhs->bits&(DECINF|DECNEG))==(DECINF|DECNEG)) {
    Din_Go(597,2048);decSetMaxValue(res, set);
    res->bits=DECNEG;                        /* negative  */
    /* there is no status to set  */
    {decNumber * ReplaceReturn107 = res; Din_Go(598,2048); return ReplaceReturn107;}
    }
  uprv_decNumberZero(&dtiny);                     /* start with 0  */
  Din_Go(600,2048);dtiny.lsu[0]=1;                            /* make number that is ..  */
  dtiny.exponent=DEC_MIN_EMIN-1;             /* .. smaller than tiniest  */
  workset.round=DEC_ROUND_CEILING;
  decAddOp(res, rhs, &dtiny, &workset, 0, &status);
  status&=DEC_Invalid_operation|DEC_sNaN;    /* only sNaN Invalid please  */
  Din_Go(602,2048);if (status!=0) {/*277*/Din_Go(601,2048);decStatus(res, status, set);/*278*/}
  {decNumber * ReplaceReturn106 = res; Din_Go(603,2048); return ReplaceReturn106;}
  } /* decNumberNextPlus  */

/* ------------------------------------------------------------------ */
/* decNumberNextToward -- next towards rhs                            */
/*                                                                    */
/*   This computes C = A +/- infinitesimal, rounded towards           */
/*   +/-Infinity in the direction of B, as per 754-1985 nextafter     */
/*   modified during revision but dropped from 754-2008.              */
/*                                                                    */
/*   res is C, the result.  C may be A or B.                          */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*                                                                    */
/* This is a generalization of 754-1985 NextAfter.                    */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberNextToward(decNumber *res, const decNumber *lhs,
                                const decNumber *rhs, decContext *set) {
  Din_Go(604,2048);decNumber dtiny;                           /* constant  */
  decContext workset=*set;                   /* work  */
  Int result;                                /* ..  */
  uInt status=0;                             /* accumulator  */
  #if DECCHECK
  if (decCheckOperands(res, lhs, rhs, set)) return res;
  #endif

  Din_Go(621,2048);if (decNumberIsNaN(lhs) || decNumberIsNaN(rhs)) {
    Din_Go(605,2048);decNaNs(res, lhs, rhs, set, &status);
    }
   else { /* Is numeric, so no chance of sNaN Invalid, etc.  */
    Din_Go(606,2048);result=decCompare(lhs, rhs, 0);     /* sign matters  */
    Din_Go(620,2048);if (result==BADINT) status|=DEC_Insufficient_storage; /* rare  */
     else { /* valid compare  */
      Din_Go(607,2048);if (result==0) uprv_decNumberCopySign(res, lhs, rhs); /* easy  */
       else { /* differ: need NextPlus or NextMinus  */
        uByte sub;                      /* add or subtract  */
        Din_Go(616,2048);if (result<0) {                 /* lhs<rhs, do nextplus  */
          /* -Infinity is the special case  */
          Din_Go(608,2048);if ((lhs->bits&(DECINF|DECNEG))==(DECINF|DECNEG)) {
            Din_Go(609,2048);decSetMaxValue(res, set);
            res->bits=DECNEG;           /* negative  */
            {decNumber * ReplaceReturn105 = res; Din_Go(610,2048); return ReplaceReturn105;}                 /* there is no status to set  */
            }
          Din_Go(611,2048);workset.round=DEC_ROUND_CEILING;
          sub=0;                        /* add, please  */
          } /* plus  */
         else {                         /* lhs>rhs, do nextminus  */
          /* +Infinity is the special case  */
          Din_Go(612,2048);if ((lhs->bits&(DECINF|DECNEG))==DECINF) {
            Din_Go(613,2048);decSetMaxValue(res, set);
            {decNumber * ReplaceReturn104 = res; Din_Go(614,2048); return ReplaceReturn104;}                 /* there is no status to set  */
            }
          Din_Go(615,2048);workset.round=DEC_ROUND_FLOOR;
          sub=DECNEG;                   /* subtract, please  */
          } /* minus  */
        uprv_decNumberZero(&dtiny);          /* start with 0  */
        Din_Go(617,2048);dtiny.lsu[0]=1;                 /* make number that is ..  */
        dtiny.exponent=DEC_MIN_EMIN-1;  /* .. smaller than tiniest  */
        decAddOp(res, lhs, &dtiny, &workset, sub, &status); /* + or -  */
        /* turn off exceptions if the result is a normal number  */
        /* (including Nmin), otherwise let all status through  */
        Din_Go(619,2048);if (uprv_decNumberIsNormal(res, set)) {/*279*/Din_Go(618,2048);status=0;/*280*/}
        } /* unequal  */
      } /* compare OK  */
    } /* numeric  */
  Din_Go(623,2048);if (status!=0) {/*281*/Din_Go(622,2048);decStatus(res, status, set);/*282*/}
  {decNumber * ReplaceReturn103 = res; Din_Go(624,2048); return ReplaceReturn103;}
  } /* decNumberNextToward  */

/* ------------------------------------------------------------------ */
/* decNumberOr -- OR two Numbers, digitwise                           */
/*                                                                    */
/*   This computes C = A | B                                          */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X|X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context (used for result length and error report)     */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/*                                                                    */
/* Logical function restrictions apply (see above); a NaN is          */
/* returned with Invalid_operation if a restriction is violated.      */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberOr(decNumber *res, const decNumber *lhs,
                        const decNumber *rhs, decContext *set) {
  Din_Go(625,2048);const Unit *ua, *ub;                  /* -> operands  */
  const Unit *msua, *msub;              /* -> operand msus  */
  Unit  *uc, *msuc;                     /* -> result and its msu  */
  Int   msudigs;                        /* digits in res msu  */
  #if DECCHECK
  if (decCheckOperands(res, lhs, rhs, set)) return res;
  #endif

  Din_Go(628,2048);if (lhs->exponent!=0 || decNumberIsSpecial(lhs) || decNumberIsNegative(lhs)
   || rhs->exponent!=0 || decNumberIsSpecial(rhs) || decNumberIsNegative(rhs)) {
    Din_Go(626,2048);decStatus(res, DEC_Invalid_operation, set);
    {decNumber * ReplaceReturn102 = res; Din_Go(627,2048); return ReplaceReturn102;}
    }
  /* operands are valid  */
  Din_Go(629,2048);ua=lhs->lsu;                          /* bottom-up  */
  ub=rhs->lsu;                          /* ..  */
  uc=res->lsu;                          /* ..  */
  msua=ua+D2U(lhs->digits)-1;           /* -> msu of lhs  */
  msub=ub+D2U(rhs->digits)-1;           /* -> msu of rhs  */
  msuc=uc+D2U(set->digits)-1;           /* -> msu of result  */
  msudigs=MSUDIGITS(set->digits);       /* [faster than remainder]  */
  Din_Go(647,2048);for (; uc<=msuc; ua++, ub++, uc++) {  /* Unit loop  */
    Unit a, b;                          /* extract units  */
    Din_Go(632,2048);if (ua>msua) {/*157*/Din_Go(630,2048);a=0;/*158*/}
     else {/*159*/Din_Go(631,2048);a=*ua;/*160*/}
    Din_Go(635,2048);if (ub>msub) {/*161*/Din_Go(633,2048);b=0;/*162*/}
     else {/*163*/Din_Go(634,2048);b=*ub;/*164*/}
    Din_Go(636,2048);*uc=0;                              /* can now write back  */
    Din_Go(646,2048);if (a|b) {                          /* maybe 1 bits to examine  */
      Int i, j;
      /* This loop could be unrolled and/or use BIN2BCD tables  */
      Din_Go(645,2048);for (i=0; i<DECDPUN; i++) {
        Din_Go(637,2048);if ((a|b)&1) {/*165*/Din_Go(638,2048);*uc=*uc+(Unit)powers[i];/*166*/}     /* effect OR  */
        Din_Go(639,2048);j=a%10;
        a=a/10;
        j|=b%10;
        b=b/10;
        Din_Go(642,2048);if (j>1) {
          Din_Go(640,2048);decStatus(res, DEC_Invalid_operation, set);
          {decNumber * ReplaceReturn101 = res; Din_Go(641,2048); return ReplaceReturn101;}
          }
        Din_Go(644,2048);if (uc==msuc && i==msudigs-1) {/*167*/Din_Go(643,2048);break;/*168*/}      /* just did final digit  */
        } /* each digit  */
      } /* non-zero  */
    } /* each unit  */
  /* [here uc-1 is the msu of the result]  */
  Din_Go(648,2048);res->digits=decGetDigits(res->lsu, uc-res->lsu);
  res->exponent=0;                      /* integer  */
  res->bits=0;                          /* sign=0  */
  {decNumber * ReplaceReturn100 = res; Din_Go(649,2048); return ReplaceReturn100;}  /* [no status to set]  */
  } /* decNumberOr  */

/* ------------------------------------------------------------------ */
/* decNumberPlus -- prefix plus operator                              */
/*                                                                    */
/*   This computes C = 0 + A                                          */
/*                                                                    */
/*   res is C, the result.  C may be A                                */
/*   rhs is A                                                         */
/*   set is the context                                               */
/*                                                                    */
/* See also decNumberCopy for a quiet bitwise version of this.        */
/* C must have space for set->digits digits.                          */
/* ------------------------------------------------------------------ */
/* This simply uses AddOp; Add will take fast path after preparing A. */
/* Performance is a concern here, as this routine is often used to    */
/* check operands and apply rounding and overflow/underflow testing.  */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberPlus(decNumber *res, const decNumber *rhs,
                          decContext *set) {
  Din_Go(650,2048);decNumber dzero;
  uInt status=0;                        /* accumulator  */
  #if DECCHECK
  if (decCheckOperands(res, DECUNUSED, rhs, set)) return res;
  #endif

  uprv_decNumberZero(&dzero);                /* make 0  */
  dzero.exponent=rhs->exponent;         /* [no coefficient expansion]  */
  decAddOp(res, &dzero, rhs, set, 0, &status);
  Din_Go(652,2048);if (status!=0) {/*169*/Din_Go(651,2048);decStatus(res, status, set);/*170*/}
  #if DECCHECK
  decCheckInexact(res, set);
  #endif
  {decNumber * ReplaceReturn99 = res; Din_Go(653,2048); return ReplaceReturn99;}
  } /* decNumberPlus  */

/* ------------------------------------------------------------------ */
/* decNumberMultiply -- multiply two Numbers                          */
/*                                                                    */
/*   This computes C = A x B                                          */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X+X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberMultiply(decNumber *res, const decNumber *lhs,
                              const decNumber *rhs, decContext *set) {
Din_Go(654,2048);  uInt status=0;                   /* accumulator  */
  decMultiplyOp(res, lhs, rhs, set, &status);
  Din_Go(656,2048);if (status!=0) {/*155*/Din_Go(655,2048);decStatus(res, status, set);/*156*/}
  #if DECCHECK
  decCheckInexact(res, set);
  #endif
  {decNumber * ReplaceReturn98 = res; Din_Go(657,2048); return ReplaceReturn98;}
  } /* decNumberMultiply  */

/* ------------------------------------------------------------------ */
/* decNumberPower -- raise a number to a power                        */
/*                                                                    */
/*   This computes C = A ** B                                         */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X**X)        */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/*                                                                    */
/* Mathematical function restrictions apply (see above); a NaN is     */
/* returned with Invalid_operation if a restriction is violated.      */
/*                                                                    */
/* However, if 1999999997<=B<=999999999 and B is an integer then the  */
/* restrictions on A and the context are relaxed to the usual bounds, */
/* for compatibility with the earlier (integer power only) version    */
/* of this function.                                                  */
/*                                                                    */
/* When B is an integer, the result may be exact, even if rounded.    */
/*                                                                    */
/* The final result is rounded according to the context; it will      */
/* almost always be correctly rounded, but may be up to 1 ulp in      */
/* error in rare cases.                                               */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberPower(decNumber *res, const decNumber *lhs,
                           const decNumber *rhs, decContext *set) {
  #if DECSUBSET
  decNumber *alloclhs=NULL;        /* non-NULL if rounded lhs allocated  */
  decNumber *allocrhs=NULL;        /* .., rhs  */
  #endif
  Din_Go(658,2048);decNumber *allocdac=NULL;        /* -> allocated acc buffer, iff used  */
  decNumber *allocinv=NULL;        /* -> allocated 1/x buffer, iff used  */
  Int   reqdigits=set->digits;     /* requested DIGITS  */
  Int   n;                         /* rhs in binary  */
  Flag  rhsint=0;                  /* 1 if rhs is an integer  */
  Flag  useint=0;                  /* 1 if can use integer calculation  */
  Flag  isoddint=0;                /* 1 if rhs is an integer and odd  */
  Int   i;                         /* work  */
  #if DECSUBSET
  Int   dropped;                   /* ..  */
  #endif
  uInt  needbytes;                 /* buffer size needed  */
  Flag  seenbit;                   /* seen a bit while powering  */
  Int   residue=0;                 /* rounding residue  */
  uInt  status=0;                  /* accumulators  */
  uByte bits=0;                    /* result sign if errors  */
  decContext aset;                 /* working context  */
  decNumber dnOne;                 /* work value 1...  */
  /* local accumulator buffer [a decNumber, with digits+elength+1 digits]  */
  decNumber dacbuff[D2N(DECBUFFER+9)];
  decNumber *dac=dacbuff;          /* -> result accumulator  */
  /* same again for possible 1/lhs calculation  */
  decNumber invbuff[D2N(DECBUFFER+9)];

  #if DECCHECK
  if (decCheckOperands(res, lhs, rhs, set)) return res;
  #endif

  Din_Go(754,2048);do {                             /* protect allocated storage  */
    #if DECSUBSET
    if (!set->extended) { /* reduce operands and set status, as needed  */
      if (lhs->digits>reqdigits) {
        alloclhs=decRoundOperand(lhs, set, &status);
        if (alloclhs==NULL) break;
        lhs=alloclhs;
        }
      if (rhs->digits>reqdigits) {
        allocrhs=decRoundOperand(rhs, set, &status);
        if (allocrhs==NULL) break;
        rhs=allocrhs;
        }
      }
    #endif
    /* [following code does not require input rounding]  */

    /* handle NaNs and rhs Infinity (lhs infinity is harder)  */
    Din_Go(659,2048);if (SPECIALARGS) {
      Din_Go(660,2048);if (decNumberIsNaN(lhs) || decNumberIsNaN(rhs)) { /* NaNs  */
        Din_Go(661,2048);decNaNs(res, lhs, rhs, set, &status);
        Din_Go(662,2048);break;}
      Din_Go(672,2048);if (decNumberIsInfinite(rhs)) {   /* rhs Infinity  */
Din_Go(663,2048);        Flag rhsneg=rhs->bits&DECNEG;   /* save rhs sign  */
        Din_Go(670,2048);if (decNumberIsNegative(lhs)    /* lhs<0  */
         && !decNumberIsZero(lhs))      /* ..  */
          status|=DEC_Invalid_operation;
         else {                         /* lhs >=0  */
          uprv_decNumberZero(&dnOne);        /* set up 1  */
          Din_Go(664,2048);dnOne.lsu[0]=1;
          uprv_decNumberCompare(dac, lhs, &dnOne, set); /* lhs ? 1  */
          uprv_decNumberZero(res);           /* prepare for 0/1/Infinity  */
          Din_Go(669,2048);if (decNumberIsNegative(dac)) {    /* lhs<1  */
            Din_Go(665,2048);if (rhsneg) res->bits|=DECINF;   /* +Infinity [else is +0]  */
            }
           else {/*171*/Din_Go(666,2048);if (dac->lsu[0]==0) {        /* lhs=1  */
            /* 1**Infinity is inexact, so return fully-padded 1.0000  */
Din_Go(667,2048);            Int shift=set->digits-1;
            *res->lsu=1;                     /* was 0, make int 1  */
            res->digits=decShiftToMost(res->lsu, 1, shift);
            res->exponent=-shift;            /* make 1.0000...  */
            status|=DEC_Inexact|DEC_Rounded; /* deemed inexact  */
            }
           else {                            /* lhs>1  */
            Din_Go(668,2048);if (!rhsneg) res->bits|=DECINF;  /* +Infinity [else is +0]  */
            ;/*172*/}}
          } /* lhs>=0  */
        Din_Go(671,2048);break;}
      /* [lhs infinity drops through]  */
      } /* specials  */

    /* Original rhs may be an integer that fits and is in range  */
    Din_Go(673,2048);n=decGetInt(rhs);
    Din_Go(677,2048);if (n!=BADINT) {                    /* it is an integer  */
      Din_Go(674,2048);rhsint=1;                         /* record the fact for 1**n  */
      isoddint=(Flag)n&1;               /* [works even if big]  */
      Din_Go(676,2048);if (n!=BIGEVEN && n!=BIGODD)      /* can use integer path?  */
        {/*173*/Din_Go(675,2048);useint=1;/*174*/}                       /* looks good  */
      }

    Din_Go(678,2048);if (decNumberIsNegative(lhs)        /* -x ..  */
      && isoddint) bits=DECNEG;         /* .. to an odd power  */

    /* handle LHS infinity  */
    Din_Go(688,2048);if (decNumberIsInfinite(lhs)) {     /* [NaNs already handled]  */
Din_Go(679,2048);      uByte rbits=rhs->bits;            /* save  */
      uprv_decNumberZero(res);               /* prepare  */
      Din_Go(686,2048);if (n==0) {/*175*/Din_Go(680,2048);*res->lsu=1;/*176*/}            /* [-]Inf**0 => 1  */
       else {
        /* -Inf**nonint -> error  */
        Din_Go(681,2048);if (!rhsint && decNumberIsNegative(lhs)) {
          Din_Go(682,2048);status|=DEC_Invalid_operation;     /* -Inf**nonint is error  */
          Din_Go(683,2048);break;}
        Din_Go(684,2048);if (!(rbits & DECNEG)) bits|=DECINF; /* was not a **-n  */
        /* [otherwise will be 0 or -0]  */
        Din_Go(685,2048);res->bits=bits;
        }
      Din_Go(687,2048);break;}

    /* similarly handle LHS zero  */
    Din_Go(696,2048);if (decNumberIsZero(lhs)) {
      Din_Go(689,2048);if (n==0) {                            /* 0**0 => Error  */
        #if DECSUBSET
        if (!set->extended) {                /* [unless subset]  */
          uprv_decNumberZero(res);
          *res->lsu=1;                       /* return 1  */
          break;}
        #endif
        Din_Go(690,2048);status|=DEC_Invalid_operation;
        }
       else {                                /* 0**x  */
Din_Go(691,2048);        uByte rbits=rhs->bits;               /* save  */
        Din_Go(693,2048);if (rbits & DECNEG) {                /* was a 0**(-n)  */
          #if DECSUBSET
          if (!set->extended) {              /* [bad if subset]  */
            status|=DEC_Invalid_operation;
            break;}
          #endif
          Din_Go(692,2048);bits|=DECINF;
          }
        uprv_decNumberZero(res);                  /* prepare  */
        /* [otherwise will be 0 or -0]  */
        Din_Go(694,2048);res->bits=bits;
        }
      Din_Go(695,2048);break;}

    /* here both lhs and rhs are finite; rhs==0 is handled in the  */
    /* integer path.  Next handle the non-integer cases  */
    Din_Go(712,2048);if (!useint) {                      /* non-integral rhs  */
      /* any -ve lhs is bad, as is either operand or context out of  */
      /* bounds  */
      Din_Go(697,2048);if (decNumberIsNegative(lhs)) {
        Din_Go(698,2048);status|=DEC_Invalid_operation;
        Din_Go(699,2048);break;}
      Din_Go(701,2048);if (decCheckMath(lhs, set, &status)
       || decCheckMath(rhs, set, &status)) {/*177*/Din_Go(700,2048);break;/*178*/} /* variable status  */

      uprv_decContextDefault(&aset, DEC_INIT_DECIMAL64); /* clean context  */
      Din_Go(702,2048);aset.emax=DEC_MAX_MATH;           /* usual bounds  */
      aset.emin=-DEC_MAX_MATH;          /* ..  */
      aset.clamp=0;                     /* and no concrete format  */

      /* calculate the result using exp(ln(lhs)*rhs), which can  */
      /* all be done into the accumulator, dac.  The precision needed  */
      /* is enough to contain the full information in the lhs (which  */
      /* is the total digits, including exponent), or the requested  */
      /* precision, if larger, + 4; 6 is used for the exponent  */
      /* maximum length, and this is also used when it is shorter  */
      /* than the requested digits as it greatly reduces the >0.5 ulp  */
      /* cases at little cost (because Ln doubles digits each  */
      /* iteration so a few extra digits rarely causes an extra  */
      /* iteration)  */
      aset.digits=MAXI(lhs->digits, set->digits)+6+4;
      } /* non-integer rhs  */

     else { /* rhs is in-range integer  */
      Din_Go(703,2048);if (n==0) {                       /* x**0 = 1  */
        /* (0**0 was handled above)  */
        uprv_decNumberZero(res);             /* result=1  */
        Din_Go(704,2048);*res->lsu=1;                    /* ..  */
        Din_Go(705,2048);break;}
      /* rhs is a non-zero integer  */
      Din_Go(707,2048);if (n<0) {/*179*/Din_Go(706,2048);n=-n;/*180*/}                    /* use abs(n)  */

      Din_Go(708,2048);aset=*set;                        /* clone the context  */
      aset.round=DEC_ROUND_HALF_EVEN;   /* internally use balanced  */
      /* calculate the working DIGITS  */
      aset.digits=reqdigits+(rhs->digits+rhs->exponent)+2;
      #if DECSUBSET
      if (!set->extended) aset.digits--;     /* use classic precision  */
      #endif
      /* it's an error if this is more than can be handled  */
      Din_Go(711,2048);if (aset.digits>DECNUMMAXP) {Din_Go(709,2048);status|=DEC_Invalid_operation; Din_Go(710,2048);break;}
      } /* integer path  */

    /* aset.digits is the count of digits for the accumulator needed  */
    /* if accumulator is too long for local storage, then allocate  */
    Din_Go(713,2048);needbytes=sizeof(decNumber)+(D2U(aset.digits)-1)*sizeof(Unit);
    /* [needbytes also used below if 1/lhs needed]  */
    Din_Go(719,2048);if (needbytes>sizeof(dacbuff)) {
      Din_Go(714,2048);allocdac=(decNumber *)malloc(needbytes);
      Din_Go(717,2048);if (allocdac==NULL) {   /* hopeless -- abandon  */
        Din_Go(715,2048);status|=DEC_Insufficient_storage;
        Din_Go(716,2048);break;}
      Din_Go(718,2048);dac=allocdac;           /* use the allocated space  */
      }
    /* here, aset is set up and accumulator is ready for use  */

    Din_Go(752,2048);if (!useint) {                           /* non-integral rhs  */
      /* x ** y; special-case x=1 here as it will otherwise always  */
      /* reduce to integer 1; decLnOp has a fastpath which detects  */
      /* the case of x=1  */
      Din_Go(720,2048);decLnOp(dac, lhs, &aset, &status);     /* dac=ln(lhs)  */
      /* [no error possible, as lhs 0 already handled]  */
      Din_Go(725,2048);if (ISZERO(dac)) {                     /* x==1, 1.0, etc.  */
        /* need to return fully-padded 1.0000 etc., but rhsint->1  */
        Din_Go(721,2048);*dac->lsu=1;                         /* was 0, make int 1  */
        Din_Go(723,2048);if (!rhsint) {                       /* add padding  */
Din_Go(722,2048);          Int shift=set->digits-1;
          dac->digits=decShiftToMost(dac->lsu, 1, shift);
          dac->exponent=-shift;              /* make 1.0000...  */
          status|=DEC_Inexact|DEC_Rounded;   /* deemed inexact  */
          }
        }
       else {
        Din_Go(724,2048);decMultiplyOp(dac, dac, rhs, &aset, &status);  /* dac=dac*rhs  */
        decExpOp(dac, dac, &aset, &status);            /* dac=exp(dac)  */
        }
      /* and drop through for final rounding  */
      } /* non-integer rhs  */

     else {                             /* carry on with integer  */
      uprv_decNumberZero(dac);               /* acc=1  */
      Din_Go(726,2048);*dac->lsu=1;                      /* ..  */

      /* if a negative power the constant 1 is needed, and if not subset  */
      /* invert the lhs now rather than inverting the result later  */
      Din_Go(735,2048);if (decNumberIsNegative(rhs)) {   /* was a **-n [hence digits>0]  */
        Din_Go(727,2048);decNumber *inv=invbuff;         /* asssume use fixed buffer  */
        uprv_decNumberCopy(&dnOne, dac);     /* dnOne=1;  [needed now or later]  */
        #if DECSUBSET
        if (set->extended) {            /* need to calculate 1/lhs  */
        #endif
          /* divide lhs into 1, putting result in dac [dac=1/dac]  */
          decDivideOp(dac, &dnOne, lhs, &aset, DIVIDE, &status);
          /* now locate or allocate space for the inverted lhs  */
          Din_Go(733,2048);if (needbytes>sizeof(invbuff)) {
            Din_Go(728,2048);allocinv=(decNumber *)malloc(needbytes);
            Din_Go(731,2048);if (allocinv==NULL) {       /* hopeless -- abandon  */
              Din_Go(729,2048);status|=DEC_Insufficient_storage;
              Din_Go(730,2048);break;}
            Din_Go(732,2048);inv=allocinv;               /* use the allocated space  */
            }
          /* [inv now points to big-enough buffer or allocated storage]  */
          uprv_decNumberCopy(inv, dac);      /* copy the 1/lhs  */
          uprv_decNumberCopy(dac, &dnOne);   /* restore acc=1  */
          Din_Go(734,2048);lhs=inv;                      /* .. and go forward with new lhs  */
        #if DECSUBSET
          }
        #endif
        }

      /* Raise-to-the-power loop...  */
      Din_Go(736,2048);seenbit=0;                   /* set once a 1-bit is encountered  */
      Din_Go(748,2048);for (i=1;;i++){              /* for each bit [top bit ignored]  */
        /* abandon if had overflow or terminal underflow  */
        Din_Go(737,2048);if (status & (DEC_Overflow|DEC_Underflow)) { /* interesting?  */
          Din_Go(738,2048);if (status&DEC_Overflow || ISZERO(dac)) {/*181*/Din_Go(739,2048);break;/*182*/}
          }
        /* [the following two lines revealed an optimizer bug in a C++  */
        /* compiler, with symptom: 5**3 -> 25, when n=n+n was used]  */
        Din_Go(740,2048);n=n<<1;                    /* move next bit to testable position  */
        Din_Go(742,2048);if (n<0) {                 /* top bit is set  */
          Din_Go(741,2048);seenbit=1;               /* OK, significant bit seen  */
          decMultiplyOp(dac, dac, lhs, &aset, &status); /* dac=dac*x  */
          }
        Din_Go(744,2048);if (i==31) {/*183*/Din_Go(743,2048);break;/*184*/}          /* that was the last bit  */
        Din_Go(746,2048);if (!seenbit) {/*185*/Din_Go(745,2048);continue;/*186*/}    /* no need to square 1  */
        Din_Go(747,2048);decMultiplyOp(dac, dac, dac, &aset, &status); /* dac=dac*dac [square]  */
        } /*i*/ /* 32 bits  */

      /* complete internal overflow or underflow processing  */
      Din_Go(751,2048);if (status & (DEC_Overflow|DEC_Underflow)) {
        #if DECSUBSET
        /* If subset, and power was negative, reverse the kind of -erflow  */
        /* [1/x not yet done]  */
        if (!set->extended && decNumberIsNegative(rhs)) {
          if (status & DEC_Overflow)
            status^=DEC_Overflow | DEC_Underflow | DEC_Subnormal;
           else { /* trickier -- Underflow may or may not be set  */
            status&=~(DEC_Underflow | DEC_Subnormal); /* [one or both]  */
            status|=DEC_Overflow;
            }
          }
        #endif
        Din_Go(749,2048);dac->bits=(dac->bits & ~DECNEG) | bits; /* force correct sign  */
        /* round subnormals [to set.digits rather than aset.digits]  */
        /* or set overflow result similarly as required  */
        decFinalize(dac, set, &residue, &status);
        uprv_decNumberCopy(res, dac);   /* copy to result (is now OK length)  */
        Din_Go(750,2048);break;
        }

      #if DECSUBSET
      if (!set->extended &&                  /* subset math  */
          decNumberIsNegative(rhs)) {        /* was a **-n [hence digits>0]  */
        /* so divide result into 1 [dac=1/dac]  */
        decDivideOp(dac, &dnOne, dac, &aset, DIVIDE, &status);
        }
      #endif
      } /* rhs integer path  */

    /* reduce result to the requested length and copy to result  */
    Din_Go(753,2048);decCopyFit(res, dac, set, &residue, &status);
    decFinish(res, set, &residue, &status);  /* final cleanup  */
    #if DECSUBSET
    if (!set->extended) decTrim(res, set, 0, 1, &dropped); /* trailing zeros  */
    #endif
    } while(0);                         /* end protected  */

  if (allocdac!=NULL) free(allocdac);   /* drop any storage used  */
  if (allocinv!=NULL) free(allocinv);   /* ..  */
  #if DECSUBSET
  if (alloclhs!=NULL) free(alloclhs);   /* ..  */
  if (allocrhs!=NULL) free(allocrhs);   /* ..  */
  #endif
  Din_Go(756,2048);if (status!=0) {/*187*/Din_Go(755,2048);decStatus(res, status, set);/*188*/}
  #if DECCHECK
  decCheckInexact(res, set);
  #endif
  {decNumber * ReplaceReturn97 = res; Din_Go(757,2048); return ReplaceReturn97;}
  } /* decNumberPower  */

/* ------------------------------------------------------------------ */
/* decNumberQuantize -- force exponent to requested value             */
/*                                                                    */
/*   This computes C = op(A, B), where op adjusts the coefficient     */
/*   of C (by rounding or shifting) such that the exponent (-scale)   */
/*   of C has exponent of B.  The numerical value of C will equal A,  */
/*   except for the effects of any rounding that occurred.            */
/*                                                                    */
/*   res is C, the result.  C may be A or B                           */
/*   lhs is A, the number to adjust                                   */
/*   rhs is B, the number with exponent to match                      */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/*                                                                    */
/* Unless there is an error or the result is infinite, the exponent   */
/* after the operation is guaranteed to be equal to that of B.        */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberQuantize(decNumber *res, const decNumber *lhs,
                              const decNumber *rhs, decContext *set) {
Din_Go(758,2048);  uInt status=0;                        /* accumulator  */
  decQuantizeOp(res, lhs, rhs, set, 1, &status);
  Din_Go(760,2048);if (status!=0) {/*189*/Din_Go(759,2048);decStatus(res, status, set);/*190*/}
  {decNumber * ReplaceReturn96 = res; Din_Go(761,2048); return ReplaceReturn96;}
  } /* decNumberQuantize  */

/* ------------------------------------------------------------------ */
/* decNumberReduce -- remove trailing zeros                           */
/*                                                                    */
/*   This computes C = 0 + A, and normalizes the result               */
/*                                                                    */
/*   res is C, the result.  C may be A                                */
/*   rhs is A                                                         */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/* ------------------------------------------------------------------ */
/* Previously known as Normalize  */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberNormalize(decNumber *res, const decNumber *rhs,
                               decContext *set) {
  {decNumber * ReplaceReturn95 = uprv_decNumberReduce(res, rhs, set); Din_Go(762,2048); return ReplaceReturn95;}
  } /* decNumberNormalize  */

U_CAPI decNumber * U_EXPORT2 uprv_decNumberReduce(decNumber *res, const decNumber *rhs,
                            decContext *set) {
Din_Go(763,2048);  #if DECSUBSET
  decNumber *allocrhs=NULL;        /* non-NULL if rounded rhs allocated  */
  #endif
  uInt status=0;                   /* as usual  */
  Int  residue=0;                  /* as usual  */
  Int  dropped;                    /* work  */

  #if DECCHECK
  if (decCheckOperands(res, DECUNUSED, rhs, set)) return res;
  #endif

  Din_Go(768,2048);do {                             /* protect allocated storage  */
    #if DECSUBSET
    if (!set->extended) {
      /* reduce operand and set lostDigits status, as needed  */
      if (rhs->digits>set->digits) {
        allocrhs=decRoundOperand(rhs, set, &status);
        if (allocrhs==NULL) break;
        rhs=allocrhs;
        }
      }
    #endif
    /* [following code does not require input rounding]  */

    /* Infinities copy through; NaNs need usual treatment  */
    Din_Go(764,2048);if (decNumberIsNaN(rhs)) {
      Din_Go(765,2048);decNaNs(res, rhs, NULL, set, &status);
      Din_Go(766,2048);break;
      }

    /* reduce result to the requested length and copy to result  */
    Din_Go(767,2048);decCopyFit(res, rhs, set, &residue, &status); /* copy & round  */
    decFinish(res, set, &residue, &status);       /* cleanup/set flags  */
    decTrim(res, set, 1, 0, &dropped);            /* normalize in place  */
                                                  /* [may clamp]  */
    } while(0);                              /* end protected  */

  #if DECSUBSET
  if (allocrhs !=NULL) free(allocrhs);       /* ..  */
  #endif
  Din_Go(770,2048);if (status!=0) {/*191*/Din_Go(769,2048);decStatus(res, status, set);/*192*/}/* then report status  */
  {decNumber * ReplaceReturn94 = res; Din_Go(771,2048); return ReplaceReturn94;}
  } /* decNumberReduce  */

/* ------------------------------------------------------------------ */
/* decNumberRescale -- force exponent to requested value              */
/*                                                                    */
/*   This computes C = op(A, B), where op adjusts the coefficient     */
/*   of C (by rounding or shifting) such that the exponent (-scale)   */
/*   of C has the value B.  The numerical value of C will equal A,    */
/*   except for the effects of any rounding that occurred.            */
/*                                                                    */
/*   res is C, the result.  C may be A or B                           */
/*   lhs is A, the number to adjust                                   */
/*   rhs is B, the requested exponent                                 */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/*                                                                    */
/* Unless there is an error or the result is infinite, the exponent   */
/* after the operation is guaranteed to be equal to B.                */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberRescale(decNumber *res, const decNumber *lhs,
                             const decNumber *rhs, decContext *set) {
Din_Go(772,2048);  uInt status=0;                        /* accumulator  */
  decQuantizeOp(res, lhs, rhs, set, 0, &status);
  Din_Go(774,2048);if (status!=0) {/*197*/Din_Go(773,2048);decStatus(res, status, set);/*198*/}
  {decNumber * ReplaceReturn93 = res; Din_Go(775,2048); return ReplaceReturn93;}
  } /* decNumberRescale  */

/* ------------------------------------------------------------------ */
/* decNumberRemainder -- divide and return remainder                  */
/*                                                                    */
/*   This computes C = A % B                                          */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X%X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberRemainder(decNumber *res, const decNumber *lhs,
                               const decNumber *rhs, decContext *set) {
Din_Go(776,2048);  uInt status=0;                        /* accumulator  */
  decDivideOp(res, lhs, rhs, set, REMAINDER, &status);
  Din_Go(778,2048);if (status!=0) {/*193*/Din_Go(777,2048);decStatus(res, status, set);/*194*/}
  #if DECCHECK
  decCheckInexact(res, set);
  #endif
  {decNumber * ReplaceReturn92 = res; Din_Go(779,2048); return ReplaceReturn92;}
  } /* decNumberRemainder  */

/* ------------------------------------------------------------------ */
/* decNumberRemainderNear -- divide and return remainder from nearest */
/*                                                                    */
/*   This computes C = A % B, where % is the IEEE remainder operator  */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X%X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberRemainderNear(decNumber *res, const decNumber *lhs,
                                   const decNumber *rhs, decContext *set) {
Din_Go(780,2048);  uInt status=0;                        /* accumulator  */
  decDivideOp(res, lhs, rhs, set, REMNEAR, &status);
  Din_Go(782,2048);if (status!=0) {/*195*/Din_Go(781,2048);decStatus(res, status, set);/*196*/}
  #if DECCHECK
  decCheckInexact(res, set);
  #endif
  {decNumber * ReplaceReturn91 = res; Din_Go(783,2048); return ReplaceReturn91;}
  } /* decNumberRemainderNear  */

/* ------------------------------------------------------------------ */
/* decNumberRotate -- rotate the coefficient of a Number left/right   */
/*                                                                    */
/*   This computes C = A rot B  (in base ten and rotating set->digits */
/*   digits).                                                         */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=XrotX)       */
/*   lhs is A                                                         */
/*   rhs is B, the number of digits to rotate (-ve to right)          */
/*   set is the context                                               */
/*                                                                    */
/* The digits of the coefficient of A are rotated to the left (if B   */
/* is positive) or to the right (if B is negative) without adjusting  */
/* the exponent or the sign of A.  If lhs->digits is less than        */
/* set->digits the coefficient is padded with zeros on the left       */
/* before the rotate.  Any leading zeros in the result are removed    */
/* as usual.                                                          */
/*                                                                    */
/* B must be an integer (q=0) and in the range -set->digits through   */
/* +set->digits.                                                      */
/* C must have space for set->digits digits.                          */
/* NaNs are propagated as usual.  Infinities are unaffected (but      */
/* B must be valid).  No status is set unless B is invalid or an      */
/* operand is an sNaN.                                                */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberRotate(decNumber *res, const decNumber *lhs,
                           const decNumber *rhs, decContext *set) {
Din_Go(784,2048);  uInt status=0;              /* accumulator  */
  Int  rotate;                /* rhs as an Int  */

  #if DECCHECK
  if (decCheckOperands(res, lhs, rhs, set)) return res;
  #endif

  /* NaNs propagate as normal  */
  Din_Go(807,2048);if (decNumberIsNaN(lhs) || decNumberIsNaN(rhs))
    {/*199*/Din_Go(785,2048);decNaNs(res, lhs, rhs, set, &status);/*200*/}
   /* rhs must be an integer  */
   else {/*201*/Din_Go(786,2048);if (decNumberIsInfinite(rhs) || rhs->exponent!=0)
    status=DEC_Invalid_operation;
   else { /* both numeric, rhs is an integer  */
    Din_Go(787,2048);rotate=decGetInt(rhs);                   /* [cannot fail]  */
    Din_Go(806,2048);if (rotate==BADINT                       /* something bad ..  */
     || rotate==BIGODD || rotate==BIGEVEN    /* .. very big ..  */
     || abs(rotate)>set->digits)             /* .. or out of range  */
      status=DEC_Invalid_operation;
     else {                                  /* rhs is OK  */
      uprv_decNumberCopy(res, lhs);
      /* convert -ve rotate to equivalent positive rotation  */
      Din_Go(789,2048);if (rotate<0) {/*203*/Din_Go(788,2048);rotate=set->digits+rotate;/*204*/}
      Din_Go(805,2048);if (rotate!=0 && rotate!=set->digits   /* zero or full rotation  */
       && !decNumberIsInfinite(res)) {       /* lhs was infinite  */
        /* left-rotate to do; 0 < rotate < set->digits  */
        uInt units, shift;                   /* work  */
        uInt msudigits;                      /* digits in result msu  */
Din_Go(790,2048);        Unit *msu=res->lsu+D2U(res->digits)-1;    /* current msu  */
        Unit *msumax=res->lsu+D2U(set->digits)-1; /* rotation msu  */
        Din_Go(792,2048);for (msu++; msu<=msumax; msu++) {/*205*/Din_Go(791,2048);*msu=0;/*206*/}   /* ensure high units=0  */
        Din_Go(793,2048);res->digits=set->digits;                  /* now full-length  */
        msudigits=MSUDIGITS(res->digits);         /* actual digits in msu  */

        /* rotation here is done in-place, in three steps  */
        /* 1. shift all to least up to one unit to unit-align final  */
        /*    lsd [any digits shifted out are rotated to the left,  */
        /*    abutted to the original msd (which may require split)]  */
        /*  */
        /*    [if there are no whole units left to rotate, the  */
        /*    rotation is now complete]  */
        /*  */
        /* 2. shift to least, from below the split point only, so that  */
        /*    the final msd is in the right place in its Unit [any  */
        /*    digits shifted out will fit exactly in the current msu,  */
        /*    left aligned, no split required]  */
        /*  */
        /* 3. rotate all the units by reversing left part, right  */
        /*    part, and then whole  */
        /*  */
        /* example: rotate right 8 digits (2 units + 2), DECDPUN=3.  */
        /*  */
        /*   start: 00a bcd efg hij klm npq  */
        /*  */
        /*      1a  000 0ab cde fgh|ijk lmn [pq saved]  */
        /*      1b  00p qab cde fgh|ijk lmn  */
        /*  */
        /*      2a  00p qab cde fgh|00i jkl [mn saved]  */
        /*      2b  mnp qab cde fgh|00i jkl  */
        /*  */
        /*      3a  fgh cde qab mnp|00i jkl  */
        /*      3b  fgh cde qab mnp|jkl 00i  */
        /*      3c  00i jkl mnp qab cde fgh  */

        /* Step 1: amount to shift is the partial right-rotate count  */
        rotate=set->digits-rotate;      /* make it right-rotate  */
        units=rotate/DECDPUN;           /* whole units to rotate  */
        shift=rotate%DECDPUN;           /* left-over digits count  */
        Din_Go(798,2048);if (shift>0) {                  /* not an exact number of units  */
Din_Go(794,2048);          uInt save=res->lsu[0]%powers[shift];    /* save low digit(s)  */
          decShiftToLeast(res->lsu, D2U(res->digits), shift);
          Din_Go(797,2048);if (shift>msudigits) {        /* msumax-1 needs >0 digits  */
Din_Go(795,2048);            uInt rem=save%powers[shift-msudigits];/* split save  */
            *msumax=(Unit)(save/powers[shift-msudigits]); /* and insert  */
            *(msumax-1)=*(msumax-1)
                       +(Unit)(rem*powers[DECDPUN-(shift-msudigits)]); /* ..  */
            }
           else { /* all fits in msumax  */
            Din_Go(796,2048);*msumax=*msumax+(Unit)(save*powers[msudigits-shift]); /* [maybe *1]  */
            }
          } /* digits shift needed  */

        /* If whole units to rotate...  */
        Din_Go(803,2048);if (units>0) {                  /* some to do  */
          /* Step 2: the units to touch are the whole ones in rotate,  */
          /*   if any, and the shift is DECDPUN-msudigits (which may be  */
          /*   0, again)  */
          Din_Go(799,2048);shift=DECDPUN-msudigits;
          Din_Go(801,2048);if (shift>0) {                /* not an exact number of units  */
Din_Go(800,2048);            uInt save=res->lsu[0]%powers[shift];  /* save low digit(s)  */
            decShiftToLeast(res->lsu, units, shift);
            *msumax=*msumax+(Unit)(save*powers[msudigits]);
            } /* partial shift needed  */

          /* Step 3: rotate the units array using triple reverse  */
          /* (reversing is easy and fast)  */
          Din_Go(802,2048);decReverse(res->lsu+units, msumax);     /* left part  */
          decReverse(res->lsu, res->lsu+units-1); /* right part  */
          decReverse(res->lsu, msumax);           /* whole  */
          } /* whole units to rotate  */
        /* the rotation may have left an undetermined number of zeros  */
        /* on the left, so true length needs to be calculated  */
        Din_Go(804,2048);res->digits=decGetDigits(res->lsu, msumax-res->lsu+1);
        } /* rotate needed  */
      } /* rhs OK  */
    ;/*202*/}} /* numerics  */
  Din_Go(809,2048);if (status!=0) {/*207*/Din_Go(808,2048);decStatus(res, status, set);/*208*/}
  {decNumber * ReplaceReturn90 = res; Din_Go(810,2048); return ReplaceReturn90;}
  } /* decNumberRotate  */

/* ------------------------------------------------------------------ */
/* decNumberSameQuantum -- test for equal exponents                   */
/*                                                                    */
/*   res is the result number, which will contain either 0 or 1       */
/*   lhs is a number to test                                          */
/*   rhs is the second (usually a pattern)                            */
/*                                                                    */
/* No errors are possible and no context is needed.                   */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberSameQuantum(decNumber *res, const decNumber *lhs,
                                 const decNumber *rhs) {
Din_Go(811,2048);  Unit ret=0;                      /* return value  */

  #if DECCHECK
  if (decCheckOperands(res, lhs, rhs, DECUNCONT)) return res;
  #endif

  Din_Go(818,2048);if (SPECIALARGS) {
    Din_Go(812,2048);if (decNumberIsNaN(lhs) && decNumberIsNaN(rhs)) {/*209*/Din_Go(813,2048);ret=1;/*210*/}
     else {/*211*/Din_Go(814,2048);if (decNumberIsInfinite(lhs) && decNumberIsInfinite(rhs)) {/*213*/Din_Go(815,2048);ret=1;/*214*/}/*212*/}
     /* [anything else with a special gives 0]  */
    }
   else {/*215*/Din_Go(816,2048);if (lhs->exponent==rhs->exponent) {/*217*/Din_Go(817,2048);ret=1;/*218*/}/*216*/}

  uprv_decNumberZero(res);              /* OK to overwrite an operand now  */
  Din_Go(819,2048);*res->lsu=ret;
  {decNumber * ReplaceReturn89 = res; Din_Go(820,2048); return ReplaceReturn89;}
  } /* decNumberSameQuantum  */

/* ------------------------------------------------------------------ */
/* decNumberScaleB -- multiply by a power of 10                       */
/*                                                                    */
/* This computes C = A x 10**B where B is an integer (q=0) with       */
/* maximum magnitude 2*(emax+digits)                                  */
/*                                                                    */
/*   res is C, the result.  C may be A or B                           */
/*   lhs is A, the number to adjust                                   */
/*   rhs is B, the requested power of ten to use                      */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/*                                                                    */
/* The result may underflow or overflow.                              */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberScaleB(decNumber *res, const decNumber *lhs,
                            const decNumber *rhs, decContext *set) {
  Int  reqexp;                /* requested exponent change [B]  */
  uInt status=0;              /* accumulator  */
  Int  residue;               /* work  */

  #if DECCHECK
  if (decCheckOperands(res, lhs, rhs, set)) return res;
  #endif

  /* Handle special values except lhs infinite  */
  Din_Go(827,2048);if (decNumberIsNaN(lhs) || decNumberIsNaN(rhs))
    {/*219*/Din_Go(821,2048);decNaNs(res, lhs, rhs, set, &status);/*220*/}
    /* rhs must be an integer  */
   else {/*221*/Din_Go(822,2048);if (decNumberIsInfinite(rhs) || rhs->exponent!=0)
    status=DEC_Invalid_operation;
   else {
    /* lhs is a number; rhs is a finite with q==0  */
    Din_Go(823,2048);reqexp=decGetInt(rhs);                   /* [cannot fail]  */
    Din_Go(826,2048);if (reqexp==BADINT                       /* something bad ..  */
     || reqexp==BIGODD || reqexp==BIGEVEN    /* .. very big ..  */
     || abs(reqexp)>(2*(set->digits+set->emax))) /* .. or out of range  */
      status=DEC_Invalid_operation;
     else {                                  /* rhs is OK  */
      uprv_decNumberCopy(res, lhs);               /* all done if infinite lhs  */
      Din_Go(825,2048);if (!decNumberIsInfinite(res)) {       /* prepare to scale  */
        Din_Go(824,2048);res->exponent+=reqexp;               /* adjust the exponent  */
        residue=0;
        decFinalize(res, set, &residue, &status); /* .. and check  */
        } /* finite LHS  */
      } /* rhs OK  */
    ;/*222*/}} /* rhs finite  */
  Din_Go(829,2048);if (status!=0) {/*223*/Din_Go(828,2048);decStatus(res, status, set);/*224*/}
  {decNumber * ReplaceReturn88 = res; Din_Go(830,2048); return ReplaceReturn88;}
  } /* decNumberScaleB  */

/* ------------------------------------------------------------------ */
/* decNumberShift -- shift the coefficient of a Number left or right  */
/*                                                                    */
/*   This computes C = A << B or C = A >> -B  (in base ten).          */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X<<X)        */
/*   lhs is A                                                         */
/*   rhs is B, the number of digits to shift (-ve to right)           */
/*   set is the context                                               */
/*                                                                    */
/* The digits of the coefficient of A are shifted to the left (if B   */
/* is positive) or to the right (if B is negative) without adjusting  */
/* the exponent or the sign of A.                                     */
/*                                                                    */
/* B must be an integer (q=0) and in the range -set->digits through   */
/* +set->digits.                                                      */
/* C must have space for set->digits digits.                          */
/* NaNs are propagated as usual.  Infinities are unaffected (but      */
/* B must be valid).  No status is set unless B is invalid or an      */
/* operand is an sNaN.                                                */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberShift(decNumber *res, const decNumber *lhs,
                           const decNumber *rhs, decContext *set) {
Din_Go(831,2048);  uInt status=0;              /* accumulator  */
  Int  shift;                 /* rhs as an Int  */

  #if DECCHECK
  if (decCheckOperands(res, lhs, rhs, set)) return res;
  #endif

  /* NaNs propagate as normal  */
  Din_Go(847,2048);if (decNumberIsNaN(lhs) || decNumberIsNaN(rhs))
    {/*225*/Din_Go(832,2048);decNaNs(res, lhs, rhs, set, &status);/*226*/}
   /* rhs must be an integer  */
   else {/*227*/Din_Go(833,2048);if (decNumberIsInfinite(rhs) || rhs->exponent!=0)
    status=DEC_Invalid_operation;
   else { /* both numeric, rhs is an integer  */
    Din_Go(834,2048);shift=decGetInt(rhs);                    /* [cannot fail]  */
    Din_Go(846,2048);if (shift==BADINT                        /* something bad ..  */
     || shift==BIGODD || shift==BIGEVEN      /* .. very big ..  */
     || abs(shift)>set->digits)              /* .. or out of range  */
      status=DEC_Invalid_operation;
     else {                                  /* rhs is OK  */
      uprv_decNumberCopy(res, lhs);
      Din_Go(845,2048);if (shift!=0 && !decNumberIsInfinite(res)) { /* something to do  */
        Din_Go(835,2048);if (shift>0) {                       /* to left  */
          Din_Go(836,2048);if (shift==set->digits) {          /* removing all  */
            Din_Go(837,2048);*res->lsu=0;                     /* so place 0  */
            res->digits=1;                   /* ..  */
            }
           else {                            /*  */
            /* first remove leading digits if necessary  */
            Din_Go(838,2048);if (res->digits+shift>set->digits) {
              Din_Go(839,2048);decDecap(res, res->digits+shift-set->digits);
              /* that updated res->digits; may have gone to 1 (for a  */
              /* single digit or for zero  */
              }
            Din_Go(841,2048);if (res->digits>1 || *res->lsu)  /* if non-zero..  */
              {/*229*/Din_Go(840,2048);res->digits=decShiftToMost(res->lsu, res->digits, shift);/*230*/}
            } /* partial left  */
          } /* left  */
         else { /* to right  */
          Din_Go(842,2048);if (-shift>=res->digits) {         /* discarding all  */
            Din_Go(843,2048);*res->lsu=0;                     /* so place 0  */
            res->digits=1;                   /* ..  */
            }
           else {
            Din_Go(844,2048);decShiftToLeast(res->lsu, D2U(res->digits), -shift);
            res->digits-=(-shift);
            }
          } /* to right  */
        } /* non-0 non-Inf shift  */
      } /* rhs OK  */
    ;/*228*/}} /* numerics  */
  Din_Go(849,2048);if (status!=0) {/*231*/Din_Go(848,2048);decStatus(res, status, set);/*232*/}
  {decNumber * ReplaceReturn87 = res; Din_Go(850,2048); return ReplaceReturn87;}
  } /* decNumberShift  */

/* ------------------------------------------------------------------ */
/* decNumberSquareRoot -- square root operator                        */
/*                                                                    */
/*   This computes C = squareroot(A)                                  */
/*                                                                    */
/*   res is C, the result.  C may be A                                */
/*   rhs is A                                                         */
/*   set is the context; note that rounding mode has no effect        */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/* ------------------------------------------------------------------ */
/* This uses the following varying-precision algorithm in:            */
/*                                                                    */
/*   Properly Rounded Variable Precision Square Root, T. E. Hull and  */
/*   A. Abrham, ACM Transactions on Mathematical Software, Vol 11 #3, */
/*   pp229-237, ACM, September 1985.                                  */
/*                                                                    */
/* The square-root is calculated using Newton's method, after which   */
/* a check is made to ensure the result is correctly rounded.         */
/*                                                                    */
/* % [Reformatted original Numerical Turing source code follows.]     */
/* function sqrt(x : real) : real                                     */
/* % sqrt(x) returns the properly rounded approximation to the square */
/* % root of x, in the precision of the calling environment, or it    */
/* % fails if x < 0.                                                  */
/* % t e hull and a abrham, august, 1984                              */
/* if x <= 0 then                                                     */
/*   if x < 0 then                                                    */
/*     assert false                                                   */
/*   else                                                             */
/*     result 0                                                       */
/*   end if                                                           */
/* end if                                                             */
/* var f := setexp(x, 0)  % fraction part of x   [0.1 <= x < 1]       */
/* var e := getexp(x)     % exponent part of x                        */
/* var approx : real                                                  */
/* if e mod 2 = 0  then                                               */
/*   approx := .259 + .819 * f   % approx to root of f                */
/* else                                                               */
/*   f := f/l0                   % adjustments                        */
/*   e := e + 1                  %   for odd                          */
/*   approx := .0819 + 2.59 * f  %   exponent                         */
/* end if                                                             */
/*                                                                    */
/* var p:= 3                                                          */
/* const maxp := currentprecision + 2                                 */
/* loop                                                               */
/*   p := min(2*p - 2, maxp)     % p = 4,6,10, . . . , maxp           */
/*   precision p                                                      */
/*   approx := .5 * (approx + f/approx)                               */
/*   exit when p = maxp                                               */
/* end loop                                                           */
/*                                                                    */
/* % approx is now within 1 ulp of the properly rounded square root   */
/* % of f; to ensure proper rounding, compare squares of (approx -    */
/* % l/2 ulp) and (approx + l/2 ulp) with f.                          */
/* p := currentprecision                                              */
/* begin                                                              */
/*   precision p + 2                                                  */
/*   const approxsubhalf := approx - setexp(.5, -p)                   */
/*   if mulru(approxsubhalf, approxsubhalf) > f then                  */
/*     approx := approx - setexp(.l, -p + 1)                          */
/*   else                                                             */
/*     const approxaddhalf := approx + setexp(.5, -p)                 */
/*     if mulrd(approxaddhalf, approxaddhalf) < f then                */
/*       approx := approx + setexp(.l, -p + 1)                        */
/*     end if                                                         */
/*   end if                                                           */
/* end                                                                */
/* result setexp(approx, e div 2)  % fix exponent                     */
/* end sqrt                                                           */
/* ------------------------------------------------------------------ */
#if defined(__clang__) || U_GCC_MAJOR_MINOR >= 406
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Warray-bounds"
#endif
U_CAPI decNumber * U_EXPORT2 uprv_decNumberSquareRoot(decNumber *res, const decNumber *rhs,
                                decContext *set) {
  Din_Go(851,2048);decContext workset, approxset;   /* work contexts  */
  decNumber dzero;                 /* used for constant zero  */
  Int  maxp;                       /* largest working precision  */
  Int  workp;                      /* working precision  */
  Int  residue=0;                  /* rounding residue  */
  uInt status=0, ignore=0;         /* status accumulators  */
  uInt rstatus;                    /* ..  */
  Int  exp;                        /* working exponent  */
  Int  ideal;                      /* ideal (preferred) exponent  */
  Int  needbytes;                  /* work  */
  Int  dropped;                    /* ..  */

  #if DECSUBSET
  decNumber *allocrhs=NULL;        /* non-NULL if rounded rhs allocated  */
  #endif
  /* buffer for f [needs +1 in case DECBUFFER 0]  */
  decNumber buff[D2N(DECBUFFER+1)];
  /* buffer for a [needs +2 to match likely maxp]  */
  decNumber bufa[D2N(DECBUFFER+2)];
  /* buffer for temporary, b [must be same size as a]  */
  decNumber bufb[D2N(DECBUFFER+2)];
  decNumber *allocbuff=NULL;       /* -> allocated buff, iff allocated  */
  decNumber *allocbufa=NULL;       /* -> allocated bufa, iff allocated  */
  decNumber *allocbufb=NULL;       /* -> allocated bufb, iff allocated  */
  decNumber *f=buff;               /* reduced fraction  */
  decNumber *a=bufa;               /* approximation to result  */
  decNumber *b=bufb;               /* intermediate result  */
  /* buffer for temporary variable, up to 3 digits  */
  decNumber buft[D2N(3)];
  decNumber *t=buft;               /* up-to-3-digit constant or work  */

  #if DECCHECK
  if (decCheckOperands(res, DECUNUSED, rhs, set)) return res;
  #endif

  Din_Go(917,2048);do {                             /* protect allocated storage  */
    #if DECSUBSET
    if (!set->extended) {
      /* reduce operand and set lostDigits status, as needed  */
      if (rhs->digits>set->digits) {
        allocrhs=decRoundOperand(rhs, set, &status);
        if (allocrhs==NULL) break;
        /* [Note: 'f' allocation below could reuse this buffer if  */
        /* used, but as this is rare they are kept separate for clarity.]  */
        rhs=allocrhs;
        }
      }
    #endif
    /* [following code does not require input rounding]  */

    /* handle infinities and NaNs  */
    Din_Go(852,2048);if (SPECIALARG) {
      Din_Go(853,2048);if (decNumberIsInfinite(rhs)) {         /* an infinity  */
        Din_Go(854,2048);if (decNumberIsNegative(rhs)) status|=DEC_Invalid_operation;
         else uprv_decNumberCopy(res, rhs);        /* +Infinity  */
        }
       else {/*233*/Din_Go(855,2048);decNaNs(res, rhs, NULL, set, &status);/*234*/} /* a NaN  */
      Din_Go(856,2048);break;
      }

    /* calculate the ideal (preferred) exponent [floor(exp/2)]  */
    /* [It would be nicer to write: ideal=rhs->exponent>>1, but this  */
    /* generates a compiler warning.  Generated code is the same.]  */
    Din_Go(857,2048);ideal=(rhs->exponent&~1)/2;         /* target  */

    /* handle zeros  */
    Din_Go(860,2048);if (ISZERO(rhs)) {
      uprv_decNumberCopy(res, rhs);          /* could be 0 or -0  */
      Din_Go(858,2048);res->exponent=ideal;              /* use the ideal [safe]  */
      /* use decFinish to clamp any out-of-range exponent, etc.  */
      decFinish(res, set, &residue, &status);
      Din_Go(859,2048);break;
      }

    /* any other -x is an oops  */
    Din_Go(863,2048);if (decNumberIsNegative(rhs)) {
      Din_Go(861,2048);status|=DEC_Invalid_operation;
      Din_Go(862,2048);break;
      }

    /* space is needed for three working variables  */
    /*   f -- the same precision as the RHS, reduced to 0.01->0.99...  */
    /*   a -- Hull's approximation -- precision, when assigned, is  */
    /*        currentprecision+1 or the input argument precision,  */
    /*        whichever is larger (+2 for use as temporary)  */
    /*   b -- intermediate temporary result (same size as a)  */
    /* if any is too long for local storage, then allocate  */
    Din_Go(864,2048);workp=MAXI(set->digits+1, rhs->digits);  /* actual rounding precision  */
    workp=MAXI(workp, 7);                    /* at least 7 for low cases  */
    maxp=workp+2;                            /* largest working precision  */

    needbytes=sizeof(decNumber)+(D2U(rhs->digits)-1)*sizeof(Unit);
    Din_Go(870,2048);if (needbytes>(Int)sizeof(buff)) {
      Din_Go(865,2048);allocbuff=(decNumber *)malloc(needbytes);
      Din_Go(868,2048);if (allocbuff==NULL) {  /* hopeless -- abandon  */
        Din_Go(866,2048);status|=DEC_Insufficient_storage;
        Din_Go(867,2048);break;}
      Din_Go(869,2048);f=allocbuff;            /* use the allocated space  */
      }
    /* a and b both need to be able to hold a maxp-length number  */
    Din_Go(871,2048);needbytes=sizeof(decNumber)+(D2U(maxp)-1)*sizeof(Unit);
    Din_Go(877,2048);if (needbytes>(Int)sizeof(bufa)) {            /* [same applies to b]  */
      Din_Go(872,2048);allocbufa=(decNumber *)malloc(needbytes);
      allocbufb=(decNumber *)malloc(needbytes);
      Din_Go(875,2048);if (allocbufa==NULL || allocbufb==NULL) {   /* hopeless  */
        Din_Go(873,2048);status|=DEC_Insufficient_storage;
        Din_Go(874,2048);break;}
      Din_Go(876,2048);a=allocbufa;            /* use the allocated spaces  */
      b=allocbufb;            /* ..  */
      }

    /* copy rhs -> f, save exponent, and reduce so 0.1 <= f < 1  */
    uprv_decNumberCopy(f, rhs);
    Din_Go(878,2048);exp=f->exponent+f->digits;               /* adjusted to Hull rules  */
    f->exponent=-(f->digits);                /* to range  */

    /* set up working context  */
    uprv_decContextDefault(&workset, DEC_INIT_DECIMAL64);
    workset.emax=DEC_MAX_EMAX;
    workset.emin=DEC_MIN_EMIN;

    /* [Until further notice, no error is possible and status bits  */
    /* (Rounded, etc.) should be ignored, not accumulated.]  */

    /* Calculate initial approximation, and allow for odd exponent  */
    workset.digits=workp;                    /* p for initial calculation  */
    t->bits=0; t->digits=3;
    a->bits=0; a->digits=3;
    Din_Go(881,2048);if ((exp & 1)==0) {                      /* even exponent  */
      /* Set t=0.259, a=0.819  */
      Din_Go(879,2048);t->exponent=-3;
      a->exponent=-3;
      #if DECDPUN>=3
        t->lsu[0]=259;
        a->lsu[0]=819;
      #elif DECDPUN==2
        t->lsu[0]=59; t->lsu[1]=2;
        a->lsu[0]=19; a->lsu[1]=8;
      #else
        t->lsu[0]=9; t->lsu[1]=5; t->lsu[2]=2;
        a->lsu[0]=9; a->lsu[1]=1; a->lsu[2]=8;
      #endif
      }
     else {                                  /* odd exponent  */
      /* Set t=0.0819, a=2.59  */
      Din_Go(880,2048);f->exponent--;                         /* f=f/10  */
      exp++;                                 /* e=e+1  */
      t->exponent=-4;
      a->exponent=-2;
      #if DECDPUN>=3
        t->lsu[0]=819;
        a->lsu[0]=259;
      #elif DECDPUN==2
        t->lsu[0]=19; t->lsu[1]=8;
        a->lsu[0]=59; a->lsu[1]=2;
      #else
        t->lsu[0]=9; t->lsu[1]=1; t->lsu[2]=8;
        a->lsu[0]=9; a->lsu[1]=5; a->lsu[2]=2;
      #endif
      }

    Din_Go(882,2048);decMultiplyOp(a, a, f, &workset, &ignore);    /* a=a*f  */
    decAddOp(a, a, t, &workset, 0, &ignore);      /* ..+t  */
    /* [a is now the initial approximation for sqrt(f), calculated with  */
    /* currentprecision, which is also a's precision.]  */

    /* the main calculation loop  */
    uprv_decNumberZero(&dzero);                   /* make 0  */
    uprv_decNumberZero(t);                        /* set t = 0.5  */
    t->lsu[0]=5;                             /* ..  */
    t->exponent=-1;                          /* ..  */
    workset.digits=3;                        /* initial p  */
    Din_Go(884,2048);for (; workset.digits<maxp;) {
      /* set p to min(2*p - 2, maxp)  [hence 3; or: 4, 6, 10, ... , maxp]  */
      Din_Go(883,2048);workset.digits=MINI(workset.digits*2-2, maxp);
      /* a = 0.5 * (a + f/a)  */
      /* [calculated at p then rounded to currentprecision]  */
      decDivideOp(b, f, a, &workset, DIVIDE, &ignore); /* b=f/a  */
      decAddOp(b, b, a, &workset, 0, &ignore);         /* b=b+a  */
      decMultiplyOp(a, b, t, &workset, &ignore);       /* a=b*0.5  */
      } /* loop  */

    /* Here, 0.1 <= a < 1 [Hull], and a has maxp digits  */
    /* now reduce to length, etc.; this needs to be done with a  */
    /* having the correct exponent so as to handle subnormals  */
    /* correctly  */
    Din_Go(885,2048);approxset=*set;                          /* get emin, emax, etc.  */
    approxset.round=DEC_ROUND_HALF_EVEN;
    a->exponent+=exp/2;                      /* set correct exponent  */
    rstatus=0;                               /* clear status  */
    residue=0;                               /* .. and accumulator  */
    decCopyFit(a, a, &approxset, &residue, &rstatus);  /* reduce (if needed)  */
    decFinish(a, &approxset, &residue, &rstatus);      /* clean and finalize  */

    /* Overflow was possible if the input exponent was out-of-range,  */
    /* in which case quit  */
    Din_Go(888,2048);if (rstatus&DEC_Overflow) {
      Din_Go(886,2048);status=rstatus;                        /* use the status as-is  */
      uprv_decNumberCopy(res, a);                 /* copy to result  */
      Din_Go(887,2048);break;
      }

    /* Preserve status except Inexact/Rounded  */
    Din_Go(889,2048);status|=(rstatus & ~(DEC_Rounded|DEC_Inexact));

    /* Carry out the Hull correction  */
    a->exponent-=exp/2;                      /* back to 0.1->1  */

    /* a is now at final precision and within 1 ulp of the properly  */
    /* rounded square root of f; to ensure proper rounding, compare  */
    /* squares of (a - l/2 ulp) and (a + l/2 ulp) with f.  */
    /* Here workset.digits=maxp and t=0.5, and a->digits determines  */
    /* the ulp  */
    workset.digits--;                             /* maxp-1 is OK now  */
    t->exponent=-a->digits-1;                     /* make 0.5 ulp  */
    decAddOp(b, a, t, &workset, DECNEG, &ignore); /* b = a - 0.5 ulp  */
    workset.round=DEC_ROUND_UP;
    decMultiplyOp(b, b, b, &workset, &ignore);    /* b = mulru(b, b)  */
    decCompareOp(b, f, b, &workset, COMPARE, &ignore); /* b ? f, reversed  */
    Din_Go(894,2048);if (decNumberIsNegative(b)) {                 /* f < b [i.e., b > f]  */
      /* this is the more common adjustment, though both are rare  */
      Din_Go(890,2048);t->exponent++;                              /* make 1.0 ulp  */
      t->lsu[0]=1;                                /* ..  */
      decAddOp(a, a, t, &workset, DECNEG, &ignore); /* a = a - 1 ulp  */
      /* assign to approx [round to length]  */
      approxset.emin-=exp/2;                      /* adjust to match a  */
      approxset.emax-=exp/2;
      decAddOp(a, &dzero, a, &approxset, 0, &ignore);
      }
     else {
      Din_Go(891,2048);decAddOp(b, a, t, &workset, 0, &ignore);    /* b = a + 0.5 ulp  */
      workset.round=DEC_ROUND_DOWN;
      decMultiplyOp(b, b, b, &workset, &ignore);  /* b = mulrd(b, b)  */
      decCompareOp(b, b, f, &workset, COMPARE, &ignore);   /* b ? f  */
      Din_Go(893,2048);if (decNumberIsNegative(b)) {               /* b < f  */
        Din_Go(892,2048);t->exponent++;                            /* make 1.0 ulp  */
        t->lsu[0]=1;                              /* ..  */
        decAddOp(a, a, t, &workset, 0, &ignore);  /* a = a + 1 ulp  */
        /* assign to approx [round to length]  */
        approxset.emin-=exp/2;                    /* adjust to match a  */
        approxset.emax-=exp/2;
        decAddOp(a, &dzero, a, &approxset, 0, &ignore);
        }
      }
    /* [no errors are possible in the above, and rounding/inexact during  */
    /* estimation are irrelevant, so status was not accumulated]  */

    /* Here, 0.1 <= a < 1  (still), so adjust back  */
    Din_Go(895,2048);a->exponent+=exp/2;                      /* set correct exponent  */

    /* count droppable zeros [after any subnormal rounding] by  */
    /* trimming a copy  */
    uprv_decNumberCopy(b, a);
    decTrim(b, set, 1, 1, &dropped);         /* [drops trailing zeros]  */

    /* Set Inexact and Rounded.  The answer can only be exact if  */
    /* it is short enough so that squaring it could fit in workp  */
    /* digits, so this is the only (relatively rare) condition that  */
    /* a careful check is needed  */
    Din_Go(911,2048);if (b->digits*2-1 > workp) {             /* cannot fit  */
      Din_Go(896,2048);status|=DEC_Inexact|DEC_Rounded;
      }
     else {                                  /* could be exact/unrounded  */
      uInt mstatus=0;                        /* local status  */
      Din_Go(897,2048);decMultiplyOp(b, b, b, &workset, &mstatus); /* try the multiply  */
      Din_Go(910,2048);if (mstatus&DEC_Overflow) {            /* result just won't fit  */
        Din_Go(898,2048);status|=DEC_Inexact|DEC_Rounded;
        }
       else {                                /* plausible  */
        Din_Go(899,2048);decCompareOp(t, b, rhs, &workset, COMPARE, &mstatus); /* b ? rhs  */
        Din_Go(909,2048);if (!ISZERO(t)) status|=DEC_Inexact|DEC_Rounded; /* not equal  */
         else {                              /* is Exact  */
          /* here, dropped is the count of trailing zeros in 'a'  */
          /* use closest exponent to ideal...  */
Din_Go(900,2048);          Int todrop=ideal-a->exponent;      /* most that can be dropped  */
          Din_Go(908,2048);if (todrop<0) status|=DEC_Rounded; /* ideally would add 0s  */
           else {                            /* unrounded  */
            /* there are some to drop, but emax may not allow all  */
Din_Go(901,2048);            Int maxexp=set->emax-set->digits+1;
            Int maxdrop=maxexp-a->exponent;
            Din_Go(903,2048);if (todrop>maxdrop && set->clamp) { /* apply clamping  */
              Din_Go(902,2048);todrop=maxdrop;
              status|=DEC_Clamped;
              }
            Din_Go(905,2048);if (dropped<todrop) {            /* clamp to those available  */
              Din_Go(904,2048);todrop=dropped;
              status|=DEC_Clamped;
              }
            Din_Go(907,2048);if (todrop>0) {                  /* have some to drop  */
              Din_Go(906,2048);decShiftToLeast(a->lsu, D2U(a->digits), todrop);
              a->exponent+=todrop;           /* maintain numerical value  */
              a->digits-=todrop;             /* new length  */
              }
            }
          }
        }
      }

    /* double-check Underflow, as perhaps the result could not have  */
    /* been subnormal (initial argument too big), or it is now Exact  */
    Din_Go(916,2048);if (status&DEC_Underflow) {
Din_Go(912,2048);      Int ae=rhs->exponent+rhs->digits-1;    /* adjusted exponent  */
      /* check if truly subnormal  */
      #if DECEXTFLAG                         /* DEC_Subnormal too  */
        Din_Go(914,2048);if (ae>=set->emin*2) {/*235*/Din_Go(913,2048);status&=~(DEC_Subnormal|DEC_Underflow);/*236*/}
      #else
        if (ae>=set->emin*2) status&=~DEC_Underflow;
      #endif
      /* check if truly inexact  */
      Din_Go(915,2048);if (!(status&DEC_Inexact)) status&=~DEC_Underflow;
      }

    uprv_decNumberCopy(res, a);                   /* a is now the result  */
    } while(0);                              /* end protected  */

  if (allocbuff!=NULL) free(allocbuff);      /* drop any storage used  */
  if (allocbufa!=NULL) free(allocbufa);      /* ..  */
  if (allocbufb!=NULL) free(allocbufb);      /* ..  */
  #if DECSUBSET
  if (allocrhs !=NULL) free(allocrhs);       /* ..  */
  #endif
  Din_Go(919,2048);if (status!=0) {/*237*/Din_Go(918,2048);decStatus(res, status, set);/*238*/}/* then report status  */
  #if DECCHECK
  decCheckInexact(res, set);
  #endif
  {decNumber * ReplaceReturn86 = res; Din_Go(920,2048); return ReplaceReturn86;}
  } /* decNumberSquareRoot  */
#if defined(__clang__) || U_GCC_MAJOR_MINOR >= 406
#pragma GCC diagnostic pop
#endif

/* ------------------------------------------------------------------ */
/* decNumberSubtract -- subtract two Numbers                          */
/*                                                                    */
/*   This computes C = A - B                                          */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X-X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberSubtract(decNumber *res, const decNumber *lhs,
                              const decNumber *rhs, decContext *set) {
Din_Go(921,2048);  uInt status=0;                        /* accumulator  */

  decAddOp(res, lhs, rhs, set, DECNEG, &status);
  Din_Go(923,2048);if (status!=0) {/*239*/Din_Go(922,2048);decStatus(res, status, set);/*240*/}
  #if DECCHECK
  decCheckInexact(res, set);
  #endif
  {decNumber * ReplaceReturn85 = res; Din_Go(924,2048); return ReplaceReturn85;}
  } /* decNumberSubtract  */

/* ------------------------------------------------------------------ */
/* decNumberToIntegralExact -- round-to-integral-value with InExact   */
/* decNumberToIntegralValue -- round-to-integral-value                */
/*                                                                    */
/*   res is the result                                                */
/*   rhs is input number                                              */
/*   set is the context                                               */
/*                                                                    */
/* res must have space for any value of rhs.                          */
/*                                                                    */
/* This implements the IEEE special operators and therefore treats    */
/* special values as valid.  For finite numbers it returns            */
/* rescale(rhs, 0) if rhs->exponent is <0.                            */
/* Otherwise the result is rhs (so no error is possible, except for   */
/* sNaN).                                                             */
/*                                                                    */
/* The context is used for rounding mode and status after sNaN, but   */
/* the digits setting is ignored.  The Exact version will signal      */
/* Inexact if the result differs numerically from rhs; the other      */
/* never signals Inexact.                                             */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberToIntegralExact(decNumber *res, const decNumber *rhs,
                                     decContext *set) {
  Din_Go(925,2048);decNumber dn;
  decContext workset;              /* working context  */
  uInt status=0;                   /* accumulator  */

  #if DECCHECK
  if (decCheckOperands(res, DECUNUSED, rhs, set)) return res;
  #endif

  /* handle infinities and NaNs  */
  Din_Go(931,2048);if (SPECIALARG) {
    Din_Go(926,2048);if (decNumberIsInfinite(rhs)) uprv_decNumberCopy(res, rhs); /* an Infinity  */
     else {/*241*/Din_Go(927,2048);decNaNs(res, rhs, NULL, set, &status);/*242*/} /* a NaN  */
    }
   else { /* finite  */
    /* have a finite number; no error possible (res must be big enough)  */
    Din_Go(928,2048);if (rhs->exponent>=0) {/*243*/{decNumber * ReplaceReturn84 = uprv_decNumberCopy(res, rhs); Din_Go(929,2048); return ReplaceReturn84;}/*244*/}
    /* that was easy, but if negative exponent there is work to do...  */
    Din_Go(930,2048);workset=*set;                  /* clone rounding, etc.  */
    workset.digits=rhs->digits;    /* no length rounding  */
    workset.traps=0;               /* no traps  */
    uprv_decNumberZero(&dn);            /* make a number with exponent 0  */
    uprv_decNumberQuantize(res, rhs, &dn, &workset);
    status|=workset.status;
    }
  Din_Go(933,2048);if (status!=0) {/*245*/Din_Go(932,2048);decStatus(res, status, set);/*246*/}
  {decNumber * ReplaceReturn83 = res; Din_Go(934,2048); return ReplaceReturn83;}
  } /* decNumberToIntegralExact  */

U_CAPI decNumber * U_EXPORT2 uprv_decNumberToIntegralValue(decNumber *res, const decNumber *rhs,
                                     decContext *set) {
  Din_Go(935,2048);decContext workset=*set;         /* working context  */
  workset.traps=0;                 /* no traps  */
  uprv_decNumberToIntegralExact(res, rhs, &workset);
  /* this never affects set, except for sNaNs; NaN will have been set  */
  /* or propagated already, so no need to call decStatus  */
  set->status|=workset.status&DEC_Invalid_operation;
  {decNumber * ReplaceReturn82 = res; Din_Go(936,2048); return ReplaceReturn82;}
  } /* decNumberToIntegralValue  */

/* ------------------------------------------------------------------ */
/* decNumberXor -- XOR two Numbers, digitwise                         */
/*                                                                    */
/*   This computes C = A ^ B                                          */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X^X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context (used for result length and error report)     */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/*                                                                    */
/* Logical function restrictions apply (see above); a NaN is          */
/* returned with Invalid_operation if a restriction is violated.      */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberXor(decNumber *res, const decNumber *lhs,
                         const decNumber *rhs, decContext *set) {
  Din_Go(937,2048);const Unit *ua, *ub;                  /* -> operands  */
  const Unit *msua, *msub;              /* -> operand msus  */
  Unit  *uc, *msuc;                     /* -> result and its msu  */
  Int   msudigs;                        /* digits in res msu  */
  #if DECCHECK
  if (decCheckOperands(res, lhs, rhs, set)) return res;
  #endif

  Din_Go(940,2048);if (lhs->exponent!=0 || decNumberIsSpecial(lhs) || decNumberIsNegative(lhs)
   || rhs->exponent!=0 || decNumberIsSpecial(rhs) || decNumberIsNegative(rhs)) {
    Din_Go(938,2048);decStatus(res, DEC_Invalid_operation, set);
    {decNumber * ReplaceReturn81 = res; Din_Go(939,2048); return ReplaceReturn81;}
    }
  /* operands are valid  */
  Din_Go(941,2048);ua=lhs->lsu;                          /* bottom-up  */
  ub=rhs->lsu;                          /* ..  */
  uc=res->lsu;                          /* ..  */
  msua=ua+D2U(lhs->digits)-1;           /* -> msu of lhs  */
  msub=ub+D2U(rhs->digits)-1;           /* -> msu of rhs  */
  msuc=uc+D2U(set->digits)-1;           /* -> msu of result  */
  msudigs=MSUDIGITS(set->digits);       /* [faster than remainder]  */
  Din_Go(959,2048);for (; uc<=msuc; ua++, ub++, uc++) {  /* Unit loop  */
    Unit a, b;                          /* extract units  */
    Din_Go(944,2048);if (ua>msua) {/*247*/Din_Go(942,2048);a=0;/*248*/}
     else {/*249*/Din_Go(943,2048);a=*ua;/*250*/}
    Din_Go(947,2048);if (ub>msub) {/*251*/Din_Go(945,2048);b=0;/*252*/}
     else {/*253*/Din_Go(946,2048);b=*ub;/*254*/}
    Din_Go(948,2048);*uc=0;                              /* can now write back  */
    Din_Go(958,2048);if (a|b) {                          /* maybe 1 bits to examine  */
      Int i, j;
      /* This loop could be unrolled and/or use BIN2BCD tables  */
      Din_Go(957,2048);for (i=0; i<DECDPUN; i++) {
        Din_Go(949,2048);if ((a^b)&1) {/*255*/Din_Go(950,2048);*uc=*uc+(Unit)powers[i];/*256*/}     /* effect XOR  */
        Din_Go(951,2048);j=a%10;
        a=a/10;
        j|=b%10;
        b=b/10;
        Din_Go(954,2048);if (j>1) {
          Din_Go(952,2048);decStatus(res, DEC_Invalid_operation, set);
          {decNumber * ReplaceReturn80 = res; Din_Go(953,2048); return ReplaceReturn80;}
          }
        Din_Go(956,2048);if (uc==msuc && i==msudigs-1) {/*257*/Din_Go(955,2048);break;/*258*/}      /* just did final digit  */
        } /* each digit  */
      } /* non-zero  */
    } /* each unit  */
  /* [here uc-1 is the msu of the result]  */
  Din_Go(960,2048);res->digits=decGetDigits(res->lsu, uc-res->lsu);
  res->exponent=0;                      /* integer  */
  res->bits=0;                          /* sign=0  */
  {decNumber * ReplaceReturn79 = res; Din_Go(961,2048); return ReplaceReturn79;}  /* [no status to set]  */
  } /* decNumberXor  */


/* ================================================================== */
/* Utility routines                                                   */
/* ================================================================== */

/* ------------------------------------------------------------------ */
/* decNumberClass -- return the decClass of a decNumber               */
/*   dn -- the decNumber to test                                      */
/*   set -- the context to use for Emin                               */
/*   returns the decClass enum                                        */
/* ------------------------------------------------------------------ */
enum decClass uprv_decNumberClass(const decNumber *dn, decContext *set) {
  Din_Go(962,2048);if (decNumberIsSpecial(dn)) {
    Din_Go(963,2048);if (decNumberIsQNaN(dn)) {/*259*/{__IAENUM__ decClass  ReplaceReturn78 = DEC_CLASS_QNAN; Din_Go(964,2048); return ReplaceReturn78;}/*260*/}
    Din_Go(966,2048);if (decNumberIsSNaN(dn)) {/*261*/{__IAENUM__ decClass  ReplaceReturn77 = DEC_CLASS_SNAN; Din_Go(965,2048); return ReplaceReturn77;}/*262*/}
    /* must be an infinity  */
    Din_Go(968,2048);if (decNumberIsNegative(dn)) {/*263*/{__IAENUM__ decClass  ReplaceReturn76 = DEC_CLASS_NEG_INF; Din_Go(967,2048); return ReplaceReturn76;}/*264*/}
    {__IAENUM__ decClass  ReplaceReturn75 = DEC_CLASS_POS_INF; Din_Go(969,2048); return ReplaceReturn75;}
    }
  /* is finite  */
  Din_Go(973,2048);if (uprv_decNumberIsNormal(dn, set)) { /* most common  */
    Din_Go(970,2048);if (decNumberIsNegative(dn)) {/*265*/{__IAENUM__ decClass  ReplaceReturn74 = DEC_CLASS_NEG_NORMAL; Din_Go(971,2048); return ReplaceReturn74;}/*266*/}
    {__IAENUM__ decClass  ReplaceReturn73 = DEC_CLASS_POS_NORMAL; Din_Go(972,2048); return ReplaceReturn73;}
    }
  /* is subnormal or zero  */
  Din_Go(977,2048);if (decNumberIsZero(dn)) {    /* most common  */
    Din_Go(974,2048);if (decNumberIsNegative(dn)) {/*267*/{__IAENUM__ decClass  ReplaceReturn72 = DEC_CLASS_NEG_ZERO; Din_Go(975,2048); return ReplaceReturn72;}/*268*/}
    {__IAENUM__ decClass  ReplaceReturn71 = DEC_CLASS_POS_ZERO; Din_Go(976,2048); return ReplaceReturn71;}
    }
  Din_Go(979,2048);if (decNumberIsNegative(dn)) {/*269*/{__IAENUM__ decClass  ReplaceReturn70 = DEC_CLASS_NEG_SUBNORMAL; Din_Go(978,2048); return ReplaceReturn70;}/*270*/}
  {__IAENUM__ decClass  ReplaceReturn69 = DEC_CLASS_POS_SUBNORMAL; Din_Go(980,2048); return ReplaceReturn69;}
  } /* decNumberClass  */

/* ------------------------------------------------------------------ */
/* decNumberClassToString -- convert decClass to a string             */
/*                                                                    */
/*  eclass is a valid decClass                                        */
/*  returns a constant string describing the class (max 13+1 chars)   */
/* ------------------------------------------------------------------ */
const char *uprv_decNumberClassToString(enum decClass eclass) {
  Din_Go(981,2048);if (eclass==DEC_CLASS_POS_NORMAL)    return DEC_ClassString_PN;
  Din_Go(982,2048);if (eclass==DEC_CLASS_NEG_NORMAL)    return DEC_ClassString_NN;
  Din_Go(983,2048);if (eclass==DEC_CLASS_POS_ZERO)      return DEC_ClassString_PZ;
  Din_Go(984,2048);if (eclass==DEC_CLASS_NEG_ZERO)      return DEC_ClassString_NZ;
  Din_Go(985,2048);if (eclass==DEC_CLASS_POS_SUBNORMAL) return DEC_ClassString_PS;
  Din_Go(986,2048);if (eclass==DEC_CLASS_NEG_SUBNORMAL) return DEC_ClassString_NS;
  Din_Go(987,2048);if (eclass==DEC_CLASS_POS_INF)       return DEC_ClassString_PI;
  Din_Go(988,2048);if (eclass==DEC_CLASS_NEG_INF)       return DEC_ClassString_NI;
  Din_Go(989,2048);if (eclass==DEC_CLASS_QNAN)          return DEC_ClassString_QN;
  Din_Go(990,2048);if (eclass==DEC_CLASS_SNAN)          return DEC_ClassString_SN;
  {const char * ReplaceReturn68 = DEC_ClassString_UN; Din_Go(991,2048); return ReplaceReturn68;}           /* Unknown  */
  } /* decNumberClassToString  */

/* ------------------------------------------------------------------ */
/* decNumberCopy -- copy a number                                     */
/*                                                                    */
/*   dest is the target decNumber                                     */
/*   src  is the source decNumber                                     */
/*   returns dest                                                     */
/*                                                                    */
/* (dest==src is allowed and is a no-op)                              */
/* All fields are updated as required.  This is a utility operation,  */
/* so special values are unchanged and no error is possible.          */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberCopy(decNumber *dest, const decNumber *src) {

  #if DECCHECK
  if (src==NULL) return uprv_decNumberZero(dest);
  #endif

  Din_Go(992,2048);if (dest==src) {/*271*/{decNumber * ReplaceReturn67 = dest; Din_Go(993,2048); return ReplaceReturn67;}/*272*/}                /* no copy required  */

  /* Use explicit assignments here as structure assignment could copy  */
  /* more than just the lsu (for small DECDPUN).  This would not affect  */
  /* the value of the results, but could disturb test harness spill  */
  /* checking.  */
  Din_Go(994,2048);dest->bits=src->bits;
  dest->exponent=src->exponent;
  dest->digits=src->digits;
  dest->lsu[0]=src->lsu[0];
  Din_Go(998,2048);if (src->digits>DECDPUN) {                 /* more Units to come  */
    Din_Go(995,2048);const Unit *smsup, *s;                   /* work  */
    Unit  *d;                                /* ..  */
    /* memcpy for the remaining Units would be safe as they cannot  */
    /* overlap.  However, this explicit loop is faster in short cases.  */
    d=dest->lsu+1;                           /* -> first destination  */
    smsup=src->lsu+D2U(src->digits);         /* -> source msu+1  */
    Din_Go(997,2048);for (s=src->lsu+1; s<smsup; s++, d++) {/*273*/Din_Go(996,2048);*d=*s;/*274*/}
    }
  {decNumber * ReplaceReturn66 = dest; Din_Go(999,2048); return ReplaceReturn66;}
  } /* decNumberCopy  */

/* ------------------------------------------------------------------ */
/* decNumberCopyAbs -- quiet absolute value operator                  */
/*                                                                    */
/*   This sets C = abs(A)                                             */
/*                                                                    */
/*   res is C, the result.  C may be A                                */
/*   rhs is A                                                         */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/* No exception or error can occur; this is a quiet bitwise operation.*/
/* See also decNumberAbs for a checking version of this.              */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberCopyAbs(decNumber *res, const decNumber *rhs) {
Din_Go(1000,2048);  #if DECCHECK
  if (decCheckOperands(res, DECUNUSED, rhs, DECUNCONT)) return res;
  #endif
  uprv_decNumberCopy(res, rhs);
  res->bits&=~DECNEG;                   /* turn off sign  */
  {decNumber * ReplaceReturn65 = res; Din_Go(1001,2048); return ReplaceReturn65;}
  } /* decNumberCopyAbs  */

/* ------------------------------------------------------------------ */
/* decNumberCopyNegate -- quiet negate value operator                 */
/*                                                                    */
/*   This sets C = negate(A)                                          */
/*                                                                    */
/*   res is C, the result.  C may be A                                */
/*   rhs is A                                                         */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/* No exception or error can occur; this is a quiet bitwise operation.*/
/* See also decNumberMinus for a checking version of this.            */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberCopyNegate(decNumber *res, const decNumber *rhs) {
Din_Go(1002,2048);  #if DECCHECK
  if (decCheckOperands(res, DECUNUSED, rhs, DECUNCONT)) return res;
  #endif
  uprv_decNumberCopy(res, rhs);
  res->bits^=DECNEG;                    /* invert the sign  */
  {decNumber * ReplaceReturn64 = res; Din_Go(1003,2048); return ReplaceReturn64;}
  } /* decNumberCopyNegate  */

/* ------------------------------------------------------------------ */
/* decNumberCopySign -- quiet copy and set sign operator              */
/*                                                                    */
/*   This sets C = A with the sign of B                               */
/*                                                                    */
/*   res is C, the result.  C may be A                                */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/* No exception or error can occur; this is a quiet bitwise operation.*/
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberCopySign(decNumber *res, const decNumber *lhs,
                              const decNumber *rhs) {
Din_Go(1004,2048);  uByte sign;                           /* rhs sign  */
  #if DECCHECK
  if (decCheckOperands(res, DECUNUSED, rhs, DECUNCONT)) return res;
  #endif
  sign=rhs->bits & DECNEG;              /* save sign bit  */
  uprv_decNumberCopy(res, lhs);
  res->bits&=~DECNEG;                   /* clear the sign  */
  res->bits|=sign;                      /* set from rhs  */
  {decNumber * ReplaceReturn63 = res; Din_Go(1005,2048); return ReplaceReturn63;}
  } /* decNumberCopySign  */

/* ------------------------------------------------------------------ */
/* decNumberGetBCD -- get the coefficient in BCD8                     */
/*   dn is the source decNumber                                       */
/*   bcd is the uInt array that will receive dn->digits BCD bytes,    */
/*     most-significant at offset 0                                   */
/*   returns bcd                                                      */
/*                                                                    */
/* bcd must have at least dn->digits bytes.  No error is possible; if */
/* dn is a NaN or Infinite, digits must be 1 and the coefficient 0.   */
/* ------------------------------------------------------------------ */
U_CAPI uByte * U_EXPORT2 uprv_decNumberGetBCD(const decNumber *dn, uByte *bcd) {
Din_Go(1006,2048);  uByte *ub=bcd+dn->digits-1;      /* -> lsd  */
  const Unit *up=dn->lsu;          /* Unit pointer, -> lsu  */

  #if DECDPUN==1                   /* trivial simple copy  */
    Din_Go(1008,2048);for (; ub>=bcd; ub--, up++) {/*79*/Din_Go(1007,2048);*ub=*up;/*80*/}
  #else                            /* chopping needed  */
    uInt u=*up;                    /* work  */
    uInt cut=DECDPUN;              /* downcounter through unit  */
    for (; ub>=bcd; ub--) {
      *ub=(uByte)(u%10);           /* [*6554 trick inhibits, here]  */
      u=u/10;
      cut--;
      if (cut>0) continue;         /* more in this unit  */
      up++;
      u=*up;
      cut=DECDPUN;
      }
  #endif
  {uint8_t * ReplaceReturn62 = bcd; Din_Go(1009,2048); return ReplaceReturn62;}
  } /* decNumberGetBCD  */

/* ------------------------------------------------------------------ */
/* decNumberSetBCD -- set (replace) the coefficient from BCD8         */
/*   dn is the target decNumber                                       */
/*   bcd is the uInt array that will source n BCD bytes, most-        */
/*     significant at offset 0                                        */
/*   n is the number of digits in the source BCD array (bcd)          */
/*   returns dn                                                       */
/*                                                                    */
/* dn must have space for at least n digits.  No error is possible;   */
/* if dn is a NaN, or Infinite, or is to become a zero, n must be 1   */
/* and bcd[0] zero.                                                   */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberSetBCD(decNumber *dn, const uByte *bcd, uInt n) {
Din_Go(1010,2048);  Unit *up=dn->lsu+D2U(dn->digits)-1;   /* -> msu [target pointer]  */
  const uByte *ub=bcd;                  /* -> source msd  */

  #if DECDPUN==1                        /* trivial simple copy  */
    Din_Go(1012,2048);for (; ub<bcd+n; ub++, up--) {/*81*/Din_Go(1011,2048);*up=*ub;/*82*/}
  #else                                 /* some assembly needed  */
    /* calculate how many digits in msu, and hence first cut  */
    Int cut=MSUDIGITS(n);               /* [faster than remainder]  */
    for (;up>=dn->lsu; up--) {          /* each Unit from msu  */
      *up=0;                            /* will take <=DECDPUN digits  */
      for (; cut>0; ub++, cut--) *up=X10(*up)+*ub;
      cut=DECDPUN;                      /* next Unit has all digits  */
      }
  #endif
  Din_Go(1013,2048);dn->digits=n;                         /* set digit count  */
  {decNumber * ReplaceReturn61 = dn; Din_Go(1014,2048); return ReplaceReturn61;}
  } /* decNumberSetBCD  */

/* ------------------------------------------------------------------ */
/* decNumberIsNormal -- test normality of a decNumber                 */
/*   dn is the decNumber to test                                      */
/*   set is the context to use for Emin                               */
/*   returns 1 if |dn| is finite and >=Nmin, 0 otherwise              */
/* ------------------------------------------------------------------ */
Int uprv_decNumberIsNormal(const decNumber *dn, decContext *set) {
Din_Go(1015,2048);  Int ae;                               /* adjusted exponent  */
  #if DECCHECK
  if (decCheckOperands(DECUNRESU, DECUNUSED, dn, set)) return 0;
  #endif

  Din_Go(1017,2048);if (decNumberIsSpecial(dn)) {/*283*/{int32_t  ReplaceReturn60 = 0; Din_Go(1016,2048); return ReplaceReturn60;}/*284*/} /* not finite  */
  Din_Go(1019,2048);if (decNumberIsZero(dn)) {/*285*/{int32_t  ReplaceReturn59 = 0; Din_Go(1018,2048); return ReplaceReturn59;}/*286*/}    /* not non-zero  */

  Din_Go(1020,2048);ae=dn->exponent+dn->digits-1;         /* adjusted exponent  */
  Din_Go(1022,2048);if (ae<set->emin) {/*287*/{int32_t  ReplaceReturn58 = 0; Din_Go(1021,2048); return ReplaceReturn58;}/*288*/}           /* is subnormal  */
  {int32_t  ReplaceReturn57 = 1; Din_Go(1023,2048); return ReplaceReturn57;}
  } /* decNumberIsNormal  */

/* ------------------------------------------------------------------ */
/* decNumberIsSubnormal -- test subnormality of a decNumber           */
/*   dn is the decNumber to test                                      */
/*   set is the context to use for Emin                               */
/*   returns 1 if |dn| is finite, non-zero, and <Nmin, 0 otherwise    */
/* ------------------------------------------------------------------ */
Int uprv_decNumberIsSubnormal(const decNumber *dn, decContext *set) {
Din_Go(1024,2048);  Int ae;                               /* adjusted exponent  */
  #if DECCHECK
  if (decCheckOperands(DECUNRESU, DECUNUSED, dn, set)) return 0;
  #endif

  Din_Go(1026,2048);if (decNumberIsSpecial(dn)) {/*289*/{int32_t  ReplaceReturn56 = 0; Din_Go(1025,2048); return ReplaceReturn56;}/*290*/} /* not finite  */
  Din_Go(1028,2048);if (decNumberIsZero(dn)) {/*291*/{int32_t  ReplaceReturn55 = 0; Din_Go(1027,2048); return ReplaceReturn55;}/*292*/}    /* not non-zero  */

  Din_Go(1029,2048);ae=dn->exponent+dn->digits-1;         /* adjusted exponent  */
  Din_Go(1031,2048);if (ae<set->emin) {/*293*/{int32_t  ReplaceReturn54 = 1; Din_Go(1030,2048); return ReplaceReturn54;}/*294*/}           /* is subnormal  */
  {int32_t  ReplaceReturn53 = 0; Din_Go(1032,2048); return ReplaceReturn53;}
  } /* decNumberIsSubnormal  */

/* ------------------------------------------------------------------ */
/* decNumberTrim -- remove insignificant zeros                        */
/*                                                                    */
/*   dn is the number to trim                                         */
/*   returns dn                                                       */
/*                                                                    */
/* All fields are updated as required.  This is a utility operation,  */
/* so special values are unchanged and no error is possible.  The     */
/* zeros are removed unconditionally.                                 */
/* ------------------------------------------------------------------ */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberTrim(decNumber *dn) {
Din_Go(1033,2048);  Int  dropped;                    /* work  */
  decContext set;                  /* ..  */
  #if DECCHECK
  if (decCheckOperands(DECUNRESU, DECUNUSED, dn, DECUNCONT)) return dn;
  #endif
  uprv_decContextDefault(&set, DEC_INIT_BASE);    /* clamp=0  */
  {decNumber * ReplaceReturn52 = decTrim(dn, &set, 0, 1, &dropped); Din_Go(1034,2048); return ReplaceReturn52;}
  } /* decNumberTrim  */

/* ------------------------------------------------------------------ */
/* decNumberVersion -- return the name and version of this module     */
/*                                                                    */
/* No error is possible.                                              */
/* ------------------------------------------------------------------ */
const char * uprv_decNumberVersion(void) {
  {const char * ReplaceReturn51 = DECVERSION; Din_Go(1035,2048); return ReplaceReturn51;}
  } /* decNumberVersion  */

/* ------------------------------------------------------------------ */
/* decNumberZero -- set a number to 0                                 */
/*                                                                    */
/*   dn is the number to set, with space for one digit                */
/*   returns dn                                                       */
/*                                                                    */
/* No error is possible.                                              */
/* ------------------------------------------------------------------ */
/* Memset is not used as it is much slower in some environments.  */
U_CAPI decNumber * U_EXPORT2 uprv_decNumberZero(decNumber *dn) {

  #if DECCHECK
  if (decCheckOperands(dn, DECUNUSED, DECUNUSED, DECUNCONT)) return dn;
  #endif

  Din_Go(1036,2048);dn->bits=0;
  dn->exponent=0;
  dn->digits=1;
  dn->lsu[0]=0;
  {decNumber * ReplaceReturn50 = dn; Din_Go(1037,2048); return ReplaceReturn50;}
  } /* decNumberZero  */

/* ================================================================== */
/* Local routines                                                     */
/* ================================================================== */

/* ------------------------------------------------------------------ */
/* decToString -- lay out a number into a string                      */
/*                                                                    */
/*   dn     is the number to lay out                                  */
/*   string is where to lay out the number                            */
/*   eng    is 1 if Engineering, 0 if Scientific                      */
/*                                                                    */
/* string must be at least dn->digits+14 characters long              */
/* No error is possible.                                              */
/*                                                                    */
/* Note that this routine can generate a -0 or 0.000.  These are      */
/* never generated in subset to-number or arithmetic, but can occur   */
/* in non-subset arithmetic (e.g., -1*0 or 1.234-1.234).              */
/* ------------------------------------------------------------------ */
/* If DECCHECK is enabled the string "?" is returned if a number is  */
/* invalid.  */
static void decToString(const decNumber *dn, char *string, Flag eng) {
Din_Go(1038,2048);  Int exp=dn->exponent;       /* local copy  */
  Int e;                      /* E-part value  */
  Int pre;                    /* digits before the '.'  */
  Int cut;                    /* for counting digits in a Unit  */
  char *c=string;             /* work [output pointer]  */
  const Unit *up=dn->lsu+D2U(dn->digits)-1; /* -> msu [input pointer]  */
  uInt u, pow;                /* work  */

  #if DECCHECK
  if (decCheckOperands(DECUNRESU, dn, DECUNUSED, DECUNCONT)) {
    strcpy(string, "?");
    return;}
  #endif

  Din_Go(1040,2048);if (decNumberIsNegative(dn)) {   /* Negatives get a minus  */
    Din_Go(1039,2048);*c='-';
    c++;
    }
  Din_Go(1049,2048);if (dn->bits&DECSPECIAL) {       /* Is a special value  */
    Din_Go(1041,2048);if (decNumberIsInfinite(dn)) {
      Din_Go(1042,2048);strcpy(c,   "Inf");
      strcpy(c+3, "inity");
      Din_Go(1043,2048);return;}
    /* a NaN  */
    Din_Go(1045,2048);if (dn->bits&DECSNAN) {        /* signalling NaN  */
      Din_Go(1044,2048);*c='s';
      c++;
      }
    Din_Go(1046,2048);strcpy(c, "NaN");
    c+=3;                          /* step past  */
    /* if not a clean non-zero coefficient, that's all there is in a  */
    /* NaN string  */
    Din_Go(1048,2048);if (exp!=0 || (*dn->lsu==0 && dn->digits==1)) {/*673*/Din_Go(1047,2048);return;/*674*/}
    /* [drop through to add integer]  */
    }

  /* calculate how many digits in msu, and hence first cut  */
  Din_Go(1050,2048);cut=MSUDIGITS(dn->digits);       /* [faster than remainder]  */
  cut--;                           /* power of ten for digit  */

  Din_Go(1058,2048);if (exp==0) {                    /* simple integer [common fastpath]  */
    Din_Go(1051,2048);for (;up>=dn->lsu; up--) {     /* each Unit from msu  */
      Din_Go(1052,2048);u=*up;                       /* contains DECDPUN digits to lay out  */
      Din_Go(1054,2048);for (; cut>=0; c++, cut--)Din_Go(1053,2048); TODIGIT(u, cut, c, pow);
      Din_Go(1055,2048);cut=DECDPUN-1;               /* next Unit has all digits  */
      }
    Din_Go(1056,2048);*c='\0';                       /* terminate the string  */
    Din_Go(1057,2048);return;}

  /* non-0 exponent -- assume plain form */
  Din_Go(1059,2048);pre=dn->digits+exp;              /* digits before '.'  */
  e=0;                             /* no E  */
  Din_Go(1072,2048);if ((exp>0) || (pre<-5)) {       /* need exponential form  */
    Din_Go(1060,2048);e=exp+dn->digits-1;            /* calculate E value  */
    pre=1;                         /* assume one digit before '.'  */
    Din_Go(1071,2048);if (eng && (e!=0)) {           /* engineering: may need to adjust  */
      Int adj;                     /* adjustment  */
      /* The C remainder operator is undefined for negative numbers, so  */
      /* a positive remainder calculation must be used here  */
      Din_Go(1065,2048);if (e<0) {
        Din_Go(1061,2048);adj=(-e)%3;
        Din_Go(1063,2048);if (adj!=0) {/*675*/Din_Go(1062,2048);adj=3-adj;/*676*/}
        }
       else { /* e>0  */
        Din_Go(1064,2048);adj=e%3;
        }
      Din_Go(1066,2048);e=e-adj;
      /* if dealing with zero still produce an exponent which is a  */
      /* multiple of three, as expected, but there will only be the  */
      /* one zero before the E, still.  Otherwise note the padding.  */
      Din_Go(1070,2048);if (!ISZERO(dn)) {/*677*/Din_Go(1067,2048);pre+=adj;/*678*/}
       else {  /* is zero  */
        Din_Go(1068,2048);if (adj!=0) {              /* 0.00Esnn needed  */
          Din_Go(1069,2048);e=e+3;
          pre=-(2-adj);
          }
        } /* zero  */
      } /* eng  */
    } /* need exponent  */

  /* lay out the digits of the coefficient, adding 0s and . as needed */
  Din_Go(1073,2048);u=*up;
  Din_Go(1100,2048);if (pre>0) {                     /* xxx.xxx or xx00 (engineering) form  */
Din_Go(1074,2048);    Int n=pre;
    Din_Go(1080,2048);for (; pre>0; pre--, c++, cut--) {
      Din_Go(1075,2048);if (cut<0) {                 /* need new Unit  */
        Din_Go(1076,2048);if (up==dn->lsu) {/*679*/Din_Go(1077,2048);break;/*680*/}    /* out of input digits (pre>digits)  */
        Din_Go(1078,2048);up--;
        cut=DECDPUN-1;
        u=*up;
        }
Din_Go(1079,2048);      TODIGIT(u, cut, c, pow);
      }
    Din_Go(1090,2048);if (n<dn->digits) {            /* more to come, after '.'  */
      Din_Go(1081,2048);*c='.'; c++;
      Din_Go(1087,2048);for (;; c++, cut--) {
        Din_Go(1082,2048);if (cut<0) {               /* need new Unit  */
          Din_Go(1083,2048);if (up==dn->lsu) {/*681*/Din_Go(1084,2048);break;/*682*/}  /* out of input digits  */
          Din_Go(1085,2048);up--;
          cut=DECDPUN-1;
          u=*up;
          }
Din_Go(1086,2048);        TODIGIT(u, cut, c, pow);
        }
      }
     else {/*683*/Din_Go(1088,2048);for (; pre>0; pre--, c++) {/*685*/Din_Go(1089,2048);*c='0';/*686*/}/*684*/} /* 0 padding (for engineering) needed  */
    }
   else {                          /* 0.xxx or 0.000xxx form  */
    Din_Go(1091,2048);*c='0'; c++;
    *c='.'; c++;
    Din_Go(1093,2048);for (; pre<0; pre++, c++) {/*687*/Din_Go(1092,2048);*c='0';/*688*/}   /* add any 0's after '.'  */
    Din_Go(1099,2048);for (; ; c++, cut--) {
      Din_Go(1094,2048);if (cut<0) {                 /* need new Unit  */
        Din_Go(1095,2048);if (up==dn->lsu) {/*689*/Din_Go(1096,2048);break;/*690*/}    /* out of input digits  */
        Din_Go(1097,2048);up--;
        cut=DECDPUN-1;
        u=*up;
        }
Din_Go(1098,2048);      TODIGIT(u, cut, c, pow);
      }
    }

  /* Finally add the E-part, if needed.  It will never be 0, has a
     base maximum and minimum of +999999999 through -999999999, but
     could range down to -1999999998 for anormal numbers */
  Din_Go(1109,2048);if (e!=0) {
    Flag had=0;               /* 1=had non-zero  */
    Din_Go(1101,2048);*c='E'; c++;
    *c='+'; c++;              /* assume positive  */
    u=e;                      /* ..  */
    Din_Go(1103,2048);if (e<0) {
      Din_Go(1102,2048);*(c-1)='-';             /* oops, need -  */
      u=-e;                   /* uInt, please  */
      }
    /* lay out the exponent [_itoa or equivalent is not ANSI C]  */
    Din_Go(1108,2048);for (cut=9; cut>=0; cut--) {
Din_Go(1104,2048);      TODIGIT(u, cut, c, pow);
      Din_Go(1106,2048);if (*c=='0' && !had) {/*691*/Din_Go(1105,2048);continue;/*692*/}    /* skip leading zeros  */
      Din_Go(1107,2048);had=1;                            /* had non-0  */
      c++;                              /* step for next  */
      } /* cut  */
    }
  Din_Go(1110,2048);*c='\0';          /* terminate the string (all paths)  */
  Din_Go(1111,2048);return;
  } /* decToString  */

/* ------------------------------------------------------------------ */
/* decAddOp -- add/subtract operation                                 */
/*                                                                    */
/*   This computes C = A + B                                          */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X+X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*   negate is DECNEG if rhs should be negated, or 0 otherwise        */
/*   status accumulates status for the caller                         */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/* Inexact in status must be 0 for correct Exact zero sign in result  */
/* ------------------------------------------------------------------ */
/* If possible, the coefficient is calculated directly into C.        */
/* However, if:                                                       */
/*   -- a digits+1 calculation is needed because the numbers are      */
/*      unaligned and span more than set->digits digits               */
/*   -- a carry to digits+1 digits looks possible                     */
/*   -- C is the same as A or B, and the result would destructively   */
/*      overlap the A or B coefficient                                */
/* then the result must be calculated into a temporary buffer.  In    */
/* this case a local (stack) buffer is used if possible, and only if  */
/* too long for that does malloc become the final resort.             */
/*                                                                    */
/* Misalignment is handled as follows:                                */
/*   Apad: (AExp>BExp) Swap operands and proceed as for BExp>AExp.    */
/*   BPad: Apply the padding by a combination of shifting (whole      */
/*         units) and multiplication (part units).                    */
/*                                                                    */
/* Addition, especially x=x+1, is speed-critical.                     */
/* The static buffer is larger than might be expected to allow for    */
/* calls from higher-level funtions (notable exp).                    */
/* ------------------------------------------------------------------ */
static decNumber * decAddOp(decNumber *res, const decNumber *lhs,
                            const decNumber *rhs, decContext *set,
                            uByte negate, uInt *status) {
  #if DECSUBSET
  decNumber *alloclhs=NULL;        /* non-NULL if rounded lhs allocated  */
  decNumber *allocrhs=NULL;        /* .., rhs  */
  #endif
  Int   rhsshift;                  /* working shift (in Units)  */
  Int   maxdigits;                 /* longest logical length  */
  Int   mult;                      /* multiplier  */
  Int   residue;                   /* rounding accumulator  */
  uByte bits;                      /* result bits  */
  Flag  diffsign;                  /* non-0 if arguments have different sign  */
  Unit  *acc;                      /* accumulator for result  */
  Unit  accbuff[SD2U(DECBUFFER*2+20)]; /* local buffer [*2+20 reduces many  */
                                   /* allocations when called from  */
                                   /* other operations, notable exp]  */
  Unit  *allocacc=NULL;            /* -> allocated acc buffer, iff allocated  */
  Int   reqdigits=set->digits;     /* local copy; requested DIGITS  */
  Int   padding;                   /* work  */

  #if DECCHECK
  if (decCheckOperands(res, lhs, rhs, set)) return res;
  #endif

  Din_Go(1197,2048);do {                             /* protect allocated storage  */
    #if DECSUBSET
    if (!set->extended) {
      /* reduce operands and set lostDigits status, as needed  */
      if (lhs->digits>reqdigits) {
        alloclhs=decRoundOperand(lhs, set, status);
        if (alloclhs==NULL) break;
        lhs=alloclhs;
        }
      if (rhs->digits>reqdigits) {
        allocrhs=decRoundOperand(rhs, set, status);
        if (allocrhs==NULL) break;
        rhs=allocrhs;
        }
      }
    #endif
    /* [following code does not require input rounding]  */

    /* note whether signs differ [used all paths]  */
    Din_Go(1112,2048);diffsign=(Flag)((lhs->bits^rhs->bits^negate)&DECNEG);

    /* handle infinities and NaNs  */
    Din_Go(1122,2048);if (SPECIALARGS) {                  /* a special bit set  */
      Din_Go(1113,2048);if (SPECIALARGS & (DECSNAN | DECNAN))  /* a NaN  */
        {/*295*/Din_Go(1114,2048);decNaNs(res, lhs, rhs, set, status);/*296*/}
       else { /* one or two infinities  */
        Din_Go(1115,2048);if (decNumberIsInfinite(lhs)) { /* LHS is infinity  */
          /* two infinities with different signs is invalid  */
          Din_Go(1116,2048);if (decNumberIsInfinite(rhs) && diffsign) {
            Din_Go(1117,2048);*status|=DEC_Invalid_operation;
            Din_Go(1118,2048);break;
            }
          Din_Go(1119,2048);bits=lhs->bits & DECNEG;      /* get sign from LHS  */
          }
         else bits=(rhs->bits^negate) & DECNEG;/* RHS must be Infinity  */
        Din_Go(1120,2048);bits|=DECINF;
        uprv_decNumberZero(res);
        res->bits=bits;                 /* set +/- infinity  */
        } /* an infinity  */
      Din_Go(1121,2048);break;
      }

    /* Quick exit for add 0s; return the non-0, modified as need be  */
    Din_Go(1135,2048);if (ISZERO(lhs)) {
      Int adjust;                       /* work  */
Din_Go(1123,2048);      Int lexp=lhs->exponent;           /* save in case LHS==RES  */
      bits=lhs->bits;                   /* ..  */
      residue=0;                        /* clear accumulator  */
      decCopyFit(res, rhs, set, &residue, status); /* copy (as needed)  */
      res->bits^=negate;                /* flip if rhs was negated  */
      #if DECSUBSET
      if (set->extended) {              /* exponents on zeros count  */
      #endif
        /* exponent will be the lower of the two  */
        adjust=lexp-res->exponent;      /* adjustment needed [if -ve]  */
        Din_Go(1133,2048);if (ISZERO(res)) {              /* both 0: special IEEE 754 rules  */
          Din_Go(1124,2048);if (adjust<0) {/*297*/Din_Go(1125,2048);res->exponent=lexp;/*298*/}  /* set exponent  */
          /* 0-0 gives +0 unless rounding to -infinity, and -0-0 gives -0  */
          Din_Go(1128,2048);if (diffsign) {
            Din_Go(1126,2048);if (set->round!=DEC_ROUND_FLOOR) {/*299*/Din_Go(1127,2048);res->bits=0;/*300*/}
             else res->bits=DECNEG;     /* preserve 0 sign  */
            }
          }
         else { /* non-0 res  */
          Din_Go(1129,2048);if (adjust<0) {     /* 0-padding needed  */
            Din_Go(1130,2048);if ((res->digits-adjust)>set->digits) {
              Din_Go(1131,2048);adjust=res->digits-set->digits;     /* to fit exactly  */
              *status|=DEC_Rounded;               /* [but exact]  */
              }
            Din_Go(1132,2048);res->digits=decShiftToMost(res->lsu, res->digits, -adjust);
            res->exponent+=adjust;                /* set the exponent.  */
            }
          } /* non-0 res  */
      #if DECSUBSET
        } /* extended  */
      #endif
      decFinish(res, set, &residue, status);      /* clean and finalize  */
      Din_Go(1134,2048);break;}

    Din_Go(1142,2048);if (ISZERO(rhs)) {                  /* [lhs is non-zero]  */
      Int adjust;                       /* work  */
Din_Go(1136,2048);      Int rexp=rhs->exponent;           /* save in case RHS==RES  */
      bits=rhs->bits;                   /* be clean  */
      residue=0;                        /* clear accumulator  */
      decCopyFit(res, lhs, set, &residue, status); /* copy (as needed)  */
      #if DECSUBSET
      if (set->extended) {              /* exponents on zeros count  */
      #endif
        /* exponent will be the lower of the two  */
        /* [0-0 case handled above]  */
        adjust=rexp-res->exponent;      /* adjustment needed [if -ve]  */
        Din_Go(1140,2048);if (adjust<0) {     /* 0-padding needed  */
          Din_Go(1137,2048);if ((res->digits-adjust)>set->digits) {
            Din_Go(1138,2048);adjust=res->digits-set->digits;     /* to fit exactly  */
            *status|=DEC_Rounded;               /* [but exact]  */
            }
          Din_Go(1139,2048);res->digits=decShiftToMost(res->lsu, res->digits, -adjust);
          res->exponent+=adjust;                /* set the exponent.  */
          }
      #if DECSUBSET
        } /* extended  */
      #endif
      decFinish(res, set, &residue, status);      /* clean and finalize  */
      Din_Go(1141,2048);break;}

    /* [NB: both fastpath and mainpath code below assume these cases  */
    /* (notably 0-0) have already been handled]  */

    /* calculate the padding needed to align the operands  */
    Din_Go(1143,2048);padding=rhs->exponent-lhs->exponent;

    /* Fastpath cases where the numbers are aligned and normal, the RHS  */
    /* is all in one unit, no operand rounding is needed, and no carry,  */
    /* lengthening, or borrow is needed  */
    Din_Go(1156,2048);if (padding==0
        && rhs->digits<=DECDPUN
        && rhs->exponent>=set->emin     /* [some normals drop through]  */
        && rhs->exponent<=set->emax-set->digits+1 /* [could clamp]  */
        && rhs->digits<=reqdigits
        && lhs->digits<=reqdigits) {
Din_Go(1144,2048);      Int partial=*lhs->lsu;
      Din_Go(1155,2048);if (!diffsign) {                  /* adding  */
        Din_Go(1145,2048);partial+=*rhs->lsu;
        Din_Go(1149,2048);if ((partial<=DECDPUNMAX)       /* result fits in unit  */
         && (lhs->digits>=DECDPUN ||    /* .. and no digits-count change  */
             partial<(Int)powers[lhs->digits])) { /* ..  */
          Din_Go(1146,2048);if (res!=lhs) uprv_decNumberCopy(res, lhs);  /* not in place  */
          Din_Go(1147,2048);*res->lsu=(Unit)partial;      /* [copy could have overwritten RHS]  */
          Din_Go(1148,2048);break;
          }
        /* else drop out for careful add  */
        }
       else {                           /* signs differ  */
        Din_Go(1150,2048);partial-=*rhs->lsu;
        Din_Go(1154,2048);if (partial>0) { /* no borrow needed, and non-0 result  */
          Din_Go(1151,2048);if (res!=lhs) uprv_decNumberCopy(res, lhs);  /* not in place  */
          Din_Go(1152,2048);*res->lsu=(Unit)partial;
          /* this could have reduced digits [but result>0]  */
          res->digits=decGetDigits(res->lsu, D2U(res->digits));
          Din_Go(1153,2048);break;
          }
        /* else drop out for careful subtract  */
        }
      }

    /* Now align (pad) the lhs or rhs so they can be added or  */
    /* subtracted, as necessary.  If one number is much larger than  */
    /* the other (that is, if in plain form there is a least one  */
    /* digit between the lowest digit of one and the highest of the  */
    /* other) padding with up to DIGITS-1 trailing zeros may be  */
    /* needed; then apply rounding (as exotic rounding modes may be  */
    /* affected by the residue).  */
    Din_Go(1157,2048);rhsshift=0;               /* rhs shift to left (padding) in Units  */
    bits=lhs->bits;           /* assume sign is that of LHS  */
    mult=1;                   /* likely multiplier  */

    /* [if padding==0 the operands are aligned; no padding is needed]  */
    Din_Go(1171,2048);if (padding!=0) {
      /* some padding needed; always pad the RHS, as any required  */
      /* padding can then be effected by a simple combination of  */
      /* shifts and a multiply  */
      Flag swapped=0;
      Din_Go(1159,2048);if (padding<0) {                  /* LHS needs the padding  */
        Din_Go(1158,2048);const decNumber *t;
        padding=-padding;               /* will be +ve  */
        bits=(uByte)(rhs->bits^negate); /* assumed sign is now that of RHS  */
        t=lhs; lhs=rhs; rhs=t;
        swapped=1;
        }

      /* If, after pad, rhs would be longer than lhs by digits+1 or  */
      /* more then lhs cannot affect the answer, except as a residue,  */
      /* so only need to pad up to a length of DIGITS+1.  */
      Din_Go(1169,2048);if (rhs->digits+padding > lhs->digits+reqdigits+1) {
        /* The RHS is sufficient  */
        /* for residue use the relative sign indication...  */
Din_Go(1160,2048);        Int shift=reqdigits-rhs->digits;     /* left shift needed  */
        residue=1;                           /* residue for rounding  */
        Din_Go(1162,2048);if (diffsign) {/*301*/Din_Go(1161,2048);residue=-residue;/*302*/}      /* signs differ  */
        /* copy, shortening if necessary  */
        Din_Go(1163,2048);decCopyFit(res, rhs, set, &residue, status);
        /* if it was already shorter, then need to pad with zeros  */
        Din_Go(1165,2048);if (shift>0) {
          Din_Go(1164,2048);res->digits=decShiftToMost(res->lsu, res->digits, shift);
          res->exponent-=shift;              /* adjust the exponent.  */
          }
        /* flip the result sign if unswapped and rhs was negated  */
        Din_Go(1167,2048);if (!swapped) {/*303*/Din_Go(1166,2048);res->bits^=negate;/*304*/}
        decFinish(res, set, &residue, status);    /* done  */
        Din_Go(1168,2048);break;}

      /* LHS digits may affect result  */
      Din_Go(1170,2048);rhsshift=D2U(padding+1)-1;        /* this much by Unit shift ..  */
      mult=powers[padding-(rhsshift*DECDPUN)]; /* .. this by multiplication  */
      } /* padding needed  */

    Din_Go(1173,2048);if (diffsign) {/*305*/Din_Go(1172,2048);mult=-mult;/*306*/}           /* signs differ  */

    /* determine the longer operand  */
    Din_Go(1174,2048);maxdigits=rhs->digits+padding;      /* virtual length of RHS  */
    Din_Go(1176,2048);if (lhs->digits>maxdigits) {/*307*/Din_Go(1175,2048);maxdigits=lhs->digits;/*308*/}

    /* Decide on the result buffer to use; if possible place directly  */
    /* into result.  */
    Din_Go(1177,2048);acc=res->lsu;                       /* assume add direct to result  */
    /* If destructive overlap, or the number is too long, or a carry or  */
    /* borrow to DIGITS+1 might be possible, a buffer must be used.  */
    /* [Might be worth more sophisticated tests when maxdigits==reqdigits]  */
    Din_Go(1185,2048);if ((maxdigits>=reqdigits)          /* is, or could be, too large  */
     || (res==rhs && rhsshift>0)) {     /* destructive overlap  */
      /* buffer needed, choose it; units for maxdigits digits will be  */
      /* needed, +1 Unit for carry or borrow  */
      Int need=D2U(maxdigits)+1;
      Din_Go(1178,2048);acc=accbuff;                      /* assume use local buffer  */
      Din_Go(1184,2048);if (need*sizeof(Unit)>sizeof(accbuff)) {
        /* printf("malloc add %ld %ld\n", need, sizeof(accbuff));  */
        Din_Go(1179,2048);allocacc=(Unit *)malloc(need*sizeof(Unit));
        Din_Go(1182,2048);if (allocacc==NULL) {           /* hopeless -- abandon  */
          Din_Go(1180,2048);*status|=DEC_Insufficient_storage;
          Din_Go(1181,2048);break;}
        Din_Go(1183,2048);acc=allocacc;
        }
      }

    Din_Go(1186,2048);res->bits=(uByte)(bits&DECNEG);     /* it's now safe to overwrite..  */
    res->exponent=lhs->exponent;        /* .. operands (even if aliased)  */

    #if DECTRACE
      decDumpAr('A', lhs->lsu, D2U(lhs->digits));
      decDumpAr('B', rhs->lsu, D2U(rhs->digits));
      printf("  :h: %ld %ld\n", rhsshift, mult);
    #endif

    /* add [A+B*m] or subtract [A+B*(-m)]  */
    U_ASSERT(rhs->digits > 0);
    U_ASSERT(lhs->digits > 0);
    res->digits=decUnitAddSub(lhs->lsu, D2U(lhs->digits),
                              rhs->lsu, D2U(rhs->digits),
                              rhsshift, acc, mult)
               *DECDPUN;           /* [units -> digits]  */
    Din_Go(1188,2048);if (res->digits<0) {           /* borrowed...  */
      Din_Go(1187,2048);res->digits=-res->digits;
      res->bits^=DECNEG;           /* flip the sign  */
      }
    #if DECTRACE
      decDumpAr('+', acc, D2U(res->digits));
    #endif

    /* If a buffer was used the result must be copied back, possibly  */
    /* shortening.  (If no buffer was used then the result must have  */
    /* fit, so can't need rounding and residue must be 0.)  */
    Din_Go(1189,2048);residue=0;                     /* clear accumulator  */
    Din_Go(1193,2048);if (acc!=res->lsu) {
      #if DECSUBSET
      if (set->extended) {         /* round from first significant digit  */
      #endif
        /* remove leading zeros that were added due to rounding up to  */
        /* integral Units -- before the test for rounding.  */
        Din_Go(1190,2048);if (res->digits>reqdigits)
          {/*309*/Din_Go(1191,2048);res->digits=decGetDigits(acc, D2U(res->digits));/*310*/}
        Din_Go(1192,2048);decSetCoeff(res, set, acc, res->digits, &residue, status);
      #if DECSUBSET
        }
       else { /* subset arithmetic rounds from original significant digit  */
        /* May have an underestimate.  This only occurs when both  */
        /* numbers fit in DECDPUN digits and are padding with a  */
        /* negative multiple (-10, -100...) and the top digit(s) become  */
        /* 0.  (This only matters when using X3.274 rules where the  */
        /* leading zero could be included in the rounding.)  */
        if (res->digits<maxdigits) {
          *(acc+D2U(res->digits))=0; /* ensure leading 0 is there  */
          res->digits=maxdigits;
          }
         else {
          /* remove leading zeros that added due to rounding up to  */
          /* integral Units (but only those in excess of the original  */
          /* maxdigits length, unless extended) before test for rounding.  */
          if (res->digits>reqdigits) {
            res->digits=decGetDigits(acc, D2U(res->digits));
            if (res->digits<maxdigits) res->digits=maxdigits;
            }
          }
        decSetCoeff(res, set, acc, res->digits, &residue, status);
        /* Now apply rounding if needed before removing leading zeros.  */
        /* This is safe because subnormals are not a possibility  */
        if (residue!=0) {
          decApplyRound(res, set, residue, status);
          residue=0;                 /* did what needed to be done  */
          }
        } /* subset  */
      #endif
      } /* used buffer  */

    /* strip leading zeros [these were left on in case of subset subtract]  */
    Din_Go(1194,2048);res->digits=decGetDigits(res->lsu, D2U(res->digits));

    /* apply checks and rounding  */
    decFinish(res, set, &residue, status);

    /* "When the sum of two operands with opposite signs is exactly  */
    /* zero, the sign of that sum shall be '+' in all rounding modes  */
    /* except round toward -Infinity, in which mode that sign shall be  */
    /* '-'."  [Subset zeros also never have '-', set by decFinish.]  */
    Din_Go(1196,2048);if (ISZERO(res) && diffsign
     #if DECSUBSET
     && set->extended
     #endif
     && (*status&DEC_Inexact)==0) {
      Din_Go(1195,2048);if (set->round==DEC_ROUND_FLOOR) res->bits|=DECNEG;   /* sign -  */
                                  else res->bits&=~DECNEG;  /* sign +  */
      }
    } while(0);                              /* end protected  */

  if (allocacc!=NULL) free(allocacc);        /* drop any storage used  */
  #if DECSUBSET
  if (allocrhs!=NULL) free(allocrhs);        /* ..  */
  if (alloclhs!=NULL) free(alloclhs);        /* ..  */
  #endif
  {decNumber * ReplaceReturn49 = res; Din_Go(1198,2048); return ReplaceReturn49;}
  } /* decAddOp  */

/* ------------------------------------------------------------------ */
/* decDivideOp -- division operation                                  */
/*                                                                    */
/*  This routine performs the calculations for all four division      */
/*  operators (divide, divideInteger, remainder, remainderNear).      */
/*                                                                    */
/*  C=A op B                                                          */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X/X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*   op  is DIVIDE, DIVIDEINT, REMAINDER, or REMNEAR respectively.    */
/*   status is the usual accumulator                                  */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/*                                                                    */
/* ------------------------------------------------------------------ */
/*   The underlying algorithm of this routine is the same as in the   */
/*   1981 S/370 implementation, that is, non-restoring long division  */
/*   with bi-unit (rather than bi-digit) estimation for each unit     */
/*   multiplier.  In this pseudocode overview, complications for the  */
/*   Remainder operators and division residues for exact rounding are */
/*   omitted for clarity.                                             */
/*                                                                    */
/*     Prepare operands and handle special values                     */
/*     Test for x/0 and then 0/x                                      */
/*     Exp =Exp1 - Exp2                                               */
/*     Exp =Exp +len(var1) -len(var2)                                 */
/*     Sign=Sign1 * Sign2                                             */
/*     Pad accumulator (Var1) to double-length with 0's (pad1)        */
/*     Pad Var2 to same length as Var1                                */
/*     msu2pair/plus=1st 2 or 1 units of var2, +1 to allow for round  */
/*     have=0                                                         */
/*     Do until (have=digits+1 OR residue=0)                          */
/*       if exp<0 then if integer divide/residue then leave           */
/*       this_unit=0                                                  */
/*       Do forever                                                   */
/*          compare numbers                                           */
/*          if <0 then leave inner_loop                               */
/*          if =0 then (* quick exit without subtract *) do           */
/*             this_unit=this_unit+1; output this_unit                */
/*             leave outer_loop; end                                  */
/*          Compare lengths of numbers (mantissae):                   */
/*          If same then tops2=msu2pair -- {units 1&2 of var2}        */
/*                  else tops2=msu2plus -- {0, unit 1 of var2}        */
/*          tops1=first_unit_of_Var1*10**DECDPUN +second_unit_of_var1 */
/*          mult=tops1/tops2  -- Good and safe guess at divisor       */
/*          if mult=0 then mult=1                                     */
/*          this_unit=this_unit+mult                                  */
/*          subtract                                                  */
/*          end inner_loop                                            */
/*        if have\=0 | this_unit\=0 then do                           */
/*          output this_unit                                          */
/*          have=have+1; end                                          */
/*        var2=var2/10                                                */
/*        exp=exp-1                                                   */
/*        end outer_loop                                              */
/*     exp=exp+1   -- set the proper exponent                         */
/*     if have=0 then generate answer=0                               */
/*     Return (Result is defined by Var1)                             */
/*                                                                    */
/* ------------------------------------------------------------------ */
/* Two working buffers are needed during the division; one (digits+   */
/* 1) to accumulate the result, and the other (up to 2*digits+1) for  */
/* long subtractions.  These are acc and var1 respectively.           */
/* var1 is a copy of the lhs coefficient, var2 is the rhs coefficient.*/
/* The static buffers may be larger than might be expected to allow   */
/* for calls from higher-level funtions (notable exp).                */
/* ------------------------------------------------------------------ */
static decNumber * decDivideOp(decNumber *res,
                               const decNumber *lhs, const decNumber *rhs,
                               decContext *set, Flag op, uInt *status) {
  #if DECSUBSET
  decNumber *alloclhs=NULL;        /* non-NULL if rounded lhs allocated  */
  decNumber *allocrhs=NULL;        /* .., rhs  */
  #endif
  Unit  accbuff[SD2U(DECBUFFER+DECDPUN+10)]; /* local buffer  */
  Unit  *acc=accbuff;              /* -> accumulator array for result  */
  Unit  *allocacc=NULL;            /* -> allocated buffer, iff allocated  */
  Unit  *accnext;                  /* -> where next digit will go  */
  Int   acclength;                 /* length of acc needed [Units]  */
  Int   accunits;                  /* count of units accumulated  */
  Int   accdigits;                 /* count of digits accumulated  */

  Unit  varbuff[SD2U(DECBUFFER*2+DECDPUN)];  /* buffer for var1  */
  Unit  *var1=varbuff;             /* -> var1 array for long subtraction  */
  Unit  *varalloc=NULL;            /* -> allocated buffer, iff used  */
  Unit  *msu1;                     /* -> msu of var1  */

  const Unit *var2;                /* -> var2 array  */
  const Unit *msu2;                /* -> msu of var2  */
  Int   msu2plus;                  /* msu2 plus one [does not vary]  */
  eInt  msu2pair;                  /* msu2 pair plus one [does not vary]  */

  Int   var1units, var2units;      /* actual lengths  */
  Int   var2ulen;                  /* logical length (units)  */
  Int   var1initpad=0;             /* var1 initial padding (digits)  */
  Int   maxdigits;                 /* longest LHS or required acc length  */
  Int   mult;                      /* multiplier for subtraction  */
  Unit  thisunit;                  /* current unit being accumulated  */
  Int   residue;                   /* for rounding  */
  Int   reqdigits=set->digits;     /* requested DIGITS  */
  Int   exponent;                  /* working exponent  */
  Int   maxexponent=0;             /* DIVIDE maximum exponent if unrounded  */
  uByte bits;                      /* working sign  */
  Unit  *target;                   /* work  */
  const Unit *source;              /* ..  */
  uInt  const *pow;                /* ..  */
  Int   shift, cut;                /* ..  */
  #if DECSUBSET
  Int   dropped;                   /* work  */
  #endif

  #if DECCHECK
  if (decCheckOperands(res, lhs, rhs, set)) return res;
  #endif

  Din_Go(1383,2048);do {                             /* protect allocated storage  */
    #if DECSUBSET
    if (!set->extended) {
      /* reduce operands and set lostDigits status, as needed  */
      if (lhs->digits>reqdigits) {
        alloclhs=decRoundOperand(lhs, set, status);
        if (alloclhs==NULL) break;
        lhs=alloclhs;
        }
      if (rhs->digits>reqdigits) {
        allocrhs=decRoundOperand(rhs, set, status);
        if (allocrhs==NULL) break;
        rhs=allocrhs;
        }
      }
    #endif
    /* [following code does not require input rounding]  */

    Din_Go(1199,2048);bits=(lhs->bits^rhs->bits)&DECNEG;  /* assumed sign for divisions  */

    /* handle infinities and NaNs  */
    Din_Go(1216,2048);if (SPECIALARGS) {                  /* a special bit set  */
      Din_Go(1200,2048);if (SPECIALARGS & (DECSNAN | DECNAN)) { /* one or two NaNs  */
        Din_Go(1201,2048);decNaNs(res, lhs, rhs, set, status);
        Din_Go(1202,2048);break;
        }
      /* one or two infinities  */
      Din_Go(1215,2048);if (decNumberIsInfinite(lhs)) {   /* LHS (dividend) is infinite  */
        Din_Go(1203,2048);if (decNumberIsInfinite(rhs) || /* two infinities are invalid ..  */
            op & (REMAINDER | REMNEAR)) { /* as is remainder of infinity  */
          Din_Go(1204,2048);*status|=DEC_Invalid_operation;
          Din_Go(1205,2048);break;
          }
        /* [Note that infinity/0 raises no exceptions]  */
        uprv_decNumberZero(res);
        Din_Go(1206,2048);res->bits=bits|DECINF;          /* set +/- infinity  */
        Din_Go(1207,2048);break;
        }
       else {                           /* RHS (divisor) is infinite  */
        Din_Go(1208,2048);residue=0;
        Din_Go(1213,2048);if (op&(REMAINDER|REMNEAR)) {
          /* result is [finished clone of] lhs  */
          Din_Go(1209,2048);decCopyFit(res, lhs, set, &residue, status);
          }
         else {  /* a division  */
          uprv_decNumberZero(res);
          Din_Go(1210,2048);res->bits=bits;               /* set +/- zero  */
          /* for DIVIDEINT the exponent is always 0.  For DIVIDE, result  */
          /* is a 0 with infinitely negative exponent, clamped to minimum  */
          Din_Go(1212,2048);if (op&DIVIDE) {
            Din_Go(1211,2048);res->exponent=set->emin-set->digits+1;
            *status|=DEC_Clamped;
            }
          }
        decFinish(res, set, &residue, status);
        Din_Go(1214,2048);break;
        }
      }

    /* handle 0 rhs (x/0)  */
    Din_Go(1222,2048);if (ISZERO(rhs)) {                  /* x/0 is always exceptional  */
      Din_Go(1217,2048);if (ISZERO(lhs)) {
        uprv_decNumberZero(res);             /* [after lhs test]  */
        Din_Go(1218,2048);*status|=DEC_Division_undefined;/* 0/0 will become NaN  */
        }
       else {
        uprv_decNumberZero(res);
        Din_Go(1220,2048);if (op&(REMAINDER|REMNEAR)) *status|=DEC_Invalid_operation;
         else {
          Din_Go(1219,2048);*status|=DEC_Division_by_zero; /* x/0  */
          res->bits=bits|DECINF;         /* .. is +/- Infinity  */
          }
        }
      Din_Go(1221,2048);break;}

    /* handle 0 lhs (0/x)  */
    Din_Go(1231,2048);if (ISZERO(lhs)) {                  /* 0/x [x!=0]  */
      #if DECSUBSET
      if (!set->extended) uprv_decNumberZero(res);
       else {
      #endif
        Din_Go(1223,2048);if (op&DIVIDE) {
          Din_Go(1224,2048);residue=0;
          exponent=lhs->exponent-rhs->exponent; /* ideal exponent  */
          uprv_decNumberCopy(res, lhs);      /* [zeros always fit]  */
          res->bits=bits;               /* sign as computed  */
          res->exponent=exponent;       /* exponent, too  */
          decFinalize(res, set, &residue, status);   /* check exponent  */
          }
         else {/*451*/Din_Go(1225,2048);if (op&DIVIDEINT) {
          uprv_decNumberZero(res);           /* integer 0  */
          Din_Go(1226,2048);res->bits=bits;               /* sign as computed  */
          }
         else {                         /* a remainder  */
          Din_Go(1227,2048);exponent=rhs->exponent;       /* [save in case overwrite]  */
          uprv_decNumberCopy(res, lhs);      /* [zeros always fit]  */
          Din_Go(1229,2048);if (exponent<res->exponent) {/*453*/Din_Go(1228,2048);res->exponent=exponent;/*454*/} /* use lower  */
          ;/*452*/}}
      #if DECSUBSET
        }
      #endif
      Din_Go(1230,2048);break;}

    /* Precalculate exponent.  This starts off adjusted (and hence fits  */
    /* in 31 bits) and becomes the usual unadjusted exponent as the  */
    /* division proceeds.  The order of evaluation is important, here,  */
    /* to avoid wrap.  */
    Din_Go(1232,2048);exponent=(lhs->exponent+lhs->digits)-(rhs->exponent+rhs->digits);

    /* If the working exponent is -ve, then some quick exits are  */
    /* possible because the quotient is known to be <1  */
    /* [for REMNEAR, it needs to be < -1, as -0.5 could need work]  */
    Din_Go(1240,2048);if (exponent<0 && !(op==DIVIDE)) {
      Din_Go(1233,2048);if (op&DIVIDEINT) {
        uprv_decNumberZero(res);                  /* integer part is 0  */
        #if DECSUBSET
        if (set->extended)
        #endif
          Din_Go(1234,2048);res->bits=bits;                    /* set +/- zero  */
        Din_Go(1235,2048);break;}
      /* fastpath remainders so long as the lhs has the smaller  */
      /* (or equal) exponent  */
      Din_Go(1239,2048);if (lhs->exponent<=rhs->exponent) {
        Din_Go(1236,2048);if (op&REMAINDER || exponent<-1) {
          /* It is REMAINDER or safe REMNEAR; result is [finished  */
          /* clone of] lhs  (r = x - 0*y)  */
          Din_Go(1237,2048);residue=0;
          decCopyFit(res, lhs, set, &residue, status);
          decFinish(res, set, &residue, status);
          Din_Go(1238,2048);break;
          }
        /* [unsafe REMNEAR drops through]  */
        }
      } /* fastpaths  */

    /* Long (slow) division is needed; roll up the sleeves... */

    /* The accumulator will hold the quotient of the division.  */
    /* If it needs to be too long for stack storage, then allocate.  */
    Din_Go(1241,2048);acclength=D2U(reqdigits+DECDPUN);   /* in Units  */
    Din_Go(1247,2048);if (acclength*sizeof(Unit)>sizeof(accbuff)) {
      /* printf("malloc dvacc %ld units\n", acclength);  */
      Din_Go(1242,2048);allocacc=(Unit *)malloc(acclength*sizeof(Unit));
      Din_Go(1245,2048);if (allocacc==NULL) {             /* hopeless -- abandon  */
        Din_Go(1243,2048);*status|=DEC_Insufficient_storage;
        Din_Go(1244,2048);break;}
      Din_Go(1246,2048);acc=allocacc;                     /* use the allocated space  */
      }

    /* var1 is the padded LHS ready for subtractions.  */
    /* If it needs to be too long for stack storage, then allocate.  */
    /* The maximum units needed for var1 (long subtraction) is:  */
    /* Enough for  */
    /*     (rhs->digits+reqdigits-1) -- to allow full slide to right  */
    /* or  (lhs->digits)             -- to allow for long lhs  */
    /* whichever is larger  */
    /*   +1                -- for rounding of slide to right  */
    /*   +1                -- for leading 0s  */
    /*   +1                -- for pre-adjust if a remainder or DIVIDEINT  */
    /* [Note: unused units do not participate in decUnitAddSub data]  */
    Din_Go(1248,2048);maxdigits=rhs->digits+reqdigits-1;
    Din_Go(1250,2048);if (lhs->digits>maxdigits) {/*455*/Din_Go(1249,2048);maxdigits=lhs->digits;/*456*/}
    Din_Go(1251,2048);var1units=D2U(maxdigits)+2;
    /* allocate a guard unit above msu1 for REMAINDERNEAR  */
    Din_Go(1253,2048);if (!(op&DIVIDE)) {/*457*/Din_Go(1252,2048);var1units++;/*458*/}
    Din_Go(1259,2048);if ((var1units+1)*sizeof(Unit)>sizeof(varbuff)) {
      /* printf("malloc dvvar %ld units\n", var1units+1);  */
      Din_Go(1254,2048);varalloc=(Unit *)malloc((var1units+1)*sizeof(Unit));
      Din_Go(1257,2048);if (varalloc==NULL) {             /* hopeless -- abandon  */
        Din_Go(1255,2048);*status|=DEC_Insufficient_storage;
        Din_Go(1256,2048);break;}
      Din_Go(1258,2048);var1=varalloc;                    /* use the allocated space  */
      }

    /* Extend the lhs and rhs to full long subtraction length.  The lhs  */
    /* is truly extended into the var1 buffer, with 0 padding, so a  */
    /* subtract in place is always possible.  The rhs (var2) has  */
    /* virtual padding (implemented by decUnitAddSub).  */
    /* One guard unit was allocated above msu1 for rem=rem+rem in  */
    /* REMAINDERNEAR.  */
    Din_Go(1260,2048);msu1=var1+var1units-1;              /* msu of var1  */
    source=lhs->lsu+D2U(lhs->digits)-1; /* msu of input array  */
    Din_Go(1262,2048);for (target=msu1; source>=lhs->lsu; source--, target--) {/*459*/Din_Go(1261,2048);*target=*source;/*460*/}
    Din_Go(1264,2048);for (; target>=var1; target--) {/*461*/Din_Go(1263,2048);*target=0;/*462*/}

    /* rhs (var2) is left-aligned with var1 at the start  */
    Din_Go(1265,2048);var2ulen=var1units;                 /* rhs logical length (units)  */
    var2units=D2U(rhs->digits);         /* rhs actual length (units)  */
    var2=rhs->lsu;                      /* -> rhs array  */
    msu2=var2+var2units-1;              /* -> msu of var2 [never changes]  */
    /* now set up the variables which will be used for estimating the  */
    /* multiplication factor.  If these variables are not exact, add  */
    /* 1 to make sure that the multiplier is never overestimated.  */
    msu2plus=*msu2;                     /* it's value ..  */
    Din_Go(1267,2048);if (var2units>1) {/*463*/Din_Go(1266,2048);msu2plus++;/*464*/}        /* .. +1 if any more  */
    Din_Go(1268,2048);msu2pair=(eInt)*msu2*(DECDPUNMAX+1);/* top two pair ..  */
    Din_Go(1272,2048);if (var2units>1) {                  /* .. [else treat 2nd as 0]  */
      Din_Go(1269,2048);msu2pair+=*(msu2-1);              /* ..  */
      Din_Go(1271,2048);if (var2units>2) {/*465*/Din_Go(1270,2048);msu2pair++;/*466*/}      /* .. +1 if any more  */
      }

    /* The calculation is working in units, which may have leading zeros,  */
    /* but the exponent was calculated on the assumption that they are  */
    /* both left-aligned.  Adjust the exponent to compensate: add the  */
    /* number of leading zeros in var1 msu and subtract those in var2 msu.  */
    /* [This is actually done by counting the digits and negating, as  */
    /* lead1=DECDPUN-digits1, and similarly for lead2.]  */
    Din_Go(1274,2048);for (pow=&powers[1]; *msu1>=*pow; pow++) {/*467*/Din_Go(1273,2048);exponent--;/*468*/}
    Din_Go(1276,2048);for (pow=&powers[1]; *msu2>=*pow; pow++) {/*469*/Din_Go(1275,2048);exponent++;/*470*/}

    /* Now, if doing an integer divide or remainder, ensure that  */
    /* the result will be Unit-aligned.  To do this, shift the var1  */
    /* accumulator towards least if need be.  (It's much easier to  */
    /* do this now than to reassemble the residue afterwards, if  */
    /* doing a remainder.)  Also ensure the exponent is not negative.  */
    Din_Go(1286,2048);if (!(op&DIVIDE)) {
      Unit *u;                          /* work  */
      /* save the initial 'false' padding of var1, in digits  */
      Din_Go(1277,2048);var1initpad=(var1units-D2U(lhs->digits))*DECDPUN;
      /* Determine the shift to do.  */
      Din_Go(1279,2048);if (exponent<0) {/*471*/Din_Go(1278,2048);cut=-exponent;/*472*/}
       else cut=DECDPUN-exponent%DECDPUN;
      Din_Go(1280,2048);decShiftToLeast(var1, var1units, cut);
      exponent+=cut;                    /* maintain numerical value  */
      var1initpad-=cut;                 /* .. and reduce padding  */
      /* clean any most-significant units which were just emptied  */
      Din_Go(1282,2048);for (u=msu1; cut>=DECDPUN; cut-=DECDPUN, u--) {/*473*/Din_Go(1281,2048);*u=0;/*474*/}
      } /* align  */
     else { /* is DIVIDE  */
      Din_Go(1283,2048);maxexponent=lhs->exponent-rhs->exponent;    /* save  */
      /* optimization: if the first iteration will just produce 0,  */
      /* preadjust to skip it [valid for DIVIDE only]  */
      Din_Go(1285,2048);if (*msu1<*msu2) {
        Din_Go(1284,2048);var2ulen--;                     /* shift down  */
        exponent-=DECDPUN;              /* update the exponent  */
        }
      }

    /* ---- start the long-division loops ------------------------------  */
    Din_Go(1287,2048);accunits=0;                         /* no units accumulated yet  */
    accdigits=0;                        /* .. or digits  */
    accnext=acc+acclength-1;            /* -> msu of acc [NB: allows digits+1]  */
    Din_Go(1331,2048);for (;;) {                          /* outer forever loop  */
      Din_Go(1288,2048);thisunit=0;                       /* current unit assumed 0  */
      /* find the next unit  */
      Din_Go(1313,2048);for (;;) {                        /* inner forever loop  */
        /* strip leading zero units [from either pre-adjust or from  */
        /* subtract last time around].  Leave at least one unit.  */
        Din_Go(1289,2048);for (; *msu1==0 && msu1>var1; msu1--) {/*475*/Din_Go(1290,2048);var1units--;/*476*/}

        Din_Go(1292,2048);if (var1units<var2ulen) {/*477*/Din_Go(1291,2048);break;/*478*/}       /* var1 too low for subtract  */
        Din_Go(1309,2048);if (var1units==var2ulen) {           /* unit-by-unit compare needed  */
          /* compare the two numbers, from msu  */
          Din_Go(1293,2048);const Unit *pv1, *pv2;
          Unit v2;                           /* units to compare  */
          pv2=msu2;                          /* -> msu  */
          Din_Go(1301,2048);for (pv1=msu1; ; pv1--, pv2--) {
            /* v1=*pv1 -- always OK  */
            Din_Go(1294,2048);v2=0;                            /* assume in padding  */
            Din_Go(1296,2048);if (pv2>=var2) {/*479*/Din_Go(1295,2048);v2=*pv2;/*480*/}          /* in range  */
            Din_Go(1298,2048);if (*pv1!=v2) {/*481*/Din_Go(1297,2048);break;/*482*/}             /* no longer the same  */
            Din_Go(1300,2048);if (pv1==var1) {/*483*/Din_Go(1299,2048);break;/*484*/}            /* done; leave pv1 as is  */
            }
          /* here when all inspected or a difference seen  */
          Din_Go(1303,2048);if (*pv1<v2) {/*485*/Din_Go(1302,2048);break;/*486*/}                /* var1 too low to subtract  */
          Din_Go(1306,2048);if (*pv1==v2) {                    /* var1 == var2  */
            /* reach here if var1 and var2 are identical; subtraction  */
            /* would increase digit by one, and the residue will be 0 so  */
            /* the calculation is done; leave the loop with residue=0.  */
            Din_Go(1304,2048);thisunit++;                      /* as though subtracted  */
            *var1=0;                         /* set var1 to 0  */
            var1units=1;                     /* ..  */
            Din_Go(1305,2048);break;  /* from inner  */
            } /* var1 == var2  */
          /* *pv1>v2.  Prepare for real subtraction; the lengths are equal  */
          /* Estimate the multiplier (there's always a msu1-1)...  */
          /* Bring in two units of var2 to provide a good estimate.  */
          Din_Go(1307,2048);mult=(Int)(((eInt)*msu1*(DECDPUNMAX+1)+*(msu1-1))/msu2pair);
          } /* lengths the same  */
         else { /* var1units > var2ulen, so subtraction is safe  */
          /* The var2 msu is one unit towards the lsu of the var1 msu,  */
          /* so only one unit for var2 can be used.  */
          Din_Go(1308,2048);mult=(Int)(((eInt)*msu1*(DECDPUNMAX+1)+*(msu1-1))/msu2plus);
          }
        Din_Go(1311,2048);if (mult==0) {/*487*/Din_Go(1310,2048);mult=1;/*488*/}                 /* must always be at least 1  */
        /* subtraction needed; var1 is > var2  */
        Din_Go(1312,2048);thisunit=(Unit)(thisunit+mult);      /* accumulate  */
        /* subtract var1-var2, into var1; only the overlap needs  */
        /* processing, as this is an in-place calculation  */
        shift=var2ulen-var2units;
        #if DECTRACE
          decDumpAr('1', &var1[shift], var1units-shift);
          decDumpAr('2', var2, var2units);
          printf("m=%ld\n", -mult);
        #endif
        decUnitAddSub(&var1[shift], var1units-shift,
                      var2, var2units, 0,
                      &var1[shift], -mult);
        #if DECTRACE
          decDumpAr('#', &var1[shift], var1units-shift);
        #endif
        /* var1 now probably has leading zeros; these are removed at the  */
        /* top of the inner loop.  */
        } /* inner loop  */

      /* The next unit has been calculated in full; unless it's a  */
      /* leading zero, add to acc  */
      Din_Go(1322,2048);if (accunits!=0 || thisunit!=0) {      /* is first or non-zero  */
        Din_Go(1314,2048);*accnext=thisunit;                   /* store in accumulator  */
        /* account exactly for the new digits  */
        Din_Go(1318,2048);if (accunits==0) {
          Din_Go(1315,2048);accdigits++;                       /* at least one  */
          Din_Go(1317,2048);for (pow=&powers[1]; thisunit>=*pow; pow++) {/*489*/Din_Go(1316,2048);accdigits++;/*490*/}
          }
         else accdigits+=DECDPUN;
        Din_Go(1319,2048);accunits++;                          /* update count  */
        accnext--;                           /* ready for next  */
        Din_Go(1321,2048);if (accdigits>reqdigits) {/*491*/Din_Go(1320,2048);break;/*492*/}      /* have enough digits  */
        }

      /* if the residue is zero, the operation is done (unless divide  */
      /* or divideInteger and still not enough digits yet)  */
      Din_Go(1327,2048);if (*var1==0 && var1units==1) {        /* residue is 0  */
        Din_Go(1323,2048);if (op&(REMAINDER|REMNEAR)) {/*493*/Din_Go(1324,2048);break;/*494*/}
        Din_Go(1326,2048);if ((op&DIVIDE) && (exponent<=maxexponent)) {/*495*/Din_Go(1325,2048);break;/*496*/}
        /* [drop through if divideInteger]  */
        }
      /* also done enough if calculating remainder or integer  */
      /* divide and just did the last ('units') unit  */
      Din_Go(1329,2048);if (exponent==0 && !(op&DIVIDE)) {/*497*/Din_Go(1328,2048);break;/*498*/}

      /* to get here, var1 is less than var2, so divide var2 by the per-  */
      /* Unit power of ten and go for the next digit  */
      Din_Go(1330,2048);var2ulen--;                            /* shift down  */
      exponent-=DECDPUN;                     /* update the exponent  */
      } /* outer loop  */

    /* ---- division is complete ---------------------------------------  */
    /* here: acc      has at least reqdigits+1 of good results (or fewer  */
    /*                if early stop), starting at accnext+1 (its lsu)  */
    /*       var1     has any residue at the stopping point  */
    /*       accunits is the number of digits collected in acc  */
    Din_Go(1334,2048);if (accunits==0) {             /* acc is 0  */
      Din_Go(1332,2048);accunits=1;                  /* show have a unit ..  */
      accdigits=1;                 /* ..  */
      *accnext=0;                  /* .. whose value is 0  */
      }
     else {/*499*/Din_Go(1333,2048);accnext++;/*500*/}               /* back to last placed  */
    /* accnext now -> lowest unit of result  */

    Din_Go(1335,2048);residue=0;                     /* assume no residue  */
    Din_Go(1381,2048);if (op&DIVIDE) {
      /* record the presence of any residue, for rounding  */
      Din_Go(1336,2048);if (*var1!=0 || var1units>1) {/*501*/Din_Go(1337,2048);residue=1;/*502*/}
       else { /* no residue  */
        /* Had an exact division; clean up spurious trailing 0s.  */
        /* There will be at most DECDPUN-1, from the final multiply,  */
        /* and then only if the result is non-0 (and even) and the  */
        /* exponent is 'loose'.  */
        #if DECDPUN>1
        Unit lsu=*accnext;
        if (!(lsu&0x01) && (lsu!=0)) {
          /* count the trailing zeros  */
          Int drop=0;
          for (;; drop++) {    /* [will terminate because lsu!=0]  */
            if (exponent>=maxexponent) break;     /* don't chop real 0s  */
            #if DECDPUN<=4
              if ((lsu-QUOT10(lsu, drop+1)
                  *powers[drop+1])!=0) break;     /* found non-0 digit  */
            #else
              if (lsu%powers[drop+1]!=0) break;   /* found non-0 digit  */
            #endif
            exponent++;
            }
          if (drop>0) {
            accunits=decShiftToLeast(accnext, accunits, drop);
            accdigits=decGetDigits(accnext, accunits);
            accunits=D2U(accdigits);
            /* [exponent was adjusted in the loop]  */
            }
          } /* neither odd nor 0  */
        #endif
        } /* exact divide  */
      } /* divide  */
     else /* op!=DIVIDE */ {
      /* check for coefficient overflow  */
      Din_Go(1338,2048);if (accdigits+exponent>reqdigits) {
        Din_Go(1339,2048);*status|=DEC_Division_impossible;
        Din_Go(1340,2048);break;
        }
      Din_Go(1380,2048);if (op & (REMAINDER|REMNEAR)) {
        /* [Here, the exponent will be 0, because var1 was adjusted  */
        /* appropriately.]  */
        Int postshift;                       /* work  */
        Flag wasodd=0;                       /* integer was odd  */
        Unit *quotlsu;                       /* for save  */
        Int  quotdigits;                     /* ..  */

        Din_Go(1341,2048);bits=lhs->bits;                      /* remainder sign is always as lhs  */

        /* Fastpath when residue is truly 0 is worthwhile [and  */
        /* simplifies the code below]  */
        Din_Go(1347,2048);if (*var1==0 && var1units==1) {      /* residue is 0  */
Din_Go(1342,2048);          Int exp=lhs->exponent;             /* save min(exponents)  */
          Din_Go(1344,2048);if (rhs->exponent<exp) {/*503*/Din_Go(1343,2048);exp=rhs->exponent;/*504*/}
          uprv_decNumberZero(res);                /* 0 coefficient  */
          #if DECSUBSET
          if (set->extended)
          #endif
          Din_Go(1345,2048);res->exponent=exp;                 /* .. with proper exponent  */
          res->bits=(uByte)(bits&DECNEG);          /* [cleaned]  */
          decFinish(res, set, &residue, status);   /* might clamp  */
          Din_Go(1346,2048);break;
          }
        /* note if the quotient was odd  */
        Din_Go(1349,2048);if (*accnext & 0x01) {/*505*/Din_Go(1348,2048);wasodd=1;/*506*/}       /* acc is odd  */
        Din_Go(1350,2048);quotlsu=accnext;                     /* save in case need to reinspect  */
        quotdigits=accdigits;                /* ..  */

        /* treat the residue, in var1, as the value to return, via acc  */
        /* calculate the unused zero digits.  This is the smaller of:  */
        /*   var1 initial padding (saved above)  */
        /*   var2 residual padding, which happens to be given by:  */
        postshift=var1initpad+exponent-lhs->exponent+rhs->exponent;
        /* [the 'exponent' term accounts for the shifts during divide]  */
        Din_Go(1352,2048);if (var1initpad<postshift) {/*507*/Din_Go(1351,2048);postshift=var1initpad;/*508*/}

        /* shift var1 the requested amount, and adjust its digits  */
        Din_Go(1353,2048);var1units=decShiftToLeast(var1, var1units, postshift);
        accnext=var1;
        accdigits=decGetDigits(var1, var1units);
        accunits=D2U(accdigits);

        exponent=lhs->exponent;         /* exponent is smaller of lhs & rhs  */
        Din_Go(1355,2048);if (rhs->exponent<exponent) {/*509*/Din_Go(1354,2048);exponent=rhs->exponent;/*510*/}

        /* Now correct the result if doing remainderNear; if it  */
        /* (looking just at coefficients) is > rhs/2, or == rhs/2 and  */
        /* the integer was odd then the result should be rem-rhs.  */
        Din_Go(1379,2048);if (op&REMNEAR) {
          Int compare, tarunits;        /* work  */
          Unit *up;                     /* ..  */
          /* calculate remainder*2 into the var1 buffer (which has  */
          /* 'headroom' of an extra unit and hence enough space)  */
          /* [a dedicated 'double' loop would be faster, here]  */
          Din_Go(1356,2048);tarunits=decUnitAddSub(accnext, accunits, accnext, accunits,
                                 0, accnext, 1);
          /* decDumpAr('r', accnext, tarunits);  */

          /* Here, accnext (var1) holds tarunits Units with twice the  */
          /* remainder's coefficient, which must now be compared to the  */
          /* RHS.  The remainder's exponent may be smaller than the RHS's.  */
          compare=decUnitCompare(accnext, tarunits, rhs->lsu, D2U(rhs->digits),
                                 rhs->exponent-exponent);
          Din_Go(1359,2048);if (compare==BADINT) {             /* deep trouble  */
            Din_Go(1357,2048);*status|=DEC_Insufficient_storage;
            Din_Go(1358,2048);break;}

          /* now restore the remainder by dividing by two; the lsu  */
          /* is known to be even.  */
          Din_Go(1364,2048);for (up=accnext; up<accnext+tarunits; up++) {
            Int half;              /* half to add to lower unit  */
            Din_Go(1360,2048);half=*up & 0x01;
            *up/=2;                /* [shift]  */
            Din_Go(1362,2048);if (!half) {/*511*/Din_Go(1361,2048);continue;/*512*/}
            Din_Go(1363,2048);*(up-1)+=(DECDPUNMAX+1)/2;
            }
          /* [accunits still describes the original remainder length]  */

          Din_Go(1378,2048);if (compare>0 || (compare==0 && wasodd)) { /* adjustment needed  */
            Int exp, expunits, exprem;       /* work  */
            /* This is effectively causing round-up of the quotient,  */
            /* so if it was the rare case where it was full and all  */
            /* nines, it would overflow and hence division-impossible  */
            /* should be raised  */
            Flag allnines=0;                 /* 1 if quotient all nines  */
            Din_Go(1373,2048);if (quotdigits==reqdigits) {     /* could be borderline  */
              Din_Go(1365,2048);for (up=quotlsu; ; up++) {
                Din_Go(1366,2048);if (quotdigits>DECDPUN) {
                  Din_Go(1367,2048);if (*up!=DECDPUNMAX) {/*513*/Din_Go(1368,2048);break;/*514*/}/* non-nines  */
                  }
                 else {                      /* this is the last Unit  */
                  Din_Go(1369,2048);if (*up==powers[quotdigits]-1) {/*515*/Din_Go(1370,2048);allnines=1;/*516*/}
                  Din_Go(1371,2048);break;
                  }
                Din_Go(1372,2048);quotdigits-=DECDPUN;         /* checked those digits  */
                } /* up  */
              } /* borderline check  */
            Din_Go(1376,2048);if (allnines) {
              Din_Go(1374,2048);*status|=DEC_Division_impossible;
              Din_Go(1375,2048);break;}

            /* rem-rhs is needed; the sign will invert.  Again, var1  */
            /* can safely be used for the working Units array.  */
            Din_Go(1377,2048);exp=rhs->exponent-exponent;      /* RHS padding needed  */
            /* Calculate units and remainder from exponent.  */
            expunits=exp/DECDPUN;
            exprem=exp%DECDPUN;
            /* subtract [A+B*(-m)]; the result will always be negative  */
            accunits=-decUnitAddSub(accnext, accunits,
                                    rhs->lsu, D2U(rhs->digits),
                                    expunits, accnext, -(Int)powers[exprem]);
            accdigits=decGetDigits(accnext, accunits); /* count digits exactly  */
            accunits=D2U(accdigits);    /* and recalculate the units for copy  */
            /* [exponent is as for original remainder]  */
            bits^=DECNEG;               /* flip the sign  */
            }
          } /* REMNEAR  */
        } /* REMAINDER or REMNEAR  */
      } /* not DIVIDE  */

    /* Set exponent and bits  */
    Din_Go(1382,2048);res->exponent=exponent;
    res->bits=(uByte)(bits&DECNEG);          /* [cleaned]  */

    /* Now the coefficient.  */
    decSetCoeff(res, set, accnext, accdigits, &residue, status);

    decFinish(res, set, &residue, status);   /* final cleanup  */

    #if DECSUBSET
    /* If a divide then strip trailing zeros if subset [after round]  */
    if (!set->extended && (op==DIVIDE)) decTrim(res, set, 0, 1, &dropped);
    #endif
    } while(0);                              /* end protected  */

  if (varalloc!=NULL) free(varalloc);   /* drop any storage used  */
  if (allocacc!=NULL) free(allocacc);   /* ..  */
  #if DECSUBSET
  if (allocrhs!=NULL) free(allocrhs);   /* ..  */
  if (alloclhs!=NULL) free(alloclhs);   /* ..  */
  #endif
  {decNumber * ReplaceReturn48 = res; Din_Go(1384,2048); return ReplaceReturn48;}
  } /* decDivideOp  */

/* ------------------------------------------------------------------ */
/* decMultiplyOp -- multiplication operation                          */
/*                                                                    */
/*  This routine performs the multiplication C=A x B.                 */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X*X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*   status is the usual accumulator                                  */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/*                                                                    */
/* ------------------------------------------------------------------ */
/* 'Classic' multiplication is used rather than Karatsuba, as the     */
/* latter would give only a minor improvement for the short numbers   */
/* expected to be handled most (and uses much more memory).           */
/*                                                                    */
/* There are two major paths here: the general-purpose ('old code')   */
/* path which handles all DECDPUN values, and a fastpath version      */
/* which is used if 64-bit ints are available, DECDPUN<=4, and more   */
/* than two calls to decUnitAddSub would be made.                     */
/*                                                                    */
/* The fastpath version lumps units together into 8-digit or 9-digit  */
/* chunks, and also uses a lazy carry strategy to minimise expensive  */
/* 64-bit divisions.  The chunks are then broken apart again into     */
/* units for continuing processing.  Despite this overhead, the       */
/* fastpath can speed up some 16-digit operations by 10x (and much    */
/* more for higher-precision calculations).                           */
/*                                                                    */
/* A buffer always has to be used for the accumulator; in the         */
/* fastpath, buffers are also always needed for the chunked copies of */
/* of the operand coefficients.                                       */
/* Static buffers are larger than needed just for multiply, to allow  */
/* for calls from other operations (notably exp).                     */
/* ------------------------------------------------------------------ */
#define FASTMUL (DECUSE64 && DECDPUN<5)
static decNumber * decMultiplyOp(decNumber *res, const decNumber *lhs,
                                 const decNumber *rhs, decContext *set,
                                 uInt *status) {
  Int    accunits;                 /* Units of accumulator in use  */
  Int    exponent;                 /* work  */
  Int    residue=0;                /* rounding residue  */
  uByte  bits;                     /* result sign  */
  Unit  *acc;                      /* -> accumulator Unit array  */
  Int    needbytes;                /* size calculator  */
  void  *allocacc=NULL;            /* -> allocated accumulator, iff allocated  */
  Unit  accbuff[SD2U(DECBUFFER*4+1)]; /* buffer (+1 for DECBUFFER==0,  */
                                   /* *4 for calls from other operations)  */
  const Unit *mer, *mermsup;       /* work  */
  Int   madlength;                 /* Units in multiplicand  */
  Int   shift;                     /* Units to shift multiplicand by  */

  #if FASTMUL
    /* if DECDPUN is 1 or 3 work in base 10**9, otherwise  */
    /* (DECDPUN is 2 or 4) then work in base 10**8  */
    #if DECDPUN & 1                /* odd  */
      #define FASTBASE 1000000000  /* base  */
      #define FASTDIGS          9  /* digits in base  */
      #define FASTLAZY         18  /* carry resolution point [1->18]  */
    #else
      #define FASTBASE  100000000
      #define FASTDIGS          8
      #define FASTLAZY       1844  /* carry resolution point [1->1844]  */
    #endif
    /* three buffers are used, two for chunked copies of the operands  */
    /* (base 10**8 or base 10**9) and one base 2**64 accumulator with  */
    /* lazy carry evaluation  */
    uInt   zlhibuff[(DECBUFFER*2+1)/8+1]; /* buffer (+1 for DECBUFFER==0)  */
    uInt  *zlhi=zlhibuff;                 /* -> lhs array  */
    uInt  *alloclhi=NULL;                 /* -> allocated buffer, iff allocated  */
    uInt   zrhibuff[(DECBUFFER*2+1)/8+1]; /* buffer (+1 for DECBUFFER==0)  */
    uInt  *zrhi=zrhibuff;                 /* -> rhs array  */
    uInt  *allocrhi=NULL;                 /* -> allocated buffer, iff allocated  */
    uLong  zaccbuff[(DECBUFFER*2+1)/4+2]; /* buffer (+1 for DECBUFFER==0)  */
    /* [allocacc is shared for both paths, as only one will run]  */
    uLong *zacc=zaccbuff;          /* -> accumulator array for exact result  */
    #if DECDPUN==1
    Int    zoff;                   /* accumulator offset  */
    #endif
    uInt  *lip, *rip;              /* item pointers  */
    uInt  *lmsi, *rmsi;            /* most significant items  */
    Int    ilhs, irhs, iacc;       /* item counts in the arrays  */
    Int    lazy;                   /* lazy carry counter  */
    uLong  lcarry;                 /* uLong carry  */
    uInt   carry;                  /* carry (NB not uLong)  */
    Int    count;                  /* work  */
    const  Unit *cup;              /* ..  */
    Unit  *up;                     /* ..  */
    uLong *lp;                     /* ..  */
    Int    p;                      /* ..  */
  #endif

  #if DECSUBSET
    decNumber *alloclhs=NULL;      /* -> allocated buffer, iff allocated  */
    decNumber *allocrhs=NULL;      /* -> allocated buffer, iff allocated  */
  #endif

  #if DECCHECK
  if (decCheckOperands(res, lhs, rhs, set)) return res;
  #endif

  /* precalculate result sign  */
  bits=(uByte)((lhs->bits^rhs->bits)&DECNEG);

  /* handle infinities and NaNs  */
  Din_Go(1393,2048);if (SPECIALARGS) {               /* a special bit set  */
    Din_Go(1385,2048);if (SPECIALARGS & (DECSNAN | DECNAN)) { /* one or two NaNs  */
      Din_Go(1386,2048);decNaNs(res, lhs, rhs, set, status);
      {decNumber * ReplaceReturn47 = res; Din_Go(1387,2048); return ReplaceReturn47;}}
    /* one or two infinities; Infinity * 0 is invalid  */
    Din_Go(1390,2048);if (((lhs->bits & DECINF)==0 && ISZERO(lhs))
      ||((rhs->bits & DECINF)==0 && ISZERO(rhs))) {
      Din_Go(1388,2048);*status|=DEC_Invalid_operation;
      {decNumber * ReplaceReturn46 = res; Din_Go(1389,2048); return ReplaceReturn46;}}
    uprv_decNumberZero(res);
    Din_Go(1391,2048);res->bits=bits|DECINF;         /* infinity  */
    {decNumber * ReplaceReturn45 = res; Din_Go(1392,2048); return ReplaceReturn45;}}

  /* For best speed, as in DMSRCN [the original Rexx numerics  */
  /* module], use the shorter number as the multiplier (rhs) and  */
  /* the longer as the multiplicand (lhs) to minimise the number of  */
  /* adds (partial products)  */
  Din_Go(1395,2048);if (lhs->digits<rhs->digits) {   /* swap...  */
    Din_Go(1394,2048);const decNumber *hold=lhs;
    lhs=rhs;
    rhs=hold;
    }

  Din_Go(1459,2048);do {                             /* protect allocated storage  */
    #if DECSUBSET
    if (!set->extended) {
      /* reduce operands and set lostDigits status, as needed  */
      if (lhs->digits>set->digits) {
        alloclhs=decRoundOperand(lhs, set, status);
        if (alloclhs==NULL) break;
        lhs=alloclhs;
        }
      if (rhs->digits>set->digits) {
        allocrhs=decRoundOperand(rhs, set, status);
        if (allocrhs==NULL) break;
        rhs=allocrhs;
        }
      }
    #endif
    /* [following code does not require input rounding]  */

    #if FASTMUL                    /* fastpath can be used  */
    /* use the fast path if there are enough digits in the shorter  */
    /* operand to make the setup and takedown worthwhile  */
    #define NEEDTWO (DECDPUN*2)    /* within two decUnitAddSub calls  */
    Din_Go(1396,2048);if (rhs->digits>NEEDTWO) {     /* use fastpath...  */
      /* calculate the number of elements in each array  */
      Din_Go(1397,2048);ilhs=(lhs->digits+FASTDIGS-1)/FASTDIGS; /* [ceiling]  */
      irhs=(rhs->digits+FASTDIGS-1)/FASTDIGS; /* ..  */
      iacc=ilhs+irhs;

      /* allocate buffers if required, as usual  */
      needbytes=ilhs*sizeof(uInt);
      Din_Go(1399,2048);if (needbytes>(Int)sizeof(zlhibuff)) {
        Din_Go(1398,2048);alloclhi=(uInt *)malloc(needbytes);
        zlhi=alloclhi;}
      Din_Go(1400,2048);needbytes=irhs*sizeof(uInt);
      Din_Go(1402,2048);if (needbytes>(Int)sizeof(zrhibuff)) {
        Din_Go(1401,2048);allocrhi=(uInt *)malloc(needbytes);
        zrhi=allocrhi;}

      /* Allocating the accumulator space needs a special case when  */
      /* DECDPUN=1 because when converting the accumulator to Units  */
      /* after the multiplication each 8-byte item becomes 9 1-byte  */
      /* units.  Therefore iacc extra bytes are needed at the front  */
      /* (rounded up to a multiple of 8 bytes), and the uLong  */
      /* accumulator starts offset the appropriate number of units  */
      /* to the right to avoid overwrite during the unchunking.  */

      /* Make sure no signed int overflow below. This is always true */
      /* if the given numbers have less digits than DEC_MAX_DIGITS. */
      U_ASSERT(iacc <= INT32_MAX/sizeof(uLong));
      Din_Go(1403,2048);needbytes=iacc*sizeof(uLong);
      #if DECDPUN==1
      zoff=(iacc+7)/8;        /* items to offset by  */
      needbytes+=zoff*8;
      #endif
      Din_Go(1405,2048);if (needbytes>(Int)sizeof(zaccbuff)) {
        Din_Go(1404,2048);allocacc=(uLong *)malloc(needbytes);
        zacc=(uLong *)allocacc;}
      Din_Go(1408,2048);if (zlhi==NULL||zrhi==NULL||zacc==NULL) {
        Din_Go(1406,2048);*status|=DEC_Insufficient_storage;
        Din_Go(1407,2048);break;}

      Din_Go(1409,2048);acc=(Unit *)zacc;       /* -> target Unit array  */
      #if DECDPUN==1
      zacc+=zoff;             /* start uLong accumulator to right  */
      #endif

      /* assemble the chunked copies of the left and right sides  */
      Din_Go(1412,2048);for (count=lhs->digits, cup=lhs->lsu, lip=zlhi; count>0; lip++)
        {/*581*/Din_Go(1410,2048);for (p=0, *lip=0; p<FASTDIGS && count>0;
             p+=DECDPUN, cup++, count-=DECDPUN)
          {/*583*/Din_Go(1411,2048);*lip+=*cup*powers[p];/*584*/}/*582*/}
      Din_Go(1413,2048);lmsi=lip-1;     /* save -> msi  */
      Din_Go(1416,2048);for (count=rhs->digits, cup=rhs->lsu, rip=zrhi; count>0; rip++)
        {/*585*/Din_Go(1414,2048);for (p=0, *rip=0; p<FASTDIGS && count>0;
             p+=DECDPUN, cup++, count-=DECDPUN)
          {/*587*/Din_Go(1415,2048);*rip+=*cup*powers[p];/*588*/}/*586*/}
      Din_Go(1417,2048);rmsi=rip-1;     /* save -> msi  */

      /* zero the accumulator  */
      Din_Go(1419,2048);for (lp=zacc; lp<zacc+iacc; lp++) {/*589*/Din_Go(1418,2048);*lp=0;/*590*/}

      /* Start the multiplication */
      /* Resolving carries can dominate the cost of accumulating the  */
      /* partial products, so this is only done when necessary.  */
      /* Each uLong item in the accumulator can hold values up to  */
      /* 2**64-1, and each partial product can be as large as  */
      /* (10**FASTDIGS-1)**2.  When FASTDIGS=9, this can be added to  */
      /* itself 18.4 times in a uLong without overflowing, so during  */
      /* the main calculation resolution is carried out every 18th  */
      /* add -- every 162 digits.  Similarly, when FASTDIGS=8, the  */
      /* partial products can be added to themselves 1844.6 times in  */
      /* a uLong without overflowing, so intermediate carry  */
      /* resolution occurs only every 14752 digits.  Hence for common  */
      /* short numbers usually only the one final carry resolution  */
      /* occurs.  */
      /* (The count is set via FASTLAZY to simplify experiments to  */
      /* measure the value of this approach: a 35% improvement on a  */
      /* [34x34] multiply.)  */
      Din_Go(1420,2048);lazy=FASTLAZY;                         /* carry delay count  */
      Din_Go(1436,2048);for (rip=zrhi; rip<=rmsi; rip++) {     /* over each item in rhs  */
        Din_Go(1421,2048);lp=zacc+(rip-zrhi);                  /* where to add the lhs  */
        Din_Go(1423,2048);for (lip=zlhi; lip<=lmsi; lip++, lp++) { /* over each item in lhs  */
          Din_Go(1422,2048);*lp+=(uLong)(*lip)*(*rip);         /* [this should in-line]  */
          } /* lip loop  */
        Din_Go(1424,2048);lazy--;
        Din_Go(1426,2048);if (lazy>0 && rip!=rmsi) {/*591*/Din_Go(1425,2048);continue;/*592*/}
        Din_Go(1427,2048);lazy=FASTLAZY;                       /* reset delay count  */
        /* spin up the accumulator resolving overflows  */
        Din_Go(1435,2048);for (lp=zacc; lp<zacc+iacc; lp++) {
          Din_Go(1428,2048);if (*lp<FASTBASE) {/*593*/Din_Go(1429,2048);continue;/*594*/}        /* it fits  */
          Din_Go(1430,2048);lcarry=*lp/FASTBASE;               /* top part [slow divide]  */
          /* lcarry can exceed 2**32-1, so check again; this check  */
          /* and occasional extra divide (slow) is well worth it, as  */
          /* it allows FASTLAZY to be increased to 18 rather than 4  */
          /* in the FASTDIGS=9 case  */
          Din_Go(1433,2048);if (lcarry<FASTBASE) {/*595*/Din_Go(1431,2048);carry=(uInt)lcarry;/*596*/}  /* [usual]  */
           else { /* two-place carry [fairly rare]  */
Din_Go(1432,2048);            uInt carry2=(uInt)(lcarry/FASTBASE);    /* top top part  */
            *(lp+2)+=carry2;                        /* add to item+2  */
            *lp-=((uLong)FASTBASE*FASTBASE*carry2); /* [slow]  */
            carry=(uInt)(lcarry-((uLong)FASTBASE*carry2)); /* [inline]  */
            }
          Din_Go(1434,2048);*(lp+1)+=carry;                    /* add to item above [inline]  */
          *lp-=((uLong)FASTBASE*carry);      /* [inline]  */
          } /* carry resolution  */
        } /* rip loop  */

      /* The multiplication is complete; time to convert back into  */
      /* units.  This can be done in-place in the accumulator and in  */
      /* 32-bit operations, because carries were resolved after the  */
      /* final add.  This needs N-1 divides and multiplies for  */
      /* each item in the accumulator (which will become up to N  */
      /* units, where 2<=N<=9).  */
      Din_Go(1441,2048);for (lp=zacc, up=acc; lp<zacc+iacc; lp++) {
Din_Go(1437,2048);        uInt item=(uInt)*lp;                 /* decapitate to uInt  */
        Din_Go(1439,2048);for (p=0; p<FASTDIGS-DECDPUN; p+=DECDPUN, up++) {
Din_Go(1438,2048);          uInt part=item/(DECDPUNMAX+1);
          *up=(Unit)(item-(part*(DECDPUNMAX+1)));
          item=part;
          } /* p  */
        Din_Go(1440,2048);*up=(Unit)item; up++;                /* [final needs no division]  */
        } /* lp  */
      Din_Go(1442,2048);accunits=up-acc;                       /* count of units  */
      }
     else { /* here to use units directly, without chunking ['old code']  */
    #endif

      /* if accumulator will be too long for local storage, then allocate  */
      Din_Go(1443,2048);acc=accbuff;                 /* -> assume buffer for accumulator  */
      needbytes=(D2U(lhs->digits)+D2U(rhs->digits))*sizeof(Unit);
      Din_Go(1449,2048);if (needbytes>(Int)sizeof(accbuff)) {
        Din_Go(1444,2048);allocacc=(Unit *)malloc(needbytes);
        Din_Go(1447,2048);if (allocacc==NULL) {Din_Go(1445,2048);*status|=DEC_Insufficient_storage; Din_Go(1446,2048);break;}
        Din_Go(1448,2048);acc=(Unit *)allocacc;                /* use the allocated space  */
        }

      /* Now the main long multiplication loop */
      /* Unlike the equivalent in the IBM Java implementation, there  */
      /* is no advantage in calculating from msu to lsu.  So, do it  */
      /* by the book, as it were.  */
      /* Each iteration calculates ACC=ACC+MULTAND*MULT  */
      Din_Go(1450,2048);accunits=1;                  /* accumulator starts at '0'  */
      *acc=0;                      /* .. (lsu=0)  */
      shift=0;                     /* no multiplicand shift at first  */
      madlength=D2U(lhs->digits);  /* this won't change  */
      mermsup=rhs->lsu+D2U(rhs->digits); /* -> msu+1 of multiplier  */

      Din_Go(1455,2048);for (mer=rhs->lsu; mer<mermsup; mer++) {
        /* Here, *mer is the next Unit in the multiplier to use  */
        /* If non-zero [optimization] add it...  */
        Din_Go(1451,2048);if (*mer!=0) {/*597*/Din_Go(1452,2048);accunits=decUnitAddSub(&acc[shift], accunits-shift,
                                            lhs->lsu, madlength, 0,
                                            &acc[shift], *mer)
                                            + shift;/*598*/}
         else { /* extend acc with a 0; it will be used shortly  */
          Din_Go(1453,2048);*(acc+accunits)=0;       /* [this avoids length of <=0 later]  */
          accunits++;
          }
        /* multiply multiplicand by 10**DECDPUN for next Unit to left  */
        Din_Go(1454,2048);shift++;                   /* add this for 'logical length'  */
        } /* n  */
    #if FASTMUL
      } /* unchunked units  */
    #endif
    /* common end-path  */
    #if DECTRACE
      decDumpAr('*', acc, accunits);         /* Show exact result  */
    #endif

    /* acc now contains the exact result of the multiplication,  */
    /* possibly with a leading zero unit; build the decNumber from  */
    /* it, noting if any residue  */
    Din_Go(1456,2048);res->bits=bits;                          /* set sign  */
    res->digits=decGetDigits(acc, accunits); /* count digits exactly  */

    /* There can be a 31-bit wrap in calculating the exponent.  */
    /* This can only happen if both input exponents are negative and  */
    /* both their magnitudes are large.  If there was a wrap, set a  */
    /* safe very negative exponent, from which decFinalize() will  */
    /* raise a hard underflow shortly.  */
    exponent=lhs->exponent+rhs->exponent;    /* calculate exponent  */
    Din_Go(1457,2048);if (lhs->exponent<0 && rhs->exponent<0 && exponent>0)
      exponent=-2*DECNUMMAXE;                /* force underflow  */
    Din_Go(1458,2048);res->exponent=exponent;                  /* OK to overwrite now  */


    /* Set the coefficient.  If any rounding, residue records  */
    decSetCoeff(res, set, acc, res->digits, &residue, status);
    decFinish(res, set, &residue, status);   /* final cleanup  */
    } while(0);                         /* end protected  */

  if (allocacc!=NULL) free(allocacc);   /* drop any storage used  */
  #if DECSUBSET
  if (allocrhs!=NULL) free(allocrhs);   /* ..  */
  if (alloclhs!=NULL) free(alloclhs);   /* ..  */
  #endif
  #if FASTMUL
  if (allocrhi!=NULL) free(allocrhi);   /* ..  */
  if (alloclhi!=NULL) free(alloclhi);   /* ..  */
  #endif
  {decNumber * ReplaceReturn44 = res; Din_Go(1460,2048); return ReplaceReturn44;}
  } /* decMultiplyOp  */

/* ------------------------------------------------------------------ */
/* decExpOp -- effect exponentiation                                  */
/*                                                                    */
/*   This computes C = exp(A)                                         */
/*                                                                    */
/*   res is C, the result.  C may be A                                */
/*   rhs is A                                                         */
/*   set is the context; note that rounding mode has no effect        */
/*                                                                    */
/* C must have space for set->digits digits. status is updated but    */
/* not set.                                                           */
/*                                                                    */
/* Restrictions:                                                      */
/*                                                                    */
/*   digits, emax, and -emin in the context must be less than         */
/*   2*DEC_MAX_MATH (1999998), and the rhs must be within these       */
/*   bounds or a zero.  This is an internal routine, so these         */
/*   restrictions are contractual and not enforced.                   */
/*                                                                    */
/* A finite result is rounded using DEC_ROUND_HALF_EVEN; it will      */
/* almost always be correctly rounded, but may be up to 1 ulp in      */
/* error in rare cases.                                               */
/*                                                                    */
/* Finite results will always be full precision and Inexact, except   */
/* when A is a zero or -Infinity (giving 1 or 0 respectively).        */
/* ------------------------------------------------------------------ */
/* This approach used here is similar to the algorithm described in   */
/*                                                                    */
/*   Variable Precision Exponential Function, T. E. Hull and          */
/*   A. Abrham, ACM Transactions on Mathematical Software, Vol 12 #2, */
/*   pp79-91, ACM, June 1986.                                         */
/*                                                                    */
/* with the main difference being that the iterations in the series   */
/* evaluation are terminated dynamically (which does not require the  */
/* extra variable-precision variables which are expensive in this     */
/* context).                                                          */
/*                                                                    */
/* The error analysis in Hull & Abrham's paper applies except for the */
/* round-off error accumulation during the series evaluation.  This   */
/* code does not precalculate the number of iterations and so cannot  */
/* use Horner's scheme.  Instead, the accumulation is done at double- */
/* precision, which ensures that the additions of the terms are exact */
/* and do not accumulate round-off (and any round-off errors in the   */
/* terms themselves move 'to the right' faster than they can          */
/* accumulate).  This code also extends the calculation by allowing,  */
/* in the spirit of other decNumber operators, the input to be more   */
/* precise than the result (the precision used is based on the more   */
/* precise of the input or requested result).                         */
/*                                                                    */
/* Implementation notes:                                              */
/*                                                                    */
/* 1. This is separated out as decExpOp so it can be called from      */
/*    other Mathematical functions (notably Ln) with a wider range    */
/*    than normal.  In particular, it can handle the slightly wider   */
/*    (double) range needed by Ln (which has to be able to calculate  */
/*    exp(-x) where x can be the tiniest number (Ntiny).              */
/*                                                                    */
/* 2. Normalizing x to be <=0.1 (instead of <=1) reduces loop         */
/*    iterations by appoximately a third with additional (although    */
/*    diminishing) returns as the range is reduced to even smaller    */
/*    fractions.  However, h (the power of 10 used to correct the     */
/*    result at the end, see below) must be kept <=8 as otherwise     */
/*    the final result cannot be computed.  Hence the leverage is a   */
/*    sliding value (8-h), where potentially the range is reduced     */
/*    more for smaller values.                                        */
/*                                                                    */
/*    The leverage that can be applied in this way is severely        */
/*    limited by the cost of the raise-to-the power at the end,       */
/*    which dominates when the number of iterations is small (less    */
/*    than ten) or when rhs is short.  As an example, the adjustment  */
/*    x**10,000,000 needs 31 multiplications, all but one full-width. */
/*                                                                    */
/* 3. The restrictions (especially precision) could be raised with    */
/*    care, but the full decNumber range seems very hard within the   */
/*    32-bit limits.                                                  */
/*                                                                    */
/* 4. The working precisions for the static buffers are twice the     */
/*    obvious size to allow for calls from decNumberPower.            */
/* ------------------------------------------------------------------ */
decNumber * decExpOp(decNumber *res, const decNumber *rhs,
                         decContext *set, uInt *status) {
Din_Go(1461,2048);  uInt ignore=0;                   /* working status  */
  Int h;                           /* adjusted exponent for 0.xxxx  */
  Int p;                           /* working precision  */
  Int residue;                     /* rounding residue  */
  uInt needbytes;                  /* for space calculations  */
  const decNumber *x=rhs;          /* (may point to safe copy later)  */
  decContext aset, tset, dset;     /* working contexts  */
  Int comp;                        /* work  */

  /* the argument is often copied to normalize it, so (unusually) it  */
  /* is treated like other buffers, using DECBUFFER, +1 in case  */
  /* DECBUFFER is 0  */
  decNumber bufr[D2N(DECBUFFER*2+1)];
  decNumber *allocrhs=NULL;        /* non-NULL if rhs buffer allocated  */

  /* the working precision will be no more than set->digits+8+1  */
  /* so for on-stack buffers DECBUFFER+9 is used, +1 in case DECBUFFER  */
  /* is 0 (and twice that for the accumulator)  */

  /* buffer for t, term (working precision plus)  */
  decNumber buft[D2N(DECBUFFER*2+9+1)];
  decNumber *allocbuft=NULL;       /* -> allocated buft, iff allocated  */
  decNumber *t=buft;               /* term  */
  /* buffer for a, accumulator (working precision * 2), at least 9  */
  decNumber bufa[D2N(DECBUFFER*4+18+1)];
  decNumber *allocbufa=NULL;       /* -> allocated bufa, iff allocated  */
  decNumber *a=bufa;               /* accumulator  */
  /* decNumber for the divisor term; this needs at most 9 digits  */
  /* and so can be fixed size [16 so can use standard context]  */
  decNumber bufd[D2N(16)];
  decNumber *d=bufd;               /* divisor  */
  decNumber numone;                /* constant 1  */

  #if DECCHECK
  Int iterations=0;                /* for later sanity check  */
  if (decCheckOperands(res, DECUNUSED, rhs, set)) return res;
  #endif

  Din_Go(1536,2048);do {                                  /* protect allocated storage  */
    Din_Go(1462,2048);if (SPECIALARG) {                   /* handle infinities and NaNs  */
      Din_Go(1463,2048);if (decNumberIsInfinite(rhs)) {   /* an infinity  */
        Din_Go(1464,2048);if (decNumberIsNegative(rhs))   /* -Infinity -> +0  */
          uprv_decNumberZero(res);
         else uprv_decNumberCopy(res, rhs);  /* +Infinity -> self  */
        }
       else {/*517*/Din_Go(1465,2048);decNaNs(res, rhs, NULL, set, status);/*518*/} /* a NaN  */
      Din_Go(1466,2048);break;}

    Din_Go(1469,2048);if (ISZERO(rhs)) {                  /* zeros -> exact 1  */
      uprv_decNumberZero(res);               /* make clean 1  */
      Din_Go(1467,2048);*res->lsu=1;                      /* ..  */
      Din_Go(1468,2048);break;}                           /* [no status to set]  */

    /* e**x when 0 < x < 0.66 is < 1+3x/2, hence can fast-path  */
    /* positive and negative tiny cases which will result in inexact  */
    /* 1.  This also allows the later add-accumulate to always be  */
    /* exact (because its length will never be more than twice the  */
    /* working precision).  */
    /* The comparator (tiny) needs just one digit, so use the  */
    /* decNumber d for it (reused as the divisor, etc., below); its  */
    /* exponent is such that if x is positive it will have  */
    /* set->digits-1 zeros between the decimal point and the digit,  */
    /* which is 4, and if x is negative one more zero there as the  */
    /* more precise result will be of the form 0.9999999 rather than  */
    /* 1.0000001.  Hence, tiny will be 0.0000004  if digits=7 and x>0  */
    /* or 0.00000004 if digits=7 and x<0.  If RHS not larger than  */
    /* this then the result will be 1.000000  */
    uprv_decNumberZero(d);                   /* clean  */
    Din_Go(1470,2048);*d->lsu=4;                          /* set 4 ..  */
    d->exponent=-set->digits;           /* * 10**(-d)  */
    Din_Go(1472,2048);if (decNumberIsNegative(rhs)) {/*519*/Din_Go(1471,2048);d->exponent--;/*520*/}  /* negative case  */
    Din_Go(1473,2048);comp=decCompare(d, rhs, 1);         /* signless compare  */
    Din_Go(1476,2048);if (comp==BADINT) {
      Din_Go(1474,2048);*status|=DEC_Insufficient_storage;
      Din_Go(1475,2048);break;}
    Din_Go(1479,2048);if (comp>=0) {                      /* rhs < d  */
Din_Go(1477,2048);      Int shift=set->digits-1;
      uprv_decNumberZero(res);               /* set 1  */
      *res->lsu=1;                      /* ..  */
      res->digits=decShiftToMost(res->lsu, 1, shift);
      res->exponent=-shift;                  /* make 1.0000...  */
      *status|=DEC_Inexact | DEC_Rounded;    /* .. inexactly  */
      Din_Go(1478,2048);break;} /* tiny  */

    /* set up the context to be used for calculating a, as this is  */
    /* used on both paths below  */
    uprv_decContextDefault(&aset, DEC_INIT_DECIMAL64);
    /* accumulator bounds are as requested (could underflow)  */
    Din_Go(1480,2048);aset.emax=set->emax;                /* usual bounds  */
    aset.emin=set->emin;                /* ..  */
    aset.clamp=0;                       /* and no concrete format  */

    /* calculate the adjusted (Hull & Abrham) exponent (where the  */
    /* decimal point is just to the left of the coefficient msd)  */
    h=rhs->exponent+rhs->digits;
    /* if h>8 then 10**h cannot be calculated safely; however, when  */
    /* h=8 then exp(|rhs|) will be at least exp(1E+7) which is at  */
    /* least 6.59E+4342944, so (due to the restriction on Emax/Emin)  */
    /* overflow (or underflow to 0) is guaranteed -- so this case can  */
    /* be handled by simply forcing the appropriate excess  */
    Din_Go(1517,2048);if (h>8) {                          /* overflow/underflow  */
      /* set up here so Power call below will over or underflow to  */
      /* zero; set accumulator to either 2 or 0.02  */
      /* [stack buffer for a is always big enough for this]  */
      uprv_decNumberZero(a);
      Din_Go(1481,2048);*a->lsu=2;                        /* not 1 but < exp(1)  */
      Din_Go(1483,2048);if (decNumberIsNegative(rhs)) {/*521*/Din_Go(1482,2048);a->exponent=-2;/*522*/} /* make 0.02  */
      Din_Go(1484,2048);h=8;                              /* clamp so 10**h computable  */
      p=9;                              /* set a working precision  */
      }
     else {                             /* h<=8  */
Din_Go(1485,2048);      Int maxlever=(rhs->digits>8?1:0);
      /* [could/should increase this for precisions >40 or so, too]  */

      /* if h is 8, cannot normalize to a lower upper limit because  */
      /* the final result will not be computable (see notes above),  */
      /* but leverage can be applied whenever h is less than 8.  */
      /* Apply as much as possible, up to a MAXLEVER digits, which  */
      /* sets the tradeoff against the cost of the later a**(10**h).  */
      /* As h is increased, the working precision below also  */
      /* increases to compensate for the "constant digits at the  */
      /* front" effect.  */
      Int lever=MINI(8-h, maxlever);    /* leverage attainable  */
      Int use=-rhs->digits-lever;       /* exponent to use for RHS  */
      h+=lever;                         /* apply leverage selected  */
      Din_Go(1487,2048);if (h<0) {                        /* clamp  */
        Din_Go(1486,2048);use+=h;                         /* [may end up subnormal]  */
        h=0;
        }
      /* Take a copy of RHS if it needs normalization (true whenever x>=1)  */
      Din_Go(1496,2048);if (rhs->exponent!=use) {
        Din_Go(1488,2048);decNumber *newrhs=bufr;         /* assume will fit on stack  */
        needbytes=sizeof(decNumber)+(D2U(rhs->digits)-1)*sizeof(Unit);
        Din_Go(1494,2048);if (needbytes>sizeof(bufr)) {   /* need malloc space  */
          Din_Go(1489,2048);allocrhs=(decNumber *)malloc(needbytes);
          Din_Go(1492,2048);if (allocrhs==NULL) {         /* hopeless -- abandon  */
            Din_Go(1490,2048);*status|=DEC_Insufficient_storage;
            Din_Go(1491,2048);break;}
          Din_Go(1493,2048);newrhs=allocrhs;              /* use the allocated space  */
          }
        uprv_decNumberCopy(newrhs, rhs);     /* copy to safe space  */
        Din_Go(1495,2048);newrhs->exponent=use;           /* normalize; now <1  */
        x=newrhs;                       /* ready for use  */
        /* decNumberShow(x);  */
        }

      /* Now use the usual power series to evaluate exp(x).  The  */
      /* series starts as 1 + x + x^2/2 ... so prime ready for the  */
      /* third term by setting the term variable t=x, the accumulator  */
      /* a=1, and the divisor d=2.  */

      /* First determine the working precision.  From Hull & Abrham  */
      /* this is set->digits+h+2.  However, if x is 'over-precise' we  */
      /* need to allow for all its digits to potentially participate  */
      /* (consider an x where all the excess digits are 9s) so in  */
      /* this case use x->digits+h+2  */
      Din_Go(1497,2048);p=MAXI(x->digits, set->digits)+h+2;    /* [h<=8]  */

      /* a and t are variable precision, and depend on p, so space  */
      /* must be allocated for them if necessary  */

      /* the accumulator needs to be able to hold 2p digits so that  */
      /* the additions on the second and subsequent iterations are  */
      /* sufficiently exact.  */
      needbytes=sizeof(decNumber)+(D2U(p*2)-1)*sizeof(Unit);
      Din_Go(1503,2048);if (needbytes>sizeof(bufa)) {     /* need malloc space  */
        Din_Go(1498,2048);allocbufa=(decNumber *)malloc(needbytes);
        Din_Go(1501,2048);if (allocbufa==NULL) {          /* hopeless -- abandon  */
          Din_Go(1499,2048);*status|=DEC_Insufficient_storage;
          Din_Go(1500,2048);break;}
        Din_Go(1502,2048);a=allocbufa;                    /* use the allocated space  */
        }
      /* the term needs to be able to hold p digits (which is  */
      /* guaranteed to be larger than x->digits, so the initial copy  */
      /* is safe); it may also be used for the raise-to-power  */
      /* calculation below, which needs an extra two digits  */
      Din_Go(1504,2048);needbytes=sizeof(decNumber)+(D2U(p+2)-1)*sizeof(Unit);
      Din_Go(1510,2048);if (needbytes>sizeof(buft)) {     /* need malloc space  */
        Din_Go(1505,2048);allocbuft=(decNumber *)malloc(needbytes);
        Din_Go(1508,2048);if (allocbuft==NULL) {          /* hopeless -- abandon  */
          Din_Go(1506,2048);*status|=DEC_Insufficient_storage;
          Din_Go(1507,2048);break;}
        Din_Go(1509,2048);t=allocbuft;                    /* use the allocated space  */
        }

      uprv_decNumberCopy(t, x);              /* term=x  */
      uprv_decNumberZero(a); Din_Go(1511,2048);*a->lsu=1;      /* accumulator=1  */
      uprv_decNumberZero(d); *d->lsu=2;      /* divisor=2  */
      uprv_decNumberZero(&numone); *numone.lsu=1; /* constant 1 for increment  */

      /* set up the contexts for calculating a, t, and d  */
      uprv_decContextDefault(&tset, DEC_INIT_DECIMAL64);
      dset=tset;
      /* accumulator bounds are set above, set precision now  */
      aset.digits=p*2;                  /* double  */
      /* term bounds avoid any underflow or overflow  */
      tset.digits=p;
      tset.emin=DEC_MIN_EMIN;           /* [emax is plenty]  */
      /* [dset.digits=16, etc., are sufficient]  */

      /* finally ready to roll  */
      Din_Go(1516,2048);for (;;) {
        #if DECCHECK
        iterations++;
        #endif
        /* only the status from the accumulation is interesting  */
        /* [but it should remain unchanged after first add]  */
        Din_Go(1512,2048);decAddOp(a, a, t, &aset, 0, status);           /* a=a+t  */
        decMultiplyOp(t, t, x, &tset, &ignore);        /* t=t*x  */
        decDivideOp(t, t, d, &tset, DIVIDE, &ignore);  /* t=t/d  */
        /* the iteration ends when the term cannot affect the result,  */
        /* if rounded to p digits, which is when its value is smaller  */
        /* than the accumulator by p+1 digits.  There must also be  */
        /* full precision in a.  */
        Din_Go(1514,2048);if (((a->digits+a->exponent)>=(t->digits+t->exponent+p+1))
            && (a->digits>=p)) {/*523*/Din_Go(1513,2048);break;/*524*/}
        Din_Go(1515,2048);decAddOp(d, d, &numone, &dset, 0, &ignore);    /* d=d+1  */
        } /* iterate  */

      #if DECCHECK
      /* just a sanity check; comment out test to show always  */
      if (iterations>p+3)
        printf("Exp iterations=%ld, status=%08lx, p=%ld, d=%ld\n",
               (LI)iterations, (LI)*status, (LI)p, (LI)x->digits);
      #endif
      } /* h<=8  */

    /* apply postconditioning: a=a**(10**h) -- this is calculated  */
    /* at a slightly higher precision than Hull & Abrham suggest  */
    Din_Go(1531,2048);if (h>0) {
      Int seenbit=0;               /* set once a 1-bit is seen  */
      Int i;                       /* counter  */
      Int n=powers[h];             /* always positive  */
      aset.digits=p+2;             /* sufficient precision  */
      /* avoid the overhead and many extra digits of decNumberPower  */
      /* as all that is needed is the short 'multipliers' loop; here  */
      /* accumulate the answer into t  */
      uprv_decNumberZero(t); *t->lsu=1; /* acc=1  */
      Din_Go(1529,2048);for (i=1;;i++){              /* for each bit [top bit ignored]  */
        /* abandon if have had overflow or terminal underflow  */
        Din_Go(1518,2048);if (*status & (DEC_Overflow|DEC_Underflow)) { /* interesting?  */
          Din_Go(1519,2048);if (*status&DEC_Overflow || ISZERO(t)) {/*525*/Din_Go(1520,2048);break;/*526*/}}
        Din_Go(1521,2048);n=n<<1;                    /* move next bit to testable position  */
        Din_Go(1523,2048);if (n<0) {                 /* top bit is set  */
          Din_Go(1522,2048);seenbit=1;               /* OK, have a significant bit  */
          decMultiplyOp(t, t, a, &aset, status); /* acc=acc*x  */
          }
        Din_Go(1525,2048);if (i==31) {/*527*/Din_Go(1524,2048);break;/*528*/}          /* that was the last bit  */
        Din_Go(1527,2048);if (!seenbit) {/*529*/Din_Go(1526,2048);continue;/*530*/}    /* no need to square 1  */
        Din_Go(1528,2048);decMultiplyOp(t, t, t, &aset, status); /* acc=acc*acc [square]  */
        } /*i*/ /* 32 bits  */
      /* decNumberShow(t);  */
      Din_Go(1530,2048);a=t;                         /* and carry on using t instead of a  */
      }

    /* Copy and round the result to res  */
    Din_Go(1532,2048);residue=1;                          /* indicate dirt to right ..  */
    Din_Go(1534,2048);if (ISZERO(a)) {/*531*/Din_Go(1533,2048);residue=0;/*532*/}           /* .. unless underflowed to 0  */
    Din_Go(1535,2048);aset.digits=set->digits;            /* [use default rounding]  */
    decCopyFit(res, a, &aset, &residue, status); /* copy & shorten  */
    decFinish(res, set, &residue, status);       /* cleanup/set flags  */
    } while(0);                         /* end protected  */

  if (allocrhs !=NULL) free(allocrhs);  /* drop any storage used  */
  if (allocbufa!=NULL) free(allocbufa); /* ..  */
  if (allocbuft!=NULL) free(allocbuft); /* ..  */
  /* [status is handled by caller]  */
  {decNumber * ReplaceReturn43 = res; Din_Go(1537,2048); return ReplaceReturn43;}
  } /* decExpOp  */

/* ------------------------------------------------------------------ */
/* Initial-estimate natural logarithm table                           */
/*                                                                    */
/*   LNnn -- 90-entry 16-bit table for values from .10 through .99.   */
/*           The result is a 4-digit encode of the coefficient (c=the */
/*           top 14 bits encoding 0-9999) and a 2-digit encode of the */
/*           exponent (e=the bottom 2 bits encoding 0-3)              */
/*                                                                    */
/*           The resulting value is given by:                         */
/*                                                                    */
/*             v = -c * 10**(-e-3)                                    */
/*                                                                    */
/*           where e and c are extracted from entry k = LNnn[x-10]    */
/*           where x is truncated (NB) into the range 10 through 99,  */
/*           and then c = k>>2 and e = k&3.                           */
/* ------------------------------------------------------------------ */
static const uShort LNnn[90]={9016,  8652,  8316,  8008,  7724,  7456,  7208,
  6972,  6748,  6540,  6340,  6148,  5968,  5792,  5628,  5464,  5312,
  5164,  5020,  4884,  4748,  4620,  4496,  4376,  4256,  4144,  4032,
 39233, 38181, 37157, 36157, 35181, 34229, 33297, 32389, 31501, 30629,
 29777, 28945, 28129, 27329, 26545, 25777, 25021, 24281, 23553, 22837,
 22137, 21445, 20769, 20101, 19445, 18801, 18165, 17541, 16925, 16321,
 15721, 15133, 14553, 13985, 13421, 12865, 12317, 11777, 11241, 10717,
 10197,  9685,  9177,  8677,  8185,  7697,  7213,  6737,  6269,  5801,
  5341,  4889,  4437, 39930, 35534, 31186, 26886, 22630, 18418, 14254,
 10130,  6046, 20055};

/* ------------------------------------------------------------------ */
/* decLnOp -- effect natural logarithm                                */
/*                                                                    */
/*   This computes C = ln(A)                                          */
/*                                                                    */
/*   res is C, the result.  C may be A                                */
/*   rhs is A                                                         */
/*   set is the context; note that rounding mode has no effect        */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/*                                                                    */
/* Notable cases:                                                     */
/*   A<0 -> Invalid                                                   */
/*   A=0 -> -Infinity (Exact)                                         */
/*   A=+Infinity -> +Infinity (Exact)                                 */
/*   A=1 exactly -> 0 (Exact)                                         */
/*                                                                    */
/* Restrictions (as for Exp):                                         */
/*                                                                    */
/*   digits, emax, and -emin in the context must be less than         */
/*   DEC_MAX_MATH+11 (1000010), and the rhs must be within these      */
/*   bounds or a zero.  This is an internal routine, so these         */
/*   restrictions are contractual and not enforced.                   */
/*                                                                    */
/* A finite result is rounded using DEC_ROUND_HALF_EVEN; it will      */
/* almost always be correctly rounded, but may be up to 1 ulp in      */
/* error in rare cases.                                               */
/* ------------------------------------------------------------------ */
/* The result is calculated using Newton's method, with each          */
/* iteration calculating a' = a + x * exp(-a) - 1.  See, for example, */
/* Epperson 1989.                                                     */
/*                                                                    */
/* The iteration ends when the adjustment x*exp(-a)-1 is tiny enough. */
/* This has to be calculated at the sum of the precision of x and the */
/* working precision.                                                 */
/*                                                                    */
/* Implementation notes:                                              */
/*                                                                    */
/* 1. This is separated out as decLnOp so it can be called from       */
/*    other Mathematical functions (e.g., Log 10) with a wider range  */
/*    than normal.  In particular, it can handle the slightly wider   */
/*    (+9+2) range needed by a power function.                        */
/*                                                                    */
/* 2. The speed of this function is about 10x slower than exp, as     */
/*    it typically needs 4-6 iterations for short numbers, and the    */
/*    extra precision needed adds a squaring effect, twice.           */
/*                                                                    */
/* 3. Fastpaths are included for ln(10) and ln(2), up to length 40,   */
/*    as these are common requests.  ln(10) is used by log10(x).      */
/*                                                                    */
/* 4. An iteration might be saved by widening the LNnn table, and     */
/*    would certainly save at least one if it were made ten times     */
/*    bigger, too (for truncated fractions 0.100 through 0.999).      */
/*    However, for most practical evaluations, at least four or five  */
/*    iterations will be neede -- so this would only speed up by      */
/*    20-25% and that probably does not justify increasing the table  */
/*    size.                                                           */
/*                                                                    */
/* 5. The static buffers are larger than might be expected to allow   */
/*    for calls from decNumberPower.                                  */
/* ------------------------------------------------------------------ */
#if defined(__clang__) || U_GCC_MAJOR_MINOR >= 406
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Warray-bounds"
#endif
decNumber * decLnOp(decNumber *res, const decNumber *rhs,
                    decContext *set, uInt *status) {
  uInt ignore=0;                   /* working status accumulator  */
  uInt needbytes;                  /* for space calculations  */
  Int residue;                     /* rounding residue  */
  Int r;                           /* rhs=f*10**r [see below]  */
  Int p;                           /* working precision  */
  Int pp;                          /* precision for iteration  */
  Int t;                           /* work  */

  /* buffers for a (accumulator, typically precision+2) and b  */
  /* (adjustment calculator, same size)  */
  decNumber bufa[D2N(DECBUFFER+12)];
  decNumber *allocbufa=NULL;       /* -> allocated bufa, iff allocated  */
  decNumber *a=bufa;               /* accumulator/work  */
  decNumber bufb[D2N(DECBUFFER*2+2)];
  decNumber *allocbufb=NULL;       /* -> allocated bufa, iff allocated  */
  decNumber *b=bufb;               /* adjustment/work  */

  decNumber  numone;               /* constant 1  */
  decNumber  cmp;                  /* work  */
  decContext aset, bset;           /* working contexts  */

  #if DECCHECK
  Int iterations=0;                /* for later sanity check  */
  if (decCheckOperands(res, DECUNUSED, rhs, set)) return res;
  #endif

  Din_Go(1597,2048);do {                                  /* protect allocated storage  */
    Din_Go(1538,2048);if (SPECIALARG) {                   /* handle infinities and NaNs  */
      Din_Go(1539,2048);if (decNumberIsInfinite(rhs)) {   /* an infinity  */
        Din_Go(1540,2048);if (decNumberIsNegative(rhs))   /* -Infinity -> error  */
          *status|=DEC_Invalid_operation;
         else uprv_decNumberCopy(res, rhs);  /* +Infinity -> self  */
        }
       else {/*565*/Din_Go(1541,2048);decNaNs(res, rhs, NULL, set, status);/*566*/} /* a NaN  */
      Din_Go(1542,2048);break;}

    Din_Go(1545,2048);if (ISZERO(rhs)) {                  /* +/- zeros -> -Infinity  */
      uprv_decNumberZero(res);               /* make clean  */
      Din_Go(1543,2048);res->bits=DECINF|DECNEG;          /* set - infinity  */
      Din_Go(1544,2048);break;}                           /* [no status to set]  */

    /* Non-zero negatives are bad...  */
    Din_Go(1548,2048);if (decNumberIsNegative(rhs)) {     /* -x -> error  */
      Din_Go(1546,2048);*status|=DEC_Invalid_operation;
      Din_Go(1547,2048);break;}

    /* Here, rhs is positive, finite, and in range  */

    /* lookaside fastpath code for ln(2) and ln(10) at common lengths  */
    Din_Go(1555,2048);if (rhs->exponent==0 && set->digits<=40) {
      #if DECDPUN==1
      Din_Go(1549,2048);if (rhs->lsu[0]==0 && rhs->lsu[1]==1 && rhs->digits==2) { /* ln(10)  */
      #else
      if (rhs->lsu[0]==10 && rhs->digits==2) {                  /* ln(10)  */
      #endif
        Din_Go(1550,2048);aset=*set; aset.round=DEC_ROUND_HALF_EVEN;
        #define LN10 "2.302585092994045684017991454684364207601"
        uprv_decNumberFromString(res, LN10, &aset);
        *status|=(DEC_Inexact | DEC_Rounded); /* is inexact  */
        Din_Go(1551,2048);break;}
      Din_Go(1554,2048);if (rhs->lsu[0]==2 && rhs->digits==1) { /* ln(2)  */
        Din_Go(1552,2048);aset=*set; aset.round=DEC_ROUND_HALF_EVEN;
        #define LN2 "0.6931471805599453094172321214581765680755"
        uprv_decNumberFromString(res, LN2, &aset);
        *status|=(DEC_Inexact | DEC_Rounded);
        Din_Go(1553,2048);break;}
      } /* integer and short  */

    /* Determine the working precision.  This is normally the  */
    /* requested precision + 2, with a minimum of 9.  However, if  */
    /* the rhs is 'over-precise' then allow for all its digits to  */
    /* potentially participate (consider an rhs where all the excess  */
    /* digits are 9s) so in this case use rhs->digits+2.  */
    Din_Go(1556,2048);p=MAXI(rhs->digits, MAXI(set->digits, 7))+2;

    /* Allocate space for the accumulator and the high-precision  */
    /* adjustment calculator, if necessary.  The accumulator must  */
    /* be able to hold p digits, and the adjustment up to  */
    /* rhs->digits+p digits.  They are also made big enough for 16  */
    /* digits so that they can be used for calculating the initial  */
    /* estimate.  */
    needbytes=sizeof(decNumber)+(D2U(MAXI(p,16))-1)*sizeof(Unit);
    Din_Go(1562,2048);if (needbytes>sizeof(bufa)) {     /* need malloc space  */
      Din_Go(1557,2048);allocbufa=(decNumber *)malloc(needbytes);
      Din_Go(1560,2048);if (allocbufa==NULL) {          /* hopeless -- abandon  */
        Din_Go(1558,2048);*status|=DEC_Insufficient_storage;
        Din_Go(1559,2048);break;}
      Din_Go(1561,2048);a=allocbufa;                    /* use the allocated space  */
      }
    Din_Go(1563,2048);pp=p+rhs->digits;
    needbytes=sizeof(decNumber)+(D2U(MAXI(pp,16))-1)*sizeof(Unit);
    Din_Go(1569,2048);if (needbytes>sizeof(bufb)) {     /* need malloc space  */
      Din_Go(1564,2048);allocbufb=(decNumber *)malloc(needbytes);
      Din_Go(1567,2048);if (allocbufb==NULL) {          /* hopeless -- abandon  */
        Din_Go(1565,2048);*status|=DEC_Insufficient_storage;
        Din_Go(1566,2048);break;}
      Din_Go(1568,2048);b=allocbufb;                    /* use the allocated space  */
      }

    /* Prepare an initial estimate in acc. Calculate this by  */
    /* considering the coefficient of x to be a normalized fraction,  */
    /* f, with the decimal point at far left and multiplied by  */
    /* 10**r.  Then, rhs=f*10**r and 0.1<=f<1, and  */
    /*   ln(x) = ln(f) + ln(10)*r  */
    /* Get the initial estimate for ln(f) from a small lookup  */
    /* table (see above) indexed by the first two digits of f,  */
    /* truncated.  */

    uprv_decContextDefault(&aset, DEC_INIT_DECIMAL64); /* 16-digit extended  */
    Din_Go(1570,2048);r=rhs->exponent+rhs->digits;        /* 'normalised' exponent  */
    uprv_decNumberFromInt32(a, r);           /* a=r  */
    uprv_decNumberFromInt32(b, 2302585);     /* b=ln(10) (2.302585)  */
    b->exponent=-6;                     /*  ..  */
    decMultiplyOp(a, a, b, &aset, &ignore);  /* a=a*b  */
    /* now get top two digits of rhs into b by simple truncate and  */
    /* force to integer  */
    residue=0;                          /* (no residue)  */
    aset.digits=2; aset.round=DEC_ROUND_DOWN;
    decCopyFit(b, rhs, &aset, &residue, &ignore); /* copy & shorten  */
    b->exponent=0;                      /* make integer  */
    t=decGetInt(b);                     /* [cannot fail]  */
    Din_Go(1571,2048);if (t<10) t=X10(t);                 /* adjust single-digit b  */
    Din_Go(1572,2048);t=LNnn[t-10];                       /* look up ln(b)  */
    uprv_decNumberFromInt32(b, t>>2);        /* b=ln(b) coefficient  */
    b->exponent=-(t&3)-3;               /* set exponent  */
    b->bits=DECNEG;                     /* ln(0.10)->ln(0.99) always -ve  */
    aset.digits=16; aset.round=DEC_ROUND_HALF_EVEN; /* restore  */
    decAddOp(a, a, b, &aset, 0, &ignore); /* acc=a+b  */
    /* the initial estimate is now in a, with up to 4 digits correct.  */
    /* When rhs is at or near Nmax the estimate will be low, so we  */
    /* will approach it from below, avoiding overflow when calling exp.  */

    uprv_decNumberZero(&numone); *numone.lsu=1;   /* constant 1 for adjustment  */

    /* accumulator bounds are as requested (could underflow, but  */
    /* cannot overflow)  */
    aset.emax=set->emax;
    aset.emin=set->emin;
    aset.clamp=0;                       /* no concrete format  */
    /* set up a context to be used for the multiply and subtract  */
    bset=aset;
    bset.emax=DEC_MAX_MATH*2;           /* use double bounds for the  */
    bset.emin=-DEC_MAX_MATH*2;          /* adjustment calculation  */
                                        /* [see decExpOp call below]  */
    /* for each iteration double the number of digits to calculate,  */
    /* up to a maximum of p  */
    pp=9;                               /* initial precision  */
    /* [initially 9 as then the sequence starts 7+2, 16+2, and  */
    /* 34+2, which is ideal for standard-sized numbers]  */
    aset.digits=pp;                     /* working context  */
    bset.digits=pp+rhs->digits;         /* wider context  */
    Din_Go(1592,2048);for (;;) {                          /* iterate  */
      #if DECCHECK
      iterations++;
      if (iterations>24) break;         /* consider 9 * 2**24  */
      #endif
      /* calculate the adjustment (exp(-a)*x-1) into b.  This is a  */
      /* catastrophic subtraction but it really is the difference  */
      /* from 1 that is of interest.  */
      /* Use the internal entry point to Exp as it allows the double  */
      /* range for calculating exp(-a) when a is the tiniest subnormal.  */
      Din_Go(1573,2048);a->bits^=DECNEG;                  /* make -a  */
      decExpOp(b, a, &bset, &ignore);   /* b=exp(-a)  */
      a->bits^=DECNEG;                  /* restore sign of a  */
      /* now multiply by rhs and subtract 1, at the wider precision  */
      decMultiplyOp(b, b, rhs, &bset, &ignore);        /* b=b*rhs  */
      decAddOp(b, b, &numone, &bset, DECNEG, &ignore); /* b=b-1  */

      /* the iteration ends when the adjustment cannot affect the  */
      /* result by >=0.5 ulp (at the requested digits), which  */
      /* is when its value is smaller than the accumulator by  */
      /* set->digits+1 digits (or it is zero) -- this is a looser  */
      /* requirement than for Exp because all that happens to the  */
      /* accumulator after this is the final rounding (but note that  */
      /* there must also be full precision in a, or a=0).  */

      Din_Go(1584,2048);if (decNumberIsZero(b) ||
          (a->digits+a->exponent)>=(b->digits+b->exponent+set->digits+1)) {
        Din_Go(1574,2048);if (a->digits==p) {/*567*/Din_Go(1575,2048);break;/*568*/}
        Din_Go(1581,2048);if (decNumberIsZero(a)) {
          Din_Go(1576,2048);decCompareOp(&cmp, rhs, &numone, &aset, COMPARE, &ignore); /* rhs=1 ?  */
          Din_Go(1579,2048);if (cmp.lsu[0]==0) {/*569*/Din_Go(1577,2048);a->exponent=0;/*570*/}            /* yes, exact 0  */
           else {/*571*/Din_Go(1578,2048);*status|=(DEC_Inexact | DEC_Rounded);/*572*/}  /* no, inexact  */
          Din_Go(1580,2048);break;
          }
        /* force padding if adjustment has gone to 0 before full length  */
        Din_Go(1583,2048);if (decNumberIsZero(b)) {/*573*/Din_Go(1582,2048);b->exponent=a->exponent-p;/*574*/}
        }

      /* not done yet ...  */
      Din_Go(1585,2048);decAddOp(a, a, b, &aset, 0, &ignore);  /* a=a+b for next estimate  */
      Din_Go(1587,2048);if (pp==p) {/*575*/Din_Go(1586,2048);continue;/*576*/}                   /* precision is at maximum  */
      /* lengthen the next calculation  */
      Din_Go(1588,2048);pp=pp*2;                               /* double precision  */
      Din_Go(1590,2048);if (pp>p) {/*577*/Din_Go(1589,2048);pp=p;/*578*/}                        /* clamp to maximum  */
      Din_Go(1591,2048);aset.digits=pp;                        /* working context  */
      bset.digits=pp+rhs->digits;            /* wider context  */
      } /* Newton's iteration  */

    #if DECCHECK
    /* just a sanity check; remove the test to show always  */
    if (iterations>24)
      printf("Ln iterations=%ld, status=%08lx, p=%ld, d=%ld\n",
            (LI)iterations, (LI)*status, (LI)p, (LI)rhs->digits);
    #endif

    /* Copy and round the result to res  */
    Din_Go(1593,2048);residue=1;                          /* indicate dirt to right  */
    Din_Go(1595,2048);if (ISZERO(a)) {/*579*/Din_Go(1594,2048);residue=0;/*580*/}           /* .. unless underflowed to 0  */
    Din_Go(1596,2048);aset.digits=set->digits;            /* [use default rounding]  */
    decCopyFit(res, a, &aset, &residue, status); /* copy & shorten  */
    decFinish(res, set, &residue, status);       /* cleanup/set flags  */
    } while(0);                         /* end protected  */

  if (allocbufa!=NULL) free(allocbufa); /* drop any storage used  */
  if (allocbufb!=NULL) free(allocbufb); /* ..  */
  /* [status is handled by caller]  */
  {decNumber * ReplaceReturn42 = res; Din_Go(1598,2048); return ReplaceReturn42;}
  } /* decLnOp  */
#if defined(__clang__) || U_GCC_MAJOR_MINOR >= 406
#pragma GCC diagnostic pop
#endif

/* ------------------------------------------------------------------ */
/* decQuantizeOp  -- force exponent to requested value                */
/*                                                                    */
/*   This computes C = op(A, B), where op adjusts the coefficient     */
/*   of C (by rounding or shifting) such that the exponent (-scale)   */
/*   of C has the value B or matches the exponent of B.               */
/*   The numerical value of C will equal A, except for the effects of */
/*   any rounding that occurred.                                      */
/*                                                                    */
/*   res is C, the result.  C may be A or B                           */
/*   lhs is A, the number to adjust                                   */
/*   rhs is B, the requested exponent                                 */
/*   set is the context                                               */
/*   quant is 1 for quantize or 0 for rescale                         */
/*   status is the status accumulator (this can be called without     */
/*          risk of control loss)                                     */
/*                                                                    */
/* C must have space for set->digits digits.                          */
/*                                                                    */
/* Unless there is an error or the result is infinite, the exponent   */
/* after the operation is guaranteed to be that requested.            */
/* ------------------------------------------------------------------ */
static decNumber * decQuantizeOp(decNumber *res, const decNumber *lhs,
                                 const decNumber *rhs, decContext *set,
                                 Flag quant, uInt *status) {
  #if DECSUBSET
  decNumber *alloclhs=NULL;        /* non-NULL if rounded lhs allocated  */
  decNumber *allocrhs=NULL;        /* .., rhs  */
  #endif
  Din_Go(1599,2048);const decNumber *inrhs=rhs;      /* save original rhs  */
  Int   reqdigits=set->digits;     /* requested DIGITS  */
  Int   reqexp;                    /* requested exponent [-scale]  */
  Int   residue=0;                 /* rounding residue  */
  Int   etiny=set->emin-(reqdigits-1);

  #if DECCHECK
  if (decCheckOperands(res, lhs, rhs, set)) return res;
  #endif

  Din_Go(1630,2048);do {                             /* protect allocated storage  */
    #if DECSUBSET
    if (!set->extended) {
      /* reduce operands and set lostDigits status, as needed  */
      if (lhs->digits>reqdigits) {
        alloclhs=decRoundOperand(lhs, set, status);
        if (alloclhs==NULL) break;
        lhs=alloclhs;
        }
      if (rhs->digits>reqdigits) { /* [this only checks lostDigits]  */
        allocrhs=decRoundOperand(rhs, set, status);
        if (allocrhs==NULL) break;
        rhs=allocrhs;
        }
      }
    #endif
    /* [following code does not require input rounding]  */

    /* Handle special values  */
    Din_Go(1600,2048);if (SPECIALARGS) {
      /* NaNs get usual processing  */
      Din_Go(1601,2048);if (SPECIALARGS & (DECSNAN | DECNAN))
        {/*617*/Din_Go(1602,2048);decNaNs(res, lhs, rhs, set, status);/*618*/}
      /* one infinity but not both is bad  */
      else {/*619*/Din_Go(1603,2048);if ((lhs->bits ^ rhs->bits) & DECINF)
        *status|=DEC_Invalid_operation;
      /* both infinity: return lhs  */
      else uprv_decNumberCopy(res, lhs);/*620*/}          /* [nop if in place]  */
      Din_Go(1604,2048);break;
      }

    /* set requested exponent  */
    Din_Go(1607,2048);if (quant) {/*621*/Din_Go(1605,2048);reqexp=inrhs->exponent;/*622*/}  /* quantize -- match exponents  */
     else {                             /* rescale -- use value of rhs  */
      /* Original rhs must be an integer that fits and is in range,  */
      /* which could be from -1999999997 to +999999999, thanks to  */
      /* subnormals  */
      Din_Go(1606,2048);reqexp=decGetInt(inrhs);               /* [cannot fail]  */
      }

    #if DECSUBSET
    if (!set->extended) etiny=set->emin;     /* no subnormals  */
    #endif

    Din_Go(1610,2048);if (reqexp==BADINT                       /* bad (rescale only) or ..  */
     || reqexp==BIGODD || reqexp==BIGEVEN    /* very big (ditto) or ..  */
     || (reqexp<etiny)                       /* < lowest  */
     || (reqexp>set->emax)) {                /* > emax  */
      Din_Go(1608,2048);*status|=DEC_Invalid_operation;
      Din_Go(1609,2048);break;}

    /* the RHS has been processed, so it can be overwritten now if necessary  */
    Din_Go(1625,2048);if (ISZERO(lhs)) {                       /* zero coefficient unchanged  */
      uprv_decNumberCopy(res, lhs);               /* [nop if in place]  */
      Din_Go(1611,2048);res->exponent=reqexp;                  /* .. just set exponent  */
      #if DECSUBSET
      if (!set->extended) res->bits=0;       /* subset specification; no -0  */
      #endif
      }
     else {                                  /* non-zero lhs  */
Din_Go(1612,2048);      Int adjust=reqexp-lhs->exponent;       /* digit adjustment needed  */
      /* if adjusted coefficient will definitely not fit, give up now  */
      Din_Go(1615,2048);if ((lhs->digits-adjust)>reqdigits) {
        Din_Go(1613,2048);*status|=DEC_Invalid_operation;
        Din_Go(1614,2048);break;
        }

      Din_Go(1624,2048);if (adjust>0) {                        /* increasing exponent  */
        /* this will decrease the length of the coefficient by adjust  */
        /* digits, and must round as it does so  */
        Din_Go(1616,2048);decContext workset;                  /* work  */
        workset=*set;                        /* clone rounding, etc.  */
        workset.digits=lhs->digits-adjust;   /* set requested length  */
        /* [note that the latter can be <1, here]  */
        decCopyFit(res, lhs, &workset, &residue, status); /* fit to result  */
        decApplyRound(res, &workset, residue, status);    /* .. and round  */
        residue=0;                                        /* [used]  */
        /* If just rounded a 999s case, exponent will be off by one;  */
        /* adjust back (after checking space), if so.  */
        Din_Go(1621,2048);if (res->exponent>reqexp) {
          /* re-check needed, e.g., for quantize(0.9999, 0.001) under  */
          /* set->digits==3  */
          Din_Go(1617,2048);if (res->digits==reqdigits) {      /* cannot shift by 1  */
            Din_Go(1618,2048);*status&=~(DEC_Inexact | DEC_Rounded); /* [clean these]  */
            *status|=DEC_Invalid_operation;
            Din_Go(1619,2048);break;
            }
          Din_Go(1620,2048);res->digits=decShiftToMost(res->lsu, res->digits, 1); /* shift  */
          res->exponent--;                   /* (re)adjust the exponent.  */
          }
        #if DECSUBSET
        if (ISZERO(res) && !set->extended) res->bits=0; /* subset; no -0  */
        #endif
        } /* increase  */
       else /* adjust<=0 */ {                /* decreasing or = exponent  */
        /* this will increase the length of the coefficient by -adjust  */
        /* digits, by adding zero or more trailing zeros; this is  */
        /* already checked for fit, above  */
        uprv_decNumberCopy(res, lhs);             /* [it will fit]  */
        /* if padding needed (adjust<0), add it now...  */
        Din_Go(1623,2048);if (adjust<0) {
          Din_Go(1622,2048);res->digits=decShiftToMost(res->lsu, res->digits, -adjust);
          res->exponent+=adjust;             /* adjust the exponent  */
          }
        } /* decrease  */
      } /* non-zero  */

    /* Check for overflow [do not use Finalize in this case, as an  */
    /* overflow here is a "don't fit" situation]  */
    Din_Go(1629,2048);if (res->exponent>set->emax-res->digits+1) {  /* too big  */
      Din_Go(1626,2048);*status|=DEC_Invalid_operation;
      Din_Go(1627,2048);break;
      }
     else {
      Din_Go(1628,2048);decFinalize(res, set, &residue, status);    /* set subnormal flags  */
      *status&=~DEC_Underflow;          /* suppress Underflow [as per 754]  */
      }
    } while(0);                         /* end protected  */

  #if DECSUBSET
  if (allocrhs!=NULL) free(allocrhs);   /* drop any storage used  */
  if (alloclhs!=NULL) free(alloclhs);   /* ..  */
  #endif
  {decNumber * ReplaceReturn41 = res; Din_Go(1631,2048); return ReplaceReturn41;}
  } /* decQuantizeOp  */

/* ------------------------------------------------------------------ */
/* decCompareOp -- compare, min, or max two Numbers                   */
/*                                                                    */
/*   This computes C = A ? B and carries out one of four operations:  */
/*     COMPARE    -- returns the signum (as a number) giving the      */
/*                   result of a comparison unless one or both        */
/*                   operands is a NaN (in which case a NaN results)  */
/*     COMPSIG    -- as COMPARE except that a quiet NaN raises        */
/*                   Invalid operation.                               */
/*     COMPMAX    -- returns the larger of the operands, using the    */
/*                   754 maxnum operation                             */
/*     COMPMAXMAG -- ditto, comparing absolute values                 */
/*     COMPMIN    -- the 754 minnum operation                         */
/*     COMPMINMAG -- ditto, comparing absolute values                 */
/*     COMTOTAL   -- returns the signum (as a number) giving the      */
/*                   result of a comparison using 754 total ordering  */
/*                                                                    */
/*   res is C, the result.  C may be A and/or B (e.g., X=X?X)         */
/*   lhs is A                                                         */
/*   rhs is B                                                         */
/*   set is the context                                               */
/*   op  is the operation flag                                        */
/*   status is the usual accumulator                                  */
/*                                                                    */
/* C must have space for one digit for COMPARE or set->digits for     */
/* COMPMAX, COMPMIN, COMPMAXMAG, or COMPMINMAG.                       */
/* ------------------------------------------------------------------ */
/* The emphasis here is on speed for common cases, and avoiding       */
/* coefficient comparison if possible.                                */
/* ------------------------------------------------------------------ */
static decNumber * decCompareOp(decNumber *res, const decNumber *lhs,
                         const decNumber *rhs, decContext *set,
                         Flag op, uInt *status) {
Din_Go(1632,2048);  #if DECSUBSET
  decNumber *alloclhs=NULL;        /* non-NULL if rounded lhs allocated  */
  decNumber *allocrhs=NULL;        /* .., rhs  */
  #endif
  Int   result=0;                  /* default result value  */
  uByte merged;                    /* work  */

  #if DECCHECK
  if (decCheckOperands(res, lhs, rhs, set)) return res;
  #endif

  Din_Go(1670,2048);do {                             /* protect allocated storage  */
    #if DECSUBSET
    if (!set->extended) {
      /* reduce operands and set lostDigits status, as needed  */
      if (lhs->digits>set->digits) {
        alloclhs=decRoundOperand(lhs, set, status);
        if (alloclhs==NULL) {result=BADINT; break;}
        lhs=alloclhs;
        }
      if (rhs->digits>set->digits) {
        allocrhs=decRoundOperand(rhs, set, status);
        if (allocrhs==NULL) {result=BADINT; break;}
        rhs=allocrhs;
        }
      }
    #endif
    /* [following code does not require input rounding]  */

    /* If total ordering then handle differing signs 'up front'  */
    Din_Go(1633,2048);if (op==COMPTOTAL) {                /* total ordering  */
      Din_Go(1634,2048);if (decNumberIsNegative(lhs) && !decNumberIsNegative(rhs)) {
        Din_Go(1635,2048);result=-1;
        Din_Go(1636,2048);break;
        }
      Din_Go(1639,2048);if (!decNumberIsNegative(lhs) && decNumberIsNegative(rhs)) {
        Din_Go(1637,2048);result=+1;
        Din_Go(1638,2048);break;
        }
      }

    /* handle NaNs specially; let infinities drop through  */
    /* This assumes sNaN (even just one) leads to NaN.  */
    Din_Go(1640,2048);merged=(lhs->bits | rhs->bits) & (DECSNAN | DECNAN);
    Din_Go(1666,2048);if (merged) {                       /* a NaN bit set  */
      Din_Go(1641,2048);if (op==COMPARE);                 /* result will be NaN  */
       else {/*389*/Din_Go(1642,2048);if (op==COMPSIG)            /* treat qNaN as sNaN  */
        {/*391*/Din_Go(1643,2048);*status|=DEC_Invalid_operation | DEC_sNaN;/*392*/}
       else {/*393*/Din_Go(1644,2048);if (op==COMPTOTAL) {        /* total ordering, always finite  */
        /* signs are known to be the same; compute the ordering here  */
        /* as if the signs are both positive, then invert for negatives  */
        Din_Go(1645,2048);if (!decNumberIsNaN(lhs)) {/*395*/Din_Go(1646,2048);result=-1;/*396*/}
         else {/*397*/Din_Go(1647,2048);if (!decNumberIsNaN(rhs)) {/*399*/Din_Go(1648,2048);result=+1;/*400*/}
         /* here if both NaNs  */
         else {/*401*/Din_Go(1649,2048);if (decNumberIsSNaN(lhs) && decNumberIsQNaN(rhs)) {/*403*/Din_Go(1650,2048);result=-1;/*404*/}
         else {/*405*/Din_Go(1651,2048);if (decNumberIsQNaN(lhs) && decNumberIsSNaN(rhs)) {/*407*/Din_Go(1652,2048);result=+1;/*408*/}
         else { /* both NaN or both sNaN  */
          /* now it just depends on the payload  */
          Din_Go(1653,2048);result=decUnitCompare(lhs->lsu, D2U(lhs->digits),
                                rhs->lsu, D2U(rhs->digits), 0);
          /* [Error not possible, as these are 'aligned']  */
          ;/*406*/}/*402*/}/*398*/}} /* both same NaNs  */
        Din_Go(1655,2048);if (decNumberIsNegative(lhs)) {/*409*/Din_Go(1654,2048);result=-result;/*410*/}
        Din_Go(1656,2048);break;
        } /* total order  */

       else {/*411*/Din_Go(1657,2048);if (merged & DECSNAN);           /* sNaN -> qNaN  */
       else { /* here if MIN or MAX and one or two quiet NaNs  */
        /* min or max -- 754 rules ignore single NaN  */
        Din_Go(1658,2048);if (!decNumberIsNaN(lhs) || !decNumberIsNaN(rhs)) {
          /* just one NaN; force choice to be the non-NaN operand  */
          Din_Go(1659,2048);op=COMPMAX;
          Din_Go(1662,2048);if (lhs->bits & DECNAN) {/*415*/Din_Go(1660,2048);result=-1;/*416*/} /* pick rhs  */
                             else {/*417*/Din_Go(1661,2048);result=+1;/*418*/} /* pick lhs  */
          Din_Go(1663,2048);break;
          }
        ;/*412*/}/*394*/}/*390*/}} /* max or min  */
      Din_Go(1664,2048);op=COMPNAN;                            /* use special path  */
      decNaNs(res, lhs, rhs, set, status);   /* propagate NaN  */
      Din_Go(1665,2048);break;
      }
    /* have numbers  */
    Din_Go(1669,2048);if (op==COMPMAXMAG || op==COMPMINMAG) {/*419*/Din_Go(1667,2048);result=decCompare(lhs, rhs, 1);/*420*/}
     else {/*421*/Din_Go(1668,2048);result=decCompare(lhs, rhs, 0);/*422*/}    /* sign matters  */
    } while(0);                              /* end protected  */

  Din_Go(1700,2048);if (result==BADINT) *status|=DEC_Insufficient_storage; /* rare  */
   else {
    Din_Go(1671,2048);if (op==COMPARE || op==COMPSIG ||op==COMPTOTAL) { /* returning signum  */
      Din_Go(1672,2048);if (op==COMPTOTAL && result==0) {
        /* operands are numerically equal or same NaN (and same sign,  */
        /* tested first); if identical, leave result 0  */
        Din_Go(1673,2048);if (lhs->exponent!=rhs->exponent) {
          Din_Go(1674,2048);if (lhs->exponent<rhs->exponent) {/*423*/Din_Go(1675,2048);result=-1;/*424*/}
           else {/*425*/Din_Go(1676,2048);result=+1;/*426*/}
          Din_Go(1678,2048);if (decNumberIsNegative(lhs)) {/*427*/Din_Go(1677,2048);result=-result;/*428*/}
          } /* lexp!=rexp  */
        } /* total-order by exponent  */
      uprv_decNumberZero(res);               /* [always a valid result]  */
      Din_Go(1681,2048);if (result!=0) {                  /* must be -1 or +1  */
        Din_Go(1679,2048);*res->lsu=1;
        Din_Go(1680,2048);if (result<0) res->bits=DECNEG;
        }
      }
     else {/*429*/Din_Go(1682,2048);if (op==COMPNAN);             /* special, drop through  */
     else {                             /* MAX or MIN, non-NaN result  */
      Int residue=0;                    /* rounding accumulator  */
      /* choose the operand for the result  */
      Din_Go(1683,2048);const decNumber *choice;
      Din_Go(1696,2048);if (result==0) { /* operands are numerically equal  */
        /* choose according to sign then exponent (see 754)  */
Din_Go(1684,2048);        uByte slhs=(lhs->bits & DECNEG);
        uByte srhs=(rhs->bits & DECNEG);
        #if DECSUBSET
        if (!set->extended) {           /* subset: force left-hand  */
          op=COMPMAX;
          result=+1;
          }
        else
        #endif
        Din_Go(1695,2048);if (slhs!=srhs) {          /* signs differ  */
          Din_Go(1685,2048);if (slhs) {/*433*/Din_Go(1686,2048);result=-1;/*434*/}     /* rhs is max  */
               else {/*435*/Din_Go(1687,2048);result=+1;/*436*/}     /* lhs is max  */
          }
         else {/*437*/Din_Go(1688,2048);if (slhs && srhs) {  /* both negative  */
          Din_Go(1689,2048);if (lhs->exponent<rhs->exponent) {/*439*/Din_Go(1690,2048);result=+1;/*440*/}
                                      else {/*441*/Din_Go(1691,2048);result=-1;/*442*/}
          /* [if equal, use lhs, technically identical]  */
          }
         else {                    /* both positive  */
          Din_Go(1692,2048);if (lhs->exponent>rhs->exponent) {/*443*/Din_Go(1693,2048);result=+1;/*444*/}
                                      else {/*445*/Din_Go(1694,2048);result=-1;/*446*/}
          /* [ditto]  */
          ;/*438*/}}
        } /* numerically equal  */
      /* here result will be non-0; reverse if looking for MIN  */
      Din_Go(1698,2048);if (op==COMPMIN || op==COMPMINMAG) {/*447*/Din_Go(1697,2048);result=-result;/*448*/}
      Din_Go(1699,2048);choice=(result>0 ? lhs : rhs);    /* choose  */
      /* copy chosen to result, rounding if need be  */
      decCopyFit(res, choice, set, &residue, status);
      decFinish(res, set, &residue, status);
      ;/*430*/}}
    }
  #if DECSUBSET
  if (allocrhs!=NULL) free(allocrhs);   /* free any storage used  */
  if (alloclhs!=NULL) free(alloclhs);   /* ..  */
  #endif
  {decNumber * ReplaceReturn40 = res; Din_Go(1701,2048); return ReplaceReturn40;}
  } /* decCompareOp  */

/* ------------------------------------------------------------------ */
/* decCompare -- compare two decNumbers by numerical value            */
/*                                                                    */
/*  This routine compares A ? B without altering them.                */
/*                                                                    */
/*  Arg1 is A, a decNumber which is not a NaN                         */
/*  Arg2 is B, a decNumber which is not a NaN                         */
/*  Arg3 is 1 for a sign-independent compare, 0 otherwise             */
/*                                                                    */
/*  returns -1, 0, or 1 for A<B, A==B, or A>B, or BADINT if failure   */
/*  (the only possible failure is an allocation error)                */
/* ------------------------------------------------------------------ */
static Int decCompare(const decNumber *lhs, const decNumber *rhs,
                      Flag abs_c) {
  Int   result;                    /* result value  */
  Int   sigr;                      /* rhs signum  */
  Int   compare;                   /* work  */

  result=1;                                  /* assume signum(lhs)  */
  Din_Go(1703,2048);if (ISZERO(lhs)) {/*361*/Din_Go(1702,2048);result=0;/*362*/}
  Din_Go(1721,2048);if (abs_c) {
    Din_Go(1704,2048);if (ISZERO(rhs)) {/*363*/{int32_t  ReplaceReturn39 = result; Din_Go(1705,2048); return ReplaceReturn39;}/*364*/}          /* LHS wins or both 0  */
    /* RHS is non-zero  */
    Din_Go(1707,2048);if (result==0) {/*365*/{int32_t  ReplaceReturn38 = -1; Din_Go(1706,2048); return ReplaceReturn38;}/*366*/}                /* LHS is 0; RHS wins  */
    /* [here, both non-zero, result=1]  */
    }
   else {                                    /* signs matter  */
    Din_Go(1708,2048);if (result && decNumberIsNegative(lhs)) {/*367*/Din_Go(1709,2048);result=-1;/*368*/}
    Din_Go(1710,2048);sigr=1;                                  /* compute signum(rhs)  */
    Din_Go(1714,2048);if (ISZERO(rhs)) {/*369*/Din_Go(1711,2048);sigr=0;/*370*/}
     else {/*371*/Din_Go(1712,2048);if (decNumberIsNegative(rhs)) {/*373*/Din_Go(1713,2048);sigr=-1;/*374*/}/*372*/}
    Din_Go(1716,2048);if (result > sigr) {/*375*/{int32_t  ReplaceReturn37 = +1; Din_Go(1715,2048); return ReplaceReturn37;}/*376*/}            /* L > R, return 1  */
    Din_Go(1718,2048);if (result < sigr) {/*377*/{int32_t  ReplaceReturn36 = -1; Din_Go(1717,2048); return ReplaceReturn36;}/*378*/}            /* L < R, return -1  */
    Din_Go(1720,2048);if (result==0) {/*379*/{int32_t  ReplaceReturn35 = 0; Din_Go(1719,2048); return ReplaceReturn35;}/*380*/}                   /* both 0  */
    }

  /* signums are the same; both are non-zero  */
  Din_Go(1727,2048);if ((lhs->bits | rhs->bits) & DECINF) {    /* one or more infinities  */
    Din_Go(1722,2048);if (decNumberIsInfinite(rhs)) {
      Din_Go(1723,2048);if (decNumberIsInfinite(lhs)) {/*381*/Din_Go(1724,2048);result=0;/*382*/}/* both infinite  */
       else {/*383*/Din_Go(1725,2048);result=-result;/*384*/}                  /* only rhs infinite  */
      }
    {int32_t  ReplaceReturn34 = result; Din_Go(1726,2048); return ReplaceReturn34;}
    }
  /* must compare the coefficients, allowing for exponents  */
  Din_Go(1729,2048);if (lhs->exponent>rhs->exponent) {         /* LHS exponent larger  */
    /* swap sides, and sign  */
    Din_Go(1728,2048);const decNumber *temp=lhs;
    lhs=rhs;
    rhs=temp;
    result=-result;
    }
  Din_Go(1730,2048);compare=decUnitCompare(lhs->lsu, D2U(lhs->digits),
                         rhs->lsu, D2U(rhs->digits),
                         rhs->exponent-lhs->exponent);
  Din_Go(1732,2048);if (compare!=BADINT) {/*385*/Din_Go(1731,2048);compare*=result;/*386*/}      /* comparison succeeded  */
  {int32_t  ReplaceReturn33 = compare; Din_Go(1733,2048); return ReplaceReturn33;}
  } /* decCompare  */

/* ------------------------------------------------------------------ */
/* decUnitCompare -- compare two >=0 integers in Unit arrays          */
/*                                                                    */
/*  This routine compares A ? B*10**E where A and B are unit arrays   */
/*  A is a plain integer                                              */
/*  B has an exponent of E (which must be non-negative)               */
/*                                                                    */
/*  Arg1 is A first Unit (lsu)                                        */
/*  Arg2 is A length in Units                                         */
/*  Arg3 is B first Unit (lsu)                                        */
/*  Arg4 is B length in Units                                         */
/*  Arg5 is E (0 if the units are aligned)                            */
/*                                                                    */
/*  returns -1, 0, or 1 for A<B, A==B, or A>B, or BADINT if failure   */
/*  (the only possible failure is an allocation error, which can      */
/*  only occur if E!=0)                                               */
/* ------------------------------------------------------------------ */
static Int decUnitCompare(const Unit *a, Int alength,
                          const Unit *b, Int blength, Int exp) {
  Unit  *acc;                      /* accumulator for result  */
  Unit  accbuff[SD2U(DECBUFFER*2+1)]; /* local buffer  */
  Unit  *allocacc=NULL;            /* -> allocated acc buffer, iff allocated  */
  Int   accunits, need;            /* units in use or needed for acc  */
  const Unit *l, *r, *u;           /* work  */
  Int   expunits, exprem, result;  /* ..  */

  Din_Go(1745,2048);if (exp==0) {                    /* aligned; fastpath  */
    Din_Go(1734,2048);if (alength>blength) {/*715*/{int32_t  ReplaceReturn32 = 1; Din_Go(1735,2048); return ReplaceReturn32;}/*716*/}
    Din_Go(1737,2048);if (alength<blength) {/*717*/{int32_t  ReplaceReturn31 = -1; Din_Go(1736,2048); return ReplaceReturn31;}/*718*/}
    /* same number of units in both -- need unit-by-unit compare  */
    Din_Go(1738,2048);l=a+alength-1;
    r=b+alength-1;
    Din_Go(1743,2048);for (;l>=a; l--, r--) {
      Din_Go(1739,2048);if (*l>*r) {/*719*/{int32_t  ReplaceReturn30 = 1; Din_Go(1740,2048); return ReplaceReturn30;}/*720*/}
      Din_Go(1742,2048);if (*l<*r) {/*721*/{int32_t  ReplaceReturn29 = -1; Din_Go(1741,2048); return ReplaceReturn29;}/*722*/}
      }
    {int32_t  ReplaceReturn28 = 0; Din_Go(1744,2048); return ReplaceReturn28;}                      /* all units match  */
    } /* aligned  */

  /* Unaligned.  If one is >1 unit longer than the other, padded  */
  /* approximately, then can return easily  */
  Din_Go(1747,2048);if (alength>blength+(Int)D2U(exp)) {/*723*/{int32_t  ReplaceReturn27 = 1; Din_Go(1746,2048); return ReplaceReturn27;}/*724*/}
  Din_Go(1749,2048);if (alength+1<blength+(Int)D2U(exp)) {/*725*/{int32_t  ReplaceReturn26 = -1; Din_Go(1748,2048); return ReplaceReturn26;}/*726*/}

  /* Need to do a real subtract.  For this, a result buffer is needed  */
  /* even though only the sign is of interest.  Its length needs  */
  /* to be the larger of alength and padded blength, +2  */
  Din_Go(1750,2048);need=blength+D2U(exp);                /* maximum real length of B  */
  Din_Go(1752,2048);if (need<alength) {/*727*/Din_Go(1751,2048);need=alength;/*728*/}
  Din_Go(1753,2048);need+=2;
  acc=accbuff;                          /* assume use local buffer  */
  Din_Go(1756,2048);if (need*sizeof(Unit)>sizeof(accbuff)) {
    Din_Go(1754,2048);allocacc=(Unit *)malloc(need*sizeof(Unit));
    if (allocacc==NULL) return BADINT;  /* hopeless -- abandon  */
    Din_Go(1755,2048);acc=allocacc;
    }
  /* Calculate units and remainder from exponent.  */
  Din_Go(1757,2048);expunits=exp/DECDPUN;
  exprem=exp%DECDPUN;
  /* subtract [A+B*(-m)]  */
  accunits=decUnitAddSub(a, alength, b, blength, expunits, acc,
                         -(Int)powers[exprem]);
  /* [UnitAddSub result may have leading zeros, even on zero]  */
  Din_Go(1762,2048);if (accunits<0) {/*731*/Din_Go(1758,2048);result=-1;/*732*/}            /* negative result  */
   else {                               /* non-negative result  */
    /* check units of the result before freeing any storage  */
    Din_Go(1759,2048);for (u=acc; u<acc+accunits-1 && *u==0;) {/*733*/Din_Go(1760,2048);u++;/*734*/}
    Din_Go(1761,2048);result=(*u==0 ? 0 : +1);
    }
  /* clean up and return the result  */
  if (allocacc!=NULL) free(allocacc);   /* drop any storage used  */
  {int32_t  ReplaceReturn25 = result; Din_Go(1763,2048); return ReplaceReturn25;}
  } /* decUnitCompare  */

/* ------------------------------------------------------------------ */
/* decUnitAddSub -- add or subtract two >=0 integers in Unit arrays   */
/*                                                                    */
/*  This routine performs the calculation:                            */
/*                                                                    */
/*  C=A+(B*M)                                                         */
/*                                                                    */
/*  Where M is in the range -DECDPUNMAX through +DECDPUNMAX.          */
/*                                                                    */
/*  A may be shorter or longer than B.                                */
/*                                                                    */
/*  Leading zeros are not removed after a calculation.  The result is */
/*  either the same length as the longer of A and B (adding any       */
/*  shift), or one Unit longer than that (if a Unit carry occurred).  */
/*                                                                    */
/*  A and B content are not altered unless C is also A or B.          */
/*  C may be the same array as A or B, but only if no zero padding is */
/*  requested (that is, C may be B only if bshift==0).                */
/*  C is filled from the lsu; only those units necessary to complete  */
/*  the calculation are referenced.                                   */
/*                                                                    */
/*  Arg1 is A first Unit (lsu)                                        */
/*  Arg2 is A length in Units                                         */
/*  Arg3 is B first Unit (lsu)                                        */
/*  Arg4 is B length in Units                                         */
/*  Arg5 is B shift in Units  (>=0; pads with 0 units if positive)    */
/*  Arg6 is C first Unit (lsu)                                        */
/*  Arg7 is M, the multiplier                                         */
/*                                                                    */
/*  returns the count of Units written to C, which will be non-zero   */
/*  and negated if the result is negative.  That is, the sign of the  */
/*  returned Int is the sign of the result (positive for zero) and    */
/*  the absolute value of the Int is the count of Units.              */
/*                                                                    */
/*  It is the caller's responsibility to make sure that C size is     */
/*  safe, allowing space if necessary for a one-Unit carry.           */
/*                                                                    */
/*  This routine is severely performance-critical; *any* change here  */
/*  must be measured (timed) to assure no performance degradation.    */
/*  In particular, trickery here tends to be counter-productive, as   */
/*  increased complexity of code hurts register optimizations on      */
/*  register-poor architectures.  Avoiding divisions is nearly        */
/*  always a Good Idea, however.                                      */
/*                                                                    */
/* Special thanks to Rick McGuire (IBM Cambridge, MA) and Dave Clark  */
/* (IBM Warwick, UK) for some of the ideas used in this routine.      */
/* ------------------------------------------------------------------ */
static Int decUnitAddSub(const Unit *a, Int alength,
                         const Unit *b, Int blength, Int bshift,
                         Unit *c, Int m) {
  Din_Go(1764,2048);const Unit *alsu=a;              /* A lsu [need to remember it]  */
  Unit *clsu=c;                    /* C ditto  */
  Unit *minC;                      /* low water mark for C  */
  Unit *maxC;                      /* high water mark for C  */
  eInt carry=0;                    /* carry integer (could be Long)  */
  Int  add;                        /* work  */
  #if DECDPUN<=4                   /* myriadal, millenary, etc.  */
  Int  est;                        /* estimated quotient  */
  #endif

  #if DECTRACE
  if (alength<1 || blength<1)
    printf("decUnitAddSub: alen blen m %ld %ld [%ld]\n", alength, blength, m);
  #endif

  maxC=c+alength;                  /* A is usually the longer  */
  minC=c+blength;                  /* .. and B the shorter  */
  Din_Go(1772,2048);if (bshift!=0) {                 /* B is shifted; low As copy across  */
    Din_Go(1765,2048);minC+=bshift;
    /* if in place [common], skip copy unless there's a gap [rare]  */
    Din_Go(1771,2048);if (a==c && bshift<=alength) {
      Din_Go(1766,2048);c+=bshift;
      a+=bshift;
      }
     else {/*705*/Din_Go(1767,2048);for (; c<clsu+bshift; a++, c++) {  /* copy needed  */
      Din_Go(1768,2048);if (a<alsu+alength) {/*707*/Din_Go(1769,2048);*c=*a;/*708*/}
       else {/*709*/Din_Go(1770,2048);*c=0;/*710*/}
      ;/*706*/}}
    }
  Din_Go(1774,2048);if (minC>maxC) { /* swap  */
Din_Go(1773,2048);    Unit *hold=minC;
    minC=maxC;
    maxC=hold;
    }

  /* For speed, do the addition as two loops; the first where both A  */
  /* and B contribute, and the second (if necessary) where only one or  */
  /* other of the numbers contribute.  */
  /* Carry handling is the same (i.e., duplicated) in each case.  */
  Din_Go(1783,2048);for (; c<minC; c++) {
    Din_Go(1775,2048);carry+=*a;
    a++;
    carry+=((eInt)*b)*m;                /* [special-casing m=1/-1  */
    b++;                                /* here is not a win]  */
    /* here carry is new Unit of digits; it could be +ve or -ve  */
    Din_Go(1778,2048);if ((ueInt)carry<=DECDPUNMAX) {     /* fastpath 0-DECDPUNMAX  */
      Din_Go(1776,2048);*c=(Unit)carry;
      carry=0;
      Din_Go(1777,2048);continue;
      }
    #if DECDPUN==4                           /* use divide-by-multiply  */
      if (carry>=0) {
        est=(((ueInt)carry>>11)*53687)>>18;
        *c=(Unit)(carry-est*(DECDPUNMAX+1)); /* remainder  */
        carry=est;                           /* likely quotient [89%]  */
        if (*c<DECDPUNMAX+1) continue;       /* estimate was correct  */
        carry++;
        *c-=DECDPUNMAX+1;
        continue;
        }
      /* negative case  */
      carry=carry+(eInt)(DECDPUNMAX+1)*(DECDPUNMAX+1); /* make positive  */
      est=(((ueInt)carry>>11)*53687)>>18;
      *c=(Unit)(carry-est*(DECDPUNMAX+1));
      carry=est-(DECDPUNMAX+1);              /* correctly negative  */
      if (*c<DECDPUNMAX+1) continue;         /* was OK  */
      carry++;
      *c-=DECDPUNMAX+1;
    #elif DECDPUN==3
      if (carry>=0) {
        est=(((ueInt)carry>>3)*16777)>>21;
        *c=(Unit)(carry-est*(DECDPUNMAX+1)); /* remainder  */
        carry=est;                           /* likely quotient [99%]  */
        if (*c<DECDPUNMAX+1) continue;       /* estimate was correct  */
        carry++;
        *c-=DECDPUNMAX+1;
        continue;
        }
      /* negative case  */
      carry=carry+(eInt)(DECDPUNMAX+1)*(DECDPUNMAX+1); /* make positive  */
      est=(((ueInt)carry>>3)*16777)>>21;
      *c=(Unit)(carry-est*(DECDPUNMAX+1));
      carry=est-(DECDPUNMAX+1);              /* correctly negative  */
      if (*c<DECDPUNMAX+1) continue;         /* was OK  */
      carry++;
      *c-=DECDPUNMAX+1;
    #elif DECDPUN<=2
      /* Can use QUOT10 as carry <= 4 digits  */
      Din_Go(1781,2048);if (carry>=0) {
        Din_Go(1779,2048);est=QUOT10(carry, DECDPUN);
        *c=(Unit)(carry-est*(DECDPUNMAX+1)); /* remainder  */
        carry=est;                           /* quotient  */
        Din_Go(1780,2048);continue;
        }
      /* negative case  */
      Din_Go(1782,2048);carry=carry+(eInt)(DECDPUNMAX+1)*(DECDPUNMAX+1); /* make positive  */
      est=QUOT10(carry, DECDPUN);
      *c=(Unit)(carry-est*(DECDPUNMAX+1));
      carry=est-(DECDPUNMAX+1);              /* correctly negative  */
    #else
      /* remainder operator is undefined if negative, so must test  */
      if ((ueInt)carry<(DECDPUNMAX+1)*2) {   /* fastpath carry +1  */
        *c=(Unit)(carry-(DECDPUNMAX+1));     /* [helps additions]  */
        carry=1;
        continue;
        }
      if (carry>=0) {
        *c=(Unit)(carry%(DECDPUNMAX+1));
        carry=carry/(DECDPUNMAX+1);
        continue;
        }
      /* negative case  */
      carry=carry+(eInt)(DECDPUNMAX+1)*(DECDPUNMAX+1); /* make positive  */
      *c=(Unit)(carry%(DECDPUNMAX+1));
      carry=carry/(DECDPUNMAX+1)-(DECDPUNMAX+1);
    #endif
    } /* c  */

  /* now may have one or other to complete  */
  /* [pretest to avoid loop setup/shutdown]  */
  Din_Go(1795,2048);if (c<maxC) {/*711*/Din_Go(1784,2048);for (; c<maxC; c++) {
    Din_Go(1785,2048);if (a<alsu+alength) {               /* still in A  */
      Din_Go(1786,2048);carry+=*a;
      a++;
      }
     else {                             /* inside B  */
      Din_Go(1787,2048);carry+=((eInt)*b)*m;
      b++;
      }
    /* here carry is new Unit of digits; it could be +ve or -ve and  */
    /* magnitude up to DECDPUNMAX squared  */
    Din_Go(1790,2048);if ((ueInt)carry<=DECDPUNMAX) {     /* fastpath 0-DECDPUNMAX  */
      Din_Go(1788,2048);*c=(Unit)carry;
      carry=0;
      Din_Go(1789,2048);continue;
      }
    /* result for this unit is negative or >DECDPUNMAX  */
    #if DECDPUN==4                           /* use divide-by-multiply  */
      if (carry>=0) {
        est=(((ueInt)carry>>11)*53687)>>18;
        *c=(Unit)(carry-est*(DECDPUNMAX+1)); /* remainder  */
        carry=est;                           /* likely quotient [79.7%]  */
        if (*c<DECDPUNMAX+1) continue;       /* estimate was correct  */
        carry++;
        *c-=DECDPUNMAX+1;
        continue;
        }
      /* negative case  */
      carry=carry+(eInt)(DECDPUNMAX+1)*(DECDPUNMAX+1); /* make positive  */
      est=(((ueInt)carry>>11)*53687)>>18;
      *c=(Unit)(carry-est*(DECDPUNMAX+1));
      carry=est-(DECDPUNMAX+1);              /* correctly negative  */
      if (*c<DECDPUNMAX+1) continue;         /* was OK  */
      carry++;
      *c-=DECDPUNMAX+1;
    #elif DECDPUN==3
      if (carry>=0) {
        est=(((ueInt)carry>>3)*16777)>>21;
        *c=(Unit)(carry-est*(DECDPUNMAX+1)); /* remainder  */
        carry=est;                           /* likely quotient [99%]  */
        if (*c<DECDPUNMAX+1) continue;       /* estimate was correct  */
        carry++;
        *c-=DECDPUNMAX+1;
        continue;
        }
      /* negative case  */
      carry=carry+(eInt)(DECDPUNMAX+1)*(DECDPUNMAX+1); /* make positive  */
      est=(((ueInt)carry>>3)*16777)>>21;
      *c=(Unit)(carry-est*(DECDPUNMAX+1));
      carry=est-(DECDPUNMAX+1);              /* correctly negative  */
      if (*c<DECDPUNMAX+1) continue;         /* was OK  */
      carry++;
      *c-=DECDPUNMAX+1;
    #elif DECDPUN<=2
      Din_Go(1793,2048);if (carry>=0) {
        Din_Go(1791,2048);est=QUOT10(carry, DECDPUN);
        *c=(Unit)(carry-est*(DECDPUNMAX+1)); /* remainder  */
        carry=est;                           /* quotient  */
        Din_Go(1792,2048);continue;
        }
      /* negative case  */
      Din_Go(1794,2048);carry=carry+(eInt)(DECDPUNMAX+1)*(DECDPUNMAX+1); /* make positive  */
      est=QUOT10(carry, DECDPUN);
      *c=(Unit)(carry-est*(DECDPUNMAX+1));
      carry=est-(DECDPUNMAX+1);              /* correctly negative  */
    #else
      if ((ueInt)carry<(DECDPUNMAX+1)*2){    /* fastpath carry 1  */
        *c=(Unit)(carry-(DECDPUNMAX+1));
        carry=1;
        continue;
        }
      /* remainder operator is undefined if negative, so must test  */
      if (carry>=0) {
        *c=(Unit)(carry%(DECDPUNMAX+1));
        carry=carry/(DECDPUNMAX+1);
        continue;
        }
      /* negative case  */
      carry=carry+(eInt)(DECDPUNMAX+1)*(DECDPUNMAX+1); /* make positive  */
      *c=(Unit)(carry%(DECDPUNMAX+1));
      carry=carry/(DECDPUNMAX+1)-(DECDPUNMAX+1);
    #endif
    ;/*712*/}} /* c  */

  /* OK, all A and B processed; might still have carry or borrow  */
  /* return number of Units in the result, negated if a borrow  */
  Din_Go(1797,2048);if (carry==0) {/*713*/{int32_t  ReplaceReturn24 = c-clsu; Din_Go(1796,2048); return ReplaceReturn24;}/*714*/}     /* no carry, so no more to do  */
  Din_Go(1800,2048);if (carry>0) {                   /* positive carry  */
    Din_Go(1798,2048);*c=(Unit)carry;                /* place as new unit  */
    c++;                           /* ..  */
    {int32_t  ReplaceReturn23 = c-clsu; Din_Go(1799,2048); return ReplaceReturn23;}
    }
  /* -ve carry: it's a borrow; complement needed  */
  Din_Go(1801,2048);add=1;                           /* temporary carry...  */
  Din_Go(1806,2048);for (c=clsu; c<maxC; c++) {
    Din_Go(1802,2048);add=DECDPUNMAX+add-*c;
    Din_Go(1805,2048);if (add<=DECDPUNMAX) {
      Din_Go(1803,2048);*c=(Unit)add;
      add=0;
      }
     else {
      Din_Go(1804,2048);*c=0;
      add=1;
      }
    }
  /* add an extra unit iff it would be non-zero  */
  #if DECTRACE
    printf("UAS borrow: add %ld, carry %ld\n", add, carry);
  #endif
  Din_Go(1808,2048);if ((add-carry-1)!=0) {
    Din_Go(1807,2048);*c=(Unit)(add-carry-1);
    c++;                      /* interesting, include it  */
    }
  {int32_t  ReplaceReturn22 = clsu-c; Din_Go(1809,2048); return ReplaceReturn22;}              /* -ve result indicates borrowed  */
  } /* decUnitAddSub  */

/* ------------------------------------------------------------------ */
/* decTrim -- trim trailing zeros or normalize                        */
/*                                                                    */
/*   dn is the number to trim or normalize                            */
/*   set is the context to use to check for clamp                     */
/*   all is 1 to remove all trailing zeros, 0 for just fraction ones  */
/*   noclamp is 1 to unconditional (unclamped) trim                   */
/*   dropped returns the number of discarded trailing zeros           */
/*   returns dn                                                       */
/*                                                                    */
/* If clamp is set in the context then the number of zeros trimmed    */
/* may be limited if the exponent is high.                            */
/* All fields are updated as required.  This is a utility operation,  */
/* so special values are unchanged and no error is possible.          */
/* ------------------------------------------------------------------ */
static decNumber * decTrim(decNumber *dn, decContext *set, Flag all,
                           Flag noclamp, Int *dropped) {
  Int   d, exp;                    /* work  */
  uInt  cut;                       /* ..  */
  Unit  *up;                       /* -> current Unit  */

  #if DECCHECK
  if (decCheckOperands(dn, DECUNUSED, DECUNUSED, DECUNCONT)) return dn;
  #endif

  *dropped=0;                           /* assume no zeros dropped  */
  Din_Go(1811,2048);if ((dn->bits & DECSPECIAL)           /* fast exit if special ..  */
    || (*dn->lsu & 0x01)) {/*693*/{decNumber * ReplaceReturn21 = dn; Din_Go(1810,2048); return ReplaceReturn21;}/*694*/}    /* .. or odd  */
  Din_Go(1814,2048);if (ISZERO(dn)) {                     /* .. or 0  */
    Din_Go(1812,2048);dn->exponent=0;                     /* (sign is preserved)  */
    {decNumber * ReplaceReturn20 = dn; Din_Go(1813,2048); return ReplaceReturn20;}
    }

  /* have a finite number which is even  */
  Din_Go(1815,2048);exp=dn->exponent;
  cut=1;                           /* digit (1-DECDPUN) in Unit  */
  up=dn->lsu;                      /* -> current Unit  */
  Din_Go(1826,2048);for (d=0; d<dn->digits-1; d++) { /* [don't strip the final digit]  */
    /* slice by powers  */
    #if DECDPUN<=4
      uInt quot=QUOT10(*up, cut);
      Din_Go(1817,2048);if ((*up-quot*powers[cut])!=0) {/*695*/Din_Go(1816,2048);break;/*696*/}  /* found non-0 digit  */
    #else
      if (*up%powers[cut]!=0) break;         /* found non-0 digit  */
    #endif
    /* have a trailing 0  */
    Din_Go(1822,2048);if (!all) {                    /* trimming  */
      /* [if exp>0 then all trailing 0s are significant for trim]  */
      Din_Go(1818,2048);if (exp<=0) {                /* if digit might be significant  */
        Din_Go(1819,2048);if (exp==0) {/*697*/Din_Go(1820,2048);break;/*698*/}         /* then quit  */
        Din_Go(1821,2048);exp++;                     /* next digit might be significant  */
        }
      }
    Din_Go(1823,2048);cut++;                         /* next power  */
    Din_Go(1825,2048);if (cut>DECDPUN) {             /* need new Unit  */
      Din_Go(1824,2048);up++;
      cut=1;
      }
    } /* d  */
  Din_Go(1828,2048);if (d==0) {/*699*/{decNumber * ReplaceReturn19 = dn; Din_Go(1827,2048); return ReplaceReturn19;}/*700*/}             /* none to drop  */

  /* may need to limit drop if clamping  */
  Din_Go(1834,2048);if (set->clamp && !noclamp) {
Din_Go(1829,2048);    Int maxd=set->emax-set->digits+1-dn->exponent;
    Din_Go(1831,2048);if (maxd<=0) {/*701*/{decNumber * ReplaceReturn18 = dn; Din_Go(1830,2048); return ReplaceReturn18;}/*702*/}        /* nothing possible  */
    Din_Go(1833,2048);if (d>maxd) {/*703*/Din_Go(1832,2048);d=maxd;/*704*/}
    }

  /* effect the drop  */
  Din_Go(1835,2048);decShiftToLeast(dn->lsu, D2U(dn->digits), d);
  dn->exponent+=d;                 /* maintain numerical value  */
  dn->digits-=d;                   /* new length  */
  *dropped=d;                      /* report the count  */
  {decNumber * ReplaceReturn17 = dn; Din_Go(1836,2048); return ReplaceReturn17;}
  } /* decTrim  */

/* ------------------------------------------------------------------ */
/* decReverse -- reverse a Unit array in place                        */
/*                                                                    */
/*   ulo    is the start of the array                                 */
/*   uhi    is the end of the array (highest Unit to include)         */
/*                                                                    */
/* The units ulo through uhi are reversed in place (if the number     */
/* of units is odd, the middle one is untouched).  Note that the      */
/* digit(s) in each unit are unaffected.                              */
/* ------------------------------------------------------------------ */
static void decReverse(Unit *ulo, Unit *uhi) {
Din_Go(1837,2048);  Unit temp;
  Din_Go(1839,2048);for (; ulo<uhi; ulo++, uhi--) {
    Din_Go(1838,2048);temp=*ulo;
    *ulo=*uhi;
    *uhi=temp;
    }
  Din_Go(1840,2048);return;
  } /* decReverse  */

/* ------------------------------------------------------------------ */
/* decShiftToMost -- shift digits in array towards most significant   */
/*                                                                    */
/*   uar    is the array                                              */
/*   digits is the count of digits in use in the array                */
/*   shift  is the number of zeros to pad with (least significant);   */
/*     it must be zero or positive                                    */
/*                                                                    */
/*   returns the new length of the integer in the array, in digits    */
/*                                                                    */
/* No overflow is permitted (that is, the uar array must be known to  */
/* be large enough to hold the result, after shifting).               */
/* ------------------------------------------------------------------ */
static Int decShiftToMost(Unit *uar, Int digits, Int shift) {
Din_Go(1841,2048);  Unit  *target, *source, *first;  /* work  */
  Int   cut;                       /* odd 0's to add  */
  uInt  next;                      /* work  */

  Din_Go(1843,2048);if (shift==0) {/*665*/{int32_t  ReplaceReturn16 = digits; Din_Go(1842,2048); return ReplaceReturn16;}/*666*/}     /* [fastpath] nothing to do  */
  Din_Go(1846,2048);if ((digits+shift)<=DECDPUN) {   /* [fastpath] single-unit case  */
    Din_Go(1844,2048);*uar=(Unit)(*uar*powers[shift]);
    {int32_t  ReplaceReturn15 = digits+shift; Din_Go(1845,2048); return ReplaceReturn15;}
    }

  Din_Go(1847,2048);next=0;                          /* all paths  */
  source=uar+D2U(digits)-1;        /* where msu comes from  */
  target=source+D2U(shift);        /* where upper part of first cut goes  */
  cut=DECDPUN-MSUDIGITS(shift);    /* where to slice  */
  Din_Go(1855,2048);if (cut==0) {                    /* unit-boundary case  */
    Din_Go(1848,2048);for (; source>=uar; source--, target--) {/*667*/Din_Go(1849,2048);*target=*source;/*668*/}
    }
   else {
    Din_Go(1850,2048);first=uar+D2U(digits+shift)-1; /* where msu of source will end up  */
    Din_Go(1854,2048);for (; source>=uar; source--, target--) {
      /* split the source Unit and accumulate remainder for next  */
      #if DECDPUN<=4
        uInt quot=QUOT10(*source, cut);
        uInt rem=*source-quot*powers[cut];
        next+=quot;
      #else
        uInt rem=*source%powers[cut];
        next+=*source/powers[cut];
      #endif
      Din_Go(1852,2048);if (target<=first) {/*669*/Din_Go(1851,2048);*target=(Unit)next;/*670*/}   /* write to target iff valid  */
      Din_Go(1853,2048);next=rem*powers[DECDPUN-cut];            /* save remainder for next Unit  */
      }
    } /* shift-move  */

  /* propagate any partial unit to one below and clear the rest  */
  Din_Go(1857,2048);for (; target>=uar; target--) {
    Din_Go(1856,2048);*target=(Unit)next;
    next=0;
    }
  {int32_t  ReplaceReturn14 = digits+shift; Din_Go(1858,2048); return ReplaceReturn14;}
  } /* decShiftToMost  */

/* ------------------------------------------------------------------ */
/* decShiftToLeast -- shift digits in array towards least significant */
/*                                                                    */
/*   uar   is the array                                               */
/*   units is length of the array, in units                           */
/*   shift is the number of digits to remove from the lsu end; it     */
/*     must be zero or positive and <= than units*DECDPUN.            */
/*                                                                    */
/*   returns the new length of the integer in the array, in units     */
/*                                                                    */
/* Removed digits are discarded (lost).  Units not required to hold   */
/* the final result are unchanged.                                    */
/* ------------------------------------------------------------------ */
static Int decShiftToLeast(Unit *uar, Int units, Int shift) {
Din_Go(1859,2048);  Unit  *target, *up;              /* work  */
  Int   cut, count;                /* work  */
  Int   quot, rem;                 /* for division  */

  Din_Go(1861,2048);if (shift==0) {/*657*/{int32_t  ReplaceReturn13 = units; Din_Go(1860,2048); return ReplaceReturn13;}/*658*/}      /* [fastpath] nothing to do  */
  Din_Go(1864,2048);if (shift==units*DECDPUN) {      /* [fastpath] little to do  */
    Din_Go(1862,2048);*uar=0;                        /* all digits cleared gives zero  */
    {int32_t  ReplaceReturn12 = 1; Din_Go(1863,2048); return ReplaceReturn12;}                      /* leaves just the one  */
    }

  Din_Go(1865,2048);target=uar;                      /* both paths  */
  cut=MSUDIGITS(shift);
  Din_Go(1870,2048);if (cut==DECDPUN) {              /* unit-boundary case; easy  */
    Din_Go(1866,2048);up=uar+D2U(shift);
    Din_Go(1868,2048);for (; up<uar+units; target++, up++) {/*659*/Din_Go(1867,2048);*target=*up;/*660*/}
    {int32_t  ReplaceReturn11 = target-uar; Din_Go(1869,2048); return ReplaceReturn11;}
    }

  /* messier  */
  Din_Go(1871,2048);up=uar+D2U(shift-cut);           /* source; correct to whole Units  */
  count=units*DECDPUN-shift;       /* the maximum new length  */
  #if DECDPUN<=4
    quot=QUOT10(*up, cut);
  #else
    quot=*up/powers[cut];
  #endif
  Din_Go(1878,2048);for (; ; target++) {
    Din_Go(1872,2048);*target=(Unit)quot;
    count-=(DECDPUN-cut);
    Din_Go(1874,2048);if (count<=0) {/*661*/Din_Go(1873,2048);break;/*662*/}
    Din_Go(1875,2048);up++;
    quot=*up;
    #if DECDPUN<=4
      quot=QUOT10(quot, cut);
      rem=*up-quot*powers[cut];
    #else
      rem=quot%powers[cut];
      quot=quot/powers[cut];
    #endif
    *target=(Unit)(*target+rem*powers[DECDPUN-cut]);
    count-=cut;
    Din_Go(1877,2048);if (count<=0) {/*663*/Din_Go(1876,2048);break;/*664*/}
    }
  {int32_t  ReplaceReturn10 = target-uar+1; Din_Go(1879,2048); return ReplaceReturn10;}
  } /* decShiftToLeast  */

#if DECSUBSET
/* ------------------------------------------------------------------ */
/* decRoundOperand -- round an operand  [used for subset only]        */
/*                                                                    */
/*   dn is the number to round (dn->digits is > set->digits)          */
/*   set is the relevant context                                      */
/*   status is the status accumulator                                 */
/*                                                                    */
/*   returns an allocated decNumber with the rounded result.          */
/*                                                                    */
/* lostDigits and other status may be set by this.                    */
/*                                                                    */
/* Since the input is an operand, it must not be modified.            */
/* Instead, return an allocated decNumber, rounded as required.       */
/* It is the caller's responsibility to free the allocated storage.   */
/*                                                                    */
/* If no storage is available then the result cannot be used, so NULL */
/* is returned.                                                       */
/* ------------------------------------------------------------------ */
static decNumber *decRoundOperand(const decNumber *dn, decContext *set,
                                  uInt *status) {
  decNumber *res;                       /* result structure  */
  uInt newstatus=0;                     /* status from round  */
  Int  residue=0;                       /* rounding accumulator  */

  /* Allocate storage for the returned decNumber, big enough for the  */
  /* length specified by the context  */
  res=(decNumber *)malloc(sizeof(decNumber)
                          +(D2U(set->digits)-1)*sizeof(Unit));
  if (res==NULL) {
    *status|=DEC_Insufficient_storage;
    return NULL;
    }
  decCopyFit(res, dn, set, &residue, &newstatus);
  decApplyRound(res, set, residue, &newstatus);

  /* If that set Inexact then "lost digits" is raised...  */
  if (newstatus & DEC_Inexact) newstatus|=DEC_Lost_digits;
  *status|=newstatus;
  return res;
  } /* decRoundOperand  */
#endif

/* ------------------------------------------------------------------ */
/* decCopyFit -- copy a number, truncating the coefficient if needed  */
/*                                                                    */
/*   dest is the target decNumber                                     */
/*   src  is the source decNumber                                     */
/*   set is the context [used for length (digits) and rounding mode]  */
/*   residue is the residue accumulator                               */
/*   status contains the current status to be updated                 */
/*                                                                    */
/* (dest==src is allowed and will be a no-op if fits)                 */
/* All fields are updated as required.                                */
/* ------------------------------------------------------------------ */
static void decCopyFit(decNumber *dest, const decNumber *src,
                       decContext *set, Int *residue, uInt *status) {
  Din_Go(1880,2048);dest->bits=src->bits;
  dest->exponent=src->exponent;
  decSetCoeff(dest, set, src->lsu, src->digits, residue, status);
  Din_Go(1881,2048);} /* decCopyFit  */

/* ------------------------------------------------------------------ */
/* decSetCoeff -- set the coefficient of a number                     */
/*                                                                    */
/*   dn    is the number whose coefficient array is to be set.        */
/*         It must have space for set->digits digits                  */
/*   set   is the context [for size]                                  */
/*   lsu   -> lsu of the source coefficient [may be dn->lsu]          */
/*   len   is digits in the source coefficient [may be dn->digits]    */
/*   residue is the residue accumulator.  This has values as in       */
/*         decApplyRound, and will be unchanged unless the            */
/*         target size is less than len.  In this case, the           */
/*         coefficient is truncated and the residue is updated to     */
/*         reflect the previous residue and the dropped digits.       */
/*   status is the status accumulator, as usual                       */
/*                                                                    */
/* The coefficient may already be in the number, or it can be an      */
/* external intermediate array.  If it is in the number, lsu must ==  */
/* dn->lsu and len must == dn->digits.                                */
/*                                                                    */
/* Note that the coefficient length (len) may be < set->digits, and   */
/* in this case this merely copies the coefficient (or is a no-op     */
/* if dn->lsu==lsu).                                                  */
/*                                                                    */
/* Note also that (only internally, from decQuantizeOp and            */
/* decSetSubnormal) the value of set->digits may be less than one,    */
/* indicating a round to left.  This routine handles that case        */
/* correctly; caller ensures space.                                   */
/*                                                                    */
/* dn->digits, dn->lsu (and as required), and dn->exponent are        */
/* updated as necessary.   dn->bits (sign) is unchanged.              */
/*                                                                    */
/* DEC_Rounded status is set if any digits are discarded.             */
/* DEC_Inexact status is set if any non-zero digits are discarded, or */
/*                       incoming residue was non-0 (implies rounded) */
/* ------------------------------------------------------------------ */
/* mapping array: maps 0-9 to canonical residues, so that a residue  */
/* can be adjusted in the range [-1, +1] and achieve correct rounding  */
/*                             0  1  2  3  4  5  6  7  8  9  */
static const uByte resmap[10]={0, 3, 3, 3, 3, 5, 7, 7, 7, 7};
static void decSetCoeff(decNumber *dn, decContext *set, const Unit *lsu,
                        Int len, Int *residue, uInt *status) {
  Int   discard;              /* number of digits to discard  */
  uInt  cut;                  /* cut point in Unit  */
  const Unit *up;             /* work  */
  Unit  *target;              /* ..  */
  Int   count;                /* ..  */
  #if DECDPUN<=4
  uInt  temp;                 /* ..  */
  #endif

  discard=len-set->digits;    /* digits to discard  */
  Din_Go(1890,2048);if (discard<=0) {           /* no digits are being discarded  */
    Din_Go(1882,2048);if (dn->lsu!=lsu) {       /* copy needed  */
      /* copy the coefficient array to the result number; no shift needed  */
      Din_Go(1883,2048);count=len;              /* avoids D2U  */
      up=lsu;
      Din_Go(1885,2048);for (target=dn->lsu; count>0; target++, up++, count-=DECDPUN)
        {/*623*/Din_Go(1884,2048);*target=*up;/*624*/}
      Din_Go(1886,2048);dn->digits=len;         /* set the new length  */
      }
    /* dn->exponent and residue are unchanged, record any inexactitude  */
    Din_Go(1888,2048);if (*residue!=0) {/*625*/Din_Go(1887,2048);*status|=(DEC_Inexact | DEC_Rounded);/*626*/}
    Din_Go(1889,2048);return;
    }

  /* some digits must be discarded ...  */
  Din_Go(1891,2048);dn->exponent+=discard;      /* maintain numerical value  */
  *status|=DEC_Rounded;       /* accumulate Rounded status  */
  Din_Go(1893,2048);if (*residue>1) {/*627*/Din_Go(1892,2048);*residue=1;/*628*/} /* previous residue now to right, so reduce  */

  Din_Go(1903,2048);if (discard>len) {          /* everything, +1, is being discarded  */
    /* guard digit is 0  */
    /* residue is all the number [NB could be all 0s]  */
    Din_Go(1894,2048);if (*residue<=0) {        /* not already positive  */
      Din_Go(1895,2048);count=len;              /* avoids D2U  */
      Din_Go(1899,2048);for (up=lsu; count>0; up++, count-=DECDPUN) {/*629*/Din_Go(1896,2048);if (*up!=0) { /* found non-0  */
        Din_Go(1897,2048);*residue=1;
        Din_Go(1898,2048);break;                /* no need to check any others  */
        ;/*630*/}}
      }
    Din_Go(1900,2048);if (*residue!=0) *status|=DEC_Inexact; /* record inexactitude  */
    Din_Go(1901,2048);*dn->lsu=0;               /* coefficient will now be 0  */
    dn->digits=1;             /* ..  */
    Din_Go(1902,2048);return;
    } /* total discard  */

  /* partial discard [most common case]  */
  /* here, at least the first (most significant) discarded digit exists  */

  /* spin up the number, noting residue during the spin, until get to  */
  /* the Unit with the first discarded digit.  When reach it, extract  */
  /* it and remember its position  */
  Din_Go(1904,2048);count=0;
  Din_Go(1910,2048);for (up=lsu;; up++) {
    Din_Go(1905,2048);count+=DECDPUN;
    Din_Go(1907,2048);if (count>=discard) {/*631*/Din_Go(1906,2048);break;/*632*/} /* full ones all checked  */
    Din_Go(1909,2048);if (*up!=0) {/*633*/Din_Go(1908,2048);*residue=1;/*634*/}
    } /* up  */

  /* here up -> Unit with first discarded digit  */
  Din_Go(1911,2048);cut=discard-(count-DECDPUN)-1;
  Din_Go(1940,2048);if (cut==DECDPUN-1) {       /* unit-boundary case (fast)  */
Din_Go(1912,2048);    Unit half=(Unit)powers[DECDPUN]>>1;
    /* set residue directly  */
    Din_Go(1918,2048);if (*up>=half) {
      Din_Go(1913,2048);if (*up>half) {/*635*/Din_Go(1914,2048);*residue=7;/*636*/}
      else {/*637*/Din_Go(1915,2048);*residue+=5;/*638*/}       /* add sticky bit  */
      }
     else { /* <half  */
      Din_Go(1916,2048);if (*up!=0) {/*639*/Din_Go(1917,2048);*residue=3;/*640*/} /* [else is 0, leave as sticky bit]  */
      }
    Din_Go(1923,2048);if (set->digits<=0) {     /* special for Quantize/Subnormal :-(  */
      Din_Go(1919,2048);*dn->lsu=0;             /* .. result is 0  */
      dn->digits=1;           /* ..  */
      }
     else {                   /* shift to least  */
      Din_Go(1920,2048);count=set->digits;      /* now digits to end up with  */
      dn->digits=count;       /* set the new length  */
      up++;                   /* move to next  */
      /* on unit boundary, so shift-down copy loop is simple  */
      Din_Go(1922,2048);for (target=dn->lsu; count>0; target++, up++, count-=DECDPUN)
        {/*641*/Din_Go(1921,2048);*target=*up;/*642*/}
      }
    } /* unit-boundary case  */

   else { /* discard digit is in low digit(s), and not top digit  */
    uInt  discard1;                /* first discarded digit  */
    uInt  quot, rem;               /* for divisions  */
    Din_Go(1928,2048);if (cut==0) {/*643*/Din_Go(1924,2048);quot=*up;/*644*/}          /* is at bottom of unit  */
     else /* cut>0 */ {            /* it's not at bottom of unit  */
      #if DECDPUN<=4
        U_ASSERT(/* cut >= 0 &&*/ cut <= 4);
        Din_Go(1925,2048);quot=QUOT10(*up, cut);
        rem=*up-quot*powers[cut];
      #else
        rem=*up%powers[cut];
        quot=*up/powers[cut];
      #endif
      Din_Go(1927,2048);if (rem!=0) {/*645*/Din_Go(1926,2048);*residue=1;/*646*/}
      }
    /* discard digit is now at bottom of quot  */
    #if DECDPUN<=4
      Din_Go(1929,2048);temp=(quot*6554)>>16;        /* fast /10  */
      /* Vowels algorithm here not a win (9 instructions)  */
      discard1=quot-X10(temp);
      quot=temp;
    #else
      discard1=quot%10;
      quot=quot/10;
    #endif
    /* here, discard1 is the guard digit, and residue is everything  */
    /* else [use mapping array to accumulate residue safely]  */
    *residue+=resmap[discard1];
    cut++;                         /* update cut  */
    /* here: up -> Unit of the array with bottom digit  */
    /*       cut is the division point for each Unit  */
    /*       quot holds the uncut high-order digits for the current unit  */
    Din_Go(1939,2048);if (set->digits<=0) {          /* special for Quantize/Subnormal :-(  */
      Din_Go(1930,2048);*dn->lsu=0;                  /* .. result is 0  */
      dn->digits=1;                /* ..  */
      }
     else {                        /* shift to least needed  */
      Din_Go(1931,2048);count=set->digits;           /* now digits to end up with  */
      dn->digits=count;            /* set the new length  */
      /* shift-copy the coefficient array to the result number  */
      Din_Go(1938,2048);for (target=dn->lsu; ; target++) {
        Din_Go(1932,2048);*target=(Unit)quot;
        count-=(DECDPUN-cut);
        Din_Go(1934,2048);if (count<=0) {/*647*/Din_Go(1933,2048);break;/*648*/}
        Din_Go(1935,2048);up++;
        quot=*up;
        #if DECDPUN<=4
          quot=QUOT10(quot, cut);
          rem=*up-quot*powers[cut];
        #else
          rem=quot%powers[cut];
          quot=quot/powers[cut];
        #endif
        *target=(Unit)(*target+rem*powers[DECDPUN-cut]);
        count-=cut;
        Din_Go(1937,2048);if (count<=0) {/*649*/Din_Go(1936,2048);break;/*650*/}
        } /* shift-copy loop  */
      } /* shift to least  */
    } /* not unit boundary  */

  Din_Go(1941,2048);if (*residue!=0) *status|=DEC_Inexact; /* record inexactitude  */
  Din_Go(1942,2048);return;
  } /* decSetCoeff  */

/* ------------------------------------------------------------------ */
/* decApplyRound -- apply pending rounding to a number                */
/*                                                                    */
/*   dn    is the number, with space for set->digits digits           */
/*   set   is the context [for size and rounding mode]                */
/*   residue indicates pending rounding, being any accumulated        */
/*         guard and sticky information.  It may be:                  */
/*         6-9: rounding digit is >5                                  */
/*         5:   rounding digit is exactly half-way                    */
/*         1-4: rounding digit is <5 and >0                           */
/*         0:   the coefficient is exact                              */
/*        -1:   as 1, but the hidden digits are subtractive, that     */
/*              is, of the opposite sign to dn.  In this case the     */
/*              coefficient must be non-0.  This case occurs when     */
/*              subtracting a small number (which can be reduced to   */
/*              a sticky bit); see decAddOp.                          */
/*   status is the status accumulator, as usual                       */
/*                                                                    */
/* This routine applies rounding while keeping the length of the      */
/* coefficient constant.  The exponent and status are unchanged       */
/* except if:                                                         */
/*                                                                    */
/*   -- the coefficient was increased and is all nines (in which      */
/*      case Overflow could occur, and is handled directly here so    */
/*      the caller does not need to re-test for overflow)             */
/*                                                                    */
/*   -- the coefficient was decreased and becomes all nines (in which */
/*      case Underflow could occur, and is also handled directly).    */
/*                                                                    */
/* All fields in dn are updated as required.                          */
/*                                                                    */
/* ------------------------------------------------------------------ */
static void decApplyRound(decNumber *dn, decContext *set, Int residue,
                          uInt *status) {
Din_Go(1943,2048);  Int  bump;                  /* 1 if coefficient needs to be incremented  */
                              /* -1 if coefficient needs to be decremented  */

  Din_Go(1945,2048);if (residue==0) {/*315*/Din_Go(1944,2048);return;/*316*/}     /* nothing to apply  */

  Din_Go(1946,2048);bump=0;                     /* assume a smooth ride  */

  /* now decide whether, and how, to round, depending on mode  */
  Din_Go(1985,2048);switch (set->round) {
    case DEC_ROUND_05UP: {    /* round zero or five up (for reround)  */
      /* This is the same as DEC_ROUND_DOWN unless there is a  */
      /* positive residue and the lsd of dn is 0 or 5, in which case  */
      /* it is bumped; when residue is <0, the number is therefore  */
      /* bumped down unless the final digit was 1 or 6 (in which  */
      /* case it is bumped down and then up -- a no-op)  */
Din_Go(1947,2048);      Int lsd5=*dn->lsu%5;     /* get lsd and quintate  */
      Din_Go(1951,2048);if (residue<0 && lsd5!=1) {/*317*/Din_Go(1948,2048);bump=-1;/*318*/}
       else {/*319*/Din_Go(1949,2048);if (residue>0 && lsd5==0) {/*321*/Din_Go(1950,2048);bump=1;/*322*/}/*320*/}
      /* [bump==1 could be applied directly; use common path for clarity]  */
      Din_Go(1952,2048);break;} /* r-05  */

    case DEC_ROUND_DOWN: {
      /* no change, except if negative residue  */
      Din_Go(1953,2048);if (residue<0) {/*323*/Din_Go(1954,2048);bump=-1;/*324*/}
      Din_Go(1955,2048);break;} /* r-d  */

    case DEC_ROUND_HALF_DOWN: {
      Din_Go(1956,2048);if (residue>5) {/*325*/Din_Go(1957,2048);bump=1;/*326*/}
      Din_Go(1958,2048);break;} /* r-h-d  */

    case DEC_ROUND_HALF_EVEN: {
      Din_Go(1959,2048);if (residue>5) {/*327*/Din_Go(1960,2048);bump=1;/*328*/}            /* >0.5 goes up  */
       else {/*329*/Din_Go(1961,2048);if (residue==5) {           /* exactly 0.5000...  */
        /* 0.5 goes up iff [new] lsd is odd  */
        Din_Go(1962,2048);if (*dn->lsu & 0x01) {/*331*/Din_Go(1963,2048);bump=1;/*332*/}
        ;/*330*/}}
      Din_Go(1964,2048);break;} /* r-h-e  */

    case DEC_ROUND_HALF_UP: {
      Din_Go(1965,2048);if (residue>=5) {/*333*/Din_Go(1966,2048);bump=1;/*334*/}
      Din_Go(1967,2048);break;} /* r-h-u  */

    case DEC_ROUND_UP: {
      Din_Go(1968,2048);if (residue>0) {/*335*/Din_Go(1969,2048);bump=1;/*336*/}
      Din_Go(1970,2048);break;} /* r-u  */

    case DEC_ROUND_CEILING: {
      /* same as _UP for positive numbers, and as _DOWN for negatives  */
      /* [negative residue cannot occur on 0]  */
      Din_Go(1971,2048);if (decNumberIsNegative(dn)) {
        Din_Go(1972,2048);if (residue<0) {/*337*/Din_Go(1973,2048);bump=-1;/*338*/}
        }
       else {
        Din_Go(1974,2048);if (residue>0) {/*339*/Din_Go(1975,2048);bump=1;/*340*/}
        }
      Din_Go(1976,2048);break;} /* r-c  */

    case DEC_ROUND_FLOOR: {
      /* same as _UP for negative numbers, and as _DOWN for positive  */
      /* [negative residue cannot occur on 0]  */
      Din_Go(1977,2048);if (!decNumberIsNegative(dn)) {
        Din_Go(1978,2048);if (residue<0) {/*341*/Din_Go(1979,2048);bump=-1;/*342*/}
        }
       else {
        Din_Go(1980,2048);if (residue>0) {/*343*/Din_Go(1981,2048);bump=1;/*344*/}
        }
      Din_Go(1982,2048);break;} /* r-f  */

    default: {      /* e.g., DEC_ROUND_MAX  */
      Din_Go(1983,2048);*status|=DEC_Invalid_context;
      #if DECTRACE || (DECCHECK && DECVERB)
      printf("Unknown rounding mode: %d\n", set->round);
      #endif
      Din_Go(1984,2048);break;}
    } /* switch  */

  /* now bump the number, up or down, if need be  */
  Din_Go(1987,2048);if (bump==0) {/*345*/Din_Go(1986,2048);return;/*346*/}                       /* no action required  */

  /* Simply use decUnitAddSub unless bumping up and the number is  */
  /* all nines.  In this special case set to 100... explicitly  */
  /* and adjust the exponent by one (as otherwise could overflow  */
  /* the array)  */
  /* Similarly handle all-nines result if bumping down.  */
  Din_Go(2021,2048);if (bump>0) {
    Unit *up;                                /* work  */
Din_Go(1988,2048);    uInt count=dn->digits;                   /* digits to be checked  */
    Din_Go(2002,2048);for (up=dn->lsu; ; up++) {
      Din_Go(1989,2048);if (count<=DECDPUN) {
        /* this is the last Unit (the msu)  */
        Din_Go(1990,2048);if (*up!=powers[count]-1) {/*347*/Din_Go(1991,2048);break;/*348*/}     /* not still 9s  */
        /* here if it, too, is all nines  */
        Din_Go(1992,2048);*up=(Unit)powers[count-1];           /* here 999 -> 100 etc.  */
        Din_Go(1994,2048);for (up=up-1; up>=dn->lsu; up--) {/*349*/Din_Go(1993,2048);*up=0;/*350*/} /* others all to 0  */
        Din_Go(1995,2048);dn->exponent++;                      /* and bump exponent  */
        /* [which, very rarely, could cause Overflow...]  */
        Din_Go(1997,2048);if ((dn->exponent+dn->digits)>set->emax+1) {
          Din_Go(1996,2048);decSetOverflow(dn, set, status);
          }
        Din_Go(1998,2048);return;                              /* done  */
        }
      /* a full unit to check, with more to come  */
      Din_Go(2000,2048);if (*up!=DECDPUNMAX) {/*351*/Din_Go(1999,2048);break;/*352*/}            /* not still 9s  */
      Din_Go(2001,2048);count-=DECDPUN;
      } /* up  */
    } /* bump>0  */
   else {                                    /* -1  */
    /* here checking for a pre-bump of 1000... (leading 1, all  */
    /* other digits zero)  */
    Unit *up, *sup;                          /* work  */
Din_Go(2003,2048);    uInt count=dn->digits;                   /* digits to be checked  */
    Din_Go(2020,2048);for (up=dn->lsu; ; up++) {
      Din_Go(2004,2048);if (count<=DECDPUN) {
        /* this is the last Unit (the msu)  */
        Din_Go(2005,2048);if (*up!=powers[count-1]) {/*353*/Din_Go(2006,2048);break;/*354*/}     /* not 100..  */
        /* here if have the 1000... case  */
        Din_Go(2007,2048);sup=up;                              /* save msu pointer  */
        *up=(Unit)powers[count]-1;           /* here 100 in msu -> 999  */
        /* others all to all-nines, too  */
        Din_Go(2009,2048);for (up=up-1; up>=dn->lsu; up--) {/*355*/Din_Go(2008,2048);*up=(Unit)powers[DECDPUN]-1;/*356*/}
        Din_Go(2010,2048);dn->exponent--;                      /* and bump exponent  */

        /* iff the number was at the subnormal boundary (exponent=etiny)  */
        /* then the exponent is now out of range, so it will in fact get  */
        /* clamped to etiny and the final 9 dropped.  */
        /* printf(">> emin=%d exp=%d sdig=%d\n", set->emin,  */
        /*        dn->exponent, set->digits);  */
        Din_Go(2015,2048);if (dn->exponent+1==set->emin-set->digits+1) {
          Din_Go(2011,2048);if (count==1 && dn->digits==1) {/*357*/Din_Go(2012,2048);*sup=0;/*358*/}  /* here 9 -> 0[.9]  */
           else {
            Din_Go(2013,2048);*sup=(Unit)powers[count-1]-1;    /* here 999.. in msu -> 99..  */
            dn->digits--;
            }
          Din_Go(2014,2048);dn->exponent++;
          *status|=DEC_Underflow | DEC_Subnormal | DEC_Inexact | DEC_Rounded;
          }
        Din_Go(2016,2048);return;                              /* done  */
        }

      /* a full unit to check, with more to come  */
      Din_Go(2018,2048);if (*up!=0) {/*359*/Din_Go(2017,2048);break;/*360*/}                     /* not still 0s  */
      Din_Go(2019,2048);count-=DECDPUN;
      } /* up  */

    } /* bump<0  */

  /* Actual bump needed.  Do it.  */
  Din_Go(2022,2048);decUnitAddSub(dn->lsu, D2U(dn->digits), uarrone, 1, 0, dn->lsu, bump);
  Din_Go(2023,2048);} /* decApplyRound  */

#if DECSUBSET
/* ------------------------------------------------------------------ */
/* decFinish -- finish processing a number                            */
/*                                                                    */
/*   dn is the number                                                 */
/*   set is the context                                               */
/*   residue is the rounding accumulator (as in decApplyRound)        */
/*   status is the accumulator                                        */
/*                                                                    */
/* This finishes off the current number by:                           */
/*    1. If not extended:                                             */
/*       a. Converting a zero result to clean '0'                     */
/*       b. Reducing positive exponents to 0, if would fit in digits  */
/*    2. Checking for overflow and subnormals (always)                */
/* Note this is just Finalize when no subset arithmetic.              */
/* All fields are updated as required.                                */
/* ------------------------------------------------------------------ */
static void decFinish(decNumber *dn, decContext *set, Int *residue,
                      uInt *status) {
  if (!set->extended) {
    if ISZERO(dn) {                /* value is zero  */
      dn->exponent=0;              /* clean exponent ..  */
      dn->bits=0;                  /* .. and sign  */
      return;                      /* no error possible  */
      }
    if (dn->exponent>=0) {         /* non-negative exponent  */
      /* >0; reduce to integer if possible  */
      if (set->digits >= (dn->exponent+dn->digits)) {
        dn->digits=decShiftToMost(dn->lsu, dn->digits, dn->exponent);
        dn->exponent=0;
        }
      }
    } /* !extended  */

  decFinalize(dn, set, residue, status);
  } /* decFinish  */
#endif

/* ------------------------------------------------------------------ */
/* decFinalize -- final check, clamp, and round of a number           */
/*                                                                    */
/*   dn is the number                                                 */
/*   set is the context                                               */
/*   residue is the rounding accumulator (as in decApplyRound)        */
/*   status is the status accumulator                                 */
/*                                                                    */
/* This finishes off the current number by checking for subnormal     */
/* results, applying any pending rounding, checking for overflow,     */
/* and applying any clamping.                                         */
/* Underflow and overflow conditions are raised as appropriate.       */
/* All fields are updated as required.                                */
/* ------------------------------------------------------------------ */
static void decFinalize(decNumber *dn, decContext *set, Int *residue,
                        uInt *status) {
  Int shift;                            /* shift needed if clamping  */
  Int tinyexp=set->emin-dn->digits+1;   /* precalculate subnormal boundary  */

  /* Must be careful, here, when checking the exponent as the  */
  /* adjusted exponent could overflow 31 bits [because it may already  */
  /* be up to twice the expected].  */

  /* First test for subnormal.  This must be done before any final  */
  /* round as the result could be rounded to Nmin or 0.  */
  Din_Go(2035,2048);if (dn->exponent<=tinyexp) {          /* prefilter  */
    Int comp;
    Din_Go(2024,2048);decNumber nmin;
    /* A very nasty case here is dn == Nmin and residue<0  */
    Din_Go(2027,2048);if (dn->exponent<tinyexp) {
      /* Go handle subnormals; this will apply round if needed.  */
      Din_Go(2025,2048);decSetSubnormal(dn, set, residue, status);
      Din_Go(2026,2048);return;
      }
    /* Equals case: only subnormal if dn=Nmin and negative residue  */
    uprv_decNumberZero(&nmin);
    Din_Go(2028,2048);nmin.lsu[0]=1;
    nmin.exponent=set->emin;
    comp=decCompare(dn, &nmin, 1);                /* (signless compare)  */
    Din_Go(2031,2048);if (comp==BADINT) {                           /* oops  */
      Din_Go(2029,2048);*status|=DEC_Insufficient_storage;          /* abandon...  */
      Din_Go(2030,2048);return;
      }
    Din_Go(2034,2048);if (*residue<0 && comp==0) {                  /* neg residue and dn==Nmin  */
      Din_Go(2032,2048);decApplyRound(dn, set, *residue, status);   /* might force down  */
      decSetSubnormal(dn, set, residue, status);
      Din_Go(2033,2048);return;
      }
    }

  /* now apply any pending round (this could raise overflow).  */
  Din_Go(2037,2048);if (*residue!=0) {/*533*/Din_Go(2036,2048);decApplyRound(dn, set, *residue, status);/*534*/}

  /* Check for overflow [redundant in the 'rare' case] or clamp  */
  Din_Go(2039,2048);if (dn->exponent<=set->emax-set->digits+1) {/*535*/Din_Go(2038,2048);return;/*536*/}   /* neither needed  */


  /* here when might have an overflow or clamp to do  */
  Din_Go(2042,2048);if (dn->exponent>set->emax-dn->digits+1) {           /* too big  */
    Din_Go(2040,2048);decSetOverflow(dn, set, status);
    Din_Go(2041,2048);return;
    }
  /* here when the result is normal but in clamp range  */
  Din_Go(2044,2048);if (!set->clamp) {/*537*/Din_Go(2043,2048);return;/*538*/}

  /* here when need to apply the IEEE exponent clamp (fold-down)  */
  Din_Go(2045,2048);shift=dn->exponent-(set->emax-set->digits+1);

  /* shift coefficient (if non-zero)  */
  Din_Go(2047,2048);if (!ISZERO(dn)) {
    Din_Go(2046,2048);dn->digits=decShiftToMost(dn->lsu, dn->digits, shift);
    }
  Din_Go(2048,2048);dn->exponent-=shift;   /* adjust the exponent to match  */
  *status|=DEC_Clamped;  /* and record the dirty deed  */
  Din_Go(2049,2048);return;
  } /* decFinalize  */

/* ------------------------------------------------------------------ */
/* decSetOverflow -- set number to proper overflow value              */
/*                                                                    */
/*   dn is the number (used for sign [only] and result)               */
/*   set is the context [used for the rounding mode, etc.]            */
/*   status contains the current status to be updated                 */
/*                                                                    */
/* This sets the sign of a number and sets its value to either        */
/* Infinity or the maximum finite value, depending on the sign of     */
/* dn and the rounding mode, following IEEE 754 rules.                */
/* ------------------------------------------------------------------ */
static void decSetOverflow(decNumber *dn, decContext *set, uInt *status) {
  Flag needmax=0;                  /* result is maximum finite value  */
  uByte sign=dn->bits&DECNEG;      /* clean and save sign bit  */

  Din_Go(2056,2048);if (ISZERO(dn)) {                /* zero does not overflow magnitude  */
Din_Go(2050,2048);    Int emax=set->emax;                      /* limit value  */
    Din_Go(2052,2048);if (set->clamp) {/*651*/Din_Go(2051,2048);emax-=set->digits-1;/*652*/}     /* lower if clamping  */
    Din_Go(2054,2048);if (dn->exponent>emax) {                 /* clamp required  */
      Din_Go(2053,2048);dn->exponent=emax;
      *status|=DEC_Clamped;
      }
    Din_Go(2055,2048);return;
    }

  uprv_decNumberZero(dn);
  Din_Go(2068,2048);switch (set->round) {
    case DEC_ROUND_DOWN: {
      Din_Go(2057,2048);needmax=1;                   /* never Infinity  */
      Din_Go(2058,2048);break;} /* r-d  */
    case DEC_ROUND_05UP: {
      Din_Go(2059,2048);needmax=1;                   /* never Infinity  */
      Din_Go(2060,2048);break;} /* r-05  */
    case DEC_ROUND_CEILING: {
      Din_Go(2061,2048);if (sign) {/*653*/Din_Go(2062,2048);needmax=1;/*654*/}         /* Infinity if non-negative  */
      Din_Go(2063,2048);break;} /* r-c  */
    case DEC_ROUND_FLOOR: {
      Din_Go(2064,2048);if (!sign) {/*655*/Din_Go(2065,2048);needmax=1;/*656*/}        /* Infinity if negative  */
      Din_Go(2066,2048);break;} /* r-f  */
    default: Din_Go(2067,2048);break;                /* Infinity in all other cases  */
    }
  Din_Go(2070,2048);if (needmax) {
    Din_Go(2069,2048);decSetMaxValue(dn, set);
    dn->bits=sign;                 /* set sign  */
    }
   else dn->bits=sign|DECINF;      /* Value is +/-Infinity  */
  Din_Go(2071,2048);*status|=DEC_Overflow | DEC_Inexact | DEC_Rounded;
  Din_Go(2072,2048);} /* decSetOverflow  */

/* ------------------------------------------------------------------ */
/* decSetMaxValue -- set number to +Nmax (maximum normal value)       */
/*                                                                    */
/*   dn is the number to set                                          */
/*   set is the context [used for digits and emax]                    */
/*                                                                    */
/* This sets the number to the maximum positive value.                */
/* ------------------------------------------------------------------ */
static void decSetMaxValue(decNumber *dn, decContext *set) {
Din_Go(2073,2048);  Unit *up;                        /* work  */
  Int count=set->digits;           /* nines to add  */
  dn->digits=count;
  /* fill in all nines to set maximum value  */
  Din_Go(2078,2048);for (up=dn->lsu; ; up++) {
    Din_Go(2074,2048);if (count>DECDPUN) *up=DECDPUNMAX;  /* unit full o'nines  */
     else {                             /* this is the msu  */
      Din_Go(2075,2048);*up=(Unit)(powers[count]-1);
      Din_Go(2076,2048);break;
      }
    Din_Go(2077,2048);count-=DECDPUN;                /* filled those digits  */
    } /* up  */
  Din_Go(2079,2048);dn->bits=0;                      /* + sign  */
  dn->exponent=set->emax-set->digits+1;
  Din_Go(2080,2048);} /* decSetMaxValue  */

/* ------------------------------------------------------------------ */
/* decSetSubnormal -- process value whose exponent is <Emin           */
/*                                                                    */
/*   dn is the number (used as input as well as output; it may have   */
/*         an allowed subnormal value, which may need to be rounded)  */
/*   set is the context [used for the rounding mode]                  */
/*   residue is any pending residue                                   */
/*   status contains the current status to be updated                 */
/*                                                                    */
/* If subset mode, set result to zero and set Underflow flags.        */
/*                                                                    */
/* Value may be zero with a low exponent; this does not set Subnormal */
/* but the exponent will be clamped to Etiny.                         */
/*                                                                    */
/* Otherwise ensure exponent is not out of range, and round as        */
/* necessary.  Underflow is set if the result is Inexact.             */
/* ------------------------------------------------------------------ */
static void decSetSubnormal(decNumber *dn, decContext *set, Int *residue,
                            uInt *status) {
  Din_Go(2081,2048);decContext workset;         /* work  */
  Int        etiny, adjust;   /* ..  */

  #if DECSUBSET
  /* simple set to zero and 'hard underflow' for subset  */
  if (!set->extended) {
    uprv_decNumberZero(dn);
    /* always full overflow  */
    *status|=DEC_Underflow | DEC_Subnormal | DEC_Inexact | DEC_Rounded;
    return;
    }
  #endif

  /* Full arithmetic -- allow subnormals, rounded to minimum exponent  */
  /* (Etiny) if needed  */
  etiny=set->emin-(set->digits-1);      /* smallest allowed exponent  */

  Din_Go(2085,2048);if ISZERO(dn) {                       /* value is zero  */
    /* residue can never be non-zero here  */
    #if DECCHECK
      if (*residue!=0) {
        printf("++ Subnormal 0 residue %ld\n", (LI)*residue);
        *status|=DEC_Invalid_operation;
        }
    #endif
    Din_Go(2082,2048);if (dn->exponent<etiny) {           /* clamp required  */
      Din_Go(2083,2048);dn->exponent=etiny;
      *status|=DEC_Clamped;
      }
    Din_Go(2084,2048);return;
    }

  Din_Go(2086,2048);*status|=DEC_Subnormal;               /* have a non-zero subnormal  */
  adjust=etiny-dn->exponent;            /* calculate digits to remove  */
  Din_Go(2089,2048);if (adjust<=0) {                      /* not out of range; unrounded  */
    /* residue can never be non-zero here, except in the Nmin-residue  */
    /* case (which is a subnormal result), so can take fast-path here  */
    /* it may already be inexact (from setting the coefficient)  */
    Din_Go(2087,2048);if (*status&DEC_Inexact) *status|=DEC_Underflow;
    Din_Go(2088,2048);return;
    }

  /* adjust>0, so need to rescale the result so exponent becomes Etiny  */
  /* [this code is similar to that in rescale]  */
  Din_Go(2090,2048);workset=*set;                         /* clone rounding, etc.  */
  workset.digits=dn->digits-adjust;     /* set requested length  */
  workset.emin-=adjust;                 /* and adjust emin to match  */
  /* [note that the latter can be <1, here, similar to Rescale case]  */
  decSetCoeff(dn, &workset, dn->lsu, dn->digits, residue, status);
  decApplyRound(dn, &workset, *residue, status);

  /* Use 754 default rule: Underflow is set iff Inexact  */
  /* [independent of whether trapped]  */
  Din_Go(2091,2048);if (*status&DEC_Inexact) *status|=DEC_Underflow;

  /* if rounded up a 999s case, exponent will be off by one; adjust  */
  /* back if so [it will fit, because it was shortened earlier]  */
  Din_Go(2093,2048);if (dn->exponent>etiny) {
    Din_Go(2092,2048);dn->digits=decShiftToMost(dn->lsu, dn->digits, 1);
    dn->exponent--;                     /* (re)adjust the exponent.  */
    }

  /* if rounded to zero, it is by definition clamped...  */
  Din_Go(2094,2048);if (ISZERO(dn)) *status|=DEC_Clamped;
  Din_Go(2095,2048);} /* decSetSubnormal  */

/* ------------------------------------------------------------------ */
/* decCheckMath - check entry conditions for a math function          */
/*                                                                    */
/*   This checks the context and the operand                          */
/*                                                                    */
/*   rhs is the operand to check                                      */
/*   set is the context to check                                      */
/*   status is unchanged if both are good                             */
/*                                                                    */
/* returns non-zero if status is changed, 0 otherwise                 */
/*                                                                    */
/* Restrictions enforced:                                             */
/*                                                                    */
/*   digits, emax, and -emin in the context must be less than         */
/*   DEC_MAX_MATH (999999), and A must be within these bounds if      */
/*   non-zero.  Invalid_operation is set in the status if a           */
/*   restriction is violated.                                         */
/* ------------------------------------------------------------------ */
static uInt decCheckMath(const decNumber *rhs, decContext *set,
                         uInt *status) {
Din_Go(2096,2048);  uInt save=*status;                         /* record  */
  Din_Go(2097,2048);if (set->digits>DEC_MAX_MATH
   || set->emax>DEC_MAX_MATH
   || -set->emin>DEC_MAX_MATH) *status|=DEC_Invalid_context;
   else if ((rhs->digits>DEC_MAX_MATH
     || rhs->exponent+rhs->digits>DEC_MAX_MATH+1
     || rhs->exponent+rhs->digits<2*(1-DEC_MAX_MATH))
     && !ISZERO(rhs)) *status|=DEC_Invalid_operation;
  {uint32_t  ReplaceReturn9 = (*status!=save); Din_Go(2098,2048); return ReplaceReturn9;}
  } /* decCheckMath  */

/* ------------------------------------------------------------------ */
/* decGetInt -- get integer from a number                             */
/*                                                                    */
/*   dn is the number [which will not be altered]                     */
/*                                                                    */
/*   returns one of:                                                  */
/*     BADINT if there is a non-zero fraction                         */
/*     the converted integer                                          */
/*     BIGEVEN if the integer is even and magnitude > 2*10**9         */
/*     BIGODD  if the integer is odd  and magnitude > 2*10**9         */
/*                                                                    */
/* This checks and gets a whole number from the input decNumber.      */
/* The sign can be determined from dn by the caller when BIGEVEN or   */
/* BIGODD is returned.                                                */
/* ------------------------------------------------------------------ */
static Int decGetInt(const decNumber *dn) {
Din_Go(2099,2048);  Int  theInt;                          /* result accumulator  */
  const Unit *up;                       /* work  */
  Int  got;                             /* digits (real or not) processed  */
  Int  ilength=dn->digits+dn->exponent; /* integral length  */
  Flag neg=decNumberIsNegative(dn);     /* 1 if -ve  */

  /* The number must be an integer that fits in 10 digits  */
  /* Assert, here, that 10 is enough for any rescale Etiny  */
  #if DEC_MAX_EMAX > 999999999
    #error GetInt may need updating [for Emax]
  #endif
  #if DEC_MIN_EMIN < -999999999
    #error GetInt may need updating [for Emin]
  #endif
  Din_Go(2101,2048);if (ISZERO(dn)) {/*541*/{int32_t  ReplaceReturn8 = 0; Din_Go(2100,2048); return ReplaceReturn8;}/*542*/}             /* zeros are OK, with any exponent  */

  Din_Go(2102,2048);up=dn->lsu;                           /* ready for lsu  */
  theInt=0;                             /* ready to accumulate  */
  Din_Go(2111,2048);if (dn->exponent>=0) {                /* relatively easy  */
    /* no fractional part [usual]; allow for positive exponent  */
    Din_Go(2103,2048);got=dn->exponent;
    }
   else { /* -ve exponent; some fractional part to check and discard  */
Din_Go(2104,2048);    Int count=-dn->exponent;            /* digits to discard  */
    /* spin up whole units until reach the Unit with the unit digit  */
    Din_Go(2106,2048);for (; count>=DECDPUN; up++) {
      if (*up!=0) return BADINT;        /* non-zero Unit to discard  */
      Din_Go(2105,2048);count-=DECDPUN;
      }
    Din_Go(2110,2048);if (count==0) {/*545*/Din_Go(2107,2048);got=0;/*546*/}                /* [a multiple of DECDPUN]  */
     else {                             /* [not multiple of DECDPUN]  */
      Int rem;                          /* work  */
      /* slice off fraction digits and check for non-zero  */
      #if DECDPUN<=4
        Din_Go(2108,2048);theInt=QUOT10(*up, count);
        rem=*up-theInt*powers[count];
      #else
        rem=*up%powers[count];          /* slice off discards  */
        theInt=*up/powers[count];
      #endif
      if (rem!=0) return BADINT;        /* non-zero fraction  */
      /* it looks good  */
      Din_Go(2109,2048);got=DECDPUN-count;                /* number of digits so far  */
      up++;                             /* ready for next  */
      }
    }
  /* now it's known there's no fractional part  */

  /* tricky code now, to accumulate up to 9.3 digits  */
  Din_Go(2113,2048);if (got==0) {Din_Go(2112,2048);theInt=*up; got+=DECDPUN; up++;} /* ensure lsu is there  */

  Din_Go(2126,2048);if (ilength<11) {
Din_Go(2114,2048);    Int save=theInt;
    /* collect any remaining unit(s)  */
    Din_Go(2116,2048);for (; got<ilength; up++) {
      Din_Go(2115,2048);theInt+=*up*powers[got];
      got+=DECDPUN;
      }
    Din_Go(2125,2048);if (ilength==10) {                  /* need to check for wrap  */
      Din_Go(2117,2048);if (theInt/(Int)powers[got-DECDPUN]!=(Int)*(up-1)) {/*549*/Din_Go(2118,2048);ilength=11;/*550*/}
         /* [that test also disallows the BADINT result case]  */
       else {/*551*/Din_Go(2119,2048);if (neg && theInt>1999999997) {/*553*/Din_Go(2120,2048);ilength=11;/*554*/}
       else {/*555*/Din_Go(2121,2048);if (!neg && theInt>999999999) {/*557*/Din_Go(2122,2048);ilength=11;/*558*/}/*556*/}/*552*/}
      Din_Go(2124,2048);if (ilength==11) {/*559*/Din_Go(2123,2048);theInt=save;/*560*/}     /* restore correct low bit  */
      }
    }

  Din_Go(2128,2048);if (ilength>10) {                     /* too big  */
    if (theInt&1) return BIGODD;        /* bottom bit 1  */
    {int32_t  ReplaceReturn7 = BIGEVEN; Din_Go(2127,2048); return ReplaceReturn7;}                     /* bottom bit 0  */
    }

  Din_Go(2130,2048);if (neg) {/*563*/Din_Go(2129,2048);theInt=-theInt;/*564*/}              /* apply sign  */
  {int32_t  ReplaceReturn6 = theInt; Din_Go(2131,2048); return ReplaceReturn6;}
  } /* decGetInt  */

/* ------------------------------------------------------------------ */
/* decDecap -- decapitate the coefficient of a number                 */
/*                                                                    */
/*   dn   is the number to be decapitated                             */
/*   drop is the number of digits to be removed from the left of dn;  */
/*     this must be <= dn->digits (if equal, the coefficient is       */
/*     set to 0)                                                      */
/*                                                                    */
/* Returns dn; dn->digits will be <= the initial digits less drop     */
/* (after removing drop digits there may be leading zero digits       */
/* which will also be removed).  Only dn->lsu and dn->digits change.  */
/* ------------------------------------------------------------------ */
static decNumber *decDecap(decNumber *dn, Int drop) {
Din_Go(2132,2048);  Unit *msu;                            /* -> target cut point  */
  Int cut;                              /* work  */
  Din_Go(2135,2048);if (drop>=dn->digits) {               /* losing the whole thing  */
    #if DECCHECK
    if (drop>dn->digits)
      printf("decDecap called with drop>digits [%ld>%ld]\n",
             (LI)drop, (LI)dn->digits);
    #endif
    Din_Go(2133,2048);dn->lsu[0]=0;
    dn->digits=1;
    {decNumber * ReplaceReturn5 = dn; Din_Go(2134,2048); return ReplaceReturn5;}
    }
  Din_Go(2136,2048);msu=dn->lsu+D2U(dn->digits-drop)-1;   /* -> likely msu  */
  cut=MSUDIGITS(dn->digits-drop);       /* digits to be in use in msu  */
  Din_Go(2138,2048);if (cut!=DECDPUN) {/*449*/Din_Go(2137,2048);*msu%=powers[cut];/*450*/}  /* clear left digits  */
  /* that may have left leading zero digits, so do a proper count...  */
  Din_Go(2139,2048);dn->digits=decGetDigits(dn->lsu, msu-dn->lsu+1);
  {decNumber * ReplaceReturn4 = dn; Din_Go(2140,2048); return ReplaceReturn4;}
  } /* decDecap  */

/* ------------------------------------------------------------------ */
/* decBiStr -- compare string with pairwise options                   */
/*                                                                    */
/*   targ is the string to compare                                    */
/*   str1 is one of the strings to compare against (length may be 0)  */
/*   str2 is the other; it must be the same length as str1            */
/*                                                                    */
/*   returns 1 if strings compare equal, (that is, it is the same     */
/*   length as str1 and str2, and each character of targ is in either */
/*   str1 or str2 in the corresponding position), or 0 otherwise      */
/*                                                                    */
/* This is used for generic caseless compare, including the awkward   */
/* case of the Turkish dotted and dotless Is.  Use as (for example):  */
/*   if (decBiStr(test, "mike", "MIKE")) ...                          */
/* ------------------------------------------------------------------ */
static Flag decBiStr(const char *targ, const char *str1, const char *str2) {
  Din_Go(2141,2048);for (;;targ++, str1++, str2++) {
    Din_Go(2142,2048);if (*targ!=*str1 && *targ!=*str2) {/*311*/{uint8_t  ReplaceReturn3 = 0; Din_Go(2143,2048); return ReplaceReturn3;}/*312*/}
    /* *targ has a match in one (or both, if terminator)  */
    Din_Go(2145,2048);if (*targ=='\0') {/*313*/Din_Go(2144,2048);break;/*314*/}
    } /* forever  */
  {uint8_t  ReplaceReturn2 = 1; Din_Go(2146,2048); return ReplaceReturn2;}
  } /* decBiStr  */

/* ------------------------------------------------------------------ */
/* decNaNs -- handle NaN operand or operands                          */
/*                                                                    */
/*   res     is the result number                                     */
/*   lhs     is the first operand                                     */
/*   rhs     is the second operand, or NULL if none                   */
/*   context is used to limit payload length                          */
/*   status  contains the current status                              */
/*   returns res in case convenient                                   */
/*                                                                    */
/* Called when one or both operands is a NaN, and propagates the      */
/* appropriate result to res.  When an sNaN is found, it is changed   */
/* to a qNaN and Invalid operation is set.                            */
/* ------------------------------------------------------------------ */
static decNumber * decNaNs(decNumber *res, const decNumber *lhs,
                           const decNumber *rhs, decContext *set,
                           uInt *status) {
  /* This decision tree ends up with LHS being the source pointer,  */
  /* and status updated if need be  */
  Din_Go(2147,2048);if (lhs->bits & DECSNAN)
    {/*599*/Din_Go(2148,2048);*status|=DEC_Invalid_operation | DEC_sNaN;/*600*/}
   else {/*601*/Din_Go(2149,2048);if (rhs==NULL);
   else {/*605*/Din_Go(2150,2048);if (rhs->bits & DECSNAN) {
    Din_Go(2151,2048);lhs=rhs;
    *status|=DEC_Invalid_operation | DEC_sNaN;
    }
   else {/*607*/Din_Go(2152,2048);if (lhs->bits & DECNAN);
   else {/*611*/Din_Go(2153,2048);lhs=rhs;/*612*/}/*608*/}/*606*/}/*602*/}

  /* propagate the payload  */
  Din_Go(2160,2048);if (lhs->digits<=set->digits) uprv_decNumberCopy(res, lhs); /* easy  */
   else { /* too long  */
    Din_Go(2154,2048);const Unit *ul;
    Unit *ur, *uresp1;
    /* copy safe number of units, then decapitate  */
    res->bits=lhs->bits;                /* need sign etc.  */
    uresp1=res->lsu+D2U(set->digits);
    Din_Go(2156,2048);for (ur=res->lsu, ul=lhs->lsu; ur<uresp1; ur++, ul++) {/*613*/Din_Go(2155,2048);*ur=*ul;/*614*/}
    Din_Go(2157,2048);res->digits=D2U(set->digits)*DECDPUN;
    /* maybe still too long  */
    Din_Go(2159,2048);if (res->digits>set->digits) {/*615*/Din_Go(2158,2048);decDecap(res, res->digits-set->digits);/*616*/}
    }

  Din_Go(2161,2048);res->bits&=~DECSNAN;        /* convert any sNaN to NaN, while  */
  res->bits|=DECNAN;          /* .. preserving sign  */
  res->exponent=0;            /* clean exponent  */
                              /* [coefficient was copied/decapitated]  */
  {decNumber * ReplaceReturn1 = res; Din_Go(2162,2048); return ReplaceReturn1;}
  } /* decNaNs  */

/* ------------------------------------------------------------------ */
/* decStatus -- apply non-zero status                                 */
/*                                                                    */
/*   dn     is the number to set if error                             */
/*   status contains the current status (not yet in context)          */
/*   set    is the context                                            */
/*                                                                    */
/* If the status is an error status, the number is set to a NaN,      */
/* unless the error was an overflow, divide-by-zero, or underflow,    */
/* in which case the number will have already been set.               */
/*                                                                    */
/* The context status is then updated with the new status.  Note that */
/* this may raise a signal, so control may never return from this     */
/* routine (hence resources must be recovered before it is called).   */
/* ------------------------------------------------------------------ */
static void decStatus(decNumber *dn, uInt status, decContext *set) {
  Din_Go(2163,2048);if (status & DEC_NaNs) {              /* error status -> NaN  */
    /* if cause was an sNaN, clear and propagate [NaN is already set up]  */
    Din_Go(2164,2048);if (status & DEC_sNaN) {/*671*/Din_Go(2165,2048);status&=~DEC_sNaN;/*672*/}
     else {
      uprv_decNumberZero(dn);                /* other error: clean throughout  */
      Din_Go(2166,2048);dn->bits=DECNAN;                  /* and make a quiet NaN  */
      }
    }
  uprv_decContextSetStatus(set, status);     /* [may not return]  */
  Din_Go(2167,2048);return;
  } /* decStatus  */

/* ------------------------------------------------------------------ */
/* decGetDigits -- count digits in a Units array                      */
/*                                                                    */
/*   uar is the Unit array holding the number (this is often an       */
/*          accumulator of some sort)                                 */
/*   len is the length of the array in units [>=1]                    */
/*                                                                    */
/*   returns the number of (significant) digits in the array          */
/*                                                                    */
/* All leading zeros are excluded, except the last if the array has   */
/* only zero Units.                                                   */
/* ------------------------------------------------------------------ */
/* This may be called twice during some operations.  */
static Int decGetDigits(Unit *uar, Int len) {
Din_Go(2168,2048);  Unit *up=uar+(len-1);            /* -> msu  */
  Int  digits=(len-1)*DECDPUN+1;   /* possible digits excluding msu  */
  #if DECDPUN>4
  uInt const *pow;                 /* work  */
  #endif
                                   /* (at least 1 in final msu)  */
  #if DECCHECK
  if (len<1) printf("decGetDigits called with len<1 [%ld]\n", (LI)len);
  #endif

  Din_Go(2175,2048);for (; up>=uar; up--) {
    Din_Go(2169,2048);if (*up==0) {                  /* unit is all 0s  */
      Din_Go(2170,2048);if (digits==1) {/*539*/Din_Go(2171,2048);break;/*540*/}        /* a zero has one digit  */
      Din_Go(2172,2048);digits-=DECDPUN;             /* adjust for 0 unit  */
      Din_Go(2173,2048);continue;}
    /* found the first (most significant) non-zero Unit  */
    #if DECDPUN>1                  /* not done yet  */
    if (*up<10) break;             /* is 1-9  */
    digits++;
    #if DECDPUN>2                  /* not done yet  */
    if (*up<100) break;            /* is 10-99  */
    digits++;
    #if DECDPUN>3                  /* not done yet  */
    if (*up<1000) break;           /* is 100-999  */
    digits++;
    #if DECDPUN>4                  /* count the rest ...  */
    for (pow=&powers[4]; *up>=*pow; pow++) digits++;
    #endif
    #endif
    #endif
    #endif
    Din_Go(2174,2048);break;
    } /* up  */
  {int32_t  ReplaceReturn0 = digits; Din_Go(2176,2048); return ReplaceReturn0;}
  } /* decGetDigits  */

#if DECTRACE | DECCHECK
/* ------------------------------------------------------------------ */
/* decNumberShow -- display a number [debug aid]                      */
/*   dn is the number to show                                         */
/*                                                                    */
/* Shows: sign, exponent, coefficient (msu first), digits             */
/*    or: sign, special-value                                         */
/* ------------------------------------------------------------------ */
/* this is public so other modules can use it  */
void uprv_decNumberShow(const decNumber *dn) {
  const Unit *up;                  /* work  */
  uInt u, d;                       /* ..  */
  Int cut;                         /* ..  */
  char isign='+';                  /* main sign  */
  if (dn==NULL) {
    printf("NULL\n");
    return;}
  if (decNumberIsNegative(dn)) isign='-';
  printf(" >> %c ", isign);
  if (dn->bits&DECSPECIAL) {       /* Is a special value  */
    if (decNumberIsInfinite(dn)) printf("Infinity");
     else {                                  /* a NaN  */
      if (dn->bits&DECSNAN) printf("sNaN");  /* signalling NaN  */
       else printf("NaN");
      }
    /* if coefficient and exponent are 0, no more to do  */
    if (dn->exponent==0 && dn->digits==1 && *dn->lsu==0) {
      printf("\n");
      return;}
    /* drop through to report other information  */
    printf(" ");
    }

  /* now carefully display the coefficient  */
  up=dn->lsu+D2U(dn->digits)-1;         /* msu  */
  printf("%ld", (LI)*up);
  for (up=up-1; up>=dn->lsu; up--) {
    u=*up;
    printf(":");
    for (cut=DECDPUN-1; cut>=0; cut--) {
      d=u/powers[cut];
      u-=d*powers[cut];
      printf("%ld", (LI)d);
      } /* cut  */
    } /* up  */
  if (dn->exponent!=0) {
    char esign='+';
    if (dn->exponent<0) esign='-';
    printf(" E%c%ld", esign, (LI)abs(dn->exponent));
    }
  printf(" [%ld]\n", (LI)dn->digits);
  } /* decNumberShow  */
#endif

#if DECTRACE || DECCHECK
/* ------------------------------------------------------------------ */
/* decDumpAr -- display a unit array [debug/check aid]                */
/*   name is a single-character tag name                              */
/*   ar   is the array to display                                     */
/*   len  is the length of the array in Units                         */
/* ------------------------------------------------------------------ */
static void decDumpAr(char name, const Unit *ar, Int len) {
  Int i;
  const char *spec;
  #if DECDPUN==9
    spec="%09d ";
  #elif DECDPUN==8
    spec="%08d ";
  #elif DECDPUN==7
    spec="%07d ";
  #elif DECDPUN==6
    spec="%06d ";
  #elif DECDPUN==5
    spec="%05d ";
  #elif DECDPUN==4
    spec="%04d ";
  #elif DECDPUN==3
    spec="%03d ";
  #elif DECDPUN==2
    spec="%02d ";
  #else
    spec="%d ";
  #endif
  printf("  :%c: ", name);
  for (i=len-1; i>=0; i--) {
    if (i==len-1) printf("%ld ", (LI)ar[i]);
     else printf(spec, ar[i]);
    }
  printf("\n");
  return;}
#endif

#if DECCHECK
/* ------------------------------------------------------------------ */
/* decCheckOperands -- check operand(s) to a routine                  */
/*   res is the result structure (not checked; it will be set to      */
/*          quiet NaN if error found (and it is not NULL))            */
/*   lhs is the first operand (may be DECUNRESU)                      */
/*   rhs is the second (may be DECUNUSED)                             */
/*   set is the context (may be DECUNCONT)                            */
/*   returns 0 if both operands, and the context are clean, or 1      */
/*     otherwise (in which case the context will show an error,       */
/*     unless NULL).  Note that res is not cleaned; caller should     */
/*     handle this so res=NULL case is safe.                          */
/* The caller is expected to abandon immediately if 1 is returned.    */
/* ------------------------------------------------------------------ */
static Flag decCheckOperands(decNumber *res, const decNumber *lhs,
                             const decNumber *rhs, decContext *set) {
  Flag bad=0;
  if (set==NULL) {                 /* oops; hopeless  */
    #if DECTRACE || DECVERB
    printf("Reference to context is NULL.\n");
    #endif
    bad=1;
    return 1;}
   else if (set!=DECUNCONT
     && (set->digits<1 || set->round>=DEC_ROUND_MAX)) {
    bad=1;
    #if DECTRACE || DECVERB
    printf("Bad context [digits=%ld round=%ld].\n",
           (LI)set->digits, (LI)set->round);
    #endif
    }
   else {
    if (res==NULL) {
      bad=1;
      #if DECTRACE
      /* this one not DECVERB as standard tests include NULL  */
      printf("Reference to result is NULL.\n");
      #endif
      }
    if (!bad && lhs!=DECUNUSED) bad=(decCheckNumber(lhs));
    if (!bad && rhs!=DECUNUSED) bad=(decCheckNumber(rhs));
    }
  if (bad) {
    if (set!=DECUNCONT) uprv_decContextSetStatus(set, DEC_Invalid_operation);
    if (res!=DECUNRESU && res!=NULL) {
      uprv_decNumberZero(res);
      res->bits=DECNAN;       /* qNaN  */
      }
    }
  return bad;
  } /* decCheckOperands  */

/* ------------------------------------------------------------------ */
/* decCheckNumber -- check a number                                   */
/*   dn is the number to check                                        */
/*   returns 0 if the number is clean, or 1 otherwise                 */
/*                                                                    */
/* The number is considered valid if it could be a result from some   */
/* operation in some valid context.                                   */
/* ------------------------------------------------------------------ */
static Flag decCheckNumber(const decNumber *dn) {
  const Unit *up;             /* work  */
  uInt maxuint;               /* ..  */
  Int ae, d, digits;          /* ..  */
  Int emin, emax;             /* ..  */

  if (dn==NULL) {             /* hopeless  */
    #if DECTRACE
    /* this one not DECVERB as standard tests include NULL  */
    printf("Reference to decNumber is NULL.\n");
    #endif
    return 1;}

  /* check special values  */
  if (dn->bits & DECSPECIAL) {
    if (dn->exponent!=0) {
      #if DECTRACE || DECVERB
      printf("Exponent %ld (not 0) for a special value [%02x].\n",
             (LI)dn->exponent, dn->bits);
      #endif
      return 1;}

    /* 2003.09.08: NaNs may now have coefficients, so next tests Inf only  */
    if (decNumberIsInfinite(dn)) {
      if (dn->digits!=1) {
        #if DECTRACE || DECVERB
        printf("Digits %ld (not 1) for an infinity.\n", (LI)dn->digits);
        #endif
        return 1;}
      if (*dn->lsu!=0) {
        #if DECTRACE || DECVERB
        printf("LSU %ld (not 0) for an infinity.\n", (LI)*dn->lsu);
        #endif
        decDumpAr('I', dn->lsu, D2U(dn->digits));
        return 1;}
      } /* Inf  */
    /* 2002.12.26: negative NaNs can now appear through proposed IEEE  */
    /*             concrete formats (decimal64, etc.).  */
    return 0;
    }

  /* check the coefficient  */
  if (dn->digits<1 || dn->digits>DECNUMMAXP) {
    #if DECTRACE || DECVERB
    printf("Digits %ld in number.\n", (LI)dn->digits);
    #endif
    return 1;}

  d=dn->digits;

  for (up=dn->lsu; d>0; up++) {
    if (d>DECDPUN) maxuint=DECDPUNMAX;
     else {                   /* reached the msu  */
      maxuint=powers[d]-1;
      if (dn->digits>1 && *up<powers[d-1]) {
        #if DECTRACE || DECVERB
        printf("Leading 0 in number.\n");
        uprv_decNumberShow(dn);
        #endif
        return 1;}
      }
    if (*up>maxuint) {
      #if DECTRACE || DECVERB
      printf("Bad Unit [%08lx] in %ld-digit number at offset %ld [maxuint %ld].\n",
              (LI)*up, (LI)dn->digits, (LI)(up-dn->lsu), (LI)maxuint);
      #endif
      return 1;}
    d-=DECDPUN;
    }

  /* check the exponent.  Note that input operands can have exponents  */
  /* which are out of the set->emin/set->emax and set->digits range  */
  /* (just as they can have more digits than set->digits).  */
  ae=dn->exponent+dn->digits-1;    /* adjusted exponent  */
  emax=DECNUMMAXE;
  emin=DECNUMMINE;
  digits=DECNUMMAXP;
  if (ae<emin-(digits-1)) {
    #if DECTRACE || DECVERB
    printf("Adjusted exponent underflow [%ld].\n", (LI)ae);
    uprv_decNumberShow(dn);
    #endif
    return 1;}
  if (ae>+emax) {
    #if DECTRACE || DECVERB
    printf("Adjusted exponent overflow [%ld].\n", (LI)ae);
    uprv_decNumberShow(dn);
    #endif
    return 1;}

  return 0;              /* it's OK  */
  } /* decCheckNumber  */

/* ------------------------------------------------------------------ */
/* decCheckInexact -- check a normal finite inexact result has digits */
/*   dn is the number to check                                        */
/*   set is the context (for status and precision)                    */
/*   sets Invalid operation, etc., if some digits are missing         */
/* [this check is not made for DECSUBSET compilation or when          */
/* subnormal is not set]                                              */
/* ------------------------------------------------------------------ */
static void decCheckInexact(const decNumber *dn, decContext *set) {
  #if !DECSUBSET && DECEXTFLAG
    if ((set->status & (DEC_Inexact|DEC_Subnormal))==DEC_Inexact
     && (set->digits!=dn->digits) && !(dn->bits & DECSPECIAL)) {
      #if DECTRACE || DECVERB
      printf("Insufficient digits [%ld] on normal Inexact result.\n",
             (LI)dn->digits);
      uprv_decNumberShow(dn);
      #endif
      uprv_decContextSetStatus(set, DEC_Invalid_operation);
      }
  #else
    /* next is a noop for quiet compiler  */
    if (dn!=NULL && dn->digits==0) set->status|=DEC_Invalid_operation;
  #endif
  return;
  } /* decCheckInexact  */
#endif

#if DECALLOC
#undef malloc
#undef free
/* ------------------------------------------------------------------ */
/* decMalloc -- accountable allocation routine                        */
/*   n is the number of bytes to allocate                             */
/*                                                                    */
/* Semantics is the same as the stdlib malloc routine, but bytes      */
/* allocated are accounted for globally, and corruption fences are    */
/* added before and after the 'actual' storage.                       */
/* ------------------------------------------------------------------ */
/* This routine allocates storage with an extra twelve bytes; 8 are   */
/* at the start and hold:                                             */
/*   0-3 the original length requested                                */
/*   4-7 buffer corruption detection fence (DECFENCE, x4)             */
/* The 4 bytes at the end also hold a corruption fence (DECFENCE, x4) */
/* ------------------------------------------------------------------ */
static void *decMalloc(size_t n) {
  uInt  size=n+12;                 /* true size  */
  void  *alloc;                    /* -> allocated storage  */
  uByte *b, *b0;                   /* work  */
  uInt  uiwork;                    /* for macros  */

  alloc=malloc(size);              /* -> allocated storage  */
  if (alloc==NULL) return NULL;    /* out of strorage  */
  b0=(uByte *)alloc;               /* as bytes  */
  decAllocBytes+=n;                /* account for storage  */
  UBFROMUI(alloc, n);              /* save n  */
  /* printf(" alloc ++ dAB: %ld (%ld)\n", (LI)decAllocBytes, (LI)n);  */
  for (b=b0+4; b<b0+8; b++) *b=DECFENCE;
  for (b=b0+n+8; b<b0+n+12; b++) *b=DECFENCE;
  return b0+8;                     /* -> play area  */
  } /* decMalloc  */

/* ------------------------------------------------------------------ */
/* decFree -- accountable free routine                                */
/*   alloc is the storage to free                                     */
/*                                                                    */
/* Semantics is the same as the stdlib malloc routine, except that    */
/* the global storage accounting is updated and the fences are        */
/* checked to ensure that no routine has written 'out of bounds'.     */
/* ------------------------------------------------------------------ */
/* This routine first checks that the fences have not been corrupted. */
/* It then frees the storage using the 'truw' storage address (that   */
/* is, offset by 8).                                                  */
/* ------------------------------------------------------------------ */
static void decFree(void *alloc) {
  uInt  n;                         /* original length  */
  uByte *b, *b0;                   /* work  */
  uInt  uiwork;                    /* for macros  */

  if (alloc==NULL) return;         /* allowed; it's a nop  */
  b0=(uByte *)alloc;               /* as bytes  */
  b0-=8;                           /* -> true start of storage  */
  n=UBTOUI(b0);                    /* lift length  */
  for (b=b0+4; b<b0+8; b++) if (*b!=DECFENCE)
    printf("=== Corrupt byte [%02x] at offset %d from %ld ===\n", *b,
           b-b0-8, (LI)b0);
  for (b=b0+n+8; b<b0+n+12; b++) if (*b!=DECFENCE)
    printf("=== Corrupt byte [%02x] at offset +%d from %ld, n=%ld ===\n", *b,
           b-b0-8, (LI)b0, (LI)n);
  free(b0);                        /* drop the storage  */
  decAllocBytes-=n;                /* account for storage  */
  /* printf(" free -- dAB: %d (%d)\n", decAllocBytes, -n);  */
  } /* decFree  */
#define malloc(a) decMalloc(a)
#define free(a) decFree(a)
#endif
