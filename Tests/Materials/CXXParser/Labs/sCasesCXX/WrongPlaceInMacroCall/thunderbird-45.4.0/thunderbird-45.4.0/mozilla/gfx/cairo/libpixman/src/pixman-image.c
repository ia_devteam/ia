/*
 * Copyright © 2000 SuSE, Inc.
 * Copyright © 2007 Red Hat, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of SuSE not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  SuSE makes no representations about the
 * suitability of this software for any purpose.  It is provided "as is"
 * without express or implied warranty.
 *
 * SuSE DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL SuSE
 * BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include "var/tmp/sensor.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "pixman-private.h"

static const pixman_color_t transparent_black = { 0, 0, 0, 0 };

static void
gradient_property_changed (pixman_image_t *image)
{
    Din_Go(1,2048);gradient_t *gradient = &image->gradient;
    int n = gradient->n_stops;
    pixman_gradient_stop_t *stops = gradient->stops;
    pixman_gradient_stop_t *begin = &(gradient->stops[-1]);
    pixman_gradient_stop_t *end = &(gradient->stops[n]);

    Din_Go(10,2048);switch (gradient->common.repeat)
    {
    default:
    case PIXMAN_REPEAT_NONE:
	Din_Go(2,2048);begin->x = INT32_MIN;
	begin->color = transparent_black;
	end->x = INT32_MAX;
	end->color = transparent_black;
	Din_Go(3,2048);break;

    case PIXMAN_REPEAT_NORMAL:
	Din_Go(4,2048);begin->x = stops[n - 1].x - pixman_fixed_1;
	begin->color = stops[n - 1].color;
	end->x = stops[0].x + pixman_fixed_1;
	end->color = stops[0].color;
	Din_Go(5,2048);break;

    case PIXMAN_REPEAT_REFLECT:
	Din_Go(6,2048);begin->x = - stops[0].x;
	begin->color = stops[0].color;
	end->x = pixman_int_to_fixed (2) - stops[n - 1].x;
	end->color = stops[n - 1].color;
	Din_Go(7,2048);break;

    case PIXMAN_REPEAT_PAD:
	Din_Go(8,2048);begin->x = INT32_MIN;
	begin->color = stops[0].color;
	end->x = INT32_MAX;
	end->color = stops[n - 1].color;
	Din_Go(9,2048);break;
    }
Din_Go(11,2048);}

pixman_bool_t
_pixman_init_gradient (gradient_t *                  gradient,
                       const pixman_gradient_stop_t *stops,
                       int                           n_stops)
{
Din_Go(12,2048);    return_val_if_fail (n_stops > 0, FALSE);

    /* We allocate two extra stops, one before the beginning of the stop list,
     * and one after the end. These stops are initialized to whatever color
     * would be used for positions outside the range of the stop list.
     *
     * This saves a bit of computation in the gradient walker.
     *
     * The pointer we store in the gradient_t struct still points to the
     * first user-supplied struct, so when freeing, we will have to
     * subtract one.
     */
    Din_Go(13,2048);gradient->stops =
	pixman_malloc_ab (n_stops + 2, sizeof (pixman_gradient_stop_t));
    Din_Go(14,2048);if (!gradient->stops)
	return FALSE;

    Din_Go(15,2048);gradient->stops += 1;
    memcpy (gradient->stops, stops, n_stops * sizeof (pixman_gradient_stop_t));
    gradient->n_stops = n_stops;

    gradient->common.property_changed = gradient_property_changed;

    {pixman_bool_t  ReplaceReturn171 = TRUE; Din_Go(16,2048); return ReplaceReturn171;}
}

void
_pixman_image_init (pixman_image_t *image)
{
    Din_Go(17,2048);image_common_t *common = &image->common;

    pixman_region32_init (&common->clip_region);

    common->alpha_count = 0;
    common->have_clip_region = FALSE;
    common->clip_sources = FALSE;
    common->transform = NULL;
    common->repeat = PIXMAN_REPEAT_NONE;
    common->filter = PIXMAN_FILTER_NEAREST;
    common->filter_params = NULL;
    common->n_filter_params = 0;
    common->alpha_map = NULL;
    common->component_alpha = FALSE;
    common->ref_count = 1;
    common->property_changed = NULL;
    common->client_clip = FALSE;
    common->destroy_func = NULL;
    common->destroy_data = NULL;
    common->dirty = TRUE;
Din_Go(18,2048);}

pixman_bool_t
_pixman_image_fini (pixman_image_t *image)
{
    Din_Go(19,2048);image_common_t *common = (image_common_t *)image;

    common->ref_count--;

    Din_Go(30,2048);if (common->ref_count == 0)
    {
	Din_Go(20,2048);if (image->common.destroy_func)
	    {/*23*/Din_Go(21,2048);image->common.destroy_func (image, image->common.destroy_data);/*24*/}

	pixman_region32_fini (&common->clip_region);

	Din_Go(22,2048);free (common->transform);
	free (common->filter_params);

	Din_Go(23,2048);if (common->alpha_map)
	    pixman_image_unref ((pixman_image_t *)common->alpha_map);

	Din_Go(26,2048);if (image->type == LINEAR ||
	    image->type == RADIAL ||
	    image->type == CONICAL)
	{
	    Din_Go(24,2048);if (image->gradient.stops)
	    {
		/* See _pixman_init_gradient() for an explanation of the - 1 */
		Din_Go(25,2048);free (image->gradient.stops - 1);
	    }

	    /* This will trigger if someone adds a property_changed
	     * method to the linear/radial/conical gradient overwriting
	     * the general one.
	     */
	    assert (
		image->common.property_changed == gradient_property_changed);
	}

	Din_Go(28,2048);if (image->type == BITS && image->bits.free_me)
	    {/*25*/Din_Go(27,2048);free (image->bits.free_me);/*26*/}

	{pixman_bool_t  ReplaceReturn170 = TRUE; Din_Go(29,2048); return ReplaceReturn170;}
    }

    {pixman_bool_t  ReplaceReturn169 = FALSE; Din_Go(31,2048); return ReplaceReturn169;}
}

pixman_image_t *
_pixman_image_allocate (void)
{
    Din_Go(32,2048);pixman_image_t *image = malloc (sizeof (pixman_image_t));

    Din_Go(34,2048);if (image)
	{/*27*/Din_Go(33,2048);_pixman_image_init (image);/*28*/}

    {pixman_image_t * ReplaceReturn168 = image; Din_Go(35,2048); return ReplaceReturn168;}
}

static void
image_property_changed (pixman_image_t *image)
{
    Din_Go(36,2048);image->common.dirty = TRUE;
Din_Go(37,2048);}

/* Ref Counting */
PIXMAN_EXPORT pixman_image_t *
pixman_image_ref (pixman_image_t *image)
{
    Din_Go(38,2048);image->common.ref_count++;

    {pixman_image_t * ReplaceReturn167 = image; Din_Go(39,2048); return ReplaceReturn167;}
}

/* returns TRUE when the image is freed */
PIXMAN_EXPORT pixman_bool_t
pixman_image_unref (pixman_image_t *image)
{
    Din_Go(40,2048);if (_pixman_image_fini (image))
    {
	Din_Go(41,2048);free (image);
	{pixman_bool_t  ReplaceReturn166 = TRUE; Din_Go(42,2048); return ReplaceReturn166;}
    }

    {pixman_bool_t  ReplaceReturn165 = FALSE; Din_Go(43,2048); return ReplaceReturn165;}
}

PIXMAN_EXPORT void
pixman_image_set_destroy_function (pixman_image_t *            image,
                                   pixman_image_destroy_func_t func,
                                   void *                      data)
{
    Din_Go(44,2048);image->common.destroy_func = func;
    image->common.destroy_data = data;
Din_Go(45,2048);}

PIXMAN_EXPORT void *
pixman_image_get_destroy_data (pixman_image_t *image)
{
  {void * ReplaceReturn164 = image->common.destroy_data; Din_Go(46,2048); return ReplaceReturn164;}
}

void
_pixman_image_reset_clip_region (pixman_image_t *image)
{
    Din_Go(47,2048);image->common.have_clip_region = FALSE;
Din_Go(48,2048);}

/* Executive Summary: This function is a no-op that only exists
 * for historical reasons.
 *
 * There used to be a bug in the X server where it would rely on
 * out-of-bounds accesses when it was asked to composite with a
 * window as the source. It would create a pixman image pointing
 * to some bogus position in memory, but then set a clip region
 * to the position where the actual bits were.
 *
 * Due to a bug in old versions of pixman, where it would not clip
 * against the image bounds when a clip region was set, this would
 * actually work. So when the pixman bug was fixed, a workaround was
 * added to allow certain out-of-bound accesses. This function disabled
 * those workarounds.
 *
 * Since 0.21.2, pixman doesn't do these workarounds anymore, so now
 * this function is a no-op.
 */
PIXMAN_EXPORT void
pixman_disable_out_of_bounds_workaround (void)
{
Din_Go(49,2048);Din_Go(50,2048);}

static void
compute_image_info (pixman_image_t *image)
{
    Din_Go(51,2048);pixman_format_code_t code;
    uint32_t flags = 0;

    /* Transform */
    Din_Go(65,2048);if (!image->common.transform)
    {
	Din_Go(52,2048);flags |= (FAST_PATH_ID_TRANSFORM	|
		  FAST_PATH_X_UNIT_POSITIVE	|
		  FAST_PATH_Y_UNIT_ZERO		|
		  FAST_PATH_AFFINE_TRANSFORM);
    }
    else
    {
	Din_Go(53,2048);flags |= FAST_PATH_HAS_TRANSFORM;

	Din_Go(62,2048);if (image->common.transform->matrix[2][0] == 0			&&
	    image->common.transform->matrix[2][1] == 0			&&
	    image->common.transform->matrix[2][2] == pixman_fixed_1)
	{
	    Din_Go(54,2048);flags |= FAST_PATH_AFFINE_TRANSFORM;

	    Din_Go(61,2048);if (image->common.transform->matrix[0][1] == 0 &&
		image->common.transform->matrix[1][0] == 0)
	    {
		Din_Go(55,2048);if (image->common.transform->matrix[0][0] == -pixman_fixed_1 &&
		    image->common.transform->matrix[1][1] == -pixman_fixed_1)
		{
		    Din_Go(56,2048);flags |= FAST_PATH_ROTATE_180_TRANSFORM;
		}
		Din_Go(57,2048);flags |= FAST_PATH_SCALE_TRANSFORM;
	    }
	    else {/*47*/Din_Go(58,2048);if (image->common.transform->matrix[0][0] == 0 &&
	             image->common.transform->matrix[1][1] == 0)
	    {
		Din_Go(59,2048);pixman_fixed_t m01 = image->common.transform->matrix[0][1];
		pixman_fixed_t m10 = image->common.transform->matrix[1][0];

		Din_Go(60,2048);if (m01 == -pixman_fixed_1 && m10 == pixman_fixed_1)
		    flags |= FAST_PATH_ROTATE_90_TRANSFORM;
		else if (m01 == pixman_fixed_1 && m10 == -pixman_fixed_1)
		    flags |= FAST_PATH_ROTATE_270_TRANSFORM;
	    ;/*48*/}}
	}

	Din_Go(63,2048);if (image->common.transform->matrix[0][0] > 0)
	    flags |= FAST_PATH_X_UNIT_POSITIVE;

	Din_Go(64,2048);if (image->common.transform->matrix[1][0] == 0)
	    flags |= FAST_PATH_Y_UNIT_ZERO;
    }

    /* Filter */
    Din_Go(81,2048);switch (image->common.filter)
    {
    case PIXMAN_FILTER_NEAREST:
    case PIXMAN_FILTER_FAST:
	Din_Go(66,2048);flags |= (FAST_PATH_NEAREST_FILTER | FAST_PATH_NO_CONVOLUTION_FILTER);
	Din_Go(67,2048);break;

    case PIXMAN_FILTER_BILINEAR:
    case PIXMAN_FILTER_GOOD:
    case PIXMAN_FILTER_BEST:
	Din_Go(68,2048);flags |= (FAST_PATH_BILINEAR_FILTER | FAST_PATH_NO_CONVOLUTION_FILTER);

	/* Here we have a chance to optimize BILINEAR filter to NEAREST if
	 * they are equivalent for the currently used transformation matrix.
	 */
	Din_Go(74,2048);if (flags & FAST_PATH_ID_TRANSFORM)
	{
	    Din_Go(69,2048);flags |= FAST_PATH_NEAREST_FILTER;
	}
	else {/*49*/Din_Go(70,2048);if (
	    /* affine and integer translation components in matrix ... */
	    ((flags & FAST_PATH_AFFINE_TRANSFORM) &&
	     !pixman_fixed_frac (image->common.transform->matrix[0][2] |
				 image->common.transform->matrix[1][2])) &&
	    (
		/* ... combined with a simple rotation */
		(flags & (FAST_PATH_ROTATE_90_TRANSFORM |
			  FAST_PATH_ROTATE_180_TRANSFORM |
			  FAST_PATH_ROTATE_270_TRANSFORM)) ||
		/* ... or combined with a simple non-rotated translation */
		(image->common.transform->matrix[0][0] == pixman_fixed_1 &&
		 image->common.transform->matrix[1][1] == pixman_fixed_1 &&
		 image->common.transform->matrix[0][1] == 0 &&
		 image->common.transform->matrix[1][0] == 0)
		)
	    )
	{
	    /* FIXME: there are some affine-test failures, showing that
	     * handling of BILINEAR and NEAREST filter is not quite
	     * equivalent when getting close to 32K for the translation
	     * components of the matrix. That's likely some bug, but for
	     * now just skip BILINEAR->NEAREST optimization in this case.
	     */
	    Din_Go(71,2048);pixman_fixed_t magic_limit = pixman_int_to_fixed (30000);
	    Din_Go(73,2048);if (image->common.transform->matrix[0][2] <= magic_limit  &&
	        image->common.transform->matrix[1][2] <= magic_limit  &&
	        image->common.transform->matrix[0][2] >= -magic_limit &&
	        image->common.transform->matrix[1][2] >= -magic_limit)
	    {
		Din_Go(72,2048);flags |= FAST_PATH_NEAREST_FILTER;
	    }
	;/*50*/}}
	Din_Go(75,2048);break;

    case PIXMAN_FILTER_CONVOLUTION:
	Din_Go(76,2048);break;

    case PIXMAN_FILTER_SEPARABLE_CONVOLUTION:
	Din_Go(77,2048);flags |= FAST_PATH_SEPARABLE_CONVOLUTION_FILTER;
	Din_Go(78,2048);break;

    default:
	Din_Go(79,2048);flags |= FAST_PATH_NO_CONVOLUTION_FILTER;
	Din_Go(80,2048);break;
    }

    /* Repeat mode */
    Din_Go(90,2048);switch (image->common.repeat)
    {
    case PIXMAN_REPEAT_NONE:
	Din_Go(82,2048);flags |=
	    FAST_PATH_NO_REFLECT_REPEAT		|
	    FAST_PATH_NO_PAD_REPEAT		|
	    FAST_PATH_NO_NORMAL_REPEAT;
	Din_Go(83,2048);break;

    case PIXMAN_REPEAT_REFLECT:
	Din_Go(84,2048);flags |=
	    FAST_PATH_NO_PAD_REPEAT		|
	    FAST_PATH_NO_NONE_REPEAT		|
	    FAST_PATH_NO_NORMAL_REPEAT;
	Din_Go(85,2048);break;

    case PIXMAN_REPEAT_PAD:
	Din_Go(86,2048);flags |=
	    FAST_PATH_NO_REFLECT_REPEAT		|
	    FAST_PATH_NO_NONE_REPEAT		|
	    FAST_PATH_NO_NORMAL_REPEAT;
	Din_Go(87,2048);break;

    default:
	Din_Go(88,2048);flags |=
	    FAST_PATH_NO_REFLECT_REPEAT		|
	    FAST_PATH_NO_PAD_REPEAT		|
	    FAST_PATH_NO_NONE_REPEAT;
	Din_Go(89,2048);break;
    }

    /* Component alpha */
    Din_Go(91,2048);if (image->common.component_alpha)
	flags |= FAST_PATH_COMPONENT_ALPHA;
    else
	flags |= FAST_PATH_UNIFIED_ALPHA;

    Din_Go(92,2048);flags |= (FAST_PATH_NO_ACCESSORS | FAST_PATH_NARROW_FORMAT);

    /* Type specific checks */
    Din_Go(119,2048);switch (image->type)
    {
    case SOLID:
	Din_Go(93,2048);code = PIXMAN_solid;

	Din_Go(94,2048);if (image->solid.color.alpha == 0xffff)
	    flags |= FAST_PATH_IS_OPAQUE;
	Din_Go(95,2048);break;

    case BITS:
	Din_Go(96,2048);if (image->bits.width == 1	&&
	    image->bits.height == 1	&&
	    image->common.repeat != PIXMAN_REPEAT_NONE)
	{
	    Din_Go(97,2048);code = PIXMAN_solid;
	}
	else
	{
	    Din_Go(98,2048);code = image->bits.format;
	    flags |= FAST_PATH_BITS_IMAGE;
	}

	Din_Go(101,2048);if (!PIXMAN_FORMAT_A (image->bits.format)				&&
	    PIXMAN_FORMAT_TYPE (image->bits.format) != PIXMAN_TYPE_GRAY		&&
	    PIXMAN_FORMAT_TYPE (image->bits.format) != PIXMAN_TYPE_COLOR)
	{
	    Din_Go(99,2048);flags |= FAST_PATH_SAMPLES_OPAQUE;

	    Din_Go(100,2048);if (image->common.repeat != PIXMAN_REPEAT_NONE)
		flags |= FAST_PATH_IS_OPAQUE;
	}

	Din_Go(102,2048);if (image->bits.read_func || image->bits.write_func)
	    flags &= ~FAST_PATH_NO_ACCESSORS;

	Din_Go(103,2048);if (PIXMAN_FORMAT_IS_WIDE (image->bits.format))
	    flags &= ~FAST_PATH_NARROW_FORMAT;

	Din_Go(104,2048);if (image->bits.format == PIXMAN_r5g6b5)
	    flags |= FAST_PATH_16_FORMAT;

	Din_Go(105,2048);break;

    case RADIAL:
	Din_Go(106,2048);code = PIXMAN_unknown;

	/*
	 * As explained in pixman-radial-gradient.c, every point of
	 * the plane has a valid associated radius (and thus will be
	 * colored) if and only if a is negative (i.e. one of the two
	 * circles contains the other one).
	 */

        Din_Go(108,2048);if (image->radial.a >= 0)
	    {/*51*/Din_Go(107,2048);break;/*52*/}

	/* Fall through */

    case CONICAL:
    case LINEAR:
	Din_Go(109,2048);code = PIXMAN_unknown;

	Din_Go(115,2048);if (image->common.repeat != PIXMAN_REPEAT_NONE)
	{
	    Din_Go(110,2048);int i;

	    flags |= FAST_PATH_IS_OPAQUE;
	    Din_Go(114,2048);for (i = 0; i < image->gradient.n_stops; ++i)
	    {
		Din_Go(111,2048);if (image->gradient.stops[i].color.alpha != 0xffff)
		{
		    Din_Go(112,2048);flags &= ~FAST_PATH_IS_OPAQUE;
		    Din_Go(113,2048);break;
		}
	    }
	}
	Din_Go(116,2048);break;

    default:
	Din_Go(117,2048);code = PIXMAN_unknown;
	Din_Go(118,2048);break;
    }

    /* Alpha map */
    Din_Go(122,2048);if (!image->common.alpha_map)
    {
	Din_Go(120,2048);flags |= FAST_PATH_NO_ALPHA_MAP;
    }
    else
    {
	Din_Go(121,2048);if (PIXMAN_FORMAT_IS_WIDE (image->common.alpha_map->format))
	    flags &= ~FAST_PATH_NARROW_FORMAT;
    }

    /* Both alpha maps and convolution filters can introduce
     * non-opaqueness in otherwise opaque images. Also
     * an image with component alpha turned on is only opaque
     * if all channels are opaque, so we simply turn it off
     * unconditionally for those images.
     */
    Din_Go(124,2048);if (image->common.alpha_map						||
	image->common.filter == PIXMAN_FILTER_CONVOLUTION		||
        image->common.filter == PIXMAN_FILTER_SEPARABLE_CONVOLUTION     ||
	image->common.component_alpha)
    {
	Din_Go(123,2048);flags &= ~(FAST_PATH_IS_OPAQUE | FAST_PATH_SAMPLES_OPAQUE);
    }

    Din_Go(125,2048);image->common.flags = flags;
    image->common.extended_format_code = code;
Din_Go(126,2048);}

void
_pixman_image_validate (pixman_image_t *image)
{
    Din_Go(127,2048);if (image->common.dirty)
    {
	Din_Go(128,2048);compute_image_info (image);

	/* It is important that property_changed is
	 * called *after* compute_image_info() because
	 * property_changed() can make use of the flags
	 * to set up accessors etc.
	 */
	Din_Go(130,2048);if (image->common.property_changed)
	    {/*29*/Din_Go(129,2048);image->common.property_changed (image);/*30*/}

	Din_Go(131,2048);image->common.dirty = FALSE;
    }

    Din_Go(133,2048);if (image->common.alpha_map)
	{/*31*/Din_Go(132,2048);_pixman_image_validate ((pixman_image_t *)image->common.alpha_map);/*32*/}
Din_Go(134,2048);}

PIXMAN_EXPORT pixman_bool_t
pixman_image_set_clip_region32 (pixman_image_t *   image,
                                pixman_region32_t *region)
{
    Din_Go(135,2048);image_common_t *common = (image_common_t *)image;
    pixman_bool_t result;

    Din_Go(138,2048);if (region)
    {
	Din_Go(136,2048);if ((result = pixman_region32_copy (&common->clip_region, region)))
	    image->common.have_clip_region = TRUE;
    }
    else
    {
	Din_Go(137,2048);_pixman_image_reset_clip_region (image);

	result = TRUE;
    }

    Din_Go(139,2048);image_property_changed (image);

    {pixman_bool_t  ReplaceReturn163 = result; Din_Go(140,2048); return ReplaceReturn163;}
}

PIXMAN_EXPORT pixman_bool_t
pixman_image_set_clip_region (pixman_image_t *   image,
                              pixman_region16_t *region)
{
    Din_Go(141,2048);image_common_t *common = (image_common_t *)image;
    pixman_bool_t result;

    Din_Go(144,2048);if (region)
    {
	Din_Go(142,2048);if ((result = pixman_region32_copy_from_region16 (&common->clip_region, region)))
	    image->common.have_clip_region = TRUE;
    }
    else
    {
	Din_Go(143,2048);_pixman_image_reset_clip_region (image);

	result = TRUE;
    }

    Din_Go(145,2048);image_property_changed (image);

    {pixman_bool_t  ReplaceReturn162 = result; Din_Go(146,2048); return ReplaceReturn162;}
}

PIXMAN_EXPORT void
pixman_image_set_has_client_clip (pixman_image_t *image,
                                  pixman_bool_t   client_clip)
{
    Din_Go(147,2048);image->common.client_clip = client_clip;
Din_Go(148,2048);}

PIXMAN_EXPORT pixman_bool_t
pixman_image_set_transform (pixman_image_t *          image,
                            const pixman_transform_t *transform)
{
    Din_Go(149,2048);static const pixman_transform_t id =
    {
	{ { pixman_fixed_1, 0, 0 },
	  { 0, pixman_fixed_1, 0 },
	  { 0, 0, pixman_fixed_1 } }
    };

    image_common_t *common = (image_common_t *)image;
    pixman_bool_t result;

    Din_Go(150,2048);if (common->transform == transform)
	return TRUE;

    Din_Go(153,2048);if (!transform || memcmp (&id, transform, sizeof (pixman_transform_t)) == 0)
    {
	Din_Go(151,2048);free (common->transform);
	common->transform = NULL;
	result = TRUE;

	Din_Go(152,2048);goto out;
    }

    Din_Go(155,2048);if (common->transform &&
	memcmp (common->transform, transform, sizeof (pixman_transform_t)) == 0)
    {
	{pixman_bool_t  ReplaceReturn161 = TRUE; Din_Go(154,2048); return ReplaceReturn161;}
    }

    Din_Go(157,2048);if (common->transform == NULL)
	{/*1*/Din_Go(156,2048);common->transform = malloc (sizeof (pixman_transform_t));/*2*/}

    Din_Go(160,2048);if (common->transform == NULL)
    {
	Din_Go(158,2048);result = FALSE;

	Din_Go(159,2048);goto out;
    }

    Din_Go(161,2048);memcpy (common->transform, transform, sizeof(pixman_transform_t));

    result = TRUE;

out:
    image_property_changed (image);

    {pixman_bool_t  ReplaceReturn160 = result; Din_Go(162,2048); return ReplaceReturn160;}
}

PIXMAN_EXPORT void
pixman_image_set_repeat (pixman_image_t *image,
                         pixman_repeat_t repeat)
{
    Din_Go(163,2048);if (image->common.repeat == repeat)
	{/*3*/Din_Go(164,2048);return;/*4*/}

    Din_Go(165,2048);image->common.repeat = repeat;

    image_property_changed (image);
Din_Go(166,2048);}

PIXMAN_EXPORT pixman_bool_t
pixman_image_set_filter (pixman_image_t *      image,
                         pixman_filter_t       filter,
                         const pixman_fixed_t *params,
                         int                   n_params)
{
    Din_Go(167,2048);image_common_t *common = (image_common_t *)image;
    pixman_fixed_t *new_params;

    Din_Go(168,2048);if (params == common->filter_params && filter == common->filter)
	return TRUE;

    Din_Go(170,2048);if (filter == PIXMAN_FILTER_SEPARABLE_CONVOLUTION)
    {
	Din_Go(169,2048);int width = pixman_fixed_to_int (params[0]);
	int height = pixman_fixed_to_int (params[1]);
	int x_phase_bits = pixman_fixed_to_int (params[2]);
	int y_phase_bits = pixman_fixed_to_int (params[3]);
	int n_x_phases = (1 << x_phase_bits);
	int n_y_phases = (1 << y_phase_bits);

	return_val_if_fail (
	    n_params == 4 + n_x_phases * width + n_y_phases * height, FALSE);
    }
    
    Din_Go(171,2048);new_params = NULL;
    Din_Go(175,2048);if (params)
    {
	Din_Go(172,2048);new_params = pixman_malloc_ab (n_params, sizeof (pixman_fixed_t));
	Din_Go(173,2048);if (!new_params)
	    return FALSE;

	Din_Go(174,2048);memcpy (new_params,
	        params, n_params * sizeof (pixman_fixed_t));
    }

    Din_Go(176,2048);common->filter = filter;

    Din_Go(178,2048);if (common->filter_params)
	{/*5*/Din_Go(177,2048);free (common->filter_params);/*6*/}

    Din_Go(179,2048);common->filter_params = new_params;
    common->n_filter_params = n_params;

    image_property_changed (image);
    {pixman_bool_t  ReplaceReturn159 = TRUE; Din_Go(180,2048); return ReplaceReturn159;}
}

PIXMAN_EXPORT void
pixman_image_set_source_clipping (pixman_image_t *image,
                                  pixman_bool_t   clip_sources)
{
    Din_Go(181,2048);if (image->common.clip_sources == clip_sources)
	{/*7*/Din_Go(182,2048);return;/*8*/}

    Din_Go(183,2048);image->common.clip_sources = clip_sources;

    image_property_changed (image);
Din_Go(184,2048);}

/* Unlike all the other property setters, this function does not
 * copy the content of indexed. Doing this copying is simply
 * way, way too expensive.
 */
PIXMAN_EXPORT void
pixman_image_set_indexed (pixman_image_t *        image,
                          const pixman_indexed_t *indexed)
{
    Din_Go(185,2048);bits_image_t *bits = (bits_image_t *)image;

    Din_Go(187,2048);if (bits->indexed == indexed)
	{/*11*/Din_Go(186,2048);return;/*12*/}

    Din_Go(188,2048);bits->indexed = indexed;

    image_property_changed (image);
Din_Go(189,2048);}

PIXMAN_EXPORT void
pixman_image_set_alpha_map (pixman_image_t *image,
                            pixman_image_t *alpha_map,
                            int16_t         x,
                            int16_t         y)
{
    Din_Go(190,2048);image_common_t *common = (image_common_t *)image;

    return_if_fail (!alpha_map || alpha_map->type == BITS);

    Din_Go(192,2048);if (alpha_map && common->alpha_count > 0)
    {
	/* If this image is being used as an alpha map itself,
	 * then you can't give it an alpha map of its own.
	 */
	Din_Go(191,2048);return;
    }

    Din_Go(194,2048);if (alpha_map && alpha_map->common.alpha_map)
    {
	/* If the image has an alpha map of its own,
	 * then it can't be used as an alpha map itself
	 */
	Din_Go(193,2048);return;
    }

    Din_Go(200,2048);if (common->alpha_map != (bits_image_t *)alpha_map)
    {
	Din_Go(195,2048);if (common->alpha_map)
	{
	    Din_Go(196,2048);common->alpha_map->common.alpha_count--;

	    pixman_image_unref ((pixman_image_t *)common->alpha_map);
	}

	Din_Go(199,2048);if (alpha_map)
	{
	    Din_Go(197,2048);common->alpha_map = (bits_image_t *)pixman_image_ref (alpha_map);

	    common->alpha_map->common.alpha_count++;
	}
	else
	{
	    Din_Go(198,2048);common->alpha_map = NULL;
	}
    }

    Din_Go(201,2048);common->alpha_origin_x = x;
    common->alpha_origin_y = y;

    image_property_changed (image);
Din_Go(202,2048);}

PIXMAN_EXPORT void
pixman_image_set_component_alpha   (pixman_image_t *image,
                                    pixman_bool_t   component_alpha)
{
    Din_Go(203,2048);if (image->common.component_alpha == component_alpha)
	{/*9*/Din_Go(204,2048);return;/*10*/}

    Din_Go(205,2048);image->common.component_alpha = component_alpha;

    image_property_changed (image);
Din_Go(206,2048);}

PIXMAN_EXPORT pixman_bool_t
pixman_image_get_component_alpha   (pixman_image_t       *image)
{
    {pixman_bool_t  ReplaceReturn158 = image->common.component_alpha; Din_Go(207,2048); return ReplaceReturn158;}
}

PIXMAN_EXPORT void
pixman_image_set_accessors (pixman_image_t *           image,
                            pixman_read_memory_func_t  read_func,
                            pixman_write_memory_func_t write_func)
{
Din_Go(208,2048);    return_if_fail (image != NULL);

    Din_Go(210,2048);if (image->type == BITS)
    {
	Din_Go(209,2048);image->bits.read_func = read_func;
	image->bits.write_func = write_func;

	image_property_changed (image);
    }
Din_Go(211,2048);}

PIXMAN_EXPORT uint32_t *
pixman_image_get_data (pixman_image_t *image)
{
    Din_Go(212,2048);if (image->type == BITS)
	{/*13*/{uint32_t * ReplaceReturn157 = image->bits.bits; Din_Go(213,2048); return ReplaceReturn157;}/*14*/}

    {uint32_t * ReplaceReturn156 = NULL; Din_Go(214,2048); return ReplaceReturn156;}
}

PIXMAN_EXPORT int
pixman_image_get_width (pixman_image_t *image)
{
    Din_Go(215,2048);if (image->type == BITS)
	{/*15*/{int  ReplaceReturn155 = image->bits.width; Din_Go(216,2048); return ReplaceReturn155;}/*16*/}

    {int  ReplaceReturn154 = 0; Din_Go(217,2048); return ReplaceReturn154;}
}

PIXMAN_EXPORT int
pixman_image_get_height (pixman_image_t *image)
{
    Din_Go(218,2048);if (image->type == BITS)
	{/*17*/{int  ReplaceReturn153 = image->bits.height; Din_Go(219,2048); return ReplaceReturn153;}/*18*/}

    {int  ReplaceReturn152 = 0; Din_Go(220,2048); return ReplaceReturn152;}
}

PIXMAN_EXPORT int
pixman_image_get_stride (pixman_image_t *image)
{
    Din_Go(221,2048);if (image->type == BITS)
	{/*19*/{int  ReplaceReturn151 = image->bits.rowstride * (int) sizeof (uint32_t); Din_Go(222,2048); return ReplaceReturn151;}/*20*/}

    {int  ReplaceReturn150 = 0; Din_Go(223,2048); return ReplaceReturn150;}
}

PIXMAN_EXPORT int
pixman_image_get_depth (pixman_image_t *image)
{
    Din_Go(224,2048);if (image->type == BITS)
	return PIXMAN_FORMAT_DEPTH (image->bits.format);

    {int  ReplaceReturn149 = 0; Din_Go(225,2048); return ReplaceReturn149;}
}

PIXMAN_EXPORT pixman_format_code_t
pixman_image_get_format (pixman_image_t *image)
{
    Din_Go(226,2048);if (image->type == BITS)
	{/*21*/{pixman_format_code_t  ReplaceReturn148 = image->bits.format; Din_Go(227,2048); return ReplaceReturn148;}/*22*/}

    {pixman_format_code_t  ReplaceReturn147 = PIXMAN_null; Din_Go(228,2048); return ReplaceReturn147;}
}

uint32_t
_pixman_image_get_solid (pixman_implementation_t *imp,
			 pixman_image_t *         image,
                         pixman_format_code_t     format)
{
    Din_Go(229,2048);uint32_t result;

    Din_Go(240,2048);if (image->type == SOLID)
    {
	Din_Go(230,2048);result = image->solid.color_32;
    }
    else {/*33*/Din_Go(231,2048);if (image->type == BITS)
    {
	Din_Go(232,2048);if (image->bits.format == PIXMAN_a8r8g8b8)
	    {/*35*/Din_Go(233,2048);result = image->bits.bits[0];/*36*/}
	else {/*37*/Din_Go(234,2048);if (image->bits.format == PIXMAN_x8r8g8b8)
	    {/*39*/Din_Go(235,2048);result = image->bits.bits[0] | 0xff000000;/*40*/}
	else {/*41*/Din_Go(236,2048);if (image->bits.format == PIXMAN_a8)
	    {/*43*/Din_Go(237,2048);result = (*(uint8_t *)image->bits.bits) << 24;/*44*/}
	else
	    {/*45*/Din_Go(238,2048);goto otherwise;/*46*/}/*42*/}/*38*/}
    }
    else
    {
	Din_Go(239,2048);pixman_iter_t iter;

    otherwise:
	_pixman_implementation_src_iter_init (
	    imp, &iter, image, 0, 0, 1, 1,
	    (uint8_t *)&result,
	    ITER_NARROW, image->common.flags);
	
	result = *iter.get_scanline (&iter, NULL);
    ;/*34*/}}

    /* If necessary, convert RGB <--> BGR. */
    Din_Go(242,2048);if (PIXMAN_FORMAT_TYPE (format) != PIXMAN_TYPE_ARGB
	&& PIXMAN_FORMAT_TYPE (format) != PIXMAN_TYPE_ARGB_SRGB)
    {
	Din_Go(241,2048);result = (((result & 0xff000000) >>  0) |
	          ((result & 0x00ff0000) >> 16) |
	          ((result & 0x0000ff00) >>  0) |
	          ((result & 0x000000ff) << 16));
    }

    {uint32_t  ReplaceReturn146 = result; Din_Go(243,2048); return ReplaceReturn146;}
}
