#include "var/tmp/sensor.h"
/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "nsBayesianFilter.h"
#include "nsIInputStream.h"
#include "nsIStreamListener.h"
#include "nsNetUtil.h"
#include "nsQuickSort.h"
#include "nsIMsgMessageService.h"
#include "nsMsgUtils.h" // for GetMessageServiceFromURI
#include "prnetdb.h"
#include "nsIMsgWindow.h"
#include "mozilla/Logging.h"
#include "nsAppDirectoryServiceDefs.h"
#include "nsUnicharUtils.h"
#include "nsDirectoryServiceUtils.h"
#include "nsIMIMEHeaderParam.h"
#include "nsNetCID.h"
#include "nsIMimeHeaders.h"
#include "nsMsgMimeCID.h"
#include "nsIMsgMailNewsUrl.h"
#include "nsIMimeMiscStatus.h"
#include "nsIPrefService.h"
#include "nsIPrefBranch.h"
#include "nsIStringEnumerator.h"
#include "nsIObserverService.h"
#include "nsIChannel.h"

using namespace mozilla;

// needed to mark attachment flag on the db hdr
#include "nsIMsgHdr.h"

// needed to strip html out of the body
#include "nsIContentSerializer.h"
#include "nsLayoutCID.h"
#include "nsIParserUtils.h"
#include "nsIDocumentEncoder.h"

#include "nsIncompleteGamma.h"
#include <math.h>
#include <prmem.h>
#include "nsIMsgTraitService.h"
#include "mozilla/Services.h"
#include <cstdlib> // for std::abs(int/long)
#include <cmath> // for std::abs(float/double)

static PRLogModuleInfo *BayesianFilterLogModule = nullptr;

#define kDefaultJunkThreshold .99 // we override this value via a pref
static const char* kBayesianFilterTokenDelimiters = " \t\n\r\f.";
static unsigned int kMinLengthForToken = 3; // lower bound on the number of characters in a word before we treat it as a token
static unsigned int kMaxLengthForToken = 12; // upper bound on the number of characters in a word to be declared as a token

#define FORGED_RECEIVED_HEADER_HINT NS_LITERAL_CSTRING("may be forged")

#ifndef M_LN2
#define M_LN2 0.69314718055994530942
#endif

#ifndef M_E
#define M_E   2.7182818284590452354
#endif

// provide base implementation of hash lookup of a string
struct BaseToken : public PLDHashEntryHdr
{
    const char* mWord;
};

// token for a particular message
// mCount, mAnalysisLink are initialized to zero by the hash code
struct Token : public BaseToken {
    uint32_t mCount;
    uint32_t mAnalysisLink; // index in mAnalysisStore of the AnalysisPerToken
                            // object for the first trait for this token
};

// token stored in a training file for a group of messages
// mTraitLink is initialized to 0 by the hash code
struct CorpusToken : public BaseToken
{
    uint32_t mTraitLink;    // index in mTraitStore of the TraitPerToken
                            // object for the first trait for this token
};

// set the value of a TraitPerToken object
TraitPerToken::TraitPerToken(uint32_t aTraitId, uint32_t aCount)
  :  mId(aTraitId), mCount(aCount), mNextLink(0)
{
Din_Go(1,2048);Din_Go(2,2048);}

// shorthand representations of trait ids for junk and good
static const uint32_t kJunkTrait = nsIJunkMailPlugin::JUNK_TRAIT;
static const uint32_t kGoodTrait = nsIJunkMailPlugin::GOOD_TRAIT;

// set the value of an AnalysisPerToken object
AnalysisPerToken::AnalysisPerToken(
  uint32_t aTraitIndex, double aDistance, double aProbability) :
    mTraitIndex(aTraitIndex),
    mDistance(aDistance),
    mProbability(aProbability),
    mNextLink(0)
{
Din_Go(3,2048);Din_Go(4,2048);}

// the initial size of the AnalysisPerToken linked list storage
const uint32_t kAnalysisStoreCapacity = 2048;

// the initial size of the TraitPerToken linked list storage
const uint32_t kTraitStoreCapacity = 16384;

// Size of Auto arrays representing per trait information
const uint32_t kTraitAutoCapacity = 10;

TokenEnumeration::TokenEnumeration(PLDHashTable* table)
    :   mIterator(table->Iter())
{
Din_Go(5,2048);Din_Go(6,2048);}

inline bool TokenEnumeration::hasMoreTokens()
{
    return !mIterator.Done();
}

inline BaseToken* TokenEnumeration::nextToken()
{
    auto token = static_cast<BaseToken*>(mIterator.Get());
    mIterator.Next();
    return token;
}

// member variables
static const PLDHashTableOps gTokenTableOps = {
    PLDHashTable::HashStringKey,
    PLDHashTable::MatchStringKey,
    PLDHashTable::MoveEntryStub,
    PLDHashTable::ClearEntryStub
};

TokenHash::TokenHash(uint32_t aEntrySize)
  : mTokenTable(&gTokenTableOps, aEntrySize, 128)
{
    Din_Go(7,2048);mEntrySize = aEntrySize;
    PL_INIT_ARENA_POOL(&mWordPool, "Words Arena", 16384);
}

TokenHash::~TokenHash()
{
    Din_Go(8,2048);PL_FinishArenaPool(&mWordPool);
Din_Go(9,2048);}

nsresult TokenHash::clearTokens()
{
    // we re-use the tokenizer when classifying multiple messages,
    // so this gets called after every message classification.
    Din_Go(10,2048);mTokenTable.ClearAndPrepareForLength(128);
    PL_FreeArenaPool(&mWordPool);
    {__IAENUM__ nsresult  ReplaceReturn254 = NS_OK; Din_Go(11,2048); return ReplaceReturn254;}
}

char* TokenHash::copyWord(const char* word, uint32_t len)
{
    Din_Go(12,2048);void* result;
    uint32_t size = 1 + len;
    PL_ARENA_ALLOCATE(result, &mWordPool, size);
    Din_Go(14,2048);if (result)
        {/*1*/Din_Go(13,2048);memcpy(result, word, size);/*2*/}
    {char * ReplaceReturn253 = reinterpret_cast<char*>(result); Din_Go(15,2048); return ReplaceReturn253;}
}

inline BaseToken* TokenHash::get(const char* word)
{
    PLDHashEntryHdr* entry = mTokenTable.Search(word);
    if (entry)
        return static_cast<BaseToken*>(entry);
    return NULL;
}

BaseToken* TokenHash::add(const char* word)
{
    Din_Go(16,2048);if (!word || !*word)
    {
      NS_ERROR("Trying to add a null word");
      {__IASTRUCT__ BaseToken * ReplaceReturn252 = nullptr; Din_Go(17,2048); return ReplaceReturn252;}
    }

    MOZ_LOG(BayesianFilterLogModule, LogLevel::Debug, ("add word: %s", word));

    Din_Go(18,2048);PLDHashEntryHdr* entry = mTokenTable.Add(word, mozilla::fallible);
    BaseToken* token = static_cast<BaseToken*>(entry);
    Din_Go(26,2048);if (token) {
        Din_Go(19,2048);if (token->mWord == NULL) {
            Din_Go(20,2048);uint32_t len = strlen(word);
            NS_ASSERTION(len != 0, "adding zero length word to tokenizer");
            Din_Go(21,2048);if (!len)
              MOZ_LOG(BayesianFilterLogModule, LogLevel::Debug, ("adding zero length word to tokenizer"));
            Din_Go(22,2048);token->mWord = copyWord(word, len);
            NS_ASSERTION(token->mWord, "copyWord failed");
            Din_Go(25,2048);if (!token->mWord) {
                MOZ_LOG(BayesianFilterLogModule, LogLevel::Error, ("copyWord failed: %s (%d)", word, len));
                Din_Go(23,2048);mTokenTable.RawRemove(entry);
                {__IASTRUCT__ BaseToken * ReplaceReturn251 = NULL; Din_Go(24,2048); return ReplaceReturn251;}
            }
        }
    }
    {__IASTRUCT__ BaseToken * ReplaceReturn250 = token; Din_Go(27,2048); return ReplaceReturn250;}
}

inline uint32_t TokenHash::countTokens()
{
  return mTokenTable.EntryCount();
}

inline TokenEnumeration TokenHash::getTokens()
{
  return TokenEnumeration(&mTokenTable);
}

Tokenizer::Tokenizer() :
  TokenHash(sizeof(Token)),
  mBodyDelimiters(kBayesianFilterTokenDelimiters),
  mHeaderDelimiters(kBayesianFilterTokenDelimiters),
  mCustomHeaderTokenization(false),
  mMaxLengthForToken(kMaxLengthForToken)
{
  Din_Go(28,2048);nsresult rv;
  nsCOMPtr<nsIPrefService> prefs = do_GetService(NS_PREFSERVICE_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS_VOID(rv);

  nsCOMPtr<nsIPrefBranch> prefBranch;
  rv = prefs->GetBranch("mailnews.bayesian_spam_filter.", getter_AddRefs(prefBranch));
  NS_ENSURE_SUCCESS_VOID(rv); // no branch defined, just use defaults

  /*
   * RSS feeds store their summary as alternate content of an iframe. But due
   * to bug 365953, this is not seen by the serializer. As a workaround, allow
   * the tokenizer to replace the iframe with div for tokenization.
   */
  rv = prefBranch->GetBoolPref("iframe_to_div", &mIframeToDiv);
  Din_Go(30,2048);if (NS_FAILED(rv))
    {/*3*/Din_Go(29,2048);mIframeToDiv = false;/*4*/}

  /*
   * the list of delimiters used to tokenize the message and body
   * defaults to the value in kBayesianFilterTokenDelimiters, but may be
   * set with the following preferences for the body and header
   * separately.
   *
   * \t, \n, \v, \f, \r, and \\ will be escaped to their normal
   * C-library values, all other two-letter combinations beginning with \
   * will be ignored.
   */

  Din_Go(31,2048);prefBranch->GetCharPref("body_delimiters", getter_Copies(mBodyDelimiters));
  Din_Go(34,2048);if (!mBodyDelimiters.IsEmpty())
    {/*5*/Din_Go(32,2048);UnescapeCString(mBodyDelimiters);/*6*/}
  else // prefBranch empties the result when it fails :(
    {/*7*/Din_Go(33,2048);mBodyDelimiters.Assign(kBayesianFilterTokenDelimiters);/*8*/}

  Din_Go(35,2048);prefBranch->GetCharPref("header_delimiters", getter_Copies(mHeaderDelimiters));
  Din_Go(38,2048);if (!mHeaderDelimiters.IsEmpty())
    {/*9*/Din_Go(36,2048);UnescapeCString(mHeaderDelimiters);/*10*/}
  else
    {/*11*/Din_Go(37,2048);mHeaderDelimiters.Assign(kBayesianFilterTokenDelimiters);/*12*/}

  /*
   * Extensions may wish to enable or disable tokenization of certain headers.
   * Define any headers to enable/disable in a string preference like this:
   *   "mailnews.bayesian_spam_filter.tokenizeheader.headername"
   *
   * where "headername" is the header to tokenize. For example, to tokenize the
   * header "x-spam-status" use the preference:
   *
   *   "mailnews.bayesian_spam_filter.tokenizeheader.x-spam-status"
   *
   * The value of the string preference will be interpreted in one of
   * four ways, depending on the value:
   *
   *   If "false" then do not tokenize that header
   *   If "full" then add the entire header value as a token,
   *     without breaking up into subtokens using delimiters
   *   If "standard" then tokenize the header using as delimiters the current
   *     value of the generic header delimiters
   *   Any other string is interpreted as a list of delimiters to use to parse
   *     the header. \t, \n, \v, \f, \r, and \\ will be escaped to their normal
   *     C-library values, all other two-letter combinations beginning with \
   *     will be ignored.
   *
   * Header names in the preference should be all lower case
   *
   * Extensions may also set the maximum length of a token (default is
   * kMaxLengthForToken) by setting the int preference:
   *   "mailnews.bayesian_spam_filter.maxlengthfortoken"
   */

  Din_Go(39,2048);char** headers;
  uint32_t count;

  // get customized maximum token length
  int32_t maxLengthForToken;
  rv = prefBranch->GetIntPref("maxlengthfortoken", &maxLengthForToken);
  mMaxLengthForToken = NS_SUCCEEDED(rv) ? uint32_t(maxLengthForToken) : kMaxLengthForToken;

  rv = prefs->GetBranch("mailnews.bayesian_spam_filter.tokenizeheader.", getter_AddRefs(prefBranch));
  Din_Go(41,2048);if (NS_SUCCEEDED(rv))
    {/*13*/Din_Go(40,2048);rv = prefBranch->GetChildList("", &count, &headers);/*14*/}

  Din_Go(55,2048);if (NS_SUCCEEDED(rv))
  {
    Din_Go(42,2048);mCustomHeaderTokenization = true;
    Din_Go(54,2048);for (uint32_t i = 0; i < count; i++)
    {
      Din_Go(43,2048);nsCString value;
      prefBranch->GetCharPref(headers[i], getter_Copies(value));
      Din_Go(46,2048);if (value.EqualsLiteral("false"))
      {
        Din_Go(44,2048);mDisabledHeaders.AppendElement(headers[i]);
        Din_Go(45,2048);continue;
      }
      Din_Go(47,2048);mEnabledHeaders.AppendElement(headers[i]);
      Din_Go(52,2048);if (value.EqualsLiteral("standard"))
        {/*15*/Din_Go(48,2048);value.SetIsVoid(true);/*16*/} // Void means use default delimiter
      else {/*17*/Din_Go(49,2048);if (value.EqualsLiteral("full"))
        {/*19*/Din_Go(50,2048);value.Truncate();/*20*/} // Empty means add full header
      else
        {/*21*/Din_Go(51,2048);UnescapeCString(value);/*22*/}/*18*/}
      Din_Go(53,2048);mEnabledHeadersDelimiters.AppendElement(value);
    }
    NS_FREE_XPCOM_ALLOCATED_POINTER_ARRAY(count, headers);
  }
Din_Go(56,2048);}

Tokenizer::~Tokenizer()
{
Din_Go(57,2048);Din_Go(58,2048);}

inline Token* Tokenizer::get(const char* word)
{
  return static_cast<Token*>(TokenHash::get(word));
}

Token* Tokenizer::add(const char* word, uint32_t count)
{
Din_Go(59,2048);  MOZ_LOG(BayesianFilterLogModule, LogLevel::Debug, ("add word: %s (count=%d)",
         word, count));

  Din_Go(60,2048);Token* token = static_cast<Token*>(TokenHash::add(word));
  Din_Go(62,2048);if (token)
  {
    Din_Go(61,2048);token->mCount += count; // hash code initializes this to zero
    MOZ_LOG(BayesianFilterLogModule, LogLevel::Debug,
           ("adding word to tokenizer: %s (count=%d) (mCount=%d)",
           word, count, token->mCount));
  }
  {__IASTRUCT__ Token * ReplaceReturn249 = token; Din_Go(63,2048); return ReplaceReturn249;}
}

static bool isDecimalNumber(const char* word)
{
    Din_Go(64,2048);const char* p = word;
    Din_Go(66,2048);if (*p == '-') {/*23*/Din_Go(65,2048);++p;/*24*/}
    Din_Go(67,2048);char c;
    Din_Go(70,2048);while ((c = *p++)) {
        Din_Go(68,2048);if (!isdigit((unsigned char) c))
            {/*25*/{_Bool  ReplaceReturn248 = false; Din_Go(69,2048); return ReplaceReturn248;}/*26*/}
    }
    {_Bool  ReplaceReturn247 = true; Din_Go(71,2048); return ReplaceReturn247;}
}

static bool isASCII(const char* word)
{
    Din_Go(72,2048);const unsigned char* p = (const unsigned char*)word;
    unsigned char c;
    Din_Go(75,2048);while ((c = *p++)) {
        Din_Go(73,2048);if (c > 127)
            {/*27*/{_Bool  ReplaceReturn246 = false; Din_Go(74,2048); return ReplaceReturn246;}/*28*/}
    }
    {_Bool  ReplaceReturn245 = true; Din_Go(76,2048); return ReplaceReturn245;}
}

inline bool isUpperCase(char c) { {_Bool  ReplaceReturn244 = ('A' <= c) && (c <= 'Z'); Din_Go(77,2048); return ReplaceReturn244;} }

static char* toLowerCase(char* str)
{
    Din_Go(78,2048);char c, *p = str;
    Din_Go(81,2048);while ((c = *p++)) {
        Din_Go(79,2048);if (isUpperCase(c))
            {/*29*/Din_Go(80,2048);p[-1] = c + ('a' - 'A');/*30*/}
    }
    {char * ReplaceReturn243 = str; Din_Go(82,2048); return ReplaceReturn243;}
}

void Tokenizer::addTokenForHeader(const char * aTokenPrefix, nsACString& aValue,
                                  bool aTokenizeValue, const char* aDelimiters)
{
  Din_Go(83,2048);if (aValue.Length())
  {
    Din_Go(84,2048);ToLowerCase(aValue);
    Din_Go(94,2048);if (!aTokenizeValue)
    {
      Din_Go(85,2048);nsCString tmpStr;
      tmpStr.Assign(aTokenPrefix);
      tmpStr.Append(':');
      tmpStr.Append(aValue);

      add(tmpStr.get());
    }
    else
    {
      Din_Go(86,2048);char* word;
      nsCString str(aValue);
      char *next = str.BeginWriting();
      const char* delimiters = !aDelimiters ?
          mHeaderDelimiters.get() : aDelimiters;
      Din_Go(93,2048);while ((word = NS_strtok(delimiters, &next)) != NULL)
      {
        Din_Go(87,2048);if (strlen(word) < kMinLengthForToken)
          {/*31*/Din_Go(88,2048);continue;/*32*/}
        Din_Go(90,2048);if (isDecimalNumber(word))
          {/*33*/Din_Go(89,2048);continue;/*34*/}
        Din_Go(92,2048);if (isASCII(word))
        {
          Din_Go(91,2048);nsCString tmpStr;
          tmpStr.Assign(aTokenPrefix);
          tmpStr.Append(':');
          tmpStr.Append(word);
          add(tmpStr.get());
        }
      }
    }
  }
Din_Go(95,2048);}

void Tokenizer::tokenizeAttachment(const char * aContentType, const char * aFileName)
{
  Din_Go(96,2048);nsAutoCString contentType;
  nsAutoCString fileName;
  fileName.Assign(aFileName);
  contentType.Assign(aContentType);

  // normalize the content type and the file name
  ToLowerCase(fileName);
  ToLowerCase(contentType);
  addTokenForHeader("attachment/filename", fileName);

  addTokenForHeader("attachment/content-type", contentType);
Din_Go(97,2048);}

void Tokenizer::tokenizeHeaders(nsIUTF8StringEnumerator * aHeaderNames, nsIUTF8StringEnumerator * aHeaderValues)
{
  Din_Go(98,2048);nsCString headerValue;
  nsAutoCString headerName; // we'll be normalizing all header names to lower case
  bool hasMore;
 
  Din_Go(137,2048);while (aHeaderNames->HasMore(&hasMore), hasMore)
  {
    Din_Go(99,2048);aHeaderNames->GetNext(headerName);
    ToLowerCase(headerName);
    aHeaderValues->GetNext(headerValue);

    bool headerProcessed = false;
    Din_Go(115,2048);if (mCustomHeaderTokenization)
    {
      // Process any exceptions set from preferences
      Din_Go(100,2048);for (uint32_t i = 0; i < mEnabledHeaders.Length(); i++)
        {/*35*/Din_Go(101,2048);if (headerName.Equals(mEnabledHeaders[i]))
        {
          Din_Go(102,2048);if (mEnabledHeadersDelimiters[i].IsVoid())
            // tokenize with standard delimiters for all headers
            {/*37*/Din_Go(103,2048);addTokenForHeader(headerName.get(), headerValue, true);/*38*/}
          else {/*39*/Din_Go(104,2048);if (mEnabledHeadersDelimiters[i].IsEmpty())
            // do not break the header into tokens
            {/*41*/Din_Go(105,2048);addTokenForHeader(headerName.get(), headerValue);/*42*/}
          else
            // use the delimiter in mEnabledHeadersDelimiters
            {/*43*/Din_Go(106,2048);addTokenForHeader(headerName.get(), headerValue, true,
                              mEnabledHeadersDelimiters[i].get());/*44*/}/*40*/}
          Din_Go(107,2048);headerProcessed = true;
          Din_Go(108,2048);break; // we found the header, no need to look for more custom values
        ;/*36*/}}

      Din_Go(112,2048);for (uint32_t i = 0; i < mDisabledHeaders.Length(); i++)
      {
        Din_Go(109,2048);if (headerName.Equals(mDisabledHeaders[i]))
        {
          Din_Go(110,2048);headerProcessed = true;
          Din_Go(111,2048);break;
        }
      }

      Din_Go(114,2048);if (headerProcessed)
        {/*45*/Din_Go(113,2048);continue;/*46*/}
    }

    Din_Go(136,2048);switch (headerName.First())
    {
    case 'c':
        Din_Go(116,2048);if (headerName.Equals("content-type"))
        {
          Din_Go(117,2048);nsresult rv;
          nsCOMPtr<nsIMIMEHeaderParam> mimehdrpar = do_GetService(NS_MIMEHEADERPARAM_CONTRACTID, &rv);
          Din_Go(119,2048);if (NS_FAILED(rv))
            {/*47*/Din_Go(118,2048);break;/*48*/}

          // extract the charset parameter
          Din_Go(120,2048);nsCString parameterValue;
          mimehdrpar->GetParameterInternal(headerValue.get(), "charset", nullptr, nullptr, getter_Copies(parameterValue));
          addTokenForHeader("charset", parameterValue);

          // create a token containing just the content type
          mimehdrpar->GetParameterInternal(headerValue.get(), "type", nullptr, nullptr, getter_Copies(parameterValue));
          Din_Go(122,2048);if (!parameterValue.Length())
            {/*49*/Din_Go(121,2048);mimehdrpar->GetParameterInternal(headerValue.get(), nullptr /* use first unnamed param */, nullptr, nullptr, getter_Copies(parameterValue));/*50*/}
          Din_Go(123,2048);addTokenForHeader("content-type/type", parameterValue);

          // XXX: should we add a token for the entire content-type header as well or just these parts we have extracted?
        }
        Din_Go(124,2048);break;
    case 'r':
      Din_Go(125,2048);if (headerName.Equals("received"))
      {
        // look for the string "may be forged" in the received headers. sendmail sometimes adds this hint
        // This does not compile on linux yet. Need to figure out why. Commenting out for now
        // if (FindInReadable(FORGED_RECEIVED_HEADER_HINT, headerValue))
        //   addTokenForHeader(headerName.get(), FORGED_RECEIVED_HEADER_HINT);
      }

      // leave out reply-to
      Din_Go(126,2048);break;
    case 's':
        Din_Go(127,2048);if (headerName.Equals("subject"))
        {
          // we want to tokenize the subject
          Din_Go(128,2048);addTokenForHeader(headerName.get(), headerValue, true);
        }

        // important: leave out sender field. Too strong of an indicator
        Din_Go(129,2048);break;
    case 'x': // (2) X-Mailer / user-agent works best if it is untokenized, just fold the case and any leading/trailing white space
        // all headers beginning with x-mozilla are being changed by us, so ignore
        Din_Go(130,2048);if (Substring(headerName, 0, 9).Equals("x-mozilla"))
          {/*51*/Din_Go(131,2048);break;/*52*/}
        // fall through
    case 'u':
        Din_Go(132,2048);addTokenForHeader(headerName.get(), headerValue);
        Din_Go(133,2048);break;
    default:
        Din_Go(134,2048);addTokenForHeader(headerName.get(), headerValue);
        Din_Go(135,2048);break;
    } // end switch

  }
Din_Go(138,2048);}

void Tokenizer::tokenize_ascii_word(char * aWord)
{
  // always deal with normalized lower case strings
  Din_Go(139,2048);toLowerCase(aWord);
  uint32_t wordLength = strlen(aWord);

  // if the wordLength is within our accepted token limit, then add it
  Din_Go(149,2048);if (wordLength >= kMinLengthForToken && wordLength <= mMaxLengthForToken)
    {/*53*/Din_Go(140,2048);add(aWord);/*54*/}
  else {/*55*/Din_Go(141,2048);if (wordLength > mMaxLengthForToken)
  {
    // don't skip over the word if it looks like an email address,
    // there is value in adding tokens for addresses
    Din_Go(142,2048);nsDependentCString word (aWord, wordLength); // CHEAP, no allocation occurs here...

    // XXX: i think the 40 byte check is just for perf reasons...if the email address is longer than that then forget about it.
    const char *atSign = strchr(aWord, '@');
    Din_Go(147,2048);if (wordLength < 40 && strchr(aWord, '.') && atSign && !strchr(atSign + 1, '@'))
    {
      Din_Go(143,2048);uint32_t numBytesToSep = atSign - aWord;
      Din_Go(146,2048);if (numBytesToSep < wordLength - 1) // if the @ sign is the last character, it must not be an email address
      {
        // split the john@foo.com into john and foo.com, treat them as separate tokens
        Din_Go(144,2048);nsCString emailNameToken;
        emailNameToken.AssignLiteral("email name:");
        emailNameToken.Append(Substring(word, 0, numBytesToSep++));
        add(emailNameToken.get());
        nsCString emailAddrToken;
        emailAddrToken.AssignLiteral("email addr:");
        emailAddrToken.Append(Substring(word, numBytesToSep, wordLength - numBytesToSep));
        add(emailAddrToken.get());
        Din_Go(145,2048);return;
      }
    }

    // there is value in generating a token indicating the number
    // of characters we are skipping. We'll round to the nearest 10
    Din_Go(148,2048);nsCString skipToken;
    skipToken.AssignLiteral("skip:");
    skipToken.Append(word[0]);
    skipToken.Append(' ');
    skipToken.AppendInt((wordLength/10) * 10);
    add(skipToken.get());
  ;/*56*/}}
Din_Go(150,2048);}

// one substract and one conditional jump should be faster than two conditional jump on most recent system.
#define IN_RANGE(x, low, high)  ((uint16_t)((x)-(low)) <= (high)-(low))

#define IS_JA_HIRAGANA(x)   IN_RANGE(x, 0x3040, 0x309F)
// swapping the range using xor operation to reduce conditional jump.
#define IS_JA_KATAKANA(x)	(IN_RANGE(x^0x0004, 0x30A0, 0x30FE)||(IN_RANGE(x, 0xFF66, 0xFF9F)))
#define IS_JA_KANJI(x)      (IN_RANGE(x, 0x2E80, 0x2FDF)||IN_RANGE(x, 0x4E00, 0x9FAF))
#define IS_JA_KUTEN(x)      (((x)==0x3001)||((x)==0xFF64)||((x)==0xFF0E))
#define IS_JA_TOUTEN(x)     (((x)==0x3002)||((x)==0xFF61)||((x)==0xFF0C))
#define IS_JA_SPACE(x)      ((x)==0x3000)
#define IS_JA_FWLATAIN(x)   IN_RANGE(x, 0xFF01, 0xFF5E)
#define IS_JA_FWNUMERAL(x)  IN_RANGE(x, 0xFF10, 0xFF19)

#define IS_JAPANESE_SPECIFIC(x) (IN_RANGE(x, 0x3040, 0x30FF)||IN_RANGE(x, 0xFF01, 0xFF9F))

enum char_class{
    others = 0,
    space,
    hiragana,
    katakana,
    kanji,
    kuten,
    touten,
    kigou,
    fwlatain,
    ascii
};

static char_class getCharClass(char16_t c)
{
  Din_Go(151,2048);char_class charClass = others;

  Din_Go(163,2048);if(IS_JA_HIRAGANA(c))
    {/*57*/Din_Go(152,2048);charClass = hiragana;/*58*/}
  else {/*59*/Din_Go(153,2048);if(IS_JA_KATAKANA(c))
    {/*61*/Din_Go(154,2048);charClass = katakana;/*62*/}
  else {/*63*/Din_Go(155,2048);if(IS_JA_KANJI(c))
    {/*65*/Din_Go(156,2048);charClass = kanji;/*66*/}
  else {/*67*/Din_Go(157,2048);if(IS_JA_KUTEN(c))
    {/*69*/Din_Go(158,2048);charClass = kuten;/*70*/}
  else {/*71*/Din_Go(159,2048);if(IS_JA_TOUTEN(c))
    {/*73*/Din_Go(160,2048);charClass = touten;/*74*/}
  else {/*75*/Din_Go(161,2048);if(IS_JA_FWLATAIN(c))
    {/*77*/Din_Go(162,2048);charClass = fwlatain;/*78*/}/*76*/}/*72*/}/*68*/}/*64*/}/*60*/}

  {__IAENUM__ char_class  ReplaceReturn242 = charClass; Din_Go(164,2048); return ReplaceReturn242;}
}

static bool isJapanese(const char* word)
{
  Din_Go(165,2048);nsString text = NS_ConvertUTF8toUTF16(word);
  char16_t* p = (char16_t*)text.get();
  char16_t c;

  // it is japanese chunk if it contains any hiragana or katakana.
  Din_Go(168,2048);while((c = *p++))
    {/*79*/Din_Go(166,2048);if( IS_JAPANESE_SPECIFIC(c))
      {/*81*/{_Bool  ReplaceReturn241 = true; Din_Go(167,2048); return ReplaceReturn241;}/*82*/}/*80*/}

  {_Bool  ReplaceReturn240 = false; Din_Go(169,2048); return ReplaceReturn240;}
}

static bool isFWNumeral(const char16_t* p1, const char16_t* p2)
{
  Din_Go(170,2048);for(;p1<p2;p1++)
    {/*83*/Din_Go(171,2048);if(!IS_JA_FWNUMERAL(*p1))
      {/*85*/{_Bool  ReplaceReturn239 = false; Din_Go(172,2048); return ReplaceReturn239;}/*86*/}/*84*/}

  {_Bool  ReplaceReturn238 = true; Din_Go(173,2048); return ReplaceReturn238;}
}

// The japanese tokenizer was added as part of Bug #277354
void Tokenizer::tokenize_japanese_word(char* chunk)
{
Din_Go(174,2048);  MOZ_LOG(BayesianFilterLogModule, LogLevel::Debug, ("entering tokenize_japanese_word(%s)", chunk));

  Din_Go(175,2048);nsString srcStr = NS_ConvertUTF8toUTF16(chunk);
  const char16_t* p1 = srcStr.get();
  const char16_t* p2 = p1;
  Din_Go(177,2048);if(!*p2) {/*87*/Din_Go(176,2048);return;/*88*/}

  Din_Go(178,2048);char_class cc = getCharClass(*p2);
  Din_Go(185,2048);while(*(++p2))
  {
    Din_Go(179,2048);if(cc == getCharClass(*p2))
      {/*89*/Din_Go(180,2048);continue;/*90*/}

    Din_Go(181,2048);nsCString token = NS_ConvertUTF16toUTF8(p1, p2-p1);
    Din_Go(183,2048);if( (!isDecimalNumber(token.get())) && (!isFWNumeral(p1, p2)))
    {
      Din_Go(182,2048);nsCString tmpStr;
      tmpStr.AppendLiteral("JA:");
      tmpStr.Append(token);
      add(tmpStr.get());
    }

    Din_Go(184,2048);cc = getCharClass(*p2);
    p1 = p2;
  }
Din_Go(186,2048);}

nsresult Tokenizer::stripHTML(const nsAString& inString, nsAString& outString)
{
  Din_Go(187,2048);uint32_t flags = nsIDocumentEncoder::OutputLFLineBreak
                 | nsIDocumentEncoder::OutputNoScriptContent
                 | nsIDocumentEncoder::OutputNoFramesContent
                 | nsIDocumentEncoder::OutputBodyOnly;
  nsCOMPtr<nsIParserUtils> utils =
    do_GetService(NS_PARSERUTILS_CONTRACTID);
  {__IAENUM__ nsresult  ReplaceReturn237 = utils->ConvertToPlainText(inString, flags, 80, outString); Din_Go(188,2048); return ReplaceReturn237;}
}

void Tokenizer::tokenize(const char* aText)
{
Din_Go(189,2048);  MOZ_LOG(BayesianFilterLogModule, LogLevel::Debug, ("tokenize: %s", aText));

  // strip out HTML tags before we begin processing
  // uggh but first we have to blow up our string into UCS2
  // since that's what the document encoder wants. UTF8/UCS2, I wish we all
  // spoke the same language here..
  Din_Go(190,2048);nsString text = NS_ConvertUTF8toUTF16(aText);
  nsString strippedUCS2;

  // RSS feeds store their summary information as an iframe. But due to
  // bug 365953, we can't see those in the plaintext serializer. As a
  // workaround, allow an option to replace iframe with div in the message
  // text. We disable by default, since most people won't be applying bayes
  // to RSS

  Din_Go(191,2048);if (mIframeToDiv)
  {
    MsgReplaceSubstring(text, NS_LITERAL_STRING("<iframe"),
                        NS_LITERAL_STRING("<div"));
    MsgReplaceSubstring(text, NS_LITERAL_STRING("/iframe>"),
                        NS_LITERAL_STRING("/div>"));
  }

  Din_Go(192,2048);stripHTML(text, strippedUCS2);

  // convert 0x3000(full width space) into 0x0020
  char16_t * substr_start = strippedUCS2.BeginWriting();
  char16_t * substr_end = strippedUCS2.EndWriting();
  Din_Go(196,2048);while (substr_start != substr_end) {
    Din_Go(193,2048);if (*substr_start == 0x3000)
        {/*91*/Din_Go(194,2048);*substr_start = 0x0020;/*92*/}
    Din_Go(195,2048);++substr_start;
  }

  Din_Go(197,2048);nsCString strippedStr = NS_ConvertUTF16toUTF8(strippedUCS2);
  char * strippedText = strippedStr.BeginWriting();
  MOZ_LOG(BayesianFilterLogModule, LogLevel::Debug, ("tokenize stripped html: %s", strippedText));

  char* word;
  char* next = strippedText;
  Din_Go(218,2048);while ((word = NS_strtok(mBodyDelimiters.get(), &next)) != NULL) {
    Din_Go(198,2048);if (!*word) {/*93*/Din_Go(199,2048);continue;/*94*/}
    Din_Go(201,2048);if (isDecimalNumber(word)) {/*95*/Din_Go(200,2048);continue;/*96*/}
    Din_Go(217,2048);if (isASCII(word))
        {/*97*/Din_Go(202,2048);tokenize_ascii_word(word);/*98*/}
    else {/*99*/Din_Go(203,2048);if (isJapanese(word))
        {/*101*/Din_Go(204,2048);tokenize_japanese_word(word);/*102*/}
    else {
        Din_Go(205,2048);nsresult rv;
        // use I18N  scanner to break this word into meaningful semantic units.
        Din_Go(209,2048);if (!mScanner) {
            Din_Go(206,2048);mScanner = do_CreateInstance(NS_SEMANTICUNITSCANNER_CONTRACTID, &rv);
            NS_ASSERTION(NS_SUCCEEDED(rv), "couldn't create semantic unit scanner!");
            Din_Go(208,2048);if (NS_FAILED(rv)) {
                Din_Go(207,2048);return;
            }
        }
        Din_Go(216,2048);if (mScanner) {
            Din_Go(210,2048);mScanner->Start("UTF-8");
            // convert this word from UTF-8 into UCS2.
            NS_ConvertUTF8toUTF16 uword(word);
            ToLowerCase(uword);
            const char16_t* utext = uword.get();
            int32_t len = uword.Length(), pos = 0, begin, end;
            bool gotUnit;
            Din_Go(215,2048);while (pos < len) {
                Din_Go(211,2048);rv = mScanner->Next(utext, len, pos, true, &begin, &end, &gotUnit);
                Din_Go(214,2048);if (NS_SUCCEEDED(rv) && gotUnit) {
                    Din_Go(212,2048);NS_ConvertUTF16toUTF8 utfUnit(utext + begin, end - begin);
                    add(utfUnit.get());
                    // advance to end of current unit.
                    pos = end;
                } else {
                    Din_Go(213,2048);break;
                }
            }
        }
    ;/*100*/}}
  }
Din_Go(219,2048);}

// helper function to escape \n, \t, etc from a CString
void Tokenizer::UnescapeCString(nsCString& aCString)
{
  Din_Go(220,2048);nsAutoCString result;

  const char* readEnd = aCString.EndReading();
  char* writeStart = result.BeginWriting();
  char* writeIter = writeStart;

  bool inEscape = false;
  Din_Go(240,2048);for (const char* readIter = aCString.BeginReading(); readIter != readEnd; readIter++)
  {
    Din_Go(221,2048);if (!inEscape)
    {
      Din_Go(222,2048);if (*readIter == '\\')
        {/*103*/Din_Go(223,2048);inEscape = true;/*104*/}
      else
        {/*105*/Din_Go(224,2048);*(writeIter++) = *readIter;/*106*/}
    }
    else
    {
      Din_Go(225,2048);inEscape = false;
      Din_Go(239,2048);switch (*readIter)
      {
        case '\\':
          Din_Go(226,2048);*(writeIter++) = '\\';
          Din_Go(227,2048);break;
        case 't':
          Din_Go(228,2048);*(writeIter++) = '\t';
          Din_Go(229,2048);break;
        case 'n':
          Din_Go(230,2048);*(writeIter++) = '\n';
          Din_Go(231,2048);break;
        case 'v':
          Din_Go(232,2048);*(writeIter++) = '\v';
          Din_Go(233,2048);break;
        case 'f':
          Din_Go(234,2048);*(writeIter++) = '\f';
          Din_Go(235,2048);break;
        case 'r':
          Din_Go(236,2048);*(writeIter++) = '\r';
          Din_Go(237,2048);break;
        default:
          // all other escapes are ignored
          Din_Go(238,2048);break;
      }
    }
  }
  Din_Go(241,2048);result.SetLength(writeIter - writeStart);
  aCString.Assign(result);
Din_Go(242,2048);}

Token* Tokenizer::copyTokens()
{
    Din_Go(243,2048);uint32_t count = countTokens();
    Din_Go(250,2048);if (count > 0) {
        Din_Go(244,2048);Token* tokens = new Token[count];
        Din_Go(248,2048);if (tokens) {
            Din_Go(245,2048);Token* tp = tokens;
            TokenEnumeration e(&mTokenTable);
            Din_Go(247,2048);while (e.hasMoreTokens())
                {/*107*/Din_Go(246,2048);*tp++ = *(static_cast<Token*>(e.nextToken()));/*108*/}
        }
        {__IASTRUCT__ Token * ReplaceReturn236 = tokens; Din_Go(249,2048); return ReplaceReturn236;}
    }
    {__IASTRUCT__ Token * ReplaceReturn235 = NULL; Din_Go(251,2048); return ReplaceReturn235;}
}

class TokenAnalyzer {
public:
    virtual ~TokenAnalyzer() {}

    virtual void analyzeTokens(Tokenizer& tokenizer) = 0;
    void setTokenListener(nsIStreamListener *aTokenListener)
    {
      mTokenListener = aTokenListener;
    }

    void setSource(const char *sourceURI) {mTokenSource = sourceURI;}

    nsCOMPtr<nsIStreamListener> mTokenListener;
    nsCString mTokenSource;

};

/**
 * This class downloads the raw content of an email message, buffering until
 * complete segments are seen, that is until a linefeed is seen, although
 * any of the valid token separators would do. This could be a further
 * refinement.
 */
class TokenStreamListener : public nsIStreamListener, nsIMsgHeaderSink {
public:
    NS_DECL_ISUPPORTS
    NS_DECL_NSIREQUESTOBSERVER
    NS_DECL_NSISTREAMLISTENER
    NS_DECL_NSIMSGHEADERSINK

    TokenStreamListener(TokenAnalyzer* analyzer);
protected:
    virtual ~TokenStreamListener();
    TokenAnalyzer* mAnalyzer;
    char* mBuffer;
    uint32_t mBufferSize;
    uint32_t mLeftOverCount;
    Tokenizer mTokenizer;
    bool mSetAttachmentFlag;
};

const uint32_t kBufferSize = 16384;

TokenStreamListener::TokenStreamListener(TokenAnalyzer* analyzer)
    :   mAnalyzer(analyzer),
        mBuffer(NULL), mBufferSize(kBufferSize), mLeftOverCount(0),
        mSetAttachmentFlag(false)
{
Din_Go(252,2048);Din_Go(253,2048);}

TokenStreamListener::~TokenStreamListener()
{
    Din_Go(254,2048);delete[] mBuffer;
    delete mAnalyzer;
Din_Go(255,2048);}

NS_IMPL_ISUPPORTS(TokenStreamListener, nsIRequestObserver, nsIStreamListener, nsIMsgHeaderSink)

NS_IMETHODIMP TokenStreamListener::ProcessHeaders(nsIUTF8StringEnumerator *aHeaderNames, nsIUTF8StringEnumerator *aHeaderValues, bool dontCollectAddress)
{
    Din_Go(256,2048);mTokenizer.tokenizeHeaders(aHeaderNames, aHeaderValues);
    {__IAENUM__ nsresult  ReplaceReturn234 = NS_OK; Din_Go(257,2048); return ReplaceReturn234;}
}

NS_IMETHODIMP TokenStreamListener::HandleAttachment(const char *contentType, const char *url, const char16_t *displayName, const char *uri, bool aIsExternalAttachment)
{
    Din_Go(258,2048);mTokenizer.tokenizeAttachment(contentType, NS_ConvertUTF16toUTF8(displayName).get());
    {__IAENUM__ nsresult  ReplaceReturn233 = NS_OK; Din_Go(259,2048); return ReplaceReturn233;}
}

NS_IMETHODIMP TokenStreamListener::AddAttachmentField(const char *field, const char *value)
{
    {__IAENUM__ nsresult  ReplaceReturn232 = NS_OK; Din_Go(260,2048); return ReplaceReturn232;}
}

NS_IMETHODIMP TokenStreamListener::OnEndAllAttachments()
{
    {__IAENUM__ nsresult  ReplaceReturn231 = NS_OK; Din_Go(261,2048); return ReplaceReturn231;}
}

NS_IMETHODIMP TokenStreamListener::OnEndMsgDownload(nsIMsgMailNewsUrl *url)
{
    {__IAENUM__ nsresult  ReplaceReturn230 = NS_OK; Din_Go(262,2048); return ReplaceReturn230;}
}


NS_IMETHODIMP TokenStreamListener::OnMsgHasRemoteContent(nsIMsgDBHdr *aMsgHdr,
                                                         nsIURI *aContentURI)
{
    {__IAENUM__ nsresult  ReplaceReturn229 = NS_OK; Din_Go(263,2048); return ReplaceReturn229;}
}

NS_IMETHODIMP TokenStreamListener::OnEndMsgHeaders(nsIMsgMailNewsUrl *url)
{
    {__IAENUM__ nsresult  ReplaceReturn228 = NS_OK; Din_Go(264,2048); return ReplaceReturn228;}
}


NS_IMETHODIMP TokenStreamListener::GetSecurityInfo(nsISupports * *aSecurityInfo)
{
    {__IAENUM__ nsresult  ReplaceReturn227 = NS_OK; Din_Go(265,2048); return ReplaceReturn227;}
}
NS_IMETHODIMP TokenStreamListener::SetSecurityInfo(nsISupports * aSecurityInfo)
{
    {__IAENUM__ nsresult  ReplaceReturn226 = NS_OK; Din_Go(266,2048); return ReplaceReturn226;}
}

NS_IMETHODIMP TokenStreamListener::GetDummyMsgHeader(nsIMsgDBHdr **aMsgDBHdr)
{
  {__IAENUM__ nsresult  ReplaceReturn225 = NS_ERROR_NOT_IMPLEMENTED; Din_Go(267,2048); return ReplaceReturn225;}
}

NS_IMETHODIMP TokenStreamListener::ResetProperties()
{
  {__IAENUM__ nsresult  ReplaceReturn224 = NS_OK; Din_Go(268,2048); return ReplaceReturn224;}
}

NS_IMETHODIMP TokenStreamListener::GetProperties(nsIWritablePropertyBag2 * *aProperties)
{
  {__IAENUM__ nsresult  ReplaceReturn223 = NS_ERROR_NOT_IMPLEMENTED; Din_Go(269,2048); return ReplaceReturn223;}
}

/* void onStartRequest (in nsIRequest aRequest, in nsISupports aContext); */
NS_IMETHODIMP TokenStreamListener::OnStartRequest(nsIRequest *aRequest, nsISupports *aContext)
{
    Din_Go(270,2048);mLeftOverCount = 0;
    Din_Go(272,2048);if (!mBuffer)
    {
        Din_Go(271,2048);mBuffer = new char[mBufferSize];
        NS_ENSURE_TRUE(mBuffer, NS_ERROR_OUT_OF_MEMORY);
    }

    // get the url for the channel and set our nsIMsgHeaderSink on it so we get notified
    // about the headers and attachments

    Din_Go(273,2048);nsCOMPtr<nsIChannel> channel (do_QueryInterface(aRequest));
    Din_Go(277,2048);if (channel)
    {
        Din_Go(274,2048);nsCOMPtr<nsIURI> uri;
        channel->GetURI(getter_AddRefs(uri));
        nsCOMPtr<nsIMsgMailNewsUrl> mailUrl = do_QueryInterface(uri);
        Din_Go(276,2048);if (mailUrl)
            {/*109*/Din_Go(275,2048);mailUrl->SetMsgHeaderSink(static_cast<nsIMsgHeaderSink*>(this));/*110*/}
    }

    {__IAENUM__ nsresult  ReplaceReturn222 = NS_OK; Din_Go(278,2048); return ReplaceReturn222;}
}

/* void onDataAvailable (in nsIRequest aRequest, in nsISupports aContext, in nsIInputStream aInputStream, in unsigned long long aOffset, in unsigned long aCount); */
NS_IMETHODIMP TokenStreamListener::OnDataAvailable(nsIRequest *aRequest, nsISupports *aContext, nsIInputStream *aInputStream, uint64_t aOffset, uint32_t aCount)
{
    Din_Go(279,2048);nsresult rv = NS_OK;

    Din_Go(304,2048);while (aCount > 0) {
        Din_Go(280,2048);uint32_t readCount, totalCount = (aCount + mLeftOverCount);
        Din_Go(283,2048);if (totalCount >= mBufferSize) {
            Din_Go(281,2048);readCount = mBufferSize - mLeftOverCount - 1;
        } else {
            Din_Go(282,2048);readCount = aCount;
        }

        // mBuffer is supposed to be allocated in onStartRequest. But something
        // is causing that to not happen, so as a last-ditch attempt we'll
        // do it here.
        Din_Go(285,2048);if (!mBuffer)
        {
          Din_Go(284,2048);mBuffer = new char[mBufferSize];
          NS_ENSURE_TRUE(mBuffer, NS_ERROR_OUT_OF_MEMORY);
        }

        Din_Go(286,2048);char* buffer = mBuffer;
        rv = aInputStream->Read(buffer + mLeftOverCount, readCount, &readCount);
        Din_Go(288,2048);if (NS_FAILED(rv))
            {/*111*/Din_Go(287,2048);break;/*112*/}

        Din_Go(291,2048);if (readCount == 0) {
            Din_Go(289,2048);rv = NS_ERROR_UNEXPECTED;
            NS_WARNING("failed to tokenize");
            Din_Go(290,2048);break;
        }

        Din_Go(292,2048);aCount -= readCount;

        /* consume the tokens up to the last legal token delimiter in the buffer. */
        totalCount = (readCount + mLeftOverCount);
        buffer[totalCount] = '\0';
        char* lastDelimiter = NULL;
        char* scan = buffer + totalCount;
        Din_Go(296,2048);while (scan > buffer) {
            Din_Go(293,2048);if (strchr(mTokenizer.mBodyDelimiters.get(), *--scan)) {
                Din_Go(294,2048);lastDelimiter = scan;
                Din_Go(295,2048);break;
            }
        }

        Din_Go(303,2048);if (lastDelimiter) {
            Din_Go(297,2048);*lastDelimiter = '\0';
            mTokenizer.tokenize(buffer);

            uint32_t consumedCount = 1 + (lastDelimiter - buffer);
            mLeftOverCount = totalCount - consumedCount;
            Din_Go(299,2048);if (mLeftOverCount)
                {/*113*/Din_Go(298,2048);memmove(buffer, buffer + consumedCount, mLeftOverCount);/*114*/}
        } else {
            /* didn't find a delimiter, keep the whole buffer around. */
            Din_Go(300,2048);mLeftOverCount = totalCount;
            Din_Go(302,2048);if (totalCount >= (mBufferSize / 2)) {
                Din_Go(301,2048);uint32_t newBufferSize = mBufferSize * 2;
                char* newBuffer = new char[newBufferSize];
                NS_ENSURE_TRUE(newBuffer, NS_ERROR_OUT_OF_MEMORY);
                memcpy(newBuffer, mBuffer, mLeftOverCount);
                delete[] mBuffer;
                mBuffer = newBuffer;
                mBufferSize = newBufferSize;
            }
        }
    }

    {__IAENUM__ nsresult  ReplaceReturn221 = rv; Din_Go(305,2048); return ReplaceReturn221;}
}

/* void onStopRequest (in nsIRequest aRequest, in nsISupports aContext, in nsresult aStatusCode); */
NS_IMETHODIMP TokenStreamListener::OnStopRequest(nsIRequest *aRequest, nsISupports *aContext, nsresult aStatusCode)
{
    Din_Go(306,2048);if (mLeftOverCount) {
        /* assume final buffer is complete. */
        Din_Go(307,2048);mBuffer[mLeftOverCount] = '\0';
        mTokenizer.tokenize(mBuffer);
    }

    /* finally, analyze the tokenized message. */
    MOZ_LOG(BayesianFilterLogModule, LogLevel::Debug, ("analyze the tokenized message"));
    Din_Go(309,2048);if (mAnalyzer)
        {/*115*/Din_Go(308,2048);mAnalyzer->analyzeTokens(mTokenizer);/*116*/}

    {__IAENUM__ nsresult  ReplaceReturn220 = NS_OK; Din_Go(310,2048); return ReplaceReturn220;}
}

/* Implementation file */

NS_IMPL_ISUPPORTS(nsBayesianFilter, nsIMsgFilterPlugin,
                   nsIJunkMailPlugin, nsIMsgCorpus, nsISupportsWeakReference,
                   nsIObserver)

nsBayesianFilter::nsBayesianFilter()
    :   mTrainingDataDirty(false)
{
    Din_Go(311,2048);if (!BayesianFilterLogModule)
      {/*117*/Din_Go(312,2048);BayesianFilterLogModule = PR_NewLogModule("BayesianFilter");/*118*/}

    Din_Go(313,2048);int32_t junkThreshold = 0;
    nsresult rv;
    nsCOMPtr<nsIPrefBranch> pPrefBranch(do_GetService(NS_PREFSERVICE_CONTRACTID, &rv));
    Din_Go(315,2048);if (pPrefBranch)
      {/*119*/Din_Go(314,2048);pPrefBranch->GetIntPref("mail.adaptivefilters.junk_threshold", &junkThreshold);/*120*/}

    Din_Go(316,2048);mJunkProbabilityThreshold = (static_cast<double>(junkThreshold)) / 100.0;
    if (mJunkProbabilityThreshold == 0 || mJunkProbabilityThreshold >= 1)
      mJunkProbabilityThreshold = kDefaultJunkThreshold;

    MOZ_LOG(BayesianFilterLogModule, LogLevel::Warning, ("junk probability threshold: %f", mJunkProbabilityThreshold));

    Din_Go(317,2048);mCorpus.readTrainingData();

    // get parameters for training data flushing, from the prefs

    nsCOMPtr<nsIPrefBranch> prefBranch;

    nsCOMPtr<nsIPrefService> prefs = do_GetService(NS_PREFSERVICE_CONTRACTID, &rv);
    NS_ASSERTION(NS_SUCCEEDED(rv),"failed accessing preferences service");
    rv = prefs->GetBranch(nullptr, getter_AddRefs(prefBranch));
    NS_ASSERTION(NS_SUCCEEDED(rv),"failed getting preferences branch");

    rv = prefBranch->GetIntPref("mailnews.bayesian_spam_filter.flush.minimum_interval",&mMinFlushInterval);
    // it is not a good idea to allow a minimum interval of under 1 second
    Din_Go(318,2048);if (NS_FAILED(rv) || (mMinFlushInterval <= 1000) )
        mMinFlushInterval = DEFAULT_MIN_INTERVAL_BETWEEN_WRITES;

    Din_Go(319,2048);rv = prefBranch->GetIntPref("mailnews.bayesian_spam_filter.junk_maxtokens", &mMaximumTokenCount);
    Din_Go(321,2048);if (NS_FAILED(rv))
      {/*123*/Din_Go(320,2048);mMaximumTokenCount = 0;/*124*/} // which means do not limit token counts
    MOZ_LOG(BayesianFilterLogModule, LogLevel::Warning, ("maximum junk tokens: %d", mMaximumTokenCount));

    Din_Go(322,2048);mTimer = do_CreateInstance(NS_TIMER_CONTRACTID, &rv);
    NS_ASSERTION(NS_SUCCEEDED(rv), "unable to create a timer; training data will only be written on exit");

    // the timer is not used on object construction, since for
    // the time being there are no dirying messages

    // give a default capacity to the memory structure used to store
    // per-message/per-trait token data
    mAnalysisStore.SetCapacity(kAnalysisStoreCapacity);

    // dummy 0th element. Index 0 means "end of list" so we need to
    // start from 1
    AnalysisPerToken analysisPT(0, 0.0, 0.0);
    mAnalysisStore.AppendElement(analysisPT);
    mNextAnalysisIndex = 1;
Din_Go(323,2048);}

nsresult nsBayesianFilter::Init()
{
  Din_Go(324,2048);nsCOMPtr<nsIObserverService> observerService =
    mozilla::services::GetObserverService();
  Din_Go(326,2048);if (observerService)
    {/*125*/Din_Go(325,2048);observerService->AddObserver(this, "profile-before-change", true);/*126*/}
  {__IAENUM__ nsresult  ReplaceReturn219 = NS_OK; Din_Go(327,2048); return ReplaceReturn219;}
}

void
nsBayesianFilter::TimerCallback(nsITimer* aTimer, void* aClosure)
{
    // we will flush the training data to disk after enough time has passed
    // since the first time a message has been classified after the last flush

    Din_Go(328,2048);nsBayesianFilter *filter = static_cast<nsBayesianFilter *>(aClosure);
    filter->mCorpus.writeTrainingData(filter->mMaximumTokenCount);
    filter->mTrainingDataDirty = false;
Din_Go(329,2048);}

nsBayesianFilter::~nsBayesianFilter()
{
    Din_Go(330,2048);if (mTimer)
    {
        Din_Go(331,2048);mTimer->Cancel();
        mTimer = nullptr;
    }
    // call shutdown when we are going away in case we need
    // to flush the training set to disk
    Din_Go(332,2048);Shutdown();
Din_Go(333,2048);}

// this object is used for one call to classifyMessage or classifyMessages().
// So if we're classifying multiple messages, this object will be used for each message.
// It's going to hold a reference to itself, basically, to stay in memory.
class MessageClassifier : public TokenAnalyzer {
public:
    // full classifier with arbitrary traits
    MessageClassifier(nsBayesianFilter* aFilter,
                      nsIJunkMailClassificationListener* aJunkListener,
                      nsIMsgTraitClassificationListener* aTraitListener,
                      nsIMsgTraitDetailListener* aDetailListener,
                      nsTArray<uint32_t>& aProTraits,
                      nsTArray<uint32_t>& aAntiTraits,
                      nsIMsgWindow *aMsgWindow,
                      uint32_t aNumMessagesToClassify,
                      const char **aMessageURIs)
    :   mFilter(aFilter),
        mJunkMailPlugin(aFilter),
        mJunkListener(aJunkListener),
        mTraitListener(aTraitListener),
        mDetailListener(aDetailListener),
        mProTraits(aProTraits),
        mAntiTraits(aAntiTraits),
        mMsgWindow(aMsgWindow)
    {
      mCurMessageToClassify = 0;
      mNumMessagesToClassify = aNumMessagesToClassify;
      mMessageURIs = (char **) moz_xmalloc(sizeof(char *) * aNumMessagesToClassify);
      for (uint32_t i = 0; i < aNumMessagesToClassify; i++)
        mMessageURIs[i] = PL_strdup(aMessageURIs[i]);

    }

    // junk-only classifier
    MessageClassifier(nsBayesianFilter* aFilter,
                      nsIJunkMailClassificationListener* aJunkListener,
                      nsIMsgWindow *aMsgWindow,
                      uint32_t aNumMessagesToClassify,
                      const char **aMessageURIs)
    :   mFilter(aFilter),
        mJunkMailPlugin(aFilter),
        mJunkListener(aJunkListener),
        mTraitListener(nullptr),
        mDetailListener(nullptr),
        mMsgWindow(aMsgWindow)
    {
      mCurMessageToClassify = 0;
      mNumMessagesToClassify = aNumMessagesToClassify;
      mMessageURIs = (char **) moz_xmalloc(sizeof(char *) * aNumMessagesToClassify);
      for (uint32_t i = 0; i < aNumMessagesToClassify; i++)
        mMessageURIs[i] = PL_strdup(aMessageURIs[i]);
      mProTraits.AppendElement(kJunkTrait);
      mAntiTraits.AppendElement(kGoodTrait);

    }

    virtual ~MessageClassifier()
    {
       if (mMessageURIs)
       {
         NS_FREE_XPCOM_ALLOCATED_POINTER_ARRAY(mNumMessagesToClassify, mMessageURIs);
       }
    }
    virtual void analyzeTokens(Tokenizer& tokenizer)
    {
        mFilter->classifyMessage(tokenizer,
                                 mTokenSource.get(),
                                 mProTraits,
                                 mAntiTraits,
                                 mJunkListener,
                                 mTraitListener,
                                 mDetailListener);
        tokenizer.clearTokens();
        classifyNextMessage();
    }

    virtual void classifyNextMessage()
    {

      if (++mCurMessageToClassify < mNumMessagesToClassify && mMessageURIs[mCurMessageToClassify]) {
        MOZ_LOG(BayesianFilterLogModule, LogLevel::Warning, ("classifyNextMessage(%s)", mMessageURIs[mCurMessageToClassify]));
        mFilter->tokenizeMessage(mMessageURIs[mCurMessageToClassify], mMsgWindow, this);
      }
      else
      {
        // call all listeners with null parameters to signify end of batch
        if (mJunkListener)
          mJunkListener->OnMessageClassified(nullptr, nsIJunkMailPlugin::UNCLASSIFIED, 0);
        if (mTraitListener)
          mTraitListener->OnMessageTraitsClassified(nullptr, 0, nullptr, nullptr);
        mTokenListener = nullptr; // this breaks the circular ref that keeps this object alive
                                 // so we will be destroyed as a result.
      }
    }

private:
    nsBayesianFilter* mFilter;
    nsCOMPtr<nsIJunkMailPlugin> mJunkMailPlugin;
    nsCOMPtr<nsIJunkMailClassificationListener> mJunkListener;
    nsCOMPtr<nsIMsgTraitClassificationListener> mTraitListener;
    nsCOMPtr<nsIMsgTraitDetailListener> mDetailListener;
    nsTArray<uint32_t> mProTraits;
    nsTArray<uint32_t> mAntiTraits;
    nsCOMPtr<nsIMsgWindow> mMsgWindow;
    int32_t mNumMessagesToClassify;
    int32_t mCurMessageToClassify; // 0-based index
    char **mMessageURIs;
};

nsresult nsBayesianFilter::tokenizeMessage(const char* aMessageURI, nsIMsgWindow *aMsgWindow, TokenAnalyzer* aAnalyzer)
{
Din_Go(334,2048);    NS_ENSURE_ARG_POINTER(aMessageURI);

    Din_Go(335,2048);nsCOMPtr <nsIMsgMessageService> msgService;
    nsresult rv = GetMessageServiceFromURI(nsDependentCString(aMessageURI), getter_AddRefs(msgService));
    NS_ENSURE_SUCCESS(rv, rv);

    aAnalyzer->setSource(aMessageURI);
    {__IAENUM__ nsresult  ReplaceReturn218 = msgService->StreamMessage(aMessageURI, aAnalyzer->mTokenListener,
                                     aMsgWindow, nullptr, true /* convert data */,
                                     NS_LITERAL_CSTRING("filter"), false, nullptr); Din_Go(336,2048); return ReplaceReturn218;}
}

// a TraitAnalysis is the per-token representation of the statistical
// calculations, basically created to group information that is then
// sorted by mDistance
struct TraitAnalysis
{
  uint32_t mTokenIndex;
  double mDistance;
  double mProbability;
};

// comparator required to sort an nsTArray
class compareTraitAnalysis
{
public:
  bool Equals(const TraitAnalysis& a, const TraitAnalysis& b) const
  {
    return a.mDistance == b.mDistance;
  }
  bool LessThan(const TraitAnalysis& a, const TraitAnalysis& b) const
  {
    return a.mDistance < b.mDistance;
  }
};

inline double dmax(double x, double y) { {double  ReplaceReturn217 = (x > y ? x : y); Din_Go(337,2048); return ReplaceReturn217;} }
inline double dmin(double x, double y) { {double  ReplaceReturn216 = (x < y ? x : y); Din_Go(338,2048); return ReplaceReturn216;} }

// Chi square functions are implemented by an incomplete gamma function.
// Note that chi2P's callers multiply the arguments by 2 but chi2P
// divides them by 2 again. Inlining chi2P gives the compiler a
// chance to notice this.

// Both chi2P and nsIncompleteGammaP set *error negative on domain
// errors and nsIncompleteGammaP sets it posivive on internal errors.
// This may be useful but the chi2P callers treat any error as fatal.

// Note that converting unsigned ints to floating point can be slow on
// some platforms (like Intel) so use signed quantities for the numeric
// routines.
static inline double chi2P (double chi2, double nu, int32_t *error)
{
    // domain checks; set error and return a dummy value
    Din_Go(339,2048);if (chi2 < 0.0 || nu <= 0.0)
    {
        Din_Go(340,2048);*error = -1;
        {double  ReplaceReturn215 = 0.0; Din_Go(341,2048); return ReplaceReturn215;}
    }
    // reversing the arguments is intentional
    {double  ReplaceReturn214 = nsIncompleteGammaP (nu/2.0, chi2/2.0, error); Din_Go(342,2048); return ReplaceReturn214;}
}

void nsBayesianFilter::classifyMessage(
  Tokenizer& tokenizer,
  const char* messageURI,
  nsTArray<uint32_t>& aProTraits,
  nsTArray<uint32_t>& aAntiTraits,
  nsIJunkMailClassificationListener* listener,
  nsIMsgTraitClassificationListener* aTraitListener,
  nsIMsgTraitDetailListener* aDetailListener)
{
    Din_Go(343,2048);Token* tokens = tokenizer.copyTokens();
    uint32_t tokenCount;
    Din_Go(346,2048);if (!tokens)
    {
      // This can happen with problems with UTF conversion
      NS_ERROR("Trying to classify a null or invalid message");
      Din_Go(344,2048);tokenCount = 0;
      // don't return so that we still call the listeners
    }
    else
    {
      Din_Go(345,2048);tokenCount = tokenizer.countTokens();
    }

    Din_Go(348,2048);if (aProTraits.Length() != aAntiTraits.Length())
    {
      NS_ERROR("Each Pro trait needs a matching Anti trait");
      Din_Go(347,2048);return;
    }

    /* this part is similar to the Graham algorithm with some adjustments. */
    Din_Go(349,2048);uint32_t traitCount = aProTraits.Length();

    // pro message counts per trait index
    nsAutoTArray<uint32_t, kTraitAutoCapacity> numProMessages;
    // anti message counts per trait index
    nsAutoTArray<uint32_t, kTraitAutoCapacity> numAntiMessages;
    // array of pro aliases per trait index
    nsAutoTArray<uint32_t*, kTraitAutoCapacity > proAliasArrays;
    // number of pro aliases per trait index
    nsAutoTArray<uint32_t, kTraitAutoCapacity > proAliasesLengths;    
    // array of anti aliases per trait index
    nsAutoTArray<uint32_t*, kTraitAutoCapacity> antiAliasArrays;
    // number of anti aliases per trait index
    nsAutoTArray<uint32_t, kTraitAutoCapacity > antiAliasesLengths;    
    // construct the outgoing listener arrays
    nsAutoTArray<uint32_t, kTraitAutoCapacity> traits;
    nsAutoTArray<uint32_t, kTraitAutoCapacity> percents;
    Din_Go(351,2048);if (traitCount > kTraitAutoCapacity)
    {
      Din_Go(350,2048);traits.SetCapacity(traitCount);
      percents.SetCapacity(traitCount);
      numProMessages.SetCapacity(traitCount);
      numAntiMessages.SetCapacity(traitCount);
      proAliasesLengths.SetCapacity(traitCount);
      antiAliasesLengths.SetCapacity(traitCount);
      proAliasArrays.SetCapacity(traitCount);
      antiAliasArrays.SetCapacity(traitCount);
    }

    Din_Go(352,2048);nsresult rv;
    nsCOMPtr<nsIMsgTraitService> traitService(do_GetService("@mozilla.org/msg-trait-service;1", &rv));
    Din_Go(353,2048);if (NS_FAILED(rv))
    {
      NS_ERROR("Failed to get trait service");
      MOZ_LOG(BayesianFilterLogModule, LogLevel::Error, ("Failed to get trait service"));
    }

    // get aliases and message counts for the pro and anti traits
    Din_Go(369,2048);for (uint32_t traitIndex = 0; traitIndex < traitCount; traitIndex++)
    {
      Din_Go(354,2048);nsresult rv;

      // pro trait
      uint32_t proAliasesLength = 0;
      uint32_t* proAliases = nullptr;
      uint32_t proTrait = aProTraits[traitIndex];
      Din_Go(357,2048);if (traitService)
      {
        Din_Go(355,2048);rv = traitService->GetAliases(proTrait, &proAliasesLength, &proAliases);
        Din_Go(356,2048);if (NS_FAILED(rv))
        {
          NS_ERROR("trait service failed to get aliases");
          MOZ_LOG(BayesianFilterLogModule, LogLevel::Error, ("trait service failed to get aliases"));
        }
      }
      Din_Go(358,2048);proAliasesLengths.AppendElement(proAliasesLength);
      proAliasArrays.AppendElement(proAliases);
      uint32_t proMessageCount = mCorpus.getMessageCount(proTrait);
      Din_Go(360,2048);for (uint32_t aliasIndex = 0; aliasIndex < proAliasesLength; aliasIndex++)
        {/*127*/Din_Go(359,2048);proMessageCount += mCorpus.getMessageCount(proAliases[aliasIndex]);/*128*/}
      Din_Go(361,2048);numProMessages.AppendElement(proMessageCount);

      // anti trait
      uint32_t antiAliasesLength = 0;
      uint32_t* antiAliases = nullptr;
      uint32_t antiTrait = aAntiTraits[traitIndex];
      Din_Go(364,2048);if (traitService)
      {
        Din_Go(362,2048);rv = traitService->GetAliases(antiTrait, &antiAliasesLength, &antiAliases);
        Din_Go(363,2048);if (NS_FAILED(rv))
        {
          NS_ERROR("trait service failed to get aliases");
          MOZ_LOG(BayesianFilterLogModule, LogLevel::Error, ("trait service failed to get aliases"));
        }
      }
      Din_Go(365,2048);antiAliasesLengths.AppendElement(antiAliasesLength);
      antiAliasArrays.AppendElement(antiAliases);
      uint32_t antiMessageCount = mCorpus.getMessageCount(antiTrait);
      Din_Go(367,2048);for (uint32_t aliasIndex = 0; aliasIndex < antiAliasesLength; aliasIndex++)
        {/*129*/Din_Go(366,2048);antiMessageCount += mCorpus.getMessageCount(antiAliases[aliasIndex]);/*130*/}
      Din_Go(368,2048);numAntiMessages.AppendElement(antiMessageCount);
    }

    Din_Go(393,2048);for (uint32_t i = 0; i < tokenCount; ++i)
    {
      Din_Go(370,2048);Token& token = tokens[i];
      CorpusToken* t = mCorpus.get(token.mWord);
      Din_Go(372,2048);if (!t)
        {/*131*/Din_Go(371,2048);continue;/*132*/}
      Din_Go(392,2048);for (uint32_t traitIndex = 0; traitIndex < traitCount; traitIndex++)
      {
        Din_Go(373,2048);uint32_t iProCount = mCorpus.getTraitCount(t, aProTraits[traitIndex]);
        // add in any counts for aliases to proTrait
        Din_Go(375,2048);for (uint32_t aliasIndex = 0; aliasIndex < proAliasesLengths[traitIndex]; aliasIndex++)
          {/*133*/Din_Go(374,2048);iProCount += mCorpus.getTraitCount(t, proAliasArrays[traitIndex][aliasIndex]);/*134*/}
        Din_Go(376,2048);double proCount = static_cast<double>(iProCount);

        uint32_t iAntiCount = mCorpus.getTraitCount(t, aAntiTraits[traitIndex]);
        // add in any counts for aliases to antiTrait
        Din_Go(378,2048);for (uint32_t aliasIndex = 0; aliasIndex < antiAliasesLengths[traitIndex]; aliasIndex++)
          {/*135*/Din_Go(377,2048);iAntiCount += mCorpus.getTraitCount(t, antiAliasArrays[traitIndex][aliasIndex]);/*136*/}
        Din_Go(379,2048);double antiCount = static_cast<double>(iAntiCount);

        double prob, denom;
        // Prevent a divide by zero error by setting defaults for prob

        // If there are no matching tokens at all, ignore.
        Din_Go(381,2048);if (antiCount == 0.0 && proCount == 0.0)
          {/*137*/Din_Go(380,2048);continue;/*138*/}
        // if only anti match, set probability to 0%
        Din_Go(388,2048);if (proCount == 0.0)
          {/*139*/Din_Go(382,2048);prob = 0.0;/*140*/}
        // if only pro match, set probability to 100%
        else {/*141*/Din_Go(383,2048);if (antiCount == 0.0)
          {/*143*/Din_Go(384,2048);prob = 1.0;/*144*/}
        // not really needed, but just to be sure check the denom as well
        else {/*145*/Din_Go(385,2048);if ((denom = proCount * numAntiMessages[traitIndex] +
                          antiCount * numProMessages[traitIndex]) == 0.0)
          {/*147*/Din_Go(386,2048);continue;/*148*/}
        else
          {/*149*/Din_Go(387,2048);prob = (proCount * numAntiMessages[traitIndex]) / denom;/*150*/}/*146*/}/*142*/}

        Din_Go(389,2048);double n = proCount + antiCount;
        prob =  (0.225 + n * prob) / (.45 + n);
        double distance = std::abs(prob - 0.5);
        Din_Go(391,2048);if (distance >= .1)
        {
          Din_Go(390,2048);mozilla::DebugOnly<nsresult> rv = setAnalysis(token, traitIndex, distance, prob);
          NS_ASSERTION(NS_SUCCEEDED(rv), "Problem in setAnalysis");
        }
      }
    }

    Din_Go(448,2048);for (uint32_t traitIndex = 0; traitIndex < traitCount; traitIndex++)
    {
      Din_Go(394,2048);nsAutoTArray<TraitAnalysis, 1024> traitAnalyses;
      // copy valid tokens into an array to sort
      Din_Go(398,2048);for (uint32_t tokenIndex = 0; tokenIndex < tokenCount; tokenIndex++)
      {
        Din_Go(395,2048);uint32_t storeIndex = getAnalysisIndex(tokens[tokenIndex], traitIndex);
        Din_Go(397,2048);if (storeIndex)
        {
          Din_Go(396,2048);TraitAnalysis ta =
            {tokenIndex,
             mAnalysisStore[storeIndex].mDistance,
             mAnalysisStore[storeIndex].mProbability};
          traitAnalyses.AppendElement(ta);
        }
      }

      // sort the array by the distances
      Din_Go(399,2048);traitAnalyses.Sort(compareTraitAnalysis());
      uint32_t count = traitAnalyses.Length();
      uint32_t first, last = count;
      const uint32_t kMaxTokens = 150;
      first = ( count > kMaxTokens) ? count - kMaxTokens : 0;

      // Setup the arrays to save details if needed
      nsTArray<double> sArray;
      nsTArray<double> hArray;
      uint32_t usedTokenCount = ( count > kMaxTokens) ? kMaxTokens : count;
      Din_Go(401,2048);if (aDetailListener)
      {
        Din_Go(400,2048);sArray.SetCapacity(usedTokenCount);
        hArray.SetCapacity(usedTokenCount);
      }

      Din_Go(402,2048);double H = 1.0, S = 1.0;
      int32_t Hexp = 0, Sexp = 0;
      uint32_t goodclues=0;
      int e;

      // index from end to analyze most significant first
      Din_Go(412,2048);for (uint32_t ip1 = last; ip1 != first; --ip1)
      {
        Din_Go(403,2048);TraitAnalysis& ta = traitAnalyses[ip1 - 1];
        Din_Go(409,2048);if (ta.mDistance > 0.0)
        {
          Din_Go(404,2048);goodclues++;
          double value = ta.mProbability;
          S *= (1.0 - value);
          H *= value;
          Din_Go(406,2048);if ( S < 1e-200 )
          {
            Din_Go(405,2048);S = frexp(S, &e);
            Sexp += e;
          }
          Din_Go(408,2048);if ( H < 1e-200 )
          {
            Din_Go(407,2048);H = frexp(H, &e);
            Hexp += e;
          }
          MOZ_LOG(BayesianFilterLogModule, LogLevel::Warning,
                 ("token probability (%s) is %f",
                  tokens[ta.mTokenIndex].mWord, ta.mProbability));
        }
        Din_Go(411,2048);if (aDetailListener)
        {
          Din_Go(410,2048);sArray.AppendElement(log(S) + Sexp * M_LN2);
          hArray.AppendElement(log(H) + Hexp * M_LN2);
        }
      }

      Din_Go(413,2048);S = log(S) + Sexp * M_LN2;
      H = log(H) + Hexp * M_LN2;

      double prob;
      Din_Go(421,2048);if (goodclues > 0)
      {
          Din_Go(414,2048);int32_t chi_error;
          S = chi2P(-2.0 * S, 2.0 * goodclues, &chi_error);
          Din_Go(416,2048);if (!chi_error)
              {/*151*/Din_Go(415,2048);H = chi2P(-2.0 * H, 2.0 * goodclues, &chi_error);/*152*/}
          // if any error toss the entire calculation
          Din_Go(419,2048);if (!chi_error)
              {/*153*/Din_Go(417,2048);prob = (S-H +1.0) / 2.0;/*154*/}
          else
              {/*155*/Din_Go(418,2048);prob = 0.5;/*156*/}
      }
      else
          {/*157*/Din_Go(420,2048);prob = 0.5;/*158*/}

      Din_Go(434,2048);if (aDetailListener)
      {
        // Prepare output arrays
        Din_Go(422,2048);nsTArray<uint32_t> tokenPercents(usedTokenCount);
        nsTArray<uint32_t> runningPercents(usedTokenCount);
        nsTArray<char16_t*> tokenStrings(usedTokenCount);

        double clueCount = 1.0;
        Din_Go(431,2048);for (uint32_t tokenIndex = 0; tokenIndex < usedTokenCount; tokenIndex++)
        {
          Din_Go(423,2048);TraitAnalysis& ta = traitAnalyses[last - 1 - tokenIndex];
          int32_t chi_error;
          S = chi2P(-2.0 * sArray[tokenIndex], 2.0 * clueCount, &chi_error);
          Din_Go(425,2048);if (!chi_error)
            {/*159*/Din_Go(424,2048);H = chi2P(-2.0 * hArray[tokenIndex], 2.0 * clueCount, &chi_error);/*160*/}
          Din_Go(426,2048);clueCount += 1.0;
          double runningProb;
          Din_Go(429,2048);if (!chi_error)
            {/*161*/Din_Go(427,2048);runningProb = (S - H + 1.0) / 2.0;/*162*/}
          else
            {/*163*/Din_Go(428,2048);runningProb = 0.5;/*164*/}
          Din_Go(430,2048);runningPercents.AppendElement(static_cast<uint32_t>(runningProb *
              100. + .5));
          tokenPercents.AppendElement(static_cast<uint32_t>(ta.mProbability *
              100. + .5));
          tokenStrings.AppendElement(ToNewUnicode(NS_ConvertUTF8toUTF16(
              tokens[ta.mTokenIndex].mWord)));
        }

        Din_Go(432,2048);aDetailListener->OnMessageTraitDetails(messageURI, aProTraits[traitIndex],
            usedTokenCount, (const char16_t**)tokenStrings.Elements(),
            tokenPercents.Elements(), runningPercents.Elements());
        Din_Go(433,2048);for (uint32_t tokenIndex = 0; tokenIndex < usedTokenCount; tokenIndex++)
          NS_Free(tokenStrings[tokenIndex]);
      }

      Din_Go(435,2048);uint32_t proPercent = static_cast<uint32_t>(prob*100. + .5);

      // directly classify junk to maintain backwards compatibility
      Din_Go(443,2048);if (aProTraits[traitIndex] == kJunkTrait)
      {
        Din_Go(436,2048);bool isJunk = (prob >= mJunkProbabilityThreshold);
        MOZ_LOG(BayesianFilterLogModule, LogLevel::Info,
               ("%s is junk probability = (%f)  HAM SCORE:%f SPAM SCORE:%f",
                messageURI, prob,H,S));

        // the algorithm in "A Plan For Spam" assumes that you have a large good
        // corpus and a large junk corpus.
        // that won't be the case with users who first use the junk mail trait
        // so, we do certain things to encourage them to train.
        //
        // if there are no good tokens, assume the message is junk
        // this will "encourage" the user to train
        // and if there are no bad tokens, assume the message is not junk
        // this will also "encourage" the user to train
        // see bug #194238

        Din_Go(440,2048);if (listener && !mCorpus.getMessageCount(kGoodTrait))
          {/*165*/Din_Go(437,2048);isJunk = true;/*166*/}
        else {/*167*/Din_Go(438,2048);if (listener && !mCorpus.getMessageCount(kJunkTrait))
          {/*169*/Din_Go(439,2048);isJunk = false;/*170*/}/*168*/}

        Din_Go(442,2048);if (listener)
          {/*171*/Din_Go(441,2048);listener->OnMessageClassified(messageURI, isJunk ?
            nsMsgJunkStatus(nsIJunkMailPlugin::JUNK) :
            nsMsgJunkStatus(nsIJunkMailPlugin::GOOD), proPercent);/*172*/}
      }

      Din_Go(445,2048);if (aTraitListener)
      {
        Din_Go(444,2048);traits.AppendElement(aProTraits[traitIndex]);
        percents.AppendElement(proPercent);
      }

      // free aliases arrays returned from XPCOM
      Din_Go(446,2048);if (proAliasesLengths[traitIndex])
        NS_Free(proAliasArrays[traitIndex]);
      Din_Go(447,2048);if (antiAliasesLengths[traitIndex])
        NS_Free(antiAliasArrays[traitIndex]);
    }

    Din_Go(450,2048);if (aTraitListener)
      {/*173*/Din_Go(449,2048);aTraitListener->OnMessageTraitsClassified(messageURI,
          traits.Length(), traits.Elements(), percents.Elements());/*174*/}

    Din_Go(451,2048);delete[] tokens;
    // reuse mAnalysisStore without clearing memory
    mNextAnalysisIndex = 1;
    // but shrink it back to the default size
    Din_Go(453,2048);if (mAnalysisStore.Length() > kAnalysisStoreCapacity)
      {/*175*/Din_Go(452,2048);mAnalysisStore.RemoveElementsAt(kAnalysisStoreCapacity,
          mAnalysisStore.Length() - kAnalysisStoreCapacity);/*176*/}
    Din_Go(454,2048);mAnalysisStore.Compact();
Din_Go(455,2048);}

void nsBayesianFilter::classifyMessage(
  Tokenizer& tokens,
  const char* messageURI,
  nsIJunkMailClassificationListener* aJunkListener)
{
  Din_Go(456,2048);nsAutoTArray<uint32_t, 1> proTraits;
  nsAutoTArray<uint32_t, 1> antiTraits;
  proTraits.AppendElement(kJunkTrait);
  antiTraits.AppendElement(kGoodTrait);
  classifyMessage(tokens, messageURI, proTraits, antiTraits,
    aJunkListener, nullptr, nullptr);
Din_Go(457,2048);}

NS_IMETHODIMP
nsBayesianFilter::Observe(nsISupports *aSubject, const char *aTopic,
                          const char16_t *someData)
{
  Din_Go(458,2048);if (!strcmp(aTopic, "profile-before-change"))
    {/*177*/Din_Go(459,2048);Shutdown();/*178*/}
  {__IAENUM__ nsresult  ReplaceReturn213 = NS_OK; Din_Go(460,2048); return ReplaceReturn213;}
}

/* void shutdown (); */
NS_IMETHODIMP nsBayesianFilter::Shutdown()
{
  Din_Go(461,2048);if (mTrainingDataDirty)
    {/*179*/Din_Go(462,2048);mCorpus.writeTrainingData(mMaximumTokenCount);/*180*/}
  Din_Go(463,2048);mTrainingDataDirty = false;

  {__IAENUM__ nsresult  ReplaceReturn212 = NS_OK; Din_Go(464,2048); return ReplaceReturn212;}
}

/* readonly attribute boolean shouldDownloadAllHeaders; */
NS_IMETHODIMP nsBayesianFilter::GetShouldDownloadAllHeaders(bool *aShouldDownloadAllHeaders)
{
    // bayesian filters work on the whole msg body currently.
    Din_Go(465,2048);*aShouldDownloadAllHeaders = false;
    {__IAENUM__ nsresult  ReplaceReturn211 = NS_OK; Din_Go(466,2048); return ReplaceReturn211;}
}

/* void classifyMessage (in string aMsgURL, in nsIJunkMailClassificationListener aListener); */
NS_IMETHODIMP nsBayesianFilter::ClassifyMessage(const char *aMessageURL, nsIMsgWindow *aMsgWindow, nsIJunkMailClassificationListener *aListener)
{
    Din_Go(467,2048);MessageClassifier* analyzer = new MessageClassifier(this, aListener, aMsgWindow, 1, &aMessageURL);
    NS_ENSURE_TRUE(analyzer, NS_ERROR_OUT_OF_MEMORY);
    TokenStreamListener *tokenListener = new TokenStreamListener(analyzer);
    NS_ENSURE_TRUE(tokenListener, NS_ERROR_OUT_OF_MEMORY);
    analyzer->setTokenListener(tokenListener);
    {__IAENUM__ nsresult  ReplaceReturn210 = tokenizeMessage(aMessageURL, aMsgWindow, analyzer); Din_Go(468,2048); return ReplaceReturn210;}
}

/* void classifyMessages (in unsigned long aCount, [array, size_is (aCount)] in string aMsgURLs, in nsIJunkMailClassificationListener aListener); */
NS_IMETHODIMP nsBayesianFilter::ClassifyMessages(uint32_t aCount, const char **aMsgURLs, nsIMsgWindow *aMsgWindow, nsIJunkMailClassificationListener *aListener)
{
Din_Go(469,2048);    NS_ENSURE_ARG_POINTER(aMsgURLs);

    Din_Go(470,2048);TokenAnalyzer* analyzer = new MessageClassifier(this, aListener, aMsgWindow, aCount, aMsgURLs);
    NS_ENSURE_TRUE(analyzer, NS_ERROR_OUT_OF_MEMORY);
    TokenStreamListener *tokenListener = new TokenStreamListener(analyzer);
    NS_ENSURE_TRUE(tokenListener, NS_ERROR_OUT_OF_MEMORY);
    analyzer->setTokenListener(tokenListener);
    {__IAENUM__ nsresult  ReplaceReturn209 = tokenizeMessage(aMsgURLs[0], aMsgWindow, analyzer); Din_Go(471,2048); return ReplaceReturn209;}
}

nsresult nsBayesianFilter::setAnalysis(Token& token, uint32_t aTraitIndex,
  double aDistance, double aProbability)
{
  Din_Go(472,2048);uint32_t nextLink = token.mAnalysisLink;
  uint32_t lastLink = 0;
  uint32_t linkCount = 0, maxLinks = 100;

  // try to find an existing element. Limit the search to maxLinks
  // as a precaution
  Din_Go(478,2048);for (linkCount = 0; nextLink && linkCount < maxLinks; linkCount++)
  {
    Din_Go(473,2048);AnalysisPerToken &rAnalysis = mAnalysisStore[nextLink];
    Din_Go(476,2048);if (rAnalysis.mTraitIndex == aTraitIndex)
    {
      Din_Go(474,2048);rAnalysis.mDistance = aDistance;
      rAnalysis.mProbability = aProbability;
      {__IAENUM__ nsresult  ReplaceReturn208 = NS_OK; Din_Go(475,2048); return ReplaceReturn208;}
    }
    Din_Go(477,2048);lastLink = nextLink;
    nextLink = rAnalysis.mNextLink;
  }
  Din_Go(480,2048);if (linkCount >= maxLinks)
    {/*181*/{__IAENUM__ nsresult  ReplaceReturn207 = NS_ERROR_FAILURE; Din_Go(479,2048); return ReplaceReturn207;}/*182*/}

  // trait does not exist, so add it

  Din_Go(481,2048);AnalysisPerToken analysis(aTraitIndex, aDistance, aProbability);
  Din_Go(486,2048);if (mAnalysisStore.Length() == mNextAnalysisIndex)
    {/*183*/Din_Go(482,2048);mAnalysisStore.InsertElementAt(mNextAnalysisIndex, analysis);/*184*/}
  else {/*185*/Din_Go(483,2048);if (mAnalysisStore.Length() > mNextAnalysisIndex)
    {/*187*/Din_Go(484,2048);mAnalysisStore.ReplaceElementsAt(mNextAnalysisIndex, 1, analysis);/*188*/}
  else // we can only insert at the end of the array
    {/*189*/{__IAENUM__ nsresult  ReplaceReturn206 = NS_ERROR_FAILURE; Din_Go(485,2048); return ReplaceReturn206;}/*190*/}/*186*/}

  Din_Go(489,2048);if (lastLink)
    // the token had at least one link, so update the last link to point to
    // the new item
    {/*191*/Din_Go(487,2048);mAnalysisStore[lastLink].mNextLink = mNextAnalysisIndex;/*192*/}
  else
    // need to update the token's first link
    {/*193*/Din_Go(488,2048);token.mAnalysisLink = mNextAnalysisIndex;/*194*/}
  Din_Go(490,2048);mNextAnalysisIndex++;
  {__IAENUM__ nsresult  ReplaceReturn205 = NS_OK; Din_Go(491,2048); return ReplaceReturn205;}
}

uint32_t nsBayesianFilter::getAnalysisIndex(Token& token, uint32_t aTraitIndex)
{
  Din_Go(492,2048);uint32_t nextLink;
  uint32_t linkCount = 0, maxLinks = 100;
  Din_Go(497,2048);for (nextLink = token.mAnalysisLink; nextLink && linkCount < maxLinks; linkCount++)
  {
    Din_Go(493,2048);AnalysisPerToken &rAnalysis = mAnalysisStore[nextLink];
    Din_Go(495,2048);if (rAnalysis.mTraitIndex == aTraitIndex)
      {/*195*/{uint32_t  ReplaceReturn204 = nextLink; Din_Go(494,2048); return ReplaceReturn204;}/*196*/}
    Din_Go(496,2048);nextLink = rAnalysis.mNextLink;
  }
  NS_ASSERTION(linkCount < maxLinks, "corrupt analysis store");

  // Trait not found, indicate by zero
  {uint32_t  ReplaceReturn203 = 0; Din_Go(498,2048); return ReplaceReturn203;}
}

NS_IMETHODIMP nsBayesianFilter::ClassifyTraitsInMessage(
  const char *aMsgURI,
  uint32_t aTraitCount,
  uint32_t *aProTraits,
  uint32_t *aAntiTraits,
  nsIMsgTraitClassificationListener *aTraitListener,
  nsIMsgWindow *aMsgWindow,
  nsIJunkMailClassificationListener *aJunkListener)
{
  {__IAENUM__ nsresult  ReplaceReturn202 = ClassifyTraitsInMessages(1, &aMsgURI, aTraitCount, aProTraits,
    aAntiTraits, aTraitListener, aMsgWindow, aJunkListener); Din_Go(499,2048); return ReplaceReturn202;}
}

NS_IMETHODIMP nsBayesianFilter::ClassifyTraitsInMessages(
  uint32_t aCount,
  const char **aMsgURIs,
  uint32_t aTraitCount,
  uint32_t *aProTraits,
  uint32_t *aAntiTraits,
  nsIMsgTraitClassificationListener *aTraitListener,
  nsIMsgWindow *aMsgWindow,
  nsIJunkMailClassificationListener *aJunkListener)
{
  Din_Go(500,2048);nsAutoTArray<uint32_t, kTraitAutoCapacity> proTraits;
  nsAutoTArray<uint32_t, kTraitAutoCapacity> antiTraits;
  Din_Go(502,2048);if (aTraitCount > kTraitAutoCapacity)
  {
    Din_Go(501,2048);proTraits.SetCapacity(aTraitCount);
    antiTraits.SetCapacity(aTraitCount);
  }
  Din_Go(503,2048);proTraits.AppendElements(aProTraits, aTraitCount);
  antiTraits.AppendElements(aAntiTraits, aTraitCount);

  MessageClassifier* analyzer = new MessageClassifier(this, aJunkListener,
    aTraitListener, nullptr, proTraits, antiTraits, aMsgWindow, aCount, aMsgURIs);
  NS_ENSURE_TRUE(analyzer, NS_ERROR_OUT_OF_MEMORY);

  TokenStreamListener *tokenListener = new TokenStreamListener(analyzer);
  NS_ENSURE_TRUE(tokenListener, NS_ERROR_OUT_OF_MEMORY);

  analyzer->setTokenListener(tokenListener);
  {__IAENUM__ nsresult  ReplaceReturn201 = tokenizeMessage(aMsgURIs[0], aMsgWindow, analyzer); Din_Go(504,2048); return ReplaceReturn201;}
}

class MessageObserver : public TokenAnalyzer {
public:
  MessageObserver(nsBayesianFilter* filter,
                  nsTArray<uint32_t>& aOldClassifications,
                  nsTArray<uint32_t>& aNewClassifications,
                  nsIJunkMailClassificationListener* aJunkListener,
                  nsIMsgTraitClassificationListener* aTraitListener)
      :   mFilter(filter), mJunkMailPlugin(filter), mJunkListener(aJunkListener),
          mTraitListener(aTraitListener),
          mOldClassifications(aOldClassifications),
          mNewClassifications(aNewClassifications)
  {
  }

  virtual void analyzeTokens(Tokenizer& tokenizer)
  {
    mFilter->observeMessage(tokenizer, mTokenSource.get(), mOldClassifications,
                            mNewClassifications, mJunkListener, mTraitListener);
    // release reference to listener, which will allow us to go away as well.
    mTokenListener = nullptr;
  }

private:
  nsBayesianFilter* mFilter;
  nsCOMPtr<nsIJunkMailPlugin> mJunkMailPlugin;
  nsCOMPtr<nsIJunkMailClassificationListener> mJunkListener;
  nsCOMPtr<nsIMsgTraitClassificationListener> mTraitListener;
  nsTArray<uint32_t> mOldClassifications;
  nsTArray<uint32_t> mNewClassifications;
};

NS_IMETHODIMP nsBayesianFilter::SetMsgTraitClassification(
    const char *aMsgURI,
    uint32_t aOldCount,
    uint32_t *aOldTraits,
    uint32_t aNewCount,
    uint32_t *aNewTraits,
    nsIMsgTraitClassificationListener *aTraitListener,
    nsIMsgWindow *aMsgWindow,
    nsIJunkMailClassificationListener *aJunkListener)
{
  Din_Go(505,2048);nsAutoTArray<uint32_t, kTraitAutoCapacity> oldTraits;
  nsAutoTArray<uint32_t, kTraitAutoCapacity> newTraits;
  Din_Go(507,2048);if (aOldCount > kTraitAutoCapacity)
    {/*197*/Din_Go(506,2048);oldTraits.SetCapacity(aOldCount);/*198*/}
  Din_Go(509,2048);if (aNewCount > kTraitAutoCapacity)
    {/*199*/Din_Go(508,2048);newTraits.SetCapacity(aNewCount);/*200*/}
  Din_Go(510,2048);oldTraits.AppendElements(aOldTraits, aOldCount);
  newTraits.AppendElements(aNewTraits, aNewCount);

  MessageObserver* analyzer = new MessageObserver(this, oldTraits,
    newTraits, aJunkListener, aTraitListener);
  NS_ENSURE_TRUE(analyzer, NS_ERROR_OUT_OF_MEMORY);

  TokenStreamListener *tokenListener = new TokenStreamListener(analyzer);
  NS_ENSURE_TRUE(tokenListener, NS_ERROR_OUT_OF_MEMORY);

  analyzer->setTokenListener(tokenListener);
  {__IAENUM__ nsresult  ReplaceReturn200 = tokenizeMessage(aMsgURI, aMsgWindow, analyzer); Din_Go(511,2048); return ReplaceReturn200;}
}

// set new message classifications for a message
void nsBayesianFilter::observeMessage(
    Tokenizer& tokenizer,
    const char* messageURL,
    nsTArray<uint32_t>& oldClassifications,
    nsTArray<uint32_t>& newClassifications,
    nsIJunkMailClassificationListener* aJunkListener,
    nsIMsgTraitClassificationListener* aTraitListener)
{

    Din_Go(512,2048);bool trainingDataWasDirty = mTrainingDataDirty;

    // Uhoh...if the user is re-training then the message may already be classified and we are classifying it again with the same classification.
    // the old code would have removed the tokens for this message then added them back. But this really hurts the message occurrence
    // count for tokens if you just removed training.dat and are re-training. See Bug #237095 for more details.
    // What can we do here? Well we can skip the token removal step if the classifications are the same and assume the user is
    // just re-training. But this then allows users to re-classify the same message on the same training set over and over again
    // leading to data skew. But that's all I can think to do right now to address this.....
    uint32_t oldLength = oldClassifications.Length();
    Din_Go(519,2048);for (uint32_t index = 0; index < oldLength; index++)
    {
      Din_Go(513,2048);uint32_t trait = oldClassifications.ElementAt(index);
      // skip removing if trait is also in the new set
      Din_Go(515,2048);if (newClassifications.Contains(trait))
        {/*201*/Din_Go(514,2048);continue;/*202*/}
      // remove the tokens from the token set it is currently in
      Din_Go(516,2048);uint32_t messageCount;
      messageCount = mCorpus.getMessageCount(trait);
      Din_Go(518,2048);if (messageCount > 0)
      {
        Din_Go(517,2048);mCorpus.setMessageCount(trait, messageCount - 1);
        mCorpus.forgetTokens(tokenizer, trait, 1);
        mTrainingDataDirty = true;
      }
    }

    Din_Go(520,2048);nsMsgJunkStatus newClassification = nsIJunkMailPlugin::UNCLASSIFIED;
    uint32_t junkPercent = 0; // 0 here is no possibility of meeting the classification
    uint32_t newLength = newClassifications.Length();
    Din_Go(527,2048);for (uint32_t index = 0; index < newLength; index++)
    {
      Din_Go(521,2048);uint32_t trait = newClassifications.ElementAt(index);
      mCorpus.setMessageCount(trait, mCorpus.getMessageCount(trait) + 1);
      mCorpus.rememberTokens(tokenizer, trait, 1);
      mTrainingDataDirty = true;

      Din_Go(526,2048);if (aJunkListener)
      {
        Din_Go(522,2048);if (trait == kJunkTrait)
        {
          Din_Go(523,2048);junkPercent = nsIJunkMailPlugin::IS_SPAM_SCORE;
          newClassification = nsIJunkMailPlugin::JUNK;
        }
        else {/*203*/Din_Go(524,2048);if (trait == kGoodTrait)
        {
          Din_Go(525,2048);junkPercent = nsIJunkMailPlugin::IS_HAM_SCORE;
          newClassification = nsIJunkMailPlugin::GOOD;
        ;/*204*/}}
      }
    }

    Din_Go(529,2048);if (aJunkListener)
      {/*205*/Din_Go(528,2048);aJunkListener->OnMessageClassified(messageURL, newClassification, junkPercent);/*206*/}

    Din_Go(537,2048);if (aTraitListener)
    {
      // construct the outgoing listener arrays
      Din_Go(530,2048);nsAutoTArray<uint32_t, kTraitAutoCapacity> traits;
      nsAutoTArray<uint32_t, kTraitAutoCapacity> percents;
      uint32_t newLength = newClassifications.Length();
      Din_Go(532,2048);if (newLength > kTraitAutoCapacity)
      {
        Din_Go(531,2048);traits.SetCapacity(newLength);
        percents.SetCapacity(newLength);
      }
      Din_Go(533,2048);traits.AppendElements(newClassifications);
      Din_Go(535,2048);for (uint32_t index = 0; index < newLength; index++)
        {/*207*/Din_Go(534,2048);percents.AppendElement(100);/*208*/} // This is 100 percent, or certainty
      Din_Go(536,2048);aTraitListener->OnMessageTraitsClassified(messageURL,
          traits.Length(), traits.Elements(), percents.Elements());
    }

    Din_Go(539,2048);if (mTrainingDataDirty && !trainingDataWasDirty && ( mTimer != nullptr ))
    {
        // if training data became dirty just now, schedule flush
        // mMinFlushInterval msec from now
        MOZ_LOG(
            BayesianFilterLogModule, LogLevel::Debug,
            ("starting training data flush timer %i msec", mMinFlushInterval));
        Din_Go(538,2048);mTimer->InitWithFuncCallback(nsBayesianFilter::TimerCallback, this, mMinFlushInterval, nsITimer::TYPE_ONE_SHOT);
    }
Din_Go(540,2048);}

NS_IMETHODIMP nsBayesianFilter::GetUserHasClassified(bool *aResult)
{
  Din_Go(541,2048);*aResult = (  (mCorpus.getMessageCount(kGoodTrait) +
                 mCorpus.getMessageCount(kJunkTrait)) &&
                 mCorpus.countTokens());
  {__IAENUM__ nsresult  ReplaceReturn199 = NS_OK; Din_Go(542,2048); return ReplaceReturn199;}
}

// Set message classification (only allows junk and good)
NS_IMETHODIMP nsBayesianFilter::SetMessageClassification(
    const char *aMsgURL,
    nsMsgJunkStatus aOldClassification,
    nsMsgJunkStatus aNewClassification,
    nsIMsgWindow *aMsgWindow,
    nsIJunkMailClassificationListener *aListener)
{
  Din_Go(543,2048);nsAutoTArray<uint32_t, 1> oldClassifications;
  nsAutoTArray<uint32_t, 1> newClassifications;

  // convert between classifications and trait
  Din_Go(547,2048);if (aOldClassification == nsIJunkMailPlugin::JUNK)
    {/*209*/Din_Go(544,2048);oldClassifications.AppendElement(kJunkTrait);/*210*/}
  else {/*211*/Din_Go(545,2048);if (aOldClassification == nsIJunkMailPlugin::GOOD)
    {/*213*/Din_Go(546,2048);oldClassifications.AppendElement(kGoodTrait);/*214*/}/*212*/}
  Din_Go(551,2048);if (aNewClassification == nsIJunkMailPlugin::JUNK)
    {/*215*/Din_Go(548,2048);newClassifications.AppendElement(kJunkTrait);/*216*/}
  else {/*217*/Din_Go(549,2048);if (aNewClassification == nsIJunkMailPlugin::GOOD)
    {/*219*/Din_Go(550,2048);newClassifications.AppendElement(kGoodTrait);/*220*/}/*218*/}

  Din_Go(552,2048);MessageObserver* analyzer = new MessageObserver(this, oldClassifications,
    newClassifications, aListener, nullptr);
  NS_ENSURE_TRUE(analyzer, NS_ERROR_OUT_OF_MEMORY);

  TokenStreamListener *tokenListener = new TokenStreamListener(analyzer);
  NS_ENSURE_TRUE(tokenListener, NS_ERROR_OUT_OF_MEMORY);

  analyzer->setTokenListener(tokenListener);
  {__IAENUM__ nsresult  ReplaceReturn198 = tokenizeMessage(aMsgURL, aMsgWindow, analyzer); Din_Go(553,2048); return ReplaceReturn198;}
}

NS_IMETHODIMP nsBayesianFilter::ResetTrainingData()
{
  {__IAENUM__ nsresult  ReplaceReturn197 = mCorpus.resetTrainingData(); Din_Go(554,2048); return ReplaceReturn197;}
}

NS_IMETHODIMP nsBayesianFilter::DetailMessage(const char *aMsgURI,
    uint32_t aProTrait, uint32_t aAntiTrait,
    nsIMsgTraitDetailListener *aDetailListener, nsIMsgWindow *aMsgWindow)
{
  Din_Go(555,2048);nsAutoTArray<uint32_t, 1> proTraits;
  nsAutoTArray<uint32_t, 1> antiTraits;
  proTraits.AppendElement(aProTrait);
  antiTraits.AppendElement(aAntiTrait);

  MessageClassifier* analyzer = new MessageClassifier(this, nullptr,
    nullptr, aDetailListener, proTraits, antiTraits, aMsgWindow, 1, &aMsgURI);
  NS_ENSURE_TRUE(analyzer, NS_ERROR_OUT_OF_MEMORY);

  TokenStreamListener *tokenListener = new TokenStreamListener(analyzer);
  NS_ENSURE_TRUE(tokenListener, NS_ERROR_OUT_OF_MEMORY);

  analyzer->setTokenListener(tokenListener);
  {__IAENUM__ nsresult  ReplaceReturn196 = tokenizeMessage(aMsgURI, aMsgWindow, analyzer); Din_Go(556,2048); return ReplaceReturn196;}
}

// nsIMsgCorpus implementation

NS_IMETHODIMP nsBayesianFilter::CorpusCounts(uint32_t aTrait,
                                             uint32_t *aMessageCount,
                                             uint32_t *aTokenCount)
{
Din_Go(557,2048);  NS_ENSURE_ARG_POINTER(aTokenCount);
  Din_Go(558,2048);*aTokenCount = mCorpus.countTokens();
  Din_Go(560,2048);if (aTrait && aMessageCount)
    {/*221*/Din_Go(559,2048);*aMessageCount = mCorpus.getMessageCount(aTrait);/*222*/}
  {__IAENUM__ nsresult  ReplaceReturn195 = NS_OK; Din_Go(561,2048); return ReplaceReturn195;}
}

NS_IMETHODIMP nsBayesianFilter::ClearTrait(uint32_t aTrait)
{
    {__IAENUM__ nsresult  ReplaceReturn194 = mCorpus.ClearTrait(aTrait); Din_Go(562,2048); return ReplaceReturn194;}
}

NS_IMETHODIMP
nsBayesianFilter::UpdateData(nsIFile *aFile,
                             bool aIsAdd,
                             uint32_t aRemapCount,
                             uint32_t *aFromTraits,
                             uint32_t *aToTraits)
{
  {__IAENUM__ nsresult  ReplaceReturn193 = mCorpus.UpdateData(aFile, aIsAdd, aRemapCount, aFromTraits, aToTraits); Din_Go(563,2048); return ReplaceReturn193;}
}

NS_IMETHODIMP
nsBayesianFilter::GetTokenCount(const nsACString &aWord,
                                uint32_t aTrait,
                                uint32_t *aCount)
{
Din_Go(564,2048);  NS_ENSURE_ARG_POINTER(aCount);
  Din_Go(565,2048);CorpusToken* t = mCorpus.get(PromiseFlatCString(aWord).get());
  uint32_t count = mCorpus.getTraitCount(t, aTrait);
  *aCount = count;
  {__IAENUM__ nsresult  ReplaceReturn192 = NS_OK; Din_Go(566,2048); return ReplaceReturn192;}
}

/* Corpus Store */

/*
    Format of the training file for version 1:
    [0xFEEDFACE]
    [number good messages][number bad messages]
    [number good tokens]
    [count][length of word]word
    ...
    [number bad tokens]
    [count][length of word]word
    ...

     Format of the trait file for version 1:
    [0xFCA93601]  (the 01 is the version)
    for each trait to write
      [id of trait to write] (0 means end of list)
      [number of messages per trait]
      for each token with non-zero count
        [count]
        [length of word]word
*/

CorpusStore::CorpusStore() :
  TokenHash(sizeof(CorpusToken)),
  mNextTraitIndex(1) // skip 0 since index=0 will mean end of linked list
{
  Din_Go(567,2048);getTrainingFile(getter_AddRefs(mTrainingFile));
  mTraitStore.SetCapacity(kTraitStoreCapacity);
  TraitPerToken traitPT(0, 0);
  mTraitStore.AppendElement(traitPT); // dummy 0th element
Din_Go(568,2048);}

CorpusStore::~CorpusStore()
{
Din_Go(569,2048);Din_Go(570,2048);}

inline int writeUInt32(FILE* stream, uint32_t value)
{
    Din_Go(571,2048);value = PR_htonl(value);
    {int  ReplaceReturn191 = fwrite(&value, sizeof(uint32_t), 1, stream); Din_Go(572,2048); return ReplaceReturn191;}
}

inline int readUInt32(FILE* stream, uint32_t* value)
{
    Din_Go(573,2048);int n = fread(value, sizeof(uint32_t), 1, stream);
    Din_Go(575,2048);if (n == 1) {
        Din_Go(574,2048);*value = PR_ntohl(*value);
    }
    {int  ReplaceReturn190 = n; Din_Go(576,2048); return ReplaceReturn190;}
}

void CorpusStore::forgetTokens(Tokenizer& aTokenizer,
                    uint32_t aTraitId, uint32_t aCount)
{
  // if we are forgetting the tokens for a message, should only
  // subtract 1 from the occurrence count for that token in the training set
  // because we assume we only bumped the training set count once per messages
  // containing the token.
  Din_Go(577,2048);TokenEnumeration tokens = aTokenizer.getTokens();
  Din_Go(579,2048);while (tokens.hasMoreTokens())
  {
    Din_Go(578,2048);CorpusToken* token = static_cast<CorpusToken*>(tokens.nextToken());
    remove(token->mWord, aTraitId, aCount);
  }
Din_Go(580,2048);}

void CorpusStore::rememberTokens(Tokenizer& aTokenizer,
                    uint32_t aTraitId, uint32_t aCount)
{
  Din_Go(581,2048);TokenEnumeration tokens = aTokenizer.getTokens();
  Din_Go(586,2048);while (tokens.hasMoreTokens())
  {
    Din_Go(582,2048);CorpusToken* token = static_cast<CorpusToken*>(tokens.nextToken());
    Din_Go(584,2048);if (!token)
    {
      NS_ERROR("null token");
      Din_Go(583,2048);continue;
    }
    Din_Go(585,2048);add(token->mWord, aTraitId, aCount);
  }
Din_Go(587,2048);}

bool CorpusStore::writeTokens(FILE* stream, bool shrink, uint32_t aTraitId)
{
  Din_Go(588,2048);uint32_t tokenCount = countTokens();
  uint32_t newTokenCount = 0;

  // calculate the tokens for this trait to write

  TokenEnumeration tokens = getTokens();
  Din_Go(592,2048);for (uint32_t i = 0; i < tokenCount; ++i)
  {
    Din_Go(589,2048);CorpusToken* token = static_cast<CorpusToken*>(tokens.nextToken());
    uint32_t count = getTraitCount(token, aTraitId);
    // Shrinking the token database is accomplished by dividing all token counts by 2.
    // If shrinking, we'll ignore counts < 2, otherwise only ignore counts of < 1
    Din_Go(591,2048);if ((shrink && count > 1) || (!shrink && count))
      {/*223*/Din_Go(590,2048);newTokenCount++;/*224*/}
  }

  Din_Go(594,2048);if (writeUInt32(stream, newTokenCount) != 1)
    {/*225*/{_Bool  ReplaceReturn189 = false; Din_Go(593,2048); return ReplaceReturn189;}/*226*/}

  Din_Go(609,2048);if (newTokenCount > 0)
  {
    Din_Go(595,2048);TokenEnumeration tokens = getTokens();
    Din_Go(608,2048);for (uint32_t i = 0; i < tokenCount; ++i)
    {
      Din_Go(596,2048);CorpusToken* token = static_cast<CorpusToken*>(tokens.nextToken());
      uint32_t wordCount = getTraitCount(token, aTraitId);
      Din_Go(598,2048);if (shrink)
        {/*227*/Din_Go(597,2048);wordCount /= 2;/*228*/}
      Din_Go(600,2048);if (!wordCount)
        {/*229*/Din_Go(599,2048);continue;/*230*/} // Don't output zero count words
      Din_Go(602,2048);if (writeUInt32(stream, wordCount) != 1)
        {/*231*/{_Bool  ReplaceReturn188 = false; Din_Go(601,2048); return ReplaceReturn188;}/*232*/}
      Din_Go(603,2048);uint32_t tokenLength = strlen(token->mWord);
      Din_Go(605,2048);if (writeUInt32(stream, tokenLength) != 1)
        {/*233*/{_Bool  ReplaceReturn187 = false; Din_Go(604,2048); return ReplaceReturn187;}/*234*/}
      Din_Go(607,2048);if (fwrite(token->mWord, tokenLength, 1, stream) != 1)
        {/*235*/{_Bool  ReplaceReturn186 = false; Din_Go(606,2048); return ReplaceReturn186;}/*236*/}
    }
  }
  {_Bool  ReplaceReturn185 = true; Din_Go(610,2048); return ReplaceReturn185;}
}

bool CorpusStore::readTokens(FILE* stream, int64_t fileSize,
                               uint32_t aTraitId, bool aIsAdd)
{
    Din_Go(611,2048);uint32_t tokenCount;
    Din_Go(613,2048);if (readUInt32(stream, &tokenCount) != 1)
        {/*237*/{_Bool  ReplaceReturn184 = false; Din_Go(612,2048); return ReplaceReturn184;}/*238*/}

    Din_Go(614,2048);int64_t fpos = ftell(stream);
    Din_Go(616,2048);if (fpos < 0)
        {/*239*/{_Bool  ReplaceReturn183 = false; Din_Go(615,2048); return ReplaceReturn183;}/*240*/}

    Din_Go(617,2048);uint32_t bufferSize = 4096;
    char* buffer = new char[bufferSize];
    Din_Go(619,2048);if (!buffer) {/*241*/{_Bool  ReplaceReturn182 = false; Din_Go(618,2048); return ReplaceReturn182;}/*242*/}

    Din_Go(645,2048);for (uint32_t i = 0; i < tokenCount; ++i) {
        Din_Go(620,2048);uint32_t count;
        Din_Go(622,2048);if (readUInt32(stream, &count) != 1)
            {/*243*/Din_Go(621,2048);break;/*244*/}
        Din_Go(623,2048);uint32_t size;
        Din_Go(625,2048);if (readUInt32(stream, &size) != 1)
            {/*245*/Din_Go(624,2048);break;/*246*/}
        Din_Go(626,2048);fpos += 8;
        Din_Go(629,2048);if (fpos + size > fileSize) {
            Din_Go(627,2048);delete[] buffer;
            {_Bool  ReplaceReturn181 = false; Din_Go(628,2048); return ReplaceReturn181;}
        }
        Din_Go(638,2048);if (size >= bufferSize) {
            Din_Go(630,2048);delete[] buffer;
            Din_Go(634,2048);while (size >= bufferSize) {
                Din_Go(631,2048);bufferSize *= 2;
                Din_Go(633,2048);if (bufferSize == 0)
                    {/*247*/{_Bool  ReplaceReturn180 = false; Din_Go(632,2048); return ReplaceReturn180;}/*248*/}
            }
            Din_Go(635,2048);buffer = new char[bufferSize];
            Din_Go(637,2048);if (!buffer) {/*249*/{_Bool  ReplaceReturn179 = false; Din_Go(636,2048); return ReplaceReturn179;}/*250*/}
        }
        Din_Go(640,2048);if (fread(buffer, size, 1, stream) != 1)
            {/*251*/Din_Go(639,2048);break;/*252*/}
        Din_Go(641,2048);fpos += size;
        buffer[size] = '\0';
        Din_Go(644,2048);if (aIsAdd)
          {/*253*/Din_Go(642,2048);add(buffer, aTraitId, count);/*254*/}
        else
          {/*255*/Din_Go(643,2048);remove(buffer, aTraitId, count);/*256*/}
    }

    Din_Go(646,2048);delete[] buffer;

    {_Bool  ReplaceReturn178 = true; Din_Go(647,2048); return ReplaceReturn178;}
}

nsresult CorpusStore::getTrainingFile(nsIFile ** aTrainingFile)
{
  // should we cache the profile manager's directory?
  Din_Go(648,2048);nsCOMPtr<nsIFile> profileDir;

  nsresult rv = NS_GetSpecialDirectory(NS_APP_USER_PROFILE_50_DIR, getter_AddRefs(profileDir));
  NS_ENSURE_SUCCESS(rv, rv);
  rv = profileDir->Append(NS_LITERAL_STRING("training.dat"));
  NS_ENSURE_SUCCESS(rv, rv);

  {__IAENUM__ nsresult  ReplaceReturn177 = profileDir->QueryInterface(NS_GET_IID(nsIFile), (void **) aTrainingFile); Din_Go(649,2048); return ReplaceReturn177;}
}

nsresult CorpusStore::getTraitFile(nsIFile ** aTraitFile)
{
  // should we cache the profile manager's directory?
  Din_Go(650,2048);nsCOMPtr<nsIFile> profileDir;

  nsresult rv = NS_GetSpecialDirectory(NS_APP_USER_PROFILE_50_DIR, getter_AddRefs(profileDir));
  NS_ENSURE_SUCCESS(rv, rv);

  rv = profileDir->Append(NS_LITERAL_STRING("traits.dat"));
  NS_ENSURE_SUCCESS(rv, rv);

  {__IAENUM__ nsresult  ReplaceReturn176 = profileDir->QueryInterface(NS_GET_IID(nsIFile), (void **) aTraitFile); Din_Go(651,2048); return ReplaceReturn176;}
}

static const char kMagicCookie[] = { '\xFE', '\xED', '\xFA', '\xCE' };

// random string used to identify trait file and version (last byte is version)
static const char kTraitCookie[] = { '\xFC', '\xA9', '\x36', '\x01' };

void CorpusStore::writeTrainingData(uint32_t aMaximumTokenCount)
{
Din_Go(652,2048);  MOZ_LOG(BayesianFilterLogModule, LogLevel::Debug, ("writeTrainingData() entered"));
  Din_Go(654,2048);if (!mTrainingFile)
    {/*257*/Din_Go(653,2048);return;/*258*/}

  /*
   * For backwards compatibility, write the good and junk tokens to
   * training.dat; additional traits are added to a different file
   */

  // open the file, and write out training data
  Din_Go(655,2048);FILE* stream;
  nsresult rv = mTrainingFile->OpenANSIFileDesc("wb", &stream);
  Din_Go(657,2048);if (NS_FAILED(rv))
    {/*259*/Din_Go(656,2048);return;/*260*/}

  // If the number of tokens exceeds our limit, set the shrink flag
  Din_Go(658,2048);bool shrink = false;
  Din_Go(660,2048);if ((aMaximumTokenCount > 0) && // if 0, do not limit tokens
      (countTokens() > aMaximumTokenCount))
  {
    Din_Go(659,2048);shrink = true;
    MOZ_LOG(BayesianFilterLogModule, LogLevel::Warning, ("shrinking token data file"));
  }

  // We implement shrink by dividing counts by two
  Din_Go(661,2048);uint32_t shrinkFactor = shrink ? 2 : 1;

  Din_Go(664,2048);if (!((fwrite(kMagicCookie, sizeof(kMagicCookie), 1, stream) == 1) &&
      (writeUInt32(stream, getMessageCount(kGoodTrait) / shrinkFactor)) &&
      (writeUInt32(stream, getMessageCount(kJunkTrait) / shrinkFactor)) &&
       writeTokens(stream, shrink, kGoodTrait) &&
       writeTokens(stream, shrink, kJunkTrait)))
  {
    NS_WARNING("failed to write training data.");
    Din_Go(662,2048);fclose(stream);
    // delete the training data file, since it is potentially corrupt.
    mTrainingFile->Remove(false);
  }
  else
  {
    Din_Go(663,2048);fclose(stream);
  }

  /*
   * Write the remaining data to a second file traits.dat
   */

  Din_Go(668,2048);if (!mTraitFile)
  {
    Din_Go(665,2048);getTraitFile(getter_AddRefs(mTraitFile));
    Din_Go(667,2048);if (!mTraitFile)
     {/*261*/Din_Go(666,2048);return;/*262*/}
  }

  // open the file, and write out training data
  Din_Go(669,2048);rv = mTraitFile->OpenANSIFileDesc("wb", &stream);
  Din_Go(671,2048);if (NS_FAILED(rv))
    {/*263*/Din_Go(670,2048);return;/*264*/}

  Din_Go(672,2048);uint32_t numberOfTraits = mMessageCounts.Length();
  bool error;
  Din_Go(686,2048);while (1) // break on error or done
  {
    Din_Go(673,2048);if ((error = (fwrite(kTraitCookie, sizeof(kTraitCookie), 1, stream) != 1)))
      {/*265*/Din_Go(674,2048);break;/*266*/}

    Din_Go(684,2048);for (uint32_t index = 0; index < numberOfTraits; index++)
    {
      Din_Go(675,2048);uint32_t trait = mMessageCountsId[index];
      Din_Go(677,2048);if (trait == 1 || trait == 2)
        {/*267*/Din_Go(676,2048);continue;/*268*/} // junk traits are stored in training.dat
      Din_Go(679,2048);if ((error = (writeUInt32(stream, trait) != 1)))
        {/*269*/Din_Go(678,2048);break;/*270*/}
      Din_Go(681,2048);if ((error = (writeUInt32(stream, mMessageCounts[index] / shrinkFactor) != 1)))
        {/*271*/Din_Go(680,2048);break;/*272*/}
      Din_Go(683,2048);if ((error = !writeTokens(stream, shrink, trait)))
        {/*273*/Din_Go(682,2048);break;/*274*/}
    }
    Din_Go(685,2048);break;
  }
  // we add a 0 at the end to represent end of trait list
  Din_Go(687,2048);error = writeUInt32(stream, 0) != 1;

  fclose(stream);
  Din_Go(689,2048);if (error)
  {
    NS_WARNING("failed to write trait data.");
    // delete the trait data file, since it is probably corrupt.
    Din_Go(688,2048);mTraitFile->Remove(false);
  }

  Din_Go(695,2048);if (shrink)
  {
    // We'll clear the tokens, and read them back in from the file.
    // Yes this is slower than in place, but this is a rare event.

    Din_Go(690,2048);if (countTokens())
    {
      Din_Go(691,2048);clearTokens();
      Din_Go(693,2048);for (uint32_t index = 0; index < numberOfTraits; index++)
        {/*275*/Din_Go(692,2048);mMessageCounts[index] = 0;/*276*/}
    }

  Din_Go(694,2048);readTrainingData();
  }
Din_Go(696,2048);}

void CorpusStore::readTrainingData()
{

  /*
   * To maintain backwards compatibility, good and junk traits
   * are stored in a file "training.dat"
   */
  Din_Go(697,2048);if (!mTrainingFile)
    {/*277*/Din_Go(698,2048);return;/*278*/}

  Din_Go(699,2048);bool exists;
  nsresult rv = mTrainingFile->Exists(&exists);
  Din_Go(701,2048);if (NS_FAILED(rv) || !exists)
    {/*279*/Din_Go(700,2048);return;/*280*/}

  Din_Go(702,2048);FILE* stream;
  rv = mTrainingFile->OpenANSIFileDesc("rb", &stream);
  Din_Go(704,2048);if (NS_FAILED(rv))
    {/*281*/Din_Go(703,2048);return;/*282*/}

  Din_Go(705,2048);int64_t fileSize;
  rv = mTrainingFile->GetFileSize(&fileSize);
  Din_Go(707,2048);if (NS_FAILED(rv))
    {/*283*/Din_Go(706,2048);return;/*284*/}

  // FIXME:  should make sure that the tokenizers are empty.
  Din_Go(708,2048);char cookie[4];
  uint32_t goodMessageCount, junkMessageCount;
  Din_Go(709,2048);if (!((fread(cookie, sizeof(cookie), 1, stream) == 1) &&
        (memcmp(cookie, kMagicCookie, sizeof(cookie)) == 0) &&
        (readUInt32(stream, &goodMessageCount) == 1) &&
        (readUInt32(stream, &junkMessageCount) == 1) &&
         readTokens(stream, fileSize, kGoodTrait, true) &&
         readTokens(stream, fileSize, kJunkTrait, true))) {
      NS_WARNING("failed to read training data.");
      MOZ_LOG(BayesianFilterLogModule, LogLevel::Error, ("failed to read training data."));
  }
  Din_Go(710,2048);setMessageCount(kGoodTrait, goodMessageCount);
  setMessageCount(kJunkTrait, junkMessageCount);

  fclose(stream);

  /*
   * Additional traits are stored in traits.dat
   */

  Din_Go(714,2048);if (!mTraitFile)
  {
    Din_Go(711,2048);getTraitFile(getter_AddRefs(mTraitFile));
    Din_Go(713,2048);if (!mTraitFile)
     {/*285*/Din_Go(712,2048);return;/*286*/}
  }

  Din_Go(715,2048);rv = mTraitFile->Exists(&exists);
  Din_Go(717,2048);if (NS_FAILED(rv) || !exists)
    {/*287*/Din_Go(716,2048);return;/*288*/}

  Din_Go(718,2048);rv = UpdateData(mTraitFile, true, 0, nullptr, nullptr);

  Din_Go(719,2048);if (NS_FAILED(rv))
  {
    NS_WARNING("failed to read training data.");
    MOZ_LOG(BayesianFilterLogModule, LogLevel::Error, ("failed to read training data."));
  }
  Din_Go(720,2048);return;
}

nsresult CorpusStore::resetTrainingData()
{
  // clear out our in memory training tokens...
  Din_Go(721,2048);if (countTokens())
    {/*289*/Din_Go(722,2048);clearTokens();/*290*/}

  Din_Go(723,2048);uint32_t length = mMessageCounts.Length();
  Din_Go(725,2048);for (uint32_t index = 0 ; index < length; index++)
    {/*291*/Din_Go(724,2048);mMessageCounts[index] = 0;/*292*/}

  Din_Go(727,2048);if (mTrainingFile)
    {/*293*/Din_Go(726,2048);mTrainingFile->Remove(false);/*294*/}
  Din_Go(729,2048);if (mTraitFile)
    {/*295*/Din_Go(728,2048);mTraitFile->Remove(false);/*296*/}
  {__IAENUM__ nsresult  ReplaceReturn175 = NS_OK; Din_Go(730,2048); return ReplaceReturn175;}
}

inline CorpusToken* CorpusStore::get(const char* word)
{
  return static_cast<CorpusToken*>(TokenHash::get(word));
}

nsresult CorpusStore::updateTrait(CorpusToken* token, uint32_t aTraitId,
                                  int32_t aCountChange)
{
Din_Go(731,2048);  NS_ENSURE_ARG_POINTER(token);
  Din_Go(732,2048);uint32_t nextLink = token->mTraitLink;
  uint32_t lastLink = 0;

  uint32_t linkCount, maxLinks = 100; //sanity check
  Din_Go(740,2048);for (linkCount = 0; nextLink && linkCount < maxLinks; linkCount++)
  {
    Din_Go(733,2048);TraitPerToken& traitPT = mTraitStore[nextLink];
    Din_Go(738,2048);if (traitPT.mId == aTraitId)
    {
      // be careful with signed versus unsigned issues here
      Din_Go(734,2048);if (static_cast<int32_t>(traitPT.mCount) + aCountChange > 0)
        {/*297*/Din_Go(735,2048);traitPT.mCount += aCountChange;/*298*/}
      else
        {/*299*/Din_Go(736,2048);traitPT.mCount = 0;/*300*/}
      // we could delete zero count traits here, but let's not. It's rare anyway.
      {__IAENUM__ nsresult  ReplaceReturn174 = NS_OK; Din_Go(737,2048); return ReplaceReturn174;}
    }
    Din_Go(739,2048);lastLink = nextLink;
    nextLink = traitPT.mNextLink;
  }
  Din_Go(742,2048);if (linkCount >= maxLinks)
    {/*301*/{__IAENUM__ nsresult  ReplaceReturn173 = NS_ERROR_FAILURE; Din_Go(741,2048); return ReplaceReturn173;}/*302*/}

  // trait does not exist, so add it

  Din_Go(753,2048);if (aCountChange > 0) // don't set a negative count
  {
    Din_Go(743,2048);TraitPerToken traitPT(aTraitId, aCountChange);
    Din_Go(748,2048);if (mTraitStore.Length() == mNextTraitIndex)
      {/*303*/Din_Go(744,2048);mTraitStore.InsertElementAt(mNextTraitIndex, traitPT);/*304*/}
    else {/*305*/Din_Go(745,2048);if (mTraitStore.Length() > mNextTraitIndex)
      {/*307*/Din_Go(746,2048);mTraitStore.ReplaceElementsAt(mNextTraitIndex, 1, traitPT);/*308*/}
    else
      {/*309*/{__IAENUM__ nsresult  ReplaceReturn172 = NS_ERROR_FAILURE; Din_Go(747,2048); return ReplaceReturn172;}/*310*/}/*306*/}
    Din_Go(751,2048);if (lastLink)
      // the token had a parent, so update it
      {/*311*/Din_Go(749,2048);mTraitStore[lastLink].mNextLink = mNextTraitIndex;/*312*/}
    else
      // need to update the token's root link
      {/*313*/Din_Go(750,2048);token->mTraitLink = mNextTraitIndex;/*314*/}
    Din_Go(752,2048);mNextTraitIndex++;
  }
  {__IAENUM__ nsresult  ReplaceReturn171 = NS_OK; Din_Go(754,2048); return ReplaceReturn171;}
}

uint32_t CorpusStore::getTraitCount(CorpusToken* token, uint32_t aTraitId)
{
  Din_Go(755,2048);uint32_t nextLink;
  Din_Go(757,2048);if (!token || !(nextLink = token->mTraitLink))
    {/*315*/{uint32_t  ReplaceReturn170 = 0; Din_Go(756,2048); return ReplaceReturn170;}/*316*/}

  Din_Go(758,2048);uint32_t linkCount, maxLinks = 100; //sanity check
  Din_Go(763,2048);for (linkCount = 0; nextLink && linkCount < maxLinks; linkCount++)
  {
    Din_Go(759,2048);TraitPerToken& traitPT = mTraitStore[nextLink];
    Din_Go(761,2048);if (traitPT.mId == aTraitId)
      {/*317*/{uint32_t  ReplaceReturn169 = traitPT.mCount; Din_Go(760,2048); return ReplaceReturn169;}/*318*/}
    Din_Go(762,2048);nextLink = traitPT.mNextLink;
  }
  NS_ASSERTION(linkCount < maxLinks, "Corrupt trait count store");

  // trait not found (or error), so count is zero
  {uint32_t  ReplaceReturn168 = 0; Din_Go(764,2048); return ReplaceReturn168;}
}

CorpusToken* CorpusStore::add(const char* word, uint32_t aTraitId, uint32_t aCount)
{
  Din_Go(765,2048);CorpusToken* token = static_cast<CorpusToken*>(TokenHash::add(word));
  Din_Go(767,2048);if (token) {
    MOZ_LOG(BayesianFilterLogModule, LogLevel::Debug,
           ("adding word to corpus store: %s (Trait=%d) (deltaCount=%d)",
            word, aTraitId, aCount));
    Din_Go(766,2048);updateTrait(token, aTraitId, aCount);
  }
  {__IASTRUCT__ CorpusToken * ReplaceReturn167 = token; Din_Go(768,2048); return ReplaceReturn167;}
 }

void CorpusStore::remove(const char* word, uint32_t aTraitId, uint32_t aCount)
{
Din_Go(769,2048);  MOZ_LOG(BayesianFilterLogModule, LogLevel::Debug,
         ("remove word: %s (TraitId=%d) (Count=%d)",
         word, aTraitId, aCount));
  Din_Go(770,2048);CorpusToken* token = get(word);
  Din_Go(772,2048);if (token)
    {/*319*/Din_Go(771,2048);updateTrait(token, aTraitId, -static_cast<int32_t>(aCount));/*320*/}
Din_Go(773,2048);}

uint32_t CorpusStore::getMessageCount(uint32_t aTraitId)
{
  Din_Go(774,2048);size_t index = mMessageCountsId.IndexOf(aTraitId);
  Din_Go(776,2048);if (index == mMessageCountsId.NoIndex)
    {/*321*/{uint32_t  ReplaceReturn166 = 0; Din_Go(775,2048); return ReplaceReturn166;}/*322*/}
  {uint32_t  ReplaceReturn165 = mMessageCounts.ElementAt(index); Din_Go(777,2048); return ReplaceReturn165;}
}

void CorpusStore::setMessageCount(uint32_t aTraitId, uint32_t aCount)
{
  Din_Go(778,2048);size_t index = mMessageCountsId.IndexOf(aTraitId);
  Din_Go(781,2048);if (index == mMessageCountsId.NoIndex)
  {
    Din_Go(779,2048);mMessageCounts.AppendElement(aCount);
    mMessageCountsId.AppendElement(aTraitId);
  }
  else
  {
    Din_Go(780,2048);mMessageCounts[index] = aCount;
  }
Din_Go(782,2048);}

nsresult
CorpusStore::UpdateData(nsIFile *aFile,
                        bool aIsAdd,
                        uint32_t aRemapCount,
                        uint32_t *aFromTraits,
                        uint32_t *aToTraits)
{
Din_Go(783,2048);  NS_ENSURE_ARG_POINTER(aFile);
  Din_Go(784,2048);if (aRemapCount)
  {
    NS_ENSURE_ARG_POINTER(aFromTraits);
    NS_ENSURE_ARG_POINTER(aToTraits);
  }

  Din_Go(785,2048);int64_t fileSize;
  nsresult rv = aFile->GetFileSize(&fileSize);
  NS_ENSURE_SUCCESS(rv, rv);

  FILE* stream;
  rv = aFile->OpenANSIFileDesc("rb", &stream);
  NS_ENSURE_SUCCESS(rv, rv);

  bool error;
  Din_Go(810,2048);do // break on error or done
  {
    Din_Go(786,2048);char cookie[4];
    Din_Go(788,2048);if ((error = (fread(cookie, sizeof(cookie), 1, stream) != 1)))
      {/*323*/Din_Go(787,2048);break;/*324*/}

    Din_Go(790,2048);if ((error = memcmp(cookie, kTraitCookie, sizeof(cookie))))
      {/*325*/Din_Go(789,2048);break;/*326*/}

    Din_Go(791,2048);uint32_t fileTrait;
    Din_Go(808,2048);while ( !(error = (readUInt32(stream, &fileTrait) != 1)) && fileTrait)
    {
      Din_Go(792,2048);uint32_t count;
      Din_Go(794,2048);if ((error = (readUInt32(stream, &count) != 1)))
        {/*327*/Din_Go(793,2048);break;/*328*/}

      Din_Go(795,2048);uint32_t localTrait = fileTrait;
      // remap the trait
      Din_Go(798,2048);for (uint32_t i = 0; i < aRemapCount; i++)
      {
        Din_Go(796,2048);if (aFromTraits[i] == fileTrait)
          {/*329*/Din_Go(797,2048);localTrait = aToTraits[i];/*330*/}
      }

      Din_Go(799,2048);uint32_t messageCount = getMessageCount(localTrait);
      Din_Go(804,2048);if (aIsAdd)
        {/*331*/Din_Go(800,2048);messageCount += count;/*332*/}
      else {/*333*/Din_Go(801,2048);if (count > messageCount)
        {/*335*/Din_Go(802,2048);messageCount = 0;/*336*/}
      else
        {/*337*/Din_Go(803,2048);messageCount -= count;/*338*/}/*334*/}
      Din_Go(805,2048);setMessageCount(localTrait, messageCount);

      Din_Go(807,2048);if ((error = !readTokens(stream, fileSize, localTrait, aIsAdd)))
        {/*339*/Din_Go(806,2048);break;/*340*/}
    }
    Din_Go(809,2048);break;
  } while (0);

  Din_Go(811,2048);fclose(stream);

  Din_Go(813,2048);if (error)
    {/*341*/{__IAENUM__ nsresult  ReplaceReturn164 = NS_ERROR_FAILURE; Din_Go(812,2048); return ReplaceReturn164;}/*342*/}
  {__IAENUM__ nsresult  ReplaceReturn163 = NS_OK; Din_Go(814,2048); return ReplaceReturn163;}
}

nsresult CorpusStore::ClearTrait(uint32_t aTrait)
{
  // clear message counts
  Din_Go(815,2048);setMessageCount(aTrait, 0);

  TokenEnumeration tokens = getTokens();
  Din_Go(817,2048);while (tokens.hasMoreTokens())
  {
    Din_Go(816,2048);CorpusToken* token = static_cast<CorpusToken*>(tokens.nextToken());
    int32_t wordCount = static_cast<int32_t>(getTraitCount(token, aTrait));
    updateTrait(token, aTrait, -wordCount);
  }
  {__IAENUM__ nsresult  ReplaceReturn162 = NS_OK; Din_Go(818,2048); return ReplaceReturn162;}
}
