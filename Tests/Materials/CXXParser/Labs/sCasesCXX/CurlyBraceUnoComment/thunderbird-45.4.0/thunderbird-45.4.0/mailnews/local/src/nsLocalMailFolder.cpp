#include "var/tmp/sensor.h"
/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "nsIPrefService.h"
#include "nsIPrefBranch.h"
#include "prlog.h"

#include "msgCore.h"    // precompiled header...
#include "nsArrayEnumerator.h"
#include "nsLocalMailFolder.h"
#include "nsMsgLocalFolderHdrs.h"
#include "nsMsgFolderFlags.h"
#include "nsMsgMessageFlags.h"
#include "prprf.h"
#include "prmem.h"
#include "nsIArray.h"
#include "nsIServiceManager.h"
#include "nsIMailboxService.h"
#include "nsParseMailbox.h"
#include "nsIMsgAccountManager.h"
#include "nsIMsgWindow.h"
#include "nsCOMPtr.h"
#include "nsIRDFService.h"
#include "nsMsgDBCID.h"
#include "nsMsgUtils.h"
#include "nsLocalUtils.h"
#include "nsIPop3IncomingServer.h"
#include "nsILocalMailIncomingServer.h"
#include "nsIMsgIncomingServer.h"
#include "nsMsgBaseCID.h"
#include "nsMsgLocalCID.h"
#include "nsStringGlue.h"
#include "nsIMsgFolderCacheElement.h"
#include "nsUnicharUtils.h"
#include "nsMsgUtils.h"
#include "nsICopyMsgStreamListener.h"
#include "nsIMsgCopyService.h"
#include "nsMsgTxn.h"
#include "nsIMessenger.h"
#include "nsMsgBaseCID.h"
#include "nsNativeCharsetUtils.h"
#include "nsIDocShell.h"
#include "nsIPrompt.h"
#include "nsIInterfaceRequestor.h"
#include "nsIInterfaceRequestorUtils.h"
#include "nsIPop3URL.h"
#include "nsIMsgMailSession.h"
#include "nsIMsgFolderCompactor.h"
#include "nsNetCID.h"
#include "nsIMsgMailNewsUrl.h"
#include "nsISpamSettings.h"
#include "nsINoIncomingServer.h"
#include "nsNativeCharsetUtils.h"
#include "nsMailHeaders.h"
#include "nsCOMArray.h"
#include "nsILineInputStream.h"
#include "nsIFileStreams.h"
#include "nsAutoPtr.h"
#include "nsIRssIncomingServer.h"
#include "nsNetUtil.h"
#include "nsIMsgFolderNotificationService.h"
#include "nsReadLine.h"
#include "nsArrayUtils.h"
#include "nsIMsgTraitService.h"
#include "nsIStringEnumerator.h"
#include "mozilla/Services.h"

//////////////////////////////////////////////////////////////////////////////
// nsLocal
/////////////////////////////////////////////////////////////////////////////

nsLocalMailCopyState::nsLocalMailCopyState() :
  m_flags(0),
  m_lastProgressTime(PR_IntervalToMilliseconds(PR_IntervalNow())),
  m_curDstKey(nsMsgKey_None),
  m_curCopyIndex(0),
  m_totalMsgCount(0),
  m_dataBufferSize(0),
  m_leftOver(0),
  m_isMove(false),
  m_dummyEnvelopeNeeded(false),
  m_fromLineSeen(false),
  m_writeFailed(false),
  m_notifyFolderLoaded(false)
{
Din_Go(819,2048);Din_Go(820,2048);}

nsLocalMailCopyState::~nsLocalMailCopyState()
{
  Din_Go(821,2048);PR_Free(m_dataBuffer);
  Din_Go(823,2048);if (m_fileStream)
    {/*1*/Din_Go(822,2048);m_fileStream->Close();/*2*/}
  Din_Go(827,2048);if (m_messageService)
  {
    Din_Go(824,2048);nsCOMPtr <nsIMsgFolder> srcFolder = do_QueryInterface(m_srcSupport);
    Din_Go(826,2048);if (srcFolder && m_message)
    {
      Din_Go(825,2048);nsCString uri;
      srcFolder->GetUriForMsg(m_message, uri);
    }
  }
Din_Go(828,2048);}

nsLocalFolderScanState::nsLocalFolderScanState() : m_uidl(nullptr)
{
Din_Go(829,2048);Din_Go(830,2048);}

nsLocalFolderScanState::~nsLocalFolderScanState()
{
Din_Go(831,2048);Din_Go(832,2048);}

///////////////////////////////////////////////////////////////////////////////
// nsMsgLocalMailFolder interface
///////////////////////////////////////////////////////////////////////////////

nsMsgLocalMailFolder::nsMsgLocalMailFolder(void)
  : mCopyState(nullptr), mHaveReadNameFromDB(false),
    mInitialized(false),
    mCheckForNewMessagesAfterParsing(false), m_parsingFolder(false),
    mDownloadState(DOWNLOAD_STATE_NONE)
{
Din_Go(833,2048);Din_Go(834,2048);}

nsMsgLocalMailFolder::~nsMsgLocalMailFolder(void)
{
Din_Go(835,2048);Din_Go(836,2048);}

NS_IMPL_ISUPPORTS_INHERITED(nsMsgLocalMailFolder,
                             nsMsgDBFolder,
                             nsICopyMessageListener,
                             nsIMsgLocalMailFolder)

////////////////////////////////////////////////////////////////////////////////

NS_IMETHODIMP
nsMsgLocalMailFolder::Init(const char* aURI)
{
  {__IAENUM__ nsresult  ReplaceReturn161 = nsMsgDBFolder::Init(aURI); Din_Go(837,2048); return ReplaceReturn161;}
}

nsresult nsMsgLocalMailFolder::CreateChildFromURI(const nsCString &uri, nsIMsgFolder **folder)
{
  Din_Go(838,2048);nsMsgLocalMailFolder *newFolder = new nsMsgLocalMailFolder;
  Din_Go(840,2048);if (!newFolder)
    {/*3*/{__IAENUM__ nsresult  ReplaceReturn160 = NS_ERROR_OUT_OF_MEMORY; Din_Go(839,2048); return ReplaceReturn160;}/*4*/}

  NS_ADDREF(*folder = newFolder);
  Din_Go(841,2048);newFolder->Init(uri.get());
  {__IAENUM__ nsresult  ReplaceReturn159 = NS_OK; Din_Go(842,2048); return ReplaceReturn159;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::CreateLocalSubfolder(const nsAString &aFolderName,
                                                         nsIMsgFolder **aChild)
{
Din_Go(843,2048);  NS_ENSURE_ARG_POINTER(aChild);
  Din_Go(844,2048);nsresult rv = CreateSubfolderInternal(aFolderName, nullptr, aChild);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIMsgFolderNotificationService> notifier(
    do_GetService(NS_MSGNOTIFICATIONSERVICE_CONTRACTID));
  Din_Go(846,2048);if (notifier)
    {/*5*/Din_Go(845,2048);notifier->NotifyFolderAdded(*aChild);/*6*/}

  {__IAENUM__ nsresult  ReplaceReturn158 = NS_OK; Din_Go(847,2048); return ReplaceReturn158;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::GetManyHeadersToDownload(bool *retval)
{
  Din_Go(848,2048);bool isLocked;
  // if the folder is locked, we're probably reparsing - let's build the
  // view when we've finished reparsing.
  GetLocked(&isLocked);
  Din_Go(851,2048);if (isLocked)
  {
    Din_Go(849,2048);*retval = true;
    {__IAENUM__ nsresult  ReplaceReturn157 = NS_OK; Din_Go(850,2048); return ReplaceReturn157;}
  }

  {__IAENUM__ nsresult  ReplaceReturn156 = nsMsgDBFolder::GetManyHeadersToDownload(retval); Din_Go(852,2048); return ReplaceReturn156;}
}

//run the url to parse the mailbox
NS_IMETHODIMP nsMsgLocalMailFolder::ParseFolder(nsIMsgWindow *aMsgWindow,
                                                nsIUrlListener *aListener)
{
  Din_Go(853,2048);nsCOMPtr<nsIMsgPluggableStore> msgStore;
  nsresult rv = GetMsgStore(getter_AddRefs(msgStore));
  NS_ENSURE_SUCCESS(rv, rv);
  Din_Go(855,2048);if (aListener != this)
    {/*7*/Din_Go(854,2048);mReparseListener = aListener;/*8*/}
  // if parsing is synchronous, we need to set m_parsingFolder to
  // true before starting. And we need to open the db before
  // setting m_parsingFolder to true.
//  OpenDatabase();
  Din_Go(856,2048);rv = msgStore->RebuildIndex(this, mDatabase, aMsgWindow, this);
  Din_Go(858,2048);if (NS_SUCCEEDED(rv))
    {/*9*/Din_Go(857,2048);m_parsingFolder = true;/*10*/}

  {__IAENUM__ nsresult  ReplaceReturn155 = rv; Din_Go(859,2048); return ReplaceReturn155;}
}

// this won't force a reparse of the folder if the db is invalid.
NS_IMETHODIMP
nsMsgLocalMailFolder::GetMsgDatabase(nsIMsgDatabase** aMsgDatabase)
{
  {__IAENUM__ nsresult  ReplaceReturn154 = GetDatabaseWOReparse(aMsgDatabase); Din_Go(860,2048); return ReplaceReturn154;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::GetSubFolders(nsISimpleEnumerator **aResult)
{
  Din_Go(861,2048);if (!mInitialized)
  {
    Din_Go(862,2048);nsCOMPtr<nsIMsgIncomingServer> server;
    nsresult rv = GetServer(getter_AddRefs(server));
    NS_ENSURE_SUCCESS(rv, NS_MSG_INVALID_OR_MISSING_SERVER);
    nsCOMPtr<nsIMsgPluggableStore> msgStore;
    // need to set this flag here to avoid infinite recursion
    mInitialized = true;
    rv = server->GetMsgStore(getter_AddRefs(msgStore));
    NS_ENSURE_SUCCESS(rv, rv);
    // This should add all existing folders as sub-folders of this folder.
    rv = msgStore->DiscoverSubFolders(this, true);

    nsCOMPtr<nsIFile> path;
    rv = GetFilePath(getter_AddRefs(path));
    Din_Go(864,2048);if (NS_FAILED(rv))
      {/*11*/{__IAENUM__ nsresult  ReplaceReturn153 = rv; Din_Go(863,2048); return ReplaceReturn153;}/*12*/}

    Din_Go(865,2048);bool directory;
    path->IsDirectory(&directory);
    Din_Go(874,2048);if (directory)
    {
      Din_Go(866,2048);SetFlag(nsMsgFolderFlags::Mail | nsMsgFolderFlags::Elided |
              nsMsgFolderFlags::Directory);

      bool isServer;
      GetIsServer(&isServer);
      Din_Go(873,2048);if (isServer)
      {
        Din_Go(867,2048);nsCOMPtr<nsIMsgIncomingServer> server;
        rv = GetServer(getter_AddRefs(server));
        NS_ENSURE_SUCCESS(rv, NS_MSG_INVALID_OR_MISSING_SERVER);

        nsCOMPtr<nsILocalMailIncomingServer> localMailServer;
        localMailServer = do_QueryInterface(server, &rv);
        NS_ENSURE_SUCCESS(rv, NS_MSG_INVALID_OR_MISSING_SERVER);

        // first create the folders on disk (as empty files)
        rv = localMailServer->CreateDefaultMailboxes();
        Din_Go(869,2048);if (NS_FAILED(rv) && rv != NS_MSG_FOLDER_EXISTS)
          {/*13*/{__IAENUM__ nsresult  ReplaceReturn152 = rv; Din_Go(868,2048); return ReplaceReturn152;}/*14*/}

        // must happen after CreateSubFolders, or the folders won't exist.
        Din_Go(870,2048);rv = localMailServer->SetFlagsOnDefaultMailboxes();
        Din_Go(872,2048);if (NS_FAILED(rv))
          {/*15*/{__IAENUM__ nsresult  ReplaceReturn151 = rv; Din_Go(871,2048); return ReplaceReturn151;}/*16*/}
      }
    }
    Din_Go(875,2048);UpdateSummaryTotals(false);
  }

  {__IAENUM__ nsresult  ReplaceReturn150 = aResult ? NS_NewArrayEnumerator(aResult, mSubFolders) : NS_ERROR_NULL_POINTER; Din_Go(876,2048); return ReplaceReturn150;}
}

nsresult nsMsgLocalMailFolder::GetDatabase()
{
  Din_Go(877,2048);nsCOMPtr <nsIMsgDatabase> msgDB;
  {__IAENUM__ nsresult  ReplaceReturn149 = GetDatabaseWOReparse(getter_AddRefs(msgDB)); Din_Go(878,2048); return ReplaceReturn149;}
}

//we treat failure as null db returned
NS_IMETHODIMP nsMsgLocalMailFolder::GetDatabaseWOReparse(nsIMsgDatabase **aDatabase)
{
Din_Go(879,2048);  NS_ENSURE_ARG(aDatabase);
  Din_Go(880,2048);if (m_parsingFolder)
    return NS_MSG_ERROR_FOLDER_SUMMARY_OUT_OF_DATE;

  Din_Go(881,2048);nsresult rv = NS_OK;
  Din_Go(885,2048);if (!mDatabase)
  {
    Din_Go(882,2048);rv = OpenDatabase();
    Din_Go(884,2048);if (mDatabase)
    {
      Din_Go(883,2048);mDatabase->AddListener(this);
      UpdateNewMessages();
    }
  }
  NS_IF_ADDREF(*aDatabase = mDatabase);
  Din_Go(887,2048);if (mDatabase)
    {/*17*/Din_Go(886,2048);mDatabase->SetLastUseTime(PR_Now());/*18*/}
  {__IAENUM__ nsresult  ReplaceReturn148 = rv; Din_Go(888,2048); return ReplaceReturn148;}
}


// Makes sure the database is open and exists.  If the database is out of date,
// then this call will run an async url to reparse the folder. The passed in
// url listener will get called when the url is done.
NS_IMETHODIMP nsMsgLocalMailFolder::GetDatabaseWithReparse(nsIUrlListener *aReparseUrlListener, nsIMsgWindow *aMsgWindow,
                                                           nsIMsgDatabase **aMsgDatabase)
{
  Din_Go(889,2048);nsresult rv = NS_OK;
  // if we're already reparsing, just remember the listener so we can notify it
  // when we've finished.
  Din_Go(892,2048);if (m_parsingFolder)
  {
    NS_ASSERTION(!mReparseListener, "can't have an existing listener");
    Din_Go(890,2048);mReparseListener = aReparseUrlListener;
    {__IAENUM__ nsresult  ReplaceReturn147 = NS_MSG_ERROR_FOLDER_SUMMARY_OUT_OF_DATE; Din_Go(891,2048); return ReplaceReturn147;}
  }

  Din_Go(928,2048);if (!mDatabase)
  {
    Din_Go(893,2048);nsCOMPtr <nsIFile> pathFile;
    rv = GetFilePath(getter_AddRefs(pathFile));
    Din_Go(895,2048);if (NS_FAILED(rv))
      {/*19*/{__IAENUM__ nsresult  ReplaceReturn146 = rv; Din_Go(894,2048); return ReplaceReturn146;}/*20*/}

    Din_Go(896,2048);bool exists;
    rv = pathFile->Exists(&exists);
    NS_ENSURE_SUCCESS(rv,rv);
    Din_Go(898,2048);if (!exists)
      {/*21*/{__IAENUM__ nsresult  ReplaceReturn145 = NS_ERROR_NULL_POINTER; Din_Go(897,2048); return ReplaceReturn145;}/*22*/}  //mDatabase will be null at this point.

    Din_Go(899,2048);nsCOMPtr<nsIMsgDBService> msgDBService = do_GetService(NS_MSGDB_SERVICE_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    nsresult folderOpen = msgDBService->OpenFolderDB(this, true,
                                                     getter_AddRefs(mDatabase));
    Din_Go(917,2048);if (folderOpen == NS_MSG_ERROR_FOLDER_SUMMARY_OUT_OF_DATE)
    {
      Din_Go(900,2048);nsCOMPtr <nsIDBFolderInfo> dbFolderInfo;
      nsCOMPtr <nsIDBFolderInfo> transferInfo;
      Din_Go(911,2048);if (mDatabase)
      {
        Din_Go(901,2048);mDatabase->GetDBFolderInfo(getter_AddRefs(dbFolderInfo));
        Din_Go(903,2048);if (dbFolderInfo)
        {
          Din_Go(902,2048);dbFolderInfo->SetNumMessages(0);
          dbFolderInfo->SetNumUnreadMessages(0);
          dbFolderInfo->GetTransferInfo(getter_AddRefs(transferInfo));
        }
        Din_Go(904,2048);dbFolderInfo = nullptr;

        // A backup message database might have been created earlier, for example
        // if the user requested a reindex. We'll use the earlier one if we can,
        // otherwise we'll try to backup at this point.
        Din_Go(909,2048);if (NS_FAILED(OpenBackupMsgDatabase()))
        {
          Din_Go(905,2048);CloseAndBackupFolderDB(EmptyCString());
          Din_Go(907,2048);if (NS_FAILED(OpenBackupMsgDatabase()) && mBackupDatabase)
          {
            Din_Go(906,2048);mBackupDatabase->RemoveListener(this);
            mBackupDatabase = nullptr;
          }
        }
        else
          {/*23*/Din_Go(908,2048);mDatabase->ForceClosed();/*24*/}

        Din_Go(910,2048);mDatabase = nullptr;
      }
      Din_Go(912,2048);nsCOMPtr <nsIFile> summaryFile;
      rv = GetSummaryFileLocation(pathFile, getter_AddRefs(summaryFile));
      NS_ENSURE_SUCCESS(rv, rv);
      // Remove summary file.
      summaryFile->Remove(false);

      // if it's out of date then reopen with upgrade.
      rv = msgDBService->CreateNewDB(this, getter_AddRefs(mDatabase));
      NS_ENSURE_SUCCESS(rv, rv);

      Din_Go(914,2048);if (transferInfo && mDatabase)
      {
        Din_Go(913,2048);SetDBTransferInfo(transferInfo);
        mDatabase->SetSummaryValid(false);
      }
    }
    else {/*25*/Din_Go(915,2048);if (folderOpen == NS_MSG_ERROR_FOLDER_SUMMARY_MISSING)
    {
      Din_Go(916,2048);rv = msgDBService->CreateNewDB(this, getter_AddRefs(mDatabase));
    ;/*26*/}}

    Din_Go(927,2048);if (mDatabase)
    {
      Din_Go(918,2048);if (mAddListener)
        {/*27*/Din_Go(919,2048);mDatabase->AddListener(this);/*28*/}

      // if we have to regenerate the folder, run the parser url.
      Din_Go(925,2048);if (folderOpen == NS_MSG_ERROR_FOLDER_SUMMARY_MISSING ||
          folderOpen == NS_MSG_ERROR_FOLDER_SUMMARY_OUT_OF_DATE)
      {
        Din_Go(920,2048);if (NS_FAILED(rv = ParseFolder(aMsgWindow, aReparseUrlListener)))
        {
          Din_Go(921,2048);if (rv == NS_MSG_FOLDER_BUSY)
          {
            Din_Go(922,2048);mDatabase->RemoveListener(this);  //we need to null out the db so that parsing gets kicked off again.
            mDatabase = nullptr;
            ThrowAlertMsg("parsingFolderFailed", aMsgWindow);
          }
          {__IAENUM__ nsresult  ReplaceReturn144 = rv; Din_Go(923,2048); return ReplaceReturn144;}
        }

        {__IAENUM__ nsresult  ReplaceReturn143 = NS_ERROR_NOT_INITIALIZED; Din_Go(924,2048); return ReplaceReturn143;}
      }

      // We have a valid database so lets extract necessary info.
      Din_Go(926,2048);UpdateSummaryTotals(true);
    }
  }
  NS_IF_ADDREF(*aMsgDatabase = mDatabase);
  {__IAENUM__ nsresult  ReplaceReturn142 = rv; Din_Go(929,2048); return ReplaceReturn142;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::UpdateFolder(nsIMsgWindow *aWindow)
{
  Din_Go(930,2048);(void) RefreshSizeOnDisk();
  nsresult rv;

  Din_Go(932,2048);if (!PromptForMasterPasswordIfNecessary())
    {/*29*/{__IAENUM__ nsresult  ReplaceReturn141 = NS_ERROR_FAILURE; Din_Go(931,2048); return ReplaceReturn141;}/*30*/}

  //If we don't currently have a database, get it.  Otherwise, the folder has been updated (presumably this
  //changes when we download headers when opening inbox).  If it's updated, send NotifyFolderLoaded.
  Din_Go(943,2048);if (!mDatabase)
  {
    // return of NS_ERROR_NOT_INITIALIZED means running parsing URL
    Din_Go(933,2048);rv = GetDatabaseWithReparse(this, aWindow, getter_AddRefs(mDatabase));
    Din_Go(935,2048);if (NS_SUCCEEDED(rv))
      {/*31*/Din_Go(934,2048);NotifyFolderEvent(mFolderLoadedAtom);/*32*/}
  }
  else
  {
    Din_Go(936,2048);bool valid;
    rv = mDatabase->GetSummaryValid(&valid);
    // don't notify folder loaded or try compaction if db isn't valid
    // (we're probably reparsing or copying msgs to it)
    Din_Go(942,2048);if (NS_SUCCEEDED(rv) && valid)
      {/*33*/Din_Go(937,2048);NotifyFolderEvent(mFolderLoadedAtom);/*34*/}
    else {/*35*/Din_Go(938,2048);if (mCopyState)
      {/*37*/Din_Go(939,2048);mCopyState->m_notifyFolderLoaded = true;/*38*/} //defer folder loaded notification
    else {/*39*/Din_Go(940,2048);if (!m_parsingFolder)// if the db was already open, it's probably OK to load it if not parsing
      {/*41*/Din_Go(941,2048);NotifyFolderEvent(mFolderLoadedAtom);/*42*/}/*40*/}/*36*/}
  }
  Din_Go(944,2048);bool filtersRun;
  bool hasNewMessages;
  GetHasNewMessages(&hasNewMessages);
  Din_Go(946,2048);if (mDatabase)
    {/*43*/Din_Go(945,2048);ApplyRetentionSettings();/*44*/}
  // if we have new messages, try the filter plugins.
  Din_Go(948,2048);if (NS_SUCCEEDED(rv) && hasNewMessages)
    {/*45*/Din_Go(947,2048);(void) CallFilterPlugins(aWindow, &filtersRun);/*46*/}
  // Callers should rely on folder loaded event to ensure completion of loading. So we'll
  // return NS_OK even if parsing is still in progress
  Din_Go(950,2048);if (rv == NS_ERROR_NOT_INITIALIZED)
    {/*47*/Din_Go(949,2048);rv = NS_OK;/*48*/}
  {__IAENUM__ nsresult  ReplaceReturn140 = rv; Din_Go(951,2048); return ReplaceReturn140;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::GetMessages(nsISimpleEnumerator **result)
{
  Din_Go(952,2048);nsCOMPtr <nsIMsgDatabase> msgDB;
  nsresult rv = GetDatabaseWOReparse(getter_AddRefs(msgDB));
  {__IAENUM__ nsresult  ReplaceReturn139 = NS_SUCCEEDED(rv) ? msgDB->EnumerateMessages(result) : rv; Din_Go(953,2048); return ReplaceReturn139;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::GetFolderURL(nsACString& aUrl)
{
  Din_Go(954,2048);nsresult rv;
  nsCOMPtr<nsIFile> path;
  rv = GetFilePath(getter_AddRefs(path));
  Din_Go(956,2048);if (NS_FAILED(rv))
    {/*49*/{__IAENUM__ nsresult  ReplaceReturn138 = rv; Din_Go(955,2048); return ReplaceReturn138;}/*50*/}

  Din_Go(957,2048);rv = NS_GetURLSpecFromFile(path, aUrl);
  NS_ENSURE_SUCCESS(rv, rv);

  aUrl.Replace(0, strlen("file:"), "mailbox:");

  {__IAENUM__ nsresult  ReplaceReturn137 = NS_OK; Din_Go(958,2048); return ReplaceReturn137;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::CreateStorageIfMissing(nsIUrlListener* aUrlListener)
{
  Din_Go(959,2048);nsresult rv;
  nsCOMPtr<nsIMsgFolder> msgParent;
  GetParent(getter_AddRefs(msgParent));

  // parent is probably not set because *this* was probably created by rdf
  // and not by folder discovery. So, we have to compute the parent.
  Din_Go(963,2048);if (!msgParent)
  {
    Din_Go(960,2048);nsAutoCString folderName(mURI);
    nsAutoCString uri;
    int32_t leafPos = folderName.RFindChar('/');
    nsAutoCString parentName(folderName);
    Din_Go(962,2048);if (leafPos > 0)
    {
      // If there is a hierarchy, there is a parent.
      // Don't strip off slash if it's the first character
      Din_Go(961,2048);parentName.SetLength(leafPos);
      // get the corresponding RDF resource
      // RDF will create the folder resource if it doesn't already exist
      nsCOMPtr<nsIRDFService> rdf = do_GetService("@mozilla.org/rdf/rdf-service;1", &rv);
      NS_ENSURE_SUCCESS(rv,rv);

      nsCOMPtr<nsIRDFResource> resource;
      rv = rdf->GetResource(parentName, getter_AddRefs(resource));
      NS_ENSURE_SUCCESS(rv,rv);

      msgParent = do_QueryInterface(resource, &rv);
      NS_ENSURE_SUCCESS(rv,rv);
    }
  }

  Din_Go(967,2048);if (msgParent)
  {
    Din_Go(964,2048);nsString folderName;
    GetName(folderName);
    rv = msgParent->CreateSubfolder(folderName, nullptr);
    // by definition, this is OK.
    Din_Go(966,2048);if (rv == NS_MSG_FOLDER_EXISTS)
      {/*51*/{__IAENUM__ nsresult  ReplaceReturn136 = NS_OK; Din_Go(965,2048); return ReplaceReturn136;}/*52*/}
  }

  {__IAENUM__ nsresult  ReplaceReturn135 = rv; Din_Go(968,2048); return ReplaceReturn135;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::CreateSubfolder(const nsAString& folderName, nsIMsgWindow *msgWindow)
{
  Din_Go(969,2048);nsCOMPtr<nsIMsgFolder> newFolder;
  nsresult rv = CreateSubfolderInternal(folderName, msgWindow, getter_AddRefs(newFolder));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIMsgFolderNotificationService> notifier(do_GetService(NS_MSGNOTIFICATIONSERVICE_CONTRACTID));
  Din_Go(971,2048);if (notifier)
    {/*53*/Din_Go(970,2048);notifier->NotifyFolderAdded(newFolder);/*54*/}

  {__IAENUM__ nsresult  ReplaceReturn134 = NS_OK; Din_Go(972,2048); return ReplaceReturn134;}
}

nsresult
nsMsgLocalMailFolder::CreateSubfolderInternal(const nsAString& folderName,
                                              nsIMsgWindow *msgWindow,
                                              nsIMsgFolder **aNewFolder)
{
  Din_Go(973,2048);nsresult rv = CheckIfFolderExists(folderName, this, msgWindow);
  // No need for an assertion: we already throw an alert.
  Din_Go(975,2048);if (NS_FAILED(rv))
    {/*55*/{__IAENUM__ nsresult  ReplaceReturn133 = rv; Din_Go(974,2048); return ReplaceReturn133;}/*56*/}
  Din_Go(976,2048);nsCOMPtr<nsIMsgPluggableStore> msgStore;
  rv = GetMsgStore(getter_AddRefs(msgStore));
  NS_ENSURE_SUCCESS(rv, rv);
  rv = msgStore->CreateFolder(this, folderName, aNewFolder);
  Din_Go(980,2048);if (rv == NS_MSG_ERROR_INVALID_FOLDER_NAME)
  {
    Din_Go(977,2048);ThrowAlertMsg("folderCreationFailed", msgWindow);
  }
  else {/*57*/Din_Go(978,2048);if (rv == NS_MSG_FOLDER_EXISTS)
  {
    Din_Go(979,2048);ThrowAlertMsg("folderExists", msgWindow);
  ;/*58*/}}

  Din_Go(984,2048);if (NS_SUCCEEDED(rv))
  {
    Din_Go(981,2048);nsCOMPtr<nsIMsgFolder> child = *aNewFolder;
    //we need to notify explicitly the flag change because it failed when we did AddSubfolder
    child->OnFlagChange(mFlags);
    child->SetPrettyName(folderName);  //because empty trash will create a new trash folder
    NotifyItemAdded(child);
    Din_Go(983,2048);if (aNewFolder)
      {/*59*/Din_Go(982,2048);child.swap(*aNewFolder);/*60*/}
  }

  {__IAENUM__ nsresult  ReplaceReturn132 = rv; Din_Go(985,2048); return ReplaceReturn132;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::CompactAll(nsIUrlListener *aListener,
                                               nsIMsgWindow *aMsgWindow,
                                               bool aCompactOfflineAlso)
{
  Din_Go(986,2048);nsresult rv = NS_OK;
  nsCOMPtr<nsIMutableArray> folderArray;
  nsCOMPtr<nsIMsgFolder> rootFolder;
  nsCOMPtr<nsIArray> allDescendents;
  rv = GetRootFolder(getter_AddRefs(rootFolder));
  nsCOMPtr<nsIMsgPluggableStore> msgStore;
  GetMsgStore(getter_AddRefs(msgStore));
  bool storeSupportsCompaction;
  msgStore->GetSupportsCompaction(&storeSupportsCompaction);
  Din_Go(988,2048);if (!storeSupportsCompaction)
    {/*61*/{__IAENUM__ nsresult  ReplaceReturn131 = NotifyCompactCompleted(); Din_Go(987,2048); return ReplaceReturn131;}/*62*/}

  Din_Go(999,2048);if (NS_SUCCEEDED(rv) && rootFolder)
  {
    Din_Go(989,2048);rv = rootFolder->GetDescendants(getter_AddRefs(allDescendents));
    NS_ENSURE_SUCCESS(rv, rv);
    uint32_t cnt = 0;
    rv = allDescendents->GetLength(&cnt);
    NS_ENSURE_SUCCESS(rv, rv);
    folderArray = do_CreateInstance(NS_ARRAY_CONTRACTID, &rv);
    int64_t expungedBytes = 0;
    Din_Go(995,2048);for (uint32_t i = 0; i < cnt; i++)
    {
      Din_Go(990,2048);nsCOMPtr<nsIMsgFolder> folder = do_QueryElementAt(allDescendents, i, &rv);
      NS_ENSURE_SUCCESS(rv, rv);

      expungedBytes = 0;
      Din_Go(992,2048);if (folder)
        {/*63*/Din_Go(991,2048);rv = folder->GetExpungedBytes(&expungedBytes);/*64*/}

      NS_ENSURE_SUCCESS(rv, rv);

      Din_Go(994,2048);if (expungedBytes > 0)
        {/*65*/Din_Go(993,2048);rv = folderArray->AppendElement(folder, false);/*66*/}
    }
    Din_Go(996,2048);rv = folderArray->GetLength(&cnt);
    NS_ENSURE_SUCCESS(rv,rv);
    Din_Go(998,2048);if (cnt == 0)
      {/*67*/{__IAENUM__ nsresult  ReplaceReturn130 = NotifyCompactCompleted(); Din_Go(997,2048); return ReplaceReturn130;}/*68*/}
  }
  Din_Go(1000,2048);nsCOMPtr <nsIMsgFolderCompactor> folderCompactor =  do_CreateInstance(NS_MSGLOCALFOLDERCOMPACTOR_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  {__IAENUM__ nsresult  ReplaceReturn129 = folderCompactor->CompactFolders(folderArray, nullptr,
                                         aListener, aMsgWindow); Din_Go(1001,2048); return ReplaceReturn129;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::Compact(nsIUrlListener *aListener, nsIMsgWindow *aMsgWindow)
{
  Din_Go(1002,2048);nsCOMPtr<nsIMsgPluggableStore> msgStore;
  nsresult rv = GetMsgStore(getter_AddRefs(msgStore));
  NS_ENSURE_SUCCESS(rv, rv);
  bool supportsCompaction;
  msgStore->GetSupportsCompaction(&supportsCompaction);
  Din_Go(1004,2048);if (supportsCompaction)
    {/*69*/{__IAENUM__ nsresult  ReplaceReturn128 = msgStore->CompactFolder(this, aListener, aMsgWindow); Din_Go(1003,2048); return ReplaceReturn128;}/*70*/}
  {__IAENUM__ nsresult  ReplaceReturn127 = NS_OK; Din_Go(1005,2048); return ReplaceReturn127;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::EmptyTrash(nsIMsgWindow *msgWindow,
                                               nsIUrlListener *aListener)
{
  Din_Go(1006,2048);nsresult rv;
  nsCOMPtr<nsIMsgFolder> trashFolder;
  rv = GetTrashFolder(getter_AddRefs(trashFolder));
  Din_Go(1023,2048);if (NS_SUCCEEDED(rv))
  {
    Din_Go(1007,2048);uint32_t flags;
    nsCString trashUri;
    trashFolder->GetURI(trashUri);
    trashFolder->GetFlags(&flags);
    int32_t totalMessages = 0;
    rv = trashFolder->GetTotalMessages(true, &totalMessages);
    Din_Go(1011,2048);if (totalMessages <= 0)
    {
      Din_Go(1008,2048);nsCOMPtr<nsISimpleEnumerator> enumerator;
      rv = trashFolder->GetSubFolders(getter_AddRefs(enumerator));
      NS_ENSURE_SUCCESS(rv,rv);
      // Any folders to deal with?
      bool hasMore;
      rv = enumerator->HasMoreElements(&hasMore);
      Din_Go(1010,2048);if (NS_FAILED(rv) || !hasMore)
        {/*71*/{__IAENUM__ nsresult  ReplaceReturn126 = NS_OK; Din_Go(1009,2048); return ReplaceReturn126;}/*72*/}
    }
    Din_Go(1012,2048);nsCOMPtr<nsIMsgFolder> parentFolder;
    rv = trashFolder->GetParent(getter_AddRefs(parentFolder));
    Din_Go(1022,2048);if (NS_SUCCEEDED(rv) && parentFolder)
    {
      Din_Go(1013,2048);nsCOMPtr <nsIDBFolderInfo> transferInfo;
      trashFolder->GetDBTransferInfo(getter_AddRefs(transferInfo));
      trashFolder->SetParent(nullptr);
      parentFolder->PropagateDelete(trashFolder, true, msgWindow);
      parentFolder->CreateSubfolder(NS_LITERAL_STRING("Trash"), nullptr);
      nsCOMPtr<nsIMsgFolder> newTrashFolder;
      rv = GetTrashFolder(getter_AddRefs(newTrashFolder));
      Din_Go(1021,2048);if (NS_SUCCEEDED(rv) && newTrashFolder)
      {
        Din_Go(1014,2048);nsCOMPtr <nsIMsgLocalMailFolder> localTrash = do_QueryInterface(newTrashFolder);
        newTrashFolder->SetDBTransferInfo(transferInfo);
        Din_Go(1016,2048);if (localTrash)
          {/*73*/Din_Go(1015,2048);localTrash->RefreshSizeOnDisk();/*74*/}
        // update the summary totals so the front end will
        // show the right thing for the new trash folder
        // see bug #161999
        Din_Go(1017,2048);nsCOMPtr<nsIDBFolderInfo> dbFolderInfo;
        nsCOMPtr<nsIMsgDatabase> db;
        newTrashFolder->GetDBFolderInfoAndDB(getter_AddRefs(dbFolderInfo), getter_AddRefs(db));
        Din_Go(1019,2048);if (dbFolderInfo)
        {
          Din_Go(1018,2048);dbFolderInfo->SetNumUnreadMessages(0);
          dbFolderInfo->SetNumMessages(0);
        }
        Din_Go(1020,2048);newTrashFolder->UpdateSummaryTotals(true);
      }
    }
  }
  {__IAENUM__ nsresult  ReplaceReturn125 = rv; Din_Go(1024,2048); return ReplaceReturn125;}
}

nsresult nsMsgLocalMailFolder::IsChildOfTrash(bool *result)
{
Din_Go(1025,2048);  NS_ENSURE_ARG_POINTER(result);
  Din_Go(1026,2048);uint32_t parentFlags = 0;
  *result = false;
  bool isServer;
  nsresult rv = GetIsServer(&isServer);
  Din_Go(1028,2048);if (NS_FAILED(rv) || isServer)
    {/*75*/{__IAENUM__ nsresult  ReplaceReturn124 = NS_OK; Din_Go(1027,2048); return ReplaceReturn124;}/*76*/}

  Din_Go(1029,2048);rv= GetFlags(&parentFlags);  //this is the parent folder
  Din_Go(1032,2048);if (parentFlags & nsMsgFolderFlags::Trash)
  {
    Din_Go(1030,2048);*result = true;
    {__IAENUM__ nsresult  ReplaceReturn123 = rv; Din_Go(1031,2048); return ReplaceReturn123;}
  }

  Din_Go(1033,2048);nsCOMPtr<nsIMsgFolder> parentFolder;
  nsCOMPtr<nsIMsgFolder> thisFolder;
  rv = QueryInterface(NS_GET_IID(nsIMsgFolder), (void **) getter_AddRefs(thisFolder));

  Din_Go(1047,2048);while (!isServer)
  {
    Din_Go(1034,2048);thisFolder->GetParent(getter_AddRefs(parentFolder));
    Din_Go(1036,2048);if (!parentFolder)
      {/*77*/{__IAENUM__ nsresult  ReplaceReturn122 = NS_OK; Din_Go(1035,2048); return ReplaceReturn122;}/*78*/}

    Din_Go(1037,2048);rv = parentFolder->GetIsServer(&isServer);
    Din_Go(1039,2048);if (NS_FAILED(rv) || isServer)
      {/*79*/{__IAENUM__ nsresult  ReplaceReturn121 = NS_OK; Din_Go(1038,2048); return ReplaceReturn121;}/*80*/}

    Din_Go(1040,2048);rv = parentFolder->GetFlags(&parentFlags);
    Din_Go(1042,2048);if (NS_FAILED(rv))
      {/*81*/{__IAENUM__ nsresult  ReplaceReturn120 = NS_OK; Din_Go(1041,2048); return ReplaceReturn120;}/*82*/}

    Din_Go(1045,2048);if (parentFlags & nsMsgFolderFlags::Trash)
    {
      Din_Go(1043,2048);*result = true;
      {__IAENUM__ nsresult  ReplaceReturn119 = rv; Din_Go(1044,2048); return ReplaceReturn119;}
    }

    Din_Go(1046,2048);thisFolder = parentFolder;
  }
  {__IAENUM__ nsresult  ReplaceReturn118 = rv; Din_Go(1048,2048); return ReplaceReturn118;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::Delete()
{
  Din_Go(1049,2048);nsresult rv;
  nsCOMPtr<nsIMsgDBService> msgDBService = do_GetService(NS_MSGDB_SERVICE_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  msgDBService->CachedDBForFolder(this, getter_AddRefs(mDatabase));
  Din_Go(1051,2048);if (mDatabase)
  {
    Din_Go(1050,2048);mDatabase->ForceClosed();
    mDatabase = nullptr;
  }

  Din_Go(1052,2048);nsCOMPtr<nsIMsgIncomingServer> server;
  rv = GetServer(getter_AddRefs(server));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIMsgPluggableStore> msgStore;

  rv = server->GetMsgStore(getter_AddRefs(msgStore));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr <nsIFile> summaryFile;
  rv = GetSummaryFile(getter_AddRefs(summaryFile));
  NS_ENSURE_SUCCESS(rv, rv);

  //Clean up .sbd folder if it exists.
  // Remove summary file.
  rv = summaryFile->Remove(false);
  NS_WARN_IF_FALSE(NS_SUCCEEDED(rv), "Could not delete msg summary file");

  rv = msgStore->DeleteFolder(this);
  Din_Go(1054,2048);if (rv == NS_ERROR_FILE_NOT_FOUND ||
      rv == NS_ERROR_FILE_TARGET_DOES_NOT_EXIST)
    {/*83*/Din_Go(1053,2048);rv = NS_OK;/*84*/} // virtual folders do not have a msgStore file
  {__IAENUM__ nsresult  ReplaceReturn117 = rv; Din_Go(1055,2048); return ReplaceReturn117;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::DeleteSubFolders(nsIArray *folders, nsIMsgWindow *msgWindow)
{
  Din_Go(1056,2048);nsresult rv;
  bool isChildOfTrash;
  IsChildOfTrash(&isChildOfTrash);

  // we don't allow multiple folder selection so this is ok.
  nsCOMPtr<nsIMsgFolder> folder = do_QueryElementAt(folders, 0);
  uint32_t folderFlags = 0;
  Din_Go(1058,2048);if (folder)
    {/*85*/Din_Go(1057,2048);folder->GetFlags(&folderFlags);/*86*/}
  // when deleting from trash, or virtual folder, just delete it.
  Din_Go(1060,2048);if (isChildOfTrash || folderFlags & nsMsgFolderFlags::Virtual)
    {/*87*/{__IAENUM__ nsresult  ReplaceReturn116 = nsMsgDBFolder::DeleteSubFolders(folders, msgWindow); Din_Go(1059,2048); return ReplaceReturn116;}/*88*/}

  Din_Go(1061,2048);nsCOMPtr<nsIMsgFolder> trashFolder;
  rv = GetTrashFolder(getter_AddRefs(trashFolder));
  Din_Go(1064,2048);if (NS_SUCCEEDED(rv))
  {
    Din_Go(1062,2048);if (folder)
    {
      Din_Go(1063,2048);nsCOMPtr<nsIMsgCopyService> copyService(do_GetService(NS_MSGCOPYSERVICE_CONTRACTID, &rv));
      NS_ENSURE_SUCCESS(rv, rv);
      rv = copyService->CopyFolders(folders, trashFolder, true, nullptr, msgWindow);
    }
  }
  {__IAENUM__ nsresult  ReplaceReturn115 = rv; Din_Go(1065,2048); return ReplaceReturn115;}
}

nsresult nsMsgLocalMailFolder::ConfirmFolderDeletion(nsIMsgWindow *aMsgWindow,
                                                     nsIMsgFolder *aFolder, bool *aResult)
{
Din_Go(1066,2048);  NS_ENSURE_ARG(aResult);
  NS_ENSURE_ARG(aMsgWindow);
  NS_ENSURE_ARG(aFolder);
  Din_Go(1067,2048);nsCOMPtr<nsIDocShell> docShell;
  aMsgWindow->GetRootDocShell(getter_AddRefs(docShell));
  Din_Go(1074,2048);if (docShell)
  {
    Din_Go(1068,2048);bool confirmDeletion = true;
    nsresult rv;
    nsCOMPtr<nsIPrefBranch> pPrefBranch(do_GetService(NS_PREFSERVICE_CONTRACTID, &rv));
    NS_ENSURE_SUCCESS(rv, rv);
    pPrefBranch->GetBoolPref("mailnews.confirm.moveFoldersToTrash", &confirmDeletion);
    Din_Go(1073,2048);if (confirmDeletion)
    {
      Din_Go(1069,2048);nsCOMPtr<nsIStringBundleService> bundleService =
        mozilla::services::GetStringBundleService();
      NS_ENSURE_TRUE(bundleService, NS_ERROR_UNEXPECTED);
      nsCOMPtr<nsIStringBundle> bundle;
      rv = bundleService->CreateBundle("chrome://messenger/locale/localMsgs.properties", getter_AddRefs(bundle));
      NS_ENSURE_SUCCESS(rv, rv);

      nsAutoString folderName;
      rv = aFolder->GetName(folderName);
      NS_ENSURE_SUCCESS(rv, rv);
      const char16_t *formatStrings[1] = { folderName.get() };

      nsAutoString deleteFolderDialogTitle;
      rv = bundle->GetStringFromName(
        MOZ_UTF16("pop3DeleteFolderDialogTitle"),
        getter_Copies(deleteFolderDialogTitle));
      NS_ENSURE_SUCCESS(rv, rv);

      nsAutoString deleteFolderButtonLabel;
      rv = bundle->GetStringFromName(
        MOZ_UTF16("pop3DeleteFolderButtonLabel"),
        getter_Copies(deleteFolderButtonLabel));
      NS_ENSURE_SUCCESS(rv, rv);

      nsAutoString confirmationStr;
      rv = bundle->FormatStringFromName(
        MOZ_UTF16("pop3MoveFolderToTrash"), formatStrings, 1,
        getter_Copies(confirmationStr));
      NS_ENSURE_SUCCESS(rv, rv);

      nsCOMPtr<nsIPrompt> dialog(do_GetInterface(docShell));
      Din_Go(1071,2048);if (dialog)
      {
        Din_Go(1070,2048);int32_t buttonPressed = 0;
        // Default the dialog to "cancel".
        const uint32_t buttonFlags =
          (nsIPrompt::BUTTON_TITLE_IS_STRING * nsIPrompt::BUTTON_POS_0) +
          (nsIPrompt::BUTTON_TITLE_CANCEL * nsIPrompt::BUTTON_POS_1);
        bool dummyValue = false;
        rv = dialog->ConfirmEx(deleteFolderDialogTitle.get(), confirmationStr.get(),
                               buttonFlags,  deleteFolderButtonLabel.get(),
                               nullptr, nullptr, nullptr, &dummyValue,
                               &buttonPressed);
        NS_ENSURE_SUCCESS(rv, rv);
        *aResult = !buttonPressed; // "ok" is in position 0
      }
    }
    else
      {/*89*/Din_Go(1072,2048);*aResult = true;/*90*/}
  }
  {__IAENUM__ nsresult  ReplaceReturn114 = NS_OK; Din_Go(1075,2048); return ReplaceReturn114;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::Rename(const nsAString& aNewName, nsIMsgWindow *msgWindow)
{
  // Renaming to the same name is easy
  Din_Go(1076,2048);if (mName.Equals(aNewName))
    {/*91*/{__IAENUM__ nsresult  ReplaceReturn113 = NS_OK; Din_Go(1077,2048); return ReplaceReturn113;}/*92*/}

  Din_Go(1078,2048);nsCOMPtr<nsIMsgFolder> parentFolder;
  nsresult rv = GetParent(getter_AddRefs(parentFolder));
  Din_Go(1080,2048);if (!parentFolder)
    {/*93*/{__IAENUM__ nsresult  ReplaceReturn112 = NS_ERROR_NULL_POINTER; Din_Go(1079,2048); return ReplaceReturn112;}/*94*/}

  Din_Go(1081,2048);rv = CheckIfFolderExists(aNewName, parentFolder, msgWindow);
  Din_Go(1083,2048);if (NS_FAILED(rv))
    {/*95*/{__IAENUM__ nsresult  ReplaceReturn111 = rv; Din_Go(1082,2048); return ReplaceReturn111;}/*96*/}

  Din_Go(1084,2048);nsCOMPtr<nsIMsgPluggableStore> msgStore;
  nsCOMPtr<nsIMsgFolder> newFolder;
  rv = GetMsgStore(getter_AddRefs(msgStore));
  NS_ENSURE_SUCCESS(rv, rv);
  rv = msgStore->RenameFolder(this, aNewName, getter_AddRefs(newFolder));
  Din_Go(1088,2048);if (NS_FAILED(rv))
  {
    Din_Go(1085,2048);if (msgWindow)
      {/*97*/Din_Go(1086,2048);(void) ThrowAlertMsg((rv == NS_MSG_FOLDER_EXISTS) ?
                            "folderExists" : "folderRenameFailed", msgWindow);/*98*/}
    {__IAENUM__ nsresult  ReplaceReturn110 = rv; Din_Go(1087,2048); return ReplaceReturn110;}
  }

  Din_Go(1089,2048);int32_t count = mSubFolders.Count();
    Din_Go(1101,2048);if (newFolder)
    {
      // Because we just renamed the db, w/o setting the pretty name in it,
      // we need to force the pretty name to be correct.
      // SetPrettyName won't write the name to the db if it doesn't think the
      // name has changed. This hack forces the pretty name to get set in the db.
      // We could set the new pretty name on the db before renaming the .msf file,
      // but if the rename failed, it would be out of sync.
      Din_Go(1090,2048);newFolder->SetPrettyName(EmptyString());
      newFolder->SetPrettyName(aNewName);
      bool changed = false;
      MatchOrChangeFilterDestination(newFolder, true /*caseInsenstive*/, &changed);
      Din_Go(1092,2048);if (changed)
        {/*99*/Din_Go(1091,2048);AlertFilterChanged(msgWindow);/*100*/}

      Din_Go(1094,2048);if (count > 0)
        {/*101*/Din_Go(1093,2048);newFolder->RenameSubFolders(msgWindow, this);/*102*/}

      // Discover the subfolders inside this folder (this is recursive)
      Din_Go(1095,2048);nsCOMPtr<nsISimpleEnumerator> dummy;
      newFolder->GetSubFolders(getter_AddRefs(dummy));

      // the newFolder should have the same flags
      newFolder->SetFlags(mFlags);
      Din_Go(1097,2048);if (parentFolder)
      {
        Din_Go(1096,2048);SetParent(nullptr);
        parentFolder->PropagateDelete(this, false, msgWindow);
        parentFolder->NotifyItemAdded(newFolder);
      }
      Din_Go(1098,2048);SetFilePath(nullptr); // forget our path, since this folder object renamed itself
      nsCOMPtr<nsIAtom> folderRenameAtom = MsgGetAtom("RenameCompleted");
      newFolder->NotifyFolderEvent(folderRenameAtom);

      nsCOMPtr<nsIMsgFolderNotificationService> notifier(do_GetService(NS_MSGNOTIFICATIONSERVICE_CONTRACTID));
      Din_Go(1100,2048);if (notifier)
        {/*103*/Din_Go(1099,2048);notifier->NotifyFolderRenamed(this, newFolder);/*104*/}
    }
  {__IAENUM__ nsresult  ReplaceReturn109 = rv; Din_Go(1102,2048); return ReplaceReturn109;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::RenameSubFolders(nsIMsgWindow *msgWindow, nsIMsgFolder *oldFolder)
{
  Din_Go(1103,2048);nsresult rv =NS_OK;
  mInitialized = true;

  uint32_t flags;
  oldFolder->GetFlags(&flags);
  SetFlags(flags);

  nsCOMPtr<nsISimpleEnumerator> enumerator;
  rv = oldFolder->GetSubFolders(getter_AddRefs(enumerator));
  NS_ENSURE_SUCCESS(rv, rv);

  bool hasMore;
  Din_Go(1113,2048);while (NS_SUCCEEDED(enumerator->HasMoreElements(&hasMore)) && hasMore)
  {
    Din_Go(1104,2048);nsCOMPtr<nsISupports> item;
    enumerator->GetNext(getter_AddRefs(item));

    nsCOMPtr<nsIMsgFolder> msgFolder(do_QueryInterface(item));
    Din_Go(1106,2048);if (!msgFolder)
      {/*105*/Din_Go(1105,2048);continue;/*106*/}

    Din_Go(1107,2048);nsString folderName;
    rv = msgFolder->GetName(folderName);
    nsCOMPtr <nsIMsgFolder> newFolder;
    AddSubfolder(folderName, getter_AddRefs(newFolder));
    Din_Go(1112,2048);if (newFolder)
    {
      Din_Go(1108,2048);newFolder->SetPrettyName(folderName);
      bool changed = false;
      msgFolder->MatchOrChangeFilterDestination(newFolder, true /*caseInsenstive*/, &changed);
      Din_Go(1110,2048);if (changed)
        {/*107*/Din_Go(1109,2048);msgFolder->AlertFilterChanged(msgWindow);/*108*/}
      Din_Go(1111,2048);newFolder->RenameSubFolders(msgWindow, msgFolder);
    }
  }
  {__IAENUM__ nsresult  ReplaceReturn108 = NS_OK; Din_Go(1114,2048); return ReplaceReturn108;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::GetPrettyName(nsAString& prettyName)
{
  {__IAENUM__ nsresult  ReplaceReturn107 = nsMsgDBFolder::GetPrettyName(prettyName); Din_Go(1115,2048); return ReplaceReturn107;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::SetPrettyName(const nsAString& aName)
{
  Din_Go(1116,2048);nsresult rv = nsMsgDBFolder::SetPrettyName(aName);
  NS_ENSURE_SUCCESS(rv, rv);
  nsCString folderName;
  rv = GetStringProperty("folderName", folderName);
  NS_ConvertUTF16toUTF8 utf8FolderName(mName);
  {__IAENUM__ nsresult  ReplaceReturn106 = NS_FAILED(rv) || !folderName.Equals(utf8FolderName) ? SetStringProperty("folderName", utf8FolderName) : rv; Din_Go(1117,2048); return ReplaceReturn106;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::GetName(nsAString& aName)
{
  Din_Go(1118,2048);ReadDBFolderInfo(false);
  {__IAENUM__ nsresult  ReplaceReturn105 = nsMsgDBFolder::GetName(aName); Din_Go(1119,2048); return ReplaceReturn105;}
}

nsresult nsMsgLocalMailFolder::OpenDatabase()
{
  Din_Go(1120,2048);nsresult rv;
  nsCOMPtr<nsIMsgDBService> msgDBService = do_GetService(NS_MSGDB_SERVICE_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr <nsIFile> file;
  rv = GetFilePath(getter_AddRefs(file));

  rv = msgDBService->OpenFolderDB(this, true, getter_AddRefs(mDatabase));
    Din_Go(1130,2048);if (rv == NS_MSG_ERROR_FOLDER_SUMMARY_MISSING)
    {
    // check if we're a real folder by looking at the parent folder.
    Din_Go(1121,2048);nsCOMPtr<nsIMsgFolder> parent;
    GetParent(getter_AddRefs(parent));
    Din_Go(1127,2048);if (parent)
    {
      // This little dance creates an empty .msf file and then checks
      // if the db is valid - this works if the folder is empty, which
      // we don't have a direct way of checking.
      Din_Go(1122,2048);nsCOMPtr<nsIMsgDatabase> db;
      rv = msgDBService->CreateNewDB(this, getter_AddRefs(db));
      Din_Go(1126,2048);if (db)
      {
        Din_Go(1123,2048);UpdateSummaryTotals(true);
        db->Close(true);
        mDatabase = nullptr;
        db = nullptr;
        rv = msgDBService->OpenFolderDB(this, false,
                                        getter_AddRefs(mDatabase));
        Din_Go(1125,2048);if (NS_FAILED(rv))
          {/*109*/Din_Go(1124,2048);mDatabase = nullptr;/*110*/}
      }
    }
  }
  else {/*111*/Din_Go(1128,2048);if (NS_FAILED(rv))
    {/*113*/Din_Go(1129,2048);mDatabase = nullptr;/*114*/}/*112*/}

  {__IAENUM__ nsresult  ReplaceReturn104 = rv; Din_Go(1131,2048); return ReplaceReturn104;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::GetDBFolderInfoAndDB(nsIDBFolderInfo **folderInfo, nsIMsgDatabase **db)
{
  Din_Go(1132,2048);if (!db || !folderInfo || !mPath || mIsServer)
    {/*115*/{__IAENUM__ nsresult  ReplaceReturn103 = NS_ERROR_NULL_POINTER; Din_Go(1133,2048); return ReplaceReturn103;}/*116*/}   //ducarroz: should we use NS_ERROR_INVALID_ARG?

  Din_Go(1134,2048);nsresult rv;
  Din_Go(1139,2048);if (mDatabase)
    {/*117*/Din_Go(1135,2048);rv = NS_OK;/*118*/}
  else
  {
    Din_Go(1136,2048);rv = OpenDatabase();

    Din_Go(1138,2048);if (mAddListener && mDatabase)
      {/*119*/Din_Go(1137,2048);mDatabase->AddListener(this);/*120*/}
  }

  NS_IF_ADDREF(*db = mDatabase);
  Din_Go(1141,2048);if (NS_SUCCEEDED(rv) && *db)
    {/*121*/Din_Go(1140,2048);rv = (*db)->GetDBFolderInfo(folderInfo);/*122*/}
  {__IAENUM__ nsresult  ReplaceReturn102 = rv; Din_Go(1142,2048); return ReplaceReturn102;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::ReadFromFolderCacheElem(nsIMsgFolderCacheElement *element)
{
Din_Go(1143,2048);  NS_ENSURE_ARG_POINTER(element);
  Din_Go(1144,2048);nsresult rv = nsMsgDBFolder::ReadFromFolderCacheElem(element);
  NS_ENSURE_SUCCESS(rv, rv);
  nsCString utf8Name;
  rv = element->GetStringProperty("folderName", utf8Name);
  NS_ENSURE_SUCCESS(rv, rv);
  CopyUTF8toUTF16(utf8Name, mName);
  {__IAENUM__ nsresult  ReplaceReturn101 = rv; Din_Go(1145,2048); return ReplaceReturn101;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::WriteToFolderCacheElem(nsIMsgFolderCacheElement *element)
{
Din_Go(1146,2048);  NS_ENSURE_ARG_POINTER(element);
  Din_Go(1147,2048);nsMsgDBFolder::WriteToFolderCacheElem(element);
  {__IAENUM__ nsresult  ReplaceReturn100 = element->SetStringProperty("folderName", NS_ConvertUTF16toUTF8(mName)); Din_Go(1148,2048); return ReplaceReturn100;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::GetDeletable(bool *deletable)
{
Din_Go(1149,2048);  NS_ENSURE_ARG_POINTER(deletable);

  Din_Go(1150,2048);bool isServer;
  GetIsServer(&isServer);
  *deletable = !(isServer || (mFlags & nsMsgFolderFlags::SpecialUse));
  {__IAENUM__ nsresult  ReplaceReturn99 = NS_OK; Din_Go(1151,2048); return ReplaceReturn99;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::RefreshSizeOnDisk()
{
  Din_Go(1152,2048);int64_t oldFolderSize = mFolderSize;
  // we set this to unknown to force it to get recalculated from disk
  mFolderSize = kSizeUnknown;
  Din_Go(1154,2048);if (NS_SUCCEEDED(GetSizeOnDisk(&mFolderSize)))
    {/*123*/Din_Go(1153,2048);NotifyIntPropertyChanged(kFolderSizeAtom, oldFolderSize, mFolderSize);/*124*/}
  {__IAENUM__ nsresult  ReplaceReturn98 = NS_OK; Din_Go(1155,2048); return ReplaceReturn98;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::GetSizeOnDisk(int64_t *aSize)
{
Din_Go(1156,2048);  NS_ENSURE_ARG_POINTER(aSize);

  Din_Go(1157,2048);bool isServer = false;
  nsresult rv = GetIsServer(&isServer);
  // If this is the rootFolder, return 0 as a safe value.
  Din_Go(1159,2048);if (NS_FAILED(rv) || isServer)
    {/*125*/Din_Go(1158,2048);mFolderSize = 0;/*126*/}

  Din_Go(1161,2048);if (mFolderSize == kSizeUnknown)
  {
    Din_Go(1160,2048);nsCOMPtr<nsIFile> file;
    rv = GetFilePath(getter_AddRefs(file));
    NS_ENSURE_SUCCESS(rv, rv);
    // Use a temporary variable so that we keep mFolderSize on kSizeUnknown
    // if GetFileSize() fails.
    int64_t folderSize;
    rv = file->GetFileSize(&folderSize);
    NS_ENSURE_SUCCESS(rv, rv);

    mFolderSize = folderSize;
  }
  Din_Go(1162,2048);*aSize = mFolderSize;
  {__IAENUM__ nsresult  ReplaceReturn97 = NS_OK; Din_Go(1163,2048); return ReplaceReturn97;}
}

nsresult
nsMsgLocalMailFolder::GetTrashFolder(nsIMsgFolder** result)
{
Din_Go(1164,2048);  NS_ENSURE_ARG_POINTER(result);
  Din_Go(1165,2048);nsresult rv;
  nsCOMPtr<nsIMsgFolder> rootFolder;
  rv = GetRootFolder(getter_AddRefs(rootFolder));
  Din_Go(1169,2048);if (NS_SUCCEEDED(rv))
  {
    Din_Go(1166,2048);rootFolder->GetFolderWithFlags(nsMsgFolderFlags::Trash, result);
    Din_Go(1168,2048);if (!*result)
      {/*127*/Din_Go(1167,2048);rv = NS_ERROR_FAILURE;/*128*/}
  }
  {__IAENUM__ nsresult  ReplaceReturn96 = rv; Din_Go(1170,2048); return ReplaceReturn96;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::DeleteMessages(nsIArray *messages,
                                     nsIMsgWindow *msgWindow,
                                     bool deleteStorage, bool isMove,
                                     nsIMsgCopyServiceListener* listener, bool allowUndo)
{
Din_Go(1171,2048);  NS_ENSURE_ARG_POINTER(messages);

  Din_Go(1172,2048);uint32_t messageCount;
  nsresult rv = messages->GetLength(&messageCount);
  NS_ENSURE_SUCCESS(rv, rv);

  // shift delete case - (delete to trash is handled in EndMove)
  // this is also the case when applying retention settings.
  Din_Go(1174,2048);if (deleteStorage && !isMove)
  {
    Din_Go(1173,2048);MarkMsgsOnPop3Server(messages, POP3_DELETE);
  }

  Din_Go(1175,2048);bool isTrashFolder = mFlags & nsMsgFolderFlags::Trash;

  // notify on delete from trash and shift-delete
  Din_Go(1179,2048);if (!isMove && (deleteStorage || isTrashFolder))
  {
    Din_Go(1176,2048);nsCOMPtr<nsIMsgFolderNotificationService> notifier(do_GetService(NS_MSGNOTIFICATIONSERVICE_CONTRACTID));
    Din_Go(1178,2048);if (notifier)
        {/*129*/Din_Go(1177,2048);notifier->NotifyMsgsDeleted(messages);/*130*/}
  }

  Din_Go(1202,2048);if (!deleteStorage && !isTrashFolder)
  {
    Din_Go(1180,2048);nsCOMPtr<nsIMsgFolder> trashFolder;
    rv = GetTrashFolder(getter_AddRefs(trashFolder));
    Din_Go(1183,2048);if (NS_SUCCEEDED(rv))
    {
      Din_Go(1181,2048);nsCOMPtr<nsIMsgCopyService> copyService = do_GetService(NS_MSGCOPYSERVICE_CONTRACTID, &rv);
      NS_ENSURE_SUCCESS(rv, rv);
      {__IAENUM__ nsresult  ReplaceReturn95 = copyService->CopyMessages(this, messages, trashFolder,
                                       true, listener, msgWindow, allowUndo); Din_Go(1182,2048); return ReplaceReturn95;}
    }
  }
  else
  {
    Din_Go(1184,2048);nsCOMPtr <nsIMsgDatabase> msgDB;
    rv = GetDatabaseWOReparse(getter_AddRefs(msgDB));
    Din_Go(1201,2048);if (NS_SUCCEEDED(rv))
    {
      Din_Go(1185,2048);if (deleteStorage && isMove && GetDeleteFromServerOnMove())
        {/*131*/Din_Go(1186,2048);MarkMsgsOnPop3Server(messages, POP3_DELETE);/*132*/}

      Din_Go(1187,2048);nsCOMPtr<nsISupports> msgSupport;
      rv = EnableNotifications(allMessageCountNotifications, false, true /*dbBatching*/);
      Din_Go(1195,2048);if (NS_SUCCEEDED(rv))
      {
        Din_Go(1188,2048);nsCOMPtr<nsIMsgPluggableStore> msgStore;
        rv = GetMsgStore(getter_AddRefs(msgStore));
        Din_Go(1192,2048);if (NS_SUCCEEDED(rv))
        {
          Din_Go(1189,2048);rv = msgStore->DeleteMessages(messages);
          nsCOMPtr<nsIMsgDBHdr> msgDBHdr;
          Din_Go(1191,2048);for (uint32_t i = 0; i < messageCount; ++i)
          {
            Din_Go(1190,2048);msgDBHdr = do_QueryElementAt(messages, i, &rv);
            rv = msgDB->DeleteHeader(msgDBHdr, nullptr, false, true);
          }
        }
      }
      else {/*133*/Din_Go(1193,2048);if (rv == NS_MSG_FOLDER_BUSY)
        {/*135*/Din_Go(1194,2048);ThrowAlertMsg("deletingMsgsFailed", msgWindow);/*136*/}/*134*/}

      // we are the source folder here for a move or shift delete
      //enable notifications because that will close the file stream
      // we've been caching, mark the db as valid, and commit it.
      Din_Go(1196,2048);EnableNotifications(allMessageCountNotifications, true, true /*dbBatching*/);
      Din_Go(1198,2048);if (!isMove)
        {/*137*/Din_Go(1197,2048);NotifyFolderEvent(NS_SUCCEEDED(rv) ? mDeleteOrMoveMsgCompletedAtom : mDeleteOrMoveMsgFailedAtom);/*138*/}
      Din_Go(1200,2048);if (msgWindow && !isMove)
        {/*139*/Din_Go(1199,2048);AutoCompact(msgWindow);/*140*/}
    }
  }

  Din_Go(1206,2048);if (msgWindow && !isMove && (deleteStorage || isTrashFolder)) {
    // Clear undo and redo stack.
    Din_Go(1203,2048);nsCOMPtr<nsITransactionManager> txnMgr;
    msgWindow->GetTransactionManager(getter_AddRefs(txnMgr));
    Din_Go(1205,2048);if (txnMgr) {
      Din_Go(1204,2048);txnMgr->Clear();
    }
  }
  {__IAENUM__ nsresult  ReplaceReturn94 = rv; Din_Go(1207,2048); return ReplaceReturn94;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::AddMessageDispositionState(nsIMsgDBHdr *aMessage, nsMsgDispositionState aDispositionFlag)
{
  Din_Go(1208,2048);nsMsgMessageFlagType msgFlag = 0;
  Din_Go(1214,2048);switch (aDispositionFlag) {
    case nsIMsgFolder::nsMsgDispositionState_Replied:
      Din_Go(1209,2048);msgFlag = nsMsgMessageFlags::Replied;
      Din_Go(1210,2048);break;
    case nsIMsgFolder::nsMsgDispositionState_Forwarded:
      Din_Go(1211,2048);msgFlag = nsMsgMessageFlags::Forwarded;
      Din_Go(1212,2048);break;
    default:
      {__IAENUM__ nsresult  ReplaceReturn93 = NS_ERROR_UNEXPECTED; Din_Go(1213,2048); return ReplaceReturn93;}
  }

  Din_Go(1215,2048);nsresult rv = nsMsgDBFolder::AddMessageDispositionState(aMessage, aDispositionFlag);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIMsgPluggableStore> msgStore;
  rv = GetMsgStore(getter_AddRefs(msgStore));
  NS_ENSURE_SUCCESS(rv, rv);
  nsCOMPtr<nsIMutableArray> messages(do_CreateInstance(NS_ARRAY_CONTRACTID, &rv));
  NS_ENSURE_SUCCESS(rv, rv);
  messages->AppendElement(aMessage, false);
  {__IAENUM__ nsresult  ReplaceReturn92 = msgStore->ChangeFlags(messages, msgFlag, true); Din_Go(1216,2048); return ReplaceReturn92;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::MarkMessagesRead(nsIArray *aMessages, bool aMarkRead)
{
  Din_Go(1217,2048);nsresult rv = nsMsgDBFolder::MarkMessagesRead(aMessages, aMarkRead);
  NS_ENSURE_SUCCESS(rv, rv);
  nsCOMPtr<nsIMsgPluggableStore> msgStore;
  rv = GetMsgStore(getter_AddRefs(msgStore));
  NS_ENSURE_SUCCESS(rv, rv);
  {__IAENUM__ nsresult  ReplaceReturn91 = msgStore->ChangeFlags(aMessages, nsMsgMessageFlags::Read, aMarkRead); Din_Go(1218,2048); return ReplaceReturn91;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::MarkMessagesFlagged(nsIArray *aMessages,
                                          bool aMarkFlagged)
{
  Din_Go(1219,2048);nsresult rv = nsMsgDBFolder::MarkMessagesFlagged(aMessages, aMarkFlagged);
  NS_ENSURE_SUCCESS(rv, rv);
  nsCOMPtr<nsIMsgPluggableStore> msgStore;
  rv = GetMsgStore(getter_AddRefs(msgStore));
  NS_ENSURE_SUCCESS(rv, rv);
  {__IAENUM__ nsresult  ReplaceReturn90 = msgStore->ChangeFlags(aMessages, nsMsgMessageFlags::Marked,
                               aMarkFlagged); Din_Go(1220,2048); return ReplaceReturn90;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::MarkAllMessagesRead(nsIMsgWindow *aMsgWindow)
{
  Din_Go(1221,2048);nsresult rv = GetDatabase();
  NS_ENSURE_SUCCESS(rv, rv);

  nsMsgKey *thoseMarked = nullptr;
  uint32_t numMarked = 0;
  EnableNotifications(allMessageCountNotifications, false, true /*dbBatching*/);
  rv = mDatabase->MarkAllRead(&numMarked, &thoseMarked);
  EnableNotifications(allMessageCountNotifications, true, true /*dbBatching*/);
  Din_Go(1223,2048);if (NS_FAILED(rv) || !numMarked || !thoseMarked)
    {/*141*/{__IAENUM__ nsresult  ReplaceReturn89 = rv; Din_Go(1222,2048); return ReplaceReturn89;}/*142*/}

  Din_Go(1236,2048);do {
    Din_Go(1224,2048);nsCOMPtr<nsIMutableArray> messages;
    rv = MsgGetHdrsFromKeys(mDatabase, thoseMarked, numMarked, getter_AddRefs(messages));
    Din_Go(1226,2048);if (NS_FAILED(rv))
      {/*143*/Din_Go(1225,2048);break;/*144*/}

    Din_Go(1227,2048);nsCOMPtr<nsIMsgPluggableStore> msgStore;
    rv = GetMsgStore(getter_AddRefs(msgStore));
    Din_Go(1229,2048);if (NS_FAILED(rv))
      {/*145*/Din_Go(1228,2048);break;/*146*/}

    Din_Go(1230,2048);rv = msgStore->ChangeFlags(messages, nsMsgMessageFlags::Read, true);
    Din_Go(1232,2048);if (NS_FAILED(rv))
      {/*147*/Din_Go(1231,2048);break;/*148*/}

    Din_Go(1233,2048);mDatabase->Commit(nsMsgDBCommitType::kLargeCommit);

    // Setup a undo-state
    Din_Go(1235,2048);if (aMsgWindow)
      {/*149*/Din_Go(1234,2048);rv = AddMarkAllReadUndoAction(aMsgWindow, thoseMarked, numMarked);/*150*/}
  } while (false);

  Din_Go(1237,2048);free(thoseMarked);
  {__IAENUM__ nsresult  ReplaceReturn88 = rv; Din_Go(1238,2048); return ReplaceReturn88;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::MarkThreadRead(nsIMsgThread *thread)
{
  Din_Go(1239,2048);nsresult rv = GetDatabase();
  NS_ENSURE_SUCCESS(rv, rv);

  nsMsgKey *thoseMarked = nullptr;
  uint32_t numMarked = 0;
  rv = mDatabase->MarkThreadRead(thread, nullptr, &numMarked, &thoseMarked);
  Din_Go(1241,2048);if (NS_FAILED(rv) || !numMarked || !thoseMarked)
    {/*151*/{__IAENUM__ nsresult  ReplaceReturn87 = rv; Din_Go(1240,2048); return ReplaceReturn87;}/*152*/}

  Din_Go(1252,2048);do {
    Din_Go(1242,2048);nsCOMPtr<nsIMutableArray> messages;
    rv = MsgGetHdrsFromKeys(mDatabase, thoseMarked, numMarked, getter_AddRefs(messages));
    Din_Go(1244,2048);if (NS_FAILED(rv))
      {/*153*/Din_Go(1243,2048);break;/*154*/}

    Din_Go(1245,2048);nsCOMPtr<nsIMsgPluggableStore> msgStore;
    rv = GetMsgStore(getter_AddRefs(msgStore));
    Din_Go(1247,2048);if (NS_FAILED(rv))
      {/*155*/Din_Go(1246,2048);break;/*156*/}

    Din_Go(1248,2048);rv = msgStore->ChangeFlags(messages, nsMsgMessageFlags::Read, true);
    Din_Go(1250,2048);if (NS_FAILED(rv))
      {/*157*/Din_Go(1249,2048);break;/*158*/}

    Din_Go(1251,2048);mDatabase->Commit(nsMsgDBCommitType::kLargeCommit);
  } while (false);

  Din_Go(1253,2048);free(thoseMarked);
  {__IAENUM__ nsresult  ReplaceReturn86 = rv; Din_Go(1254,2048); return ReplaceReturn86;}
}

nsresult
nsMsgLocalMailFolder::InitCopyState(nsISupports* aSupport,
                                    nsIArray* messages,
                                    bool isMove,
                                    nsIMsgCopyServiceListener* listener,
                                    nsIMsgWindow *msgWindow, bool isFolder,
                                    bool allowUndo)
{
  Din_Go(1255,2048);nsCOMPtr<nsIFile> path;

  NS_ASSERTION(!mCopyState, "already copying a msg into this folder");
  Din_Go(1257,2048);if (mCopyState)
    {/*159*/{__IAENUM__ nsresult  ReplaceReturn85 = NS_ERROR_FAILURE; Din_Go(1256,2048); return ReplaceReturn85;}/*160*/} // already has a  copy in progress

  // get mDatabase set, so we can use it to add new hdrs to this db.
  // calling GetDatabase will set mDatabase - we use the comptr
  // here to avoid doubling the refcnt on mDatabase. We don't care if this
  // fails - we just want to give it a chance. It will definitely fail in
  // nsLocalMailFolder::EndCopy because we will have written data to the folder
  // and changed its size.
  Din_Go(1258,2048);nsCOMPtr <nsIMsgDatabase> msgDB;
  GetDatabaseWOReparse(getter_AddRefs(msgDB));
  bool isLocked;

  GetLocked(&isLocked);
  Din_Go(1259,2048);if (isLocked)
    return NS_MSG_FOLDER_BUSY;

  Din_Go(1260,2048);AcquireSemaphore(static_cast<nsIMsgLocalMailFolder*>(this));

  mCopyState = new nsLocalMailCopyState();
  NS_ENSURE_TRUE(mCopyState, NS_ERROR_OUT_OF_MEMORY);

  mCopyState->m_dataBuffer = (char*) PR_CALLOC(COPY_BUFFER_SIZE+1);
  NS_ENSURE_TRUE(mCopyState->m_dataBuffer, NS_ERROR_OUT_OF_MEMORY);

  mCopyState->m_dataBufferSize = COPY_BUFFER_SIZE;
  mCopyState->m_destDB = msgDB;

  //Before we continue we should verify that there is enough diskspace.
  //XXX How do we do this?
  nsresult rv;
  mCopyState->m_srcSupport = do_QueryInterface(aSupport, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  mCopyState->m_messages = messages;
  mCopyState->m_curCopyIndex = 0;
  mCopyState->m_isMove = isMove;
  mCopyState->m_isFolder = isFolder;
  mCopyState->m_allowUndo = allowUndo;
  mCopyState->m_msgWindow = msgWindow;
  rv = messages->GetLength(&mCopyState->m_totalMsgCount);
  Din_Go(1262,2048);if (listener)
    {/*161*/Din_Go(1261,2048);mCopyState->m_listener = do_QueryInterface(listener, &rv);/*162*/}
  Din_Go(1263,2048);mCopyState->m_copyingMultipleMessages = false;
  mCopyState->m_wholeMsgInStream = false;

  // If we have source messages then we need destination messages too.
  Din_Go(1265,2048);if (messages)
    {/*163*/Din_Go(1264,2048);mCopyState->m_destMessages = do_CreateInstance(NS_ARRAY_CONTRACTID);/*164*/}

  {__IAENUM__ nsresult  ReplaceReturn84 = rv; Din_Go(1266,2048); return ReplaceReturn84;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::OnAnnouncerGoingAway(nsIDBChangeAnnouncer *instigator)
{
  Din_Go(1267,2048);if (mCopyState)
    {/*165*/Din_Go(1268,2048);mCopyState->m_destDB = nullptr;/*166*/}
  {__IAENUM__ nsresult  ReplaceReturn83 = nsMsgDBFolder::OnAnnouncerGoingAway(instigator); Din_Go(1269,2048); return ReplaceReturn83;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::OnCopyCompleted(nsISupports *srcSupport, bool moveCopySucceeded)
{
  Din_Go(1270,2048);if (mCopyState && mCopyState->m_notifyFolderLoaded)
    {/*167*/Din_Go(1271,2048);NotifyFolderEvent(mFolderLoadedAtom);/*168*/}

  Din_Go(1272,2048);(void) RefreshSizeOnDisk();
  // we are the destination folder for a move/copy
  bool haveSemaphore;
  nsresult rv = TestSemaphore(static_cast<nsIMsgLocalMailFolder*>(this), &haveSemaphore);
  Din_Go(1274,2048);if (NS_SUCCEEDED(rv) && haveSemaphore)
    {/*169*/Din_Go(1273,2048);ReleaseSemaphore(static_cast<nsIMsgLocalMailFolder*>(this));/*170*/}

  Din_Go(1276,2048);if (mCopyState && !mCopyState->m_newMsgKeywords.IsEmpty() &&
      mCopyState->m_newHdr)
  {
    Din_Go(1275,2048);nsCOMPtr<nsIMutableArray> messageArray(do_CreateInstance(NS_ARRAY_CONTRACTID, &rv));
    NS_ENSURE_TRUE(messageArray, rv);
    messageArray->AppendElement(mCopyState->m_newHdr, false);
    AddKeywordsToMessages(messageArray, mCopyState->m_newMsgKeywords);
  }
  Din_Go(1278,2048);if (moveCopySucceeded && mDatabase)
  {
    Din_Go(1277,2048);mDatabase->SetSummaryValid(true);
    (void) CloseDBIfFolderNotOpen();
  }

  Din_Go(1279,2048);delete mCopyState;
  mCopyState = nullptr;
  nsCOMPtr<nsIMsgCopyService> copyService = do_GetService(NS_MSGCOPYSERVICE_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  {__IAENUM__ nsresult  ReplaceReturn82 = copyService->NotifyCompletion(srcSupport, this, moveCopySucceeded ? NS_OK : NS_ERROR_FAILURE); Din_Go(1280,2048); return ReplaceReturn82;}
}

bool nsMsgLocalMailFolder::CheckIfSpaceForCopy(nsIMsgWindow *msgWindow,
                                                 nsIMsgFolder *srcFolder,
                                                 nsISupports *srcSupports,
                                                 bool isMove,
                                                 int64_t totalMsgSize)
{
  Din_Go(1281,2048);bool spaceNotAvailable = true;
  nsresult rv = WarnIfLocalFileTooBig(msgWindow, totalMsgSize, &spaceNotAvailable);
  Din_Go(1286,2048);if (NS_FAILED(rv) || spaceNotAvailable)
  {
    Din_Go(1282,2048);if (isMove && srcFolder)
      {/*171*/Din_Go(1283,2048);srcFolder->NotifyFolderEvent(mDeleteOrMoveMsgFailedAtom);/*172*/}
    Din_Go(1284,2048);OnCopyCompleted(srcSupports, false);
    {_Bool  ReplaceReturn81 = false; Din_Go(1285,2048); return ReplaceReturn81;}
  }
  {_Bool  ReplaceReturn80 = true; Din_Go(1287,2048); return ReplaceReturn80;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::CopyMessages(nsIMsgFolder* srcFolder, nsIArray*
                                   messages, bool isMove,
                                   nsIMsgWindow *msgWindow,
                                   nsIMsgCopyServiceListener* listener,
                                   bool isFolder, bool allowUndo)
{
  Din_Go(1288,2048);nsCOMPtr<nsISupports> srcSupport = do_QueryInterface(srcFolder);
  bool isServer;
  nsresult rv = GetIsServer(&isServer);
  Din_Go(1292,2048);if (NS_SUCCEEDED(rv) && isServer)
  {
    NS_ERROR("Destination is the root folder. Cannot move/copy here");
    Din_Go(1290,2048);if (isMove)
      {/*173*/Din_Go(1289,2048);srcFolder->NotifyFolderEvent(mDeleteOrMoveMsgFailedAtom);/*174*/}
    {__IAENUM__ nsresult  ReplaceReturn79 = OnCopyCompleted(srcSupport, false); Din_Go(1291,2048); return ReplaceReturn79;}
  }

  Din_Go(1293,2048);UpdateTimestamps(allowUndo);
  nsCString protocolType;
  rv = srcFolder->GetURI(protocolType);
  protocolType.SetLength(protocolType.FindChar(':'));

  bool needOfflineBody = (WeAreOffline() &&
    (MsgLowerCaseEqualsLiteral(protocolType, "imap") ||
     MsgLowerCaseEqualsLiteral(protocolType, "news")));
  int64_t totalMsgSize = 0;
  uint32_t numMessages = 0;
  messages->GetLength(&numMessages);
  Din_Go(1304,2048);for (uint32_t i = 0; i < numMessages; i++)
  {
    Din_Go(1294,2048);nsCOMPtr<nsIMsgDBHdr> message(do_QueryElementAt(messages, i, &rv));
    Din_Go(1303,2048);if (NS_SUCCEEDED(rv) && message)
    {
      Din_Go(1295,2048);nsMsgKey key;
      uint32_t msgSize;
      message->GetMessageSize(&msgSize);

      /* 200 is a per-message overhead to account for any extra data added
         to the message.
      */
      totalMsgSize += msgSize + 200;

      Din_Go(1302,2048);if (needOfflineBody)
      {
        Din_Go(1296,2048);bool hasMsgOffline = false;
        message->GetMessageKey(&key);
        srcFolder->HasMsgOffline(key, &hasMsgOffline);
        Din_Go(1301,2048);if (!hasMsgOffline)
        {
          Din_Go(1297,2048);if (isMove)
            {/*175*/Din_Go(1298,2048);srcFolder->NotifyFolderEvent(mDeleteOrMoveMsgFailedAtom);/*176*/}
          Din_Go(1299,2048);ThrowAlertMsg("cantMoveMsgWOBodyOffline", msgWindow);
          {__IAENUM__ nsresult  ReplaceReturn78 = OnCopyCompleted(srcSupport, false); Din_Go(1300,2048); return ReplaceReturn78;}
        }
      }
    }
  }

  Din_Go(1306,2048);if (!CheckIfSpaceForCopy(msgWindow, srcFolder, srcSupport, isMove,
                           totalMsgSize))
    {/*177*/{__IAENUM__ nsresult  ReplaceReturn77 = NS_OK; Din_Go(1305,2048); return ReplaceReturn77;}/*178*/}

  NS_ENSURE_SUCCESS(rv, rv);
  Din_Go(1307,2048);bool storeDidCopy = false;
  nsCOMPtr<nsIMsgPluggableStore> msgStore;
  rv = GetMsgStore(getter_AddRefs(msgStore));
  NS_ENSURE_SUCCESS(rv, rv);
  nsCOMPtr<nsITransaction> undoTxn;
  rv = msgStore->CopyMessages(isMove, messages, this, listener,
                              getter_AddRefs(undoTxn), &storeDidCopy);
  Din_Go(1315,2048);if (storeDidCopy)
  {
    NS_ASSERTION(undoTxn, "if store does copy, it needs to add undo action");
    Din_Go(1311,2048);if (msgWindow && undoTxn)
    {
      Din_Go(1308,2048);nsCOMPtr<nsITransactionManager> txnMgr;
      msgWindow->GetTransactionManager(getter_AddRefs(txnMgr));
      Din_Go(1310,2048);if (txnMgr)
        {/*179*/Din_Go(1309,2048);txnMgr->DoTransaction(undoTxn);/*180*/}
    }
    Din_Go(1313,2048);if (isMove)
      {/*181*/Din_Go(1312,2048);srcFolder->NotifyFolderEvent(NS_SUCCEEDED(rv) ? mDeleteOrMoveMsgCompletedAtom :
                                                      mDeleteOrMoveMsgFailedAtom);/*182*/}

    {__IAENUM__ nsresult  ReplaceReturn76 = rv; Din_Go(1314,2048); return ReplaceReturn76;}
  }
  // If the store doesn't do the copy, we'll stream the source messages into
  // the target folder, using getMsgInputStream and getNewMsgOutputStream.

  // don't update the counts in the dest folder until it is all over
  Din_Go(1316,2048);EnableNotifications(allMessageCountNotifications, false, false /*dbBatching*/);  //dest folder doesn't need db batching

  // sort the message array by key
  uint32_t numMsgs = 0;
  messages->GetLength(&numMsgs);
  nsTArray<nsMsgKey> keyArray(numMsgs);
  Din_Go(1323,2048);if (numMsgs > 1)
  {
    Din_Go(1317,2048);for (uint32_t i = 0; i < numMsgs; i++)
    {
      Din_Go(1318,2048);nsCOMPtr<nsIMsgDBHdr> aMessage = do_QueryElementAt(messages, i, &rv);
      Din_Go(1320,2048);if (NS_SUCCEEDED(rv) && aMessage)
      {
        Din_Go(1319,2048);nsMsgKey key;
        aMessage->GetMessageKey(&key);
        keyArray.AppendElement(key);
      }
    }

    Din_Go(1321,2048);keyArray.Sort();

    nsCOMPtr<nsIMutableArray> sortedMsgs(do_CreateInstance(NS_ARRAY_CONTRACTID));
    rv = MessagesInKeyOrder(keyArray, srcFolder, sortedMsgs);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = InitCopyState(srcSupport, sortedMsgs, isMove, listener, msgWindow, isFolder, allowUndo);
  }
  else
    {/*183*/Din_Go(1322,2048);rv = InitCopyState(srcSupport, messages, isMove, listener, msgWindow, isFolder, allowUndo);/*184*/}

  Din_Go(1326,2048);if (NS_FAILED(rv))
  {
    Din_Go(1324,2048);ThrowAlertMsg("operationFailedFolderBusy", msgWindow);
    (void) OnCopyCompleted(srcSupport, false);
    {__IAENUM__ nsresult  ReplaceReturn75 = rv; Din_Go(1325,2048); return ReplaceReturn75;}
  }

  Din_Go(1332,2048);if (!MsgLowerCaseEqualsLiteral(protocolType, "mailbox"))
  {
    Din_Go(1327,2048);mCopyState->m_dummyEnvelopeNeeded = true;
    nsParseMailMessageState* parseMsgState = new nsParseMailMessageState();
    Din_Go(1331,2048);if (parseMsgState)
    {
      Din_Go(1328,2048);nsCOMPtr<nsIMsgDatabase> msgDb;
      mCopyState->m_parseMsgState = parseMsgState;
      GetDatabaseWOReparse(getter_AddRefs(msgDb));
      Din_Go(1330,2048);if (msgDb)
        {/*185*/Din_Go(1329,2048);parseMsgState->SetMailDB(msgDb);/*186*/}
    }
  }

  // undo stuff
  Din_Go(1342,2048);if (allowUndo)    //no undo for folder move/copy or or move/copy from search window
  {
    Din_Go(1333,2048);RefPtr<nsLocalMoveCopyMsgTxn> msgTxn = new nsLocalMoveCopyMsgTxn;
    Din_Go(1341,2048);if (msgTxn && NS_SUCCEEDED(msgTxn->Init(srcFolder, this, isMove)))
    {
      Din_Go(1334,2048);msgTxn->SetMsgWindow(msgWindow);
      Din_Go(1339,2048);if (isMove)
      {
        Din_Go(1335,2048);if (mFlags & nsMsgFolderFlags::Trash)
          {/*187*/Din_Go(1336,2048);msgTxn->SetTransactionType(nsIMessenger::eDeleteMsg);/*188*/}
        else
          {/*189*/Din_Go(1337,2048);msgTxn->SetTransactionType(nsIMessenger::eMoveMsg);/*190*/}
      }
      else
        {/*191*/Din_Go(1338,2048);msgTxn->SetTransactionType(nsIMessenger::eCopyMsg);/*192*/}
      Din_Go(1340,2048);msgTxn.swap(mCopyState->m_undoMsgTxn);
    }
  }

  Din_Go(1351,2048);if (numMsgs > 1 && ((MsgLowerCaseEqualsLiteral(protocolType, "imap") && !WeAreOffline()) ||
                      MsgLowerCaseEqualsLiteral(protocolType, "mailbox")))
  {
    Din_Go(1343,2048);mCopyState->m_copyingMultipleMessages = true;
    rv = CopyMessagesTo(mCopyState->m_messages, keyArray, msgWindow, this, isMove);
    Din_Go(1345,2048);if (NS_FAILED(rv))
    {
      NS_ERROR("copy message failed");
      Din_Go(1344,2048);(void) OnCopyCompleted(srcSupport, false);
    }
  }
  else
  {
    Din_Go(1346,2048);nsCOMPtr<nsISupports> msgSupport = do_QueryElementAt(mCopyState->m_messages, 0);
    Din_Go(1350,2048);if (msgSupport)
    {
      Din_Go(1347,2048);rv = CopyMessageTo(msgSupport, this, msgWindow, isMove);
      Din_Go(1349,2048);if (NS_FAILED(rv))
      {
        NS_ASSERTION(false, "copy message failed");
        Din_Go(1348,2048);(void) OnCopyCompleted(srcSupport, false);
      }
    }
  }
  // if this failed immediately, need to turn back on notifications and inform FE.
  Din_Go(1355,2048);if (NS_FAILED(rv))
  {
    Din_Go(1352,2048);if (isMove)
      {/*193*/Din_Go(1353,2048);srcFolder->NotifyFolderEvent(mDeleteOrMoveMsgFailedAtom);/*194*/}
    Din_Go(1354,2048);EnableNotifications(allMessageCountNotifications, true, false /*dbBatching*/);  //dest folder doesn't need db batching
  }
  {__IAENUM__ nsresult  ReplaceReturn74 = rv; Din_Go(1356,2048); return ReplaceReturn74;}
}
// for srcFolder that are on different server than the dstFolder.
// "this" is the parent of the new dest folder.
nsresult
nsMsgLocalMailFolder::CopyFolderAcrossServer(nsIMsgFolder* srcFolder, nsIMsgWindow *msgWindow,
                  nsIMsgCopyServiceListener *listener )
{
  Din_Go(1357,2048);mInitialized = true;

  nsString folderName;
  srcFolder->GetName(folderName);

  nsCOMPtr<nsIMsgFolder> newMsgFolder;
  nsresult rv = CreateSubfolderInternal(folderName, msgWindow, getter_AddRefs(newMsgFolder));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsISimpleEnumerator> messages;
  rv = srcFolder->GetMessages(getter_AddRefs(messages));

  nsCOMPtr<nsIMutableArray> msgArray(do_CreateInstance(NS_ARRAY_CONTRACTID));

  bool hasMoreElements;
  nsCOMPtr<nsISupports> aSupport;

  Din_Go(1359,2048);if (messages)
    {/*195*/Din_Go(1358,2048);messages->HasMoreElements(&hasMoreElements);/*196*/}

  Din_Go(1361,2048);while (hasMoreElements && NS_SUCCEEDED(rv))
  {
    Din_Go(1360,2048);rv = messages->GetNext(getter_AddRefs(aSupport));
    rv = msgArray->AppendElement(aSupport, false);
    messages->HasMoreElements(&hasMoreElements);
  }

  Din_Go(1362,2048);uint32_t numMsgs=0;
  msgArray->GetLength(&numMsgs);

  Din_Go(1368,2048);if (numMsgs > 0 )   //if only srcFolder has messages..
    {/*197*/Din_Go(1363,2048);newMsgFolder->CopyMessages(srcFolder, msgArray, false, msgWindow, listener, true /* is folder*/, false /* allowUndo */);/*198*/}
  else
  {
    Din_Go(1364,2048);nsCOMPtr <nsIMsgLocalMailFolder> localFolder = do_QueryInterface(newMsgFolder);
    Din_Go(1367,2048);if (localFolder)
    {
      // normally these would get called from ::EndCopy when the last message
      // was finished copying. But since there are no messages, we have to call
      // them explicitly.
      Din_Go(1365,2048);nsCOMPtr<nsISupports> srcSupports = do_QueryInterface(newMsgFolder);
      localFolder->CopyAllSubFolders(srcFolder, msgWindow, listener);
      {__IAENUM__ nsresult  ReplaceReturn73 = localFolder->OnCopyCompleted(srcSupports, true); Din_Go(1366,2048); return ReplaceReturn73;}
    }
  }
  {__IAENUM__ nsresult  ReplaceReturn72 = NS_OK; Din_Go(1369,2048); return ReplaceReturn72;}  // otherwise the front-end will say Exception::CopyFolder
}

nsresult    //copy the sub folders
nsMsgLocalMailFolder::CopyAllSubFolders(nsIMsgFolder *srcFolder,
                                      nsIMsgWindow *msgWindow,
                                      nsIMsgCopyServiceListener *listener )
{
  Din_Go(1370,2048);nsCOMPtr<nsISimpleEnumerator> enumerator;
  nsresult rv = srcFolder->GetSubFolders(getter_AddRefs(enumerator));
  NS_ENSURE_SUCCESS(rv, rv);

  bool hasMore;
  Din_Go(1374,2048);while (NS_SUCCEEDED(enumerator->HasMoreElements(&hasMore)) && hasMore)
  {
    Din_Go(1371,2048);nsCOMPtr<nsISupports> item;
    enumerator->GetNext(getter_AddRefs(item));

    nsCOMPtr<nsIMsgFolder> folder(do_QueryInterface(item));
    Din_Go(1373,2048);if (folder)
      {/*199*/Din_Go(1372,2048);CopyFolderAcrossServer(folder, msgWindow, listener);/*200*/}
  }
  {__IAENUM__ nsresult  ReplaceReturn71 = rv; Din_Go(1375,2048); return ReplaceReturn71;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::CopyFolder( nsIMsgFolder* srcFolder, bool isMoveFolder,
                                   nsIMsgWindow *msgWindow,
                                   nsIMsgCopyServiceListener* listener)
{
Din_Go(1376,2048);  NS_ENSURE_ARG_POINTER(srcFolder);
  // isMoveFolder == true when "this" and srcFolder are on same server
  {__IAENUM__ nsresult  ReplaceReturn70 = isMoveFolder ? CopyFolderLocal(srcFolder, isMoveFolder, msgWindow, listener ) :
                      CopyFolderAcrossServer(srcFolder, msgWindow, listener ); Din_Go(1377,2048); return ReplaceReturn70;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::CopyFolderLocal(nsIMsgFolder *srcFolder,
                                      bool isMoveFolder,
                                      nsIMsgWindow *msgWindow,
                                      nsIMsgCopyServiceListener *aListener)
{
  Din_Go(1378,2048);mInitialized = true;
  bool isChildOfTrash;
  nsresult rv = IsChildOfTrash(&isChildOfTrash);
  Din_Go(1388,2048);if (NS_SUCCEEDED(rv) && isChildOfTrash)
  {
    // do it just for the parent folder (isMoveFolder is true for parent only) if we are deleting/moving a folder tree
    // don't confirm for rss folders.
    Din_Go(1379,2048);if (isMoveFolder)
    {
      // if there's a msgWindow, confirm the deletion
      Din_Go(1380,2048);if (msgWindow) 
      {

        Din_Go(1381,2048);bool okToDelete = false;
        ConfirmFolderDeletion(msgWindow, srcFolder, &okToDelete);
        Din_Go(1382,2048);if (!okToDelete)
          return NS_MSG_ERROR_COPY_FOLDER_ABORTED;
      }
      // if we are moving a favorite folder to trash, we should clear the favorites flag
      // so it gets removed from the view.
      Din_Go(1383,2048);srcFolder->ClearFlag(nsMsgFolderFlags::Favorite);
    }

    Din_Go(1384,2048);bool match = false;
    rv = srcFolder->MatchOrChangeFilterDestination(nullptr, false, &match);
    Din_Go(1387,2048);if (match && msgWindow)
    {
      Din_Go(1385,2048);bool confirmed = false;
      srcFolder->ConfirmFolderDeletionForFilter(msgWindow, &confirmed);
      Din_Go(1386,2048);if (!confirmed)
        return NS_MSG_ERROR_COPY_FOLDER_ABORTED;
    }
  }

  Din_Go(1389,2048);nsAutoString newFolderName;
  nsAutoString folderName;
  rv = srcFolder->GetName(folderName);
  Din_Go(1391,2048);if (NS_WARN_IF(NS_FAILED(rv))) {
    {__IAENUM__ nsresult  ReplaceReturn69 = rv; Din_Go(1390,2048); return ReplaceReturn69;}
  }

  Din_Go(1405,2048);if (!isMoveFolder) {
    Din_Go(1392,2048);rv = CheckIfFolderExists(folderName, this, msgWindow);
    Din_Go(1394,2048);if (NS_WARN_IF(NS_FAILED(rv))) {
      {__IAENUM__ nsresult  ReplaceReturn68 = rv; Din_Go(1393,2048); return ReplaceReturn68;}
    }
  }
  else
  {
    // If folder name already exists in destination, generate a new unique name.
    Din_Go(1395,2048);bool containsChild = true;
    uint32_t i;
    Din_Go(1402,2048);for (i = 1; containsChild; i++) {
      Din_Go(1396,2048);newFolderName.Assign(folderName);
      Din_Go(1398,2048);if (i > 1) {
        // This could be localizable but Toolkit is fine without it, see
        // mozilla/toolkit/content/contentAreaUtils.js::uniqueFile()
        Din_Go(1397,2048);newFolderName.Append('(');
        newFolderName.AppendInt(i);
        newFolderName.Append(')');
      }
      Din_Go(1399,2048);rv = ContainsChildNamed(newFolderName, &containsChild);
      Din_Go(1401,2048);if (NS_WARN_IF(NS_FAILED(rv))) {
        {__IAENUM__ nsresult  ReplaceReturn67 = rv; Din_Go(1400,2048); return ReplaceReturn67;}
      }
    }

    // 'i' is one more than the number of iterations done
    // and the number tacked onto the name of the folder.
    Din_Go(1404,2048);if (i > 2 && !isChildOfTrash) {
      // Folder name already exists, ask if rename is OK.
      // If moving to Trash, don't ask and do it.
      Din_Go(1403,2048);if (!ConfirmAutoFolderRename(msgWindow, folderName, newFolderName))
        return NS_MSG_ERROR_COPY_FOLDER_ABORTED;
    }
  }

  Din_Go(1406,2048);nsCOMPtr<nsIMsgPluggableStore> msgStore;
  rv = GetMsgStore(getter_AddRefs(msgStore));
  Din_Go(1408,2048);if (NS_WARN_IF(NS_FAILED(rv))) {
    {__IAENUM__ nsresult  ReplaceReturn66 = rv; Din_Go(1407,2048); return ReplaceReturn66;}
  }

  {__IAENUM__ nsresult  ReplaceReturn65 = msgStore->CopyFolder(srcFolder, this, isMoveFolder, msgWindow,
                              aListener, newFolderName); Din_Go(1409,2048); return ReplaceReturn65;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::CopyFileMessage(nsIFile* aFile, 
                                      nsIMsgDBHdr *msgToReplace,
                                      bool isDraftOrTemplate,
                                      uint32_t newMsgFlags,
                                      const nsACString &aNewMsgKeywords,
                                      nsIMsgWindow *msgWindow,
                                      nsIMsgCopyServiceListener* listener)
{
Din_Go(1410,2048);  NS_ENSURE_ARG_POINTER(aFile);
  Din_Go(1411,2048);nsresult rv = NS_ERROR_NULL_POINTER;
  nsParseMailMessageState* parseMsgState = nullptr;
  int64_t fileSize = 0;

  nsCOMPtr<nsISupports> fileSupport(do_QueryInterface(aFile, &rv));

  aFile->GetFileSize(&fileSize);
  Din_Go(1413,2048);if (!CheckIfSpaceForCopy(msgWindow, nullptr, fileSupport, false, fileSize))
    {/*201*/{__IAENUM__ nsresult  ReplaceReturn64 = NS_OK; Din_Go(1412,2048); return ReplaceReturn64;}/*202*/}

  Din_Go(1414,2048);nsCOMPtr<nsIMutableArray> messages(do_CreateInstance(NS_ARRAY_CONTRACTID));

  Din_Go(1416,2048);if (msgToReplace)
    {/*203*/Din_Go(1415,2048);messages->AppendElement(msgToReplace, false);/*204*/}

  Din_Go(1417,2048);rv = InitCopyState(fileSupport, messages, msgToReplace ? true : false,
                     listener, msgWindow, false, false);
  Din_Go(1445,2048);if (NS_SUCCEEDED(rv))
  {
    Din_Go(1418,2048);if (mCopyState)
      {/*205*/Din_Go(1419,2048);mCopyState->m_newMsgKeywords = aNewMsgKeywords;/*206*/}

    Din_Go(1420,2048);parseMsgState = new nsParseMailMessageState();
    NS_ENSURE_TRUE(parseMsgState, NS_ERROR_OUT_OF_MEMORY);
      nsCOMPtr<nsIMsgDatabase> msgDb;
      mCopyState->m_parseMsgState = parseMsgState;
      GetDatabaseWOReparse(getter_AddRefs(msgDb));
      Din_Go(1422,2048);if (msgDb)
        {/*207*/Din_Go(1421,2048);parseMsgState->SetMailDB(msgDb);/*208*/}

    Din_Go(1423,2048);nsCOMPtr<nsIInputStream> inputStream;
    rv = NS_NewLocalFileInputStream(getter_AddRefs(inputStream), aFile);

    // All or none for adding a message file to the store
    Din_Go(1425,2048);if (NS_SUCCEEDED(rv) && fileSize > PR_INT32_MAX)
        {/*209*/Din_Go(1424,2048);rv = NS_ERROR_ILLEGAL_VALUE;/*210*/} // may need error code for max msg size

    Din_Go(1433,2048);if (NS_SUCCEEDED(rv) && inputStream) 
    {
      Din_Go(1426,2048);char buffer[5];
      uint32_t readCount;
      rv = inputStream->Read(buffer, 5, &readCount);
      Din_Go(1432,2048);if (NS_SUCCEEDED(rv)) 
      {
        Din_Go(1427,2048);if (strncmp(buffer, "From ", 5))
          {/*211*/Din_Go(1428,2048);mCopyState->m_dummyEnvelopeNeeded = true;/*212*/}
        Din_Go(1429,2048);nsCOMPtr <nsISeekableStream> seekableStream = do_QueryInterface(inputStream, &rv);
        Din_Go(1431,2048);if (NS_SUCCEEDED(rv))
          {/*213*/Din_Go(1430,2048);seekableStream->Seek(nsISeekableStream::NS_SEEK_SET, 0);/*214*/}
      }
    }

     Din_Go(1434,2048);mCopyState->m_wholeMsgInStream = true;
     Din_Go(1436,2048);if (NS_SUCCEEDED(rv))
      {/*215*/Din_Go(1435,2048);rv = BeginCopy(nullptr);/*216*/}

    Din_Go(1438,2048);if (NS_SUCCEEDED(rv))
      {/*217*/Din_Go(1437,2048);rv = CopyData(inputStream, (int32_t) fileSize);/*218*/}

    Din_Go(1440,2048);if (NS_SUCCEEDED(rv))
      {/*219*/Din_Go(1439,2048);rv = EndCopy(true);/*220*/}

    //mDatabase should have been initialized above - if we got msgDb
    // If we were going to delete, here is where we would do it. But because
    // existing code already supports doing those deletes, we are just going
    // to end the copy.
    Din_Go(1442,2048);if (NS_SUCCEEDED(rv) && msgToReplace && mDatabase)
      {/*221*/Din_Go(1441,2048);rv = OnCopyCompleted(fileSupport, true);/*222*/}

    Din_Go(1444,2048);if (inputStream)
      {/*223*/Din_Go(1443,2048);inputStream->Close();/*224*/}
  }

  Din_Go(1447,2048);if (NS_FAILED(rv))
    {/*225*/Din_Go(1446,2048);(void) OnCopyCompleted(fileSupport, false);/*226*/}

  {__IAENUM__ nsresult  ReplaceReturn63 = rv; Din_Go(1448,2048); return ReplaceReturn63;}
}

nsresult nsMsgLocalMailFolder::DeleteMessage(nsISupports *message,
                                             nsIMsgWindow *msgWindow,
                                             bool deleteStorage, bool commit)
{
  Din_Go(1449,2048);nsresult rv = NS_OK;
  Din_Go(1455,2048);if (deleteStorage)
  {
    Din_Go(1450,2048);nsCOMPtr <nsIMsgDBHdr> msgDBHdr(do_QueryInterface(message, &rv));

    Din_Go(1454,2048);if (NS_SUCCEEDED(rv))
    {
      Din_Go(1451,2048);GetDatabase();
      Din_Go(1453,2048);if (mDatabase)
        {/*227*/Din_Go(1452,2048);rv = mDatabase->DeleteHeader(msgDBHdr, nullptr, commit, true);/*228*/}
    }
  }
  {__IAENUM__ nsresult  ReplaceReturn62 = rv; Din_Go(1456,2048); return ReplaceReturn62;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::GetNewMessages(nsIMsgWindow *aWindow, nsIUrlListener *aListener)
{
  Din_Go(1457,2048);nsCOMPtr<nsIMsgIncomingServer> server;
  nsresult rv = GetServer(getter_AddRefs(server));
  NS_ENSURE_SUCCESS(rv, NS_MSG_INVALID_OR_MISSING_SERVER);

  nsCOMPtr<nsILocalMailIncomingServer> localMailServer = do_QueryInterface(server, &rv);
  NS_ENSURE_SUCCESS(rv, NS_MSG_INVALID_OR_MISSING_SERVER);

  // XXX todo, move all this into nsILocalMailIncomingServer's GetNewMail
  // so that we don't have to have RSS foo here.
  nsCOMPtr<nsIRssIncomingServer> rssServer = do_QueryInterface(server, &rv);
  Din_Go(1459,2048);if (NS_SUCCEEDED(rv))
    {/*229*/{__IAENUM__ nsresult  ReplaceReturn61 = localMailServer->GetNewMail(aWindow, aListener, this, nullptr); Din_Go(1458,2048); return ReplaceReturn61;}/*230*/}

  Din_Go(1460,2048);nsCOMPtr<nsIMsgFolder> inbox;
  nsCOMPtr<nsIMsgFolder> rootFolder;
  rv = server->GetRootMsgFolder(getter_AddRefs(rootFolder));
  Din_Go(1462,2048);if (NS_SUCCEEDED(rv) && rootFolder)
  {
    Din_Go(1461,2048);rootFolder->GetFolderWithFlags(nsMsgFolderFlags::Inbox, getter_AddRefs(inbox));
  }
  Din_Go(1463,2048);nsCOMPtr<nsIMsgLocalMailFolder> localInbox = do_QueryInterface(inbox, &rv);
  Din_Go(1467,2048);if (NS_SUCCEEDED(rv))
  {
    Din_Go(1464,2048);bool valid = false;
    nsCOMPtr <nsIMsgDatabase> db;
    // this will kick off a reparse if the db is out of date.
    rv = localInbox->GetDatabaseWithReparse(nullptr, aWindow, getter_AddRefs(db));
    Din_Go(1466,2048);if (NS_SUCCEEDED(rv))
    {
      Din_Go(1465,2048);db->GetSummaryValid(&valid);
      rv = valid ? localMailServer->GetNewMail(aWindow, aListener, inbox, nullptr) :
                   localInbox->SetCheckForNewMessagesAfterParsing(true);
    }
  }
  {__IAENUM__ nsresult  ReplaceReturn60 = rv; Din_Go(1468,2048); return ReplaceReturn60;}
}

nsresult nsMsgLocalMailFolder::WriteStartOfNewMessage()
{
  Din_Go(1469,2048);nsCOMPtr <nsISeekableStream> seekableStream = do_QueryInterface(mCopyState->m_fileStream);
  int64_t filePos;
  seekableStream->Tell(&filePos);

  // CopyFileMessage() and CopyMessages() from servers other than pop3
  Din_Go(1473,2048);if (mCopyState->m_parseMsgState)
  {
    Din_Go(1470,2048);if (mCopyState->m_parseMsgState->m_newMsgHdr)
      {/*231*/Din_Go(1471,2048);mCopyState->m_parseMsgState->m_newMsgHdr->GetMessageKey(&mCopyState->m_curDstKey);/*232*/}
    Din_Go(1472,2048);mCopyState->m_parseMsgState->SetEnvelopePos(filePos);
    mCopyState->m_parseMsgState->SetState(nsIMsgParseMailMsgState::ParseHeadersState);
  }
  Din_Go(1492,2048);if (mCopyState->m_dummyEnvelopeNeeded)
  {
    Din_Go(1474,2048);nsCString result;
    nsAutoCString nowStr;
    MsgGenerateNowStr(nowStr);
    result.AppendLiteral("From - ");
    result.Append(nowStr);
    result.Append(MSG_LINEBREAK);

    // *** jt - hard code status line for now; come back later
    nsresult rv;
    nsCOMPtr <nsIMsgDBHdr> curSourceMessage = do_QueryElementAt(mCopyState->m_messages,
                                                                mCopyState->m_curCopyIndex, &rv);

    char statusStrBuf[50];
    Din_Go(1477,2048);if (curSourceMessage)
    {
      Din_Go(1475,2048);uint32_t dbFlags = 0;
      curSourceMessage->GetFlags(&dbFlags);

      // write out x-mozilla-status, but make sure we don't write out nsMsgMessageFlags::Offline
      PR_snprintf(statusStrBuf, sizeof(statusStrBuf), X_MOZILLA_STATUS_FORMAT MSG_LINEBREAK,
        dbFlags & ~(nsMsgMessageFlags::RuntimeOnly | nsMsgMessageFlags::Offline) & 0x0000FFFF);
    }
    else
      {/*233*/Din_Go(1476,2048);strcpy(statusStrBuf, "X-Mozilla-Status: 0001" MSG_LINEBREAK);/*234*/}
    Din_Go(1478,2048);uint32_t bytesWritten;
    mCopyState->m_fileStream->Write(result.get(), result.Length(), &bytesWritten);
    Din_Go(1480,2048);if (mCopyState->m_parseMsgState)
        {/*235*/Din_Go(1479,2048);mCopyState->m_parseMsgState->ParseAFolderLine(
          result.get(), result.Length());/*236*/}
    Din_Go(1481,2048);mCopyState->m_fileStream->Write(statusStrBuf, strlen(statusStrBuf), &bytesWritten);
    Din_Go(1483,2048);if (mCopyState->m_parseMsgState)
        {/*237*/Din_Go(1482,2048);mCopyState->m_parseMsgState->ParseAFolderLine(
        statusStrBuf, strlen(statusStrBuf));/*238*/}
    Din_Go(1484,2048);result = "X-Mozilla-Status2: 00000000" MSG_LINEBREAK;
    mCopyState->m_fileStream->Write(result.get(), result.Length(), &bytesWritten);
    Din_Go(1486,2048);if (mCopyState->m_parseMsgState)
        {/*239*/Din_Go(1485,2048);mCopyState->m_parseMsgState->ParseAFolderLine(
          result.get(), result.Length());/*240*/}
    Din_Go(1487,2048);result = X_MOZILLA_KEYWORDS;
    mCopyState->m_fileStream->Write(result.get(), result.Length(), &bytesWritten);
    Din_Go(1489,2048);if (mCopyState->m_parseMsgState)
        {/*241*/Din_Go(1488,2048);mCopyState->m_parseMsgState->ParseAFolderLine(
          result.get(), result.Length());/*242*/}
   Din_Go(1490,2048);mCopyState->m_fromLineSeen = true;
  }
  else
    {/*243*/Din_Go(1491,2048);mCopyState->m_fromLineSeen = false;/*244*/}

  Din_Go(1493,2048);mCopyState->m_curCopyIndex++;
  {__IAENUM__ nsresult  ReplaceReturn59 = NS_OK; Din_Go(1494,2048); return ReplaceReturn59;}
}

nsresult nsMsgLocalMailFolder::InitCopyMsgHdrAndFileStream()
{
  Din_Go(1495,2048);nsCOMPtr<nsIMsgPluggableStore> msgStore;
  nsresult rv = GetMsgStore(getter_AddRefs(mCopyState->m_msgStore));
  NS_ENSURE_SUCCESS(rv, rv);
  bool reusable;
  rv = mCopyState->m_msgStore->
         GetNewMsgOutputStream(this, getter_AddRefs(mCopyState->m_newHdr),
                               &reusable,
                               getter_AddRefs(mCopyState->m_fileStream));
  NS_ENSURE_SUCCESS(rv, rv);
  Din_Go(1497,2048);if (mCopyState->m_parseMsgState)
    {/*245*/Din_Go(1496,2048);mCopyState->m_parseMsgState->SetNewMsgHdr(mCopyState->m_newHdr);/*246*/}
  {__IAENUM__ nsresult  ReplaceReturn58 = rv; Din_Go(1498,2048); return ReplaceReturn58;}
}

//nsICopyMessageListener
NS_IMETHODIMP nsMsgLocalMailFolder::BeginCopy(nsIMsgDBHdr *message)
{
  Din_Go(1499,2048);if (!mCopyState)
    {/*247*/{__IAENUM__ nsresult  ReplaceReturn57 = NS_ERROR_NULL_POINTER; Din_Go(1500,2048); return ReplaceReturn57;}/*248*/}

  Din_Go(1501,2048);nsresult rv;
  Din_Go(1503,2048);if (!mCopyState->m_copyingMultipleMessages)
  {
    Din_Go(1502,2048);rv = InitCopyMsgHdrAndFileStream();
    NS_ENSURE_SUCCESS(rv, rv);
  }
  Din_Go(1504,2048);nsCOMPtr <nsISeekableStream> seekableStream = do_QueryInterface(mCopyState->m_fileStream, &rv);

  //  XXX ToDo: When copying multiple messages from a non-offline-enabled IMAP
  //  server, this fails. (The copy succeeds because the file stream is created
  //  subsequently in StartMessage) We should not be warning on an expected error.
  //  Perhaps there are unexpected consequences of returning early?
  NS_ENSURE_SUCCESS(rv, rv);
  seekableStream->Seek(nsISeekableStream::NS_SEEK_END, 0);

  int32_t messageIndex = (mCopyState->m_copyingMultipleMessages) ? mCopyState->m_curCopyIndex - 1 : mCopyState->m_curCopyIndex;
  NS_ASSERTION(!mCopyState->m_copyingMultipleMessages || messageIndex >= 0, "messageIndex invalid");
  // by the time we get here, m_curCopyIndex is 1 relative because WriteStartOfNewMessage increments it
  mCopyState->m_messages->QueryElementAt(messageIndex, NS_GET_IID(nsIMsgDBHdr),
                                  (void **)getter_AddRefs(mCopyState->m_message));
  // The flags of the source message can get changed when it is deleted, so
  // save them here.
  Din_Go(1506,2048);if (mCopyState->m_message)
    {/*249*/Din_Go(1505,2048);mCopyState->m_message->GetFlags(&(mCopyState->m_flags));/*250*/}
  Din_Go(1507,2048);DisplayMoveCopyStatusMsg();
  Din_Go(1509,2048);if (mCopyState->m_listener)
    {/*251*/Din_Go(1508,2048);mCopyState->m_listener->OnProgress(mCopyState->m_curCopyIndex, mCopyState->m_totalMsgCount);/*252*/}
  // if we're copying more than one message, StartMessage will handle this.
  {__IAENUM__ nsresult  ReplaceReturn56 = !mCopyState->m_copyingMultipleMessages ? WriteStartOfNewMessage() : rv; Din_Go(1510,2048); return ReplaceReturn56;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::CopyData(nsIInputStream *aIStream, int32_t aLength)
{
  //check to make sure we have control of the write.
  Din_Go(1511,2048);bool haveSemaphore;
  nsresult rv = NS_OK;

  rv = TestSemaphore(static_cast<nsIMsgLocalMailFolder*>(this), &haveSemaphore);
  Din_Go(1513,2048);if (NS_FAILED(rv))
    {/*253*/{__IAENUM__ nsresult  ReplaceReturn55 = rv; Din_Go(1512,2048); return ReplaceReturn55;}/*254*/}
  Din_Go(1514,2048);if (!haveSemaphore)
    return NS_MSG_FOLDER_BUSY;

  Din_Go(1516,2048);if (!mCopyState)
    {/*255*/{__IAENUM__ nsresult  ReplaceReturn54 = NS_ERROR_OUT_OF_MEMORY; Din_Go(1515,2048); return ReplaceReturn54;}/*256*/}

  Din_Go(1517,2048);uint32_t readCount;
  //allocate one extra byte for '\0' at the end and another extra byte at the
  //front to insert a '>' if we have a "From" line
  //allocate 2 more for crlf that may be needed for those without crlf at end of file
  Din_Go(1522,2048);if ( aLength + mCopyState->m_leftOver + 4 > mCopyState->m_dataBufferSize )
  {
    Din_Go(1518,2048);char *newBuffer = (char *) PR_REALLOC(mCopyState->m_dataBuffer, aLength + mCopyState->m_leftOver + 4);
    Din_Go(1520,2048);if (!newBuffer)
      {/*257*/{__IAENUM__ nsresult  ReplaceReturn53 = NS_ERROR_OUT_OF_MEMORY; Din_Go(1519,2048); return ReplaceReturn53;}/*258*/}

    Din_Go(1521,2048);mCopyState->m_dataBuffer = newBuffer;
    mCopyState->m_dataBufferSize = aLength + mCopyState->m_leftOver + 3;
  }

  Din_Go(1523,2048);nsCOMPtr <nsISeekableStream> seekableStream = do_QueryInterface(mCopyState->m_fileStream, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  seekableStream->Seek(nsISeekableStream::NS_SEEK_END, 0);

  rv = aIStream->Read(mCopyState->m_dataBuffer + mCopyState->m_leftOver + 1, aLength, &readCount);
  NS_ENSURE_SUCCESS(rv, rv);
  mCopyState->m_leftOver += readCount;
  mCopyState->m_dataBuffer[mCopyState->m_leftOver + 1] ='\0';
  char *start = mCopyState->m_dataBuffer + 1;
  char *endBuffer = mCopyState->m_dataBuffer + mCopyState->m_leftOver + 1;

  uint32_t lineLength;
  uint32_t bytesWritten;

  Din_Go(1548,2048);while (1)
  {
    Din_Go(1524,2048);char *end = PL_strnpbrk(start, "\r\n", endBuffer - start);
    Din_Go(1530,2048);if (!end)
    {
      Din_Go(1525,2048);mCopyState->m_leftOver -= (start - mCopyState->m_dataBuffer - 1);
      // In CopyFileMessage, a complete message is being copied in a single
      // call to CopyData, and if it does not have a LINEBREAK at the EOF,
      // then end will be null after reading the last line, and we need
      // to append the LINEBREAK to the buffer to enable transfer of the last line.
      Din_Go(1529,2048);if (mCopyState->m_wholeMsgInStream)
      {
        Din_Go(1526,2048);end = start + mCopyState->m_leftOver;
        memcpy (end, MSG_LINEBREAK + '\0', MSG_LINEBREAK_LEN + 1);
      }
      else
      {
        Din_Go(1527,2048);memmove (mCopyState->m_dataBuffer + 1, start, mCopyState->m_leftOver);
        Din_Go(1528,2048);break;
      }
    }

    //need to set the linebreak_len each time
    Din_Go(1531,2048);uint32_t linebreak_len = 1; //assume CR or LF
    Din_Go(1533,2048);if (*end == '\r' && *(end+1) == '\n')
      {/*259*/Din_Go(1532,2048);linebreak_len = 2;/*260*/}  //CRLF

    Din_Go(1537,2048);if (!mCopyState->m_fromLineSeen)
    {
      Din_Go(1534,2048);mCopyState->m_fromLineSeen = true;
      NS_ASSERTION(strncmp(start, "From ", 5) == 0,
        "Fatal ... bad message format\n");
    }
    else {/*261*/Din_Go(1535,2048);if (strncmp(start, "From ", 5) == 0)
    {
      //if we're at the beginning of the buffer, we've reserved a byte to
      //insert a '>'.  If we're in the middle, we're overwriting the previous
      //line ending, but we've already written it to m_fileStream, so it's OK.
      Din_Go(1536,2048);*--start = '>';
    ;/*262*/}}

    Din_Go(1538,2048);lineLength = end - start + linebreak_len;
    rv = mCopyState->m_fileStream->Write(start, lineLength, &bytesWritten);
    Din_Go(1541,2048);if (bytesWritten != lineLength || NS_FAILED(rv))
    {
      Din_Go(1539,2048);ThrowAlertMsg("copyMsgWriteFailed", mCopyState->m_msgWindow);
      mCopyState->m_writeFailed = true;
      {__IAENUM__ nsresult  ReplaceReturn52 = NS_MSG_ERROR_WRITING_MAIL_FOLDER; Din_Go(1540,2048); return ReplaceReturn52;}
    }

    Din_Go(1543,2048);if (mCopyState->m_parseMsgState)
      {/*263*/Din_Go(1542,2048);mCopyState->m_parseMsgState->ParseAFolderLine(start, lineLength);/*264*/}

    Din_Go(1544,2048);start = end + linebreak_len;
    Din_Go(1547,2048);if (start >= endBuffer)
    {
      Din_Go(1545,2048);mCopyState->m_leftOver = 0;
      Din_Go(1546,2048);break;
    }
  }
  {__IAENUM__ nsresult  ReplaceReturn51 = rv; Din_Go(1549,2048); return ReplaceReturn51;}
}

void nsMsgLocalMailFolder::CopyPropertiesToMsgHdr(nsIMsgDBHdr *destHdr,
                                                  nsIMsgDBHdr *srcHdr,
                                                  bool aIsMove)
{
  Din_Go(1550,2048);nsresult rv;
  nsCOMPtr<nsIPrefBranch> prefBranch(do_GetService(NS_PREFSERVICE_CONTRACTID, &rv));
  NS_ENSURE_SUCCESS_VOID(rv);

  nsCString dontPreserve;

  // These preferences exist so that extensions can control which properties
  // are preserved in the database when a message is moved or copied. All
  // properties are preserved except those listed in these preferences
  Din_Go(1553,2048);if (aIsMove)
    {/*265*/Din_Go(1551,2048);prefBranch->GetCharPref("mailnews.database.summary.dontPreserveOnMove",
                            getter_Copies(dontPreserve));/*266*/}
  else
    {/*267*/Din_Go(1552,2048);prefBranch->GetCharPref("mailnews.database.summary.dontPreserveOnCopy",
                            getter_Copies(dontPreserve));/*268*/}

  Din_Go(1554,2048);CopyHdrPropertiesWithSkipList(destHdr, srcHdr, dontPreserve);
Din_Go(1555,2048);}

void
nsMsgLocalMailFolder::CopyHdrPropertiesWithSkipList(nsIMsgDBHdr *destHdr,
                                                    nsIMsgDBHdr *srcHdr,
                                                    const nsCString &skipList)
{
  Din_Go(1556,2048);nsCOMPtr<nsIUTF8StringEnumerator> propertyEnumerator;
  nsresult rv = srcHdr->GetPropertyEnumerator(getter_AddRefs(propertyEnumerator));
  NS_ENSURE_SUCCESS_VOID(rv);

  // We'll add spaces at beginning and end so we can search for space-name-space
  nsCString dontPreserveEx(NS_LITERAL_CSTRING(" "));
  dontPreserveEx.Append(skipList);
  dontPreserveEx.AppendLiteral(" ");

  nsAutoCString property;
  nsCString sourceString;
  bool hasMore;
  Din_Go(1561,2048);while (NS_SUCCEEDED(propertyEnumerator->HasMore(&hasMore)) && hasMore)
  {
    Din_Go(1557,2048);propertyEnumerator->GetNext(property);
    nsAutoCString propertyEx(NS_LITERAL_CSTRING(" "));
    propertyEx.Append(property);
    propertyEx.AppendLiteral(" ");
    Din_Go(1559,2048);if (dontPreserveEx.Find(propertyEx) != -1) // -1 is not found
      {/*269*/Din_Go(1558,2048);continue;/*270*/}

    Din_Go(1560,2048);srcHdr->GetStringProperty(property.get(), getter_Copies(sourceString));
    destHdr->SetStringProperty(property.get(), sourceString.get());
  }

  Din_Go(1562,2048);nsMsgLabelValue label = 0;
  srcHdr->GetLabel(&label);
  destHdr->SetLabel(label);
Din_Go(1563,2048);}

NS_IMETHODIMP nsMsgLocalMailFolder::EndCopy(bool aCopySucceeded)
{
  Din_Go(1564,2048);if (!mCopyState)
    {/*271*/{__IAENUM__ nsresult  ReplaceReturn50 = NS_OK; Din_Go(1565,2048); return ReplaceReturn50;}/*272*/}

  // we are the destination folder for a move/copy
  Din_Go(1566,2048);nsresult rv = aCopySucceeded ? NS_OK : NS_ERROR_FAILURE;

  Din_Go(1574,2048);if (!aCopySucceeded || mCopyState->m_writeFailed)
  {
    Din_Go(1567,2048);if (mCopyState->m_fileStream)
    {
      Din_Go(1568,2048);if (mCopyState->m_curDstKey != nsMsgKey_None)
        {/*273*/Din_Go(1569,2048);mCopyState->m_msgStore->DiscardNewMessage(mCopyState->m_fileStream,
                                                  mCopyState->m_newHdr);/*274*/}
      Din_Go(1570,2048);mCopyState->m_fileStream->Close();
    }

    Din_Go(1572,2048);if (!mCopyState->m_isMove)
    {
      // passing true because the messages that have been successfully
      // copied have their corresponding hdrs in place. The message that has
      // failed has been truncated so the msf file and berkeley mailbox
      // are in sync.
      Din_Go(1571,2048);(void) OnCopyCompleted(mCopyState->m_srcSupport, true);
      // enable the dest folder
      EnableNotifications(allMessageCountNotifications, true, false /*dbBatching*/); //dest folder doesn't need db batching
    }
    {__IAENUM__ nsresult  ReplaceReturn49 = NS_OK; Din_Go(1573,2048); return ReplaceReturn49;}
  }

  Din_Go(1575,2048);bool multipleCopiesFinished = (mCopyState->m_curCopyIndex >= mCopyState->m_totalMsgCount);

  RefPtr<nsLocalMoveCopyMsgTxn> localUndoTxn = mCopyState->m_undoMsgTxn;

  NS_ASSERTION(mCopyState->m_leftOver == 0, "whoops, something wrong with previous copy");
  mCopyState->m_leftOver = 0; // reset to 0.
  // need to reset this in case we're move/copying multiple msgs.
  mCopyState->m_fromLineSeen = false;

  // flush the copied message. We need a close at the end to get the
  // file size and time updated correctly.
  //
  // These filestream closes are handled inconsistently in the code. In some
  // cases, this is done in EndMessage, while in others it is done here in
  // EndCopy. When we do the close in EndMessage, we'll set
  // mCopyState->m_fileStream to null since it is no longer needed, and detect
  // here the null stream so we know that we don't have to close it here.
  //
  // Similarly, m_parseMsgState->GetNewMsgHdr() returns a null hdr if the hdr
  // has already been processed by EndMessage so it is not doubly added here.

  nsCOMPtr <nsISeekableStream> seekableStream(do_QueryInterface(mCopyState->m_fileStream));
  Din_Go(1586,2048);if (seekableStream)
  {
    Din_Go(1576,2048);if (mCopyState->m_dummyEnvelopeNeeded)
    {
      Din_Go(1577,2048);uint32_t bytesWritten;
      seekableStream->Seek(nsISeekableStream::NS_SEEK_END, 0);
      mCopyState->m_fileStream->Write(MSG_LINEBREAK, MSG_LINEBREAK_LEN, &bytesWritten);
      Din_Go(1579,2048);if (mCopyState->m_parseMsgState)
        {/*275*/Din_Go(1578,2048);mCopyState->m_parseMsgState->ParseAFolderLine(CRLF, MSG_LINEBREAK_LEN);/*276*/}
    }
    Din_Go(1580,2048);rv = mCopyState->m_msgStore->FinishNewMessage(mCopyState->m_fileStream,
                                                  mCopyState->m_newHdr);
    Din_Go(1582,2048);if (NS_SUCCEEDED(rv) && mCopyState->m_newHdr)
      {/*277*/Din_Go(1581,2048);mCopyState->m_newHdr->GetMessageKey(&mCopyState->m_curDstKey);/*278*/}
    Din_Go(1585,2048);if (multipleCopiesFinished)
      {/*279*/Din_Go(1583,2048);mCopyState->m_fileStream->Close();/*280*/}
    else
      {/*281*/Din_Go(1584,2048);mCopyState->m_fileStream->Flush();/*282*/}
  }
  //Copy the header to the new database
  Din_Go(1601,2048);if (mCopyState->m_message)
  {
    //  CopyMessages() goes here, and CopyFileMessages() with metadata to save;
    Din_Go(1587,2048);nsCOMPtr<nsIMsgDBHdr> newHdr;
    Din_Go(1595,2048);if (!mCopyState->m_parseMsgState)
    {
      Din_Go(1588,2048);if (mCopyState->m_destDB)
      {
        Din_Go(1589,2048);if (mCopyState->m_newHdr)
        {
          Din_Go(1590,2048);newHdr = mCopyState->m_newHdr;
          CopyHdrPropertiesWithSkipList(newHdr, mCopyState->m_message,
                                        NS_LITERAL_CSTRING("storeToken msgOffset"));
//          UpdateNewMsgHdr(mCopyState->m_message, newHdr);
          // We need to copy more than just what UpdateNewMsgHdr does. In fact,
          // I think we want to copy almost every property other than
          // storeToken and msgOffset.
          mCopyState->m_destDB->AddNewHdrToDB(newHdr, true);
        }
        else
        {
        Din_Go(1591,2048);rv = mCopyState->m_destDB->CopyHdrFromExistingHdr(mCopyState->m_curDstKey,
          mCopyState->m_message, true,
          getter_AddRefs(newHdr));
        }
        Din_Go(1592,2048);uint32_t newHdrFlags;
        Din_Go(1594,2048);if (newHdr)
        {
          // turn off offline flag - it's not valid for local mail folders.
          Din_Go(1593,2048);newHdr->AndFlags(~nsMsgMessageFlags::Offline, &newHdrFlags);
          mCopyState->m_destMessages->AppendElement(newHdr, false);
        }
      }
      // we can do undo with the dest folder db, see bug #198909
      //else
      //  mCopyState->m_undoMsgTxn = nullptr; //null out the transaction because we can't undo w/o the msg db
    }

    // if we plan on allowing undo, (if we have a mCopyState->m_parseMsgState or not)
    // we need to save the source and dest keys on the undo txn.
    // see bug #179856 for details
    Din_Go(1596,2048);bool isImap;
    Din_Go(1600,2048);if (NS_SUCCEEDED(rv) && localUndoTxn) {
      Din_Go(1597,2048);localUndoTxn->GetSrcIsImap(&isImap);
      Din_Go(1599,2048);if (!isImap || !mCopyState->m_copyingMultipleMessages)
      {
        Din_Go(1598,2048);nsMsgKey aKey;
        uint32_t statusOffset;
        mCopyState->m_message->GetMessageKey(&aKey);
        mCopyState->m_message->GetStatusOffset(&statusOffset);
        localUndoTxn->AddSrcKey(aKey);
        localUndoTxn->AddSrcStatusOffset(statusOffset);
        localUndoTxn->AddDstKey(mCopyState->m_curDstKey);
      }
    }
  }
  Din_Go(1602,2048);nsCOMPtr<nsIMsgDBHdr> newHdr;
  // CopyFileMessage() and CopyMessages() from servers other than mailbox
  Din_Go(1617,2048);if (mCopyState->m_parseMsgState)
  {
    Din_Go(1603,2048);nsCOMPtr<nsIMsgDatabase> msgDb;
    mCopyState->m_parseMsgState->FinishHeader();
    GetDatabaseWOReparse(getter_AddRefs(msgDb));
    Din_Go(1613,2048);if (msgDb)
    {
      Din_Go(1604,2048);nsresult result = mCopyState->m_parseMsgState->GetNewMsgHdr(getter_AddRefs(newHdr));
      // we need to copy newHdr because mCopyState will get cleared 
      // in OnCopyCompleted, but we need OnCopyCompleted to know about
      // the newHdr, via mCopyState. And we send a notification about newHdr
      // after OnCopyCompleted.
      mCopyState->m_newHdr = newHdr;
      Din_Go(1611,2048);if (NS_SUCCEEDED(result) && newHdr)
      {
        // Copy message metadata.
        Din_Go(1605,2048);if (mCopyState->m_message)
        {
          // Propagate the new flag on an imap to local folder filter action
          // Flags may get changed when deleting the original source message in
          // IMAP. We have a copy of the original flags, but parseMsgState has
          // already tried to decide what those flags should be. Who to believe?
          // Let's only deal here with the flags that might get changed, Read
          // and New, and trust upstream code for everything else.
          Din_Go(1606,2048);uint32_t readAndNew = nsMsgMessageFlags::New | nsMsgMessageFlags::Read;
          uint32_t newFlags;
          newHdr->GetFlags(&newFlags);
          newHdr->SetFlags( (newFlags & ~readAndNew) | 
                            ((mCopyState->m_flags) & readAndNew));

          // Copy other message properties.
          CopyPropertiesToMsgHdr(newHdr, mCopyState->m_message, mCopyState->m_isMove);
        }
        Din_Go(1607,2048);msgDb->AddNewHdrToDB(newHdr, true);
        Din_Go(1609,2048);if (localUndoTxn)
        {
          // ** jt - recording the message size for possible undo use; the
          // message size is different for pop3 and imap4 messages
          Din_Go(1608,2048);uint32_t msgSize;
          newHdr->GetMessageSize(&msgSize);
          localUndoTxn->AddDstMsgSize(msgSize);
        }

        Din_Go(1610,2048);mCopyState->m_destMessages->AppendElement(newHdr, false);
      }
      // msgDb->SetSummaryValid(true);
      // msgDb->Commit(nsMsgDBCommitType::kLargeCommit);
    }
    else
      {/*283*/Din_Go(1612,2048);mCopyState->m_undoMsgTxn = nullptr;/*284*/} //null out the transaction because we can't undo w/o the msg db

    Din_Go(1614,2048);mCopyState->m_parseMsgState->Clear();
    Din_Go(1616,2048);if (mCopyState->m_listener) // CopyFileMessage() only
      {/*285*/Din_Go(1615,2048);mCopyState->m_listener->SetMessageKey(mCopyState->m_curDstKey);/*286*/}
  }

  Din_Go(1641,2048);if (!multipleCopiesFinished && !mCopyState->m_copyingMultipleMessages)
  {
    // CopyMessages() goes here; CopyFileMessage() never gets in here because
    // curCopyIndex will always be less than the mCopyState->m_totalMsgCount
    Din_Go(1618,2048);nsCOMPtr<nsISupports> aSupport = do_QueryElementAt(mCopyState->m_messages, mCopyState->m_curCopyIndex);
    rv = CopyMessageTo(aSupport, this, mCopyState->m_msgWindow, mCopyState->m_isMove);
  }
  else
  {
    // If we have some headers, then there is a source, so notify itemMoveCopyCompleted.
    // If we don't have any headers already, (eg save as draft, send) then notify itemAdded.
    // This notification is done after the messages are deleted, so that saving a new draft
    // of a message works correctly -- first an itemDeleted is sent for the old draft, then
    // an itemAdded for the new draft.
    Din_Go(1619,2048);uint32_t numHdrs;
    mCopyState->m_messages->GetLength(&numHdrs);

    Din_Go(1623,2048);if (multipleCopiesFinished && numHdrs && !mCopyState->m_isFolder)
    {
      // we need to send this notification before we delete the source messages,
      // because deleting the source messages clears out the src msg db hdr.
      Din_Go(1620,2048);nsCOMPtr<nsIMsgFolderNotificationService> notifier(do_GetService(NS_MSGNOTIFICATIONSERVICE_CONTRACTID));
      Din_Go(1622,2048);if (notifier)
        {/*287*/Din_Go(1621,2048);notifier->NotifyMsgsMoveCopyCompleted(mCopyState->m_isMove,
                                              mCopyState->m_messages,
                                              this, mCopyState->m_destMessages);/*288*/}
    }

    Din_Go(1636,2048);if (!mCopyState->m_isMove)
    {
      Din_Go(1624,2048);if (multipleCopiesFinished)
      {
        Din_Go(1625,2048);nsCOMPtr<nsIMsgFolder> srcFolder;
        srcFolder = do_QueryInterface(mCopyState->m_srcSupport);
        Din_Go(1627,2048);if (mCopyState->m_isFolder)
          {/*289*/Din_Go(1626,2048);CopyAllSubFolders(srcFolder, nullptr, nullptr);/*290*/}  //Copy all subfolders then notify completion

        Din_Go(1631,2048);if (mCopyState->m_msgWindow && mCopyState->m_undoMsgTxn)
        {
          Din_Go(1628,2048);nsCOMPtr<nsITransactionManager> txnMgr;
          mCopyState->m_msgWindow->GetTransactionManager(getter_AddRefs(txnMgr));
          Din_Go(1630,2048);if (txnMgr)
            {/*291*/Din_Go(1629,2048);txnMgr->DoTransaction(mCopyState->m_undoMsgTxn);/*292*/}
        }

        // enable the dest folder
        Din_Go(1632,2048);EnableNotifications(allMessageCountNotifications, true, false /*dbBatching*/); //dest folder doesn't need db batching
        Din_Go(1634,2048);if (srcFolder && !mCopyState->m_isFolder)
        {
          // I'm not too sure of the proper location of this event. It seems to need to be
          // after the EnableNotifications, or the folder counts can be incorrect
          // during the mDeleteOrMoveMsgCompletedAtom call.
          Din_Go(1633,2048);srcFolder->NotifyFolderEvent(mDeleteOrMoveMsgCompletedAtom);
        }
        Din_Go(1635,2048);(void) OnCopyCompleted(mCopyState->m_srcSupport, true);
      }
    }
    // Send the itemAdded notification in case we didn't send the itemMoveCopyCompleted notification earlier.
    // Posting news messages involves this, yet doesn't have the newHdr initialized, so don't send any
    // notifications in that case.
    Din_Go(1640,2048);if (!numHdrs && newHdr)
    {
      Din_Go(1637,2048);nsCOMPtr<nsIMsgFolderNotificationService> notifier(do_GetService(NS_MSGNOTIFICATIONSERVICE_CONTRACTID));
      Din_Go(1639,2048);if (notifier)
      {
        Din_Go(1638,2048);notifier->NotifyMsgAdded(newHdr);
        // We do not appear to trigger classification in this case, so let's
        // paper over the abyss by just sending the classification notification.
        nsCOMPtr <nsIMutableArray> oneHeaderArray =
          do_CreateInstance(NS_ARRAY_CONTRACTID);
        oneHeaderArray->AppendElement(newHdr, false);
        notifier->NotifyMsgsClassified(oneHeaderArray, false, false);
        // (We do not add the NotReportedClassified processing flag since we
        // just reported it!)
      }
    }
  }
  {__IAENUM__ nsresult  ReplaceReturn48 = rv; Din_Go(1642,2048); return ReplaceReturn48;}
}

static bool gGotGlobalPrefs;
static bool gDeleteFromServerOnMove;

bool nsMsgLocalMailFolder::GetDeleteFromServerOnMove()
{
  Din_Go(1643,2048);if (!gGotGlobalPrefs)
  {
    Din_Go(1644,2048);nsCOMPtr<nsIPrefBranch> pPrefBranch(do_GetService(NS_PREFSERVICE_CONTRACTID));
    Din_Go(1646,2048);if (pPrefBranch)
    {
      Din_Go(1645,2048);pPrefBranch->GetBoolPref("mail.pop3.deleteFromServerOnMove", &gDeleteFromServerOnMove);
      gGotGlobalPrefs = true;
    }
  }
  {_Bool  ReplaceReturn47 = gDeleteFromServerOnMove; Din_Go(1647,2048); return ReplaceReturn47;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::EndMove(bool moveSucceeded)
{
  Din_Go(1648,2048);nsresult rv;
  Din_Go(1650,2048);if (!mCopyState)
    {/*293*/{__IAENUM__ nsresult  ReplaceReturn46 = NS_OK; Din_Go(1649,2048); return ReplaceReturn46;}/*294*/}

  Din_Go(1653,2048);if (!moveSucceeded || mCopyState->m_writeFailed)
  {
    //Notify that a completion finished.
    Din_Go(1651,2048);nsCOMPtr<nsIMsgFolder> srcFolder = do_QueryInterface(mCopyState->m_srcSupport, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    srcFolder->NotifyFolderEvent(mDeleteOrMoveMsgFailedAtom);

    /* passing true because the messages that have been successfully copied have their corressponding
               hdrs in place. The message that has failed has been truncated so the msf file and berkeley mailbox
               are in sync*/

    (void) OnCopyCompleted(mCopyState->m_srcSupport, true);
    // enable the dest folder
    EnableNotifications(allMessageCountNotifications, true, false /*dbBatching*/ );  //dest folder doesn't need db batching
    {__IAENUM__ nsresult  ReplaceReturn45 = NS_OK; Din_Go(1652,2048); return ReplaceReturn45;}
  }

  Din_Go(1665,2048);if (mCopyState && mCopyState->m_curCopyIndex >= mCopyState->m_totalMsgCount)
  {
    //Notify that a completion finished.
    Din_Go(1654,2048);nsCOMPtr<nsIMsgFolder> srcFolder = do_QueryInterface(mCopyState->m_srcSupport, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    nsCOMPtr <nsIMsgLocalMailFolder> localSrcFolder = do_QueryInterface(srcFolder);
    Din_Go(1658,2048);if (localSrcFolder)
    {
      // if we are the trash and a local msg is being moved to us, mark the source
      // for delete from server, if so configured.
      Din_Go(1655,2048);if (mFlags & nsMsgFolderFlags::Trash)
      {
        // if we're deleting on all moves, we'll mark this message for deletion when
        // we call DeleteMessages on the source folder. So don't mark it for deletion
        // here, in that case.
        Din_Go(1656,2048);if (!GetDeleteFromServerOnMove())
          {/*295*/Din_Go(1657,2048);localSrcFolder->MarkMsgsOnPop3Server(mCopyState->m_messages, POP3_DELETE);/*296*/}
      }
    }
    // lets delete these all at once - much faster that way
    Din_Go(1659,2048);rv = srcFolder->DeleteMessages(mCopyState->m_messages, mCopyState->m_msgWindow, true, true, nullptr, mCopyState->m_allowUndo);
    AutoCompact(mCopyState->m_msgWindow);

    // enable the dest folder
    EnableNotifications(allMessageCountNotifications, true, false /*dbBatching*/); //dest folder doesn't need db batching
    // I'm not too sure of the proper location of this event. It seems to need to be
    // after the EnableNotifications, or the folder counts can be incorrect
    // during the mDeleteOrMoveMsgCompletedAtom call.
    srcFolder->NotifyFolderEvent(NS_SUCCEEDED(rv) ? mDeleteOrMoveMsgCompletedAtom : mDeleteOrMoveMsgFailedAtom);

    Din_Go(1663,2048);if (NS_SUCCEEDED(rv) && mCopyState->m_msgWindow && mCopyState->m_undoMsgTxn)
    {
      Din_Go(1660,2048);nsCOMPtr<nsITransactionManager> txnMgr;
      mCopyState->m_msgWindow->GetTransactionManager(getter_AddRefs(txnMgr));
      Din_Go(1662,2048);if (txnMgr)
        {/*297*/Din_Go(1661,2048);txnMgr->DoTransaction(mCopyState->m_undoMsgTxn);/*298*/}
    }
    Din_Go(1664,2048);(void) OnCopyCompleted(mCopyState->m_srcSupport, NS_SUCCEEDED(rv) ? true : false);  //clear the copy state so that the next message from a different folder can be move
  }

  {__IAENUM__ nsresult  ReplaceReturn44 = NS_OK; Din_Go(1666,2048); return ReplaceReturn44;}

}

// this is the beginning of the next message copied
NS_IMETHODIMP nsMsgLocalMailFolder::StartMessage()
{
Din_Go(1667,2048);  // We get crashes that we don't understand (bug 284876), so stupidly prevent that.
  NS_ENSURE_ARG_POINTER(mCopyState);
  Din_Go(1668,2048);nsresult rv = InitCopyMsgHdrAndFileStream();
  NS_ENSURE_SUCCESS(rv, rv);
  {__IAENUM__ nsresult  ReplaceReturn43 = WriteStartOfNewMessage(); Din_Go(1669,2048); return ReplaceReturn43;}
}

// just finished the current message.
NS_IMETHODIMP nsMsgLocalMailFolder::EndMessage(nsMsgKey key)
{
Din_Go(1670,2048);  NS_ENSURE_ARG_POINTER(mCopyState);

  Din_Go(1671,2048);RefPtr<nsLocalMoveCopyMsgTxn> localUndoTxn = mCopyState->m_undoMsgTxn;
  nsCOMPtr<nsIMsgWindow> msgWindow;
  nsresult rv;

  Din_Go(1673,2048);if (localUndoTxn)
  {
    Din_Go(1672,2048);localUndoTxn->GetMsgWindow(getter_AddRefs(msgWindow));
    localUndoTxn->AddSrcKey(key);
    localUndoTxn->AddDstKey(mCopyState->m_curDstKey);
  }

  // I think this is always true for online to offline copy
  Din_Go(1674,2048);mCopyState->m_dummyEnvelopeNeeded = true;
  nsCOMPtr <nsISeekableStream> seekableStream = do_QueryInterface(mCopyState->m_fileStream, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  seekableStream->Seek(nsISeekableStream::NS_SEEK_END, 0);
  uint32_t bytesWritten;
  mCopyState->m_fileStream->Write(MSG_LINEBREAK, MSG_LINEBREAK_LEN, &bytesWritten);
  Din_Go(1676,2048);if (mCopyState->m_parseMsgState)
    {/*299*/Din_Go(1675,2048);mCopyState->m_parseMsgState->ParseAFolderLine(CRLF, MSG_LINEBREAK_LEN);/*300*/}

  Din_Go(1677,2048);rv = mCopyState->m_msgStore->FinishNewMessage(mCopyState->m_fileStream,
                                                mCopyState->m_newHdr);
  mCopyState->m_fileStream->Close();
  mCopyState->m_fileStream = nullptr; // all done with the file stream

  // CopyFileMessage() and CopyMessages() from servers other than mailbox
  Din_Go(1694,2048);if (mCopyState->m_parseMsgState)
  {
    Din_Go(1678,2048);nsCOMPtr<nsIMsgDatabase> msgDb;
    nsCOMPtr<nsIMsgDBHdr> newHdr;

    mCopyState->m_parseMsgState->FinishHeader();

    rv = mCopyState->m_parseMsgState->GetNewMsgHdr(getter_AddRefs(newHdr));
    Din_Go(1690,2048);if (NS_SUCCEEDED(rv) && newHdr)
    {
      Din_Go(1679,2048);nsCOMPtr<nsIMsgFolder> srcFolder = do_QueryInterface(mCopyState->m_srcSupport, &rv);
      NS_ENSURE_SUCCESS(rv, rv);
      nsCOMPtr<nsIMsgDatabase> srcDB;
      srcFolder->GetMsgDatabase(getter_AddRefs(srcDB));
      Din_Go(1683,2048);if (srcDB)
      {
        Din_Go(1680,2048);nsCOMPtr <nsIMsgDBHdr> srcMsgHdr;
        srcDB->GetMsgHdrForKey(key, getter_AddRefs(srcMsgHdr));
        Din_Go(1682,2048);if (srcMsgHdr)
          {/*301*/Din_Go(1681,2048);CopyPropertiesToMsgHdr(newHdr, srcMsgHdr, mCopyState->m_isMove);/*302*/}
      }
      Din_Go(1684,2048);rv = GetDatabaseWOReparse(getter_AddRefs(msgDb));
      Din_Go(1689,2048);if (NS_SUCCEEDED(rv) && msgDb)
      {
        Din_Go(1685,2048);msgDb->AddNewHdrToDB(newHdr, true);
        Din_Go(1687,2048);if (localUndoTxn)
        {
          // ** jt - recording the message size for possible undo use; the
          // message size is different for pop3 and imap4 messages
          Din_Go(1686,2048);uint32_t msgSize;
          newHdr->GetMessageSize(&msgSize);
          localUndoTxn->AddDstMsgSize(msgSize);
        }
      }
      else
        {/*303*/Din_Go(1688,2048);mCopyState->m_undoMsgTxn = nullptr;/*304*/} //null out the transaction because we can't undo w/o the msg db
    }
    Din_Go(1691,2048);mCopyState->m_parseMsgState->Clear();

    Din_Go(1693,2048);if (mCopyState->m_listener) // CopyFileMessage() only
      {/*305*/Din_Go(1692,2048);mCopyState->m_listener->SetMessageKey(mCopyState->m_curDstKey);/*306*/}
  }

  Din_Go(1696,2048);if (mCopyState->m_fileStream)
    {/*307*/Din_Go(1695,2048);mCopyState->m_fileStream->Flush();/*308*/}
  {__IAENUM__ nsresult  ReplaceReturn42 = NS_OK; Din_Go(1697,2048); return ReplaceReturn42;}
}


nsresult nsMsgLocalMailFolder::CopyMessagesTo(nsIArray *messages, nsTArray<nsMsgKey> &keyArray,
                                             nsIMsgWindow *aMsgWindow, nsIMsgFolder *dstFolder,
                                             bool isMove)
{
  Din_Go(1698,2048);if (!mCopyState)
    {/*309*/{__IAENUM__ nsresult  ReplaceReturn41 = NS_ERROR_OUT_OF_MEMORY; Din_Go(1699,2048); return ReplaceReturn41;}/*310*/}

  Din_Go(1700,2048);nsresult rv;

  nsCOMPtr<nsICopyMessageStreamListener> copyStreamListener = do_CreateInstance(NS_COPYMESSAGESTREAMLISTENER_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv,rv);

  nsCOMPtr<nsICopyMessageListener> copyListener(do_QueryInterface(dstFolder, &rv));
  NS_ENSURE_SUCCESS(rv, NS_ERROR_NO_INTERFACE);

  nsCOMPtr<nsIMsgFolder> srcFolder(do_QueryInterface(mCopyState->m_srcSupport, &rv));
  NS_ENSURE_SUCCESS(rv, NS_ERROR_NO_INTERFACE);

  rv = copyStreamListener->Init(srcFolder, copyListener, nullptr);
  Din_Go(1702,2048);if (NS_FAILED(rv))
    {/*311*/{__IAENUM__ nsresult  ReplaceReturn40 = rv; Din_Go(1701,2048); return ReplaceReturn40;}/*312*/}

  Din_Go(1704,2048);if (!mCopyState->m_messageService)
  {
    Din_Go(1703,2048);nsCString uri;
    srcFolder->GetURI(uri);
    rv = GetMessageServiceFromURI(uri, getter_AddRefs(mCopyState->m_messageService));
  }

  Din_Go(1709,2048);if (NS_SUCCEEDED(rv) && mCopyState->m_messageService)
  {
    Din_Go(1705,2048);nsCOMPtr<nsIStreamListener> streamListener(do_QueryInterface(copyStreamListener, &rv));
    NS_ENSURE_SUCCESS(rv, NS_ERROR_NO_INTERFACE);

    mCopyState->m_curCopyIndex = 0;
    // we need to kick off the first message - subsequent messages
    // are kicked off by nsMailboxProtocol when it finishes a message
    // before starting the next message. Only do this if the source folder
    // is a local folder, however. IMAP will handle calling StartMessage for
    // each message that gets downloaded, and news doesn't go through here
    // because news only downloads one message at a time, and this routine
    // is for multiple message copy.
    nsCOMPtr <nsIMsgLocalMailFolder> srcLocalFolder = do_QueryInterface(srcFolder);
    Din_Go(1707,2048);if (srcLocalFolder)
      {/*313*/Din_Go(1706,2048);StartMessage();/*314*/}
    Din_Go(1708,2048);nsCOMPtr<nsIURI> dummyNull;
    rv = mCopyState->m_messageService->CopyMessages(keyArray.Length(),
                                                    keyArray.Elements(),
                                                    srcFolder, streamListener,
                                                    isMove, nullptr, aMsgWindow,
                                                    getter_AddRefs(dummyNull));
  }
  {__IAENUM__ nsresult  ReplaceReturn39 = rv; Din_Go(1710,2048); return ReplaceReturn39;}
}

nsresult nsMsgLocalMailFolder::CopyMessageTo(nsISupports *message,
                                             nsIMsgFolder *dstFolder /* dst same as "this" */,
                                             nsIMsgWindow *aMsgWindow,
                                             bool isMove)
{
  Din_Go(1711,2048);if (!mCopyState)
    {/*315*/{__IAENUM__ nsresult  ReplaceReturn38 = NS_ERROR_OUT_OF_MEMORY; Din_Go(1712,2048); return ReplaceReturn38;}/*316*/}

  Din_Go(1713,2048);nsresult rv;
  nsCOMPtr<nsIMsgDBHdr> msgHdr(do_QueryInterface(message, &rv));
  NS_ENSURE_SUCCESS(rv, NS_ERROR_NO_INTERFACE);

  mCopyState->m_message = do_QueryInterface(msgHdr, &rv);

  nsCOMPtr<nsIMsgFolder> srcFolder(do_QueryInterface(mCopyState->m_srcSupport, &rv));
  NS_ENSURE_SUCCESS(rv, NS_ERROR_NO_INTERFACE);
  nsCString uri;
  srcFolder->GetUriForMsg(msgHdr, uri);

  nsCOMPtr<nsICopyMessageStreamListener> copyStreamListener = do_CreateInstance(NS_COPYMESSAGESTREAMLISTENER_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv,rv);

  nsCOMPtr<nsICopyMessageListener> copyListener(do_QueryInterface(dstFolder, &rv));
  NS_ENSURE_SUCCESS(rv, NS_ERROR_NO_INTERFACE);

  rv = copyStreamListener->Init(srcFolder, copyListener, nullptr);
  Din_Go(1715,2048);if (NS_FAILED(rv))
    {/*317*/{__IAENUM__ nsresult  ReplaceReturn37 = rv; Din_Go(1714,2048); return ReplaceReturn37;}/*318*/}

  Din_Go(1717,2048);if (!mCopyState->m_messageService)
    {/*319*/Din_Go(1716,2048);rv = GetMessageServiceFromURI(uri, getter_AddRefs(mCopyState->m_messageService));/*320*/}

  Din_Go(1719,2048);if (NS_SUCCEEDED(rv) && mCopyState->m_messageService)
  {
    Din_Go(1718,2048);nsCOMPtr<nsIStreamListener> streamListener(do_QueryInterface(copyStreamListener, &rv));
    NS_ENSURE_SUCCESS(rv, NS_ERROR_NO_INTERFACE);
    nsCOMPtr<nsIURI> dummyNull;
    rv = mCopyState->m_messageService->CopyMessage(uri.get(), streamListener, isMove, nullptr, aMsgWindow,
                                                   getter_AddRefs(dummyNull));
  }
  {__IAENUM__ nsresult  ReplaceReturn36 = rv; Din_Go(1720,2048); return ReplaceReturn36;}
}

// A message is being deleted from a POP3 mail file, so check and see if we have the message
// being deleted in the server. If so, then we need to remove the message from the server as well.
// We have saved the UIDL of the message in the popstate.dat file and we must match this uidl, so
// read the message headers and see if we have it, then mark the message for deletion from the server.
// The next time we look at mail the message will be deleted from the server.

NS_IMETHODIMP
nsMsgLocalMailFolder::MarkMsgsOnPop3Server(nsIArray *aMessages, int32_t aMark)
{
  Din_Go(1721,2048);nsLocalFolderScanState folderScanState;
  nsCOMPtr<nsIPop3IncomingServer> curFolderPop3MailServer;
  nsCOMArray<nsIPop3IncomingServer> pop3Servers; // servers with msgs deleted...

  nsCOMPtr<nsIMsgIncomingServer> incomingServer;
  nsresult rv = GetServer(getter_AddRefs(incomingServer));
  NS_ENSURE_SUCCESS(rv, NS_MSG_INVALID_OR_MISSING_SERVER);

  nsCOMPtr <nsIMsgAccountManager> accountManager = do_GetService(NS_MSGACCOUNTMANAGER_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv,rv);

  // I wonder if we should run through the pop3 accounts and see if any of them have
  // leave on server set. If not, we could short-circuit some of this.

  curFolderPop3MailServer = do_QueryInterface(incomingServer, &rv);
  rv = GetFolderScanState(&folderScanState);
  NS_ENSURE_SUCCESS(rv,rv);

  uint32_t srcCount;
  aMessages->GetLength(&srcCount);

  // Filter delete requests are always honored, others are subject
  // to the deleteMailLeftOnServer preference.
  int32_t mark;
  mark = (aMark == POP3_FORCE_DEL) ? POP3_DELETE : aMark;

  Din_Go(1744,2048);for (uint32_t i = 0; i < srcCount; i++)
  {
    /* get uidl for this message */
    Din_Go(1722,2048);nsCOMPtr<nsIMsgDBHdr> msgDBHdr (do_QueryElementAt(aMessages, i, &rv));

    uint32_t flags = 0;

    Din_Go(1743,2048);if (msgDBHdr)
    {
      Din_Go(1723,2048);msgDBHdr->GetFlags(&flags);
      nsCOMPtr <nsIPop3IncomingServer> msgPop3Server = curFolderPop3MailServer;
      bool leaveOnServer = false;
      bool deleteMailLeftOnServer = false;
      // set up defaults, in case there's no x-mozilla-account header
      Din_Go(1725,2048);if (curFolderPop3MailServer)
      {
        Din_Go(1724,2048);curFolderPop3MailServer->GetDeleteMailLeftOnServer(&deleteMailLeftOnServer);
        curFolderPop3MailServer->GetLeaveMessagesOnServer(&leaveOnServer);
      }

      Din_Go(1726,2048);rv = GetUidlFromFolder(&folderScanState, msgDBHdr);
      Din_Go(1728,2048);if (!NS_SUCCEEDED(rv))
        {/*321*/Din_Go(1727,2048);continue;/*322*/}

      Din_Go(1734,2048);if (folderScanState.m_uidl)
      {
        Din_Go(1729,2048);nsCOMPtr <nsIMsgAccount> account;
        rv = accountManager->GetAccount(folderScanState.m_accountKey, getter_AddRefs(account));
        Din_Go(1733,2048);if (NS_SUCCEEDED(rv) && account)
        {
          Din_Go(1730,2048);account->GetIncomingServer(getter_AddRefs(incomingServer));
          nsCOMPtr<nsIPop3IncomingServer> curMsgPop3MailServer = do_QueryInterface(incomingServer);
          Din_Go(1732,2048);if (curMsgPop3MailServer)
          {
            Din_Go(1731,2048);msgPop3Server = curMsgPop3MailServer;
            msgPop3Server->GetDeleteMailLeftOnServer(&deleteMailLeftOnServer);
            msgPop3Server->GetLeaveMessagesOnServer(&leaveOnServer);
          }
        }
      }
      // ignore this header if not partial and leaveOnServer not set...
      // or if we can't find the pop3 server.
      Din_Go(1736,2048);if (!msgPop3Server || (! (flags & nsMsgMessageFlags::Partial) && !leaveOnServer))
        {/*323*/Din_Go(1735,2048);continue;/*324*/}
      // if marking deleted, ignore header if we're not deleting from
      // server when deleting locally.
      Din_Go(1738,2048);if (aMark == POP3_DELETE && leaveOnServer && !deleteMailLeftOnServer)
        {/*325*/Din_Go(1737,2048);continue;/*326*/}
      Din_Go(1742,2048);if (folderScanState.m_uidl)
      {
        Din_Go(1739,2048);msgPop3Server->AddUidlToMark(folderScanState.m_uidl, mark);
        // remember this pop server in list of servers with msgs deleted
        Din_Go(1741,2048);if (pop3Servers.IndexOfObject(msgPop3Server) == -1)
          {/*327*/Din_Go(1740,2048);pop3Servers.AppendObject(msgPop3Server);/*328*/}
      }
    }
  }
  Din_Go(1746,2048);if (folderScanState.m_inputStream)
    {/*329*/Din_Go(1745,2048);folderScanState.m_inputStream->Close();/*330*/}
  // need to do this for all pop3 mail servers that had messages deleted.
  Din_Go(1747,2048);uint32_t serverCount = pop3Servers.Count();
  Din_Go(1749,2048);for (uint32_t index = 0; index < serverCount; index++)
    {/*331*/Din_Go(1748,2048);pop3Servers[index]->MarkMessages();/*332*/}

  {__IAENUM__ nsresult  ReplaceReturn35 = rv; Din_Go(1750,2048); return ReplaceReturn35;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::DeleteDownloadMsg(nsIMsgDBHdr *aMsgHdr, bool *aDoSelect)
{
  Din_Go(1751,2048);uint32_t numMsgs;
  char *newMsgId;

  // This method is only invoked thru DownloadMessagesForOffline()
  Din_Go(1766,2048);if (mDownloadState != DOWNLOAD_STATE_NONE)
  {
    // We only remember the first key, no matter how many
    // messages were originally selected.
    Din_Go(1752,2048);if (mDownloadState == DOWNLOAD_STATE_INITED)
    {
      Din_Go(1753,2048);aMsgHdr->GetMessageKey(&mDownloadSelectKey);
      mDownloadState = DOWNLOAD_STATE_GOTMSG;
    }

    Din_Go(1754,2048);aMsgHdr->GetMessageId(&newMsgId);

    // Walk through all the selected headers, looking for a matching
    // Message-ID.
    numMsgs = mDownloadMessages.Length();
    Din_Go(1765,2048);for (uint32_t i = 0; i < numMsgs; i++)
    {
      Din_Go(1755,2048);nsresult rv;
      nsCOMPtr<nsIMsgDBHdr> msgDBHdr = mDownloadMessages[i];
      char *oldMsgId = nullptr;
      msgDBHdr->GetMessageId(&oldMsgId);

      // Delete the first match and remove it from the array
      Din_Go(1764,2048);if (!PL_strcmp(newMsgId, oldMsgId))
      {
        Din_Go(1756,2048);rv = GetDatabase();
        Din_Go(1758,2048);if (!mDatabase)
          {/*333*/{__IAENUM__ nsresult  ReplaceReturn34 = rv; Din_Go(1757,2048); return ReplaceReturn34;}/*334*/}

        Din_Go(1759,2048);UpdateNewMsgHdr(msgDBHdr, aMsgHdr);

#if DOWNLOAD_NOTIFY_STYLE == DOWNLOAD_NOTIFY_LAST
        msgDBHdr->GetMessageKey(&mDownloadOldKey);
        msgDBHdr->GetThreadParent(&mDownloadOldParent);
        msgDBHdr->GetFlags(&mDownloadOldFlags);
        mDatabase->DeleteHeader(msgDBHdr, nullptr, false, false);
        // Tell caller we want to select this message
        if (aDoSelect)
          *aDoSelect = true;
#else
        mDatabase->DeleteHeader(msgDBHdr, nullptr, false, true);
        // Tell caller we want to select this message
        Din_Go(1761,2048);if (aDoSelect && mDownloadState == DOWNLOAD_STATE_GOTMSG)
          {/*335*/Din_Go(1760,2048);*aDoSelect = true;/*336*/}
#endif
        Din_Go(1762,2048);mDownloadMessages.RemoveElementAt(i);
        Din_Go(1763,2048);break;
      }
    }
  }

  {__IAENUM__ nsresult  ReplaceReturn33 = NS_OK; Din_Go(1767,2048); return ReplaceReturn33;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::SelectDownloadMsg()
{
#if DOWNLOAD_NOTIFY_STYLE == DOWNLOAD_NOTIFY_LAST
  if (mDownloadState >= DOWNLOAD_STATE_GOTMSG)
  {
    nsresult rv = GetDatabase();
    if (!mDatabase)
      return rv;
    }
    mDatabase->NotifyKeyDeletedAll(mDownloadOldKey, mDownloadOldParent, mDownloadOldFlags, nullptr);
  }
#endif

  Din_Go(1768,2048);if (mDownloadState == DOWNLOAD_STATE_GOTMSG && mDownloadWindow)
  {
    Din_Go(1769,2048);nsAutoCString newuri;
    nsBuildLocalMessageURI(mBaseMessageURI.get(), mDownloadSelectKey, newuri);
    nsCOMPtr<nsIMsgWindowCommands> windowCommands;
    mDownloadWindow->GetWindowCommands(getter_AddRefs(windowCommands));
    Din_Go(1771,2048);if (windowCommands)
      {/*337*/Din_Go(1770,2048);windowCommands->SelectMessage(newuri);/*338*/}
    Din_Go(1772,2048);mDownloadState = DOWNLOAD_STATE_DIDSEL;
  }
  {__IAENUM__ nsresult  ReplaceReturn32 = NS_OK; Din_Go(1773,2048); return ReplaceReturn32;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::DownloadMessagesForOffline(nsIArray *aMessages, nsIMsgWindow *aWindow)
{
  Din_Go(1774,2048);if (mDownloadState != DOWNLOAD_STATE_NONE)
    {/*339*/{__IAENUM__ nsresult  ReplaceReturn31 = NS_ERROR_FAILURE; Din_Go(1775,2048); return ReplaceReturn31;}/*340*/} // already has a download in progress

  // We're starting a download...
  Din_Go(1776,2048);mDownloadState = DOWNLOAD_STATE_INITED;

  MarkMsgsOnPop3Server(aMessages, POP3_FETCH_BODY);

  // Pull out all the PARTIAL messages into a new array
  uint32_t srcCount;
  aMessages->GetLength(&srcCount);

  nsresult rv;
  Din_Go(1782,2048);for (uint32_t i = 0; i < srcCount; i++)
  {
    Din_Go(1777,2048);nsCOMPtr<nsIMsgDBHdr> msgDBHdr (do_QueryElementAt(aMessages, i, &rv));
    Din_Go(1781,2048);if (NS_SUCCEEDED(rv))
    {
      Din_Go(1778,2048);uint32_t flags = 0;
      msgDBHdr->GetFlags(&flags);
      Din_Go(1780,2048);if (flags & nsMsgMessageFlags::Partial)
        {/*341*/Din_Go(1779,2048);mDownloadMessages.AppendElement(msgDBHdr);/*342*/}
    }
  }
  Din_Go(1783,2048);mDownloadWindow = aWindow;

  nsCOMPtr<nsIMsgIncomingServer> server;
  rv = GetServer(getter_AddRefs(server));
  NS_ENSURE_SUCCESS(rv, NS_MSG_INVALID_OR_MISSING_SERVER);

  nsCOMPtr<nsILocalMailIncomingServer> localMailServer = do_QueryInterface(server, &rv);
  NS_ENSURE_SUCCESS(rv, NS_MSG_INVALID_OR_MISSING_SERVER);
  {__IAENUM__ nsresult  ReplaceReturn30 = localMailServer->GetNewMail(aWindow, this, this, nullptr); Din_Go(1784,2048); return ReplaceReturn30;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::NotifyDelete()
{
  Din_Go(1785,2048);NotifyFolderEvent(mDeleteOrMoveMsgCompletedAtom);
  {__IAENUM__ nsresult  ReplaceReturn29 = NS_OK; Din_Go(1786,2048); return ReplaceReturn29;}
}

// TODO:  once we move certain code into the IncomingServer (search for TODO)
// this method will go away.
// sometimes this gets called when we don't have the server yet, so
// that's why we're not calling GetServer()
NS_IMETHODIMP
nsMsgLocalMailFolder::GetIncomingServerType(nsACString& aServerType)
{
  Din_Go(1787,2048);nsresult rv;
  Din_Go(1809,2048);if (mType.IsEmpty())
  {
    Din_Go(1788,2048);nsCOMPtr<nsIURL> url = do_CreateInstance(NS_STANDARDURL_CONTRACTID, &rv);
    Din_Go(1790,2048);if (NS_FAILED(rv))
      {/*343*/{__IAENUM__ nsresult  ReplaceReturn28 = rv; Din_Go(1789,2048); return ReplaceReturn28;}/*344*/}

    Din_Go(1791,2048);rv = url->SetSpec(mURI);
    Din_Go(1793,2048);if (NS_FAILED(rv))
      {/*345*/{__IAENUM__ nsresult  ReplaceReturn27 = rv; Din_Go(1792,2048); return ReplaceReturn27;}/*346*/}

    Din_Go(1794,2048);nsCOMPtr<nsIMsgAccountManager> accountManager =
             do_GetService(NS_MSGACCOUNTMANAGER_CONTRACTID, &rv);
    Din_Go(1796,2048);if (NS_FAILED(rv))
      {/*347*/{__IAENUM__ nsresult  ReplaceReturn26 = rv; Din_Go(1795,2048); return ReplaceReturn26;}/*348*/}

    Din_Go(1797,2048);nsCOMPtr<nsIMsgIncomingServer> server;
    // try "none" first
    url->SetScheme(NS_LITERAL_CSTRING("none"));
    rv = accountManager->FindServerByURI(url, false, getter_AddRefs(server));
    Din_Go(1808,2048);if (NS_SUCCEEDED(rv) && server)
      {/*349*/Din_Go(1798,2048);mType.AssignLiteral("none");/*350*/}
    else
    {
      // next try "pop3"
      Din_Go(1799,2048);url->SetScheme(NS_LITERAL_CSTRING("pop3"));
      rv = accountManager->FindServerByURI(url, false, getter_AddRefs(server));
      Din_Go(1807,2048);if (NS_SUCCEEDED(rv) && server)
        {/*351*/Din_Go(1800,2048);mType.AssignLiteral("pop3");/*352*/}
      else
      {
        // next try "rss"
        Din_Go(1801,2048);url->SetScheme(NS_LITERAL_CSTRING("rss"));
        rv = accountManager->FindServerByURI(url, false, getter_AddRefs(server));
        Din_Go(1806,2048);if (NS_SUCCEEDED(rv) && server)
          {/*353*/Din_Go(1802,2048);mType.AssignLiteral("rss");/*354*/}
        else
        {
#ifdef HAVE_MOVEMAIL
          // next try "movemail"
          Din_Go(1803,2048);url->SetScheme(NS_LITERAL_CSTRING("movemail"));
          rv = accountManager->FindServerByURI(url, false, getter_AddRefs(server));
          Din_Go(1805,2048);if (NS_SUCCEEDED(rv) && server)
            {/*355*/Din_Go(1804,2048);mType.AssignLiteral("movemail");/*356*/}
#endif /* HAVE_MOVEMAIL */
        }
      }
    }
  }
  Din_Go(1810,2048);aServerType = mType;
  {__IAENUM__ nsresult  ReplaceReturn25 = NS_OK; Din_Go(1811,2048); return ReplaceReturn25;}
}

nsresult nsMsgLocalMailFolder::CreateBaseMessageURI(const nsACString& aURI)
{
  {__IAENUM__ nsresult  ReplaceReturn24 = nsCreateLocalBaseMessageURI(aURI, mBaseMessageURI); Din_Go(1812,2048); return ReplaceReturn24;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::OnStartRunningUrl(nsIURI * aUrl)
{
  Din_Go(1813,2048);nsresult rv;
  nsCOMPtr<nsIPop3URL> popurl = do_QueryInterface(aUrl, &rv);
  Din_Go(1819,2048);if (NS_SUCCEEDED(rv))
  {
    Din_Go(1814,2048);nsAutoCString aSpec;
    aUrl->GetSpec(aSpec);
    Din_Go(1818,2048);if (strstr(aSpec.get(), "uidl="))
    {
      Din_Go(1815,2048);nsCOMPtr<nsIPop3Sink> popsink;
      rv = popurl->GetPop3Sink(getter_AddRefs(popsink));
      Din_Go(1817,2048);if (NS_SUCCEEDED(rv))
      {
        Din_Go(1816,2048);popsink->SetBaseMessageUri(mBaseMessageURI.get());
        nsCString messageuri;
        popurl->GetMessageUri(getter_Copies(messageuri));
        popsink->SetOrigMessageUri(messageuri);
      }
    }
  }
  {__IAENUM__ nsresult  ReplaceReturn23 = nsMsgDBFolder::OnStartRunningUrl(aUrl); Din_Go(1820,2048); return ReplaceReturn23;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::OnStopRunningUrl(nsIURI * aUrl, nsresult aExitCode)
{
  // If we just finished a DownloadMessages call, reset...
  Din_Go(1821,2048);if (mDownloadState != DOWNLOAD_STATE_NONE)
  {
    Din_Go(1822,2048);mDownloadState = DOWNLOAD_STATE_NONE;
    mDownloadMessages.Clear();
    mDownloadWindow = nullptr;
    {__IAENUM__ nsresult  ReplaceReturn22 = nsMsgDBFolder::OnStopRunningUrl(aUrl, aExitCode); Din_Go(1823,2048); return ReplaceReturn22;}
  }

  Din_Go(1824,2048);nsresult rv;
  Din_Go(1851,2048);if (NS_SUCCEEDED(aExitCode))
  {
    Din_Go(1825,2048);nsCOMPtr<nsIMsgMailSession> mailSession = do_GetService(NS_MSGMAILSESSION_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    nsCOMPtr<nsIMsgWindow> msgWindow;
    rv = mailSession->GetTopmostMsgWindow(getter_AddRefs(msgWindow));
    nsAutoCString aSpec;
    Din_Go(1827,2048);if (aUrl)
    {/*357*/Din_Go(1826,2048);aUrl->GetSpec(aSpec);/*358*/}

    Din_Go(1844,2048);if (strstr(aSpec.get(), "uidl="))
    {
      Din_Go(1828,2048);nsCOMPtr<nsIPop3URL> popurl = do_QueryInterface(aUrl, &rv);
      Din_Go(1843,2048);if (NS_SUCCEEDED(rv))
      {
        Din_Go(1829,2048);nsCString messageuri;
        rv = popurl->GetMessageUri(getter_Copies(messageuri));
        Din_Go(1842,2048);if (NS_SUCCEEDED(rv))
        {
          NS_ENSURE_SUCCESS(rv, rv);
          Din_Go(1830,2048);nsCOMPtr <nsIMsgDBHdr> msgDBHdr;
          rv = GetMsgDBHdrFromURI(messageuri.get(), getter_AddRefs(msgDBHdr));
          Din_Go(1834,2048);if (NS_SUCCEEDED(rv))
          {
            Din_Go(1831,2048);GetDatabase();
            Din_Go(1833,2048);if (mDatabase)
              {/*359*/Din_Go(1832,2048);mDatabase->DeleteHeader(msgDBHdr, nullptr, true, true);/*360*/}
          }

          Din_Go(1835,2048);nsCOMPtr<nsIPop3Sink> pop3sink;
          nsCString newMessageUri;
          rv = popurl->GetPop3Sink(getter_AddRefs(pop3sink));
          Din_Go(1841,2048);if (NS_SUCCEEDED(rv))
          {
            Din_Go(1836,2048);pop3sink->GetMessageUri(getter_Copies(newMessageUri));
            Din_Go(1840,2048);if (msgWindow)
            {
              Din_Go(1837,2048);nsCOMPtr<nsIMsgWindowCommands> windowCommands;
              msgWindow->GetWindowCommands(getter_AddRefs(windowCommands));
              Din_Go(1839,2048);if (windowCommands)
                {/*361*/Din_Go(1838,2048);windowCommands->SelectMessage(newMessageUri);/*362*/}
            }
          }
        }
      }
    }

    Din_Go(1850,2048);if (mFlags & nsMsgFolderFlags::Inbox)
    {
      Din_Go(1845,2048);if (mDatabase && mCheckForNewMessagesAfterParsing)
      {
        Din_Go(1846,2048);bool valid = false; // GetSummaryValid may return without setting valid.
        mDatabase->GetSummaryValid(&valid);
        Din_Go(1848,2048);if (valid && msgWindow)
          {/*363*/Din_Go(1847,2048);rv = GetNewMessages(msgWindow, nullptr);/*364*/}
        Din_Go(1849,2048);mCheckForNewMessagesAfterParsing = false;
      }
    }
  }

  Din_Go(1855,2048);if (m_parsingFolder)
  {
    // Clear this before calling OnStopRunningUrl, in case the url listener
    // tries to get the database.
    Din_Go(1852,2048);m_parsingFolder = false;

    // TODO: Updating the size should be pushed down into the msg store backend
    // so that the size is recalculated as part of parsing the folder data
    // (important for maildir), once GetSizeOnDisk is pushed into the msgStores
    // (bug 1032360).
    (void)RefreshSizeOnDisk();

    // Update the summary totals so the front end will
    // show the right thing.
    UpdateSummaryTotals(true);

    Din_Go(1854,2048);if (mReparseListener)
    {
      Din_Go(1853,2048);nsCOMPtr<nsIUrlListener> saveReparseListener = mReparseListener;
      mReparseListener = nullptr;
      saveReparseListener->OnStopRunningUrl(aUrl, aExitCode);
    }
  }
  Din_Go(1861,2048);if (mFlags & nsMsgFolderFlags::Inbox)
  {
    // if we are the inbox and running pop url
    Din_Go(1856,2048);nsCOMPtr<nsIPop3URL> popurl = do_QueryInterface(aUrl, &rv);
    Din_Go(1860,2048);if (NS_SUCCEEDED(rv))
    {
      Din_Go(1857,2048);nsCOMPtr<nsIMsgIncomingServer> server;
      GetServer(getter_AddRefs(server));
      // this is the deferred to account, in the global inbox case
      Din_Go(1859,2048);if (server)
        {/*365*/Din_Go(1858,2048);server->SetPerformingBiff(false);/*366*/}  //biff is over
    }
  }
  {__IAENUM__ nsresult  ReplaceReturn21 = nsMsgDBFolder::OnStopRunningUrl(aUrl, aExitCode); Din_Go(1862,2048); return ReplaceReturn21;}
}

nsresult nsMsgLocalMailFolder::DisplayMoveCopyStatusMsg()
{
  Din_Go(1863,2048);nsresult rv = NS_OK;
  Din_Go(1878,2048);if (mCopyState)
  {
    Din_Go(1864,2048);if (!mCopyState->m_statusFeedback)
    {
      // get msgWindow from undo txn
      Din_Go(1865,2048);nsCOMPtr<nsIMsgWindow> msgWindow;
      Din_Go(1867,2048);if (mCopyState->m_undoMsgTxn)
        {/*367*/Din_Go(1866,2048);mCopyState->m_undoMsgTxn->GetMsgWindow(getter_AddRefs(msgWindow));/*368*/}
      Din_Go(1869,2048);if (!msgWindow)
        {/*369*/{__IAENUM__ nsresult  ReplaceReturn20 = NS_OK; Din_Go(1868,2048); return ReplaceReturn20;}/*370*/} // not a fatal error.

      Din_Go(1870,2048);msgWindow->GetStatusFeedback(getter_AddRefs(mCopyState->m_statusFeedback));
    }

    Din_Go(1872,2048);if (!mCopyState->m_stringBundle)
    {
      Din_Go(1871,2048);nsCOMPtr<nsIStringBundleService> bundleService =
        mozilla::services::GetStringBundleService();
      NS_ENSURE_TRUE(bundleService, NS_ERROR_UNEXPECTED);
      rv = bundleService->CreateBundle("chrome://messenger/locale/localMsgs.properties", getter_AddRefs(mCopyState->m_stringBundle));
      NS_ENSURE_SUCCESS(rv, rv);
    }
    Din_Go(1877,2048);if (mCopyState->m_statusFeedback && mCopyState->m_stringBundle)
    {
      Din_Go(1873,2048);nsString folderName;
      GetName(folderName);
      nsAutoString numMsgSoFarString;
      numMsgSoFarString.AppendInt((mCopyState->m_copyingMultipleMessages) ? mCopyState->m_curCopyIndex : 1);

      nsAutoString totalMessagesString;
      totalMessagesString.AppendInt(mCopyState->m_totalMsgCount);
      nsString finalString;
      const char16_t * stringArray[] = { numMsgSoFarString.get(), totalMessagesString.get(), folderName.get() };
      rv = mCopyState->m_stringBundle->FormatStringFromName(
        (mCopyState->m_isMove) ?
        MOZ_UTF16("movingMessagesStatus") :
        MOZ_UTF16("copyingMessagesStatus"),
        stringArray, 3, getter_Copies(finalString));
      int64_t nowMS = PR_IntervalToMilliseconds(PR_IntervalNow());

      // only update status/progress every half second
      Din_Go(1875,2048);if (nowMS - mCopyState->m_lastProgressTime < 500 &&
          mCopyState->m_curCopyIndex < mCopyState->m_totalMsgCount)
        {/*371*/{__IAENUM__ nsresult  ReplaceReturn19 = NS_OK; Din_Go(1874,2048); return ReplaceReturn19;}/*372*/}

      Din_Go(1876,2048);mCopyState->m_lastProgressTime = nowMS;
      mCopyState->m_statusFeedback->ShowStatusString(finalString);
      mCopyState->m_statusFeedback->ShowProgress(mCopyState->m_curCopyIndex * 100 / mCopyState->m_totalMsgCount);
    }
  }
  {__IAENUM__ nsresult  ReplaceReturn18 = rv; Din_Go(1879,2048); return ReplaceReturn18;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::SetFlagsOnDefaultMailboxes(uint32_t flags)
{
  Din_Go(1880,2048);if (flags & nsMsgFolderFlags::Inbox)
    {/*373*/Din_Go(1881,2048);setSubfolderFlag(NS_LITERAL_STRING("Inbox"), nsMsgFolderFlags::Inbox);/*374*/}

  Din_Go(1883,2048);if (flags & nsMsgFolderFlags::SentMail)
    {/*375*/Din_Go(1882,2048);setSubfolderFlag(NS_LITERAL_STRING("Sent"), nsMsgFolderFlags::SentMail);/*376*/}

  Din_Go(1885,2048);if (flags & nsMsgFolderFlags::Drafts)
    {/*377*/Din_Go(1884,2048);setSubfolderFlag(NS_LITERAL_STRING("Drafts"), nsMsgFolderFlags::Drafts);/*378*/}

  Din_Go(1887,2048);if (flags & nsMsgFolderFlags::Templates)
    {/*379*/Din_Go(1886,2048);setSubfolderFlag(NS_LITERAL_STRING("Templates"), nsMsgFolderFlags::Templates);/*380*/}

  Din_Go(1889,2048);if (flags & nsMsgFolderFlags::Trash)
    {/*381*/Din_Go(1888,2048);setSubfolderFlag(NS_LITERAL_STRING("Trash"), nsMsgFolderFlags::Trash);/*382*/}

  Din_Go(1891,2048);if (flags & nsMsgFolderFlags::Queue)
    {/*383*/Din_Go(1890,2048);setSubfolderFlag(NS_LITERAL_STRING("Unsent Messages"), nsMsgFolderFlags::Queue);/*384*/}

  Din_Go(1893,2048);if (flags & nsMsgFolderFlags::Junk)
    {/*385*/Din_Go(1892,2048);setSubfolderFlag(NS_LITERAL_STRING("Junk"), nsMsgFolderFlags::Junk);/*386*/}

  Din_Go(1895,2048);if (flags & nsMsgFolderFlags::Archive)
    {/*387*/Din_Go(1894,2048);setSubfolderFlag(NS_LITERAL_STRING("Archives"), nsMsgFolderFlags::Archive);/*388*/}

  {__IAENUM__ nsresult  ReplaceReturn17 = NS_OK; Din_Go(1896,2048); return ReplaceReturn17;}
}

nsresult
nsMsgLocalMailFolder::setSubfolderFlag(const nsAString& aFolderName, uint32_t flags)
{
  // FindSubFolder() expects the folder name to be escaped
  // see bug #192043
  Din_Go(1897,2048);nsAutoCString escapedFolderName;
  nsresult rv = NS_MsgEscapeEncodeURLPath(aFolderName, escapedFolderName);
  NS_ENSURE_SUCCESS(rv,rv);
  nsCOMPtr<nsIMsgFolder> msgFolder;
  rv = FindSubFolder(escapedFolderName, getter_AddRefs(msgFolder));
  NS_ENSURE_SUCCESS(rv, rv);

  // we only want to do this if the folder *really* exists,
  // so check if it has a parent. Otherwise, we'll create the
  // .msf file when we don't want to.
  nsCOMPtr <nsIMsgFolder> parent;
  msgFolder->GetParent(getter_AddRefs(parent));
  Din_Go(1899,2048);if (!parent)
    {/*389*/{__IAENUM__ nsresult  ReplaceReturn16 = NS_ERROR_FAILURE; Din_Go(1898,2048); return ReplaceReturn16;}/*390*/}

  Din_Go(1900,2048);rv = msgFolder->SetFlag(flags);
  NS_ENSURE_SUCCESS(rv, rv);
  {__IAENUM__ nsresult  ReplaceReturn15 = msgFolder->SetPrettyName(aFolderName); Din_Go(1901,2048); return ReplaceReturn15;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::GetCheckForNewMessagesAfterParsing(bool *aCheckForNewMessagesAfterParsing)
{
Din_Go(1902,2048);  NS_ENSURE_ARG_POINTER(aCheckForNewMessagesAfterParsing);
  Din_Go(1903,2048);*aCheckForNewMessagesAfterParsing = mCheckForNewMessagesAfterParsing;
  {__IAENUM__ nsresult  ReplaceReturn14 = NS_OK; Din_Go(1904,2048); return ReplaceReturn14;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::SetCheckForNewMessagesAfterParsing(bool aCheckForNewMessagesAfterParsing)
{
  Din_Go(1905,2048);mCheckForNewMessagesAfterParsing = aCheckForNewMessagesAfterParsing;
  {__IAENUM__ nsresult  ReplaceReturn13 = NS_OK; Din_Go(1906,2048); return ReplaceReturn13;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::NotifyCompactCompleted()
{
  Din_Go(1907,2048);mExpungedBytes = 0;
  m_newMsgs.Clear(); // if compacted, m_newMsgs probably aren't valid.
  // if compacted, processing flags probably also aren't valid.
  ClearProcessingFlags();
  (void) RefreshSizeOnDisk();
  (void) CloseDBIfFolderNotOpen();
  nsCOMPtr <nsIAtom> compactCompletedAtom;
  compactCompletedAtom = MsgGetAtom("CompactCompleted");
  NotifyFolderEvent(compactCompletedAtom);
  {__IAENUM__ nsresult  ReplaceReturn12 = NS_OK; Din_Go(1908,2048); return ReplaceReturn12;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::Shutdown(bool shutdownChildren)
{
  Din_Go(1909,2048);mInitialized = false;
  {__IAENUM__ nsresult  ReplaceReturn11 = nsMsgDBFolder::Shutdown(shutdownChildren); Din_Go(1910,2048); return ReplaceReturn11;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::OnMessageClassified(const char *aMsgURI,
  nsMsgJunkStatus aClassification,
  uint32_t aJunkPercent)

{
  Din_Go(1911,2048);nsCOMPtr<nsIMsgIncomingServer> server;
  nsresult rv = GetServer(getter_AddRefs(server));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsISpamSettings> spamSettings;
  rv = server->GetSpamSettings(getter_AddRefs(spamSettings));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCString spamFolderURI;
  rv = spamSettings->GetSpamFolderURI(getter_Copies(spamFolderURI));
  NS_ENSURE_SUCCESS(rv,rv);

  Din_Go(1944,2048);if (aMsgURI) // not end of batch
  {
    Din_Go(1912,2048);nsCOMPtr <nsIMsgDBHdr> msgHdr;
    rv = GetMsgDBHdrFromURI(aMsgURI, getter_AddRefs(msgHdr));
    NS_ENSURE_SUCCESS(rv, rv);

    nsMsgKey msgKey;
    rv = msgHdr->GetMessageKey(&msgKey);
    NS_ENSURE_SUCCESS(rv, rv);

    // check if this message needs junk classification
    uint32_t processingFlags;
    GetProcessingFlags(msgKey, &processingFlags);

    Din_Go(1924,2048);if (processingFlags & nsMsgProcessingFlags::ClassifyJunk)
    {
      Din_Go(1913,2048);nsMsgDBFolder::OnMessageClassified(aMsgURI, aClassification, aJunkPercent);

      Din_Go(1923,2048);if (aClassification == nsIJunkMailPlugin::JUNK)
      {
        Din_Go(1914,2048);bool willMoveMessage = false;

        // don't do the move when we are opening up
        // the junk mail folder or the trash folder
        // or when manually classifying messages in those folders
        Din_Go(1921,2048);if (!(mFlags & nsMsgFolderFlags::Junk || mFlags & nsMsgFolderFlags::Trash))
        {
          Din_Go(1915,2048);bool moveOnSpam = false;
          rv = spamSettings->GetMoveOnSpam(&moveOnSpam);
          NS_ENSURE_SUCCESS(rv,rv);
          Din_Go(1920,2048);if (moveOnSpam)
          {
            Din_Go(1916,2048);nsCOMPtr<nsIMsgFolder> folder;
            rv = GetExistingFolder(spamFolderURI, getter_AddRefs(folder));
            Din_Go(1919,2048);if (NS_SUCCEEDED(rv) && folder)
            {
              Din_Go(1917,2048);rv = folder->SetFlag(nsMsgFolderFlags::Junk);
              NS_ENSURE_SUCCESS(rv,rv);
              mSpamKeysToMove.AppendElement(msgKey);
              willMoveMessage = true;
            }
            else
            {
              // XXX TODO
              // JUNK MAIL RELATED
              // the listener should do
              // rv = folder->SetFlag(nsMsgFolderFlags::Junk);
              // NS_ENSURE_SUCCESS(rv,rv);
              // mSpamKeysToMove.AppendElement(msgKey);
              // willMoveMessage = true;
              Din_Go(1918,2048);rv = GetOrCreateFolder(spamFolderURI, nullptr /* aListener */);
              NS_ASSERTION(NS_SUCCEEDED(rv), "GetOrCreateFolder failed");
            }
          }
        }
        Din_Go(1922,2048);rv = spamSettings->LogJunkHit(msgHdr, willMoveMessage);
        NS_ENSURE_SUCCESS(rv,rv);
      }
    }
  }

  else // end of batch
  {
    // Parent will apply post bayes filters.
    Din_Go(1925,2048);nsMsgDBFolder::OnMessageClassified(nullptr, nsIJunkMailPlugin::UNCLASSIFIED, 0);
    nsCOMPtr<nsIMutableArray> messages(do_CreateInstance(NS_ARRAY_CONTRACTID));
    Din_Go(1940,2048);if (!mSpamKeysToMove.IsEmpty())
    {
      Din_Go(1926,2048);nsCOMPtr<nsIMsgFolder> folder;
      Din_Go(1928,2048);if (!spamFolderURI.IsEmpty())
        {/*391*/Din_Go(1927,2048);rv = GetExistingFolder(spamFolderURI, getter_AddRefs(folder));/*392*/}
      Din_Go(1935,2048);for (uint32_t keyIndex = 0; keyIndex < mSpamKeysToMove.Length(); keyIndex++)
      {
        // If an upstream filter moved this message, don't move it here.
        Din_Go(1929,2048);nsMsgKey msgKey = mSpamKeysToMove.ElementAt(keyIndex);
        nsMsgProcessingFlagType processingFlags;
        GetProcessingFlags(msgKey, &processingFlags);
        Din_Go(1934,2048);if (folder && !(processingFlags & nsMsgProcessingFlags::FilterToMove))
        {
          Din_Go(1930,2048);nsCOMPtr<nsIMsgDBHdr> mailHdr;
          rv = GetMessageHeader(msgKey, getter_AddRefs(mailHdr));
          Din_Go(1932,2048);if (NS_SUCCEEDED(rv) && mailHdr)
            {/*393*/Din_Go(1931,2048);messages->AppendElement(mailHdr, false);/*394*/}
        }
        else
        {
          // We don't need the processing flag any more.
          Din_Go(1933,2048);AndProcessingFlags(msgKey, ~nsMsgProcessingFlags::FilterToMove);
        }
      }

      Din_Go(1939,2048);if (folder)
      {
        Din_Go(1936,2048);nsCOMPtr<nsIMsgCopyService> copySvc = do_GetService(NS_MSGCOPYSERVICE_CONTRACTID, &rv);
        NS_ENSURE_SUCCESS(rv,rv);

        rv = copySvc->CopyMessages(this, messages, folder, true,
          /*nsIMsgCopyServiceListener* listener*/ nullptr, nullptr, false /*allowUndo*/);
        NS_ASSERTION(NS_SUCCEEDED(rv), "CopyMessages failed");
        Din_Go(1938,2048);if (NS_FAILED(rv))
        {
          Din_Go(1937,2048);nsAutoCString logMsg("failed to copy junk messages to junk folder rv = ");
          logMsg.AppendInt(static_cast<uint32_t>(rv), 16);
          spamSettings->LogJunkString(logMsg.get());
        }
      }
    }
    Din_Go(1941,2048);int32_t numNewMessages;
    GetNumNewMessages(false, &numNewMessages);
    uint32_t length;
    messages->GetLength(&length);
    SetNumNewMessages(numNewMessages - length);
    mSpamKeysToMove.Clear();
    // check if this is the inbox first...
    Din_Go(1943,2048);if (mFlags & nsMsgFolderFlags::Inbox)
      {/*395*/Din_Go(1942,2048);PerformBiffNotifications();/*396*/}
  }
  {__IAENUM__ nsresult  ReplaceReturn10 = NS_OK; Din_Go(1945,2048); return ReplaceReturn10;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::GetFolderScanState(nsLocalFolderScanState *aState)
{
Din_Go(1946,2048);  NS_ENSURE_ARG_POINTER(aState);

  Din_Go(1947,2048);nsresult rv = GetMsgStore(getter_AddRefs(aState->m_msgStore));
  NS_ENSURE_SUCCESS(rv, rv);
    aState->m_uidl = nullptr;
  {__IAENUM__ nsresult  ReplaceReturn9 = rv; Din_Go(1948,2048); return ReplaceReturn9;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::GetUidlFromFolder(nsLocalFolderScanState *aState, nsIMsgDBHdr *aMsgDBHdr)
{
  Din_Go(1949,2048);bool more = false;
  uint32_t size = 0, len = 0;
  const char *accountKey = nullptr;
  nsresult rv = GetMsgInputStream(aMsgDBHdr, &aState->m_streamReusable,
                                  getter_AddRefs(aState->m_inputStream));
  aState->m_seekableStream = do_QueryInterface(aState->m_inputStream);
  NS_ENSURE_SUCCESS(rv,rv);

  nsAutoPtr<nsLineBuffer<char> > lineBuffer(new nsLineBuffer<char>);
  NS_ENSURE_TRUE(lineBuffer, NS_ERROR_OUT_OF_MEMORY);

  aState->m_uidl = nullptr;

  aMsgDBHdr->GetMessageSize(&len);
  Din_Go(1964,2048);while (len > 0)
  {
    Din_Go(1950,2048);rv = NS_ReadLine(aState->m_inputStream.get(), lineBuffer.get(), aState->m_header, &more);
    Din_Go(1963,2048);if (NS_SUCCEEDED(rv))
    {
      Din_Go(1951,2048);size = aState->m_header.Length();
      Din_Go(1953,2048);if (!size)
        {/*397*/Din_Go(1952,2048);break;/*398*/}
      // this isn't quite right - need to account for line endings
      Din_Go(1954,2048);len -= size;
      // account key header will always be before X_UIDL header
      Din_Go(1962,2048);if (!accountKey)
      {
        Din_Go(1955,2048);accountKey = strstr(aState->m_header.get(), HEADER_X_MOZILLA_ACCOUNT_KEY);
        Din_Go(1957,2048);if (accountKey)
        {
          Din_Go(1956,2048);accountKey += strlen(HEADER_X_MOZILLA_ACCOUNT_KEY) + 2;
          aState->m_accountKey = accountKey;
        }
      }
      else
      {
        Din_Go(1958,2048);aState->m_uidl = strstr(aState->m_header.get(), X_UIDL);
        Din_Go(1961,2048);if (aState->m_uidl)
        {
          Din_Go(1959,2048);aState->m_uidl += X_UIDL_LEN + 2; // skip UIDL: header
          Din_Go(1960,2048);break;
        }
      }
    }
  }
  Din_Go(1966,2048);if (!aState->m_streamReusable)
  {
    Din_Go(1965,2048);aState->m_inputStream->Close();
    aState->m_inputStream = nullptr;
  }
  Din_Go(1967,2048);lineBuffer = nullptr;
  {__IAENUM__ nsresult  ReplaceReturn8 = rv; Din_Go(1968,2048); return ReplaceReturn8;}
}

/**
 * Adds a message to the end of the folder, parsing it as it goes, and
 * applying filters, if applicable.
 */
NS_IMETHODIMP
nsMsgLocalMailFolder::AddMessage(const char *aMessage, nsIMsgDBHdr **aHdr)
{
  Din_Go(1969,2048);const char *aMessages[] = {aMessage};
  nsCOMPtr<nsIArray> hdrs;
  nsresult rv = AddMessageBatch(1, aMessages, getter_AddRefs(hdrs));
  NS_ENSURE_SUCCESS(rv, rv);
  nsCOMPtr<nsIMsgDBHdr> hdr(do_QueryElementAt(hdrs, 0, &rv));
  NS_ENSURE_SUCCESS(rv, rv);
  hdr.forget(aHdr);
  {__IAENUM__ nsresult  ReplaceReturn7 = rv; Din_Go(1970,2048); return ReplaceReturn7;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::AddMessageBatch(uint32_t aMessageCount,
                                      const char **aMessages,
                                      nsIArray **aHdrArray)
{
Din_Go(1971,2048);  NS_ENSURE_ARG_POINTER(aHdrArray);

  Din_Go(1972,2048);nsCOMPtr<nsIMsgIncomingServer> server;
  nsresult rv = GetServer(getter_AddRefs(server));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIMsgPluggableStore> msgStore;
  nsCOMPtr <nsIOutputStream> outFileStream;
  nsCOMPtr<nsIMsgDBHdr> newHdr;

  rv = server->GetMsgStore(getter_AddRefs(msgStore));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIMsgFolder> rootFolder;
  rv = GetRootFolder(getter_AddRefs(rootFolder));
  NS_ENSURE_SUCCESS(rv, rv);

  bool isLocked;

  GetLocked(&isLocked);
  Din_Go(1973,2048);if (isLocked)
    return NS_MSG_FOLDER_BUSY;

  Din_Go(1974,2048);AcquireSemaphore(static_cast<nsIMsgLocalMailFolder*>(this));

  Din_Go(1984,2048);if (NS_SUCCEEDED(rv))
  {
    Din_Go(1975,2048);nsCOMPtr<nsIMutableArray> hdrArray =
      do_CreateInstance(NS_ARRAY_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    Din_Go(1983,2048);for (uint32_t i = 0; i < aMessageCount; i++)
    {
      Din_Go(1976,2048);RefPtr<nsParseNewMailState> newMailParser = new nsParseNewMailState;
      NS_ENSURE_TRUE(newMailParser, NS_ERROR_OUT_OF_MEMORY);
      Din_Go(1978,2048);if (!mGettingNewMessages)
        {/*399*/Din_Go(1977,2048);newMailParser->DisableFilters();/*400*/}
      Din_Go(1979,2048);bool reusable;
      rv = msgStore->GetNewMsgOutputStream(this, getter_AddRefs(newHdr),
                                      &reusable,
                                      getter_AddRefs(outFileStream));
      NS_ENSURE_SUCCESS(rv, rv);

      // Get a msgWindow. Proceed without one, but filter actions to imap folders
      // will silently fail if not signed in and no window for a prompt.
      nsCOMPtr<nsIMsgWindow> msgWindow;
      nsCOMPtr<nsIMsgMailSession> mailSession = do_GetService(NS_MSGMAILSESSION_CONTRACTID, &rv);
      Din_Go(1981,2048);if (NS_SUCCEEDED(rv))
        {/*401*/Din_Go(1980,2048);mailSession->GetTopmostMsgWindow(getter_AddRefs(msgWindow));/*402*/}

      Din_Go(1982,2048);rv = newMailParser->Init(rootFolder, this,
                               msgWindow, newHdr, outFileStream);

      uint32_t bytesWritten, messageLen = strlen(aMessages[i]);
      outFileStream->Write(aMessages[i], messageLen, &bytesWritten);
      newMailParser->BufferInput(aMessages[i], messageLen);

      msgStore->FinishNewMessage(outFileStream, newHdr);
      outFileStream->Close();
      outFileStream = nullptr;
      newMailParser->OnStopRequest(nullptr, nullptr, NS_OK);
      newMailParser->EndMsgDownload();
      hdrArray->AppendElement(newHdr, false);
    }
    NS_ADDREF(*aHdrArray = hdrArray);
  }
  Din_Go(1985,2048);ReleaseSemaphore(static_cast<nsIMsgLocalMailFolder*>(this));
  {__IAENUM__ nsresult  ReplaceReturn6 = rv; Din_Go(1986,2048); return ReplaceReturn6;}
}

NS_IMETHODIMP
nsMsgLocalMailFolder::WarnIfLocalFileTooBig(nsIMsgWindow *aWindow,
                                            int64_t aSpaceRequested,
                                            bool *aTooBig)
{
Din_Go(1987,2048);  NS_ENSURE_ARG_POINTER(aTooBig);

  Din_Go(1988,2048);*aTooBig = true;
  nsCOMPtr<nsIMsgPluggableStore> msgStore;
  nsresult rv = GetMsgStore(getter_AddRefs(msgStore));
  NS_ENSURE_SUCCESS(rv, rv);
  bool spaceAvailable = false;
  // check if we have a reasonable amount of space left
  rv = msgStore->HasSpaceAvailable(this, aSpaceRequested, &spaceAvailable);
  Din_Go(1993,2048);if (NS_SUCCEEDED(rv) && spaceAvailable) {
    Din_Go(1989,2048);*aTooBig = false;
  } else {/*403*/Din_Go(1990,2048);if (rv == NS_ERROR_FILE_TOO_BIG) {
    Din_Go(1991,2048);ThrowAlertMsg("mailboxTooLarge", aWindow);
  } else {
    Din_Go(1992,2048);ThrowAlertMsg("outOfDiskSpace", aWindow);
  ;/*404*/}}
  {__IAENUM__ nsresult  ReplaceReturn5 = NS_OK; Din_Go(1994,2048); return ReplaceReturn5;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::FetchMsgPreviewText(nsMsgKey *aKeysToFetch, uint32_t aNumKeys,
                                                 bool aLocalOnly, nsIUrlListener *aUrlListener,
                                                 bool *aAsyncResults)
{
Din_Go(1995,2048);  NS_ENSURE_ARG_POINTER(aKeysToFetch);
  NS_ENSURE_ARG_POINTER(aAsyncResults);

  Din_Go(1996,2048);*aAsyncResults = false;
  nsCOMPtr <nsIInputStream> inputStream;
  nsCOMPtr<nsIMsgPluggableStore> msgStore;
  nsresult rv = GetMsgStore(getter_AddRefs(msgStore));
  NS_ENSURE_SUCCESS(rv, rv);

  Din_Go(2001,2048);for (uint32_t i = 0; i < aNumKeys; i++)
  {
    Din_Go(1997,2048);nsCOMPtr <nsIMsgDBHdr> msgHdr;
    nsCString prevBody;
    rv = GetMessageHeader(aKeysToFetch[i], getter_AddRefs(msgHdr));
    NS_ENSURE_SUCCESS(rv, rv);
    // ignore messages that already have a preview body.
    msgHdr->GetStringProperty("preview", getter_Copies(prevBody));
    Din_Go(1999,2048);if (!prevBody.IsEmpty())
      {/*405*/Din_Go(1998,2048);continue;/*406*/}

    Din_Go(2000,2048);bool reusable;
    rv = GetMsgInputStream(msgHdr, &reusable, getter_AddRefs(inputStream));
    NS_ENSURE_SUCCESS(rv,rv);
    rv = GetMsgPreviewTextFromStream(msgHdr, inputStream);
  }
  {__IAENUM__ nsresult  ReplaceReturn4 = rv; Din_Go(2002,2048); return ReplaceReturn4;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::AddKeywordsToMessages(nsIArray *aMessages, const nsACString& aKeywords)
{
  {__IAENUM__ nsresult  ReplaceReturn3 = ChangeKeywordForMessages(aMessages, aKeywords, true /* add */); Din_Go(2003,2048); return ReplaceReturn3;}
}
nsresult nsMsgLocalMailFolder::ChangeKeywordForMessages(nsIArray *aMessages, const nsACString& aKeywords, bool add)
{
  Din_Go(2004,2048);nsresult rv = (add) ? nsMsgDBFolder::AddKeywordsToMessages(aMessages, aKeywords)
                      : nsMsgDBFolder::RemoveKeywordsFromMessages(aMessages, aKeywords);

    NS_ENSURE_SUCCESS(rv, rv);
  nsCOMPtr<nsIMsgPluggableStore> msgStore;
  GetMsgStore(getter_AddRefs(msgStore));
    NS_ENSURE_SUCCESS(rv, rv);
  {__IAENUM__ nsresult  ReplaceReturn2 = msgStore->ChangeKeywords(aMessages, aKeywords, add); Din_Go(2005,2048); return ReplaceReturn2;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::RemoveKeywordsFromMessages(nsIArray *aMessages, const nsACString& aKeywords)
{
  {__IAENUM__ nsresult  ReplaceReturn1 = ChangeKeywordForMessages(aMessages, aKeywords, false /* remove */); Din_Go(2006,2048); return ReplaceReturn1;}
}

NS_IMETHODIMP nsMsgLocalMailFolder::UpdateNewMsgHdr(nsIMsgDBHdr* aOldHdr, nsIMsgDBHdr* aNewHdr)
{
Din_Go(2007,2048);  NS_ENSURE_ARG_POINTER(aOldHdr);
  NS_ENSURE_ARG_POINTER(aNewHdr);
  // Preserve any properties set on the message.
  Din_Go(2008,2048);CopyPropertiesToMsgHdr(aNewHdr, aOldHdr, true);

  // Preserve keywords manually, since they are set as don't preserve.
  nsCString keywordString;
  aOldHdr->GetStringProperty("keywords", getter_Copies(keywordString));
  aNewHdr->SetStringProperty("keywords", keywordString.get());

  // If the junk score was set by the plugin, remove junkscore to force a new
  // junk analysis, this time using the body.
  nsCString junkScoreOrigin;
  aOldHdr->GetStringProperty("junkscoreorigin", getter_Copies(junkScoreOrigin));
  Din_Go(2010,2048);if (junkScoreOrigin.EqualsLiteral("plugin"))
    {/*407*/Din_Go(2009,2048);aNewHdr->SetStringProperty("junkscore", "");/*408*/}

  {__IAENUM__ nsresult  ReplaceReturn0 = NS_OK; Din_Go(2011,2048); return ReplaceReturn0;}
}
