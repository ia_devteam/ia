/*
 * Ceph string constants
 */
#include "include/types.h"
#include "var/tmp/sensor.h"

const char *ceph_entity_type_name(int type)
{
	SensorCall(1);switch (type) {
	case CEPH_ENTITY_TYPE_MDS: {const char * ReplaceReturn116 = "mds"; SensorCall(2); return ReplaceReturn116;}
	case CEPH_ENTITY_TYPE_OSD: {const char * ReplaceReturn115 = "osd"; SensorCall(3); return ReplaceReturn115;}
	case CEPH_ENTITY_TYPE_MON: {const char * ReplaceReturn114 = "mon"; SensorCall(4); return ReplaceReturn114;}
	case CEPH_ENTITY_TYPE_CLIENT: {const char * ReplaceReturn113 = "client"; SensorCall(5); return ReplaceReturn113;}
	case CEPH_ENTITY_TYPE_AUTH: {const char * ReplaceReturn112 = "auth"; SensorCall(6); return ReplaceReturn112;}
	default: {const char * ReplaceReturn111 = "unknown"; SensorCall(7); return ReplaceReturn111;}
	}
SensorCall(8);}

const char *ceph_osd_op_name(int op)
{
	SensorCall(9);switch (op) {
#define GENERATE_CASE(op, opcode, str)	case CEPH_OSD_OP_##op: return (str);
__CEPH_FORALL_OSD_OPS(GENERATE_CASE)
#undef GENERATE_CASE
	default:
		{const char * ReplaceReturn110 = "???"; SensorCall(11); return ReplaceReturn110;}
	}
SensorCall(12);}

const char *ceph_osd_state_name(int s)
{
	SensorCall(13);switch (s) {
	case CEPH_OSD_EXISTS:
		{const char * ReplaceReturn109 = "exists"; SensorCall(14); return ReplaceReturn109;}
	case CEPH_OSD_UP:
		{const char * ReplaceReturn108 = "up"; SensorCall(15); return ReplaceReturn108;}
	case CEPH_OSD_AUTOOUT:
		{const char * ReplaceReturn107 = "autoout"; SensorCall(16); return ReplaceReturn107;}
	case CEPH_OSD_NEW:
		{const char * ReplaceReturn106 = "new"; SensorCall(17); return ReplaceReturn106;}
	default:
		{const char * ReplaceReturn105 = "???"; SensorCall(18); return ReplaceReturn105;}
	}	
SensorCall(19);}

const char *ceph_osd_watch_op_name(int o)
{
	SensorCall(20);switch (o) {
	case CEPH_OSD_WATCH_OP_UNWATCH:
		{const char * ReplaceReturn104 = "unwatch"; SensorCall(21); return ReplaceReturn104;}
	case CEPH_OSD_WATCH_OP_WATCH:
		{const char * ReplaceReturn103 = "watch"; SensorCall(22); return ReplaceReturn103;}
	case CEPH_OSD_WATCH_OP_RECONNECT:
		{const char * ReplaceReturn102 = "reconnect"; SensorCall(23); return ReplaceReturn102;}
	case CEPH_OSD_WATCH_OP_PING:
		{const char * ReplaceReturn101 = "ping"; SensorCall(24); return ReplaceReturn101;}
	default:
		{const char * ReplaceReturn100 = "???"; SensorCall(25); return ReplaceReturn100;}
	}
SensorCall(26);}

const char *ceph_mds_state_name(int s)
{
	SensorCall(27);switch (s) {
		/* down and out */
	case CEPH_MDS_STATE_DNE:        {const char * ReplaceReturn99 = "down:dne"; SensorCall(28); return ReplaceReturn99;}
	case CEPH_MDS_STATE_STOPPED:    {const char * ReplaceReturn98 = "down:stopped"; SensorCall(29); return ReplaceReturn98;}
	case CEPH_MDS_STATE_DAMAGED:   {const char * ReplaceReturn97 = "down:damaged"; SensorCall(30); return ReplaceReturn97;}
		/* up and out */
	case CEPH_MDS_STATE_BOOT:       {const char * ReplaceReturn96 = "up:boot"; SensorCall(31); return ReplaceReturn96;}
	case CEPH_MDS_STATE_STANDBY:    {const char * ReplaceReturn95 = "up:standby"; SensorCall(32); return ReplaceReturn95;}
	case CEPH_MDS_STATE_STANDBY_REPLAY:    {const char * ReplaceReturn94 = "up:standby-replay"; SensorCall(33); return ReplaceReturn94;}
	case CEPH_MDS_STATE_REPLAYONCE: {const char * ReplaceReturn93 = "up:oneshot-replay"; SensorCall(34); return ReplaceReturn93;}
	case CEPH_MDS_STATE_CREATING:   {const char * ReplaceReturn92 = "up:creating"; SensorCall(35); return ReplaceReturn92;}
	case CEPH_MDS_STATE_STARTING:   {const char * ReplaceReturn91 = "up:starting"; SensorCall(36); return ReplaceReturn91;}
		/* up and in */
	case CEPH_MDS_STATE_REPLAY:     {const char * ReplaceReturn90 = "up:replay"; SensorCall(37); return ReplaceReturn90;}
	case CEPH_MDS_STATE_RESOLVE:    {const char * ReplaceReturn89 = "up:resolve"; SensorCall(38); return ReplaceReturn89;}
	case CEPH_MDS_STATE_RECONNECT:  {const char * ReplaceReturn88 = "up:reconnect"; SensorCall(39); return ReplaceReturn88;}
	case CEPH_MDS_STATE_REJOIN:     {const char * ReplaceReturn87 = "up:rejoin"; SensorCall(40); return ReplaceReturn87;}
	case CEPH_MDS_STATE_CLIENTREPLAY: {const char * ReplaceReturn86 = "up:clientreplay"; SensorCall(41); return ReplaceReturn86;}
	case CEPH_MDS_STATE_ACTIVE:     {const char * ReplaceReturn85 = "up:active"; SensorCall(42); return ReplaceReturn85;}
	case CEPH_MDS_STATE_STOPPING:   {const char * ReplaceReturn84 = "up:stopping"; SensorCall(43); return ReplaceReturn84;}
	}
	{const char * ReplaceReturn83 = "???"; SensorCall(44); return ReplaceReturn83;}
}

const char *ceph_session_op_name(int op)
{
	SensorCall(45);switch (op) {
	case CEPH_SESSION_REQUEST_OPEN: {const char * ReplaceReturn82 = "request_open"; SensorCall(46); return ReplaceReturn82;}
	case CEPH_SESSION_OPEN: {const char * ReplaceReturn81 = "open"; SensorCall(47); return ReplaceReturn81;}
	case CEPH_SESSION_REQUEST_CLOSE: {const char * ReplaceReturn80 = "request_close"; SensorCall(48); return ReplaceReturn80;}
	case CEPH_SESSION_CLOSE: {const char * ReplaceReturn79 = "close"; SensorCall(49); return ReplaceReturn79;}
	case CEPH_SESSION_REQUEST_RENEWCAPS: {const char * ReplaceReturn78 = "request_renewcaps"; SensorCall(50); return ReplaceReturn78;}
	case CEPH_SESSION_RENEWCAPS: {const char * ReplaceReturn77 = "renewcaps"; SensorCall(51); return ReplaceReturn77;}
	case CEPH_SESSION_STALE: {const char * ReplaceReturn76 = "stale"; SensorCall(52); return ReplaceReturn76;}
	case CEPH_SESSION_RECALL_STATE: {const char * ReplaceReturn75 = "recall_state"; SensorCall(53); return ReplaceReturn75;}
	case CEPH_SESSION_FLUSHMSG: {const char * ReplaceReturn74 = "flushmsg"; SensorCall(54); return ReplaceReturn74;}
	case CEPH_SESSION_FLUSHMSG_ACK: {const char * ReplaceReturn73 = "flushmsg_ack"; SensorCall(55); return ReplaceReturn73;}
	case CEPH_SESSION_REJECT: {const char * ReplaceReturn72 = "reject"; SensorCall(56); return ReplaceReturn72;}
	}
	{const char * ReplaceReturn71 = "???"; SensorCall(57); return ReplaceReturn71;}
}

const char *ceph_mds_op_name(int op)
{
	SensorCall(58);switch (op) {
	case CEPH_MDS_OP_LOOKUP:  {const char * ReplaceReturn70 = "lookup"; SensorCall(59); return ReplaceReturn70;}
	case CEPH_MDS_OP_LOOKUPHASH:  {const char * ReplaceReturn69 = "lookuphash"; SensorCall(60); return ReplaceReturn69;}
	case CEPH_MDS_OP_LOOKUPPARENT:  {const char * ReplaceReturn68 = "lookupparent"; SensorCall(61); return ReplaceReturn68;}
	case CEPH_MDS_OP_LOOKUPINO:  {const char * ReplaceReturn67 = "lookupino"; SensorCall(62); return ReplaceReturn67;}
	case CEPH_MDS_OP_LOOKUPNAME:  {const char * ReplaceReturn66 = "lookupname"; SensorCall(63); return ReplaceReturn66;}
	case CEPH_MDS_OP_GETATTR:  {const char * ReplaceReturn65 = "getattr"; SensorCall(64); return ReplaceReturn65;}
	case CEPH_MDS_OP_SETXATTR: {const char * ReplaceReturn64 = "setxattr"; SensorCall(65); return ReplaceReturn64;}
	case CEPH_MDS_OP_SETATTR: {const char * ReplaceReturn63 = "setattr"; SensorCall(66); return ReplaceReturn63;}
	case CEPH_MDS_OP_RMXATTR: {const char * ReplaceReturn62 = "rmxattr"; SensorCall(67); return ReplaceReturn62;}
	case CEPH_MDS_OP_SETLAYOUT: {const char * ReplaceReturn61 = "setlayou"; SensorCall(68); return ReplaceReturn61;}
	case CEPH_MDS_OP_SETDIRLAYOUT: {const char * ReplaceReturn60 = "setdirlayout"; SensorCall(69); return ReplaceReturn60;}
	case CEPH_MDS_OP_READDIR: {const char * ReplaceReturn59 = "readdir"; SensorCall(70); return ReplaceReturn59;}
	case CEPH_MDS_OP_MKNOD: {const char * ReplaceReturn58 = "mknod"; SensorCall(71); return ReplaceReturn58;}
	case CEPH_MDS_OP_LINK: {const char * ReplaceReturn57 = "link"; SensorCall(72); return ReplaceReturn57;}
	case CEPH_MDS_OP_UNLINK: {const char * ReplaceReturn56 = "unlink"; SensorCall(73); return ReplaceReturn56;}
	case CEPH_MDS_OP_RENAME: {const char * ReplaceReturn55 = "rename"; SensorCall(74); return ReplaceReturn55;}
	case CEPH_MDS_OP_MKDIR: {const char * ReplaceReturn54 = "mkdir"; SensorCall(75); return ReplaceReturn54;}
	case CEPH_MDS_OP_RMDIR: {const char * ReplaceReturn53 = "rmdir"; SensorCall(76); return ReplaceReturn53;}
	case CEPH_MDS_OP_SYMLINK: {const char * ReplaceReturn52 = "symlink"; SensorCall(77); return ReplaceReturn52;}
	case CEPH_MDS_OP_CREATE: {const char * ReplaceReturn51 = "create"; SensorCall(78); return ReplaceReturn51;}
	case CEPH_MDS_OP_OPEN: {const char * ReplaceReturn50 = "open"; SensorCall(79); return ReplaceReturn50;}
	case CEPH_MDS_OP_LOOKUPSNAP: {const char * ReplaceReturn49 = "lookupsnap"; SensorCall(80); return ReplaceReturn49;}
	case CEPH_MDS_OP_LSSNAP: {const char * ReplaceReturn48 = "lssnap"; SensorCall(81); return ReplaceReturn48;}
	case CEPH_MDS_OP_MKSNAP: {const char * ReplaceReturn47 = "mksnap"; SensorCall(82); return ReplaceReturn47;}
	case CEPH_MDS_OP_RMSNAP: {const char * ReplaceReturn46 = "rmsnap"; SensorCall(83); return ReplaceReturn46;}
	case CEPH_MDS_OP_RENAMESNAP: {const char * ReplaceReturn45 = "renamesnap"; SensorCall(84); return ReplaceReturn45;}
	case CEPH_MDS_OP_SETFILELOCK: {const char * ReplaceReturn44 = "setfilelock"; SensorCall(85); return ReplaceReturn44;}
	case CEPH_MDS_OP_GETFILELOCK: {const char * ReplaceReturn43 = "getfilelock"; SensorCall(86); return ReplaceReturn43;}
	case CEPH_MDS_OP_FRAGMENTDIR: {const char * ReplaceReturn42 = "fragmentdir"; SensorCall(87); return ReplaceReturn42;}
	case CEPH_MDS_OP_EXPORTDIR: {const char * ReplaceReturn41 = "exportdir"; SensorCall(88); return ReplaceReturn41;}
	case CEPH_MDS_OP_FLUSH: {const char * ReplaceReturn40 = "flush_path"; SensorCall(89); return ReplaceReturn40;}
	case CEPH_MDS_OP_ENQUEUE_SCRUB: {const char * ReplaceReturn39 = "enqueue_scrub"; SensorCall(90); return ReplaceReturn39;}
	case CEPH_MDS_OP_REPAIR_FRAGSTATS: {const char * ReplaceReturn38 = "repair_fragstats"; SensorCall(91); return ReplaceReturn38;}
	case CEPH_MDS_OP_REPAIR_INODESTATS: {const char * ReplaceReturn37 = "repair_inodestats"; SensorCall(92); return ReplaceReturn37;}
	}
	{const char * ReplaceReturn36 = "???"; SensorCall(93); return ReplaceReturn36;}
}

const char *ceph_cap_op_name(int op)
{
	SensorCall(94);switch (op) {
	case CEPH_CAP_OP_GRANT: {const char * ReplaceReturn35 = "grant"; SensorCall(95); return ReplaceReturn35;}
	case CEPH_CAP_OP_REVOKE: {const char * ReplaceReturn34 = "revoke"; SensorCall(96); return ReplaceReturn34;}
	case CEPH_CAP_OP_TRUNC: {const char * ReplaceReturn33 = "trunc"; SensorCall(97); return ReplaceReturn33;}
	case CEPH_CAP_OP_EXPORT: {const char * ReplaceReturn32 = "export"; SensorCall(98); return ReplaceReturn32;}
	case CEPH_CAP_OP_IMPORT: {const char * ReplaceReturn31 = "import"; SensorCall(99); return ReplaceReturn31;}
	case CEPH_CAP_OP_UPDATE: {const char * ReplaceReturn30 = "update"; SensorCall(100); return ReplaceReturn30;}
	case CEPH_CAP_OP_DROP: {const char * ReplaceReturn29 = "drop"; SensorCall(101); return ReplaceReturn29;}
	case CEPH_CAP_OP_FLUSH: {const char * ReplaceReturn28 = "flush"; SensorCall(102); return ReplaceReturn28;}
	case CEPH_CAP_OP_FLUSH_ACK: {const char * ReplaceReturn27 = "flush_ack"; SensorCall(103); return ReplaceReturn27;}
	case CEPH_CAP_OP_FLUSHSNAP: {const char * ReplaceReturn26 = "flushsnap"; SensorCall(104); return ReplaceReturn26;}
	case CEPH_CAP_OP_FLUSHSNAP_ACK: {const char * ReplaceReturn25 = "flushsnap_ack"; SensorCall(105); return ReplaceReturn25;}
	case CEPH_CAP_OP_RELEASE: {const char * ReplaceReturn24 = "release"; SensorCall(106); return ReplaceReturn24;}
	case CEPH_CAP_OP_RENEW: {const char * ReplaceReturn23 = "renew"; SensorCall(107); return ReplaceReturn23;}
	}
	{const char * ReplaceReturn22 = "???"; SensorCall(108); return ReplaceReturn22;}
}

const char *ceph_lease_op_name(int o)
{
	SensorCall(109);switch (o) {
	case CEPH_MDS_LEASE_REVOKE: {const char * ReplaceReturn21 = "revoke"; SensorCall(110); return ReplaceReturn21;}
	case CEPH_MDS_LEASE_RELEASE: {const char * ReplaceReturn20 = "release"; SensorCall(111); return ReplaceReturn20;}
	case CEPH_MDS_LEASE_RENEW: {const char * ReplaceReturn19 = "renew"; SensorCall(112); return ReplaceReturn19;}
	case CEPH_MDS_LEASE_REVOKE_ACK: {const char * ReplaceReturn18 = "revoke_ack"; SensorCall(113); return ReplaceReturn18;}
	}
	{const char * ReplaceReturn17 = "???"; SensorCall(114); return ReplaceReturn17;}
}

const char *ceph_snap_op_name(int o)
{
	SensorCall(115);switch (o) {
	case CEPH_SNAP_OP_UPDATE: {const char * ReplaceReturn16 = "update"; SensorCall(116); return ReplaceReturn16;}
	case CEPH_SNAP_OP_CREATE: {const char * ReplaceReturn15 = "create"; SensorCall(117); return ReplaceReturn15;}
	case CEPH_SNAP_OP_DESTROY: {const char * ReplaceReturn14 = "destroy"; SensorCall(118); return ReplaceReturn14;}
	case CEPH_SNAP_OP_SPLIT: {const char * ReplaceReturn13 = "split"; SensorCall(119); return ReplaceReturn13;}
	}
	{const char * ReplaceReturn12 = "???"; SensorCall(120); return ReplaceReturn12;}
}

const char *ceph_watch_event_name(int e)
{
	SensorCall(121);switch (e) {
	case CEPH_WATCH_EVENT_NOTIFY: {const char * ReplaceReturn11 = "notify"; SensorCall(122); return ReplaceReturn11;}
	case CEPH_WATCH_EVENT_NOTIFY_COMPLETE: {const char * ReplaceReturn10 = "notify_complete"; SensorCall(123); return ReplaceReturn10;}
	case CEPH_WATCH_EVENT_DISCONNECT: {const char * ReplaceReturn9 = "disconnect"; SensorCall(124); return ReplaceReturn9;}
	}
	{const char * ReplaceReturn8 = "???"; SensorCall(125); return ReplaceReturn8;}
}

const char *ceph_pool_op_name(int op)
{
	SensorCall(126);switch (op) {
	case POOL_OP_CREATE: {const char * ReplaceReturn7 = "create"; SensorCall(127); return ReplaceReturn7;}
	case POOL_OP_DELETE: {const char * ReplaceReturn6 = "delete"; SensorCall(128); return ReplaceReturn6;}
	case POOL_OP_AUID_CHANGE: {const char * ReplaceReturn5 = "auid change"; SensorCall(129); return ReplaceReturn5;}
	case POOL_OP_CREATE_SNAP: {const char * ReplaceReturn4 = "create snap"; SensorCall(130); return ReplaceReturn4;}
	case POOL_OP_DELETE_SNAP: {const char * ReplaceReturn3 = "delete snap"; SensorCall(131); return ReplaceReturn3;}
	case POOL_OP_CREATE_UNMANAGED_SNAP: {const char * ReplaceReturn2 = "create unmanaged snap"; SensorCall(132); return ReplaceReturn2;}
	case POOL_OP_DELETE_UNMANAGED_SNAP: {const char * ReplaceReturn1 = "delete unmanaged snap"; SensorCall(133); return ReplaceReturn1;}
	}
	{const char * ReplaceReturn0 = "???"; SensorCall(134); return ReplaceReturn0;}
}
