/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This file incorporates work covered by the following license notice:
 *
 *   Licensed to the Apache Software Foundation (ASF) under one or more
 *   contributor license agreements. See the NOTICE file distributed
 *   with this work for additional information regarding copyright
 *   ownership. The ASF licenses this file to you under the Apache
 *   License, Version 2.0 (the "License"); you may not use this file
 *   except in compliance with the License. You may obtain a copy of
 *   the License at http://www.apache.org/licenses/LICENSE-2.0 .
 */


#include <stdlib.h>
#include "var/tmp/sensor.h"
#include <comphelper/string.hxx>
#include <sal/log.hxx>
#include <tools/debug.hxx>
#include <i18nlangtag/mslangid.hxx>
#include <unotools/charclass.hxx>
#include <unotools/localedatawrapper.hxx>
#include <unotools/numberformatcodewrapper.hxx>
#include <rtl/instance.hxx>

#include <svl/zforlist.hxx>
#include <svl/zformat.hxx>
#include <unotools/digitgroupingiterator.hxx>

#include "zforscan.hxx"

#include <svl/nfsymbol.hxx>
using namespace svt;

const sal_Unicode cNoBreakSpace = 0xA0;
const sal_Unicode cNarrowNoBreakSpace = 0x202F;

namespace
{
    struct ImplEnglishColors
    {
        const OUString* operator()()
        {
            static const OUString aEnglishColors[NF_MAX_DEFAULT_COLORS] =
            {
                OUString( "BLACK" ),
                OUString( "BLUE" ),
                OUString( "GREEN" ),
                OUString( "CYAN" ),
                OUString( "RED" ),
                OUString( "MAGENTA" ),
                OUString( "BROWN" ),
                OUString( "GREY" ),
                OUString( "YELLOW" ),
                OUString( "WHITE" )
            };
            return &aEnglishColors[0];
        }
    };

    struct theEnglishColors
            : public rtl::StaticAggregate< const OUString, ImplEnglishColors> {};

}

ImpSvNumberformatScan::ImpSvNumberformatScan( SvNumberFormatter* pFormatterP )
    : eNewLnge(LANGUAGE_DONTKNOW)
    , eTmpLnge(LANGUAGE_DONTKNOW)
    , nCurrPos(-1)
{
    SensorCall();pFormatter = pFormatterP;
    bConvertMode = false;
    bConvertSystemToSystem = false;
    //! All keywords MUST be UPPERCASE!
    sKeyword[NF_KEY_E] =     "E";        // Exponent
    sKeyword[NF_KEY_AMPM] =  "AM/PM";    // AM/PM
    sKeyword[NF_KEY_AP] =    "A/P";      // AM/PM short
    sKeyword[NF_KEY_MI] =    "M";        // Minute
    sKeyword[NF_KEY_MMI] =   "MM";       // Minute 02
    sKeyword[NF_KEY_S] =     "S";        // Second
    sKeyword[NF_KEY_SS] =    "SS";       // Second 02
    sKeyword[NF_KEY_Q] =     "Q";        // Quarter short 'Q'
    sKeyword[NF_KEY_QQ] =    "QQ";       // Quarter long
    sKeyword[NF_KEY_NN] =    "NN";       // Day of week short
    sKeyword[NF_KEY_NNN] =   "NNN";      // Day of week long
    sKeyword[NF_KEY_NNNN] =  "NNNN";     // Day of week long incl. separator
    sKeyword[NF_KEY_WW] =    "WW";       // Week of year
    sKeyword[NF_KEY_CCC] =   "CCC";      // Currency abbreviation
    bKeywordsNeedInit = true;            // locale dependent keywords
    bCompatCurNeedInit = true;           // locale dependent compatibility currency strings

    StandardColor[0]  =  Color(COL_BLACK);
    StandardColor[1]  =  Color(COL_LIGHTBLUE);
    StandardColor[2]  =  Color(COL_LIGHTGREEN);
    StandardColor[3]  =  Color(COL_LIGHTCYAN);
    StandardColor[4]  =  Color(COL_LIGHTRED);
    StandardColor[5]  =  Color(COL_LIGHTMAGENTA);
    StandardColor[6]  =  Color(COL_BROWN);
    StandardColor[7]  =  Color(COL_GRAY);
    StandardColor[8]  =  Color(COL_YELLOW);
    StandardColor[9]  =  Color(COL_WHITE);

    pNullDate = new Date(30,12,1899);
    nStandardPrec = 2;

    sErrStr =  "###";
    Reset();
SensorCall();}

ImpSvNumberformatScan::~ImpSvNumberformatScan()
{
    SensorCall();delete pNullDate;
    Reset();
SensorCall();}

void ImpSvNumberformatScan::ChangeIntl()
{
    SensorCall();bKeywordsNeedInit = true;
    bCompatCurNeedInit = true;
    // may be initialized by InitSpecialKeyword()
    sKeyword[NF_KEY_TRUE].clear();
    sKeyword[NF_KEY_FALSE].clear();
SensorCall();}

void ImpSvNumberformatScan::InitSpecialKeyword( NfKeywordIndex eIdx ) const
{
    SensorCall();switch ( eIdx )
    {
    case NF_KEY_TRUE :
        const_cast<ImpSvNumberformatScan*>(this)->sKeyword[NF_KEY_TRUE] =
            pFormatter->GetCharClass()->uppercase( pFormatter->GetLocaleData()->getTrueWord() );
        SensorCall();if ( sKeyword[NF_KEY_TRUE].isEmpty() )
        {
            SAL_WARN( "svl.numbers", "InitSpecialKeyword: TRUE_WORD?" );
            const_cast<ImpSvNumberformatScan*>(this)->sKeyword[NF_KEY_TRUE] = "TRUE";
        }
        SensorCall();break;
    case NF_KEY_FALSE :
        const_cast<ImpSvNumberformatScan*>(this)->sKeyword[NF_KEY_FALSE] =
            pFormatter->GetCharClass()->uppercase( pFormatter->GetLocaleData()->getFalseWord() );
        SensorCall();if ( sKeyword[NF_KEY_FALSE].isEmpty() )
        {
            SAL_WARN( "svl.numbers", "InitSpecialKeyword: FALSE_WORD?" );
            const_cast<ImpSvNumberformatScan*>(this)->sKeyword[NF_KEY_FALSE] = "FALSE";
        }
        SensorCall();break;
    default:
        SAL_WARN( "svl.numbers", "InitSpecialKeyword: unknown request" );
    }
SensorCall();}

void ImpSvNumberformatScan::InitCompatCur() const
{
    SensorCall();ImpSvNumberformatScan* pThis = const_cast<ImpSvNumberformatScan*>(this);
    // currency symbol for old style ("automatic") compatibility format codes
    pFormatter->GetCompatibilityCurrency( pThis->sCurSymbol, pThis->sCurAbbrev );
    // currency symbol upper case
    pThis->sCurString = pFormatter->GetCharClass()->uppercase( sCurSymbol );
    bCompatCurNeedInit = false;
SensorCall();}

void ImpSvNumberformatScan::InitKeywords() const
{
    SensorCall();if ( !bKeywordsNeedInit )
        {/*1*/SensorCall();return ;/*2*/}
    SensorCall();const_cast<ImpSvNumberformatScan*>(this)->SetDependentKeywords();
    bKeywordsNeedInit = false;
SensorCall();}

/** Extract the name of General, Standard, Whatever, ignoring leading modifiers
    such as [NatNum1]. */
static OUString lcl_extractStandardGeneralName( const OUString & rCode )
{
    SensorCall();OUString aStr;
    const sal_Unicode* p = rCode.getStr();
    const sal_Unicode* const pStop = p + rCode.getLength();
    const sal_Unicode* pBeg = p;    // name begins here
    bool bMod = false;
    bool bDone = false;
    SensorCall();while (p < pStop && !bDone)
    {
        SensorCall();switch (*p)
        {
        case '[':
            SensorCall();bMod = true;
            SensorCall();break;
        case ']':
            SensorCall();if (bMod)
            {
                SensorCall();bMod = false;
                pBeg = p+1;
            }
            // else: would be a locale data error, easily to be spotted in
            // UI dialog
            SensorCall();break;
        case ';':
            SensorCall();if (!bMod)
            {
                SensorCall();bDone = true;
                --p;    // put back, increment by one follows
            }
            SensorCall();break;
        }
        SensorCall();++p;
        SensorCall();if (bMod)
        {
            SensorCall();pBeg = p;
        }
    }
    SensorCall();if (pBeg < p)
    {
        SensorCall();aStr = rCode.copy( pBeg - rCode.getStr(), p - pBeg);
    }
    {rtl::OUString  ReplaceReturn = aStr; SensorCall(); return ReplaceReturn;}
}

void ImpSvNumberformatScan::SetDependentKeywords()
{
    SensorCall();using namespace ::com::sun::star;
    using namespace ::com::sun::star::uno;

    const CharClass* pCharClass = pFormatter->GetCharClass();
    const LocaleDataWrapper* pLocaleData = pFormatter->GetLocaleData();
    // #80023# be sure to generate keywords for the loaded Locale, not for the
    // requested Locale, otherwise number format codes might not match
    const LanguageTag& rLoadedLocale = pLocaleData->getLoadedLanguageTag();
    LanguageType eLang = rLoadedLocale.getLanguageType( false);
    NumberFormatCodeWrapper aNumberFormatCode( pFormatter->GetComponentContext(),
            rLoadedLocale.getLocale() );

    i18n::NumberFormatCode aFormat = aNumberFormatCode.getFormatCode( NF_NUMBER_STANDARD );
    sNameStandardFormat = lcl_extractStandardGeneralName( aFormat.Code);
    sKeyword[NF_KEY_GENERAL] = pCharClass->uppercase( sNameStandardFormat );

    // preset new calendar keywords
    sKeyword[NF_KEY_AAA] =   "AAA";
    sKeyword[NF_KEY_AAAA] =  "AAAA";
    sKeyword[NF_KEY_EC] =    "E";
    sKeyword[NF_KEY_EEC] =   "EE";
    sKeyword[NF_KEY_G] =     "G";
    sKeyword[NF_KEY_GG] =    "GG";
    sKeyword[NF_KEY_GGG] =   "GGG";
    sKeyword[NF_KEY_R] =     "R";
    sKeyword[NF_KEY_RR] =    "RR";

    // Thai T NatNum special. Other locale's small letter 't' results in upper
    // case comparison not matching but length does in conversion mode. Ugly.
    SensorCall();if (eLang == LANGUAGE_THAI)
    {
        sKeyword[NF_KEY_THAI_T] = "T";
    }
    else
    {
        sKeyword[NF_KEY_THAI_T] = "t";
    }
    SensorCall();switch ( eLang )
    {
    case LANGUAGE_GERMAN:
    case LANGUAGE_GERMAN_SWISS:
    case LANGUAGE_GERMAN_AUSTRIAN:
    case LANGUAGE_GERMAN_LUXEMBOURG:
    case LANGUAGE_GERMAN_LIECHTENSTEIN:
        //! all capital letters
        sKeyword[NF_KEY_M] =         "M";     // month 1
        sKeyword[NF_KEY_MM] =        "MM";    // month 01
        sKeyword[NF_KEY_MMM] =       "MMM";   // month Jan
        sKeyword[NF_KEY_MMMM] =      "MMMM";  // month Januar
        sKeyword[NF_KEY_MMMMM] =     "MMMMM"; // month J
        sKeyword[NF_KEY_H] =         "H";     // hour 2
        sKeyword[NF_KEY_HH] =        "HH";    // hour 02
        sKeyword[NF_KEY_D] =         "T";
        sKeyword[NF_KEY_DD] =        "TT";
        sKeyword[NF_KEY_DDD] =       "TTT";
        sKeyword[NF_KEY_DDDD] =      "TTTT";
        sKeyword[NF_KEY_YY] =        "JJ";
        sKeyword[NF_KEY_YYYY] =      "JJJJ";
        sKeyword[NF_KEY_BOOLEAN] =   "LOGISCH";
        sKeyword[NF_KEY_COLOR] =     "FARBE";
        sKeyword[NF_KEY_BLACK] =     "SCHWARZ";
        sKeyword[NF_KEY_BLUE] =      "BLAU";
        sKeyword[NF_KEY_GREEN] = OUString( "GR" "\xDC" "N", 4, RTL_TEXTENCODING_ISO_8859_1 );
        sKeyword[NF_KEY_CYAN] =      "CYAN";
        sKeyword[NF_KEY_RED] =       "ROT";
        sKeyword[NF_KEY_MAGENTA] =   "MAGENTA";
        sKeyword[NF_KEY_BROWN] =     "BRAUN";
        sKeyword[NF_KEY_GREY] =      "GRAU";
        sKeyword[NF_KEY_YELLOW] =    "GELB";
        sKeyword[NF_KEY_WHITE] =     "WEISS";
        SensorCall();break;
    default:
        // day
        SensorCall();switch ( eLang )
        {
        case LANGUAGE_ITALIAN:
        case LANGUAGE_ITALIAN_SWISS:
            sKeyword[NF_KEY_D] = "G";
            sKeyword[NF_KEY_DD] = "GG";
            sKeyword[NF_KEY_DDD] = "GGG";
            sKeyword[NF_KEY_DDDD] = "GGGG";
            // must exchange the era code, same as Xcl
            sKeyword[NF_KEY_G] = "X";
            sKeyword[NF_KEY_GG] = "XX";
            sKeyword[NF_KEY_GGG] = "XXX";
            SensorCall();break;
        case LANGUAGE_FRENCH:
        case LANGUAGE_FRENCH_BELGIAN:
        case LANGUAGE_FRENCH_CANADIAN:
        case LANGUAGE_FRENCH_SWISS:
        case LANGUAGE_FRENCH_LUXEMBOURG:
        case LANGUAGE_FRENCH_MONACO:
            sKeyword[NF_KEY_D] = "J";
            sKeyword[NF_KEY_DD] = "JJ";
            sKeyword[NF_KEY_DDD] = "JJJ";
            sKeyword[NF_KEY_DDDD] = "JJJJ";
            SensorCall();break;
        case LANGUAGE_FINNISH:
            sKeyword[NF_KEY_D] = "P";
            sKeyword[NF_KEY_DD] = "PP";
            sKeyword[NF_KEY_DDD] = "PPP";
            sKeyword[NF_KEY_DDDD] = "PPPP";
            SensorCall();break;
        default:
            sKeyword[NF_KEY_D] = "D";
            sKeyword[NF_KEY_DD] = "DD";
            sKeyword[NF_KEY_DDD] = "DDD";
            sKeyword[NF_KEY_DDDD] = "DDDD";
        }
        // month
        SensorCall();switch ( eLang )
        {
        case LANGUAGE_FINNISH:
            sKeyword[NF_KEY_M] = "K";
            sKeyword[NF_KEY_MM] = "KK";
            sKeyword[NF_KEY_MMM] = "KKK";
            sKeyword[NF_KEY_MMMM] = "KKKK";
            sKeyword[NF_KEY_MMMMM] = "KKKKK";
            SensorCall();break;
        default:
            sKeyword[NF_KEY_M] = "M";
            sKeyword[NF_KEY_MM] = "MM";
            sKeyword[NF_KEY_MMM] = "MMM";
            sKeyword[NF_KEY_MMMM] = "MMMM";
            sKeyword[NF_KEY_MMMMM] = "MMMMM";
        }
        // year
        SensorCall();switch ( eLang )
        {
        case LANGUAGE_ITALIAN:
        case LANGUAGE_ITALIAN_SWISS:
        case LANGUAGE_FRENCH:
        case LANGUAGE_FRENCH_BELGIAN:
        case LANGUAGE_FRENCH_CANADIAN:
        case LANGUAGE_FRENCH_SWISS:
        case LANGUAGE_FRENCH_LUXEMBOURG:
        case LANGUAGE_FRENCH_MONACO:
        case LANGUAGE_PORTUGUESE:
        case LANGUAGE_PORTUGUESE_BRAZILIAN:
        case LANGUAGE_SPANISH_MODERN:
        case LANGUAGE_SPANISH_DATED:
        case LANGUAGE_SPANISH_MEXICAN:
        case LANGUAGE_SPANISH_GUATEMALA:
        case LANGUAGE_SPANISH_COSTARICA:
        case LANGUAGE_SPANISH_PANAMA:
        case LANGUAGE_SPANISH_DOMINICAN_REPUBLIC:
        case LANGUAGE_SPANISH_VENEZUELA:
        case LANGUAGE_SPANISH_COLOMBIA:
        case LANGUAGE_SPANISH_PERU:
        case LANGUAGE_SPANISH_ARGENTINA:
        case LANGUAGE_SPANISH_ECUADOR:
        case LANGUAGE_SPANISH_CHILE:
        case LANGUAGE_SPANISH_URUGUAY:
        case LANGUAGE_SPANISH_PARAGUAY:
        case LANGUAGE_SPANISH_BOLIVIA:
        case LANGUAGE_SPANISH_EL_SALVADOR:
        case LANGUAGE_SPANISH_HONDURAS:
        case LANGUAGE_SPANISH_NICARAGUA:
        case LANGUAGE_SPANISH_PUERTO_RICO:
            sKeyword[NF_KEY_YY] = "AA";
            sKeyword[NF_KEY_YYYY] = "AAAA";
            // must exchange the day of week name code, same as Xcl
            sKeyword[NF_KEY_AAA] =   "OOO";
            sKeyword[NF_KEY_AAAA] =  "OOOO";
            SensorCall();break;
        case LANGUAGE_DUTCH:
        case LANGUAGE_DUTCH_BELGIAN:
            sKeyword[NF_KEY_YY] = "JJ";
            sKeyword[NF_KEY_YYYY] = "JJJJ";
            SensorCall();break;
        case LANGUAGE_FINNISH:
            sKeyword[NF_KEY_YY] = "VV";
            sKeyword[NF_KEY_YYYY] = "VVVV";
            SensorCall();break;
        default:
            sKeyword[NF_KEY_YY] = "YY";
            sKeyword[NF_KEY_YYYY] = "YYYY";
        }
        // hour
        SensorCall();switch ( eLang )
        {
        case LANGUAGE_DUTCH:
        case LANGUAGE_DUTCH_BELGIAN:
            sKeyword[NF_KEY_H] = "U";
            sKeyword[NF_KEY_HH] = "UU";
            SensorCall();break;
        case LANGUAGE_FINNISH:
        case LANGUAGE_SWEDISH:
        case LANGUAGE_SWEDISH_FINLAND:
        case LANGUAGE_DANISH:
        case LANGUAGE_NORWEGIAN:
        case LANGUAGE_NORWEGIAN_BOKMAL:
        case LANGUAGE_NORWEGIAN_NYNORSK:
            sKeyword[NF_KEY_H] = "T";
            sKeyword[NF_KEY_HH] = "TT";
            SensorCall();break;
        default:
            sKeyword[NF_KEY_H] = "H";
            sKeyword[NF_KEY_HH] = "HH";
        }
        // boolean
        sKeyword[NF_KEY_BOOLEAN] = "BOOLEAN";
        // colours
        sKeyword[NF_KEY_COLOR] =     "COLOR";
        sKeyword[NF_KEY_BLACK] =     "BLACK";
        sKeyword[NF_KEY_BLUE] =      "BLUE";
        sKeyword[NF_KEY_GREEN] =     "GREEN";
        sKeyword[NF_KEY_CYAN] =      "CYAN";
        sKeyword[NF_KEY_RED] =       "RED";
        sKeyword[NF_KEY_MAGENTA] =   "MAGENTA";
        sKeyword[NF_KEY_BROWN] =     "BROWN";
        sKeyword[NF_KEY_GREY] =      "GREY";
        sKeyword[NF_KEY_YELLOW] =    "YELLOW";
        sKeyword[NF_KEY_WHITE] =     "WHITE";
        SensorCall();break;
    }

    // boolean keyords
    SensorCall();InitSpecialKeyword( NF_KEY_TRUE );
    InitSpecialKeyword( NF_KEY_FALSE );

    // compatibility currency strings
    InitCompatCur();
SensorCall();}

void ImpSvNumberformatScan::ChangeNullDate(sal_uInt16 nDay, sal_uInt16 nMonth, sal_uInt16 nYear)
{
    SensorCall();if ( pNullDate )
        {/*3*/SensorCall();*pNullDate = Date(nDay, nMonth, nYear);/*4*/}
    else
        {/*5*/SensorCall();pNullDate = new Date(nDay, nMonth, nYear);/*6*/}
SensorCall();}

void ImpSvNumberformatScan::ChangeStandardPrec(sal_uInt16 nPrec)
{
    SensorCall();nStandardPrec = nPrec;
SensorCall();}

Color* ImpSvNumberformatScan::GetColor(OUString& sStr)
{
    SensorCall();OUString sString = pFormatter->GetCharClass()->uppercase(sStr);
    const NfKeywordTable & rKeyword = GetKeywords();
    size_t i = 0;
    SensorCall();while (i < NF_MAX_DEFAULT_COLORS && sString != rKeyword[NF_KEY_FIRSTCOLOR+i] )
    {
        SensorCall();i++;
    }
    SensorCall();if ( i >= NF_MAX_DEFAULT_COLORS )
    {
        SensorCall();const OUString* pEnglishColors = theEnglishColors::get();
        size_t j = 0;
        SensorCall();while ( j < NF_MAX_DEFAULT_COLORS && sString != pEnglishColors[j] )
        {
            SensorCall();++j;
        }
        SensorCall();if ( j < NF_MAX_DEFAULT_COLORS )
        {
            SensorCall();i = j;
        }
    }

    SensorCall();Color* pResult = NULL;
    SensorCall();if (i >= NF_MAX_DEFAULT_COLORS)
    {
        SensorCall();const OUString& rColorWord = rKeyword[NF_KEY_COLOR];
        SensorCall();if (sString.startsWith(rColorWord))
        {
            SensorCall();sal_Int32 nPos = rColorWord.getLength();
            sStr = sStr.copy(nPos);
            sStr = comphelper::string::strip(sStr, ' ');
            SensorCall();if (bConvertMode)
            {
                SensorCall();pFormatter->ChangeIntl(eNewLnge);
                sStr = GetKeywords()[NF_KEY_COLOR] + sStr; // Color -> FARBE
                pFormatter->ChangeIntl(eTmpLnge);
            }
            else
            {
                sStr = rColorWord + sStr;
            }
            SensorCall();sString = sString.copy(nPos);
            sString = comphelper::string::strip(sString, ' ');

            SensorCall();if ( CharClass::isAsciiNumeric( sString ) )
            {
                SensorCall();long nIndex = sString.toInt32();
                SensorCall();if (nIndex > 0 && nIndex <= 64)
                {
                    SensorCall();pResult = pFormatter->GetUserDefColor((sal_uInt16)nIndex-1);
                }
            }
        }
    }
    else
    {
        SensorCall();sStr.clear();
        SensorCall();if (bConvertMode)
        {
            SensorCall();pFormatter->ChangeIntl(eNewLnge);
            sStr = GetKeywords()[NF_KEY_FIRSTCOLOR+i]; // red -> rot
            pFormatter->ChangeIntl(eTmpLnge);
        }
        else
        {
            SensorCall();sStr = rKeyword[NF_KEY_FIRSTCOLOR+i];
        }
        SensorCall();pResult = &(StandardColor[i]);
    }
    {Color * ReplaceReturn = pResult; SensorCall(); return ReplaceReturn;}
}

short ImpSvNumberformatScan::GetKeyWord( const OUString& sSymbol, sal_Int32 nPos )
{
    SensorCall();OUString sString = pFormatter->GetCharClass()->uppercase( sSymbol, nPos, sSymbol.getLength() - nPos );
    const NfKeywordTable & rKeyword = GetKeywords();
    // #77026# for the Xcl perverts: the GENERAL keyword is recognized anywhere
    SensorCall();if ( sString.startsWith( rKeyword[NF_KEY_GENERAL] ))
    {
        {short  ReplaceReturn = NF_KEY_GENERAL; SensorCall(); return ReplaceReturn;}
    }
    //! MUST be a reverse search to find longer strings first
    SensorCall();short i = NF_KEYWORD_ENTRIES_COUNT-1;
    bool bFound = false;
    SensorCall();for ( ; i > NF_KEY_LASTKEYWORD_SO5; --i )
    {
        SensorCall();bFound = sString.startsWith(rKeyword[i]);
        SensorCall();if ( bFound )
        {
            SensorCall();break;
        }
    }
    // new keywords take precedence over old keywords
    SensorCall();if ( !bFound )
    {
        // skip the gap of colors et al between new and old keywords and search on
        SensorCall();i = NF_KEY_LASTKEYWORD;
        SensorCall();while ( i > 0 && sString.indexOf(rKeyword[i]) != 0 )
        {
            SensorCall();i--;
        }
        SensorCall();if ( i > NF_KEY_LASTOLDKEYWORD && sString != rKeyword[i] )
        {
            // found something, but maybe it's something else?
            // e.g. new NNN is found in NNNN, for NNNN we must search on
            SensorCall();short j = i - 1;
            SensorCall();while ( j > 0 && sString.indexOf(rKeyword[j]) != 0 )
            {
                SensorCall();j--;
            }
            SensorCall();if ( j && rKeyword[j].getLength() > rKeyword[i].getLength() )
            {
                {short  ReplaceReturn = j; SensorCall(); return ReplaceReturn;}
            }
        }
    }
    // The Thai T NatNum modifier during Xcl import.
    SensorCall();if (i == 0 && bConvertMode &&
        sString[0] == 'T' &&
        eTmpLnge == LANGUAGE_ENGLISH_US &&
        MsLangId::getRealLanguage( eNewLnge) == LANGUAGE_THAI)
    {
        SensorCall();i = NF_KEY_THAI_T;
    }
    {short  ReplaceReturn = i; SensorCall(); return ReplaceReturn;} // 0 => not found
}

/**
 * Next_Symbol
 *
 * Splits up the input for further processing (by the Turing machine).
 *
 * Starting state = SsStar
 *
 * ---------------+-------------------+---------------------------+---------------
 * Old state      | Character read    | Event                     | New state
 * ---------------+-------------------+---------------------------+---------------
 * SsStart        | Character         | Symbol = Character        | SsGetWord
 *                |    "              | Type = String             | SsGetString
 *                |    \              | Type = String             | SsGetChar
 *                |    *              | Type = Star               | SsGetStar
 *                |    _              | Type = Blank              | SsGetBlank
 *                | @ # 0 ? / . , % [ | Symbol = Character;       |
 *                | ] ' Blank         | Type = Control character  | SsStop
 *                | $ - + ( ) :       | Type  = String;           |
 *                | Else              | Symbol = Character        | SsStop
 * ---------------|-------------------+---------------------------+---------------
 * SsGetChar      | Else              | Symbol = Character        | SsStop
 * ---------------+-------------------+---------------------------+---------------
 * GetString      | "                 |                           | SsStop
 *                | Else              | Symbol += Character       | GetString
 * ---------------+-------------------+---------------------------+---------------
 * SsGetWord      | Character         | Symbol += Character       |
 *                | + -        (E+ E-)| Symbol += Character       | SsStop
 *                | /          (AM/PM)| Symbol += Character       |
 *                | Else              | Pos--, if Key Type = Word | SsStop
 * ---------------+-------------------+---------------------------+---------------
 * SsGetStar      | Else              | Symbol += Character       | SsStop
 *                |                   | Mark special case *       |
 * ---------------+-------------------+---------------------------+---------------
 * SsGetBlank     | Else              | Symbol + =Character       | SsStop
 *                |                   | Mark special case  _      |
 * ---------------------------------------------------------------+--------------
 *
 * If we recognize a keyword in the state SsGetWord (even as the symbol's start text)
 * we write back the rest of the characters!
 */

enum ScanState
{
    SsStop      = 0,
    SsStart     = 1,
    SsGetChar   = 2,
    SsGetString = 3,
    SsGetWord   = 4,
    SsGetStar   = 5,
    SsGetBlank  = 6
};

short ImpSvNumberformatScan::Next_Symbol( const OUString& rStr,
                                          sal_Int32& nPos,
                                          OUString& sSymbol )
{
    SensorCall();if ( bKeywordsNeedInit )
    {
        SensorCall();InitKeywords();
    }
    SensorCall();const CharClass* pChrCls = pFormatter->GetCharClass();
    const LocaleDataWrapper* pLoc = pFormatter->GetLocaleData();
    const sal_Int32 nStart = nPos;
    short eType = 0;
    ScanState eState = SsStart;
    sSymbol.clear();
    SensorCall();while ( nPos < rStr.getLength() && eState != SsStop )
    {
        SensorCall();sal_Unicode cToken = rStr[nPos++];
        SensorCall();switch (eState)
        {
        case SsStart:
            // Fetch any currency longer than one character and don't get
            // confused later on by "E/" or other combinations of letters
            // and meaningful symbols. Necessary for old automatic currency.
            // #96158# But don't do it if we're starting a "[...]" section,
            // for example a "[$...]" new currency symbol to not parse away
            // "$U" (symbol) of "[$UYU]" (abbreviation).
            SensorCall();if ( nCurrPos >= 0 && sCurString.getLength() > 1 &&
                 nPos-1 + sCurString.getLength() <= rStr.getLength() &&
                 !(nPos > 1 && rStr[nPos-2] == '[') )
            {
                SensorCall();OUString aTest = pChrCls->uppercase( rStr.copy( nPos-1, sCurString.getLength() ) );
                SensorCall();if ( aTest == sCurString )
                {
                    SensorCall();sSymbol = rStr.copy( --nPos, sCurString.getLength() );
                    nPos = nPos + sSymbol.getLength();
                    eState = SsStop;
                    eType = NF_SYMBOLTYPE_STRING;
                    {short  ReplaceReturn = eType; SensorCall(); return ReplaceReturn;}
                }
            }
            SensorCall();switch (cToken)
            {
            case '#':
            case '0':
            case '?':
            case '%':
            case '@':
            case '[':
            case ']':
            case ',':
            case '.':
            case '/':
            case '\'':
            case ' ':
            case ':':
            case '-':
                SensorCall();eType = NF_SYMBOLTYPE_DEL;
                sSymbol += OUString(cToken);
                eState = SsStop;
                SensorCall();break;
            case '*':
                SensorCall();eType = NF_SYMBOLTYPE_STAR;
                sSymbol += OUString(cToken);
                eState = SsGetStar;
                SensorCall();break;
            case '_':
                SensorCall();eType = NF_SYMBOLTYPE_BLANK;
                sSymbol += OUString(cToken);
                eState = SsGetBlank;
                SensorCall();break;
            case '"':
                SensorCall();eType = NF_SYMBOLTYPE_STRING;
                eState = SsGetString;
                sSymbol += OUString(cToken);
                SensorCall();break;
            case '\\':
                SensorCall();eType = NF_SYMBOLTYPE_STRING;
                eState = SsGetChar;
                sSymbol += OUString(cToken);
                SensorCall();break;
            case '$':
            case '+':
            case '(':
            case ')':
                SensorCall();eType = NF_SYMBOLTYPE_STRING;
                eState = SsStop;
                sSymbol += OUString(cToken);
                SensorCall();break;
            default :
                SensorCall();if (StringEqualsChar( pFormatter->GetNumDecimalSep(), cToken) ||
                    StringEqualsChar( pFormatter->GetNumThousandSep(), cToken) ||
                    StringEqualsChar( pFormatter->GetDateSep(), cToken) ||
                    StringEqualsChar( pLoc->getTimeSep(), cToken) ||
                    StringEqualsChar( pLoc->getTime100SecSep(), cToken))
                {
                    // Another separator than pre-known ASCII
                    SensorCall();eType = NF_SYMBOLTYPE_DEL;
                    sSymbol += OUString(cToken);
                    eState = SsStop;
                }
                else {/*7*/SensorCall();if ( pChrCls->isLetter( rStr, nPos-1 ) )
                {
                    SensorCall();short nTmpType = GetKeyWord( rStr, nPos-1 );
                    SensorCall();if ( nTmpType )
                    {
                        SensorCall();bool bCurrency = false;
                        // "Automatic" currency may start with keyword,
                        // like "R" (Rand) and 'R' (era)
                        SensorCall();if ( nCurrPos >= 0 &&
                             nPos-1 + sCurString.getLength() <= rStr.getLength() &&
                             sCurString.startsWith( sKeyword[nTmpType] ) )
                        {
                            SensorCall();OUString aTest = pChrCls->uppercase( rStr.copy( nPos-1, sCurString.getLength() ) );
                            SensorCall();if ( aTest == sCurString )
                            {
                                SensorCall();bCurrency = true;
                            }
                        }
                        SensorCall();if ( bCurrency )
                        {
                            SensorCall();eState = SsGetWord;
                            sSymbol += OUString(cToken);
                        }
                        else
                        {
                            SensorCall();eType = nTmpType;
                            sal_Int32 nLen = sKeyword[eType].getLength();
                            sSymbol = rStr.copy( nPos-1, nLen );
                            SensorCall();if ((eType == NF_KEY_E || IsAmbiguousE(eType)) && nPos < rStr.getLength())
                            {
                                SensorCall();sal_Unicode cNext = rStr[nPos];
                                SensorCall();switch ( cNext )
                                {
                                case '+' :
                                case '-' :  // E+ E- combine to one symbol
                                    SensorCall();sSymbol += OUString(cNext);
                                    eType = NF_KEY_E;
                                    nPos++;
                                    SensorCall();break;
                                case '0' :
                                case '#' :  // scientific E without sign
                                    SensorCall();eType = NF_KEY_E;
                                    SensorCall();break;
                                }
                            }
                            SensorCall();nPos--;
                            nPos = nPos + nLen;
                            eState = SsStop;
                        }
                    }
                    else
                    {
                        SensorCall();eState = SsGetWord;
                        sSymbol += OUString(cToken);
                    }
                }
                else
                {
                    SensorCall();eType = NF_SYMBOLTYPE_STRING;
                    eState = SsStop;
                    sSymbol += OUString(cToken);
                ;/*8*/}}
                SensorCall();break;
            }
            SensorCall();break;
        case SsGetChar:
            SensorCall();sSymbol += OUString(cToken);
            eState = SsStop;
            SensorCall();break;
        case SsGetString:
            SensorCall();if (cToken == '"')
            {
                SensorCall();eState = SsStop;
            }
            SensorCall();sSymbol += OUString(cToken);
            SensorCall();break;
        case SsGetWord:
            SensorCall();if ( pChrCls->isLetter( rStr, nPos-1 ) )
            {
                SensorCall();short nTmpType = GetKeyWord( rStr, nPos-1 );
                SensorCall();if ( nTmpType )
                {
                    // beginning of keyword, stop scan and put back
                    SensorCall();eType = NF_SYMBOLTYPE_STRING;
                    eState = SsStop;
                    nPos--;
                }
                else
                {
                    SensorCall();sSymbol += OUString(cToken);
                }
            }
            else
            {
                SensorCall();bool bDontStop = false;
                sal_Unicode cNext;
                SensorCall();switch (cToken)
                {
                case '/': // AM/PM, A/P
                    SensorCall();cNext = rStr[nPos];
                    SensorCall();if ( cNext == 'P' || cNext == 'p' )
                    {
                        SensorCall();sal_Int32 nLen = sSymbol.getLength();
                        SensorCall();if ( 1 <= nLen &&
                             (sSymbol[0] == 'A' || sSymbol[0] == 'a') &&
                             (nLen == 1 ||
                              (nLen == 2 && (sSymbol[1] == 'M' || sSymbol[1] == 'm')
                               && (rStr[nPos + 1] == 'M' || rStr[nPos + 1] == 'm'))))
                        {
                            SensorCall();sSymbol += OUString(cToken);
                            bDontStop = true;
                        }
                    }
                    SensorCall();break;
                }
                // anything not recognized will stop the scan
                SensorCall();if ( eState != SsStop && !bDontStop )
                {
                    SensorCall();eState = SsStop;
                    nPos--;
                    eType = NF_SYMBOLTYPE_STRING;
                }
            }
            SensorCall();break;
        case SsGetStar:
            SensorCall();eState = SsStop;
            sSymbol += OUString(cToken);
            nRepPos = (nPos - nStart) - 1; // every time > 0!!
            SensorCall();break;
        case SsGetBlank:
            SensorCall();eState = SsStop;
            sSymbol += OUString(cToken);
            SensorCall();break;
        default:
            SensorCall();break;
        } // of switch
    } // of while
    SensorCall();if (eState == SsGetWord)
    {
        SensorCall();eType = NF_SYMBOLTYPE_STRING;
    }
    {short  ReplaceReturn = eType; SensorCall(); return ReplaceReturn;}
}

sal_Int32 ImpSvNumberformatScan::Symbol_Division(const OUString& rString)
{
    SensorCall();nCurrPos = -1;
    // Do we have some sort of currency?
    OUString sString = pFormatter->GetCharClass()->uppercase(rString);
    sal_Int32 nCPos = 0;
    SensorCall();while (nCPos >= 0 && nCPos < sString.getLength())
    {
        SensorCall();nCPos = sString.indexOf(GetCurString(),nCPos);
        SensorCall();if (nCPos >= 0)
        {
            // In Quotes?
            SensorCall();sal_Int32 nQ = SvNumberformat::GetQuoteEnd( sString, nCPos );
            SensorCall();if ( nQ < 0 )
            {
                SensorCall();sal_Unicode c;
                SensorCall();if ( nCPos == 0 ||
                    ((c = sString[nCPos-1]) != '"'
                            && c != '\\') ) // dm can be protected by "dm \d
                {
                    SensorCall();nCurrPos = nCPos;
                    nCPos = -1;
                }
                else
                {
                    SensorCall();nCPos++; // Continue search
                }
            }
            else
            {
                SensorCall();nCPos = nQ + 1; // Continue search
            }
        }
    }
    SensorCall();nAnzStrings = 0;
    bool bStar = false; // Is set on detecting '*'
    Reset();

    sal_Int32 nPos = 0;
    const sal_Int32 nLen = rString.getLength();
    SensorCall();while (nPos < nLen && nAnzStrings < NF_MAX_FORMAT_SYMBOLS)
    {
        SensorCall();nTypeArray[nAnzStrings] = Next_Symbol(rString, nPos, sStrArray[nAnzStrings]);
        SensorCall();if (nTypeArray[nAnzStrings] == NF_SYMBOLTYPE_STAR)
        { // Monitoring the '*'
            SensorCall();if (bStar)
            {
                {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;} // Error: double '*'
            }
            else
            {
                // Valid only if there is a character following, else we are
                // at the end of a code that does not have a fill character
                // (yet?).
                SensorCall();if (sStrArray[nAnzStrings].getLength() < 2)
                    {/*9*/{sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}/*10*/}
                SensorCall();bStar = true;
            }
        }
        SensorCall();nAnzStrings++;
    }

    {sal_Int32  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;} // 0 => ok
}

void ImpSvNumberformatScan::SkipStrings(sal_uInt16& i, sal_Int32& nPos)
{
    SensorCall();while (i < nAnzStrings && (   nTypeArray[i] == NF_SYMBOLTYPE_STRING
                               || nTypeArray[i] == NF_SYMBOLTYPE_BLANK
                               || nTypeArray[i] == NF_SYMBOLTYPE_STAR) )
    {
        SensorCall();nPos = nPos + sStrArray[i].getLength();
        i++;
    }
SensorCall();}

sal_uInt16 ImpSvNumberformatScan::PreviousKeyword(sal_uInt16 i)
{
    SensorCall();short res = 0;
    SensorCall();if (i > 0 && i < nAnzStrings)
    {
        SensorCall();i--;
        SensorCall();while (i > 0 && nTypeArray[i] <= 0)
        {
            SensorCall();i--;
        }
        SensorCall();if (nTypeArray[i] > 0)
        {
            SensorCall();res = nTypeArray[i];
        }
    }
    {sal_uInt16  ReplaceReturn = res; SensorCall(); return ReplaceReturn;}
}

sal_uInt16 ImpSvNumberformatScan::NextKeyword(sal_uInt16 i)
{
    SensorCall();short res = 0;
    SensorCall();if (i < nAnzStrings-1)
    {
        SensorCall();i++;
        SensorCall();while (i < nAnzStrings-1 && nTypeArray[i] <= 0)
        {
            SensorCall();i++;
        }
        SensorCall();if (nTypeArray[i] > 0)
        {
            SensorCall();res = nTypeArray[i];
        }
    }
    {sal_uInt16  ReplaceReturn = res; SensorCall(); return ReplaceReturn;}
}

short ImpSvNumberformatScan::PreviousType( sal_uInt16 i )
{
    SensorCall();if ( i > 0 && i < nAnzStrings )
    {
        SensorCall();do
        {
            SensorCall();i--;
        }
        while ( i > 0 && nTypeArray[i] == NF_SYMBOLTYPE_EMPTY );
        {short  ReplaceReturn = nTypeArray[i]; SensorCall(); return ReplaceReturn;}
    }
    {short  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

sal_Unicode ImpSvNumberformatScan::PreviousChar(sal_uInt16 i)
{
    SensorCall();sal_Unicode res = ' ';
    SensorCall();if (i > 0 && i < nAnzStrings)
    {
        SensorCall();i--;
        SensorCall();while (i > 0 &&
               ( nTypeArray[i] == NF_SYMBOLTYPE_EMPTY ||
                 nTypeArray[i] == NF_SYMBOLTYPE_STRING ||
                 nTypeArray[i] == NF_SYMBOLTYPE_STAR ||
                 nTypeArray[i] == NF_SYMBOLTYPE_BLANK ))
        {
            SensorCall();i--;
        }
        SensorCall();if (sStrArray[i].getLength() > 0)
        {
            SensorCall();res = sStrArray[i][sStrArray[i].getLength()-1];
        }
    }
    {sal_Unicode  ReplaceReturn = res; SensorCall(); return ReplaceReturn;}
}

sal_Unicode ImpSvNumberformatScan::NextChar(sal_uInt16 i)
{
    SensorCall();sal_Unicode res = ' ';
    SensorCall();if (i < nAnzStrings-1)
    {
        SensorCall();i++;
        SensorCall();while (i < nAnzStrings-1 &&
               ( nTypeArray[i] == NF_SYMBOLTYPE_EMPTY ||
                 nTypeArray[i] == NF_SYMBOLTYPE_STRING ||
                 nTypeArray[i] == NF_SYMBOLTYPE_STAR ||
                 nTypeArray[i] == NF_SYMBOLTYPE_BLANK))
        {
            SensorCall();i++;
        }
        SensorCall();if (sStrArray[i].getLength() > 0)
        {
            SensorCall();res = sStrArray[i][0];
        }
    }
    {sal_Unicode  ReplaceReturn = res; SensorCall(); return ReplaceReturn;}
}

bool ImpSvNumberformatScan::IsLastBlankBeforeFrac(sal_uInt16 i)
{
    SensorCall();bool res = true;
    SensorCall();if (i < nAnzStrings-1)
    {
        SensorCall();bool bStop = false;
        i++;
        SensorCall();while (i < nAnzStrings-1 && !bStop)
        {
            SensorCall();i++;
            SensorCall();if ( nTypeArray[i] == NF_SYMBOLTYPE_DEL &&
                 sStrArray[i][0] == '/')
            {
                SensorCall();bStop = true;
            }
            else {/*11*/SensorCall();if ( nTypeArray[i] == NF_SYMBOLTYPE_DEL &&
                      sStrArray[i][0] == ' ')
            {
                SensorCall();res = false;
            ;/*12*/}}
        }
        SensorCall();if (!bStop) // no '/'{
        {
            SensorCall();res = false;
        }
    }
    else
    {
        SensorCall();res = false; // no '/' any more
    }
    {_Bool  ReplaceReturn = res; SensorCall(); return ReplaceReturn;}
}

void ImpSvNumberformatScan::Reset()
{
    SensorCall();nAnzStrings = 0;
    nAnzResStrings = 0;
    eScannedType = css::util::NumberFormat::UNDEFINED;
    nRepPos = 0;
    bExp = false;
    bThousand = false;
    nThousand = 0;
    bDecSep = false;
    nDecPos = (sal_uInt16)-1;
    nExpPos = (sal_uInt16)-1;
    nBlankPos = (sal_uInt16)-1;
    nCntPre = 0;
    nCntPost = 0;
    nCntExp = 0;
    bFrac = false;
    bBlank = false;
    nNatNumModifier = 0;
SensorCall();}

bool ImpSvNumberformatScan::Is100SecZero( sal_uInt16 i, bool bHadDecSep )
{
    SensorCall();sal_uInt16 nIndexPre = PreviousKeyword( i );
    {_Bool  ReplaceReturn = (nIndexPre == NF_KEY_S || nIndexPre == NF_KEY_SS) &&
            (bHadDecSep ||
             ( i > 0 && nTypeArray[i-1] == NF_SYMBOLTYPE_STRING)); SensorCall(); return ReplaceReturn;}
              // SS"any"00  take "any" as a valid decimal separator
}

sal_Int32 ImpSvNumberformatScan::ScanType()
{
    SensorCall();const LocaleDataWrapper* pLoc = pFormatter->GetLocaleData();

    sal_Int32 nPos = 0;
    sal_uInt16 i = 0;
    short eNewType;
    bool bMatchBracket = false;
    bool bHaveGeneral = false; // if General/Standard encountered

    SkipStrings(i, nPos);
    SensorCall();while (i < nAnzStrings)
    {
        SensorCall();if (nTypeArray[i] > 0)
        {   // keyword
            SensorCall();sal_uInt16 nIndexPre;
            sal_uInt16 nIndexNex;
            sal_Unicode cChar;

            SensorCall();switch (nTypeArray[i])
            {
            case NF_KEY_E:                          // E
                SensorCall();eNewType = css::util::NumberFormat::SCIENTIFIC;
                SensorCall();break;
            case NF_KEY_AMPM:                       // AM,A,PM,P
            case NF_KEY_AP:
            case NF_KEY_H:                          // H
            case NF_KEY_HH:                         // HH
            case NF_KEY_S:                          // S
            case NF_KEY_SS:                         // SS
                SensorCall();eNewType = css::util::NumberFormat::TIME;
                SensorCall();break;
            case NF_KEY_M:                          // M
            case NF_KEY_MM:                         // MM
                // minute or month
                SensorCall();nIndexPre = PreviousKeyword(i);
                nIndexNex = NextKeyword(i);
                cChar = PreviousChar(i);
                SensorCall();if (nIndexPre == NF_KEY_H   ||      // H
                    nIndexPre == NF_KEY_HH  ||      // HH
                    nIndexNex == NF_KEY_S   ||      // S
                    nIndexNex == NF_KEY_SS  ||      // SS
                    cChar == '['  )                 // [M
                {
                    SensorCall();eNewType = css::util::NumberFormat::TIME;
                    nTypeArray[i] -= 2;             // 6 -> 4, 7 -> 5
                }
                else
                {
                    SensorCall();eNewType = css::util::NumberFormat::DATE;
                }
                SensorCall();break;
            case NF_KEY_MMM:                        // MMM
            case NF_KEY_MMMM:                       // MMMM
            case NF_KEY_MMMMM:                      // MMMMM
            case NF_KEY_Q:                          // Q
            case NF_KEY_QQ:                         // QQ
            case NF_KEY_D:                          // D
            case NF_KEY_DD:                         // DD
            case NF_KEY_DDD:                        // DDD
            case NF_KEY_DDDD:                       // DDDD
            case NF_KEY_YY:                         // YY
            case NF_KEY_YYYY:                       // YYYY
            case NF_KEY_NN:                         // NN
            case NF_KEY_NNN:                        // NNN
            case NF_KEY_NNNN:                       // NNNN
            case NF_KEY_WW :                        // WW
            case NF_KEY_AAA :                       // AAA
            case NF_KEY_AAAA :                      // AAAA
            case NF_KEY_EC :                        // E
            case NF_KEY_EEC :                       // EE
            case NF_KEY_G :                         // G
            case NF_KEY_GG :                        // GG
            case NF_KEY_GGG :                       // GGG
            case NF_KEY_R :                         // R
            case NF_KEY_RR :                        // RR
                SensorCall();eNewType = css::util::NumberFormat::DATE;
                SensorCall();break;
            case NF_KEY_CCC:                        // CCC
                SensorCall();eNewType = css::util::NumberFormat::CURRENCY;
                SensorCall();break;
            case NF_KEY_GENERAL:                    // Standard
                SensorCall();eNewType = css::util::NumberFormat::NUMBER;
                bHaveGeneral = true;
                SensorCall();break;
            default:
                SensorCall();eNewType = css::util::NumberFormat::UNDEFINED;
                SensorCall();break;
            }
        }
        else
        {                                           // control character
            SensorCall();switch ( sStrArray[i][0] )
            {
            case '#':
            case '?':
                SensorCall();eNewType = css::util::NumberFormat::NUMBER;
                SensorCall();break;
            case '0':
                SensorCall();if ( (eScannedType & css::util::NumberFormat::TIME) == css::util::NumberFormat::TIME )
                {
                    SensorCall();if ( Is100SecZero( i, bDecSep ) )
                    {
                        SensorCall();bDecSep = true;                 // subsequent 0's
                        eNewType = css::util::NumberFormat::TIME;
                    }
                    else
                    {
                        {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}                    // Error
                    }
                }
                else
                {
                    SensorCall();eNewType = css::util::NumberFormat::NUMBER;
                }
                SensorCall();break;
            case '%':
                SensorCall();eNewType = css::util::NumberFormat::PERCENT;
                SensorCall();break;
            case '/':
                SensorCall();eNewType = css::util::NumberFormat::FRACTION;
                SensorCall();break;
            case '[':
                SensorCall();if ( i < nAnzStrings-1 &&
                     nTypeArray[i+1] == NF_SYMBOLTYPE_STRING &&
                     sStrArray[i+1][0] == '$' )
                {
                    SensorCall();eNewType = css::util::NumberFormat::CURRENCY;
                    bMatchBracket = true;
                }
                else {/*13*/SensorCall();if ( i < nAnzStrings-1 &&
                          nTypeArray[i+1] == NF_SYMBOLTYPE_STRING &&
                          sStrArray[i+1][0] == '~' )
                {
                    SensorCall();eNewType = css::util::NumberFormat::DATE;
                    bMatchBracket = true;
                }
                else
                {
                    SensorCall();sal_uInt16 nIndexNex = NextKeyword(i);
                    SensorCall();if (nIndexNex == NF_KEY_H   ||  // H
                        nIndexNex == NF_KEY_HH  ||  // HH
                        nIndexNex == NF_KEY_M   ||  // M
                        nIndexNex == NF_KEY_MM  ||  // MM
                        nIndexNex == NF_KEY_S   ||  // S
                        nIndexNex == NF_KEY_SS   )  // SS
                        {/*15*/SensorCall();eNewType = css::util::NumberFormat::TIME;/*16*/}
                    else
                    {
                        {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}                // Error
                    }
                ;/*14*/}}
                SensorCall();break;
            case '@':
                SensorCall();eNewType = css::util::NumberFormat::TEXT;
                SensorCall();break;
            default:
                SensorCall();if (pLoc->getTime100SecSep().equals(sStrArray[i]))
                {
                    SensorCall();bDecSep = true;                  // for SS,0
                }
                SensorCall();eNewType = css::util::NumberFormat::UNDEFINED;
                SensorCall();break;
            }
        }
        SensorCall();if (eScannedType == css::util::NumberFormat::UNDEFINED)
        {
            SensorCall();eScannedType = eNewType;
        }
        else {/*17*/SensorCall();if (eScannedType == css::util::NumberFormat::TEXT || eNewType == css::util::NumberFormat::TEXT)
        {
            SensorCall();eScannedType = css::util::NumberFormat::TEXT; // Text always remains text
        }
        else {/*19*/SensorCall();if (eNewType == css::util::NumberFormat::UNDEFINED)
        { // Remains as is
        }
        else {/*21*/SensorCall();if (eScannedType != eNewType)
        {
            SensorCall();switch (eScannedType)
            {
            case css::util::NumberFormat::DATE:
                SensorCall();switch (eNewType)
                {
                case css::util::NumberFormat::TIME:
                    SensorCall();eScannedType = css::util::NumberFormat::DATETIME;
                    SensorCall();break;
                case css::util::NumberFormat::FRACTION:         // DD/MM
                    SensorCall();break;
                default:
                    SensorCall();if (nCurrPos >= 0)
                    {
                        SensorCall();eScannedType = css::util::NumberFormat::UNDEFINED;
                    }
                    else {/*23*/SensorCall();if ( sStrArray[i] != OUString(pFormatter->GetDateSep()) )
                    {
                        {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}
                    ;/*24*/}}
                }
                SensorCall();break;
            case css::util::NumberFormat::TIME:
                SensorCall();switch (eNewType)
                {
                case css::util::NumberFormat::DATE:
                    SensorCall();eScannedType = css::util::NumberFormat::DATETIME;
                    SensorCall();break;
                case css::util::NumberFormat::FRACTION:         // MM/SS
                    SensorCall();break;
                default:
                    SensorCall();if (nCurrPos >= 0)
                    {
                        SensorCall();eScannedType = css::util::NumberFormat::UNDEFINED;
                    }
                    else {/*25*/SensorCall();if (!pLoc->getTimeSep().equals(sStrArray[i]))
                    {
                        {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}
                    ;/*26*/}}
                    SensorCall();break;
                }
                SensorCall();break;
            case css::util::NumberFormat::DATETIME:
                SensorCall();switch (eNewType)
                {
                case css::util::NumberFormat::TIME:
                case css::util::NumberFormat::DATE:
                    SensorCall();break;
                case css::util::NumberFormat::FRACTION:         // DD/MM
                    SensorCall();break;
                default:
                    SensorCall();if (nCurrPos >= 0)
                    {
                        SensorCall();eScannedType = css::util::NumberFormat::UNDEFINED;
                    }
                    else {/*27*/SensorCall();if ( OUString(pFormatter->GetDateSep()) != sStrArray[i] &&
                              !pLoc->getTimeSep().equals(sStrArray[i]) )
                    {
                        {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}
                    ;/*28*/}}
                }
                SensorCall();break;
            case css::util::NumberFormat::PERCENT:
                SensorCall();switch (eNewType)
                {
                case css::util::NumberFormat::NUMBER:   // Only number to percent
                    SensorCall();break;
                default:
                    {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}
                }
                SensorCall();break;
            case css::util::NumberFormat::SCIENTIFIC:
                SensorCall();switch (eNewType)
                {
                case css::util::NumberFormat::NUMBER:   // Only number to E
                    SensorCall();break;
                default:
                    {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}
                }
                SensorCall();break;
            case css::util::NumberFormat::NUMBER:
                SensorCall();switch (eNewType)
                {
                case css::util::NumberFormat::SCIENTIFIC:
                case css::util::NumberFormat::PERCENT:
                case css::util::NumberFormat::FRACTION:
                case css::util::NumberFormat::CURRENCY:
                    SensorCall();eScannedType = eNewType;
                    SensorCall();break;
                default:
                    SensorCall();if (nCurrPos >= 0)
                    {
                        SensorCall();eScannedType = css::util::NumberFormat::UNDEFINED;
                    }
                    else
                    {
                        {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}
                    }
                }
                SensorCall();break;
            case css::util::NumberFormat::FRACTION:
                SensorCall();switch (eNewType)
                {
                case css::util::NumberFormat::NUMBER:   // Only number to fraction
                    SensorCall();break;
                default:
                    {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}
                }
                SensorCall();break;
            default:
                SensorCall();break;
            }
        ;/*22*/}/*20*/}/*18*/}}
        SensorCall();nPos = nPos + sStrArray[i].getLength(); // Position of correction
        i++;
        SensorCall();if ( bMatchBracket )
        {   // no type detection inside of matching brackets if [$...], [~...]
            SensorCall();while ( bMatchBracket && i < nAnzStrings )
            {
                SensorCall();if ( nTypeArray[i] == NF_SYMBOLTYPE_DEL
                     && sStrArray[i][0] == ']' )
                {
                    SensorCall();bMatchBracket = false;
                }
                else
                {
                    SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                }
                SensorCall();nPos = nPos + sStrArray[i].getLength();
                i++;
            }
            SensorCall();if ( bMatchBracket )
            {
                {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;} // missing closing bracket at end of code
            }
        }
        SensorCall();SkipStrings(i, nPos);
    }

    SensorCall();if ((eScannedType == css::util::NumberFormat::NUMBER ||
         eScannedType == css::util::NumberFormat::UNDEFINED) &&
        nCurrPos >= 0 && !bHaveGeneral)
    {
        SensorCall();eScannedType = css::util::NumberFormat::CURRENCY; // old "automatic" currency
    }
    SensorCall();if (eScannedType == css::util::NumberFormat::UNDEFINED)
    {
        SensorCall();eScannedType = css::util::NumberFormat::DEFINED;
    }
    {sal_Int32  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;} // All is fine
}

bool ImpSvNumberformatScan::InsertSymbol( sal_uInt16 & nPos, svt::NfSymbolType eType, const OUString& rStr )
{
    SensorCall();if (nAnzStrings >= NF_MAX_FORMAT_SYMBOLS || nPos > nAnzStrings)
    {
        {_Bool  ReplaceReturn = false; SensorCall(); return ReplaceReturn;}
    }
    SensorCall();if (nPos > 0 && nTypeArray[nPos-1] == NF_SYMBOLTYPE_EMPTY)
    {
        SensorCall();--nPos; // reuse position
    }
    else
    {
        SensorCall();if ((size_t) (nAnzStrings + 1) >= NF_MAX_FORMAT_SYMBOLS)
        {
            {_Bool  ReplaceReturn = false; SensorCall(); return ReplaceReturn;}
        }
        SensorCall();++nAnzStrings;
        SensorCall();for (size_t i = nAnzStrings; i > nPos; --i)
        {
            SensorCall();nTypeArray[i] = nTypeArray[i-1];
            sStrArray[i] = sStrArray[i-1];
        }
    }
    SensorCall();++nAnzResStrings;
    nTypeArray[nPos] = static_cast<short>(eType);
    sStrArray[nPos] = rStr;
    {_Bool  ReplaceReturn = true; SensorCall(); return ReplaceReturn;}
}

int ImpSvNumberformatScan::FinalScanGetCalendar( sal_Int32& nPos, sal_uInt16& i,
                                                 sal_uInt16& rAnzResStrings )
{
    SensorCall();if ( i < nAnzStrings-1 &&
         sStrArray[i][0] == '[' &&
         nTypeArray[i+1] == NF_SYMBOLTYPE_STRING &&
         sStrArray[i+1][0] == '~' )
    {
        // [~calendarID]
        SensorCall();nPos = nPos + sStrArray[i].getLength();           // [
        nTypeArray[i] = NF_SYMBOLTYPE_CALDEL;
        nPos = nPos + sStrArray[++i].getLength();         // ~
        sStrArray[i-1] += sStrArray[i];                   // [~
        nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
        rAnzResStrings--;
        SensorCall();if ( ++i >= nAnzStrings )
        {
            {int  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;} // error
        }
        SensorCall();nPos = nPos + sStrArray[i].getLength();           // calendarID
        OUString& rStr = sStrArray[i];
        nTypeArray[i] = NF_SYMBOLTYPE_CALENDAR;          // convert
        i++;
        SensorCall();while ( i < nAnzStrings && sStrArray[i][0] != ']' )
        {
            SensorCall();nPos = nPos + sStrArray[i].getLength();
            rStr += sStrArray[i];
            nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
            rAnzResStrings--;
            i++;
        }
        SensorCall();if ( rStr.getLength() && i < nAnzStrings &&
             sStrArray[i][0] == ']' )
        {
            SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_CALDEL;
            nPos = nPos + sStrArray[i].getLength();
            i++;
        }
        else
        {
            {int  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;} // error
        }
        {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
    }
    {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

sal_Int32 ImpSvNumberformatScan::FinalScan( OUString& rString )
{
    SensorCall();const LocaleDataWrapper* pLoc = pFormatter->GetLocaleData();

    // save values for convert mode
    OUString sOldDecSep       = pFormatter->GetNumDecimalSep();
    OUString sOldThousandSep  = pFormatter->GetNumThousandSep();
    OUString sOldDateSep      = pFormatter->GetDateSep();
    OUString sOldTimeSep      = pLoc->getTimeSep();
    OUString sOldTime100SecSep= pLoc->getTime100SecSep();
    OUString sOldCurSymbol    = GetCurSymbol();
    OUString sOldCurString = GetCurString();
    sal_Unicode cOldKeyH    = sKeyword[NF_KEY_H][0];
    sal_Unicode cOldKeyMI   = sKeyword[NF_KEY_MI][0];
    sal_Unicode cOldKeyS    = sKeyword[NF_KEY_S][0];

    // If the group separator is a No-Break Space (French) continue with a
    // normal space instead so queries on space work correctly.
    // The same for Narrow No-Break Space just in case some locale uses it.
    // The format string is adjusted to allow both.
    // For output of the format code string the LocaleData characters are used.
    SensorCall();if ( (sOldThousandSep[0] == cNoBreakSpace || sOldThousandSep[0] == cNarrowNoBreakSpace) &&
            sOldThousandSep.getLength() == 1 )
    {
        sOldThousandSep = " ";
    }
    // change locale data et al
    SensorCall();if (bConvertMode)
    {
        SensorCall();pFormatter->ChangeIntl(eNewLnge);
        //! pointer may have changed
        pLoc = pFormatter->GetLocaleData();
        //! init new keywords
        InitKeywords();
    }
    SensorCall();const CharClass* pChrCls = pFormatter->GetCharClass();

    sal_Int32 nPos = 0;                    // error correction position
    sal_uInt16 i = 0;                      // symbol loop counter
    sal_uInt16 nCounter = 0;               // counts digits
    nAnzResStrings = nAnzStrings;          // counts remaining symbols
    bDecSep = false;                       // reset in case already used in TypeCheck
    bool bThaiT = false;                   // Thai T NatNum modifier present
    bool bTimePart = false;

    SensorCall();switch (eScannedType)
    {
    case css::util::NumberFormat::TEXT:
    case css::util::NumberFormat::DEFINED:
        SensorCall();while (i < nAnzStrings)
        {
            SensorCall();switch (nTypeArray[i])
            {
            case NF_SYMBOLTYPE_BLANK:
            case NF_SYMBOLTYPE_STAR:
                SensorCall();break;
            case NF_KEY_GENERAL : // #77026# "General" is the same as "@"
                SensorCall();break;
            default:
                SensorCall();if ( nTypeArray[i] != NF_SYMBOLTYPE_DEL ||
                     sStrArray[i][0] != '@' )
                {
                    SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                }
                SensorCall();break;
            }
            SensorCall();nPos = nPos + sStrArray[i].getLength();
            i++;
        } // of while
        SensorCall();break;

    case css::util::NumberFormat::NUMBER:
    case css::util::NumberFormat::PERCENT:
    case css::util::NumberFormat::CURRENCY:
    case css::util::NumberFormat::SCIENTIFIC:
    case css::util::NumberFormat::FRACTION:
        SensorCall();while (i < nAnzStrings)
        {
            // TODO: rechecking eScannedType is unnecessary.
            // This switch-case is for eScannedType == css::util::NumberFormat::FRACTION anyway
            SensorCall();if (eScannedType == css::util::NumberFormat::FRACTION &&        // special case
                nTypeArray[i] == NF_SYMBOLTYPE_DEL &&           // # ### #/#
                StringEqualsChar( sOldThousandSep, ' ' ) &&     // e.g. France or Sweden
                StringEqualsChar( sStrArray[i], ' ' ) &&
                !bFrac                          &&
                IsLastBlankBeforeFrac(i) )
            {
                SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;           // del->string
            }                                                   // No thousands marker

            SensorCall();if (nTypeArray[i] == NF_SYMBOLTYPE_BLANK    ||
                nTypeArray[i] == NF_SYMBOLTYPE_STAR ||
                nTypeArray[i] == NF_KEY_CCC         ||          // CCC
                nTypeArray[i] == NF_KEY_GENERAL )               // Standard
            {
                SensorCall();if (nTypeArray[i] == NF_KEY_GENERAL)
                {
                    SensorCall();nThousand = FLAG_STANDARD_IN_FORMAT;
                    SensorCall();if ( bConvertMode )
                    {
                        SensorCall();sStrArray[i] = sNameStandardFormat;
                    }
                }
                SensorCall();nPos = nPos + sStrArray[i].getLength();
                i++;
            }
            else {/*29*/SensorCall();if (nTypeArray[i] == NF_SYMBOLTYPE_STRING ||   // No Strings or
                     nTypeArray[i] > 0)                         // Keywords
            {
                SensorCall();if (eScannedType == css::util::NumberFormat::SCIENTIFIC &&
                    nTypeArray[i] == NF_KEY_E)                  // E+
                {
                    SensorCall();if (bExp)                                   // Double
                    {
                        {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}
                    }
                    SensorCall();bExp = true;
                    nExpPos = i;
                    SensorCall();if (bDecSep)
                    {
                        SensorCall();nCntPost = nCounter;
                    }
                    else
                    {
                        SensorCall();nCntPre = nCounter;
                    }
                    SensorCall();nCounter = 0;
                    nTypeArray[i] = NF_SYMBOLTYPE_EXP;
                }
                else {/*31*/SensorCall();if (eScannedType == css::util::NumberFormat::FRACTION &&
                         sStrArray[i][0] == ' ')
                {
                    SensorCall();if (!bBlank && !bFrac) // Not double or after a /
                    {
                        SensorCall();if (bDecSep && nCounter > 0) // Decimal places
                        {
                            {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;} // Error
                        }
                        SensorCall();bBlank = true;
                        nBlankPos = i;
                        nCntPre = nCounter;
                        nCounter = 0;
                    }
                    SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_FRACBLANK;
                }
                else {/*33*/SensorCall();if (nTypeArray[i] == NF_KEY_THAI_T)
                {
                    SensorCall();bThaiT = true;
                    sStrArray[i] = sKeyword[nTypeArray[i]];
                }
                else {/*35*/SensorCall();if (sStrArray[i][0] >= '0' &&
                         sStrArray[i][0] <= '9')
                {
                    SensorCall();OUString sDiv;
                    sal_uInt16 j = i;
                    SensorCall();while(j < nAnzStrings)
                    {
                        SensorCall();sDiv += sStrArray[j++];
                    }
                    SensorCall();if (OUString::number(sDiv.toInt32()) == sDiv)
                    {
                        // Found a Divisor
                        SensorCall();while (i < j)
                        {
                            SensorCall();nTypeArray[i++] = NF_SYMBOLTYPE_FRAC_FDIV;
                        }
                        SensorCall();i = j - 1; // Stop the loop
                        SensorCall();if (nCntPost)
                        {
                            SensorCall();nCounter = nCntPost;
                        }
                        else {/*37*/SensorCall();if (nCntPre)
                        {
                            SensorCall();nCounter = nCntPre;
                        ;/*38*/}}
                        // don't artificially increment nCntPre for forced denominator
                        SensorCall();if ( ( eScannedType != css::util::NumberFormat::FRACTION ) && (!nCntPre) )
                        {
                            SensorCall();nCntPre++;
                        }
                    }
                }
                else
                {
                    SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                ;/*36*/}/*34*/}/*32*/}}
                SensorCall();nPos = nPos + sStrArray[i].getLength();
                i++;
            }
            else {/*39*/SensorCall();if (nTypeArray[i] == NF_SYMBOLTYPE_DEL)
            {
                SensorCall();sal_Unicode cHere = sStrArray[i][0];
                sal_Unicode cSaved = cHere;
                // Handle not pre-known separators in switch.
                sal_Unicode cSimplified;
                SensorCall();if (StringEqualsChar( pFormatter->GetNumThousandSep(), cHere))
                {
                    SensorCall();cSimplified = ',';
                }
                else {/*41*/SensorCall();if (StringEqualsChar( pFormatter->GetNumDecimalSep(), cHere))
                {
                    SensorCall();cSimplified = '.';
                }
                else
                {
                    SensorCall();cSimplified = cHere;
                ;/*42*/}}

                SensorCall();OUString& rStr = sStrArray[i];

                SensorCall();switch ( cSimplified )
                {
                case '#':
                case '0':
                case '?':
                    SensorCall();if (nThousand > 0)                  // #... #
                    {
                        {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}                    // Error
                    }
                    else {/*43*/SensorCall();if (bFrac && cHere == '0')
                    {
                        {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}                    // Denominator is 0
                    ;/*44*/}}
                    SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_DIGIT;
                    nPos = nPos + rStr.getLength();
                    i++;
                    nCounter++;
                    SensorCall();while (i < nAnzStrings &&
                           (sStrArray[i][0] == '#' ||
                            sStrArray[i][0] == '0' ||
                            sStrArray[i][0] == '?'))
                    {
                        SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_DIGIT;
                        nPos = nPos + sStrArray[i].getLength();
                        nCounter++;
                        i++;
                    }
                    SensorCall();break;
                case '-':
                    SensorCall();if ( bDecSep && nDecPos+1 == i &&
                         nTypeArray[nDecPos] == NF_SYMBOLTYPE_DECSEP )
                    {
                        // "0.--"
                        SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_DIGIT;
                        nPos = nPos + rStr.getLength();
                        i++;
                        nCounter++;
                        SensorCall();while (i < nAnzStrings &&
                               (sStrArray[i][0] == '-') )
                        {
                            // If more than two dashes are present in
                            // currency formats the last dash will be
                            // interpreted literally as a minus sign.
                            // Has to be this ugly. Period.
                            SensorCall();if ( eScannedType == css::util::NumberFormat::CURRENCY
                                 && rStr.getLength() >= 2 &&
                                 (i == nAnzStrings-1 ||
                                  sStrArray[i+1][0] != '-') )
                            {
                                SensorCall();break;
                            }
                            SensorCall();rStr += sStrArray[i];
                            nPos = nPos + sStrArray[i].getLength();
                            nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
                            nAnzResStrings--;
                            nCounter++;
                            i++;
                        }
                    }
                    else
                    {
                        SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                        nPos = nPos + sStrArray[i].getLength();
                        i++;
                    }
                    SensorCall();break;
                case '.':
                case ',':
                case '\'':
                case ' ':
                    SensorCall();if ( StringEqualsChar( sOldThousandSep, cSaved ) )
                    {
                        // previous char with skip empty
                        SensorCall();sal_Unicode cPre = PreviousChar(i);
                        sal_Unicode cNext;
                        SensorCall();if (bExp || bBlank || bFrac)
                        {
                            // after E, / or ' '
                            SensorCall();if ( !StringEqualsChar( sOldThousandSep, ' ' ) )
                            {
                                SensorCall();nPos = nPos + sStrArray[i].getLength();
                                nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
                                nAnzResStrings--;
                                i++; // eat it
                            }
                            else
                            {
                                SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                            }
                        }
                        else {/*45*/SensorCall();if (i > 0 && i < nAnzStrings-1   &&
                                 (cPre == '#' || cPre == '0')      &&
                                 ((cNext = NextChar(i)) == '#' || cNext == '0')) // #,#
                        {
                            SensorCall();nPos = nPos + sStrArray[i].getLength();
                            SensorCall();if (!bThousand) // only once
                            {
                                SensorCall();bThousand = true;
                            }
                            // Eat it, will be reinserted at proper grouping positions further down.
                            SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
                            nAnzResStrings--;
                            i++;
                        }
                        else {/*47*/SensorCall();if (i > 0 && (cPre == '#' || cPre == '0')
                                 && PreviousType(i) == NF_SYMBOLTYPE_DIGIT
                                 && nThousand < FLAG_STANDARD_IN_FORMAT )
                        {   // #,,,,
                            SensorCall();if ( StringEqualsChar( sOldThousandSep, ' ' ) )
                            {
                                // strange, those French..
                                SensorCall();bool bFirst = true;
                                //  set a hard No-Break Space or ConvertMode
                                const OUString& rSepF = pFormatter->GetNumThousandSep();
                                SensorCall();while ( i < nAnzStrings &&
                                        sStrArray[i] == sOldThousandSep &&
                                        StringEqualsChar( sOldThousandSep, NextChar(i) ) )
                                {   // last was a space or another space
                                    // is following => separator
                                    SensorCall();nPos = nPos + sStrArray[i].getLength();
                                    SensorCall();if ( bFirst )
                                    {
                                        SensorCall();bFirst = false;
                                        rStr = rSepF;
                                        nTypeArray[i] = NF_SYMBOLTYPE_THSEP;
                                    }
                                    else
                                    {
                                        SensorCall();rStr += rSepF;
                                        nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
                                        nAnzResStrings--;
                                    }
                                    SensorCall();nThousand++;
                                    i++;
                                }
                                SensorCall();if ( i < nAnzStrings-1 &&
                                     sStrArray[i] == sOldThousandSep )
                                {
                                    // something following last space
                                    // => space if currency contained,
                                    // else separator
                                    SensorCall();nPos = nPos + sStrArray[i].getLength();
                                    SensorCall();if ( (nPos <= nCurrPos &&
                                          nCurrPos < nPos + sStrArray[i+1].getLength()) ||
                                         nTypeArray[i+1] == NF_KEY_CCC ||
                                         (i < nAnzStrings-2 &&
                                          sStrArray[i+1][0] == '[' &&
                                          sStrArray[i+2][0] == '$') )
                                    {
                                        SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                                    }
                                    else
                                    {
                                        SensorCall();if ( bFirst )
                                        {
                                            SensorCall();bFirst = false;
                                            rStr = rSepF;
                                            nTypeArray[i] = NF_SYMBOLTYPE_THSEP;
                                        }
                                        else
                                        {
                                            SensorCall();rStr += rSepF;
                                            nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
                                            nAnzResStrings--;
                                        }
                                        SensorCall();nThousand++;
                                    }
                                    SensorCall();i++;
                                }
                            }
                            else
                            {
                                SensorCall();do
                                {
                                    SensorCall();nThousand++;
                                    nTypeArray[i] = NF_SYMBOLTYPE_THSEP;
                                    nPos = nPos + sStrArray[i].getLength();
                                    sStrArray[i] = pFormatter->GetNumThousandSep();
                                    i++;
                                }
                                while (i < nAnzStrings && sStrArray[i] == sOldThousandSep);
                            }
                        }
                        else // any grsep
                        {
                            SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                            nPos = nPos + rStr.getLength();
                            i++;
                            SensorCall();while ( i < nAnzStrings && sStrArray[i] == sOldThousandSep )
                            {
                                SensorCall();rStr += sStrArray[i];
                                nPos = nPos + sStrArray[i].getLength();
                                nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
                                nAnzResStrings--;
                                i++;
                            }
                        ;/*48*/}/*46*/}}
                    }
                    else {/*49*/SensorCall();if ( StringEqualsChar( sOldDecSep, cSaved ) )
                    {
                        SensorCall();if (bBlank || bFrac)    // . behind / or ' '
                        {
                            {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}        // error
                        }
                        else {/*51*/SensorCall();if (bExp)          // behind E
                        {
                            SensorCall();nPos = nPos + sStrArray[i].getLength();
                            nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
                            nAnzResStrings--;
                            i++;                // eat it
                        }
                        else {/*53*/SensorCall();if (bDecSep)       // any .
                        {
                            SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                            nPos = nPos + rStr.getLength();
                            i++;
                            SensorCall();while ( i < nAnzStrings && sStrArray[i] == sOldDecSep )
                            {
                                SensorCall();rStr += sStrArray[i];
                                nPos = nPos + sStrArray[i].getLength();
                                nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
                                nAnzResStrings--;
                                i++;
                            }
                        }
                        else
                        {
                            SensorCall();nPos = nPos + sStrArray[i].getLength();
                            nTypeArray[i] = NF_SYMBOLTYPE_DECSEP;
                            sStrArray[i] = pFormatter->GetNumDecimalSep();
                            bDecSep = true;
                            nDecPos = i;
                            nCntPre = nCounter;
                            nCounter = 0;

                            i++;
                        ;/*54*/}/*52*/}}
                    } // of else = DecSep
                    else // . without meaning
                    {
                        SensorCall();if (cSaved == ' ' &&
                            eScannedType == css::util::NumberFormat::FRACTION &&
                            StringEqualsChar( sStrArray[i], ' ' ) )
                        {
                            SensorCall();if (!bBlank && !bFrac)  // no dups
                            {                       // or behind /
                                SensorCall();if (bDecSep && nCounter > 0) // dec.
                                {
                                    {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;} // error
                                }
                                SensorCall();bBlank = true;
                                nBlankPos = i;
                                nCntPre = nCounter;
                                nCounter = 0;
                            }
                            SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                            nPos = nPos + sStrArray[i].getLength();
                        }
                        else
                        {
                            SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                            nPos = nPos + rStr.getLength();
                            i++;
                            SensorCall();while (i < nAnzStrings && StringEqualsChar( sStrArray[i], cSaved ) )
                            {
                                SensorCall();rStr += sStrArray[i];
                                nPos = nPos + sStrArray[i].getLength();
                                nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
                                nAnzResStrings--;
                                i++;
                            }
                        }
                    ;/*50*/}}
                    SensorCall();break;
                case '/':
                    SensorCall();if (eScannedType == css::util::NumberFormat::FRACTION)
                    {
                        SensorCall();if ( i == 0 ||
                             (nTypeArray[i-1] != NF_SYMBOLTYPE_DIGIT &&
                              nTypeArray[i-1] != NF_SYMBOLTYPE_EMPTY) )
                        {
                            {sal_Int32  ReplaceReturn = nPos ? nPos : 1; SensorCall(); return ReplaceReturn;} // /? not allowed
                        }
                        else {/*55*/SensorCall();if (!bFrac || (bDecSep && nCounter > 0))
                        {
                            SensorCall();bFrac = true;
                            nCntPost = nCounter;
                            nCounter = 0;
                            nTypeArray[i] = NF_SYMBOLTYPE_FRAC;
                            nPos = nPos + sStrArray[i].getLength();
                            i++;
                        }
                        else // / double or in , in the denominator
                        {
                            {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;} // Error
                        ;/*56*/}}
                    }
                    else
                    {
                        SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                        nPos = nPos + sStrArray[i].getLength();
                        i++;
                    }
                    SensorCall();break;
                case '[' :
                    SensorCall();if ( eScannedType == css::util::NumberFormat::CURRENCY &&
                         i < nAnzStrings-1 &&
                         nTypeArray[i+1] == NF_SYMBOLTYPE_STRING &&
                         sStrArray[i+1][0] == '$' )
                    {
                        // [$DM-xxx]
                        SensorCall();nPos = nPos + sStrArray[i].getLength();     // [
                        nTypeArray[i] = NF_SYMBOLTYPE_CURRDEL;
                        nPos = nPos + sStrArray[++i].getLength();   // $
                        sStrArray[i-1] += sStrArray[i];             // [$
                        nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
                        nAnzResStrings--;
                        SensorCall();if ( ++i >= nAnzStrings )
                        {
                            {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;} // Error
                        }
                        SensorCall();nPos = nPos + sStrArray[i].getLength();     // DM
                        OUString* pStr = &sStrArray[i];
                        nTypeArray[i] = NF_SYMBOLTYPE_CURRENCY; // convert
                        bool bHadDash = false;
                        i++;
                        SensorCall();while ( i < nAnzStrings && sStrArray[i][0] != ']' )
                        {
                            SensorCall();nPos = nPos + sStrArray[i].getLength();
                            SensorCall();if ( bHadDash )
                            {
                                SensorCall();*pStr += sStrArray[i];
                                nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
                                nAnzResStrings--;
                            }
                            else
                            {
                                SensorCall();if ( sStrArray[i][0] == '-' )
                                {
                                    SensorCall();bHadDash = true;
                                    pStr = &sStrArray[i];
                                    nTypeArray[i] = NF_SYMBOLTYPE_CURREXT;
                                }
                                else
                                {
                                    SensorCall();*pStr += sStrArray[i];
                                    nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
                                    nAnzResStrings--;
                                }
                            }
                            SensorCall();i++;
                        }
                        SensorCall();if ( rStr.getLength() && i < nAnzStrings && sStrArray[i][0] == ']' )
                        {
                            SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_CURRDEL;
                            nPos = nPos + sStrArray[i].getLength();
                            i++;
                        }
                        else
                        {
                            {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;} // Error
                        }
                    }
                    else
                    {
                        SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                        nPos = nPos + sStrArray[i].getLength();
                        i++;
                    }
                    SensorCall();break;
                default: // Other Dels
                    SensorCall();if (eScannedType == css::util::NumberFormat::PERCENT && cHere == '%')
                    {
                        SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_PERCENT;
                    }
                    else
                    {
                        SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                    }
                    SensorCall();nPos = nPos + sStrArray[i].getLength();
                    i++;
                    SensorCall();break;
                } // of switch (Del)
            } // of else Del
            else
            {
                SAL_WARN( "svl.numbers", "unknown NF_SYMBOLTYPE_..." );
                SensorCall();nPos = nPos + sStrArray[i].getLength();
                i++;
            ;/*40*/}/*30*/}}
        } // of while
        SensorCall();if (eScannedType == css::util::NumberFormat::FRACTION)
        {
            SensorCall();if (bFrac)
            {
                SensorCall();nCntExp = nCounter;
            }
            else {/*57*/SensorCall();if (bBlank)
            {
                SensorCall();nCntPost = nCounter;
            }
            else
            {
                SensorCall();nCntPre = nCounter;
            ;/*58*/}}
        }
        else
        {
            SensorCall();if (bExp)
            {
                SensorCall();nCntExp = nCounter;
            }
            else {/*59*/SensorCall();if (bDecSep)
            {
                SensorCall();nCntPost = nCounter;
            }
            else
            {
                SensorCall();nCntPre = nCounter;
            ;/*60*/}}
        }
        SensorCall();if (bThousand) // Expansion of grouping separators
        {
            SensorCall();sal_uInt16 nMaxPos;
            SensorCall();if (bFrac)
            {
                SensorCall();if (bBlank)
                {
                    SensorCall();nMaxPos = nBlankPos;
                }
                else
                {
                    SensorCall();nMaxPos = 0;                // no grouping
                }
            }
            else {/*61*/SensorCall();if (bDecSep)                   // decimal separator present
            {
                SensorCall();nMaxPos = nDecPos;
            }
            else {/*63*/SensorCall();if (bExp)                      // 'E' exponent present
            {
                SensorCall();nMaxPos = nExpPos;
            }
            else                                // up to end
            {
                SensorCall();nMaxPos = i;
            ;/*64*/}/*62*/}}
            // Insert separators at proper positions.
            SensorCall();sal_Int32 nCount = 0;
            utl::DigitGroupingIterator aGrouping( pLoc->getDigitGrouping());
            size_t nFirstDigitSymbol = nMaxPos;
            size_t nFirstGroupingSymbol = nMaxPos;
            i = nMaxPos;
            SensorCall();while (i-- > 0)
            {
                SensorCall();if (nTypeArray[i] == NF_SYMBOLTYPE_DIGIT)
                {
                    SensorCall();nFirstDigitSymbol = i;
                    nCount = nCount + sStrArray[i].getLength(); // MSC converts += to int and then warns, so ...
                    // Insert separator only if not leftmost symbol.
                    SensorCall();if (i > 0 && nCount >= aGrouping.getPos())
                    {
                        DBG_ASSERT( sStrArray[i].getLength() == 1,
                                    "ImpSvNumberformatScan::FinalScan: combined digits in group separator insertion");
                        SensorCall();if (!InsertSymbol( i, NF_SYMBOLTYPE_THSEP, pFormatter->GetNumThousandSep()))
                        {
                            // nPos isn't correct here, but signals error
                            {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}
                        }
                        // i may have been decremented by 1
                        SensorCall();nFirstDigitSymbol = i + 1;
                        nFirstGroupingSymbol = i;
                        aGrouping.advance();
                    }
                }
            }
            // Generated something like "string",000; remove separator again.
            SensorCall();if (nFirstGroupingSymbol < nFirstDigitSymbol)
            {
                SensorCall();nTypeArray[nFirstGroupingSymbol] = NF_SYMBOLTYPE_EMPTY;
                nAnzResStrings--;
            }
        }
        // Combine digits into groups to save memory (Info will be copied
        // later, taking only non-empty symbols).
        SensorCall();for (i = 0; i < nAnzStrings; ++i)
        {
            SensorCall();if (nTypeArray[i] == NF_SYMBOLTYPE_DIGIT)
            {
                SensorCall();OUString& rStr = sStrArray[i];
                SensorCall();while (++i < nAnzStrings && nTypeArray[i] == NF_SYMBOLTYPE_DIGIT)
                {
                    SensorCall();rStr += sStrArray[i];
                    nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
                    nAnzResStrings--;
                }
            }
        }
        SensorCall();break; // of css::util::NumberFormat::NUMBER
    case css::util::NumberFormat::DATE:
        SensorCall();while (i < nAnzStrings)
        {
            SensorCall();int nCalRet;
            SensorCall();switch (nTypeArray[i])
            {
            case NF_SYMBOLTYPE_BLANK:
            case NF_SYMBOLTYPE_STAR:
            case NF_SYMBOLTYPE_STRING:
                SensorCall();nPos = nPos + sStrArray[i].getLength();
                i++;
                SensorCall();break;
            case NF_SYMBOLTYPE_DEL:
                SensorCall();if (sStrArray[i] == sOldDateSep)
                {
                    SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_DATESEP;
                    nPos = nPos + sStrArray[i].getLength();
                    SensorCall();if (bConvertMode)
                    {
                        SensorCall();sStrArray[i] = pFormatter->GetDateSep();
                    }
                    SensorCall();i++;
                }
                else {/*65*/SensorCall();if ( (nCalRet = FinalScanGetCalendar( nPos, i, nAnzResStrings )) != 0 )
                {
                    SensorCall();if ( nCalRet < 0  )
                    {
                        {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;} // error
                    }
                }
                else
                {
                    SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                    nPos = nPos + sStrArray[i].getLength();
                    i++;
                ;/*66*/}}
                SensorCall();break;
            case NF_KEY_THAI_T :
                SensorCall();bThaiT = true;
                // fall through
            case NF_KEY_M:                          // M
            case NF_KEY_MM:                         // MM
            case NF_KEY_MMM:                        // MMM
            case NF_KEY_MMMM:                       // MMMM
            case NF_KEY_MMMMM:                      // MMMMM
            case NF_KEY_Q:                          // Q
            case NF_KEY_QQ:                         // QQ
            case NF_KEY_D:                          // D
            case NF_KEY_DD:                         // DD
            case NF_KEY_DDD:                        // DDD
            case NF_KEY_DDDD:                       // DDDD
            case NF_KEY_YY:                         // YY
            case NF_KEY_YYYY:                       // YYYY
            case NF_KEY_NN:                         // NN
            case NF_KEY_NNN:                        // NNN
            case NF_KEY_NNNN:                       // NNNN
            case NF_KEY_WW :                        // WW
            case NF_KEY_AAA :                       // AAA
            case NF_KEY_AAAA :                      // AAAA
            case NF_KEY_EC :                        // E
            case NF_KEY_EEC :                       // EE
            case NF_KEY_G :                         // G
            case NF_KEY_GG :                        // GG
            case NF_KEY_GGG :                       // GGG
            case NF_KEY_R :                         // R
            case NF_KEY_RR :                        // RR
                SensorCall();sStrArray[i] = sKeyword[nTypeArray[i]]; // tTtT -> TTTT
                nPos = nPos + sStrArray[i].getLength();
                i++;
                SensorCall();break;
            default: // Other keywords
                SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                nPos = nPos + sStrArray[i].getLength();
                i++;
                SensorCall();break;
            }
        } // of while
        SensorCall();break; // of css::util::NumberFormat::DATE
    case css::util::NumberFormat::TIME:
        SensorCall();while (i < nAnzStrings)
        {
            SensorCall();sal_Unicode cChar;

            SensorCall();switch (nTypeArray[i])
            {
            case NF_SYMBOLTYPE_BLANK:
            case NF_SYMBOLTYPE_STAR:
                SensorCall();nPos = nPos + sStrArray[i].getLength();
                i++;
                SensorCall();break;
            case NF_SYMBOLTYPE_DEL:
                SensorCall();switch( sStrArray[i][0] )
                {
                case '0':
                    SensorCall();if ( Is100SecZero( i, bDecSep ) )
                    {
                        SensorCall();bDecSep = true;
                        nTypeArray[i] = NF_SYMBOLTYPE_DIGIT;
                        OUString& rStr = sStrArray[i];
                        i++;
                        nPos = nPos + sStrArray[i].getLength();
                        nCounter++;
                        SensorCall();while (i < nAnzStrings &&
                               sStrArray[i][0] == '0')
                        {
                            SensorCall();rStr += sStrArray[i];
                            nPos = nPos + sStrArray[i].getLength();
                            nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
                            nAnzResStrings--;
                            nCounter++;
                            i++;
                        }
                    }
                    else
                    {
                        {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}
                    }
                    SensorCall();break;
                case '#':
                case '?':
                    {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}
                case '[':
                    SensorCall();if (bThousand) // Double
                    {
                        {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}
                    }
                    SensorCall();bThousand = true; // Empty for Time
                    cChar = pChrCls->uppercase(OUString(NextChar(i)))[0];
                    SensorCall();if ( cChar == cOldKeyH )
                    {
                        SensorCall();nThousand = 1;      // H
                    }
                    else {/*67*/SensorCall();if ( cChar == cOldKeyMI )
                    {
                        SensorCall();nThousand = 2;      // M
                    }
                    else {/*69*/SensorCall();if ( cChar == cOldKeyS )
                    {
                        SensorCall();nThousand = 3;      // S
                    }
                    else
                    {
                        {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}
                    ;/*70*/}/*68*/}}
                    SensorCall();nPos = nPos + sStrArray[i].getLength();
                    i++;
                    SensorCall();break;
                case ']':
                    SensorCall();if (!bThousand) // No preceding [
                    {
                        {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}
                    }
                    SensorCall();nPos = nPos + sStrArray[i].getLength();
                    i++;
                    SensorCall();break;
                default:
                    SensorCall();nPos = nPos + sStrArray[i].getLength();
                    SensorCall();if ( sStrArray[i] == sOldTimeSep )
                    {
                        SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_TIMESEP;
                        SensorCall();if ( bConvertMode )
                        {
                            SensorCall();sStrArray[i] = pLoc->getTimeSep();
                        }
                    }
                    else {/*71*/SensorCall();if ( sStrArray[i] == sOldTime100SecSep )
                    {
                        SensorCall();bDecSep = true;
                        nTypeArray[i] = NF_SYMBOLTYPE_TIME100SECSEP;
                        SensorCall();if ( bConvertMode )
                        {
                            SensorCall();sStrArray[i] = pLoc->getTime100SecSep();
                        }
                    }
                    else
                    {
                        SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                    ;/*72*/}}
                    SensorCall();i++;
                    SensorCall();break;
                }
                SensorCall();break;
            case NF_SYMBOLTYPE_STRING:
                SensorCall();nPos = nPos + sStrArray[i].getLength();
                i++;
                SensorCall();break;
            case NF_KEY_AMPM:                       // AM/PM
            case NF_KEY_AP:                         // A/P
                SensorCall();bExp = true;                        // Abuse for A/P
                sStrArray[i] = sKeyword[nTypeArray[i]]; // tTtT -> TTTT
                nPos = nPos + sStrArray[i].getLength();
                i++;
                SensorCall();break;
            case NF_KEY_THAI_T :
                SensorCall();bThaiT = true;
                // fall through
            case NF_KEY_MI:                         // M
            case NF_KEY_MMI:                        // MM
            case NF_KEY_H:                          // H
            case NF_KEY_HH:                         // HH
            case NF_KEY_S:                          // S
            case NF_KEY_SS:                         // SS
                SensorCall();sStrArray[i] = sKeyword[nTypeArray[i]]; // tTtT -> TTTT
                nPos = nPos + sStrArray[i].getLength();
                i++;
                SensorCall();break;
            default: // Other keywords
                SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                nPos = nPos + sStrArray[i].getLength();
                i++;
                SensorCall();break;
            }
        }                                       // of while
        SensorCall();nCntPost = nCounter;                    // Zero counter
        SensorCall();if (bExp)
        {
            SensorCall();nCntExp = 1;                        // Remembers AM/PM
        }
        SensorCall();break;                                 // of css::util::NumberFormat::TIME
    case css::util::NumberFormat::DATETIME:
        SensorCall();while (i < nAnzStrings)
        {
            SensorCall();int nCalRet;
            SensorCall();switch (nTypeArray[i])
            {
            case NF_SYMBOLTYPE_BLANK:
            case NF_SYMBOLTYPE_STAR:
            case NF_SYMBOLTYPE_STRING:
                SensorCall();nPos = nPos + sStrArray[i].getLength();
                i++;
                SensorCall();break;
            case NF_SYMBOLTYPE_DEL:
                SensorCall();if ( (nCalRet = FinalScanGetCalendar( nPos, i, nAnzResStrings )) != 0 )
                {
                    SensorCall();if ( nCalRet < 0  )
                    {
                        {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;} // Error
                    }
                }
                else
                {
                    SensorCall();switch( sStrArray[i][0] )
                    {
                    case '0':
                        SensorCall();if ( bTimePart && Is100SecZero( i, bDecSep ) )
                        {
                            SensorCall();bDecSep = true;
                            nTypeArray[i] = NF_SYMBOLTYPE_DIGIT;
                            OUString& rStr = sStrArray[i];
                            i++;
                            nPos = nPos + sStrArray[i].getLength();
                            nCounter++;
                            SensorCall();while (i < nAnzStrings &&
                                   sStrArray[i][0] == '0')
                            {
                                SensorCall();rStr += sStrArray[i];
                                nPos = nPos + sStrArray[i].getLength();
                                nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
                                nAnzResStrings--;
                                nCounter++;
                                i++;
                            }
                        }
                        else
                        {
                            {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}
                        }
                        SensorCall();break;
                    case '#':
                    case '?':
                        {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}
                    default:
                        SensorCall();nPos = nPos + sStrArray[i].getLength();
                        SensorCall();if (bTimePart)
                        {
                            SensorCall();if ( sStrArray[i] == sOldTimeSep )
                            {
                                SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_TIMESEP;
                                SensorCall();if ( bConvertMode )
                                {
                                    SensorCall();sStrArray[i] = pLoc->getTimeSep();
                                }
                            }
                            else {/*73*/SensorCall();if ( sStrArray[i] == sOldTime100SecSep )
                            {
                                SensorCall();bDecSep = true;
                                nTypeArray[i] = NF_SYMBOLTYPE_TIME100SECSEP;
                                SensorCall();if ( bConvertMode )
                                {
                                    SensorCall();sStrArray[i] = pLoc->getTime100SecSep();
                                }
                            }
                            else
                            {
                                SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                            ;/*74*/}}
                        }
                        else
                        {
                            SensorCall();if ( sStrArray[i] == sOldDateSep )
                            {
                                SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_DATESEP;
                                SensorCall();if (bConvertMode)
                                    {/*75*/SensorCall();sStrArray[i] = pFormatter->GetDateSep();/*76*/}
                            }
                            else
                            {
                                SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                            }
                        }
                        SensorCall();i++;
                        SensorCall();break;
                    }
                }
                SensorCall();break;
            case NF_KEY_AMPM:                       // AM/PM
            case NF_KEY_AP:                         // A/P
                SensorCall();bTimePart = true;
                bExp = true;                        // Abuse for A/P
                sStrArray[i] = sKeyword[nTypeArray[i]]; // tTtT -> TTTT
                nPos = nPos + sStrArray[i].getLength();
                i++;
                SensorCall();break;
            case NF_KEY_MI:                         // M
            case NF_KEY_MMI:                        // MM
            case NF_KEY_H:                          // H
            case NF_KEY_HH:                         // HH
            case NF_KEY_S:                          // S
            case NF_KEY_SS:                         // SS
                SensorCall();bTimePart = true;
                sStrArray[i] = sKeyword[nTypeArray[i]]; // tTtT -> TTTT
                nPos = nPos + sStrArray[i].getLength();
                i++;
                SensorCall();break;
            case NF_KEY_M:                          // M
            case NF_KEY_MM:                         // MM
            case NF_KEY_MMM:                        // MMM
            case NF_KEY_MMMM:                       // MMMM
            case NF_KEY_MMMMM:                      // MMMMM
            case NF_KEY_Q:                          // Q
            case NF_KEY_QQ:                         // QQ
            case NF_KEY_D:                          // D
            case NF_KEY_DD:                         // DD
            case NF_KEY_DDD:                        // DDD
            case NF_KEY_DDDD:                       // DDDD
            case NF_KEY_YY:                         // YY
            case NF_KEY_YYYY:                       // YYYY
            case NF_KEY_NN:                         // NN
            case NF_KEY_NNN:                        // NNN
            case NF_KEY_NNNN:                       // NNNN
            case NF_KEY_WW :                        // WW
            case NF_KEY_AAA :                       // AAA
            case NF_KEY_AAAA :                      // AAAA
            case NF_KEY_EC :                        // E
            case NF_KEY_EEC :                       // EE
            case NF_KEY_G :                         // G
            case NF_KEY_GG :                        // GG
            case NF_KEY_GGG :                       // GGG
            case NF_KEY_R :                         // R
            case NF_KEY_RR :                        // RR
                SensorCall();bTimePart = false;
                sStrArray[i] = sKeyword[nTypeArray[i]]; // tTtT -> TTTT
                nPos = nPos + sStrArray[i].getLength();
                i++;
                SensorCall();break;
            case NF_KEY_THAI_T :
                SensorCall();bThaiT = true;
                sStrArray[i] = sKeyword[nTypeArray[i]];
                nPos = nPos + sStrArray[i].getLength();
                i++;
                SensorCall();break;
            default: // Other keywords
                SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_STRING;
                nPos = nPos + sStrArray[i].getLength();
                i++;
                SensorCall();break;
            }
        } // of while
        SensorCall();nCntPost = nCounter; // decimals (100th seconds)
        SensorCall();if (bExp)
        {
            SensorCall();nCntExp = 1; // Remembers AM/PM
        }
        SensorCall();break; // of css::util::NumberFormat::DATETIME
    default:
        SensorCall();break;
    }
    SensorCall();if (eScannedType == css::util::NumberFormat::SCIENTIFIC &&
        (nCntPre + nCntPost == 0 || nCntExp == 0))
    {
        {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}
    }
    else {/*77*/SensorCall();if (eScannedType == css::util::NumberFormat::FRACTION && (nCntExp > 8 || nCntExp == 0))
    {
        {sal_Int32  ReplaceReturn = nPos; SensorCall(); return ReplaceReturn;}
    ;/*78*/}}
    SensorCall();if (bThaiT && !GetNatNumModifier())
    {
        SensorCall();SetNatNumModifier(1);
    }
    SensorCall();if ( bConvertMode )
    {
        // strings containing keywords of the target locale must be quoted, so
        // the user sees the difference and is able to edit the format string
        SensorCall();for ( i=0; i < nAnzStrings; i++ )
        {
            SensorCall();if ( nTypeArray[i] == NF_SYMBOLTYPE_STRING &&
                 sStrArray[i][0] != '\"' )
            {
                SensorCall();if ( bConvertSystemToSystem && eScannedType == css::util::NumberFormat::CURRENCY )
                {
                    // don't stringize automatic currency, will be converted
                    SensorCall();if ( sStrArray[i] == sOldCurSymbol )
                    {
                        SensorCall();continue; // for
                    }
                    // DM might be splitted into D and M
                    SensorCall();if ( sStrArray[i].getLength() < sOldCurSymbol.getLength() &&
                         pChrCls->uppercase( sStrArray[i], 0, 1 )[0] ==
                         sOldCurString[0] )
                    {
                        SensorCall();OUString aTmp( sStrArray[i] );
                        sal_uInt16 j = i + 1;
                        SensorCall();while ( aTmp.getLength() < sOldCurSymbol.getLength() &&
                                j < nAnzStrings &&
                                nTypeArray[j] == NF_SYMBOLTYPE_STRING )
                        {
                            SensorCall();aTmp += sStrArray[j++];
                        }
                        SensorCall();if ( pChrCls->uppercase( aTmp ) == sOldCurString )
                        {
                            SensorCall();sStrArray[i++] = aTmp;
                            SensorCall();for ( ; i<j; i++ )
                            {
                                SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
                                nAnzResStrings--;
                            }
                            SensorCall();i = j - 1;
                            SensorCall();continue; // for
                        }
                    }
                }
                SensorCall();OUString& rStr = sStrArray[i];
                sal_Int32 nLen = rStr.getLength();
                SensorCall();for ( sal_Int32 j = 0; j < nLen; j++ )
                {
                    SensorCall();if ( (j == 0 || rStr[j - 1] != '\\') && GetKeyWord( rStr, j ) )
                    {
                        rStr = "\"" + rStr + "\"";
                        SensorCall();break; // for
                    }
                }
            }
        }
    }
    // Concatenate strings, remove quotes for output, and rebuild the format string
    SensorCall();rString.clear();
    i = 0;
    SensorCall();while (i < nAnzStrings)
    {
        SensorCall();sal_Int32 nStringPos;
        sal_Int32 nArrPos = 0;
        sal_uInt16 iPos = i;
        SensorCall();switch ( nTypeArray[i] )
        {
        case NF_SYMBOLTYPE_STRING :
            SensorCall();nStringPos = rString.getLength();
            SensorCall();do
            {
                SensorCall();if (sStrArray[i].getLength() == 2 &&
                    sStrArray[i][0] == '\\')
                {
                    // Unescape some simple forms of symbols even in the UI
                    // visible string to prevent duplicates that differ
                    // only in notation, originating from import.
                    // e.g. YYYY-MM-DD and YYYY\-MM\-DD are identical,
                    // but 0\ 000 0 and 0 000 0 in a French locale are not.

                    SensorCall();sal_Unicode c = sStrArray[i][1];

                    SensorCall();switch (c)
                    {
                    case '+':
                    case '-':
                        SensorCall();rString += OUString(c);
                        SensorCall();break;
                    case ' ':
                    case '.':
                    case '/':
                        SensorCall();if (((eScannedType & css::util::NumberFormat::DATE) == 0) &&
                            (StringEqualsChar( pFormatter->GetNumThousandSep(), c) ||
                             StringEqualsChar( pFormatter->GetNumDecimalSep(), c) ||
                             (c == ' ' &&
                              (StringEqualsChar( pFormatter->GetNumThousandSep(), cNoBreakSpace) ||
                               StringEqualsChar( pFormatter->GetNumThousandSep(), cNarrowNoBreakSpace)))))
                        {
                            SensorCall();rString += sStrArray[i];
                        }
                        else {/*79*/SensorCall();if ((eScannedType & css::util::NumberFormat::DATE) &&
                                 StringEqualsChar( pFormatter->GetDateSep(), c))
                        {
                            SensorCall();rString += sStrArray[i];
                        }
                        else {/*81*/SensorCall();if ((eScannedType & css::util::NumberFormat::TIME) &&
                                 (StringEqualsChar( pLoc->getTimeSep(), c) ||
                                  StringEqualsChar( pLoc->getTime100SecSep(), c)))
                        {
                            SensorCall();rString += sStrArray[i];
                        }
                        else {/*83*/SensorCall();if (eScannedType & css::util::NumberFormat::FRACTION)
                        {
                            SensorCall();rString += sStrArray[i];
                        }
                        else
                        {
                            SensorCall();rString += OUString(c);
                        ;/*84*/}/*82*/}/*80*/}}
                        SensorCall();break;
                    default:
                        SensorCall();rString += sStrArray[i];
                    }
                }
                else
                {
                    SensorCall();rString += sStrArray[i];
                }
                SensorCall();if ( RemoveQuotes( sStrArray[i] ) > 0 )
                {
                    // update currency up to quoted string
                    SensorCall();if ( eScannedType == css::util::NumberFormat::CURRENCY )
                    {
                        // dM -> DM  or  DM -> $  in old automatic
                        // currency formats, oh my ..., why did we ever introduce them?
                        SensorCall();OUString aTmp( pChrCls->uppercase( sStrArray[iPos], nArrPos,
                                                           sStrArray[iPos].getLength()-nArrPos ) );
                        sal_Int32 nCPos = aTmp.indexOf( sOldCurString );
                        SensorCall();if ( nCPos >= 0 )
                        {
                            SensorCall();const OUString& rCur = bConvertMode && bConvertSystemToSystem ?
                                GetCurSymbol() : sOldCurSymbol;
                            sStrArray[iPos] = sStrArray[iPos].replaceAt( nArrPos + nCPos,
                                                                         sOldCurString.getLength(),
                                                                         rCur );
                            rString = rString.replaceAt( nStringPos + nCPos,
                                                         sOldCurString.getLength(),
                                                         rCur );
                        }
                        SensorCall();nStringPos = rString.getLength();
                        SensorCall();if ( iPos == i )
                        {
                            SensorCall();nArrPos = sStrArray[iPos].getLength();
                        }
                        else
                        {
                            SensorCall();nArrPos = sStrArray[iPos].getLength() + sStrArray[i].getLength();
                        }
                    }
                }
                SensorCall();if ( iPos != i )
                {
                    SensorCall();sStrArray[iPos] += sStrArray[i];
                    nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
                    nAnzResStrings--;
                }
                SensorCall();i++;
            }
            while ( i < nAnzStrings && nTypeArray[i] == NF_SYMBOLTYPE_STRING );

            SensorCall();if ( i < nAnzStrings )
            {
                SensorCall();i--; // enter switch on next symbol again
            }
            SensorCall();if ( eScannedType == css::util::NumberFormat::CURRENCY && nStringPos < rString.getLength() )
            {
                // same as above, since last RemoveQuotes
                SensorCall();OUString aTmp( pChrCls->uppercase( sStrArray[iPos], nArrPos,
                                                   sStrArray[iPos].getLength()-nArrPos ) );
                sal_Int32 nCPos = aTmp.indexOf( sOldCurString );
                SensorCall();if ( nCPos >= 0 )
                {
                    SensorCall();const OUString& rCur = bConvertMode && bConvertSystemToSystem ?
                        GetCurSymbol() : sOldCurSymbol;
                    sStrArray[iPos] = sStrArray[iPos].replaceAt( nArrPos + nCPos,
                                                                 sOldCurString.getLength(),
                                                                 rCur );
                    rString = rString.replaceAt( nStringPos + nCPos,
                                                 sOldCurString.getLength(), rCur );
                }
            }
            SensorCall();break;
        case NF_SYMBOLTYPE_CURRENCY :
            SensorCall();rString += sStrArray[i];
            RemoveQuotes( sStrArray[i] );
            SensorCall();break;
        case NF_KEY_THAI_T:
            SensorCall();if (bThaiT && GetNatNumModifier() == 1)
            {
                // Remove T from format code, will be replaced with a [NatNum1] prefix.
                SensorCall();nTypeArray[i] = NF_SYMBOLTYPE_EMPTY;
                nAnzResStrings--;
            }
            else
            {
                SensorCall();rString += sStrArray[i];
            }
            SensorCall();break;
        case NF_SYMBOLTYPE_EMPTY :
            // nothing
            SensorCall();break;
        default:
            SensorCall();rString += sStrArray[i];
        }
        SensorCall();i++;
    }
    {sal_Int32  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

sal_Int32 ImpSvNumberformatScan::RemoveQuotes( OUString& rStr )
{
    SensorCall();if ( rStr.getLength() > 1 )
    {
        SensorCall();sal_Unicode c = rStr[0];
        sal_Int32 n = rStr.getLength() - 1;
        SensorCall();if ( c == '"' && rStr[n] == '"' )
        {
            SensorCall();rStr = rStr.copy( 1, n-1);
            {sal_Int32  ReplaceReturn = 2; SensorCall(); return ReplaceReturn;}
        }
        else {/*85*/SensorCall();if ( c == '\\' )
        {
            SensorCall();rStr = rStr.copy(1);
            {sal_Int32  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
        ;/*86*/}}
    }
    {sal_Int32  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

sal_Int32 ImpSvNumberformatScan::ScanFormat( OUString& rString )
{
    SensorCall();sal_Int32 res = Symbol_Division(rString); // Lexical analysis
    SensorCall();if (!res)
    {
        SensorCall();res = ScanType(); // Recognizing the Format type
    }
    SensorCall();if (!res)
    {
        SensorCall();res = FinalScan( rString ); // Type dependent final analysis
    }
    {sal_Int32  ReplaceReturn = res; SensorCall(); return ReplaceReturn;} // res = control position; res = 0 => Format ok
}

void ImpSvNumberformatScan::CopyInfo(ImpSvNumberformatInfo* pInfo, sal_uInt16 nAnz)
{
    SensorCall();size_t i,j;
    j = 0;
    i = 0;
    SensorCall();while (i < nAnz && j < NF_MAX_FORMAT_SYMBOLS)
    {
        SensorCall();if (nTypeArray[j] != NF_SYMBOLTYPE_EMPTY)
        {
            SensorCall();pInfo->sStrArray[i]  = sStrArray[j];
            pInfo->nTypeArray[i] = nTypeArray[j];
            i++;
        }
        SensorCall();j++;
    }
    SensorCall();pInfo->eScannedType = eScannedType;
    pInfo->bThousand    = bThousand;
    pInfo->nThousand    = nThousand;
    pInfo->nCntPre      = nCntPre;
    pInfo->nCntPost     = nCntPost;
    pInfo->nCntExp      = nCntExp;
SensorCall();}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
