/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This file incorporates work covered by the following license notice:
 *
 *   Licensed to the Apache Software Foundation (ASF) under one or more
 *   contributor license agreements. See the NOTICE file distributed
 *   with this work for additional information regarding copyright
 *   ownership. The ASF licenses this file to you under the Apache
 *   License, Version 2.0 (the "License"); you may not use this file
 *   except in compliance with the License. You may obtain a copy of
 *   the License at http://www.apache.org/licenses/LICENSE-2.0 .
 */

#include <comphelper/stillreadwriteinteraction.hxx>
#include "var/tmp/sensor.h"

#include <com/sun/star/ucb/InteractiveIOException.hpp>

#include <com/sun/star/task/XInteractionAbort.hpp>

#include <com/sun/star/ucb/UnsupportedDataSinkException.hpp>

namespace comphelper{

StillReadWriteInteraction::StillReadWriteInteraction(const css::uno::Reference< css::task::XInteractionHandler >& xHandler)
             : m_bUsed                    (false)
             , m_bHandledByMySelf         (false)
             , m_bHandledByInternalHandler(false)
{
    SensorCall(1);::std::vector< ::ucbhelper::InterceptedInteraction::InterceptedRequest > lInterceptions;
    ::ucbhelper::InterceptedInteraction::InterceptedRequest                  aInterceptedRequest;

    aInterceptedRequest.Handle = HANDLE_INTERACTIVEIOEXCEPTION;
    aInterceptedRequest.Request <<= css::ucb::InteractiveIOException();
    aInterceptedRequest.Continuation = cppu::UnoType<css::task::XInteractionAbort>::get();
    aInterceptedRequest.MatchExact = false;
    lInterceptions.push_back(aInterceptedRequest);

    aInterceptedRequest.Handle = HANDLE_UNSUPPORTEDDATASINKEXCEPTION;
    aInterceptedRequest.Request <<= css::ucb::UnsupportedDataSinkException();
    aInterceptedRequest.Continuation = cppu::UnoType<css::task::XInteractionAbort>::get();
    aInterceptedRequest.MatchExact = false;
    lInterceptions.push_back(aInterceptedRequest);

    setInterceptedHandler(xHandler);
    setInterceptions(lInterceptions);
SensorCall(2);}

void StillReadWriteInteraction::resetInterceptions()
{
    SensorCall(3);setInterceptions(::std::vector< ::ucbhelper::InterceptedInteraction::InterceptedRequest >());
SensorCall(4);}

void StillReadWriteInteraction::resetErrorStates()
{
    SensorCall(5);m_bUsed                     = false;
    m_bHandledByMySelf          = false;
    m_bHandledByInternalHandler = false;
SensorCall(6);}


ucbhelper::InterceptedInteraction::EInterceptionState StillReadWriteInteraction::intercepted(const ::ucbhelper::InterceptedInteraction::InterceptedRequest&                         aRequest,
                                                                  const ::com::sun::star::uno::Reference< ::com::sun::star::task::XInteractionRequest >& xRequest)
{
    // we are used!
    SensorCall(7);m_bUsed = true;

    // check if it's a real interception - might some parameters are not the right ones ...
    bool bAbort = false;
    SensorCall(12);switch(aRequest.Handle)
    {
    case HANDLE_INTERACTIVEIOEXCEPTION:
        {
            SensorCall(8);css::ucb::InteractiveIOException exIO;
            xRequest->getRequest() >>= exIO;
            bAbort = (
                (exIO.Code == css::ucb::IOErrorCode_ACCESS_DENIED     )
                || (exIO.Code == css::ucb::IOErrorCode_LOCKING_VIOLATION )
                || (exIO.Code == css::ucb::IOErrorCode_NOT_EXISTING )
#ifdef MACOSX
                // this is a workaround for MAC, on this platform if the file is locked
                // the returned error code looks to be wrong
                || (exIO.Code == css::ucb::IOErrorCode_GENERAL )
#endif
                );
        }
        SensorCall(9);break;

    case HANDLE_UNSUPPORTEDDATASINKEXCEPTION:
        {
            SensorCall(10);bAbort = true;
        }
        SensorCall(11);break;
    }

    // handle interaction by ourself
    SensorCall(18);if (bAbort)
    {
        SensorCall(13);m_bHandledByMySelf = true;
        css::uno::Reference< css::task::XInteractionContinuation > xAbort = ::ucbhelper::InterceptedInteraction::extractContinuation(
            xRequest->getContinuations(),
            cppu::UnoType<css::task::XInteractionAbort>::get() );
        SensorCall(15);if (!xAbort.is())
            {/*1*/{ucbhelper::InterceptedInteraction::EInterceptionState  ReplaceReturn2 = ::ucbhelper::InterceptedInteraction::E_NO_CONTINUATION_FOUND; SensorCall(14); return ReplaceReturn2;}/*2*/}
        SensorCall(16);xAbort->select();
        {ucbhelper::InterceptedInteraction::EInterceptionState  ReplaceReturn1 = ::ucbhelper::InterceptedInteraction::E_INTERCEPTED; SensorCall(17); return ReplaceReturn1;}
    }

    // Otherwhise use internal handler.
    SensorCall(20);if (m_xInterceptedHandler.is())
    {
        SensorCall(19);m_bHandledByInternalHandler = true;
        m_xInterceptedHandler->handle(xRequest);
    }
    {ucbhelper::InterceptedInteraction::EInterceptionState  ReplaceReturn0 = ::ucbhelper::InterceptedInteraction::E_INTERCEPTED; SensorCall(21); return ReplaceReturn0;}
}
}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
