/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This file incorporates work covered by the following license notice:
 *
 *   Licensed to the Apache Software Foundation (ASF) under one or more
 *   contributor license agreements. See the NOTICE file distributed
 *   with this work for additional information regarding copyright
 *   ownership. The ASF licenses this file to you under the Apache
 *   License, Version 2.0 (the "License"); you may not use this file
 *   except in compliance with the License. You may obtain a copy of
 *   the License at http://www.apache.org/licenses/LICENSE-2.0 .
 */

#include <rtl/math.hxx>
#include "var/tmp/sensor.h"
#include <osl/endian.h>
#include <vcl/graph.hxx>
#include <tools/poly.hxx>
#include <tools/fract.hxx>
#include <vcl/graphicfilter.hxx>
#include "sgffilt.hxx"
#include "sgfbram.hxx"
#include "sgvmain.hxx"
#include "sgvspln.hxx"
#include <unotools/ucbstreamhelper.hxx>

#if defined OSL_BIGENDIAN

#define SWAPPOINT(p) {  \
    p.x=OSL_SWAPWORD(p.x); \
    p.y=OSL_SWAPWORD(p.y); }

#define SWAPPAGE(p) {                         \
    p.Next   =OSL_SWAPDWORD (p.Next   );           \
    p.nList  =OSL_SWAPDWORD (p.nList  );           \
    p.ListEnd=OSL_SWAPDWORD (p.ListEnd);           \
    p.Paper.Size.x=OSL_SWAPWORD(p.Paper.Size.x); \
    p.Paper.Size.y=OSL_SWAPWORD(p.Paper.Size.y); \
    p.Paper.RandL =OSL_SWAPWORD(p.Paper.RandL ); \
    p.Paper.RandR =OSL_SWAPWORD(p.Paper.RandR ); \
    p.Paper.RandO =OSL_SWAPWORD(p.Paper.RandO ); \
    p.Paper.RandU =OSL_SWAPWORD(p.Paper.RandU ); \
    SWAPPOINT(p.U);                           \
    sal_uInt16 iTemp;                             \
    for (iTemp=0;iTemp<20;iTemp++) {          \
        rPage.HlpLnH[iTemp]=OSL_SWAPWORD(rPage.HlpLnH[iTemp]);       \
        rPage.HlpLnV[iTemp]=OSL_SWAPWORD(rPage.HlpLnV[iTemp]);      }}

#define SWAPOBJK(o) {                 \
    o.Last    =OSL_SWAPDWORD (o.Last    ); \
    o.Next    =OSL_SWAPDWORD (o.Next    ); \
    o.MemSize =OSL_SWAPWORD(o.MemSize ); \
    SWAPPOINT(o.ObjMin);              \
    SWAPPOINT(o.ObjMax);              }

#define SWAPLINE(l) {             \
    l.LMSize=OSL_SWAPWORD(l.LMSize); \
    l.LDicke=OSL_SWAPWORD(l.LDicke); }

#define SWAPAREA(a) {               \
    a.FDummy2=OSL_SWAPWORD(a.FDummy2); \
    a.FMuster=OSL_SWAPWORD(a.FMuster); }

#define SWAPTEXT(t) {               \
    SWAPLINE(t.L);                  \
    SWAPAREA(t.F);                  \
    t.FontLo =OSL_SWAPWORD(t.FontLo ); \
    t.FontHi =OSL_SWAPWORD(t.FontHi ); \
    t.Grad   =OSL_SWAPWORD(t.Grad   ); \
    t.Breite =OSL_SWAPWORD(t.Breite ); \
    t.Schnitt=OSL_SWAPWORD(t.Schnitt); \
    t.LnFeed =OSL_SWAPWORD(t.LnFeed ); \
    t.Slant  =OSL_SWAPWORD(t.Slant  ); \
    SWAPLINE(t.ShdL);               \
    SWAPAREA(t.ShdF);               \
    SWAPPOINT(t.ShdVers);           \
    SWAPAREA(t.BackF);              }

#endif

//  Restrictions:

//  - area patterns are matched to the available ones in Starview.
//  - line ends are always rounded in StarView and continue past the end of line.
//  - line patterns are matched to the available ones in Starview.
//    transparency/opacity is not taken into account
//  - no rotated ellipses

// for font translation
SgfFontLst* pSgfFonts = 0;

// for circle kinds, text and rotated rectangles
void RotatePoint(PointType& P, sal_Int16 cx, sal_Int16 cy, double sn, double cs)
{
    SensorCall();sal_Int16  dx,dy;
    double x1,y1;
    dx=P.x-cx;
    dy=P.y-cy;
    x1=dx*cs-dy*sn;
    y1=dy*cs+dx*sn;
    P.x=cx+sal_Int16(x1);
    P.y=cy+sal_Int16(y1);
SensorCall();}

void RotatePoint(Point& P, sal_Int16 cx, sal_Int16 cy, double sn, double cs)
{
    SensorCall();sal_Int16  dx,dy;
    double x1,y1;
    dx=(sal_Int16)(P.X()-cx);
    dy=(sal_Int16)(P.Y()-cy);
    x1=dx*cs-dy*sn;
    y1=dy*cs+dx*sn;
    P=Point(cx+sal_Int16(x1),cy+sal_Int16(y1));
SensorCall();}

sal_Int16 iMulDiv(sal_Int16 a, sal_Int16 Mul, sal_Int16 Div)
{
    SensorCall();sal_Int32 Temp;
    Temp=sal_Int32(a)*sal_Int32(Mul)/sal_Int32(Div);
    {sal_Int16  ReplaceReturn = sal_Int16(Temp); SensorCall(); return ReplaceReturn;}
}

sal_uInt16 MulDiv(sal_uInt16 a, sal_uInt16 Mul, sal_uInt16 Div)
{
    SensorCall();sal_uInt32 Temp;
    Temp=sal_uInt32(a)*sal_uInt32(Mul)/sal_uInt32(Div);
    {sal_uInt16  ReplaceReturn = sal_uInt16(Temp); SensorCall(); return ReplaceReturn;}
}

// SgfFilterSDrw

SvStream& ReadDtHdType(SvStream& rIStream, DtHdType& rDtHd)
{
    SensorCall();rIStream.Read(&rDtHd.Reserved[0], DtHdSize);
    {SvStream & ReplaceReturn = rIStream; SensorCall(); return ReplaceReturn;}
}

void DtHdOverSeek(SvStream& rInp)
{
    SensorCall();sal_uLong FPos=rInp.Tell();
    FPos+=(sal_uLong)DtHdSize;
    rInp.Seek(FPos);
SensorCall();}

PageType::PageType()
{
    SensorCall();memset( this, 0, sizeof( PageType ) );
SensorCall();}

SvStream& ReadPageType(SvStream& rIStream, PageType& rPage)
{
    SensorCall();rIStream.Read(&rPage.Next, PageSize);
#if defined OSL_BIGENDIAN
    SWAPPAGE(rPage);
#endif
    {SvStream & ReplaceReturn = rIStream; SensorCall(); return ReplaceReturn;}
}

void ObjkOverSeek(SvStream& rInp, ObjkType& rObjk)
{
    SensorCall();sal_uLong Siz;
    Siz=(sal_uLong)rObjk.MemSize+rObjk.Last;  // ObjSize+ObjAnhSize
    rInp.Seek(rInp.Tell()+Siz);
SensorCall();}

SvStream& ReadObjkType(SvStream& rInp, ObjkType& rObjk)
{
    // fileposition in stream is not changed!
    SensorCall();sal_uLong nPos;
    nPos=rInp.Tell();
    rInp.Read(&rObjk.Last, ObjkSize);
#if defined OSL_BIGENDIAN
    SWAPOBJK(rObjk);
#endif
    rInp.Seek(nPos);
    {SvStream & ReplaceReturn = rInp; SensorCall(); return ReplaceReturn;}
}
SvStream& ReadStrkType(SvStream& rInp, StrkType& rStrk)
{
    SensorCall();rInp.Read(&rStrk.Last, StrkSize);
#if defined OSL_BIGENDIAN
    SWAPOBJK (rStrk);
    SWAPLINE (rStrk.L);
    SWAPPOINT(rStrk.Pos1);
    SWAPPOINT(rStrk.Pos2);
#endif
    {SvStream & ReplaceReturn = rInp; SensorCall(); return ReplaceReturn;}
}
SvStream& ReadRectType(SvStream& rInp, RectType& rRect)
{
    SensorCall();rInp.Read(&rRect.Last, RectSize);
#if defined OSL_BIGENDIAN
    SWAPOBJK (rRect);
    SWAPLINE (rRect.L);
    SWAPAREA (rRect.F);
    SWAPPOINT(rRect.Pos1);
    SWAPPOINT(rRect.Pos2);
    rRect.Radius        = OSL_SWAPWORD(rRect.Radius  );
    rRect.RotationAngle = OSL_SWAPWORD(rRect.RotationAngle);
    rRect.Slant         = OSL_SWAPWORD(rRect.Slant   );
#endif
    {SvStream & ReplaceReturn = rInp; SensorCall(); return ReplaceReturn;}
}
SvStream& ReadPolyType(SvStream& rInp, PolyType& rPoly)
{
    SensorCall();rInp.Read(&rPoly.Last, PolySize);
#if defined OSL_BIGENDIAN
    SWAPOBJK (rPoly);
    SWAPLINE (rPoly.L);
    SWAPAREA (rPoly.F);
#endif
    {SvStream & ReplaceReturn = rInp; SensorCall(); return ReplaceReturn;}
}
SvStream& ReadSplnType(SvStream& rInp, SplnType& rSpln)
{
    SensorCall();rInp.Read(&rSpln.Last, SplnSize);
#if defined OSL_BIGENDIAN
    SWAPOBJK (rSpln);
    SWAPLINE (rSpln.L);
    SWAPAREA (rSpln.F);
#endif
    {SvStream & ReplaceReturn = rInp; SensorCall(); return ReplaceReturn;}
}
SvStream& ReadCircType(SvStream& rInp, CircType& rCirc)
{
    SensorCall();rInp.Read(&rCirc.Last, CircSize);
#if defined OSL_BIGENDIAN
    SWAPOBJK (rCirc);
    SWAPLINE (rCirc.L);
    SWAPAREA (rCirc.F);
    SWAPPOINT(rCirc.Radius);
    SWAPPOINT(rCirc.Center);
    rCirc.RotationAngle = OSL_SWAPWORD(rCirc.RotationAngle );
    rCirc.StartAngle     = OSL_SWAPWORD(rCirc.StartAngle);
    rCirc.RelAngle       = OSL_SWAPWORD(rCirc.RelAngle  );
#endif
    {SvStream & ReplaceReturn = rInp; SensorCall(); return ReplaceReturn;}
}
SvStream& ReadTextType(SvStream& rInp, TextType& rText)
{
    SensorCall();rInp.Read(&rText.Last, TextSize);
#if defined OSL_BIGENDIAN
    SWAPOBJK (rText);
    SWAPTEXT (rText.T);
    SWAPPOINT(rText.Pos1);
    SWAPPOINT(rText.Pos2);
    rText.TopOfs        = OSL_SWAPWORD(rText.TopOfs  );
    rText.RotationAngle = OSL_SWAPWORD(rText.RotationAngle);
    rText.BoxSlant      = OSL_SWAPWORD(rText.BoxSlant);
    rText.BufSize       = OSL_SWAPWORD(rText.BufSize );
    SWAPPOINT(rText.FitSize);
    rText.FitBreit      = OSL_SWAPWORD(rText.FitBreit);
#endif
    rText.Buffer=NULL;
    {SvStream & ReplaceReturn = rInp; SensorCall(); return ReplaceReturn;}
}
SvStream& ReadBmapType(SvStream& rInp, BmapType& rBmap)
{
    SensorCall();rInp.Read(&rBmap.Last, BmapSize);
#if defined OSL_BIGENDIAN
    SWAPOBJK (rBmap);
    SWAPAREA (rBmap.F);
    SWAPPOINT(rBmap.Pos1);
    SWAPPOINT(rBmap.Pos2);
    rBmap.RotationAngle = OSL_SWAPWORD(rBmap.RotationAngle);
    rBmap.Slant         = OSL_SWAPWORD(rBmap.Slant   );
    SWAPPOINT(rBmap.PixSize);
#endif
    {SvStream & ReplaceReturn = rInp; SensorCall(); return ReplaceReturn;}
}
SvStream& ReadGrupType(SvStream& rInp, GrupType& rGrup)
{
    SensorCall();rInp.Read(&rGrup.Last, GrupSize);
#if defined OSL_BIGENDIAN
    SWAPOBJK (rGrup);
    rGrup.SbLo     =OSL_SWAPWORD(rGrup.SbLo     );
    rGrup.SbHi     =OSL_SWAPWORD(rGrup.SbHi     );
    rGrup.UpLo     =OSL_SWAPWORD(rGrup.UpLo     );
    rGrup.UpHi     =OSL_SWAPWORD(rGrup.UpHi     );
    rGrup.ChartSize=OSL_SWAPWORD(rGrup.ChartSize);
    rGrup.ChartPtr =OSL_SWAPDWORD (rGrup.ChartPtr );
#endif
    {SvStream & ReplaceReturn = rInp; SensorCall(); return ReplaceReturn;}
}

Color Sgv2SvFarbe(sal_uInt8 nFrb1, sal_uInt8 nFrb2, sal_uInt8 nInts)
{
    SensorCall();sal_uInt16 r1=0,g1=0,b1=0,r2=0,g2=0,b2=0;
    sal_uInt8   nInt2=100-nInts;
    SensorCall();switch(nFrb1 & 0x07) {
        case 0:  SensorCall();r1=0xFF; g1=0xFF; b1=0xFF; SensorCall();break;
        case 1:  SensorCall();r1=0xFF; g1=0xFF;          SensorCall();break;
        case 2:           SensorCall();g1=0xFF; b1=0xFF; SensorCall();break;
        case 3:           SensorCall();g1=0xFF;          SensorCall();break;
        case 4:  SensorCall();r1=0xFF;          b1=0xFF; SensorCall();break;
        case 5:  SensorCall();r1=0xFF;                   SensorCall();break;
        case 6:                    SensorCall();b1=0xFF; SensorCall();break;
        case 7:                             SensorCall();break;
    }
    SensorCall();switch(nFrb2 & 0x07) {
        case 0:  SensorCall();r2=0xFF; g2=0xFF; b2=0xFF; SensorCall();break;
        case 1:  SensorCall();r2=0xFF; g2=0xFF;          SensorCall();break;
        case 2:           SensorCall();g2=0xFF; b2=0xFF; SensorCall();break;
        case 3:           SensorCall();g2=0xFF;          SensorCall();break;
        case 4:  SensorCall();r2=0xFF;          b2=0xFF; SensorCall();break;
        case 5:  SensorCall();r2=0xFF;                   SensorCall();break;
        case 6:                    SensorCall();b2=0xFF; SensorCall();break;
        case 7:                             SensorCall();break;
    }
    SensorCall();r1=(sal_uInt16)((sal_uInt32)r1*nInts/100+(sal_uInt32)r2*nInt2/100);
    g1=(sal_uInt16)((sal_uInt32)g1*nInts/100+(sal_uInt32)g2*nInt2/100);
    b1=(sal_uInt16)((sal_uInt32)b1*nInts/100+(sal_uInt32)b2*nInt2/100);
    Color aColor( (sal_uInt8)r1, (sal_uInt8)g1, (sal_uInt8)b1 );
    {Color  ReplaceReturn = aColor; SensorCall(); return ReplaceReturn;}
}

void SetLine(ObjLineType& rLine, OutputDevice& rOut)
{
    SensorCall();if( 0 == ( rLine.LMuster & 0x07 ) )
        {/*1*/SensorCall();rOut.SetLineColor();/*2*/}
    else
        {/*3*/SensorCall();rOut.SetLineColor( Sgv2SvFarbe(rLine.LFarbe,rLine.LBFarbe,rLine.LIntens) );/*4*/}
SensorCall();}

void SetArea(ObjAreaType& rArea, OutputDevice& rOut)
{
    SensorCall();if( 0 == ( rArea.FMuster & 0x00FF ) )
        {/*5*/SensorCall();rOut.SetFillColor();/*6*/}
    else
        {/*7*/SensorCall();rOut.SetFillColor( Sgv2SvFarbe( rArea.FFarbe,rArea.FBFarbe,rArea.FIntens ) );/*8*/}
SensorCall();}

void ObjkType::Draw(OutputDevice&)
{
SensorCall();SensorCall();}

void Obj0Type::Draw(OutputDevice&) {SensorCall();}

void StrkType::Draw(OutputDevice& rOut)
{
    SensorCall();SetLine(L,rOut);
    rOut.DrawLine(Point(Pos1.x,Pos1.y),Point(Pos2.x,Pos2.y)); // !!!
SensorCall();}

void SgfAreaColorIntens(sal_uInt16 Muster, sal_uInt8 Col1, sal_uInt8 Col2, sal_uInt8 Int, OutputDevice& rOut)
{
    SensorCall();ObjAreaType F;
    F.FMuster=Muster;
    F.FFarbe=Col2;
    F.FBFarbe=Col1;
    F.FIntens=Int;
    SetArea(F,rOut);
SensorCall();}

void DrawSlideRect(sal_Int16 x1, sal_Int16 y1, sal_Int16 x2, sal_Int16 y2, ObjAreaType& F, OutputDevice& rOut)
{
    SensorCall();sal_Int16 i,i0,b,b0;
    sal_Int16 Int1,Int2;
    sal_Int16 Col1,Col2;
    //     ClipMerk: HgdClipRec;
    sal_Int16 cx,cy;
    sal_Int16 MaxR;
    sal_Int32 dx,dy;

    rOut.SetLineColor();
    SensorCall();if (x1>x2) { SensorCall();i=x1; x1=x2; x2=i; }
    SensorCall();if (y1>y2) { SensorCall();i=y1; y1=y2; y2=i; }
    SensorCall();Col1=F.FBFarbe & 0x87; Col2=F.FFarbe & 0x87;
    Int1=100-F.FIntens; Int2=F.FIntens;
    SensorCall();if (Int1==Int2) {
        SensorCall();SgfAreaColorIntens(F.FMuster,(sal_uInt8)Col1,(sal_uInt8)Col2,(sal_uInt8)Int2,rOut);
        rOut.DrawRect(Rectangle(x1,y1,x2,y2));
    } else {
        SensorCall();b0=Int1;
        SensorCall();switch (F.FBFarbe & 0x38) {
            case 0x08: { // vertikal
                SensorCall();i0=y1;
                i=y1;
                SensorCall();while (i<=y2) {
                    SensorCall();b=Int1+sal_Int16((sal_Int32)(Int2-Int1)*(sal_Int32)(i-y1) /(sal_Int32)(y2-y1+1));
                    SensorCall();if (b!=b0) {
                        SensorCall();SgfAreaColorIntens(F.FMuster,(sal_uInt8)Col1,(sal_uInt8)Col2,(sal_uInt8)b0,rOut);
                        rOut.DrawRect(Rectangle(x1,i0,x2,i-1));
                        i0=i; b0=b;
                    }
                    SensorCall();i++;
                }
                SensorCall();SgfAreaColorIntens(F.FMuster,(sal_uInt8)Col1,(sal_uInt8)Col2,(sal_uInt8)Int2,rOut);
                rOut.DrawRect(Rectangle(x1,i0,x2,y2));
            } SensorCall();break;
            case 0x28: { // horizontal
                SensorCall();i0=x1;
                i=x1;
                SensorCall();while (i<=x2) {
                    SensorCall();b=Int1+sal_Int16((sal_Int32)(Int2-Int1)*(sal_Int32)(i-x1) /(sal_Int32)(x2-x1+1));
                    SensorCall();if (b!=b0) {
                        SensorCall();SgfAreaColorIntens(F.FMuster,(sal_uInt8)Col1,(sal_uInt8)Col2,(sal_uInt8)b0,rOut);
                        rOut.DrawRect(Rectangle(i0,y1,i-1,y2));
                        i0=i; b0=b;
                    }
                    SensorCall();i++;
                }
                SensorCall();SgfAreaColorIntens(F.FMuster,(sal_uInt8)Col1,(sal_uInt8)Col2,(sal_uInt8)Int2,rOut);
                rOut.DrawRect(Rectangle(i0,y1,x2,y2));
            } SensorCall();break;

            case 0x18: case 0x38: { // circle
                SensorCall();vcl::Region ClipMerk=rOut.GetClipRegion();
                double a;

                rOut.SetClipRegion(vcl::Region(Rectangle(x1,y1,x2,y2)));
                cx=(x1+x2) /2;
                cy=(y1+y2) /2;
                dx=x2-x1+1;
                dy=y2-y1+1;
                a=sqrt((double)(dx*dx+dy*dy));
                MaxR=sal_Int16(a) /2 +1;
                b0=Int2;
                i0=MaxR; SensorCall();if (MaxR<1) {/*9*/SensorCall();MaxR=1;/*10*/}
                SensorCall();i=MaxR;
                SensorCall();while (i>=0) {
                    SensorCall();b=Int1+sal_Int16((sal_Int32(Int2-Int1)*sal_Int32(i)) /sal_Int32(MaxR));
                    SensorCall();if (b!=b0) {
                        SensorCall();SgfAreaColorIntens(F.FMuster,(sal_uInt8)Col1,(sal_uInt8)Col2,(sal_uInt8)b0,rOut);
                        rOut.DrawEllipse(Rectangle(cx-i0,cy-i0,cx+i0,cy+i0));
                        i0=i; b0=b;
                    }
                    SensorCall();i--;
                }
                SensorCall();SgfAreaColorIntens(F.FMuster,(sal_uInt8)Col1,(sal_uInt8)Col2,(sal_uInt8)Int1,rOut);
                rOut.DrawEllipse(Rectangle(cx-i0,cy-i0,cx+i0,cy+i0));
                rOut.SetClipRegion(ClipMerk);
            } SensorCall();break; // circle
        }
    }
SensorCall();}

void RectType::Draw(OutputDevice& rOut)
{
    SensorCall();if (L.LMuster!=0) {/*11*/SensorCall();L.LMuster=1;/*12*/} // no line separator here, only on or off
    SensorCall();SetArea(F,rOut);
    SensorCall();if (RotationAngle==0) {
    SensorCall();if ((F.FBFarbe & 0x38)==0 || Radius!=0) {
            SensorCall();SetLine(L,rOut);
            rOut.DrawRect(Rectangle(Pos1.x,Pos1.y,Pos2.x,Pos2.y),Radius,Radius);
        } else {
            SensorCall();DrawSlideRect(Pos1.x,Pos1.y,Pos2.x,Pos2.y,F,rOut);
            SensorCall();if (L.LMuster!=0) {
                SensorCall();SetLine(L,rOut);
                rOut.SetFillColor();
                rOut.DrawRect(Rectangle(Pos1.x,Pos1.y,Pos2.x,Pos2.y));
            }
        }
    } else {
        SensorCall();Point  aPts[4];
        sal_uInt16 i;
        double sn,cs;
        sn=sin(double(RotationAngle)*3.14159265359/18000);
        cs=cos(double(RotationAngle)*3.14159265359/18000);
        aPts[0]=Point(Pos1.x,Pos1.y);
        aPts[1]=Point(Pos2.x,Pos1.y);
        aPts[2]=Point(Pos2.x,Pos2.y);
        aPts[3]=Point(Pos1.x,Pos2.y);
        SensorCall();for (i=0;i<4;i++) {
            SensorCall();RotatePoint(aPts[i],Pos1.x,Pos1.y,sn,cs);
        }
        SensorCall();SetLine(L,rOut);
        Polygon aPoly(4,aPts);
        rOut.DrawPolygon(aPoly);
    }
SensorCall();}

void PolyType::Draw(OutputDevice& rOut)
{
    SensorCall();if ((Flags & PolyClosBit) !=0) {/*13*/SensorCall();SetArea(F,rOut);/*14*/}
    SensorCall();SetLine(L,rOut);
    Polygon aPoly(nPoints);
    sal_uInt16 i;
    SensorCall();for(i=0;i<nPoints;i++) {/*15*/SensorCall();aPoly.SetPoint(Point(EckP[i].x,EckP[i].y),i);/*16*/}
    SensorCall();if ((Flags & PolyClosBit) !=0) {
        SensorCall();rOut.DrawPolygon(aPoly);
    } else {
        SensorCall();rOut.DrawPolyLine(aPoly);
    }
SensorCall();}

void SplnType::Draw(OutputDevice& rOut)
{
    SensorCall();if ((Flags & PolyClosBit) !=0) {/*17*/SensorCall();SetArea(F,rOut);/*18*/}
    SensorCall();SetLine(L,rOut);
    Polygon aPoly(0);
    Polygon aSpln(nPoints);
    sal_uInt16 i;
    SensorCall();for(i=0;i<nPoints;i++) {/*19*/SensorCall();aSpln.SetPoint(Point(EckP[i].x,EckP[i].y),i);/*20*/}
    SensorCall();if ((Flags & PolyClosBit) !=0) {
        SensorCall();Spline2Poly(aSpln,true,aPoly);
        SensorCall();if (aPoly.GetSize()>0) {/*21*/SensorCall();rOut.DrawPolygon(aPoly);/*22*/}
    } else {
        SensorCall();Spline2Poly(aSpln,false,aPoly);
        SensorCall();if (aPoly.GetSize()>0) {/*23*/SensorCall();rOut.DrawPolyLine(aPoly);/*24*/}
    }
SensorCall();}

void DrawSlideCirc(sal_Int16 cx, sal_Int16 cy, sal_Int16 rx, sal_Int16 ry, ObjAreaType& F, OutputDevice& rOut)
{
    SensorCall();sal_Int16 x1=cx-rx;
    sal_Int16 y1=cy-ry;
    sal_Int16 x2=cx+rx;
    sal_Int16 y2=cy+ry;

    sal_Int16 i,i0,b,b0;
    sal_Int16 Int1,Int2;
    sal_Int16 Col1,Col2;

    rOut.SetLineColor();
    Col1=F.FBFarbe & 0x87; Col2=F.FFarbe & 0x87;
    Int1=100-F.FIntens; Int2=F.FIntens;
    SensorCall();if (Int1==Int2) {
        SensorCall();SgfAreaColorIntens(F.FMuster,(sal_uInt8)Col1,(sal_uInt8)Col2,(sal_uInt8)Int2,rOut);
        rOut.DrawEllipse(Rectangle(x1,y1,x2,y2));
    } else {
        SensorCall();b0=Int1;
        SensorCall();switch (F.FBFarbe & 0x38) {
            case 0x08: { // vertical
                SensorCall();vcl::Region ClipMerk=rOut.GetClipRegion();
                i0=y1;
                i=y1;
                SensorCall();while (i<=y2) {
                    SensorCall();b=Int1+sal_Int16((sal_Int32)(Int2-Int1)*(sal_Int32)(i-y1) /(sal_Int32)(y2-y1+1));
                    SensorCall();if (b!=b0) {
                        SensorCall();SgfAreaColorIntens(F.FMuster,(sal_uInt8)Col1,(sal_uInt8)Col2,(sal_uInt8)b0,rOut);
                        rOut.SetClipRegion(vcl::Region(Rectangle(x1,i0,x2,i-1)));
                        rOut.DrawEllipse(Rectangle(x1,y1,x2,y2));
                        i0=i; b0=b;
                    }
                    SensorCall();i++;
                }
                SensorCall();SgfAreaColorIntens(F.FMuster,(sal_uInt8)Col1,(sal_uInt8)Col2,(sal_uInt8)Int2,rOut);
                rOut.SetClipRegion(vcl::Region(Rectangle(x1,i0,x2,y2)));
                rOut.DrawEllipse(Rectangle(x1,y1,x2,y2));
                rOut.SetClipRegion(ClipMerk);
            } SensorCall();break;
            case 0x28: { // horizontal
                SensorCall();vcl::Region ClipMerk=rOut.GetClipRegion();
                i0=x1;
                i=x1;
                SensorCall();while (i<=x2) {
                    SensorCall();b=Int1+sal_Int16((sal_Int32)(Int2-Int1)*(sal_Int32)(i-x1) /(sal_Int32)(x2-x1+1));
                    SensorCall();if (b!=b0) {
                        SensorCall();SgfAreaColorIntens(F.FMuster,(sal_uInt8)Col1,(sal_uInt8)Col2,(sal_uInt8)b0,rOut);
                        rOut.SetClipRegion(vcl::Region(Rectangle(i0,y1,i-1,y2)));
                        rOut.DrawEllipse(Rectangle(x1,y1,x2,y2));
                        i0=i; b0=b;
                    }
                    SensorCall();i++;
                }
                SensorCall();SgfAreaColorIntens(F.FMuster,(sal_uInt8)Col1,(sal_uInt8)Col2,(sal_uInt8)Int2,rOut);
                rOut.SetClipRegion(vcl::Region(Rectangle(i0,y1,x2,y2)));
                rOut.DrawEllipse(Rectangle(x1,y1,x2,y2));
                rOut.SetClipRegion(ClipMerk);
            } SensorCall();break;

            case 0x18: case 0x38: { // circle
                SensorCall();sal_Int16 MaxR;

                SensorCall();if (rx<1) {/*25*/SensorCall();rx=1;/*26*/}
                SensorCall();if (ry<1) {/*27*/SensorCall();ry=1;/*28*/}
                SensorCall();MaxR=rx;
                b0=Int2;
                i0=MaxR;
                i=MaxR;
                SensorCall();while (i>=0) {
                    SensorCall();b=Int1+sal_Int16((sal_Int32(Int2-Int1)*sal_Int32(i)) /sal_Int32(MaxR));
                    SensorCall();if (b!=b0) {
                        SensorCall();sal_Int32 temp=sal_Int32(i0)*sal_Int32(ry)/sal_Int32(rx);
                        sal_Int16 j0=sal_Int16(temp);
                        SgfAreaColorIntens(F.FMuster,(sal_uInt8)Col1,(sal_uInt8)Col2,(sal_uInt8)b0,rOut);
                        rOut.DrawEllipse(Rectangle(cx-i0,cy-j0,cx+i0,cy+j0));
                        i0=i; b0=b;
                    }
                    SensorCall();i--;
                }
                SensorCall();SgfAreaColorIntens(F.FMuster,(sal_uInt8)Col1,(sal_uInt8)Col2,(sal_uInt8)Int1,rOut);
                rOut.DrawEllipse(Rectangle(cx-i0,cy-i0,cx+i0,cy+i0));
            } SensorCall();break; // circle
        }
    }
SensorCall();}

void CircType::Draw(OutputDevice& rOut)
{
    SensorCall();Rectangle aRect(Center.x-Radius.x,Center.y-Radius.y,Center.x+Radius.x,Center.y+Radius.y);

    SensorCall();if (L.LMuster!=0) {/*29*/SensorCall();L.LMuster=1;/*30*/} // no line pattern here, only on or off
    SensorCall();SetArea(F,rOut);
    SensorCall();if ((Flags & 0x03)==CircFull) {
        SensorCall();if ((F.FBFarbe & 0x38)==0) {
            SensorCall();SetLine(L,rOut);
            rOut.DrawEllipse(aRect);
        } else {
            SensorCall();DrawSlideCirc(Center.x,Center.y,Radius.x,Radius.y,F,rOut);
            SensorCall();if (L.LMuster!=0) {
                SensorCall();SetLine(L,rOut);
                rOut.SetFillColor();
                rOut.DrawEllipse(aRect);
            }
        }
    } else {
        SensorCall();PointType a,b;
        Point  aStrt,aEnde;
        double sn,cs;

        a.x=Center.x+Radius.x; a.y=Center.y; b=a;
        sn=sin(double(StartAngle)*3.14159265359/18000);
        cs=cos(double(StartAngle)*3.14159265359/18000);
        RotatePoint(a,Center.x,Center.y,sn,cs);
        sn=sin(double(StartAngle+RelAngle)*3.14159265359/18000);
        cs=cos(double(StartAngle+RelAngle)*3.14159265359/18000);
        RotatePoint(b,Center.x,Center.y,sn,cs);
        SensorCall();if (Radius.x!=Radius.y) {
          SensorCall();if (Radius.x<1) {/*31*/SensorCall();Radius.x=1;/*32*/}
          SensorCall();if (Radius.y<1) {/*33*/SensorCall();Radius.y=1;/*34*/}
          SensorCall();a.y = a.y - Center.y;
          b.y = b.y - Center.y;
          a.y=iMulDiv(a.y,Radius.y,Radius.x);
          b.y=iMulDiv(b.y,Radius.y,Radius.x);
          a.y = a.y + Center.y;
          b.y = b.y + Center.y;
        }
        SensorCall();aStrt=Point(a.x,a.y);
        aEnde=Point(b.x,b.y);
        SetLine(L,rOut);
        SensorCall();switch (Flags & 0x03) {
            case CircArc : SensorCall();rOut.DrawArc(aRect,aEnde,aStrt); SensorCall();break;
            case CircSect:
            case CircAbsn: SensorCall();rOut.DrawPie(aRect,aEnde,aStrt); SensorCall();break;
        }
    }
SensorCall();}

void BmapType::Draw(OutputDevice& rOut)
{
    //ifstream aInp;
    SensorCall();unsigned char   nSgfTyp;
    sal_uInt16      nVersion;
    OUString        aStr(
        reinterpret_cast< char const * >(&Filename[ 1 ]),
        (sal_Int32)Filename[ 0 ], RTL_TEXTENCODING_UTF8 );
    INetURLObject   aFNam( aStr );

    SvStream* pInp = ::utl::UcbStreamHelper::CreateStream( aFNam.GetMainURL( INetURLObject::NO_DECODE ), StreamMode::READ );
    SensorCall();if ( pInp )
    {
        SensorCall();nSgfTyp=CheckSgfTyp( *pInp,nVersion);
        SensorCall();switch(nSgfTyp) {
            case SGF_BITIMAGE: {
                SensorCall();GraphicFilter aFlt;
                Graphic aGrf;
                aFlt.ImportGraphic(aGrf,aFNam);
                aGrf.Draw(&rOut,Point(Pos1.x,Pos1.y),Size(Pos2.x-Pos1.x,Pos2.y-Pos1.y));
            } SensorCall();break;
            case SGF_SIMPVECT: {
                SensorCall();GDIMetaFile aMtf;
                SgfVectXofs=Pos1.x;
                SgfVectYofs=Pos1.y;
                SgfVectXmul=Pos2.x-Pos1.x;
                SgfVectYmul=Pos2.y-Pos1.y;
                SgfVectXdiv=0;
                SgfVectYdiv=0;
                SgfVectScal=true;
                SgfVectFilter(*pInp,aMtf);
                SgfVectXofs=0;
                SgfVectYofs=0;
                SgfVectXmul=0;
                SgfVectYmul=0;
                SgfVectXdiv=0;
                SgfVectYdiv=0;
                SgfVectScal=false;
                aMtf.Play(&rOut);
            } SensorCall();break;
        }
        SensorCall();delete pInp;
    }
SensorCall();}

sal_uInt32 GrupType::GetSubPtr()
{
    {sal_uInt32  ReplaceReturn = sal_uInt32(SbLo)+0x00010000*sal_uInt32(SbHi); SensorCall(); return ReplaceReturn;}
}

void DrawObjkList( SvStream& rInp, OutputDevice& rOut )
{
    SensorCall();ObjkType aObjk;
    sal_uInt16 nGrpCnt=0;
    bool bEnd=false;
    SensorCall();do {
        SensorCall();ReadObjkType( rInp, aObjk );
        SensorCall();if (!rInp.GetError()) {
            SensorCall();switch(aObjk.Art) {
                case ObjStrk: { SensorCall();StrkType aStrk; ReadStrkType( rInp, aStrk ); SensorCall();if (!rInp.GetError()) {/*35*/SensorCall();aStrk.Draw(rOut);/*36*/} } SensorCall();break;
                case ObjRect: { SensorCall();RectType aRect; ReadRectType( rInp, aRect ); SensorCall();if (!rInp.GetError()) {/*37*/SensorCall();aRect.Draw(rOut);/*38*/} } SensorCall();break;
                case ObjCirc: { SensorCall();CircType aCirc; ReadCircType( rInp, aCirc ); SensorCall();if (!rInp.GetError()) {/*39*/SensorCall();aCirc.Draw(rOut);/*40*/} } SensorCall();break;
                case ObjText: {
                    SensorCall();TextType aText;
                    ReadTextType( rInp, aText );
                    SensorCall();if (!rInp.GetError()) {
                        SensorCall();aText.Buffer=new UCHAR[aText.BufSize+1]; // add one for LookAhead at CK-separation
                        rInp.Read(aText.Buffer, aText.BufSize);
                        SensorCall();if (!rInp.GetError()) {/*41*/SensorCall();aText.Draw(rOut);/*42*/}
                        SensorCall();delete[] aText.Buffer;
                    }
                } SensorCall();break;
                case ObjBmap: {
                    SensorCall();BmapType aBmap;
                    ReadBmapType( rInp, aBmap );
                    SensorCall();if (!rInp.GetError()) {
                        SensorCall();aBmap.Draw(rOut);
                    }
                } SensorCall();break;
                case ObjPoly: {
                    SensorCall();PolyType aPoly;
                    ReadPolyType( rInp, aPoly );
                    SensorCall();if (!rInp.GetError()) {
                        SensorCall();aPoly.EckP=new PointType[aPoly.nPoints];
                        rInp.Read(aPoly.EckP, 4*aPoly.nPoints);
#if defined OSL_BIGENDIAN
                        for(short i=0;i<aPoly.nPoints;i++) SWAPPOINT(aPoly.EckP[i]);
#endif
                        SensorCall();if (!rInp.GetError()) {/*43*/SensorCall();aPoly.Draw(rOut);/*44*/}
                        SensorCall();delete[] aPoly.EckP;
                    }
                } SensorCall();break;
                case ObjSpln: {
                    SensorCall();SplnType aSpln;
                    ReadSplnType( rInp, aSpln );
                    SensorCall();if (!rInp.GetError()) {
                        SensorCall();aSpln.EckP=new PointType[aSpln.nPoints];
                        rInp.Read(aSpln.EckP, 4*aSpln.nPoints);
#if defined OSL_BIGENDIAN
                        for(short i=0;i<aSpln.nPoints;i++) SWAPPOINT(aSpln.EckP[i]);
#endif
                        SensorCall();if (!rInp.GetError()) {/*45*/SensorCall();aSpln.Draw(rOut);/*46*/}
                        SensorCall();delete[] aSpln.EckP;
                    }
                } SensorCall();break;
                case ObjGrup: {
                    SensorCall();GrupType aGrup;
                    ReadGrupType( rInp, aGrup );
                    SensorCall();if (!rInp.GetError()) {
                        SensorCall();rInp.Seek(rInp.Tell()+aGrup.Last);   // object appendix
                        SensorCall();if(aGrup.GetSubPtr()!=0L) {/*47*/SensorCall();nGrpCnt++;/*48*/} // DrawObjkList(rInp,rOut );
                    }
                } SensorCall();break;
                default: {
                    SensorCall();aObjk.Draw(rOut);          // object name on 2. Screen
                    ObjkOverSeek(rInp,aObjk);  // to next object
                }
            }
        } // if rInp
        SensorCall();if (!rInp.GetError()) {
            SensorCall();if (aObjk.Next==0L) {
                SensorCall();if (nGrpCnt==0) {/*49*/SensorCall();bEnd=true;/*50*/}
                else {/*51*/SensorCall();nGrpCnt--;/*52*/}
            }
        } else {
            SensorCall();bEnd=true;  // read error
        }
    } while (!bEnd);
SensorCall();}

void SkipObjkList(SvStream& rInp)
{
    SensorCall();ObjkType aObjk;
    SensorCall();do
    {
        SensorCall();ReadObjkType( rInp, aObjk );
        SensorCall();if(aObjk.Art==ObjGrup) {
            SensorCall();GrupType aGrup;
            ReadGrupType( rInp, aGrup );
            rInp.Seek(rInp.Tell()+aGrup.Last); // object appendix
            SensorCall();if(aGrup.GetSubPtr()!=0L) {/*53*/SensorCall();SkipObjkList(rInp);/*54*/}
        } else {
            SensorCall();ObjkOverSeek(rInp,aObjk);  // to next object
        }
    } while (aObjk.Next!=0L && !rInp.GetError());
SensorCall();}

bool SgfFilterSDrw( SvStream& rInp, SgfHeader&, SgfEntry&, GDIMetaFile& rMtf )
{
    SensorCall();bool          bRet = false;
    PageType      aPage;
    ScopedVclPtrInstance< VirtualDevice > aOutDev;
    OutputDevice* pOutDev;
    sal_uLong         nStdPos;
    sal_uLong         nCharPos;
    sal_uInt16        Num;

    pOutDev=aOutDev.get();
    DtHdOverSeek(rInp); // read dataheader

    nStdPos=rInp.Tell();
    SensorCall();do {                // read standard page
        SensorCall();ReadPageType( rInp, aPage );
        SensorCall();if (aPage.nList!=0) {/*55*/SensorCall();SkipObjkList(rInp);/*56*/}
    } while (aPage.Next!=0L && !rInp.GetError());

    SensorCall();nCharPos=rInp.Tell();
    ReadPageType( rInp, aPage );

    rMtf.Record(pOutDev);
    Num=aPage.StdPg;
    SensorCall();if (Num!=0) {
      SensorCall();rInp.Seek(nStdPos);
      SensorCall();while(Num>1 && aPage.Next!=0L && !rInp.GetError()) { // search standard page
        SensorCall();ReadPageType( rInp, aPage );
        SensorCall();if (aPage.nList!=0) {/*57*/SensorCall();SkipObjkList(rInp);/*58*/}
        SensorCall();Num--;
      }
      SensorCall();ReadPageType( rInp, aPage );
      SensorCall();if(Num==1 && aPage.nList!=0L) {/*59*/SensorCall();DrawObjkList( rInp,*pOutDev );/*60*/}
      SensorCall();rInp.Seek(nCharPos);
      nCharPos=rInp.Tell();
      ReadPageType( rInp, aPage );
    }
    SensorCall();if (aPage.nList!=0L) {/*61*/SensorCall();DrawObjkList(rInp,*pOutDev );/*62*/}

    SensorCall();rMtf.Stop();
    rMtf.WindStart();
    MapMode aMap(MAP_10TH_MM,Point(),Fraction(1,4),Fraction(1,4));
    rMtf.SetPrefMapMode(aMap);
    rMtf.SetPrefSize(Size((sal_Int16)aPage.Paper.Size.x,(sal_Int16)aPage.Paper.Size.y));
    bRet=true;
    {_Bool  ReplaceReturn = bRet; SensorCall(); return ReplaceReturn;}
}

bool SgfSDrwFilter(SvStream& rInp, GDIMetaFile& rMtf, const INetURLObject& _aIniPath )
{
#if OSL_DEBUG_LEVEL > 1 // check record size. New compiler possibly aligns different!
    if (sizeof(ObjTextType)!=ObjTextTypeSize)  return false;
#endif

    SensorCall();sal_uLong   nFileStart;        // offset of SgfHeaders. In general 0.
    SgfHeader   aHead;
    SgfEntry    aEntr;
    sal_uLong   nNext;
    bool        bRet=false;        // return value

    INetURLObject aIniPath = _aIniPath;
    aIniPath.Append(OUString("sgf.ini"));

    pSgfFonts = new SgfFontLst;

    pSgfFonts->AssignFN( aIniPath.GetMainURL( INetURLObject::NO_DECODE ) );
    nFileStart=rInp.Tell();
    ReadSgfHeader( rInp, aHead );
    SensorCall();if (aHead.ChkMagic() && aHead.Typ==SgfStarDraw && aHead.Version==SGV_VERSION) {
        SensorCall();nNext=aHead.GetOffset();
        SensorCall();while (nNext && !rInp.GetError()) {
            SensorCall();rInp.Seek(nFileStart+nNext);
            ReadSgfEntry( rInp, aEntr );
            nNext=aEntr.GetOffset();
            SensorCall();if (aEntr.Typ==aHead.Typ) {
                SensorCall();bRet=SgfFilterSDrw( rInp,aHead,aEntr,rMtf );
            }
        } // while(nNext)
    }
    SensorCall();delete pSgfFonts;
    {_Bool  ReplaceReturn = bRet; SensorCall(); return ReplaceReturn;}
}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
