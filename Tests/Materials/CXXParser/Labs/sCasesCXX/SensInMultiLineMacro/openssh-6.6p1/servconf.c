
/* $OpenBSD: servconf.c,v 1.249 2014/01/29 06:18:35 djm Exp $ */
/*
 * Copyright (c) 1995 Tatu Ylonen <ylo@cs.hut.fi>, Espoo, Finland
 *                    All rights reserved
 *
 * As far as I am concerned, the code I have written for this software
 * can be used freely for any purpose.  Any derived versions of this
 * software must be clearly marked as such, and if the derived work is
 * incompatible with the protocol description in the RFC file, it must be
 * called by a name other than "ssh" or "Secure Shell".
 */

#include "includes.h"
#include "var/tmp/sensor.h"

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>

#include <ctype.h>
#include <netdb.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <stdarg.h>
#include <errno.h>
#ifdef HAVE_UTIL_H
#include <util.h>
#endif

#include "openbsd-compat/sys-queue.h"
#include "xmalloc.h"
#include "ssh.h"
#include "log.h"
#include "buffer.h"
#include "servconf.h"
#include "compat.h"
#include "pathnames.h"
#include "misc.h"
#include "cipher.h"
#include "key.h"
#include "kex.h"
#include "mac.h"
#include "match.h"
#include "channels.h"
#include "groupaccess.h"
#include "canohost.h"
#include "packet.h"
#include "hostfile.h"
#include "auth.h"
#include "ssh-gss.h"

static void add_listen_addr(ServerOptions *, char *, int);
static void add_one_listen_addr(ServerOptions *, char *, int);

/* Use of privilege separation or not */
extern int use_privsep;
extern Buffer cfg;

/* Initializes the server options to their default values. */

void
initialize_server_options(ServerOptions *options)
{
	Din_Go(1,2048);memset(options, 0, sizeof(*options));

	/* Portable-specific options */
	options->use_pam = -1;

	/* Standard Options */
	options->num_ports = 0;
	options->ports_from_cmdline = 0;
	options->listen_addrs = NULL;
	options->address_family = -1;
	options->num_host_key_files = 0;
	options->num_host_cert_files = 0;
	options->host_key_agent = NULL;
	options->pid_file = NULL;
	options->server_key_bits = -1;
	options->login_grace_time = -1;
	options->key_regeneration_time = -1;
	options->permit_root_login = PERMIT_NOT_SET;
	options->ignore_rhosts = -1;
	options->ignore_user_known_hosts = -1;
	options->print_motd = -1;
	options->print_lastlog = -1;
	options->x11_forwarding = -1;
	options->x11_display_offset = -1;
	options->x11_max_displays = -1;
	options->x11_use_localhost = -1;
	options->permit_tty = -1;
	options->xauth_location = NULL;
	options->strict_modes = -1;
	options->tcp_keep_alive = -1;
	options->log_facility = SYSLOG_FACILITY_NOT_SET;
	options->log_level = SYSLOG_LEVEL_NOT_SET;
	options->rhosts_rsa_authentication = -1;
	options->hostbased_authentication = -1;
	options->hostbased_uses_name_from_packet_only = -1;
	options->rsa_authentication = -1;
	options->pubkey_authentication = -1;
	options->kerberos_authentication = -1;
	options->kerberos_or_local_passwd = -1;
	options->kerberos_ticket_cleanup = -1;
	options->kerberos_get_afs_token = -1;
	options->gss_authentication=-1;
	options->gss_keyex = -1;
	options->gss_cleanup_creds = -1;
	options->gss_strict_acceptor = -1;
	options->gss_store_rekey = -1;
	options->gss_kex_algorithms = NULL;
	options->password_authentication = -1;
	options->kbd_interactive_authentication = -1;
	options->challenge_response_authentication = -1;
	options->permit_empty_passwd = -1;
	options->permit_user_env = -1;
	options->use_login = -1;
	options->compression = -1;
	options->rekey_limit = -1;
	options->rekey_interval = -1;
	options->allow_tcp_forwarding = -1;
	options->allow_agent_forwarding = -1;
	options->num_allow_users = 0;
	options->num_deny_users = 0;
	options->num_allow_groups = 0;
	options->num_deny_groups = 0;
	options->ciphers = NULL;
	options->macs = NULL;
	options->kex_algorithms = NULL;
	options->protocol = SSH_PROTO_UNKNOWN;
	options->gateway_ports = -1;
	options->num_subsystems = 0;
	options->max_startups_begin = -1;
	options->max_startups_rate = -1;
	options->max_startups = -1;
	options->max_authtries = -1;
	options->max_sessions = -1;
	options->banner = NULL;
	options->show_patchlevel = -1;
	options->use_dns = -1;
	options->client_alive_interval = -1;
	options->client_alive_count_max = -1;
	options->num_authkeys_files = 0;
	options->num_accept_env = 0;
	options->permit_tun = -1;
	options->num_permitted_opens = -1;
	options->adm_forced_command = NULL;
	options->chroot_directory = NULL;
	options->authorized_keys_command = NULL;
	options->authorized_keys_command_user = NULL;
	options->revoked_keys_file = NULL;
	options->trusted_user_ca_keys = NULL;
	options->authorized_principals_file = NULL;
	options->ip_qos_interactive = -1;
	options->ip_qos_bulk = -1;
	options->version_addendum = NULL;
	options->use_kuserok = -1;
	options->enable_k5users = -1;
	options->expose_auth_methods = -1;
Din_Go(2,2048);}

void
fill_default_server_options(ServerOptions *options)
{
	/* Portable-specific options */
	Din_Go(3,2048);if (options->use_pam == -1)
		{/*3*/Din_Go(4,2048);options->use_pam = 0;/*4*/}

	/* Standard Options */
	Din_Go(5,2048);if (options->protocol == SSH_PROTO_UNKNOWN)
		options->protocol = SSH_PROTO_2;
	Din_Go(7,2048);if (options->num_host_key_files == 0) {
		/* fill default hostkeys for protocols */
		if (options->protocol & SSH_PROTO_1)
			options->host_key_files[options->num_host_key_files++] =
			    _PATH_HOST_KEY_FILE;
		Din_Go(6,2048);if (options->protocol & SSH_PROTO_2) {
			options->host_key_files[options->num_host_key_files++] =
			    _PATH_HOST_RSA_KEY_FILE;
			options->host_key_files[options->num_host_key_files++] =
			    _PATH_HOST_DSA_KEY_FILE;
#ifdef OPENSSL_HAS_ECC
			options->host_key_files[options->num_host_key_files++] =
			    _PATH_HOST_ECDSA_KEY_FILE;
#endif
			options->host_key_files[options->num_host_key_files++] =
			    _PATH_HOST_ED25519_KEY_FILE;
		}
	}
	/* No certificates by default */
	Din_Go(8,2048);if (options->num_ports == 0)
		options->ports[options->num_ports++] = SSH_DEFAULT_PORT;
	Din_Go(10,2048);if (options->listen_addrs == NULL)
		{/*5*/Din_Go(9,2048);add_listen_addr(options, NULL, 0);/*6*/}
	Din_Go(11,2048);if (options->pid_file == NULL)
		options->pid_file = _PATH_SSH_DAEMON_PID_FILE;
	Din_Go(13,2048);if (options->server_key_bits == -1)
		{/*7*/Din_Go(12,2048);options->server_key_bits = 1024;/*8*/}
	Din_Go(15,2048);if (options->login_grace_time == -1)
		{/*9*/Din_Go(14,2048);options->login_grace_time = 120;/*10*/}
	Din_Go(17,2048);if (options->key_regeneration_time == -1)
		{/*11*/Din_Go(16,2048);options->key_regeneration_time = 3600;/*12*/}
	Din_Go(18,2048);if (options->permit_root_login == PERMIT_NOT_SET)
		options->permit_root_login = PERMIT_YES;
	Din_Go(20,2048);if (options->ignore_rhosts == -1)
		{/*13*/Din_Go(19,2048);options->ignore_rhosts = 1;/*14*/}
	Din_Go(22,2048);if (options->ignore_user_known_hosts == -1)
		{/*15*/Din_Go(21,2048);options->ignore_user_known_hosts = 0;/*16*/}
	Din_Go(24,2048);if (options->print_motd == -1)
		{/*17*/Din_Go(23,2048);options->print_motd = 1;/*18*/}
	Din_Go(26,2048);if (options->print_lastlog == -1)
		{/*19*/Din_Go(25,2048);options->print_lastlog = 1;/*20*/}
	Din_Go(28,2048);if (options->x11_forwarding == -1)
		{/*21*/Din_Go(27,2048);options->x11_forwarding = 0;/*22*/}
	Din_Go(30,2048);if (options->x11_display_offset == -1)
		{/*23*/Din_Go(29,2048);options->x11_display_offset = 10;/*24*/}
	Din_Go(31,2048);if (options->x11_max_displays == -1)
		options->x11_max_displays = DEFAULT_MAX_DISPLAYS;
	Din_Go(33,2048);if (options->x11_use_localhost == -1)
		{/*25*/Din_Go(32,2048);options->x11_use_localhost = 1;/*26*/}
	Din_Go(34,2048);if (options->xauth_location == NULL)
		options->xauth_location = _PATH_XAUTH;
	Din_Go(36,2048);if (options->permit_tty == -1)
		{/*27*/Din_Go(35,2048);options->permit_tty = 1;/*28*/}
	Din_Go(38,2048);if (options->strict_modes == -1)
		{/*29*/Din_Go(37,2048);options->strict_modes = 1;/*30*/}
	Din_Go(40,2048);if (options->tcp_keep_alive == -1)
		{/*31*/Din_Go(39,2048);options->tcp_keep_alive = 1;/*32*/}
	Din_Go(42,2048);if (options->log_facility == SYSLOG_FACILITY_NOT_SET)
		{/*33*/Din_Go(41,2048);options->log_facility = SYSLOG_FACILITY_AUTH;/*34*/}
	Din_Go(44,2048);if (options->log_level == SYSLOG_LEVEL_NOT_SET)
		{/*35*/Din_Go(43,2048);options->log_level = SYSLOG_LEVEL_INFO;/*36*/}
	Din_Go(46,2048);if (options->rhosts_rsa_authentication == -1)
		{/*37*/Din_Go(45,2048);options->rhosts_rsa_authentication = 0;/*38*/}
	Din_Go(48,2048);if (options->hostbased_authentication == -1)
		{/*39*/Din_Go(47,2048);options->hostbased_authentication = 0;/*40*/}
	Din_Go(50,2048);if (options->hostbased_uses_name_from_packet_only == -1)
		{/*41*/Din_Go(49,2048);options->hostbased_uses_name_from_packet_only = 0;/*42*/}
	Din_Go(52,2048);if (options->rsa_authentication == -1)
		{/*43*/Din_Go(51,2048);options->rsa_authentication = 1;/*44*/}
	Din_Go(54,2048);if (options->pubkey_authentication == -1)
		{/*45*/Din_Go(53,2048);options->pubkey_authentication = 1;/*46*/}
	Din_Go(56,2048);if (options->kerberos_authentication == -1)
		{/*47*/Din_Go(55,2048);options->kerberos_authentication = 0;/*48*/}
	Din_Go(58,2048);if (options->kerberos_or_local_passwd == -1)
		{/*49*/Din_Go(57,2048);options->kerberos_or_local_passwd = 1;/*50*/}
	Din_Go(60,2048);if (options->kerberos_ticket_cleanup == -1)
		{/*51*/Din_Go(59,2048);options->kerberos_ticket_cleanup = 1;/*52*/}
	Din_Go(62,2048);if (options->kerberos_get_afs_token == -1)
		{/*53*/Din_Go(61,2048);options->kerberos_get_afs_token = 0;/*54*/}
	Din_Go(64,2048);if (options->gss_authentication == -1)
		{/*55*/Din_Go(63,2048);options->gss_authentication = 0;/*56*/}
	Din_Go(66,2048);if (options->gss_keyex == -1)
		{/*57*/Din_Go(65,2048);options->gss_keyex = 0;/*58*/}
	Din_Go(68,2048);if (options->gss_cleanup_creds == -1)
		{/*59*/Din_Go(67,2048);options->gss_cleanup_creds = 1;/*60*/}
	Din_Go(70,2048);if (options->gss_strict_acceptor == -1)
		{/*61*/Din_Go(69,2048);options->gss_strict_acceptor = 1;/*62*/}
	Din_Go(72,2048);if (options->gss_store_rekey == -1)
		{/*63*/Din_Go(71,2048);options->gss_store_rekey = 0;/*64*/}
	Din_Go(74,2048);if (options->gss_kex_algorithms == NULL)
		{/*65*/Din_Go(73,2048);options->gss_kex_algorithms = strdup(GSS_KEX_DEFAULT_KEX);/*66*/}
	Din_Go(76,2048);if (options->password_authentication == -1)
		{/*67*/Din_Go(75,2048);options->password_authentication = 1;/*68*/}
	Din_Go(78,2048);if (options->kbd_interactive_authentication == -1)
		{/*69*/Din_Go(77,2048);options->kbd_interactive_authentication = 0;/*70*/}
	Din_Go(80,2048);if (options->challenge_response_authentication == -1)
		{/*71*/Din_Go(79,2048);options->challenge_response_authentication = 1;/*72*/}
	Din_Go(82,2048);if (options->permit_empty_passwd == -1)
		{/*73*/Din_Go(81,2048);options->permit_empty_passwd = 0;/*74*/}
	Din_Go(84,2048);if (options->permit_user_env == -1)
		{/*75*/Din_Go(83,2048);options->permit_user_env = 0;/*76*/}
	Din_Go(86,2048);if (options->use_login == -1)
		{/*77*/Din_Go(85,2048);options->use_login = 0;/*78*/}
	Din_Go(87,2048);if (options->compression == -1)
		options->compression = COMP_DELAYED;
	Din_Go(89,2048);if (options->rekey_limit == -1)
		{/*79*/Din_Go(88,2048);options->rekey_limit = 0;/*80*/}
	Din_Go(91,2048);if (options->rekey_interval == -1)
		{/*81*/Din_Go(90,2048);options->rekey_interval = 0;/*82*/}
	Din_Go(92,2048);if (options->allow_tcp_forwarding == -1)
		options->allow_tcp_forwarding = FORWARD_ALLOW;
	Din_Go(94,2048);if (options->allow_agent_forwarding == -1)
		{/*83*/Din_Go(93,2048);options->allow_agent_forwarding = 1;/*84*/}
	Din_Go(96,2048);if (options->gateway_ports == -1)
		{/*85*/Din_Go(95,2048);options->gateway_ports = 0;/*86*/}
	Din_Go(98,2048);if (options->max_startups == -1)
		{/*87*/Din_Go(97,2048);options->max_startups = 100;/*88*/}
	Din_Go(100,2048);if (options->max_startups_rate == -1)
		{/*89*/Din_Go(99,2048);options->max_startups_rate = 30;/*90*/}		/* 30% */
	Din_Go(102,2048);if (options->max_startups_begin == -1)
		{/*91*/Din_Go(101,2048);options->max_startups_begin = 10;/*92*/}
	Din_Go(103,2048);if (options->max_authtries == -1)
		options->max_authtries = DEFAULT_AUTH_FAIL_MAX;
	Din_Go(104,2048);if (options->max_sessions == -1)
		options->max_sessions = DEFAULT_SESSIONS_MAX;
	Din_Go(106,2048);if (options->use_dns == -1)
		{/*93*/Din_Go(105,2048);options->use_dns = 1;/*94*/}
	Din_Go(108,2048);if (options->client_alive_interval == -1)
		{/*95*/Din_Go(107,2048);options->client_alive_interval = 0;/*96*/}
	Din_Go(110,2048);if (options->client_alive_count_max == -1)
		{/*97*/Din_Go(109,2048);options->client_alive_count_max = 3;/*98*/}
	Din_Go(112,2048);if (options->num_authkeys_files == 0) {
		Din_Go(111,2048);options->authorized_keys_files[options->num_authkeys_files++] =
		    xstrdup(_PATH_SSH_USER_PERMITTED_KEYS);
		options->authorized_keys_files[options->num_authkeys_files++] =
		    xstrdup(_PATH_SSH_USER_PERMITTED_KEYS2);
	}
	Din_Go(113,2048);if (options->permit_tun == -1)
		options->permit_tun = SSH_TUNMODE_NO;
	Din_Go(114,2048);if (options->ip_qos_interactive == -1)
		options->ip_qos_interactive = IPTOS_LOWDELAY;
	Din_Go(115,2048);if (options->ip_qos_bulk == -1)
		options->ip_qos_bulk = IPTOS_THROUGHPUT;
	Din_Go(117,2048);if (options->version_addendum == NULL)
		{/*99*/Din_Go(116,2048);options->version_addendum = xstrdup("");/*100*/}
	Din_Go(119,2048);if (options->show_patchlevel == -1)
		{/*101*/Din_Go(118,2048);options->show_patchlevel = 0;/*102*/}
	Din_Go(121,2048);if (options->use_kuserok == -1)
		{/*103*/Din_Go(120,2048);options->use_kuserok = 1;/*104*/}
	Din_Go(123,2048);if (options->enable_k5users == -1)
		{/*105*/Din_Go(122,2048);options->enable_k5users = 0;/*106*/}
	Din_Go(124,2048);if (options->expose_auth_methods == -1)
		options->expose_auth_methods = EXPOSE_AUTHMETH_NEVER;

	/* Turn privilege separation on by default */
	Din_Go(125,2048);if (use_privsep == -1)
		use_privsep = PRIVSEP_NOSANDBOX;

	/* Similar handling for AuthenticationMethods=any */
	Din_Go(127,2048);if (options->num_auth_methods == 1 &&
	    strcmp(options->auth_methods[0], "any") == 0) {
		Din_Go(126,2048);free(options->auth_methods[0]);
		options->auth_methods[0] = NULL;
		options->num_auth_methods = 0;
	}

#ifndef HAVE_MMAP
	if (use_privsep && options->compression == 1) {
		error("This platform does not support both privilege "
		    "separation and compression");
		error("Compression disabled");
		options->compression = 0;
	}
#endif

Din_Go(128,2048);}

/* Keyword tokens. */
typedef enum {
	sBadOption,		/* == unknown option */
	/* Portable-specific options */
	sUsePAM,
	/* Standard Options */
	sPort, sHostKeyFile, sServerKeyBits, sLoginGraceTime, sKeyRegenerationTime,
	sPermitRootLogin, sLogFacility, sLogLevel,
	sRhostsRSAAuthentication, sRSAAuthentication,
	sKerberosAuthentication, sKerberosOrLocalPasswd, sKerberosTicketCleanup,
	sKerberosGetAFSToken, sKerberosUseKuserok,
	sKerberosTgtPassing, sChallengeResponseAuthentication,
	sPasswordAuthentication, sKbdInteractiveAuthentication,
	sListenAddress, sAddressFamily,
	sPrintMotd, sPrintLastLog, sIgnoreRhosts,
	sX11Forwarding, sX11DisplayOffset, sX11MaxDisplays, sX11UseLocalhost,
	sPermitTTY, sStrictModes, sEmptyPasswd, sTCPKeepAlive,
	sPermitUserEnvironment, sUseLogin, sAllowTcpForwarding, sCompression,
	sRekeyLimit, sAllowUsers, sDenyUsers, sAllowGroups, sDenyGroups,
	sIgnoreUserKnownHosts, sCiphers, sMacs, sProtocol, sPidFile,
	sGatewayPorts, sPubkeyAuthentication, sXAuthLocation, sSubsystem,
	sMaxStartups, sMaxAuthTries, sMaxSessions,
	sBanner, sShowPatchLevel, sUseDNS, sHostbasedAuthentication,
	sHostbasedUsesNameFromPacketOnly, sClientAliveInterval,
	sClientAliveCountMax, sAuthorizedKeysFile,
	sGssAuthentication, sGssCleanupCreds, sGssEnablek5users, sGssStrictAcceptor,
	sGssKeyEx, sGssStoreRekey, sGssKexAlgorithms, sAcceptEnv, sPermitTunnel,
	sMatch, sPermitOpen, sForceCommand, sChrootDirectory,
	sUsePrivilegeSeparation, sAllowAgentForwarding,
	sHostCertificate,
	sRevokedKeys, sTrustedUserCAKeys, sAuthorizedPrincipalsFile,
	sKexAlgorithms, sIPQoS, sVersionAddendum,
	sAuthorizedKeysCommand, sAuthorizedKeysCommandUser,
	sAuthenticationMethods, sHostKeyAgent,
	sExposeAuthenticationMethods,
	sDeprecated, sUnsupported
} ServerOpCodes;

#define SSHCFG_GLOBAL	0x01	/* allowed in main section of sshd_config */
#define SSHCFG_MATCH	0x02	/* allowed inside a Match section */
#define SSHCFG_ALL	(SSHCFG_GLOBAL|SSHCFG_MATCH)

/* Textual representation of the tokens. */
static struct {
	const char *name;
	ServerOpCodes opcode;
	u_int flags;
} keywords[] = {
	/* Portable-specific options */
#ifdef USE_PAM
	{ "usepam", sUsePAM, SSHCFG_GLOBAL },
#else
	{ "usepam", sUnsupported, SSHCFG_GLOBAL },
#endif
	{ "pamauthenticationviakbdint", sDeprecated, SSHCFG_GLOBAL },
	/* Standard Options */
	{ "port", sPort, SSHCFG_GLOBAL },
	{ "hostkey", sHostKeyFile, SSHCFG_GLOBAL },
	{ "hostdsakey", sHostKeyFile, SSHCFG_GLOBAL },		/* alias */
	{ "hostkeyagent", sHostKeyAgent, SSHCFG_GLOBAL },
	{ "pidfile", sPidFile, SSHCFG_GLOBAL },
	{ "serverkeybits", sServerKeyBits, SSHCFG_GLOBAL },
	{ "logingracetime", sLoginGraceTime, SSHCFG_GLOBAL },
	{ "keyregenerationinterval", sKeyRegenerationTime, SSHCFG_GLOBAL },
	{ "permitrootlogin", sPermitRootLogin, SSHCFG_ALL },
	{ "syslogfacility", sLogFacility, SSHCFG_GLOBAL },
	{ "loglevel", sLogLevel, SSHCFG_GLOBAL },
	{ "rhostsauthentication", sDeprecated, SSHCFG_GLOBAL },
	{ "rhostsrsaauthentication", sRhostsRSAAuthentication, SSHCFG_ALL },
	{ "hostbasedauthentication", sHostbasedAuthentication, SSHCFG_ALL },
	{ "hostbasedusesnamefrompacketonly", sHostbasedUsesNameFromPacketOnly, SSHCFG_ALL },
	{ "rsaauthentication", sRSAAuthentication, SSHCFG_ALL },
	{ "pubkeyauthentication", sPubkeyAuthentication, SSHCFG_ALL },
	{ "dsaauthentication", sPubkeyAuthentication, SSHCFG_GLOBAL }, /* alias */
#ifdef KRB5
	{ "kerberosauthentication", sKerberosAuthentication, SSHCFG_ALL },
	{ "kerberosorlocalpasswd", sKerberosOrLocalPasswd, SSHCFG_GLOBAL },
	{ "kerberosticketcleanup", sKerberosTicketCleanup, SSHCFG_GLOBAL },
#ifdef USE_AFS
	{ "kerberosgetafstoken", sKerberosGetAFSToken, SSHCFG_GLOBAL },
#else
	{ "kerberosgetafstoken", sUnsupported, SSHCFG_GLOBAL },
#endif
	{ "kerberosusekuserok", sKerberosUseKuserok, SSHCFG_ALL },
#else
	{ "kerberosauthentication", sUnsupported, SSHCFG_ALL },
	{ "kerberosorlocalpasswd", sUnsupported, SSHCFG_GLOBAL },
	{ "kerberosticketcleanup", sUnsupported, SSHCFG_GLOBAL },
	{ "kerberosgetafstoken", sUnsupported, SSHCFG_GLOBAL },
	{ "kerberosusekuserok", sUnsupported, SSHCFG_ALL },
#endif
	{ "kerberostgtpassing", sUnsupported, SSHCFG_GLOBAL },
	{ "afstokenpassing", sUnsupported, SSHCFG_GLOBAL },
#ifdef GSSAPI
	{ "gssapiauthentication", sGssAuthentication, SSHCFG_ALL },
	{ "gssapicleanupcredentials", sGssCleanupCreds, SSHCFG_GLOBAL },
	{ "gssapicleanupcreds", sGssCleanupCreds, SSHCFG_GLOBAL },
	{ "gssapistrictacceptorcheck", sGssStrictAcceptor, SSHCFG_GLOBAL },
	{ "gssapikeyexchange", sGssKeyEx, SSHCFG_GLOBAL },
	{ "gssapistorecredentialsonrekey", sGssStoreRekey, SSHCFG_GLOBAL },
	{ "gssapienablek5users", sGssEnablek5users, SSHCFG_ALL },
	{ "gssapikexalgorithms", sGssKexAlgorithms, SSHCFG_GLOBAL },
#else
	{ "gssapiauthentication", sUnsupported, SSHCFG_ALL },
	{ "gssapicleanupcredentials", sUnsupported, SSHCFG_GLOBAL },
	{ "gssapicleanupcreds", sUnsupported, SSHCFG_GLOBAL },
	{ "gssapistrictacceptorcheck", sUnsupported, SSHCFG_GLOBAL },
	{ "gssapikeyexchange", sUnsupported, SSHCFG_GLOBAL },
	{ "gssapistorecredentialsonrekey", sUnsupported, SSHCFG_GLOBAL },
	{ "gssapienablek5users", sUnsupported, SSHCFG_ALL },
	{ "gssapikexalgorithms", sUnsupported, SSHCFG_GLOBAL },
#endif
	{ "gssusesessionccache", sUnsupported, SSHCFG_GLOBAL },
	{ "gssapiusesessioncredcache", sUnsupported, SSHCFG_GLOBAL },
	{ "passwordauthentication", sPasswordAuthentication, SSHCFG_ALL },
	{ "kbdinteractiveauthentication", sKbdInteractiveAuthentication, SSHCFG_ALL },
	{ "challengeresponseauthentication", sChallengeResponseAuthentication, SSHCFG_GLOBAL },
	{ "skeyauthentication", sChallengeResponseAuthentication, SSHCFG_GLOBAL }, /* alias */
	{ "checkmail", sDeprecated, SSHCFG_GLOBAL },
	{ "listenaddress", sListenAddress, SSHCFG_GLOBAL },
	{ "addressfamily", sAddressFamily, SSHCFG_GLOBAL },
	{ "printmotd", sPrintMotd, SSHCFG_GLOBAL },
	{ "printlastlog", sPrintLastLog, SSHCFG_GLOBAL },
	{ "ignorerhosts", sIgnoreRhosts, SSHCFG_GLOBAL },
	{ "ignoreuserknownhosts", sIgnoreUserKnownHosts, SSHCFG_GLOBAL },
	{ "x11forwarding", sX11Forwarding, SSHCFG_ALL },
	{ "x11displayoffset", sX11DisplayOffset, SSHCFG_ALL },
	{ "x11maxdisplays", sX11MaxDisplays, SSHCFG_ALL },
	{ "x11uselocalhost", sX11UseLocalhost, SSHCFG_ALL },
	{ "xauthlocation", sXAuthLocation, SSHCFG_GLOBAL },
	{ "strictmodes", sStrictModes, SSHCFG_GLOBAL },
	{ "permitemptypasswords", sEmptyPasswd, SSHCFG_ALL },
	{ "permituserenvironment", sPermitUserEnvironment, SSHCFG_GLOBAL },
	{ "uselogin", sUseLogin, SSHCFG_GLOBAL },
	{ "compression", sCompression, SSHCFG_GLOBAL },
	{ "rekeylimit", sRekeyLimit, SSHCFG_ALL },
	{ "tcpkeepalive", sTCPKeepAlive, SSHCFG_GLOBAL },
	{ "keepalive", sTCPKeepAlive, SSHCFG_GLOBAL },	/* obsolete alias */
	{ "allowtcpforwarding", sAllowTcpForwarding, SSHCFG_ALL },
	{ "allowagentforwarding", sAllowAgentForwarding, SSHCFG_ALL },
	{ "allowusers", sAllowUsers, SSHCFG_ALL },
	{ "denyusers", sDenyUsers, SSHCFG_ALL },
	{ "allowgroups", sAllowGroups, SSHCFG_ALL },
	{ "denygroups", sDenyGroups, SSHCFG_ALL },
	{ "ciphers", sCiphers, SSHCFG_GLOBAL },
	{ "macs", sMacs, SSHCFG_GLOBAL },
	{ "protocol", sProtocol, SSHCFG_GLOBAL },
	{ "gatewayports", sGatewayPorts, SSHCFG_ALL },
	{ "subsystem", sSubsystem, SSHCFG_GLOBAL },
	{ "maxstartups", sMaxStartups, SSHCFG_GLOBAL },
	{ "maxauthtries", sMaxAuthTries, SSHCFG_ALL },
	{ "maxsessions", sMaxSessions, SSHCFG_ALL },
	{ "banner", sBanner, SSHCFG_ALL },
	{ "showpatchlevel", sShowPatchLevel, SSHCFG_GLOBAL },
	{ "usedns", sUseDNS, SSHCFG_GLOBAL },
	{ "verifyreversemapping", sDeprecated, SSHCFG_GLOBAL },
	{ "reversemappingcheck", sDeprecated, SSHCFG_GLOBAL },
	{ "clientaliveinterval", sClientAliveInterval, SSHCFG_GLOBAL },
	{ "clientalivecountmax", sClientAliveCountMax, SSHCFG_GLOBAL },
	{ "authorizedkeysfile", sAuthorizedKeysFile, SSHCFG_ALL },
	{ "authorizedkeysfile2", sDeprecated, SSHCFG_ALL },
	{ "useprivilegeseparation", sUsePrivilegeSeparation, SSHCFG_GLOBAL},
	{ "acceptenv", sAcceptEnv, SSHCFG_ALL },
	{ "permittunnel", sPermitTunnel, SSHCFG_ALL },
	{ "permittty", sPermitTTY, SSHCFG_ALL },
	{ "match", sMatch, SSHCFG_ALL },
	{ "permitopen", sPermitOpen, SSHCFG_ALL },
	{ "forcecommand", sForceCommand, SSHCFG_ALL },
	{ "chrootdirectory", sChrootDirectory, SSHCFG_ALL },
	{ "hostcertificate", sHostCertificate, SSHCFG_GLOBAL },
	{ "revokedkeys", sRevokedKeys, SSHCFG_ALL },
	{ "trustedusercakeys", sTrustedUserCAKeys, SSHCFG_ALL },
	{ "authorizedprincipalsfile", sAuthorizedPrincipalsFile, SSHCFG_ALL },
	{ "kexalgorithms", sKexAlgorithms, SSHCFG_GLOBAL },
	{ "ipqos", sIPQoS, SSHCFG_ALL },
	{ "authorizedkeyscommand", sAuthorizedKeysCommand, SSHCFG_ALL },
	{ "authorizedkeyscommanduser", sAuthorizedKeysCommandUser, SSHCFG_ALL },
	{ "versionaddendum", sVersionAddendum, SSHCFG_GLOBAL },
	{ "authenticationmethods", sAuthenticationMethods, SSHCFG_ALL },
	{ "exposeauthenticationmethods", sExposeAuthenticationMethods, SSHCFG_ALL },
	{ NULL, sBadOption, 0 }
};

static struct {
	int val;
	char *text;
} tunmode_desc[] = {
	{ SSH_TUNMODE_NO, "no" },
	{ SSH_TUNMODE_POINTOPOINT, "point-to-point" },
	{ SSH_TUNMODE_ETHERNET, "ethernet" },
	{ SSH_TUNMODE_YES, "yes" },
	{ -1, NULL }
};

/*
 * Returns the number of the token pointed to by cp or sBadOption.
 */

static ServerOpCodes
parse_token(const char *cp, const char *filename,
	    int linenum, u_int *flags)
{
	Din_Go(129,2048);u_int i;

	Din_Go(133,2048);for (i = 0; keywords[i].name; i++)
		{/*467*/Din_Go(130,2048);if (strcasecmp(cp, keywords[i].name) == 0) {
			Din_Go(131,2048);*flags = keywords[i].flags;
			{ServerOpCodes  ReplaceReturn = keywords[i].opcode; Din_Go(132,2048); return ReplaceReturn;}
		;/*468*/}}

	Din_Go(134,2048);error("%s: line %d: Bad configuration option: %s",
	    filename, linenum, cp);
	{ServerOpCodes  ReplaceReturn = sBadOption; Din_Go(135,2048); return ReplaceReturn;}
}

char *
derelativise_path(const char *path)
{
	Din_Go(136,2048);char *expanded, *ret, cwd[MAXPATHLEN];

	expanded = tilde_expand_filename(path, getuid());
	Din_Go(138,2048);if (*expanded == '/')
		{/*453*/{char * ReplaceReturn = expanded; Din_Go(137,2048); return ReplaceReturn;}/*454*/}
	Din_Go(140,2048);if (getcwd(cwd, sizeof(cwd)) == NULL)
		{/*455*/Din_Go(139,2048);fatal("%s: getcwd: %s", __func__, strerror(errno));/*456*/}
	Din_Go(141,2048);xasprintf(&ret, "%s/%s", cwd, expanded);
	free(expanded);
	{char * ReplaceReturn = ret; Din_Go(142,2048); return ReplaceReturn;}
}

static void
add_listen_addr(ServerOptions *options, char *addr, int port)
{
	Din_Go(143,2048);u_int i;

	Din_Go(144,2048);if (options->num_ports == 0)
		options->ports[options->num_ports++] = SSH_DEFAULT_PORT;
	Din_Go(145,2048);if (options->address_family == -1)
		options->address_family = AF_UNSPEC;
	Din_Go(149,2048);if (port == 0)
		{/*457*/Din_Go(146,2048);for (i = 0; i < options->num_ports; i++)
			{/*459*/Din_Go(147,2048);add_one_listen_addr(options, addr, options->ports[i]);/*460*/}/*458*/}
	else
		{/*461*/Din_Go(148,2048);add_one_listen_addr(options, addr, port);/*462*/}
Din_Go(150,2048);}

static void
add_one_listen_addr(ServerOptions *options, char *addr, int port)
{
	Din_Go(151,2048);struct addrinfo hints, *ai, *aitop;
	char strport[NI_MAXSERV];
	int gaierr;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = options->address_family;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = (addr == NULL) ? AI_PASSIVE : 0;
	snprintf(strport, sizeof strport, "%d", port);
	Din_Go(153,2048);if ((gaierr = getaddrinfo(addr, strport, &hints, &aitop)) != 0)
		{/*463*/Din_Go(152,2048);fatal("bad addr or host: %s (%s)",
		    addr ? addr : "<NULL>",
		    ssh_gai_strerror(gaierr));/*464*/}
	Din_Go(154,2048);for (ai = aitop; ai->ai_next; ai = ai->ai_next)
		;
	Din_Go(155,2048);ai->ai_next = options->listen_addrs;
	options->listen_addrs = aitop;
Din_Go(156,2048);}

struct connection_info *
get_connection_info(int populate, int use_dns)
{
	Din_Go(157,2048);static struct connection_info ci;

	Din_Go(159,2048);if (!populate)
		{/*1*/{__IASTRUCT__ connection_info * ReplaceReturn = &ci; Din_Go(158,2048); return ReplaceReturn;}/*2*/}
	Din_Go(160,2048);ci.host = get_canonical_hostname(use_dns);
	ci.address = get_remote_ipaddr();
	ci.laddress = get_local_ipaddr(packet_get_connection_in());
	ci.lport = get_local_port();
	{__IASTRUCT__ connection_info * ReplaceReturn = &ci; Din_Go(161,2048); return ReplaceReturn;}
}

/*
 * The strategy for the Match blocks is that the config file is parsed twice.
 *
 * The first time is at startup.  activep is initialized to 1 and the
 * directives in the global context are processed and acted on.  Hitting a
 * Match directive unsets activep and the directives inside the block are
 * checked for syntax only.
 *
 * The second time is after a connection has been established but before
 * authentication.  activep is initialized to 2 and global config directives
 * are ignored since they have already been processed.  If the criteria in a
 * Match block is met, activep is set and the subsequent directives
 * processed and actioned until EOF or another Match block unsets it.  Any
 * options set are copied into the main server config.
 *
 * Potential additions/improvements:
 *  - Add Match support for pre-kex directives, eg Protocol, Ciphers.
 *
 *  - Add a Tag directive (idea from David Leonard) ala pf, eg:
 *	Match Address 192.168.0.*
 *		Tag trusted
 *	Match Group wheel
 *		Tag trusted
 *	Match Tag trusted
 *		AllowTcpForwarding yes
 *		GatewayPorts clientspecified
 *		[...]
 *
 *  - Add a PermittedChannelRequests directive
 *	Match Group shell
 *		PermittedChannelRequests session,forwarded-tcpip
 */

static int
match_cfg_line_group(const char *grps, int line, const char *user)
{
	Din_Go(162,2048);int result = 0;
	struct passwd *pw;

	Din_Go(164,2048);if (user == NULL)
		{/*469*/Din_Go(163,2048);goto out;/*470*/}

	Din_Go(171,2048);if ((pw = getpwnam(user)) == NULL) {
		Din_Go(165,2048);debug("Can't match group at line %d because user %.100s does "
		    "not exist", line, user);
	} else {/*471*/Din_Go(166,2048);if (ga_init(pw->pw_name, pw->pw_gid) == 0) {
		Din_Go(167,2048);debug("Can't Match group because user %.100s not in any group "
		    "at line %d", user, line);
	} else {/*473*/Din_Go(168,2048);if (ga_match_pattern_list(grps) != 1) {
		Din_Go(169,2048);debug("user %.100s does not match group list %.100s at line %d",
		    user, grps, line);
	} else {
		Din_Go(170,2048);debug("user %.100s matched group list %.100s at line %d", user,
		    grps, line);
		result = 1;
	;/*474*/}/*472*/}}
out:
	Din_Go(172,2048);ga_free();
	{int  ReplaceReturn = result; Din_Go(173,2048); return ReplaceReturn;}
}

/*
 * All of the attributes on a single Match line are ANDed together, so we need
 * to check every attribute and set the result to zero if any attribute does
 * not match.
 */
static int
match_cfg_line(char **condition, int line, struct connection_info *ci)
{
	Din_Go(174,2048);int result = 1, attributes = 0, port;
	char *arg, *attrib, *cp = *condition;
	size_t len;

	Din_Go(177,2048);if (ci == NULL)
		{/*475*/Din_Go(175,2048);debug3("checking syntax for 'Match %s'", cp);/*476*/}
	else
		{/*477*/Din_Go(176,2048);debug3("checking match for '%s' user %s host %s addr %s "
		    "laddr %s lport %d", cp, ci->user ? ci->user : "(null)",
		    ci->host ? ci->host : "(null)",
		    ci->address ? ci->address : "(null)",
		    ci->laddress ? ci->laddress : "(null)", ci->lport);/*478*/}

	Din_Go(242,2048);while ((attrib = strdelim(&cp)) && *attrib != '\0') {
		Din_Go(178,2048);attributes++;
		Din_Go(184,2048);if (strcasecmp(attrib, "all") == 0) {
			Din_Go(179,2048);if (attributes != 1 ||
			    ((arg = strdelim(&cp)) != NULL && *arg != '\0')) {
				Din_Go(180,2048);error("'all' cannot be combined with other "
				    "Match attributes");
				{int  ReplaceReturn = -1; Din_Go(181,2048); return ReplaceReturn;}
			}
			Din_Go(182,2048);*condition = cp;
			{int  ReplaceReturn = 1; Din_Go(183,2048); return ReplaceReturn;}
		}
		Din_Go(187,2048);if ((arg = strdelim(&cp)) == NULL || *arg == '\0') {
			Din_Go(185,2048);error("Missing Match criteria for %s", attrib);
			{int  ReplaceReturn = -1; Din_Go(186,2048); return ReplaceReturn;}
		}
		Din_Go(188,2048);len = strlen(arg);
		Din_Go(241,2048);if (strcasecmp(attrib, "user") == 0) {
			Din_Go(189,2048);if (ci == NULL || ci->user == NULL) {
				Din_Go(190,2048);result = 0;
				Din_Go(191,2048);continue;
			}
			Din_Go(194,2048);if (match_pattern_list(ci->user, arg, len, 0) != 1)
				{/*479*/Din_Go(192,2048);result = 0;/*480*/}
			else
				{/*481*/Din_Go(193,2048);debug("user %.100s matched 'User %.100s' at "
				    "line %d", ci->user, arg, line);/*482*/}
		} else {/*483*/Din_Go(195,2048);if (strcasecmp(attrib, "group") == 0) {
			Din_Go(196,2048);if (ci == NULL || ci->user == NULL) {
				Din_Go(197,2048);result = 0;
				Din_Go(198,2048);continue;
			}
			Din_Go(201,2048);switch (match_cfg_line_group(arg, line, ci->user)) {
			case -1:
				{int  ReplaceReturn = -1; Din_Go(199,2048); return ReplaceReturn;}
			case 0:
				Din_Go(200,2048);result = 0;
			}
		} else {/*485*/Din_Go(202,2048);if (strcasecmp(attrib, "host") == 0) {
			Din_Go(203,2048);if (ci == NULL || ci->host == NULL) {
				Din_Go(204,2048);result = 0;
				Din_Go(205,2048);continue;
			}
			Din_Go(208,2048);if (match_hostname(ci->host, arg, len) != 1)
				{/*487*/Din_Go(206,2048);result = 0;/*488*/}
			else
				{/*489*/Din_Go(207,2048);debug("connection from %.100s matched 'Host "
				    "%.100s' at line %d", ci->host, arg, line);/*490*/}
		} else {/*491*/Din_Go(209,2048);if (strcasecmp(attrib, "address") == 0) {
			Din_Go(210,2048);if (ci == NULL || ci->address == NULL) {
				Din_Go(211,2048);result = 0;
				Din_Go(212,2048);continue;
			}
			Din_Go(218,2048);switch (addr_match_list(ci->address, arg)) {
			case 1:
				Din_Go(213,2048);debug("connection from %.100s matched 'Address "
				    "%.100s' at line %d", ci->address, arg, line);
				Din_Go(214,2048);break;
			case 0:
			case -1:
				Din_Go(215,2048);result = 0;
				Din_Go(216,2048);break;
			case -2:
				{int  ReplaceReturn = -1; Din_Go(217,2048); return ReplaceReturn;}
			}
		} else {/*493*/Din_Go(219,2048);if (strcasecmp(attrib, "localaddress") == 0){
			Din_Go(220,2048);if (ci == NULL || ci->laddress == NULL) {
				Din_Go(221,2048);result = 0;
				Din_Go(222,2048);continue;
			}
			Din_Go(228,2048);switch (addr_match_list(ci->laddress, arg)) {
			case 1:
				Din_Go(223,2048);debug("connection from %.100s matched "
				    "'LocalAddress %.100s' at line %d",
				    ci->laddress, arg, line);
				Din_Go(224,2048);break;
			case 0:
			case -1:
				Din_Go(225,2048);result = 0;
				Din_Go(226,2048);break;
			case -2:
				{int  ReplaceReturn = -1; Din_Go(227,2048); return ReplaceReturn;}
			}
		} else {/*495*/Din_Go(229,2048);if (strcasecmp(attrib, "localport") == 0) {
			Din_Go(230,2048);if ((port = a2port(arg)) == -1) {
				Din_Go(231,2048);error("Invalid LocalPort '%s' on Match line",
				    arg);
				{int  ReplaceReturn = -1; Din_Go(232,2048); return ReplaceReturn;}
			}
			Din_Go(235,2048);if (ci == NULL || ci->lport == 0) {
				Din_Go(233,2048);result = 0;
				Din_Go(234,2048);continue;
			}
			/* TODO support port lists */
			Din_Go(238,2048);if (port == ci->lport)
				{/*497*/Din_Go(236,2048);debug("connection from %.100s matched "
				    "'LocalPort %d' at line %d",
				    ci->laddress, port, line);/*498*/}
			else
				{/*499*/Din_Go(237,2048);result = 0;/*500*/}
		} else {
			Din_Go(239,2048);error("Unsupported Match attribute %s", attrib);
			{int  ReplaceReturn = -1; Din_Go(240,2048); return ReplaceReturn;}
		;/*496*/}/*494*/}/*492*/}/*486*/}/*484*/}}
	}
	Din_Go(245,2048);if (attributes == 0) {
		Din_Go(243,2048);error("One or more attributes required for Match");
		{int  ReplaceReturn = -1; Din_Go(244,2048); return ReplaceReturn;}
	}
	Din_Go(247,2048);if (ci != NULL)
		{/*501*/Din_Go(246,2048);debug3("match %sfound", result ? "" : "not ");/*502*/}
	Din_Go(248,2048);*condition = cp;
	{int  ReplaceReturn = result; Din_Go(249,2048); return ReplaceReturn;}
}

#define WHITESPACE " \t\r\n"

/* Multistate option parsing */
struct multistate {
	char *key;
	int value;
};
static const struct multistate multistate_addressfamily[] = {
	{ "inet",			AF_INET },
	{ "inet6",			AF_INET6 },
	{ "any",			AF_UNSPEC },
	{ NULL, -1 }
};
static const struct multistate multistate_permitrootlogin[] = {
	{ "without-password",		PERMIT_NO_PASSWD },
	{ "forced-commands-only",	PERMIT_FORCED_ONLY },
	{ "yes",			PERMIT_YES },
	{ "no",				PERMIT_NO },
	{ NULL, -1 }
};
static const struct multistate multistate_compression[] = {
	{ "delayed",			COMP_DELAYED },
	{ "yes",			COMP_ZLIB },
	{ "no",				COMP_NONE },
	{ NULL, -1 }
};
static const struct multistate multistate_gatewayports[] = {
	{ "clientspecified",		2 },
	{ "yes",			1 },
	{ "no",				0 },
	{ NULL, -1 }
};
static const struct multistate multistate_privsep[] = {
	{ "yes",			PRIVSEP_NOSANDBOX },
	{ "sandbox",			PRIVSEP_ON },
	{ "nosandbox",			PRIVSEP_NOSANDBOX },
	{ "no",				PRIVSEP_OFF },
	{ NULL, -1 }
};
static const struct multistate multistate_tcpfwd[] = {
	{ "yes",			FORWARD_ALLOW },
	{ "all",			FORWARD_ALLOW },
	{ "no",				FORWARD_DENY },
	{ "remote",			FORWARD_REMOTE },
	{ "local",			FORWARD_LOCAL },
	{ NULL, -1 }
};
static const struct multistate multistate_exposeauthmeth[] = {
	{ "never",			EXPOSE_AUTHMETH_NEVER },
	{ "pam-only",			EXPOSE_AUTHMETH_PAMONLY },
	{ "pam-and-env",		EXPOSE_AUTHMETH_PAMENV },
	{ NULL, -1}
};

int
process_server_config_line(ServerOptions *options, char *line,
    const char *filename, int linenum, int *activep,
    struct connection_info *connectinfo)
{
	Din_Go(250,2048);char *cp, **charptr, *arg, *p;
	int cmdline = 0, *intptr, value, value2, n, port;
	SyslogFacility *log_facility_ptr;
	LogLevel *log_level_ptr;
	ServerOpCodes opcode;
	u_int i, flags = 0;
	size_t len;
	long long val64;
	const struct multistate *multistate_ptr;

	cp = line;
	Din_Go(252,2048);if ((arg = strdelim(&cp)) == NULL)
		{/*107*/{int  ReplaceReturn = 0; Din_Go(251,2048); return ReplaceReturn;}/*108*/}
	/* Ignore leading whitespace */
	Din_Go(254,2048);if (*arg == '\0')
		{/*109*/Din_Go(253,2048);arg = strdelim(&cp);/*110*/}
	Din_Go(256,2048);if (!arg || !*arg || *arg == '#')
		{/*111*/{int  ReplaceReturn = 0; Din_Go(255,2048); return ReplaceReturn;}/*112*/}
	Din_Go(257,2048);intptr = NULL;
	charptr = NULL;
	opcode = parse_token(arg, filename, linenum, &flags);

	Din_Go(259,2048);if (activep == NULL) { /* We are processing a command line directive */
		Din_Go(258,2048);cmdline = 1;
		activep = &cmdline;
	}
	Din_Go(261,2048);if (*activep && opcode != sMatch)
		{/*113*/Din_Go(260,2048);debug3("%s:%d setting %s %s", filename, linenum, arg, cp);/*114*/}
	Din_Go(267,2048);if (*activep == 0 && !(flags & SSHCFG_MATCH)) {
		Din_Go(262,2048);if (connectinfo == NULL) {
			Din_Go(263,2048);fatal("%s line %d: Directive '%s' is not allowed "
			    "within a Match block", filename, linenum, arg);
		} else { /* this is a directive we have already processed */
			Din_Go(264,2048);while (arg)
				{/*115*/Din_Go(265,2048);arg = strdelim(&cp);/*116*/}
			{int  ReplaceReturn = 0; Din_Go(266,2048); return ReplaceReturn;}
		}
	}

	Din_Go(735,2048);switch (opcode) {
	/* Portable-specific options */
	case sUsePAM:
		Din_Go(268,2048);intptr = &options->use_pam;
		Din_Go(269,2048);goto parse_flag;

	/* Standard Options */
	case sBadOption:
		{int  ReplaceReturn = -1; Din_Go(270,2048); return ReplaceReturn;}
	case sPort:
		/* ignore ports from configfile if cmdline specifies ports */
		Din_Go(271,2048);if (options->ports_from_cmdline)
			{/*117*/{int  ReplaceReturn = 0; Din_Go(272,2048); return ReplaceReturn;}/*118*/}
		Din_Go(274,2048);if (options->listen_addrs != NULL)
			{/*119*/Din_Go(273,2048);fatal("%s line %d: ports must be specified before "
			    "ListenAddress.", filename, linenum);/*120*/}
		Din_Go(276,2048);if (options->num_ports >= MAX_PORTS)
			{/*121*/Din_Go(275,2048);fatal("%s line %d: too many ports.",
			    filename, linenum);/*122*/}
		Din_Go(277,2048);arg = strdelim(&cp);
		Din_Go(279,2048);if (!arg || *arg == '\0')
			{/*123*/Din_Go(278,2048);fatal("%s line %d: missing port number.",
			    filename, linenum);/*124*/}
		Din_Go(280,2048);options->ports[options->num_ports++] = a2port(arg);
		Din_Go(282,2048);if (options->ports[options->num_ports-1] <= 0)
			{/*125*/Din_Go(281,2048);fatal("%s line %d: Badly formatted port number.",
			    filename, linenum);/*126*/}
		Din_Go(283,2048);break;

	case sServerKeyBits:
		Din_Go(284,2048);intptr = &options->server_key_bits;
 parse_int:
		arg = strdelim(&cp);
		Din_Go(286,2048);if (!arg || *arg == '\0')
			{/*127*/Din_Go(285,2048);fatal("%s line %d: missing integer value.",
			    filename, linenum);/*128*/}
		Din_Go(287,2048);value = atoi(arg);
		Din_Go(289,2048);if (*activep && *intptr == -1)
			{/*129*/Din_Go(288,2048);*intptr = value;/*130*/}
		Din_Go(290,2048);break;

	case sLoginGraceTime:
		Din_Go(291,2048);intptr = &options->login_grace_time;
 parse_time:
		arg = strdelim(&cp);
		Din_Go(293,2048);if (!arg || *arg == '\0')
			{/*131*/Din_Go(292,2048);fatal("%s line %d: missing time value.",
			    filename, linenum);/*132*/}
		Din_Go(295,2048);if ((value = convtime(arg)) == -1)
			{/*133*/Din_Go(294,2048);fatal("%s line %d: invalid time value.",
			    filename, linenum);/*134*/}
		Din_Go(297,2048);if (*intptr == -1)
			{/*135*/Din_Go(296,2048);*intptr = value;/*136*/}
		Din_Go(298,2048);break;

	case sKeyRegenerationTime:
		Din_Go(299,2048);intptr = &options->key_regeneration_time;
		Din_Go(300,2048);goto parse_time;

	case sListenAddress:
		Din_Go(301,2048);arg = strdelim(&cp);
		Din_Go(303,2048);if (arg == NULL || *arg == '\0')
			{/*137*/Din_Go(302,2048);fatal("%s line %d: missing address",
			    filename, linenum);/*138*/}
		/* check for bare IPv6 address: no "[]" and 2 or more ":" */
		Din_Go(306,2048);if (strchr(arg, '[') == NULL && (p = strchr(arg, ':')) != NULL
		    && strchr(p+1, ':') != NULL) {
			Din_Go(304,2048);add_listen_addr(options, arg, 0);
			Din_Go(305,2048);break;
		}
		Din_Go(307,2048);p = hpdelim(&arg);
		Din_Go(309,2048);if (p == NULL)
			{/*139*/Din_Go(308,2048);fatal("%s line %d: bad address:port usage",
			    filename, linenum);/*140*/}
		Din_Go(310,2048);p = cleanhostname(p);
		Din_Go(314,2048);if (arg == NULL)
			{/*141*/Din_Go(311,2048);port = 0;/*142*/}
		else {/*143*/Din_Go(312,2048);if ((port = a2port(arg)) <= 0)
			{/*145*/Din_Go(313,2048);fatal("%s line %d: bad port number", filename, linenum);/*146*/}/*144*/}

		Din_Go(315,2048);add_listen_addr(options, p, port);

		Din_Go(316,2048);break;

	case sAddressFamily:
		Din_Go(317,2048);intptr = &options->address_family;
		multistate_ptr = multistate_addressfamily;
		Din_Go(319,2048);if (options->listen_addrs != NULL)
			{/*147*/Din_Go(318,2048);fatal("%s line %d: address family must be specified "
			    "before ListenAddress.", filename, linenum);/*148*/}
 parse_multistate:
		Din_Go(320,2048);arg = strdelim(&cp);
		Din_Go(322,2048);if (!arg || *arg == '\0')
			{/*149*/Din_Go(321,2048);fatal("%s line %d: missing argument.",
			    filename, linenum);/*150*/}
		Din_Go(323,2048);value = -1;
		Din_Go(327,2048);for (i = 0; multistate_ptr[i].key != NULL; i++) {
			Din_Go(324,2048);if (strcasecmp(arg, multistate_ptr[i].key) == 0) {
				Din_Go(325,2048);value = multistate_ptr[i].value;
				Din_Go(326,2048);break;
			}
		}
		Din_Go(329,2048);if (value == -1)
			{/*151*/Din_Go(328,2048);fatal("%s line %d: unsupported option \"%s\".",
			    filename, linenum, arg);/*152*/}
		Din_Go(331,2048);if (*activep && *intptr == -1)
			{/*153*/Din_Go(330,2048);*intptr = value;/*154*/}
		Din_Go(332,2048);break;

	case sHostKeyFile:
		Din_Go(333,2048);intptr = &options->num_host_key_files;
		Din_Go(335,2048);if (*intptr >= MAX_HOSTKEYS)
			{/*155*/Din_Go(334,2048);fatal("%s line %d: too many host keys specified (max %d).",
			    filename, linenum, MAX_HOSTKEYS);/*156*/}
		Din_Go(336,2048);charptr = &options->host_key_files[*intptr];
 parse_filename:
		arg = strdelim(&cp);
		Din_Go(338,2048);if (!arg || *arg == '\0')
			{/*157*/Din_Go(337,2048);fatal("%s line %d: missing file name.",
			    filename, linenum);/*158*/}
		Din_Go(342,2048);if (*activep && *charptr == NULL) {
			Din_Go(339,2048);*charptr = derelativise_path(arg);
			/* increase optional counter */
			Din_Go(341,2048);if (intptr != NULL)
				{/*159*/Din_Go(340,2048);*intptr = *intptr + 1;/*160*/}
		}
		Din_Go(343,2048);break;

	case sHostKeyAgent:
		Din_Go(344,2048);charptr = &options->host_key_agent;
		arg = strdelim(&cp);
		Din_Go(346,2048);if (!arg || *arg == '\0')
			{/*161*/Din_Go(345,2048);fatal("%s line %d: missing socket name.",
			    filename, linenum);/*162*/}
		Din_Go(348,2048);if (*activep && *charptr == NULL)
			{/*163*/Din_Go(347,2048);*charptr = !strcmp(arg, SSH_AUTHSOCKET_ENV_NAME) ?
			    xstrdup(arg) : derelativise_path(arg);/*164*/}
		Din_Go(349,2048);break;

	case sHostCertificate:
		Din_Go(350,2048);intptr = &options->num_host_cert_files;
		Din_Go(352,2048);if (*intptr >= MAX_HOSTKEYS)
			{/*165*/Din_Go(351,2048);fatal("%s line %d: too many host certificates "
			    "specified (max %d).", filename, linenum,
			    MAX_HOSTCERTS);/*166*/}
		Din_Go(353,2048);charptr = &options->host_cert_files[*intptr];
		Din_Go(354,2048);goto parse_filename;
		Din_Go(355,2048);break;

	case sPidFile:
		Din_Go(356,2048);charptr = &options->pid_file;
		Din_Go(357,2048);goto parse_filename;

	case sPermitRootLogin:
		Din_Go(358,2048);intptr = &options->permit_root_login;
		multistate_ptr = multistate_permitrootlogin;
		Din_Go(359,2048);goto parse_multistate;

	case sIgnoreRhosts:
		Din_Go(360,2048);intptr = &options->ignore_rhosts;
 parse_flag:
		arg = strdelim(&cp);
		Din_Go(362,2048);if (!arg || *arg == '\0')
			{/*167*/Din_Go(361,2048);fatal("%s line %d: missing yes/no argument.",
			    filename, linenum);/*168*/}
		Din_Go(363,2048);value = 0;	/* silence compiler */
		Din_Go(368,2048);if (strcmp(arg, "yes") == 0)
			{/*169*/Din_Go(364,2048);value = 1;/*170*/}
		else {/*171*/Din_Go(365,2048);if (strcmp(arg, "no") == 0)
			{/*173*/Din_Go(366,2048);value = 0;/*174*/}
		else
			{/*175*/Din_Go(367,2048);fatal("%s line %d: Bad yes/no argument: %s",
				filename, linenum, arg);/*176*/}/*172*/}
		Din_Go(370,2048);if (*activep && *intptr == -1)
			{/*177*/Din_Go(369,2048);*intptr = value;/*178*/}
		Din_Go(371,2048);break;

	case sIgnoreUserKnownHosts:
		Din_Go(372,2048);intptr = &options->ignore_user_known_hosts;
		Din_Go(373,2048);goto parse_flag;

	case sRhostsRSAAuthentication:
		Din_Go(374,2048);intptr = &options->rhosts_rsa_authentication;
		Din_Go(375,2048);goto parse_flag;

	case sHostbasedAuthentication:
		Din_Go(376,2048);intptr = &options->hostbased_authentication;
		Din_Go(377,2048);goto parse_flag;

	case sHostbasedUsesNameFromPacketOnly:
		Din_Go(378,2048);intptr = &options->hostbased_uses_name_from_packet_only;
		Din_Go(379,2048);goto parse_flag;

	case sRSAAuthentication:
		Din_Go(380,2048);intptr = &options->rsa_authentication;
		Din_Go(381,2048);goto parse_flag;

	case sPubkeyAuthentication:
		Din_Go(382,2048);intptr = &options->pubkey_authentication;
		Din_Go(383,2048);goto parse_flag;

	case sKerberosAuthentication:
		Din_Go(384,2048);intptr = &options->kerberos_authentication;
		Din_Go(385,2048);goto parse_flag;

	case sKerberosOrLocalPasswd:
		Din_Go(386,2048);intptr = &options->kerberos_or_local_passwd;
		Din_Go(387,2048);goto parse_flag;

	case sKerberosTicketCleanup:
		Din_Go(388,2048);intptr = &options->kerberos_ticket_cleanup;
		Din_Go(389,2048);goto parse_flag;

	case sKerberosGetAFSToken:
		Din_Go(390,2048);intptr = &options->kerberos_get_afs_token;
		Din_Go(391,2048);goto parse_flag;

	case sGssAuthentication:
		Din_Go(392,2048);intptr = &options->gss_authentication;
		Din_Go(393,2048);goto parse_flag;

	case sGssKeyEx:
		Din_Go(394,2048);intptr = &options->gss_keyex;
		Din_Go(395,2048);goto parse_flag;

	case sGssCleanupCreds:
		Din_Go(396,2048);intptr = &options->gss_cleanup_creds;
		Din_Go(397,2048);goto parse_flag;

	case sGssStrictAcceptor:
		Din_Go(398,2048);intptr = &options->gss_strict_acceptor;
		Din_Go(399,2048);goto parse_flag;

	case sGssStoreRekey:
		Din_Go(400,2048);intptr = &options->gss_store_rekey;
		Din_Go(401,2048);goto parse_flag;

	case sGssKexAlgorithms:
		Din_Go(402,2048);arg = strdelim(&cp);
		Din_Go(404,2048);if (!arg || *arg == '\0')
			{/*179*/Din_Go(403,2048);fatal("%.200s line %d: Missing argument.",
			    filename, linenum);/*180*/}
		Din_Go(406,2048);if (!gss_kex_names_valid(arg))
			{/*181*/Din_Go(405,2048);fatal("%.200s line %d: Bad GSSAPI KexAlgorithms '%s'.",
			    filename, linenum, arg ? arg : "<NONE>");/*182*/}
		Din_Go(408,2048);if (*activep && options->gss_kex_algorithms == NULL)
			{/*183*/Din_Go(407,2048);options->gss_kex_algorithms = xstrdup(arg);/*184*/}
		Din_Go(409,2048);break;

	case sPasswordAuthentication:
		Din_Go(410,2048);intptr = &options->password_authentication;
		Din_Go(411,2048);goto parse_flag;

	case sKbdInteractiveAuthentication:
		Din_Go(412,2048);intptr = &options->kbd_interactive_authentication;
		Din_Go(413,2048);goto parse_flag;

	case sChallengeResponseAuthentication:
		Din_Go(414,2048);intptr = &options->challenge_response_authentication;
		Din_Go(415,2048);goto parse_flag;

	case sPrintMotd:
		Din_Go(416,2048);intptr = &options->print_motd;
		Din_Go(417,2048);goto parse_flag;

	case sPrintLastLog:
		Din_Go(418,2048);intptr = &options->print_lastlog;
		Din_Go(419,2048);goto parse_flag;

	case sX11Forwarding:
		Din_Go(420,2048);intptr = &options->x11_forwarding;
		Din_Go(421,2048);goto parse_flag;

	case sX11DisplayOffset:
		Din_Go(422,2048);intptr = &options->x11_display_offset;
		Din_Go(423,2048);goto parse_int;

	case sX11MaxDisplays:
		Din_Go(424,2048);intptr = &options->x11_max_displays;
		Din_Go(425,2048);goto parse_int;

	case sX11UseLocalhost:
		Din_Go(426,2048);intptr = &options->x11_use_localhost;
		Din_Go(427,2048);goto parse_flag;

	case sXAuthLocation:
		Din_Go(428,2048);charptr = &options->xauth_location;
		Din_Go(429,2048);goto parse_filename;

	case sPermitTTY:
		Din_Go(430,2048);intptr = &options->permit_tty;
		Din_Go(431,2048);goto parse_flag;

	case sStrictModes:
		Din_Go(432,2048);intptr = &options->strict_modes;
		Din_Go(433,2048);goto parse_flag;

	case sTCPKeepAlive:
		Din_Go(434,2048);intptr = &options->tcp_keep_alive;
		Din_Go(435,2048);goto parse_flag;

	case sEmptyPasswd:
		Din_Go(436,2048);intptr = &options->permit_empty_passwd;
		Din_Go(437,2048);goto parse_flag;

	case sPermitUserEnvironment:
		Din_Go(438,2048);intptr = &options->permit_user_env;
		Din_Go(439,2048);goto parse_flag;

	case sUseLogin:
		Din_Go(440,2048);intptr = &options->use_login;
		Din_Go(441,2048);goto parse_flag;

	case sCompression:
		Din_Go(442,2048);intptr = &options->compression;
		multistate_ptr = multistate_compression;
		Din_Go(443,2048);goto parse_multistate;

	case sRekeyLimit:
		Din_Go(444,2048);arg = strdelim(&cp);
		Din_Go(446,2048);if (!arg || *arg == '\0')
			{/*185*/Din_Go(445,2048);fatal("%.200s line %d: Missing argument.", filename,
			    linenum);/*186*/}
		Din_Go(454,2048);if (strcmp(arg, "default") == 0) {
			Din_Go(447,2048);val64 = 0;
		} else {
			Din_Go(448,2048);if (scan_scaled(arg, &val64) == -1)
				{/*187*/Din_Go(449,2048);fatal("%.200s line %d: Bad number '%s': %s",
				    filename, linenum, arg, strerror(errno));/*188*/}
			/* check for too-large or too-small limits */
			Din_Go(451,2048);if (val64 > UINT_MAX)
				{/*189*/Din_Go(450,2048);fatal("%.200s line %d: RekeyLimit too large",
				    filename, linenum);/*190*/}
			Din_Go(453,2048);if (val64 != 0 && val64 < 16)
				{/*191*/Din_Go(452,2048);fatal("%.200s line %d: RekeyLimit too small",
				    filename, linenum);/*192*/}
		}
		Din_Go(456,2048);if (*activep && options->rekey_limit == -1)
			{/*193*/Din_Go(455,2048);options->rekey_limit = (u_int32_t)val64;/*194*/}
		Din_Go(462,2048);if (cp != NULL) { /* optional rekey interval present */
			Din_Go(457,2048);if (strcmp(cp, "none") == 0) {
				Din_Go(458,2048);(void)strdelim(&cp);	/* discard */
				Din_Go(459,2048);break;
			}
			Din_Go(460,2048);intptr = &options->rekey_interval;
			Din_Go(461,2048);goto parse_time;
		}
		Din_Go(463,2048);break;

	case sGatewayPorts:
		Din_Go(464,2048);intptr = &options->gateway_ports;
		multistate_ptr = multistate_gatewayports;
		Din_Go(465,2048);goto parse_multistate;

	case sUseDNS:
		Din_Go(466,2048);intptr = &options->use_dns;
		Din_Go(467,2048);goto parse_flag;

	case sLogFacility:
		Din_Go(468,2048);log_facility_ptr = &options->log_facility;
		arg = strdelim(&cp);
		value = log_facility_number(arg);
		Din_Go(470,2048);if (value == SYSLOG_FACILITY_NOT_SET)
			{/*195*/Din_Go(469,2048);fatal("%.200s line %d: unsupported log facility '%s'",
			    filename, linenum, arg ? arg : "<NONE>");/*196*/}
		Din_Go(472,2048);if (*log_facility_ptr == -1)
			{/*197*/Din_Go(471,2048);*log_facility_ptr = (SyslogFacility) value;/*198*/}
		Din_Go(473,2048);break;

	case sLogLevel:
		Din_Go(474,2048);log_level_ptr = &options->log_level;
		arg = strdelim(&cp);
		value = log_level_number(arg);
		Din_Go(476,2048);if (value == SYSLOG_LEVEL_NOT_SET)
			{/*199*/Din_Go(475,2048);fatal("%.200s line %d: unsupported log level '%s'",
			    filename, linenum, arg ? arg : "<NONE>");/*200*/}
		Din_Go(478,2048);if (*log_level_ptr == -1)
			{/*201*/Din_Go(477,2048);*log_level_ptr = (LogLevel) value;/*202*/}
		Din_Go(479,2048);break;

	case sAllowTcpForwarding:
		Din_Go(480,2048);intptr = &options->allow_tcp_forwarding;
		multistate_ptr = multistate_tcpfwd;
		Din_Go(481,2048);goto parse_multistate;

	case sAllowAgentForwarding:
		Din_Go(482,2048);intptr = &options->allow_agent_forwarding;
		Din_Go(483,2048);goto parse_flag;

	case sUsePrivilegeSeparation:
		Din_Go(484,2048);intptr = &use_privsep;
		multistate_ptr = multistate_privsep;
		Din_Go(485,2048);goto parse_multistate;

	case sShowPatchLevel:
		Din_Go(486,2048);intptr = &options->show_patchlevel;
		Din_Go(487,2048);goto parse_flag;

	case sAllowUsers:
		Din_Go(488,2048);while ((arg = strdelim(&cp)) && *arg != '\0') {
			Din_Go(489,2048);if (options->num_allow_users >= MAX_ALLOW_USERS)
				{/*203*/Din_Go(490,2048);fatal("%s line %d: too many allow users.",
				    filename, linenum);/*204*/}
			Din_Go(492,2048);if (!*activep)
				{/*205*/Din_Go(491,2048);continue;/*206*/}
			Din_Go(493,2048);options->allow_users[options->num_allow_users++] =
			    xstrdup(arg);
		}
		Din_Go(494,2048);break;

	case sDenyUsers:
		Din_Go(495,2048);while ((arg = strdelim(&cp)) && *arg != '\0') {
			Din_Go(496,2048);if (options->num_deny_users >= MAX_DENY_USERS)
				{/*207*/Din_Go(497,2048);fatal("%s line %d: too many deny users.",
				    filename, linenum);/*208*/}
			Din_Go(499,2048);if (!*activep)
				{/*209*/Din_Go(498,2048);continue;/*210*/}
			Din_Go(500,2048);options->deny_users[options->num_deny_users++] =
			    xstrdup(arg);
		}
		Din_Go(501,2048);break;

	case sAllowGroups:
		Din_Go(502,2048);while ((arg = strdelim(&cp)) && *arg != '\0') {
			Din_Go(503,2048);if (options->num_allow_groups >= MAX_ALLOW_GROUPS)
				{/*211*/Din_Go(504,2048);fatal("%s line %d: too many allow groups.",
				    filename, linenum);/*212*/}
			Din_Go(506,2048);if (!*activep)
				{/*213*/Din_Go(505,2048);continue;/*214*/}
			Din_Go(507,2048);options->allow_groups[options->num_allow_groups++] =
			    xstrdup(arg);
		}
		Din_Go(508,2048);break;

	case sDenyGroups:
		Din_Go(509,2048);while ((arg = strdelim(&cp)) && *arg != '\0') {
			Din_Go(510,2048);if (options->num_deny_groups >= MAX_DENY_GROUPS)
				{/*215*/Din_Go(511,2048);fatal("%s line %d: too many deny groups.",
				    filename, linenum);/*216*/}
			Din_Go(513,2048);if (!*activep)
				{/*217*/Din_Go(512,2048);continue;/*218*/}
			Din_Go(514,2048);options->deny_groups[options->num_deny_groups++] =
			    xstrdup(arg);
		}
		Din_Go(515,2048);break;

	case sCiphers:
		Din_Go(516,2048);arg = strdelim(&cp);
		Din_Go(518,2048);if (!arg || *arg == '\0')
			{/*219*/Din_Go(517,2048);fatal("%s line %d: Missing argument.", filename, linenum);/*220*/}
		Din_Go(520,2048);if (!ciphers_valid(arg))
			{/*221*/Din_Go(519,2048);fatal("%s line %d: Bad SSH2 cipher spec '%s'.",
			    filename, linenum, arg ? arg : "<NONE>");/*222*/}
		Din_Go(522,2048);if (options->ciphers == NULL)
			{/*223*/Din_Go(521,2048);options->ciphers = xstrdup(arg);/*224*/}
		Din_Go(523,2048);break;

	case sMacs:
		Din_Go(524,2048);arg = strdelim(&cp);
		Din_Go(526,2048);if (!arg || *arg == '\0')
			{/*225*/Din_Go(525,2048);fatal("%s line %d: Missing argument.", filename, linenum);/*226*/}
		Din_Go(528,2048);if (!mac_valid(arg))
			{/*227*/Din_Go(527,2048);fatal("%s line %d: Bad SSH2 mac spec '%s'.",
			    filename, linenum, arg ? arg : "<NONE>");/*228*/}
		Din_Go(530,2048);if (options->macs == NULL)
			{/*229*/Din_Go(529,2048);options->macs = xstrdup(arg);/*230*/}
		Din_Go(531,2048);break;

	case sKexAlgorithms:
		Din_Go(532,2048);arg = strdelim(&cp);
		Din_Go(534,2048);if (!arg || *arg == '\0')
			{/*231*/Din_Go(533,2048);fatal("%s line %d: Missing argument.",
			    filename, linenum);/*232*/}
		Din_Go(536,2048);if (!kex_names_valid(arg))
			{/*233*/Din_Go(535,2048);fatal("%s line %d: Bad SSH2 KexAlgorithms '%s'.",
			    filename, linenum, arg ? arg : "<NONE>");/*234*/}
		Din_Go(538,2048);if (options->kex_algorithms == NULL)
			{/*235*/Din_Go(537,2048);options->kex_algorithms = xstrdup(arg);/*236*/}
		Din_Go(539,2048);break;

	case sProtocol:
		Din_Go(540,2048);intptr = &options->protocol;
		arg = strdelim(&cp);
		Din_Go(542,2048);if (!arg || *arg == '\0')
			{/*237*/Din_Go(541,2048);fatal("%s line %d: Missing argument.", filename, linenum);/*238*/}
		Din_Go(543,2048);value = proto_spec(arg);
		Din_Go(545,2048);if (value == SSH_PROTO_UNKNOWN)
			{/*239*/Din_Go(544,2048);fatal("%s line %d: Bad protocol spec '%s'.",
			    filename, linenum, arg ? arg : "<NONE>");/*240*/}
		Din_Go(547,2048);if (*intptr == SSH_PROTO_UNKNOWN)
			{/*241*/Din_Go(546,2048);*intptr = value;/*242*/}
		Din_Go(548,2048);break;

	case sSubsystem:
		Din_Go(549,2048);if (options->num_subsystems >= MAX_SUBSYSTEMS) {
			Din_Go(550,2048);fatal("%s line %d: too many subsystems defined.",
			    filename, linenum);
		}
		Din_Go(551,2048);arg = strdelim(&cp);
		Din_Go(553,2048);if (!arg || *arg == '\0')
			{/*243*/Din_Go(552,2048);fatal("%s line %d: Missing subsystem name.",
			    filename, linenum);/*244*/}
		Din_Go(556,2048);if (!*activep) {
			/*arg =*/ Din_Go(554,2048);(void) strdelim(&cp);
			Din_Go(555,2048);break;
		}
		Din_Go(559,2048);for (i = 0; i < options->num_subsystems; i++)
			{/*245*/Din_Go(557,2048);if (strcmp(arg, options->subsystem_name[i]) == 0)
				{/*247*/Din_Go(558,2048);fatal("%s line %d: Subsystem '%s' already defined.",
				    filename, linenum, arg);/*248*/}/*246*/}
		Din_Go(560,2048);options->subsystem_name[options->num_subsystems] = xstrdup(arg);
		arg = strdelim(&cp);
		Din_Go(562,2048);if (!arg || *arg == '\0')
			{/*249*/Din_Go(561,2048);fatal("%s line %d: Missing subsystem command.",
			    filename, linenum);/*250*/}
		Din_Go(563,2048);options->subsystem_command[options->num_subsystems] = xstrdup(arg);

		/* Collect arguments (separate to executable) */
		p = xstrdup(arg);
		len = strlen(p) + 1;
		Din_Go(565,2048);while ((arg = strdelim(&cp)) != NULL && *arg != '\0') {
			Din_Go(564,2048);len += 1 + strlen(arg);
			p = xrealloc(p, 1, len);
			strlcat(p, " ", len);
			strlcat(p, arg, len);
		}
		Din_Go(566,2048);options->subsystem_args[options->num_subsystems] = p;
		options->num_subsystems++;
		Din_Go(567,2048);break;

	case sMaxStartups:
		Din_Go(568,2048);arg = strdelim(&cp);
		Din_Go(570,2048);if (!arg || *arg == '\0')
			{/*251*/Din_Go(569,2048);fatal("%s line %d: Missing MaxStartups spec.",
			    filename, linenum);/*252*/}
		Din_Go(576,2048);if ((n = sscanf(arg, "%d:%d:%d",
		    &options->max_startups_begin,
		    &options->max_startups_rate,
		    &options->max_startups)) == 3) {
			Din_Go(571,2048);if (options->max_startups_begin >
			    options->max_startups ||
			    options->max_startups_rate > 100 ||
			    options->max_startups_rate < 1)
				{/*253*/Din_Go(572,2048);fatal("%s line %d: Illegal MaxStartups spec.",
				    filename, linenum);/*254*/}
		} else {/*255*/Din_Go(573,2048);if (n != 1)
			{/*257*/Din_Go(574,2048);fatal("%s line %d: Illegal MaxStartups spec.",
			    filename, linenum);/*258*/}
		else
			{/*259*/Din_Go(575,2048);options->max_startups = options->max_startups_begin;/*260*/}/*256*/}
		Din_Go(577,2048);break;

	case sMaxAuthTries:
		Din_Go(578,2048);intptr = &options->max_authtries;
		Din_Go(579,2048);goto parse_int;

	case sMaxSessions:
		Din_Go(580,2048);intptr = &options->max_sessions;
		Din_Go(581,2048);goto parse_int;

	case sBanner:
		Din_Go(582,2048);charptr = &options->banner;
		Din_Go(583,2048);goto parse_filename;

	/*
	 * These options can contain %X options expanded at
	 * connect time, so that you can specify paths like:
	 *
	 * AuthorizedKeysFile	/etc/ssh_keys/%u
	 */
	case sAuthorizedKeysFile:
		Din_Go(584,2048);if (*activep && options->num_authkeys_files == 0) {
			Din_Go(585,2048);while ((arg = strdelim(&cp)) && *arg != '\0') {
				Din_Go(586,2048);if (options->num_authkeys_files >=
				    MAX_AUTHKEYS_FILES)
					{/*261*/Din_Go(587,2048);fatal("%s line %d: "
					    "too many authorized keys files.",
					    filename, linenum);/*262*/}
				Din_Go(588,2048);options->authorized_keys_files[
				    options->num_authkeys_files++] =
				    tilde_expand_filename(arg, getuid());
			}
		}
		{int  ReplaceReturn = 0; Din_Go(589,2048); return ReplaceReturn;}

	case sAuthorizedPrincipalsFile:
		Din_Go(590,2048);charptr = &options->authorized_principals_file;
		arg = strdelim(&cp);
		Din_Go(592,2048);if (!arg || *arg == '\0')
			{/*263*/Din_Go(591,2048);fatal("%s line %d: missing file name.",
			    filename, linenum);/*264*/}
		Din_Go(594,2048);if (*activep && *charptr == NULL) {
			Din_Go(593,2048);*charptr = tilde_expand_filename(arg, getuid());
			/* increase optional counter */
			/* DEAD CODE intptr is still NULL ;)
  			 if (intptr != NULL)
				*intptr = *intptr + 1; */
		}
		Din_Go(595,2048);break;

	case sClientAliveInterval:
		Din_Go(596,2048);intptr = &options->client_alive_interval;
		Din_Go(597,2048);goto parse_time;

	case sClientAliveCountMax:
		Din_Go(598,2048);intptr = &options->client_alive_count_max;
		Din_Go(599,2048);goto parse_int;

	case sAcceptEnv:
		Din_Go(600,2048);while ((arg = strdelim(&cp)) && *arg != '\0') {
			Din_Go(601,2048);if (strchr(arg, '=') != NULL)
				{/*265*/Din_Go(602,2048);fatal("%s line %d: Invalid environment name.",
				    filename, linenum);/*266*/}
			Din_Go(604,2048);if (options->num_accept_env >= MAX_ACCEPT_ENV)
				{/*267*/Din_Go(603,2048);fatal("%s line %d: too many allow env.",
				    filename, linenum);/*268*/}
			Din_Go(606,2048);if (!*activep)
				{/*269*/Din_Go(605,2048);continue;/*270*/}
			Din_Go(607,2048);options->accept_env[options->num_accept_env++] =
			    xstrdup(arg);
		}
		Din_Go(608,2048);break;

	case sPermitTunnel:
		Din_Go(609,2048);intptr = &options->permit_tun;
		arg = strdelim(&cp);
		Din_Go(611,2048);if (!arg || *arg == '\0')
			{/*271*/Din_Go(610,2048);fatal("%s line %d: Missing yes/point-to-point/"
			    "ethernet/no argument.", filename, linenum);/*272*/}
		Din_Go(612,2048);value = -1;
		Din_Go(616,2048);for (i = 0; tunmode_desc[i].val != -1; i++)
			{/*273*/Din_Go(613,2048);if (strcmp(tunmode_desc[i].text, arg) == 0) {
				Din_Go(614,2048);value = tunmode_desc[i].val;
				Din_Go(615,2048);break;
			;/*274*/}}
		Din_Go(618,2048);if (value == -1)
			{/*275*/Din_Go(617,2048);fatal("%s line %d: Bad yes/point-to-point/ethernet/"
			    "no argument: %s", filename, linenum, arg);/*276*/}
		Din_Go(620,2048);if (*intptr == -1)
			{/*277*/Din_Go(619,2048);*intptr = value;/*278*/}
		Din_Go(621,2048);break;

	case sMatch:
		Din_Go(622,2048);if (cmdline)
			{/*279*/Din_Go(623,2048);fatal("Match directive not supported as a command-line "
			   "option");/*280*/}
		Din_Go(624,2048);value = match_cfg_line(&cp, linenum, connectinfo);
		Din_Go(626,2048);if (value < 0)
			{/*281*/Din_Go(625,2048);fatal("%s line %d: Bad Match condition", filename,
			    linenum);/*282*/}
		Din_Go(627,2048);*activep = value;
		Din_Go(628,2048);break;

	case sKerberosUseKuserok:
		Din_Go(629,2048);intptr = &options->use_kuserok;
		Din_Go(630,2048);goto parse_flag;

	case sGssEnablek5users:
		Din_Go(631,2048);intptr = &options->enable_k5users;
		Din_Go(632,2048);goto parse_flag;

	case sPermitOpen:
		Din_Go(633,2048);arg = strdelim(&cp);
		Din_Go(635,2048);if (!arg || *arg == '\0')
			{/*283*/Din_Go(634,2048);fatal("%s line %d: missing PermitOpen specification",
			    filename, linenum);/*284*/}
		Din_Go(636,2048);n = options->num_permitted_opens;	/* modified later */
		Din_Go(640,2048);if (strcmp(arg, "any") == 0) {
			Din_Go(637,2048);if (*activep && n == -1) {
				Din_Go(638,2048);channel_clear_adm_permitted_opens();
				options->num_permitted_opens = 0;
			}
			Din_Go(639,2048);break;
		}
		Din_Go(644,2048);if (strcmp(arg, "none") == 0) {
			Din_Go(641,2048);if (*activep && n == -1) {
				Din_Go(642,2048);options->num_permitted_opens = 1;
				channel_disable_adm_local_opens();
			}
			Din_Go(643,2048);break;
		}
		Din_Go(646,2048);if (*activep && n == -1)
			{/*285*/Din_Go(645,2048);channel_clear_adm_permitted_opens();/*286*/}
		Din_Go(655,2048);for (; arg != NULL && *arg != '\0'; arg = strdelim(&cp)) {
			Din_Go(647,2048);p = hpdelim(&arg);
			Din_Go(649,2048);if (p == NULL)
				{/*287*/Din_Go(648,2048);fatal("%s line %d: missing host in PermitOpen",
				    filename, linenum);/*288*/}
			Din_Go(650,2048);p = cleanhostname(p);
			Din_Go(652,2048);if (arg == NULL || ((port = permitopen_port(arg)) < 0))
				{/*289*/Din_Go(651,2048);fatal("%s line %d: bad port number in "
				    "PermitOpen", filename, linenum);/*290*/}
			Din_Go(654,2048);if (*activep && n == -1)
				{/*291*/Din_Go(653,2048);options->num_permitted_opens =
				    channel_add_adm_permitted_opens(p, port);/*292*/}
		}
		Din_Go(656,2048);break;

	case sForceCommand:
		Din_Go(657,2048);if (cp == NULL || *cp == '\0')
			{/*293*/Din_Go(658,2048);fatal("%.200s line %d: Missing argument.", filename,
			    linenum);/*294*/}
		Din_Go(659,2048);len = strspn(cp, WHITESPACE);
		Din_Go(661,2048);if (*activep && options->adm_forced_command == NULL)
			{/*295*/Din_Go(660,2048);options->adm_forced_command = xstrdup(cp + len);/*296*/}
		{int  ReplaceReturn = 0; Din_Go(662,2048); return ReplaceReturn;}

	case sChrootDirectory:
		Din_Go(663,2048);charptr = &options->chroot_directory;

		arg = strdelim(&cp);
		Din_Go(665,2048);if (!arg || *arg == '\0')
			{/*297*/Din_Go(664,2048);fatal("%s line %d: missing file name.",
			    filename, linenum);/*298*/}
		Din_Go(667,2048);if (*activep && *charptr == NULL)
			{/*299*/Din_Go(666,2048);*charptr = xstrdup(arg);/*300*/}
		Din_Go(668,2048);break;

	case sTrustedUserCAKeys:
		Din_Go(669,2048);charptr = &options->trusted_user_ca_keys;
		Din_Go(670,2048);goto parse_filename;

	case sRevokedKeys:
		Din_Go(671,2048);charptr = &options->revoked_keys_file;
		Din_Go(672,2048);goto parse_filename;

	case sIPQoS:
		Din_Go(673,2048);arg = strdelim(&cp);
		Din_Go(675,2048);if ((value = parse_ipqos(arg)) == -1)
			{/*301*/Din_Go(674,2048);fatal("%s line %d: Bad IPQoS value: %s",
			    filename, linenum, arg);/*302*/}
		Din_Go(676,2048);arg = strdelim(&cp);
		Din_Go(680,2048);if (arg == NULL)
			{/*303*/Din_Go(677,2048);value2 = value;/*304*/}
		else {/*305*/Din_Go(678,2048);if ((value2 = parse_ipqos(arg)) == -1)
			{/*307*/Din_Go(679,2048);fatal("%s line %d: Bad IPQoS value: %s",
			    filename, linenum, arg);/*308*/}/*306*/}
		Din_Go(682,2048);if (*activep) {
			Din_Go(681,2048);options->ip_qos_interactive = value;
			options->ip_qos_bulk = value2;
		}
		Din_Go(683,2048);break;

	case sVersionAddendum:
		Din_Go(684,2048);if (cp == NULL || *cp == '\0')
			{/*309*/Din_Go(685,2048);fatal("%.200s line %d: Missing argument.", filename,
			    linenum);/*310*/}
		Din_Go(686,2048);len = strspn(cp, WHITESPACE);
		Din_Go(692,2048);if (*activep && options->version_addendum == NULL) {
			Din_Go(687,2048);if (strcasecmp(cp + len, "none") == 0)
				{/*311*/Din_Go(688,2048);options->version_addendum = xstrdup("");/*312*/}
			else {/*313*/Din_Go(689,2048);if (strchr(cp + len, '\r') != NULL)
				{/*315*/Din_Go(690,2048);fatal("%.200s line %d: Invalid argument",
				    filename, linenum);/*316*/}
			else
				{/*317*/Din_Go(691,2048);options->version_addendum = xstrdup(cp + len);/*318*/}/*314*/}
		}
		{int  ReplaceReturn = 0; Din_Go(693,2048); return ReplaceReturn;}

	case sAuthorizedKeysCommand:
		Din_Go(694,2048);len = strspn(cp, WHITESPACE);
		Din_Go(698,2048);if (*activep && options->authorized_keys_command == NULL) {
			Din_Go(695,2048);if (cp[len] != '/' && strcasecmp(cp + len, "none") != 0)
				{/*319*/Din_Go(696,2048);fatal("%.200s line %d: AuthorizedKeysCommand "
				    "must be an absolute path",
				    filename, linenum);/*320*/}
			Din_Go(697,2048);options->authorized_keys_command = xstrdup(cp + len);
		}
		{int  ReplaceReturn = 0; Din_Go(699,2048); return ReplaceReturn;}

	case sAuthorizedKeysCommandUser:
		Din_Go(700,2048);charptr = &options->authorized_keys_command_user;

		arg = strdelim(&cp);
		Din_Go(702,2048);if (*activep && *charptr == NULL)
			{/*321*/Din_Go(701,2048);*charptr = xstrdup(arg);/*322*/}
		Din_Go(703,2048);break;

	case sAuthenticationMethods:
		Din_Go(704,2048);if (options->num_auth_methods == 0) {
			Din_Go(705,2048);value = 0; /* seen "any" pseudo-method */
			value2 = 0; /* sucessfully parsed any method */
			Din_Go(720,2048);while ((arg = strdelim(&cp)) && *arg != '\0') {
				Din_Go(706,2048);if (options->num_auth_methods >=
				    MAX_AUTH_METHODS)
					{/*323*/Din_Go(707,2048);fatal("%s line %d: "
					    "too many authentication methods.",
					    filename, linenum);/*324*/}
				Din_Go(715,2048);if (strcmp(arg, "any") == 0) {
					Din_Go(708,2048);if (options->num_auth_methods > 0) {
						Din_Go(709,2048);fatal("%s line %d: \"any\" "
						    "must appear alone in "
						    "AuthenticationMethods",
						    filename, linenum);
					}
					Din_Go(710,2048);value = 1;
				} else {/*325*/Din_Go(711,2048);if (value) {
					Din_Go(712,2048);fatal("%s line %d: \"any\" must appear "
					    "alone in AuthenticationMethods",
					    filename, linenum);
				} else {/*327*/Din_Go(713,2048);if (auth2_methods_valid(arg, 0) != 0) {
					Din_Go(714,2048);fatal("%s line %d: invalid "
					    "authentication method list.",
					    filename, linenum);
				;/*328*/}/*326*/}}
				Din_Go(716,2048);value2 = 1;
 				Din_Go(718,2048);if (!*activep)
 					{/*329*/Din_Go(717,2048);continue;/*330*/}
				Din_Go(719,2048);options->auth_methods[
				    options->num_auth_methods++] = xstrdup(arg);
			}
			Din_Go(722,2048);if (value2 == 0) {
				Din_Go(721,2048);fatal("%s line %d: no AuthenticationMethods "
				    "specified", filename, linenum);
			}
		}
		{int  ReplaceReturn = 0; Din_Go(723,2048); return ReplaceReturn;}

	case sExposeAuthenticationMethods:
		Din_Go(724,2048);intptr = &options->expose_auth_methods;
		multistate_ptr = multistate_exposeauthmeth;
		Din_Go(725,2048);goto parse_multistate;

	case sDeprecated:
		Din_Go(726,2048);logit("%s line %d: Deprecated option %s",
		    filename, linenum, arg);
		Din_Go(728,2048);while (arg)
		    {/*331*/Din_Go(727,2048);arg = strdelim(&cp);/*332*/}
		Din_Go(729,2048);break;

	case sUnsupported:
		Din_Go(730,2048);logit("%s line %d: Unsupported option %s",
		    filename, linenum, arg);
		Din_Go(732,2048);while (arg)
		    {/*333*/Din_Go(731,2048);arg = strdelim(&cp);/*334*/}
		Din_Go(733,2048);break;

	default:
		Din_Go(734,2048);fatal("%s line %d: Missing handler for opcode %s (%d)",
		    filename, linenum, arg, opcode);
	}
	Din_Go(737,2048);if ((arg = strdelim(&cp)) != NULL && *arg != '\0')
		{/*335*/Din_Go(736,2048);fatal("%s line %d: garbage at end of line; \"%.200s\".",
		    filename, linenum, arg);/*336*/}
	{int  ReplaceReturn = 0; Din_Go(738,2048); return ReplaceReturn;}
}

/* Reads the server configuration file. */

void
load_server_config(const char *filename, Buffer *conf)
{
	Din_Go(739,2048);char line[4096], *cp;
	FILE *f;
	int lineno = 0;

	debug2("%s: filename %s", __func__, filename);
	Din_Go(741,2048);if ((f = fopen(filename, "r")) == NULL) {
		Din_Go(740,2048);perror(filename);
		exit(1);
	}
	Din_Go(742,2048);buffer_clear(conf);
	Din_Go(749,2048);while (fgets(line, sizeof(line), f)) {
		Din_Go(743,2048);lineno++;
		Din_Go(745,2048);if (strlen(line) == sizeof(line) - 1)
			{/*337*/Din_Go(744,2048);fatal("%s line %d too long", filename, lineno);/*338*/}
		/*
		 * Trim out comments and strip whitespace
		 * NB - preserve newlines, they are needed to reproduce
		 * line numbers later for error messages
		 */
		Din_Go(747,2048);if ((cp = strchr(line, '#')) != NULL)
			{/*339*/Din_Go(746,2048);memcpy(cp, "\n", 2);/*340*/}
		Din_Go(748,2048);cp = line + strspn(line, " \t\r");

		buffer_append(conf, cp, strlen(cp));
	}
	Din_Go(750,2048);buffer_append(conf, "\0", 1);
	fclose(f);
	debug2("%s: done config len = %d", __func__, buffer_len(conf));
Din_Go(751,2048);}

void
parse_server_match_config(ServerOptions *options,
   struct connection_info *connectinfo)
{
	Din_Go(752,2048);ServerOptions mo;

	initialize_server_options(&mo);
	parse_server_config(&mo, "reprocess config", &cfg, connectinfo);
	copy_set_server_options(options, &mo, 0);
Din_Go(753,2048);}

int parse_server_match_testspec(struct connection_info *ci, char *spec)
{
	Din_Go(754,2048);char *p;

	Din_Go(770,2048);while ((p = strsep(&spec, ",")) && *p != '\0') {
		Din_Go(755,2048);if (strncmp(p, "addr=", 5) == 0) {
			Din_Go(756,2048);ci->address = xstrdup(p + 5);
		} else {/*345*/Din_Go(757,2048);if (strncmp(p, "host=", 5) == 0) {
			Din_Go(758,2048);ci->host = xstrdup(p + 5);
		} else {/*347*/Din_Go(759,2048);if (strncmp(p, "user=", 5) == 0) {
			Din_Go(760,2048);ci->user = xstrdup(p + 5);
		} else {/*349*/Din_Go(761,2048);if (strncmp(p, "laddr=", 6) == 0) {
			Din_Go(762,2048);ci->laddress = xstrdup(p + 6);
		} else {/*351*/Din_Go(763,2048);if (strncmp(p, "lport=", 6) == 0) {
			Din_Go(764,2048);ci->lport = a2port(p + 6);
			Din_Go(767,2048);if (ci->lport == -1) {
				Din_Go(765,2048);fprintf(stderr, "Invalid port '%s' in test mode"
				   " specification %s\n", p+6, p);
				{int  ReplaceReturn = -1; Din_Go(766,2048); return ReplaceReturn;}
			}
		} else {
			Din_Go(768,2048);fprintf(stderr, "Invalid test mode specification %s\n",
			   p);
			{int  ReplaceReturn = -1; Din_Go(769,2048); return ReplaceReturn;}
		;/*352*/}/*350*/}/*348*/}/*346*/}}
	}
	{int  ReplaceReturn = 0; Din_Go(771,2048); return ReplaceReturn;}
}

/*
 * returns 1 for a complete spec, 0 for partial spec and -1 for an
 * empty spec.
 */
int server_match_spec_complete(struct connection_info *ci)
{
	Din_Go(772,2048);if (ci->user && ci->host && ci->address)
		{/*353*/{int  ReplaceReturn = 1; Din_Go(773,2048); return ReplaceReturn;}/*354*/}	/* complete */
	Din_Go(775,2048);if (!ci->user && !ci->host && !ci->address)
		{/*355*/{int  ReplaceReturn = -1; Din_Go(774,2048); return ReplaceReturn;}/*356*/}	/* empty */
	{int  ReplaceReturn = 0; Din_Go(776,2048); return ReplaceReturn;}	/* partial */
}

/*
 * Copy any supported values that are set.
 *
 * If the preauth flag is set, we do not bother copying the string or
 * array values that are not used pre-authentication, because any that we
 * do use must be explictly sent in mm_getpwnamallow().
 */
void
copy_set_server_options(ServerOptions *dst, ServerOptions *src, int preauth)
{
#define M_CP_INTOPT(n) do {\
	if (src->n != -1) \
		dst->n = src->n; \
} while (0)

	Din_Go(777,2048);u_int i;

	M_CP_INTOPT(password_authentication);/*358*/}
	M_CP_INTOPT(gss_authentication);
	M_CP_INTOPT(rsa_authentication);
	M_CP_INTOPT(pubkey_authentication);
	M_CP_INTOPT(kerberos_authentication);
	M_CP_INTOPT(hostbased_authentication);
	M_CP_INTOPT(hostbased_uses_name_from_packet_only);
	M_CP_INTOPT(kbd_interactive_authentication);
	M_CP_INTOPT(permit_root_login);
	M_CP_INTOPT(permit_empty_passwd);

	M_CP_INTOPT(allow_tcp_forwarding);
	M_CP_INTOPT(allow_agent_forwarding);
	M_CP_INTOPT(permit_tun);
	M_CP_INTOPT(gateway_ports);
	M_CP_INTOPT(x11_display_offset);
	M_CP_INTOPT(x11_forwarding);
	M_CP_INTOPT(x11_max_displays);
	M_CP_INTOPT(x11_use_localhost);
	M_CP_INTOPT(permit_tty);
	M_CP_INTOPT(max_sessions);
	M_CP_INTOPT(max_authtries);
	M_CP_INTOPT(ip_qos_interactive);
	M_CP_INTOPT(ip_qos_bulk);
	M_CP_INTOPT(use_kuserok);
	M_CP_INTOPT(enable_k5users);
	M_CP_INTOPT(rekey_limit);
	M_CP_INTOPT(rekey_interval);
	M_CP_INTOPT(expose_auth_methods);

	/* M_CP_STROPT and M_CP_STRARRAYOPT should not appear before here */
#define M_CP_STROPT(n) do {\
	if (src->n != NULL && dst->n != src->n) { \
		free(dst->n); \
		dst->n = src->n; \
	} \
} while(0)
#define M_CP_STRARRAYOPT(n, num_n) do {\
	if (src->num_n != 0) { \
		for (i = 0; i < dst->num_n; i++) \
			free(dst->n[i]); \
		for (dst->num_n = 0; dst->num_n < src->num_n; dst->num_n++) \
			dst->n[dst->num_n] = src->n[dst->num_n]; \
	} \
} while(0)

	/* See comment in servconf.h */
	COPY_MATCH_STRING_OPTS();

	/*
	 * The only things that should be below this point are string options
	 * which are only used after authentication.
	 */
	Din_Go(782,2048);if (preauth)
		{/*441*/Din_Go(781,2048);return;/*442*/}

	M_CP_STROPT(adm_forced_command);
	M_CP_STROPT(chroot_directory);
Din_Go(786,2048);}

#undef M_CP_INTOPT
#undef M_CP_STROPT
#undef M_CP_STRARRAYOPT

void
parse_server_config(ServerOptions *options, const char *filename, Buffer *conf,
    struct connection_info *connectinfo)
{
	Din_Go(787,2048);int active, linenum, bad_options = 0;
	char *cp, *obuf, *cbuf;

	debug2("%s: config %s len %d", __func__, filename, buffer_len(conf));

	obuf = cbuf = xstrdup(buffer_ptr(conf));
	active = connectinfo ? 0 : 1;
	linenum = 1;
	Din_Go(790,2048);while ((cp = strsep(&cbuf, "\n")) != NULL) {
		Din_Go(788,2048);if (process_server_config_line(options, cp, filename,
		    linenum++, &active, connectinfo) != 0)
			{/*341*/Din_Go(789,2048);bad_options++;/*342*/}
	}
	Din_Go(791,2048);free(obuf);
	Din_Go(793,2048);if (bad_options > 0)
		{/*343*/Din_Go(792,2048);fatal("%s: terminating, %d bad configuration options",
		    filename, bad_options);/*344*/}
Din_Go(794,2048);}

static const char *
fmt_multistate_int(int val, const struct multistate *m)
{
	Din_Go(795,2048);u_int i;

	Din_Go(798,2048);for (i = 0; m[i].key != NULL; i++) {
		Din_Go(796,2048);if (m[i].value == val)
			{/*503*/{const char * ReplaceReturn = m[i].key; Din_Go(797,2048); return ReplaceReturn;}/*504*/}
	}
	{const char * ReplaceReturn = "UNKNOWN"; Din_Go(799,2048); return ReplaceReturn;}
}

static const char *
fmt_intarg(ServerOpCodes code, int val)
{
	Din_Go(800,2048);if (val == -1)
		{/*505*/{const char * ReplaceReturn = "unset"; Din_Go(801,2048); return ReplaceReturn;}/*506*/}
	Din_Go(818,2048);switch (code) {
	case sAddressFamily:
		{const char * ReplaceReturn = fmt_multistate_int(val, multistate_addressfamily); Din_Go(802,2048); return ReplaceReturn;}
	case sPermitRootLogin:
		{const char * ReplaceReturn = fmt_multistate_int(val, multistate_permitrootlogin); Din_Go(803,2048); return ReplaceReturn;}
	case sGatewayPorts:
		{const char * ReplaceReturn = fmt_multistate_int(val, multistate_gatewayports); Din_Go(804,2048); return ReplaceReturn;}
	case sCompression:
		{const char * ReplaceReturn = fmt_multistate_int(val, multistate_compression); Din_Go(805,2048); return ReplaceReturn;}
	case sUsePrivilegeSeparation:
		{const char * ReplaceReturn = fmt_multistate_int(val, multistate_privsep); Din_Go(806,2048); return ReplaceReturn;}
	case sAllowTcpForwarding:
		{const char * ReplaceReturn = fmt_multistate_int(val, multistate_tcpfwd); Din_Go(807,2048); return ReplaceReturn;}
	case sExposeAuthenticationMethods:
		{const char * ReplaceReturn = fmt_multistate_int(val, multistate_exposeauthmeth); Din_Go(808,2048); return ReplaceReturn;}
	case sProtocol:
		Din_Go(809,2048);switch (val) {
		case SSH_PROTO_1:
			{const char * ReplaceReturn = "1"; Din_Go(810,2048); return ReplaceReturn;}
		case SSH_PROTO_2:
			{const char * ReplaceReturn = "2"; Din_Go(811,2048); return ReplaceReturn;}
		case (SSH_PROTO_1|SSH_PROTO_2):
			{const char * ReplaceReturn = "2,1"; Din_Go(812,2048); return ReplaceReturn;}
		default:
			{const char * ReplaceReturn = "UNKNOWN"; Din_Go(813,2048); return ReplaceReturn;}
		}
	default:
		Din_Go(814,2048);switch (val) {
		case 0:
			{const char * ReplaceReturn = "no"; Din_Go(815,2048); return ReplaceReturn;}
		case 1:
			{const char * ReplaceReturn = "yes"; Din_Go(816,2048); return ReplaceReturn;}
		default:
			{const char * ReplaceReturn = "UNKNOWN"; Din_Go(817,2048); return ReplaceReturn;}
		}
	}
Din_Go(819,2048);}

static const char *
lookup_opcode_name(ServerOpCodes code)
{
	Din_Go(820,2048);u_int i;

	Din_Go(823,2048);for (i = 0; keywords[i].name != NULL; i++)
		{/*507*/Din_Go(821,2048);if (keywords[i].opcode == code)
			{/*509*/Din_Go(822,2048);return(keywords[i].name);/*510*/}/*508*/}
	{const char * ReplaceReturn = "UNKNOWN"; Din_Go(824,2048); return ReplaceReturn;}
}

static void
dump_cfg_int(ServerOpCodes code, int val)
{
	Din_Go(825,2048);printf("%s %d\n", lookup_opcode_name(code), val);
Din_Go(826,2048);}

static void
dump_cfg_fmtint(ServerOpCodes code, int val)
{
	Din_Go(827,2048);printf("%s %s\n", lookup_opcode_name(code), fmt_intarg(code, val));
Din_Go(828,2048);}

static void
dump_cfg_string(ServerOpCodes code, const char *val)
{
	Din_Go(829,2048);if (val == NULL)
		{/*511*/Din_Go(830,2048);return;/*512*/}
	Din_Go(831,2048);printf("%s %s\n", lookup_opcode_name(code), val);
Din_Go(832,2048);}

static void
dump_cfg_strarray(ServerOpCodes code, u_int count, char **vals)
{
	Din_Go(833,2048);u_int i;

	Din_Go(835,2048);for (i = 0; i < count; i++)
		{/*513*/Din_Go(834,2048);printf("%s %s\n", lookup_opcode_name(code), vals[i]);/*514*/}
Din_Go(836,2048);}

static void
dump_cfg_strarray_oneline(ServerOpCodes code, u_int count, char **vals)
{
	Din_Go(837,2048);u_int i;

	Din_Go(839,2048);if (count <= 0 && code != sAuthenticationMethods)
		{/*515*/Din_Go(838,2048);return;/*516*/}
	Din_Go(840,2048);printf("%s", lookup_opcode_name(code));
	Din_Go(842,2048);for (i = 0; i < count; i++)
		{/*517*/Din_Go(841,2048);printf(" %s",  vals[i]);/*518*/}
	Din_Go(844,2048);if (code == sAuthenticationMethods && count == 0)
		{/*519*/Din_Go(843,2048);printf(" any");/*520*/}
	Din_Go(845,2048);printf("\n");
Din_Go(846,2048);}

void
dump_config(ServerOptions *o)
{
	Din_Go(847,2048);u_int i;
	int ret;
	struct addrinfo *ai;
	char addr[NI_MAXHOST], port[NI_MAXSERV], *s = NULL;

	/* these are usually at the top of the config */
	Din_Go(849,2048);for (i = 0; i < o->num_ports; i++)
		{/*443*/Din_Go(848,2048);printf("port %d\n", o->ports[i]);/*444*/}
	Din_Go(850,2048);dump_cfg_fmtint(sProtocol, o->protocol);
	dump_cfg_fmtint(sAddressFamily, o->address_family);

	/* ListenAddress must be after Port */
	Din_Go(856,2048);for (ai = o->listen_addrs; ai; ai = ai->ai_next) {
		Din_Go(851,2048);if ((ret = getnameinfo(ai->ai_addr, ai->ai_addrlen, addr,
		    sizeof(addr), port, sizeof(port),
		    NI_NUMERICHOST|NI_NUMERICSERV)) != 0) {
			Din_Go(852,2048);error("getnameinfo failed: %.100s",
			    (ret != EAI_SYSTEM) ? gai_strerror(ret) :
			    strerror(errno));
		} else {
			Din_Go(853,2048);if (ai->ai_family == AF_INET6)
				{/*445*/Din_Go(854,2048);printf("listenaddress [%s]:%s\n", addr, port);/*446*/}
			else
				{/*447*/Din_Go(855,2048);printf("listenaddress %s:%s\n", addr, port);/*448*/}
		}
	}

	/* integer arguments */
#ifdef USE_PAM
	Din_Go(857,2048);dump_cfg_fmtint(sUsePAM, o->use_pam);
#endif
	dump_cfg_int(sServerKeyBits, o->server_key_bits);
	dump_cfg_int(sLoginGraceTime, o->login_grace_time);
	dump_cfg_int(sKeyRegenerationTime, o->key_regeneration_time);
	dump_cfg_int(sX11DisplayOffset, o->x11_display_offset);
	dump_cfg_int(sX11MaxDisplays, o->x11_max_displays);
	dump_cfg_int(sMaxAuthTries, o->max_authtries);
	dump_cfg_int(sMaxSessions, o->max_sessions);
	dump_cfg_int(sClientAliveInterval, o->client_alive_interval);
	dump_cfg_int(sClientAliveCountMax, o->client_alive_count_max);

	/* formatted integer arguments */
	dump_cfg_fmtint(sPermitRootLogin, o->permit_root_login);
	dump_cfg_fmtint(sIgnoreRhosts, o->ignore_rhosts);
	dump_cfg_fmtint(sIgnoreUserKnownHosts, o->ignore_user_known_hosts);
	dump_cfg_fmtint(sRhostsRSAAuthentication, o->rhosts_rsa_authentication);
	dump_cfg_fmtint(sHostbasedAuthentication, o->hostbased_authentication);
	dump_cfg_fmtint(sHostbasedUsesNameFromPacketOnly,
	    o->hostbased_uses_name_from_packet_only);
	dump_cfg_fmtint(sRSAAuthentication, o->rsa_authentication);
	dump_cfg_fmtint(sPubkeyAuthentication, o->pubkey_authentication);
#ifdef KRB5
	dump_cfg_fmtint(sKerberosAuthentication, o->kerberos_authentication);
	dump_cfg_fmtint(sKerberosOrLocalPasswd, o->kerberos_or_local_passwd);
	dump_cfg_fmtint(sKerberosTicketCleanup, o->kerberos_ticket_cleanup);
# ifdef USE_AFS
	dump_cfg_fmtint(sKerberosGetAFSToken, o->kerberos_get_afs_token);
# endif
#endif
#ifdef GSSAPI
	dump_cfg_fmtint(sGssAuthentication, o->gss_authentication);
	dump_cfg_fmtint(sGssCleanupCreds, o->gss_cleanup_creds);
	dump_cfg_fmtint(sGssKeyEx, o->gss_keyex);
	dump_cfg_fmtint(sGssStrictAcceptor, o->gss_strict_acceptor);
	dump_cfg_fmtint(sGssStoreRekey, o->gss_store_rekey);
	dump_cfg_string(sGssKexAlgorithms, o->gss_kex_algorithms);
#endif
	dump_cfg_fmtint(sPasswordAuthentication, o->password_authentication);
	dump_cfg_fmtint(sKbdInteractiveAuthentication,
	    o->kbd_interactive_authentication);
	dump_cfg_fmtint(sChallengeResponseAuthentication,
	    o->challenge_response_authentication);
	dump_cfg_fmtint(sPrintMotd, o->print_motd);
	dump_cfg_fmtint(sPrintLastLog, o->print_lastlog);
	dump_cfg_fmtint(sX11Forwarding, o->x11_forwarding);
	dump_cfg_fmtint(sX11UseLocalhost, o->x11_use_localhost);
	dump_cfg_fmtint(sPermitTTY, o->permit_tty);
	dump_cfg_fmtint(sStrictModes, o->strict_modes);
	dump_cfg_fmtint(sTCPKeepAlive, o->tcp_keep_alive);
	dump_cfg_fmtint(sEmptyPasswd, o->permit_empty_passwd);
	dump_cfg_fmtint(sPermitUserEnvironment, o->permit_user_env);
	dump_cfg_fmtint(sUseLogin, o->use_login);
	dump_cfg_fmtint(sCompression, o->compression);
	dump_cfg_fmtint(sGatewayPorts, o->gateway_ports);
	dump_cfg_fmtint(sShowPatchLevel, o->show_patchlevel);
	dump_cfg_fmtint(sUseDNS, o->use_dns);
	dump_cfg_fmtint(sAllowTcpForwarding, o->allow_tcp_forwarding);
	dump_cfg_fmtint(sAllowAgentForwarding, o->allow_agent_forwarding);
	dump_cfg_fmtint(sUsePrivilegeSeparation, use_privsep);
	dump_cfg_fmtint(sKerberosUseKuserok, o->use_kuserok);
	dump_cfg_fmtint(sGssEnablek5users, o->enable_k5users);

	/* string arguments */
	dump_cfg_string(sPidFile, o->pid_file);
	dump_cfg_string(sXAuthLocation, o->xauth_location);
	dump_cfg_string(sCiphers, o->ciphers ? o->ciphers :
	    cipher_alg_list(',', 0));
	dump_cfg_string(sMacs, o->macs ? o->macs : mac_alg_list(','));
	dump_cfg_string(sBanner, o->banner == NULL ? "none" : o->banner);
	dump_cfg_string(sForceCommand, o->adm_forced_command);
	dump_cfg_string(sChrootDirectory, o->chroot_directory);
	dump_cfg_string(sTrustedUserCAKeys, o->trusted_user_ca_keys);
	dump_cfg_string(sRevokedKeys, o->revoked_keys_file);
	dump_cfg_string(sAuthorizedPrincipalsFile,
	    o->authorized_principals_file);
	dump_cfg_string(sVersionAddendum, *o->version_addendum == '\0' ?
	    "none" : o->version_addendum);
	dump_cfg_string(sAuthorizedKeysCommand, o->authorized_keys_command);
	dump_cfg_string(sAuthorizedKeysCommandUser, o->authorized_keys_command_user);
	dump_cfg_string(sHostKeyAgent, o->host_key_agent);
	dump_cfg_string(sKexAlgorithms, o->kex_algorithms ? o->kex_algorithms :
	    kex_alg_list(','));

	/* string arguments requiring a lookup */
	dump_cfg_string(sLogLevel, log_level_name(o->log_level));
	dump_cfg_string(sLogFacility, log_facility_name(o->log_facility));

	/* string array arguments */
	dump_cfg_strarray_oneline(sAuthorizedKeysFile, o->num_authkeys_files,
	    o->authorized_keys_files);
	dump_cfg_strarray(sHostKeyFile, o->num_host_key_files,
	     o->host_key_files);
	dump_cfg_strarray(sHostCertificate, o->num_host_cert_files,
	     o->host_cert_files);
	dump_cfg_strarray(sAllowUsers, o->num_allow_users, o->allow_users);
	dump_cfg_strarray(sDenyUsers, o->num_deny_users, o->deny_users);
	dump_cfg_strarray(sAllowGroups, o->num_allow_groups, o->allow_groups);
	dump_cfg_strarray(sDenyGroups, o->num_deny_groups, o->deny_groups);
	dump_cfg_strarray(sAcceptEnv, o->num_accept_env, o->accept_env);
	dump_cfg_strarray_oneline(sAuthenticationMethods,
	    o->num_auth_methods, o->auth_methods);
	dump_cfg_fmtint(sExposeAuthenticationMethods, o->expose_auth_methods);

	/* other arguments */
	Din_Go(859,2048);for (i = 0; i < o->num_subsystems; i++)
		{/*449*/Din_Go(858,2048);printf("subsystem %s %s\n", o->subsystem_name[i],
		    o->subsystem_args[i]);/*450*/}

	Din_Go(860,2048);printf("maxstartups %d:%d:%d\n", o->max_startups_begin,
	    o->max_startups_rate, o->max_startups);

	Din_Go(864,2048);for (i = 0; tunmode_desc[i].val != -1; i++)
		{/*451*/Din_Go(861,2048);if (tunmode_desc[i].val == o->permit_tun) {
			Din_Go(862,2048);s = tunmode_desc[i].text;
			Din_Go(863,2048);break;
		;/*452*/}}
	Din_Go(865,2048);dump_cfg_string(sPermitTunnel, s);

	printf("ipqos %s ", iptos2str(o->ip_qos_interactive));
	printf("%s\n", iptos2str(o->ip_qos_bulk));

	printf("rekeylimit %lld %d\n", (long long)o->rekey_limit,
	    o->rekey_interval);

	channel_print_adm_permitted_opens();
Din_Go(866,2048);}
