/* $OpenBSD: sshconnect2.c,v 1.204 2014/02/02 03:44:32 djm Exp $ */
/*
 * Copyright (c) 2000 Markus Friedl.  All rights reserved.
 * Copyright (c) 2008 Damien Miller.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "includes.h"
#include "var/tmp/sensor.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/stat.h>

#include <errno.h>
#include <fcntl.h>
#include <langinfo.h>
#include <locale.h>
#include <netdb.h>
#include <pwd.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#if defined(HAVE_STRNVIS) && defined(HAVE_VIS_H) && !defined(BROKEN_STRNVIS)
#include <vis.h>
#endif

#include <openssl/fips.h>

#include "openbsd-compat/sys-queue.h"

#include "xmalloc.h"
#include "ssh.h"
#include "ssh2.h"
#include "buffer.h"
#include "packet.h"
#include "compat.h"
#include "cipher.h"
#include "key.h"
#include "kex.h"
#include "myproposal.h"
#include "sshconnect.h"
#include "authfile.h"
#include "dh.h"
#include "authfd.h"
#include "log.h"
#include "readconf.h"
#include "misc.h"
#include "match.h"
#include "dispatch.h"
#include "canohost.h"
#include "msg.h"
#include "pathnames.h"
#include "uidswap.h"
#include "hostfile.h"

#ifdef GSSAPI
#include "ssh-gss.h"
#endif

/* import */
extern char *client_version_string;
extern char *server_version_string;
extern Options options;

/*
 * SSH2 key exchange
 */

u_char *session_id2 = NULL;
u_int session_id2_len = 0;

char *xxx_host;
struct sockaddr *xxx_hostaddr;

Kex *xxx_kex = NULL;

static int
verify_host_key_callback(Key *hostkey)
{
	Din_Go(867,2048);if (verify_host_key(xxx_host, xxx_hostaddr, hostkey) == -1)
		{/*23*/Din_Go(868,2048);fatal("Host key verification failed.");/*24*/}
	{int  ReplaceReturn = 0; Din_Go(869,2048); return ReplaceReturn;}
}

static char *
order_hostkeyalgs(char *host, struct sockaddr *hostaddr, u_short port)
{
	Din_Go(870,2048);char *oavail, *avail, *first, *last, *alg, *hostname, *ret;
	size_t maxlen;
	struct hostkeys *hostkeys;
	int ktype;
	u_int i;

	/* Find all hostkeys for this hostname */
	get_hostfile_hostname_ipaddr(host, hostaddr, port, &hostname, NULL);
	hostkeys = init_hostkeys();
	Din_Go(872,2048);for (i = 0; i < options.num_user_hostfiles; i++)
		{/*25*/Din_Go(871,2048);load_hostkeys(hostkeys, hostname, options.user_hostfiles[i]);/*26*/}
	Din_Go(874,2048);for (i = 0; i < options.num_system_hostfiles; i++)
		{/*27*/Din_Go(873,2048);load_hostkeys(hostkeys, hostname, options.system_hostfiles[i]);/*28*/}

	Din_Go(875,2048);oavail = avail = xstrdup(KEX_DEFAULT_PK_ALG);
	maxlen = strlen(avail) + 1;
	first = xmalloc(maxlen);
	last = xmalloc(maxlen);
	*first = *last = '\0';

#define ALG_APPEND(to, from) \
	do { \
		if (*to != '\0') \
			strlcat(to, ",", maxlen); \
		strlcat(to, from, maxlen); \
	} while (0)

	Din_Go(878,2048);while ((alg = strsep(&avail, ",")) && *alg != '\0') {
		Din_Go(876,2048);if ((ktype = key_type_from_name(alg)) == KEY_UNSPEC)
			{/*29*/Din_Go(877,2048);fatal("%s: unknown alg %s", __func__, alg);/*30*/}
		if (lookup_key_in_hostkeys_by_type(hostkeys,
		    key_type_plain(ktype), NULL))
			ALG_APPEND(first, alg);
		else
			ALG_APPEND(last, alg);
	}
#undef ALG_APPEND
	Din_Go(879,2048);xasprintf(&ret, "%s%s%s", first, *first == '\0' ? "" : ",", last);
	Din_Go(881,2048);if (*first != '\0')
		{/*39*/Din_Go(880,2048);debug3("%s: prefer hostkeyalgs: %s", __func__, first);/*40*/}

	Din_Go(882,2048);free(first);
	free(last);
	free(hostname);
	free(oavail);
	free_hostkeys(hostkeys);

	{char * ReplaceReturn = ret; Din_Go(883,2048); return ReplaceReturn;}
}

void
ssh_kex2(char *host, struct sockaddr *hostaddr, u_short port)
{
	Din_Go(884,2048);Kex *kex;

#ifdef GSSAPI
	char *orig = NULL, *gss = NULL;
	char *gss_host = NULL;
#endif

	xxx_host = host;
	xxx_hostaddr = hostaddr;

#ifdef GSSAPI
	Din_Go(894,2048);if (options.gss_keyex) {
		Din_Go(885,2048);if (FIPS_mode()) {
			Din_Go(886,2048);logit("Disabling GSSAPIKeyExchange. Not usable in FIPS mode");
			options.gss_keyex = 0;
		} else {
			/* Add the GSSAPI mechanisms currently supported on this 
			 * client to the key exchange algorithm proposal */
			Din_Go(887,2048);orig = myproposal[PROPOSAL_KEX_ALGS];

			Din_Go(890,2048);if (options.gss_trust_dns)
				{/*1*/Din_Go(888,2048);gss_host = (char *)get_canonical_hostname(1);/*2*/}
			else
				{/*3*/Din_Go(889,2048);gss_host = host;/*4*/}

			Din_Go(891,2048);gss = ssh_gssapi_client_mechanisms(gss_host,
			    options.gss_client_identity, options.gss_kex_algorithms);
			Din_Go(893,2048);if (gss) {
				Din_Go(892,2048);debug("Offering GSSAPI proposal: %s", gss);
				xasprintf(&myproposal[PROPOSAL_KEX_ALGS],
				    "%s,%s", gss, orig);
			}
		}
	}
#endif

	Din_Go(896,2048);if (options.ciphers == (char *)-1) {
		Din_Go(895,2048);logit("No valid ciphers for protocol version 2 given, using defaults.");
		options.ciphers = NULL;
	}
	Din_Go(900,2048);if (options.ciphers != NULL) {
		Din_Go(897,2048);myproposal[PROPOSAL_ENC_ALGS_CTOS] =
		myproposal[PROPOSAL_ENC_ALGS_STOC] = options.ciphers;
	} else {/*5*/Din_Go(898,2048);if (FIPS_mode()) {
		Din_Go(899,2048);myproposal[PROPOSAL_ENC_ALGS_CTOS] =
		myproposal[PROPOSAL_ENC_ALGS_STOC] = KEX_FIPS_ENCRYPT;

	;/*6*/}}
	Din_Go(901,2048);myproposal[PROPOSAL_ENC_ALGS_CTOS] =
	    compat_cipher_proposal(myproposal[PROPOSAL_ENC_ALGS_CTOS]);
	myproposal[PROPOSAL_ENC_ALGS_STOC] =
	    compat_cipher_proposal(myproposal[PROPOSAL_ENC_ALGS_STOC]);
	Din_Go(904,2048);if (options.compression) {
		Din_Go(902,2048);myproposal[PROPOSAL_COMP_ALGS_CTOS] =
		myproposal[PROPOSAL_COMP_ALGS_STOC] = "zlib@openssh.com,zlib,none";
	} else {
		Din_Go(903,2048);myproposal[PROPOSAL_COMP_ALGS_CTOS] =
		myproposal[PROPOSAL_COMP_ALGS_STOC] = "none,zlib@openssh.com,zlib";
	}
	Din_Go(908,2048);if (options.macs != NULL) {
		Din_Go(905,2048);myproposal[PROPOSAL_MAC_ALGS_CTOS] =
		myproposal[PROPOSAL_MAC_ALGS_STOC] = options.macs;
	} else {/*7*/Din_Go(906,2048);if (FIPS_mode()) {
		Din_Go(907,2048);myproposal[PROPOSAL_MAC_ALGS_CTOS] =
		myproposal[PROPOSAL_MAC_ALGS_STOC] = KEX_FIPS_MAC;
	;/*8*/}}

	Din_Go(911,2048);if (options.hostkeyalgorithms != NULL)
		{/*9*/Din_Go(909,2048);myproposal[PROPOSAL_SERVER_HOST_KEY_ALGS] =
		    compat_pkalg_proposal(options.hostkeyalgorithms);/*10*/}
	else {
		/* Prefer algorithms that we already have keys for */
		Din_Go(910,2048);myproposal[PROPOSAL_SERVER_HOST_KEY_ALGS] =
		    compat_pkalg_proposal(
		    order_hostkeyalgs(host, hostaddr, port));
	}
	Din_Go(913,2048);if (options.kex_algorithms != NULL)
		{/*11*/Din_Go(912,2048);myproposal[PROPOSAL_KEX_ALGS] = options.kex_algorithms;/*12*/}
	else if (FIPS_mode())
		myproposal[PROPOSAL_KEX_ALGS] = KEX_DEFAULT_KEX_FIPS;

	Din_Go(914,2048);myproposal[PROPOSAL_KEX_ALGS] = compat_kex_proposal(
	    myproposal[PROPOSAL_KEX_ALGS]);
#ifdef GSSAPI
	/* If we've got GSSAPI algorithms, then we also support the
	 * 'null' hostkey, as a last resort */
	Din_Go(916,2048);if (options.gss_keyex && gss) {
		Din_Go(915,2048);orig = myproposal[PROPOSAL_SERVER_HOST_KEY_ALGS];
		xasprintf(&myproposal[PROPOSAL_SERVER_HOST_KEY_ALGS], 
		    "%s,null", orig);
		free(gss);
	}
#endif

	Din_Go(918,2048);if (options.rekey_limit || options.rekey_interval)
		{/*13*/Din_Go(917,2048);packet_set_rekey_limits((u_int32_t)options.rekey_limit,
		    (time_t)options.rekey_interval);/*14*/}

	/* start key exchange */
	Din_Go(919,2048);kex = kex_setup(myproposal);
	kex->kex[KEX_DH_GRP1_SHA1] = kexdh_client;
	kex->kex[KEX_DH_GRP14_SHA1] = kexdh_client;
	kex->kex[KEX_DH_GEX_SHA1] = kexgex_client;
	kex->kex[KEX_DH_GEX_SHA256] = kexgex_client;
	kex->kex[KEX_ECDH_SHA2] = kexecdh_client;
	kex->kex[KEX_C25519_SHA256] = kexc25519_client;
#ifdef GSSAPI
	Din_Go(921,2048);if (options.gss_keyex) {
		Din_Go(920,2048);kex->kex[KEX_GSS_GRP1_SHA1] = kexgss_client;
		kex->kex[KEX_GSS_GRP14_SHA1] = kexgss_client;
		kex->kex[KEX_GSS_GEX_SHA1] = kexgss_client;
	}
#endif
	Din_Go(922,2048);kex->client_version_string=client_version_string;
	kex->server_version_string=server_version_string;
	kex->verify_host_key=&verify_host_key_callback;

#ifdef GSSAPI
	Din_Go(927,2048);if (options.gss_keyex) {
		Din_Go(923,2048);kex->gss_deleg_creds = options.gss_deleg_creds;
		kex->gss_trust_dns = options.gss_trust_dns;
		kex->gss_client = options.gss_client_identity;
		Din_Go(926,2048);if (options.gss_server_identity) {
			Din_Go(924,2048);kex->gss_host = options.gss_server_identity;
		} else {
			Din_Go(925,2048);kex->gss_host = gss_host;
        }
	}
#endif

	Din_Go(928,2048);xxx_kex = kex;

	dispatch_run(DISPATCH_BLOCK, &kex->done, kex);

	Din_Go(930,2048);if (options.use_roaming && !kex->roaming) {
		Din_Go(929,2048);debug("Roaming not allowed by server");
		options.use_roaming = 0;
	}

	Din_Go(931,2048);session_id2 = kex->session_id;
	session_id2_len = kex->session_id_len;

#ifdef DEBUG_KEXDH
	/* send 1st encrypted/maced/compressed message */
	packet_start(SSH2_MSG_IGNORE);
	packet_put_cstring("markus");
	packet_send();
	packet_write_wait();
#endif
Din_Go(932,2048);}

/*
 * Authenticate user
 */

typedef struct Authctxt Authctxt;
typedef struct Authmethod Authmethod;
typedef struct identity Identity;
typedef struct idlist Idlist;

struct identity {
	TAILQ_ENTRY(identity) next;
	AuthenticationConnection *ac;	/* set if agent supports key */
	Key	*key;			/* public/private key */
	char	*filename;		/* comment for agent-only keys */
	int	tried;
	int	isprivate;		/* key points to the private key */
	int	userprovided;
};
TAILQ_HEAD(idlist, identity);

struct Authctxt {
	const char *server_user;
	const char *local_user;
	const char *host;
	const char *service;
	Authmethod *method;
	sig_atomic_t success;
	char *authlist;
	/* pubkey */
	Idlist keys;
	AuthenticationConnection *agent;
	/* hostbased */
	Sensitive *sensitive;
	/* kbd-interactive */
	int info_req_seen;
	/* generic */
	void *methoddata;
};
struct Authmethod {
	char	*name;		/* string to compare against server's list */
	int	(*userauth)(Authctxt *authctxt);
	void	(*cleanup)(Authctxt *authctxt);
	int	*enabled;	/* flag in option struct that enables method */
	int	*batch_flag;	/* flag in option struct that disables method */
};

void	input_userauth_success(int, u_int32_t, void *);
void	input_userauth_success_unexpected(int, u_int32_t, void *);
void	input_userauth_failure(int, u_int32_t, void *);
void	input_userauth_banner(int, u_int32_t, void *);
void	input_userauth_error(int, u_int32_t, void *);
void	input_userauth_info_req(int, u_int32_t, void *);
void	input_userauth_pk_ok(int, u_int32_t, void *);
void	input_userauth_passwd_changereq(int, u_int32_t, void *);

int	userauth_none(Authctxt *);
int	userauth_pubkey(Authctxt *);
int	userauth_passwd(Authctxt *);
int	userauth_kbdint(Authctxt *);
int	userauth_hostbased(Authctxt *);

#ifdef GSSAPI
int	userauth_gssapi(Authctxt *authctxt);
void	input_gssapi_response(int type, u_int32_t, void *);
void	input_gssapi_token(int type, u_int32_t, void *);
void	input_gssapi_hash(int type, u_int32_t, void *);
void	input_gssapi_error(int, u_int32_t, void *);
void	input_gssapi_errtok(int, u_int32_t, void *);
int	userauth_gsskeyex(Authctxt *authctxt);
#endif

void	userauth(Authctxt *, char *);

static int sign_and_send_pubkey(Authctxt *, Identity *);
static void pubkey_prepare(Authctxt *);
static void pubkey_cleanup(Authctxt *);
static Key *load_identity_file(char *, int);

static Authmethod *authmethod_get(char *authlist);
static Authmethod *authmethod_lookup(const char *name);
static char *authmethods_get(void);

Authmethod authmethods[] = {
#ifdef GSSAPI
	{"gssapi-keyex",
		userauth_gsskeyex,
		NULL,
		&options.gss_authentication,
		NULL},
	{"gssapi-with-mic",
		userauth_gssapi,
		NULL,
		&options.gss_authentication,
		NULL},
#endif
	{"hostbased",
		userauth_hostbased,
		NULL,
		&options.hostbased_authentication,
		NULL},
	{"publickey",
		userauth_pubkey,
		NULL,
		&options.pubkey_authentication,
		NULL},
	{"keyboard-interactive",
		userauth_kbdint,
		NULL,
		&options.kbd_interactive_authentication,
		&options.batch_mode},
	{"password",
		userauth_passwd,
		NULL,
		&options.password_authentication,
		&options.batch_mode},
	{"none",
		userauth_none,
		NULL,
		NULL,
		NULL},
	{NULL, NULL, NULL, NULL, NULL}
};

void
ssh_userauth2(const char *local_user, const char *server_user, char *host,
    Sensitive *sensitive)
{
	Din_Go(933,2048);Authctxt authctxt;
	int type;

	Din_Go(935,2048);if (options.challenge_response_authentication)
		{/*15*/Din_Go(934,2048);options.kbd_interactive_authentication = 1;/*16*/}

	Din_Go(936,2048);packet_start(SSH2_MSG_SERVICE_REQUEST);
	packet_put_cstring("ssh-userauth");
	packet_send();
	debug("SSH2_MSG_SERVICE_REQUEST sent");
	packet_write_wait();
	type = packet_read();
	Din_Go(938,2048);if (type != SSH2_MSG_SERVICE_ACCEPT)
		{/*17*/Din_Go(937,2048);fatal("Server denied authentication request: %d", type);/*18*/}
	Din_Go(941,2048);if (packet_remaining() > 0) {
		Din_Go(939,2048);char *reply = packet_get_string(NULL);
		debug2("service_accept: %s", reply);
		free(reply);
	} else {
		Din_Go(940,2048);debug2("buggy server: service_accept w/o service");
	}
	packet_check_eom();
	Din_Go(942,2048);debug("SSH2_MSG_SERVICE_ACCEPT received");

	Din_Go(944,2048);if (options.preferred_authentications == NULL)
		{/*19*/Din_Go(943,2048);options.preferred_authentications = authmethods_get();/*20*/}

	/* setup authentication context */
	Din_Go(945,2048);memset(&authctxt, 0, sizeof(authctxt));
	pubkey_prepare(&authctxt);
	authctxt.server_user = server_user;
	authctxt.local_user = local_user;
	authctxt.host = host;
	authctxt.service = "ssh-connection";		/* service name */
	authctxt.success = 0;
	authctxt.method = authmethod_lookup("none");
	authctxt.authlist = NULL;
	authctxt.methoddata = NULL;
	authctxt.sensitive = sensitive;
	authctxt.info_req_seen = 0;
	Din_Go(947,2048);if (authctxt.method == NULL)
		{/*21*/Din_Go(946,2048);fatal("ssh_userauth2: internal error: cannot send userauth none request");/*22*/}

	/* initial userauth request */
	Din_Go(948,2048);userauth_none(&authctxt);

	dispatch_init(&input_userauth_error);
	dispatch_set(SSH2_MSG_USERAUTH_SUCCESS, &input_userauth_success);
	dispatch_set(SSH2_MSG_USERAUTH_FAILURE, &input_userauth_failure);
	dispatch_set(SSH2_MSG_USERAUTH_BANNER, &input_userauth_banner);
	dispatch_run(DISPATCH_BLOCK, &authctxt.success, &authctxt);	/* loop until success */

	pubkey_cleanup(&authctxt);
	dispatch_range(SSH2_MSG_USERAUTH_MIN, SSH2_MSG_USERAUTH_MAX, NULL);

	debug("Authentication succeeded (%s).", authctxt.method->name);
Din_Go(949,2048);}

void
userauth(Authctxt *authctxt, char *authlist)
{
	Din_Go(950,2048);if (authctxt->method != NULL && authctxt->method->cleanup != NULL)
		{/*109*/Din_Go(951,2048);authctxt->method->cleanup(authctxt);/*110*/}

	Din_Go(952,2048);free(authctxt->methoddata);
	authctxt->methoddata = NULL;
	Din_Go(955,2048);if (authlist == NULL) {
		Din_Go(953,2048);authlist = authctxt->authlist;
	} else {
		Din_Go(954,2048);free(authctxt->authlist);
		authctxt->authlist = authlist;
	}
	Din_Go(964,2048);for (;;) {
		Din_Go(956,2048);Authmethod *method = authmethod_get(authlist);
		Din_Go(958,2048);if (method == NULL)
			{/*111*/Din_Go(957,2048);fatal("Permission denied (%s).", authlist);/*112*/}
		Din_Go(959,2048);authctxt->method = method;

		/* reset the per method handler */
		dispatch_range(SSH2_MSG_USERAUTH_PER_METHOD_MIN,
		    SSH2_MSG_USERAUTH_PER_METHOD_MAX, NULL);

		/* and try new method */
		Din_Go(963,2048);if (method->userauth(authctxt) != 0) {
			Din_Go(960,2048);debug2("we sent a %s packet, wait for reply", method->name);
			Din_Go(961,2048);break;
		} else {
			Din_Go(962,2048);debug2("we did not send a packet, disable method");
			method->enabled = NULL;
		}
	}
Din_Go(965,2048);}

/* ARGSUSED */
void
input_userauth_error(int type, u_int32_t seq, void *ctxt)
{
	Din_Go(966,2048);fatal("input_userauth_error: bad message during authentication: "
	    "type %d", type);
Din_Go(967,2048);}

/* Check whether we can display UTF-8 safely */
static int
utf8_ok(void)
{
	Din_Go(968,2048);static int ret = -1;
	char *cp;

	Din_Go(970,2048);if (ret == -1) {
		Din_Go(969,2048);setlocale(LC_CTYPE, "");
		cp = nl_langinfo(CODESET);
		ret = strcmp(cp, "UTF-8") == 0;
	}
	{int  ReplaceReturn = ret; Din_Go(971,2048); return ReplaceReturn;}
}

/* ARGSUSED */
void
input_userauth_banner(int type, u_int32_t seq, void *ctxt)
{
	Din_Go(972,2048);char *msg, *raw, *lang;
	u_int done, len;

	debug3("input_userauth_banner");

	raw = packet_get_string(&len);
	lang = packet_get_string(NULL);
	Din_Go(983,2048);if (len > 0 && options.log_level >= SYSLOG_LEVEL_INFO) {
		Din_Go(973,2048);if (len > 65536)
			{/*49*/Din_Go(974,2048);len = 65536;/*50*/}
		Din_Go(975,2048);msg = xmalloc(len * 4 + 1); /* max expansion from strnvis() */
		done = 0;
		Din_Go(979,2048);if (utf8_ok()) {
			Din_Go(976,2048);if (utf8_stringprep(raw, msg, len * 4 + 1) == 0)
				{/*51*/Din_Go(977,2048);done = 1;/*52*/}
			else
				{/*53*/Din_Go(978,2048);debug2("%s: UTF8 stringprep failed", __func__);/*54*/}
		}
		/*
		 * Fallback to strnvis if UTF8 display not supported or
		 * conversion failed.
		 */
		Din_Go(981,2048);if (!done) {
			Din_Go(980,2048);strnvis(msg, raw, len * 4 + 1,
			    VIS_SAFE|VIS_OCTAL|VIS_NOSLASH);
		}
		Din_Go(982,2048);fprintf(stderr, "%s", msg);
		free(msg);
	}
	Din_Go(984,2048);free(raw);
	free(lang);
Din_Go(985,2048);}

/* ARGSUSED */
void
input_userauth_success(int type, u_int32_t seq, void *ctxt)
{
	Din_Go(986,2048);Authctxt *authctxt = ctxt;

	Din_Go(988,2048);if (authctxt == NULL)
		{/*41*/Din_Go(987,2048);fatal("input_userauth_success: no authentication context");/*42*/}
	Din_Go(989,2048);free(authctxt->authlist);
	authctxt->authlist = NULL;
	Din_Go(991,2048);if (authctxt->method != NULL && authctxt->method->cleanup != NULL)
		{/*43*/Din_Go(990,2048);authctxt->method->cleanup(authctxt);/*44*/}
	Din_Go(992,2048);free(authctxt->methoddata);
	authctxt->methoddata = NULL;
	authctxt->success = 1;			/* break out */
Din_Go(993,2048);}

void
input_userauth_success_unexpected(int type, u_int32_t seq, void *ctxt)
{
	Din_Go(994,2048);Authctxt *authctxt = ctxt;

	Din_Go(996,2048);if (authctxt == NULL)
		{/*45*/Din_Go(995,2048);fatal("%s: no authentication context", __func__);/*46*/}

	Din_Go(997,2048);fatal("Unexpected authentication success during %s.",
	    authctxt->method->name);
Din_Go(998,2048);}

/* ARGSUSED */
void
input_userauth_failure(int type, u_int32_t seq, void *ctxt)
{
	Din_Go(999,2048);Authctxt *authctxt = ctxt;
	char *authlist = NULL;
	int partial;

	Din_Go(1001,2048);if (authctxt == NULL)
		{/*47*/Din_Go(1000,2048);fatal("input_userauth_failure: no authentication context");/*48*/}

	Din_Go(1002,2048);authlist = packet_get_string(NULL);
	partial = packet_get_char();
	packet_check_eom();

	Din_Go(1004,2048);if (partial != 0) {
		Din_Go(1003,2048);logit("Authenticated with partial success.");
		/* reset state */
		pubkey_cleanup(authctxt);
		pubkey_prepare(authctxt);
	}
	Din_Go(1005,2048);debug("Authentications that can continue: %s", authlist);

	userauth(authctxt, authlist);
Din_Go(1006,2048);}

/* ARGSUSED */
void
input_userauth_pk_ok(int type, u_int32_t seq, void *ctxt)
{
	Din_Go(1007,2048);Authctxt *authctxt = ctxt;
	Key *key = NULL;
	Identity *id = NULL;
	Buffer b;
	int pktype, sent = 0;
	u_int alen, blen;
	char *pkalg, *fp;
	u_char *pkblob;

	Din_Go(1009,2048);if (authctxt == NULL)
		{/*61*/Din_Go(1008,2048);fatal("input_userauth_pk_ok: no authentication context");/*62*/}
	Din_Go(1012,2048);if (datafellows & SSH_BUG_PKOK) {
		/* this is similar to SSH_BUG_PKAUTH */
		Din_Go(1010,2048);debug2("input_userauth_pk_ok: SSH_BUG_PKOK");
		pkblob = packet_get_string(&blen);
		buffer_init(&b);
		buffer_append(&b, pkblob, blen);
		pkalg = buffer_get_string(&b, &alen);
		buffer_free(&b);
	} else {
		Din_Go(1011,2048);pkalg = packet_get_string(&alen);
		pkblob = packet_get_string(&blen);
	}
	packet_check_eom();

	Din_Go(1013,2048);debug("Server accepts key: pkalg %s blen %u", pkalg, blen);

	Din_Go(1016,2048);if ((pktype = key_type_from_name(pkalg)) == KEY_UNSPEC) {
		Din_Go(1014,2048);debug("unknown pkalg %s", pkalg);
		Din_Go(1015,2048);goto done;
	}
	Din_Go(1019,2048);if ((key = key_from_blob(pkblob, blen)) == NULL) {
		Din_Go(1017,2048);debug("no key from blob. pkalg %s", pkalg);
		Din_Go(1018,2048);goto done;
	}
	Din_Go(1022,2048);if (key->type != pktype) {
		Din_Go(1020,2048);error("input_userauth_pk_ok: type mismatch "
		    "for decoded key (received %d, expected %d)",
		    key->type, pktype);
		Din_Go(1021,2048);goto done;
	}
	Din_Go(1023,2048);fp = key_selected_fingerprint(key, SSH_FP_HEX);
	debug2("input_userauth_pk_ok: fp %s%s",
	    key_fingerprint_prefix(), fp);
	free(fp);

	/*
	 * search keys in the reverse order, because last candidate has been
	 * moved to the end of the queue.  this also avoids confusion by
	 * duplicate keys
	 */
	TAILQ_FOREACH_REVERSE(id, &authctxt->keys, idlist, next) {
		if (key_equal(key, id->key)) {
			sent = sign_and_send_pubkey(authctxt, id);
			break;
		}
	}
done:
	Din_Go(1025,2048);if (key != NULL)
		{/*63*/Din_Go(1024,2048);key_free(key);/*64*/}
	Din_Go(1026,2048);free(pkalg);
	free(pkblob);

	/* try another method if we did not send a packet */
	Din_Go(1028,2048);if (sent == 0)
		{/*65*/Din_Go(1027,2048);userauth(authctxt, NULL);/*66*/}
Din_Go(1029,2048);}

#ifdef GSSAPI
int
userauth_gssapi(Authctxt *authctxt)
{
	Din_Go(1030,2048);Gssctxt *gssctxt = NULL;
	static gss_OID_set gss_supported = NULL;
	static u_int mech = 0;
	OM_uint32 min;
	int ok = 0;
	const char *gss_host = NULL;

	Din_Go(1037,2048);if (options.gss_server_identity)
		{/*87*/Din_Go(1031,2048);gss_host = options.gss_server_identity;/*88*/}
	else {/*89*/Din_Go(1032,2048);if (options.gss_trust_dns) {
		Din_Go(1033,2048);gss_host = get_canonical_hostname(1);
		Din_Go(1035,2048);if ( strcmp( gss_host, "UNKNOWN" )  == 0 )
			{/*91*/Din_Go(1034,2048);gss_host = authctxt->host;/*92*/}
	}
	else
		{/*93*/Din_Go(1036,2048);gss_host = authctxt->host;/*94*/}/*90*/}

	/* Try one GSSAPI method at a time, rather than sending them all at
	 * once. */

	Din_Go(1041,2048);if (gss_supported == NULL)
		{/*95*/Din_Go(1038,2048);if (GSS_ERROR(gss_indicate_mechs(&min, &gss_supported))) {
			Din_Go(1039,2048);gss_supported = NULL;
			{int  ReplaceReturn = 0; Din_Go(1040,2048); return ReplaceReturn;}
		;/*96*/}}

	/* Check to see if the mechanism is usable before we offer it */
	Din_Go(1045,2048);while (mech < gss_supported->count && !ok) {
		/* My DER encoding requires length<128 */
		Din_Go(1042,2048);if (gss_supported->elements[mech].length < 128 &&
		    ssh_gssapi_check_mechanism(&gssctxt, 
		    &gss_supported->elements[mech], gss_host, 
                    options.gss_client_identity)) {
			Din_Go(1043,2048);ok = 1; /* Mechanism works */
		} else {
			Din_Go(1044,2048);mech++;
		}
	}

	Din_Go(1047,2048);if (!ok)
		{/*97*/{int  ReplaceReturn = 0; Din_Go(1046,2048); return ReplaceReturn;}/*98*/}

	Din_Go(1048,2048);authctxt->methoddata=(void *)gssctxt;

	packet_start(SSH2_MSG_USERAUTH_REQUEST);
	packet_put_cstring(authctxt->server_user);
	packet_put_cstring(authctxt->service);
	packet_put_cstring(authctxt->method->name);

	packet_put_int(1);

	packet_put_int((gss_supported->elements[mech].length) + 2);
	packet_put_char(SSH_GSS_OIDTYPE);
	packet_put_char(gss_supported->elements[mech].length);
	packet_put_raw(gss_supported->elements[mech].elements,
	    gss_supported->elements[mech].length);

	packet_send();

	dispatch_set(SSH2_MSG_USERAUTH_GSSAPI_RESPONSE, &input_gssapi_response);
	dispatch_set(SSH2_MSG_USERAUTH_GSSAPI_TOKEN, &input_gssapi_token);
	dispatch_set(SSH2_MSG_USERAUTH_GSSAPI_ERROR, &input_gssapi_error);
	dispatch_set(SSH2_MSG_USERAUTH_GSSAPI_ERRTOK, &input_gssapi_errtok);

	mech++; /* Move along to next candidate */

	{int  ReplaceReturn = 1; Din_Go(1049,2048); return ReplaceReturn;}
}

static OM_uint32
process_gssapi_token(void *ctxt, gss_buffer_t recv_tok)
{
	Din_Go(1050,2048);Authctxt *authctxt = ctxt;
	Gssctxt *gssctxt = authctxt->methoddata;
	gss_buffer_desc send_tok = GSS_C_EMPTY_BUFFER;
	gss_buffer_desc mic = GSS_C_EMPTY_BUFFER;
	gss_buffer_desc gssbuf;
	OM_uint32 status, ms, flags;
	Buffer b;

	status = ssh_gssapi_init_ctx(gssctxt, options.gss_deleg_creds,
	    recv_tok, &send_tok, &flags);

	Din_Go(1055,2048);if (send_tok.length > 0) {
		Din_Go(1051,2048);if (GSS_ERROR(status))
			{/*147*/Din_Go(1052,2048);packet_start(SSH2_MSG_USERAUTH_GSSAPI_ERRTOK);/*148*/}
		else
			{/*149*/Din_Go(1053,2048);packet_start(SSH2_MSG_USERAUTH_GSSAPI_TOKEN);/*150*/}

		Din_Go(1054,2048);packet_put_string(send_tok.value, send_tok.length);
		packet_send();
		gss_release_buffer(&ms, &send_tok);
	}

	Din_Go(1062,2048);if (status == GSS_S_COMPLETE) {
		/* send either complete or MIC, depending on mechanism */
		Din_Go(1056,2048);if (!(flags & GSS_C_INTEG_FLAG)) {
			Din_Go(1057,2048);packet_start(SSH2_MSG_USERAUTH_GSSAPI_EXCHANGE_COMPLETE);
			packet_send();
		} else {
			Din_Go(1058,2048);ssh_gssapi_buildmic(&b, authctxt->server_user,
			    authctxt->service, "gssapi-with-mic");

			gssbuf.value = buffer_ptr(&b);
			gssbuf.length = buffer_len(&b);

			status = ssh_gssapi_sign(gssctxt, &gssbuf, &mic);

			Din_Go(1060,2048);if (!GSS_ERROR(status)) {
				Din_Go(1059,2048);packet_start(SSH2_MSG_USERAUTH_GSSAPI_MIC);
				packet_put_string(mic.value, mic.length);

				packet_send();
			}

			Din_Go(1061,2048);buffer_free(&b);
			gss_release_buffer(&ms, &mic);
		}
	}

	{OM_uint32  ReplaceReturn = status; Din_Go(1063,2048); return ReplaceReturn;}
}

/* ARGSUSED */
void
input_gssapi_response(int type, u_int32_t plen, void *ctxt)
{
	Din_Go(1064,2048);Authctxt *authctxt = ctxt;
	Gssctxt *gssctxt;
	u_int oidlen;
	u_char *oidv;

	Din_Go(1066,2048);if (authctxt == NULL)
		{/*99*/Din_Go(1065,2048);fatal("input_gssapi_response: no authentication context");/*100*/}
	Din_Go(1067,2048);gssctxt = authctxt->methoddata;

	/* Setup our OID */
	oidv = packet_get_string(&oidlen);

	Din_Go(1070,2048);if (oidlen <= 2 ||
	    oidv[0] != SSH_GSS_OIDTYPE ||
	    oidv[1] != oidlen - 2) {
		Din_Go(1068,2048);free(oidv);
		debug("Badly encoded mechanism OID received");
		userauth(authctxt, NULL);
		Din_Go(1069,2048);return;
	}

	Din_Go(1072,2048);if (!ssh_gssapi_check_oid(gssctxt, oidv + 2, oidlen - 2))
		{/*101*/Din_Go(1071,2048);fatal("Server returned different OID than expected");/*102*/}

	packet_check_eom();

	Din_Go(1073,2048);free(oidv);

	Din_Go(1076,2048);if (GSS_ERROR(process_gssapi_token(ctxt, GSS_C_NO_BUFFER))) {
		/* Start again with next method on list */
		Din_Go(1074,2048);debug("Trying to start again");
		userauth(authctxt, NULL);
		Din_Go(1075,2048);return;
	}
Din_Go(1077,2048);}

/* ARGSUSED */
void
input_gssapi_token(int type, u_int32_t plen, void *ctxt)
{
	Din_Go(1078,2048);Authctxt *authctxt = ctxt;
	gss_buffer_desc recv_tok;
	OM_uint32 status;
	u_int slen;

	Din_Go(1080,2048);if (authctxt == NULL)
		{/*103*/Din_Go(1079,2048);fatal("input_gssapi_response: no authentication context");/*104*/}

	Din_Go(1081,2048);recv_tok.value = packet_get_string(&slen);
	recv_tok.length = slen;	/* safe typecast */

	packet_check_eom();

	status = process_gssapi_token(ctxt, &recv_tok);

	free(recv_tok.value);

	Din_Go(1084,2048);if (GSS_ERROR(status)) {
		/* Start again with the next method in the list */
		Din_Go(1082,2048);userauth(authctxt, NULL);
		Din_Go(1083,2048);return;
	}
Din_Go(1085,2048);}

/* ARGSUSED */
void
input_gssapi_errtok(int type, u_int32_t plen, void *ctxt)
{
	Din_Go(1086,2048);Authctxt *authctxt = ctxt;
	Gssctxt *gssctxt;
	gss_buffer_desc send_tok = GSS_C_EMPTY_BUFFER;
	gss_buffer_desc recv_tok;
	OM_uint32 ms;
	u_int len;

	Din_Go(1088,2048);if (authctxt == NULL)
		{/*105*/Din_Go(1087,2048);fatal("input_gssapi_response: no authentication context");/*106*/}
	Din_Go(1089,2048);gssctxt = authctxt->methoddata;

	recv_tok.value = packet_get_string(&len);
	recv_tok.length = len;

	packet_check_eom();

	/* Stick it into GSSAPI and see what it says */
	(void)ssh_gssapi_init_ctx(gssctxt, options.gss_deleg_creds,
	    &recv_tok, &send_tok, NULL);

	free(recv_tok.value);
	gss_release_buffer(&ms, &send_tok);

	/* Server will be returning a failed packet after this one */
Din_Go(1090,2048);}

/* ARGSUSED */
void
input_gssapi_error(int type, u_int32_t plen, void *ctxt)
{
	Din_Go(1091,2048);char *msg;
	char *lang;

	/* maj */(void)packet_get_int();
	/* min */(void)packet_get_int();
	msg=packet_get_string(NULL);
	lang=packet_get_string(NULL);

	packet_check_eom();

	debug("Server GSSAPI Error:\n%s", msg);
	free(msg);
	free(lang);
Din_Go(1092,2048);}

int
userauth_gsskeyex(Authctxt *authctxt)
{
	Din_Go(1093,2048);Buffer b;
	gss_buffer_desc gssbuf;
	gss_buffer_desc mic = GSS_C_EMPTY_BUFFER;
	OM_uint32 ms;

	static int attempt = 0;
	Din_Go(1095,2048);if (attempt++ >= 1)
		{/*107*/{int  ReplaceReturn = (0); Din_Go(1094,2048); return ReplaceReturn;}/*108*/}

	Din_Go(1098,2048);if (gss_kex_context == NULL) {
		Din_Go(1096,2048);debug("No valid Key exchange context"); 
		{int  ReplaceReturn = (0); Din_Go(1097,2048); return ReplaceReturn;}
	}

	Din_Go(1099,2048);ssh_gssapi_buildmic(&b, authctxt->server_user, authctxt->service,
	    "gssapi-keyex");

	gssbuf.value = buffer_ptr(&b);
	gssbuf.length = buffer_len(&b);

	Din_Go(1102,2048);if (GSS_ERROR(ssh_gssapi_sign(gss_kex_context, &gssbuf, &mic))) {
		Din_Go(1100,2048);buffer_free(&b);
		{int  ReplaceReturn = (0); Din_Go(1101,2048); return ReplaceReturn;}
	}

	Din_Go(1103,2048);packet_start(SSH2_MSG_USERAUTH_REQUEST);
	packet_put_cstring(authctxt->server_user);
	packet_put_cstring(authctxt->service);
	packet_put_cstring(authctxt->method->name);
	packet_put_string(mic.value, mic.length);
	packet_send();

	buffer_free(&b);
	gss_release_buffer(&ms, &mic);

	{int  ReplaceReturn = (1); Din_Go(1104,2048); return ReplaceReturn;}
}

#endif /* GSSAPI */

int
userauth_none(Authctxt *authctxt)
{
	/* initial userauth request */
	Din_Go(1105,2048);packet_start(SSH2_MSG_USERAUTH_REQUEST);
	packet_put_cstring(authctxt->server_user);
	packet_put_cstring(authctxt->service);
	packet_put_cstring(authctxt->method->name);
	packet_send();
	{int  ReplaceReturn = 1; Din_Go(1106,2048); return ReplaceReturn;}
}

int
userauth_passwd(Authctxt *authctxt)
{
	Din_Go(1107,2048);static int attempt = 0;
	char prompt[150];
	char *password;
	const char *host = options.host_key_alias ?  options.host_key_alias :
	    authctxt->host;

	Din_Go(1109,2048);if (attempt++ >= options.number_of_password_prompts)
		{/*77*/{int  ReplaceReturn = 0; Din_Go(1108,2048); return ReplaceReturn;}/*78*/}

	Din_Go(1111,2048);if (attempt != 1)
		{/*79*/Din_Go(1110,2048);error("Permission denied, please try again.");/*80*/}

	Din_Go(1112,2048);snprintf(prompt, sizeof(prompt), "%.30s@%.128s's password: ",
	    authctxt->server_user, host);
	password = read_passphrase(prompt, 0);
	packet_start(SSH2_MSG_USERAUTH_REQUEST);
	packet_put_cstring(authctxt->server_user);
	packet_put_cstring(authctxt->service);
	packet_put_cstring(authctxt->method->name);
	packet_put_char(0);
	packet_put_cstring(password);
	explicit_bzero(password, strlen(password));
	free(password);
	packet_add_padding(64);
	packet_send();

	dispatch_set(SSH2_MSG_USERAUTH_PASSWD_CHANGEREQ,
	    &input_userauth_passwd_changereq);

	{int  ReplaceReturn = 1; Din_Go(1113,2048); return ReplaceReturn;}
}

/*
 * parse PASSWD_CHANGEREQ, prompt user and send SSH2_MSG_USERAUTH_REQUEST
 */
/* ARGSUSED */
void
input_userauth_passwd_changereq(int type, u_int32_t seqnr, void *ctxt)
{
	Din_Go(1114,2048);Authctxt *authctxt = ctxt;
	char *info, *lang, *password = NULL, *retype = NULL;
	char prompt[150];
	const char *host = options.host_key_alias ? options.host_key_alias :
	    authctxt->host;

	debug2("input_userauth_passwd_changereq");

	Din_Go(1116,2048);if (authctxt == NULL)
		{/*67*/Din_Go(1115,2048);fatal("input_userauth_passwd_changereq: "
		    "no authentication context");/*68*/}

	Din_Go(1117,2048);info = packet_get_string(NULL);
	lang = packet_get_string(NULL);
	Din_Go(1119,2048);if (strlen(info) > 0)
		{/*69*/Din_Go(1118,2048);logit("%s", info);/*70*/}
	Din_Go(1120,2048);free(info);
	free(lang);
	packet_start(SSH2_MSG_USERAUTH_REQUEST);
	packet_put_cstring(authctxt->server_user);
	packet_put_cstring(authctxt->service);
	packet_put_cstring(authctxt->method->name);
	packet_put_char(1);			/* additional info */
	snprintf(prompt, sizeof(prompt),
	    "Enter %.30s@%.128s's old password: ",
	    authctxt->server_user, host);
	password = read_passphrase(prompt, 0);
	packet_put_cstring(password);
	explicit_bzero(password, strlen(password));
	free(password);
	password = NULL;
	Din_Go(1128,2048);while (password == NULL) {
		Din_Go(1121,2048);snprintf(prompt, sizeof(prompt),
		    "Enter %.30s@%.128s's new password: ",
		    authctxt->server_user, host);
		password = read_passphrase(prompt, RP_ALLOW_EOF);
		Din_Go(1123,2048);if (password == NULL) {
			/* bail out */
			Din_Go(1122,2048);return;
		}
		Din_Go(1124,2048);snprintf(prompt, sizeof(prompt),
		    "Retype %.30s@%.128s's new password: ",
		    authctxt->server_user, host);
		retype = read_passphrase(prompt, 0);
		Din_Go(1126,2048);if (strcmp(password, retype) != 0) {
			Din_Go(1125,2048);explicit_bzero(password, strlen(password));
			free(password);
			logit("Mismatch; try again, EOF to quit.");
			password = NULL;
		}
		Din_Go(1127,2048);explicit_bzero(retype, strlen(retype));
		free(retype);
	}
	Din_Go(1129,2048);packet_put_cstring(password);
	explicit_bzero(password, strlen(password));
	free(password);
	packet_add_padding(64);
	packet_send();

	dispatch_set(SSH2_MSG_USERAUTH_PASSWD_CHANGEREQ,
	    &input_userauth_passwd_changereq);
Din_Go(1130,2048);}

static int
identity_sign(Identity *id, u_char **sigp, u_int *lenp,
    u_char *data, u_int datalen)
{
	Din_Go(1131,2048);Key *prv;
	int ret;

	/* the agent supports this key */
	Din_Go(1133,2048);if (id->ac)
		{/*151*/{int  ReplaceReturn = (ssh_agent_sign(id->ac, id->key, sigp, lenp,
		    data, datalen)); Din_Go(1132,2048); return ReplaceReturn;}/*152*/}
	/*
	 * we have already loaded the private key or
	 * the private key is stored in external hardware
	 */
	Din_Go(1135,2048);if (id->isprivate || (id->key->flags & KEY_FLAG_EXT))
		{/*153*/{int  ReplaceReturn = (key_sign(id->key, sigp, lenp, data, datalen)); Din_Go(1134,2048); return ReplaceReturn;}/*154*/}
	/* load the private key from the file */
	Din_Go(1137,2048);if ((prv = load_identity_file(id->filename, id->userprovided)) == NULL)
		{/*155*/{int  ReplaceReturn = (-1); Din_Go(1136,2048); return ReplaceReturn;}/*156*/}
	Din_Go(1138,2048);ret = key_sign(prv, sigp, lenp, data, datalen);
	key_free(prv);
	{int  ReplaceReturn = (ret); Din_Go(1139,2048); return ReplaceReturn;}
}

static int
sign_and_send_pubkey(Authctxt *authctxt, Identity *id)
{
	Din_Go(1140,2048);Buffer b;
	u_char *blob, *signature;
	u_int bloblen, slen;
	u_int skip = 0;
	int ret = -1;
	int have_sig = 1;
	char *fp;

	fp = key_selected_fingerprint(id->key, SSH_FP_HEX);
	debug3("sign_and_send_pubkey: %s %s%s", key_type(id->key),
	    key_fingerprint_prefix(), fp);
	free(fp);

	Din_Go(1143,2048);if (key_to_blob(id->key, &blob, &bloblen) == 0) {
		/* we cannot handle this key */
		Din_Go(1141,2048);debug3("sign_and_send_pubkey: cannot handle key");
		{int  ReplaceReturn = 0; Din_Go(1142,2048); return ReplaceReturn;}
	}
	/* data to be signed */
	Din_Go(1144,2048);buffer_init(&b);
	Din_Go(1147,2048);if (datafellows & SSH_OLD_SESSIONID) {
		Din_Go(1145,2048);buffer_append(&b, session_id2, session_id2_len);
		skip = session_id2_len;
	} else {
		Din_Go(1146,2048);buffer_put_string(&b, session_id2, session_id2_len);
		skip = buffer_len(&b);
	}
	Din_Go(1148,2048);buffer_put_char(&b, SSH2_MSG_USERAUTH_REQUEST);
	buffer_put_cstring(&b, authctxt->server_user);
	buffer_put_cstring(&b,
	    datafellows & SSH_BUG_PKSERVICE ?
	    "ssh-userauth" :
	    authctxt->service);
	Din_Go(1151,2048);if (datafellows & SSH_BUG_PKAUTH) {
		Din_Go(1149,2048);buffer_put_char(&b, have_sig);
	} else {
		Din_Go(1150,2048);buffer_put_cstring(&b, authctxt->method->name);
		buffer_put_char(&b, have_sig);
		buffer_put_cstring(&b, key_ssh_name(id->key));
	}
	Din_Go(1152,2048);buffer_put_string(&b, blob, bloblen);

	/* generate signature */
	ret = identity_sign(id, &signature, &slen,
	    buffer_ptr(&b), buffer_len(&b));
	Din_Go(1155,2048);if (ret == -1) {
		Din_Go(1153,2048);free(blob);
		buffer_free(&b);
		{int  ReplaceReturn = 0; Din_Go(1154,2048); return ReplaceReturn;}
	}
#ifdef DEBUG_PK
	buffer_dump(&b);
#endif
	Din_Go(1160,2048);if (datafellows & SSH_BUG_PKSERVICE) {
		Din_Go(1156,2048);buffer_clear(&b);
		buffer_append(&b, session_id2, session_id2_len);
		skip = session_id2_len;
		buffer_put_char(&b, SSH2_MSG_USERAUTH_REQUEST);
		buffer_put_cstring(&b, authctxt->server_user);
		buffer_put_cstring(&b, authctxt->service);
		buffer_put_cstring(&b, authctxt->method->name);
		buffer_put_char(&b, have_sig);
		Din_Go(1158,2048);if (!(datafellows & SSH_BUG_PKAUTH))
			{/*113*/Din_Go(1157,2048);buffer_put_cstring(&b, key_ssh_name(id->key));/*114*/}
		Din_Go(1159,2048);buffer_put_string(&b, blob, bloblen);
	}
	Din_Go(1161,2048);free(blob);

	/* append signature */
	buffer_put_string(&b, signature, slen);
	free(signature);

	/* skip session id and packet type */
	Din_Go(1163,2048);if (buffer_len(&b) < skip + 1)
		{/*115*/Din_Go(1162,2048);fatal("userauth_pubkey: internal error");/*116*/}
	Din_Go(1164,2048);buffer_consume(&b, skip + 1);

	/* put remaining data from buffer into packet */
	packet_start(SSH2_MSG_USERAUTH_REQUEST);
	packet_put_raw(buffer_ptr(&b), buffer_len(&b));
	buffer_free(&b);
	packet_send();

	{int  ReplaceReturn = 1; Din_Go(1165,2048); return ReplaceReturn;}
}

static int
send_pubkey_test(Authctxt *authctxt, Identity *id)
{
	Din_Go(1166,2048);u_char *blob;
	u_int bloblen, have_sig = 0;

	debug3("send_pubkey_test");

	Din_Go(1169,2048);if (key_to_blob(id->key, &blob, &bloblen) == 0) {
		/* we cannot handle this key */
		Din_Go(1167,2048);debug3("send_pubkey_test: cannot handle key");
		{int  ReplaceReturn = 0; Din_Go(1168,2048); return ReplaceReturn;}
	}
	/* register callback for USERAUTH_PK_OK message */
	Din_Go(1170,2048);dispatch_set(SSH2_MSG_USERAUTH_PK_OK, &input_userauth_pk_ok);

	packet_start(SSH2_MSG_USERAUTH_REQUEST);
	packet_put_cstring(authctxt->server_user);
	packet_put_cstring(authctxt->service);
	packet_put_cstring(authctxt->method->name);
	packet_put_char(have_sig);
	Din_Go(1172,2048);if (!(datafellows & SSH_BUG_PKAUTH))
		{/*157*/Din_Go(1171,2048);packet_put_cstring(key_ssh_name(id->key));/*158*/}
	Din_Go(1173,2048);packet_put_string(blob, bloblen);
	free(blob);
	packet_send();
	{int  ReplaceReturn = 1; Din_Go(1174,2048); return ReplaceReturn;}
}

static Key *
load_identity_file(char *filename, int userprovided)
{
	Din_Go(1175,2048);Key *private;
	char prompt[300], *passphrase;
	int perm_ok = 0, quit, i;
	struct stat st;

	Din_Go(1178,2048);if (stat(filename, &st) < 0) {
		Din_Go(1176,2048);(userprovided ? logit : debug3)("no such identity: %s: %s",
		    filename, strerror(errno));
		{Key * ReplaceReturn = NULL; Din_Go(1177,2048); return ReplaceReturn;}
	}
	Din_Go(1179,2048);private = key_load_private_type(KEY_UNSPEC, filename, "", NULL, &perm_ok);
	Din_Go(1183,2048);if (!perm_ok) {
		Din_Go(1180,2048);if (private != NULL)
			{/*129*/Din_Go(1181,2048);key_free(private);/*130*/}
		{Key * ReplaceReturn = NULL; Din_Go(1182,2048); return ReplaceReturn;}
	}
	Din_Go(1195,2048);if (private == NULL) {
		Din_Go(1184,2048);if (options.batch_mode)
			return NULL;
		Din_Go(1185,2048);snprintf(prompt, sizeof prompt,
		    "Enter passphrase for key '%.100s': ", filename);
		Din_Go(1194,2048);for (i = 0; i < options.number_of_password_prompts; i++) {
			Din_Go(1186,2048);passphrase = read_passphrase(prompt, 0);
			Din_Go(1189,2048);if (strcmp(passphrase, "") != 0) {
				Din_Go(1187,2048);private = key_load_private_type(KEY_UNSPEC,
				    filename, passphrase, NULL, NULL);
				quit = 0;
			} else {
				Din_Go(1188,2048);debug2("no passphrase given, try next key");
				quit = 1;
			}
			Din_Go(1190,2048);explicit_bzero(passphrase, strlen(passphrase));
			free(passphrase);
			Din_Go(1192,2048);if (private != NULL || quit)
				{/*131*/Din_Go(1191,2048);break;/*132*/}
			Din_Go(1193,2048);debug2("bad passphrase given, try again...");
		}
	}
	{Key * ReplaceReturn = private; Din_Go(1196,2048); return ReplaceReturn;}
}

/*
 * try keys in the following order:
 *	1. agent keys that are found in the config file
 *	2. other agent keys
 *	3. keys that are only listed in the config file
 */
static void
pubkey_prepare(Authctxt *authctxt)
{
	Din_Go(1197,2048);Identity *id, *id2, *tmp;
	Idlist agent, files, *preferred;
	Key *key;
	AuthenticationConnection *ac;
	char *comment;
	int i, found;

	TAILQ_INIT(&agent);	/* keys from the agent */
	TAILQ_INIT(&files);	/* keys from the config file */
	preferred = &authctxt->keys;
	TAILQ_INIT(preferred);	/* preferred order of keys */

	/* list of keys stored in the filesystem and PKCS#11 */
	Din_Go(1204,2048);for (i = 0; i < options.num_identity_files; i++) {
		Din_Go(1198,2048);key = options.identity_keys[i];
		Din_Go(1200,2048);if (key && key->type == KEY_RSA1)
			{/*117*/Din_Go(1199,2048);continue;/*118*/}
		Din_Go(1202,2048);if (key && key->cert && key->cert->type != SSH2_CERT_TYPE_USER)
			{/*119*/Din_Go(1201,2048);continue;/*120*/}
		Din_Go(1203,2048);options.identity_keys[i] = NULL;
		id = xcalloc(1, sizeof(*id));
		id->key = key;
		id->filename = xstrdup(options.identity_files[i]);
		id->userprovided = options.identity_file_userprovided[i];
		TAILQ_INSERT_TAIL(&files, id, next);
	}
	/* Prefer PKCS11 keys that are explicitly listed */
	TAILQ_FOREACH_SAFE(id, &files, next, tmp) {
		if (id->key == NULL || (id->key->flags & KEY_FLAG_EXT) == 0)
			{/*121*/continue;/*122*/}
		found = 0;
		TAILQ_FOREACH(id2, &files, next) {
			if (id2->key == NULL ||
			    (id2->key->flags & KEY_FLAG_EXT) != 0)
				{/*123*/continue;/*124*/}
			if (key_equal(id->key, id2->key)) {
				TAILQ_REMOVE(&files, id, next);
				TAILQ_INSERT_TAIL(preferred, id, next);
				found = 1;
				break;
			}
		}
		/* If IdentitiesOnly set and key not found then don't use it */
		if (!found && options.identities_only) {
			TAILQ_REMOVE(&files, id, next);
			explicit_bzero(id, sizeof(*id));
			free(id);
		}
	}
	/* list of keys supported by the agent */
	Din_Go(1211,2048);if ((ac = ssh_get_authentication_connection())) {
		Din_Go(1205,2048);for (key = ssh_get_first_identity(ac, &comment, 2);
		    key != NULL;
		    key = ssh_get_next_identity(ac, &comment, 2)) {
			Din_Go(1206,2048);found = 0;
			TAILQ_FOREACH(id, &files, next) {
				/* agent keys from the config file are preferred */
				if (key_equal(key, id->key)) {
					key_free(key);
					free(comment);
					TAILQ_REMOVE(&files, id, next);
					TAILQ_INSERT_TAIL(preferred, id, next);
					id->ac = ac;
					found = 1;
					break;
				}
			}
			Din_Go(1208,2048);if (!found && !options.identities_only) {
				Din_Go(1207,2048);id = xcalloc(1, sizeof(*id));
				id->key = key;
				id->filename = comment;
				id->ac = ac;
				TAILQ_INSERT_TAIL(&agent, id, next);
			}
		}
		/* append remaining agent keys */
		Din_Go(1209,2048);for (id = TAILQ_FIRST(&agent); id; id = TAILQ_FIRST(&agent)) {
			TAILQ_REMOVE(&agent, id, next);
			TAILQ_INSERT_TAIL(preferred, id, next);
		}
		Din_Go(1210,2048);authctxt->agent = ac;
	}
	/* append remaining keys from the config file */
	Din_Go(1212,2048);for (id = TAILQ_FIRST(&files); id; id = TAILQ_FIRST(&files)) {
		TAILQ_REMOVE(&files, id, next);
		TAILQ_INSERT_TAIL(preferred, id, next);
	}
	TAILQ_FOREACH(id, preferred, next) {
		debug2("key: %s (%p),%s", id->filename, id->key,
		    id->userprovided ? " explicit" : "");
	}
}

static void
pubkey_cleanup(Authctxt *authctxt)
{
	Din_Go(1213,2048);Identity *id;

	Din_Go(1215,2048);if (authctxt->agent != NULL)
		{/*125*/Din_Go(1214,2048);ssh_close_authentication_connection(authctxt->agent);/*126*/}
	Din_Go(1219,2048);for (id = TAILQ_FIRST(&authctxt->keys); id;
	    id = TAILQ_FIRST(&authctxt->keys)) {
		TAILQ_REMOVE(&authctxt->keys, id, next);
		Din_Go(1217,2048);if (id->key)
			{/*127*/Din_Go(1216,2048);key_free(id->key);/*128*/}
		Din_Go(1218,2048);free(id->filename);
		free(id);
	}
Din_Go(1220,2048);}

int
userauth_pubkey(Authctxt *authctxt)
{
	Din_Go(1221,2048);Identity *id;
	int sent = 0;

	Din_Go(1238,2048);while ((id = TAILQ_FIRST(&authctxt->keys))) {
		Din_Go(1222,2048);if (id->tried++)
			{/*71*/{int  ReplaceReturn = (0); Din_Go(1223,2048); return ReplaceReturn;}/*72*/}
		/* move key to the end of the queue */
		TAILQ_REMOVE(&authctxt->keys, id, next);
		TAILQ_INSERT_TAIL(&authctxt->keys, id, next);
		/*
		 * send a test message if we have the public key. for
		 * encrypted keys we cannot do this and have to load the
		 * private key instead
		 */
		Din_Go(1235,2048);if (id->key != NULL) {
			Din_Go(1224,2048);if (key_type_plain(id->key->type) == KEY_RSA &&
			    (datafellows & SSH_BUG_RSASIGMD5) != 0) {
				Din_Go(1225,2048);debug("Skipped %s key %s for RSA/MD5 server",
				    key_type(id->key), id->filename);
			} else {/*73*/Din_Go(1226,2048);if (id->key->type != KEY_RSA1) {
				Din_Go(1227,2048);debug("Offering %s public key: %s",
				    key_type(id->key), id->filename);
				sent = send_pubkey_test(authctxt, id);
			;/*74*/}}
		} else {
			Din_Go(1228,2048);debug("Trying private key: %s", id->filename);
			id->key = load_identity_file(id->filename,
			    id->userprovided);
			Din_Go(1234,2048);if (id->key != NULL) {
				Din_Go(1229,2048);id->isprivate = 1;
				Din_Go(1232,2048);if (key_type_plain(id->key->type) == KEY_RSA &&
				    (datafellows & SSH_BUG_RSASIGMD5) != 0) {
					Din_Go(1230,2048);debug("Skipped %s key %s for RSA/MD5 "
					    "server", key_type(id->key),
					    id->filename);
				} else {
					Din_Go(1231,2048);sent = sign_and_send_pubkey(
					    authctxt, id);
				}
				Din_Go(1233,2048);key_free(id->key);
				id->key = NULL;
			}
		}
		Din_Go(1237,2048);if (sent)
			{/*75*/{int  ReplaceReturn = (sent); Din_Go(1236,2048); return ReplaceReturn;}/*76*/}
	}
	{int  ReplaceReturn = (0); Din_Go(1239,2048); return ReplaceReturn;}
}

/*
 * Send userauth request message specifying keyboard-interactive method.
 */
int
userauth_kbdint(Authctxt *authctxt)
{
	Din_Go(1240,2048);static int attempt = 0;

	Din_Go(1242,2048);if (attempt++ >= options.number_of_password_prompts)
		{/*81*/{int  ReplaceReturn = 0; Din_Go(1241,2048); return ReplaceReturn;}/*82*/}
	/* disable if no SSH2_MSG_USERAUTH_INFO_REQUEST has been seen */
	Din_Go(1245,2048);if (attempt > 1 && !authctxt->info_req_seen) {
		Din_Go(1243,2048);debug3("userauth_kbdint: disable: no info_req_seen");
		dispatch_set(SSH2_MSG_USERAUTH_INFO_REQUEST, NULL);
		{int  ReplaceReturn = 0; Din_Go(1244,2048); return ReplaceReturn;}
	}

	Din_Go(1246,2048);debug2("userauth_kbdint");
	packet_start(SSH2_MSG_USERAUTH_REQUEST);
	packet_put_cstring(authctxt->server_user);
	packet_put_cstring(authctxt->service);
	packet_put_cstring(authctxt->method->name);
	packet_put_cstring("");					/* lang */
	packet_put_cstring(options.kbd_interactive_devices ?
	    options.kbd_interactive_devices : "");
	packet_send();

	dispatch_set(SSH2_MSG_USERAUTH_INFO_REQUEST, &input_userauth_info_req);
	{int  ReplaceReturn = 1; Din_Go(1247,2048); return ReplaceReturn;}
}

/*
 * parse INFO_REQUEST, prompt user and send INFO_RESPONSE
 */
void
input_userauth_info_req(int type, u_int32_t seq, void *ctxt)
{
	Din_Go(1248,2048);Authctxt *authctxt = ctxt;
	char *name, *inst, *lang, *prompt, *response;
	u_int num_prompts, i;
	int echo = 0;

	debug2("input_userauth_info_req");

	Din_Go(1250,2048);if (authctxt == NULL)
		{/*55*/Din_Go(1249,2048);fatal("input_userauth_info_req: no authentication context");/*56*/}

	Din_Go(1251,2048);authctxt->info_req_seen = 1;

	name = packet_get_string(NULL);
	inst = packet_get_string(NULL);
	lang = packet_get_string(NULL);
	Din_Go(1253,2048);if (strlen(name) > 0)
		{/*57*/Din_Go(1252,2048);logit("%s", name);/*58*/}
	Din_Go(1255,2048);if (strlen(inst) > 0)
		{/*59*/Din_Go(1254,2048);logit("%s", inst);/*60*/}
	Din_Go(1256,2048);free(name);
	free(inst);
	free(lang);

	num_prompts = packet_get_int();
	/*
	 * Begin to build info response packet based on prompts requested.
	 * We commit to providing the correct number of responses, so if
	 * further on we run into a problem that prevents this, we have to
	 * be sure and clean this up and send a correct error response.
	 */
	packet_start(SSH2_MSG_USERAUTH_INFO_RESPONSE);
	packet_put_int(num_prompts);

	debug2("input_userauth_info_req: num_prompts %d", num_prompts);
	Din_Go(1258,2048);for (i = 0; i < num_prompts; i++) {
		Din_Go(1257,2048);prompt = packet_get_string(NULL);
		echo = packet_get_char();

		response = read_passphrase(prompt, echo ? RP_ECHO : 0);

		packet_put_cstring(response);
		explicit_bzero(response, strlen(response));
		free(response);
		free(prompt);
	}
	packet_check_eom(); /* done with parsing incoming message. */

	Din_Go(1259,2048);packet_add_padding(64);
	packet_send();
Din_Go(1260,2048);}

static int
ssh_keysign(Key *key, u_char **sigp, u_int *lenp,
    u_char *data, u_int datalen)
{
	Din_Go(1261,2048);Buffer b;
	struct stat st;
	pid_t pid;
	int to[2], from[2], status, version = 2;

	debug2("ssh_keysign called");

	Din_Go(1264,2048);if (stat(_PATH_SSH_KEY_SIGN, &st) < 0) {
		Din_Go(1262,2048);error("ssh_keysign: not installed: %s", strerror(errno));
		{int  ReplaceReturn = -1; Din_Go(1263,2048); return ReplaceReturn;}
	}
	Din_Go(1266,2048);if (fflush(stdout) != 0)
		{/*159*/Din_Go(1265,2048);error("ssh_keysign: fflush: %s", strerror(errno));/*160*/}
	Din_Go(1269,2048);if (pipe(to) < 0) {
		Din_Go(1267,2048);error("ssh_keysign: pipe: %s", strerror(errno));
		{int  ReplaceReturn = -1; Din_Go(1268,2048); return ReplaceReturn;}
	}
	Din_Go(1272,2048);if (pipe(from) < 0) {
		Din_Go(1270,2048);error("ssh_keysign: pipe: %s", strerror(errno));
		{int  ReplaceReturn = -1; Din_Go(1271,2048); return ReplaceReturn;}
	}
	Din_Go(1275,2048);if ((pid = fork()) < 0) {
		Din_Go(1273,2048);error("ssh_keysign: fork: %s", strerror(errno));
		{int  ReplaceReturn = -1; Din_Go(1274,2048); return ReplaceReturn;}
	}
	Din_Go(1283,2048);if (pid == 0) {
		/* keep the socket on exec */
		Din_Go(1276,2048);fcntl(packet_get_connection_in(), F_SETFD, 0);
		permanently_drop_suid(getuid());
		close(from[0]);
		Din_Go(1278,2048);if (dup2(from[1], STDOUT_FILENO) < 0)
			{/*161*/Din_Go(1277,2048);fatal("ssh_keysign: dup2: %s", strerror(errno));/*162*/}
		Din_Go(1279,2048);close(to[1]);
		Din_Go(1281,2048);if (dup2(to[0], STDIN_FILENO) < 0)
			{/*163*/Din_Go(1280,2048);fatal("ssh_keysign: dup2: %s", strerror(errno));/*164*/}
		Din_Go(1282,2048);close(from[1]);
		close(to[0]);
		execl(_PATH_SSH_KEY_SIGN, _PATH_SSH_KEY_SIGN, (char *) 0);
		fatal("ssh_keysign: exec(%s): %s", _PATH_SSH_KEY_SIGN,
		    strerror(errno));
	}
	Din_Go(1284,2048);close(from[1]);
	close(to[0]);

	buffer_init(&b);
	buffer_put_int(&b, packet_get_connection_in()); /* send # of socket */
	buffer_put_string(&b, data, datalen);
	Din_Go(1286,2048);if (ssh_msg_send(to[1], version, &b) == -1)
		{/*165*/Din_Go(1285,2048);fatal("ssh_keysign: couldn't send request");/*166*/}

	Din_Go(1289,2048);if (ssh_msg_recv(from[0], &b) < 0) {
		Din_Go(1287,2048);error("ssh_keysign: no reply");
		buffer_free(&b);
		{int  ReplaceReturn = -1; Din_Go(1288,2048); return ReplaceReturn;}
	}
	Din_Go(1290,2048);close(from[0]);
	close(to[1]);

	Din_Go(1293,2048);while (waitpid(pid, &status, 0) < 0)
		{/*167*/Din_Go(1291,2048);if (errno != EINTR)
			{/*169*/Din_Go(1292,2048);break;/*170*/}/*168*/}

	Din_Go(1296,2048);if (buffer_get_char(&b) != version) {
		Din_Go(1294,2048);error("ssh_keysign: bad version");
		buffer_free(&b);
		{int  ReplaceReturn = -1; Din_Go(1295,2048); return ReplaceReturn;}
	}
	Din_Go(1297,2048);*sigp = buffer_get_string(&b, lenp);
	buffer_free(&b);

	{int  ReplaceReturn = 0; Din_Go(1298,2048); return ReplaceReturn;}
}

int
userauth_hostbased(Authctxt *authctxt)
{
	Din_Go(1299,2048);Key *private = NULL;
	Sensitive *sensitive = authctxt->sensitive;
	Buffer b;
	u_char *signature, *blob;
	char *chost, *pkalg, *p;
	const char *service;
	u_int blen, slen;
	int ok, i, found = 0;

	/* check for a useful key */
	Din_Go(1304,2048);for (i = 0; i < sensitive->nkeys; i++) {
		Din_Go(1300,2048);private = sensitive->keys[i];
		Din_Go(1303,2048);if (private && private->type != KEY_RSA1) {
			Din_Go(1301,2048);found = 1;
			/* we take and free the key */
			sensitive->keys[i] = NULL;
			Din_Go(1302,2048);break;
		}
	}
	Din_Go(1307,2048);if (!found) {
		Din_Go(1305,2048);debug("No more client hostkeys for hostbased authentication.");
		{int  ReplaceReturn = 0; Din_Go(1306,2048); return ReplaceReturn;}
	}
	Din_Go(1310,2048);if (key_to_blob(private, &blob, &blen) == 0) {
		Din_Go(1308,2048);key_free(private);
		{int  ReplaceReturn = 0; Din_Go(1309,2048); return ReplaceReturn;}
	}
	/* figure out a name for the client host */
	Din_Go(1311,2048);p = get_local_name(packet_get_connection_in());
	Din_Go(1314,2048);if (p == NULL) {
		Din_Go(1312,2048);error("userauth_hostbased: cannot get local ipaddr/name");
		key_free(private);
		free(blob);
		{int  ReplaceReturn = 0; Din_Go(1313,2048); return ReplaceReturn;}
	}
	Din_Go(1315,2048);xasprintf(&chost, "%s.", p);
	debug2("userauth_hostbased: chost %s", chost);
	free(p);

	service = datafellows & SSH_BUG_HBSERVICE ? "ssh-userauth" :
	    authctxt->service;
	pkalg = xstrdup(key_ssh_name(private));
	buffer_init(&b);
	/* construct data */
	buffer_put_string(&b, session_id2, session_id2_len);
	buffer_put_char(&b, SSH2_MSG_USERAUTH_REQUEST);
	buffer_put_cstring(&b, authctxt->server_user);
	buffer_put_cstring(&b, service);
	buffer_put_cstring(&b, authctxt->method->name);
	buffer_put_cstring(&b, pkalg);
	buffer_put_string(&b, blob, blen);
	buffer_put_cstring(&b, chost);
	buffer_put_cstring(&b, authctxt->local_user);
#ifdef DEBUG_PK
	buffer_dump(&b);
#endif
	Din_Go(1318,2048);if (sensitive->external_keysign)
		{/*83*/Din_Go(1316,2048);ok = ssh_keysign(private, &signature, &slen,
		    buffer_ptr(&b), buffer_len(&b));/*84*/}
	else
		{/*85*/Din_Go(1317,2048);ok = key_sign(private, &signature, &slen,
		    buffer_ptr(&b), buffer_len(&b));/*86*/}
	Din_Go(1319,2048);key_free(private);
	buffer_free(&b);
	Din_Go(1322,2048);if (ok != 0) {
		Din_Go(1320,2048);error("key_sign failed");
		free(chost);
		free(pkalg);
		free(blob);
		{int  ReplaceReturn = 0; Din_Go(1321,2048); return ReplaceReturn;}
	}
	Din_Go(1323,2048);packet_start(SSH2_MSG_USERAUTH_REQUEST);
	packet_put_cstring(authctxt->server_user);
	packet_put_cstring(authctxt->service);
	packet_put_cstring(authctxt->method->name);
	packet_put_cstring(pkalg);
	packet_put_string(blob, blen);
	packet_put_cstring(chost);
	packet_put_cstring(authctxt->local_user);
	packet_put_string(signature, slen);
	explicit_bzero(signature, slen);
	free(signature);
	free(chost);
	free(pkalg);
	free(blob);

	packet_send();
	{int  ReplaceReturn = 1; Din_Go(1324,2048); return ReplaceReturn;}
}

/* find auth method */

/*
 * given auth method name, if configurable options permit this method fill
 * in auth_ident field and return true, otherwise return false.
 */
static int
authmethod_is_enabled(Authmethod *method)
{
	Din_Go(1325,2048);if (method == NULL)
		{/*171*/{int  ReplaceReturn = 0; Din_Go(1326,2048); return ReplaceReturn;}/*172*/}
	/* return false if options indicate this method is disabled */
	Din_Go(1328,2048);if  (method->enabled == NULL || *method->enabled == 0)
		{/*173*/{int  ReplaceReturn = 0; Din_Go(1327,2048); return ReplaceReturn;}/*174*/}
	/* return false if batch mode is enabled but method needs interactive mode */
	Din_Go(1330,2048);if  (method->batch_flag != NULL && *method->batch_flag != 0)
		{/*175*/{int  ReplaceReturn = 0; Din_Go(1329,2048); return ReplaceReturn;}/*176*/}
	{int  ReplaceReturn = 1; Din_Go(1331,2048); return ReplaceReturn;}
}

static Authmethod *
authmethod_lookup(const char *name)
{
	Din_Go(1332,2048);Authmethod *method = NULL;
	Din_Go(1336,2048);if (name != NULL)
		{/*139*/Din_Go(1333,2048);for (method = authmethods; method->name != NULL; method++)
			{/*141*/Din_Go(1334,2048);if (strcmp(name, method->name) == 0)
				{/*143*/{Authmethod * ReplaceReturn = method; Din_Go(1335,2048); return ReplaceReturn;}/*144*/}/*142*/}/*140*/}
	Din_Go(1337,2048);debug2("Unrecognized authentication method name: %s", name ? name : "NULL");
	{Authmethod * ReplaceReturn = NULL; Din_Go(1338,2048); return ReplaceReturn;}
}

/* XXX internal state */
static Authmethod *current = NULL;
static char *supported = NULL;
static char *preferred = NULL;

/*
 * Given the authentication method list sent by the server, return the
 * next method we should try.  If the server initially sends a nil list,
 * use a built-in default list.
 */
static Authmethod *
authmethod_get(char *authlist)
{
	Din_Go(1339,2048);char *name = NULL;
	u_int next;

	/* Use a suitable default if we're passed a nil list.  */
	Din_Go(1341,2048);if (authlist == NULL || strlen(authlist) == 0)
		{/*133*/Din_Go(1340,2048);authlist = options.preferred_authentications;/*134*/}

	Din_Go(1345,2048);if (supported == NULL || strcmp(authlist, supported) != 0) {
		Din_Go(1342,2048);debug3("start over, passed a different list %s", authlist);
		free(supported);
		supported = xstrdup(authlist);
		preferred = options.preferred_authentications;
		debug3("preferred %s", preferred);
		current = NULL;
	} else {/*135*/Din_Go(1343,2048);if (current != NULL && authmethod_is_enabled(current))
		{/*137*/{Authmethod * ReplaceReturn = current; Din_Go(1344,2048); return ReplaceReturn;}/*138*/}/*136*/}

	Din_Go(1354,2048);for (;;) {
		Din_Go(1346,2048);if ((name = match_list(preferred, supported, &next)) == NULL) {
			Din_Go(1347,2048);debug("No more authentication methods to try.");
			current = NULL;
			{Authmethod * ReplaceReturn = NULL; Din_Go(1348,2048); return ReplaceReturn;}
		}
		Din_Go(1349,2048);preferred += next;
		debug3("authmethod_lookup %s", name);
		debug3("remaining preferred: %s", preferred);
		Din_Go(1352,2048);if ((current = authmethod_lookup(name)) != NULL &&
		    authmethod_is_enabled(current)) {
			Din_Go(1350,2048);debug3("authmethod_is_enabled %s", name);
			debug("Next authentication method: %s", name);
			free(name);
			{Authmethod * ReplaceReturn = current; Din_Go(1351,2048); return ReplaceReturn;}
		}
		Din_Go(1353,2048);free(name);
	}
Din_Go(1355,2048);}

static char *
authmethods_get(void)
{
	Din_Go(1356,2048);Authmethod *method = NULL;
	Buffer b;
	char *list;

	buffer_init(&b);
	Din_Go(1361,2048);for (method = authmethods; method->name != NULL; method++) {
		Din_Go(1357,2048);if (authmethod_is_enabled(method)) {
			Din_Go(1358,2048);if (buffer_len(&b) > 0)
				{/*145*/Din_Go(1359,2048);buffer_append(&b, ",", 1);/*146*/}
			Din_Go(1360,2048);buffer_append(&b, method->name, strlen(method->name));
		}
	}
	Din_Go(1362,2048);buffer_append(&b, "\0", 1);
	list = xstrdup(buffer_ptr(&b));
	buffer_free(&b);
	{char * ReplaceReturn = list; Din_Go(1363,2048); return ReplaceReturn;}
}

