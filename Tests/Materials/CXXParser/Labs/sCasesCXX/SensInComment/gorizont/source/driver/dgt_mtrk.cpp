//---------------------------------------------------------------------------
#include <math.h>
#include "var/tmp/sensor.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "dgt_osid.h"
#include "dgt_base.h"
#include "dgt_mtrk.h"
//---------------------------------------------------------------------------
TDgtMetricEditor::TDgtMetricEditor()
{
	SensorCall(1);dim        = NULL;
	part       = NULL;
	type       = NULL;
	metric     = NULL;
	isx_metric = NULL;
//
	nodeSize     = 4;
	dFactor      = 4.0;
	mode         = vmMetricNone;
	setxymode    = meXYFloat;
	activecontur = -1;
	activenode   = -1;
	derivenode   = 0;
	curr_xy.x    = 0.0;
	curr_xy.y    = 0.0;
	x_fixed      = 0;
	y_fixed      = 0;
	snap_xy.x    = 0.0;
	snap_xy.y    = 0.0;
	snapmode     = 0;
	snapped      = 0;
	memset(&snap_object,0,sizeof snap_object);
SensorCall(2);}
//---------------------------------------------------------------------------
TDgtMetricEditor::~TDgtMetricEditor()
{
	SensorCall(3);delete[] isx_metric;
	delete[] metric;
//
	delete[] dim;
	delete[] part;
	delete[] type;
SensorCall(4);}
//---------------------------------------------------------------
void TDgtMetricEditor::setxy(DIM_NODE &_node, double _x, double _y)
{
	SensorCall(5);if((setxymode == meXYShort) || (setxymode == meXYLong))
	{
		SensorCall(6);if(_x > 0.0)
			{/*1*/SensorCall(7);_x = (int)(_x + 0.5);/*2*/}
		else {/*3*/SensorCall(8);if(_x < 0.0)
			{/*5*/SensorCall(9);_x = (int)(_x - 0.5);/*6*/}/*4*/}
		SensorCall(13);if(_y > 0.0)
			{/*7*/SensorCall(10);_y = (int)(_y + 0.5);/*8*/}
		else {/*9*/SensorCall(11);if(_y < 0.0)
			{/*11*/SensorCall(12);_y = (int)(_y - 0.5);/*12*/}/*10*/}
	}
	SensorCall(15);if(setxymode == meXYShort)
	{
		SensorCall(14);_x = min2(max2(-32768, _x), 32767);
		_y = min2(max2(-32768, _y), 32767);
	}
//
	SensorCall(16);_node.x = _x;
	_node.y = _y;
SensorCall(17);}
//---------------------------------------------------------------
void TDgtMetricEditor::pix2dim1(double &_x, double &_y)
{
	SensorCall(18);pix2dim(_x, _y);
//
	DIM_NODE node;
	memset(&node, 0, sizeof(node));
	setxy(node, _x, _y);
	_x = node.x;
	_y = node.y;
SensorCall(19);}
//---------------------------------------------------------------
int TDgtMetricEditor::under(double _x, double _y)
{
	SensorCall(20);DIM_NODE *node = metricNodeA(activenode);
	SensorCall(24);if(node)
	{
		SensorCall(21);double x = node->x,
		       y = node->y;
		dim2pix(x, y);
		SensorCall(23);if((fabs(x - _x) < nodeSize + 1) &&
		   (fabs(y - _y) < nodeSize + 1))
			{/*13*/{int  ReplaceReturn148 = activenode; SensorCall(22); return ReplaceReturn148;}/*14*/}
	}
	SensorCall(25);activenode = -1;
//
	int n = NDim__NodesCount(metric);
	node = NDim__Nodes(metric);

	double dx,
	       dy;
	SensorCall(32);for(int a = 0; a < n; a++, node++)
	{
		SensorCall(26);double x = node->x,
		       y = node->y;
		dim2pix(x, y);
		SensorCall(31);if((fabs(x -= _x) < nodeSize + 1) &&
		   (fabs(y -= _y) < nodeSize + 1))
			{/*15*/SensorCall(27);if(activenode == -1)
			{
				SensorCall(28);activenode = a;
				dx = x;
				dy = y;
			}
			else {/*17*/SensorCall(29);if(dx*dx + dy*dy > x*x + y*y)
			{
				SensorCall(30);activenode = a;
				dx = x;
				dy = y;
			;/*18*/}/*16*/}}
	}
//
	{int  ReplaceReturn147 = activenode; SensorCall(33); return ReplaceReturn147;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::activenodeA()
{
	SensorCall(34);double x = 0,
	       y = 0;
	mouseposget(x, y);
//
	{int  ReplaceReturn146 = under(x, y); SensorCall(35); return ReplaceReturn146;}
}
//---------------------------------------------------------------
void TDgtMetricEditor::mouserectset(int _mode)
{
	SensorCall(36);switch(_mode)
	{
		case vmClipNone:
		case vmClipControl:
			SensorCall(37);break;
		case vmClipXControl:
		case vmClipYControl:
			SensorCall(38);break;
	}
SensorCall(39);}
//---------------------------------------------------------------
DIM_NODE *TDgtMetricEditor::metricNodeA(int _node)
{
	SensorCall(40);if(_node < 0)
		return NULL;
	SensorCall(41);if(_node >= NDim__NodesCount(metric))
		return NULL;
//
	{__IASTRUCT__ DIM_NODE * ReplaceReturn145 = NDim__Nodes(metric) + _node; SensorCall(42); return ReplaceReturn145;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricNodeB(int _contur, int _node)
{
	SensorCall(43);if((_contur < 0) || (_contur >= metric->ContursCount))
		{/*19*/{int  ReplaceReturn144 = -1; SensorCall(44); return ReplaceReturn144;}/*20*/}
	SensorCall(45);DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
	SensorCall(47);if((_node < 0) || (_node >= contur[_contur].NodesCount))
		{/*21*/{int  ReplaceReturn143 = -1; SensorCall(46); return ReplaceReturn143;}/*22*/}
//
	SensorCall(49);for(int a = 0; a < _contur; a++, contur++)
		{/*23*/SensorCall(48);_node += contur->NodesCount;/*24*/}
//
	{int  ReplaceReturn142 = _node; SensorCall(50); return ReplaceReturn142;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricNodeInsertA(double _x, double _y)
{
SensorCall(51);DIM_CONTUR *conturs;
DIM_NODE   *nodes;
double     radius=_dgt_max_double;
int       contur,node;

	SensorCall(53);if(metric->ContursCount < 1)
		{/*25*/{int  ReplaceReturn141 = metricNodeInsertB(0, 0, _x, _y); SensorCall(52); return ReplaceReturn141;}/*26*/}

	SensorCall(60);if(under(_x, _y) != -1)
		{
// EE - 15-Aug-03  17:18:58 - razreshim vstavky parnoy vershini
		SensorCall(54);conturs = (DIM_CONTUR*)(metric + 1);
		node    = activenode;
		SensorCall(58);for (int b = 0; b < metric->ContursCount; b++)
			{
			SensorCall(55);if (node < conturs->NodesCount)
				{/*27*/{int  ReplaceReturn140 = metricNodeInsertB(b, node, _x, _y); SensorCall(56); return ReplaceReturn140;}/*28*/}
			SensorCall(57);node -= conturs->NodesCount;
			conturs++;
			}
// EE - end
		{int  ReplaceReturn139 = activenode; SensorCall(59); return ReplaceReturn139;}
		}
//
	SensorCall(61);double x = _x,
	       y = _y;
	pix2dim(x, y);
//
	conturs = (DIM_CONTUR*)(metric + 1);
	nodes   = NDim__Nodes(metric);
	contur  = -1;
	node    = -1;
	SensorCall(104);for(int b = 0; b < metric->ContursCount; b++)
	{
		SensorCall(62);if((radius == _dgt_max_double) &&
		   (contur == -1) &&
		   (conturs->NodesCount == 0))
		{
			SensorCall(63);contur = b;
			node = 0;
		}
		else {/*29*/SensorCall(64);if(conturs->NodesCount == 1)
		{
			SensorCall(65);double r1 = hypot(x - nodes->x, y - nodes->y);
			SensorCall(67);if(r1 < radius)
			{
				SensorCall(66);contur = b;
				node = 1;
				radius = r1;
			}
		}
		else {/*31*/SensorCall(68);if(ItClosed(nodes, conturs->NodesCount))
		{
			SensorCall(69);if(conturs->NodesCount == 2)
			{
				SensorCall(70);double r1 = hypot(x - nodes->x, y - nodes->y);
				SensorCall(72);if(r1 < radius)
				{
					SensorCall(71);contur = b;
					node = 1;
					radius = r1;
				}
			}
			else
			{
				SensorCall(73);int n = conturs->NodesCount - 1;
				SensorCall(80);for(int a = 0; a < n; a++)
				{
					SensorCall(74);XYD n0 = {x, y},
					    n1 = {nodes[(a + n + 2)%n].x, nodes[(a + n + 2)%n].y},
					    n2 = {nodes[(a + n + 1)%n].x, nodes[(a + n + 1)%n].y},
					    n3 = {nodes[(a + n    )%n].x, nodes[(a + n    )%n].y},
					    n4 = {nodes[(a + n - 1)%n].x, nodes[(a + n - 1)%n].y},
					   *nn1 = &n1,
					   *nn4 = &n4;
//					    nnnn;
// EE - 14-Aug-03  19:31:33 - vstavka vershini okolo parnix vershin
						SensorCall(75);if ((n1.x == n2.x) && (n1.y == n2.y))
							nn1 = NULL;
						SensorCall(76);if ((n3.x == n4.x) && (n3.y == n4.y))
							nn4 = NULL;
// EE - end
					SensorCall(77);double r1 = NAlg__LineDistance(
						n0, (XYD*)NULL, nn1, n2, n3, nn4);
//						n0,      &nnnn, nn1, n2, n3, nn4);
					SensorCall(79);if(r1 < radius)
					{
//dim2pix(nnnn.x, nnnn.y);
//_x = nnnn.x;
//_y = nnnn.y;
						SensorCall(78);contur = b;
						node = a + 1;
						radius = r1;
					}
				}
			}
		}
		else
		{
			SensorCall(81);int nn = conturs->NodesCount - 1,
			     n = conturs->NodesCount;
			SensorCall(102);for(int a = 0; a < nn; a++)
			{
				SensorCall(82);XYD n0 = {x, y},
				    n1 = {nodes[(a + n + 2)%n].x, nodes[(a + n + 2)%n].y},
				    n2 = {nodes[(a + n + 1)%n].x, nodes[(a + n + 1)%n].y},
				    n3 = {nodes[(a + n    )%n].x, nodes[(a + n    )%n].y},
				    n4 = {nodes[(a + n - 1)%n].x, nodes[(a + n - 1)%n].y},
				   *nn1 = &n1,
				   *nn4 = &n4;
				SensorCall(83);if(a == 0)
					nn4 = NULL;
				SensorCall(84);if(a == (nn - 1))
					nn1 = NULL;
// EE - 14-Aug-03  19:31:33 - vstavka vershini okolo parnix vershin
				SensorCall(85);if ((n1.x == n2.x) && (n1.y == n2.y))
					nn1 = NULL;
				SensorCall(86);if ((n3.x == n4.x) && (n3.y == n4.y))
					nn4 = NULL;
// EE - end
				SensorCall(87);double r1 = NAlg__LineDistance(
					n0, (XYD*)NULL, nn1, n2, n3, nn4);
				SensorCall(101);if((r1 == _dgt_max_double) && (a == 0))
				{
					SensorCall(88);r1 = hypot(x - n3.x, y - n3.y);
					SensorCall(90);if(r1 < radius)
					{
						SensorCall(89);contur = b;
						node = 0;
						radius = r1;
					}
					SensorCall(94);if(n == 2)
					{
						SensorCall(91);r1 = hypot(x - n2.x, y - n2.y);
						SensorCall(93);if(r1 < radius)
						{
							SensorCall(92);contur = b;
							node = n;
							radius = r1;
						}
					}
				}
				else {/*33*/SensorCall(95);if((r1 == _dgt_max_double) && (a == (nn - 1)))
				{
					SensorCall(96);r1 = hypot(x - n2.x, y - n2.y);
					SensorCall(98);if(r1 < radius)
					{
						SensorCall(97);contur = b;
						node = n;
						radius = r1;
					}
				}
				else {/*35*/SensorCall(99);if(r1 < radius)
				{
					SensorCall(100);contur = b;
					node = a + 1;
					radius = r1;
				;/*36*/}/*34*/}}
			}
		;/*32*/}/*30*/}}
		SensorCall(103);nodes += conturs->NodesCount;
		conturs++;
	}
//
	{int  ReplaceReturn138 = metricNodeInsertB(contur, node, _x, _y); SensorCall(105); return ReplaceReturn138;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricNodeInsertB(int _contur, int _node, double _x, double _y)
{
	SensorCall(106);activenode = -1;
	SensorCall(108);if((_node < 0) || (_contur < 0))
		{/*37*/{int  ReplaceReturn137 = activenode; SensorCall(107); return ReplaceReturn137;}/*38*/}
//
	SensorCall(110);if(_contur > metric->ContursCount)
		{/*39*/SensorCall(109);_contur = metric->ContursCount;/*40*/}
	SensorCall(114);if(metric->ContursCount > _contur)
	{
		SensorCall(111);if(_node > ((DIM_CONTUR*)(metric + 1) + _contur)->NodesCount)
			{/*41*/SensorCall(112);_node = ((DIM_CONTUR*)(metric + 1) + _contur)->NodesCount;/*42*/}
	}
	else
		{/*43*/SensorCall(113);_node = 0;/*44*/}
//
	SensorCall(115);int n1 = NDim__NodesCount(metric),
	     n = 0;
	DIM_CONTUR *dc = (DIM_CONTUR*)(metric + 1);
	SensorCall(120);for(int a = 0; a < metric->ContursCount; a++, dc++)
		{/*45*/SensorCall(116);if(_contur == a)
		{
			SensorCall(117);n += _node;
			SensorCall(118);break;
		}
		else
			{/*47*/SensorCall(119);n += dc->NodesCount;/*48*/}/*46*/}
//
	SensorCall(121);int len = NDim__MetricLen(metric);
	len += sizeof(DIM_NODE);
	SensorCall(123);if(metric->ContursCount == _contur)
		{/*49*/SensorCall(122);len += sizeof(DIM_CONTUR);/*50*/}
	SensorCall(124);DIM_METRIC *m1 = (DIM_METRIC*)new char[len];
	memset(m1, 0, len);
	memcpy(m1, metric, sizeof(DIM_METRIC) + sizeof(DIM_CONTUR)*metric->ContursCount);
	SensorCall(126);if(metric->ContursCount == _contur)
		{/*51*/SensorCall(125);m1->ContursCount++;/*52*/}
	SensorCall(127);dc = (DIM_CONTUR*)(m1 + 1) + _contur;
	dc->NodesCount++;
//
	memcpy((DIM_CONTUR*)(m1 + 1) + m1->ContursCount,
	    (DIM_CONTUR*)(metric + 1) + metric->ContursCount,
	    n*sizeof(DIM_NODE));
	memcpy((DIM_NODE*)((DIM_CONTUR*)(m1 + 1) + m1->ContursCount) + n + 1,
	    (DIM_NODE*)((DIM_CONTUR*)(metric + 1) + metric->ContursCount) + n,
	    (n1 - n)*sizeof(DIM_NODE));
//
	DIM_NODE *dn = (DIM_NODE*)((DIM_CONTUR*)(m1 + 1) + m1->ContursCount);
	SensorCall(131);if(dc->NodesCount > 1)
	{
		SensorCall(128);if(dc->NodesCount - 1 == _node)
			{/*53*/SensorCall(129);dn[n].Flags |= dn[n - 1].Flags & DIM_NODE::dnPenUp;/*54*/}
		else
			{/*55*/SensorCall(130);dn[n].Flags |= dn[n + 1].Flags & DIM_NODE::dnPenUp;/*56*/}
	}
	SensorCall(132);pix2dim(_x, _y);
//
	setxy(dn[n], _x, _y);
//
	SensorCall(147);if((_node > 0) && (_node < dc->NodesCount - 1))
	{
		SensorCall(133);if ((dn[n-1].dXOut != 0.0) || (dn[n-1].dYOut != 0.0) ||
		    (dn[n+1].dXIn  != 0.0) || (dn[n+1].dYIn  != 0.0))
		{
			SensorCall(134);double dx = dn[n+1].x - dn[n-1].x;
			double dy = dn[n+1].y - dn[n-1].y;
			double h  = hypot(dx,dy),
				   h_in, h_out;
			SensorCall(136);if (h != 0.0)
			{
				SensorCall(135);h_in  = hypot(dn[n].x-dn[n-1].x,dn[n].y-dn[n-1].y);
				h_out = hypot(dn[n+1].x-dn[n].x,dn[n+1].y-dn[n].y);
				dn[n].dXIn  = dx * h_in  / h;
				dn[n].dYIn  = dy * h_in  / h;
				dn[n].dXOut = dx * h_out / h;
				dn[n].dYOut = dy * h_out / h;
			}
			SensorCall(141);if(_node > 1)
			{
				SensorCall(137);n--;
				dx = dn[n+1].x - dn[n-1].x;
				dy = dn[n+1].y - dn[n-1].y;
				h  = hypot(dx,dy);
				SensorCall(139);if (h != 0.0)
				{
					SensorCall(138);h_out = hypot(dn[n+1].x-dn[n].x,dn[n+1].y-dn[n].y);
					dn[n].dXOut = dx * h_out / h;
					dn[n].dYOut = dy * h_out / h;
				}
				SensorCall(140);n++;
			}
			SensorCall(146);if(_node < dc->NodesCount - 2)
			{
				SensorCall(142);n++;
				dx = dn[n+1].x - dn[n-1].x;
				dy = dn[n+1].y - dn[n-1].y;
				h  = hypot(dx,dy);
				SensorCall(144);if (h != 0.0)
				{
					SensorCall(143);h_in  = hypot(dn[n].x-dn[n-1].x,dn[n].y-dn[n-1].y);
					dn[n].dXIn  = dx * h_in  / h;
					dn[n].dYIn  = dy * h_in  / h;
				}
				SensorCall(145);n--;
			}
		}
	}
//
	SensorCall(148);delete[] metric;
	metric = m1;
//
	{int  ReplaceReturn136 = activenode = n; SensorCall(149); return ReplaceReturn136;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricNodeInsertC(double _x, double _y)
{
SensorCall(150);int n;

	SensorCall(152);if((activecontur < 0) || (activecontur > metric->ContursCount))
		{/*57*/{int  ReplaceReturn135 = -1; SensorCall(151); return ReplaceReturn135;}/*58*/}
//
	SensorCall(157);if((metric->ContursCount > 0) && (activecontur < metric->ContursCount)) // EE - 30-Jan-07  19:42:49 "&& (activecontur..."
		{
		SensorCall(153);n = ((DIM_CONTUR*)(metric + 1) + activecontur)->NodesCount; // EE - 19-Mar-04  13:06:09 " + activecontur"
		SensorCall(156);if (n > 2)
			{/*59*/SensorCall(154);if (activenode < n - 1) // EE - 19-Mar-04  13:34:00
				{/*61*/SensorCall(155);metricConturOpen(activecontur);/*62*/}/*60*/}
		}
//
	SensorCall(165);if(activecontur == metric->ContursCount)
	{
		SensorCall(158);metricNodeInsertB(activecontur, 0, _x, _y);
		metricNodeInsertB(activecontur, 1, _x, _y);
	}
	else
	{
		SensorCall(159);n = ((DIM_CONTUR*)(metric + 1) + activecontur)->NodesCount;
		SensorCall(164);if(n < 1)
		{
			SensorCall(160);metricNodeInsertB(activecontur, 0, _x, _y);
			metricNodeInsertB(activecontur, 1, _x, _y);
		}
		else {/*63*/SensorCall(161);if(n == 1)
		{
			SensorCall(162);metricNodeInsertB(activecontur, 0, _x, _y);
			DIM_NODE *n0 = metricNodeA(activenode),
			         *n1 = n0 + 1;
			n1->x = n0->x;
			n1->y = n0->y;
			activenode++;
		}
		else
		{
			SensorCall(163);n = max2(0, n - 1);
			metricNodeInsertB(activecontur, n, _x, _y);
			DIM_NODE *n0 = metricNodeA(activenode),
			         *n1 = n0 + 1;
			n1->x = n0->x;
			n1->y = n0->y;
			activenode++;
		;/*64*/}}
	}
//
	{int  ReplaceReturn134 = activenode; SensorCall(166); return ReplaceReturn134;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricNodeDeleteA(int _node)
{
	SensorCall(167);activenode = -1;
//
	{int  ReplaceReturn133 = metricNodeDeleteB(
		metricContur(_node),
		metricConturNode(_node)); SensorCall(168); return ReplaceReturn133;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricNodeDeleteB(int _contur, int _node)
{
	SensorCall(169);activenode = metricNodeB(_contur, _node);
	SensorCall(171);if(activenode < 0)
		{/*65*/{int  ReplaceReturn132 = activenode; SensorCall(170); return ReplaceReturn132;}/*66*/}
//
	SensorCall(178);if(mode == vmMetricNewLine)
	{
		SensorCall(172);DIM_CONTUR *c0 = (DIM_CONTUR*)(metric + 1) + _contur;
		SensorCall(176);if(c0->NodesCount > 1)
		{
			SensorCall(173);DIM_NODE *n0 = NDim__Nodes(metric) + activenode;
			int nn = NDim__NodesCount(metric);
			memmove(n0, n0 + 1, (nn - activenode - 1)*sizeof(*n0));
			c0->NodesCount--;
// EE - 22-Apr-10  18:55:04
			nn --;
			SensorCall(175);if (activenode >= nn)
				{/*67*/SensorCall(174);activenode = nn - 1;/*68*/}
// EE - end
		}
		{int  ReplaceReturn131 = activenode; SensorCall(177); return ReplaceReturn131;}
	}
//
	SensorCall(179);int len = NDim__MetricLen(metric),
	     empty = 0,
	     count = 0;
	DIM_METRIC *m1 = (DIM_METRIC*)new char[len];
	memset(m1, 0, len);
	memcpy(m1, metric, sizeof(*m1) + metric->ContursCount*sizeof(DIM_CONTUR));
	DIM_CONTUR *c0 = (DIM_CONTUR*)(metric + 1),
	           *c1 = (DIM_CONTUR*)(m1 + 1);
	DIM_NODE   *n0 = NDim__Nodes(metric),
	           *n1 = NDim__Nodes(m1),
	           *n1t = n1,
	           *node = metricNodeA(activenode);
	SensorCall(224);for(int a = 0; a < metric->ContursCount; a++)
	{
		SensorCall(180);if(c1->NodesCount > 0)
		{
			SensorCall(181);if(node->Flags & DIM_NODE::dnSelected)
			{
				SensorCall(182);int n = 0;
				SensorCall(199);if(ItClosed(n0, c0->NodesCount))
				{
					SensorCall(183);if(_node == c0->NodesCount - 1)
						{/*69*/SensorCall(184);_node = 0;/*70*/}
					SensorCall(185);int nn = _node;
					SensorCall(190);for(int b = 0; b < c0->NodesCount - 1; b++)
						{/*71*/SensorCall(186);if(!(n0[b].Flags & DIM_NODE::dnSelected))
							{/*73*/SensorCall(187);memcpy(n1 + n++, n0 + b, sizeof(*n1));/*74*/}
						else
							{/*75*/SensorCall(188);if(nn > b)
								{/*77*/SensorCall(189);_node--;/*78*/}/*76*/}/*72*/}
					SensorCall(192);if(n > 0)
						{/*79*/SensorCall(191);memcpy(n1 + n++, n1, sizeof(*n1));/*80*/}
				}
				else
				{
					SensorCall(193);int nn = _node;
					SensorCall(198);for(int b = 0; b < c0->NodesCount; b++)
						{/*81*/SensorCall(194);if(!(n0[b].Flags & DIM_NODE::dnSelected))
							{/*83*/SensorCall(195);memcpy(n1 + n++, n0 + b, sizeof(*n1));/*84*/}
						else
							{/*85*/SensorCall(196);if(nn > b)
								{/*87*/SensorCall(197);_node--;/*88*/}/*86*/}/*82*/}
				}
				SensorCall(200);c1->NodesCount = n;
				n1 += c1->NodesCount;
				n0 += c0->NodesCount;
			}
			else
			{
				SensorCall(201);if(_contur == a)
				{
					SensorCall(202);if(ItClosed(n0, c0->NodesCount) &&
						((_node == 0) || (_node == c0->NodesCount - 1)))
					{
						SensorCall(203);if(_node ==  c0->NodesCount - 1)
							{/*89*/SensorCall(204);_node = 0;/*90*/}
						SensorCall(210);if(c0->NodesCount < 3)
							{/*91*/SensorCall(205);c1->NodesCount = 1;/*92*/}
						else
							{/*93*/SensorCall(206);for(int b = 1; b < c0->NodesCount; b++)
								{/*95*/SensorCall(207);if(b == c0->NodesCount - 1)
									{/*97*/SensorCall(208);memcpy(n1 + (b - 1), n0 + 1, sizeof(*n1));/*98*/}
								else
									{/*99*/SensorCall(209);memcpy(n1 + (b - 1), n0 + b, sizeof(*n1));/*100*/}/*96*/}/*94*/}
					}
					else
					{
						SensorCall(211);for(int b = 0, t = 0; b < c0->NodesCount; b++)
							{/*101*/SensorCall(212);if(b != _node)
								{/*103*/SensorCall(213);memcpy(n1 + t++, n0 + b, sizeof(*n1));/*104*/}/*102*/}
						SensorCall(215);if(_node ==  c0->NodesCount - 1)
						{
							SensorCall(214);_node--;
							_node = max2(0, _node);
						}
					}
					SensorCall(216);c1->NodesCount--;
				}
				else
					{/*105*/SensorCall(217);for(int b = 0; b < c0->NodesCount; b++)
						{/*107*/SensorCall(218);memcpy(n1 + b, n0 + b, sizeof(*n1));/*108*/}/*106*/}
				SensorCall(219);n1 += c1->NodesCount;
				n0 += c0->NodesCount;
			}
		}
		SensorCall(222);if(c1->NodesCount < 1)
			{/*109*/SensorCall(220);empty++;/*110*/}
		else
			{/*111*/SensorCall(221);count += c1->NodesCount;/*112*/}
		SensorCall(223);c1++;
		c0++;
	}
	SensorCall(238);if(empty)
	{
		SensorCall(225);c1 = (DIM_CONTUR*)(m1 + 1);
		int n = 0;
		SensorCall(234);for(int a = 0; a < m1->ContursCount; a++)
			{/*113*/SensorCall(226);if(c1[a].NodesCount > 0)
			{
				SensorCall(227);if(_contur == a)
					{/*115*/SensorCall(228);_contur = n;/*116*/}
				SensorCall(230);if(a != n)
					{/*117*/SensorCall(229);memcpy(c1 + n, c1 + a, sizeof(*c1));/*118*/}
				SensorCall(231);n++;
			}
			else
				{/*119*/SensorCall(232);if(_contur == a)
					{/*121*/SensorCall(233);_contur = -1;/*122*/}/*120*/}/*114*/}
		SensorCall(235);m1->ContursCount = n;
		SensorCall(237);if(m1->ContursCount > 0)
		{
			SensorCall(236);n1 = NDim__Nodes(m1);
			memmove(n1, n1t, count*sizeof(DIM_NODE));
		}
	}
	SensorCall(239);delete[] metric;
	metric = m1;
//
	metricGoNode(activenode = metricNodeB(_contur, _node));
//
	{int  ReplaceReturn130 = activenode; SensorCall(240); return ReplaceReturn130;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricConturDivide(int _contur, int _node)
{
	SensorCall(241);if((_contur < 0) || (_contur >= metric->ContursCount))
		{/*123*/{int  ReplaceReturn129 = activenode; SensorCall(242); return ReplaceReturn129;}/*124*/}

	SensorCall(243);DIM_CONTUR *dc,*dc1;
	dc = (DIM_CONTUR*)(metric + 1);
	SensorCall(245);if((_node <= 0) || (_node >= dc[_contur].NodesCount))
		{/*125*/{int  ReplaceReturn128 = activenode; SensorCall(244); return ReplaceReturn128;}/*126*/}

	SensorCall(246);int n   = NDim__NodesCount(metric),
	     len = NDim__MetricLen(metric);
	len += sizeof(DIM_CONTUR);
	DIM_METRIC *m1 = (DIM_METRIC*)new char[len];
	memset(m1, 0, len);
	memcpy(m1, metric, sizeof(DIM_METRIC));
	m1->ContursCount++;
	dc1 = (DIM_CONTUR*)(m1 + 1);
	memcpy(dc1,dc,(_contur+1)*sizeof(DIM_CONTUR));
	memcpy(dc1+_contur+1,dc+_contur,(metric->ContursCount-_contur)*sizeof(DIM_CONTUR));
	dc1[_contur  ].NodesCount  = _node;
	dc1[_contur+1].NodesCount -= _node;
	memcpy(dc1+m1->ContursCount,dc+metric->ContursCount,n*sizeof(DIM_NODE));

	delete[] metric;
	metric = m1;

	SensorCall(248);if (activecontur == _contur)
		{/*127*/SensorCall(247);activecontur = _contur + 1;/*128*/}

	{int  ReplaceReturn127 = activenode; SensorCall(249); return ReplaceReturn127;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricConturMerge(int _contur)
{
	SensorCall(250);if((_contur < 1) || (_contur >= metric->ContursCount))
		{/*129*/{int  ReplaceReturn126 = activenode; SensorCall(251); return ReplaceReturn126;}/*130*/}

	SensorCall(252);int n = NDim__NodesCount(metric);
	DIM_CONTUR *dc = (DIM_CONTUR*)(metric + 1);
	metric->ContursCount--;
	dc[_contur-1].NodesCount += dc[_contur].NodesCount;
	memmove(dc+_contur,dc+_contur+1,(metric->ContursCount-_contur)*sizeof(DIM_CONTUR));
	memmove(dc+metric->ContursCount,dc+metric->ContursCount+1,n*sizeof(DIM_NODE));

	SensorCall(254);if (activecontur == _contur)
		{/*131*/SensorCall(253);activecontur = _contur - 1;/*132*/}

	{int  ReplaceReturn125 = activenode; SensorCall(255); return ReplaceReturn125;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricGoNearest(double _x, double _y)
{
	SensorCall(256);if(under(_x, _y) != -1)
		{/*133*/{int  ReplaceReturn124 = activenode; SensorCall(257); return ReplaceReturn124;}/*134*/}
//
	SensorCall(258);int n = NDim__NodesCount(metric),
	     n1 = -1;
	DIM_NODE *node = NDim__Nodes(metric);
	double r = 0;
	SensorCall(266);for(int a = 0; a < n; a++, node++)
	{
		SensorCall(259);double x = node->x,
		       y = node->y;
		dim2pix(x, y);
		SensorCall(261);if(!insidescreen(x, y))
			{/*135*/SensorCall(260);continue;/*136*/}
		SensorCall(265);if(n1 == -1)
		{
			SensorCall(262);n1 = a;
			r = hypot(x - _x, y - _y);
		}
		else {/*137*/SensorCall(263);if(hypot(x - _x, y - _y) < r)
		{
			SensorCall(264);n1 = a;
			r = hypot(x - _x, y - _y);
		;/*138*/}}
	}
//
	{int  ReplaceReturn123 = metricGoNode(n1); SensorCall(267); return ReplaceReturn123;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricGoNode(int _node)
{
	SensorCall(268);DIM_NODE *node = metricNodeA(_node);
	SensorCall(270);if(!node)
		{/*139*/{int  ReplaceReturn122 = activenode; SensorCall(269); return ReplaceReturn122;}/*140*/}
//
	SensorCall(271);double x = node->x,
	       y = node->y;
	dim2pix(x, y);
	SensorCall(273);if(insidescreen(x, y))
	{
		SensorCall(272);mouseposset(x, y);
		activenode = _node;
	}
//
	{int  ReplaceReturn121 = activenode; SensorCall(274); return ReplaceReturn121;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricNodeKeyMove(int _node, int _mode)
{
	SensorCall(275);if(!metric)
		{/*141*/{int  ReplaceReturn120 = 1; SensorCall(276); return ReplaceReturn120;}/*142*/}
//
	SensorCall(300);switch(mode)
	{
		case vmMetricNone:
		{
			SensorCall(277);double x = 0,
			       y = 0;
			mouseposget(x, y);
			under(x, y);
			SensorCall(280);if(activenode == -1)
			{
				SensorCall(278);metricGoNearest(x, y);
				SensorCall(279);break;
			}
//
			SensorCall(281);double x0,
			       y0,
			       x1,
			       y1,
			       dx = 0.0,
			       dy = 0.0,
			       dx0 = 0.0,
			       dy0 = 0.0;
			SensorCall(291);switch(_mode)
			{
				case meMoveUp:
					SensorCall(282);dy0 = 1.0;
					SensorCall(283);break;
				case meMoveDown:
					SensorCall(284);dy0 = -1.0;
					SensorCall(285);break;
				case meMoveLeft:
					SensorCall(286);dx0 = -1.0;
					SensorCall(287);break;
				case meMoveRight:
					SensorCall(288);dx0 = 1.0;
					SensorCall(289);break;
				default:
					SensorCall(290);break;
			}
			SensorCall(293);if(!dx0 && !dy0)
				{/*143*/SensorCall(292);break;/*144*/}
//
			SensorCall(294);hide();
//
			DIM_NODE *n0 = metricNodeA(activenode),
			          n1;
			xy0.x = x;
			xy0.y = y;
			SensorCall(296);do
			{
				SensorCall(295);memcpy(&n1, n0, sizeof(n1));
//
				dx += dx0;
				dy += dy0;
//
				x0 = xy0.x,
				y0 = xy0.y,
				x1 = x0 + dx,
				y1 = y0 + dy;
//
				pix2dim(x0, y0);
				pix2dim(x1, y1);
//
				dxy.x = x1 - x0;
				dxy.y = y1 - y0;
//
				setxy(n1, n1.x + dxy.x, n1.y + dxy.y);
			}
			while((n1.x == n0->x) && (n1.y == n0->y));
//
			SensorCall(297);mode = vmMetricMove;
			metricCorrect(metric);
			mode = vmMetricNone;
//
			metricGoNode(activenode);
//
			draw();
			SensorCall(298);break;
		}
		case vmMetricNewLine:
		case vmMetricRotate:
		case vmMetricScale:
			SensorCall(299);break;
	}
//
	SensorCall(301);showstatus();
	{int  ReplaceReturn119 = 0; SensorCall(302); return ReplaceReturn119;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricContur(int _node)
{
	SensorCall(303);int n1 = NDim__NodesCount(metric);
	SensorCall(305);if((_node < 0) || (_node >= n1))
		{/*145*/{int  ReplaceReturn118 = -1; SensorCall(304); return ReplaceReturn118;}/*146*/}
//
	SensorCall(306);DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
	int        a = 0;
	SensorCall(310);for(; a < metric->ContursCount; a++, contur++)
		{/*147*/SensorCall(307);if(_node >= contur->NodesCount)
			{/*149*/SensorCall(308);_node -= contur->NodesCount;/*150*/}
		else
			{/*151*/SensorCall(309);break;/*152*/}/*148*/}
//
	{int  ReplaceReturn117 = a; SensorCall(311); return ReplaceReturn117;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricConturNode(int _node)
{
	SensorCall(312);int n1 = NDim__NodesCount(metric);
	SensorCall(314);if((_node < 0) || (_node >= n1))
		{/*153*/{int  ReplaceReturn116 = -1; SensorCall(313); return ReplaceReturn116;}/*154*/}
//
	SensorCall(315);DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
	SensorCall(319);for(int a = 0; a < metric->ContursCount; a++, contur++)
		{/*155*/SensorCall(316);if(_node >= contur->NodesCount)
			{/*157*/SensorCall(317);_node -= contur->NodesCount;/*158*/}
		else
			{/*159*/SensorCall(318);break;/*160*/}/*156*/}
//
	{int  ReplaceReturn115 = _node; SensorCall(320); return ReplaceReturn115;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricConturOpen(int _contur)
{
	SensorCall(321);if(!metric)
		{/*161*/{int  ReplaceReturn114 = 1; SensorCall(322); return ReplaceReturn114;}/*162*/}
	SensorCall(324);if(metric->ContursCount < 1)
		{/*163*/{int  ReplaceReturn113 = 0; SensorCall(323); return ReplaceReturn113;}/*164*/}
	SensorCall(326);if(_contur >= metric->ContursCount)
		{/*165*/{int  ReplaceReturn112 = 2; SensorCall(325); return ReplaceReturn112;}/*166*/}
//
	SensorCall(327);DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
	DIM_NODE   *nodes = NDim__Nodes(metric);
	int        count = NDim__NodesCount(metric);
	SensorCall(335);for(int a = 0; a < metric->ContursCount; a++, contur++)
	{
		SensorCall(328);int n = contur->NodesCount;
		SensorCall(331);if((_contur < 0) || (a == _contur))
			{/*167*/SensorCall(329);while(ItClosed(nodes, n))
				{/*169*/SensorCall(330);n--;/*170*/}/*168*/}
		SensorCall(333);if(contur->NodesCount > n)
			{/*171*/SensorCall(332);memmove(
				nodes + n,
				nodes + contur->NodesCount,
				sizeof(*nodes)*(count - contur->NodesCount));/*172*/}
		SensorCall(334);nodes += n;
		count -= contur->NodesCount;
		contur->NodesCount = n;
	}
//
	{int  ReplaceReturn111 = 0; SensorCall(336); return ReplaceReturn111;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricConturClose(int _contur)
{
	SensorCall(337);if(!metric)
		{/*173*/{int  ReplaceReturn110 = 1; SensorCall(338); return ReplaceReturn110;}/*174*/}
	SensorCall(340);if(metric->ContursCount < 1)
		{/*175*/{int  ReplaceReturn109 = 0; SensorCall(339); return ReplaceReturn109;}/*176*/}
	SensorCall(342);if(_contur >= metric->ContursCount)
		{/*177*/{int  ReplaceReturn108 = 2; SensorCall(341); return ReplaceReturn108;}/*178*/}
//
	SensorCall(343);int len = NDim__MetricLen(metric);
	len += sizeof(DIM_NODE)*metric->ContursCount;
	DIM_METRIC *m1 = (DIM_METRIC*)new char[len];
	memset(m1, 0, len);
	memcpy(m1, metric, sizeof(*m1));
//
	DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1),
	           *c1 = (DIM_CONTUR*)(m1 + 1);
	DIM_NODE   *nodes = (DIM_NODE*)(contur + metric->ContursCount),
	           *n1 = (DIM_NODE*)(c1 + m1->ContursCount);
//
	SensorCall(349);for(int a = 0; a < metric->ContursCount; a++, contur++, c1++)
	{
		SensorCall(344);memcpy(c1, contur, sizeof(*c1));
		memcpy(n1, nodes, c1->NodesCount*sizeof(*n1));
		SensorCall(347);if((_contur < 0) || (_contur == a))
			{/*179*/SensorCall(345);if(!ItClosed(n1, c1->NodesCount))
				{/*181*/SensorCall(346);memcpy(n1 + c1->NodesCount++, n1, sizeof(*n1));/*182*/}/*180*/}
		SensorCall(348);nodes += contur->NodesCount;
		n1 += c1->NodesCount;
	}
//
	SensorCall(350);delete[] metric;
	metric = m1;
//
	{int  ReplaceReturn107 = 0; SensorCall(351); return ReplaceReturn107;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricConturInvert(int _contur)
{
	SensorCall(352);if(!metric)
		{/*183*/{int  ReplaceReturn106 = 1; SensorCall(353); return ReplaceReturn106;}/*184*/}
	SensorCall(355);if(metric->ContursCount < 1)
		{/*185*/{int  ReplaceReturn105 = 0; SensorCall(354); return ReplaceReturn105;}/*186*/}
	SensorCall(357);if(_contur >= metric->ContursCount)
		{/*187*/{int  ReplaceReturn104 = 2; SensorCall(356); return ReplaceReturn104;}/*188*/}
//
	SensorCall(358);int len = NDim__MetricLen(metric);
	DIM_METRIC *m1 = (DIM_METRIC*)new char[len];
	memcpy(m1, metric, len);
	DIM_CONTUR *c1 = (DIM_CONTUR*)(m1 + 1);
	DIM_NODE   *nodes = NDim__Nodes(metric),
	           *n1 = NDim__Nodes(m1);
	SensorCall(371);for(int b = 0; b < m1->ContursCount; b++)
	{
		SensorCall(359);int n = c1->NodesCount;
		SensorCall(361);if(ItClosed(n1, n))
			{/*189*/SensorCall(360);memcpy(n1 + (n - 1), n1, sizeof(DIM_NODE));/*190*/}
		SensorCall(369);if((_contur < 0) || (_contur == b))
			{/*191*/SensorCall(362);for(int a = 0, a1 = n - 1; a < n; a++, a1--)
			{
				SensorCall(363);memcpy(nodes + a, n1 + a1, sizeof(DIM_NODE));
				SensorCall(367);if (a)
					{
					SensorCall(364);if(n1[a1+1].Flags & DIM_NODE::dnPenUp) // EE - 09-Sep-04  10:48:47 - a1+1
						{/*193*/SensorCall(365);nodes[a].Flags |=  DIM_NODE::dnPenUp;/*194*/}
					else
						{/*195*/SensorCall(366);nodes[a].Flags &= ~DIM_NODE::dnPenUp;/*196*/}
					}
				SensorCall(368);nodes[a].dXIn  = -n1[a1].dXOut;
				nodes[a].dXOut = -n1[a1].dXIn;
				nodes[a].dYIn  = -n1[a1].dYOut;
				nodes[a].dYOut = -n1[a1].dYIn;
				nodes[a].dZIn  = -n1[a1].dZOut;
				nodes[a].dZOut = -n1[a1].dZIn;
			;/*192*/}}
		SensorCall(370);nodes += n;
		n1 += n;
		c1++;
	}
	SensorCall(372);delete[] m1;
//
	{int  ReplaceReturn103 = 0; SensorCall(373); return ReplaceReturn103;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricGetFlag(int *_flag)
{
SensorCall(374);int rc=1;
double x=0,y=0;
int uc,an;
DIM_NODE *node;

SensorCall(376);if (!metric)
	{/*197*/SensorCall(375);return(rc);/*198*/}

SensorCall(385);switch(mode)
	{
	case vmMetricNone :
		SensorCall(377);mouseposget(x, y);
		under(x, y);
		SensorCall(379);if (activenode >= 0)
			{
			SensorCall(378);node = metricNodeA(activenode);
			*_flag = node->Flags;
			rc = 0;
			}
		SensorCall(380);break;
	case vmMetricNewLine :
		SensorCall(381);uc = ((DIM_CONTUR*)(metric + 1) + activecontur)->NodesCount - 1;
		an = metricNodeB(activecontur,max2(0,uc));
		SensorCall(383);if (an >= 0)
			{
			SensorCall(382);node = metricNodeA(an);
			*_flag = node->Flags;
			rc = 0;
			}
		SensorCall(384);break;
	}
SensorCall(386);return(rc);
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricSetFlag(int _flag,bool _state)
{
SensorCall(387);double x=0,y=0;
int i;
int uc,an;
DIM_NODE *node;

SensorCall(389);if (!metric)
	{/*199*/SensorCall(388);return(1);/*200*/}

SensorCall(414);switch(mode)
	{
	case vmMetricNone :
		SensorCall(390);mouseposget(x, y);
		under(x, y);
		SensorCall(404);if (activenode == -1)
			{/*201*/SensorCall(391);metricGoNearest(x, y);/*202*/}
		else
			{
			SensorCall(392);hide();
			node = metricNodeA(activenode);
			SensorCall(402);if (node->Flags & DIM_NODE::dnSelected)
				{
				SensorCall(393);node = NDim__Nodes(metric);
				uc   = NDim__NodesCount(metric);
				SensorCall(398);for (i=0; i<uc; i++)
					{
					SensorCall(394);if (node[i].Flags & DIM_NODE::dnSelected)
						{
						SensorCall(395);if (_state)
							{/*203*/SensorCall(396);node[i].Flags |=  _flag;/*204*/}
						else
							{/*205*/SensorCall(397);node[i].Flags &= ~_flag;/*206*/}
						}
					}
				}
			else
				{
				SensorCall(399);if (_state)
					{/*207*/SensorCall(400);node->Flags |=  _flag;/*208*/}
				else
					{/*209*/SensorCall(401);node->Flags &= ~_flag;/*210*/}
				}
			SensorCall(403);draw();
			}
		SensorCall(405);break;
	case vmMetricNewLine :
		SensorCall(406);uc = ((DIM_CONTUR*)(metric + 1) + activecontur)->NodesCount - 1;
		an = metricNodeB(activecontur,max2(0,uc));
		SensorCall(412);if (an == -1)
			;
		else
			{
			SensorCall(407);hide();
			node = metricNodeA(an);
			SensorCall(410);if (_state)
				{/*213*/SensorCall(408);node->Flags |=  _flag;/*214*/}
			else
				{/*215*/SensorCall(409);node->Flags &= ~_flag;/*216*/}
			SensorCall(411);draw();
			}
		SensorCall(413);break;

	}
SensorCall(415);return(0);
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricInputContinue(int _contur)
{
	SensorCall(416);metricPassivate();
//
	hide();
//
	SensorCall(421);if((_contur < 0) || (_contur >= metric->ContursCount))
	{
		SensorCall(417);activecontur = metric->ContursCount;
		activenode = -1;
	}
	else
	{
		SensorCall(418);metricConturOpen(_contur);
//
		activecontur = _contur;
		activenode = ((DIM_CONTUR*)(metric + 1) + activecontur)->NodesCount;
		SensorCall(420);if(activenode > 0)
		{
			SensorCall(419);double x = 0,
			       y = 0;
			mouseposget(x, y);
			metricNodeInsertB(activecontur, activenode, x, y);
		}
	}
//
	SensorCall(422);mode = vmMetricNewLine;
//
	draw();
//
	{int  ReplaceReturn102 = 0; SensorCall(423); return ReplaceReturn102;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricMovingStart(double _x, double _y)
{
	SensorCall(424);activecontur = -1;
	mode = vmMetricMove;
	xy0.x = _x;
	xy0.y = _y;
	dxy.x = 0;
	dxy.y = 0;
//
	{int  ReplaceReturn101 = 0; SensorCall(425); return ReplaceReturn101;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricMovingContinue(double _x, double _y)
{
	SensorCall(426);if(mode != vmMetricMove)
		{/*217*/{int  ReplaceReturn100 = 1; SensorCall(427); return ReplaceReturn100;}/*218*/}
//
	SensorCall(428);hide();
//
	double x0 = xy0.x,
	       y0 = xy0.y,
	       x1 = _x,
	       y1 = _y;
	pix2dim(x0, y0);
	pix2dim(x1, y1);
//
	dxy.x = x1 - x0;
	dxy.y = y1 - y0;
//
	draw();
//
	{int  ReplaceReturn99 = 0; SensorCall(429); return ReplaceReturn99;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::deriveMovingStart(double _x, double _y)
{
	SensorCall(430);mode = vmMetricDerive;
	xy0.x = _x;
	xy0.y = _y;
//
	{int  ReplaceReturn98 = 0; SensorCall(431); return ReplaceReturn98;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::deriveMovingContinue(double _x, double _y)
{
SensorCall(432);double x,y,dx,dy;
int dn,is,ie;

	SensorCall(434);if(mode != vmMetricDerive)
		{/*219*/{int  ReplaceReturn97 = 1; SensorCall(433); return ReplaceReturn97;}/*220*/}
//
	SensorCall(435);hide();
//
	DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
	DIM_NODE *nodes = NDim__Nodes(metric);
	dn = abs(derivenode) - 1;
	x = _x;
	y = _y;
	pix2dim(x,y);
	SensorCall(438);if (derivenode < 0)
		{
		SensorCall(436);dx = (nodes[dn].x - x) * dFactor;
		dy = (nodes[dn].y - y) * dFactor;
		nodes[dn].dXIn = dx;
		nodes[dn].dYIn = dy;
		}
	else
		{
		SensorCall(437);dx = (x - nodes[dn].x) * dFactor;
		dy = (y - nodes[dn].y) * dFactor;
		nodes[dn].dXOut = dx;
		nodes[dn].dYOut = dy;
		}

	SensorCall(439);is = 0;
	SensorCall(449);for (int b = 0; b < metric->ContursCount; b++)
		{
		SensorCall(440);ie = is + contur->NodesCount - 1;
		SensorCall(447);if ((dn >= is) && (dn <= ie))
			{
			SensorCall(441);if (ItClosed(nodes+is, contur->NodesCount))
				{
				SensorCall(442);if (dn == is)
					{
					SensorCall(443);nodes[ie].dXIn  = nodes[is].dXIn;
					nodes[ie].dYIn  = nodes[is].dYIn;
					nodes[ie].dXOut = nodes[is].dXOut;
					nodes[ie].dYOut = nodes[is].dYOut;
					}
				else
					{
					SensorCall(444);if (dn == ie)
						{
						SensorCall(445);nodes[is].dXIn  = nodes[ie].dXIn;
						nodes[is].dYIn  = nodes[ie].dYIn;
						nodes[is].dXOut = nodes[ie].dXOut;
						nodes[is].dYOut = nodes[ie].dYOut;
						}
					}
				}
			SensorCall(446);break;
			}
		SensorCall(448);is += contur->NodesCount;
		contur ++;
		}
//
	SensorCall(450);draw();
//
	{int  ReplaceReturn96 = 0; SensorCall(451); return ReplaceReturn96;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricSelectionStart(double _x, double _y)
{
	SensorCall(452);activecontur = -1;
	activenode = -1;
	mode = vmMetricSelect;
	xy0.x = _x;
	xy0.y = _y;
	xy1.x = _x;
	xy1.y = _y;
//
	drawrect();
//
	{int  ReplaceReturn95 = 0; SensorCall(453); return ReplaceReturn95;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricSelectionContinue(double _x, double _y)
{
	SensorCall(454);hiderect();
//
	xy1.x = _x;
	xy1.y = _y;
//
	drawrect();
//
	{int  ReplaceReturn94 = 0; SensorCall(455); return ReplaceReturn94;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricRotateStart(double _x, double _y)
{
	SensorCall(456);activecontur = -1;
	activenode = -1;
	rotangle = 0.0;
	mode = vmMetricRotate;
	xy0.x = _x;
	xy0.y = _y;
	xy1.x = _x;
	xy1.y = _y;
//
	{int  ReplaceReturn93 = 0; SensorCall(457); return ReplaceReturn93;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricRotateContinue(double _x, double _y)
{
	SensorCall(458);if(mode != vmMetricRotate)
		{/*221*/{int  ReplaceReturn92 = 1; SensorCall(459); return ReplaceReturn92;}/*222*/}
//
	SensorCall(460);hide();
//
	xy1.x = _x;
	xy1.y = _y;
//
	double cx = rotcenter.x;
	double cy = rotcenter.y;
	dim2pix(cx, cy);
	rotangle = 0.0;
	dxy.x = xy1.x - cx;
	dxy.y = xy1.y - cy;
	SensorCall(462);if ((dxy.x != 0.0) || (dxy.y != 0.0))
		{/*223*/SensorCall(461);rotangle = atan2(dxy.y, dxy.x);/*224*/}
	SensorCall(463);dxy.x = xy0.x - cx;
	dxy.y = xy0.y - cy;
	SensorCall(465);if ((dxy.x != 0.0) || (dxy.y != 0.0))
		{/*225*/SensorCall(464);rotangle -= atan2(dxy.y, dxy.x);/*226*/}
//
	SensorCall(466);draw();
//
	{int  ReplaceReturn91 = 0; SensorCall(467); return ReplaceReturn91;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricPassivate()
{
	SensorCall(468);if(mode == vmMetricNone)
		{/*227*/{int  ReplaceReturn90 = 0; SensorCall(469); return ReplaceReturn90;}/*228*/}
//
	SensorCall(470);hide();
//
	SensorCall(476);if((mode == vmMetricNewLine) && !(activenode < 0))
	{
		SensorCall(471);int contur = metricContur(activenode);
		DIM_CONTUR *c0 = (DIM_CONTUR*)(metric + 1) + contur;
		SensorCall(475);if(c0->NodesCount < 2)
		{
			SensorCall(472);if(c0->NodesCount > 0)
			{
				SensorCall(473);DIM_NODE *n0 = NDim__Nodes(metric) + activenode;
				int nn = NDim__NodesCount(metric);
				memmove(n0, n0 + 1, (nn - activenode - 1)*sizeof(*n0));
				c0->NodesCount = 0;
			}
			SensorCall(474);int lc = (metric->ContursCount - contur - 1)*sizeof(DIM_CONTUR),
			     ln = NDim__NodesCount(metric)*sizeof(DIM_NODE);
			memmove(c0, c0 + 1, lc + ln);
			metric->ContursCount--;
			activenode = -1;
		}
	}
//
	SensorCall(477);metricCorrect(metric);
	SensorCall(479);if(mode == vmMetricSelect)
	{
		SensorCall(478);hiderect();
	}
//
	SensorCall(480);activecontur = -1;
	mode = vmMetricNone;
//
	draw();
//
	{int  ReplaceReturn89 = 0; SensorCall(481); return ReplaceReturn89;}
}
//---------------------------------------------------------------
void TDgtMetricEditor::hide()
{
	SensorCall(482);draw();
SensorCall(483);}
//---------------------------------------------------------------
void TDgtMetricEditor::draw()
{
	SensorCall(484);if(dim)
	{
		SensorCall(485);DIM_OBJECT *dim1 = AllocDim();
		draw(dim1);
		delete[] dim1;
	}
	else {/*229*/SensorCall(486);if(part || type)
	{
		SensorCall(487);DIM_OBJECT *dim1 = NDim__DimNew();
		DIM_METRIC *m1 = NDim__MetricCopy(metric);
		metricCorrect(m1);
		NDim__MetricSet(dim1, m1);
		delete[] m1;
		draw(dim1);
		delete[] dim1;
	;/*230*/}}
//	else if(part)
//	{
//		RULE_PART *rp = AllocPart();
//		draw(rp);
//		delete[] rp;
//	}
//	else if(type)
//	{
//		RULE_PART_TYPE *rt = AllocType();
//		draw(rt);
//		delete[] rt;
//	}
SensorCall(488);}
//---------------------------------------------------------------
void TDgtMetricEditor::hiderect()
{
	SensorCall(489);drawrect();
SensorCall(490);}
//---------------------------------------------------------------
void TDgtMetricEditor::drawrect()
{
	SensorCall(491);drawrect(xy0.x, xy0.y, xy1.x, xy1.y);
SensorCall(492);}
//---------------------------------------------------------------
int TDgtMetricEditor::InitDim(DIM_OBJECT *_dim, DIM_HEAD *_dh)
{
	SensorCall(493);setxymode = meXYFloat;
//
	hide();
//
	delete[] isx_metric;
	isx_metric = NULL;
	delete[] metric;
	metric = NULL;
//
	delete[] dim;
	dim = NULL;
	delete[] part;
	part = NULL;
	delete[] type;
	type = NULL;
//
	mode = vmMetricNone;
	activecontur = -1;
	activenode   = -1;
	derivenode   = 0;
	curr_xy.x    = 0.0;
	curr_xy.y    = 0.0;
	x_fixed      = 0;
	y_fixed      = 0;
	snap_xy.x    = 0.0;
	snap_xy.y    = 0.0;
	snapmode     = 0;
	snapped      = 0;
//
	SensorCall(495);if(!_dim)
		{/*231*/{int  ReplaceReturn88 = 1; SensorCall(494); return ReplaceReturn88;}/*232*/}
/* EE - 27-Oct-06  17:17:48 - A zachem Long a ne Float ? Pust' ostanetsa Float
	if(_dh)
		if(!(_dh->Flags & DIM_HEAD::dhBL))
			setxymode = meXYLong;
*/
	SensorCall(496);dim = NDim__DimCopy(_dim);
	NDim__2DIM_NODE(dim);
//
	DIM_METRIC *metric1 = NDim__Metric(dim);
	SensorCall(503);if(!metric1)
	{
		SensorCall(497);metric = (DIM_METRIC*)new char[sizeof(DIM_METRIC)];
		memset(metric, 0, sizeof(DIM_METRIC));
		metric->Flags = DIM_METRIC::xyDIMNode;
	}
	else
	{
		SensorCall(498);metric = NDim__MetricCopy(metric1);
		SensorCall(502);if(setxymode == meXYLong)
		{
			SensorCall(499);int n = NDim__NodesCount(metric);
			DIM_NODE *nn = NDim__Nodes(metric);
			SensorCall(501);for(int a = 0; a < n; a++)
				{/*233*/SensorCall(500);setxy(nn[a], nn[a].x, nn[a].y);/*234*/}
		}
	}
	SensorCall(504);isx_metric = NDim__MetricCopy(metric);
//
	draw();
//
	{int  ReplaceReturn87 = 0; SensorCall(505); return ReplaceReturn87;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::InitPart(RULE_PART *_rp)
{
	SensorCall(506);setxymode = meXYFloat;
//
	hide();
//
	delete[] isx_metric;
	isx_metric = NULL;
	delete[] metric;
	metric = NULL;
//
	delete[] dim;
	dim = NULL;
	delete[] part;
	part = NULL;
	delete[] type;
	type = NULL;
//
	mode = vmMetricNone;
	activecontur = -1;
	activenode = -1;
//
	SensorCall(508);if(!_rp)
		{/*235*/{int  ReplaceReturn86 = 1; SensorCall(507); return ReplaceReturn86;}/*236*/}
//
	SensorCall(509);setxymode = meXYShort;
	int len = NRul__PartLen(*_rp);
	part = (RULE_PART*)new char[len];
	memcpy(part, _rp, len);
//
	int n = NAno__NodesCount((RULE_PART_CONTUR*)(part + 1), part->PartType.ContursCount),
	     ml = sizeof(DIM_METRIC) + sizeof(DIM_CONTUR)*part->PartType.ContursCount + sizeof(DIM_NODE)*n;
	metric = (DIM_METRIC*)new char[ml];
	memset(metric, 0, ml);
	metric->Flags = DIM_METRIC::xyDIMNode;
	metric->ContursCount = part->PartType.ContursCount;
	DIM_CONTUR *dc = (DIM_CONTUR*)(metric + 1);
	RULE_PART_CONTUR *rc = (RULE_PART_CONTUR*)(part + 1);
	SensorCall(511);for(int a = 0; a < metric->ContursCount; a++)
		{/*237*/SensorCall(510);dc[a].NodesCount = rc[a].NodesCount;/*238*/}
	SensorCall(512);XY *xy = (XY*)(rc + metric->ContursCount);
	DIM_NODE *node = (DIM_NODE*)(dc + metric->ContursCount);
	SensorCall(514);for(int a = 0; a < n; a++)
	{
		SensorCall(513);node[a].x = xy[a].x;
		node[a].y = xy[a].y;
	}
	SensorCall(515);isx_metric = NDim__MetricCopy(metric);
//
	draw();
//
	{int  ReplaceReturn85 = 0; SensorCall(516); return ReplaceReturn85;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::InitType(RULE_PART_TYPE *_rt)
{
	SensorCall(517);setxymode = meXYFloat;
//
	hide();
//
	delete[] metric;
	metric = NULL;
//
	delete[] dim;
	dim = NULL;
	delete[] part;
	part = NULL;
	delete[] type;
	type = NULL;
//
	mode = vmMetricNone;
	activecontur = -1;
	activenode = -1;
//
	SensorCall(519);if(!_rt)
		{/*239*/{int  ReplaceReturn84 = 1; SensorCall(518); return ReplaceReturn84;}/*240*/}
//
	SensorCall(520);setxymode = meXYShort;
	int len = NRul__TypeLen(*_rt);
	type = (RULE_PART_TYPE*)new char[len];
	memcpy(type, _rt, len);
//
	int n = NAno__NodesCount((RULE_PART_CONTUR*)(type + 1), type->ContursCount),
	     ml = sizeof(DIM_METRIC) + sizeof(DIM_CONTUR)*type->ContursCount + sizeof(DIM_NODE)*n;
	metric = (DIM_METRIC*)new char[ml];
	memset(metric, 0, ml);
	metric->Flags = DIM_METRIC::xyDIMNode;
	metric->ContursCount = type->ContursCount;
	DIM_CONTUR *dc = (DIM_CONTUR*)(metric + 1);
	RULE_PART_CONTUR *rc = (RULE_PART_CONTUR*)(type + 1);
	SensorCall(522);for(int a = 0; a < metric->ContursCount; a++)
		{/*241*/SensorCall(521);dc[a].NodesCount = rc[a].NodesCount;/*242*/}
	SensorCall(523);XY *xy = (XY*)(rc + metric->ContursCount);
	DIM_NODE *node = (DIM_NODE*)(dc + metric->ContursCount);
	SensorCall(525);for(int a = 0; a < n; a++)
	{
		SensorCall(524);node[a].x = xy[a].x;
		node[a].y = xy[a].y;
	}
//
	SensorCall(526);draw();
//
	{int  ReplaceReturn83 = 0; SensorCall(527); return ReplaceReturn83;}
}
//---------------------------------------------------------------------------
DIM_OBJECT *TDgtMetricEditor::AllocDim()
{
	SensorCall(528);if(!metric || !dim)
		return NULL;
//
	SensorCall(529);DIM_OBJECT *dim1 = NDim__DimCopy(dim);
//
	DIM_METRIC *m1 = NDim__MetricCopy(metric);
	metricCorrect(m1);
	NDim__MetricSet(dim1, m1);
	delete[] m1;
//
	{__IASTRUCT__ DIM_OBJECT * ReplaceReturn82 = dim1; SensorCall(530); return ReplaceReturn82;}
}
//---------------------------------------------------------------------------
RULE_PART *TDgtMetricEditor::AllocPart()
{
	SensorCall(531);if(!metric || !part)
		return NULL;
//
	SensorCall(532);int len = NRul__PartLen(*part);
	RULE_PART *rp = (RULE_PART*)new char[len];
	memcpy(rp, part, len);
//
	DIM_METRIC *m1 = NDim__MetricCopy(metric);
	metricCorrect(m1);
//
	int n = NAno__NodesCount((DIM_CONTUR*)(m1 + 1), m1->ContursCount);
	int *conturs = new int[m1->ContursCount];
	SensorCall(534);for(int a = 0; a < m1->ContursCount; a++)
		{/*243*/SensorCall(533);conturs[a] = ((DIM_CONTUR*)(m1 + 1) + a)->NodesCount;/*244*/}
	SensorCall(535);XY *xy = new XY[n];
	DIM_NODE *node = (DIM_NODE*)((DIM_CONTUR*)(m1 + 1) + m1->ContursCount);
	SensorCall(537);for(int a = 0; a < n; a++)
	{
		SensorCall(536);xy[a].x = node[a].x;
		xy[a].y = node[a].y;
	}
//
	SensorCall(538);int rs = NRul__PartMetricSet(rp, xy, conturs, m1->ContursCount);
//
	delete[] conturs;
	delete[] xy;
//
	delete[] m1;
//
	SensorCall(541);if(rs)
	{
		SensorCall(539);delete[] rp;
		{__IASTRUCT__ RULE_PART * ReplaceReturn81 = NULL; SensorCall(540); return ReplaceReturn81;}
	}
//
	{__IASTRUCT__ RULE_PART * ReplaceReturn80 = rp; SensorCall(542); return ReplaceReturn80;}
}
//---------------------------------------------------------------------------
RULE_PART_TYPE *TDgtMetricEditor::AllocType()
{
	SensorCall(543);if(!metric || ! type)
		return NULL;
//
	SensorCall(544);int len = NRul__TypeLen(*type);
	RULE_PART_TYPE *rt = (RULE_PART_TYPE*)new char[len];
	memcpy(rt, type, len);
//
	DIM_METRIC *m1 = NDim__MetricCopy(metric);
	metricCorrect(m1);
//
	int n = NAno__NodesCount((DIM_CONTUR*)(m1 + 1), m1->ContursCount);
	int *conturs = new int[m1->ContursCount];
	SensorCall(546);for(int a = 0; a < m1->ContursCount; a++)
		{/*245*/SensorCall(545);conturs[a] = ((DIM_CONTUR*)(m1 + 1) + a)->NodesCount;/*246*/}
	SensorCall(547);XY *xy = new XY[n];
	DIM_NODE *node = (DIM_NODE*)((DIM_CONTUR*)(m1 + 1) + m1->ContursCount);
	SensorCall(549);for(int a = 0; a < n; a++)
	{
		SensorCall(548);xy[a].x = node[a].x;
		xy[a].y = node[a].y;
	}
//
	SensorCall(550);int rs = NRul__TypeMetricSet(rt, xy, conturs, m1->ContursCount);
//
	delete[] conturs;
	delete[] xy;
//
	delete[] m1;
//
	SensorCall(553);if(rs)
	{
		SensorCall(551);delete[] rt;
		{__IASTRUCT__ RULE_PART_TYPE * ReplaceReturn79 = NULL; SensorCall(552); return ReplaceReturn79;}
	}
//
	{__IASTRUCT__ RULE_PART_TYPE * ReplaceReturn78 = rt; SensorCall(554); return ReplaceReturn78;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::MouseDown(double _x, double _y, int _shift)
{
	SensorCall(555);if(!metric)
		{/*247*/{int  ReplaceReturn77 = 1; SensorCall(556); return ReplaceReturn77;}/*248*/}

	SensorCall(562);switch(mode)
		{
		case vmMetricMove:
		case vmMetricDerive:
		case vmMetricNewLine:
			SensorCall(557);if (x_fixed)
				{/*249*/SensorCall(558);_x = curr_xy.x;/*250*/}
			SensorCall(560);if (y_fixed)
				{/*251*/SensorCall(559);_y = curr_xy.y;/*252*/}
			SensorCall(561);break;
		}
	SensorCall(563);curr_xy.x = _x;
	curr_xy.y = _y;

	mouserectset(vmClipControl);
	int snap = 0;
	SensorCall(615);switch(mode)
	{
		case vmMetricNone:
		{
			SensorCall(564);if(metric->ContursCount == 0)
			{
				SensorCall(565);metricInputContinue(0);
				ID_OBJECT snapobj = {0, 0};
				SensorCall(567);if (snapmode)
					{/*253*/SensorCall(566);snap = snappos(_x, _y, snapobj);/*254*/}
				SensorCall(568);hide();
				SensorCall(570);if (snapmode)
				{
					SensorCall(569);snapped = 0; //snap;
					memcpy(&snap_object,&snapobj,sizeof(ID_OBJECT));
					snap_xy.x = _x;
					snap_xy.y = _y;
					pix2dim(snap_xy.x, snap_xy.y);
				}
				SensorCall(571);metricNodeInsertC(_x, _y);
				draw();
				SensorCall(572);break;
			}
			SensorCall(573);int uc;
			uc = under_derive(_x, _y);
			SensorCall(602);if (uc)
				{/*255*/SensorCall(574);deriveMovingStart(_x, _y);/*256*/}
			else
				{
				SensorCall(575);uc = under(_x, _y);
				SensorCall(601);if(uc == -1)
					{/*257*/SensorCall(576);metricSelectionStart(_x, _y);/*258*/}
				else
					{
					SensorCall(577);if (_shift & vmViewShift)
						{
						SensorCall(578);int is=-1,ie=-1;
						int n = NDim__NodesCount(metric);
						DIM_NODE *node = NDim__Nodes(metric);
						hide();
						SensorCall(584);for(int a = 0; a < n; a++)
							{
							SensorCall(579);if (node[a].Flags & DIM_NODE::dnSelected)
								{
								SensorCall(580);node[a].Flags &= ~DIM_NODE::dnSelected;
								SensorCall(582);if (is < 0)
									{/*259*/SensorCall(581);is = a;/*260*/}
								SensorCall(583);ie = a;
								}
							}
						SensorCall(589);if (is < 0)
							{
							SensorCall(585);ie = is = activenode;
							}
						else
							{
							SensorCall(586);if (activenode >= is)
								{/*261*/SensorCall(587);ie = activenode;/*262*/}
							else
								{
								SensorCall(588);ie = is;
								is = activenode;
								}
							}
						SensorCall(591);for(int a = is; a <= ie; a++)
							{/*263*/SensorCall(590);node[a].Flags |= DIM_NODE::dnSelected;/*264*/}
						SensorCall(592);draw();
						}
					else
						{
						SensorCall(593);DIM_NODE *node = metricNodeA(activenode);
						SensorCall(600);if (_shift & vmViewCtrl)
							{
							SensorCall(594);hide();
							SensorCall(597);if (node->Flags & DIM_NODE::dnSelected)
								{/*265*/SensorCall(595);node->Flags &= ~DIM_NODE::dnSelected;/*266*/}
							else
								{/*267*/SensorCall(596);node->Flags |=  DIM_NODE::dnSelected;/*268*/}
							SensorCall(598);draw();
							}
						else
							{
// EE - 22-Dec-09  11:59:34
							SensorCall(599);_x = node->x;
							_y = node->y;
							dim2pix(_x, _y);
// EE - end
							metricMovingStart(_x, _y);
							}
						}
					}
				}
			SensorCall(603);break;
		}
		case vmMetricNewLine:
		{
			SensorCall(604);ID_OBJECT snapobj = {0, 0};
			SensorCall(606);if (snapmode)
				{/*269*/SensorCall(605);snap = snappos(_x, _y, snapobj);/*270*/}
			SensorCall(607);hide();
			SensorCall(609);if (snapmode)
			{
				SensorCall(608);snapped = 0; //snap;
				memcpy(&snap_object,&snapobj,sizeof(ID_OBJECT));
				snap_xy.x = _x;
				snap_xy.y = _y;
				pix2dim(snap_xy.x, snap_xy.y);
			}
			SensorCall(610);metricNodeInsertC(_x, _y);
			draw();
			SensorCall(611);break;
		}
		case vmMetricRotate | vmViewDown:
			SensorCall(612);metricRotateStart(_x, _y);
			SensorCall(613);break;
		case vmMetricScale:
			SensorCall(614);break;
	}
//
	SensorCall(616);showstatus();
	{int  ReplaceReturn76 = 0; SensorCall(617); return ReplaceReturn76;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::MouseMove(double _x, double _y)
{
	SensorCall(618);if(!metric)
		{/*271*/{int  ReplaceReturn75 = 1; SensorCall(619); return ReplaceReturn75;}/*272*/}

	SensorCall(625);switch(mode)
		{
		case vmMetricMove:
		case vmMetricDerive:
		case vmMetricNewLine:
			SensorCall(620);if (x_fixed)
				{/*273*/SensorCall(621);_x = curr_xy.x;/*274*/}
			SensorCall(623);if (y_fixed)
				{/*275*/SensorCall(622);_y = curr_xy.y;/*276*/}
			SensorCall(624);break;
		}
	SensorCall(626);curr_xy.x = _x;
	curr_xy.y = _y;

	XYD xyd = {_x, _y};
	int snap = 0;
	SensorCall(653);switch(mode)
	{
		case vmMetricNone:
			SensorCall(627);if(metric->ContursCount == 0)
			{
				SensorCall(628);if (snapmode)
				{
					SensorCall(629);ID_OBJECT snapobj = {0, 0};
					snap = snappos(xyd.x, xyd.y, snapobj);
					hide();
					snapped = snap;
					memcpy(&snap_object,&snapobj,sizeof(ID_OBJECT));
					snap_xy.x = xyd.x;
					snap_xy.y = xyd.y;
					pix2dim(snap_xy.x, snap_xy.y);
					draw();
				}
			}
			SensorCall(630);break;
		case vmMetricMove:
			SensorCall(631);if (snapmode)
			{
				SensorCall(632);ID_OBJECT snapobj = {0, 0};
				snap = snappos(xyd.x, xyd.y, snapobj);
				hide();
				snapped = snap;
				memcpy(&snap_object,&snapobj,sizeof(ID_OBJECT));
				snap_xy.x = xyd.x;
				snap_xy.y = xyd.y;
				pix2dim(snap_xy.x, snap_xy.y);
				draw();
			}
			SensorCall(633);metricMovingContinue(_x, _y);
			SensorCall(634);break;
		case vmMetricSelect:
			SensorCall(635);metricSelectionContinue(_x, _y);
			SensorCall(636);break;
		case vmMetricDerive:
			SensorCall(637);deriveMovingContinue(_x, _y);
			SensorCall(638);break;
		case vmMetricNewLine:
		{
			SensorCall(639);ID_OBJECT snapobj = {0, 0};
			SensorCall(641);if (snapmode)
				{/*277*/SensorCall(640);snap = snappos(xyd.x, xyd.y, snapobj);/*278*/}
			SensorCall(642);hide();
			SensorCall(644);if (snapmode)
			{
				SensorCall(643);snapped = snap;
				memcpy(&snap_object,&snapobj,sizeof(ID_OBJECT));
				snap_xy.x = xyd.x;
				snap_xy.y = xyd.y;
				pix2dim(snap_xy.x, snap_xy.y);
			}
			SensorCall(645);DIM_NODE *node = metricNodeA(activenode);
			SensorCall(647);if(node)
			{
				SensorCall(646);pix2dim1(_x, _y);
				node->x = _x;
				node->y = _y;
			}
			SensorCall(648);draw();
			SensorCall(649);break;
		}
		case vmMetricRotate:
			SensorCall(650);metricRotateContinue(_x, _y);
			SensorCall(651);break;
		case vmMetricScale:
			SensorCall(652);break;
	}
//
	SensorCall(654);showstatus();
	{int  ReplaceReturn74 = 0; SensorCall(655); return ReplaceReturn74;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::MouseUp(double _x, double _y)
{
	SensorCall(656);if(!metric)
		{/*279*/{int  ReplaceReturn73 = 1; SensorCall(657); return ReplaceReturn73;}/*280*/}

	SensorCall(663);switch(mode)
		{
		case vmMetricMove:
		case vmMetricDerive:
		case vmMetricNewLine:
			SensorCall(658);if (x_fixed)
				{/*281*/SensorCall(659);_x = curr_xy.x;/*282*/}
			SensorCall(661);if (y_fixed)
				{/*283*/SensorCall(660);_y = curr_xy.y;/*284*/}
			SensorCall(662);break;
		}
	SensorCall(664);curr_xy.x = _x;
	curr_xy.y = _y;

	mouserectset(vmClipNone);
	int snap = 0;
	SensorCall(676);switch(mode)
	{
		case vmMetricMove:
			SensorCall(665);if (snapmode)
			{
				SensorCall(666);ID_OBJECT snapobj = {0, 0};
				snap = snappos(_x, _y, snapobj);
				hide();
				snapped = 0; //snap;
				memcpy(&snap_object,&snapobj,sizeof(ID_OBJECT));
				snap_xy.x = _x;
				snap_xy.y = _y;
				pix2dim(snap_xy.x, snap_xy.y);
				draw();
			}
			SensorCall(667);metricMovingContinue(_x, _y);
			metricPassivate();
			SensorCall(668);break;
		case vmMetricSelect:
			SensorCall(669);metricSelectionContinue(_x, _y);
			metricPassivate();
			SensorCall(670);break;
		case vmMetricDerive:
			SensorCall(671);deriveMovingContinue(_x, _y);
			metricPassivate();
			SensorCall(672);break;
		case vmMetricRotate:
			SensorCall(673);metricRotateContinue(_x, _y);
			metricPassivate();
			SensorCall(674);break;
		case vmMetricScale:
			SensorCall(675);break;
	}
//
	SensorCall(677);showstatus();
	{int  ReplaceReturn72 = 0; SensorCall(678); return ReplaceReturn72;}
}
//---------------------------------------------------------------
int TDgtMetricEditor::NodeFlag(int *_flag)
{
SensorCall(679);int rs;

rs = metricGetFlag(_flag);
{int  ReplaceReturn71 = rs; SensorCall(680); return ReplaceReturn71;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyPenUp(bool _state)
{
SensorCall(681);int rs;

rs = metricSetFlag(DIM_NODE::dnPenUp,_state);
{int  ReplaceReturn70 = rs; SensorCall(682); return ReplaceReturn70;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyNoMarker(bool _state)
{
SensorCall(683);int rs;

rs = metricSetFlag(DIM_NODE::dnNoMarker,_state);
{int  ReplaceReturn69 = rs; SensorCall(684); return ReplaceReturn69;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyOrient(bool _state)
{
SensorCall(685);int rs;

rs = metricSetFlag(DIM_NODE::dnOrient,_state);
{int  ReplaceReturn68 = rs; SensorCall(686); return ReplaceReturn68;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::MetricAsLine()
{
	SensorCall(687);hide();
	int rs = MetricDerivesClear(metric);
	draw();
//
	{int  ReplaceReturn67 = rs; SensorCall(688); return ReplaceReturn67;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::MetricAsErmit()
{
	SensorCall(689);hide();
	int rs = MetricDerivesAuto(metric);
	draw();
//
	{int  ReplaceReturn66 = rs; SensorCall(690); return ReplaceReturn66;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyIns()
{
	SensorCall(691);if(!metric)
		{/*285*/{int  ReplaceReturn65 = 1; SensorCall(692); return ReplaceReturn65;}/*286*/}
//
	SensorCall(703);switch(mode)
	{
		case vmMetricNone:
		{
			SensorCall(693);double x = 0,
			       y = 0;
			mouseposget(x, y);
			SensorCall(696);if(metric->ContursCount == 0)
			{
				SensorCall(694);metricInputContinue(0);
				hide();
				metricNodeInsertC(x, y);
				draw();
				SensorCall(695);break;
			}
			SensorCall(698);if(1) //under(x, y) == -1) // EE - 15-Aug-03  17:18:58 - razreshim vstavky parnoy vershini
			{
				SensorCall(697);hide();
				metricNodeInsertA(x, y);
				draw();
//				metricGoNode(activenode); // EE - 01-Oct-04  13:54:52 - ostavim cursor v pokoe
			}
			SensorCall(699);break;
		}
		case vmMetricNewLine:
		{
			SensorCall(700);double x = 0,
			       y = 0;
			mouseposget(x, y);
			hide();
			metricNodeInsertC(x, y);
			draw();
			SensorCall(701);break;
		}
		case vmMetricRotate:
		case vmMetricScale:
			SensorCall(702);break;
	}
//
	SensorCall(704);showstatus();
	{int  ReplaceReturn64 = 0; SensorCall(705); return ReplaceReturn64;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyDel()
{
	SensorCall(706);if(!metric)
		{/*287*/{int  ReplaceReturn63 = 1; SensorCall(707); return ReplaceReturn63;}/*288*/}
//
	SensorCall(716);switch(mode)
	{
		case vmMetricNone:
		{
			SensorCall(708);double x = 0,
			       y = 0;
			mouseposget(x, y);
			under(x, y);
			SensorCall(711);if(activenode == -1)
				{/*289*/SensorCall(709);metricGoNearest(x, y);/*290*/}
			else
			{
				SensorCall(710);hide();
				metricNodeDeleteA(activenode);
				draw();
				metricGoNode(activenode);
			}
			SensorCall(712);break;
		}
		case vmMetricNewLine:
		{
			SensorCall(713);int uc = ((DIM_CONTUR*)(metric + 1) + activecontur)->NodesCount - 2;
			hide();
			metricNodeDeleteB(activecontur, max2(0, uc));
			draw();
			SensorCall(714);break;
		}
		case vmMetricRotate:
		case vmMetricScale:
			SensorCall(715);break;
	}
//
	SensorCall(717);showstatus();
	{int  ReplaceReturn62 = 0; SensorCall(718); return ReplaceReturn62;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyUp()
{
	{int  ReplaceReturn61 = metricNodeKeyMove(activenode, meMoveUp); SensorCall(719); return ReplaceReturn61;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyDown()
{
	{int  ReplaceReturn60 = metricNodeKeyMove(activenode, meMoveDown); SensorCall(720); return ReplaceReturn60;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyLeft()
{
	{int  ReplaceReturn59 = metricNodeKeyMove(activenode, meMoveLeft); SensorCall(721); return ReplaceReturn59;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyRight()
{
	{int  ReplaceReturn58 = metricNodeKeyMove(activenode, meMoveRight); SensorCall(722); return ReplaceReturn58;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyToNearest()
{
	SensorCall(723);if(!metric)
		{/*291*/{int  ReplaceReturn57 = 1; SensorCall(724); return ReplaceReturn57;}/*292*/}
//
	SensorCall(730);switch(mode)
	{
		case vmMetricNone:
		{
			SensorCall(725);double x = 0,
			       y = 0;
			mouseposget(x, y);
			under(x, y);
			SensorCall(727);if(activenode == -1)
				{/*293*/SensorCall(726);metricGoNearest(x, y);/*294*/}
			SensorCall(728);break;
		}
		case vmMetricNewLine:
		case vmMetricRotate:
		case vmMetricScale:
			SensorCall(729);break;
	}
//
	SensorCall(731);showstatus();
	{int  ReplaceReturn56 = 0; SensorCall(732); return ReplaceReturn56;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyToNext()
{
	SensorCall(733);if(!metric)
		{/*295*/{int  ReplaceReturn55 = 1; SensorCall(734); return ReplaceReturn55;}/*296*/}
//
	SensorCall(755);switch(mode)
	{
		case vmMetricNone:
		{
			SensorCall(735);double x = 0,
			       y = 0;
			mouseposget(x, y);
			under(x, y);
			SensorCall(752);if(activenode == -1)
				{/*297*/SensorCall(736);metricGoNearest(x, y);/*298*/}
			else
			{
				SensorCall(737);DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
				DIM_NODE *n0 = NDim__Nodes(metric),
				         *n1 = n0 + activenode;
				SensorCall(741);for(int a = 0; a < metric->ContursCount; a++, contur++)
				{
					SensorCall(738);if(contur->NodesCount > (n1 - n0))
						{/*299*/SensorCall(739);break;/*300*/}
					SensorCall(740);n0 += contur->NodesCount;
				}
				SensorCall(750);if(ItClosed(n0, contur->NodesCount))
				{
					SensorCall(742);if((n1 - n0) == (contur->NodesCount - 1))//last
						{/*301*/SensorCall(743);n1 = n0 + 1;/*302*/}
					else {/*303*/SensorCall(744);if((n1 - n0) == (contur->NodesCount - 2))//last - 1
						{/*305*/SensorCall(745);n1++;/*306*/} //n1 = n0; // EE - 29-Mar-04  14:23:11
					else
						{/*307*/SensorCall(746);n1++;/*308*/}/*304*/}
				}
				else
				{
					SensorCall(747);if((n1 - n0) == (contur->NodesCount - 1))//last
						{/*309*/SensorCall(748);n1 = n0;/*310*/}
					else
						{/*311*/SensorCall(749);n1++;/*312*/}
				}
				SensorCall(751);n0 = NDim__Nodes(metric);
//				metricGoNode(n1 - n0);  // EE - 10-Jun-08  12:01:04 - walk through current contour
				metricGoNode((activenode < NDim__NodesCount(metric) - 1) ? activenode + 1 : 0); // EE - 10-Jun-08  12:01:04 - walk through all contours
			}
			SensorCall(753);break;
		}
		case vmMetricNewLine:
		case vmMetricRotate:
		case vmMetricScale:
			SensorCall(754);break;
	}
//
	SensorCall(756);showstatus();
	{int  ReplaceReturn54 = 0; SensorCall(757); return ReplaceReturn54;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyToPrev()
{
	SensorCall(758);if(!metric)
		{/*313*/{int  ReplaceReturn53 = 1; SensorCall(759); return ReplaceReturn53;}/*314*/}
//
	SensorCall(778);switch(mode)
	{
		case vmMetricNone:
		{
			SensorCall(760);double x = 0,
			       y = 0;
			mouseposget(x, y);
			under(x, y);
			SensorCall(775);if(activenode == -1)
				{/*315*/SensorCall(761);metricGoNearest(x, y);/*316*/}
			else
			{
				SensorCall(762);DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
				DIM_NODE *n0 = (DIM_NODE*)(contur + metric->ContursCount),
				         *n1 = n0 + activenode;
				SensorCall(766);for(int a = 0; a < metric->ContursCount; a++, contur++)
				{
					SensorCall(763);if(contur->NodesCount > (n1 - n0))
						{/*317*/SensorCall(764);break;/*318*/}
					SensorCall(765);n0 += contur->NodesCount;
				}
				SensorCall(773);if(ItClosed(n0, contur->NodesCount))
				{
					SensorCall(767);if((n1 - n0) == 0)
						{/*319*/SensorCall(768);n1 = n0 + contur->NodesCount - 2;/*320*/}
					else
						{/*321*/SensorCall(769);n1--;/*322*/}
				}
				else
				{
					SensorCall(770);if((n1 - n0) == 0)
						{/*323*/SensorCall(771);n1 = n0 + contur->NodesCount - 1;/*324*/}
					else
						{/*325*/SensorCall(772);n1--;/*326*/}
				}
				SensorCall(774);n0 = (DIM_NODE*)((DIM_CONTUR*)(metric + 1) + metric->ContursCount);
//				metricGoNode(n1 - n0); // EE - 10-Jun-08  12:01:04 - walk through current contour
				metricGoNode((activenode > 0) ? activenode - 1 : NDim__NodesCount(metric) - 1); // EE - 10-Jun-08  12:01:04 - walk through all contours
			}
			SensorCall(776);break;
		}
		case vmMetricNewLine:
		case vmMetricRotate:
		case vmMetricScale:
			SensorCall(777);break;
	}
//
	SensorCall(779);showstatus();
	{int  ReplaceReturn52 = 0; SensorCall(780); return ReplaceReturn52;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyToFirst()
{
	SensorCall(781);if(mode != vmMetricNone)
		{/*327*/{int  ReplaceReturn51 = 1; SensorCall(782); return ReplaceReturn51;}/*328*/}
//
	SensorCall(783);int contur = metricContur(activenodeA());
	SensorCall(786);if(contur < 0)
		{/*329*/SensorCall(784);metricGoNode(0);/*330*/}
	else
		{/*331*/SensorCall(785);metricGoNode(metricNodeB(contur, 0));/*332*/}
//
	{int  ReplaceReturn50 = 0; SensorCall(787); return ReplaceReturn50;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyToLast()
{
	SensorCall(788);if(mode != vmMetricNone)
		{/*333*/{int  ReplaceReturn49 = 1; SensorCall(789); return ReplaceReturn49;}/*334*/}
//
	SensorCall(790);int contur = metricContur(activenodeA()),
	     n = NDim__NodesCount(metric);
	SensorCall(794);if(n > 0)
	{
		SensorCall(791);if(contur < 0)
			{/*335*/SensorCall(792);metricGoNode(n - 1);/*336*/}
		else
			{/*337*/SensorCall(793);metricGoNode(metricNodeB(contur,
				((DIM_CONTUR*)(metric + 1) + contur)->NodesCount - 1));/*338*/}
	}
//
	{int  ReplaceReturn48 = 0; SensorCall(795); return ReplaceReturn48;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeySelectAll()
{
	SensorCall(796);if(mode != vmMetricNone)
		{/*339*/{int  ReplaceReturn47 = 1; SensorCall(797); return ReplaceReturn47;}/*340*/}
//
	SensorCall(798);int n = NDim__NodesCount(metric);
	DIM_NODE *node = NDim__Nodes(metric);
	hide();
	SensorCall(800);for(int a = 0; a < n; a++)
		{/*341*/SensorCall(799);node[a].Flags |= DIM_NODE::dnSelected;/*342*/}
	SensorCall(801);draw();
//
	{int  ReplaceReturn46 = 0; SensorCall(802); return ReplaceReturn46;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyMakeFirstNode()
{
	SensorCall(803);if(!metric)
		{/*343*/{int  ReplaceReturn45 = 1; SensorCall(804); return ReplaceReturn45;}/*344*/}
//
	SensorCall(822);switch(mode)
	{
		case vmMetricNone:
		{
			SensorCall(805);double x = 0,
			       y = 0;
			mouseposget(x, y);
			under(x, y);
			SensorCall(819);if(activenode == -1)
				{/*345*/SensorCall(806);metricGoNearest(x, y);/*346*/}
			else
				{
				SensorCall(807);hide();
				DIM_NODE *nodes,*nodes1;
				nodes = metricNodeA(activenode);
				SensorCall(817);if (nodes != NULL)
					{
					SensorCall(808);DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
					int nc = metricContur(activenode);
					int nn = metricConturNode(activenode);
					contur += nc;
					nodes  -= nn;
					SensorCall(816);if ((nn > 0) && ItClosed(nodes,contur->NodesCount))
						{
						SensorCall(809);nodes1 = new DIM_NODE[contur->NodesCount];
						memcpy(nodes1,nodes,contur->NodesCount*sizeof(DIM_NODE));
						memcpy(nodes,nodes1+nn,(contur->NodesCount-nn)*sizeof(DIM_NODE));
						memcpy(nodes+(contur->NodesCount-nn-1),nodes1,(nn+1)*sizeof(DIM_NODE));
						nodes[0].Flags &= ~DIM_NODE::dnPenUp;
						SensorCall(812);if (nodes1[contur->NodesCount-1].Flags & DIM_NODE::dnPenUp)
							{/*347*/SensorCall(810);nodes[contur->NodesCount-nn-1].Flags |=  DIM_NODE::dnPenUp;/*348*/}
						else
							{/*349*/SensorCall(811);nodes[contur->NodesCount-nn-1].Flags &= ~DIM_NODE::dnPenUp;/*350*/}
						SensorCall(814);for(int a = 0; a < contur->NodesCount; a++)
							{/*351*/SensorCall(813);nodes[a].Flags &= ~DIM_NODE::dnSelected;/*352*/}
						SensorCall(815);delete[] nodes1;

						activenode = metricNodeB(nc,0);
						}
					}
				SensorCall(818);draw();
				}
			SensorCall(820);break;
		}
		case vmMetricNewLine:
		case vmMetricRotate:
		case vmMetricScale:
			SensorCall(821);break;
	}
//
	SensorCall(823);showstatus();
	{int  ReplaceReturn44 = 0; SensorCall(824); return ReplaceReturn44;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyConturOpen()
{
	SensorCall(825);int rs = 1;
	SensorCall(827);if(!metric)
		{/*353*/{int  ReplaceReturn43 = rs; SensorCall(826); return ReplaceReturn43;}/*354*/}
//
	SensorCall(828);hide();
	rs = metricConturOpen(metricContur(activenodeA()));
	draw();
//
	showstatus();
	{int  ReplaceReturn42 = rs; SensorCall(829); return ReplaceReturn42;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyConturClose()
{
	SensorCall(830);int rs = 1;
	SensorCall(832);if(!metric)
		{/*355*/{int  ReplaceReturn41 = rs; SensorCall(831); return ReplaceReturn41;}/*356*/}
//
	SensorCall(833);hide();
	rs = metricConturClose(metricContur(activenodeA()));
	draw();
//
	showstatus();
	{int  ReplaceReturn40 = rs; SensorCall(834); return ReplaceReturn40;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyConturInvert()
{
	SensorCall(835);int rs = 1;
	SensorCall(837);if(!metric)
		{/*357*/{int  ReplaceReturn39 = rs; SensorCall(836); return ReplaceReturn39;}/*358*/}
//
	SensorCall(838);hide();
	rs = metricConturInvert(metricContur(activenodeA()));
	draw();
//
	showstatus();
	{int  ReplaceReturn38 = rs; SensorCall(839); return ReplaceReturn38;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyConturDivide()
{
	SensorCall(840);int rs = 1;
	SensorCall(842);if(!metric)
		{/*359*/{int  ReplaceReturn37 = rs; SensorCall(841); return ReplaceReturn37;}/*360*/}
//
	SensorCall(843);hide();
	int a = activenodeA();
	rs = metricConturDivide(metricContur(a),metricConturNode(a));
	draw();
//
	showstatus();
	{int  ReplaceReturn36 = rs; SensorCall(844); return ReplaceReturn36;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyConturMerge()
{
	SensorCall(845);int rs = 1;
	SensorCall(847);if(!metric)
		{/*361*/{int  ReplaceReturn35 = rs; SensorCall(846); return ReplaceReturn35;}/*362*/}
//
	SensorCall(848);hide();
	int a = activenodeA();
	rs = metricConturMerge(metricContur(a));
	draw();
//
	showstatus();
	{int  ReplaceReturn34 = rs; SensorCall(849); return ReplaceReturn34;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeySnapContur()
{
	SensorCall(850);int rs = 1;
	SensorCall(852);if(!metric)
		{/*363*/{int  ReplaceReturn33 = rs; SensorCall(851); return ReplaceReturn33;}/*364*/}

	SensorCall(853);int a = activenodeA();
	SensorCall(855);if (mode == vmMetricNewLine)
		{/*365*/SensorCall(854);a --;/*366*/}
	SensorCall(857);if (a < 0)
		{/*367*/{int  ReplaceReturn32 = rs; SensorCall(856); return ReplaceReturn32;}/*368*/}
	SensorCall(859);if (metricContur(a - 1) != metricContur(a))
		{/*369*/{int  ReplaceReturn31 = rs; SensorCall(858); return ReplaceReturn31;}/*370*/}

	SensorCall(860);XYD xyd[2];
	DIM_NODE *dn,*nodes=NULL;
	dn = metricNodeA(a - 1);
	xyd[0].x = dn[0].x;
	xyd[0].y = dn[0].y;
	xyd[1].x = dn[1].x;
	xyd[1].y = dn[1].y;
	int n = snapconturdim(xyd[0],xyd[1],nodes);
	SensorCall(879);if (nodes && (n > 1))
	{
		SensorCall(861);hide();

		int i,j,cntn;
		cntn = NDim__NodesCount(metric);
		dn = NDim__Nodes(metric);
		SensorCall(863);for (i=0; i<cntn; i++,dn++)
			{/*371*/SensorCall(862);dn->Flags &= ~DIM_NODE::dnSelected;/*372*/}
		SensorCall(864);i = a;
		int con  = metricContur(i);
		int node = metricConturNode(i);
		node --;
		SensorCall(874);for (j=1; j<n; j++)
			{
			SensorCall(865);dim2pix(nodes[j].x,nodes[j].y);
			i = metricNodeInsertB(con,node+j,nodes[j].x,nodes[j].y);
			dn = NDim__Nodes(metric);
			SensorCall(867);if (i == -1)
				{/*373*/SensorCall(866);break;/*374*/}
			SensorCall(869);if ((j == 1) && (i > 0))
				{
				SensorCall(868);dn[i-1].dXOut = nodes[j-1].dXOut;
				dn[i-1].dYOut = nodes[j-1].dYOut;
				}
			SensorCall(870);dn[i].dXIn = nodes[j].dXIn;
			dn[i].dYIn = nodes[j].dYIn;
			SensorCall(872);if (j < n - 1)
				{
				SensorCall(871);dn[i].dXOut = nodes[j].dXOut;
				dn[i].dYOut = nodes[j].dYOut;
				}
//			if (mode == vmMetricNone)
//				dn[i].Flags |= DIM_NODE::dnSelected;
			SensorCall(873);i ++;
			}
		SensorCall(875);metricNodeDeleteA(i);
		SensorCall(877);if (mode == vmMetricNewLine)
			{/*375*/SensorCall(876);activenode = i;/*376*/}

		SensorCall(878);rs = 0;
		draw();
	}
	SensorCall(881);if (nodes)
		{/*377*/SensorCall(880);delete[] nodes;/*378*/}

	SensorCall(882);showstatus();
	{int  ReplaceReturn30 = rs; SensorCall(883); return ReplaceReturn30;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::ModeMain()
{
	SensorCall(884);if(!metric)
		{/*379*/{int  ReplaceReturn29 = 1; SensorCall(885); return ReplaceReturn29;}/*380*/}
//
	SensorCall(886);int rs = metricPassivate();
//
	showstatus();
	{int  ReplaceReturn28 = rs; SensorCall(887); return ReplaceReturn28;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::ModeInputNew()
{
	SensorCall(888);if(!metric)
		{/*381*/{int  ReplaceReturn27 = 1; SensorCall(889); return ReplaceReturn27;}/*382*/}
//
	SensorCall(890);int rs = metricInputContinue(metric->ContursCount);
//
	showstatus();
	{int  ReplaceReturn26 = rs; SensorCall(891); return ReplaceReturn26;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::ModeInputContinue()
{
	SensorCall(892);if(!metric)
		{/*383*/{int  ReplaceReturn25 = 1; SensorCall(893); return ReplaceReturn25;}/*384*/}
//
	SensorCall(894);int rs = metricInputContinue(metricContur(activenodeA()));
//
	showstatus();
	{int  ReplaceReturn24 = rs; SensorCall(895); return ReplaceReturn24;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::ModeRotate(double _cx, double _cy)
{
	SensorCall(896);int rs = 0;

	SensorCall(901);if ((mode & vmViewModes) == vmMetricRotate)
	{
		SensorCall(897);if (mode & vmViewDown)
			{/*385*/SensorCall(898);mode = vmMetricNone;/*386*/}
		else
			{/*387*/SensorCall(899);rs = metricPassivate();/*388*/}
	}
	else
	{
		SensorCall(900);rs = metricPassivate();
		rotcenter.x = _cx;
		rotcenter.y = _cy;
		rotangle = 0.0;
		mode = vmMetricRotate | vmViewDown;
	}

	SensorCall(902);showstatus();
	{int  ReplaceReturn23 = rs; SensorCall(903); return ReplaceReturn23;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::FixX(bool _fix)
{
	SensorCall(904);if (_fix)
	{
		SensorCall(905);DIM_NODE *node = metricNodeA(activenode - 1);
		SensorCall(907);if(node)
		{
			SensorCall(906);double x = node->x,
			       y = node->y;
			dim2pix(x, y);
			curr_xy.x = x;
		}
	}

	SensorCall(908);x_fixed = _fix;
	{int  ReplaceReturn22 = x_fixed; SensorCall(909); return ReplaceReturn22;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::FixY(bool _fix)
{
	SensorCall(910);if (_fix)
	{
		SensorCall(911);DIM_NODE *node = metricNodeA(activenode - 1);
		SensorCall(913);if(node)
		{
			SensorCall(912);double x = node->x,
			       y = node->y;
			dim2pix(x, y);
			curr_xy.y = y;
		}
	}

	SensorCall(914);y_fixed = _fix;
	{int  ReplaceReturn21 = y_fixed; SensorCall(915); return ReplaceReturn21;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::XFixed()
{
	{int  ReplaceReturn20 = x_fixed; SensorCall(916); return ReplaceReturn20;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::YFixed()
{
	{int  ReplaceReturn19 = y_fixed; SensorCall(917); return ReplaceReturn19;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::IsActive()
{
	SensorCall(918);if(metric)
		{/*389*/{int  ReplaceReturn18 = 1; SensorCall(919); return ReplaceReturn18;}/*390*/}
	{int  ReplaceReturn17 = 0; SensorCall(920); return ReplaceReturn17;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::IsChanged()
{
SensorCall(921);int changed=0,isx_len,len;

isx_len = NDim__MetricLen(isx_metric);
len     = NDim__MetricLen(metric);
SensorCall(928);if ((isx_metric != NULL) || (metric != NULL))
	{
	SensorCall(922);if ((isx_metric != NULL) && (metric != NULL))
		{
		SensorCall(923);if (isx_len == len)
			{
			SensorCall(924);if (len)
				{/*391*/SensorCall(925);changed = memcmp(isx_metric,metric,len);/*392*/}
			}
		else
			{/*393*/SensorCall(926);changed = -1;/*394*/}
		}
	else
		{/*395*/SensorCall(927);changed = -1;/*396*/}
	}
/*
if(metric)
	changed = -1;
*/
SensorCall(929);return(changed);
}
//---------------------------------------------------------------
int TDgtMetricEditor::ItClosed(DIM_NODE *_nodes, int _count)
{
//	if(mode == vmMetricNewLine)
//		return 0;
//
	SensorCall(930);if((_count < 2) || !_nodes)
		{/*397*/{int  ReplaceReturn16 = 0; SensorCall(931); return ReplaceReturn16;}/*398*/}
	SensorCall(933);if((_nodes->x != _nodes[_count - 1].x) ||
	   (_nodes->y != _nodes[_count - 1].y))
//	if(memcmp(nodes, nodes + _count - 1, sizeof(*nodes)))
		{/*399*/{int  ReplaceReturn15 = 0; SensorCall(932); return ReplaceReturn15;}/*400*/}
//
	{int  ReplaceReturn14 = 1; SensorCall(934); return ReplaceReturn14;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::metricCorrect(DIM_METRIC *&_metric)
{
SensorCall(935);double xx,yy;

	SensorCall(937);if(!_metric)
		{/*401*/{int  ReplaceReturn13 = 1; SensorCall(936); return ReplaceReturn13;}/*402*/}
//
	SensorCall(965);if((mode == vmMetricMove) && (activenode != -1))
	{
		SensorCall(938);DIM_NODE *n0 = (DIM_NODE*)((DIM_CONTUR*)(_metric + 1) + _metric->ContursCount),
		         *n1 = n0 + activenode;
		SensorCall(964);if(n1->Flags & DIM_NODE::dnSelected)
		{
			SensorCall(939);DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
			SensorCall(952);for(int a = 0; a < _metric->ContursCount; a++, contur++)
			{
				SensorCall(940);int n = contur->NodesCount - 1;
				SensorCall(951);if(ItClosed(n0, contur->NodesCount))
				{
					SensorCall(941);if((n0[0].Flags & DIM_NODE::dnSelected) ||
					   (n0[n].Flags & DIM_NODE::dnSelected))
					{
						SensorCall(942);setxy(n0[0], n0[0].x + dxy.x, n0[0].y + dxy.y);
						setxy(n0[n], n0[n].x + dxy.x, n0[n].y + dxy.y);
					}
					SensorCall(943);n0++;
					SensorCall(946);for(int a = 1; a < n; a++, n0++)
						{/*403*/SensorCall(944);if(n0->Flags & DIM_NODE::dnSelected)
							{/*405*/SensorCall(945);setxy(*n0, n0->x + dxy.x, n0->y + dxy.y);/*406*/}/*404*/}
					SensorCall(947);n0++;
				}
				else
					{/*407*/SensorCall(948);for(int a = 0; a < contur->NodesCount; a++, n0++)
						{/*409*/SensorCall(949);if(n0->Flags & DIM_NODE::dnSelected)
							{/*411*/SensorCall(950);setxy(*n0, n0->x + dxy.x, n0->y + dxy.y);/*412*/}/*410*/}/*408*/}
			}
		}
		else
		{
			SensorCall(953);DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
			SensorCall(957);for(int a = 0; a < _metric->ContursCount; a++, contur++)
			{
				SensorCall(954);if(contur->NodesCount > (n1 - n0))
					{/*413*/SensorCall(955);break;/*414*/}
				SensorCall(956);n0 += contur->NodesCount;
			}
			SensorCall(958);int n = contur->NodesCount - 1;
			SensorCall(963);if(ItClosed(n0, contur->NodesCount))
			{
				SensorCall(959);if((n1 == n0) || (n1 == (n0 + n)))
				{
					SensorCall(960);setxy(n0[0], n0[0].x + dxy.x, n0[0].y + dxy.y);
					setxy(n0[n], n0[n].x + dxy.x, n0[n].y + dxy.y);
				}
				else
					{/*415*/SensorCall(961);setxy(*n1, n1->x + dxy.x, n1->y + dxy.y);/*416*/}
			}
			else
				{/*417*/SensorCall(962);setxy(*n1, n1->x + dxy.x, n1->y + dxy.y);/*418*/}
		}
	}
//
	SensorCall(972);if((mode == vmMetricSelect) && (_metric == metric))
	{
		SensorCall(966);int n = NDim__NodesCount(_metric);
		DIM_NODE *n0 = NDim__Nodes(_metric);
		double x0 = min2(xy0.x, xy1.x),
			   y0 = min2(xy0.y, xy1.y),
			   x1 = max2(xy0.x, xy1.x),
		       y1 = max2(xy0.y, xy1.y);
/* EE - 09-Jun-03  17:37:22 - � �������� �������� �����������. ���� ���������� �������� dim2pix (��.����)
		pix2dim(x0, y0);
		pix2dim(x1, y1);
*/
		SensorCall(971);for(int a = 0; a < n; a++, n0++)
			{
			SensorCall(967);xx = n0->x;
			yy = n0->y;
			dim2pix(xx,yy); // EE - 09-Jun-03  17:37:22
			SensorCall(970);if((xx >= x0) && (xx <= x1) && (yy >= y0) && (yy <= y1))
				{/*419*/SensorCall(968);n0->Flags |=  DIM_NODE::dnSelected;/*420*/}
			else
				{/*421*/SensorCall(969);n0->Flags &= ~DIM_NODE::dnSelected;/*422*/}
			}
	}
//
	SensorCall(974);if((mode == vmMetricNewLine) && (_metric == metric))
	{
		SensorCall(973);metricNodeDeleteA(activenode);
	}
//
	SensorCall(981);if(mode == vmMetricRotate)
	{
		SensorCall(975);double cx = rotcenter.x;
		double cy = rotcenter.y;
		dim2pix(cx, cy);
		dxy.x = cos(rotangle);
		dxy.y = sin(rotangle);
		int n = NDim__NodesCount(_metric);
		DIM_NODE *nodes = NDim__Nodes(_metric);
		SensorCall(980);for (int a = 0; a < n; a++)
		{
			SensorCall(976);XYD xyd[3];
			xyd[0].x = nodes[a].x;
			xyd[0].y = nodes[a].y;
			xyd[1].x = nodes[a].x + nodes[a].dXIn;
			xyd[1].y = nodes[a].y + nodes[a].dYIn;
			xyd[2].x = nodes[a].x + nodes[a].dXOut;
			xyd[2].y = nodes[a].y + nodes[a].dYOut;
			SensorCall(978);for (int i = 0; i < 3; i++)
			{
				SensorCall(977);dim2pix(xyd[i].x, xyd[i].y);
				xyd[i].x -= cx;
				xyd[i].y -= cy;
				double x = (xyd[i].x * dxy.x - xyd[i].y * dxy.y) + cx;
				double y = (xyd[i].x * dxy.y + xyd[i].y * dxy.x) + cy;
				pix2dim(x, y);
				xyd[i].x = x;
				xyd[i].y = y;
			}
			SensorCall(979);setxy(nodes[a], xyd[0].x, xyd[0].y);
			nodes[a].dXIn  = xyd[1].x - xyd[0].x;
			nodes[a].dYIn  = xyd[1].y - xyd[0].y;
			nodes[a].dXOut = xyd[2].x - xyd[0].x;
			nodes[a].dYOut = xyd[2].y - xyd[0].y;
		}
	}
//
	{int  ReplaceReturn12 = 0; SensorCall(982); return ReplaceReturn12;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::MetricDerivesClear(DIM_METRIC *_metric)
{
	SensorCall(983);if(!_metric)
		{/*423*/{int  ReplaceReturn11 = 1; SensorCall(984); return ReplaceReturn11;}/*424*/}
//
	SensorCall(985);DIM_NODE *nodes = NDim__Nodes(_metric);
	int      count = NDim__NodesCount(_metric);
	SensorCall(987);for(int a = 0; a < count; a++)
	{
		SensorCall(986);nodes[a].dXIn  = 0.0;
		nodes[a].dYIn  = 0.0;
		nodes[a].dXOut = 0.0;
		nodes[a].dYOut = 0.0;
//		nodes[a].dZIn  = 0.0;
//		nodes[a].dZOut = 0.0;
	}
//	_metric->Flags &= ~DIM_METRIC::exDerives;
//
	{int  ReplaceReturn10 = 0; SensorCall(988); return ReplaceReturn10;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::MetricDerivesAuto(DIM_METRIC *_metric)
{
	SensorCall(989);if(MetricDerivesClear(_metric))
		{/*425*/{int  ReplaceReturn9 = 1; SensorCall(990); return ReplaceReturn9;}/*426*/}
	SensorCall(992);if(_metric->ContursCount < 1)
		{/*427*/{int  ReplaceReturn8 = 0; SensorCall(991); return ReplaceReturn8;}/*428*/}
//
	SensorCall(993);DIM_CONTUR *contur = (DIM_CONTUR*)(_metric + 1);
	DIM_NODE *nodes = NDim__Nodes(_metric);
	SensorCall(1005);for(int a = 0; a < _metric->ContursCount; a++)
	{
		SensorCall(994);int n = contur->NodesCount - 1;
		SensorCall(998);for(int b = 1; b < n; b++)
		{
			SensorCall(995);double dx = nodes[b+1].x - nodes[b-1].x;
			double dy = nodes[b+1].y - nodes[b-1].y;
			double h  = hypot(dx,dy);
			SensorCall(997);if (h != 0.0)
				{
				SensorCall(996);double h_in  = hypot(nodes[b].x-nodes[b-1].x,nodes[b].y-nodes[b-1].y);
				double h_out = hypot(nodes[b+1].x-nodes[b].x,nodes[b+1].y-nodes[b].y);
				nodes[b].dXIn  = dx * h_in  / h;
				nodes[b].dYIn  = dy * h_in  / h;
				nodes[b].dXOut = dx * h_out / h;
				nodes[b].dYOut = dy * h_out / h;
				}

/* EE - 02-Apr-09  16:17:34 - see above
			nodes[b].dXIn = nodes[b].dXOut = (nodes[b + 1].x - nodes[b - 1].x) / 2.0;
			nodes[b].dYIn = nodes[b].dYOut = (nodes[b + 1].y - nodes[b - 1].y) / 2.0;
*/
		}
		SensorCall(1003);if(ItClosed(nodes, contur->NodesCount))
		{
			SensorCall(999);double dx = nodes[1].x - nodes[n-1].x;
			double dy = nodes[1].y - nodes[n-1].y;
			double h  = hypot(dx,dy);
			SensorCall(1001);if (h != 0.0)
				{
				SensorCall(1000);double h_in  = hypot(nodes[0].x-nodes[n-1].x,nodes[0].y-nodes[n-1].y);
				double h_out = hypot(nodes[1].x-nodes[0].x,nodes[1].y-nodes[0].y);
				nodes[0].dXIn  = dx * h_in  / h;
				nodes[0].dYIn  = dy * h_in  / h;
				nodes[0].dXOut = dx * h_out / h;
				nodes[0].dYOut = dy * h_out / h;
				}

/* EE - 02-Apr-09  16:17:34 - see above
			nodes[0].dXIn = nodes[0].dXOut = (nodes[1].x - nodes[n - 1].x)/2.0;
			nodes[0].dYIn = nodes[0].dYOut = (nodes[1].y - nodes[n - 1].y)/2.0;
*/
			SensorCall(1002);nodes[n].dXIn  = nodes[0].dXIn;
			nodes[n].dYIn  = nodes[0].dYIn;
			nodes[n].dXOut = nodes[0].dXOut;
			nodes[n].dYOut = nodes[0].dYOut;
		}
		SensorCall(1004);nodes += contur->NodesCount;
		contur++;
	}
//	_metric->Flags |= DIM_METRIC::exDerives;
//
	{int  ReplaceReturn7 = 0; SensorCall(1006); return ReplaceReturn7;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::under_derive(double _x, double _y)
{

SensorCall(1007);derivenode = 0;

SensorCall(1022);if (metric)
	{
	SensorCall(1008);DIM_NODE *nodes = NDim__Nodes(metric);
	int      count = NDim__NodesCount(metric);
	SensorCall(1021);for(int a = 0; a < count; a++)
		{
		SensorCall(1009);if (nodes[a].Flags & DIM_NODE::dnSelected)
			{
			SensorCall(1010);double x,y;
			SensorCall(1015);if ((nodes[a].dXIn != 0.0) || (nodes[a].dYIn != 0.0))
				{
				SensorCall(1011);x = nodes[a].x - nodes[a].dXIn / dFactor;
				y = nodes[a].y - nodes[a].dYIn / dFactor;
				dim2pix(x,y);
				SensorCall(1014);if ((fabs(x-_x) < nodeSize) && (fabs(y-_y) < nodeSize))
					{
					SensorCall(1012);derivenode = -(a + 1);
					SensorCall(1013);break;
					}
				}
			SensorCall(1020);if ((nodes[a].dXOut != 0.0) || (nodes[a].dYOut != 0.0))
				{
				SensorCall(1016);x = nodes[a].x + nodes[a].dXOut / dFactor;
				y = nodes[a].y + nodes[a].dYOut / dFactor;
				dim2pix(x,y);
				SensorCall(1019);if ((fabs(x-_x) < nodeSize) && (fabs(y-_y) < nodeSize))
					{
					SensorCall(1017);derivenode = a + 1;
					SensorCall(1018);break;
					}
				}
			}
		}
	}

SensorCall(1023);return(derivenode);
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::SetSnapMode(bool _snapmode)
{
	SensorCall(1024);hide();
	snapmode = _snapmode;
	snapped = 0;
	draw();
	{int  ReplaceReturn6 = snapmode; SensorCall(1025); return ReplaceReturn6;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::SnapMode()
{
	{int  ReplaceReturn5 = snapmode; SensorCall(1026); return ReplaceReturn5;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::snappos(double &_x, double &_y, ID_OBJECT &_obj)
{
	SensorCall(1027);int snap = 0;
	{int  ReplaceReturn4 = snap; SensorCall(1028); return ReplaceReturn4;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::snapconturdim(XYD _xyd0, XYD _xyd1, DIM_NODE *&_nodes)
{
	SensorCall(1029);_nodes = NULL;
	{int  ReplaceReturn3 = 0; SensorCall(1030); return ReplaceReturn3;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::ostoclipboard()
{
	SensorCall(1031);return(1);
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::osfromclipboard()
{
	SensorCall(1032);return(1);
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyToClipboard()
{
	SensorCall(1033);int rs = 1;

	rs = ostoclipboard();
	{int  ReplaceReturn2 = rs; SensorCall(1034); return ReplaceReturn2;}
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyFromClipboard()
{
	SensorCall(1035);int rs = 1;
	SensorCall(1037);if(!metric)
		{/*429*/{int  ReplaceReturn1 = rs; SensorCall(1036); return ReplaceReturn1;}/*430*/}
//
	SensorCall(1038);hide();
	rs = osfromclipboard();
	draw();
	SensorCall(1040);if (!rs)
		{/*431*/SensorCall(1039);KeyToNext();/*432*/}
//
	SensorCall(1041);showstatus();
	{int  ReplaceReturn0 = rs; SensorCall(1042); return ReplaceReturn0;}
}
//---------------------------------------------------------------------------
