/* PyBytes (bytearray) implementation */

#define PY_SSIZE_T_CLEAN
#include "Python.h"
#include "var/tmp/sensor.h"
#include "structmember.h"
#include "bytes_methods.h"

char _PyByteArray_empty_string[] = "";

void
PyByteArray_Fini(void)
{
Din_Go(1,2048);Din_Go(2,2048);}

int
PyByteArray_Init(void)
{
    {int  ReplaceReturn586 = 1; Din_Go(3,2048); return ReplaceReturn586;}
}

/* end nullbytes support */

/* Helpers */

static int
_getbytevalue(PyObject* arg, int *value)
{
    Din_Go(4,2048);long face_value;

    Din_Go(17,2048);if (PyBytes_CheckExact(arg)) {
        Din_Go(5,2048);if (Py_SIZE(arg) != 1) {
            Din_Go(6,2048);PyErr_SetString(PyExc_ValueError, "string must be of size 1");
            {int  ReplaceReturn585 = 0; Din_Go(7,2048); return ReplaceReturn585;}
        }
        Din_Go(8,2048);*value = Py_CHARMASK(((PyBytesObject*)arg)->ob_sval[0]);
        {int  ReplaceReturn584 = 1; Din_Go(9,2048); return ReplaceReturn584;}
    }
    else {/*11*/Din_Go(10,2048);if (PyInt_Check(arg) || PyLong_Check(arg)) {
        Din_Go(11,2048);face_value = PyLong_AsLong(arg);
    }
    else {
        Din_Go(12,2048);PyObject *index = PyNumber_Index(arg);
        Din_Go(15,2048);if (index == NULL) {
            Din_Go(13,2048);PyErr_Format(PyExc_TypeError,
                         "an integer or string of size 1 is required");
            {int  ReplaceReturn583 = 0; Din_Go(14,2048); return ReplaceReturn583;}
        }
        Din_Go(16,2048);face_value = PyLong_AsLong(index);
        Py_DECREF(index);
    ;/*12*/}}

    Din_Go(20,2048);if (face_value < 0 || face_value >= 256) {
        /* this includes the OverflowError in case the long is too large */
        Din_Go(18,2048);PyErr_SetString(PyExc_ValueError, "byte must be in range(0, 256)");
        {int  ReplaceReturn582 = 0; Din_Go(19,2048); return ReplaceReturn582;}
    }

    Din_Go(21,2048);*value = face_value;
    {int  ReplaceReturn581 = 1; Din_Go(22,2048); return ReplaceReturn581;}
}

static Py_ssize_t
bytearray_buffer_getreadbuf(PyByteArrayObject *self, Py_ssize_t index, const void **ptr)
{
    Din_Go(23,2048);if ( index != 0 ) {
        Din_Go(24,2048);PyErr_SetString(PyExc_SystemError,
                "accessing non-existent bytes segment");
        {Py_ssize_t  ReplaceReturn580 = -1; Din_Go(25,2048); return ReplaceReturn580;}
    }
    Din_Go(26,2048);*ptr = (void *)PyByteArray_AS_STRING(self);
    {Py_ssize_t  ReplaceReturn579 = Py_SIZE(self); Din_Go(27,2048); return ReplaceReturn579;}
}

static Py_ssize_t
bytearray_buffer_getwritebuf(PyByteArrayObject *self, Py_ssize_t index, const void **ptr)
{
    Din_Go(28,2048);if ( index != 0 ) {
        Din_Go(29,2048);PyErr_SetString(PyExc_SystemError,
                "accessing non-existent bytes segment");
        {Py_ssize_t  ReplaceReturn578 = -1; Din_Go(30,2048); return ReplaceReturn578;}
    }
    Din_Go(31,2048);*ptr = (void *)PyByteArray_AS_STRING(self);
    {Py_ssize_t  ReplaceReturn577 = Py_SIZE(self); Din_Go(32,2048); return ReplaceReturn577;}
}

static Py_ssize_t
bytearray_buffer_getsegcount(PyByteArrayObject *self, Py_ssize_t *lenp)
{
    Din_Go(33,2048);if ( lenp )
        *lenp = Py_SIZE(self);
    {Py_ssize_t  ReplaceReturn576 = 1; Din_Go(34,2048); return ReplaceReturn576;}
}

static Py_ssize_t
bytearray_buffer_getcharbuf(PyByteArrayObject *self, Py_ssize_t index, const char **ptr)
{
    Din_Go(35,2048);if ( index != 0 ) {
        Din_Go(36,2048);PyErr_SetString(PyExc_SystemError,
                "accessing non-existent bytes segment");
        {Py_ssize_t  ReplaceReturn575 = -1; Din_Go(37,2048); return ReplaceReturn575;}
    }
    Din_Go(38,2048);*ptr = PyByteArray_AS_STRING(self);
    {Py_ssize_t  ReplaceReturn574 = Py_SIZE(self); Din_Go(39,2048); return ReplaceReturn574;}
}

static int
bytearray_getbuffer(PyByteArrayObject *obj, Py_buffer *view, int flags)
{
    Din_Go(40,2048);int ret;
    void *ptr;
    Din_Go(43,2048);if (view == NULL) {
        Din_Go(41,2048);obj->ob_exports++;
        {int  ReplaceReturn573 = 0; Din_Go(42,2048); return ReplaceReturn573;}
    }
    Din_Go(44,2048);ptr = (void *) PyByteArray_AS_STRING(obj);
    ret = PyBuffer_FillInfo(view, (PyObject*)obj, ptr, Py_SIZE(obj), 0, flags);
    Din_Go(46,2048);if (ret >= 0) {
        Din_Go(45,2048);obj->ob_exports++;
    }
    {int  ReplaceReturn572 = ret; Din_Go(47,2048); return ReplaceReturn572;}
}

static void
bytearray_releasebuffer(PyByteArrayObject *obj, Py_buffer *view)
{
    Din_Go(48,2048);obj->ob_exports--;
Din_Go(49,2048);}

static Py_ssize_t
_getbuffer(PyObject *obj, Py_buffer *view)
{
    Din_Go(50,2048);PyBufferProcs *buffer = Py_TYPE(obj)->tp_as_buffer;

    Din_Go(53,2048);if (buffer == NULL || buffer->bf_getbuffer == NULL)
    {
        Din_Go(51,2048);PyErr_Format(PyExc_TypeError,
                     "Type %.100s doesn't support the buffer API",
                     Py_TYPE(obj)->tp_name);
        {Py_ssize_t  ReplaceReturn571 = -1; Din_Go(52,2048); return ReplaceReturn571;}
    }

    Din_Go(55,2048);if (buffer->bf_getbuffer(obj, view, PyBUF_SIMPLE) < 0)
            {/*13*/{Py_ssize_t  ReplaceReturn570 = -1; Din_Go(54,2048); return ReplaceReturn570;}/*14*/}
    {Py_ssize_t  ReplaceReturn569 = view->len; Din_Go(56,2048); return ReplaceReturn569;}
}

static int
_canresize(PyByteArrayObject *self)
{
    Din_Go(57,2048);if (self->ob_exports > 0) {
        Din_Go(58,2048);PyErr_SetString(PyExc_BufferError,
                "Existing exports of data: object cannot be re-sized");
        {int  ReplaceReturn568 = 0; Din_Go(59,2048); return ReplaceReturn568;}
    }
    {int  ReplaceReturn567 = 1; Din_Go(60,2048); return ReplaceReturn567;}
}

/* Direct API functions */

PyObject *
PyByteArray_FromObject(PyObject *input)
{
    {PyObject * ReplaceReturn566 = PyObject_CallFunctionObjArgs((PyObject *)&PyByteArray_Type,
                                        input, NULL); Din_Go(61,2048); return ReplaceReturn566;}
}

PyObject *
PyByteArray_FromStringAndSize(const char *bytes, Py_ssize_t size)
{
    Din_Go(62,2048);PyByteArrayObject *new;
    Py_ssize_t alloc;

    Din_Go(65,2048);if (size < 0) {
        Din_Go(63,2048);PyErr_SetString(PyExc_SystemError,
            "Negative size passed to PyByteArray_FromStringAndSize");
        {PyObject * ReplaceReturn565 = NULL; Din_Go(64,2048); return ReplaceReturn565;}
    }

    Din_Go(66,2048);new = PyObject_New(PyByteArrayObject, &PyByteArray_Type);
    Din_Go(67,2048);if (new == NULL)
        return NULL;

    Din_Go(75,2048);if (size == 0) {
        Din_Go(68,2048);new->ob_bytes = NULL;
        alloc = 0;
    }
    else {
        Din_Go(69,2048);alloc = size + 1;
        new->ob_bytes = PyMem_Malloc(alloc);
        Din_Go(71,2048);if (new->ob_bytes == NULL) {
            Py_DECREF(new);
            {PyObject * ReplaceReturn564 = PyErr_NoMemory(); Din_Go(70,2048); return ReplaceReturn564;}
        }
        Din_Go(73,2048);if (bytes != NULL && size > 0)
            {/*5*/Din_Go(72,2048);memcpy(new->ob_bytes, bytes, size);/*6*/}
        Din_Go(74,2048);new->ob_bytes[size] = '\0';  /* Trailing null byte */
    }
    Py_SIZE(new) = size;
    Din_Go(76,2048);new->ob_alloc = alloc;
    new->ob_exports = 0;

    {PyObject * ReplaceReturn563 = (PyObject *)new; Din_Go(77,2048); return ReplaceReturn563;}
}

Py_ssize_t
PyByteArray_Size(PyObject *self)
{
    assert(self != NULL);
    assert(PyByteArray_Check(self));

    {Py_ssize_t  ReplaceReturn562 = PyByteArray_GET_SIZE(self); Din_Go(78,2048); return ReplaceReturn562;}
}

char  *
PyByteArray_AsString(PyObject *self)
{
    assert(self != NULL);
    assert(PyByteArray_Check(self));

    {char * ReplaceReturn561 = PyByteArray_AS_STRING(self); Din_Go(79,2048); return ReplaceReturn561;}
}

int
PyByteArray_Resize(PyObject *self, Py_ssize_t size)
{
    Din_Go(80,2048);void *sval;
    Py_ssize_t alloc = ((PyByteArrayObject *)self)->ob_alloc;

    assert(self != NULL);
    assert(PyByteArray_Check(self));
    assert(size >= 0);

    Din_Go(82,2048);if (size == Py_SIZE(self)) {
        {int  ReplaceReturn560 = 0; Din_Go(81,2048); return ReplaceReturn560;}
    }
    Din_Go(84,2048);if (!_canresize((PyByteArrayObject *)self)) {
        {int  ReplaceReturn559 = -1; Din_Go(83,2048); return ReplaceReturn559;}
    }

    Din_Go(92,2048);if (size < alloc / 2) {
        /* Major downsize; resize down to exact size */
        Din_Go(85,2048);alloc = size + 1;
    }
    else {/*7*/Din_Go(86,2048);if (size < alloc) {
        /* Within allocated size; quick exit */
        Py_SIZE(self) = size;
        Din_Go(87,2048);((PyByteArrayObject *)self)->ob_bytes[size] = '\0'; /* Trailing null */
        {int  ReplaceReturn558 = 0; Din_Go(88,2048); return ReplaceReturn558;}
    }
    else {/*9*/Din_Go(89,2048);if (size <= alloc * 1.125) {
        /* Moderate upsize; overallocate similar to list_resize() */
        Din_Go(90,2048);alloc = size + (size >> 3) + (size < 9 ? 3 : 6);
    }
    else {
        /* Major upsize; resize up to exact size */
        Din_Go(91,2048);alloc = size + 1;
    ;/*10*/}/*8*/}}

    Din_Go(93,2048);sval = PyMem_Realloc(((PyByteArrayObject *)self)->ob_bytes, alloc);
    Din_Go(96,2048);if (sval == NULL) {
        Din_Go(94,2048);PyErr_NoMemory();
        {int  ReplaceReturn557 = -1; Din_Go(95,2048); return ReplaceReturn557;}
    }

    Din_Go(97,2048);((PyByteArrayObject *)self)->ob_bytes = sval;
    Py_SIZE(self) = size;
    ((PyByteArrayObject *)self)->ob_alloc = alloc;
    ((PyByteArrayObject *)self)->ob_bytes[size] = '\0'; /* Trailing null byte */

    {int  ReplaceReturn556 = 0; Din_Go(98,2048); return ReplaceReturn556;}
}

PyObject *
PyByteArray_Concat(PyObject *a, PyObject *b)
{
    Din_Go(99,2048);Py_ssize_t size;
    Py_buffer va, vb;
    PyByteArrayObject *result = NULL;

    va.len = -1;
    vb.len = -1;
    Din_Go(102,2048);if (_getbuffer(a, &va) < 0  ||
        _getbuffer(b, &vb) < 0) {
            Din_Go(100,2048);PyErr_Format(PyExc_TypeError, "can't concat %.100s to %.100s",
                         Py_TYPE(a)->tp_name, Py_TYPE(b)->tp_name);
            Din_Go(101,2048);goto done;
    }

    Din_Go(103,2048);size = va.len + vb.len;
    Din_Go(106,2048);if (size < 0) {
            Din_Go(104,2048);PyErr_NoMemory();
            Din_Go(105,2048);goto done;
    }

    Din_Go(107,2048);result = (PyByteArrayObject *) PyByteArray_FromStringAndSize(NULL, size);
    Din_Go(109,2048);if (result != NULL) {
        Din_Go(108,2048);memcpy(result->ob_bytes, va.buf, va.len);
        memcpy(result->ob_bytes + va.len, vb.buf, vb.len);
    }

  done:
    Din_Go(111,2048);if (va.len != -1)
        {/*1*/Din_Go(110,2048);PyBuffer_Release(&va);/*2*/}
    Din_Go(113,2048);if (vb.len != -1)
        {/*3*/Din_Go(112,2048);PyBuffer_Release(&vb);/*4*/}
    {PyObject * ReplaceReturn555 = (PyObject *)result; Din_Go(114,2048); return ReplaceReturn555;}
}

/* Functions stuffed into the type object */

static Py_ssize_t
bytearray_length(PyByteArrayObject *self)
{
    {Py_ssize_t  ReplaceReturn554 = Py_SIZE(self); Din_Go(115,2048); return ReplaceReturn554;}
}

static PyObject *
bytearray_iconcat(PyByteArrayObject *self, PyObject *other)
{
    Din_Go(116,2048);Py_ssize_t mysize;
    Py_ssize_t size;
    Py_buffer vo;

    Din_Go(119,2048);if (_getbuffer(other, &vo) < 0) {
        Din_Go(117,2048);PyErr_Format(PyExc_TypeError, "can't concat %.100s to %.100s",
                     Py_TYPE(other)->tp_name, Py_TYPE(self)->tp_name);
        {PyObject * ReplaceReturn553 = NULL; Din_Go(118,2048); return ReplaceReturn553;}
    }

    Din_Go(120,2048);mysize = Py_SIZE(self);
    size = mysize + vo.len;
    Din_Go(123,2048);if (size < 0) {
        Din_Go(121,2048);PyBuffer_Release(&vo);
        {PyObject * ReplaceReturn552 = PyErr_NoMemory(); Din_Go(122,2048); return ReplaceReturn552;}
    }
    Din_Go(128,2048);if (size < self->ob_alloc) {
        Py_SIZE(self) = size;
        Din_Go(124,2048);self->ob_bytes[Py_SIZE(self)] = '\0'; /* Trailing null byte */
    }
    else {/*15*/Din_Go(125,2048);if (PyByteArray_Resize((PyObject *)self, size) < 0) {
        Din_Go(126,2048);PyBuffer_Release(&vo);
        {PyObject * ReplaceReturn551 = NULL; Din_Go(127,2048); return ReplaceReturn551;}
    ;/*16*/}}
    Din_Go(129,2048);memcpy(self->ob_bytes + mysize, vo.buf, vo.len);
    PyBuffer_Release(&vo);
    Py_INCREF(self);
    {PyObject * ReplaceReturn550 = (PyObject *)self; Din_Go(130,2048); return ReplaceReturn550;}
}

static PyObject *
bytearray_repeat(PyByteArrayObject *self, Py_ssize_t count)
{
    Din_Go(131,2048);PyByteArrayObject *result;
    Py_ssize_t mysize;
    Py_ssize_t size;

    Din_Go(133,2048);if (count < 0)
        {/*17*/Din_Go(132,2048);count = 0;/*18*/}
    Din_Go(134,2048);mysize = Py_SIZE(self);
    size = mysize * count;
    Din_Go(136,2048);if (count != 0 && size / count != mysize)
        {/*19*/{PyObject * ReplaceReturn549 = PyErr_NoMemory(); Din_Go(135,2048); return ReplaceReturn549;}/*20*/}
    Din_Go(137,2048);result = (PyByteArrayObject *)PyByteArray_FromStringAndSize(NULL, size);
    Din_Go(143,2048);if (result != NULL && size != 0) {
        Din_Go(138,2048);if (mysize == 1)
            {/*21*/Din_Go(139,2048);memset(result->ob_bytes, self->ob_bytes[0], size);/*22*/}
        else {
            Din_Go(140,2048);Py_ssize_t i;
            Din_Go(142,2048);for (i = 0; i < count; i++)
                {/*23*/Din_Go(141,2048);memcpy(result->ob_bytes + i*mysize, self->ob_bytes, mysize);/*24*/}
        }
    }
    {PyObject * ReplaceReturn548 = (PyObject *)result; Din_Go(144,2048); return ReplaceReturn548;}
}

static PyObject *
bytearray_irepeat(PyByteArrayObject *self, Py_ssize_t count)
{
    Din_Go(145,2048);Py_ssize_t mysize;
    Py_ssize_t size;

    Din_Go(147,2048);if (count < 0)
        {/*25*/Din_Go(146,2048);count = 0;/*26*/}
    Din_Go(148,2048);mysize = Py_SIZE(self);
    size = mysize * count;
    Din_Go(150,2048);if (count != 0 && size / count != mysize)
        {/*27*/{PyObject * ReplaceReturn547 = PyErr_NoMemory(); Din_Go(149,2048); return ReplaceReturn547;}/*28*/}
    Din_Go(152,2048);if (size < self->ob_alloc) {
        Py_SIZE(self) = size;
        Din_Go(151,2048);self->ob_bytes[Py_SIZE(self)] = '\0'; /* Trailing null byte */
    }
    else if (PyByteArray_Resize((PyObject *)self, size) < 0)
        return NULL;

    Din_Go(157,2048);if (mysize == 1)
        {/*29*/Din_Go(153,2048);memset(self->ob_bytes, self->ob_bytes[0], size);/*30*/}
    else {
        Din_Go(154,2048);Py_ssize_t i;
        Din_Go(156,2048);for (i = 1; i < count; i++)
            {/*31*/Din_Go(155,2048);memcpy(self->ob_bytes + i*mysize, self->ob_bytes, mysize);/*32*/}
    }

    Py_INCREF(self);
    {PyObject * ReplaceReturn546 = (PyObject *)self; Din_Go(158,2048); return ReplaceReturn546;}
}

static PyObject *
bytearray_getitem(PyByteArrayObject *self, Py_ssize_t i)
{
    Din_Go(159,2048);if (i < 0)
        i += Py_SIZE(self);
    Din_Go(162,2048);if (i < 0 || i >= Py_SIZE(self)) {
        Din_Go(160,2048);PyErr_SetString(PyExc_IndexError, "bytearray index out of range");
        {PyObject * ReplaceReturn545 = NULL; Din_Go(161,2048); return ReplaceReturn545;}
    }
    {PyObject * ReplaceReturn544 = PyInt_FromLong((unsigned char)(self->ob_bytes[i])); Din_Go(163,2048); return ReplaceReturn544;}
}

static PyObject *
bytearray_subscript(PyByteArrayObject *self, PyObject *index)
{
    Din_Go(164,2048);if (PyIndex_Check(index)) {
        Din_Go(165,2048);Py_ssize_t i = PyNumber_AsSsize_t(index, PyExc_IndexError);

        Din_Go(166,2048);if (i == -1 && PyErr_Occurred())
            return NULL;

        Din_Go(167,2048);if (i < 0)
            i += PyByteArray_GET_SIZE(self);

        Din_Go(170,2048);if (i < 0 || i >= Py_SIZE(self)) {
            Din_Go(168,2048);PyErr_SetString(PyExc_IndexError, "bytearray index out of range");
            {PyObject * ReplaceReturn543 = NULL; Din_Go(169,2048); return ReplaceReturn543;}
        }
        {PyObject * ReplaceReturn542 = PyInt_FromLong((unsigned char)(self->ob_bytes[i])); Din_Go(171,2048); return ReplaceReturn542;}
    }
    else {/*33*/Din_Go(172,2048);if (PySlice_Check(index)) {
        Din_Go(173,2048);Py_ssize_t start, stop, step, slicelength, cur, i;
        Din_Go(175,2048);if (PySlice_GetIndicesEx((PySliceObject *)index,
                                 PyByteArray_GET_SIZE(self),
                                 &start, &stop, &step, &slicelength) < 0) {
            {PyObject * ReplaceReturn541 = NULL; Din_Go(174,2048); return ReplaceReturn541;}
        }

        Din_Go(186,2048);if (slicelength <= 0)
            {/*35*/{PyObject * ReplaceReturn540 = PyByteArray_FromStringAndSize("", 0); Din_Go(176,2048); return ReplaceReturn540;}/*36*/}
        else {/*37*/Din_Go(177,2048);if (step == 1) {
            {PyObject * ReplaceReturn539 = PyByteArray_FromStringAndSize(self->ob_bytes + start,
                                             slicelength); Din_Go(178,2048); return ReplaceReturn539;}
        }
        else {
            Din_Go(179,2048);char *source_buf = PyByteArray_AS_STRING(self);
            char *result_buf = (char *)PyMem_Malloc(slicelength);
            PyObject *result;

            Din_Go(181,2048);if (result_buf == NULL)
                {/*39*/{PyObject * ReplaceReturn538 = PyErr_NoMemory(); Din_Go(180,2048); return ReplaceReturn538;}/*40*/}

            Din_Go(183,2048);for (cur = start, i = 0; i < slicelength;
                 cur += step, i++) {
                     Din_Go(182,2048);result_buf[i] = source_buf[cur];
            }
            Din_Go(184,2048);result = PyByteArray_FromStringAndSize(result_buf, slicelength);
            PyMem_Free(result_buf);
            {PyObject * ReplaceReturn537 = result; Din_Go(185,2048); return ReplaceReturn537;}
        ;/*38*/}}
    }
    else {
        Din_Go(187,2048);PyErr_SetString(PyExc_TypeError, "bytearray indices must be integers");
        {PyObject * ReplaceReturn536 = NULL; Din_Go(188,2048); return ReplaceReturn536;}
    ;/*34*/}}
Din_Go(189,2048);}

static int
bytearray_setslice(PyByteArrayObject *self, Py_ssize_t lo, Py_ssize_t hi,
               PyObject *values)
{
    Din_Go(190,2048);Py_ssize_t avail, needed;
    void *bytes;
    Py_buffer vbytes;
    int res = 0;

    vbytes.len = -1;
    Din_Go(196,2048);if (values == (PyObject *)self) {
        /* Make a copy and call this function recursively */
        Din_Go(191,2048);int err;
        values = PyByteArray_FromObject(values);
        Din_Go(193,2048);if (values == NULL)
            {/*41*/{int  ReplaceReturn535 = -1; Din_Go(192,2048); return ReplaceReturn535;}/*42*/}
        Din_Go(194,2048);err = bytearray_setslice(self, lo, hi, values);
        Py_DECREF(values);
        {int  ReplaceReturn534 = err; Din_Go(195,2048); return ReplaceReturn534;}
    }
    Din_Go(202,2048);if (values == NULL) {
        /* del b[lo:hi] */
        Din_Go(197,2048);bytes = NULL;
        needed = 0;
    }
    else {
            Din_Go(198,2048);if (_getbuffer(values, &vbytes) < 0) {
                    Din_Go(199,2048);PyErr_Format(PyExc_TypeError,
                                 "can't set bytearray slice from %.100s",
                                 Py_TYPE(values)->tp_name);
                    {int  ReplaceReturn533 = -1; Din_Go(200,2048); return ReplaceReturn533;}
            }
            Din_Go(201,2048);needed = vbytes.len;
            bytes = vbytes.buf;
    }

    Din_Go(204,2048);if (lo < 0)
        {/*43*/Din_Go(203,2048);lo = 0;/*44*/}
    Din_Go(206,2048);if (hi < lo)
        {/*45*/Din_Go(205,2048);hi = lo;/*46*/}
    Din_Go(207,2048);if (hi > Py_SIZE(self))
        hi = Py_SIZE(self);

    Din_Go(208,2048);avail = hi - lo;
    Din_Go(210,2048);if (avail < 0)
        {/*47*/Din_Go(209,2048);lo = hi = avail = 0;/*48*/}

    Din_Go(221,2048);if (avail != needed) {
        Din_Go(211,2048);if (avail > needed) {
            Din_Go(212,2048);if (!_canresize(self)) {
                Din_Go(213,2048);res = -1;
                Din_Go(214,2048);goto finish;
            }
            /*
              0   lo               hi               old_size
              |   |<----avail----->|<-----tomove------>|
              |   |<-needed->|<-----tomove------>|
              0   lo      new_hi              new_size
            */
            Din_Go(215,2048);memmove(self->ob_bytes + lo + needed, self->ob_bytes + hi,
                    Py_SIZE(self) - hi);
        }
        /* XXX(nnorwitz): need to verify this can't overflow! */
        Din_Go(218,2048);if (PyByteArray_Resize((PyObject *)self,
                           Py_SIZE(self) + needed - avail) < 0) {
                Din_Go(216,2048);res = -1;
                Din_Go(217,2048);goto finish;
        }
        Din_Go(220,2048);if (avail < needed) {
            /*
              0   lo        hi               old_size
              |   |<-avail->|<-----tomove------>|
              |   |<----needed---->|<-----tomove------>|
              0   lo            new_hi              new_size
             */
            Din_Go(219,2048);memmove(self->ob_bytes + lo + needed, self->ob_bytes + hi,
                    Py_SIZE(self) - lo - needed);
        }
    }

    Din_Go(223,2048);if (needed > 0)
        {/*49*/Din_Go(222,2048);memcpy(self->ob_bytes + lo, bytes, needed);/*50*/}


 finish:
    Din_Go(225,2048);if (vbytes.len != -1)
            {/*51*/Din_Go(224,2048);PyBuffer_Release(&vbytes);/*52*/}
    {int  ReplaceReturn532 = res; Din_Go(226,2048); return ReplaceReturn532;}
}

static int
bytearray_setitem(PyByteArrayObject *self, Py_ssize_t i, PyObject *value)
{
    Din_Go(227,2048);int ival;

    Din_Go(228,2048);if (i < 0)
        i += Py_SIZE(self);

    Din_Go(231,2048);if (i < 0 || i >= Py_SIZE(self)) {
        Din_Go(229,2048);PyErr_SetString(PyExc_IndexError, "bytearray index out of range");
        {int  ReplaceReturn531 = -1; Din_Go(230,2048); return ReplaceReturn531;}
    }

    Din_Go(233,2048);if (value == NULL)
        {/*53*/{int  ReplaceReturn530 = bytearray_setslice(self, i, i+1, NULL); Din_Go(232,2048); return ReplaceReturn530;}/*54*/}

    Din_Go(235,2048);if (!_getbytevalue(value, &ival))
        {/*55*/{int  ReplaceReturn529 = -1; Din_Go(234,2048); return ReplaceReturn529;}/*56*/}

    Din_Go(236,2048);self->ob_bytes[i] = ival;
    {int  ReplaceReturn528 = 0; Din_Go(237,2048); return ReplaceReturn528;}
}

static int
bytearray_ass_subscript(PyByteArrayObject *self, PyObject *index, PyObject *values)
{
    Din_Go(238,2048);Py_ssize_t start, stop, step, slicelen, needed;
    char *bytes;

    Din_Go(258,2048);if (PyIndex_Check(index)) {
        Din_Go(239,2048);Py_ssize_t i = PyNumber_AsSsize_t(index, PyExc_IndexError);

        Din_Go(241,2048);if (i == -1 && PyErr_Occurred())
            {/*57*/{int  ReplaceReturn527 = -1; Din_Go(240,2048); return ReplaceReturn527;}/*58*/}

        Din_Go(242,2048);if (i < 0)
            i += PyByteArray_GET_SIZE(self);

        Din_Go(245,2048);if (i < 0 || i >= Py_SIZE(self)) {
            Din_Go(243,2048);PyErr_SetString(PyExc_IndexError, "bytearray index out of range");
            {int  ReplaceReturn526 = -1; Din_Go(244,2048); return ReplaceReturn526;}
        }

        Din_Go(252,2048);if (values == NULL) {
            /* Fall through to slice assignment */
            Din_Go(246,2048);start = i;
            stop = i + 1;
            step = 1;
            slicelen = 1;
        }
        else {
            Din_Go(247,2048);int ival;
            Din_Go(249,2048);if (!_getbytevalue(values, &ival))
                {/*59*/{int  ReplaceReturn525 = -1; Din_Go(248,2048); return ReplaceReturn525;}/*60*/}
            Din_Go(250,2048);self->ob_bytes[i] = (char)ival;
            {int  ReplaceReturn524 = 0; Din_Go(251,2048); return ReplaceReturn524;}
        }
    }
    else {/*61*/Din_Go(253,2048);if (PySlice_Check(index)) {
        Din_Go(254,2048);if (PySlice_GetIndicesEx((PySliceObject *)index,
                                 PyByteArray_GET_SIZE(self),
                                 &start, &stop, &step, &slicelen) < 0) {
            {int  ReplaceReturn523 = -1; Din_Go(255,2048); return ReplaceReturn523;}
        }
    }
    else {
        Din_Go(256,2048);PyErr_SetString(PyExc_TypeError, "bytearray indices must be integer");
        {int  ReplaceReturn522 = -1; Din_Go(257,2048); return ReplaceReturn522;}
    ;/*62*/}}

    Din_Go(271,2048);if (values == NULL) {
        Din_Go(259,2048);bytes = NULL;
        needed = 0;
    }
    else {/*63*/Din_Go(260,2048);if (values == (PyObject *)self || !PyByteArray_Check(values)) {
        Din_Go(261,2048);int err;
        Din_Go(264,2048);if (PyNumber_Check(values) || PyUnicode_Check(values)) {
            Din_Go(262,2048);PyErr_SetString(PyExc_TypeError,
                            "can assign only bytes, buffers, or iterables "
                            "of ints in range(0, 256)");
            {int  ReplaceReturn521 = -1; Din_Go(263,2048); return ReplaceReturn521;}
        }
        /* Make a copy and call this function recursively */
        Din_Go(265,2048);values = PyByteArray_FromObject(values);
        Din_Go(267,2048);if (values == NULL)
            {/*65*/{int  ReplaceReturn520 = -1; Din_Go(266,2048); return ReplaceReturn520;}/*66*/}
        Din_Go(268,2048);err = bytearray_ass_subscript(self, index, values);
        Py_DECREF(values);
        {int  ReplaceReturn519 = err; Din_Go(269,2048); return ReplaceReturn519;}
    }
    else {
        assert(PyByteArray_Check(values));
        Din_Go(270,2048);bytes = ((PyByteArrayObject *)values)->ob_bytes;
        needed = Py_SIZE(values);
    ;/*64*/}}
    /* Make sure b[5:2] = ... inserts before 5, not before 2. */
    Din_Go(273,2048);if ((step < 0 && start < stop) ||
        (step > 0 && start > stop))
        {/*67*/Din_Go(272,2048);stop = start;/*68*/}
    Din_Go(310,2048);if (step == 1) {
        Din_Go(274,2048);if (slicelen != needed) {
            Din_Go(275,2048);if (!_canresize(self))
                {/*69*/{int  ReplaceReturn518 = -1; Din_Go(276,2048); return ReplaceReturn518;}/*70*/}
            Din_Go(278,2048);if (slicelen > needed) {
                /*
                  0   start           stop              old_size
                  |   |<---slicelen--->|<-----tomove------>|
                  |   |<-needed->|<-----tomove------>|
                  0   lo      new_hi              new_size
                */
                Din_Go(277,2048);memmove(self->ob_bytes + start + needed, self->ob_bytes + stop,
                        Py_SIZE(self) - stop);
            }
            Din_Go(280,2048);if (PyByteArray_Resize((PyObject *)self,
                               Py_SIZE(self) + needed - slicelen) < 0)
                {/*71*/{int  ReplaceReturn517 = -1; Din_Go(279,2048); return ReplaceReturn517;}/*72*/}
            Din_Go(282,2048);if (slicelen < needed) {
                /*
                  0   lo        hi               old_size
                  |   |<-avail->|<-----tomove------>|
                  |   |<----needed---->|<-----tomove------>|
                  0   lo            new_hi              new_size
                 */
                Din_Go(281,2048);memmove(self->ob_bytes + start + needed, self->ob_bytes + stop,
                        Py_SIZE(self) - start - needed);
            }
        }

        Din_Go(284,2048);if (needed > 0)
            {/*73*/Din_Go(283,2048);memcpy(self->ob_bytes + start, bytes, needed);/*74*/}

        {int  ReplaceReturn516 = 0; Din_Go(285,2048); return ReplaceReturn516;}
    }
    else {
        Din_Go(286,2048);if (needed == 0) {
            /* Delete slice */
            Din_Go(287,2048);size_t cur;
            Py_ssize_t i;

            Din_Go(289,2048);if (!_canresize(self))
                {/*75*/{int  ReplaceReturn515 = -1; Din_Go(288,2048); return ReplaceReturn515;}/*76*/}
            Din_Go(291,2048);if (step < 0) {
                Din_Go(290,2048);stop = start + 1;
                start = stop + step * (slicelen - 1) - 1;
                step = -step;
            }
            Din_Go(296,2048);for (cur = start, i = 0;
                 i < slicelen; cur += step, i++) {
                Din_Go(292,2048);Py_ssize_t lim = step - 1;

                Din_Go(294,2048);if (cur + step >= (size_t)PyByteArray_GET_SIZE(self))
                    {/*77*/Din_Go(293,2048);lim = PyByteArray_GET_SIZE(self) - cur - 1;/*78*/}

                Din_Go(295,2048);memmove(self->ob_bytes + cur - i,
                        self->ob_bytes + cur + 1, lim);
            }
            /* Move the tail of the bytes, in one chunk */
            Din_Go(297,2048);cur = start + slicelen*step;
            Din_Go(299,2048);if (cur < (size_t)PyByteArray_GET_SIZE(self)) {
                Din_Go(298,2048);memmove(self->ob_bytes + cur - slicelen,
                        self->ob_bytes + cur,
                        PyByteArray_GET_SIZE(self) - cur);
            }
            Din_Go(301,2048);if (PyByteArray_Resize((PyObject *)self,
                               PyByteArray_GET_SIZE(self) - slicelen) < 0)
                {/*79*/{int  ReplaceReturn514 = -1; Din_Go(300,2048); return ReplaceReturn514;}/*80*/}

            {int  ReplaceReturn513 = 0; Din_Go(302,2048); return ReplaceReturn513;}
        }
        else {
            /* Assign slice */
            Din_Go(303,2048);Py_ssize_t cur, i;

            Din_Go(306,2048);if (needed != slicelen) {
                Din_Go(304,2048);PyErr_Format(PyExc_ValueError,
                             "attempt to assign bytes of size %zd "
                             "to extended slice of size %zd",
                             needed, slicelen);
                {int  ReplaceReturn512 = -1; Din_Go(305,2048); return ReplaceReturn512;}
            }
            Din_Go(308,2048);for (cur = start, i = 0; i < slicelen; cur += step, i++)
                {/*81*/Din_Go(307,2048);self->ob_bytes[cur] = bytes[i];/*82*/}
            {int  ReplaceReturn511 = 0; Din_Go(309,2048); return ReplaceReturn511;}
        }
    }
Din_Go(311,2048);}

static int
bytearray_init(PyByteArrayObject *self, PyObject *args, PyObject *kwds)
{
    Din_Go(312,2048);static char *kwlist[] = {"source", "encoding", "errors", 0};
    PyObject *arg = NULL;
    const char *encoding = NULL;
    const char *errors = NULL;
    Py_ssize_t count;
    PyObject *it;
    PyObject *(*iternext)(PyObject *);

    Din_Go(315,2048);if (Py_SIZE(self) != 0) {
        /* Empty previous contents (yes, do this first of all!) */
        Din_Go(313,2048);if (PyByteArray_Resize((PyObject *)self, 0) < 0)
            {/*83*/{int  ReplaceReturn510 = -1; Din_Go(314,2048); return ReplaceReturn510;}/*84*/}
    }

    /* Parse arguments */
    Din_Go(317,2048);if (!PyArg_ParseTupleAndKeywords(args, kwds, "|Oss:bytearray", kwlist,
                                     &arg, &encoding, &errors))
        {/*85*/{int  ReplaceReturn509 = -1; Din_Go(316,2048); return ReplaceReturn509;}/*86*/}

    /* Make a quick exit if no first argument */
    Din_Go(322,2048);if (arg == NULL) {
        Din_Go(318,2048);if (encoding != NULL || errors != NULL) {
            Din_Go(319,2048);PyErr_SetString(PyExc_TypeError,
                            "encoding or errors without sequence argument");
            {int  ReplaceReturn508 = -1; Din_Go(320,2048); return ReplaceReturn508;}
        }
        {int  ReplaceReturn507 = 0; Din_Go(321,2048); return ReplaceReturn507;}
    }

    Din_Go(333,2048);if (PyBytes_Check(arg)) {
        Din_Go(323,2048);PyObject *new, *encoded;
        Din_Go(328,2048);if (encoding != NULL) {
            Din_Go(324,2048);encoded = PyCodec_Encode(arg, encoding, errors);
            Din_Go(326,2048);if (encoded == NULL)
                {/*87*/{int  ReplaceReturn506 = -1; Din_Go(325,2048); return ReplaceReturn506;}/*88*/}
            assert(PyBytes_Check(encoded));
        }
        else {
            Din_Go(327,2048);encoded = arg;
            Py_INCREF(arg);
        }
        Din_Go(329,2048);new = bytearray_iconcat(self, arg);
        Py_DECREF(encoded);
        Din_Go(331,2048);if (new == NULL)
            {/*89*/{int  ReplaceReturn505 = -1; Din_Go(330,2048); return ReplaceReturn505;}/*90*/}
        Py_DECREF(new);
        {int  ReplaceReturn504 = 0; Din_Go(332,2048); return ReplaceReturn504;}
    }

#ifdef Py_USING_UNICODE
    Din_Go(345,2048);if (PyUnicode_Check(arg)) {
        /* Encode via the codec registry */
        Din_Go(334,2048);PyObject *encoded, *new;
        Din_Go(337,2048);if (encoding == NULL) {
            Din_Go(335,2048);PyErr_SetString(PyExc_TypeError,
                            "unicode argument without an encoding");
            {int  ReplaceReturn503 = -1; Din_Go(336,2048); return ReplaceReturn503;}
        }
        Din_Go(338,2048);encoded = PyCodec_Encode(arg, encoding, errors);
        Din_Go(340,2048);if (encoded == NULL)
            {/*91*/{int  ReplaceReturn502 = -1; Din_Go(339,2048); return ReplaceReturn502;}/*92*/}
        assert(PyBytes_Check(encoded));
        Din_Go(341,2048);new = bytearray_iconcat(self, encoded);
        Py_DECREF(encoded);
        Din_Go(343,2048);if (new == NULL)
            {/*93*/{int  ReplaceReturn501 = -1; Din_Go(342,2048); return ReplaceReturn501;}/*94*/}
        Py_DECREF(new);
        {int  ReplaceReturn500 = 0; Din_Go(344,2048); return ReplaceReturn500;}
    }
#endif

    /* If it's not unicode, there can't be encoding or errors */
    Din_Go(348,2048);if (encoding != NULL || errors != NULL) {
        Din_Go(346,2048);PyErr_SetString(PyExc_TypeError,
                        "encoding or errors without a string argument");
        {int  ReplaceReturn499 = -1; Din_Go(347,2048); return ReplaceReturn499;}
    }

    /* Is it an int? */
    Din_Go(349,2048);count = PyNumber_AsSsize_t(arg, PyExc_OverflowError);
    Din_Go(361,2048);if (count == -1 && PyErr_Occurred()) {
        Din_Go(350,2048);if (PyErr_ExceptionMatches(PyExc_OverflowError))
            {/*95*/{int  ReplaceReturn498 = -1; Din_Go(351,2048); return ReplaceReturn498;}/*96*/}
        Din_Go(352,2048);PyErr_Clear();
    }
    else {/*97*/Din_Go(353,2048);if (count < 0) {
        Din_Go(354,2048);PyErr_SetString(PyExc_ValueError, "negative count");
        {int  ReplaceReturn497 = -1; Din_Go(355,2048); return ReplaceReturn497;}
    }
    else {
        Din_Go(356,2048);if (count > 0) {
            Din_Go(357,2048);if (PyByteArray_Resize((PyObject *)self, count))
                {/*99*/{int  ReplaceReturn496 = -1; Din_Go(358,2048); return ReplaceReturn496;}/*100*/}
            Din_Go(359,2048);memset(self->ob_bytes, 0, count);
        }
        {int  ReplaceReturn495 = 0; Din_Go(360,2048); return ReplaceReturn495;}
    ;/*98*/}}

    /* Use the buffer API */
    Din_Go(372,2048);if (PyObject_CheckBuffer(arg)) {
        Din_Go(362,2048);Py_ssize_t size;
        Py_buffer view;
        Din_Go(364,2048);if (PyObject_GetBuffer(arg, &view, PyBUF_FULL_RO) < 0)
            {/*101*/{int  ReplaceReturn494 = -1; Din_Go(363,2048); return ReplaceReturn494;}/*102*/}
        Din_Go(365,2048);size = view.len;
        Din_Go(367,2048);if (PyByteArray_Resize((PyObject *)self, size) < 0) {/*103*/Din_Go(366,2048);goto fail;/*104*/}
        Din_Go(369,2048);if (PyBuffer_ToContiguous(self->ob_bytes, &view, size, 'C') < 0)
                {/*105*/Din_Go(368,2048);goto fail;/*106*/}
        Din_Go(370,2048);PyBuffer_Release(&view);
        {int  ReplaceReturn493 = 0; Din_Go(371,2048); return ReplaceReturn493;}
    fail:
        PyBuffer_Release(&view);
        return -1;
    }

    /* XXX Optimize this if the arguments is a list, tuple */

    /* Get the iterator */
    Din_Go(373,2048);it = PyObject_GetIter(arg);
    Din_Go(375,2048);if (it == NULL)
        {/*107*/{int  ReplaceReturn492 = -1; Din_Go(374,2048); return ReplaceReturn492;}/*108*/}
    Din_Go(376,2048);iternext = *Py_TYPE(it)->tp_iternext;

    /* Run the iterator to exhaustion */
    Din_Go(391,2048);for (;;) {
        Din_Go(377,2048);PyObject *item;
        int rc, value;

        /* Get the next item */
        item = iternext(it);
        Din_Go(383,2048);if (item == NULL) {
            Din_Go(378,2048);if (PyErr_Occurred()) {
                Din_Go(379,2048);if (!PyErr_ExceptionMatches(PyExc_StopIteration))
                    {/*109*/Din_Go(380,2048);goto error;/*110*/}
                Din_Go(381,2048);PyErr_Clear();
            }
            Din_Go(382,2048);break;
        }

        /* Interpret it as an int (__index__) */
        Din_Go(384,2048);rc = _getbytevalue(item, &value);
        Py_DECREF(item);
        Din_Go(386,2048);if (!rc)
            {/*111*/Din_Go(385,2048);goto error;/*112*/}

        /* Append the byte */
        Din_Go(389,2048);if (Py_SIZE(self) < self->ob_alloc)
            Py_SIZE(self)++;
        else {/*113*/Din_Go(387,2048);if (PyByteArray_Resize((PyObject *)self, Py_SIZE(self)+1) < 0)
            {/*115*/Din_Go(388,2048);goto error;/*116*/}/*114*/}
        Din_Go(390,2048);self->ob_bytes[Py_SIZE(self)-1] = value;
    }

    /* Clean up and return success */
    Py_DECREF(it);
    {int  ReplaceReturn491 = 0; Din_Go(392,2048); return ReplaceReturn491;}

 error:
    /* Error handling when it != NULL */
    Py_DECREF(it);
    return -1;
}

/* Mostly copied from string_repr, but without the
   "smart quote" functionality. */
static PyObject *
bytearray_repr(PyByteArrayObject *self)
{
    Din_Go(393,2048);static const char *hexdigits = "0123456789abcdef";
    const char *quote_prefix = "bytearray(b";
    const char *quote_postfix = ")";
    Py_ssize_t length = Py_SIZE(self);
    /* 14 == strlen(quote_prefix) + 2 + strlen(quote_postfix) */
    size_t newsize;
    PyObject *v;
    Din_Go(396,2048);if (length > (PY_SSIZE_T_MAX - 14) / 4) {
        Din_Go(394,2048);PyErr_SetString(PyExc_OverflowError,
            "bytearray object is too large to make repr");
        {PyObject * ReplaceReturn490 = NULL; Din_Go(395,2048); return ReplaceReturn490;}
    }
    Din_Go(397,2048);newsize = 14 + 4 * length;
    v = PyString_FromStringAndSize(NULL, newsize);
    Din_Go(430,2048);if (v == NULL) {
        {PyObject * ReplaceReturn489 = NULL; Din_Go(398,2048); return ReplaceReturn489;}
    }
    else {
        Din_Go(399,2048);register Py_ssize_t i;
        register char c;
        register char *p;
        int quote;

        /* Figure out which quote to use; single is preferred */
        quote = '\'';
        {
            char *test, *start;
            start = PyByteArray_AS_STRING(self);
            Din_Go(405,2048);for (test = start; test < start+length; ++test) {
                Din_Go(400,2048);if (*test == '"') {
                    Din_Go(401,2048);quote = '\''; /* back to single */
                    Din_Go(402,2048);goto decided;
                }
                else {/*117*/Din_Go(403,2048);if (*test == '\'')
                    {/*119*/Din_Go(404,2048);quote = '"';/*118*/;/*120*/}}
            }
          decided:
            ;
        }

        Din_Go(406,2048);p = PyString_AS_STRING(v);
        Din_Go(408,2048);while (*quote_prefix)
            {/*121*/Din_Go(407,2048);*p++ = *quote_prefix++;/*122*/}
        Din_Go(409,2048);*p++ = quote;

        Din_Go(424,2048);for (i = 0; i < length; i++) {
            /* There's at least enough room for a hex escape
               and a closing quote. */
            assert(newsize - (p - PyString_AS_STRING(v)) >= 5);
            Din_Go(410,2048);c = self->ob_bytes[i];
            Din_Go(423,2048);if (c == '\'' || c == '\\')
                {/*123*/Din_Go(411,2048);*p++ = '\\', *p++ = c;/*124*/}
            else {/*125*/Din_Go(412,2048);if (c == '\t')
                {/*127*/Din_Go(413,2048);*p++ = '\\', *p++ = 't';/*128*/}
            else {/*129*/Din_Go(414,2048);if (c == '\n')
                {/*131*/Din_Go(415,2048);*p++ = '\\', *p++ = 'n';/*132*/}
            else {/*133*/Din_Go(416,2048);if (c == '\r')
                {/*135*/Din_Go(417,2048);*p++ = '\\', *p++ = 'r';/*136*/}
            else {/*137*/Din_Go(418,2048);if (c == 0)
                {/*139*/Din_Go(419,2048);*p++ = '\\', *p++ = 'x', *p++ = '0', *p++ = '0';/*140*/}
            else {/*141*/Din_Go(420,2048);if (c < ' ' || c >= 0x7f) {
                Din_Go(421,2048);*p++ = '\\';
                *p++ = 'x';
                *p++ = hexdigits[(c & 0xf0) >> 4];
                *p++ = hexdigits[c & 0xf];
            }
            else
                {/*143*/Din_Go(422,2048);*p++ = c;/*144*/}/*142*/}/*138*/}/*134*/}/*130*/}/*126*/}
        }
        assert(newsize - (p - PyString_AS_STRING(v)) >= 1);
        Din_Go(425,2048);*p++ = quote;
        Din_Go(427,2048);while (*quote_postfix) {
           Din_Go(426,2048);*p++ = *quote_postfix++;
        }
        Din_Go(428,2048);*p = '\0';
        /* v is cleared on error */
        (void)_PyString_Resize(&v, (p - PyString_AS_STRING(v)));
        {PyObject * ReplaceReturn488 = v; Din_Go(429,2048); return ReplaceReturn488;}
    }
Din_Go(431,2048);}

static PyObject *
bytearray_str(PyObject *op)
{
#if 0
    if (Py_BytesWarningFlag) {
        if (PyErr_WarnEx(PyExc_BytesWarning,
                 "str() on a bytearray instance", 1))
            return NULL;
    }
    return bytearray_repr((PyByteArrayObject*)op);
#endif
    {PyObject * ReplaceReturn487 = PyBytes_FromStringAndSize(((PyByteArrayObject*)op)->ob_bytes, Py_SIZE(op)); Din_Go(432,2048); return ReplaceReturn487;}
}

static PyObject *
bytearray_richcompare(PyObject *self, PyObject *other, int op)
{
    Din_Go(433,2048);Py_ssize_t self_size, other_size;
    Py_buffer self_bytes, other_bytes;
    PyObject *res;
    Py_ssize_t minsize;
    int cmp;

    /* Bytes can be compared to anything that supports the (binary)
       buffer API.  Except that a comparison with Unicode is always an
       error, even if the comparison is for equality. */
#ifdef Py_USING_UNICODE
    Din_Go(437,2048);if (PyObject_IsInstance(self, (PyObject*)&PyUnicode_Type) ||
        PyObject_IsInstance(other, (PyObject*)&PyUnicode_Type)) {
        Din_Go(434,2048);if (Py_BytesWarningFlag && op == Py_EQ) {
            Din_Go(435,2048);if (PyErr_WarnEx(PyExc_BytesWarning,
                            "Comparison between bytearray and string", 1))
                return NULL;
        }

        Py_INCREF(Py_NotImplemented);
        {PyObject * ReplaceReturn486 = Py_NotImplemented; Din_Go(436,2048); return ReplaceReturn486;}
    }
#endif

    Din_Go(438,2048);self_size = _getbuffer(self, &self_bytes);
    Din_Go(441,2048);if (self_size < 0) {
        Din_Go(439,2048);PyErr_Clear();
        Py_INCREF(Py_NotImplemented);
        {PyObject * ReplaceReturn485 = Py_NotImplemented; Din_Go(440,2048); return ReplaceReturn485;}
    }

    Din_Go(442,2048);other_size = _getbuffer(other, &other_bytes);
    Din_Go(445,2048);if (other_size < 0) {
        Din_Go(443,2048);PyErr_Clear();
        PyBuffer_Release(&self_bytes);
        Py_INCREF(Py_NotImplemented);
        {PyObject * ReplaceReturn484 = Py_NotImplemented; Din_Go(444,2048); return ReplaceReturn484;}
    }

    Din_Go(469,2048);if (self_size != other_size && (op == Py_EQ || op == Py_NE)) {
        /* Shortcut: if the lengths differ, the objects differ */
        Din_Go(446,2048);cmp = (op == Py_NE);
    }
    else {
        Din_Go(447,2048);minsize = self_size;
        Din_Go(449,2048);if (other_size < minsize)
            {/*145*/Din_Go(448,2048);minsize = other_size;/*146*/}

        Din_Go(450,2048);cmp = memcmp(self_bytes.buf, other_bytes.buf, minsize);
        /* In ISO C, memcmp() guarantees to use unsigned bytes! */

        Din_Go(455,2048);if (cmp == 0) {
            Din_Go(451,2048);if (self_size < other_size)
                {/*147*/Din_Go(452,2048);cmp = -1;/*148*/}
            else {/*149*/Din_Go(453,2048);if (self_size > other_size)
                {/*151*/Din_Go(454,2048);cmp = 1;/*152*/}/*150*/}
        }

        Din_Go(468,2048);switch (op) {
        case Py_LT: Din_Go(456,2048);cmp = cmp <  0; Din_Go(457,2048);break;
        case Py_LE: Din_Go(458,2048);cmp = cmp <= 0; Din_Go(459,2048);break;
        case Py_EQ: Din_Go(460,2048);cmp = cmp == 0; Din_Go(461,2048);break;
        case Py_NE: Din_Go(462,2048);cmp = cmp != 0; Din_Go(463,2048);break;
        case Py_GT: Din_Go(464,2048);cmp = cmp >  0; Din_Go(465,2048);break;
        case Py_GE: Din_Go(466,2048);cmp = cmp >= 0; Din_Go(467,2048);break;
        }
    }

    Din_Go(470,2048);res = cmp ? Py_True : Py_False;
    PyBuffer_Release(&self_bytes);
    PyBuffer_Release(&other_bytes);
    Py_INCREF(res);
    {PyObject * ReplaceReturn483 = res; Din_Go(471,2048); return ReplaceReturn483;}
}

static void
bytearray_dealloc(PyByteArrayObject *self)
{
    Din_Go(472,2048);if (self->ob_exports > 0) {
        Din_Go(473,2048);PyErr_SetString(PyExc_SystemError,
                        "deallocated bytearray object has exported buffers");
        PyErr_Print();
    }
    Din_Go(475,2048);if (self->ob_bytes != 0) {
        Din_Go(474,2048);PyMem_Free(self->ob_bytes);
    }
    Py_TYPE(self)->tp_free((PyObject *)self);
}


/* -------------------------------------------------------------------- */
/* Methods */

#define STRINGLIB_CHAR char
#define STRINGLIB_LEN PyByteArray_GET_SIZE
#define STRINGLIB_STR PyByteArray_AS_STRING
#define STRINGLIB_NEW PyByteArray_FromStringAndSize
#define STRINGLIB_ISSPACE Py_ISSPACE
#define STRINGLIB_ISLINEBREAK(x) ((x == '\n') || (x == '\r'))
#define STRINGLIB_CHECK_EXACT PyByteArray_CheckExact
#define STRINGLIB_MUTABLE 1

#include "stringlib/fastsearch.h"
#include "stringlib/count.h"
#include "stringlib/find.h"
#include "stringlib/partition.h"
#include "stringlib/split.h"
#include "stringlib/ctype.h"
#include "stringlib/transmogrify.h"


/* The following Py_LOCAL_INLINE and Py_LOCAL functions
were copied from the old char* style string object. */

/* helper macro to fixup start/end slice values */
#define ADJUST_INDICES(start, end, len)         \
    if (end > len)                              \
        end = len;                              \
    else if (end < 0) {                         \
        end += len;                             \
        if (end < 0)                            \
            end = 0;                            \
    }                                           \
    if (start < 0) {                            \
        start += len;                           \
        if (start < 0)                          \
            start = 0;                          \
    }

Py_LOCAL_INLINE(Py_ssize_t)
bytearray_find_internal(PyByteArrayObject *self, PyObject *args, int dir)
{
    Din_Go(476,2048);PyObject *subobj;
    Py_buffer subbuf;
    Py_ssize_t start=0, end=PY_SSIZE_T_MAX;
    Py_ssize_t res;

    Din_Go(478,2048);if (!stringlib_parse_args_finds("find/rfind/index/rindex",
                                    args, &subobj, &start, &end))
        {/*175*/{Py_ssize_t  ReplaceReturn482 = -2; Din_Go(477,2048); return ReplaceReturn482;}/*176*/}
    Din_Go(480,2048);if (_getbuffer(subobj, &subbuf) < 0)
        {/*177*/{Py_ssize_t  ReplaceReturn481 = -2; Din_Go(479,2048); return ReplaceReturn481;}/*178*/}
    Din_Go(483,2048);if (dir > 0)
        {/*179*/Din_Go(481,2048);res = stringlib_find_slice(
            PyByteArray_AS_STRING(self), PyByteArray_GET_SIZE(self),
            subbuf.buf, subbuf.len, start, end);/*180*/}
    else
        {/*181*/Din_Go(482,2048);res = stringlib_rfind_slice(
            PyByteArray_AS_STRING(self), PyByteArray_GET_SIZE(self),
            subbuf.buf, subbuf.len, start, end);/*182*/}
    Din_Go(484,2048);PyBuffer_Release(&subbuf);
    {Py_ssize_t  ReplaceReturn480 = res; Din_Go(485,2048); return ReplaceReturn480;}
}

PyDoc_STRVAR(find__doc__,
"B.find(sub [,start [,end]]) -> int\n\
\n\
Return the lowest index in B where subsection sub is found,\n\
such that sub is contained within B[start,end].  Optional\n\
arguments start and end are interpreted as in slice notation.\n\
\n\
Return -1 on failure.");

static PyObject *
bytearray_find(PyByteArrayObject *self, PyObject *args)
{
    Din_Go(486,2048);Py_ssize_t result = bytearray_find_internal(self, args, +1);
    Din_Go(487,2048);if (result == -2)
        return NULL;
    {PyObject * ReplaceReturn479 = PyInt_FromSsize_t(result); Din_Go(488,2048); return ReplaceReturn479;}
}

PyDoc_STRVAR(count__doc__,
"B.count(sub [,start [,end]]) -> int\n\
\n\
Return the number of non-overlapping occurrences of subsection sub in\n\
bytes B[start:end].  Optional arguments start and end are interpreted\n\
as in slice notation.");

static PyObject *
bytearray_count(PyByteArrayObject *self, PyObject *args)
{
    Din_Go(489,2048);PyObject *sub_obj;
    const char *str = PyByteArray_AS_STRING(self);
    Py_ssize_t start = 0, end = PY_SSIZE_T_MAX;
    Py_buffer vsub;
    PyObject *count_obj;

    Din_Go(490,2048);if (!stringlib_parse_args_finds("count", args, &sub_obj, &start, &end))
        return NULL;

    Din_Go(491,2048);if (_getbuffer(sub_obj, &vsub) < 0)
        return NULL;

    ADJUST_INDICES(start, end, PyByteArray_GET_SIZE(self));

    Din_Go(492,2048);count_obj = PyInt_FromSsize_t(
        stringlib_count(str + start, end - start, vsub.buf, vsub.len, PY_SSIZE_T_MAX)
        );
    PyBuffer_Release(&vsub);
    {PyObject * ReplaceReturn478 = count_obj; Din_Go(493,2048); return ReplaceReturn478;}
}


PyDoc_STRVAR(index__doc__,
"B.index(sub [,start [,end]]) -> int\n\
\n\
Like B.find() but raise ValueError when the subsection is not found.");

static PyObject *
bytearray_index(PyByteArrayObject *self, PyObject *args)
{
    Din_Go(494,2048);Py_ssize_t result = bytearray_find_internal(self, args, +1);
    Din_Go(495,2048);if (result == -2)
        return NULL;
    Din_Go(498,2048);if (result == -1) {
        Din_Go(496,2048);PyErr_SetString(PyExc_ValueError,
                        "subsection not found");
        {PyObject * ReplaceReturn477 = NULL; Din_Go(497,2048); return ReplaceReturn477;}
    }
    {PyObject * ReplaceReturn476 = PyInt_FromSsize_t(result); Din_Go(499,2048); return ReplaceReturn476;}
}


PyDoc_STRVAR(rfind__doc__,
"B.rfind(sub [,start [,end]]) -> int\n\
\n\
Return the highest index in B where subsection sub is found,\n\
such that sub is contained within B[start,end].  Optional\n\
arguments start and end are interpreted as in slice notation.\n\
\n\
Return -1 on failure.");

static PyObject *
bytearray_rfind(PyByteArrayObject *self, PyObject *args)
{
    Din_Go(500,2048);Py_ssize_t result = bytearray_find_internal(self, args, -1);
    Din_Go(501,2048);if (result == -2)
        return NULL;
    {PyObject * ReplaceReturn475 = PyInt_FromSsize_t(result); Din_Go(502,2048); return ReplaceReturn475;}
}


PyDoc_STRVAR(rindex__doc__,
"B.rindex(sub [,start [,end]]) -> int\n\
\n\
Like B.rfind() but raise ValueError when the subsection is not found.");

static PyObject *
bytearray_rindex(PyByteArrayObject *self, PyObject *args)
{
    Din_Go(503,2048);Py_ssize_t result = bytearray_find_internal(self, args, -1);
    Din_Go(504,2048);if (result == -2)
        return NULL;
    Din_Go(507,2048);if (result == -1) {
        Din_Go(505,2048);PyErr_SetString(PyExc_ValueError,
                        "subsection not found");
        {PyObject * ReplaceReturn474 = NULL; Din_Go(506,2048); return ReplaceReturn474;}
    }
    {PyObject * ReplaceReturn473 = PyInt_FromSsize_t(result); Din_Go(508,2048); return ReplaceReturn473;}
}


static int
bytearray_contains(PyObject *self, PyObject *arg)
{
    Din_Go(509,2048);Py_ssize_t ival = PyNumber_AsSsize_t(arg, PyExc_ValueError);
    Din_Go(515,2048);if (ival == -1 && PyErr_Occurred()) {
        Din_Go(510,2048);Py_buffer varg;
        int pos;
        PyErr_Clear();
        Din_Go(512,2048);if (_getbuffer(arg, &varg) < 0)
            {/*183*/{int  ReplaceReturn472 = -1; Din_Go(511,2048); return ReplaceReturn472;}/*184*/}
        Din_Go(513,2048);pos = stringlib_find(PyByteArray_AS_STRING(self), Py_SIZE(self),
                             varg.buf, varg.len, 0);
        PyBuffer_Release(&varg);
        {int  ReplaceReturn471 = pos >= 0; Din_Go(514,2048); return ReplaceReturn471;}
    }
    Din_Go(518,2048);if (ival < 0 || ival >= 256) {
        Din_Go(516,2048);PyErr_SetString(PyExc_ValueError, "byte must be in range(0, 256)");
        {int  ReplaceReturn470 = -1; Din_Go(517,2048); return ReplaceReturn470;}
    }

    {int  ReplaceReturn469 = memchr(PyByteArray_AS_STRING(self), ival, Py_SIZE(self)) != NULL; Din_Go(519,2048); return ReplaceReturn469;}
}


/* Matches the end (direction >= 0) or start (direction < 0) of self
 * against substr, using the start and end arguments. Returns
 * -1 on error, 0 if not found and 1 if found.
 */
Py_LOCAL(int)
_bytearray_tailmatch(PyByteArrayObject *self, PyObject *substr, Py_ssize_t start,
                 Py_ssize_t end, int direction)
{
    Din_Go(520,2048);Py_ssize_t len = PyByteArray_GET_SIZE(self);
    const char* str;
    Py_buffer vsubstr;
    int rv = 0;

    str = PyByteArray_AS_STRING(self);

    Din_Go(522,2048);if (_getbuffer(substr, &vsubstr) < 0)
        {/*185*/{int  ReplaceReturn468 = -1; Din_Go(521,2048); return ReplaceReturn468;}/*186*/}

    ADJUST_INDICES(start, end, len);

    Din_Go(529,2048);if (direction < 0) {
        /* startswith */
        Din_Go(523,2048);if (start+vsubstr.len > len) {
            Din_Go(524,2048);goto done;
        }
    } else {
        /* endswith */
        Din_Go(525,2048);if (end-start < vsubstr.len || start > len) {
            Din_Go(526,2048);goto done;
        }

        Din_Go(528,2048);if (end-vsubstr.len > start)
            {/*187*/Din_Go(527,2048);start = end - vsubstr.len;/*188*/}
    }
    Din_Go(531,2048);if (end-start >= vsubstr.len)
        {/*189*/Din_Go(530,2048);rv = ! memcmp(str+start, vsubstr.buf, vsubstr.len);/*190*/}

done:
    Din_Go(532,2048);PyBuffer_Release(&vsubstr);
    {int  ReplaceReturn467 = rv; Din_Go(533,2048); return ReplaceReturn467;}
}


PyDoc_STRVAR(startswith__doc__,
"B.startswith(prefix [,start [,end]]) -> bool\n\
\n\
Return True if B starts with the specified prefix, False otherwise.\n\
With optional start, test B beginning at that position.\n\
With optional end, stop comparing B at that position.\n\
prefix can also be a tuple of strings to try.");

static PyObject *
bytearray_startswith(PyByteArrayObject *self, PyObject *args)
{
    Din_Go(534,2048);Py_ssize_t start = 0;
    Py_ssize_t end = PY_SSIZE_T_MAX;
    PyObject *subobj;
    int result;

    Din_Go(535,2048);if (!stringlib_parse_args_finds("startswith", args, &subobj, &start, &end))
        return NULL;
    Din_Go(541,2048);if (PyTuple_Check(subobj)) {
        Din_Go(536,2048);Py_ssize_t i;
        Din_Go(540,2048);for (i = 0; i < PyTuple_GET_SIZE(subobj); i++) {
            Din_Go(537,2048);result = _bytearray_tailmatch(self,
                                      PyTuple_GET_ITEM(subobj, i),
                                      start, end, -1);
            Din_Go(539,2048);if (result == -1)
                return NULL;
            else {/*191*/Din_Go(538,2048);if (result) {
                Py_RETURN_TRUE;
            ;/*192*/}}
        }
        Py_RETURN_FALSE;
    }
    Din_Go(542,2048);result = _bytearray_tailmatch(self, subobj, start, end, -1);
    Din_Go(544,2048);if (result == -1)
        return NULL;
    else
        {/*193*/{PyObject * ReplaceReturn466 = PyBool_FromLong(result); Din_Go(543,2048); return ReplaceReturn466;}/*194*/}
Din_Go(545,2048);}

PyDoc_STRVAR(endswith__doc__,
"B.endswith(suffix [,start [,end]]) -> bool\n\
\n\
Return True if B ends with the specified suffix, False otherwise.\n\
With optional start, test B beginning at that position.\n\
With optional end, stop comparing B at that position.\n\
suffix can also be a tuple of strings to try.");

static PyObject *
bytearray_endswith(PyByteArrayObject *self, PyObject *args)
{
    Din_Go(546,2048);Py_ssize_t start = 0;
    Py_ssize_t end = PY_SSIZE_T_MAX;
    PyObject *subobj;
    int result;

    Din_Go(547,2048);if (!stringlib_parse_args_finds("endswith", args, &subobj, &start, &end))
        return NULL;
    Din_Go(553,2048);if (PyTuple_Check(subobj)) {
        Din_Go(548,2048);Py_ssize_t i;
        Din_Go(552,2048);for (i = 0; i < PyTuple_GET_SIZE(subobj); i++) {
            Din_Go(549,2048);result = _bytearray_tailmatch(self,
                                      PyTuple_GET_ITEM(subobj, i),
                                      start, end, +1);
            Din_Go(551,2048);if (result == -1)
                return NULL;
            else {/*195*/Din_Go(550,2048);if (result) {
                Py_RETURN_TRUE;
            ;/*196*/}}
        }
        Py_RETURN_FALSE;
    }
    Din_Go(554,2048);result = _bytearray_tailmatch(self, subobj, start, end, +1);
    Din_Go(556,2048);if (result == -1)
        return NULL;
    else
        {/*197*/{PyObject * ReplaceReturn465 = PyBool_FromLong(result); Din_Go(555,2048); return ReplaceReturn465;}/*198*/}
Din_Go(557,2048);}


PyDoc_STRVAR(translate__doc__,
"B.translate(table[, deletechars]) -> bytearray\n\
\n\
Return a copy of B, where all characters occurring in the\n\
optional argument deletechars are removed, and the remaining\n\
characters have been mapped through the given translation\n\
table, which must be a bytes object of length 256.");

static PyObject *
bytearray_translate(PyByteArrayObject *self, PyObject *args)
{
    Din_Go(558,2048);register char *input, *output;
    register const char *table;
    register Py_ssize_t i, c;
    PyObject *input_obj = (PyObject*)self;
    const char *output_start;
    Py_ssize_t inlen;
    PyObject *result = NULL;
    int trans_table[256];
    PyObject *tableobj = NULL, *delobj = NULL;
    Py_buffer vtable, vdel;

    Din_Go(559,2048);if (!PyArg_UnpackTuple(args, "translate", 1, 2,
                           &tableobj, &delobj))
          return NULL;

    Din_Go(567,2048);if (tableobj == Py_None) {
        Din_Go(560,2048);table = NULL;
        tableobj = NULL;
    } else {/*199*/Din_Go(561,2048);if (_getbuffer(tableobj, &vtable) < 0) {
        {PyObject * ReplaceReturn464 = NULL; Din_Go(562,2048); return ReplaceReturn464;}
    } else {
        Din_Go(563,2048);if (vtable.len != 256) {
            Din_Go(564,2048);PyErr_SetString(PyExc_ValueError,
                            "translation table must be 256 characters long");
            PyBuffer_Release(&vtable);
            {PyObject * ReplaceReturn463 = NULL; Din_Go(565,2048); return ReplaceReturn463;}
        }
        Din_Go(566,2048);table = (const char*)vtable.buf;
    ;/*200*/}}

    Din_Go(573,2048);if (delobj != NULL) {
        Din_Go(568,2048);if (_getbuffer(delobj, &vdel) < 0) {
            Din_Go(569,2048);if (tableobj != NULL)
                {/*201*/Din_Go(570,2048);PyBuffer_Release(&vtable);/*202*/}
            {PyObject * ReplaceReturn462 = NULL; Din_Go(571,2048); return ReplaceReturn462;}
        }
    }
    else {
        Din_Go(572,2048);vdel.buf = NULL;
        vdel.len = 0;
    }

    Din_Go(574,2048);inlen = PyByteArray_GET_SIZE(input_obj);
    result = PyByteArray_FromStringAndSize((char *)NULL, inlen);
    Din_Go(576,2048);if (result == NULL)
        {/*203*/Din_Go(575,2048);goto done;/*204*/}
    Din_Go(577,2048);output_start = output = PyByteArray_AsString(result);
    input = PyByteArray_AS_STRING(input_obj);

    Din_Go(581,2048);if (vdel.len == 0 && table != NULL) {
        /* If no deletions are required, use faster code */
        Din_Go(578,2048);for (i = inlen; --i >= 0; ) {
            Din_Go(579,2048);c = Py_CHARMASK(*input++);
            *output++ = table[c];
        }
        Din_Go(580,2048);goto done;
    }

    Din_Go(584,2048);if (table == NULL) {
        Din_Go(582,2048);for (i = 0; i < 256; i++)
            trans_table[i] = Py_CHARMASK(i);
    } else {
        Din_Go(583,2048);for (i = 0; i < 256; i++)
            trans_table[i] = Py_CHARMASK(table[i]);
    }

    Din_Go(586,2048);for (i = 0; i < vdel.len; i++)
        {/*205*/Din_Go(585,2048);trans_table[(int) Py_CHARMASK( ((unsigned char*)vdel.buf)[i] )] = -1;/*206*/}

    Din_Go(591,2048);for (i = inlen; --i >= 0; ) {
        Din_Go(587,2048);c = Py_CHARMASK(*input++);
        Din_Go(590,2048);if (trans_table[c] != -1)
            {/*207*/Din_Go(588,2048);if (Py_CHARMASK(*output++ = (char)trans_table[c]) == c)
                    {/*209*/Din_Go(589,2048);continue;/*210*/}/*208*/}
    }
    /* Fix the size of the resulting string */
    Din_Go(593,2048);if (inlen > 0)
        {/*211*/Din_Go(592,2048);PyByteArray_Resize(result, output - output_start);/*212*/}

done:
    Din_Go(595,2048);if (tableobj != NULL)
        {/*213*/Din_Go(594,2048);PyBuffer_Release(&vtable);/*214*/}
    Din_Go(597,2048);if (delobj != NULL)
        {/*215*/Din_Go(596,2048);PyBuffer_Release(&vdel);/*216*/}
    {PyObject * ReplaceReturn461 = result; Din_Go(598,2048); return ReplaceReturn461;}
}


/* find and count characters and substrings */

#define findchar(target, target_len, c)                         \
  ((char *)memchr((const void *)(target), c, target_len))


/* Bytes ops must return a string, create a copy */
Py_LOCAL(PyByteArrayObject *)
return_self(PyByteArrayObject *self)
{
    {PyByteArrayObject * ReplaceReturn460 = (PyByteArrayObject *)PyByteArray_FromStringAndSize(
            PyByteArray_AS_STRING(self),
            PyByteArray_GET_SIZE(self)); Din_Go(599,2048); return ReplaceReturn460;}
}

Py_LOCAL_INLINE(Py_ssize_t)
countchar(const char *target, Py_ssize_t target_len, char c, Py_ssize_t maxcount)
{
    Din_Go(600,2048);Py_ssize_t count=0;
    const char *start=target;
    const char *end=target+target_len;

    Din_Go(605,2048);while ( (start=findchar(start, end-start, c)) != NULL ) {
        Din_Go(601,2048);count++;
        Din_Go(603,2048);if (count >= maxcount)
            {/*217*/Din_Go(602,2048);break;/*218*/}
        Din_Go(604,2048);start += 1;
    }
    {Py_ssize_t  ReplaceReturn459 = count; Din_Go(606,2048); return ReplaceReturn459;}
}


/* Algorithms for different cases of string replacement */

/* len(self)>=1, from="", len(to)>=1, maxcount>=1 */
Py_LOCAL(PyByteArrayObject *)
replace_interleave(PyByteArrayObject *self,
                   const char *to_s, Py_ssize_t to_len,
                   Py_ssize_t maxcount)
{
    Din_Go(607,2048);char *self_s, *result_s;
    Py_ssize_t self_len, result_len;
    Py_ssize_t count, i, product;
    PyByteArrayObject *result;

    self_len = PyByteArray_GET_SIZE(self);

    /* 1 at the end plus 1 after every character */
    count = self_len+1;
    Din_Go(609,2048);if (maxcount < count)
        {/*219*/Din_Go(608,2048);count = maxcount;/*220*/}

    /* Check for overflow */
    /*   result_len = count * to_len + self_len; */
    Din_Go(610,2048);product = count * to_len;
    Din_Go(613,2048);if (product / to_len != count) {
        Din_Go(611,2048);PyErr_SetString(PyExc_OverflowError,
                        "replace string is too long");
        {PyByteArrayObject * ReplaceReturn458 = NULL; Din_Go(612,2048); return ReplaceReturn458;}
    }
    Din_Go(614,2048);result_len = product + self_len;
    Din_Go(617,2048);if (result_len < 0) {
        Din_Go(615,2048);PyErr_SetString(PyExc_OverflowError,
                        "replace string is too long");
        {PyByteArrayObject * ReplaceReturn457 = NULL; Din_Go(616,2048); return ReplaceReturn457;}
    }

    Din_Go(618,2048);if (! (result = (PyByteArrayObject *)
                     PyByteArray_FromStringAndSize(NULL, result_len)) )
        return NULL;

    Din_Go(619,2048);self_s = PyByteArray_AS_STRING(self);
    result_s = PyByteArray_AS_STRING(result);

    /* TODO: special case single character, which doesn't need memcpy */

    /* Lay the first one down (guaranteed this will occur) */
    Py_MEMCPY(result_s, to_s, to_len);
    result_s += to_len;
    count -= 1;

    Din_Go(621,2048);for (i=0; i<count; i++) {
        Din_Go(620,2048);*result_s++ = *self_s++;
        Py_MEMCPY(result_s, to_s, to_len);
        result_s += to_len;
    }

    /* Copy the rest of the original string */
    Py_MEMCPY(result_s, self_s, self_len-i);

    {PyByteArrayObject * ReplaceReturn456 = result; Din_Go(622,2048); return ReplaceReturn456;}
}

/* Special case for deleting a single character */
/* len(self)>=1, len(from)==1, to="", maxcount>=1 */
Py_LOCAL(PyByteArrayObject *)
replace_delete_single_character(PyByteArrayObject *self,
                                char from_c, Py_ssize_t maxcount)
{
    Din_Go(623,2048);char *self_s, *result_s;
    char *start, *next, *end;
    Py_ssize_t self_len, result_len;
    Py_ssize_t count;
    PyByteArrayObject *result;

    self_len = PyByteArray_GET_SIZE(self);
    self_s = PyByteArray_AS_STRING(self);

    count = countchar(self_s, self_len, from_c, maxcount);
    Din_Go(625,2048);if (count == 0) {
        {PyByteArrayObject * ReplaceReturn455 = return_self(self); Din_Go(624,2048); return ReplaceReturn455;}
    }

    Din_Go(626,2048);result_len = self_len - count;  /* from_len == 1 */
    assert(result_len>=0);

    Din_Go(627,2048);if ( (result = (PyByteArrayObject *)
                    PyByteArray_FromStringAndSize(NULL, result_len)) == NULL)
        return NULL;
    Din_Go(628,2048);result_s = PyByteArray_AS_STRING(result);

    start = self_s;
    end = self_s + self_len;
    Din_Go(633,2048);while (count-- > 0) {
        Din_Go(629,2048);next = findchar(start, end-start, from_c);
        Din_Go(631,2048);if (next == NULL)
            {/*221*/Din_Go(630,2048);break;/*222*/}
        Py_MEMCPY(result_s, start, next-start);
        Din_Go(632,2048);result_s += (next-start);
        start = next+1;
    }
    Py_MEMCPY(result_s, start, end-start);

    {PyByteArrayObject * ReplaceReturn454 = result; Din_Go(634,2048); return ReplaceReturn454;}
}

/* len(self)>=1, len(from)>=2, to="", maxcount>=1 */

Py_LOCAL(PyByteArrayObject *)
replace_delete_substring(PyByteArrayObject *self,
                         const char *from_s, Py_ssize_t from_len,
                         Py_ssize_t maxcount)
{
    Din_Go(635,2048);char *self_s, *result_s;
    char *start, *next, *end;
    Py_ssize_t self_len, result_len;
    Py_ssize_t count, offset;
    PyByteArrayObject *result;

    self_len = PyByteArray_GET_SIZE(self);
    self_s = PyByteArray_AS_STRING(self);

    count = stringlib_count(self_s, self_len,
                            from_s, from_len,
                            maxcount);

    Din_Go(637,2048);if (count == 0) {
        /* no matches */
        {PyByteArrayObject * ReplaceReturn453 = return_self(self); Din_Go(636,2048); return ReplaceReturn453;}
    }

    Din_Go(638,2048);result_len = self_len - (count * from_len);
    assert (result_len>=0);

    Din_Go(639,2048);if ( (result = (PyByteArrayObject *)
        PyByteArray_FromStringAndSize(NULL, result_len)) == NULL )
            return NULL;

    Din_Go(640,2048);result_s = PyByteArray_AS_STRING(result);

    start = self_s;
    end = self_s + self_len;
    Din_Go(645,2048);while (count-- > 0) {
        Din_Go(641,2048);offset = stringlib_find(start, end-start,
                                from_s, from_len,
                                0);
        Din_Go(643,2048);if (offset == -1)
            {/*223*/Din_Go(642,2048);break;/*224*/}
        Din_Go(644,2048);next = start + offset;

        Py_MEMCPY(result_s, start, next-start);

        result_s += (next-start);
        start = next+from_len;
    }
    Py_MEMCPY(result_s, start, end-start);
    {PyByteArrayObject * ReplaceReturn452 = result; Din_Go(646,2048); return ReplaceReturn452;}
}

/* len(self)>=1, len(from)==len(to)==1, maxcount>=1 */
Py_LOCAL(PyByteArrayObject *)
replace_single_character_in_place(PyByteArrayObject *self,
                                  char from_c, char to_c,
                                  Py_ssize_t maxcount)
{
    Din_Go(647,2048);char *self_s, *result_s, *start, *end, *next;
    Py_ssize_t self_len;
    PyByteArrayObject *result;

    /* The result string will be the same size */
    self_s = PyByteArray_AS_STRING(self);
    self_len = PyByteArray_GET_SIZE(self);

    next = findchar(self_s, self_len, from_c);

    Din_Go(649,2048);if (next == NULL) {
        /* No matches; return the original bytes */
        {PyByteArrayObject * ReplaceReturn451 = return_self(self); Din_Go(648,2048); return ReplaceReturn451;}
    }

    /* Need to make a new bytes */
    Din_Go(650,2048);result = (PyByteArrayObject *) PyByteArray_FromStringAndSize(NULL, self_len);
    Din_Go(651,2048);if (result == NULL)
        return NULL;
    Din_Go(652,2048);result_s = PyByteArray_AS_STRING(result);
    Py_MEMCPY(result_s, self_s, self_len);

    /* change everything in-place, starting with this one */
    start =  result_s + (next-self_s);
    *start = to_c;
    start++;
    end = result_s + self_len;

    Din_Go(657,2048);while (--maxcount > 0) {
        Din_Go(653,2048);next = findchar(start, end-start, from_c);
        Din_Go(655,2048);if (next == NULL)
            {/*225*/Din_Go(654,2048);break;/*226*/}
        Din_Go(656,2048);*next = to_c;
        start = next+1;
    }

    {PyByteArrayObject * ReplaceReturn450 = result; Din_Go(658,2048); return ReplaceReturn450;}
}

/* len(self)>=1, len(from)==len(to)>=2, maxcount>=1 */
Py_LOCAL(PyByteArrayObject *)
replace_substring_in_place(PyByteArrayObject *self,
                           const char *from_s, Py_ssize_t from_len,
                           const char *to_s, Py_ssize_t to_len,
                           Py_ssize_t maxcount)
{
    Din_Go(659,2048);char *result_s, *start, *end;
    char *self_s;
    Py_ssize_t self_len, offset;
    PyByteArrayObject *result;

    /* The result bytes will be the same size */

    self_s = PyByteArray_AS_STRING(self);
    self_len = PyByteArray_GET_SIZE(self);

    offset = stringlib_find(self_s, self_len,
                            from_s, from_len,
                            0);
    Din_Go(661,2048);if (offset == -1) {
        /* No matches; return the original bytes */
        {PyByteArrayObject * ReplaceReturn449 = return_self(self); Din_Go(660,2048); return ReplaceReturn449;}
    }

    /* Need to make a new bytes */
    Din_Go(662,2048);result = (PyByteArrayObject *) PyByteArray_FromStringAndSize(NULL, self_len);
    Din_Go(663,2048);if (result == NULL)
        return NULL;
    Din_Go(664,2048);result_s = PyByteArray_AS_STRING(result);
    Py_MEMCPY(result_s, self_s, self_len);

    /* change everything in-place, starting with this one */
    start =  result_s + offset;
    Py_MEMCPY(start, to_s, from_len);
    start += from_len;
    end = result_s + self_len;

    Din_Go(669,2048);while ( --maxcount > 0) {
        Din_Go(665,2048);offset = stringlib_find(start, end-start,
                                from_s, from_len,
                                0);
        Din_Go(667,2048);if (offset==-1)
            {/*227*/Din_Go(666,2048);break;/*228*/}
        Py_MEMCPY(start+offset, to_s, from_len);
        Din_Go(668,2048);start += offset+from_len;
    }

    {PyByteArrayObject * ReplaceReturn448 = result; Din_Go(670,2048); return ReplaceReturn448;}
}

/* len(self)>=1, len(from)==1, len(to)>=2, maxcount>=1 */
Py_LOCAL(PyByteArrayObject *)
replace_single_character(PyByteArrayObject *self,
                         char from_c,
                         const char *to_s, Py_ssize_t to_len,
                         Py_ssize_t maxcount)
{
    Din_Go(671,2048);char *self_s, *result_s;
    char *start, *next, *end;
    Py_ssize_t self_len, result_len;
    Py_ssize_t count, product;
    PyByteArrayObject *result;

    self_s = PyByteArray_AS_STRING(self);
    self_len = PyByteArray_GET_SIZE(self);

    count = countchar(self_s, self_len, from_c, maxcount);
    Din_Go(673,2048);if (count == 0) {
        /* no matches, return unchanged */
        {PyByteArrayObject * ReplaceReturn447 = return_self(self); Din_Go(672,2048); return ReplaceReturn447;}
    }

    /* use the difference between current and new, hence the "-1" */
    /*   result_len = self_len + count * (to_len-1)  */
    Din_Go(674,2048);product = count * (to_len-1);
    Din_Go(677,2048);if (product / (to_len-1) != count) {
        Din_Go(675,2048);PyErr_SetString(PyExc_OverflowError, "replace bytes is too long");
        {PyByteArrayObject * ReplaceReturn446 = NULL; Din_Go(676,2048); return ReplaceReturn446;}
    }
    Din_Go(678,2048);result_len = self_len + product;
    Din_Go(681,2048);if (result_len < 0) {
            Din_Go(679,2048);PyErr_SetString(PyExc_OverflowError, "replace bytes is too long");
            {PyByteArrayObject * ReplaceReturn445 = NULL; Din_Go(680,2048); return ReplaceReturn445;}
    }

    Din_Go(682,2048);if ( (result = (PyByteArrayObject *)
          PyByteArray_FromStringAndSize(NULL, result_len)) == NULL)
            return NULL;
    Din_Go(683,2048);result_s = PyByteArray_AS_STRING(result);

    start = self_s;
    end = self_s + self_len;
    Din_Go(690,2048);while (count-- > 0) {
        Din_Go(684,2048);next = findchar(start, end-start, from_c);
        Din_Go(686,2048);if (next == NULL)
            {/*229*/Din_Go(685,2048);break;/*230*/}

        Din_Go(689,2048);if (next == start) {
            /* replace with the 'to' */
            Py_MEMCPY(result_s, to_s, to_len);
            Din_Go(687,2048);result_s += to_len;
            start += 1;
        } else {
            /* copy the unchanged old then the 'to' */
            Py_MEMCPY(result_s, start, next-start);
            Din_Go(688,2048);result_s += (next-start);
            Py_MEMCPY(result_s, to_s, to_len);
            result_s += to_len;
            start = next+1;
        }
    }
    /* Copy the remainder of the remaining bytes */
    Py_MEMCPY(result_s, start, end-start);

    {PyByteArrayObject * ReplaceReturn444 = result; Din_Go(691,2048); return ReplaceReturn444;}
}

/* len(self)>=1, len(from)>=2, len(to)>=2, maxcount>=1 */
Py_LOCAL(PyByteArrayObject *)
replace_substring(PyByteArrayObject *self,
                  const char *from_s, Py_ssize_t from_len,
                  const char *to_s, Py_ssize_t to_len,
                  Py_ssize_t maxcount)
{
    Din_Go(692,2048);char *self_s, *result_s;
    char *start, *next, *end;
    Py_ssize_t self_len, result_len;
    Py_ssize_t count, offset, product;
    PyByteArrayObject *result;

    self_s = PyByteArray_AS_STRING(self);
    self_len = PyByteArray_GET_SIZE(self);

    count = stringlib_count(self_s, self_len,
                            from_s, from_len,
                            maxcount);

    Din_Go(694,2048);if (count == 0) {
        /* no matches, return unchanged */
        {PyByteArrayObject * ReplaceReturn443 = return_self(self); Din_Go(693,2048); return ReplaceReturn443;}
    }

    /* Check for overflow */
    /*    result_len = self_len + count * (to_len-from_len) */
    Din_Go(695,2048);product = count * (to_len-from_len);
    Din_Go(698,2048);if (product / (to_len-from_len) != count) {
        Din_Go(696,2048);PyErr_SetString(PyExc_OverflowError, "replace bytes is too long");
        {PyByteArrayObject * ReplaceReturn442 = NULL; Din_Go(697,2048); return ReplaceReturn442;}
    }
    Din_Go(699,2048);result_len = self_len + product;
    Din_Go(702,2048);if (result_len < 0) {
        Din_Go(700,2048);PyErr_SetString(PyExc_OverflowError, "replace bytes is too long");
        {PyByteArrayObject * ReplaceReturn441 = NULL; Din_Go(701,2048); return ReplaceReturn441;}
    }

    Din_Go(703,2048);if ( (result = (PyByteArrayObject *)
          PyByteArray_FromStringAndSize(NULL, result_len)) == NULL)
        return NULL;
    Din_Go(704,2048);result_s = PyByteArray_AS_STRING(result);

    start = self_s;
    end = self_s + self_len;
    Din_Go(712,2048);while (count-- > 0) {
        Din_Go(705,2048);offset = stringlib_find(start, end-start,
                                from_s, from_len,
                                0);
        Din_Go(707,2048);if (offset == -1)
            {/*231*/Din_Go(706,2048);break;/*232*/}
        Din_Go(708,2048);next = start+offset;
        Din_Go(711,2048);if (next == start) {
            /* replace with the 'to' */
            Py_MEMCPY(result_s, to_s, to_len);
            Din_Go(709,2048);result_s += to_len;
            start += from_len;
        } else {
            /* copy the unchanged old then the 'to' */
            Py_MEMCPY(result_s, start, next-start);
            Din_Go(710,2048);result_s += (next-start);
            Py_MEMCPY(result_s, to_s, to_len);
            result_s += to_len;
            start = next+from_len;
        }
    }
    /* Copy the remainder of the remaining bytes */
    Py_MEMCPY(result_s, start, end-start);

    {PyByteArrayObject * ReplaceReturn440 = result; Din_Go(713,2048); return ReplaceReturn440;}
}


Py_LOCAL(PyByteArrayObject *)
replace(PyByteArrayObject *self,
        const char *from_s, Py_ssize_t from_len,
        const char *to_s, Py_ssize_t to_len,
        Py_ssize_t maxcount)
{
    Din_Go(714,2048);if (maxcount < 0) {
        Din_Go(715,2048);maxcount = PY_SSIZE_T_MAX;
    } else {/*233*/Din_Go(716,2048);if (maxcount == 0 || PyByteArray_GET_SIZE(self) == 0) {
        /* nothing to do; return the original bytes */
        {PyByteArrayObject * ReplaceReturn439 = return_self(self); Din_Go(717,2048); return ReplaceReturn439;}
    ;/*234*/}}

    Din_Go(719,2048);if (maxcount == 0 ||
        (from_len == 0 && to_len == 0)) {
        /* nothing to do; return the original bytes */
        {PyByteArrayObject * ReplaceReturn438 = return_self(self); Din_Go(718,2048); return ReplaceReturn438;}
    }

    /* Handle zero-length special cases */

    Din_Go(721,2048);if (from_len == 0) {
        /* insert the 'to' bytes everywhere.   */
        /*    >>> "Python".replace("", ".")     */
        /*    '.P.y.t.h.o.n.'                   */
        {PyByteArrayObject * ReplaceReturn437 = replace_interleave(self, to_s, to_len, maxcount); Din_Go(720,2048); return ReplaceReturn437;}
    }

    /* Except for "".replace("", "A") == "A" there is no way beyond this */
    /* point for an empty self bytes to generate a non-empty bytes */
    /* Special case so the remaining code always gets a non-empty bytes */
    Din_Go(723,2048);if (PyByteArray_GET_SIZE(self) == 0) {
        {PyByteArrayObject * ReplaceReturn436 = return_self(self); Din_Go(722,2048); return ReplaceReturn436;}
    }

    Din_Go(727,2048);if (to_len == 0) {
        /* delete all occurances of 'from' bytes */
        Din_Go(724,2048);if (from_len == 1) {
            {PyByteArrayObject * ReplaceReturn435 = replace_delete_single_character(
                    self, from_s[0], maxcount); Din_Go(725,2048); return ReplaceReturn435;}
        } else {
            {PyByteArrayObject * ReplaceReturn434 = replace_delete_substring(self, from_s, from_len, maxcount); Din_Go(726,2048); return ReplaceReturn434;}
        }
    }

    /* Handle special case where both bytes have the same length */

    Din_Go(731,2048);if (from_len == to_len) {
        Din_Go(728,2048);if (from_len == 1) {
            {PyByteArrayObject * ReplaceReturn433 = replace_single_character_in_place(
                    self,
                    from_s[0],
                    to_s[0],
                    maxcount); Din_Go(729,2048); return ReplaceReturn433;}
        } else {
            {PyByteArrayObject * ReplaceReturn432 = replace_substring_in_place(
                self, from_s, from_len, to_s, to_len, maxcount); Din_Go(730,2048); return ReplaceReturn432;}
        }
    }

    /* Otherwise use the more generic algorithms */
    Din_Go(734,2048);if (from_len == 1) {
        {PyByteArrayObject * ReplaceReturn431 = replace_single_character(self, from_s[0],
                                        to_s, to_len, maxcount); Din_Go(732,2048); return ReplaceReturn431;}
    } else {
        /* len('from')>=2, len('to')>=1 */
        {PyByteArrayObject * ReplaceReturn430 = replace_substring(self, from_s, from_len, to_s, to_len, maxcount); Din_Go(733,2048); return ReplaceReturn430;}
    }
Din_Go(735,2048);}


PyDoc_STRVAR(replace__doc__,
"B.replace(old, new[, count]) -> bytes\n\
\n\
Return a copy of B with all occurrences of subsection\n\
old replaced by new.  If the optional argument count is\n\
given, only the first count occurrences are replaced.");

static PyObject *
bytearray_replace(PyByteArrayObject *self, PyObject *args)
{
    Din_Go(736,2048);Py_ssize_t count = -1;
    PyObject *from, *to, *res;
    Py_buffer vfrom, vto;

    Din_Go(737,2048);if (!PyArg_ParseTuple(args, "OO|n:replace", &from, &to, &count))
        return NULL;

    Din_Go(738,2048);if (_getbuffer(from, &vfrom) < 0)
        return NULL;
    Din_Go(741,2048);if (_getbuffer(to, &vto) < 0) {
        Din_Go(739,2048);PyBuffer_Release(&vfrom);
        {PyObject * ReplaceReturn429 = NULL; Din_Go(740,2048); return ReplaceReturn429;}
    }

    Din_Go(742,2048);res = (PyObject *)replace((PyByteArrayObject *) self,
                              vfrom.buf, vfrom.len,
                              vto.buf, vto.len, count);

    PyBuffer_Release(&vfrom);
    PyBuffer_Release(&vto);
    {PyObject * ReplaceReturn428 = res; Din_Go(743,2048); return ReplaceReturn428;}
}

PyDoc_STRVAR(split__doc__,
"B.split([sep[, maxsplit]]) -> list of bytearray\n\
\n\
Return a list of the sections in B, using sep as the delimiter.\n\
If sep is not given, B is split on ASCII whitespace characters\n\
(space, tab, return, newline, formfeed, vertical tab).\n\
If maxsplit is given, at most maxsplit splits are done.");

static PyObject *
bytearray_split(PyByteArrayObject *self, PyObject *args)
{
    Din_Go(744,2048);Py_ssize_t len = PyByteArray_GET_SIZE(self), n;
    Py_ssize_t maxsplit = -1;
    const char *s = PyByteArray_AS_STRING(self), *sub;
    PyObject *list, *subobj = Py_None;
    Py_buffer vsub;

    Din_Go(745,2048);if (!PyArg_ParseTuple(args, "|On:split", &subobj, &maxsplit))
        return NULL;
    Din_Go(746,2048);if (maxsplit < 0)
        maxsplit = PY_SSIZE_T_MAX;

    Din_Go(748,2048);if (subobj == Py_None)
        {/*235*/{PyObject * ReplaceReturn427 = stringlib_split_whitespace((PyObject*) self, s, len, maxsplit); Din_Go(747,2048); return ReplaceReturn427;}/*236*/}

    Din_Go(749,2048);if (_getbuffer(subobj, &vsub) < 0)
        return NULL;
    Din_Go(750,2048);sub = vsub.buf;
    n = vsub.len;

    list = stringlib_split(
        (PyObject*) self, s, len, sub, n, maxsplit
        );
    PyBuffer_Release(&vsub);
    {PyObject * ReplaceReturn426 = list; Din_Go(751,2048); return ReplaceReturn426;}
}

PyDoc_STRVAR(partition__doc__,
"B.partition(sep) -> (head, sep, tail)\n\
\n\
Searches for the separator sep in B, and returns the part before it,\n\
the separator itself, and the part after it.  If the separator is not\n\
found, returns B and two empty bytearray objects.");

static PyObject *
bytearray_partition(PyByteArrayObject *self, PyObject *sep_obj)
{
    Din_Go(752,2048);PyObject *bytesep, *result;

    bytesep = PyByteArray_FromObject(sep_obj);
    Din_Go(753,2048);if (! bytesep)
        return NULL;

    Din_Go(754,2048);result = stringlib_partition(
            (PyObject*) self,
            PyByteArray_AS_STRING(self), PyByteArray_GET_SIZE(self),
            bytesep,
            PyByteArray_AS_STRING(bytesep), PyByteArray_GET_SIZE(bytesep)
            );

    Py_DECREF(bytesep);
    {PyObject * ReplaceReturn425 = result; Din_Go(755,2048); return ReplaceReturn425;}
}

PyDoc_STRVAR(rpartition__doc__,
"B.rpartition(sep) -> (head, sep, tail)\n\
\n\
Searches for the separator sep in B, starting at the end of B,\n\
and returns the part before it, the separator itself, and the\n\
part after it.  If the separator is not found, returns two empty\n\
bytearray objects and B.");

static PyObject *
bytearray_rpartition(PyByteArrayObject *self, PyObject *sep_obj)
{
    Din_Go(756,2048);PyObject *bytesep, *result;

    bytesep = PyByteArray_FromObject(sep_obj);
    Din_Go(757,2048);if (! bytesep)
        return NULL;

    Din_Go(758,2048);result = stringlib_rpartition(
            (PyObject*) self,
            PyByteArray_AS_STRING(self), PyByteArray_GET_SIZE(self),
            bytesep,
            PyByteArray_AS_STRING(bytesep), PyByteArray_GET_SIZE(bytesep)
            );

    Py_DECREF(bytesep);
    {PyObject * ReplaceReturn424 = result; Din_Go(759,2048); return ReplaceReturn424;}
}

PyDoc_STRVAR(rsplit__doc__,
"B.rsplit(sep[, maxsplit]) -> list of bytearray\n\
\n\
Return a list of the sections in B, using sep as the delimiter,\n\
starting at the end of B and working to the front.\n\
If sep is not given, B is split on ASCII whitespace characters\n\
(space, tab, return, newline, formfeed, vertical tab).\n\
If maxsplit is given, at most maxsplit splits are done.");

static PyObject *
bytearray_rsplit(PyByteArrayObject *self, PyObject *args)
{
    Din_Go(760,2048);Py_ssize_t len = PyByteArray_GET_SIZE(self), n;
    Py_ssize_t maxsplit = -1;
    const char *s = PyByteArray_AS_STRING(self), *sub;
    PyObject *list, *subobj = Py_None;
    Py_buffer vsub;

    Din_Go(761,2048);if (!PyArg_ParseTuple(args, "|On:rsplit", &subobj, &maxsplit))
        return NULL;
    Din_Go(762,2048);if (maxsplit < 0)
        maxsplit = PY_SSIZE_T_MAX;

    Din_Go(764,2048);if (subobj == Py_None)
        {/*237*/{PyObject * ReplaceReturn423 = stringlib_rsplit_whitespace((PyObject*) self, s, len, maxsplit); Din_Go(763,2048); return ReplaceReturn423;}/*238*/}

    Din_Go(765,2048);if (_getbuffer(subobj, &vsub) < 0)
        return NULL;
    Din_Go(766,2048);sub = vsub.buf;
    n = vsub.len;

    list = stringlib_rsplit(
        (PyObject*) self, s, len, sub, n, maxsplit
        );
    PyBuffer_Release(&vsub);
    {PyObject * ReplaceReturn422 = list; Din_Go(767,2048); return ReplaceReturn422;}
}

PyDoc_STRVAR(reverse__doc__,
"B.reverse() -> None\n\
\n\
Reverse the order of the values in B in place.");
static PyObject *
bytearray_reverse(PyByteArrayObject *self, PyObject *unused)
{
    Din_Go(768,2048);char swap, *head, *tail;
    Py_ssize_t i, j, n = Py_SIZE(self);

    j = n / 2;
    head = self->ob_bytes;
    tail = head + n - 1;
    Din_Go(770,2048);for (i = 0; i < j; i++) {
        Din_Go(769,2048);swap = *head;
        *head++ = *tail;
        *tail-- = swap;
    }

    Py_RETURN_NONE;
}

PyDoc_STRVAR(insert__doc__,
"B.insert(index, int) -> None\n\
\n\
Insert a single item into the bytearray before the given index.");
static PyObject *
bytearray_insert(PyByteArrayObject *self, PyObject *args)
{
    Din_Go(771,2048);PyObject *value;
    int ival;
    Py_ssize_t where, n = Py_SIZE(self);

    Din_Go(772,2048);if (!PyArg_ParseTuple(args, "nO:insert", &where, &value))
        return NULL;

    Din_Go(775,2048);if (n == PY_SSIZE_T_MAX) {
        Din_Go(773,2048);PyErr_SetString(PyExc_OverflowError,
                        "cannot add more objects to bytearray");
        {PyObject * ReplaceReturn421 = NULL; Din_Go(774,2048); return ReplaceReturn421;}
    }
    Din_Go(776,2048);if (!_getbytevalue(value, &ival))
        return NULL;
    Din_Go(777,2048);if (PyByteArray_Resize((PyObject *)self, n + 1) < 0)
        return NULL;

    Din_Go(781,2048);if (where < 0) {
        Din_Go(778,2048);where += n;
        Din_Go(780,2048);if (where < 0)
            {/*239*/Din_Go(779,2048);where = 0;/*240*/}
    }
    Din_Go(783,2048);if (where > n)
        {/*241*/Din_Go(782,2048);where = n;/*242*/}
    Din_Go(784,2048);memmove(self->ob_bytes + where + 1, self->ob_bytes + where, n - where);
    self->ob_bytes[where] = ival;

    Py_RETURN_NONE;
}

PyDoc_STRVAR(append__doc__,
"B.append(int) -> None\n\
\n\
Append a single item to the end of B.");
static PyObject *
bytearray_append(PyByteArrayObject *self, PyObject *arg)
{
    Din_Go(785,2048);int value;
    Py_ssize_t n = Py_SIZE(self);

    Din_Go(786,2048);if (! _getbytevalue(arg, &value))
        return NULL;
    Din_Go(789,2048);if (n == PY_SSIZE_T_MAX) {
        Din_Go(787,2048);PyErr_SetString(PyExc_OverflowError,
                        "cannot add more objects to bytearray");
        {PyObject * ReplaceReturn420 = NULL; Din_Go(788,2048); return ReplaceReturn420;}
    }
    Din_Go(790,2048);if (PyByteArray_Resize((PyObject *)self, n + 1) < 0)
        return NULL;

    Din_Go(791,2048);self->ob_bytes[n] = value;

    Py_RETURN_NONE;
}

PyDoc_STRVAR(extend__doc__,
"B.extend(iterable int) -> None\n\
\n\
Append all the elements from the iterator or sequence to the\n\
end of B.");
static PyObject *
bytearray_extend(PyByteArrayObject *self, PyObject *arg)
{
    Din_Go(792,2048);PyObject *it, *item, *bytearray_obj;
    Py_ssize_t buf_size = 0, len = 0;
    int value;
    char *buf;

    /* bytearray_setslice code only accepts something supporting PEP 3118. */
    Din_Go(794,2048);if (PyObject_CheckBuffer(arg)) {
        Din_Go(793,2048);if (bytearray_setslice(self, Py_SIZE(self), Py_SIZE(self), arg) == -1)
            return NULL;

        Py_RETURN_NONE;
    }

    Din_Go(795,2048);it = PyObject_GetIter(arg);
    Din_Go(796,2048);if (it == NULL)
        return NULL;

    /* Try to determine the length of the argument. 32 is arbitrary. */
    Din_Go(797,2048);buf_size = _PyObject_LengthHint(arg, 32);
    Din_Go(799,2048);if (buf_size == -1) {
        Py_DECREF(it);
        {PyObject * ReplaceReturn419 = NULL; Din_Go(798,2048); return ReplaceReturn419;}
    }

    Din_Go(800,2048);bytearray_obj = PyByteArray_FromStringAndSize(NULL, buf_size);
    Din_Go(802,2048);if (bytearray_obj == NULL) {
        Py_DECREF(it);
        {PyObject * ReplaceReturn418 = NULL; Din_Go(801,2048); return ReplaceReturn418;}
    }
    Din_Go(803,2048);buf = PyByteArray_AS_STRING(bytearray_obj);

    Din_Go(812,2048);while ((item = PyIter_Next(it)) != NULL) {
        Din_Go(804,2048);if (! _getbytevalue(item, &value)) {
            Py_DECREF(item);
            Py_DECREF(it);
            Py_DECREF(bytearray_obj);
            {PyObject * ReplaceReturn417 = NULL; Din_Go(805,2048); return ReplaceReturn417;}
        }
        Din_Go(806,2048);buf[len++] = value;
        Py_DECREF(item);

        Din_Go(811,2048);if (len >= buf_size) {
            Din_Go(807,2048);buf_size = len + (len >> 1) + 1;
            Din_Go(809,2048);if (PyByteArray_Resize((PyObject *)bytearray_obj, buf_size) < 0) {
                Py_DECREF(it);
                Py_DECREF(bytearray_obj);
                {PyObject * ReplaceReturn416 = NULL; Din_Go(808,2048); return ReplaceReturn416;}
            }
            /* Recompute the `buf' pointer, since the resizing operation may
               have invalidated it. */
            Din_Go(810,2048);buf = PyByteArray_AS_STRING(bytearray_obj);
        }
    }
    Py_DECREF(it);

    /* Resize down to exact size. */
    Din_Go(814,2048);if (PyByteArray_Resize((PyObject *)bytearray_obj, len) < 0) {
        Py_DECREF(bytearray_obj);
        {PyObject * ReplaceReturn415 = NULL; Din_Go(813,2048); return ReplaceReturn415;}
    }

    Din_Go(816,2048);if (bytearray_setslice(self, Py_SIZE(self), Py_SIZE(self), bytearray_obj) == -1) {
        Py_DECREF(bytearray_obj);
        {PyObject * ReplaceReturn414 = NULL; Din_Go(815,2048); return ReplaceReturn414;}
    }
    Py_DECREF(bytearray_obj);

    Py_RETURN_NONE;
}

PyDoc_STRVAR(pop__doc__,
"B.pop([index]) -> int\n\
\n\
Remove and return a single item from B. If no index\n\
argument is given, will pop the last value.");
static PyObject *
bytearray_pop(PyByteArrayObject *self, PyObject *args)
{
    Din_Go(817,2048);int value;
    Py_ssize_t where = -1, n = Py_SIZE(self);

    Din_Go(818,2048);if (!PyArg_ParseTuple(args, "|n:pop", &where))
        return NULL;

    Din_Go(821,2048);if (n == 0) {
        Din_Go(819,2048);PyErr_SetString(PyExc_IndexError,
                        "pop from empty bytearray");
        {PyObject * ReplaceReturn413 = NULL; Din_Go(820,2048); return ReplaceReturn413;}
    }
    Din_Go(822,2048);if (where < 0)
        where += Py_SIZE(self);
    Din_Go(825,2048);if (where < 0 || where >= Py_SIZE(self)) {
        Din_Go(823,2048);PyErr_SetString(PyExc_IndexError, "pop index out of range");
        {PyObject * ReplaceReturn412 = NULL; Din_Go(824,2048); return ReplaceReturn412;}
    }
    Din_Go(826,2048);if (!_canresize(self))
        return NULL;

    Din_Go(827,2048);value = self->ob_bytes[where];
    memmove(self->ob_bytes + where, self->ob_bytes + where + 1, n - where);
    Din_Go(828,2048);if (PyByteArray_Resize((PyObject *)self, n - 1) < 0)
        return NULL;

    {PyObject * ReplaceReturn411 = PyInt_FromLong((unsigned char)value); Din_Go(829,2048); return ReplaceReturn411;}
}

PyDoc_STRVAR(remove__doc__,
"B.remove(int) -> None\n\
\n\
Remove the first occurance of a value in B.");
static PyObject *
bytearray_remove(PyByteArrayObject *self, PyObject *arg)
{
    Din_Go(830,2048);int value;
    Py_ssize_t where, n = Py_SIZE(self);

    Din_Go(831,2048);if (! _getbytevalue(arg, &value))
        return NULL;

    Din_Go(834,2048);for (where = 0; where < n; where++) {
        Din_Go(832,2048);if (self->ob_bytes[where] == value)
            {/*243*/Din_Go(833,2048);break;/*244*/}
    }
    Din_Go(837,2048);if (where == n) {
        Din_Go(835,2048);PyErr_SetString(PyExc_ValueError, "value not found in bytearray");
        {PyObject * ReplaceReturn410 = NULL; Din_Go(836,2048); return ReplaceReturn410;}
    }
    Din_Go(838,2048);if (!_canresize(self))
        return NULL;

    Din_Go(839,2048);memmove(self->ob_bytes + where, self->ob_bytes + where + 1, n - where);
    Din_Go(840,2048);if (PyByteArray_Resize((PyObject *)self, n - 1) < 0)
        return NULL;

    Py_RETURN_NONE;
}

/* XXX These two helpers could be optimized if argsize == 1 */

static Py_ssize_t
lstrip_helper(unsigned char *myptr, Py_ssize_t mysize,
              void *argptr, Py_ssize_t argsize)
{
    Din_Go(841,2048);Py_ssize_t i = 0;
    Din_Go(843,2048);while (i < mysize && memchr(argptr, myptr[i], argsize))
        {/*245*/Din_Go(842,2048);i++;/*246*/}
    {Py_ssize_t  ReplaceReturn409 = i; Din_Go(844,2048); return ReplaceReturn409;}
}

static Py_ssize_t
rstrip_helper(unsigned char *myptr, Py_ssize_t mysize,
              void *argptr, Py_ssize_t argsize)
{
    Din_Go(845,2048);Py_ssize_t i = mysize - 1;
    Din_Go(847,2048);while (i >= 0 && memchr(argptr, myptr[i], argsize))
        {/*247*/Din_Go(846,2048);i--;/*248*/}
    {Py_ssize_t  ReplaceReturn408 = i + 1; Din_Go(848,2048); return ReplaceReturn408;}
}

PyDoc_STRVAR(strip__doc__,
"B.strip([bytes]) -> bytearray\n\
\n\
Strip leading and trailing bytes contained in the argument.\n\
If the argument is omitted, strip ASCII whitespace.");
static PyObject *
bytearray_strip(PyByteArrayObject *self, PyObject *args)
{
    Din_Go(849,2048);Py_ssize_t left, right, mysize, argsize;
    void *myptr, *argptr;
    PyObject *arg = Py_None;
    Py_buffer varg;
    Din_Go(850,2048);if (!PyArg_ParseTuple(args, "|O:strip", &arg))
        return NULL;
    Din_Go(854,2048);if (arg == Py_None) {
        Din_Go(851,2048);argptr = "\t\n\r\f\v ";
        argsize = 6;
    }
    else {
        Din_Go(852,2048);if (_getbuffer(arg, &varg) < 0)
            return NULL;
        Din_Go(853,2048);argptr = varg.buf;
        argsize = varg.len;
    }
    Din_Go(855,2048);myptr = self->ob_bytes;
    mysize = Py_SIZE(self);
    left = lstrip_helper(myptr, mysize, argptr, argsize);
    Din_Go(858,2048);if (left == mysize)
        {/*249*/Din_Go(856,2048);right = left;/*250*/}
    else
        {/*251*/Din_Go(857,2048);right = rstrip_helper(myptr, mysize, argptr, argsize);/*252*/}
    Din_Go(860,2048);if (arg != Py_None)
        {/*253*/Din_Go(859,2048);PyBuffer_Release(&varg);/*254*/}
    {PyObject * ReplaceReturn407 = PyByteArray_FromStringAndSize(self->ob_bytes + left, right - left); Din_Go(861,2048); return ReplaceReturn407;}
}

PyDoc_STRVAR(lstrip__doc__,
"B.lstrip([bytes]) -> bytearray\n\
\n\
Strip leading bytes contained in the argument.\n\
If the argument is omitted, strip leading ASCII whitespace.");
static PyObject *
bytearray_lstrip(PyByteArrayObject *self, PyObject *args)
{
    Din_Go(862,2048);Py_ssize_t left, right, mysize, argsize;
    void *myptr, *argptr;
    PyObject *arg = Py_None;
    Py_buffer varg;
    Din_Go(863,2048);if (!PyArg_ParseTuple(args, "|O:lstrip", &arg))
        return NULL;
    Din_Go(867,2048);if (arg == Py_None) {
        Din_Go(864,2048);argptr = "\t\n\r\f\v ";
        argsize = 6;
    }
    else {
        Din_Go(865,2048);if (_getbuffer(arg, &varg) < 0)
            return NULL;
        Din_Go(866,2048);argptr = varg.buf;
        argsize = varg.len;
    }
    Din_Go(868,2048);myptr = self->ob_bytes;
    mysize = Py_SIZE(self);
    left = lstrip_helper(myptr, mysize, argptr, argsize);
    right = mysize;
    Din_Go(870,2048);if (arg != Py_None)
        {/*255*/Din_Go(869,2048);PyBuffer_Release(&varg);/*256*/}
    {PyObject * ReplaceReturn406 = PyByteArray_FromStringAndSize(self->ob_bytes + left, right - left); Din_Go(871,2048); return ReplaceReturn406;}
}

PyDoc_STRVAR(rstrip__doc__,
"B.rstrip([bytes]) -> bytearray\n\
\n\
Strip trailing bytes contained in the argument.\n\
If the argument is omitted, strip trailing ASCII whitespace.");
static PyObject *
bytearray_rstrip(PyByteArrayObject *self, PyObject *args)
{
    Din_Go(872,2048);Py_ssize_t left, right, mysize, argsize;
    void *myptr, *argptr;
    PyObject *arg = Py_None;
    Py_buffer varg;
    Din_Go(873,2048);if (!PyArg_ParseTuple(args, "|O:rstrip", &arg))
        return NULL;
    Din_Go(877,2048);if (arg == Py_None) {
        Din_Go(874,2048);argptr = "\t\n\r\f\v ";
        argsize = 6;
    }
    else {
        Din_Go(875,2048);if (_getbuffer(arg, &varg) < 0)
            return NULL;
        Din_Go(876,2048);argptr = varg.buf;
        argsize = varg.len;
    }
    Din_Go(878,2048);myptr = self->ob_bytes;
    mysize = Py_SIZE(self);
    left = 0;
    right = rstrip_helper(myptr, mysize, argptr, argsize);
    Din_Go(880,2048);if (arg != Py_None)
        {/*257*/Din_Go(879,2048);PyBuffer_Release(&varg);/*258*/}
    {PyObject * ReplaceReturn405 = PyByteArray_FromStringAndSize(self->ob_bytes + left, right - left); Din_Go(881,2048); return ReplaceReturn405;}
}

PyDoc_STRVAR(decode_doc,
"B.decode([encoding[, errors]]) -> unicode object.\n\
\n\
Decodes B using the codec registered for encoding. encoding defaults\n\
to the default encoding. errors may be given to set a different error\n\
handling scheme.  Default is 'strict' meaning that encoding errors raise\n\
a UnicodeDecodeError.  Other possible values are 'ignore' and 'replace'\n\
as well as any other name registered with codecs.register_error that is\n\
able to handle UnicodeDecodeErrors.");

static PyObject *
bytearray_decode(PyObject *self, PyObject *args, PyObject *kwargs)
{
    Din_Go(882,2048);const char *encoding = NULL;
    const char *errors = NULL;
    static char *kwlist[] = {"encoding", "errors", 0};

    Din_Go(883,2048);if (!PyArg_ParseTupleAndKeywords(args, kwargs, "|ss:decode", kwlist, &encoding, &errors))
        return NULL;
    Din_Go(885,2048);if (encoding == NULL) {
#ifdef Py_USING_UNICODE
        Din_Go(884,2048);encoding = PyUnicode_GetDefaultEncoding();
#else
        PyErr_SetString(PyExc_ValueError, "no encoding specified");
        return NULL;
#endif
    }
    {PyObject * ReplaceReturn404 = PyCodec_Decode(self, encoding, errors); Din_Go(886,2048); return ReplaceReturn404;}
}

PyDoc_STRVAR(alloc_doc,
"B.__alloc__() -> int\n\
\n\
Returns the number of bytes actually allocated.");

static PyObject *
bytearray_alloc(PyByteArrayObject *self)
{
    {PyObject * ReplaceReturn403 = PyInt_FromSsize_t(self->ob_alloc); Din_Go(887,2048); return ReplaceReturn403;}
}

PyDoc_STRVAR(join_doc,
"B.join(iterable_of_bytes) -> bytes\n\
\n\
Concatenates any number of bytearray objects, with B in between each pair.");

static PyObject *
bytearray_join(PyByteArrayObject *self, PyObject *it)
{
    Din_Go(888,2048);PyObject *seq;
    Py_ssize_t mysize = Py_SIZE(self);
    Py_ssize_t i;
    Py_ssize_t n;
    PyObject **items;
    Py_ssize_t totalsize = 0;
    PyObject *result;
    char *dest;

    seq = PySequence_Fast(it, "can only join an iterable");
    Din_Go(889,2048);if (seq == NULL)
        return NULL;
    Din_Go(890,2048);n = PySequence_Fast_GET_SIZE(seq);
    items = PySequence_Fast_ITEMS(seq);

    /* Compute the total size, and check that they are all bytes */
    /* XXX Shouldn't we use _getbuffer() on these items instead? */
    Din_Go(901,2048);for (i = 0; i < n; i++) {
        Din_Go(891,2048);PyObject *obj = items[i];
        Din_Go(894,2048);if (!PyByteArray_Check(obj) && !PyBytes_Check(obj)) {
            Din_Go(892,2048);PyErr_Format(PyExc_TypeError,
                         "can only join an iterable of bytes "
                         "(item %ld has type '%.100s')",
                         /* XXX %ld isn't right on Win64 */
                         (long)i, Py_TYPE(obj)->tp_name);
            Din_Go(893,2048);goto error;
        }
        Din_Go(896,2048);if (i > 0)
            {/*259*/Din_Go(895,2048);totalsize += mysize;/*260*/}
        Din_Go(897,2048);totalsize += Py_SIZE(obj);
        Din_Go(900,2048);if (totalsize < 0) {
            Din_Go(898,2048);PyErr_NoMemory();
            Din_Go(899,2048);goto error;
        }
    }

    /* Allocate the result, and copy the bytes */
    Din_Go(902,2048);result = PyByteArray_FromStringAndSize(NULL, totalsize);
    Din_Go(904,2048);if (result == NULL)
        {/*261*/Din_Go(903,2048);goto error;/*262*/}
    Din_Go(905,2048);dest = PyByteArray_AS_STRING(result);
    Din_Go(911,2048);for (i = 0; i < n; i++) {
        Din_Go(906,2048);PyObject *obj = items[i];
        Py_ssize_t size = Py_SIZE(obj);
        char *buf;
        Din_Go(907,2048);if (PyByteArray_Check(obj))
           buf = PyByteArray_AS_STRING(obj);
        else
           buf = PyBytes_AS_STRING(obj);
        Din_Go(909,2048);if (i) {
            Din_Go(908,2048);memcpy(dest, self->ob_bytes, mysize);
            dest += mysize;
        }
        Din_Go(910,2048);memcpy(dest, buf, size);
        dest += size;
    }

    /* Done */
    Py_DECREF(seq);
    {PyObject * ReplaceReturn402 = result; Din_Go(912,2048); return ReplaceReturn402;}

    /* Error handling */
  error:
    Py_DECREF(seq);
    return NULL;
}

PyDoc_STRVAR(splitlines__doc__,
"B.splitlines(keepends=False) -> list of lines\n\
\n\
Return a list of the lines in B, breaking at line boundaries.\n\
Line breaks are not included in the resulting list unless keepends\n\
is given and true.");

static PyObject*
bytearray_splitlines(PyObject *self, PyObject *args)
{
    Din_Go(913,2048);int keepends = 0;

    Din_Go(914,2048);if (!PyArg_ParseTuple(args, "|i:splitlines", &keepends))
        return NULL;

    {PyObject * ReplaceReturn401 = stringlib_splitlines(
        (PyObject*) self, PyByteArray_AS_STRING(self),
        PyByteArray_GET_SIZE(self), keepends
        ); Din_Go(915,2048); return ReplaceReturn401;}
}

PyDoc_STRVAR(fromhex_doc,
"bytearray.fromhex(string) -> bytearray\n\
\n\
Create a bytearray object from a string of hexadecimal numbers.\n\
Spaces between two numbers are accepted.\n\
Example: bytearray.fromhex('B9 01EF') -> bytearray(b'\\xb9\\x01\\xef').");

static int
hex_digit_to_int(char c)
{
    Din_Go(916,2048);if (Py_ISDIGIT(c))
        {/*263*/{int  ReplaceReturn400 = c - '0'; Din_Go(917,2048); return ReplaceReturn400;}/*264*/}
    else {
        Din_Go(918,2048);if (Py_ISUPPER(c))
            c = Py_TOLOWER(c);
        Din_Go(920,2048);if (c >= 'a' && c <= 'f')
            {/*265*/{int  ReplaceReturn399 = c - 'a' + 10; Din_Go(919,2048); return ReplaceReturn399;}/*266*/}
    }
    {int  ReplaceReturn398 = -1; Din_Go(921,2048); return ReplaceReturn398;}
}

static PyObject *
bytearray_fromhex(PyObject *cls, PyObject *args)
{
    Din_Go(922,2048);PyObject *newbytes;
    char *buf;
    char *hex;
    Py_ssize_t hexlen, byteslen, i, j;
    int top, bot;

    Din_Go(923,2048);if (!PyArg_ParseTuple(args, "s#:fromhex", &hex, &hexlen))
        return NULL;
    Din_Go(924,2048);byteslen = hexlen/2; /* This overestimates if there are spaces */
    newbytes = PyByteArray_FromStringAndSize(NULL, byteslen);
    Din_Go(925,2048);if (!newbytes)
        return NULL;
    Din_Go(926,2048);buf = PyByteArray_AS_STRING(newbytes);
    Din_Go(936,2048);for (i = j = 0; i < hexlen; i += 2) {
        /* skip over spaces in the input */
        Din_Go(927,2048);while (hex[i] == ' ')
            {/*267*/Din_Go(928,2048);i++;/*268*/}
        Din_Go(930,2048);if (i >= hexlen)
            {/*269*/Din_Go(929,2048);break;/*270*/}
        Din_Go(931,2048);top = hex_digit_to_int(hex[i]);
        bot = hex_digit_to_int(hex[i+1]);
        Din_Go(934,2048);if (top == -1 || bot == -1) {
            Din_Go(932,2048);PyErr_Format(PyExc_ValueError,
                         "non-hexadecimal number found in "
                         "fromhex() arg at position %zd", i);
            Din_Go(933,2048);goto error;
        }
        Din_Go(935,2048);buf[j++] = (top << 4) + bot;
    }
    Din_Go(938,2048);if (PyByteArray_Resize(newbytes, j) < 0)
        {/*271*/Din_Go(937,2048);goto error;/*272*/}
    {PyObject * ReplaceReturn397 = newbytes; Din_Go(939,2048); return ReplaceReturn397;}

  error:
    Py_DECREF(newbytes);
    return NULL;
}

PyDoc_STRVAR(reduce_doc, "Return state information for pickling.");

static PyObject *
bytearray_reduce(PyByteArrayObject *self)
{
    Din_Go(940,2048);PyObject *latin1, *dict;
    Din_Go(943,2048);if (self->ob_bytes)
#ifdef Py_USING_UNICODE
        {/*273*/Din_Go(941,2048);latin1 = PyUnicode_DecodeLatin1(self->ob_bytes,
                                        Py_SIZE(self), NULL);/*274*/}
#else
        latin1 = PyString_FromStringAndSize(self->ob_bytes, Py_SIZE(self));
#endif
    else
#ifdef Py_USING_UNICODE
        {/*275*/Din_Go(942,2048);latin1 = PyUnicode_FromString("");/*276*/}
#else
        latin1 = PyString_FromString("");
#endif

    Din_Go(944,2048);dict = PyObject_GetAttrString((PyObject *)self, "__dict__");
    Din_Go(946,2048);if (dict == NULL) {
        Din_Go(945,2048);PyErr_Clear();
        dict = Py_None;
        Py_INCREF(dict);
    }

    {PyObject * ReplaceReturn396 = Py_BuildValue("(O(Ns)N)", Py_TYPE(self), latin1, "latin-1", dict); Din_Go(947,2048); return ReplaceReturn396;}
}

PyDoc_STRVAR(sizeof_doc,
"B.__sizeof__() -> int\n\
 \n\
Returns the size of B in memory, in bytes");
static PyObject *
bytearray_sizeof(PyByteArrayObject *self)
{
    Din_Go(948,2048);Py_ssize_t res;

    res = sizeof(PyByteArrayObject) + self->ob_alloc * sizeof(char);
    {PyObject * ReplaceReturn395 = PyInt_FromSsize_t(res); Din_Go(949,2048); return ReplaceReturn395;}
}

static PySequenceMethods bytearray_as_sequence = {
    (lenfunc)bytearray_length,              /* sq_length */
    (binaryfunc)PyByteArray_Concat,         /* sq_concat */
    (ssizeargfunc)bytearray_repeat,         /* sq_repeat */
    (ssizeargfunc)bytearray_getitem,        /* sq_item */
    0,                                      /* sq_slice */
    (ssizeobjargproc)bytearray_setitem,     /* sq_ass_item */
    0,                                      /* sq_ass_slice */
    (objobjproc)bytearray_contains,         /* sq_contains */
    (binaryfunc)bytearray_iconcat,          /* sq_inplace_concat */
    (ssizeargfunc)bytearray_irepeat,        /* sq_inplace_repeat */
};

static PyMappingMethods bytearray_as_mapping = {
    (lenfunc)bytearray_length,
    (binaryfunc)bytearray_subscript,
    (objobjargproc)bytearray_ass_subscript,
};

static PyBufferProcs bytearray_as_buffer = {
    (readbufferproc)bytearray_buffer_getreadbuf,
    (writebufferproc)bytearray_buffer_getwritebuf,
    (segcountproc)bytearray_buffer_getsegcount,
    (charbufferproc)bytearray_buffer_getcharbuf,
    (getbufferproc)bytearray_getbuffer,
    (releasebufferproc)bytearray_releasebuffer,
};

static PyMethodDef
bytearray_methods[] = {
    {"__alloc__", (PyCFunction)bytearray_alloc, METH_NOARGS, alloc_doc},
    {"__reduce__", (PyCFunction)bytearray_reduce, METH_NOARGS, reduce_doc},
    {"__sizeof__", (PyCFunction)bytearray_sizeof, METH_NOARGS, sizeof_doc},
    {"append", (PyCFunction)bytearray_append, METH_O, append__doc__},
    {"capitalize", (PyCFunction)stringlib_capitalize, METH_NOARGS,
     _Py_capitalize__doc__},
    {"center", (PyCFunction)stringlib_center, METH_VARARGS, center__doc__},
    {"count", (PyCFunction)bytearray_count, METH_VARARGS, count__doc__},
    {"decode", (PyCFunction)bytearray_decode, METH_VARARGS | METH_KEYWORDS, decode_doc},
    {"endswith", (PyCFunction)bytearray_endswith, METH_VARARGS, endswith__doc__},
    {"expandtabs", (PyCFunction)stringlib_expandtabs, METH_VARARGS,
     expandtabs__doc__},
    {"extend", (PyCFunction)bytearray_extend, METH_O, extend__doc__},
    {"find", (PyCFunction)bytearray_find, METH_VARARGS, find__doc__},
    {"fromhex", (PyCFunction)bytearray_fromhex, METH_VARARGS|METH_CLASS,
     fromhex_doc},
    {"index", (PyCFunction)bytearray_index, METH_VARARGS, index__doc__},
    {"insert", (PyCFunction)bytearray_insert, METH_VARARGS, insert__doc__},
    {"isalnum", (PyCFunction)stringlib_isalnum, METH_NOARGS,
     _Py_isalnum__doc__},
    {"isalpha", (PyCFunction)stringlib_isalpha, METH_NOARGS,
     _Py_isalpha__doc__},
    {"isdigit", (PyCFunction)stringlib_isdigit, METH_NOARGS,
     _Py_isdigit__doc__},
    {"islower", (PyCFunction)stringlib_islower, METH_NOARGS,
     _Py_islower__doc__},
    {"isspace", (PyCFunction)stringlib_isspace, METH_NOARGS,
     _Py_isspace__doc__},
    {"istitle", (PyCFunction)stringlib_istitle, METH_NOARGS,
     _Py_istitle__doc__},
    {"isupper", (PyCFunction)stringlib_isupper, METH_NOARGS,
     _Py_isupper__doc__},
    {"join", (PyCFunction)bytearray_join, METH_O, join_doc},
    {"ljust", (PyCFunction)stringlib_ljust, METH_VARARGS, ljust__doc__},
    {"lower", (PyCFunction)stringlib_lower, METH_NOARGS, _Py_lower__doc__},
    {"lstrip", (PyCFunction)bytearray_lstrip, METH_VARARGS, lstrip__doc__},
    {"partition", (PyCFunction)bytearray_partition, METH_O, partition__doc__},
    {"pop", (PyCFunction)bytearray_pop, METH_VARARGS, pop__doc__},
    {"remove", (PyCFunction)bytearray_remove, METH_O, remove__doc__},
    {"replace", (PyCFunction)bytearray_replace, METH_VARARGS, replace__doc__},
    {"reverse", (PyCFunction)bytearray_reverse, METH_NOARGS, reverse__doc__},
    {"rfind", (PyCFunction)bytearray_rfind, METH_VARARGS, rfind__doc__},
    {"rindex", (PyCFunction)bytearray_rindex, METH_VARARGS, rindex__doc__},
    {"rjust", (PyCFunction)stringlib_rjust, METH_VARARGS, rjust__doc__},
    {"rpartition", (PyCFunction)bytearray_rpartition, METH_O, rpartition__doc__},
    {"rsplit", (PyCFunction)bytearray_rsplit, METH_VARARGS, rsplit__doc__},
    {"rstrip", (PyCFunction)bytearray_rstrip, METH_VARARGS, rstrip__doc__},
    {"split", (PyCFunction)bytearray_split, METH_VARARGS, split__doc__},
    {"splitlines", (PyCFunction)bytearray_splitlines, METH_VARARGS,
     splitlines__doc__},
    {"startswith", (PyCFunction)bytearray_startswith, METH_VARARGS ,
     startswith__doc__},
    {"strip", (PyCFunction)bytearray_strip, METH_VARARGS, strip__doc__},
    {"swapcase", (PyCFunction)stringlib_swapcase, METH_NOARGS,
     _Py_swapcase__doc__},
    {"title", (PyCFunction)stringlib_title, METH_NOARGS, _Py_title__doc__},
    {"translate", (PyCFunction)bytearray_translate, METH_VARARGS,
     translate__doc__},
    {"upper", (PyCFunction)stringlib_upper, METH_NOARGS, _Py_upper__doc__},
    {"zfill", (PyCFunction)stringlib_zfill, METH_VARARGS, zfill__doc__},
    {NULL}
};

PyDoc_STRVAR(bytearray_doc,
"bytearray(iterable_of_ints) -> bytearray.\n\
bytearray(string, encoding[, errors]) -> bytearray.\n\
bytearray(bytes_or_bytearray) -> mutable copy of bytes_or_bytearray.\n\
bytearray(memory_view) -> bytearray.\n\
\n\
Construct an mutable bytearray object from:\n\
  - an iterable yielding integers in range(256)\n\
  - a text string encoded using the specified encoding\n\
  - a bytes or a bytearray object\n\
  - any object implementing the buffer API.\n\
\n\
bytearray(int) -> bytearray.\n\
\n\
Construct a zero-initialized bytearray of the given length.");


static PyObject *bytearray_iter(PyObject *seq);

PyTypeObject PyByteArray_Type = {
    PyVarObject_HEAD_INIT(&PyType_Type, 0)
    "bytearray",
    sizeof(PyByteArrayObject),
    0,
    (destructor)bytearray_dealloc,       /* tp_dealloc */
    0,                                  /* tp_print */
    0,                                  /* tp_getattr */
    0,                                  /* tp_setattr */
    0,                                  /* tp_compare */
    (reprfunc)bytearray_repr,           /* tp_repr */
    0,                                  /* tp_as_number */
    &bytearray_as_sequence,             /* tp_as_sequence */
    &bytearray_as_mapping,              /* tp_as_mapping */
    0,                                  /* tp_hash */
    0,                                  /* tp_call */
    bytearray_str,                      /* tp_str */
    PyObject_GenericGetAttr,            /* tp_getattro */
    0,                                  /* tp_setattro */
    &bytearray_as_buffer,               /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE |
    Py_TPFLAGS_HAVE_NEWBUFFER,          /* tp_flags */
    bytearray_doc,                      /* tp_doc */
    0,                                  /* tp_traverse */
    0,                                  /* tp_clear */
    (richcmpfunc)bytearray_richcompare, /* tp_richcompare */
    0,                                  /* tp_weaklistoffset */
    bytearray_iter,                     /* tp_iter */
    0,                                  /* tp_iternext */
    bytearray_methods,                  /* tp_methods */
    0,                                  /* tp_members */
    0,                                  /* tp_getset */
    0,                                  /* tp_base */
    0,                                  /* tp_dict */
    0,                                  /* tp_descr_get */
    0,                                  /* tp_descr_set */
    0,                                  /* tp_dictoffset */
    (initproc)bytearray_init,           /* tp_init */
    PyType_GenericAlloc,                /* tp_alloc */
    PyType_GenericNew,                  /* tp_new */
    PyObject_Del,                       /* tp_free */
};

/*********************** Bytes Iterator ****************************/

typedef struct {
    PyObject_HEAD
    Py_ssize_t it_index;
    PyByteArrayObject *it_seq; /* Set to NULL when iterator is exhausted */
} bytesiterobject;

static void
bytearrayiter_dealloc(bytesiterobject *it)
{
Din_Go(950,2048);    _PyObject_GC_UNTRACK(it);
    Py_XDECREF(it->it_seq);
    Din_Go(951,2048);PyObject_GC_Del(it);
Din_Go(952,2048);}

static int
bytearrayiter_traverse(bytesiterobject *it, visitproc visit, void *arg)
{
Din_Go(953,2048);    Py_VISIT(it->it_seq);
    {int  ReplaceReturn394 = 0; Din_Go(954,2048); return ReplaceReturn394;}
}

static PyObject *
bytearrayiter_next(bytesiterobject *it)
{
    Din_Go(955,2048);PyByteArrayObject *seq;
    PyObject *item;

    assert(it != NULL);
    seq = it->it_seq;
    Din_Go(956,2048);if (seq == NULL)
        return NULL;
    assert(PyByteArray_Check(seq));

    Din_Go(961,2048);if (it->it_index < PyByteArray_GET_SIZE(seq)) {
        Din_Go(957,2048);item = PyInt_FromLong(
            (unsigned char)seq->ob_bytes[it->it_index]);
        Din_Go(959,2048);if (item != NULL)
            {/*277*/Din_Go(958,2048);++it->it_index;/*278*/}
        {PyObject * ReplaceReturn393 = item; Din_Go(960,2048); return ReplaceReturn393;}
    }

    Py_DECREF(seq);
    Din_Go(962,2048);it->it_seq = NULL;
    {PyObject * ReplaceReturn392 = NULL; Din_Go(963,2048); return ReplaceReturn392;}
}

static PyObject *
bytesarrayiter_length_hint(bytesiterobject *it)
{
    Din_Go(964,2048);Py_ssize_t len = 0;
    Din_Go(966,2048);if (it->it_seq)
        {/*279*/Din_Go(965,2048);len = PyByteArray_GET_SIZE(it->it_seq) - it->it_index;/*280*/}
    {PyObject * ReplaceReturn391 = PyInt_FromSsize_t(len); Din_Go(967,2048); return ReplaceReturn391;}
}

PyDoc_STRVAR(length_hint_doc,
    "Private method returning an estimate of len(list(it)).");

static PyMethodDef bytearrayiter_methods[] = {
    {"__length_hint__", (PyCFunction)bytesarrayiter_length_hint, METH_NOARGS,
     length_hint_doc},
    {NULL, NULL} /* sentinel */
};

PyTypeObject PyByteArrayIter_Type = {
    PyVarObject_HEAD_INIT(&PyType_Type, 0)
    "bytearray_iterator",              /* tp_name */
    sizeof(bytesiterobject),           /* tp_basicsize */
    0,                                 /* tp_itemsize */
    /* methods */
    (destructor)bytearrayiter_dealloc, /* tp_dealloc */
    0,                                 /* tp_print */
    0,                                 /* tp_getattr */
    0,                                 /* tp_setattr */
    0,                                 /* tp_compare */
    0,                                 /* tp_repr */
    0,                                 /* tp_as_number */
    0,                                 /* tp_as_sequence */
    0,                                 /* tp_as_mapping */
    0,                                 /* tp_hash */
    0,                                 /* tp_call */
    0,                                 /* tp_str */
    PyObject_GenericGetAttr,           /* tp_getattro */
    0,                                 /* tp_setattro */
    0,                                 /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_HAVE_GC, /* tp_flags */
    0,                                 /* tp_doc */
    (traverseproc)bytearrayiter_traverse,  /* tp_traverse */
    0,                                 /* tp_clear */
    0,                                 /* tp_richcompare */
    0,                                 /* tp_weaklistoffset */
    PyObject_SelfIter,                 /* tp_iter */
    (iternextfunc)bytearrayiter_next,  /* tp_iternext */
    bytearrayiter_methods,             /* tp_methods */
    0,
};

static PyObject *
bytearray_iter(PyObject *seq)
{
    Din_Go(968,2048);bytesiterobject *it;

    Din_Go(970,2048);if (!PyByteArray_Check(seq)) {
        PyErr_BadInternalCall();
        {PyObject * ReplaceReturn390 = NULL; Din_Go(969,2048); return ReplaceReturn390;}
    }
    Din_Go(971,2048);it = PyObject_GC_New(bytesiterobject, &PyByteArrayIter_Type);
    Din_Go(972,2048);if (it == NULL)
        return NULL;
    Din_Go(973,2048);it->it_index = 0;
    Py_INCREF(seq);
    it->it_seq = (PyByteArrayObject *)seq;
    _PyObject_GC_TRACK(it);
    {PyObject * ReplaceReturn389 = (PyObject *)it; Din_Go(974,2048); return ReplaceReturn389;}
}
