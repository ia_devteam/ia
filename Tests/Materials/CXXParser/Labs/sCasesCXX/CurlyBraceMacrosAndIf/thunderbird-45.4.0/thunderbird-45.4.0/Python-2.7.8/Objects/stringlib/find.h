#include "var/tmp/sensor.h"
/* stringlib: find/index implementation */

#ifndef STRINGLIB_FIND_H
#define STRINGLIB_FIND_H

#ifndef STRINGLIB_FASTSEARCH_H
#error must include "stringlib/fastsearch.h" before including this module
#endif

Py_LOCAL_INLINE(Py_ssize_t)
stringlib_find(const STRINGLIB_CHAR* str, Py_ssize_t str_len,
               const STRINGLIB_CHAR* sub, Py_ssize_t sub_len,
               Py_ssize_t offset)
{
    Din_Go(3703,2048);Py_ssize_t pos;

    Din_Go(3705,2048);if (str_len < 0)
        {/*153*/{Py_ssize_t  ReplaceReturn732 = -1; Din_Go(3704,2048); return ReplaceReturn732;}/*154*/}
    Din_Go(3707,2048);if (sub_len == 0)
        {/*155*/{Py_ssize_t  ReplaceReturn731 = offset; Din_Go(3706,2048); return ReplaceReturn731;}/*156*/}

    Din_Go(3708,2048);pos = fastsearch(str, str_len, sub, sub_len, -1, FAST_SEARCH);

    Din_Go(3710,2048);if (pos >= 0)
        {/*157*/Din_Go(3709,2048);pos += offset;/*158*/}

    {Py_ssize_t  ReplaceReturn730 = pos; Din_Go(3711,2048); return ReplaceReturn730;}
}

Py_LOCAL_INLINE(Py_ssize_t)
stringlib_rfind(const STRINGLIB_CHAR* str, Py_ssize_t str_len,
                const STRINGLIB_CHAR* sub, Py_ssize_t sub_len,
                Py_ssize_t offset)
{
    Din_Go(3712,2048);Py_ssize_t pos;

    Din_Go(3714,2048);if (str_len < 0)
        {/*159*/{Py_ssize_t  ReplaceReturn729 = -1; Din_Go(3713,2048); return ReplaceReturn729;}/*160*/}
    Din_Go(3716,2048);if (sub_len == 0)
        {/*161*/{Py_ssize_t  ReplaceReturn728 = str_len + offset; Din_Go(3715,2048); return ReplaceReturn728;}/*162*/}

    Din_Go(3717,2048);pos = fastsearch(str, str_len, sub, sub_len, -1, FAST_RSEARCH);

    Din_Go(3719,2048);if (pos >= 0)
        {/*163*/Din_Go(3718,2048);pos += offset;/*164*/}

    {Py_ssize_t  ReplaceReturn727 = pos; Din_Go(3720,2048); return ReplaceReturn727;}
}

/* helper macro to fixup start/end slice values */
#define ADJUST_INDICES(start, end, len)         \
    if (end > len)                              \
        end = len;                              \
    else if (end < 0) {                         \
        end += len;                             \
        if (end < 0)                            \
            end = 0;                            \
    }                                           \
    if (start < 0) {                            \
        start += len;                           \
        if (start < 0)                          \
            start = 0;                          \
    }

Py_LOCAL_INLINE(Py_ssize_t)
stringlib_find_slice(const STRINGLIB_CHAR* str, Py_ssize_t str_len,
                     const STRINGLIB_CHAR* sub, Py_ssize_t sub_len,
                     Py_ssize_t start, Py_ssize_t end)
{
Din_Go(3721,2048);    ADJUST_INDICES(start, end, str_len);
    {Py_ssize_t  ReplaceReturn726 = stringlib_find(str + start, end - start, sub, sub_len, start); Din_Go(3722,2048); return ReplaceReturn726;}
}

Py_LOCAL_INLINE(Py_ssize_t)
stringlib_rfind_slice(const STRINGLIB_CHAR* str, Py_ssize_t str_len,
                      const STRINGLIB_CHAR* sub, Py_ssize_t sub_len,
                      Py_ssize_t start, Py_ssize_t end)
{
Din_Go(3723,2048);    ADJUST_INDICES(start, end, str_len);
    {Py_ssize_t  ReplaceReturn725 = stringlib_rfind(str + start, end - start, sub, sub_len, start); Din_Go(3724,2048); return ReplaceReturn725;}
}

#ifdef STRINGLIB_WANT_CONTAINS_OBJ

Py_LOCAL_INLINE(int)
stringlib_contains_obj(PyObject* str, PyObject* sub)
{
    return stringlib_find(
        STRINGLIB_STR(str), STRINGLIB_LEN(str),
        STRINGLIB_STR(sub), STRINGLIB_LEN(sub), 0
        ) != -1;
}

#endif /* STRINGLIB_WANT_CONTAINS_OBJ */

/*
This function is a helper for the "find" family (find, rfind, index,
rindex) and for count, startswith and endswith, because they all have
the same behaviour for the arguments.

It does not touch the variables received until it knows everything 
is ok.
*/

#define FORMAT_BUFFER_SIZE 50

Py_LOCAL_INLINE(int)
stringlib_parse_args_finds(const char * function_name, PyObject *args,
                           PyObject **subobj,
                           Py_ssize_t *start, Py_ssize_t *end)
{
    Din_Go(3725,2048);PyObject *tmp_subobj;
    Py_ssize_t tmp_start = 0;
    Py_ssize_t tmp_end = PY_SSIZE_T_MAX;
    PyObject *obj_start=Py_None, *obj_end=Py_None;
    char format[FORMAT_BUFFER_SIZE] = "O|OO:";
    size_t len = strlen(format);

    strncpy(format + len, function_name, FORMAT_BUFFER_SIZE - len - 1);
    format[FORMAT_BUFFER_SIZE - 1] = '\0';

    Din_Go(3727,2048);if (!PyArg_ParseTuple(args, format, &tmp_subobj, &obj_start, &obj_end))
        {/*165*/{int  ReplaceReturn724 = 0; Din_Go(3726,2048); return ReplaceReturn724;}/*166*/}

    /* To support None in "start" and "end" arguments, meaning
       the same as if they were not passed.
    */
    Din_Go(3730,2048);if (obj_start != Py_None)
        {/*167*/Din_Go(3728,2048);if (!_PyEval_SliceIndex(obj_start, &tmp_start))
            {/*169*/{int  ReplaceReturn723 = 0; Din_Go(3729,2048); return ReplaceReturn723;}/*170*/}/*168*/}
    Din_Go(3733,2048);if (obj_end != Py_None)
        {/*171*/Din_Go(3731,2048);if (!_PyEval_SliceIndex(obj_end, &tmp_end))
            {/*173*/{int  ReplaceReturn722 = 0; Din_Go(3732,2048); return ReplaceReturn722;}/*174*/}/*172*/}

    Din_Go(3734,2048);*start = tmp_start;
    *end = tmp_end;
    *subobj = tmp_subobj;
    {int  ReplaceReturn721 = 1; Din_Go(3735,2048); return ReplaceReturn721;}
}

#undef FORMAT_BUFFER_SIZE

#if STRINGLIB_IS_UNICODE

/*
Wraps stringlib_parse_args_finds() and additionally ensures that the
first argument is a unicode object.

Note that we receive a pointer to the pointer of the substring object,
so when we create that object in this function we don't DECREF it,
because it continues living in the caller functions (those functions, 
after finishing using the substring, must DECREF it).
*/

Py_LOCAL_INLINE(int)
stringlib_parse_args_finds_unicode(const char * function_name, PyObject *args,
                                   PyUnicodeObject **substring,
                                   Py_ssize_t *start, Py_ssize_t *end)
{
    PyObject *tmp_substring;

    if(stringlib_parse_args_finds(function_name, args, &tmp_substring,
                                  start, end)) {
        tmp_substring = PyUnicode_FromObject(tmp_substring);
        if (!tmp_substring)
            return 0;
        *substring = (PyUnicodeObject *)tmp_substring;
        return 1;
    }
    return 0;
}

#endif /* STRINGLIB_IS_UNICODE */

#endif /* STRINGLIB_FIND_H */
