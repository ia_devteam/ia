/* Type object implementation */

#include "Python.h"
#include "var/tmp/sensor.h"
#include "structmember.h"

#include <ctype.h>


/* Support type attribute cache */

/* The cache can keep references to the names alive for longer than
   they normally would.  This is why the maximum size is limited to
   MCACHE_MAX_ATTR_SIZE, since it might be a problem if very large
   strings are used as attribute names. */
#define MCACHE_MAX_ATTR_SIZE    100
#define MCACHE_SIZE_EXP         10
#define MCACHE_HASH(version, name_hash)                                 \
        (((unsigned int)(version) * (unsigned int)(name_hash))          \
         >> (8*sizeof(unsigned int) - MCACHE_SIZE_EXP))
#define MCACHE_HASH_METHOD(type, name)                                  \
        MCACHE_HASH((type)->tp_version_tag,                     \
                    ((PyStringObject *)(name))->ob_shash)
#define MCACHE_CACHEABLE_NAME(name)                                     \
        PyString_CheckExact(name) &&                            \
        PyString_GET_SIZE(name) <= MCACHE_MAX_ATTR_SIZE

struct method_cache_entry {
    unsigned int version;
    PyObject *name;             /* reference to exactly a str or None */
    PyObject *value;            /* borrowed */
};

static struct method_cache_entry method_cache[1 << MCACHE_SIZE_EXP];
static unsigned int next_version_tag = 0;

unsigned int
PyType_ClearCache(void)
{
    Din_Go(975,2048);Py_ssize_t i;
    unsigned int cur_version_tag = next_version_tag - 1;

    Din_Go(977,2048);for (i = 0; i < (1 << MCACHE_SIZE_EXP); i++) {
        Din_Go(976,2048);method_cache[i].version = 0;
        Py_CLEAR(method_cache[i].name);
        method_cache[i].value = NULL;
    }
    Din_Go(978,2048);next_version_tag = 0;
    /* mark all version tags as invalid */
    PyType_Modified(&PyBaseObject_Type);
    {unsigned int  ReplaceReturn388 = cur_version_tag; Din_Go(979,2048); return ReplaceReturn388;}
}

void
PyType_Modified(PyTypeObject *type)
{
    /* Invalidate any cached data for the specified type and all
       subclasses.  This function is called after the base
       classes, mro, or attributes of the type are altered.

       Invariants:

       - Py_TPFLAGS_VALID_VERSION_TAG is never set if
         Py_TPFLAGS_HAVE_VERSION_TAG is not set (e.g. on type
         objects coming from non-recompiled extension modules)

       - before Py_TPFLAGS_VALID_VERSION_TAG can be set on a type,
         it must first be set on all super types.

       This function clears the Py_TPFLAGS_VALID_VERSION_TAG of a
       type (so it must first clear it on all subclasses).  The
       tp_version_tag value is meaningless unless this flag is set.
       We don't assign new version tags eagerly, but only as
       needed.
     */
    Din_Go(980,2048);PyObject *raw, *ref;
    Py_ssize_t i, n;

    Din_Go(982,2048);if (!PyType_HasFeature(type, Py_TPFLAGS_VALID_VERSION_TAG))
        {/*53*/Din_Go(981,2048);return;/*54*/}

    Din_Go(983,2048);raw = type->tp_subclasses;
    Din_Go(989,2048);if (raw != NULL) {
        Din_Go(984,2048);n = PyList_GET_SIZE(raw);
        Din_Go(988,2048);for (i = 0; i < n; i++) {
            Din_Go(985,2048);ref = PyList_GET_ITEM(raw, i);
            ref = PyWeakref_GET_OBJECT(ref);
            Din_Go(987,2048);if (ref != Py_None) {
                Din_Go(986,2048);PyType_Modified((PyTypeObject *)ref);
            }
        }
    }
    Din_Go(990,2048);type->tp_flags &= ~Py_TPFLAGS_VALID_VERSION_TAG;
Din_Go(991,2048);}

static void
type_mro_modified(PyTypeObject *type, PyObject *bases) {
    /*
       Check that all base classes or elements of the mro of type are
       able to be cached.  This function is called after the base
       classes or mro of the type are altered.

       Unset HAVE_VERSION_TAG and VALID_VERSION_TAG if the type
       inherits from an old-style class, either directly or if it
       appears in the MRO of a new-style class.  No support either for
       custom MROs that include types that are not officially super
       types.

       Called from mro_internal, which will subsequently be called on
       each subclass when their mro is recursively updated.
     */
    Din_Go(992,2048);Py_ssize_t i, n;
    int clear = 0;

    Din_Go(994,2048);if (!PyType_HasFeature(type, Py_TPFLAGS_HAVE_VERSION_TAG))
        {/*61*/Din_Go(993,2048);return;/*62*/}

    Din_Go(995,2048);n = PyTuple_GET_SIZE(bases);
    Din_Go(1004,2048);for (i = 0; i < n; i++) {
        Din_Go(996,2048);PyObject *b = PyTuple_GET_ITEM(bases, i);
        PyTypeObject *cls;

        Din_Go(999,2048);if (!PyType_Check(b) ) {
            Din_Go(997,2048);clear = 1;
            Din_Go(998,2048);break;
        }

        Din_Go(1000,2048);cls = (PyTypeObject *)b;

        Din_Go(1003,2048);if (!PyType_HasFeature(cls, Py_TPFLAGS_HAVE_VERSION_TAG) ||
            !PyType_IsSubtype(type, cls)) {
            Din_Go(1001,2048);clear = 1;
            Din_Go(1002,2048);break;
        }
    }

    Din_Go(1006,2048);if (clear)
        {/*63*/Din_Go(1005,2048);type->tp_flags &= ~(Py_TPFLAGS_HAVE_VERSION_TAG|
                            Py_TPFLAGS_VALID_VERSION_TAG);/*64*/}
Din_Go(1007,2048);}

static int
assign_version_tag(PyTypeObject *type)
{
    /* Ensure that the tp_version_tag is valid and set
       Py_TPFLAGS_VALID_VERSION_TAG.  To respect the invariant, this
       must first be done on all super classes.  Return 0 if this
       cannot be done, 1 if Py_TPFLAGS_VALID_VERSION_TAG.
    */
    Din_Go(1008,2048);Py_ssize_t i, n;
    PyObject *bases;

    Din_Go(1010,2048);if (PyType_HasFeature(type, Py_TPFLAGS_VALID_VERSION_TAG))
        {/*65*/{int  ReplaceReturn387 = 1; Din_Go(1009,2048); return ReplaceReturn387;}/*66*/}
    Din_Go(1012,2048);if (!PyType_HasFeature(type, Py_TPFLAGS_HAVE_VERSION_TAG))
        {/*67*/{int  ReplaceReturn386 = 0; Din_Go(1011,2048); return ReplaceReturn386;}/*68*/}
    Din_Go(1014,2048);if (!PyType_HasFeature(type, Py_TPFLAGS_READY))
        {/*69*/{int  ReplaceReturn385 = 0; Din_Go(1013,2048); return ReplaceReturn385;}/*70*/}

    Din_Go(1015,2048);type->tp_version_tag = next_version_tag++;
    /* for stress-testing: next_version_tag &= 0xFF; */

    Din_Go(1020,2048);if (type->tp_version_tag == 0) {
        /* wrap-around or just starting Python - clear the whole
           cache by filling names with references to Py_None.
           Values are also set to NULL for added protection, as they
           are borrowed reference */
        Din_Go(1016,2048);for (i = 0; i < (1 << MCACHE_SIZE_EXP); i++) {
            Din_Go(1017,2048);method_cache[i].value = NULL;
            Py_XDECREF(method_cache[i].name);
            method_cache[i].name = Py_None;
            Py_INCREF(Py_None);
        }
        /* mark all version tags as invalid */
        Din_Go(1018,2048);PyType_Modified(&PyBaseObject_Type);
        {int  ReplaceReturn384 = 1; Din_Go(1019,2048); return ReplaceReturn384;}
    }
    Din_Go(1021,2048);bases = type->tp_bases;
    n = PyTuple_GET_SIZE(bases);
    Din_Go(1025,2048);for (i = 0; i < n; i++) {
        Din_Go(1022,2048);PyObject *b = PyTuple_GET_ITEM(bases, i);
        assert(PyType_Check(b));
        Din_Go(1024,2048);if (!assign_version_tag((PyTypeObject *)b))
            {/*71*/{int  ReplaceReturn383 = 0; Din_Go(1023,2048); return ReplaceReturn383;}/*72*/}
    }
    Din_Go(1026,2048);type->tp_flags |= Py_TPFLAGS_VALID_VERSION_TAG;
    {int  ReplaceReturn382 = 1; Din_Go(1027,2048); return ReplaceReturn382;}
}


static PyMemberDef type_members[] = {
    {"__basicsize__", T_PYSSIZET, offsetof(PyTypeObject,tp_basicsize),READONLY},
    {"__itemsize__", T_PYSSIZET, offsetof(PyTypeObject, tp_itemsize), READONLY},
    {"__flags__", T_LONG, offsetof(PyTypeObject, tp_flags), READONLY},
    {"__weakrefoffset__", T_LONG,
     offsetof(PyTypeObject, tp_weaklistoffset), READONLY},
    {"__base__", T_OBJECT, offsetof(PyTypeObject, tp_base), READONLY},
    {"__dictoffset__", T_LONG,
     offsetof(PyTypeObject, tp_dictoffset), READONLY},
    {"__mro__", T_OBJECT, offsetof(PyTypeObject, tp_mro), READONLY},
    {0}
};

static PyObject *
type_name(PyTypeObject *type, void *context)
{
    Din_Go(1028,2048);const char *s;

    Din_Go(1036,2048);if (type->tp_flags & Py_TPFLAGS_HEAPTYPE) {
        Din_Go(1029,2048);PyHeapTypeObject* et = (PyHeapTypeObject*)type;

        Py_INCREF(et->ht_name);
        {PyObject * ReplaceReturn381 = et->ht_name; Din_Go(1030,2048); return ReplaceReturn381;}
    }
    else {
        Din_Go(1031,2048);s = strrchr(type->tp_name, '.');
        Din_Go(1034,2048);if (s == NULL)
            {/*73*/Din_Go(1032,2048);s = type->tp_name;/*74*/}
        else
            {/*75*/Din_Go(1033,2048);s++;/*76*/}
        {PyObject * ReplaceReturn380 = PyString_FromString(s); Din_Go(1035,2048); return ReplaceReturn380;}
    }
Din_Go(1037,2048);}

static int
type_set_name(PyTypeObject *type, PyObject *value, void *context)
{
    Din_Go(1038,2048);PyHeapTypeObject* et;
    PyObject *tmp;

    Din_Go(1041,2048);if (!(type->tp_flags & Py_TPFLAGS_HEAPTYPE)) {
        Din_Go(1039,2048);PyErr_Format(PyExc_TypeError,
                     "can't set %s.__name__", type->tp_name);
        {int  ReplaceReturn379 = -1; Din_Go(1040,2048); return ReplaceReturn379;}
    }
    Din_Go(1044,2048);if (!value) {
        Din_Go(1042,2048);PyErr_Format(PyExc_TypeError,
                     "can't delete %s.__name__", type->tp_name);
        {int  ReplaceReturn378 = -1; Din_Go(1043,2048); return ReplaceReturn378;}
    }
    Din_Go(1047,2048);if (!PyString_Check(value)) {
        Din_Go(1045,2048);PyErr_Format(PyExc_TypeError,
                     "can only assign string to %s.__name__, not '%s'",
                     type->tp_name, Py_TYPE(value)->tp_name);
        {int  ReplaceReturn377 = -1; Din_Go(1046,2048); return ReplaceReturn377;}
    }
    Din_Go(1050,2048);if (strlen(PyString_AS_STRING(value))
        != (size_t)PyString_GET_SIZE(value)) {
        Din_Go(1048,2048);PyErr_Format(PyExc_ValueError,
                     "__name__ must not contain null bytes");
        {int  ReplaceReturn376 = -1; Din_Go(1049,2048); return ReplaceReturn376;}
    }

    Din_Go(1051,2048);et = (PyHeapTypeObject*)type;

    Py_INCREF(value);

    /* Wait until et is a sane state before Py_DECREF'ing the old et->ht_name
       value.  (Bug #16447.)  */
    tmp = et->ht_name;
    et->ht_name = value;

    type->tp_name = PyString_AS_STRING(value);
    Py_DECREF(tmp);

    {int  ReplaceReturn375 = 0; Din_Go(1052,2048); return ReplaceReturn375;}
}

static PyObject *
type_module(PyTypeObject *type, void *context)
{
    Din_Go(1053,2048);PyObject *mod;
    char *s;

    Din_Go(1063,2048);if (type->tp_flags & Py_TPFLAGS_HEAPTYPE) {
        Din_Go(1054,2048);mod = PyDict_GetItemString(type->tp_dict, "__module__");
        Din_Go(1057,2048);if (!mod) {
            Din_Go(1055,2048);PyErr_Format(PyExc_AttributeError, "__module__");
            {PyObject * ReplaceReturn374 = 0; Din_Go(1056,2048); return ReplaceReturn374;}
        }
        Py_XINCREF(mod);
        {PyObject * ReplaceReturn373 = mod; Din_Go(1058,2048); return ReplaceReturn373;}
    }
    else {
        Din_Go(1059,2048);s = strrchr(type->tp_name, '.');
        Din_Go(1061,2048);if (s != NULL)
            {/*77*/{PyObject * ReplaceReturn372 = PyString_FromStringAndSize(
                type->tp_name, (Py_ssize_t)(s - type->tp_name)); Din_Go(1060,2048); return ReplaceReturn372;}/*78*/}
        {PyObject * ReplaceReturn371 = PyString_FromString("__builtin__"); Din_Go(1062,2048); return ReplaceReturn371;}
    }
Din_Go(1064,2048);}

static int
type_set_module(PyTypeObject *type, PyObject *value, void *context)
{
    Din_Go(1065,2048);if (!(type->tp_flags & Py_TPFLAGS_HEAPTYPE)) {
        Din_Go(1066,2048);PyErr_Format(PyExc_TypeError,
                     "can't set %s.__module__", type->tp_name);
        {int  ReplaceReturn370 = -1; Din_Go(1067,2048); return ReplaceReturn370;}
    }
    Din_Go(1070,2048);if (!value) {
        Din_Go(1068,2048);PyErr_Format(PyExc_TypeError,
                     "can't delete %s.__module__", type->tp_name);
        {int  ReplaceReturn369 = -1; Din_Go(1069,2048); return ReplaceReturn369;}
    }

    Din_Go(1071,2048);PyType_Modified(type);

    {int  ReplaceReturn368 = PyDict_SetItemString(type->tp_dict, "__module__", value); Din_Go(1072,2048); return ReplaceReturn368;}
}

static PyObject *
type_abstractmethods(PyTypeObject *type, void *context)
{
    Din_Go(1073,2048);PyObject *mod = NULL;
    /* type itself has an __abstractmethods__ descriptor (this). Don't return
       that. */
    Din_Go(1075,2048);if (type != &PyType_Type)
        {/*79*/Din_Go(1074,2048);mod = PyDict_GetItemString(type->tp_dict, "__abstractmethods__");/*80*/}
    Din_Go(1078,2048);if (!mod) {
        Din_Go(1076,2048);PyErr_SetString(PyExc_AttributeError, "__abstractmethods__");
        {PyObject * ReplaceReturn367 = NULL; Din_Go(1077,2048); return ReplaceReturn367;}
    }
    Py_XINCREF(mod);
    {PyObject * ReplaceReturn366 = mod; Din_Go(1079,2048); return ReplaceReturn366;}
}

static int
type_set_abstractmethods(PyTypeObject *type, PyObject *value, void *context)
{
    /* __abstractmethods__ should only be set once on a type, in
       abc.ABCMeta.__new__, so this function doesn't do anything
       special to update subclasses.
    */
    Din_Go(1080,2048);int abstract, res;
    Din_Go(1089,2048);if (value != NULL) {
        Din_Go(1081,2048);abstract = PyObject_IsTrue(value);
        Din_Go(1083,2048);if (abstract < 0)
            {/*81*/{int  ReplaceReturn365 = -1; Din_Go(1082,2048); return ReplaceReturn365;}/*82*/}
        Din_Go(1084,2048);res = PyDict_SetItemString(type->tp_dict, "__abstractmethods__", value);
    }
    else {
        Din_Go(1085,2048);abstract = 0;
        res = PyDict_DelItemString(type->tp_dict, "__abstractmethods__");
        Din_Go(1088,2048);if (res && PyErr_ExceptionMatches(PyExc_KeyError)) {
            Din_Go(1086,2048);PyErr_SetString(PyExc_AttributeError, "__abstractmethods__");
            {int  ReplaceReturn364 = -1; Din_Go(1087,2048); return ReplaceReturn364;}
        }
    }
    Din_Go(1092,2048);if (res == 0) {
        Din_Go(1090,2048);PyType_Modified(type);
        Din_Go(1091,2048);if (abstract)
            type->tp_flags |= Py_TPFLAGS_IS_ABSTRACT;
        else
            type->tp_flags &= ~Py_TPFLAGS_IS_ABSTRACT;
    }
    {int  ReplaceReturn363 = res; Din_Go(1093,2048); return ReplaceReturn363;}
}

static PyObject *
type_get_bases(PyTypeObject *type, void *context)
{
Din_Go(1094,2048);    Py_INCREF(type->tp_bases);
    {PyObject * ReplaceReturn362 = type->tp_bases; Din_Go(1095,2048); return ReplaceReturn362;}
}

static PyTypeObject *best_base(PyObject *);
static int mro_internal(PyTypeObject *);
static int compatible_for_assignment(PyTypeObject *, PyTypeObject *, char *);
static int add_subclass(PyTypeObject*, PyTypeObject*);
static void remove_subclass(PyTypeObject *, PyTypeObject *);
static void update_all_slots(PyTypeObject *);

typedef int (*update_callback)(PyTypeObject *, void *);
static int update_subclasses(PyTypeObject *type, PyObject *name,
                             update_callback callback, void *data);
static int recurse_down_subclasses(PyTypeObject *type, PyObject *name,
                                   update_callback callback, void *data);

static int
mro_subclasses(PyTypeObject *type, PyObject* temp)
{
    Din_Go(1096,2048);PyTypeObject *subclass;
    PyObject *ref, *subclasses, *old_mro;
    Py_ssize_t i, n;

    subclasses = type->tp_subclasses;
    Din_Go(1098,2048);if (subclasses == NULL)
        {/*121*/{int  ReplaceReturn361 = 0; Din_Go(1097,2048); return ReplaceReturn361;}/*122*/}
    assert(PyList_Check(subclasses));
    Din_Go(1099,2048);n = PyList_GET_SIZE(subclasses);
    Din_Go(1114,2048);for (i = 0; i < n; i++) {
        Din_Go(1100,2048);ref = PyList_GET_ITEM(subclasses, i);
        assert(PyWeakref_CheckRef(ref));
        subclass = (PyTypeObject *)PyWeakref_GET_OBJECT(ref);
        assert(subclass != NULL);
        Din_Go(1102,2048);if ((PyObject *)subclass == Py_None)
            {/*123*/Din_Go(1101,2048);continue;/*124*/}
        assert(PyType_Check(subclass));
        Din_Go(1103,2048);old_mro = subclass->tp_mro;
        Din_Go(1111,2048);if (mro_internal(subclass) < 0) {
            Din_Go(1104,2048);subclass->tp_mro = old_mro;
            {int  ReplaceReturn360 = -1; Din_Go(1105,2048); return ReplaceReturn360;}
        }
        else {
            Din_Go(1106,2048);PyObject* tuple;
            tuple = PyTuple_Pack(2, subclass, old_mro);
            Py_DECREF(old_mro);
            Din_Go(1108,2048);if (!tuple)
                {/*125*/{int  ReplaceReturn359 = -1; Din_Go(1107,2048); return ReplaceReturn359;}/*126*/}
            Din_Go(1110,2048);if (PyList_Append(temp, tuple) < 0)
                {/*127*/{int  ReplaceReturn358 = -1; Din_Go(1109,2048); return ReplaceReturn358;}/*128*/}
            Py_DECREF(tuple);
        }
        Din_Go(1113,2048);if (mro_subclasses(subclass, temp) < 0)
            {/*129*/{int  ReplaceReturn357 = -1; Din_Go(1112,2048); return ReplaceReturn357;}/*130*/}
    }
    {int  ReplaceReturn356 = 0; Din_Go(1115,2048); return ReplaceReturn356;}
}

static int
type_set_bases(PyTypeObject *type, PyObject *value, void *context)
{
    Din_Go(1116,2048);Py_ssize_t i;
    int r = 0;
    PyObject *ob, *temp;
    PyTypeObject *new_base, *old_base;
    PyObject *old_bases, *old_mro;

    Din_Go(1119,2048);if (!(type->tp_flags & Py_TPFLAGS_HEAPTYPE)) {
        Din_Go(1117,2048);PyErr_Format(PyExc_TypeError,
                     "can't set %s.__bases__", type->tp_name);
        {int  ReplaceReturn355 = -1; Din_Go(1118,2048); return ReplaceReturn355;}
    }
    Din_Go(1122,2048);if (!value) {
        Din_Go(1120,2048);PyErr_Format(PyExc_TypeError,
                     "can't delete %s.__bases__", type->tp_name);
        {int  ReplaceReturn354 = -1; Din_Go(1121,2048); return ReplaceReturn354;}
    }
    Din_Go(1125,2048);if (!PyTuple_Check(value)) {
        Din_Go(1123,2048);PyErr_Format(PyExc_TypeError,
             "can only assign tuple to %s.__bases__, not %s",
                 type->tp_name, Py_TYPE(value)->tp_name);
        {int  ReplaceReturn353 = -1; Din_Go(1124,2048); return ReplaceReturn353;}
    }
    Din_Go(1128,2048);if (PyTuple_GET_SIZE(value) == 0) {
        Din_Go(1126,2048);PyErr_Format(PyExc_TypeError,
             "can only assign non-empty tuple to %s.__bases__, not ()",
                 type->tp_name);
        {int  ReplaceReturn352 = -1; Din_Go(1127,2048); return ReplaceReturn352;}
    }
    Din_Go(1137,2048);for (i = 0; i < PyTuple_GET_SIZE(value); i++) {
        Din_Go(1129,2048);ob = PyTuple_GET_ITEM(value, i);
        Din_Go(1132,2048);if (!PyClass_Check(ob) && !PyType_Check(ob)) {
            Din_Go(1130,2048);PyErr_Format(
                PyExc_TypeError,
    "%s.__bases__ must be tuple of old- or new-style classes, not '%s'",
                            type->tp_name, Py_TYPE(ob)->tp_name);
                    {int  ReplaceReturn351 = -1; Din_Go(1131,2048); return ReplaceReturn351;}
        }
        Din_Go(1136,2048);if (PyType_Check(ob)) {
            Din_Go(1133,2048);if (PyType_IsSubtype((PyTypeObject*)ob, type)) {
                Din_Go(1134,2048);PyErr_SetString(PyExc_TypeError,
            "a __bases__ item causes an inheritance cycle");
                {int  ReplaceReturn350 = -1; Din_Go(1135,2048); return ReplaceReturn350;}
            }
        }
    }

    Din_Go(1138,2048);new_base = best_base(value);

    Din_Go(1140,2048);if (!new_base) {
        {int  ReplaceReturn349 = -1; Din_Go(1139,2048); return ReplaceReturn349;}
    }

    Din_Go(1142,2048);if (!compatible_for_assignment(type->tp_base, new_base, "__bases__"))
        {/*131*/{int  ReplaceReturn348 = -1; Din_Go(1141,2048); return ReplaceReturn348;}/*132*/}

    Py_INCREF(new_base);
    Py_INCREF(value);

    Din_Go(1143,2048);old_bases = type->tp_bases;
    old_base = type->tp_base;
    old_mro = type->tp_mro;

    type->tp_bases = value;
    type->tp_base = new_base;

    Din_Go(1145,2048);if (mro_internal(type) < 0) {
        Din_Go(1144,2048);goto bail;
    }

    Din_Go(1146,2048);temp = PyList_New(0);
    Din_Go(1148,2048);if (!temp)
        {/*133*/Din_Go(1147,2048);goto bail;/*134*/}

    Din_Go(1149,2048);r = mro_subclasses(type, temp);

    Din_Go(1153,2048);if (r < 0) {
        Din_Go(1150,2048);for (i = 0; i < PyList_Size(temp); i++) {
            Din_Go(1151,2048);PyTypeObject* cls;
            PyObject* mro;
            PyArg_UnpackTuple(PyList_GET_ITEM(temp, i),
                             "", 2, 2, &cls, &mro);
            Py_INCREF(mro);
            ob = cls->tp_mro;
            cls->tp_mro = mro;
            Py_DECREF(ob);
        }
        Py_DECREF(temp);
        Din_Go(1152,2048);goto bail;
    }

    Py_DECREF(temp);

    /* any base that was in __bases__ but now isn't, we
       need to remove |type| from its tp_subclasses.
       conversely, any class now in __bases__ that wasn't
       needs to have |type| added to its subclasses. */

    /* for now, sod that: just remove from all old_bases,
       add to all new_bases */

    Din_Go(1157,2048);for (i = PyTuple_GET_SIZE(old_bases) - 1; i >= 0; i--) {
        Din_Go(1154,2048);ob = PyTuple_GET_ITEM(old_bases, i);
        Din_Go(1156,2048);if (PyType_Check(ob)) {
            Din_Go(1155,2048);remove_subclass(
                (PyTypeObject*)ob, type);
        }
    }

    Din_Go(1162,2048);for (i = PyTuple_GET_SIZE(value) - 1; i >= 0; i--) {
        Din_Go(1158,2048);ob = PyTuple_GET_ITEM(value, i);
        Din_Go(1161,2048);if (PyType_Check(ob)) {
            Din_Go(1159,2048);if (add_subclass((PyTypeObject*)ob, type) < 0)
                {/*135*/Din_Go(1160,2048);r = -1;/*136*/}
        }
    }

    Din_Go(1163,2048);update_all_slots(type);

    Py_DECREF(old_bases);
    Py_DECREF(old_base);
    Py_DECREF(old_mro);

    {int  ReplaceReturn347 = r; Din_Go(1164,2048); return ReplaceReturn347;}

  bail:
    Py_DECREF(type->tp_bases);
    Py_DECREF(type->tp_base);
    if (type->tp_mro != old_mro) {
        Py_DECREF(type->tp_mro);
    }

    type->tp_bases = old_bases;
    type->tp_base = old_base;
    type->tp_mro = old_mro;

    return -1;
}

static PyObject *
type_dict(PyTypeObject *type, void *context)
{
    Din_Go(1165,2048);if (type->tp_dict == NULL) {
        Py_INCREF(Py_None);
        {PyObject * ReplaceReturn346 = Py_None; Din_Go(1166,2048); return ReplaceReturn346;}
    }
    {PyObject * ReplaceReturn345 = PyDictProxy_New(type->tp_dict); Din_Go(1167,2048); return ReplaceReturn345;}
}

static PyObject *
type_get_doc(PyTypeObject *type, void *context)
{
    Din_Go(1168,2048);PyObject *result;
    Din_Go(1170,2048);if (!(type->tp_flags & Py_TPFLAGS_HEAPTYPE) && type->tp_doc != NULL)
        {/*137*/{PyObject * ReplaceReturn344 = PyString_FromString(type->tp_doc); Din_Go(1169,2048); return ReplaceReturn344;}/*138*/}
    Din_Go(1171,2048);result = PyDict_GetItemString(type->tp_dict, "__doc__");
    Din_Go(1175,2048);if (result == NULL) {
        Din_Go(1172,2048);result = Py_None;
        Py_INCREF(result);
    }
    else {/*139*/Din_Go(1173,2048);if (Py_TYPE(result)->tp_descr_get) {
        Din_Go(1174,2048);result = Py_TYPE(result)->tp_descr_get(result, NULL,
                                               (PyObject *)type);
    }
    else {
        Py_INCREF(result);
    ;/*140*/}}
    {PyObject * ReplaceReturn343 = result; Din_Go(1176,2048); return ReplaceReturn343;}
}

static PyObject *
type___instancecheck__(PyObject *type, PyObject *inst)
{
    Din_Go(1177,2048);switch (_PyObject_RealIsInstance(inst, type)) {
    case -1:
        {PyObject * ReplaceReturn342 = NULL; Din_Go(1178,2048); return ReplaceReturn342;}
    case 0:
        Py_RETURN_FALSE;
    default:
        Py_RETURN_TRUE;
    }
Din_Go(1179,2048);}


static PyObject *
type___subclasscheck__(PyObject *type, PyObject *inst)
{
    Din_Go(1180,2048);switch (_PyObject_RealIsSubclass(inst, type)) {
    case -1:
        {PyObject * ReplaceReturn341 = NULL; Din_Go(1181,2048); return ReplaceReturn341;}
    case 0:
        Py_RETURN_FALSE;
    default:
        Py_RETURN_TRUE;
    }
Din_Go(1182,2048);}


static PyGetSetDef type_getsets[] = {
    {"__name__", (getter)type_name, (setter)type_set_name, NULL},
    {"__bases__", (getter)type_get_bases, (setter)type_set_bases, NULL},
    {"__module__", (getter)type_module, (setter)type_set_module, NULL},
    {"__abstractmethods__", (getter)type_abstractmethods,
     (setter)type_set_abstractmethods, NULL},
    {"__dict__",  (getter)type_dict,  NULL, NULL},
    {"__doc__", (getter)type_get_doc, NULL, NULL},
    {0}
};


static PyObject*
type_richcompare(PyObject *v, PyObject *w, int op)
{
    Din_Go(1183,2048);PyObject *result;
    Py_uintptr_t vv, ww;
    int c;

    /* Make sure both arguments are types. */
    Din_Go(1186,2048);if (!PyType_Check(v) || !PyType_Check(w) ||
        /* If there is a __cmp__ method defined, let it be called instead
           of our dumb function designed merely to warn.  See bug
           #7491. */
        Py_TYPE(v)->tp_compare || Py_TYPE(w)->tp_compare) {
        Din_Go(1184,2048);result = Py_NotImplemented;
        Din_Go(1185,2048);goto out;
    }

    /* Py3K warning if comparison isn't == or !=  */
    Din_Go(1188,2048);if (Py_Py3kWarningFlag && op != Py_EQ && op != Py_NE &&
        PyErr_WarnEx(PyExc_DeprecationWarning,
                   "type inequality comparisons not supported "
                   "in 3.x", 1) < 0) {
        {PyObject * ReplaceReturn340 = NULL; Din_Go(1187,2048); return ReplaceReturn340;}
    }

    /* Compare addresses */
    Din_Go(1189,2048);vv = (Py_uintptr_t)v;
    ww = (Py_uintptr_t)w;
    Din_Go(1204,2048);switch (op) {
    case Py_LT: Din_Go(1190,2048);c = vv <  ww; Din_Go(1191,2048);break;
    case Py_LE: Din_Go(1192,2048);c = vv <= ww; Din_Go(1193,2048);break;
    case Py_EQ: Din_Go(1194,2048);c = vv == ww; Din_Go(1195,2048);break;
    case Py_NE: Din_Go(1196,2048);c = vv != ww; Din_Go(1197,2048);break;
    case Py_GT: Din_Go(1198,2048);c = vv >  ww; Din_Go(1199,2048);break;
    case Py_GE: Din_Go(1200,2048);c = vv >= ww; Din_Go(1201,2048);break;
    default:
        Din_Go(1202,2048);result = Py_NotImplemented;
        Din_Go(1203,2048);goto out;
    }
    Din_Go(1205,2048);result = c ? Py_True : Py_False;

  /* incref and return */
  out:
    Py_INCREF(result);
    {PyObject * ReplaceReturn339 = result; Din_Go(1206,2048); return ReplaceReturn339;}
}

static PyObject *
type_repr(PyTypeObject *type)
{
    Din_Go(1207,2048);PyObject *mod, *name, *rtn;
    char *kind;

    mod = type_module(type, NULL);
    Din_Go(1211,2048);if (mod == NULL)
        {/*141*/Din_Go(1208,2048);PyErr_Clear();/*142*/}
    else {/*143*/Din_Go(1209,2048);if (!PyString_Check(mod)) {
        Py_DECREF(mod);
        Din_Go(1210,2048);mod = NULL;
    ;/*144*/}}
    Din_Go(1212,2048);name = type_name(type, NULL);
    Din_Go(1214,2048);if (name == NULL) {
        Py_XDECREF(mod);
        {PyObject * ReplaceReturn338 = NULL; Din_Go(1213,2048); return ReplaceReturn338;}
    }

    Din_Go(1217,2048);if (type->tp_flags & Py_TPFLAGS_HEAPTYPE)
        {/*145*/Din_Go(1215,2048);kind = "class";/*146*/}
    else
        {/*147*/Din_Go(1216,2048);kind = "type";/*148*/}

    Din_Go(1220,2048);if (mod != NULL && strcmp(PyString_AS_STRING(mod), "__builtin__")) {
        Din_Go(1218,2048);rtn = PyString_FromFormat("<%s '%s.%s'>",
                                  kind,
                                  PyString_AS_STRING(mod),
                                  PyString_AS_STRING(name));
    }
    else
        {/*149*/Din_Go(1219,2048);rtn = PyString_FromFormat("<%s '%s'>", kind, type->tp_name);/*150*/}

    Py_XDECREF(mod);
    Py_DECREF(name);
    {PyObject * ReplaceReturn337 = rtn; Din_Go(1221,2048); return ReplaceReturn337;}
}

static PyObject *
type_call(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    Din_Go(1222,2048);PyObject *obj;

    Din_Go(1225,2048);if (type->tp_new == NULL) {
        Din_Go(1223,2048);PyErr_Format(PyExc_TypeError,
                     "cannot create '%.100s' instances",
                     type->tp_name);
        {PyObject * ReplaceReturn336 = NULL; Din_Go(1224,2048); return ReplaceReturn336;}
    }

    Din_Go(1226,2048);obj = type->tp_new(type, args, kwds);
    Din_Go(1234,2048);if (obj != NULL) {
        /* Ugly exception: when the call was type(something),
           don't call tp_init on the result. */
        Din_Go(1227,2048);if (type == &PyType_Type &&
            PyTuple_Check(args) && PyTuple_GET_SIZE(args) == 1 &&
            (kwds == NULL ||
             (PyDict_Check(kwds) && PyDict_Size(kwds) == 0)))
            {/*151*/{PyObject * ReplaceReturn335 = obj; Din_Go(1228,2048); return ReplaceReturn335;}/*152*/}
        /* If the returned object is not an instance of type,
           it won't be initialized. */
        Din_Go(1230,2048);if (!PyType_IsSubtype(obj->ob_type, type))
            {/*153*/{PyObject * ReplaceReturn334 = obj; Din_Go(1229,2048); return ReplaceReturn334;}/*154*/}
        Din_Go(1231,2048);type = obj->ob_type;
        Din_Go(1233,2048);if (PyType_HasFeature(type, Py_TPFLAGS_HAVE_CLASS) &&
            type->tp_init != NULL &&
            type->tp_init(obj, args, kwds) < 0) {
            Py_DECREF(obj);
            Din_Go(1232,2048);obj = NULL;
        }
    }
    {PyObject * ReplaceReturn333 = obj; Din_Go(1235,2048); return ReplaceReturn333;}
}

PyObject *
PyType_GenericAlloc(PyTypeObject *type, Py_ssize_t nitems)
{
    Din_Go(1236,2048);PyObject *obj;
    const size_t size = _PyObject_VAR_SIZE(type, nitems+1);
    /* note that we need to add one, for the sentinel */

    Din_Go(1239,2048);if (PyType_IS_GC(type))
        {/*41*/Din_Go(1237,2048);obj = _PyObject_GC_Malloc(size);/*42*/}
    else
        {/*43*/Din_Go(1238,2048);obj = (PyObject *)PyObject_MALLOC(size);/*44*/}

    Din_Go(1241,2048);if (obj == NULL)
        {/*45*/{PyObject * ReplaceReturn332 = PyErr_NoMemory(); Din_Go(1240,2048); return ReplaceReturn332;}/*46*/}

    Din_Go(1242,2048);memset(obj, '\0', size);

    Din_Go(1243,2048);if (type->tp_flags & Py_TPFLAGS_HEAPTYPE)
        Py_INCREF(type);

    Din_Go(1244,2048);if (type->tp_itemsize == 0)
        PyObject_INIT(obj, type);
    else
        (void) PyObject_INIT_VAR((PyVarObject *)obj, type, nitems);

    Din_Go(1245,2048);if (PyType_IS_GC(type))
        _PyObject_GC_TRACK(obj);
    {PyObject * ReplaceReturn331 = obj; Din_Go(1246,2048); return ReplaceReturn331;}
}

PyObject *
PyType_GenericNew(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    {PyObject * ReplaceReturn330 = type->tp_alloc(type, 0); Din_Go(1247,2048); return ReplaceReturn330;}
}

/* Helpers for subtyping */

static int
traverse_slots(PyTypeObject *type, PyObject *self, visitproc visit, void *arg)
{
    Din_Go(1248,2048);Py_ssize_t i, n;
    PyMemberDef *mp;

    n = Py_SIZE(type);
    mp = PyHeapType_GET_MEMBERS((PyHeapTypeObject *)type);
    Din_Go(1255,2048);for (i = 0; i < n; i++, mp++) {
        Din_Go(1249,2048);if (mp->type == T_OBJECT_EX) {
            Din_Go(1250,2048);char *addr = (char *)self + mp->offset;
            PyObject *obj = *(PyObject **)addr;
            Din_Go(1254,2048);if (obj != NULL) {
                Din_Go(1251,2048);int err = visit(obj, arg);
                Din_Go(1253,2048);if (err)
                    {/*155*/{int  ReplaceReturn329 = err; Din_Go(1252,2048); return ReplaceReturn329;}/*156*/}
            }
        }
    }
    {int  ReplaceReturn328 = 0; Din_Go(1256,2048); return ReplaceReturn328;}
}

static int
subtype_traverse(PyObject *self, visitproc visit, void *arg)
{
    Din_Go(1257,2048);PyTypeObject *type, *base;
    traverseproc basetraverse;

    /* Find the nearest base with a different tp_traverse,
       and traverse slots while we're at it */
    type = Py_TYPE(self);
    base = type;
    Din_Go(1263,2048);while ((basetraverse = base->tp_traverse) == subtype_traverse) {
        Din_Go(1258,2048);if (Py_SIZE(base)) {
            Din_Go(1259,2048);int err = traverse_slots(base, self, visit, arg);
            Din_Go(1261,2048);if (err)
                {/*157*/{int  ReplaceReturn327 = err; Din_Go(1260,2048); return ReplaceReturn327;}/*158*/}
        }
        Din_Go(1262,2048);base = base->tp_base;
        assert(base);
    }

    Din_Go(1266,2048);if (type->tp_dictoffset != base->tp_dictoffset) {
        Din_Go(1264,2048);PyObject **dictptr = _PyObject_GetDictPtr(self);
        Din_Go(1265,2048);if (dictptr && *dictptr)
            Py_VISIT(*dictptr);
    }

    Din_Go(1267,2048);if (type->tp_flags & Py_TPFLAGS_HEAPTYPE)
        /* For a heaptype, the instances count as references
           to the type.          Traverse the type so the collector
           can find cycles involving this link. */
        Py_VISIT(type);

    Din_Go(1269,2048);if (basetraverse)
        {/*159*/{int  ReplaceReturn326 = basetraverse(self, visit, arg); Din_Go(1268,2048); return ReplaceReturn326;}/*160*/}
    {int  ReplaceReturn325 = 0; Din_Go(1270,2048); return ReplaceReturn325;}
}

static void
clear_slots(PyTypeObject *type, PyObject *self)
{
    Din_Go(1271,2048);Py_ssize_t i, n;
    PyMemberDef *mp;

    n = Py_SIZE(type);
    mp = PyHeapType_GET_MEMBERS((PyHeapTypeObject *)type);
    Din_Go(1276,2048);for (i = 0; i < n; i++, mp++) {
        Din_Go(1272,2048);if (mp->type == T_OBJECT_EX && !(mp->flags & READONLY)) {
            Din_Go(1273,2048);char *addr = (char *)self + mp->offset;
            PyObject *obj = *(PyObject **)addr;
            Din_Go(1275,2048);if (obj != NULL) {
                Din_Go(1274,2048);*(PyObject **)addr = NULL;
                Py_DECREF(obj);
            }
        }
    }
Din_Go(1277,2048);}

static int
subtype_clear(PyObject *self)
{
    Din_Go(1278,2048);PyTypeObject *type, *base;
    inquiry baseclear;

    /* Find the nearest base with a different tp_clear
       and clear slots while we're at it */
    type = Py_TYPE(self);
    base = type;
    Din_Go(1282,2048);while ((baseclear = base->tp_clear) == subtype_clear) {
        Din_Go(1279,2048);if (Py_SIZE(base))
            {/*161*/Din_Go(1280,2048);clear_slots(base, self);/*162*/}
        Din_Go(1281,2048);base = base->tp_base;
        assert(base);
    }

    /* Clear the instance dict (if any), to break cycles involving only
       __dict__ slots (as in the case 'self.__dict__ is self'). */
    Din_Go(1285,2048);if (type->tp_dictoffset != base->tp_dictoffset) {
        Din_Go(1283,2048);PyObject **dictptr = _PyObject_GetDictPtr(self);
        Din_Go(1284,2048);if (dictptr && *dictptr)
            Py_CLEAR(*dictptr);
    }

    Din_Go(1287,2048);if (baseclear)
        {/*163*/{int  ReplaceReturn324 = baseclear(self); Din_Go(1286,2048); return ReplaceReturn324;}/*164*/}
    {int  ReplaceReturn323 = 0; Din_Go(1288,2048); return ReplaceReturn323;}
}

static void
subtype_dealloc(PyObject *self)
{
    Din_Go(1289,2048);PyTypeObject *type, *base;
    destructor basedealloc;
    PyThreadState *tstate = PyThreadState_GET();

    /* Extract the type; we expect it to be a heap type */
    type = Py_TYPE(self);
    assert(type->tp_flags & Py_TPFLAGS_HEAPTYPE);

    /* Test whether the type has GC exactly once */

    Din_Go(1299,2048);if (!PyType_IS_GC(type)) {
        /* It's really rare to find a dynamic type that doesn't have
           GC; it can only happen when deriving from 'object' and not
           adding any slots or instance variables.  This allows
           certain simplifications: there's no need to call
           clear_slots(), or DECREF the dict, or clear weakrefs. */

        /* Maybe call finalizer; exit early if resurrected */
        Din_Go(1290,2048);if (type->tp_del) {
            Din_Go(1291,2048);type->tp_del(self);
            Din_Go(1293,2048);if (self->ob_refcnt > 0)
                {/*165*/Din_Go(1292,2048);return;/*166*/}
        }

        /* Find the nearest base with a different tp_dealloc */
        Din_Go(1294,2048);base = type;
        Din_Go(1296,2048);while ((basedealloc = base->tp_dealloc) == subtype_dealloc) {
            assert(Py_SIZE(base) == 0);
            Din_Go(1295,2048);base = base->tp_base;
            assert(base);
        }

        /* Extract the type again; tp_del may have changed it */
        Din_Go(1297,2048);type = Py_TYPE(self);

        /* Call the base tp_dealloc() */
        assert(basedealloc);
        basedealloc(self);

        /* Can't reference self beyond this point */
        Py_DECREF(type);

        /* Done */
        Din_Go(1298,2048);return;
    }

    /* We get here only if the type has GC */

    /* UnTrack and re-Track around the trashcan macro, alas */
    /* See explanation at end of function for full disclosure */
    Din_Go(1300,2048);PyObject_GC_UnTrack(self);
    ++_PyTrash_delete_nesting;
    ++ tstate->trash_delete_nesting;
    Py_TRASHCAN_SAFE_BEGIN(self);
    --_PyTrash_delete_nesting;
    -- tstate->trash_delete_nesting;
    /* DO NOT restore GC tracking at this point.  weakref callbacks
     * (if any, and whether directly here or indirectly in something we
     * call) may trigger GC, and if self is tracked at that point, it
     * will look like trash to GC and GC will try to delete self again.
     */

    /* Find the nearest base with a different tp_dealloc */
    base = type;
    while ((basedealloc = base->tp_dealloc) == subtype_dealloc) {
        base = base->tp_base;
        assert(base);
    }

    /* If we added a weaklist, we clear it.      Do this *before* calling
       the finalizer (__del__), clearing slots, or clearing the instance
       dict. */

    if (type->tp_weaklistoffset && !base->tp_weaklistoffset)
        {/*167*/PyObject_ClearWeakRefs(self);/*168*/}

    /* Maybe call finalizer; exit early if resurrected */
    if (type->tp_del) {
        _PyObject_GC_TRACK(self);
        type->tp_del(self);
        if (self->ob_refcnt > 0)
            {/*169*/goto endlabel;/*170*/}              /* resurrected */
        else
            _PyObject_GC_UNTRACK(self);
        /* New weakrefs could be created during the finalizer call.
            If this occurs, clear them out without calling their
            finalizers since they might rely on part of the object
            being finalized that has already been destroyed. */
        if (type->tp_weaklistoffset && !base->tp_weaklistoffset) {
            /* Modeled after GET_WEAKREFS_LISTPTR() */
            PyWeakReference **list = (PyWeakReference **) \
                PyObject_GET_WEAKREFS_LISTPTR(self);
            while (*list)
                {/*171*/_PyWeakref_ClearRef(*list);/*172*/}
        }
    }

    /*  Clear slots up to the nearest base with a different tp_dealloc */
    base = type;
    while (base->tp_dealloc == subtype_dealloc) {
        if (Py_SIZE(base))
            {/*173*/clear_slots(base, self);/*174*/}
        base = base->tp_base;
        assert(base);
    }

    /* If we added a dict, DECREF it */
    if (type->tp_dictoffset && !base->tp_dictoffset) {
        PyObject **dictptr = _PyObject_GetDictPtr(self);
        if (dictptr != NULL) {
            PyObject *dict = *dictptr;
            if (dict != NULL) {
                Py_DECREF(dict);
                *dictptr = NULL;
            }
        }
    }

    /* Extract the type again; tp_del may have changed it */
    type = Py_TYPE(self);

    /* Call the base tp_dealloc(); first retrack self if
     * basedealloc knows about gc.
     */
    if (PyType_IS_GC(base))
        _PyObject_GC_TRACK(self);
    assert(basedealloc);
    basedealloc(self);

    /* Can't reference self beyond this point */
    Py_DECREF(type);

  endlabel:
    ++_PyTrash_delete_nesting;
    ++ tstate->trash_delete_nesting;
    Py_TRASHCAN_SAFE_END(self);
    --_PyTrash_delete_nesting;
    -- tstate->trash_delete_nesting;

    /* Explanation of the weirdness around the trashcan macros:

       Q. What do the trashcan macros do?

       A. Read the comment titled "Trashcan mechanism" in object.h.
          For one, this explains why there must be a call to GC-untrack
          before the trashcan begin macro.      Without understanding the
          trashcan code, the answers to the following questions don't make
          sense.

       Q. Why do we GC-untrack before the trashcan and then immediately
          GC-track again afterward?

       A. In the case that the base class is GC-aware, the base class
          probably GC-untracks the object.      If it does that using the
          UNTRACK macro, this will crash when the object is already
          untracked.  Because we don't know what the base class does, the
          only safe thing is to make sure the object is tracked when we
          call the base class dealloc.  But...  The trashcan begin macro
          requires that the object is *untracked* before it is called.  So
          the dance becomes:

         GC untrack
         trashcan begin
         GC track

       Q. Why did the last question say "immediately GC-track again"?
          It's nowhere near immediately.

       A. Because the code *used* to re-track immediately.      Bad Idea.
          self has a refcount of 0, and if gc ever gets its hands on it
          (which can happen if any weakref callback gets invoked), it
          looks like trash to gc too, and gc also tries to delete self
          then.  But we're already deleting self.  Double deallocation is
          a subtle disaster.

       Q. Why the bizarre (net-zero) manipulation of
          _PyTrash_delete_nesting around the trashcan macros?

       A. Some base classes (e.g. list) also use the trashcan mechanism.
          The following scenario used to be possible:

          - suppose the trashcan level is one below the trashcan limit

          - subtype_dealloc() is called

          - the trashcan limit is not yet reached, so the trashcan level
        is incremented and the code between trashcan begin and end is
        executed

          - this destroys much of the object's contents, including its
        slots and __dict__

          - basedealloc() is called; this is really list_dealloc(), or
        some other type which also uses the trashcan macros

          - the trashcan limit is now reached, so the object is put on the
        trashcan's to-be-deleted-later list

          - basedealloc() returns

          - subtype_dealloc() decrefs the object's type

          - subtype_dealloc() returns

          - later, the trashcan code starts deleting the objects from its
        to-be-deleted-later list

          - subtype_dealloc() is called *AGAIN* for the same object

          - at the very least (if the destroyed slots and __dict__ don't
        cause problems) the object's type gets decref'ed a second
        time, which is *BAD*!!!

          The remedy is to make sure that if the code between trashcan
          begin and end in subtype_dealloc() is called, the code between
          trashcan begin and end in basedealloc() will also be called.
          This is done by decrementing the level after passing into the
          trashcan block, and incrementing it just before leaving the
          block.

          But now it's possible that a chain of objects consisting solely
          of objects whose deallocator is subtype_dealloc() will defeat
          the trashcan mechanism completely: the decremented level means
          that the effective level never reaches the limit.      Therefore, we
          *increment* the level *before* entering the trashcan block, and
          matchingly decrement it after leaving.  This means the trashcan
          code will trigger a little early, but that's no big deal.

       Q. Are there any live examples of code in need of all this
          complexity?

       A. Yes.  See SF bug 668433 for code that crashed (when Python was
          compiled in debug mode) before the trashcan level manipulations
          were added.  For more discussion, see SF patches 581742, 575073
          and bug 574207.
    */
Din_Go(1301,2048);}

static PyTypeObject *solid_base(PyTypeObject *type);

/* type test with subclassing support */

int
PyType_IsSubtype(PyTypeObject *a, PyTypeObject *b)
{
    Din_Go(1302,2048);PyObject *mro;

    Din_Go(1304,2048);if (!(a->tp_flags & Py_TPFLAGS_HAVE_CLASS))
        {/*1*/{int  ReplaceReturn322 = b == a || b == &PyBaseObject_Type; Din_Go(1303,2048); return ReplaceReturn322;}/*2*/}

    Din_Go(1305,2048);mro = a->tp_mro;
    Din_Go(1316,2048);if (mro != NULL) {
        /* Deal with multiple inheritance without recursion
           by walking the MRO tuple */
        Din_Go(1306,2048);Py_ssize_t i, n;
        assert(PyTuple_Check(mro));
        n = PyTuple_GET_SIZE(mro);
        Din_Go(1309,2048);for (i = 0; i < n; i++) {
            Din_Go(1307,2048);if (PyTuple_GET_ITEM(mro, i) == (PyObject *)b)
                {/*3*/{int  ReplaceReturn321 = 1; Din_Go(1308,2048); return ReplaceReturn321;}/*4*/}
        }
        {int  ReplaceReturn320 = 0; Din_Go(1310,2048); return ReplaceReturn320;}
    }
    else {
        /* a is not completely initilized yet; follow tp_base */
        Din_Go(1311,2048);do {
            Din_Go(1312,2048);if (a == b)
                {/*5*/{int  ReplaceReturn319 = 1; Din_Go(1313,2048); return ReplaceReturn319;}/*6*/}
            Din_Go(1314,2048);a = a->tp_base;
        } while (a != NULL);
        {int  ReplaceReturn318 = b == &PyBaseObject_Type; Din_Go(1315,2048); return ReplaceReturn318;}
    }
Din_Go(1317,2048);}

/* Internal routines to do a method lookup in the type
   without looking in the instance dictionary
   (so we can't use PyObject_GetAttr) but still binding
   it to the instance.  The arguments are the object,
   the method name as a C string, and the address of a
   static variable used to cache the interned Python string.

   Two variants:

   - lookup_maybe() returns NULL without raising an exception
     when the _PyType_Lookup() call fails;

   - lookup_method() always raises an exception upon errors.

   - _PyObject_LookupSpecial() exported for the benefit of other places.
*/

static PyObject *
lookup_maybe(PyObject *self, char *attrstr, PyObject **attrobj)
{
    Din_Go(1318,2048);PyObject *res;

    Din_Go(1321,2048);if (*attrobj == NULL) {
        Din_Go(1319,2048);*attrobj = PyString_InternFromString(attrstr);
        Din_Go(1320,2048);if (*attrobj == NULL)
            return NULL;
    }
    Din_Go(1322,2048);res = _PyType_Lookup(Py_TYPE(self), *attrobj);
    Din_Go(1326,2048);if (res != NULL) {
        Din_Go(1323,2048);descrgetfunc f;
        Din_Go(1325,2048);if ((f = Py_TYPE(res)->tp_descr_get) == NULL)
            Py_INCREF(res);
        else
            {/*183*/Din_Go(1324,2048);res = f(res, self, (PyObject *)(Py_TYPE(self)));/*184*/}
    }
    {PyObject * ReplaceReturn317 = res; Din_Go(1327,2048); return ReplaceReturn317;}
}

static PyObject *
lookup_method(PyObject *self, char *attrstr, PyObject **attrobj)
{
    Din_Go(1328,2048);PyObject *res = lookup_maybe(self, attrstr, attrobj);
    Din_Go(1330,2048);if (res == NULL && !PyErr_Occurred())
        {/*185*/Din_Go(1329,2048);PyErr_SetObject(PyExc_AttributeError, *attrobj);/*186*/}
    {PyObject * ReplaceReturn316 = res; Din_Go(1331,2048); return ReplaceReturn316;}
}

PyObject *
_PyObject_LookupSpecial(PyObject *self, char *attrstr, PyObject **attrobj)
{
Din_Go(1332,2048);    assert(!PyInstance_Check(self));
    {PyObject * ReplaceReturn315 = lookup_maybe(self, attrstr, attrobj); Din_Go(1333,2048); return ReplaceReturn315;}
}

/* A variation of PyObject_CallMethod that uses lookup_method()
   instead of PyObject_GetAttrString().  This uses the same convention
   as lookup_method to cache the interned name string object. */

static PyObject *
call_method(PyObject *o, char *name, PyObject **nameobj, char *format, ...)
{
    Din_Go(1334,2048);va_list va;
    PyObject *args, *func = 0, *retval;
    va_start(va, format);

    func = lookup_maybe(o, name, nameobj);
    Din_Go(1338,2048);if (func == NULL) {
        va_end(va);
        Din_Go(1336,2048);if (!PyErr_Occurred())
            {/*187*/Din_Go(1335,2048);PyErr_SetObject(PyExc_AttributeError, *nameobj);/*188*/}
        {PyObject * ReplaceReturn314 = NULL; Din_Go(1337,2048); return ReplaceReturn314;}
    }

    Din_Go(1341,2048);if (format && *format)
        {/*189*/Din_Go(1339,2048);args = Py_VaBuildValue(format, va);/*190*/}
    else
        {/*191*/Din_Go(1340,2048);args = PyTuple_New(0);/*192*/}

    va_end(va);

    Din_Go(1342,2048);if (args == NULL)
        return NULL;

    assert(PyTuple_Check(args));
    Din_Go(1343,2048);retval = PyObject_Call(func, args, NULL);

    Py_DECREF(args);
    Py_DECREF(func);

    {PyObject * ReplaceReturn313 = retval; Din_Go(1344,2048); return ReplaceReturn313;}
}

/* Clone of call_method() that returns NotImplemented when the lookup fails. */

static PyObject *
call_maybe(PyObject *o, char *name, PyObject **nameobj, char *format, ...)
{
    Din_Go(1345,2048);va_list va;
    PyObject *args, *func = 0, *retval;
    va_start(va, format);

    func = lookup_maybe(o, name, nameobj);
    Din_Go(1349,2048);if (func == NULL) {
        va_end(va);
        Din_Go(1347,2048);if (!PyErr_Occurred()) {
            Py_INCREF(Py_NotImplemented);
            {PyObject * ReplaceReturn312 = Py_NotImplemented; Din_Go(1346,2048); return ReplaceReturn312;}
        }
        {PyObject * ReplaceReturn311 = NULL; Din_Go(1348,2048); return ReplaceReturn311;}
    }

    Din_Go(1352,2048);if (format && *format)
        {/*193*/Din_Go(1350,2048);args = Py_VaBuildValue(format, va);/*194*/}
    else
        {/*195*/Din_Go(1351,2048);args = PyTuple_New(0);/*196*/}

    va_end(va);

    Din_Go(1353,2048);if (args == NULL)
        return NULL;

    assert(PyTuple_Check(args));
    Din_Go(1354,2048);retval = PyObject_Call(func, args, NULL);

    Py_DECREF(args);
    Py_DECREF(func);

    {PyObject * ReplaceReturn310 = retval; Din_Go(1355,2048); return ReplaceReturn310;}
}

static int
fill_classic_mro(PyObject *mro, PyObject *cls)
{
    Din_Go(1356,2048);PyObject *bases, *base;
    Py_ssize_t i, n;

    assert(PyList_Check(mro));
    assert(PyClass_Check(cls));
    i = PySequence_Contains(mro, cls);
    Din_Go(1358,2048);if (i < 0)
        {/*197*/{int  ReplaceReturn309 = -1; Din_Go(1357,2048); return ReplaceReturn309;}/*198*/}
    Din_Go(1361,2048);if (!i) {
        Din_Go(1359,2048);if (PyList_Append(mro, cls) < 0)
            {/*199*/{int  ReplaceReturn308 = -1; Din_Go(1360,2048); return ReplaceReturn308;}/*200*/}
    }
    Din_Go(1362,2048);bases = ((PyClassObject *)cls)->cl_bases;
    assert(bases && PyTuple_Check(bases));
    n = PyTuple_GET_SIZE(bases);
    Din_Go(1366,2048);for (i = 0; i < n; i++) {
        Din_Go(1363,2048);base = PyTuple_GET_ITEM(bases, i);
        Din_Go(1365,2048);if (fill_classic_mro(mro, base) < 0)
            {/*201*/{int  ReplaceReturn307 = -1; Din_Go(1364,2048); return ReplaceReturn307;}/*202*/}
    }
    {int  ReplaceReturn306 = 0; Din_Go(1367,2048); return ReplaceReturn306;}
}

static PyObject *
classic_mro(PyObject *cls)
{
    Din_Go(1368,2048);PyObject *mro;

    assert(PyClass_Check(cls));
    mro = PyList_New(0);
    Din_Go(1371,2048);if (mro != NULL) {
        Din_Go(1369,2048);if (fill_classic_mro(mro, cls) == 0)
            {/*203*/{PyObject * ReplaceReturn305 = mro; Din_Go(1370,2048); return ReplaceReturn305;}/*204*/}
        Py_DECREF(mro);
    }
    {PyObject * ReplaceReturn304 = NULL; Din_Go(1372,2048); return ReplaceReturn304;}
}

/*
    Method resolution order algorithm C3 described in
    "A Monotonic Superclass Linearization for Dylan",
    by Kim Barrett, Bob Cassel, Paul Haahr,
    David A. Moon, Keith Playford, and P. Tucker Withington.
    (OOPSLA 1996)

    Some notes about the rules implied by C3:

    No duplicate bases.
    It isn't legal to repeat a class in a list of base classes.

    The next three properties are the 3 constraints in "C3".

    Local precendece order.
    If A precedes B in C's MRO, then A will precede B in the MRO of all
    subclasses of C.

    Monotonicity.
    The MRO of a class must be an extension without reordering of the
    MRO of each of its superclasses.

    Extended Precedence Graph (EPG).
    Linearization is consistent if there is a path in the EPG from
    each class to all its successors in the linearization.  See
    the paper for definition of EPG.
 */

static int
tail_contains(PyObject *list, int whence, PyObject *o) {
    Din_Go(1373,2048);Py_ssize_t j, size;
    size = PyList_GET_SIZE(list);

    Din_Go(1376,2048);for (j = whence+1; j < size; j++) {
        Din_Go(1374,2048);if (PyList_GET_ITEM(list, j) == o)
            {/*205*/{int  ReplaceReturn303 = 1; Din_Go(1375,2048); return ReplaceReturn303;}/*206*/}
    }
    {int  ReplaceReturn302 = 0; Din_Go(1377,2048); return ReplaceReturn302;}
}

static PyObject *
class_name(PyObject *cls)
{
    Din_Go(1378,2048);PyObject *name = PyObject_GetAttrString(cls, "__name__");
    Din_Go(1380,2048);if (name == NULL) {
        Din_Go(1379,2048);PyErr_Clear();
        Py_XDECREF(name);
        name = PyObject_Repr(cls);
    }
    Din_Go(1381,2048);if (name == NULL)
        return NULL;
    Din_Go(1383,2048);if (!PyString_Check(name)) {
        Py_DECREF(name);
        {PyObject * ReplaceReturn301 = NULL; Din_Go(1382,2048); return ReplaceReturn301;}
    }
    {PyObject * ReplaceReturn300 = name; Din_Go(1384,2048); return ReplaceReturn300;}
}

static int
check_duplicates(PyObject *list)
{
    Din_Go(1385,2048);Py_ssize_t i, j, n;
    /* Let's use a quadratic time algorithm,
       assuming that the bases lists is short.
    */
    n = PyList_GET_SIZE(list);
    Din_Go(1391,2048);for (i = 0; i < n; i++) {
        Din_Go(1386,2048);PyObject *o = PyList_GET_ITEM(list, i);
        Din_Go(1390,2048);for (j = i + 1; j < n; j++) {
            Din_Go(1387,2048);if (PyList_GET_ITEM(list, j) == o) {
                Din_Go(1388,2048);o = class_name(o);
                PyErr_Format(PyExc_TypeError,
                             "duplicate base class %s",
                             o ? PyString_AS_STRING(o) : "?");
                Py_XDECREF(o);
                {int  ReplaceReturn299 = -1; Din_Go(1389,2048); return ReplaceReturn299;}
            }
        }
    }
    {int  ReplaceReturn298 = 0; Din_Go(1392,2048); return ReplaceReturn298;}
}

/* Raise a TypeError for an MRO order disagreement.

   It's hard to produce a good error message.  In the absence of better
   insight into error reporting, report the classes that were candidates
   to be put next into the MRO.  There is some conflict between the
   order in which they should be put in the MRO, but it's hard to
   diagnose what constraint can't be satisfied.
*/

static void
set_mro_error(PyObject *to_merge, int *remain)
{
    Din_Go(1393,2048);Py_ssize_t i, n, off, to_merge_size;
    char buf[1000];
    PyObject *k, *v;
    PyObject *set = PyDict_New();
    Din_Go(1395,2048);if (!set) {/*207*/Din_Go(1394,2048);return;/*208*/}

    Din_Go(1396,2048);to_merge_size = PyList_GET_SIZE(to_merge);
    Din_Go(1402,2048);for (i = 0; i < to_merge_size; i++) {
        Din_Go(1397,2048);PyObject *L = PyList_GET_ITEM(to_merge, i);
        Din_Go(1401,2048);if (remain[i] < PyList_GET_SIZE(L)) {
            Din_Go(1398,2048);PyObject *c = PyList_GET_ITEM(L, remain[i]);
            Din_Go(1400,2048);if (PyDict_SetItem(set, c, Py_None) < 0) {
                Py_DECREF(set);
                Din_Go(1399,2048);return;
            }
        }
    }
    Din_Go(1403,2048);n = PyDict_Size(set);

    off = PyOS_snprintf(buf, sizeof(buf), "Cannot create a \
consistent method resolution\norder (MRO) for bases");
    i = 0;
    Din_Go(1407,2048);while (PyDict_Next(set, &i, &k, &v) && (size_t)off < sizeof(buf)) {
        Din_Go(1404,2048);PyObject *name = class_name(k);
        off += PyOS_snprintf(buf + off, sizeof(buf) - off, " %s",
                             name ? PyString_AS_STRING(name) : "?");
        Py_XDECREF(name);
        Din_Go(1406,2048);if (--n && (size_t)(off+1) < sizeof(buf)) {
            Din_Go(1405,2048);buf[off++] = ',';
            buf[off] = '\0';
        }
    }
    Din_Go(1408,2048);PyErr_SetString(PyExc_TypeError, buf);
    Py_DECREF(set);
}

static int
pmerge(PyObject *acc, PyObject* to_merge) {
    Din_Go(1409,2048);Py_ssize_t i, j, to_merge_size, empty_cnt;
    int *remain;
    int ok;

    to_merge_size = PyList_GET_SIZE(to_merge);

    /* remain stores an index into each sublist of to_merge.
       remain[i] is the index of the next base in to_merge[i]
       that is not included in acc.
    */
    remain = (int *)PyMem_MALLOC(SIZEOF_INT*to_merge_size);
    Din_Go(1411,2048);if (remain == NULL)
        {/*209*/{int  ReplaceReturn297 = -1; Din_Go(1410,2048); return ReplaceReturn297;}/*210*/}
    Din_Go(1413,2048);for (i = 0; i < to_merge_size; i++)
        {/*211*/Din_Go(1412,2048);remain[i] = 0;/*212*/}

  again:
    Din_Go(1414,2048);empty_cnt = 0;
    Din_Go(1433,2048);for (i = 0; i < to_merge_size; i++) {
        Din_Go(1415,2048);PyObject *candidate;

        PyObject *cur_list = PyList_GET_ITEM(to_merge, i);

        Din_Go(1418,2048);if (remain[i] >= PyList_GET_SIZE(cur_list)) {
            Din_Go(1416,2048);empty_cnt++;
            Din_Go(1417,2048);continue;
        }

        /* Choose next candidate for MRO.

           The input sequences alone can determine the choice.
           If not, choose the class which appears in the MRO
           of the earliest direct superclass of the new class.
        */

        Din_Go(1419,2048);candidate = PyList_GET_ITEM(cur_list, remain[i]);
        Din_Go(1423,2048);for (j = 0; j < to_merge_size; j++) {
            Din_Go(1420,2048);PyObject *j_lst = PyList_GET_ITEM(to_merge, j);
            Din_Go(1422,2048);if (tail_contains(j_lst, remain[j], candidate)) {
                Din_Go(1421,2048);goto skip; /* continue outer loop */
            }
        }
        Din_Go(1424,2048);ok = PyList_Append(acc, candidate);
        Din_Go(1427,2048);if (ok < 0) {
            Din_Go(1425,2048);PyMem_Free(remain);
            {int  ReplaceReturn296 = -1; Din_Go(1426,2048); return ReplaceReturn296;}
        }
        Din_Go(1431,2048);for (j = 0; j < to_merge_size; j++) {
            Din_Go(1428,2048);PyObject *j_lst = PyList_GET_ITEM(to_merge, j);
            Din_Go(1430,2048);if (remain[j] < PyList_GET_SIZE(j_lst) &&
                PyList_GET_ITEM(j_lst, remain[j]) == candidate) {
                Din_Go(1429,2048);remain[j]++;
            }
        }
        Din_Go(1432,2048);goto again;
      skip: ;
    }

    Din_Go(1435,2048);if (empty_cnt == to_merge_size) {
        PyMem_FREE(remain);
        {int  ReplaceReturn295 = 0; Din_Go(1434,2048); return ReplaceReturn295;}
    }
    Din_Go(1436,2048);set_mro_error(to_merge, remain);
    PyMem_FREE(remain);
    {int  ReplaceReturn294 = -1; Din_Go(1437,2048); return ReplaceReturn294;}
}

static PyObject *
mro_implementation(PyTypeObject *type)
{
    Din_Go(1438,2048);Py_ssize_t i, n;
    int ok;
    PyObject *bases, *result;
    PyObject *to_merge, *bases_aslist;

    Din_Go(1440,2048);if (type->tp_dict == NULL) {
        Din_Go(1439,2048);if (PyType_Ready(type) < 0)
            return NULL;
    }

    /* Find a superclass linearization that honors the constraints
       of the explicit lists of bases and the constraints implied by
       each base class.

       to_merge is a list of lists, where each list is a superclass
       linearization implied by a base class.  The last element of
       to_merge is the declared list of bases.
    */

    Din_Go(1441,2048);bases = type->tp_bases;
    n = PyTuple_GET_SIZE(bases);

    to_merge = PyList_New(n+1);
    Din_Go(1442,2048);if (to_merge == NULL)
        return NULL;

    Din_Go(1449,2048);for (i = 0; i < n; i++) {
        Din_Go(1443,2048);PyObject *base = PyTuple_GET_ITEM(bases, i);
        PyObject *parentMRO;
        Din_Go(1446,2048);if (PyType_Check(base))
            {/*213*/Din_Go(1444,2048);parentMRO = PySequence_List(
                ((PyTypeObject*)base)->tp_mro);/*214*/}
        else
            {/*215*/Din_Go(1445,2048);parentMRO = classic_mro(base);/*216*/}
        Din_Go(1448,2048);if (parentMRO == NULL) {
            Py_DECREF(to_merge);
            {PyObject * ReplaceReturn293 = NULL; Din_Go(1447,2048); return ReplaceReturn293;}
        }

        PyList_SET_ITEM(to_merge, i, parentMRO);
    }

    Din_Go(1450,2048);bases_aslist = PySequence_List(bases);
    Din_Go(1452,2048);if (bases_aslist == NULL) {
        Py_DECREF(to_merge);
        {PyObject * ReplaceReturn292 = NULL; Din_Go(1451,2048); return ReplaceReturn292;}
    }
    /* This is just a basic sanity check. */
    Din_Go(1454,2048);if (check_duplicates(bases_aslist) < 0) {
        Py_DECREF(to_merge);
        Py_DECREF(bases_aslist);
        {PyObject * ReplaceReturn291 = NULL; Din_Go(1453,2048); return ReplaceReturn291;}
    }
    PyList_SET_ITEM(to_merge, n, bases_aslist);

    Din_Go(1455,2048);result = Py_BuildValue("[O]", (PyObject *)type);
    Din_Go(1457,2048);if (result == NULL) {
        Py_DECREF(to_merge);
        {PyObject * ReplaceReturn290 = NULL; Din_Go(1456,2048); return ReplaceReturn290;}
    }

    Din_Go(1458,2048);ok = pmerge(result, to_merge);
    Py_DECREF(to_merge);
    Din_Go(1460,2048);if (ok < 0) {
        Py_DECREF(result);
        {PyObject * ReplaceReturn289 = NULL; Din_Go(1459,2048); return ReplaceReturn289;}
    }

    {PyObject * ReplaceReturn288 = result; Din_Go(1461,2048); return ReplaceReturn288;}
}

static PyObject *
mro_external(PyObject *self)
{
    Din_Go(1462,2048);PyTypeObject *type = (PyTypeObject *)self;

    {PyObject * ReplaceReturn287 = mro_implementation(type); Din_Go(1463,2048); return ReplaceReturn287;}
}

static int
mro_internal(PyTypeObject *type)
{
    Din_Go(1464,2048);PyObject *mro, *result, *tuple;
    int checkit = 0;

    Din_Go(1470,2048);if (Py_TYPE(type) == &PyType_Type) {
        Din_Go(1465,2048);result = mro_implementation(type);
    }
    else {
        Din_Go(1466,2048);static PyObject *mro_str;
        checkit = 1;
        mro = lookup_method((PyObject *)type, "mro", &mro_str);
        Din_Go(1468,2048);if (mro == NULL)
            {/*93*/{int  ReplaceReturn286 = -1; Din_Go(1467,2048); return ReplaceReturn286;}/*94*/}
        Din_Go(1469,2048);result = PyObject_CallObject(mro, NULL);
        Py_DECREF(mro);
    }
    Din_Go(1472,2048);if (result == NULL)
        {/*95*/{int  ReplaceReturn285 = -1; Din_Go(1471,2048); return ReplaceReturn285;}/*96*/}
    Din_Go(1473,2048);tuple = PySequence_Tuple(result);
    Py_DECREF(result);
    Din_Go(1475,2048);if (tuple == NULL)
        {/*97*/{int  ReplaceReturn284 = -1; Din_Go(1474,2048); return ReplaceReturn284;}/*98*/}
    Din_Go(1488,2048);if (checkit) {
        Din_Go(1476,2048);Py_ssize_t i, len;
        PyObject *cls;
        PyTypeObject *solid;

        solid = solid_base(type);

        len = PyTuple_GET_SIZE(tuple);

        Din_Go(1487,2048);for (i = 0; i < len; i++) {
            Din_Go(1477,2048);PyTypeObject *t;
            cls = PyTuple_GET_ITEM(tuple, i);
            Din_Go(1482,2048);if (PyClass_Check(cls))
                {/*99*/Din_Go(1478,2048);continue;/*100*/}
            else {/*101*/Din_Go(1479,2048);if (!PyType_Check(cls)) {
                Din_Go(1480,2048);PyErr_Format(PyExc_TypeError,
                 "mro() returned a non-class ('%.500s')",
                                 Py_TYPE(cls)->tp_name);
                Py_DECREF(tuple);
                {int  ReplaceReturn283 = -1; Din_Go(1481,2048); return ReplaceReturn283;}
            ;/*102*/}}
            Din_Go(1483,2048);t = (PyTypeObject*)cls;
            Din_Go(1486,2048);if (!PyType_IsSubtype(solid, solid_base(t))) {
                Din_Go(1484,2048);PyErr_Format(PyExc_TypeError,
             "mro() returned base with unsuitable layout ('%.500s')",
                                     t->tp_name);
                        Py_DECREF(tuple);
                        {int  ReplaceReturn282 = -1; Din_Go(1485,2048); return ReplaceReturn282;}
            }
        }
    }
    Din_Go(1489,2048);type->tp_mro = tuple;

    type_mro_modified(type, type->tp_mro);
    /* corner case: the old-style super class might have been hidden
       from the custom MRO */
    type_mro_modified(type, type->tp_bases);

    PyType_Modified(type);

    {int  ReplaceReturn281 = 0; Din_Go(1490,2048); return ReplaceReturn281;}
}


/* Calculate the best base amongst multiple base classes.
   This is the first one that's on the path to the "solid base". */

static PyTypeObject *
best_base(PyObject *bases)
{
    Din_Go(1491,2048);Py_ssize_t i, n;
    PyTypeObject *base, *winner, *candidate, *base_i;
    PyObject *base_proto;

    assert(PyTuple_Check(bases));
    n = PyTuple_GET_SIZE(bases);
    assert(n > 0);
    base = NULL;
    winner = NULL;
    Din_Go(1509,2048);for (i = 0; i < n; i++) {
        Din_Go(1492,2048);base_proto = PyTuple_GET_ITEM(bases, i);
        Din_Go(1494,2048);if (PyClass_Check(base_proto))
            {/*83*/Din_Go(1493,2048);continue;/*84*/}
        Din_Go(1497,2048);if (!PyType_Check(base_proto)) {
            Din_Go(1495,2048);PyErr_SetString(
                PyExc_TypeError,
                "bases must be types");
            {PyTypeObject * ReplaceReturn280 = NULL; Din_Go(1496,2048); return ReplaceReturn280;}
        }
        Din_Go(1498,2048);base_i = (PyTypeObject *)base_proto;
        Din_Go(1500,2048);if (base_i->tp_dict == NULL) {
            Din_Go(1499,2048);if (PyType_Ready(base_i) < 0)
                return NULL;
        }
        Din_Go(1501,2048);candidate = solid_base(base_i);
        Din_Go(1508,2048);if (winner == NULL) {
            Din_Go(1502,2048);winner = candidate;
            base = base_i;
        }
        else {/*85*/Din_Go(1503,2048);if (PyType_IsSubtype(winner, candidate))
            ;
        else {/*89*/Din_Go(1504,2048);if (PyType_IsSubtype(candidate, winner)) {
            Din_Go(1505,2048);winner = candidate;
            base = base_i;
        }
        else {
            Din_Go(1506,2048);PyErr_SetString(
                PyExc_TypeError,
                "multiple bases have "
                "instance lay-out conflict");
            {PyTypeObject * ReplaceReturn279 = NULL; Din_Go(1507,2048); return ReplaceReturn279;}
        ;/*90*/}/*86*/}}
    }
    Din_Go(1511,2048);if (base == NULL)
        {/*91*/Din_Go(1510,2048);PyErr_SetString(PyExc_TypeError,
            "a new-style class can't have only classic bases");/*92*/}
    {PyTypeObject * ReplaceReturn278 = base; Din_Go(1512,2048); return ReplaceReturn278;}
}

static int
extra_ivars(PyTypeObject *type, PyTypeObject *base)
{
    Din_Go(1513,2048);size_t t_size = type->tp_basicsize;
    size_t b_size = base->tp_basicsize;

    assert(t_size >= b_size); /* Else type smaller than base! */
    Din_Go(1515,2048);if (type->tp_itemsize || base->tp_itemsize) {
        /* If itemsize is involved, stricter rules */
        {int  ReplaceReturn277 = t_size != b_size ||
            type->tp_itemsize != base->tp_itemsize; Din_Go(1514,2048); return ReplaceReturn277;}
    }
    Din_Go(1517,2048);if (type->tp_weaklistoffset && base->tp_weaklistoffset == 0 &&
        type->tp_weaklistoffset + sizeof(PyObject *) == t_size &&
        type->tp_flags & Py_TPFLAGS_HEAPTYPE)
        {/*217*/Din_Go(1516,2048);t_size -= sizeof(PyObject *);/*218*/}
    Din_Go(1519,2048);if (type->tp_dictoffset && base->tp_dictoffset == 0 &&
        type->tp_dictoffset + sizeof(PyObject *) == t_size &&
        type->tp_flags & Py_TPFLAGS_HEAPTYPE)
        {/*219*/Din_Go(1518,2048);t_size -= sizeof(PyObject *);/*220*/}

    {int  ReplaceReturn276 = t_size != b_size; Din_Go(1520,2048); return ReplaceReturn276;}
}

static PyTypeObject *
solid_base(PyTypeObject *type)
{
    Din_Go(1521,2048);PyTypeObject *base;

    Din_Go(1524,2048);if (type->tp_base)
        {/*175*/Din_Go(1522,2048);base = solid_base(type->tp_base);/*176*/}
    else
        {/*177*/Din_Go(1523,2048);base = &PyBaseObject_Type;/*178*/}
    Din_Go(1527,2048);if (extra_ivars(type, base))
        {/*179*/{PyTypeObject * ReplaceReturn275 = type; Din_Go(1525,2048); return ReplaceReturn275;}/*180*/}
    else
        {/*181*/{PyTypeObject * ReplaceReturn274 = base; Din_Go(1526,2048); return ReplaceReturn274;}/*182*/}
Din_Go(1528,2048);}

static void object_dealloc(PyObject *);
static int object_init(PyObject *, PyObject *, PyObject *);
static int update_slot(PyTypeObject *, PyObject *);
static void fixup_slot_dispatchers(PyTypeObject *);

/*
 * Helpers for  __dict__ descriptor.  We don't want to expose the dicts
 * inherited from various builtin types.  The builtin base usually provides
 * its own __dict__ descriptor, so we use that when we can.
 */
static PyTypeObject *
get_builtin_base_with_dict(PyTypeObject *type)
{
    Din_Go(1529,2048);while (type->tp_base != NULL) {
        Din_Go(1530,2048);if (type->tp_dictoffset != 0 &&
            !(type->tp_flags & Py_TPFLAGS_HEAPTYPE))
            {/*229*/{PyTypeObject * ReplaceReturn273 = type; Din_Go(1531,2048); return ReplaceReturn273;}/*230*/}
        Din_Go(1532,2048);type = type->tp_base;
    }
    {PyTypeObject * ReplaceReturn272 = NULL; Din_Go(1533,2048); return ReplaceReturn272;}
}

static PyObject *
get_dict_descriptor(PyTypeObject *type)
{
    Din_Go(1534,2048);static PyObject *dict_str;
    PyObject *descr;

    Din_Go(1537,2048);if (dict_str == NULL) {
        Din_Go(1535,2048);dict_str = PyString_InternFromString("__dict__");
        Din_Go(1536,2048);if (dict_str == NULL)
            return NULL;
    }
    Din_Go(1538,2048);descr = _PyType_Lookup(type, dict_str);
    Din_Go(1539,2048);if (descr == NULL || !PyDescr_IsData(descr))
        return NULL;

    {PyObject * ReplaceReturn271 = descr; Din_Go(1540,2048); return ReplaceReturn271;}
}

static void
raise_dict_descr_error(PyObject *obj)
{
    Din_Go(1541,2048);PyErr_Format(PyExc_TypeError,
                 "this __dict__ descriptor does not support "
                 "'%.200s' objects", obj->ob_type->tp_name);
Din_Go(1542,2048);}

static PyObject *
subtype_dict(PyObject *obj, void *context)
{
    Din_Go(1543,2048);PyObject **dictptr;
    PyObject *dict;
    PyTypeObject *base;

    base = get_builtin_base_with_dict(obj->ob_type);
    Din_Go(1553,2048);if (base != NULL) {
        Din_Go(1544,2048);descrgetfunc func;
        PyObject *descr = get_dict_descriptor(base);
        Din_Go(1547,2048);if (descr == NULL) {
            Din_Go(1545,2048);raise_dict_descr_error(obj);
            {PyObject * ReplaceReturn270 = NULL; Din_Go(1546,2048); return ReplaceReturn270;}
        }
        Din_Go(1548,2048);func = descr->ob_type->tp_descr_get;
        Din_Go(1551,2048);if (func == NULL) {
            Din_Go(1549,2048);raise_dict_descr_error(obj);
            {PyObject * ReplaceReturn269 = NULL; Din_Go(1550,2048); return ReplaceReturn269;}
        }
        {PyObject * ReplaceReturn268 = func(descr, obj, (PyObject *)(obj->ob_type)); Din_Go(1552,2048); return ReplaceReturn268;}
    }

    Din_Go(1554,2048);dictptr = _PyObject_GetDictPtr(obj);
    Din_Go(1557,2048);if (dictptr == NULL) {
        Din_Go(1555,2048);PyErr_SetString(PyExc_AttributeError,
                        "This object has no __dict__");
        {PyObject * ReplaceReturn267 = NULL; Din_Go(1556,2048); return ReplaceReturn267;}
    }
    Din_Go(1558,2048);dict = *dictptr;
    Din_Go(1560,2048);if (dict == NULL)
        {/*231*/Din_Go(1559,2048);*dictptr = dict = PyDict_New();/*232*/}
    Py_XINCREF(dict);
    {PyObject * ReplaceReturn266 = dict; Din_Go(1561,2048); return ReplaceReturn266;}
}

static int
subtype_setdict(PyObject *obj, PyObject *value, void *context)
{
    Din_Go(1562,2048);PyObject **dictptr;
    PyObject *dict;
    PyTypeObject *base;

    base = get_builtin_base_with_dict(obj->ob_type);
    Din_Go(1572,2048);if (base != NULL) {
        Din_Go(1563,2048);descrsetfunc func;
        PyObject *descr = get_dict_descriptor(base);
        Din_Go(1566,2048);if (descr == NULL) {
            Din_Go(1564,2048);raise_dict_descr_error(obj);
            {int  ReplaceReturn265 = -1; Din_Go(1565,2048); return ReplaceReturn265;}
        }
        Din_Go(1567,2048);func = descr->ob_type->tp_descr_set;
        Din_Go(1570,2048);if (func == NULL) {
            Din_Go(1568,2048);raise_dict_descr_error(obj);
            {int  ReplaceReturn264 = -1; Din_Go(1569,2048); return ReplaceReturn264;}
        }
        {int  ReplaceReturn263 = func(descr, obj, value); Din_Go(1571,2048); return ReplaceReturn263;}
    }

    Din_Go(1573,2048);dictptr = _PyObject_GetDictPtr(obj);
    Din_Go(1576,2048);if (dictptr == NULL) {
        Din_Go(1574,2048);PyErr_SetString(PyExc_AttributeError,
                        "This object has no __dict__");
        {int  ReplaceReturn262 = -1; Din_Go(1575,2048); return ReplaceReturn262;}
    }
    Din_Go(1579,2048);if (value != NULL && !PyDict_Check(value)) {
        Din_Go(1577,2048);PyErr_Format(PyExc_TypeError,
                     "__dict__ must be set to a dictionary, "
                     "not a '%.200s'", Py_TYPE(value)->tp_name);
        {int  ReplaceReturn261 = -1; Din_Go(1578,2048); return ReplaceReturn261;}
    }
    Din_Go(1580,2048);dict = *dictptr;
    Py_XINCREF(value);
    *dictptr = value;
    Py_XDECREF(dict);
    {int  ReplaceReturn260 = 0; Din_Go(1581,2048); return ReplaceReturn260;}
}

static PyObject *
subtype_getweakref(PyObject *obj, void *context)
{
    Din_Go(1582,2048);PyObject **weaklistptr;
    PyObject *result;

    Din_Go(1585,2048);if (Py_TYPE(obj)->tp_weaklistoffset == 0) {
        Din_Go(1583,2048);PyErr_SetString(PyExc_AttributeError,
                        "This object has no __weakref__");
        {PyObject * ReplaceReturn259 = NULL; Din_Go(1584,2048); return ReplaceReturn259;}
    }
    assert(Py_TYPE(obj)->tp_weaklistoffset > 0);
    assert(Py_TYPE(obj)->tp_weaklistoffset + sizeof(PyObject *) <=
           (size_t)(Py_TYPE(obj)->tp_basicsize));
    Din_Go(1586,2048);weaklistptr = (PyObject **)
        ((char *)obj + Py_TYPE(obj)->tp_weaklistoffset);
    Din_Go(1588,2048);if (*weaklistptr == NULL)
        result = Py_None;
    else
        {/*233*/Din_Go(1587,2048);result = *weaklistptr;/*234*/}
    Py_INCREF(result);
    {PyObject * ReplaceReturn258 = result; Din_Go(1589,2048); return ReplaceReturn258;}
}

/* Three variants on the subtype_getsets list. */

static PyGetSetDef subtype_getsets_full[] = {
    {"__dict__", subtype_dict, subtype_setdict,
     PyDoc_STR("dictionary for instance variables (if defined)")},
    {"__weakref__", subtype_getweakref, NULL,
     PyDoc_STR("list of weak references to the object (if defined)")},
    {0}
};

static PyGetSetDef subtype_getsets_dict_only[] = {
    {"__dict__", subtype_dict, subtype_setdict,
     PyDoc_STR("dictionary for instance variables (if defined)")},
    {0}
};

static PyGetSetDef subtype_getsets_weakref_only[] = {
    {"__weakref__", subtype_getweakref, NULL,
     PyDoc_STR("list of weak references to the object (if defined)")},
    {0}
};

static int
valid_identifier(PyObject *s)
{
    Din_Go(1590,2048);unsigned char *p;
    Py_ssize_t i, n;

    Din_Go(1593,2048);if (!PyString_Check(s)) {
        Din_Go(1591,2048);PyErr_Format(PyExc_TypeError,
                     "__slots__ items must be strings, not '%.200s'",
                     Py_TYPE(s)->tp_name);
        {int  ReplaceReturn257 = 0; Din_Go(1592,2048); return ReplaceReturn257;}
    }
    Din_Go(1594,2048);p = (unsigned char *) PyString_AS_STRING(s);
    n = PyString_GET_SIZE(s);
    /* We must reject an empty name.  As a hack, we bump the
       length to 1 so that the loop will balk on the trailing \0. */
    Din_Go(1596,2048);if (n == 0)
        {/*235*/Din_Go(1595,2048);n = 1;/*236*/}
    Din_Go(1600,2048);for (i = 0; i < n; i++, p++) {
        Din_Go(1597,2048);if (!(i == 0 ? isalpha(*p) : isalnum(*p)) && *p != '_') {
            Din_Go(1598,2048);PyErr_SetString(PyExc_TypeError,
                            "__slots__ must be identifiers");
            {int  ReplaceReturn256 = 0; Din_Go(1599,2048); return ReplaceReturn256;}
        }
    }
    {int  ReplaceReturn255 = 1; Din_Go(1601,2048); return ReplaceReturn255;}
}

#ifdef Py_USING_UNICODE
/* Replace Unicode objects in slots.  */

static PyObject *
_unicode_to_string(PyObject *slots, Py_ssize_t nslots)
{
    Din_Go(1602,2048);PyObject *tmp = NULL;
    PyObject *slot_name, *new_name;
    Py_ssize_t i;

    Din_Go(1610,2048);for (i = 0; i < nslots; i++) {
        Din_Go(1603,2048);if (PyUnicode_Check(slot_name = PyTuple_GET_ITEM(slots, i))) {
            Din_Go(1604,2048);if (tmp == NULL) {
                Din_Go(1605,2048);tmp = PySequence_List(slots);
                Din_Go(1606,2048);if (tmp == NULL)
                    return NULL;
            }
            Din_Go(1607,2048);new_name = _PyUnicode_AsDefaultEncodedString(slot_name,
                                                         NULL);
            Din_Go(1609,2048);if (new_name == NULL) {
                Py_DECREF(tmp);
                {PyObject * ReplaceReturn254 = NULL; Din_Go(1608,2048); return ReplaceReturn254;}
            }
            Py_INCREF(new_name);
            PyList_SET_ITEM(tmp, i, new_name);
            Py_DECREF(slot_name);
        }
    }
    Din_Go(1612,2048);if (tmp != NULL) {
        Din_Go(1611,2048);slots = PyList_AsTuple(tmp);
        Py_DECREF(tmp);
    }
    {PyObject * ReplaceReturn253 = slots; Din_Go(1613,2048); return ReplaceReturn253;}
}
#endif

/* Forward */
static int
object_init(PyObject *self, PyObject *args, PyObject *kwds);

static int
type_init(PyObject *cls, PyObject *args, PyObject *kwds)
{
    Din_Go(1614,2048);int res;

    assert(args != NULL && PyTuple_Check(args));
    assert(kwds == NULL || PyDict_Check(kwds));

    Din_Go(1617,2048);if (kwds != NULL && PyDict_Check(kwds) && PyDict_Size(kwds) != 0) {
        Din_Go(1615,2048);PyErr_SetString(PyExc_TypeError,
                        "type.__init__() takes no keyword arguments");
        {int  ReplaceReturn252 = -1; Din_Go(1616,2048); return ReplaceReturn252;}
    }

    Din_Go(1620,2048);if (args != NULL && PyTuple_Check(args) &&
        (PyTuple_GET_SIZE(args) != 1 && PyTuple_GET_SIZE(args) != 3)) {
        Din_Go(1618,2048);PyErr_SetString(PyExc_TypeError,
                        "type.__init__() takes 1 or 3 arguments");
        {int  ReplaceReturn251 = -1; Din_Go(1619,2048); return ReplaceReturn251;}
    }

    /* Call object.__init__(self) now. */
    /* XXX Could call super(type, cls).__init__() but what's the point? */
    Din_Go(1621,2048);args = PyTuple_GetSlice(args, 0, 0);
    res = object_init(cls, args, NULL);
    Py_DECREF(args);
    {int  ReplaceReturn250 = res; Din_Go(1622,2048); return ReplaceReturn250;}
}

static PyObject *
type_new(PyTypeObject *metatype, PyObject *args, PyObject *kwds)
{
    Din_Go(1623,2048);PyObject *name, *bases, *dict;
    static char *kwlist[] = {"name", "bases", "dict", 0};
    PyObject *slots, *tmp, *newslots;
    PyTypeObject *type, *base, *tmptype, *winner;
    PyHeapTypeObject *et;
    PyMemberDef *mp;
    Py_ssize_t i, nbases, nslots, slotoffset, add_dict, add_weak;
    int j, may_add_dict, may_add_weak;

    assert(args != NULL && PyTuple_Check(args));
    assert(kwds == NULL || PyDict_Check(kwds));

    /* Special case: type(x) should return x->ob_type */
    {
        const Py_ssize_t nargs = PyTuple_GET_SIZE(args);
        const Py_ssize_t nkwds = kwds == NULL ? 0 : PyDict_Size(kwds);

        Din_Go(1626,2048);if (PyType_CheckExact(metatype) && nargs == 1 && nkwds == 0) {
            Din_Go(1624,2048);PyObject *x = PyTuple_GET_ITEM(args, 0);
            Py_INCREF(Py_TYPE(x));
            {PyObject * ReplaceReturn249 = (PyObject *) Py_TYPE(x); Din_Go(1625,2048); return ReplaceReturn249;}
        }

        /* SF bug 475327 -- if that didn't trigger, we need 3
           arguments. but PyArg_ParseTupleAndKeywords below may give
           a msg saying type() needs exactly 3. */
        Din_Go(1629,2048);if (nargs + nkwds != 3) {
            Din_Go(1627,2048);PyErr_SetString(PyExc_TypeError,
                            "type() takes 1 or 3 arguments");
            {PyObject * ReplaceReturn248 = NULL; Din_Go(1628,2048); return ReplaceReturn248;}
        }
    }

    /* Check arguments: (name, bases, dict) */
    Din_Go(1630,2048);if (!PyArg_ParseTupleAndKeywords(args, kwds, "SO!O!:type", kwlist,
                                     &name,
                                     &PyTuple_Type, &bases,
                                     &PyDict_Type, &dict))
        return NULL;

    /* Determine the proper metatype to deal with this,
       and check for metatype conflicts while we're at it.
       Note that if some other metatype wins to contract,
       it's possible that its instances are not types. */
    Din_Go(1631,2048);nbases = PyTuple_GET_SIZE(bases);
    winner = metatype;
    Din_Go(1642,2048);for (i = 0; i < nbases; i++) {
        Din_Go(1632,2048);tmp = PyTuple_GET_ITEM(bases, i);
        tmptype = tmp->ob_type;
        Din_Go(1634,2048);if (tmptype == &PyClass_Type)
            {/*237*/Din_Go(1633,2048);continue;/*238*/} /* Special case classic classes */
        Din_Go(1636,2048);if (PyType_IsSubtype(winner, tmptype))
            {/*239*/Din_Go(1635,2048);continue;/*240*/}
        Din_Go(1639,2048);if (PyType_IsSubtype(tmptype, winner)) {
            Din_Go(1637,2048);winner = tmptype;
            Din_Go(1638,2048);continue;
        }
        Din_Go(1640,2048);PyErr_SetString(PyExc_TypeError,
                        "metaclass conflict: "
                        "the metaclass of a derived class "
                        "must be a (non-strict) subclass "
                        "of the metaclasses of all its bases");
        {PyObject * ReplaceReturn247 = NULL; Din_Go(1641,2048); return ReplaceReturn247;}
    }
    Din_Go(1646,2048);if (winner != metatype) {
        Din_Go(1643,2048);if (winner->tp_new != type_new) /* Pass it to the winner */
            {/*241*/{PyObject * ReplaceReturn246 = winner->tp_new(winner, args, kwds); Din_Go(1644,2048); return ReplaceReturn246;}/*242*/}
        Din_Go(1645,2048);metatype = winner;
    }

    /* Adjust for empty tuple bases */
    Din_Go(1650,2048);if (nbases == 0) {
        Din_Go(1647,2048);bases = PyTuple_Pack(1, &PyBaseObject_Type);
        Din_Go(1648,2048);if (bases == NULL)
            return NULL;
        Din_Go(1649,2048);nbases = 1;
    }
    else
        Py_INCREF(bases);

    /* XXX From here until type is allocated, "return NULL" leaks bases! */

    /* Calculate best base, and check that all bases are type objects */
    Din_Go(1651,2048);base = best_base(bases);
    Din_Go(1653,2048);if (base == NULL) {
        Py_DECREF(bases);
        {PyObject * ReplaceReturn245 = NULL; Din_Go(1652,2048); return ReplaceReturn245;}
    }
    Din_Go(1656,2048);if (!PyType_HasFeature(base, Py_TPFLAGS_BASETYPE)) {
        Din_Go(1654,2048);PyErr_Format(PyExc_TypeError,
                     "type '%.100s' is not an acceptable base type",
                     base->tp_name);
        Py_DECREF(bases);
        {PyObject * ReplaceReturn244 = NULL; Din_Go(1655,2048); return ReplaceReturn244;}
    }

    /* Check for a __slots__ sequence variable in dict, and count it */
    Din_Go(1657,2048);slots = PyDict_GetItemString(dict, "__slots__");
    nslots = 0;
    add_dict = 0;
    add_weak = 0;
    may_add_dict = base->tp_dictoffset == 0;
    may_add_weak = base->tp_weaklistoffset == 0 && base->tp_itemsize == 0;
    Din_Go(1729,2048);if (slots == NULL) {
        Din_Go(1658,2048);if (may_add_dict) {
            Din_Go(1659,2048);add_dict++;
        }
        Din_Go(1661,2048);if (may_add_weak) {
            Din_Go(1660,2048);add_weak++;
        }
    }
    else {
        /* Have slots */

        /* Make it into a tuple */
        Din_Go(1662,2048);if (PyString_Check(slots) || PyUnicode_Check(slots))
            {/*243*/Din_Go(1663,2048);slots = PyTuple_Pack(1, slots);/*244*/}
        else
            {/*245*/Din_Go(1664,2048);slots = PySequence_Tuple(slots);/*246*/}
        Din_Go(1666,2048);if (slots == NULL) {
            Py_DECREF(bases);
            {PyObject * ReplaceReturn243 = NULL; Din_Go(1665,2048); return ReplaceReturn243;}
        }
        assert(PyTuple_Check(slots));

        /* Are slots allowed? */
        Din_Go(1667,2048);nslots = PyTuple_GET_SIZE(slots);
        Din_Go(1670,2048);if (nslots > 0 && base->tp_itemsize != 0) {
            Din_Go(1668,2048);PyErr_Format(PyExc_TypeError,
                         "nonempty __slots__ "
                         "not supported for subtype of '%s'",
                         base->tp_name);
          bad_slots:
            Py_DECREF(bases);
            Py_DECREF(slots);
            {PyObject * ReplaceReturn242 = NULL; Din_Go(1669,2048); return ReplaceReturn242;}
        }

#ifdef Py_USING_UNICODE
        Din_Go(1671,2048);tmp = _unicode_to_string(slots, nslots);
        Din_Go(1673,2048);if (tmp == NULL)
            {/*247*/Din_Go(1672,2048);goto bad_slots;/*248*/}
        Din_Go(1675,2048);if (tmp != slots) {
            Py_DECREF(slots);
            Din_Go(1674,2048);slots = tmp;
        }
#endif
        /* Check for valid slot names and two special cases */
        Din_Go(1690,2048);for (i = 0; i < nslots; i++) {
            Din_Go(1676,2048);PyObject *tmp = PyTuple_GET_ITEM(slots, i);
            char *s;
            Din_Go(1678,2048);if (!valid_identifier(tmp))
                {/*249*/Din_Go(1677,2048);goto bad_slots;/*250*/}
            assert(PyString_Check(tmp));
            Din_Go(1679,2048);s = PyString_AS_STRING(tmp);
            Din_Go(1684,2048);if (strcmp(s, "__dict__") == 0) {
                Din_Go(1680,2048);if (!may_add_dict || add_dict) {
                    Din_Go(1681,2048);PyErr_SetString(PyExc_TypeError,
                        "__dict__ slot disallowed: "
                        "we already got one");
                    Din_Go(1682,2048);goto bad_slots;
                }
                Din_Go(1683,2048);add_dict++;
            }
            Din_Go(1689,2048);if (strcmp(s, "__weakref__") == 0) {
                Din_Go(1685,2048);if (!may_add_weak || add_weak) {
                    Din_Go(1686,2048);PyErr_SetString(PyExc_TypeError,
                        "__weakref__ slot disallowed: "
                        "either we already got one, "
                        "or __itemsize__ != 0");
                    Din_Go(1687,2048);goto bad_slots;
                }
                Din_Go(1688,2048);add_weak++;
            }
        }

        /* Copy slots into a list, mangle names and sort them.
           Sorted names are needed for __class__ assignment.
           Convert them back to tuple at the end.
        */
        Din_Go(1691,2048);newslots = PyList_New(nslots - add_dict - add_weak);
        Din_Go(1693,2048);if (newslots == NULL)
            {/*251*/Din_Go(1692,2048);goto bad_slots;/*252*/}
        Din_Go(1701,2048);for (i = j = 0; i < nslots; i++) {
            Din_Go(1694,2048);char *s;
            tmp = PyTuple_GET_ITEM(slots, i);
            s = PyString_AS_STRING(tmp);
            Din_Go(1696,2048);if ((add_dict && strcmp(s, "__dict__") == 0) ||
                (add_weak && strcmp(s, "__weakref__") == 0))
                {/*253*/Din_Go(1695,2048);continue;/*254*/}
            Din_Go(1697,2048);tmp =_Py_Mangle(name, tmp);
            Din_Go(1699,2048);if (!tmp) {
                Py_DECREF(newslots);
                Din_Go(1698,2048);goto bad_slots;
            }
            PyList_SET_ITEM(newslots, j, tmp);
            Din_Go(1700,2048);j++;
        }
        assert(j == nslots - add_dict - add_weak);
        Din_Go(1702,2048);nslots = j;
        Py_DECREF(slots);
        Din_Go(1704,2048);if (PyList_Sort(newslots) == -1) {
            Py_DECREF(bases);
            Py_DECREF(newslots);
            {PyObject * ReplaceReturn241 = NULL; Din_Go(1703,2048); return ReplaceReturn241;}
        }
        Din_Go(1705,2048);slots = PyList_AsTuple(newslots);
        Py_DECREF(newslots);
        Din_Go(1707,2048);if (slots == NULL) {
            Py_DECREF(bases);
            {PyObject * ReplaceReturn240 = NULL; Din_Go(1706,2048); return ReplaceReturn240;}
        }

        /* Secondary bases may provide weakrefs or dict */
        Din_Go(1728,2048);if (nbases > 1 &&
            ((may_add_dict && !add_dict) ||
             (may_add_weak && !add_weak))) {
            Din_Go(1708,2048);for (i = 0; i < nbases; i++) {
                Din_Go(1709,2048);tmp = PyTuple_GET_ITEM(bases, i);
                Din_Go(1711,2048);if (tmp == (PyObject *)base)
                    {/*255*/Din_Go(1710,2048);continue;/*256*/} /* Skip primary base */
                Din_Go(1717,2048);if (PyClass_Check(tmp)) {
                    /* Classic base class provides both */
                    Din_Go(1712,2048);if (may_add_dict && !add_dict)
                        {/*257*/Din_Go(1713,2048);add_dict++;/*258*/}
                    Din_Go(1715,2048);if (may_add_weak && !add_weak)
                        {/*259*/Din_Go(1714,2048);add_weak++;/*260*/}
                    Din_Go(1716,2048);break;
                }
                assert(PyType_Check(tmp));
                Din_Go(1718,2048);tmptype = (PyTypeObject *)tmp;
                Din_Go(1720,2048);if (may_add_dict && !add_dict &&
                    tmptype->tp_dictoffset != 0)
                    {/*261*/Din_Go(1719,2048);add_dict++;/*262*/}
                Din_Go(1722,2048);if (may_add_weak && !add_weak &&
                    tmptype->tp_weaklistoffset != 0)
                    {/*263*/Din_Go(1721,2048);add_weak++;/*264*/}
                Din_Go(1724,2048);if (may_add_dict && !add_dict)
                    {/*265*/Din_Go(1723,2048);continue;/*266*/}
                Din_Go(1726,2048);if (may_add_weak && !add_weak)
                    {/*267*/Din_Go(1725,2048);continue;/*268*/}
                /* Nothing more to check */
                Din_Go(1727,2048);break;
            }
        }
    }

    /* XXX From here until type is safely allocated,
       "return NULL" may leak slots! */

    /* Allocate the type object */
    Din_Go(1730,2048);type = (PyTypeObject *)metatype->tp_alloc(metatype, nslots);
    Din_Go(1732,2048);if (type == NULL) {
        Py_XDECREF(slots);
        Py_DECREF(bases);
        {PyObject * ReplaceReturn239 = NULL; Din_Go(1731,2048); return ReplaceReturn239;}
    }

    /* Keep name and slots alive in the extended type object */
    Din_Go(1733,2048);et = (PyHeapTypeObject *)type;
    Py_INCREF(name);
    et->ht_name = name;
    et->ht_slots = slots;

    /* Initialize tp_flags */
    type->tp_flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_HEAPTYPE |
        Py_TPFLAGS_BASETYPE;
    Din_Go(1734,2048);if (base->tp_flags & Py_TPFLAGS_HAVE_GC)
        type->tp_flags |= Py_TPFLAGS_HAVE_GC;
    Din_Go(1735,2048);if (base->tp_flags & Py_TPFLAGS_HAVE_NEWBUFFER)
        type->tp_flags |= Py_TPFLAGS_HAVE_NEWBUFFER;

    /* It's a new-style number unless it specifically inherits any
       old-style numeric behavior */
    Din_Go(1736,2048);if ((base->tp_flags & Py_TPFLAGS_CHECKTYPES) ||
        (base->tp_as_number == NULL))
        type->tp_flags |= Py_TPFLAGS_CHECKTYPES;

    /* Initialize essential fields */
    Din_Go(1737,2048);type->tp_as_number = &et->as_number;
    type->tp_as_sequence = &et->as_sequence;
    type->tp_as_mapping = &et->as_mapping;
    type->tp_as_buffer = &et->as_buffer;
    type->tp_name = PyString_AS_STRING(name);

    /* Set tp_base and tp_bases */
    type->tp_bases = bases;
    Py_INCREF(base);
    type->tp_base = base;

    /* Initialize tp_dict from passed-in dict */
    type->tp_dict = dict = PyDict_Copy(dict);
    Din_Go(1739,2048);if (dict == NULL) {
        Py_DECREF(type);
        {PyObject * ReplaceReturn238 = NULL; Din_Go(1738,2048); return ReplaceReturn238;}
    }

    /* Set __module__ in the dict */
    Din_Go(1745,2048);if (PyDict_GetItemString(dict, "__module__") == NULL) {
        Din_Go(1740,2048);tmp = PyEval_GetGlobals();
        Din_Go(1744,2048);if (tmp != NULL) {
            Din_Go(1741,2048);tmp = PyDict_GetItemString(tmp, "__name__");
            Din_Go(1743,2048);if (tmp != NULL) {
                Din_Go(1742,2048);if (PyDict_SetItemString(dict, "__module__",
                                         tmp) < 0)
                    return NULL;
            }
        }
    }

    /* Set tp_doc to a copy of dict['__doc__'], if the latter is there
       and is a string.  The __doc__ accessor will first look for tp_doc;
       if that fails, it will still look into __dict__.
    */
    {
        Din_Go(1746,2048);PyObject *doc = PyDict_GetItemString(dict, "__doc__");
        Din_Go(1751,2048);if (doc != NULL && PyString_Check(doc)) {
            Din_Go(1747,2048);const size_t n = (size_t)PyString_GET_SIZE(doc);
            char *tp_doc = (char *)PyObject_MALLOC(n+1);
            Din_Go(1749,2048);if (tp_doc == NULL) {
                Py_DECREF(type);
                {PyObject * ReplaceReturn237 = NULL; Din_Go(1748,2048); return ReplaceReturn237;}
            }
            Din_Go(1750,2048);memcpy(tp_doc, PyString_AS_STRING(doc), n+1);
            type->tp_doc = tp_doc;
        }
    }

    /* Special-case __new__: if it's a plain function,
       make it a static function */
    Din_Go(1752,2048);tmp = PyDict_GetItemString(dict, "__new__");
    Din_Go(1757,2048);if (tmp != NULL && PyFunction_Check(tmp)) {
        Din_Go(1753,2048);tmp = PyStaticMethod_New(tmp);
        Din_Go(1755,2048);if (tmp == NULL) {
            Py_DECREF(type);
            {PyObject * ReplaceReturn236 = NULL; Din_Go(1754,2048); return ReplaceReturn236;}
        }
        Din_Go(1756,2048);PyDict_SetItemString(dict, "__new__", tmp);
        Py_DECREF(tmp);
    }

    /* Add descriptors for custom slots from __slots__, or for __dict__ */
    Din_Go(1758,2048);mp = PyHeapType_GET_MEMBERS(et);
    slotoffset = base->tp_basicsize;
    Din_Go(1761,2048);if (slots != NULL) {
        Din_Go(1759,2048);for (i = 0; i < nslots; i++, mp++) {
            Din_Go(1760,2048);mp->name = PyString_AS_STRING(
                PyTuple_GET_ITEM(slots, i));
            mp->type = T_OBJECT_EX;
            mp->offset = slotoffset;

            /* __dict__ and __weakref__ are already filtered out */
            assert(strcmp(mp->name, "__dict__") != 0);
            assert(strcmp(mp->name, "__weakref__") != 0);

            slotoffset += sizeof(PyObject *);
        }
    }
    Din_Go(1766,2048);if (add_dict) {
        Din_Go(1762,2048);if (base->tp_itemsize)
            {/*269*/Din_Go(1763,2048);type->tp_dictoffset = -(long)sizeof(PyObject *);/*270*/}
        else
            {/*271*/Din_Go(1764,2048);type->tp_dictoffset = slotoffset;/*272*/}
        Din_Go(1765,2048);slotoffset += sizeof(PyObject *);
    }
    Din_Go(1768,2048);if (add_weak) {
        assert(!base->tp_itemsize);
        Din_Go(1767,2048);type->tp_weaklistoffset = slotoffset;
        slotoffset += sizeof(PyObject *);
    }
    Din_Go(1769,2048);type->tp_basicsize = slotoffset;
    type->tp_itemsize = base->tp_itemsize;
    type->tp_members = PyHeapType_GET_MEMBERS(et);

    Din_Go(1771,2048);if (type->tp_weaklistoffset && type->tp_dictoffset)
        {/*273*/Din_Go(1770,2048);type->tp_getset = subtype_getsets_full;/*274*/}
    else if (type->tp_weaklistoffset && !type->tp_dictoffset)
        type->tp_getset = subtype_getsets_weakref_only;
    else if (!type->tp_weaklistoffset && type->tp_dictoffset)
        type->tp_getset = subtype_getsets_dict_only;
    else
        type->tp_getset = NULL;

    /* Special case some slots */
    Din_Go(1776,2048);if (type->tp_dictoffset != 0 || nslots > 0) {
        Din_Go(1772,2048);if (base->tp_getattr == NULL && base->tp_getattro == NULL)
            {/*275*/Din_Go(1773,2048);type->tp_getattro = PyObject_GenericGetAttr;/*276*/}
        Din_Go(1775,2048);if (base->tp_setattr == NULL && base->tp_setattro == NULL)
            {/*277*/Din_Go(1774,2048);type->tp_setattro = PyObject_GenericSetAttr;/*278*/}
    }
    Din_Go(1777,2048);type->tp_dealloc = subtype_dealloc;

    /* Enable GC unless there are really no instance variables possible */
    Din_Go(1778,2048);if (!(type->tp_basicsize == sizeof(PyObject) &&
          type->tp_itemsize == 0))
        type->tp_flags |= Py_TPFLAGS_HAVE_GC;

    /* Always override allocation strategy to use regular heap */
    Din_Go(1779,2048);type->tp_alloc = PyType_GenericAlloc;
    Din_Go(1781,2048);if (type->tp_flags & Py_TPFLAGS_HAVE_GC) {
        Din_Go(1780,2048);type->tp_free = PyObject_GC_Del;
        type->tp_traverse = subtype_traverse;
        type->tp_clear = subtype_clear;
    }
    else
        type->tp_free = PyObject_Del;

    /* Initialize the rest */
    Din_Go(1783,2048);if (PyType_Ready(type) < 0) {
        Py_DECREF(type);
        {PyObject * ReplaceReturn235 = NULL; Din_Go(1782,2048); return ReplaceReturn235;}
    }

    /* Put the proper slots in place */
    Din_Go(1784,2048);fixup_slot_dispatchers(type);

    {PyObject * ReplaceReturn234 = (PyObject *)type; Din_Go(1785,2048); return ReplaceReturn234;}
}

/* Internal API to look for a name through the MRO.
   This returns a borrowed reference, and doesn't set an exception! */
PyObject *
_PyType_Lookup(PyTypeObject *type, PyObject *name)
{
    Din_Go(1786,2048);Py_ssize_t i, n;
    PyObject *mro, *res, *base, *dict;
    unsigned int h;

    Din_Go(1790,2048);if (MCACHE_CACHEABLE_NAME(name) &&
        PyType_HasFeature(type, Py_TPFLAGS_VALID_VERSION_TAG)) {
        /* fast path */
        Din_Go(1787,2048);h = MCACHE_HASH_METHOD(type, name);
        Din_Go(1789,2048);if (method_cache[h].version == type->tp_version_tag &&
            method_cache[h].name == name)
            {/*47*/{PyObject * ReplaceReturn233 = method_cache[h].value; Din_Go(1788,2048); return ReplaceReturn233;}/*48*/}
    }

    /* Look in tp_dict of types in MRO */
    Din_Go(1791,2048);mro = type->tp_mro;

    /* If mro is NULL, the type is either not yet initialized
       by PyType_Ready(), or already cleared by type_clear().
       Either way the safest thing to do is to return NULL. */
    Din_Go(1792,2048);if (mro == NULL)
        return NULL;

    Din_Go(1793,2048);res = NULL;
    assert(PyTuple_Check(mro));
    n = PyTuple_GET_SIZE(mro);
    Din_Go(1801,2048);for (i = 0; i < n; i++) {
        Din_Go(1794,2048);base = PyTuple_GET_ITEM(mro, i);
        Din_Go(1797,2048);if (PyClass_Check(base))
            {/*49*/Din_Go(1795,2048);dict = ((PyClassObject *)base)->cl_dict;/*50*/}
        else {
            assert(PyType_Check(base));
            Din_Go(1796,2048);dict = ((PyTypeObject *)base)->tp_dict;
        }
        assert(dict && PyDict_Check(dict));
        Din_Go(1798,2048);res = PyDict_GetItem(dict, name);
        Din_Go(1800,2048);if (res != NULL)
            {/*51*/Din_Go(1799,2048);break;/*52*/}
    }

    Din_Go(1803,2048);if (MCACHE_CACHEABLE_NAME(name) && assign_version_tag(type)) {
        Din_Go(1802,2048);h = MCACHE_HASH_METHOD(type, name);
        method_cache[h].version = type->tp_version_tag;
        method_cache[h].value = res;  /* borrowed */
        Py_INCREF(name);
        Py_DECREF(method_cache[h].name);
        method_cache[h].name = name;
    }
    {PyObject * ReplaceReturn232 = res; Din_Go(1804,2048); return ReplaceReturn232;}
}

/* This is similar to PyObject_GenericGetAttr(),
   but uses _PyType_Lookup() instead of just looking in type->tp_dict. */
static PyObject *
type_getattro(PyTypeObject *type, PyObject *name)
{
    Din_Go(1805,2048);PyTypeObject *metatype = Py_TYPE(type);
    PyObject *meta_attribute, *attribute;
    descrgetfunc meta_get;

    Din_Go(1808,2048);if (!PyString_Check(name)) {
        Din_Go(1806,2048);PyErr_Format(PyExc_TypeError,
                     "attribute name must be string, not '%.200s'",
                     name->ob_type->tp_name);
        {PyObject * ReplaceReturn231 = NULL; Din_Go(1807,2048); return ReplaceReturn231;}
    }

    /* Initialize this type (we'll assume the metatype is initialized) */
    Din_Go(1810,2048);if (type->tp_dict == NULL) {
        Din_Go(1809,2048);if (PyType_Ready(type) < 0)
            return NULL;
    }

    /* No readable descriptor found yet */
    Din_Go(1811,2048);meta_get = NULL;

    /* Look for the attribute in the metatype */
    meta_attribute = _PyType_Lookup(metatype, name);

    Din_Go(1815,2048);if (meta_attribute != NULL) {
        Din_Go(1812,2048);meta_get = Py_TYPE(meta_attribute)->tp_descr_get;

        Din_Go(1814,2048);if (meta_get != NULL && PyDescr_IsData(meta_attribute)) {
            /* Data descriptors implement tp_descr_set to intercept
             * writes. Assume the attribute is not overridden in
             * type's tp_dict (and bases): call the descriptor now.
             */
            {PyObject * ReplaceReturn230 = meta_get(meta_attribute, (PyObject *)type,
                            (PyObject *)metatype); Din_Go(1813,2048); return ReplaceReturn230;}
        }
        Py_INCREF(meta_attribute);
    }

    /* No data descriptor found on metatype. Look in tp_dict of this
     * type and its bases */
    Din_Go(1816,2048);attribute = _PyType_Lookup(type, name);
    Din_Go(1821,2048);if (attribute != NULL) {
        /* Implement descriptor functionality, if any */
        Din_Go(1817,2048);descrgetfunc local_get = Py_TYPE(attribute)->tp_descr_get;

        Py_XDECREF(meta_attribute);

        Din_Go(1819,2048);if (local_get != NULL) {
            /* NULL 2nd argument indicates the descriptor was
             * found on the target object itself (or a base)  */
            {PyObject * ReplaceReturn229 = local_get(attribute, (PyObject *)NULL,
                             (PyObject *)type); Din_Go(1818,2048); return ReplaceReturn229;}
        }

        Py_INCREF(attribute);
        {PyObject * ReplaceReturn228 = attribute; Din_Go(1820,2048); return ReplaceReturn228;}
    }

    /* No attribute found in local __dict__ (or bases): use the
     * descriptor from the metatype, if any */
    Din_Go(1824,2048);if (meta_get != NULL) {
        Din_Go(1822,2048);PyObject *res;
        res = meta_get(meta_attribute, (PyObject *)type,
                       (PyObject *)metatype);
        Py_DECREF(meta_attribute);
        {PyObject * ReplaceReturn227 = res; Din_Go(1823,2048); return ReplaceReturn227;}
    }

    /* If an ordinary attribute was found on the metatype, return it now */
    Din_Go(1826,2048);if (meta_attribute != NULL) {
        {PyObject * ReplaceReturn226 = meta_attribute; Din_Go(1825,2048); return ReplaceReturn226;}
    }

    /* Give up */
    Din_Go(1827,2048);PyErr_Format(PyExc_AttributeError,
                     "type object '%.50s' has no attribute '%.400s'",
                     type->tp_name, PyString_AS_STRING(name));
    {PyObject * ReplaceReturn225 = NULL; Din_Go(1828,2048); return ReplaceReturn225;}
}

static int
type_setattro(PyTypeObject *type, PyObject *name, PyObject *value)
{
    Din_Go(1829,2048);if (!(type->tp_flags & Py_TPFLAGS_HEAPTYPE)) {
        Din_Go(1830,2048);PyErr_Format(
            PyExc_TypeError,
            "can't set attributes of built-in/extension type '%s'",
            type->tp_name);
        {int  ReplaceReturn224 = -1; Din_Go(1831,2048); return ReplaceReturn224;}
    }
    Din_Go(1833,2048);if (PyObject_GenericSetAttr((PyObject *)type, name, value) < 0)
        {/*279*/{int  ReplaceReturn223 = -1; Din_Go(1832,2048); return ReplaceReturn223;}/*280*/}
    {int  ReplaceReturn222 = update_slot(type, name); Din_Go(1834,2048); return ReplaceReturn222;}
}

static void
type_dealloc(PyTypeObject *type)
{
    Din_Go(1835,2048);PyHeapTypeObject *et;

    /* Assert this is a heap-allocated type object */
    assert(type->tp_flags & Py_TPFLAGS_HEAPTYPE);
    _PyObject_GC_UNTRACK(type);
    PyObject_ClearWeakRefs((PyObject *)type);
    et = (PyHeapTypeObject *)type;
    Py_XDECREF(type->tp_base);
    Py_XDECREF(type->tp_dict);
    Py_XDECREF(type->tp_bases);
    Py_XDECREF(type->tp_mro);
    Py_XDECREF(type->tp_cache);
    Py_XDECREF(type->tp_subclasses);
    /* A type's tp_doc is heap allocated, unlike the tp_doc slots
     * of most other objects.  It's okay to cast it to char *.
     */
    PyObject_Free((char *)type->tp_doc);
    Py_XDECREF(et->ht_name);
    Py_XDECREF(et->ht_slots);
    Py_TYPE(type)->tp_free((PyObject *)type);
}

static PyObject *
type_subclasses(PyTypeObject *type, PyObject *args_ignored)
{
    Din_Go(1836,2048);PyObject *list, *raw, *ref;
    Py_ssize_t i, n;

    list = PyList_New(0);
    Din_Go(1837,2048);if (list == NULL)
        return NULL;
    Din_Go(1838,2048);raw = type->tp_subclasses;
    Din_Go(1840,2048);if (raw == NULL)
        {/*281*/{PyObject * ReplaceReturn221 = list; Din_Go(1839,2048); return ReplaceReturn221;}/*282*/}
    assert(PyList_Check(raw));
    Din_Go(1841,2048);n = PyList_GET_SIZE(raw);
    Din_Go(1846,2048);for (i = 0; i < n; i++) {
        Din_Go(1842,2048);ref = PyList_GET_ITEM(raw, i);
        assert(PyWeakref_CheckRef(ref));
        ref = PyWeakref_GET_OBJECT(ref);
        Din_Go(1845,2048);if (ref != Py_None) {
            Din_Go(1843,2048);if (PyList_Append(list, ref) < 0) {
                Py_DECREF(list);
                {PyObject * ReplaceReturn220 = NULL; Din_Go(1844,2048); return ReplaceReturn220;}
            }
        }
    }
    {PyObject * ReplaceReturn219 = list; Din_Go(1847,2048); return ReplaceReturn219;}
}

static PyMethodDef type_methods[] = {
    {"mro", (PyCFunction)mro_external, METH_NOARGS,
     PyDoc_STR("mro() -> list\nreturn a type's method resolution order")},
    {"__subclasses__", (PyCFunction)type_subclasses, METH_NOARGS,
     PyDoc_STR("__subclasses__() -> list of immediate subclasses")},
    {"__instancecheck__", type___instancecheck__, METH_O,
     PyDoc_STR("__instancecheck__() -> bool\ncheck if an object is an instance")},
    {"__subclasscheck__", type___subclasscheck__, METH_O,
     PyDoc_STR("__subclasscheck__() -> bool\ncheck if a class is a subclass")},
    {0}
};

PyDoc_STRVAR(type_doc,
"type(object) -> the object's type\n"
"type(name, bases, dict) -> a new type");

static int
type_traverse(PyTypeObject *type, visitproc visit, void *arg)
{
Din_Go(1848,2048);    /* Because of type_is_gc(), the collector only calls this
       for heaptypes. */
    assert(type->tp_flags & Py_TPFLAGS_HEAPTYPE);

    Py_VISIT(type->tp_dict);
    Py_VISIT(type->tp_cache);
    Py_VISIT(type->tp_mro);
    Py_VISIT(type->tp_bases);
    Py_VISIT(type->tp_base);

    /* There's no need to visit type->tp_subclasses or
       ((PyHeapTypeObject *)type)->ht_slots, because they can't be involved
       in cycles; tp_subclasses is a list of weak references,
       and slots is a tuple of strings. */

    {int  ReplaceReturn218 = 0; Din_Go(1849,2048); return ReplaceReturn218;}
}

static int
type_clear(PyTypeObject *type)
{
Din_Go(1850,2048);    /* Because of type_is_gc(), the collector only calls this
       for heaptypes. */
    assert(type->tp_flags & Py_TPFLAGS_HEAPTYPE);

    /* We need to invalidate the method cache carefully before clearing
       the dict, so that other objects caught in a reference cycle
       don't start calling destroyed methods.

       Otherwise, the only field we need to clear is tp_mro, which is
       part of a hard cycle (its first element is the class itself) that
       won't be broken otherwise (it's a tuple and tuples don't have a
       tp_clear handler).  None of the other fields need to be
       cleared, and here's why:

       tp_cache:
           Not used; if it were, it would be a dict.

       tp_bases, tp_base:
           If these are involved in a cycle, there must be at least
           one other, mutable object in the cycle, e.g. a base
           class's dict; the cycle will be broken that way.

       tp_subclasses:
           A list of weak references can't be part of a cycle; and
           lists have their own tp_clear.

       slots (in PyHeapTypeObject):
           A tuple of strings can't be part of a cycle.
    */

    PyType_Modified(type);
    Din_Go(1852,2048);if (type->tp_dict)
        {/*283*/Din_Go(1851,2048);PyDict_Clear(type->tp_dict);/*284*/}
    Py_CLEAR(type->tp_mro);

    {int  ReplaceReturn217 = 0; Din_Go(1853,2048); return ReplaceReturn217;}
}

static int
type_is_gc(PyTypeObject *type)
{
    {int  ReplaceReturn216 = type->tp_flags & Py_TPFLAGS_HEAPTYPE; Din_Go(1854,2048); return ReplaceReturn216;}
}

PyTypeObject PyType_Type = {
    PyVarObject_HEAD_INIT(&PyType_Type, 0)
    "type",                                     /* tp_name */
    sizeof(PyHeapTypeObject),                   /* tp_basicsize */
    sizeof(PyMemberDef),                        /* tp_itemsize */
    (destructor)type_dealloc,                   /* tp_dealloc */
    0,                                          /* tp_print */
    0,                                          /* tp_getattr */
    0,                                          /* tp_setattr */
    0,                                  /* tp_compare */
    (reprfunc)type_repr,                        /* tp_repr */
    0,                                          /* tp_as_number */
    0,                                          /* tp_as_sequence */
    0,                                          /* tp_as_mapping */
    (hashfunc)_Py_HashPointer,                  /* tp_hash */
    (ternaryfunc)type_call,                     /* tp_call */
    0,                                          /* tp_str */
    (getattrofunc)type_getattro,                /* tp_getattro */
    (setattrofunc)type_setattro,                /* tp_setattro */
    0,                                          /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_HAVE_GC |
        Py_TPFLAGS_BASETYPE | Py_TPFLAGS_TYPE_SUBCLASS,         /* tp_flags */
    type_doc,                                   /* tp_doc */
    (traverseproc)type_traverse,                /* tp_traverse */
    (inquiry)type_clear,                        /* tp_clear */
    type_richcompare,                                           /* tp_richcompare */
    offsetof(PyTypeObject, tp_weaklist),        /* tp_weaklistoffset */
    0,                                          /* tp_iter */
    0,                                          /* tp_iternext */
    type_methods,                               /* tp_methods */
    type_members,                               /* tp_members */
    type_getsets,                               /* tp_getset */
    0,                                          /* tp_base */
    0,                                          /* tp_dict */
    0,                                          /* tp_descr_get */
    0,                                          /* tp_descr_set */
    offsetof(PyTypeObject, tp_dict),            /* tp_dictoffset */
    type_init,                                  /* tp_init */
    0,                                          /* tp_alloc */
    type_new,                                   /* tp_new */
    PyObject_GC_Del,                            /* tp_free */
    (inquiry)type_is_gc,                        /* tp_is_gc */
};


/* The base type of all types (eventually)... except itself. */

/* You may wonder why object.__new__() only complains about arguments
   when object.__init__() is not overridden, and vice versa.

   Consider the use cases:

   1. When neither is overridden, we want to hear complaints about
      excess (i.e., any) arguments, since their presence could
      indicate there's a bug.

   2. When defining an Immutable type, we are likely to override only
      __new__(), since __init__() is called too late to initialize an
      Immutable object.  Since __new__() defines the signature for the
      type, it would be a pain to have to override __init__() just to
      stop it from complaining about excess arguments.

   3. When defining a Mutable type, we are likely to override only
      __init__().  So here the converse reasoning applies: we don't
      want to have to override __new__() just to stop it from
      complaining.

   4. When __init__() is overridden, and the subclass __init__() calls
      object.__init__(), the latter should complain about excess
      arguments; ditto for __new__().

   Use cases 2 and 3 make it unattractive to unconditionally check for
   excess arguments.  The best solution that addresses all four use
   cases is as follows: __init__() complains about excess arguments
   unless __new__() is overridden and __init__() is not overridden
   (IOW, if __init__() is overridden or __new__() is not overridden);
   symmetrically, __new__() complains about excess arguments unless
   __init__() is overridden and __new__() is not overridden
   (IOW, if __new__() is overridden or __init__() is not overridden).

   However, for backwards compatibility, this breaks too much code.
   Therefore, in 2.6, we'll *warn* about excess arguments when both
   methods are overridden; for all other cases we'll use the above
   rules.

*/

/* Forward */
static PyObject *
object_new(PyTypeObject *type, PyObject *args, PyObject *kwds);

static int
excess_args(PyObject *args, PyObject *kwds)
{
    {int  ReplaceReturn215 = PyTuple_GET_SIZE(args) ||
        (kwds && PyDict_Check(kwds) && PyDict_Size(kwds)); Din_Go(1855,2048); return ReplaceReturn215;}
}

static int
object_init(PyObject *self, PyObject *args, PyObject *kwds)
{
    Din_Go(1856,2048);int err = 0;
    Din_Go(1862,2048);if (excess_args(args, kwds)) {
        Din_Go(1857,2048);PyTypeObject *type = Py_TYPE(self);
        Din_Go(1861,2048);if (type->tp_init != object_init &&
            type->tp_new != object_new)
        {
            Din_Go(1858,2048);err = PyErr_WarnEx(PyExc_DeprecationWarning,
                       "object.__init__() takes no parameters",
                       1);
        }
        else {/*301*/Din_Go(1859,2048);if (type->tp_init != object_init ||
                 type->tp_new == object_new)
        {
            Din_Go(1860,2048);PyErr_SetString(PyExc_TypeError,
                "object.__init__() takes no parameters");
            err = -1;
        ;/*302*/}}
    }
    {int  ReplaceReturn214 = err; Din_Go(1863,2048); return ReplaceReturn214;}
}

static PyObject *
object_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    Din_Go(1864,2048);int err = 0;
    Din_Go(1869,2048);if (excess_args(args, kwds)) {
        Din_Go(1865,2048);if (type->tp_new != object_new &&
            type->tp_init != object_init)
        {
            Din_Go(1866,2048);err = PyErr_WarnEx(PyExc_DeprecationWarning,
                       "object() takes no parameters",
                       1);
        }
        else {/*285*/Din_Go(1867,2048);if (type->tp_new != object_new ||
                 type->tp_init == object_init)
        {
            Din_Go(1868,2048);PyErr_SetString(PyExc_TypeError,
                "object() takes no parameters");
            err = -1;
        ;/*286*/}}
    }
    Din_Go(1870,2048);if (err < 0)
        return NULL;

    Din_Go(1895,2048);if (type->tp_flags & Py_TPFLAGS_IS_ABSTRACT) {
        Din_Go(1871,2048);static PyObject *comma = NULL;
        PyObject *abstract_methods = NULL;
        PyObject *builtins;
        PyObject *sorted;
        PyObject *sorted_methods = NULL;
        PyObject *joined = NULL;
        const char *joined_str;

        /* Compute ", ".join(sorted(type.__abstractmethods__))
           into joined. */
        abstract_methods = type_abstractmethods(type, NULL);
        Din_Go(1873,2048);if (abstract_methods == NULL)
            {/*287*/Din_Go(1872,2048);goto error;/*288*/}
        Din_Go(1874,2048);builtins = PyEval_GetBuiltins();
        Din_Go(1876,2048);if (builtins == NULL)
            {/*289*/Din_Go(1875,2048);goto error;/*290*/}
        Din_Go(1877,2048);sorted = PyDict_GetItemString(builtins, "sorted");
        Din_Go(1879,2048);if (sorted == NULL)
            {/*291*/Din_Go(1878,2048);goto error;/*292*/}
        Din_Go(1880,2048);sorted_methods = PyObject_CallFunctionObjArgs(sorted,
                                                      abstract_methods,
                                                      NULL);
        Din_Go(1882,2048);if (sorted_methods == NULL)
            {/*293*/Din_Go(1881,2048);goto error;/*294*/}
        Din_Go(1886,2048);if (comma == NULL) {
            Din_Go(1883,2048);comma = PyString_InternFromString(", ");
            Din_Go(1885,2048);if (comma == NULL)
                {/*295*/Din_Go(1884,2048);goto error;/*296*/}
        }
        Din_Go(1887,2048);joined = PyObject_CallMethod(comma, "join",
                                     "O",  sorted_methods);
        Din_Go(1889,2048);if (joined == NULL)
            {/*297*/Din_Go(1888,2048);goto error;/*298*/}
        Din_Go(1890,2048);joined_str = PyString_AsString(joined);
        Din_Go(1892,2048);if (joined_str == NULL)
            {/*299*/Din_Go(1891,2048);goto error;/*300*/}

        Din_Go(1893,2048);PyErr_Format(PyExc_TypeError,
                     "Can't instantiate abstract class %s "
                     "with abstract methods %s",
                     type->tp_name,
                     joined_str);
    error:
        Py_XDECREF(joined);
        Py_XDECREF(sorted_methods);
        Py_XDECREF(abstract_methods);
        {PyObject * ReplaceReturn213 = NULL; Din_Go(1894,2048); return ReplaceReturn213;}
    }
    {PyObject * ReplaceReturn212 = type->tp_alloc(type, 0); Din_Go(1896,2048); return ReplaceReturn212;}
}

static void
object_dealloc(PyObject *self)
{
Din_Go(1897,2048);    Py_TYPE(self)->tp_free(self);
Din_Go(1898,2048);}

static PyObject *
object_repr(PyObject *self)
{
    Din_Go(1899,2048);PyTypeObject *type;
    PyObject *mod, *name, *rtn;

    type = Py_TYPE(self);
    mod = type_module(type, NULL);
    Din_Go(1903,2048);if (mod == NULL)
        {/*303*/Din_Go(1900,2048);PyErr_Clear();/*304*/}
    else {/*305*/Din_Go(1901,2048);if (!PyString_Check(mod)) {
        Py_DECREF(mod);
        Din_Go(1902,2048);mod = NULL;
    ;/*306*/}}
    Din_Go(1904,2048);name = type_name(type, NULL);
    Din_Go(1906,2048);if (name == NULL) {
        Py_XDECREF(mod);
        {PyObject * ReplaceReturn211 = NULL; Din_Go(1905,2048); return ReplaceReturn211;}
    }
    Din_Go(1909,2048);if (mod != NULL && strcmp(PyString_AS_STRING(mod), "__builtin__"))
        {/*307*/Din_Go(1907,2048);rtn = PyString_FromFormat("<%s.%s object at %p>",
                                  PyString_AS_STRING(mod),
                                  PyString_AS_STRING(name),
                                  self);/*308*/}
    else
        {/*309*/Din_Go(1908,2048);rtn = PyString_FromFormat("<%s object at %p>",
                                  type->tp_name, self);/*310*/}
    Py_XDECREF(mod);
    Py_DECREF(name);
    {PyObject * ReplaceReturn210 = rtn; Din_Go(1910,2048); return ReplaceReturn210;}
}

static PyObject *
object_str(PyObject *self)
{
    Din_Go(1911,2048);unaryfunc f;

    f = Py_TYPE(self)->tp_repr;
    Din_Go(1913,2048);if (f == NULL)
        {/*311*/Din_Go(1912,2048);f = object_repr;/*312*/}
    {PyObject * ReplaceReturn209 = f(self); Din_Go(1914,2048); return ReplaceReturn209;}
}

static PyObject *
object_get_class(PyObject *self, void *closure)
{
Din_Go(1915,2048);    Py_INCREF(Py_TYPE(self));
    {PyObject * ReplaceReturn208 = (PyObject *)(Py_TYPE(self)); Din_Go(1916,2048); return ReplaceReturn208;}
}

static int
equiv_structs(PyTypeObject *a, PyTypeObject *b)
{
    {int  ReplaceReturn207 = a == b ||
           (a != NULL &&
        b != NULL &&
        a->tp_basicsize == b->tp_basicsize &&
        a->tp_itemsize == b->tp_itemsize &&
        a->tp_dictoffset == b->tp_dictoffset &&
        a->tp_weaklistoffset == b->tp_weaklistoffset &&
        ((a->tp_flags & Py_TPFLAGS_HAVE_GC) ==
         (b->tp_flags & Py_TPFLAGS_HAVE_GC))); Din_Go(1917,2048); return ReplaceReturn207;}
}

static int
same_slots_added(PyTypeObject *a, PyTypeObject *b)
{
    Din_Go(1918,2048);PyTypeObject *base = a->tp_base;
    Py_ssize_t size;
    PyObject *slots_a, *slots_b;

    assert(base == b->tp_base);
    size = base->tp_basicsize;
    Din_Go(1920,2048);if (a->tp_dictoffset == size && b->tp_dictoffset == size)
        {/*313*/Din_Go(1919,2048);size += sizeof(PyObject *);/*314*/}
    Din_Go(1922,2048);if (a->tp_weaklistoffset == size && b->tp_weaklistoffset == size)
        {/*315*/Din_Go(1921,2048);size += sizeof(PyObject *);/*316*/}

    /* Check slots compliance */
    Din_Go(1923,2048);slots_a = ((PyHeapTypeObject *)a)->ht_slots;
    slots_b = ((PyHeapTypeObject *)b)->ht_slots;
    Din_Go(1927,2048);if (slots_a && slots_b) {
        Din_Go(1924,2048);if (PyObject_Compare(slots_a, slots_b) != 0)
            {/*317*/{int  ReplaceReturn206 = 0; Din_Go(1925,2048); return ReplaceReturn206;}/*318*/}
        Din_Go(1926,2048);size += sizeof(PyObject *) * PyTuple_GET_SIZE(slots_a);
    }
    {int  ReplaceReturn205 = size == a->tp_basicsize && size == b->tp_basicsize; Din_Go(1928,2048); return ReplaceReturn205;}
}

static int
compatible_for_assignment(PyTypeObject* oldto, PyTypeObject* newto, char* attr)
{
    Din_Go(1929,2048);PyTypeObject *newbase, *oldbase;

    Din_Go(1932,2048);if (newto->tp_dealloc != oldto->tp_dealloc ||
        newto->tp_free != oldto->tp_free)
    {
        Din_Go(1930,2048);PyErr_Format(PyExc_TypeError,
                     "%s assignment: "
                     "'%s' deallocator differs from '%s'",
                     attr,
                     newto->tp_name,
                     oldto->tp_name);
        {int  ReplaceReturn204 = 0; Din_Go(1931,2048); return ReplaceReturn204;}
    }
    Din_Go(1933,2048);newbase = newto;
    oldbase = oldto;
    Din_Go(1935,2048);while (equiv_structs(newbase, newbase->tp_base))
        {/*103*/Din_Go(1934,2048);newbase = newbase->tp_base;/*104*/}
    Din_Go(1937,2048);while (equiv_structs(oldbase, oldbase->tp_base))
        {/*105*/Din_Go(1936,2048);oldbase = oldbase->tp_base;/*106*/}
    Din_Go(1940,2048);if (newbase != oldbase &&
        (newbase->tp_base != oldbase->tp_base ||
         !same_slots_added(newbase, oldbase))) {
        Din_Go(1938,2048);PyErr_Format(PyExc_TypeError,
                     "%s assignment: "
                     "'%s' object layout differs from '%s'",
                     attr,
                     newto->tp_name,
                     oldto->tp_name);
        {int  ReplaceReturn203 = 0; Din_Go(1939,2048); return ReplaceReturn203;}
    }

    {int  ReplaceReturn202 = 1; Din_Go(1941,2048); return ReplaceReturn202;}
}

static int
object_set_class(PyObject *self, PyObject *value, void *closure)
{
    Din_Go(1942,2048);PyTypeObject *oldto = Py_TYPE(self);
    PyTypeObject *newto;

    Din_Go(1945,2048);if (value == NULL) {
        Din_Go(1943,2048);PyErr_SetString(PyExc_TypeError,
                        "can't delete __class__ attribute");
        {int  ReplaceReturn201 = -1; Din_Go(1944,2048); return ReplaceReturn201;}
    }
    Din_Go(1948,2048);if (!PyType_Check(value)) {
        Din_Go(1946,2048);PyErr_Format(PyExc_TypeError,
          "__class__ must be set to new-style class, not '%s' object",
          Py_TYPE(value)->tp_name);
        {int  ReplaceReturn200 = -1; Din_Go(1947,2048); return ReplaceReturn200;}
    }
    Din_Go(1949,2048);newto = (PyTypeObject *)value;
    Din_Go(1952,2048);if (!(newto->tp_flags & Py_TPFLAGS_HEAPTYPE) ||
        !(oldto->tp_flags & Py_TPFLAGS_HEAPTYPE))
    {
        Din_Go(1950,2048);PyErr_Format(PyExc_TypeError,
                     "__class__ assignment: only for heap types");
        {int  ReplaceReturn199 = -1; Din_Go(1951,2048); return ReplaceReturn199;}
    }
    Din_Go(1955,2048);if (compatible_for_assignment(newto, oldto, "__class__")) {
        Py_INCREF(newto);
        Py_TYPE(self) = newto;
        Py_DECREF(oldto);
        {int  ReplaceReturn198 = 0; Din_Go(1953,2048); return ReplaceReturn198;}
    }
    else {
        {int  ReplaceReturn197 = -1; Din_Go(1954,2048); return ReplaceReturn197;}
    }
Din_Go(1956,2048);}

static PyGetSetDef object_getsets[] = {
    {"__class__", object_get_class, object_set_class,
     PyDoc_STR("the object's class")},
    {0}
};


/* Stuff to implement __reduce_ex__ for pickle protocols >= 2.
   We fall back to helpers in copy_reg for:
   - pickle protocols < 2
   - calculating the list of slot names (done only once per class)
   - the __newobj__ function (which is used as a token but never called)
*/

static PyObject *
import_copyreg(void)
{
    Din_Go(1957,2048);static PyObject *copyreg_str;

    Din_Go(1960,2048);if (!copyreg_str) {
        Din_Go(1958,2048);copyreg_str = PyString_InternFromString("copy_reg");
        Din_Go(1959,2048);if (copyreg_str == NULL)
            return NULL;
    }

    {PyObject * ReplaceReturn196 = PyImport_Import(copyreg_str); Din_Go(1961,2048); return ReplaceReturn196;}
}

static PyObject *
slotnames(PyObject *cls)
{
    Din_Go(1962,2048);PyObject *clsdict;
    PyObject *copyreg;
    PyObject *slotnames;

    Din_Go(1964,2048);if (!PyType_Check(cls)) {
        Py_INCREF(Py_None);
        {PyObject * ReplaceReturn195 = Py_None; Din_Go(1963,2048); return ReplaceReturn195;}
    }

    Din_Go(1965,2048);clsdict = ((PyTypeObject *)cls)->tp_dict;
    slotnames = PyDict_GetItemString(clsdict, "__slotnames__");
    Din_Go(1967,2048);if (slotnames != NULL && PyList_Check(slotnames)) {
        Py_INCREF(slotnames);
        {PyObject * ReplaceReturn194 = slotnames; Din_Go(1966,2048); return ReplaceReturn194;}
    }

    Din_Go(1968,2048);copyreg = import_copyreg();
    Din_Go(1969,2048);if (copyreg == NULL)
        return NULL;

    Din_Go(1970,2048);slotnames = PyObject_CallMethod(copyreg, "_slotnames", "O", cls);
    Py_DECREF(copyreg);
    Din_Go(1972,2048);if (slotnames != NULL &&
        slotnames != Py_None &&
        !PyList_Check(slotnames))
    {
        Din_Go(1971,2048);PyErr_SetString(PyExc_TypeError,
            "copy_reg._slotnames didn't return a list or None");
        Py_DECREF(slotnames);
        slotnames = NULL;
    }

    {PyObject * ReplaceReturn193 = slotnames; Din_Go(1973,2048); return ReplaceReturn193;}
}

static PyObject *
reduce_2(PyObject *obj)
{
    Din_Go(1974,2048);PyObject *cls, *getnewargs;
    PyObject *args = NULL, *args2 = NULL;
    PyObject *getstate = NULL, *state = NULL, *names = NULL;
    PyObject *slots = NULL, *listitems = NULL, *dictitems = NULL;
    PyObject *copyreg = NULL, *newobj = NULL, *res = NULL;
    Py_ssize_t i, n;

    cls = PyObject_GetAttrString(obj, "__class__");
    Din_Go(1975,2048);if (cls == NULL)
        return NULL;

    Din_Go(1976,2048);getnewargs = PyObject_GetAttrString(obj, "__getnewargs__");
    Din_Go(1982,2048);if (getnewargs != NULL) {
        Din_Go(1977,2048);args = PyObject_CallObject(getnewargs, NULL);
        Py_DECREF(getnewargs);
        Din_Go(1980,2048);if (args != NULL && !PyTuple_Check(args)) {
            Din_Go(1978,2048);PyErr_Format(PyExc_TypeError,
                "__getnewargs__ should return a tuple, "
                "not '%.200s'", Py_TYPE(args)->tp_name);
            Din_Go(1979,2048);goto end;
        }
    }
    else {
        Din_Go(1981,2048);PyErr_Clear();
        args = PyTuple_New(0);
    }
    Din_Go(1984,2048);if (args == NULL)
        {/*319*/Din_Go(1983,2048);goto end;/*320*/}

    Din_Go(1985,2048);getstate = PyObject_GetAttrString(obj, "__getstate__");
    Din_Go(2012,2048);if (getstate != NULL) {
        Din_Go(1986,2048);state = PyObject_CallObject(getstate, NULL);
        Py_DECREF(getstate);
        Din_Go(1988,2048);if (state == NULL)
            {/*321*/Din_Go(1987,2048);goto end;/*322*/}
    }
    else {
        Din_Go(1989,2048);PyErr_Clear();
        state = PyObject_GetAttrString(obj, "__dict__");
        Din_Go(1991,2048);if (state == NULL) {
            Din_Go(1990,2048);PyErr_Clear();
            state = Py_None;
            Py_INCREF(state);
        }
        Din_Go(1992,2048);names = slotnames(cls);
        Din_Go(1994,2048);if (names == NULL)
            {/*323*/Din_Go(1993,2048);goto end;/*324*/}
        Din_Go(2011,2048);if (names != Py_None) {
            assert(PyList_Check(names));
            Din_Go(1995,2048);slots = PyDict_New();
            Din_Go(1997,2048);if (slots == NULL)
                {/*325*/Din_Go(1996,2048);goto end;/*326*/}
            Din_Go(1998,2048);n = 0;
            /* Can't pre-compute the list size; the list
               is stored on the class so accessible to other
               threads, which may be run by DECREF */
            Din_Go(2006,2048);for (i = 0; i < PyList_GET_SIZE(names); i++) {
                Din_Go(1999,2048);PyObject *name, *value;
                name = PyList_GET_ITEM(names, i);
                value = PyObject_GetAttr(obj, name);
                Din_Go(2005,2048);if (value == NULL)
                    {/*327*/Din_Go(2000,2048);PyErr_Clear();/*328*/}
                else {
                    Din_Go(2001,2048);int err = PyDict_SetItem(slots, name,
                                             value);
                    Py_DECREF(value);
                    Din_Go(2003,2048);if (err)
                        {/*329*/Din_Go(2002,2048);goto end;/*330*/}
                    Din_Go(2004,2048);n++;
                }
            }
            Din_Go(2010,2048);if (n) {
                Din_Go(2007,2048);state = Py_BuildValue("(NO)", state, slots);
                Din_Go(2009,2048);if (state == NULL)
                    {/*331*/Din_Go(2008,2048);goto end;/*332*/}
            }
        }
    }

    Din_Go(2017,2048);if (!PyList_Check(obj)) {
        Din_Go(2013,2048);listitems = Py_None;
        Py_INCREF(listitems);
    }
    else {
        Din_Go(2014,2048);listitems = PyObject_GetIter(obj);
        Din_Go(2016,2048);if (listitems == NULL)
            {/*333*/Din_Go(2015,2048);goto end;/*334*/}
    }

    Din_Go(2022,2048);if (!PyDict_Check(obj)) {
        Din_Go(2018,2048);dictitems = Py_None;
        Py_INCREF(dictitems);
    }
    else {
        Din_Go(2019,2048);dictitems = PyObject_CallMethod(obj, "iteritems", "");
        Din_Go(2021,2048);if (dictitems == NULL)
            {/*335*/Din_Go(2020,2048);goto end;/*336*/}
    }

    Din_Go(2023,2048);copyreg = import_copyreg();
    Din_Go(2025,2048);if (copyreg == NULL)
        {/*337*/Din_Go(2024,2048);goto end;/*338*/}
    Din_Go(2026,2048);newobj = PyObject_GetAttrString(copyreg, "__newobj__");
    Din_Go(2028,2048);if (newobj == NULL)
        {/*339*/Din_Go(2027,2048);goto end;/*340*/}

    Din_Go(2029,2048);n = PyTuple_GET_SIZE(args);
    args2 = PyTuple_New(n+1);
    Din_Go(2031,2048);if (args2 == NULL)
        {/*341*/Din_Go(2030,2048);goto end;/*342*/}
    PyTuple_SET_ITEM(args2, 0, cls);
    Din_Go(2032,2048);cls = NULL;
    Din_Go(2034,2048);for (i = 0; i < n; i++) {
        Din_Go(2033,2048);PyObject *v = PyTuple_GET_ITEM(args, i);
        Py_INCREF(v);
        PyTuple_SET_ITEM(args2, i+1, v);
    }

    Din_Go(2035,2048);res = PyTuple_Pack(5, newobj, args2, state, listitems, dictitems);

  end:
    Py_XDECREF(cls);
    Py_XDECREF(args);
    Py_XDECREF(args2);
    Py_XDECREF(slots);
    Py_XDECREF(state);
    Py_XDECREF(names);
    Py_XDECREF(listitems);
    Py_XDECREF(dictitems);
    Py_XDECREF(copyreg);
    Py_XDECREF(newobj);
    {PyObject * ReplaceReturn192 = res; Din_Go(2036,2048); return ReplaceReturn192;}
}

/*
 * There were two problems when object.__reduce__ and object.__reduce_ex__
 * were implemented in the same function:
 *  - trying to pickle an object with a custom __reduce__ method that
 *    fell back to object.__reduce__ in certain circumstances led to
 *    infinite recursion at Python level and eventual RuntimeError.
 *  - Pickling objects that lied about their type by overwriting the
 *    __class__ descriptor could lead to infinite recursion at C level
 *    and eventual segfault.
 *
 * Because of backwards compatibility, the two methods still have to
 * behave in the same way, even if this is not required by the pickle
 * protocol. This common functionality was moved to the _common_reduce
 * function.
 */
static PyObject *
_common_reduce(PyObject *self, int proto)
{
    Din_Go(2037,2048);PyObject *copyreg, *res;

    Din_Go(2039,2048);if (proto >= 2)
        {/*343*/{PyObject * ReplaceReturn191 = reduce_2(self); Din_Go(2038,2048); return ReplaceReturn191;}/*344*/}

    Din_Go(2040,2048);copyreg = import_copyreg();
    Din_Go(2041,2048);if (!copyreg)
        return NULL;

    Din_Go(2042,2048);res = PyEval_CallMethod(copyreg, "_reduce_ex", "(Oi)", self, proto);
    Py_DECREF(copyreg);

    {PyObject * ReplaceReturn190 = res; Din_Go(2043,2048); return ReplaceReturn190;}
}

static PyObject *
object_reduce(PyObject *self, PyObject *args)
{
    Din_Go(2044,2048);int proto = 0;

    Din_Go(2045,2048);if (!PyArg_ParseTuple(args, "|i:__reduce__", &proto))
        return NULL;

    {PyObject * ReplaceReturn189 = _common_reduce(self, proto); Din_Go(2046,2048); return ReplaceReturn189;}
}

static PyObject *
object_reduce_ex(PyObject *self, PyObject *args)
{
    Din_Go(2047,2048);PyObject *reduce, *res;
    int proto = 0;

    Din_Go(2048,2048);if (!PyArg_ParseTuple(args, "|i:__reduce_ex__", &proto))
        return NULL;

    Din_Go(2049,2048);reduce = PyObject_GetAttrString(self, "__reduce__");
    Din_Go(2061,2048);if (reduce == NULL)
        {/*345*/Din_Go(2050,2048);PyErr_Clear();/*346*/}
    else {
        Din_Go(2051,2048);PyObject *cls, *clsreduce, *objreduce;
        int override;
        cls = PyObject_GetAttrString(self, "__class__");
        Din_Go(2053,2048);if (cls == NULL) {
            Py_DECREF(reduce);
            {PyObject * ReplaceReturn188 = NULL; Din_Go(2052,2048); return ReplaceReturn188;}
        }
        Din_Go(2054,2048);clsreduce = PyObject_GetAttrString(cls, "__reduce__");
        Py_DECREF(cls);
        Din_Go(2056,2048);if (clsreduce == NULL) {
            Py_DECREF(reduce);
            {PyObject * ReplaceReturn187 = NULL; Din_Go(2055,2048); return ReplaceReturn187;}
        }
        Din_Go(2057,2048);objreduce = PyDict_GetItemString(PyBaseObject_Type.tp_dict,
                                         "__reduce__");
        override = (clsreduce != objreduce);
        Py_DECREF(clsreduce);
        Din_Go(2060,2048);if (override) {
            Din_Go(2058,2048);res = PyObject_CallObject(reduce, NULL);
            Py_DECREF(reduce);
            {PyObject * ReplaceReturn186 = res; Din_Go(2059,2048); return ReplaceReturn186;}
        }
        else
            Py_DECREF(reduce);
    }

    {PyObject * ReplaceReturn185 = _common_reduce(self, proto); Din_Go(2062,2048); return ReplaceReturn185;}
}

static PyObject *
object_subclasshook(PyObject *cls, PyObject *args)
{
Din_Go(2063,2048);    Py_INCREF(Py_NotImplemented);
    {PyObject * ReplaceReturn184 = Py_NotImplemented; Din_Go(2064,2048); return ReplaceReturn184;}
}

PyDoc_STRVAR(object_subclasshook_doc,
"Abstract classes can override this to customize issubclass().\n"
"\n"
"This is invoked early on by abc.ABCMeta.__subclasscheck__().\n"
"It should return True, False or NotImplemented.  If it returns\n"
"NotImplemented, the normal algorithm is used.  Otherwise, it\n"
"overrides the normal algorithm (and the outcome is cached).\n");

/*
   from PEP 3101, this code implements:

   class object:
       def __format__(self, format_spec):
       if isinstance(format_spec, str):
           return format(str(self), format_spec)
       elif isinstance(format_spec, unicode):
           return format(unicode(self), format_spec)
*/
static PyObject *
object_format(PyObject *self, PyObject *args)
{
    Din_Go(2065,2048);PyObject *format_spec;
    PyObject *self_as_str = NULL;
    PyObject *result = NULL;
    Py_ssize_t format_len;

    Din_Go(2066,2048);if (!PyArg_ParseTuple(args, "O:__format__", &format_spec))
        return NULL;
#ifdef Py_USING_UNICODE
    Din_Go(2072,2048);if (PyUnicode_Check(format_spec)) {
        Din_Go(2067,2048);format_len = PyUnicode_GET_SIZE(format_spec);
        self_as_str = PyObject_Unicode(self);
    } else {/*347*/Din_Go(2068,2048);if (PyString_Check(format_spec)) {
#else
    if (PyString_Check(format_spec)) {
#endif
        Din_Go(2069,2048);format_len = PyString_GET_SIZE(format_spec);
        self_as_str = PyObject_Str(self);
    } else {
        Din_Go(2070,2048);PyErr_SetString(PyExc_TypeError,
                 "argument to __format__ must be unicode or str");
        {PyObject * ReplaceReturn183 = NULL; Din_Go(2071,2048); return ReplaceReturn183;}
    ;/*348*/}}

    Din_Go(2077,2048);if (self_as_str != NULL) {
        /* Issue 7994: If we're converting to a string, we
           should reject format specifications */
        Din_Go(2073,2048);if (format_len > 0) {
            Din_Go(2074,2048);if (PyErr_WarnEx(PyExc_PendingDeprecationWarning,
             "object.__format__ with a non-empty format "
             "string is deprecated", 1) < 0) {
                Din_Go(2075,2048);goto done;
            }
            /* Eventually this will become an error:
            PyErr_Format(PyExc_TypeError,
               "non-empty format string passed to object.__format__");
            goto done;
            */
        }
        Din_Go(2076,2048);result = PyObject_Format(self_as_str, format_spec);
    }

done:
    Py_XDECREF(self_as_str);

    {PyObject * ReplaceReturn182 = result; Din_Go(2078,2048); return ReplaceReturn182;}
}

static PyObject *
object_sizeof(PyObject *self, PyObject *args)
{
    Din_Go(2079,2048);Py_ssize_t res, isize;

    res = 0;
    isize = self->ob_type->tp_itemsize;
    Din_Go(2081,2048);if (isize > 0)
        {/*349*/Din_Go(2080,2048);res = self->ob_type->ob_size * isize;/*350*/}
    Din_Go(2082,2048);res += self->ob_type->tp_basicsize;

    {PyObject * ReplaceReturn181 = PyInt_FromSsize_t(res); Din_Go(2083,2048); return ReplaceReturn181;}
}

static PyMethodDef object_methods[] = {
    {"__reduce_ex__", object_reduce_ex, METH_VARARGS,
     PyDoc_STR("helper for pickle")},
    {"__reduce__", object_reduce, METH_VARARGS,
     PyDoc_STR("helper for pickle")},
    {"__subclasshook__", object_subclasshook, METH_CLASS | METH_VARARGS,
     object_subclasshook_doc},
    {"__format__", object_format, METH_VARARGS,
     PyDoc_STR("default object formatter")},
    {"__sizeof__", object_sizeof, METH_NOARGS,
     PyDoc_STR("__sizeof__() -> int\nsize of object in memory, in bytes")},
    {0}
};


PyTypeObject PyBaseObject_Type = {
    PyVarObject_HEAD_INIT(&PyType_Type, 0)
    "object",                                   /* tp_name */
    sizeof(PyObject),                           /* tp_basicsize */
    0,                                          /* tp_itemsize */
    object_dealloc,                             /* tp_dealloc */
    0,                                          /* tp_print */
    0,                                          /* tp_getattr */
    0,                                          /* tp_setattr */
    0,                                          /* tp_compare */
    object_repr,                                /* tp_repr */
    0,                                          /* tp_as_number */
    0,                                          /* tp_as_sequence */
    0,                                          /* tp_as_mapping */
    (hashfunc)_Py_HashPointer,                  /* tp_hash */
    0,                                          /* tp_call */
    object_str,                                 /* tp_str */
    PyObject_GenericGetAttr,                    /* tp_getattro */
    PyObject_GenericSetAttr,                    /* tp_setattro */
    0,                                          /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
    PyDoc_STR("The most base type"),            /* tp_doc */
    0,                                          /* tp_traverse */
    0,                                          /* tp_clear */
    0,                                          /* tp_richcompare */
    0,                                          /* tp_weaklistoffset */
    0,                                          /* tp_iter */
    0,                                          /* tp_iternext */
    object_methods,                             /* tp_methods */
    0,                                          /* tp_members */
    object_getsets,                             /* tp_getset */
    0,                                          /* tp_base */
    0,                                          /* tp_dict */
    0,                                          /* tp_descr_get */
    0,                                          /* tp_descr_set */
    0,                                          /* tp_dictoffset */
    object_init,                                /* tp_init */
    PyType_GenericAlloc,                        /* tp_alloc */
    object_new,                                 /* tp_new */
    PyObject_Del,                               /* tp_free */
};


/* Initialize the __dict__ in a type object */

static int
add_methods(PyTypeObject *type, PyMethodDef *meth)
{
    Din_Go(2084,2048);PyObject *dict = type->tp_dict;

    Din_Go(2104,2048);for (; meth->ml_name != NULL; meth++) {
        Din_Go(2085,2048);PyObject *descr;
        int err;
        Din_Go(2087,2048);if (PyDict_GetItemString(dict, meth->ml_name) &&
            !(meth->ml_flags & METH_COEXIST))
                {/*351*/Din_Go(2086,2048);continue;/*352*/}
        Din_Go(2098,2048);if (meth->ml_flags & METH_CLASS) {
            Din_Go(2088,2048);if (meth->ml_flags & METH_STATIC) {
                Din_Go(2089,2048);PyErr_SetString(PyExc_ValueError,
                     "method cannot be both class and static");
                {int  ReplaceReturn180 = -1; Din_Go(2090,2048); return ReplaceReturn180;}
            }
            Din_Go(2091,2048);descr = PyDescr_NewClassMethod(type, meth);
        }
        else {/*353*/Din_Go(2092,2048);if (meth->ml_flags & METH_STATIC) {
            Din_Go(2093,2048);PyObject *cfunc = PyCFunction_New(meth, NULL);
            Din_Go(2095,2048);if (cfunc == NULL)
                {/*355*/{int  ReplaceReturn179 = -1; Din_Go(2094,2048); return ReplaceReturn179;}/*356*/}
            Din_Go(2096,2048);descr = PyStaticMethod_New(cfunc);
            Py_DECREF(cfunc);
        }
        else {
            Din_Go(2097,2048);descr = PyDescr_NewMethod(type, meth);
        ;/*354*/}}
        Din_Go(2100,2048);if (descr == NULL)
            {/*357*/{int  ReplaceReturn178 = -1; Din_Go(2099,2048); return ReplaceReturn178;}/*358*/}
        Din_Go(2101,2048);err = PyDict_SetItemString(dict, meth->ml_name, descr);
        Py_DECREF(descr);
        Din_Go(2103,2048);if (err < 0)
            {/*359*/{int  ReplaceReturn177 = -1; Din_Go(2102,2048); return ReplaceReturn177;}/*360*/}
    }
    {int  ReplaceReturn176 = 0; Din_Go(2105,2048); return ReplaceReturn176;}
}

static int
add_members(PyTypeObject *type, PyMemberDef *memb)
{
    Din_Go(2106,2048);PyObject *dict = type->tp_dict;

    Din_Go(2115,2048);for (; memb->name != NULL; memb++) {
        Din_Go(2107,2048);PyObject *descr;
        Din_Go(2109,2048);if (PyDict_GetItemString(dict, memb->name))
            {/*361*/Din_Go(2108,2048);continue;/*362*/}
        Din_Go(2110,2048);descr = PyDescr_NewMember(type, memb);
        Din_Go(2112,2048);if (descr == NULL)
            {/*363*/{int  ReplaceReturn175 = -1; Din_Go(2111,2048); return ReplaceReturn175;}/*364*/}
        Din_Go(2114,2048);if (PyDict_SetItemString(dict, memb->name, descr) < 0)
            {/*365*/{int  ReplaceReturn174 = -1; Din_Go(2113,2048); return ReplaceReturn174;}/*366*/}
        Py_DECREF(descr);
    }
    {int  ReplaceReturn173 = 0; Din_Go(2116,2048); return ReplaceReturn173;}
}

static int
add_getset(PyTypeObject *type, PyGetSetDef *gsp)
{
    Din_Go(2117,2048);PyObject *dict = type->tp_dict;

    Din_Go(2126,2048);for (; gsp->name != NULL; gsp++) {
        Din_Go(2118,2048);PyObject *descr;
        Din_Go(2120,2048);if (PyDict_GetItemString(dict, gsp->name))
            {/*367*/Din_Go(2119,2048);continue;/*368*/}
        Din_Go(2121,2048);descr = PyDescr_NewGetSet(type, gsp);

        Din_Go(2123,2048);if (descr == NULL)
            {/*369*/{int  ReplaceReturn172 = -1; Din_Go(2122,2048); return ReplaceReturn172;}/*370*/}
        Din_Go(2125,2048);if (PyDict_SetItemString(dict, gsp->name, descr) < 0)
            {/*371*/{int  ReplaceReturn171 = -1; Din_Go(2124,2048); return ReplaceReturn171;}/*372*/}
        Py_DECREF(descr);
    }
    {int  ReplaceReturn170 = 0; Din_Go(2127,2048); return ReplaceReturn170;}
}

#define BUFFER_FLAGS (Py_TPFLAGS_HAVE_GETCHARBUFFER | Py_TPFLAGS_HAVE_NEWBUFFER)

static void
inherit_special(PyTypeObject *type, PyTypeObject *base)
{
    Din_Go(2128,2048);Py_ssize_t oldsize, newsize;

    /* Special flag magic */
    Din_Go(2130,2048);if (!type->tp_as_buffer && base->tp_as_buffer) {
        Din_Go(2129,2048);type->tp_flags &= ~BUFFER_FLAGS;
        type->tp_flags |=
            base->tp_flags & BUFFER_FLAGS;
    }
    Din_Go(2132,2048);if (!type->tp_as_sequence && base->tp_as_sequence) {
        Din_Go(2131,2048);type->tp_flags &= ~Py_TPFLAGS_HAVE_SEQUENCE_IN;
        type->tp_flags |= base->tp_flags & Py_TPFLAGS_HAVE_SEQUENCE_IN;
    }
    Din_Go(2137,2048);if ((type->tp_flags & Py_TPFLAGS_HAVE_INPLACEOPS) !=
        (base->tp_flags & Py_TPFLAGS_HAVE_INPLACEOPS)) {
        Din_Go(2133,2048);if ((!type->tp_as_number && base->tp_as_number) ||
            (!type->tp_as_sequence && base->tp_as_sequence)) {
            Din_Go(2134,2048);type->tp_flags &= ~Py_TPFLAGS_HAVE_INPLACEOPS;
            Din_Go(2136,2048);if (!type->tp_as_number && !type->tp_as_sequence) {
                Din_Go(2135,2048);type->tp_flags |= base->tp_flags &
                    Py_TPFLAGS_HAVE_INPLACEOPS;
            }
        }
        /* Wow */
    }
    Din_Go(2139,2048);if (!type->tp_as_number && base->tp_as_number) {
        Din_Go(2138,2048);type->tp_flags &= ~Py_TPFLAGS_CHECKTYPES;
        type->tp_flags |= base->tp_flags & Py_TPFLAGS_CHECKTYPES;
    }

    /* Copying basicsize is connected to the GC flags */
    Din_Go(2140,2048);oldsize = base->tp_basicsize;
    newsize = type->tp_basicsize ? type->tp_basicsize : oldsize;
    Din_Go(2146,2048);if (!(type->tp_flags & Py_TPFLAGS_HAVE_GC) &&
        (base->tp_flags & Py_TPFLAGS_HAVE_GC) &&
        (type->tp_flags & Py_TPFLAGS_HAVE_RICHCOMPARE/*GC slots exist*/) &&
        (!type->tp_traverse && !type->tp_clear)) {
        Din_Go(2141,2048);type->tp_flags |= Py_TPFLAGS_HAVE_GC;
        Din_Go(2143,2048);if (type->tp_traverse == NULL)
            {/*373*/Din_Go(2142,2048);type->tp_traverse = base->tp_traverse;/*374*/}
        Din_Go(2145,2048);if (type->tp_clear == NULL)
            {/*375*/Din_Go(2144,2048);type->tp_clear = base->tp_clear;/*376*/}
    }
    Din_Go(2150,2048);if (type->tp_flags & base->tp_flags & Py_TPFLAGS_HAVE_CLASS) {
        /* The condition below could use some explanation.
           It appears that tp_new is not inherited for static types
           whose base class is 'object'; this seems to be a precaution
           so that old extension types don't suddenly become
           callable (object.__new__ wouldn't insure the invariants
           that the extension type's own factory function ensures).
           Heap types, of course, are under our control, so they do
           inherit tp_new; static extension types that specify some
           other built-in type as the default are considered
           new-style-aware so they also inherit object.__new__. */
        Din_Go(2147,2048);if (base != &PyBaseObject_Type ||
            (type->tp_flags & Py_TPFLAGS_HEAPTYPE)) {
            Din_Go(2148,2048);if (type->tp_new == NULL)
                {/*377*/Din_Go(2149,2048);type->tp_new = base->tp_new;/*378*/}
        }
    }
    Din_Go(2151,2048);type->tp_basicsize = newsize;

    /* Copy other non-function slots */

#undef COPYVAL
#define COPYVAL(SLOT) \
    if (type->SLOT == 0) type->SLOT = base->SLOT

    COPYVAL(tp_itemsize);/*380*/}
    Din_Go(2154,2048);if (type->tp_flags & base->tp_flags & Py_TPFLAGS_HAVE_WEAKREFS) {
        COPYVAL(tp_weaklistoffset);
    }
    Din_Go(2155,2048);if (type->tp_flags & base->tp_flags & Py_TPFLAGS_HAVE_CLASS) {
        COPYVAL(tp_dictoffset);
    }

    /* Setup fast subclass flags */
    Din_Go(2156,2048);if (PyType_IsSubtype(base, (PyTypeObject*)PyExc_BaseException))
        type->tp_flags |= Py_TPFLAGS_BASE_EXC_SUBCLASS;
    else if (PyType_IsSubtype(base, &PyType_Type))
        type->tp_flags |= Py_TPFLAGS_TYPE_SUBCLASS;
    else if (PyType_IsSubtype(base, &PyInt_Type))
        type->tp_flags |= Py_TPFLAGS_INT_SUBCLASS;
    else if (PyType_IsSubtype(base, &PyLong_Type))
        type->tp_flags |= Py_TPFLAGS_LONG_SUBCLASS;
    else if (PyType_IsSubtype(base, &PyString_Type))
        type->tp_flags |= Py_TPFLAGS_STRING_SUBCLASS;
#ifdef Py_USING_UNICODE
    else if (PyType_IsSubtype(base, &PyUnicode_Type))
        type->tp_flags |= Py_TPFLAGS_UNICODE_SUBCLASS;
#endif
    else if (PyType_IsSubtype(base, &PyTuple_Type))
        type->tp_flags |= Py_TPFLAGS_TUPLE_SUBCLASS;
    else if (PyType_IsSubtype(base, &PyList_Type))
        type->tp_flags |= Py_TPFLAGS_LIST_SUBCLASS;
    else if (PyType_IsSubtype(base, &PyDict_Type))
        type->tp_flags |= Py_TPFLAGS_DICT_SUBCLASS;
Din_Go(2157,2048);}

static int
overrides_name(PyTypeObject *type, char *name)
{
    Din_Go(2158,2048);PyObject *dict = type->tp_dict;

    assert(dict != NULL);
    Din_Go(2160,2048);if (PyDict_GetItemString(dict, name) != NULL) {
        {int  ReplaceReturn169 = 1; Din_Go(2159,2048); return ReplaceReturn169;}
    }
    {int  ReplaceReturn168 = 0; Din_Go(2161,2048); return ReplaceReturn168;}
}

#define OVERRIDES_HASH(x)       overrides_name(x, "__hash__")
#define OVERRIDES_EQ(x)         overrides_name(x, "__eq__")

static void
inherit_slots(PyTypeObject *type, PyTypeObject *base)
{
    Din_Go(2162,2048);PyTypeObject *basebase;

#undef SLOTDEFINED
#undef COPYSLOT
#undef COPYNUM
#undef COPYSEQ
#undef COPYMAP
#undef COPYBUF

#define SLOTDEFINED(SLOT) \
    (base->SLOT != 0 && \
     (basebase == NULL || base->SLOT != basebase->SLOT))

#define COPYSLOT(SLOT) \
    if (!type->SLOT && SLOTDEFINED(SLOT)) type->SLOT = base->SLOT

#define COPYNUM(SLOT) COPYSLOT(tp_as_number->SLOT)
#define COPYSEQ(SLOT) COPYSLOT(tp_as_sequence->SLOT)
#define COPYMAP(SLOT) COPYSLOT(tp_as_mapping->SLOT)
#define COPYBUF(SLOT) COPYSLOT(tp_as_buffer->SLOT)

    /* This won't inherit indirect slots (from tp_as_number etc.)
       if type doesn't provide the space. */

    Din_Go(2169,2048);if (type->tp_as_number != NULL && base->tp_as_number != NULL) {
        Din_Go(2163,2048);basebase = base->tp_base;
        Din_Go(2164,2048);if (basebase->tp_as_number == NULL)
            basebase = NULL;
        COPYNUM(nb_add);/*386*/}
        COPYNUM(nb_subtract);
        COPYNUM(nb_multiply);
        COPYNUM(nb_divide);
        COPYNUM(nb_remainder);
        COPYNUM(nb_divmod);
        COPYNUM(nb_power);
        COPYNUM(nb_negative);
        COPYNUM(nb_positive);
        COPYNUM(nb_absolute);
        COPYNUM(nb_nonzero);
        COPYNUM(nb_invert);
        COPYNUM(nb_lshift);
        COPYNUM(nb_rshift);
        COPYNUM(nb_and);
        COPYNUM(nb_xor);
        COPYNUM(nb_or);
        COPYNUM(nb_coerce);
        COPYNUM(nb_int);
        COPYNUM(nb_long);
        COPYNUM(nb_float);
        COPYNUM(nb_oct);
        COPYNUM(nb_hex);
        COPYNUM(nb_inplace_add);
        COPYNUM(nb_inplace_subtract);
        COPYNUM(nb_inplace_multiply);
        COPYNUM(nb_inplace_divide);
        COPYNUM(nb_inplace_remainder);
        COPYNUM(nb_inplace_power);
        COPYNUM(nb_inplace_lshift);
        COPYNUM(nb_inplace_rshift);
        COPYNUM(nb_inplace_and);
        COPYNUM(nb_inplace_xor);
        COPYNUM(nb_inplace_or);
        Din_Go(2167,2048);if (base->tp_flags & Py_TPFLAGS_CHECKTYPES) {
            COPYNUM(nb_true_divide);
            COPYNUM(nb_floor_divide);
            COPYNUM(nb_inplace_true_divide);
            COPYNUM(nb_inplace_floor_divide);
        }
        Din_Go(2168,2048);if (base->tp_flags & Py_TPFLAGS_HAVE_INDEX) {
            COPYNUM(nb_index);
        }
    }

    Din_Go(2172,2048);if (type->tp_as_sequence != NULL && base->tp_as_sequence != NULL) {
        Din_Go(2170,2048);basebase = base->tp_base;
        Din_Go(2171,2048);if (basebase->tp_as_sequence == NULL)
            basebase = NULL;
        COPYSEQ(sq_length);
        COPYSEQ(sq_concat);
        COPYSEQ(sq_repeat);
        COPYSEQ(sq_item);
        COPYSEQ(sq_slice);
        COPYSEQ(sq_ass_item);
        COPYSEQ(sq_ass_slice);
        COPYSEQ(sq_contains);
        COPYSEQ(sq_inplace_concat);
        COPYSEQ(sq_inplace_repeat);
    }

    Din_Go(2175,2048);if (type->tp_as_mapping != NULL && base->tp_as_mapping != NULL) {
        Din_Go(2173,2048);basebase = base->tp_base;
        Din_Go(2174,2048);if (basebase->tp_as_mapping == NULL)
            basebase = NULL;
        COPYMAP(mp_length);
        COPYMAP(mp_subscript);
        COPYMAP(mp_ass_subscript);
    }

    Din_Go(2178,2048);if (type->tp_as_buffer != NULL && base->tp_as_buffer != NULL) {
        Din_Go(2176,2048);basebase = base->tp_base;
        Din_Go(2177,2048);if (basebase->tp_as_buffer == NULL)
            basebase = NULL;
        COPYBUF(bf_getreadbuffer);
        COPYBUF(bf_getwritebuffer);
        COPYBUF(bf_getsegcount);
        COPYBUF(bf_getcharbuffer);
        COPYBUF(bf_getbuffer);
        COPYBUF(bf_releasebuffer);
    }

    Din_Go(2179,2048);basebase = base->tp_base;

    COPYSLOT(tp_dealloc);
    COPYSLOT(tp_print);
    Din_Go(2181,2048);if (type->tp_getattr == NULL && type->tp_getattro == NULL) {
        Din_Go(2180,2048);type->tp_getattr = base->tp_getattr;
        type->tp_getattro = base->tp_getattro;
    }
    Din_Go(2183,2048);if (type->tp_setattr == NULL && type->tp_setattro == NULL) {
        Din_Go(2182,2048);type->tp_setattr = base->tp_setattr;
        type->tp_setattro = base->tp_setattro;
    }
    /* tp_compare see tp_richcompare */
    COPYSLOT(tp_repr);
    /* tp_hash see tp_richcompare */
    COPYSLOT(tp_call);
    COPYSLOT(tp_str);
    Din_Go(2191,2048);if (type->tp_flags & base->tp_flags & Py_TPFLAGS_HAVE_RICHCOMPARE) {
        Din_Go(2184,2048);if (type->tp_compare == NULL &&
            type->tp_richcompare == NULL &&
            type->tp_hash == NULL)
        {
            Din_Go(2185,2048);type->tp_compare = base->tp_compare;
            type->tp_richcompare = base->tp_richcompare;
            type->tp_hash = base->tp_hash;
            /* Check for changes to inherited methods in Py3k*/
            Din_Go(2190,2048);if (Py_Py3kWarningFlag) {
                Din_Go(2186,2048);if (base->tp_hash &&
                                (base->tp_hash != PyObject_HashNotImplemented) &&
                                !OVERRIDES_HASH(type)) {
                    Din_Go(2187,2048);if (OVERRIDES_EQ(type)) {
                        Din_Go(2188,2048);if (PyErr_WarnPy3k("Overriding "
                                           "__eq__ blocks inheritance "
                                           "of __hash__ in 3.x",
                                           1) < 0)
                            /* XXX This isn't right.  If the warning is turned
                               into an exception, we should be communicating
                               the error back to the caller, but figuring out
                               how to clean up in that case is tricky.  See
                               issue 8627 for more. */
                            {/*511*/Din_Go(2189,2048);PyErr_Clear();/*512*/}
                    }
                }
            }
        }
    }
    else {
        COPYSLOT(tp_compare);
    }
    Din_Go(2192,2048);if (type->tp_flags & base->tp_flags & Py_TPFLAGS_HAVE_ITER) {
        COPYSLOT(tp_iter);
        COPYSLOT(tp_iternext);
    }
    Din_Go(2196,2048);if (type->tp_flags & base->tp_flags & Py_TPFLAGS_HAVE_CLASS) {
        COPYSLOT(tp_descr_get);
        COPYSLOT(tp_descr_set);
        COPYSLOT(tp_dictoffset);
        COPYSLOT(tp_init);
        COPYSLOT(tp_alloc);
        COPYSLOT(tp_is_gc);
        Din_Go(2195,2048);if ((type->tp_flags & Py_TPFLAGS_HAVE_GC) ==
            (base->tp_flags & Py_TPFLAGS_HAVE_GC)) {
            /* They agree about gc. */
            COPYSLOT(tp_free);
        }
        else {/*533*/Din_Go(2193,2048);if ((type->tp_flags & Py_TPFLAGS_HAVE_GC) &&
                 type->tp_free == NULL &&
                 base->tp_free == _PyObject_Del) {
            /* A bit of magic to plug in the correct default
             * tp_free function when a derived class adds gc,
             * didn't define tp_free, and the base uses the
             * default non-gc tp_free.
             */
            Din_Go(2194,2048);type->tp_free = PyObject_GC_Del;
        ;/*534*/}}
        /* else they didn't agree about gc, and there isn't something
         * obvious to be done -- the type is on its own.
         */
    }
Din_Go(2197,2048);}

static int add_operators(PyTypeObject *);

int
PyType_Ready(PyTypeObject *type)
{
    Din_Go(2198,2048);PyObject *dict, *bases;
    PyTypeObject *base;
    Py_ssize_t i, n;

    Din_Go(2200,2048);if (type->tp_flags & Py_TPFLAGS_READY) {
        assert(type->tp_dict != NULL);
        {int  ReplaceReturn167 = 0; Din_Go(2199,2048); return ReplaceReturn167;}
    }
    assert((type->tp_flags & Py_TPFLAGS_READYING) == 0);

    Din_Go(2201,2048);type->tp_flags |= Py_TPFLAGS_READYING;

#ifdef Py_TRACE_REFS
    /* PyType_Ready is the closest thing we have to a choke point
     * for type objects, so is the best place I can think of to try
     * to get type objects into the doubly-linked list of all objects.
     * Still, not all type objects go thru PyType_Ready.
     */
    _Py_AddToAllObjects((PyObject *)type, 0);
#endif

    /* Initialize tp_base (defaults to BaseObject unless that's us) */
    base = type->tp_base;
    Din_Go(2203,2048);if (base == NULL && type != &PyBaseObject_Type) {
        Din_Go(2202,2048);base = type->tp_base = &PyBaseObject_Type;
        Py_INCREF(base);
    }

    /* Now the only way base can still be NULL is if type is
     * &PyBaseObject_Type.
     */

    /* Initialize the base class */
    Din_Go(2206,2048);if (base && base->tp_dict == NULL) {
        Din_Go(2204,2048);if (PyType_Ready(base) < 0)
            {/*7*/Din_Go(2205,2048);goto error;/*8*/}
    }

    /* Initialize ob_type if NULL.      This means extensions that want to be
       compilable separately on Windows can call PyType_Ready() instead of
       initializing the ob_type field of their type objects. */
    /* The test for base != NULL is really unnecessary, since base is only
       NULL when type is &PyBaseObject_Type, and we know its ob_type is
       not NULL (it's initialized to &PyType_Type).      But coverity doesn't
       know that. */
    Din_Go(2207,2048);if (Py_TYPE(type) == NULL && base != NULL)
        Py_TYPE(type) = Py_TYPE(base);

    /* Initialize tp_bases */
    Din_Go(2208,2048);bases = type->tp_bases;
    Din_Go(2215,2048);if (bases == NULL) {
        Din_Go(2209,2048);if (base == NULL)
            {/*9*/Din_Go(2210,2048);bases = PyTuple_New(0);/*10*/}
        else
            {/*11*/Din_Go(2211,2048);bases = PyTuple_Pack(1, base);/*12*/}
        Din_Go(2213,2048);if (bases == NULL)
            {/*13*/Din_Go(2212,2048);goto error;/*14*/}
        Din_Go(2214,2048);type->tp_bases = bases;
    }

    /* Initialize tp_dict */
    Din_Go(2216,2048);dict = type->tp_dict;
    Din_Go(2221,2048);if (dict == NULL) {
        Din_Go(2217,2048);dict = PyDict_New();
        Din_Go(2219,2048);if (dict == NULL)
            {/*15*/Din_Go(2218,2048);goto error;/*16*/}
        Din_Go(2220,2048);type->tp_dict = dict;
    }

    /* Add type-specific descriptors to tp_dict */
    Din_Go(2223,2048);if (add_operators(type) < 0)
        {/*17*/Din_Go(2222,2048);goto error;/*18*/}
    Din_Go(2226,2048);if (type->tp_methods != NULL) {
        Din_Go(2224,2048);if (add_methods(type, type->tp_methods) < 0)
            {/*19*/Din_Go(2225,2048);goto error;/*20*/}
    }
    Din_Go(2229,2048);if (type->tp_members != NULL) {
        Din_Go(2227,2048);if (add_members(type, type->tp_members) < 0)
            {/*21*/Din_Go(2228,2048);goto error;/*22*/}
    }
    Din_Go(2232,2048);if (type->tp_getset != NULL) {
        Din_Go(2230,2048);if (add_getset(type, type->tp_getset) < 0)
            {/*23*/Din_Go(2231,2048);goto error;/*24*/}
    }

    /* Calculate method resolution order */
    Din_Go(2234,2048);if (mro_internal(type) < 0) {
        Din_Go(2233,2048);goto error;
    }

    /* Inherit special flags from dominant base */
    Din_Go(2236,2048);if (type->tp_base != NULL)
        {/*25*/Din_Go(2235,2048);inherit_special(type, type->tp_base);/*26*/}

    /* Initialize tp_dict properly */
    Din_Go(2237,2048);bases = type->tp_mro;
    assert(bases != NULL);
    assert(PyTuple_Check(bases));
    n = PyTuple_GET_SIZE(bases);
    Din_Go(2241,2048);for (i = 1; i < n; i++) {
        Din_Go(2238,2048);PyObject *b = PyTuple_GET_ITEM(bases, i);
        Din_Go(2240,2048);if (PyType_Check(b))
            {/*27*/Din_Go(2239,2048);inherit_slots(type, (PyTypeObject *)b);/*28*/}
    }

    /* Sanity check for tp_free. */
    Din_Go(2244,2048);if (PyType_IS_GC(type) && (type->tp_flags & Py_TPFLAGS_BASETYPE) &&
        (type->tp_free == NULL || type->tp_free == PyObject_Del)) {
        /* This base class needs to call tp_free, but doesn't have
         * one, or its tp_free is for non-gc'ed objects.
         */
        Din_Go(2242,2048);PyErr_Format(PyExc_TypeError, "type '%.100s' participates in "
                     "gc and is a base type but has inappropriate "
                     "tp_free slot",
                     type->tp_name);
        Din_Go(2243,2048);goto error;
    }

    /* if the type dictionary doesn't contain a __doc__, set it from
       the tp_doc slot.
     */
    Din_Go(2251,2048);if (PyDict_GetItemString(type->tp_dict, "__doc__") == NULL) {
        Din_Go(2245,2048);if (type->tp_doc != NULL) {
            Din_Go(2246,2048);PyObject *doc = PyString_FromString(type->tp_doc);
            Din_Go(2248,2048);if (doc == NULL)
                {/*29*/Din_Go(2247,2048);goto error;/*30*/}
            Din_Go(2249,2048);PyDict_SetItemString(type->tp_dict, "__doc__", doc);
            Py_DECREF(doc);
        } else {
            Din_Go(2250,2048);PyDict_SetItemString(type->tp_dict,
                                 "__doc__", Py_None);
        }
    }

    /* Some more special stuff */
    Din_Go(2252,2048);base = type->tp_base;
    Din_Go(2261,2048);if (base != NULL) {
        Din_Go(2253,2048);if (type->tp_as_number == NULL)
            {/*31*/Din_Go(2254,2048);type->tp_as_number = base->tp_as_number;/*32*/}
        Din_Go(2256,2048);if (type->tp_as_sequence == NULL)
            {/*33*/Din_Go(2255,2048);type->tp_as_sequence = base->tp_as_sequence;/*34*/}
        Din_Go(2258,2048);if (type->tp_as_mapping == NULL)
            {/*35*/Din_Go(2257,2048);type->tp_as_mapping = base->tp_as_mapping;/*36*/}
        Din_Go(2260,2048);if (type->tp_as_buffer == NULL)
            {/*37*/Din_Go(2259,2048);type->tp_as_buffer = base->tp_as_buffer;/*38*/}
    }

    /* Link into each base class's list of subclasses */
    Din_Go(2262,2048);bases = type->tp_bases;
    n = PyTuple_GET_SIZE(bases);
    Din_Go(2266,2048);for (i = 0; i < n; i++) {
        Din_Go(2263,2048);PyObject *b = PyTuple_GET_ITEM(bases, i);
        Din_Go(2265,2048);if (PyType_Check(b) &&
            add_subclass((PyTypeObject *)b, type) < 0)
            {/*39*/Din_Go(2264,2048);goto error;/*40*/}
    }

    /* All done -- set the ready flag */
    assert(type->tp_dict != NULL);
    Din_Go(2267,2048);type->tp_flags =
        (type->tp_flags & ~Py_TPFLAGS_READYING) | Py_TPFLAGS_READY;
    {int  ReplaceReturn166 = 0; Din_Go(2268,2048); return ReplaceReturn166;}

  error:
    type->tp_flags &= ~Py_TPFLAGS_READYING;
    return -1;
}

static int
add_subclass(PyTypeObject *base, PyTypeObject *type)
{
    Din_Go(2269,2048);Py_ssize_t i;
    int result;
    PyObject *list, *ref, *newobj;

    list = base->tp_subclasses;
    Din_Go(2273,2048);if (list == NULL) {
        Din_Go(2270,2048);base->tp_subclasses = list = PyList_New(0);
        Din_Go(2272,2048);if (list == NULL)
            {/*107*/{int  ReplaceReturn165 = -1; Din_Go(2271,2048); return ReplaceReturn165;}/*108*/}
    }
    assert(PyList_Check(list));
    Din_Go(2274,2048);newobj = PyWeakref_NewRef((PyObject *)type, NULL);
    i = PyList_GET_SIZE(list);
    Din_Go(2278,2048);while (--i >= 0) {
        Din_Go(2275,2048);ref = PyList_GET_ITEM(list, i);
        assert(PyWeakref_CheckRef(ref));
        Din_Go(2277,2048);if (PyWeakref_GET_OBJECT(ref) == Py_None)
            {/*109*/{int  ReplaceReturn164 = PyList_SetItem(list, i, newobj); Din_Go(2276,2048); return ReplaceReturn164;}/*110*/}
    }
    Din_Go(2279,2048);result = PyList_Append(list, newobj);
    Py_DECREF(newobj);
    {int  ReplaceReturn163 = result; Din_Go(2280,2048); return ReplaceReturn163;}
}

static void
remove_subclass(PyTypeObject *base, PyTypeObject *type)
{
    Din_Go(2281,2048);Py_ssize_t i;
    PyObject *list, *ref;

    list = base->tp_subclasses;
    Din_Go(2283,2048);if (list == NULL) {
        Din_Go(2282,2048);return;
    }
    assert(PyList_Check(list));
    Din_Go(2284,2048);i = PyList_GET_SIZE(list);
    Din_Go(2289,2048);while (--i >= 0) {
        Din_Go(2285,2048);ref = PyList_GET_ITEM(list, i);
        assert(PyWeakref_CheckRef(ref));
        Din_Go(2288,2048);if (PyWeakref_GET_OBJECT(ref) == (PyObject*)type) {
            /* this can't fail, right? */
            Din_Go(2286,2048);PySequence_DelItem(list, i);
            Din_Go(2287,2048);return;
        }
    }
Din_Go(2290,2048);}

static int
check_num_args(PyObject *ob, int n)
{
    Din_Go(2291,2048);if (!PyTuple_CheckExact(ob)) {
        Din_Go(2292,2048);PyErr_SetString(PyExc_SystemError,
            "PyArg_UnpackTuple() argument list is not a tuple");
        {int  ReplaceReturn162 = 0; Din_Go(2293,2048); return ReplaceReturn162;}
    }
    Din_Go(2295,2048);if (n == PyTuple_GET_SIZE(ob))
        {/*549*/{int  ReplaceReturn161 = 1; Din_Go(2294,2048); return ReplaceReturn161;}/*550*/}
    Din_Go(2296,2048);PyErr_Format(
        PyExc_TypeError,
        "expected %d arguments, got %zd", n, PyTuple_GET_SIZE(ob));
    {int  ReplaceReturn160 = 0; Din_Go(2297,2048); return ReplaceReturn160;}
}

/* Generic wrappers for overloadable 'operators' such as __getitem__ */

/* There's a wrapper *function* for each distinct function typedef used
   for type object slots (e.g. binaryfunc, ternaryfunc, etc.).  There's a
   wrapper *table* for each distinct operation (e.g. __len__, __add__).
   Most tables have only one entry; the tables for binary operators have two
   entries, one regular and one with reversed arguments. */

static PyObject *
wrap_lenfunc(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2298,2048);lenfunc func = (lenfunc)wrapped;
    Py_ssize_t res;

    Din_Go(2299,2048);if (!check_num_args(args, 0))
        return NULL;
    Din_Go(2300,2048);res = (*func)(self);
    Din_Go(2301,2048);if (res == -1 && PyErr_Occurred())
        return NULL;
    {PyObject * ReplaceReturn159 = PyInt_FromLong((long)res); Din_Go(2302,2048); return ReplaceReturn159;}
}

static PyObject *
wrap_inquirypred(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2303,2048);inquiry func = (inquiry)wrapped;
    int res;

    Din_Go(2304,2048);if (!check_num_args(args, 0))
        return NULL;
    Din_Go(2305,2048);res = (*func)(self);
    Din_Go(2306,2048);if (res == -1 && PyErr_Occurred())
        return NULL;
    {PyObject * ReplaceReturn158 = PyBool_FromLong((long)res); Din_Go(2307,2048); return ReplaceReturn158;}
}

static PyObject *
wrap_binaryfunc(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2308,2048);binaryfunc func = (binaryfunc)wrapped;
    PyObject *other;

    Din_Go(2309,2048);if (!check_num_args(args, 1))
        return NULL;
    Din_Go(2310,2048);other = PyTuple_GET_ITEM(args, 0);
    {PyObject * ReplaceReturn157 = (*func)(self, other); Din_Go(2311,2048); return ReplaceReturn157;}
}

static PyObject *
wrap_binaryfunc_l(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2312,2048);binaryfunc func = (binaryfunc)wrapped;
    PyObject *other;

    Din_Go(2313,2048);if (!check_num_args(args, 1))
        return NULL;
    Din_Go(2314,2048);other = PyTuple_GET_ITEM(args, 0);
    Din_Go(2316,2048);if (!(self->ob_type->tp_flags & Py_TPFLAGS_CHECKTYPES) &&
        !PyType_IsSubtype(other->ob_type, self->ob_type)) {
        Py_INCREF(Py_NotImplemented);
        {PyObject * ReplaceReturn156 = Py_NotImplemented; Din_Go(2315,2048); return ReplaceReturn156;}
    }
    {PyObject * ReplaceReturn155 = (*func)(self, other); Din_Go(2317,2048); return ReplaceReturn155;}
}

static PyObject *
wrap_binaryfunc_r(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2318,2048);binaryfunc func = (binaryfunc)wrapped;
    PyObject *other;

    Din_Go(2319,2048);if (!check_num_args(args, 1))
        return NULL;
    Din_Go(2320,2048);other = PyTuple_GET_ITEM(args, 0);
    Din_Go(2322,2048);if (!(self->ob_type->tp_flags & Py_TPFLAGS_CHECKTYPES) &&
        !PyType_IsSubtype(other->ob_type, self->ob_type)) {
        Py_INCREF(Py_NotImplemented);
        {PyObject * ReplaceReturn154 = Py_NotImplemented; Din_Go(2321,2048); return ReplaceReturn154;}
    }
    {PyObject * ReplaceReturn153 = (*func)(other, self); Din_Go(2323,2048); return ReplaceReturn153;}
}

static PyObject *
wrap_coercefunc(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2324,2048);coercion func = (coercion)wrapped;
    PyObject *other, *res;
    int ok;

    Din_Go(2325,2048);if (!check_num_args(args, 1))
        return NULL;
    Din_Go(2326,2048);other = PyTuple_GET_ITEM(args, 0);
    ok = func(&self, &other);
    Din_Go(2327,2048);if (ok < 0)
        return NULL;
    Din_Go(2329,2048);if (ok > 0) {
        Py_INCREF(Py_NotImplemented);
        {PyObject * ReplaceReturn152 = Py_NotImplemented; Din_Go(2328,2048); return ReplaceReturn152;}
    }
    Din_Go(2330,2048);res = PyTuple_New(2);
    Din_Go(2332,2048);if (res == NULL) {
        Py_DECREF(self);
        Py_DECREF(other);
        {PyObject * ReplaceReturn151 = NULL; Din_Go(2331,2048); return ReplaceReturn151;}
    }
    PyTuple_SET_ITEM(res, 0, self);
    PyTuple_SET_ITEM(res, 1, other);
    {PyObject * ReplaceReturn150 = res; Din_Go(2333,2048); return ReplaceReturn150;}
}

static PyObject *
wrap_ternaryfunc(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2334,2048);ternaryfunc func = (ternaryfunc)wrapped;
    PyObject *other;
    PyObject *third = Py_None;

    /* Note: This wrapper only works for __pow__() */

    Din_Go(2335,2048);if (!PyArg_UnpackTuple(args, "", 1, 2, &other, &third))
        return NULL;
    {PyObject * ReplaceReturn149 = (*func)(self, other, third); Din_Go(2336,2048); return ReplaceReturn149;}
}

static PyObject *
wrap_ternaryfunc_r(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2337,2048);ternaryfunc func = (ternaryfunc)wrapped;
    PyObject *other;
    PyObject *third = Py_None;

    /* Note: This wrapper only works for __pow__() */

    Din_Go(2338,2048);if (!PyArg_UnpackTuple(args, "", 1, 2, &other, &third))
        return NULL;
    {PyObject * ReplaceReturn148 = (*func)(other, self, third); Din_Go(2339,2048); return ReplaceReturn148;}
}

static PyObject *
wrap_unaryfunc(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2340,2048);unaryfunc func = (unaryfunc)wrapped;

    Din_Go(2341,2048);if (!check_num_args(args, 0))
        return NULL;
    {PyObject * ReplaceReturn147 = (*func)(self); Din_Go(2342,2048); return ReplaceReturn147;}
}

static PyObject *
wrap_indexargfunc(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2343,2048);ssizeargfunc func = (ssizeargfunc)wrapped;
    PyObject* o;
    Py_ssize_t i;

    Din_Go(2344,2048);if (!PyArg_UnpackTuple(args, "", 1, 1, &o))
        return NULL;
    Din_Go(2345,2048);i = PyNumber_AsSsize_t(o, PyExc_OverflowError);
    Din_Go(2346,2048);if (i == -1 && PyErr_Occurred())
        return NULL;
    {PyObject * ReplaceReturn146 = (*func)(self, i); Din_Go(2347,2048); return ReplaceReturn146;}
}

static Py_ssize_t
getindex(PyObject *self, PyObject *arg)
{
    Din_Go(2348,2048);Py_ssize_t i;

    i = PyNumber_AsSsize_t(arg, PyExc_OverflowError);
    Din_Go(2350,2048);if (i == -1 && PyErr_Occurred())
        {/*551*/{Py_ssize_t  ReplaceReturn145 = -1; Din_Go(2349,2048); return ReplaceReturn145;}/*552*/}
    Din_Go(2357,2048);if (i < 0) {
        Din_Go(2351,2048);PySequenceMethods *sq = Py_TYPE(self)->tp_as_sequence;
        Din_Go(2356,2048);if (sq && sq->sq_length) {
            Din_Go(2352,2048);Py_ssize_t n = (*sq->sq_length)(self);
            Din_Go(2354,2048);if (n < 0)
                {/*553*/{Py_ssize_t  ReplaceReturn144 = -1; Din_Go(2353,2048); return ReplaceReturn144;}/*554*/}
            Din_Go(2355,2048);i += n;
        }
    }
    {Py_ssize_t  ReplaceReturn143 = i; Din_Go(2358,2048); return ReplaceReturn143;}
}

static PyObject *
wrap_sq_item(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2359,2048);ssizeargfunc func = (ssizeargfunc)wrapped;
    PyObject *arg;
    Py_ssize_t i;

    Din_Go(2363,2048);if (PyTuple_GET_SIZE(args) == 1) {
        Din_Go(2360,2048);arg = PyTuple_GET_ITEM(args, 0);
        i = getindex(self, arg);
        Din_Go(2361,2048);if (i == -1 && PyErr_Occurred())
            return NULL;
        {PyObject * ReplaceReturn142 = (*func)(self, i); Din_Go(2362,2048); return ReplaceReturn142;}
    }
    Din_Go(2364,2048);check_num_args(args, 1);
    assert(PyErr_Occurred());
    {PyObject * ReplaceReturn141 = NULL; Din_Go(2365,2048); return ReplaceReturn141;}
}

static PyObject *
wrap_ssizessizeargfunc(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2366,2048);ssizessizeargfunc func = (ssizessizeargfunc)wrapped;
    Py_ssize_t i, j;

    Din_Go(2367,2048);if (!PyArg_ParseTuple(args, "nn", &i, &j))
        return NULL;
    {PyObject * ReplaceReturn140 = (*func)(self, i, j); Din_Go(2368,2048); return ReplaceReturn140;}
}

static PyObject *
wrap_sq_setitem(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2369,2048);ssizeobjargproc func = (ssizeobjargproc)wrapped;
    Py_ssize_t i;
    int res;
    PyObject *arg, *value;

    Din_Go(2370,2048);if (!PyArg_UnpackTuple(args, "", 2, 2, &arg, &value))
        return NULL;
    Din_Go(2371,2048);i = getindex(self, arg);
    Din_Go(2372,2048);if (i == -1 && PyErr_Occurred())
        return NULL;
    Din_Go(2373,2048);res = (*func)(self, i, value);
    Din_Go(2374,2048);if (res == -1 && PyErr_Occurred())
        return NULL;
    Py_INCREF(Py_None);
    {PyObject * ReplaceReturn139 = Py_None; Din_Go(2375,2048); return ReplaceReturn139;}
}

static PyObject *
wrap_sq_delitem(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2376,2048);ssizeobjargproc func = (ssizeobjargproc)wrapped;
    Py_ssize_t i;
    int res;
    PyObject *arg;

    Din_Go(2377,2048);if (!check_num_args(args, 1))
        return NULL;
    Din_Go(2378,2048);arg = PyTuple_GET_ITEM(args, 0);
    i = getindex(self, arg);
    Din_Go(2379,2048);if (i == -1 && PyErr_Occurred())
        return NULL;
    Din_Go(2380,2048);res = (*func)(self, i, NULL);
    Din_Go(2381,2048);if (res == -1 && PyErr_Occurred())
        return NULL;
    Py_INCREF(Py_None);
    {PyObject * ReplaceReturn138 = Py_None; Din_Go(2382,2048); return ReplaceReturn138;}
}

static PyObject *
wrap_ssizessizeobjargproc(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2383,2048);ssizessizeobjargproc func = (ssizessizeobjargproc)wrapped;
    Py_ssize_t i, j;
    int res;
    PyObject *value;

    Din_Go(2384,2048);if (!PyArg_ParseTuple(args, "nnO", &i, &j, &value))
        return NULL;
    Din_Go(2385,2048);res = (*func)(self, i, j, value);
    Din_Go(2386,2048);if (res == -1 && PyErr_Occurred())
        return NULL;
    Py_INCREF(Py_None);
    {PyObject * ReplaceReturn137 = Py_None; Din_Go(2387,2048); return ReplaceReturn137;}
}

static PyObject *
wrap_delslice(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2388,2048);ssizessizeobjargproc func = (ssizessizeobjargproc)wrapped;
    Py_ssize_t i, j;
    int res;

    Din_Go(2389,2048);if (!PyArg_ParseTuple(args, "nn", &i, &j))
        return NULL;
    Din_Go(2390,2048);res = (*func)(self, i, j, NULL);
    Din_Go(2391,2048);if (res == -1 && PyErr_Occurred())
        return NULL;
    Py_INCREF(Py_None);
    {PyObject * ReplaceReturn136 = Py_None; Din_Go(2392,2048); return ReplaceReturn136;}
}

/* XXX objobjproc is a misnomer; should be objargpred */
static PyObject *
wrap_objobjproc(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2393,2048);objobjproc func = (objobjproc)wrapped;
    int res;
    PyObject *value;

    Din_Go(2394,2048);if (!check_num_args(args, 1))
        return NULL;
    Din_Go(2395,2048);value = PyTuple_GET_ITEM(args, 0);
    res = (*func)(self, value);
    Din_Go(2397,2048);if (res == -1 && PyErr_Occurred())
        return NULL;
    else
        {/*555*/{PyObject * ReplaceReturn135 = PyBool_FromLong(res); Din_Go(2396,2048); return ReplaceReturn135;}/*556*/}
Din_Go(2398,2048);}

static PyObject *
wrap_objobjargproc(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2399,2048);objobjargproc func = (objobjargproc)wrapped;
    int res;
    PyObject *key, *value;

    Din_Go(2400,2048);if (!PyArg_UnpackTuple(args, "", 2, 2, &key, &value))
        return NULL;
    Din_Go(2401,2048);res = (*func)(self, key, value);
    Din_Go(2402,2048);if (res == -1 && PyErr_Occurred())
        return NULL;
    Py_INCREF(Py_None);
    {PyObject * ReplaceReturn134 = Py_None; Din_Go(2403,2048); return ReplaceReturn134;}
}

static PyObject *
wrap_delitem(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2404,2048);objobjargproc func = (objobjargproc)wrapped;
    int res;
    PyObject *key;

    Din_Go(2405,2048);if (!check_num_args(args, 1))
        return NULL;
    Din_Go(2406,2048);key = PyTuple_GET_ITEM(args, 0);
    res = (*func)(self, key, NULL);
    Din_Go(2407,2048);if (res == -1 && PyErr_Occurred())
        return NULL;
    Py_INCREF(Py_None);
    {PyObject * ReplaceReturn133 = Py_None; Din_Go(2408,2048); return ReplaceReturn133;}
}

static PyObject *
wrap_cmpfunc(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2409,2048);cmpfunc func = (cmpfunc)wrapped;
    int res;
    PyObject *other;

    Din_Go(2410,2048);if (!check_num_args(args, 1))
        return NULL;
    Din_Go(2411,2048);other = PyTuple_GET_ITEM(args, 0);
    Din_Go(2414,2048);if (Py_TYPE(other)->tp_compare != func &&
        !PyType_IsSubtype(Py_TYPE(other), Py_TYPE(self))) {
        Din_Go(2412,2048);PyErr_Format(
            PyExc_TypeError,
            "%s.__cmp__(x,y) requires y to be a '%s', not a '%s'",
            Py_TYPE(self)->tp_name,
            Py_TYPE(self)->tp_name,
            Py_TYPE(other)->tp_name);
        {PyObject * ReplaceReturn132 = NULL; Din_Go(2413,2048); return ReplaceReturn132;}
    }
    Din_Go(2415,2048);res = (*func)(self, other);
    Din_Go(2416,2048);if (PyErr_Occurred())
        return NULL;
    {PyObject * ReplaceReturn131 = PyInt_FromLong((long)res); Din_Go(2417,2048); return ReplaceReturn131;}
}

/* Helper to check for object.__setattr__ or __delattr__ applied to a type.
   This is called the Carlo Verre hack after its discoverer. */
static int
hackcheck(PyObject *self, setattrofunc func, char *what)
{
    Din_Go(2418,2048);PyTypeObject *type = Py_TYPE(self);
    Din_Go(2420,2048);while (type && type->tp_flags & Py_TPFLAGS_HEAPTYPE)
        {/*557*/Din_Go(2419,2048);type = type->tp_base;/*558*/}
    /* If type is NULL now, this is a really weird type.
       In the spirit of backwards compatibility (?), just shut up. */
    Din_Go(2423,2048);if (type && type->tp_setattro != func) {
        Din_Go(2421,2048);PyErr_Format(PyExc_TypeError,
                     "can't apply this %s to %s object",
                     what,
                     type->tp_name);
        {int  ReplaceReturn130 = 0; Din_Go(2422,2048); return ReplaceReturn130;}
    }
    {int  ReplaceReturn129 = 1; Din_Go(2424,2048); return ReplaceReturn129;}
}

static PyObject *
wrap_setattr(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2425,2048);setattrofunc func = (setattrofunc)wrapped;
    int res;
    PyObject *name, *value;

    Din_Go(2426,2048);if (!PyArg_UnpackTuple(args, "", 2, 2, &name, &value))
        return NULL;
    Din_Go(2427,2048);if (!hackcheck(self, func, "__setattr__"))
        return NULL;
    Din_Go(2428,2048);res = (*func)(self, name, value);
    Din_Go(2429,2048);if (res < 0)
        return NULL;
    Py_INCREF(Py_None);
    {PyObject * ReplaceReturn128 = Py_None; Din_Go(2430,2048); return ReplaceReturn128;}
}

static PyObject *
wrap_delattr(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2431,2048);setattrofunc func = (setattrofunc)wrapped;
    int res;
    PyObject *name;

    Din_Go(2432,2048);if (!check_num_args(args, 1))
        return NULL;
    Din_Go(2433,2048);name = PyTuple_GET_ITEM(args, 0);
    Din_Go(2434,2048);if (!hackcheck(self, func, "__delattr__"))
        return NULL;
    Din_Go(2435,2048);res = (*func)(self, name, NULL);
    Din_Go(2436,2048);if (res < 0)
        return NULL;
    Py_INCREF(Py_None);
    {PyObject * ReplaceReturn127 = Py_None; Din_Go(2437,2048); return ReplaceReturn127;}
}

static PyObject *
wrap_hashfunc(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2438,2048);hashfunc func = (hashfunc)wrapped;
    long res;

    Din_Go(2439,2048);if (!check_num_args(args, 0))
        return NULL;
    Din_Go(2440,2048);res = (*func)(self);
    Din_Go(2441,2048);if (res == -1 && PyErr_Occurred())
        return NULL;
    {PyObject * ReplaceReturn126 = PyInt_FromLong(res); Din_Go(2442,2048); return ReplaceReturn126;}
}

static PyObject *
wrap_call(PyObject *self, PyObject *args, void *wrapped, PyObject *kwds)
{
    Din_Go(2443,2048);ternaryfunc func = (ternaryfunc)wrapped;

    {PyObject * ReplaceReturn125 = (*func)(self, args, kwds); Din_Go(2444,2048); return ReplaceReturn125;}
}

static PyObject *
wrap_richcmpfunc(PyObject *self, PyObject *args, void *wrapped, int op)
{
    Din_Go(2445,2048);richcmpfunc func = (richcmpfunc)wrapped;
    PyObject *other;

    Din_Go(2446,2048);if (!check_num_args(args, 1))
        return NULL;
    Din_Go(2447,2048);other = PyTuple_GET_ITEM(args, 0);
    {PyObject * ReplaceReturn124 = (*func)(self, other, op); Din_Go(2448,2048); return ReplaceReturn124;}
}

#undef RICHCMP_WRAPPER
#define RICHCMP_WRAPPER(NAME, OP) \
static PyObject * \
richcmp_##NAME(PyObject *self, PyObject *args, void *wrapped) \
{ \
    return wrap_richcmpfunc(self, args, wrapped, OP); \
}

RICHCMP_WRAPPER(lt, Py_LT)
RICHCMP_WRAPPER(le, Py_LE)
RICHCMP_WRAPPER(eq, Py_EQ)
RICHCMP_WRAPPER(ne, Py_NE)
RICHCMP_WRAPPER(gt, Py_GT)
RICHCMP_WRAPPER(ge, Py_GE)

static PyObject *
wrap_next(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2450,2048);unaryfunc func = (unaryfunc)wrapped;
    PyObject *res;

    Din_Go(2451,2048);if (!check_num_args(args, 0))
        return NULL;
    Din_Go(2452,2048);res = (*func)(self);
    Din_Go(2454,2048);if (res == NULL && !PyErr_Occurred())
        {/*559*/Din_Go(2453,2048);PyErr_SetNone(PyExc_StopIteration);/*560*/}
    {PyObject * ReplaceReturn123 = res; Din_Go(2455,2048); return ReplaceReturn123;}
}

static PyObject *
wrap_descr_get(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2456,2048);descrgetfunc func = (descrgetfunc)wrapped;
    PyObject *obj;
    PyObject *type = NULL;

    Din_Go(2457,2048);if (!PyArg_UnpackTuple(args, "", 1, 2, &obj, &type))
        return NULL;
    Din_Go(2458,2048);if (obj == Py_None)
        obj = NULL;
    Din_Go(2459,2048);if (type == Py_None)
        type = NULL;
    Din_Go(2462,2048);if (type == NULL &&obj == NULL) {
        Din_Go(2460,2048);PyErr_SetString(PyExc_TypeError,
                        "__get__(None, None) is invalid");
        {PyObject * ReplaceReturn122 = NULL; Din_Go(2461,2048); return ReplaceReturn122;}
    }
    {PyObject * ReplaceReturn121 = (*func)(self, obj, type); Din_Go(2463,2048); return ReplaceReturn121;}
}

static PyObject *
wrap_descr_set(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2464,2048);descrsetfunc func = (descrsetfunc)wrapped;
    PyObject *obj, *value;
    int ret;

    Din_Go(2465,2048);if (!PyArg_UnpackTuple(args, "", 2, 2, &obj, &value))
        return NULL;
    Din_Go(2466,2048);ret = (*func)(self, obj, value);
    Din_Go(2467,2048);if (ret < 0)
        return NULL;
    Py_INCREF(Py_None);
    {PyObject * ReplaceReturn120 = Py_None; Din_Go(2468,2048); return ReplaceReturn120;}
}

static PyObject *
wrap_descr_delete(PyObject *self, PyObject *args, void *wrapped)
{
    Din_Go(2469,2048);descrsetfunc func = (descrsetfunc)wrapped;
    PyObject *obj;
    int ret;

    Din_Go(2470,2048);if (!check_num_args(args, 1))
        return NULL;
    Din_Go(2471,2048);obj = PyTuple_GET_ITEM(args, 0);
    ret = (*func)(self, obj, NULL);
    Din_Go(2472,2048);if (ret < 0)
        return NULL;
    Py_INCREF(Py_None);
    {PyObject * ReplaceReturn119 = Py_None; Din_Go(2473,2048); return ReplaceReturn119;}
}

static PyObject *
wrap_init(PyObject *self, PyObject *args, void *wrapped, PyObject *kwds)
{
    Din_Go(2474,2048);initproc func = (initproc)wrapped;

    Din_Go(2475,2048);if (func(self, args, kwds) < 0)
        return NULL;
    Py_INCREF(Py_None);
    {PyObject * ReplaceReturn118 = Py_None; Din_Go(2476,2048); return ReplaceReturn118;}
}

static PyObject *
tp_new_wrapper(PyObject *self, PyObject *args, PyObject *kwds)
{
    Din_Go(2477,2048);PyTypeObject *type, *subtype, *staticbase;
    PyObject *arg0, *res;

    Din_Go(2479,2048);if (self == NULL || !PyType_Check(self))
        {/*561*/Din_Go(2478,2048);Py_FatalError("__new__() called with non-type 'self'");/*562*/}
    Din_Go(2480,2048);type = (PyTypeObject *)self;
    Din_Go(2483,2048);if (!PyTuple_Check(args) || PyTuple_GET_SIZE(args) < 1) {
        Din_Go(2481,2048);PyErr_Format(PyExc_TypeError,
                     "%s.__new__(): not enough arguments",
                     type->tp_name);
        {PyObject * ReplaceReturn117 = NULL; Din_Go(2482,2048); return ReplaceReturn117;}
    }
    Din_Go(2484,2048);arg0 = PyTuple_GET_ITEM(args, 0);
    Din_Go(2487,2048);if (!PyType_Check(arg0)) {
        Din_Go(2485,2048);PyErr_Format(PyExc_TypeError,
                     "%s.__new__(X): X is not a type object (%s)",
                     type->tp_name,
                     Py_TYPE(arg0)->tp_name);
        {PyObject * ReplaceReturn116 = NULL; Din_Go(2486,2048); return ReplaceReturn116;}
    }
    Din_Go(2488,2048);subtype = (PyTypeObject *)arg0;
    Din_Go(2491,2048);if (!PyType_IsSubtype(subtype, type)) {
        Din_Go(2489,2048);PyErr_Format(PyExc_TypeError,
                     "%s.__new__(%s): %s is not a subtype of %s",
                     type->tp_name,
                     subtype->tp_name,
                     subtype->tp_name,
                     type->tp_name);
        {PyObject * ReplaceReturn115 = NULL; Din_Go(2490,2048); return ReplaceReturn115;}
    }

    /* Check that the use doesn't do something silly and unsafe like
       object.__new__(dict).  To do this, we check that the
       most derived base that's not a heap type is this type. */
    Din_Go(2492,2048);staticbase = subtype;
    Din_Go(2494,2048);while (staticbase && (staticbase->tp_flags & Py_TPFLAGS_HEAPTYPE))
        {/*563*/Din_Go(2493,2048);staticbase = staticbase->tp_base;/*564*/}
    /* If staticbase is NULL now, it is a really weird type.
       In the spirit of backwards compatibility (?), just shut up. */
    Din_Go(2497,2048);if (staticbase && staticbase->tp_new != type->tp_new) {
        Din_Go(2495,2048);PyErr_Format(PyExc_TypeError,
                     "%s.__new__(%s) is not safe, use %s.__new__()",
                     type->tp_name,
                     subtype->tp_name,
                     staticbase == NULL ? "?" : staticbase->tp_name);
        {PyObject * ReplaceReturn114 = NULL; Din_Go(2496,2048); return ReplaceReturn114;}
    }

    Din_Go(2498,2048);args = PyTuple_GetSlice(args, 1, PyTuple_GET_SIZE(args));
    Din_Go(2499,2048);if (args == NULL)
        return NULL;
    Din_Go(2500,2048);res = type->tp_new(subtype, args, kwds);
    Py_DECREF(args);
    {PyObject * ReplaceReturn113 = res; Din_Go(2501,2048); return ReplaceReturn113;}
}

static struct PyMethodDef tp_new_methoddef[] = {
    {"__new__", (PyCFunction)tp_new_wrapper, METH_VARARGS|METH_KEYWORDS,
     PyDoc_STR("T.__new__(S, ...) -> "
               "a new object with type S, a subtype of T")},
    {0}
};

static int
add_tp_new_wrapper(PyTypeObject *type)
{
    Din_Go(2502,2048);PyObject *func;

    Din_Go(2504,2048);if (PyDict_GetItemString(type->tp_dict, "__new__") != NULL)
        {/*565*/{int  ReplaceReturn112 = 0; Din_Go(2503,2048); return ReplaceReturn112;}/*566*/}
    Din_Go(2505,2048);func = PyCFunction_New(tp_new_methoddef, (PyObject *)type);
    Din_Go(2507,2048);if (func == NULL)
        {/*567*/{int  ReplaceReturn111 = -1; Din_Go(2506,2048); return ReplaceReturn111;}/*568*/}
    Din_Go(2509,2048);if (PyDict_SetItemString(type->tp_dict, "__new__", func)) {
        Py_DECREF(func);
        {int  ReplaceReturn110 = -1; Din_Go(2508,2048); return ReplaceReturn110;}
    }
    Py_DECREF(func);
    {int  ReplaceReturn109 = 0; Din_Go(2510,2048); return ReplaceReturn109;}
}

/* Slot wrappers that call the corresponding __foo__ slot.  See comments
   below at override_slots() for more explanation. */

#define SLOT0(FUNCNAME, OPSTR) \
static PyObject * \
FUNCNAME(PyObject *self) \
{ \
    static PyObject *cache_str; \
    return call_method(self, OPSTR, &cache_str, "()"); \
}

#define SLOT1(FUNCNAME, OPSTR, ARG1TYPE, ARGCODES) \
static PyObject * \
FUNCNAME(PyObject *self, ARG1TYPE arg1) \
{ \
    static PyObject *cache_str; \
    return call_method(self, OPSTR, &cache_str, "(" ARGCODES ")", arg1); \
}

/* Boolean helper for SLOT1BINFULL().
   right.__class__ is a nontrivial subclass of left.__class__. */
static int
method_is_overloaded(PyObject *left, PyObject *right, char *name)
{
    Din_Go(2515,2048);PyObject *a, *b;
    int ok;

    b = PyObject_GetAttrString((PyObject *)(Py_TYPE(right)), name);
    Din_Go(2518,2048);if (b == NULL) {
        Din_Go(2516,2048);PyErr_Clear();
        /* If right doesn't have it, it's not overloaded */
        {int  ReplaceReturn108 = 0; Din_Go(2517,2048); return ReplaceReturn108;}
    }

    Din_Go(2519,2048);a = PyObject_GetAttrString((PyObject *)(Py_TYPE(left)), name);
    Din_Go(2522,2048);if (a == NULL) {
        Din_Go(2520,2048);PyErr_Clear();
        Py_DECREF(b);
        /* If right has it but left doesn't, it's overloaded */
        {int  ReplaceReturn107 = 1; Din_Go(2521,2048); return ReplaceReturn107;}
    }

    Din_Go(2523,2048);ok = PyObject_RichCompareBool(a, b, Py_NE);
    Py_DECREF(a);
    Py_DECREF(b);
    Din_Go(2526,2048);if (ok < 0) {
        Din_Go(2524,2048);PyErr_Clear();
        {int  ReplaceReturn106 = 0; Din_Go(2525,2048); return ReplaceReturn106;}
    }

    {int  ReplaceReturn105 = ok; Din_Go(2527,2048); return ReplaceReturn105;}
}


#define SLOT1BINFULL(FUNCNAME, TESTFUNC, SLOTNAME, OPSTR, ROPSTR) \
static PyObject * \
FUNCNAME(PyObject *self, PyObject *other) \
{ \
    static PyObject *cache_str, *rcache_str; \
    int do_other = Py_TYPE(self) != Py_TYPE(other) && \
        Py_TYPE(other)->tp_as_number != NULL && \
        Py_TYPE(other)->tp_as_number->SLOTNAME == TESTFUNC; \
    if (Py_TYPE(self)->tp_as_number != NULL && \
        Py_TYPE(self)->tp_as_number->SLOTNAME == TESTFUNC) { \
        PyObject *r; \
        if (do_other && \
            PyType_IsSubtype(Py_TYPE(other), Py_TYPE(self)) && \
            method_is_overloaded(self, other, ROPSTR)) { \
            r = call_maybe( \
                other, ROPSTR, &rcache_str, "(O)", self); \
            if (r != Py_NotImplemented) \
                return r; \
            Py_DECREF(r); \
            do_other = 0; \
        } \
        r = call_maybe( \
            self, OPSTR, &cache_str, "(O)", other); \
        if (r != Py_NotImplemented || \
            Py_TYPE(other) == Py_TYPE(self)) \
            return r; \
        Py_DECREF(r); \
    } \
    if (do_other) { \
        return call_maybe( \
            other, ROPSTR, &rcache_str, "(O)", self); \
    } \
    Py_INCREF(Py_NotImplemented); \
    return Py_NotImplemented; \
}

#define SLOT1BIN(FUNCNAME, SLOTNAME, OPSTR, ROPSTR) \
    SLOT1BINFULL(FUNCNAME, FUNCNAME, SLOTNAME, OPSTR, ROPSTR)

#define SLOT2(FUNCNAME, OPSTR, ARG1TYPE, ARG2TYPE, ARGCODES) \
static PyObject * \
FUNCNAME(PyObject *self, ARG1TYPE arg1, ARG2TYPE arg2) \
{ \
    static PyObject *cache_str; \
    return call_method(self, OPSTR, &cache_str, \
                       "(" ARGCODES ")", arg1, arg2); \
}

static Py_ssize_t
slot_sq_length(PyObject *self)
{
    Din_Go(2542,2048);static PyObject *len_str;
    PyObject *res = call_method(self, "__len__", &len_str, "()");
    Py_ssize_t len;

    Din_Go(2544,2048);if (res == NULL)
        {/*569*/{Py_ssize_t  ReplaceReturn104 = -1; Din_Go(2543,2048); return ReplaceReturn104;}/*570*/}
    Din_Go(2545,2048);len = PyInt_AsSsize_t(res);
    Py_DECREF(res);
    Din_Go(2549,2048);if (len < 0) {
        Din_Go(2546,2048);if (!PyErr_Occurred())
            {/*571*/Din_Go(2547,2048);PyErr_SetString(PyExc_ValueError,
                            "__len__() should return >= 0");/*572*/}
        {Py_ssize_t  ReplaceReturn103 = -1; Din_Go(2548,2048); return ReplaceReturn103;}
    }
    {Py_ssize_t  ReplaceReturn102 = len; Din_Go(2550,2048); return ReplaceReturn102;}
}

/* Super-optimized version of slot_sq_item.
   Other slots could do the same... */
static PyObject *
slot_sq_item(PyObject *self, Py_ssize_t i)
{
    Din_Go(2551,2048);static PyObject *getitem_str;
    PyObject *func, *args = NULL, *ival = NULL, *retval = NULL;
    descrgetfunc f;

    Din_Go(2554,2048);if (getitem_str == NULL) {
        Din_Go(2552,2048);getitem_str = PyString_InternFromString("__getitem__");
        Din_Go(2553,2048);if (getitem_str == NULL)
            return NULL;
    }
    Din_Go(2555,2048);func = _PyType_Lookup(Py_TYPE(self), getitem_str);
    Din_Go(2567,2048);if (func != NULL) {
        Din_Go(2556,2048);if ((f = Py_TYPE(func)->tp_descr_get) == NULL)
            Py_INCREF(func);
        else {
            Din_Go(2557,2048);func = f(func, self, (PyObject *)(Py_TYPE(self)));
            Din_Go(2559,2048);if (func == NULL) {
                {PyObject * ReplaceReturn101 = NULL; Din_Go(2558,2048); return ReplaceReturn101;}
            }
        }
        Din_Go(2560,2048);ival = PyInt_FromSsize_t(i);
        Din_Go(2565,2048);if (ival != NULL) {
            Din_Go(2561,2048);args = PyTuple_New(1);
            Din_Go(2564,2048);if (args != NULL) {
                PyTuple_SET_ITEM(args, 0, ival);
                Din_Go(2562,2048);retval = PyObject_Call(func, args, NULL);
                Py_XDECREF(args);
                Py_XDECREF(func);
                {PyObject * ReplaceReturn100 = retval; Din_Go(2563,2048); return ReplaceReturn100;}
            }
        }
    }
    else {
        Din_Go(2566,2048);PyErr_SetObject(PyExc_AttributeError, getitem_str);
    }
    Py_XDECREF(args);
    Py_XDECREF(ival);
    Py_XDECREF(func);
    {PyObject * ReplaceReturn99 = NULL; Din_Go(2568,2048); return ReplaceReturn99;}
}

static PyObject*
slot_sq_slice(PyObject *self, Py_ssize_t i, Py_ssize_t j)
{
    Din_Go(2569,2048);static PyObject *getslice_str;

    Din_Go(2570,2048);if (PyErr_WarnPy3k("in 3.x, __getslice__ has been removed; "
                        "use __getitem__", 1) < 0)
        return NULL;
    {PyObject * ReplaceReturn98 = call_method(self, "__getslice__", &getslice_str,
        "nn", i, j); Din_Go(2571,2048); return ReplaceReturn98;}
}

static int
slot_sq_ass_item(PyObject *self, Py_ssize_t index, PyObject *value)
{
    Din_Go(2572,2048);PyObject *res;
    static PyObject *delitem_str, *setitem_str;

    Din_Go(2575,2048);if (value == NULL)
        {/*573*/Din_Go(2573,2048);res = call_method(self, "__delitem__", &delitem_str,
                          "(n)", index);/*574*/}
    else
        {/*575*/Din_Go(2574,2048);res = call_method(self, "__setitem__", &setitem_str,
                          "(nO)", index, value);/*576*/}
    Din_Go(2577,2048);if (res == NULL)
        {/*577*/{int  ReplaceReturn97 = -1; Din_Go(2576,2048); return ReplaceReturn97;}/*578*/}
    Py_DECREF(res);
    {int  ReplaceReturn96 = 0; Din_Go(2578,2048); return ReplaceReturn96;}
}

static int
slot_sq_ass_slice(PyObject *self, Py_ssize_t i, Py_ssize_t j, PyObject *value)
{
    Din_Go(2579,2048);PyObject *res;
    static PyObject *delslice_str, *setslice_str;

    Din_Go(2586,2048);if (value == NULL) {
        Din_Go(2580,2048);if (PyErr_WarnPy3k("in 3.x, __delslice__ has been removed; "
                           "use __delitem__", 1) < 0)
            {/*579*/{int  ReplaceReturn95 = -1; Din_Go(2581,2048); return ReplaceReturn95;}/*580*/}
        Din_Go(2582,2048);res = call_method(self, "__delslice__", &delslice_str,
                          "(nn)", i, j);
    }
    else {
        Din_Go(2583,2048);if (PyErr_WarnPy3k("in 3.x, __setslice__ has been removed; "
                                "use __setitem__", 1) < 0)
            {/*581*/{int  ReplaceReturn94 = -1; Din_Go(2584,2048); return ReplaceReturn94;}/*582*/}
        Din_Go(2585,2048);res = call_method(self, "__setslice__", &setslice_str,
                  "(nnO)", i, j, value);
    }
    Din_Go(2588,2048);if (res == NULL)
        {/*583*/{int  ReplaceReturn93 = -1; Din_Go(2587,2048); return ReplaceReturn93;}/*584*/}
    Py_DECREF(res);
    {int  ReplaceReturn92 = 0; Din_Go(2589,2048); return ReplaceReturn92;}
}

static int
slot_sq_contains(PyObject *self, PyObject *value)
{
    Din_Go(2590,2048);PyObject *func, *res, *args;
    int result = -1;

    static PyObject *contains_str;

    func = lookup_maybe(self, "__contains__", &contains_str);
    Din_Go(2598,2048);if (func != NULL) {
        Din_Go(2591,2048);args = PyTuple_Pack(1, value);
        Din_Go(2593,2048);if (args == NULL)
            res = NULL;
        else {
            Din_Go(2592,2048);res = PyObject_Call(func, args, NULL);
            Py_DECREF(args);
        }
        Py_DECREF(func);
        Din_Go(2595,2048);if (res != NULL) {
            Din_Go(2594,2048);result = PyObject_IsTrue(res);
            Py_DECREF(res);
        }
    }
    else {/*585*/Din_Go(2596,2048);if (! PyErr_Occurred()) {
        /* Possible results: -1 and 1 */
        Din_Go(2597,2048);result = (int)_PySequence_IterSearch(self, value,
                                         PY_ITERSEARCH_CONTAINS);
    ;/*586*/}}
    {int  ReplaceReturn91 = result; Din_Go(2599,2048); return ReplaceReturn91;}
}

#define slot_mp_length slot_sq_length

SLOT1(slot_mp_subscript, "__getitem__", PyObject *, "O")

static int
slot_mp_ass_subscript(PyObject *self, PyObject *key, PyObject *value)
{
    Din_Go(2600,2048);PyObject *res;
    static PyObject *delitem_str, *setitem_str;

    Din_Go(2603,2048);if (value == NULL)
        {/*587*/Din_Go(2601,2048);res = call_method(self, "__delitem__", &delitem_str,
                          "(O)", key);/*588*/}
    else
        {/*589*/Din_Go(2602,2048);res = call_method(self, "__setitem__", &setitem_str,
                         "(OO)", key, value);/*590*/}
    Din_Go(2605,2048);if (res == NULL)
        {/*591*/{int  ReplaceReturn90 = -1; Din_Go(2604,2048); return ReplaceReturn90;}/*592*/}
    Py_DECREF(res);
    {int  ReplaceReturn89 = 0; Din_Go(2606,2048); return ReplaceReturn89;}
}

SLOT1BIN(slot_nb_add, nb_add, "__add__", "__radd__")
SLOT1BIN(slot_nb_subtract, nb_subtract, "__sub__", "__rsub__")
SLOT1BIN(slot_nb_multiply, nb_multiply, "__mul__", "__rmul__")
SLOT1BIN(slot_nb_divide, nb_divide, "__div__", "__rdiv__")
SLOT1BIN(slot_nb_remainder, nb_remainder, "__mod__", "__rmod__")
SLOT1BIN(slot_nb_divmod, nb_divmod, "__divmod__", "__rdivmod__")

static PyObject *slot_nb_power(PyObject *, PyObject *, PyObject *);

SLOT1BINFULL(slot_nb_power_binary, slot_nb_power,
             nb_power, "__pow__", "__rpow__")

static PyObject *
slot_nb_power(PyObject *self, PyObject *other, PyObject *modulus)
{
    Din_Go(2607,2048);static PyObject *pow_str;

    Din_Go(2609,2048);if (modulus == Py_None)
        {/*617*/{PyObject * ReplaceReturn88 = slot_nb_power_binary(self, other); Din_Go(2608,2048); return ReplaceReturn88;}/*618*/}
    /* Three-arg power doesn't use __rpow__.  But ternary_op
       can call this when the second argument's type uses
       slot_nb_power, so check before calling self.__pow__. */
    Din_Go(2611,2048);if (Py_TYPE(self)->tp_as_number != NULL &&
        Py_TYPE(self)->tp_as_number->nb_power == slot_nb_power) {
        {PyObject * ReplaceReturn87 = call_method(self, "__pow__", &pow_str,
                           "(OO)", other, modulus); Din_Go(2610,2048); return ReplaceReturn87;}
    }
    Py_INCREF(Py_NotImplemented);
    {PyObject * ReplaceReturn86 = Py_NotImplemented; Din_Go(2612,2048); return ReplaceReturn86;}
}

SLOT0(slot_nb_negative, "__neg__")
SLOT0(slot_nb_positive, "__pos__")
SLOT0(slot_nb_absolute, "__abs__")

static int
slot_nb_nonzero(PyObject *self)
{
    Din_Go(2613,2048);PyObject *func, *args;
    static PyObject *nonzero_str, *len_str;
    int result = -1;
    int using_len = 0;

    func = lookup_maybe(self, "__nonzero__", &nonzero_str);
    Din_Go(2620,2048);if (func == NULL) {
        Din_Go(2614,2048);if (PyErr_Occurred())
            {/*623*/{int  ReplaceReturn85 = -1; Din_Go(2615,2048); return ReplaceReturn85;}/*624*/}
        Din_Go(2616,2048);func = lookup_maybe(self, "__len__", &len_str);
        Din_Go(2618,2048);if (func == NULL)
            {/*625*/{int  ReplaceReturn84 = PyErr_Occurred() ? -1 : 1; Din_Go(2617,2048); return ReplaceReturn84;}/*626*/}
        Din_Go(2619,2048);using_len = 1;
    }
    Din_Go(2621,2048);args = PyTuple_New(0);
    Din_Go(2627,2048);if (args != NULL) {
        Din_Go(2622,2048);PyObject *temp = PyObject_Call(func, args, NULL);
        Py_DECREF(args);
        Din_Go(2626,2048);if (temp != NULL) {
            Din_Go(2623,2048);if (PyInt_CheckExact(temp) || PyBool_Check(temp))
                {/*627*/Din_Go(2624,2048);result = PyObject_IsTrue(temp);/*628*/}
            else {
                Din_Go(2625,2048);PyErr_Format(PyExc_TypeError,
                             "%s should return "
                             "bool or int, returned %s",
                             (using_len ? "__len__"
                                        : "__nonzero__"),
                             temp->ob_type->tp_name);
                result = -1;
            }
            Py_DECREF(temp);
        }
    }
    Py_DECREF(func);
    {int  ReplaceReturn83 = result; Din_Go(2628,2048); return ReplaceReturn83;}
}


static PyObject *
slot_nb_index(PyObject *self)
{
    Din_Go(2629,2048);static PyObject *index_str;
    {PyObject * ReplaceReturn82 = call_method(self, "__index__", &index_str, "()"); Din_Go(2630,2048); return ReplaceReturn82;}
}


SLOT0(slot_nb_invert, "__invert__")
SLOT1BIN(slot_nb_lshift, nb_lshift, "__lshift__", "__rlshift__")
SLOT1BIN(slot_nb_rshift, nb_rshift, "__rshift__", "__rrshift__")
SLOT1BIN(slot_nb_and, nb_and, "__and__", "__rand__")
SLOT1BIN(slot_nb_xor, nb_xor, "__xor__", "__rxor__")
SLOT1BIN(slot_nb_or, nb_or, "__or__", "__ror__")

static int
slot_nb_coerce(PyObject **a, PyObject **b)
{
    Din_Go(2631,2048);static PyObject *coerce_str;
    PyObject *self = *a, *other = *b;

    Din_Go(2641,2048);if (self->ob_type->tp_as_number != NULL &&
        self->ob_type->tp_as_number->nb_coerce == slot_nb_coerce) {
        Din_Go(2632,2048);PyObject *r;
        r = call_maybe(
            self, "__coerce__", &coerce_str, "(O)", other);
        Din_Go(2634,2048);if (r == NULL)
            {/*649*/{int  ReplaceReturn81 = -1; Din_Go(2633,2048); return ReplaceReturn81;}/*650*/}
        Din_Go(2640,2048);if (r == Py_NotImplemented) {
            Py_DECREF(r);
        }
        else {
            Din_Go(2635,2048);if (!PyTuple_Check(r) || PyTuple_GET_SIZE(r) != 2) {
                Din_Go(2636,2048);PyErr_SetString(PyExc_TypeError,
                    "__coerce__ didn't return a 2-tuple");
                Py_DECREF(r);
                {int  ReplaceReturn80 = -1; Din_Go(2637,2048); return ReplaceReturn80;}
            }
            Din_Go(2638,2048);*a = PyTuple_GET_ITEM(r, 0);
            Py_INCREF(*a);
            *b = PyTuple_GET_ITEM(r, 1);
            Py_INCREF(*b);
            Py_DECREF(r);
            {int  ReplaceReturn79 = 0; Din_Go(2639,2048); return ReplaceReturn79;}
        }
    }
    Din_Go(2652,2048);if (other->ob_type->tp_as_number != NULL &&
        other->ob_type->tp_as_number->nb_coerce == slot_nb_coerce) {
        Din_Go(2642,2048);PyObject *r;
        r = call_maybe(
            other, "__coerce__", &coerce_str, "(O)", self);
        Din_Go(2644,2048);if (r == NULL)
            {/*651*/{int  ReplaceReturn78 = -1; Din_Go(2643,2048); return ReplaceReturn78;}/*652*/}
        Din_Go(2646,2048);if (r == Py_NotImplemented) {
            Py_DECREF(r);
            {int  ReplaceReturn77 = 1; Din_Go(2645,2048); return ReplaceReturn77;}
        }
        Din_Go(2649,2048);if (!PyTuple_Check(r) || PyTuple_GET_SIZE(r) != 2) {
            Din_Go(2647,2048);PyErr_SetString(PyExc_TypeError,
                            "__coerce__ didn't return a 2-tuple");
            Py_DECREF(r);
            {int  ReplaceReturn76 = -1; Din_Go(2648,2048); return ReplaceReturn76;}
        }
        Din_Go(2650,2048);*a = PyTuple_GET_ITEM(r, 1);
        Py_INCREF(*a);
        *b = PyTuple_GET_ITEM(r, 0);
        Py_INCREF(*b);
        Py_DECREF(r);
        {int  ReplaceReturn75 = 0; Din_Go(2651,2048); return ReplaceReturn75;}
    }
    {int  ReplaceReturn74 = 1; Din_Go(2653,2048); return ReplaceReturn74;}
}

SLOT0(slot_nb_int, "__int__")
SLOT0(slot_nb_long, "__long__")
SLOT0(slot_nb_float, "__float__")
SLOT0(slot_nb_oct, "__oct__")
SLOT0(slot_nb_hex, "__hex__")
SLOT1(slot_nb_inplace_add, "__iadd__", PyObject *, "O")
SLOT1(slot_nb_inplace_subtract, "__isub__", PyObject *, "O")
SLOT1(slot_nb_inplace_multiply, "__imul__", PyObject *, "O")
SLOT1(slot_nb_inplace_divide, "__idiv__", PyObject *, "O")
SLOT1(slot_nb_inplace_remainder, "__imod__", PyObject *, "O")
/* Can't use SLOT1 here, because nb_inplace_power is ternary */
static PyObject *
slot_nb_inplace_power(PyObject *self, PyObject * arg1, PyObject *arg2)
{
  Din_Go(2654,2048);static PyObject *cache_str;
  {PyObject * ReplaceReturn73 = call_method(self, "__ipow__", &cache_str, "(" "O" ")", arg1); Din_Go(2655,2048); return ReplaceReturn73;}
}
SLOT1(slot_nb_inplace_lshift, "__ilshift__", PyObject *, "O")
SLOT1(slot_nb_inplace_rshift, "__irshift__", PyObject *, "O")
SLOT1(slot_nb_inplace_and, "__iand__", PyObject *, "O")
SLOT1(slot_nb_inplace_xor, "__ixor__", PyObject *, "O")
SLOT1(slot_nb_inplace_or, "__ior__", PyObject *, "O")
SLOT1BIN(slot_nb_floor_divide, nb_floor_divide,
         "__floordiv__", "__rfloordiv__")
SLOT1BIN(slot_nb_true_divide, nb_true_divide, "__truediv__", "__rtruediv__")
SLOT1(slot_nb_inplace_floor_divide, "__ifloordiv__", PyObject *, "O")
SLOT1(slot_nb_inplace_true_divide, "__itruediv__", PyObject *, "O")

static int
half_compare(PyObject *self, PyObject *other)
{
    Din_Go(2656,2048);PyObject *func, *args, *res;
    static PyObject *cmp_str;
    Py_ssize_t c;

    func = lookup_method(self, "__cmp__", &cmp_str);
    Din_Go(2668,2048);if (func == NULL) {
        Din_Go(2657,2048);PyErr_Clear();
    }
    else {
        Din_Go(2658,2048);args = PyTuple_Pack(1, other);
        Din_Go(2660,2048);if (args == NULL)
            res = NULL;
        else {
            Din_Go(2659,2048);res = PyObject_Call(func, args, NULL);
            Py_DECREF(args);
        }
        Py_DECREF(func);
        Din_Go(2667,2048);if (res != Py_NotImplemented) {
            Din_Go(2661,2048);if (res == NULL)
                {/*661*/{int  ReplaceReturn72 = -2; Din_Go(2662,2048); return ReplaceReturn72;}/*662*/}
            Din_Go(2663,2048);c = PyInt_AsLong(res);
            Py_DECREF(res);
            Din_Go(2665,2048);if (c == -1 && PyErr_Occurred())
                {/*663*/{int  ReplaceReturn71 = -2; Din_Go(2664,2048); return ReplaceReturn71;}/*664*/}
            {int  ReplaceReturn70 = (c < 0) ? -1 : (c > 0) ? 1 : 0; Din_Go(2666,2048); return ReplaceReturn70;}
        }
        Py_DECREF(res);
    }
    {int  ReplaceReturn69 = 2; Din_Go(2669,2048); return ReplaceReturn69;}
}

/* This slot is published for the benefit of try_3way_compare in object.c */
int
_PyObject_SlotCompare(PyObject *self, PyObject *other)
{
    Din_Go(2670,2048);int c;

    Din_Go(2674,2048);if (Py_TYPE(self)->tp_compare == _PyObject_SlotCompare) {
        Din_Go(2671,2048);c = half_compare(self, other);
        Din_Go(2673,2048);if (c <= 1)
            {/*55*/{int  ReplaceReturn68 = c; Din_Go(2672,2048); return ReplaceReturn68;}/*56*/}
    }
    Din_Go(2680,2048);if (Py_TYPE(other)->tp_compare == _PyObject_SlotCompare) {
        Din_Go(2675,2048);c = half_compare(other, self);
        Din_Go(2677,2048);if (c < -1)
            {/*57*/{int  ReplaceReturn67 = -2; Din_Go(2676,2048); return ReplaceReturn67;}/*58*/}
        Din_Go(2679,2048);if (c <= 1)
            {/*59*/{int  ReplaceReturn66 = -c; Din_Go(2678,2048); return ReplaceReturn66;}/*60*/}
    }
    {int  ReplaceReturn65 = (void *)self < (void *)other ? -1 :
        (void *)self > (void *)other ? 1 : 0; Din_Go(2681,2048); return ReplaceReturn65;}
}

static PyObject *
slot_tp_repr(PyObject *self)
{
    Din_Go(2682,2048);PyObject *func, *res;
    static PyObject *repr_str;

    func = lookup_method(self, "__repr__", &repr_str);
    Din_Go(2685,2048);if (func != NULL) {
        Din_Go(2683,2048);res = PyEval_CallObject(func, NULL);
        Py_DECREF(func);
        {PyObject * ReplaceReturn64 = res; Din_Go(2684,2048); return ReplaceReturn64;}
    }
    Din_Go(2686,2048);PyErr_Clear();
    {PyObject * ReplaceReturn63 = PyString_FromFormat("<%s object at %p>",
                               Py_TYPE(self)->tp_name, self); Din_Go(2687,2048); return ReplaceReturn63;}
}

static PyObject *
slot_tp_str(PyObject *self)
{
    Din_Go(2688,2048);PyObject *func, *res;
    static PyObject *str_str;

    func = lookup_method(self, "__str__", &str_str);
    Din_Go(2693,2048);if (func != NULL) {
        Din_Go(2689,2048);res = PyEval_CallObject(func, NULL);
        Py_DECREF(func);
        {PyObject * ReplaceReturn62 = res; Din_Go(2690,2048); return ReplaceReturn62;}
    }
    else {
        Din_Go(2691,2048);PyErr_Clear();
        {PyObject * ReplaceReturn61 = slot_tp_repr(self); Din_Go(2692,2048); return ReplaceReturn61;}
    }
Din_Go(2694,2048);}

static long
slot_tp_hash(PyObject *self)
{
    Din_Go(2695,2048);PyObject *func;
    static PyObject *hash_str, *eq_str, *cmp_str;
    long h;

    func = lookup_method(self, "__hash__", &hash_str);

    Din_Go(2708,2048);if (func != NULL && func != Py_None) {
        Din_Go(2696,2048);PyObject *res = PyEval_CallObject(func, NULL);
        Py_DECREF(func);
        Din_Go(2698,2048);if (res == NULL)
            {/*665*/{long  ReplaceReturn60 = -1; Din_Go(2697,2048); return ReplaceReturn60;}/*666*/}
        Din_Go(2701,2048);if (PyLong_Check(res))
            {/*667*/Din_Go(2699,2048);h = PyLong_Type.tp_hash(res);/*668*/}
        else
            {/*669*/Din_Go(2700,2048);h = PyInt_AsLong(res);/*670*/}
        Py_DECREF(res);
    }
    else {
        Py_XDECREF(func); /* may be None */
        Din_Go(2702,2048);PyErr_Clear();
        func = lookup_method(self, "__eq__", &eq_str);
        Din_Go(2704,2048);if (func == NULL) {
            Din_Go(2703,2048);PyErr_Clear();
            func = lookup_method(self, "__cmp__", &cmp_str);
        }
        Din_Go(2706,2048);if (func != NULL) {
            Py_DECREF(func);
            {long  ReplaceReturn59 = PyObject_HashNotImplemented(self); Din_Go(2705,2048); return ReplaceReturn59;}
        }
        Din_Go(2707,2048);PyErr_Clear();
        h = _Py_HashPointer((void *)self);
    }
    Din_Go(2710,2048);if (h == -1 && !PyErr_Occurred())
        {/*671*/Din_Go(2709,2048);h = -2;/*672*/}
    {long  ReplaceReturn58 = h; Din_Go(2711,2048); return ReplaceReturn58;}
}

static PyObject *
slot_tp_call(PyObject *self, PyObject *args, PyObject *kwds)
{
    Din_Go(2712,2048);static PyObject *call_str;
    PyObject *meth = lookup_method(self, "__call__", &call_str);
    PyObject *res;

    Din_Go(2713,2048);if (meth == NULL)
        return NULL;

    Din_Go(2714,2048);res = PyObject_Call(meth, args, kwds);

    Py_DECREF(meth);
    {PyObject * ReplaceReturn57 = res; Din_Go(2715,2048); return ReplaceReturn57;}
}

/* There are two slot dispatch functions for tp_getattro.

   - slot_tp_getattro() is used when __getattribute__ is overridden
     but no __getattr__ hook is present;

   - slot_tp_getattr_hook() is used when a __getattr__ hook is present.

   The code in update_one_slot() always installs slot_tp_getattr_hook(); this
   detects the absence of __getattr__ and then installs the simpler slot if
   necessary. */

static PyObject *
slot_tp_getattro(PyObject *self, PyObject *name)
{
    Din_Go(2716,2048);static PyObject *getattribute_str = NULL;
    {PyObject * ReplaceReturn56 = call_method(self, "__getattribute__", &getattribute_str,
                       "(O)", name); Din_Go(2717,2048); return ReplaceReturn56;}
}

static PyObject *
call_attribute(PyObject *self, PyObject *attr, PyObject *name)
{
    Din_Go(2718,2048);PyObject *res, *descr = NULL;
    descrgetfunc f = Py_TYPE(attr)->tp_descr_get;

    Din_Go(2722,2048);if (f != NULL) {
        Din_Go(2719,2048);descr = f(attr, self, (PyObject *)(Py_TYPE(self)));
        Din_Go(2721,2048);if (descr == NULL)
            return NULL;
        else
            {/*673*/Din_Go(2720,2048);attr = descr;/*674*/}
    }
    Din_Go(2723,2048);res = PyObject_CallFunctionObjArgs(attr, name, NULL);
    Py_XDECREF(descr);
    {PyObject * ReplaceReturn55 = res; Din_Go(2724,2048); return ReplaceReturn55;}
}

static PyObject *
slot_tp_getattr_hook(PyObject *self, PyObject *name)
{
    Din_Go(2725,2048);PyTypeObject *tp = Py_TYPE(self);
    PyObject *getattr, *getattribute, *res;
    static PyObject *getattribute_str = NULL;
    static PyObject *getattr_str = NULL;

    Din_Go(2728,2048);if (getattr_str == NULL) {
        Din_Go(2726,2048);getattr_str = PyString_InternFromString("__getattr__");
        Din_Go(2727,2048);if (getattr_str == NULL)
            return NULL;
    }
    Din_Go(2731,2048);if (getattribute_str == NULL) {
        Din_Go(2729,2048);getattribute_str =
            PyString_InternFromString("__getattribute__");
        Din_Go(2730,2048);if (getattribute_str == NULL)
            return NULL;
    }
    /* speed hack: we could use lookup_maybe, but that would resolve the
       method fully for each attribute lookup for classes with
       __getattr__, even when the attribute is present. So we use
       _PyType_Lookup and create the method only when needed, with
       call_attribute. */
    Din_Go(2732,2048);getattr = _PyType_Lookup(tp, getattr_str);
    Din_Go(2735,2048);if (getattr == NULL) {
        /* No __getattr__ hook: use a simpler dispatcher */
        Din_Go(2733,2048);tp->tp_getattro = slot_tp_getattro;
        {PyObject * ReplaceReturn54 = slot_tp_getattro(self, name); Din_Go(2734,2048); return ReplaceReturn54;}
    }
    Py_INCREF(getattr);
    /* speed hack: we could use lookup_maybe, but that would resolve the
       method fully for each attribute lookup for classes with
       __getattr__, even when self has the default __getattribute__
       method. So we use _PyType_Lookup and create the method only when
       needed, with call_attribute. */
    Din_Go(2736,2048);getattribute = _PyType_Lookup(tp, getattribute_str);
    Din_Go(2739,2048);if (getattribute == NULL ||
        (Py_TYPE(getattribute) == &PyWrapperDescr_Type &&
         ((PyWrapperDescrObject *)getattribute)->d_wrapped ==
         (void *)PyObject_GenericGetAttr))
        {/*675*/Din_Go(2737,2048);res = PyObject_GenericGetAttr(self, name);/*676*/}
    else {
        Py_INCREF(getattribute);
        Din_Go(2738,2048);res = call_attribute(self, getattribute, name);
        Py_DECREF(getattribute);
    }
    Din_Go(2741,2048);if (res == NULL && PyErr_ExceptionMatches(PyExc_AttributeError)) {
        Din_Go(2740,2048);PyErr_Clear();
        res = call_attribute(self, getattr, name);
    }
    Py_DECREF(getattr);
    {PyObject * ReplaceReturn53 = res; Din_Go(2742,2048); return ReplaceReturn53;}
}

static int
slot_tp_setattro(PyObject *self, PyObject *name, PyObject *value)
{
    Din_Go(2743,2048);PyObject *res;
    static PyObject *delattr_str, *setattr_str;

    Din_Go(2746,2048);if (value == NULL)
        {/*677*/Din_Go(2744,2048);res = call_method(self, "__delattr__", &delattr_str,
                          "(O)", name);/*678*/}
    else
        {/*679*/Din_Go(2745,2048);res = call_method(self, "__setattr__", &setattr_str,
                          "(OO)", name, value);/*680*/}
    Din_Go(2748,2048);if (res == NULL)
        {/*681*/{int  ReplaceReturn52 = -1; Din_Go(2747,2048); return ReplaceReturn52;}/*682*/}
    Py_DECREF(res);
    {int  ReplaceReturn51 = 0; Din_Go(2749,2048); return ReplaceReturn51;}
}

static char *name_op[] = {
    "__lt__",
    "__le__",
    "__eq__",
    "__ne__",
    "__gt__",
    "__ge__",
};

static PyObject *
half_richcompare(PyObject *self, PyObject *other, int op)
{
    Din_Go(2750,2048);PyObject *func, *args, *res;
    static PyObject *op_str[6];

    func = lookup_method(self, name_op[op], &op_str[op]);
    Din_Go(2753,2048);if (func == NULL) {
        Din_Go(2751,2048);PyErr_Clear();
        Py_INCREF(Py_NotImplemented);
        {PyObject * ReplaceReturn50 = Py_NotImplemented; Din_Go(2752,2048); return ReplaceReturn50;}
    }
    Din_Go(2754,2048);args = PyTuple_Pack(1, other);
    Din_Go(2756,2048);if (args == NULL)
        res = NULL;
    else {
        Din_Go(2755,2048);res = PyObject_Call(func, args, NULL);
        Py_DECREF(args);
    }
    Py_DECREF(func);
    {PyObject * ReplaceReturn49 = res; Din_Go(2757,2048); return ReplaceReturn49;}
}

static PyObject *
slot_tp_richcompare(PyObject *self, PyObject *other, int op)
{
    Din_Go(2758,2048);PyObject *res;

    Din_Go(2762,2048);if (Py_TYPE(self)->tp_richcompare == slot_tp_richcompare) {
        Din_Go(2759,2048);res = half_richcompare(self, other, op);
        Din_Go(2761,2048);if (res != Py_NotImplemented)
            {/*683*/{PyObject * ReplaceReturn48 = res; Din_Go(2760,2048); return ReplaceReturn48;}/*684*/}
        Py_DECREF(res);
    }
    Din_Go(2766,2048);if (Py_TYPE(other)->tp_richcompare == slot_tp_richcompare) {
        Din_Go(2763,2048);res = half_richcompare(other, self, _Py_SwappedOp[op]);
        Din_Go(2765,2048);if (res != Py_NotImplemented) {
            {PyObject * ReplaceReturn47 = res; Din_Go(2764,2048); return ReplaceReturn47;}
        }
        Py_DECREF(res);
    }
    Py_INCREF(Py_NotImplemented);
    {PyObject * ReplaceReturn46 = Py_NotImplemented; Din_Go(2767,2048); return ReplaceReturn46;}
}

static PyObject *
slot_tp_iter(PyObject *self)
{
    Din_Go(2768,2048);PyObject *func, *res;
    static PyObject *iter_str, *getitem_str;

    func = lookup_method(self, "__iter__", &iter_str);
    Din_Go(2773,2048);if (func != NULL) {
        Din_Go(2769,2048);PyObject *args;
        args = res = PyTuple_New(0);
        Din_Go(2771,2048);if (args != NULL) {
            Din_Go(2770,2048);res = PyObject_Call(func, args, NULL);
            Py_DECREF(args);
        }
        Py_DECREF(func);
        {PyObject * ReplaceReturn45 = res; Din_Go(2772,2048); return ReplaceReturn45;}
    }
    Din_Go(2774,2048);PyErr_Clear();
    func = lookup_method(self, "__getitem__", &getitem_str);
    Din_Go(2777,2048);if (func == NULL) {
        Din_Go(2775,2048);PyErr_Format(PyExc_TypeError,
                     "'%.200s' object is not iterable",
                     Py_TYPE(self)->tp_name);
        {PyObject * ReplaceReturn44 = NULL; Din_Go(2776,2048); return ReplaceReturn44;}
    }
    Py_DECREF(func);
    {PyObject * ReplaceReturn43 = PySeqIter_New(self); Din_Go(2778,2048); return ReplaceReturn43;}
}

static PyObject *
slot_tp_iternext(PyObject *self)
{
    Din_Go(2779,2048);static PyObject *next_str;
    {PyObject * ReplaceReturn42 = call_method(self, "next", &next_str, "()"); Din_Go(2780,2048); return ReplaceReturn42;}
}

static PyObject *
slot_tp_descr_get(PyObject *self, PyObject *obj, PyObject *type)
{
    Din_Go(2781,2048);PyTypeObject *tp = Py_TYPE(self);
    PyObject *get;
    static PyObject *get_str = NULL;

    Din_Go(2784,2048);if (get_str == NULL) {
        Din_Go(2782,2048);get_str = PyString_InternFromString("__get__");
        Din_Go(2783,2048);if (get_str == NULL)
            return NULL;
    }
    Din_Go(2785,2048);get = _PyType_Lookup(tp, get_str);
    Din_Go(2788,2048);if (get == NULL) {
        /* Avoid further slowdowns */
        Din_Go(2786,2048);if (tp->tp_descr_get == slot_tp_descr_get)
            tp->tp_descr_get = NULL;
        Py_INCREF(self);
        {PyObject * ReplaceReturn41 = self; Din_Go(2787,2048); return ReplaceReturn41;}
    }
    Din_Go(2789,2048);if (obj == NULL)
        obj = Py_None;
    Din_Go(2790,2048);if (type == NULL)
        type = Py_None;
    {PyObject * ReplaceReturn40 = PyObject_CallFunctionObjArgs(get, self, obj, type, NULL); Din_Go(2791,2048); return ReplaceReturn40;}
}

static int
slot_tp_descr_set(PyObject *self, PyObject *target, PyObject *value)
{
    Din_Go(2792,2048);PyObject *res;
    static PyObject *del_str, *set_str;

    Din_Go(2795,2048);if (value == NULL)
        {/*685*/Din_Go(2793,2048);res = call_method(self, "__delete__", &del_str,
                          "(O)", target);/*686*/}
    else
        {/*687*/Din_Go(2794,2048);res = call_method(self, "__set__", &set_str,
                          "(OO)", target, value);/*688*/}
    Din_Go(2797,2048);if (res == NULL)
        {/*689*/{int  ReplaceReturn39 = -1; Din_Go(2796,2048); return ReplaceReturn39;}/*690*/}
    Py_DECREF(res);
    {int  ReplaceReturn38 = 0; Din_Go(2798,2048); return ReplaceReturn38;}
}

static int
slot_tp_init(PyObject *self, PyObject *args, PyObject *kwds)
{
    Din_Go(2799,2048);static PyObject *init_str;
    PyObject *meth = lookup_method(self, "__init__", &init_str);
    PyObject *res;

    Din_Go(2801,2048);if (meth == NULL)
        {/*691*/{int  ReplaceReturn37 = -1; Din_Go(2800,2048); return ReplaceReturn37;}/*692*/}
    Din_Go(2802,2048);res = PyObject_Call(meth, args, kwds);
    Py_DECREF(meth);
    Din_Go(2804,2048);if (res == NULL)
        {/*693*/{int  ReplaceReturn36 = -1; Din_Go(2803,2048); return ReplaceReturn36;}/*694*/}
    Din_Go(2807,2048);if (res != Py_None) {
        Din_Go(2805,2048);PyErr_Format(PyExc_TypeError,
                     "__init__() should return None, not '%.200s'",
                     Py_TYPE(res)->tp_name);
        Py_DECREF(res);
        {int  ReplaceReturn35 = -1; Din_Go(2806,2048); return ReplaceReturn35;}
    }
    Py_DECREF(res);
    {int  ReplaceReturn34 = 0; Din_Go(2808,2048); return ReplaceReturn34;}
}

static PyObject *
slot_tp_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    Din_Go(2809,2048);static PyObject *new_str;
    PyObject *func;
    PyObject *newargs, *x;
    Py_ssize_t i, n;

    Din_Go(2812,2048);if (new_str == NULL) {
        Din_Go(2810,2048);new_str = PyString_InternFromString("__new__");
        Din_Go(2811,2048);if (new_str == NULL)
            return NULL;
    }
    Din_Go(2813,2048);func = PyObject_GetAttr((PyObject *)type, new_str);
    Din_Go(2814,2048);if (func == NULL)
        return NULL;
    assert(PyTuple_Check(args));
    Din_Go(2815,2048);n = PyTuple_GET_SIZE(args);
    newargs = PyTuple_New(n+1);
    Din_Go(2816,2048);if (newargs == NULL)
        return NULL;
    Py_INCREF(type);
    PyTuple_SET_ITEM(newargs, 0, (PyObject *)type);
    Din_Go(2818,2048);for (i = 0; i < n; i++) {
        Din_Go(2817,2048);x = PyTuple_GET_ITEM(args, i);
        Py_INCREF(x);
        PyTuple_SET_ITEM(newargs, i+1, x);
    }
    Din_Go(2819,2048);x = PyObject_Call(func, newargs, kwds);
    Py_DECREF(newargs);
    Py_DECREF(func);
    {PyObject * ReplaceReturn33 = x; Din_Go(2820,2048); return ReplaceReturn33;}
}

static void
slot_tp_del(PyObject *self)
{
    Din_Go(2821,2048);static PyObject *del_str = NULL;
    PyObject *del, *res;
    PyObject *error_type, *error_value, *error_traceback;

    /* Temporarily resurrect the object. */
    assert(self->ob_refcnt == 0);
    self->ob_refcnt = 1;

    /* Save the current exception, if any. */
    PyErr_Fetch(&error_type, &error_value, &error_traceback);

    /* Execute __del__ method, if any. */
    del = lookup_maybe(self, "__del__", &del_str);
    Din_Go(2825,2048);if (del != NULL) {
        Din_Go(2822,2048);res = PyEval_CallObject(del, NULL);
        Din_Go(2824,2048);if (res == NULL)
            {/*695*/Din_Go(2823,2048);PyErr_WriteUnraisable(del);/*696*/}
        else
            Py_DECREF(res);
        Py_DECREF(del);
    }

    /* Restore the saved exception. */
    Din_Go(2826,2048);PyErr_Restore(error_type, error_value, error_traceback);

    /* Undo the temporary resurrection; can't use DECREF here, it would
     * cause a recursive call.
     */
    assert(self->ob_refcnt > 0);
    Din_Go(2828,2048);if (--self->ob_refcnt == 0)
        {/*697*/Din_Go(2827,2048);return;/*698*/}         /* this is the normal path out */

    /* __del__ resurrected it!  Make it look like the original Py_DECREF
     * never happened.
     */
    {
        Din_Go(2829,2048);Py_ssize_t refcnt = self->ob_refcnt;
        _Py_NewReference(self);
        self->ob_refcnt = refcnt;
    }
    assert(!PyType_IS_GC(Py_TYPE(self)) ||
           _Py_AS_GC(self)->gc.gc_refs != _PyGC_REFS_UNTRACKED);
    /* If Py_REF_DEBUG, _Py_NewReference bumped _Py_RefTotal, so
     * we need to undo that. */
    _Py_DEC_REFTOTAL;
    /* If Py_TRACE_REFS, _Py_NewReference re-added self to the object
     * chain, so no more to do there.
     * If COUNT_ALLOCS, the original decref bumped tp_frees, and
     * _Py_NewReference bumped tp_allocs:  both of those need to be
     * undone.
     */
#ifdef COUNT_ALLOCS
    --Py_TYPE(self)->tp_frees;
    --Py_TYPE(self)->tp_allocs;
#endif
}


/*
Table mapping __foo__ names to tp_foo offsets and slot_tp_foo wrapper functions.

The table is ordered by offsets relative to the 'PyHeapTypeObject' structure,
which incorporates the additional structures used for numbers, sequences and
mappings.  Note that multiple names may map to the same slot (e.g. __eq__,
__ne__ etc. all map to tp_richcompare) and one name may map to multiple slots
(e.g. __str__ affects tp_str as well as tp_repr). The table is terminated with
an all-zero entry.  (This table is further initialized in init_slotdefs().)
*/

typedef struct wrapperbase slotdef;

#undef TPSLOT
#undef FLSLOT
#undef ETSLOT
#undef SQSLOT
#undef MPSLOT
#undef NBSLOT
#undef UNSLOT
#undef IBSLOT
#undef BINSLOT
#undef RBINSLOT

#define TPSLOT(NAME, SLOT, FUNCTION, WRAPPER, DOC) \
    {NAME, offsetof(PyTypeObject, SLOT), (void *)(FUNCTION), WRAPPER, \
     PyDoc_STR(DOC)}
#define FLSLOT(NAME, SLOT, FUNCTION, WRAPPER, DOC, FLAGS) \
    {NAME, offsetof(PyTypeObject, SLOT), (void *)(FUNCTION), WRAPPER, \
     PyDoc_STR(DOC), FLAGS}
#define ETSLOT(NAME, SLOT, FUNCTION, WRAPPER, DOC) \
    {NAME, offsetof(PyHeapTypeObject, SLOT), (void *)(FUNCTION), WRAPPER, \
     PyDoc_STR(DOC)}
#define SQSLOT(NAME, SLOT, FUNCTION, WRAPPER, DOC) \
    ETSLOT(NAME, as_sequence.SLOT, FUNCTION, WRAPPER, DOC)
#define MPSLOT(NAME, SLOT, FUNCTION, WRAPPER, DOC) \
    ETSLOT(NAME, as_mapping.SLOT, FUNCTION, WRAPPER, DOC)
#define NBSLOT(NAME, SLOT, FUNCTION, WRAPPER, DOC) \
    ETSLOT(NAME, as_number.SLOT, FUNCTION, WRAPPER, DOC)
#define UNSLOT(NAME, SLOT, FUNCTION, WRAPPER, DOC) \
    ETSLOT(NAME, as_number.SLOT, FUNCTION, WRAPPER, \
           "x." NAME "() <==> " DOC)
#define IBSLOT(NAME, SLOT, FUNCTION, WRAPPER, DOC) \
    ETSLOT(NAME, as_number.SLOT, FUNCTION, WRAPPER, \
           "x." NAME "(y) <==> x" DOC "y")
#define BINSLOT(NAME, SLOT, FUNCTION, DOC) \
    ETSLOT(NAME, as_number.SLOT, FUNCTION, wrap_binaryfunc_l, \
           "x." NAME "(y) <==> x" DOC "y")
#define RBINSLOT(NAME, SLOT, FUNCTION, DOC) \
    ETSLOT(NAME, as_number.SLOT, FUNCTION, wrap_binaryfunc_r, \
           "x." NAME "(y) <==> y" DOC "x")
#define BINSLOTNOTINFIX(NAME, SLOT, FUNCTION, DOC) \
    ETSLOT(NAME, as_number.SLOT, FUNCTION, wrap_binaryfunc_l, \
           "x." NAME "(y) <==> " DOC)
#define RBINSLOTNOTINFIX(NAME, SLOT, FUNCTION, DOC) \
    ETSLOT(NAME, as_number.SLOT, FUNCTION, wrap_binaryfunc_r, \
           "x." NAME "(y) <==> " DOC)

static slotdef slotdefs[] = {
    TPSLOT("__str__", tp_print, NULL, NULL, ""),
    TPSLOT("__repr__", tp_print, NULL, NULL, ""),
    TPSLOT("__getattribute__", tp_getattr, NULL, NULL, ""),
    TPSLOT("__getattr__", tp_getattr, NULL, NULL, ""),
    TPSLOT("__setattr__", tp_setattr, NULL, NULL, ""),
    TPSLOT("__delattr__", tp_setattr, NULL, NULL, ""),
    TPSLOT("__cmp__", tp_compare, _PyObject_SlotCompare, wrap_cmpfunc,
           "x.__cmp__(y) <==> cmp(x,y)"),
    TPSLOT("__repr__", tp_repr, slot_tp_repr, wrap_unaryfunc,
           "x.__repr__() <==> repr(x)"),
    TPSLOT("__hash__", tp_hash, slot_tp_hash, wrap_hashfunc,
           "x.__hash__() <==> hash(x)"),
    FLSLOT("__call__", tp_call, slot_tp_call, (wrapperfunc)wrap_call,
           "x.__call__(...) <==> x(...)", PyWrapperFlag_KEYWORDS),
    TPSLOT("__str__", tp_str, slot_tp_str, wrap_unaryfunc,
           "x.__str__() <==> str(x)"),
    TPSLOT("__getattribute__", tp_getattro, slot_tp_getattr_hook,
           wrap_binaryfunc, "x.__getattribute__('name') <==> x.name"),
    TPSLOT("__getattr__", tp_getattro, slot_tp_getattr_hook, NULL, ""),
    TPSLOT("__setattr__", tp_setattro, slot_tp_setattro, wrap_setattr,
           "x.__setattr__('name', value) <==> x.name = value"),
    TPSLOT("__delattr__", tp_setattro, slot_tp_setattro, wrap_delattr,
           "x.__delattr__('name') <==> del x.name"),
    TPSLOT("__lt__", tp_richcompare, slot_tp_richcompare, richcmp_lt,
           "x.__lt__(y) <==> x<y"),
    TPSLOT("__le__", tp_richcompare, slot_tp_richcompare, richcmp_le,
           "x.__le__(y) <==> x<=y"),
    TPSLOT("__eq__", tp_richcompare, slot_tp_richcompare, richcmp_eq,
           "x.__eq__(y) <==> x==y"),
    TPSLOT("__ne__", tp_richcompare, slot_tp_richcompare, richcmp_ne,
           "x.__ne__(y) <==> x!=y"),
    TPSLOT("__gt__", tp_richcompare, slot_tp_richcompare, richcmp_gt,
           "x.__gt__(y) <==> x>y"),
    TPSLOT("__ge__", tp_richcompare, slot_tp_richcompare, richcmp_ge,
           "x.__ge__(y) <==> x>=y"),
    TPSLOT("__iter__", tp_iter, slot_tp_iter, wrap_unaryfunc,
           "x.__iter__() <==> iter(x)"),
    TPSLOT("next", tp_iternext, slot_tp_iternext, wrap_next,
           "x.next() -> the next value, or raise StopIteration"),
    TPSLOT("__get__", tp_descr_get, slot_tp_descr_get, wrap_descr_get,
           "descr.__get__(obj[, type]) -> value"),
    TPSLOT("__set__", tp_descr_set, slot_tp_descr_set, wrap_descr_set,
           "descr.__set__(obj, value)"),
    TPSLOT("__delete__", tp_descr_set, slot_tp_descr_set,
           wrap_descr_delete, "descr.__delete__(obj)"),
    FLSLOT("__init__", tp_init, slot_tp_init, (wrapperfunc)wrap_init,
           "x.__init__(...) initializes x; "
           "see help(type(x)) for signature",
           PyWrapperFlag_KEYWORDS),
    TPSLOT("__new__", tp_new, slot_tp_new, NULL, ""),
    TPSLOT("__del__", tp_del, slot_tp_del, NULL, ""),
    BINSLOT("__add__", nb_add, slot_nb_add,
        "+"),
    RBINSLOT("__radd__", nb_add, slot_nb_add,
             "+"),
    BINSLOT("__sub__", nb_subtract, slot_nb_subtract,
        "-"),
    RBINSLOT("__rsub__", nb_subtract, slot_nb_subtract,
             "-"),
    BINSLOT("__mul__", nb_multiply, slot_nb_multiply,
        "*"),
    RBINSLOT("__rmul__", nb_multiply, slot_nb_multiply,
             "*"),
    BINSLOT("__div__", nb_divide, slot_nb_divide,
        "/"),
    RBINSLOT("__rdiv__", nb_divide, slot_nb_divide,
             "/"),
    BINSLOT("__mod__", nb_remainder, slot_nb_remainder,
        "%"),
    RBINSLOT("__rmod__", nb_remainder, slot_nb_remainder,
             "%"),
    BINSLOTNOTINFIX("__divmod__", nb_divmod, slot_nb_divmod,
        "divmod(x, y)"),
    RBINSLOTNOTINFIX("__rdivmod__", nb_divmod, slot_nb_divmod,
             "divmod(y, x)"),
    NBSLOT("__pow__", nb_power, slot_nb_power, wrap_ternaryfunc,
           "x.__pow__(y[, z]) <==> pow(x, y[, z])"),
    NBSLOT("__rpow__", nb_power, slot_nb_power, wrap_ternaryfunc_r,
           "y.__rpow__(x[, z]) <==> pow(x, y[, z])"),
    UNSLOT("__neg__", nb_negative, slot_nb_negative, wrap_unaryfunc, "-x"),
    UNSLOT("__pos__", nb_positive, slot_nb_positive, wrap_unaryfunc, "+x"),
    UNSLOT("__abs__", nb_absolute, slot_nb_absolute, wrap_unaryfunc,
           "abs(x)"),
    UNSLOT("__nonzero__", nb_nonzero, slot_nb_nonzero, wrap_inquirypred,
           "x != 0"),
    UNSLOT("__invert__", nb_invert, slot_nb_invert, wrap_unaryfunc, "~x"),
    BINSLOT("__lshift__", nb_lshift, slot_nb_lshift, "<<"),
    RBINSLOT("__rlshift__", nb_lshift, slot_nb_lshift, "<<"),
    BINSLOT("__rshift__", nb_rshift, slot_nb_rshift, ">>"),
    RBINSLOT("__rrshift__", nb_rshift, slot_nb_rshift, ">>"),
    BINSLOT("__and__", nb_and, slot_nb_and, "&"),
    RBINSLOT("__rand__", nb_and, slot_nb_and, "&"),
    BINSLOT("__xor__", nb_xor, slot_nb_xor, "^"),
    RBINSLOT("__rxor__", nb_xor, slot_nb_xor, "^"),
    BINSLOT("__or__", nb_or, slot_nb_or, "|"),
    RBINSLOT("__ror__", nb_or, slot_nb_or, "|"),
    NBSLOT("__coerce__", nb_coerce, slot_nb_coerce, wrap_coercefunc,
           "x.__coerce__(y) <==> coerce(x, y)"),
    UNSLOT("__int__", nb_int, slot_nb_int, wrap_unaryfunc,
           "int(x)"),
    UNSLOT("__long__", nb_long, slot_nb_long, wrap_unaryfunc,
           "long(x)"),
    UNSLOT("__float__", nb_float, slot_nb_float, wrap_unaryfunc,
           "float(x)"),
    UNSLOT("__oct__", nb_oct, slot_nb_oct, wrap_unaryfunc,
           "oct(x)"),
    UNSLOT("__hex__", nb_hex, slot_nb_hex, wrap_unaryfunc,
           "hex(x)"),
    IBSLOT("__iadd__", nb_inplace_add, slot_nb_inplace_add,
           wrap_binaryfunc, "+="),
    IBSLOT("__isub__", nb_inplace_subtract, slot_nb_inplace_subtract,
           wrap_binaryfunc, "-="),
    IBSLOT("__imul__", nb_inplace_multiply, slot_nb_inplace_multiply,
           wrap_binaryfunc, "*="),
    IBSLOT("__idiv__", nb_inplace_divide, slot_nb_inplace_divide,
           wrap_binaryfunc, "/="),
    IBSLOT("__imod__", nb_inplace_remainder, slot_nb_inplace_remainder,
           wrap_binaryfunc, "%="),
    IBSLOT("__ipow__", nb_inplace_power, slot_nb_inplace_power,
           wrap_binaryfunc, "**="),
    IBSLOT("__ilshift__", nb_inplace_lshift, slot_nb_inplace_lshift,
           wrap_binaryfunc, "<<="),
    IBSLOT("__irshift__", nb_inplace_rshift, slot_nb_inplace_rshift,
           wrap_binaryfunc, ">>="),
    IBSLOT("__iand__", nb_inplace_and, slot_nb_inplace_and,
           wrap_binaryfunc, "&="),
    IBSLOT("__ixor__", nb_inplace_xor, slot_nb_inplace_xor,
           wrap_binaryfunc, "^="),
    IBSLOT("__ior__", nb_inplace_or, slot_nb_inplace_or,
           wrap_binaryfunc, "|="),
    BINSLOT("__floordiv__", nb_floor_divide, slot_nb_floor_divide, "//"),
    RBINSLOT("__rfloordiv__", nb_floor_divide, slot_nb_floor_divide, "//"),
    BINSLOT("__truediv__", nb_true_divide, slot_nb_true_divide, "/"),
    RBINSLOT("__rtruediv__", nb_true_divide, slot_nb_true_divide, "/"),
    IBSLOT("__ifloordiv__", nb_inplace_floor_divide,
           slot_nb_inplace_floor_divide, wrap_binaryfunc, "//"),
    IBSLOT("__itruediv__", nb_inplace_true_divide,
           slot_nb_inplace_true_divide, wrap_binaryfunc, "/"),
    NBSLOT("__index__", nb_index, slot_nb_index, wrap_unaryfunc,
           "x[y:z] <==> x[y.__index__():z.__index__()]"),
    MPSLOT("__len__", mp_length, slot_mp_length, wrap_lenfunc,
           "x.__len__() <==> len(x)"),
    MPSLOT("__getitem__", mp_subscript, slot_mp_subscript,
           wrap_binaryfunc,
           "x.__getitem__(y) <==> x[y]"),
    MPSLOT("__setitem__", mp_ass_subscript, slot_mp_ass_subscript,
           wrap_objobjargproc,
           "x.__setitem__(i, y) <==> x[i]=y"),
    MPSLOT("__delitem__", mp_ass_subscript, slot_mp_ass_subscript,
           wrap_delitem,
           "x.__delitem__(y) <==> del x[y]"),
    SQSLOT("__len__", sq_length, slot_sq_length, wrap_lenfunc,
           "x.__len__() <==> len(x)"),
    /* Heap types defining __add__/__mul__ have sq_concat/sq_repeat == NULL.
       The logic in abstract.c always falls back to nb_add/nb_multiply in
       this case.  Defining both the nb_* and the sq_* slots to call the
       user-defined methods has unexpected side-effects, as shown by
       test_descr.notimplemented() */
    SQSLOT("__add__", sq_concat, NULL, wrap_binaryfunc,
      "x.__add__(y) <==> x+y"),
    SQSLOT("__mul__", sq_repeat, NULL, wrap_indexargfunc,
      "x.__mul__(n) <==> x*n"),
    SQSLOT("__rmul__", sq_repeat, NULL, wrap_indexargfunc,
      "x.__rmul__(n) <==> n*x"),
    SQSLOT("__getitem__", sq_item, slot_sq_item, wrap_sq_item,
           "x.__getitem__(y) <==> x[y]"),
    SQSLOT("__getslice__", sq_slice, slot_sq_slice, wrap_ssizessizeargfunc,
           "x.__getslice__(i, j) <==> x[i:j]\n\
           \n\
           Use of negative indices is not supported."),
    SQSLOT("__setitem__", sq_ass_item, slot_sq_ass_item, wrap_sq_setitem,
           "x.__setitem__(i, y) <==> x[i]=y"),
    SQSLOT("__delitem__", sq_ass_item, slot_sq_ass_item, wrap_sq_delitem,
           "x.__delitem__(y) <==> del x[y]"),
    SQSLOT("__setslice__", sq_ass_slice, slot_sq_ass_slice,
           wrap_ssizessizeobjargproc,
           "x.__setslice__(i, j, y) <==> x[i:j]=y\n\
           \n\
           Use  of negative indices is not supported."),
    SQSLOT("__delslice__", sq_ass_slice, slot_sq_ass_slice, wrap_delslice,
           "x.__delslice__(i, j) <==> del x[i:j]\n\
           \n\
           Use of negative indices is not supported."),
    SQSLOT("__contains__", sq_contains, slot_sq_contains, wrap_objobjproc,
           "x.__contains__(y) <==> y in x"),
    SQSLOT("__iadd__", sq_inplace_concat, NULL,
      wrap_binaryfunc, "x.__iadd__(y) <==> x+=y"),
    SQSLOT("__imul__", sq_inplace_repeat, NULL,
      wrap_indexargfunc, "x.__imul__(y) <==> x*=y"),
    {NULL}
};

/* Given a type pointer and an offset gotten from a slotdef entry, return a
   pointer to the actual slot.  This is not quite the same as simply adding
   the offset to the type pointer, since it takes care to indirect through the
   proper indirection pointer (as_buffer, etc.); it returns NULL if the
   indirection pointer is NULL. */
static void **
slotptr(PyTypeObject *type, int ioffset)
{
    Din_Go(2830,2048);char *ptr;
    long offset = ioffset;

    /* Note: this depends on the order of the members of PyHeapTypeObject! */
    assert(offset >= 0);
    assert((size_t)offset < offsetof(PyHeapTypeObject, as_buffer));
    Din_Go(2837,2048);if ((size_t)offset >= offsetof(PyHeapTypeObject, as_sequence)) {
        Din_Go(2831,2048);ptr = (char *)type->tp_as_sequence;
        offset -= offsetof(PyHeapTypeObject, as_sequence);
    }
    else {/*699*/Din_Go(2832,2048);if ((size_t)offset >= offsetof(PyHeapTypeObject, as_mapping)) {
        Din_Go(2833,2048);ptr = (char *)type->tp_as_mapping;
        offset -= offsetof(PyHeapTypeObject, as_mapping);
    }
    else {/*701*/Din_Go(2834,2048);if ((size_t)offset >= offsetof(PyHeapTypeObject, as_number)) {
        Din_Go(2835,2048);ptr = (char *)type->tp_as_number;
        offset -= offsetof(PyHeapTypeObject, as_number);
    }
    else {
        Din_Go(2836,2048);ptr = (char *)type;
    ;/*702*/}/*700*/}}
    Din_Go(2839,2048);if (ptr != NULL)
        {/*703*/Din_Go(2838,2048);ptr += offset;/*704*/}
    {void ** ReplaceReturn32 = (void **)ptr; Din_Go(2840,2048); return ReplaceReturn32;}
}

/* Length of array of slotdef pointers used to store slots with the
   same __name__.  There should be at most MAX_EQUIV-1 slotdef entries with
   the same __name__, for any __name__. Since that's a static property, it is
   appropriate to declare fixed-size arrays for this. */
#define MAX_EQUIV 10

/* Return a slot pointer for a given name, but ONLY if the attribute has
   exactly one slot function.  The name must be an interned string. */
static void **
resolve_slotdups(PyTypeObject *type, PyObject *name)
{
    /* XXX Maybe this could be optimized more -- but is it worth it? */

    /* pname and ptrs act as a little cache */
    Din_Go(2841,2048);static PyObject *pname;
    static slotdef *ptrs[MAX_EQUIV];
    slotdef *p, **pp;
    void **res, **ptr;

    Din_Go(2847,2048);if (pname != name) {
        /* Collect all slotdefs that match name into ptrs. */
        Din_Go(2842,2048);pname = name;
        pp = ptrs;
        Din_Go(2845,2048);for (p = slotdefs; p->name_strobj; p++) {
            Din_Go(2843,2048);if (p->name_strobj == name)
                {/*705*/Din_Go(2844,2048);*pp++ = p;/*706*/}
        }
        Din_Go(2846,2048);*pp = NULL;
    }

    /* Look in all matching slots of the type; if exactly one of these has
       a filled-in slot, return its value.      Otherwise return NULL. */
    Din_Go(2848,2048);res = NULL;
    Din_Go(2854,2048);for (pp = ptrs; *pp; pp++) {
        Din_Go(2849,2048);ptr = slotptr(type, (*pp)->offset);
        Din_Go(2851,2048);if (ptr == NULL || *ptr == NULL)
            {/*707*/Din_Go(2850,2048);continue;/*708*/}
        Din_Go(2852,2048);if (res != NULL)
            return NULL;
        Din_Go(2853,2048);res = ptr;
    }
    {void ** ReplaceReturn31 = res; Din_Go(2855,2048); return ReplaceReturn31;}
}

/* Common code for update_slots_callback() and fixup_slot_dispatchers().  This
   does some incredibly complex thinking and then sticks something into the
   slot.  (It sees if the adjacent slotdefs for the same slot have conflicting
   interests, and then stores a generic wrapper or a specific function into
   the slot.)  Return a pointer to the next slotdef with a different offset,
   because that's convenient  for fixup_slot_dispatchers(). */
static slotdef *
update_one_slot(PyTypeObject *type, slotdef *p)
{
    Din_Go(2856,2048);PyObject *descr;
    PyWrapperDescrObject *d;
    void *generic = NULL, *specific = NULL;
    int use_generic = 0;
    int offset = p->offset;
    void **ptr = slotptr(type, offset);

    Din_Go(2860,2048);if (ptr == NULL) {
        Din_Go(2857,2048);do {
            Din_Go(2858,2048);++p;
        } while (p->offset == offset);
        {slotdef * ReplaceReturn30 = p; Din_Go(2859,2048); return ReplaceReturn30;}
    }
    Din_Go(2880,2048);do {
        Din_Go(2861,2048);descr = _PyType_Lookup(type, p->name_strobj);
        Din_Go(2865,2048);if (descr == NULL) {
            Din_Go(2862,2048);if (ptr == (void**)&type->tp_iternext) {
                Din_Go(2863,2048);specific = _PyObject_NextNotImplemented;
            }
            Din_Go(2864,2048);continue;
        }
        Din_Go(2879,2048);if (Py_TYPE(descr) == &PyWrapperDescr_Type &&
            ((PyWrapperDescrObject *)descr)->d_base->name_strobj == p->name_strobj) {
            Din_Go(2866,2048);void **tptr = resolve_slotdups(type, p->name_strobj);
            Din_Go(2868,2048);if (tptr == NULL || tptr == ptr)
                {/*709*/Din_Go(2867,2048);generic = p->function;/*710*/}
            Din_Go(2869,2048);d = (PyWrapperDescrObject *)descr;
            Din_Go(2873,2048);if (d->d_base->wrapper == p->wrapper &&
                PyType_IsSubtype(type, d->d_type))
            {
                Din_Go(2870,2048);if (specific == NULL ||
                    specific == d->d_wrapped)
                    {/*711*/Din_Go(2871,2048);specific = d->d_wrapped;/*712*/}
                else
                    {/*713*/Din_Go(2872,2048);use_generic = 1;/*714*/}
            }
        }
        else {/*715*/Din_Go(2874,2048);if (Py_TYPE(descr) == &PyCFunction_Type &&
                 PyCFunction_GET_FUNCTION(descr) ==
                 (PyCFunction)tp_new_wrapper &&
                 ptr == (void**)&type->tp_new)
        {
            /* The __new__ wrapper is not a wrapper descriptor,
               so must be special-cased differently.
               If we don't do this, creating an instance will
               always use slot_tp_new which will look up
               __new__ in the MRO which will call tp_new_wrapper
               which will look through the base classes looking
               for a static base and call its tp_new (usually
               PyType_GenericNew), after performing various
               sanity checks and constructing a new argument
               list.  Cut all that nonsense short -- this speeds
               up instance creation tremendously. */
            Din_Go(2875,2048);specific = (void *)type->tp_new;
            /* XXX I'm not 100% sure that there isn't a hole
               in this reasoning that requires additional
               sanity checks.  I'll buy the first person to
               point out a bug in this reasoning a beer. */
        }
        else {/*717*/Din_Go(2876,2048);if (descr == Py_None &&
                 ptr == (void**)&type->tp_hash) {
            /* We specifically allow __hash__ to be set to None
               to prevent inheritance of the default
               implementation from object.__hash__ */
            Din_Go(2877,2048);specific = PyObject_HashNotImplemented;
        }
        else {
            Din_Go(2878,2048);use_generic = 1;
            generic = p->function;
        ;/*718*/}/*716*/}}
    } while ((++p)->offset == offset);
    Din_Go(2883,2048);if (specific && !use_generic)
        {/*719*/Din_Go(2881,2048);*ptr = specific;/*720*/}
    else
        {/*721*/Din_Go(2882,2048);*ptr = generic;/*722*/}
    {slotdef * ReplaceReturn29 = p; Din_Go(2884,2048); return ReplaceReturn29;}
}

/* In the type, update the slots whose slotdefs are gathered in the pp array.
   This is a callback for update_subclasses(). */
static int
update_slots_callback(PyTypeObject *type, void *data)
{
    Din_Go(2885,2048);slotdef **pp = (slotdef **)data;

    Din_Go(2887,2048);for (; *pp; pp++)
        {/*723*/Din_Go(2886,2048);update_one_slot(type, *pp);/*724*/}
    {int  ReplaceReturn28 = 0; Din_Go(2888,2048); return ReplaceReturn28;}
}

/* Initialize the slotdefs table by adding interned string objects for the
   names and sorting the entries. */
static void
init_slotdefs(void)
{
    Din_Go(2889,2048);slotdef *p;
    static int initialized = 0;

    Din_Go(2891,2048);if (initialized)
        {/*725*/Din_Go(2890,2048);return;/*726*/}
    Din_Go(2895,2048);for (p = slotdefs; p->name; p++) {
        /* Slots must be ordered by their offset in the PyHeapTypeObject. */
        assert(!p[1].name || p->offset <= p[1].offset);
        Din_Go(2892,2048);p->name_strobj = PyString_InternFromString(p->name);
        Din_Go(2894,2048);if (!p->name_strobj)
            {/*727*/Din_Go(2893,2048);Py_FatalError("Out of memory interning slotdef names");/*728*/}
    }
    Din_Go(2896,2048);initialized = 1;
Din_Go(2897,2048);}

/* Update the slots after assignment to a class (type) attribute. */
static int
update_slot(PyTypeObject *type, PyObject *name)
{
    Din_Go(2898,2048);slotdef *ptrs[MAX_EQUIV];
    slotdef *p;
    slotdef **pp;
    int offset;

    /* Clear the VALID_VERSION flag of 'type' and all its
       subclasses.  This could possibly be unified with the
       update_subclasses() recursion below, but carefully:
       they each have their own conditions on which to stop
       recursing into subclasses. */
    PyType_Modified(type);

    init_slotdefs();
    pp = ptrs;
    Din_Go(2901,2048);for (p = slotdefs; p->name; p++) {
        /* XXX assume name is interned! */
        Din_Go(2899,2048);if (p->name_strobj == name)
            {/*221*/Din_Go(2900,2048);*pp++ = p;/*222*/}
    }
    Din_Go(2902,2048);*pp = NULL;
    Din_Go(2907,2048);for (pp = ptrs; *pp; pp++) {
        Din_Go(2903,2048);p = *pp;
        offset = p->offset;
        Din_Go(2905,2048);while (p > slotdefs && (p-1)->offset == offset)
            {/*223*/Din_Go(2904,2048);--p;/*224*/}
        Din_Go(2906,2048);*pp = p;
    }
    Din_Go(2909,2048);if (ptrs[0] == NULL)
        {/*225*/{int  ReplaceReturn27 = 0; Din_Go(2908,2048); return ReplaceReturn27;}/*226*/} /* Not an attribute that affects any slots */
    {int  ReplaceReturn26 = update_subclasses(type, name,
                             update_slots_callback, (void *)ptrs); Din_Go(2910,2048); return ReplaceReturn26;}
}

/* Store the proper functions in the slot dispatches at class (type)
   definition time, based upon which operations the class overrides in its
   dict. */
static void
fixup_slot_dispatchers(PyTypeObject *type)
{
    Din_Go(2911,2048);slotdef *p;

    init_slotdefs();
    Din_Go(2913,2048);for (p = slotdefs; p->name; )
        {/*227*/Din_Go(2912,2048);p = update_one_slot(type, p);/*228*/}
Din_Go(2914,2048);}

static void
update_all_slots(PyTypeObject* type)
{
    Din_Go(2915,2048);slotdef *p;

    init_slotdefs();
    Din_Go(2917,2048);for (p = slotdefs; p->name; p++) {
        /* update_slot returns int but can't actually fail */
        Din_Go(2916,2048);update_slot(type, p->name_strobj);
    }
Din_Go(2918,2048);}

/* recurse_down_subclasses() and update_subclasses() are mutually
   recursive functions to call a callback for all subclasses,
   but refraining from recursing into subclasses that define 'name'. */

static int
update_subclasses(PyTypeObject *type, PyObject *name,
                  update_callback callback, void *data)
{
    Din_Go(2919,2048);if (callback(type, data) < 0)
        {/*111*/{int  ReplaceReturn25 = -1; Din_Go(2920,2048); return ReplaceReturn25;}/*112*/}
    {int  ReplaceReturn24 = recurse_down_subclasses(type, name, callback, data); Din_Go(2921,2048); return ReplaceReturn24;}
}

static int
recurse_down_subclasses(PyTypeObject *type, PyObject *name,
                        update_callback callback, void *data)
{
    Din_Go(2922,2048);PyTypeObject *subclass;
    PyObject *ref, *subclasses, *dict;
    Py_ssize_t i, n;

    subclasses = type->tp_subclasses;
    Din_Go(2924,2048);if (subclasses == NULL)
        {/*113*/{int  ReplaceReturn23 = 0; Din_Go(2923,2048); return ReplaceReturn23;}/*114*/}
    assert(PyList_Check(subclasses));
    Din_Go(2925,2048);n = PyList_GET_SIZE(subclasses);
    Din_Go(2934,2048);for (i = 0; i < n; i++) {
        Din_Go(2926,2048);ref = PyList_GET_ITEM(subclasses, i);
        assert(PyWeakref_CheckRef(ref));
        subclass = (PyTypeObject *)PyWeakref_GET_OBJECT(ref);
        assert(subclass != NULL);
        Din_Go(2928,2048);if ((PyObject *)subclass == Py_None)
            {/*115*/Din_Go(2927,2048);continue;/*116*/}
        assert(PyType_Check(subclass));
        /* Avoid recursing down into unaffected classes */
        Din_Go(2929,2048);dict = subclass->tp_dict;
        Din_Go(2931,2048);if (dict != NULL && PyDict_Check(dict) &&
            PyDict_GetItem(dict, name) != NULL)
            {/*117*/Din_Go(2930,2048);continue;/*118*/}
        Din_Go(2933,2048);if (update_subclasses(subclass, name, callback, data) < 0)
            {/*119*/{int  ReplaceReturn22 = -1; Din_Go(2932,2048); return ReplaceReturn22;}/*120*/}
    }
    {int  ReplaceReturn21 = 0; Din_Go(2935,2048); return ReplaceReturn21;}
}

/* This function is called by PyType_Ready() to populate the type's
   dictionary with method descriptors for function slots.  For each
   function slot (like tp_repr) that's defined in the type, one or more
   corresponding descriptors are added in the type's tp_dict dictionary
   under the appropriate name (like __repr__).  Some function slots
   cause more than one descriptor to be added (for example, the nb_add
   slot adds both __add__ and __radd__ descriptors) and some function
   slots compete for the same descriptor (for example both sq_item and
   mp_subscript generate a __getitem__ descriptor).

   In the latter case, the first slotdef entry encountered wins.  Since
   slotdef entries are sorted by the offset of the slot in the
   PyHeapTypeObject, this gives us some control over disambiguating
   between competing slots: the members of PyHeapTypeObject are listed
   from most general to least general, so the most general slot is
   preferred.  In particular, because as_mapping comes before as_sequence,
   for a type that defines both mp_subscript and sq_item, mp_subscript
   wins.

   This only adds new descriptors and doesn't overwrite entries in
   tp_dict that were previously defined.  The descriptors contain a
   reference to the C function they must call, so that it's safe if they
   are copied into a subtype's __dict__ and the subtype has a different
   C function in its slot -- calling the method defined by the
   descriptor will call the C function that was used to create it,
   rather than the C function present in the slot when it is called.
   (This is important because a subtype may have a C function in the
   slot that calls the method from the dictionary, and we want to avoid
   infinite recursion here.) */

static int
add_operators(PyTypeObject *type)
{
    Din_Go(2936,2048);PyObject *dict = type->tp_dict;
    slotdef *p;
    PyObject *descr;
    void **ptr;

    init_slotdefs();
    Din_Go(2952,2048);for (p = slotdefs; p->name; p++) {
        Din_Go(2937,2048);if (p->wrapper == NULL)
            {/*535*/Din_Go(2938,2048);continue;/*536*/}
        Din_Go(2939,2048);ptr = slotptr(type, p->offset);
        Din_Go(2941,2048);if (!ptr || !*ptr)
            {/*537*/Din_Go(2940,2048);continue;/*538*/}
        Din_Go(2943,2048);if (PyDict_GetItem(dict, p->name_strobj))
            {/*539*/Din_Go(2942,2048);continue;/*540*/}
        Din_Go(2951,2048);if (*ptr == PyObject_HashNotImplemented) {
            /* Classes may prevent the inheritance of the tp_hash
               slot by storing PyObject_HashNotImplemented in it. Make it
               visible as a None value for the __hash__ attribute. */
            Din_Go(2944,2048);if (PyDict_SetItem(dict, p->name_strobj, Py_None) < 0)
                {/*541*/{int  ReplaceReturn20 = -1; Din_Go(2945,2048); return ReplaceReturn20;}/*542*/}
        }
        else {
            Din_Go(2946,2048);descr = PyDescr_NewWrapper(type, p, *ptr);
            Din_Go(2948,2048);if (descr == NULL)
                {/*543*/{int  ReplaceReturn19 = -1; Din_Go(2947,2048); return ReplaceReturn19;}/*544*/}
            Din_Go(2950,2048);if (PyDict_SetItem(dict, p->name_strobj, descr) < 0)
                {/*545*/{int  ReplaceReturn18 = -1; Din_Go(2949,2048); return ReplaceReturn18;}/*546*/}
            Py_DECREF(descr);
        }
    }
    Din_Go(2955,2048);if (type->tp_new != NULL) {
        Din_Go(2953,2048);if (add_tp_new_wrapper(type) < 0)
            {/*547*/{int  ReplaceReturn17 = -1; Din_Go(2954,2048); return ReplaceReturn17;}/*548*/}
    }
    {int  ReplaceReturn16 = 0; Din_Go(2956,2048); return ReplaceReturn16;}
}


/* Cooperative 'super' */

typedef struct {
    PyObject_HEAD
    PyTypeObject *type;
    PyObject *obj;
    PyTypeObject *obj_type;
} superobject;

static PyMemberDef super_members[] = {
    {"__thisclass__", T_OBJECT, offsetof(superobject, type), READONLY,
     "the class invoking super()"},
    {"__self__",  T_OBJECT, offsetof(superobject, obj), READONLY,
     "the instance invoking super(); may be None"},
    {"__self_class__", T_OBJECT, offsetof(superobject, obj_type), READONLY,
     "the type of the instance invoking super(); may be None"},
    {0}
};

static void
super_dealloc(PyObject *self)
{
    Din_Go(2957,2048);superobject *su = (superobject *)self;

    _PyObject_GC_UNTRACK(self);
    Py_XDECREF(su->obj);
    Py_XDECREF(su->type);
    Py_XDECREF(su->obj_type);
    Py_TYPE(self)->tp_free(self);
}

static PyObject *
super_repr(PyObject *self)
{
    Din_Go(2958,2048);superobject *su = (superobject *)self;

    Din_Go(2961,2048);if (su->obj_type)
        {/*729*/{PyObject * ReplaceReturn15 = PyString_FromFormat(
            "<super: <class '%s'>, <%s object>>",
            su->type ? su->type->tp_name : "NULL",
            su->obj_type->tp_name); Din_Go(2959,2048); return ReplaceReturn15;}/*730*/}
    else
        {/*731*/{PyObject * ReplaceReturn14 = PyString_FromFormat(
            "<super: <class '%s'>, NULL>",
            su->type ? su->type->tp_name : "NULL"); Din_Go(2960,2048); return ReplaceReturn14;}/*732*/}
Din_Go(2962,2048);}

static PyObject *
super_getattro(PyObject *self, PyObject *name)
{
    Din_Go(2963,2048);superobject *su = (superobject *)self;
    int skip = su->obj_type == NULL;

    Din_Go(2965,2048);if (!skip) {
        /* We want __class__ to return the class of the super object
           (i.e. super, or a subclass), not the class of su->obj. */
        Din_Go(2964,2048);skip = (PyString_Check(name) &&
            PyString_GET_SIZE(name) == 9 &&
            strcmp(PyString_AS_STRING(name), "__class__") == 0);
    }

    Din_Go(2987,2048);if (!skip) {
        Din_Go(2966,2048);PyObject *mro, *res, *tmp, *dict;
        PyTypeObject *starttype;
        descrgetfunc f;
        Py_ssize_t i, n;

        starttype = su->obj_type;
        mro = starttype->tp_mro;

        Din_Go(2969,2048);if (mro == NULL)
            {/*733*/Din_Go(2967,2048);n = 0;/*734*/}
        else {
            assert(PyTuple_Check(mro));
            Din_Go(2968,2048);n = PyTuple_GET_SIZE(mro);
        }
        Din_Go(2972,2048);for (i = 0; i < n; i++) {
            Din_Go(2970,2048);if ((PyObject *)(su->type) == PyTuple_GET_ITEM(mro, i))
                {/*735*/Din_Go(2971,2048);break;/*736*/}
        }
        Din_Go(2973,2048);i++;
        res = NULL;
        Din_Go(2986,2048);for (; i < n; i++) {
            Din_Go(2974,2048);tmp = PyTuple_GET_ITEM(mro, i);
            Din_Go(2979,2048);if (PyType_Check(tmp))
                {/*737*/Din_Go(2975,2048);dict = ((PyTypeObject *)tmp)->tp_dict;/*738*/}
            else {/*739*/Din_Go(2976,2048);if (PyClass_Check(tmp))
                {/*741*/Din_Go(2977,2048);dict = ((PyClassObject *)tmp)->cl_dict;/*742*/}
            else
                {/*743*/Din_Go(2978,2048);continue;/*744*/}/*740*/}
            Din_Go(2980,2048);res = PyDict_GetItem(dict, name);
            Din_Go(2985,2048);if (res != NULL) {
                Py_INCREF(res);
                Din_Go(2981,2048);f = Py_TYPE(res)->tp_descr_get;
                Din_Go(2983,2048);if (f != NULL) {
                    Din_Go(2982,2048);tmp = f(res,
                        /* Only pass 'obj' param if
                           this is instance-mode super
                           (See SF ID #743627)
                        */
                        (su->obj == (PyObject *)
                                    su->obj_type
                            ? (PyObject *)NULL
                            : su->obj),
                        (PyObject *)starttype);
                    Py_DECREF(res);
                    res = tmp;
                }
                {PyObject * ReplaceReturn13 = res; Din_Go(2984,2048); return ReplaceReturn13;}
            }
        }
    }
    {PyObject * ReplaceReturn12 = PyObject_GenericGetAttr(self, name); Din_Go(2988,2048); return ReplaceReturn12;}
}

static PyTypeObject *
supercheck(PyTypeObject *type, PyObject *obj)
{
    /* Check that a super() call makes sense.  Return a type object.

       obj can be a new-style class, or an instance of one:

       - If it is a class, it must be a subclass of 'type'.      This case is
         used for class methods; the return value is obj.

       - If it is an instance, it must be an instance of 'type'.  This is
         the normal case; the return value is obj.__class__.

       But... when obj is an instance, we want to allow for the case where
       Py_TYPE(obj) is not a subclass of type, but obj.__class__ is!
       This will allow using super() with a proxy for obj.
    */

    /* Check for first bullet above (special case) */
    Din_Go(2989,2048);if (PyType_Check(obj) && PyType_IsSubtype((PyTypeObject *)obj, type)) {
        Py_INCREF(obj);
        {PyTypeObject * ReplaceReturn11 = (PyTypeObject *)obj; Din_Go(2990,2048); return ReplaceReturn11;}
    }

    /* Normal case */
    Din_Go(3003,2048);if (PyType_IsSubtype(Py_TYPE(obj), type)) {
        Py_INCREF(Py_TYPE(obj));
        {PyTypeObject * ReplaceReturn10 = Py_TYPE(obj); Din_Go(2991,2048); return ReplaceReturn10;}
    }
    else {
        /* Try the slow way */
        Din_Go(2992,2048);static PyObject *class_str = NULL;
        PyObject *class_attr;

        Din_Go(2995,2048);if (class_str == NULL) {
            Din_Go(2993,2048);class_str = PyString_FromString("__class__");
            Din_Go(2994,2048);if (class_str == NULL)
                return NULL;
        }

        Din_Go(2996,2048);class_attr = PyObject_GetAttr(obj, class_str);

        Din_Go(3000,2048);if (class_attr != NULL &&
            PyType_Check(class_attr) &&
            (PyTypeObject *)class_attr != Py_TYPE(obj))
        {
            Din_Go(2997,2048);int ok = PyType_IsSubtype(
                (PyTypeObject *)class_attr, type);
            Din_Go(2999,2048);if (ok)
                {/*745*/{PyTypeObject * ReplaceReturn9 = (PyTypeObject *)class_attr; Din_Go(2998,2048); return ReplaceReturn9;}/*746*/}
        }

        Din_Go(3002,2048);if (class_attr == NULL)
            {/*747*/Din_Go(3001,2048);PyErr_Clear();/*748*/}
        else
            Py_DECREF(class_attr);
    }

    Din_Go(3004,2048);PyErr_SetString(PyExc_TypeError,
                    "super(type, obj): "
                    "obj must be an instance or subtype of type");
    {PyTypeObject * ReplaceReturn8 = NULL; Din_Go(3005,2048); return ReplaceReturn8;}
}

static PyObject *
super_descr_get(PyObject *self, PyObject *obj, PyObject *type)
{
    Din_Go(3006,2048);superobject *su = (superobject *)self;
    superobject *newobj;

    Din_Go(3008,2048);if (obj == NULL || obj == Py_None || su->obj != NULL) {
        /* Not binding to an object, or already bound */
        Py_INCREF(self);
        {PyObject * ReplaceReturn7 = self; Din_Go(3007,2048); return ReplaceReturn7;}
    }
    Din_Go(3016,2048);if (Py_TYPE(su) != &PySuper_Type)
        /* If su is an instance of a (strict) subclass of super,
           call its type */
        {/*749*/{PyObject * ReplaceReturn6 = PyObject_CallFunctionObjArgs((PyObject *)Py_TYPE(su),
                                            su->type, obj, NULL); Din_Go(3009,2048); return ReplaceReturn6;}/*750*/}
    else {
        /* Inline the common case */
        Din_Go(3010,2048);PyTypeObject *obj_type = supercheck(su->type, obj);
        Din_Go(3011,2048);if (obj_type == NULL)
            return NULL;
        Din_Go(3012,2048);newobj = (superobject *)PySuper_Type.tp_new(&PySuper_Type,
                                                 NULL, NULL);
        Din_Go(3013,2048);if (newobj == NULL)
            return NULL;
        Py_INCREF(su->type);
        Py_INCREF(obj);
        Din_Go(3014,2048);newobj->type = su->type;
        newobj->obj = obj;
        newobj->obj_type = obj_type;
        {PyObject * ReplaceReturn5 = (PyObject *)newobj; Din_Go(3015,2048); return ReplaceReturn5;}
    }
Din_Go(3017,2048);}

static int
super_init(PyObject *self, PyObject *args, PyObject *kwds)
{
    Din_Go(3018,2048);superobject *su = (superobject *)self;
    PyTypeObject *type;
    PyObject *obj = NULL;
    PyTypeObject *obj_type = NULL;

    Din_Go(3020,2048);if (!_PyArg_NoKeywords("super", kwds))
        {/*751*/{int  ReplaceReturn4 = -1; Din_Go(3019,2048); return ReplaceReturn4;}/*752*/}
    Din_Go(3022,2048);if (!PyArg_ParseTuple(args, "O!|O:super", &PyType_Type, &type, &obj))
        {/*753*/{int  ReplaceReturn3 = -1; Din_Go(3021,2048); return ReplaceReturn3;}/*754*/}
    Din_Go(3023,2048);if (obj == Py_None)
        obj = NULL;
    Din_Go(3027,2048);if (obj != NULL) {
        Din_Go(3024,2048);obj_type = supercheck(type, obj);
        Din_Go(3026,2048);if (obj_type == NULL)
            {/*755*/{int  ReplaceReturn2 = -1; Din_Go(3025,2048); return ReplaceReturn2;}/*756*/}
        Py_INCREF(obj);
    }
    Py_INCREF(type);
    Din_Go(3028,2048);su->type = type;
    su->obj = obj;
    su->obj_type = obj_type;
    {int  ReplaceReturn1 = 0; Din_Go(3029,2048); return ReplaceReturn1;}
}

PyDoc_STRVAR(super_doc,
"super(type, obj) -> bound super object; requires isinstance(obj, type)\n"
"super(type) -> unbound super object\n"
"super(type, type2) -> bound super object; requires issubclass(type2, type)\n"
"Typical use to call a cooperative superclass method:\n"
"class C(B):\n"
"    def meth(self, arg):\n"
"        super(C, self).meth(arg)");

static int
super_traverse(PyObject *self, visitproc visit, void *arg)
{
    Din_Go(3030,2048);superobject *su = (superobject *)self;

    Py_VISIT(su->obj);
    Py_VISIT(su->type);
    Py_VISIT(su->obj_type);

    {int  ReplaceReturn0 = 0; Din_Go(3031,2048); return ReplaceReturn0;}
}

PyTypeObject PySuper_Type = {
    PyVarObject_HEAD_INIT(&PyType_Type, 0)
    "super",                                    /* tp_name */
    sizeof(superobject),                        /* tp_basicsize */
    0,                                          /* tp_itemsize */
    /* methods */
    super_dealloc,                              /* tp_dealloc */
    0,                                          /* tp_print */
    0,                                          /* tp_getattr */
    0,                                          /* tp_setattr */
    0,                                          /* tp_compare */
    super_repr,                                 /* tp_repr */
    0,                                          /* tp_as_number */
    0,                                          /* tp_as_sequence */
    0,                                          /* tp_as_mapping */
    0,                                          /* tp_hash */
    0,                                          /* tp_call */
    0,                                          /* tp_str */
    super_getattro,                             /* tp_getattro */
    0,                                          /* tp_setattro */
    0,                                          /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_HAVE_GC |
        Py_TPFLAGS_BASETYPE,                    /* tp_flags */
    super_doc,                                  /* tp_doc */
    super_traverse,                             /* tp_traverse */
    0,                                          /* tp_clear */
    0,                                          /* tp_richcompare */
    0,                                          /* tp_weaklistoffset */
    0,                                          /* tp_iter */
    0,                                          /* tp_iternext */
    0,                                          /* tp_methods */
    super_members,                              /* tp_members */
    0,                                          /* tp_getset */
    0,                                          /* tp_base */
    0,                                          /* tp_dict */
    super_descr_get,                            /* tp_descr_get */
    0,                                          /* tp_descr_set */
    0,                                          /* tp_dictoffset */
    super_init,                                 /* tp_init */
    PyType_GenericAlloc,                        /* tp_alloc */
    PyType_GenericNew,                          /* tp_new */
    PyObject_GC_Del,                            /* tp_free */
};
