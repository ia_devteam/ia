
/* Tokenizer implementation */

#include "Python.h"
#include "var/tmp/sensor.h"
#include "pgenheaders.h"

#include <ctype.h>
#include <assert.h>

#include "tokenizer.h"
#include "errcode.h"

#ifndef PGEN
#include "unicodeobject.h"
#include "stringobject.h"
#include "fileobject.h"
#include "codecs.h"
#include "abstract.h"
#include "pydebug.h"
#endif /* PGEN */

extern char *PyOS_Readline(FILE *, FILE *, char *);
/* Return malloc'ed string including trailing \n;
   empty malloc'ed string for EOF;
   NULL if interrupted */

/* Don't ever change this -- it would break the portability of Python code */
#define TABSIZE 8

/* Forward */
static struct tok_state *tok_new(void);
static int tok_nextc(struct tok_state *tok);
static void tok_backup(struct tok_state *tok, int c);

/* Token names */

char *_PyParser_TokenNames[] = {
    "ENDMARKER",
    "NAME",
    "NUMBER",
    "STRING",
    "NEWLINE",
    "INDENT",
    "DEDENT",
    "LPAR",
    "RPAR",
    "LSQB",
    "RSQB",
    "COLON",
    "COMMA",
    "SEMI",
    "PLUS",
    "MINUS",
    "STAR",
    "SLASH",
    "VBAR",
    "AMPER",
    "LESS",
    "GREATER",
    "EQUAL",
    "DOT",
    "PERCENT",
    "BACKQUOTE",
    "LBRACE",
    "RBRACE",
    "EQEQUAL",
    "NOTEQUAL",
    "LESSEQUAL",
    "GREATEREQUAL",
    "TILDE",
    "CIRCUMFLEX",
    "LEFTSHIFT",
    "RIGHTSHIFT",
    "DOUBLESTAR",
    "PLUSEQUAL",
    "MINEQUAL",
    "STAREQUAL",
    "SLASHEQUAL",
    "PERCENTEQUAL",
    "AMPEREQUAL",
    "VBAREQUAL",
    "CIRCUMFLEXEQUAL",
    "LEFTSHIFTEQUAL",
    "RIGHTSHIFTEQUAL",
    "DOUBLESTAREQUAL",
    "DOUBLESLASH",
    "DOUBLESLASHEQUAL",
    "AT",
    /* This table must match the #defines in token.h! */
    "OP",
    "<ERRORTOKEN>",
    "<N_TOKENS>"
};

/* Create and initialize a new tok_state structure */

static struct tok_state *
tok_new(void)
{
    Din_Go(3032,2048);struct tok_state *tok = (struct tok_state *)PyMem_MALLOC(
                                            sizeof(struct tok_state));
    Din_Go(3033,2048);if (tok == NULL)
        return NULL;
    Din_Go(3034,2048);tok->buf = tok->cur = tok->end = tok->inp = tok->start = NULL;
    tok->done = E_OK;
    tok->fp = NULL;
    tok->input = NULL;
    tok->tabsize = TABSIZE;
    tok->indent = 0;
    tok->indstack[0] = 0;
    tok->atbol = 1;
    tok->pendin = 0;
    tok->prompt = tok->nextprompt = NULL;
    tok->lineno = 0;
    tok->level = 0;
    tok->filename = NULL;
    tok->altwarning = 0;
    tok->alterror = 0;
    tok->alttabsize = 1;
    tok->altindstack[0] = 0;
    tok->decoding_state = 0;
    tok->decoding_erred = 0;
    tok->read_coding_spec = 0;
    tok->encoding = NULL;
    tok->cont_line = 0;
#ifndef PGEN
    tok->decoding_readline = NULL;
    tok->decoding_buffer = NULL;
#endif
    {__IASTRUCT__ tok_state * ReplaceReturn720 = tok; Din_Go(3035,2048); return ReplaceReturn720;}
}

static char *
new_string(const char *s, Py_ssize_t len)
{
    Din_Go(3036,2048);char* result = (char *)PyMem_MALLOC(len + 1);
    Din_Go(3038,2048);if (result != NULL) {
        Din_Go(3037,2048);memcpy(result, s, len);
        result[len] = '\0';
    }
    {char * ReplaceReturn719 = result; Din_Go(3039,2048); return ReplaceReturn719;}
}

#ifdef PGEN

static char *
decoding_fgets(char *s, int size, struct tok_state *tok)
{
    return fgets(s, size, tok->fp);
}

static int
decoding_feof(struct tok_state *tok)
{
    return feof(tok->fp);
}

static char *
decode_str(const char *str, int exec_input, struct tok_state *tok)
{
    return new_string(str, strlen(str));
}

#else /* PGEN */

static char *
error_ret(struct tok_state *tok) /* XXX */
{
    Din_Go(3040,2048);tok->decoding_erred = 1;
    Din_Go(3041,2048);if (tok->fp != NULL && tok->buf != NULL) /* see PyTokenizer_Free */
        PyMem_FREE(tok->buf);
    Din_Go(3042,2048);tok->buf = NULL;
    {char * ReplaceReturn718 = NULL; Din_Go(3043,2048); return ReplaceReturn718;}                /* as if it were EOF */
}


static char *
get_normal_name(char *s)        /* for utf-8 and latin-1 */
{
    Din_Go(3044,2048);char buf[13];
    int i;
    Din_Go(3048,2048);for (i = 0; i < 12; i++) {
        Din_Go(3045,2048);int c = s[i];
        Din_Go(3047,2048);if (c == '\0')
            {/*19*/Din_Go(3046,2048);break;/*20*/}
        else if (c == '_')
            buf[i] = '-';
        else
            buf[i] = tolower(c);
    }
    Din_Go(3049,2048);buf[i] = '\0';
    Din_Go(3054,2048);if (strcmp(buf, "utf-8") == 0 ||
        strncmp(buf, "utf-8-", 6) == 0)
        {/*21*/{char * ReplaceReturn717 = "utf-8"; Din_Go(3050,2048); return ReplaceReturn717;}/*22*/}
    else {/*23*/Din_Go(3051,2048);if (strcmp(buf, "latin-1") == 0 ||
             strcmp(buf, "iso-8859-1") == 0 ||
             strcmp(buf, "iso-latin-1") == 0 ||
             strncmp(buf, "latin-1-", 8) == 0 ||
             strncmp(buf, "iso-8859-1-", 11) == 0 ||
             strncmp(buf, "iso-latin-1-", 12) == 0)
        {/*25*/{char * ReplaceReturn716 = "iso-8859-1"; Din_Go(3052,2048); return ReplaceReturn716;}/*26*/}
    else
        {/*27*/{char * ReplaceReturn715 = s; Din_Go(3053,2048); return ReplaceReturn715;}/*28*/}/*24*/}
Din_Go(3055,2048);}

/* Return the coding spec in S, or NULL if none is found.  */

static char *
get_coding_spec(const char *s, Py_ssize_t size)
{
    Din_Go(3056,2048);Py_ssize_t i;
    /* Coding spec must be in a comment, and that comment must be
     * the only statement on the source code line. */
    Din_Go(3060,2048);for (i = 0; i < size - 6; i++) {
        Din_Go(3057,2048);if (s[i] == '#')
            {/*29*/Din_Go(3058,2048);break;/*30*/}
        Din_Go(3059,2048);if (s[i] != ' ' && s[i] != '\t' && s[i] != '\014')
            return NULL;
    }
    Din_Go(3076,2048);for (; i < size - 6; i++) { /* XXX inefficient search */
        Din_Go(3061,2048);const char* t = s + i;
        Din_Go(3075,2048);if (strncmp(t, "coding", 6) == 0) {
            Din_Go(3062,2048);const char* begin = NULL;
            t += 6;
            Din_Go(3064,2048);if (t[0] != ':' && t[0] != '=')
                {/*31*/Din_Go(3063,2048);continue;/*32*/}
            Din_Go(3066,2048);do {
                Din_Go(3065,2048);t++;
            } while (t[0] == '\x20' || t[0] == '\t');

            Din_Go(3067,2048);begin = t;
            Din_Go(3069,2048);while (Py_ISALNUM(t[0]) ||
                   t[0] == '-' || t[0] == '_' || t[0] == '.')
                {/*33*/Din_Go(3068,2048);t++;/*34*/}

            Din_Go(3074,2048);if (begin < t) {
                Din_Go(3070,2048);char* r = new_string(begin, t - begin);
                char* q = get_normal_name(r);
                Din_Go(3072,2048);if (r != q) {
                    PyMem_FREE(r);
                    Din_Go(3071,2048);r = new_string(q, strlen(q));
                }
                {char * ReplaceReturn714 = r; Din_Go(3073,2048); return ReplaceReturn714;}
            }
        }
    }
    {char * ReplaceReturn713 = NULL; Din_Go(3077,2048); return ReplaceReturn713;}
}

/* Check whether the line contains a coding spec. If it does,
   invoke the set_readline function for the new encoding.
   This function receives the tok_state and the new encoding.
   Return 1 on success, 0 on failure.  */

static int
check_coding_spec(const char* line, Py_ssize_t size, struct tok_state *tok,
                  int set_readline(struct tok_state *, const char *))
{
    Din_Go(3078,2048);char * cs;
    int r = 1;

    Din_Go(3080,2048);if (tok->cont_line)
        /* It's a continuation line, so it can't be a coding spec. */
        {/*35*/{int  ReplaceReturn712 = 1; Din_Go(3079,2048); return ReplaceReturn712;}/*36*/}
    Din_Go(3081,2048);cs = get_coding_spec(line, size);
    Din_Go(3093,2048);if (cs != NULL) {
        Din_Go(3082,2048);tok->read_coding_spec = 1;
        Din_Go(3092,2048);if (tok->encoding == NULL) {
            assert(tok->decoding_state == 1); /* raw */
            Din_Go(3088,2048);if (strcmp(cs, "utf-8") == 0 ||
                strcmp(cs, "iso-8859-1") == 0) {
                Din_Go(3083,2048);tok->encoding = cs;
            } else {
#ifdef Py_USING_UNICODE
                Din_Go(3084,2048);r = set_readline(tok, cs);
                Din_Go(3087,2048);if (r) {
                    Din_Go(3085,2048);tok->encoding = cs;
                    tok->decoding_state = -1;
                }
                else {
                    Din_Go(3086,2048);PyErr_Format(PyExc_SyntaxError,
                                 "encoding problem: %s", cs);
                    PyMem_FREE(cs);
                }
#else
                /* Without Unicode support, we cannot
                   process the coding spec. Since there
                   won't be any Unicode literals, that
                   won't matter. */
                PyMem_FREE(cs);
#endif
            }
        } else {                /* then, compare cs with BOM */
            Din_Go(3089,2048);r = (strcmp(tok->encoding, cs) == 0);
            Din_Go(3091,2048);if (!r)
                {/*37*/Din_Go(3090,2048);PyErr_Format(PyExc_SyntaxError,
                             "encoding problem: %s with BOM", cs);/*38*/}
            PyMem_FREE(cs);
        }
    }
    {int  ReplaceReturn711 = r; Din_Go(3094,2048); return ReplaceReturn711;}
}

/* See whether the file starts with a BOM. If it does,
   invoke the set_readline function with the new encoding.
   Return 1 on success, 0 on failure.  */

static int
check_bom(int get_char(struct tok_state *),
          void unget_char(int, struct tok_state *),
          int set_readline(struct tok_state *, const char *),
          struct tok_state *tok)
{
    Din_Go(3095,2048);int ch1, ch2, ch3;
    ch1 = get_char(tok);
    tok->decoding_state = 1;
    Din_Go(3108,2048);if (ch1 == EOF) {
        {int  ReplaceReturn710 = 1; Din_Go(3096,2048); return ReplaceReturn710;}
    } else {/*39*/Din_Go(3097,2048);if (ch1 == 0xEF) {
        Din_Go(3098,2048);ch2 = get_char(tok);
        Din_Go(3101,2048);if (ch2 != 0xBB) {
            Din_Go(3099,2048);unget_char(ch2, tok);
            unget_char(ch1, tok);
            {int  ReplaceReturn709 = 1; Din_Go(3100,2048); return ReplaceReturn709;}
        }
        Din_Go(3102,2048);ch3 = get_char(tok);
        Din_Go(3105,2048);if (ch3 != 0xBF) {
            Din_Go(3103,2048);unget_char(ch3, tok);
            unget_char(ch2, tok);
            unget_char(ch1, tok);
            {int  ReplaceReturn708 = 1; Din_Go(3104,2048); return ReplaceReturn708;}
        }
#if 0
    /* Disable support for UTF-16 BOMs until a decision
       is made whether this needs to be supported.  */
    } else if (ch1 == 0xFE) {
        ch2 = get_char(tok);
        if (ch2 != 0xFF) {
            unget_char(ch2, tok);
            unget_char(ch1, tok);
            return 1;
        }
        if (!set_readline(tok, "utf-16-be"))
            return 0;
        tok->decoding_state = -1;
    } else if (ch1 == 0xFF) {
        ch2 = get_char(tok);
        if (ch2 != 0xFE) {
            unget_char(ch2, tok);
            unget_char(ch1, tok);
            return 1;
        }
        if (!set_readline(tok, "utf-16-le"))
            return 0;
        tok->decoding_state = -1;
#endif
    } else {
        Din_Go(3106,2048);unget_char(ch1, tok);
        {int  ReplaceReturn707 = 1; Din_Go(3107,2048); return ReplaceReturn707;}
    ;/*40*/}}
    Din_Go(3109,2048);if (tok->encoding != NULL)
        PyMem_FREE(tok->encoding);
    Din_Go(3110,2048);tok->encoding = new_string("utf-8", 5);     /* resulting is in utf-8 */
    {int  ReplaceReturn706 = 1; Din_Go(3111,2048); return ReplaceReturn706;}
}

/* Read a line of text from TOK into S, using the stream in TOK.
   Return NULL on failure, else S.

   On entry, tok->decoding_buffer will be one of:
     1) NULL: need to call tok->decoding_readline to get a new line
     2) PyUnicodeObject *: decoding_feof has called tok->decoding_readline and
       stored the result in tok->decoding_buffer
     3) PyStringObject *: previous call to fp_readl did not have enough room
       (in the s buffer) to copy entire contents of the line read
       by tok->decoding_readline.  tok->decoding_buffer has the overflow.
       In this case, fp_readl is called in a loop (with an expanded buffer)
       until the buffer ends with a '\n' (or until the end of the file is
       reached): see tok_nextc and its calls to decoding_fgets.
*/

static char *
fp_readl(char *s, int size, struct tok_state *tok)
{
#ifndef Py_USING_UNICODE
    /* In a non-Unicode built, this should never be called. */
    Py_FatalError("fp_readl should not be called in this build.");
    return NULL; /* Keep compiler happy (not reachable) */
#else
    Din_Go(3112,2048);PyObject* utf8 = NULL;
    PyObject* buf = tok->decoding_buffer;
    char *str;
    Py_ssize_t utf8len;

    /* Ask for one less byte so we can terminate it */
    assert(size > 0);
    size--;

    Din_Go(3122,2048);if (buf == NULL) {
        Din_Go(3113,2048);buf = PyObject_CallObject(tok->decoding_readline, NULL);
        Din_Go(3115,2048);if (buf == NULL)
            {/*41*/{char * ReplaceReturn705 = error_ret(tok); Din_Go(3114,2048); return ReplaceReturn705;}/*42*/}
        Din_Go(3118,2048);if (!PyUnicode_Check(buf)) {
            Py_DECREF(buf);
            Din_Go(3116,2048);PyErr_SetString(PyExc_SyntaxError,
                            "codec did not return a unicode object");
            {char * ReplaceReturn704 = error_ret(tok); Din_Go(3117,2048); return ReplaceReturn704;}
        }
    } else {
        Din_Go(3119,2048);tok->decoding_buffer = NULL;
        Din_Go(3121,2048);if (PyString_CheckExact(buf))
            {/*43*/Din_Go(3120,2048);utf8 = buf;/*44*/}
    }
    Din_Go(3126,2048);if (utf8 == NULL) {
        Din_Go(3123,2048);utf8 = PyUnicode_AsUTF8String(buf);
        Py_DECREF(buf);
        Din_Go(3125,2048);if (utf8 == NULL)
            {/*45*/{char * ReplaceReturn703 = error_ret(tok); Din_Go(3124,2048); return ReplaceReturn703;}/*46*/}
    }
    Din_Go(3127,2048);str = PyString_AsString(utf8);
    utf8len = PyString_GET_SIZE(utf8);
    Din_Go(3132,2048);if (utf8len > size) {
        Din_Go(3128,2048);tok->decoding_buffer = PyString_FromStringAndSize(str+size, utf8len-size);
        Din_Go(3130,2048);if (tok->decoding_buffer == NULL) {
            Py_DECREF(utf8);
            {char * ReplaceReturn702 = error_ret(tok); Din_Go(3129,2048); return ReplaceReturn702;}
        }
        Din_Go(3131,2048);utf8len = size;
    }
    Din_Go(3133,2048);memcpy(s, str, utf8len);
    s[utf8len] = '\0';
    Py_DECREF(utf8);
    Din_Go(3134,2048);if (utf8len == 0)
        return NULL; /* EOF */
    {char * ReplaceReturn701 = s; Din_Go(3135,2048); return ReplaceReturn701;}
#endif
}

/* Set the readline function for TOK to a StreamReader's
   readline function. The StreamReader is named ENC.

   This function is called from check_bom and check_coding_spec.

   ENC is usually identical to the future value of tok->encoding,
   except for the (currently unsupported) case of UTF-16.

   Return 1 on success, 0 on failure. */

static int
fp_setreadl(struct tok_state *tok, const char* enc)
{
    Din_Go(3136,2048);PyObject *reader, *stream, *readline;

    /* XXX: constify filename argument. */
    stream = PyFile_FromFile(tok->fp, (char*)tok->filename, "rb", NULL);
    Din_Go(3138,2048);if (stream == NULL)
        {/*47*/{int  ReplaceReturn700 = 0; Din_Go(3137,2048); return ReplaceReturn700;}/*48*/}

    Din_Go(3139,2048);reader = PyCodec_StreamReader(enc, stream, NULL);
    Py_DECREF(stream);
    Din_Go(3141,2048);if (reader == NULL)
        {/*49*/{int  ReplaceReturn699 = 0; Din_Go(3140,2048); return ReplaceReturn699;}/*50*/}

    Din_Go(3142,2048);readline = PyObject_GetAttrString(reader, "readline");
    Py_DECREF(reader);
    Din_Go(3144,2048);if (readline == NULL)
        {/*51*/{int  ReplaceReturn698 = 0; Din_Go(3143,2048); return ReplaceReturn698;}/*52*/}

    Din_Go(3145,2048);tok->decoding_readline = readline;
    {int  ReplaceReturn697 = 1; Din_Go(3146,2048); return ReplaceReturn697;}
}

/* Fetch the next byte from TOK. */

static int fp_getc(struct tok_state *tok) {
    {int  ReplaceReturn696 = getc(tok->fp); Din_Go(3147,2048); return ReplaceReturn696;}
}

/* Unfetch the last byte back into TOK.  */

static void fp_ungetc(int c, struct tok_state *tok) {
    Din_Go(3148,2048);ungetc(c, tok->fp);
Din_Go(3149,2048);}

/* Read a line of input from TOK. Determine encoding
   if necessary.  */

static char *
decoding_fgets(char *s, int size, struct tok_state *tok)
{
    Din_Go(3150,2048);char *line = NULL;
    int badchar = 0;
    Din_Go(3159,2048);for (;;) {
        Din_Go(3151,2048);if (tok->decoding_state < 0) {
            /* We already have a codec associated with
               this input. */
            Din_Go(3152,2048);line = fp_readl(s, size, tok);
            Din_Go(3153,2048);break;
        } else {/*53*/Din_Go(3154,2048);if (tok->decoding_state > 0) {
            /* We want a 'raw' read. */
            Din_Go(3155,2048);line = Py_UniversalNewlineFgets(s, size,
                                            tok->fp, NULL);
            Din_Go(3156,2048);break;
        } else {
            /* We have not yet determined the encoding.
               If an encoding is found, use the file-pointer
               reader functions from now on. */
            Din_Go(3157,2048);if (!check_bom(fp_getc, fp_ungetc, fp_setreadl, tok))
                {/*55*/{char * ReplaceReturn695 = error_ret(tok); Din_Go(3158,2048); return ReplaceReturn695;}/*56*/}
            assert(tok->decoding_state != 0);
        ;/*54*/}}
    }
    Din_Go(3162,2048);if (line != NULL && tok->lineno < 2 && !tok->read_coding_spec) {
        Din_Go(3160,2048);if (!check_coding_spec(line, strlen(line), tok, fp_setreadl)) {
            {char * ReplaceReturn694 = error_ret(tok); Din_Go(3161,2048); return ReplaceReturn694;}
        }
    }
#ifndef PGEN
    /* The default encoding is ASCII, so make sure we don't have any
       non-ASCII bytes in it. */
    Din_Go(3168,2048);if (line && !tok->encoding) {
        Din_Go(3163,2048);unsigned char *c;
        Din_Go(3167,2048);for (c = (unsigned char *)line; *c; c++)
            {/*57*/Din_Go(3164,2048);if (*c > 127) {
                Din_Go(3165,2048);badchar = *c;
                Din_Go(3166,2048);break;
            ;/*58*/}}
    }
    Din_Go(3171,2048);if (badchar) {
        Din_Go(3169,2048);char buf[500];
        /* Need to add 1 to the line number, since this line
           has not been counted, yet.  */
        sprintf(buf,
            "Non-ASCII character '\\x%.2x' "
            "in file %.200s on line %i, "
            "but no encoding declared; "
            "see http://python.org/dev/peps/pep-0263/ for details",
            badchar, tok->filename, tok->lineno + 1);
        PyErr_SetString(PyExc_SyntaxError, buf);
        {char * ReplaceReturn693 = error_ret(tok); Din_Go(3170,2048); return ReplaceReturn693;}
    }
#endif
    {char * ReplaceReturn692 = line; Din_Go(3172,2048); return ReplaceReturn692;}
}

static int
decoding_feof(struct tok_state *tok)
{
    Din_Go(3173,2048);if (tok->decoding_state >= 0) {
        {int  ReplaceReturn691 = feof(tok->fp); Din_Go(3174,2048); return ReplaceReturn691;}
    } else {
        Din_Go(3175,2048);PyObject* buf = tok->decoding_buffer;
        Din_Go(3181,2048);if (buf == NULL) {
            Din_Go(3176,2048);buf = PyObject_CallObject(tok->decoding_readline, NULL);
            Din_Go(3180,2048);if (buf == NULL) {
                Din_Go(3177,2048);error_ret(tok);
                {int  ReplaceReturn690 = 1; Din_Go(3178,2048); return ReplaceReturn690;}
            } else {
                Din_Go(3179,2048);tok->decoding_buffer = buf;
            }
        }
        {int  ReplaceReturn689 = PyObject_Length(buf) == 0; Din_Go(3182,2048); return ReplaceReturn689;}
    }
Din_Go(3183,2048);}

/* Fetch a byte from TOK, using the string buffer. */

static int
buf_getc(struct tok_state *tok) {
    {int  ReplaceReturn688 = Py_CHARMASK(*tok->str++); Din_Go(3184,2048); return ReplaceReturn688;}
}

/* Unfetch a byte from TOK, using the string buffer. */

static void
buf_ungetc(int c, struct tok_state *tok) {
    Din_Go(3185,2048);tok->str--;
    assert(Py_CHARMASK(*tok->str) == c);        /* tok->cur may point to read-only segment */
}

/* Set the readline function for TOK to ENC. For the string-based
   tokenizer, this means to just record the encoding. */

static int
buf_setreadl(struct tok_state *tok, const char* enc) {
    Din_Go(3186,2048);tok->enc = enc;
    {int  ReplaceReturn687 = 1; Din_Go(3187,2048); return ReplaceReturn687;}
}

/* Return a UTF-8 encoding Python string object from the
   C byte string STR, which is encoded with ENC. */

#ifdef Py_USING_UNICODE
static PyObject *
translate_into_utf8(const char* str, const char* enc) {
    Din_Go(3188,2048);PyObject *utf8;
    PyObject* buf = PyUnicode_Decode(str, strlen(str), enc, NULL);
    Din_Go(3189,2048);if (buf == NULL)
        return NULL;
    Din_Go(3190,2048);utf8 = PyUnicode_AsUTF8String(buf);
    Py_DECREF(buf);
    {PyObject * ReplaceReturn686 = utf8; Din_Go(3191,2048); return ReplaceReturn686;}
}
#endif


static char *
translate_newlines(const char *s, int exec_input, struct tok_state *tok) {
    Din_Go(3192,2048);int skip_next_lf = 0, needed_length = strlen(s) + 2, final_length;
    char *buf, *current;
    char c = '\0';
    buf = PyMem_MALLOC(needed_length);
    Din_Go(3195,2048);if (buf == NULL) {
        Din_Go(3193,2048);tok->done = E_NOMEM;
        {char * ReplaceReturn685 = NULL; Din_Go(3194,2048); return ReplaceReturn685;}
    }
    Din_Go(3206,2048);for (current = buf; *s; s++, current++) {
        Din_Go(3196,2048);c = *s;
        Din_Go(3202,2048);if (skip_next_lf) {
            Din_Go(3197,2048);skip_next_lf = 0;
            Din_Go(3201,2048);if (c == '\n') {
                Din_Go(3198,2048);c = *++s;
                Din_Go(3200,2048);if (!c)
                    {/*59*/Din_Go(3199,2048);break;/*60*/}
            }
        }
        Din_Go(3204,2048);if (c == '\r') {
            Din_Go(3203,2048);skip_next_lf = 1;
            c = '\n';
        }
        Din_Go(3205,2048);*current = c;
    }
    /* If this is exec input, add a newline to the end of the string if
       there isn't one already. */
    Din_Go(3208,2048);if (exec_input && c != '\n') {
        Din_Go(3207,2048);*current = '\n';
        current++;
    }
    Din_Go(3209,2048);*current = '\0';
    final_length = current - buf + 1;
    Din_Go(3210,2048);if (final_length < needed_length && final_length)
        /* should never fail */
        buf = PyMem_REALLOC(buf, final_length);
    {char * ReplaceReturn684 = buf; Din_Go(3211,2048); return ReplaceReturn684;}
}

/* Decode a byte string STR for use as the buffer of TOK.
   Look for encoding declarations inside STR, and record them
   inside TOK.  */

static const char *
decode_str(const char *input, int single, struct tok_state *tok)
{
    Din_Go(3212,2048);PyObject* utf8 = NULL;
    const char *str;
    const char *s;
    const char *newl[2] = {NULL, NULL};
    int lineno = 0;
    tok->input = str = translate_newlines(input, single, tok);
    Din_Go(3213,2048);if (str == NULL)
        return NULL;
    Din_Go(3214,2048);tok->enc = NULL;
    tok->str = str;
    Din_Go(3216,2048);if (!check_bom(buf_getc, buf_ungetc, buf_setreadl, tok))
        {/*61*/{const char * ReplaceReturn683 = error_ret(tok); Din_Go(3215,2048); return ReplaceReturn683;}/*62*/}
    Din_Go(3217,2048);str = tok->str;             /* string after BOM if any */
    assert(str);
#ifdef Py_USING_UNICODE
    Din_Go(3222,2048);if (tok->enc != NULL) {
        Din_Go(3218,2048);utf8 = translate_into_utf8(str, tok->enc);
        Din_Go(3220,2048);if (utf8 == NULL)
            {/*63*/{const char * ReplaceReturn682 = error_ret(tok); Din_Go(3219,2048); return ReplaceReturn682;}/*64*/}
        Din_Go(3221,2048);str = PyString_AsString(utf8);
    }
#endif
    Din_Go(3229,2048);for (s = str;; s++) {
        Din_Go(3223,2048);if (*s == '\0') {/*65*/Din_Go(3224,2048);break;/*66*/}
        else {/*67*/Din_Go(3225,2048);if (*s == '\n') {
            assert(lineno < 2);
            Din_Go(3226,2048);newl[lineno] = s;
            lineno++;
            Din_Go(3228,2048);if (lineno == 2) {/*69*/Din_Go(3227,2048);break;/*70*/}
        ;/*68*/}}
    }
    Din_Go(3230,2048);tok->enc = NULL;
    /* need to check line 1 and 2 separately since check_coding_spec
       assumes a single line as input */
    Din_Go(3236,2048);if (newl[0]) {
        Din_Go(3231,2048);if (!check_coding_spec(str, newl[0] - str, tok, buf_setreadl))
            {/*71*/{const char * ReplaceReturn681 = error_ret(tok); Din_Go(3232,2048); return ReplaceReturn681;}/*72*/}
        Din_Go(3235,2048);if (tok->enc == NULL && newl[1]) {
            Din_Go(3233,2048);if (!check_coding_spec(newl[0]+1, newl[1] - newl[0],
                                   tok, buf_setreadl))
                {/*73*/{const char * ReplaceReturn680 = error_ret(tok); Din_Go(3234,2048); return ReplaceReturn680;}/*74*/}
        }
    }
#ifdef Py_USING_UNICODE
    Din_Go(3241,2048);if (tok->enc != NULL) {
        assert(utf8 == NULL);
        Din_Go(3237,2048);utf8 = translate_into_utf8(str, tok->enc);
        Din_Go(3239,2048);if (utf8 == NULL)
            {/*75*/{const char * ReplaceReturn679 = error_ret(tok); Din_Go(3238,2048); return ReplaceReturn679;}/*76*/}
        Din_Go(3240,2048);str = PyString_AsString(utf8);
    }
#endif
    assert(tok->decoding_buffer == NULL);
    Din_Go(3242,2048);tok->decoding_buffer = utf8; /* CAUTION */
    {const char * ReplaceReturn678 = str; Din_Go(3243,2048); return ReplaceReturn678;}
}

#endif /* PGEN */

/* Set up tokenizer for string */

struct tok_state *
PyTokenizer_FromString(const char *str, int exec_input)
{
    Din_Go(3244,2048);struct tok_state *tok = tok_new();
    Din_Go(3245,2048);if (tok == NULL)
        return NULL;
    Din_Go(3246,2048);str = (char *)decode_str(str, exec_input, tok);
    Din_Go(3249,2048);if (str == NULL) {
        Din_Go(3247,2048);PyTokenizer_Free(tok);
        {__IASTRUCT__ tok_state * ReplaceReturn677 = NULL; Din_Go(3248,2048); return ReplaceReturn677;}
    }

    /* XXX: constify members. */
    Din_Go(3250,2048);tok->buf = tok->cur = tok->end = tok->inp = (char*)str;
    {__IASTRUCT__ tok_state * ReplaceReturn676 = tok; Din_Go(3251,2048); return ReplaceReturn676;}
}


/* Set up tokenizer for file */

struct tok_state *
PyTokenizer_FromFile(FILE *fp, char *ps1, char *ps2)
{
    Din_Go(3252,2048);struct tok_state *tok = tok_new();
    Din_Go(3253,2048);if (tok == NULL)
        return NULL;
    Din_Go(3256,2048);if ((tok->buf = (char *)PyMem_MALLOC(BUFSIZ)) == NULL) {
        Din_Go(3254,2048);PyTokenizer_Free(tok);
        {__IASTRUCT__ tok_state * ReplaceReturn675 = NULL; Din_Go(3255,2048); return ReplaceReturn675;}
    }
    Din_Go(3257,2048);tok->cur = tok->inp = tok->buf;
    tok->end = tok->buf + BUFSIZ;
    tok->fp = fp;
    tok->prompt = ps1;
    tok->nextprompt = ps2;
    {__IASTRUCT__ tok_state * ReplaceReturn674 = tok; Din_Go(3258,2048); return ReplaceReturn674;}
}


/* Free a tok_state structure */

void
PyTokenizer_Free(struct tok_state *tok)
{
    Din_Go(3259,2048);if (tok->encoding != NULL)
        PyMem_FREE(tok->encoding);
#ifndef PGEN
    Py_XDECREF(tok->decoding_readline);
    Py_XDECREF(tok->decoding_buffer);
#endif
    Din_Go(3260,2048);if (tok->fp != NULL && tok->buf != NULL)
        PyMem_FREE(tok->buf);
    Din_Go(3261,2048);if (tok->input)
        PyMem_FREE((char *)tok->input);
    PyMem_FREE(tok);
}

#if !defined(PGEN) && defined(Py_USING_UNICODE)
static int
tok_stdin_decode(struct tok_state *tok, char **inp)
{
    Din_Go(3262,2048);PyObject *enc, *sysstdin, *decoded, *utf8;
    const char *encoding;
    char *converted;

    Din_Go(3264,2048);if (PySys_GetFile((char *)"stdin", NULL) != stdin)
        {/*77*/{int  ReplaceReturn673 = 0; Din_Go(3263,2048); return ReplaceReturn673;}/*78*/}
    Din_Go(3265,2048);sysstdin = PySys_GetObject("stdin");
    Din_Go(3267,2048);if (sysstdin == NULL || !PyFile_Check(sysstdin))
        {/*79*/{int  ReplaceReturn672 = 0; Din_Go(3266,2048); return ReplaceReturn672;}/*80*/}

    Din_Go(3268,2048);enc = ((PyFileObject *)sysstdin)->f_encoding;
    Din_Go(3270,2048);if (enc == NULL || !PyString_Check(enc))
        {/*81*/{int  ReplaceReturn671 = 0; Din_Go(3269,2048); return ReplaceReturn671;}/*82*/}
    Py_INCREF(enc);

    Din_Go(3271,2048);encoding = PyString_AsString(enc);
    decoded = PyUnicode_Decode(*inp, strlen(*inp), encoding, NULL);
    Din_Go(3273,2048);if (decoded == NULL)
        {/*83*/Din_Go(3272,2048);goto error_clear;/*84*/}

    Din_Go(3274,2048);utf8 = PyUnicode_AsEncodedString(decoded, "utf-8", NULL);
    Py_DECREF(decoded);
    Din_Go(3276,2048);if (utf8 == NULL)
        {/*85*/Din_Go(3275,2048);goto error_clear;/*86*/}

    assert(PyString_Check(utf8));
    Din_Go(3277,2048);converted = new_string(PyString_AS_STRING(utf8),
                           PyString_GET_SIZE(utf8));
    Py_DECREF(utf8);
    Din_Go(3279,2048);if (converted == NULL)
        {/*87*/Din_Go(3278,2048);goto error_nomem;/*88*/}

    PyMem_FREE(*inp);
    Din_Go(3280,2048);*inp = converted;
    Din_Go(3281,2048);if (tok->encoding != NULL)
        PyMem_FREE(tok->encoding);
    Din_Go(3282,2048);tok->encoding = new_string(encoding, strlen(encoding));
    Din_Go(3284,2048);if (tok->encoding == NULL)
        {/*89*/Din_Go(3283,2048);goto error_nomem;/*90*/}

    Py_DECREF(enc);
    {int  ReplaceReturn670 = 0; Din_Go(3285,2048); return ReplaceReturn670;}

error_nomem:
    Py_DECREF(enc);
    tok->done = E_NOMEM;
    return -1;

error_clear:
    Py_DECREF(enc);
    if (!PyErr_ExceptionMatches(PyExc_UnicodeDecodeError)) {
        tok->done = E_ERROR;
        return -1;
    }
    /* Fallback to iso-8859-1: for backward compatibility */
    PyErr_Clear();
    return 0;
}
#endif

/* Get next char, updating state; error code goes into tok->done */

static int
tok_nextc(register struct tok_state *tok)
{
    Din_Go(3286,2048);for (;;) {
        Din_Go(3287,2048);if (tok->cur != tok->inp) {
            {int  ReplaceReturn669 = Py_CHARMASK(*tok->cur++); Din_Go(3288,2048); return ReplaceReturn669;} /* Fast path */
        }
        Din_Go(3289,2048);if (tok->done != E_OK)
            return EOF;
        Din_Go(3301,2048);if (tok->fp == NULL) {
            Din_Go(3290,2048);char *end = strchr(tok->inp, '\n');
            Din_Go(3296,2048);if (end != NULL)
                {/*1*/Din_Go(3291,2048);end++;/*2*/}
            else {
                Din_Go(3292,2048);end = strchr(tok->inp, '\0');
                Din_Go(3295,2048);if (end == tok->inp) {
                    Din_Go(3293,2048);tok->done = E_EOF;
                    {int  ReplaceReturn668 = EOF; Din_Go(3294,2048); return ReplaceReturn668;}
                }
            }
            Din_Go(3298,2048);if (tok->start == NULL)
                {/*3*/Din_Go(3297,2048);tok->buf = tok->cur;/*4*/}
            Din_Go(3299,2048);tok->line_start = tok->cur;
            tok->lineno++;
            tok->inp = end;
            {int  ReplaceReturn667 = Py_CHARMASK(*tok->cur++); Din_Go(3300,2048); return ReplaceReturn667;}
        }
        Din_Go(3347,2048);if (tok->prompt != NULL) {
            Din_Go(3302,2048);char *newtok = PyOS_Readline(stdin, stdout, tok->prompt);
            Din_Go(3304,2048);if (tok->nextprompt != NULL)
                {/*5*/Din_Go(3303,2048);tok->prompt = tok->nextprompt;/*6*/}
            Din_Go(3317,2048);if (newtok == NULL)
                tok->done = E_INTR;
            else {/*7*/Din_Go(3305,2048);if (*newtok == '\0') {
                PyMem_FREE(newtok);
                Din_Go(3306,2048);tok->done = E_EOF;
            }
#if !defined(PGEN) && defined(Py_USING_UNICODE)
            else {/*9*/Din_Go(3307,2048);if (tok_stdin_decode(tok, &newtok) != 0)
                PyMem_FREE(newtok);
#endif
            else {/*11*/Din_Go(3308,2048);if (tok->start != NULL) {
                Din_Go(3309,2048);size_t start = tok->start - tok->buf;
                size_t oldlen = tok->cur - tok->buf;
                size_t newlen = oldlen + strlen(newtok);
                char *buf = tok->buf;
                buf = (char *)PyMem_REALLOC(buf, newlen+1);
                tok->lineno++;
                Din_Go(3312,2048);if (buf == NULL) {
                    PyMem_FREE(tok->buf);
                    Din_Go(3310,2048);tok->buf = NULL;
                    PyMem_FREE(newtok);
                    tok->done = E_NOMEM;
                    {int  ReplaceReturn666 = EOF; Din_Go(3311,2048); return ReplaceReturn666;}
                }
                Din_Go(3313,2048);tok->buf = buf;
                tok->cur = tok->buf + oldlen;
                tok->line_start = tok->cur;
                strcpy(tok->buf + oldlen, newtok);
                PyMem_FREE(newtok);
                tok->inp = tok->buf + newlen;
                tok->end = tok->inp + 1;
                tok->start = tok->buf + start;
            }
            else {
                Din_Go(3314,2048);tok->lineno++;
                Din_Go(3315,2048);if (tok->buf != NULL)
                    PyMem_FREE(tok->buf);
                Din_Go(3316,2048);tok->buf = newtok;
                tok->line_start = tok->buf;
                tok->cur = tok->buf;
                tok->line_start = tok->buf;
                tok->inp = strchr(tok->buf, '\0');
                tok->end = tok->inp + 1;
            ;/*12*/}/*10*/}/*8*/}}
        }
        else {
            Din_Go(3318,2048);int done = 0;
            Py_ssize_t cur = 0;
            char *pt;
            Din_Go(3331,2048);if (tok->start == NULL) {
                Din_Go(3319,2048);if (tok->buf == NULL) {
                    Din_Go(3320,2048);tok->buf = (char *)
                        PyMem_MALLOC(BUFSIZ);
                    Din_Go(3323,2048);if (tok->buf == NULL) {
                        Din_Go(3321,2048);tok->done = E_NOMEM;
                        {int  ReplaceReturn665 = EOF; Din_Go(3322,2048); return ReplaceReturn665;}
                    }
                    Din_Go(3324,2048);tok->end = tok->buf + BUFSIZ;
                }
                Din_Go(3327,2048);if (decoding_fgets(tok->buf, (int)(tok->end - tok->buf),
                          tok) == NULL) {
                    Din_Go(3325,2048);tok->done = E_EOF;
                    done = 1;
                }
                else {
                    Din_Go(3326,2048);tok->done = E_OK;
                    tok->inp = strchr(tok->buf, '\0');
                    done = tok->inp[-1] == '\n';
                }
            }
            else {
                Din_Go(3328,2048);cur = tok->cur - tok->buf;
                Din_Go(3330,2048);if (decoding_feof(tok)) {
                    Din_Go(3329,2048);tok->done = E_EOF;
                    done = 1;
                }
                else
                    tok->done = E_OK;
            }
            Din_Go(3332,2048);tok->lineno++;
            /* Read until '\n' or EOF */
            Din_Go(3342,2048);while (!done) {
                Din_Go(3333,2048);Py_ssize_t curstart = tok->start == NULL ? -1 :
                          tok->start - tok->buf;
                Py_ssize_t curvalid = tok->inp - tok->buf;
                Py_ssize_t newsize = curvalid + BUFSIZ;
                char *newbuf = tok->buf;
                newbuf = (char *)PyMem_REALLOC(newbuf,
                                               newsize);
                Din_Go(3336,2048);if (newbuf == NULL) {
                    Din_Go(3334,2048);tok->done = E_NOMEM;
                    tok->cur = tok->inp;
                    {int  ReplaceReturn664 = EOF; Din_Go(3335,2048); return ReplaceReturn664;}
                }
                Din_Go(3337,2048);tok->buf = newbuf;
                tok->inp = tok->buf + curvalid;
                tok->end = tok->buf + newsize;
                tok->start = curstart < 0 ? NULL :
                         tok->buf + curstart;
                Din_Go(3340,2048);if (decoding_fgets(tok->inp,
                               (int)(tok->end - tok->inp),
                               tok) == NULL) {
                    /* Break out early on decoding
                       errors, as tok->buf will be NULL
                     */
                    Din_Go(3338,2048);if (tok->decoding_erred)
                        return EOF;
                    /* Last line does not end in \n,
                       fake one */
                    Din_Go(3339,2048);strcpy(tok->inp, "\n");
                }
                Din_Go(3341,2048);tok->inp = strchr(tok->inp, '\0');
                done = tok->inp[-1] == '\n';
            }
            Din_Go(3346,2048);if (tok->buf != NULL) {
                Din_Go(3343,2048);tok->cur = tok->buf + cur;
                tok->line_start = tok->cur;
                /* replace "\r\n" with "\n" */
                /* For Mac leave the \r, giving a syntax error */
                pt = tok->inp - 2;
                Din_Go(3345,2048);if (pt >= tok->buf && *pt == '\r') {
                    Din_Go(3344,2048);*pt++ = '\n';
                    *pt = '\0';
                    tok->inp = pt;
                }
            }
        }
        Din_Go(3352,2048);if (tok->done != E_OK) {
            Din_Go(3348,2048);if (tok->prompt != NULL)
                {/*13*/Din_Go(3349,2048);PySys_WriteStderr("\n");/*14*/}
            Din_Go(3350,2048);tok->cur = tok->inp;
            {int  ReplaceReturn663 = EOF; Din_Go(3351,2048); return ReplaceReturn663;}
        }
    }
    /*NOTREACHED*/
Din_Go(3353,2048);}


/* Back-up one character */

static void
tok_backup(register struct tok_state *tok, register int c)
{
    Din_Go(3354,2048);if (c != EOF) {
        Din_Go(3355,2048);if (--tok->cur < tok->buf)
            {/*15*/Din_Go(3356,2048);Py_FatalError("tok_backup: beginning of buffer");/*16*/}
        Din_Go(3358,2048);if (*tok->cur != c)
            {/*17*/Din_Go(3357,2048);*tok->cur = c;/*18*/}
    }
Din_Go(3359,2048);}


/* Return the token corresponding to a single character */

int
PyToken_OneChar(int c)
{
    Din_Go(3360,2048);switch (c) {
    case '(':           {int  ReplaceReturn662 = LPAR; Din_Go(3361,2048); return ReplaceReturn662;}
    case ')':           {int  ReplaceReturn661 = RPAR; Din_Go(3362,2048); return ReplaceReturn661;}
    case '[':           {int  ReplaceReturn660 = LSQB; Din_Go(3363,2048); return ReplaceReturn660;}
    case ']':           {int  ReplaceReturn659 = RSQB; Din_Go(3364,2048); return ReplaceReturn659;}
    case ':':           {int  ReplaceReturn658 = COLON; Din_Go(3365,2048); return ReplaceReturn658;}
    case ',':           {int  ReplaceReturn657 = COMMA; Din_Go(3366,2048); return ReplaceReturn657;}
    case ';':           {int  ReplaceReturn656 = SEMI; Din_Go(3367,2048); return ReplaceReturn656;}
    case '+':           {int  ReplaceReturn655 = PLUS; Din_Go(3368,2048); return ReplaceReturn655;}
    case '-':           {int  ReplaceReturn654 = MINUS; Din_Go(3369,2048); return ReplaceReturn654;}
    case '*':           {int  ReplaceReturn653 = STAR; Din_Go(3370,2048); return ReplaceReturn653;}
    case '/':           {int  ReplaceReturn652 = SLASH; Din_Go(3371,2048); return ReplaceReturn652;}
    case '|':           {int  ReplaceReturn651 = VBAR; Din_Go(3372,2048); return ReplaceReturn651;}
    case '&':           {int  ReplaceReturn650 = AMPER; Din_Go(3373,2048); return ReplaceReturn650;}
    case '<':           {int  ReplaceReturn649 = LESS; Din_Go(3374,2048); return ReplaceReturn649;}
    case '>':           {int  ReplaceReturn648 = GREATER; Din_Go(3375,2048); return ReplaceReturn648;}
    case '=':           {int  ReplaceReturn647 = EQUAL; Din_Go(3376,2048); return ReplaceReturn647;}
    case '.':           {int  ReplaceReturn646 = DOT; Din_Go(3377,2048); return ReplaceReturn646;}
    case '%':           {int  ReplaceReturn645 = PERCENT; Din_Go(3378,2048); return ReplaceReturn645;}
    case '`':           {int  ReplaceReturn644 = BACKQUOTE; Din_Go(3379,2048); return ReplaceReturn644;}
    case '{':           {int  ReplaceReturn643 = LBRACE; Din_Go(3380,2048); return ReplaceReturn643;}
    case '}':           {int  ReplaceReturn642 = RBRACE; Din_Go(3381,2048); return ReplaceReturn642;}
    case '^':           {int  ReplaceReturn641 = CIRCUMFLEX; Din_Go(3382,2048); return ReplaceReturn641;}
    case '~':           {int  ReplaceReturn640 = TILDE; Din_Go(3383,2048); return ReplaceReturn640;}
    case '@':       {int  ReplaceReturn639 = AT; Din_Go(3384,2048); return ReplaceReturn639;}
    default:            {int  ReplaceReturn638 = OP; Din_Go(3385,2048); return ReplaceReturn638;}
    }
Din_Go(3386,2048);}


int
PyToken_TwoChars(int c1, int c2)
{
    Din_Go(3387,2048);switch (c1) {
    case '=':
        Din_Go(3388,2048);switch (c2) {
        case '=':               {int  ReplaceReturn637 = EQEQUAL; Din_Go(3389,2048); return ReplaceReturn637;}
        }
        Din_Go(3390,2048);break;
    case '!':
        Din_Go(3391,2048);switch (c2) {
        case '=':               {int  ReplaceReturn636 = NOTEQUAL; Din_Go(3392,2048); return ReplaceReturn636;}
        }
        Din_Go(3393,2048);break;
    case '<':
        Din_Go(3394,2048);switch (c2) {
        case '>':               {int  ReplaceReturn635 = NOTEQUAL; Din_Go(3395,2048); return ReplaceReturn635;}
        case '=':               {int  ReplaceReturn634 = LESSEQUAL; Din_Go(3396,2048); return ReplaceReturn634;}
        case '<':               {int  ReplaceReturn633 = LEFTSHIFT; Din_Go(3397,2048); return ReplaceReturn633;}
        }
        Din_Go(3398,2048);break;
    case '>':
        Din_Go(3399,2048);switch (c2) {
        case '=':               {int  ReplaceReturn632 = GREATEREQUAL; Din_Go(3400,2048); return ReplaceReturn632;}
        case '>':               {int  ReplaceReturn631 = RIGHTSHIFT; Din_Go(3401,2048); return ReplaceReturn631;}
        }
        Din_Go(3402,2048);break;
    case '+':
        Din_Go(3403,2048);switch (c2) {
        case '=':               {int  ReplaceReturn630 = PLUSEQUAL; Din_Go(3404,2048); return ReplaceReturn630;}
        }
        Din_Go(3405,2048);break;
    case '-':
        Din_Go(3406,2048);switch (c2) {
        case '=':               {int  ReplaceReturn629 = MINEQUAL; Din_Go(3407,2048); return ReplaceReturn629;}
        }
        Din_Go(3408,2048);break;
    case '*':
        Din_Go(3409,2048);switch (c2) {
        case '*':               {int  ReplaceReturn628 = DOUBLESTAR; Din_Go(3410,2048); return ReplaceReturn628;}
        case '=':               {int  ReplaceReturn627 = STAREQUAL; Din_Go(3411,2048); return ReplaceReturn627;}
        }
        Din_Go(3412,2048);break;
    case '/':
        Din_Go(3413,2048);switch (c2) {
        case '/':               {int  ReplaceReturn626 = DOUBLESLASH; Din_Go(3414,2048); return ReplaceReturn626;}
        case '=':               {int  ReplaceReturn625 = SLASHEQUAL; Din_Go(3415,2048); return ReplaceReturn625;}
        }
        Din_Go(3416,2048);break;
    case '|':
        Din_Go(3417,2048);switch (c2) {
        case '=':               {int  ReplaceReturn624 = VBAREQUAL; Din_Go(3418,2048); return ReplaceReturn624;}
        }
        Din_Go(3419,2048);break;
    case '%':
        Din_Go(3420,2048);switch (c2) {
        case '=':               {int  ReplaceReturn623 = PERCENTEQUAL; Din_Go(3421,2048); return ReplaceReturn623;}
        }
        Din_Go(3422,2048);break;
    case '&':
        Din_Go(3423,2048);switch (c2) {
        case '=':               {int  ReplaceReturn622 = AMPEREQUAL; Din_Go(3424,2048); return ReplaceReturn622;}
        }
        Din_Go(3425,2048);break;
    case '^':
        Din_Go(3426,2048);switch (c2) {
        case '=':               {int  ReplaceReturn621 = CIRCUMFLEXEQUAL; Din_Go(3427,2048); return ReplaceReturn621;}
        }
        Din_Go(3428,2048);break;
    }
    {int  ReplaceReturn620 = OP; Din_Go(3429,2048); return ReplaceReturn620;}
}

int
PyToken_ThreeChars(int c1, int c2, int c3)
{
    Din_Go(3430,2048);switch (c1) {
    case '<':
        Din_Go(3431,2048);switch (c2) {
        case '<':
            Din_Go(3432,2048);switch (c3) {
            case '=':
                {int  ReplaceReturn619 = LEFTSHIFTEQUAL; Din_Go(3433,2048); return ReplaceReturn619;}
            }
            Din_Go(3434,2048);break;
        }
        Din_Go(3435,2048);break;
    case '>':
        Din_Go(3436,2048);switch (c2) {
        case '>':
            Din_Go(3437,2048);switch (c3) {
            case '=':
                {int  ReplaceReturn618 = RIGHTSHIFTEQUAL; Din_Go(3438,2048); return ReplaceReturn618;}
            }
            Din_Go(3439,2048);break;
        }
        Din_Go(3440,2048);break;
    case '*':
        Din_Go(3441,2048);switch (c2) {
        case '*':
            Din_Go(3442,2048);switch (c3) {
            case '=':
                {int  ReplaceReturn617 = DOUBLESTAREQUAL; Din_Go(3443,2048); return ReplaceReturn617;}
            }
            Din_Go(3444,2048);break;
        }
        Din_Go(3445,2048);break;
    case '/':
        Din_Go(3446,2048);switch (c2) {
        case '/':
            Din_Go(3447,2048);switch (c3) {
            case '=':
                {int  ReplaceReturn616 = DOUBLESLASHEQUAL; Din_Go(3448,2048); return ReplaceReturn616;}
            }
            Din_Go(3449,2048);break;
        }
        Din_Go(3450,2048);break;
    }
    {int  ReplaceReturn615 = OP; Din_Go(3451,2048); return ReplaceReturn615;}
}

static int
indenterror(struct tok_state *tok)
{
    Din_Go(3452,2048);if (tok->alterror) {
        Din_Go(3453,2048);tok->done = E_TABSPACE;
        tok->cur = tok->inp;
        {int  ReplaceReturn614 = 1; Din_Go(3454,2048); return ReplaceReturn614;}
    }
    Din_Go(3456,2048);if (tok->altwarning) {
        Din_Go(3455,2048);PySys_WriteStderr("%s: inconsistent use of tabs and spaces "
                          "in indentation\n", tok->filename);
        tok->altwarning = 0;
    }
    {int  ReplaceReturn613 = 0; Din_Go(3457,2048); return ReplaceReturn613;}
}

/* Get next token, after space stripping etc. */

static int
tok_get(register struct tok_state *tok, char **p_start, char **p_end)
{
    Din_Go(3458,2048);register int c;
    int blankline;

    *p_start = *p_end = NULL;
  nextline:
    tok->start = NULL;
    blankline = 0;

    /* Get indentation level */
    Din_Go(3492,2048);if (tok->atbol) {
        Din_Go(3459,2048);register int col = 0;
        register int altcol = 0;
        tok->atbol = 0;
        Din_Go(3468,2048);for (;;) {
            Din_Go(3460,2048);c = tok_nextc(tok);
            Din_Go(3467,2048);if (c == ' ')
                {/*91*/Din_Go(3461,2048);col++, altcol++;/*92*/}
            else {/*93*/Din_Go(3462,2048);if (c == '\t') {
                Din_Go(3463,2048);col = (col/tok->tabsize + 1) * tok->tabsize;
                altcol = (altcol/tok->alttabsize + 1)
                    * tok->alttabsize;
            }
            else {/*95*/Din_Go(3464,2048);if (c == '\014') /* Control-L (formfeed) */
                {/*97*/Din_Go(3465,2048);col = altcol = 0;/*98*/} /* For Emacs users */
            else
                {/*99*/Din_Go(3466,2048);break;/*100*/}/*94*/;/*96*/}}
        }
        Din_Go(3469,2048);tok_backup(tok, c);
        Din_Go(3473,2048);if (c == '#' || c == '\n') {
            /* Lines with only whitespace and/or comments
               shouldn't affect the indentation and are
               not passed to the parser as NEWLINE tokens,
               except *totally* empty lines in interactive
               mode, which signal the end of a command group. */
            Din_Go(3470,2048);if (col == 0 && c == '\n' && tok->prompt != NULL)
                {/*101*/Din_Go(3471,2048);blankline = 0;/*102*/} /* Let it through */
            else
                {/*103*/Din_Go(3472,2048);blankline = 1;/*104*/} /* Ignore completely */
            /* We can't jump back right here since we still
               may need to skip to the end of a comment */
        }
        Din_Go(3491,2048);if (!blankline && tok->level == 0) {
            Din_Go(3474,2048);if (col == tok->indstack[tok->indent]) {
                /* No change */
                Din_Go(3475,2048);if (altcol != tok->altindstack[tok->indent]) {
                    Din_Go(3476,2048);if (indenterror(tok))
                        return ERRORTOKEN;
                }
            }
            else {/*105*/Din_Go(3477,2048);if (col > tok->indstack[tok->indent]) {
                /* Indent -- always one */
                Din_Go(3478,2048);if (tok->indent+1 >= MAXINDENT) {
                    Din_Go(3479,2048);tok->done = E_TOODEEP;
                    tok->cur = tok->inp;
                    {int  ReplaceReturn612 = ERRORTOKEN; Din_Go(3480,2048); return ReplaceReturn612;}
                }
                Din_Go(3482,2048);if (altcol <= tok->altindstack[tok->indent]) {
                    Din_Go(3481,2048);if (indenterror(tok))
                        return ERRORTOKEN;
                }
                Din_Go(3483,2048);tok->pendin++;
                tok->indstack[++tok->indent] = col;
                tok->altindstack[tok->indent] = altcol;
            }
            else /* col < tok->indstack[tok->indent] */ {
                /* Dedent -- any number, must be consistent */
                Din_Go(3484,2048);while (tok->indent > 0 &&
                    col < tok->indstack[tok->indent]) {
                    Din_Go(3485,2048);tok->pendin--;
                    tok->indent--;
                }
                Din_Go(3488,2048);if (col != tok->indstack[tok->indent]) {
                    Din_Go(3486,2048);tok->done = E_DEDENT;
                    tok->cur = tok->inp;
                    {int  ReplaceReturn611 = ERRORTOKEN; Din_Go(3487,2048); return ReplaceReturn611;}
                }
                Din_Go(3490,2048);if (altcol != tok->altindstack[tok->indent]) {
                    Din_Go(3489,2048);if (indenterror(tok))
                        return ERRORTOKEN;
                }
            ;/*106*/}}
        }
    }

    Din_Go(3493,2048);tok->start = tok->cur;

    /* Return pending indents/dedents */
    Din_Go(3499,2048);if (tok->pendin != 0) {
        Din_Go(3494,2048);if (tok->pendin < 0) {
            Din_Go(3495,2048);tok->pendin++;
            {int  ReplaceReturn610 = DEDENT; Din_Go(3496,2048); return ReplaceReturn610;}
        }
        else {
            Din_Go(3497,2048);tok->pendin--;
            {int  ReplaceReturn609 = INDENT; Din_Go(3498,2048); return ReplaceReturn609;}
        }
    }

 again:
    Din_Go(3500,2048);tok->start = NULL;
    /* Skip spaces */
    Din_Go(3502,2048);do {
        Din_Go(3501,2048);c = tok_nextc(tok);
    } while (c == ' ' || c == '\t' || c == '\014');

    /* Set start of current token */
    Din_Go(3503,2048);tok->start = tok->cur - 1;

    /* Skip comment, while looking for tab-setting magic */
    Din_Go(3517,2048);if (c == '#') {
        Din_Go(3504,2048);static char *tabforms[] = {
            "tab-width:",                       /* Emacs */
            ":tabstop=",                        /* vim, full form */
            ":ts=",                             /* vim, abbreviated form */
            "set tabsize=",                     /* will vi never die? */
        /* more templates can be added here to support other editors */
        };
        char cbuf[80];
        char *tp, **cp;
        tp = cbuf;
        Din_Go(3506,2048);do {
            Din_Go(3505,2048);*tp++ = c = tok_nextc(tok);
        } while (c != EOF && c != '\n' &&
                 (size_t)(tp - cbuf + 1) < sizeof(cbuf));
        Din_Go(3507,2048);*tp = '\0';
        Din_Go(3514,2048);for (cp = tabforms;
             cp < tabforms + sizeof(tabforms)/sizeof(tabforms[0]);
             cp++) {
            Din_Go(3508,2048);if ((tp = strstr(cbuf, *cp))) {
                Din_Go(3509,2048);int newsize = atoi(tp + strlen(*cp));

                Din_Go(3513,2048);if (newsize >= 1 && newsize <= 40) {
                    Din_Go(3510,2048);tok->tabsize = newsize;
                    Din_Go(3512,2048);if (Py_VerboseFlag)
                        {/*107*/Din_Go(3511,2048);PySys_WriteStderr(
                        "Tab size set to %d\n",
                        newsize);/*108*/}
                }
            }
        }
        Din_Go(3516,2048);while (c != EOF && c != '\n')
            {/*109*/Din_Go(3515,2048);c = tok_nextc(tok);/*110*/}
    }

    /* Check for EOF and errors now */
    Din_Go(3519,2048);if (c == EOF) {
        {int  ReplaceReturn608 = tok->done == E_EOF ? ENDMARKER : ERRORTOKEN; Din_Go(3518,2048); return ReplaceReturn608;}
    }

    /* Identifier (most frequent token!) */
    Din_Go(3541,2048);if (Py_ISALPHA(c) || c == '_') {
        /* Process r"", u"" and ur"" */
        Din_Go(3520,2048);switch (c) {
        case 'b':
        case 'B':
            Din_Go(3521,2048);c = tok_nextc(tok);
            Din_Go(3523,2048);if (c == 'r' || c == 'R')
                {/*111*/Din_Go(3522,2048);c = tok_nextc(tok);/*112*/}
            Din_Go(3525,2048);if (c == '"' || c == '\'')
                {/*113*/Din_Go(3524,2048);goto letter_quote;/*114*/}
            Din_Go(3526,2048);break;
        case 'r':
        case 'R':
            Din_Go(3527,2048);c = tok_nextc(tok);
            Din_Go(3529,2048);if (c == '"' || c == '\'')
                {/*115*/Din_Go(3528,2048);goto letter_quote;/*116*/}
            Din_Go(3530,2048);break;
        case 'u':
        case 'U':
            Din_Go(3531,2048);c = tok_nextc(tok);
            Din_Go(3533,2048);if (c == 'r' || c == 'R')
                {/*117*/Din_Go(3532,2048);c = tok_nextc(tok);/*118*/}
            Din_Go(3535,2048);if (c == '"' || c == '\'')
                {/*119*/Din_Go(3534,2048);goto letter_quote;/*120*/}
            Din_Go(3536,2048);break;
        }
        Din_Go(3538,2048);while (c != EOF && (Py_ISALNUM(c) || c == '_')) {
            Din_Go(3537,2048);c = tok_nextc(tok);
        }
        Din_Go(3539,2048);tok_backup(tok, c);
        *p_start = tok->start;
        *p_end = tok->cur;
        {int  ReplaceReturn607 = NAME; Din_Go(3540,2048); return ReplaceReturn607;}
    }

    /* Newline */
    Din_Go(3547,2048);if (c == '\n') {
        Din_Go(3542,2048);tok->atbol = 1;
        Din_Go(3544,2048);if (blankline || tok->level > 0)
            {/*121*/Din_Go(3543,2048);goto nextline;/*122*/}
        Din_Go(3545,2048);*p_start = tok->start;
        *p_end = tok->cur - 1; /* Leave '\n' out of the string */
        tok->cont_line = 0;
        {int  ReplaceReturn606 = NEWLINE; Din_Go(3546,2048); return ReplaceReturn606;}
    }

    /* Period or number starting with period? */
    Din_Go(3553,2048);if (c == '.') {
        Din_Go(3548,2048);c = tok_nextc(tok);
        Din_Go(3552,2048);if (isdigit(c)) {
            Din_Go(3549,2048);goto fraction;
        }
        else {
            Din_Go(3550,2048);tok_backup(tok, c);
            *p_start = tok->start;
            *p_end = tok->cur;
            {int  ReplaceReturn605 = DOT; Din_Go(3551,2048); return ReplaceReturn605;}
        }
    }

    /* Number */
    Din_Go(3622,2048);if (isdigit(c)) {
        Din_Go(3554,2048);if (c == '0') {
            /* Hex, octal or binary -- maybe. */
            Din_Go(3555,2048);c = tok_nextc(tok);
            Din_Go(3557,2048);if (c == '.')
                {/*123*/Din_Go(3556,2048);goto fraction;/*124*/}
#ifndef WITHOUT_COMPLEX
            Din_Go(3559,2048);if (c == 'j' || c == 'J')
                {/*125*/Din_Go(3558,2048);goto imaginary;/*126*/}
#endif
            Din_Go(3596,2048);if (c == 'x' || c == 'X') {

                /* Hex */
                Din_Go(3560,2048);c = tok_nextc(tok);
                Din_Go(3563,2048);if (!isxdigit(c)) {
                    Din_Go(3561,2048);tok->done = E_TOKEN;
                    tok_backup(tok, c);
                    {int  ReplaceReturn604 = ERRORTOKEN; Din_Go(3562,2048); return ReplaceReturn604;}
                }
                Din_Go(3565,2048);do {
                    Din_Go(3564,2048);c = tok_nextc(tok);
                } while (isxdigit(c));
            }
            else {/*127*/Din_Go(3566,2048);if (c == 'o' || c == 'O') {
                /* Octal */
                Din_Go(3567,2048);c = tok_nextc(tok);
                Din_Go(3570,2048);if (c < '0' || c >= '8') {
                    Din_Go(3568,2048);tok->done = E_TOKEN;
                    tok_backup(tok, c);
                    {int  ReplaceReturn603 = ERRORTOKEN; Din_Go(3569,2048); return ReplaceReturn603;}
                }
                Din_Go(3572,2048);do {
                    Din_Go(3571,2048);c = tok_nextc(tok);
                } while ('0' <= c && c < '8');
            }
            else {/*129*/Din_Go(3573,2048);if (c == 'b' || c == 'B') {
                /* Binary */
                Din_Go(3574,2048);c = tok_nextc(tok);
                Din_Go(3577,2048);if (c != '0' && c != '1') {
                    Din_Go(3575,2048);tok->done = E_TOKEN;
                    tok_backup(tok, c);
                    {int  ReplaceReturn602 = ERRORTOKEN; Din_Go(3576,2048); return ReplaceReturn602;}
                }
                Din_Go(3579,2048);do {
                    Din_Go(3578,2048);c = tok_nextc(tok);
                } while (c == '0' || c == '1');
            }
            else {
                Din_Go(3580,2048);int found_decimal = 0;
                /* Octal; c is first char of it */
                /* There's no 'isoctdigit' macro, sigh */
                Din_Go(3582,2048);while ('0' <= c && c < '8') {
                    Din_Go(3581,2048);c = tok_nextc(tok);
                }
                Din_Go(3586,2048);if (isdigit(c)) {
                    Din_Go(3583,2048);found_decimal = 1;
                    Din_Go(3585,2048);do {
                        Din_Go(3584,2048);c = tok_nextc(tok);
                    } while (isdigit(c));
                }
                Din_Go(3595,2048);if (c == '.')
                    {/*131*/Din_Go(3587,2048);goto fraction;/*132*/}
                else {/*133*/Din_Go(3588,2048);if (c == 'e' || c == 'E')
                    {/*135*/Din_Go(3589,2048);goto exponent;/*136*/}
#ifndef WITHOUT_COMPLEX
                else {/*137*/Din_Go(3590,2048);if (c == 'j' || c == 'J')
                    {/*139*/Din_Go(3591,2048);goto imaginary;/*140*/}
#endif
                else {/*141*/Din_Go(3592,2048);if (found_decimal) {
                    Din_Go(3593,2048);tok->done = E_TOKEN;
                    tok_backup(tok, c);
                    {int  ReplaceReturn601 = ERRORTOKEN; Din_Go(3594,2048); return ReplaceReturn601;}
                ;/*142*/}/*138*/}/*134*/}}
            ;/*130*/}/*128*/}}
            Din_Go(3598,2048);if (c == 'l' || c == 'L')
                {/*143*/Din_Go(3597,2048);c = tok_nextc(tok);/*144*/}
        }
        else {
            /* Decimal */
            Din_Go(3599,2048);do {
                Din_Go(3600,2048);c = tok_nextc(tok);
            } while (isdigit(c));
            Din_Go(3619,2048);if (c == 'l' || c == 'L')
                {/*145*/Din_Go(3601,2048);c = tok_nextc(tok);/*146*/}
            else {
                /* Accept floating point numbers. */
                Din_Go(3602,2048);if (c == '.') {
        fraction:
                    /* Fraction */
                    Din_Go(3603,2048);do {
                        Din_Go(3604,2048);c = tok_nextc(tok);
                    } while (isdigit(c));
                }
                Din_Go(3616,2048);if (c == 'e' || c == 'E') {
                    Din_Go(3605,2048);int e;
                  exponent:
                    e = c;
                    /* Exponent part */
                    c = tok_nextc(tok);
                    Din_Go(3613,2048);if (c == '+' || c == '-') {
                        Din_Go(3606,2048);c = tok_nextc(tok);
                        Din_Go(3609,2048);if (!isdigit(c)) {
                            Din_Go(3607,2048);tok->done = E_TOKEN;
                            tok_backup(tok, c);
                            {int  ReplaceReturn600 = ERRORTOKEN; Din_Go(3608,2048); return ReplaceReturn600;}
                        }
                    } else {/*147*/Din_Go(3610,2048);if (!isdigit(c)) {
                        Din_Go(3611,2048);tok_backup(tok, c);
                        tok_backup(tok, e);
                        *p_start = tok->start;
                        *p_end = tok->cur;
                        {int  ReplaceReturn599 = NUMBER; Din_Go(3612,2048); return ReplaceReturn599;}
                    ;/*148*/}}
                    Din_Go(3615,2048);do {
                        Din_Go(3614,2048);c = tok_nextc(tok);
                    } while (isdigit(c));
                }
#ifndef WITHOUT_COMPLEX
                Din_Go(3618,2048);if (c == 'j' || c == 'J')
                    /* Imaginary part */
        imaginary:
                    {/*149*/Din_Go(3617,2048);c = tok_nextc(tok);/*150*/}
#endif
            }
        }
        Din_Go(3620,2048);tok_backup(tok, c);
        *p_start = tok->start;
        *p_end = tok->cur;
        {int  ReplaceReturn598 = NUMBER; Din_Go(3621,2048); return ReplaceReturn598;}
    }

  letter_quote:
    /* String */
    Din_Go(3653,2048);if (c == '\'' || c == '"') {
        Din_Go(3623,2048);Py_ssize_t quote2 = tok->cur - tok->start + 1;
        int quote = c;
        int triple = 0;
        int tripcount = 0;
        Din_Go(3650,2048);for (;;) {
            Din_Go(3624,2048);c = tok_nextc(tok);
            Din_Go(3649,2048);if (c == '\n') {
                Din_Go(3625,2048);if (!triple) {
                    Din_Go(3626,2048);tok->done = E_EOLS;
                    tok_backup(tok, c);
                    {int  ReplaceReturn597 = ERRORTOKEN; Din_Go(3627,2048); return ReplaceReturn597;}
                }
                Din_Go(3628,2048);tripcount = 0;
                tok->cont_line = 1; /* multiline string. */
            }
            else {/*151*/Din_Go(3629,2048);if (c == EOF) {
                Din_Go(3630,2048);if (triple)
                    tok->done = E_EOFS;
                else
                    tok->done = E_EOLS;
                Din_Go(3631,2048);tok->cur = tok->inp;
                {int  ReplaceReturn596 = ERRORTOKEN; Din_Go(3632,2048); return ReplaceReturn596;}
            }
            else {/*153*/Din_Go(3633,2048);if (c == quote) {
                Din_Go(3634,2048);tripcount++;
                Din_Go(3640,2048);if (tok->cur - tok->start == quote2) {
                    Din_Go(3635,2048);c = tok_nextc(tok);
                    Din_Go(3638,2048);if (c == quote) {
                        Din_Go(3636,2048);triple = 1;
                        tripcount = 0;
                        Din_Go(3637,2048);continue;
                    }
                    Din_Go(3639,2048);tok_backup(tok, c);
                }
                Din_Go(3642,2048);if (!triple || tripcount == 3)
                    {/*155*/Din_Go(3641,2048);break;/*156*/}
            }
            else {/*157*/Din_Go(3643,2048);if (c == '\\') {
                Din_Go(3644,2048);tripcount = 0;
                c = tok_nextc(tok);
                Din_Go(3647,2048);if (c == EOF) {
                    Din_Go(3645,2048);tok->done = E_EOLS;
                    tok->cur = tok->inp;
                    {int  ReplaceReturn595 = ERRORTOKEN; Din_Go(3646,2048); return ReplaceReturn595;}
                }
            }
            else
                {/*159*/Din_Go(3648,2048);tripcount = 0;/*160*/}/*158*/}/*154*/}/*152*/}
        }
        Din_Go(3651,2048);*p_start = tok->start;
        *p_end = tok->cur;
        {int  ReplaceReturn594 = STRING; Din_Go(3652,2048); return ReplaceReturn594;}
    }

    /* Line continuation */
    Din_Go(3660,2048);if (c == '\\') {
        Din_Go(3654,2048);c = tok_nextc(tok);
        Din_Go(3657,2048);if (c != '\n') {
            Din_Go(3655,2048);tok->done = E_LINECONT;
            tok->cur = tok->inp;
            {int  ReplaceReturn593 = ERRORTOKEN; Din_Go(3656,2048); return ReplaceReturn593;}
        }
        Din_Go(3658,2048);tok->cont_line = 1;
        Din_Go(3659,2048);goto again; /* Read next line */
    }

    /* Check for two-character token */
    {
        Din_Go(3661,2048);int c2 = tok_nextc(tok);
        int token = PyToken_TwoChars(c, c2);
#ifndef PGEN
        Din_Go(3664,2048);if (Py_Py3kWarningFlag && token == NOTEQUAL && c == '<') {
            Din_Go(3662,2048);if (PyErr_WarnExplicit(PyExc_DeprecationWarning,
                                   "<> not supported in 3.x; use !=",
                                   tok->filename, tok->lineno,
                                   NULL, NULL)) {
                {int  ReplaceReturn592 = ERRORTOKEN; Din_Go(3663,2048); return ReplaceReturn592;}
            }
        }
#endif
        Din_Go(3671,2048);if (token != OP) {
            Din_Go(3665,2048);int c3 = tok_nextc(tok);
            int token3 = PyToken_ThreeChars(c, c2, c3);
            Din_Go(3668,2048);if (token3 != OP) {
                Din_Go(3666,2048);token = token3;
            } else {
                Din_Go(3667,2048);tok_backup(tok, c3);
            }
            Din_Go(3669,2048);*p_start = tok->start;
            *p_end = tok->cur;
            {int  ReplaceReturn591 = token; Din_Go(3670,2048); return ReplaceReturn591;}
        }
        Din_Go(3672,2048);tok_backup(tok, c2);
    }

    /* Keep track of parentheses nesting level */
    Din_Go(3677,2048);switch (c) {
    case '(':
    case '[':
    case '{':
        Din_Go(3673,2048);tok->level++;
        Din_Go(3674,2048);break;
    case ')':
    case ']':
    case '}':
        Din_Go(3675,2048);tok->level--;
        Din_Go(3676,2048);break;
    }

    /* Punctuation character */
    Din_Go(3678,2048);*p_start = tok->start;
    *p_end = tok->cur;
    {int  ReplaceReturn590 = PyToken_OneChar(c); Din_Go(3679,2048); return ReplaceReturn590;}
}

int
PyTokenizer_Get(struct tok_state *tok, char **p_start, char **p_end)
{
    Din_Go(3680,2048);int result = tok_get(tok, p_start, p_end);
    Din_Go(3682,2048);if (tok->decoding_erred) {
        Din_Go(3681,2048);result = ERRORTOKEN;
        tok->done = E_DECODE;
    }
    {int  ReplaceReturn589 = result; Din_Go(3683,2048); return ReplaceReturn589;}
}

/* This function is only called from parsetok. However, it cannot live
   there, as it must be empty for PGEN, and we can check for PGEN only
   in this file. */

#if defined(PGEN) || !defined(Py_USING_UNICODE)
char*
PyTokenizer_RestoreEncoding(struct tok_state* tok, int len, int* offset)
{
    return NULL;
}
#else
#ifdef Py_USING_UNICODE
static PyObject *
dec_utf8(const char *enc, const char *text, size_t len) {
    Din_Go(3684,2048);PyObject *ret = NULL;
    PyObject *unicode_text = PyUnicode_DecodeUTF8(text, len, "replace");
    Din_Go(3686,2048);if (unicode_text) {
        Din_Go(3685,2048);ret = PyUnicode_AsEncodedString(unicode_text, enc, "replace");
        Py_DECREF(unicode_text);
    }
    Din_Go(3688,2048);if (!ret) {
        Din_Go(3687,2048);PyErr_Clear();
    }
    {PyObject * ReplaceReturn588 = ret; Din_Go(3689,2048); return ReplaceReturn588;}
}
char *
PyTokenizer_RestoreEncoding(struct tok_state* tok, int len, int *offset)
{
    Din_Go(3690,2048);char *text = NULL;
    Din_Go(3701,2048);if (tok->encoding) {
        /* convert source to original encondig */
        Din_Go(3691,2048);PyObject *lineobj = dec_utf8(tok->encoding, tok->buf, len);
        Din_Go(3700,2048);if (lineobj != NULL) {
            Din_Go(3692,2048);int linelen = PyString_Size(lineobj);
            const char *line = PyString_AsString(lineobj);
            text = PyObject_MALLOC(linelen + 1);
            Din_Go(3695,2048);if (text != NULL && line != NULL) {
                Din_Go(3693,2048);if (linelen)
                    strncpy(text, line, linelen);
                Din_Go(3694,2048);text[linelen] = '\0';
            }
            Py_DECREF(lineobj);

            /* adjust error offset */
            Din_Go(3699,2048);if (*offset > 1) {
                Din_Go(3696,2048);PyObject *offsetobj = dec_utf8(tok->encoding,
                                               tok->buf, *offset-1);
                Din_Go(3698,2048);if (offsetobj) {
                    Din_Go(3697,2048);*offset = PyString_Size(offsetobj) + 1;
                    Py_DECREF(offsetobj);
                }
            }

        }
    }
    {char * ReplaceReturn587 = text; Din_Go(3702,2048); return ReplaceReturn587;}

}
#endif /* defined(Py_USING_UNICODE) */
#endif


#ifdef Py_DEBUG

void
tok_dump(int type, char *start, char *end)
{
    printf("%s", _PyParser_TokenNames[type]);
    if (type == NAME || type == NUMBER || type == STRING || type == OP)
        printf("(%.*s)", (int)(end - start), start);
}

#endif
