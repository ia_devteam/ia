/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim: set ts=8 sts=2 et sw=2 tw=80: */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "nscore.h"
#include "var/tmp/sensor.h"
#include "nsCRTGlue.h"
#include "prprf.h"
#include "nsStringAPI.h"
#include "nsXPCOMStrings.h"
#include "nsDebug.h"

#include <stdio.h>

#if defined(_MSC_VER) && _MSC_VER < 1900
#define snprintf _snprintf
#endif

// nsAString

uint32_t
nsAString::BeginReading(const char_type** aBegin, const char_type** aEnd) const
{
  Din_Go(1,2048);uint32_t len = NS_StringGetData(*this, aBegin);
  Din_Go(3,2048);if (aEnd) {
    Din_Go(2,2048);*aEnd = *aBegin + len;
  }

  {uint32_t  ReplaceReturn96 = len; Din_Go(4,2048); return ReplaceReturn96;}
}

const nsAString::char_type*
nsAString::BeginReading() const
{
  Din_Go(5,2048);const char_type* data;
  NS_StringGetData(*this, &data);
  {const nsAString::char_type * ReplaceReturn95 = data; Din_Go(6,2048); return ReplaceReturn95;}
}

const nsAString::char_type*
nsAString::EndReading() const
{
  Din_Go(7,2048);const char_type* data;
  uint32_t len = NS_StringGetData(*this, &data);
  {const nsAString::char_type * ReplaceReturn94 = data + len; Din_Go(8,2048); return ReplaceReturn94;}
}

uint32_t
nsAString::BeginWriting(char_type** aBegin, char_type** aEnd, uint32_t aNewSize)
{
  Din_Go(9,2048);uint32_t len = NS_StringGetMutableData(*this, aNewSize, aBegin);
  Din_Go(11,2048);if (aEnd) {
    Din_Go(10,2048);*aEnd = *aBegin + len;
  }

  {uint32_t  ReplaceReturn93 = len; Din_Go(12,2048); return ReplaceReturn93;}
}

nsAString::char_type*
nsAString::BeginWriting(uint32_t aLen)
{
  Din_Go(13,2048);char_type* data;
  NS_StringGetMutableData(*this, aLen, &data);
  {nsAString::char_type * ReplaceReturn92 = data; Din_Go(14,2048); return ReplaceReturn92;}
}

nsAString::char_type*
nsAString::EndWriting()
{
  Din_Go(15,2048);char_type* data;
  uint32_t len = NS_StringGetMutableData(*this, UINT32_MAX, &data);
  {nsAString::char_type * ReplaceReturn91 = data + len; Din_Go(16,2048); return ReplaceReturn91;}
}

bool
nsAString::SetLength(uint32_t aLen)
{
  Din_Go(17,2048);char_type* data;
  NS_StringGetMutableData(*this, aLen, &data);
  {_Bool  ReplaceReturn90 = data != nullptr; Din_Go(18,2048); return ReplaceReturn90;}
}

void
nsAString::AssignLiteral(const char* aStr)
{
  Din_Go(19,2048);uint32_t len = strlen(aStr);
  char16_t* buf = BeginWriting(len);
  Din_Go(21,2048);if (!buf) {
    Din_Go(20,2048);return;
  }

  Din_Go(23,2048);for (; *aStr; ++aStr, ++buf) {
    Din_Go(22,2048);*buf = *aStr;
  }
Din_Go(24,2048);}

void
nsAString::AppendLiteral(const char* aASCIIStr)
{
  Din_Go(25,2048);uint32_t appendLen = strlen(aASCIIStr);

  uint32_t thisLen = Length();
  char16_t* begin;
  char16_t* end;
  BeginWriting(&begin, &end, appendLen + thisLen);
  Din_Go(27,2048);if (!begin) {
    Din_Go(26,2048);return;
  }

  Din_Go(29,2048);for (begin += thisLen; begin < end; ++begin, ++aASCIIStr) {
    Din_Go(28,2048);*begin = *aASCIIStr;
  }
Din_Go(30,2048);}

void
nsAString::StripChars(const char* aSet)
{
Din_Go(31,2048);  nsString copy(*this);

  const char_type* source;
  const char_type* sourceEnd;
  copy.BeginReading(&source, &sourceEnd);

  char_type* dest;
  BeginWriting(&dest);
  Din_Go(33,2048);if (!dest) {
    Din_Go(32,2048);return;
  }

  Din_Go(34,2048);char_type* curDest = dest;

  Din_Go(41,2048);for (; source < sourceEnd; ++source) {
    Din_Go(35,2048);const char* test;
    Din_Go(38,2048);for (test = aSet; *test; ++test) {
      Din_Go(36,2048);if (*source == char_type(*test)) {
        Din_Go(37,2048);break;
      }
    }

    Din_Go(40,2048);if (!*test) {
      // not stripped, copy this char
      Din_Go(39,2048);*curDest = *source;
      ++curDest;
    }
  }

  Din_Go(42,2048);SetLength(curDest - dest);
Din_Go(43,2048);}

void
nsAString::Trim(const char* aSet, bool aLeading, bool aTrailing)
{
Din_Go(44,2048);  NS_ASSERTION(aLeading || aTrailing, "Ineffective Trim");

  Din_Go(45,2048);const char16_t* start;
  const char16_t* end;
  uint32_t cutLen;

  Din_Go(56,2048);if (aLeading) {
    Din_Go(46,2048);BeginReading(&start, &end);
    Din_Go(53,2048);for (cutLen = 0; start < end; ++start, ++cutLen) {
      Din_Go(47,2048);const char* test;
      Din_Go(50,2048);for (test = aSet; *test; ++test) {
        Din_Go(48,2048);if (*test == *start) {
          Din_Go(49,2048);break;
        }
      }
      Din_Go(52,2048);if (!*test) {
        Din_Go(51,2048);break;
      }
    }
    Din_Go(55,2048);if (cutLen) {
      Din_Go(54,2048);NS_StringCutData(*this, 0, cutLen);
    }
  }
  Din_Go(67,2048);if (aTrailing) {
    Din_Go(57,2048);uint32_t len = BeginReading(&start, &end);
    --end;
    Din_Go(64,2048);for (cutLen = 0; end >= start; --end, ++cutLen) {
      Din_Go(58,2048);const char* test;
      Din_Go(61,2048);for (test = aSet; *test; ++test) {
        Din_Go(59,2048);if (*test == *end) {
          Din_Go(60,2048);break;
        }
      }
      Din_Go(63,2048);if (!*test) {
        Din_Go(62,2048);break;
      }
    }
    Din_Go(66,2048);if (cutLen) {
      Din_Go(65,2048);NS_StringCutData(*this, len - cutLen, cutLen);
    }
  }
Din_Go(68,2048);}

int32_t
nsAString::DefaultComparator(const char_type* aStrA, const char_type* aStrB,
                             uint32_t aLen)
{
  Din_Go(69,2048);for (const char_type* end = aStrA + aLen; aStrA < end; ++aStrA, ++aStrB) {
    Din_Go(70,2048);if (*aStrA == *aStrB) {
      Din_Go(71,2048);continue;
    }

    {int32_t  ReplaceReturn89 = *aStrA < *aStrB ? -1 : 1; Din_Go(72,2048); return ReplaceReturn89;}
  }

  {int32_t  ReplaceReturn88 = 0; Din_Go(73,2048); return ReplaceReturn88;}
}

int32_t
nsAString::Compare(const char_type* aOther, ComparatorFunc aComparator) const
{
  Din_Go(74,2048);const char_type* cself;
  uint32_t selflen = NS_StringGetData(*this, &cself);
  uint32_t otherlen = NS_strlen(aOther);
  uint32_t comparelen = selflen <= otherlen ? selflen : otherlen;

  int32_t result = aComparator(cself, aOther, comparelen);
  Din_Go(79,2048);if (result == 0) {
    Din_Go(75,2048);if (selflen < otherlen) {
      {int32_t  ReplaceReturn87 = -1; Din_Go(76,2048); return ReplaceReturn87;}
    } else {/*1*/Din_Go(77,2048);if (selflen > otherlen) {
      {int32_t  ReplaceReturn86 = 1; Din_Go(78,2048); return ReplaceReturn86;}
    ;/*2*/}}
  }
  {int32_t  ReplaceReturn85 = result; Din_Go(80,2048); return ReplaceReturn85;}
}

int32_t
nsAString::Compare(const self_type& aOther, ComparatorFunc aComparator) const
{
  Din_Go(81,2048);const char_type* cself;
  const char_type* cother;
  uint32_t selflen = NS_StringGetData(*this, &cself);
  uint32_t otherlen = NS_StringGetData(aOther, &cother);
  uint32_t comparelen = selflen <= otherlen ? selflen : otherlen;

  int32_t result = aComparator(cself, cother, comparelen);
  Din_Go(86,2048);if (result == 0) {
    Din_Go(82,2048);if (selflen < otherlen) {
      {int32_t  ReplaceReturn84 = -1; Din_Go(83,2048); return ReplaceReturn84;}
    } else {/*3*/Din_Go(84,2048);if (selflen > otherlen) {
      {int32_t  ReplaceReturn83 = 1; Din_Go(85,2048); return ReplaceReturn83;}
    ;/*4*/}}
  }
  {int32_t  ReplaceReturn82 = result; Din_Go(87,2048); return ReplaceReturn82;}
}

bool
nsAString::Equals(const char_type* aOther, ComparatorFunc aComparator) const
{
  Din_Go(88,2048);const char_type* cself;
  uint32_t selflen = NS_StringGetData(*this, &cself);
  uint32_t otherlen = NS_strlen(aOther);

  Din_Go(90,2048);if (selflen != otherlen) {
    {_Bool  ReplaceReturn81 = false; Din_Go(89,2048); return ReplaceReturn81;}
  }

  {_Bool  ReplaceReturn80 = aComparator(cself, aOther, selflen) == 0; Din_Go(91,2048); return ReplaceReturn80;}
}

bool
nsAString::Equals(const self_type& aOther, ComparatorFunc aComparator) const
{
  Din_Go(92,2048);const char_type* cself;
  const char_type* cother;
  uint32_t selflen = NS_StringGetData(*this, &cself);
  uint32_t otherlen = NS_StringGetData(aOther, &cother);

  Din_Go(94,2048);if (selflen != otherlen) {
    {_Bool  ReplaceReturn79 = false; Din_Go(93,2048); return ReplaceReturn79;}
  }

  {_Bool  ReplaceReturn78 = aComparator(cself, cother, selflen) == 0; Din_Go(95,2048); return ReplaceReturn78;}
}

bool
nsAString::EqualsLiteral(const char* aASCIIString) const
{
  Din_Go(96,2048);const char16_t* begin;
  const char16_t* end;
  BeginReading(&begin, &end);

  Din_Go(99,2048);for (; begin < end; ++begin, ++aASCIIString) {
    Din_Go(97,2048);if (!*aASCIIString || !NS_IsAscii(*begin) ||
        (char)*begin != *aASCIIString) {
      {_Bool  ReplaceReturn77 = false; Din_Go(98,2048); return ReplaceReturn77;}
    }
  }

  {_Bool  ReplaceReturn76 = *aASCIIString == '\0'; Din_Go(100,2048); return ReplaceReturn76;}
}

bool
nsAString::LowerCaseEqualsLiteral(const char* aASCIIString) const
{
  Din_Go(101,2048);const char16_t* begin;
  const char16_t* end;
  BeginReading(&begin, &end);

  Din_Go(104,2048);for (; begin < end; ++begin, ++aASCIIString) {
    Din_Go(102,2048);if (!*aASCIIString || !NS_IsAscii(*begin) ||
        NS_ToLower((char)*begin) != *aASCIIString) {
      {_Bool  ReplaceReturn75 = false; Din_Go(103,2048); return ReplaceReturn75;}
    }
  }

  {_Bool  ReplaceReturn74 = *aASCIIString == '\0'; Din_Go(105,2048); return ReplaceReturn74;}
}

int32_t
nsAString::Find(const self_type& aStr, uint32_t aOffset,
                ComparatorFunc aComparator) const
{
  Din_Go(106,2048);const char_type* begin;
  const char_type* end;
  uint32_t selflen = BeginReading(&begin, &end);

  Din_Go(108,2048);if (aOffset > selflen) {
    {int32_t  ReplaceReturn73 = -1; Din_Go(107,2048); return ReplaceReturn73;}
  }

  Din_Go(109,2048);const char_type* other;
  uint32_t otherlen = aStr.BeginReading(&other);

  Din_Go(111,2048);if (otherlen > selflen - aOffset) {
    {int32_t  ReplaceReturn72 = -1; Din_Go(110,2048); return ReplaceReturn72;}
  }

  // We want to stop searching otherlen characters before the end of the string
  Din_Go(112,2048);end -= otherlen;

  Din_Go(115,2048);for (const char_type* cur = begin + aOffset; cur <= end; ++cur) {
    Din_Go(113,2048);if (!aComparator(cur, other, otherlen)) {
      {int32_t  ReplaceReturn71 = cur - begin; Din_Go(114,2048); return ReplaceReturn71;}
    }
  }
  {int32_t  ReplaceReturn70 = -1; Din_Go(116,2048); return ReplaceReturn70;}
}

static bool
ns_strnmatch(const char16_t* aStr, const char* aSubstring, uint32_t aLen)
{
  Din_Go(117,2048);for (; aLen; ++aStr, ++aSubstring, --aLen) {
    Din_Go(118,2048);if (!NS_IsAscii(*aStr)) {
      {_Bool  ReplaceReturn69 = false; Din_Go(119,2048); return ReplaceReturn69;}
    }

    Din_Go(121,2048);if ((char)*aStr != *aSubstring) {
      {_Bool  ReplaceReturn68 = false; Din_Go(120,2048); return ReplaceReturn68;}
    }
  }

  {_Bool  ReplaceReturn67 = true; Din_Go(122,2048); return ReplaceReturn67;}
}

static bool
ns_strnimatch(const char16_t* aStr, const char* aSubstring, uint32_t aLen)
{
  Din_Go(123,2048);for (; aLen; ++aStr, ++aSubstring, --aLen) {
    Din_Go(124,2048);if (!NS_IsAscii(*aStr)) {
      {_Bool  ReplaceReturn66 = false; Din_Go(125,2048); return ReplaceReturn66;}
    }

    Din_Go(127,2048);if (NS_ToLower((char)*aStr) != NS_ToLower(*aSubstring)) {
      {_Bool  ReplaceReturn65 = false; Din_Go(126,2048); return ReplaceReturn65;}
    }
  }

  {_Bool  ReplaceReturn64 = true; Din_Go(128,2048); return ReplaceReturn64;}
}

int32_t
nsAString::Find(const char* aStr, uint32_t aOffset, bool aIgnoreCase) const
{
  Din_Go(129,2048);bool (*match)(const char16_t*, const char*, uint32_t) =
    aIgnoreCase ? ns_strnimatch : ns_strnmatch;

  const char_type* begin;
  const char_type* end;
  uint32_t selflen = BeginReading(&begin, &end);

  Din_Go(131,2048);if (aOffset > selflen) {
    {int32_t  ReplaceReturn63 = -1; Din_Go(130,2048); return ReplaceReturn63;}
  }

  Din_Go(132,2048);uint32_t otherlen = strlen(aStr);

  Din_Go(134,2048);if (otherlen > selflen - aOffset) {
    {int32_t  ReplaceReturn62 = -1; Din_Go(133,2048); return ReplaceReturn62;}
  }

  // We want to stop searching otherlen characters before the end of the string
  Din_Go(135,2048);end -= otherlen;

  Din_Go(138,2048);for (const char_type* cur = begin + aOffset; cur <= end; ++cur) {
    Din_Go(136,2048);if (match(cur, aStr, otherlen)) {
      {int32_t  ReplaceReturn61 = cur - begin; Din_Go(137,2048); return ReplaceReturn61;}
    }
  }
  {int32_t  ReplaceReturn60 = -1; Din_Go(139,2048); return ReplaceReturn60;}
}

int32_t
nsAString::RFind(const self_type& aStr, int32_t aOffset,
                 ComparatorFunc aComparator) const
{
  Din_Go(140,2048);const char_type* begin;
  const char_type* end;
  uint32_t selflen = BeginReading(&begin, &end);

  const char_type* other;
  uint32_t otherlen = aStr.BeginReading(&other);

  Din_Go(142,2048);if (selflen < otherlen) {
    {int32_t  ReplaceReturn59 = -1; Din_Go(141,2048); return ReplaceReturn59;}
  }

  Din_Go(145,2048);if (aOffset < 0 || uint32_t(aOffset) > (selflen - otherlen)) {
    Din_Go(143,2048);end -= otherlen;
  } else {
    Din_Go(144,2048);end = begin + aOffset;
  }

  Din_Go(148,2048);for (const char_type* cur = end; cur >= begin; --cur) {
    Din_Go(146,2048);if (!aComparator(cur, other, otherlen)) {
      {int32_t  ReplaceReturn58 = cur - begin; Din_Go(147,2048); return ReplaceReturn58;}
    }
  }
  {int32_t  ReplaceReturn57 = -1; Din_Go(149,2048); return ReplaceReturn57;}
}

int32_t
nsAString::RFind(const char* aStr, int32_t aOffset, bool aIgnoreCase) const
{
  Din_Go(150,2048);bool (*match)(const char16_t*, const char*, uint32_t) =
    aIgnoreCase ? ns_strnimatch : ns_strnmatch;

  const char_type* begin;
  const char_type* end;
  uint32_t selflen = BeginReading(&begin, &end);
  uint32_t otherlen = strlen(aStr);

  Din_Go(152,2048);if (selflen < otherlen) {
    {int32_t  ReplaceReturn56 = -1; Din_Go(151,2048); return ReplaceReturn56;}
  }

  Din_Go(155,2048);if (aOffset < 0 || uint32_t(aOffset) > (selflen - otherlen)) {
    Din_Go(153,2048);end -= otherlen;
  } else {
    Din_Go(154,2048);end = begin + aOffset;
  }

  Din_Go(158,2048);for (const char_type* cur = end; cur >= begin; --cur) {
    Din_Go(156,2048);if (match(cur, aStr, otherlen)) {
      {int32_t  ReplaceReturn55 = cur - begin; Din_Go(157,2048); return ReplaceReturn55;}
    }
  }
  {int32_t  ReplaceReturn54 = -1; Din_Go(159,2048); return ReplaceReturn54;}
}

int32_t
nsAString::FindChar(char_type aChar, uint32_t aOffset) const
{
  Din_Go(160,2048);const char_type* start;
  const char_type* end;
  uint32_t len = BeginReading(&start, &end);
  Din_Go(162,2048);if (aOffset > len) {
    {int32_t  ReplaceReturn53 = -1; Din_Go(161,2048); return ReplaceReturn53;}
  }

  Din_Go(163,2048);const char_type* cur;

  Din_Go(166,2048);for (cur = start + aOffset; cur < end; ++cur) {
    Din_Go(164,2048);if (*cur == aChar) {
      {int32_t  ReplaceReturn52 = cur - start; Din_Go(165,2048); return ReplaceReturn52;}
    }
  }

  {int32_t  ReplaceReturn51 = -1; Din_Go(167,2048); return ReplaceReturn51;}
}

int32_t
nsAString::RFindChar(char_type aChar) const
{
  Din_Go(168,2048);const char16_t* start;
  const char16_t* end;
  BeginReading(&start, &end);

  Din_Go(172,2048);do {
    Din_Go(169,2048);--end;

    Din_Go(171,2048);if (*end == aChar) {
      {int32_t  ReplaceReturn50 = end - start; Din_Go(170,2048); return ReplaceReturn50;}
    }

  } while (end >= start);

  {int32_t  ReplaceReturn49 = -1; Din_Go(173,2048); return ReplaceReturn49;}
}

void
nsAString::AppendInt(int aInt, int32_t aRadix)
{
  Din_Go(174,2048);const char* fmt;
  Din_Go(182,2048);switch (aRadix) {
    case 8:
      Din_Go(175,2048);fmt = "%o";
      Din_Go(176,2048);break;

    case 10:
      Din_Go(177,2048);fmt = "%d";
      Din_Go(178,2048);break;

    case 16:
      Din_Go(179,2048);fmt = "%x";
      Din_Go(180,2048);break;

    default:
      NS_ERROR("Unrecognized radix");
      Din_Go(181,2048);fmt = "";
  };

  Din_Go(183,2048);char buf[20];
  int len = snprintf(buf, sizeof(buf), fmt, aInt);
  buf[sizeof(buf) - 1] = '\0';

  Append(NS_ConvertASCIItoUTF16(buf, len));
Din_Go(184,2048);}

// Strings

#ifndef XPCOM_GLUE_AVOID_NSPR
int32_t
nsAString::ToInteger(nsresult* aErrorCode, uint32_t aRadix) const
{
  NS_ConvertUTF16toUTF8 narrow(*this);

  const char* fmt;
  switch (aRadix) {
    case 10:
      fmt = "%i";
      break;

    case 16:
      fmt = "%x";
      break;

    default:
      NS_ERROR("Unrecognized radix!");
      *aErrorCode = NS_ERROR_INVALID_ARG;
      return 0;
  }

  int32_t result = 0;
  if (PR_sscanf(narrow.get(), fmt, &result) == 1) {
    *aErrorCode = NS_OK;
  } else {
    *aErrorCode = NS_ERROR_FAILURE;
  }

  return result;
}

int64_t
nsAString::ToInteger64(nsresult* aErrorCode, uint32_t aRadix) const
{
  NS_ConvertUTF16toUTF8 narrow(*this);

  const char* fmt;
  switch (aRadix) {
    case 10:
      fmt = "%lli";
      break;

    case 16:
      fmt = "%llx";
      break;

    default:
      NS_ERROR("Unrecognized radix!");
      *aErrorCode = NS_ERROR_INVALID_ARG;
      return 0;
  }

  int64_t result = 0;
  if (PR_sscanf(narrow.get(), fmt, &result) == 1) {
    *aErrorCode = NS_OK;
  } else {
    *aErrorCode = NS_ERROR_FAILURE;
  }

  return result;
}
#endif // XPCOM_GLUE_AVOID_NSPR

// nsACString

uint32_t
nsACString::BeginReading(const char_type** aBegin, const char_type** aEnd) const
{
  Din_Go(185,2048);uint32_t len = NS_CStringGetData(*this, aBegin);
  Din_Go(187,2048);if (aEnd) {
    Din_Go(186,2048);*aEnd = *aBegin + len;
  }

  {uint32_t  ReplaceReturn48 = len; Din_Go(188,2048); return ReplaceReturn48;}
}

const nsACString::char_type*
nsACString::BeginReading() const
{
  Din_Go(189,2048);const char_type* data;
  NS_CStringGetData(*this, &data);
  {const nsACString::char_type * ReplaceReturn47 = data; Din_Go(190,2048); return ReplaceReturn47;}
}

const nsACString::char_type*
nsACString::EndReading() const
{
  Din_Go(191,2048);const char_type* data;
  uint32_t len = NS_CStringGetData(*this, &data);
  {const nsACString::char_type * ReplaceReturn46 = data + len; Din_Go(192,2048); return ReplaceReturn46;}
}

uint32_t
nsACString::BeginWriting(char_type** aBegin, char_type** aEnd,
                         uint32_t aNewSize)
{
  Din_Go(193,2048);uint32_t len = NS_CStringGetMutableData(*this, aNewSize, aBegin);
  Din_Go(195,2048);if (aEnd) {
    Din_Go(194,2048);*aEnd = *aBegin + len;
  }

  {uint32_t  ReplaceReturn45 = len; Din_Go(196,2048); return ReplaceReturn45;}
}

nsACString::char_type*
nsACString::BeginWriting(uint32_t aLen)
{
  Din_Go(197,2048);char_type* data;
  NS_CStringGetMutableData(*this, aLen, &data);
  {nsACString::char_type * ReplaceReturn44 = data; Din_Go(198,2048); return ReplaceReturn44;}
}

nsACString::char_type*
nsACString::EndWriting()
{
  Din_Go(199,2048);char_type* data;
  uint32_t len = NS_CStringGetMutableData(*this, UINT32_MAX, &data);
  {nsACString::char_type * ReplaceReturn43 = data + len; Din_Go(200,2048); return ReplaceReturn43;}
}

bool
nsACString::SetLength(uint32_t aLen)
{
  Din_Go(201,2048);char_type* data;
  NS_CStringGetMutableData(*this, aLen, &data);
  {_Bool  ReplaceReturn42 = data != nullptr; Din_Go(202,2048); return ReplaceReturn42;}
}

void
nsACString::StripChars(const char* aSet)
{
Din_Go(203,2048);  nsCString copy(*this);

  const char_type* source;
  const char_type* sourceEnd;
  copy.BeginReading(&source, &sourceEnd);

  char_type* dest;
  BeginWriting(&dest);
  Din_Go(205,2048);if (!dest) {
    Din_Go(204,2048);return;
  }

  Din_Go(206,2048);char_type* curDest = dest;

  Din_Go(213,2048);for (; source < sourceEnd; ++source) {
    Din_Go(207,2048);const char* test;
    Din_Go(210,2048);for (test = aSet; *test; ++test) {
      Din_Go(208,2048);if (*source == char_type(*test)) {
        Din_Go(209,2048);break;
      }
    }

    Din_Go(212,2048);if (!*test) {
      // not stripped, copy this char
      Din_Go(211,2048);*curDest = *source;
      ++curDest;
    }
  }

  Din_Go(214,2048);SetLength(curDest - dest);
Din_Go(215,2048);}

void
nsACString::Trim(const char* aSet, bool aLeading, bool aTrailing)
{
Din_Go(216,2048);  NS_ASSERTION(aLeading || aTrailing, "Ineffective Trim");

  Din_Go(217,2048);const char* start;
  const char* end;
  uint32_t cutLen;

  Din_Go(228,2048);if (aLeading) {
    Din_Go(218,2048);BeginReading(&start, &end);
    Din_Go(225,2048);for (cutLen = 0; start < end; ++start, ++cutLen) {
      Din_Go(219,2048);const char* test;
      Din_Go(222,2048);for (test = aSet; *test; ++test) {
        Din_Go(220,2048);if (*test == *start) {
          Din_Go(221,2048);break;
        }
      }
      Din_Go(224,2048);if (!*test) {
        Din_Go(223,2048);break;
      }
    }
    Din_Go(227,2048);if (cutLen) {
      Din_Go(226,2048);NS_CStringCutData(*this, 0, cutLen);
    }
  }
  Din_Go(239,2048);if (aTrailing) {
    Din_Go(229,2048);uint32_t len = BeginReading(&start, &end);
    --end;
    Din_Go(236,2048);for (cutLen = 0; end >= start; --end, ++cutLen) {
      Din_Go(230,2048);const char* test;
      Din_Go(233,2048);for (test = aSet; *test; ++test) {
        Din_Go(231,2048);if (*test == *end) {
          Din_Go(232,2048);break;
        }
      }
      Din_Go(235,2048);if (!*test) {
        Din_Go(234,2048);break;
      }
    }
    Din_Go(238,2048);if (cutLen) {
      Din_Go(237,2048);NS_CStringCutData(*this, len - cutLen, cutLen);
    }
  }
Din_Go(240,2048);}

int32_t
nsACString::DefaultComparator(const char_type* aStrA, const char_type* aStrB,
                              uint32_t aLen)
{
  {int32_t  ReplaceReturn41 = memcmp(aStrA, aStrB, aLen); Din_Go(241,2048); return ReplaceReturn41;}
}

int32_t
nsACString::Compare(const char_type* aOther, ComparatorFunc aComparator) const
{
  Din_Go(242,2048);const char_type* cself;
  uint32_t selflen = NS_CStringGetData(*this, &cself);
  uint32_t otherlen = strlen(aOther);
  uint32_t comparelen = selflen <= otherlen ? selflen : otherlen;

  int32_t result = aComparator(cself, aOther, comparelen);
  Din_Go(247,2048);if (result == 0) {
    Din_Go(243,2048);if (selflen < otherlen) {
      {int32_t  ReplaceReturn40 = -1; Din_Go(244,2048); return ReplaceReturn40;}
    } else {/*5*/Din_Go(245,2048);if (selflen > otherlen) {
      {int32_t  ReplaceReturn39 = 1; Din_Go(246,2048); return ReplaceReturn39;}
    ;/*6*/}}
  }
  {int32_t  ReplaceReturn38 = result; Din_Go(248,2048); return ReplaceReturn38;}
}

int32_t
nsACString::Compare(const self_type& aOther, ComparatorFunc aComparator) const
{
  Din_Go(249,2048);const char_type* cself;
  const char_type* cother;
  uint32_t selflen = NS_CStringGetData(*this, &cself);
  uint32_t otherlen = NS_CStringGetData(aOther, &cother);
  uint32_t comparelen = selflen <= otherlen ? selflen : otherlen;

  int32_t result = aComparator(cself, cother, comparelen);
  Din_Go(254,2048);if (result == 0) {
    Din_Go(250,2048);if (selflen < otherlen) {
      {int32_t  ReplaceReturn37 = -1; Din_Go(251,2048); return ReplaceReturn37;}
    } else {/*7*/Din_Go(252,2048);if (selflen > otherlen) {
      {int32_t  ReplaceReturn36 = 1; Din_Go(253,2048); return ReplaceReturn36;}
    ;/*8*/}}
  }
  {int32_t  ReplaceReturn35 = result; Din_Go(255,2048); return ReplaceReturn35;}
}

bool
nsACString::Equals(const char_type* aOther, ComparatorFunc aComparator) const
{
  Din_Go(256,2048);const char_type* cself;
  uint32_t selflen = NS_CStringGetData(*this, &cself);
  uint32_t otherlen = strlen(aOther);

  Din_Go(258,2048);if (selflen != otherlen) {
    {_Bool  ReplaceReturn34 = false; Din_Go(257,2048); return ReplaceReturn34;}
  }

  {_Bool  ReplaceReturn33 = aComparator(cself, aOther, selflen) == 0; Din_Go(259,2048); return ReplaceReturn33;}
}

bool
nsACString::Equals(const self_type& aOther, ComparatorFunc aComparator) const
{
  Din_Go(260,2048);const char_type* cself;
  const char_type* cother;
  uint32_t selflen = NS_CStringGetData(*this, &cself);
  uint32_t otherlen = NS_CStringGetData(aOther, &cother);

  Din_Go(262,2048);if (selflen != otherlen) {
    {_Bool  ReplaceReturn32 = false; Din_Go(261,2048); return ReplaceReturn32;}
  }

  {_Bool  ReplaceReturn31 = aComparator(cself, cother, selflen) == 0; Din_Go(263,2048); return ReplaceReturn31;}
}

int32_t
nsACString::Find(const self_type& aStr, uint32_t aOffset,
                 ComparatorFunc aComparator) const
{
  Din_Go(264,2048);const char_type* begin;
  const char_type* end;
  uint32_t selflen = BeginReading(&begin, &end);

  Din_Go(266,2048);if (aOffset > selflen) {
    {int32_t  ReplaceReturn30 = -1; Din_Go(265,2048); return ReplaceReturn30;}
  }

  Din_Go(267,2048);const char_type* other;
  uint32_t otherlen = aStr.BeginReading(&other);

  Din_Go(269,2048);if (otherlen > selflen - aOffset) {
    {int32_t  ReplaceReturn29 = -1; Din_Go(268,2048); return ReplaceReturn29;}
  }

  // We want to stop searching otherlen characters before the end of the string
  Din_Go(270,2048);end -= otherlen;

  Din_Go(273,2048);for (const char_type* cur = begin + aOffset; cur <= end; ++cur) {
    Din_Go(271,2048);if (!aComparator(cur, other, otherlen)) {
      {int32_t  ReplaceReturn28 = cur - begin; Din_Go(272,2048); return ReplaceReturn28;}
    }
  }
  {int32_t  ReplaceReturn27 = -1; Din_Go(274,2048); return ReplaceReturn27;}
}

int32_t
nsACString::Find(const char_type* aStr, ComparatorFunc aComparator) const
{
  {int32_t  ReplaceReturn26 = Find(aStr, strlen(aStr), aComparator); Din_Go(275,2048); return ReplaceReturn26;}
}

int32_t
nsACString::Find(const char_type* aStr, uint32_t aLen,
                 ComparatorFunc aComparator) const
{
  Din_Go(276,2048);const char_type* begin;
  const char_type* end;
  uint32_t selflen = BeginReading(&begin, &end);

  Din_Go(278,2048);if (aLen == 0) {
    NS_WARNING("Searching for zero-length string.");
    {int32_t  ReplaceReturn25 = -1; Din_Go(277,2048); return ReplaceReturn25;}
  }

  Din_Go(280,2048);if (aLen > selflen) {
    {int32_t  ReplaceReturn24 = -1; Din_Go(279,2048); return ReplaceReturn24;}
  }

  // We want to stop searching otherlen characters before the end of the string
  Din_Go(281,2048);end -= aLen;

  Din_Go(284,2048);for (const char_type* cur = begin; cur <= end; ++cur) {
    Din_Go(282,2048);if (!aComparator(cur, aStr, aLen)) {
      {int32_t  ReplaceReturn23 = cur - begin; Din_Go(283,2048); return ReplaceReturn23;}
    }
  }
  {int32_t  ReplaceReturn22 = -1; Din_Go(285,2048); return ReplaceReturn22;}
}

int32_t
nsACString::RFind(const self_type& aStr, int32_t aOffset,
                  ComparatorFunc aComparator) const
{
  Din_Go(286,2048);const char_type* begin;
  const char_type* end;
  uint32_t selflen = BeginReading(&begin, &end);

  const char_type* other;
  uint32_t otherlen = aStr.BeginReading(&other);

  Din_Go(288,2048);if (selflen < otherlen) {
    {int32_t  ReplaceReturn21 = -1; Din_Go(287,2048); return ReplaceReturn21;}
  }

  Din_Go(291,2048);if (aOffset < 0 || uint32_t(aOffset) > (selflen - otherlen)) {
    Din_Go(289,2048);end -= otherlen;
  } else {
    Din_Go(290,2048);end = begin + aOffset;
  }

  Din_Go(294,2048);for (const char_type* cur = end; cur >= begin; --cur) {
    Din_Go(292,2048);if (!aComparator(cur, other, otherlen)) {
      {int32_t  ReplaceReturn20 = cur - begin; Din_Go(293,2048); return ReplaceReturn20;}
    }
  }
  {int32_t  ReplaceReturn19 = -1; Din_Go(295,2048); return ReplaceReturn19;}
}

int32_t
nsACString::RFind(const char_type* aStr, ComparatorFunc aComparator) const
{
  {int32_t  ReplaceReturn18 = RFind(aStr, strlen(aStr), aComparator); Din_Go(296,2048); return ReplaceReturn18;}
}

int32_t
nsACString::RFind(const char_type* aStr, int32_t aLen,
                  ComparatorFunc aComparator) const
{
  Din_Go(297,2048);const char_type* begin;
  const char_type* end;
  uint32_t selflen = BeginReading(&begin, &end);

  Din_Go(299,2048);if (aLen <= 0) {
    NS_WARNING("Searching for zero-length string.");
    {int32_t  ReplaceReturn17 = -1; Din_Go(298,2048); return ReplaceReturn17;}
  }

  Din_Go(301,2048);if (uint32_t(aLen) > selflen) {
    {int32_t  ReplaceReturn16 = -1; Din_Go(300,2048); return ReplaceReturn16;}
  }

  // We want to start searching otherlen characters before the end of the string
  Din_Go(302,2048);end -= aLen;

  Din_Go(305,2048);for (const char_type* cur = end; cur >= begin; --cur) {
    Din_Go(303,2048);if (!aComparator(cur, aStr, aLen)) {
      {int32_t  ReplaceReturn15 = cur - begin; Din_Go(304,2048); return ReplaceReturn15;}
    }
  }
  {int32_t  ReplaceReturn14 = -1; Din_Go(306,2048); return ReplaceReturn14;}
}

int32_t
nsACString::FindChar(char_type aChar, uint32_t aOffset) const
{
  Din_Go(307,2048);const char_type* start;
  const char_type* end;
  uint32_t len = BeginReading(&start, &end);
  Din_Go(309,2048);if (aOffset > len) {
    {int32_t  ReplaceReturn13 = -1; Din_Go(308,2048); return ReplaceReturn13;}
  }

  Din_Go(310,2048);const char_type* cur;

  Din_Go(313,2048);for (cur = start + aOffset; cur < end; ++cur) {
    Din_Go(311,2048);if (*cur == aChar) {
      {int32_t  ReplaceReturn12 = cur - start; Din_Go(312,2048); return ReplaceReturn12;}
    }
  }

  {int32_t  ReplaceReturn11 = -1; Din_Go(314,2048); return ReplaceReturn11;}
}

int32_t
nsACString::RFindChar(char_type aChar) const
{
  Din_Go(315,2048);const char* start;
  const char* end;
  BeginReading(&start, &end);

  Din_Go(318,2048);for (; end >= start; --end) {
    Din_Go(316,2048);if (*end == aChar) {
      {int32_t  ReplaceReturn10 = end - start; Din_Go(317,2048); return ReplaceReturn10;}
    }
  }

  {int32_t  ReplaceReturn9 = -1; Din_Go(319,2048); return ReplaceReturn9;}
}

void
nsACString::AppendInt(int aInt, int32_t aRadix)
{
  Din_Go(320,2048);const char* fmt;
  Din_Go(328,2048);switch (aRadix) {
    case 8:
      Din_Go(321,2048);fmt = "%o";
      Din_Go(322,2048);break;

    case 10:
      Din_Go(323,2048);fmt = "%d";
      Din_Go(324,2048);break;

    case 16:
      Din_Go(325,2048);fmt = "%x";
      Din_Go(326,2048);break;

    default:
      NS_ERROR("Unrecognized radix");
      Din_Go(327,2048);fmt = "";
  };

  Din_Go(329,2048);char buf[20];
  int len = snprintf(buf, sizeof(buf), fmt, aInt);
  buf[sizeof(buf) - 1] = '\0';

  Append(buf, len);
Din_Go(330,2048);}

#ifndef XPCOM_GLUE_AVOID_NSPR
int32_t
nsACString::ToInteger(nsresult* aErrorCode, uint32_t aRadix) const
{
  const char* fmt;
  switch (aRadix) {
    case 10:
      fmt = "%i";
      break;

    case 16:
      fmt = "%x";
      break;

    default:
      NS_ERROR("Unrecognized radix!");
      *aErrorCode = NS_ERROR_INVALID_ARG;
      return 0;
  }

  int32_t result = 0;
  if (PR_sscanf(nsCString(*this).get(), fmt, &result) == 1) {
    *aErrorCode = NS_OK;
  } else {
    *aErrorCode = NS_ERROR_FAILURE;
  }

  return result;
}

int64_t
nsACString::ToInteger64(nsresult* aErrorCode, uint32_t aRadix) const
{
  const char* fmt;
  switch (aRadix) {
    case 10:
      fmt = "%lli";
      break;

    case 16:
      fmt = "%llx";
      break;

    default:
      NS_ERROR("Unrecognized radix!");
      *aErrorCode = NS_ERROR_INVALID_ARG;
      return 0;
  }

  int64_t result = 0;
  if (PR_sscanf(nsCString(*this).get(), fmt, &result) == 1) {
    *aErrorCode = NS_OK;
  } else {
    *aErrorCode = NS_ERROR_FAILURE;
  }

  return result;
}
#endif // XPCOM_GLUE_AVOID_NSPR

// Substrings

nsDependentSubstring::nsDependentSubstring(const abstract_string_type& aStr,
                                           uint32_t aStartPos)
{
  Din_Go(331,2048);const char16_t* data;
  uint32_t len = NS_StringGetData(aStr, &data);

  Din_Go(333,2048);if (aStartPos > len) {
    Din_Go(332,2048);aStartPos = len;
  }

  Din_Go(334,2048);NS_StringContainerInit2(*this, data + aStartPos, len - aStartPos,
                          NS_STRING_CONTAINER_INIT_DEPEND |
                          NS_STRING_CONTAINER_INIT_SUBSTRING);
Din_Go(335,2048);}

nsDependentSubstring::nsDependentSubstring(const abstract_string_type& aStr,
                                           uint32_t aStartPos,
                                           uint32_t aLength)
{
  Din_Go(336,2048);const char16_t* data;
  uint32_t len = NS_StringGetData(aStr, &data);

  Din_Go(338,2048);if (aStartPos > len) {
    Din_Go(337,2048);aStartPos = len;
  }

  Din_Go(340,2048);if (aStartPos + aLength > len) {
    Din_Go(339,2048);aLength = len - aStartPos;
  }

  Din_Go(341,2048);NS_StringContainerInit2(*this, data + aStartPos, aLength,
                          NS_STRING_CONTAINER_INIT_DEPEND |
                            NS_STRING_CONTAINER_INIT_SUBSTRING);
Din_Go(342,2048);}

nsDependentCSubstring::nsDependentCSubstring(const abstract_string_type& aStr,
                                             uint32_t aStartPos)
{
  Din_Go(343,2048);const char* data;
  uint32_t len = NS_CStringGetData(aStr, &data);

  Din_Go(345,2048);if (aStartPos > len) {
    Din_Go(344,2048);aStartPos = len;
  }

  Din_Go(346,2048);NS_CStringContainerInit2(*this, data + aStartPos, len - aStartPos,
                           NS_CSTRING_CONTAINER_INIT_DEPEND |
                           NS_CSTRING_CONTAINER_INIT_SUBSTRING);
Din_Go(347,2048);}

nsDependentCSubstring::nsDependentCSubstring(const abstract_string_type& aStr,
                                             uint32_t aStartPos,
                                             uint32_t aLength)
{
  Din_Go(348,2048);const char* data;
  uint32_t len = NS_CStringGetData(aStr, &data);

  Din_Go(350,2048);if (aStartPos > len) {
    Din_Go(349,2048);aStartPos = len;
  }

  Din_Go(352,2048);if (aStartPos + aLength > len) {
    Din_Go(351,2048);aLength = len - aStartPos;
  }

  Din_Go(353,2048);NS_CStringContainerInit2(*this, data + aStartPos, aLength,
                           NS_CSTRING_CONTAINER_INIT_DEPEND |
                           NS_CSTRING_CONTAINER_INIT_SUBSTRING);
Din_Go(354,2048);}

// Utils

char*
ToNewUTF8String(const nsAString& aSource)
{
Din_Go(355,2048);  nsCString temp;
  CopyUTF16toUTF8(aSource, temp);
  {char * ReplaceReturn8 = NS_CStringCloneData(temp); Din_Go(356,2048); return ReplaceReturn8;}
}

void
CompressWhitespace(nsAString& aString)
{
  Din_Go(357,2048);char16_t* start;
  uint32_t len = NS_StringGetMutableData(aString, UINT32_MAX, &start);
  char16_t* end = start + len;
  char16_t* from = start;
  char16_t* to = start;

  // Skip any leading whitespace
  Din_Go(359,2048);while (from < end && NS_IsAsciiWhitespace(*from)) {
    Din_Go(358,2048);from++;
  }

  Din_Go(366,2048);while (from < end) {
    Din_Go(360,2048);char16_t theChar = *from++;

    Din_Go(364,2048);if (NS_IsAsciiWhitespace(theChar)) {
      // We found a whitespace char, so skip over any more
      Din_Go(361,2048);while (from < end && NS_IsAsciiWhitespace(*from)) {
        Din_Go(362,2048);from++;
      }

      // Turn all whitespace into spaces
      Din_Go(363,2048);theChar = ' ';
    }

    Din_Go(365,2048);*to++ = theChar;
  }

  // Drop any trailing space
  Din_Go(368,2048);if (to > start && to[-1] == ' ') {
    Din_Go(367,2048);to--;
  }

  // Re-terminate the string
  Din_Go(369,2048);*to = '\0';

  // Set the new length
  aString.SetLength(to - start);
Din_Go(370,2048);}

uint32_t
ToLowerCase(nsACString& aStr)
{
  Din_Go(371,2048);char* begin;
  char* end;
  uint32_t len = aStr.BeginWriting(&begin, &end);

  Din_Go(373,2048);for (; begin < end; ++begin) {
    Din_Go(372,2048);*begin = NS_ToLower(*begin);
  }

  {uint32_t  ReplaceReturn7 = len; Din_Go(374,2048); return ReplaceReturn7;}
}

uint32_t
ToUpperCase(nsACString& aStr)
{
  Din_Go(375,2048);char* begin;
  char* end;
  uint32_t len = aStr.BeginWriting(&begin, &end);

  Din_Go(377,2048);for (; begin < end; ++begin) {
    Din_Go(376,2048);*begin = NS_ToUpper(*begin);
  }

  {uint32_t  ReplaceReturn6 = len; Din_Go(378,2048); return ReplaceReturn6;}
}

uint32_t
ToLowerCase(const nsACString& aSrc, nsACString& aDest)
{
  Din_Go(379,2048);const char* begin;
  const char* end;
  uint32_t len = aSrc.BeginReading(&begin, &end);

  char* dest;
  NS_CStringGetMutableData(aDest, len, &dest);

  Din_Go(381,2048);for (; begin < end; ++begin, ++dest) {
    Din_Go(380,2048);*dest = NS_ToLower(*begin);
  }

  {uint32_t  ReplaceReturn5 = len; Din_Go(382,2048); return ReplaceReturn5;}
}

uint32_t
ToUpperCase(const nsACString& aSrc, nsACString& aDest)
{
  Din_Go(383,2048);const char* begin;
  const char* end;
  uint32_t len = aSrc.BeginReading(&begin, &end);

  char* dest;
  NS_CStringGetMutableData(aDest, len, &dest);

  Din_Go(385,2048);for (; begin < end; ++begin, ++dest) {
    Din_Go(384,2048);*dest = NS_ToUpper(*begin);
  }

  {uint32_t  ReplaceReturn4 = len; Din_Go(386,2048); return ReplaceReturn4;}
}

int32_t
CaseInsensitiveCompare(const char* aStrA, const char* aStrB,
                       uint32_t aLen)
{
  Din_Go(387,2048);for (const char* aend = aStrA + aLen; aStrA < aend; ++aStrA, ++aStrB) {
    Din_Go(388,2048);char la = NS_ToLower(*aStrA);
    char lb = NS_ToLower(*aStrB);

    Din_Go(390,2048);if (la == lb) {
      Din_Go(389,2048);continue;
    }

    {int32_t  ReplaceReturn3 = la < lb ? -1 : 1; Din_Go(391,2048); return ReplaceReturn3;}
  }

  {int32_t  ReplaceReturn2 = 0; Din_Go(392,2048); return ReplaceReturn2;}
}

bool
ParseString(const nsACString& aSource, char aDelimiter,
            nsTArray<nsCString>& aArray)
{
  Din_Go(393,2048);int32_t start = 0;
  int32_t end = aSource.Length();

  uint32_t oldLength = aArray.Length();

  Din_Go(406,2048);for (;;) {
    Din_Go(394,2048);int32_t delimiter = aSource.FindChar(aDelimiter, start);
    Din_Go(396,2048);if (delimiter < 0) {
      Din_Go(395,2048);delimiter = end;
    }

    Din_Go(400,2048);if (delimiter != start) {
      Din_Go(397,2048);if (!aArray.AppendElement(Substring(aSource, start, delimiter - start))) {
        Din_Go(398,2048);aArray.RemoveElementsAt(oldLength, aArray.Length() - oldLength);
        {_Bool  ReplaceReturn1 = false; Din_Go(399,2048); return ReplaceReturn1;}
      }
    }

    Din_Go(402,2048);if (delimiter == end) {
      Din_Go(401,2048);break;
    }
    Din_Go(403,2048);start = ++delimiter;
    Din_Go(405,2048);if (start == end) {
      Din_Go(404,2048);break;
    }
  }

  {_Bool  ReplaceReturn0 = true; Din_Go(407,2048); return ReplaceReturn0;}
}

#undef snprintf
