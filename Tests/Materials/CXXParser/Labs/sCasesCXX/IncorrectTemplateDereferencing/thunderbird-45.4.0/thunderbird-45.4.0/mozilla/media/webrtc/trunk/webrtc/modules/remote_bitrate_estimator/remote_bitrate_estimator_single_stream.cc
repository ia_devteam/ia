/*
 *  Copyright (c) 2012 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */
#include <map>
#include "var/tmp/sensor.h"

#include "webrtc/base/constructormagic.h"
#include "webrtc/base/scoped_ptr.h"
#include "webrtc/base/thread_annotations.h"
#include "webrtc/modules/remote_bitrate_estimator/rate_statistics.h"
#include "webrtc/modules/remote_bitrate_estimator/include/remote_bitrate_estimator.h"
#include "webrtc/modules/remote_bitrate_estimator/inter_arrival.h"
#include "webrtc/modules/remote_bitrate_estimator/overuse_detector.h"
#include "webrtc/modules/remote_bitrate_estimator/overuse_estimator.h"
#include "webrtc/modules/remote_bitrate_estimator/remote_rate_control.h"
#include "webrtc/system_wrappers/interface/clock.h"
#include "webrtc/system_wrappers/interface/critical_section_wrapper.h"
#include "webrtc/system_wrappers/interface/logging.h"
#include "webrtc/typedefs.h"

namespace webrtc {

enum { kTimestampGroupLengthMs = 5 };
static const double kTimestampToMs = 1.0 / 90.0;

class RemoteBitrateEstimatorImpl : public RemoteBitrateEstimator {
 public:
  RemoteBitrateEstimatorImpl(RemoteBitrateObserver* observer,
                             Clock* clock,
                             RateControlType control_type,
                             uint32_t min_bitrate_bps);
  virtual ~RemoteBitrateEstimatorImpl();

  void IncomingPacket(int64_t arrival_time_ms,
                      size_t payload_size,
                      const RTPHeader& header) override;
  int32_t Process() override;
  int64_t TimeUntilNextProcess() override;
  void OnRttUpdate(int64_t rtt) override;
  void RemoveStream(unsigned int ssrc) override;
  bool LatestEstimate(std::vector<unsigned int>* ssrcs,
                      unsigned int* bitrate_bps) const override;
  bool GetStats(ReceiveBandwidthEstimatorStats* output) const override;

 private:
  struct Detector {
    explicit Detector(int64_t last_packet_time_ms,
                      const OverUseDetectorOptions& options,
                      bool enable_burst_grouping)
        : last_packet_time_ms(last_packet_time_ms),
          inter_arrival(90 * kTimestampGroupLengthMs, kTimestampToMs,
                        enable_burst_grouping),
          estimator(options),
          detector(options) {}
    int64_t last_packet_time_ms;
    InterArrival inter_arrival;
    OveruseEstimator estimator;
    OveruseDetector detector;
  };

  typedef std::map<unsigned int, Detector*> SsrcOveruseEstimatorMap;

  // Triggers a new estimate calculation.
  void UpdateEstimate(int64_t time_now)
      EXCLUSIVE_LOCKS_REQUIRED(crit_sect_.get());

  void GetSsrcs(std::vector<unsigned int>* ssrcs) const
      SHARED_LOCKS_REQUIRED(crit_sect_.get());

  Clock* clock_;
  SsrcOveruseEstimatorMap overuse_detectors_ GUARDED_BY(crit_sect_.get());
  RateStatistics incoming_bitrate_ GUARDED_BY(crit_sect_.get());
  rtc::scoped_ptr<RemoteRateControl> remote_rate_ GUARDED_BY(crit_sect_.get());
  RemoteBitrateObserver* observer_ GUARDED_BY(crit_sect_.get());
  rtc::scoped_ptr<CriticalSectionWrapper> crit_sect_;
  int64_t last_process_time_;
  int64_t process_interval_ms_ GUARDED_BY(crit_sect_.get());

  DISALLOW_IMPLICIT_CONSTRUCTORS(RemoteBitrateEstimatorImpl);
};

RemoteBitrateEstimatorImpl::RemoteBitrateEstimatorImpl(
    RemoteBitrateObserver* observer,
    Clock* clock,
    RateControlType control_type,
    uint32_t min_bitrate_bps)
    : clock_(clock),
      incoming_bitrate_(1000, 8000),
      remote_rate_(RemoteRateControl::Create(control_type, min_bitrate_bps)),
      observer_(observer),
      crit_sect_(CriticalSectionWrapper::CreateCriticalSection()),
      last_process_time_(-1),
      process_interval_ms_(kProcessIntervalMs) {
Din_Go(36,2048);  assert(observer_);
Din_Go(37,2048);}

RemoteBitrateEstimatorImpl::~RemoteBitrateEstimatorImpl() {
  Din_Go(38,2048);while (!overuse_detectors_.empty()) {
    Din_Go(39,2048);SsrcOveruseEstimatorMap::iterator it = overuse_detectors_.begin();
    delete it->second;
    overuse_detectors_.erase(it);
  }
Din_Go(40,2048);}

void RemoteBitrateEstimatorImpl::IncomingPacket(
    int64_t arrival_time_ms,
    size_t payload_size,
    const RTPHeader& header) {
  Din_Go(41,2048);uint32_t ssrc = header.ssrc;
  uint32_t rtp_timestamp = header.timestamp +
      header.extension.transmissionTimeOffset;
  int64_t now_ms = clock_->TimeInMilliseconds();
  CriticalSectionScoped cs(crit_sect_.get());
  SsrcOveruseEstimatorMap::iterator it = overuse_detectors_.find(ssrc);
  Din_Go(43,2048);if (it == overuse_detectors_.end()) {
    // This is a new SSRC. Adding to map.
    // TODO(holmer): If the channel changes SSRC the old SSRC will still be
    // around in this map until the channel is deleted. This is OK since the
    // callback will no longer be called for the old SSRC. This will be
    // automatically cleaned up when we have one RemoteBitrateEstimator per REMB
    // group.
    Din_Go(42,2048);std::pair<SsrcOveruseEstimatorMap::iterator, bool> insert_result =
        overuse_detectors_.insert(std::make_pair(ssrc, new Detector(
            now_ms,
            OverUseDetectorOptions(),
            remote_rate_->GetControlType() == kAimdControl)));
    it = insert_result.first;
  }
  Din_Go(44,2048);Detector* estimator = it->second;
  estimator->last_packet_time_ms = now_ms;
  incoming_bitrate_.Update(payload_size, now_ms);
  const BandwidthUsage prior_state = estimator->detector.State();
  uint32_t timestamp_delta = 0;
  int64_t time_delta = 0;
  int size_delta = 0;
  Din_Go(46,2048);if (estimator->inter_arrival.ComputeDeltas(rtp_timestamp, arrival_time_ms,
                                             payload_size, &timestamp_delta,
                                             &time_delta, &size_delta)) {
    Din_Go(45,2048);double timestamp_delta_ms = timestamp_delta * kTimestampToMs;
    estimator->estimator.Update(time_delta, timestamp_delta_ms, size_delta,
                                estimator->detector.State());
    estimator->detector.Detect(estimator->estimator.offset(),
                               timestamp_delta_ms,
                               estimator->estimator.num_of_deltas());
  }
  Din_Go(50,2048);if (estimator->detector.State() == kBwOverusing) {
    Din_Go(47,2048);uint32_t incoming_bitrate = incoming_bitrate_.Rate(now_ms);
    Din_Go(49,2048);if (prior_state != kBwOverusing ||
        remote_rate_->TimeToReduceFurther(now_ms, incoming_bitrate)) {
      // The first overuse should immediately trigger a new estimate.
      // We also have to update the estimate immediately if we are overusing
      // and the target bitrate is too high compared to what we are receiving.
      Din_Go(48,2048);UpdateEstimate(now_ms);
    }
  }
Din_Go(51,2048);}

int32_t RemoteBitrateEstimatorImpl::Process() {
  Din_Go(52,2048);if (TimeUntilNextProcess() > 0) {
    {int32_t  ReplaceReturn9 = 0; Din_Go(53,2048); return ReplaceReturn9;}
  }
  {
    Din_Go(54,2048);CriticalSectionScoped cs(crit_sect_.get());
    UpdateEstimate(clock_->TimeInMilliseconds());
  }
  last_process_time_ = clock_->TimeInMilliseconds();
  {int32_t  ReplaceReturn8 = 0; Din_Go(55,2048); return ReplaceReturn8;}
}

int64_t RemoteBitrateEstimatorImpl::TimeUntilNextProcess() {
  Din_Go(56,2048);if (last_process_time_ < 0) {
    {int64_t  ReplaceReturn7 = 0; Din_Go(57,2048); return ReplaceReturn7;}
  }
  {
    Din_Go(58,2048);CriticalSectionScoped cs_(crit_sect_.get());
    {int64_t  ReplaceReturn6 = last_process_time_ + process_interval_ms_ -
        clock_->TimeInMilliseconds(); Din_Go(59,2048); return ReplaceReturn6;}
  }
}

void RemoteBitrateEstimatorImpl::UpdateEstimate(int64_t now_ms) {
  Din_Go(60,2048);BandwidthUsage bw_state = kBwNormal;
  double sum_var_noise = 0.0;
  SsrcOveruseEstimatorMap::iterator it = overuse_detectors_.begin();
  Din_Go(68,2048);while (it != overuse_detectors_.end()) {
    Din_Go(61,2048);const int64_t time_of_last_received_packet =
        it->second->last_packet_time_ms;
    Din_Go(67,2048);if (time_of_last_received_packet >= 0 &&
        now_ms - time_of_last_received_packet > kStreamTimeOutMs) {
      // This over-use detector hasn't received packets for |kStreamTimeOutMs|
      // milliseconds and is considered stale.
      Din_Go(62,2048);delete it->second;
      overuse_detectors_.erase(it++);
    } else {
      Din_Go(63,2048);sum_var_noise += it->second->estimator.var_noise();
      // Make sure that we trigger an over-use if any of the over-use detectors
      // is detecting over-use.
      Din_Go(65,2048);if (it->second->detector.State() > bw_state) {
        Din_Go(64,2048);bw_state = it->second->detector.State();
      }
      Din_Go(66,2048);++it;
    }
  }
  // We can't update the estimate if we don't have any active streams.
  Din_Go(71,2048);if (overuse_detectors_.empty()) {
    Din_Go(69,2048);remote_rate_.reset(RemoteRateControl::Create(
        remote_rate_->GetControlType(), remote_rate_->GetMinBitrate()));
    Din_Go(70,2048);return;
  }
  Din_Go(72,2048);double mean_noise_var = sum_var_noise /
      static_cast<double>(overuse_detectors_.size());
  const RateControlInput input(bw_state,
                               incoming_bitrate_.Rate(now_ms),
                               mean_noise_var);
  const RateControlRegion region = remote_rate_->Update(&input, now_ms);
  unsigned int target_bitrate = remote_rate_->UpdateBandwidthEstimate(now_ms);
  Din_Go(74,2048);if (remote_rate_->ValidEstimate()) {
    Din_Go(73,2048);process_interval_ms_ = remote_rate_->GetFeedbackInterval();
    std::vector<unsigned int> ssrcs;
    GetSsrcs(&ssrcs);
    observer_->OnReceiveBitrateChanged(ssrcs, target_bitrate);
  }
  Din_Go(76,2048);for (it = overuse_detectors_.begin(); it != overuse_detectors_.end(); ++it) {
    Din_Go(75,2048);it->second->detector.SetRateControlRegion(region);
  }
Din_Go(77,2048);}

void RemoteBitrateEstimatorImpl::OnRttUpdate(int64_t rtt) {
  Din_Go(78,2048);CriticalSectionScoped cs(crit_sect_.get());
  remote_rate_->SetRtt(rtt);
Din_Go(79,2048);}

void RemoteBitrateEstimatorImpl::RemoveStream(unsigned int ssrc) {
  Din_Go(80,2048);CriticalSectionScoped cs(crit_sect_.get());
  SsrcOveruseEstimatorMap::iterator it = overuse_detectors_.find(ssrc);
  Din_Go(82,2048);if (it != overuse_detectors_.end()) {
    Din_Go(81,2048);delete it->second;
    overuse_detectors_.erase(it);
  }
Din_Go(83,2048);}

bool RemoteBitrateEstimatorImpl::LatestEstimate(
    std::vector<unsigned int>* ssrcs,
    unsigned int* bitrate_bps) const {
  Din_Go(84,2048);CriticalSectionScoped cs(crit_sect_.get());
  assert(bitrate_bps);
  Din_Go(86,2048);if (!remote_rate_->ValidEstimate()) {
    {_Bool  ReplaceReturn5 = false; Din_Go(85,2048); return ReplaceReturn5;}
  }
  Din_Go(87,2048);GetSsrcs(ssrcs);
  Din_Go(90,2048);if (ssrcs->empty())
    {/*3*/Din_Go(88,2048);*bitrate_bps = 0;/*4*/}
  else
    {/*5*/Din_Go(89,2048);*bitrate_bps = remote_rate_->LatestEstimate();/*6*/}
  {_Bool  ReplaceReturn4 = true; Din_Go(91,2048); return ReplaceReturn4;}
}

bool RemoteBitrateEstimatorImpl::GetStats(
    ReceiveBandwidthEstimatorStats* output) const {
  // Not implemented.
  {_Bool  ReplaceReturn3 = false; Din_Go(92,2048); return ReplaceReturn3;}
}

void RemoteBitrateEstimatorImpl::GetSsrcs(
    std::vector<unsigned int>* ssrcs) const {
Din_Go(93,2048);  assert(ssrcs);
  ssrcs->resize(overuse_detectors_.size());
  int i = 0;
  Din_Go(95,2048);for (SsrcOveruseEstimatorMap::const_iterator it = overuse_detectors_.begin();
      it != overuse_detectors_.end(); ++it, ++i) {
    Din_Go(94,2048);(*ssrcs)[i] = it->first;
  }
Din_Go(96,2048);}

RemoteBitrateEstimator* RemoteBitrateEstimatorFactory::Create(
    webrtc::RemoteBitrateObserver* observer,
    webrtc::Clock* clock,
    RateControlType control_type,
    uint32_t min_bitrate_bps) const {
  LOG(LS_INFO) << "RemoteBitrateEstimatorFactory: Instantiating.";
  {webrtc::RemoteBitrateEstimator * ReplaceReturn2 = new RemoteBitrateEstimatorImpl(observer, clock, control_type,
                                        min_bitrate_bps); Din_Go(97,2048); return ReplaceReturn2;}
}
}  // namespace webrtc
