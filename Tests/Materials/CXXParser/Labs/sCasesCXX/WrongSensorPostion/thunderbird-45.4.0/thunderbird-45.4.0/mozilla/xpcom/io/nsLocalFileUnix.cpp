/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim: set ts=8 sts=2 et sw=2 tw=80: */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * Implementation of nsIFile for "unixy" systems.
 */

#include "mozilla/ArrayUtils.h"
#include "var/tmp/sensor.h"
#include "mozilla/Attributes.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <utime.h>
#include <dirent.h>
#include <ctype.h>
#include <locale.h>
#if defined(VMS)
#include <fabdef.h>
#endif

#if defined(HAVE_SYS_QUOTA_H) && defined(HAVE_LINUX_QUOTA_H)
#define USE_LINUX_QUOTACTL
#include <sys/mount.h>
#include <sys/quota.h>
#ifndef BLOCK_SIZE
#define BLOCK_SIZE 1024 /* kernel block size */
#endif
#endif

#include "xpcom-private.h"
#include "nsDirectoryServiceDefs.h"
#include "nsCRT.h"
#include "nsCOMPtr.h"
#include "nsMemory.h"
#include "nsIFile.h"
#include "nsString.h"
#include "nsReadableUtils.h"
#include "nsLocalFile.h"
#include "nsIComponentManager.h"
#include "nsXPIDLString.h"
#include "prproces.h"
#include "nsIDirectoryEnumerator.h"
#include "nsISimpleEnumerator.h"
#include "private/pprio.h"
#include "prlink.h"

#ifdef MOZ_WIDGET_GTK
#include "nsIGIOService.h"
#endif

#ifdef MOZ_WIDGET_COCOA
#include <Carbon/Carbon.h>
#include "CocoaFileUtils.h"
#include "prmem.h"
#include "plbase64.h"

static nsresult MacErrorMapper(OSErr inErr);
#endif

#ifdef MOZ_WIDGET_ANDROID
#include "AndroidBridge.h"
#include "nsIMIMEService.h"
#include <linux/magic.h>
#endif

#ifdef MOZ_ENABLE_CONTENTACTION
#include <contentaction/contentaction.h>
#endif

#include "nsNativeCharsetUtils.h"
#include "nsTraceRefcnt.h"
#include "nsHashKeys.h"

using namespace mozilla;

#define ENSURE_STAT_CACHE()                     \
    PR_BEGIN_MACRO                              \
        if (!FillStatCache())                   \
             return NSRESULT_FOR_ERRNO();       \
    PR_END_MACRO

#define CHECK_mPath()                           \
    PR_BEGIN_MACRO                              \
        if (mPath.IsEmpty())                    \
            return NS_ERROR_NOT_INITIALIZED;    \
    PR_END_MACRO

/* directory enumerator */
class nsDirEnumeratorUnix final
  : public nsISimpleEnumerator
  , public nsIDirectoryEnumerator
{
public:
  nsDirEnumeratorUnix();

  // nsISupports interface
  NS_DECL_ISUPPORTS

  // nsISimpleEnumerator interface
  NS_DECL_NSISIMPLEENUMERATOR

  // nsIDirectoryEnumerator interface
  NS_DECL_NSIDIRECTORYENUMERATOR

  NS_IMETHOD Init(nsLocalFile* aParent, bool aIgnored);

private:
  ~nsDirEnumeratorUnix();

protected:
  NS_IMETHOD GetNextEntry();

  DIR*           mDir;
  struct dirent* mEntry;
  nsCString      mParentPath;
};

nsDirEnumeratorUnix::nsDirEnumeratorUnix() :
  mDir(nullptr),
  mEntry(nullptr)
{
Din_Go(286,2048);Din_Go(287,2048);}

nsDirEnumeratorUnix::~nsDirEnumeratorUnix()
{
  Din_Go(288,2048);Close();
Din_Go(289,2048);}

NS_IMPL_ISUPPORTS(nsDirEnumeratorUnix, nsISimpleEnumerator,
                  nsIDirectoryEnumerator)

NS_IMETHODIMP
nsDirEnumeratorUnix::Init(nsLocalFile* aParent,
                          bool aResolveSymlinks /*ignored*/)
{
  Din_Go(290,2048);nsAutoCString dirPath;
  Din_Go(292,2048);if (NS_FAILED(aParent->GetNativePath(dirPath)) ||
      dirPath.IsEmpty()) {
    {__IAENUM__ nsresult  ReplaceReturn198 = NS_ERROR_FILE_INVALID_PATH; Din_Go(291,2048); return ReplaceReturn198;}
  }

  Din_Go(294,2048);if (NS_FAILED(aParent->GetNativePath(mParentPath))) {
    {__IAENUM__ nsresult  ReplaceReturn197 = NS_ERROR_FAILURE; Din_Go(293,2048); return ReplaceReturn197;}
  }

  Din_Go(295,2048);mDir = opendir(dirPath.get());
  Din_Go(297,2048);if (!mDir) {
    {__IAENUM__ nsresult  ReplaceReturn196 = NSRESULT_FOR_ERRNO(); Din_Go(296,2048); return ReplaceReturn196;}
  }
  {__IAENUM__ nsresult  ReplaceReturn195 = GetNextEntry(); Din_Go(298,2048); return ReplaceReturn195;}
}

NS_IMETHODIMP
nsDirEnumeratorUnix::HasMoreElements(bool* aResult)
{
  Din_Go(299,2048);*aResult = mDir && mEntry;
  Din_Go(301,2048);if (!*aResult) {
    Din_Go(300,2048);Close();
  }
  {__IAENUM__ nsresult  ReplaceReturn194 = NS_OK; Din_Go(302,2048); return ReplaceReturn194;}
}

NS_IMETHODIMP
nsDirEnumeratorUnix::GetNext(nsISupports** aResult)
{
  Din_Go(303,2048);nsCOMPtr<nsIFile> file;
  nsresult rv = GetNextFile(getter_AddRefs(file));
  Din_Go(305,2048);if (NS_FAILED(rv)) {
    {__IAENUM__ nsresult  ReplaceReturn193 = rv; Din_Go(304,2048); return ReplaceReturn193;}
  }
  NS_IF_ADDREF(*aResult = file);
  {__IAENUM__ nsresult  ReplaceReturn192 = NS_OK; Din_Go(306,2048); return ReplaceReturn192;}
}

NS_IMETHODIMP
nsDirEnumeratorUnix::GetNextEntry()
{
  Din_Go(307,2048);do {
    errno = 0;
    Din_Go(308,2048);mEntry = readdir(mDir);

    // end of dir or error
    Din_Go(310,2048);if (!mEntry) {
      {__IAENUM__ nsresult  ReplaceReturn191 = NSRESULT_FOR_ERRNO(); Din_Go(309,2048); return ReplaceReturn191;}
    }

    // keep going past "." and ".."
  } while (mEntry->d_name[0] == '.'     &&
           (mEntry->d_name[1] == '\0'    ||   // .\0
            (mEntry->d_name[1] == '.'     &&
             mEntry->d_name[2] == '\0')));      // ..\0
  {__IAENUM__ nsresult  ReplaceReturn190 = NS_OK; Din_Go(311,2048); return ReplaceReturn190;}
}

NS_IMETHODIMP
nsDirEnumeratorUnix::GetNextFile(nsIFile** aResult)
{
  Din_Go(312,2048);nsresult rv;
  Din_Go(315,2048);if (!mDir || !mEntry) {
    Din_Go(313,2048);*aResult = nullptr;
    {__IAENUM__ nsresult  ReplaceReturn189 = NS_OK; Din_Go(314,2048); return ReplaceReturn189;}
  }

  Din_Go(316,2048);nsCOMPtr<nsIFile> file = new nsLocalFile();

  Din_Go(318,2048);if (NS_FAILED(rv = file->InitWithNativePath(mParentPath)) ||
      NS_FAILED(rv = file->AppendNative(nsDependentCString(mEntry->d_name)))) {
    {__IAENUM__ nsresult  ReplaceReturn188 = rv; Din_Go(317,2048); return ReplaceReturn188;}
  }

  Din_Go(319,2048);file.forget(aResult);
  {__IAENUM__ nsresult  ReplaceReturn187 = GetNextEntry(); Din_Go(320,2048); return ReplaceReturn187;}
}

NS_IMETHODIMP
nsDirEnumeratorUnix::Close()
{
  Din_Go(321,2048);if (mDir) {
    Din_Go(322,2048);closedir(mDir);
    mDir = nullptr;
  }
  {__IAENUM__ nsresult  ReplaceReturn186 = NS_OK; Din_Go(323,2048); return ReplaceReturn186;}
}

nsLocalFile::nsLocalFile()
{
Din_Go(324,2048);Din_Go(325,2048);}

nsLocalFile::nsLocalFile(const nsLocalFile& aOther)
  : mPath(aOther.mPath)
{
Din_Go(326,2048);Din_Go(327,2048);}

#ifdef MOZ_WIDGET_COCOA
NS_IMPL_ISUPPORTS(nsLocalFile,
                  nsILocalFileMac,
                  nsILocalFile,
                  nsIFile,
                  nsIHashable)
#else
NS_IMPL_ISUPPORTS(nsLocalFile,
                  nsILocalFile,
                  nsIFile,
                  nsIHashable)
#endif

nsresult
nsLocalFile::nsLocalFileConstructor(nsISupports* aOuter,
                                    const nsIID& aIID,
                                    void** aInstancePtr)
{
  Din_Go(328,2048);if (NS_WARN_IF(!aInstancePtr)) {
    {__IAENUM__ nsresult  ReplaceReturn185 = NS_ERROR_INVALID_ARG; Din_Go(329,2048); return ReplaceReturn185;}
  }
  Din_Go(331,2048);if (NS_WARN_IF(aOuter)) {
    {__IAENUM__ nsresult  ReplaceReturn184 = NS_ERROR_NO_AGGREGATION; Din_Go(330,2048); return ReplaceReturn184;}
  }

  Din_Go(332,2048);*aInstancePtr = nullptr;

  nsCOMPtr<nsIFile> inst = new nsLocalFile();
  {__IAENUM__ nsresult  ReplaceReturn183 = inst->QueryInterface(aIID, aInstancePtr); Din_Go(333,2048); return ReplaceReturn183;}
}

bool
nsLocalFile::FillStatCache()
{
  Din_Go(334,2048);if (STAT(mPath.get(), &mCachedStat) == -1) {
    // try lstat it may be a symlink
    Din_Go(335,2048);if (LSTAT(mPath.get(), &mCachedStat) == -1) {
      {_Bool  ReplaceReturn182 = false; Din_Go(336,2048); return ReplaceReturn182;}
    }
  }
  {_Bool  ReplaceReturn181 = true; Din_Go(337,2048); return ReplaceReturn181;}
}

NS_IMETHODIMP
nsLocalFile::Clone(nsIFile** aFile)
{
  // Just copy-construct ourselves
  Din_Go(338,2048);RefPtr<nsLocalFile> copy = new nsLocalFile(*this);
  copy.forget(aFile);
  {__IAENUM__ nsresult  ReplaceReturn180 = NS_OK; Din_Go(339,2048); return ReplaceReturn180;}
}

NS_IMETHODIMP
nsLocalFile::InitWithNativePath(const nsACString& aFilePath)
{
  Din_Go(340,2048);if (aFilePath.EqualsLiteral("~") ||
      Substring(aFilePath, 0, 2).EqualsLiteral("~/")) {
    Din_Go(341,2048);nsCOMPtr<nsIFile> homeDir;
    nsAutoCString homePath;
    Din_Go(343,2048);if (NS_FAILED(NS_GetSpecialDirectory(NS_OS_HOME_DIR,
                                         getter_AddRefs(homeDir))) ||
        NS_FAILED(homeDir->GetNativePath(homePath))) {
      {__IAENUM__ nsresult  ReplaceReturn179 = NS_ERROR_FAILURE; Din_Go(342,2048); return ReplaceReturn179;}
    }

    Din_Go(344,2048);mPath = homePath;
    Din_Go(346,2048);if (aFilePath.Length() > 2) {
      Din_Go(345,2048);mPath.Append(Substring(aFilePath, 1, aFilePath.Length() - 1));
    }
  } else {
    Din_Go(347,2048);if (aFilePath.IsEmpty() || aFilePath.First() != '/') {
      {__IAENUM__ nsresult  ReplaceReturn178 = NS_ERROR_FILE_UNRECOGNIZED_PATH; Din_Go(348,2048); return ReplaceReturn178;}
    }
    Din_Go(349,2048);mPath = aFilePath;
  }

  // trim off trailing slashes
  Din_Go(350,2048);ssize_t len = mPath.Length();
  Din_Go(352,2048);while ((len > 1) && (mPath[len - 1] == '/')) {
    Din_Go(351,2048);--len;
  }
  Din_Go(353,2048);mPath.SetLength(len);

  {__IAENUM__ nsresult  ReplaceReturn177 = NS_OK; Din_Go(354,2048); return ReplaceReturn177;}
}

NS_IMETHODIMP
nsLocalFile::CreateAllAncestors(uint32_t aPermissions)
{
  // <jband> I promise to play nice
  Din_Go(355,2048);char* buffer = mPath.BeginWriting();
  char* slashp = buffer;

#ifdef DEBUG_NSIFILE
  fprintf(stderr, "nsIFile: before: %s\n", buffer);
#endif

  Din_Go(367,2048);while ((slashp = strchr(slashp + 1, '/'))) {
    /*
     * Sequences of '/' are equivalent to a single '/'.
     */
    Din_Go(356,2048);if (slashp[1] == '/') {
      Din_Go(357,2048);continue;
    }

    /*
     * If the path has a trailing slash, don't make the last component,
     * because we'll get EEXIST in Create when we try to build the final
     * component again, and it's easier to condition the logic here than
     * there.
     */
    Din_Go(359,2048);if (slashp[1] == '\0') {
      Din_Go(358,2048);break;
    }

    /* Temporarily NUL-terminate here */
    Din_Go(360,2048);*slashp = '\0';
#ifdef DEBUG_NSIFILE
    fprintf(stderr, "nsIFile: mkdir(\"%s\")\n", buffer);
#endif
    int mkdir_result = mkdir(buffer, aPermissions);
    int mkdir_errno  = errno;
    Din_Go(363,2048);if (mkdir_result == -1) {
      /*
       * Always set |errno| to EEXIST if the dir already exists
       * (we have to do this here since the errno value is not consistent
       * in all cases - various reasons like different platform,
       * automounter-controlled dir, etc. can affect it (see bug 125489
       * for details)).
       */
      Din_Go(361,2048);if (access(buffer, F_OK) == 0) {
        Din_Go(362,2048);mkdir_errno = EEXIST;
      }
    }

    /* Put the / back before we (maybe) return */
    Din_Go(364,2048);*slashp = '/';

    /*
     * We could get EEXIST for an existing file -- not directory --
     * with the name of one of our ancestors, but that's OK: we'll get
     * ENOTDIR when we try to make the next component in the path,
     * either here on back in Create, and error out appropriately.
     */
    Din_Go(366,2048);if (mkdir_result == -1 && mkdir_errno != EEXIST) {
      {__IAENUM__ nsresult  ReplaceReturn176 = nsresultForErrno(mkdir_errno); Din_Go(365,2048); return ReplaceReturn176;}
    }
  }

#ifdef DEBUG_NSIFILE
  fprintf(stderr, "nsIFile: after: %s\n", buffer);
#endif

  {__IAENUM__ nsresult  ReplaceReturn175 = NS_OK; Din_Go(368,2048); return ReplaceReturn175;}
}

NS_IMETHODIMP
nsLocalFile::OpenNSPRFileDesc(int32_t aFlags, int32_t aMode,
                              PRFileDesc** aResult)
{
  Din_Go(369,2048);*aResult = PR_Open(mPath.get(), aFlags, aMode);
  Din_Go(371,2048);if (!*aResult) {
    {__IAENUM__ nsresult  ReplaceReturn174 = NS_ErrorAccordingToNSPR(); Din_Go(370,2048); return ReplaceReturn174;}
  }

  Din_Go(373,2048);if (aFlags & DELETE_ON_CLOSE) {
    Din_Go(372,2048);PR_Delete(mPath.get());
  }

#if defined(HAVE_POSIX_FADVISE)
  Din_Go(375,2048);if (aFlags & OS_READAHEAD) {
    Din_Go(374,2048);posix_fadvise(PR_FileDesc2NativeHandle(*aResult), 0, 0,
                  POSIX_FADV_SEQUENTIAL);
  }
#endif
  {__IAENUM__ nsresult  ReplaceReturn173 = NS_OK; Din_Go(376,2048); return ReplaceReturn173;}
}

NS_IMETHODIMP
nsLocalFile::OpenANSIFileDesc(const char* aMode, FILE** aResult)
{
  Din_Go(377,2048);*aResult = fopen(mPath.get(), aMode);
  Din_Go(379,2048);if (!*aResult) {
    {__IAENUM__ nsresult  ReplaceReturn172 = NS_ERROR_FAILURE; Din_Go(378,2048); return ReplaceReturn172;}
  }

  {__IAENUM__ nsresult  ReplaceReturn171 = NS_OK; Din_Go(380,2048); return ReplaceReturn171;}
}

static int
do_create(const char* aPath, int aFlags, mode_t aMode, PRFileDesc** aResult)
{
  Din_Go(381,2048);*aResult = PR_Open(aPath, aFlags, aMode);
  {int  ReplaceReturn170 = *aResult ? 0 : -1; Din_Go(382,2048); return ReplaceReturn170;}
}

static int
do_mkdir(const char* aPath, int aFlags, mode_t aMode, PRFileDesc** aResult)
{
  Din_Go(383,2048);*aResult = nullptr;
  {int  ReplaceReturn169 = mkdir(aPath, aMode); Din_Go(384,2048); return ReplaceReturn169;}
}

nsresult
nsLocalFile::CreateAndKeepOpen(uint32_t aType, int aFlags,
                               uint32_t aPermissions, PRFileDesc** aResult)
{
  Din_Go(385,2048);if (aType != NORMAL_FILE_TYPE && aType != DIRECTORY_TYPE) {
    {__IAENUM__ nsresult  ReplaceReturn168 = NS_ERROR_FILE_UNKNOWN_TYPE; Din_Go(386,2048); return ReplaceReturn168;}
  }

  Din_Go(387,2048);int (*createFunc)(const char*, int, mode_t, PRFileDesc**) =
    (aType == NORMAL_FILE_TYPE) ? do_create : do_mkdir;

  int result = createFunc(mPath.get(), aFlags, aPermissions, aResult);
  Din_Go(398,2048);if (result == -1 && errno == ENOENT) {
    /*
     * If we failed because of missing ancestor components, try to create
     * them and then retry the original creation.
     *
     * Ancestor directories get the same permissions as the file we're
     * creating, with the X bit set for each of (user,group,other) with
     * an R bit in the original permissions.    If you want to do anything
     * fancy like setgid or sticky bits, do it by hand.
     */
    Din_Go(388,2048);int dirperm = aPermissions;
    Din_Go(390,2048);if (aPermissions & S_IRUSR) {
      Din_Go(389,2048);dirperm |= S_IXUSR;
    }
    Din_Go(392,2048);if (aPermissions & S_IRGRP) {
      Din_Go(391,2048);dirperm |= S_IXGRP;
    }
    Din_Go(394,2048);if (aPermissions & S_IROTH) {
      Din_Go(393,2048);dirperm |= S_IXOTH;
    }

#ifdef DEBUG_NSIFILE
    fprintf(stderr, "nsIFile: perm = %o, dirperm = %o\n", aPermissions,
            dirperm);
#endif

    Din_Go(396,2048);if (NS_FAILED(CreateAllAncestors(dirperm))) {
      {__IAENUM__ nsresult  ReplaceReturn167 = NS_ERROR_FAILURE; Din_Go(395,2048); return ReplaceReturn167;}
    }

#ifdef DEBUG_NSIFILE
    fprintf(stderr, "nsIFile: Create(\"%s\") again\n", mPath.get());
#endif
    Din_Go(397,2048);result = createFunc(mPath.get(), aFlags, aPermissions, aResult);
  }
  {__IAENUM__ nsresult  ReplaceReturn166 = NSRESULT_FOR_RETURN(result); Din_Go(399,2048); return ReplaceReturn166;}
}

NS_IMETHODIMP
nsLocalFile::Create(uint32_t aType, uint32_t aPermissions)
{
  Din_Go(400,2048);PRFileDesc* junk = nullptr;
  nsresult rv = CreateAndKeepOpen(aType,
                                  PR_WRONLY | PR_CREATE_FILE | PR_TRUNCATE |
                                  PR_EXCL,
                                  aPermissions,
                                  &junk);
  Din_Go(402,2048);if (junk) {
    Din_Go(401,2048);PR_Close(junk);
  }
  {__IAENUM__ nsresult  ReplaceReturn165 = rv; Din_Go(403,2048); return ReplaceReturn165;}
}

NS_IMETHODIMP
nsLocalFile::AppendNative(const nsACString& aFragment)
{
  Din_Go(404,2048);if (aFragment.IsEmpty()) {
    {__IAENUM__ nsresult  ReplaceReturn164 = NS_OK; Din_Go(405,2048); return ReplaceReturn164;}
  }

  // only one component of path can be appended
  nsACString::const_iterator begin, end;
  Din_Go(407,2048);if (FindCharInReadable('/', aFragment.BeginReading(begin),
                         aFragment.EndReading(end))) {
    {__IAENUM__ nsresult  ReplaceReturn163 = NS_ERROR_FILE_UNRECOGNIZED_PATH; Din_Go(406,2048); return ReplaceReturn163;}
  }

  {__IAENUM__ nsresult  ReplaceReturn162 = AppendRelativeNativePath(aFragment); Din_Go(408,2048); return ReplaceReturn162;}
}

NS_IMETHODIMP
nsLocalFile::AppendRelativeNativePath(const nsACString& aFragment)
{
  Din_Go(409,2048);if (aFragment.IsEmpty()) {
    {__IAENUM__ nsresult  ReplaceReturn161 = NS_OK; Din_Go(410,2048); return ReplaceReturn161;}
  }

  // No leading '/'
  Din_Go(412,2048);if (aFragment.First() == '/') {
    {__IAENUM__ nsresult  ReplaceReturn160 = NS_ERROR_FILE_UNRECOGNIZED_PATH; Din_Go(411,2048); return ReplaceReturn160;}
  }

  Din_Go(414,2048);if (!mPath.EqualsLiteral("/")) {
    Din_Go(413,2048);mPath.Append('/');
  }
  Din_Go(415,2048);mPath.Append(aFragment);

  {__IAENUM__ nsresult  ReplaceReturn159 = NS_OK; Din_Go(416,2048); return ReplaceReturn159;}
}

NS_IMETHODIMP
nsLocalFile::Normalize()
{
  Din_Go(417,2048);char resolved_path[PATH_MAX] = "";
  char* resolved_path_ptr = nullptr;

  resolved_path_ptr = realpath(mPath.get(), resolved_path);

  // if there is an error, the return is null.
  Din_Go(419,2048);if (!resolved_path_ptr) {
    {__IAENUM__ nsresult  ReplaceReturn158 = NSRESULT_FOR_ERRNO(); Din_Go(418,2048); return ReplaceReturn158;}
  }

  Din_Go(420,2048);mPath = resolved_path;
  {__IAENUM__ nsresult  ReplaceReturn157 = NS_OK; Din_Go(421,2048); return ReplaceReturn157;}
}

void
nsLocalFile::LocateNativeLeafName(nsACString::const_iterator& aBegin,
                                  nsACString::const_iterator& aEnd)
{
  // XXX perhaps we should cache this??

  Din_Go(422,2048);mPath.BeginReading(aBegin);
  mPath.EndReading(aEnd);

  nsACString::const_iterator it = aEnd;
  nsACString::const_iterator stop = aBegin;
  --stop;
  Din_Go(426,2048);while (--it != stop) {
    Din_Go(423,2048);if (*it == '/') {
      Din_Go(424,2048);aBegin = ++it;
      Din_Go(425,2048);return;
    }
  }
  // else, the entire path is the leaf name (which means this
  // isn't an absolute path... unexpected??)
Din_Go(427,2048);}

NS_IMETHODIMP
nsLocalFile::GetNativeLeafName(nsACString& aLeafName)
{
Din_Go(428,2048);  nsACString::const_iterator begin, end;
  LocateNativeLeafName(begin, end);
  aLeafName = Substring(begin, end);
  {__IAENUM__ nsresult  ReplaceReturn156 = NS_OK; Din_Go(429,2048); return ReplaceReturn156;}
}

NS_IMETHODIMP
nsLocalFile::SetNativeLeafName(const nsACString& aLeafName)
{
Din_Go(430,2048);  nsACString::const_iterator begin, end;
  LocateNativeLeafName(begin, end);
  mPath.Replace(begin.get() - mPath.get(), Distance(begin, end), aLeafName);
  {__IAENUM__ nsresult  ReplaceReturn155 = NS_OK; Din_Go(431,2048); return ReplaceReturn155;}
}

NS_IMETHODIMP
nsLocalFile::GetNativePath(nsACString& aResult)
{
  Din_Go(432,2048);aResult = mPath;
  {__IAENUM__ nsresult  ReplaceReturn154 = NS_OK; Din_Go(433,2048); return ReplaceReturn154;}
}

nsresult
nsLocalFile::GetNativeTargetPathName(nsIFile* aNewParent,
                                     const nsACString& aNewName,
                                     nsACString& aResult)
{
  Din_Go(434,2048);nsresult rv;
  nsCOMPtr<nsIFile> oldParent;

  Din_Go(450,2048);if (!aNewParent) {
    Din_Go(435,2048);if (NS_FAILED(rv = GetParent(getter_AddRefs(oldParent)))) {
      {__IAENUM__ nsresult  ReplaceReturn153 = rv; Din_Go(436,2048); return ReplaceReturn153;}
    }
    Din_Go(437,2048);aNewParent = oldParent.get();
  } else {
    // check to see if our target directory exists
    Din_Go(438,2048);bool targetExists;
    Din_Go(440,2048);if (NS_FAILED(rv = aNewParent->Exists(&targetExists))) {
      {__IAENUM__ nsresult  ReplaceReturn152 = rv; Din_Go(439,2048); return ReplaceReturn152;}
    }

    Din_Go(449,2048);if (!targetExists) {
      // XXX create the new directory with some permissions
      Din_Go(441,2048);rv = aNewParent->Create(DIRECTORY_TYPE, 0755);
      Din_Go(443,2048);if (NS_FAILED(rv)) {
        {__IAENUM__ nsresult  ReplaceReturn151 = rv; Din_Go(442,2048); return ReplaceReturn151;}
      }
    } else {
      // make sure that the target is actually a directory
      Din_Go(444,2048);bool targetIsDirectory;
      Din_Go(446,2048);if (NS_FAILED(rv = aNewParent->IsDirectory(&targetIsDirectory))) {
        {__IAENUM__ nsresult  ReplaceReturn150 = rv; Din_Go(445,2048); return ReplaceReturn150;}
      }
      Din_Go(448,2048);if (!targetIsDirectory) {
        {__IAENUM__ nsresult  ReplaceReturn149 = NS_ERROR_FILE_DESTINATION_NOT_DIR; Din_Go(447,2048); return ReplaceReturn149;}
      }
    }
  }

  nsACString::const_iterator nameBegin, nameEnd;
  Din_Go(453,2048);if (!aNewName.IsEmpty()) {
    Din_Go(451,2048);aNewName.BeginReading(nameBegin);
    aNewName.EndReading(nameEnd);
  } else {
    Din_Go(452,2048);LocateNativeLeafName(nameBegin, nameEnd);
  }

  Din_Go(454,2048);nsAutoCString dirName;
  Din_Go(456,2048);if (NS_FAILED(rv = aNewParent->GetNativePath(dirName))) {
    {__IAENUM__ nsresult  ReplaceReturn148 = rv; Din_Go(455,2048); return ReplaceReturn148;}
  }

  Din_Go(457,2048);aResult = dirName + NS_LITERAL_CSTRING("/") + Substring(nameBegin, nameEnd);
  {__IAENUM__ nsresult  ReplaceReturn147 = NS_OK; Din_Go(458,2048); return ReplaceReturn147;}
}

nsresult
nsLocalFile::CopyDirectoryTo(nsIFile* aNewParent)
{
  Din_Go(459,2048);nsresult rv;
  /*
   * dirCheck is used for various boolean test results such as from Equals,
   * Exists, isDir, etc.
   */
  bool dirCheck, isSymlink;
  uint32_t oldPerms;

  Din_Go(461,2048);if (NS_FAILED(rv = IsDirectory(&dirCheck))) {
    {__IAENUM__ nsresult  ReplaceReturn146 = rv; Din_Go(460,2048); return ReplaceReturn146;}
  }
  Din_Go(463,2048);if (!dirCheck) {
    {__IAENUM__ nsresult  ReplaceReturn145 = CopyToNative(aNewParent, EmptyCString()); Din_Go(462,2048); return ReplaceReturn145;}
  }

  Din_Go(465,2048);if (NS_FAILED(rv = Equals(aNewParent, &dirCheck))) {
    {__IAENUM__ nsresult  ReplaceReturn144 = rv; Din_Go(464,2048); return ReplaceReturn144;}
  }
  Din_Go(467,2048);if (dirCheck) {
    // can't copy dir to itself
    {__IAENUM__ nsresult  ReplaceReturn143 = NS_ERROR_INVALID_ARG; Din_Go(466,2048); return ReplaceReturn143;}
  }

  Din_Go(469,2048);if (NS_FAILED(rv = aNewParent->Exists(&dirCheck))) {
    {__IAENUM__ nsresult  ReplaceReturn142 = rv; Din_Go(468,2048); return ReplaceReturn142;}
  }
  // get the dirs old permissions
  Din_Go(471,2048);if (NS_FAILED(rv = GetPermissions(&oldPerms))) {
    {__IAENUM__ nsresult  ReplaceReturn141 = rv; Din_Go(470,2048); return ReplaceReturn141;}
  }
  Din_Go(485,2048);if (!dirCheck) {
    Din_Go(472,2048);if (NS_FAILED(rv = aNewParent->Create(DIRECTORY_TYPE, oldPerms))) {
      {__IAENUM__ nsresult  ReplaceReturn140 = rv; Din_Go(473,2048); return ReplaceReturn140;}
    }
  } else {    // dir exists lets try to use leaf
    Din_Go(474,2048);nsAutoCString leafName;
    Din_Go(476,2048);if (NS_FAILED(rv = GetNativeLeafName(leafName))) {
      {__IAENUM__ nsresult  ReplaceReturn139 = rv; Din_Go(475,2048); return ReplaceReturn139;}
    }
    Din_Go(478,2048);if (NS_FAILED(rv = aNewParent->AppendNative(leafName))) {
      {__IAENUM__ nsresult  ReplaceReturn138 = rv; Din_Go(477,2048); return ReplaceReturn138;}
    }
    Din_Go(480,2048);if (NS_FAILED(rv = aNewParent->Exists(&dirCheck))) {
      {__IAENUM__ nsresult  ReplaceReturn137 = rv; Din_Go(479,2048); return ReplaceReturn137;}
    }
    Din_Go(482,2048);if (dirCheck) {
      {__IAENUM__ nsresult  ReplaceReturn136 = NS_ERROR_FILE_ALREADY_EXISTS; Din_Go(481,2048); return ReplaceReturn136;}  // dest exists
    }
    Din_Go(484,2048);if (NS_FAILED(rv = aNewParent->Create(DIRECTORY_TYPE, oldPerms))) {
      {__IAENUM__ nsresult  ReplaceReturn135 = rv; Din_Go(483,2048); return ReplaceReturn135;}
    }
  }

  Din_Go(486,2048);nsCOMPtr<nsISimpleEnumerator> dirIterator;
  Din_Go(488,2048);if (NS_FAILED(rv = GetDirectoryEntries(getter_AddRefs(dirIterator)))) {
    {__IAENUM__ nsresult  ReplaceReturn134 = rv; Din_Go(487,2048); return ReplaceReturn134;}
  }

  Din_Go(489,2048);bool hasMore = false;
  Din_Go(516,2048);while (dirIterator->HasMoreElements(&hasMore), hasMore) {
    Din_Go(490,2048);nsCOMPtr<nsISupports> supports;
    nsCOMPtr<nsIFile> entry;
    rv = dirIterator->GetNext(getter_AddRefs(supports));
    entry = do_QueryInterface(supports);
    Din_Go(492,2048);if (NS_FAILED(rv) || !entry) {
      Din_Go(491,2048);continue;
    }
    Din_Go(494,2048);if (NS_FAILED(rv = entry->IsSymlink(&isSymlink))) {
      {__IAENUM__ nsresult  ReplaceReturn133 = rv; Din_Go(493,2048); return ReplaceReturn133;}
    }
    Din_Go(496,2048);if (NS_FAILED(rv = entry->IsDirectory(&dirCheck))) {
      {__IAENUM__ nsresult  ReplaceReturn132 = rv; Din_Go(495,2048); return ReplaceReturn132;}
    }
    Din_Go(515,2048);if (dirCheck && !isSymlink) {
      Din_Go(497,2048);nsCOMPtr<nsIFile> destClone;
      rv = aNewParent->Clone(getter_AddRefs(destClone));
      Din_Go(506,2048);if (NS_SUCCEEDED(rv)) {
        Din_Go(498,2048);if (NS_FAILED(rv = entry->CopyToNative(destClone, EmptyCString()))) {
#ifdef DEBUG
          Din_Go(499,2048);nsresult rv2;
          nsAutoCString pathName;
          Din_Go(501,2048);if (NS_FAILED(rv2 = entry->GetNativePath(pathName))) {
            {__IAENUM__ nsresult  ReplaceReturn131 = rv2; Din_Go(500,2048); return ReplaceReturn131;}
          }
          Din_Go(502,2048);printf("Operation not supported: %s\n", pathName.get());
#endif
          Din_Go(504,2048);if (rv == NS_ERROR_OUT_OF_MEMORY) {
            {__IAENUM__ nsresult  ReplaceReturn130 = rv; Din_Go(503,2048); return ReplaceReturn130;}
          }
          Din_Go(505,2048);continue;
        }
      }
    } else {
      Din_Go(507,2048);if (NS_FAILED(rv = entry->CopyToNative(aNewParent, EmptyCString()))) {
#ifdef DEBUG
        Din_Go(508,2048);nsresult rv2;
        nsAutoCString pathName;
        Din_Go(510,2048);if (NS_FAILED(rv2 = entry->GetNativePath(pathName))) {
          {__IAENUM__ nsresult  ReplaceReturn129 = rv2; Din_Go(509,2048); return ReplaceReturn129;}
        }
        Din_Go(511,2048);printf("Operation not supported: %s\n", pathName.get());
#endif
        Din_Go(513,2048);if (rv == NS_ERROR_OUT_OF_MEMORY) {
          {__IAENUM__ nsresult  ReplaceReturn128 = rv; Din_Go(512,2048); return ReplaceReturn128;}
        }
        Din_Go(514,2048);continue;
      }
    }
  }
  {__IAENUM__ nsresult  ReplaceReturn127 = NS_OK; Din_Go(517,2048); return ReplaceReturn127;}
}

NS_IMETHODIMP
nsLocalFile::CopyToNative(nsIFile* aNewParent, const nsACString& aNewName)
{
  Din_Go(518,2048);nsresult rv;
  // check to make sure that this has been initialized properly
  CHECK_mPath();

  // we copy the parent here so 'aNewParent' remains immutable
  nsCOMPtr <nsIFile> workParent;
  Din_Go(523,2048);if (aNewParent) {
    Din_Go(519,2048);if (NS_FAILED(rv = aNewParent->Clone(getter_AddRefs(workParent)))) {
      {__IAENUM__ nsresult  ReplaceReturn126 = rv; Din_Go(520,2048); return ReplaceReturn126;}
    }
  } else {
    Din_Go(521,2048);if (NS_FAILED(rv = GetParent(getter_AddRefs(workParent)))) {
      {__IAENUM__ nsresult  ReplaceReturn125 = rv; Din_Go(522,2048); return ReplaceReturn125;}
    }
  }

  // check to see if we are a directory or if we are a file
  Din_Go(524,2048);bool isDirectory;
  Din_Go(526,2048);if (NS_FAILED(rv = IsDirectory(&isDirectory))) {
    {__IAENUM__ nsresult  ReplaceReturn124 = rv; Din_Go(525,2048); return ReplaceReturn124;}
  }

  Din_Go(527,2048);nsAutoCString newPathName;
  Din_Go(578,2048);if (isDirectory) {
    Din_Go(528,2048);if (!aNewName.IsEmpty()) {
      Din_Go(529,2048);if (NS_FAILED(rv = workParent->AppendNative(aNewName))) {
        {__IAENUM__ nsresult  ReplaceReturn123 = rv; Din_Go(530,2048); return ReplaceReturn123;}
      }
    } else {
      Din_Go(531,2048);if (NS_FAILED(rv = GetNativeLeafName(newPathName))) {
        {__IAENUM__ nsresult  ReplaceReturn122 = rv; Din_Go(532,2048); return ReplaceReturn122;}
      }
      Din_Go(534,2048);if (NS_FAILED(rv = workParent->AppendNative(newPathName))) {
        {__IAENUM__ nsresult  ReplaceReturn121 = rv; Din_Go(533,2048); return ReplaceReturn121;}
      }
    }
    Din_Go(536,2048);if (NS_FAILED(rv = CopyDirectoryTo(workParent))) {
      {__IAENUM__ nsresult  ReplaceReturn120 = rv; Din_Go(535,2048); return ReplaceReturn120;}
    }
  } else {
    Din_Go(537,2048);rv = GetNativeTargetPathName(workParent, aNewName, newPathName);
    Din_Go(539,2048);if (NS_FAILED(rv)) {
      {__IAENUM__ nsresult  ReplaceReturn119 = rv; Din_Go(538,2048); return ReplaceReturn119;}
    }

#ifdef DEBUG_blizzard
    printf("nsLocalFile::CopyTo() %s -> %s\n", mPath.get(), newPathName.get());
#endif

    // actually create the file.
    Din_Go(540,2048);nsLocalFile* newFile = new nsLocalFile();
    nsCOMPtr<nsIFile> fileRef(newFile); // release on exit

    rv = newFile->InitWithNativePath(newPathName);
    Din_Go(542,2048);if (NS_FAILED(rv)) {
      {__IAENUM__ nsresult  ReplaceReturn118 = rv; Din_Go(541,2048); return ReplaceReturn118;}
    }

    // get the old permissions
    Din_Go(543,2048);uint32_t myPerms;
    GetPermissions(&myPerms);

    // Create the new file with the old file's permissions, even if write
    // permission is missing.  We can't create with write permission and
    // then change back to myPerm on all filesystems (FAT on Linux, e.g.).
    // But we can write to a read-only file on all Unix filesystems if we
    // open it successfully for writing.

    PRFileDesc* newFD;
    rv = newFile->CreateAndKeepOpen(NORMAL_FILE_TYPE,
                                    PR_WRONLY | PR_CREATE_FILE | PR_TRUNCATE,
                                    myPerms,
                                    &newFD);
    Din_Go(545,2048);if (NS_FAILED(rv)) {
      {__IAENUM__ nsresult  ReplaceReturn117 = rv; Din_Go(544,2048); return ReplaceReturn117;}
    }

    // open the old file, too
    Din_Go(546,2048);bool specialFile;
    Din_Go(549,2048);if (NS_FAILED(rv = IsSpecial(&specialFile))) {
      Din_Go(547,2048);PR_Close(newFD);
      {__IAENUM__ nsresult  ReplaceReturn116 = rv; Din_Go(548,2048); return ReplaceReturn116;}
    }
    Din_Go(552,2048);if (specialFile) {
#ifdef DEBUG
      Din_Go(550,2048);printf("Operation not supported: %s\n", mPath.get());
#endif
      // make sure to clean up properly
      PR_Close(newFD);
      {__IAENUM__ nsresult  ReplaceReturn115 = NS_OK; Din_Go(551,2048); return ReplaceReturn115;}
    }

    Din_Go(553,2048);PRFileDesc* oldFD;
    rv = OpenNSPRFileDesc(PR_RDONLY, myPerms, &oldFD);
    Din_Go(556,2048);if (NS_FAILED(rv)) {
      // make sure to clean up properly
      Din_Go(554,2048);PR_Close(newFD);
      {__IAENUM__ nsresult  ReplaceReturn114 = rv; Din_Go(555,2048); return ReplaceReturn114;}
    }

#ifdef DEBUG_blizzard
    int32_t totalRead = 0;
    int32_t totalWritten = 0;
#endif
    Din_Go(557,2048);char buf[BUFSIZ];
    int32_t bytesRead;

    // record PR_Write() error for better error message later.
    nsresult saved_write_error = NS_OK;
    nsresult saved_read_error = NS_OK;
    nsresult saved_read_close_error = NS_OK;
    nsresult saved_write_close_error = NS_OK;

    // DONE: Does PR_Read() return bytesRead < 0 for error?
    // Yes., The errors from PR_Read are not so common and
    // the value may not have correspondence in NS_ERROR_*, but
    // we do catch it still, immediately after while() loop.
    // We can differentiate errors pf PR_Read and PR_Write by
    // looking at saved_write_error value. If PR_Write error occurs (and not
    // PR_Read() error), save_write_error is not NS_OK.

    Din_Go(562,2048);while ((bytesRead = PR_Read(oldFD, buf, BUFSIZ)) > 0) {
#ifdef DEBUG_blizzard
      totalRead += bytesRead;
#endif

      // PR_Write promises never to do a short write
      Din_Go(558,2048);int32_t bytesWritten = PR_Write(newFD, buf, bytesRead);
      Din_Go(561,2048);if (bytesWritten < 0) {
        Din_Go(559,2048);saved_write_error = NSRESULT_FOR_ERRNO();
        bytesRead = -1;
        Din_Go(560,2048);break;
      }
      NS_ASSERTION(bytesWritten == bytesRead, "short PR_Write?");

#ifdef DEBUG_blizzard
      totalWritten += bytesWritten;
#endif
    }

    // TODO/FIXME: If CIFS (and NFS?) may force read/write to return EINTR,
    // we are better off to prepare for retrying. But we need confirmation if
    // EINTR is returned.

    // Record error if PR_Read() failed.
    // Must be done before any other I/O which may reset errno.
    Din_Go(564,2048);if (bytesRead < 0 && saved_write_error == NS_OK) {
      Din_Go(563,2048);saved_read_error = NSRESULT_FOR_ERRNO();
    }

#ifdef DEBUG_blizzard
    printf("read %d bytes, wrote %d bytes\n",
           totalRead, totalWritten);
#endif

    // DONE: Errors of close can occur.  Read man page of
    // close(2);
    // This is likely to happen if the file system is remote file
    // system (NFS, CIFS, etc.) and network outage occurs.
    // At least, we should tell the user that filesystem/disk is
    // hosed (possibly due to network error, hard disk failure,
    // etc.) so that users can take remedial action.

    // close the files
    Din_Go(566,2048);if (PR_Close(newFD) < 0) {
      Din_Go(565,2048);saved_write_close_error = NSRESULT_FOR_ERRNO();
#if DEBUG
      // This error merits printing.
      fprintf(stderr, "ERROR: PR_Close(newFD) returned error. errno = %d\n", errno);
#endif
    }

    Din_Go(568,2048);if (PR_Close(oldFD) < 0) {
      Din_Go(567,2048);saved_read_close_error = NSRESULT_FOR_ERRNO();
#if DEBUG
      fprintf(stderr, "ERROR: PR_Close(oldFD) returned error. errno = %d\n", errno);
#endif
    }

    // Let us report the failure to write and read.
    // check for write/read error after cleaning up
    Din_Go(573,2048);if (bytesRead < 0) {
      Din_Go(569,2048);if (saved_write_error != NS_OK) {
        {__IAENUM__ nsresult  ReplaceReturn113 = saved_write_error; Din_Go(570,2048); return ReplaceReturn113;}
      } else {/*1*/Din_Go(571,2048);if (saved_read_error != NS_OK) {
        {__IAENUM__ nsresult  ReplaceReturn112 = saved_read_error; Din_Go(572,2048); return ReplaceReturn112;}
      }
#if DEBUG
      else {              // sanity check. Die and debug.
        MOZ_ASSERT(0);
      ;/*2*/}}
#endif
    }

    Din_Go(575,2048);if (saved_write_close_error != NS_OK) {
      {__IAENUM__ nsresult  ReplaceReturn111 = saved_write_close_error; Din_Go(574,2048); return ReplaceReturn111;}
    }
    Din_Go(577,2048);if (saved_read_close_error != NS_OK) {
      {__IAENUM__ nsresult  ReplaceReturn110 = saved_read_close_error; Din_Go(576,2048); return ReplaceReturn110;}
    }
  }
  {__IAENUM__ nsresult  ReplaceReturn109 = rv; Din_Go(579,2048); return ReplaceReturn109;}
}

NS_IMETHODIMP
nsLocalFile::CopyToFollowingLinksNative(nsIFile* aNewParent,
                                        const nsACString& aNewName)
{
  {__IAENUM__ nsresult  ReplaceReturn108 = CopyToNative(aNewParent, aNewName); Din_Go(580,2048); return ReplaceReturn108;}
}

NS_IMETHODIMP
nsLocalFile::MoveToNative(nsIFile* aNewParent, const nsACString& aNewName)
{
  Din_Go(581,2048);nsresult rv;

  // check to make sure that this has been initialized properly
  CHECK_mPath();

  // check to make sure that we have a new parent
  nsAutoCString newPathName;
  rv = GetNativeTargetPathName(aNewParent, aNewName, newPathName);
  Din_Go(583,2048);if (NS_FAILED(rv)) {
    {__IAENUM__ nsresult  ReplaceReturn107 = rv; Din_Go(582,2048); return ReplaceReturn107;}
  }

  // try for atomic rename, falling back to copy/delete
  Din_Go(589,2048);if (rename(mPath.get(), newPathName.get()) < 0) {
#ifdef VMS
    if (errno == EXDEV || errno == ENXIO) {
#else
    Din_Go(584,2048);if (errno == EXDEV) {
#endif
      Din_Go(585,2048);rv = CopyToNative(aNewParent, aNewName);
      Din_Go(587,2048);if (NS_SUCCEEDED(rv)) {
        Din_Go(586,2048);rv = Remove(true);
      }
    } else {
      Din_Go(588,2048);rv = NSRESULT_FOR_ERRNO();
    }
  }

  Din_Go(591,2048);if (NS_SUCCEEDED(rv)) {
    // Adjust this
    Din_Go(590,2048);mPath = newPathName;
  }
  {__IAENUM__ nsresult  ReplaceReturn106 = rv; Din_Go(592,2048); return ReplaceReturn106;}
}

NS_IMETHODIMP
nsLocalFile::Remove(bool aRecursive)
{
Din_Go(593,2048);  CHECK_mPath();
  ENSURE_STAT_CACHE();

  Din_Go(594,2048);bool isSymLink;

  nsresult rv = IsSymlink(&isSymLink);
  Din_Go(596,2048);if (NS_FAILED(rv)) {
    {__IAENUM__ nsresult  ReplaceReturn105 = rv; Din_Go(595,2048); return ReplaceReturn105;}
  }

  Din_Go(598,2048);if (isSymLink || !S_ISDIR(mCachedStat.st_mode)) {
    {__IAENUM__ nsresult  ReplaceReturn104 = NSRESULT_FOR_RETURN(unlink(mPath.get())); Din_Go(597,2048); return ReplaceReturn104;}
  }

  Din_Go(613,2048);if (aRecursive) {
    Din_Go(599,2048);nsDirEnumeratorUnix* dir = new nsDirEnumeratorUnix();

    nsCOMPtr<nsISimpleEnumerator> dirRef(dir); // release on exit

    rv = dir->Init(this, false);
    Din_Go(601,2048);if (NS_FAILED(rv)) {
      {__IAENUM__ nsresult  ReplaceReturn103 = rv; Din_Go(600,2048); return ReplaceReturn103;}
    }

    Din_Go(602,2048);bool more;
    Din_Go(612,2048);while (dir->HasMoreElements(&more), more) {
      Din_Go(603,2048);nsCOMPtr<nsISupports> item;
      rv = dir->GetNext(getter_AddRefs(item));
      Din_Go(605,2048);if (NS_FAILED(rv)) {
        {__IAENUM__ nsresult  ReplaceReturn102 = NS_ERROR_FAILURE; Din_Go(604,2048); return ReplaceReturn102;}
      }

      Din_Go(606,2048);nsCOMPtr<nsIFile> file = do_QueryInterface(item, &rv);
      Din_Go(608,2048);if (NS_FAILED(rv)) {
        {__IAENUM__ nsresult  ReplaceReturn101 = NS_ERROR_FAILURE; Din_Go(607,2048); return ReplaceReturn101;}
      }
      Din_Go(609,2048);rv = file->Remove(aRecursive);

#ifdef ANDROID
      // See bug 580434 - Bionic gives us just deleted files
      if (rv == NS_ERROR_FILE_TARGET_DOES_NOT_EXIST) {
        continue;
      }
#endif
      Din_Go(611,2048);if (NS_FAILED(rv)) {
        {__IAENUM__ nsresult  ReplaceReturn100 = rv; Din_Go(610,2048); return ReplaceReturn100;}
      }
    }
  }

  {__IAENUM__ nsresult  ReplaceReturn99 = NSRESULT_FOR_RETURN(rmdir(mPath.get())); Din_Go(614,2048); return ReplaceReturn99;}
}

NS_IMETHODIMP
nsLocalFile::GetLastModifiedTime(PRTime* aLastModTime)
{
Din_Go(615,2048);  CHECK_mPath();
  Din_Go(617,2048);if (NS_WARN_IF(!aLastModTime)) {
    {__IAENUM__ nsresult  ReplaceReturn98 = NS_ERROR_INVALID_ARG; Din_Go(616,2048); return ReplaceReturn98;}
  }

  Din_Go(618,2048);PRFileInfo64 info;
  Din_Go(620,2048);if (PR_GetFileInfo64(mPath.get(), &info) != PR_SUCCESS) {
    {__IAENUM__ nsresult  ReplaceReturn97 = NSRESULT_FOR_ERRNO(); Din_Go(619,2048); return ReplaceReturn97;}
  }
  Din_Go(621,2048);PRTime modTime = info.modifyTime;
  Din_Go(624,2048);if (modTime == 0) {
    Din_Go(622,2048);*aLastModTime = 0;
  } else {
    Din_Go(623,2048);*aLastModTime = modTime / PR_USEC_PER_MSEC;
  }

  {__IAENUM__ nsresult  ReplaceReturn96 = NS_OK; Din_Go(625,2048); return ReplaceReturn96;}
}

NS_IMETHODIMP
nsLocalFile::SetLastModifiedTime(PRTime aLastModTime)
{
Din_Go(626,2048);  CHECK_mPath();

  Din_Go(627,2048);int result;
  Din_Go(630,2048);if (aLastModTime != 0) {
    ENSURE_STAT_CACHE();
    Din_Go(628,2048);struct utimbuf ut;
    ut.actime = mCachedStat.st_atime;

    // convert milliseconds to seconds since the unix epoch
    ut.modtime = (time_t)(aLastModTime / PR_MSEC_PER_SEC);
    result = utime(mPath.get(), &ut);
  } else {
    Din_Go(629,2048);result = utime(mPath.get(), nullptr);
  }
  {__IAENUM__ nsresult  ReplaceReturn95 = NSRESULT_FOR_RETURN(result); Din_Go(631,2048); return ReplaceReturn95;}
}

NS_IMETHODIMP
nsLocalFile::GetLastModifiedTimeOfLink(PRTime* aLastModTimeOfLink)
{
Din_Go(632,2048);  CHECK_mPath();
  Din_Go(634,2048);if (NS_WARN_IF(!aLastModTimeOfLink)) {
    {__IAENUM__ nsresult  ReplaceReturn94 = NS_ERROR_INVALID_ARG; Din_Go(633,2048); return ReplaceReturn94;}
  }

  Din_Go(635,2048);struct STAT sbuf;
  Din_Go(637,2048);if (LSTAT(mPath.get(), &sbuf) == -1) {
    {__IAENUM__ nsresult  ReplaceReturn93 = NSRESULT_FOR_ERRNO(); Din_Go(636,2048); return ReplaceReturn93;}
  }
  Din_Go(638,2048);*aLastModTimeOfLink = PRTime(sbuf.st_mtime) * PR_MSEC_PER_SEC;

  {__IAENUM__ nsresult  ReplaceReturn92 = NS_OK; Din_Go(639,2048); return ReplaceReturn92;}
}

/*
 * utime(2) may or may not dereference symlinks, joy.
 */
NS_IMETHODIMP
nsLocalFile::SetLastModifiedTimeOfLink(PRTime aLastModTimeOfLink)
{
  {__IAENUM__ nsresult  ReplaceReturn91 = SetLastModifiedTime(aLastModTimeOfLink); Din_Go(640,2048); return ReplaceReturn91;}
}

/*
 * Only send back permissions bits: maybe we want to send back the whole
 * mode_t to permit checks against other file types?
 */

#define NORMALIZE_PERMS(mode)    ((mode)& (S_IRWXU | S_IRWXG | S_IRWXO))

NS_IMETHODIMP
nsLocalFile::GetPermissions(uint32_t* aPermissions)
{
  Din_Go(641,2048);if (NS_WARN_IF(!aPermissions)) {
    {__IAENUM__ nsresult  ReplaceReturn90 = NS_ERROR_INVALID_ARG; Din_Go(642,2048); return ReplaceReturn90;}
  }
  ENSURE_STAT_CACHE();
  Din_Go(643,2048);*aPermissions = NORMALIZE_PERMS(mCachedStat.st_mode);
  {__IAENUM__ nsresult  ReplaceReturn89 = NS_OK; Din_Go(644,2048); return ReplaceReturn89;}
}

NS_IMETHODIMP
nsLocalFile::GetPermissionsOfLink(uint32_t* aPermissionsOfLink)
{
Din_Go(645,2048);  CHECK_mPath();
  Din_Go(647,2048);if (NS_WARN_IF(!aPermissionsOfLink)) {
    {__IAENUM__ nsresult  ReplaceReturn88 = NS_ERROR_INVALID_ARG; Din_Go(646,2048); return ReplaceReturn88;}
  }

  Din_Go(648,2048);struct STAT sbuf;
  Din_Go(650,2048);if (LSTAT(mPath.get(), &sbuf) == -1) {
    {__IAENUM__ nsresult  ReplaceReturn87 = NSRESULT_FOR_ERRNO(); Din_Go(649,2048); return ReplaceReturn87;}
  }
  Din_Go(651,2048);*aPermissionsOfLink = NORMALIZE_PERMS(sbuf.st_mode);
  {__IAENUM__ nsresult  ReplaceReturn86 = NS_OK; Din_Go(652,2048); return ReplaceReturn86;}
}

NS_IMETHODIMP
nsLocalFile::SetPermissions(uint32_t aPermissions)
{
Din_Go(653,2048);  CHECK_mPath();

  /*
   * Race condition here: we should use fchmod instead, there's no way to
   * guarantee the name still refers to the same file.
   */
  Din_Go(655,2048);if (chmod(mPath.get(), aPermissions) >= 0) {
    {__IAENUM__ nsresult  ReplaceReturn85 = NS_OK; Din_Go(654,2048); return ReplaceReturn85;}
  }
#if defined(ANDROID) && defined(STATFS)
  // For the time being, this is restricted for use by Android, but we
  // will figure out what to do for all platforms in bug 638503
  struct STATFS sfs;
  if (STATFS(mPath.get(), &sfs) < 0) {
    return NSRESULT_FOR_ERRNO();
  }

  // if this is a FAT file system we can't set file permissions
  if (sfs.f_type == MSDOS_SUPER_MAGIC) {
    return NS_OK;
  }
#endif
  {__IAENUM__ nsresult  ReplaceReturn84 = NSRESULT_FOR_ERRNO(); Din_Go(656,2048); return ReplaceReturn84;}
}

NS_IMETHODIMP
nsLocalFile::SetPermissionsOfLink(uint32_t aPermissions)
{
  // There isn't a consistent mechanism for doing this on UNIX platforms. We
  // might want to carefully implement this in the future though.
  {__IAENUM__ nsresult  ReplaceReturn83 = NS_ERROR_NOT_IMPLEMENTED; Din_Go(657,2048); return ReplaceReturn83;}
}

NS_IMETHODIMP
nsLocalFile::GetFileSize(int64_t* aFileSize)
{
  Din_Go(658,2048);if (NS_WARN_IF(!aFileSize)) {
    {__IAENUM__ nsresult  ReplaceReturn82 = NS_ERROR_INVALID_ARG; Din_Go(659,2048); return ReplaceReturn82;}
  }
  Din_Go(660,2048);*aFileSize = 0;
  ENSURE_STAT_CACHE();

#if defined(VMS)
  /* Only two record formats can report correct file content size */
  if ((mCachedStat.st_fab_rfm != FAB$C_STMLF) &&
      (mCachedStat.st_fab_rfm != FAB$C_STMCR)) {
    return NS_ERROR_FAILURE;
  }
#endif

  Din_Go(662,2048);if (!S_ISDIR(mCachedStat.st_mode)) {
    Din_Go(661,2048);*aFileSize = (int64_t)mCachedStat.st_size;
  }
  {__IAENUM__ nsresult  ReplaceReturn81 = NS_OK; Din_Go(663,2048); return ReplaceReturn81;}
}

NS_IMETHODIMP
nsLocalFile::SetFileSize(int64_t aFileSize)
{
Din_Go(664,2048);  CHECK_mPath();

#if defined(ANDROID)
  /* no truncate on bionic */
  int fd = open(mPath.get(), O_WRONLY);
  if (fd == -1) {
    return NSRESULT_FOR_ERRNO();
  }

  int ret = ftruncate(fd, (off_t)aFileSize);
  close(fd);

  if (ret == -1) {
    return NSRESULT_FOR_ERRNO();
  }
#elif defined(HAVE_TRUNCATE64)
  Din_Go(666,2048);if (truncate64(mPath.get(), (off64_t)aFileSize) == -1) {
    {__IAENUM__ nsresult  ReplaceReturn80 = NSRESULT_FOR_ERRNO(); Din_Go(665,2048); return ReplaceReturn80;}
  }
#else
  off_t size = (off_t)aFileSize;
  if (truncate(mPath.get(), size) == -1) {
    return NSRESULT_FOR_ERRNO();
  }
#endif
  {__IAENUM__ nsresult  ReplaceReturn79 = NS_OK; Din_Go(667,2048); return ReplaceReturn79;}
}

NS_IMETHODIMP
nsLocalFile::GetFileSizeOfLink(int64_t* aFileSize)
{
Din_Go(668,2048);  CHECK_mPath();
  Din_Go(670,2048);if (NS_WARN_IF(!aFileSize)) {
    {__IAENUM__ nsresult  ReplaceReturn78 = NS_ERROR_INVALID_ARG; Din_Go(669,2048); return ReplaceReturn78;}
  }

  Din_Go(671,2048);struct STAT sbuf;
  Din_Go(673,2048);if (LSTAT(mPath.get(), &sbuf) == -1) {
    {__IAENUM__ nsresult  ReplaceReturn77 = NSRESULT_FOR_ERRNO(); Din_Go(672,2048); return ReplaceReturn77;}
  }

  Din_Go(674,2048);*aFileSize = (int64_t)sbuf.st_size;
  {__IAENUM__ nsresult  ReplaceReturn76 = NS_OK; Din_Go(675,2048); return ReplaceReturn76;}
}

#if defined(USE_LINUX_QUOTACTL)
/*
 * Searches /proc/self/mountinfo for given device (Major:Minor),
 * returns exported name from /dev
 *
 * Fails when /proc/self/mountinfo or diven device don't exist.
 */
static bool
GetDeviceName(int aDeviceMajor, int aDeviceMinor, nsACString& aDeviceName)
{
  Din_Go(676,2048);bool ret = false;

  const int kMountInfoLineLength = 200;
  const int kMountInfoDevPosition = 6;

  char mountinfoLine[kMountInfoLineLength];
  char deviceNum[kMountInfoLineLength];

  snprintf(deviceNum, kMountInfoLineLength, "%d:%d", aDeviceMajor, aDeviceMinor);

  FILE* f = fopen("/proc/self/mountinfo", "rt");
  Din_Go(678,2048);if (!f) {
    {_Bool  ReplaceReturn75 = ret; Din_Go(677,2048); return ReplaceReturn75;}
  }

  // Expects /proc/self/mountinfo in format:
  // 'ID ID major:minor root mountpoint flags - type devicename flags'
  Din_Go(689,2048);while (fgets(mountinfoLine, kMountInfoLineLength, f)) {
    Din_Go(679,2048);char* p_dev = strstr(mountinfoLine, deviceNum);

    Din_Go(683,2048);for (int i = 0; i < kMountInfoDevPosition && p_dev; ++i) {
      Din_Go(680,2048);p_dev = strchr(p_dev, ' ');
      Din_Go(682,2048);if (p_dev) {
        Din_Go(681,2048);p_dev++;
      }
    }

    Din_Go(688,2048);if (p_dev) {
      Din_Go(684,2048);char* p_dev_end = strchr(p_dev, ' ');
      Din_Go(687,2048);if (p_dev_end) {
        Din_Go(685,2048);*p_dev_end = '\0';
        aDeviceName.Assign(p_dev);
        ret = true;
        Din_Go(686,2048);break;
      }
    }
  }

  Din_Go(690,2048);fclose(f);
  {_Bool  ReplaceReturn74 = ret; Din_Go(691,2048); return ReplaceReturn74;}
}
#endif

NS_IMETHODIMP
nsLocalFile::GetDiskSpaceAvailable(int64_t* aDiskSpaceAvailable)
{
  Din_Go(692,2048);if (NS_WARN_IF(!aDiskSpaceAvailable)) {
    {__IAENUM__ nsresult  ReplaceReturn73 = NS_ERROR_INVALID_ARG; Din_Go(693,2048); return ReplaceReturn73;}
  }

  // These systems have the operations necessary to check disk space.

#ifdef STATFS

  // check to make sure that mPath is properly initialized
  CHECK_mPath();

  Din_Go(694,2048);struct STATFS fs_buf;

  /*
   * Members of the STATFS struct that you should know about:
   * F_BSIZE = block size on disk.
   * f_bavail = number of free blocks available to a non-superuser.
   * f_bfree = number of total free blocks in file system.
   */

  Din_Go(697,2048);if (STATFS(mPath.get(), &fs_buf) < 0) {
    // The call to STATFS failed.
#ifdef DEBUG
    Din_Go(695,2048);printf("ERROR: GetDiskSpaceAvailable: STATFS call FAILED. \n");
#endif
    {__IAENUM__ nsresult  ReplaceReturn72 = NS_ERROR_FAILURE; Din_Go(696,2048); return ReplaceReturn72;}
  }

  Din_Go(698,2048);*aDiskSpaceAvailable = (int64_t)fs_buf.F_BSIZE * fs_buf.f_bavail;

#ifdef DEBUG_DISK_SPACE
  printf("DiskSpaceAvailable: %lu bytes\n",
         *aDiskSpaceAvailable);
#endif

#if defined(USE_LINUX_QUOTACTL)

  Din_Go(700,2048);if (!FillStatCache()) {
    // Return available size from statfs
    {__IAENUM__ nsresult  ReplaceReturn71 = NS_OK; Din_Go(699,2048); return ReplaceReturn71;}
  }

  Din_Go(701,2048);nsCString deviceName;
  Din_Go(703,2048);if (!GetDeviceName(major(mCachedStat.st_dev),
                     minor(mCachedStat.st_dev),
                     deviceName)) {
    {__IAENUM__ nsresult  ReplaceReturn70 = NS_OK; Din_Go(702,2048); return ReplaceReturn70;}
  }

  Din_Go(704,2048);struct dqblk dq;
  Din_Go(710,2048);if (!quotactl(QCMD(Q_GETQUOTA, USRQUOTA), deviceName.get(),
                getuid(), (caddr_t)&dq)
#ifdef QIF_BLIMITS
      && dq.dqb_valid & QIF_BLIMITS
#endif
      && dq.dqb_bhardlimit) {
    Din_Go(705,2048);int64_t QuotaSpaceAvailable = 0;
    // dqb_bhardlimit is count of BLOCK_SIZE blocks, dqb_curspace is bytes
    Din_Go(707,2048);if ((BLOCK_SIZE * dq.dqb_bhardlimit) > dq.dqb_curspace)
      {/*3*/Din_Go(706,2048);QuotaSpaceAvailable = int64_t(BLOCK_SIZE * dq.dqb_bhardlimit - dq.dqb_curspace);/*4*/}
    Din_Go(709,2048);if (QuotaSpaceAvailable < *aDiskSpaceAvailable) {
      Din_Go(708,2048);*aDiskSpaceAvailable = QuotaSpaceAvailable;
    }
  }
#endif

  {__IAENUM__ nsresult  ReplaceReturn69 = NS_OK; Din_Go(711,2048); return ReplaceReturn69;}

#else
  /*
   * This platform doesn't have statfs or statvfs.  I'm sure that there's
   * a way to check for free disk space on platforms that don't have statfs
   * (I'm SURE they have df, for example).
   *
   * Until we figure out how to do that, lets be honest and say that this
   * command isn't implemented properly for these platforms yet.
   */
#ifdef DEBUG
  printf("ERROR: GetDiskSpaceAvailable: Not implemented for plaforms without statfs.\n");
#endif
  return NS_ERROR_NOT_IMPLEMENTED;

#endif /* STATFS */

}

NS_IMETHODIMP
nsLocalFile::GetParent(nsIFile** aParent)
{
Din_Go(712,2048);  CHECK_mPath();
  Din_Go(714,2048);if (NS_WARN_IF(!aParent)) {
    {__IAENUM__ nsresult  ReplaceReturn68 = NS_ERROR_INVALID_ARG; Din_Go(713,2048); return ReplaceReturn68;}
  }
  Din_Go(715,2048);*aParent = nullptr;

  // if '/' we are at the top of the volume, return null
  Din_Go(717,2048);if (mPath.EqualsLiteral("/")) {
    {__IAENUM__ nsresult  ReplaceReturn67 = NS_OK; Din_Go(716,2048); return ReplaceReturn67;}
  }

  // <brendan, after jband> I promise to play nice
  Din_Go(718,2048);char* buffer = mPath.BeginWriting();
  char* slashp = buffer;

  // find the last significant slash in buffer
  slashp = strrchr(buffer, '/');
  NS_ASSERTION(slashp, "non-canonical path?");
  Din_Go(720,2048);if (!slashp) {
    {__IAENUM__ nsresult  ReplaceReturn66 = NS_ERROR_FILE_INVALID_PATH; Din_Go(719,2048); return ReplaceReturn66;}
  }

  // for the case where we are at '/'
  Din_Go(722,2048);if (slashp == buffer) {
    Din_Go(721,2048);slashp++;
  }

  // temporarily terminate buffer at the last significant slash
  Din_Go(723,2048);char c = *slashp;
  *slashp = '\0';

  nsCOMPtr<nsIFile> localFile;
  nsresult rv = NS_NewNativeLocalFile(nsDependentCString(buffer), true,
                                      getter_AddRefs(localFile));

  // make buffer whole again
  *slashp = c;

  Din_Go(725,2048);if (NS_FAILED(rv)) {
    {__IAENUM__ nsresult  ReplaceReturn65 = rv; Din_Go(724,2048); return ReplaceReturn65;}
  }

  Din_Go(726,2048);localFile.forget(aParent);
  {__IAENUM__ nsresult  ReplaceReturn64 = NS_OK; Din_Go(727,2048); return ReplaceReturn64;}
}

/*
 * The results of Exists, isWritable and isReadable are not cached.
 */


NS_IMETHODIMP
nsLocalFile::Exists(bool* aResult)
{
Din_Go(728,2048);  CHECK_mPath();
  Din_Go(730,2048);if (NS_WARN_IF(!aResult)) {
    {__IAENUM__ nsresult  ReplaceReturn63 = NS_ERROR_INVALID_ARG; Din_Go(729,2048); return ReplaceReturn63;}
  }

  Din_Go(731,2048);*aResult = (access(mPath.get(), F_OK) == 0);
  {__IAENUM__ nsresult  ReplaceReturn62 = NS_OK; Din_Go(732,2048); return ReplaceReturn62;}
}


NS_IMETHODIMP
nsLocalFile::IsWritable(bool* aResult)
{
Din_Go(733,2048);  CHECK_mPath();
  Din_Go(735,2048);if (NS_WARN_IF(!aResult)) {
    {__IAENUM__ nsresult  ReplaceReturn61 = NS_ERROR_INVALID_ARG; Din_Go(734,2048); return ReplaceReturn61;}
  }

  Din_Go(736,2048);*aResult = (access(mPath.get(), W_OK) == 0);
  Din_Go(738,2048);if (*aResult || errno == EACCES) {
    {__IAENUM__ nsresult  ReplaceReturn60 = NS_OK; Din_Go(737,2048); return ReplaceReturn60;}
  }
  {__IAENUM__ nsresult  ReplaceReturn59 = NSRESULT_FOR_ERRNO(); Din_Go(739,2048); return ReplaceReturn59;}
}

NS_IMETHODIMP
nsLocalFile::IsReadable(bool* aResult)
{
Din_Go(740,2048);  CHECK_mPath();
  Din_Go(742,2048);if (NS_WARN_IF(!aResult)) {
    {__IAENUM__ nsresult  ReplaceReturn58 = NS_ERROR_INVALID_ARG; Din_Go(741,2048); return ReplaceReturn58;}
  }

  Din_Go(743,2048);*aResult = (access(mPath.get(), R_OK) == 0);
  Din_Go(745,2048);if (*aResult || errno == EACCES) {
    {__IAENUM__ nsresult  ReplaceReturn57 = NS_OK; Din_Go(744,2048); return ReplaceReturn57;}
  }
  {__IAENUM__ nsresult  ReplaceReturn56 = NSRESULT_FOR_ERRNO(); Din_Go(746,2048); return ReplaceReturn56;}
}

NS_IMETHODIMP
nsLocalFile::IsExecutable(bool* aResult)
{
Din_Go(747,2048);  CHECK_mPath();
  Din_Go(749,2048);if (NS_WARN_IF(!aResult)) {
    {__IAENUM__ nsresult  ReplaceReturn55 = NS_ERROR_INVALID_ARG; Din_Go(748,2048); return ReplaceReturn55;}
  }

  // Check extension (bug 663899). On certain platforms, the file
  // extension may cause the OS to treat it as executable regardless of
  // the execute bit, such as .jar on Mac OS X. We borrow the code from
  // nsLocalFileWin, slightly modified.

  // Don't be fooled by symlinks.
  Din_Go(750,2048);bool symLink;
  nsresult rv = IsSymlink(&symLink);
  Din_Go(752,2048);if (NS_FAILED(rv)) {
    {__IAENUM__ nsresult  ReplaceReturn54 = rv; Din_Go(751,2048); return ReplaceReturn54;}
  }

  Din_Go(753,2048);nsAutoString path;
  Din_Go(756,2048);if (symLink) {
    Din_Go(754,2048);GetTarget(path);
  } else {
    Din_Go(755,2048);GetPath(path);
  }

  Din_Go(757,2048);int32_t dotIdx = path.RFindChar(char16_t('.'));
  Din_Go(766,2048);if (dotIdx != kNotFound) {
    // Convert extension to lower case.
    Din_Go(758,2048);char16_t* p = path.BeginWriting();
    Din_Go(760,2048);for (p += dotIdx + 1; *p; ++p) {
      Din_Go(759,2048);*p += (*p >= L'A' && *p <= L'Z') ? 'a' - 'A' : 0;
    }

    // Search for any of the set of executable extensions.
    Din_Go(761,2048);static const char* const executableExts[] = {
      "air",  // Adobe AIR installer
      "jar"   // java application bundle
    };
    nsDependentSubstring ext = Substring(path, dotIdx + 1);
    Din_Go(765,2048);for (size_t i = 0; i < ArrayLength(executableExts); i++) {
      Din_Go(762,2048);if (ext.EqualsASCII(executableExts[i])) {
        // Found a match.  Set result and quit.
        Din_Go(763,2048);*aResult = true;
        {__IAENUM__ nsresult  ReplaceReturn53 = NS_OK; Din_Go(764,2048); return ReplaceReturn53;}
      }
    }
  }

  // On OS X, then query Launch Services.
#ifdef MOZ_WIDGET_COCOA
  // Certain Mac applications, such as Classic applications, which
  // run under Rosetta, might not have the +x mode bit but are still
  // considered to be executable by Launch Services (bug 646748).
  CFURLRef url;
  if (NS_FAILED(GetCFURL(&url))) {
    return NS_ERROR_FAILURE;
  }

  LSRequestedInfo theInfoRequest = kLSRequestAllInfo;
  LSItemInfoRecord theInfo;
  OSStatus result = ::LSCopyItemInfoForURL(url, theInfoRequest, &theInfo);
  ::CFRelease(url);
  if (result == noErr) {
    if ((theInfo.flags & kLSItemInfoIsApplication) != 0) {
      *aResult = true;
      return NS_OK;
    }
  }
#endif

  // Then check the execute bit.
  Din_Go(767,2048);*aResult = (access(mPath.get(), X_OK) == 0);
#ifdef SOLARIS
  // On Solaris, access will always return 0 for root user, however
  // the file is only executable if S_IXUSR | S_IXGRP | S_IXOTH is set.
  // See bug 351950, https://bugzilla.mozilla.org/show_bug.cgi?id=351950
  if (*aResult) {
    struct STAT buf;

    *aResult = (STAT(mPath.get(), &buf) == 0);
    if (*aResult || errno == EACCES) {
      *aResult = *aResult && (buf.st_mode & (S_IXUSR | S_IXGRP | S_IXOTH));
      return NS_OK;
    }

    return NSRESULT_FOR_ERRNO();
  }
#endif
  Din_Go(769,2048);if (*aResult || errno == EACCES) {
    {__IAENUM__ nsresult  ReplaceReturn52 = NS_OK; Din_Go(768,2048); return ReplaceReturn52;}
  }
  {__IAENUM__ nsresult  ReplaceReturn51 = NSRESULT_FOR_ERRNO(); Din_Go(770,2048); return ReplaceReturn51;}
}

NS_IMETHODIMP
nsLocalFile::IsDirectory(bool* aResult)
{
  Din_Go(771,2048);if (NS_WARN_IF(!aResult)) {
    {__IAENUM__ nsresult  ReplaceReturn50 = NS_ERROR_INVALID_ARG; Din_Go(772,2048); return ReplaceReturn50;}
  }
  Din_Go(773,2048);*aResult = false;
  ENSURE_STAT_CACHE();
  *aResult = S_ISDIR(mCachedStat.st_mode);
  {__IAENUM__ nsresult  ReplaceReturn49 = NS_OK; Din_Go(774,2048); return ReplaceReturn49;}
}

NS_IMETHODIMP
nsLocalFile::IsFile(bool* aResult)
{
  Din_Go(775,2048);if (NS_WARN_IF(!aResult)) {
    {__IAENUM__ nsresult  ReplaceReturn48 = NS_ERROR_INVALID_ARG; Din_Go(776,2048); return ReplaceReturn48;}
  }
  Din_Go(777,2048);*aResult = false;
  ENSURE_STAT_CACHE();
  *aResult = S_ISREG(mCachedStat.st_mode);
  {__IAENUM__ nsresult  ReplaceReturn47 = NS_OK; Din_Go(778,2048); return ReplaceReturn47;}
}

NS_IMETHODIMP
nsLocalFile::IsHidden(bool* aResult)
{
  Din_Go(779,2048);if (NS_WARN_IF(!aResult)) {
    {__IAENUM__ nsresult  ReplaceReturn46 = NS_ERROR_INVALID_ARG; Din_Go(780,2048); return ReplaceReturn46;}
  }
  nsACString::const_iterator begin, end;
  Din_Go(781,2048);LocateNativeLeafName(begin, end);
  *aResult = (*begin == '.');
  {__IAENUM__ nsresult  ReplaceReturn45 = NS_OK; Din_Go(782,2048); return ReplaceReturn45;}
}

NS_IMETHODIMP
nsLocalFile::IsSymlink(bool* aResult)
{
  Din_Go(783,2048);if (NS_WARN_IF(!aResult)) {
    {__IAENUM__ nsresult  ReplaceReturn44 = NS_ERROR_INVALID_ARG; Din_Go(784,2048); return ReplaceReturn44;}
  }
  CHECK_mPath();

  Din_Go(785,2048);struct STAT symStat;
  Din_Go(787,2048);if (LSTAT(mPath.get(), &symStat) == -1) {
    {__IAENUM__ nsresult  ReplaceReturn43 = NSRESULT_FOR_ERRNO(); Din_Go(786,2048); return ReplaceReturn43;}
  }
  Din_Go(788,2048);*aResult = S_ISLNK(symStat.st_mode);
  {__IAENUM__ nsresult  ReplaceReturn42 = NS_OK; Din_Go(789,2048); return ReplaceReturn42;}
}

NS_IMETHODIMP
nsLocalFile::IsSpecial(bool* aResult)
{
  Din_Go(790,2048);if (NS_WARN_IF(!aResult)) {
    {__IAENUM__ nsresult  ReplaceReturn41 = NS_ERROR_INVALID_ARG; Din_Go(791,2048); return ReplaceReturn41;}
  }
  ENSURE_STAT_CACHE();
  Din_Go(792,2048);*aResult = S_ISCHR(mCachedStat.st_mode)   ||
             S_ISBLK(mCachedStat.st_mode)   ||
#ifdef S_ISSOCK
             S_ISSOCK(mCachedStat.st_mode)  ||
#endif
             S_ISFIFO(mCachedStat.st_mode);

  {__IAENUM__ nsresult  ReplaceReturn40 = NS_OK; Din_Go(793,2048); return ReplaceReturn40;}
}

NS_IMETHODIMP
nsLocalFile::Equals(nsIFile* aInFile, bool* aResult)
{
  Din_Go(794,2048);if (NS_WARN_IF(!aInFile)) {
    {__IAENUM__ nsresult  ReplaceReturn39 = NS_ERROR_INVALID_ARG; Din_Go(795,2048); return ReplaceReturn39;}
  }
  Din_Go(797,2048);if (NS_WARN_IF(!aResult)) {
    {__IAENUM__ nsresult  ReplaceReturn38 = NS_ERROR_INVALID_ARG; Din_Go(796,2048); return ReplaceReturn38;}
  }
  Din_Go(798,2048);*aResult = false;

  nsAutoCString inPath;
  nsresult rv = aInFile->GetNativePath(inPath);
  Din_Go(800,2048);if (NS_FAILED(rv)) {
    {__IAENUM__ nsresult  ReplaceReturn37 = rv; Din_Go(799,2048); return ReplaceReturn37;}
  }

  // We don't need to worry about "/foo/" vs. "/foo" here
  // because trailing slashes are stripped on init.
  Din_Go(801,2048);*aResult = !strcmp(inPath.get(), mPath.get());
  {__IAENUM__ nsresult  ReplaceReturn36 = NS_OK; Din_Go(802,2048); return ReplaceReturn36;}
}

NS_IMETHODIMP
nsLocalFile::Contains(nsIFile* aInFile, bool* aResult)
{
Din_Go(803,2048);  CHECK_mPath();
  Din_Go(805,2048);if (NS_WARN_IF(!aInFile)) {
    {__IAENUM__ nsresult  ReplaceReturn35 = NS_ERROR_INVALID_ARG; Din_Go(804,2048); return ReplaceReturn35;}
  }
  Din_Go(807,2048);if (NS_WARN_IF(!aResult)) {
    {__IAENUM__ nsresult  ReplaceReturn34 = NS_ERROR_INVALID_ARG; Din_Go(806,2048); return ReplaceReturn34;}
  }

  Din_Go(808,2048);nsAutoCString inPath;
  nsresult rv;

  Din_Go(810,2048);if (NS_FAILED(rv = aInFile->GetNativePath(inPath))) {
    {__IAENUM__ nsresult  ReplaceReturn33 = rv; Din_Go(809,2048); return ReplaceReturn33;}
  }

  Din_Go(811,2048);*aResult = false;

  ssize_t len = mPath.Length();
  Din_Go(814,2048);if (strncmp(mPath.get(), inPath.get(), len) == 0) {
    // Now make sure that the |aInFile|'s path has a separator at len,
    // which implies that it has more components after len.
    Din_Go(812,2048);if (inPath[len] == '/') {
      Din_Go(813,2048);*aResult = true;
    }
  }

  {__IAENUM__ nsresult  ReplaceReturn32 = NS_OK; Din_Go(815,2048); return ReplaceReturn32;}
}

NS_IMETHODIMP
nsLocalFile::GetNativeTarget(nsACString& aResult)
{
Din_Go(816,2048);  CHECK_mPath();
  Din_Go(817,2048);aResult.Truncate();

  struct STAT symStat;
  Din_Go(819,2048);if (LSTAT(mPath.get(), &symStat) == -1) {
    {__IAENUM__ nsresult  ReplaceReturn31 = NSRESULT_FOR_ERRNO(); Din_Go(818,2048); return ReplaceReturn31;}
  }

  Din_Go(821,2048);if (!S_ISLNK(symStat.st_mode)) {
    {__IAENUM__ nsresult  ReplaceReturn30 = NS_ERROR_FILE_INVALID_PATH; Din_Go(820,2048); return ReplaceReturn30;}
  }

  Din_Go(822,2048);int32_t size = (int32_t)symStat.st_size;
  char* target = (char*)moz_xmalloc(size + 1);
  Din_Go(824,2048);if (!target) {
    {__IAENUM__ nsresult  ReplaceReturn29 = NS_ERROR_OUT_OF_MEMORY; Din_Go(823,2048); return ReplaceReturn29;}
  }

  Din_Go(827,2048);if (readlink(mPath.get(), target, (size_t)size) < 0) {
    Din_Go(825,2048);free(target);
    {__IAENUM__ nsresult  ReplaceReturn28 = NSRESULT_FOR_ERRNO(); Din_Go(826,2048); return ReplaceReturn28;}
  }
  Din_Go(828,2048);target[size] = '\0';

  nsresult rv = NS_OK;
  nsCOMPtr<nsIFile> self(this);
  int32_t maxLinks = 40;
  Din_Go(859,2048);while (true) {
    Din_Go(829,2048);if (maxLinks-- == 0) {
      Din_Go(830,2048);rv = NS_ERROR_FILE_UNRESOLVABLE_SYMLINK;
      Din_Go(831,2048);break;
    }

    Din_Go(841,2048);if (target[0] != '/') {
      Din_Go(832,2048);nsCOMPtr<nsIFile> parent;
      Din_Go(834,2048);if (NS_FAILED(rv = self->GetParent(getter_AddRefs(parent)))) {
        Din_Go(833,2048);break;
      }
      Din_Go(836,2048);if (NS_FAILED(rv = parent->AppendRelativeNativePath(nsDependentCString(target)))) {
        Din_Go(835,2048);break;
      }
      Din_Go(838,2048);if (NS_FAILED(rv = parent->GetNativePath(aResult))) {
        Din_Go(837,2048);break;
      }
      Din_Go(839,2048);self = parent;
    } else {
      Din_Go(840,2048);aResult = target;
    }

    Din_Go(842,2048);const nsPromiseFlatCString& flatRetval = PromiseFlatCString(aResult);

    // Any failure in testing the current target we'll just interpret
    // as having reached our destiny.
    Din_Go(844,2048);if (LSTAT(flatRetval.get(), &symStat) == -1) {
      Din_Go(843,2048);break;
    }

    // And of course we're done if it isn't a symlink.
    Din_Go(846,2048);if (!S_ISLNK(symStat.st_mode)) {
      Din_Go(845,2048);break;
    }

    Din_Go(847,2048);int32_t newSize = (int32_t)symStat.st_size;
    Din_Go(853,2048);if (newSize > size) {
      Din_Go(848,2048);char* newTarget = (char*)moz_xrealloc(target, newSize + 1);
      Din_Go(851,2048);if (!newTarget) {
        Din_Go(849,2048);rv = NS_ERROR_OUT_OF_MEMORY;
        Din_Go(850,2048);break;
      }
      Din_Go(852,2048);target = newTarget;
      size = newSize;
    }

    Din_Go(854,2048);int32_t linkLen = readlink(flatRetval.get(), target, size);
    Din_Go(857,2048);if (linkLen == -1) {
      Din_Go(855,2048);rv = NSRESULT_FOR_ERRNO();
      Din_Go(856,2048);break;
    }
    Din_Go(858,2048);target[linkLen] = '\0';
  }

  Din_Go(860,2048);free(target);

  Din_Go(862,2048);if (NS_FAILED(rv)) {
    Din_Go(861,2048);aResult.Truncate();
  }
  {__IAENUM__ nsresult  ReplaceReturn27 = rv; Din_Go(863,2048); return ReplaceReturn27;}
}

NS_IMETHODIMP
nsLocalFile::GetFollowLinks(bool* aFollowLinks)
{
  Din_Go(864,2048);*aFollowLinks = true;
  {__IAENUM__ nsresult  ReplaceReturn26 = NS_OK; Din_Go(865,2048); return ReplaceReturn26;}
}

NS_IMETHODIMP
nsLocalFile::SetFollowLinks(bool aFollowLinks)
{
  {__IAENUM__ nsresult  ReplaceReturn25 = NS_OK; Din_Go(866,2048); return ReplaceReturn25;}
}

NS_IMETHODIMP
nsLocalFile::GetDirectoryEntries(nsISimpleEnumerator** aEntries)
{
  Din_Go(867,2048);RefPtr<nsDirEnumeratorUnix> dir = new nsDirEnumeratorUnix();

  nsresult rv = dir->Init(this, false);
  Din_Go(870,2048);if (NS_FAILED(rv)) {
    Din_Go(868,2048);*aEntries = nullptr;
  } else {
    Din_Go(869,2048);dir.forget(aEntries);
  }

  {__IAENUM__ nsresult  ReplaceReturn24 = rv; Din_Go(871,2048); return ReplaceReturn24;}
}

NS_IMETHODIMP
nsLocalFile::Load(PRLibrary** aResult)
{
Din_Go(872,2048);  CHECK_mPath();
  Din_Go(874,2048);if (NS_WARN_IF(!aResult)) {
    {__IAENUM__ nsresult  ReplaceReturn23 = NS_ERROR_INVALID_ARG; Din_Go(873,2048); return ReplaceReturn23;}
  }

#ifdef NS_BUILD_REFCNT_LOGGING
  Din_Go(875,2048);nsTraceRefcnt::SetActivityIsLegal(false);
#endif

  *aResult = PR_LoadLibrary(mPath.get());

#ifdef NS_BUILD_REFCNT_LOGGING
  nsTraceRefcnt::SetActivityIsLegal(true);
#endif

  Din_Go(877,2048);if (!*aResult) {
    {__IAENUM__ nsresult  ReplaceReturn22 = NS_ERROR_FAILURE; Din_Go(876,2048); return ReplaceReturn22;}
  }
  {__IAENUM__ nsresult  ReplaceReturn21 = NS_OK; Din_Go(878,2048); return ReplaceReturn21;}
}

NS_IMETHODIMP
nsLocalFile::GetPersistentDescriptor(nsACString& aPersistentDescriptor)
{
  {__IAENUM__ nsresult  ReplaceReturn20 = GetNativePath(aPersistentDescriptor); Din_Go(879,2048); return ReplaceReturn20;}
}

NS_IMETHODIMP
nsLocalFile::SetPersistentDescriptor(const nsACString& aPersistentDescriptor)
{
#ifdef MOZ_WIDGET_COCOA
  if (aPersistentDescriptor.IsEmpty()) {
    return NS_ERROR_INVALID_ARG;
  }

  // Support pathnames as user-supplied descriptors if they begin with '/'
  // or '~'.  These characters do not collide with the base64 set used for
  // encoding alias records.
  char first = aPersistentDescriptor.First();
  if (first == '/' || first == '~') {
    return InitWithNativePath(aPersistentDescriptor);
  }

  uint32_t dataSize = aPersistentDescriptor.Length();
  char* decodedData = PL_Base64Decode(
    PromiseFlatCString(aPersistentDescriptor).get(), dataSize, nullptr);
  if (!decodedData) {
    NS_ERROR("SetPersistentDescriptor was given bad data");
    return NS_ERROR_FAILURE;
  }

  // Cast to an alias record and resolve.
  AliasRecord aliasHeader = *(AliasPtr)decodedData;
  int32_t aliasSize = ::GetAliasSizeFromPtr(&aliasHeader);
  if (aliasSize > ((int32_t)dataSize * 3) / 4) { // be paranoid about having too few data
    PR_Free(decodedData);
    return NS_ERROR_FAILURE;
  }

  nsresult rv = NS_OK;

  // Move the now-decoded data into the Handle.
  // The size of the decoded data is 3/4 the size of the encoded data. See plbase64.h
  Handle  newHandle = nullptr;
  if (::PtrToHand(decodedData, &newHandle, aliasSize) != noErr) {
    rv = NS_ERROR_OUT_OF_MEMORY;
  }
  PR_Free(decodedData);
  if (NS_FAILED(rv)) {
    return rv;
  }

  Boolean changed;
  FSRef resolvedFSRef;
  OSErr err = ::FSResolveAlias(nullptr, (AliasHandle)newHandle, &resolvedFSRef,
                               &changed);

  rv = MacErrorMapper(err);
  DisposeHandle(newHandle);
  if (NS_FAILED(rv)) {
    return rv;
  }

  return InitWithFSRef(&resolvedFSRef);
#else
  {__IAENUM__ nsresult  ReplaceReturn19 = InitWithNativePath(aPersistentDescriptor); Din_Go(880,2048); return ReplaceReturn19;}
#endif
}

NS_IMETHODIMP
nsLocalFile::Reveal()
{
#ifdef MOZ_WIDGET_GTK
  Din_Go(881,2048);nsCOMPtr<nsIGIOService> giovfs = do_GetService(NS_GIOSERVICE_CONTRACTID);
  Din_Go(883,2048);if (!giovfs) {
    {__IAENUM__ nsresult  ReplaceReturn18 = NS_ERROR_FAILURE; Din_Go(882,2048); return ReplaceReturn18;}
  }

  Din_Go(884,2048);bool isDirectory;
  Din_Go(886,2048);if (NS_FAILED(IsDirectory(&isDirectory))) {
    {__IAENUM__ nsresult  ReplaceReturn17 = NS_ERROR_FAILURE; Din_Go(885,2048); return ReplaceReturn17;}
  }

  Din_Go(896,2048);if (isDirectory) {
    {__IAENUM__ nsresult  ReplaceReturn16 = giovfs->ShowURIForInput(mPath); Din_Go(887,2048); return ReplaceReturn16;}
  } else {/*5*/Din_Go(888,2048);if (NS_SUCCEEDED(giovfs->OrgFreedesktopFileManager1ShowItems(mPath))) {
    {__IAENUM__ nsresult  ReplaceReturn15 = NS_OK; Din_Go(889,2048); return ReplaceReturn15;}
  } else {
    Din_Go(890,2048);nsCOMPtr<nsIFile> parentDir;
    nsAutoCString dirPath;
    Din_Go(892,2048);if (NS_FAILED(GetParent(getter_AddRefs(parentDir)))) {
      {__IAENUM__ nsresult  ReplaceReturn14 = NS_ERROR_FAILURE; Din_Go(891,2048); return ReplaceReturn14;}
    }
    Din_Go(894,2048);if (NS_FAILED(parentDir->GetNativePath(dirPath))) {
      {__IAENUM__ nsresult  ReplaceReturn13 = NS_ERROR_FAILURE; Din_Go(893,2048); return ReplaceReturn13;}
    }

    {__IAENUM__ nsresult  ReplaceReturn12 = giovfs->ShowURIForInput(dirPath); Din_Go(895,2048); return ReplaceReturn12;}
  ;/*6*/}}
#elif defined(MOZ_WIDGET_COCOA)
  CFURLRef url;
  if (NS_SUCCEEDED(GetCFURL(&url))) {
    nsresult rv = CocoaFileUtils::RevealFileInFinder(url);
    ::CFRelease(url);
    return rv;
  }
  return NS_ERROR_FAILURE;
#else
  return NS_ERROR_FAILURE;
#endif
Din_Go(897,2048);}

NS_IMETHODIMP
nsLocalFile::Launch()
{
#ifdef MOZ_WIDGET_GTK
  Din_Go(898,2048);nsCOMPtr<nsIGIOService> giovfs = do_GetService(NS_GIOSERVICE_CONTRACTID);
  Din_Go(900,2048);if (!giovfs) {
    {__IAENUM__ nsresult  ReplaceReturn11 = NS_ERROR_FAILURE; Din_Go(899,2048); return ReplaceReturn11;}
  }

  {__IAENUM__ nsresult  ReplaceReturn10 = giovfs->ShowURIForInput(mPath); Din_Go(901,2048); return ReplaceReturn10;}
#elif defined(MOZ_ENABLE_CONTENTACTION)
  QUrl uri = QUrl::fromLocalFile(QString::fromUtf8(mPath.get()));
  ContentAction::Action action =
    ContentAction::Action::defaultActionForFile(uri);

  if (action.isValid()) {
    action.trigger();
    return NS_OK;
  }

  return NS_ERROR_FAILURE;
#elif defined(MOZ_WIDGET_ANDROID)
  // Try to get a mimetype, if this fails just use the file uri alone
  nsresult rv;
  nsAutoCString type;
  nsCOMPtr<nsIMIMEService> mimeService(do_GetService("@mozilla.org/mime;1", &rv));
  if (NS_SUCCEEDED(rv)) {
    rv = mimeService->GetTypeFromFile(this, type);
  }

  nsAutoCString fileUri = NS_LITERAL_CSTRING("file://") + mPath;
  return widget::GeckoAppShell::OpenUriExternal(
    NS_ConvertUTF8toUTF16(fileUri),
    NS_ConvertUTF8toUTF16(type),
    EmptyString(),
    EmptyString(),
    EmptyString(),
    EmptyString()) ? NS_OK : NS_ERROR_FAILURE;
#elif defined(MOZ_WIDGET_COCOA)
  CFURLRef url;
  if (NS_SUCCEEDED(GetCFURL(&url))) {
    nsresult rv = CocoaFileUtils::OpenURL(url);
    ::CFRelease(url);
    return rv;
  }
  return NS_ERROR_FAILURE;
#else
  return NS_ERROR_FAILURE;
#endif
}

nsresult
NS_NewNativeLocalFile(const nsACString& aPath, bool aFollowSymlinks,
                      nsIFile** aResult)
{
  Din_Go(902,2048);RefPtr<nsLocalFile> file = new nsLocalFile();

  file->SetFollowLinks(aFollowSymlinks);

  Din_Go(906,2048);if (!aPath.IsEmpty()) {
    Din_Go(903,2048);nsresult rv = file->InitWithNativePath(aPath);
    Din_Go(905,2048);if (NS_FAILED(rv)) {
      {__IAENUM__ nsresult  ReplaceReturn9 = rv; Din_Go(904,2048); return ReplaceReturn9;}
    }
  }
  Din_Go(907,2048);file.forget(aResult);
  {__IAENUM__ nsresult  ReplaceReturn8 = NS_OK; Din_Go(908,2048); return ReplaceReturn8;}
}

//-----------------------------------------------------------------------------
// unicode support
//-----------------------------------------------------------------------------

#define SET_UCS(func, ucsArg) \
    { \
        nsAutoCString buf; \
        nsresult rv = NS_CopyUnicodeToNative(ucsArg, buf); \
        if (NS_FAILED(rv)) \
            return rv; \
        return (func)(buf); \
    }

#define GET_UCS(func, ucsArg) \
    { \
        nsAutoCString buf; \
        nsresult rv = (func)(buf); \
        if (NS_FAILED(rv)) return rv; \
        return NS_CopyNativeToUnicode(buf, ucsArg); \
    }

#define SET_UCS_2ARGS_2(func, opaqueArg, ucsArg) \
    { \
        nsAutoCString buf; \
        nsresult rv = NS_CopyUnicodeToNative(ucsArg, buf); \
        if (NS_FAILED(rv)) \
            return rv; \
        return (func)(opaqueArg, buf); \
    }

// Unicode interface Wrapper
nsresult
nsLocalFile::InitWithPath(const nsAString& aFilePath)
{
Din_Go(909,2048);  SET_UCS(InitWithNativePath, aFilePath);
}
nsresult
nsLocalFile::Append(const nsAString& aNode)
{
Din_Go(910,2048);  SET_UCS(AppendNative, aNode);
}
nsresult
nsLocalFile::AppendRelativePath(const nsAString& aNode)
{
Din_Go(911,2048);  SET_UCS(AppendRelativeNativePath, aNode);
}
nsresult
nsLocalFile::GetLeafName(nsAString& aLeafName)
{
Din_Go(912,2048);  GET_UCS(GetNativeLeafName, aLeafName);
}
nsresult
nsLocalFile::SetLeafName(const nsAString& aLeafName)
{
Din_Go(913,2048);  SET_UCS(SetNativeLeafName, aLeafName);
}
nsresult
nsLocalFile::GetPath(nsAString& aResult)
{
  {__IAENUM__ nsresult  ReplaceReturn7 = NS_CopyNativeToUnicode(mPath, aResult); Din_Go(914,2048); return ReplaceReturn7;}
}
nsresult
nsLocalFile::CopyTo(nsIFile* aNewParentDir, const nsAString& aNewName)
{
Din_Go(915,2048);  SET_UCS_2ARGS_2(CopyToNative , aNewParentDir, aNewName);
}
nsresult
nsLocalFile::CopyToFollowingLinks(nsIFile* aNewParentDir,
                                  const nsAString& aNewName)
{
Din_Go(916,2048);  SET_UCS_2ARGS_2(CopyToFollowingLinksNative , aNewParentDir, aNewName);
}
nsresult
nsLocalFile::MoveTo(nsIFile* aNewParentDir, const nsAString& aNewName)
{
Din_Go(917,2048);  SET_UCS_2ARGS_2(MoveToNative, aNewParentDir, aNewName);
}

NS_IMETHODIMP
nsLocalFile::RenameTo(nsIFile* aNewParentDir, const nsAString& aNewName)
{
Din_Go(918,2048);  SET_UCS_2ARGS_2(RenameToNative, aNewParentDir, aNewName);
}

NS_IMETHODIMP
nsLocalFile::RenameToNative(nsIFile* aNewParentDir, const nsACString& aNewName)
{
  Din_Go(919,2048);nsresult rv;

  // check to make sure that this has been initialized properly
  CHECK_mPath();

  // check to make sure that we have a new parent
  nsAutoCString newPathName;
  rv = GetNativeTargetPathName(aNewParentDir, aNewName, newPathName);
  Din_Go(921,2048);if (NS_FAILED(rv)) {
    {__IAENUM__ nsresult  ReplaceReturn6 = rv; Din_Go(920,2048); return ReplaceReturn6;}
  }

  // try for atomic rename
  Din_Go(925,2048);if (rename(mPath.get(), newPathName.get()) < 0) {
#ifdef VMS
    if (errno == EXDEV || errno == ENXIO) {
#else
    Din_Go(922,2048);if (errno == EXDEV) {
#endif
      Din_Go(923,2048);rv = NS_ERROR_FILE_ACCESS_DENIED;
    } else {
      Din_Go(924,2048);rv = NSRESULT_FOR_ERRNO();
    }
  }

  {__IAENUM__ nsresult  ReplaceReturn5 = rv; Din_Go(926,2048); return ReplaceReturn5;}
}

nsresult
nsLocalFile::GetTarget(nsAString& aResult)
{
Din_Go(927,2048);  GET_UCS(GetNativeTarget, aResult);
}

// nsIHashable

NS_IMETHODIMP
nsLocalFile::Equals(nsIHashable* aOther, bool* aResult)
{
  Din_Go(928,2048);nsCOMPtr<nsIFile> otherFile(do_QueryInterface(aOther));
  Din_Go(931,2048);if (!otherFile) {
    Din_Go(929,2048);*aResult = false;
    {__IAENUM__ nsresult  ReplaceReturn4 = NS_OK; Din_Go(930,2048); return ReplaceReturn4;}
  }

  {__IAENUM__ nsresult  ReplaceReturn3 = Equals(otherFile, aResult); Din_Go(932,2048); return ReplaceReturn3;}
}

NS_IMETHODIMP
nsLocalFile::GetHashCode(uint32_t* aResult)
{
  Din_Go(933,2048);*aResult = HashString(mPath);
  {__IAENUM__ nsresult  ReplaceReturn2 = NS_OK; Din_Go(934,2048); return ReplaceReturn2;}
}

nsresult
NS_NewLocalFile(const nsAString& aPath, bool aFollowLinks, nsIFile** aResult)
{
  Din_Go(935,2048);nsAutoCString buf;
  nsresult rv = NS_CopyUnicodeToNative(aPath, buf);
  Din_Go(937,2048);if (NS_FAILED(rv)) {
    {__IAENUM__ nsresult  ReplaceReturn1 = rv; Din_Go(936,2048); return ReplaceReturn1;}
  }
  {__IAENUM__ nsresult  ReplaceReturn0 = NS_NewNativeLocalFile(buf, aFollowLinks, aResult); Din_Go(938,2048); return ReplaceReturn0;}
}

//-----------------------------------------------------------------------------
// global init/shutdown
//-----------------------------------------------------------------------------

void
nsLocalFile::GlobalInit()
{
Din_Go(939,2048);Din_Go(940,2048);}

void
nsLocalFile::GlobalShutdown()
{
Din_Go(941,2048);Din_Go(942,2048);}

// nsILocalFileMac

#ifdef MOZ_WIDGET_COCOA

static nsresult MacErrorMapper(OSErr inErr)
{
  nsresult outErr;

  switch (inErr) {
    case noErr:
      outErr = NS_OK;
      break;

    case fnfErr:
    case afpObjectNotFound:
    case afpDirNotFound:
      outErr = NS_ERROR_FILE_NOT_FOUND;
      break;

    case dupFNErr:
    case afpObjectExists:
      outErr = NS_ERROR_FILE_ALREADY_EXISTS;
      break;

    case dskFulErr:
    case afpDiskFull:
      outErr = NS_ERROR_FILE_DISK_FULL;
      break;

    case fLckdErr:
    case afpVolLocked:
      outErr = NS_ERROR_FILE_IS_LOCKED;
      break;

    case afpAccessDenied:
      outErr = NS_ERROR_FILE_ACCESS_DENIED;
      break;

    case afpDirNotEmpty:
      outErr = NS_ERROR_FILE_DIR_NOT_EMPTY;
      break;

    // Can't find good map for some
    case bdNamErr:
      outErr = NS_ERROR_FAILURE;
      break;

    default:
      outErr = NS_ERROR_FAILURE;
      break;
  }

  return outErr;
}

static nsresult CFStringReftoUTF8(CFStringRef aInStrRef, nsACString& aOutStr)
{
  // first see if the conversion would succeed and find the length of the result
  CFIndex usedBufLen, inStrLen = ::CFStringGetLength(aInStrRef);
  CFIndex charsConverted = ::CFStringGetBytes(aInStrRef, CFRangeMake(0, inStrLen),
                                              kCFStringEncodingUTF8, 0, false,
                                              nullptr, 0, &usedBufLen);
  if (charsConverted == inStrLen) {
    // all characters converted, do the actual conversion
    aOutStr.SetLength(usedBufLen);
    if (aOutStr.Length() != (unsigned int)usedBufLen) {
      return NS_ERROR_OUT_OF_MEMORY;
    }
    UInt8* buffer = (UInt8*)aOutStr.BeginWriting();
    ::CFStringGetBytes(aInStrRef, CFRangeMake(0, inStrLen), kCFStringEncodingUTF8,
                       0, false, buffer, usedBufLen, &usedBufLen);
    return NS_OK;
  }

  return NS_ERROR_FAILURE;
}

NS_IMETHODIMP
nsLocalFile::InitWithCFURL(CFURLRef aCFURL)
{
  UInt8 path[PATH_MAX];
  if (::CFURLGetFileSystemRepresentation(aCFURL, true, path, PATH_MAX)) {
    nsDependentCString nativePath((char*)path);
    return InitWithNativePath(nativePath);
  }

  return NS_ERROR_FAILURE;
}

NS_IMETHODIMP
nsLocalFile::InitWithFSRef(const FSRef* aFSRef)
{
  if (NS_WARN_IF(!aFSRef)) {
    return NS_ERROR_INVALID_ARG;
  }

  CFURLRef newURLRef = ::CFURLCreateFromFSRef(kCFAllocatorDefault, aFSRef);
  if (newURLRef) {
    nsresult rv = InitWithCFURL(newURLRef);
    ::CFRelease(newURLRef);
    return rv;
  }

  return NS_ERROR_FAILURE;
}

NS_IMETHODIMP
nsLocalFile::GetCFURL(CFURLRef* aResult)
{
  CHECK_mPath();

  bool isDir;
  IsDirectory(&isDir);
  *aResult = ::CFURLCreateFromFileSystemRepresentation(kCFAllocatorDefault,
                                                       (UInt8*)mPath.get(),
                                                       mPath.Length(),
                                                       isDir);

  return (*aResult ? NS_OK : NS_ERROR_FAILURE);
}

NS_IMETHODIMP
nsLocalFile::GetFSRef(FSRef* aResult)
{
  if (NS_WARN_IF(!aResult)) {
    return NS_ERROR_INVALID_ARG;
  }

  nsresult rv = NS_ERROR_FAILURE;

  CFURLRef url = nullptr;
  if (NS_SUCCEEDED(GetCFURL(&url))) {
    if (::CFURLGetFSRef(url, aResult)) {
      rv = NS_OK;
    }
    ::CFRelease(url);
  }

  return rv;
}

NS_IMETHODIMP
nsLocalFile::GetFSSpec(FSSpec* aResult)
{
  if (NS_WARN_IF(!aResult)) {
    return NS_ERROR_INVALID_ARG;
  }

  FSRef fsRef;
  nsresult rv = GetFSRef(&fsRef);
  if (NS_SUCCEEDED(rv)) {
    OSErr err = ::FSGetCatalogInfo(&fsRef, kFSCatInfoNone, nullptr, nullptr,
                                   aResult, nullptr);
    return MacErrorMapper(err);
  }

  return rv;
}

NS_IMETHODIMP
nsLocalFile::GetFileSizeWithResFork(int64_t* aFileSizeWithResFork)
{
  if (NS_WARN_IF(!aFileSizeWithResFork)) {
    return NS_ERROR_INVALID_ARG;
  }

  FSRef fsRef;
  nsresult rv = GetFSRef(&fsRef);
  if (NS_FAILED(rv)) {
    return rv;
  }

  FSCatalogInfo catalogInfo;
  OSErr err = ::FSGetCatalogInfo(&fsRef, kFSCatInfoDataSizes + kFSCatInfoRsrcSizes,
                                 &catalogInfo, nullptr, nullptr, nullptr);
  if (err != noErr) {
    return MacErrorMapper(err);
  }

  *aFileSizeWithResFork =
    catalogInfo.dataLogicalSize + catalogInfo.rsrcLogicalSize;
  return NS_OK;
}

NS_IMETHODIMP
nsLocalFile::GetFileType(OSType* aFileType)
{
  CFURLRef url;
  if (NS_SUCCEEDED(GetCFURL(&url))) {
    nsresult rv = CocoaFileUtils::GetFileTypeCode(url, aFileType);
    ::CFRelease(url);
    return rv;
  }
  return NS_ERROR_FAILURE;
}

NS_IMETHODIMP
nsLocalFile::SetFileType(OSType aFileType)
{
  CFURLRef url;
  if (NS_SUCCEEDED(GetCFURL(&url))) {
    nsresult rv = CocoaFileUtils::SetFileTypeCode(url, aFileType);
    ::CFRelease(url);
    return rv;
  }
  return NS_ERROR_FAILURE;
}

NS_IMETHODIMP
nsLocalFile::GetFileCreator(OSType* aFileCreator)
{
  CFURLRef url;
  if (NS_SUCCEEDED(GetCFURL(&url))) {
    nsresult rv = CocoaFileUtils::GetFileCreatorCode(url, aFileCreator);
    ::CFRelease(url);
    return rv;
  }
  return NS_ERROR_FAILURE;
}

NS_IMETHODIMP
nsLocalFile::SetFileCreator(OSType aFileCreator)
{
  CFURLRef url;
  if (NS_SUCCEEDED(GetCFURL(&url))) {
    nsresult rv = CocoaFileUtils::SetFileCreatorCode(url, aFileCreator);
    ::CFRelease(url);
    return rv;
  }
  return NS_ERROR_FAILURE;
}

NS_IMETHODIMP
nsLocalFile::LaunchWithDoc(nsIFile* aDocToLoad, bool aLaunchInBackground)
{
  bool isExecutable;
  nsresult rv = IsExecutable(&isExecutable);
  if (NS_FAILED(rv)) {
    return rv;
  }
  if (!isExecutable) {
    return NS_ERROR_FILE_EXECUTION_FAILED;
  }

  FSRef appFSRef, docFSRef;
  rv = GetFSRef(&appFSRef);
  if (NS_FAILED(rv)) {
    return rv;
  }

  if (aDocToLoad) {
    nsCOMPtr<nsILocalFileMac> macDoc = do_QueryInterface(aDocToLoad);
    rv = macDoc->GetFSRef(&docFSRef);
    if (NS_FAILED(rv)) {
      return rv;
    }
  }

  LSLaunchFlags theLaunchFlags = kLSLaunchDefaults;
  LSLaunchFSRefSpec thelaunchSpec;

  if (aLaunchInBackground) {
    theLaunchFlags |= kLSLaunchDontSwitch;
  }
  memset(&thelaunchSpec, 0, sizeof(LSLaunchFSRefSpec));

  thelaunchSpec.appRef = &appFSRef;
  if (aDocToLoad) {
    thelaunchSpec.numDocs = 1;
    thelaunchSpec.itemRefs = &docFSRef;
  }
  thelaunchSpec.launchFlags = theLaunchFlags;

  OSErr err = ::LSOpenFromRefSpec(&thelaunchSpec, nullptr);
  if (err != noErr) {
    return MacErrorMapper(err);
  }

  return NS_OK;
}

NS_IMETHODIMP
nsLocalFile::OpenDocWithApp(nsIFile* aAppToOpenWith, bool aLaunchInBackground)
{
  FSRef docFSRef;
  nsresult rv = GetFSRef(&docFSRef);
  if (NS_FAILED(rv)) {
    return rv;
  }

  if (!aAppToOpenWith) {
    OSErr err = ::LSOpenFSRef(&docFSRef, nullptr);
    return MacErrorMapper(err);
  }

  nsCOMPtr<nsILocalFileMac> appFileMac = do_QueryInterface(aAppToOpenWith, &rv);
  if (!appFileMac) {
    return rv;
  }

  bool isExecutable;
  rv = appFileMac->IsExecutable(&isExecutable);
  if (NS_FAILED(rv)) {
    return rv;
  }
  if (!isExecutable) {
    return NS_ERROR_FILE_EXECUTION_FAILED;
  }

  FSRef appFSRef;
  rv = appFileMac->GetFSRef(&appFSRef);
  if (NS_FAILED(rv)) {
    return rv;
  }

  LSLaunchFlags theLaunchFlags = kLSLaunchDefaults;
  LSLaunchFSRefSpec thelaunchSpec;

  if (aLaunchInBackground) {
    theLaunchFlags |= kLSLaunchDontSwitch;
  }
  memset(&thelaunchSpec, 0, sizeof(LSLaunchFSRefSpec));

  thelaunchSpec.appRef = &appFSRef;
  thelaunchSpec.numDocs = 1;
  thelaunchSpec.itemRefs = &docFSRef;
  thelaunchSpec.launchFlags = theLaunchFlags;

  OSErr err = ::LSOpenFromRefSpec(&thelaunchSpec, nullptr);
  if (err != noErr) {
    return MacErrorMapper(err);
  }

  return NS_OK;
}

NS_IMETHODIMP
nsLocalFile::IsPackage(bool* aResult)
{
  if (NS_WARN_IF(!aResult)) {
    return NS_ERROR_INVALID_ARG;
  }
  *aResult = false;

  CFURLRef url;
  nsresult rv = GetCFURL(&url);
  if (NS_FAILED(rv)) {
    return rv;
  }

  LSItemInfoRecord info;
  OSStatus status = ::LSCopyItemInfoForURL(url, kLSRequestBasicFlagsOnly, &info);

  ::CFRelease(url);

  if (status != noErr) {
    return NS_ERROR_FAILURE;
  }

  *aResult = !!(info.flags & kLSItemInfoIsPackage);

  return NS_OK;
}

NS_IMETHODIMP
nsLocalFile::GetBundleDisplayName(nsAString& aOutBundleName)
{
  bool isPackage = false;
  nsresult rv = IsPackage(&isPackage);
  if (NS_FAILED(rv) || !isPackage) {
    return NS_ERROR_FAILURE;
  }

  nsAutoString name;
  rv = GetLeafName(name);
  if (NS_FAILED(rv)) {
    return rv;
  }

  int32_t length = name.Length();
  if (Substring(name, length - 4, length).EqualsLiteral(".app")) {
    // 4 characters in ".app"
    aOutBundleName = Substring(name, 0, length - 4);
  } else {
    aOutBundleName = name;
  }

  return NS_OK;
}

NS_IMETHODIMP
nsLocalFile::GetBundleIdentifier(nsACString& aOutBundleIdentifier)
{
  nsresult rv = NS_ERROR_FAILURE;

  CFURLRef urlRef;
  if (NS_SUCCEEDED(GetCFURL(&urlRef))) {
    CFBundleRef bundle = ::CFBundleCreate(nullptr, urlRef);
    if (bundle) {
      CFStringRef bundleIdentifier = ::CFBundleGetIdentifier(bundle);
      if (bundleIdentifier) {
        rv = CFStringReftoUTF8(bundleIdentifier, aOutBundleIdentifier);
      }
      ::CFRelease(bundle);
    }
    ::CFRelease(urlRef);
  }

  return rv;
}

NS_IMETHODIMP
nsLocalFile::GetBundleContentsLastModifiedTime(int64_t* aLastModTime)
{
  CHECK_mPath();
  if (NS_WARN_IF(!aLastModTime)) {
    return NS_ERROR_INVALID_ARG;
  }

  bool isPackage = false;
  nsresult rv = IsPackage(&isPackage);
  if (NS_FAILED(rv) || !isPackage) {
    return GetLastModifiedTime(aLastModTime);
  }

  nsAutoCString infoPlistPath(mPath);
  infoPlistPath.AppendLiteral("/Contents/Info.plist");
  PRFileInfo64 info;
  if (PR_GetFileInfo64(infoPlistPath.get(), &info) != PR_SUCCESS) {
    return GetLastModifiedTime(aLastModTime);
  }
  int64_t modTime = int64_t(info.modifyTime);
  if (modTime == 0) {
    *aLastModTime = 0;
  } else {
    *aLastModTime = modTime / int64_t(PR_USEC_PER_MSEC);
  }

  return NS_OK;
}

NS_IMETHODIMP nsLocalFile::InitWithFile(nsIFile* aFile)
{
  if (NS_WARN_IF(!aFile)) {
    return NS_ERROR_INVALID_ARG;
  }

  nsAutoCString nativePath;
  nsresult rv = aFile->GetNativePath(nativePath);
  if (NS_FAILED(rv)) {
    return rv;
  }

  return InitWithNativePath(nativePath);
}

nsresult
NS_NewLocalFileWithFSRef(const FSRef* aFSRef, bool aFollowLinks,
                         nsILocalFileMac** aResult)
{
  RefPtr<nsLocalFile> file = new nsLocalFile();

  file->SetFollowLinks(aFollowLinks);

  nsresult rv = file->InitWithFSRef(aFSRef);
  if (NS_FAILED(rv)) {
    return rv;
  }
  file.forget(aResult);
  return NS_OK;
}

nsresult
NS_NewLocalFileWithCFURL(const CFURLRef aURL, bool aFollowLinks,
                         nsILocalFileMac** aResult)
{
  RefPtr<nsLocalFile> file = new nsLocalFile();

  file->SetFollowLinks(aFollowLinks);

  nsresult rv = file->InitWithCFURL(aURL);
  if (NS_FAILED(rv)) {
    return rv;
  }
  file.forget(aResult);
  return NS_OK;
}

#endif
