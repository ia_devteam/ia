#include "var/tmp/sensor.h"
/* Copyright (C) 2007-2008 Jean-Marc Valin
   Copyright (C) 2008      Thorvald Natvig
      
   File: resample.c
   Arbitrary resampling code

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:

   1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

   3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
   IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
   DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
   INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
*/

/*
   The design goals of this code are:
      - Very fast algorithm
      - SIMD-friendly algorithm
      - Low memory requirement
      - Good *perceptual* quality (and not best SNR)

   Warning: This resampler is relatively new. Although I think I got rid of 
   all the major bugs and I don't expect the API to change anymore, there
   may be something I've missed. So use with caution.

   This algorithm is based on this original resampling algorithm:
   Smith, Julius O. Digital Audio Resampling Home Page
   Center for Computer Research in Music and Acoustics (CCRMA), 
   Stanford University, 2007.
   Web published at http://www-ccrma.stanford.edu/~jos/resample/.

   There is one main difference, though. This resampler uses cubic 
   interpolation instead of linear interpolation in the above paper. This
   makes the table much smaller and makes it possible to compute that table
   on a per-stream basis. In turn, being able to tweak the table for each 
   stream makes it possible to both reduce complexity on simple ratios 
   (e.g. 2/3), and get rid of the rounding operations in the inner loop. 
   The latter both reduces CPU time and makes the algorithm more SIMD-friendly.
*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define RESAMPLE_HUGEMEM 1

#ifdef OUTSIDE_SPEEX
#include <stdlib.h>
static void *speex_alloc (int size) {{void * ReplaceReturn238 = calloc(size,1); Din_Go(1,2048); return ReplaceReturn238;}}
static void *speex_realloc (void *ptr, int size) {{void * ReplaceReturn237 = realloc(ptr, size); Din_Go(2,2048); return ReplaceReturn237;}}
static void speex_free (void *ptr) {Din_Go(3,2048);free(ptr);Din_Go(4,2048);}
#include "speex_resampler.h"
#include "arch.h"
#else /* OUTSIDE_SPEEX */

#include "speex/speex_resampler.h"
#include "arch.h"
#include "os_support.h"
#endif /* OUTSIDE_SPEEX */

#include "stack_alloc.h"
#include <math.h>
#include <limits.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#ifdef FIXED_POINT
#define WORD2INT(x) ((x) < -32767 ? -32768 : ((x) > 32766 ? 32767 : (x)))  
#else
#define WORD2INT(x) ((x) < -32767.5f ? -32768 : ((x) > 32766.5f ? 32767 : floor(.5+(x))))  
#endif
               
#define IMAX(a,b) ((a) > (b) ? (a) : (b))
#define IMIN(a,b) ((a) < (b) ? (a) : (b))

#ifndef NULL
#define NULL 0
#endif

#include "simd_detect.h"

/* Numer of elements to allocate on the stack */
#ifdef VAR_ARRAYS
#define FIXED_STACK_ALLOC 8192
#else
#define FIXED_STACK_ALLOC 1024
#endif

typedef int (*resampler_basic_func)(SpeexResamplerState *, spx_uint32_t , const spx_word16_t *, spx_uint32_t *, spx_word16_t *, spx_uint32_t *);

struct SpeexResamplerState_ {
   spx_uint32_t in_rate;
   spx_uint32_t out_rate;
   spx_uint32_t num_rate;
   spx_uint32_t den_rate;
   
   int    quality;
   spx_uint32_t nb_channels;
   spx_uint32_t filt_len;
   spx_uint32_t mem_alloc_size;
   spx_uint32_t buffer_size;
   int          int_advance;
   int          frac_advance;
   float  cutoff;
   spx_uint32_t oversample;
   int          initialised;
   int          started;
   
   /* These are per-channel */
   spx_int32_t  *last_sample;
   spx_uint32_t *samp_frac_num;
   spx_uint32_t *magic_samples;
   
   spx_word16_t *mem;
   spx_word16_t *sinc_table;
   spx_uint32_t sinc_table_length;
   resampler_basic_func resampler_ptr;
         
   int    in_stride;
   int    out_stride;
} ;

static const double kaiser12_table[68] = {
   0.99859849, 1.00000000, 0.99859849, 0.99440475, 0.98745105, 0.97779076,
   0.96549770, 0.95066529, 0.93340547, 0.91384741, 0.89213598, 0.86843014,
   0.84290116, 0.81573067, 0.78710866, 0.75723148, 0.72629970, 0.69451601,
   0.66208321, 0.62920216, 0.59606986, 0.56287762, 0.52980938, 0.49704014,
   0.46473455, 0.43304576, 0.40211431, 0.37206735, 0.34301800, 0.31506490,
   0.28829195, 0.26276832, 0.23854851, 0.21567274, 0.19416736, 0.17404546,
   0.15530766, 0.13794294, 0.12192957, 0.10723616, 0.09382272, 0.08164178,
   0.07063950, 0.06075685, 0.05193064, 0.04409466, 0.03718069, 0.03111947,
   0.02584161, 0.02127838, 0.01736250, 0.01402878, 0.01121463, 0.00886058,
   0.00691064, 0.00531256, 0.00401805, 0.00298291, 0.00216702, 0.00153438,
   0.00105297, 0.00069463, 0.00043489, 0.00025272, 0.00013031, 0.0000527734,
   0.00001000, 0.00000000};
/*
static const double kaiser12_table[36] = {
   0.99440475, 1.00000000, 0.99440475, 0.97779076, 0.95066529, 0.91384741,
   0.86843014, 0.81573067, 0.75723148, 0.69451601, 0.62920216, 0.56287762,
   0.49704014, 0.43304576, 0.37206735, 0.31506490, 0.26276832, 0.21567274,
   0.17404546, 0.13794294, 0.10723616, 0.08164178, 0.06075685, 0.04409466,
   0.03111947, 0.02127838, 0.01402878, 0.00886058, 0.00531256, 0.00298291,
   0.00153438, 0.00069463, 0.00025272, 0.0000527734, 0.00000500, 0.00000000};
*/
static const double kaiser10_table[36] = {
   0.99537781, 1.00000000, 0.99537781, 0.98162644, 0.95908712, 0.92831446,
   0.89005583, 0.84522401, 0.79486424, 0.74011713, 0.68217934, 0.62226347,
   0.56155915, 0.50119680, 0.44221549, 0.38553619, 0.33194107, 0.28205962,
   0.23636152, 0.19515633, 0.15859932, 0.12670280, 0.09935205, 0.07632451,
   0.05731132, 0.04193980, 0.02979584, 0.02044510, 0.01345224, 0.00839739,
   0.00488951, 0.00257636, 0.00115101, 0.00035515, 0.00000000, 0.00000000};

static const double kaiser8_table[36] = {
   0.99635258, 1.00000000, 0.99635258, 0.98548012, 0.96759014, 0.94302200,
   0.91223751, 0.87580811, 0.83439927, 0.78875245, 0.73966538, 0.68797126,
   0.63451750, 0.58014482, 0.52566725, 0.47185369, 0.41941150, 0.36897272,
   0.32108304, 0.27619388, 0.23465776, 0.19672670, 0.16255380, 0.13219758,
   0.10562887, 0.08273982, 0.06335451, 0.04724088, 0.03412321, 0.02369490,
   0.01563093, 0.00959968, 0.00527363, 0.00233883, 0.00050000, 0.00000000};
   
static const double kaiser6_table[36] = {
   0.99733006, 1.00000000, 0.99733006, 0.98935595, 0.97618418, 0.95799003,
   0.93501423, 0.90755855, 0.87598009, 0.84068475, 0.80211977, 0.76076565,
   0.71712752, 0.67172623, 0.62508937, 0.57774224, 0.53019925, 0.48295561,
   0.43647969, 0.39120616, 0.34752997, 0.30580127, 0.26632152, 0.22934058,
   0.19505503, 0.16360756, 0.13508755, 0.10953262, 0.08693120, 0.06722600,
   0.05031820, 0.03607231, 0.02432151, 0.01487334, 0.00752000, 0.00000000};

struct FuncDef {
   const double *table;
   int oversample;
};
      
static const struct FuncDef _KAISER12 = {kaiser12_table, 64};
#define KAISER12 (&_KAISER12)
/*static struct FuncDef _KAISER12 = {kaiser12_table, 32};
#define KAISER12 (&_KAISER12)*/
static const struct FuncDef _KAISER10 = {kaiser10_table, 32};
#define KAISER10 (&_KAISER10)
static const struct FuncDef _KAISER8 = {kaiser8_table, 32};
#define KAISER8 (&_KAISER8)
static const struct FuncDef _KAISER6 = {kaiser6_table, 32};
#define KAISER6 (&_KAISER6)

struct QualityMapping {
   int base_length;
   int oversample;
   float downsample_bandwidth;
   float upsample_bandwidth;
   const struct FuncDef *window_func;
};


/* This table maps conversion quality to internal parameters. There are two
   reasons that explain why the up-sampling bandwidth is larger than the 
   down-sampling bandwidth:
   1) When up-sampling, we can assume that the spectrum is already attenuated
      close to the Nyquist rate (from an A/D or a previous resampling filter)
   2) Any aliasing that occurs very close to the Nyquist rate will be masked
      by the sinusoids/noise just below the Nyquist rate (guaranteed only for
      up-sampling).
*/
static const struct QualityMapping quality_map[11] = {
   {  8,  4, 0.830f, 0.860f, KAISER6 }, /* Q0 */
   { 16,  4, 0.850f, 0.880f, KAISER6 }, /* Q1 */
   { 32,  4, 0.882f, 0.910f, KAISER6 }, /* Q2 */  /* 82.3% cutoff ( ~60 dB stop) 6  */
   { 48,  8, 0.895f, 0.917f, KAISER8 }, /* Q3 */  /* 84.9% cutoff ( ~80 dB stop) 8  */
   { 64,  8, 0.921f, 0.940f, KAISER8 }, /* Q4 */  /* 88.7% cutoff ( ~80 dB stop) 8  */
   { 80, 16, 0.922f, 0.940f, KAISER10}, /* Q5 */  /* 89.1% cutoff (~100 dB stop) 10 */
   { 96, 16, 0.940f, 0.945f, KAISER10}, /* Q6 */  /* 91.5% cutoff (~100 dB stop) 10 */
   {128, 16, 0.950f, 0.950f, KAISER10}, /* Q7 */  /* 93.1% cutoff (~100 dB stop) 10 */
   {160, 16, 0.960f, 0.960f, KAISER10}, /* Q8 */  /* 94.5% cutoff (~100 dB stop) 10 */
   {192, 32, 0.968f, 0.968f, KAISER12}, /* Q9 */  /* 95.5% cutoff (~100 dB stop) 10 */
   {256, 32, 0.975f, 0.975f, KAISER12}, /* Q10 */ /* 96.6% cutoff (~100 dB stop) 10 */
};
/*8,24,40,56,80,104,128,160,200,256,320*/
static double compute_func(float x, const struct FuncDef *func)
{
   Din_Go(5,2048);float y, frac;
   double interp[4];
   int ind; 
   y = x*func->oversample;
   ind = (int)floor(y);
   frac = (y-ind);
   /* CSE with handle the repeated powers */
   interp[3] =  -0.1666666667*frac + 0.1666666667*(frac*frac*frac);
   interp[2] = frac + 0.5*(frac*frac) - 0.5*(frac*frac*frac);
   /*interp[2] = 1.f - 0.5f*frac - frac*frac + 0.5f*frac*frac*frac;*/
   interp[0] = -0.3333333333*frac + 0.5*(frac*frac) - 0.1666666667*(frac*frac*frac);
   /* Just to make sure we don't have rounding problems */
   interp[1] = 1.f-interp[3]-interp[2]-interp[0];
   
   /*sum = frac*accum[1] + (1-frac)*accum[2];*/
   {double  ReplaceReturn236 = interp[0]*func->table[ind] + interp[1]*func->table[ind+1] + interp[2]*func->table[ind+2] + interp[3]*func->table[ind+3]; Din_Go(6,2048); return ReplaceReturn236;}
}

#if 0
#include <stdio.h>
int main(int argc, char **argv)
{
   int i;
   for (i=0;i<256;i++)
   {
      printf ("%f\n", compute_func(i/256., KAISER12));
   }
   return 0;
}
#endif

#ifdef FIXED_POINT
/* The slow way of computing a sinc for the table. Should improve that some day */
static spx_word16_t sinc(float cutoff, float x, int N, const struct FuncDef *window_func)
{
   /*fprintf (stderr, "%f ", x);*/
   float xx = x * cutoff;
   if (fabs(x)<1e-6f)
      return WORD2INT(32768.*cutoff);
   else if (fabs(x) > .5f*N)
      return 0;
   /*FIXME: Can it really be any slower than this? */
   return WORD2INT(32768.*cutoff*sin(M_PI*xx)/(M_PI*xx) * compute_func(fabs(2.*x/N), window_func));
}
#else
/* The slow way of computing a sinc for the table. Should improve that some day */
static spx_word16_t sinc(float cutoff, float x, int N, const struct FuncDef *window_func)
{
   /*fprintf (stderr, "%f ", x);*/
   Din_Go(7,2048);float xx = x * cutoff;
   Din_Go(11,2048);if (fabs(x)<1e-6)
      {/*37*/{spx_word16_t  ReplaceReturn235 = cutoff; Din_Go(8,2048); return ReplaceReturn235;}/*38*/}
   else {/*39*/Din_Go(9,2048);if (fabs(x) > .5*N)
      {/*41*/{spx_word16_t  ReplaceReturn234 = 0; Din_Go(10,2048); return ReplaceReturn234;}/*42*/}/*40*/}
   /*FIXME: Can it really be any slower than this? */
   {spx_word16_t  ReplaceReturn233 = cutoff*sin(M_PI*xx)/(M_PI*xx) * compute_func(fabs(2.*x/N), window_func); Din_Go(12,2048); return ReplaceReturn233;}
}
#endif

#ifdef FIXED_POINT
static void cubic_coef(spx_word16_t x, spx_word16_t interp[4])
{
   /* Compute interpolation coefficients. I'm not sure whether this corresponds to cubic interpolation
   but I know it's MMSE-optimal on a sinc */
   spx_word16_t x2, x3;
   x2 = MULT16_16_P15(x, x);
   x3 = MULT16_16_P15(x, x2);
   interp[0] = PSHR32(MULT16_16(QCONST16(-0.16667f, 15),x) + MULT16_16(QCONST16(0.16667f, 15),x3),15);
   interp[1] = EXTRACT16(EXTEND32(x) + SHR32(SUB32(EXTEND32(x2),EXTEND32(x3)),1));
   interp[3] = PSHR32(MULT16_16(QCONST16(-0.33333f, 15),x) + MULT16_16(QCONST16(.5f,15),x2) - MULT16_16(QCONST16(0.16667f, 15),x3),15);
   /* Just to make sure we don't have rounding problems */
   interp[2] = Q15_ONE-interp[0]-interp[1]-interp[3];
   if (interp[2]<32767)
      interp[2]+=1;
}
#else
static void cubic_coef(spx_word16_t frac, spx_word16_t interp[4])
{
   /* Compute interpolation coefficients. I'm not sure whether this corresponds to cubic interpolation
   but I know it's MMSE-optimal on a sinc */
   Din_Go(13,2048);interp[0] =  -0.16667f*frac + 0.16667f*frac*frac*frac;
   interp[1] = frac + 0.5f*frac*frac - 0.5f*frac*frac*frac;
   /*interp[2] = 1.f - 0.5f*frac - frac*frac + 0.5f*frac*frac*frac;*/
   interp[3] = -0.33333f*frac + 0.5f*frac*frac - 0.16667f*frac*frac*frac;
   /* Just to make sure we don't have rounding problems */
   interp[2] = 1.-interp[0]-interp[1]-interp[3];
Din_Go(14,2048);}
#endif

static int resampler_basic_direct_single(SpeexResamplerState *st, spx_uint32_t channel_index, const spx_word16_t *in, spx_uint32_t *in_len, spx_word16_t *out, spx_uint32_t *out_len)
{
   Din_Go(15,2048);const int N = st->filt_len;
   int out_sample = 0;
   int last_sample = st->last_sample[channel_index];
   spx_uint32_t samp_frac_num = st->samp_frac_num[channel_index];
   const spx_word16_t *sinc_table = st->sinc_table;
   const int out_stride = st->out_stride;
   const int int_advance = st->int_advance;
   const int frac_advance = st->frac_advance;
   const spx_uint32_t den_rate = st->den_rate;
   spx_word32_t sum;

   Din_Go(25,2048);while (!(last_sample >= (spx_int32_t)*in_len || out_sample >= (spx_int32_t)*out_len))
   {
      Din_Go(16,2048);const spx_word16_t *sinct = & sinc_table[samp_frac_num*N];
      const spx_word16_t *iptr = & in[last_sample];

#ifdef OVERRIDE_INNER_PRODUCT_SINGLE
      Din_Go(21,2048);if (!moz_speex_have_single_simd()) {
#endif
      Din_Go(17,2048);int j;
      sum = 0;
      Din_Go(18,2048);for(j=0;j<N;j++) sum += MULT16_16(sinct[j], iptr[j]);

/*    This code is slower on most DSPs which have only 2 accumulators.
      Plus this this forces truncation to 32 bits and you lose the HW guard bits.
      I think we can trust the compiler and let it vectorize and/or unroll itself.
      spx_word32_t accum[4] = {0,0,0,0};
      for(j=0;j<N;j+=4) {
        accum[0] += MULT16_16(sinct[j], iptr[j]);
        accum[1] += MULT16_16(sinct[j+1], iptr[j+1]);
        accum[2] += MULT16_16(sinct[j+2], iptr[j+2]);
        accum[3] += MULT16_16(sinct[j+3], iptr[j+3]);
      }
      sum = accum[0] + accum[1] + accum[2] + accum[3];
*/
      Din_Go(19,2048);sum = SATURATE32PSHR(sum, 15, 32767);
#ifdef OVERRIDE_INNER_PRODUCT_SINGLE
      } else {
      Din_Go(20,2048);sum = inner_product_single(sinct, iptr, N);
      }
#endif

      Din_Go(22,2048);out[out_stride * out_sample++] = sum;
      last_sample += int_advance;
      samp_frac_num += frac_advance;
      Din_Go(24,2048);if (samp_frac_num >= den_rate)
      {
         Din_Go(23,2048);samp_frac_num -= den_rate;
         last_sample++;
      }
   }

   Din_Go(26,2048);st->last_sample[channel_index] = last_sample;
   st->samp_frac_num[channel_index] = samp_frac_num;
   {int  ReplaceReturn232 = out_sample; Din_Go(27,2048); return ReplaceReturn232;}
}

#ifdef FIXED_POINT
#else
/* This is the same as the previous function, except with a double-precision accumulator */
static int resampler_basic_direct_double(SpeexResamplerState *st, spx_uint32_t channel_index, const spx_word16_t *in, spx_uint32_t *in_len, spx_word16_t *out, spx_uint32_t *out_len)
{
   Din_Go(28,2048);const int N = st->filt_len;
   int out_sample = 0;
   int last_sample = st->last_sample[channel_index];
   spx_uint32_t samp_frac_num = st->samp_frac_num[channel_index];
   const spx_word16_t *sinc_table = st->sinc_table;
   const int out_stride = st->out_stride;
   const int int_advance = st->int_advance;
   const int frac_advance = st->frac_advance;
   const spx_uint32_t den_rate = st->den_rate;
   double sum;

   Din_Go(39,2048);while (!(last_sample >= (spx_int32_t)*in_len || out_sample >= (spx_int32_t)*out_len))
   {
      Din_Go(29,2048);const spx_word16_t *sinct = & sinc_table[samp_frac_num*N];
      const spx_word16_t *iptr = & in[last_sample];

#ifdef OVERRIDE_INNER_PRODUCT_DOUBLE
      Din_Go(35,2048);if(moz_speex_have_double_simd()) {
#endif
      Din_Go(30,2048);int j;
      double accum[4] = {0,0,0,0};

      Din_Go(32,2048);for(j=0;j<N;j+=4) {
        Din_Go(31,2048);accum[0] += sinct[j]*iptr[j];
        accum[1] += sinct[j+1]*iptr[j+1];
        accum[2] += sinct[j+2]*iptr[j+2];
        accum[3] += sinct[j+3]*iptr[j+3];
      }
      Din_Go(33,2048);sum = accum[0] + accum[1] + accum[2] + accum[3];
#ifdef OVERRIDE_INNER_PRODUCT_DOUBLE
      } else {
      Din_Go(34,2048);sum = inner_product_double(sinct, iptr, N);
      }
#endif

      Din_Go(36,2048);out[out_stride * out_sample++] = PSHR32(sum, 15);
      last_sample += int_advance;
      samp_frac_num += frac_advance;
      Din_Go(38,2048);if (samp_frac_num >= den_rate)
      {
         Din_Go(37,2048);samp_frac_num -= den_rate;
         last_sample++;
      }
   }

   Din_Go(40,2048);st->last_sample[channel_index] = last_sample;
   st->samp_frac_num[channel_index] = samp_frac_num;
   {int  ReplaceReturn231 = out_sample; Din_Go(41,2048); return ReplaceReturn231;}
}
#endif

static int resampler_basic_interpolate_single(SpeexResamplerState *st, spx_uint32_t channel_index, const spx_word16_t *in, spx_uint32_t *in_len, spx_word16_t *out, spx_uint32_t *out_len)
{
   Din_Go(42,2048);const int N = st->filt_len;
   int out_sample = 0;
   int last_sample = st->last_sample[channel_index];
   spx_uint32_t samp_frac_num = st->samp_frac_num[channel_index];
   const int out_stride = st->out_stride;
   const int int_advance = st->int_advance;
   const int frac_advance = st->frac_advance;
   const spx_uint32_t den_rate = st->den_rate;
   spx_word32_t sum;

   Din_Go(53,2048);while (!(last_sample >= (spx_int32_t)*in_len || out_sample >= (spx_int32_t)*out_len))
   {
      Din_Go(43,2048);const spx_word16_t *iptr = & in[last_sample];

      const int offset = samp_frac_num*st->oversample/st->den_rate;
#ifdef FIXED_POINT
      const spx_word16_t frac = PDIV32(SHL32((samp_frac_num*st->oversample) % st->den_rate,15),st->den_rate);
#else
      const spx_word16_t frac = ((float)((samp_frac_num*st->oversample) % st->den_rate))/st->den_rate;
#endif
      spx_word16_t interp[4];


#ifdef OVERRIDE_INTERPOLATE_PRODUCT_SINGLE
      Din_Go(49,2048);if (!moz_speex_have_single_simd()) {
#endif
      Din_Go(44,2048);int j;
      spx_word32_t accum[4] = {0,0,0,0};

      Din_Go(46,2048);for(j=0;j<N;j++) {
        Din_Go(45,2048);const spx_word16_t curr_in=iptr[j];
        accum[0] += MULT16_16(curr_in,st->sinc_table[4+(j+1)*st->oversample-offset-2]);
        accum[1] += MULT16_16(curr_in,st->sinc_table[4+(j+1)*st->oversample-offset-1]);
        accum[2] += MULT16_16(curr_in,st->sinc_table[4+(j+1)*st->oversample-offset]);
        accum[3] += MULT16_16(curr_in,st->sinc_table[4+(j+1)*st->oversample-offset+1]);
      }

      Din_Go(47,2048);cubic_coef(frac, interp);
      sum = MULT16_32_Q15(interp[0],SHR32(accum[0], 1)) + MULT16_32_Q15(interp[1],SHR32(accum[1], 1)) + MULT16_32_Q15(interp[2],SHR32(accum[2], 1)) + MULT16_32_Q15(interp[3],SHR32(accum[3], 1));
      sum = SATURATE32PSHR(sum, 15, 32767);
#ifdef OVERRIDE_INTERPOLATE_PRODUCT_SINGLE
      } else {
      Din_Go(48,2048);cubic_coef(frac, interp);
      sum = interpolate_product_single(iptr, st->sinc_table + st->oversample + 4 - offset - 2, N, st->oversample, interp);
      }
#endif
      
      Din_Go(50,2048);out[out_stride * out_sample++] = sum;
      last_sample += int_advance;
      samp_frac_num += frac_advance;
      Din_Go(52,2048);if (samp_frac_num >= den_rate)
      {
         Din_Go(51,2048);samp_frac_num -= den_rate;
         last_sample++;
      }
   }

   Din_Go(54,2048);st->last_sample[channel_index] = last_sample;
   st->samp_frac_num[channel_index] = samp_frac_num;
   {int  ReplaceReturn230 = out_sample; Din_Go(55,2048); return ReplaceReturn230;}
}

#ifdef FIXED_POINT
#else
/* This is the same as the previous function, except with a double-precision accumulator */
static int resampler_basic_interpolate_double(SpeexResamplerState *st, spx_uint32_t channel_index, const spx_word16_t *in, spx_uint32_t *in_len, spx_word16_t *out, spx_uint32_t *out_len)
{
   Din_Go(56,2048);const int N = st->filt_len;
   int out_sample = 0;
   int last_sample = st->last_sample[channel_index];
   spx_uint32_t samp_frac_num = st->samp_frac_num[channel_index];
   const int out_stride = st->out_stride;
   const int int_advance = st->int_advance;
   const int frac_advance = st->frac_advance;
   const spx_uint32_t den_rate = st->den_rate;
   spx_word32_t sum;

   Din_Go(67,2048);while (!(last_sample >= (spx_int32_t)*in_len || out_sample >= (spx_int32_t)*out_len))
   {
      Din_Go(57,2048);const spx_word16_t *iptr = & in[last_sample];

      const int offset = samp_frac_num*st->oversample/st->den_rate;
#ifdef FIXED_POINT
      const spx_word16_t frac = PDIV32(SHL32((samp_frac_num*st->oversample) % st->den_rate,15),st->den_rate);
#else
      const spx_word16_t frac = ((float)((samp_frac_num*st->oversample) % st->den_rate))/st->den_rate;
#endif
      spx_word16_t interp[4];


#ifdef OVERRIDE_INTERPOLATE_PRODUCT_DOUBLE
      Din_Go(63,2048);if (!moz_speex_have_double_simd()) {
#endif
      Din_Go(58,2048);int j;
      double accum[4] = {0,0,0,0};

      Din_Go(60,2048);for(j=0;j<N;j++) {
        Din_Go(59,2048);const double curr_in=iptr[j];
        accum[0] += MULT16_16(curr_in,st->sinc_table[4+(j+1)*st->oversample-offset-2]);
        accum[1] += MULT16_16(curr_in,st->sinc_table[4+(j+1)*st->oversample-offset-1]);
        accum[2] += MULT16_16(curr_in,st->sinc_table[4+(j+1)*st->oversample-offset]);
        accum[3] += MULT16_16(curr_in,st->sinc_table[4+(j+1)*st->oversample-offset+1]);
      }

      Din_Go(61,2048);cubic_coef(frac, interp);
      sum = MULT16_32_Q15(interp[0],accum[0]) + MULT16_32_Q15(interp[1],accum[1]) + MULT16_32_Q15(interp[2],accum[2]) + MULT16_32_Q15(interp[3],accum[3]);
#ifdef OVERRIDE_INTERPOLATE_PRODUCT_DOUBLE
      } else {
      Din_Go(62,2048);cubic_coef(frac, interp);
      sum = interpolate_product_double(iptr, st->sinc_table + st->oversample + 4 - offset - 2, N, st->oversample, interp);
      }
#endif
      
      Din_Go(64,2048);out[out_stride * out_sample++] = PSHR32(sum,15);
      last_sample += int_advance;
      samp_frac_num += frac_advance;
      Din_Go(66,2048);if (samp_frac_num >= den_rate)
      {
         Din_Go(65,2048);samp_frac_num -= den_rate;
         last_sample++;
      }
   }

   Din_Go(68,2048);st->last_sample[channel_index] = last_sample;
   st->samp_frac_num[channel_index] = samp_frac_num;
   {int  ReplaceReturn229 = out_sample; Din_Go(69,2048); return ReplaceReturn229;}
}
#endif

/* This resampler is used to produce zero output in situations where memory
   for the filter could not be allocated.  The expected numbers of input and
   output samples are still processed so that callers failing to check error
   codes are not surprised, possibly getting into infinite loops. */
static int resampler_basic_zero(SpeexResamplerState *st, spx_uint32_t channel_index, const spx_word16_t *in, spx_uint32_t *in_len, spx_word16_t *out, spx_uint32_t *out_len)
{
   Din_Go(70,2048);int out_sample = 0;
   int last_sample = st->last_sample[channel_index];
   spx_uint32_t samp_frac_num = st->samp_frac_num[channel_index];
   const int out_stride = st->out_stride;
   const int int_advance = st->int_advance;
   const int frac_advance = st->frac_advance;
   const spx_uint32_t den_rate = st->den_rate;

   Din_Go(74,2048);while (!(last_sample >= (spx_int32_t)*in_len || out_sample >= (spx_int32_t)*out_len))
   {
      Din_Go(71,2048);out[out_stride * out_sample++] = 0;
      last_sample += int_advance;
      samp_frac_num += frac_advance;
      Din_Go(73,2048);if (samp_frac_num >= den_rate)
      {
         Din_Go(72,2048);samp_frac_num -= den_rate;
         last_sample++;
      }
   }

   Din_Go(75,2048);st->last_sample[channel_index] = last_sample;
   st->samp_frac_num[channel_index] = samp_frac_num;
   {int  ReplaceReturn228 = out_sample; Din_Go(76,2048); return ReplaceReturn228;}
}

static int update_filter(SpeexResamplerState *st)
{
Din_Go(77,2048);   spx_uint32_t old_length = st->filt_len;
   spx_uint32_t old_alloc_size = st->mem_alloc_size;
   int use_direct;
   spx_uint32_t min_sinc_table_length;
   spx_uint32_t min_alloc_size;

   st->int_advance = st->num_rate/st->den_rate;
   st->frac_advance = st->num_rate%st->den_rate;
   st->oversample = quality_map[st->quality].oversample;
   st->filt_len = quality_map[st->quality].base_length;
   
   Din_Go(90,2048);if (st->num_rate > st->den_rate)
   {
      /* down-sampling */
      Din_Go(78,2048);st->cutoff = quality_map[st->quality].downsample_bandwidth * st->den_rate / st->num_rate;
      /* FIXME: divide the numerator and denominator by a certain amount if they're too large */
      st->filt_len = st->filt_len*st->num_rate / st->den_rate;
      /* Round up to make sure we have a multiple of 8 for SSE */
      st->filt_len = ((st->filt_len-1)&(~0x7))+8;
      Din_Go(80,2048);if (2*st->den_rate < st->num_rate)
         {/*43*/Din_Go(79,2048);st->oversample >>= 1;/*44*/}
      Din_Go(82,2048);if (4*st->den_rate < st->num_rate)
         {/*45*/Din_Go(81,2048);st->oversample >>= 1;/*46*/}
      Din_Go(84,2048);if (8*st->den_rate < st->num_rate)
         {/*47*/Din_Go(83,2048);st->oversample >>= 1;/*48*/}
      Din_Go(86,2048);if (16*st->den_rate < st->num_rate)
         {/*49*/Din_Go(85,2048);st->oversample >>= 1;/*50*/}
      Din_Go(88,2048);if (st->oversample < 1)
         {/*51*/Din_Go(87,2048);st->oversample = 1;/*52*/}
   } else {
      /* up-sampling */
      Din_Go(89,2048);st->cutoff = quality_map[st->quality].upsample_bandwidth;
   }
   
   Din_Go(91,2048);use_direct =
#ifdef RESAMPLE_HUGEMEM
      /* Choose the direct resampler, even with higher initialization costs,
         when resampling any multiple of 100 to 44100. */
      st->den_rate <= 441
#else
      /* Choose the resampling type that requires the least amount of memory */
      st->filt_len*st->den_rate <= st->filt_len*st->oversample+8
#endif
                && INT_MAX/sizeof(spx_word16_t)/st->den_rate >= st->filt_len;
   Din_Go(96,2048);if (use_direct)
   {
      Din_Go(92,2048);min_sinc_table_length = st->filt_len*st->den_rate;
   } else {
      Din_Go(93,2048);if ((INT_MAX/sizeof(spx_word16_t)-8)/st->oversample < st->filt_len)
         {/*53*/Din_Go(94,2048);goto fail;/*54*/}

      Din_Go(95,2048);min_sinc_table_length = st->filt_len*st->oversample+8;
   }
   Din_Go(101,2048);if (st->sinc_table_length < min_sinc_table_length)
   {
      Din_Go(97,2048);spx_word16_t *sinc_table = (spx_word16_t *)speex_realloc(st->sinc_table,min_sinc_table_length*sizeof(spx_word16_t));
      Din_Go(99,2048);if (!sinc_table)
         {/*55*/Din_Go(98,2048);goto fail;/*56*/}

      Din_Go(100,2048);st->sinc_table = sinc_table;
      st->sinc_table_length = min_sinc_table_length;
   }
   Din_Go(113,2048);if (use_direct)
   {
      spx_uint32_t i;
      Din_Go(104,2048);for (i=0;i<st->den_rate;i++)
      {
         spx_int32_t j;
         Din_Go(103,2048);for (j=0;j<st->filt_len;j++)
         {
            Din_Go(102,2048);st->sinc_table[i*st->filt_len+j] = sinc(st->cutoff,((j-(spx_int32_t)st->filt_len/2+1)-((float)i)/st->den_rate), st->filt_len, quality_map[st->quality].window_func);
         }
      }
#ifdef FIXED_POINT
      st->resampler_ptr = resampler_basic_direct_single;
#else
      Din_Go(107,2048);if (st->quality>8)
         {/*57*/Din_Go(105,2048);st->resampler_ptr = resampler_basic_direct_double;/*58*/}
      else
         {/*59*/Din_Go(106,2048);st->resampler_ptr = resampler_basic_direct_single;/*60*/}
#endif
      /*fprintf (stderr, "resampler uses direct sinc table and normalised cutoff %f\n", cutoff);*/
   } else {
      spx_int32_t i;
      Din_Go(109,2048);for (i=-4;i<(spx_int32_t)(st->oversample*st->filt_len+4);i++)
         {/*61*/Din_Go(108,2048);st->sinc_table[i+4] = sinc(st->cutoff,(i/(float)st->oversample - st->filt_len/2), st->filt_len, quality_map[st->quality].window_func);/*62*/}
#ifdef FIXED_POINT
      st->resampler_ptr = resampler_basic_interpolate_single;
#else
      Din_Go(112,2048);if (st->quality>8)
         {/*63*/Din_Go(110,2048);st->resampler_ptr = resampler_basic_interpolate_double;/*64*/}
      else
         {/*65*/Din_Go(111,2048);st->resampler_ptr = resampler_basic_interpolate_single;/*66*/}
#endif
      /*fprintf (stderr, "resampler uses interpolated sinc table and normalised cutoff %f\n", cutoff);*/
   }

   /* Here's the place where we update the filter memory to take into account
      the change in filter length. It's probably the messiest part of the code
      due to handling of lots of corner cases. */

   /* Adding buffer_size to filt_len won't overflow here because filt_len
      could be multiplied by sizeof(spx_word16_t) above. */
   Din_Go(114,2048);min_alloc_size = st->filt_len-1 + st->buffer_size;
   Din_Go(121,2048);if (min_alloc_size > st->mem_alloc_size)
   {
      Din_Go(115,2048);spx_word16_t *mem;
      Din_Go(119,2048);if (INT_MAX/sizeof(spx_word16_t)/st->nb_channels < min_alloc_size)
          {/*67*/Din_Go(116,2048);goto fail;/*68*/}
      else {/*69*/Din_Go(117,2048);if (!(mem = (spx_word16_t*)speex_realloc(st->mem, st->nb_channels*min_alloc_size * sizeof(*mem))))
          {/*71*/Din_Go(118,2048);goto fail;/*72*/}/*70*/}

      Din_Go(120,2048);st->mem = mem;
      st->mem_alloc_size = min_alloc_size;
   }
   Din_Go(147,2048);if (!st->started)
   {
      spx_uint32_t i;
      Din_Go(123,2048);for (i=0;i<st->nb_channels*st->mem_alloc_size;i++)
         {/*73*/Din_Go(122,2048);st->mem[i] = 0;/*74*/}
      /*speex_warning("reinit filter");*/
   } else {/*75*/Din_Go(124,2048);if (st->filt_len > old_length)
   {
      spx_uint32_t i;
      /* Increase the filter length */
      /*speex_warning("increase filter size");*/
      Din_Go(140,2048);for (i=st->nb_channels;i--;)
      {
         spx_uint32_t j;
Din_Go(125,2048);         spx_uint32_t olen = old_length;
         /*if (st->magic_samples[i])*/
         {
            /* Try and remove the magic samples as if nothing had happened */
            
            /* FIXME: This is wrong but for now we need it to avoid going over the array bounds */
            olen = old_length + 2*st->magic_samples[i];
            Din_Go(127,2048);for (j=old_length-1+st->magic_samples[i];j--;)
               {/*77*/Din_Go(126,2048);st->mem[i*st->mem_alloc_size+j+st->magic_samples[i]] = st->mem[i*old_alloc_size+j];/*78*/}
            Din_Go(129,2048);for (j=0;j<st->magic_samples[i];j++)
               {/*79*/Din_Go(128,2048);st->mem[i*st->mem_alloc_size+j] = 0;/*80*/}
            Din_Go(130,2048);st->magic_samples[i] = 0;
         }
         Din_Go(139,2048);if (st->filt_len > olen)
         {
            /* If the new filter length is still bigger than the "augmented" length */
            /* Copy data going backward */
            Din_Go(131,2048);for (j=0;j<olen-1;j++)
               {/*81*/Din_Go(132,2048);st->mem[i*st->mem_alloc_size+(st->filt_len-2-j)] = st->mem[i*st->mem_alloc_size+(olen-2-j)];/*82*/}
            /* Then put zeros for lack of anything better */
            Din_Go(134,2048);for (;j<st->filt_len-1;j++)
               {/*83*/Din_Go(133,2048);st->mem[i*st->mem_alloc_size+(st->filt_len-2-j)] = 0;/*84*/}
            /* Adjust last_sample */
            Din_Go(135,2048);st->last_sample[i] += (st->filt_len - olen)/2;
         } else {
            /* Put back some of the magic! */
            Din_Go(136,2048);st->magic_samples[i] = (olen - st->filt_len)/2;
            Din_Go(138,2048);for (j=0;j<st->filt_len-1+st->magic_samples[i];j++)
               {/*85*/Din_Go(137,2048);st->mem[i*st->mem_alloc_size+j] = st->mem[i*st->mem_alloc_size+j+st->magic_samples[i]];/*86*/}
         }
      }
   } else {/*87*/Din_Go(141,2048);if (st->filt_len < old_length)
   {
      spx_uint32_t i;
      /* Reduce filter length, this a bit tricky. We need to store some of the memory as "magic"
         samples so they can be used directly as input the next time(s) */
      Din_Go(146,2048);for (i=0;i<st->nb_channels;i++)
      {
         spx_uint32_t j;
Din_Go(142,2048);         spx_uint32_t old_magic = st->magic_samples[i];
         st->magic_samples[i] = (old_length - st->filt_len)/2;
         /* We must copy some of the memory that's no longer used */
         /* Copy data going backward */
         Din_Go(144,2048);for (j=0;j<st->filt_len-1+st->magic_samples[i]+old_magic;j++)
            {/*89*/Din_Go(143,2048);st->mem[i*st->mem_alloc_size+j] = st->mem[i*st->mem_alloc_size+j+st->magic_samples[i]];/*90*/}
         Din_Go(145,2048);st->magic_samples[i] += old_magic;
      }
   ;/*88*/}/*76*/}}
   {int  ReplaceReturn227 = RESAMPLER_ERR_SUCCESS; Din_Go(148,2048); return ReplaceReturn227;}

fail:
   st->resampler_ptr = resampler_basic_zero;
   /* st->mem may still contain consumed input samples for the filter.
      Restore filt_len so that filt_len - 1 still points to the position after
      the last of these samples. */
   st->filt_len = old_length;
   return RESAMPLER_ERR_ALLOC_FAILED;
}

EXPORT SpeexResamplerState *speex_resampler_init(spx_uint32_t nb_channels, spx_uint32_t in_rate, spx_uint32_t out_rate, int quality, int *err)
{
   {SpeexResamplerState * ReplaceReturn226 = speex_resampler_init_frac(nb_channels, in_rate, out_rate, in_rate, out_rate, quality, err); Din_Go(149,2048); return ReplaceReturn226;}
}

EXPORT SpeexResamplerState *speex_resampler_init_frac(spx_uint32_t nb_channels, spx_uint32_t ratio_num, spx_uint32_t ratio_den, spx_uint32_t in_rate, spx_uint32_t out_rate, int quality, int *err)
{
Din_Go(150,2048);   spx_uint32_t i;
   SpeexResamplerState *st;
   int filter_err;

   Din_Go(154,2048);if (quality > 10 || quality < 0)
   {
      Din_Go(151,2048);if (err)
         {/*1*/Din_Go(152,2048);*err = RESAMPLER_ERR_INVALID_ARG;/*2*/}
      {SpeexResamplerState * ReplaceReturn225 = NULL; Din_Go(153,2048); return ReplaceReturn225;}
   }
   Din_Go(155,2048);st = (SpeexResamplerState *)speex_alloc(sizeof(SpeexResamplerState));
   st->initialised = 0;
   st->started = 0;
   st->in_rate = 0;
   st->out_rate = 0;
   st->num_rate = 0;
   st->den_rate = 0;
   st->quality = -1;
   st->sinc_table_length = 0;
   st->mem_alloc_size = 0;
   st->filt_len = 0;
   st->mem = 0;
   st->resampler_ptr = 0;
         
   st->cutoff = 1.f;
   st->nb_channels = nb_channels;
   st->in_stride = 1;
   st->out_stride = 1;
   
   st->buffer_size = 160;
   
   /* Per channel data */
   st->last_sample = (spx_int32_t*)speex_alloc(nb_channels*sizeof(spx_int32_t));
   st->magic_samples = (spx_uint32_t*)speex_alloc(nb_channels*sizeof(spx_uint32_t));
   st->samp_frac_num = (spx_uint32_t*)speex_alloc(nb_channels*sizeof(spx_uint32_t));
   Din_Go(157,2048);for (i=0;i<nb_channels;i++)
   {
      Din_Go(156,2048);st->last_sample[i] = 0;
      st->magic_samples[i] = 0;
      st->samp_frac_num[i] = 0;
   }

   speex_resampler_set_quality(st, quality);
   speex_resampler_set_rate_frac(st, ratio_num, ratio_den, in_rate, out_rate);

   Din_Go(158,2048);filter_err = update_filter(st);
   Din_Go(161,2048);if (filter_err == RESAMPLER_ERR_SUCCESS)
   {
      Din_Go(159,2048);st->initialised = 1;
   } else {
      speex_resampler_destroy(st);
      Din_Go(160,2048);st = NULL;
   }
   Din_Go(163,2048);if (err)
      {/*3*/Din_Go(162,2048);*err = filter_err;/*4*/}

   {SpeexResamplerState * ReplaceReturn224 = st; Din_Go(164,2048); return ReplaceReturn224;}
}

EXPORT void speex_resampler_destroy(SpeexResamplerState *st)
{
   Din_Go(165,2048);speex_free(st->mem);
   speex_free(st->sinc_table);
   speex_free(st->last_sample);
   speex_free(st->magic_samples);
   speex_free(st->samp_frac_num);
   speex_free(st);
Din_Go(166,2048);}

static int speex_resampler_process_native(SpeexResamplerState *st, spx_uint32_t channel_index, spx_uint32_t *in_len, spx_word16_t *out, spx_uint32_t *out_len)
{
   Din_Go(167,2048);int j=0;
   const int N = st->filt_len;
   int out_sample = 0;
   spx_word16_t *mem = st->mem + channel_index * st->mem_alloc_size;
   spx_uint32_t ilen;
   
   st->started = 1;
   
   /* Call the right resampler through the function ptr */
   out_sample = st->resampler_ptr(st, channel_index, mem, in_len, out, out_len);
   
   Din_Go(169,2048);if (st->last_sample[channel_index] < (spx_int32_t)*in_len)
      {/*91*/Din_Go(168,2048);*in_len = st->last_sample[channel_index];/*92*/}
   Din_Go(170,2048);*out_len = out_sample;
   st->last_sample[channel_index] -= *in_len;
   
   ilen = *in_len;

   Din_Go(172,2048);for(j=0;j<N-1;++j)
     {/*93*/Din_Go(171,2048);mem[j] = mem[j+ilen];/*94*/}

   {int  ReplaceReturn223 = RESAMPLER_ERR_SUCCESS; Din_Go(173,2048); return ReplaceReturn223;}
}

static int speex_resampler_magic(SpeexResamplerState *st, spx_uint32_t channel_index, spx_word16_t **out, spx_uint32_t out_len) {
Din_Go(174,2048);   spx_uint32_t tmp_in_len = st->magic_samples[channel_index];
   spx_word16_t *mem = st->mem + channel_index * st->mem_alloc_size;
   const int N = st->filt_len;
   
   speex_resampler_process_native(st, channel_index, &tmp_in_len, *out, &out_len);

   st->magic_samples[channel_index] -= tmp_in_len;
   
   /* If we couldn't process all "magic" input samples, save the rest for next time */
   Din_Go(177,2048);if (st->magic_samples[channel_index])
   {
      spx_uint32_t i;
      Din_Go(176,2048);for (i=0;i<st->magic_samples[channel_index];i++)
         {/*95*/Din_Go(175,2048);mem[N-1+i]=mem[N-1+i+tmp_in_len];/*96*/}
   }
   Din_Go(178,2048);*out += out_len*st->out_stride;
   {int  ReplaceReturn222 = out_len; Din_Go(179,2048); return ReplaceReturn222;}
}

#ifdef FIXED_POINT
EXPORT int speex_resampler_process_int(SpeexResamplerState *st, spx_uint32_t channel_index, const spx_int16_t *in, spx_uint32_t *in_len, spx_int16_t *out, spx_uint32_t *out_len)
#else
EXPORT int speex_resampler_process_float(SpeexResamplerState *st, spx_uint32_t channel_index, const float *in, spx_uint32_t *in_len, float *out, spx_uint32_t *out_len)
#endif
{
   Din_Go(180,2048);int j;
   spx_uint32_t ilen = *in_len;
   spx_uint32_t olen = *out_len;
   spx_word16_t *x = st->mem + channel_index * st->mem_alloc_size;
   const int filt_offs = st->filt_len - 1;
   const spx_uint32_t xlen = st->mem_alloc_size - filt_offs;
   const int istride = st->in_stride;

   Din_Go(182,2048);if (st->magic_samples[channel_index]) 
      {/*5*/Din_Go(181,2048);olen -= speex_resampler_magic(st, channel_index, &out, olen);/*6*/}
   Din_Go(193,2048);if (! st->magic_samples[channel_index]) {
      Din_Go(183,2048);while (ilen && olen) {
Din_Go(184,2048);        spx_uint32_t ichunk = (ilen > xlen) ? xlen : ilen;
        spx_uint32_t ochunk = olen;
 
        Din_Go(189,2048);if (in) {
           Din_Go(185,2048);for(j=0;j<ichunk;++j)
              {/*7*/Din_Go(186,2048);x[j+filt_offs]=in[j*istride];/*8*/}
        } else {
          Din_Go(187,2048);for(j=0;j<ichunk;++j)
            {/*9*/Din_Go(188,2048);x[j+filt_offs]=0;/*10*/}
        }
        Din_Go(190,2048);speex_resampler_process_native(st, channel_index, &ichunk, out, &ochunk);
        ilen -= ichunk;
        olen -= ochunk;
        out += ochunk * st->out_stride;
        Din_Go(192,2048);if (in)
           {/*11*/Din_Go(191,2048);in += ichunk * istride;/*12*/}
      }
   }
   Din_Go(194,2048);*in_len -= ilen;
   *out_len -= olen;
   {int  ReplaceReturn221 = st->resampler_ptr == resampler_basic_zero ? RESAMPLER_ERR_ALLOC_FAILED : RESAMPLER_ERR_SUCCESS; Din_Go(195,2048); return ReplaceReturn221;}
}

#ifdef FIXED_POINT
EXPORT int speex_resampler_process_float(SpeexResamplerState *st, spx_uint32_t channel_index, const float *in, spx_uint32_t *in_len, float *out, spx_uint32_t *out_len)
#else
EXPORT int speex_resampler_process_int(SpeexResamplerState *st, spx_uint32_t channel_index, const spx_int16_t *in, spx_uint32_t *in_len, spx_int16_t *out, spx_uint32_t *out_len)
#endif
{
   Din_Go(196,2048);int j;
   const int istride_save = st->in_stride;
   const int ostride_save = st->out_stride;
   spx_uint32_t ilen = *in_len;
   spx_uint32_t olen = *out_len;
   spx_word16_t *x = st->mem + channel_index * st->mem_alloc_size;
   const spx_uint32_t xlen = st->mem_alloc_size - (st->filt_len - 1);
#ifdef VAR_ARRAYS
   const unsigned int ylen = (olen < FIXED_STACK_ALLOC) ? olen : FIXED_STACK_ALLOC;
   VARDECL(spx_word16_t *ystack);
   ALLOC(ystack, ylen, spx_word16_t);
#else
   const unsigned int ylen = FIXED_STACK_ALLOC;
   spx_word16_t ystack[FIXED_STACK_ALLOC];
#endif

   st->out_stride = 1;
   
   Din_Go(211,2048);while (ilen && olen) {
     Din_Go(197,2048);spx_word16_t *y = ystack;
     spx_uint32_t ichunk = (ilen > xlen) ? xlen : ilen;
     spx_uint32_t ochunk = (olen > ylen) ? ylen : olen;
     spx_uint32_t omagic = 0;

     Din_Go(199,2048);if (st->magic_samples[channel_index]) {
       Din_Go(198,2048);omagic = speex_resampler_magic(st, channel_index, &y, ochunk);
       ochunk -= omagic;
       olen -= omagic;
     }
     Din_Go(207,2048);if (! st->magic_samples[channel_index]) {
       Din_Go(200,2048);if (in) {
         Din_Go(201,2048);for(j=0;j<ichunk;++j)
#ifdef FIXED_POINT
           x[j+st->filt_len-1]=WORD2INT(in[j*istride_save]);
#else
           {/*13*/Din_Go(202,2048);x[j+st->filt_len-1]=in[j*istride_save];/*14*/}
#endif
       } else {
         Din_Go(203,2048);for(j=0;j<ichunk;++j)
           {/*15*/Din_Go(204,2048);x[j+st->filt_len-1]=0;/*16*/}
       }

       Din_Go(205,2048);speex_resampler_process_native(st, channel_index, &ichunk, y, &ochunk);
     } else {
       Din_Go(206,2048);ichunk = 0;
       ochunk = 0;
     }

     for (j=0;j<ochunk+omagic;++j)
#ifdef FIXED_POINT
        out[j*ostride_save] = ystack[j];
#else
        out[j*ostride_save] = WORD2INT(ystack[j]);
#endif
     
     Din_Go(208,2048);ilen -= ichunk;
     olen -= ochunk;
     out += (ochunk+omagic) * ostride_save;
     Din_Go(210,2048);if (in)
       {/*19*/Din_Go(209,2048);in += ichunk * istride_save;/*20*/}
   }
   Din_Go(212,2048);st->out_stride = ostride_save;
   *in_len -= ilen;
   *out_len -= olen;

   {int  ReplaceReturn220 = st->resampler_ptr == resampler_basic_zero ? RESAMPLER_ERR_ALLOC_FAILED : RESAMPLER_ERR_SUCCESS; Din_Go(213,2048); return ReplaceReturn220;}
}

EXPORT int speex_resampler_process_interleaved_float(SpeexResamplerState *st, const float *in, spx_uint32_t *in_len, float *out, spx_uint32_t *out_len)
{
Din_Go(214,2048);   spx_uint32_t i;
   int istride_save, ostride_save;
   spx_uint32_t bak_out_len = *out_len;
   spx_uint32_t bak_in_len = *in_len;
   istride_save = st->in_stride;
   ostride_save = st->out_stride;
   st->in_stride = st->out_stride = st->nb_channels;
   Din_Go(217,2048);for (i=0;i<st->nb_channels;i++)
   {
      Din_Go(215,2048);*out_len = bak_out_len;
      *in_len = bak_in_len;
      Din_Go(216,2048);if (in != NULL)
         speex_resampler_process_float(st, i, in+i, in_len, out+i, out_len);
      else
         speex_resampler_process_float(st, i, NULL, in_len, out+i, out_len);
   }
   Din_Go(218,2048);st->in_stride = istride_save;
   st->out_stride = ostride_save;
   {int  ReplaceReturn219 = st->resampler_ptr == resampler_basic_zero ? RESAMPLER_ERR_ALLOC_FAILED : RESAMPLER_ERR_SUCCESS; Din_Go(219,2048); return ReplaceReturn219;}
}
               
EXPORT int speex_resampler_process_interleaved_int(SpeexResamplerState *st, const spx_int16_t *in, spx_uint32_t *in_len, spx_int16_t *out, spx_uint32_t *out_len)
{
Din_Go(220,2048);   spx_uint32_t i;
   int istride_save, ostride_save;
   spx_uint32_t bak_out_len = *out_len;
   spx_uint32_t bak_in_len = *in_len;
   istride_save = st->in_stride;
   ostride_save = st->out_stride;
   st->in_stride = st->out_stride = st->nb_channels;
   Din_Go(223,2048);for (i=0;i<st->nb_channels;i++)
   {
      Din_Go(221,2048);*out_len = bak_out_len;
      *in_len = bak_in_len;
      Din_Go(222,2048);if (in != NULL)
         speex_resampler_process_int(st, i, in+i, in_len, out+i, out_len);
      else
         speex_resampler_process_int(st, i, NULL, in_len, out+i, out_len);
   }
   Din_Go(224,2048);st->in_stride = istride_save;
   st->out_stride = ostride_save;
   {int  ReplaceReturn218 = st->resampler_ptr == resampler_basic_zero ? RESAMPLER_ERR_ALLOC_FAILED : RESAMPLER_ERR_SUCCESS; Din_Go(225,2048); return ReplaceReturn218;}
}

EXPORT int speex_resampler_set_rate(SpeexResamplerState *st, spx_uint32_t in_rate, spx_uint32_t out_rate)
{
   {int  ReplaceReturn217 = speex_resampler_set_rate_frac(st, in_rate, out_rate, in_rate, out_rate); Din_Go(226,2048); return ReplaceReturn217;}
}

EXPORT void speex_resampler_get_rate(SpeexResamplerState *st, spx_uint32_t *in_rate, spx_uint32_t *out_rate)
{
   Din_Go(227,2048);*in_rate = st->in_rate;
   *out_rate = st->out_rate;
Din_Go(228,2048);}

EXPORT int speex_resampler_set_rate_frac(SpeexResamplerState *st, spx_uint32_t ratio_num, spx_uint32_t ratio_den, spx_uint32_t in_rate, spx_uint32_t out_rate)
{
   spx_uint32_t fact;
   spx_uint32_t old_den;
   spx_uint32_t i;
   Din_Go(230,2048);if (st->in_rate == in_rate && st->out_rate == out_rate && st->num_rate == ratio_num && st->den_rate == ratio_den)
      {/*21*/{int  ReplaceReturn216 = RESAMPLER_ERR_SUCCESS; Din_Go(229,2048); return ReplaceReturn216;}/*22*/}
   
   Din_Go(231,2048);old_den = st->den_rate;
   st->in_rate = in_rate;
   st->out_rate = out_rate;
   st->num_rate = ratio_num;
   st->den_rate = ratio_den;
   /* FIXME: This is terribly inefficient, but who cares (at least for now)? */
   Din_Go(234,2048);for (fact=2;fact<=IMIN(st->num_rate, st->den_rate);fact++)
   {
      Din_Go(232,2048);while ((st->num_rate % fact == 0) && (st->den_rate % fact == 0))
      {
         Din_Go(233,2048);st->num_rate /= fact;
         st->den_rate /= fact;
      }
   }
      
   Din_Go(239,2048);if (old_den > 0)
   {
      Din_Go(235,2048);for (i=0;i<st->nb_channels;i++)
      {
         Din_Go(236,2048);st->samp_frac_num[i]=st->samp_frac_num[i]*st->den_rate/old_den;
         /* Safety net */
         Din_Go(238,2048);if (st->samp_frac_num[i] >= st->den_rate)
            {/*23*/Din_Go(237,2048);st->samp_frac_num[i] = st->den_rate-1;/*24*/}
      }
   }
   
   Din_Go(241,2048);if (st->initialised)
      {/*25*/{int  ReplaceReturn215 = update_filter(st); Din_Go(240,2048); return ReplaceReturn215;}/*26*/}
   {int  ReplaceReturn214 = RESAMPLER_ERR_SUCCESS; Din_Go(242,2048); return ReplaceReturn214;}
}

EXPORT void speex_resampler_get_ratio(SpeexResamplerState *st, spx_uint32_t *ratio_num, spx_uint32_t *ratio_den)
{
   Din_Go(243,2048);*ratio_num = st->num_rate;
   *ratio_den = st->den_rate;
Din_Go(244,2048);}

EXPORT int speex_resampler_set_quality(SpeexResamplerState *st, int quality)
{
   Din_Go(245,2048);if (quality > 10 || quality < 0)
      {/*27*/{int  ReplaceReturn213 = RESAMPLER_ERR_INVALID_ARG; Din_Go(246,2048); return ReplaceReturn213;}/*28*/}
   Din_Go(248,2048);if (st->quality == quality)
      {/*29*/{int  ReplaceReturn212 = RESAMPLER_ERR_SUCCESS; Din_Go(247,2048); return ReplaceReturn212;}/*30*/}
   Din_Go(249,2048);st->quality = quality;
   Din_Go(251,2048);if (st->initialised)
      {/*31*/{int  ReplaceReturn211 = update_filter(st); Din_Go(250,2048); return ReplaceReturn211;}/*32*/}
   {int  ReplaceReturn210 = RESAMPLER_ERR_SUCCESS; Din_Go(252,2048); return ReplaceReturn210;}
}

EXPORT void speex_resampler_get_quality(SpeexResamplerState *st, int *quality)
{
   Din_Go(253,2048);*quality = st->quality;
Din_Go(254,2048);}

EXPORT void speex_resampler_set_input_stride(SpeexResamplerState *st, spx_uint32_t stride)
{
   Din_Go(255,2048);st->in_stride = stride;
Din_Go(256,2048);}

EXPORT void speex_resampler_get_input_stride(SpeexResamplerState *st, spx_uint32_t *stride)
{
   Din_Go(257,2048);*stride = st->in_stride;
Din_Go(258,2048);}

EXPORT void speex_resampler_set_output_stride(SpeexResamplerState *st, spx_uint32_t stride)
{
   Din_Go(259,2048);st->out_stride = stride;
Din_Go(260,2048);}

EXPORT void speex_resampler_get_output_stride(SpeexResamplerState *st, spx_uint32_t *stride)
{
   Din_Go(261,2048);*stride = st->out_stride;
Din_Go(262,2048);}

EXPORT int speex_resampler_get_input_latency(SpeexResamplerState *st)
{
  {int  ReplaceReturn209 = st->filt_len / 2; Din_Go(263,2048); return ReplaceReturn209;}
}

EXPORT int speex_resampler_get_output_latency(SpeexResamplerState *st)
{
  {int  ReplaceReturn208 = ((st->filt_len / 2) * st->den_rate + (st->num_rate >> 1)) / st->num_rate; Din_Go(264,2048); return ReplaceReturn208;}
}

EXPORT int speex_resampler_skip_zeros(SpeexResamplerState *st)
{
Din_Go(265,2048);   spx_uint32_t i;
   Din_Go(267,2048);for (i=0;i<st->nb_channels;i++)
      {/*33*/Din_Go(266,2048);st->last_sample[i] = st->filt_len/2;/*34*/}
   {int  ReplaceReturn207 = RESAMPLER_ERR_SUCCESS; Din_Go(268,2048); return ReplaceReturn207;}
}

EXPORT int speex_resampler_set_skip_frac_num(SpeexResamplerState *st, spx_uint32_t skip_frac_num)
{
   spx_uint32_t i;
   spx_uint32_t last_sample = skip_frac_num / st->den_rate;
   spx_uint32_t samp_frac_num = skip_frac_num % st->den_rate;
   Din_Go(270,2048);for (i=0;i<st->nb_channels;i++) {
      Din_Go(269,2048);st->last_sample[i] = last_sample;
      st->samp_frac_num[i] = samp_frac_num;
   }
   {int  ReplaceReturn206 = RESAMPLER_ERR_SUCCESS; Din_Go(271,2048); return ReplaceReturn206;}
}

EXPORT int speex_resampler_reset_mem(SpeexResamplerState *st)
{
Din_Go(272,2048);   spx_uint32_t i;
   Din_Go(274,2048);for (i=0;i<st->nb_channels;i++)
   {
      Din_Go(273,2048);st->last_sample[i] = 0;
      st->magic_samples[i] = 0;
      st->samp_frac_num[i] = 0;
   }
   Din_Go(276,2048);for (i=0;i<st->nb_channels*(st->filt_len-1);i++)
      {/*35*/Din_Go(275,2048);st->mem[i] = 0;/*36*/}
   {int  ReplaceReturn205 = RESAMPLER_ERR_SUCCESS; Din_Go(277,2048); return ReplaceReturn205;}
}

EXPORT const char *speex_resampler_strerror(int err)
{
   Din_Go(278,2048);switch (err)
   {
      case RESAMPLER_ERR_SUCCESS:
         {const char * ReplaceReturn204 = "Success."; Din_Go(279,2048); return ReplaceReturn204;}
      case RESAMPLER_ERR_ALLOC_FAILED:
         {const char * ReplaceReturn203 = "Memory allocation failed."; Din_Go(280,2048); return ReplaceReturn203;}
      case RESAMPLER_ERR_BAD_STATE:
         {const char * ReplaceReturn202 = "Bad resampler state."; Din_Go(281,2048); return ReplaceReturn202;}
      case RESAMPLER_ERR_INVALID_ARG:
         {const char * ReplaceReturn201 = "Invalid argument."; Din_Go(282,2048); return ReplaceReturn201;}
      case RESAMPLER_ERR_PTR_OVERLAP:
         {const char * ReplaceReturn200 = "Input and output buffers overlap."; Din_Go(283,2048); return ReplaceReturn200;}
      default:
         {const char * ReplaceReturn199 = "Unknown error. Bad error code or strange version mismatch."; Din_Go(284,2048); return ReplaceReturn199;}
   }
Din_Go(285,2048);}
