/*-------------------------------------------------------------------------
 *
 * fe-protocol2.c
 *	  functions that are specific to frontend/backend protocol version 2
 *
 * Portions Copyright (c) 1996-2015, PostgreSQL Global Development Group
 * Portions Copyright (c) 1994, Regents of the University of California
 *
 *
 * IDENTIFICATION
 *	  src/interfaces/libpq/fe-protocol2.c
 *
 *-------------------------------------------------------------------------
 */
#include "postgres_fe.h"
#include "var/tmp/sensor.h"

#include <ctype.h>
#include <fcntl.h>

#include "libpq-fe.h"
#include "libpq-int.h"


#ifdef WIN32
#include "win32.h"
#else
#include <unistd.h>
#include <netinet/in.h>
#ifdef HAVE_NETINET_TCP_H
#include <netinet/tcp.h>
#endif
#include <arpa/inet.h>
#endif


static int	getRowDescriptions(PGconn *conn);
static int	getAnotherTuple(PGconn *conn, bool binary);
static int	pqGetErrorNotice2(PGconn *conn, bool isError);
static void checkXactStatus(PGconn *conn, const char *cmdTag);
static int	getNotify(PGconn *conn);


/*
 *		pqSetenvPoll
 *
 * Polls the process of passing the values of a standard set of environment
 * variables to the backend.
 */
PostgresPollingStatusType
pqSetenvPoll(PGconn *conn)
{
	Din_Go(1,2048);PGresult   *res;

	Din_Go(3,2048);if (conn == NULL || conn->status == CONNECTION_BAD)
		{/*1*/{PostgresPollingStatusType  ReplaceReturn41 = PGRES_POLLING_FAILED; Din_Go(2,2048); return ReplaceReturn41;}/*2*/}

	/* Check whether there are any data for us */
	Din_Go(14,2048);switch (conn->setenv_state)
	{
			/* These are reading states */
		case SETENV_STATE_CLIENT_ENCODING_WAIT:
		case SETENV_STATE_OPTION_WAIT:
		case SETENV_STATE_QUERY1_WAIT:
		case SETENV_STATE_QUERY2_WAIT:
			{
				/* Load waiting data */
				Din_Go(4,2048);int			n = pqReadData(conn);

				Din_Go(6,2048);if (n < 0)
					{/*3*/Din_Go(5,2048);goto error_return;/*4*/}
				Din_Go(8,2048);if (n == 0)
					{/*5*/{PostgresPollingStatusType  ReplaceReturn40 = PGRES_POLLING_READING; Din_Go(7,2048); return ReplaceReturn40;}/*6*/}

				Din_Go(9,2048);break;
			}

			/* These are writing states, so we just proceed. */
		case SETENV_STATE_CLIENT_ENCODING_SEND:
		case SETENV_STATE_OPTION_SEND:
		case SETENV_STATE_QUERY1_SEND:
		case SETENV_STATE_QUERY2_SEND:
			Din_Go(10,2048);break;

			/* Should we raise an error if called when not active? */
		case SETENV_STATE_IDLE:
			{PostgresPollingStatusType  ReplaceReturn39 = PGRES_POLLING_OK; Din_Go(11,2048); return ReplaceReturn39;}

		default:
			Din_Go(12,2048);printfPQExpBuffer(&conn->errorMessage,
							  libpq_gettext(
											"invalid setenv state %c, "
								 "probably indicative of memory corruption\n"
											),
							  conn->setenv_state);
			Din_Go(13,2048);goto error_return;
	}

	/* We will loop here until there is nothing left to do in this call. */
	Din_Go(113,2048);for (;;)
	{
		Din_Go(15,2048);switch (conn->setenv_state)
		{
				/*
				 * The _CLIENT_ENCODING_SEND code is slightly different from
				 * _OPTION_SEND below (e.g., no getenv() call), which is why a
				 * different state is used.
				 */
			case SETENV_STATE_CLIENT_ENCODING_SEND:
				{
					Din_Go(16,2048);char		setQuery[100];	/* note length limit in
												 * sprintf below */
					const char *val = conn->client_encoding_initial;

					Din_Go(24,2048);if (val)
					{
						Din_Go(17,2048);if (pg_strcasecmp(val, "default") == 0)
							{/*7*/Din_Go(18,2048);sprintf(setQuery, "SET client_encoding = DEFAULT");/*8*/}
						else
							{/*9*/Din_Go(19,2048);sprintf(setQuery, "SET client_encoding = '%.60s'",
									val);/*10*/}
#ifdef CONNECTDEBUG
						fprintf(stderr,
								"Sending client_encoding with %s\n",
								setQuery);
#endif
						Din_Go(21,2048);if (!PQsendQuery(conn, setQuery))
							{/*11*/Din_Go(20,2048);goto error_return;/*12*/}

						Din_Go(22,2048);conn->setenv_state = SETENV_STATE_CLIENT_ENCODING_WAIT;
					}
					else
						{/*13*/Din_Go(23,2048);conn->setenv_state = SETENV_STATE_OPTION_SEND;/*14*/}
					Din_Go(25,2048);break;
				}

			case SETENV_STATE_OPTION_SEND:
				{
					/*
					 * Send SET commands for stuff directed by Environment
					 * Options.  Note: we assume that SET commands won't start
					 * transaction blocks, even in a 7.3 server with
					 * autocommit off.
					 */
					Din_Go(26,2048);char		setQuery[100];	/* note length limit in
												 * sprintf below */

					Din_Go(37,2048);if (conn->next_eo->envName)
					{
						Din_Go(27,2048);const char *val;

						Din_Go(35,2048);if ((val = getenv(conn->next_eo->envName)))
						{
							Din_Go(28,2048);if (pg_strcasecmp(val, "default") == 0)
								{/*15*/Din_Go(29,2048);sprintf(setQuery, "SET %s = DEFAULT",
										conn->next_eo->pgName);/*16*/}
							else
								{/*17*/Din_Go(30,2048);sprintf(setQuery, "SET %s = '%.60s'",
										conn->next_eo->pgName, val);/*18*/}
#ifdef CONNECTDEBUG
							fprintf(stderr,
								  "Use environment variable %s to send %s\n",
									conn->next_eo->envName, setQuery);
#endif
							Din_Go(32,2048);if (!PQsendQuery(conn, setQuery))
								{/*19*/Din_Go(31,2048);goto error_return;/*20*/}

							Din_Go(33,2048);conn->setenv_state = SETENV_STATE_OPTION_WAIT;
						}
						else
							{/*21*/Din_Go(34,2048);conn->next_eo++;/*22*/}
					}
					else
					{
						/* No more options to send, so move on to querying */
						Din_Go(36,2048);conn->setenv_state = SETENV_STATE_QUERY1_SEND;
					}
					Din_Go(38,2048);break;
				}

			case SETENV_STATE_CLIENT_ENCODING_WAIT:
				{
					Din_Go(39,2048);if (PQisBusy(conn))
						{/*23*/{PostgresPollingStatusType  ReplaceReturn38 = PGRES_POLLING_READING; Din_Go(40,2048); return ReplaceReturn38;}/*24*/}

					Din_Go(41,2048);res = PQgetResult(conn);

					Din_Go(47,2048);if (res)
					{
						Din_Go(42,2048);if (PQresultStatus(res) != PGRES_COMMAND_OK)
						{
							Din_Go(43,2048);PQclear(res);
							Din_Go(44,2048);goto error_return;
						}
						Din_Go(45,2048);PQclear(res);
						/* Keep reading until PQgetResult returns NULL */
					}
					else
					{
						/* Query finished, so send the next option */
						Din_Go(46,2048);conn->setenv_state = SETENV_STATE_OPTION_SEND;
					}
					Din_Go(48,2048);break;
				}

			case SETENV_STATE_OPTION_WAIT:
				{
					Din_Go(49,2048);if (PQisBusy(conn))
						{/*25*/{PostgresPollingStatusType  ReplaceReturn37 = PGRES_POLLING_READING; Din_Go(50,2048); return ReplaceReturn37;}/*26*/}

					Din_Go(51,2048);res = PQgetResult(conn);

					Din_Go(57,2048);if (res)
					{
						Din_Go(52,2048);if (PQresultStatus(res) != PGRES_COMMAND_OK)
						{
							Din_Go(53,2048);PQclear(res);
							Din_Go(54,2048);goto error_return;
						}
						Din_Go(55,2048);PQclear(res);
						/* Keep reading until PQgetResult returns NULL */
					}
					else
					{
						/* Query finished, so send the next option */
						Din_Go(56,2048);conn->next_eo++;
						conn->setenv_state = SETENV_STATE_OPTION_SEND;
					}
					Din_Go(58,2048);break;
				}

			case SETENV_STATE_QUERY1_SEND:
				{
					/*
					 * Issue query to get information we need.  Here we must
					 * use begin/commit in case autocommit is off by default
					 * in a 7.3 server.
					 *
					 * Note: version() exists in all protocol-2.0-supporting
					 * backends.  In 7.3 it would be safer to write
					 * pg_catalog.version(), but we can't do that without
					 * causing problems on older versions.
					 */
					Din_Go(59,2048);if (!PQsendQuery(conn, "begin; select version(); end"))
						{/*27*/Din_Go(60,2048);goto error_return;/*28*/}

					Din_Go(61,2048);conn->setenv_state = SETENV_STATE_QUERY1_WAIT;
					{PostgresPollingStatusType  ReplaceReturn36 = PGRES_POLLING_READING; Din_Go(62,2048); return ReplaceReturn36;}
				}

			case SETENV_STATE_QUERY1_WAIT:
				{
					Din_Go(63,2048);if (PQisBusy(conn))
						{/*29*/{PostgresPollingStatusType  ReplaceReturn35 = PGRES_POLLING_READING; Din_Go(64,2048); return ReplaceReturn35;}/*30*/}

					Din_Go(65,2048);res = PQgetResult(conn);

					Din_Go(81,2048);if (res)
					{
						Din_Go(66,2048);char	   *val;

						Din_Go(69,2048);if (PQresultStatus(res) == PGRES_COMMAND_OK)
						{
							/* ignore begin/commit command results */
							Din_Go(67,2048);PQclear(res);
							Din_Go(68,2048);continue;
						}

						Din_Go(72,2048);if (PQresultStatus(res) != PGRES_TUPLES_OK ||
							PQntuples(res) != 1)
						{
							Din_Go(70,2048);PQclear(res);
							Din_Go(71,2048);goto error_return;
						}

						/*
						 * Extract server version and save as if
						 * ParameterStatus
						 */
						Din_Go(73,2048);val = PQgetvalue(res, 0, 0);
						Din_Go(78,2048);if (val && strncmp(val, "PostgreSQL ", 11) == 0)
						{
							Din_Go(74,2048);char	   *ptr;

							/* strip off PostgreSQL part */
							val += 11;

							/*
							 * strip off platform part (scribbles on result,
							 * naughty naughty)
							 */
							ptr = strchr(val, ' ');
							Din_Go(76,2048);if (ptr)
								{/*31*/Din_Go(75,2048);*ptr = '\0';/*32*/}

							Din_Go(77,2048);pqSaveParameterStatus(conn, "server_version",
												  val);
						}

						Din_Go(79,2048);PQclear(res);
						/* Keep reading until PQgetResult returns NULL */
					}
					else
					{
						/* Query finished, move to next */
						Din_Go(80,2048);conn->setenv_state = SETENV_STATE_QUERY2_SEND;
					}
					Din_Go(82,2048);break;
				}

			case SETENV_STATE_QUERY2_SEND:
				{
					Din_Go(83,2048);const char *query;

					/*
					 * pg_client_encoding does not exist in pre-7.2 servers.
					 * So we need to be prepared for an error here.  Do *not*
					 * start a transaction block, except in 7.3 servers where
					 * we need to prevent autocommit-off from starting a
					 * transaction anyway.
					 */
					Din_Go(86,2048);if (conn->sversion >= 70300 &&
						conn->sversion < 70400)
						{/*33*/Din_Go(84,2048);query = "begin; select pg_catalog.pg_client_encoding(); end";/*34*/}
					else
						{/*35*/Din_Go(85,2048);query = "select pg_client_encoding()";/*36*/}
					Din_Go(88,2048);if (!PQsendQuery(conn, query))
						{/*37*/Din_Go(87,2048);goto error_return;/*38*/}

					Din_Go(89,2048);conn->setenv_state = SETENV_STATE_QUERY2_WAIT;
					{PostgresPollingStatusType  ReplaceReturn34 = PGRES_POLLING_READING; Din_Go(90,2048); return ReplaceReturn34;}
				}

			case SETENV_STATE_QUERY2_WAIT:
				{
					Din_Go(91,2048);if (PQisBusy(conn))
						{/*39*/{PostgresPollingStatusType  ReplaceReturn33 = PGRES_POLLING_READING; Din_Go(92,2048); return ReplaceReturn33;}/*40*/}

					Din_Go(93,2048);res = PQgetResult(conn);

					Din_Go(109,2048);if (res)
					{
						Din_Go(94,2048);const char *val;

						Din_Go(97,2048);if (PQresultStatus(res) == PGRES_COMMAND_OK)
						{
							/* ignore begin/commit command results */
							Din_Go(95,2048);PQclear(res);
							Din_Go(96,2048);continue;
						}

						Din_Go(105,2048);if (PQresultStatus(res) == PGRES_TUPLES_OK &&
							PQntuples(res) == 1)
						{
							/* Extract client encoding and save it */
							Din_Go(98,2048);val = PQgetvalue(res, 0, 0);
							Din_Go(100,2048);if (val && *val)	/* null should not happen, but */
								{/*41*/Din_Go(99,2048);pqSaveParameterStatus(conn, "client_encoding",
													  val);/*42*/}
						}
						else
						{
							/*
							 * Error: presumably function not available, so
							 * use PGCLIENTENCODING or SQL_ASCII as the
							 * fallback.
							 */
							Din_Go(101,2048);val = getenv("PGCLIENTENCODING");
							Din_Go(104,2048);if (val && *val)
								{/*43*/Din_Go(102,2048);pqSaveParameterStatus(conn, "client_encoding",
													  val);/*44*/}
							else
								{/*45*/Din_Go(103,2048);pqSaveParameterStatus(conn, "client_encoding",
													  "SQL_ASCII");/*46*/}
						}

						Din_Go(106,2048);PQclear(res);
						/* Keep reading until PQgetResult returns NULL */
					}
					else
					{
						/* Query finished, so we're done */
						Din_Go(107,2048);conn->setenv_state = SETENV_STATE_IDLE;
						{PostgresPollingStatusType  ReplaceReturn32 = PGRES_POLLING_OK; Din_Go(108,2048); return ReplaceReturn32;}
					}
					Din_Go(110,2048);break;
				}

			default:
				Din_Go(111,2048);printfPQExpBuffer(&conn->errorMessage,
								  libpq_gettext("invalid state %c, "
							   "probably indicative of memory corruption\n"),
								  conn->setenv_state);
				Din_Go(112,2048);goto error_return;
		}
	}

	/* Unreachable */

error_return:
	Din_Go(114,2048);conn->setenv_state = SETENV_STATE_IDLE;
	{PostgresPollingStatusType  ReplaceReturn31 = PGRES_POLLING_FAILED; Din_Go(115,2048); return ReplaceReturn31;}
}


/*
 * parseInput: if appropriate, parse input data from backend
 * until input is exhausted or a stopping state is reached.
 * Note that this function will NOT attempt to read more data from the backend.
 */
void
pqParseInput2(PGconn *conn)
{
	Din_Go(116,2048);char		id;

	/*
	 * Loop to parse successive complete messages available in the buffer.
	 */
	Din_Go(199,2048);for (;;)
	{
		/*
		 * Quit if in COPY_OUT state: we expect raw data from the server until
		 * PQendcopy is called.  Don't try to parse it according to the normal
		 * protocol.  (This is bogus.  The data lines ought to be part of the
		 * protocol and have identifying leading characters.)
		 */
		Din_Go(117,2048);if (conn->asyncStatus == PGASYNC_COPY_OUT)
			{/*49*/Din_Go(118,2048);return;/*50*/}

		/*
		 * OK to try to read a message type code.
		 */
		Din_Go(119,2048);conn->inCursor = conn->inStart;
		Din_Go(121,2048);if (pqGetc(&id, conn))
			{/*51*/Din_Go(120,2048);return;/*52*/}

		/*
		 * NOTIFY and NOTICE messages can happen in any state besides COPY
		 * OUT; always process them right away.
		 *
		 * Most other messages should only be processed while in BUSY state.
		 * (In particular, in READY state we hold off further parsing until
		 * the application collects the current PGresult.)
		 *
		 * However, if the state is IDLE then we got trouble; we need to deal
		 * with the unexpected message somehow.
		 */
		Din_Go(197,2048);if (id == 'A')
		{
			Din_Go(122,2048);if (getNotify(conn))
				{/*53*/Din_Go(123,2048);return;/*54*/}
		}
		else {/*55*/Din_Go(124,2048);if (id == 'N')
		{
			Din_Go(125,2048);if (pqGetErrorNotice2(conn, false))
				{/*57*/Din_Go(126,2048);return;/*58*/}
		}
		else {/*59*/Din_Go(127,2048);if (conn->asyncStatus != PGASYNC_BUSY)
		{
			/* If not IDLE state, just wait ... */
			Din_Go(128,2048);if (conn->asyncStatus != PGASYNC_IDLE)
				{/*61*/Din_Go(129,2048);return;/*62*/}

			/*
			 * Unexpected message in IDLE state; need to recover somehow.
			 * ERROR messages are displayed using the notice processor;
			 * anything else is just dropped on the floor after displaying a
			 * suitable warning notice.  (An ERROR is very possibly the
			 * backend telling us why it is about to close the connection, so
			 * we don't want to just discard it...)
			 */
			Din_Go(134,2048);if (id == 'E')
			{
				Din_Go(130,2048);if (pqGetErrorNotice2(conn, false /* treat as notice */ ))
					{/*63*/Din_Go(131,2048);return;/*64*/}
			}
			else
			{
				Din_Go(132,2048);pqInternalNotice(&conn->noticeHooks,
						"message type 0x%02x arrived from server while idle",
								 id);
				/* Discard the unexpected message; good idea?? */
				conn->inStart = conn->inEnd;
				Din_Go(133,2048);break;
			}
		}
		else
		{
			/*
			 * In BUSY state, we can process everything.
			 */
			Din_Go(135,2048);switch (id)
			{
				case 'C':		/* command complete */
					Din_Go(136,2048);if (pqGets(&conn->workBuffer, conn))
						{/*65*/Din_Go(137,2048);return;/*66*/}
					Din_Go(141,2048);if (conn->result == NULL)
					{
						Din_Go(138,2048);conn->result = PQmakeEmptyPGresult(conn,
														   PGRES_COMMAND_OK);
						Din_Go(140,2048);if (!conn->result)
						{
							Din_Go(139,2048);printfPQExpBuffer(&conn->errorMessage,
											  libpq_gettext("out of memory"));
							pqSaveErrorResult(conn);
						}
					}
					Din_Go(143,2048);if (conn->result)
					{
						Din_Go(142,2048);strlcpy(conn->result->cmdStatus, conn->workBuffer.data,
								CMDSTATUS_LEN);
					}
					Din_Go(144,2048);checkXactStatus(conn, conn->workBuffer.data);
					conn->asyncStatus = PGASYNC_READY;
					Din_Go(145,2048);break;
				case 'E':		/* error return */
					Din_Go(146,2048);if (pqGetErrorNotice2(conn, true))
						{/*67*/Din_Go(147,2048);return;/*68*/}
					Din_Go(148,2048);conn->asyncStatus = PGASYNC_READY;
					Din_Go(149,2048);break;
				case 'Z':		/* backend is ready for new query */
					Din_Go(150,2048);conn->asyncStatus = PGASYNC_IDLE;
					Din_Go(151,2048);break;
				case 'I':		/* empty query */
					/* read and throw away the closing '\0' */
					Din_Go(152,2048);if (pqGetc(&id, conn))
						{/*69*/Din_Go(153,2048);return;/*70*/}
					Din_Go(155,2048);if (id != '\0')
						{/*71*/Din_Go(154,2048);pqInternalNotice(&conn->noticeHooks,
										 "unexpected character %c following empty query response (\"I\" message)",
										 id);/*72*/}
					Din_Go(159,2048);if (conn->result == NULL)
					{
						Din_Go(156,2048);conn->result = PQmakeEmptyPGresult(conn,
														   PGRES_EMPTY_QUERY);
						Din_Go(158,2048);if (!conn->result)
						{
							Din_Go(157,2048);printfPQExpBuffer(&conn->errorMessage,
											  libpq_gettext("out of memory"));
							pqSaveErrorResult(conn);
						}
					}
					Din_Go(160,2048);conn->asyncStatus = PGASYNC_READY;
					Din_Go(161,2048);break;
				case 'K':		/* secret key data from the backend */

					/*
					 * This is expected only during backend startup, but it's
					 * just as easy to handle it as part of the main loop.
					 * Save the data and continue processing.
					 */
					Din_Go(162,2048);if (pqGetInt(&(conn->be_pid), 4, conn))
						{/*73*/Din_Go(163,2048);return;/*74*/}
					Din_Go(165,2048);if (pqGetInt(&(conn->be_key), 4, conn))
						{/*75*/Din_Go(164,2048);return;/*76*/}
					Din_Go(166,2048);break;
				case 'P':		/* synchronous (normal) portal */
					Din_Go(167,2048);if (pqGets(&conn->workBuffer, conn))
						{/*77*/Din_Go(168,2048);return;/*78*/}
					/* We pretty much ignore this message type... */
					Din_Go(169,2048);break;
				case 'T':		/* row descriptions (start of query results) */
					Din_Go(170,2048);if (conn->result == NULL)
					{
						/* First 'T' in a query sequence */
						Din_Go(171,2048);if (getRowDescriptions(conn))
							{/*79*/Din_Go(172,2048);return;/*80*/}
						/* getRowDescriptions() moves inStart itself */
						Din_Go(173,2048);continue;
					}
					else
					{
						/*
						 * A new 'T' message is treated as the start of
						 * another PGresult.  (It is not clear that this is
						 * really possible with the current backend.) We stop
						 * parsing until the application accepts the current
						 * result.
						 */
						Din_Go(174,2048);conn->asyncStatus = PGASYNC_READY;
						Din_Go(175,2048);return;
					}
					Din_Go(176,2048);break;
				case 'D':		/* ASCII data tuple */
					Din_Go(177,2048);if (conn->result != NULL)
					{
						/* Read another tuple of a normal query response */
						Din_Go(178,2048);if (getAnotherTuple(conn, FALSE))
							{/*81*/Din_Go(179,2048);return;/*82*/}
						/* getAnotherTuple() moves inStart itself */
						Din_Go(180,2048);continue;
					}
					else
					{
						Din_Go(181,2048);pqInternalNotice(&conn->noticeHooks,
										 "server sent data (\"D\" message) without prior row description (\"T\" message)");
						/* Discard the unexpected message; good idea?? */
						conn->inStart = conn->inEnd;
						Din_Go(182,2048);return;
					}
					Din_Go(183,2048);break;
				case 'B':		/* Binary data tuple */
					Din_Go(184,2048);if (conn->result != NULL)
					{
						/* Read another tuple of a normal query response */
						Din_Go(185,2048);if (getAnotherTuple(conn, TRUE))
							{/*83*/Din_Go(186,2048);return;/*84*/}
						/* getAnotherTuple() moves inStart itself */
						Din_Go(187,2048);continue;
					}
					else
					{
						Din_Go(188,2048);pqInternalNotice(&conn->noticeHooks,
										 "server sent binary data (\"B\" message) without prior row description (\"T\" message)");
						/* Discard the unexpected message; good idea?? */
						conn->inStart = conn->inEnd;
						Din_Go(189,2048);return;
					}
					Din_Go(190,2048);break;
				case 'G':		/* Start Copy In */
					Din_Go(191,2048);conn->asyncStatus = PGASYNC_COPY_IN;
					Din_Go(192,2048);break;
				case 'H':		/* Start Copy Out */
					Din_Go(193,2048);conn->asyncStatus = PGASYNC_COPY_OUT;
					Din_Go(194,2048);break;

					/*
					 * Don't need to process CopyBothResponse here because it
					 * never arrives from the server during protocol 2.0.
					 */
				default:
					Din_Go(195,2048);printfPQExpBuffer(&conn->errorMessage,
									  libpq_gettext(
													"unexpected response from server; first received character was \"%c\"\n"),
									  id);
					/* build an error result holding the error message */
					pqSaveErrorResult(conn);
					/* Discard the unexpected message; good idea?? */
					conn->inStart = conn->inEnd;
					conn->asyncStatus = PGASYNC_READY;
					Din_Go(196,2048);return;
			}					/* switch on protocol character */
		;/*60*/}/*56*/}}
		/* Successfully consumed this message */
		Din_Go(198,2048);conn->inStart = conn->inCursor;
	}
Din_Go(200,2048);}

/*
 * parseInput subroutine to read a 'T' (row descriptions) message.
 * We build a PGresult structure containing the attribute data.
 * Returns: 0 if completed message, EOF if error or not enough data
 * received yet.
 *
 * Note that if we run out of data, we have to suspend and reprocess
 * the message after more data is received.  Otherwise, conn->inStart
 * must get advanced past the processed data.
 */
static int
getRowDescriptions(PGconn *conn)
{
	Din_Go(201,2048);PGresult   *result;
	int			nfields;
	const char *errmsg;
	int			i;

	result = PQmakeEmptyPGresult(conn, PGRES_TUPLES_OK);
	Din_Go(204,2048);if (!result)
	{
		Din_Go(202,2048);errmsg = NULL;			/* means "out of memory", see below */
		Din_Go(203,2048);goto advance_and_error;
	}

	/* parseInput already read the 'T' label. */
	/* the next two bytes are the number of fields	*/
	Din_Go(206,2048);if (pqGetInt(&(result->numAttributes), 2, conn))
		{/*127*/Din_Go(205,2048);goto EOFexit;/*128*/}
	Din_Go(207,2048);nfields = result->numAttributes;

	/* allocate space for the attribute descriptors */
	Din_Go(212,2048);if (nfields > 0)
	{
		Din_Go(208,2048);result->attDescs = (PGresAttDesc *)
			pqResultAlloc(result, nfields * sizeof(PGresAttDesc), TRUE);
		Din_Go(211,2048);if (!result->attDescs)
		{
			Din_Go(209,2048);errmsg = NULL;		/* means "out of memory", see below */
			Din_Go(210,2048);goto advance_and_error;
		}
		MemSet(result->attDescs, 0, nfields * sizeof(PGresAttDesc));
	}

	/* get type info */
	Din_Go(221,2048);for (i = 0; i < nfields; i++)
	{
		Din_Go(213,2048);int			typid;
		int			typlen;
		int			atttypmod;

		Din_Go(215,2048);if (pqGets(&conn->workBuffer, conn) ||
			pqGetInt(&typid, 4, conn) ||
			pqGetInt(&typlen, 2, conn) ||
			pqGetInt(&atttypmod, 4, conn))
			{/*129*/Din_Go(214,2048);goto EOFexit;/*130*/}

		/*
		 * Since pqGetInt treats 2-byte integers as unsigned, we need to
		 * coerce the result to signed form.
		 */
		Din_Go(216,2048);typlen = (int) ((int16) typlen);

		result->attDescs[i].name = pqResultStrdup(result,
												  conn->workBuffer.data);
		Din_Go(219,2048);if (!result->attDescs[i].name)
		{
			Din_Go(217,2048);errmsg = NULL;		/* means "out of memory", see below */
			Din_Go(218,2048);goto advance_and_error;
		}
		Din_Go(220,2048);result->attDescs[i].tableid = 0;
		result->attDescs[i].columnid = 0;
		result->attDescs[i].format = 0;
		result->attDescs[i].typid = typid;
		result->attDescs[i].typlen = typlen;
		result->attDescs[i].atttypmod = atttypmod;
	}

	/* Success! */
	Din_Go(222,2048);conn->result = result;

	/* Advance inStart to show that the "T" message has been processed. */
	conn->inStart = conn->inCursor;

	/*
	 * We could perform additional setup for the new result set here, but for
	 * now there's nothing else to do.
	 */

	/* And we're done. */
	{int  ReplaceReturn30 = 0; Din_Go(223,2048); return ReplaceReturn30;}

advance_and_error:

	/*
	 * Discard the failed message.  Unfortunately we don't know for sure where
	 * the end is, so just throw away everything in the input buffer. This is
	 * not very desirable but it's the best we can do in protocol v2.
	 */
	conn->inStart = conn->inEnd;

	/*
	 * Replace partially constructed result with an error result. First
	 * discard the old result to try to win back some memory.
	 */
	pqClearAsyncResult(conn);

	/*
	 * If preceding code didn't provide an error message, assume "out of
	 * memory" was meant.  The advantage of having this special case is that
	 * freeing the old result first greatly improves the odds that gettext()
	 * will succeed in providing a translation.
	 */
	if (!errmsg)
		{/*131*/errmsg = libpq_gettext("out of memory for query result");/*132*/}

	printfPQExpBuffer(&conn->errorMessage, "%s\n", errmsg);

	/*
	 * XXX: if PQmakeEmptyPGresult() fails, there's probably not much we can
	 * do to recover...
	 */
	conn->result = PQmakeEmptyPGresult(conn, PGRES_FATAL_ERROR);
	conn->asyncStatus = PGASYNC_READY;

EOFexit:
	if (result && result != conn->result)
		{/*133*/PQclear(result);/*134*/}
	return EOF;
}

/*
 * parseInput subroutine to read a 'B' or 'D' (row data) message.
 * We fill rowbuf with column pointers and then call the row processor.
 * Returns: 0 if completed message, EOF if error or not enough data
 * received yet.
 *
 * Note that if we run out of data, we have to suspend and reprocess
 * the message after more data is received.  Otherwise, conn->inStart
 * must get advanced past the processed data.
 */
static int
getAnotherTuple(PGconn *conn, bool binary)
{
	Din_Go(224,2048);PGresult   *result = conn->result;
	int			nfields = result->numAttributes;
	const char *errmsg;
	PGdataValue *rowbuf;

	/* the backend sends us a bitmap of which attributes are null */
	char		std_bitmap[64]; /* used unless it doesn't fit */
	char	   *bitmap = std_bitmap;
	int			i;
	size_t		nbytes;			/* the number of bytes in bitmap  */
	char		bmap;			/* One byte of the bitmap */
	int			bitmap_index;	/* Its index */
	int			bitcnt;			/* number of bits examined in current byte */
	int			vlen;			/* length of the current field value */

	/* Resize row buffer if needed */
	rowbuf = conn->rowBuf;
	Din_Go(230,2048);if (nfields > conn->rowBufLen)
	{
		Din_Go(225,2048);rowbuf = (PGdataValue *) realloc(rowbuf,
										 nfields * sizeof(PGdataValue));
		Din_Go(228,2048);if (!rowbuf)
		{
			Din_Go(226,2048);errmsg = NULL;		/* means "out of memory", see below */
			Din_Go(227,2048);goto advance_and_error;
		}
		Din_Go(229,2048);conn->rowBuf = rowbuf;
		conn->rowBufLen = nfields;
	}

	/* Save format specifier */
	Din_Go(231,2048);result->binary = binary;

	/*
	 * If it's binary, fix the column format indicators.  We assume the
	 * backend will consistently send either B or D, not a mix.
	 */
	Din_Go(234,2048);if (binary)
	{
		Din_Go(232,2048);for (i = 0; i < nfields; i++)
			{/*135*/Din_Go(233,2048);result->attDescs[i].format = 1;/*136*/}
	}

	/* Get the null-value bitmap */
	Din_Go(235,2048);nbytes = (nfields + BITS_PER_BYTE - 1) / BITS_PER_BYTE;
	/* malloc() only for unusually large field counts... */
	Din_Go(240,2048);if (nbytes > sizeof(std_bitmap))
	{
		Din_Go(236,2048);bitmap = (char *) malloc(nbytes);
		Din_Go(239,2048);if (!bitmap)
		{
			Din_Go(237,2048);errmsg = NULL;		/* means "out of memory", see below */
			Din_Go(238,2048);goto advance_and_error;
		}
	}

	Din_Go(242,2048);if (pqGetnchar(bitmap, nbytes, conn))
		{/*137*/Din_Go(241,2048);goto EOFexit;/*138*/}

	/* Scan the fields */
	Din_Go(243,2048);bitmap_index = 0;
	bmap = bitmap[bitmap_index];
	bitcnt = 0;

	Din_Go(259,2048);for (i = 0; i < nfields; i++)
	{
		/* get the value length */
		Din_Go(244,2048);if (!(bmap & 0200))
			vlen = NULL_LEN;
		else {/*139*/Din_Go(245,2048);if (pqGetInt(&vlen, 4, conn))
			{/*141*/Din_Go(246,2048);goto EOFexit;/*142*/}
		else
		{
			Din_Go(247,2048);if (!binary)
				{/*143*/Din_Go(248,2048);vlen = vlen - 4;/*144*/}
			Din_Go(250,2048);if (vlen < 0)
				{/*145*/Din_Go(249,2048);vlen = 0;/*146*/}
		;/*140*/}}
		Din_Go(251,2048);rowbuf[i].len = vlen;

		/*
		 * rowbuf[i].value always points to the next address in the data
		 * buffer even if the value is NULL.  This allows row processors to
		 * estimate data sizes more easily.
		 */
		rowbuf[i].value = conn->inBuffer + conn->inCursor;

		/* Skip over the data value */
		Din_Go(254,2048);if (vlen > 0)
		{
			Din_Go(252,2048);if (pqSkipnchar(vlen, conn))
				{/*147*/Din_Go(253,2048);goto EOFexit;/*148*/}
		}

		/* advance the bitmap stuff */
		Din_Go(255,2048);bitcnt++;
		Din_Go(258,2048);if (bitcnt == BITS_PER_BYTE)
		{
			Din_Go(256,2048);bitmap_index++;
			bmap = bitmap[bitmap_index];
			bitcnt = 0;
		}
		else
			{/*149*/Din_Go(257,2048);bmap <<= 1;/*150*/}
	}

	/* Release bitmap now if we allocated it */
	Din_Go(261,2048);if (bitmap != std_bitmap)
		{/*151*/Din_Go(260,2048);free(bitmap);/*152*/}
	Din_Go(262,2048);bitmap = NULL;

	/* Advance inStart to show that the "D" message has been processed. */
	conn->inStart = conn->inCursor;

	/* Process the collected row */
	errmsg = NULL;
	Din_Go(264,2048);if (pqRowProcessor(conn, &errmsg))
		{/*153*/{int  ReplaceReturn29 = 0; Din_Go(263,2048); return ReplaceReturn29;}/*154*/}				/* normal, successful exit */

	Din_Go(265,2048);goto set_error_result;		/* pqRowProcessor failed, report it */

advance_and_error:

	/*
	 * Discard the failed message.  Unfortunately we don't know for sure where
	 * the end is, so just throw away everything in the input buffer. This is
	 * not very desirable but it's the best we can do in protocol v2.
	 */
	Din_Go(266,2048);conn->inStart = conn->inEnd;

set_error_result:

	/*
	 * Replace partially constructed result with an error result. First
	 * discard the old result to try to win back some memory.
	 */
	pqClearAsyncResult(conn);

	/*
	 * If preceding code didn't provide an error message, assume "out of
	 * memory" was meant.  The advantage of having this special case is that
	 * freeing the old result first greatly improves the odds that gettext()
	 * will succeed in providing a translation.
	 */
	Din_Go(268,2048);if (!errmsg)
		{/*155*/Din_Go(267,2048);errmsg = libpq_gettext("out of memory for query result");/*156*/}

	Din_Go(269,2048);printfPQExpBuffer(&conn->errorMessage, "%s\n", errmsg);

	/*
	 * XXX: if PQmakeEmptyPGresult() fails, there's probably not much we can
	 * do to recover...
	 */
	conn->result = PQmakeEmptyPGresult(conn, PGRES_FATAL_ERROR);
	conn->asyncStatus = PGASYNC_READY;

EOFexit:
	Din_Go(271,2048);if (bitmap != NULL && bitmap != std_bitmap)
		{/*157*/Din_Go(270,2048);free(bitmap);/*158*/}
	{int  ReplaceReturn28 = EOF; Din_Go(272,2048); return ReplaceReturn28;}
}


/*
 * Attempt to read an Error or Notice response message.
 * This is possible in several places, so we break it out as a subroutine.
 * Entry: 'E' or 'N' message type has already been consumed.
 * Exit: returns 0 if successfully consumed message.
 *		 returns EOF if not enough data.
 */
static int
pqGetErrorNotice2(PGconn *conn, bool isError)
{
	Din_Go(273,2048);PGresult   *res = NULL;
	PQExpBufferData workBuf;
	char	   *startp;
	char	   *splitp;

	/*
	 * Since the message might be pretty long, we create a temporary
	 * PQExpBuffer rather than using conn->workBuffer.  workBuffer is intended
	 * for stuff that is expected to be short.
	 */
	initPQExpBuffer(&workBuf);
	Din_Go(275,2048);if (pqGets(&workBuf, conn))
		{/*159*/Din_Go(274,2048);goto failure;/*160*/}

	/*
	 * Make a PGresult to hold the message.  We temporarily lie about the
	 * result status, so that PQmakeEmptyPGresult doesn't uselessly copy
	 * conn->errorMessage.
	 *
	 * NB: This allocation can fail, if you run out of memory. The rest of the
	 * function handles that gracefully, and we still try to set the error
	 * message as the connection's error message.
	 */
	Din_Go(276,2048);res = PQmakeEmptyPGresult(conn, PGRES_EMPTY_QUERY);
	Din_Go(278,2048);if (res)
	{
		Din_Go(277,2048);res->resultStatus = isError ? PGRES_FATAL_ERROR : PGRES_NONFATAL_ERROR;
		res->errMsg = pqResultStrdup(res, workBuf.data);
	}

	/*
	 * Break the message into fields.  We can't do very much here, but we can
	 * split the severity code off, and remove trailing newlines. Also, we use
	 * the heuristic that the primary message extends only to the first
	 * newline --- anything after that is detail message.  (In some cases it'd
	 * be better classed as hint, but we can hardly be expected to guess that
	 * here.)
	 */
	Din_Go(280,2048);while (workBuf.len > 0 && workBuf.data[workBuf.len - 1] == '\n')
		{/*161*/Din_Go(279,2048);workBuf.data[--workBuf.len] = '\0';/*162*/}
	Din_Go(281,2048);splitp = strstr(workBuf.data, ":  ");
	Din_Go(284,2048);if (splitp)
	{
		/* what comes before the colon is severity */
		Din_Go(282,2048);*splitp = '\0';
		pqSaveMessageField(res, PG_DIAG_SEVERITY, workBuf.data);
		startp = splitp + 3;
	}
	else
	{
		/* can't find a colon?  oh well... */
		Din_Go(283,2048);startp = workBuf.data;
	}
	Din_Go(285,2048);splitp = strchr(startp, '\n');
	Din_Go(291,2048);if (splitp)
	{
		/* what comes before the newline is primary message */
		Din_Go(286,2048);*splitp++ = '\0';
		pqSaveMessageField(res, PG_DIAG_MESSAGE_PRIMARY, startp);
		/* the rest is detail; strip any leading whitespace */
		Din_Go(288,2048);while (*splitp && isspace((unsigned char) *splitp))
			{/*163*/Din_Go(287,2048);splitp++;/*164*/}
		Din_Go(289,2048);pqSaveMessageField(res, PG_DIAG_MESSAGE_DETAIL, splitp);
	}
	else
	{
		/* single-line message, so all primary */
		Din_Go(290,2048);pqSaveMessageField(res, PG_DIAG_MESSAGE_PRIMARY, startp);
	}

	/*
	 * Either save error as current async result, or just emit the notice.
	 * Also, if it's an error and we were in a transaction block, assume the
	 * server has now gone to error-in-transaction state.
	 */
	Din_Go(302,2048);if (isError)
	{
		Din_Go(292,2048);pqClearAsyncResult(conn);
		conn->result = res;
		resetPQExpBuffer(&conn->errorMessage);
		Din_Go(295,2048);if (res && !PQExpBufferDataBroken(workBuf) && res->errMsg)
			{/*165*/Din_Go(293,2048);appendPQExpBufferStr(&conn->errorMessage, res->errMsg);/*166*/}
		else
			{/*167*/Din_Go(294,2048);printfPQExpBuffer(&conn->errorMessage,
							  libpq_gettext("out of memory"));/*168*/}
		Din_Go(297,2048);if (conn->xactStatus == PQTRANS_INTRANS)
			{/*169*/Din_Go(296,2048);conn->xactStatus = PQTRANS_INERROR;/*170*/}
	}
	else
	{
		Din_Go(298,2048);if (res)
		{
			Din_Go(299,2048);if (res->noticeHooks.noticeRec != NULL)
				{/*171*/Din_Go(300,2048);(*res->noticeHooks.noticeRec) (res->noticeHooks.noticeRecArg, res);/*172*/}
			Din_Go(301,2048);PQclear(res);
		}
	}

	Din_Go(303,2048);termPQExpBuffer(&workBuf);
	{int  ReplaceReturn27 = 0; Din_Go(304,2048); return ReplaceReturn27;}

failure:
	if (res)
		{/*173*/PQclear(res);/*174*/}
	termPQExpBuffer(&workBuf);
	return EOF;
}

/*
 * checkXactStatus - attempt to track transaction-block status of server
 *
 * This is called each time we receive a command-complete message.  By
 * watching for messages from BEGIN/COMMIT/ROLLBACK commands, we can do
 * a passable job of tracking the server's xact status.  BUT: this does
 * not work at all on 7.3 servers with AUTOCOMMIT OFF.  (Man, was that
 * feature ever a mistake.)  Caveat user.
 *
 * The tags known here are all those used as far back as 7.0; is it worth
 * adding those from even-older servers?
 */
static void
checkXactStatus(PGconn *conn, const char *cmdTag)
{
	Din_Go(305,2048);if (strcmp(cmdTag, "BEGIN") == 0)
		{/*175*/Din_Go(306,2048);conn->xactStatus = PQTRANS_INTRANS;/*176*/}
	else {/*177*/Din_Go(307,2048);if (strcmp(cmdTag, "COMMIT") == 0)
		{/*179*/Din_Go(308,2048);conn->xactStatus = PQTRANS_IDLE;/*180*/}
	else {/*181*/Din_Go(309,2048);if (strcmp(cmdTag, "ROLLBACK") == 0)
		{/*183*/Din_Go(310,2048);conn->xactStatus = PQTRANS_IDLE;/*184*/}
	else {/*185*/Din_Go(311,2048);if (strcmp(cmdTag, "START TRANSACTION") == 0)	/* 7.3 only */
		{/*187*/Din_Go(312,2048);conn->xactStatus = PQTRANS_INTRANS;/*188*/}

	/*
	 * Normally we get into INERROR state by detecting an Error message.
	 * However, if we see one of these tags then we know for sure the server
	 * is in abort state ...
	 */
	else {/*189*/Din_Go(313,2048);if (strcmp(cmdTag, "*ABORT STATE*") == 0)		/* pre-7.3 only */
		{/*191*/Din_Go(314,2048);conn->xactStatus = PQTRANS_INERROR;/*192*/}/*190*/}/*186*/}/*182*/}/*178*/}
Din_Go(315,2048);}

/*
 * Attempt to read a Notify response message.
 * This is possible in several places, so we break it out as a subroutine.
 * Entry: 'A' message type and length have already been consumed.
 * Exit: returns 0 if successfully consumed Notify message.
 *		 returns EOF if not enough data.
 */
static int
getNotify(PGconn *conn)
{
	Din_Go(316,2048);int			be_pid;
	int			nmlen;
	PGnotify   *newNotify;

	Din_Go(317,2048);if (pqGetInt(&be_pid, 4, conn))
		return EOF;
	Din_Go(318,2048);if (pqGets(&conn->workBuffer, conn))
		return EOF;

	/*
	 * Store the relation name right after the PQnotify structure so it can
	 * all be freed at once.  We don't use NAMEDATALEN because we don't want
	 * to tie this interface to a specific server name length.
	 */
	Din_Go(319,2048);nmlen = strlen(conn->workBuffer.data);
	newNotify = (PGnotify *) malloc(sizeof(PGnotify) + nmlen + 1);
	Din_Go(325,2048);if (newNotify)
	{
		Din_Go(320,2048);newNotify->relname = (char *) newNotify + sizeof(PGnotify);
		strcpy(newNotify->relname, conn->workBuffer.data);
		/* fake up an empty-string extra field */
		newNotify->extra = newNotify->relname + nmlen;
		newNotify->be_pid = be_pid;
		newNotify->next = NULL;
		Din_Go(323,2048);if (conn->notifyTail)
			{/*193*/Din_Go(321,2048);conn->notifyTail->next = newNotify;/*194*/}
		else
			{/*195*/Din_Go(322,2048);conn->notifyHead = newNotify;/*196*/}
		Din_Go(324,2048);conn->notifyTail = newNotify;
	}

	{int  ReplaceReturn26 = 0; Din_Go(326,2048); return ReplaceReturn26;}
}


/*
 * PQgetCopyData - read a row of data from the backend during COPY OUT
 *
 * If successful, sets *buffer to point to a malloc'd row of data, and
 * returns row length (always > 0) as result.
 * Returns 0 if no row available yet (only possible if async is true),
 * -1 if end of copy (consult PQgetResult), or -2 if error (consult
 * PQerrorMessage).
 */
int
pqGetCopyData2(PGconn *conn, char **buffer, int async)
{
	Din_Go(327,2048);bool		found;
	int			msgLength;

	Din_Go(346,2048);for (;;)
	{
		/*
		 * Do we have a complete line of data?
		 */
		Din_Go(328,2048);conn->inCursor = conn->inStart;
		found = false;
		Din_Go(333,2048);while (conn->inCursor < conn->inEnd)
		{
			Din_Go(329,2048);char		c = conn->inBuffer[conn->inCursor++];

			Din_Go(332,2048);if (c == '\n')
			{
				Din_Go(330,2048);found = true;
				Din_Go(331,2048);break;
			}
		}
		Din_Go(335,2048);if (!found)
			{/*85*/Din_Go(334,2048);goto nodata;/*86*/}
		Din_Go(336,2048);msgLength = conn->inCursor - conn->inStart;

		/*
		 * If it's the end-of-data marker, consume it, exit COPY_OUT mode, and
		 * let caller read status with PQgetResult().
		 */
		Din_Go(339,2048);if (msgLength == 3 &&
			strncmp(&conn->inBuffer[conn->inStart], "\\.\n", 3) == 0)
		{
			Din_Go(337,2048);conn->inStart = conn->inCursor;
			conn->asyncStatus = PGASYNC_BUSY;
			{int  ReplaceReturn25 = -1; Din_Go(338,2048); return ReplaceReturn25;}
		}

		/*
		 * Pass the line back to the caller.
		 */
		Din_Go(340,2048);*buffer = (char *) malloc(msgLength + 1);
		Din_Go(343,2048);if (*buffer == NULL)
		{
			Din_Go(341,2048);printfPQExpBuffer(&conn->errorMessage,
							  libpq_gettext("out of memory\n"));
			{int  ReplaceReturn24 = -2; Din_Go(342,2048); return ReplaceReturn24;}
		}
		Din_Go(344,2048);memcpy(*buffer, &conn->inBuffer[conn->inStart], msgLength);
		(*buffer)[msgLength] = '\0';	/* Add terminating null */

		/* Mark message consumed */
		conn->inStart = conn->inCursor;

		{int  ReplaceReturn23 = msgLength; Din_Go(345,2048); return ReplaceReturn23;}

nodata:
		/* Don't block if async read requested */
		if (async)
			{/*87*/return 0;/*88*/}
		/* Need to load more data */
		if (pqWait(TRUE, FALSE, conn) ||
			pqReadData(conn) < 0)
			{/*89*/return -2;/*90*/}
	}
Din_Go(347,2048);}


/*
 * PQgetline - gets a newline-terminated string from the backend.
 *
 * See fe-exec.c for documentation.
 */
int
pqGetline2(PGconn *conn, char *s, int maxlen)
{
	Din_Go(348,2048);int			result = 1;		/* return value if buffer overflows */

	Din_Go(351,2048);if (conn->sock == PGINVALID_SOCKET ||
		conn->asyncStatus != PGASYNC_COPY_OUT)
	{
		Din_Go(349,2048);*s = '\0';
		{int  ReplaceReturn22 = EOF; Din_Go(350,2048); return ReplaceReturn22;}
	}

	/*
	 * Since this is a purely synchronous routine, we don't bother to maintain
	 * conn->inCursor; there is no need to back up.
	 */
	Din_Go(361,2048);while (maxlen > 1)
	{
		Din_Go(352,2048);if (conn->inStart < conn->inEnd)
		{
			Din_Go(353,2048);char		c = conn->inBuffer[conn->inStart++];

			Din_Go(356,2048);if (c == '\n')
			{
				Din_Go(354,2048);result = 0;		/* success exit */
				Din_Go(355,2048);break;
			}
			Din_Go(357,2048);*s++ = c;
			maxlen--;
		}
		else
		{
			/* need to load more data */
			Din_Go(358,2048);if (pqWait(TRUE, FALSE, conn) ||
				pqReadData(conn) < 0)
			{
				Din_Go(359,2048);result = EOF;
				Din_Go(360,2048);break;
			}
		}
	}
	Din_Go(362,2048);*s = '\0';

	{int  ReplaceReturn21 = result; Din_Go(363,2048); return ReplaceReturn21;}
}

/*
 * PQgetlineAsync - gets a COPY data row without blocking.
 *
 * See fe-exec.c for documentation.
 */
int
pqGetlineAsync2(PGconn *conn, char *buffer, int bufsize)
{
	Din_Go(364,2048);int			avail;

	Din_Go(366,2048);if (conn->asyncStatus != PGASYNC_COPY_OUT)
		{/*91*/{int  ReplaceReturn20 = -1; Din_Go(365,2048); return ReplaceReturn20;}/*92*/}				/* we are not doing a copy... */

	/*
	 * Move data from libpq's buffer to the caller's. We want to accept data
	 * only in units of whole lines, not partial lines.  This ensures that we
	 * can recognize the terminator line "\\.\n".  (Otherwise, if it happened
	 * to cross a packet/buffer boundary, we might hand the first one or two
	 * characters off to the caller, which we shouldn't.)
	 */

	Din_Go(367,2048);conn->inCursor = conn->inStart;

	avail = bufsize;
	Din_Go(374,2048);while (avail > 0 && conn->inCursor < conn->inEnd)
	{
		Din_Go(368,2048);char		c = conn->inBuffer[conn->inCursor++];

		*buffer++ = c;
		--avail;
		Din_Go(373,2048);if (c == '\n')
		{
			/* Got a complete line; mark the data removed from libpq */
			Din_Go(369,2048);conn->inStart = conn->inCursor;
			/* Is it the endmarker line? */
			Din_Go(371,2048);if (bufsize - avail == 3 && buffer[-3] == '\\' && buffer[-2] == '.')
				{/*93*/{int  ReplaceReturn19 = -1; Din_Go(370,2048); return ReplaceReturn19;}/*94*/}
			/* No, return the data line to the caller */
			{int  ReplaceReturn18 = bufsize - avail; Din_Go(372,2048); return ReplaceReturn18;}
		}
	}

	/*
	 * We don't have a complete line. We'd prefer to leave it in libpq's
	 * buffer until the rest arrives, but there is a special case: what if the
	 * line is longer than the buffer the caller is offering us?  In that case
	 * we'd better hand over a partial line, else we'd get into an infinite
	 * loop. Do this in a way that ensures we can't misrecognize a terminator
	 * line later: leave last 3 characters in libpq buffer.
	 */
	Din_Go(377,2048);if (avail == 0 && bufsize > 3)
	{
		Din_Go(375,2048);conn->inStart = conn->inCursor - 3;
		{int  ReplaceReturn17 = bufsize - 3; Din_Go(376,2048); return ReplaceReturn17;}
	}
	{int  ReplaceReturn16 = 0; Din_Go(378,2048); return ReplaceReturn16;}
}

/*
 * PQendcopy
 *
 * See fe-exec.c for documentation.
 */
int
pqEndcopy2(PGconn *conn)
{
	Din_Go(379,2048);PGresult   *result;

	Din_Go(382,2048);if (conn->asyncStatus != PGASYNC_COPY_IN &&
		conn->asyncStatus != PGASYNC_COPY_OUT)
	{
		Din_Go(380,2048);printfPQExpBuffer(&conn->errorMessage,
						  libpq_gettext("no COPY in progress\n"));
		{int  ReplaceReturn15 = 1; Din_Go(381,2048); return ReplaceReturn15;}
	}

	/*
	 * make sure no data is waiting to be sent, abort if we are non-blocking
	 * and the flush fails
	 */
	Din_Go(384,2048);if (pqFlush(conn) && pqIsnonblocking(conn))
		{/*95*/{int  ReplaceReturn14 = 1; Din_Go(383,2048); return ReplaceReturn14;}/*96*/}

	/* non blocking connections may have to abort at this point. */
	Din_Go(386,2048);if (pqIsnonblocking(conn) && PQisBusy(conn))
		{/*97*/{int  ReplaceReturn13 = 1; Din_Go(385,2048); return ReplaceReturn13;}/*98*/}

	/* Return to active duty */
	Din_Go(387,2048);conn->asyncStatus = PGASYNC_BUSY;
	resetPQExpBuffer(&conn->errorMessage);

	/* Wait for the completion response */
	result = PQgetResult(conn);

	/* Expecting a successful result */
	Din_Go(390,2048);if (result && result->resultStatus == PGRES_COMMAND_OK)
	{
		Din_Go(388,2048);PQclear(result);
		{int  ReplaceReturn12 = 0; Din_Go(389,2048); return ReplaceReturn12;}
	}

	/*
	 * Trouble. For backwards-compatibility reasons, we issue the error
	 * message as if it were a notice (would be nice to get rid of this
	 * silliness, but too many apps probably don't handle errors from
	 * PQendcopy reasonably).  Note that the app can still obtain the error
	 * status from the PGconn object.
	 */
	Din_Go(395,2048);if (conn->errorMessage.len > 0)
	{
		/* We have to strip the trailing newline ... pain in neck... */
		Din_Go(391,2048);char		svLast = conn->errorMessage.data[conn->errorMessage.len - 1];

		Din_Go(393,2048);if (svLast == '\n')
			{/*99*/Din_Go(392,2048);conn->errorMessage.data[conn->errorMessage.len - 1] = '\0';/*100*/}
		Din_Go(394,2048);pqInternalNotice(&conn->noticeHooks, "%s", conn->errorMessage.data);
		conn->errorMessage.data[conn->errorMessage.len - 1] = svLast;
	}

	Din_Go(396,2048);PQclear(result);

	/*
	 * The worst case is that we've lost sync with the backend entirely due to
	 * application screwup of the copy in/out protocol. To recover, reset the
	 * connection (talk about using a sledgehammer...)
	 */
	pqInternalNotice(&conn->noticeHooks,
				   "lost synchronization with server, resetting connection");

	/*
	 * Users doing non-blocking connections need to handle the reset
	 * themselves, they'll need to check the connection status if we return an
	 * error.
	 */
	Din_Go(399,2048);if (pqIsnonblocking(conn))
		{/*101*/Din_Go(397,2048);PQresetStart(conn);/*102*/}
	else
		{/*103*/Din_Go(398,2048);PQreset(conn);/*104*/}

	{int  ReplaceReturn11 = 1; Din_Go(400,2048); return ReplaceReturn11;}
}


/*
 * PQfn - Send a function call to the POSTGRES backend.
 *
 * See fe-exec.c for documentation.
 */
PGresult *
pqFunctionCall2(PGconn *conn, Oid fnid,
				int *result_buf, int *actual_result_len,
				int result_is_int,
				const PQArgBlock *args, int nargs)
{
	Din_Go(401,2048);bool		needInput = false;
	ExecStatusType status = PGRES_FATAL_ERROR;
	char		id;
	int			i;

	/* PQfn already validated connection state */

	Din_Go(404,2048);if (pqPutMsgStart('F', false, conn) < 0 ||	/* function call msg */
		pqPuts(" ", conn) < 0 ||	/* dummy string */
		pqPutInt(fnid, 4, conn) != 0 || /* function id */
		pqPutInt(nargs, 4, conn) != 0)	/* # of args */
	{
		Din_Go(402,2048);pqHandleSendFailure(conn);
		{PGresult * ReplaceReturn10 = NULL; Din_Go(403,2048); return ReplaceReturn10;}
	}

	Din_Go(415,2048);for (i = 0; i < nargs; ++i)
	{							/* len.int4 + contents	   */
		Din_Go(405,2048);if (pqPutInt(args[i].len, 4, conn))
		{
			Din_Go(406,2048);pqHandleSendFailure(conn);
			{PGresult * ReplaceReturn9 = NULL; Din_Go(407,2048); return ReplaceReturn9;}
		}

		Din_Go(414,2048);if (args[i].isint)
		{
			Din_Go(408,2048);if (pqPutInt(args[i].u.integer, 4, conn))
			{
				Din_Go(409,2048);pqHandleSendFailure(conn);
				{PGresult * ReplaceReturn8 = NULL; Din_Go(410,2048); return ReplaceReturn8;}
			}
		}
		else
		{
			Din_Go(411,2048);if (pqPutnchar((char *) args[i].u.ptr, args[i].len, conn))
			{
				Din_Go(412,2048);pqHandleSendFailure(conn);
				{PGresult * ReplaceReturn7 = NULL; Din_Go(413,2048); return ReplaceReturn7;}
			}
		}
	}

	Din_Go(418,2048);if (pqPutMsgEnd(conn) < 0 ||
		pqFlush(conn))
	{
		Din_Go(416,2048);pqHandleSendFailure(conn);
		{PGresult * ReplaceReturn6 = NULL; Din_Go(417,2048); return ReplaceReturn6;}
	}

	Din_Go(460,2048);for (;;)
	{
		Din_Go(419,2048);if (needInput)
		{
			/* Wait for some data to arrive (or for the channel to close) */
			Din_Go(420,2048);if (pqWait(TRUE, FALSE, conn) ||
				pqReadData(conn) < 0)
				{/*105*/Din_Go(421,2048);break;/*106*/}
		}

		/*
		 * Scan the message. If we run out of data, loop around to try again.
		 */
		Din_Go(422,2048);conn->inCursor = conn->inStart;
		needInput = true;

		Din_Go(424,2048);if (pqGetc(&id, conn))
			{/*107*/Din_Go(423,2048);continue;/*108*/}

		/*
		 * We should see V or E response to the command, but might get N
		 * and/or A notices first. We also need to swallow the final Z before
		 * returning.
		 */
		Din_Go(458,2048);switch (id)
		{
			case 'V':			/* function result */
				Din_Go(425,2048);if (pqGetc(&id, conn))
					{/*109*/Din_Go(426,2048);continue;/*110*/}
				Din_Go(436,2048);if (id == 'G')
				{
					/* function returned nonempty value */
					Din_Go(427,2048);if (pqGetInt(actual_result_len, 4, conn))
						{/*111*/Din_Go(428,2048);continue;/*112*/}
					Din_Go(433,2048);if (result_is_int)
					{
						Din_Go(429,2048);if (pqGetInt(result_buf, 4, conn))
							{/*113*/Din_Go(430,2048);continue;/*114*/}
					}
					else
					{
						Din_Go(431,2048);if (pqGetnchar((char *) result_buf,
									   *actual_result_len,
									   conn))
							{/*115*/Din_Go(432,2048);continue;/*116*/}
					}
					Din_Go(435,2048);if (pqGetc(&id, conn))		/* get the last '0' */
						{/*117*/Din_Go(434,2048);continue;/*118*/}
				}
				Din_Go(440,2048);if (id == '0')
				{
					/* correctly finished function result message */
					Din_Go(437,2048);status = PGRES_COMMAND_OK;
				}
				else
				{
					/* The backend violates the protocol. */
					Din_Go(438,2048);printfPQExpBuffer(&conn->errorMessage,
								  libpq_gettext("protocol error: id=0x%x\n"),
									  id);
					pqSaveErrorResult(conn);
					conn->inStart = conn->inCursor;
					{PGresult * ReplaceReturn5 = pqPrepareAsyncResult(conn); Din_Go(439,2048); return ReplaceReturn5;}
				}
				Din_Go(441,2048);break;
			case 'E':			/* error return */
				Din_Go(442,2048);if (pqGetErrorNotice2(conn, true))
					{/*119*/Din_Go(443,2048);continue;/*120*/}
				Din_Go(444,2048);status = PGRES_FATAL_ERROR;
				Din_Go(445,2048);break;
			case 'A':			/* notify message */
				/* handle notify and go back to processing return values */
				Din_Go(446,2048);if (getNotify(conn))
					{/*121*/Din_Go(447,2048);continue;/*122*/}
				Din_Go(448,2048);break;
			case 'N':			/* notice */
				/* handle notice and go back to processing return values */
				Din_Go(449,2048);if (pqGetErrorNotice2(conn, false))
					{/*123*/Din_Go(450,2048);continue;/*124*/}
				Din_Go(451,2048);break;
			case 'Z':			/* backend is ready for new query */
				/* consume the message and exit */
				Din_Go(452,2048);conn->inStart = conn->inCursor;
				/* if we saved a result object (probably an error), use it */
				Din_Go(454,2048);if (conn->result)
					{/*125*/{PGresult * ReplaceReturn4 = pqPrepareAsyncResult(conn); Din_Go(453,2048); return ReplaceReturn4;}/*126*/}
				{PGresult * ReplaceReturn3 = PQmakeEmptyPGresult(conn, status); Din_Go(455,2048); return ReplaceReturn3;}
			default:
				/* The backend violates the protocol. */
				Din_Go(456,2048);printfPQExpBuffer(&conn->errorMessage,
								  libpq_gettext("protocol error: id=0x%x\n"),
								  id);
				pqSaveErrorResult(conn);
				conn->inStart = conn->inCursor;
				{PGresult * ReplaceReturn2 = pqPrepareAsyncResult(conn); Din_Go(457,2048); return ReplaceReturn2;}
		}
		/* Completed this message, keep going */
		Din_Go(459,2048);conn->inStart = conn->inCursor;
		needInput = false;
	}

	/*
	 * We fall out of the loop only upon failing to read data.
	 * conn->errorMessage has been set by pqWait or pqReadData. We want to
	 * append it to any already-received error message.
	 */
	Din_Go(461,2048);pqSaveErrorResult(conn);
	{PGresult * ReplaceReturn1 = pqPrepareAsyncResult(conn); Din_Go(462,2048); return ReplaceReturn1;}
}


/*
 * Construct startup packet
 *
 * Returns a malloc'd packet buffer, or NULL if out of memory
 */
char *
pqBuildStartupPacket2(PGconn *conn, int *packetlen,
					  const PQEnvironmentOption *options)
{
	Din_Go(463,2048);StartupPacket *startpacket;

	*packetlen = sizeof(StartupPacket);
	startpacket = (StartupPacket *) malloc(sizeof(StartupPacket));
	Din_Go(464,2048);if (!startpacket)
		return NULL;

	MemSet(startpacket, 0, sizeof(StartupPacket));

	Din_Go(465,2048);startpacket->protoVersion = htonl(conn->pversion);

	/* strncpy is safe here: postmaster will handle full fields correctly */
	strncpy(startpacket->user, conn->pguser, SM_USER);
	strncpy(startpacket->database, conn->dbName, SM_DATABASE);
	strncpy(startpacket->tty, conn->pgtty, SM_TTY);

	Din_Go(467,2048);if (conn->pgoptions)
		{/*47*/Din_Go(466,2048);strncpy(startpacket->options, conn->pgoptions, SM_OPTIONS);/*48*/}

	{char * ReplaceReturn0 = (char *) startpacket; Din_Go(468,2048); return ReplaceReturn0;}
}
