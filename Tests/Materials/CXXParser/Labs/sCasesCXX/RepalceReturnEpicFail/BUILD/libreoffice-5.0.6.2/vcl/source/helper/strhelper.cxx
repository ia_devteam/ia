/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This file incorporates work covered by the following license notice:
 *
 *   Licensed to the Apache Software Foundation (ASF) under one or more
 *   contributor license agreements. See the NOTICE file distributed
 *   with this work for additional information regarding copyright
 *   ownership. The ASF licenses this file to you under the Apache
 *   License, Version 2.0 (the "License"); you may not use this file
 *   except in compliance with the License. You may obtain a copy of
 *   the License at http://www.apache.org/licenses/LICENSE-2.0 .
 */

#include "vcl/strhelper.hxx"
#include "var/tmp/sensor.h"
#include "sal/alloca.h"

namespace psp {

inline bool isSpace( sal_Unicode cChar )
{
    SensorCall();return
        cChar == ' '    || cChar == '\t'    ||
        cChar == '\r'   || cChar == '\n'    ||
        cChar == 0x0c   || cChar == 0x0b;
}

inline bool isProtect( sal_Unicode cChar )
{
    {_Bool  ReplaceReturn = cChar == '`' || cChar == '\'' || cChar == '"'; SensorCall(); return ReplaceReturn;}
}

inline void CopyUntil( char*& pTo, const char*& pFrom, char cUntil, bool bIncludeUntil = false )
{
    SensorCall();do
    {
        SensorCall();if( *pFrom == '\\' )
        {
            SensorCall();pFrom++;
            SensorCall();if( *pFrom )
            {
                SensorCall();*pTo = *pFrom;
                pTo++;
            }
        }
        else {/*109*/SensorCall();if( bIncludeUntil || ! isProtect( *pFrom ) )
        {
            SensorCall();*pTo = *pFrom;
            pTo++;
        ;/*110*/}}
        SensorCall();pFrom++;
    } while( *pFrom && *pFrom != cUntil );
    // copy the terminating character unless zero or protector
    SensorCall();if( ! isProtect( *pFrom ) || bIncludeUntil )
    {
        SensorCall();*pTo = *pFrom;
        SensorCall();if( *pTo )
            {/*111*/SensorCall();pTo++;/*112*/}
    }
    SensorCall();if( *pFrom )
        {/*113*/SensorCall();pFrom++;/*114*/}
SensorCall();}

inline void CopyUntil( sal_Unicode*& pTo, const sal_Unicode*& pFrom, sal_Unicode cUntil, bool bIncludeUntil = false )
{
    SensorCall();do
    {
        SensorCall();if( *pFrom == '\\' )
        {
            SensorCall();pFrom++;
            SensorCall();if( *pFrom )
            {
                SensorCall();*pTo = *pFrom;
                pTo++;
            }
        }
        else {/*115*/SensorCall();if( bIncludeUntil || ! isProtect( *pFrom ) )
        {
            SensorCall();*pTo = *pFrom;
            pTo++;
        ;/*116*/}}
        SensorCall();pFrom++;
    } while( *pFrom && *pFrom != cUntil );
    // copy the terminating character unless zero or protector
    SensorCall();if( ! isProtect( *pFrom ) || bIncludeUntil )
    {
        SensorCall();*pTo = *pFrom;
        SensorCall();if( *pTo )
            {/*117*/SensorCall();pTo++;/*118*/}
    }
    SensorCall();if( *pFrom )
        {/*119*/SensorCall();pFrom++;/*120*/}
SensorCall();}

OUString GetCommandLineToken( int nToken, const OUString& rLine )
{
    SensorCall();sal_Int32 nLen = rLine.getLength();
    SensorCall();if( ! nLen )
        {/*1*/{rtl::OUString  ReplaceReturn = OUString(); SensorCall(); return ReplaceReturn;}/*2*/}

    SensorCall();int nActualToken = 0;
    sal_Unicode* pBuffer = static_cast<sal_Unicode*>(alloca( sizeof(sal_Unicode)*( nLen + 1 ) ));
    const sal_Unicode* pRun = rLine.getStr();
    sal_Unicode* pLeap = NULL;

    SensorCall();while( *pRun && nActualToken <= nToken )
    {
        SensorCall();while( *pRun && isSpace( *pRun ) )
            {/*3*/SensorCall();pRun++;/*4*/}
        SensorCall();pLeap = pBuffer;
        SensorCall();while( *pRun && ! isSpace( *pRun ) )
        {
            SensorCall();if( *pRun == '\\' )
            {
                // escapement
                SensorCall();pRun++;
                *pLeap = *pRun;
                pLeap++;
                SensorCall();if( *pRun )
                    {/*5*/SensorCall();pRun++;/*6*/}
            }
            else {/*7*/SensorCall();if( *pRun == '`' )
                {/*9*/SensorCall();CopyUntil( pLeap, pRun, '`' );/*10*/}
            else {/*11*/SensorCall();if( *pRun == '\'' )
                {/*13*/SensorCall();CopyUntil( pLeap, pRun, '\'' );/*14*/}
            else {/*15*/SensorCall();if( *pRun == '"' )
                {/*17*/SensorCall();CopyUntil( pLeap, pRun, '"' );/*18*/}
            else
            {
                SensorCall();*pLeap = *pRun;
                pLeap++;
                pRun++;
            ;/*16*/}/*12*/}/*8*/}}
        }
        SensorCall();if( nActualToken != nToken )
            {/*19*/SensorCall();pBuffer[0] = 0;/*20*/}
        SensorCall();nActualToken++;
    }

    SensorCall();*pLeap = 0;

    {rtl::OUString  ReplaceReturn = OUString(pBuffer); SensorCall(); return ReplaceReturn;}
}

OString GetCommandLineToken(int nToken, const OString& rLine)
{
    SensorCall();sal_Int32 nLen = rLine.getLength();
    SensorCall();if (!nLen)
        {/*21*/{rtl::OString  ReplaceReturn = rLine; SensorCall(); return ReplaceReturn;}/*22*/}

    SensorCall();int nActualToken = 0;
    char* pBuffer = static_cast<char*>(alloca( nLen + 1 ));
    const char* pRun = rLine.getStr();
    char* pLeap = NULL;

    SensorCall();while( *pRun && nActualToken <= nToken )
    {
        SensorCall();while( *pRun && isSpace( *pRun ) )
            {/*23*/SensorCall();pRun++;/*24*/}
        SensorCall();pLeap = pBuffer;
        SensorCall();while( *pRun && ! isSpace( *pRun ) )
        {
            SensorCall();if( *pRun == '\\' )
            {
                // escapement
                SensorCall();pRun++;
                *pLeap = *pRun;
                pLeap++;
                SensorCall();if( *pRun )
                    {/*25*/SensorCall();pRun++;/*26*/}
            }
            else {/*27*/SensorCall();if( *pRun == '`' )
                {/*29*/SensorCall();CopyUntil( pLeap, pRun, '`' );/*30*/}
            else {/*31*/SensorCall();if( *pRun == '\'' )
                {/*33*/SensorCall();CopyUntil( pLeap, pRun, '\'' );/*34*/}
            else {/*35*/SensorCall();if( *pRun == '"' )
                {/*37*/SensorCall();CopyUntil( pLeap, pRun, '"' );/*38*/}
            else
            {
                SensorCall();*pLeap = *pRun;
                pLeap++;
                pRun++;
            ;/*36*/}/*32*/}/*28*/}}
        }
        SensorCall();if( nActualToken != nToken )
            {/*39*/SensorCall();pBuffer[0] = 0;/*40*/}
        SensorCall();nActualToken++;
    }

    SensorCall();*pLeap = 0;

    {rtl::OString  ReplaceReturn = OString(pBuffer); SensorCall(); return ReplaceReturn;}
}

int GetCommandLineTokenCount(const OUString& rLine)
{
    SensorCall();if (rLine.isEmpty())
        {/*41*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*42*/}

    SensorCall();int nTokenCount = 0;
    const sal_Unicode *pRun = rLine.getStr();

    SensorCall();while( *pRun )
    {
        SensorCall();while( *pRun && isSpace( *pRun ) )
            {/*43*/SensorCall();pRun++;/*44*/}
        SensorCall();if( ! *pRun )
            {/*45*/SensorCall();break;/*46*/}
        SensorCall();while( *pRun && ! isSpace( *pRun ) )
        {
            SensorCall();if( *pRun == '\\' )
            {
                // escapement
                SensorCall();pRun++;
                SensorCall();if( *pRun )
                    {/*47*/SensorCall();pRun++;/*48*/}
            }
            else {/*49*/SensorCall();if( *pRun == '`' )
            {
                SensorCall();do {/*51*/SensorCall();pRun++;/*52*/} while( *pRun && *pRun != '`' );
                SensorCall();if( *pRun )
                    {/*53*/SensorCall();pRun++;/*54*/}
            }
            else {/*55*/SensorCall();if( *pRun == '\'' )
            {
                SensorCall();do {/*57*/SensorCall();pRun++;/*58*/} while( *pRun && *pRun != '\'' );
                SensorCall();if( *pRun )
                    {/*59*/SensorCall();pRun++;/*60*/}
            }
            else {/*61*/SensorCall();if( *pRun == '"' )
            {
                SensorCall();do {/*63*/SensorCall();pRun++;/*64*/} while( *pRun && *pRun != '"' );
                SensorCall();if( *pRun )
                    {/*65*/SensorCall();pRun++;/*66*/}
            }
            else
                {/*67*/SensorCall();pRun++;/*68*/}/*62*/}/*56*/}/*50*/}
        }
        SensorCall();nTokenCount++;
    }

    {int  ReplaceReturn = nTokenCount; SensorCall(); return ReplaceReturn;}
}

OUString WhitespaceToSpace( const OUString& rLine, bool bProtect )
{
    SensorCall();sal_Int32 nLen = rLine.getLength();
    SensorCall();if( ! nLen )
        {/*69*/{rtl::OUString  ReplaceReturn = OUString(); SensorCall(); return ReplaceReturn;}/*70*/}

    SensorCall();sal_Unicode *pBuffer = static_cast<sal_Unicode*>(alloca( sizeof(sal_Unicode)*(nLen + 1) ));
    const sal_Unicode *pRun = rLine.getStr();
    sal_Unicode *pLeap = pBuffer;

    SensorCall();while( *pRun )
    {
        SensorCall();if( *pRun && isSpace( *pRun ) )
        {
            SensorCall();*pLeap = ' ';
            pLeap++;
            pRun++;
        }
        SensorCall();while( *pRun && isSpace( *pRun ) )
            {/*71*/SensorCall();pRun++;/*72*/}
        SensorCall();while( *pRun && ! isSpace( *pRun ) )
        {
            SensorCall();if( *pRun == '\\' )
            {
                // escapement
                SensorCall();pRun++;
                *pLeap = *pRun;
                pLeap++;
                SensorCall();if( *pRun )
                    {/*73*/SensorCall();pRun++;/*74*/}
            }
            else {/*75*/SensorCall();if( bProtect && *pRun == '`' )
                {/*77*/SensorCall();CopyUntil( pLeap, pRun, '`', true );/*78*/}
            else {/*79*/SensorCall();if( bProtect && *pRun == '\'' )
                {/*81*/SensorCall();CopyUntil( pLeap, pRun, '\'', true );/*82*/}
            else {/*83*/SensorCall();if( bProtect && *pRun == '"' )
                {/*85*/SensorCall();CopyUntil( pLeap, pRun, '"', true );/*86*/}
            else
            {
                SensorCall();*pLeap = *pRun;
                ++pLeap;
                ++pRun;
            ;/*84*/}/*80*/}/*76*/}}
        }
    }

    SensorCall();*pLeap = 0;

    // there might be a space at beginning or end
    pLeap--;
    SensorCall();if( *pLeap == ' ' )
        {/*87*/SensorCall();*pLeap = 0;/*88*/}

    {rtl::OUString  ReplaceReturn = OUString(*pBuffer == ' ' ? pBuffer+1 : pBuffer); SensorCall(); return ReplaceReturn;}
}

OString WhitespaceToSpace(const OString& rLine, bool bProtect)
{
    SensorCall();sal_Int32 nLen = rLine.getLength();
    SensorCall();if (!nLen)
        {/*89*/{rtl::OString  ReplaceReturn = rLine; SensorCall(); return ReplaceReturn;}/*90*/}

    SensorCall();char *pBuffer = static_cast<char*>(alloca( nLen + 1 ));
    const char *pRun = rLine.getStr();
    char *pLeap = pBuffer;

    SensorCall();while( *pRun )
    {
        SensorCall();if( *pRun && isSpace( *pRun ) )
        {
            SensorCall();*pLeap = ' ';
            pLeap++;
            pRun++;
        }
        SensorCall();while( *pRun && isSpace( *pRun ) )
            {/*91*/SensorCall();pRun++;/*92*/}
        SensorCall();while( *pRun && ! isSpace( *pRun ) )
        {
            SensorCall();if( *pRun == '\\' )
            {
                // escapement
                SensorCall();pRun++;
                *pLeap = *pRun;
                pLeap++;
                SensorCall();if( *pRun )
                    {/*93*/SensorCall();pRun++;/*94*/}
            }
            else {/*95*/SensorCall();if( bProtect && *pRun == '`' )
                {/*97*/SensorCall();CopyUntil( pLeap, pRun, '`', true );/*98*/}
            else {/*99*/SensorCall();if( bProtect && *pRun == '\'' )
                {/*101*/SensorCall();CopyUntil( pLeap, pRun, '\'', true );/*102*/}
            else {/*103*/SensorCall();if( bProtect && *pRun == '"' )
                {/*105*/SensorCall();CopyUntil( pLeap, pRun, '"', true );/*106*/}
            else
            {
                SensorCall();*pLeap = *pRun;
                ++pLeap;
                ++pRun;
            ;/*104*/}/*100*/}/*96*/}}
        }
    }

    SensorCall();*pLeap = 0;

    // there might be a space at beginning or end
    pLeap--;
    SensorCall();if( *pLeap == ' ' )
        {/*107*/SensorCall();*pLeap = 0;/*108*/}

    {rtl::OString  ReplaceReturn = OString(*pBuffer == ' ' ? pBuffer+1 : pBuffer); SensorCall(); return ReplaceReturn;}
}

} // namespace

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
