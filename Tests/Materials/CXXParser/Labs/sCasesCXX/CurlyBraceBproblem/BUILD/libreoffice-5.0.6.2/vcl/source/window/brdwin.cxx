#include "var/tmp/sensor.h"
/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This file incorporates work covered by the following license notice:
 *
 *   Licensed to the Apache Software Foundation (ASF) under one or more
 *   contributor license agreements. See the NOTICE file distributed
 *   with this work for additional information regarding copyright
 *   ownership. The ASF licenses this file to you under the Apache
 *   License, Version 2.0 (the "License"); you may not use this file
 *   except in compliance with the License. You may obtain a copy of
 *   the License at http://www.apache.org/licenses/LICENSE-2.0 .
 */

#include <svids.hrc>
#include <svdata.hxx>
#include <brdwin.hxx>
#include <window.h>

#include <vcl/event.hxx>
#include <vcl/decoview.hxx>
#include <vcl/syswin.hxx>
#include <vcl/dockwin.hxx>
#include <vcl/floatwin.hxx>
#include <vcl/bitmap.hxx>
#include <vcl/gradient.hxx>
#include <vcl/image.hxx>
#include <vcl/virdev.hxx>
#include <vcl/help.hxx>
#include <vcl/edit.hxx>
#include <vcl/metric.hxx>
#include <vcl/settings.hxx>

using namespace ::com::sun::star::uno;

// useful caption height for title bar buttons
#define MIN_CAPTION_HEIGHT 18

static void ImplGetPinImage( DrawButtonFlags nStyle, bool bPinIn, Image& rImage )
{
    // load ImageList if not available yet
    SensorCall();ImplSVData* pSVData = ImplGetSVData();
    SensorCall();if ( !pSVData->maCtrlData.mpPinImgList )
    {
        SensorCall();ResMgr* pResMgr = ImplGetResMgr();
        pSVData->maCtrlData.mpPinImgList = new ImageList();
        SensorCall();if( pResMgr )
        {
            SensorCall();Color aMaskColor( 0x00, 0x00, 0xFF );
            pSVData->maCtrlData.mpPinImgList->InsertFromHorizontalBitmap
                ( ResId( SV_RESID_BITMAP_PIN, *pResMgr ), 4,
                  &aMaskColor, NULL, NULL, 0);
        }
    }

    // get and return Image
    SensorCall();sal_uInt16 nId;
    SensorCall();if ( nStyle & DrawButtonFlags::Pressed )
    {
        SensorCall();if ( bPinIn )
            {/*1*/SensorCall();nId = 4;/*2*/}
        else
            {/*3*/SensorCall();nId = 3;/*4*/}
    }
    else
    {
        SensorCall();if ( bPinIn )
            {/*5*/SensorCall();nId = 2;/*6*/}
        else
            {/*7*/SensorCall();nId = 1;/*8*/}
    }
    SensorCall();rImage = pSVData->maCtrlData.mpPinImgList->GetImage( nId );
SensorCall();}

namespace vcl {

void Window::ImplCalcSymbolRect( Rectangle& rRect )
{
    // Add border, not shown in the non-default representation,
    // as we want to use it for small buttons
    SensorCall();rRect.Left()--;
    rRect.Top()--;
    rRect.Right()++;
    rRect.Bottom()++;

    // we leave 5% room between the symbol and the button border
    long nExtraWidth = ((rRect.GetWidth()*50)+500)/1000;
    long nExtraHeight = ((rRect.GetHeight()*50)+500)/1000;
    rRect.Left()    += nExtraWidth;
    rRect.Right()   -= nExtraWidth;
    rRect.Top()     += nExtraHeight;
    rRect.Bottom()  -= nExtraHeight;
SensorCall();}

} /* namespace vcl */

static void ImplDrawBrdWinSymbol( vcl::RenderContext* pDev,
                                  const Rectangle& rRect, SymbolType eSymbol )
{
    // we leave 5% room between the symbol and the button border
    SensorCall();DecorationView  aDecoView( pDev );
    Rectangle       aTempRect = rRect;
    vcl::Window::ImplCalcSymbolRect( aTempRect );
    aDecoView.DrawSymbol( aTempRect, eSymbol,
                          pDev->GetSettings().GetStyleSettings().GetButtonTextColor() );
SensorCall();}

static void ImplDrawBrdWinSymbolButton( vcl::RenderContext* pDev,
                                        const Rectangle& rRect,
                                        SymbolType eSymbol, DrawButtonFlags nState )
{
    SensorCall();bool bMouseOver(nState & DrawButtonFlags::Highlight);
    nState &= ~DrawButtonFlags::Highlight;

    Rectangle aTempRect;
    vcl::Window *pWin = dynamic_cast< vcl::Window* >(pDev);
    SensorCall();if( pWin )
    {
        SensorCall();if( bMouseOver )
        {
            // provide a bright background for selection effect
            SensorCall();pDev->SetFillColor( pDev->GetSettings().GetStyleSettings().GetWindowColor() );
            pDev->SetLineColor();
            pDev->DrawRect( rRect );
            pWin->DrawSelectionBackground( rRect, 2, bool(nState & DrawButtonFlags::Pressed),
                                            true, false );
        }
        SensorCall();aTempRect = rRect;
        aTempRect.Left()+=3;
        aTempRect.Right()-=4;
        aTempRect.Top()+=3;
        aTempRect.Bottom()-=4;
    }
    else
    {
        SensorCall();DecorationView aDecoView( pDev );
        aTempRect = aDecoView.DrawButton( rRect, nState|DrawButtonFlags::Flat );
    }
    SensorCall();ImplDrawBrdWinSymbol( pDev, aTempRect, eSymbol );
SensorCall();}

// - ImplBorderWindowView -

ImplBorderWindowView::~ImplBorderWindowView()
{
SensorCall();SensorCall();}

bool ImplBorderWindowView::MouseMove( const MouseEvent& )
{
    {_Bool  ReplaceReturn = false; SensorCall(); return ReplaceReturn;}
}

bool ImplBorderWindowView::MouseButtonDown( const MouseEvent& )
{
    {_Bool  ReplaceReturn = false; SensorCall(); return ReplaceReturn;}
}

bool ImplBorderWindowView::Tracking( const TrackingEvent& )
{
    {_Bool  ReplaceReturn = false; SensorCall(); return ReplaceReturn;}
}

OUString ImplBorderWindowView::RequestHelp( const Point&, Rectangle& )
{
    {rtl::OUString  ReplaceReturn = OUString(); SensorCall(); return ReplaceReturn;}
}

Rectangle ImplBorderWindowView::GetMenuRect() const
{
    {Rectangle  ReplaceReturn = Rectangle(); SensorCall(); return ReplaceReturn;}
}

void ImplBorderWindowView::ImplInitTitle(ImplBorderFrameData* pData)
{
    SensorCall();ImplBorderWindow* pBorderWindow = pData->mpBorderWindow;

    SensorCall();if ( !(pBorderWindow->GetStyle() & (WB_MOVEABLE | WB_POPUP)) ||
          (pData->mnTitleType == BORDERWINDOW_TITLE_NONE) )
    {
        SensorCall();pData->mnTitleType   = BORDERWINDOW_TITLE_NONE;
        pData->mnTitleHeight = 0;
    }
    else
    {
        SensorCall();const StyleSettings& rStyleSettings = pData->mpOutDev->GetSettings().GetStyleSettings();
        SensorCall();if (pData->mnTitleType == BORDERWINDOW_TITLE_TEAROFF)
            {/*9*/SensorCall();pData->mnTitleHeight = rStyleSettings.GetTearOffTitleHeight();/*10*/}
        else
        {
            SensorCall();if (pData->mnTitleType == BORDERWINDOW_TITLE_SMALL)
            {
                pBorderWindow->SetPointFont(*pBorderWindow, rStyleSettings.GetFloatTitleFont() );
                SensorCall();pData->mnTitleHeight = rStyleSettings.GetFloatTitleHeight();
            }
            else // pData->mnTitleType == BORDERWINDOW_TITLE_NORMAL
            {
                // FIXME RenderContext
                pBorderWindow->SetPointFont(*pBorderWindow, rStyleSettings.GetTitleFont());
                SensorCall();pData->mnTitleHeight = rStyleSettings.GetTitleHeight();
            }
            SensorCall();long nTextHeight = pBorderWindow->GetTextHeight();
            SensorCall();if (nTextHeight > pData->mnTitleHeight)
                {/*11*/SensorCall();pData->mnTitleHeight = nTextHeight;/*12*/}
        }
    }
SensorCall();}

sal_uInt16 ImplBorderWindowView::ImplHitTest( ImplBorderFrameData* pData, const Point& rPos )
{
    SensorCall();ImplBorderWindow* pBorderWindow = pData->mpBorderWindow;

    SensorCall();if ( pData->maTitleRect.IsInside( rPos ) )
    {
        SensorCall();if ( pData->maCloseRect.IsInside( rPos ) )
            return BORDERWINDOW_HITTEST_CLOSE;
        else if ( pData->maRollRect.IsInside( rPos ) )
            return BORDERWINDOW_HITTEST_ROLL;
        else if ( pData->maMenuRect.IsInside( rPos ) )
            return BORDERWINDOW_HITTEST_MENU;
        else if ( pData->maDockRect.IsInside( rPos ) )
            return BORDERWINDOW_HITTEST_DOCK;
        else if ( pData->maHideRect.IsInside( rPos ) )
            return BORDERWINDOW_HITTEST_HIDE;
        else if ( pData->maHelpRect.IsInside( rPos ) )
            return BORDERWINDOW_HITTEST_HELP;
        else if ( pData->maPinRect.IsInside( rPos ) )
            return BORDERWINDOW_HITTEST_PIN;
        else
            return BORDERWINDOW_HITTEST_TITLE;
    }

    SensorCall();if ( (pBorderWindow->GetStyle() & WB_SIZEABLE) &&
         !pBorderWindow->mbRollUp )
    {
        SensorCall();long nSizeWidth = pData->mnNoTitleTop+pData->mnTitleHeight;
        SensorCall();if ( nSizeWidth < 16 )
            {/*13*/SensorCall();nSizeWidth = 16;/*14*/}

        // no corner resize for floating toolbars, which would lead to jumps while formatting
        // setting nSizeWidth = 0 will only return pure left,top,right,bottom
        SensorCall();if( pBorderWindow->GetStyle() & (WB_OWNERDRAWDECORATION | WB_POPUP) )
            {/*15*/SensorCall();nSizeWidth = 0;/*16*/}

        SensorCall();if ( rPos.X() < pData->mnLeftBorder )
        {
            SensorCall();if ( rPos.Y() < nSizeWidth )
                return BORDERWINDOW_HITTEST_TOPLEFT;
            else if ( rPos.Y() >= pData->mnHeight-nSizeWidth )
                return BORDERWINDOW_HITTEST_BOTTOMLEFT;
            else
                return BORDERWINDOW_HITTEST_LEFT;
        }
        else {/*17*/SensorCall();if ( rPos.X() >= pData->mnWidth-pData->mnRightBorder )
        {
            SensorCall();if ( rPos.Y() < nSizeWidth )
                return BORDERWINDOW_HITTEST_TOPRIGHT;
            else if ( rPos.Y() >= pData->mnHeight-nSizeWidth )
                return BORDERWINDOW_HITTEST_BOTTOMRIGHT;
            else
                return BORDERWINDOW_HITTEST_RIGHT;
        }
        else {/*19*/SensorCall();if ( rPos.Y() < pData->mnNoTitleTop )
        {
            SensorCall();if ( rPos.X() < nSizeWidth )
                return BORDERWINDOW_HITTEST_TOPLEFT;
            else if ( rPos.X() >= pData->mnWidth-nSizeWidth )
                return BORDERWINDOW_HITTEST_TOPRIGHT;
            else
                return BORDERWINDOW_HITTEST_TOP;
        }
        else {/*21*/SensorCall();if ( rPos.Y() >= pData->mnHeight-pData->mnBottomBorder )
        {
            SensorCall();if ( rPos.X() < nSizeWidth )
                return BORDERWINDOW_HITTEST_BOTTOMLEFT;
            else if ( rPos.X() >= pData->mnWidth-nSizeWidth )
                return BORDERWINDOW_HITTEST_BOTTOMRIGHT;
            else
                return BORDERWINDOW_HITTEST_BOTTOM;
        ;/*22*/}/*20*/}/*18*/}}
    }

    {sal_uInt16  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

bool ImplBorderWindowView::ImplMouseMove( ImplBorderFrameData* pData, const MouseEvent& rMEvt )
{
    SensorCall();DrawButtonFlags oldCloseState = pData->mnCloseState;
    DrawButtonFlags oldMenuState = pData->mnMenuState;
    pData->mnCloseState &= ~DrawButtonFlags::Highlight;
    pData->mnMenuState &= ~DrawButtonFlags::Highlight;

    Point aMousePos = rMEvt.GetPosPixel();
    sal_uInt16 nHitTest = ImplHitTest( pData, aMousePos );
    PointerStyle ePtrStyle = PointerStyle::Arrow;
    SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_LEFT )
        {/*23*/SensorCall();ePtrStyle = PointerStyle::WindowWSize;/*24*/}
    else {/*25*/SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_RIGHT )
        {/*27*/SensorCall();ePtrStyle = PointerStyle::WindowESize;/*28*/}
    else {/*29*/SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_TOP )
        {/*31*/SensorCall();ePtrStyle = PointerStyle::WindowNSize;/*32*/}
    else {/*33*/SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_BOTTOM )
        {/*35*/SensorCall();ePtrStyle = PointerStyle::WindowSSize;/*36*/}
    else {/*37*/SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_TOPLEFT )
        {/*39*/SensorCall();ePtrStyle = PointerStyle::WindowNWSize;/*40*/}
    else {/*41*/SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_BOTTOMRIGHT )
        {/*43*/SensorCall();ePtrStyle = PointerStyle::WindowSESize;/*44*/}
    else {/*45*/SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_TOPRIGHT )
        {/*47*/SensorCall();ePtrStyle = PointerStyle::WindowNESize;/*48*/}
    else {/*49*/SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_BOTTOMLEFT )
        {/*51*/SensorCall();ePtrStyle = PointerStyle::WindowSWSize;/*52*/}
    else {/*53*/if ( nHitTest & BORDERWINDOW_HITTEST_CLOSE );/*50*/}/*46*/}/*42*/}/*38*/}/*34*/}/*30*/}/*26*/}
        pData->mnCloseState |= DrawButtonFlags::Highlight;/*54*/}
    else if ( nHitTest & BORDERWINDOW_HITTEST_MENU )
        pData->mnMenuState |= DrawButtonFlags::Highlight;
    pData->mpBorderWindow->SetPointer( Pointer( ePtrStyle ) );

    if( pData->mnCloseState != oldCloseState )
        pData->mpBorderWindow->Invalidate( pData->maCloseRect );
    if( pData->mnMenuState != oldMenuState )
        pData->mpBorderWindow->Invalidate( pData->maMenuRect );

    {_Bool  ReplaceReturn = true; SensorCall(); return ReplaceReturn;}
}

OUString ImplBorderWindowView::ImplRequestHelp( ImplBorderFrameData* pData,
                                              const Point& rPos,
                                              Rectangle& rHelpRect )
{
    SensorCall();sal_uInt16 nHelpId = 0;
    OUString aHelpStr;
    sal_uInt16 nHitTest = ImplHitTest( pData, rPos );
    SensorCall();if ( nHitTest )
    {
        SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_CLOSE )
        {
            SensorCall();nHelpId     = SV_HELPTEXT_CLOSE;
            rHelpRect   = pData->maCloseRect;
        }
        else {/*55*/SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_ROLL )
        {
            SensorCall();if ( pData->mpBorderWindow->mbRollUp )
                nHelpId = SV_HELPTEXT_ROLLDOWN;
            else
                nHelpId = SV_HELPTEXT_ROLLUP;
            SensorCall();rHelpRect   = pData->maRollRect;
        }
        else {/*57*/SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_DOCK )
        {
            SensorCall();nHelpId     = SV_HELPTEXT_MAXIMIZE;
            rHelpRect   = pData->maDockRect;
        }
        else {/*59*/SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_HIDE )
        {
            SensorCall();nHelpId     = SV_HELPTEXT_MINIMIZE;
            rHelpRect   = pData->maHideRect;
        }
        else {/*61*/SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_HELP )
        {
            SensorCall();nHelpId     = SV_HELPTEXT_HELP;
            rHelpRect   = pData->maHelpRect;
        }
        else {/*63*/SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_PIN )
        {
            SensorCall();nHelpId     = SV_HELPTEXT_ALWAYSVISIBLE;
            rHelpRect   = pData->maPinRect;
        }
        else {/*65*/SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_TITLE )
        {
            SensorCall();if( !pData->maTitleRect.IsEmpty() )
            {
                // tooltip only if title truncated
                SensorCall();if( pData->mbTitleClipped )
                {
                    SensorCall();rHelpRect   = pData->maTitleRect;
                    // no help id, use window title as help string
                    aHelpStr    = pData->mpBorderWindow->GetText();
                }
            }
        ;/*66*/}/*64*/}/*62*/}/*60*/}/*58*/}/*56*/}}
    }

    SensorCall();if( nHelpId && ImplGetResMgr() )
        {/*67*/SensorCall();aHelpStr = ResId(nHelpId, *ImplGetResMgr()).toString();/*68*/}

    {rtl::OUString  ReplaceReturn = aHelpStr; SensorCall(); return ReplaceReturn;}
}

long ImplBorderWindowView::ImplCalcTitleWidth( const ImplBorderFrameData* pData )
{
    // title is not visible therefore no width
    SensorCall();if ( !pData->mnTitleHeight )
        {/*69*/{long  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*70*/}

    SensorCall();ImplBorderWindow* pBorderWindow = pData->mpBorderWindow;
    long nTitleWidth = pBorderWindow->GetTextWidth( pBorderWindow->GetText() )+6;
    nTitleWidth += pData->maPinRect.GetWidth();
    nTitleWidth += pData->maCloseRect.GetWidth();
    nTitleWidth += pData->maRollRect.GetWidth();
    nTitleWidth += pData->maDockRect.GetWidth();
    nTitleWidth += pData->maMenuRect.GetWidth();
    nTitleWidth += pData->maHideRect.GetWidth();
    nTitleWidth += pData->maHelpRect.GetWidth();
    nTitleWidth += pData->mnLeftBorder+pData->mnRightBorder;
    {long  ReplaceReturn = nTitleWidth; SensorCall(); return ReplaceReturn;}
}

// - ImplNoBorderWindowView -

ImplNoBorderWindowView::ImplNoBorderWindowView( ImplBorderWindow* )
{
SensorCall();SensorCall();}

void ImplNoBorderWindowView::Init( OutputDevice*, long, long )
{
SensorCall();SensorCall();}

void ImplNoBorderWindowView::GetBorder( sal_Int32& rLeftBorder, sal_Int32& rTopBorder,
                                        sal_Int32& rRightBorder, sal_Int32& rBottomBorder ) const
{
    SensorCall();rLeftBorder     = 0;
    rTopBorder      = 0;
    rRightBorder    = 0;
    rBottomBorder   = 0;
SensorCall();}

long ImplNoBorderWindowView::CalcTitleWidth() const
{
    {const long  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

void ImplNoBorderWindowView::DrawWindow(vcl::RenderContext&, sal_uInt16, const Point*)
{
SensorCall();SensorCall();}

// - ImplSmallBorderWindowView -
ImplSmallBorderWindowView::ImplSmallBorderWindowView( ImplBorderWindow* pBorderWindow )
    : mpBorderWindow(pBorderWindow)
    , mpOutDev(NULL)
    , mnWidth(0)
    , mnHeight(0)
    , mnLeftBorder(0)
    , mnTopBorder(0)
    , mnRightBorder(0)
    , mnBottomBorder(0)
    , mbNWFBorder(false)
{
SensorCall();SensorCall();}

void ImplSmallBorderWindowView::Init( OutputDevice* pDev, long nWidth, long nHeight )
{
    mpOutDev    = pDev;
    SensorCall();mnWidth     = nWidth;
    mnHeight    = nHeight;
    mbNWFBorder = false;

    vcl::Window *pWin = NULL, *pCtrl = NULL;
    if (mpOutDev->GetOutDevType() == OUTDEV_WINDOW)
        pWin = static_cast<vcl::Window*>(mpOutDev.get());

    if (pWin)
        pCtrl = mpBorderWindow->GetWindow(GetWindowType::Client);

    long nOrigLeftBorder = mnLeftBorder;
    long nOrigTopBorder = mnTopBorder;
    long nOrigRightBorder = mnRightBorder;
    long nOrigBottomBorder = mnBottomBorder;

    WindowBorderStyle nBorderStyle = mpBorderWindow->GetBorderStyle();
    SensorCall();if ( nBorderStyle & WindowBorderStyle::NOBORDER )
    {
        SensorCall();mnLeftBorder    = 0;
        mnTopBorder     = 0;
        mnRightBorder   = 0;
        mnBottomBorder  = 0;
    }
    else
    {
        // FIXME: this is currently only on OS X, check with other
        // platforms
        SensorCall();if( ImplGetSVData()->maNWFData.mbNoFocusRects && !( nBorderStyle & WindowBorderStyle::NWF ) )
        {
            // for native widget drawing we must find out what
            // control this border belongs to
            SensorCall();ControlType aCtrlType = 0;
            SensorCall();if (pCtrl)
            {
                SensorCall();switch( pCtrl->GetType() )
                {
                    case WINDOW_LISTBOX:
                        SensorCall();if( pCtrl->GetStyle() & WB_DROPDOWN )
                        {
                            SensorCall();aCtrlType = CTRL_LISTBOX;
                            mbNWFBorder = true;
                        }
                        SensorCall();break;
                    case WINDOW_COMBOBOX:
                        SensorCall();if( pCtrl->GetStyle() & WB_DROPDOWN )
                        {
                            SensorCall();aCtrlType = CTRL_COMBOBOX;
                            mbNWFBorder = true;
                        }
                        SensorCall();break;
                    case WINDOW_MULTILINEEDIT:
                        SensorCall();aCtrlType = CTRL_MULTILINE_EDITBOX;
                        mbNWFBorder = true;
                        SensorCall();break;
                    case WINDOW_EDIT:
                    case WINDOW_PATTERNFIELD:
                    case WINDOW_METRICFIELD:
                    case WINDOW_CURRENCYFIELD:
                    case WINDOW_DATEFIELD:
                    case WINDOW_TIMEFIELD:
                    case WINDOW_LONGCURRENCYFIELD:
                    case WINDOW_NUMERICFIELD:
                    case WINDOW_SPINFIELD:
                    case WINDOW_CALCINPUTLINE:
                        SensorCall();mbNWFBorder = true;
                        SensorCall();if (pCtrl->GetStyle() & WB_SPIN)
                            aCtrlType = CTRL_SPINBOX;
                        else
                            aCtrlType = CTRL_EDITBOX;
                        SensorCall();break;
                    default:
                        SensorCall();break;
                }
            }
            SensorCall();if( mbNWFBorder )
            {
                SensorCall();ImplControlValue aControlValue;
                Rectangle aCtrlRegion( (const Point&)Point(), Size( mnWidth < 10 ? 10 : mnWidth, mnHeight < 10 ? 10 : mnHeight ) );
                Rectangle aBounds( aCtrlRegion );
                Rectangle aContent( aCtrlRegion );
                SensorCall();if( pWin->GetNativeControlRegion( aCtrlType, PART_ENTIRE_CONTROL, aCtrlRegion,
                                                  ControlState::ENABLED, aControlValue, OUString(),
                                                  aBounds, aContent ) )
                {
                    SensorCall();mnLeftBorder    = aContent.Left() - aBounds.Left();
                    mnRightBorder   = aBounds.Right() - aContent.Right();
                    mnTopBorder     = aContent.Top() - aBounds.Top();
                    mnBottomBorder  = aBounds.Bottom() - aContent.Bottom();
                    SensorCall();if( mnWidth && mnHeight )
                    {

                        mpBorderWindow->SetPaintTransparent( true );
                        mpBorderWindow->SetBackground();
                        SensorCall();pCtrl->SetPaintTransparent( true );

                        vcl::Window* pCompoundParent = NULL;
                        SensorCall();if( pWin->GetParent() && pWin->GetParent()->IsCompoundControl() )
                            {/*71*/SensorCall();pCompoundParent = pWin->GetParent();/*72*/}

                        SensorCall();if( pCompoundParent )
                            {/*73*/SensorCall();pCompoundParent->SetPaintTransparent( true );/*74*/}

                        SensorCall();if( mnWidth < aBounds.GetWidth() || mnHeight < aBounds.GetHeight() )
                        {
                            SensorCall();if( ! pCompoundParent ) // compound controls have to fix themselves
                            {
                                SensorCall();Point aPos( mpBorderWindow->GetPosPixel() );
                                SensorCall();if( mnWidth < aBounds.GetWidth() )
                                    {/*75*/SensorCall();aPos.X() -= (aBounds.GetWidth() - mnWidth) / 2;/*76*/}
                                SensorCall();if( mnHeight < aBounds.GetHeight() )
                                    {/*77*/SensorCall();aPos.Y() -= (aBounds.GetHeight() - mnHeight) / 2;/*78*/}
                                mpBorderWindow->SetPosSizePixel( aPos, aBounds.GetSize() );
                            }
                        }
                    }
                }
                else
                    {/*79*/SensorCall();mbNWFBorder = false;/*80*/}
            }
        }

        SensorCall();if( ! mbNWFBorder )
        {
            SensorCall();DrawFrameStyle nStyle = DrawFrameStyle::NONE;
            DrawFrameFlags nFlags = DrawFrameFlags::NoDraw;
            // move border outside if border was converted or if the BorderWindow is a frame window,
            SensorCall();if ( mpBorderWindow->mbSmallOutBorder )
                {/*81*/SensorCall();nStyle = DrawFrameStyle::DoubleOut;/*82*/}
            else {/*83*/SensorCall();if ( nBorderStyle & WindowBorderStyle::NWF )
                {/*85*/SensorCall();nStyle = DrawFrameStyle::NWF;/*86*/}
            else
                {/*87*/SensorCall();nStyle = DrawFrameStyle::DoubleIn;/*88*/}/*84*/}
            if ( nBorderStyle & WindowBorderStyle::MONO )
                nFlags |= DrawFrameFlags::Mono;

            SensorCall();DecorationView  aDecoView( mpOutDev );
            Rectangle       aRect( 0, 0, 10, 10 );
            Rectangle       aCalcRect = aDecoView.DrawFrame( aRect, nStyle, nFlags );
            mnLeftBorder    = aCalcRect.Left();
            mnTopBorder     = aCalcRect.Top();
            mnRightBorder   = aRect.Right()-aCalcRect.Right();
            mnBottomBorder  = aRect.Bottom()-aCalcRect.Bottom();
        }
    }

    SensorCall();if (pCtrl)
    {
        //fdo#57090 If the borders have changed, then trigger a queue_resize on
        //the bordered window, which will resync its borders at that point
        SensorCall();if (nOrigLeftBorder != mnLeftBorder ||
            nOrigTopBorder != mnTopBorder ||
            nOrigRightBorder != mnRightBorder ||
            nOrigBottomBorder != mnBottomBorder)
        {
            SensorCall();pCtrl->queue_resize();
        }
    }
SensorCall();}

void ImplSmallBorderWindowView::GetBorder( sal_Int32& rLeftBorder, sal_Int32& rTopBorder,
                                           sal_Int32& rRightBorder, sal_Int32& rBottomBorder ) const
{
    SensorCall();rLeftBorder     = mnLeftBorder;
    rTopBorder      = mnTopBorder;
    rRightBorder    = mnRightBorder;
    rBottomBorder   = mnBottomBorder;
SensorCall();}

long ImplSmallBorderWindowView::CalcTitleWidth() const
{
    {const long  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

void ImplSmallBorderWindowView::DrawWindow(vcl::RenderContext& rRenderContext, sal_uInt16 nDrawFlags, const Point*)
{
    SensorCall();WindowBorderStyle nBorderStyle = mpBorderWindow->GetBorderStyle();
    SensorCall();if (nBorderStyle & WindowBorderStyle::NOBORDER)
        {/*89*/SensorCall();return;/*90*/}

    SensorCall();bool bNativeOK = false;
    // for native widget drawing we must find out what
    // control this border belongs to
    vcl::Window* pCtrl = mpBorderWindow->GetWindow(GetWindowType::Client);

    ControlType aCtrlType = 0;
    ControlPart aCtrlPart = PART_ENTIRE_CONTROL;
    SensorCall();if (pCtrl)
    {
        SensorCall();switch (pCtrl->GetType())
        {
            case WINDOW_MULTILINEEDIT:
                SensorCall();aCtrlType = CTRL_MULTILINE_EDITBOX;
                SensorCall();break;
            case WINDOW_EDIT:
            case WINDOW_PATTERNFIELD:
            case WINDOW_METRICFIELD:
            case WINDOW_CURRENCYFIELD:
            case WINDOW_DATEFIELD:
            case WINDOW_TIMEFIELD:
            case WINDOW_LONGCURRENCYFIELD:
            case WINDOW_NUMERICFIELD:
            case WINDOW_SPINFIELD:
            case WINDOW_CALCINPUTLINE:
                SensorCall();if (pCtrl->GetStyle() & WB_SPIN)
                    aCtrlType = CTRL_SPINBOX;
                else
                    aCtrlType = CTRL_EDITBOX;
                SensorCall();break;

            case WINDOW_LISTBOX:
            case WINDOW_MULTILISTBOX:
            case WINDOW_TREELISTBOX:
                SensorCall();aCtrlType = CTRL_LISTBOX;
                SensorCall();if (pCtrl->GetStyle() & WB_DROPDOWN)
                    aCtrlPart = PART_ENTIRE_CONTROL;
                else
                    aCtrlPart = PART_WINDOW;
                SensorCall();break;

            case WINDOW_LISTBOXWINDOW:
                SensorCall();aCtrlType = CTRL_LISTBOX;
                aCtrlPart = PART_WINDOW;
                SensorCall();break;

            case WINDOW_COMBOBOX:
            case WINDOW_PATTERNBOX:
            case WINDOW_NUMERICBOX:
            case WINDOW_METRICBOX:
            case WINDOW_CURRENCYBOX:
            case WINDOW_DATEBOX:
            case WINDOW_TIMEBOX:
            case WINDOW_LONGCURRENCYBOX:
                SensorCall();if (pCtrl->GetStyle() & WB_DROPDOWN)
                {
                    SensorCall();aCtrlType = CTRL_COMBOBOX;
                    aCtrlPart = PART_ENTIRE_CONTROL;
                }
                else
                {
                    SensorCall();aCtrlType = CTRL_LISTBOX;
                    aCtrlPart = PART_WINDOW;
                }
                SensorCall();break;

            default:
                SensorCall();break;
        }
    }

    SensorCall();if (aCtrlType && pCtrl->IsNativeControlSupported(aCtrlType, aCtrlPart))
    {
        SensorCall();ImplControlValue aControlValue;
        ControlState nState = ControlState::ENABLED;

        if (!mpBorderWindow->IsEnabled())
            nState &= ~ControlState::ENABLED;
        SensorCall();if (mpBorderWindow->HasFocus())
            {/*91*/nState |= ControlState::FOCUSED;/*92*/}
        else {/*93*/SensorCall();if(mbNWFBorder)
        {
            // FIXME: this is currently only on OS X, see if other platforms can profit

            // FIXME: for OS X focus rings all controls need to support GetNativeControlRegion
            // for the dropdown style
            if (pCtrl->HasFocus() || pCtrl->HasChildPathFocus())
                nState |= ControlState::FOCUSED;
        ;/*94*/}}

        SensorCall();bool bMouseOver = false;
        vcl::Window *pCtrlChild = pCtrl->GetWindow(GetWindowType::FirstChild);
        SensorCall();while(pCtrlChild && !(bMouseOver = pCtrlChild->IsMouseOver()))
        {
            SensorCall();pCtrlChild = pCtrlChild->GetWindow(GetWindowType::Next);
        }

        if (bMouseOver)
            nState |= ControlState::ROLLOVER;

        SensorCall();Point aPoint;
        Rectangle aCtrlRegion(aPoint, Size(mnWidth, mnHeight));

        Rectangle aBoundingRgn(aPoint, Size(mnWidth, mnHeight));
        Rectangle aContentRgn(aCtrlRegion);
        SensorCall();if (!ImplGetSVData()->maNWFData.mbCanDrawWidgetAnySize &&
            rRenderContext.GetNativeControlRegion(aCtrlType, aCtrlPart, aCtrlRegion,
                                         nState, aControlValue, OUString(),
                                         aBoundingRgn, aContentRgn))
        {
            SensorCall();aCtrlRegion=aContentRgn;
        }

        SensorCall();bNativeOK = rRenderContext.DrawNativeControl(aCtrlType, aCtrlPart, aCtrlRegion, nState, aControlValue, OUString());

        // if the native theme draws the spinbuttons in one call, make sure the proper settings
        // are passed, this might force a redraw though.... (TODO: improve)
        SensorCall();if ((aCtrlType == CTRL_SPINBOX) && !pCtrl->IsNativeControlSupported(CTRL_SPINBOX, PART_BUTTON_UP))
        {
            SensorCall();Edit* pEdit = static_cast<Edit*>(pCtrl)->GetSubEdit();
            if (pEdit && !pEdit->SupportsDoubleBuffering())
                pCtrl->Paint(*pCtrl, Rectangle());  // make sure the buttons are also drawn as they might overwrite the border
        }
    }

    SensorCall();if (bNativeOK)
        {/*95*/SensorCall();return;/*96*/}

    SensorCall();if (nDrawFlags & BORDERWINDOW_DRAW_FRAME)
    {
        SensorCall();DrawFrameStyle nStyle = DrawFrameStyle::NONE;
        DrawFrameFlags nFlags = DrawFrameFlags::NONE;
        // move border outside if border was converted or if the border window is a frame window,
        SensorCall();if (mpBorderWindow->mbSmallOutBorder)
            {/*97*/SensorCall();nStyle = DrawFrameStyle::DoubleOut;/*98*/}
        else {/*99*/SensorCall();if (nBorderStyle & WindowBorderStyle::NWF)
            {/*101*/SensorCall();nStyle = DrawFrameStyle::NWF;/*102*/}
        else
            {/*103*/SensorCall();nStyle = DrawFrameStyle::DoubleIn;/*104*/}/*100*/}
        if (nBorderStyle & WindowBorderStyle::MONO)
            nFlags |= DrawFrameFlags::Mono;
        if (nBorderStyle & WindowBorderStyle::MENU)
            nFlags |= DrawFrameFlags::Menu;
        // tell DrawFrame that we're drawing a window border of a frame window to avoid round corners
        if (mpBorderWindow == mpBorderWindow->ImplGetFrameWindow())
            nFlags |= DrawFrameFlags::WindowBorder;

        SensorCall();DecorationView aDecoView(&rRenderContext);
        Point aTmpPoint;
        Rectangle aInRect(aTmpPoint, Size(mnWidth, mnHeight));
        aDecoView.DrawFrame(aInRect, nStyle, nFlags);
    }
SensorCall();}

// - ImplStdBorderWindowView -

ImplStdBorderWindowView::ImplStdBorderWindowView( ImplBorderWindow* pBorderWindow )
{
SensorCall();    maFrameData.mpBorderWindow  = pBorderWindow;
    maFrameData.mbDragFull      = false;
    maFrameData.mnHitTest       = 0;
    maFrameData.mnPinState      = DrawButtonFlags::NONE;
    maFrameData.mnCloseState    = DrawButtonFlags::NONE;
    maFrameData.mnRollState     = DrawButtonFlags::NONE;
    maFrameData.mnDockState     = DrawButtonFlags::NONE;
    maFrameData.mnMenuState     = DrawButtonFlags::NONE;
    maFrameData.mnHideState     = DrawButtonFlags::NONE;
    maFrameData.mnHelpState     = DrawButtonFlags::NONE;
    maFrameData.mbTitleClipped  = false;

    mpATitleVirDev              = NULL;
    mpDTitleVirDev              = NULL;
SensorCall();}

ImplStdBorderWindowView::~ImplStdBorderWindowView()
{
SensorCall();    mpATitleVirDev.disposeAndClear();
    mpDTitleVirDev.disposeAndClear();
SensorCall();}

bool ImplStdBorderWindowView::MouseMove( const MouseEvent& rMEvt )
{
SensorCall();    return ImplMouseMove( &maFrameData, rMEvt );
SensorCall();}

bool ImplStdBorderWindowView::MouseButtonDown( const MouseEvent& rMEvt )
{
    SensorCall();ImplBorderWindow* pBorderWindow = maFrameData.mpBorderWindow;

    SensorCall();if ( rMEvt.IsLeft() || rMEvt.IsRight() )
    {
        maFrameData.maMouseOff = rMEvt.GetPosPixel();
        maFrameData.mnHitTest = ImplHitTest( &maFrameData, maFrameData.maMouseOff );
        SensorCall();if ( maFrameData.mnHitTest )
        {
            SensorCall();DragFullOptions nDragFullTest = DragFullOptions::NONE;
            bool bTracking = true;
            bool bHitTest = true;

            SensorCall();if ( maFrameData.mnHitTest & BORDERWINDOW_HITTEST_CLOSE )
            {
                maFrameData.mnCloseState |= DrawButtonFlags::Pressed;
                SensorCall();pBorderWindow->InvalidateBorder();
            }
            else {/*105*/SensorCall();if ( maFrameData.mnHitTest & BORDERWINDOW_HITTEST_ROLL )
            {
                maFrameData.mnRollState |= DrawButtonFlags::Pressed;
                SensorCall();pBorderWindow->InvalidateBorder();
            }
            else {/*107*/SensorCall();if ( maFrameData.mnHitTest & BORDERWINDOW_HITTEST_DOCK )
            {
                maFrameData.mnDockState |= DrawButtonFlags::Pressed;
                SensorCall();pBorderWindow->InvalidateBorder();
            }
            else {/*109*/SensorCall();if ( maFrameData.mnHitTest & BORDERWINDOW_HITTEST_MENU )
            {
                maFrameData.mnMenuState |= DrawButtonFlags::Pressed;
                SensorCall();pBorderWindow->InvalidateBorder();

                // call handler already on mouse down
                SensorCall();if ( pBorderWindow->ImplGetClientWindow()->IsSystemWindow() )
                {
                    SensorCall();SystemWindow* pClientWindow = static_cast<SystemWindow*>(pBorderWindow->ImplGetClientWindow());
                    pClientWindow->TitleButtonClick( TitleButton::Menu );
                }
            }
            else {/*111*/SensorCall();if ( maFrameData.mnHitTest & BORDERWINDOW_HITTEST_HIDE )
            {
                maFrameData.mnHideState |= DrawButtonFlags::Pressed;
                SensorCall();pBorderWindow->InvalidateBorder();
            }
            else {/*113*/SensorCall();if ( maFrameData.mnHitTest & BORDERWINDOW_HITTEST_HELP )
            {
                maFrameData.mnHelpState |= DrawButtonFlags::Pressed;
                SensorCall();pBorderWindow->InvalidateBorder();
            }
            else {/*115*/SensorCall();if ( maFrameData.mnHitTest & BORDERWINDOW_HITTEST_PIN )
            {
                maFrameData.mnPinState |= DrawButtonFlags::Pressed;
                SensorCall();pBorderWindow->InvalidateBorder();
            }
            else
            {
                SensorCall();if ( rMEvt.GetClicks() == 1 )
                {
                    SensorCall();if ( bTracking )
                    {
                        SensorCall();Point   aPos         = pBorderWindow->GetPosPixel();
                        Size    aSize        = pBorderWindow->GetOutputSizePixel();
                        maFrameData.mnTrackX      = aPos.X();
                        maFrameData.mnTrackY      = aPos.Y();
                        maFrameData.mnTrackWidth  = aSize.Width();
                        maFrameData.mnTrackHeight = aSize.Height();

                        SensorCall();if ( maFrameData.mnHitTest & BORDERWINDOW_HITTEST_TITLE )
                            {/*117*/SensorCall();nDragFullTest = DragFullOptions::WindowMove;/*118*/}
                        else
                            {/*119*/SensorCall();nDragFullTest = DragFullOptions::WindowSize;/*120*/}
                    }
                }
                else
                {
                    SensorCall();bTracking = false;

                    SensorCall();if ( (maFrameData.mnHitTest & BORDERWINDOW_DRAW_TITLE) &&
                         ((rMEvt.GetClicks() % 2) == 0) )
                    {
                        maFrameData.mnHitTest = 0;
                        SensorCall();bHitTest = false;

                        SensorCall();if ( pBorderWindow->ImplGetClientWindow()->IsSystemWindow() )
                        {
                            SensorCall();SystemWindow* pClientWindow = static_cast<SystemWindow*>(pBorderWindow->ImplGetClientWindow());
                            SensorCall();if ( true /*pBorderWindow->mbDockBtn*/ )   // always perform docking on double click, no button required
                                {/*121*/SensorCall();pClientWindow->TitleButtonClick( TitleButton::Docking );/*122*/}
                            else {/*123*/SensorCall();if ( pBorderWindow->GetStyle() & WB_ROLLABLE )
                            {
                                SensorCall();if ( pClientWindow->IsRollUp() )
                                    {/*125*/SensorCall();pClientWindow->RollDown();/*126*/}
                                else
                                    {/*127*/SensorCall();pClientWindow->RollUp();/*128*/}
                                SensorCall();pClientWindow->Roll();
                            ;/*124*/}}
                        }
                    }
                }
            ;/*116*/}/*114*/}/*112*/}/*110*/}/*108*/}/*106*/}}

            SensorCall();if ( bTracking )
            {
                maFrameData.mbDragFull = false;
                if ( nDragFullTest != DragFullOptions::NONE )
                    maFrameData.mbDragFull = true;   // always fulldrag for proper docking, ignore system settings
                pBorderWindow->StartTracking();
            }
            else {/*129*/if ( bHitTest )
                maFrameData.mnHitTest = 0;/*130*/}
        }
    }

    {_Bool  ReplaceReturn = true; SensorCall(); return ReplaceReturn;}
}

bool ImplStdBorderWindowView::Tracking( const TrackingEvent& rTEvt )
{
    SensorCall();ImplBorderWindow* pBorderWindow = maFrameData.mpBorderWindow;

    SensorCall();if ( rTEvt.IsTrackingEnded() )
    {
        SensorCall();sal_uInt16 nHitTest = maFrameData.mnHitTest;
        maFrameData.mnHitTest = 0;

        SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_CLOSE )
        {
            SensorCall();if ( maFrameData.mnCloseState & DrawButtonFlags::Pressed )
            {
                maFrameData.mnCloseState &= ~DrawButtonFlags::Pressed;
                SensorCall();pBorderWindow->InvalidateBorder();

                // do not call a Click-Handler when aborting
                SensorCall();if ( !rTEvt.IsTrackingCanceled() )
                {
                    // dispatch to correct window type (why is Close() not virtual ??? )
                    // TODO: make Close() virtual
                    SensorCall();vcl::Window *pWin = pBorderWindow->ImplGetClientWindow()->ImplGetWindow();
                    SystemWindow  *pSysWin  = dynamic_cast<SystemWindow* >(pWin);
                    DockingWindow *pDockWin = dynamic_cast<DockingWindow*>(pWin);
                    SensorCall();if ( pSysWin )
                        {/*131*/SensorCall();pSysWin->Close();/*132*/}
                    else {/*133*/SensorCall();if ( pDockWin )
                        {/*135*/SensorCall();pDockWin->Close();/*136*/}/*134*/}
                }
            }
        }
        else {/*137*/SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_ROLL )
        {
            SensorCall();if ( maFrameData.mnRollState & DrawButtonFlags::Pressed )
            {
                maFrameData.mnRollState &= ~DrawButtonFlags::Pressed;
                SensorCall();pBorderWindow->InvalidateBorder();

                // do not call a Click-Handler when aborting
                SensorCall();if ( !rTEvt.IsTrackingCanceled() )
                {
                    SensorCall();if ( pBorderWindow->ImplGetClientWindow()->IsSystemWindow() )
                    {
                        SensorCall();SystemWindow* pClientWindow = static_cast<SystemWindow*>(pBorderWindow->ImplGetClientWindow());
                        SensorCall();if ( pClientWindow->IsRollUp() )
                            {/*139*/SensorCall();pClientWindow->RollDown();/*140*/}
                        else
                            {/*141*/SensorCall();pClientWindow->RollUp();/*142*/}
                        SensorCall();pClientWindow->Roll();
                    }
                }
            }
        }
        else {/*143*/SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_DOCK )
        {
            SensorCall();if ( maFrameData.mnDockState & DrawButtonFlags::Pressed )
            {
                maFrameData.mnDockState &= ~DrawButtonFlags::Pressed;
                SensorCall();pBorderWindow->InvalidateBorder();

                // do not call a Click-Handler when aborting
                SensorCall();if ( !rTEvt.IsTrackingCanceled() )
                {
                    SensorCall();if ( pBorderWindow->ImplGetClientWindow()->IsSystemWindow() )
                    {
                        SensorCall();SystemWindow* pClientWindow = static_cast<SystemWindow*>(pBorderWindow->ImplGetClientWindow());
                        pClientWindow->TitleButtonClick( TitleButton::Docking );
                    }
                }
            }
        }
        else {/*145*/SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_MENU )
        {
            SensorCall();if ( maFrameData.mnMenuState & DrawButtonFlags::Pressed )
            {
                maFrameData.mnMenuState &= ~DrawButtonFlags::Pressed;
                SensorCall();pBorderWindow->InvalidateBorder();

                // handler already called on mouse down
            }
        }
        else {/*147*/SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_HIDE )
        {
            SensorCall();if ( maFrameData.mnHideState & DrawButtonFlags::Pressed )
            {
                maFrameData.mnHideState &= ~DrawButtonFlags::Pressed;
                SensorCall();pBorderWindow->InvalidateBorder();

                // do not call a Click-Handler when aborting
                SensorCall();if ( !rTEvt.IsTrackingCanceled() )
                {
                    SensorCall();if ( pBorderWindow->ImplGetClientWindow()->IsSystemWindow() )
                    {
                        SensorCall();SystemWindow* pClientWindow = static_cast<SystemWindow*>(pBorderWindow->ImplGetClientWindow());
                        pClientWindow->TitleButtonClick( TitleButton::Hide );
                    }
                }
            }
        }
        else {/*149*/SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_HELP )
        {
            SensorCall();if ( maFrameData.mnHelpState & DrawButtonFlags::Pressed )
            {
                maFrameData.mnHelpState &= ~DrawButtonFlags::Pressed;
                SensorCall();pBorderWindow->InvalidateBorder();

                // do not call a Click-Handler when aborting
                SensorCall();if ( !rTEvt.IsTrackingCanceled() )
                {
                }
            }
        }
        else {/*151*/SensorCall();if ( nHitTest & BORDERWINDOW_HITTEST_PIN )
        {
            SensorCall();if ( maFrameData.mnPinState & DrawButtonFlags::Pressed )
            {
                maFrameData.mnPinState &= ~DrawButtonFlags::Pressed;
                SensorCall();pBorderWindow->InvalidateBorder();

                // do not call a Click-Handler when aborting
                SensorCall();if ( !rTEvt.IsTrackingCanceled() )
                {
                    SensorCall();if ( pBorderWindow->ImplGetClientWindow()->IsSystemWindow() )
                    {
                        SensorCall();SystemWindow* pClientWindow = static_cast<SystemWindow*>(pBorderWindow->ImplGetClientWindow());
                        pClientWindow->SetPin( !pClientWindow->IsPinned() );
                        pClientWindow->Pin();
                    }
                }
            }
        }
        else
        {
            SensorCall();if ( maFrameData.mbDragFull )
            {
                // restore old state when aborting
                if ( rTEvt.IsTrackingCanceled() )
                    pBorderWindow->SetPosSizePixel( Point( maFrameData.mnTrackX, maFrameData.mnTrackY ), Size( maFrameData.mnTrackWidth, maFrameData.mnTrackHeight ) );
            }
            else
            {
                pBorderWindow->HideTracking();
                if ( !rTEvt.IsTrackingCanceled() )
                    pBorderWindow->SetPosSizePixel( Point( maFrameData.mnTrackX, maFrameData.mnTrackY ), Size( maFrameData.mnTrackWidth, maFrameData.mnTrackHeight ) );
            }

            SensorCall();if ( !rTEvt.IsTrackingCanceled() )
            {
                SensorCall();if ( pBorderWindow->ImplGetClientWindow()->ImplIsFloatingWindow() )
                {
                    if ( static_cast<FloatingWindow*>(pBorderWindow->ImplGetClientWindow())->IsInPopupMode() )
                        static_cast<FloatingWindow*>(pBorderWindow->ImplGetClientWindow())->EndPopupMode( FloatWinPopupEndFlags::TearOff );
                }
            }
        ;/*152*/}/*150*/}/*148*/}/*146*/}/*144*/}/*138*/}}
    }
    else {/*153*/SensorCall();if ( !rTEvt.GetMouseEvent().IsSynthetic() )
    {
        SensorCall();Point aMousePos = rTEvt.GetMouseEvent().GetPosPixel();

        SensorCall();if ( maFrameData.mnHitTest & BORDERWINDOW_HITTEST_CLOSE )
        {
            SensorCall();if ( maFrameData.maCloseRect.IsInside( aMousePos ) )
            {
                SensorCall();if ( !(maFrameData.mnCloseState & DrawButtonFlags::Pressed) )
                {
                    maFrameData.mnCloseState |= DrawButtonFlags::Pressed;
                    SensorCall();pBorderWindow->InvalidateBorder();
                }
            }
            else
            {
                SensorCall();if ( maFrameData.mnCloseState & DrawButtonFlags::Pressed )
                {
                    maFrameData.mnCloseState &= ~DrawButtonFlags::Pressed;
                    SensorCall();pBorderWindow->InvalidateBorder();
                }
            }
        }
        else {/*155*/SensorCall();if ( maFrameData.mnHitTest & BORDERWINDOW_HITTEST_ROLL )
        {
            SensorCall();if ( maFrameData.maRollRect.IsInside( aMousePos ) )
            {
                SensorCall();if ( !(maFrameData.mnRollState & DrawButtonFlags::Pressed) )
                {
                    maFrameData.mnRollState |= DrawButtonFlags::Pressed;
                    SensorCall();pBorderWindow->InvalidateBorder();
                }
            }
            else
            {
                SensorCall();if ( maFrameData.mnRollState & DrawButtonFlags::Pressed )
                {
                    maFrameData.mnRollState &= ~DrawButtonFlags::Pressed;
                    SensorCall();pBorderWindow->InvalidateBorder();
                }
            }
        }
        else {/*157*/SensorCall();if ( maFrameData.mnHitTest & BORDERWINDOW_HITTEST_DOCK )
        {
            SensorCall();if ( maFrameData.maDockRect.IsInside( aMousePos ) )
            {
                SensorCall();if ( !(maFrameData.mnDockState & DrawButtonFlags::Pressed) )
                {
                    maFrameData.mnDockState |= DrawButtonFlags::Pressed;
                    SensorCall();pBorderWindow->InvalidateBorder();
                }
            }
            else
            {
                SensorCall();if ( maFrameData.mnDockState & DrawButtonFlags::Pressed )
                {
                    maFrameData.mnDockState &= ~DrawButtonFlags::Pressed;
                    SensorCall();pBorderWindow->InvalidateBorder();
                }
            }
        }
        else {/*159*/SensorCall();if ( maFrameData.mnHitTest & BORDERWINDOW_HITTEST_MENU )
        {
            SensorCall();if ( maFrameData.maMenuRect.IsInside( aMousePos ) )
            {
                SensorCall();if ( !(maFrameData.mnMenuState & DrawButtonFlags::Pressed) )
                {
                    maFrameData.mnMenuState |= DrawButtonFlags::Pressed;
                    SensorCall();pBorderWindow->InvalidateBorder();
                }
            }
            else
            {
                SensorCall();if ( maFrameData.mnMenuState & DrawButtonFlags::Pressed )
                {
                    maFrameData.mnMenuState &= ~DrawButtonFlags::Pressed;
                    SensorCall();pBorderWindow->InvalidateBorder();
                }
            }
        }
        else {/*161*/SensorCall();if ( maFrameData.mnHitTest & BORDERWINDOW_HITTEST_HIDE )
        {
            SensorCall();if ( maFrameData.maHideRect.IsInside( aMousePos ) )
            {
                SensorCall();if ( !(maFrameData.mnHideState & DrawButtonFlags::Pressed) )
                {
                    maFrameData.mnHideState |= DrawButtonFlags::Pressed;
                    SensorCall();pBorderWindow->InvalidateBorder();
                }
            }
            else
            {
                SensorCall();if ( maFrameData.mnHideState & DrawButtonFlags::Pressed )
                {
                    maFrameData.mnHideState &= ~DrawButtonFlags::Pressed;
                    SensorCall();pBorderWindow->InvalidateBorder();
                }
            }
        }
        else {/*163*/SensorCall();if ( maFrameData.mnHitTest & BORDERWINDOW_HITTEST_HELP )
        {
            SensorCall();if ( maFrameData.maHelpRect.IsInside( aMousePos ) )
            {
                SensorCall();if ( !(maFrameData.mnHelpState & DrawButtonFlags::Pressed) )
                {
                    maFrameData.mnHelpState |= DrawButtonFlags::Pressed;
                    SensorCall();pBorderWindow->InvalidateBorder();
                }
            }
            else
            {
                SensorCall();if ( maFrameData.mnHelpState & DrawButtonFlags::Pressed )
                {
                    maFrameData.mnHelpState &= ~DrawButtonFlags::Pressed;
                    SensorCall();pBorderWindow->InvalidateBorder();
                }
            }
        }
        else {/*165*/SensorCall();if ( maFrameData.mnHitTest & BORDERWINDOW_HITTEST_PIN )
        {
            SensorCall();if ( maFrameData.maPinRect.IsInside( aMousePos ) )
            {
                SensorCall();if ( !(maFrameData.mnPinState & DrawButtonFlags::Pressed) )
                {
                    maFrameData.mnPinState |= DrawButtonFlags::Pressed;
                    SensorCall();pBorderWindow->InvalidateBorder();
                }
            }
            else
            {
                SensorCall();if ( maFrameData.mnPinState & DrawButtonFlags::Pressed )
                {
                    maFrameData.mnPinState &= ~DrawButtonFlags::Pressed;
                    SensorCall();pBorderWindow->InvalidateBorder();
                }
            }
        }
        else
        {
            aMousePos.X()    -= maFrameData.maMouseOff.X();
            aMousePos.Y()    -= maFrameData.maMouseOff.Y();

            SensorCall();if ( maFrameData.mnHitTest & BORDERWINDOW_HITTEST_TITLE )
            {
                maFrameData.mpBorderWindow->SetPointer( Pointer( PointerStyle::Move ) );

                SensorCall();Point aPos = pBorderWindow->GetPosPixel();
                aPos.X() += aMousePos.X();
                aPos.Y() += aMousePos.Y();
                SensorCall();if ( maFrameData.mbDragFull )
                {
                    SensorCall();pBorderWindow->MoveToByDrag(aPos);
                    pBorderWindow->ImplUpdateAll();
                    pBorderWindow->ImplGetFrameWindow()->ImplUpdateAll();
                }
                else
                {
                    maFrameData.mnTrackX = aPos.X();
                    maFrameData.mnTrackY = aPos.Y();
                    pBorderWindow->ShowTracking( Rectangle( pBorderWindow->ScreenToOutputPixel( aPos ), pBorderWindow->GetOutputSizePixel() ), SHOWTRACK_BIG );
                }
            }
            else
            {
                SensorCall();Point       aOldPos         = pBorderWindow->GetPosPixel();
                Size        aSize           = pBorderWindow->GetSizePixel();
                Rectangle   aNewRect( aOldPos, aSize );
                long        nOldWidth       = aSize.Width();
                long        nOldHeight      = aSize.Height();
                long        nBorderWidth    = maFrameData.mnLeftBorder+maFrameData.mnRightBorder;
                long        nBorderHeight   = maFrameData.mnTopBorder+maFrameData.mnBottomBorder;
                long        nMinWidth       = pBorderWindow->mnMinWidth+nBorderWidth;
                long        nMinHeight      = pBorderWindow->mnMinHeight+nBorderHeight;
                long        nMinWidth2      = nBorderWidth;
                long        nMaxWidth       = pBorderWindow->mnMaxWidth+nBorderWidth;
                long        nMaxHeight      = pBorderWindow->mnMaxHeight+nBorderHeight;

                SensorCall();if ( maFrameData.mnTitleHeight )
                {
                    SensorCall();nMinWidth2 += 4;

                    if ( pBorderWindow->GetStyle() & WB_CLOSEABLE )
                        nMinWidth2 += maFrameData.maCloseRect.GetWidth();
                }
                SensorCall();if ( nMinWidth2 > nMinWidth )
                    {/*167*/SensorCall();nMinWidth = nMinWidth2;/*168*/}
                SensorCall();if ( maFrameData.mnHitTest & (BORDERWINDOW_HITTEST_LEFT | BORDERWINDOW_HITTEST_TOPLEFT | BORDERWINDOW_HITTEST_BOTTOMLEFT) )
                {
                    SensorCall();aNewRect.Left() += aMousePos.X();
                    SensorCall();if ( aNewRect.GetWidth() < nMinWidth )
                        {/*169*/SensorCall();aNewRect.Left() = aNewRect.Right()-nMinWidth+1;/*170*/}
                    else {/*171*/SensorCall();if ( aNewRect.GetWidth() > nMaxWidth )
                        {/*173*/SensorCall();aNewRect.Left() = aNewRect.Right()-nMaxWidth+1;/*174*/}/*172*/}
                }
                else {/*175*/SensorCall();if ( maFrameData.mnHitTest & (BORDERWINDOW_HITTEST_RIGHT | BORDERWINDOW_HITTEST_TOPRIGHT | BORDERWINDOW_HITTEST_BOTTOMRIGHT) )
                {
                    SensorCall();aNewRect.Right() += aMousePos.X();
                    SensorCall();if ( aNewRect.GetWidth() < nMinWidth )
                        {/*177*/SensorCall();aNewRect.Right() = aNewRect.Left()+nMinWidth+1;/*178*/}
                    else {/*179*/SensorCall();if ( aNewRect.GetWidth() > nMaxWidth )
                        {/*181*/SensorCall();aNewRect.Right() = aNewRect.Left()+nMaxWidth+1;/*182*/}/*180*/}
                ;/*176*/}}
                SensorCall();if ( maFrameData.mnHitTest & (BORDERWINDOW_HITTEST_TOP | BORDERWINDOW_HITTEST_TOPLEFT | BORDERWINDOW_HITTEST_TOPRIGHT) )
                {
                    SensorCall();aNewRect.Top() += aMousePos.Y();
                    SensorCall();if ( aNewRect.GetHeight() < nMinHeight )
                        {/*183*/SensorCall();aNewRect.Top() = aNewRect.Bottom()-nMinHeight+1;/*184*/}
                    else {/*185*/SensorCall();if ( aNewRect.GetHeight() > nMaxHeight )
                        {/*187*/SensorCall();aNewRect.Top() = aNewRect.Bottom()-nMaxHeight+1;/*188*/}/*186*/}
                }
                else {/*189*/SensorCall();if ( maFrameData.mnHitTest & (BORDERWINDOW_HITTEST_BOTTOM | BORDERWINDOW_HITTEST_BOTTOMLEFT | BORDERWINDOW_HITTEST_BOTTOMRIGHT) )
                {
                    SensorCall();aNewRect.Bottom() += aMousePos.Y();
                    SensorCall();if ( aNewRect.GetHeight() < nMinHeight )
                        {/*191*/SensorCall();aNewRect.Bottom() = aNewRect.Top()+nMinHeight+1;/*192*/}
                    else {/*193*/SensorCall();if ( aNewRect.GetHeight() > nMaxHeight )
                        {/*195*/SensorCall();aNewRect.Bottom() = aNewRect.Top()+nMaxHeight+1;/*196*/}/*194*/}
                ;/*190*/}}

                // call Resizing-Handler for SystemWindows
                SensorCall();if ( pBorderWindow->ImplGetClientWindow()->IsSystemWindow() )
                {
                    // adjust size for Resizing-call
                    SensorCall();aSize = aNewRect.GetSize();
                    aSize.Width()   -= nBorderWidth;
                    aSize.Height()  -= nBorderHeight;
                    static_cast<SystemWindow*>(pBorderWindow->ImplGetClientWindow())->Resizing( aSize );
                    aSize.Width()   += nBorderWidth;
                    aSize.Height()  += nBorderHeight;
                    SensorCall();if ( aSize.Width() < nMinWidth )
                        {/*197*/SensorCall();aSize.Width() = nMinWidth;/*198*/}
                    SensorCall();if ( aSize.Height() < nMinHeight )
                        {/*199*/SensorCall();aSize.Height() = nMinHeight;/*200*/}
                    SensorCall();if ( aSize.Width() > nMaxWidth )
                        {/*201*/SensorCall();aSize.Width() = nMaxWidth;/*202*/}
                    SensorCall();if ( aSize.Height() > nMaxHeight )
                        {/*203*/SensorCall();aSize.Height() = nMaxHeight;/*204*/}
                    SensorCall();if ( maFrameData.mnHitTest & (BORDERWINDOW_HITTEST_LEFT | BORDERWINDOW_HITTEST_TOPLEFT | BORDERWINDOW_HITTEST_BOTTOMLEFT) )
                        {/*205*/SensorCall();aNewRect.Left() = aNewRect.Right()-aSize.Width()+1;/*206*/}
                    else
                        {/*207*/SensorCall();aNewRect.Right() = aNewRect.Left()+aSize.Width()-1;/*208*/}
                    SensorCall();if ( maFrameData.mnHitTest & (BORDERWINDOW_HITTEST_TOP | BORDERWINDOW_HITTEST_TOPLEFT | BORDERWINDOW_HITTEST_TOPRIGHT) )
                        {/*209*/SensorCall();aNewRect.Top() = aNewRect.Bottom()-aSize.Height()+1;/*210*/}
                    else
                        {/*211*/SensorCall();aNewRect.Bottom() = aNewRect.Top()+aSize.Height()-1;/*212*/}
                }

                SensorCall();if ( maFrameData.mbDragFull )
                {
                    // no move (only resize) if position did not change
                    if( aOldPos != aNewRect.TopLeft() )
                        pBorderWindow->setPosSizePixel( aNewRect.Left(), aNewRect.Top(),
                                                    aNewRect.GetWidth(), aNewRect.GetHeight(), PosSizeFlags::PosSize );
                    else
                        pBorderWindow->setPosSizePixel( aNewRect.Left(), aNewRect.Top(),
                                                    aNewRect.GetWidth(), aNewRect.GetHeight(), PosSizeFlags::Size );

                    pBorderWindow->ImplUpdateAll();
                    pBorderWindow->ImplGetFrameWindow()->ImplUpdateAll();
                    if ( maFrameData.mnHitTest & (BORDERWINDOW_HITTEST_RIGHT | BORDERWINDOW_HITTEST_TOPRIGHT | BORDERWINDOW_HITTEST_BOTTOMRIGHT) )
                        maFrameData.maMouseOff.X() += aNewRect.GetWidth()-nOldWidth;
                    if ( maFrameData.mnHitTest & (BORDERWINDOW_HITTEST_BOTTOM | BORDERWINDOW_HITTEST_BOTTOMLEFT | BORDERWINDOW_HITTEST_BOTTOMRIGHT) )
                        maFrameData.maMouseOff.Y() += aNewRect.GetHeight()-nOldHeight;
                }
                else
                {
                    maFrameData.mnTrackX        = aNewRect.Left();
                    maFrameData.mnTrackY        = aNewRect.Top();
                    maFrameData.mnTrackWidth    = aNewRect.GetWidth();
                    maFrameData.mnTrackHeight   = aNewRect.GetHeight();
                    pBorderWindow->ShowTracking( Rectangle( pBorderWindow->ScreenToOutputPixel( aNewRect.TopLeft() ), aNewRect.GetSize() ), SHOWTRACK_BIG );
                }
            }
        ;/*166*/}/*164*/}/*162*/}/*160*/}/*158*/}/*156*/}}
    ;/*154*/}}

    {_Bool  ReplaceReturn = true; SensorCall(); return ReplaceReturn;}
}

OUString ImplStdBorderWindowView::RequestHelp( const Point& rPos, Rectangle& rHelpRect )
{
SensorCall();    return ImplRequestHelp( &maFrameData, rPos, rHelpRect );
SensorCall();}

Rectangle ImplStdBorderWindowView::GetMenuRect() const
{
SensorCall();    return maFrameData.maMenuRect;
SensorCall();}

void ImplStdBorderWindowView::Init( OutputDevice* pDev, long nWidth, long nHeight )
{
    SensorCall();ImplBorderFrameData*    pData = &maFrameData;
    ImplBorderWindow*       pBorderWindow = maFrameData.mpBorderWindow;
    const StyleSettings&    rStyleSettings = pDev->GetSettings().GetStyleSettings();
    DecorationView          aDecoView( pDev );
    Rectangle               aRect( 0, 0, 10, 10 );
    Rectangle               aCalcRect = aDecoView.DrawFrame( aRect, DrawFrameStyle::DoubleOut, DrawFrameFlags::NoDraw );

    pData->mpOutDev         = pDev;
    pData->mnWidth          = nWidth;
    pData->mnHeight         = nHeight;

    pData->mnTitleType      = pBorderWindow->mnTitleType;
    pData->mbFloatWindow    = pBorderWindow->mbFloatWindow;

    SensorCall();if ( !(pBorderWindow->GetStyle() & (WB_MOVEABLE | WB_POPUP)) || (pData->mnTitleType == BORDERWINDOW_TITLE_NONE) )
        {/*213*/SensorCall();pData->mnBorderSize = 0;/*214*/}
    else {/*215*/SensorCall();if ( pData->mnTitleType == BORDERWINDOW_TITLE_TEAROFF )
        {/*217*/SensorCall();pData->mnBorderSize = 0;/*218*/}
    else
        {/*219*/SensorCall();pData->mnBorderSize = rStyleSettings.GetBorderSize();/*220*/}/*216*/}
    SensorCall();pData->mnLeftBorder     = aCalcRect.Left();
    pData->mnTopBorder      = aCalcRect.Top();
    pData->mnRightBorder    = aRect.Right()-aCalcRect.Right();
    pData->mnBottomBorder   = aRect.Bottom()-aCalcRect.Bottom();
    pData->mnLeftBorder    += pData->mnBorderSize;
    pData->mnTopBorder     += pData->mnBorderSize;
    pData->mnRightBorder   += pData->mnBorderSize;
    pData->mnBottomBorder  += pData->mnBorderSize;
    pData->mnNoTitleTop     = pData->mnTopBorder;

    ImplInitTitle(&maFrameData);
    SensorCall();if (pData->mnTitleHeight)
    {
        // to improve symbol display force a minum title height
        if( pData->mnTitleHeight < MIN_CAPTION_HEIGHT )
            pData->mnTitleHeight = MIN_CAPTION_HEIGHT;

        // set a proper background for drawing
        // highlighted buttons in the title
        pBorderWindow->SetBackground( rStyleSettings.GetFaceColor() );

        SensorCall();pData->maTitleRect.Left()    = pData->mnLeftBorder;
        pData->maTitleRect.Right()   = nWidth-pData->mnRightBorder-1;
        pData->maTitleRect.Top()     = pData->mnTopBorder;
        pData->maTitleRect.Bottom()  = pData->maTitleRect.Top()+pData->mnTitleHeight-1;

        SensorCall();if ( pData->mnTitleType & (BORDERWINDOW_TITLE_NORMAL | BORDERWINDOW_TITLE_SMALL) )
        {
            SensorCall();long nLeft          = pData->maTitleRect.Left();
            long nRight         = pData->maTitleRect.Right();
            long nItemTop       = pData->maTitleRect.Top();
            long nItemBottom    = pData->maTitleRect.Bottom();
            nLeft              += 1;
            nRight             -= 3;
            nItemTop           += 2;
            nItemBottom        -= 2;

            SensorCall();if ( pBorderWindow->GetStyle() & WB_PINABLE )
            {
                SensorCall();Image aImage;
                ImplGetPinImage( DrawButtonFlags::NONE, false, aImage );
                pData->maPinRect.Top()    = nItemTop;
                pData->maPinRect.Bottom() = nItemBottom;
                pData->maPinRect.Left()   = nLeft;
                pData->maPinRect.Right()  = pData->maPinRect.Left()+aImage.GetSizePixel().Width();
                nLeft += pData->maPinRect.GetWidth()+3;
            }

            SensorCall();if ( pBorderWindow->GetStyle() & WB_CLOSEABLE )
            {
                SensorCall();pData->maCloseRect.Top()    = nItemTop;
                pData->maCloseRect.Bottom() = nItemBottom;
                pData->maCloseRect.Right()  = nRight;
                pData->maCloseRect.Left()   = pData->maCloseRect.Right()-pData->maCloseRect.GetHeight()+1;
                nRight -= pData->maCloseRect.GetWidth()+3;
            }

            SensorCall();if ( pBorderWindow->mbMenuBtn )
            {
                SensorCall();pData->maMenuRect.Top()    = nItemTop;
                pData->maMenuRect.Bottom() = nItemBottom;
                pData->maMenuRect.Right()  = nRight;
                pData->maMenuRect.Left()   = pData->maMenuRect.Right()-pData->maMenuRect.GetHeight()+1;
                nRight -= pData->maMenuRect.GetWidth();
            }

            SensorCall();if ( pBorderWindow->mbDockBtn )
            {
                SensorCall();pData->maDockRect.Top()    = nItemTop;
                pData->maDockRect.Bottom() = nItemBottom;
                pData->maDockRect.Right()  = nRight;
                pData->maDockRect.Left()   = pData->maDockRect.Right()-pData->maDockRect.GetHeight()+1;
                nRight -= pData->maDockRect.GetWidth();
                SensorCall();if ( !pBorderWindow->mbHideBtn &&
                     !(pBorderWindow->GetStyle() & WB_ROLLABLE) )
                    {/*223*/SensorCall();nRight -= 3;/*224*/}
            }

            SensorCall();if ( pBorderWindow->mbHideBtn )
            {
                SensorCall();pData->maHideRect.Top()    = nItemTop;
                pData->maHideRect.Bottom() = nItemBottom;
                pData->maHideRect.Right()  = nRight;
                pData->maHideRect.Left()   = pData->maHideRect.Right()-pData->maHideRect.GetHeight()+1;
                nRight -= pData->maHideRect.GetWidth();
                SensorCall();if ( !(pBorderWindow->GetStyle() & WB_ROLLABLE) )
                    {/*225*/SensorCall();nRight -= 3;/*226*/}
            }

            SensorCall();if ( pBorderWindow->GetStyle() & WB_ROLLABLE )
            {
                SensorCall();pData->maRollRect.Top()    = nItemTop;
                pData->maRollRect.Bottom() = nItemBottom;
                pData->maRollRect.Right()  = nRight;
                pData->maRollRect.Left()   = pData->maRollRect.Right()-pData->maRollRect.GetHeight()+1;
                nRight -= pData->maRollRect.GetWidth();
            }
        }
        else
        {
            SensorCall();pData->maPinRect.SetEmpty();
            pData->maCloseRect.SetEmpty();
            pData->maDockRect.SetEmpty();
            pData->maMenuRect.SetEmpty();
            pData->maHideRect.SetEmpty();
            pData->maRollRect.SetEmpty();
            pData->maHelpRect.SetEmpty();
        }

        SensorCall();pData->mnTopBorder  += pData->mnTitleHeight;
    }
    else
    {
        SensorCall();pData->maTitleRect.SetEmpty();
        pData->maPinRect.SetEmpty();
        pData->maCloseRect.SetEmpty();
        pData->maDockRect.SetEmpty();
        pData->maMenuRect.SetEmpty();
        pData->maHideRect.SetEmpty();
        pData->maRollRect.SetEmpty();
        pData->maHelpRect.SetEmpty();
    }
SensorCall();}

void ImplStdBorderWindowView::GetBorder( sal_Int32& rLeftBorder, sal_Int32& rTopBorder,
                                         sal_Int32& rRightBorder, sal_Int32& rBottomBorder ) const
{
SensorCall();    rLeftBorder     = maFrameData.mnLeftBorder;
    rTopBorder      = maFrameData.mnTopBorder;
    rRightBorder    = maFrameData.mnRightBorder;
    rBottomBorder   = maFrameData.mnBottomBorder;
SensorCall();}

long ImplStdBorderWindowView::CalcTitleWidth() const
{
SensorCall();    return ImplCalcTitleWidth( &maFrameData );
SensorCall();}

void ImplStdBorderWindowView::DrawWindow(vcl::RenderContext& rRenderContext, sal_uInt16 nDrawFlags, const Point* pOffset)
{
    SensorCall();ImplBorderFrameData* pData = &maFrameData;
    ImplBorderWindow* pBorderWindow = pData->mpBorderWindow;
    Point aTmpPoint = pOffset ? Point(*pOffset) : Point();
    Rectangle aInRect( aTmpPoint, Size( pData->mnWidth, pData->mnHeight ) );
    const StyleSettings& rStyleSettings = rRenderContext.GetSettings().GetStyleSettings();
    DecorationView aDecoView(&rRenderContext);
    Color aFaceColor(rStyleSettings.GetFaceColor());
    Color aFrameColor(aFaceColor);

    aFrameColor.DecreaseContrast(sal_uInt8(0.5 * 255));

    // Draw Frame
    SensorCall();if (nDrawFlags & BORDERWINDOW_DRAW_FRAME)
    {
        SensorCall();vcl::Region oldClipRgn(rRenderContext.GetClipRegion());

        // for popups, don't draw part of the frame
        SensorCall();if (pData->mnTitleType == BORDERWINDOW_TITLE_POPUP)
        {
            SensorCall();FloatingWindow* pWin = dynamic_cast<FloatingWindow*>(pData->mpBorderWindow->GetWindow(GetWindowType::Client));
            SensorCall();if (pWin)
            {
                SensorCall();vcl::Region aClipRgn(aInRect);
                Rectangle aItemClipRect(pWin->ImplGetItemEdgeClipRect());
                SensorCall();if (!aItemClipRect.IsEmpty())
                {
                    aItemClipRect.SetPos(pData->mpBorderWindow->AbsoluteScreenToOutputPixel(aItemClipRect.TopLeft()));
                    SensorCall();aClipRgn.Exclude(aItemClipRect);
                    rRenderContext.SetClipRegion(aClipRgn);
                }
            }
        }

        // single line frame
        SensorCall();rRenderContext.SetLineColor(aFrameColor);
        rRenderContext.SetFillColor();
        rRenderContext.DrawRect(aInRect);
        ++aInRect.Left();
        --aInRect.Right();
        ++aInRect.Top();
        --aInRect.Bottom();

        // restore
        SensorCall();if (pData->mnTitleType == BORDERWINDOW_TITLE_POPUP)
            {/*227*/SensorCall();rRenderContext.SetClipRegion(oldClipRgn);/*228*/}
    }
    else
        {/*229*/SensorCall();aInRect = aDecoView.DrawFrame(aInRect, DrawFrameStyle::DoubleOut, DrawFrameFlags::NoDraw);/*230*/}

    // Draw Border
    SensorCall();rRenderContext.SetLineColor();
    long nBorderSize = pData->mnBorderSize;
    SensorCall();if ((nDrawFlags & BORDERWINDOW_DRAW_BORDER) && nBorderSize)
    {
        SensorCall();rRenderContext.SetFillColor(rStyleSettings.GetFaceColor());
        rRenderContext.DrawRect(Rectangle(Point(aInRect.Left(), aInRect.Top()),
                                 Size(aInRect.GetWidth(), nBorderSize)));
        rRenderContext.DrawRect(Rectangle(Point(aInRect.Left(), aInRect.Top() + nBorderSize),
                                 Size(nBorderSize, aInRect.GetHeight() - nBorderSize)));
        rRenderContext.DrawRect(Rectangle(Point(aInRect.Left(), aInRect.Bottom() - nBorderSize + 1),
                                 Size(aInRect.GetWidth(), nBorderSize)));
        rRenderContext.DrawRect(Rectangle(Point(aInRect.Right()-nBorderSize + 1, aInRect.Top() + nBorderSize),
                                 Size(nBorderSize, aInRect.GetHeight() - nBorderSize)));
    }

    // Draw Title
    SensorCall();if ((nDrawFlags & BORDERWINDOW_DRAW_TITLE) && !pData->maTitleRect.IsEmpty())
    {
        SensorCall();aInRect = pData->maTitleRect;

        // use no gradient anymore, just a static titlecolor
        SensorCall();if (pData->mnTitleType != BORDERWINDOW_TITLE_POPUP)
            {/*231*/SensorCall();rRenderContext.SetFillColor(aFrameColor);/*232*/}
        else
            {/*233*/SensorCall();rRenderContext.SetFillColor(aFaceColor);/*234*/}

        SensorCall();rRenderContext.SetTextColor(rStyleSettings.GetButtonTextColor());
        Rectangle aTitleRect(pData->maTitleRect);
        SensorCall();if(pOffset)
            {/*235*/SensorCall();aTitleRect.Move(pOffset->X(), pOffset->Y());/*236*/}
        SensorCall();rRenderContext.DrawRect(aTitleRect);

        SensorCall();if (pData->mnTitleType != BORDERWINDOW_TITLE_TEAROFF)
        {
            SensorCall();aInRect.Left()  += 2;
            aInRect.Right() -= 2;

            SensorCall();if (!pData->maPinRect.IsEmpty())
                {/*237*/SensorCall();aInRect.Left() = pData->maPinRect.Right() + 2;/*238*/}

            SensorCall();if (!pData->maHelpRect.IsEmpty())
                {/*239*/SensorCall();aInRect.Right() = pData->maHelpRect.Left() - 2;/*240*/}
            else {/*241*/SensorCall();if (!pData->maRollRect.IsEmpty())
                {/*243*/SensorCall();aInRect.Right() = pData->maRollRect.Left() - 2;/*244*/}
            else {/*245*/SensorCall();if (!pData->maHideRect.IsEmpty())
                {/*247*/SensorCall();aInRect.Right() = pData->maHideRect.Left() - 2;/*248*/}
            else {/*249*/SensorCall();if (!pData->maDockRect.IsEmpty())
                {/*251*/SensorCall();aInRect.Right() = pData->maDockRect.Left() - 2;/*252*/}
            else {/*253*/SensorCall();if (!pData->maMenuRect.IsEmpty())
                {/*255*/SensorCall();aInRect.Right() = pData->maMenuRect.Left() - 2;/*256*/}
            else {/*257*/SensorCall();if (!pData->maCloseRect.IsEmpty())
                {/*259*/SensorCall();aInRect.Right() = pData->maCloseRect.Left() - 2;/*260*/}/*258*/}/*254*/}/*250*/}/*246*/}/*242*/}

            SensorCall();if (pOffset)
                {/*261*/SensorCall();aInRect.Move(pOffset->X(), pOffset->Y());/*262*/}

            SensorCall();DrawTextFlags nTextStyle = DrawTextFlags::Left | DrawTextFlags::VCenter | DrawTextFlags::EndEllipsis | DrawTextFlags::Clip;

            // must show tooltip ?
            TextRectInfo aInfo;
            rRenderContext.GetTextRect(aInRect, pBorderWindow->GetText(), nTextStyle, &aInfo);
            pData->mbTitleClipped = aInfo.IsEllipses();

            rRenderContext.DrawText(aInRect, pBorderWindow->GetText(), nTextStyle);
        }
    }

    SensorCall();if (((nDrawFlags & BORDERWINDOW_DRAW_CLOSE) || (nDrawFlags & BORDERWINDOW_DRAW_TITLE))
       && !pData->maCloseRect.IsEmpty())
    {
        SensorCall();Rectangle aSymbolRect(pData->maCloseRect);
        SensorCall();if (pOffset)
            {/*263*/SensorCall();aSymbolRect.Move(pOffset->X(), pOffset->Y());/*264*/}
        ImplDrawBrdWinSymbolButton(&rRenderContext, aSymbolRect, SymbolType::CLOSE, pData->mnCloseState);
    }
    SensorCall();if (((nDrawFlags & BORDERWINDOW_DRAW_DOCK) || (nDrawFlags & BORDERWINDOW_DRAW_TITLE))
       && !pData->maDockRect.IsEmpty())
    {
        SensorCall();Rectangle aSymbolRect(pData->maDockRect);
        SensorCall();if (pOffset)
            {/*265*/SensorCall();aSymbolRect.Move(pOffset->X(), pOffset->Y());/*266*/}
        ImplDrawBrdWinSymbolButton(&rRenderContext, aSymbolRect, SymbolType::DOCK, pData->mnDockState);
    }
    SensorCall();if (((nDrawFlags & BORDERWINDOW_DRAW_MENU) || (nDrawFlags & BORDERWINDOW_DRAW_TITLE))
       && !pData->maMenuRect.IsEmpty())
    {
        SensorCall();Rectangle aSymbolRect(pData->maMenuRect);
        SensorCall();if (pOffset)
            {/*267*/SensorCall();aSymbolRect.Move(pOffset->X(), pOffset->Y());/*268*/}
        ImplDrawBrdWinSymbolButton(&rRenderContext, aSymbolRect, SymbolType::MENU, pData->mnMenuState);
    }
    SensorCall();if (((nDrawFlags & BORDERWINDOW_DRAW_HIDE) || (nDrawFlags & BORDERWINDOW_DRAW_TITLE))
       && !pData->maHideRect.IsEmpty())
    {
        SensorCall();Rectangle aSymbolRect(pData->maHideRect);
        SensorCall();if (pOffset)
            {/*269*/SensorCall();aSymbolRect.Move(pOffset->X(), pOffset->Y());/*270*/}
        ImplDrawBrdWinSymbolButton(&rRenderContext, aSymbolRect, SymbolType::HIDE, pData->mnHideState);
    }
    SensorCall();if (((nDrawFlags & BORDERWINDOW_DRAW_ROLL) || (nDrawFlags & BORDERWINDOW_DRAW_TITLE))
       && !pData->maRollRect.IsEmpty())
    {
        SensorCall();SymbolType eType;
        if (pBorderWindow->mbRollUp)
            eType = SymbolType::ROLLDOWN;
        else
            eType = SymbolType::ROLLUP;
        Rectangle aSymbolRect(pData->maRollRect);
        SensorCall();if (pOffset)
            {/*271*/SensorCall();aSymbolRect.Move(pOffset->X(), pOffset->Y());/*272*/}
        SensorCall();ImplDrawBrdWinSymbolButton(&rRenderContext, aSymbolRect, eType, pData->mnRollState);
    }

    SensorCall();if (((nDrawFlags & BORDERWINDOW_DRAW_HELP) || (nDrawFlags & BORDERWINDOW_DRAW_TITLE))
       && !pData->maHelpRect.IsEmpty())
    {
        SensorCall();Rectangle aSymbolRect(pData->maHelpRect);
        SensorCall();if (pOffset)
            {/*273*/SensorCall();aSymbolRect.Move(pOffset->X(), pOffset->Y());/*274*/}
        ImplDrawBrdWinSymbolButton(&rRenderContext, aSymbolRect, SymbolType::HELP, pData->mnHelpState);
    }
    SensorCall();if (((nDrawFlags & BORDERWINDOW_DRAW_PIN) || (nDrawFlags & BORDERWINDOW_DRAW_TITLE))
       && !pData->maPinRect.IsEmpty())
    {
        SensorCall();Image aImage;
        ImplGetPinImage(pData->mnPinState, pBorderWindow->mbPinned, aImage);
        Size  aImageSize = aImage.GetSizePixel();
        long  nRectHeight = pData->maPinRect.GetHeight();
        Point aPos(pData->maPinRect.TopLeft());
        SensorCall();if (pOffset)
            {/*275*/SensorCall();aPos.Move(pOffset->X(), pOffset->Y());/*276*/}
        SensorCall();if (nRectHeight < aImageSize.Height())
        {
            SensorCall();rRenderContext.DrawImage(aPos, Size( aImageSize.Width(), nRectHeight ), aImage);
        }
        else
        {
            SensorCall();aPos.Y() += (nRectHeight-aImageSize.Height()) / 2;
            rRenderContext.DrawImage(aPos, aImage);
        }
    }
SensorCall();}

void ImplBorderWindow::ImplInit( vcl::Window* pParent,
                                 WinBits nStyle, sal_uInt16 nTypeStyle,
                                 const ::com::sun::star::uno::Any& )
{
    SensorCall();ImplInit( pParent, nStyle, nTypeStyle, NULL );
SensorCall();}

void ImplBorderWindow::ImplInit( vcl::Window* pParent,
                                 WinBits nStyle, sal_uInt16 nTypeStyle,
                                 SystemParentData* pSystemParentData
                                 )
{
    // remove all unwanted WindowBits
    SensorCall();WinBits nOrgStyle = nStyle;
    WinBits nTestStyle = (WB_MOVEABLE | WB_SIZEABLE | WB_ROLLABLE | WB_PINABLE | WB_CLOSEABLE | WB_STANDALONE | WB_DIALOGCONTROL | WB_NODIALOGCONTROL | WB_SYSTEMFLOATWIN | WB_INTROWIN | WB_DEFAULTWIN | WB_TOOLTIPWIN | WB_NOSHADOW | WB_OWNERDRAWDECORATION | WB_SYSTEMCHILDWINDOW  | WB_POPUP);
    SensorCall();if ( nTypeStyle & BORDERWINDOW_STYLE_APP )
        {/*277*/SensorCall();nTestStyle |= WB_APP;/*278*/}
    SensorCall();nStyle &= nTestStyle;

    mpWindowImpl->mbBorderWin       = true;
    mbSmallOutBorder    = false;
    SensorCall();if ( nTypeStyle & BORDERWINDOW_STYLE_FRAME )
    {
        SensorCall();if( (nStyle & WB_SYSTEMCHILDWINDOW) )
        {
            SensorCall();mpWindowImpl->mbOverlapWin  = true;
            mpWindowImpl->mbFrame       = true;
            mbFrameBorder               = false;
        }
        else {/*279*/SensorCall();if( (nStyle & (WB_OWNERDRAWDECORATION | WB_POPUP)) )
        {
            SensorCall();mpWindowImpl->mbOverlapWin  = true;
            mpWindowImpl->mbFrame       = true;
            mbFrameBorder   = (nOrgStyle & WB_NOBORDER) == 0;
        }
        else
        {
            SensorCall();mpWindowImpl->mbOverlapWin  = true;
            mpWindowImpl->mbFrame       = true;
            mbFrameBorder   = false;
            // closeable windows may have a border as well, eg. system floating windows without caption
            SensorCall();if ( (nOrgStyle & (WB_BORDER | WB_NOBORDER | WB_MOVEABLE | WB_SIZEABLE/* | WB_CLOSEABLE*/)) == WB_BORDER )
                {/*281*/SensorCall();mbSmallOutBorder = true;/*282*/}
        ;/*280*/}}
    }
    else {/*283*/SensorCall();if ( nTypeStyle & BORDERWINDOW_STYLE_OVERLAP )
    {
        SensorCall();mpWindowImpl->mbOverlapWin  = true;
        mbFrameBorder   = true;
    }
    else
        {/*285*/SensorCall();mbFrameBorder   = false;/*286*/}/*284*/}

    SensorCall();if ( nTypeStyle & BORDERWINDOW_STYLE_FLOAT )
        {/*287*/SensorCall();mbFloatWindow = true;/*288*/}
    else
        {/*289*/SensorCall();mbFloatWindow = false;/*290*/}

    Window::ImplInit( pParent, nStyle, pSystemParentData );
    SetBackground();
    SetTextFillColor();

    mpMenuBarWindow = NULL;
    SensorCall();mnMinWidth      = 0;
    mnMinHeight     = 0;
    mnMaxWidth      = SHRT_MAX;
    mnMaxHeight     = SHRT_MAX;
    mnRollHeight    = 0;
    mnOrgMenuHeight = 0;
    mbPinned        = false;
    mbRollUp        = false;
    mbMenuHide      = false;
    mbDockBtn       = false;
    mbMenuBtn       = false;
    mbHideBtn       = false;
    mbDisplayActive = IsActive();

    SensorCall();if ( nTypeStyle & BORDERWINDOW_STYLE_FLOAT )
        mnTitleType = BORDERWINDOW_TITLE_SMALL;
    else
        mnTitleType = BORDERWINDOW_TITLE_NORMAL;
    mnBorderStyle   = WindowBorderStyle::NORMAL;
    SensorCall();InitView();
SensorCall();}

ImplBorderWindow::ImplBorderWindow( vcl::Window* pParent,
                                    SystemParentData* pSystemParentData,
                                    WinBits nStyle, sal_uInt16 nTypeStyle
                                    ) : Window( WINDOW_BORDERWINDOW )
{
    SensorCall();ImplInit( pParent, nStyle, nTypeStyle, pSystemParentData );
SensorCall();}

ImplBorderWindow::ImplBorderWindow( vcl::Window* pParent, WinBits nStyle ,
                                    sal_uInt16 nTypeStyle ) :
    Window( WINDOW_BORDERWINDOW )
{
    SensorCall();ImplInit( pParent, nStyle, nTypeStyle, ::com::sun::star::uno::Any() );
SensorCall();}

ImplBorderWindow::~ImplBorderWindow()
{
SensorCall();    disposeOnce();
SensorCall();}

void ImplBorderWindow::dispose()
{
    SensorCall();delete mpBorderView;
    mpBorderView = NULL;
    mpMenuBarWindow.clear();
    vcl::Window::dispose();
SensorCall();}

void ImplBorderWindow::MouseMove( const MouseEvent& rMEvt )
{
    SensorCall();if (mpBorderView)
        {/*291*/SensorCall();mpBorderView->MouseMove( rMEvt );/*292*/}
SensorCall();}

void ImplBorderWindow::MouseButtonDown( const MouseEvent& rMEvt )
{
    SensorCall();if (mpBorderView)
        {/*293*/SensorCall();mpBorderView->MouseButtonDown( rMEvt );/*294*/}
SensorCall();}

void ImplBorderWindow::Tracking( const TrackingEvent& rTEvt )
{
    SensorCall();if (mpBorderView)
        {/*295*/SensorCall();mpBorderView->Tracking( rTEvt );/*296*/}
SensorCall();}

void ImplBorderWindow::Paint( vcl::RenderContext& rRenderContext, const Rectangle& )
{
    SensorCall();if (mpBorderView)
        {/*297*/SensorCall();mpBorderView->DrawWindow(rRenderContext, BORDERWINDOW_DRAW_ALL);/*298*/}
SensorCall();}

void ImplBorderWindow::Draw( const Rectangle&, OutputDevice* pOutDev, const Point& rPos )
{
    SensorCall();if (mpBorderView)
        {/*299*/SensorCall();mpBorderView->DrawWindow(*pOutDev, BORDERWINDOW_DRAW_ALL, &rPos);/*300*/}
SensorCall();}

void ImplBorderWindow::Activate()
{
    SensorCall();SetDisplayActive( true );
    Window::Activate();
SensorCall();}

void ImplBorderWindow::Deactivate()
{
    // remove active windows from the ruler, also ignore the Deactivate
    // if a menu becomes active
    SensorCall();if ( GetActivateMode() && !ImplGetSVData()->maWinData.mbNoDeactivate )
        {/*301*/SensorCall();SetDisplayActive( false );/*302*/}
    Window::Deactivate();
SensorCall();}

void ImplBorderWindow::RequestHelp( const HelpEvent& rHEvt )
{
    // no keyboard help for border window
    SensorCall();if ( rHEvt.GetMode() & (HelpEventMode::BALLOON | HelpEventMode::QUICK) && !rHEvt.KeyboardActivated() )
    {
        SensorCall();Point       aMousePosPixel = ScreenToOutputPixel( rHEvt.GetMousePosPixel() );
        Rectangle   aHelpRect;
        OUString    aHelpStr( mpBorderView->RequestHelp( aMousePosPixel, aHelpRect ) );

        // retrieve rectangle
        SensorCall();if ( !aHelpStr.isEmpty() )
        {
            aHelpRect.SetPos( OutputToScreenPixel( aHelpRect.TopLeft() ) );
            if ( rHEvt.GetMode() & HelpEventMode::BALLOON )
                Help::ShowBalloon( this, aHelpRect.Center(), aHelpRect, aHelpStr );
            else
                Help::ShowQuickHelp( this, aHelpRect, aHelpStr );
            SensorCall();return;
        }
    }

    Window::RequestHelp( rHEvt );
SensorCall();}

void ImplBorderWindow::Resize()
{
    SensorCall();Size aSize = GetOutputSizePixel();

    SensorCall();if ( !mbRollUp )
    {
        SensorCall();vcl::Window* pClientWindow = ImplGetClientWindow();

        SensorCall();if ( mpMenuBarWindow )
        {
            SensorCall();sal_Int32 nLeftBorder;
            sal_Int32 nTopBorder;
            sal_Int32 nRightBorder;
            sal_Int32 nBottomBorder;
            long nMenuHeight = mpMenuBarWindow->GetSizePixel().Height();
            SensorCall();if ( mbMenuHide )
            {
                SensorCall();if ( nMenuHeight )
                    {/*303*/SensorCall();mnOrgMenuHeight = nMenuHeight;/*304*/}
                SensorCall();nMenuHeight = 0;
            }
            else
            {
                SensorCall();if ( !nMenuHeight )
                    {/*305*/SensorCall();nMenuHeight = mnOrgMenuHeight;/*306*/}
            }
            SensorCall();mpBorderView->GetBorder( nLeftBorder, nTopBorder, nRightBorder, nBottomBorder );
            mpMenuBarWindow->setPosSizePixel( nLeftBorder,
                                              nTopBorder,
                                              aSize.Width()-nLeftBorder-nRightBorder,
                                              nMenuHeight,
                                              PosSizeFlags::Pos |
                                              PosSizeFlags::Width | PosSizeFlags::Height );
        }

        SensorCall();GetBorder( pClientWindow->mpWindowImpl->mnLeftBorder, pClientWindow->mpWindowImpl->mnTopBorder,
                   pClientWindow->mpWindowImpl->mnRightBorder, pClientWindow->mpWindowImpl->mnBottomBorder );
        pClientWindow->ImplPosSizeWindow( pClientWindow->mpWindowImpl->mnLeftBorder,
                                          pClientWindow->mpWindowImpl->mnTopBorder,
                                          aSize.Width()-pClientWindow->mpWindowImpl->mnLeftBorder-pClientWindow->mpWindowImpl->mnRightBorder,
                                          aSize.Height()-pClientWindow->mpWindowImpl->mnTopBorder-pClientWindow->mpWindowImpl->mnBottomBorder,
                                          PosSizeFlags::X | PosSizeFlags::Y |
                                          PosSizeFlags::Width | PosSizeFlags::Height );
    }

    // UpdateView
    mpBorderView->Init( this, aSize.Width(), aSize.Height() );
    SensorCall();InvalidateBorder();

    Window::Resize();
SensorCall();}

void ImplBorderWindow::StateChanged( StateChangedType nType )
{
    SensorCall();if ( (nType == StateChangedType::Text) ||
         (nType == StateChangedType::Image) ||
         (nType == StateChangedType::Data) )
    {
        SensorCall();if (IsReallyVisible() && mbFrameBorder)
            {/*307*/SensorCall();InvalidateBorder();/*308*/}
    }

    Window::StateChanged( nType );
SensorCall();}

void ImplBorderWindow::DataChanged( const DataChangedEvent& rDCEvt )
{
    SensorCall();if ( (rDCEvt.GetType() == DataChangedEventType::FONTS) ||
         (rDCEvt.GetType() == DataChangedEventType::FONTSUBSTITUTION) ||
         ((rDCEvt.GetType() == DataChangedEventType::SETTINGS) &&
          (rDCEvt.GetFlags() & AllSettingsFlags::STYLE)) )
    {
        if ( !mpWindowImpl->mbFrame || (GetStyle() & (WB_OWNERDRAWDECORATION | WB_POPUP)) )
            UpdateView( true, ImplGetWindow()->GetOutputSizePixel() );
    }

    Window::DataChanged( rDCEvt );
SensorCall();}

void ImplBorderWindow::InitView()
{
    SensorCall();if ( mbSmallOutBorder )
        {/*309*/mpBorderView = new ImplSmallBorderWindowView( this );/*310*/}
    else {/*311*/SensorCall();if ( mpWindowImpl->mbFrame )
    {
        SensorCall();if( mbFrameBorder )
            {/*313*/mpBorderView = new ImplStdBorderWindowView( this );/*314*/}
        else
            {/*315*/SensorCall();mpBorderView = new ImplNoBorderWindowView( this );/*316*/}
    }
    else {/*317*/if ( !mbFrameBorder );/*312*/}
        mpBorderView = new ImplSmallBorderWindowView( this );/*318*/}
    else
        mpBorderView = new ImplStdBorderWindowView( this );
    SensorCall();Size aSize = GetOutputSizePixel();
    mpBorderView->Init( this, aSize.Width(), aSize.Height() );
SensorCall();}

void ImplBorderWindow::UpdateView( bool bNewView, const Size& rNewOutSize )
{
    SensorCall();sal_Int32 nLeftBorder;
    sal_Int32 nTopBorder;
    sal_Int32 nRightBorder;
    sal_Int32 nBottomBorder;
    Size aOldSize = GetSizePixel();
    Size aOutputSize = rNewOutSize;

    SensorCall();if ( bNewView )
    {
        SensorCall();delete mpBorderView;
        InitView();
    }
    else
    {
        SensorCall();Size aSize = aOutputSize;
        mpBorderView->GetBorder( nLeftBorder, nTopBorder, nRightBorder, nBottomBorder );
        aSize.Width()  += nLeftBorder+nRightBorder;
        aSize.Height() += nTopBorder+nBottomBorder;
        mpBorderView->Init( this, aSize.Width(), aSize.Height() );
    }

    SensorCall();vcl::Window* pClientWindow = ImplGetClientWindow();
    SensorCall();if ( pClientWindow )
    {
        SensorCall();GetBorder( pClientWindow->mpWindowImpl->mnLeftBorder, pClientWindow->mpWindowImpl->mnTopBorder,
                   pClientWindow->mpWindowImpl->mnRightBorder, pClientWindow->mpWindowImpl->mnBottomBorder );
    }
    SensorCall();GetBorder( nLeftBorder, nTopBorder, nRightBorder, nBottomBorder );
    SensorCall();if ( aOldSize.Width() || aOldSize.Height() )
    {
        SensorCall();aOutputSize.Width()     += nLeftBorder+nRightBorder;
        aOutputSize.Height()    += nTopBorder+nBottomBorder;
        SensorCall();if ( aOutputSize == GetSizePixel() )
            {/*319*/SensorCall();InvalidateBorder();/*320*/}
        else
            {/*321*/SetSizePixel( aOutputSize );/*322*/}
    }
SensorCall();}

void ImplBorderWindow::InvalidateBorder()
{
    SensorCall();if ( IsReallyVisible() )
    {
        // invalidate only if we have a border
        SensorCall();sal_Int32 nLeftBorder;
        sal_Int32 nTopBorder;
        sal_Int32 nRightBorder;
        sal_Int32 nBottomBorder;
        mpBorderView->GetBorder( nLeftBorder, nTopBorder, nRightBorder, nBottomBorder );
        SensorCall();if ( nLeftBorder || nTopBorder || nRightBorder || nBottomBorder )
        {
            SensorCall();Rectangle   aWinRect( Point( 0, 0 ), GetOutputSizePixel() );
            vcl::Region      aRegion( aWinRect );
            aWinRect.Left()   += nLeftBorder;
            aWinRect.Top()    += nTopBorder;
            aWinRect.Right()  -= nRightBorder;
            aWinRect.Bottom() -= nBottomBorder;
            // no output area anymore, now invalidate all
            SensorCall();if ( (aWinRect.Right() < aWinRect.Left()) ||
                 (aWinRect.Bottom() < aWinRect.Top()) )
                {/*323*/Invalidate( INVALIDATE_NOCHILDREN );/*324*/}
            else
            {
                SensorCall();aRegion.Exclude( aWinRect );
                Invalidate( aRegion, INVALIDATE_NOCHILDREN );
            }
        }
    }
SensorCall();}

void ImplBorderWindow::SetDisplayActive( bool bActive )
{
    SensorCall();if ( mbDisplayActive != bActive )
    {
        SensorCall();mbDisplayActive = bActive;
        SensorCall();if ( mbFrameBorder )
            {/*325*/SensorCall();InvalidateBorder();/*326*/}
    }
SensorCall();}

void ImplBorderWindow::SetTitleType( sal_uInt16 nTitleType, const Size& rSize )
{
    SensorCall();mnTitleType = nTitleType;
    UpdateView( false, rSize );
SensorCall();}

void ImplBorderWindow::SetBorderStyle( WindowBorderStyle nStyle )
{
    SensorCall();if ( !mbFrameBorder && (mnBorderStyle != nStyle) )
    {
        SensorCall();mnBorderStyle = nStyle;
        UpdateView( false, ImplGetWindow()->GetOutputSizePixel() );
    }
SensorCall();}

void ImplBorderWindow::SetPin( bool bPin )
{
    SensorCall();mbPinned = bPin;
    InvalidateBorder();
SensorCall();}

void ImplBorderWindow::SetRollUp( bool bRollUp, const Size& rSize )
{
    SensorCall();mbRollUp = bRollUp;
    mnRollHeight = rSize.Height();
    UpdateView( false, rSize );
SensorCall();}

void ImplBorderWindow::SetCloseButton()
{
    SetStyle( GetStyle() | WB_CLOSEABLE );
    SensorCall();Size aSize = GetOutputSizePixel();
    mpBorderView->Init( this, aSize.Width(), aSize.Height() );
    InvalidateBorder();
SensorCall();}

void ImplBorderWindow::SetDockButton( bool bDockButton )
{
    SensorCall();mbDockBtn = bDockButton;
    Size aSize = GetOutputSizePixel();
    mpBorderView->Init( this, aSize.Width(), aSize.Height() );
    InvalidateBorder();
SensorCall();}

void ImplBorderWindow::SetHideButton( bool bHideButton )
{
    SensorCall();mbHideBtn = bHideButton;
    Size aSize = GetOutputSizePixel();
    mpBorderView->Init( this, aSize.Width(), aSize.Height() );
    InvalidateBorder();
SensorCall();}

void ImplBorderWindow::SetMenuButton( bool bMenuButton )
{
    SensorCall();mbMenuBtn = bMenuButton;
    Size aSize = GetOutputSizePixel();
    mpBorderView->Init( this, aSize.Width(), aSize.Height() );
    InvalidateBorder();
SensorCall();}

void ImplBorderWindow::UpdateMenuHeight()
{
    SensorCall();Resize();
SensorCall();}

void ImplBorderWindow::SetMenuBarWindow( vcl::Window* pWindow )
{
    mpMenuBarWindow = pWindow;
    SensorCall();UpdateMenuHeight();
    SensorCall();if ( pWindow )
        {/*327*/SensorCall();pWindow->Show();/*328*/}
SensorCall();}

void ImplBorderWindow::SetMenuBarMode( bool bHide )
{
    SensorCall();mbMenuHide = bHide;
    UpdateMenuHeight();
SensorCall();}

void ImplBorderWindow::GetBorder( sal_Int32& rLeftBorder, sal_Int32& rTopBorder,
                                  sal_Int32& rRightBorder, sal_Int32& rBottomBorder ) const
{
    SensorCall();mpBorderView->GetBorder( rLeftBorder, rTopBorder, rRightBorder, rBottomBorder );
    if ( mpMenuBarWindow && !mbMenuHide )
        rTopBorder += mpMenuBarWindow->GetSizePixel().Height();
SensorCall();}

long ImplBorderWindow::CalcTitleWidth() const
{
    {const long  ReplaceReturn = mpBorderView->CalcTitleWidth(); SensorCall(); return ReplaceReturn;}
}

Rectangle ImplBorderWindow::GetMenuRect() const
{
    {Rectangle  ReplaceReturn = mpBorderView->GetMenuRect(); SensorCall(); return ReplaceReturn;}
}

void ImplBorderWindow::MoveToByDrag(const Point& rNewPos)
{
SensorCall();    setPosSizePixel(rNewPos.X(), rNewPos.Y(), 0, 0, PosSizeFlags::Pos | PosSizeFlags::ByDrag);
SensorCall();}

Size ImplBorderWindow::GetOptimalSize() const
{
    SensorCall();const vcl::Window* pClientWindow = ImplGetClientWindow();
    SensorCall();if (pClientWindow)
        {/*329*/{Size  ReplaceReturn = pClientWindow->GetOptimalSize(); SensorCall(); return ReplaceReturn;}/*330*/}
    {Size  ReplaceReturn = Size(mnMinWidth, mnMinHeight); SensorCall(); return ReplaceReturn;}
}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
