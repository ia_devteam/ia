#include "var/tmp/sensor.h"
/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This file incorporates work covered by the following license notice:
 *
 *   Licensed to the Apache Software Foundation (ASF) under one or more
 *   contributor license agreements. See the NOTICE file distributed
 *   with this work for additional information regarding copyright
 *   ownership. The ASF licenses this file to you under the Apache
 *   License, Version 2.0 (the "License"); you may not use this file
 *   except in compliance with the License. You may obtain a copy of
 *   the License at http://www.apache.org/licenses/LICENSE-2.0 .
 */

#include <svl/urihelper.hxx>
#include <com/sun/star/ucb/Command.hpp>
#include <com/sun/star/ucb/IllegalIdentifierException.hpp>
#include <com/sun/star/ucb/UniversalContentBroker.hpp>
#include <com/sun/star/ucb/UnsupportedCommandException.hpp>
#include <com/sun/star/ucb/XCommandEnvironment.hpp>
#include <com/sun/star/ucb/XCommandProcessor.hpp>
#include <com/sun/star/ucb/XContent.hpp>
#include <com/sun/star/ucb/XUniversalContentBroker.hpp>
#include <com/sun/star/uno/Any.hxx>
#include <com/sun/star/uno/Exception.hpp>
#include <com/sun/star/uno/Reference.hxx>
#include <com/sun/star/uno/RuntimeException.hpp>
#include <com/sun/star/uno/XComponentContext.hpp>
#include <com/sun/star/uri/UriReferenceFactory.hpp>
#include <com/sun/star/uri/XUriReference.hpp>
#include <com/sun/star/uri/XUriReferenceFactory.hpp>
#include <comphelper/processfactory.hxx>
#include <osl/diagnose.h>
#include <rtl/character.hxx>
#include <rtl/instance.hxx>
#include <rtl/ustrbuf.hxx>
#include <rtl/ustring.h>
#include <rtl/ustring.hxx>
#include <sal/types.h>
#include <tools/inetmime.hxx>
#include <unotools/charclass.hxx>

using namespace com::sun::star;

OUString URIHelper::SmartRel2Abs(INetURLObject const & rTheBaseURIRef,
                                 OUString const & rTheRelURIRef,
                                 Link<OUString *, bool> const & rMaybeFileHdl,
                                 bool bCheckFileExists,
                                 bool bIgnoreFragment,
                                 INetURLObject::EncodeMechanism eEncodeMechanism,
                                 INetURLObject::DecodeMechanism eDecodeMechanism,
                                 rtl_TextEncoding eCharset,
                                 bool bRelativeNonURIs,
                                 INetURLObject::FSysStyle eStyle)
{
    // Backwards compatibility:
    SensorCall();if( rTheRelURIRef.startsWith("#") )
        {/*37*/{rtl::OUString  ReplaceReturn = rTheRelURIRef; SensorCall(); return ReplaceReturn;}/*38*/}

    SensorCall();INetURLObject aAbsURIRef;
    SensorCall();if (rTheBaseURIRef.HasError())
        {/*39*/SensorCall();aAbsURIRef. SetSmartURL(rTheRelURIRef, eEncodeMechanism, eCharset, eStyle);/*40*/}
    else
    {
        SensorCall();bool bWasAbsolute;
        aAbsURIRef = rTheBaseURIRef.smartRel2Abs(rTheRelURIRef,
                                                 bWasAbsolute,
                                                 bIgnoreFragment,
                                                 eEncodeMechanism,
                                                 eCharset,
                                                 bRelativeNonURIs,
                                                 eStyle);
        SensorCall();if (bCheckFileExists
            && !bWasAbsolute
            && (aAbsURIRef.GetProtocol() == INetProtocol::File))
        {
            SensorCall();INetURLObject aNonFileURIRef;
            aNonFileURIRef.SetSmartURL(rTheRelURIRef,
                                       eEncodeMechanism,
                                       eCharset,
                                       eStyle);
            SensorCall();if (!aNonFileURIRef.HasError()
                && aNonFileURIRef.GetProtocol() != INetProtocol::File)
            {
                SensorCall();bool bMaybeFile = false;
                SensorCall();if (rMaybeFileHdl.IsSet())
                {
                    SensorCall();OUString aFilePath(rTheRelURIRef);
                    bMaybeFile = rMaybeFileHdl.Call(&aFilePath);
                }
                SensorCall();if (!bMaybeFile)
                    {/*41*/SensorCall();aAbsURIRef = aNonFileURIRef;/*42*/}
            }
        }
    }
    {rtl::OUString  ReplaceReturn = aAbsURIRef.GetMainURL(eDecodeMechanism, eCharset); SensorCall(); return ReplaceReturn;}
}

namespace { struct MaybeFileHdl : public rtl::Static< Link<OUString *, bool>, MaybeFileHdl > {}; }

void URIHelper::SetMaybeFileHdl(Link<OUString *, bool> const & rTheMaybeFileHdl)
{
SensorCall();    MaybeFileHdl::get() = rTheMaybeFileHdl;
SensorCall();}

Link<OUString *, bool> URIHelper::GetMaybeFileHdl()
{
SensorCall();    return MaybeFileHdl::get();
SensorCall();}

namespace {

bool isAbsoluteHierarchicalUriReference(
    css::uno::Reference< css::uri::XUriReference > const & uriReference)
{
SensorCall();    return uriReference.is() && uriReference->isAbsolute()
        && uriReference->isHierarchical() && !uriReference->hasRelativePath();
SensorCall();}

// To improve performance, assume that if for any prefix URL of a given
// hierarchical URL either a UCB content cannot be created, or the UCB content
// does not support the getCasePreservingURL command, then this will hold for
// any other prefix URL of the given URL, too:
enum Result { Success, GeneralFailure, SpecificFailure };

Result normalizePrefix( css::uno::Reference< css::ucb::XUniversalContentBroker > const & broker,
                        OUString const & uri, OUString * normalized)
{
SensorCall();    OSL_ASSERT(broker.is() && normalized != 0);
    css::uno::Reference< css::ucb::XContent > content;
    SensorCall();try {
        content = broker->queryContent(broker->createContentIdentifier(uri));
    } catch (css::ucb::IllegalIdentifierException &) {}
    SensorCall();if (!content.is()) {
        {__IAENUM__  ReplaceReturn = GeneralFailure; SensorCall(); return ReplaceReturn;}
    }
    SensorCall();try {
        #if OSL_DEBUG_LEVEL > 0
        bool ok =
        #endif
            (css::uno::Reference< css::ucb::XCommandProcessor >(
                   content, css::uno::UNO_QUERY_THROW)->execute(
                       css::ucb::Command("getCasePreservingURL",
                           -1, css::uno::Any()),
                       0,
                       css::uno::Reference< css::ucb::XCommandEnvironment >())
               >>= *normalized);
        OSL_ASSERT(ok);
    } catch (css::uno::RuntimeException &) {
        SensorCall();throw;
    } catch (css::ucb::UnsupportedCommandException &) {
        {__IAENUM__  ReplaceReturn = GeneralFailure; SensorCall(); return ReplaceReturn;}
    } catch (css::uno::Exception &) {
        {__IAENUM__  ReplaceReturn = SpecificFailure; SensorCall(); return ReplaceReturn;}
    }
    {__IAENUM__  ReplaceReturn = Success; SensorCall(); return ReplaceReturn;}
}

OUString normalize(
    css::uno::Reference< css::ucb::XUniversalContentBroker > const & broker,
    css::uno::Reference< css::uri::XUriReferenceFactory > const & uriFactory,
    OUString const & uriReference)
{
    // normalizePrefix can potentially fail (a typically example being a file
    // URL that denotes a non-existing resource); in such a case, try to
    // normalize as long a prefix of the given URL as possible (i.e., normalize
    // all the existing directories within the path):
    SensorCall();OUString normalized;
    sal_Int32 n = uriReference.indexOf('#');
    normalized = n == -1 ? uriReference : uriReference.copy(0, n);
    SensorCall();switch (normalizePrefix(broker, normalized, &normalized)) {
    case Success:
        return n == -1 ? normalized : normalized + uriReference.copy(n);
    case GeneralFailure:
        {rtl::OUString  ReplaceReturn = uriReference; SensorCall(); return ReplaceReturn;}
    case SpecificFailure:
    default:
        SensorCall();break;
    }
    SensorCall();css::uno::Reference< css::uri::XUriReference > ref(
        uriFactory->parse(uriReference));
    SensorCall();if (!isAbsoluteHierarchicalUriReference(ref)) {
        {rtl::OUString  ReplaceReturn = uriReference; SensorCall(); return ReplaceReturn;}
    }
    SensorCall();sal_Int32 count = ref->getPathSegmentCount();
    SensorCall();if (count < 2) {
        {rtl::OUString  ReplaceReturn = uriReference; SensorCall(); return ReplaceReturn;}
    }
    SensorCall();OUStringBuffer head(ref->getScheme());
    head.append(':');
    SensorCall();if (ref->hasAuthority()) {
        SensorCall();head.append("//");
        head.append(ref->getAuthority());
    }
    SensorCall();for (sal_Int32 i = count - 1; i > 0; --i) {
        SensorCall();OUStringBuffer buf(head);
        SensorCall();for (sal_Int32 j = 0; j < i; ++j) {
            SensorCall();buf.append('/');
            buf.append(ref->getPathSegment(j));
        }
        SensorCall();normalized = buf.makeStringAndClear();
        SensorCall();if (normalizePrefix(broker, normalized, &normalized) != SpecificFailure)
        {
            SensorCall();buf.append(normalized);
            css::uno::Reference< css::uri::XUriReference > preRef(
                uriFactory->parse(normalized));
            SensorCall();if (!isAbsoluteHierarchicalUriReference(preRef)) {
                // This could only happen if something is inconsistent:
                SensorCall();break;
            }
            SensorCall();sal_Int32 preCount = preRef->getPathSegmentCount();
            // normalizePrefix may have added or removed a final slash:
            SensorCall();if (preCount != i) {
                SensorCall();if (preCount == i - 1) {
                    SensorCall();buf.append('/');
                } else {/*43*/SensorCall();if (preCount - 1 == i && !buf.isEmpty()
                           && buf[buf.getLength() - 1] == '/')
                {
                    SensorCall();buf.setLength(buf.getLength() - 1);
                } else {
                    // This could only happen if something is inconsistent:
                    SensorCall();break;
                ;/*44*/}}
            }
            SensorCall();for (sal_Int32 j = i; j < count; ++j) {
                SensorCall();buf.append('/');
                buf.append(ref->getPathSegment(j));
            }
            SensorCall();if (ref->hasQuery()) {
                SensorCall();buf.append('?');
                buf.append(ref->getQuery());
            }
            SensorCall();if (ref->hasFragment()) {
                SensorCall();buf.append('#');
                buf.append(ref->getFragment());
            }
            {rtl::OUString  ReplaceReturn = buf.makeStringAndClear(); SensorCall(); return ReplaceReturn;}
        }
    }
    {rtl::OUString  ReplaceReturn = uriReference; SensorCall(); return ReplaceReturn;}
}

}

css::uno::Reference< css::uri::XUriReference >
URIHelper::normalizedMakeRelative(
    css::uno::Reference< css::uno::XComponentContext > const & context,
    OUString const & baseUriReference, OUString const & uriReference)
{
SensorCall();    OSL_ASSERT(context.is());
    css::uno::Reference< css::ucb::XUniversalContentBroker > broker(
        css::ucb::UniversalContentBroker::create(context));
    css::uno::Reference< css::uri::XUriReferenceFactory > uriFactory(
        css::uri::UriReferenceFactory::create(context));
    return uriFactory->makeRelative(
        uriFactory->parse(normalize(broker, uriFactory, baseUriReference)),
        uriFactory->parse(normalize(broker, uriFactory, uriReference)), true,
        true, false);
SensorCall();}

OUString URIHelper::simpleNormalizedMakeRelative(
    OUString const & baseUriReference, OUString const & uriReference)
{
    SensorCall();com::sun::star::uno::Reference< com::sun::star::uri::XUriReference > rel(
        URIHelper::normalizedMakeRelative(
            comphelper::getProcessComponentContext(), baseUriReference,
            uriReference));
    return rel.is() ? rel->getUriReference() : uriReference;
SensorCall();}


//  FindFirstURLInText


namespace {

inline sal_Int32 nextChar(OUString const & rStr, sal_Int32 nPos)
{
    {sal_Int32  ReplaceReturn = rtl::isHighSurrogate(rStr[nPos])
           && rStr.getLength() - nPos >= 2
           && rtl::isLowSurrogate(rStr[nPos + 1]) ?
        nPos + 2 : nPos + 1; SensorCall(); return ReplaceReturn;}
}

bool isBoundary1(CharClass const & rCharClass, OUString const & rStr,
                 sal_Int32 nPos, sal_Int32 nEnd)
{
    SensorCall();if (nPos == nEnd)
        {/*45*/{_Bool  ReplaceReturn = true; SensorCall(); return ReplaceReturn;}/*46*/}
    SensorCall();if (rCharClass.isLetterNumeric(rStr, nPos))
        {/*47*/{_Bool  ReplaceReturn = false; SensorCall(); return ReplaceReturn;}/*48*/}
    SensorCall();switch (rStr[nPos])
    {
    case '$':
    case '%':
    case '&':
    case '-':
    case '/':
    case '@':
    case '\\':
        {_Bool  ReplaceReturn = false; SensorCall(); return ReplaceReturn;}
    default:
        {_Bool  ReplaceReturn = true; SensorCall(); return ReplaceReturn;}
    }
SensorCall();}

bool isBoundary2(CharClass const & rCharClass, OUString const & rStr,
                 sal_Int32 nPos, sal_Int32 nEnd)
{
    SensorCall();if (nPos == nEnd)
        {/*49*/{_Bool  ReplaceReturn = true; SensorCall(); return ReplaceReturn;}/*50*/}
    SensorCall();if (rCharClass.isLetterNumeric(rStr, nPos))
        {/*51*/{_Bool  ReplaceReturn = false; SensorCall(); return ReplaceReturn;}/*52*/}
    SensorCall();switch (rStr[nPos])
    {
    case '!':
    case '#':
    case '$':
    case '%':
    case '&':
    case '\'':
    case '*':
    case '+':
    case '-':
    case '/':
    case '=':
    case '?':
    case '@':
    case '^':
    case '_':
    case '`':
    case '{':
    case '|':
    case '}':
    case '~':
        {_Bool  ReplaceReturn = false; SensorCall(); return ReplaceReturn;}
    default:
        {_Bool  ReplaceReturn = true; SensorCall(); return ReplaceReturn;}
    }
SensorCall();}

bool checkWChar(CharClass const & rCharClass, OUString const & rStr,
                sal_Int32 * pPos, sal_Int32 * pEnd, bool bBackslash = false,
                bool bPipe = false)
{
    SensorCall();sal_Unicode c = rStr[*pPos];
    SensorCall();if (rtl::isAscii(c))
    {
        SensorCall();static sal_uInt8 const aMap[128]
            = { 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 4, 4, 4, 1,   //  !"#$%&'
                1, 1, 1, 1, 1, 4, 1, 4,   // ()*+,-./
                4, 4, 4, 4, 4, 4, 4, 4,   // 01234567
                4, 4, 1, 1, 0, 1, 0, 1,   // 89:;<=>?
                4, 4, 4, 4, 4, 4, 4, 4,   // @ABCDEFG
                4, 4, 4, 4, 4, 4, 4, 4,   // HIJKLMNO
                4, 4, 4, 4, 4, 4, 4, 4,   // PQRSTUVW
                4, 4, 4, 1, 2, 1, 0, 1,   // XYZ[\]^_
                0, 4, 4, 4, 4, 4, 4, 4,   // `abcdefg
                4, 4, 4, 4, 4, 4, 4, 4,   // hijklmno
                4, 4, 4, 4, 4, 4, 4, 4,   // pqrstuvw
                4, 4, 4, 0, 3, 0, 1, 0 }; // xyz{|}~
        SensorCall();switch (aMap[c])
        {
            default: // not uric
                {_Bool  ReplaceReturn = false; SensorCall(); return ReplaceReturn;}

            case 1: // uric
                SensorCall();++(*pPos);
                {_Bool  ReplaceReturn = true; SensorCall(); return ReplaceReturn;}

            case 2: // "\"
                SensorCall();if (bBackslash)
                {
                    SensorCall();*pEnd = ++(*pPos);
                    {_Bool  ReplaceReturn = true; SensorCall(); return ReplaceReturn;}
                }
                else
                    {/*53*/{_Bool  ReplaceReturn = false; SensorCall(); return ReplaceReturn;}/*54*/}

            case 3: // "|"
                SensorCall();if (bPipe)
                {
                    SensorCall();*pEnd = ++(*pPos);
                    {_Bool  ReplaceReturn = true; SensorCall(); return ReplaceReturn;}
                }
                else
                    {/*55*/{_Bool  ReplaceReturn = false; SensorCall(); return ReplaceReturn;}/*56*/}

            case 4: // alpha, digit, "$", "%", "&", "-", "/", "@" (see
                    // isBoundary1)
                SensorCall();*pEnd = ++(*pPos);
                {_Bool  ReplaceReturn = true; SensorCall(); return ReplaceReturn;}
        }
    }
    else {/*57*/SensorCall();if (rCharClass.isLetterNumeric(rStr, *pPos))
    {
        SensorCall();*pEnd = *pPos = nextChar(rStr, *pPos);
        {_Bool  ReplaceReturn = true; SensorCall(); return ReplaceReturn;}
    }
    else
        {/*59*/{_Bool  ReplaceReturn = false; SensorCall(); return ReplaceReturn;}/*60*/}/*58*/}
SensorCall();}

sal_uInt32 scanDomain(OUString const & rStr, sal_Int32 * pPos,
                      sal_Int32 nEnd)
{
    SensorCall();sal_Unicode const * pBuffer = rStr.getStr();
    sal_Unicode const * p = pBuffer + *pPos;
    sal_uInt32 nLabels = INetURLObject::scanDomain(p, pBuffer + nEnd, false);
    *pPos = sal::static_int_cast< sal_Int32 >(p - pBuffer);
    {sal_uInt32  ReplaceReturn = nLabels; SensorCall(); return ReplaceReturn;}
}

}

OUString URIHelper::FindFirstURLInText(OUString const & rText,
                                       sal_Int32 & rBegin,
                                       sal_Int32 & rEnd,
                                       CharClass const & rCharClass,
                                       INetURLObject::EncodeMechanism eMechanism,
                                       rtl_TextEncoding eCharset,
                                       INetURLObject::FSysStyle eStyle)
{
    SensorCall();if (!(rBegin <= rEnd && rEnd <= rText.getLength()))
        {/*1*/{rtl::OUString  ReplaceReturn = OUString(); SensorCall(); return ReplaceReturn;}/*2*/}

    // Search for the first substring of [rBegin..rEnd[ that matches any of the
    // following productions (for which the appropriate style bit is set in
    // eStyle, if applicable).

    // 1st Production (known scheme):
    //    \B1 <one of the known schemes, except file> ":" 1*wchar ["#" 1*wchar]
    //        \B1

    // 2nd Production (file):
    //    \B1 "FILE:" 1*(wchar / "\" / "|") ["#" 1*wchar] \B1

    // 3rd Production (ftp):
    //    \B1 "FTP" 2*("." label) ["/" *wchar] ["#" 1*wchar] \B1

    // 4th Production (http):
    //    \B1 "WWW" 2*("." label) ["/" *wchar] ["#" 1*wchar] \B1

    // 5th Production (mailto):
    //    \B2 local-part "@" domain \B1

    // 6th Production (UNC file):
    //    \B1 "\\" domain "\" *(wchar / "\") \B1

    // 7th Production (DOS file):
    //    \B1 ALPHA ":\" *(wchar / "\") \B1

    // 8th Production (Unix-like DOS file):
    //    \B1 ALPHA ":/" *(wchar / "\") \B1

    // The productions use the following auxiliary rules.

    //    local-part = atom *("." atom)
    //    atom = 1*(alphanum / "!" / "#" / "$" / "%" / "&" / "'" / "*" / "+"
    //              / "-" / "/" / "=" / "?" / "^" / "_" / "`" / "{" / "|" / "}"
    //              / "~")
    //    domain = label *("." label)
    //    label = alphanum [*(alphanum / "-") alphanum]
    //    alphanum = ALPHA / DIGIT
    //    wchar = <any uric character (ignoring the escaped rule), or "%", or
    //             a letter or digit (according to rCharClass)>

    // "\B1" (boundary 1) stands for the beginning or end of the block of text,
    // or a character that is neither (a) a letter or digit (according to
    // rCharClass), nor (b) any of "$", "%", "&", "-", "/", "@", or "\".
    // (FIXME:  What was the rationale for this set of punctuation characters?)

    // "\B2" (boundary 2) stands for the beginning or end of the block of text,
    // or a character that is neither (a) a letter or digit (according to
    // rCharClass), nor (b) any of "!", "#", "$", "%", "&", "'", "*", "+", "-",
    // "/", "=", "?", "@", "^", "_", "`", "{", "|", "}", or "~" (i.e., an RFC
    // 822 <atom> character, or "@" from \B1's set above).

    // Productions 1--4, and 6--8 try to find a maximum-length match, but they
    // stop at the first <wchar> character that is a "\B1" character which is
    // only followed by "\B1" characters (taking "\" and "|" characters into
    // account appropriately).  Production 5 simply tries to find a maximum-
    // length match.

    // Productions 1--4 use the given eMechanism and eCharset.  Productions 5--9
    // use ENCODE_ALL.

    // Productions 6--9 are only applicable if the FSYS_DOS bit is set in
    // eStyle.

    SensorCall();bool bBoundary1 = true;
    bool bBoundary2 = true;
    SensorCall();for (sal_Int32 nPos = rBegin; nPos != rEnd; nPos = nextChar(rText, nPos))
    {
        SensorCall();sal_Unicode c = rText[nPos];
        SensorCall();if (bBoundary1)
        {
            SensorCall();if (rtl::isAsciiAlpha(c))
            {
                SensorCall();sal_Int32 i = nPos;
                INetProtocol eScheme = INetURLObject::CompareProtocolScheme(rText.copy(i, rEnd - i));
                SensorCall();if (eScheme == INetProtocol::File) // 2nd
                {
                    SensorCall();while (rText[i++] != ':') ;
                    SensorCall();sal_Int32 nPrefixEnd = i;
                    sal_Int32 nUriEnd = i;
                    SensorCall();while (i != rEnd
                           && checkWChar(rCharClass, rText, &i, &nUriEnd, true,
                                         true)) ;
                    SensorCall();if (i != nPrefixEnd && i != rEnd && rText[i] == '#')
                    {
                        SensorCall();++i;
                        SensorCall();while (i != rEnd
                               && checkWChar(rCharClass, rText, &i, &nUriEnd)) ;
                    }
                    SensorCall();if (nUriEnd != nPrefixEnd
                        && isBoundary1(rCharClass, rText, nUriEnd, rEnd))
                    {
                        SensorCall();INetURLObject aUri(rText.copy(nPos, nUriEnd - nPos),
                                           INetProtocol::File, eMechanism, eCharset,
                                           eStyle);
                        SensorCall();if (!aUri.HasError())
                        {
                            SensorCall();rBegin = nPos;
                            rEnd = nUriEnd;
                            SensorCall();return
                                aUri.GetMainURL(INetURLObject::DECODE_TO_IURI);
                        }
                    }
                }
                else {/*9*/SensorCall();if (eScheme != INetProtocol::NotValid) // 1st
                {
                    SensorCall();while (rText[i++] != ':') ;
                    SensorCall();sal_Int32 nPrefixEnd = i;
                    sal_Int32 nUriEnd = i;
                    SensorCall();while (i != rEnd
                           && checkWChar(rCharClass, rText, &i, &nUriEnd)) ;
                    SensorCall();if (i != nPrefixEnd && i != rEnd && rText[i] == '#')
                    {
                        SensorCall();++i;
                        SensorCall();while (i != rEnd
                               && checkWChar(rCharClass, rText, &i, &nUriEnd)) ;
                    }
                    SensorCall();if (nUriEnd != nPrefixEnd
                        && (isBoundary1(rCharClass, rText, nUriEnd, rEnd)
                            || rText[nUriEnd] == '\\'))
                    {
                        SensorCall();INetURLObject aUri(rText.copy(nPos, nUriEnd - nPos),
                                           INetProtocol::Http, eMechanism,
                                           eCharset);
                        SensorCall();if (!aUri.HasError())
                        {
                            SensorCall();rBegin = nPos;
                            rEnd = nUriEnd;
                            SensorCall();return
                                aUri.GetMainURL(INetURLObject::DECODE_TO_IURI);
                        }
                    }
                ;/*10*/}}

                // 3rd, 4th:
                SensorCall();i = nPos;
                sal_uInt32 nLabels = scanDomain(rText, &i, rEnd);
                SensorCall();if (nLabels >= 3
                    && rText[nPos + 3] == '.'
                    && (((rText[nPos] == 'w'
                          || rText[nPos] == 'W')
                         && (rText[nPos + 1] == 'w'
                             || rText[nPos + 1] == 'W')
                         && (rText[nPos + 2] == 'w'
                             || rText[nPos + 2] == 'W'))
                        || ((rText[nPos] == 'f'
                             || rText[nPos] == 'F')
                            && (rText[nPos + 1] == 't'
                                || rText[nPos + 1] == 'T')
                            && (rText[nPos + 2] == 'p'
                                || rText[nPos + 2] == 'P'))))
                    // (note that rText.GetChar(nPos + 3) is guaranteed to be
                    // valid)
                {
                    SensorCall();sal_Int32 nUriEnd = i;
                    SensorCall();if (i != rEnd && rText[i] == '/')
                    {
                        SensorCall();nUriEnd = ++i;
                        SensorCall();while (i != rEnd
                               && checkWChar(rCharClass, rText, &i, &nUriEnd)) ;
                    }
                    SensorCall();if (i != rEnd && rText[i] == '#')
                    {
                        SensorCall();++i;
                        SensorCall();while (i != rEnd
                               && checkWChar(rCharClass, rText, &i, &nUriEnd)) ;
                    }
                    SensorCall();if (isBoundary1(rCharClass, rText, nUriEnd, rEnd)
                        || rText[nUriEnd] == '\\')
                    {
                        SensorCall();INetURLObject aUri(rText.copy(nPos, nUriEnd - nPos),
                                           INetProtocol::Http, eMechanism,
                                           eCharset);
                        SensorCall();if (!aUri.HasError())
                        {
                            SensorCall();rBegin = nPos;
                            rEnd = nUriEnd;
                            SensorCall();return
                                aUri.GetMainURL(INetURLObject::DECODE_TO_IURI);
                        }
                    }
                }

                SensorCall();if ((eStyle & INetURLObject::FSYS_DOS) != 0 && rEnd - nPos >= 3
                    && rText[nPos + 1] == ':'
                    && (rText[nPos + 2] == '/'
                        || rText[nPos + 2] == '\\')) // 7th, 8th
                {
                    SensorCall();i = nPos + 3;
                    sal_Int32 nUriEnd = i;
                    SensorCall();while (i != rEnd
                           && checkWChar(rCharClass, rText, &i, &nUriEnd)) ;
                    SensorCall();if (isBoundary1(rCharClass, rText, nUriEnd, rEnd))
                    {
                        SensorCall();INetURLObject aUri(rText.copy(nPos, nUriEnd - nPos),
                                           INetProtocol::File,
                                           INetURLObject::ENCODE_ALL,
                                           RTL_TEXTENCODING_UTF8,
                                           INetURLObject::FSYS_DOS);
                        SensorCall();if (!aUri.HasError())
                        {
                            SensorCall();rBegin = nPos;
                            rEnd = nUriEnd;
                            SensorCall();return
                                aUri.GetMainURL(INetURLObject::DECODE_TO_IURI);
                        }
                    }
                }
            }
            else {/*23*/SensorCall();if ((eStyle & INetURLObject::FSYS_DOS) != 0 && rEnd - nPos >= 2
                     && rText[nPos] == '\\'
                     && rText[nPos + 1] == '\\') // 6th
            {
                SensorCall();sal_Int32 i = nPos + 2;
                sal_uInt32 nLabels = scanDomain(rText, &i, rEnd);
                SensorCall();if (nLabels >= 1 && i != rEnd && rText[i] == '\\')
                {
                    SensorCall();sal_Int32 nUriEnd = ++i;
                    SensorCall();while (i != rEnd
                           && checkWChar(rCharClass, rText, &i, &nUriEnd,
                                         true)) ;
                    SensorCall();if (isBoundary1(rCharClass, rText, nUriEnd, rEnd))
                    {
                        SensorCall();INetURLObject aUri(rText.copy(nPos, nUriEnd - nPos),
                                           INetProtocol::File,
                                           INetURLObject::ENCODE_ALL,
                                           RTL_TEXTENCODING_UTF8,
                                           INetURLObject::FSYS_DOS);
                        SensorCall();if (!aUri.HasError())
                        {
                            SensorCall();rBegin = nPos;
                            rEnd = nUriEnd;
                            SensorCall();return
                                aUri.GetMainURL(INetURLObject::DECODE_TO_IURI);
                        }
                    }
                }
            ;/*24*/}}
        }
        SensorCall();if (bBoundary2 && INetMIME::isAtomChar(c)) // 5th
        {
            SensorCall();bool bDot = false;
            SensorCall();for (sal_Int32 i = nPos + 1; i != rEnd; ++i)
            {
                SensorCall();sal_Unicode c2 = rText[i];
                SensorCall();if (INetMIME::isAtomChar(c2))
                    {/*27*/SensorCall();bDot = false;/*28*/}
                else {/*29*/SensorCall();if (bDot)
                    {/*31*/SensorCall();break;/*32*/}
                else {/*33*/SensorCall();if (c2 == '.')
                    {/*35*/SensorCall();bDot = true;/*36*/}
                else
                {
                    SensorCall();if (c2 == '@')
                    {
                        SensorCall();++i;
                        sal_uInt32 nLabels = scanDomain(rText, &i, rEnd);
                        SensorCall();if (nLabels >= 1
                            && isBoundary1(rCharClass, rText, i, rEnd))
                        {
                            SensorCall();INetURLObject aUri(rText.copy(nPos, i - nPos),
                                               INetProtocol::Mailto,
                                               INetURLObject::ENCODE_ALL);
                            SensorCall();if (!aUri.HasError())
                            {
                                SensorCall();rBegin = nPos;
                                rEnd = i;
                                {rtl::OUString  ReplaceReturn = aUri.GetMainURL(
                                           INetURLObject::DECODE_TO_IURI); SensorCall(); return ReplaceReturn;}
                            }
                        }
                    }
                    SensorCall();break;
                ;/*34*/}/*30*/}}
            }
        }
        SensorCall();bBoundary1 = isBoundary1(rCharClass, rText, nPos, rEnd);
        bBoundary2 = isBoundary2(rCharClass, rText, nPos, rEnd);
    }
    SensorCall();rBegin = rEnd;
    {rtl::OUString  ReplaceReturn = OUString(); SensorCall(); return ReplaceReturn;}
}

OUString URIHelper::removePassword(OUString const & rURI,
                                   INetURLObject::EncodeMechanism eEncodeMechanism,
                                   INetURLObject::DecodeMechanism eDecodeMechanism,
                                   rtl_TextEncoding eCharset)
{
    SensorCall();INetURLObject aObj(rURI, eEncodeMechanism, eCharset);
    {rtl::OUString  ReplaceReturn = aObj.HasError() ?
               rURI :
               aObj.GetURLNoPass(eDecodeMechanism, eCharset); SensorCall(); return ReplaceReturn;}
}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
