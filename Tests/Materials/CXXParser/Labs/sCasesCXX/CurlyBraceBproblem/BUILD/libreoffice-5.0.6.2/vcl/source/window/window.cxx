#include "var/tmp/sensor.h"
/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This file incorporates work covered by the following license notice:
 *
 *   Licensed to the Apache Software Foundation (ASF) under one or more
 *   contributor license agreements. See the NOTICE file distributed
 *   with this work for additional information regarding copyright
 *   ownership. The ASF licenses this file to you under the Apache
 *   License, Version 2.0 (the "License"); you may not use this file
 *   except in compliance with the License. You may obtain a copy of
 *   the License at http://www.apache.org/licenses/LICENSE-2.0 .
 */

#include <config_features.h>
#include <rtl/strbuf.hxx>
#include <tools/rc.h>

#include <sal/types.h>

#include <vcl/salgtype.hxx>
#include <vcl/event.hxx>
#include <vcl/help.hxx>
#include <vcl/cursor.hxx>
#include <vcl/svapp.hxx>
#include <vcl/window.hxx>
#include <vcl/syswin.hxx>
#include <vcl/syschild.hxx>
#include <vcl/dockwin.hxx>
#include <vcl/wall.hxx>
#include <vcl/fixed.hxx>
#include <vcl/gradient.hxx>
#include <vcl/button.hxx>
#include <vcl/taskpanelist.hxx>
#include <vcl/dialog.hxx>
#include <vcl/unowrap.hxx>
#include <vcl/gdimtf.hxx>
#include <vcl/lazydelete.hxx>
#include <vcl/virdev.hxx>
#include <vcl/settings.hxx>
#include <vcl/sysdata.hxx>

#include <salframe.hxx>
#include <salobj.hxx>
#include <salinst.hxx>
#include <salgdi.hxx>
#include <svdata.hxx>
#include <dbggui.hxx>
#include <window.h>
#include <toolbox.h>
#include <outdev.h>
#include <brdwin.hxx>
#include <helpwin.hxx>
#include <dndlcon.hxx>

#include <com/sun/star/awt/XTopWindow.hpp>
#include <com/sun/star/awt/XDisplayConnection.hpp>
#include <com/sun/star/datatransfer/clipboard/XClipboard.hpp>
#include <com/sun/star/datatransfer/clipboard/SystemClipboard.hpp>
#include <com/sun/star/rendering/CanvasFactory.hpp>
#include <com/sun/star/rendering/XSpriteCanvas.hpp>
#include <comphelper/processfactory.hxx>

#include <cassert>
#include <set>
#include <typeinfo>

#ifdef WNT // see #140456#
#include <win/salframe.h>
#endif


using namespace ::com::sun::star::uno;
using namespace ::com::sun::star::lang;
using namespace ::com::sun::star::datatransfer::clipboard;
using namespace ::com::sun::star::datatransfer::dnd;

using ::com::sun::star::awt::XTopWindow;

namespace vcl {

Window::Window( WindowType nType )
{
    SensorCall();ImplInitWindowData( nType );
SensorCall();}

Window::Window( vcl::Window* pParent, WinBits nStyle )
{

    SensorCall();ImplInitWindowData( WINDOW_WINDOW );
    ImplInit( pParent, nStyle, NULL );
SensorCall();}

Window::Window( vcl::Window* pParent, const ResId& rResId )
    : mpWindowImpl(NULL)
{
    SensorCall();rResId.SetRT( RSC_WINDOW );
    WinBits nStyle = ImplInitRes( rResId );
    ImplInitWindowData( WINDOW_WINDOW );
    ImplInit( pParent, nStyle, NULL );
    ImplLoadRes( rResId );

    SensorCall();if ( !(nStyle & WB_HIDE) )
        {/*1*/SensorCall();Show();/*2*/}
SensorCall();}

#if OSL_DEBUG_LEVEL > 0
namespace
{
    OString lcl_createWindowInfo(const vcl::Window& i_rWindow)
    {
        // skip border windows, they don't carry information which helps diagnosing the problem
        const vcl::Window* pWindow( &i_rWindow );
        while ( pWindow && ( pWindow->GetType() == WINDOW_BORDERWINDOW ) )
            pWindow = pWindow->GetWindow( GetWindowType::FirstChild );
        if ( !pWindow )
            pWindow = &i_rWindow;

        OStringBuffer aErrorString;
        aErrorString.append(' ');
        aErrorString.append(typeid( *pWindow ).name());
        aErrorString.append(" (");
        aErrorString.append(OUStringToOString(pWindow->GetText(), RTL_TEXTENCODING_UTF8));
        aErrorString.append(")");
        return aErrorString.makeStringAndClear();
    }
}
#endif

bool Window::IsDisposed() const
{
    {const _Bool  ReplaceReturn = !mpWindowImpl; SensorCall(); return ReplaceReturn;}
}

void Window::dispose()
{
    assert( mpWindowImpl );
    assert( !mpWindowImpl->mbInDispose ); // should only be called from disposeOnce()
    assert( (!mpWindowImpl->mpParent ||
            !mpWindowImpl->mpParent->IsDisposed()) &&
            "vcl::Window child should have its parent disposed first" );

    // remove Key and Mouse events issued by Application::PostKey/MouseEvent
    Application::RemoveMouseAndKeyEvents( this );

    // Dispose of the canvas implementation (which, currently, has an
    // own wrapper window as a child to this one.
    Reference< css::rendering::XCanvas > xCanvas( mpWindowImpl->mxCanvas );
    SensorCall();if( xCanvas.is() )
    {
        SensorCall();Reference < XComponent > xCanvasComponent( xCanvas, UNO_QUERY );
        if( xCanvasComponent.is() )
            xCanvasComponent->dispose();
    }

    SensorCall();mpWindowImpl->mbInDispose = true;

    CallEventListeners( VCLEVENT_OBJECT_DYING );

    // do not send child events for frames that were registered as native frames
    SensorCall();if( !ImplIsAccessibleNativeFrame() && mpWindowImpl->mbReallyVisible )
        {/*3*/SensorCall();if ( ImplIsAccessibleCandidate() && GetAccessibleParentWindow() )
            {/*5*/SensorCall();GetAccessibleParentWindow()->CallEventListeners( VCLEVENT_WINDOW_CHILDDESTROYED, this );/*6*/}/*4*/}

    // remove associated data structures from dockingmanager
    SensorCall();ImplGetDockingManager()->RemoveWindow( this );

    // remove ownerdraw decorated windows from list in the top-most frame window
    SensorCall();if( (GetStyle() & WB_OWNERDRAWDECORATION) && mpWindowImpl->mbFrame )
    {
        SensorCall();::std::vector< VclPtr<vcl::Window> >& rList = ImplGetOwnerDrawList();
        auto p = ::std::find( rList.begin(), rList.end(), VclPtr<vcl::Window>(this) );
        if( p != rList.end() )
            rList.erase( p );
    }

    // shutdown drag and drop
    SensorCall();Reference < XComponent > xDnDComponent( mpWindowImpl->mxDNDListenerContainer, UNO_QUERY );

    if( xDnDComponent.is() )
        xDnDComponent->dispose();

    SensorCall();if( mpWindowImpl->mbFrame && mpWindowImpl->mpFrameData )
    {
        SensorCall();try
        {
            // deregister drop target listener
            SensorCall();if( mpWindowImpl->mpFrameData->mxDropTargetListener.is() )
            {
                SensorCall();Reference< XDragGestureRecognizer > xDragGestureRecognizer =
                    Reference< XDragGestureRecognizer > (mpWindowImpl->mpFrameData->mxDragSource, UNO_QUERY);
                SensorCall();if( xDragGestureRecognizer.is() )
                {
                    xDragGestureRecognizer->removeDragGestureListener(
                        Reference< XDragGestureListener > (mpWindowImpl->mpFrameData->mxDropTargetListener, UNO_QUERY));
                }

                mpWindowImpl->mpFrameData->mxDropTarget->removeDropTargetListener( mpWindowImpl->mpFrameData->mxDropTargetListener );
                mpWindowImpl->mpFrameData->mxDropTargetListener.clear();
            }

            // shutdown drag and drop for this frame window
            SensorCall();Reference< XComponent > xComponent( mpWindowImpl->mpFrameData->mxDropTarget, UNO_QUERY );

            // DNDEventDispatcher does not hold a reference of the DropTarget,
            // so it's ok if it does not support XComponent
            if( xComponent.is() )
                xComponent->dispose();
        }
        catch (const Exception&)
        {
            // can be safely ignored here.
        }
    }

    SensorCall();UnoWrapperBase* pWrapper = Application::GetUnoWrapper( false );
    SensorCall();if ( pWrapper )
        {/*7*/SensorCall();pWrapper->WindowDestroyed( this );/*8*/}

    // MT: Must be called after WindowDestroyed!
    // Otherwise, if the accessible is a VCLXWindow, it will try to destroy this window again!
    // But accessibility implementations from applications need this dispose.
    SensorCall();if ( mpWindowImpl->mxAccessible.is() )
    {
        SensorCall();Reference< XComponent> xC( mpWindowImpl->mxAccessible, UNO_QUERY );
        if ( xC.is() )
            xC->dispose();
    }

    SensorCall();ImplSVData* pSVData = ImplGetSVData();

    SensorCall();if ( pSVData->maHelpData.mpHelpWin && (pSVData->maHelpData.mpHelpWin->GetParent() == this) )
        {/*9*/SensorCall();ImplDestroyHelpWindow( true );/*10*/}

    DBG_ASSERT( pSVData->maWinData.mpTrackWin.get() != this,
                "Window::~Window(): Window is in TrackingMode" );
    DBG_ASSERT( pSVData->maWinData.mpCaptureWin.get() != this,
                "Window::~Window(): Window has the mouse captured" );

    // due to old compatibility
    SensorCall();if ( pSVData->maWinData.mpTrackWin == this )
        {/*11*/SensorCall();EndTracking();/*12*/}
    SensorCall();if ( pSVData->maWinData.mpCaptureWin == this )
        {/*13*/SensorCall();ReleaseMouse();/*14*/}
    if ( pSVData->maWinData.mpDefDialogParent == this )
        pSVData->maWinData.mpDefDialogParent = NULL;

#if OSL_DEBUG_LEVEL > 0
    if ( true ) // always perform these tests in debug builds
    {
        OStringBuffer aErrorStr;
        bool        bError = false;
        vcl::Window*     pTempWin;

        if ( mpWindowImpl->mpFirstChild )
        {
            OStringBuffer aTempStr("Window (");
            aTempStr.append(lcl_createWindowInfo(*this));
            aTempStr.append(") with live children destroyed: ");
            pTempWin = mpWindowImpl->mpFirstChild;
            while ( pTempWin )
            {
                aTempStr.append(lcl_createWindowInfo(*pTempWin));
                pTempWin = pTempWin->mpWindowImpl->mpNext;
            }
            OSL_FAIL( aTempStr.getStr() );
            Application::Abort(OStringToOUString(aTempStr.makeStringAndClear(), RTL_TEXTENCODING_UTF8));   // abort in debug builds, this must be fixed!
        }

        if (mpWindowImpl->mpFrameData != 0)
        {
            pTempWin = mpWindowImpl->mpFrameData->mpFirstOverlap;
            while ( pTempWin )
            {
                if ( ImplIsRealParentPath( pTempWin ) )
                {
                    bError = true;
                    aErrorStr.append(lcl_createWindowInfo(*pTempWin));
                }
                pTempWin = pTempWin->mpWindowImpl->mpNextOverlap;
            }
            if ( bError )
            {
                OStringBuffer aTempStr;
                aTempStr.append("Window (");
                aTempStr.append(lcl_createWindowInfo(*this));
                aTempStr.append(") with live SystemWindows destroyed: ");
                aTempStr.append(aErrorStr.toString());
                OSL_FAIL(aTempStr.getStr());
                // abort in debug builds, must be fixed!
                Application::Abort(OStringToOUString(
                                     aTempStr.makeStringAndClear(), RTL_TEXTENCODING_UTF8));
            }
        }

        bError = false;
        pTempWin = pSVData->maWinData.mpFirstFrame;
        while ( pTempWin )
        {
            if ( ImplIsRealParentPath( pTempWin ) )
            {
                bError = true;
                aErrorStr.append(lcl_createWindowInfo(*pTempWin));
            }
            pTempWin = pTempWin->mpWindowImpl->mpFrameData->mpNextFrame;
        }
        if ( bError )
        {
            OStringBuffer aTempStr( "Window (" );
            aTempStr.append(lcl_createWindowInfo(*this));
            aTempStr.append(") with live SystemWindows destroyed: ");
            aTempStr.append(aErrorStr.toString());
            OSL_FAIL( aTempStr.getStr() );
            Application::Abort(OStringToOUString(aTempStr.makeStringAndClear(), RTL_TEXTENCODING_UTF8));   // abort in debug builds, this must be fixed!
        }

        if ( mpWindowImpl->mpFirstOverlap )
        {
            OStringBuffer aTempStr("Window (");
            aTempStr.append(lcl_createWindowInfo(*this));
            aTempStr.append(") with live SystemWindows destroyed: ");
            pTempWin = mpWindowImpl->mpFirstOverlap;
            while ( pTempWin )
            {
                aTempStr.append(lcl_createWindowInfo(*pTempWin));
                pTempWin = pTempWin->mpWindowImpl->mpNext;
            }
            OSL_FAIL( aTempStr.getStr() );
            Application::Abort(OStringToOUString(aTempStr.makeStringAndClear(), RTL_TEXTENCODING_UTF8));   // abort in debug builds, this must be fixed!
        }

        vcl::Window* pMyParent = GetParent();
        SystemWindow* pMySysWin = NULL;

        while ( pMyParent )
        {
            if ( pMyParent->IsSystemWindow() )
            {
                pMySysWin = dynamic_cast<SystemWindow *>(pMyParent);
            }
            pMyParent = pMyParent->GetParent();
        }
        if ( pMySysWin && pMySysWin->ImplIsInTaskPaneList( this ) )
        {
            OStringBuffer aTempStr("Window (");
            aTempStr.append(lcl_createWindowInfo(*this));
            aTempStr.append(") still in TaskPanelList!");
            OSL_FAIL( aTempStr.getStr() );
            Application::Abort(OStringToOUString(aTempStr.makeStringAndClear(), RTL_TEXTENCODING_UTF8));   // abort in debug builds, this must be fixed!
        }
    }
#endif

    SensorCall();if( mpWindowImpl->mbIsInTaskPaneList )
    {
        SensorCall();vcl::Window* pMyParent = GetParent();
        SystemWindow* pMySysWin = NULL;

        SensorCall();while ( pMyParent )
        {
            SensorCall();if ( pMyParent->IsSystemWindow() )
            {
                SensorCall();pMySysWin = dynamic_cast<SystemWindow *>(pMyParent);
            }
            SensorCall();pMyParent = pMyParent->GetParent();
        }
        SensorCall();if ( pMySysWin && pMySysWin->ImplIsInTaskPaneList( this ) )
        {
            SensorCall();pMySysWin->GetTaskPaneList()->RemoveWindow( this );
        }
        else
        {
            SensorCall();OStringBuffer aTempStr("Window (");
            aTempStr.append(OUStringToOString(GetText(), RTL_TEXTENCODING_UTF8));
            aTempStr.append(") not found in TaskPanelList!");
            OSL_FAIL( aTempStr.getStr() );
        }
    }

    // remove from size-group if necessary
    SensorCall();remove_from_all_size_groups();

    // clear mnemonic labels
    std::vector<VclPtr<FixedText> > aMnemonicLabels(list_mnemonic_labels());
    for (auto aI = aMnemonicLabels.begin(); aI != aMnemonicLabels.end(); ++aI)
    {
        remove_mnemonic_label(*aI);
    }

    // hide window in order to trigger the Paint-Handling
    Hide();

    // announce the window is to be destroyed
    {
        NotifyEvent aNEvt( MouseNotifyEvent::DESTROY, this );
        CompatNotify( aNEvt );
    }

    // EndExtTextInputMode
    SensorCall();if ( pSVData->maWinData.mpExtTextInputWin == this )
    {
        SensorCall();EndExtTextInput( EXTTEXTINPUT_END_COMPLETE );
        if ( pSVData->maWinData.mpExtTextInputWin == this )
            pSVData->maWinData.mpExtTextInputWin = NULL;
    }

    // check if the focus window is our child
    SensorCall();bool bHasFocussedChild = false;
    SensorCall();if( pSVData->maWinData.mpFocusWin && ImplIsRealParentPath( pSVData->maWinData.mpFocusWin ) )
    {
        // #122232#, this must not happen and is an application bug ! but we try some cleanup to hopefully avoid crashes, see below
        SensorCall();bHasFocussedChild = true;
#if OSL_DEBUG_LEVEL > 0
        OStringBuffer aTempStr("Window (");
        aTempStr.append(OUStringToOString(GetText(),
            RTL_TEXTENCODING_UTF8)).
                append(") with focussed child window destroyed ! THIS WILL LEAD TO CRASHES AND MUST BE FIXED !");
        OSL_FAIL( aTempStr.getStr() );
        Application::Abort(OStringToOUString(aTempStr.makeStringAndClear(), RTL_TEXTENCODING_UTF8 ));   // abort in debug build version, this must be fixed!
#endif
    }

    // if we get focus pass focus to another window
    SensorCall();vcl::Window* pOverlapWindow = ImplGetFirstOverlapWindow();
    SensorCall();if ( pSVData->maWinData.mpFocusWin == this
        || bHasFocussedChild )  // #122232#, see above, try some cleanup
    {
        SensorCall();if ( mpWindowImpl->mbFrame )
        {
            pSVData->maWinData.mpFocusWin = NULL;
            pOverlapWindow->mpWindowImpl->mpLastFocusWindow = NULL;
        }
        else
        {
            SensorCall();vcl::Window* pParent = GetParent();
            vcl::Window* pBorderWindow = mpWindowImpl->mpBorderWindow;
        // when windows overlap, give focus to the parent
        // of the next FrameWindow
            SensorCall();if ( pBorderWindow )
            {
                if ( pBorderWindow->ImplIsOverlapWindow() )
                    pParent = pBorderWindow->mpWindowImpl->mpOverlapWindow;
            }
            else {/*15*/if ( ImplIsOverlapWindow() )
                pParent = mpWindowImpl->mpOverlapWindow;/*16*/}

            SensorCall();if ( pParent && pParent->IsEnabled() && pParent->IsInputEnabled() && ! pParent->IsInModalMode() )
                {/*17*/SensorCall();pParent->GrabFocus();/*18*/}
            else
                {/*19*/mpWindowImpl->mpFrameWindow->GrabFocus();/*20*/}

            // If the focus was set back to 'this' set it to nothing
            SensorCall();if ( pSVData->maWinData.mpFocusWin == this )
            {
                pSVData->maWinData.mpFocusWin = NULL;
                pOverlapWindow->mpWindowImpl->mpLastFocusWindow = NULL;
            }
        }
    }

    if ( pOverlapWindow != 0 &&
         pOverlapWindow->mpWindowImpl->mpLastFocusWindow == this )
        pOverlapWindow->mpWindowImpl->mpLastFocusWindow = NULL;

    // reset hint for DefModalDialogParent
    if( pSVData->maWinData.mpActiveApplicationFrame == this )
        pSVData->maWinData.mpActiveApplicationFrame = NULL;

    // reset marked windows
    SensorCall();if ( mpWindowImpl->mpFrameData != 0 )
    {
        if ( mpWindowImpl->mpFrameData->mpFocusWin == this )
            mpWindowImpl->mpFrameData->mpFocusWin = NULL;
        if ( mpWindowImpl->mpFrameData->mpMouseMoveWin == this )
            mpWindowImpl->mpFrameData->mpMouseMoveWin = NULL;
        if ( mpWindowImpl->mpFrameData->mpMouseDownWin == this )
            mpWindowImpl->mpFrameData->mpMouseDownWin = NULL;
    }

    // reset Deactivate-Window
    if ( pSVData->maWinData.mpLastDeacWin == this )
        pSVData->maWinData.mpLastDeacWin = NULL;

    SensorCall();if ( mpWindowImpl->mpFrameData )
    {
        SensorCall();if ( mpWindowImpl->mpFrameData->mnFocusId )
            {/*21*/SensorCall();Application::RemoveUserEvent( mpWindowImpl->mpFrameData->mnFocusId );/*22*/}
        SensorCall();mpWindowImpl->mpFrameData->mnFocusId = NULL;
        SensorCall();if ( mpWindowImpl->mpFrameData->mnMouseMoveId )
            {/*23*/SensorCall();Application::RemoveUserEvent( mpWindowImpl->mpFrameData->mnMouseMoveId );/*24*/}
        SensorCall();mpWindowImpl->mpFrameData->mnMouseMoveId = NULL;
    }

    // release SalGraphics
    SensorCall();OutputDevice *pOutDev = GetOutDev();
    pOutDev->ReleaseGraphics();

    // notify ImplDelData subscribers of this window about the window deletion
    ImplDelData* pDelData = mpWindowImpl->mpFirstDel;
    SensorCall();while ( pDelData )
    {
        SensorCall();pDelData->mbDel = true;
        pDelData->mpWindow.clear();  // #112873# pDel is not associated with a Window anymore
        pDelData = pDelData->mpNext;
    }

    // remove window from the lists
    SensorCall();ImplRemoveWindow( true );

    // de-register as "top window child" at our parent, if necessary
    SensorCall();if ( mpWindowImpl->mbFrame )
    {
        SensorCall();bool bIsTopWindow = mpWindowImpl->mpWinData && ( mpWindowImpl->mpWinData->mnIsTopWindow == 1 );
        SensorCall();if ( mpWindowImpl->mpRealParent && bIsTopWindow )
        {
            SensorCall();ImplWinData* pParentWinData = mpWindowImpl->mpRealParent->ImplGetWinData();

            auto myPos = ::std::find( pParentWinData->maTopWindowChildren.begin(),
                pParentWinData->maTopWindowChildren.end(), VclPtr<vcl::Window>(this) );
            DBG_ASSERT( myPos != pParentWinData->maTopWindowChildren.end(), "Window::~Window: inconsistency in top window chain!" );
            if ( myPos != pParentWinData->maTopWindowChildren.end() )
                pParentWinData->maTopWindowChildren.erase( myPos );
        }
    }

    // cleanup Extra Window Data, TODO: add and use ImplWinData destructor
    SensorCall();if ( mpWindowImpl->mpWinData )
    {
        SensorCall();if ( mpWindowImpl->mpWinData->mpExtOldText )
            {/*25*/SensorCall();delete mpWindowImpl->mpWinData->mpExtOldText;/*26*/}
        SensorCall();if ( mpWindowImpl->mpWinData->mpExtOldAttrAry )
            {/*27*/SensorCall();delete mpWindowImpl->mpWinData->mpExtOldAttrAry;/*28*/}
        SensorCall();if ( mpWindowImpl->mpWinData->mpCursorRect )
            {/*29*/SensorCall();delete mpWindowImpl->mpWinData->mpCursorRect;/*30*/}
        SensorCall();if ( mpWindowImpl->mpWinData->mpCompositionCharRects)
            {/*31*/SensorCall();delete[] mpWindowImpl->mpWinData->mpCompositionCharRects;/*32*/}
        SensorCall();if ( mpWindowImpl->mpWinData->mpFocusRect )
            {/*33*/SensorCall();delete mpWindowImpl->mpWinData->mpFocusRect;/*34*/}
        SensorCall();if ( mpWindowImpl->mpWinData->mpTrackRect )
            {/*35*/SensorCall();delete mpWindowImpl->mpWinData->mpTrackRect;/*36*/}

        SensorCall();delete mpWindowImpl->mpWinData;
    }

    // cleanup overlap related window data
    SensorCall();if ( mpWindowImpl->mpOverlapData )
        {/*37*/SensorCall();delete mpWindowImpl->mpOverlapData;/*38*/}

    // remove BorderWindow or Frame window data
    mpWindowImpl->mpBorderWindow.disposeAndClear();
    SensorCall();if ( mpWindowImpl->mbFrame )
    {
        SensorCall();if ( pSVData->maWinData.mpFirstFrame == this )
            {/*39*/pSVData->maWinData.mpFirstFrame = mpWindowImpl->mpFrameData->mpNextFrame;/*40*/}
        else
        {
            SensorCall();vcl::Window* pSysWin = pSVData->maWinData.mpFirstFrame;
            while ( pSysWin->mpWindowImpl->mpFrameData->mpNextFrame.get() != this )
                pSysWin = pSysWin->mpWindowImpl->mpFrameData->mpNextFrame;

            assert (mpWindowImpl->mpFrameData->mpNextFrame.get() != pSysWin);
            pSysWin->mpWindowImpl->mpFrameData->mpNextFrame = mpWindowImpl->mpFrameData->mpNextFrame;
        }
        SensorCall();mpWindowImpl->mpFrame->SetCallback( NULL, NULL );
        pSVData->mpDefInst->DestroyFrame( mpWindowImpl->mpFrame );
        assert (mpWindowImpl->mpFrameData->mnFocusId == NULL);
        assert (mpWindowImpl->mpFrameData->mnMouseMoveId == NULL);
        delete mpWindowImpl->mpFrameData;
    }

    // should be the last statements
    SensorCall();delete mpWindowImpl; mpWindowImpl = NULL;

    OutputDevice::dispose();
SensorCall();}

Window::~Window()
{
    // FIXME: we should kill all LazyDeletor usage.
    SensorCall();vcl::LazyDeletor::Undelete( this );
    disposeOnce();
SensorCall();}

// We will eventually being removing the inheritance of OutputDevice
// from Window. It will be replaced with a transient relationship such
// that the OutputDevice is only live for the scope of the Paint method.
// In the meantime this can help move us towards a Window use an
// OutputDevice, not being one.

::OutputDevice const* Window::GetOutDev() const
{
SensorCall();    return this;
SensorCall();}

::OutputDevice* Window::GetOutDev()
{
SensorCall();    return this;
SensorCall();}

} /* namespace vcl */

WindowImpl::WindowImpl( WindowType nType )
{
    SensorCall();maZoom                              = Fraction( 1, 1 );
    maWinRegion                         = vcl::Region(true);
    maWinClipRegion                     = vcl::Region(true);
    mpWinData                           = NULL;                      // Extra Window Data, that we dont need for all windows
    mpOverlapData                       = NULL;                      // Overlap Data
    mpFrameData                         = NULL;                      // Frame Data
    mpFrame                             = NULL;                      // Pointer to frame window
    mpSysObj                            = NULL;
    mpFrameWindow                       = NULL;                      // window to top level parent (same as frame window)
    mpOverlapWindow                     = NULL;                      // first overlap parent
    mpBorderWindow                      = NULL;                      // Border-Window
    mpClientWindow                      = NULL;                      // Client-Window of a FrameWindow
    mpParent                            = NULL;                      // parent (incl. BorderWindow)
    mpRealParent                        = NULL;                      // real parent (excl. BorderWindow)
    mpFirstChild                        = NULL;                      // first child window
    mpLastChild                         = NULL;                      // last child window
    mpFirstOverlap                      = NULL;                      // first overlap window (only set in overlap windows)
    mpLastOverlap                       = NULL;                      // last overlap window (only set in overlap windows)
    mpPrev                              = NULL;                      // prev window
    mpNext                              = NULL;                      // next window
    mpNextOverlap                       = NULL;                      // next overlap window of frame
    mpLastFocusWindow                   = NULL;                      // window for focus restore
    mpDlgCtrlDownWindow                 = NULL;                      // window for dialog control
    mpFirstDel                          = NULL;                      // Dtor notification list
    mnEventListenersIteratingCount = 0;
    mpUserData                          = NULL;                      // user data
    mpCursor                            = NULL;                      // cursor
    mpControlFont                       = NULL;                      // font properties
    mpVCLXWindow                        = NULL;
    mpAccessibleInfos                   = NULL;
    maControlForeground                 = Color( COL_TRANSPARENT );  // no foreground set
    maControlBackground                 = Color( COL_TRANSPARENT );  // no background set
    mnLeftBorder                        = 0;                         // left border
    mnTopBorder                         = 0;                         // top border
    mnRightBorder                       = 0;                         // right border
    mnBottomBorder                      = 0;                         // bottom border
    mnWidthRequest                      = -1;                        // width request
    mnHeightRequest                     = -1;                        // height request
    mnOptimalWidthCache                 = -1;                        // optimal width cache
    mnOptimalHeightCache                = -1;                        // optimal height cache
    mnX                                 = 0;                         // X-Position to Parent
    mnY                                 = 0;                         // Y-Position to Parent
    mnAbsScreenX                        = 0;                         // absolute X-position on screen, used for RTL window positioning
    mpChildClipRegion                   = NULL;                      // Child-Clip-Region when ClipChildren
    mpPaintRegion                       = NULL;                      // Paint-ClipRegion
    mnStyle                             = 0;                         // style (init in ImplInitWindow)
    mnPrevStyle                         = 0;                         // prevstyle (set in SetStyle)
    mnExtendedStyle                     = 0;                         // extended style (init in ImplInitWindow)
    mnPrevExtendedStyle                 = 0;                         // prevstyle (set in SetExtendedStyle)
    mnType                              = nType;                     // type
    mnGetFocusFlags                     = 0;                         // Flags fuer GetFocus()-Aufruf
    mnWaitCount                         = 0;                         // Wait-Count (>1 == Warte-MousePointer)
    mnPaintFlags                        = 0;                         // Flags for ImplCallPaint
    mnParentClipMode                    = 0;                         // Flags for Parent-ClipChildren-Mode
    mnActivateMode                      = 0;                         // Will be converted in System/Overlap-Windows
    mnDlgCtrlFlags                      = 0;                         // DialogControl-Flags
    mnLockCount                         = 0;                         // LockCount
    meAlwaysInputMode                   = AlwaysInputNone;           // neither AlwaysEnableInput nor AlwaysDisableInput called
    meHalign                            = VCL_ALIGN_FILL;
    meValign                            = VCL_ALIGN_FILL;
    mePackType                          = VCL_PACK_START;
    mnPadding                           = 0;
    mnGridHeight                        = 1;
    mnGridLeftAttach                    = -1;
    mnGridTopAttach                     = -1;
    mnGridWidth                         = 1;
    mnBorderWidth                       = 0;
    mnMarginLeft                        = 0;
    mnMarginRight                       = 0;
    mnMarginTop                         = 0;
    mnMarginBottom                      = 0;
    mbFrame                             = false;                     // true: Window is a frame window
    mbBorderWin                         = false;                     // true: Window is a border window
    mbOverlapWin                        = false;                     // true: Window is a overlap window
    mbSysWin                            = false;                     // true: SystemWindow is the base class
    mbDialog                            = false;                     // true: Dialog is the base class
    mbDockWin                           = false;                     // true: DockingWindow is the base class
    mbFloatWin                          = false;                     // true: FloatingWindow is the base class
    mbPushButton                        = false;                     // true: PushButton is the base class
    mbToolBox                           = false;                     // true: ToolBox is the base class
    mbMenuFloatingWindow                = false;                     // true: MenuFloatingWindow is the base class
    mbToolbarFloatingWindow             = false;                     // true: ImplPopupFloatWin is the base class, used for subtoolbars
    mbSplitter                          = false;                     // true: Splitter is the base class
    mbVisible                           = false;                     // true: Show( true ) called
    mbOverlapVisible                    = false;                     // true: Hide called for visible window from ImplHideAllOverlapWindow()
    mbDisabled                          = false;                     // true: Enable( false ) called
    mbInputDisabled                     = false;                     // true: EnableInput( false ) called
    mbDropDisabled                      = false;                     // true: Drop is enabled
    mbNoUpdate                          = false;                     // true: SetUpdateMode( false ) called
    mbNoParentUpdate                    = false;                     // true: SetParentUpdateMode( false ) called
    mbActive                            = false;                     // true: Window Active
    mbParentActive                      = false;                     // true: OverlapActive from Parent
    mbReallyVisible                     = false;                     // true: this and all parents to an overlapped window are visible
    mbReallyShown                       = false;                     // true: this and all parents to an overlapped window are shown
    mbInInitShow                        = false;                     // true: we are in InitShow
    mbChildNotify                       = false;                     // true: ChildNotify
    mbChildPtrOverwrite                 = false;                     // true: PointerStyle overwrites Child-Pointer
    mbNoPtrVisible                      = false;                     // true: ShowPointer( false ) called
    mbMouseMove                         = false;                     // true: BaseMouseMove called
    mbPaintFrame                        = false;                     // true: Paint is visible, but not painted
    mbInPaint                           = false;                     // true: Inside PaintHdl
    mbMouseButtonDown                   = false;                     // true: BaseMouseButtonDown called
    mbMouseButtonUp                     = false;                     // true: BaseMouseButtonUp called
    mbKeyInput                          = false;                     // true: BaseKeyInput called
    mbKeyUp                             = false;                     // true: BaseKeyUp called
    mbCommand                           = false;                     // true: BaseCommand called
    mbDefPos                            = true;                      // true: Position is not Set
    mbDefSize                           = true;                      // true: Size is not Set
    mbCallMove                          = true;                      // true: Move must be called by Show
    mbCallResize                        = true;                      // true: Resize must be called by Show
    mbWaitSystemResize                  = true;                      // true: Wait for System-Resize
    mbInitWinClipRegion                 = true;                      // true: Calc Window Clip Region
    mbInitChildRegion                   = false;                     // true: InitChildClipRegion
    mbWinRegion                         = false;                     // true: Window Region
    mbClipChildren                      = false;                     // true: Child-window should be clipped
    mbClipSiblings                      = false;                     // true: Adjacent Child-window should be clipped
    mbChildTransparent                  = false;                     // true: Child-windows are allowed to switch to transparent (incl. Parent-CLIPCHILDREN)
    mbPaintTransparent                  = false;                     // true: Paints should be executed on the Parent
    mbMouseTransparent                  = false;                     // true: Window is transparent for Mouse
    mbDlgCtrlStart                      = false;                     // true: From here on own Dialog-Control
    mbFocusVisible                      = false;                     // true: Focus Visible
    mbUseNativeFocus                    = false;
    mbNativeFocusVisible                = false;                     // true: native Focus Visible
    mbInShowFocus                       = false;                     // prevent recursion
    mbInHideFocus                       = false;                     // prevent recursion
    mbTrackVisible                      = false;                     // true: Tracking Visible
    mbControlForeground                 = false;                     // true: Foreground-Property set
    mbControlBackground                 = false;                     // true: Background-Property set
    mbAlwaysOnTop                       = false;                     // true: always visible for all others windows
    mbCompoundControl                   = false;                     // true: Composite Control => Listener...
    mbCompoundControlHasFocus           = false;                     // true: Composite Control has focus somewhere
    mbPaintDisabled                     = false;                     // true: Paint should not be executed
    mbAllResize                         = false;                     // true: Also sent ResizeEvents with 0,0
    mbInDispose                         = false;                     // true: We're still in Window::dispose()
    mbExtTextInput                      = false;                     // true: ExtTextInput-Mode is active
    mbInFocusHdl                        = false;                     // true: Within GetFocus-Handler
    mbCreatedWithToolkit                = false;
    mbSuppressAccessibilityEvents       = false;                     // true: do not send any accessibility events
    mbDrawSelectionBackground           = false;                     // true: draws transparent window background to indicate (toolbox) selection
    mbIsInTaskPaneList                  = false;                     // true: window was added to the taskpanelist in the topmost system window
    mnNativeBackground                  = 0;                         // initialize later, depends on type
    mbCallHandlersDuringInputDisabled   = false;                     // true: call event handlers even if input is disabled
    mbHelpTextDynamic                   = false;                     // true: append help id in HELP_DEBUG case
    mbFakeFocusSet                      = false;                     // true: pretend as if the window has focus.
    mbHexpand                           = false;
    mbVexpand                           = false;
    mbExpand                            = false;
    mbFill                              = true;
    mbSecondary                         = false;
    mbNonHomogeneous                    = false;
    mbDoubleBuffering                   = getenv("VCL_DOUBLEBUFFERING_FORCE_ENABLE"); // when we are not sure, assume it cannot do double-buffering via RenderContext
SensorCall();}

WindowImpl::~WindowImpl()
{
    SensorCall();delete mpChildClipRegion;
    delete mpAccessibleInfos;
    delete mpControlFont;
SensorCall();}

namespace vcl {

bool Window::AcquireGraphics() const
{
SensorCall();    DBG_TESTSOLARMUTEX();

    SensorCall();if ( mpGraphics )
        {/*41*/{const _Bool  ReplaceReturn = true; SensorCall(); return ReplaceReturn;}/*42*/}

    SensorCall();mbInitLineColor     = true;
    mbInitFillColor     = true;
    mbInitFont          = true;
    mbInitTextColor     = true;
    mbInitClipRegion    = true;

    ImplSVData* pSVData = ImplGetSVData();

    mpGraphics = mpWindowImpl->mpFrame->AcquireGraphics();
    // try harder if no wingraphics was available directly
    SensorCall();if ( !mpGraphics )
    {
        // find another output device in the same frame
        SensorCall();OutputDevice* pReleaseOutDev = pSVData->maGDIData.mpLastWinGraphics;
        SensorCall();while ( pReleaseOutDev )
        {
            SensorCall();if ( static_cast<vcl::Window*>(pReleaseOutDev)->mpWindowImpl->mpFrame == mpWindowImpl->mpFrame )
                {/*43*/SensorCall();break;/*44*/}
            pReleaseOutDev = pReleaseOutDev->mpPrevGraphics;
        }

        SensorCall();if ( pReleaseOutDev )
        {
            // steal the wingraphics from the other outdev
            SensorCall();mpGraphics = pReleaseOutDev->mpGraphics;
            pReleaseOutDev->ReleaseGraphics( false );
        }
        else
        {
            // if needed retry after releasing least recently used wingraphics
            SensorCall();while ( !mpGraphics )
            {
                SensorCall();if ( !pSVData->maGDIData.mpLastWinGraphics )
                    {/*45*/SensorCall();break;/*46*/}
                pSVData->maGDIData.mpLastWinGraphics->ReleaseGraphics();
                SensorCall();mpGraphics = mpWindowImpl->mpFrame->AcquireGraphics();
            }
        }
    }

    // update global LRU list of wingraphics
    SensorCall();if ( mpGraphics )
    {
        mpNextGraphics = pSVData->maGDIData.mpFirstWinGraphics;
        pSVData->maGDIData.mpFirstWinGraphics = const_cast<vcl::Window*>(this);
        if ( mpNextGraphics )
            mpNextGraphics->mpPrevGraphics = const_cast<vcl::Window*>(this);
        if ( !pSVData->maGDIData.mpLastWinGraphics )
            pSVData->maGDIData.mpLastWinGraphics = const_cast<vcl::Window*>(this);
    }

    SensorCall();if ( mpGraphics )
    {
        SensorCall();mpGraphics->SetXORMode( (ROP_INVERT == meRasterOp) || (ROP_XOR == meRasterOp), ROP_INVERT == meRasterOp );
        mpGraphics->setAntiAliasB2DDraw(bool(mnAntialiasing & AntialiasingFlags::EnableB2dDraw));
    }

    return mpGraphics != nullptr;
SensorCall();}

void Window::ReleaseGraphics( bool bRelease )
{
SensorCall();    DBG_TESTSOLARMUTEX();

    SensorCall();if ( !mpGraphics )
        {/*47*/SensorCall();return;/*48*/}

    // release the fonts of the physically released graphics device
    if( bRelease )
        ImplReleaseFonts();

    SensorCall();ImplSVData* pSVData = ImplGetSVData();

    vcl::Window* pWindow = (vcl::Window*)this;

    SensorCall();if ( bRelease )
        {/*49*/SensorCall();pWindow->mpWindowImpl->mpFrame->ReleaseGraphics( mpGraphics );/*50*/}
    // remove from global LRU list of window graphics
    if ( mpPrevGraphics )
        mpPrevGraphics->mpNextGraphics = mpNextGraphics;
    else
        pSVData->maGDIData.mpFirstWinGraphics = mpNextGraphics;
    if ( mpNextGraphics )
        mpNextGraphics->mpPrevGraphics = mpPrevGraphics;
    else
        pSVData->maGDIData.mpLastWinGraphics = mpPrevGraphics;

    SensorCall();mpGraphics      = NULL;
    mpPrevGraphics  = NULL;
    mpNextGraphics  = NULL;
SensorCall();}

static sal_Int32 CountDPIScaleFactor(sal_Int32 nDPI)
{
    SensorCall();sal_Int32 nResult = 1;

#ifndef MACOSX
    // Setting of HiDPI is unfortunately all only a heuristic; and to add
    // insult to an injury, the system is constantly lying to us about
    // the DPI and whatnot
    // eg. fdo#77059 - set the value from which we do consider the
    // screen hi-dpi to greater than 168
    if (nDPI > 168)
        nResult = std::max(sal_Int32(1), (nDPI + 48) / 96);
#else
    (void)nDPI;
#endif

    {sal_Int32  ReplaceReturn = nResult; SensorCall(); return ReplaceReturn;}
}

void Window::ImplInit( vcl::Window* pParent, WinBits nStyle, SystemParentData* pSystemParentData )
{
SensorCall();    DBG_ASSERT( mpWindowImpl->mbFrame || pParent || GetType() == WINDOW_FIXEDIMAGE,
        "Window::Window(): pParent == NULL" );

    ImplSVData* pSVData = ImplGetSVData();
    vcl::Window*     pRealParent = pParent;

    // inherit 3D look
    SensorCall();if ( !mpWindowImpl->mbOverlapWin && pParent && (pParent->GetStyle() & WB_3DLOOK) )
        {/*51*/SensorCall();nStyle |= WB_3DLOOK;/*52*/}

    // create border window if necessary
    SensorCall();if ( !mpWindowImpl->mbFrame && !mpWindowImpl->mbBorderWin && !mpWindowImpl->mpBorderWindow
         && (nStyle & (WB_BORDER | WB_SYSTEMCHILDWINDOW) ) )
    {
        SensorCall();sal_uInt16 nBorderTypeStyle = 0;
        SensorCall();if( (nStyle & WB_SYSTEMCHILDWINDOW) )
        {
            // handle WB_SYSTEMCHILDWINDOW
            // these should be analogous to a top level frame; meaning they
            // should have a border window with style BORDERWINDOW_STYLE_FRAME
            // which controls their size
            SensorCall();nBorderTypeStyle |= BORDERWINDOW_STYLE_FRAME;
            nStyle |= WB_BORDER;
        }
        SensorCall();VclPtrInstance<ImplBorderWindow> pBorderWin( pParent, nStyle & (WB_BORDER | WB_DIALOGCONTROL | WB_NODIALOGCONTROL), nBorderTypeStyle );
        ((vcl::Window*)pBorderWin)->mpWindowImpl->mpClientWindow = this;
        pBorderWin->GetBorder( mpWindowImpl->mnLeftBorder, mpWindowImpl->mnTopBorder, mpWindowImpl->mnRightBorder, mpWindowImpl->mnBottomBorder );
        mpWindowImpl->mpBorderWindow  = pBorderWin;
        pParent = mpWindowImpl->mpBorderWindow;
    }
    else {/*53*/SensorCall();if( !mpWindowImpl->mbFrame && ! pParent )
    {
        SensorCall();mpWindowImpl->mbOverlapWin  = true;
        mpWindowImpl->mbFrame = true;
    ;/*54*/}}

    // insert window in list
    SensorCall();ImplInsertWindow( pParent );
    mpWindowImpl->mnStyle = nStyle;

    // Overlap-Window-Data
    SensorCall();if ( mpWindowImpl->mbOverlapWin )
    {
        SensorCall();mpWindowImpl->mpOverlapData                   = new ImplOverlapData;
        mpWindowImpl->mpOverlapData->mpSaveBackDev    = NULL;
        mpWindowImpl->mpOverlapData->mpSaveBackRgn    = NULL;
        mpWindowImpl->mpOverlapData->mpNextBackWin    = NULL;
        mpWindowImpl->mpOverlapData->mnSaveBackSize   = 0;
        mpWindowImpl->mpOverlapData->mbSaveBack       = false;
        mpWindowImpl->mpOverlapData->mnTopLevel       = 1;
    }

    SensorCall();if( pParent && ! mpWindowImpl->mbFrame )
        {/*55*/SensorCall();mbEnableRTL = AllSettings::GetLayoutRTL();/*56*/}

    // test for frame creation
    SensorCall();if ( mpWindowImpl->mbFrame )
    {
        // create frame
        SensorCall();sal_uLong nFrameStyle = 0;

        SensorCall();if ( nStyle & WB_MOVEABLE )
            nFrameStyle |= SAL_FRAME_STYLE_MOVEABLE;
        SensorCall();if ( nStyle & WB_SIZEABLE )
            nFrameStyle |= SAL_FRAME_STYLE_SIZEABLE;
        SensorCall();if ( nStyle & WB_CLOSEABLE )
            nFrameStyle |= SAL_FRAME_STYLE_CLOSEABLE;
        SensorCall();if ( nStyle & WB_APP )
            nFrameStyle |= SAL_FRAME_STYLE_DEFAULT;
        // check for undecorated floating window
        SensorCall();if( // 1. floating windows that are not moveable/sizeable (only closeable allowed)
            ( !(nFrameStyle & ~SAL_FRAME_STYLE_CLOSEABLE) &&
            ( mpWindowImpl->mbFloatWin || ((GetType() == WINDOW_BORDERWINDOW) && static_cast<ImplBorderWindow*>(this)->mbFloatWindow) || (nStyle & WB_SYSTEMFLOATWIN) ) ) ||
            // 2. borderwindows of floaters with ownerdraw decoration
            ( ((GetType() == WINDOW_BORDERWINDOW) && static_cast<ImplBorderWindow*>(this)->mbFloatWindow && (nStyle & WB_OWNERDRAWDECORATION) ) ) )
        {
            SensorCall();nFrameStyle = SAL_FRAME_STYLE_FLOAT;
            SensorCall();if( nStyle & WB_OWNERDRAWDECORATION )
                {/*57*/SensorCall();nFrameStyle |= (SAL_FRAME_STYLE_OWNERDRAWDECORATION | SAL_FRAME_STYLE_NOSHADOW);/*58*/}
        }
        else if( mpWindowImpl->mbFloatWin )
            nFrameStyle |= SAL_FRAME_STYLE_TOOLWINDOW;

        SensorCall();if( nStyle & WB_INTROWIN )
            nFrameStyle |= SAL_FRAME_STYLE_INTRO;
        SensorCall();if( nStyle & WB_TOOLTIPWIN )
            nFrameStyle |= SAL_FRAME_STYLE_TOOLTIP;

        SensorCall();if( nStyle & WB_NOSHADOW )
            nFrameStyle |= SAL_FRAME_STYLE_NOSHADOW;

        SensorCall();if( nStyle & WB_SYSTEMCHILDWINDOW )
            nFrameStyle |= SAL_FRAME_STYLE_SYSTEMCHILD;

        SensorCall();switch (mpWindowImpl->mnType)
        {
            case WINDOW_DIALOG:
            case WINDOW_TABDIALOG:
            case WINDOW_MODALDIALOG:
            case WINDOW_MODELESSDIALOG:
            case WINDOW_MESSBOX:
            case WINDOW_INFOBOX:
            case WINDOW_WARNINGBOX:
            case WINDOW_ERRORBOX:
            case WINDOW_QUERYBOX:
                SensorCall();nFrameStyle |= SAL_FRAME_STYLE_DIALOG;
            default:
                SensorCall();break;
        }

        SensorCall();SalFrame* pParentFrame = NULL;
        SensorCall();if ( pParent )
            {/*59*/SensorCall();pParentFrame = pParent->mpWindowImpl->mpFrame;/*60*/}
        SensorCall();SalFrame* pFrame;
        SensorCall();if ( pSystemParentData )
            {/*61*/SensorCall();pFrame = pSVData->mpDefInst->CreateChildFrame( pSystemParentData, nFrameStyle | SAL_FRAME_STYLE_PLUG );/*62*/}
        else
            {/*63*/SensorCall();pFrame = pSVData->mpDefInst->CreateFrame( pParentFrame, nFrameStyle );/*64*/}
        SensorCall();if ( !pFrame )
        {
            // do not abort but throw an exception, may be the current thread terminates anyway (plugin-scenario)
            throw RuntimeException(
                "Could not create system window!",
                Reference< XInterface >() );
        }

        SensorCall();pFrame->SetCallback( this, ImplWindowFrameProc );

        // set window frame data
        mpWindowImpl->mpFrameData     = new ImplFrameData;
        mpWindowImpl->mpFrame         = pFrame;
        mpWindowImpl->mpFrameWindow   = this;
        mpWindowImpl->mpOverlapWindow = this;

        // set frame data
        assert (pSVData->maWinData.mpFirstFrame.get() != this);
        mpWindowImpl->mpFrameData->mpNextFrame        = pSVData->maWinData.mpFirstFrame;
        pSVData->maWinData.mpFirstFrame = this;
        mpWindowImpl->mpFrameData->mpFirstOverlap     = NULL;
        mpWindowImpl->mpFrameData->mpFocusWin         = NULL;
        mpWindowImpl->mpFrameData->mpMouseMoveWin     = NULL;
        mpWindowImpl->mpFrameData->mpMouseDownWin     = NULL;
        mpWindowImpl->mpFrameData->mpFirstBackWin     = NULL;
        mpWindowImpl->mpFrameData->mpFontCollection   = pSVData->maGDIData.mpScreenFontList;
        mpWindowImpl->mpFrameData->mpFontCache        = pSVData->maGDIData.mpScreenFontCache;
        mpWindowImpl->mpFrameData->mnAllSaveBackSize  = 0;
        mpWindowImpl->mpFrameData->mnFocusId          = 0;
        mpWindowImpl->mpFrameData->mnMouseMoveId      = 0;
        mpWindowImpl->mpFrameData->mnLastMouseX       = -1;
        mpWindowImpl->mpFrameData->mnLastMouseY       = -1;
        mpWindowImpl->mpFrameData->mnBeforeLastMouseX = -1;
        mpWindowImpl->mpFrameData->mnBeforeLastMouseY = -1;
        mpWindowImpl->mpFrameData->mnFirstMouseX      = -1;
        mpWindowImpl->mpFrameData->mnFirstMouseY      = -1;
        mpWindowImpl->mpFrameData->mnLastMouseWinX    = -1;
        mpWindowImpl->mpFrameData->mnLastMouseWinY    = -1;
        mpWindowImpl->mpFrameData->mnModalMode        = 0;
        mpWindowImpl->mpFrameData->mnMouseDownTime    = 0;
        mpWindowImpl->mpFrameData->mnClickCount       = 0;
        mpWindowImpl->mpFrameData->mnFirstMouseCode   = 0;
        mpWindowImpl->mpFrameData->mnMouseCode        = 0;
        mpWindowImpl->mpFrameData->mnMouseMode        = MouseEventModifiers::NONE;
        mpWindowImpl->mpFrameData->meMapUnit          = MAP_PIXEL;
        mpWindowImpl->mpFrameData->mbHasFocus         = false;
        mpWindowImpl->mpFrameData->mbInMouseMove      = false;
        mpWindowImpl->mpFrameData->mbMouseIn          = false;
        mpWindowImpl->mpFrameData->mbStartDragCalled  = false;
        mpWindowImpl->mpFrameData->mbNeedSysWindow    = false;
        mpWindowImpl->mpFrameData->mbMinimized        = false;
        mpWindowImpl->mpFrameData->mbStartFocusState  = false;
        mpWindowImpl->mpFrameData->mbInSysObjFocusHdl = false;
        mpWindowImpl->mpFrameData->mbInSysObjToTopHdl = false;
        mpWindowImpl->mpFrameData->mbSysObjFocus      = false;
        SensorCall();if (!ImplDoTiledRendering())
        {
            mpWindowImpl->mpFrameData->maPaintIdle.SetPriority( SchedulerPriority::REPAINT );
            mpWindowImpl->mpFrameData->maPaintIdle.SetIdleHdl( LINK( this, Window, ImplHandlePaintHdl ) );
            mpWindowImpl->mpFrameData->maPaintIdle.SetDebugName( "vcl::Window maPaintIdle" );
        }
        mpWindowImpl->mpFrameData->maResizeIdle.SetPriority( SchedulerPriority::RESIZE );
        mpWindowImpl->mpFrameData->maResizeIdle.SetIdleHdl( LINK( this, Window, ImplHandleResizeTimerHdl ) );
        mpWindowImpl->mpFrameData->maResizeIdle.SetDebugName( "vcl::Window maResizeIdle" );
        SensorCall();mpWindowImpl->mpFrameData->mbInternalDragGestureRecognizer = false;
        if (!(nStyle & WB_DEFAULTWIN) && SupportsDoubleBuffering())
            mpWindowImpl->mpFrameData->mpBuffer = VclPtrInstance<VirtualDevice>();
        mpWindowImpl->mpFrameData->mbInBufferedPaint = false;

        SensorCall();if ( pRealParent && IsTopWindow() )
        {
            SensorCall();ImplWinData* pParentWinData = pRealParent->ImplGetWinData();
            pParentWinData->maTopWindowChildren.push_back( this );
        }
    }

    // init data
    mpWindowImpl->mpRealParent = pRealParent;

    // #99318: make sure fontcache and list is available before call to SetSettings
    SensorCall();mpFontCollection      = mpWindowImpl->mpFrameData->mpFontCollection;
    mpFontCache     = mpWindowImpl->mpFrameData->mpFontCache;

    SensorCall();if ( mpWindowImpl->mbFrame )
    {
        SensorCall();if ( pParent )
        {
            SensorCall();mpWindowImpl->mpFrameData->mnDPIX     = pParent->mpWindowImpl->mpFrameData->mnDPIX;
            mpWindowImpl->mpFrameData->mnDPIY     = pParent->mpWindowImpl->mpFrameData->mnDPIY;
        }
        else
        {
            SensorCall();OutputDevice *pOutDev = GetOutDev();
            SensorCall();if ( pOutDev->AcquireGraphics() )
            {
                SensorCall();mpGraphics->GetResolution( mpWindowImpl->mpFrameData->mnDPIX, mpWindowImpl->mpFrameData->mnDPIY );
            }
        }

        // add ownerdraw decorated frame windows to list in the top-most frame window
        // so they can be hidden on lose focus
        if( nStyle & WB_OWNERDRAWDECORATION )
            ImplGetOwnerDrawList().push_back( this );

        // delay settings initialization until first "real" frame
        // this relies on the IntroWindow not needing any system settings
        SensorCall();if ( !pSVData->maAppData.mbSettingsInit &&
             ! (nStyle & (WB_INTROWIN|WB_DEFAULTWIN))
             )
        {
            // side effect: ImplUpdateGlobalSettings does an ImplGetFrame()->UpdateSettings
            ImplUpdateGlobalSettings( *pSVData->maAppData.mpSettings );
            OutputDevice::SetSettings( *pSVData->maAppData.mpSettings );
            pSVData->maAppData.mbSettingsInit = true;
        }

        // If we create a Window with default size, query this
        // size directly, because we want resize all Controls to
        // the correct size before we display the window
        SensorCall();if ( nStyle & (WB_MOVEABLE | WB_SIZEABLE | WB_APP) )
            {/*65*/SensorCall();mpWindowImpl->mpFrame->GetClientSize( mnOutWidth, mnOutHeight );/*66*/}
    }
    else
    {
        SensorCall();if ( pParent )
        {
            SensorCall();if ( !ImplIsOverlapWindow() )
            {
                SensorCall();mpWindowImpl->mbDisabled          = pParent->mpWindowImpl->mbDisabled;
                mpWindowImpl->mbInputDisabled     = pParent->mpWindowImpl->mbInputDisabled;
                mpWindowImpl->meAlwaysInputMode   = pParent->mpWindowImpl->meAlwaysInputMode;
            }

            OutputDevice::SetSettings( pParent->GetSettings() );
        }

    }

    // setup the scale factor for Hi-DPI displays
    SensorCall();mnDPIScaleFactor = CountDPIScaleFactor(mpWindowImpl->mpFrameData->mnDPIY);

    const StyleSettings& rStyleSettings = mxSettings->GetStyleSettings();
    sal_uInt16 nScreenZoom = rStyleSettings.GetScreenZoom();
    mnDPIX          = (mpWindowImpl->mpFrameData->mnDPIX*nScreenZoom)/100;
    mnDPIY          = (mpWindowImpl->mpFrameData->mnDPIY*nScreenZoom)/100;
    maFont          = rStyleSettings.GetAppFont();

    ImplPointToLogic(*this, maFont);

    SensorCall();if ( nStyle & WB_3DLOOK )
    {
        SetTextColor( rStyleSettings.GetButtonTextColor() );
        SetBackground( Wallpaper( rStyleSettings.GetFaceColor() ) );
    }
    else
    {
        SetTextColor( rStyleSettings.GetWindowTextColor() );
        SetBackground( Wallpaper( rStyleSettings.GetWindowColor() ) );
    }

    SensorCall();(void)ImplUpdatePos();

    // calculate app font res (except for the Intro Window or the default window)
    SensorCall();if ( mpWindowImpl->mbFrame && !pSVData->maGDIData.mnAppFontX && ! (nStyle & (WB_INTROWIN|WB_DEFAULTWIN)) )
        {/*67*/SensorCall();ImplInitAppFontData( this );/*68*/}

    SensorCall();if ( GetAccessibleParentWindow()  && GetParent() != Application::GetDefDialogParent() )
        {/*69*/SensorCall();GetAccessibleParentWindow()->CallEventListeners( VCLEVENT_WINDOW_CHILDCREATED, this );/*70*/}
SensorCall();}

void Window::ImplInitAppFontData( vcl::Window* pWindow )
{
    SensorCall();ImplSVData* pSVData = ImplGetSVData();
    long nTextHeight = pWindow->GetTextHeight();
    long nTextWidth = pWindow->approximate_char_width() * 8;
    long nSymHeight = nTextHeight*4;
    // Make the basis wider if the font is too narrow
    // such that the dialog looks symmetrical and does not become too narrow.
    // Add some extra space when the dialog has the same width,
    // as a little more space is better.
    SensorCall();if ( nSymHeight > nTextWidth )
        {/*71*/SensorCall();nTextWidth = nSymHeight;/*72*/}
    else {/*73*/SensorCall();if ( nSymHeight+5 > nTextWidth )
        {/*75*/SensorCall();nTextWidth = nSymHeight+5;/*76*/}/*74*/}
    pSVData->maGDIData.mnAppFontX = nTextWidth * 10 / 8;
    pSVData->maGDIData.mnAppFontY = nTextHeight * 10;

#ifdef MACOSX
    // FIXME: this is currently only on OS X, check with other
    // platforms
    if( pSVData->maNWFData.mbNoFocusRects )
    {
        // try to find out whether there is a large correction
        // of control sizes, if yes, make app font scalings larger
        // so dialog positioning is not completely off
        ImplControlValue aControlValue;
        Rectangle aCtrlRegion( Point(), Size( nTextWidth < 10 ? 10 : nTextWidth, nTextHeight < 10 ? 10 : nTextHeight ) );
        Rectangle aBoundingRgn( aCtrlRegion );
        Rectangle aContentRgn( aCtrlRegion );
        if( pWindow->GetNativeControlRegion( CTRL_EDITBOX, PART_ENTIRE_CONTROL, aCtrlRegion,
                                             ControlState::ENABLED, aControlValue, OUString(),
                                             aBoundingRgn, aContentRgn ) )
        {
            // comment: the magical +6 is for the extra border in bordered
            // (which is the standard) edit fields
            if( aContentRgn.GetHeight() - nTextHeight > (nTextHeight+4)/4 )
                pSVData->maGDIData.mnAppFontY = (aContentRgn.GetHeight()-4) * 10;
        }
    }
#endif

    pSVData->maGDIData.mnRealAppFontX = pSVData->maGDIData.mnAppFontX;
    if ( pSVData->maAppData.mnDialogScaleX )
        pSVData->maGDIData.mnAppFontX += (pSVData->maGDIData.mnAppFontX*pSVData->maAppData.mnDialogScaleX)/100;
SensorCall();}

void Window::ImplInitWindowData( WindowType nType )
{
    SensorCall();mpWindowImpl = new WindowImpl( nType );

    meOutDevType        = OUTDEV_WINDOW;

    mbEnableRTL         = AllSettings::GetLayoutRTL();         // true: this outdev will be mirrored if RTL window layout (UI mirroring) is globally active
SensorCall();}

void Window::getFrameDev( const Point& rPt, const Point& rDevPt, const Size& rDevSize, OutputDevice& rDev )
{
    SensorCall();bool bOldMap = mbMap;
    mbMap = false;
    rDev.DrawOutDev( rDevPt, rDevSize, rPt, rDevSize, *this );
    mbMap = bOldMap;
SensorCall();}

void Window::drawFrameDev( const Point& rPt, const Point& rDevPt, const Size& rDevSize,
                           const OutputDevice& rOutDev, const vcl::Region& rRegion )
{

    SensorCall();GDIMetaFile*    pOldMetaFile = mpMetaFile;
    bool            bOldMap = mbMap;
    RasterOp        eOldROP = GetRasterOp();
    mpMetaFile = NULL;
    mbMap = false;
    SetRasterOp( ROP_OVERPAINT );

    SensorCall();if ( !IsDeviceOutputNecessary() )
        {/*77*/SensorCall();return;/*78*/}

    SensorCall();if ( !mpGraphics )
    {
        SensorCall();if ( !AcquireGraphics() )
            {/*79*/SensorCall();return;/*80*/}
    }

    SensorCall();if ( rRegion.IsNull() )
        {/*81*/SensorCall();mpGraphics->ResetClipRegion();/*82*/}
    else
        {/*83*/SelectClipRegion( rRegion );/*84*/}

    SensorCall();SalTwoRect aPosAry(rDevPt.X(), rDevPt.Y(), rDevSize.Width(), rDevSize.Height(),
                       rPt.X(), rPt.Y(), rDevSize.Width(), rDevSize.Height());
    drawOutDevDirect( &rOutDev, aPosAry );

    // Ensure that ClipRegion is recalculated and set
    mbInitClipRegion = true;

    SetRasterOp( eOldROP );
    mbMap = bOldMap;
    mpMetaFile = pOldMetaFile;
SensorCall();}


ImplWinData* Window::ImplGetWinData() const
{
    SensorCall();if ( !mpWindowImpl->mpWinData )
    {
        SensorCall();static const char* pNoNWF = getenv( "SAL_NO_NWF" );

        const_cast<vcl::Window*>(this)->mpWindowImpl->mpWinData = new ImplWinData;
        mpWindowImpl->mpWinData->mpExtOldText     = NULL;
        mpWindowImpl->mpWinData->mpExtOldAttrAry  = NULL;
        mpWindowImpl->mpWinData->mpCursorRect     = NULL;
        mpWindowImpl->mpWinData->mnCursorExtWidth = 0;
        mpWindowImpl->mpWinData->mpCompositionCharRects = NULL;
        mpWindowImpl->mpWinData->mnCompositionCharRects = 0;
        mpWindowImpl->mpWinData->mpFocusRect      = NULL;
        mpWindowImpl->mpWinData->mpTrackRect      = NULL;
        mpWindowImpl->mpWinData->mnTrackFlags     = 0;
        mpWindowImpl->mpWinData->mnIsTopWindow  = (sal_uInt16) ~0;  // not initialized yet, 0/1 will indicate TopWindow (see IsTopWindow())
        mpWindowImpl->mpWinData->mbMouseOver      = false;
        mpWindowImpl->mpWinData->mbEnableNativeWidget = !(pNoNWF && *pNoNWF); // true: try to draw this control with native theme API
    }

    {const struct ImplWinData * ReplaceReturn = mpWindowImpl->mpWinData; SensorCall(); return ReplaceReturn;}
}


void Window::CopyDeviceArea( SalTwoRect& aPosAry, bool bWindowInvalidate )
{
    SensorCall();if (aPosAry.mnSrcWidth == 0 || aPosAry.mnSrcHeight == 0 || aPosAry.mnDestWidth == 0 || aPosAry.mnDestHeight == 0)
        {/*85*/SensorCall();return;/*86*/}

    SensorCall();if (bWindowInvalidate)
    {
        SensorCall();const Rectangle aSrcRect(Point(aPosAry.mnSrcX, aPosAry.mnSrcY),
                Size(aPosAry.mnSrcWidth, aPosAry.mnSrcHeight));

        ImplMoveAllInvalidateRegions(aSrcRect,
                aPosAry.mnDestX-aPosAry.mnSrcX,
                aPosAry.mnDestY-aPosAry.mnSrcY,
                false);

        mpGraphics->CopyArea(aPosAry.mnDestX, aPosAry.mnDestY,
                aPosAry.mnSrcX, aPosAry.mnSrcY,
                aPosAry.mnSrcWidth, aPosAry.mnSrcHeight,
                SAL_COPYAREA_WINDOWINVALIDATE, this);

        SensorCall();return;
    }

    OutputDevice::CopyDeviceArea(aPosAry, bWindowInvalidate);
SensorCall();}

SalGraphics* Window::ImplGetFrameGraphics() const
{
    SensorCall();if ( mpWindowImpl->mpFrameWindow->mpGraphics )
    {
        mpWindowImpl->mpFrameWindow->mbInitClipRegion = true;
    }
    else
    {
        SensorCall();OutputDevice* pFrameWinOutDev = mpWindowImpl->mpFrameWindow;
        SensorCall();if ( ! pFrameWinOutDev->AcquireGraphics() )
        {
            {SalGraphics * ReplaceReturn = NULL; SensorCall(); return ReplaceReturn;}
        }
    }
    mpWindowImpl->mpFrameWindow->mpGraphics->ResetClipRegion();
    return mpWindowImpl->mpFrameWindow->mpGraphics;
SensorCall();}

void Window::ImplSetReallyVisible()
{
    // #i43594# it is possible that INITSHOW was never send, because the visibility state changed between
    // ImplCallInitShow() and ImplSetReallyVisible() when called from Show()
    // mbReallyShown is a useful indicator
    SensorCall();if( !mpWindowImpl->mbReallyShown )
        {/*87*/SensorCall();ImplCallInitShow();/*88*/}

    SensorCall();bool bBecameReallyVisible = !mpWindowImpl->mbReallyVisible;

    mbDevOutput     = true;
    mpWindowImpl->mbReallyVisible = true;
    mpWindowImpl->mbReallyShown   = true;

    // the SHOW/HIDE events serve as indicators to send child creation/destroy events to the access bridge.
    // For this, the data member of the event must not be NULL.
    // Previously, we did this in Window::Show, but there some events got lost in certain situations. Now
    // we're doing it when the visibility really changes
    SensorCall();if( bBecameReallyVisible && ImplIsAccessibleCandidate() )
        {/*89*/SensorCall();CallEventListeners( VCLEVENT_WINDOW_SHOW, this );/*90*/}
        // TODO. It's kind of a hack that we're re-using the VCLEVENT_WINDOW_SHOW. Normally, we should
        // introduce another event which explicitly triggers the Accessibility implementations.

    SensorCall();vcl::Window* pWindow = mpWindowImpl->mpFirstOverlap;
    SensorCall();while ( pWindow )
    {
        SensorCall();if ( pWindow->mpWindowImpl->mbVisible )
            {/*91*/SensorCall();pWindow->ImplSetReallyVisible();/*92*/}
        pWindow = pWindow->mpWindowImpl->mpNext;
    }

    pWindow = mpWindowImpl->mpFirstChild;
    SensorCall();while ( pWindow )
    {
        SensorCall();if ( pWindow->mpWindowImpl->mbVisible )
            {/*93*/SensorCall();pWindow->ImplSetReallyVisible();/*94*/}
        pWindow = pWindow->mpWindowImpl->mpNext;
    }
SensorCall();}

void Window::ImplAddDel( ImplDelData* pDel ) // TODO: make "const" when incompatibility ok
{
    SensorCall();if ( IsDisposed() )
    {
        SensorCall();pDel->mbDel = true;
        SensorCall();return;
    }

    DBG_ASSERT( !pDel->mpWindow, "Window::ImplAddDel(): cannot add ImplDelData twice !" );
    SensorCall();if( !pDel->mpWindow )
    {
        pDel->mpWindow = this;  // #112873# store ref to this window, so pDel can remove itself
        SensorCall();pDel->mpNext = mpWindowImpl->mpFirstDel;
        mpWindowImpl->mpFirstDel = pDel;
    }
SensorCall();}

void Window::ImplRemoveDel( ImplDelData* pDel ) // TODO: make "const" when incompatibility ok
{
    pDel->mpWindow = NULL;      // #112873# pDel is not associated with a Window anymore

    SensorCall();if ( IsDisposed() )
        {/*95*/SensorCall();return;/*96*/}

    SensorCall();if ( mpWindowImpl->mpFirstDel == pDel )
        {/*97*/SensorCall();mpWindowImpl->mpFirstDel = pDel->mpNext;/*98*/}
    else
    {
        SensorCall();ImplDelData* pData = mpWindowImpl->mpFirstDel;
        SensorCall();while ( pData->mpNext != pDel )
            {/*99*/SensorCall();pData = pData->mpNext;/*100*/}
        SensorCall();pData->mpNext = pDel->mpNext;
    }
SensorCall();}

void Window::ImplInitResolutionSettings()
{
    // recalculate AppFont-resolution and DPI-resolution
    SensorCall();if (mpWindowImpl->mbFrame)
    {
        SensorCall();const StyleSettings& rStyleSettings = mxSettings->GetStyleSettings();
        sal_uInt16 nScreenZoom = rStyleSettings.GetScreenZoom();
        mnDPIX = (mpWindowImpl->mpFrameData->mnDPIX*nScreenZoom)/100;
        mnDPIY = (mpWindowImpl->mpFrameData->mnDPIY*nScreenZoom)/100;

        // setup the scale factor for Hi-DPI displays
        mnDPIScaleFactor = CountDPIScaleFactor(mpWindowImpl->mpFrameData->mnDPIY);
        SetPointFont(*this, rStyleSettings.GetAppFont());
    }
    else {/*101*/SensorCall();if ( mpWindowImpl->mpParent )
    {
        mnDPIX  = mpWindowImpl->mpParent->mnDPIX;
        mnDPIY  = mpWindowImpl->mpParent->mnDPIY;
        mnDPIScaleFactor = mpWindowImpl->mpParent->mnDPIScaleFactor;
    ;/*102*/}}

    // update the recalculated values for logical units
    // and also tools belonging to the values
    SensorCall();if (IsMapModeEnabled())
    {
        SensorCall();MapMode aMapMode = GetMapMode();
        SetMapMode();
        SetMapMode( aMapMode );
    }
SensorCall();}

void Window::ImplPointToLogic(vcl::RenderContext& rRenderContext, vcl::Font& rFont) const
{
    SensorCall();Size aSize = rFont.GetSize();
    sal_uInt16 nScreenFontZoom = rRenderContext.GetSettings().GetStyleSettings().GetScreenFontZoom();

    SensorCall();if (aSize.Width())
    {
        SensorCall();aSize.Width() *= mpWindowImpl->mpFrameData->mnDPIX;
        aSize.Width() += 72 / 2;
        aSize.Width() /= 72;
        aSize.Width() *= nScreenFontZoom;
        aSize.Width() /= 100;
    }
    SensorCall();aSize.Height() *= mpWindowImpl->mpFrameData->mnDPIY;
    aSize.Height() += 72/2;
    aSize.Height() /= 72;
    aSize.Height() *= nScreenFontZoom;
    aSize.Height() /= 100;

    SensorCall();if (rRenderContext.IsMapModeEnabled())
        {/*103*/SensorCall();aSize = rRenderContext.PixelToLogic(aSize);/*104*/}

    SensorCall();rFont.SetSize(aSize);
SensorCall();}

void Window::ImplLogicToPoint(vcl::RenderContext& rRenderContext, vcl::Font& rFont) const
{
    SensorCall();Size aSize = rFont.GetSize();
    sal_uInt16 nScreenFontZoom = rRenderContext.GetSettings().GetStyleSettings().GetScreenFontZoom();

    SensorCall();if (rRenderContext.IsMapModeEnabled())
        {/*105*/SensorCall();aSize = rRenderContext.LogicToPixel(aSize);/*106*/}

    SensorCall();if (aSize.Width())
    {
        SensorCall();aSize.Width() *= 100;
        aSize.Width() /= nScreenFontZoom;
        aSize.Width() *= 72;
        aSize.Width() += mpWindowImpl->mpFrameData->mnDPIX / 2;
        aSize.Width() /= mpWindowImpl->mpFrameData->mnDPIX;
    }
    SensorCall();aSize.Height() *= 100;
    aSize.Height() /= nScreenFontZoom;
    aSize.Height() *= 72;
    aSize.Height() += mpWindowImpl->mpFrameData->mnDPIY / 2;
    aSize.Height() /= mpWindowImpl->mpFrameData->mnDPIY;

    rFont.SetSize(aSize);
SensorCall();}

bool Window::ImplUpdatePos()
{
    SensorCall();bool bSysChild = false;

    SensorCall();if ( ImplIsOverlapWindow() )
    {
        SensorCall();mnOutOffX  = mpWindowImpl->mnX;
        mnOutOffY  = mpWindowImpl->mnY;
    }
    else
    {
        SensorCall();vcl::Window* pParent = ImplGetParent();

        mnOutOffX  = mpWindowImpl->mnX + pParent->mnOutOffX;
        mnOutOffY  = mpWindowImpl->mnY + pParent->mnOutOffY;
    }

    SensorCall();vcl::Window* pChild = mpWindowImpl->mpFirstChild;
    SensorCall();while ( pChild )
    {
        SensorCall();if ( pChild->ImplUpdatePos() )
            {/*107*/SensorCall();bSysChild = true;/*108*/}
        pChild = pChild->mpWindowImpl->mpNext;
    }

    SensorCall();if ( mpWindowImpl->mpSysObj )
        {/*109*/SensorCall();bSysChild = true;/*110*/}

    {_Bool  ReplaceReturn = bSysChild; SensorCall(); return ReplaceReturn;}
}

void Window::ImplUpdateSysObjPos()
{
    SensorCall();if ( mpWindowImpl->mpSysObj )
        {/*111*/SensorCall();mpWindowImpl->mpSysObj->SetPosSize( mnOutOffX, mnOutOffY, mnOutWidth, mnOutHeight );/*112*/}

    SensorCall();vcl::Window* pChild = mpWindowImpl->mpFirstChild;
    SensorCall();while ( pChild )
    {
        SensorCall();pChild->ImplUpdateSysObjPos();
        pChild = pChild->mpWindowImpl->mpNext;
    }
SensorCall();}

void Window::ImplPosSizeWindow( long nX, long nY,
                                long nWidth, long nHeight, PosSizeFlags nFlags )
{
    SensorCall();bool    bNewPos         = false;
    bool    bNewSize        = false;
    bool    bCopyBits       = false;
    long    nOldOutOffX     = mnOutOffX;
    long    nOldOutOffY     = mnOutOffY;
    long    nOldOutWidth    = mnOutWidth;
    long    nOldOutHeight   = mnOutHeight;
    vcl::Region* pOverlapRegion  = NULL;
    vcl::Region* pOldRegion      = NULL;

    SensorCall();if ( IsReallyVisible() )
    {
        SensorCall();if ( mpWindowImpl->mpFrameData->mpFirstBackWin )
            {/*113*/SensorCall();ImplInvalidateAllOverlapBackgrounds();/*114*/}

        SensorCall();Rectangle aOldWinRect( Point( nOldOutOffX, nOldOutOffY ),
                               Size( nOldOutWidth, nOldOutHeight ) );
        pOldRegion = new vcl::Region( aOldWinRect );
        if ( mpWindowImpl->mbWinRegion )
            pOldRegion->Intersect( ImplPixelToDevicePixel( mpWindowImpl->maWinRegion ) );

        SensorCall();if ( mnOutWidth && mnOutHeight && !mpWindowImpl->mbPaintTransparent &&
             !mpWindowImpl->mbInitWinClipRegion && !mpWindowImpl->maWinClipRegion.IsEmpty() &&
             !HasPaintEvent() )
            {/*115*/SensorCall();bCopyBits = true;/*116*/}
    }

    SensorCall();bool bnXRecycled = false; // avoid duplicate mirroring in RTL case
    SensorCall();if ( nFlags & PosSizeFlags::Width )
    {
        SensorCall();if(!( nFlags & PosSizeFlags::X ))
        {
            SensorCall();nX = mpWindowImpl->mnX;
            nFlags |= PosSizeFlags::X;
            bnXRecycled = true; // we're using a mnX which was already mirrored in RTL case
        }

        SensorCall();if ( nWidth < 0 )
            {/*117*/SensorCall();nWidth = 0;/*118*/}
        SensorCall();if ( nWidth != mnOutWidth )
        {
            SensorCall();mnOutWidth = nWidth;
            bNewSize = true;
            bCopyBits = false;
        }
    }
    SensorCall();if ( nFlags & PosSizeFlags::Height )
    {
        SensorCall();if ( nHeight < 0 )
            {/*119*/SensorCall();nHeight = 0;/*120*/}
        SensorCall();if ( nHeight != mnOutHeight )
        {
            SensorCall();mnOutHeight = nHeight;
            bNewSize = true;
            bCopyBits = false;
        }
    }

    SensorCall();if ( nFlags & PosSizeFlags::X )
    {
        SensorCall();long nOrgX = nX;
        // --- RTL ---  (compare the screen coordinates)
        Point aPtDev( Point( nX+mnOutOffX, 0 ) );
        OutputDevice *pOutDev = GetOutDev();
        SensorCall();if( pOutDev->HasMirroredGraphics() )
        {
            mpGraphics->mirror( aPtDev.X(), this );

            // #106948# always mirror our pos if our parent is not mirroring, even
            // if we are also not mirroring
            // --- RTL --- check if parent is in different coordinates
            SensorCall();if( !bnXRecycled && mpWindowImpl->mpParent && !mpWindowImpl->mpParent->mpWindowImpl->mbFrame && mpWindowImpl->mpParent->ImplIsAntiparallel() )
            {
                // --- RTL --- (re-mirror at parent window)
                nX = mpWindowImpl->mpParent->mnOutWidth - mnOutWidth - nX;
            }
            /* #i99166# An LTR window in RTL UI that gets sized only would be
               expected to not moved its upper left point
            */
            SensorCall();if( bnXRecycled )
            {
                SensorCall();if( ImplIsAntiparallel() )
                {
                    SensorCall();aPtDev.X() = mpWindowImpl->mnAbsScreenX;
                    nOrgX = mpWindowImpl->maPos.X();
                }
            }
        }
        else {/*121*/SensorCall();if( !bnXRecycled && mpWindowImpl->mpParent && !mpWindowImpl->mpParent->mpWindowImpl->mbFrame && mpWindowImpl->mpParent->ImplIsAntiparallel() )
        {
            // mirrored window in LTR UI
            {
                // --- RTL --- (re-mirror at parent window)
                nX = mpWindowImpl->mpParent->mnOutWidth - mnOutWidth - nX;
            }
        ;/*122*/}}

        // check maPos as well, as it could have been changed for client windows (ImplCallMove())
        SensorCall();if ( mpWindowImpl->mnAbsScreenX != aPtDev.X() || nX != mpWindowImpl->mnX || nOrgX != mpWindowImpl->maPos.X() )
        {
            SensorCall();if ( bCopyBits && !pOverlapRegion )
            {
                SensorCall();pOverlapRegion = new vcl::Region();
                ImplCalcOverlapRegion( Rectangle( Point( mnOutOffX, mnOutOffY ),
                                                  Size( mnOutWidth, mnOutHeight ) ),
                                       *pOverlapRegion, false, true, true );
            }
            SensorCall();mpWindowImpl->mnX = nX;
            mpWindowImpl->maPos.X() = nOrgX;
            mpWindowImpl->mnAbsScreenX = aPtDev.X();    // --- RTL --- (store real screen pos)
            bNewPos = true;
        }
    }
    SensorCall();if ( nFlags & PosSizeFlags::Y )
    {
        // check maPos as well, as it could have been changed for client windows (ImplCallMove())
        SensorCall();if ( nY != mpWindowImpl->mnY || nY != mpWindowImpl->maPos.Y() )
        {
            SensorCall();if ( bCopyBits && !pOverlapRegion )
            {
                SensorCall();pOverlapRegion = new vcl::Region();
                ImplCalcOverlapRegion( Rectangle( Point( mnOutOffX, mnOutOffY ),
                                                  Size( mnOutWidth, mnOutHeight ) ),
                                       *pOverlapRegion, false, true, true );
            }
            SensorCall();mpWindowImpl->mnY = nY;
            mpWindowImpl->maPos.Y() = nY;
            bNewPos = true;
        }
    }

    SensorCall();if ( bNewPos || bNewSize )
    {
        SensorCall();bool bUpdateSysObjPos = false;
        SensorCall();if ( bNewPos )
            {/*123*/SensorCall();bUpdateSysObjPos = ImplUpdatePos();/*124*/}

        // the borderwindow always specifies the position for its client window
        if ( mpWindowImpl->mpBorderWindow )
            mpWindowImpl->maPos = mpWindowImpl->mpBorderWindow->mpWindowImpl->maPos;

        SensorCall();if ( mpWindowImpl->mpClientWindow )
        {
            mpWindowImpl->mpClientWindow->ImplPosSizeWindow( mpWindowImpl->mpClientWindow->mpWindowImpl->mnLeftBorder,
                                               mpWindowImpl->mpClientWindow->mpWindowImpl->mnTopBorder,
                                               mnOutWidth-mpWindowImpl->mpClientWindow->mpWindowImpl->mnLeftBorder-mpWindowImpl->mpClientWindow->mpWindowImpl->mnRightBorder,
                                               mnOutHeight-mpWindowImpl->mpClientWindow->mpWindowImpl->mnTopBorder-mpWindowImpl->mpClientWindow->mpWindowImpl->mnBottomBorder,
                                               PosSizeFlags::X | PosSizeFlags::Y |
                                               PosSizeFlags::Width | PosSizeFlags::Height );
            // If we have a client window, then this is the position
            // of the Application's floating windows
            mpWindowImpl->mpClientWindow->mpWindowImpl->maPos = mpWindowImpl->maPos;
            SensorCall();if ( bNewPos )
            {
                SensorCall();if ( mpWindowImpl->mpClientWindow->IsVisible() )
                {
                    mpWindowImpl->mpClientWindow->ImplCallMove();
                }
                else
                {
                    mpWindowImpl->mpClientWindow->mpWindowImpl->mbCallMove = true;
                }
            }
        }

        // Move()/Resize() will be called only for Show(), such that
        // at least one is called before Show()
        SensorCall();if ( IsVisible() )
        {
            SensorCall();if ( bNewPos )
            {
                SensorCall();ImplCallMove();
            }
            SensorCall();if ( bNewSize )
            {
                SensorCall();ImplCallResize();
            }
        }
        else
        {
            SensorCall();if ( bNewPos )
                {/*125*/SensorCall();mpWindowImpl->mbCallMove = true;/*126*/}
            SensorCall();if ( bNewSize )
                {/*127*/SensorCall();mpWindowImpl->mbCallResize = true;/*128*/}
        }

        SensorCall();bool bUpdateSysObjClip = false;
        SensorCall();if ( IsReallyVisible() )
        {
            SensorCall();if ( bNewPos || bNewSize )
            {
                // reset background storage
                SensorCall();if ( mpWindowImpl->mpOverlapData && mpWindowImpl->mpOverlapData->mpSaveBackDev )
                    {/*129*/SensorCall();ImplDeleteOverlapBackground();/*130*/}
                SensorCall();if ( mpWindowImpl->mpFrameData->mpFirstBackWin )
                    {/*131*/SensorCall();ImplInvalidateAllOverlapBackgrounds();/*132*/}
                // set Clip-Flag
                SensorCall();bUpdateSysObjClip = !ImplSetClipFlag( true );
            }

            // invalidate window content ?
            SensorCall();if ( bNewPos || (mnOutWidth > nOldOutWidth) || (mnOutHeight > nOldOutHeight) )
            {
                SensorCall();if ( bNewPos )
                {
                    SensorCall();bool bInvalidate = false;
                    bool bParentPaint = true;
                    if ( !ImplIsOverlapWindow() )
                        bParentPaint = mpWindowImpl->mpParent->IsPaintEnabled();
                    SensorCall();if ( bCopyBits && bParentPaint && !HasPaintEvent() )
                    {
                        SensorCall();Point aPoint( mnOutOffX, mnOutOffY );
                        vcl::Region aRegion( Rectangle( aPoint,
                                                   Size( mnOutWidth, mnOutHeight ) ) );
                        if ( mpWindowImpl->mbWinRegion )
                            aRegion.Intersect( ImplPixelToDevicePixel( mpWindowImpl->maWinRegion ) );
                        ImplClipBoundaries( aRegion, false, true );
                        SensorCall();if ( !pOverlapRegion->IsEmpty() )
                        {
                            SensorCall();pOverlapRegion->Move( mnOutOffX-nOldOutOffX, mnOutOffY-nOldOutOffY );
                            aRegion.Exclude( *pOverlapRegion );
                        }
                        SensorCall();if ( !aRegion.IsEmpty() )
                        {
                            // adapt Paint areas
                            SensorCall();ImplMoveAllInvalidateRegions( Rectangle( Point( nOldOutOffX, nOldOutOffY ),
                                                                     Size( nOldOutWidth, nOldOutHeight ) ),
                                                          mnOutOffX-nOldOutOffX, mnOutOffY-nOldOutOffY,
                                                          true );
                            SalGraphics* pGraphics = ImplGetFrameGraphics();
                            SensorCall();if ( pGraphics )
                            {

                                SensorCall();OutputDevice *pOutDev = GetOutDev();
                                const bool bSelectClipRegion = pOutDev->SelectClipRegion( aRegion, pGraphics );
                                SensorCall();if ( bSelectClipRegion )
                                {
                                    pGraphics->CopyArea( mnOutOffX, mnOutOffY,
                                                         nOldOutOffX, nOldOutOffY,
                                                         nOldOutWidth, nOldOutHeight,
                                                         SAL_COPYAREA_WINDOWINVALIDATE, this );
                                }
                                else
                                    {/*133*/SensorCall();bInvalidate = true;/*134*/}
                            }
                            else
                                {/*135*/SensorCall();bInvalidate = true;/*136*/}
                            SensorCall();if ( !bInvalidate )
                            {
                                SensorCall();if ( !pOverlapRegion->IsEmpty() )
                                    {/*137*/SensorCall();ImplInvalidateFrameRegion( pOverlapRegion, INVALIDATE_CHILDREN );/*138*/}
                            }
                        }
                        else
                            {/*139*/SensorCall();bInvalidate = true;/*140*/}
                    }
                    else
                        {/*141*/SensorCall();bInvalidate = true;/*142*/}
                    SensorCall();if ( bInvalidate )
                        {/*143*/SensorCall();ImplInvalidateFrameRegion( NULL, INVALIDATE_CHILDREN );/*144*/}
                }
                else
                {
                    SensorCall();Point aPoint( mnOutOffX, mnOutOffY );
                    vcl::Region aRegion( Rectangle( aPoint,
                                               Size( mnOutWidth, mnOutHeight ) ) );
                    aRegion.Exclude( *pOldRegion );
                    if ( mpWindowImpl->mbWinRegion )
                        aRegion.Intersect( ImplPixelToDevicePixel( mpWindowImpl->maWinRegion ) );
                    ImplClipBoundaries( aRegion, false, true );
                    SensorCall();if ( !aRegion.IsEmpty() )
                        {/*145*/SensorCall();ImplInvalidateFrameRegion( &aRegion, INVALIDATE_CHILDREN );/*146*/}
                }
            }

            // invalidate Parent or Overlaps
            SensorCall();if ( bNewPos ||
                 (mnOutWidth < nOldOutWidth) || (mnOutHeight < nOldOutHeight) )
            {
                SensorCall();vcl::Region aRegion( *pOldRegion );
                SensorCall();if ( !mpWindowImpl->mbPaintTransparent )
                    {/*147*/SensorCall();ImplExcludeWindowRegion( aRegion );/*148*/}
                SensorCall();ImplClipBoundaries( aRegion, false, true );
                SensorCall();if ( !aRegion.IsEmpty() && !mpWindowImpl->mpBorderWindow )
                    {/*149*/SensorCall();ImplInvalidateParentFrameRegion( aRegion );/*150*/}
            }
        }

        // adapt system objects
        SensorCall();if ( bUpdateSysObjClip )
            {/*151*/SensorCall();ImplUpdateSysObjClip();/*152*/}
        SensorCall();if ( bUpdateSysObjPos )
            {/*153*/SensorCall();ImplUpdateSysObjPos();/*154*/}
        SensorCall();if ( bNewSize && mpWindowImpl->mpSysObj )
            {/*155*/SensorCall();mpWindowImpl->mpSysObj->SetPosSize( mnOutOffX, mnOutOffY, mnOutWidth, mnOutHeight );/*156*/}
    }

    SensorCall();delete pOverlapRegion;
    delete pOldRegion;
SensorCall();}

void Window::ImplNewInputContext()
{
    SensorCall();ImplSVData* pSVData = ImplGetSVData();
    vcl::Window*     pFocusWin = pSVData->maWinData.mpFocusWin;
    SensorCall();if ( !pFocusWin )
        {/*157*/SensorCall();return;/*158*/}

    // Is InputContext changed?
    SensorCall();const InputContext& rInputContext = pFocusWin->GetInputContext();
    SensorCall();if ( rInputContext == pFocusWin->mpWindowImpl->mpFrameData->maOldInputContext )
        {/*159*/SensorCall();return;/*160*/}

    SensorCall();pFocusWin->mpWindowImpl->mpFrameData->maOldInputContext = rInputContext;

    SalInputContext         aNewContext;
    const vcl::Font&        rFont = rInputContext.GetFont();
    const OUString&         rFontName = rFont.GetName();
    ImplFontEntry*          pFontEntry = NULL;
    aNewContext.mpFont = NULL;
    SensorCall();if (!rFontName.isEmpty())
    {
        SensorCall();OutputDevice *pFocusWinOutDev = pFocusWin->GetOutDev();
        Size aSize = pFocusWinOutDev->ImplLogicToDevicePixel( rFont.GetSize() );
        SensorCall();if ( !aSize.Height() )
        {
            // only set default sizes if the font height in logical
            // coordinates equals 0
            SensorCall();if ( rFont.GetSize().Height() )
                {/*161*/SensorCall();aSize.Height() = 1;/*162*/}
            else
                {/*163*/SensorCall();aSize.Height() = (12*pFocusWin->mnDPIY)/72;/*164*/}
        }
        SensorCall();pFontEntry = pFocusWin->mpFontCache->GetFontEntry( pFocusWin->mpFontCollection,
                         rFont, aSize, static_cast<float>(aSize.Height()) );
        SensorCall();if ( pFontEntry )
            {/*165*/SensorCall();aNewContext.mpFont = &pFontEntry->maFontSelData;/*166*/}
    }
    SensorCall();aNewContext.meLanguage  = rFont.GetLanguage();
    aNewContext.mnOptions   = rInputContext.GetOptions();
    pFocusWin->ImplGetFrame()->SetInputContext( &aNewContext );

    SensorCall();if ( pFontEntry )
        {/*167*/SensorCall();pFocusWin->mpFontCache->Release( pFontEntry );/*168*/}
SensorCall();}

void Window::doLazyDelete()
{
    SensorCall();SystemWindow* pSysWin = dynamic_cast<SystemWindow*>(this);
    DockingWindow* pDockWin = dynamic_cast<DockingWindow*>(this);
    SensorCall();if( pSysWin || ( pDockWin && pDockWin->IsFloatingMode() ) )
    {
        SensorCall();Show( false );
        SetParent( ImplGetDefaultWindow() );
    }
    SensorCall();vcl::LazyDeletor::Delete( this );
SensorCall();}

KeyIndicatorState Window::GetIndicatorState() const
{
    {const enum KeyIndicatorState  ReplaceReturn = mpWindowImpl->mpFrame->GetIndicatorState(); SensorCall(); return ReplaceReturn;}
}

void Window::SimulateKeyPress( sal_uInt16 nKeyCode ) const
{
    SensorCall();mpWindowImpl->mpFrame->SimulateKeyPress(nKeyCode);
SensorCall();}

void Window::KeyInput( const KeyEvent& rKEvt )
{
    SensorCall();NotifyEvent aNEvt( MouseNotifyEvent::KEYINPUT, this, &rKEvt );
    SensorCall();if ( !CompatNotify( aNEvt ) )
        {/*169*/SensorCall();mpWindowImpl->mbKeyInput = true;/*170*/}
SensorCall();}

void Window::KeyUp( const KeyEvent& rKEvt )
{
    SensorCall();NotifyEvent aNEvt( MouseNotifyEvent::KEYUP, this, &rKEvt );
    SensorCall();if ( !CompatNotify( aNEvt ) )
        {/*171*/SensorCall();mpWindowImpl->mbKeyUp = true;/*172*/}
SensorCall();}

void Window::Draw( OutputDevice*, const Point&, const Size&, sal_uLong )
{
SensorCall();SensorCall();}

void Window::Move() {SensorCall();}

void Window::Resize() {SensorCall();}

void Window::Activate() {SensorCall();}

void Window::Deactivate() {SensorCall();}

void Window::GetFocus()
{
    SensorCall();if ( HasFocus() && mpWindowImpl->mpLastFocusWindow && !(mpWindowImpl->mnDlgCtrlFlags & WINDOW_DLGCTRL_WANTFOCUS) )
    {
        SensorCall();ImplDelData aDogtag( this );
        mpWindowImpl->mpLastFocusWindow->GrabFocus();
        SensorCall();if( aDogtag.IsDead() )
            {/*173*/SensorCall();return;/*174*/}
    }

    SensorCall();NotifyEvent aNEvt( MouseNotifyEvent::GETFOCUS, this );
    CompatNotify( aNEvt );
SensorCall();}

void Window::LoseFocus()
{
    SensorCall();NotifyEvent aNEvt( MouseNotifyEvent::LOSEFOCUS, this );
    CompatNotify( aNEvt );
SensorCall();}

void Window::RequestHelp( const HelpEvent& rHEvt )
{
    // if Balloon-Help is requested, show the balloon
    // with help text set
    SensorCall();if ( rHEvt.GetMode() & HelpEventMode::BALLOON )
    {
        SensorCall();OUString rStr = GetHelpText();
        SensorCall();if ( rStr.isEmpty() )
            {/*175*/SensorCall();rStr = GetQuickHelpText();/*176*/}
        SensorCall();if ( rStr.isEmpty() && ImplGetParent() && !ImplIsOverlapWindow() )
            {/*177*/SensorCall();ImplGetParent()->RequestHelp( rHEvt );/*178*/}
        else
        {
            SensorCall();Point aPos = GetPosPixel();
            SensorCall();if ( ImplGetParent() && !ImplIsOverlapWindow() )
                {/*179*/SensorCall();aPos = ImplGetParent()->OutputToScreenPixel( aPos );/*180*/}
            SensorCall();Rectangle   aRect( aPos, GetSizePixel() );

            Help::ShowBalloon( this, rHEvt.GetMousePosPixel(), aRect, rStr );
        }
    }
    else {/*181*/SensorCall();if ( rHEvt.GetMode() & HelpEventMode::QUICK )
    {
        SensorCall();const OUString& rStr = GetQuickHelpText();
        SensorCall();if ( rStr.isEmpty() && ImplGetParent() && !ImplIsOverlapWindow() )
            {/*183*/SensorCall();ImplGetParent()->RequestHelp( rHEvt );/*184*/}
        else
        {
            SensorCall();Point aPos = GetPosPixel();
            SensorCall();if ( ImplGetParent() && !ImplIsOverlapWindow() )
                {/*185*/SensorCall();aPos = ImplGetParent()->OutputToScreenPixel( aPos );/*186*/}
            SensorCall();Rectangle   aRect( aPos, GetSizePixel() );
            OUString      aHelpText;
            SensorCall();if ( !rStr.isEmpty() )
                {/*187*/SensorCall();aHelpText = GetHelpText();/*188*/}
            SensorCall();Help::ShowQuickHelp( this, aRect, rStr, aHelpText, QuickHelpFlags::CtrlText );
        }
    }
    else
    {
        SensorCall();OUString aStrHelpId( OStringToOUString( GetHelpId(), RTL_TEXTENCODING_UTF8 ) );
        SensorCall();if ( aStrHelpId.isEmpty() && ImplGetParent() )
            {/*189*/SensorCall();ImplGetParent()->RequestHelp( rHEvt );/*190*/}
        else
        {
            SensorCall();Help* pHelp = Application::GetHelp();
            SensorCall();if ( pHelp )
            {
                SensorCall();if( !aStrHelpId.isEmpty() )
                    {/*191*/SensorCall();pHelp->Start( aStrHelpId, this );/*192*/}
                else
                    {/*193*/pHelp->Start( OUString( OOO_HELP_INDEX  ), this );/*194*/}
            }
        }
    ;/*182*/}}
SensorCall();}

void Window::Command( const CommandEvent& rCEvt )
{
    SensorCall();CallEventListeners( VCLEVENT_WINDOW_COMMAND, (void*)&rCEvt );

    NotifyEvent aNEvt( MouseNotifyEvent::COMMAND, this, &rCEvt );
    SensorCall();if ( !CompatNotify( aNEvt ) )
        {/*195*/SensorCall();mpWindowImpl->mbCommand = true;/*196*/}
SensorCall();}

void Window::Tracking( const TrackingEvent& rTEvt )
{

    SensorCall();ImplDockingWindowWrapper *pWrapper = ImplGetDockingManager()->GetDockingWindowWrapper( this );
    SensorCall();if( pWrapper )
        {/*197*/SensorCall();pWrapper->Tracking( rTEvt );/*198*/}
SensorCall();}

void Window::StateChanged(StateChangedType eType)
{
    SensorCall();switch (eType)
    {
        //stuff that doesn't invalidate the layout
        case StateChangedType::ControlForeground:
        case StateChangedType::ControlBackground:
        case StateChangedType::Transparent:
        case StateChangedType::UpdateMode:
        case StateChangedType::ReadOnly:
        case StateChangedType::Enable:
        case StateChangedType::State:
        case StateChangedType::Data:
        case StateChangedType::InitShow:
        case StateChangedType::ControlFocus:
            break;
        //stuff that does invalidate the layout
        default:
            SensorCall();queue_resize(eType);
            SensorCall();break;
    }
SensorCall();}

bool Window::IsLocked( bool bChildren ) const
{
    SensorCall();if ( mpWindowImpl->mnLockCount != 0 )
        {/*199*/{const _Bool  ReplaceReturn = true; SensorCall(); return ReplaceReturn;}/*200*/}

    SensorCall();if ( bChildren || mpWindowImpl->mbChildNotify )
    {
        SensorCall();vcl::Window* pChild = mpWindowImpl->mpFirstChild;
        SensorCall();while ( pChild )
        {
            SensorCall();if ( pChild->IsLocked( true ) )
                {/*201*/{const _Bool  ReplaceReturn = true; SensorCall(); return ReplaceReturn;}/*202*/}
            pChild = pChild->mpWindowImpl->mpNext;
        }
    }

    {const _Bool  ReplaceReturn = false; SensorCall(); return ReplaceReturn;}
}

void Window::SetStyle( WinBits nStyle )
{
    SensorCall();if ( mpWindowImpl && mpWindowImpl->mnStyle != nStyle )
    {
        SensorCall();mpWindowImpl->mnPrevStyle = mpWindowImpl->mnStyle;
        mpWindowImpl->mnStyle = nStyle;
        CompatStateChanged( StateChangedType::Style );
    }
SensorCall();}

void Window::SetExtendedStyle( WinBits nExtendedStyle )
{

    SensorCall();if ( mpWindowImpl->mnExtendedStyle != nExtendedStyle )
    {
        SensorCall();vcl::Window* pWindow = ImplGetBorderWindow();
        SensorCall();if( ! pWindow )
            {/*203*/SensorCall();pWindow = this;/*204*/}
        SensorCall();if( pWindow->mpWindowImpl->mbFrame )
        {
            SensorCall();SalExtStyle nExt = 0;
            SensorCall();if( (nExtendedStyle & WB_EXT_DOCUMENT) )
                nExt |= SAL_FRAME_EXT_STYLE_DOCUMENT;
            SensorCall();if( (nExtendedStyle & WB_EXT_DOCMODIFIED) )
                nExt |= SAL_FRAME_EXT_STYLE_DOCMODIFIED;

            SensorCall();pWindow->ImplGetFrame()->SetExtendedFrameStyle( nExt );
        }
        SensorCall();mpWindowImpl->mnPrevExtendedStyle = mpWindowImpl->mnExtendedStyle;
        mpWindowImpl->mnExtendedStyle = nExtendedStyle;
        CompatStateChanged( StateChangedType::ExtendedStyle );
    }
SensorCall();}

void Window::SetBorderStyle( WindowBorderStyle nBorderStyle )
{

    SensorCall();if ( mpWindowImpl->mpBorderWindow )
    {
        SensorCall();if( nBorderStyle == WindowBorderStyle::REMOVEBORDER &&
            ! mpWindowImpl->mpBorderWindow->mpWindowImpl->mbFrame &&
            mpWindowImpl->mpBorderWindow->mpWindowImpl->mpParent
            )
        {
            // this is a little awkward: some controls (e.g. svtools ProgressBar)
            // cannot avoid getting constructed with WB_BORDER but want to disable
            // borders in case of NWF drawing. So they need a method to remove their border window
            SensorCall();VclPtr<vcl::Window> pBorderWin = mpWindowImpl->mpBorderWindow;
            // remove us as border window's client
            pBorderWin->mpWindowImpl->mpClientWindow = NULL;
            mpWindowImpl->mpBorderWindow = NULL;
            mpWindowImpl->mpRealParent = pBorderWin->mpWindowImpl->mpParent;
            // reparent us above the border window
            SetParent( pBorderWin->mpWindowImpl->mpParent );
            // set us to the position and size of our previous border
            Point aBorderPos( pBorderWin->GetPosPixel() );
            Size aBorderSize( pBorderWin->GetSizePixel() );
            setPosSizePixel( aBorderPos.X(), aBorderPos.Y(), aBorderSize.Width(), aBorderSize.Height() );
            // release border window
            pBorderWin.disposeAndClear();

            // set new style bits
            SetStyle( GetStyle() & (~WB_BORDER) );
        }
        else
        {
            if ( mpWindowImpl->mpBorderWindow->GetType() == WINDOW_BORDERWINDOW )
                static_cast<ImplBorderWindow*>(mpWindowImpl->mpBorderWindow.get())->SetBorderStyle( nBorderStyle );
            else
                mpWindowImpl->mpBorderWindow->SetBorderStyle( nBorderStyle );
        }
    }
SensorCall();}

WindowBorderStyle Window::GetBorderStyle() const
{

    SensorCall();if ( mpWindowImpl->mpBorderWindow )
    {
        if ( mpWindowImpl->mpBorderWindow->GetType() == WINDOW_BORDERWINDOW )
            return static_cast<ImplBorderWindow*>(mpWindowImpl->mpBorderWindow.get())->GetBorderStyle();
        else
            return mpWindowImpl->mpBorderWindow->GetBorderStyle();
    }

    return WindowBorderStyle::NONE;
SensorCall();}

long Window::CalcTitleWidth() const
{

    SensorCall();if ( mpWindowImpl->mpBorderWindow )
    {
        if ( mpWindowImpl->mpBorderWindow->GetType() == WINDOW_BORDERWINDOW )
            return static_cast<ImplBorderWindow*>(mpWindowImpl->mpBorderWindow.get())->CalcTitleWidth();
        else
            return mpWindowImpl->mpBorderWindow->CalcTitleWidth();
    }
    else {/*205*/SensorCall();if ( mpWindowImpl->mbFrame && (mpWindowImpl->mnStyle & WB_MOVEABLE) )
    {
        // we guess the width for frame windows as we do not know the
        // border of external dialogs
        SensorCall();const StyleSettings& rStyleSettings = GetSettings().GetStyleSettings();
        vcl::Font aFont = GetFont();
        const_cast<vcl::Window*>(this)->SetPointFont(*const_cast<Window*>(this), rStyleSettings.GetTitleFont());
        long nTitleWidth = GetTextWidth( GetText() );
        const_cast<vcl::Window*>(this)->SetFont( aFont );
        nTitleWidth += rStyleSettings.GetTitleHeight() * 3;
        nTitleWidth += rStyleSettings.GetBorderSize() * 2;
        nTitleWidth += 10;
        {const long  ReplaceReturn = nTitleWidth; SensorCall(); return ReplaceReturn;}
    ;/*206*/}}

    {const long  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

void Window::SetInputContext( const InputContext& rInputContext )
{

    SensorCall();mpWindowImpl->maInputContext = rInputContext;
    SensorCall();if ( !mpWindowImpl->mbInFocusHdl && HasFocus() )
        {/*207*/SensorCall();ImplNewInputContext();/*208*/}
SensorCall();}

void Window::EndExtTextInput( sal_uInt16 nFlags )
{

    SensorCall();if ( mpWindowImpl->mbExtTextInput )
        {/*209*/SensorCall();ImplGetFrame()->EndExtTextInput( nFlags );/*210*/}
SensorCall();}

void Window::SetCursorRect( const Rectangle* pRect, long nExtTextInputWidth )
{

    SensorCall();ImplWinData* pWinData = ImplGetWinData();
    SensorCall();if ( pWinData->mpCursorRect )
    {
        SensorCall();if ( pRect )
            {/*211*/SensorCall();*pWinData->mpCursorRect = *pRect;/*212*/}
        else
        {
            SensorCall();delete pWinData->mpCursorRect;
            pWinData->mpCursorRect = NULL;
        }
    }
    else
    {
        SensorCall();if ( pRect )
            {/*213*/SensorCall();pWinData->mpCursorRect = new Rectangle( *pRect );/*214*/}
    }

    SensorCall();pWinData->mnCursorExtWidth = nExtTextInputWidth;

SensorCall();}

const Rectangle* Window::GetCursorRect() const
{

    SensorCall();ImplWinData* pWinData = ImplGetWinData();
    {const class Rectangle * ReplaceReturn = pWinData->mpCursorRect; SensorCall(); return ReplaceReturn;}
}

long Window::GetCursorExtTextInputWidth() const
{

    SensorCall();ImplWinData* pWinData = ImplGetWinData();
    {const long  ReplaceReturn = pWinData->mnCursorExtWidth; SensorCall(); return ReplaceReturn;}
}

void Window::SetCompositionCharRect( const Rectangle* pRect, long nCompositionLength, bool bVertical ) {

    SensorCall();ImplWinData* pWinData = ImplGetWinData();
    delete[] pWinData->mpCompositionCharRects;
    pWinData->mbVertical = bVertical;
    pWinData->mpCompositionCharRects = NULL;
    pWinData->mnCompositionCharRects = nCompositionLength;
    SensorCall();if ( pRect && (nCompositionLength > 0) )
    {
        SensorCall();pWinData->mpCompositionCharRects = new Rectangle[nCompositionLength];
        SensorCall();for (long i = 0; i < nCompositionLength; ++i)
            {/*215*/SensorCall();pWinData->mpCompositionCharRects[i] = pRect[i];/*216*/}
    }
SensorCall();}

void Window::CollectChildren(::std::vector<vcl::Window *>& rAllChildren )
{
    rAllChildren.push_back( this );

    SensorCall();vcl::Window* pChild = mpWindowImpl->mpFirstChild;
    SensorCall();while ( pChild )
    {
        pChild->CollectChildren( rAllChildren );
        pChild = pChild->mpWindowImpl->mpNext;
    }
SensorCall();}

void Window::SetPointFont(vcl::RenderContext& rRenderContext, const vcl::Font& rFont)
{
    SensorCall();vcl::Font aFont = rFont;
    ImplPointToLogic(rRenderContext, aFont);
    rRenderContext.SetFont(aFont);
SensorCall();}

vcl::Font Window::GetPointFont(vcl::RenderContext& rRenderContext) const
{
    SensorCall();vcl::Font aFont = rRenderContext.GetFont();
    ImplLogicToPoint(rRenderContext, aFont);
    {const vcl::Font  ReplaceReturn = aFont; SensorCall(); return ReplaceReturn;}
}

void Window::Show(bool bVisible, sal_uInt16 nFlags)
{
    SensorCall();if ( IsDisposed() || mpWindowImpl->mbVisible == bVisible )
        {/*217*/SensorCall();return;/*218*/}

    SensorCall();ImplDelData aDogTag( this );

    bool bRealVisibilityChanged = false;
    mpWindowImpl->mbVisible = bVisible;

    SensorCall();if ( !bVisible )
    {
        SensorCall();ImplHideAllOverlaps();
        SensorCall();if( aDogTag.IsDead() )
            {/*219*/SensorCall();return;/*220*/}

        SensorCall();if ( mpWindowImpl->mpBorderWindow )
        {
            SensorCall();bool bOldUpdate = mpWindowImpl->mpBorderWindow->mpWindowImpl->mbNoParentUpdate;
            if ( mpWindowImpl->mbNoParentUpdate )
                mpWindowImpl->mpBorderWindow->mpWindowImpl->mbNoParentUpdate = true;
            mpWindowImpl->mpBorderWindow->Show( false, nFlags );
            mpWindowImpl->mpBorderWindow->mpWindowImpl->mbNoParentUpdate = bOldUpdate;
        }
        else {/*221*/SensorCall();if ( mpWindowImpl->mbFrame )
        {
            SensorCall();mpWindowImpl->mbSuppressAccessibilityEvents = true;
            mpWindowImpl->mpFrame->Show( false, false );
        ;/*222*/}}

        CompatStateChanged( StateChangedType::Visible );

        SensorCall();if ( mpWindowImpl->mbReallyVisible )
        {
            SensorCall();vcl::Region  aInvRegion;
            bool    bSaveBack = false;

            SensorCall();if ( ImplIsOverlapWindow() && !mpWindowImpl->mbFrame )
            {
                SensorCall();if ( ImplRestoreOverlapBackground( aInvRegion ) )
                    {/*223*/SensorCall();bSaveBack = true;/*224*/}
            }

            SensorCall();if ( !bSaveBack )
            {
                SensorCall();if ( mpWindowImpl->mbInitWinClipRegion )
                    {/*225*/SensorCall();ImplInitWinClipRegion();/*226*/}
                aInvRegion = mpWindowImpl->maWinClipRegion;
            }

            SensorCall();if( aDogTag.IsDead() )
                {/*227*/SensorCall();return;/*228*/}

            SensorCall();bRealVisibilityChanged = mpWindowImpl->mbReallyVisible;
            ImplResetReallyVisible();
            ImplSetClipFlag();

            SensorCall();if ( ImplIsOverlapWindow() && !mpWindowImpl->mbFrame )
            {
                // convert focus
                SensorCall();if ( !(nFlags & SHOW_NOFOCUSCHANGE) && HasChildPathFocus() )
                {
                    if ( mpWindowImpl->mpOverlapWindow->IsEnabled() &&
                         mpWindowImpl->mpOverlapWindow->IsInputEnabled() &&
                         ! mpWindowImpl->mpOverlapWindow->IsInModalMode()
                         )
                        mpWindowImpl->mpOverlapWindow->GrabFocus();
                }
            }

            SensorCall();if ( !mpWindowImpl->mbFrame )
            {
                SensorCall();if( mpWindowImpl->mpWinData && mpWindowImpl->mpWinData->mbEnableNativeWidget )
                {
                    /*
                    * #i48371# native theming: some themes draw outside the control
                    * area we tell them to (bad thing, but we cannot do much about it ).
                    * On hiding these controls they get invalidated with their window rectangle
                    * which leads to the parts outside the control area being left and not
                    * invalidated. Workaround: invalidate an area on the parent, too
                    */
                    SensorCall();const int workaround_border = 5;
                    Rectangle aBounds( aInvRegion.GetBoundRect() );
                    aBounds.Left()      -= workaround_border;
                    aBounds.Top()       -= workaround_border;
                    aBounds.Right()     += workaround_border;
                    aBounds.Bottom()    += workaround_border;
                    aInvRegion = aBounds;
                }
                SensorCall();if ( !mpWindowImpl->mbNoParentUpdate && !(nFlags & SHOW_NOPARENTUPDATE) )
                {
                    SensorCall();if ( !aInvRegion.IsEmpty() )
                        {/*229*/SensorCall();ImplInvalidateParentFrameRegion( aInvRegion );/*230*/}
                }
                SensorCall();ImplGenerateMouseMove();
            }
        }
    }
    else
    {
        // inherit native widget flag for form controls
        // required here, because frames never show up in the child hierarchy - which should be fixed....
        // eg, the drop down of a combobox which is a system floating window
        SensorCall();if( mpWindowImpl->mbFrame && GetParent() && GetParent()->IsCompoundControl() &&
            GetParent()->IsNativeWidgetEnabled() != IsNativeWidgetEnabled() )
        {
            SensorCall();EnableNativeWidget( GetParent()->IsNativeWidgetEnabled() );
        }

        SensorCall();if ( mpWindowImpl->mbCallMove )
        {
            SensorCall();ImplCallMove();
        }
        SensorCall();if ( mpWindowImpl->mbCallResize )
        {
            SensorCall();ImplCallResize();
        }

        CompatStateChanged( StateChangedType::Visible );

        SensorCall();vcl::Window* pTestParent;
        SensorCall();if ( ImplIsOverlapWindow() )
            {/*231*/pTestParent = mpWindowImpl->mpOverlapWindow;/*232*/}
        else
            {/*233*/SensorCall();pTestParent = ImplGetParent();/*234*/}
        SensorCall();if ( mpWindowImpl->mbFrame || pTestParent->mpWindowImpl->mbReallyVisible )
        {
            // if a window becomes visible, send all child windows a StateChange,
            // such that these can initialise themselves
            SensorCall();ImplCallInitShow();

            // If it is a SystemWindow it automatically pops up on top of
            // all other windows if needed.
            SensorCall();if ( ImplIsOverlapWindow() && !(nFlags & SHOW_NOACTIVATE) )
            {
                SensorCall();ImplStartToTop(( nFlags & SHOW_FOREGROUNDTASK ) ? TOTOP_FOREGROUNDTASK : 0 );
                ImplFocusToTop( 0, false );
            }

            // save background
            SensorCall();if ( mpWindowImpl->mpOverlapData && mpWindowImpl->mpOverlapData->mbSaveBack )
                {/*235*/SensorCall();ImplSaveOverlapBackground();/*236*/}
            // adjust mpWindowImpl->mbReallyVisible
            SensorCall();bRealVisibilityChanged = !mpWindowImpl->mbReallyVisible;
            ImplSetReallyVisible();

            // assure clip rectangles will be recalculated
            ImplSetClipFlag();

            SensorCall();if ( !mpWindowImpl->mbFrame )
            {
                SensorCall();sal_uInt16 nInvalidateFlags = INVALIDATE_CHILDREN;
                SensorCall();if( ! IsPaintTransparent() )
                    nInvalidateFlags |= INVALIDATE_NOTRANSPARENT;
                SensorCall();ImplInvalidate( NULL, nInvalidateFlags );
                ImplGenerateMouseMove();
            }
        }

        SensorCall();if ( mpWindowImpl->mpBorderWindow )
            {/*237*/mpWindowImpl->mpBorderWindow->Show( true, nFlags );/*238*/}
        else {/*239*/SensorCall();if ( mpWindowImpl->mbFrame )
        {
            // #106431#, hide SplashScreen
            SensorCall();ImplSVData* pSVData = ImplGetSVData();
            SensorCall();if ( !pSVData->mpIntroWindow )
            {
                // The right way would be just to call this (not even in the 'if')
                SensorCall();GetpApp()->InitFinished();
            }
            else {/*241*/SensorCall();if ( !ImplIsWindowOrChild( pSVData->mpIntroWindow ) )
            {
                // ... but the VCL splash is broken, and it needs this
                // (for ./soffice .uno:NewDoc)
                pSVData->mpIntroWindow->Hide();
            ;/*242*/}}

            //DBG_ASSERT( !mpWindowImpl->mbSuppressAccessibilityEvents, "Window::Show() - Frame reactivated");
            SensorCall();mpWindowImpl->mbSuppressAccessibilityEvents = false;

            mpWindowImpl->mbPaintFrame = true;
            SensorCall();if (!Application::GetSettings().GetMiscSettings().GetPseudoHeadless())
            {
                SensorCall();bool bNoActivate = (nFlags & (SHOW_NOACTIVATE|SHOW_NOFOCUSCHANGE)) != 0;
                mpWindowImpl->mpFrame->Show( true, bNoActivate );
            }
            SensorCall();if( aDogTag.IsDead() )
                {/*243*/SensorCall();return;/*244*/}

            // Query the correct size of the window, if we are waiting for
            // a system resize
            SensorCall();if ( mpWindowImpl->mbWaitSystemResize )
            {
                SensorCall();long nOutWidth;
                long nOutHeight;
                mpWindowImpl->mpFrame->GetClientSize( nOutWidth, nOutHeight );
                ImplHandleResize( this, nOutWidth, nOutHeight );
            }

            if (mpWindowImpl->mpFrameData->mpBuffer && mpWindowImpl->mpFrameData->mpBuffer->GetOutputSizePixel() != GetOutputSizePixel())
                // Make sure that the buffer size matches the window size, even if no resize was needed.
                mpWindowImpl->mpFrameData->mpBuffer->SetOutputSizePixel(GetOutputSizePixel());
        ;/*240*/}}

        SensorCall();if( aDogTag.IsDead() )
            {/*245*/SensorCall();return;/*246*/}

#if OSL_DEBUG_LEVEL > 0
        if ( IsDialog() || (GetType() == WINDOW_TABPAGE) || (GetType() == WINDOW_DOCKINGWINDOW) )
        {
            DBG_DIALOGTEST( this );
        }
#endif

        SensorCall();ImplShowAllOverlaps();
    }

    SensorCall();if( aDogTag.IsDead() )
        {/*247*/SensorCall();return;/*248*/}
    // invalidate all saved backgrounds
    SensorCall();if ( mpWindowImpl->mpFrameData->mpFirstBackWin )
        {/*249*/SensorCall();ImplInvalidateAllOverlapBackgrounds();/*250*/}

    // the SHOW/HIDE events also serve as indicators to send child creation/destroy events to the access bridge
    // However, the access bridge only uses this event if the data member is not NULL (it's kind of a hack that
    // we re-use the SHOW/HIDE events this way, with this particular semantics).
    // Since #104887#, the notifications for the access bridge are done in Impl(Set|Reset)ReallyVisible. Here, we
    // now only notify with a NULL data pointer, for all other clients except the access bridge.
    SensorCall();if ( !bRealVisibilityChanged )
        {/*251*/SensorCall();CallEventListeners( mpWindowImpl->mbVisible ? VCLEVENT_WINDOW_SHOW : VCLEVENT_WINDOW_HIDE, NULL );/*252*/}
    SensorCall();if( aDogTag.IsDead() )
        {/*253*/SensorCall();return;/*254*/}

SensorCall();}

Size Window::GetSizePixel() const
{
    SensorCall();if (!mpWindowImpl)
    {
        SAL_WARN("vcl.layout", "WTF no windowimpl");
        {Size  ReplaceReturn = Size(0,0); SensorCall(); return ReplaceReturn;}
    }

    // #i43257# trigger pending resize handler to assure correct window sizes
    SensorCall();if( mpWindowImpl->mpFrameData->maResizeIdle.IsActive() )
    {
        SensorCall();ImplDelData aDogtag( const_cast<Window*>(this) );
        mpWindowImpl->mpFrameData->maResizeIdle.Stop();
        mpWindowImpl->mpFrameData->maResizeIdle.GetIdleHdl().Call( NULL );
        SensorCall();if( aDogtag.IsDead() )
            {/*255*/{Size  ReplaceReturn = Size(0,0); SensorCall(); return ReplaceReturn;}/*256*/}
    }

    {Size  ReplaceReturn = Size( mnOutWidth+mpWindowImpl->mnLeftBorder+mpWindowImpl->mnRightBorder,
                 mnOutHeight+mpWindowImpl->mnTopBorder+mpWindowImpl->mnBottomBorder ); SensorCall(); return ReplaceReturn;}
}

void Window::GetBorder( sal_Int32& rLeftBorder, sal_Int32& rTopBorder,
                               sal_Int32& rRightBorder, sal_Int32& rBottomBorder ) const
{
    SensorCall();rLeftBorder     = mpWindowImpl->mnLeftBorder;
    rTopBorder      = mpWindowImpl->mnTopBorder;
    rRightBorder    = mpWindowImpl->mnRightBorder;
    rBottomBorder   = mpWindowImpl->mnBottomBorder;
SensorCall();}

void Window::Enable( bool bEnable, bool bChild )
{
    SensorCall();if ( IsDisposed() )
        {/*257*/SensorCall();return;/*258*/}

    SensorCall();if ( !bEnable )
    {
        // the tracking mode will be stopped or the capture will be stolen
        // when a window is disabled,
        SensorCall();if ( IsTracking() )
            {/*259*/SensorCall();EndTracking( TrackingEventFlags::Cancel );/*260*/}
        SensorCall();if ( IsMouseCaptured() )
            {/*261*/SensorCall();ReleaseMouse();/*262*/}
        // try to pass focus to the next control
        // if the window has focus and is contained in the dialog control
        // mpWindowImpl->mbDisabled should only be set after a call of ImplDlgCtrlNextWindow().
        // Otherwise ImplDlgCtrlNextWindow() should be used
        SensorCall();if ( HasFocus() )
            {/*263*/SensorCall();ImplDlgCtrlNextWindow();/*264*/}
    }

    SensorCall();if ( mpWindowImpl->mpBorderWindow )
    {
        mpWindowImpl->mpBorderWindow->Enable( bEnable, false );
        if ( (mpWindowImpl->mpBorderWindow->GetType() == WINDOW_BORDERWINDOW) &&
             static_cast<ImplBorderWindow*>(mpWindowImpl->mpBorderWindow.get())->mpMenuBarWindow )
            static_cast<ImplBorderWindow*>(mpWindowImpl->mpBorderWindow.get())->mpMenuBarWindow->Enable( bEnable, true );
    }

    // #i56102# restore app focus win in case the
    // window was disabled when the frame focus changed
    SensorCall();ImplSVData* pSVData = ImplGetSVData();
    if( bEnable &&
        pSVData->maWinData.mpFocusWin == nullptr &&
        mpWindowImpl->mpFrameData->mbHasFocus &&
        mpWindowImpl->mpFrameData->mpFocusWin == this )
        pSVData->maWinData.mpFocusWin = this;

    SensorCall();if ( mpWindowImpl->mbDisabled != !bEnable )
    {
        SensorCall();mpWindowImpl->mbDisabled = !bEnable;
        SensorCall();if ( mpWindowImpl->mpSysObj )
            {/*265*/SensorCall();mpWindowImpl->mpSysObj->Enable( bEnable && !mpWindowImpl->mbInputDisabled );/*266*/}
        CompatStateChanged( StateChangedType::Enable );

        SensorCall();CallEventListeners( bEnable ? VCLEVENT_WINDOW_ENABLED : VCLEVENT_WINDOW_DISABLED );
    }

    SensorCall();if ( bChild || mpWindowImpl->mbChildNotify )
    {
        SensorCall();vcl::Window* pChild = mpWindowImpl->mpFirstChild;
        SensorCall();while ( pChild )
        {
            SensorCall();pChild->Enable( bEnable, bChild );
            pChild = pChild->mpWindowImpl->mpNext;
        }
    }

    SensorCall();if ( IsReallyVisible() )
        {/*267*/SensorCall();ImplGenerateMouseMove();/*268*/}
SensorCall();}

void Window::SetCallHandlersOnInputDisabled( bool bCall )
{
    SensorCall();mpWindowImpl->mbCallHandlersDuringInputDisabled = bCall;

    vcl::Window* pChild = mpWindowImpl->mpFirstChild;
    SensorCall();while ( pChild )
    {
        SensorCall();pChild->SetCallHandlersOnInputDisabled( bCall );
        pChild = pChild->mpWindowImpl->mpNext;
    }
SensorCall();}

bool Window::IsCallHandlersOnInputDisabled() const
{
    {const _Bool  ReplaceReturn = mpWindowImpl->mbCallHandlersDuringInputDisabled; SensorCall(); return ReplaceReturn;}
}

void Window::EnableInput( bool bEnable, bool bChild )
{

    SensorCall();bool bNotify = (bEnable != mpWindowImpl->mbInputDisabled);
    SensorCall();if ( mpWindowImpl->mpBorderWindow )
    {
        mpWindowImpl->mpBorderWindow->EnableInput( bEnable, false );
        if ( (mpWindowImpl->mpBorderWindow->GetType() == WINDOW_BORDERWINDOW) &&
             static_cast<ImplBorderWindow*>(mpWindowImpl->mpBorderWindow.get())->mpMenuBarWindow )
            static_cast<ImplBorderWindow*>(mpWindowImpl->mpBorderWindow.get())->mpMenuBarWindow->EnableInput( bEnable, true );
    }

    SensorCall();if ( (! bEnable && mpWindowImpl->meAlwaysInputMode != AlwaysInputEnabled) ||
         (  bEnable && mpWindowImpl->meAlwaysInputMode != AlwaysInputDisabled) )
    {
        // automatically stop the tracking mode or steal capture
        // if the window is disabled
        SensorCall();if ( !bEnable )
        {
            SensorCall();if ( IsTracking() )
                {/*269*/SensorCall();EndTracking( TrackingEventFlags::Cancel );/*270*/}
            SensorCall();if ( IsMouseCaptured() )
                {/*271*/SensorCall();ReleaseMouse();/*272*/}
        }

        SensorCall();if ( mpWindowImpl->mbInputDisabled != !bEnable )
        {
            SensorCall();mpWindowImpl->mbInputDisabled = !bEnable;
            SensorCall();if ( mpWindowImpl->mpSysObj )
                {/*273*/SensorCall();mpWindowImpl->mpSysObj->Enable( !mpWindowImpl->mbDisabled && bEnable );/*274*/}
        }
    }

    // #i56102# restore app focus win in case the
    // window was disabled when the frame focus changed
    SensorCall();ImplSVData* pSVData = ImplGetSVData();
    if( bEnable &&
        pSVData->maWinData.mpFocusWin == nullptr &&
        mpWindowImpl->mpFrameData->mbHasFocus &&
        mpWindowImpl->mpFrameData->mpFocusWin == this )
        pSVData->maWinData.mpFocusWin = this;

    SensorCall();if ( bChild || mpWindowImpl->mbChildNotify )
    {
        SensorCall();vcl::Window* pChild = mpWindowImpl->mpFirstChild;
        SensorCall();while ( pChild )
        {
            SensorCall();pChild->EnableInput( bEnable, bChild );
            pChild = pChild->mpWindowImpl->mpNext;
        }
    }

    SensorCall();if ( IsReallyVisible() )
        {/*275*/SensorCall();ImplGenerateMouseMove();/*276*/}

    // #104827# notify parent
    SensorCall();if ( bNotify )
    {
        SensorCall();NotifyEvent aNEvt( bEnable ? MouseNotifyEvent::INPUTENABLE : MouseNotifyEvent::INPUTDISABLE, this );
        CompatNotify( aNEvt );
    }
SensorCall();}

void Window::EnableInput( bool bEnable, bool bChild, bool bSysWin,
                          const vcl::Window* pExcludeWindow )
{

    SensorCall();EnableInput( bEnable, bChild );
    SensorCall();if ( bSysWin )
    {
        // pExcuteWindow is the first Overlap-Frame --> if this
        // shouldn't be the case, than this must be changed in dialog.cxx
        SensorCall();if( pExcludeWindow )
            {/*277*/SensorCall();pExcludeWindow = pExcludeWindow->ImplGetFirstOverlapWindow();/*278*/}
        SensorCall();vcl::Window* pSysWin = mpWindowImpl->mpFrameWindow->mpWindowImpl->mpFrameData->mpFirstOverlap;
        SensorCall();while ( pSysWin )
        {
            // Is Window in the path from this window
            SensorCall();if ( ImplGetFirstOverlapWindow()->ImplIsWindowOrChild( pSysWin, true ) )
            {
                // Is Window not in the exclude window path or not the
                // exclude window, than change the status
                SensorCall();if ( !pExcludeWindow || !pExcludeWindow->ImplIsWindowOrChild( pSysWin, true ) )
                    {/*279*/SensorCall();pSysWin->EnableInput( bEnable, bChild );/*280*/}
            }
            pSysWin = pSysWin->mpWindowImpl->mpNextOverlap;
        }

        // enable/disable floating system windows as well
        SensorCall();vcl::Window* pFrameWin = ImplGetSVData()->maWinData.mpFirstFrame;
        SensorCall();while ( pFrameWin )
        {
            SensorCall();if( pFrameWin->ImplIsFloatingWindow() )
            {
                // Is Window in the path from this window
                SensorCall();if ( ImplGetFirstOverlapWindow()->ImplIsWindowOrChild( pFrameWin, true ) )
                {
                    // Is Window not in the exclude window path or not the
                    // exclude window, than change the status
                    SensorCall();if ( !pExcludeWindow || !pExcludeWindow->ImplIsWindowOrChild( pFrameWin, true ) )
                        {/*281*/SensorCall();pFrameWin->EnableInput( bEnable, bChild );/*282*/}
                }
            }
            pFrameWin = pFrameWin->mpWindowImpl->mpFrameData->mpNextFrame;
        }

        // the same for ownerdraw floating windows
        SensorCall();if( mpWindowImpl->mbFrame )
        {
            SensorCall();::std::vector< VclPtr<vcl::Window> >& rList = mpWindowImpl->mpFrameData->maOwnerDrawList;
            auto p = rList.begin();
            while( p != rList.end() )
            {
                // Is Window in the path from this window
                if ( ImplGetFirstOverlapWindow()->ImplIsWindowOrChild( (*p), true ) )
                {
                    // Is Window not in the exclude window path or not the
                    // exclude window, than change the status
                    if ( !pExcludeWindow || !pExcludeWindow->ImplIsWindowOrChild( (*p), true ) )
                        (*p)->EnableInput( bEnable, bChild );
                }
                ++p;
            }
        }
    }
SensorCall();}

void Window::AlwaysEnableInput( bool bAlways, bool bChild )
{

    if ( mpWindowImpl->mpBorderWindow )
        mpWindowImpl->mpBorderWindow->AlwaysEnableInput( bAlways, false );

    SensorCall();if( bAlways && mpWindowImpl->meAlwaysInputMode != AlwaysInputEnabled )
    {
        SensorCall();mpWindowImpl->meAlwaysInputMode = AlwaysInputEnabled;

        SensorCall();if ( bAlways )
            {/*283*/SensorCall();EnableInput( true, false );/*284*/}
    }
    else {/*285*/SensorCall();if( ! bAlways && mpWindowImpl->meAlwaysInputMode == AlwaysInputEnabled )
    {
        SensorCall();mpWindowImpl->meAlwaysInputMode = AlwaysInputNone;
    ;/*286*/}}

    SensorCall();if ( bChild || mpWindowImpl->mbChildNotify )
    {
        SensorCall();vcl::Window* pChild = mpWindowImpl->mpFirstChild;
        SensorCall();while ( pChild )
        {
            SensorCall();pChild->AlwaysEnableInput( bAlways, bChild );
            pChild = pChild->mpWindowImpl->mpNext;
        }
    }
SensorCall();}

void Window::AlwaysDisableInput( bool bAlways, bool bChild )
{

    if ( mpWindowImpl->mpBorderWindow )
        mpWindowImpl->mpBorderWindow->AlwaysDisableInput( bAlways, false );

    SensorCall();if( bAlways && mpWindowImpl->meAlwaysInputMode != AlwaysInputDisabled )
    {
        SensorCall();mpWindowImpl->meAlwaysInputMode = AlwaysInputDisabled;

        SensorCall();if ( bAlways )
            {/*287*/SensorCall();EnableInput( false, false );/*288*/}
    }
    else {/*289*/SensorCall();if( ! bAlways && mpWindowImpl->meAlwaysInputMode == AlwaysInputDisabled )
    {
        SensorCall();mpWindowImpl->meAlwaysInputMode = AlwaysInputNone;
    ;/*290*/}}

    SensorCall();if ( bChild || mpWindowImpl->mbChildNotify )
    {
        SensorCall();vcl::Window* pChild = mpWindowImpl->mpFirstChild;
        SensorCall();while ( pChild )
        {
            SensorCall();pChild->AlwaysDisableInput( bAlways, bChild );
            pChild = pChild->mpWindowImpl->mpNext;
        }
    }
SensorCall();}

void Window::SetActivateMode( sal_uInt16 nMode )
{

    if ( mpWindowImpl->mpBorderWindow )
        mpWindowImpl->mpBorderWindow->SetActivateMode( nMode );

    SensorCall();if ( mpWindowImpl->mnActivateMode != nMode )
    {
        SensorCall();mpWindowImpl->mnActivateMode = nMode;

        // possibly trigger Decativate/Activate
        SensorCall();if ( mpWindowImpl->mnActivateMode )
        {
            SensorCall();if ( (mpWindowImpl->mbActive || (GetType() == WINDOW_BORDERWINDOW)) &&
                 !HasChildPathFocus( true ) )
            {
                SensorCall();mpWindowImpl->mbActive = false;
                Deactivate();
            }
        }
        else
        {
            SensorCall();if ( !mpWindowImpl->mbActive || (GetType() == WINDOW_BORDERWINDOW) )
            {
                SensorCall();mpWindowImpl->mbActive = true;
                Activate();
            }
        }
    }
SensorCall();}

void Window::setPosSizePixel( long nX, long nY,
                              long nWidth, long nHeight, PosSizeFlags nFlags )
{

    SensorCall();bool bHasValidSize = !mpWindowImpl->mbDefSize;

    SensorCall();if ( nFlags & PosSizeFlags::Pos )
        {/*291*/SensorCall();mpWindowImpl->mbDefPos = false;/*292*/}
    SensorCall();if ( nFlags & PosSizeFlags::Size )
        {/*293*/SensorCall();mpWindowImpl->mbDefSize = false;/*294*/}

    // The top BorderWindow is the window which is to be positioned
    SensorCall();vcl::Window* pWindow = this;
    while ( pWindow->mpWindowImpl->mpBorderWindow )
        pWindow = pWindow->mpWindowImpl->mpBorderWindow;

    SensorCall();if ( pWindow->mpWindowImpl->mbFrame )
    {
        // Note: if we're positioning a frame, the coordinates are interpreted
        // as being the top-left corner of the window's client area and NOT
        // as the position of the border ! (due to limitations of several UNIX window managers)
        SensorCall();long nOldWidth  = pWindow->mnOutWidth;

        SensorCall();if ( !(nFlags & PosSizeFlags::Width) )
            {/*295*/SensorCall();nWidth = pWindow->mnOutWidth;/*296*/}
        SensorCall();if ( !(nFlags & PosSizeFlags::Height) )
            {/*297*/SensorCall();nHeight = pWindow->mnOutHeight;/*298*/}

        SensorCall();sal_uInt16 nSysFlags=0;
        vcl::Window *pParent = GetParent();

        SensorCall();if( nFlags & PosSizeFlags::Width )
            nSysFlags |= SAL_FRAME_POSSIZE_WIDTH;
        SensorCall();if( nFlags & PosSizeFlags::Height )
            nSysFlags |= SAL_FRAME_POSSIZE_HEIGHT;
        SensorCall();if( nFlags & PosSizeFlags::ByDrag )
            nSysFlags |= SAL_FRAME_POSSIZE_BYDRAG;
        SensorCall();if( nFlags & PosSizeFlags::X )
        {
            SensorCall();nSysFlags |= SAL_FRAME_POSSIZE_X;
            SensorCall();if( pParent && (pWindow->GetStyle() & WB_SYSTEMCHILDWINDOW) )
            {
                SensorCall();nX += pParent->mnOutOffX;
            }
            SensorCall();if( pParent && pParent->ImplIsAntiparallel() )
            {
                // --- RTL --- (re-mirror at parent window)
                SensorCall();Rectangle aRect( Point ( nX, nY ), Size( nWidth, nHeight ) );
                const OutputDevice *pParentOutDev = pParent->GetOutDev();
                pParentOutDev->ReMirror( aRect );
                nX = aRect.Left();
            }
        }
        SensorCall();if( !(nFlags & PosSizeFlags::X) && bHasValidSize && pWindow->mpWindowImpl->mpFrame->maGeometry.nWidth )
        {
            // --- RTL ---  make sure the old right aligned position is not changed
            //              system windows will always grow to the right
            SensorCall();if ( pParent )
            {
                SensorCall();OutputDevice *pParentOutDev = pParent->GetOutDev();
                SensorCall();if( pParentOutDev->HasMirroredGraphics() )
                {
                    SensorCall();long myWidth = nOldWidth;
                    SensorCall();if( !myWidth )
                        {/*299*/SensorCall();myWidth = mpWindowImpl->mpFrame->GetUnmirroredGeometry().nWidth;/*300*/}
                    SensorCall();if( !myWidth )
                        {/*301*/SensorCall();myWidth = nWidth;/*302*/}
                    nFlags |= PosSizeFlags::X;
                    SensorCall();nSysFlags |= SAL_FRAME_POSSIZE_X;
                    nX = mpWindowImpl->mpFrame->GetUnmirroredGeometry().nX - pParent->mpWindowImpl->mpFrame->GetUnmirroredGeometry().nX -
                        mpWindowImpl->mpFrame->GetUnmirroredGeometry().nLeftDecoration;
                    nX = pParent->mpWindowImpl->mpFrame->GetUnmirroredGeometry().nX - mpWindowImpl->mpFrame->GetUnmirroredGeometry().nLeftDecoration +
                        pParent->mpWindowImpl->mpFrame->GetUnmirroredGeometry().nWidth - myWidth - 1 - mpWindowImpl->mpFrame->GetUnmirroredGeometry().nX;
                    SensorCall();if(!(nFlags & PosSizeFlags::Y))
                    {
                        nFlags |= PosSizeFlags::Y;
                        SensorCall();nSysFlags |= SAL_FRAME_POSSIZE_Y;
                        nY = mpWindowImpl->mpFrame->GetUnmirroredGeometry().nY - pWindow->GetParent()->mpWindowImpl->mpFrame->GetUnmirroredGeometry().nY -
                            mpWindowImpl->mpFrame->GetUnmirroredGeometry().nTopDecoration;
                    }
                }
            }
        }
        SensorCall();if( nFlags & PosSizeFlags::Y )
        {
            SensorCall();nSysFlags |= SAL_FRAME_POSSIZE_Y;
            SensorCall();if( pParent && (pWindow->GetStyle() & WB_SYSTEMCHILDWINDOW) )
            {
                SensorCall();nY += pParent->mnOutOffY;
            }
        }

        SensorCall();if( nSysFlags & (SAL_FRAME_POSSIZE_WIDTH|SAL_FRAME_POSSIZE_HEIGHT) )
        {
            // check for min/max client size and adjust size accordingly
            // otherwise it may happen that the resize event is ignored, i.e. the old size remains
            // unchanged but ImplHandleResize() is called with the wrong size
            SensorCall();SystemWindow *pSystemWindow = dynamic_cast< SystemWindow* >( pWindow );
            SensorCall();if( pSystemWindow )
            {
                SensorCall();Size aMinSize = pSystemWindow->GetMinOutputSizePixel();
                Size aMaxSize = pSystemWindow->GetMaxOutputSizePixel();
                SensorCall();if( nWidth < aMinSize.Width() )
                    {/*303*/SensorCall();nWidth = aMinSize.Width();/*304*/}
                SensorCall();if( nHeight < aMinSize.Height() )
                    {/*305*/SensorCall();nHeight = aMinSize.Height();/*306*/}

                SensorCall();if( nWidth > aMaxSize.Width() )
                    {/*307*/SensorCall();nWidth = aMaxSize.Width();/*308*/}
                SensorCall();if( nHeight > aMaxSize.Height() )
                    {/*309*/SensorCall();nHeight = aMaxSize.Height();/*310*/}
            }
        }

        SensorCall();pWindow->mpWindowImpl->mpFrame->SetPosSize( nX, nY, nWidth, nHeight, nSysFlags );

        // Resize should be called directly. If we havn't
        // set the correct size, we get a second resize from
        // the system with the correct size. This can be happened
        // if the size is to small or to large.
        ImplHandleResize( pWindow, nWidth, nHeight );
    }
    else
    {
        SensorCall();pWindow->ImplPosSizeWindow( nX, nY, nWidth, nHeight, nFlags );
        SensorCall();if ( IsReallyVisible() )
            {/*311*/SensorCall();ImplGenerateMouseMove();/*312*/}
    }
SensorCall();}

Point Window::GetPosPixel() const
{
    {Point  ReplaceReturn = mpWindowImpl->maPos; SensorCall(); return ReplaceReturn;}
}

Rectangle Window::GetDesktopRectPixel() const
{
    SensorCall();Rectangle rRect;
    mpWindowImpl->mpFrameWindow->mpWindowImpl->mpFrame->GetWorkArea( rRect );
    {Rectangle  ReplaceReturn = rRect; SensorCall(); return ReplaceReturn;}
}

Point Window::OutputToScreenPixel( const Point& rPos ) const
{
    // relative to top level parent
    {Point  ReplaceReturn = Point( rPos.X()+mnOutOffX, rPos.Y()+mnOutOffY ); SensorCall(); return ReplaceReturn;}
}

Point Window::ScreenToOutputPixel( const Point& rPos ) const
{
    // relative to top level parent
    {Point  ReplaceReturn = Point( rPos.X()-mnOutOffX, rPos.Y()-mnOutOffY ); SensorCall(); return ReplaceReturn;}
}

long Window::ImplGetUnmirroredOutOffX()
{
    // revert mnOutOffX changes that were potentially made in ImplPosSizeWindow
    SensorCall();long offx = mnOutOffX;
    OutputDevice *pOutDev = GetOutDev();
    SensorCall();if( pOutDev->HasMirroredGraphics() )
    {
        SensorCall();if( mpWindowImpl->mpParent && !mpWindowImpl->mpParent->mpWindowImpl->mbFrame && mpWindowImpl->mpParent->ImplIsAntiparallel() )
        {
            if ( !ImplIsOverlapWindow() )
                offx -= mpWindowImpl->mpParent->mnOutOffX;

            offx = mpWindowImpl->mpParent->mnOutWidth - mnOutWidth - offx;

            if ( !ImplIsOverlapWindow() )
                offx += mpWindowImpl->mpParent->mnOutOffX;

        }
    }
    {long  ReplaceReturn = offx; SensorCall(); return ReplaceReturn;}
}

// normalized screen pixel are independent of mirroring
Point Window::OutputToNormalizedScreenPixel( const Point& rPos ) const
{
    // relative to top level parent
    SensorCall();long offx = const_cast<vcl::Window*>(this)->ImplGetUnmirroredOutOffX();
    {Point  ReplaceReturn = Point( rPos.X()+offx, rPos.Y()+mnOutOffY ); SensorCall(); return ReplaceReturn;}
}

Point Window::NormalizedScreenToOutputPixel( const Point& rPos ) const
{
    // relative to top level parent
    SensorCall();long offx = const_cast<vcl::Window*>(this)->ImplGetUnmirroredOutOffX();
    {Point  ReplaceReturn = Point( rPos.X()-offx, rPos.Y()-mnOutOffY ); SensorCall(); return ReplaceReturn;}
}

Point Window::OutputToAbsoluteScreenPixel( const Point& rPos ) const
{
    // relative to the screen
    SensorCall();Point p = OutputToScreenPixel( rPos );
    SalFrameGeometry g = mpWindowImpl->mpFrame->GetGeometry();
    p.X() += g.nX;
    p.Y() += g.nY;
    {Point  ReplaceReturn = p; SensorCall(); return ReplaceReturn;}
}

Point Window::AbsoluteScreenToOutputPixel( const Point& rPos ) const
{
    // relative to the screen
    SensorCall();Point p = ScreenToOutputPixel( rPos );
    SalFrameGeometry g = mpWindowImpl->mpFrame->GetGeometry();
    p.X() -= g.nX;
    p.Y() -= g.nY;
    {Point  ReplaceReturn = p; SensorCall(); return ReplaceReturn;}
}

Rectangle Window::ImplOutputToUnmirroredAbsoluteScreenPixel( const Rectangle &rRect ) const
{
    // this method creates unmirrored screen coordinates to be compared with the desktop
    // and is used for positioning of RTL popup windows correctly on the screen
    SensorCall();SalFrameGeometry g = mpWindowImpl->mpFrame->GetUnmirroredGeometry();

    Point p1 = OutputToScreenPixel( rRect.TopRight() );
    p1.X() = g.nX+g.nWidth-p1.X();
    p1.Y() += g.nY;

    Point p2 = OutputToScreenPixel( rRect.BottomLeft() );
    p2.X() = g.nX+g.nWidth-p2.X();
    p2.Y() += g.nY;

    {Rectangle  ReplaceReturn = Rectangle( p1, p2 ); SensorCall(); return ReplaceReturn;}
}

Rectangle Window::GetWindowExtentsRelative( vcl::Window *pRelativeWindow ) const
{
    // with decoration
    {Rectangle  ReplaceReturn = ImplGetWindowExtentsRelative( pRelativeWindow, false ); SensorCall(); return ReplaceReturn;}
}

Rectangle Window::GetClientWindowExtentsRelative( vcl::Window *pRelativeWindow ) const
{
    // without decoration
    {Rectangle  ReplaceReturn = ImplGetWindowExtentsRelative( pRelativeWindow, true ); SensorCall(); return ReplaceReturn;}
}

Rectangle Window::ImplGetWindowExtentsRelative( vcl::Window *pRelativeWindow, bool bClientOnly ) const
{
    SensorCall();SalFrameGeometry g = mpWindowImpl->mpFrame->GetGeometry();
    // make sure we use the extent of our border window,
    // otherwise we miss a few pixels
    const vcl::Window *pWin = (!bClientOnly && mpWindowImpl->mpBorderWindow) ? mpWindowImpl->mpBorderWindow : this;

    Point aPos( pWin->OutputToScreenPixel( Point(0,0) ) );
    aPos.X() += g.nX;
    aPos.Y() += g.nY;
    Size aSize ( pWin->GetSizePixel() );
    // #104088# do not add decoration to the workwindow to be compatible to java accessibility api
    SensorCall();if( !bClientOnly && (mpWindowImpl->mbFrame || (mpWindowImpl->mpBorderWindow && mpWindowImpl->mpBorderWindow->mpWindowImpl->mbFrame && GetType() != WINDOW_WORKWINDOW)) )
    {
        SensorCall();aPos.X() -= g.nLeftDecoration;
        aPos.Y() -= g.nTopDecoration;
        aSize.Width() += g.nLeftDecoration + g.nRightDecoration;
        aSize.Height() += g.nTopDecoration + g.nBottomDecoration;
    }
    SensorCall();if( pRelativeWindow )
    {
        // #106399# express coordinates relative to borderwindow
        SensorCall();vcl::Window *pRelWin = (!bClientOnly && pRelativeWindow->mpWindowImpl->mpBorderWindow) ? pRelativeWindow->mpWindowImpl->mpBorderWindow.get() : pRelativeWindow;
        aPos = pRelWin->AbsoluteScreenToOutputPixel( aPos );
    }
    {Rectangle  ReplaceReturn = Rectangle( aPos, aSize ); SensorCall(); return ReplaceReturn;}
}

void Window::Scroll( long nHorzScroll, long nVertScroll, sal_uInt16 nFlags )
{

    SensorCall();ImplScroll( Rectangle( Point( mnOutOffX, mnOutOffY ),
                           Size( mnOutWidth, mnOutHeight ) ),
                nHorzScroll, nVertScroll, nFlags & ~SCROLL_CLIP );
SensorCall();}

void Window::Scroll( long nHorzScroll, long nVertScroll,
                     const Rectangle& rRect, sal_uInt16 nFlags )
{
    SensorCall();OutputDevice *pOutDev = GetOutDev();
    Rectangle aRect = pOutDev->ImplLogicToDevicePixel( rRect );
    aRect.Intersection( Rectangle( Point( mnOutOffX, mnOutOffY ), Size( mnOutWidth, mnOutHeight ) ) );
    SensorCall();if ( !aRect.IsEmpty() )
        {/*313*/SensorCall();ImplScroll( aRect, nHorzScroll, nVertScroll, nFlags );/*314*/}
SensorCall();}

void Window::Flush()
{

    SensorCall();const Rectangle aWinRect( Point( mnOutOffX, mnOutOffY ), Size( mnOutWidth, mnOutHeight ) );
    mpWindowImpl->mpFrame->Flush( aWinRect );
SensorCall();}

void Window::Sync()
{

    SensorCall();mpWindowImpl->mpFrame->Sync();
SensorCall();}

void Window::SetUpdateMode( bool bUpdate )
{
    SensorCall();mpWindowImpl->mbNoUpdate = !bUpdate;
    CompatStateChanged( StateChangedType::UpdateMode );
SensorCall();}

void Window::GrabFocus()
{
    SensorCall();ImplGrabFocus( 0 );
SensorCall();}

bool Window::HasFocus() const
{
SensorCall();    return (this == ImplGetSVData()->maWinData.mpFocusWin);
SensorCall();}

void Window::GrabFocusToDocument()
{
    SensorCall();ImplGrabFocusToDocument(0);
SensorCall();}

void Window::SetFakeFocus( bool bFocus )
{
    SensorCall();ImplGetWindowImpl()->mbFakeFocusSet = bFocus;
SensorCall();}

bool Window::HasChildPathFocus( bool bSystemWindow ) const
{

    SensorCall();vcl::Window* pFocusWin = ImplGetSVData()->maWinData.mpFocusWin;
    SensorCall();if ( pFocusWin )
        {/*315*/{const _Bool  ReplaceReturn = ImplIsWindowOrChild( pFocusWin, bSystemWindow ); SensorCall(); return ReplaceReturn;}/*316*/}
    {const _Bool  ReplaceReturn = false; SensorCall(); return ReplaceReturn;}
}

void Window::SetCursor( vcl::Cursor* pCursor )
{

    SensorCall();if ( mpWindowImpl->mpCursor != pCursor )
    {
        SensorCall();if ( mpWindowImpl->mpCursor )
            {/*317*/SensorCall();mpWindowImpl->mpCursor->ImplHide( true );/*318*/}
        SensorCall();mpWindowImpl->mpCursor = pCursor;
        SensorCall();if ( pCursor )
            {/*319*/SensorCall();pCursor->ImplShow();/*320*/}
    }
SensorCall();}

void Window::SetText( const OUString& rStr )
{
    SensorCall();if (!mpWindowImpl || rStr == mpWindowImpl->maText)
        {/*321*/SensorCall();return;/*322*/}

    SensorCall();OUString oldTitle( mpWindowImpl->maText );
    mpWindowImpl->maText = rStr;

    SensorCall();if ( mpWindowImpl->mpBorderWindow )
        {/*323*/mpWindowImpl->mpBorderWindow->SetText( rStr );/*324*/}
    else {/*325*/SensorCall();if ( mpWindowImpl->mbFrame )
        {/*327*/SensorCall();mpWindowImpl->mpFrame->SetTitle( rStr );/*328*/}/*326*/}

    SensorCall();CallEventListeners( VCLEVENT_WINDOW_FRAMETITLECHANGED, &oldTitle );

    // #107247# needed for accessibility
    // The VCLEVENT_WINDOW_FRAMETITLECHANGED is (mis)used to notify accessible name changes.
    // Therefore a window, which is labeled by this window, must also notify an accessible
    // name change.
    SensorCall();if ( IsReallyVisible() )
    {
        SensorCall();vcl::Window* pWindow = GetAccessibleRelationLabelFor();
        SensorCall();if ( pWindow && pWindow != this )
            {/*329*/SensorCall();pWindow->CallEventListeners( VCLEVENT_WINDOW_FRAMETITLECHANGED, &oldTitle );/*330*/}
    }

    CompatStateChanged( StateChangedType::Text );
SensorCall();}

OUString Window::GetText() const
{

    {rtl::OUString  ReplaceReturn = mpWindowImpl->maText; SensorCall(); return ReplaceReturn;}
}

OUString Window::GetDisplayText() const
{

    {rtl::OUString  ReplaceReturn = GetText(); SensorCall(); return ReplaceReturn;}
}

const Wallpaper& Window::GetDisplayBackground() const
{
    // FIXME: fix issue 52349, need to fix this really in
    // all NWF enabled controls
    SensorCall();const ToolBox* pTB = dynamic_cast<const ToolBox*>(this);
    SensorCall();if( pTB )
    {
        SensorCall();if( IsNativeWidgetEnabled() )
            {/*331*/{const class Wallpaper & ReplaceReturn = pTB->ImplGetToolBoxPrivateData()->maDisplayBackground; SensorCall(); return ReplaceReturn;}/*332*/}
    }

    SensorCall();if( !IsBackground() )
    {
        if( mpWindowImpl->mpParent )
            return mpWindowImpl->mpParent->GetDisplayBackground();
    }

    SensorCall();const Wallpaper& rBack = GetBackground();
    if( ! rBack.IsBitmap() &&
        ! rBack.IsGradient() &&
        rBack.GetColor().GetColor() == COL_TRANSPARENT &&
        mpWindowImpl->mpParent )
            return mpWindowImpl->mpParent->GetDisplayBackground();
    {const class Wallpaper & ReplaceReturn = rBack; SensorCall(); return ReplaceReturn;}
}

const OUString& Window::GetHelpText() const
{
    SensorCall();OUString aStrHelpId( OStringToOUString( GetHelpId(), RTL_TEXTENCODING_UTF8 ) );
    bool bStrHelpId = !aStrHelpId.isEmpty();

    SensorCall();if ( !mpWindowImpl->maHelpText.getLength() && bStrHelpId )
    {
        SensorCall();if ( !IsDialog() && (mpWindowImpl->mnType != WINDOW_TABPAGE) && (mpWindowImpl->mnType != WINDOW_FLOATINGWINDOW) )
        {
            SensorCall();Help* pHelp = Application::GetHelp();
            SensorCall();if ( pHelp )
            {
                SensorCall();mpWindowImpl->maHelpText = pHelp->GetHelpText(aStrHelpId, this);
                mpWindowImpl->mbHelpTextDynamic = false;
            }
        }
    }
    else {/*333*/SensorCall();if( mpWindowImpl->mbHelpTextDynamic && bStrHelpId )
    {
        SensorCall();static const char* pEnv = getenv( "HELP_DEBUG" );
        SensorCall();if( pEnv && *pEnv )
        {
            SensorCall();OUStringBuffer aTxt( 64+mpWindowImpl->maHelpText.getLength() );
            aTxt.append( mpWindowImpl->maHelpText );
            aTxt.appendAscii( "\n------------------\n" );
            aTxt.append( OUString( aStrHelpId ) );
            mpWindowImpl->maHelpText = aTxt.makeStringAndClear();
        }
        SensorCall();mpWindowImpl->mbHelpTextDynamic = false;
    ;/*334*/}}

    {const class rtl::OUString & ReplaceReturn = mpWindowImpl->maHelpText; SensorCall(); return ReplaceReturn;}
}

void Window::SetWindowPeer( Reference< css::awt::XWindowPeer > xPeer, VCLXWindow* pVCLXWindow  )
{
    // be safe against re-entrance: first clear the old ref, then assign the new one
    mpWindowImpl->mxWindowPeer.clear();
    mpWindowImpl->mxWindowPeer = xPeer;

    SensorCall();mpWindowImpl->mpVCLXWindow = pVCLXWindow;
SensorCall();}

Reference< css::awt::XWindowPeer > Window::GetComponentInterface( bool bCreate )
{
    SensorCall();if ( !mpWindowImpl->mxWindowPeer.is() && bCreate )
    {
        SensorCall();UnoWrapperBase* pWrapper = Application::GetUnoWrapper();
        if ( pWrapper )
            mpWindowImpl->mxWindowPeer = pWrapper->GetWindowInterface( this, true );
    }
    return mpWindowImpl->mxWindowPeer;
SensorCall();}

void Window::SetComponentInterface( Reference< css::awt::XWindowPeer > xIFace )
{
    SensorCall();UnoWrapperBase* pWrapper = Application::GetUnoWrapper();
    DBG_ASSERT( pWrapper, "SetComponentInterface: No Wrapper!" );
    if ( pWrapper )
        pWrapper->SetWindowInterface( this, xIFace );
}

void Window::ImplCallDeactivateListeners( vcl::Window *pNew )
{
    // no deactivation if the newly activated window is my child
    SensorCall();if ( !pNew || !ImplIsChild( pNew ) )
    {
        SensorCall();ImplDelData aDogtag( this );
        CallEventListeners( VCLEVENT_WINDOW_DEACTIVATE );
        SensorCall();if( aDogtag.IsDead() )
            {/*335*/SensorCall();return;/*336*/}

        // #100759#, avoid walking the wrong frame's hierarchy
        //           eg, undocked docking windows (ImplDockFloatWin)
        SensorCall();if ( ImplGetParent() && mpWindowImpl->mpFrameWindow == ImplGetParent()->mpWindowImpl->mpFrameWindow )
            {/*337*/SensorCall();ImplGetParent()->ImplCallDeactivateListeners( pNew );/*338*/}
    }
SensorCall();}

void Window::ImplCallActivateListeners( vcl::Window *pOld )
{
    // no activation if the old active window is my child
    SensorCall();if ( !pOld || !ImplIsChild( pOld ) )
    {
        SensorCall();ImplDelData aDogtag( this );
        CallEventListeners( VCLEVENT_WINDOW_ACTIVATE, pOld );
        SensorCall();if( aDogtag.IsDead() )
            {/*339*/SensorCall();return;/*340*/}

        SensorCall();if ( ImplGetParent() )
            {/*341*/SensorCall();ImplGetParent()->ImplCallActivateListeners( pOld );/*342*/}
        else {/*343*/SensorCall();if( (mpWindowImpl->mnStyle & WB_INTROWIN) == 0 )
        {
            // top level frame reached: store hint for DefModalDialogParent
            ImplGetSVData()->maWinData.mpActiveApplicationFrame = mpWindowImpl->mpFrameWindow;
        ;/*344*/}}
    }
SensorCall();}

Reference< XClipboard > Window::GetClipboard()
{

    SensorCall();if( mpWindowImpl->mpFrameData )
    {
        SensorCall();if( ! mpWindowImpl->mpFrameData->mxClipboard.is() )
        {
            SensorCall();try
            {
                mpWindowImpl->mpFrameData->mxClipboard
                    = css::datatransfer::clipboard::SystemClipboard::create(
                        comphelper::getProcessComponentContext());
            }
            catch (DeploymentException & e)
            {
                SAL_WARN(
                    "vcl.window",
                    "ignoring DeploymentException \"" << e.Message << "\"");
            }
        }

        return mpWindowImpl->mpFrameData->mxClipboard;
    }

    return static_cast < XClipboard * > (0);
SensorCall();}

Reference< XClipboard > Window::GetPrimarySelection()
{

    SensorCall();if( mpWindowImpl->mpFrameData )
    {
        SensorCall();if( ! mpWindowImpl->mpFrameData->mxSelection.is() )
        {
            SensorCall();try
            {
                SensorCall();Reference< XComponentContext > xContext( comphelper::getProcessComponentContext() );

#if HAVE_FEATURE_X11
                // A hack, making the primary selection available as an instance
                // of the SystemClipboard service on X11:
                Sequence< Any > args(1);
                args[0] <<= OUString("PRIMARY");
                mpWindowImpl->mpFrameData->mxSelection.set(
                    (xContext->getServiceManager()->
                     createInstanceWithArgumentsAndContext(
                         "com.sun.star.datatransfer.clipboard.SystemClipboard",
                         args, xContext)),
                    UNO_QUERY_THROW);
#else
                static Reference< XClipboard > s_xSelection(
                    xContext->getServiceManager()->createInstanceWithContext( "com.sun.star.datatransfer.clipboard.GenericClipboard", xContext ), UNO_QUERY );

                mpWindowImpl->mpFrameData->mxSelection = s_xSelection;
#endif
            }
            catch (RuntimeException & e)
            {
                SAL_WARN(
                    "vcl.window",
                    "ignoring RuntimeException \"" << e.Message << "\"");
            }
        }

        return mpWindowImpl->mpFrameData->mxSelection;
    }

    return static_cast < XClipboard * > (0);
SensorCall();}

void Window::RecordLayoutData( vcl::ControlLayoutData* pLayout, const Rectangle& rRect )
{
SensorCall();    assert(mpOutDevData);
    mpOutDevData->mpRecordLayout = pLayout;
    mpOutDevData->maRecordRect = rRect;
    Paint(*this, rRect);
    mpOutDevData->mpRecordLayout = NULL;
SensorCall();}

void Window::DrawSelectionBackground( const Rectangle& rRect, sal_uInt16 highlight, bool bChecked, bool bDrawBorder, bool bDrawExtBorderOnly )
{
    SensorCall();DrawSelectionBackground( rRect, highlight, bChecked, bDrawBorder, bDrawExtBorderOnly, 0, NULL, NULL );
SensorCall();}

void Window::DrawSelectionBackground( const Rectangle& rRect, sal_uInt16 highlight, bool bChecked, bool bDrawBorder, bool bDrawExtBorderOnly, Color* pSelectionTextColor )
{
    SensorCall();DrawSelectionBackground( rRect, highlight, bChecked, bDrawBorder, bDrawExtBorderOnly, 0, pSelectionTextColor, NULL );
SensorCall();}

void Window::DrawSelectionBackground( const Rectangle& rRect,
                                      sal_uInt16 highlight,
                                      bool bChecked,
                                      bool bDrawBorder,
                                      bool bDrawExtBorderOnly,
                                      long nCornerRadius,
                                      Color* pSelectionTextColor,
                                      Color* pPaintColor
                                      )
{
    SensorCall();if( rRect.IsEmpty() )
        {/*345*/SensorCall();return;/*346*/}

    SensorCall();bool bRoundEdges = nCornerRadius > 0;

    const StyleSettings& rStyles = GetSettings().GetStyleSettings();

    // colors used for item highlighting
    Color aSelectionBorderCol( pPaintColor ? *pPaintColor : rStyles.GetHighlightColor() );
    Color aSelectionFillCol( aSelectionBorderCol );

    bool bDark = rStyles.GetFaceColor().IsDark();
    bool bBright = ( rStyles.GetFaceColor() == Color( COL_WHITE ) );

    int c1 = aSelectionBorderCol.GetLuminance();
    int c2 = GetDisplayBackground().GetColor().GetLuminance();

    SensorCall();if( !bDark && !bBright && abs( c2-c1 ) < (pPaintColor ? 40 : 75) )
    {
        // constrast too low
        SensorCall();sal_uInt16 h,s,b;
        aSelectionFillCol.RGBtoHSB( h, s, b );
        SensorCall();if( b > 50 )    {/*347*/SensorCall();b -= 40;/*348*/}
        else            {/*349*/SensorCall();b += 40;/*350*/}
        SensorCall();aSelectionFillCol.SetColor( Color::HSBtoRGB( h, s, b ) );
        aSelectionBorderCol = aSelectionFillCol;
    }

    SensorCall();if( bRoundEdges )
    {
        SensorCall();if( aSelectionBorderCol.IsDark() )
            {/*351*/SensorCall();aSelectionBorderCol.IncreaseLuminance( 128 );/*352*/}
        else
            {/*353*/SensorCall();aSelectionBorderCol.DecreaseLuminance( 128 );/*354*/}
    }

    SensorCall();Rectangle aRect( rRect );
    SensorCall();if( bDrawExtBorderOnly )
    {
        SensorCall();--aRect.Left();
        --aRect.Top();
        ++aRect.Right();
        ++aRect.Bottom();
    }
    SensorCall();Color oldFillCol = GetFillColor();
    Color oldLineCol = GetLineColor();

    if( bDrawBorder )
        SetLineColor( bDark ? Color(COL_WHITE) : ( bBright ? Color(COL_BLACK) : aSelectionBorderCol ) );
    else
        SetLineColor();

    sal_uInt16 nPercent = 0;
    SensorCall();if( !highlight )
    {
        SensorCall();if( bDark )
            aSelectionFillCol = COL_BLACK;
        else
            {/*355*/SensorCall();nPercent = 80;/*356*/}  // just checked (light)
    }
    else
    {
        SensorCall();if( bChecked && highlight == 2 )
        {
            SensorCall();if( bDark )
                aSelectionFillCol = COL_LIGHTGRAY;
            else {/*357*/SensorCall();if ( bBright )
            {
                SensorCall();aSelectionFillCol = COL_BLACK;
                SetLineColor( COL_BLACK );
                nPercent = 0;
            }
            else
                {/*359*/SensorCall();nPercent = bRoundEdges ? 40 : 20;/*360*/}/*358*/}          // selected, pressed or checked ( very dark )
        }
        else {/*361*/SensorCall();if( bChecked || highlight == 1 )
        {
            SensorCall();if( bDark )
                aSelectionFillCol = COL_GRAY;
            else {/*363*/SensorCall();if ( bBright )
            {
                SensorCall();aSelectionFillCol = COL_BLACK;
                SetLineColor( COL_BLACK );
                nPercent = 0;
            }
            else
                {/*365*/SensorCall();nPercent = bRoundEdges ? 60 : 35;/*366*/}/*364*/}          // selected, pressed or checked ( very dark )
        }
        else
        {
            SensorCall();if( bDark )
                aSelectionFillCol = COL_LIGHTGRAY;
            else {/*367*/SensorCall();if ( bBright )
            {
                SensorCall();aSelectionFillCol = COL_BLACK;
                SetLineColor( COL_BLACK );
                SensorCall();if( highlight == 3 )
                    {/*369*/SensorCall();nPercent = 80;/*370*/}
                else
                    {/*371*/SensorCall();nPercent = 0;/*372*/}
            }
            else
                {/*373*/SensorCall();nPercent = 70;/*374*/}/*368*/}          // selected ( dark )
        ;/*362*/}}
    }

    SensorCall();if( bDark && bDrawExtBorderOnly )
    {
        SetFillColor();
        SensorCall();if( pSelectionTextColor )
            {/*375*/SensorCall();*pSelectionTextColor = rStyles.GetHighlightTextColor();/*376*/}
    }
    else
    {
        SetFillColor( aSelectionFillCol );
        SensorCall();if( pSelectionTextColor )
        {
            SensorCall();Color aTextColor = IsControlBackground() ? GetControlForeground() : rStyles.GetButtonTextColor();
            Color aHLTextColor = rStyles.GetHighlightTextColor();
            int nTextDiff = abs(aSelectionFillCol.GetLuminance() - aTextColor.GetLuminance());
            int nHLDiff = abs(aSelectionFillCol.GetLuminance() - aHLTextColor.GetLuminance());
            *pSelectionTextColor = (nHLDiff >= nTextDiff) ? aHLTextColor : aTextColor;
        }
    }

    SensorCall();if( bDark )
    {
        DrawRect( aRect );
    }
    else
    {
        SensorCall();if( bRoundEdges )
        {
            SensorCall();Polygon aPoly( aRect, nCornerRadius, nCornerRadius );
            tools::PolyPolygon aPolyPoly( aPoly );
            DrawTransparent( aPolyPoly, nPercent );
        }
        else
        {
            SensorCall();Polygon aPoly( aRect );
            tools::PolyPolygon aPolyPoly( aPoly );
            DrawTransparent( aPolyPoly, nPercent );
        }
    }

    SetFillColor( oldFillCol );
    SetLineColor( oldLineCol );
SensorCall();}

// controls should return the window that gets the
// focus by default, so keyevents can be sent to that window directly
vcl::Window* Window::GetPreferredKeyInputWindow()
{
    {vcl::Window * ReplaceReturn = this; SensorCall(); return ReplaceReturn;}
}

bool Window::IsScrollable() const
{
    // check for scrollbars
    SensorCall();vcl::Window *pChild = mpWindowImpl->mpFirstChild;
    SensorCall();while( pChild )
    {
        SensorCall();if( pChild->GetType() == WINDOW_SCROLLBAR )
            {/*377*/{const _Bool  ReplaceReturn = true; SensorCall(); return ReplaceReturn;}/*378*/}
        else
            {/*379*/pChild = pChild->mpWindowImpl->mpNext;/*380*/}
    }
    {const _Bool  ReplaceReturn = false; SensorCall(); return ReplaceReturn;}
}

void Window::ImplMirrorFramePos( Point &pt ) const
{
    SensorCall();pt.X() = mpWindowImpl->mpFrame->maGeometry.nWidth-1-pt.X();
SensorCall();}

// frame based modal counter (dialogs are not modal to the whole application anymore)
bool Window::IsInModalMode() const
{
SensorCall();    return (mpWindowImpl->mpFrameWindow->mpWindowImpl->mpFrameData->mnModalMode != 0);
SensorCall();}

bool Window::IsInModalNonRefMode() const
{
    SensorCall();if(mpWindowImpl->mnStyle & WB_REFMODE)
        {/*381*/{const _Bool  ReplaceReturn = false; SensorCall(); return ReplaceReturn;}/*382*/}

    {const _Bool  ReplaceReturn = IsInModalMode(); SensorCall(); return ReplaceReturn;}
}

void Window::ImplIncModalCount()
{
    SensorCall();vcl::Window* pFrameWindow = mpWindowImpl->mpFrameWindow;
    vcl::Window* pParent = pFrameWindow;
    SensorCall();while( pFrameWindow )
    {
        SensorCall();pFrameWindow->mpWindowImpl->mpFrameData->mnModalMode++;
        while( pParent && pParent->mpWindowImpl->mpFrameWindow == pFrameWindow )
        {
            pParent = pParent->GetParent();
        }
        pFrameWindow = pParent ? pParent->mpWindowImpl->mpFrameWindow.get() : NULL;
    }
SensorCall();}
void Window::ImplDecModalCount()
{
    SensorCall();vcl::Window* pFrameWindow = mpWindowImpl->mpFrameWindow;
    vcl::Window* pParent = pFrameWindow;
    SensorCall();while( pFrameWindow )
    {
        SensorCall();pFrameWindow->mpWindowImpl->mpFrameData->mnModalMode--;
        while( pParent && pParent->mpWindowImpl->mpFrameWindow == pFrameWindow )
        {
            pParent = pParent->GetParent();
        }
        pFrameWindow = pParent ? pParent->mpWindowImpl->mpFrameWindow.get() : NULL;
    }
SensorCall();}

void Window::ImplIsInTaskPaneList( bool mbIsInTaskList )
{
    SensorCall();mpWindowImpl->mbIsInTaskPaneList = mbIsInTaskList;
SensorCall();}

void Window::ImplNotifyIconifiedState( bool bIconified )
{
SensorCall();    mpWindowImpl->mpFrameWindow->CallEventListeners( bIconified ? VCLEVENT_WINDOW_MINIMIZE : VCLEVENT_WINDOW_NORMALIZE );
    // #109206# notify client window as well to have toolkit topwindow listeners notified
    if( mpWindowImpl->mpFrameWindow->mpWindowImpl->mpClientWindow && mpWindowImpl->mpFrameWindow != mpWindowImpl->mpFrameWindow->mpWindowImpl->mpClientWindow )
        mpWindowImpl->mpFrameWindow->mpWindowImpl->mpClientWindow->CallEventListeners( bIconified ? VCLEVENT_WINDOW_MINIMIZE : VCLEVENT_WINDOW_NORMALIZE );
SensorCall();}

bool Window::HasActiveChildFrame()
{
    SensorCall();bool bRet = false;
    vcl::Window *pFrameWin = ImplGetSVData()->maWinData.mpFirstFrame;
    SensorCall();while( pFrameWin )
    {
        SensorCall();if( pFrameWin != mpWindowImpl->mpFrameWindow )
        {
            SensorCall();bool bDecorated = false;
            vcl::Window *pChildFrame = pFrameWin->ImplGetWindow();
            // #i15285# unfortunately WB_MOVEABLE is the same as WB_TABSTOP which can
            // be removed for ToolBoxes to influence the keyboard accessibility
            // thus WB_MOVEABLE is no indicator for decoration anymore
            // but FloatingWindows carry this information in their TitleType...
            // TODO: avoid duplicate WinBits !!!
            SensorCall();if( pChildFrame && pChildFrame->ImplIsFloatingWindow() )
                {/*383*/SensorCall();bDecorated = static_cast<FloatingWindow*>(pChildFrame)->GetTitleType() != FloatWinTitleType::NONE;/*384*/}
            SensorCall();if( bDecorated || (pFrameWin->mpWindowImpl->mnStyle & (WB_MOVEABLE | WB_SIZEABLE) ) )
                {/*385*/SensorCall();if( pChildFrame && pChildFrame->IsVisible() && pChildFrame->IsActive() )
                {
                    SensorCall();if( ImplIsChild( pChildFrame, true ) )
                    {
                        SensorCall();bRet = true;
                        SensorCall();break;
                    }
                ;/*386*/}}
        }
        pFrameWin = pFrameWin->mpWindowImpl->mpFrameData->mpNextFrame;
    }
    {_Bool  ReplaceReturn = bRet; SensorCall(); return ReplaceReturn;}
}

LanguageType Window::GetInputLanguage() const
{
    {const LanguageType  ReplaceReturn = mpWindowImpl->mpFrame->GetInputLanguage(); SensorCall(); return ReplaceReturn;}
}

void Window::EnableNativeWidget( bool bEnable )
{
    SensorCall();static const char* pNoNWF = getenv( "SAL_NO_NWF" );
    SensorCall();if( pNoNWF && *pNoNWF )
        {/*387*/SensorCall();bEnable = false;/*388*/}

    SensorCall();if( bEnable != ImplGetWinData()->mbEnableNativeWidget )
    {
        SensorCall();ImplGetWinData()->mbEnableNativeWidget = bEnable;

        // send datachanged event to allow for internal changes required for NWF
        // like clipmode, transparency, etc.
        DataChangedEvent aDCEvt( DataChangedEventType::SETTINGS, mxSettings.get(), AllSettingsFlags::STYLE );
        CompatDataChanged( aDCEvt );

        // sometimes the borderwindow is queried, so keep it in sync
        if( mpWindowImpl->mpBorderWindow )
            mpWindowImpl->mpBorderWindow->ImplGetWinData()->mbEnableNativeWidget = bEnable;
    }

    // push down, useful for compound controls
    SensorCall();vcl::Window *pChild = mpWindowImpl->mpFirstChild;
    SensorCall();while( pChild )
    {
        SensorCall();pChild->EnableNativeWidget( bEnable );
        pChild = pChild->mpWindowImpl->mpNext;
    }
SensorCall();}

bool Window::IsNativeWidgetEnabled() const
{
    {const _Bool  ReplaceReturn = ImplGetWinData()->mbEnableNativeWidget; SensorCall(); return ReplaceReturn;}
}

Reference< css::rendering::XCanvas > Window::ImplGetCanvas( const Size& rFullscreenSize,
                                                       bool        bFullscreen,
                                                       bool        bSpriteCanvas ) const
{
    // try to retrieve hard reference from weak member
    SensorCall();Reference< css::rendering::XCanvas > xCanvas( mpWindowImpl->mxCanvas );

    // canvas still valid? Then we're done.
    if( xCanvas.is() )
        return xCanvas;

    Sequence< Any > aArg(6);

    // Feed any with operating system's window handle

    // common: first any is VCL pointer to window (for VCL canvas)
    aArg[ 0 ] = makeAny( reinterpret_cast<sal_Int64>(this) );

    // TODO(Q1): Make GetSystemData method virtual

    // check whether we're a SysChild: have to fetch system data
    // directly from SystemChildWindow, because the GetSystemData
    // method is unfortunately not virtual
    const SystemChildWindow* pSysChild = dynamic_cast< const SystemChildWindow* >( this );
    SensorCall();if( pSysChild )
    {
        aArg[ 1 ] = pSysChild->GetSystemDataAny();
        aArg[ 5 ] = pSysChild->GetSystemGfxDataAny();
    }
    else
    {
        aArg[ 1 ] = GetSystemDataAny();
        aArg[ 5 ] = GetSystemGfxDataAny();
    }

    if( bFullscreen )
        aArg[ 2 ] = makeAny( css::awt::Rectangle( 0, 0,
                                    rFullscreenSize.Width(),
                                    rFullscreenSize.Height() ) );
    else
        aArg[ 2 ] = makeAny( css::awt::Rectangle( mnOutOffX, mnOutOffY, mnOutWidth, mnOutHeight ) );

    aArg[ 3 ] = makeAny( mpWindowImpl->mbAlwaysOnTop );
    aArg[ 4 ] = makeAny( Reference< css::awt::XWindow >(
                             const_cast<vcl::Window*>(this)->GetComponentInterface(),
                             UNO_QUERY ));

    SensorCall();Reference< XComponentContext > xContext = comphelper::getProcessComponentContext();

    // Create canvas instance with window handle

    static vcl::DeleteUnoReferenceOnDeinit<XMultiComponentFactory> xStaticCanvasFactory(
        css::rendering::CanvasFactory::create( xContext ) );
    Reference<XMultiComponentFactory> xCanvasFactory(xStaticCanvasFactory.get());

    SensorCall();if(xCanvasFactory.is())
    {
#ifdef WNT
        // see #140456# - if we're running on a multiscreen setup,
        // request special, multi-screen safe sprite canvas
        // implementation (not DX5 canvas, as it cannot cope with
        // surfaces spanning multiple displays). Note: canvas
        // (without sprite) stays the same)
        const sal_uInt32 nDisplay = static_cast< WinSalFrame* >( mpWindowImpl->mpFrame )->mnDisplay;
        if( (nDisplay >= Application::GetScreenCount()) )
        {
            xCanvas.set( xCanvasFactory->createInstanceWithArgumentsAndContext(
                                 bSpriteCanvas ?
                                 OUString( "com.sun.star.rendering.SpriteCanvas.MultiScreen" ) :
                                 OUString( "com.sun.star.rendering.Canvas.MultiScreen" ),
                                 aArg,
                                 xContext ),
                             UNO_QUERY );

        }
        else
        {
#endif
            xCanvas.set( xCanvasFactory->createInstanceWithArgumentsAndContext(
                             bSpriteCanvas ?
                             OUString( "com.sun.star.rendering.SpriteCanvas" ) :
                             OUString( "com.sun.star.rendering.Canvas" ),
                             aArg,
                             xContext ),
                         UNO_QUERY );

#ifdef WNT
        }
#endif
        mpWindowImpl->mxCanvas = xCanvas;
    }

    // no factory??? Empty reference, then.
    return xCanvas;
SensorCall();}

Reference< css::rendering::XCanvas > Window::GetCanvas() const
{
SensorCall();    return ImplGetCanvas( Size(), false, false );
SensorCall();}

Reference< css::rendering::XSpriteCanvas > Window::GetSpriteCanvas() const
{
    SensorCall();Reference< css::rendering::XSpriteCanvas > xSpriteCanvas(
        ImplGetCanvas( Size(), false, true ), UNO_QUERY );
    return xSpriteCanvas;
SensorCall();}

OUString Window::GetSurroundingText() const
{
  {rtl::OUString  ReplaceReturn = OUString(); SensorCall(); return ReplaceReturn;}
}

Selection Window::GetSurroundingTextSelection() const
{
  {Selection  ReplaceReturn = Selection( 0, 0 ); SensorCall(); return ReplaceReturn;}
}

bool Window::UsePolyPolygonForComplexGradient()
{
    SensorCall();if ( meRasterOp != ROP_OVERPAINT )
        {/*389*/{_Bool  ReplaceReturn = true; SensorCall(); return ReplaceReturn;}/*390*/}

    {_Bool  ReplaceReturn = false; SensorCall(); return ReplaceReturn;}
}

void Window::ApplySettings(vcl::RenderContext& /*rRenderContext*/)
{
SensorCall();SensorCall();}

void Window::DrawGradientWallpaper(vcl::RenderContext& rRenderContext,
                                   long nX, long nY, long nWidth, long nHeight,
                                   const Wallpaper& rWallpaper)
{
    SensorCall();Rectangle aBound;
    GDIMetaFile* pOldMetaFile = mpMetaFile;
    const bool bOldMap = mbMap;
    bool bNeedGradient = true;

    aBound = Rectangle(Point(nX, nY), Size(nWidth, nHeight));

    mpMetaFile = NULL;
    rRenderContext.EnableMapMode(false);
    rRenderContext.Push(PushFlags::CLIPREGION);
    rRenderContext.IntersectClipRegion(Rectangle(Point(nX, nY), Size(nWidth, nHeight)));

    SensorCall();if (rWallpaper.GetStyle() == WALLPAPER_APPLICATIONGRADIENT)
    {
        // limit gradient to useful size, so that it still can be noticed
        // in maximized windows
        SensorCall();long gradientWidth = GetDesktopRectPixel().GetSize().Width();
        SensorCall();if (gradientWidth > 1024)
            {/*391*/SensorCall();gradientWidth = 1024;/*392*/}
        SensorCall();if (mnOutOffX + nWidth > gradientWidth)
            {/*393*/SensorCall();rRenderContext.DrawColorWallpaper(nX, nY, nWidth, nHeight, rWallpaper.GetGradient().GetEndColor());/*394*/}
        SensorCall();if (mnOutOffX > gradientWidth)
            {/*395*/SensorCall();bNeedGradient = false;/*396*/}
        else
            {/*397*/SensorCall();aBound = Rectangle(Point(-mnOutOffX, nY), Size(gradientWidth, nHeight));/*398*/}
    }

    SensorCall();if (bNeedGradient)
        {/*399*/SensorCall();rRenderContext.DrawGradient(aBound, rWallpaper.GetGradient());/*400*/}

    SensorCall();rRenderContext.Pop();
    rRenderContext.EnableMapMode(bOldMap);
    mpMetaFile = pOldMetaFile;
SensorCall();}

const SystemEnvData* Window::GetSystemData() const
{

    {const struct SystemEnvData * ReplaceReturn = mpWindowImpl->mpFrame ? mpWindowImpl->mpFrame->GetSystemData() : NULL; SensorCall(); return ReplaceReturn;}
}

Any Window::GetSystemDataAny() const
{
    SensorCall();Any aRet;
    const SystemEnvData* pSysData = GetSystemData();
    SensorCall();if( pSysData )
    {
        SensorCall();Sequence< sal_Int8 > aSeq( reinterpret_cast<sal_Int8 const *>(pSysData), pSysData->nSize );
        aRet <<= aSeq;
    }
    {com::sun::star::uno::Any  ReplaceReturn = aRet; SensorCall(); return ReplaceReturn;}
}

vcl::RenderSettings& Window::GetRenderSettings()
{
    {vcl::RenderSettings & ReplaceReturn = mpWindowImpl->maRenderSettings; SensorCall(); return ReplaceReturn;}
}

bool Window::SupportsDoubleBuffering() const
{
    {const _Bool  ReplaceReturn = mpWindowImpl->mbDoubleBuffering; SensorCall(); return ReplaceReturn;}
}

void Window::SetDoubleBuffering(bool bDoubleBuffering)
{
    SensorCall();mpWindowImpl->mbDoubleBuffering = bDoubleBuffering;
SensorCall();}

/*
 * The rational here is that we moved destructors to
 * dispose and this altered a lot of code paths, that
 * are better left unchanged for now.
 */
#define COMPAT_BODY(method,args) \
    if (!mpWindowImpl || mpWindowImpl->mbInDispose) \
        Window::method args; \
    else \
        method args;

void Window::CompatGetFocus()
{
SensorCall();    COMPAT_BODY(GetFocus,())
}

void Window::CompatLoseFocus()
{
SensorCall();    COMPAT_BODY(LoseFocus,())
}

void Window::CompatStateChanged( StateChangedType nStateChange )
{
SensorCall();    COMPAT_BODY(StateChanged,(nStateChange))
}

void Window::CompatDataChanged( const DataChangedEvent& rDCEvt )
{
SensorCall();    COMPAT_BODY(DataChanged,(rDCEvt))
}

bool Window::CompatPreNotify( NotifyEvent& rNEvt )
{
    SensorCall();if (!mpWindowImpl || mpWindowImpl->mbInDispose)
        {/*417*/{_Bool  ReplaceReturn = Window::PreNotify( rNEvt ); SensorCall(); return ReplaceReturn;}/*418*/}
    else
        {/*419*/{_Bool  ReplaceReturn = PreNotify( rNEvt ); SensorCall(); return ReplaceReturn;}/*420*/}
SensorCall();}

bool Window::CompatNotify( NotifyEvent& rNEvt )
{
    SensorCall();if (!mpWindowImpl || mpWindowImpl->mbInDispose)
        {/*421*/{_Bool  ReplaceReturn = Window::Notify( rNEvt ); SensorCall(); return ReplaceReturn;}/*422*/}
    else
        {/*423*/{_Bool  ReplaceReturn = Notify( rNEvt ); SensorCall(); return ReplaceReturn;}/*424*/}
SensorCall();}

} /* namespace vcl */

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
