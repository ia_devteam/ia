/*-------------------------------------------------------------------------
 *
 * fmgr.c
 *	  The Postgres function manager.
 *
 * Portions Copyright (c) 1996-2015, PostgreSQL Global Development Group
 * Portions Copyright (c) 1994, Regents of the University of California
 *
 *
 * IDENTIFICATION
 *	  src/backend/utils/fmgr/fmgr.c
 *
 *-------------------------------------------------------------------------
 */

#include "postgres.h"
#include "var/tmp/sensor.h"

#include "access/tuptoaster.h"
#include "catalog/pg_language.h"
#include "catalog/pg_proc.h"
#include "executor/functions.h"
#include "executor/spi.h"
#include "lib/stringinfo.h"
#include "miscadmin.h"
#include "nodes/nodeFuncs.h"
#include "pgstat.h"
#include "utils/acl.h"
#include "utils/builtins.h"
#include "utils/fmgrtab.h"
#include "utils/guc.h"
#include "utils/lsyscache.h"
#include "utils/syscache.h"

/*
 * Hooks for function calls
 */
PGDLLIMPORT needs_fmgr_hook_type needs_fmgr_hook = NULL;
PGDLLIMPORT fmgr_hook_type fmgr_hook = NULL;

/*
 * Declaration for old-style function pointer type.  This is now used only
 * in fmgr_oldstyle() and is no longer exported.
 *
 * The m68k SVR4 ABI defines that pointers are returned in %a0 instead of
 * %d0. So if a function pointer is declared to return a pointer, the
 * compiler may look only into %a0, but if the called function was declared
 * to return an integer type, it puts its value only into %d0. So the
 * caller doesn't pick up the correct return value. The solution is to
 * declare the function pointer to return int, so the compiler picks up the
 * return value from %d0. (Functions returning pointers put their value
 * *additionally* into %d0 for compatibility.) The price is that there are
 * some warnings about int->pointer conversions ... which we can suppress
 * with suitably ugly casts in fmgr_oldstyle().
 */
#if (defined(__mc68000__) || (defined(__m68k__))) && defined(__ELF__)
typedef int32 (*func_ptr) ();
#else
typedef char *(*func_ptr) ();
#endif

/*
 * For an oldstyle function, fn_extra points to a record like this:
 */
typedef struct
{
	func_ptr	func;			/* Address of the oldstyle function */
	bool		arg_toastable[FUNC_MAX_ARGS];	/* is n'th arg of a toastable
												 * datatype? */
} Oldstyle_fnextra;

/*
 * Hashtable for fast lookup of external C functions
 */
typedef struct
{
	/* fn_oid is the hash key and so must be first! */
	Oid			fn_oid;			/* OID of an external C function */
	TransactionId fn_xmin;		/* for checking up-to-dateness */
	ItemPointerData fn_tid;
	PGFunction	user_fn;		/* the function's address */
	const Pg_finfo_record *inforec;		/* address of its info record */
} CFuncHashTabEntry;

static HTAB *CFuncHash = NULL;


static void fmgr_info_cxt_security(Oid functionId, FmgrInfo *finfo, MemoryContext mcxt,
					   bool ignore_security);
static void fmgr_info_C_lang(Oid functionId, FmgrInfo *finfo, HeapTuple procedureTuple);
static void fmgr_info_other_lang(Oid functionId, FmgrInfo *finfo, HeapTuple procedureTuple);
static CFuncHashTabEntry *lookup_C_func(HeapTuple procedureTuple);
static void record_C_func(HeapTuple procedureTuple,
			  PGFunction user_fn, const Pg_finfo_record *inforec);
static Datum fmgr_oldstyle(PG_FUNCTION_ARGS);
static Datum fmgr_security_definer(PG_FUNCTION_ARGS);


/*
 * Lookup routines for builtin-function table.  We can search by either Oid
 * or name, but search by Oid is much faster.
 */

static const FmgrBuiltin *
fmgr_isbuiltin(Oid id)
{
	Din_Go(1,2048);int			low = 0;
	int			high = fmgr_nbuiltins - 1;

	/*
	 * Loop invariant: low is the first index that could contain target entry,
	 * and high is the last index that could contain it.
	 */
	Din_Go(8,2048);while (low <= high)
	{
		Din_Go(2,2048);int			i = (high + low) / 2;
		const FmgrBuiltin *ptr = &fmgr_builtins[i];

		Din_Go(7,2048);if (id == ptr->foid)
			{/*55*/{const FmgrBuiltin * ReplaceReturn67 = ptr; Din_Go(3,2048); return ReplaceReturn67;}/*56*/}
		else {/*57*/Din_Go(4,2048);if (id > ptr->foid)
			{/*59*/Din_Go(5,2048);low = i + 1;/*60*/}
		else
			{/*61*/Din_Go(6,2048);high = i - 1;/*62*/}/*58*/}
	}
	{const FmgrBuiltin * ReplaceReturn66 = NULL; Din_Go(9,2048); return ReplaceReturn66;}
}

/*
 * Lookup a builtin by name.  Note there can be more than one entry in
 * the array with the same name, but they should all point to the same
 * routine.
 */
static const FmgrBuiltin *
fmgr_lookupByName(const char *name)
{
	Din_Go(10,2048);int			i;

	Din_Go(13,2048);for (i = 0; i < fmgr_nbuiltins; i++)
	{
		Din_Go(11,2048);if (strcmp(name, fmgr_builtins[i].funcName) == 0)
			{/*63*/{const FmgrBuiltin * ReplaceReturn65 = fmgr_builtins + i; Din_Go(12,2048); return ReplaceReturn65;}/*64*/}
	}
	{const FmgrBuiltin * ReplaceReturn64 = NULL; Din_Go(14,2048); return ReplaceReturn64;}
}

/*
 * This routine fills a FmgrInfo struct, given the OID
 * of the function to be called.
 *
 * The caller's CurrentMemoryContext is used as the fn_mcxt of the info
 * struct; this means that any subsidiary data attached to the info struct
 * (either by fmgr_info itself, or later on by a function call handler)
 * will be allocated in that context.  The caller must ensure that this
 * context is at least as long-lived as the info struct itself.  This is
 * not a problem in typical cases where the info struct is on the stack or
 * in freshly-palloc'd space.  However, if one intends to store an info
 * struct in a long-lived table, it's better to use fmgr_info_cxt.
 */
void
fmgr_info(Oid functionId, FmgrInfo *finfo)
{
	Din_Go(15,2048);fmgr_info_cxt_security(functionId, finfo, CurrentMemoryContext, false);
Din_Go(16,2048);}

/*
 * Fill a FmgrInfo struct, specifying a memory context in which its
 * subsidiary data should go.
 */
void
fmgr_info_cxt(Oid functionId, FmgrInfo *finfo, MemoryContext mcxt)
{
	Din_Go(17,2048);fmgr_info_cxt_security(functionId, finfo, mcxt, false);
Din_Go(18,2048);}

/*
 * This one does the actual work.  ignore_security is ordinarily false
 * but is set to true when we need to avoid recursion.
 */
static void
fmgr_info_cxt_security(Oid functionId, FmgrInfo *finfo, MemoryContext mcxt,
					   bool ignore_security)
{
	Din_Go(19,2048);const FmgrBuiltin *fbp;
	HeapTuple	procedureTuple;
	Form_pg_proc procedureStruct;
	Datum		prosrcdatum;
	bool		isnull;
	char	   *prosrc;

	/*
	 * fn_oid *must* be filled in last.  Some code assumes that if fn_oid is
	 * valid, the whole struct is valid.  Some FmgrInfo struct's do survive
	 * elogs.
	 */
	finfo->fn_oid = InvalidOid;
	finfo->fn_extra = NULL;
	finfo->fn_mcxt = mcxt;
	finfo->fn_expr = NULL;		/* caller may set this later */

	Din_Go(22,2048);if ((fbp = fmgr_isbuiltin(functionId)) != NULL)
	{
		/*
		 * Fast path for builtin functions: don't bother consulting pg_proc
		 */
		Din_Go(20,2048);finfo->fn_nargs = fbp->nargs;
		finfo->fn_strict = fbp->strict;
		finfo->fn_retset = fbp->retset;
		finfo->fn_stats = TRACK_FUNC_ALL;		/* ie, never track */
		finfo->fn_addr = fbp->func;
		finfo->fn_oid = functionId;
		Din_Go(21,2048);return;
	}

	/* Otherwise we need the pg_proc entry */
	Din_Go(23,2048);procedureTuple = SearchSysCache1(PROCOID, ObjectIdGetDatum(functionId));
	Din_Go(24,2048);if (!HeapTupleIsValid(procedureTuple))
		elog(ERROR, "cache lookup failed for function %u", functionId);
	Din_Go(25,2048);procedureStruct = (Form_pg_proc) GETSTRUCT(procedureTuple);

	finfo->fn_nargs = procedureStruct->pronargs;
	finfo->fn_strict = procedureStruct->proisstrict;
	finfo->fn_retset = procedureStruct->proretset;

	/*
	 * If it has prosecdef set, non-null proconfig, or if a plugin wants to
	 * hook function entry/exit, use fmgr_security_definer call handler ---
	 * unless we are being called again by fmgr_security_definer or
	 * fmgr_info_other_lang.
	 *
	 * When using fmgr_security_definer, function stats tracking is always
	 * disabled at the outer level, and instead we set the flag properly in
	 * fmgr_security_definer's private flinfo and implement the tracking
	 * inside fmgr_security_definer.  This loses the ability to charge the
	 * overhead of fmgr_security_definer to the function, but gains the
	 * ability to set the track_functions GUC as a local GUC parameter of an
	 * interesting function and have the right things happen.
	 */
	Din_Go(28,2048);if (!ignore_security &&
		(procedureStruct->prosecdef ||
		 !heap_attisnull(procedureTuple, Anum_pg_proc_proconfig) ||
		 FmgrHookIsNeeded(functionId)))
	{
		Din_Go(26,2048);finfo->fn_addr = fmgr_security_definer;
		finfo->fn_stats = TRACK_FUNC_ALL;		/* ie, never track */
		finfo->fn_oid = functionId;
		ReleaseSysCache(procedureTuple);
		Din_Go(27,2048);return;
	}

	Din_Go(41,2048);switch (procedureStruct->prolang)
	{
		case INTERNALlanguageId:

			/*
			 * For an ordinary builtin function, we should never get here
			 * because the isbuiltin() search above will have succeeded.
			 * However, if the user has done a CREATE FUNCTION to create an
			 * alias for a builtin function, we can end up here.  In that case
			 * we have to look up the function by name.  The name of the
			 * internal function is stored in prosrc (it doesn't have to be
			 * the same as the name of the alias!)
			 */
			Din_Go(29,2048);prosrcdatum = SysCacheGetAttr(PROCOID, procedureTuple,
										  Anum_pg_proc_prosrc, &isnull);
			Din_Go(30,2048);if (isnull)
				elog(ERROR, "null prosrc");
			Din_Go(31,2048);prosrc = TextDatumGetCString(prosrcdatum);
			fbp = fmgr_lookupByName(prosrc);
			Din_Go(32,2048);if (fbp == NULL)
				ereport(ERROR,
						(errcode(ERRCODE_UNDEFINED_FUNCTION),
						 errmsg("internal function \"%s\" is not in internal lookup table",
								prosrc)));
			Din_Go(33,2048);pfree(prosrc);
			/* Should we check that nargs, strict, retset match the table? */
			finfo->fn_addr = fbp->func;
			/* note this policy is also assumed in fast path above */
			finfo->fn_stats = TRACK_FUNC_ALL;	/* ie, never track */
			Din_Go(34,2048);break;

		case ClanguageId:
			Din_Go(35,2048);fmgr_info_C_lang(functionId, finfo, procedureTuple);
			finfo->fn_stats = TRACK_FUNC_PL;	/* ie, track if ALL */
			Din_Go(36,2048);break;

		case SQLlanguageId:
			Din_Go(37,2048);finfo->fn_addr = fmgr_sql;
			finfo->fn_stats = TRACK_FUNC_PL;	/* ie, track if ALL */
			Din_Go(38,2048);break;

		default:
			Din_Go(39,2048);fmgr_info_other_lang(functionId, finfo, procedureTuple);
			finfo->fn_stats = TRACK_FUNC_OFF;	/* ie, track if not OFF */
			Din_Go(40,2048);break;
	}

	Din_Go(42,2048);finfo->fn_oid = functionId;
	ReleaseSysCache(procedureTuple);
Din_Go(43,2048);}

/*
 * Special fmgr_info processing for C-language functions.  Note that
 * finfo->fn_oid is not valid yet.
 */
static void
fmgr_info_C_lang(Oid functionId, FmgrInfo *finfo, HeapTuple procedureTuple)
{
	Din_Go(44,2048);Form_pg_proc procedureStruct = (Form_pg_proc) GETSTRUCT(procedureTuple);
	CFuncHashTabEntry *hashentry;
	PGFunction	user_fn;
	const Pg_finfo_record *inforec;
	Oldstyle_fnextra *fnextra;
	bool		isnull;
	int			i;

	/*
	 * See if we have the function address cached already
	 */
	hashentry = lookup_C_func(procedureTuple);
	Din_Go(51,2048);if (hashentry)
	{
		Din_Go(45,2048);user_fn = hashentry->user_fn;
		inforec = hashentry->inforec;
	}
	else
	{
		Din_Go(46,2048);Datum		prosrcattr,
					probinattr;
		char	   *prosrcstring,
				   *probinstring;
		void	   *libraryhandle;

		/*
		 * Get prosrc and probin strings (link symbol and library filename).
		 * While in general these columns might be null, that's not allowed
		 * for C-language functions.
		 */
		prosrcattr = SysCacheGetAttr(PROCOID, procedureTuple,
									 Anum_pg_proc_prosrc, &isnull);
		Din_Go(47,2048);if (isnull)
			elog(ERROR, "null prosrc for C function %u", functionId);
		Din_Go(48,2048);prosrcstring = TextDatumGetCString(prosrcattr);

		probinattr = SysCacheGetAttr(PROCOID, procedureTuple,
									 Anum_pg_proc_probin, &isnull);
		Din_Go(49,2048);if (isnull)
			elog(ERROR, "null probin for C function %u", functionId);
		Din_Go(50,2048);probinstring = TextDatumGetCString(probinattr);

		/* Look up the function itself */
		user_fn = load_external_function(probinstring, prosrcstring, true,
										 &libraryhandle);

		/* Get the function information record (real or default) */
		inforec = fetch_finfo_record(libraryhandle, prosrcstring);

		/* Cache the addresses for later calls */
		record_C_func(procedureTuple, user_fn, inforec);

		pfree(prosrcstring);
		pfree(probinstring);
	}

	Din_Go(59,2048);switch (inforec->api_version)
	{
		case 0:
			/* Old style: need to use a handler */
			Din_Go(52,2048);finfo->fn_addr = fmgr_oldstyle;
			fnextra = (Oldstyle_fnextra *)
				MemoryContextAllocZero(finfo->fn_mcxt,
									   sizeof(Oldstyle_fnextra));
			finfo->fn_extra = (void *) fnextra;
			fnextra->func = (func_ptr) user_fn;
			Din_Go(54,2048);for (i = 0; i < procedureStruct->pronargs; i++)
			{
				Din_Go(53,2048);fnextra->arg_toastable[i] =
					TypeIsToastable(procedureStruct->proargtypes.values[i]);
			}
			Din_Go(55,2048);break;
		case 1:
			/* New style: call directly */
			Din_Go(56,2048);finfo->fn_addr = user_fn;
			Din_Go(57,2048);break;
		default:
			/* Shouldn't get here if fetch_finfo_record did its job */
			elog(ERROR, "unrecognized function API version: %d",
				 inforec->api_version);
			Din_Go(58,2048);break;
	}
Din_Go(60,2048);}

/*
 * Special fmgr_info processing for other-language functions.  Note
 * that finfo->fn_oid is not valid yet.
 */
static void
fmgr_info_other_lang(Oid functionId, FmgrInfo *finfo, HeapTuple procedureTuple)
{
	Din_Go(61,2048);Form_pg_proc procedureStruct = (Form_pg_proc) GETSTRUCT(procedureTuple);
	Oid			language = procedureStruct->prolang;
	HeapTuple	languageTuple;
	Form_pg_language languageStruct;
	FmgrInfo	plfinfo;

	languageTuple = SearchSysCache1(LANGOID, ObjectIdGetDatum(language));
	Din_Go(62,2048);if (!HeapTupleIsValid(languageTuple))
		elog(ERROR, "cache lookup failed for language %u", language);
	Din_Go(63,2048);languageStruct = (Form_pg_language) GETSTRUCT(languageTuple);

	/*
	 * Look up the language's call handler function, ignoring any attributes
	 * that would normally cause insertion of fmgr_security_definer.  We need
	 * to get back a bare pointer to the actual C-language function.
	 */
	fmgr_info_cxt_security(languageStruct->lanplcallfoid, &plfinfo,
						   CurrentMemoryContext, true);
	finfo->fn_addr = plfinfo.fn_addr;

	/*
	 * If lookup of the PL handler function produced nonnull fn_extra,
	 * complain --- it must be an oldstyle function! We no longer support
	 * oldstyle PL handlers.
	 */
	Din_Go(64,2048);if (plfinfo.fn_extra != NULL)
		elog(ERROR, "language %u has old-style handler", language);

	Din_Go(65,2048);ReleaseSysCache(languageTuple);
Din_Go(66,2048);}

/*
 * Fetch and validate the information record for the given external function.
 * The function is specified by a handle for the containing library
 * (obtained from load_external_function) as well as the function name.
 *
 * If no info function exists for the given name, it is not an error.
 * Instead we return a default info record for a version-0 function.
 * We want to raise an error here only if the info function returns
 * something bogus.
 *
 * This function is broken out of fmgr_info_C_lang so that fmgr_c_validator
 * can validate the information record for a function not yet entered into
 * pg_proc.
 */
const Pg_finfo_record *
fetch_finfo_record(void *filehandle, char *funcname)
{
	Din_Go(67,2048);char	   *infofuncname;
	PGFInfoFunction infofunc;
	const Pg_finfo_record *inforec;
	static Pg_finfo_record default_inforec = {0};

	infofuncname = psprintf("pg_finfo_%s", funcname);

	/* Try to look up the info function */
	infofunc = (PGFInfoFunction) lookup_external_function(filehandle,
														  infofuncname);
	Din_Go(70,2048);if (infofunc == NULL)
	{
		/* Not found --- assume version 0 */
		Din_Go(68,2048);pfree(infofuncname);
		{const Pg_finfo_record * ReplaceReturn63 = &default_inforec; Din_Go(69,2048); return ReplaceReturn63;}
	}

	/* Found, so call it */
	Din_Go(71,2048);inforec = (*infofunc) ();

	/* Validate result as best we can */
	Din_Go(72,2048);if (inforec == NULL)
		elog(ERROR, "null result from info function \"%s\"", infofuncname);
	Din_Go(75,2048);switch (inforec->api_version)
	{
		case 0:
		case 1:
			/* OK, no additional fields to validate */
			Din_Go(73,2048);break;
		default:
			ereport(ERROR,
					(errcode(ERRCODE_INVALID_PARAMETER_VALUE),
					 errmsg("unrecognized API version %d reported by info function \"%s\"",
							inforec->api_version, infofuncname)));
			Din_Go(74,2048);break;
	}

	Din_Go(76,2048);pfree(infofuncname);
	{const Pg_finfo_record * ReplaceReturn62 = inforec; Din_Go(77,2048); return ReplaceReturn62;}
}


/*-------------------------------------------------------------------------
 *		Routines for caching lookup information for external C functions.
 *
 * The routines in dfmgr.c are relatively slow, so we try to avoid running
 * them more than once per external function per session.  We use a hash table
 * with the function OID as the lookup key.
 *-------------------------------------------------------------------------
 */

/*
 * lookup_C_func: try to find a C function in the hash table
 *
 * If an entry exists and is up to date, return it; else return NULL
 */
static CFuncHashTabEntry *
lookup_C_func(HeapTuple procedureTuple)
{
	Din_Go(78,2048);Oid			fn_oid = HeapTupleGetOid(procedureTuple);
	CFuncHashTabEntry *entry;

	Din_Go(79,2048);if (CFuncHash == NULL)
		return NULL;			/* no table yet */
	Din_Go(80,2048);entry = (CFuncHashTabEntry *)
		hash_search(CFuncHash,
					&fn_oid,
					HASH_FIND,
					NULL);
	Din_Go(81,2048);if (entry == NULL)
		return NULL;			/* no such entry */
	Din_Go(83,2048);if (entry->fn_xmin == HeapTupleHeaderGetRawXmin(procedureTuple->t_data) &&
		ItemPointerEquals(&entry->fn_tid, &procedureTuple->t_self))
		{/*33*/{CFuncHashTabEntry * ReplaceReturn61 = entry; Din_Go(82,2048); return ReplaceReturn61;}/*34*/}			/* OK */
	{CFuncHashTabEntry * ReplaceReturn60 = NULL; Din_Go(84,2048); return ReplaceReturn60;}				/* entry is out of date */
}

/*
 * record_C_func: enter (or update) info about a C function in the hash table
 */
static void
record_C_func(HeapTuple procedureTuple,
			  PGFunction user_fn, const Pg_finfo_record *inforec)
{
	Din_Go(85,2048);Oid			fn_oid = HeapTupleGetOid(procedureTuple);
	CFuncHashTabEntry *entry;
	bool		found;

	/* Create the hash table if it doesn't exist yet */
	Din_Go(87,2048);if (CFuncHash == NULL)
	{
		Din_Go(86,2048);HASHCTL		hash_ctl;

		MemSet(&hash_ctl, 0, sizeof(hash_ctl));
		hash_ctl.keysize = sizeof(Oid);
		hash_ctl.entrysize = sizeof(CFuncHashTabEntry);
		CFuncHash = hash_create("CFuncHash",
								100,
								&hash_ctl,
								HASH_ELEM | HASH_BLOBS);
	}

	Din_Go(88,2048);entry = (CFuncHashTabEntry *)
		hash_search(CFuncHash,
					&fn_oid,
					HASH_ENTER,
					&found);
	/* OID is already filled in */
	entry->fn_xmin = HeapTupleHeaderGetRawXmin(procedureTuple->t_data);
	entry->fn_tid = procedureTuple->t_self;
	entry->user_fn = user_fn;
	entry->inforec = inforec;
Din_Go(89,2048);}

/*
 * clear_external_function_hash: remove entries for a library being closed
 *
 * Presently we just zap the entire hash table, but later it might be worth
 * the effort to remove only the entries associated with the given handle.
 */
void
clear_external_function_hash(void *filehandle)
{
	Din_Go(90,2048);if (CFuncHash)
		{/*15*/Din_Go(91,2048);hash_destroy(CFuncHash);/*16*/}
	Din_Go(92,2048);CFuncHash = NULL;
Din_Go(93,2048);}


/*
 * Copy an FmgrInfo struct
 *
 * This is inherently somewhat bogus since we can't reliably duplicate
 * language-dependent subsidiary info.  We cheat by zeroing fn_extra,
 * instead, meaning that subsidiary info will have to be recomputed.
 */
void
fmgr_info_copy(FmgrInfo *dstinfo, FmgrInfo *srcinfo,
			   MemoryContext destcxt)
{
	Din_Go(94,2048);memcpy(dstinfo, srcinfo, sizeof(FmgrInfo));
	dstinfo->fn_mcxt = destcxt;
	Din_Go(96,2048);if (dstinfo->fn_addr == fmgr_oldstyle)
	{
		/* For oldstyle functions we must copy fn_extra */
		Din_Go(95,2048);Oldstyle_fnextra *fnextra;

		fnextra = (Oldstyle_fnextra *)
			MemoryContextAlloc(destcxt, sizeof(Oldstyle_fnextra));
		memcpy(fnextra, srcinfo->fn_extra, sizeof(Oldstyle_fnextra));
		dstinfo->fn_extra = (void *) fnextra;
	}
	else
		dstinfo->fn_extra = NULL;
Din_Go(97,2048);}


/*
 * Specialized lookup routine for fmgr_internal_validator: given the alleged
 * name of an internal function, return the OID of the function.
 * If the name is not recognized, return InvalidOid.
 */
Oid
fmgr_internal_function(const char *proname)
{
	Din_Go(98,2048);const FmgrBuiltin *fbp = fmgr_lookupByName(proname);

	Din_Go(99,2048);if (fbp == NULL)
		return InvalidOid;
	{Oid  ReplaceReturn59 = fbp->foid; Din_Go(100,2048); return ReplaceReturn59;}
}


/*
 * Handler for old-style "C" language functions
 */
static Datum
fmgr_oldstyle(PG_FUNCTION_ARGS)
{
	Din_Go(101,2048);Oldstyle_fnextra *fnextra;
	int			n_arguments = fcinfo->nargs;
	int			i;
	bool		isnull;
	func_ptr	user_fn;
	char	   *returnValue;

	Din_Go(102,2048);if (fcinfo->flinfo == NULL || fcinfo->flinfo->fn_extra == NULL)
		elog(ERROR, "fmgr_oldstyle received NULL pointer");
	Din_Go(103,2048);fnextra = (Oldstyle_fnextra *) fcinfo->flinfo->fn_extra;

	/*
	 * Result is NULL if any argument is NULL, but we still call the function
	 * (peculiar, but that's the way it worked before, and after all this is a
	 * backwards-compatibility wrapper).  Note, however, that we'll never get
	 * here with NULL arguments if the function is marked strict.
	 *
	 * We also need to detoast any TOAST-ed inputs, since it's unlikely that
	 * an old-style function knows about TOASTing.
	 */
	isnull = false;
	Din_Go(105,2048);for (i = 0; i < n_arguments; i++)
	{
		Din_Go(104,2048);if (PG_ARGISNULL(i))
			isnull = true;
		else if (fnextra->arg_toastable[i])
			fcinfo->arg[i] = PointerGetDatum(PG_DETOAST_DATUM(fcinfo->arg[i]));
	}
	Din_Go(106,2048);fcinfo->isnull = isnull;

	user_fn = fnextra->func;

	Din_Go(143,2048);switch (n_arguments)
	{
		case 0:
			Din_Go(107,2048);returnValue = (char *) (*user_fn) ();
			Din_Go(108,2048);break;
		case 1:

			/*
			 * nullvalue() used to use isNull to check if arg is NULL; perhaps
			 * there are other functions still out there that also rely on
			 * this undocumented hack?
			 */
			Din_Go(109,2048);returnValue = (char *) (*user_fn) (fcinfo->arg[0],
											   &fcinfo->isnull);
			Din_Go(110,2048);break;
		case 2:
			Din_Go(111,2048);returnValue = (char *) (*user_fn) (fcinfo->arg[0],
											   fcinfo->arg[1]);
			Din_Go(112,2048);break;
		case 3:
			Din_Go(113,2048);returnValue = (char *) (*user_fn) (fcinfo->arg[0],
											   fcinfo->arg[1],
											   fcinfo->arg[2]);
			Din_Go(114,2048);break;
		case 4:
			Din_Go(115,2048);returnValue = (char *) (*user_fn) (fcinfo->arg[0],
											   fcinfo->arg[1],
											   fcinfo->arg[2],
											   fcinfo->arg[3]);
			Din_Go(116,2048);break;
		case 5:
			Din_Go(117,2048);returnValue = (char *) (*user_fn) (fcinfo->arg[0],
											   fcinfo->arg[1],
											   fcinfo->arg[2],
											   fcinfo->arg[3],
											   fcinfo->arg[4]);
			Din_Go(118,2048);break;
		case 6:
			Din_Go(119,2048);returnValue = (char *) (*user_fn) (fcinfo->arg[0],
											   fcinfo->arg[1],
											   fcinfo->arg[2],
											   fcinfo->arg[3],
											   fcinfo->arg[4],
											   fcinfo->arg[5]);
			Din_Go(120,2048);break;
		case 7:
			Din_Go(121,2048);returnValue = (char *) (*user_fn) (fcinfo->arg[0],
											   fcinfo->arg[1],
											   fcinfo->arg[2],
											   fcinfo->arg[3],
											   fcinfo->arg[4],
											   fcinfo->arg[5],
											   fcinfo->arg[6]);
			Din_Go(122,2048);break;
		case 8:
			Din_Go(123,2048);returnValue = (char *) (*user_fn) (fcinfo->arg[0],
											   fcinfo->arg[1],
											   fcinfo->arg[2],
											   fcinfo->arg[3],
											   fcinfo->arg[4],
											   fcinfo->arg[5],
											   fcinfo->arg[6],
											   fcinfo->arg[7]);
			Din_Go(124,2048);break;
		case 9:
			Din_Go(125,2048);returnValue = (char *) (*user_fn) (fcinfo->arg[0],
											   fcinfo->arg[1],
											   fcinfo->arg[2],
											   fcinfo->arg[3],
											   fcinfo->arg[4],
											   fcinfo->arg[5],
											   fcinfo->arg[6],
											   fcinfo->arg[7],
											   fcinfo->arg[8]);
			Din_Go(126,2048);break;
		case 10:
			Din_Go(127,2048);returnValue = (char *) (*user_fn) (fcinfo->arg[0],
											   fcinfo->arg[1],
											   fcinfo->arg[2],
											   fcinfo->arg[3],
											   fcinfo->arg[4],
											   fcinfo->arg[5],
											   fcinfo->arg[6],
											   fcinfo->arg[7],
											   fcinfo->arg[8],
											   fcinfo->arg[9]);
			Din_Go(128,2048);break;
		case 11:
			Din_Go(129,2048);returnValue = (char *) (*user_fn) (fcinfo->arg[0],
											   fcinfo->arg[1],
											   fcinfo->arg[2],
											   fcinfo->arg[3],
											   fcinfo->arg[4],
											   fcinfo->arg[5],
											   fcinfo->arg[6],
											   fcinfo->arg[7],
											   fcinfo->arg[8],
											   fcinfo->arg[9],
											   fcinfo->arg[10]);
			Din_Go(130,2048);break;
		case 12:
			Din_Go(131,2048);returnValue = (char *) (*user_fn) (fcinfo->arg[0],
											   fcinfo->arg[1],
											   fcinfo->arg[2],
											   fcinfo->arg[3],
											   fcinfo->arg[4],
											   fcinfo->arg[5],
											   fcinfo->arg[6],
											   fcinfo->arg[7],
											   fcinfo->arg[8],
											   fcinfo->arg[9],
											   fcinfo->arg[10],
											   fcinfo->arg[11]);
			Din_Go(132,2048);break;
		case 13:
			Din_Go(133,2048);returnValue = (char *) (*user_fn) (fcinfo->arg[0],
											   fcinfo->arg[1],
											   fcinfo->arg[2],
											   fcinfo->arg[3],
											   fcinfo->arg[4],
											   fcinfo->arg[5],
											   fcinfo->arg[6],
											   fcinfo->arg[7],
											   fcinfo->arg[8],
											   fcinfo->arg[9],
											   fcinfo->arg[10],
											   fcinfo->arg[11],
											   fcinfo->arg[12]);
			Din_Go(134,2048);break;
		case 14:
			Din_Go(135,2048);returnValue = (char *) (*user_fn) (fcinfo->arg[0],
											   fcinfo->arg[1],
											   fcinfo->arg[2],
											   fcinfo->arg[3],
											   fcinfo->arg[4],
											   fcinfo->arg[5],
											   fcinfo->arg[6],
											   fcinfo->arg[7],
											   fcinfo->arg[8],
											   fcinfo->arg[9],
											   fcinfo->arg[10],
											   fcinfo->arg[11],
											   fcinfo->arg[12],
											   fcinfo->arg[13]);
			Din_Go(136,2048);break;
		case 15:
			Din_Go(137,2048);returnValue = (char *) (*user_fn) (fcinfo->arg[0],
											   fcinfo->arg[1],
											   fcinfo->arg[2],
											   fcinfo->arg[3],
											   fcinfo->arg[4],
											   fcinfo->arg[5],
											   fcinfo->arg[6],
											   fcinfo->arg[7],
											   fcinfo->arg[8],
											   fcinfo->arg[9],
											   fcinfo->arg[10],
											   fcinfo->arg[11],
											   fcinfo->arg[12],
											   fcinfo->arg[13],
											   fcinfo->arg[14]);
			Din_Go(138,2048);break;
		case 16:
			Din_Go(139,2048);returnValue = (char *) (*user_fn) (fcinfo->arg[0],
											   fcinfo->arg[1],
											   fcinfo->arg[2],
											   fcinfo->arg[3],
											   fcinfo->arg[4],
											   fcinfo->arg[5],
											   fcinfo->arg[6],
											   fcinfo->arg[7],
											   fcinfo->arg[8],
											   fcinfo->arg[9],
											   fcinfo->arg[10],
											   fcinfo->arg[11],
											   fcinfo->arg[12],
											   fcinfo->arg[13],
											   fcinfo->arg[14],
											   fcinfo->arg[15]);
			Din_Go(140,2048);break;
		default:

			/*
			 * Increasing FUNC_MAX_ARGS doesn't automatically add cases to the
			 * above code, so mention the actual value in this error not
			 * FUNC_MAX_ARGS.  You could add cases to the above if you needed
			 * to support old-style functions with many arguments, but making
			 * 'em be new-style is probably a better idea.
			 */
			ereport(ERROR,
					(errcode(ERRCODE_TOO_MANY_ARGUMENTS),
			 errmsg("function %u has too many arguments (%d, maximum is %d)",
					fcinfo->flinfo->fn_oid, n_arguments, 16)));
			Din_Go(141,2048);returnValue = NULL; /* keep compiler quiet */
			Din_Go(142,2048);break;
	}

	{Datum  ReplaceReturn58 = PointerGetDatum(returnValue); Din_Go(144,2048); return ReplaceReturn58;}
}


/*
 * Support for security-definer and proconfig-using functions.  We support
 * both of these features using the same call handler, because they are
 * often used together and it would be inefficient (as well as notationally
 * messy) to have two levels of call handler involved.
 */
struct fmgr_security_definer_cache
{
	FmgrInfo	flinfo;			/* lookup info for target function */
	Oid			userid;			/* userid to set, or InvalidOid */
	ArrayType  *proconfig;		/* GUC values to set, or NULL */
	Datum		arg;			/* passthrough argument for plugin modules */
};

/*
 * Function handler for security-definer/proconfig/plugin-hooked functions.
 * We extract the OID of the actual function and do a fmgr lookup again.
 * Then we fetch the pg_proc row and copy the owner ID and proconfig fields.
 * (All this info is cached for the duration of the current query.)
 * To execute a call, we temporarily replace the flinfo with the cached
 * and looked-up one, while keeping the outer fcinfo (which contains all
 * the actual arguments, etc.) intact.  This is not re-entrant, but then
 * the fcinfo itself can't be used re-entrantly anyway.
 */
static Datum
fmgr_security_definer(PG_FUNCTION_ARGS)
{
	Din_Go(145,2048);Datum		result;
	struct fmgr_security_definer_cache *volatile fcache;
	FmgrInfo   *save_flinfo;
	Oid			save_userid;
	int			save_sec_context;
	volatile int save_nestlevel;
	PgStat_FunctionCallUsage fcusage;

	Din_Go(156,2048);if (!fcinfo->flinfo->fn_extra)
	{
		Din_Go(146,2048);HeapTuple	tuple;
		Form_pg_proc procedureStruct;
		Datum		datum;
		bool		isnull;
		MemoryContext oldcxt;

		fcache = MemoryContextAllocZero(fcinfo->flinfo->fn_mcxt,
										sizeof(*fcache));

		fmgr_info_cxt_security(fcinfo->flinfo->fn_oid, &fcache->flinfo,
							   fcinfo->flinfo->fn_mcxt, true);
		fcache->flinfo.fn_expr = fcinfo->flinfo->fn_expr;

		tuple = SearchSysCache1(PROCOID,
								ObjectIdGetDatum(fcinfo->flinfo->fn_oid));
		Din_Go(147,2048);if (!HeapTupleIsValid(tuple))
			elog(ERROR, "cache lookup failed for function %u",
				 fcinfo->flinfo->fn_oid);
		Din_Go(148,2048);procedureStruct = (Form_pg_proc) GETSTRUCT(tuple);

		Din_Go(150,2048);if (procedureStruct->prosecdef)
			{/*35*/Din_Go(149,2048);fcache->userid = procedureStruct->proowner;/*36*/}

		Din_Go(151,2048);datum = SysCacheGetAttr(PROCOID, tuple, Anum_pg_proc_proconfig,
								&isnull);
		Din_Go(153,2048);if (!isnull)
		{
			Din_Go(152,2048);oldcxt = MemoryContextSwitchTo(fcinfo->flinfo->fn_mcxt);
			fcache->proconfig = DatumGetArrayTypePCopy(datum);
			MemoryContextSwitchTo(oldcxt);
		}

		Din_Go(154,2048);ReleaseSysCache(tuple);

		fcinfo->flinfo->fn_extra = fcache;
	}
	else
		{/*37*/Din_Go(155,2048);fcache = fcinfo->flinfo->fn_extra;/*38*/}

	/* GetUserIdAndSecContext is cheap enough that no harm in a wasted call */
	Din_Go(157,2048);GetUserIdAndSecContext(&save_userid, &save_sec_context);
	Din_Go(160,2048);if (fcache->proconfig)		/* Need a new GUC nesting level */
		{/*39*/Din_Go(158,2048);save_nestlevel = NewGUCNestLevel();/*40*/}
	else
		{/*41*/Din_Go(159,2048);save_nestlevel = 0;/*42*/}		/* keep compiler quiet */

	Din_Go(162,2048);if (OidIsValid(fcache->userid))
		{/*43*/Din_Go(161,2048);SetUserIdAndSecContext(fcache->userid,
							save_sec_context | SECURITY_LOCAL_USERID_CHANGE);/*44*/}

	Din_Go(164,2048);if (fcache->proconfig)
	{
		Din_Go(163,2048);ProcessGUCArray(fcache->proconfig,
						(superuser() ? PGC_SUSET : PGC_USERSET),
						PGC_S_SESSION,
						GUC_ACTION_SAVE);
	}

	/* function manager hook */
	Din_Go(166,2048);if (fmgr_hook)
		{/*45*/Din_Go(165,2048);(*fmgr_hook) (FHET_START, &fcache->flinfo, &fcache->arg);/*46*/}

	/*
	 * We don't need to restore GUC or userid settings on error, because the
	 * ensuing xact or subxact abort will do that.  The PG_TRY block is only
	 * needed to clean up the flinfo link.
	 */
	Din_Go(167,2048);save_flinfo = fcinfo->flinfo;

	PG_TRY();
	{
		fcinfo->flinfo = &fcache->flinfo;

		/* See notes in fmgr_info_cxt_security */
		pgstat_init_function_usage(fcinfo, &fcusage);

		result = FunctionCallInvoke(fcinfo);

		/*
		 * We could be calling either a regular or a set-returning function,
		 * so we have to test to see what finalize flag to use.
		 */
		pgstat_end_function_usage(&fcusage,
								  (fcinfo->resultinfo == NULL ||
								   !IsA(fcinfo->resultinfo, ReturnSetInfo) ||
								   ((ReturnSetInfo *) fcinfo->resultinfo)->isDone != ExprMultipleResult));
	}
	PG_CATCH();
	{
		fcinfo->flinfo = save_flinfo;
		if (fmgr_hook)
			{/*47*/(*fmgr_hook) (FHET_ABORT, &fcache->flinfo, &fcache->arg);/*48*/}
		PG_RE_THROW();
	}
	PG_END_TRY();

	fcinfo->flinfo = save_flinfo;

	Din_Go(169,2048);if (fcache->proconfig)
		{/*49*/Din_Go(168,2048);AtEOXact_GUC(true, save_nestlevel);/*50*/}
	Din_Go(171,2048);if (OidIsValid(fcache->userid))
		{/*51*/Din_Go(170,2048);SetUserIdAndSecContext(save_userid, save_sec_context);/*52*/}
	Din_Go(173,2048);if (fmgr_hook)
		{/*53*/Din_Go(172,2048);(*fmgr_hook) (FHET_END, &fcache->flinfo, &fcache->arg);/*54*/}

	{Datum  ReplaceReturn57 = result; Din_Go(174,2048); return ReplaceReturn57;}
}


/*-------------------------------------------------------------------------
 *		Support routines for callers of fmgr-compatible functions
 *-------------------------------------------------------------------------
 */

/*
 * These are for invocation of a specifically named function with a
 * directly-computed parameter list.  Note that neither arguments nor result
 * are allowed to be NULL.  Also, the function cannot be one that needs to
 * look at FmgrInfo, since there won't be any.
 */
Datum
DirectFunctionCall1Coll(PGFunction func, Oid collation, Datum arg1)
{
	Din_Go(175,2048);FunctionCallInfoData fcinfo;
	Datum		result;

	InitFunctionCallInfoData(fcinfo, NULL, 1, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.argnull[0] = false;

	result = (*func) (&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(176,2048);if (fcinfo.isnull)
		elog(ERROR, "function %p returned NULL", (void *) func);

	{Datum  ReplaceReturn56 = result; Din_Go(177,2048); return ReplaceReturn56;}
}

Datum
DirectFunctionCall2Coll(PGFunction func, Oid collation, Datum arg1, Datum arg2)
{
	Din_Go(178,2048);FunctionCallInfoData fcinfo;
	Datum		result;

	InitFunctionCallInfoData(fcinfo, NULL, 2, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;

	result = (*func) (&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(179,2048);if (fcinfo.isnull)
		elog(ERROR, "function %p returned NULL", (void *) func);

	{Datum  ReplaceReturn55 = result; Din_Go(180,2048); return ReplaceReturn55;}
}

Datum
DirectFunctionCall3Coll(PGFunction func, Oid collation, Datum arg1, Datum arg2,
						Datum arg3)
{
	Din_Go(181,2048);FunctionCallInfoData fcinfo;
	Datum		result;

	InitFunctionCallInfoData(fcinfo, NULL, 3, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;

	result = (*func) (&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(182,2048);if (fcinfo.isnull)
		elog(ERROR, "function %p returned NULL", (void *) func);

	{Datum  ReplaceReturn54 = result; Din_Go(183,2048); return ReplaceReturn54;}
}

Datum
DirectFunctionCall4Coll(PGFunction func, Oid collation, Datum arg1, Datum arg2,
						Datum arg3, Datum arg4)
{
	Din_Go(184,2048);FunctionCallInfoData fcinfo;
	Datum		result;

	InitFunctionCallInfoData(fcinfo, NULL, 4, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.arg[3] = arg4;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;
	fcinfo.argnull[3] = false;

	result = (*func) (&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(185,2048);if (fcinfo.isnull)
		elog(ERROR, "function %p returned NULL", (void *) func);

	{Datum  ReplaceReturn53 = result; Din_Go(186,2048); return ReplaceReturn53;}
}

Datum
DirectFunctionCall5Coll(PGFunction func, Oid collation, Datum arg1, Datum arg2,
						Datum arg3, Datum arg4, Datum arg5)
{
	Din_Go(187,2048);FunctionCallInfoData fcinfo;
	Datum		result;

	InitFunctionCallInfoData(fcinfo, NULL, 5, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.arg[3] = arg4;
	fcinfo.arg[4] = arg5;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;
	fcinfo.argnull[3] = false;
	fcinfo.argnull[4] = false;

	result = (*func) (&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(188,2048);if (fcinfo.isnull)
		elog(ERROR, "function %p returned NULL", (void *) func);

	{Datum  ReplaceReturn52 = result; Din_Go(189,2048); return ReplaceReturn52;}
}

Datum
DirectFunctionCall6Coll(PGFunction func, Oid collation, Datum arg1, Datum arg2,
						Datum arg3, Datum arg4, Datum arg5,
						Datum arg6)
{
	Din_Go(190,2048);FunctionCallInfoData fcinfo;
	Datum		result;

	InitFunctionCallInfoData(fcinfo, NULL, 6, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.arg[3] = arg4;
	fcinfo.arg[4] = arg5;
	fcinfo.arg[5] = arg6;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;
	fcinfo.argnull[3] = false;
	fcinfo.argnull[4] = false;
	fcinfo.argnull[5] = false;

	result = (*func) (&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(191,2048);if (fcinfo.isnull)
		elog(ERROR, "function %p returned NULL", (void *) func);

	{Datum  ReplaceReturn51 = result; Din_Go(192,2048); return ReplaceReturn51;}
}

Datum
DirectFunctionCall7Coll(PGFunction func, Oid collation, Datum arg1, Datum arg2,
						Datum arg3, Datum arg4, Datum arg5,
						Datum arg6, Datum arg7)
{
	Din_Go(193,2048);FunctionCallInfoData fcinfo;
	Datum		result;

	InitFunctionCallInfoData(fcinfo, NULL, 7, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.arg[3] = arg4;
	fcinfo.arg[4] = arg5;
	fcinfo.arg[5] = arg6;
	fcinfo.arg[6] = arg7;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;
	fcinfo.argnull[3] = false;
	fcinfo.argnull[4] = false;
	fcinfo.argnull[5] = false;
	fcinfo.argnull[6] = false;

	result = (*func) (&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(194,2048);if (fcinfo.isnull)
		elog(ERROR, "function %p returned NULL", (void *) func);

	{Datum  ReplaceReturn50 = result; Din_Go(195,2048); return ReplaceReturn50;}
}

Datum
DirectFunctionCall8Coll(PGFunction func, Oid collation, Datum arg1, Datum arg2,
						Datum arg3, Datum arg4, Datum arg5,
						Datum arg6, Datum arg7, Datum arg8)
{
	Din_Go(196,2048);FunctionCallInfoData fcinfo;
	Datum		result;

	InitFunctionCallInfoData(fcinfo, NULL, 8, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.arg[3] = arg4;
	fcinfo.arg[4] = arg5;
	fcinfo.arg[5] = arg6;
	fcinfo.arg[6] = arg7;
	fcinfo.arg[7] = arg8;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;
	fcinfo.argnull[3] = false;
	fcinfo.argnull[4] = false;
	fcinfo.argnull[5] = false;
	fcinfo.argnull[6] = false;
	fcinfo.argnull[7] = false;

	result = (*func) (&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(197,2048);if (fcinfo.isnull)
		elog(ERROR, "function %p returned NULL", (void *) func);

	{Datum  ReplaceReturn49 = result; Din_Go(198,2048); return ReplaceReturn49;}
}

Datum
DirectFunctionCall9Coll(PGFunction func, Oid collation, Datum arg1, Datum arg2,
						Datum arg3, Datum arg4, Datum arg5,
						Datum arg6, Datum arg7, Datum arg8,
						Datum arg9)
{
	Din_Go(199,2048);FunctionCallInfoData fcinfo;
	Datum		result;

	InitFunctionCallInfoData(fcinfo, NULL, 9, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.arg[3] = arg4;
	fcinfo.arg[4] = arg5;
	fcinfo.arg[5] = arg6;
	fcinfo.arg[6] = arg7;
	fcinfo.arg[7] = arg8;
	fcinfo.arg[8] = arg9;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;
	fcinfo.argnull[3] = false;
	fcinfo.argnull[4] = false;
	fcinfo.argnull[5] = false;
	fcinfo.argnull[6] = false;
	fcinfo.argnull[7] = false;
	fcinfo.argnull[8] = false;

	result = (*func) (&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(200,2048);if (fcinfo.isnull)
		elog(ERROR, "function %p returned NULL", (void *) func);

	{Datum  ReplaceReturn48 = result; Din_Go(201,2048); return ReplaceReturn48;}
}


/*
 * These are for invocation of a previously-looked-up function with a
 * directly-computed parameter list.  Note that neither arguments nor result
 * are allowed to be NULL.
 */
Datum
FunctionCall1Coll(FmgrInfo *flinfo, Oid collation, Datum arg1)
{
	Din_Go(202,2048);FunctionCallInfoData fcinfo;
	Datum		result;

	InitFunctionCallInfoData(fcinfo, flinfo, 1, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.argnull[0] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(203,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", fcinfo.flinfo->fn_oid);

	{Datum  ReplaceReturn47 = result; Din_Go(204,2048); return ReplaceReturn47;}
}

Datum
FunctionCall2Coll(FmgrInfo *flinfo, Oid collation, Datum arg1, Datum arg2)
{
	/*
	 * XXX if you change this routine, see also the inlined version in
	 * utils/sort/tuplesort.c!
	 */
	Din_Go(205,2048);FunctionCallInfoData fcinfo;
	Datum		result;

	InitFunctionCallInfoData(fcinfo, flinfo, 2, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(206,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", fcinfo.flinfo->fn_oid);

	{Datum  ReplaceReturn46 = result; Din_Go(207,2048); return ReplaceReturn46;}
}

Datum
FunctionCall3Coll(FmgrInfo *flinfo, Oid collation, Datum arg1, Datum arg2,
				  Datum arg3)
{
	Din_Go(208,2048);FunctionCallInfoData fcinfo;
	Datum		result;

	InitFunctionCallInfoData(fcinfo, flinfo, 3, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(209,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", fcinfo.flinfo->fn_oid);

	{Datum  ReplaceReturn45 = result; Din_Go(210,2048); return ReplaceReturn45;}
}

Datum
FunctionCall4Coll(FmgrInfo *flinfo, Oid collation, Datum arg1, Datum arg2,
				  Datum arg3, Datum arg4)
{
	Din_Go(211,2048);FunctionCallInfoData fcinfo;
	Datum		result;

	InitFunctionCallInfoData(fcinfo, flinfo, 4, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.arg[3] = arg4;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;
	fcinfo.argnull[3] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(212,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", fcinfo.flinfo->fn_oid);

	{Datum  ReplaceReturn44 = result; Din_Go(213,2048); return ReplaceReturn44;}
}

Datum
FunctionCall5Coll(FmgrInfo *flinfo, Oid collation, Datum arg1, Datum arg2,
				  Datum arg3, Datum arg4, Datum arg5)
{
	Din_Go(214,2048);FunctionCallInfoData fcinfo;
	Datum		result;

	InitFunctionCallInfoData(fcinfo, flinfo, 5, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.arg[3] = arg4;
	fcinfo.arg[4] = arg5;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;
	fcinfo.argnull[3] = false;
	fcinfo.argnull[4] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(215,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", fcinfo.flinfo->fn_oid);

	{Datum  ReplaceReturn43 = result; Din_Go(216,2048); return ReplaceReturn43;}
}

Datum
FunctionCall6Coll(FmgrInfo *flinfo, Oid collation, Datum arg1, Datum arg2,
				  Datum arg3, Datum arg4, Datum arg5,
				  Datum arg6)
{
	Din_Go(217,2048);FunctionCallInfoData fcinfo;
	Datum		result;

	InitFunctionCallInfoData(fcinfo, flinfo, 6, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.arg[3] = arg4;
	fcinfo.arg[4] = arg5;
	fcinfo.arg[5] = arg6;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;
	fcinfo.argnull[3] = false;
	fcinfo.argnull[4] = false;
	fcinfo.argnull[5] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(218,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", fcinfo.flinfo->fn_oid);

	{Datum  ReplaceReturn42 = result; Din_Go(219,2048); return ReplaceReturn42;}
}

Datum
FunctionCall7Coll(FmgrInfo *flinfo, Oid collation, Datum arg1, Datum arg2,
				  Datum arg3, Datum arg4, Datum arg5,
				  Datum arg6, Datum arg7)
{
	Din_Go(220,2048);FunctionCallInfoData fcinfo;
	Datum		result;

	InitFunctionCallInfoData(fcinfo, flinfo, 7, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.arg[3] = arg4;
	fcinfo.arg[4] = arg5;
	fcinfo.arg[5] = arg6;
	fcinfo.arg[6] = arg7;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;
	fcinfo.argnull[3] = false;
	fcinfo.argnull[4] = false;
	fcinfo.argnull[5] = false;
	fcinfo.argnull[6] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(221,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", fcinfo.flinfo->fn_oid);

	{Datum  ReplaceReturn41 = result; Din_Go(222,2048); return ReplaceReturn41;}
}

Datum
FunctionCall8Coll(FmgrInfo *flinfo, Oid collation, Datum arg1, Datum arg2,
				  Datum arg3, Datum arg4, Datum arg5,
				  Datum arg6, Datum arg7, Datum arg8)
{
	Din_Go(223,2048);FunctionCallInfoData fcinfo;
	Datum		result;

	InitFunctionCallInfoData(fcinfo, flinfo, 8, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.arg[3] = arg4;
	fcinfo.arg[4] = arg5;
	fcinfo.arg[5] = arg6;
	fcinfo.arg[6] = arg7;
	fcinfo.arg[7] = arg8;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;
	fcinfo.argnull[3] = false;
	fcinfo.argnull[4] = false;
	fcinfo.argnull[5] = false;
	fcinfo.argnull[6] = false;
	fcinfo.argnull[7] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(224,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", fcinfo.flinfo->fn_oid);

	{Datum  ReplaceReturn40 = result; Din_Go(225,2048); return ReplaceReturn40;}
}

Datum
FunctionCall9Coll(FmgrInfo *flinfo, Oid collation, Datum arg1, Datum arg2,
				  Datum arg3, Datum arg4, Datum arg5,
				  Datum arg6, Datum arg7, Datum arg8,
				  Datum arg9)
{
	Din_Go(226,2048);FunctionCallInfoData fcinfo;
	Datum		result;

	InitFunctionCallInfoData(fcinfo, flinfo, 9, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.arg[3] = arg4;
	fcinfo.arg[4] = arg5;
	fcinfo.arg[5] = arg6;
	fcinfo.arg[6] = arg7;
	fcinfo.arg[7] = arg8;
	fcinfo.arg[8] = arg9;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;
	fcinfo.argnull[3] = false;
	fcinfo.argnull[4] = false;
	fcinfo.argnull[5] = false;
	fcinfo.argnull[6] = false;
	fcinfo.argnull[7] = false;
	fcinfo.argnull[8] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(227,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", fcinfo.flinfo->fn_oid);

	{Datum  ReplaceReturn39 = result; Din_Go(228,2048); return ReplaceReturn39;}
}


/*
 * These are for invocation of a function identified by OID with a
 * directly-computed parameter list.  Note that neither arguments nor result
 * are allowed to be NULL.  These are essentially fmgr_info() followed
 * by FunctionCallN().  If the same function is to be invoked repeatedly,
 * do the fmgr_info() once and then use FunctionCallN().
 */
Datum
OidFunctionCall0Coll(Oid functionId, Oid collation)
{
	Din_Go(229,2048);FmgrInfo	flinfo;
	FunctionCallInfoData fcinfo;
	Datum		result;

	fmgr_info(functionId, &flinfo);

	InitFunctionCallInfoData(fcinfo, &flinfo, 0, collation, NULL, NULL);

	result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(230,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", flinfo.fn_oid);

	{Datum  ReplaceReturn38 = result; Din_Go(231,2048); return ReplaceReturn38;}
}

Datum
OidFunctionCall1Coll(Oid functionId, Oid collation, Datum arg1)
{
	Din_Go(232,2048);FmgrInfo	flinfo;
	FunctionCallInfoData fcinfo;
	Datum		result;

	fmgr_info(functionId, &flinfo);

	InitFunctionCallInfoData(fcinfo, &flinfo, 1, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.argnull[0] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(233,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", flinfo.fn_oid);

	{Datum  ReplaceReturn37 = result; Din_Go(234,2048); return ReplaceReturn37;}
}

Datum
OidFunctionCall2Coll(Oid functionId, Oid collation, Datum arg1, Datum arg2)
{
	Din_Go(235,2048);FmgrInfo	flinfo;
	FunctionCallInfoData fcinfo;
	Datum		result;

	fmgr_info(functionId, &flinfo);

	InitFunctionCallInfoData(fcinfo, &flinfo, 2, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(236,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", flinfo.fn_oid);

	{Datum  ReplaceReturn36 = result; Din_Go(237,2048); return ReplaceReturn36;}
}

Datum
OidFunctionCall3Coll(Oid functionId, Oid collation, Datum arg1, Datum arg2,
					 Datum arg3)
{
	Din_Go(238,2048);FmgrInfo	flinfo;
	FunctionCallInfoData fcinfo;
	Datum		result;

	fmgr_info(functionId, &flinfo);

	InitFunctionCallInfoData(fcinfo, &flinfo, 3, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(239,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", flinfo.fn_oid);

	{Datum  ReplaceReturn35 = result; Din_Go(240,2048); return ReplaceReturn35;}
}

Datum
OidFunctionCall4Coll(Oid functionId, Oid collation, Datum arg1, Datum arg2,
					 Datum arg3, Datum arg4)
{
	Din_Go(241,2048);FmgrInfo	flinfo;
	FunctionCallInfoData fcinfo;
	Datum		result;

	fmgr_info(functionId, &flinfo);

	InitFunctionCallInfoData(fcinfo, &flinfo, 4, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.arg[3] = arg4;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;
	fcinfo.argnull[3] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(242,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", flinfo.fn_oid);

	{Datum  ReplaceReturn34 = result; Din_Go(243,2048); return ReplaceReturn34;}
}

Datum
OidFunctionCall5Coll(Oid functionId, Oid collation, Datum arg1, Datum arg2,
					 Datum arg3, Datum arg4, Datum arg5)
{
	Din_Go(244,2048);FmgrInfo	flinfo;
	FunctionCallInfoData fcinfo;
	Datum		result;

	fmgr_info(functionId, &flinfo);

	InitFunctionCallInfoData(fcinfo, &flinfo, 5, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.arg[3] = arg4;
	fcinfo.arg[4] = arg5;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;
	fcinfo.argnull[3] = false;
	fcinfo.argnull[4] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(245,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", flinfo.fn_oid);

	{Datum  ReplaceReturn33 = result; Din_Go(246,2048); return ReplaceReturn33;}
}

Datum
OidFunctionCall6Coll(Oid functionId, Oid collation, Datum arg1, Datum arg2,
					 Datum arg3, Datum arg4, Datum arg5,
					 Datum arg6)
{
	Din_Go(247,2048);FmgrInfo	flinfo;
	FunctionCallInfoData fcinfo;
	Datum		result;

	fmgr_info(functionId, &flinfo);

	InitFunctionCallInfoData(fcinfo, &flinfo, 6, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.arg[3] = arg4;
	fcinfo.arg[4] = arg5;
	fcinfo.arg[5] = arg6;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;
	fcinfo.argnull[3] = false;
	fcinfo.argnull[4] = false;
	fcinfo.argnull[5] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(248,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", flinfo.fn_oid);

	{Datum  ReplaceReturn32 = result; Din_Go(249,2048); return ReplaceReturn32;}
}

Datum
OidFunctionCall7Coll(Oid functionId, Oid collation, Datum arg1, Datum arg2,
					 Datum arg3, Datum arg4, Datum arg5,
					 Datum arg6, Datum arg7)
{
	Din_Go(250,2048);FmgrInfo	flinfo;
	FunctionCallInfoData fcinfo;
	Datum		result;

	fmgr_info(functionId, &flinfo);

	InitFunctionCallInfoData(fcinfo, &flinfo, 7, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.arg[3] = arg4;
	fcinfo.arg[4] = arg5;
	fcinfo.arg[5] = arg6;
	fcinfo.arg[6] = arg7;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;
	fcinfo.argnull[3] = false;
	fcinfo.argnull[4] = false;
	fcinfo.argnull[5] = false;
	fcinfo.argnull[6] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(251,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", flinfo.fn_oid);

	{Datum  ReplaceReturn31 = result; Din_Go(252,2048); return ReplaceReturn31;}
}

Datum
OidFunctionCall8Coll(Oid functionId, Oid collation, Datum arg1, Datum arg2,
					 Datum arg3, Datum arg4, Datum arg5,
					 Datum arg6, Datum arg7, Datum arg8)
{
	Din_Go(253,2048);FmgrInfo	flinfo;
	FunctionCallInfoData fcinfo;
	Datum		result;

	fmgr_info(functionId, &flinfo);

	InitFunctionCallInfoData(fcinfo, &flinfo, 8, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.arg[3] = arg4;
	fcinfo.arg[4] = arg5;
	fcinfo.arg[5] = arg6;
	fcinfo.arg[6] = arg7;
	fcinfo.arg[7] = arg8;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;
	fcinfo.argnull[3] = false;
	fcinfo.argnull[4] = false;
	fcinfo.argnull[5] = false;
	fcinfo.argnull[6] = false;
	fcinfo.argnull[7] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(254,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", flinfo.fn_oid);

	{Datum  ReplaceReturn30 = result; Din_Go(255,2048); return ReplaceReturn30;}
}

Datum
OidFunctionCall9Coll(Oid functionId, Oid collation, Datum arg1, Datum arg2,
					 Datum arg3, Datum arg4, Datum arg5,
					 Datum arg6, Datum arg7, Datum arg8,
					 Datum arg9)
{
	Din_Go(256,2048);FmgrInfo	flinfo;
	FunctionCallInfoData fcinfo;
	Datum		result;

	fmgr_info(functionId, &flinfo);

	InitFunctionCallInfoData(fcinfo, &flinfo, 9, collation, NULL, NULL);

	fcinfo.arg[0] = arg1;
	fcinfo.arg[1] = arg2;
	fcinfo.arg[2] = arg3;
	fcinfo.arg[3] = arg4;
	fcinfo.arg[4] = arg5;
	fcinfo.arg[5] = arg6;
	fcinfo.arg[6] = arg7;
	fcinfo.arg[7] = arg8;
	fcinfo.arg[8] = arg9;
	fcinfo.argnull[0] = false;
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;
	fcinfo.argnull[3] = false;
	fcinfo.argnull[4] = false;
	fcinfo.argnull[5] = false;
	fcinfo.argnull[6] = false;
	fcinfo.argnull[7] = false;
	fcinfo.argnull[8] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(257,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", flinfo.fn_oid);

	{Datum  ReplaceReturn29 = result; Din_Go(258,2048); return ReplaceReturn29;}
}


/*
 * Special cases for convenient invocation of datatype I/O functions.
 */

/*
 * Call a previously-looked-up datatype input function.
 *
 * "str" may be NULL to indicate we are reading a NULL.  In this case
 * the caller should assume the result is NULL, but we'll call the input
 * function anyway if it's not strict.  So this is almost but not quite
 * the same as FunctionCall3.
 *
 * One important difference from the bare function call is that we will
 * push any active SPI context, allowing SPI-using I/O functions to be
 * called from other SPI functions without extra notation.  This is a hack,
 * but the alternative of expecting all SPI functions to do SPI_push/SPI_pop
 * around I/O calls seems worse.
 */
Datum
InputFunctionCall(FmgrInfo *flinfo, char *str, Oid typioparam, int32 typmod)
{
	Din_Go(259,2048);FunctionCallInfoData fcinfo;
	Datum		result;
	bool		pushed;

	Din_Go(261,2048);if (str == NULL && flinfo->fn_strict)
		{/*11*/{Datum  ReplaceReturn28 = (Datum) 0; Din_Go(260,2048); return ReplaceReturn28;}/*12*/}		/* just return null result */

	Din_Go(262,2048);pushed = SPI_push_conditional();

	InitFunctionCallInfoData(fcinfo, flinfo, 3, InvalidOid, NULL, NULL);

	fcinfo.arg[0] = CStringGetDatum(str);
	fcinfo.arg[1] = ObjectIdGetDatum(typioparam);
	fcinfo.arg[2] = Int32GetDatum(typmod);
	fcinfo.argnull[0] = (str == NULL);
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Should get null result if and only if str is NULL */
	Din_Go(265,2048);if (str == NULL)
	{
		Din_Go(263,2048);if (!fcinfo.isnull)
			elog(ERROR, "input function %u returned non-NULL",
				 fcinfo.flinfo->fn_oid);
	}
	else
	{
		Din_Go(264,2048);if (fcinfo.isnull)
			elog(ERROR, "input function %u returned NULL",
				 fcinfo.flinfo->fn_oid);
	}

	Din_Go(266,2048);SPI_pop_conditional(pushed);

	{Datum  ReplaceReturn27 = result; Din_Go(267,2048); return ReplaceReturn27;}
}

/*
 * Call a previously-looked-up datatype output function.
 *
 * Do not call this on NULL datums.
 *
 * This is almost just window dressing for FunctionCall1, but it includes
 * SPI context pushing for the same reasons as InputFunctionCall.
 */
char *
OutputFunctionCall(FmgrInfo *flinfo, Datum val)
{
	Din_Go(268,2048);char	   *result;
	bool		pushed;

	pushed = SPI_push_conditional();

	result = DatumGetCString(FunctionCall1(flinfo, val));

	SPI_pop_conditional(pushed);

	{char * ReplaceReturn26 = result; Din_Go(269,2048); return ReplaceReturn26;}
}

/*
 * Call a previously-looked-up datatype binary-input function.
 *
 * "buf" may be NULL to indicate we are reading a NULL.  In this case
 * the caller should assume the result is NULL, but we'll call the receive
 * function anyway if it's not strict.  So this is almost but not quite
 * the same as FunctionCall3.  Also, this includes SPI context pushing for
 * the same reasons as InputFunctionCall.
 */
Datum
ReceiveFunctionCall(FmgrInfo *flinfo, StringInfo buf,
					Oid typioparam, int32 typmod)
{
	Din_Go(270,2048);FunctionCallInfoData fcinfo;
	Datum		result;
	bool		pushed;

	Din_Go(272,2048);if (buf == NULL && flinfo->fn_strict)
		{/*13*/{Datum  ReplaceReturn25 = (Datum) 0; Din_Go(271,2048); return ReplaceReturn25;}/*14*/}		/* just return null result */

	Din_Go(273,2048);pushed = SPI_push_conditional();

	InitFunctionCallInfoData(fcinfo, flinfo, 3, InvalidOid, NULL, NULL);

	fcinfo.arg[0] = PointerGetDatum(buf);
	fcinfo.arg[1] = ObjectIdGetDatum(typioparam);
	fcinfo.arg[2] = Int32GetDatum(typmod);
	fcinfo.argnull[0] = (buf == NULL);
	fcinfo.argnull[1] = false;
	fcinfo.argnull[2] = false;

	result = FunctionCallInvoke(&fcinfo);

	/* Should get null result if and only if buf is NULL */
	Din_Go(276,2048);if (buf == NULL)
	{
		Din_Go(274,2048);if (!fcinfo.isnull)
			elog(ERROR, "receive function %u returned non-NULL",
				 fcinfo.flinfo->fn_oid);
	}
	else
	{
		Din_Go(275,2048);if (fcinfo.isnull)
			elog(ERROR, "receive function %u returned NULL",
				 fcinfo.flinfo->fn_oid);
	}

	Din_Go(277,2048);SPI_pop_conditional(pushed);

	{Datum  ReplaceReturn24 = result; Din_Go(278,2048); return ReplaceReturn24;}
}

/*
 * Call a previously-looked-up datatype binary-output function.
 *
 * Do not call this on NULL datums.
 *
 * This is little more than window dressing for FunctionCall1, but it does
 * guarantee a non-toasted result, which strictly speaking the underlying
 * function doesn't.  Also, this includes SPI context pushing for the same
 * reasons as InputFunctionCall.
 */
bytea *
SendFunctionCall(FmgrInfo *flinfo, Datum val)
{
	Din_Go(279,2048);bytea	   *result;
	bool		pushed;

	pushed = SPI_push_conditional();

	result = DatumGetByteaP(FunctionCall1(flinfo, val));

	SPI_pop_conditional(pushed);

	{bytea * ReplaceReturn23 = result; Din_Go(280,2048); return ReplaceReturn23;}
}

/*
 * As above, for I/O functions identified by OID.  These are only to be used
 * in seldom-executed code paths.  They are not only slow but leak memory.
 */
Datum
OidInputFunctionCall(Oid functionId, char *str, Oid typioparam, int32 typmod)
{
	Din_Go(281,2048);FmgrInfo	flinfo;

	fmgr_info(functionId, &flinfo);
	{Datum  ReplaceReturn22 = InputFunctionCall(&flinfo, str, typioparam, typmod); Din_Go(282,2048); return ReplaceReturn22;}
}

char *
OidOutputFunctionCall(Oid functionId, Datum val)
{
	Din_Go(283,2048);FmgrInfo	flinfo;

	fmgr_info(functionId, &flinfo);
	{char * ReplaceReturn21 = OutputFunctionCall(&flinfo, val); Din_Go(284,2048); return ReplaceReturn21;}
}

Datum
OidReceiveFunctionCall(Oid functionId, StringInfo buf,
					   Oid typioparam, int32 typmod)
{
	Din_Go(285,2048);FmgrInfo	flinfo;

	fmgr_info(functionId, &flinfo);
	{Datum  ReplaceReturn20 = ReceiveFunctionCall(&flinfo, buf, typioparam, typmod); Din_Go(286,2048); return ReplaceReturn20;}
}

bytea *
OidSendFunctionCall(Oid functionId, Datum val)
{
	Din_Go(287,2048);FmgrInfo	flinfo;

	fmgr_info(functionId, &flinfo);
	{bytea * ReplaceReturn19 = SendFunctionCall(&flinfo, val); Din_Go(288,2048); return ReplaceReturn19;}
}


/*
 * !!! OLD INTERFACE !!!
 *
 * fmgr() is the only remaining vestige of the old-style caller support
 * functions.  It's no longer used anywhere in the Postgres distribution,
 * but we should leave it around for a release or two to ease the transition
 * for user-supplied C functions.  OidFunctionCallN() replaces it for new
 * code.
 *
 * DEPRECATED, DO NOT USE IN NEW CODE
 */
char *
fmgr(Oid procedureId,...)
{
	Din_Go(289,2048);FmgrInfo	flinfo;
	FunctionCallInfoData fcinfo;
	int			n_arguments;
	Datum		result;

	fmgr_info(procedureId, &flinfo);

	MemSet(&fcinfo, 0, sizeof(fcinfo));
	fcinfo.flinfo = &flinfo;
	fcinfo.nargs = flinfo.fn_nargs;
	n_arguments = fcinfo.nargs;

	Din_Go(293,2048);if (n_arguments > 0)
	{
		Din_Go(290,2048);va_list		pvar;
		int			i;

		Din_Go(291,2048);if (n_arguments > FUNC_MAX_ARGS)
			ereport(ERROR,
					(errcode(ERRCODE_TOO_MANY_ARGUMENTS),
			 errmsg("function %u has too many arguments (%d, maximum is %d)",
					flinfo.fn_oid, n_arguments, FUNC_MAX_ARGS)));
		va_start(pvar, procedureId);
		Din_Go(292,2048);for (i = 0; i < n_arguments; i++)
			fcinfo.arg[i] = PointerGetDatum(va_arg(pvar, char *));
		va_end(pvar);
	}

	Din_Go(294,2048);result = FunctionCallInvoke(&fcinfo);

	/* Check for null result, since caller is clearly not expecting one */
	Din_Go(295,2048);if (fcinfo.isnull)
		elog(ERROR, "function %u returned NULL", flinfo.fn_oid);

	{char * ReplaceReturn18 = DatumGetPointer(result); Din_Go(296,2048); return ReplaceReturn18;}
}


/*-------------------------------------------------------------------------
 *		Support routines for standard maybe-pass-by-reference datatypes
 *
 * int8, float4, and float8 can be passed by value if Datum is wide enough.
 * (For backwards-compatibility reasons, we allow pass-by-ref to be chosen
 * at compile time even if pass-by-val is possible.)  For the float types,
 * we need a support routine even if we are passing by value, because many
 * machines pass int and float function parameters/results differently;
 * so we need to play weird games with unions.
 *
 * Note: there is only one switch controlling the pass-by-value option for
 * both int8 and float8; this is to avoid making things unduly complicated
 * for the timestamp types, which might have either representation.
 *-------------------------------------------------------------------------
 */

#ifndef USE_FLOAT8_BYVAL		/* controls int8 too */

Datum
Int64GetDatum(int64 X)
{
	int64	   *retval = (int64 *) palloc(sizeof(int64));

	*retval = X;
	return PointerGetDatum(retval);
}
#endif   /* USE_FLOAT8_BYVAL */

Datum
Float4GetDatum(float4 X)
{
#ifdef USE_FLOAT4_BYVAL
	Din_Go(297,2048);union
	{
		float4		value;
		int32		retval;
	}			myunion;

	myunion.value = X;
	{Datum  ReplaceReturn17 = SET_4_BYTES(myunion.retval); Din_Go(298,2048); return ReplaceReturn17;}
#else
	float4	   *retval = (float4 *) palloc(sizeof(float4));

	*retval = X;
	return PointerGetDatum(retval);
#endif
}

#ifdef USE_FLOAT4_BYVAL

float4
DatumGetFloat4(Datum X)
{
	Din_Go(299,2048);union
	{
		int32		value;
		float4		retval;
	}			myunion;

	myunion.value = GET_4_BYTES(X);
	{float4  ReplaceReturn16 = myunion.retval; Din_Go(300,2048); return ReplaceReturn16;}
}
#endif   /* USE_FLOAT4_BYVAL */

Datum
Float8GetDatum(float8 X)
{
#ifdef USE_FLOAT8_BYVAL
	Din_Go(301,2048);union
	{
		float8		value;
		int64		retval;
	}			myunion;

	myunion.value = X;
	{Datum  ReplaceReturn15 = SET_8_BYTES(myunion.retval); Din_Go(302,2048); return ReplaceReturn15;}
#else
	float8	   *retval = (float8 *) palloc(sizeof(float8));

	*retval = X;
	return PointerGetDatum(retval);
#endif
}

#ifdef USE_FLOAT8_BYVAL

float8
DatumGetFloat8(Datum X)
{
	Din_Go(303,2048);union
	{
		int64		value;
		float8		retval;
	}			myunion;

	myunion.value = GET_8_BYTES(X);
	{float8  ReplaceReturn14 = myunion.retval; Din_Go(304,2048); return ReplaceReturn14;}
}
#endif   /* USE_FLOAT8_BYVAL */


/*-------------------------------------------------------------------------
 *		Support routines for toastable datatypes
 *-------------------------------------------------------------------------
 */

struct varlena *
pg_detoast_datum(struct varlena * datum)
{
	Din_Go(305,2048);if (VARATT_IS_EXTENDED(datum))
		{/*1*/{__IASTRUCT__ varlena * ReplaceReturn13 = heap_tuple_untoast_attr(datum); Din_Go(306,2048); return ReplaceReturn13;}/*2*/}
	else
		{/*3*/{__IASTRUCT__ varlena * ReplaceReturn12 = datum; Din_Go(307,2048); return ReplaceReturn12;}/*4*/}
Din_Go(308,2048);}

struct varlena *
pg_detoast_datum_copy(struct varlena * datum)
{
	Din_Go(309,2048);if (VARATT_IS_EXTENDED(datum))
		{/*5*/{__IASTRUCT__ varlena * ReplaceReturn11 = heap_tuple_untoast_attr(datum); Din_Go(310,2048); return ReplaceReturn11;}/*6*/}
	else
	{
		/* Make a modifiable copy of the varlena object */
		Din_Go(311,2048);Size		len = VARSIZE(datum);
		struct varlena *result = (struct varlena *) palloc(len);

		memcpy(result, datum, len);
		{__IASTRUCT__ varlena * ReplaceReturn10 = result; Din_Go(312,2048); return ReplaceReturn10;}
	}
Din_Go(313,2048);}

struct varlena *
pg_detoast_datum_slice(struct varlena * datum, int32 first, int32 count)
{
	/* Only get the specified portion from the toast rel */
	{__IASTRUCT__ varlena * ReplaceReturn9 = heap_tuple_untoast_attr_slice(datum, first, count); Din_Go(314,2048); return ReplaceReturn9;}
}

struct varlena *
pg_detoast_datum_packed(struct varlena * datum)
{
	Din_Go(315,2048);if (VARATT_IS_COMPRESSED(datum) || VARATT_IS_EXTERNAL(datum))
		{/*7*/{__IASTRUCT__ varlena * ReplaceReturn8 = heap_tuple_untoast_attr(datum); Din_Go(316,2048); return ReplaceReturn8;}/*8*/}
	else
		{/*9*/{__IASTRUCT__ varlena * ReplaceReturn7 = datum; Din_Go(317,2048); return ReplaceReturn7;}/*10*/}
Din_Go(318,2048);}

/*-------------------------------------------------------------------------
 *		Support routines for extracting info from fn_expr parse tree
 *
 * These are needed by polymorphic functions, which accept multiple possible
 * input types and need help from the parser to know what they've got.
 * Also, some functions might be interested in whether a parameter is constant.
 * Functions taking VARIADIC ANY also need to know about the VARIADIC keyword.
 *-------------------------------------------------------------------------
 */

/*
 * Get the actual type OID of the function return type
 *
 * Returns InvalidOid if information is not available
 */
Oid
get_fn_expr_rettype(FmgrInfo *flinfo)
{
	Din_Go(319,2048);Node	   *expr;

	/*
	 * can't return anything useful if we have no FmgrInfo or if its fn_expr
	 * node has not been initialized
	 */
	Din_Go(320,2048);if (!flinfo || !flinfo->fn_expr)
		return InvalidOid;

	Din_Go(321,2048);expr = flinfo->fn_expr;

	{Oid  ReplaceReturn6 = exprType(expr); Din_Go(322,2048); return ReplaceReturn6;}
}

/*
 * Get the actual type OID of a specific function argument (counting from 0)
 *
 * Returns InvalidOid if information is not available
 */
Oid
get_fn_expr_argtype(FmgrInfo *flinfo, int argnum)
{
	/*
	 * can't return anything useful if we have no FmgrInfo or if its fn_expr
	 * node has not been initialized
	 */
	Din_Go(323,2048);if (!flinfo || !flinfo->fn_expr)
		return InvalidOid;

	{Oid  ReplaceReturn5 = get_call_expr_argtype(flinfo->fn_expr, argnum); Din_Go(324,2048); return ReplaceReturn5;}
}

/*
 * Get the actual type OID of a specific function argument (counting from 0),
 * but working from the calling expression tree instead of FmgrInfo
 *
 * Returns InvalidOid if information is not available
 */
Oid
get_call_expr_argtype(Node *expr, int argnum)
{
	Din_Go(325,2048);List	   *args;
	Oid			argtype;

	Din_Go(326,2048);if (expr == NULL)
		return InvalidOid;

	Din_Go(328,2048);if (IsA(expr, FuncExpr))
		{/*17*/Din_Go(327,2048);args = ((FuncExpr *) expr)->args;/*18*/}
	else if (IsA(expr, OpExpr))
		args = ((OpExpr *) expr)->args;
	else if (IsA(expr, DistinctExpr))
		args = ((DistinctExpr *) expr)->args;
	else if (IsA(expr, ScalarArrayOpExpr))
		args = ((ScalarArrayOpExpr *) expr)->args;
	else if (IsA(expr, ArrayCoerceExpr))
		args = list_make1(((ArrayCoerceExpr *) expr)->arg);
	else if (IsA(expr, NullIfExpr))
		args = ((NullIfExpr *) expr)->args;
	else if (IsA(expr, WindowFunc))
		args = ((WindowFunc *) expr)->args;
	else
		return InvalidOid;

	Din_Go(329,2048);if (argnum < 0 || argnum >= list_length(args))
		return InvalidOid;

	Din_Go(330,2048);argtype = exprType((Node *) list_nth(args, argnum));

	/*
	 * special hack for ScalarArrayOpExpr and ArrayCoerceExpr: what the
	 * underlying function will actually get passed is the element type of the
	 * array.
	 */
	Din_Go(334,2048);if (IsA(expr, ScalarArrayOpExpr) &&
		argnum == 1)
		{/*19*/Din_Go(331,2048);argtype = get_base_element_type(argtype);/*20*/}
	else {/*21*/Din_Go(332,2048);if (IsA(expr, ArrayCoerceExpr) &&
			 argnum == 0)
		{/*23*/Din_Go(333,2048);argtype = get_base_element_type(argtype);/*24*/}/*22*/}

	{Oid  ReplaceReturn4 = argtype; Din_Go(335,2048); return ReplaceReturn4;}
}

/*
 * Find out whether a specific function argument is constant for the
 * duration of a query
 *
 * Returns false if information is not available
 */
bool
get_fn_expr_arg_stable(FmgrInfo *flinfo, int argnum)
{
	/*
	 * can't return anything useful if we have no FmgrInfo or if its fn_expr
	 * node has not been initialized
	 */
	Din_Go(336,2048);if (!flinfo || !flinfo->fn_expr)
		return false;

	{bool  ReplaceReturn3 = get_call_expr_arg_stable(flinfo->fn_expr, argnum); Din_Go(337,2048); return ReplaceReturn3;}
}

/*
 * Find out whether a specific function argument is constant for the
 * duration of a query, but working from the calling expression tree
 *
 * Returns false if information is not available
 */
bool
get_call_expr_arg_stable(Node *expr, int argnum)
{
	Din_Go(338,2048);List	   *args;
	Node	   *arg;

	Din_Go(339,2048);if (expr == NULL)
		return false;

	Din_Go(341,2048);if (IsA(expr, FuncExpr))
		{/*25*/Din_Go(340,2048);args = ((FuncExpr *) expr)->args;/*26*/}
	else if (IsA(expr, OpExpr))
		args = ((OpExpr *) expr)->args;
	else if (IsA(expr, DistinctExpr))
		args = ((DistinctExpr *) expr)->args;
	else if (IsA(expr, ScalarArrayOpExpr))
		args = ((ScalarArrayOpExpr *) expr)->args;
	else if (IsA(expr, ArrayCoerceExpr))
		args = list_make1(((ArrayCoerceExpr *) expr)->arg);
	else if (IsA(expr, NullIfExpr))
		args = ((NullIfExpr *) expr)->args;
	else if (IsA(expr, WindowFunc))
		args = ((WindowFunc *) expr)->args;
	else
		return false;

	Din_Go(342,2048);if (argnum < 0 || argnum >= list_length(args))
		return false;

	Din_Go(343,2048);arg = (Node *) list_nth(args, argnum);

	/*
	 * Either a true Const or an external Param will have a value that doesn't
	 * change during the execution of the query.  In future we might want to
	 * consider other cases too, e.g. now().
	 */
	Din_Go(344,2048);if (IsA(arg, Const))
		return true;
	Din_Go(345,2048);if (IsA(arg, Param) &&
		((Param *) arg)->paramkind == PARAM_EXTERN)
		return true;

	{bool  ReplaceReturn2 = false; Din_Go(346,2048); return ReplaceReturn2;}
}

/*
 * Get the VARIADIC flag from the function invocation
 *
 * Returns false (the default assumption) if information is not available
 *
 * Note this is generally only of interest to VARIADIC ANY functions
 */
bool
get_fn_expr_variadic(FmgrInfo *flinfo)
{
	Din_Go(347,2048);Node	   *expr;

	/*
	 * can't return anything useful if we have no FmgrInfo or if its fn_expr
	 * node has not been initialized
	 */
	Din_Go(348,2048);if (!flinfo || !flinfo->fn_expr)
		return false;

	Din_Go(349,2048);expr = flinfo->fn_expr;

	Din_Go(351,2048);if (IsA(expr, FuncExpr))
		{/*27*/{bool  ReplaceReturn1 = ((FuncExpr *) expr)->funcvariadic; Din_Go(350,2048); return ReplaceReturn1;}/*28*/}
	else
		return false;
Din_Go(352,2048);}

/*-------------------------------------------------------------------------
 *		Support routines for procedural language implementations
 *-------------------------------------------------------------------------
 */

/*
 * Verify that a validator is actually associated with the language of a
 * particular function and that the user has access to both the language and
 * the function.  All validators should call this before doing anything
 * substantial.  Doing so ensures a user cannot achieve anything with explicit
 * calls to validators that he could not achieve with CREATE FUNCTION or by
 * simply calling an existing function.
 *
 * When this function returns false, callers should skip all validation work
 * and call PG_RETURN_VOID().  This never happens at present; it is reserved
 * for future expansion.
 *
 * In particular, checking that the validator corresponds to the function's
 * language allows untrusted language validators to assume they process only
 * superuser-chosen source code.  (Untrusted language call handlers, by
 * definition, do assume that.)  A user lacking the USAGE language privilege
 * would be unable to reach the validator through CREATE FUNCTION, so we check
 * that to block explicit calls as well.  Checking the EXECUTE privilege on
 * the function is often superfluous, because most users can clone the
 * function to get an executable copy.  It is meaningful against users with no
 * database TEMP right and no permanent schema CREATE right, thereby unable to
 * create any function.  Also, if the function tracks persistent state by
 * function OID or name, validating the original function might permit more
 * mischief than creating and validating a clone thereof.
 */
bool
CheckFunctionValidatorAccess(Oid validatorOid, Oid functionOid)
{
	Din_Go(353,2048);HeapTuple	procTup;
	HeapTuple	langTup;
	Form_pg_proc procStruct;
	Form_pg_language langStruct;
	AclResult	aclresult;

	/* Get the function's pg_proc entry */
	procTup = SearchSysCache1(PROCOID, ObjectIdGetDatum(functionOid));
	Din_Go(354,2048);if (!HeapTupleIsValid(procTup))
		elog(ERROR, "cache lookup failed for function %u", functionOid);
	Din_Go(355,2048);procStruct = (Form_pg_proc) GETSTRUCT(procTup);

	/*
	 * Fetch pg_language entry to know if this is the correct validation
	 * function for that pg_proc entry.
	 */
	langTup = SearchSysCache1(LANGOID, ObjectIdGetDatum(procStruct->prolang));
	Din_Go(356,2048);if (!HeapTupleIsValid(langTup))
		elog(ERROR, "cache lookup failed for language %u", procStruct->prolang);
	Din_Go(357,2048);langStruct = (Form_pg_language) GETSTRUCT(langTup);

	Din_Go(358,2048);if (langStruct->lanvalidator != validatorOid)
		ereport(ERROR,
				(errcode(ERRCODE_INSUFFICIENT_PRIVILEGE),
				 errmsg("language validation function %u called for language %u instead of %u",
						validatorOid, procStruct->prolang,
						langStruct->lanvalidator)));

	/* first validate that we have permissions to use the language */
	Din_Go(359,2048);aclresult = pg_language_aclcheck(procStruct->prolang, GetUserId(),
									 ACL_USAGE);
	Din_Go(361,2048);if (aclresult != ACLCHECK_OK)
		{/*29*/Din_Go(360,2048);aclcheck_error(aclresult, ACL_KIND_LANGUAGE,
					   NameStr(langStruct->lanname));/*30*/}

	/*
	 * Check whether we are allowed to execute the function itself. If we can
	 * execute it, there should be no possible side-effect of
	 * compiling/validation that execution can't have.
	 */
	Din_Go(362,2048);aclresult = pg_proc_aclcheck(functionOid, GetUserId(), ACL_EXECUTE);
	Din_Go(364,2048);if (aclresult != ACLCHECK_OK)
		{/*31*/Din_Go(363,2048);aclcheck_error(aclresult, ACL_KIND_PROC, NameStr(procStruct->proname));/*32*/}

	Din_Go(365,2048);ReleaseSysCache(procTup);
	ReleaseSysCache(langTup);

	{bool  ReplaceReturn0 = true; Din_Go(366,2048); return ReplaceReturn0;}
}
