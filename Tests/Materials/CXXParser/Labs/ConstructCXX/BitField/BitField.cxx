#include <iostream>
#include "var/tmp/sensor.h"
struct S {
    // will usually occupy 2 bytes:
    // 3 bits: value of b1
    // 5 bits: unused
    // 6 bits: value of b2
    // 2 bits: value of b3
    unsigned char b1 : 3;
    unsigned char :0; // start a new byte
    unsigned char b2 : 6;
    unsigned char b3 : 2;
};
int main()
{
    SensorCall(1);std::cout << sizeof(S) << '\n'; // usually prints 2
SensorCall(2);}