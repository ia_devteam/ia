#include <iostream>
#include "var/tmp/sensor.h"
 
namespace foo {
    namespace bar {
         namespace baz {
             int qux = 42;
         }
    }
}
 
namespace fbz = foo::bar::baz;
 
int main()
{
    SensorCall(1);std::cout << fbz::qux << '\n';
SensorCall(2);}