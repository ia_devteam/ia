#include <iostream>
#include "var/tmp/sensor.h"
 
// enum that takes 16 bits
enum smallenum //: int16_t
{
    a,
    b,
    c
};
 
 
// color may be red (value 0), yellow (value 1), green (value 20), or blue (value 21)
enum color
{
    red,
    yellow,
    green = 20,
    blue
};
 
// altitude may be altitude::high or altitude::low
//enum class altitude: char
//{ 
//     high='h',
//     low='l', // C++11 allows the extra comma
//}; 
 
// the constant d is 0, the constant e is 1, the constant f is 3
enum
{
    d,
    e,
    f = e + 2
};
 
//enumeration types (both scoped and unscoped) can have overloaded operators
std::ostream& operator<<(std::ostream& os, color c)
{
    SensorCall(1);switch(c)
    {
        case red   : SensorCall(2);os << "red";    SensorCall(3);break;
        case yellow: SensorCall(4);os << "yellow"; SensorCall(5);break;
        case green : SensorCall(6);os << "green";  SensorCall(7);break;
        case blue  : SensorCall(8);os << "blue";   SensorCall(9);break;
        default    : SensorCall(10);os.setstate(std::ios_base::failbit);
    }
    {std::ostream & ReplaceReturn0 = os; SensorCall(11); return ReplaceReturn0;}
}
 
//std::ostream& operator<<(std::ostream& os, altitude al)
//{
//    return os << static_cast<char>(al);
//}
 
int main()
{
    SensorCall(12);color col = red;
    //altitude a;
    //a = altitude::low;
 
    std::cout << "col = " << col << '\n'
              << "a = "   << ""  << '\n'
              << "f = "   << f   << '\n';
SensorCall(13);}
