#include <iostream>
#include "var/tmp/sensor.h"
#include <algorithm>
#include <string>
#include <cctype> // int std::toupper(int)
#include <locale> // template<class CharT> CharT std::toupper(CharT,const locale&)
 
int f(int) { {int  ReplaceReturn4 = 1; SensorCall(1); return ReplaceReturn4;} }
int f(double) { {int  ReplaceReturn3 = 2; SensorCall(2); return ReplaceReturn3;} }
 
void g( int(&f1)(int), int(*f2)(double) ) {SensorCall(3);}
 
template< int(*F)(int) >
struct Templ {};
 
struct Foo {
    int mf(int) { {int  ReplaceReturn2 = 3; SensorCall(4); return ReplaceReturn2;} }
    int mf(double) { {int  ReplaceReturn1 = 4; SensorCall(5); return ReplaceReturn1;} }
};
 
// 5. return value
int (*(foo)(void))(int) {
    {int  ReplaceReturn0 = f; SensorCall(6); return ReplaceReturn0;} // selects int f(int)
}
 
int main()
{
    // 1. initialization
    SensorCall(7);int (*pf)(double) = f; // selects int f(double)
    int (&rf)(int) = f; // selects int f(int)
    int (Foo::*mpf)(int) = &Foo::mf; // selects int mf(int)
 
    // 2. assignment
    pf = NULL;
    pf = &f; // selects int f(double)
 
    // 3. function argument
    g(f, f); // selects int f(int) for the 1st argument
             // and int f(double) for the second
 
    // 4. user-defined operator
 
    // 6. cast
    std::string str = "example";
    std::transform(str.begin(), str.end(), str.begin(),
                   static_cast<int(*)(int)>(std::toupper)); // selects int toupper(int)
    std::cout << str << '\n';
 
    // 7. template argument
    Templ<f> t;  // selects int f(int)
SensorCall(8);}
