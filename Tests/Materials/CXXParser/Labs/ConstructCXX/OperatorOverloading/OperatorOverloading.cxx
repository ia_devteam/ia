#include <iostream>
#include "var/tmp/sensor.h"
 
class Fraction
{
    int gcd(int a, int b) { {int  ReplaceReturn = b == 0 ? a : gcd(b, a % b); SensorCall(); return ReplaceReturn;} }
    int n, d;
public:
    Fraction(int n, int d = 1) : n(n/gcd(n, d)), d(d/gcd(n, d)) {SensorCall(); SensorCall();}
    int num() const { {int  ReplaceReturn = n; SensorCall(); return ReplaceReturn;} }
    int den() const { {int  ReplaceReturn = d; SensorCall(); return ReplaceReturn;} }
    Fraction& operator*=(const Fraction& rhs)
    {
        SensorCall();int new_n = n * rhs.n/gcd(n * rhs.n, d * rhs.d);
        d = d * rhs.d/gcd(n * rhs.n, d * rhs.d);
        n = new_n;
        SensorCall();return *this;
    }
};
std::ostream& operator<<(std::ostream& out, const Fraction& f)
{
   {std::ostream & ReplaceReturn = out << f.num() << '/' << f.den() ; SensorCall(); return ReplaceReturn;}
}
bool operator==(const Fraction& lhs, const Fraction& rhs)
{
    SensorCall();return lhs.num() == rhs.num() && lhs.den() == rhs.den();
}
bool operator!=(const Fraction& lhs, const Fraction& rhs)
{
    SensorCall();return !(lhs == rhs);
}
Fraction operator*(Fraction lhs, const Fraction& rhs)
{
    SensorCall();return lhs *= rhs;
}
 
int main()
{
   SensorCall();Fraction f1(3, 8), f2(1, 2), f3(10, 2);
   std::cout << f1 << " * " << f2 << " = " << f1 * f2 << '\n'
             << f2 << " * " << f3 << " = " << f2 * f3 << '\n'
             <<  2 << " * " << f1 << " = " <<  2 * f1 << '\n';
SensorCall();}