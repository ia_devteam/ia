#include "var/tmp/sensor.h"
// template_specifications1.cpp
template <class T, int i> class TestClass
{
public:
	char buffer[i];
	T testFunc(T* p1);
};

template <class T, int i>
T TestClass<T, i>::testFunc(T* p1)
{
	SensorCall(1);return *(p1++);
};

// To create an instance of TestClass
TestClass<char, 5> ClassInst;
int main()
{
	SensorCall(2);ClassInst.buffer[0] = 'h';
	ClassInst.buffer[1] = 'e';
	ClassInst.buffer[2] = 'l';
	ClassInst.buffer[3] = 'l';
	ClassInst.buffer[4] = '0';
	ClassInst.testFunc(&ClassInst.buffer[0]);
SensorCall(3);}