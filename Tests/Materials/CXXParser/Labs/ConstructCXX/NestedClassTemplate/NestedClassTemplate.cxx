// nested_class_template2.cpp
// compile with: /EHsc
#include <iostream>
#include "var/tmp/sensor.h"
using namespace std;

template <class T>
class X
{
	template <class U> class Y
	{
		U* u;
	public:
		Y();
		U& Value();
		void print();
		~Y();
	};

	Y<int> y;
public:
	X(T t) { SensorCall(1);y.Value() = t; SensorCall(2);}
	void print() { SensorCall(3);y.print(); SensorCall(4);}
};

template <class T>
template <class U>
X<T>::Y<U>::Y()
{
	SensorCall(5);cout << "X<T>::Y<U>::Y()" << endl;
	u = new U();
SensorCall(6);}

template <class T>
template <class U>
U& X<T>::Y<U>::Value()
{
	SensorCall(7);return *u;
}

template <class T>
template <class U>
void X<T>::Y<U>::print()
{
	SensorCall(8);cout << this->Value() << endl;
SensorCall(9);}

template <class T>
template <class U>
X<T>::Y<U>::~Y()
{
	SensorCall(10);cout << "X<T>::Y<U>::~Y()" << endl;
	delete u;
SensorCall(11);}

int main()
{
	SensorCall(12);X<int>* xi = new X<int>(10);
	X<char>* xc = new X<char>('c');
	xi->print();
	xc->print();
	delete xi;
	delete xc;
SensorCall(13);}