#define ABCD 2
#include <iostream>
#include "var/tmp/sensor.h"
 
int main()
{
	SensorCall(1);int if_int = ABCD;
#ifdef ABCD
    std::cout << "1: yes\n";
#else
    std::cout << "1: no\n";
#endif
 
#ifndef ABCD
    std::cout << "2: no1\n";
#elif ABCD == 2
	SensorCall(3);if (if_int)
    {/*1*/SensorCall(2);std::cout << "2: yes\n";/*2*/}
#else
    std::cout << "2: no2\n";
#endif
 
#if !defined(DCBA) && (ABCD < 2*4-3)
    SensorCall(4);std::cout << "3: yes\n";
#endif
SensorCall(5);}