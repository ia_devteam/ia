#include <iostream>
#include "var/tmp/sensor.h"

struct A
{
	int i;

	A(int i) : i(i) {SensorCall(1);}

	~A()
	{
		SensorCall(2);std::cout << "~a" << i << std::endl;
	SensorCall(3);}
};

int main()
{
	SensorCall(4);A a1(1);
	A* p;

	{ // nested scope
		A a2(2);
		p = new A(3);
	} // a2 out of scope

	delete p; // calls the destructor of a3
SensorCall(5);}