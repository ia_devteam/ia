// IfElseStatements.cxx 
#include <stdio.h>  
#include "var/tmp/sensor.h"
  
int main()   
{  
   SensorCall(1);int x = 0;  
   SensorCall(4);if (x == 0)  
   {  
      SensorCall(2);printf("x is 0!\n");  
   }  
   else  
   {  
      SensorCall(3);printf("x is not 0!\n"); // this statement will not be executed  
   }  
  
   SensorCall(5);x = 2;  
   SensorCall(10);if (x == 0)  
   {  
      SensorCall(6);printf("x is 0!\n"); // this statement will not be executed  
   }  
   else {/*1*/SensorCall(7);if (x == 1) 
   {  
      SensorCall(8);printf("x is 1!\n");  
   }
   else
	   {/*3*/SensorCall(9);printf("x is greater than 1!\n");/*4*/}/*2*/}
  
   {int  ReplaceReturn0 = 0; SensorCall(11); return ReplaceReturn0;}  
}  