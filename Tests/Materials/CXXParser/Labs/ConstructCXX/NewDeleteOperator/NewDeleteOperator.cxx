// new_op_new.cpp
// compile with: /EHsc
#include<new>
#include "var/tmp/sensor.h"
#include<iostream>

using namespace std;

class MyClass 
{
public: 
   MyClass( )
   {
      SensorCall(1);cout << "Construction MyClass." << this << endl;
   SensorCall(2);};

   ~MyClass( )
   {
      SensorCall(3);imember = 0; cout << "Destructing MyClass." << this << endl;
   SensorCall(4);};
   int imember;
};

int main( ) 
{
   // The first form of new delete
   SensorCall(5);MyClass* fPtr = new MyClass;
   delete fPtr;

   // The second form of new delete
   MyClass* fPtr2 = new( nothrow ) MyClass;
   delete fPtr2;

   // The third form of new delete
   char x[sizeof( MyClass )];
   MyClass* fPtr3 = new( &x[0] ) MyClass;
   fPtr3 -> ~MyClass();
   cout << "The address of x[0] is : " << ( void* )&x[0] << endl;
SensorCall(6);}