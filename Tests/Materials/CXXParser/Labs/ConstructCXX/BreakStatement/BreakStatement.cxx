// BreakStatement.cxx
#include <iostream>  
#include "var/tmp/sensor.h"
using namespace std;  
enum Suit{ Diamonds, Hearts, Clubs, Spades }; 

int main()  
{  
    // An example of a standard for loop  
    SensorCall(1);for (int i = 1; i < 10; i++)  
    {  
        SensorCall(2);cout << i << '\n';  
        SensorCall(4);if (i == 4)  
            {/*1*/SensorCall(3);break;/*2*/}  
    }  
  
    // An example of a range-based for loop  
	SensorCall(5);int nums [] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};  
  
    SensorCall(9);for (int i : nums) {  
        SensorCall(6);if (i == 4) {  
            SensorCall(7);break;  
        }  
        SensorCall(8);cout << i << '\n';  
    }  
   
	SensorCall(10);int i = 0;  
  
    SensorCall(14);while (i < 10) {  
        SensorCall(11);if (i == 4) {  
            SensorCall(12);break;  
        }  
        SensorCall(13);cout << i << '\n';  
        i++;  
    }  
  
    SensorCall(15);i = 0;  
    SensorCall(19);do {  
        SensorCall(16);if (i == 4) {  
            SensorCall(17);break;  
        }  
        SensorCall(18);cout << i << '\n';  
        i++;  
    } while (i < 10);  
	
	SensorCall(20);Suit hand; 
    // Assume that some enum value is set for hand  
    // In this example, each case is handled separately  
    SensorCall(30);switch (hand)  
    {  
    case Diamonds:  
        SensorCall(21);cout << "got Diamonds \n";  
        SensorCall(22);break;  
    case Hearts:  
        SensorCall(23);cout << "got Hearts \n";  
        SensorCall(24);break;  
    case Clubs:  
        SensorCall(25);cout << "got Clubs \n";  
        SensorCall(26);break;  
    case Spades:  
        SensorCall(27);cout << "got Spades \n";  
        SensorCall(28);break;  
    default:   
          SensorCall(29);cout << "didn't get card \n";  
    }  
    // In this example, Diamonds and Hearts are handled one way, and  
    // Clubs, Spades, and the default value are handled another way  
    SensorCall(34);switch (hand)  
    {  
    case Diamonds:  
    case Hearts:  
        SensorCall(31);cout << "got a red card \n";  
        SensorCall(32);break;  
    case Clubs:  
    case Spades:   
    default:  
        SensorCall(33);cout << "didn't get a red card \n";  
    }  
   
SensorCall(35);}  
