#include <iostream>
#include "var/tmp/sensor.h"
using namespace std;

#include <iostream>
#include <algorithm>
#include <string>
 
int main()
{
    SensorCall(1);int j = 2;
    SensorCall(3);do { // compound statement is the loop body
        SensorCall(2);j += 2;
        std::cout << j << " ";
    } while (j < 9);
    SensorCall(4);std::cout << '\n';
    // common situation where do-while loop is used
    std::string s = "aba";
    std::sort(s.begin(), s.end());
    SensorCall(6);do {/*1*/SensorCall(5);std::cout << s << '\n';/*2*/} // expression statement is the loop body
    while(std::next_permutation(s.begin(), s.end()));
SensorCall(7);}