#include <iostream>
#include "var/tmp/sensor.h"
#include <vector>
#include <algorithm>

struct Printer { // generic functor
	std::ostream& os;
	Printer(std::ostream& os) : os(os) {SensorCall(1);}
	template<typename T>
	void operator()(const T& obj) { SensorCall(2);os << obj << ' '; SensorCall(3);} // member template
};

int main()
{
	SensorCall(4);std::vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	std::for_each(v.begin(), v.end(), Printer(std::cout));
	std::string s = "abc";
	std::for_each(s.begin(), s.end(), Printer(std::cout));
SensorCall(5);}