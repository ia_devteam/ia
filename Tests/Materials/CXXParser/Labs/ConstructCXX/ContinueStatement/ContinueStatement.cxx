// ContinueStatement.cxx  
#include <stdio.h>  
#include "var/tmp/sensor.h"
int main()  
{  
    SensorCall(1);int i = 0;  
    SensorCall(4);do  
    {  
        SensorCall(2);i++;  
        printf("before the continue\n");  
        SensorCall(3);continue;  
        printf("after the continue, should never print\n");  
     } while (i < 3);  
  
     SensorCall(5);printf("after the do loop\n");  
SensorCall(6);}  
