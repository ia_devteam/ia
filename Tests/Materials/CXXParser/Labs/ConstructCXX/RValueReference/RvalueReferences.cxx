#include <iostream>
#include "var/tmp/sensor.h"
#include <string>
#include <utility>

void f(int& x)
{
	SensorCall(1);std::cout << "lvalue reference overload f(" << x << ")\n";
SensorCall(2);}

void f(const int& x)
{
	SensorCall(3);std::cout << "lvalue reference to const overload f(" << x << ")\n";
SensorCall(4);}

void f(int&& x)
{
	SensorCall(5);std::cout << "rvalue reference overload f(" << x << ")\n";
SensorCall(6);}

int main()
{
	SensorCall(7);std::string s1 = "Test";
	//  std::string&& r1 = s1;           // error: can't bind to lvalue

	const std::string& r2 = s1 + s1; // okay: lvalue reference to const extends lifetime
										//  r2 += "Test";                    // error: can't modify through reference to const

	std::string&& r3 = s1 + s1;      // okay: rvalue reference extends lifetime
	r3 += "Test";                    // okay: can modify through reference to non-const
	std::cout << r3 << '\n';
	

	int i = 1;
	const int ci = 2;
	f(i);  // calls f(int&)
	f(ci); // calls f(const int&)
	f(3);  // calls f(int&&)
		   // would call f(const int&) if f(int&&) overload wasn't provided
	//f(std::move(i)); // calls f(int&&)

					 // rvalue reference variables are lvalues when used in expressions
	int&& x = 1;
	f(x);            // calls f(int& x)
	//f(std::move(x)); // calls f(int&& x)
SensorCall(8);}
