#include <iostream>
#include "var/tmp/sensor.h"
#include <string>
#include <utility>
 
void fa(int i)
{
    SensorCall(1);if (i == 2)
         {/*1*/SensorCall(2);return;/*2*/}
    SensorCall(3);std::cout << i << '\n';
SensorCall(4);} // implied return;
 
int fb(int i)
{
    SensorCall(5);if (i > 4)
         {/*3*/{int  ReplaceReturn1 = 4; SensorCall(6); return ReplaceReturn1;}/*4*/}
    SensorCall(7);std::cout << i << '\n';
    {int  ReplaceReturn0 = 2; SensorCall(8); return ReplaceReturn0;}
}
 
//std::pair<std::string, int> fc(const char* p, int x)
//{
//    return {p, x};
//}
 
void fd()
{
    SensorCall(9);return fa(10); // fa(10) is a void expression
}
 
int main()
{
    SensorCall(10);fa(2); // returns, does nothing when i==2
    fa(1); // prints its argument, then returns
    int i = fb(5); // returns 4
    i = fb(i); // prints its argument, returns 2
    //std::cout << i << '\n'
    //          << fc("Hello", 7).second << '\n';
    fd();
SensorCall(11);}
