// GotoStatement.cpp  
#include <stdio.h>  
#include "var/tmp/sensor.h"
int main()  
{  
    SensorCall(1);int i, j;  
  
    SensorCall(7);for ( i = 0; i < 10; i++ )  
    {  
        SensorCall(2);printf( "Outer loop executing. i = %d\n", i );  
        SensorCall(6);for ( j = 0; j < 2; j++ )  
        {  
            SensorCall(3);printf( " Inner loop executing. j = %d\n", j );  
            SensorCall(5);if ( i == 3 )  
                {/*1*/SensorCall(4);goto stop;/*2*/}  
        }  
    }  
  
    // This message does not print:   
    SensorCall(8);printf( "Loop exited. i = %d\n", i );  
  
    stop:   
    printf( "Jumped to stop. i = %d\n", i );  
SensorCall(9);}  