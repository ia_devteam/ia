#include <iostream>
#include "var/tmp/sensor.h"
using namespace std;

int main() {
    // while loop with a single statement
    SensorCall(1);int i = 0;
    SensorCall(3);while (i < 10)
         {/*1*/SensorCall(2);i++;/*2*/}
    SensorCall(4);std::cout << i << '\n';
 
    // while loop with a compound statement
    int j = 2;
    SensorCall(6);while (j < 9) {
        SensorCall(5);std::cout << j << ' ';
        j += 2;
    }
    SensorCall(7);std::cout << '\n';
 
   // while loop with a declaration condition
   char cstr[] = "Hello";
   int k = 0;
   SensorCall(9);while (char c = cstr[k++])
       {/*3*/SensorCall(8);std::cout << c;/*4*/}
   SensorCall(10);std::cout << '\n';
SensorCall(11);}
