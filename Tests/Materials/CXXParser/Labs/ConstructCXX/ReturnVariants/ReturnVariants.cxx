#include "var/tmp/sensor.h"
// ReturnVariants.cxx - различные виды return; для отладки ReplaceReturn#

class OGRLineString
{
	private : OGRLineString * const pc_OGRLineString = this;  //- const pointer to OGRLineString;

	public : OGRLineString *getPointIterator() const;
			 const char *getGeometryName() const;
			 void veryVoidMethod() const;
			 static void failsToSupply() 
			 {
				 SensorCall();return;
			 }
};

const char * OGRLineString::getGeometryName() const
{
	{const char * ReplaceReturn = "LINESTRING"; SensorCall(); return ReplaceReturn;} 
}

OGRLineString *OGRLineString::getPointIterator() const
{	
	{OGRLineString * ReplaceReturn = pc_OGRLineString; SensorCall(); return ReplaceReturn;}
}

void OGRLineString::veryVoidMethod() const
{
	SensorCall();return;
}

int main(int argc, char* argv[]) 
{
	SensorCall();OGRLineString OGRLine;
	OGRLineString * const pOGRLine = OGRLine.getPointIterator();
	OGRLine.getGeometryName();

	OGRLine.veryVoidMethod();
	//
	{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}