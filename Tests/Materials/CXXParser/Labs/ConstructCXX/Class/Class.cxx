#include <iostream>  
#include "var/tmp/sensor.h"
#include <string>  
#define TRUE = 1  
using namespace std;

class dog
{
public:
	dog()
	{
		SensorCall(1);_legs = 4;
		_bark = true;
	SensorCall(2);}

	void setDogSize(string dogSize)
	{
		SensorCall(3);_dogSize = dogSize;
	SensorCall(4);}
	virtual void setEars(string type)      // virtual function  
	{
		SensorCall(5);_earType = type;
	SensorCall(6);}

private:
	string _dogSize, _earType;
	int _legs;
	bool _bark;

};

class breed : public dog
{
public:
	breed(string color, string size)
	{
		SensorCall(7);_color = color;
		setDogSize(size);
	SensorCall(8);}

	string getColor();

	// virtual function redefined  
	void setEars(string length, string type)
	{
		SensorCall(9);_earLength = length;
		_earType = type;
	SensorCall(10);}

protected:
	string _color, _earLength, _earType;
};

string breed::getColor()
{
	{string  ReplaceReturn0 = _color; SensorCall(11); return ReplaceReturn0;}
}


int main()
{
	SensorCall(12);dog mongrel;
	breed labrador("yellow", "large");
	mongrel.setEars("pointy");
	labrador.setEars("long", "floppy");
	cout << "Cody is a " << labrador.getColor() << " labrador" << endl;
SensorCall(13);}
