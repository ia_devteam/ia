#include "stdio.h"
#include "var/tmp/sensor.h"

namespace D {
	int d1;
	void f(char a_char)
	{
		SensorCall(1);printf("%c\n", a_char);
	SensorCall(2);}
}
using namespace D; // introduces D::d1, D::f, D::d2, D::f,
				   //  E::e, and E::f into global namespace!

int d1; // OK: no conflict with D::d1 when declaring
namespace E {
	int e;
	void f(int a_int)
	{
		SensorCall(3);printf("%d\n", a_int);
	SensorCall(4);}
}
namespace D { // namespace extension
	int d2;
	using namespace E; // transitive using-directive
	void f(int a_int)
	{
		SensorCall(5);printf("%d\n", a_int);
	SensorCall(6);}
}

int main() {
	//d1++; // error: ambiguous ::d1 or D::d1?
	SensorCall(7);::d1++; // OK
	D::d1++; // OK
	d2++; // OK, d2 is D::d2
	e++; // OK: e is E::e due to transitive using
	E::f(1); // error: ambiguous: D::f(int) or E::f(int)?
	f('a'); // OK: the only f(char) is D::f(char)
	{int  ReplaceReturn0 = 0; SensorCall(8); return ReplaceReturn0;}
}
