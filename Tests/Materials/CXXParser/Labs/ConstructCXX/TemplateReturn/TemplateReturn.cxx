#include "var/tmp/sensor.h"
// TemplateReturn.cpp

template <typename T>
T minimum(const T& lhs, const T& rhs)
{
	SensorCall(1);return lhs < rhs ? lhs : rhs;
}

int main()
{
	SensorCall(2);int i_min = -1;
	int i_max = 10;
	int result = minimum(i_min, i_max);
SensorCall(3);}