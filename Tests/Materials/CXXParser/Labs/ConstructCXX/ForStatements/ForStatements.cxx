#include <stdio.h>  
#include "var/tmp/sensor.h"
#include <string>
#include <iostream>
#include <vector>
using namespace std;

int main() {
	
	// classic C-style for statement
	SensorCall(1);char* line = "H e  \tl\tlo World\0";
	int space = 0;
	int tab = 0;
	int i;
	int max = string(line).length();
	SensorCall(6);for (i = 0; i < max; i++)
	{
		SensorCall(2);if (line[i] == ' ')
		{
			SensorCall(3);space++;
		}
		SensorCall(5);if (line[i] == '\t')
		{
			SensorCall(4);tab++;
		}
	}

	SensorCall(7);cout << "For statement #1" << endl;
	cout << "Number of spaces: " << space << endl;
	cout << "Number of tabs: " << tab << endl;
	
	space = 0;
	tab = 0;
	SensorCall(12);for (int iT = 0; iT < max; iT++)
	{
		SensorCall(8);if (line[iT] == ' ')
		{
			SensorCall(9);space++;
		}
		SensorCall(11);if (line[iT] == '\t')
		{
			SensorCall(10);tab++;
		}
	}

	SensorCall(13);cout << "For statement #2" << endl;
	cout << "Number of spaces: " << space << endl;
	cout << "Number of tabs: " << tab << endl;

	// 'for each' C++ statement 
	//string MyString = string("abcd");
	//for each (char c in MyString)
	//	cout << c;
	//cout << endl;

	//range-based 'for' statement
	// Basic 10-element integer array.  
	int x[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

	// Range-based for loop to iterate through the array.  
	SensorCall(15);for (int y : x) { // Access by value using a copy declared as a specific type.   
					  // Not preferred.  
		SensorCall(14);cout << y << " ";
	}
	SensorCall(16);cout << endl;

	// The auto keyword causes type inference to be used. Preferred.  

	SensorCall(18);for (auto y : x) { // Copy of 'x', almost always undesirable  
		SensorCall(17);cout << y << " ";
	}
	SensorCall(19);cout << endl;

	SensorCall(21);for (auto &y : x) { // Type inference by reference.  
						// Observes and/or modifies in-place. Preferred when modify is needed.  
		SensorCall(20);cout << y << " ";
	}
	SensorCall(22);cout << endl;

	SensorCall(24);for (const auto &y : x) { // Type inference by reference.  
							  // Observes in-place. Preferred when no modify is needed.  
		SensorCall(23);cout << y << " ";
	}
	SensorCall(25);cout << endl;
	cout << "end of integer array test" << endl;
	cout << endl;

	// Create a vector object that contains 10 elements.  
	vector<double> v;
	SensorCall(27);for (int i = 0; i < 10; ++i) {
		SensorCall(26);v.push_back(i + 0.14159);
	}

	// Range-based for loop to iterate through the vector, observing in-place.  
	SensorCall(29);for (const auto &j : v) {
		SensorCall(28);cout << j << " ";
	}
	SensorCall(30);cout << endl;
	cout << "end of vector test" << endl;

	{int  ReplaceReturn0 = 0; SensorCall(31); return ReplaceReturn0;}
}
