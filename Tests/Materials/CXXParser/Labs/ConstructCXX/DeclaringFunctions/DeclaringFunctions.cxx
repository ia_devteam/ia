#include <iostream>
#include "var/tmp/sensor.h"
#include <string>
#include <stdlib.h>
 
// declaration in namespace(file) scope
// (the definition is provided later)
int f1();
 
// simple function with a default argument, returning nothing
void f0(const std::string& arg = "world")
{
    SensorCall(1);std::cout << "Hello, " << arg << '\n';
SensorCall(2);}
 
// function returning a pointer to f0
//auto fp11() -> void(*)(const std::string&)
//{
//    return f0;
//}
 
// function returning a pointer to f0, pre-C++11 style
void (*fp03())(const std::string&)
{
    SensorCall(3);return f0;
}

// factorial calculator
long factorial (long a)
{
  SensorCall(4);if (a > 1)
   {/*1*/{long  ReplaceReturn4 = (a * factorial (a-1)); SensorCall(5); return ReplaceReturn4;}/*2*/}
  else
   {/*3*/{long  ReplaceReturn3 = 1; SensorCall(6); return ReplaceReturn3;}/*4*/}
SensorCall(7);}

int main()
{
    SensorCall(8);f0();
    //fp11()("test");
    fp03()("again");
    int f2(std::string); // declaration in function scope
    std::cout << f2("bad12") << '\n';
	long number = 9;
  	std::cout << number << "! = " << factorial (number) << std::endl;
	{int  ReplaceReturn2 = 0; SensorCall(9); return ReplaceReturn2;}
}
 
// simple non-member function returning int
int f1()
{
    {int  ReplaceReturn1 = 42; SensorCall(10); return ReplaceReturn1;}
}
 
// function with an exception specification and a function try block
int f2(std::string str)
{ 
    {int  ReplaceReturn0 = atoi(str.c_str()); SensorCall(11); return ReplaceReturn0;}
}
