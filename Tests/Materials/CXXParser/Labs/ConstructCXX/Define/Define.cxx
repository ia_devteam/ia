#include "var/tmp/sensor.h"
// Define.cxx

// Macro to define cursor lines   
#define CURSOR(top, bottom) (((top) << 8) | (bottom))
#define IS_ODD(value)(value % 2)

bool isALPHA(char ch);
bool isDIGIT(char ch);

#define USERFUN 287
#define VAR 293
#define SNARFWORD \
	d = tokenbuf; \
	while (isALPHA(*s) || isDIGIT(*s) || *s == '_') \
	    *d++ = *s++; \
	*d = '\0'; \
	d = tokenbuf; \
	if (*s == '(') \
	    idtype = USERFUN; \
	else \
	    idtype = VAR;

//
#define MAX_RES 5
long mCURSORS[MAX_RES];

int main()
{
	SensorCall(1);int c_top = 40;
	int c_bottom = 0;
	SensorCall(3);for (int iT = 0; iT < MAX_RES; iT++)
	{
		SensorCall(2);c_top = c_top * 2;
		c_bottom = int((c_top / 4) * 3);
		mCURSORS[iT] = CURSOR(c_top, c_bottom);
	}
	//
	SensorCall(4);int odd_val = IS_ODD(8);
	int noodd_val = IS_ODD(31);

	// one source code
	char perl_src[] = "#!/ usr / bin / perl\nprint \"Hello, world!\n\";";
	char *tokenbuf = &perl_src[0];
	char *s = &perl_src[0];
	char *d;
	int idtype = 0;
	SNARFWORD;

	// another source code
	SensorCall(5);char perl_src2[] = "use Mojolicious::Lite;\nget '/' = > {text = > 'Hello World!'}\n;app->start;";
	tokenbuf = &perl_src2[0];
	s = &perl_src2[0];
	idtype = 0;
	SNARFWORD;

	{int  ReplaceReturn5 = 0; SensorCall(6); return ReplaceReturn5;}
}

bool isALPHA(char ch)
{
	SensorCall(7);unsigned int uint_ch = (unsigned int)(ch);
	SensorCall(9);if ( uint_ch >= 65 && uint_ch <= 90 )
		{/*1*/{_Bool  ReplaceReturn4 = true; SensorCall(8); return ReplaceReturn4;}/*2*/}
	SensorCall(11);if ( uint_ch >= 97 && uint_ch <= 122 )
		{/*3*/{_Bool  ReplaceReturn3 = true; SensorCall(10); return ReplaceReturn3;}/*4*/}
	{_Bool  ReplaceReturn2 = false; SensorCall(12); return ReplaceReturn2;}
}
bool isDIGIT(char ch)
{
	SensorCall(13);unsigned int uint_ch = (unsigned int)(ch);
	SensorCall(15);if (uint_ch >= 48 && uint_ch <= 57)
		{/*5*/{_Bool  ReplaceReturn1 = true; SensorCall(14); return ReplaceReturn1;}/*6*/} 
	{_Bool  ReplaceReturn0 = false; SensorCall(16); return ReplaceReturn0;}
}