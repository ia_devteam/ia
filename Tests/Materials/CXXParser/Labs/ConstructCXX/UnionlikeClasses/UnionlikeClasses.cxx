#include <iostream>
#include "var/tmp/sensor.h"
 
// S has one non-static data member (tag), three enumerator members (CHAR, INT, DOUBLE), 
// and three variant members (c, i, d)
struct S
{
    enum{CHAR, INT, DOUBLE} tag;
    union
    {
        char c;
        int i;
        double d;
    };
};
 
void print_s(const S& s)
{
    SensorCall(1);switch(s.tag)
    {
        case S::CHAR: SensorCall(2);std::cout << s.c << '\n'; SensorCall(3);break;
        case S::INT: SensorCall(4);std::cout << s.i << '\n'; SensorCall(5);break;
        case S::DOUBLE: SensorCall(6);std::cout << s.d << '\n'; SensorCall(7);break;
    }
SensorCall(8);}
 
int main()
{
    SensorCall(9);S s = {S::CHAR, 'a'};
    print_s(s);
    s.tag = S::INT;
    s.i = 123;
    print_s(s);
SensorCall(10);}