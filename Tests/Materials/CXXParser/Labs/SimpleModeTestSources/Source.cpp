#include "/opt/sensor.h"
// custom countdown using while
#include <iostream>
using namespace std;
int n;
int main()
{
	SensorCall(1);n = 10;

	SensorCall(3);while (n>0) {
		SensorCall(2);cout << n << ", ";
		--n;
	}

	SensorCall(4);cout << "liftoff!\n";SensorCall(5);
}

int whileloop() 
{
	SensorCall(6);n = 10;

	SensorCall(8);while (n>0) {
		SensorCall(7);cout << n << ", ";
		--n;
	}

	SensorCall(9);cout << "liftoff!\n";
	int  ReplaceReturn6 = 0; SensorCall(10); return ReplaceReturn6;
}

void ifcases()
{
	SensorCall(11);int x = 5;
	SensorCall(13);if (x == 100)
		{/*1*/SensorCall(12);cout << "x is 100";/*2*/}
	SensorCall(15);if (x == 100)
	{
		SensorCall(14);cout << "x is ";
		cout << x;
	}
	SensorCall(17);if (x == 100) { SensorCall(16);cout << "x is "; cout << x; }
	SensorCall(20);if (x == 100)
		{/*3*/SensorCall(18);cout << "x is 100";/*4*/}
	else
		{/*5*/SensorCall(19);cout << "x is not 100";/*6*/}
	SensorCall(25);if (x > 0)
		{/*7*/SensorCall(21);cout << "x is positive";/*8*/}
	else {/*9*/SensorCall(22);if (x < 0)
		{/*11*/SensorCall(23);cout << "x is negative";/*12*/}
	else
		{/*13*/SensorCall(24);cout << "x is 0";/*14*/}/*10*/}SensorCall(26);
}

void dowhile()
{
	SensorCall(27);string str;
	SensorCall(29);do {
		SensorCall(28);cout << "Enter text: ";
		//getline(cin, str);
		//cout << str << '\n';
	} while (strcmp(str.c_str(),"goodbye"));SensorCall(30);
}

void forloop()
{
	SensorCall(31);for (int n = 10; n>0; n--) {
		SensorCall(32);cout << n << ", ";
	}
	SensorCall(33);cout << "liftoff!\n";
	SensorCall(35);for (int n = 10; n>0; n--) {/*15*/SensorCall(34);cout << n << ", ";/*16*/} 
	SensorCall(36);string str{ "Hello!" };
	SensorCall(38);for (char c : str)
	{
		SensorCall(37);cout << "[" << c << "]";
	}
	SensorCall(39);cout << '\n';
	SensorCall(41);for (auto c : str)
		{/*17*/SensorCall(40);cout << "[" << c << "]";/*18*/}SensorCall(42);
}

void breakst()
{
	SensorCall(43);for (int n = 10; n>0; n--)
	{
		SensorCall(44);cout << n << ", ";
		SensorCall(47);if (n == 3)
		{
			SensorCall(45);cout << "countdown aborted!";
			SensorCall(46);break;
		}
	}SensorCall(48);
}

void continuest()
{
	SensorCall(49);for (n = 10; n>0; n--) {
		SensorCall(50);if (n == 5) {/*19*/SensorCall(51);continue;/*20*/}
		SensorCall(52);cout << n << ", ";
	}
	SensorCall(53);cout << "liftoff!\n";SensorCall(54);
}

void gotost()
{
	SensorCall(55);int n = 10;
mylabel:
	cout << n << ", ";
	n--;
	SensorCall(57);if (n>0) {/*21*/SensorCall(56);goto mylabel;/*22*/}
	SensorCall(58);cout << "liftoff!\n";SensorCall(59);
}

int switchst()
{
	SensorCall(60);int x = 1;
	SensorCall(66);switch (x) {
	case 0:
	case 1:
		SensorCall(61);cout << "x is 1";
		SensorCall(62);break;
	case 2:
	{
			  SensorCall(63);cout << "x is 2";
			  SensorCall(64);break;
	}
	default:
		SensorCall(65);cout << "value of x unknown";
	}

	int  ReplaceReturn5 = main(); SensorCall(67); return ReplaceReturn5;
}

template <class SomeType>
SomeType sum(SomeType a, SomeType b)
{
	SomeType  ReplaceReturn4 = a + b; SensorCall(68); return ReplaceReturn4;
}

int maintest() {
	SensorCall(69);int i = 5, j = 6, k;
	double f = 2.0, g = 0.5, h;
	k = sum<int>(i, j);
	h = sum<double>(f, g);
	cout << k << '\n';
	cout << h << '\n';
	int  ReplaceReturn3 = 0; SensorCall(70); return ReplaceReturn3;
}

template <class T, int N>
T fixed_multiply(T val)
{
	T  ReplaceReturn2 = val * N; SensorCall(71); return ReplaceReturn2;
}

int mainnontype() {
	SensorCall(72);std::cout << fixed_multiply<int, 2>(10) << '\n';
	std::cout << fixed_multiply<int, 3>(10) << '\n';
	int  ReplaceReturn1 = - 100; SensorCall(73); return ReplaceReturn1;
}

class Rectangle {
	int width, height;
public:
	void set_values(int, int);
	int area() { int  ReplaceReturn0 = width*height; SensorCall(74); return ReplaceReturn0; }
	Rectangle::Rectangle(int a, int b) {
		SensorCall(75);width = a;
		height = b;SensorCall(76);
	}
};

class ChildRect : public Rectangle {
protected:
	int a;
private:
	int b;
public:
	int c;
	ChildRect():Rectangle(1,1){
		SensorCall(77);b = 2;SensorCall(78);
	}
};

void Rectangle::set_values(int x, int y) {
	SensorCall(79);width = x;
	height = y;SensorCall(80);
}

void tryCatch()
{
	SensorCall(81);try
	{
		SensorCall(82);throw 20;
	}
	catch (int e)
	{
		SensorCall(83);cout << "An exception occurred. Exception Nr. " << e << '\n';
		SensorCall(84);throw;
	}
	SensorCall(85);ChildRect test;
	test.area();SensorCall(86);
}


#define OBRACE {
#define CBRACE }

void test() OBRACE
SensorCall(87);SensorCall(88);int a;
CBRACE
