/* $Id: iptcutil.c,v 1.8 2011-05-08 00:44:18 fwarmerdam Exp $ */

#include "tif_config.h"
#include "/var/tmp/sensor.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#ifdef HAVE_STRINGS_H
# include <strings.h>
#endif

#ifdef HAVE_IO_H
# include <io.h>
#endif

#ifdef HAVE_FCNTL_H
# include <fcntl.h>
#endif

#ifdef WIN32
#define STRNICMP strnicmp
#else 
#define STRNICMP strncasecmp
#endif 

typedef struct _tag_spec
{
  short
    id;

  char
    *name;
} tag_spec;

static tag_spec tags[] = {
    { 5,"Image Name" },
    { 7,"Edit Status" },
    { 10,"Priority" },
    { 15,"Category" },
    { 20,"Supplemental Category" },
    { 22,"Fixture Identifier" },
    { 25,"Keyword" },
    { 30,"Release Date" },
    { 35,"Release Time" },
    { 40,"Special Instructions" },
    { 45,"Reference Service" },
    { 47,"Reference Date" },
    { 50,"Reference Number" },
    { 55,"Created Date" },
    { 60,"Created Time" },
    { 65,"Originating Program" },
    { 70,"Program Version" },
    { 75,"Object Cycle" },
    { 80,"Byline" },
    { 85,"Byline Title" },
    { 90,"City" },
    { 95,"Province State" },
    { 100,"Country Code" },
    { 101,"Country" },
    { 103,"Original Transmission Reference" },
    { 105,"Headline" },
    { 110,"Credit" },
    { 115,"Source" },
    { 116,"Copyright String" },
    { 120,"Caption" },
    { 121,"Local Caption" },
    { 122,"Caption Writer" },
    { 200,"Custom Field 1" },
    { 201,"Custom Field 2" },
    { 202,"Custom Field 3" },
    { 203,"Custom Field 4" },
    { 204,"Custom Field 5" },
    { 205,"Custom Field 6" },
    { 206,"Custom Field 7" },
    { 207,"Custom Field 8" },
    { 208,"Custom Field 9" },
    { 209,"Custom Field 10" },
    { 210,"Custom Field 11" },
    { 211,"Custom Field 12" },
    { 212,"Custom Field 13" },
    { 213,"Custom Field 14" },
    { 214,"Custom Field 15" },
    { 215,"Custom Field 16" },
    { 216,"Custom Field 17" },
    { 217,"Custom Field 18" },
    { 218,"Custom Field 19" },
    { 219,"Custom Field 20" }
};

/*
 * We format the output using HTML conventions
 * to preserve control characters and such.
 */
void formatString(FILE *ofile, const char *s, int len)
{
SensorCall();  putc('"', ofile);
  SensorCall();for (; len > 0; --len, ++s) {
    SensorCall();int c = *s;
    SensorCall();switch (c) {
    case '&':
      SensorCall();fputs("&amp;", ofile);
      SensorCall();break;
#ifdef HANDLE_GT_LT
    case '<':
      fputs("&lt;", ofile);
      break;
    case '>':
      fputs("&gt;", ofile);
      break;
#endif
    case '"':
      SensorCall();fputs("&quot;", ofile);
      SensorCall();break;
    default:
      SensorCall();if (iscntrl(c))
        {/*1*/SensorCall();fprintf(ofile, "&#%d;", c);/*2*/}
      else
        putc(*s, ofile);
      SensorCall();break;
    }
  }
  SensorCall();fputs("\"\n", ofile);
SensorCall();}

typedef struct _html_code
{
  short
    len;
  const char
    *code,
    val;
} html_code;

static html_code html_codes[] = {
#ifdef HANDLE_GT_LT
    { 4,"&lt;",'<' },
    { 4,"&gt;",'>' },
#endif
    { 5,"&amp;",'&' },
    { 6,"&quot;",'"' }
};

/*
 * This routine converts HTML escape sequence
 * back to the original ASCII representation.
 * - returns the number of characters dropped.
 */
int convertHTMLcodes(char *s, int len)
{
  SensorCall();if (len <=0 || s==(char*)NULL || *s=='\0')
    {/*3*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*4*/}

  SensorCall();if (s[1] == '#')
    {
      SensorCall();int val, o;

      SensorCall();if (sscanf(s,"&#%d;",&val) == 1)
      {
        SensorCall();o = 3;
        SensorCall();while (s[o] != ';')
        {
          SensorCall();o++;
          SensorCall();if (o > 5)
            {/*5*/SensorCall();break;/*6*/}
        }
        SensorCall();if (o < 5)
          {/*7*/SensorCall();strcpy(s+1, s+1+o);/*8*/}
        SensorCall();*s = val;
        {int  ReplaceReturn = o; SensorCall(); return ReplaceReturn;}
      }
    }
  else
    {
      SensorCall();int
        i,
        codes = sizeof(html_codes) / sizeof(html_code);

      SensorCall();for (i=0; i < codes; i++)
      {
        SensorCall();if (html_codes[i].len <= len)
          {/*9*/SensorCall();if (STRNICMP(s, html_codes[i].code, html_codes[i].len) == 0)
            {
              SensorCall();strcpy(s+1, s+html_codes[i].len);
              *s = html_codes[i].val;
              {int  ReplaceReturn = html_codes[i].len-1; SensorCall(); return ReplaceReturn;}
            ;/*10*/}}
      }
    }

  {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

int formatIPTC(FILE *ifile, FILE *ofile)
{
  SensorCall();unsigned int
    foundiptc,
    tagsfound;

  unsigned char
    recnum,
    dataset;

  char
    *readable,
    *str;

  long
    tagindx,
    taglen;

  int
    i,
    tagcount = sizeof(tags) / sizeof(tag_spec);

  char
    c;

  foundiptc = 0; /* found the IPTC-Header */
  tagsfound = 0; /* number of tags found */

  c = getc(ifile);
  SensorCall();while (c != EOF)
  {
	  SensorCall();if (c == 0x1c)
	    {/*11*/SensorCall();foundiptc = 1;/*12*/}
	  else
      {
        SensorCall();if (foundiptc)
	        {/*13*/{int  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}/*14*/}
        else
	        {/*15*/SensorCall();continue;/*16*/}
	    }

    /* we found the 0x1c tag and now grab the dataset and record number tags */
    SensorCall();dataset = getc(ifile);
	  SensorCall();if ((char) dataset == EOF)
	    {/*17*/{int  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}/*18*/}
    SensorCall();recnum = getc(ifile);
	  SensorCall();if ((char) recnum == EOF)
	    {/*19*/{int  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}/*20*/}
    /* try to match this record to one of the ones in our named table */
    SensorCall();for (i=0; i< tagcount; i++)
    {
      SensorCall();if (tags[i].id == recnum)
          {/*21*/SensorCall();break;/*22*/}
    }
    SensorCall();if (i < tagcount)
      {/*23*/SensorCall();readable = tags[i].name;/*24*/}
    else
      {/*25*/SensorCall();readable = "";/*26*/}

    /* then we decode the length of the block that follows - long or short fmt */
    SensorCall();c = getc(ifile);
	  SensorCall();if (c == EOF)
	    {/*27*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*28*/}
	  SensorCall();if (c & (unsigned char) 0x80)
      {
        SensorCall();unsigned char
          buffer[4];

        SensorCall();for (i=0; i<4; i++)
        {
          SensorCall();c = buffer[i] = getc(ifile);
          SensorCall();if (c == EOF)
            {/*29*/{int  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}/*30*/}
        }
        SensorCall();taglen = (((long) buffer[ 0 ]) << 24) |
                 (((long) buffer[ 1 ]) << 16) | 
	               (((long) buffer[ 2 ]) <<  8) |
                 (((long) buffer[ 3 ]));
	    }
    else
      {
        SensorCall();unsigned char
          x = c;

        taglen = ((long) x) << 8;
        x = getc(ifile);
        SensorCall();if ((char)x == EOF)
          {/*31*/{int  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}/*32*/}
        SensorCall();taglen |= (long) x;
	    }
    /* make a buffer to hold the tag data and snag it from the input stream */
    SensorCall();str = (char *) malloc((unsigned int) (taglen+1));
    SensorCall();if (str == (char *) NULL)
      {
        SensorCall();printf("Memory allocation failed");
        {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
      }
    SensorCall();for (tagindx=0; tagindx<taglen; tagindx++)
    {
      SensorCall();c = str[tagindx] = getc(ifile);
      SensorCall();if (c == EOF)
      {
          SensorCall();free(str);
          {int  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}
      }
    }
    SensorCall();str[ taglen ] = 0;

    /* now finish up by formatting this binary data into ASCII equivalent */
    SensorCall();if (strlen(readable) > 0)
	    {/*33*/SensorCall();fprintf(ofile, "%d#%d#%s=",(unsigned int)dataset, (unsigned int) recnum, readable);/*34*/}
    else
	    {/*35*/SensorCall();fprintf(ofile, "%d#%d=",(unsigned int)dataset, (unsigned int) recnum);/*36*/}
    SensorCall();formatString( ofile, str, taglen );
    free(str);

	  tagsfound++;

    c = getc(ifile);
  }
  {int  ReplaceReturn = tagsfound; SensorCall(); return ReplaceReturn;}
}

int tokenizer(unsigned inflag,char *token,int tokmax,char *line,
char *white,char *brkchar,char *quote,char eschar,char *brkused,
int *next,char *quoted);

char *super_fgets(char *b, int *blen, FILE *file)
{
  SensorCall();int
    c,
    len;

  char
    *q;

  len=*blen;
  SensorCall();for (q=b; ; q++)
  {
    SensorCall();c=fgetc(file);
    SensorCall();if (c == EOF || c == '\n')
      {/*47*/SensorCall();break;/*48*/}
    SensorCall();if (((long)q - (long)b + 1 ) >= (long) len)
      {
        SensorCall();long
          tlen;

        tlen=(long)q-(long)b;
        len<<=1;
        b=(char *) realloc((char *) b,(len+2));
        SensorCall();if ((char *) b == (char *) NULL)
          {/*49*/SensorCall();break;/*50*/}
        SensorCall();q=b+tlen;
      }
    SensorCall();*q=(unsigned char) c;
  }
  SensorCall();*blen=0;
  SensorCall();if ((unsigned char *)b != (unsigned char *) NULL)
    {
      SensorCall();int
        tlen;

      tlen=(long)q - (long)b;
      SensorCall();if (tlen == 0)
        return (char *) NULL;
      SensorCall();b[tlen] = '\0';
      *blen=++tlen;
    }
  {char * ReplaceReturn = b; SensorCall(); return ReplaceReturn;}
}

#define BUFFER_SZ 4096

int main(int argc, char *argv[])
{            
  SensorCall();unsigned int
    length;

  unsigned char
    *buffer;

  int
    i,
    mode; /* iptc binary, or iptc text */

  FILE
    *ifile = stdin,
    *ofile = stdout;

  char
    c,
    *usage = "usage: iptcutil -t | -b [-i file] [-o file] <input >output";

  SensorCall();if( argc < 2 )
    {
      SensorCall();puts(usage);
	    {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
    }

  SensorCall();mode = 0;
  length = -1;
  buffer = (unsigned char *)NULL;

  SensorCall();for (i=1; i<argc; i++)
  {
    SensorCall();c = argv[i][0];
    SensorCall();if (c == '-' || c == '/')
      {
        SensorCall();c = argv[i][1];
        SensorCall();switch( c )
        {
        case 't':
	        SensorCall();mode = 1;
#ifdef WIN32
          /* Set "stdout" to binary mode: */
          _setmode( _fileno( ofile ), _O_BINARY );
#endif
	        SensorCall();break;
        case 'b':
	        SensorCall();mode = 0;
#ifdef WIN32
          /* Set "stdin" to binary mode: */
          _setmode( _fileno( ifile ), _O_BINARY );
#endif
	        SensorCall();break;
        case 'i':
          SensorCall();if (mode == 0)
            {/*51*/SensorCall();ifile = fopen(argv[++i], "rb");/*52*/}
          else
            {/*53*/SensorCall();ifile = fopen(argv[++i], "rt");/*54*/}
          SensorCall();if (ifile == (FILE *)NULL)
            {
	            SensorCall();printf("Unable to open: %s\n", argv[i]);
              {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
            }
	        SensorCall();break;
        case 'o':
          SensorCall();if (mode == 0)
            {/*55*/SensorCall();ofile = fopen(argv[++i], "wt");/*56*/}
          else
            {/*57*/SensorCall();ofile = fopen(argv[++i], "wb");/*58*/}
          SensorCall();if (ofile == (FILE *)NULL)
            {
	            SensorCall();printf("Unable to open: %s\n", argv[i]);
              {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
            }
	        SensorCall();break;
        default:
	        SensorCall();printf("Unknown option: %s\n", argv[i]);
	        {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
        }
      }
    else
      {
        SensorCall();puts(usage);
	      {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
      }
  }

  SensorCall();if (mode == 0) /* handle binary iptc info */
    {/*59*/SensorCall();formatIPTC(ifile, ofile);/*60*/}

  SensorCall();if (mode == 1) /* handle text form of iptc info */
    {
      SensorCall();char
        brkused,
        quoted,
        *line,
        *token,
        *newstr;

      int
        state,
        next;

      unsigned char
        recnum = 0,
        dataset = 0;

      int
        inputlen = BUFFER_SZ;

      line = (char *) malloc(inputlen);     
      token = (char *)NULL;
      SensorCall();while((line = super_fgets(line,&inputlen,ifile))!=NULL)
      {
        SensorCall();state=0;
        next=0;

        token = (char *) malloc(inputlen);     
        newstr = (char *) malloc(inputlen);     
        SensorCall();while(tokenizer(0, token, inputlen, line, "", "=", "\"", 0,
          &brkused,&next,&quoted)==0)
        {
          SensorCall();if (state == 0)
            {                  
              SensorCall();int
                state,
                next;

              char
                brkused,
                quoted;

              state=0;
              next=0;
              SensorCall();while(tokenizer(0, newstr, inputlen, token, "", "#", "", 0,
                &brkused, &next, &quoted)==0)
              {
                SensorCall();if (state == 0)
                  {/*61*/SensorCall();dataset = (unsigned char) atoi(newstr);/*62*/}
                else
                   {/*63*/SensorCall();if (state == 1)
                     {/*65*/SensorCall();recnum = (unsigned char) atoi(newstr);/*66*/}/*64*/}
                SensorCall();state++;
              }
            }
          else
            {/*67*/SensorCall();if (state == 1)
              {
                SensorCall();int
                  next;

                unsigned long
                  len;

                char
                  brkused,
                  quoted;

                next=0;
                len = strlen(token);
                SensorCall();while(tokenizer(0, newstr, inputlen, token, "", "&", "", 0,
                  &brkused, &next, &quoted)==0)
                {
                  SensorCall();if (brkused && next > 0)
                    {
                      SensorCall();char
                        *s = &token[next-1];

                      len -= convertHTMLcodes(s, strlen(s));
                    }
                }

                SensorCall();fputc(0x1c, ofile);
                fputc(dataset, ofile);
                fputc(recnum, ofile);
                SensorCall();if (len < 0x10000)
                  {
                    SensorCall();fputc((len >> 8) & 255, ofile);
                    fputc(len & 255, ofile);
                  }
                else
                  {
                    SensorCall();fputc(((len >> 24) & 255) | 0x80, ofile);
                    fputc((len >> 16) & 255, ofile);
                    fputc((len >> 8) & 255, ofile);
                    fputc(len & 255, ofile);
                  }
                SensorCall();next=0;
                SensorCall();while (len--)
                  {/*69*/SensorCall();fputc(token[next++], ofile);/*70*/}
              ;/*68*/}}
          SensorCall();state++;
        }
        SensorCall();free(token);
        token = (char *)NULL;
        free(newstr);
        newstr = (char *)NULL;
      }
      SensorCall();free(line);

      fclose( ifile );
      fclose( ofile );
    }

  {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

/*
	This routine is a generalized, finite state token parser. It allows
    you extract tokens one at a time from a string of characters.  The
    characters used for white space, for break characters, and for quotes
    can be specified. Also, characters in the string can be preceded by
    a specifiable escape character which removes any special meaning the
    character may have.

	There are a lot of formal parameters in this subroutine call, but
	once you get familiar with them, this routine is fairly easy to use.
	"#define" macros can be used to generate simpler looking calls for
	commonly used applications of this routine.

	First, some terminology:

	token:		used here, a single unit of information in
				the form of a group of characters.

	white space:	space that gets ignored (except within quotes
				or when escaped), like blanks and tabs.  in
				addition, white space terminates a non-quoted
				token.

	break character: a character that separates non-quoted tokens.
				commas are a common break character.  the
				usage of break characters to signal the end
				of a token is the same as that of white space,
				except multiple break characters with nothing
				or only white space between generate a null
				token for each two break characters together.

				for example, if blank is set to be the white
				space and comma is set to be the break
				character, the line ...

				A, B, C ,  , DEF

				... consists of 5 tokens:

				1)	"A"
				2)	"B"
				3)	"C"
				4)	""      (the null string)
				5)	"DEF"

	quote character: 	a character that, when surrounding a group
				of other characters, causes the group of
				characters to be treated as a single token,
				no matter how many white spaces or break
				characters exist in the group.	also, a
				token always terminates after the closing
				quote.	for example, if ' is the quote
				character, blank is white space, and comma
				is the break character, the following
				string ...

				A, ' B, CD'EF GHI

				... consists of 4 tokens:

				1)	"A"
				2)	" B, CD" (note the blanks & comma)
				3)	"EF"
				4)	"GHI"

				the quote characters themselves do
				not appear in the resultant tokens.  the
				double quotes are delimiters i use here for
				documentation purposes only.

	escape character:	a character which itself is ignored but
				which causes the next character to be
				used as is.  ^ and \ are often used as
				escape characters.  an escape in the last
				position of the string gets treated as a
				"normal" (i.e., non-quote, non-white,
				non-break, and non-escape) character.
				for example, assume white space, break
				character, and quote are the same as in the
				above examples, and further, assume that
				^ is the escape character.  then, in the
				string ...

				ABC, ' DEF ^' GH' I ^ J K^ L ^

				... there are 7 tokens:

				1)	"ABC"
				2)	" DEF ' GH"
				3)	"I"
				4)	" "     (a lone blank)
				5)	"J"
				6)	"K L"
				7)	"^"     (passed as is at end of line)


	OK, now that you have this background, here's how to call "tokenizer":

	result=tokenizer(flag,token,maxtok,string,white,break,quote,escape,
		      brkused,next,quoted)

	result: 	0 if we haven't reached EOS (end of string), and
			1 if we have (this is an "int").

	flag:		right now, only the low order 3 bits are used.
			1 => convert non-quoted tokens to upper case
			2 => convert non-quoted tokens to lower case
			0 => do not convert non-quoted tokens
			(this is a "char").

	token:		a character string containing the returned next token
			(this is a "char[]").

	maxtok: 	the maximum size of "token".  characters beyond
			"maxtok" are truncated (this is an "int").

	string: 	the string to be parsed (this is a "char[]").

	white:		a string of the valid white spaces.  example:

			char whitesp[]={" \t"};

			blank and tab will be valid white space (this is
			a "char[]").

	break:		a string of the valid break characters.  example:

			char breakch[]={";,"};

			semicolon and comma will be valid break characters
			(this is a "char[]").

			IMPORTANT:  do not use the name "break" as a C
			variable, as this is a reserved word in C.

	quote:		a string of the valid quote characters.  an example
			would be

			char whitesp[]={"'\"");

			(this causes single and double quotes to be valid)
			note that a token starting with one of these characters
			needs the same quote character to terminate it.

			for example,

			"ABC '

			is unterminated, but

			"DEF" and 'GHI'

			are properly terminated.  note that different quote
			characters can appear on the same line; only for
			a given token do the quote characters have to be
			the same (this is a "char[]").

	escape: 	the escape character (NOT a string ... only one
			allowed).  use zero if none is desired (this is
			a "char").

	brkused:	the break character used to terminate the current
			token.	if the token was quoted, this will be the
			quote used.  if the token is the last one on the
			line, this will be zero (this is a pointer to a
			"char").

	next:		this variable points to the first character of the
			next token.  it gets reset by "tokenizer" as it steps
			through the string.  set it to 0 upon initialization,
			and leave it alone after that.	you can change it
			if you want to jump around in the string or re-parse
			from the beginning, but be careful (this is a
			pointer to an "int").

	quoted: 	set to 1 (true) if the token was quoted and 0 (false)
			if not.  you may need this information (for example:
			in C, a string with quotes around it is a character
			string, while one without is an identifier).

			(this is a pointer to a "char").
*/

/* states */

#define IN_WHITE 0
#define IN_TOKEN 1
#define IN_QUOTE 2
#define IN_OZONE 3

int _p_state;	   /* current state	 */
unsigned _p_flag;  /* option flag	 */
char _p_curquote;  /* current quote char */
int _p_tokpos;	   /* current token pos  */

/* routine to find character in string ... used only by "tokenizer" */

int sindex(char ch,char *string)
{
  SensorCall();char *cp;
  SensorCall();for(cp=string;*cp;++cp)
    {/*71*/SensorCall();if(ch==*cp)
      {/*73*/{int  ReplaceReturn = (int)(cp-string); SensorCall(); return ReplaceReturn;}/*74*/}/*72*/}	/* return postion of character */
  {int  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}			/* eol ... no match found */
}

/* routine to store a character in a string ... used only by "tokenizer" */

void chstore(char *string,int max,char ch)
{
  SensorCall();char c;
  SensorCall();if(_p_tokpos>=0&&_p_tokpos<max-1)
  {
    SensorCall();if(_p_state==IN_QUOTE)
      {/*75*/SensorCall();c=ch;/*76*/}
    else
      {/*77*/SensorCall();switch(_p_flag&3)
      {
	    case 1: 	    /* convert to upper */
	      SensorCall();c=toupper(ch);
	      SensorCall();break;

	    case 2: 	    /* convert to lower */
	      SensorCall();c=tolower(ch);
	      SensorCall();break;

	    default:	    /* use as is */
	      SensorCall();c=ch;
	      SensorCall();break;
      ;/*78*/}}
    SensorCall();string[_p_tokpos++]=c;
  }
  SensorCall();return;
}

int tokenizer(unsigned inflag,char *token,int tokmax,char *line,
  char *white,char *brkchar,char *quote,char eschar,char *brkused,
    int *next,char *quoted)
{
  SensorCall();int qp;
  char c,nc;

  *brkused=0;		/* initialize to null */
  *quoted=0;		/* assume not quoted  */

  SensorCall();if(!line[*next])	/* if we're at end of line, indicate such */
    {/*37*/{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}/*38*/}

  SensorCall();_p_state=IN_WHITE;   /* initialize state */
  _p_curquote=0;	   /* initialize previous quote char */
  _p_flag=inflag;	   /* set option flag */

  SensorCall();for(_p_tokpos=0;(c=line[*next]);++(*next))	/* main loop */
  {
    SensorCall();if((qp=sindex(c,brkchar))>=0)  /* break */
    {
      SensorCall();switch(_p_state)
      {
	    case IN_WHITE:		/* these are the same here ...	*/
	    case IN_TOKEN:		/* ... just get out		*/
	    case IN_OZONE:		/* ditto			*/
	      SensorCall();++(*next);
	      *brkused=brkchar[qp];
	      SensorCall();goto byebye;

	    case IN_QUOTE:		 /* just keep going */
	      SensorCall();chstore(token,tokmax,c);
	      SensorCall();break;
      }
    }
    else {/*39*/SensorCall();if((qp=sindex(c,quote))>=0)  /* quote */
    {
      SensorCall();switch(_p_state)
      {
	    case IN_WHITE:	 /* these are identical, */
	      SensorCall();_p_state=IN_QUOTE; /* change states   */
	      _p_curquote=quote[qp]; /* save quote char */
	      *quoted=1;	/* set to true as long as something is in quotes */
	      SensorCall();break;

	    case IN_QUOTE:
	      SensorCall();if(quote[qp]==_p_curquote) /* same as the beginning quote? */
	      {
	        SensorCall();_p_state=IN_OZONE;
	        _p_curquote=0;
	      }
	      else
	        {/*41*/SensorCall();chstore(token,tokmax,c);/*42*/} /* treat as regular char */
	      SensorCall();break;

	    case IN_TOKEN:
	    case IN_OZONE:
	      SensorCall();*brkused=c; /* uses quote as break char */
	      SensorCall();goto byebye;
      }
    }
    else {/*43*/SensorCall();if((qp=sindex(c,white))>=0) /* white */
    {
      SensorCall();switch(_p_state)
      {
	    case IN_WHITE:
	    case IN_OZONE:
	      SensorCall();break;		/* keep going */

	    case IN_TOKEN:
	      SensorCall();_p_state=IN_OZONE;
	      SensorCall();break;

	    case IN_QUOTE:
	      SensorCall();chstore(token,tokmax,c); /* it's valid here */
	      SensorCall();break;
      }
    }
    else {/*45*/SensorCall();if(c==eschar)  /* escape */
    {
      SensorCall();nc=line[(*next)+1];
      SensorCall();if(nc==0) 		/* end of line */
      {
	    SensorCall();*brkused=0;
	    chstore(token,tokmax,c);
	    ++(*next);
	    SensorCall();goto byebye;
      }
      SensorCall();switch(_p_state)
      {
	    case IN_WHITE:
	      SensorCall();--(*next);
	      _p_state=IN_TOKEN;
	      SensorCall();break;

	    case IN_TOKEN:
	    case IN_QUOTE:
	      SensorCall();++(*next);
	      chstore(token,tokmax,nc);
	      SensorCall();break;

	    case IN_OZONE:
	      SensorCall();goto byebye;
      }
    }
    else	/* anything else is just a real character */
    {
      SensorCall();switch(_p_state)
      {
	    case IN_WHITE:
	      SensorCall();_p_state=IN_TOKEN; /* switch states */

	    case IN_TOKEN:		 /* these 2 are     */
	    case IN_QUOTE:		 /*  identical here */
	      SensorCall();chstore(token,tokmax,c);
	      SensorCall();break;

	    case IN_OZONE:
	      SensorCall();goto byebye;
      }
    ;/*46*/}/*44*/}/*40*/}}
  }		/* end of main loop */

byebye:
  SensorCall();token[_p_tokpos]=0;	/* make sure token ends with EOS */

  {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
