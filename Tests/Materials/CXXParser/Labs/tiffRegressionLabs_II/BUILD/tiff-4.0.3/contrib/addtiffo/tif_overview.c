/******************************************************************************
 * tif_overview.c,v 1.9 2005/05/25 09:03:16 dron Exp
 *
 * Project:  TIFF Overview Builder
 * Purpose:  Library function for building overviews in a TIFF file.
 * Author:   Frank Warmerdam, warmerdam@pobox.com
 *
 * Notes:
 *  o Currently only images with bits_per_sample of a multiple of eight
 *    will work.
 *
 *  o The downsampler currently just takes the top left pixel from the
 *    source rectangle.  Eventually sampling options of averaging, mode, and
 *    ``center pixel'' should be offered.
 *
 *  o The code will attempt to use the same kind of compression,
 *    photometric interpretation, and organization as the source image, but
 *    it doesn't copy geotiff tags to the reduced resolution images.
 *
 *  o Reduced resolution overviews for multi-sample files will currently
 *    always be generated as PLANARCONFIG_SEPARATE.  This could be fixed
 *    reasonable easily if needed to improve compatibility with other
 *    packages.  Many don't properly support PLANARCONFIG_SEPARATE. 
 * 
 ******************************************************************************
 * Copyright (c) 1999, Frank Warmerdam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 ******************************************************************************
 */

/* TODO: update notes in header above */

#include <stdio.h>
#include "/var/tmp/sensor.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "tiffio.h"
#include "tif_ovrcache.h"

#ifndef FALSE
#  define FALSE 0
#  define TRUE 1
#endif

#ifndef MAX
#  define MIN(a,b)      ((a<b) ? a : b)
#  define MAX(a,b)      ((a>b) ? a : b)
#endif

void TIFFBuildOverviews( TIFF *, int, int *, int, const char *,
                         int (*)(double,void*), void * );

/************************************************************************/
/*                         TIFF_WriteOverview()                         */
/*                                                                      */
/*      Create a new directory, without any image data for an overview. */
/*      Returns offset of newly created overview directory, but the     */
/*      current directory is reset to be the one in used when this      */
/*      function is called.                                             */
/************************************************************************/

uint32 TIFF_WriteOverview( TIFF *hTIFF, uint32 nXSize, uint32 nYSize,
                           int nBitsPerPixel, int nPlanarConfig, int nSamples, 
                           int nBlockXSize, int nBlockYSize,
                           int bTiled, int nCompressFlag, int nPhotometric,
                           int nSampleFormat,
                           unsigned short *panRed,
                           unsigned short *panGreen,
                           unsigned short *panBlue,
                           int bUseSubIFDs,
                           int nHorSubsampling, int nVerSubsampling )

{
    SensorCall();toff_t	nBaseDirOffset;
    toff_t	nOffset;

    (void) bUseSubIFDs;

    nBaseDirOffset = TIFFCurrentDirOffset( hTIFF );

    TIFFCreateDirectory( hTIFF );

/* -------------------------------------------------------------------- */
/*      Setup TIFF fields.                                              */
/* -------------------------------------------------------------------- */
    TIFFSetField( hTIFF, TIFFTAG_IMAGEWIDTH, nXSize );
    TIFFSetField( hTIFF, TIFFTAG_IMAGELENGTH, nYSize );
    SensorCall();if( nSamples == 1 )
        {/*1*/SensorCall();TIFFSetField( hTIFF, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG );/*2*/}
    else
        {/*3*/SensorCall();TIFFSetField( hTIFF, TIFFTAG_PLANARCONFIG, nPlanarConfig );/*4*/}

    SensorCall();TIFFSetField( hTIFF, TIFFTAG_BITSPERSAMPLE, nBitsPerPixel );
    TIFFSetField( hTIFF, TIFFTAG_SAMPLESPERPIXEL, nSamples );
    TIFFSetField( hTIFF, TIFFTAG_COMPRESSION, nCompressFlag );
    TIFFSetField( hTIFF, TIFFTAG_PHOTOMETRIC, nPhotometric );
    TIFFSetField( hTIFF, TIFFTAG_SAMPLEFORMAT, nSampleFormat );

    SensorCall();if( bTiled )
    {
        SensorCall();TIFFSetField( hTIFF, TIFFTAG_TILEWIDTH, nBlockXSize );
        TIFFSetField( hTIFF, TIFFTAG_TILELENGTH, nBlockYSize );
    }
    else
        {/*5*/SensorCall();TIFFSetField( hTIFF, TIFFTAG_ROWSPERSTRIP, nBlockYSize );/*6*/}

    SensorCall();TIFFSetField( hTIFF, TIFFTAG_SUBFILETYPE, FILETYPE_REDUCEDIMAGE );

    SensorCall();if( nPhotometric == PHOTOMETRIC_YCBCR || nPhotometric == PHOTOMETRIC_ITULAB )
    {
        SensorCall();TIFFSetField( hTIFF, TIFFTAG_YCBCRSUBSAMPLING, nHorSubsampling, nVerSubsampling);
        /* TODO: also write YCbCrPositioning and YCbCrCoefficients tag identical to source IFD */
    }
    /* TODO: add command-line parameter for selecting jpeg compression quality
     * that gets ignored when compression isn't jpeg */

/* -------------------------------------------------------------------- */
/*	Write color table if one is present.				*/
/* -------------------------------------------------------------------- */
    SensorCall();if( panRed != NULL )
    {
        SensorCall();TIFFSetField( hTIFF, TIFFTAG_COLORMAP, panRed, panGreen, panBlue );
    }

/* -------------------------------------------------------------------- */
/*      Write directory, and return byte offset.                        */
/* -------------------------------------------------------------------- */
    SensorCall();if( TIFFWriteCheck( hTIFF, bTiled, "TIFFBuildOverviews" ) == 0 )
        {/*7*/{uint32  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*8*/}

    SensorCall();TIFFWriteDirectory( hTIFF );
    TIFFSetDirectory( hTIFF, (tdir_t) (TIFFNumberOfDirectories(hTIFF)-1) );

    nOffset = TIFFCurrentDirOffset( hTIFF );

    TIFFSetSubDirectory( hTIFF, nBaseDirOffset );

    {uint32  ReplaceReturn = nOffset; SensorCall(); return ReplaceReturn;}
}

/************************************************************************/
/*                       TIFF_GetSourceSamples()                        */
/************************************************************************/

static void 
TIFF_GetSourceSamples( double * padfSamples, unsigned char *pabySrc, 
                       int nPixelBytes, int nSampleFormat, 
                       uint32 nXSize, uint32 nYSize, 
                       int nPixelOffset, int nLineOffset )
{
    SensorCall();uint32  iXOff, iYOff;
    int     iSample;

    iSample = 0;

    SensorCall();for( iYOff = 0; iYOff < nYSize; iYOff++ )
    {
        SensorCall();for( iXOff = 0; iXOff < nXSize; iXOff++ )
        {
            SensorCall();unsigned char *pabyData;

            pabyData = pabySrc + iYOff * nLineOffset + iXOff * nPixelOffset;

            SensorCall();if( nSampleFormat == SAMPLEFORMAT_UINT && nPixelBytes == 1 )
            {
                SensorCall();padfSamples[iSample++] = *pabyData;
            }
            else {/*9*/SensorCall();if( nSampleFormat == SAMPLEFORMAT_UINT && nPixelBytes == 2 )
            {
                SensorCall();padfSamples[iSample++] = ((uint16 *) pabyData)[0];
            }
            else {/*11*/SensorCall();if( nSampleFormat == SAMPLEFORMAT_UINT && nPixelBytes == 4 )
            {
                SensorCall();padfSamples[iSample++] = ((uint32 *) pabyData)[0];
            }
            else {/*13*/SensorCall();if( nSampleFormat == SAMPLEFORMAT_INT && nPixelBytes == 2 )
            {
                SensorCall();padfSamples[iSample++] = ((int16 *) pabyData)[0];
            }
            else {/*15*/SensorCall();if( nSampleFormat == SAMPLEFORMAT_INT && nPixelBytes == 32 )
            {
                SensorCall();padfSamples[iSample++] = ((int32 *) pabyData)[0];
            }
            else {/*17*/SensorCall();if( nSampleFormat == SAMPLEFORMAT_IEEEFP && nPixelBytes == 4 )
            {
                SensorCall();padfSamples[iSample++] = ((float *) pabyData)[0];
            }
            else {/*19*/SensorCall();if( nSampleFormat == SAMPLEFORMAT_IEEEFP && nPixelBytes == 8 )
            {
                SensorCall();padfSamples[iSample++] = ((double *) pabyData)[0];
            ;/*20*/}/*18*/}/*16*/}/*14*/}/*12*/}/*10*/}}
        }
    }
SensorCall();} 

/************************************************************************/
/*                           TIFF_SetSample()                           */
/************************************************************************/

static void 
TIFF_SetSample( unsigned char * pabyData, int nPixelBytes, int nSampleFormat, 
                double dfValue )

{
    SensorCall();if( nSampleFormat == SAMPLEFORMAT_UINT && nPixelBytes == 1 )
    {
        SensorCall();*pabyData = (unsigned char) MAX(0,MIN(255,dfValue));
    }
    else {/*21*/SensorCall();if( nSampleFormat == SAMPLEFORMAT_UINT && nPixelBytes == 2 )
    {
        SensorCall();*((uint16 *)pabyData) = (uint16) MAX(0,MIN(65535,dfValue));
    }
    else {/*23*/SensorCall();if( nSampleFormat == SAMPLEFORMAT_UINT && nPixelBytes == 4 )
    {
        SensorCall();*((uint32 *)pabyData) = (uint32) dfValue;
    }
    else {/*25*/SensorCall();if( nSampleFormat == SAMPLEFORMAT_INT && nPixelBytes == 2 )
    {
        SensorCall();*((int16 *)pabyData) = (int16) MAX(-32768,MIN(32767,dfValue));
    }
    else {/*27*/SensorCall();if( nSampleFormat == SAMPLEFORMAT_INT && nPixelBytes == 32 )
    {
        SensorCall();*((int32 *)pabyData) = (int32) dfValue;
    }
    else {/*29*/SensorCall();if( nSampleFormat == SAMPLEFORMAT_IEEEFP && nPixelBytes == 4 )
    {
        SensorCall();*((float *)pabyData) = (float) dfValue;
    }
    else {/*31*/SensorCall();if( nSampleFormat == SAMPLEFORMAT_IEEEFP && nPixelBytes == 8 )
    {
        SensorCall();*((double *)pabyData) = dfValue;
    ;/*32*/}/*30*/}/*28*/}/*26*/}/*24*/}/*22*/}}
SensorCall();}

/************************************************************************/
/*                          TIFF_DownSample()                           */
/*                                                                      */
/*      Down sample a tile of full res data into a window of a tile     */
/*      of downsampled data.                                            */
/************************************************************************/

static
void TIFF_DownSample( unsigned char *pabySrcTile,
                      uint32 nBlockXSize, uint32 nBlockYSize,
                      int nPixelSkewBits, int nBitsPerPixel,
                      unsigned char * pabyOTile,
                      uint32 nOBlockXSize, uint32 nOBlockYSize,
                      uint32 nTXOff, uint32 nTYOff, int nOMult,
                      int nSampleFormat, const char * pszResampling )

{
    SensorCall();uint32	i, j;
    int         k, nPixelBytes = (nBitsPerPixel) / 8;
    int		nPixelGroupBytes = (nBitsPerPixel+nPixelSkewBits)/8;
    unsigned char *pabySrc, *pabyDst;
    double      *padfSamples;

    assert( nBitsPerPixel >= 8 );

    padfSamples = (double *) malloc(sizeof(double) * nOMult * nOMult);

/* ==================================================================== */
/*      Loop over scanline chunks to process, establishing where the    */
/*      data is going.                                                  */
/* ==================================================================== */
    SensorCall();for( j = 0; j*nOMult < nBlockYSize; j++ )
    {
        SensorCall();if( j + nTYOff >= nOBlockYSize )
            {/*33*/SensorCall();break;/*34*/}

        SensorCall();pabyDst = pabyOTile + ((j+nTYOff)*nOBlockXSize + nTXOff)
            * nPixelBytes * nPixelGroupBytes;

/* -------------------------------------------------------------------- */
/*      Handler nearest resampling ... we don't even care about the     */
/*      data type, we just do a bytewise copy.                          */
/* -------------------------------------------------------------------- */
        SensorCall();if( strncmp(pszResampling,"nearest",4) == 0
            || strncmp(pszResampling,"NEAR",4) == 0 )
        {
            SensorCall();pabySrc = pabySrcTile + j*nOMult*nBlockXSize * nPixelGroupBytes;

            SensorCall();for( i = 0; i*nOMult < nBlockXSize; i++ )
            {
                SensorCall();if( i + nTXOff >= nOBlockXSize )
                    {/*35*/SensorCall();break;/*36*/}
            
                /*
                 * For now use simple subsampling, from the top left corner
                 * of the source block of pixels.
                 */

                SensorCall();for( k = 0; k < nPixelBytes; k++ )
                    {/*37*/SensorCall();pabyDst[k] = pabySrc[k];/*38*/}

                SensorCall();pabyDst += nPixelBytes * nPixelGroupBytes;
                pabySrc += nOMult * nPixelGroupBytes;
            }
        }

/* -------------------------------------------------------------------- */
/*      Handle the case of averaging.  For this we also have to         */
/*      handle each sample format we are concerned with.                */
/* -------------------------------------------------------------------- */
        else {/*39*/SensorCall();if( strncmp(pszResampling,"averag",6) == 0
                 || strncmp(pszResampling,"AVERAG",6) == 0 )
        {
            SensorCall();pabySrc = pabySrcTile + j*nOMult*nBlockXSize * nPixelGroupBytes;

            SensorCall();for( i = 0; i*nOMult < nBlockXSize; i++ )
            {
                SensorCall();double   dfTotal;
                uint32   nXSize, nYSize, iSample;

                SensorCall();if( i + nTXOff >= nOBlockXSize )
                    {/*41*/SensorCall();break;/*42*/}

                SensorCall();nXSize = MIN((uint32)nOMult,nBlockXSize-i);
                nYSize = MIN((uint32)nOMult,nBlockYSize-j);

                TIFF_GetSourceSamples( padfSamples, pabySrc,
                                       nPixelBytes, nSampleFormat,
                                       nXSize, nYSize,
                                       nPixelGroupBytes,
                                       nPixelGroupBytes * nBlockXSize );

                dfTotal = 0;
                SensorCall();for( iSample = 0; iSample < nXSize*nYSize; iSample++ )
                {
                    SensorCall();dfTotal += padfSamples[iSample];
                }

                SensorCall();TIFF_SetSample( pabyDst, nPixelBytes, nSampleFormat, 
                                dfTotal / (nXSize*nYSize) );

                pabySrc += nOMult * nPixelGroupBytes;
                pabyDst += nPixelBytes;
            }
        ;/*40*/}}
    }

    SensorCall();free( padfSamples );
SensorCall();}

/************************************************************************/
/*                     TIFF_DownSample_Subsampled()                     */
/************************************************************************/
static
void TIFF_DownSample_Subsampled( unsigned char *pabySrcTile, int nSample,
                                 uint32 nBlockXSize, uint32 nBlockYSize,
                                 unsigned char * pabyOTile,
                                 uint32 nOBlockXSize, uint32 nOBlockYSize,
                                 uint32 nTXOff, uint32 nTYOff, int nOMult,
                                 const char *pszResampling,
                                 int nHorSubsampling, int nVerSubsampling )
{
    /* TODO: test with variety of subsampling values, and incovinient tile/strip sizes */
    SensorCall();int nSampleBlockSize;
    int nSourceSampleRowSize;
    int nDestSampleRowSize;
    uint32  nSourceX, nSourceY;
    uint32  nSourceXSec, nSourceYSec;
    uint32  nSourceXSecEnd, nSourceYSecEnd;
    uint32  nDestX, nDestY;
    int nSampleOffsetInSampleBlock;
    unsigned int nCummulator;
    unsigned int nCummulatorCount;

    nSampleBlockSize = nHorSubsampling * nVerSubsampling + 2;
    nSourceSampleRowSize = 
        ( ( nBlockXSize + nHorSubsampling - 1 ) / nHorSubsampling ) * nSampleBlockSize;
    nDestSampleRowSize = 
        ( ( nOBlockXSize + nHorSubsampling - 1 ) / nHorSubsampling ) * nSampleBlockSize;

    SensorCall();if( strncmp(pszResampling,"nearest",4) == 0
        || strncmp(pszResampling,"NEAR",4) == 0 )
    {
    	SensorCall();if( nSample == 0 )
        {
            SensorCall();for( nSourceY = 0, nDestY = nTYOff; 
                 nSourceY < nBlockYSize; 
                 nSourceY += nOMult, nDestY ++)
            {
                SensorCall();if( nDestY >= nOBlockYSize )
                    {/*43*/SensorCall();break;/*44*/}

                SensorCall();for( nSourceX = 0, nDestX = nTXOff; 
                     nSourceX < nBlockXSize; 
                     nSourceX += nOMult, nDestX ++)
                {
                    SensorCall();if( nDestX >= nOBlockXSize )
                        {/*45*/SensorCall();break;/*46*/}

                    SensorCall();* ( pabyOTile + ( nDestY / nVerSubsampling ) * nDestSampleRowSize
                        + ( nDestY % nVerSubsampling ) * nHorSubsampling
                        + ( nDestX / nHorSubsampling ) * nSampleBlockSize
                        + ( nDestX % nHorSubsampling ) ) =
                        * ( pabySrcTile + ( nSourceY / nVerSubsampling ) * nSourceSampleRowSize
                            + ( nSourceY % nVerSubsampling ) * nHorSubsampling
                            + ( nSourceX / nHorSubsampling ) * nSampleBlockSize
                            + ( nSourceX % nHorSubsampling ) );
                }
            }
        }
        else
        {
            SensorCall();nSampleOffsetInSampleBlock = nHorSubsampling * nVerSubsampling + nSample - 1;
            SensorCall();for( nSourceY = 0, nDestY = ( nTYOff / nVerSubsampling ); 
                 nSourceY < ( nBlockYSize / nVerSubsampling );
                 nSourceY += nOMult, nDestY ++)
            {
                SensorCall();if( nDestY*nVerSubsampling >= nOBlockYSize )
                    {/*47*/SensorCall();break;/*48*/}

            	SensorCall();for( nSourceX = 0, nDestX = ( nTXOff / nHorSubsampling ); 
                     nSourceX < ( nBlockXSize / nHorSubsampling );
                     nSourceX += nOMult, nDestX ++)
                {
                    SensorCall();if( nDestX*nHorSubsampling >= nOBlockXSize )
                        {/*49*/SensorCall();break;/*50*/}

                    SensorCall();* ( pabyOTile + nDestY * nDestSampleRowSize
                        + nDestX * nSampleBlockSize
                        + nSampleOffsetInSampleBlock ) =
                    	* ( pabySrcTile + nSourceY * nSourceSampleRowSize
                            + nSourceX * nSampleBlockSize
                            + nSampleOffsetInSampleBlock );
                }
            }
        }
    }
    else {/*51*/SensorCall();if( strncmp(pszResampling,"averag",6) == 0
             || strncmp(pszResampling,"AVERAG",6) == 0 )
    {
    	SensorCall();if( nSample == 0 )
        {
            SensorCall();for( nSourceY = 0, nDestY = nTYOff; nSourceY < nBlockYSize; nSourceY += nOMult, nDestY ++)
            {
                SensorCall();if( nDestY >= nOBlockYSize )
                    {/*53*/SensorCall();break;/*54*/}

                SensorCall();for( nSourceX = 0, nDestX = nTXOff; nSourceX < nBlockXSize; nSourceX += nOMult, nDestX ++)
                {
                    SensorCall();if( nDestX >= nOBlockXSize )
                        {/*55*/SensorCall();break;/*56*/}

                    SensorCall();nSourceXSecEnd = nSourceX + nOMult;
                    SensorCall();if( nSourceXSecEnd > nBlockXSize )
                        {/*57*/SensorCall();nSourceXSecEnd = nBlockXSize;/*58*/}
                    SensorCall();nSourceYSecEnd = nSourceY + nOMult;
                    SensorCall();if( nSourceYSecEnd > nBlockYSize )
                        {/*59*/SensorCall();nSourceYSecEnd = nBlockYSize;/*60*/}
                    SensorCall();nCummulator = 0;
                    SensorCall();for( nSourceYSec = nSourceY; nSourceYSec < nSourceYSecEnd; nSourceYSec ++)
                    {
                        SensorCall();for( nSourceXSec = nSourceX; nSourceXSec < nSourceXSecEnd; nSourceXSec ++)
                        {
                            SensorCall();nCummulator += * ( pabySrcTile + ( nSourceYSec / nVerSubsampling ) * nSourceSampleRowSize
                                               + ( nSourceYSec % nVerSubsampling ) * nHorSubsampling
                                               + ( nSourceXSec / nHorSubsampling ) * nSampleBlockSize
                                               + ( nSourceXSec % nHorSubsampling ) );
                        }
                    }
                    SensorCall();nCummulatorCount = ( nSourceXSecEnd - nSourceX ) * ( nSourceYSecEnd - nSourceY );
                    * ( pabyOTile + ( nDestY / nVerSubsampling ) * nDestSampleRowSize
                        + ( nDestY % nVerSubsampling ) * nHorSubsampling
                        + ( nDestX / nHorSubsampling ) * nSampleBlockSize
                        + ( nDestX % nHorSubsampling ) ) =
                        ( ( nCummulator + ( nCummulatorCount >> 1 ) ) / nCummulatorCount );
                }
            }
        }
        else
        {
            SensorCall();nSampleOffsetInSampleBlock = nHorSubsampling * nVerSubsampling + nSample - 1;
            SensorCall();for( nSourceY = 0, nDestY = ( nTYOff / nVerSubsampling ); nSourceY < ( nBlockYSize / nVerSubsampling );
                 nSourceY += nOMult, nDestY ++)
            {
                SensorCall();if( nDestY*nVerSubsampling >= nOBlockYSize )
                    {/*61*/SensorCall();break;/*62*/}

                SensorCall();for( nSourceX = 0, nDestX = ( nTXOff / nHorSubsampling ); nSourceX < ( nBlockXSize / nHorSubsampling );
                     nSourceX += nOMult, nDestX ++)
                {
                    SensorCall();if( nDestX*nHorSubsampling >= nOBlockXSize )
                        {/*63*/SensorCall();break;/*64*/}

                    SensorCall();nSourceXSecEnd = nSourceX + nOMult;
                    SensorCall();if( nSourceXSecEnd > ( nBlockXSize / nHorSubsampling ) )
                        {/*65*/SensorCall();nSourceXSecEnd = ( nBlockXSize / nHorSubsampling );/*66*/}
                    SensorCall();nSourceYSecEnd = nSourceY + nOMult;
                    SensorCall();if( nSourceYSecEnd > ( nBlockYSize / nVerSubsampling ) )
                        {/*67*/SensorCall();nSourceYSecEnd = ( nBlockYSize / nVerSubsampling );/*68*/}
                    SensorCall();nCummulator = 0;
                    SensorCall();for( nSourceYSec = nSourceY; nSourceYSec < nSourceYSecEnd; nSourceYSec ++)
                    {
                        SensorCall();for( nSourceXSec = nSourceX; nSourceXSec < nSourceXSecEnd; nSourceXSec ++)
                        {
                            SensorCall();nCummulator += * ( pabySrcTile + nSourceYSec * nSourceSampleRowSize
                                               + nSourceXSec * nSampleBlockSize
                                               + nSampleOffsetInSampleBlock );
                        }
                    }
                    SensorCall();nCummulatorCount = ( nSourceXSecEnd - nSourceX ) * ( nSourceYSecEnd - nSourceY );
                    * ( pabyOTile + nDestY * nDestSampleRowSize
                        + nDestX * nSampleBlockSize
                        + nSampleOffsetInSampleBlock ) =
                        ( ( nCummulator + ( nCummulatorCount >> 1 ) ) / nCummulatorCount );
                }
            }
        }
    ;/*52*/}}
SensorCall();}

/************************************************************************/
/*                      TIFF_ProcessFullResBlock()                      */
/*                                                                      */
/*      Process one block of full res data, downsampling into each      */
/*      of the overviews.                                               */
/************************************************************************/

void TIFF_ProcessFullResBlock( TIFF *hTIFF, int nPlanarConfig,
                               int bSubsampled,
                               int nHorSubsampling, int nVerSubsampling,
                               int nOverviews, int * panOvList,
                               int nBitsPerPixel,
                               int nSamples, TIFFOvrCache ** papoRawBIs,
                               uint32 nSXOff, uint32 nSYOff,
                               unsigned char *pabySrcTile,
                               uint32 nBlockXSize, uint32 nBlockYSize,
                               int nSampleFormat, const char * pszResampling )
    
{
    SensorCall();int		iOverview, iSample;

    SensorCall();for( iSample = 0; iSample < nSamples; iSample++ )
    {
        /*
         * We have to read a tile/strip for each sample for
         * PLANARCONFIG_SEPARATE.  Otherwise, we just read all the samples
         * at once when handling the first sample.
         */
        SensorCall();if( nPlanarConfig == PLANARCONFIG_SEPARATE || iSample == 0 )
        {
            SensorCall();if( TIFFIsTiled(hTIFF) )
            {
                SensorCall();TIFFReadEncodedTile( hTIFF,
                                     TIFFComputeTile(hTIFF, nSXOff, nSYOff,
                                                     0, (tsample_t)iSample ),
                                     pabySrcTile,
                                     TIFFTileSize(hTIFF));
            }
            else
            {
                SensorCall();TIFFReadEncodedStrip( hTIFF,
                                      TIFFComputeStrip(hTIFF, nSYOff,
                                                       (tsample_t) iSample),
                                      pabySrcTile,
                                      TIFFStripSize(hTIFF) );
            }
        }

        /*        
         * Loop over destination overview layers
         */
        SensorCall();for( iOverview = 0; iOverview < nOverviews; iOverview++ )
        {
            SensorCall();TIFFOvrCache *poRBI = papoRawBIs[iOverview];
            unsigned char *pabyOTile;
            uint32  nTXOff, nTYOff, nOXOff, nOYOff, nOMult;
            uint32  nOBlockXSize = poRBI->nBlockXSize;
            uint32  nOBlockYSize = poRBI->nBlockYSize;
            int     nSkewBits, nSampleByteOffset; 

            /*
             * Fetch the destination overview tile
             */
            nOMult = panOvList[iOverview];
            nOXOff = (nSXOff/nOMult) / nOBlockXSize;
            nOYOff = (nSYOff/nOMult) / nOBlockYSize;

            SensorCall();if( bSubsampled )
            {
                SensorCall();pabyOTile = TIFFGetOvrBlock_Subsampled( poRBI, nOXOff, nOYOff );

                /*
                 * Establish the offset into this tile at which we should
                 * start placing data.
                 */
                nTXOff = (nSXOff - nOXOff*nOMult*nOBlockXSize) / nOMult;
                nTYOff = (nSYOff - nOYOff*nOMult*nOBlockYSize) / nOMult;


#ifdef DBMALLOC
                malloc_chain_check( 1 );
#endif
                TIFF_DownSample_Subsampled( pabySrcTile, iSample,
                                            nBlockXSize, nBlockYSize,
                                            pabyOTile,
                                            poRBI->nBlockXSize, poRBI->nBlockYSize,
                                            nTXOff, nTYOff,
                                            nOMult, pszResampling,
                                            nHorSubsampling, nVerSubsampling );
#ifdef DBMALLOC
                malloc_chain_check( 1 );
#endif

            }
            else
            {

                SensorCall();pabyOTile = TIFFGetOvrBlock( poRBI, nOXOff, nOYOff, iSample );

                /*
                 * Establish the offset into this tile at which we should
                 * start placing data.
                 */
                nTXOff = (nSXOff - nOXOff*nOMult*nOBlockXSize) / nOMult;
                nTYOff = (nSYOff - nOYOff*nOMult*nOBlockYSize) / nOMult;

                /*
                 * Figure out the skew (extra space between ``our samples'') and
                 * the byte offset to the first sample.
                 */
                assert( (nBitsPerPixel % 8) == 0 );
                SensorCall();if( nPlanarConfig == PLANARCONFIG_SEPARATE )
                {
                    SensorCall();nSkewBits = 0;
                    nSampleByteOffset = 0;
                }
                else
                {
                    SensorCall();nSkewBits = nBitsPerPixel * (nSamples-1);
                    nSampleByteOffset = (nBitsPerPixel/8) * iSample;
                }

                /*
                 * Perform the downsampling.
                 */
#ifdef DBMALLOC
                malloc_chain_check( 1 );
#endif
                SensorCall();TIFF_DownSample( pabySrcTile + nSampleByteOffset,
                               nBlockXSize, nBlockYSize,
                               nSkewBits, nBitsPerPixel, pabyOTile,
                               poRBI->nBlockXSize, poRBI->nBlockYSize,
                               nTXOff, nTYOff,
                               nOMult, nSampleFormat, pszResampling );
#ifdef DBMALLOC
                malloc_chain_check( 1 );
#endif
            }
        }
    }
SensorCall();}

/************************************************************************/
/*                        TIFF_BuildOverviews()                         */
/*                                                                      */
/*      Build the requested list of overviews.  Overviews are           */
/*      maintained in a bunch of temporary files and then these are     */
/*      written back to the TIFF file.  Only one pass through the       */
/*      source TIFF file is made for any number of output               */
/*      overviews.                                                      */
/************************************************************************/

void TIFFBuildOverviews( TIFF *hTIFF, int nOverviews, int * panOvList,
                         int bUseSubIFDs, const char *pszResampleMethod,
                         int (*pfnProgress)( double, void * ),
                         void * pProgressData )

{
    SensorCall();TIFFOvrCache	**papoRawBIs;
    uint32		nXSize, nYSize, nBlockXSize, nBlockYSize;
    uint16		nBitsPerPixel, nPhotometric, nCompressFlag, nSamples,
        nPlanarConfig, nSampleFormat;
    int         bSubsampled;
    uint16      nHorSubsampling, nVerSubsampling;
    int			bTiled, nSXOff, nSYOff, i;
    unsigned char	*pabySrcTile;
    uint16		*panRedMap, *panGreenMap, *panBlueMap;
    TIFFErrorHandler    pfnWarning;

    (void) pfnProgress;
    (void) pProgressData;

/* -------------------------------------------------------------------- */
/*      Get the base raster size.                                       */
/* -------------------------------------------------------------------- */
    TIFFGetField( hTIFF, TIFFTAG_IMAGEWIDTH, &nXSize );
    TIFFGetField( hTIFF, TIFFTAG_IMAGELENGTH, &nYSize );

    TIFFGetField( hTIFF, TIFFTAG_BITSPERSAMPLE, &nBitsPerPixel );
    /* TODO: nBitsPerPixel seems misnomer and may need renaming to nBitsPerSample */
    TIFFGetField( hTIFF, TIFFTAG_SAMPLESPERPIXEL, &nSamples );
    TIFFGetFieldDefaulted( hTIFF, TIFFTAG_PLANARCONFIG, &nPlanarConfig );

    TIFFGetFieldDefaulted( hTIFF, TIFFTAG_PHOTOMETRIC, &nPhotometric );
    TIFFGetFieldDefaulted( hTIFF, TIFFTAG_COMPRESSION, &nCompressFlag );
    TIFFGetFieldDefaulted( hTIFF, TIFFTAG_SAMPLEFORMAT, &nSampleFormat );

    SensorCall();if( nPhotometric == PHOTOMETRIC_YCBCR || nPhotometric == PHOTOMETRIC_ITULAB )
    {
        SensorCall();if( nBitsPerPixel != 8 || nSamples != 3 || nPlanarConfig != PLANARCONFIG_CONTIG ||
            nSampleFormat != SAMPLEFORMAT_UINT)
        {
            /* TODO: use of TIFFError is inconsistent with use of fprintf in addtiffo.c, sort out */
            SensorCall();TIFFErrorExt( TIFFClientdata(hTIFF), "TIFFBuildOverviews",
                          "File `%s' has an unsupported subsampling configuration.\n",
                          TIFFFileName(hTIFF) );
            /* If you need support for this particular flavor, please contact either
             * Frank Warmerdam warmerdam@pobox.com
             * Joris Van Damme info@awaresystems.be
             */
            SensorCall();return;
        }
        SensorCall();bSubsampled = 1;
        TIFFGetField( hTIFF, TIFFTAG_YCBCRSUBSAMPLING, &nHorSubsampling, &nVerSubsampling );
        /* TODO: find out if maybe TIFFGetFieldDefaulted is better choice for YCbCrSubsampling tag */
    }
    else
    {
        SensorCall();if( nBitsPerPixel < 8 )
        {
            /* TODO: use of TIFFError is inconsistent with use of fprintf in addtiffo.c, sort out */
            SensorCall();TIFFErrorExt( TIFFClientdata(hTIFF), "TIFFBuildOverviews",
                          "File `%s' has samples of %d bits per sample.  Sample\n"
                          "sizes of less than 8 bits per sample are not supported.\n",
                          TIFFFileName(hTIFF), nBitsPerPixel );
            SensorCall();return;
        }
        SensorCall();bSubsampled = 0;
        nHorSubsampling = 1;
        nVerSubsampling = 1;
    }

/* -------------------------------------------------------------------- */
/*      Turn off warnings to avoid alot of repeated warnings while      */
/*      rereading directories.                                          */
/* -------------------------------------------------------------------- */
    SensorCall();pfnWarning = TIFFSetWarningHandler( NULL );

/* -------------------------------------------------------------------- */
/*      Get the base raster block size.                                 */
/* -------------------------------------------------------------------- */
    SensorCall();if( TIFFGetField( hTIFF, TIFFTAG_ROWSPERSTRIP, &(nBlockYSize) ) )
    {
        SensorCall();nBlockXSize = nXSize;
        bTiled = FALSE;
    }
    else
    {
        SensorCall();TIFFGetField( hTIFF, TIFFTAG_TILEWIDTH, &nBlockXSize );
        TIFFGetField( hTIFF, TIFFTAG_TILELENGTH, &nBlockYSize );
        bTiled = TRUE;
    }

/* -------------------------------------------------------------------- */
/*	Capture the pallette if there is one.				*/
/* -------------------------------------------------------------------- */
    SensorCall();if( TIFFGetField( hTIFF, TIFFTAG_COLORMAP,
                      &panRedMap, &panGreenMap, &panBlueMap ) )
    {
        SensorCall();uint16		*panRed2, *panGreen2, *panBlue2;
        int             nColorCount = 1 << nBitsPerPixel;

        panRed2 = (uint16 *) _TIFFmalloc(2*nColorCount);
        panGreen2 = (uint16 *) _TIFFmalloc(2*nColorCount);
        panBlue2 = (uint16 *) _TIFFmalloc(2*nColorCount);

        memcpy( panRed2, panRedMap, 2 * nColorCount );
        memcpy( panGreen2, panGreenMap, 2 * nColorCount );
        memcpy( panBlue2, panBlueMap, 2 * nColorCount );

        panRedMap = panRed2;
        panGreenMap = panGreen2;
        panBlueMap = panBlue2;
    }
    else
    {
        SensorCall();panRedMap = panGreenMap = panBlueMap = NULL;
    }

/* -------------------------------------------------------------------- */
/*      Initialize overviews.                                           */
/* -------------------------------------------------------------------- */
    SensorCall();papoRawBIs = (TIFFOvrCache **) _TIFFmalloc(nOverviews*sizeof(void*));

    SensorCall();for( i = 0; i < nOverviews; i++ )
    {
        SensorCall();uint32  nOXSize, nOYSize, nOBlockXSize, nOBlockYSize;
        toff_t  nDirOffset;

        nOXSize = (nXSize + panOvList[i] - 1) / panOvList[i];
        nOYSize = (nYSize + panOvList[i] - 1) / panOvList[i];

        nOBlockXSize = MIN(nBlockXSize,nOXSize);
        nOBlockYSize = MIN(nBlockYSize,nOYSize);

        SensorCall();if( bTiled )
        {
            SensorCall();if( (nOBlockXSize % 16) != 0 )
                {/*69*/SensorCall();nOBlockXSize = nOBlockXSize + 16 - (nOBlockXSize % 16);/*70*/}

            SensorCall();if( (nOBlockYSize % 16) != 0 )
                {/*71*/SensorCall();nOBlockYSize = nOBlockYSize + 16 - (nOBlockYSize % 16);/*72*/}
        }

        SensorCall();nDirOffset = TIFF_WriteOverview( hTIFF, nOXSize, nOYSize,
                                         nBitsPerPixel, nPlanarConfig,
                                         nSamples, nOBlockXSize, nOBlockYSize,
                                         bTiled, nCompressFlag, nPhotometric,
                                         nSampleFormat,
                                         panRedMap, panGreenMap, panBlueMap,
                                         bUseSubIFDs,
                                         nHorSubsampling, nVerSubsampling );
        
        papoRawBIs[i] = TIFFCreateOvrCache( hTIFF, nDirOffset );
    }

    SensorCall();if( panRedMap != NULL )
    {
        SensorCall();_TIFFfree( panRedMap );
        _TIFFfree( panGreenMap );
        _TIFFfree( panBlueMap );
    }
    
/* -------------------------------------------------------------------- */
/*      Allocate a buffer to hold a source block.                       */
/* -------------------------------------------------------------------- */
    SensorCall();if( bTiled )
        {/*73*/SensorCall();pabySrcTile = (unsigned char *) _TIFFmalloc(TIFFTileSize(hTIFF));/*74*/}
    else
        {/*75*/SensorCall();pabySrcTile = (unsigned char *) _TIFFmalloc(TIFFStripSize(hTIFF));/*76*/}
    
/* -------------------------------------------------------------------- */
/*      Loop over the source raster, applying data to the               */
/*      destination raster.                                             */
/* -------------------------------------------------------------------- */
    SensorCall();for( nSYOff = 0; nSYOff < (int) nYSize; nSYOff += nBlockYSize )
    {
        SensorCall();for( nSXOff = 0; nSXOff < (int) nXSize; nSXOff += nBlockXSize )
        {
            /*
             * Read and resample into the various overview images.
             */
            
            SensorCall();TIFF_ProcessFullResBlock( hTIFF, nPlanarConfig,
                                      bSubsampled,nHorSubsampling,nVerSubsampling,
                                      nOverviews, panOvList,
                                      nBitsPerPixel, nSamples, papoRawBIs,
                                      nSXOff, nSYOff, pabySrcTile,
                                      nBlockXSize, nBlockYSize,
                                      nSampleFormat, pszResampleMethod );
        }
    }

    SensorCall();_TIFFfree( pabySrcTile );

/* -------------------------------------------------------------------- */
/*      Cleanup the rawblockedimage files.                              */
/* -------------------------------------------------------------------- */
    SensorCall();for( i = 0; i < nOverviews; i++ )
    {
        SensorCall();TIFFDestroyOvrCache( papoRawBIs[i] );
    }

    SensorCall();if( papoRawBIs != NULL )
        {/*77*/SensorCall();_TIFFfree( papoRawBIs );/*78*/}

    SensorCall();TIFFSetWarningHandler( pfnWarning );
SensorCall();}


/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
