/* $Id: tiff-grayscale.c,v 1.6 2010-06-08 18:55:15 bfriesen Exp $ */

/*
 * tiff-grayscale.c -- create a Class G (grayscale) TIFF file
 *      with a gray response curve in linear optical density
 *
 * Copyright 1990 by Digital Equipment Corporation, Maynard, Massachusetts.
 *
 *                        All Rights Reserved
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose and without fee is hereby granted,
 * provided that the above copyright notice appear in all copies and that
 * both that copyright notice and this permission notice appear in
 * supporting documentation, and that the name of Digital not be
 * used in advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 *
 * DIGITAL DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * DIGITAL BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#include <math.h>
#include "/var/tmp/sensor.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tiffio.h"

#define WIDTH       512
#define HEIGHT      WIDTH

char *              programName;
void                Usage();

int main(int argc, char **argv)
{
    SensorCall();int             bits_per_pixel = 8, cmsize, i, j, k,
                    gray_index, chunk_size = 32, nchunks = 16;
    unsigned char * scan_line;
    uint16 *        gray;
    float           refblackwhite[2*1];
    TIFF *          tif;

    programName = argv[0];

    SensorCall();if (argc != 4)
        {/*1*/SensorCall();Usage();/*2*/}

    SensorCall();if (!strcmp(argv[1], "-depth"))
         {/*3*/SensorCall();bits_per_pixel = atoi(argv[2]);/*4*/}
    else
         {/*5*/SensorCall();Usage();/*6*/}

    SensorCall();switch (bits_per_pixel) {
        case 8:
            SensorCall();nchunks = 16;
            chunk_size = 32;
            SensorCall();break;
        case 4:
            SensorCall();nchunks = 4;
            chunk_size = 128;
            SensorCall();break;
        case 2:
            SensorCall();nchunks = 2;
            chunk_size = 256;
            SensorCall();break;
        default:
            SensorCall();Usage();
    }

    SensorCall();cmsize = nchunks * nchunks;
    gray = (uint16 *) malloc(cmsize * sizeof(uint16));

    gray[0] = 3000;
    SensorCall();for (i = 1; i < cmsize; i++)
        {/*7*/SensorCall();gray[i] = (uint16) (-log10((double) i / (cmsize - 1)) * 1000);/*8*/}

    SensorCall();refblackwhite[0] = 0.0;
    refblackwhite[1] = (float)((1L<<bits_per_pixel) - 1);

    SensorCall();if ((tif = TIFFOpen(argv[3], "w")) == NULL) {
        SensorCall();fprintf(stderr, "can't open %s as a TIFF file\n", argv[3]);
		free(gray);
        {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
    }

    SensorCall();TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, WIDTH);
    TIFFSetField(tif, TIFFTAG_IMAGELENGTH, HEIGHT);
    TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, bits_per_pixel);
    TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
    TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
    TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1);
    TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, 1);
    TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    TIFFSetField(tif, TIFFTAG_REFERENCEBLACKWHITE, refblackwhite);
    TIFFSetField(tif, TIFFTAG_TRANSFERFUNCTION, gray);
    TIFFSetField(tif, TIFFTAG_RESOLUTIONUNIT, RESUNIT_NONE);

    scan_line = (unsigned char *) malloc(WIDTH / (8 / bits_per_pixel));

    SensorCall();for (i = 0; i < HEIGHT; i++) {
        SensorCall();for (j = 0, k = 0; j < WIDTH;) {
            SensorCall();gray_index = (j / chunk_size) + ((i / chunk_size) * nchunks);

            SensorCall();switch (bits_per_pixel) {
            case 8:
                SensorCall();scan_line[k++] = gray_index;
                j++;
                SensorCall();break;
            case 4:
                SensorCall();scan_line[k++] = (gray_index << 4) + gray_index;
                j += 2;
                SensorCall();break;
            case 2:
                SensorCall();scan_line[k++] = (gray_index << 6) + (gray_index << 4)
                    + (gray_index << 2) + gray_index;
                j += 4;
                SensorCall();break;
            }
        }
        SensorCall();TIFFWriteScanline(tif, scan_line, i, 0);
    }

    SensorCall();free(scan_line);
    TIFFClose(tif);
    {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

void
Usage()
{
    SensorCall();fprintf(stderr, "Usage: %s -depth (8 | 4 | 2) tiff-image\n", programName);
    exit(0);
SensorCall();}
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
