/* $Id: tif_dirread.c,v 1.178 2012-08-19 16:56:34 bfriesen Exp $ */

/*
 * Copyright (c) 1988-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

/*
 * TIFF Library.
 *
 * Directory Read Support Routines.
 */

/* Suggested pending improvements:
 * - add a field 'ignore' to the TIFFDirEntry structure, to flag status,
 *   eliminating current use of the IGNORE value, and therefore eliminating
 *   current irrational behaviour on tags with tag id code 0
 * - add a field 'field_info' to the TIFFDirEntry structure, and set that with
 *   the pointer to the appropriate TIFFField structure early on in
 *   TIFFReadDirectory, so as to eliminate current possibly repetitive lookup.
 */

#include "tiffiop.h"
#include "/var/tmp/sensor.h"

#define IGNORE 0          /* tag placeholder used below */
#define FAILED_FII    ((uint32) -1)

#ifdef HAVE_IEEEFP
# define TIFFCvtIEEEFloatToNative(tif, n, fp)
# define TIFFCvtIEEEDoubleToNative(tif, n, dp)
#else
extern void TIFFCvtIEEEFloatToNative(TIFF*, uint32, float*);
extern void TIFFCvtIEEEDoubleToNative(TIFF*, uint32, double*);
#endif

enum TIFFReadDirEntryErr {
	TIFFReadDirEntryErrOk = 0,
	TIFFReadDirEntryErrCount = 1,
	TIFFReadDirEntryErrType = 2,
	TIFFReadDirEntryErrIo = 3,
	TIFFReadDirEntryErrRange = 4,
	TIFFReadDirEntryErrPsdif = 5,
	TIFFReadDirEntryErrSizesan = 6,
	TIFFReadDirEntryErrAlloc = 7,
};

static enum TIFFReadDirEntryErr TIFFReadDirEntryByte(TIFF* tif, TIFFDirEntry* direntry, uint8* value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryShort(TIFF* tif, TIFFDirEntry* direntry, uint16* value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryLong(TIFF* tif, TIFFDirEntry* direntry, uint32* value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryLong8(TIFF* tif, TIFFDirEntry* direntry, uint64* value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryFloat(TIFF* tif, TIFFDirEntry* direntry, float* value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryDouble(TIFF* tif, TIFFDirEntry* direntry, double* value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryIfd8(TIFF* tif, TIFFDirEntry* direntry, uint64* value);

static enum TIFFReadDirEntryErr TIFFReadDirEntryArray(TIFF* tif, TIFFDirEntry* direntry, uint32* count, uint32 desttypesize, void** value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryByteArray(TIFF* tif, TIFFDirEntry* direntry, uint8** value);
static enum TIFFReadDirEntryErr TIFFReadDirEntrySbyteArray(TIFF* tif, TIFFDirEntry* direntry, int8** value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryShortArray(TIFF* tif, TIFFDirEntry* direntry, uint16** value);
static enum TIFFReadDirEntryErr TIFFReadDirEntrySshortArray(TIFF* tif, TIFFDirEntry* direntry, int16** value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryLongArray(TIFF* tif, TIFFDirEntry* direntry, uint32** value);
static enum TIFFReadDirEntryErr TIFFReadDirEntrySlongArray(TIFF* tif, TIFFDirEntry* direntry, int32** value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryLong8Array(TIFF* tif, TIFFDirEntry* direntry, uint64** value);
static enum TIFFReadDirEntryErr TIFFReadDirEntrySlong8Array(TIFF* tif, TIFFDirEntry* direntry, int64** value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryFloatArray(TIFF* tif, TIFFDirEntry* direntry, float** value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryDoubleArray(TIFF* tif, TIFFDirEntry* direntry, double** value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryIfd8Array(TIFF* tif, TIFFDirEntry* direntry, uint64** value);

static enum TIFFReadDirEntryErr TIFFReadDirEntryPersampleShort(TIFF* tif, TIFFDirEntry* direntry, uint16* value);
#if 0
static enum TIFFReadDirEntryErr TIFFReadDirEntryPersampleDouble(TIFF* tif, TIFFDirEntry* direntry, double* value);
#endif

static void TIFFReadDirEntryCheckedByte(TIFF* tif, TIFFDirEntry* direntry, uint8* value);
static void TIFFReadDirEntryCheckedSbyte(TIFF* tif, TIFFDirEntry* direntry, int8* value);
static void TIFFReadDirEntryCheckedShort(TIFF* tif, TIFFDirEntry* direntry, uint16* value);
static void TIFFReadDirEntryCheckedSshort(TIFF* tif, TIFFDirEntry* direntry, int16* value);
static void TIFFReadDirEntryCheckedLong(TIFF* tif, TIFFDirEntry* direntry, uint32* value);
static void TIFFReadDirEntryCheckedSlong(TIFF* tif, TIFFDirEntry* direntry, int32* value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckedLong8(TIFF* tif, TIFFDirEntry* direntry, uint64* value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckedSlong8(TIFF* tif, TIFFDirEntry* direntry, int64* value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckedRational(TIFF* tif, TIFFDirEntry* direntry, double* value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckedSrational(TIFF* tif, TIFFDirEntry* direntry, double* value);
static void TIFFReadDirEntryCheckedFloat(TIFF* tif, TIFFDirEntry* direntry, float* value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckedDouble(TIFF* tif, TIFFDirEntry* direntry, double* value);

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeByteSbyte(int8 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeByteShort(uint16 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeByteSshort(int16 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeByteLong(uint32 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeByteSlong(int32 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeByteLong8(uint64 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeByteSlong8(int64 value);

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSbyteByte(uint8 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSbyteShort(uint16 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSbyteSshort(int16 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSbyteLong(uint32 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSbyteSlong(int32 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSbyteLong8(uint64 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSbyteSlong8(int64 value);

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeShortSbyte(int8 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeShortSshort(int16 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeShortLong(uint32 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeShortSlong(int32 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeShortLong8(uint64 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeShortSlong8(int64 value);

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSshortShort(uint16 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSshortLong(uint32 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSshortSlong(int32 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSshortLong8(uint64 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSshortSlong8(int64 value);

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeLongSbyte(int8 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeLongSshort(int16 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeLongSlong(int32 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeLongLong8(uint64 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeLongSlong8(int64 value);

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSlongLong(uint32 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSlongLong8(uint64 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSlongSlong8(int64 value);

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeLong8Sbyte(int8 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeLong8Sshort(int16 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeLong8Slong(int32 value);
static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeLong8Slong8(int64 value);

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSlong8Long8(uint64 value);

static enum TIFFReadDirEntryErr TIFFReadDirEntryData(TIFF* tif, uint64 offset, tmsize_t size, void* dest);
static void TIFFReadDirEntryOutputErr(TIFF* tif, enum TIFFReadDirEntryErr err, const char* module, const char* tagname, int recover);

static void TIFFReadDirectoryCheckOrder(TIFF* tif, TIFFDirEntry* dir, uint16 dircount);
static TIFFDirEntry* TIFFReadDirectoryFindEntry(TIFF* tif, TIFFDirEntry* dir, uint16 dircount, uint16 tagid);
static void TIFFReadDirectoryFindFieldInfo(TIFF* tif, uint16 tagid, uint32* fii);

static int EstimateStripByteCounts(TIFF* tif, TIFFDirEntry* dir, uint16 dircount);
static void MissingRequired(TIFF*, const char*);
static int TIFFCheckDirOffset(TIFF* tif, uint64 diroff);
static int CheckDirCount(TIFF*, TIFFDirEntry*, uint32);
static uint16 TIFFFetchDirectory(TIFF* tif, uint64 diroff, TIFFDirEntry** pdir, uint64* nextdiroff);
static int TIFFFetchNormalTag(TIFF*, TIFFDirEntry*, int recover);
static int TIFFFetchStripThing(TIFF* tif, TIFFDirEntry* dir, uint32 nstrips, uint64** lpp);
static int TIFFFetchSubjectDistance(TIFF*, TIFFDirEntry*);
static void ChopUpSingleUncompressedStrip(TIFF*);
static uint64 TIFFReadUInt64(const uint8 *value);

typedef union _UInt64Aligned_t
{
        double d;
	uint64 l;
	uint32 i[2];
	uint16 s[4];
	uint8  c[8];
} UInt64Aligned_t;

/*
  Unaligned safe copy of a uint64 value from an octet array.
*/
static uint64 TIFFReadUInt64(const uint8 *value)
{
	SensorCall();UInt64Aligned_t result;

	result.c[0]=value[0];
	result.c[1]=value[1];
	result.c[2]=value[2];
	result.c[3]=value[3];
	result.c[4]=value[4];
	result.c[5]=value[5];
	result.c[6]=value[6];
	result.c[7]=value[7];

	{uint64  ReplaceReturn = result.l; SensorCall(); return ReplaceReturn;}
}

static enum TIFFReadDirEntryErr TIFFReadDirEntryByte(TIFF* tif, TIFFDirEntry* direntry, uint8* value)
{
	SensorCall();enum TIFFReadDirEntryErr err;
	SensorCall();if (direntry->tdir_count!=1)
		{/*85*/SensorCall();return(TIFFReadDirEntryErrCount);/*86*/}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
			SensorCall();TIFFReadDirEntryCheckedByte(tif,direntry,value);
			SensorCall();return(TIFFReadDirEntryErrOk);
		case TIFF_SBYTE:
			{
				SensorCall();int8 m;
				TIFFReadDirEntryCheckedSbyte(tif,direntry,&m);
				err=TIFFReadDirEntryCheckRangeByteSbyte(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*87*/SensorCall();return(err);/*88*/}
				SensorCall();*value=(uint8)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SHORT:
			{
				SensorCall();uint16 m;
				TIFFReadDirEntryCheckedShort(tif,direntry,&m);
				err=TIFFReadDirEntryCheckRangeByteShort(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*89*/SensorCall();return(err);/*90*/}
				SensorCall();*value=(uint8)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SSHORT:
			{
				SensorCall();int16 m;
				TIFFReadDirEntryCheckedSshort(tif,direntry,&m);
				err=TIFFReadDirEntryCheckRangeByteSshort(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*91*/SensorCall();return(err);/*92*/}
				SensorCall();*value=(uint8)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_LONG:
			{
				SensorCall();uint32 m;
				TIFFReadDirEntryCheckedLong(tif,direntry,&m);
				err=TIFFReadDirEntryCheckRangeByteLong(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*93*/SensorCall();return(err);/*94*/}
				SensorCall();*value=(uint8)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SLONG:
			{
				SensorCall();int32 m;
				TIFFReadDirEntryCheckedSlong(tif,direntry,&m);
				err=TIFFReadDirEntryCheckRangeByteSlong(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*95*/SensorCall();return(err);/*96*/}
				SensorCall();*value=(uint8)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_LONG8:
			{
				SensorCall();uint64 m;
				err=TIFFReadDirEntryCheckedLong8(tif,direntry,&m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*97*/SensorCall();return(err);/*98*/}
				SensorCall();err=TIFFReadDirEntryCheckRangeByteLong8(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*99*/SensorCall();return(err);/*100*/}
				SensorCall();*value=(uint8)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SLONG8:
			{
				SensorCall();int64 m;
				err=TIFFReadDirEntryCheckedSlong8(tif,direntry,&m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*101*/SensorCall();return(err);/*102*/}
				SensorCall();err=TIFFReadDirEntryCheckRangeByteSlong8(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*103*/SensorCall();return(err);/*104*/}
				SensorCall();*value=(uint8)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		default:
			SensorCall();return(TIFFReadDirEntryErrType);
	}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryShort(TIFF* tif, TIFFDirEntry* direntry, uint16* value)
{
	SensorCall();enum TIFFReadDirEntryErr err;
	SensorCall();if (direntry->tdir_count!=1)
		{/*105*/SensorCall();return(TIFFReadDirEntryErrCount);/*106*/}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
			{
				SensorCall();uint8 m;
				TIFFReadDirEntryCheckedByte(tif,direntry,&m);
				*value=(uint16)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SBYTE:
			{
				SensorCall();int8 m;
				TIFFReadDirEntryCheckedSbyte(tif,direntry,&m);
				err=TIFFReadDirEntryCheckRangeShortSbyte(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*107*/SensorCall();return(err);/*108*/}
				SensorCall();*value=(uint16)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SHORT:
			SensorCall();TIFFReadDirEntryCheckedShort(tif,direntry,value);
			SensorCall();return(TIFFReadDirEntryErrOk);
		case TIFF_SSHORT:
			{
				SensorCall();int16 m;
				TIFFReadDirEntryCheckedSshort(tif,direntry,&m);
				err=TIFFReadDirEntryCheckRangeShortSshort(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*109*/SensorCall();return(err);/*110*/}
				SensorCall();*value=(uint16)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_LONG:
			{
				SensorCall();uint32 m;
				TIFFReadDirEntryCheckedLong(tif,direntry,&m);
				err=TIFFReadDirEntryCheckRangeShortLong(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*111*/SensorCall();return(err);/*112*/}
				SensorCall();*value=(uint16)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SLONG:
			{
				SensorCall();int32 m;
				TIFFReadDirEntryCheckedSlong(tif,direntry,&m);
				err=TIFFReadDirEntryCheckRangeShortSlong(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*113*/SensorCall();return(err);/*114*/}
				SensorCall();*value=(uint16)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_LONG8:
			{
				SensorCall();uint64 m;
				err=TIFFReadDirEntryCheckedLong8(tif,direntry,&m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*115*/SensorCall();return(err);/*116*/}
				SensorCall();err=TIFFReadDirEntryCheckRangeShortLong8(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*117*/SensorCall();return(err);/*118*/}
				SensorCall();*value=(uint16)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SLONG8:
			{
				SensorCall();int64 m;
				err=TIFFReadDirEntryCheckedSlong8(tif,direntry,&m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*119*/SensorCall();return(err);/*120*/}
				SensorCall();err=TIFFReadDirEntryCheckRangeShortSlong8(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*121*/SensorCall();return(err);/*122*/}
				SensorCall();*value=(uint16)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		default:
			SensorCall();return(TIFFReadDirEntryErrType);
	}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryLong(TIFF* tif, TIFFDirEntry* direntry, uint32* value)
{
	SensorCall();enum TIFFReadDirEntryErr err;
	SensorCall();if (direntry->tdir_count!=1)
		{/*123*/SensorCall();return(TIFFReadDirEntryErrCount);/*124*/}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
			{
				SensorCall();uint8 m;
				TIFFReadDirEntryCheckedByte(tif,direntry,&m);
				*value=(uint32)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SBYTE:
			{
				SensorCall();int8 m;
				TIFFReadDirEntryCheckedSbyte(tif,direntry,&m);
				err=TIFFReadDirEntryCheckRangeLongSbyte(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*125*/SensorCall();return(err);/*126*/}
				SensorCall();*value=(uint32)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SHORT:
			{
				SensorCall();uint16 m;
				TIFFReadDirEntryCheckedShort(tif,direntry,&m);
				*value=(uint32)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SSHORT:
			{
				SensorCall();int16 m;
				TIFFReadDirEntryCheckedSshort(tif,direntry,&m);
				err=TIFFReadDirEntryCheckRangeLongSshort(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*127*/SensorCall();return(err);/*128*/}
				SensorCall();*value=(uint32)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_LONG:
			SensorCall();TIFFReadDirEntryCheckedLong(tif,direntry,value);
			SensorCall();return(TIFFReadDirEntryErrOk);
		case TIFF_SLONG:
			{
				SensorCall();int32 m;
				TIFFReadDirEntryCheckedSlong(tif,direntry,&m);
				err=TIFFReadDirEntryCheckRangeLongSlong(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*129*/SensorCall();return(err);/*130*/}
				SensorCall();*value=(uint32)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_LONG8:
			{
				SensorCall();uint64 m;
				err=TIFFReadDirEntryCheckedLong8(tif,direntry,&m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*131*/SensorCall();return(err);/*132*/}
				SensorCall();err=TIFFReadDirEntryCheckRangeLongLong8(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*133*/SensorCall();return(err);/*134*/}
				SensorCall();*value=(uint32)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SLONG8:
			{
				SensorCall();int64 m;
				err=TIFFReadDirEntryCheckedSlong8(tif,direntry,&m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*135*/SensorCall();return(err);/*136*/}
				SensorCall();err=TIFFReadDirEntryCheckRangeLongSlong8(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*137*/SensorCall();return(err);/*138*/}
				SensorCall();*value=(uint32)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		default:
			SensorCall();return(TIFFReadDirEntryErrType);
	}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryLong8(TIFF* tif, TIFFDirEntry* direntry, uint64* value)
{
	SensorCall();enum TIFFReadDirEntryErr err;
	SensorCall();if (direntry->tdir_count!=1)
		{/*139*/SensorCall();return(TIFFReadDirEntryErrCount);/*140*/}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
			{
				SensorCall();uint8 m;
				TIFFReadDirEntryCheckedByte(tif,direntry,&m);
				*value=(uint64)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SBYTE:
			{
				SensorCall();int8 m;
				TIFFReadDirEntryCheckedSbyte(tif,direntry,&m);
				err=TIFFReadDirEntryCheckRangeLong8Sbyte(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*141*/SensorCall();return(err);/*142*/}
				SensorCall();*value=(uint64)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SHORT:
			{
				SensorCall();uint16 m;
				TIFFReadDirEntryCheckedShort(tif,direntry,&m);
				*value=(uint64)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SSHORT:
			{
				SensorCall();int16 m;
				TIFFReadDirEntryCheckedSshort(tif,direntry,&m);
				err=TIFFReadDirEntryCheckRangeLong8Sshort(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*143*/SensorCall();return(err);/*144*/}
				SensorCall();*value=(uint64)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_LONG:
			{
				SensorCall();uint32 m;
				TIFFReadDirEntryCheckedLong(tif,direntry,&m);
				*value=(uint64)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SLONG:
			{
				SensorCall();int32 m;
				TIFFReadDirEntryCheckedSlong(tif,direntry,&m);
				err=TIFFReadDirEntryCheckRangeLong8Slong(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*145*/SensorCall();return(err);/*146*/}
				SensorCall();*value=(uint64)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_LONG8:
			SensorCall();err=TIFFReadDirEntryCheckedLong8(tif,direntry,value);
			SensorCall();return(err);
		case TIFF_SLONG8:
			{
				SensorCall();int64 m;
				err=TIFFReadDirEntryCheckedSlong8(tif,direntry,&m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*147*/SensorCall();return(err);/*148*/}
				SensorCall();err=TIFFReadDirEntryCheckRangeLong8Slong8(m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*149*/SensorCall();return(err);/*150*/}
				SensorCall();*value=(uint64)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		default:
			SensorCall();return(TIFFReadDirEntryErrType);
	}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryFloat(TIFF* tif, TIFFDirEntry* direntry, float* value)
{
	SensorCall();enum TIFFReadDirEntryErr err;
	SensorCall();if (direntry->tdir_count!=1)
		{/*151*/SensorCall();return(TIFFReadDirEntryErrCount);/*152*/}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
			{
				SensorCall();uint8 m;
				TIFFReadDirEntryCheckedByte(tif,direntry,&m);
				*value=(float)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SBYTE:
			{
				SensorCall();int8 m;
				TIFFReadDirEntryCheckedSbyte(tif,direntry,&m);
				*value=(float)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SHORT:
			{
				SensorCall();uint16 m;
				TIFFReadDirEntryCheckedShort(tif,direntry,&m);
				*value=(float)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SSHORT:
			{
				SensorCall();int16 m;
				TIFFReadDirEntryCheckedSshort(tif,direntry,&m);
				*value=(float)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_LONG:
			{
				SensorCall();uint32 m;
				TIFFReadDirEntryCheckedLong(tif,direntry,&m);
				*value=(float)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SLONG:
			{
				SensorCall();int32 m;
				TIFFReadDirEntryCheckedSlong(tif,direntry,&m);
				*value=(float)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_LONG8:
			{
				SensorCall();uint64 m;
				err=TIFFReadDirEntryCheckedLong8(tif,direntry,&m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*153*/SensorCall();return(err);/*154*/}
#if defined(__WIN32__) && (_MSC_VER < 1500)
				/*
				 * XXX: MSVC 6.0 does not support conversion
				 * of 64-bit integers into floating point
				 * values.
				 */
				*value = _TIFFUInt64ToFloat(m);
#else
				SensorCall();*value=(float)m;
#endif
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SLONG8:
			{
				SensorCall();int64 m;
				err=TIFFReadDirEntryCheckedSlong8(tif,direntry,&m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*155*/SensorCall();return(err);/*156*/}
				SensorCall();*value=(float)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_RATIONAL:
			{
				SensorCall();double m;
				err=TIFFReadDirEntryCheckedRational(tif,direntry,&m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*157*/SensorCall();return(err);/*158*/}
				SensorCall();*value=(float)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SRATIONAL:
			{
				SensorCall();double m;
				err=TIFFReadDirEntryCheckedSrational(tif,direntry,&m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*159*/SensorCall();return(err);/*160*/}
				SensorCall();*value=(float)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_FLOAT:
			SensorCall();TIFFReadDirEntryCheckedFloat(tif,direntry,value);
			SensorCall();return(TIFFReadDirEntryErrOk);
		case TIFF_DOUBLE:
			{
				SensorCall();double m;
				err=TIFFReadDirEntryCheckedDouble(tif,direntry,&m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*161*/SensorCall();return(err);/*162*/}
				SensorCall();*value=(float)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		default:
			SensorCall();return(TIFFReadDirEntryErrType);
	}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryDouble(TIFF* tif, TIFFDirEntry* direntry, double* value)
{
	SensorCall();enum TIFFReadDirEntryErr err;
	SensorCall();if (direntry->tdir_count!=1)
		{/*163*/SensorCall();return(TIFFReadDirEntryErrCount);/*164*/}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
			{
				SensorCall();uint8 m;
				TIFFReadDirEntryCheckedByte(tif,direntry,&m);
				*value=(double)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SBYTE:
			{
				SensorCall();int8 m;
				TIFFReadDirEntryCheckedSbyte(tif,direntry,&m);
				*value=(double)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SHORT:
			{
				SensorCall();uint16 m;
				TIFFReadDirEntryCheckedShort(tif,direntry,&m);
				*value=(double)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SSHORT:
			{
				SensorCall();int16 m;
				TIFFReadDirEntryCheckedSshort(tif,direntry,&m);
				*value=(double)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_LONG:
			{
				SensorCall();uint32 m;
				TIFFReadDirEntryCheckedLong(tif,direntry,&m);
				*value=(double)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SLONG:
			{
				SensorCall();int32 m;
				TIFFReadDirEntryCheckedSlong(tif,direntry,&m);
				*value=(double)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_LONG8:
			{
				SensorCall();uint64 m;
				err=TIFFReadDirEntryCheckedLong8(tif,direntry,&m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*165*/SensorCall();return(err);/*166*/}
#if defined(__WIN32__) && (_MSC_VER < 1500)
				/*
				 * XXX: MSVC 6.0 does not support conversion
				 * of 64-bit integers into floating point
				 * values.
				 */
				*value = _TIFFUInt64ToDouble(m);
#else
				SensorCall();*value = (double)m;
#endif
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SLONG8:
			{
				SensorCall();int64 m;
				err=TIFFReadDirEntryCheckedSlong8(tif,direntry,&m);
				SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{/*167*/SensorCall();return(err);/*168*/}
				SensorCall();*value=(double)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_RATIONAL:
			SensorCall();err=TIFFReadDirEntryCheckedRational(tif,direntry,value);
			SensorCall();return(err);
		case TIFF_SRATIONAL:
			SensorCall();err=TIFFReadDirEntryCheckedSrational(tif,direntry,value);
			SensorCall();return(err);
		case TIFF_FLOAT:
			{
				SensorCall();float m;
				TIFFReadDirEntryCheckedFloat(tif,direntry,&m);
				*value=(double)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_DOUBLE:
			SensorCall();err=TIFFReadDirEntryCheckedDouble(tif,direntry,value);
			SensorCall();return(err);
		default:
			SensorCall();return(TIFFReadDirEntryErrType);
	}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryIfd8(TIFF* tif, TIFFDirEntry* direntry, uint64* value)
{
	SensorCall();enum TIFFReadDirEntryErr err;
	SensorCall();if (direntry->tdir_count!=1)
		{/*169*/SensorCall();return(TIFFReadDirEntryErrCount);/*170*/}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_LONG:
		case TIFF_IFD:
			{
				SensorCall();uint32 m;
				TIFFReadDirEntryCheckedLong(tif,direntry,&m);
				*value=(uint64)m;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_LONG8:
		case TIFF_IFD8:
			SensorCall();err=TIFFReadDirEntryCheckedLong8(tif,direntry,value);
			SensorCall();return(err);
		default:
			SensorCall();return(TIFFReadDirEntryErrType);
	}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryArray(TIFF* tif, TIFFDirEntry* direntry, uint32* count, uint32 desttypesize, void** value)
{
	SensorCall();int typesize;
	uint32 datasize;
	void* data;
	typesize=TIFFDataWidth(direntry->tdir_type);
	SensorCall();if ((direntry->tdir_count==0)||(typesize==0))
	{
		SensorCall();*value=0;
		SensorCall();return(TIFFReadDirEntryErrOk);
	}
        SensorCall();(void) desttypesize;

        /* 
         * As a sanity check, make sure we have no more than a 2GB tag array 
         * in either the current data type or the dest data type.  This also
         * avoids problems with overflow of tmsize_t on 32bit systems.
         */
	SensorCall();if ((uint64)(2147483647/typesize)<direntry->tdir_count)
		{/*171*/SensorCall();return(TIFFReadDirEntryErrSizesan);/*172*/}
	SensorCall();if ((uint64)(2147483647/desttypesize)<direntry->tdir_count)
		{/*173*/SensorCall();return(TIFFReadDirEntryErrSizesan);/*174*/}

	SensorCall();*count=(uint32)direntry->tdir_count;
	datasize=(*count)*typesize;
	assert((tmsize_t)datasize>0);
	data=_TIFFCheckMalloc(tif, *count, typesize, "ReadDirEntryArray");
	SensorCall();if (data==0)
		{/*175*/SensorCall();return(TIFFReadDirEntryErrAlloc);/*176*/}
	SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
	{
		SensorCall();if (datasize<=4)
			{/*177*/SensorCall();_TIFFmemcpy(data,&direntry->tdir_offset,datasize);/*178*/}
		else
		{
			SensorCall();enum TIFFReadDirEntryErr err;
			uint32 offset = direntry->tdir_offset.toff_long;
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*179*/SensorCall();TIFFSwabLong(&offset);/*180*/}
			SensorCall();err=TIFFReadDirEntryData(tif,(uint64)offset,(tmsize_t)datasize,data);
			SensorCall();if (err!=TIFFReadDirEntryErrOk)
			{
				SensorCall();_TIFFfree(data);
				SensorCall();return(err);
			}
		}
	}
	else
	{
		SensorCall();if (datasize<=8)
			{/*181*/SensorCall();_TIFFmemcpy(data,&direntry->tdir_offset,datasize);/*182*/}
		else
		{
			SensorCall();enum TIFFReadDirEntryErr err;
			uint64 offset = direntry->tdir_offset.toff_long8;
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*183*/SensorCall();TIFFSwabLong8(&offset);/*184*/}
			SensorCall();err=TIFFReadDirEntryData(tif,offset,(tmsize_t)datasize,data);
			SensorCall();if (err!=TIFFReadDirEntryErrOk)
			{
				SensorCall();_TIFFfree(data);
				SensorCall();return(err);
			}
		}
	}
	SensorCall();*value=data;
	SensorCall();return(TIFFReadDirEntryErrOk);
}

static enum TIFFReadDirEntryErr TIFFReadDirEntryByteArray(TIFF* tif, TIFFDirEntry* direntry, uint8** value)
{
	SensorCall();enum TIFFReadDirEntryErr err;
	uint32 count;
	void* origdata;
	uint8* data;
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_ASCII:
		case TIFF_UNDEFINED:
		case TIFF_BYTE:
		case TIFF_SBYTE:
		case TIFF_SHORT:
		case TIFF_SSHORT:
		case TIFF_LONG:
		case TIFF_SLONG:
		case TIFF_LONG8:
		case TIFF_SLONG8:
			SensorCall();break;
		default:
			SensorCall();return(TIFFReadDirEntryErrType);
	}
	SensorCall();err=TIFFReadDirEntryArray(tif,direntry,&count,1,&origdata);
	SensorCall();if ((err!=TIFFReadDirEntryErrOk)||(origdata==0))
	{
		SensorCall();*value=0;
		SensorCall();return(err);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_ASCII:
		case TIFF_UNDEFINED:
		case TIFF_BYTE:
			SensorCall();*value=(uint8*)origdata;
			SensorCall();return(TIFFReadDirEntryErrOk);
		case TIFF_SBYTE:
			{
				SensorCall();int8* m;
				uint32 n;
				m=(int8*)origdata;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();err=TIFFReadDirEntryCheckRangeByteSbyte(*m);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{
						SensorCall();_TIFFfree(origdata);
						SensorCall();return(err);
					}
					SensorCall();m++;
				}
				SensorCall();*value=(uint8*)origdata;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
	}
	SensorCall();data=(uint8*)_TIFFmalloc(count);
	SensorCall();if (data==0)
	{
		SensorCall();_TIFFfree(origdata);
		SensorCall();return(TIFFReadDirEntryErrAlloc);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_SHORT:
			{
				SensorCall();uint16* ma;
				uint8* mb;
				uint32 n;
				ma=(uint16*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*185*/SensorCall();TIFFSwabShort(ma);/*186*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeByteShort(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*187*/SensorCall();break;/*188*/}
					SensorCall();*mb++=(uint8)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SSHORT:
			{
				SensorCall();int16* ma;
				uint8* mb;
				uint32 n;
				ma=(int16*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*189*/SensorCall();TIFFSwabShort((uint16*)ma);/*190*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeByteSshort(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*191*/SensorCall();break;/*192*/}
					SensorCall();*mb++=(uint8)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_LONG:
			{
				SensorCall();uint32* ma;
				uint8* mb;
				uint32 n;
				ma=(uint32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*193*/SensorCall();TIFFSwabLong(ma);/*194*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeByteLong(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*195*/SensorCall();break;/*196*/}
					SensorCall();*mb++=(uint8)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SLONG:
			{
				SensorCall();int32* ma;
				uint8* mb;
				uint32 n;
				ma=(int32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*197*/SensorCall();TIFFSwabLong((uint32*)ma);/*198*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeByteSlong(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*199*/SensorCall();break;/*200*/}
					SensorCall();*mb++=(uint8)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_LONG8:
			{
				SensorCall();uint64* ma;
				uint8* mb;
				uint32 n;
				ma=(uint64*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*201*/SensorCall();TIFFSwabLong8(ma);/*202*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeByteLong8(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*203*/SensorCall();break;/*204*/}
					SensorCall();*mb++=(uint8)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SLONG8:
			{
				SensorCall();int64* ma;
				uint8* mb;
				uint32 n;
				ma=(int64*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*205*/SensorCall();TIFFSwabLong8((uint64*)ma);/*206*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeByteSlong8(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*207*/SensorCall();break;/*208*/}
					SensorCall();*mb++=(uint8)(*ma++);
				}
			}
			SensorCall();break;
	}
	SensorCall();_TIFFfree(origdata);
	SensorCall();if (err!=TIFFReadDirEntryErrOk)
	{
		SensorCall();_TIFFfree(data);
		SensorCall();return(err);
	}
	SensorCall();*value=data;
	SensorCall();return(TIFFReadDirEntryErrOk);
}

static enum TIFFReadDirEntryErr TIFFReadDirEntrySbyteArray(TIFF* tif, TIFFDirEntry* direntry, int8** value)
{
	SensorCall();enum TIFFReadDirEntryErr err;
	uint32 count;
	void* origdata;
	int8* data;
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_UNDEFINED:
		case TIFF_BYTE:
		case TIFF_SBYTE:
		case TIFF_SHORT:
		case TIFF_SSHORT:
		case TIFF_LONG:
		case TIFF_SLONG:
		case TIFF_LONG8:
		case TIFF_SLONG8:
			SensorCall();break;
		default:
			SensorCall();return(TIFFReadDirEntryErrType);
	}
	SensorCall();err=TIFFReadDirEntryArray(tif,direntry,&count,1,&origdata);
	SensorCall();if ((err!=TIFFReadDirEntryErrOk)||(origdata==0))
	{
		SensorCall();*value=0;
		SensorCall();return(err);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_UNDEFINED:
		case TIFF_BYTE:
			{
				SensorCall();uint8* m;
				uint32 n;
				m=(uint8*)origdata;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();err=TIFFReadDirEntryCheckRangeSbyteByte(*m);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{
						SensorCall();_TIFFfree(origdata);
						SensorCall();return(err);
					}
					SensorCall();m++;
				}
				SensorCall();*value=(int8*)origdata;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SBYTE:
			SensorCall();*value=(int8*)origdata;
			SensorCall();return(TIFFReadDirEntryErrOk);
	}
	SensorCall();data=(int8*)_TIFFmalloc(count);
	SensorCall();if (data==0)
	{
		SensorCall();_TIFFfree(origdata);
		SensorCall();return(TIFFReadDirEntryErrAlloc);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_SHORT:
			{
				SensorCall();uint16* ma;
				int8* mb;
				uint32 n;
				ma=(uint16*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*209*/SensorCall();TIFFSwabShort(ma);/*210*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeSbyteShort(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*211*/SensorCall();break;/*212*/}
					SensorCall();*mb++=(int8)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SSHORT:
			{
				SensorCall();int16* ma;
				int8* mb;
				uint32 n;
				ma=(int16*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*213*/SensorCall();TIFFSwabShort((uint16*)ma);/*214*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeSbyteSshort(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*215*/SensorCall();break;/*216*/}
					SensorCall();*mb++=(int8)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_LONG:
			{
				SensorCall();uint32* ma;
				int8* mb;
				uint32 n;
				ma=(uint32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*217*/SensorCall();TIFFSwabLong(ma);/*218*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeSbyteLong(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*219*/SensorCall();break;/*220*/}
					SensorCall();*mb++=(int8)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SLONG:
			{
				SensorCall();int32* ma;
				int8* mb;
				uint32 n;
				ma=(int32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*221*/SensorCall();TIFFSwabLong((uint32*)ma);/*222*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeSbyteSlong(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*223*/SensorCall();break;/*224*/}
					SensorCall();*mb++=(int8)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_LONG8:
			{
				SensorCall();uint64* ma;
				int8* mb;
				uint32 n;
				ma=(uint64*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*225*/SensorCall();TIFFSwabLong8(ma);/*226*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeSbyteLong8(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*227*/SensorCall();break;/*228*/}
					SensorCall();*mb++=(int8)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SLONG8:
			{
				SensorCall();int64* ma;
				int8* mb;
				uint32 n;
				ma=(int64*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*229*/SensorCall();TIFFSwabLong8((uint64*)ma);/*230*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeSbyteSlong8(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*231*/SensorCall();break;/*232*/}
					SensorCall();*mb++=(int8)(*ma++);
				}
			}
			SensorCall();break;
	}
	SensorCall();_TIFFfree(origdata);
	SensorCall();if (err!=TIFFReadDirEntryErrOk)
	{
		SensorCall();_TIFFfree(data);
		SensorCall();return(err);
	}
	SensorCall();*value=data;
	SensorCall();return(TIFFReadDirEntryErrOk);
}

static enum TIFFReadDirEntryErr TIFFReadDirEntryShortArray(TIFF* tif, TIFFDirEntry* direntry, uint16** value)
{
	SensorCall();enum TIFFReadDirEntryErr err;
	uint32 count;
	void* origdata;
	uint16* data;
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
		case TIFF_SBYTE:
		case TIFF_SHORT:
		case TIFF_SSHORT:
		case TIFF_LONG:
		case TIFF_SLONG:
		case TIFF_LONG8:
		case TIFF_SLONG8:
			SensorCall();break;
		default:
			SensorCall();return(TIFFReadDirEntryErrType);
	}
	SensorCall();err=TIFFReadDirEntryArray(tif,direntry,&count,2,&origdata);
	SensorCall();if ((err!=TIFFReadDirEntryErrOk)||(origdata==0))
	{
		SensorCall();*value=0;
		SensorCall();return(err);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_SHORT:
			SensorCall();*value=(uint16*)origdata;
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*233*/SensorCall();TIFFSwabArrayOfShort(*value,count);/*234*/}  
			SensorCall();return(TIFFReadDirEntryErrOk);
		case TIFF_SSHORT:
			{
				SensorCall();int16* m;
				uint32 n;
				m=(int16*)origdata;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*235*/SensorCall();TIFFSwabShort((uint16*)m);/*236*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeShortSshort(*m);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{
						SensorCall();_TIFFfree(origdata);
						SensorCall();return(err);
					}
					SensorCall();m++;
				}
				SensorCall();*value=(uint16*)origdata;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
	}
	SensorCall();data=(uint16*)_TIFFmalloc(count*2);
	SensorCall();if (data==0)
	{
		SensorCall();_TIFFfree(origdata);
		SensorCall();return(TIFFReadDirEntryErrAlloc);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
			{
				SensorCall();uint8* ma;
				uint16* mb;
				uint32 n;
				ma=(uint8*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
					{/*237*/SensorCall();*mb++=(uint16)(*ma++);/*238*/}
			}
			SensorCall();break;
		case TIFF_SBYTE:
			{
				SensorCall();int8* ma;
				uint16* mb;
				uint32 n;
				ma=(int8*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();err=TIFFReadDirEntryCheckRangeShortSbyte(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*239*/SensorCall();break;/*240*/}
					SensorCall();*mb++=(uint16)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_LONG:
			{
				SensorCall();uint32* ma;
				uint16* mb;
				uint32 n;
				ma=(uint32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*241*/SensorCall();TIFFSwabLong(ma);/*242*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeShortLong(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*243*/SensorCall();break;/*244*/}
					SensorCall();*mb++=(uint16)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SLONG:
			{
				SensorCall();int32* ma;
				uint16* mb;
				uint32 n;
				ma=(int32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*245*/SensorCall();TIFFSwabLong((uint32*)ma);/*246*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeShortSlong(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*247*/SensorCall();break;/*248*/}
					SensorCall();*mb++=(uint16)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_LONG8:
			{
				SensorCall();uint64* ma;
				uint16* mb;
				uint32 n;
				ma=(uint64*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*249*/SensorCall();TIFFSwabLong8(ma);/*250*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeShortLong8(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*251*/SensorCall();break;/*252*/}
					SensorCall();*mb++=(uint16)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SLONG8:
			{
				SensorCall();int64* ma;
				uint16* mb;
				uint32 n;
				ma=(int64*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*253*/SensorCall();TIFFSwabLong8((uint64*)ma);/*254*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeShortSlong8(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*255*/SensorCall();break;/*256*/}
					SensorCall();*mb++=(uint16)(*ma++);
				}
			}
			SensorCall();break;
	}
	SensorCall();_TIFFfree(origdata);
	SensorCall();if (err!=TIFFReadDirEntryErrOk)
	{
		SensorCall();_TIFFfree(data);
		SensorCall();return(err);
	}
	SensorCall();*value=data;
	SensorCall();return(TIFFReadDirEntryErrOk);
}

static enum TIFFReadDirEntryErr TIFFReadDirEntrySshortArray(TIFF* tif, TIFFDirEntry* direntry, int16** value)
{
	SensorCall();enum TIFFReadDirEntryErr err;
	uint32 count;
	void* origdata;
	int16* data;
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
		case TIFF_SBYTE:
		case TIFF_SHORT:
		case TIFF_SSHORT:
		case TIFF_LONG:
		case TIFF_SLONG:
		case TIFF_LONG8:
		case TIFF_SLONG8:
			SensorCall();break;
		default:
			SensorCall();return(TIFFReadDirEntryErrType);
	}
	SensorCall();err=TIFFReadDirEntryArray(tif,direntry,&count,2,&origdata);
	SensorCall();if ((err!=TIFFReadDirEntryErrOk)||(origdata==0))
	{
		SensorCall();*value=0;
		SensorCall();return(err);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_SHORT:
			{
				SensorCall();uint16* m;
				uint32 n;
				m=(uint16*)origdata;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*257*/SensorCall();TIFFSwabShort(m);/*258*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeSshortShort(*m);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{
						SensorCall();_TIFFfree(origdata);
						SensorCall();return(err);
					}
					SensorCall();m++;
				}
				SensorCall();*value=(int16*)origdata;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SSHORT:
			SensorCall();*value=(int16*)origdata;
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*259*/SensorCall();TIFFSwabArrayOfShort((uint16*)(*value),count);/*260*/}
			SensorCall();return(TIFFReadDirEntryErrOk);
	}
	SensorCall();data=(int16*)_TIFFmalloc(count*2);
	SensorCall();if (data==0)
	{
		SensorCall();_TIFFfree(origdata);
		SensorCall();return(TIFFReadDirEntryErrAlloc);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
			{
				SensorCall();uint8* ma;
				int16* mb;
				uint32 n;
				ma=(uint8*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
					{/*261*/SensorCall();*mb++=(int16)(*ma++);/*262*/}
			}
			SensorCall();break;
		case TIFF_SBYTE:
			{
				SensorCall();int8* ma;
				int16* mb;
				uint32 n;
				ma=(int8*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
					{/*263*/SensorCall();*mb++=(int16)(*ma++);/*264*/}
			}
			SensorCall();break;
		case TIFF_LONG:
			{
				SensorCall();uint32* ma;
				int16* mb;
				uint32 n;
				ma=(uint32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*265*/SensorCall();TIFFSwabLong(ma);/*266*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeSshortLong(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*267*/SensorCall();break;/*268*/}
					SensorCall();*mb++=(int16)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SLONG:
			{
				SensorCall();int32* ma;
				int16* mb;
				uint32 n;
				ma=(int32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*269*/SensorCall();TIFFSwabLong((uint32*)ma);/*270*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeSshortSlong(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*271*/SensorCall();break;/*272*/}
					SensorCall();*mb++=(int16)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_LONG8:
			{
				SensorCall();uint64* ma;
				int16* mb;
				uint32 n;
				ma=(uint64*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*273*/SensorCall();TIFFSwabLong8(ma);/*274*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeSshortLong8(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*275*/SensorCall();break;/*276*/}
					SensorCall();*mb++=(int16)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SLONG8:
			{
				SensorCall();int64* ma;
				int16* mb;
				uint32 n;
				ma=(int64*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*277*/SensorCall();TIFFSwabLong8((uint64*)ma);/*278*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeSshortSlong8(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*279*/SensorCall();break;/*280*/}
					SensorCall();*mb++=(int16)(*ma++);
				}
			}
			SensorCall();break;
	}
	SensorCall();_TIFFfree(origdata);
	SensorCall();if (err!=TIFFReadDirEntryErrOk)
	{
		SensorCall();_TIFFfree(data);
		SensorCall();return(err);
	}
	SensorCall();*value=data;
	SensorCall();return(TIFFReadDirEntryErrOk);
}

static enum TIFFReadDirEntryErr TIFFReadDirEntryLongArray(TIFF* tif, TIFFDirEntry* direntry, uint32** value)
{
	SensorCall();enum TIFFReadDirEntryErr err;
	uint32 count;
	void* origdata;
	uint32* data;
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
		case TIFF_SBYTE:
		case TIFF_SHORT:
		case TIFF_SSHORT:
		case TIFF_LONG:
		case TIFF_SLONG:
		case TIFF_LONG8:
		case TIFF_SLONG8:
			SensorCall();break;
		default:
			SensorCall();return(TIFFReadDirEntryErrType);
	}
	SensorCall();err=TIFFReadDirEntryArray(tif,direntry,&count,4,&origdata);
	SensorCall();if ((err!=TIFFReadDirEntryErrOk)||(origdata==0))
	{
		SensorCall();*value=0;
		SensorCall();return(err);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_LONG:
			SensorCall();*value=(uint32*)origdata;
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*281*/SensorCall();TIFFSwabArrayOfLong(*value,count);/*282*/}
			SensorCall();return(TIFFReadDirEntryErrOk);
		case TIFF_SLONG:
			{
				SensorCall();int32* m;
				uint32 n;
				m=(int32*)origdata;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*283*/SensorCall();TIFFSwabLong((uint32*)m);/*284*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeLongSlong(*m);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{
						SensorCall();_TIFFfree(origdata);
						SensorCall();return(err);
					}
					SensorCall();m++;
				}
				SensorCall();*value=(uint32*)origdata;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
	}
	SensorCall();data=(uint32*)_TIFFmalloc(count*4);
	SensorCall();if (data==0)
	{
		SensorCall();_TIFFfree(origdata);
		SensorCall();return(TIFFReadDirEntryErrAlloc);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
			{
				SensorCall();uint8* ma;
				uint32* mb;
				uint32 n;
				ma=(uint8*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
					{/*285*/SensorCall();*mb++=(uint32)(*ma++);/*286*/}
			}
			SensorCall();break;
		case TIFF_SBYTE:
			{
				SensorCall();int8* ma;
				uint32* mb;
				uint32 n;
				ma=(int8*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();err=TIFFReadDirEntryCheckRangeLongSbyte(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*287*/SensorCall();break;/*288*/}
					SensorCall();*mb++=(uint32)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SHORT:
			{
				SensorCall();uint16* ma;
				uint32* mb;
				uint32 n;
				ma=(uint16*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*289*/SensorCall();TIFFSwabShort(ma);/*290*/}
					SensorCall();*mb++=(uint32)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SSHORT:
			{
				SensorCall();int16* ma;
				uint32* mb;
				uint32 n;
				ma=(int16*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*291*/SensorCall();TIFFSwabShort((uint16*)ma);/*292*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeLongSshort(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*293*/SensorCall();break;/*294*/}
					SensorCall();*mb++=(uint32)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_LONG8:
			{
				SensorCall();uint64* ma;
				uint32* mb;
				uint32 n;
				ma=(uint64*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*295*/SensorCall();TIFFSwabLong8(ma);/*296*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeLongLong8(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*297*/SensorCall();break;/*298*/}
					SensorCall();*mb++=(uint32)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SLONG8:
			{
				SensorCall();int64* ma;
				uint32* mb;
				uint32 n;
				ma=(int64*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*299*/SensorCall();TIFFSwabLong8((uint64*)ma);/*300*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeLongSlong8(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*301*/SensorCall();break;/*302*/}
					SensorCall();*mb++=(uint32)(*ma++);
				}
			}
			SensorCall();break;
	}
	SensorCall();_TIFFfree(origdata);
	SensorCall();if (err!=TIFFReadDirEntryErrOk)
	{
		SensorCall();_TIFFfree(data);
		SensorCall();return(err);
	}
	SensorCall();*value=data;
	SensorCall();return(TIFFReadDirEntryErrOk);
}

static enum TIFFReadDirEntryErr TIFFReadDirEntrySlongArray(TIFF* tif, TIFFDirEntry* direntry, int32** value)
{
	SensorCall();enum TIFFReadDirEntryErr err;
	uint32 count;
	void* origdata;
	int32* data;
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
		case TIFF_SBYTE:
		case TIFF_SHORT:
		case TIFF_SSHORT:
		case TIFF_LONG:
		case TIFF_SLONG:
		case TIFF_LONG8:
		case TIFF_SLONG8:
			SensorCall();break;
		default:
			SensorCall();return(TIFFReadDirEntryErrType);
	}
	SensorCall();err=TIFFReadDirEntryArray(tif,direntry,&count,4,&origdata);
	SensorCall();if ((err!=TIFFReadDirEntryErrOk)||(origdata==0))
	{
		SensorCall();*value=0;
		SensorCall();return(err);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_LONG:
			{
				SensorCall();uint32* m;
				uint32 n;
				m=(uint32*)origdata;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*303*/SensorCall();TIFFSwabLong((uint32*)m);/*304*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeSlongLong(*m);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{
						SensorCall();_TIFFfree(origdata);
						SensorCall();return(err);
					}
					SensorCall();m++;
				}
				SensorCall();*value=(int32*)origdata;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SLONG:
			SensorCall();*value=(int32*)origdata;
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*305*/SensorCall();TIFFSwabArrayOfLong((uint32*)(*value),count);/*306*/}
			SensorCall();return(TIFFReadDirEntryErrOk);
	}
	SensorCall();data=(int32*)_TIFFmalloc(count*4);
	SensorCall();if (data==0)
	{
		SensorCall();_TIFFfree(origdata);
		SensorCall();return(TIFFReadDirEntryErrAlloc);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
			{
				SensorCall();uint8* ma;
				int32* mb;
				uint32 n;
				ma=(uint8*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
					{/*307*/SensorCall();*mb++=(int32)(*ma++);/*308*/}
			}
			SensorCall();break;
		case TIFF_SBYTE:
			{
				SensorCall();int8* ma;
				int32* mb;
				uint32 n;
				ma=(int8*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
					{/*309*/SensorCall();*mb++=(int32)(*ma++);/*310*/}
			}
			SensorCall();break;
		case TIFF_SHORT:
			{
				SensorCall();uint16* ma;
				int32* mb;
				uint32 n;
				ma=(uint16*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*311*/SensorCall();TIFFSwabShort(ma);/*312*/}
					SensorCall();*mb++=(int32)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SSHORT:
			{
				SensorCall();int16* ma;
				int32* mb;
				uint32 n;
				ma=(int16*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*313*/SensorCall();TIFFSwabShort((uint16*)ma);/*314*/}
					SensorCall();*mb++=(int32)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_LONG8:
			{
				SensorCall();uint64* ma;
				int32* mb;
				uint32 n;
				ma=(uint64*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*315*/SensorCall();TIFFSwabLong8(ma);/*316*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeSlongLong8(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*317*/SensorCall();break;/*318*/}
					SensorCall();*mb++=(int32)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SLONG8:
			{
				SensorCall();int64* ma;
				int32* mb;
				uint32 n;
				ma=(int64*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*319*/SensorCall();TIFFSwabLong8((uint64*)ma);/*320*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeSlongSlong8(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*321*/SensorCall();break;/*322*/}
					SensorCall();*mb++=(int32)(*ma++);
				}
			}
			SensorCall();break;
	}
	SensorCall();_TIFFfree(origdata);
	SensorCall();if (err!=TIFFReadDirEntryErrOk)
	{
		SensorCall();_TIFFfree(data);
		SensorCall();return(err);
	}
	SensorCall();*value=data;
	SensorCall();return(TIFFReadDirEntryErrOk);
}

static enum TIFFReadDirEntryErr TIFFReadDirEntryLong8Array(TIFF* tif, TIFFDirEntry* direntry, uint64** value)
{
	SensorCall();enum TIFFReadDirEntryErr err;
	uint32 count;
	void* origdata;
	uint64* data;
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
		case TIFF_SBYTE:
		case TIFF_SHORT:
		case TIFF_SSHORT:
		case TIFF_LONG:
		case TIFF_SLONG:
		case TIFF_LONG8:
		case TIFF_SLONG8:
			SensorCall();break;
		default:
			SensorCall();return(TIFFReadDirEntryErrType);
	}
	SensorCall();err=TIFFReadDirEntryArray(tif,direntry,&count,8,&origdata);
	SensorCall();if ((err!=TIFFReadDirEntryErrOk)||(origdata==0))
	{
		SensorCall();*value=0;
		SensorCall();return(err);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_LONG8:
			SensorCall();*value=(uint64*)origdata;
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*323*/SensorCall();TIFFSwabArrayOfLong8(*value,count);/*324*/}
			SensorCall();return(TIFFReadDirEntryErrOk);
		case TIFF_SLONG8:
			{
				SensorCall();int64* m;
				uint32 n;
				m=(int64*)origdata;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*325*/SensorCall();TIFFSwabLong8((uint64*)m);/*326*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeLong8Slong8(*m);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{
						SensorCall();_TIFFfree(origdata);
						SensorCall();return(err);
					}
					SensorCall();m++;
				}
				SensorCall();*value=(uint64*)origdata;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
	}
	SensorCall();data=(uint64*)_TIFFmalloc(count*8);
	SensorCall();if (data==0)
	{
		SensorCall();_TIFFfree(origdata);
		SensorCall();return(TIFFReadDirEntryErrAlloc);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
			{
				SensorCall();uint8* ma;
				uint64* mb;
				uint32 n;
				ma=(uint8*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
					{/*327*/SensorCall();*mb++=(uint64)(*ma++);/*328*/}
			}
			SensorCall();break;
		case TIFF_SBYTE:
			{
				SensorCall();int8* ma;
				uint64* mb;
				uint32 n;
				ma=(int8*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();err=TIFFReadDirEntryCheckRangeLong8Sbyte(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*329*/SensorCall();break;/*330*/}
					SensorCall();*mb++=(uint64)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SHORT:
			{
				SensorCall();uint16* ma;
				uint64* mb;
				uint32 n;
				ma=(uint16*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*331*/SensorCall();TIFFSwabShort(ma);/*332*/}
					SensorCall();*mb++=(uint64)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SSHORT:
			{
				SensorCall();int16* ma;
				uint64* mb;
				uint32 n;
				ma=(int16*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*333*/SensorCall();TIFFSwabShort((uint16*)ma);/*334*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeLong8Sshort(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*335*/SensorCall();break;/*336*/}
					SensorCall();*mb++=(uint64)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_LONG:
			{
				SensorCall();uint32* ma;
				uint64* mb;
				uint32 n;
				ma=(uint32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*337*/SensorCall();TIFFSwabLong(ma);/*338*/}
					SensorCall();*mb++=(uint64)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SLONG:
			{
				SensorCall();int32* ma;
				uint64* mb;
				uint32 n;
				ma=(int32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*339*/SensorCall();TIFFSwabLong((uint32*)ma);/*340*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeLong8Slong(*ma);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
						{/*341*/SensorCall();break;/*342*/}
					SensorCall();*mb++=(uint64)(*ma++);
				}
			}
			SensorCall();break;
	}
	SensorCall();_TIFFfree(origdata);
	SensorCall();if (err!=TIFFReadDirEntryErrOk)
	{
		SensorCall();_TIFFfree(data);
		SensorCall();return(err);
	}
	SensorCall();*value=data;
	SensorCall();return(TIFFReadDirEntryErrOk);
}

static enum TIFFReadDirEntryErr TIFFReadDirEntrySlong8Array(TIFF* tif, TIFFDirEntry* direntry, int64** value)
{
	SensorCall();enum TIFFReadDirEntryErr err;
	uint32 count;
	void* origdata;
	int64* data;
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
		case TIFF_SBYTE:
		case TIFF_SHORT:
		case TIFF_SSHORT:
		case TIFF_LONG:
		case TIFF_SLONG:
		case TIFF_LONG8:
		case TIFF_SLONG8:
			SensorCall();break;
		default:
			SensorCall();return(TIFFReadDirEntryErrType);
	}
	SensorCall();err=TIFFReadDirEntryArray(tif,direntry,&count,8,&origdata);
	SensorCall();if ((err!=TIFFReadDirEntryErrOk)||(origdata==0))
	{
		SensorCall();*value=0;
		SensorCall();return(err);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_LONG8:
			{
				SensorCall();uint64* m;
				uint32 n;
				m=(uint64*)origdata;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*343*/SensorCall();TIFFSwabLong8(m);/*344*/}
					SensorCall();err=TIFFReadDirEntryCheckRangeSlong8Long8(*m);
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{
						SensorCall();_TIFFfree(origdata);
						SensorCall();return(err);
					}
					SensorCall();m++;
				}
				SensorCall();*value=(int64*)origdata;
				SensorCall();return(TIFFReadDirEntryErrOk);
			}
		case TIFF_SLONG8:
			SensorCall();*value=(int64*)origdata;
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*345*/SensorCall();TIFFSwabArrayOfLong8((uint64*)(*value),count);/*346*/}
			SensorCall();return(TIFFReadDirEntryErrOk);
	}
	SensorCall();data=(int64*)_TIFFmalloc(count*8);
	SensorCall();if (data==0)
	{
		SensorCall();_TIFFfree(origdata);
		SensorCall();return(TIFFReadDirEntryErrAlloc);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
			{
				SensorCall();uint8* ma;
				int64* mb;
				uint32 n;
				ma=(uint8*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
					{/*347*/SensorCall();*mb++=(int64)(*ma++);/*348*/}
			}
			SensorCall();break;
		case TIFF_SBYTE:
			{
				SensorCall();int8* ma;
				int64* mb;
				uint32 n;
				ma=(int8*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
					{/*349*/SensorCall();*mb++=(int64)(*ma++);/*350*/}
			}
			SensorCall();break;
		case TIFF_SHORT:
			{
				SensorCall();uint16* ma;
				int64* mb;
				uint32 n;
				ma=(uint16*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*351*/SensorCall();TIFFSwabShort(ma);/*352*/}
					SensorCall();*mb++=(int64)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SSHORT:
			{
				SensorCall();int16* ma;
				int64* mb;
				uint32 n;
				ma=(int16*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*353*/SensorCall();TIFFSwabShort((uint16*)ma);/*354*/}
					SensorCall();*mb++=(int64)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_LONG:
			{
				SensorCall();uint32* ma;
				int64* mb;
				uint32 n;
				ma=(uint32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*355*/SensorCall();TIFFSwabLong(ma);/*356*/}
					SensorCall();*mb++=(int64)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SLONG:
			{
				SensorCall();int32* ma;
				int64* mb;
				uint32 n;
				ma=(int32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*357*/SensorCall();TIFFSwabLong((uint32*)ma);/*358*/}
					SensorCall();*mb++=(int64)(*ma++);
				}
			}
			SensorCall();break;
	}
	SensorCall();_TIFFfree(origdata);
	SensorCall();if (err!=TIFFReadDirEntryErrOk)
	{
		SensorCall();_TIFFfree(data);
		SensorCall();return(err);
	}
	SensorCall();*value=data;
	SensorCall();return(TIFFReadDirEntryErrOk);
}

static enum TIFFReadDirEntryErr TIFFReadDirEntryFloatArray(TIFF* tif, TIFFDirEntry* direntry, float** value)
{
	SensorCall();enum TIFFReadDirEntryErr err;
	uint32 count;
	void* origdata;
	float* data;
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
		case TIFF_SBYTE:
		case TIFF_SHORT:
		case TIFF_SSHORT:
		case TIFF_LONG:
		case TIFF_SLONG:
		case TIFF_LONG8:
		case TIFF_SLONG8:
		case TIFF_RATIONAL:
		case TIFF_SRATIONAL:
		case TIFF_FLOAT:
		case TIFF_DOUBLE:
			SensorCall();break;
		default:
			SensorCall();return(TIFFReadDirEntryErrType);
	}
	SensorCall();err=TIFFReadDirEntryArray(tif,direntry,&count,4,&origdata);
	SensorCall();if ((err!=TIFFReadDirEntryErrOk)||(origdata==0))
	{
		SensorCall();*value=0;
		SensorCall();return(err);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_FLOAT:
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*359*/SensorCall();TIFFSwabArrayOfLong((uint32*)origdata,count);/*360*/}  
			TIFFCvtIEEEDoubleToNative(tif,count,(float*)origdata);
			SensorCall();*value=(float*)origdata;
			SensorCall();return(TIFFReadDirEntryErrOk);
	}
	SensorCall();data=(float*)_TIFFmalloc(count*sizeof(float));
	SensorCall();if (data==0)
	{
		SensorCall();_TIFFfree(origdata);
		SensorCall();return(TIFFReadDirEntryErrAlloc);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
			{
				SensorCall();uint8* ma;
				float* mb;
				uint32 n;
				ma=(uint8*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
					{/*361*/SensorCall();*mb++=(float)(*ma++);/*362*/}
			}
			SensorCall();break;
		case TIFF_SBYTE:
			{
				SensorCall();int8* ma;
				float* mb;
				uint32 n;
				ma=(int8*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
					{/*363*/SensorCall();*mb++=(float)(*ma++);/*364*/}
			}
			SensorCall();break;
		case TIFF_SHORT:
			{
				SensorCall();uint16* ma;
				float* mb;
				uint32 n;
				ma=(uint16*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*365*/SensorCall();TIFFSwabShort(ma);/*366*/}
					SensorCall();*mb++=(float)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SSHORT:
			{
				SensorCall();int16* ma;
				float* mb;
				uint32 n;
				ma=(int16*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*367*/SensorCall();TIFFSwabShort((uint16*)ma);/*368*/}
					SensorCall();*mb++=(float)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_LONG:
			{
				SensorCall();uint32* ma;
				float* mb;
				uint32 n;
				ma=(uint32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*369*/SensorCall();TIFFSwabLong(ma);/*370*/}
					SensorCall();*mb++=(float)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SLONG:
			{
				SensorCall();int32* ma;
				float* mb;
				uint32 n;
				ma=(int32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*371*/SensorCall();TIFFSwabLong((uint32*)ma);/*372*/}
					SensorCall();*mb++=(float)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_LONG8:
			{
				SensorCall();uint64* ma;
				float* mb;
				uint32 n;
				ma=(uint64*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*373*/SensorCall();TIFFSwabLong8(ma);/*374*/}
#if defined(__WIN32__) && (_MSC_VER < 1500)
					/*
					 * XXX: MSVC 6.0 does not support
					 * conversion of 64-bit integers into
					 * floating point values.
					 */
					*mb++ = _TIFFUInt64ToFloat(*ma++);
#else
					SensorCall();*mb++ = (float)(*ma++);
#endif
				}
			}
			SensorCall();break;
		case TIFF_SLONG8:
			{
				SensorCall();int64* ma;
				float* mb;
				uint32 n;
				ma=(int64*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*375*/SensorCall();TIFFSwabLong8((uint64*)ma);/*376*/}
					SensorCall();*mb++=(float)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_RATIONAL:
			{
				SensorCall();uint32* ma;
				uint32 maa;
				uint32 mab;
				float* mb;
				uint32 n;
				ma=(uint32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*377*/SensorCall();TIFFSwabLong(ma);/*378*/}
					SensorCall();maa=*ma++;
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*379*/SensorCall();TIFFSwabLong(ma);/*380*/}
					SensorCall();mab=*ma++;
					SensorCall();if (mab==0)
						{/*381*/SensorCall();*mb++=0.0;/*382*/}
					else
						{/*383*/SensorCall();*mb++=(float)maa/(float)mab;/*384*/}
				}
			}
			SensorCall();break;
		case TIFF_SRATIONAL:
			{
				SensorCall();uint32* ma;
				int32 maa;
				uint32 mab;
				float* mb;
				uint32 n;
				ma=(uint32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*385*/SensorCall();TIFFSwabLong(ma);/*386*/}
					SensorCall();maa=*(int32*)ma;
					ma++;
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*387*/SensorCall();TIFFSwabLong(ma);/*388*/}
					SensorCall();mab=*ma++;
					SensorCall();if (mab==0)
						{/*389*/SensorCall();*mb++=0.0;/*390*/}
					else
						{/*391*/SensorCall();*mb++=(float)maa/(float)mab;/*392*/}
				}
			}
			SensorCall();break;
		case TIFF_DOUBLE:
			{
				SensorCall();double* ma;
				float* mb;
				uint32 n;
				SensorCall();if (tif->tif_flags&TIFF_SWAB)
					{/*393*/SensorCall();TIFFSwabArrayOfLong8((uint64*)origdata,count);/*394*/}
				TIFFCvtIEEEDoubleToNative(tif,count,(double*)origdata);
				SensorCall();ma=(double*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
					{/*395*/SensorCall();*mb++=(float)(*ma++);/*396*/}
			}
			SensorCall();break;
	}
	SensorCall();_TIFFfree(origdata);
	SensorCall();if (err!=TIFFReadDirEntryErrOk)
	{
		SensorCall();_TIFFfree(data);
		SensorCall();return(err);
	}
	SensorCall();*value=data;
	SensorCall();return(TIFFReadDirEntryErrOk);
}

static enum TIFFReadDirEntryErr
TIFFReadDirEntryDoubleArray(TIFF* tif, TIFFDirEntry* direntry, double** value)
{
	SensorCall();enum TIFFReadDirEntryErr err;
	uint32 count;
	void* origdata;
	double* data;
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
		case TIFF_SBYTE:
		case TIFF_SHORT:
		case TIFF_SSHORT:
		case TIFF_LONG:
		case TIFF_SLONG:
		case TIFF_LONG8:
		case TIFF_SLONG8:
		case TIFF_RATIONAL:
		case TIFF_SRATIONAL:
		case TIFF_FLOAT:
		case TIFF_DOUBLE:
			SensorCall();break;
		default:
			SensorCall();return(TIFFReadDirEntryErrType);
	}
	SensorCall();err=TIFFReadDirEntryArray(tif,direntry,&count,8,&origdata);
	SensorCall();if ((err!=TIFFReadDirEntryErrOk)||(origdata==0))
	{
		SensorCall();*value=0;
		SensorCall();return(err);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_DOUBLE:
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*397*/SensorCall();TIFFSwabArrayOfLong8((uint64*)origdata,count);/*398*/}
			TIFFCvtIEEEDoubleToNative(tif,count,(double*)origdata);
			SensorCall();*value=(double*)origdata;
			SensorCall();return(TIFFReadDirEntryErrOk);
	}
	SensorCall();data=(double*)_TIFFmalloc(count*sizeof(double));
	SensorCall();if (data==0)
	{
		SensorCall();_TIFFfree(origdata);
		SensorCall();return(TIFFReadDirEntryErrAlloc);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_BYTE:
			{
				SensorCall();uint8* ma;
				double* mb;
				uint32 n;
				ma=(uint8*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
					{/*399*/SensorCall();*mb++=(double)(*ma++);/*400*/}
			}
			SensorCall();break;
		case TIFF_SBYTE:
			{
				SensorCall();int8* ma;
				double* mb;
				uint32 n;
				ma=(int8*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
					{/*401*/SensorCall();*mb++=(double)(*ma++);/*402*/}
			}
			SensorCall();break;
		case TIFF_SHORT:
			{
				SensorCall();uint16* ma;
				double* mb;
				uint32 n;
				ma=(uint16*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*403*/SensorCall();TIFFSwabShort(ma);/*404*/}
					SensorCall();*mb++=(double)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SSHORT:
			{
				SensorCall();int16* ma;
				double* mb;
				uint32 n;
				ma=(int16*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*405*/SensorCall();TIFFSwabShort((uint16*)ma);/*406*/}
					SensorCall();*mb++=(double)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_LONG:
			{
				SensorCall();uint32* ma;
				double* mb;
				uint32 n;
				ma=(uint32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*407*/SensorCall();TIFFSwabLong(ma);/*408*/}
					SensorCall();*mb++=(double)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_SLONG:
			{
				SensorCall();int32* ma;
				double* mb;
				uint32 n;
				ma=(int32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*409*/SensorCall();TIFFSwabLong((uint32*)ma);/*410*/}
					SensorCall();*mb++=(double)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_LONG8:
			{
				SensorCall();uint64* ma;
				double* mb;
				uint32 n;
				ma=(uint64*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*411*/SensorCall();TIFFSwabLong8(ma);/*412*/}
#if defined(__WIN32__) && (_MSC_VER < 1500)
					/*
					 * XXX: MSVC 6.0 does not support
					 * conversion of 64-bit integers into
					 * floating point values.
					 */
					*mb++ = _TIFFUInt64ToDouble(*ma++);
#else
					SensorCall();*mb++ = (double)(*ma++);
#endif
				}
			}
			SensorCall();break;
		case TIFF_SLONG8:
			{
				SensorCall();int64* ma;
				double* mb;
				uint32 n;
				ma=(int64*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*413*/SensorCall();TIFFSwabLong8((uint64*)ma);/*414*/}
					SensorCall();*mb++=(double)(*ma++);
				}
			}
			SensorCall();break;
		case TIFF_RATIONAL:
			{
				SensorCall();uint32* ma;
				uint32 maa;
				uint32 mab;
				double* mb;
				uint32 n;
				ma=(uint32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*415*/SensorCall();TIFFSwabLong(ma);/*416*/}
					SensorCall();maa=*ma++;
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*417*/SensorCall();TIFFSwabLong(ma);/*418*/}
					SensorCall();mab=*ma++;
					SensorCall();if (mab==0)
						{/*419*/SensorCall();*mb++=0.0;/*420*/}
					else
						{/*421*/SensorCall();*mb++=(double)maa/(double)mab;/*422*/}
				}
			}
			SensorCall();break;
		case TIFF_SRATIONAL:
			{
				SensorCall();uint32* ma;
				int32 maa;
				uint32 mab;
				double* mb;
				uint32 n;
				ma=(uint32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*423*/SensorCall();TIFFSwabLong(ma);/*424*/}
					SensorCall();maa=*(int32*)ma;
					ma++;
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*425*/SensorCall();TIFFSwabLong(ma);/*426*/}
					SensorCall();mab=*ma++;
					SensorCall();if (mab==0)
						{/*427*/SensorCall();*mb++=0.0;/*428*/}
					else
						{/*429*/SensorCall();*mb++=(double)maa/(double)mab;/*430*/}
				}
			}
			SensorCall();break;
		case TIFF_FLOAT:
			{
				SensorCall();float* ma;
				double* mb;
				uint32 n;
				SensorCall();if (tif->tif_flags&TIFF_SWAB)
					{/*431*/SensorCall();TIFFSwabArrayOfLong((uint32*)origdata,count);/*432*/}  
				TIFFCvtIEEEFloatToNative(tif,count,(float*)origdata);
				SensorCall();ma=(float*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
					{/*433*/SensorCall();*mb++=(double)(*ma++);/*434*/}
			}
			SensorCall();break;
	}
	SensorCall();_TIFFfree(origdata);
	SensorCall();if (err!=TIFFReadDirEntryErrOk)
	{
		SensorCall();_TIFFfree(data);
		SensorCall();return(err);
	}
	SensorCall();*value=data;
	SensorCall();return(TIFFReadDirEntryErrOk);
}

static enum TIFFReadDirEntryErr TIFFReadDirEntryIfd8Array(TIFF* tif, TIFFDirEntry* direntry, uint64** value)
{
	SensorCall();enum TIFFReadDirEntryErr err;
	uint32 count;
	void* origdata;
	uint64* data;
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_LONG:
		case TIFF_LONG8:
		case TIFF_IFD:
		case TIFF_IFD8:
			SensorCall();break;
		default:
			SensorCall();return(TIFFReadDirEntryErrType);
	}
	SensorCall();err=TIFFReadDirEntryArray(tif,direntry,&count,8,&origdata);
	SensorCall();if ((err!=TIFFReadDirEntryErrOk)||(origdata==0))
	{
		SensorCall();*value=0;
		SensorCall();return(err);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_LONG8:
		case TIFF_IFD8:
			SensorCall();*value=(uint64*)origdata;
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*435*/SensorCall();TIFFSwabArrayOfLong8(*value,count);/*436*/}
			SensorCall();return(TIFFReadDirEntryErrOk);
	}
	SensorCall();data=(uint64*)_TIFFmalloc(count*8);
	SensorCall();if (data==0)
	{
		SensorCall();_TIFFfree(origdata);
		SensorCall();return(TIFFReadDirEntryErrAlloc);
	}
	SensorCall();switch (direntry->tdir_type)
	{
		case TIFF_LONG:
		case TIFF_IFD:
			{
				SensorCall();uint32* ma;
				uint64* mb;
				uint32 n;
				ma=(uint32*)origdata;
				mb=data;
				SensorCall();for (n=0; n<count; n++)
				{
					SensorCall();if (tif->tif_flags&TIFF_SWAB)
						{/*437*/SensorCall();TIFFSwabLong(ma);/*438*/}
					SensorCall();*mb++=(uint64)(*ma++);
				}
			}
			SensorCall();break;
	}
	SensorCall();_TIFFfree(origdata);
	SensorCall();if (err!=TIFFReadDirEntryErrOk)
	{
		SensorCall();_TIFFfree(data);
		SensorCall();return(err);
	}
	SensorCall();*value=data;
	SensorCall();return(TIFFReadDirEntryErrOk);
}

static enum TIFFReadDirEntryErr TIFFReadDirEntryPersampleShort(TIFF* tif, TIFFDirEntry* direntry, uint16* value)
{
	SensorCall();enum TIFFReadDirEntryErr err;
	uint16* m;
	uint16* na;
	uint16 nb;
	SensorCall();if (direntry->tdir_count<(uint64)tif->tif_dir.td_samplesperpixel)
		{/*439*/SensorCall();return(TIFFReadDirEntryErrCount);/*440*/}
	SensorCall();err=TIFFReadDirEntryShortArray(tif,direntry,&m);
	SensorCall();if (err!=TIFFReadDirEntryErrOk)
		{/*441*/SensorCall();return(err);/*442*/}
	SensorCall();na=m;
	nb=tif->tif_dir.td_samplesperpixel;
	*value=*na++;
	nb--;
	SensorCall();while (nb>0)
	{
		SensorCall();if (*na++!=*value)
		{
			SensorCall();err=TIFFReadDirEntryErrPsdif;
			SensorCall();break;
		}
		SensorCall();nb--;
	}
	SensorCall();_TIFFfree(m);
	SensorCall();return(err);
}

#if 0
static enum TIFFReadDirEntryErr TIFFReadDirEntryPersampleDouble(TIFF* tif, TIFFDirEntry* direntry, double* value)
{
	enum TIFFReadDirEntryErr err;
	double* m;
	double* na;
	uint16 nb;
	if (direntry->tdir_count<(uint64)tif->tif_dir.td_samplesperpixel)
		return(TIFFReadDirEntryErrCount);
	err=TIFFReadDirEntryDoubleArray(tif,direntry,&m);
	if (err!=TIFFReadDirEntryErrOk)
		return(err);
	na=m;
	nb=tif->tif_dir.td_samplesperpixel;
	*value=*na++;
	nb--;
	while (nb>0)
	{
		if (*na++!=*value)
		{
			err=TIFFReadDirEntryErrPsdif;
			break;
		}
		nb--;
	}
	_TIFFfree(m);
	return(err);
}
#endif

static void TIFFReadDirEntryCheckedByte(TIFF* tif, TIFFDirEntry* direntry, uint8* value)
{
	SensorCall();(void) tif;
	*value=*(uint8*)(&direntry->tdir_offset);
SensorCall();}

static void TIFFReadDirEntryCheckedSbyte(TIFF* tif, TIFFDirEntry* direntry, int8* value)
{
	SensorCall();(void) tif;
	*value=*(int8*)(&direntry->tdir_offset);
SensorCall();}

static void TIFFReadDirEntryCheckedShort(TIFF* tif, TIFFDirEntry* direntry, uint16* value)
{
	SensorCall();*value = direntry->tdir_offset.toff_short;
	/* *value=*(uint16*)(&direntry->tdir_offset); */
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*443*/SensorCall();TIFFSwabShort(value);/*444*/}
SensorCall();}

static void TIFFReadDirEntryCheckedSshort(TIFF* tif, TIFFDirEntry* direntry, int16* value)
{
	SensorCall();*value=*(int16*)(&direntry->tdir_offset);
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*445*/SensorCall();TIFFSwabShort((uint16*)value);/*446*/}
SensorCall();}

static void TIFFReadDirEntryCheckedLong(TIFF* tif, TIFFDirEntry* direntry, uint32* value)
{
	SensorCall();*value=*(uint32*)(&direntry->tdir_offset);
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*447*/SensorCall();TIFFSwabLong(value);/*448*/}
SensorCall();}

static void TIFFReadDirEntryCheckedSlong(TIFF* tif, TIFFDirEntry* direntry, int32* value)
{
	SensorCall();*value=*(int32*)(&direntry->tdir_offset);
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*449*/SensorCall();TIFFSwabLong((uint32*)value);/*450*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckedLong8(TIFF* tif, TIFFDirEntry* direntry, uint64* value)
{
	SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
	{
		SensorCall();enum TIFFReadDirEntryErr err;
		uint32 offset = direntry->tdir_offset.toff_long;
		SensorCall();if (tif->tif_flags&TIFF_SWAB)
			{/*451*/SensorCall();TIFFSwabLong(&offset);/*452*/}
		SensorCall();err=TIFFReadDirEntryData(tif,offset,8,value);
		SensorCall();if (err!=TIFFReadDirEntryErrOk)
			{/*453*/SensorCall();return(err);/*454*/}
	}
	else
		{/*455*/SensorCall();*value = direntry->tdir_offset.toff_long8;/*456*/}
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*457*/SensorCall();TIFFSwabLong8(value);/*458*/}
	SensorCall();return(TIFFReadDirEntryErrOk);
}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckedSlong8(TIFF* tif, TIFFDirEntry* direntry, int64* value)
{
	SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
	{
		SensorCall();enum TIFFReadDirEntryErr err;
		uint32 offset = direntry->tdir_offset.toff_long;
		SensorCall();if (tif->tif_flags&TIFF_SWAB)
			{/*459*/SensorCall();TIFFSwabLong(&offset);/*460*/}
		SensorCall();err=TIFFReadDirEntryData(tif,offset,8,value);
		SensorCall();if (err!=TIFFReadDirEntryErrOk)
			{/*461*/SensorCall();return(err);/*462*/}
	}
	else
		{/*463*/SensorCall();*value=*(int64*)(&direntry->tdir_offset);/*464*/}
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*465*/SensorCall();TIFFSwabLong8((uint64*)value);/*466*/}
	SensorCall();return(TIFFReadDirEntryErrOk);
}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckedRational(TIFF* tif, TIFFDirEntry* direntry, double* value)
{
	SensorCall();UInt64Aligned_t m;

	assert(sizeof(double)==8);
	assert(sizeof(uint64)==8);
	assert(sizeof(uint32)==4);
	SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
	{
		SensorCall();enum TIFFReadDirEntryErr err;
		uint32 offset = direntry->tdir_offset.toff_long;
		SensorCall();if (tif->tif_flags&TIFF_SWAB)
			{/*467*/SensorCall();TIFFSwabLong(&offset);/*468*/}
		SensorCall();err=TIFFReadDirEntryData(tif,offset,8,m.i);
		SensorCall();if (err!=TIFFReadDirEntryErrOk)
			{/*469*/SensorCall();return(err);/*470*/}
	}
	else
		{/*471*/SensorCall();m.l = direntry->tdir_offset.toff_long8;/*472*/}
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*473*/SensorCall();TIFFSwabArrayOfLong(m.i,2);/*474*/}
	SensorCall();if (m.i[0]==0)
		{/*475*/SensorCall();*value=0.0;/*476*/}
	else
		{/*477*/SensorCall();*value=(double)m.i[0]/(double)m.i[1];/*478*/}
	SensorCall();return(TIFFReadDirEntryErrOk);
}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckedSrational(TIFF* tif, TIFFDirEntry* direntry, double* value)
{
	SensorCall();UInt64Aligned_t m;
	assert(sizeof(double)==8);
	assert(sizeof(uint64)==8);
	assert(sizeof(int32)==4);
	assert(sizeof(uint32)==4);
	SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
	{
		SensorCall();enum TIFFReadDirEntryErr err;
		uint32 offset = direntry->tdir_offset.toff_long;
		SensorCall();if (tif->tif_flags&TIFF_SWAB)
			{/*479*/SensorCall();TIFFSwabLong(&offset);/*480*/}
		SensorCall();err=TIFFReadDirEntryData(tif,offset,8,m.i);
		SensorCall();if (err!=TIFFReadDirEntryErrOk)
			{/*481*/SensorCall();return(err);/*482*/}
	}
	else
		{/*483*/SensorCall();m.l=direntry->tdir_offset.toff_long8;/*484*/}
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*485*/SensorCall();TIFFSwabArrayOfLong(m.i,2);/*486*/}
	SensorCall();if ((int32)m.i[0]==0)
		{/*487*/SensorCall();*value=0.0;/*488*/}
	else
		{/*489*/SensorCall();*value=(double)((int32)m.i[0])/(double)m.i[1];/*490*/}
	SensorCall();return(TIFFReadDirEntryErrOk);
}

static void TIFFReadDirEntryCheckedFloat(TIFF* tif, TIFFDirEntry* direntry, float* value)
{
         SensorCall();union
	 {
	   float  f;
	   uint32 i;
	 } float_union;
	assert(sizeof(float)==4);
	assert(sizeof(uint32)==4);
	assert(sizeof(float_union)==4);
	float_union.i=*(uint32*)(&direntry->tdir_offset);
	*value=float_union.f;
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*491*/SensorCall();TIFFSwabLong((uint32*)value);/*492*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckedDouble(TIFF* tif, TIFFDirEntry* direntry, double* value)
{
	assert(sizeof(double)==8);
	assert(sizeof(uint64)==8);
	assert(sizeof(UInt64Aligned_t)==8);
	SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
	{
		SensorCall();enum TIFFReadDirEntryErr err;
		uint32 offset = direntry->tdir_offset.toff_long;
		SensorCall();if (tif->tif_flags&TIFF_SWAB)
			{/*493*/SensorCall();TIFFSwabLong(&offset);/*494*/}
		SensorCall();err=TIFFReadDirEntryData(tif,offset,8,value);
		SensorCall();if (err!=TIFFReadDirEntryErrOk)
			{/*495*/SensorCall();return(err);/*496*/}
	}
	else
	{
	       SensorCall();UInt64Aligned_t uint64_union;
	       uint64_union.l=direntry->tdir_offset.toff_long8;
	       *value=uint64_union.d;
	}
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*497*/SensorCall();TIFFSwabLong8((uint64*)value);/*498*/}
	SensorCall();return(TIFFReadDirEntryErrOk);
}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeByteSbyte(int8 value)
{
	SensorCall();if (value<0)
		{/*499*/SensorCall();return(TIFFReadDirEntryErrRange);/*500*/}
	else
		{/*501*/SensorCall();return(TIFFReadDirEntryErrOk);/*502*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeByteShort(uint16 value)
{
	SensorCall();if (value>0xFF)
		{/*503*/SensorCall();return(TIFFReadDirEntryErrRange);/*504*/}
	else
		{/*505*/SensorCall();return(TIFFReadDirEntryErrOk);/*506*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeByteSshort(int16 value)
{
	SensorCall();if ((value<0)||(value>0xFF))
		{/*507*/SensorCall();return(TIFFReadDirEntryErrRange);/*508*/}
	else
		{/*509*/SensorCall();return(TIFFReadDirEntryErrOk);/*510*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeByteLong(uint32 value)
{
	SensorCall();if (value>0xFF)
		{/*511*/SensorCall();return(TIFFReadDirEntryErrRange);/*512*/}
	else
		{/*513*/SensorCall();return(TIFFReadDirEntryErrOk);/*514*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeByteSlong(int32 value)
{
	SensorCall();if ((value<0)||(value>0xFF))
		{/*515*/SensorCall();return(TIFFReadDirEntryErrRange);/*516*/}
	else
		{/*517*/SensorCall();return(TIFFReadDirEntryErrOk);/*518*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeByteLong8(uint64 value)
{
	SensorCall();if (value>0xFF)
		{/*519*/SensorCall();return(TIFFReadDirEntryErrRange);/*520*/}
	else
		{/*521*/SensorCall();return(TIFFReadDirEntryErrOk);/*522*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeByteSlong8(int64 value)
{
	SensorCall();if ((value<0)||(value>0xFF))
		{/*523*/SensorCall();return(TIFFReadDirEntryErrRange);/*524*/}
	else
		{/*525*/SensorCall();return(TIFFReadDirEntryErrOk);/*526*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSbyteByte(uint8 value)
{
	SensorCall();if (value>0x7F)
		{/*527*/SensorCall();return(TIFFReadDirEntryErrRange);/*528*/}
	else
		{/*529*/SensorCall();return(TIFFReadDirEntryErrOk);/*530*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSbyteShort(uint16 value)
{
	SensorCall();if (value>0x7F)
		{/*531*/SensorCall();return(TIFFReadDirEntryErrRange);/*532*/}
	else
		{/*533*/SensorCall();return(TIFFReadDirEntryErrOk);/*534*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSbyteSshort(int16 value)
{
	SensorCall();if ((value<-0x80)||(value>0x7F))
		{/*535*/SensorCall();return(TIFFReadDirEntryErrRange);/*536*/}
	else
		{/*537*/SensorCall();return(TIFFReadDirEntryErrOk);/*538*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSbyteLong(uint32 value)
{
	SensorCall();if (value>0x7F)
		{/*539*/SensorCall();return(TIFFReadDirEntryErrRange);/*540*/}
	else
		{/*541*/SensorCall();return(TIFFReadDirEntryErrOk);/*542*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSbyteSlong(int32 value)
{
	SensorCall();if ((value<-0x80)||(value>0x7F))
		{/*543*/SensorCall();return(TIFFReadDirEntryErrRange);/*544*/}
	else
		{/*545*/SensorCall();return(TIFFReadDirEntryErrOk);/*546*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSbyteLong8(uint64 value)
{
	SensorCall();if (value>0x7F)
		{/*547*/SensorCall();return(TIFFReadDirEntryErrRange);/*548*/}
	else
		{/*549*/SensorCall();return(TIFFReadDirEntryErrOk);/*550*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSbyteSlong8(int64 value)
{
	SensorCall();if ((value<-0x80)||(value>0x7F))
		{/*551*/SensorCall();return(TIFFReadDirEntryErrRange);/*552*/}
	else
		{/*553*/SensorCall();return(TIFFReadDirEntryErrOk);/*554*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeShortSbyte(int8 value)
{
	SensorCall();if (value<0)
		{/*555*/SensorCall();return(TIFFReadDirEntryErrRange);/*556*/}
	else
		{/*557*/SensorCall();return(TIFFReadDirEntryErrOk);/*558*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeShortSshort(int16 value)
{
	SensorCall();if (value<0)
		{/*559*/SensorCall();return(TIFFReadDirEntryErrRange);/*560*/}
	else
		{/*561*/SensorCall();return(TIFFReadDirEntryErrOk);/*562*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeShortLong(uint32 value)
{
	SensorCall();if (value>0xFFFF)
		{/*563*/SensorCall();return(TIFFReadDirEntryErrRange);/*564*/}
	else
		{/*565*/SensorCall();return(TIFFReadDirEntryErrOk);/*566*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeShortSlong(int32 value)
{
	SensorCall();if ((value<0)||(value>0xFFFF))
		{/*567*/SensorCall();return(TIFFReadDirEntryErrRange);/*568*/}
	else
		{/*569*/SensorCall();return(TIFFReadDirEntryErrOk);/*570*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeShortLong8(uint64 value)
{
	SensorCall();if (value>0xFFFF)
		{/*571*/SensorCall();return(TIFFReadDirEntryErrRange);/*572*/}
	else
		{/*573*/SensorCall();return(TIFFReadDirEntryErrOk);/*574*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeShortSlong8(int64 value)
{
	SensorCall();if ((value<0)||(value>0xFFFF))
		{/*575*/SensorCall();return(TIFFReadDirEntryErrRange);/*576*/}
	else
		{/*577*/SensorCall();return(TIFFReadDirEntryErrOk);/*578*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSshortShort(uint16 value)
{
	SensorCall();if (value>0x7FFF)
		{/*579*/SensorCall();return(TIFFReadDirEntryErrRange);/*580*/}
	else
		{/*581*/SensorCall();return(TIFFReadDirEntryErrOk);/*582*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSshortLong(uint32 value)
{
	SensorCall();if (value>0x7FFF)
		{/*583*/SensorCall();return(TIFFReadDirEntryErrRange);/*584*/}
	else
		{/*585*/SensorCall();return(TIFFReadDirEntryErrOk);/*586*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSshortSlong(int32 value)
{
	SensorCall();if ((value<-0x8000)||(value>0x7FFF))
		{/*587*/SensorCall();return(TIFFReadDirEntryErrRange);/*588*/}
	else
		{/*589*/SensorCall();return(TIFFReadDirEntryErrOk);/*590*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSshortLong8(uint64 value)
{
	SensorCall();if (value>0x7FFF)
		{/*591*/SensorCall();return(TIFFReadDirEntryErrRange);/*592*/}
	else
		{/*593*/SensorCall();return(TIFFReadDirEntryErrOk);/*594*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeSshortSlong8(int64 value)
{
	SensorCall();if ((value<-0x8000)||(value>0x7FFF))
		{/*595*/SensorCall();return(TIFFReadDirEntryErrRange);/*596*/}
	else
		{/*597*/SensorCall();return(TIFFReadDirEntryErrOk);/*598*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeLongSbyte(int8 value)
{
	SensorCall();if (value<0)
		{/*599*/SensorCall();return(TIFFReadDirEntryErrRange);/*600*/}
	else
		{/*601*/SensorCall();return(TIFFReadDirEntryErrOk);/*602*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeLongSshort(int16 value)
{
	SensorCall();if (value<0)
		{/*603*/SensorCall();return(TIFFReadDirEntryErrRange);/*604*/}
	else
		{/*605*/SensorCall();return(TIFFReadDirEntryErrOk);/*606*/}
SensorCall();}

static enum TIFFReadDirEntryErr TIFFReadDirEntryCheckRangeLongSlong(int32 value)
{
	SensorCall();if (value<0)
		{/*607*/SensorCall();return(TIFFReadDirEntryErrRange);/*608*/}
	else
		{/*609*/SensorCall();return(TIFFReadDirEntryErrOk);/*610*/}
SensorCall();}

/*
 * Largest 32-bit unsigned integer value.
 */
#if defined(__WIN32__) && defined(_MSC_VER)
# define TIFF_UINT32_MAX 0xFFFFFFFFI64
#else
# define TIFF_UINT32_MAX 0xFFFFFFFFLL
#endif

static enum TIFFReadDirEntryErr
TIFFReadDirEntryCheckRangeLongLong8(uint64 value)
{
	SensorCall();if (value > TIFF_UINT32_MAX)
		{/*611*/SensorCall();return(TIFFReadDirEntryErrRange);/*612*/}
	else
		{/*613*/SensorCall();return(TIFFReadDirEntryErrOk);/*614*/}
SensorCall();}

static enum TIFFReadDirEntryErr
TIFFReadDirEntryCheckRangeLongSlong8(int64 value)
{
	SensorCall();if ((value<0) || (value > TIFF_UINT32_MAX))
		{/*615*/SensorCall();return(TIFFReadDirEntryErrRange);/*616*/}
	else
		{/*617*/SensorCall();return(TIFFReadDirEntryErrOk);/*618*/}
SensorCall();}

#undef TIFF_UINT32_MAX

static enum TIFFReadDirEntryErr
TIFFReadDirEntryCheckRangeSlongLong(uint32 value)
{
	SensorCall();if (value > 0x7FFFFFFFUL)
		{/*619*/SensorCall();return(TIFFReadDirEntryErrRange);/*620*/}
	else
		{/*621*/SensorCall();return(TIFFReadDirEntryErrOk);/*622*/}
SensorCall();}

static enum TIFFReadDirEntryErr
TIFFReadDirEntryCheckRangeSlongLong8(uint64 value)
{
	SensorCall();if (value > 0x7FFFFFFFUL)
		{/*623*/SensorCall();return(TIFFReadDirEntryErrRange);/*624*/}
	else
		{/*625*/SensorCall();return(TIFFReadDirEntryErrOk);/*626*/}
SensorCall();}

static enum TIFFReadDirEntryErr
TIFFReadDirEntryCheckRangeSlongSlong8(int64 value)
{
	SensorCall();if ((value < 0L-0x80000000L) || (value > 0x7FFFFFFFL))
		{/*627*/SensorCall();return(TIFFReadDirEntryErrRange);/*628*/}
	else
		{/*629*/SensorCall();return(TIFFReadDirEntryErrOk);/*630*/}
SensorCall();}

static enum TIFFReadDirEntryErr
TIFFReadDirEntryCheckRangeLong8Sbyte(int8 value)
{
	SensorCall();if (value < 0)
		{/*631*/SensorCall();return(TIFFReadDirEntryErrRange);/*632*/}
	else
		{/*633*/SensorCall();return(TIFFReadDirEntryErrOk);/*634*/}
SensorCall();}

static enum TIFFReadDirEntryErr
TIFFReadDirEntryCheckRangeLong8Sshort(int16 value)
{
	SensorCall();if (value < 0)
		{/*635*/SensorCall();return(TIFFReadDirEntryErrRange);/*636*/}
	else
		{/*637*/SensorCall();return(TIFFReadDirEntryErrOk);/*638*/}
SensorCall();}

static enum TIFFReadDirEntryErr
TIFFReadDirEntryCheckRangeLong8Slong(int32 value)
{
	SensorCall();if (value < 0)
		{/*639*/SensorCall();return(TIFFReadDirEntryErrRange);/*640*/}
	else
		{/*641*/SensorCall();return(TIFFReadDirEntryErrOk);/*642*/}
SensorCall();}

static enum TIFFReadDirEntryErr
TIFFReadDirEntryCheckRangeLong8Slong8(int64 value)
{
	SensorCall();if (value < 0)
		{/*643*/SensorCall();return(TIFFReadDirEntryErrRange);/*644*/}
	else
		{/*645*/SensorCall();return(TIFFReadDirEntryErrOk);/*646*/}
SensorCall();}

/*
 * Largest 64-bit signed integer value.
 */
#if defined(__WIN32__) && defined(_MSC_VER)
# define TIFF_INT64_MAX 0x7FFFFFFFFFFFFFFFI64
#else
# define TIFF_INT64_MAX 0x7FFFFFFFFFFFFFFFLL
#endif

static enum TIFFReadDirEntryErr
TIFFReadDirEntryCheckRangeSlong8Long8(uint64 value)
{
	SensorCall();if (value > TIFF_INT64_MAX)
		{/*647*/SensorCall();return(TIFFReadDirEntryErrRange);/*648*/}
	else
		{/*649*/SensorCall();return(TIFFReadDirEntryErrOk);/*650*/}
SensorCall();}

#undef TIFF_INT64_MAX

static enum TIFFReadDirEntryErr
TIFFReadDirEntryData(TIFF* tif, uint64 offset, tmsize_t size, void* dest)
{
SensorCall();	assert(size>0);
	SensorCall();if (!isMapped(tif)) {
		SensorCall();if (!SeekOK(tif,offset))
			{/*651*/SensorCall();return(TIFFReadDirEntryErrIo);/*652*/}
		SensorCall();if (!ReadOK(tif,dest,size))
			{/*653*/SensorCall();return(TIFFReadDirEntryErrIo);/*654*/}
	} else {
		SensorCall();size_t ma,mb;
		ma=(size_t)offset;
		mb=ma+size;
		SensorCall();if (((uint64)ma!=offset)
		    || (mb < ma)
		    || (mb - ma != (size_t) size)
		    || (mb < (size_t)size)
		    || (mb > (size_t)tif->tif_size)
		    )
			{/*655*/SensorCall();return(TIFFReadDirEntryErrIo);/*656*/}
		SensorCall();_TIFFmemcpy(dest,tif->tif_base+ma,size);
	}
	SensorCall();return(TIFFReadDirEntryErrOk);
}

static void TIFFReadDirEntryOutputErr(TIFF* tif, enum TIFFReadDirEntryErr err, const char* module, const char* tagname, int recover)
{
	SensorCall();if (!recover) {
		SensorCall();switch (err) {
			case TIFFReadDirEntryErrCount:
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					     "Incorrect count for \"%s\"",
					     tagname);
				SensorCall();break;
			case TIFFReadDirEntryErrType:
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					     "Incompatible type for \"%s\"",
					     tagname);
				SensorCall();break;
			case TIFFReadDirEntryErrIo:
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					     "IO error during reading of \"%s\"",
					     tagname);
				SensorCall();break;
			case TIFFReadDirEntryErrRange:
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					     "Incorrect value for \"%s\"",
					     tagname);
				SensorCall();break;
			case TIFFReadDirEntryErrPsdif:
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
			"Cannot handle different values per sample for \"%s\"",
					     tagname);
				SensorCall();break;
			case TIFFReadDirEntryErrSizesan:
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				"Sanity check on size of \"%s\" value failed",
					     tagname);
				SensorCall();break;
			case TIFFReadDirEntryErrAlloc:
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					     "Out of memory reading of \"%s\"",
					     tagname);
				SensorCall();break;
			default:
				assert(0);   /* we should never get here */
				SensorCall();break;
		}
	} else {
		SensorCall();switch (err) {
			case TIFFReadDirEntryErrCount:
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				"Incorrect count for \"%s\"; tag ignored",
					     tagname);
				SensorCall();break;
			case TIFFReadDirEntryErrType:
				SensorCall();TIFFWarningExt(tif->tif_clientdata, module,
				"Incompatible type for \"%s\"; tag ignored",
					       tagname);
				SensorCall();break;
			case TIFFReadDirEntryErrIo:
				SensorCall();TIFFWarningExt(tif->tif_clientdata, module,
			"IO error during reading of \"%s\"; tag ignored",
					       tagname);
				SensorCall();break;
			case TIFFReadDirEntryErrRange:
				SensorCall();TIFFWarningExt(tif->tif_clientdata, module,
				"Incorrect value for \"%s\"; tag ignored",
					       tagname);
				SensorCall();break;
			case TIFFReadDirEntryErrPsdif:
				SensorCall();TIFFWarningExt(tif->tif_clientdata, module,
	"Cannot handle different values per sample for \"%s\"; tag ignored",
					       tagname);
				SensorCall();break;
			case TIFFReadDirEntryErrSizesan:
				SensorCall();TIFFWarningExt(tif->tif_clientdata, module,
		"Sanity check on size of \"%s\" value failed; tag ignored",
					       tagname);
				SensorCall();break;
			case TIFFReadDirEntryErrAlloc:
				SensorCall();TIFFWarningExt(tif->tif_clientdata, module,
				"Out of memory reading of \"%s\"; tag ignored",
					       tagname);
				SensorCall();break;
			default:
				assert(0);   /* we should never get here */
				SensorCall();break;
		}
	}
SensorCall();}

/*
 * Read the next TIFF directory from a file and convert it to the internal
 * format. We read directories sequentially.
 */
int
TIFFReadDirectory(TIFF* tif)
{
	SensorCall();static const char module[] = "TIFFReadDirectory";
	TIFFDirEntry* dir;
	uint16 dircount;
	TIFFDirEntry* dp;
	uint16 di;
	const TIFFField* fip;
	uint32 fii=FAILED_FII;
        toff_t nextdiroff;
	tif->tif_diroff=tif->tif_nextdiroff;
	SensorCall();if (!TIFFCheckDirOffset(tif,tif->tif_nextdiroff))
		{/*1*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*2*/}           /* last offset or bad offset (IFD looping) */
	SensorCall();(*tif->tif_cleanup)(tif);   /* cleanup any previous compression state */
	tif->tif_curdir++;
        nextdiroff = tif->tif_nextdiroff;
	dircount=TIFFFetchDirectory(tif,nextdiroff,&dir,&tif->tif_nextdiroff);
	SensorCall();if (!dircount)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,
		    "Failed to read directory at offset " TIFF_UINT64_FORMAT,nextdiroff);
		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}
	SensorCall();TIFFReadDirectoryCheckOrder(tif,dir,dircount);

        /*
         * Mark duplicates of any tag to be ignored (bugzilla 1994)
         * to avoid certain pathological problems.
         */
	{
		TIFFDirEntry* ma;
		uint16 mb;
		SensorCall();for (ma=dir, mb=0; mb<dircount; ma++, mb++)
		{
			SensorCall();TIFFDirEntry* na;
			uint16 nb;
			SensorCall();for (na=ma+1, nb=mb+1; nb<dircount; na++, nb++)
			{
				if (ma->tdir_tag==na->tdir_tag)
					na->tdir_tag=IGNORE;
			}
		}
	}
        
	SensorCall();tif->tif_flags &= ~TIFF_BEENWRITING;    /* reset before new dir */
	tif->tif_flags &= ~TIFF_BUF4WRITE;      /* reset before new dir */
	/* free any old stuff and reinit */
	TIFFFreeDirectory(tif);
	TIFFDefaultDirectory(tif);
	/*
	 * Electronic Arts writes gray-scale TIFF files
	 * without a PlanarConfiguration directory entry.
	 * Thus we setup a default value here, even though
	 * the TIFF spec says there is no default value.
	 */
	TIFFSetField(tif,TIFFTAG_PLANARCONFIG,PLANARCONFIG_CONTIG);
	/*
	 * Setup default value and then make a pass over
	 * the fields to check type and tag information,
	 * and to extract info required to size data
	 * structures.  A second pass is made afterwards
	 * to read in everthing not taken in the first pass.
	 * But we must process the Compression tag first
	 * in order to merge in codec-private tag definitions (otherwise
	 * we may get complaints about unknown tags).  However, the
	 * Compression tag may be dependent on the SamplesPerPixel
	 * tag value because older TIFF specs permited Compression
	 * to be written as a SamplesPerPixel-count tag entry.
	 * Thus if we don't first figure out the correct SamplesPerPixel
	 * tag value then we may end up ignoring the Compression tag
	 * value because it has an incorrect count value (if the
	 * true value of SamplesPerPixel is not 1).
	 */
	dp=TIFFReadDirectoryFindEntry(tif,dir,dircount,TIFFTAG_SAMPLESPERPIXEL);
	SensorCall();if (dp)
	{
		SensorCall();if (!TIFFFetchNormalTag(tif,dp,0))
			{/*5*/SensorCall();goto bad;/*6*/}
		SensorCall();dp->tdir_tag=IGNORE;
	}
	SensorCall();dp=TIFFReadDirectoryFindEntry(tif,dir,dircount,TIFFTAG_COMPRESSION);
	SensorCall();if (dp)
	{
		/*
		 * The 5.0 spec says the Compression tag has one value, while
		 * earlier specs say it has one value per sample.  Because of
		 * this, we accept the tag if one value is supplied with either
		 * count.
		 */
		SensorCall();uint16 value;
		enum TIFFReadDirEntryErr err;
		err=TIFFReadDirEntryShort(tif,dp,&value);
		SensorCall();if (err==TIFFReadDirEntryErrCount)
			{/*7*/SensorCall();err=TIFFReadDirEntryPersampleShort(tif,dp,&value);/*8*/}
		SensorCall();if (err!=TIFFReadDirEntryErrOk)
		{
			SensorCall();TIFFReadDirEntryOutputErr(tif,err,module,"Compression",0);
			SensorCall();goto bad;
		}
		SensorCall();if (!TIFFSetField(tif,TIFFTAG_COMPRESSION,value))
			{/*9*/SensorCall();goto bad;/*10*/}
		SensorCall();dp->tdir_tag=IGNORE;
	}
	else
	{
		SensorCall();if (!TIFFSetField(tif,TIFFTAG_COMPRESSION,COMPRESSION_NONE))
			{/*11*/SensorCall();goto bad;/*12*/}
	}
	/*
	 * First real pass over the directory.
	 */
	SensorCall();for (di=0, dp=dir; di<dircount; di++, dp++)
	{
		SensorCall();if (dp->tdir_tag!=IGNORE)
		{
			SensorCall();TIFFReadDirectoryFindFieldInfo(tif,dp->tdir_tag,&fii);
			SensorCall();if (fii == FAILED_FII)
			{
				SensorCall();TIFFWarningExt(tif->tif_clientdata, module,
				    "Unknown field with tag %d (0x%x) encountered",
				    dp->tdir_tag,dp->tdir_tag);
                                /* the following knowingly leaks the 
                                   anonymous field structure */
				SensorCall();if (!_TIFFMergeFields(tif,
					_TIFFCreateAnonField(tif,
						dp->tdir_tag,
						(TIFFDataType) dp->tdir_type),
					1)) {
					SensorCall();TIFFWarningExt(tif->tif_clientdata,
					    module,
					    "Registering anonymous field with tag %d (0x%x) failed",
					    dp->tdir_tag,
					    dp->tdir_tag);
					dp->tdir_tag=IGNORE;
				} else {
					SensorCall();TIFFReadDirectoryFindFieldInfo(tif,dp->tdir_tag,&fii);
					assert(fii != FAILED_FII);
				}
			}
		}
		SensorCall();if (dp->tdir_tag!=IGNORE)
		{
			SensorCall();fip=tif->tif_fields[fii];
			SensorCall();if (fip->field_bit==FIELD_IGNORE)
				{/*13*/SensorCall();dp->tdir_tag=IGNORE;/*14*/}
			else
			{
				SensorCall();switch (dp->tdir_tag)
				{
					case TIFFTAG_STRIPOFFSETS:
					case TIFFTAG_STRIPBYTECOUNTS:
					case TIFFTAG_TILEOFFSETS:
					case TIFFTAG_TILEBYTECOUNTS:
						TIFFSetFieldBit(tif,fip->field_bit);
						SensorCall();break;
					case TIFFTAG_IMAGEWIDTH:
					case TIFFTAG_IMAGELENGTH:
					case TIFFTAG_IMAGEDEPTH:
					case TIFFTAG_TILELENGTH:
					case TIFFTAG_TILEWIDTH:
					case TIFFTAG_TILEDEPTH:
					case TIFFTAG_PLANARCONFIG:
					case TIFFTAG_ROWSPERSTRIP:
					case TIFFTAG_EXTRASAMPLES:
						SensorCall();if (!TIFFFetchNormalTag(tif,dp,0))
							{/*15*/SensorCall();goto bad;/*16*/}
						SensorCall();dp->tdir_tag=IGNORE;
						SensorCall();break;
				}
			}
		}
	}
	/*
	 * XXX: OJPEG hack.
	 * If a) compression is OJPEG, b) planarconfig tag says it's separate,
	 * c) strip offsets/bytecounts tag are both present and
	 * d) both contain exactly one value, then we consistently find
	 * that the buggy implementation of the buggy compression scheme
	 * matches contig planarconfig best. So we 'fix-up' the tag here
	 */
	SensorCall();if ((tif->tif_dir.td_compression==COMPRESSION_OJPEG)&&
	    (tif->tif_dir.td_planarconfig==PLANARCONFIG_SEPARATE))
	{
        SensorCall();if (!_TIFFFillStriles(tif))
            {/*17*/SensorCall();goto bad;/*18*/}
		SensorCall();dp=TIFFReadDirectoryFindEntry(tif,dir,dircount,TIFFTAG_STRIPOFFSETS);
		SensorCall();if ((dp!=0)&&(dp->tdir_count==1))
		{
			SensorCall();dp=TIFFReadDirectoryFindEntry(tif,dir,dircount,
			    TIFFTAG_STRIPBYTECOUNTS);
			SensorCall();if ((dp!=0)&&(dp->tdir_count==1))
			{
				SensorCall();tif->tif_dir.td_planarconfig=PLANARCONFIG_CONTIG;
				TIFFWarningExt(tif->tif_clientdata,module,
				    "Planarconfig tag value assumed incorrect, "
				    "assuming data is contig instead of chunky");
			}
		}
	}
	/*
	 * Allocate directory structure and setup defaults.
	 */
	SensorCall();if (!TIFFFieldSet(tif,FIELD_IMAGEDIMENSIONS))
	{
		SensorCall();MissingRequired(tif,"ImageLength");
		SensorCall();goto bad;
	}
	/*
	 * Setup appropriate structures (by strip or by tile)
	 */
	SensorCall();if (!TIFFFieldSet(tif, FIELD_TILEDIMENSIONS)) {
		SensorCall();tif->tif_dir.td_nstrips = TIFFNumberOfStrips(tif);  
		tif->tif_dir.td_tilewidth = tif->tif_dir.td_imagewidth;
		tif->tif_dir.td_tilelength = tif->tif_dir.td_rowsperstrip;
		tif->tif_dir.td_tiledepth = tif->tif_dir.td_imagedepth;
		tif->tif_flags &= ~TIFF_ISTILED;
	} else {
		SensorCall();tif->tif_dir.td_nstrips = TIFFNumberOfTiles(tif);
		tif->tif_flags |= TIFF_ISTILED;
	}
	SensorCall();if (!tif->tif_dir.td_nstrips) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
		    "Cannot handle zero number of %s",
		    isTiled(tif) ? "tiles" : "strips");
		SensorCall();goto bad;
	}
	SensorCall();tif->tif_dir.td_stripsperimage = tif->tif_dir.td_nstrips;
	SensorCall();if (tif->tif_dir.td_planarconfig == PLANARCONFIG_SEPARATE)
		{/*19*/SensorCall();tif->tif_dir.td_stripsperimage /= tif->tif_dir.td_samplesperpixel;/*20*/}
	SensorCall();if (!TIFFFieldSet(tif, FIELD_STRIPOFFSETS)) {
		SensorCall();if ((tif->tif_dir.td_compression==COMPRESSION_OJPEG) &&
		    (isTiled(tif)==0) &&
		    (tif->tif_dir.td_nstrips==1)) {
			/*
			 * XXX: OJPEG hack.
			 * If a) compression is OJPEG, b) it's not a tiled TIFF,
			 * and c) the number of strips is 1,
			 * then we tolerate the absence of stripoffsets tag,
			 * because, presumably, all required data is in the
			 * JpegInterchangeFormat stream.
			 */
			TIFFSetFieldBit(tif, FIELD_STRIPOFFSETS);
		} else {
			SensorCall();MissingRequired(tif,
				isTiled(tif) ? "TileOffsets" : "StripOffsets");
			SensorCall();goto bad;
		}
	}
	/*
	 * Second pass: extract other information.
	 */
	SensorCall();for (di=0, dp=dir; di<dircount; di++, dp++)
	{
		SensorCall();switch (dp->tdir_tag)
		{
			case IGNORE:
				SensorCall();break;
			case TIFFTAG_MINSAMPLEVALUE:
			case TIFFTAG_MAXSAMPLEVALUE:
			case TIFFTAG_BITSPERSAMPLE:
			case TIFFTAG_DATATYPE:
			case TIFFTAG_SAMPLEFORMAT:
				/*
				 * The MinSampleValue, MaxSampleValue, BitsPerSample
				 * DataType and SampleFormat tags are supposed to be
				 * written as one value/sample, but some vendors
				 * incorrectly write one value only -- so we accept
				 * that as well (yech). Other vendors write correct
				 * value for NumberOfSamples, but incorrect one for
				 * BitsPerSample and friends, and we will read this
				 * too.
				 */
				{
					SensorCall();uint16 value;
					enum TIFFReadDirEntryErr err;
					err=TIFFReadDirEntryShort(tif,dp,&value);
					SensorCall();if (err==TIFFReadDirEntryErrCount)
						{/*21*/SensorCall();err=TIFFReadDirEntryPersampleShort(tif,dp,&value);/*22*/}
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{
						SensorCall();fip = TIFFFieldWithTag(tif,dp->tdir_tag);
						TIFFReadDirEntryOutputErr(tif,err,module,fip ? fip->field_name : "unknown tagname",0);
						SensorCall();goto bad;
					}
					SensorCall();if (!TIFFSetField(tif,dp->tdir_tag,value))
						{/*23*/SensorCall();goto bad;/*24*/}
				}
				SensorCall();break;
			case TIFFTAG_SMINSAMPLEVALUE:
			case TIFFTAG_SMAXSAMPLEVALUE:
				{

					SensorCall();double *data;
					enum TIFFReadDirEntryErr err;
					uint32 saved_flags;
					int m;
					SensorCall();if (dp->tdir_count != (uint64)tif->tif_dir.td_samplesperpixel)
						{/*25*/SensorCall();err = TIFFReadDirEntryErrCount;/*26*/}
					else
						{/*27*/SensorCall();err = TIFFReadDirEntryDoubleArray(tif, dp, &data);/*28*/}
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
					{
						SensorCall();fip = TIFFFieldWithTag(tif,dp->tdir_tag);
						TIFFReadDirEntryOutputErr(tif,err,module,fip ? fip->field_name : "unknown tagname",0);
						SensorCall();goto bad;
					}
					SensorCall();saved_flags = tif->tif_flags;
					tif->tif_flags |= TIFF_PERSAMPLE;
					m = TIFFSetField(tif,dp->tdir_tag,data);
					tif->tif_flags = saved_flags;
					_TIFFfree(data);
					SensorCall();if (!m)
						{/*29*/SensorCall();goto bad;/*30*/}
				}
				SensorCall();break;
			case TIFFTAG_STRIPOFFSETS:
			case TIFFTAG_TILEOFFSETS:
#if defined(DEFER_STRILE_LOAD)
                                _TIFFmemcpy( &(tif->tif_dir.td_stripoffset_entry),
                                             dp, sizeof(TIFFDirEntry) );
#else                          
				SensorCall();if (!TIFFFetchStripThing(tif,dp,tif->tif_dir.td_nstrips,&tif->tif_dir.td_stripoffset))  
					{/*31*/SensorCall();goto bad;/*32*/}
#endif                                
				SensorCall();break;
			case TIFFTAG_STRIPBYTECOUNTS:
			case TIFFTAG_TILEBYTECOUNTS:
#if defined(DEFER_STRILE_LOAD)
                                _TIFFmemcpy( &(tif->tif_dir.td_stripbytecount_entry),
                                             dp, sizeof(TIFFDirEntry) );
#else                          
				SensorCall();if (!TIFFFetchStripThing(tif,dp,tif->tif_dir.td_nstrips,&tif->tif_dir.td_stripbytecount))  
					{/*33*/SensorCall();goto bad;/*34*/}
#endif                                
				SensorCall();break;
			case TIFFTAG_COLORMAP:
			case TIFFTAG_TRANSFERFUNCTION:
				{
					SensorCall();enum TIFFReadDirEntryErr err;
					uint32 countpersample;
					uint32 countrequired;
					uint32 incrementpersample;
					uint16* value=NULL;
					countpersample=(1L<<tif->tif_dir.td_bitspersample);
					SensorCall();if ((dp->tdir_tag==TIFFTAG_TRANSFERFUNCTION)&&(dp->tdir_count==(uint64)countpersample))
					{
						SensorCall();countrequired=countpersample;
						incrementpersample=0;
					}
					else
					{
						SensorCall();countrequired=3*countpersample;
						incrementpersample=countpersample;
					}
					SensorCall();if (dp->tdir_count!=(uint64)countrequired)
						{/*35*/SensorCall();err=TIFFReadDirEntryErrCount;/*36*/}
					else
						{/*37*/SensorCall();err=TIFFReadDirEntryShortArray(tif,dp,&value);/*38*/}
					SensorCall();if (err!=TIFFReadDirEntryErrOk)
                    {
						SensorCall();fip = TIFFFieldWithTag(tif,dp->tdir_tag);
						TIFFReadDirEntryOutputErr(tif,err,module,fip ? fip->field_name : "unknown tagname",1);
                    }
					else
					{
						SensorCall();TIFFSetField(tif,dp->tdir_tag,value,value+incrementpersample,value+2*incrementpersample);
						_TIFFfree(value);
					}
				}
				SensorCall();break;
/* BEGIN REV 4.0 COMPATIBILITY */
			case TIFFTAG_OSUBFILETYPE:
				{
					SensorCall();uint16 valueo;
					uint32 value;
					SensorCall();if (TIFFReadDirEntryShort(tif,dp,&valueo)==TIFFReadDirEntryErrOk)
					{
						SensorCall();switch (valueo)
						{
							case OFILETYPE_REDUCEDIMAGE: SensorCall();value=FILETYPE_REDUCEDIMAGE; SensorCall();break;
							case OFILETYPE_PAGE: SensorCall();value=FILETYPE_PAGE; SensorCall();break;
							default: SensorCall();value=0; SensorCall();break;
						}
						SensorCall();if (value!=0)
							{/*39*/SensorCall();TIFFSetField(tif,TIFFTAG_SUBFILETYPE,value);/*40*/}
					}
				}
				SensorCall();break;
/* END REV 4.0 COMPATIBILITY */
			default:
				SensorCall();(void) TIFFFetchNormalTag(tif, dp, TRUE);
				SensorCall();break;
		}
	}
	/*
	 * OJPEG hack:
	 * - If a) compression is OJPEG, and b) photometric tag is missing,
	 * then we consistently find that photometric should be YCbCr
	 * - If a) compression is OJPEG, and b) photometric tag says it's RGB,
	 * then we consistently find that the buggy implementation of the
	 * buggy compression scheme matches photometric YCbCr instead.
	 * - If a) compression is OJPEG, and b) bitspersample tag is missing,
	 * then we consistently find bitspersample should be 8.
	 * - If a) compression is OJPEG, b) samplesperpixel tag is missing,
	 * and c) photometric is RGB or YCbCr, then we consistently find
	 * samplesperpixel should be 3
	 * - If a) compression is OJPEG, b) samplesperpixel tag is missing,
	 * and c) photometric is MINISWHITE or MINISBLACK, then we consistently
	 * find samplesperpixel should be 3
	 */
	SensorCall();if (tif->tif_dir.td_compression==COMPRESSION_OJPEG)
	{
		SensorCall();if (!TIFFFieldSet(tif,FIELD_PHOTOMETRIC))
		{
			SensorCall();TIFFWarningExt(tif->tif_clientdata, module,
			    "Photometric tag is missing, assuming data is YCbCr");
			SensorCall();if (!TIFFSetField(tif,TIFFTAG_PHOTOMETRIC,PHOTOMETRIC_YCBCR))
				{/*41*/SensorCall();goto bad;/*42*/}
		}
		else {/*43*/SensorCall();if (tif->tif_dir.td_photometric==PHOTOMETRIC_RGB)
		{
			SensorCall();tif->tif_dir.td_photometric=PHOTOMETRIC_YCBCR;
			TIFFWarningExt(tif->tif_clientdata, module,
			    "Photometric tag value assumed incorrect, "
			    "assuming data is YCbCr instead of RGB");
		;/*44*/}}
		SensorCall();if (!TIFFFieldSet(tif,FIELD_BITSPERSAMPLE))
		{
			SensorCall();TIFFWarningExt(tif->tif_clientdata,module,
			    "BitsPerSample tag is missing, assuming 8 bits per sample");
			SensorCall();if (!TIFFSetField(tif,TIFFTAG_BITSPERSAMPLE,8))
				{/*45*/SensorCall();goto bad;/*46*/}
		}
		SensorCall();if (!TIFFFieldSet(tif,FIELD_SAMPLESPERPIXEL))
		{
			SensorCall();if (tif->tif_dir.td_photometric==PHOTOMETRIC_RGB)
			{
				SensorCall();TIFFWarningExt(tif->tif_clientdata,module,
				    "SamplesPerPixel tag is missing, "
				    "assuming correct SamplesPerPixel value is 3");
				SensorCall();if (!TIFFSetField(tif,TIFFTAG_SAMPLESPERPIXEL,3))
					{/*47*/SensorCall();goto bad;/*48*/}
			}
			SensorCall();if (tif->tif_dir.td_photometric==PHOTOMETRIC_YCBCR)
			{
				SensorCall();TIFFWarningExt(tif->tif_clientdata,module,
				    "SamplesPerPixel tag is missing, "
				    "applying correct SamplesPerPixel value of 3");
				SensorCall();if (!TIFFSetField(tif,TIFFTAG_SAMPLESPERPIXEL,3))
					{/*49*/SensorCall();goto bad;/*50*/}
			}
			else {/*51*/SensorCall();if ((tif->tif_dir.td_photometric==PHOTOMETRIC_MINISWHITE)
				 || (tif->tif_dir.td_photometric==PHOTOMETRIC_MINISBLACK))
			{
				/*
				 * SamplesPerPixel tag is missing, but is not required
				 * by spec.  Assume correct SamplesPerPixel value of 1.
				 */
				SensorCall();if (!TIFFSetField(tif,TIFFTAG_SAMPLESPERPIXEL,1))
					{/*53*/SensorCall();goto bad;/*54*/}
			;/*52*/}}
			/*
			 * SamplesPerPixel value has changed, adjust SMinSampleValue
			 * and SMaxSampleValue arrays if necessary
			 */
			{
				SensorCall();uint32 saved_flags;
				saved_flags = tif->tif_flags;
				tif->tif_flags &= ~TIFF_PERSAMPLE;
				SensorCall();if (TIFFFieldSet(tif,FIELD_SMINSAMPLEVALUE))
				{
					SensorCall();if (!TIFFSetField(tif,TIFFTAG_SMINSAMPLEVALUE,tif->tif_dir.td_sminsamplevalue[0]))
					{
						SensorCall();tif->tif_flags = saved_flags;
						SensorCall();goto bad;
					}
				}
				SensorCall();if (TIFFFieldSet(tif,FIELD_SMAXSAMPLEVALUE))
				{
					SensorCall();if (!TIFFSetField(tif,TIFFTAG_SMAXSAMPLEVALUE,tif->tif_dir.td_smaxsamplevalue[0]))
					{
						SensorCall();tif->tif_flags = saved_flags;
						SensorCall();goto bad;
					}
				}
				SensorCall();tif->tif_flags = saved_flags;
			}
		}
	}
	/*
	 * Verify Palette image has a Colormap.
	 */
	SensorCall();if (tif->tif_dir.td_photometric == PHOTOMETRIC_PALETTE &&
	    !TIFFFieldSet(tif, FIELD_COLORMAP)) {
		SensorCall();if ( tif->tif_dir.td_bitspersample>=8 && tif->tif_dir.td_samplesperpixel==3)
			tif->tif_dir.td_photometric = PHOTOMETRIC_RGB;
		else {/*55*/SensorCall();if (tif->tif_dir.td_bitspersample>=8)
			tif->tif_dir.td_photometric = PHOTOMETRIC_MINISBLACK;
		else {
			SensorCall();MissingRequired(tif, "Colormap");
			SensorCall();goto bad;
		;/*56*/}}
	}
	/*
	 * OJPEG hack:
	 * We do no further messing with strip/tile offsets/bytecounts in OJPEG
	 * TIFFs
	 */
	SensorCall();if (tif->tif_dir.td_compression!=COMPRESSION_OJPEG)
	{
		/*
		 * Attempt to deal with a missing StripByteCounts tag.
		 */
		SensorCall();if (!TIFFFieldSet(tif, FIELD_STRIPBYTECOUNTS)) {
			/*
			 * Some manufacturers violate the spec by not giving
			 * the size of the strips.  In this case, assume there
			 * is one uncompressed strip of data.
			 */
			SensorCall();if ((tif->tif_dir.td_planarconfig == PLANARCONFIG_CONTIG &&
			    tif->tif_dir.td_nstrips > 1) ||
			    (tif->tif_dir.td_planarconfig == PLANARCONFIG_SEPARATE &&
			     tif->tif_dir.td_nstrips != (uint32)tif->tif_dir.td_samplesperpixel)) {
			    SensorCall();MissingRequired(tif, "StripByteCounts");
			    SensorCall();goto bad;
			}
			SensorCall();TIFFWarningExt(tif->tif_clientdata, module,
				"TIFF directory is missing required "
				"\"StripByteCounts\" field, calculating from imagelength");
			SensorCall();if (EstimateStripByteCounts(tif, dir, dircount) < 0)
			    {/*57*/SensorCall();goto bad;/*58*/}
		/*
		 * Assume we have wrong StripByteCount value (in case
		 * of single strip) in following cases:
		 *   - it is equal to zero along with StripOffset;
		 *   - it is larger than file itself (in case of uncompressed
		 *     image);
		 *   - it is smaller than the size of the bytes per row
		 *     multiplied on the number of rows.  The last case should
		 *     not be checked in the case of writing new image,
		 *     because we may do not know the exact strip size
		 *     until the whole image will be written and directory
		 *     dumped out.
		 */
		#define	BYTECOUNTLOOKSBAD \
		    ( (tif->tif_dir.td_stripbytecount[0] == 0 && tif->tif_dir.td_stripoffset[0] != 0) || \
		      (tif->tif_dir.td_compression == COMPRESSION_NONE && \
		       tif->tif_dir.td_stripbytecount[0] > TIFFGetFileSize(tif) - tif->tif_dir.td_stripoffset[0]) || \
		      (tif->tif_mode == O_RDONLY && \
		       tif->tif_dir.td_compression == COMPRESSION_NONE && \
		       tif->tif_dir.td_stripbytecount[0] < TIFFScanlineSize64(tif) * tif->tif_dir.td_imagelength) )

		} else {/*59*/SensorCall();if (tif->tif_dir.td_nstrips == 1
                           && _TIFFFillStriles(tif)
			   && tif->tif_dir.td_stripoffset[0] != 0
			   && BYTECOUNTLOOKSBAD) {
			/*
			 * XXX: Plexus (and others) sometimes give a value of
			 * zero for a tag when they don't know what the
			 * correct value is!  Try and handle the simple case
			 * of estimating the size of a one strip image.
			 */
			SensorCall();TIFFWarningExt(tif->tif_clientdata, module,
			    "Bogus \"StripByteCounts\" field, ignoring and calculating from imagelength");
			SensorCall();if(EstimateStripByteCounts(tif, dir, dircount) < 0)
			    {/*61*/SensorCall();goto bad;/*62*/}

#if !defined(DEFER_STRILE_LOAD)
		} else {/*63*/SensorCall();if (tif->tif_dir.td_planarconfig == PLANARCONFIG_CONTIG
			   && tif->tif_dir.td_nstrips > 2
			   && tif->tif_dir.td_compression == COMPRESSION_NONE
			   && tif->tif_dir.td_stripbytecount[0] != tif->tif_dir.td_stripbytecount[1]
			   && tif->tif_dir.td_stripbytecount[0] != 0
			   && tif->tif_dir.td_stripbytecount[1] != 0 ) {
			/*
			 * XXX: Some vendors fill StripByteCount array with
			 * absolutely wrong values (it can be equal to
			 * StripOffset array, for example). Catch this case
			 * here.
                         *
                         * We avoid this check if deferring strile loading
                         * as it would always force us to load the strip/tile
                         * information.
			 */
			SensorCall();TIFFWarningExt(tif->tif_clientdata, module,
			    "Wrong \"StripByteCounts\" field, ignoring and calculating from imagelength");
			SensorCall();if (EstimateStripByteCounts(tif, dir, dircount) < 0)
			    {/*65*/SensorCall();goto bad;/*66*/}
#endif /* !defined(DEFER_STRILE_LOAD) */                        
		;/*64*/}/*60*/}}
	}
	SensorCall();if (dir)
	{
		SensorCall();_TIFFfree(dir);
		dir=NULL;
	}
	SensorCall();if (!TIFFFieldSet(tif, FIELD_MAXSAMPLEVALUE))
	{
		SensorCall();if (tif->tif_dir.td_bitspersample>=16)
			{/*67*/SensorCall();tif->tif_dir.td_maxsamplevalue=0xFFFF;/*68*/}
		else
			{/*69*/SensorCall();tif->tif_dir.td_maxsamplevalue = (uint16)((1L<<tif->tif_dir.td_bitspersample)-1);/*70*/}
	}
	/*
	 * XXX: We can optimize checking for the strip bounds using the sorted
	 * bytecounts array. See also comments for TIFFAppendToStrip()
	 * function in tif_write.c.
	 */
#if !defined(DEFER_STRILE_LOAD)        
	SensorCall();if (tif->tif_dir.td_nstrips > 1) {
		SensorCall();uint32 strip;

		tif->tif_dir.td_stripbytecountsorted = 1;
		SensorCall();for (strip = 1; strip < tif->tif_dir.td_nstrips; strip++) {
			SensorCall();if (tif->tif_dir.td_stripoffset[strip - 1] >
			    tif->tif_dir.td_stripoffset[strip]) {
				SensorCall();tif->tif_dir.td_stripbytecountsorted = 0;
				SensorCall();break;
			}
		}
	}
#endif /* !defined(DEFER_STRILE_LOAD) */
        
	/*
	 * An opportunity for compression mode dependent tag fixup
	 */
	SensorCall();(*tif->tif_fixuptags)(tif);

	/*
	 * Some manufacturers make life difficult by writing
	 * large amounts of uncompressed data as a single strip.
	 * This is contrary to the recommendations of the spec.
	 * The following makes an attempt at breaking such images
	 * into strips closer to the recommended 8k bytes.  A
	 * side effect, however, is that the RowsPerStrip tag
	 * value may be changed.
	 */
	SensorCall();if ((tif->tif_dir.td_planarconfig==PLANARCONFIG_CONTIG)&&
	    (tif->tif_dir.td_nstrips==1)&&
	    (tif->tif_dir.td_compression==COMPRESSION_NONE)&&  
	    ((tif->tif_flags&(TIFF_STRIPCHOP|TIFF_ISTILED))==TIFF_STRIPCHOP))
    {
        SensorCall();if ( !_TIFFFillStriles(tif) || !tif->tif_dir.td_stripbytecount )
            {/*71*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*72*/}
		SensorCall();ChopUpSingleUncompressedStrip(tif);
    }

        /*
         * Clear the dirty directory flag. 
         */
	SensorCall();tif->tif_flags &= ~TIFF_DIRTYDIRECT;
	tif->tif_flags &= ~TIFF_DIRTYSTRIP;

	/*
	 * Reinitialize i/o since we are starting on a new directory.
	 */
	tif->tif_row = (uint32) -1;
	tif->tif_curstrip = (uint32) -1;
	tif->tif_col = (uint32) -1;
	tif->tif_curtile = (uint32) -1;
	tif->tif_tilesize = (tmsize_t) -1;

	tif->tif_scanlinesize = TIFFScanlineSize(tif);
	SensorCall();if (!tif->tif_scanlinesize) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
		    "Cannot handle zero scanline size");
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}

	SensorCall();if (isTiled(tif)) {
		SensorCall();tif->tif_tilesize = TIFFTileSize(tif);
		SensorCall();if (!tif->tif_tilesize) {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
			     "Cannot handle zero tile size");
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
	} else {
		SensorCall();if (!TIFFStripSize(tif)) {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
			    "Cannot handle zero strip size");
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
	}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
bad:
	if (dir)
		{/*73*/_TIFFfree(dir);/*74*/}
	return (0);
}

static void
TIFFReadDirectoryCheckOrder(TIFF* tif, TIFFDirEntry* dir, uint16 dircount)
{
	SensorCall();static const char module[] = "TIFFReadDirectoryCheckOrder";
	uint16 m;
	uint16 n;
	TIFFDirEntry* o;
	m=0;
	SensorCall();for (n=0, o=dir; n<dircount; n++, o++)
	{
		SensorCall();if (o->tdir_tag<m)
		{
			SensorCall();TIFFWarningExt(tif->tif_clientdata,module,
			    "Invalid TIFF directory; tags are not sorted in ascending order");
			SensorCall();break;
		}
		SensorCall();m=o->tdir_tag+1;
	}
SensorCall();}

static TIFFDirEntry*
TIFFReadDirectoryFindEntry(TIFF* tif, TIFFDirEntry* dir, uint16 dircount, uint16 tagid)
{
	SensorCall();TIFFDirEntry* m;
	uint16 n;
	(void) tif;
	SensorCall();for (m=dir, n=0; n<dircount; m++, n++)
	{
		SensorCall();if (m->tdir_tag==tagid)
			{/*657*/SensorCall();return(m);/*658*/}
	}
	SensorCall();return(0);
}

static void
TIFFReadDirectoryFindFieldInfo(TIFF* tif, uint16 tagid, uint32* fii)
{
	SensorCall();int32 ma,mb,mc;
	ma=-1;
	mc=(int32)tif->tif_nfields;
	SensorCall();while (1)
	{
		SensorCall();if (ma+1==mc)
		{
			SensorCall();*fii = FAILED_FII;
			SensorCall();return;
		}
		SensorCall();mb=(ma+mc)/2;
		SensorCall();if (tif->tif_fields[mb]->field_tag==(uint32)tagid)
			{/*659*/SensorCall();break;/*660*/}
		SensorCall();if (tif->tif_fields[mb]->field_tag<(uint32)tagid)
			{/*661*/SensorCall();ma=mb;/*662*/}
		else
			{/*663*/SensorCall();mc=mb;/*664*/}
	}
	SensorCall();while (1)
	{
		SensorCall();if (mb==0)
			{/*665*/SensorCall();break;/*666*/}
		SensorCall();if (tif->tif_fields[mb-1]->field_tag!=(uint32)tagid)
			{/*667*/SensorCall();break;/*668*/}
		SensorCall();mb--;
	}
	SensorCall();*fii=mb;
SensorCall();}

/*
 * Read custom directory from the arbitarry offset.
 * The code is very similar to TIFFReadDirectory().
 */
int
TIFFReadCustomDirectory(TIFF* tif, toff_t diroff,
			const TIFFFieldArray* infoarray)
{
	SensorCall();static const char module[] = "TIFFReadCustomDirectory";
	TIFFDirEntry* dir;
	uint16 dircount;
	TIFFDirEntry* dp;
	uint16 di;
	const TIFFField* fip;
	uint32 fii;
	_TIFFSetupFields(tif, infoarray);
	dircount=TIFFFetchDirectory(tif,diroff,&dir,NULL);
	SensorCall();if (!dircount)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,
		    "Failed to read custom directory at offset " TIFF_UINT64_FORMAT,diroff);
		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}
	SensorCall();TIFFFreeDirectory(tif);
	_TIFFmemset(&tif->tif_dir, 0, sizeof(TIFFDirectory));
	TIFFReadDirectoryCheckOrder(tif,dir,dircount);
	SensorCall();for (di=0, dp=dir; di<dircount; di++, dp++)
	{
		SensorCall();TIFFReadDirectoryFindFieldInfo(tif,dp->tdir_tag,&fii);
		SensorCall();if (fii == FAILED_FII)
		{
			SensorCall();TIFFWarningExt(tif->tif_clientdata, module,
			    "Unknown field with tag %d (0x%x) encountered",
			    dp->tdir_tag, dp->tdir_tag);
			SensorCall();if (!_TIFFMergeFields(tif, _TIFFCreateAnonField(tif,
						dp->tdir_tag,
						(TIFFDataType) dp->tdir_type),
					     1)) {
				SensorCall();TIFFWarningExt(tif->tif_clientdata, module,
				    "Registering anonymous field with tag %d (0x%x) failed",
				    dp->tdir_tag, dp->tdir_tag);
				dp->tdir_tag=IGNORE;
			} else {
				SensorCall();TIFFReadDirectoryFindFieldInfo(tif,dp->tdir_tag,&fii);
				assert( fii != FAILED_FII );
			}
		}
		SensorCall();if (dp->tdir_tag!=IGNORE)
		{
			SensorCall();fip=tif->tif_fields[fii];
			SensorCall();if (fip->field_bit==FIELD_IGNORE)
				{/*75*/SensorCall();dp->tdir_tag=IGNORE;/*76*/}
			else
			{
				/* check data type */
				SensorCall();while ((fip->field_type!=TIFF_ANY)&&(fip->field_type!=dp->tdir_type))
				{
					SensorCall();fii++;
					SensorCall();if ((fii==tif->tif_nfields)||
					    (tif->tif_fields[fii]->field_tag!=(uint32)dp->tdir_tag))
					{
						SensorCall();fii=0xFFFF;
						SensorCall();break;
					}
					SensorCall();fip=tif->tif_fields[fii];
				}
				SensorCall();if (fii==0xFFFF)
				{
					SensorCall();TIFFWarningExt(tif->tif_clientdata, module,
					    "Wrong data type %d for \"%s\"; tag ignored",
					    dp->tdir_type,fip->field_name);
					dp->tdir_tag=IGNORE;
				}
				else
				{
					/* check count if known in advance */
					SensorCall();if ((fip->field_readcount!=TIFF_VARIABLE)&&
					    (fip->field_readcount!=TIFF_VARIABLE2))
					{
						SensorCall();uint32 expected;
						SensorCall();if (fip->field_readcount==TIFF_SPP)
							{/*77*/SensorCall();expected=(uint32)tif->tif_dir.td_samplesperpixel;/*78*/}
						else
							{/*79*/SensorCall();expected=(uint32)fip->field_readcount;/*80*/}
						if (!CheckDirCount(tif,dp,expected))
							dp->tdir_tag=IGNORE;
					}
				}
			}
			SensorCall();switch (dp->tdir_tag)
			{
				case IGNORE:
					SensorCall();break;
				case EXIFTAG_SUBJECTDISTANCE:
					SensorCall();(void) TIFFFetchSubjectDistance(tif,dp);
					SensorCall();break;
				default:
					SensorCall();(void) TIFFFetchNormalTag(tif, dp, TRUE);
					SensorCall();break;
			}
		}
	}
	SensorCall();if (dir)
		{/*83*/SensorCall();_TIFFfree(dir);/*84*/}
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

/*
 * EXIF is important special case of custom IFD, so we have a special
 * function to read it.
 */
int
TIFFReadEXIFDirectory(TIFF* tif, toff_t diroff)
{
	SensorCall();const TIFFFieldArray* exifFieldArray;
	exifFieldArray = _TIFFGetExifFields();
	{int  ReplaceReturn = TIFFReadCustomDirectory(tif, diroff, exifFieldArray); SensorCall(); return ReplaceReturn;}  
}

static int
EstimateStripByteCounts(TIFF* tif, TIFFDirEntry* dir, uint16 dircount)
{
	SensorCall();static const char module[] = "EstimateStripByteCounts";

	TIFFDirEntry *dp;
	TIFFDirectory *td = &tif->tif_dir;
	uint32 strip;

    _TIFFFillStriles( tif );

	SensorCall();if (td->td_stripbytecount)
		{/*669*/SensorCall();_TIFFfree(td->td_stripbytecount);/*670*/}
	SensorCall();td->td_stripbytecount = (uint64*)
	    _TIFFCheckMalloc(tif, td->td_nstrips, sizeof (uint64),
		"for \"StripByteCounts\" array");
        SensorCall();if( td->td_stripbytecount == NULL )
            {/*671*/{int  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}/*672*/}

	SensorCall();if (td->td_compression != COMPRESSION_NONE) {
		SensorCall();uint64 space;
		uint64 filesize;
		uint16 n;
		filesize = TIFFGetFileSize(tif);
		SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
			{/*673*/SensorCall();space=sizeof(TIFFHeaderClassic)+2+dircount*12+4;/*674*/}
		else
			{/*675*/SensorCall();space=sizeof(TIFFHeaderBig)+8+dircount*20+8;/*676*/}
		/* calculate amount of space used by indirect values */
		SensorCall();for (dp = dir, n = dircount; n > 0; n--, dp++)
		{
			SensorCall();uint32 typewidth = TIFFDataWidth((TIFFDataType) dp->tdir_type);
			uint64 datasize;
			typewidth = TIFFDataWidth((TIFFDataType) dp->tdir_type);
			SensorCall();if (typewidth == 0) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				    "Cannot determine size of unknown tag type %d",
				    dp->tdir_type);
				{int  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}
			}
			SensorCall();datasize=(uint64)typewidth*dp->tdir_count;
			SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
			{
				SensorCall();if (datasize<=4)
					{/*677*/SensorCall();datasize=0;/*678*/}
			}
			else
			{
				SensorCall();if (datasize<=8)
					{/*679*/SensorCall();datasize=0;/*680*/}
			}
			SensorCall();space+=datasize;
		}
		SensorCall();space = filesize - space;
		SensorCall();if (td->td_planarconfig == PLANARCONFIG_SEPARATE)
			{/*681*/SensorCall();space /= td->td_samplesperpixel;/*682*/}
		SensorCall();for (strip = 0; strip < td->td_nstrips; strip++)
			{/*683*/SensorCall();td->td_stripbytecount[strip] = space;/*684*/}
		/*
		 * This gross hack handles the case were the offset to
		 * the last strip is past the place where we think the strip
		 * should begin.  Since a strip of data must be contiguous,
		 * it's safe to assume that we've overestimated the amount
		 * of data in the strip and trim this number back accordingly.
		 */
		SensorCall();strip--;
		SensorCall();if (td->td_stripoffset[strip]+td->td_stripbytecount[strip] > filesize)
			{/*685*/SensorCall();td->td_stripbytecount[strip] = filesize - td->td_stripoffset[strip];/*686*/}
	} else {/*687*/SensorCall();if (isTiled(tif)) {
		SensorCall();uint64 bytespertile = TIFFTileSize64(tif);

		SensorCall();for (strip = 0; strip < td->td_nstrips; strip++)
		    {/*689*/SensorCall();td->td_stripbytecount[strip] = bytespertile;/*690*/}
	} else {
		SensorCall();uint64 rowbytes = TIFFScanlineSize64(tif);
		uint32 rowsperstrip = td->td_imagelength/td->td_stripsperimage;
		SensorCall();for (strip = 0; strip < td->td_nstrips; strip++)
			{/*691*/SensorCall();td->td_stripbytecount[strip] = rowbytes * rowsperstrip;/*692*/}
	;/*688*/}}
	TIFFSetFieldBit(tif, FIELD_STRIPBYTECOUNTS);
	SensorCall();if (!TIFFFieldSet(tif, FIELD_ROWSPERSTRIP))
		{/*693*/SensorCall();td->td_rowsperstrip = td->td_imagelength;/*694*/}
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

static void
MissingRequired(TIFF* tif, const char* tagname)
{
	SensorCall();static const char module[] = "MissingRequired";

	TIFFErrorExt(tif->tif_clientdata, module,
	    "TIFF directory is missing required \"%s\" field",
	    tagname);
SensorCall();}

/*
 * Check the directory offset against the list of already seen directory
 * offsets. This is a trick to prevent IFD looping. The one can create TIFF
 * file with looped directory pointers. We will maintain a list of already
 * seen directories and check every IFD offset against that list.
 */
static int
TIFFCheckDirOffset(TIFF* tif, uint64 diroff)
{
	SensorCall();uint16 n;

	SensorCall();if (diroff == 0)			/* no more directories */
		{/*695*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*696*/}

	SensorCall();for (n = 0; n < tif->tif_dirnumber && tif->tif_dirlist; n++) {
		SensorCall();if (tif->tif_dirlist[n] == diroff)
			{/*697*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*698*/}
	}

	SensorCall();tif->tif_dirnumber++;

	SensorCall();if (tif->tif_dirnumber > tif->tif_dirlistsize) {
		SensorCall();uint64* new_dirlist;

		/*
		 * XXX: Reduce memory allocation granularity of the dirlist
		 * array.
		 */
		new_dirlist = (uint64*)_TIFFCheckRealloc(tif, tif->tif_dirlist,
		    tif->tif_dirnumber, 2 * sizeof(uint64), "for IFD list");
		SensorCall();if (!new_dirlist)
			{/*699*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*700*/}
		SensorCall();tif->tif_dirlistsize = 2 * tif->tif_dirnumber;
		tif->tif_dirlist = new_dirlist;
	}

	SensorCall();tif->tif_dirlist[tif->tif_dirnumber - 1] = diroff;

	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

/*
 * Check the count field of a directory entry against a known value.  The
 * caller is expected to skip/ignore the tag if there is a mismatch.
 */
static int
CheckDirCount(TIFF* tif, TIFFDirEntry* dir, uint32 count)
{
	SensorCall();if ((uint64)count > dir->tdir_count) {
		SensorCall();const TIFFField* fip = TIFFFieldWithTag(tif, dir->tdir_tag);
		TIFFWarningExt(tif->tif_clientdata, tif->tif_name,
	"incorrect count for field \"%s\" (" TIFF_UINT64_FORMAT ", expecting %u); tag ignored",
		    fip ? fip->field_name : "unknown tagname",
		    dir->tdir_count, count);
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	} else {/*701*/SensorCall();if ((uint64)count < dir->tdir_count) {
		SensorCall();const TIFFField* fip = TIFFFieldWithTag(tif, dir->tdir_tag);
		TIFFWarningExt(tif->tif_clientdata, tif->tif_name,
	"incorrect count for field \"%s\" (" TIFF_UINT64_FORMAT ", expecting %u); tag trimmed",
		    fip ? fip->field_name : "unknown tagname",
		    dir->tdir_count, count);
		dir->tdir_count = count;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	;/*702*/}}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

/*
 * Read IFD structure from the specified offset. If the pointer to
 * nextdiroff variable has been specified, read it too. Function returns a
 * number of fields in the directory or 0 if failed.
 */
static uint16
TIFFFetchDirectory(TIFF* tif, uint64 diroff, TIFFDirEntry** pdir,
                   uint64 *nextdiroff)
{
	SensorCall();static const char module[] = "TIFFFetchDirectory";

	void* origdir;
	uint16 dircount16;
	uint32 dirsize;
	TIFFDirEntry* dir;
	uint8* ma;
	TIFFDirEntry* mb;
	uint16 n;

	assert(pdir);

	tif->tif_diroff = diroff;
	SensorCall();if (nextdiroff)
		{/*703*/SensorCall();*nextdiroff = 0;/*704*/}
	SensorCall();if (!isMapped(tif)) {
		SensorCall();if (!SeekOK(tif, tif->tif_diroff)) {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				"%s: Seek error accessing TIFF directory",
				tif->tif_name);
			{uint16  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
		}
		SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
		{
			SensorCall();if (!ReadOK(tif, &dircount16, sizeof (uint16))) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				    "%s: Can not read TIFF directory count",
				    tif->tif_name);
				{uint16  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
			}
			SensorCall();if (tif->tif_flags & TIFF_SWAB)
				{/*705*/SensorCall();TIFFSwabShort(&dircount16);/*706*/}
			SensorCall();if (dircount16>4096)
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				    "Sanity check on directory count failed, this is probably not a valid IFD offset");
				{uint16  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
			}
			SensorCall();dirsize = 12;
		} else {
			SensorCall();uint64 dircount64;
			SensorCall();if (!ReadOK(tif, &dircount64, sizeof (uint64))) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					"%s: Can not read TIFF directory count",
					tif->tif_name);
				{uint16  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
			}
			SensorCall();if (tif->tif_flags & TIFF_SWAB)
				{/*707*/SensorCall();TIFFSwabLong8(&dircount64);/*708*/}
			SensorCall();if (dircount64>4096)
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				    "Sanity check on directory count failed, this is probably not a valid IFD offset");
				{uint16  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
			}
			SensorCall();dircount16 = (uint16)dircount64;
			dirsize = 20;
		}
		SensorCall();origdir = _TIFFCheckMalloc(tif, dircount16,
		    dirsize, "to read TIFF directory");
		SensorCall();if (origdir == NULL)
			{/*709*/{uint16  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*710*/}
		SensorCall();if (!ReadOK(tif, origdir, (tmsize_t)(dircount16*dirsize))) {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				"%.100s: Can not read TIFF directory",
				tif->tif_name);
			_TIFFfree(origdir);
			{uint16  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
		}
		/*
		 * Read offset to next directory for sequential scans if
		 * needed.
		 */
		SensorCall();if (nextdiroff)
		{
			SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
			{
				SensorCall();uint32 nextdiroff32;
				SensorCall();if (!ReadOK(tif, &nextdiroff32, sizeof(uint32)))
					{/*711*/SensorCall();nextdiroff32 = 0;/*712*/}
				SensorCall();if (tif->tif_flags&TIFF_SWAB)
					{/*713*/SensorCall();TIFFSwabLong(&nextdiroff32);/*714*/}
				SensorCall();*nextdiroff=nextdiroff32;
			} else {
				SensorCall();if (!ReadOK(tif, nextdiroff, sizeof(uint64)))
					{/*715*/SensorCall();*nextdiroff = 0;/*716*/}
				SensorCall();if (tif->tif_flags&TIFF_SWAB)
					{/*717*/SensorCall();TIFFSwabLong8(nextdiroff);/*718*/}
			}
		}
	} else {
		SensorCall();tmsize_t m;
		tmsize_t off = (tmsize_t) tif->tif_diroff;
		SensorCall();if ((uint64)off!=tif->tif_diroff)
		{
			SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Can not read TIFF directory count");
			SensorCall();return(0);
		}

		/*
		 * Check for integer overflow when validating the dir_off,
		 * otherwise a very high offset may cause an OOB read and
		 * crash the client. Make two comparisons instead of
		 *
		 *  off + sizeof(uint16) > tif->tif_size
		 *
		 * to avoid overflow.
		 */
		SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
		{
			SensorCall();m=off+sizeof(uint16);
			SensorCall();if ((m<off)||(m<(tmsize_t)sizeof(uint16))||(m>tif->tif_size)) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					"Can not read TIFF directory count");
				{uint16  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
			} else {
				SensorCall();_TIFFmemcpy(&dircount16, tif->tif_base + off,
					    sizeof(uint16));
			}
			SensorCall();off += sizeof (uint16);
			SensorCall();if (tif->tif_flags & TIFF_SWAB)
				{/*719*/SensorCall();TIFFSwabShort(&dircount16);/*720*/}
			SensorCall();if (dircount16>4096)
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				    "Sanity check on directory count failed, this is probably not a valid IFD offset");
				{uint16  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
			}
			SensorCall();dirsize = 12;
		}
		else
		{
			SensorCall();tmsize_t m;
			uint64 dircount64;
			m=off+sizeof(uint64);
			SensorCall();if ((m<off)||(m<(tmsize_t)sizeof(uint64))||(m>tif->tif_size)) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					"Can not read TIFF directory count");
				{uint16  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
			} else {
				SensorCall();_TIFFmemcpy(&dircount64, tif->tif_base + off,
					    sizeof(uint64));
			}
			SensorCall();off += sizeof (uint64);
			SensorCall();if (tif->tif_flags & TIFF_SWAB)
				{/*721*/SensorCall();TIFFSwabLong8(&dircount64);/*722*/}
			SensorCall();if (dircount64>4096)
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				    "Sanity check on directory count failed, this is probably not a valid IFD offset");
				{uint16  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
			}
			SensorCall();dircount16 = (uint16)dircount64;
			dirsize = 20;
		}
		SensorCall();if (dircount16 == 0 )
		{
			SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
			             "Sanity check on directory count failed, zero tag directories not supported");
			{uint16  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
		}
		SensorCall();origdir = _TIFFCheckMalloc(tif, dircount16,
						dirsize,
						"to read TIFF directory");
		SensorCall();if (origdir == NULL)
			{/*723*/{uint16  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*724*/}
		SensorCall();m=off+dircount16*dirsize;
		SensorCall();if ((m<off)||(m<(tmsize_t)(dircount16*dirsize))||(m>tif->tif_size)) {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				     "Can not read TIFF directory");
			_TIFFfree(origdir);
			{uint16  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
		} else {
			SensorCall();_TIFFmemcpy(origdir, tif->tif_base + off,
				    dircount16 * dirsize);
		}
		SensorCall();if (nextdiroff) {
			SensorCall();off += dircount16 * dirsize;
			SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
			{
				SensorCall();uint32 nextdiroff32;
				m=off+sizeof(uint32);
				SensorCall();if ((m<off)||(m<(tmsize_t)sizeof(uint32))||(m>tif->tif_size))
					{/*725*/SensorCall();nextdiroff32 = 0;/*726*/}
				else
					{/*727*/SensorCall();_TIFFmemcpy(&nextdiroff32, tif->tif_base + off,
						    sizeof (uint32));/*728*/}
				SensorCall();if (tif->tif_flags&TIFF_SWAB)
					{/*729*/SensorCall();TIFFSwabLong(&nextdiroff32);/*730*/}
				SensorCall();*nextdiroff = nextdiroff32;
			}
			else
			{
				SensorCall();m=off+sizeof(uint64);
				SensorCall();if ((m<off)||(m<(tmsize_t)sizeof(uint64))||(m>tif->tif_size))
					{/*731*/SensorCall();*nextdiroff = 0;/*732*/}
				else
					{/*733*/SensorCall();_TIFFmemcpy(nextdiroff, tif->tif_base + off,
						    sizeof (uint64));/*734*/}
				SensorCall();if (tif->tif_flags&TIFF_SWAB)
					{/*735*/SensorCall();TIFFSwabLong8(nextdiroff);/*736*/}
			}
		}
	}
	SensorCall();dir = (TIFFDirEntry*)_TIFFCheckMalloc(tif, dircount16,
						sizeof(TIFFDirEntry),
						"to read TIFF directory");
	SensorCall();if (dir==0)
	{
		SensorCall();_TIFFfree(origdir);
		{uint16  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}
	SensorCall();ma=(uint8*)origdir;
	mb=dir;
	SensorCall();for (n=0; n<dircount16; n++)
	{
		SensorCall();if (tif->tif_flags&TIFF_SWAB)
			{/*737*/SensorCall();TIFFSwabShort((uint16*)ma);/*738*/}
		SensorCall();mb->tdir_tag=*(uint16*)ma;
		ma+=sizeof(uint16);
		SensorCall();if (tif->tif_flags&TIFF_SWAB)
			{/*739*/SensorCall();TIFFSwabShort((uint16*)ma);/*740*/}
		SensorCall();mb->tdir_type=*(uint16*)ma;
		ma+=sizeof(uint16);
		SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
		{
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*741*/SensorCall();TIFFSwabLong((uint32*)ma);/*742*/}
			SensorCall();mb->tdir_count=(uint64)(*(uint32*)ma);
			ma+=sizeof(uint32);
			*(uint32*)(&mb->tdir_offset)=*(uint32*)ma;
			ma+=sizeof(uint32);
		}
		else
		{
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*743*/SensorCall();TIFFSwabLong8((uint64*)ma);/*744*/}
                        SensorCall();mb->tdir_count=TIFFReadUInt64(ma);
			ma+=sizeof(uint64);
			mb->tdir_offset.toff_long8=TIFFReadUInt64(ma);
			ma+=sizeof(uint64);
		}
		SensorCall();mb++;
	}
	SensorCall();_TIFFfree(origdir);
	*pdir = dir;
	{uint16  ReplaceReturn = dircount16; SensorCall(); return ReplaceReturn;}
}

/*
 * Fetch a tag that is not handled by special case code.
 */
static int
TIFFFetchNormalTag(TIFF* tif, TIFFDirEntry* dp, int recover)
{
	SensorCall();static const char module[] = "TIFFFetchNormalTag";
	enum TIFFReadDirEntryErr err;
	uint32 fii;
	const TIFFField* fip = NULL;
	TIFFReadDirectoryFindFieldInfo(tif,dp->tdir_tag,&fii);
        SensorCall();if( fii == FAILED_FII )
        {
            SensorCall();TIFFErrorExt(tif->tif_clientdata, "TIFFFetchNormalTag",
                         "No definition found for tag %d",
                         dp->tdir_tag);
            {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
        }
	SensorCall();fip=tif->tif_fields[fii];
	assert(fip->set_field_type!=TIFF_SETGET_OTHER);  /* if so, we shouldn't arrive here but deal with this in specialized code */
	assert(fip->set_field_type!=TIFF_SETGET_INT);    /* if so, we shouldn't arrive here as this is only the case for pseudo-tags */
	err=TIFFReadDirEntryErrOk;
	SensorCall();switch (fip->set_field_type)
	{
		case TIFF_SETGET_UNDEFINED:
			SensorCall();break;
		case TIFF_SETGET_ASCII:
			{
				SensorCall();uint8* data;
				assert(fip->field_passcount==0);
				err=TIFFReadDirEntryByteArray(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();uint8* ma;
					uint32 mb;
					int n;
					ma=data;
					mb=0;
					SensorCall();while (mb<(uint32)dp->tdir_count)
					{
						SensorCall();if (*ma==0)
							{/*745*/SensorCall();break;/*746*/}
						SensorCall();ma++;
						mb++;
					}
					SensorCall();if (mb+1<(uint32)dp->tdir_count)
						{/*747*/SensorCall();TIFFWarningExt(tif->tif_clientdata,module,"ASCII value for tag \"%s\" contains null byte in value; value incorrectly truncated during reading due to implementation limitations",fip->field_name);/*748*/}
					else {/*749*/SensorCall();if (mb+1>(uint32)dp->tdir_count)
					{
						SensorCall();uint8* o;
						TIFFWarningExt(tif->tif_clientdata,module,"ASCII value for tag \"%s\" does not end in null byte",fip->field_name);
						SensorCall();if ((uint32)dp->tdir_count+1!=dp->tdir_count+1)
							o=NULL;
						else
							{/*751*/SensorCall();o=_TIFFmalloc((uint32)dp->tdir_count+1);/*752*/}
						SensorCall();if (o==NULL)
						{
							SensorCall();if (data!=NULL)
								{/*753*/SensorCall();_TIFFfree(data);/*754*/}
							SensorCall();return(0);
						}
						SensorCall();_TIFFmemcpy(o,data,(uint32)dp->tdir_count);
						o[(uint32)dp->tdir_count]=0;
						SensorCall();if (data!=0)
							{/*755*/SensorCall();_TIFFfree(data);/*756*/}
						SensorCall();data=o;
					;/*750*/}}
					SensorCall();n=TIFFSetField(tif,dp->tdir_tag,data);
					SensorCall();if (data!=0)
						{/*757*/SensorCall();_TIFFfree(data);/*758*/}
					SensorCall();if (!n)
						{/*759*/SensorCall();return(0);/*760*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_UINT8:
			{
				SensorCall();uint8 data=0;
				assert(fip->field_readcount==1);
				assert(fip->field_passcount==0);
				err=TIFFReadDirEntryByte(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();if (!TIFFSetField(tif,dp->tdir_tag,data))
						{/*761*/SensorCall();return(0);/*762*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_UINT16:
			{
				SensorCall();uint16 data;
				assert(fip->field_readcount==1);
				assert(fip->field_passcount==0);
				err=TIFFReadDirEntryShort(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();if (!TIFFSetField(tif,dp->tdir_tag,data))
						{/*763*/SensorCall();return(0);/*764*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_UINT32:
			{
				SensorCall();uint32 data;
				assert(fip->field_readcount==1);
				assert(fip->field_passcount==0);
				err=TIFFReadDirEntryLong(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();if (!TIFFSetField(tif,dp->tdir_tag,data))
						{/*765*/SensorCall();return(0);/*766*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_UINT64:
			{
				SensorCall();uint64 data;
				assert(fip->field_readcount==1);
				assert(fip->field_passcount==0);
				err=TIFFReadDirEntryLong8(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();if (!TIFFSetField(tif,dp->tdir_tag,data))
						{/*767*/SensorCall();return(0);/*768*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_FLOAT:
			{
				SensorCall();float data;
				assert(fip->field_readcount==1);
				assert(fip->field_passcount==0);
				err=TIFFReadDirEntryFloat(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();if (!TIFFSetField(tif,dp->tdir_tag,data))
						{/*769*/SensorCall();return(0);/*770*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_DOUBLE:
			{
				SensorCall();double data;
				assert(fip->field_readcount==1);
				assert(fip->field_passcount==0);
				err=TIFFReadDirEntryDouble(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();if (!TIFFSetField(tif,dp->tdir_tag,data))
						{/*771*/SensorCall();return(0);/*772*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_IFD8:
			{
				SensorCall();uint64 data;
				assert(fip->field_readcount==1);
				assert(fip->field_passcount==0);
				err=TIFFReadDirEntryIfd8(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();if (!TIFFSetField(tif,dp->tdir_tag,data))
						{/*773*/SensorCall();return(0);/*774*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_UINT16_PAIR:
			{
				SensorCall();uint16* data;
				assert(fip->field_readcount==2);
				assert(fip->field_passcount==0);
				SensorCall();if (dp->tdir_count!=2) {
					SensorCall();TIFFWarningExt(tif->tif_clientdata,module,
						       "incorrect count for field \"%s\", expected 2, got %d",
						       fip->field_name,(int)dp->tdir_count);
					SensorCall();return(0);
				}
				SensorCall();err=TIFFReadDirEntryShortArray(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();int m;
					m=TIFFSetField(tif,dp->tdir_tag,data[0],data[1]);
					_TIFFfree(data);
					SensorCall();if (!m)
						{/*775*/SensorCall();return(0);/*776*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C0_UINT8:
			{
				SensorCall();uint8* data;
				assert(fip->field_readcount>=1);
				assert(fip->field_passcount==0);
				SensorCall();if (dp->tdir_count!=(uint64)fip->field_readcount) {
					SensorCall();TIFFWarningExt(tif->tif_clientdata,module,
						       "incorrect count for field \"%s\", expected %d, got %d",
						       fip->field_name,(int) fip->field_readcount, (int)dp->tdir_count);
					{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
				}
				else
				{
					SensorCall();err=TIFFReadDirEntryByteArray(tif,dp,&data);
					SensorCall();if (err==TIFFReadDirEntryErrOk)
					{
						SensorCall();int m;
						m=TIFFSetField(tif,dp->tdir_tag,data);
						SensorCall();if (data!=0)
							{/*777*/SensorCall();_TIFFfree(data);/*778*/}
						SensorCall();if (!m)
							{/*779*/SensorCall();return(0);/*780*/}
					}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C0_UINT16:
			{
				SensorCall();uint16* data;
				assert(fip->field_readcount>=1);
				assert(fip->field_passcount==0);
				SensorCall();if (dp->tdir_count!=(uint64)fip->field_readcount)
                                    /* corrupt file */;
				else
				{
					SensorCall();err=TIFFReadDirEntryShortArray(tif,dp,&data);
					SensorCall();if (err==TIFFReadDirEntryErrOk)
					{
						SensorCall();int m;
						m=TIFFSetField(tif,dp->tdir_tag,data);
						SensorCall();if (data!=0)
							{/*783*/SensorCall();_TIFFfree(data);/*784*/}
						SensorCall();if (!m)
							{/*785*/SensorCall();return(0);/*786*/}
					}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C0_UINT32:
			{
				SensorCall();uint32* data;
				assert(fip->field_readcount>=1);
				assert(fip->field_passcount==0);
				SensorCall();if (dp->tdir_count!=(uint64)fip->field_readcount)
                                    /* corrupt file */;
				else
				{
					SensorCall();err=TIFFReadDirEntryLongArray(tif,dp,&data);
					SensorCall();if (err==TIFFReadDirEntryErrOk)
					{
						SensorCall();int m;
						m=TIFFSetField(tif,dp->tdir_tag,data);
						SensorCall();if (data!=0)
							{/*789*/SensorCall();_TIFFfree(data);/*790*/}
						SensorCall();if (!m)
							{/*791*/SensorCall();return(0);/*792*/}
					}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C0_FLOAT:
			{
				SensorCall();float* data;
				assert(fip->field_readcount>=1);
				assert(fip->field_passcount==0);
				SensorCall();if (dp->tdir_count!=(uint64)fip->field_readcount)
                                    /* corrupt file */;
				else
				{
					SensorCall();err=TIFFReadDirEntryFloatArray(tif,dp,&data);
					SensorCall();if (err==TIFFReadDirEntryErrOk)
					{
						SensorCall();int m;
						m=TIFFSetField(tif,dp->tdir_tag,data);
						SensorCall();if (data!=0)
							{/*795*/SensorCall();_TIFFfree(data);/*796*/}
						SensorCall();if (!m)
							{/*797*/SensorCall();return(0);/*798*/}
					}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C16_ASCII:
			{
				SensorCall();uint8* data;
				assert(fip->field_readcount==TIFF_VARIABLE);
				assert(fip->field_passcount==1);
				SensorCall();if (dp->tdir_count>0xFFFF)
					{/*799*/SensorCall();err=TIFFReadDirEntryErrCount;/*800*/}
				else
				{
					SensorCall();err=TIFFReadDirEntryByteArray(tif,dp,&data);
					SensorCall();if (err==TIFFReadDirEntryErrOk)
					{
						SensorCall();int m;
						m=TIFFSetField(tif,dp->tdir_tag,(uint16)(dp->tdir_count),data);
						SensorCall();if (data!=0)
							{/*801*/SensorCall();_TIFFfree(data);/*802*/}
						SensorCall();if (!m)
							{/*803*/SensorCall();return(0);/*804*/}
					}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C16_UINT8:
			{
				SensorCall();uint8* data;
				assert(fip->field_readcount==TIFF_VARIABLE);
				assert(fip->field_passcount==1);
				SensorCall();if (dp->tdir_count>0xFFFF)
					{/*805*/SensorCall();err=TIFFReadDirEntryErrCount;/*806*/}
				else
				{
					SensorCall();err=TIFFReadDirEntryByteArray(tif,dp,&data);
					SensorCall();if (err==TIFFReadDirEntryErrOk)
					{
						SensorCall();int m;
						m=TIFFSetField(tif,dp->tdir_tag,(uint16)(dp->tdir_count),data);
						SensorCall();if (data!=0)
							{/*807*/SensorCall();_TIFFfree(data);/*808*/}
						SensorCall();if (!m)
							{/*809*/SensorCall();return(0);/*810*/}
					}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C16_UINT16:
			{
				SensorCall();uint16* data;
				assert(fip->field_readcount==TIFF_VARIABLE);
				assert(fip->field_passcount==1);
				SensorCall();if (dp->tdir_count>0xFFFF)
					{/*811*/SensorCall();err=TIFFReadDirEntryErrCount;/*812*/}
				else
				{
					SensorCall();err=TIFFReadDirEntryShortArray(tif,dp,&data);
					SensorCall();if (err==TIFFReadDirEntryErrOk)
					{
						SensorCall();int m;
						m=TIFFSetField(tif,dp->tdir_tag,(uint16)(dp->tdir_count),data);
						SensorCall();if (data!=0)
							{/*813*/SensorCall();_TIFFfree(data);/*814*/}
						SensorCall();if (!m)
							{/*815*/SensorCall();return(0);/*816*/}
					}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C16_UINT32:
			{
				SensorCall();uint32* data;
				assert(fip->field_readcount==TIFF_VARIABLE);
				assert(fip->field_passcount==1);
				SensorCall();if (dp->tdir_count>0xFFFF)
					{/*817*/SensorCall();err=TIFFReadDirEntryErrCount;/*818*/}
				else
				{
					SensorCall();err=TIFFReadDirEntryLongArray(tif,dp,&data);
					SensorCall();if (err==TIFFReadDirEntryErrOk)
					{
						SensorCall();int m;
						m=TIFFSetField(tif,dp->tdir_tag,(uint16)(dp->tdir_count),data);
						SensorCall();if (data!=0)
							{/*819*/SensorCall();_TIFFfree(data);/*820*/}
						SensorCall();if (!m)
							{/*821*/SensorCall();return(0);/*822*/}
					}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C16_UINT64:
			{
				SensorCall();uint64* data;
				assert(fip->field_readcount==TIFF_VARIABLE);
				assert(fip->field_passcount==1);
				SensorCall();if (dp->tdir_count>0xFFFF)
					{/*823*/SensorCall();err=TIFFReadDirEntryErrCount;/*824*/}
				else
				{
					SensorCall();err=TIFFReadDirEntryLong8Array(tif,dp,&data);
					SensorCall();if (err==TIFFReadDirEntryErrOk)
					{
						SensorCall();int m;
						m=TIFFSetField(tif,dp->tdir_tag,(uint16)(dp->tdir_count),data);
						SensorCall();if (data!=0)
							{/*825*/SensorCall();_TIFFfree(data);/*826*/}
						SensorCall();if (!m)
							{/*827*/SensorCall();return(0);/*828*/}
					}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C16_FLOAT:
			{
				SensorCall();float* data;
				assert(fip->field_readcount==TIFF_VARIABLE);
				assert(fip->field_passcount==1);
				SensorCall();if (dp->tdir_count>0xFFFF)
					{/*829*/SensorCall();err=TIFFReadDirEntryErrCount;/*830*/}
				else
				{
					SensorCall();err=TIFFReadDirEntryFloatArray(tif,dp,&data);
					SensorCall();if (err==TIFFReadDirEntryErrOk)
					{
						SensorCall();int m;
						m=TIFFSetField(tif,dp->tdir_tag,(uint16)(dp->tdir_count),data);
						SensorCall();if (data!=0)
							{/*831*/SensorCall();_TIFFfree(data);/*832*/}
						SensorCall();if (!m)
							{/*833*/SensorCall();return(0);/*834*/}
					}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C16_DOUBLE:
			{
				SensorCall();double* data;
				assert(fip->field_readcount==TIFF_VARIABLE);
				assert(fip->field_passcount==1);
				SensorCall();if (dp->tdir_count>0xFFFF)
					{/*835*/SensorCall();err=TIFFReadDirEntryErrCount;/*836*/}
				else
				{
					SensorCall();err=TIFFReadDirEntryDoubleArray(tif,dp,&data);
					SensorCall();if (err==TIFFReadDirEntryErrOk)
					{
						SensorCall();int m;
						m=TIFFSetField(tif,dp->tdir_tag,(uint16)(dp->tdir_count),data);
						SensorCall();if (data!=0)
							{/*837*/SensorCall();_TIFFfree(data);/*838*/}
						SensorCall();if (!m)
							{/*839*/SensorCall();return(0);/*840*/}
					}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C16_IFD8:
			{
				SensorCall();uint64* data;
				assert(fip->field_readcount==TIFF_VARIABLE);
				assert(fip->field_passcount==1);
				SensorCall();if (dp->tdir_count>0xFFFF)
					{/*841*/SensorCall();err=TIFFReadDirEntryErrCount;/*842*/}
				else
				{
					SensorCall();err=TIFFReadDirEntryIfd8Array(tif,dp,&data);
					SensorCall();if (err==TIFFReadDirEntryErrOk)
					{
						SensorCall();int m;
						m=TIFFSetField(tif,dp->tdir_tag,(uint16)(dp->tdir_count),data);
						SensorCall();if (data!=0)
							{/*843*/SensorCall();_TIFFfree(data);/*844*/}
						SensorCall();if (!m)
							{/*845*/SensorCall();return(0);/*846*/}
					}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C32_ASCII:
			{
				SensorCall();uint8* data;
				assert(fip->field_readcount==TIFF_VARIABLE2);
				assert(fip->field_passcount==1);
				err=TIFFReadDirEntryByteArray(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();int m;
					m=TIFFSetField(tif,dp->tdir_tag,(uint32)(dp->tdir_count),data);
					SensorCall();if (data!=0)
						{/*847*/SensorCall();_TIFFfree(data);/*848*/}
					SensorCall();if (!m)
						{/*849*/SensorCall();return(0);/*850*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C32_UINT8:
			{
				SensorCall();uint8* data;
				assert(fip->field_readcount==TIFF_VARIABLE2);
				assert(fip->field_passcount==1);
				err=TIFFReadDirEntryByteArray(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();int m;
					m=TIFFSetField(tif,dp->tdir_tag,(uint32)(dp->tdir_count),data);
					SensorCall();if (data!=0)
						{/*851*/SensorCall();_TIFFfree(data);/*852*/}
					SensorCall();if (!m)
						{/*853*/SensorCall();return(0);/*854*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C32_SINT8:
			{
				SensorCall();int8* data = NULL;
				assert(fip->field_readcount==TIFF_VARIABLE2);
				assert(fip->field_passcount==1);
				err=TIFFReadDirEntrySbyteArray(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();int m;
					m=TIFFSetField(tif,dp->tdir_tag,(uint32)(dp->tdir_count),data);
					SensorCall();if (data!=0)
						{/*855*/SensorCall();_TIFFfree(data);/*856*/}
					SensorCall();if (!m)
						{/*857*/SensorCall();return(0);/*858*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C32_UINT16:
			{
				SensorCall();uint16* data;
				assert(fip->field_readcount==TIFF_VARIABLE2);
				assert(fip->field_passcount==1);
				err=TIFFReadDirEntryShortArray(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();int m;
					m=TIFFSetField(tif,dp->tdir_tag,(uint32)(dp->tdir_count),data);
					SensorCall();if (data!=0)
						{/*859*/SensorCall();_TIFFfree(data);/*860*/}
					SensorCall();if (!m)
						{/*861*/SensorCall();return(0);/*862*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C32_SINT16:
			{
				SensorCall();int16* data = NULL;
				assert(fip->field_readcount==TIFF_VARIABLE2);
				assert(fip->field_passcount==1);
				err=TIFFReadDirEntrySshortArray(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();int m;
					m=TIFFSetField(tif,dp->tdir_tag,(uint32)(dp->tdir_count),data);
					SensorCall();if (data!=0)
						{/*863*/SensorCall();_TIFFfree(data);/*864*/}
					SensorCall();if (!m)
						{/*865*/SensorCall();return(0);/*866*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C32_UINT32:
			{
				SensorCall();uint32* data;
				assert(fip->field_readcount==TIFF_VARIABLE2);
				assert(fip->field_passcount==1);
				err=TIFFReadDirEntryLongArray(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();int m;
					m=TIFFSetField(tif,dp->tdir_tag,(uint32)(dp->tdir_count),data);
					SensorCall();if (data!=0)
						{/*867*/SensorCall();_TIFFfree(data);/*868*/}
					SensorCall();if (!m)
						{/*869*/SensorCall();return(0);/*870*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C32_SINT32:
			{
				SensorCall();int32* data = NULL;
				assert(fip->field_readcount==TIFF_VARIABLE2);
				assert(fip->field_passcount==1);
				err=TIFFReadDirEntrySlongArray(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();int m;
					m=TIFFSetField(tif,dp->tdir_tag,(uint32)(dp->tdir_count),data);
					SensorCall();if (data!=0)
						{/*871*/SensorCall();_TIFFfree(data);/*872*/}
					SensorCall();if (!m)
						{/*873*/SensorCall();return(0);/*874*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C32_UINT64:
			{
				SensorCall();uint64* data;
				assert(fip->field_readcount==TIFF_VARIABLE2);
				assert(fip->field_passcount==1);
				err=TIFFReadDirEntryLong8Array(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();int m;
					m=TIFFSetField(tif,dp->tdir_tag,(uint32)(dp->tdir_count),data);
					SensorCall();if (data!=0)
						{/*875*/SensorCall();_TIFFfree(data);/*876*/}
					SensorCall();if (!m)
						{/*877*/SensorCall();return(0);/*878*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C32_SINT64:
			{
				SensorCall();int64* data = NULL;
				assert(fip->field_readcount==TIFF_VARIABLE2);
				assert(fip->field_passcount==1);
				err=TIFFReadDirEntrySlong8Array(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();int m;
					m=TIFFSetField(tif,dp->tdir_tag,(uint32)(dp->tdir_count),data);
					SensorCall();if (data!=0)
						{/*879*/SensorCall();_TIFFfree(data);/*880*/}
					SensorCall();if (!m)
						{/*881*/SensorCall();return(0);/*882*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C32_FLOAT:
			{
				SensorCall();float* data;
				assert(fip->field_readcount==TIFF_VARIABLE2);
				assert(fip->field_passcount==1);
				err=TIFFReadDirEntryFloatArray(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();int m;
					m=TIFFSetField(tif,dp->tdir_tag,(uint32)(dp->tdir_count),data);
					SensorCall();if (data!=0)
						{/*883*/SensorCall();_TIFFfree(data);/*884*/}
					SensorCall();if (!m)
						{/*885*/SensorCall();return(0);/*886*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C32_DOUBLE:
			{
				SensorCall();double* data;
				assert(fip->field_readcount==TIFF_VARIABLE2);
				assert(fip->field_passcount==1);
				err=TIFFReadDirEntryDoubleArray(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();int m;
					m=TIFFSetField(tif,dp->tdir_tag,(uint32)(dp->tdir_count),data);
					SensorCall();if (data!=0)
						{/*887*/SensorCall();_TIFFfree(data);/*888*/}
					SensorCall();if (!m)
						{/*889*/SensorCall();return(0);/*890*/}
				}
			}
			SensorCall();break;
		case TIFF_SETGET_C32_IFD8:
			{
				SensorCall();uint64* data;
				assert(fip->field_readcount==TIFF_VARIABLE2);
				assert(fip->field_passcount==1);
				err=TIFFReadDirEntryIfd8Array(tif,dp,&data);
				SensorCall();if (err==TIFFReadDirEntryErrOk)
				{
					SensorCall();int m;
					m=TIFFSetField(tif,dp->tdir_tag,(uint32)(dp->tdir_count),data);
					SensorCall();if (data!=0)
						{/*891*/SensorCall();_TIFFfree(data);/*892*/}
					SensorCall();if (!m)
						{/*893*/SensorCall();return(0);/*894*/}
				}
			}
			SensorCall();break;
		default:
			assert(0);    /* we should never get here */
			SensorCall();break;
	}
	SensorCall();if (err!=TIFFReadDirEntryErrOk)
	{
		SensorCall();TIFFReadDirEntryOutputErr(tif,err,module,fip ? fip->field_name : "unknown tagname",recover);
		SensorCall();return(0);
	}
	SensorCall();return(1);
}

/*
 * Fetch a set of offsets or lengths.
 * While this routine says "strips", in fact it's also used for tiles.
 */
static int
TIFFFetchStripThing(TIFF* tif, TIFFDirEntry* dir, uint32 nstrips, uint64** lpp)
{
	SensorCall();static const char module[] = "TIFFFetchStripThing";
	enum TIFFReadDirEntryErr err;
	uint64* data;
	err=TIFFReadDirEntryLong8Array(tif,dir,&data);
	SensorCall();if (err!=TIFFReadDirEntryErrOk)
	{
		SensorCall();const TIFFField* fip = TIFFFieldWithTag(tif,dir->tdir_tag); 
		TIFFReadDirEntryOutputErr(tif,err,module,fip ? fip->field_name : "unknown tagname",0);
		SensorCall();return(0);
	}
	SensorCall();if (dir->tdir_count!=(uint64)nstrips)
	{
		SensorCall();uint64* resizeddata;
		resizeddata=(uint64*)_TIFFCheckMalloc(tif,nstrips,sizeof(uint64),"for strip array");
		SensorCall();if (resizeddata==0) {
			SensorCall();_TIFFfree(data);
			SensorCall();return(0);
		}
		SensorCall();if (dir->tdir_count<(uint64)nstrips)
		{
			SensorCall();_TIFFmemcpy(resizeddata,data,(uint32)dir->tdir_count*sizeof(uint64));
			_TIFFmemset(resizeddata+(uint32)dir->tdir_count,0,(nstrips-(uint32)dir->tdir_count)*sizeof(uint64));
		}
		else
			{/*895*/SensorCall();_TIFFmemcpy(resizeddata,data,nstrips*sizeof(uint64));/*896*/}
		SensorCall();_TIFFfree(data);
		data=resizeddata;
	}
	SensorCall();*lpp=data;
	SensorCall();return(1);
}

/*
 * Fetch and set the SubjectDistance EXIF tag.
 */
static int
TIFFFetchSubjectDistance(TIFF* tif, TIFFDirEntry* dir)
{
	SensorCall();static const char module[] = "TIFFFetchSubjectDistance";
	enum TIFFReadDirEntryErr err;
	UInt64Aligned_t m;
    m.l=0;
	assert(sizeof(double)==8);
	assert(sizeof(uint64)==8);
	assert(sizeof(uint32)==4);
	SensorCall();if (dir->tdir_count!=1)
		{/*897*/SensorCall();err=TIFFReadDirEntryErrCount;/*898*/}
	else {/*899*/SensorCall();if (dir->tdir_type!=TIFF_RATIONAL)
		{/*901*/SensorCall();err=TIFFReadDirEntryErrType;/*902*/}
	else
	{
		SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
		{
			SensorCall();uint32 offset;
			offset=*(uint32*)(&dir->tdir_offset);
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*903*/SensorCall();TIFFSwabLong(&offset);/*904*/}
			SensorCall();err=TIFFReadDirEntryData(tif,offset,8,m.i);
		}
		else
		{
			SensorCall();m.l=dir->tdir_offset.toff_long8;
			err=TIFFReadDirEntryErrOk;
		}
	;/*900*/}}
	SensorCall();if (err==TIFFReadDirEntryErrOk)
	{
		SensorCall();double n;
		SensorCall();if (tif->tif_flags&TIFF_SWAB)
			{/*905*/SensorCall();TIFFSwabArrayOfLong(m.i,2);/*906*/}
		SensorCall();if (m.i[0]==0)
			{/*907*/SensorCall();n=0.0;/*908*/}
		else {/*909*/SensorCall();if (m.i[0]==0xFFFFFFFF)
			/*
			 * XXX: Numerator 0xFFFFFFFF means that we have infinite
			 * distance. Indicate that with a negative floating point
			 * SubjectDistance value.
			 */
			{/*911*/SensorCall();n=-1.0;/*912*/}
		else
			{/*913*/SensorCall();n=(double)m.i[0]/(double)m.i[1];/*914*/}/*910*/}
		SensorCall();return(TIFFSetField(tif,dir->tdir_tag,n));
	}
	else
	{
		SensorCall();TIFFReadDirEntryOutputErr(tif,err,module,"SubjectDistance",TRUE);
		SensorCall();return(0);
	}
SensorCall();}

/*
 * Replace a single strip (tile) of uncompressed data by multiple strips
 * (tiles), each approximately STRIP_SIZE_DEFAULT bytes. This is useful for
 * dealing with large images or for dealing with machines with a limited
 * amount memory.
 */
static void
ChopUpSingleUncompressedStrip(TIFF* tif)
{
	SensorCall();register TIFFDirectory *td = &tif->tif_dir;
	uint64 bytecount;
	uint64 offset;
	uint32 rowblock;
	uint64 rowblockbytes;
	uint64 stripbytes;
	uint32 strip;
	uint64 nstrips64;
	uint32 nstrips32;
	uint32 rowsperstrip;
	uint64* newcounts;
	uint64* newoffsets;

	bytecount = td->td_stripbytecount[0];
	offset = td->td_stripoffset[0];
	assert(td->td_planarconfig == PLANARCONFIG_CONTIG);
	SensorCall();if ((td->td_photometric == PHOTOMETRIC_YCBCR)&&
	    (!isUpSampled(tif)))
		{/*915*/SensorCall();rowblock = td->td_ycbcrsubsampling[1];/*916*/}
	else
		{/*917*/SensorCall();rowblock = 1;/*918*/}
	SensorCall();rowblockbytes = TIFFVTileSize64(tif, rowblock);
	/*
	 * Make the rows hold at least one scanline, but fill specified amount
	 * of data if possible.
	 */
	SensorCall();if (rowblockbytes > STRIP_SIZE_DEFAULT) {
		SensorCall();stripbytes = rowblockbytes;
		rowsperstrip = rowblock;
	} else {/*919*/SensorCall();if (rowblockbytes > 0 ) {
		SensorCall();uint32 rowblocksperstrip;
		rowblocksperstrip = (uint32) (STRIP_SIZE_DEFAULT / rowblockbytes);
		rowsperstrip = rowblocksperstrip * rowblock;
		stripbytes = rowblocksperstrip * rowblockbytes;
	}
	else
	    {/*921*/SensorCall();/*920*/}return;/*922*/}

	/*
	 * never increase the number of strips in an image
	 */
	SensorCall();if (rowsperstrip >= td->td_rowsperstrip)
		{/*923*/SensorCall();return;/*924*/}
	SensorCall();nstrips64 = TIFFhowmany_64(bytecount, stripbytes);
	SensorCall();if ((nstrips64==0)||(nstrips64>0xFFFFFFFF)) /* something is wonky, do nothing. */
	    {/*925*/SensorCall();return;/*926*/}
	SensorCall();nstrips32 = (uint32)nstrips64;

	newcounts = (uint64*) _TIFFCheckMalloc(tif, nstrips32, sizeof (uint64),
				"for chopped \"StripByteCounts\" array");
	newoffsets = (uint64*) _TIFFCheckMalloc(tif, nstrips32, sizeof (uint64),
				"for chopped \"StripOffsets\" array");
	SensorCall();if (newcounts == NULL || newoffsets == NULL) {
		/*
		 * Unable to allocate new strip information, give up and use
		 * the original one strip information.
		 */
		SensorCall();if (newcounts != NULL)
			{/*927*/SensorCall();_TIFFfree(newcounts);/*928*/}
		SensorCall();if (newoffsets != NULL)
			{/*929*/SensorCall();_TIFFfree(newoffsets);/*930*/}
		SensorCall();return;
	}
	/*
	 * Fill the strip information arrays with new bytecounts and offsets
	 * that reflect the broken-up format.
	 */
	SensorCall();for (strip = 0; strip < nstrips32; strip++) {
		SensorCall();if (stripbytes > bytecount)
			{/*931*/SensorCall();stripbytes = bytecount;/*932*/}
		SensorCall();newcounts[strip] = stripbytes;
		newoffsets[strip] = offset;
		offset += stripbytes;
		bytecount -= stripbytes;
	}
	/*
	 * Replace old single strip info with multi-strip info.
	 */
	SensorCall();td->td_stripsperimage = td->td_nstrips = nstrips32;
	TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, rowsperstrip);

	_TIFFfree(td->td_stripbytecount);
	_TIFFfree(td->td_stripoffset);
	td->td_stripbytecount = newcounts;
	td->td_stripoffset = newoffsets;
	td->td_stripbytecountsorted = 1;
SensorCall();}

int _TIFFFillStriles( TIFF *tif )
{
#if defined(DEFER_STRILE_LOAD)
        register TIFFDirectory *td = &tif->tif_dir;
        int return_value = 1;

        if( td->td_stripoffset != NULL )
                return 1;

        if( td->td_stripoffset_entry.tdir_count == 0 )
                return 0;

        if (!TIFFFetchStripThing(tif,&(td->td_stripoffset_entry),
                                 td->td_nstrips,&td->td_stripoffset))
        {
                return_value = 0;
        }

        if (!TIFFFetchStripThing(tif,&(td->td_stripbytecount_entry),
                                 td->td_nstrips,&td->td_stripbytecount))
        {
                return_value = 0;
        }

        _TIFFmemset( &(td->td_stripoffset_entry), 0, sizeof(TIFFDirEntry));
        _TIFFmemset( &(td->td_stripbytecount_entry), 0, sizeof(TIFFDirEntry));

	if (tif->tif_dir.td_nstrips > 1 && return_value == 1 ) {
		uint32 strip;

		tif->tif_dir.td_stripbytecountsorted = 1;
		for (strip = 1; strip < tif->tif_dir.td_nstrips; strip++) {
			if (tif->tif_dir.td_stripoffset[strip - 1] >
			    tif->tif_dir.td_stripoffset[strip]) {
				tif->tif_dir.td_stripbytecountsorted = 0;
				break;
			}
		}
	}

        return return_value;
#else /* !defined(DEFER_STRILE_LOAD) */
        SensorCall();(void) tif;
        {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
#endif 
}


/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
