/* $Id: tif_predict.c,v 1.32 2010-03-10 18:56:49 bfriesen Exp $ */

/*
 * Copyright (c) 1988-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

/*
 * TIFF Library.
 *
 * Predictor Tag Support (used by multiple codecs).
 */
#include "tiffiop.h"
#include "/var/tmp/sensor.h"
#include "tif_predict.h"

#define	PredictorState(tif)	((TIFFPredictorState*) (tif)->tif_data)

static void horAcc8(TIFF* tif, uint8* cp0, tmsize_t cc);
static void horAcc16(TIFF* tif, uint8* cp0, tmsize_t cc);
static void horAcc32(TIFF* tif, uint8* cp0, tmsize_t cc);
static void swabHorAcc16(TIFF* tif, uint8* cp0, tmsize_t cc);
static void swabHorAcc32(TIFF* tif, uint8* cp0, tmsize_t cc);
static void horDiff8(TIFF* tif, uint8* cp0, tmsize_t cc);
static void horDiff16(TIFF* tif, uint8* cp0, tmsize_t cc);
static void horDiff32(TIFF* tif, uint8* cp0, tmsize_t cc);
static void fpAcc(TIFF* tif, uint8* cp0, tmsize_t cc);
static void fpDiff(TIFF* tif, uint8* cp0, tmsize_t cc);
static int PredictorDecodeRow(TIFF* tif, uint8* op0, tmsize_t occ0, uint16 s);
static int PredictorDecodeTile(TIFF* tif, uint8* op0, tmsize_t occ0, uint16 s);
static int PredictorEncodeRow(TIFF* tif, uint8* bp, tmsize_t cc, uint16 s);
static int PredictorEncodeTile(TIFF* tif, uint8* bp0, tmsize_t cc0, uint16 s);

static int
PredictorSetup(TIFF* tif)
{
	SensorCall();static const char module[] = "PredictorSetup";

	TIFFPredictorState* sp = PredictorState(tif);
	TIFFDirectory* td = &tif->tif_dir;

	SensorCall();switch (sp->predictor)		/* no differencing */
	{
		case PREDICTOR_NONE:
			{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
		case PREDICTOR_HORIZONTAL:
			SensorCall();if (td->td_bitspersample != 8
			    && td->td_bitspersample != 16
			    && td->td_bitspersample != 32) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				    "Horizontal differencing \"Predictor\" not supported with %d-bit samples",
				    td->td_bitspersample);
				{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
			}
			SensorCall();break;
		case PREDICTOR_FLOATINGPOINT:
			SensorCall();if (td->td_sampleformat != SAMPLEFORMAT_IEEEFP) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				    "Floating point \"Predictor\" not supported with %d data format",
				    td->td_sampleformat);
				{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
			}
			SensorCall();break;
		default:
			SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
			    "\"Predictor\" value %d not supported",
			    sp->predictor);
			{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}
	SensorCall();sp->stride = (td->td_planarconfig == PLANARCONFIG_CONTIG ?
	    td->td_samplesperpixel : 1);
	/*
	 * Calculate the scanline/tile-width size in bytes.
	 */
	SensorCall();if (isTiled(tif))
		{/*13*/SensorCall();sp->rowsize = TIFFTileRowSize(tif);/*14*/}
	else
		{/*15*/SensorCall();sp->rowsize = TIFFScanlineSize(tif);/*16*/}
	SensorCall();if (sp->rowsize == 0)
		{/*17*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*18*/}

	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

static int
PredictorSetupDecode(TIFF* tif)
{
	SensorCall();TIFFPredictorState* sp = PredictorState(tif);
	TIFFDirectory* td = &tif->tif_dir;

	SensorCall();if (!(*sp->setupdecode)(tif) || !PredictorSetup(tif))
		{/*19*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*20*/}

	SensorCall();if (sp->predictor == 2) {
		SensorCall();switch (td->td_bitspersample) {
			case 8:  SensorCall();sp->decodepfunc = horAcc8; SensorCall();break;
			case 16: SensorCall();sp->decodepfunc = horAcc16; SensorCall();break;
			case 32: SensorCall();sp->decodepfunc = horAcc32; SensorCall();break;
		}
		/*
		 * Override default decoding method with one that does the
		 * predictor stuff.
		 */
                SensorCall();if( tif->tif_decoderow != PredictorDecodeRow )
                {
                    SensorCall();sp->decoderow = tif->tif_decoderow;
                    tif->tif_decoderow = PredictorDecodeRow;
                    sp->decodestrip = tif->tif_decodestrip;
                    tif->tif_decodestrip = PredictorDecodeTile;
                    sp->decodetile = tif->tif_decodetile;
                    tif->tif_decodetile = PredictorDecodeTile;
                }

		/*
		 * If the data is horizontally differenced 16-bit data that
		 * requires byte-swapping, then it must be byte swapped before
		 * the accumulation step.  We do this with a special-purpose
		 * routine and override the normal post decoding logic that
		 * the library setup when the directory was read.
		 */
		SensorCall();if (tif->tif_flags & TIFF_SWAB) {
			SensorCall();if (sp->decodepfunc == horAcc16) {
				SensorCall();sp->decodepfunc = swabHorAcc16;
				tif->tif_postdecode = _TIFFNoPostDecode;
            } else {/*21*/SensorCall();if (sp->decodepfunc == horAcc32) {
				SensorCall();sp->decodepfunc = swabHorAcc32;
				tif->tif_postdecode = _TIFFNoPostDecode;
            ;/*22*/}}
		}
	}

	else {/*23*/SensorCall();if (sp->predictor == 3) {
		SensorCall();sp->decodepfunc = fpAcc;
		/*
		 * Override default decoding method with one that does the
		 * predictor stuff.
		 */
                SensorCall();if( tif->tif_decoderow != PredictorDecodeRow )
                {
                    SensorCall();sp->decoderow = tif->tif_decoderow;
                    tif->tif_decoderow = PredictorDecodeRow;
                    sp->decodestrip = tif->tif_decodestrip;
                    tif->tif_decodestrip = PredictorDecodeTile;
                    sp->decodetile = tif->tif_decodetile;
                    tif->tif_decodetile = PredictorDecodeTile;
                }
		/*
		 * The data should not be swapped outside of the floating
		 * point predictor, the accumulation routine should return
		 * byres in the native order.
		 */
		SensorCall();if (tif->tif_flags & TIFF_SWAB) {
			SensorCall();tif->tif_postdecode = _TIFFNoPostDecode;
		}
		/*
		 * Allocate buffer to keep the decoded bytes before
		 * rearranging in the ight order
		 */
	;/*24*/}}

	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

static int
PredictorSetupEncode(TIFF* tif)
{
	SensorCall();TIFFPredictorState* sp = PredictorState(tif);
	TIFFDirectory* td = &tif->tif_dir;

	SensorCall();if (!(*sp->setupencode)(tif) || !PredictorSetup(tif))
		{/*25*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*26*/}

	SensorCall();if (sp->predictor == 2) {
		SensorCall();switch (td->td_bitspersample) {
			case 8:  SensorCall();sp->encodepfunc = horDiff8; SensorCall();break;
			case 16: SensorCall();sp->encodepfunc = horDiff16; SensorCall();break;
			case 32: SensorCall();sp->encodepfunc = horDiff32; SensorCall();break;
		}
		/*
		 * Override default encoding method with one that does the
		 * predictor stuff.
		 */
                SensorCall();if( tif->tif_encoderow != PredictorEncodeRow )
                {
                    SensorCall();sp->encoderow = tif->tif_encoderow;
                    tif->tif_encoderow = PredictorEncodeRow;
                    sp->encodestrip = tif->tif_encodestrip;
                    tif->tif_encodestrip = PredictorEncodeTile;
                    sp->encodetile = tif->tif_encodetile;
                    tif->tif_encodetile = PredictorEncodeTile;
                }
	}

	else {/*27*/SensorCall();if (sp->predictor == 3) {
		SensorCall();sp->encodepfunc = fpDiff;
		/*
		 * Override default encoding method with one that does the
		 * predictor stuff.
		 */
                SensorCall();if( tif->tif_encoderow != PredictorEncodeRow )
                {
                    SensorCall();sp->encoderow = tif->tif_encoderow;
                    tif->tif_encoderow = PredictorEncodeRow;
                    sp->encodestrip = tif->tif_encodestrip;
                    tif->tif_encodestrip = PredictorEncodeTile;
                    sp->encodetile = tif->tif_encodetile;
                    tif->tif_encodetile = PredictorEncodeTile;
                }
	;/*28*/}}

	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

#define REPEAT4(n, op)		\
    switch (n) {		\
    default: { tmsize_t i; for (i = n-4; i > 0; i--) { op; } } \
    case 4:  op;		\
    case 3:  op;		\
    case 2:  op;		\
    case 1:  op;		\
    case 0:  ;			\
    }

static void
horAcc8(TIFF* tif, uint8* cp0, tmsize_t cc)
{
	SensorCall();tmsize_t stride = PredictorState(tif)->stride;

	char* cp = (char*) cp0;
	assert((cc%stride)==0);
	SensorCall();if (cc > stride) {
		/*
		 * Pipeline the most common cases.
		 */
		SensorCall();if (stride == 3)  {
			SensorCall();unsigned int cr = cp[0];
			unsigned int cg = cp[1];
			unsigned int cb = cp[2];
			cc -= 3;
			cp += 3;
			SensorCall();while (cc>0) {
				SensorCall();cp[0] = (char) (cr += cp[0]);
				cp[1] = (char) (cg += cp[1]);
				cp[2] = (char) (cb += cp[2]);
				cc -= 3;
				cp += 3;
			}
		} else {/*1*/SensorCall();if (stride == 4)  {
			SensorCall();unsigned int cr = cp[0];
			unsigned int cg = cp[1];
			unsigned int cb = cp[2];
			unsigned int ca = cp[3];
			cc -= 4;
			cp += 4;
			SensorCall();while (cc>0) {
				SensorCall();cp[0] = (char) (cr += cp[0]);
				cp[1] = (char) (cg += cp[1]);
				cp[2] = (char) (cb += cp[2]);
				cp[3] = (char) (ca += cp[3]);
				cc -= 4;
				cp += 4;
			}
		} else  {
			SensorCall();cc -= stride;
			SensorCall();do {
				REPEAT4(stride, cp[stride] =
					(char) (cp[stride] + *cp); cp++)
				SensorCall();cc -= stride;
			} while (cc>0);
		;/*2*/}}
	}
SensorCall();}

static void
swabHorAcc16(TIFF* tif, uint8* cp0, tmsize_t cc)
{
	SensorCall();tmsize_t stride = PredictorState(tif)->stride;
	uint16* wp = (uint16*) cp0;
	tmsize_t wc = cc / 2;

	assert((cc%(2*stride))==0);

	SensorCall();if (wc > stride) {
		SensorCall();TIFFSwabArrayOfShort(wp, wc);
		wc -= stride;
		SensorCall();do {
			REPEAT4(stride, wp[stride] += wp[0]; wp++)
			SensorCall();wc -= stride;
		} while (wc > 0);
	}
SensorCall();}

static void
horAcc16(TIFF* tif, uint8* cp0, tmsize_t cc)
{
	SensorCall();tmsize_t stride = PredictorState(tif)->stride;
	uint16* wp = (uint16*) cp0;
	tmsize_t wc = cc / 2;

	assert((cc%(2*stride))==0);

	SensorCall();if (wc > stride) {
		SensorCall();wc -= stride;
		SensorCall();do {
			REPEAT4(stride, wp[stride] += wp[0]; wp++)
			SensorCall();wc -= stride;
		} while (wc > 0);
	}
SensorCall();}

static void
swabHorAcc32(TIFF* tif, uint8* cp0, tmsize_t cc)
{
	SensorCall();tmsize_t stride = PredictorState(tif)->stride;
	uint32* wp = (uint32*) cp0;
	tmsize_t wc = cc / 4;

	assert((cc%(4*stride))==0);

	SensorCall();if (wc > stride) {
		SensorCall();TIFFSwabArrayOfLong(wp, wc);
		wc -= stride;
		SensorCall();do {
			REPEAT4(stride, wp[stride] += wp[0]; wp++)
			SensorCall();wc -= stride;
		} while (wc > 0);
	}
SensorCall();}

static void
horAcc32(TIFF* tif, uint8* cp0, tmsize_t cc)
{
	SensorCall();tmsize_t stride = PredictorState(tif)->stride;
	uint32* wp = (uint32*) cp0;
	tmsize_t wc = cc / 4;

	assert((cc%(4*stride))==0);

	SensorCall();if (wc > stride) {
		SensorCall();wc -= stride;
		SensorCall();do {
			REPEAT4(stride, wp[stride] += wp[0]; wp++)
			SensorCall();wc -= stride;
		} while (wc > 0);
	}
SensorCall();}

/*
 * Floating point predictor accumulation routine.
 */
static void
fpAcc(TIFF* tif, uint8* cp0, tmsize_t cc)
{
	SensorCall();tmsize_t stride = PredictorState(tif)->stride;
	uint32 bps = tif->tif_dir.td_bitspersample / 8;
	tmsize_t wc = cc / bps;
	tmsize_t count = cc;
	uint8 *cp = (uint8 *) cp0;
	uint8 *tmp = (uint8 *)_TIFFmalloc(cc);

	assert((cc%(bps*stride))==0);

	SensorCall();if (!tmp)
		{/*5*/SensorCall();return;/*6*/}

	SensorCall();while (count > stride) {
		REPEAT4(stride, cp[stride] += cp[0]; cp++)
		SensorCall();count -= stride;
	}

	SensorCall();_TIFFmemcpy(tmp, cp0, cc);
	cp = (uint8 *) cp0;
	SensorCall();for (count = 0; count < wc; count++) {
		SensorCall();uint32 byte;
		SensorCall();for (byte = 0; byte < bps; byte++) {
			#if WORDS_BIGENDIAN
			cp[bps * count + byte] = tmp[byte * wc + count];
			#else
			SensorCall();cp[bps * count + byte] =
				tmp[(bps - byte - 1) * wc + count];
			#endif
		}
	}
	SensorCall();_TIFFfree(tmp);
SensorCall();}

/*
 * Decode a scanline and apply the predictor routine.
 */
static int
PredictorDecodeRow(TIFF* tif, uint8* op0, tmsize_t occ0, uint16 s)
{
	SensorCall();TIFFPredictorState *sp = PredictorState(tif);

	assert(sp != NULL);
	assert(sp->decoderow != NULL);
	assert(sp->decodepfunc != NULL);  

	SensorCall();if ((*sp->decoderow)(tif, op0, occ0, s)) {
		SensorCall();(*sp->decodepfunc)(tif, op0, occ0);
		{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
	} else
		{/*9*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*10*/}
SensorCall();}

/*
 * Decode a tile/strip and apply the predictor routine.
 * Note that horizontal differencing must be done on a
 * row-by-row basis.  The width of a "row" has already
 * been calculated at pre-decode time according to the
 * strip/tile dimensions.
 */
static int
PredictorDecodeTile(TIFF* tif, uint8* op0, tmsize_t occ0, uint16 s)
{
	SensorCall();TIFFPredictorState *sp = PredictorState(tif);

	assert(sp != NULL);
	assert(sp->decodetile != NULL);

	SensorCall();if ((*sp->decodetile)(tif, op0, occ0, s)) {
		SensorCall();tmsize_t rowsize = sp->rowsize;
		assert(rowsize > 0);
		assert((occ0%rowsize)==0);
		assert(sp->decodepfunc != NULL);
		SensorCall();while (occ0 > 0) {
			SensorCall();(*sp->decodepfunc)(tif, op0, rowsize);
			occ0 -= rowsize;
			op0 += rowsize;
		}
		{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
	} else
		{/*11*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*12*/}
SensorCall();}

static void
horDiff8(TIFF* tif, uint8* cp0, tmsize_t cc)
{
	SensorCall();TIFFPredictorState* sp = PredictorState(tif);
	tmsize_t stride = sp->stride;
	char* cp = (char*) cp0;

	assert((cc%stride)==0);

	SensorCall();if (cc > stride) {
		SensorCall();cc -= stride;
		/*
		 * Pipeline the most common cases.
		 */
		SensorCall();if (stride == 3) {
			SensorCall();int r1, g1, b1;
			int r2 = cp[0];
			int g2 = cp[1];
			int b2 = cp[2];
			SensorCall();do {
				SensorCall();r1 = cp[3]; cp[3] = r1-r2; r2 = r1;
				g1 = cp[4]; cp[4] = g1-g2; g2 = g1;
				b1 = cp[5]; cp[5] = b1-b2; b2 = b1;
				cp += 3;
			} while ((cc -= 3) > 0);
		} else {/*3*/SensorCall();if (stride == 4) {
			SensorCall();int r1, g1, b1, a1;
			int r2 = cp[0];
			int g2 = cp[1];
			int b2 = cp[2];
			int a2 = cp[3];
			SensorCall();do {
				SensorCall();r1 = cp[4]; cp[4] = r1-r2; r2 = r1;
				g1 = cp[5]; cp[5] = g1-g2; g2 = g1;
				b1 = cp[6]; cp[6] = b1-b2; b2 = b1;
				a1 = cp[7]; cp[7] = a1-a2; a2 = a1;
				cp += 4;
			} while ((cc -= 4) > 0);
		} else {
			SensorCall();cp += cc - 1;
			SensorCall();do {
				REPEAT4(stride, cp[stride] -= cp[0]; cp--)
			} while ((cc -= stride) > 0);
		;/*4*/}}
	}
SensorCall();}

static void
horDiff16(TIFF* tif, uint8* cp0, tmsize_t cc)
{
	SensorCall();TIFFPredictorState* sp = PredictorState(tif);
	tmsize_t stride = sp->stride;
	int16 *wp = (int16*) cp0;
	tmsize_t wc = cc/2;

	assert((cc%(2*stride))==0);

	SensorCall();if (wc > stride) {
		SensorCall();wc -= stride;
		wp += wc - 1;
		SensorCall();do {
			REPEAT4(stride, wp[stride] -= wp[0]; wp--)
			SensorCall();wc -= stride;
		} while (wc > 0);
	}
SensorCall();}

static void
horDiff32(TIFF* tif, uint8* cp0, tmsize_t cc)
{
	SensorCall();TIFFPredictorState* sp = PredictorState(tif);
	tmsize_t stride = sp->stride;
	int32 *wp = (int32*) cp0;
	tmsize_t wc = cc/4;

	assert((cc%(4*stride))==0);

	SensorCall();if (wc > stride) {
		SensorCall();wc -= stride;
		wp += wc - 1;
		SensorCall();do {
			REPEAT4(stride, wp[stride] -= wp[0]; wp--)
			SensorCall();wc -= stride;
		} while (wc > 0);
	}
SensorCall();}

/*
 * Floating point predictor differencing routine.
 */
static void
fpDiff(TIFF* tif, uint8* cp0, tmsize_t cc)
{
	SensorCall();tmsize_t stride = PredictorState(tif)->stride;
	uint32 bps = tif->tif_dir.td_bitspersample / 8;
	tmsize_t wc = cc / bps;
	tmsize_t count;
	uint8 *cp = (uint8 *) cp0;
	uint8 *tmp = (uint8 *)_TIFFmalloc(cc);

	assert((cc%(bps*stride))==0);

	SensorCall();if (!tmp)
		{/*7*/SensorCall();return;/*8*/}

	SensorCall();_TIFFmemcpy(tmp, cp0, cc);
	SensorCall();for (count = 0; count < wc; count++) {
		SensorCall();uint32 byte;
		SensorCall();for (byte = 0; byte < bps; byte++) {
			#if WORDS_BIGENDIAN
			cp[byte * wc + count] = tmp[bps * count + byte];
			#else
			SensorCall();cp[(bps - byte - 1) * wc + count] =
				tmp[bps * count + byte];
			#endif
		}
	}
	SensorCall();_TIFFfree(tmp);

	cp = (uint8 *) cp0;
	cp += cc - stride - 1;
	for (count = cc; count > stride; count -= stride)
		REPEAT4(stride, cp[stride] -= cp[0]; cp--)
}

static int
PredictorEncodeRow(TIFF* tif, uint8* bp, tmsize_t cc, uint16 s)
{
	SensorCall();TIFFPredictorState *sp = PredictorState(tif);

	assert(sp != NULL);
	assert(sp->encodepfunc != NULL);
	assert(sp->encoderow != NULL);

	/* XXX horizontal differencing alters user's data XXX */
	(*sp->encodepfunc)(tif, bp, cc);
	{int  ReplaceReturn = (*sp->encoderow)(tif, bp, cc, s); SensorCall(); return ReplaceReturn;}
}

static int
PredictorEncodeTile(TIFF* tif, uint8* bp0, tmsize_t cc0, uint16 s)
{
	SensorCall();static const char module[] = "PredictorEncodeTile";
	TIFFPredictorState *sp = PredictorState(tif);
        uint8 *working_copy;
	tmsize_t cc = cc0, rowsize;
	unsigned char* bp;
        int result_code;

	assert(sp != NULL);
	assert(sp->encodepfunc != NULL);
	assert(sp->encodetile != NULL);

        /* 
         * Do predictor manipulation in a working buffer to avoid altering
         * the callers buffer. http://trac.osgeo.org/gdal/ticket/1965
         */
        working_copy = (uint8*) _TIFFmalloc(cc0);
        SensorCall();if( working_copy == NULL )
        {
            SensorCall();TIFFErrorExt(tif->tif_clientdata, module, 
                         "Out of memory allocating " TIFF_SSIZE_FORMAT " byte temp buffer.",
                         cc0 );
            {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
        }
        SensorCall();memcpy( working_copy, bp0, cc0 );
        bp = working_copy;

	rowsize = sp->rowsize;
	assert(rowsize > 0);
	assert((cc0%rowsize)==0);
	SensorCall();while (cc > 0) {
		SensorCall();(*sp->encodepfunc)(tif, bp, rowsize);
		cc -= rowsize;
		bp += rowsize;
	}
	SensorCall();result_code = (*sp->encodetile)(tif, working_copy, cc0, s);

        _TIFFfree( working_copy );

        {int  ReplaceReturn = result_code; SensorCall(); return ReplaceReturn;}
}

#define	FIELD_PREDICTOR	(FIELD_CODEC+0)		/* XXX */

static const TIFFField predictFields[] = {
    { TIFFTAG_PREDICTOR, 1, 1, TIFF_SHORT, 0, TIFF_SETGET_UINT16, TIFF_SETGET_UINT16, FIELD_PREDICTOR, FALSE, FALSE, "Predictor", NULL },
};

static int
PredictorVSetField(TIFF* tif, uint32 tag, va_list ap)
{
	SensorCall();TIFFPredictorState *sp = PredictorState(tif);

	assert(sp != NULL);
	assert(sp->vsetparent != NULL);

	SensorCall();switch (tag) {
	case TIFFTAG_PREDICTOR:
		SensorCall();sp->predictor = (uint16) va_arg(ap, uint16_vap);
		TIFFSetFieldBit(tif, FIELD_PREDICTOR);
		SensorCall();break;
	default:
		{int  ReplaceReturn = (*sp->vsetparent)(tif, tag, ap); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();tif->tif_flags |= TIFF_DIRTYDIRECT;
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

static int
PredictorVGetField(TIFF* tif, uint32 tag, va_list ap)
{
	SensorCall();TIFFPredictorState *sp = PredictorState(tif);

	assert(sp != NULL);
	assert(sp->vgetparent != NULL);

	SensorCall();switch (tag) {
	case TIFFTAG_PREDICTOR:
		SensorCall();*va_arg(ap, uint16*) = sp->predictor;
		SensorCall();break;
	default:
		{int  ReplaceReturn = (*sp->vgetparent)(tif, tag, ap); SensorCall(); return ReplaceReturn;}
	}
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

static void
PredictorPrintDir(TIFF* tif, FILE* fd, long flags)
{
	SensorCall();TIFFPredictorState* sp = PredictorState(tif);

	(void) flags;
	SensorCall();if (TIFFFieldSet(tif,FIELD_PREDICTOR)) {
		SensorCall();fprintf(fd, "  Predictor: ");
		SensorCall();switch (sp->predictor) {
			case 1: SensorCall();fprintf(fd, "none "); SensorCall();break;
			case 2: SensorCall();fprintf(fd, "horizontal differencing "); SensorCall();break;
			case 3: SensorCall();fprintf(fd, "floating point predictor "); SensorCall();break;
		}
		SensorCall();fprintf(fd, "%u (0x%x)\n", sp->predictor, sp->predictor);
	}
	SensorCall();if (sp->printdir)
		{/*29*/SensorCall();(*sp->printdir)(tif, fd, flags);/*30*/}
SensorCall();}

int
TIFFPredictorInit(TIFF* tif)
{
	SensorCall();TIFFPredictorState* sp = PredictorState(tif);

	assert(sp != 0);

	/*
	 * Merge codec-specific tag information.
	 */
	SensorCall();if (!_TIFFMergeFields(tif, predictFields,
			      TIFFArrayCount(predictFields))) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, "TIFFPredictorInit",
		    "Merging Predictor codec-specific tags failed");
		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}

	/*
	 * Override parent get/set field methods.
	 */
	SensorCall();sp->vgetparent = tif->tif_tagmethods.vgetfield;
	tif->tif_tagmethods.vgetfield =
            PredictorVGetField;/* hook for predictor tag */
	sp->vsetparent = tif->tif_tagmethods.vsetfield;
	tif->tif_tagmethods.vsetfield =
	    PredictorVSetField;/* hook for predictor tag */
	sp->printdir = tif->tif_tagmethods.printdir;
	tif->tif_tagmethods.printdir =
            PredictorPrintDir;	/* hook for predictor tag */

	sp->setupdecode = tif->tif_setupdecode;
	tif->tif_setupdecode = PredictorSetupDecode;
	sp->setupencode = tif->tif_setupencode;
	tif->tif_setupencode = PredictorSetupEncode;

	sp->predictor = 1;			/* default value */
	sp->encodepfunc = NULL;			/* no predictor routine */
	sp->decodepfunc = NULL;			/* no predictor routine */
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

int
TIFFPredictorCleanup(TIFF* tif)
{
	SensorCall();TIFFPredictorState* sp = PredictorState(tif);

	assert(sp != 0);

	tif->tif_tagmethods.vgetfield = sp->vgetparent;
	tif->tif_tagmethods.vsetfield = sp->vsetparent;
	tif->tif_tagmethods.printdir = sp->printdir;
	tif->tif_setupdecode = sp->setupdecode;
	tif->tif_setupencode = sp->setupencode;

	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
