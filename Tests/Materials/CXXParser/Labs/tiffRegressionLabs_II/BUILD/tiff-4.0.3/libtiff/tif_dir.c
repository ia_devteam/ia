/* $Id: tif_dir.c,v 1.113 2012-06-14 20:32:53 fwarmerdam Exp $ */

/*
 * Copyright (c) 1988-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

/*
 * TIFF Library.
 *
 * Directory Tag Get & Set Routines.
 * (and also some miscellaneous stuff)
 */
#include "tiffiop.h"
#include "/var/tmp/sensor.h"

/*
 * These are used in the backwards compatibility code...
 */
#define DATATYPE_VOID		0       /* !untyped data */
#define DATATYPE_INT		1       /* !signed integer data */
#define DATATYPE_UINT		2       /* !unsigned integer data */
#define DATATYPE_IEEEFP		3       /* !IEEE floating point data */

static void
setByteArray(void** vpp, void* vp, size_t nmemb, size_t elem_size)
{
	SensorCall();if (*vpp)
		{/*31*/SensorCall();_TIFFfree(*vpp), *vpp = 0;/*32*/}
	SensorCall();if (vp) {
		SensorCall();tmsize_t bytes = (tmsize_t)(nmemb * elem_size);
		SensorCall();if (elem_size && bytes / elem_size == nmemb)
			{/*33*/SensorCall();*vpp = (void*) _TIFFmalloc(bytes);/*34*/}
		SensorCall();if (*vpp)
			{/*35*/SensorCall();_TIFFmemcpy(*vpp, vp, bytes);/*36*/}
	}
SensorCall();}
void _TIFFsetByteArray(void** vpp, void* vp, uint32 n)
    { SensorCall();setByteArray(vpp, vp, n, 1); SensorCall();}
void _TIFFsetString(char** cpp, char* cp)
    { SensorCall();setByteArray((void**) cpp, (void*) cp, strlen(cp)+1, 1); SensorCall();}
void _TIFFsetNString(char** cpp, char* cp, uint32 n)
    { SensorCall();setByteArray((void**) cpp, (void*) cp, n, 1); SensorCall();}
void _TIFFsetShortArray(uint16** wpp, uint16* wp, uint32 n)
    { SensorCall();setByteArray((void**) wpp, (void*) wp, n, sizeof (uint16)); SensorCall();}
void _TIFFsetLongArray(uint32** lpp, uint32* lp, uint32 n)
    { SensorCall();setByteArray((void**) lpp, (void*) lp, n, sizeof (uint32)); SensorCall();}
void _TIFFsetLong8Array(uint64** lpp, uint64* lp, uint32 n)
    { SensorCall();setByteArray((void**) lpp, (void*) lp, n, sizeof (uint64)); SensorCall();}
void _TIFFsetFloatArray(float** fpp, float* fp, uint32 n)
    { SensorCall();setByteArray((void**) fpp, (void*) fp, n, sizeof (float)); SensorCall();}
void _TIFFsetDoubleArray(double** dpp, double* dp, uint32 n)
    { SensorCall();setByteArray((void**) dpp, (void*) dp, n, sizeof (double)); SensorCall();}

static void
setDoubleArrayOneValue(double** vpp, double value, size_t nmemb)
{
	SensorCall();if (*vpp)
		{/*37*/SensorCall();_TIFFfree(*vpp);/*38*/}
	SensorCall();*vpp = _TIFFmalloc(nmemb*sizeof(double));
	SensorCall();if (*vpp)
	{
		SensorCall();while (nmemb--)
			{/*39*/SensorCall();((double*)*vpp)[nmemb] = value;/*40*/}
	}
SensorCall();}

/*
 * Install extra samples information.
 */
static int
setExtraSamples(TIFFDirectory* td, va_list ap, uint32* v)
{
/* XXX: Unassociated alpha data == 999 is a known Corel Draw bug, see below */
#define EXTRASAMPLE_COREL_UNASSALPHA 999 

	SensorCall();uint16* va;
	uint32 i;

	*v = (uint16) va_arg(ap, uint16_vap);
	SensorCall();if ((uint16) *v > td->td_samplesperpixel)
		{/*41*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*42*/}
	SensorCall();va = va_arg(ap, uint16*);
	SensorCall();if (*v > 0 && va == NULL)		/* typically missing param */
		{/*43*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*44*/}
	SensorCall();for (i = 0; i < *v; i++) {
		SensorCall();if (va[i] > EXTRASAMPLE_UNASSALPHA) {
			/*
			 * XXX: Corel Draw is known to produce incorrect
			 * ExtraSamples tags which must be patched here if we
			 * want to be able to open some of the damaged TIFF
			 * files: 
			 */
			SensorCall();if (va[i] == EXTRASAMPLE_COREL_UNASSALPHA)
				va[i] = EXTRASAMPLE_UNASSALPHA;
			else
				{/*45*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*46*/}
		}
	}
	SensorCall();td->td_extrasamples = (uint16) *v;
	_TIFFsetShortArray(&td->td_sampleinfo, va, td->td_extrasamples);
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}

#undef EXTRASAMPLE_COREL_UNASSALPHA
}

/*
 * Confirm we have "samplesperpixel" ink names separated by \0.  Returns 
 * zero if the ink names are not as expected.
 */
static uint32
checkInkNamesString(TIFF* tif, uint32 slen, const char* s)
{
	SensorCall();TIFFDirectory* td = &tif->tif_dir;
	uint16 i = td->td_samplesperpixel;

	SensorCall();if (slen > 0) {
		SensorCall();const char* ep = s+slen;
		const char* cp = s;
		SensorCall();for (; i > 0; i--) {
			SensorCall();for (; cp < ep && *cp != '\0'; cp++) {}
			SensorCall();if (cp >= ep)
				{/*47*/SensorCall();goto bad;/*48*/}
			SensorCall();cp++;				/* skip \0 */
		}
		{uint32  ReplaceReturn = ((uint32)(cp-s)); SensorCall(); return ReplaceReturn;}
	}
bad:
	SensorCall();TIFFErrorExt(tif->tif_clientdata, "TIFFSetField",
	    "%s: Invalid InkNames value; expecting %d names, found %d",
	    tif->tif_name,
	    td->td_samplesperpixel,
	    td->td_samplesperpixel-i);
	{uint32  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
}

static int
_TIFFVSetField(TIFF* tif, uint32 tag, va_list ap)
{
	SensorCall();static const char module[] = "_TIFFVSetField";

	TIFFDirectory* td = &tif->tif_dir;
	int status = 1;
	uint32 v32, i, v;
    double dblval;
	char* s;
	const TIFFField *fip = TIFFFindField(tif, tag, TIFF_ANY);
	uint32 standard_tag = tag;

	/*
	 * We want to force the custom code to be used for custom
	 * fields even if the tag happens to match a well known 
	 * one - important for reinterpreted handling of standard
	 * tag values in custom directories (ie. EXIF) 
	 */
	SensorCall();if (fip->field_bit == FIELD_CUSTOM) {
		SensorCall();standard_tag = 0;
	}

	SensorCall();switch (standard_tag) {
	case TIFFTAG_SUBFILETYPE:
		SensorCall();td->td_subfiletype = (uint32) va_arg(ap, uint32);
		SensorCall();break;
	case TIFFTAG_IMAGEWIDTH:
		SensorCall();td->td_imagewidth = (uint32) va_arg(ap, uint32);
		SensorCall();break;
	case TIFFTAG_IMAGELENGTH:
		SensorCall();td->td_imagelength = (uint32) va_arg(ap, uint32);
		SensorCall();break;
	case TIFFTAG_BITSPERSAMPLE:
		SensorCall();td->td_bitspersample = (uint16) va_arg(ap, uint16_vap);
		/*
		 * If the data require post-decoding processing to byte-swap
		 * samples, set it up here.  Note that since tags are required
		 * to be ordered, compression code can override this behaviour
		 * in the setup method if it wants to roll the post decoding
		 * work in with its normal work.
		 */
		SensorCall();if (tif->tif_flags & TIFF_SWAB) {
			SensorCall();if (td->td_bitspersample == 8)
				{/*49*/SensorCall();tif->tif_postdecode = _TIFFNoPostDecode;/*50*/}
			else {/*51*/SensorCall();if (td->td_bitspersample == 16)
				{/*53*/SensorCall();tif->tif_postdecode = _TIFFSwab16BitData;/*54*/}
			else {/*55*/SensorCall();if (td->td_bitspersample == 24)
				{/*57*/SensorCall();tif->tif_postdecode = _TIFFSwab24BitData;/*58*/}
			else {/*59*/SensorCall();if (td->td_bitspersample == 32)
				{/*61*/SensorCall();tif->tif_postdecode = _TIFFSwab32BitData;/*62*/}
			else {/*63*/SensorCall();if (td->td_bitspersample == 64)
				{/*65*/SensorCall();tif->tif_postdecode = _TIFFSwab64BitData;/*66*/}
			else {/*67*/SensorCall();if (td->td_bitspersample == 128) /* two 64's */
				{/*69*/SensorCall();tif->tif_postdecode = _TIFFSwab64BitData;/*70*/}/*68*/}/*64*/}/*60*/}/*56*/}/*52*/}
		}
		SensorCall();break;
	case TIFFTAG_COMPRESSION:
		SensorCall();v = (uint16) va_arg(ap, uint16_vap);
		/*
		 * If we're changing the compression scheme, the notify the
		 * previous module so that it can cleanup any state it's
		 * setup.
		 */
		SensorCall();if (TIFFFieldSet(tif, FIELD_COMPRESSION)) {
			SensorCall();if ((uint32)td->td_compression == v)
				{/*71*/SensorCall();break;/*72*/}
			SensorCall();(*tif->tif_cleanup)(tif);
			tif->tif_flags &= ~TIFF_CODERSETUP;
		}
		/*
		 * Setup new compression routine state.
		 */
		SensorCall();if( (status = TIFFSetCompressionScheme(tif, v)) != 0 )
		    {/*73*/SensorCall();td->td_compression = (uint16) v;/*74*/}
		else
		    {/*75*/SensorCall();status = 0;/*76*/}
		SensorCall();break;
	case TIFFTAG_PHOTOMETRIC:
		SensorCall();td->td_photometric = (uint16) va_arg(ap, uint16_vap);
		SensorCall();break;
	case TIFFTAG_THRESHHOLDING:
		SensorCall();td->td_threshholding = (uint16) va_arg(ap, uint16_vap);
		SensorCall();break;
	case TIFFTAG_FILLORDER:
		SensorCall();v = (uint16) va_arg(ap, uint16_vap);
		SensorCall();if (v != FILLORDER_LSB2MSB && v != FILLORDER_MSB2LSB)
			{/*77*/SensorCall();goto badvalue;/*78*/}
		SensorCall();td->td_fillorder = (uint16) v;
		SensorCall();break;
	case TIFFTAG_ORIENTATION:
		SensorCall();v = (uint16) va_arg(ap, uint16_vap);
		SensorCall();if (v < ORIENTATION_TOPLEFT || ORIENTATION_LEFTBOT < v)
			{/*79*/SensorCall();goto badvalue;/*80*/}
		else
			{/*81*/SensorCall();td->td_orientation = (uint16) v;/*82*/}
		SensorCall();break;
	case TIFFTAG_SAMPLESPERPIXEL:
		SensorCall();v = (uint16) va_arg(ap, uint16_vap);
		SensorCall();if (v == 0)
			{/*83*/SensorCall();goto badvalue;/*84*/}
		SensorCall();td->td_samplesperpixel = (uint16) v;
		SensorCall();break;
	case TIFFTAG_ROWSPERSTRIP:
		SensorCall();v32 = (uint32) va_arg(ap, uint32);
		SensorCall();if (v32 == 0)
			{/*85*/SensorCall();goto badvalue32;/*86*/}
		SensorCall();td->td_rowsperstrip = v32;
		SensorCall();if (!TIFFFieldSet(tif, FIELD_TILEDIMENSIONS)) {
			SensorCall();td->td_tilelength = v32;
			td->td_tilewidth = td->td_imagewidth;
		}
		SensorCall();break;
	case TIFFTAG_MINSAMPLEVALUE:
		SensorCall();td->td_minsamplevalue = (uint16) va_arg(ap, uint16_vap);
		SensorCall();break;
	case TIFFTAG_MAXSAMPLEVALUE:
		SensorCall();td->td_maxsamplevalue = (uint16) va_arg(ap, uint16_vap);
		SensorCall();break;
	case TIFFTAG_SMINSAMPLEVALUE:
		SensorCall();if (tif->tif_flags & TIFF_PERSAMPLE)
			{/*87*/SensorCall();_TIFFsetDoubleArray(&td->td_sminsamplevalue, va_arg(ap, double*), td->td_samplesperpixel);/*88*/}
		else
			{/*89*/SensorCall();setDoubleArrayOneValue(&td->td_sminsamplevalue, va_arg(ap, double), td->td_samplesperpixel);/*90*/}
		SensorCall();break;
	case TIFFTAG_SMAXSAMPLEVALUE:
		SensorCall();if (tif->tif_flags & TIFF_PERSAMPLE)
			{/*91*/SensorCall();_TIFFsetDoubleArray(&td->td_smaxsamplevalue, va_arg(ap, double*), td->td_samplesperpixel);/*92*/}
		else
			{/*93*/SensorCall();setDoubleArrayOneValue(&td->td_smaxsamplevalue, va_arg(ap, double), td->td_samplesperpixel);/*94*/}
		SensorCall();break;
	case TIFFTAG_XRESOLUTION:
        SensorCall();dblval = va_arg(ap, double);
        SensorCall();if( dblval < 0 )
            {/*95*/SensorCall();goto badvaluedouble;/*96*/}
		SensorCall();td->td_xresolution = (float) dblval;
		SensorCall();break;
	case TIFFTAG_YRESOLUTION:
        SensorCall();dblval = va_arg(ap, double);
        SensorCall();if( dblval < 0 )
            {/*97*/SensorCall();goto badvaluedouble;/*98*/}
		SensorCall();td->td_yresolution = (float) dblval;
		SensorCall();break;
	case TIFFTAG_PLANARCONFIG:
		SensorCall();v = (uint16) va_arg(ap, uint16_vap);
		SensorCall();if (v != PLANARCONFIG_CONTIG && v != PLANARCONFIG_SEPARATE)
			{/*99*/SensorCall();goto badvalue;/*100*/}
		SensorCall();td->td_planarconfig = (uint16) v;
		SensorCall();break;
	case TIFFTAG_XPOSITION:
		SensorCall();td->td_xposition = (float) va_arg(ap, double);
		SensorCall();break;
	case TIFFTAG_YPOSITION:
		SensorCall();td->td_yposition = (float) va_arg(ap, double);
		SensorCall();break;
	case TIFFTAG_RESOLUTIONUNIT:
		SensorCall();v = (uint16) va_arg(ap, uint16_vap);
		SensorCall();if (v < RESUNIT_NONE || RESUNIT_CENTIMETER < v)
			{/*101*/SensorCall();goto badvalue;/*102*/}
		SensorCall();td->td_resolutionunit = (uint16) v;
		SensorCall();break;
	case TIFFTAG_PAGENUMBER:
		SensorCall();td->td_pagenumber[0] = (uint16) va_arg(ap, uint16_vap);
		td->td_pagenumber[1] = (uint16) va_arg(ap, uint16_vap);
		SensorCall();break;
	case TIFFTAG_HALFTONEHINTS:
		SensorCall();td->td_halftonehints[0] = (uint16) va_arg(ap, uint16_vap);
		td->td_halftonehints[1] = (uint16) va_arg(ap, uint16_vap);
		SensorCall();break;
	case TIFFTAG_COLORMAP:
		SensorCall();v32 = (uint32)(1L<<td->td_bitspersample);
		_TIFFsetShortArray(&td->td_colormap[0], va_arg(ap, uint16*), v32);
		_TIFFsetShortArray(&td->td_colormap[1], va_arg(ap, uint16*), v32);
		_TIFFsetShortArray(&td->td_colormap[2], va_arg(ap, uint16*), v32);
		SensorCall();break;
	case TIFFTAG_EXTRASAMPLES:
		SensorCall();if (!setExtraSamples(td, ap, &v))
			{/*103*/SensorCall();goto badvalue;/*104*/}
		SensorCall();break;
	case TIFFTAG_MATTEING:
		SensorCall();td->td_extrasamples =  (((uint16) va_arg(ap, uint16_vap)) != 0);
		SensorCall();if (td->td_extrasamples) {
			SensorCall();uint16 sv = EXTRASAMPLE_ASSOCALPHA;
			_TIFFsetShortArray(&td->td_sampleinfo, &sv, 1);
		}
		SensorCall();break;
	case TIFFTAG_TILEWIDTH:
		SensorCall();v32 = (uint32) va_arg(ap, uint32);
		SensorCall();if (v32 % 16) {
			SensorCall();if (tif->tif_mode != O_RDONLY)
				{/*105*/SensorCall();goto badvalue32;/*106*/}
			SensorCall();TIFFWarningExt(tif->tif_clientdata, tif->tif_name,
				"Nonstandard tile width %d, convert file", v32);
		}
		SensorCall();td->td_tilewidth = v32;
		tif->tif_flags |= TIFF_ISTILED;
		SensorCall();break;
	case TIFFTAG_TILELENGTH:
		SensorCall();v32 = (uint32) va_arg(ap, uint32);
		SensorCall();if (v32 % 16) {
			SensorCall();if (tif->tif_mode != O_RDONLY)
				{/*107*/SensorCall();goto badvalue32;/*108*/}
			SensorCall();TIFFWarningExt(tif->tif_clientdata, tif->tif_name,
			    "Nonstandard tile length %d, convert file", v32);
		}
		SensorCall();td->td_tilelength = v32;
		tif->tif_flags |= TIFF_ISTILED;
		SensorCall();break;
	case TIFFTAG_TILEDEPTH:
		SensorCall();v32 = (uint32) va_arg(ap, uint32);
		SensorCall();if (v32 == 0)
			{/*109*/SensorCall();goto badvalue32;/*110*/}
		SensorCall();td->td_tiledepth = v32;
		SensorCall();break;
	case TIFFTAG_DATATYPE:
		SensorCall();v = (uint16) va_arg(ap, uint16_vap);
		SensorCall();switch (v) {
		case DATATYPE_VOID:	SensorCall();v = SAMPLEFORMAT_VOID;	SensorCall();break;
		case DATATYPE_INT:	SensorCall();v = SAMPLEFORMAT_INT;	SensorCall();break;
		case DATATYPE_UINT:	SensorCall();v = SAMPLEFORMAT_UINT;	SensorCall();break;
		case DATATYPE_IEEEFP:	SensorCall();v = SAMPLEFORMAT_IEEEFP;SensorCall();break;
		default:		SensorCall();goto badvalue;
		}
		SensorCall();td->td_sampleformat = (uint16) v;
		SensorCall();break;
	case TIFFTAG_SAMPLEFORMAT:
		SensorCall();v = (uint16) va_arg(ap, uint16_vap);
		SensorCall();if (v < SAMPLEFORMAT_UINT || SAMPLEFORMAT_COMPLEXIEEEFP < v)
			{/*111*/SensorCall();goto badvalue;/*112*/}
		SensorCall();td->td_sampleformat = (uint16) v;

		/*  Try to fix up the SWAB function for complex data. */
		SensorCall();if( td->td_sampleformat == SAMPLEFORMAT_COMPLEXINT
		    && td->td_bitspersample == 32
		    && tif->tif_postdecode == _TIFFSwab32BitData )
		    {/*113*/SensorCall();tif->tif_postdecode = _TIFFSwab16BitData;/*114*/}
		else {/*115*/SensorCall();if( (td->td_sampleformat == SAMPLEFORMAT_COMPLEXINT
			  || td->td_sampleformat == SAMPLEFORMAT_COMPLEXIEEEFP)
			 && td->td_bitspersample == 64
			 && tif->tif_postdecode == _TIFFSwab64BitData )
		    {/*117*/SensorCall();tif->tif_postdecode = _TIFFSwab32BitData;/*118*/}/*116*/}
		SensorCall();break;
	case TIFFTAG_IMAGEDEPTH:
		SensorCall();td->td_imagedepth = (uint32) va_arg(ap, uint32);
		SensorCall();break;
	case TIFFTAG_SUBIFD:
		SensorCall();if ((tif->tif_flags & TIFF_INSUBIFD) == 0) {
			SensorCall();td->td_nsubifd = (uint16) va_arg(ap, uint16_vap);
			_TIFFsetLong8Array(&td->td_subifd, (uint64*) va_arg(ap, uint64*),
			    (long) td->td_nsubifd);
		} else {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				     "%s: Sorry, cannot nest SubIFDs",
				     tif->tif_name);
			status = 0;
		}
		SensorCall();break;
	case TIFFTAG_YCBCRPOSITIONING:
		SensorCall();td->td_ycbcrpositioning = (uint16) va_arg(ap, uint16_vap);
		SensorCall();break;
	case TIFFTAG_YCBCRSUBSAMPLING:
		SensorCall();td->td_ycbcrsubsampling[0] = (uint16) va_arg(ap, uint16_vap);
		td->td_ycbcrsubsampling[1] = (uint16) va_arg(ap, uint16_vap);
		SensorCall();break;
	case TIFFTAG_TRANSFERFUNCTION:
		SensorCall();v = (td->td_samplesperpixel - td->td_extrasamples) > 1 ? 3 : 1;
		SensorCall();for (i = 0; i < v; i++)
			{/*119*/SensorCall();_TIFFsetShortArray(&td->td_transferfunction[i],
			    va_arg(ap, uint16*), 1L<<td->td_bitspersample);/*120*/}
		SensorCall();break;
	case TIFFTAG_REFERENCEBLACKWHITE:
		/* XXX should check for null range */
		SensorCall();_TIFFsetFloatArray(&td->td_refblackwhite, va_arg(ap, float*), 6);
		SensorCall();break;
	case TIFFTAG_INKNAMES:
		SensorCall();v = (uint16) va_arg(ap, uint16_vap);
		s = va_arg(ap, char*);
		v = checkInkNamesString(tif, v, s);
		status = v > 0;
		SensorCall();if( v > 0 ) {
			SensorCall();_TIFFsetNString(&td->td_inknames, s, v);
			td->td_inknameslen = v;
		}
		SensorCall();break;
	case TIFFTAG_PERSAMPLE:
		SensorCall();v = (uint16) va_arg(ap, uint16_vap);
		SensorCall();if( v == PERSAMPLE_MULTI )
			tif->tif_flags |= TIFF_PERSAMPLE;
		else
			tif->tif_flags &= ~TIFF_PERSAMPLE;
		SensorCall();break;
	default: {
		SensorCall();TIFFTagValue *tv;
		int tv_size, iCustom;

		/*
		 * This can happen if multiple images are open with different
		 * codecs which have private tags.  The global tag information
		 * table may then have tags that are valid for one file but not
		 * the other. If the client tries to set a tag that is not valid
		 * for the image's codec then we'll arrive here.  This
		 * happens, for example, when tiffcp is used to convert between
		 * compression schemes and codec-specific tags are blindly copied.
		 */
		SensorCall();if(fip == NULL || fip->field_bit != FIELD_CUSTOM) {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
			    "%s: Invalid %stag \"%s\" (not supported by codec)",
			    tif->tif_name, isPseudoTag(tag) ? "pseudo-" : "",
			    fip ? fip->field_name : "Unknown");
			status = 0;
			SensorCall();break;
		}

		/*
		 * Find the existing entry for this custom value.
		 */
		SensorCall();tv = NULL;
		SensorCall();for (iCustom = 0; iCustom < td->td_customValueCount; iCustom++) {
			SensorCall();if (td->td_customValues[iCustom].info->field_tag == tag) {
				SensorCall();tv = td->td_customValues + iCustom;
				SensorCall();if (tv->value != NULL) {
					SensorCall();_TIFFfree(tv->value);
					tv->value = NULL;
				}
				SensorCall();break;
			}
		}

		/*
		 * Grow the custom list if the entry was not found.
		 */
		SensorCall();if(tv == NULL) {
			SensorCall();TIFFTagValue *new_customValues;

			td->td_customValueCount++;
			new_customValues = (TIFFTagValue *)
			    _TIFFrealloc(td->td_customValues,
			    sizeof(TIFFTagValue) * td->td_customValueCount);
			SensorCall();if (!new_customValues) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				    "%s: Failed to allocate space for list of custom values",
				    tif->tif_name);
				status = 0;
				SensorCall();goto end;
			}

			SensorCall();td->td_customValues = new_customValues;

			tv = td->td_customValues + (td->td_customValueCount - 1);
			tv->info = fip;
			tv->value = NULL;
			tv->count = 0;
		}

		/*
		 * Set custom value ... save a copy of the custom tag value.
		 */
		SensorCall();tv_size = _TIFFDataSize(fip->field_type);
		SensorCall();if (tv_size == 0) {
			SensorCall();status = 0;
			TIFFErrorExt(tif->tif_clientdata, module,
			    "%s: Bad field type %d for \"%s\"",
			    tif->tif_name, fip->field_type,
			    fip->field_name);
			SensorCall();goto end;
		}

		SensorCall();if (fip->field_type == TIFF_ASCII)
		{
			SensorCall();uint32 ma;
			char* mb;
			SensorCall();if (fip->field_passcount)
			{
				assert(fip->field_writecount==TIFF_VARIABLE2);
				SensorCall();ma=(uint32)va_arg(ap,uint32);
				mb=(char*)va_arg(ap,char*);
			}
			else
			{
				SensorCall();mb=(char*)va_arg(ap,char*);
				ma=(uint32)(strlen(mb)+1);
			}
			SensorCall();tv->count=ma;
			setByteArray(&tv->value,mb,ma,1);
		}
		else
		{
			SensorCall();if (fip->field_passcount) {
				SensorCall();if (fip->field_writecount == TIFF_VARIABLE2)
					tv->count = (uint32) va_arg(ap, uint32);
				else
					tv->count = (int) va_arg(ap, int);
			} else {/*121*/SensorCall();if (fip->field_writecount == TIFF_VARIABLE
			   || fip->field_writecount == TIFF_VARIABLE2)
				{/*123*/SensorCall();tv->count = 1;/*124*/}
			else {/*125*/SensorCall();if (fip->field_writecount == TIFF_SPP)
				{/*127*/SensorCall();tv->count = td->td_samplesperpixel;/*128*/}
			else
				{/*129*/SensorCall();tv->count = fip->field_writecount;/*130*/}/*126*/}/*122*/}

			SensorCall();if (tv->count == 0) {
				SensorCall();status = 0;
				TIFFErrorExt(tif->tif_clientdata, module,
					     "%s: Null count for \"%s\" (type "
					     "%d, writecount %d, passcount %d)",
					     tif->tif_name,
					     fip->field_name,
					     fip->field_type,
					     fip->field_writecount,
					     fip->field_passcount);
				SensorCall();goto end;
			}

			SensorCall();tv->value = _TIFFCheckMalloc(tif, tv->count, tv_size,
			    "custom tag binary object");
			SensorCall();if (!tv->value) {
				SensorCall();status = 0;
				SensorCall();goto end;
			}

			SensorCall();if (fip->field_tag == TIFFTAG_DOTRANGE 
			    && strcmp(fip->field_name,"DotRange") == 0) {
				/* TODO: This is an evil exception and should not have been
				   handled this way ... likely best if we move it into
				   the directory structure with an explicit field in 
				   libtiff 4.1 and assign it a FIELD_ value */
				SensorCall();uint16 v[2];
				v[0] = (uint16)va_arg(ap, int);
				v[1] = (uint16)va_arg(ap, int);
				_TIFFmemcpy(tv->value, &v, 4);
			}

			else {/*131*/SensorCall();if (fip->field_passcount
				  || fip->field_writecount == TIFF_VARIABLE
				  || fip->field_writecount == TIFF_VARIABLE2
				  || fip->field_writecount == TIFF_SPP
				  || tv->count > 1) {
				SensorCall();_TIFFmemcpy(tv->value, va_arg(ap, void *),
				    tv->count * tv_size);
			} else {
				SensorCall();char *val = (char *)tv->value;
				assert( tv->count == 1 );

				SensorCall();switch (fip->field_type) {
				case TIFF_BYTE:
				case TIFF_UNDEFINED:
					{
						SensorCall();uint8 v = (uint8)va_arg(ap, int);
						_TIFFmemcpy(val, &v, tv_size);
					}
					SensorCall();break;
				case TIFF_SBYTE:
					{
						SensorCall();int8 v = (int8)va_arg(ap, int);
						_TIFFmemcpy(val, &v, tv_size);
					}
					SensorCall();break;
				case TIFF_SHORT:
					{
						SensorCall();uint16 v = (uint16)va_arg(ap, int);
						_TIFFmemcpy(val, &v, tv_size);
					}
					SensorCall();break;
				case TIFF_SSHORT:
					{
						SensorCall();int16 v = (int16)va_arg(ap, int);
						_TIFFmemcpy(val, &v, tv_size);
					}
					SensorCall();break;
				case TIFF_LONG:
				case TIFF_IFD:
					{
						SensorCall();uint32 v = va_arg(ap, uint32);
						_TIFFmemcpy(val, &v, tv_size);
					}
					SensorCall();break;
				case TIFF_SLONG:
					{
						SensorCall();int32 v = va_arg(ap, int32);
						_TIFFmemcpy(val, &v, tv_size);
					}
					SensorCall();break;
				case TIFF_LONG8:
				case TIFF_IFD8:
					{
						SensorCall();uint64 v = va_arg(ap, uint64);
						_TIFFmemcpy(val, &v, tv_size);
					}
					SensorCall();break;
				case TIFF_SLONG8:
					{
						SensorCall();int64 v = va_arg(ap, int64);
						_TIFFmemcpy(val, &v, tv_size);
					}
					SensorCall();break;
				case TIFF_RATIONAL:
				case TIFF_SRATIONAL:
				case TIFF_FLOAT:
					{
						SensorCall();float v = (float)va_arg(ap, double);
						_TIFFmemcpy(val, &v, tv_size);
					}
					SensorCall();break;
				case TIFF_DOUBLE:
					{
						SensorCall();double v = va_arg(ap, double);
						_TIFFmemcpy(val, &v, tv_size);
					}
					SensorCall();break;
				default:
					SensorCall();_TIFFmemset(val, 0, tv_size);
					status = 0;
					SensorCall();break;
				}
			;/*132*/}}
		}
	}
	}
	SensorCall();if (status) {
		SensorCall();const TIFFField* fip=TIFFFieldWithTag(tif,tag);
		SensorCall();if (fip)                
			TIFFSetFieldBit(tif, fip->field_bit);
		SensorCall();tif->tif_flags |= TIFF_DIRTYDIRECT;
	}

end:
	va_end(ap);
	{int  ReplaceReturn = (status); SensorCall(); return ReplaceReturn;}
badvalue:
        {
		const TIFFField* fip=TIFFFieldWithTag(tif,tag);
		TIFFErrorExt(tif->tif_clientdata, module,
		     "%s: Bad value %u for \"%s\" tag",
		     tif->tif_name, v,
		     fip ? fip->field_name : "Unknown");
		va_end(ap);
        }
	return (0);
badvalue32:
        {
		const TIFFField* fip=TIFFFieldWithTag(tif,tag);
		TIFFErrorExt(tif->tif_clientdata, module,
		     "%s: Bad value %u for \"%s\" tag",
		     tif->tif_name, v32,
		     fip ? fip->field_name : "Unknown");
		va_end(ap);
        }
	return (0);
badvaluedouble:
        {
		const TIFFField* fip=TIFFFieldWithTag(tif,tag);
        TIFFErrorExt(tif->tif_clientdata, module,
             "%s: Bad value %f for \"%s\" tag",
             tif->tif_name, dblval,
		     fip->field_name);
        va_end(ap);
        }
    return (0);
}

/*
 * Return 1/0 according to whether or not
 * it is permissible to set the tag's value.
 * Note that we allow ImageLength to be changed
 * so that we can append and extend to images.
 * Any other tag may not be altered once writing
 * has commenced, unless its value has no effect
 * on the format of the data that is written.
 */
static int
OkToChangeTag(TIFF* tif, uint32 tag)
{
	SensorCall();const TIFFField* fip = TIFFFindField(tif, tag, TIFF_ANY);
	SensorCall();if (!fip) {			/* unknown tag */
		SensorCall();TIFFErrorExt(tif->tif_clientdata, "TIFFSetField", "%s: Unknown %stag %u",
		    tif->tif_name, isPseudoTag(tag) ? "pseudo-" : "", tag);
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();if (tag != TIFFTAG_IMAGELENGTH && (tif->tif_flags & TIFF_BEENWRITING) &&
	    !fip->field_oktochange) {
		/*
		 * Consult info table to see if tag can be changed
		 * after we've started writing.  We only allow changes
		 * to those tags that don't/shouldn't affect the
		 * compression and/or format of the data.
		 */
		SensorCall();TIFFErrorExt(tif->tif_clientdata, "TIFFSetField",
		    "%s: Cannot modify tag \"%s\" while writing",
		    tif->tif_name, fip->field_name);
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

/*
 * Record the value of a field in the
 * internal directory structure.  The
 * field will be written to the file
 * when/if the directory structure is
 * updated.
 */
int
TIFFSetField(TIFF* tif, uint32 tag, ...)
{
	SensorCall();va_list ap;
	int status;

	va_start(ap, tag);
	status = TIFFVSetField(tif, tag, ap);
	va_end(ap);
	{int  ReplaceReturn = (status); SensorCall(); return ReplaceReturn;}
}

/*
 * Clear the contents of the field in the internal structure.
 */
int
TIFFUnsetField(TIFF* tif, uint32 tag)
{
    SensorCall();const TIFFField *fip =  TIFFFieldWithTag(tif, tag);
    TIFFDirectory* td = &tif->tif_dir;

    SensorCall();if( !fip )
        {/*25*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*26*/}

    SensorCall();if( fip->field_bit != FIELD_CUSTOM )
        TIFFClrFieldBit(tif, fip->field_bit);
    else
    {
        SensorCall();TIFFTagValue *tv = NULL;
        int i;

        SensorCall();for (i = 0; i < td->td_customValueCount; i++) {
                
            SensorCall();tv = td->td_customValues + i;
            SensorCall();if( tv->info->field_tag == tag )
                {/*27*/SensorCall();break;/*28*/}
        }

        SensorCall();if( i < td->td_customValueCount )
        {
            SensorCall();_TIFFfree(tv->value);
            SensorCall();for( ; i < td->td_customValueCount-1; i++) {
                SensorCall();td->td_customValues[i] = td->td_customValues[i+1];
            }
            SensorCall();td->td_customValueCount--;
        }
    }
        
    SensorCall();tif->tif_flags |= TIFF_DIRTYDIRECT;

    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

/*
 * Like TIFFSetField, but taking a varargs
 * parameter list.  This routine is useful
 * for building higher-level interfaces on
 * top of the library.
 */
int
TIFFVSetField(TIFF* tif, uint32 tag, va_list ap)
{
	{int  ReplaceReturn = OkToChangeTag(tif, tag) ?
	    (*tif->tif_tagmethods.vsetfield)(tif, tag, ap) : 0; SensorCall(); return ReplaceReturn;}
}

static int
_TIFFVGetField(TIFF* tif, uint32 tag, va_list ap)
{
	SensorCall();TIFFDirectory* td = &tif->tif_dir;
	int ret_val = 1;
	uint32 standard_tag = tag;
	const TIFFField* fip = TIFFFindField(tif, tag, TIFF_ANY);
	
	/*
	 * We want to force the custom code to be used for custom
	 * fields even if the tag happens to match a well known 
	 * one - important for reinterpreted handling of standard
	 * tag values in custom directories (ie. EXIF) 
	 */
	SensorCall();if (fip->field_bit == FIELD_CUSTOM) {
		SensorCall();standard_tag = 0;
	}

	SensorCall();switch (standard_tag) {
		case TIFFTAG_SUBFILETYPE:
			SensorCall();*va_arg(ap, uint32*) = td->td_subfiletype;
			SensorCall();break;
		case TIFFTAG_IMAGEWIDTH:
			SensorCall();*va_arg(ap, uint32*) = td->td_imagewidth;
			SensorCall();break;
		case TIFFTAG_IMAGELENGTH:
			SensorCall();*va_arg(ap, uint32*) = td->td_imagelength;
			SensorCall();break;
		case TIFFTAG_BITSPERSAMPLE:
			SensorCall();*va_arg(ap, uint16*) = td->td_bitspersample;
			SensorCall();break;
		case TIFFTAG_COMPRESSION:
			SensorCall();*va_arg(ap, uint16*) = td->td_compression;
			SensorCall();break;
		case TIFFTAG_PHOTOMETRIC:
			SensorCall();*va_arg(ap, uint16*) = td->td_photometric;
			SensorCall();break;
		case TIFFTAG_THRESHHOLDING:
			SensorCall();*va_arg(ap, uint16*) = td->td_threshholding;
			SensorCall();break;
		case TIFFTAG_FILLORDER:
			SensorCall();*va_arg(ap, uint16*) = td->td_fillorder;
			SensorCall();break;
		case TIFFTAG_ORIENTATION:
			SensorCall();*va_arg(ap, uint16*) = td->td_orientation;
			SensorCall();break;
		case TIFFTAG_SAMPLESPERPIXEL:
			SensorCall();*va_arg(ap, uint16*) = td->td_samplesperpixel;
			SensorCall();break;
		case TIFFTAG_ROWSPERSTRIP:
			SensorCall();*va_arg(ap, uint32*) = td->td_rowsperstrip;
			SensorCall();break;
		case TIFFTAG_MINSAMPLEVALUE:
			SensorCall();*va_arg(ap, uint16*) = td->td_minsamplevalue;
			SensorCall();break;
		case TIFFTAG_MAXSAMPLEVALUE:
			SensorCall();*va_arg(ap, uint16*) = td->td_maxsamplevalue;
			SensorCall();break;
		case TIFFTAG_SMINSAMPLEVALUE:
			SensorCall();if (tif->tif_flags & TIFF_PERSAMPLE)
				{/*133*/SensorCall();*va_arg(ap, double**) = td->td_sminsamplevalue;/*134*/}
			else
			{
				/* libtiff historially treats this as a single value. */
				SensorCall();uint16 i;
				double v = td->td_sminsamplevalue[0];
				SensorCall();for (i=1; i < td->td_samplesperpixel; ++i)
					{/*135*/SensorCall();if( td->td_sminsamplevalue[i] < v )
						{/*137*/SensorCall();v = td->td_sminsamplevalue[i];/*138*/}/*136*/}
				SensorCall();*va_arg(ap, double*) = v;
			}
			SensorCall();break;
		case TIFFTAG_SMAXSAMPLEVALUE:
			SensorCall();if (tif->tif_flags & TIFF_PERSAMPLE)
				{/*139*/SensorCall();*va_arg(ap, double**) = td->td_smaxsamplevalue;/*140*/}
			else
			{
				/* libtiff historially treats this as a single value. */
				SensorCall();uint16 i;
				double v = td->td_smaxsamplevalue[0];
				SensorCall();for (i=1; i < td->td_samplesperpixel; ++i)
					{/*141*/SensorCall();if( td->td_smaxsamplevalue[i] > v )
						{/*143*/SensorCall();v = td->td_smaxsamplevalue[i];/*144*/}/*142*/}
				SensorCall();*va_arg(ap, double*) = v;
			}
			SensorCall();break;
		case TIFFTAG_XRESOLUTION:
			SensorCall();*va_arg(ap, float*) = td->td_xresolution;
			SensorCall();break;
		case TIFFTAG_YRESOLUTION:
			SensorCall();*va_arg(ap, float*) = td->td_yresolution;
			SensorCall();break;
		case TIFFTAG_PLANARCONFIG:
			SensorCall();*va_arg(ap, uint16*) = td->td_planarconfig;
			SensorCall();break;
		case TIFFTAG_XPOSITION:
			SensorCall();*va_arg(ap, float*) = td->td_xposition;
			SensorCall();break;
		case TIFFTAG_YPOSITION:
			SensorCall();*va_arg(ap, float*) = td->td_yposition;
			SensorCall();break;
		case TIFFTAG_RESOLUTIONUNIT:
			SensorCall();*va_arg(ap, uint16*) = td->td_resolutionunit;
			SensorCall();break;
		case TIFFTAG_PAGENUMBER:
			SensorCall();*va_arg(ap, uint16*) = td->td_pagenumber[0];
			*va_arg(ap, uint16*) = td->td_pagenumber[1];
			SensorCall();break;
		case TIFFTAG_HALFTONEHINTS:
			SensorCall();*va_arg(ap, uint16*) = td->td_halftonehints[0];
			*va_arg(ap, uint16*) = td->td_halftonehints[1];
			SensorCall();break;
		case TIFFTAG_COLORMAP:
			SensorCall();*va_arg(ap, uint16**) = td->td_colormap[0];
			*va_arg(ap, uint16**) = td->td_colormap[1];
			*va_arg(ap, uint16**) = td->td_colormap[2];
			SensorCall();break;
		case TIFFTAG_STRIPOFFSETS:
		case TIFFTAG_TILEOFFSETS:
			SensorCall();_TIFFFillStriles( tif );
			*va_arg(ap, uint64**) = td->td_stripoffset;
			SensorCall();break;
		case TIFFTAG_STRIPBYTECOUNTS:
		case TIFFTAG_TILEBYTECOUNTS:
			SensorCall();_TIFFFillStriles( tif );
			*va_arg(ap, uint64**) = td->td_stripbytecount;
			SensorCall();break;
		case TIFFTAG_MATTEING:
			SensorCall();*va_arg(ap, uint16*) =
			    (td->td_extrasamples == 1 &&
			    td->td_sampleinfo[0] == EXTRASAMPLE_ASSOCALPHA);
			SensorCall();break;
		case TIFFTAG_EXTRASAMPLES:
			SensorCall();*va_arg(ap, uint16*) = td->td_extrasamples;
			*va_arg(ap, uint16**) = td->td_sampleinfo;
			SensorCall();break;
		case TIFFTAG_TILEWIDTH:
			SensorCall();*va_arg(ap, uint32*) = td->td_tilewidth;
			SensorCall();break;
		case TIFFTAG_TILELENGTH:
			SensorCall();*va_arg(ap, uint32*) = td->td_tilelength;
			SensorCall();break;
		case TIFFTAG_TILEDEPTH:
			SensorCall();*va_arg(ap, uint32*) = td->td_tiledepth;
			SensorCall();break;
		case TIFFTAG_DATATYPE:
			SensorCall();switch (td->td_sampleformat) {
				case SAMPLEFORMAT_UINT:
					SensorCall();*va_arg(ap, uint16*) = DATATYPE_UINT;
					SensorCall();break;
				case SAMPLEFORMAT_INT:
					SensorCall();*va_arg(ap, uint16*) = DATATYPE_INT;
					SensorCall();break;
				case SAMPLEFORMAT_IEEEFP:
					SensorCall();*va_arg(ap, uint16*) = DATATYPE_IEEEFP;
					SensorCall();break;
				case SAMPLEFORMAT_VOID:
					SensorCall();*va_arg(ap, uint16*) = DATATYPE_VOID;
					SensorCall();break;
			}
			SensorCall();break;
		case TIFFTAG_SAMPLEFORMAT:
			SensorCall();*va_arg(ap, uint16*) = td->td_sampleformat;
			SensorCall();break;
		case TIFFTAG_IMAGEDEPTH:
			SensorCall();*va_arg(ap, uint32*) = td->td_imagedepth;
			SensorCall();break;
		case TIFFTAG_SUBIFD:
			SensorCall();*va_arg(ap, uint16*) = td->td_nsubifd;
			*va_arg(ap, uint64**) = td->td_subifd;
			SensorCall();break;
		case TIFFTAG_YCBCRPOSITIONING:
			SensorCall();*va_arg(ap, uint16*) = td->td_ycbcrpositioning;
			SensorCall();break;
		case TIFFTAG_YCBCRSUBSAMPLING:
			SensorCall();*va_arg(ap, uint16*) = td->td_ycbcrsubsampling[0];
			*va_arg(ap, uint16*) = td->td_ycbcrsubsampling[1];
			SensorCall();break;
		case TIFFTAG_TRANSFERFUNCTION:
			SensorCall();*va_arg(ap, uint16**) = td->td_transferfunction[0];
			SensorCall();if (td->td_samplesperpixel - td->td_extrasamples > 1) {
				SensorCall();*va_arg(ap, uint16**) = td->td_transferfunction[1];
				*va_arg(ap, uint16**) = td->td_transferfunction[2];
			}
			SensorCall();break;
		case TIFFTAG_REFERENCEBLACKWHITE:
			SensorCall();*va_arg(ap, float**) = td->td_refblackwhite;
			SensorCall();break;
		case TIFFTAG_INKNAMES:
			SensorCall();*va_arg(ap, char**) = td->td_inknames;
			SensorCall();break;
		default:
			{
				SensorCall();int i;

				/*
				 * This can happen if multiple images are open
				 * with different codecs which have private
				 * tags.  The global tag information table may
				 * then have tags that are valid for one file
				 * but not the other. If the client tries to
				 * get a tag that is not valid for the image's
				 * codec then we'll arrive here.
				 */
				SensorCall();if( fip == NULL || fip->field_bit != FIELD_CUSTOM )
				{
					SensorCall();TIFFErrorExt(tif->tif_clientdata, "_TIFFVGetField",
					    "%s: Invalid %stag \"%s\" "
					    "(not supported by codec)",
					    tif->tif_name,
					    isPseudoTag(tag) ? "pseudo-" : "",
					    fip ? fip->field_name : "Unknown");
					ret_val = 0;
					SensorCall();break;
				}

				/*
				 * Do we have a custom value?
				 */
				SensorCall();ret_val = 0;
				SensorCall();for (i = 0; i < td->td_customValueCount; i++) {
					SensorCall();TIFFTagValue *tv = td->td_customValues + i;

					SensorCall();if (tv->info->field_tag != tag)
						{/*145*/SensorCall();continue;/*146*/}

					SensorCall();if (fip->field_passcount) {
						SensorCall();if (fip->field_readcount == TIFF_VARIABLE2)
							{/*147*/SensorCall();*va_arg(ap, uint32*) = (uint32)tv->count;/*148*/}
						else  /* Assume TIFF_VARIABLE */
							{/*149*/SensorCall();*va_arg(ap, uint16*) = (uint16)tv->count;/*150*/}
						SensorCall();*va_arg(ap, void **) = tv->value;
						ret_val = 1;
					} else {/*151*/SensorCall();if (fip->field_tag == TIFFTAG_DOTRANGE
						   && strcmp(fip->field_name,"DotRange") == 0) {
						/* TODO: This is an evil exception and should not have been
						   handled this way ... likely best if we move it into
						   the directory structure with an explicit field in 
						   libtiff 4.1 and assign it a FIELD_ value */
						SensorCall();*va_arg(ap, uint16*) = ((uint16 *)tv->value)[0];
						*va_arg(ap, uint16*) = ((uint16 *)tv->value)[1];
						ret_val = 1;
					} else {
						SensorCall();if (fip->field_type == TIFF_ASCII
						    || fip->field_readcount == TIFF_VARIABLE
						    || fip->field_readcount == TIFF_VARIABLE2
						    || fip->field_readcount == TIFF_SPP
						    || tv->count > 1) {
							SensorCall();*va_arg(ap, void **) = tv->value;
							ret_val = 1;
						} else {
							SensorCall();char *val = (char *)tv->value;
							assert( tv->count == 1 );
							SensorCall();switch (fip->field_type) {
							case TIFF_BYTE:
							case TIFF_UNDEFINED:
								SensorCall();*va_arg(ap, uint8*) =
									*(uint8 *)val;
								ret_val = 1;
								SensorCall();break;
							case TIFF_SBYTE:
								SensorCall();*va_arg(ap, int8*) =
									*(int8 *)val;
								ret_val = 1;
								SensorCall();break;
							case TIFF_SHORT:
								SensorCall();*va_arg(ap, uint16*) =
									*(uint16 *)val;
								ret_val = 1;
								SensorCall();break;
							case TIFF_SSHORT:
								SensorCall();*va_arg(ap, int16*) =
									*(int16 *)val;
								ret_val = 1;
								SensorCall();break;
							case TIFF_LONG:
							case TIFF_IFD:
								SensorCall();*va_arg(ap, uint32*) =
									*(uint32 *)val;
								ret_val = 1;
								SensorCall();break;
							case TIFF_SLONG:
								SensorCall();*va_arg(ap, int32*) =
									*(int32 *)val;
								ret_val = 1;
								SensorCall();break;
							case TIFF_LONG8:
							case TIFF_IFD8:
								SensorCall();*va_arg(ap, uint64*) =
									*(uint64 *)val;
								ret_val = 1;
								SensorCall();break;
							case TIFF_SLONG8:
								SensorCall();*va_arg(ap, int64*) =
									*(int64 *)val;
								ret_val = 1;
								SensorCall();break;
							case TIFF_RATIONAL:
							case TIFF_SRATIONAL:
							case TIFF_FLOAT:
								SensorCall();*va_arg(ap, float*) =
									*(float *)val;
								ret_val = 1;
								SensorCall();break;
							case TIFF_DOUBLE:
								SensorCall();*va_arg(ap, double*) =
									*(double *)val;
								ret_val = 1;
								SensorCall();break;
							default:
								SensorCall();ret_val = 0;
								SensorCall();break;
							}
						}
					;/*152*/}}
					SensorCall();break;
				}
			}
	}
	SensorCall();return(ret_val);
}

/*
 * Return the value of a field in the
 * internal directory structure.
 */
int
TIFFGetField(TIFF* tif, uint32 tag, ...)
{
	SensorCall();int status;
	va_list ap;

	va_start(ap, tag);
	status = TIFFVGetField(tif, tag, ap);
	va_end(ap);
	{int  ReplaceReturn = (status); SensorCall(); return ReplaceReturn;}
}

/*
 * Like TIFFGetField, but taking a varargs
 * parameter list.  This routine is useful
 * for building higher-level interfaces on
 * top of the library.
 */
int
TIFFVGetField(TIFF* tif, uint32 tag, va_list ap)
{
	SensorCall();const TIFFField* fip = TIFFFindField(tif, tag, TIFF_ANY);
	{int  ReplaceReturn = (fip && (isPseudoTag(tag) || TIFFFieldSet(tif, fip->field_bit)) ?
	    (*tif->tif_tagmethods.vgetfield)(tif, tag, ap) : 0); SensorCall(); return ReplaceReturn;}
}

#define	CleanupField(member) {		\
    if (td->member) {			\
	_TIFFfree(td->member);		\
	td->member = 0;			\
    }					\
}

/*
 * Release storage associated with a directory.
 */
void
TIFFFreeDirectory(TIFF* tif)
{
	SensorCall();TIFFDirectory *td = &tif->tif_dir;
	int            i;

	_TIFFmemset(td->td_fieldsset, 0, FIELD_SETLONGS);
	CleanupField(td_sminsamplevalue);
	CleanupField(td_smaxsamplevalue);
	CleanupField(td_colormap[0]);
	CleanupField(td_colormap[1]);
	CleanupField(td_colormap[2]);
	CleanupField(td_sampleinfo);
	CleanupField(td_subifd);
	CleanupField(td_inknames);
	CleanupField(td_refblackwhite);
	CleanupField(td_transferfunction[0]);
	CleanupField(td_transferfunction[1]);
	CleanupField(td_transferfunction[2]);
	CleanupField(td_stripoffset);
	CleanupField(td_stripbytecount);
	TIFFClrFieldBit(tif, FIELD_YCBCRSUBSAMPLING);
	TIFFClrFieldBit(tif, FIELD_YCBCRPOSITIONING);

	/* Cleanup custom tag values */
	SensorCall();for( i = 0; i < td->td_customValueCount; i++ ) {
		SensorCall();if (td->td_customValues[i].value)
			{/*7*/SensorCall();_TIFFfree(td->td_customValues[i].value);/*8*/}
	}

	SensorCall();td->td_customValueCount = 0;
	CleanupField(td_customValues);

#if defined(DEFER_STRILE_LOAD)
        _TIFFmemset( &(td->td_stripoffset_entry), 0, sizeof(TIFFDirEntry));
        _TIFFmemset( &(td->td_stripbytecount_entry), 0, sizeof(TIFFDirEntry));
#endif        
}
#undef CleanupField

/*
 * Client Tag extension support (from Niles Ritter).
 */
static TIFFExtendProc _TIFFextender = (TIFFExtendProc) NULL;

TIFFExtendProc
TIFFSetTagExtender(TIFFExtendProc extender)
{
	SensorCall();TIFFExtendProc prev = _TIFFextender;
	_TIFFextender = extender;
	{TIFFExtendProc  ReplaceReturn = (prev); SensorCall(); return ReplaceReturn;}
}

/*
 * Setup for a new directory.  Should we automatically call
 * TIFFWriteDirectory() if the current one is dirty?
 *
 * The newly created directory will not exist on the file till
 * TIFFWriteDirectory(), TIFFFlush() or TIFFClose() is called.
 */
int
TIFFCreateDirectory(TIFF* tif)
{
	SensorCall();TIFFDefaultDirectory(tif);
	tif->tif_diroff = 0;
	tif->tif_nextdiroff = 0;
	tif->tif_curoff = 0;
	tif->tif_row = (uint32) -1;
	tif->tif_curstrip = (uint32) -1;

	{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

int
TIFFCreateCustomDirectory(TIFF* tif, const TIFFFieldArray* infoarray)
{
	SensorCall();TIFFDefaultDirectory(tif);

	/*
	 * Reset the field definitions to match the application provided list. 
	 * Hopefully TIFFDefaultDirectory() won't have done anything irreversable
	 * based on it's assumption this is an image directory.
	 */
	_TIFFSetupFields(tif, infoarray);

	tif->tif_diroff = 0;
	tif->tif_nextdiroff = 0;
	tif->tif_curoff = 0;
	tif->tif_row = (uint32) -1;
	tif->tif_curstrip = (uint32) -1;

	{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

int
TIFFCreateEXIFDirectory(TIFF* tif)
{
	SensorCall();const TIFFFieldArray* exifFieldArray;
	exifFieldArray = _TIFFGetExifFields();
	{int  ReplaceReturn = TIFFCreateCustomDirectory(tif, exifFieldArray); SensorCall(); return ReplaceReturn;}
}

/*
 * Setup a default directory structure.
 */
int
TIFFDefaultDirectory(TIFF* tif)
{
	SensorCall();register TIFFDirectory* td = &tif->tif_dir;
	const TIFFFieldArray* tiffFieldArray;

	tiffFieldArray = _TIFFGetFields();
	_TIFFSetupFields(tif, tiffFieldArray);   

	_TIFFmemset(td, 0, sizeof (*td));
	td->td_fillorder = FILLORDER_MSB2LSB;
	td->td_bitspersample = 1;
	td->td_threshholding = THRESHHOLD_BILEVEL;
	td->td_orientation = ORIENTATION_TOPLEFT;
	td->td_samplesperpixel = 1;
	td->td_rowsperstrip = (uint32) -1;
	td->td_tilewidth = 0;
	td->td_tilelength = 0;
	td->td_tiledepth = 1;
	td->td_stripbytecountsorted = 1; /* Our own arrays always sorted. */  
	td->td_resolutionunit = RESUNIT_INCH;
	td->td_sampleformat = SAMPLEFORMAT_UINT;
	td->td_imagedepth = 1;
	td->td_ycbcrsubsampling[0] = 2;
	td->td_ycbcrsubsampling[1] = 2;
	td->td_ycbcrpositioning = YCBCRPOSITION_CENTERED;
	tif->tif_postdecode = _TIFFNoPostDecode;  
	tif->tif_foundfield = NULL;
	tif->tif_tagmethods.vsetfield = _TIFFVSetField;  
	tif->tif_tagmethods.vgetfield = _TIFFVGetField;
	tif->tif_tagmethods.printdir = NULL;
	/*
	 *  Give client code a chance to install their own
	 *  tag extensions & methods, prior to compression overloads.
	 */
	SensorCall();if (_TIFFextender)
		{/*29*/SensorCall();(*_TIFFextender)(tif);/*30*/}
	SensorCall();(void) TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
	/*
	 * NB: The directory is marked dirty as a result of setting
	 * up the default compression scheme.  However, this really
	 * isn't correct -- we want TIFF_DIRTYDIRECT to be set only
	 * if the user does something.  We could just do the setup
	 * by hand, but it seems better to use the normal mechanism
	 * (i.e. TIFFSetField).
	 */
	tif->tif_flags &= ~TIFF_DIRTYDIRECT;

	/*
	 * As per http://bugzilla.remotesensing.org/show_bug.cgi?id=19
	 * we clear the ISTILED flag when setting up a new directory.
	 * Should we also be clearing stuff like INSUBIFD?
	 */
	tif->tif_flags &= ~TIFF_ISTILED;

	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

static int
TIFFAdvanceDirectory(TIFF* tif, uint64* nextdir, uint64* off)
{
	SensorCall();static const char module[] = "TIFFAdvanceDirectory";
	SensorCall();if (isMapped(tif))
	{
		SensorCall();uint64 poff=*nextdir;
		SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
		{
			SensorCall();tmsize_t poffa,poffb,poffc,poffd;
			uint16 dircount;
			uint32 nextdir32;
			poffa=(tmsize_t)poff;
			poffb=poffa+sizeof(uint16);
			SensorCall();if (((uint64)poffa!=poff)||(poffb<poffa)||(poffb<(tmsize_t)sizeof(uint16))||(poffb>tif->tif_size))
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Error fetching directory count");
				SensorCall();return(0);
			}
			SensorCall();_TIFFmemcpy(&dircount,tif->tif_base+poffa,sizeof(uint16));
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*153*/SensorCall();TIFFSwabShort(&dircount);/*154*/}
			SensorCall();poffc=poffb+dircount*12;
			poffd=poffc+sizeof(uint32);
			SensorCall();if ((poffc<poffb)||(poffc<dircount*12)||(poffd<poffc)||(poffd<(tmsize_t)sizeof(uint32))||(poffd>tif->tif_size))
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Error fetching directory link");
				SensorCall();return(0);
			}
			SensorCall();if (off!=NULL)
				{/*155*/SensorCall();*off=(uint64)poffc;/*156*/}
			SensorCall();_TIFFmemcpy(&nextdir32,tif->tif_base+poffc,sizeof(uint32));
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*157*/SensorCall();TIFFSwabLong(&nextdir32);/*158*/}
			SensorCall();*nextdir=nextdir32;
		}
		else
		{
			SensorCall();tmsize_t poffa,poffb,poffc,poffd;
			uint64 dircount64;
			uint16 dircount16;
			poffa=(tmsize_t)poff;
			poffb=poffa+sizeof(uint64);
			SensorCall();if (((uint64)poffa!=poff)||(poffb<poffa)||(poffb<(tmsize_t)sizeof(uint64))||(poffb>tif->tif_size))
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Error fetching directory count");
				SensorCall();return(0);
			}
			SensorCall();_TIFFmemcpy(&dircount64,tif->tif_base+poffa,sizeof(uint64));
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*159*/SensorCall();TIFFSwabLong8(&dircount64);/*160*/}
			SensorCall();if (dircount64>0xFFFF)
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Sanity check on directory count failed");
				SensorCall();return(0);
			}
			SensorCall();dircount16=(uint16)dircount64;
			poffc=poffb+dircount16*20;
			poffd=poffc+sizeof(uint64);
			SensorCall();if ((poffc<poffb)||(poffc<dircount16*20)||(poffd<poffc)||(poffd<(tmsize_t)sizeof(uint64))||(poffd>tif->tif_size))
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Error fetching directory link");
				SensorCall();return(0);
			}
			SensorCall();if (off!=NULL)
				{/*161*/SensorCall();*off=(uint64)poffc;/*162*/}
			SensorCall();_TIFFmemcpy(nextdir,tif->tif_base+poffc,sizeof(uint64));
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*163*/SensorCall();TIFFSwabLong8(nextdir);/*164*/}
		}
		SensorCall();return(1);
	}
	else
	{
		SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
		{
			SensorCall();uint16 dircount;
			uint32 nextdir32;
			SensorCall();if (!SeekOK(tif, *nextdir) ||
			    !ReadOK(tif, &dircount, sizeof (uint16))) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "%s: Error fetching directory count",
				    tif->tif_name);
				{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
			}
			SensorCall();if (tif->tif_flags & TIFF_SWAB)
				{/*165*/SensorCall();TIFFSwabShort(&dircount);/*166*/}
			SensorCall();if (off != NULL)
				*off = TIFFSeekFile(tif,
				    dircount*12, SEEK_CUR);
			else
				(void) TIFFSeekFile(tif,
				    dircount*12, SEEK_CUR);
			SensorCall();if (!ReadOK(tif, &nextdir32, sizeof (uint32))) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "%s: Error fetching directory link",
				    tif->tif_name);
				{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
			}
			SensorCall();if (tif->tif_flags & TIFF_SWAB)
				{/*167*/SensorCall();TIFFSwabLong(&nextdir32);/*168*/}
			SensorCall();*nextdir=nextdir32;
		}
		else
		{
			SensorCall();uint64 dircount64;
			uint16 dircount16;
			SensorCall();if (!SeekOK(tif, *nextdir) ||
			    !ReadOK(tif, &dircount64, sizeof (uint64))) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "%s: Error fetching directory count",
				    tif->tif_name);
				{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
			}
			SensorCall();if (tif->tif_flags & TIFF_SWAB)
				{/*169*/SensorCall();TIFFSwabLong8(&dircount64);/*170*/}
			SensorCall();if (dircount64>0xFFFF)
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "Error fetching directory count");
				SensorCall();return(0);
			}
			SensorCall();dircount16 = (uint16)dircount64;
			SensorCall();if (off != NULL)
				*off = TIFFSeekFile(tif,
				    dircount16*20, SEEK_CUR);
			else
				(void) TIFFSeekFile(tif,
				    dircount16*20, SEEK_CUR);
			SensorCall();if (!ReadOK(tif, nextdir, sizeof (uint64))) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "%s: Error fetching directory link",
				    tif->tif_name);
				{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
			}
			SensorCall();if (tif->tif_flags & TIFF_SWAB)
				{/*171*/SensorCall();TIFFSwabLong8(nextdir);/*172*/}
		}
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	}
SensorCall();}

/*
 * Count the number of directories in a file.
 */
uint16
TIFFNumberOfDirectories(TIFF* tif)
{
	SensorCall();uint64 nextdir;
	uint16 n;
	SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
		{/*1*/SensorCall();nextdir = tif->tif_header.classic.tiff_diroff;/*2*/}
	else
		{/*3*/SensorCall();nextdir = tif->tif_header.big.tiff_diroff;/*4*/}
	SensorCall();n = 0;
	SensorCall();while (nextdir != 0 && TIFFAdvanceDirectory(tif, &nextdir, NULL))
		{/*5*/SensorCall();n++;/*6*/}
	{uint16  ReplaceReturn = (n); SensorCall(); return ReplaceReturn;}
}

/*
 * Set the n-th directory as the current directory.
 * NB: Directories are numbered starting at 0.
 */
int
TIFFSetDirectory(TIFF* tif, uint16 dirn)
{
	SensorCall();uint64 nextdir;
	uint16 n;

	SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
		{/*9*/SensorCall();nextdir = tif->tif_header.classic.tiff_diroff;/*10*/}
	else
		{/*11*/SensorCall();nextdir = tif->tif_header.big.tiff_diroff;/*12*/}
	SensorCall();for (n = dirn; n > 0 && nextdir != 0; n--)
		{/*13*/SensorCall();if (!TIFFAdvanceDirectory(tif, &nextdir, NULL))
			{/*15*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*16*/}/*14*/}
	SensorCall();tif->tif_nextdiroff = nextdir;
	/*
	 * Set curdir to the actual directory index.  The
	 * -1 is because TIFFReadDirectory will increment
	 * tif_curdir after successfully reading the directory.
	 */
	tif->tif_curdir = (dirn - n) - 1;
	/*
	 * Reset tif_dirnumber counter and start new list of seen directories.
	 * We need this to prevent IFD loops.
	 */
	tif->tif_dirnumber = 0;
	{int  ReplaceReturn = (TIFFReadDirectory(tif)); SensorCall(); return ReplaceReturn;}
}

/*
 * Set the current directory to be the directory
 * located at the specified file offset.  This interface
 * is used mainly to access directories linked with
 * the SubIFD tag (e.g. thumbnail images).
 */
int
TIFFSetSubDirectory(TIFF* tif, uint64 diroff)
{
	SensorCall();tif->tif_nextdiroff = diroff;
	/*
	 * Reset tif_dirnumber counter and start new list of seen directories.
	 * We need this to prevent IFD loops.
	 */
	tif->tif_dirnumber = 0;
	{int  ReplaceReturn = (TIFFReadDirectory(tif)); SensorCall(); return ReplaceReturn;}
}

/*
 * Return file offset of the current directory.
 */
uint64
TIFFCurrentDirOffset(TIFF* tif)
{
	{uint64  ReplaceReturn = (tif->tif_diroff); SensorCall(); return ReplaceReturn;}
}

/*
 * Return an indication of whether or not we are
 * at the last directory in the file.
 */
int
TIFFLastDirectory(TIFF* tif)
{
	{int  ReplaceReturn = (tif->tif_nextdiroff == 0); SensorCall(); return ReplaceReturn;}
}

/*
 * Unlink the specified directory from the directory chain.
 */
int
TIFFUnlinkDirectory(TIFF* tif, uint16 dirn)
{
	SensorCall();static const char module[] = "TIFFUnlinkDirectory";
	uint64 nextdir;
	uint64 off;
	uint16 n;

	SensorCall();if (tif->tif_mode == O_RDONLY) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
                             "Can not unlink directory in read-only file");
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
	/*
	 * Go to the directory before the one we want
	 * to unlink and nab the offset of the link
	 * field we'll need to patch.
	 */
	SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
	{
		SensorCall();nextdir = tif->tif_header.classic.tiff_diroff;
		off = 4;
	}
	else
	{
		SensorCall();nextdir = tif->tif_header.big.tiff_diroff;
		off = 8;
	}
	SensorCall();for (n = dirn-1; n > 0; n--) {
		SensorCall();if (nextdir == 0) {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "Directory %d does not exist", dirn);
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
		SensorCall();if (!TIFFAdvanceDirectory(tif, &nextdir, &off))
			{/*17*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*18*/}
	}
	/*
	 * Advance to the directory to be unlinked and fetch
	 * the offset of the directory that follows.
	 */
	SensorCall();if (!TIFFAdvanceDirectory(tif, &nextdir, NULL))
		{/*19*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*20*/}
	/*
	 * Go back and patch the link field of the preceding
	 * directory to point to the offset of the directory
	 * that follows.
	 */
	SensorCall();(void) TIFFSeekFile(tif, off, SEEK_SET);
	SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
	{
		SensorCall();uint32 nextdir32;
		nextdir32=(uint32)nextdir;
		assert((uint64)nextdir32==nextdir);
		SensorCall();if (tif->tif_flags & TIFF_SWAB)
			{/*21*/SensorCall();TIFFSwabLong(&nextdir32);/*22*/}
		SensorCall();if (!WriteOK(tif, &nextdir32, sizeof (uint32))) {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "Error writing directory link");
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
	}
	else
	{
		SensorCall();if (tif->tif_flags & TIFF_SWAB)
			{/*23*/SensorCall();TIFFSwabLong8(&nextdir);/*24*/}
		SensorCall();if (!WriteOK(tif, &nextdir, sizeof (uint64))) {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "Error writing directory link");
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
	}
	/*
	 * Leave directory state setup safely.  We don't have
	 * facilities for doing inserting and removing directories,
	 * so it's safest to just invalidate everything.  This
	 * means that the caller can only append to the directory
	 * chain.
	 */
	SensorCall();(*tif->tif_cleanup)(tif);
	SensorCall();if ((tif->tif_flags & TIFF_MYBUFFER) && tif->tif_rawdata) {
		SensorCall();_TIFFfree(tif->tif_rawdata);
		tif->tif_rawdata = NULL;
		tif->tif_rawcc = 0;
                tif->tif_rawdataoff = 0;
                tif->tif_rawdataloaded = 0;
	}
	SensorCall();tif->tif_flags &= ~(TIFF_BEENWRITING|TIFF_BUFFERSETUP|TIFF_POSTENCODE|TIFF_BUF4WRITE);
	TIFFFreeDirectory(tif);
	TIFFDefaultDirectory(tif);
	tif->tif_diroff = 0;			/* force link on next write */
	tif->tif_nextdiroff = 0;		/* next write must be at end */
	tif->tif_curoff = 0;
	tif->tif_row = (uint32) -1;
	tif->tif_curstrip = (uint32) -1;
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
