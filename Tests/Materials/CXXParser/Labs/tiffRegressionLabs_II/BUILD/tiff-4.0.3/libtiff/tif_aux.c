/* $Id: tif_aux.c,v 1.26 2010-07-01 15:33:28 dron Exp $ */

/*
 * Copyright (c) 1991-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

/*
 * TIFF Library.
 *
 * Auxiliary Support Routines.
 */
#include "tiffiop.h"
#include "/var/tmp/sensor.h"
#include "tif_predict.h"
#include <math.h>

uint32
_TIFFMultiply32(TIFF* tif, uint32 first, uint32 second, const char* where)
{
	SensorCall();uint32 bytes = first * second;

	SensorCall();if (second && bytes / second != first) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, where, "Integer overflow in %s", where);
		bytes = 0;
	}

	{uint32  ReplaceReturn = bytes; SensorCall(); return ReplaceReturn;}
}

uint64
_TIFFMultiply64(TIFF* tif, uint64 first, uint64 second, const char* where)
{
	SensorCall();uint64 bytes = first * second;

	SensorCall();if (second && bytes / second != first) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, where, "Integer overflow in %s", where);
		bytes = 0;
	}

	{uint64  ReplaceReturn = bytes; SensorCall(); return ReplaceReturn;}
}

void*
_TIFFCheckRealloc(TIFF* tif, void* buffer,
		  tmsize_t nmemb, tmsize_t elem_size, const char* what)
{
	SensorCall();void* cp = NULL;
	tmsize_t bytes = nmemb * elem_size;

	/*
	 * XXX: Check for integer overflow.
	 */
	SensorCall();if (nmemb && elem_size && bytes / elem_size == nmemb)
		{/*5*/SensorCall();cp = _TIFFrealloc(buffer, bytes);/*6*/}

	SensorCall();if (cp == NULL) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, tif->tif_name,
			     "Failed to allocate memory for %s "
			     "(%ld elements of %ld bytes each)",
			     what,(long) nmemb, (long) elem_size);
	}

	{void * ReplaceReturn = cp; SensorCall(); return ReplaceReturn;}
}

void*
_TIFFCheckMalloc(TIFF* tif, tmsize_t nmemb, tmsize_t elem_size, const char* what)
{
	{void * ReplaceReturn = _TIFFCheckRealloc(tif, NULL, nmemb, elem_size, what); SensorCall(); return ReplaceReturn;}  
}

static int
TIFFDefaultTransferFunction(TIFFDirectory* td)
{
	SensorCall();uint16 **tf = td->td_transferfunction;
	tmsize_t i, n, nbytes;

	tf[0] = tf[1] = tf[2] = 0;
	SensorCall();if (td->td_bitspersample >= sizeof(tmsize_t) * 8 - 2)
		{/*7*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*8*/}

	SensorCall();n = ((tmsize_t)1)<<td->td_bitspersample;
	nbytes = n * sizeof (uint16);
	SensorCall();if (!(tf[0] = (uint16 *)_TIFFmalloc(nbytes)))
		{/*9*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*10*/}
	SensorCall();tf[0][0] = 0;
	SensorCall();for (i = 1; i < n; i++) {
		SensorCall();double t = (double)i/((double) n-1.);
		tf[0][i] = (uint16)floor(65535.*pow(t, 2.2) + .5);
	}

	SensorCall();if (td->td_samplesperpixel - td->td_extrasamples > 1) {
		SensorCall();if (!(tf[1] = (uint16 *)_TIFFmalloc(nbytes)))
			{/*11*/SensorCall();goto bad;/*12*/}
		SensorCall();_TIFFmemcpy(tf[1], tf[0], nbytes);
		SensorCall();if (!(tf[2] = (uint16 *)_TIFFmalloc(nbytes)))
			{/*13*/SensorCall();goto bad;/*14*/}
		SensorCall();_TIFFmemcpy(tf[2], tf[0], nbytes);
	}
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}

bad:
	if (tf[0])
		{/*15*/_TIFFfree(tf[0]);/*16*/}
	if (tf[1])
		{/*17*/_TIFFfree(tf[1]);/*18*/}
	if (tf[2])
		{/*19*/_TIFFfree(tf[2]);/*20*/}
	tf[0] = tf[1] = tf[2] = 0;
	return 0;
}

static int
TIFFDefaultRefBlackWhite(TIFFDirectory* td)
{
	SensorCall();int i;

	SensorCall();if (!(td->td_refblackwhite = (float *)_TIFFmalloc(6*sizeof (float))))
		{/*21*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*22*/}
        SensorCall();if (td->td_photometric == PHOTOMETRIC_YCBCR) {
		/*
		 * YCbCr (Class Y) images must have the ReferenceBlackWhite
		 * tag set. Fix the broken images, which lacks that tag.
		 */
		SensorCall();td->td_refblackwhite[0] = 0.0F;
		td->td_refblackwhite[1] = td->td_refblackwhite[3] =
			td->td_refblackwhite[5] = 255.0F;
		td->td_refblackwhite[2] = td->td_refblackwhite[4] = 128.0F;
	} else {
		/*
		 * Assume RGB (Class R)
		 */
		SensorCall();for (i = 0; i < 3; i++) {
		    SensorCall();td->td_refblackwhite[2*i+0] = 0;
		    td->td_refblackwhite[2*i+1] =
			    (float)((1L<<td->td_bitspersample)-1L);
		}
	}
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

/*
 * Like TIFFGetField, but return any default
 * value if the tag is not present in the directory.
 *
 * NB:	We use the value in the directory, rather than
 *	explcit values so that defaults exist only one
 *	place in the library -- in TIFFDefaultDirectory.
 */
int
TIFFVGetFieldDefaulted(TIFF* tif, uint32 tag, va_list ap)
{
	SensorCall();TIFFDirectory *td = &tif->tif_dir;

	SensorCall();if (TIFFVGetField(tif, tag, ap))
		{/*1*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*2*/}
	SensorCall();switch (tag) {
	case TIFFTAG_SUBFILETYPE:
		SensorCall();*va_arg(ap, uint32 *) = td->td_subfiletype;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_BITSPERSAMPLE:
		SensorCall();*va_arg(ap, uint16 *) = td->td_bitspersample;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_THRESHHOLDING:
		SensorCall();*va_arg(ap, uint16 *) = td->td_threshholding;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_FILLORDER:
		SensorCall();*va_arg(ap, uint16 *) = td->td_fillorder;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_ORIENTATION:
		SensorCall();*va_arg(ap, uint16 *) = td->td_orientation;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_SAMPLESPERPIXEL:
		SensorCall();*va_arg(ap, uint16 *) = td->td_samplesperpixel;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_ROWSPERSTRIP:
		SensorCall();*va_arg(ap, uint32 *) = td->td_rowsperstrip;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_MINSAMPLEVALUE:
		SensorCall();*va_arg(ap, uint16 *) = td->td_minsamplevalue;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_MAXSAMPLEVALUE:
		SensorCall();*va_arg(ap, uint16 *) = td->td_maxsamplevalue;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_PLANARCONFIG:
		SensorCall();*va_arg(ap, uint16 *) = td->td_planarconfig;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_RESOLUTIONUNIT:
		SensorCall();*va_arg(ap, uint16 *) = td->td_resolutionunit;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_PREDICTOR:
                {
			SensorCall();TIFFPredictorState* sp = (TIFFPredictorState*) tif->tif_data;
			*va_arg(ap, uint16*) = (uint16) sp->predictor;
			{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
                }
	case TIFFTAG_DOTRANGE:
		SensorCall();*va_arg(ap, uint16 *) = 0;
		*va_arg(ap, uint16 *) = (1<<td->td_bitspersample)-1;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_INKSET:
		SensorCall();*va_arg(ap, uint16 *) = INKSET_CMYK;
		{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
	case TIFFTAG_NUMBEROFINKS:
		SensorCall();*va_arg(ap, uint16 *) = 4;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_EXTRASAMPLES:
		SensorCall();*va_arg(ap, uint16 *) = td->td_extrasamples;
		*va_arg(ap, uint16 **) = td->td_sampleinfo;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_MATTEING:
		SensorCall();*va_arg(ap, uint16 *) =
		    (td->td_extrasamples == 1 &&
		     td->td_sampleinfo[0] == EXTRASAMPLE_ASSOCALPHA);
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_TILEDEPTH:
		SensorCall();*va_arg(ap, uint32 *) = td->td_tiledepth;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_DATATYPE:
		SensorCall();*va_arg(ap, uint16 *) = td->td_sampleformat-1;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_SAMPLEFORMAT:
		SensorCall();*va_arg(ap, uint16 *) = td->td_sampleformat;
                SensorCall();return(1);
	case TIFFTAG_IMAGEDEPTH:
		SensorCall();*va_arg(ap, uint32 *) = td->td_imagedepth;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_YCBCRCOEFFICIENTS:
		{
			/* defaults are from CCIR Recommendation 601-1 */
			SensorCall();static float ycbcrcoeffs[] = { 0.299f, 0.587f, 0.114f };
			*va_arg(ap, float **) = ycbcrcoeffs;
			{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
		}
	case TIFFTAG_YCBCRSUBSAMPLING:
		SensorCall();*va_arg(ap, uint16 *) = td->td_ycbcrsubsampling[0];
		*va_arg(ap, uint16 *) = td->td_ycbcrsubsampling[1];
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_YCBCRPOSITIONING:
		SensorCall();*va_arg(ap, uint16 *) = td->td_ycbcrpositioning;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_WHITEPOINT:
		{
			SensorCall();static float whitepoint[2];

			/* TIFF 6.0 specification tells that it is no default
			   value for the WhitePoint, but AdobePhotoshop TIFF
			   Technical Note tells that it should be CIE D50. */
			whitepoint[0] =	D50_X0 / (D50_X0 + D50_Y0 + D50_Z0);
			whitepoint[1] =	D50_Y0 / (D50_X0 + D50_Y0 + D50_Z0);
			*va_arg(ap, float **) = whitepoint;
			{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
		}
	case TIFFTAG_TRANSFERFUNCTION:
		SensorCall();if (!td->td_transferfunction[0] &&
		    !TIFFDefaultTransferFunction(td)) {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, tif->tif_name, "No space for \"TransferFunction\" tag");
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
		SensorCall();*va_arg(ap, uint16 **) = td->td_transferfunction[0];
		SensorCall();if (td->td_samplesperpixel - td->td_extrasamples > 1) {
			SensorCall();*va_arg(ap, uint16 **) = td->td_transferfunction[1];
			*va_arg(ap, uint16 **) = td->td_transferfunction[2];
		}
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	case TIFFTAG_REFERENCEBLACKWHITE:
		SensorCall();if (!td->td_refblackwhite && !TIFFDefaultRefBlackWhite(td))
			{/*3*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*4*/}
		SensorCall();*va_arg(ap, float **) = td->td_refblackwhite;
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	}
	{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

/*
 * Like TIFFGetField, but return any default
 * value if the tag is not present in the directory.
 */
int
TIFFGetFieldDefaulted(TIFF* tif, uint32 tag, ...)
{
	SensorCall();int ok;
	va_list ap;

	va_start(ap, tag);
	ok =  TIFFVGetFieldDefaulted(tif, tag, ap);
	va_end(ap);
	{int  ReplaceReturn = (ok); SensorCall(); return ReplaceReturn;}
}

struct _Int64Parts {
	int32 low, high;
};

typedef union {
	struct _Int64Parts part;
	int64 value;
} _Int64;

float
_TIFFUInt64ToFloat(uint64 ui64)
{
	SensorCall();_Int64 i;

	i.value = ui64;
	SensorCall();if (i.part.high >= 0) {
		{float  ReplaceReturn = (float)i.value; SensorCall(); return ReplaceReturn;}
	} else {
		SensorCall();long double df;
		df = (long double)i.value;
		df += 18446744073709551616.0; /* adding 2**64 */
		{float  ReplaceReturn = (float)df; SensorCall(); return ReplaceReturn;}
	}
SensorCall();}

double
_TIFFUInt64ToDouble(uint64 ui64)
{
	SensorCall();_Int64 i;

	i.value = ui64;
	SensorCall();if (i.part.high >= 0) {
		{double  ReplaceReturn = (double)i.value; SensorCall(); return ReplaceReturn;}
	} else {
		SensorCall();long double df;
		df = (long double)i.value;
		df += 18446744073709551616.0; /* adding 2**64 */
		{double  ReplaceReturn = (double)df; SensorCall(); return ReplaceReturn;}
	}
SensorCall();}

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
