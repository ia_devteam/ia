/* $Id: tif_tile.c,v 1.23 2012-06-06 05:33:55 fwarmerdam Exp $ */

/*
 * Copyright (c) 1991-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

/*
 * TIFF Library.
 *
 * Tiled Image Support Routines.
 */
#include "tiffiop.h"
#include "/var/tmp/sensor.h"

/*
 * Compute which tile an (x,y,z,s) value is in.
 */
uint32
TIFFComputeTile(TIFF* tif, uint32 x, uint32 y, uint32 z, uint16 s)
{
	SensorCall();TIFFDirectory *td = &tif->tif_dir;
	uint32 dx = td->td_tilewidth;
	uint32 dy = td->td_tilelength;
	uint32 dz = td->td_tiledepth;
	uint32 tile = 1;

	SensorCall();if (td->td_imagedepth == 1)
		{/*9*/SensorCall();z = 0;/*10*/}
	SensorCall();if (dx == (uint32) -1)
		{/*11*/SensorCall();dx = td->td_imagewidth;/*12*/}
	SensorCall();if (dy == (uint32) -1)
		{/*13*/SensorCall();dy = td->td_imagelength;/*14*/}
	SensorCall();if (dz == (uint32) -1)
		{/*15*/SensorCall();dz = td->td_imagedepth;/*16*/}
	SensorCall();if (dx != 0 && dy != 0 && dz != 0) {
		SensorCall();uint32 xpt = TIFFhowmany_32(td->td_imagewidth, dx);
		uint32 ypt = TIFFhowmany_32(td->td_imagelength, dy);
		uint32 zpt = TIFFhowmany_32(td->td_imagedepth, dz);

		SensorCall();if (td->td_planarconfig == PLANARCONFIG_SEPARATE) 
			{/*17*/SensorCall();tile = (xpt*ypt*zpt)*s +
			     (xpt*ypt)*(z/dz) +
			     xpt*(y/dy) +
			     x/dx;/*18*/}
		else
			{/*19*/SensorCall();tile = (xpt*ypt)*(z/dz) + xpt*(y/dy) + x/dx;/*20*/}
	}
	{uint32  ReplaceReturn = (tile); SensorCall(); return ReplaceReturn;}
}

/*
 * Check an (x,y,z,s) coordinate
 * against the image bounds.
 */
int
TIFFCheckTile(TIFF* tif, uint32 x, uint32 y, uint32 z, uint16 s)
{
	SensorCall();TIFFDirectory *td = &tif->tif_dir;

	SensorCall();if (x >= td->td_imagewidth) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, tif->tif_name,
			     "%lu: Col out of range, max %lu",
			     (unsigned long) x,
			     (unsigned long) (td->td_imagewidth - 1));
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();if (y >= td->td_imagelength) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, tif->tif_name,
			     "%lu: Row out of range, max %lu",
			     (unsigned long) y,
			     (unsigned long) (td->td_imagelength - 1));
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();if (z >= td->td_imagedepth) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, tif->tif_name,
			     "%lu: Depth out of range, max %lu",
			     (unsigned long) z,
			     (unsigned long) (td->td_imagedepth - 1));
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();if (td->td_planarconfig == PLANARCONFIG_SEPARATE &&
	    s >= td->td_samplesperpixel) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, tif->tif_name,
			     "%lu: Sample out of range, max %lu",
			     (unsigned long) s,
			     (unsigned long) (td->td_samplesperpixel - 1));
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

/*
 * Compute how many tiles are in an image.
 */
uint32
TIFFNumberOfTiles(TIFF* tif)
{
	SensorCall();TIFFDirectory *td = &tif->tif_dir;
	uint32 dx = td->td_tilewidth;
	uint32 dy = td->td_tilelength;
	uint32 dz = td->td_tiledepth;
	uint32 ntiles;

	SensorCall();if (dx == (uint32) -1)
		{/*21*/SensorCall();dx = td->td_imagewidth;/*22*/}
	SensorCall();if (dy == (uint32) -1)
		{/*23*/SensorCall();dy = td->td_imagelength;/*24*/}
	SensorCall();if (dz == (uint32) -1)
		{/*25*/SensorCall();dz = td->td_imagedepth;/*26*/}
	SensorCall();ntiles = (dx == 0 || dy == 0 || dz == 0) ? 0 :
	    _TIFFMultiply32(tif, _TIFFMultiply32(tif, TIFFhowmany_32(td->td_imagewidth, dx),
	    TIFFhowmany_32(td->td_imagelength, dy),
	    "TIFFNumberOfTiles"),
	    TIFFhowmany_32(td->td_imagedepth, dz), "TIFFNumberOfTiles");
	SensorCall();if (td->td_planarconfig == PLANARCONFIG_SEPARATE)
		{/*27*/SensorCall();ntiles = _TIFFMultiply32(tif, ntiles, td->td_samplesperpixel,
		    "TIFFNumberOfTiles");/*28*/}
	{uint32  ReplaceReturn = (ntiles); SensorCall(); return ReplaceReturn;}
}

/*
 * Compute the # bytes in each row of a tile.
 */
uint64
TIFFTileRowSize64(TIFF* tif)
{
	SensorCall();TIFFDirectory *td = &tif->tif_dir;
	uint64 rowsize;

	SensorCall();if (td->td_tilelength == 0 || td->td_tilewidth == 0)
		{/*1*/{uint64  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*2*/}
	SensorCall();rowsize = _TIFFMultiply64(tif, td->td_bitspersample, td->td_tilewidth,
	    "TIFFTileRowSize");
	SensorCall();if (td->td_planarconfig == PLANARCONFIG_CONTIG)
		{/*3*/SensorCall();rowsize = _TIFFMultiply64(tif, rowsize, td->td_samplesperpixel,
		    "TIFFTileRowSize");/*4*/}
	{uint64  ReplaceReturn = (TIFFhowmany8_64(rowsize)); SensorCall(); return ReplaceReturn;}
}
tmsize_t
TIFFTileRowSize(TIFF* tif)
{
	SensorCall();static const char module[] = "TIFFTileRowSize";
	uint64 m;
	tmsize_t n;
	m=TIFFTileRowSize64(tif);
	n=(tmsize_t)m;
	SensorCall();if ((uint64)n!=m)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Integer overflow");
		n=0;
	}
	SensorCall();return(n);
}

/*
 * Compute the # bytes in a variable length, row-aligned tile.
 */
uint64
TIFFVTileSize64(TIFF* tif, uint32 nrows)
{
	SensorCall();static const char module[] = "TIFFVTileSize64";
	TIFFDirectory *td = &tif->tif_dir;
	SensorCall();if (td->td_tilelength == 0 || td->td_tilewidth == 0 ||
	    td->td_tiledepth == 0)
		{/*5*/{uint64  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*6*/}
	SensorCall();if ((td->td_planarconfig==PLANARCONFIG_CONTIG)&&
	    (td->td_photometric==PHOTOMETRIC_YCBCR)&&
	    (td->td_samplesperpixel==3)&&
	    (!isUpSampled(tif)))
	{
		/*
		 * Packed YCbCr data contain one Cb+Cr for every
		 * HorizontalSampling*VerticalSampling Y values.
		 * Must also roundup width and height when calculating
		 * since images that are not a multiple of the
		 * horizontal/vertical subsampling area include
		 * YCbCr data for the extended image.
		 */
		SensorCall();uint16 ycbcrsubsampling[2];
		uint16 samplingblock_samples;
		uint32 samplingblocks_hor;
		uint32 samplingblocks_ver;
		uint64 samplingrow_samples;
		uint64 samplingrow_size;
		TIFFGetFieldDefaulted(tif,TIFFTAG_YCBCRSUBSAMPLING,ycbcrsubsampling+0,
		    ycbcrsubsampling+1);
		SensorCall();if ((ycbcrsubsampling[0] != 1 && ycbcrsubsampling[0] != 2 && ycbcrsubsampling[0] != 4)
		    ||(ycbcrsubsampling[1] != 1 && ycbcrsubsampling[1] != 2 && ycbcrsubsampling[1] != 4))
		{
			SensorCall();TIFFErrorExt(tif->tif_clientdata,module,
				     "Invalid YCbCr subsampling (%dx%d)", 
				     ycbcrsubsampling[0], 
				     ycbcrsubsampling[1] );
			{uint64  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
		}
		SensorCall();samplingblock_samples=ycbcrsubsampling[0]*ycbcrsubsampling[1]+2;
		samplingblocks_hor=TIFFhowmany_32(td->td_tilewidth,ycbcrsubsampling[0]);
		samplingblocks_ver=TIFFhowmany_32(nrows,ycbcrsubsampling[1]);
		samplingrow_samples=_TIFFMultiply64(tif,samplingblocks_hor,samplingblock_samples,module);
		samplingrow_size=TIFFhowmany8_64(_TIFFMultiply64(tif,samplingrow_samples,td->td_bitspersample,module));
		SensorCall();return(_TIFFMultiply64(tif,samplingrow_size,samplingblocks_ver,module));
	}
	else
		{/*7*/SensorCall();return(_TIFFMultiply64(tif,nrows,TIFFTileRowSize64(tif),module));/*8*/}
SensorCall();}
tmsize_t
TIFFVTileSize(TIFF* tif, uint32 nrows)
{
	SensorCall();static const char module[] = "TIFFVTileSize";
	uint64 m;
	tmsize_t n;
	m=TIFFVTileSize64(tif,nrows);
	n=(tmsize_t)m;
	SensorCall();if ((uint64)n!=m)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Integer overflow");
		n=0;
	}
	SensorCall();return(n);
}

/*
 * Compute the # bytes in a row-aligned tile.
 */
uint64
TIFFTileSize64(TIFF* tif)
{
	{uint64  ReplaceReturn = (TIFFVTileSize64(tif, tif->tif_dir.td_tilelength)); SensorCall(); return ReplaceReturn;}
}
tmsize_t
TIFFTileSize(TIFF* tif)
{
	SensorCall();static const char module[] = "TIFFTileSize";
	uint64 m;
	tmsize_t n;
	m=TIFFTileSize64(tif);
	n=(tmsize_t)m;
	SensorCall();if ((uint64)n!=m)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Integer overflow");
		n=0;
	}
	SensorCall();return(n);
}

/*
 * Compute a default tile size based on the image
 * characteristics and a requested value.  If a
 * request is <1 then we choose a size according
 * to certain heuristics.
 */
void
TIFFDefaultTileSize(TIFF* tif, uint32* tw, uint32* th)
{
	SensorCall();(*tif->tif_deftilesize)(tif, tw, th);
SensorCall();}

void
_TIFFDefaultTileSize(TIFF* tif, uint32* tw, uint32* th)
{
	SensorCall();(void) tif;
	SensorCall();if (*(int32*) tw < 1)
		{/*29*/SensorCall();*tw = 256;/*30*/}
	SensorCall();if (*(int32*) th < 1)
		{/*31*/SensorCall();*th = 256;/*32*/}
	/* roundup to a multiple of 16 per the spec */
	SensorCall();if (*tw & 0xf)
		*tw = TIFFroundup_32(*tw, 16);
	SensorCall();if (*th & 0xf)
		*th = TIFFroundup_32(*th, 16);
SensorCall();}

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
