/* $Id: tif_compress.c,v 1.22 2010-03-10 18:56:48 bfriesen Exp $ */

/*
 * Copyright (c) 1988-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

/*
 * TIFF Library
 *
 * Compression Scheme Configuration Support.
 */
#include "tiffiop.h"
#include "/var/tmp/sensor.h"

static int
TIFFNoEncode(TIFF* tif, const char* method)
{
	SensorCall();const TIFFCodec* c = TIFFFindCODEC(tif->tif_dir.td_compression);

	SensorCall();if (c) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, tif->tif_name,
			     "%s %s encoding is not implemented",
			     c->name, method);
	} else {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, tif->tif_name,
			"Compression scheme %u %s encoding is not implemented",
			     tif->tif_dir.td_compression, method);
	}
	{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
}

int
_TIFFNoRowEncode(TIFF* tif, uint8* pp, tmsize_t cc, uint16 s)
{
	SensorCall();(void) pp; (void) cc; (void) s;
	{int  ReplaceReturn = (TIFFNoEncode(tif, "scanline")); SensorCall(); return ReplaceReturn;}
}

int
_TIFFNoStripEncode(TIFF* tif, uint8* pp, tmsize_t cc, uint16 s)
{
	SensorCall();(void) pp; (void) cc; (void) s;
	{int  ReplaceReturn = (TIFFNoEncode(tif, "strip")); SensorCall(); return ReplaceReturn;}
}

int
_TIFFNoTileEncode(TIFF* tif, uint8* pp, tmsize_t cc, uint16 s)
{
	SensorCall();(void) pp; (void) cc; (void) s;
	{int  ReplaceReturn = (TIFFNoEncode(tif, "tile")); SensorCall(); return ReplaceReturn;}
}

static int
TIFFNoDecode(TIFF* tif, const char* method)
{
	SensorCall();const TIFFCodec* c = TIFFFindCODEC(tif->tif_dir.td_compression);

	SensorCall();if (c)
		{/*11*/SensorCall();TIFFErrorExt(tif->tif_clientdata, tif->tif_name,
			     "%s %s decoding is not implemented",
			     c->name, method);/*12*/}
	else
		{/*13*/SensorCall();TIFFErrorExt(tif->tif_clientdata, tif->tif_name,
			     "Compression scheme %u %s decoding is not implemented",
			     tif->tif_dir.td_compression, method);/*14*/}
	{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
}

int
_TIFFNoFixupTags(TIFF* tif)
{
	SensorCall();(void) tif;
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

int
_TIFFNoRowDecode(TIFF* tif, uint8* pp, tmsize_t cc, uint16 s)
{
	SensorCall();(void) pp; (void) cc; (void) s;
	{int  ReplaceReturn = (TIFFNoDecode(tif, "scanline")); SensorCall(); return ReplaceReturn;}
}

int
_TIFFNoStripDecode(TIFF* tif, uint8* pp, tmsize_t cc, uint16 s)
{
	SensorCall();(void) pp; (void) cc; (void) s;
	{int  ReplaceReturn = (TIFFNoDecode(tif, "strip")); SensorCall(); return ReplaceReturn;}
}

int
_TIFFNoTileDecode(TIFF* tif, uint8* pp, tmsize_t cc, uint16 s)
{
	SensorCall();(void) pp; (void) cc; (void) s;
	{int  ReplaceReturn = (TIFFNoDecode(tif, "tile")); SensorCall(); return ReplaceReturn;}
}

int
_TIFFNoSeek(TIFF* tif, uint32 off)
{
	SensorCall();(void) off;
	TIFFErrorExt(tif->tif_clientdata, tif->tif_name,
		     "Compression algorithm does not support random access");
	{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
}

int
_TIFFNoPreCode(TIFF* tif, uint16 s)
{
	SensorCall();(void) tif; (void) s;
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

static int _TIFFtrue(TIFF* tif) { SensorCall();(void) tif; {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;} }
static void _TIFFvoid(TIFF* tif) { SensorCall();(void) tif; SensorCall();}

void
_TIFFSetDefaultCompressionState(TIFF* tif)
{
	SensorCall();tif->tif_fixuptags = _TIFFNoFixupTags; 
	tif->tif_decodestatus = TRUE;
	tif->tif_setupdecode = _TIFFtrue;
	tif->tif_predecode = _TIFFNoPreCode;
	tif->tif_decoderow = _TIFFNoRowDecode;  
	tif->tif_decodestrip = _TIFFNoStripDecode;
	tif->tif_decodetile = _TIFFNoTileDecode;  
	tif->tif_encodestatus = TRUE;
	tif->tif_setupencode = _TIFFtrue;
	tif->tif_preencode = _TIFFNoPreCode;
	tif->tif_postencode = _TIFFtrue;
	tif->tif_encoderow = _TIFFNoRowEncode;
	tif->tif_encodestrip = _TIFFNoStripEncode;  
	tif->tif_encodetile = _TIFFNoTileEncode;  
	tif->tif_close = _TIFFvoid;
	tif->tif_seek = _TIFFNoSeek;
	tif->tif_cleanup = _TIFFvoid;
	tif->tif_defstripsize = _TIFFDefaultStripSize;
	tif->tif_deftilesize = _TIFFDefaultTileSize;
	tif->tif_flags &= ~(TIFF_NOBITREV|TIFF_NOREADRAW);
SensorCall();}

int
TIFFSetCompressionScheme(TIFF* tif, int scheme)
{
	SensorCall();const TIFFCodec *c = TIFFFindCODEC((uint16) scheme);

	_TIFFSetDefaultCompressionState(tif);
	/*
	 * Don't treat an unknown compression scheme as an error.
	 * This permits applications to open files with data that
	 * the library does not have builtin support for, but which
	 * may still be meaningful.
	 */
	{int  ReplaceReturn = (c ? (*c->init)(tif, scheme) : 1); SensorCall(); return ReplaceReturn;}
}

/*
 * Other compression schemes may be registered.  Registered
 * schemes can also override the builtin versions provided
 * by this library.
 */
typedef struct _codec {
	struct _codec* next;
	TIFFCodec* info;
} codec_t;
static codec_t* registeredCODECS = NULL;

const TIFFCodec*
TIFFFindCODEC(uint16 scheme)
{
	SensorCall();const TIFFCodec* c;
	codec_t* cd;

	SensorCall();for (cd = registeredCODECS; cd; cd = cd->next)
		{/*1*/SensorCall();if (cd->info->scheme == scheme)
			{/*3*/{const TIFFCodec * ReplaceReturn = ((const TIFFCodec*) cd->info); SensorCall(); return ReplaceReturn;}/*4*/}/*2*/}
	SensorCall();for (c = _TIFFBuiltinCODECS; c->name; c++)
		{/*5*/SensorCall();if (c->scheme == scheme)
			{/*7*/{const TIFFCodec * ReplaceReturn = (c); SensorCall(); return ReplaceReturn;}/*8*/}/*6*/}
	{const TIFFCodec * ReplaceReturn = ((const TIFFCodec*) 0); SensorCall(); return ReplaceReturn;}
}

TIFFCodec*
TIFFRegisterCODEC(uint16 scheme, const char* name, TIFFInitMethod init)
{
	SensorCall();codec_t* cd = (codec_t*)
	    _TIFFmalloc((tmsize_t)(sizeof (codec_t) + sizeof (TIFFCodec) + strlen(name)+1));

	SensorCall();if (cd != NULL) {
		SensorCall();cd->info = (TIFFCodec*) ((uint8*) cd + sizeof (codec_t));
		cd->info->name = (char*)
		    ((uint8*) cd->info + sizeof (TIFFCodec));
		strcpy(cd->info->name, name);
		cd->info->scheme = scheme;
		cd->info->init = init;
		cd->next = registeredCODECS;
		registeredCODECS = cd;
	} else {
		SensorCall();TIFFErrorExt(0, "TIFFRegisterCODEC",
		    "No space to register compression scheme %s", name);
		{TIFFCodec * ReplaceReturn = NULL; SensorCall(); return ReplaceReturn;}
	}
	{TIFFCodec * ReplaceReturn = (cd->info); SensorCall(); return ReplaceReturn;}
}

void
TIFFUnRegisterCODEC(TIFFCodec* c)
{
	SensorCall();codec_t* cd;
	codec_t** pcd;

	SensorCall();for (pcd = &registeredCODECS; (cd = *pcd); pcd = &cd->next)
		{/*9*/SensorCall();if (cd->info == c) {
			SensorCall();*pcd = cd->next;
			_TIFFfree(cd);
			SensorCall();return;
		;/*10*/}}
	SensorCall();TIFFErrorExt(0, "TIFFUnRegisterCODEC",
	    "Cannot remove compression scheme %s; not registered", c->name);
SensorCall();}

/************************************************************************/
/*                       TIFFGetConfisuredCODECs()                      */
/************************************************************************/

/**
 * Get list of configured codecs, both built-in and registered by user.
 * Caller is responsible to free this structure.
 * 
 * @return returns array of TIFFCodec records (the last record should be NULL)
 * or NULL if function failed.
 */

TIFFCodec*
TIFFGetConfiguredCODECs()
{
	SensorCall();int i = 1;
	codec_t *cd;
	const TIFFCodec* c;
	TIFFCodec* codecs = NULL;
	TIFFCodec* new_codecs;

	SensorCall();for (cd = registeredCODECS; cd; cd = cd->next) {
		SensorCall();new_codecs = (TIFFCodec *)
			_TIFFrealloc(codecs, i * sizeof(TIFFCodec));
		SensorCall();if (!new_codecs) {
			SensorCall();_TIFFfree (codecs);
			{TIFFCodec * ReplaceReturn = NULL; SensorCall(); return ReplaceReturn;}
		}
		SensorCall();codecs = new_codecs;
		_TIFFmemcpy(codecs + i - 1, cd, sizeof(TIFFCodec));
		i++;
	}
	SensorCall();for (c = _TIFFBuiltinCODECS; c->name; c++) {
		SensorCall();if (TIFFIsCODECConfigured(c->scheme)) {
			SensorCall();new_codecs = (TIFFCodec *)
				_TIFFrealloc(codecs, i * sizeof(TIFFCodec));
			SensorCall();if (!new_codecs) {
				SensorCall();_TIFFfree (codecs);
				{TIFFCodec * ReplaceReturn = NULL; SensorCall(); return ReplaceReturn;}
			}
			SensorCall();codecs = new_codecs;
			_TIFFmemcpy(codecs + i - 1, (const void*)c, sizeof(TIFFCodec));
			i++;
		}
	}

	SensorCall();new_codecs = (TIFFCodec *) _TIFFrealloc(codecs, i * sizeof(TIFFCodec));
	SensorCall();if (!new_codecs) {
		SensorCall();_TIFFfree (codecs);
		{TIFFCodec * ReplaceReturn = NULL; SensorCall(); return ReplaceReturn;}
	}
	SensorCall();codecs = new_codecs;
	_TIFFmemset(codecs + i - 1, 0, sizeof(TIFFCodec));

	{TIFFCodec * ReplaceReturn = codecs; SensorCall(); return ReplaceReturn;}
}

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
