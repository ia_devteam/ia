/* $Id: tif_jpeg.c,v 1.111 2012-07-06 18:48:04 bfriesen Exp $ */

/*
 * Copyright (c) 1994-1997 Sam Leffler
 * Copyright (c) 1994-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN

#include "tiffiop.h"
#include "/var/tmp/sensor.h"
#ifdef JPEG_SUPPORT

/*
 * TIFF Library
 *
 * JPEG Compression support per TIFF Technical Note #2
 * (*not* per the original TIFF 6.0 spec).
 *
 * This file is simply an interface to the libjpeg library written by
 * the Independent JPEG Group.  You need release 5 or later of the IJG
 * code, which you can find on the Internet at ftp.uu.net:/graphics/jpeg/.
 *
 * Contributed by Tom Lane <tgl@sss.pgh.pa.us>.
 */
#include <setjmp.h>

int TIFFFillStrip(TIFF* tif, uint32 strip);
int TIFFFillTile(TIFF* tif, uint32 tile);
int TIFFReInitJPEG_12( TIFF *tif, int scheme, int is_encode );

/* We undefine FAR to avoid conflict with JPEG definition */

#ifdef FAR
#undef FAR
#endif

/*
  Libjpeg's jmorecfg.h defines INT16 and INT32, but only if XMD_H is
  not defined.  Unfortunately, the MinGW and Borland compilers include
  a typedef for INT32, which causes a conflict.  MSVC does not include
  a conficting typedef given the headers which are included.
*/
#if defined(__BORLANDC__) || defined(__MINGW32__)
# define XMD_H 1
#endif

/*
   The windows RPCNDR.H file defines boolean, but defines it with the
   unsigned char size.  You should compile JPEG library using appropriate
   definitions in jconfig.h header, but many users compile library in wrong
   way. That causes errors of the following type:

   "JPEGLib: JPEG parameter struct mismatch: library thinks size is 432,
   caller expects 464"

   For such users we wil fix the problem here. See install.doc file from
   the JPEG library distribution for details.
*/

/* Define "boolean" as unsigned char, not int, per Windows custom. */
#if defined(__WIN32__) && !defined(__MINGW32__)
# ifndef __RPCNDR_H__            /* don't conflict if rpcndr.h already read */
   typedef unsigned char boolean;
# endif
# define HAVE_BOOLEAN            /* prevent jmorecfg.h from redefining it */
#endif

#include "jpeglib.h"
#include "jerror.h"

/* 
 * Do we want to do special processing suitable for when JSAMPLE is a
 * 16bit value?  
 */

#if defined(JPEG_LIB_MK1)
#  define JPEG_LIB_MK1_OR_12BIT 1
#elif BITS_IN_JSAMPLE == 12
#  define JPEG_LIB_MK1_OR_12BIT 1
#endif

/*
 * We are using width_in_blocks which is supposed to be private to
 * libjpeg. Unfortunately, the libjpeg delivered with Cygwin has
 * renamed this member to width_in_data_units.  Since the header has
 * also renamed a define, use that unique define name in order to
 * detect the problem header and adjust to suit.
 */
#if defined(D_MAX_DATA_UNITS_IN_MCU)
#define width_in_blocks width_in_data_units
#endif

/*
 * On some machines it may be worthwhile to use _setjmp or sigsetjmp
 * in place of plain setjmp.  These macros will make it easier.
 */
#define SETJMP(jbuf)		setjmp(jbuf)
#define LONGJMP(jbuf,code)	longjmp(jbuf,code)
#define JMP_BUF			jmp_buf

typedef struct jpeg_destination_mgr jpeg_destination_mgr;
typedef struct jpeg_source_mgr jpeg_source_mgr;
typedef struct jpeg_error_mgr jpeg_error_mgr;

/*
 * State block for each open TIFF file using
 * libjpeg to do JPEG compression/decompression.
 *
 * libjpeg's visible state is either a jpeg_compress_struct
 * or jpeg_decompress_struct depending on which way we
 * are going.  comm can be used to refer to the fields
 * which are common to both.
 *
 * NB: cinfo is required to be the first member of JPEGState,
 *     so we can safely cast JPEGState* -> jpeg_xxx_struct*
 *     and vice versa!
 */
typedef struct {
	union {
		struct jpeg_compress_struct c;
		struct jpeg_decompress_struct d;
		struct jpeg_common_struct comm;
	} cinfo;			/* NB: must be first */
	int             cinfo_initialized;

	jpeg_error_mgr	err;		/* libjpeg error manager */
	JMP_BUF		exit_jmpbuf;	/* for catching libjpeg failures */
	/*
	 * The following two members could be a union, but
	 * they're small enough that it's not worth the effort.
	 */
	jpeg_destination_mgr dest;	/* data dest for compression */
	jpeg_source_mgr	src;		/* data source for decompression */
					/* private state */
	TIFF*		tif;		/* back link needed by some code */
	uint16		photometric;	/* copy of PhotometricInterpretation */
	uint16		h_sampling;	/* luminance sampling factors */
	uint16		v_sampling;
	tmsize_t   	bytesperline;	/* decompressed bytes per scanline */
	/* pointers to intermediate buffers when processing downsampled data */
	JSAMPARRAY	ds_buffer[MAX_COMPONENTS];
	int		scancount;	/* number of "scanlines" accumulated */
	int		samplesperclump;

	TIFFVGetMethod	vgetparent;	/* super-class method */
	TIFFVSetMethod	vsetparent;	/* super-class method */
	TIFFPrintMethod printdir;	/* super-class method */
	TIFFStripMethod	defsparent;	/* super-class method */
	TIFFTileMethod	deftparent;	/* super-class method */
					/* pseudo-tag fields */
	void*		jpegtables;	/* JPEGTables tag value, or NULL */
	uint32		jpegtables_length; /* number of bytes in same */
	int		jpegquality;	/* Compression quality level */
	int		jpegcolormode;	/* Auto RGB<=>YCbCr convert? */
	int		jpegtablesmode;	/* What to put in JPEGTables */

        int             ycbcrsampling_fetched;
} JPEGState;

#define	JState(tif)	((JPEGState*)(tif)->tif_data)

static int JPEGDecode(TIFF* tif, uint8* buf, tmsize_t cc, uint16 s);
static int JPEGDecodeRaw(TIFF* tif, uint8* buf, tmsize_t cc, uint16 s);
static int JPEGEncode(TIFF* tif, uint8* buf, tmsize_t cc, uint16 s);
static int JPEGEncodeRaw(TIFF* tif, uint8* buf, tmsize_t cc, uint16 s);
static int JPEGInitializeLibJPEG(TIFF * tif, int decode );
static int DecodeRowError(TIFF* tif, uint8* buf, tmsize_t cc, uint16 s);

#define	FIELD_JPEGTABLES	(FIELD_CODEC+0)

static const TIFFField jpegFields[] = {
    { TIFFTAG_JPEGTABLES, -3, -3, TIFF_UNDEFINED, 0, TIFF_SETGET_C32_UINT8, TIFF_SETGET_C32_UINT8, FIELD_JPEGTABLES, FALSE, TRUE, "JPEGTables", NULL },
    { TIFFTAG_JPEGQUALITY, 0, 0, TIFF_ANY, 0, TIFF_SETGET_INT, TIFF_SETGET_UNDEFINED, FIELD_PSEUDO, TRUE, FALSE, "", NULL },
    { TIFFTAG_JPEGCOLORMODE, 0, 0, TIFF_ANY, 0, TIFF_SETGET_INT, TIFF_SETGET_UNDEFINED, FIELD_PSEUDO, FALSE, FALSE, "", NULL },
    { TIFFTAG_JPEGTABLESMODE, 0, 0, TIFF_ANY, 0, TIFF_SETGET_INT, TIFF_SETGET_UNDEFINED, FIELD_PSEUDO, FALSE, FALSE, "", NULL }
};

/*
 * libjpeg interface layer.
 *
 * We use setjmp/longjmp to return control to libtiff
 * when a fatal error is encountered within the JPEG
 * library.  We also direct libjpeg error and warning
 * messages through the appropriate libtiff handlers.
 */

/*
 * Error handling routines (these replace corresponding
 * IJG routines from jerror.c).  These are used for both
 * compression and decompression.
 */
static void
TIFFjpeg_error_exit(j_common_ptr cinfo)
{
	SensorCall();JPEGState *sp = (JPEGState *) cinfo;	/* NB: cinfo assumed first */
	char buffer[JMSG_LENGTH_MAX];

	(*cinfo->err->format_message) (cinfo, buffer);
	TIFFErrorExt(sp->tif->tif_clientdata, "JPEGLib", "%s", buffer);		/* display the error message */
	jpeg_abort(cinfo);			/* clean up libjpeg state */
	LONGJMP(sp->exit_jmpbuf, 1);		/* return to libtiff caller */
}

/*
 * This routine is invoked only for warning messages,
 * since error_exit does its own thing and trace_level
 * is never set > 0.
 */
static void
TIFFjpeg_output_message(j_common_ptr cinfo)
{
	SensorCall();char buffer[JMSG_LENGTH_MAX];

	(*cinfo->err->format_message) (cinfo, buffer);
	TIFFWarningExt(((JPEGState *) cinfo)->tif->tif_clientdata, "JPEGLib", "%s", buffer);
SensorCall();}

/*
 * Interface routines.  This layer of routines exists
 * primarily to limit side-effects from using setjmp.
 * Also, normal/error returns are converted into return
 * values per libtiff practice.
 */
#define	CALLJPEG(sp, fail, op)	(SETJMP((sp)->exit_jmpbuf) ? (fail) : (op))
#define	CALLVJPEG(sp, op)	CALLJPEG(sp, 0, ((op),1))

static int
TIFFjpeg_create_compress(JPEGState* sp)
{
	/* initialize JPEG error handling */
	SensorCall();sp->cinfo.c.err = jpeg_std_error(&sp->err);
	sp->err.error_exit = TIFFjpeg_error_exit;
	sp->err.output_message = TIFFjpeg_output_message;

	{int  ReplaceReturn = CALLVJPEG(sp, jpeg_create_compress(&sp->cinfo.c)); SensorCall(); return ReplaceReturn;}
}

static int
TIFFjpeg_create_decompress(JPEGState* sp)
{
	/* initialize JPEG error handling */
	SensorCall();sp->cinfo.d.err = jpeg_std_error(&sp->err);
	sp->err.error_exit = TIFFjpeg_error_exit;
	sp->err.output_message = TIFFjpeg_output_message;

	{int  ReplaceReturn = CALLVJPEG(sp, jpeg_create_decompress(&sp->cinfo.d)); SensorCall(); return ReplaceReturn;}
}

static int
TIFFjpeg_set_defaults(JPEGState* sp)
{
	{int  ReplaceReturn = CALLVJPEG(sp, jpeg_set_defaults(&sp->cinfo.c)); SensorCall(); return ReplaceReturn;}
}

static int
TIFFjpeg_set_colorspace(JPEGState* sp, J_COLOR_SPACE colorspace)
{
	{int  ReplaceReturn = CALLVJPEG(sp, jpeg_set_colorspace(&sp->cinfo.c, colorspace)); SensorCall(); return ReplaceReturn;}
}

static int
TIFFjpeg_set_quality(JPEGState* sp, int quality, boolean force_baseline)
{
	{int  ReplaceReturn = CALLVJPEG(sp,
	    jpeg_set_quality(&sp->cinfo.c, quality, force_baseline)); SensorCall(); return ReplaceReturn;}
}

static int
TIFFjpeg_suppress_tables(JPEGState* sp, boolean suppress)
{
	{int  ReplaceReturn = CALLVJPEG(sp, jpeg_suppress_tables(&sp->cinfo.c, suppress)); SensorCall(); return ReplaceReturn;}
}

static int
TIFFjpeg_start_compress(JPEGState* sp, boolean write_all_tables)
{
	{int  ReplaceReturn = CALLVJPEG(sp,
	    jpeg_start_compress(&sp->cinfo.c, write_all_tables)); SensorCall(); return ReplaceReturn;}
}

static int
TIFFjpeg_write_scanlines(JPEGState* sp, JSAMPARRAY scanlines, int num_lines)
{
	{int  ReplaceReturn = CALLJPEG(sp, -1, (int) jpeg_write_scanlines(&sp->cinfo.c,
	    scanlines, (JDIMENSION) num_lines)); SensorCall(); return ReplaceReturn;}
}

static int
TIFFjpeg_write_raw_data(JPEGState* sp, JSAMPIMAGE data, int num_lines)
{
	{int  ReplaceReturn = CALLJPEG(sp, -1, (int) jpeg_write_raw_data(&sp->cinfo.c,
	    data, (JDIMENSION) num_lines)); SensorCall(); return ReplaceReturn;}
}

static int
TIFFjpeg_finish_compress(JPEGState* sp)
{
	{int  ReplaceReturn = CALLVJPEG(sp, jpeg_finish_compress(&sp->cinfo.c)); SensorCall(); return ReplaceReturn;}
}

static int
TIFFjpeg_write_tables(JPEGState* sp)
{
	{int  ReplaceReturn = CALLVJPEG(sp, jpeg_write_tables(&sp->cinfo.c)); SensorCall(); return ReplaceReturn;}
}

static int
TIFFjpeg_read_header(JPEGState* sp, boolean require_image)
{
	{int  ReplaceReturn = CALLJPEG(sp, -1, jpeg_read_header(&sp->cinfo.d, require_image)); SensorCall(); return ReplaceReturn;}
}

static int
TIFFjpeg_start_decompress(JPEGState* sp)
{
	{int  ReplaceReturn = CALLVJPEG(sp, jpeg_start_decompress(&sp->cinfo.d)); SensorCall(); return ReplaceReturn;}
}

static int
TIFFjpeg_read_scanlines(JPEGState* sp, JSAMPARRAY scanlines, int max_lines)
{
	{int  ReplaceReturn = CALLJPEG(sp, -1, (int) jpeg_read_scanlines(&sp->cinfo.d,
	    scanlines, (JDIMENSION) max_lines)); SensorCall(); return ReplaceReturn;}
}

static int
TIFFjpeg_read_raw_data(JPEGState* sp, JSAMPIMAGE data, int max_lines)
{
	{int  ReplaceReturn = CALLJPEG(sp, -1, (int) jpeg_read_raw_data(&sp->cinfo.d,
	    data, (JDIMENSION) max_lines)); SensorCall(); return ReplaceReturn;}
}

static int
TIFFjpeg_finish_decompress(JPEGState* sp)
{
	{int  ReplaceReturn = CALLJPEG(sp, -1, (int) jpeg_finish_decompress(&sp->cinfo.d)); SensorCall(); return ReplaceReturn;}
}

static int
TIFFjpeg_abort(JPEGState* sp)
{
	{int  ReplaceReturn = CALLVJPEG(sp, jpeg_abort(&sp->cinfo.comm)); SensorCall(); return ReplaceReturn;}
}

static int
TIFFjpeg_destroy(JPEGState* sp)
{
	{int  ReplaceReturn = CALLVJPEG(sp, jpeg_destroy(&sp->cinfo.comm)); SensorCall(); return ReplaceReturn;}
}

static JSAMPARRAY
TIFFjpeg_alloc_sarray(JPEGState* sp, int pool_id,
		      JDIMENSION samplesperrow, JDIMENSION numrows)
{
	{JSAMPARRAY  ReplaceReturn = CALLJPEG(sp, (JSAMPARRAY) NULL,
	    (*sp->cinfo.comm.mem->alloc_sarray)
		(&sp->cinfo.comm, pool_id, samplesperrow, numrows)); SensorCall(); return ReplaceReturn;}
}

/*
 * JPEG library destination data manager.
 * These routines direct compressed data from libjpeg into the
 * libtiff output buffer.
 */

static void
std_init_destination(j_compress_ptr cinfo)
{
	SensorCall();JPEGState* sp = (JPEGState*) cinfo;
	TIFF* tif = sp->tif;

	sp->dest.next_output_byte = (JOCTET*) tif->tif_rawdata;
	sp->dest.free_in_buffer = (size_t) tif->tif_rawdatasize;
SensorCall();}

static boolean
std_empty_output_buffer(j_compress_ptr cinfo)
{
	SensorCall();JPEGState* sp = (JPEGState*) cinfo;
	TIFF* tif = sp->tif;

	/* the entire buffer has been filled */
	tif->tif_rawcc = tif->tif_rawdatasize;

#ifdef IPPJ_HUFF
       /*
        * The Intel IPP performance library does not necessarily fill up
        * the whole output buffer on each pass, so only dump out the parts
        * that have been filled.
        *   http://trac.osgeo.org/gdal/wiki/JpegIPP
        */
       if ( sp->dest.free_in_buffer >= 0 ) {
               tif->tif_rawcc = tif->tif_rawdatasize - sp->dest.free_in_buffer;
       }
#endif

	TIFFFlushData1(tif);
	sp->dest.next_output_byte = (JOCTET*) tif->tif_rawdata;
	sp->dest.free_in_buffer = (size_t) tif->tif_rawdatasize;

	{boolean  ReplaceReturn = (TRUE); SensorCall(); return ReplaceReturn;}
}

static void
std_term_destination(j_compress_ptr cinfo)
{
	SensorCall();JPEGState* sp = (JPEGState*) cinfo;
	TIFF* tif = sp->tif;

	tif->tif_rawcp = (uint8*) sp->dest.next_output_byte;
	tif->tif_rawcc =
	    tif->tif_rawdatasize - (tmsize_t) sp->dest.free_in_buffer;
	/* NB: libtiff does the final buffer flush */
SensorCall();}

static void
TIFFjpeg_data_dest(JPEGState* sp, TIFF* tif)
{
	SensorCall();(void) tif;
	sp->cinfo.c.dest = &sp->dest;
	sp->dest.init_destination = std_init_destination;
	sp->dest.empty_output_buffer = std_empty_output_buffer;
	sp->dest.term_destination = std_term_destination;
SensorCall();}

/*
 * Alternate destination manager for outputting to JPEGTables field.
 */

static void
tables_init_destination(j_compress_ptr cinfo)
{
	SensorCall();JPEGState* sp = (JPEGState*) cinfo;

	/* while building, jpegtables_length is allocated buffer size */
	sp->dest.next_output_byte = (JOCTET*) sp->jpegtables;
	sp->dest.free_in_buffer = (size_t) sp->jpegtables_length;
SensorCall();}

static boolean
tables_empty_output_buffer(j_compress_ptr cinfo)
{
	SensorCall();JPEGState* sp = (JPEGState*) cinfo;
	void* newbuf;

	/* the entire buffer has been filled; enlarge it by 1000 bytes */
	newbuf = _TIFFrealloc((void*) sp->jpegtables,
			      (tmsize_t) (sp->jpegtables_length + 1000));
	SensorCall();if (newbuf == NULL)
		ERREXIT1(cinfo, JERR_OUT_OF_MEMORY, 100);
	SensorCall();sp->dest.next_output_byte = (JOCTET*) newbuf + sp->jpegtables_length;
	sp->dest.free_in_buffer = (size_t) 1000;
	sp->jpegtables = newbuf;
	sp->jpegtables_length += 1000;
	{boolean  ReplaceReturn = (TRUE); SensorCall(); return ReplaceReturn;}
}

static void
tables_term_destination(j_compress_ptr cinfo)
{
	SensorCall();JPEGState* sp = (JPEGState*) cinfo;

	/* set tables length to number of bytes actually emitted */
	sp->jpegtables_length -= (uint32) sp->dest.free_in_buffer;
SensorCall();}

static int
TIFFjpeg_tables_dest(JPEGState* sp, TIFF* tif)
{
	SensorCall();(void) tif;
	/*
	 * Allocate a working buffer for building tables.
	 * Initial size is 1000 bytes, which is usually adequate.
	 */
	SensorCall();if (sp->jpegtables)
		{/*45*/SensorCall();_TIFFfree(sp->jpegtables);/*46*/}
	SensorCall();sp->jpegtables_length = 1000;
	sp->jpegtables = (void*) _TIFFmalloc((tmsize_t) sp->jpegtables_length);
	SensorCall();if (sp->jpegtables == NULL) {
		SensorCall();sp->jpegtables_length = 0;
		TIFFErrorExt(sp->tif->tif_clientdata, "TIFFjpeg_tables_dest", "No space for JPEGTables");
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();sp->cinfo.c.dest = &sp->dest;
	sp->dest.init_destination = tables_init_destination;
	sp->dest.empty_output_buffer = tables_empty_output_buffer;
	sp->dest.term_destination = tables_term_destination;
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

/*
 * JPEG library source data manager.
 * These routines supply compressed data to libjpeg.
 */

static void
std_init_source(j_decompress_ptr cinfo)
{
	SensorCall();JPEGState* sp = (JPEGState*) cinfo;
	TIFF* tif = sp->tif;

	sp->src.next_input_byte = (const JOCTET*) tif->tif_rawdata;
	sp->src.bytes_in_buffer = (size_t) tif->tif_rawcc;
SensorCall();}

static boolean
std_fill_input_buffer(j_decompress_ptr cinfo)
{
	SensorCall();JPEGState* sp = (JPEGState* ) cinfo;
	static const JOCTET dummy_EOI[2] = { 0xFF, JPEG_EOI };

#ifdef IPPJ_HUFF
        /*
         * The Intel IPP performance library does not necessarily read the whole
         * input buffer in one pass, so it is possible to get here with data
         * yet to read. 
         * 
         * We just return without doing anything, until the entire buffer has
         * been read.  
         * http://trac.osgeo.org/gdal/wiki/JpegIPP
         */
        if( sp->src.bytes_in_buffer > 0 ) {
            return (TRUE);
        }
#endif

	/*
         * Normally the whole strip/tile is read and so we don't need to do
         * a fill.  In the case of CHUNKY_STRIP_READ_SUPPORT we might not have
         * all the data, but the rawdata is refreshed between scanlines and
         * we push this into the io machinery in JPEGDecode(). 	 
         * http://trac.osgeo.org/gdal/ticket/3894
	 */
        
	WARNMS(cinfo, JWRN_JPEG_EOF);
	/* insert a fake EOI marker */
	sp->src.next_input_byte = dummy_EOI;
	sp->src.bytes_in_buffer = 2;
	{boolean  ReplaceReturn = (TRUE); SensorCall(); return ReplaceReturn;}
}

static void
std_skip_input_data(j_decompress_ptr cinfo, long num_bytes)
{
	SensorCall();JPEGState* sp = (JPEGState*) cinfo;

	SensorCall();if (num_bytes > 0) {
		SensorCall();if ((size_t)num_bytes > sp->src.bytes_in_buffer) {
			/* oops, buffer overrun */
			SensorCall();(void) std_fill_input_buffer(cinfo);
		} else {
			SensorCall();sp->src.next_input_byte += (size_t) num_bytes;
			sp->src.bytes_in_buffer -= (size_t) num_bytes;
		}
	}
SensorCall();}

static void
std_term_source(j_decompress_ptr cinfo)
{
	/* No work necessary here */
	SensorCall();(void) cinfo;
SensorCall();}

static void
TIFFjpeg_data_src(JPEGState* sp, TIFF* tif)
{
	SensorCall();(void) tif;
	sp->cinfo.d.src = &sp->src;
	sp->src.init_source = std_init_source;
	sp->src.fill_input_buffer = std_fill_input_buffer;
	sp->src.skip_input_data = std_skip_input_data;
	sp->src.resync_to_restart = jpeg_resync_to_restart;
	sp->src.term_source = std_term_source;
	sp->src.bytes_in_buffer = 0;		/* for safety */
	sp->src.next_input_byte = NULL;
SensorCall();}

/*
 * Alternate source manager for reading from JPEGTables.
 * We can share all the code except for the init routine.
 */

static void
tables_init_source(j_decompress_ptr cinfo)
{
	SensorCall();JPEGState* sp = (JPEGState*) cinfo;

	sp->src.next_input_byte = (const JOCTET*) sp->jpegtables;
	sp->src.bytes_in_buffer = (size_t) sp->jpegtables_length;
SensorCall();}

static void
TIFFjpeg_tables_src(JPEGState* sp, TIFF* tif)
{
	SensorCall();TIFFjpeg_data_src(sp, tif);
	sp->src.init_source = tables_init_source;
SensorCall();}

/*
 * Allocate downsampled-data buffers needed for downsampled I/O.
 * We use values computed in jpeg_start_compress or jpeg_start_decompress.
 * We use libjpeg's allocator so that buffers will be released automatically
 * when done with strip/tile.
 * This is also a handy place to compute samplesperclump, bytesperline.
 */
static int
alloc_downsampled_buffers(TIFF* tif, jpeg_component_info* comp_info,
			  int num_components)
{
	SensorCall();JPEGState* sp = JState(tif);
	int ci;
	jpeg_component_info* compptr;
	JSAMPARRAY buf;
	int samples_per_clump = 0;

	SensorCall();for (ci = 0, compptr = comp_info; ci < num_components;
	     ci++, compptr++) {
		SensorCall();samples_per_clump += compptr->h_samp_factor *
			compptr->v_samp_factor;
		buf = TIFFjpeg_alloc_sarray(sp, JPOOL_IMAGE,
				compptr->width_in_blocks * DCTSIZE,
				(JDIMENSION) (compptr->v_samp_factor*DCTSIZE));
		SensorCall();if (buf == NULL)
			{/*47*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*48*/}
		SensorCall();sp->ds_buffer[ci] = buf;
	}
	SensorCall();sp->samplesperclump = samples_per_clump;
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}


/*
 * JPEG Decoding.
 */

#ifdef CHECK_JPEG_YCBCR_SUBSAMPLING

#define JPEG_MARKER_SOF0 0xC0
#define JPEG_MARKER_SOF1 0xC1
#define JPEG_MARKER_SOF3 0xC3
#define JPEG_MARKER_DHT 0xC4
#define JPEG_MARKER_SOI 0xD8
#define JPEG_MARKER_SOS 0xDA
#define JPEG_MARKER_DQT 0xDB
#define JPEG_MARKER_DRI 0xDD
#define JPEG_MARKER_APP0 0xE0
#define JPEG_MARKER_COM 0xFE
struct JPEGFixupTagsSubsamplingData
{
	TIFF* tif;
	void* buffer;
	uint32 buffersize;
	uint8* buffercurrentbyte;
	uint32 bufferbytesleft;
	uint64 fileoffset;
	uint64 filebytesleft;
	uint8 filepositioned;
};
static void JPEGFixupTagsSubsampling(TIFF* tif);
static int JPEGFixupTagsSubsamplingSec(struct JPEGFixupTagsSubsamplingData* data);
static int JPEGFixupTagsSubsamplingReadByte(struct JPEGFixupTagsSubsamplingData* data, uint8* result);
static int JPEGFixupTagsSubsamplingReadWord(struct JPEGFixupTagsSubsamplingData* data, uint16* result);
static void JPEGFixupTagsSubsamplingSkip(struct JPEGFixupTagsSubsamplingData* data, uint16 skiplength);

#endif

static int
JPEGFixupTags(TIFF* tif)
{
#ifdef CHECK_JPEG_YCBCR_SUBSAMPLING
	SensorCall();if ((tif->tif_dir.td_photometric==PHOTOMETRIC_YCBCR)&&
	    (tif->tif_dir.td_planarconfig==PLANARCONFIG_CONTIG)&&
	    (tif->tif_dir.td_samplesperpixel==3))
		{/*83*/SensorCall();JPEGFixupTagsSubsampling(tif);/*84*/}
#endif
        
	SensorCall();return(1);
}

#ifdef CHECK_JPEG_YCBCR_SUBSAMPLING

static void
JPEGFixupTagsSubsampling(TIFF* tif)
{
	/*
	 * Some JPEG-in-TIFF produces do not emit the YCBCRSUBSAMPLING values in
	 * the TIFF tags, but still use non-default (2,2) values within the jpeg
	 * data stream itself.  In order for TIFF applications to work properly
	 * - for instance to get the strip buffer size right - it is imperative
	 * that the subsampling be available before we start reading the image
	 * data normally.  This function will attempt to analyze the first strip in
	 * order to get the sampling values from the jpeg data stream.
	 *
	 * Note that JPEGPreDeocode() will produce a fairly loud warning when the
	 * discovered sampling does not match the default sampling (2,2) or whatever
	 * was actually in the tiff tags.
	 *
	 * See the bug in bugzilla for details:
	 *
	 * http://bugzilla.remotesensing.org/show_bug.cgi?id=168
	 *
	 * Frank Warmerdam, July 2002
	 * Joris Van Damme, May 2007
	 */
	SensorCall();static const char module[] = "JPEGFixupTagsSubsampling";
	struct JPEGFixupTagsSubsamplingData m;

        _TIFFFillStriles( tif );
        
        SensorCall();if( tif->tif_dir.td_stripbytecount == NULL
            || tif->tif_dir.td_stripbytecount[0] == 0 )
        {
            /* Do not even try to check if the first strip/tile does not
               yet exist, as occurs when GDAL has created a new NULL file
               for instance. */
            SensorCall();return;
        }

	SensorCall();m.tif=tif;
	m.buffersize=2048;
	m.buffer=_TIFFmalloc(m.buffersize);
	SensorCall();if (m.buffer==NULL)
	{
		SensorCall();TIFFWarningExt(tif->tif_clientdata,module,
		    "Unable to allocate memory for auto-correcting of subsampling values; auto-correcting skipped");
		SensorCall();return;
	}
	SensorCall();m.buffercurrentbyte=NULL;
	m.bufferbytesleft=0;
	m.fileoffset=tif->tif_dir.td_stripoffset[0];
	m.filepositioned=0;
	m.filebytesleft=tif->tif_dir.td_stripbytecount[0];
	SensorCall();if (!JPEGFixupTagsSubsamplingSec(&m))
		{/*49*/SensorCall();TIFFWarningExt(tif->tif_clientdata,module,
		    "Unable to auto-correct subsampling values, likely corrupt JPEG compressed data in first strip/tile; auto-correcting skipped");/*50*/}
	SensorCall();_TIFFfree(m.buffer);
SensorCall();}

static int
JPEGFixupTagsSubsamplingSec(struct JPEGFixupTagsSubsamplingData* data)
{
	SensorCall();static const char module[] = "JPEGFixupTagsSubsamplingSec";
	uint8 m;
	SensorCall();while (1)
	{
		SensorCall();while (1)
		{
			SensorCall();if (!JPEGFixupTagsSubsamplingReadByte(data,&m))
				{/*51*/SensorCall();return(0);/*52*/}
			SensorCall();if (m==255)
				{/*53*/SensorCall();break;/*54*/}
		}
		SensorCall();while (1)
		{
			SensorCall();if (!JPEGFixupTagsSubsamplingReadByte(data,&m))
				{/*55*/SensorCall();return(0);/*56*/}
			SensorCall();if (m!=255)
				{/*57*/SensorCall();break;/*58*/}
		}
		SensorCall();switch (m)
		{
			case JPEG_MARKER_SOI:
				/* this type of marker has no data and should be skipped */
				SensorCall();break;
			case JPEG_MARKER_COM:
			case JPEG_MARKER_APP0:
			case JPEG_MARKER_APP0+1:
			case JPEG_MARKER_APP0+2:
			case JPEG_MARKER_APP0+3:
			case JPEG_MARKER_APP0+4:
			case JPEG_MARKER_APP0+5:
			case JPEG_MARKER_APP0+6:
			case JPEG_MARKER_APP0+7:
			case JPEG_MARKER_APP0+8:
			case JPEG_MARKER_APP0+9:
			case JPEG_MARKER_APP0+10:
			case JPEG_MARKER_APP0+11:
			case JPEG_MARKER_APP0+12:
			case JPEG_MARKER_APP0+13:
			case JPEG_MARKER_APP0+14:
			case JPEG_MARKER_APP0+15:
			case JPEG_MARKER_DQT:
			case JPEG_MARKER_SOS:
			case JPEG_MARKER_DHT:
			case JPEG_MARKER_DRI:
				/* this type of marker has data, but it has no use to us and should be skipped */
				{
					SensorCall();uint16 n;
					SensorCall();if (!JPEGFixupTagsSubsamplingReadWord(data,&n))
						{/*59*/SensorCall();return(0);/*60*/}
					SensorCall();if (n<2)
						{/*61*/SensorCall();return(0);/*62*/}
					SensorCall();n-=2;
					SensorCall();if (n>0)
						{/*63*/SensorCall();JPEGFixupTagsSubsamplingSkip(data,n);/*64*/}
				}
				SensorCall();break;
			case JPEG_MARKER_SOF0:
			case JPEG_MARKER_SOF1:
				/* this marker contains the subsampling factors we're scanning for */
				{
					SensorCall();uint16 n;
					uint16 o;
					uint8 p;
					uint8 ph,pv;
					SensorCall();if (!JPEGFixupTagsSubsamplingReadWord(data,&n))
						{/*65*/SensorCall();return(0);/*66*/}
					SensorCall();if (n!=8+data->tif->tif_dir.td_samplesperpixel*3)
						{/*67*/SensorCall();return(0);/*68*/}
					SensorCall();JPEGFixupTagsSubsamplingSkip(data,7);
					SensorCall();if (!JPEGFixupTagsSubsamplingReadByte(data,&p))
						{/*69*/SensorCall();return(0);/*70*/}
					SensorCall();ph=(p>>4);
					pv=(p&15);
					JPEGFixupTagsSubsamplingSkip(data,1);
					SensorCall();for (o=1; o<data->tif->tif_dir.td_samplesperpixel; o++)
					{
						SensorCall();JPEGFixupTagsSubsamplingSkip(data,1);
						SensorCall();if (!JPEGFixupTagsSubsamplingReadByte(data,&p))
							{/*71*/SensorCall();return(0);/*72*/}
						SensorCall();if (p!=0x11)
						{
							SensorCall();TIFFWarningExt(data->tif->tif_clientdata,module,
							    "Subsampling values inside JPEG compressed data have no TIFF equivalent, auto-correction of TIFF subsampling values failed");
							SensorCall();return(1);
						}
						SensorCall();JPEGFixupTagsSubsamplingSkip(data,1);
					}
					SensorCall();if (((ph!=1)&&(ph!=2)&&(ph!=4))||((pv!=1)&&(pv!=2)&&(pv!=4)))
					{
						SensorCall();TIFFWarningExt(data->tif->tif_clientdata,module,
						    "Subsampling values inside JPEG compressed data have no TIFF equivalent, auto-correction of TIFF subsampling values failed");
						SensorCall();return(1);
					}
					SensorCall();if ((ph!=data->tif->tif_dir.td_ycbcrsubsampling[0])||(pv!=data->tif->tif_dir.td_ycbcrsubsampling[1]))
					{
						SensorCall();TIFFWarningExt(data->tif->tif_clientdata,module,
						    "Auto-corrected former TIFF subsampling values [%d,%d] to match subsampling values inside JPEG compressed data [%d,%d]",
						    (int)data->tif->tif_dir.td_ycbcrsubsampling[0],
						    (int)data->tif->tif_dir.td_ycbcrsubsampling[1],
						    (int)ph,(int)pv);
						data->tif->tif_dir.td_ycbcrsubsampling[0]=ph;
						data->tif->tif_dir.td_ycbcrsubsampling[1]=pv;
					}
				}
				SensorCall();return(1);
			default:
				SensorCall();return(0);
		}
	}
SensorCall();}

static int
JPEGFixupTagsSubsamplingReadByte(struct JPEGFixupTagsSubsamplingData* data, uint8* result)
{
	SensorCall();if (data->bufferbytesleft==0)
	{
		SensorCall();uint32 m;
		SensorCall();if (data->filebytesleft==0)
			{/*73*/SensorCall();return(0);/*74*/}
		SensorCall();if (!data->filepositioned)
		{
			TIFFSeekFile(data->tif,data->fileoffset,SEEK_SET);
			SensorCall();data->filepositioned=1;
		}
		SensorCall();m=data->buffersize;
		SensorCall();if ((uint64)m>data->filebytesleft)
			{/*75*/SensorCall();m=(uint32)data->filebytesleft;/*76*/}
		assert(m<0x80000000UL);
		SensorCall();if (TIFFReadFile(data->tif,data->buffer,(tmsize_t)m)!=(tmsize_t)m)
			{/*77*/SensorCall();return(0);/*78*/}
		SensorCall();data->buffercurrentbyte=data->buffer;
		data->bufferbytesleft=m;
		data->fileoffset+=m;
		data->filebytesleft-=m;
	}
	SensorCall();*result=*data->buffercurrentbyte;
	data->buffercurrentbyte++;
	data->bufferbytesleft--;
	SensorCall();return(1);
}

static int
JPEGFixupTagsSubsamplingReadWord(struct JPEGFixupTagsSubsamplingData* data, uint16* result)
{
	SensorCall();uint8 ma;
	uint8 mb;
	SensorCall();if (!JPEGFixupTagsSubsamplingReadByte(data,&ma))
		{/*79*/SensorCall();return(0);/*80*/}
	SensorCall();if (!JPEGFixupTagsSubsamplingReadByte(data,&mb))
		{/*81*/SensorCall();return(0);/*82*/}
	SensorCall();*result=(ma<<8)|mb;
	SensorCall();return(1);
}

static void
JPEGFixupTagsSubsamplingSkip(struct JPEGFixupTagsSubsamplingData* data, uint16 skiplength)
{
	SensorCall();if ((uint32)skiplength<=data->bufferbytesleft)
	{
		SensorCall();data->buffercurrentbyte+=skiplength;
		data->bufferbytesleft-=skiplength;
	}
	else
	{
		SensorCall();uint16 m;
		m=skiplength-data->bufferbytesleft;
		SensorCall();if (m<=data->filebytesleft)
		{
			SensorCall();data->bufferbytesleft=0;
			data->fileoffset+=m;
			data->filebytesleft-=m;
			data->filepositioned=0;
		}
		else
		{
			SensorCall();data->bufferbytesleft=0;
			data->filebytesleft=0;
		}
	}
SensorCall();}

#endif


static int
JPEGSetupDecode(TIFF* tif)
{
	SensorCall();JPEGState* sp = JState(tif);
	TIFFDirectory *td = &tif->tif_dir;

#if defined(JPEG_DUAL_MODE_8_12) && !defined(TIFFInitJPEG)
        if( tif->tif_dir.td_bitspersample == 12 )
            return TIFFReInitJPEG_12( tif, COMPRESSION_JPEG, 0 );
#endif

	JPEGInitializeLibJPEG( tif, TRUE );

	assert(sp != NULL);
	assert(sp->cinfo.comm.is_decompressor);

	/* Read JPEGTables if it is present */
	SensorCall();if (TIFFFieldSet(tif,FIELD_JPEGTABLES)) {
		SensorCall();TIFFjpeg_tables_src(sp, tif);
		SensorCall();if(TIFFjpeg_read_header(sp,FALSE) != JPEG_HEADER_TABLES_ONLY) {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, "JPEGSetupDecode", "Bogus JPEGTables field");
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
	}

	/* Grab parameters that are same for all strips/tiles */
	SensorCall();sp->photometric = td->td_photometric;
	SensorCall();switch (sp->photometric) {
	case PHOTOMETRIC_YCBCR:
		SensorCall();sp->h_sampling = td->td_ycbcrsubsampling[0];
		sp->v_sampling = td->td_ycbcrsubsampling[1];
		SensorCall();break;
	default:
		/* TIFF 6.0 forbids subsampling of all other color spaces */
		SensorCall();sp->h_sampling = 1;
		sp->v_sampling = 1;
		SensorCall();break;
	}

	/* Set up for reading normal data */
	SensorCall();TIFFjpeg_data_src(sp, tif);
	tif->tif_postdecode = _TIFFNoPostDecode; /* override byte swapping */
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

/*
 * Set up for decoding a strip or tile.
 */
static int
JPEGPreDecode(TIFF* tif, uint16 s)
{
	SensorCall();JPEGState *sp = JState(tif);
	TIFFDirectory *td = &tif->tif_dir;
	static const char module[] = "JPEGPreDecode";
	uint32 segment_width, segment_height;
	int downsampled_output;
	int ci;

	assert(sp != NULL);
  
	SensorCall();if (sp->cinfo.comm.is_decompressor == 0)
	{
		SensorCall();tif->tif_setupdecode( tif );
	}
  
	assert(sp->cinfo.comm.is_decompressor);
	/*
	 * Reset decoder state from any previous strip/tile,
	 * in case application didn't read the whole strip.
	 */
	SensorCall();if (!TIFFjpeg_abort(sp))
		{/*85*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*86*/}
	/*
	 * Read the header for this strip/tile.
	 */
        
	SensorCall();if (TIFFjpeg_read_header(sp, TRUE) != JPEG_HEADER_OK)
		{/*87*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*88*/}

        SensorCall();tif->tif_rawcp = (uint8*) sp->src.next_input_byte;
        tif->tif_rawcc = sp->src.bytes_in_buffer;

	/*
	 * Check image parameters and set decompression parameters.
	 */
	segment_width = td->td_imagewidth;
	segment_height = td->td_imagelength - tif->tif_row;
	SensorCall();if (isTiled(tif)) {
                SensorCall();segment_width = td->td_tilewidth;
                segment_height = td->td_tilelength;
		sp->bytesperline = TIFFTileRowSize(tif);
	} else {
		SensorCall();if (segment_height > td->td_rowsperstrip)
			{/*89*/SensorCall();segment_height = td->td_rowsperstrip;/*90*/}
		SensorCall();sp->bytesperline = TIFFScanlineSize(tif);
	}
	SensorCall();if (td->td_planarconfig == PLANARCONFIG_SEPARATE && s > 0) {
		/*
		 * For PC 2, scale down the expected strip/tile size
		 * to match a downsampled component
		 */
		SensorCall();segment_width = TIFFhowmany_32(segment_width, sp->h_sampling);
		segment_height = TIFFhowmany_32(segment_height, sp->v_sampling);
	}
	SensorCall();if (sp->cinfo.d.image_width < segment_width ||
	    sp->cinfo.d.image_height < segment_height) {
		SensorCall();TIFFWarningExt(tif->tif_clientdata, module,
			       "Improper JPEG strip/tile size, "
			       "expected %dx%d, got %dx%d",
			       segment_width, segment_height,
			       sp->cinfo.d.image_width,
			       sp->cinfo.d.image_height);
	} 
	SensorCall();if (sp->cinfo.d.image_width > segment_width ||
	    sp->cinfo.d.image_height > segment_height) {
		/*
		 * This case could be dangerous, if the strip or tile size has
		 * been reported as less than the amount of data jpeg will
		 * return, some potential security issues arise. Catch this
		 * case and error out.
		 */
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
			     "JPEG strip/tile size exceeds expected dimensions,"
			     " expected %dx%d, got %dx%d",
			     segment_width, segment_height,
			     sp->cinfo.d.image_width, sp->cinfo.d.image_height);
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();if (sp->cinfo.d.num_components !=
	    (td->td_planarconfig == PLANARCONFIG_CONTIG ?
	     td->td_samplesperpixel : 1)) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "Improper JPEG component count");
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
#ifdef JPEG_LIB_MK1
	if (12 != td->td_bitspersample && 8 != td->td_bitspersample) {
		TIFFErrorExt(tif->tif_clientdata, module, "Improper JPEG data precision");
		return (0);
	}
	sp->cinfo.d.data_precision = td->td_bitspersample;
	sp->cinfo.d.bits_in_jsample = td->td_bitspersample;
#else
	SensorCall();if (sp->cinfo.d.data_precision != td->td_bitspersample) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "Improper JPEG data precision");
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
#endif
	SensorCall();if (td->td_planarconfig == PLANARCONFIG_CONTIG) {
		/* Component 0 should have expected sampling factors */
		SensorCall();if (sp->cinfo.d.comp_info[0].h_samp_factor != sp->h_sampling ||
		    sp->cinfo.d.comp_info[0].v_samp_factor != sp->v_sampling) {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				       "Improper JPEG sampling factors %d,%d\n"
				       "Apparently should be %d,%d.",
				       sp->cinfo.d.comp_info[0].h_samp_factor,
				       sp->cinfo.d.comp_info[0].v_samp_factor,
				       sp->h_sampling, sp->v_sampling);
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
		/* Rest should have sampling factors 1,1 */
		SensorCall();for (ci = 1; ci < sp->cinfo.d.num_components; ci++) {
			SensorCall();if (sp->cinfo.d.comp_info[ci].h_samp_factor != 1 ||
			    sp->cinfo.d.comp_info[ci].v_samp_factor != 1) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "Improper JPEG sampling factors");
				{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
			}
		}
	} else {
		/* PC 2's single component should have sampling factors 1,1 */
		SensorCall();if (sp->cinfo.d.comp_info[0].h_samp_factor != 1 ||
		    sp->cinfo.d.comp_info[0].v_samp_factor != 1) {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "Improper JPEG sampling factors");
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
	}
	SensorCall();downsampled_output = FALSE;
	SensorCall();if (td->td_planarconfig == PLANARCONFIG_CONTIG &&
	    sp->photometric == PHOTOMETRIC_YCBCR &&
	    sp->jpegcolormode == JPEGCOLORMODE_RGB) {
		/* Convert YCbCr to RGB */
		SensorCall();sp->cinfo.d.jpeg_color_space = JCS_YCbCr;
		sp->cinfo.d.out_color_space = JCS_RGB;
	} else {
		/* Suppress colorspace handling */
		SensorCall();sp->cinfo.d.jpeg_color_space = JCS_UNKNOWN;
		sp->cinfo.d.out_color_space = JCS_UNKNOWN;
		SensorCall();if (td->td_planarconfig == PLANARCONFIG_CONTIG &&
		    (sp->h_sampling != 1 || sp->v_sampling != 1))
			downsampled_output = TRUE;
		/* XXX what about up-sampling? */
	}
	SensorCall();if (downsampled_output) {
		/* Need to use raw-data interface to libjpeg */
		SensorCall();sp->cinfo.d.raw_data_out = TRUE;
#if JPEG_LIB_VERSION >= 70
		sp->cinfo.d.do_fancy_upsampling = FALSE;
#endif /* JPEG_LIB_VERSION >= 70 */
		tif->tif_decoderow = DecodeRowError;
		tif->tif_decodestrip = JPEGDecodeRaw;
		tif->tif_decodetile = JPEGDecodeRaw;
	} else {
		/* Use normal interface to libjpeg */
		SensorCall();sp->cinfo.d.raw_data_out = FALSE;
		tif->tif_decoderow = JPEGDecode;
		tif->tif_decodestrip = JPEGDecode;
		tif->tif_decodetile = JPEGDecode;  
	}
	/* Start JPEG decompressor */
	SensorCall();if (!TIFFjpeg_start_decompress(sp))
		{/*91*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*92*/}
	/* Allocate downsampled-data buffers if needed */
	SensorCall();if (downsampled_output) {
		SensorCall();if (!alloc_downsampled_buffers(tif, sp->cinfo.d.comp_info,
					       sp->cinfo.d.num_components))
			{/*93*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*94*/}
		SensorCall();sp->scancount = DCTSIZE;	/* mark buffer empty */
	}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

/*
 * Decode a chunk of pixels.
 * "Standard" case: returned data is not downsampled.
 */
/*ARGSUSED*/ static int
JPEGDecode(TIFF* tif, uint8* buf, tmsize_t cc, uint16 s)
{
	SensorCall();JPEGState *sp = JState(tif);
	tmsize_t nrows;
	(void) s;

        /*
        ** Update available information, buffer may have been refilled
        ** between decode requests
        */
	sp->src.next_input_byte = (const JOCTET*) tif->tif_rawcp;
	sp->src.bytes_in_buffer = (size_t) tif->tif_rawcc;

        SensorCall();if( sp->bytesperline == 0 )
                {/*1*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*2*/}
        
	SensorCall();nrows = cc / sp->bytesperline;
	SensorCall();if (cc % sp->bytesperline)
		{/*3*/SensorCall();TIFFWarningExt(tif->tif_clientdata, tif->tif_name, "fractional scanline not read");/*4*/}

	SensorCall();if( nrows > (tmsize_t) sp->cinfo.d.image_height )
		{/*5*/SensorCall();nrows = sp->cinfo.d.image_height;/*6*/}

	/* data is expected to be read in multiples of a scanline */
	SensorCall();if (nrows)
	{
		SensorCall();JSAMPROW line_work_buf = NULL;

		/*
		 * For 6B, only use temporary buffer for 12 bit imagery.
		 * For Mk1 always use it.
		 */
#if !defined(JPEG_LIB_MK1)
		SensorCall();if( sp->cinfo.d.data_precision == 12 )
#endif
		{
			SensorCall();line_work_buf = (JSAMPROW)
			    _TIFFmalloc(sizeof(short) * sp->cinfo.d.output_width
			    * sp->cinfo.d.num_components );
		}

		SensorCall();do {
			SensorCall();if( line_work_buf != NULL )
			{
				/*
				 * In the MK1 case, we aways read into a 16bit buffer, and then
				 * pack down to 12bit or 8bit.  In 6B case we only read into 16
				 * bit buffer for 12bit data, which we need to repack.
				*/
				SensorCall();if (TIFFjpeg_read_scanlines(sp, &line_work_buf, 1) != 1)
					{/*7*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*8*/}

				SensorCall();if( sp->cinfo.d.data_precision == 12 )
				{
					SensorCall();int value_pairs = (sp->cinfo.d.output_width
					    * sp->cinfo.d.num_components) / 2;
					int iPair;

					SensorCall();for( iPair = 0; iPair < value_pairs; iPair++ )
					{
						SensorCall();unsigned char *out_ptr =
						    ((unsigned char *) buf) + iPair * 3;
						JSAMPLE *in_ptr = line_work_buf + iPair * 2;

						out_ptr[0] = (in_ptr[0] & 0xff0) >> 4;
						out_ptr[1] = ((in_ptr[0] & 0xf) << 4)
						    | ((in_ptr[1] & 0xf00) >> 8);
						out_ptr[2] = ((in_ptr[1] & 0xff) >> 0);
					}
				}
				else {/*9*/SensorCall();if( sp->cinfo.d.data_precision == 8 )
				{
					SensorCall();int value_count = (sp->cinfo.d.output_width
					    * sp->cinfo.d.num_components);
					int iValue;

					SensorCall();for( iValue = 0; iValue < value_count; iValue++ )
					{
						SensorCall();((unsigned char *) buf)[iValue] =
						    line_work_buf[iValue] & 0xff;
					}
				;/*10*/}}
			}
			else
			{
				/*
				 * In the libjpeg6b 8bit case.  We read directly into the
				 * TIFF buffer.
				*/
				SensorCall();JSAMPROW bufptr = (JSAMPROW)buf;

				SensorCall();if (TIFFjpeg_read_scanlines(sp, &bufptr, 1) != 1)
					{/*11*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*12*/}
			}

			SensorCall();++tif->tif_row;
			buf += sp->bytesperline;
			cc -= sp->bytesperline;
		} while (--nrows > 0);

		SensorCall();if( line_work_buf != NULL )
			{/*13*/SensorCall();_TIFFfree( line_work_buf );/*14*/}
	}

        /* Update information on consumed data */
        SensorCall();tif->tif_rawcp = (uint8*) sp->src.next_input_byte;
        tif->tif_rawcc = sp->src.bytes_in_buffer;
                
	/* Close down the decompressor if we've finished the strip or tile. */
	{int  ReplaceReturn = sp->cinfo.d.output_scanline < sp->cinfo.d.output_height
	    || TIFFjpeg_finish_decompress(sp); SensorCall(); return ReplaceReturn;}
}

/*ARGSUSED*/ static int
DecodeRowError(TIFF* tif, uint8* buf, tmsize_t cc, uint16 s)

{
    SensorCall();(void) buf;
    (void) cc;
    (void) s;

    TIFFErrorExt(tif->tif_clientdata, "TIFFReadScanline",
                 "scanline oriented access is not supported for downsampled JPEG compressed images, consider enabling TIFF_JPEGCOLORMODE as JPEGCOLORMODE_RGB." );
    {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

/*
 * Decode a chunk of pixels.
 * Returned data is downsampled per sampling factors.
 */
/*ARGSUSED*/ static int
JPEGDecodeRaw(TIFF* tif, uint8* buf, tmsize_t cc, uint16 s)
{
	SensorCall();JPEGState *sp = JState(tif);
	tmsize_t nrows;
	(void) s;

	/* data is expected to be read in multiples of a scanline */
	SensorCall();if ( (nrows = sp->cinfo.d.image_height) ) {

		/* Cb,Cr both have sampling factors 1, so this is correct */
		SensorCall();JDIMENSION clumps_per_line = sp->cinfo.d.comp_info[1].downsampled_width;            
		int samples_per_clump = sp->samplesperclump;

#if defined(JPEG_LIB_MK1_OR_12BIT)
		unsigned short* tmpbuf = _TIFFmalloc(sizeof(unsigned short) *
						     sp->cinfo.d.output_width *
						     sp->cinfo.d.num_components);
		if(tmpbuf==NULL) {
                        TIFFErrorExt(tif->tif_clientdata, "JPEGDecodeRaw",
				     "Out of memory");
			return 0;
                }
#endif

		SensorCall();do {
			SensorCall();jpeg_component_info *compptr;
			int ci, clumpoffset;

                        SensorCall();if( cc < sp->bytesperline ) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, "JPEGDecodeRaw",
					     "application buffer not large enough for all data.");
				{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
                        }

			/* Reload downsampled-data buffer if needed */
			SensorCall();if (sp->scancount >= DCTSIZE) {
				SensorCall();int n = sp->cinfo.d.max_v_samp_factor * DCTSIZE;
				SensorCall();if (TIFFjpeg_read_raw_data(sp, sp->ds_buffer, n) != n)
					{/*15*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*16*/}
				SensorCall();sp->scancount = 0;
			}
			/*
			 * Fastest way to unseparate data is to make one pass
			 * over the scanline for each row of each component.
			 */
			SensorCall();clumpoffset = 0;    /* first sample in clump */
			SensorCall();for (ci = 0, compptr = sp->cinfo.d.comp_info;
			     ci < sp->cinfo.d.num_components;
			     ci++, compptr++) {
				SensorCall();int hsamp = compptr->h_samp_factor;
				int vsamp = compptr->v_samp_factor;
				int ypos;

				SensorCall();for (ypos = 0; ypos < vsamp; ypos++) {
					SensorCall();JSAMPLE *inptr = sp->ds_buffer[ci][sp->scancount*vsamp + ypos];
					JDIMENSION nclump;
#if defined(JPEG_LIB_MK1_OR_12BIT)
					JSAMPLE *outptr = (JSAMPLE*)tmpbuf + clumpoffset;
#else
					JSAMPLE *outptr = (JSAMPLE*)buf + clumpoffset;
					SensorCall();if (cc < (tmsize_t) (clumpoffset + samples_per_clump*(clumps_per_line-1) + hsamp)) {
						SensorCall();TIFFErrorExt(tif->tif_clientdata, "JPEGDecodeRaw",
							     "application buffer not large enough for all data, possible subsampling issue");
						{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
					}
#endif

					SensorCall();if (hsamp == 1) {
						/* fast path for at least Cb and Cr */
						SensorCall();for (nclump = clumps_per_line; nclump-- > 0; ) {
							SensorCall();outptr[0] = *inptr++;
							outptr += samples_per_clump;
						}
					} else {
						SensorCall();int xpos;

						/* general case */
						SensorCall();for (nclump = clumps_per_line; nclump-- > 0; ) {
							SensorCall();for (xpos = 0; xpos < hsamp; xpos++)
								{/*17*/SensorCall();outptr[xpos] = *inptr++;/*18*/}
							SensorCall();outptr += samples_per_clump;
						}
					}
					SensorCall();clumpoffset += hsamp;
				}
			}

#if defined(JPEG_LIB_MK1_OR_12BIT)
			{
				if (sp->cinfo.d.data_precision == 8)
				{
					int i=0;
					int len = sp->cinfo.d.output_width * sp->cinfo.d.num_components;
					for (i=0; i<len; i++)
					{
						((unsigned char*)buf)[i] = tmpbuf[i] & 0xff;
					}
				}
				else
				{         /* 12-bit */
					int value_pairs = (sp->cinfo.d.output_width
							   * sp->cinfo.d.num_components) / 2;
					int iPair;
					for( iPair = 0; iPair < value_pairs; iPair++ )
					{
						unsigned char *out_ptr = ((unsigned char *) buf) + iPair * 3;
						JSAMPLE *in_ptr = (JSAMPLE *) (tmpbuf + iPair * 2);
						out_ptr[0] = (in_ptr[0] & 0xff0) >> 4;
						out_ptr[1] = ((in_ptr[0] & 0xf) << 4)
							| ((in_ptr[1] & 0xf00) >> 8);
						out_ptr[2] = ((in_ptr[1] & 0xff) >> 0);
					}
				}
			}
#endif

			SensorCall();sp->scancount ++;
			tif->tif_row += sp->v_sampling;

			buf += sp->bytesperline;
			cc -= sp->bytesperline;

			nrows -= sp->v_sampling;
		} while (nrows > 0);

#if defined(JPEG_LIB_MK1_OR_12BIT)
		_TIFFfree(tmpbuf);
#endif

	}

	/* Close down the decompressor if done. */
	{int  ReplaceReturn = sp->cinfo.d.output_scanline < sp->cinfo.d.output_height
		|| TIFFjpeg_finish_decompress(sp); SensorCall(); return ReplaceReturn;}
}


/*
 * JPEG Encoding.
 */

static void
unsuppress_quant_table (JPEGState* sp, int tblno)
{
	SensorCall();JQUANT_TBL* qtbl;

	SensorCall();if ((qtbl = sp->cinfo.c.quant_tbl_ptrs[tblno]) != NULL)
		qtbl->sent_table = FALSE;
SensorCall();}

static void
unsuppress_huff_table (JPEGState* sp, int tblno)
{
	SensorCall();JHUFF_TBL* htbl;

	SensorCall();if ((htbl = sp->cinfo.c.dc_huff_tbl_ptrs[tblno]) != NULL)
		htbl->sent_table = FALSE;
	SensorCall();if ((htbl = sp->cinfo.c.ac_huff_tbl_ptrs[tblno]) != NULL)
		htbl->sent_table = FALSE;
SensorCall();}

static int
prepare_JPEGTables(TIFF* tif)
{
	SensorCall();JPEGState* sp = JState(tif);

	/* Initialize quant tables for current quality setting */
	SensorCall();if (!TIFFjpeg_set_quality(sp, sp->jpegquality, FALSE))
		{/*95*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*96*/}
	/* Mark only the tables we want for output */
	/* NB: chrominance tables are currently used only with YCbCr */
	SensorCall();if (!TIFFjpeg_suppress_tables(sp, TRUE))
		{/*97*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*98*/}
	SensorCall();if (sp->jpegtablesmode & JPEGTABLESMODE_QUANT) {
		SensorCall();unsuppress_quant_table(sp, 0);
		SensorCall();if (sp->photometric == PHOTOMETRIC_YCBCR)
			{/*99*/SensorCall();unsuppress_quant_table(sp, 1);/*100*/}
	}
	SensorCall();if (sp->jpegtablesmode & JPEGTABLESMODE_HUFF) {
		SensorCall();unsuppress_huff_table(sp, 0);
		SensorCall();if (sp->photometric == PHOTOMETRIC_YCBCR)
			{/*101*/SensorCall();unsuppress_huff_table(sp, 1);/*102*/}
	}
	/* Direct libjpeg output into jpegtables */
	SensorCall();if (!TIFFjpeg_tables_dest(sp, tif))
		{/*103*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*104*/}
	/* Emit tables-only datastream */
	SensorCall();if (!TIFFjpeg_write_tables(sp))
		{/*105*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*106*/}

	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

static int
JPEGSetupEncode(TIFF* tif)
{
	SensorCall();JPEGState* sp = JState(tif);
	TIFFDirectory *td = &tif->tif_dir;
	static const char module[] = "JPEGSetupEncode";

#if defined(JPEG_DUAL_MODE_8_12) && !defined(TIFFInitJPEG)
        if( tif->tif_dir.td_bitspersample == 12 )
            return TIFFReInitJPEG_12( tif, COMPRESSION_JPEG, 1 );
#endif

        JPEGInitializeLibJPEG( tif, FALSE );

	assert(sp != NULL);
	assert(!sp->cinfo.comm.is_decompressor);

	/*
	 * Initialize all JPEG parameters to default values.
	 * Note that jpeg_set_defaults needs legal values for
	 * in_color_space and input_components.
	 */
	sp->cinfo.c.in_color_space = JCS_UNKNOWN;
	sp->cinfo.c.input_components = 1;
	SensorCall();if (!TIFFjpeg_set_defaults(sp))
		{/*107*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*108*/}
	/* Set per-file parameters */
	SensorCall();sp->photometric = td->td_photometric;
	SensorCall();switch (sp->photometric) {
	case PHOTOMETRIC_YCBCR:
		SensorCall();sp->h_sampling = td->td_ycbcrsubsampling[0];
		sp->v_sampling = td->td_ycbcrsubsampling[1];
		/*
		 * A ReferenceBlackWhite field *must* be present since the
		 * default value is inappropriate for YCbCr.  Fill in the
		 * proper value if application didn't set it.
		 */
		{
			float *ref;
			SensorCall();if (!TIFFGetField(tif, TIFFTAG_REFERENCEBLACKWHITE,
					  &ref)) {
				SensorCall();float refbw[6];
				long top = 1L << td->td_bitspersample;
				refbw[0] = 0;
				refbw[1] = (float)(top-1L);
				refbw[2] = (float)(top>>1);
				refbw[3] = refbw[1];
				refbw[4] = refbw[2];
				refbw[5] = refbw[1];
				TIFFSetField(tif, TIFFTAG_REFERENCEBLACKWHITE,
					     refbw);
			}
		}
		SensorCall();break;
	case PHOTOMETRIC_PALETTE:		/* disallowed by Tech Note */
	case PHOTOMETRIC_MASK:
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
			  "PhotometricInterpretation %d not allowed for JPEG",
			  (int) sp->photometric);
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	default:
		/* TIFF 6.0 forbids subsampling of all other color spaces */
		SensorCall();sp->h_sampling = 1;
		sp->v_sampling = 1;
		SensorCall();break;
	}

	/* Verify miscellaneous parameters */

	/*
	 * This would need work if libtiff ever supports different
	 * depths for different components, or if libjpeg ever supports
	 * run-time selection of depth.  Neither is imminent.
	 */
#ifdef JPEG_LIB_MK1
        /* BITS_IN_JSAMPLE now permits 8 and 12 --- dgilbert */
	if (td->td_bitspersample != 8 && td->td_bitspersample != 12) 
#else
	SensorCall();if (td->td_bitspersample != BITS_IN_JSAMPLE )
#endif
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "BitsPerSample %d not allowed for JPEG",
			  (int) td->td_bitspersample);
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();sp->cinfo.c.data_precision = td->td_bitspersample;
#ifdef JPEG_LIB_MK1
        sp->cinfo.c.bits_in_jsample = td->td_bitspersample;
#endif
	SensorCall();if (isTiled(tif)) {
		SensorCall();if ((td->td_tilelength % (sp->v_sampling * DCTSIZE)) != 0) {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				  "JPEG tile height must be multiple of %d",
				  sp->v_sampling * DCTSIZE);
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
		SensorCall();if ((td->td_tilewidth % (sp->h_sampling * DCTSIZE)) != 0) {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				  "JPEG tile width must be multiple of %d",
				  sp->h_sampling * DCTSIZE);
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
	} else {
		SensorCall();if (td->td_rowsperstrip < td->td_imagelength &&
		    (td->td_rowsperstrip % (sp->v_sampling * DCTSIZE)) != 0) {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				  "RowsPerStrip must be multiple of %d for JPEG",
				  sp->v_sampling * DCTSIZE);
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
	}

	/* Create a JPEGTables field if appropriate */
	SensorCall();if (sp->jpegtablesmode & (JPEGTABLESMODE_QUANT|JPEGTABLESMODE_HUFF)) {
                SensorCall();if( sp->jpegtables == NULL
                    || memcmp(sp->jpegtables,"\0\0\0\0\0\0\0\0\0",8) == 0 )
                {
                        SensorCall();if (!prepare_JPEGTables(tif))
                                {/*109*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*110*/}
                        /* Mark the field present */
                        /* Can't use TIFFSetField since BEENWRITING is already set! */
                        SensorCall();tif->tif_flags |= TIFF_DIRTYDIRECT;
                        TIFFSetFieldBit(tif, FIELD_JPEGTABLES);
                }
	} else {
		/* We do not support application-supplied JPEGTables, */
		/* so mark the field not present */
		TIFFClrFieldBit(tif, FIELD_JPEGTABLES);
	}

	/* Direct libjpeg output to libtiff's output buffer */
	SensorCall();TIFFjpeg_data_dest(sp, tif);

	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

/*
 * Set encoding state at the start of a strip or tile.
 */
static int
JPEGPreEncode(TIFF* tif, uint16 s)
{
	SensorCall();JPEGState *sp = JState(tif);
	TIFFDirectory *td = &tif->tif_dir;
	static const char module[] = "JPEGPreEncode";
	uint32 segment_width, segment_height;
	int downsampled_input;

	assert(sp != NULL);
  
	SensorCall();if (sp->cinfo.comm.is_decompressor == 1)
	{
		SensorCall();tif->tif_setupencode( tif );
	}
  
	assert(!sp->cinfo.comm.is_decompressor);
	/*
	 * Set encoding parameters for this strip/tile.
	 */
	SensorCall();if (isTiled(tif)) {
		SensorCall();segment_width = td->td_tilewidth;
		segment_height = td->td_tilelength;
		sp->bytesperline = TIFFTileRowSize(tif);
	} else {
		SensorCall();segment_width = td->td_imagewidth;
		segment_height = td->td_imagelength - tif->tif_row;
		SensorCall();if (segment_height > td->td_rowsperstrip)
			{/*111*/SensorCall();segment_height = td->td_rowsperstrip;/*112*/}
		SensorCall();sp->bytesperline = TIFFScanlineSize(tif);
	}
	SensorCall();if (td->td_planarconfig == PLANARCONFIG_SEPARATE && s > 0) {
		/* for PC 2, scale down the strip/tile size
		 * to match a downsampled component
		 */
		SensorCall();segment_width = TIFFhowmany_32(segment_width, sp->h_sampling); 
		segment_height = TIFFhowmany_32(segment_height, sp->v_sampling);
	}
	SensorCall();if (segment_width > 65535 || segment_height > 65535) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "Strip/tile too large for JPEG");
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();sp->cinfo.c.image_width = segment_width;
	sp->cinfo.c.image_height = segment_height;
	downsampled_input = FALSE;
	SensorCall();if (td->td_planarconfig == PLANARCONFIG_CONTIG) {
		SensorCall();sp->cinfo.c.input_components = td->td_samplesperpixel;
		SensorCall();if (sp->photometric == PHOTOMETRIC_YCBCR) {
			SensorCall();if (sp->jpegcolormode == JPEGCOLORMODE_RGB) {
				SensorCall();sp->cinfo.c.in_color_space = JCS_RGB;
			} else {
				SensorCall();sp->cinfo.c.in_color_space = JCS_YCbCr;
				SensorCall();if (sp->h_sampling != 1 || sp->v_sampling != 1)
					downsampled_input = TRUE;
			}
			SensorCall();if (!TIFFjpeg_set_colorspace(sp, JCS_YCbCr))
				{/*113*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*114*/}
			/*
			 * Set Y sampling factors;
			 * we assume jpeg_set_colorspace() set the rest to 1
			 */
			SensorCall();sp->cinfo.c.comp_info[0].h_samp_factor = sp->h_sampling;
			sp->cinfo.c.comp_info[0].v_samp_factor = sp->v_sampling;
		} else {
			SensorCall();if ((td->td_photometric == PHOTOMETRIC_MINISWHITE || td->td_photometric == PHOTOMETRIC_MINISBLACK) && td->td_samplesperpixel == 1)
				{/*115*/SensorCall();sp->cinfo.c.in_color_space = JCS_GRAYSCALE;/*116*/}
			else {/*117*/SensorCall();if (td->td_photometric == PHOTOMETRIC_RGB && td->td_samplesperpixel == 3)
				{/*119*/SensorCall();sp->cinfo.c.in_color_space = JCS_RGB;/*120*/}
			else {/*121*/SensorCall();if (td->td_photometric == PHOTOMETRIC_SEPARATED && td->td_samplesperpixel == 4)
				{/*123*/SensorCall();sp->cinfo.c.in_color_space = JCS_CMYK;/*124*/}
			else
				{/*125*/SensorCall();sp->cinfo.c.in_color_space = JCS_UNKNOWN;/*126*/}/*122*/}/*118*/}
			SensorCall();if (!TIFFjpeg_set_colorspace(sp, sp->cinfo.c.in_color_space))
				{/*127*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*128*/}
			/* jpeg_set_colorspace set all sampling factors to 1 */
		}
	} else {
		SensorCall();sp->cinfo.c.input_components = 1;
		sp->cinfo.c.in_color_space = JCS_UNKNOWN;
		SensorCall();if (!TIFFjpeg_set_colorspace(sp, JCS_UNKNOWN))
			{/*129*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*130*/}
		SensorCall();sp->cinfo.c.comp_info[0].component_id = s;
		/* jpeg_set_colorspace() set sampling factors to 1 */
		SensorCall();if (sp->photometric == PHOTOMETRIC_YCBCR && s > 0) {
			SensorCall();sp->cinfo.c.comp_info[0].quant_tbl_no = 1;
			sp->cinfo.c.comp_info[0].dc_tbl_no = 1;
			sp->cinfo.c.comp_info[0].ac_tbl_no = 1;
		}
	}
	/* ensure libjpeg won't write any extraneous markers */
	SensorCall();sp->cinfo.c.write_JFIF_header = FALSE;
	sp->cinfo.c.write_Adobe_marker = FALSE;
	/* set up table handling correctly */
        SensorCall();if (!TIFFjpeg_set_quality(sp, sp->jpegquality, FALSE))
		{/*131*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*132*/}
	SensorCall();if (! (sp->jpegtablesmode & JPEGTABLESMODE_QUANT)) {
		SensorCall();unsuppress_quant_table(sp, 0);
		unsuppress_quant_table(sp, 1);
	}
	SensorCall();if (sp->jpegtablesmode & JPEGTABLESMODE_HUFF)
		sp->cinfo.c.optimize_coding = FALSE;
	else
		sp->cinfo.c.optimize_coding = TRUE;
	SensorCall();if (downsampled_input) {
		/* Need to use raw-data interface to libjpeg */
		SensorCall();sp->cinfo.c.raw_data_in = TRUE;
		tif->tif_encoderow = JPEGEncodeRaw;
		tif->tif_encodestrip = JPEGEncodeRaw;
		tif->tif_encodetile = JPEGEncodeRaw;
	} else {
		/* Use normal interface to libjpeg */
		SensorCall();sp->cinfo.c.raw_data_in = FALSE;
		tif->tif_encoderow = JPEGEncode;
		tif->tif_encodestrip = JPEGEncode;
		tif->tif_encodetile = JPEGEncode;
	}
	/* Start JPEG compressor */
	SensorCall();if (!TIFFjpeg_start_compress(sp, FALSE))
		{/*133*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*134*/}
	/* Allocate downsampled-data buffers if needed */
	SensorCall();if (downsampled_input) {
		SensorCall();if (!alloc_downsampled_buffers(tif, sp->cinfo.c.comp_info,
					       sp->cinfo.c.num_components))
			{/*135*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*136*/}
	}
	SensorCall();sp->scancount = 0;

	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

/*
 * Encode a chunk of pixels.
 * "Standard" case: incoming data is not downsampled.
 */
static int
JPEGEncode(TIFF* tif, uint8* buf, tmsize_t cc, uint16 s)
{
	SensorCall();JPEGState *sp = JState(tif);
	tmsize_t nrows;
	JSAMPROW bufptr[1];
        short *line16 = NULL;
        int    line16_count = 0;

	(void) s;
	assert(sp != NULL);
	/* data is expected to be supplied in multiples of a scanline */
	nrows = cc / sp->bytesperline;
	SensorCall();if (cc % sp->bytesperline)
            {/*19*/SensorCall();TIFFWarningExt(tif->tif_clientdata, tif->tif_name, 
                           "fractional scanline discarded");/*20*/}

        /* The last strip will be limited to image size */
        SensorCall();if( !isTiled(tif) && tif->tif_row+nrows > tif->tif_dir.td_imagelength )
            {/*21*/SensorCall();nrows = tif->tif_dir.td_imagelength - tif->tif_row;/*22*/}

        SensorCall();if( sp->cinfo.c.data_precision == 12 )
        {
            SensorCall();line16_count = (sp->bytesperline * 2) / 3;
            line16 = (short *) _TIFFmalloc(sizeof(short) * line16_count);
	    // FIXME: undiagnosed malloc failure
        }
            
	SensorCall();while (nrows-- > 0) {

            SensorCall();if( sp->cinfo.c.data_precision == 12 )
            {

                SensorCall();int value_pairs = line16_count / 2;
                int iPair;

		bufptr[0] = (JSAMPROW) line16;

                SensorCall();for( iPair = 0; iPair < value_pairs; iPair++ )
                {
                    SensorCall();unsigned char *in_ptr =
                        ((unsigned char *) buf) + iPair * 3;
                    JSAMPLE *out_ptr = (JSAMPLE *) (line16 + iPair * 2);

                    out_ptr[0] = (in_ptr[0] << 4) | ((in_ptr[1] & 0xf0) >> 4);
                    out_ptr[1] = ((in_ptr[1] & 0x0f) << 8) | in_ptr[2];
                }
            }
            else
            {
		SensorCall();bufptr[0] = (JSAMPROW) buf;
            }
            SensorCall();if (TIFFjpeg_write_scanlines(sp, bufptr, 1) != 1)
                {/*23*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*24*/}
            SensorCall();if (nrows > 0)
                {/*25*/SensorCall();tif->tif_row++;/*26*/}
            SensorCall();buf += sp->bytesperline;
	}

        SensorCall();if( sp->cinfo.c.data_precision == 12 )
        {
            SensorCall();_TIFFfree( line16 );
        }
            
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

/*
 * Encode a chunk of pixels.
 * Incoming data is expected to be downsampled per sampling factors.
 */
static int
JPEGEncodeRaw(TIFF* tif, uint8* buf, tmsize_t cc, uint16 s)
{
	SensorCall();JPEGState *sp = JState(tif);
	JSAMPLE* inptr;
	JSAMPLE* outptr;
	tmsize_t nrows;
	JDIMENSION clumps_per_line, nclump;
	int clumpoffset, ci, xpos, ypos;
	jpeg_component_info* compptr;
	int samples_per_clump = sp->samplesperclump;
	tmsize_t bytesperclumpline;

	(void) s;
	assert(sp != NULL);
	/* data is expected to be supplied in multiples of a clumpline */
	/* a clumpline is equivalent to v_sampling desubsampled scanlines */
	/* TODO: the following calculation of bytesperclumpline, should substitute calculation of sp->bytesperline, except that it is per v_sampling lines */
	bytesperclumpline = (((sp->cinfo.c.image_width+sp->h_sampling-1)/sp->h_sampling)
			     *(sp->h_sampling*sp->v_sampling+2)*sp->cinfo.c.data_precision+7)
			    /8;

	nrows = ( cc / bytesperclumpline ) * sp->v_sampling;
	SensorCall();if (cc % bytesperclumpline)
		{/*27*/SensorCall();TIFFWarningExt(tif->tif_clientdata, tif->tif_name, "fractional scanline discarded");/*28*/}

	/* Cb,Cr both have sampling factors 1, so this is correct */
	SensorCall();clumps_per_line = sp->cinfo.c.comp_info[1].downsampled_width;

	SensorCall();while (nrows > 0) {
		/*
		 * Fastest way to separate the data is to make one pass
		 * over the scanline for each row of each component.
		 */
		SensorCall();clumpoffset = 0;		/* first sample in clump */
		SensorCall();for (ci = 0, compptr = sp->cinfo.c.comp_info;
		     ci < sp->cinfo.c.num_components;
		     ci++, compptr++) {
		    SensorCall();int hsamp = compptr->h_samp_factor;
		    int vsamp = compptr->v_samp_factor;
		    int padding = (int) (compptr->width_in_blocks * DCTSIZE -
					 clumps_per_line * hsamp);
		    SensorCall();for (ypos = 0; ypos < vsamp; ypos++) {
			SensorCall();inptr = ((JSAMPLE*) buf) + clumpoffset;
			outptr = sp->ds_buffer[ci][sp->scancount*vsamp + ypos];
			SensorCall();if (hsamp == 1) {
			    /* fast path for at least Cb and Cr */
			    SensorCall();for (nclump = clumps_per_line; nclump-- > 0; ) {
				SensorCall();*outptr++ = inptr[0];
				inptr += samples_per_clump;
			    }
			} else {
			    /* general case */
			    SensorCall();for (nclump = clumps_per_line; nclump-- > 0; ) {
				SensorCall();for (xpos = 0; xpos < hsamp; xpos++)
				    {/*29*/SensorCall();*outptr++ = inptr[xpos];/*30*/}
				SensorCall();inptr += samples_per_clump;
			    }
			}
			/* pad each scanline as needed */
			SensorCall();for (xpos = 0; xpos < padding; xpos++) {
			    SensorCall();*outptr = outptr[-1];
			    outptr++;
			}
			SensorCall();clumpoffset += hsamp;
		    }
		}
		SensorCall();sp->scancount++;
		SensorCall();if (sp->scancount >= DCTSIZE) {
			SensorCall();int n = sp->cinfo.c.max_v_samp_factor * DCTSIZE;
			SensorCall();if (TIFFjpeg_write_raw_data(sp, sp->ds_buffer, n) != n)
				{/*31*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*32*/}
			SensorCall();sp->scancount = 0;
		}
		SensorCall();tif->tif_row += sp->v_sampling;
		buf += bytesperclumpline;
		nrows -= sp->v_sampling;
	}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

/*
 * Finish up at the end of a strip or tile.
 */
static int
JPEGPostEncode(TIFF* tif)
{
	SensorCall();JPEGState *sp = JState(tif);

	SensorCall();if (sp->scancount > 0) {
		/*
		 * Need to emit a partial bufferload of downsampled data.
		 * Pad the data vertically.
		 */
		SensorCall();int ci, ypos, n;
		jpeg_component_info* compptr;

		SensorCall();for (ci = 0, compptr = sp->cinfo.c.comp_info;
		     ci < sp->cinfo.c.num_components;
		     ci++, compptr++) {
			SensorCall();int vsamp = compptr->v_samp_factor;
			tmsize_t row_width = compptr->width_in_blocks * DCTSIZE
				* sizeof(JSAMPLE);
			SensorCall();for (ypos = sp->scancount * vsamp;
			     ypos < DCTSIZE * vsamp; ypos++) {
				SensorCall();_TIFFmemcpy((void*)sp->ds_buffer[ci][ypos],
					    (void*)sp->ds_buffer[ci][ypos-1],
					    row_width);

			}
		}
		SensorCall();n = sp->cinfo.c.max_v_samp_factor * DCTSIZE;
		SensorCall();if (TIFFjpeg_write_raw_data(sp, sp->ds_buffer, n) != n)
			{/*137*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*138*/}
	}

	{int  ReplaceReturn = (TIFFjpeg_finish_compress(JState(tif))); SensorCall(); return ReplaceReturn;}
}

static void
JPEGCleanup(TIFF* tif)
{
	SensorCall();JPEGState *sp = JState(tif);
	
	assert(sp != 0);

	tif->tif_tagmethods.vgetfield = sp->vgetparent;
	tif->tif_tagmethods.vsetfield = sp->vsetparent;
	tif->tif_tagmethods.printdir = sp->printdir;

	SensorCall();if( sp != NULL ) {
		SensorCall();if( sp->cinfo_initialized )
		    {/*139*/SensorCall();TIFFjpeg_destroy(sp);/*140*/}	/* release libjpeg resources */
		SensorCall();if (sp->jpegtables)		/* tag value */
			{/*141*/SensorCall();_TIFFfree(sp->jpegtables);/*142*/}
	}
	SensorCall();_TIFFfree(tif->tif_data);	/* release local state */
	tif->tif_data = NULL;

	_TIFFSetDefaultCompressionState(tif);
SensorCall();}

static void 
JPEGResetUpsampled( TIFF* tif )
{
	SensorCall();JPEGState* sp = JState(tif);
	TIFFDirectory* td = &tif->tif_dir;

	/*
	 * Mark whether returned data is up-sampled or not so TIFFStripSize
	 * and TIFFTileSize return values that reflect the true amount of
	 * data.
	 */
	tif->tif_flags &= ~TIFF_UPSAMPLED;
	SensorCall();if (td->td_planarconfig == PLANARCONFIG_CONTIG) {
		SensorCall();if (td->td_photometric == PHOTOMETRIC_YCBCR &&
		    sp->jpegcolormode == JPEGCOLORMODE_RGB) {
			SensorCall();tif->tif_flags |= TIFF_UPSAMPLED;
		} else {
#ifdef notdef
			if (td->td_ycbcrsubsampling[0] != 1 ||
			    td->td_ycbcrsubsampling[1] != 1)
				; /* XXX what about up-sampling? */
#endif
		}
	}

	/*
	 * Must recalculate cached tile size in case sampling state changed.
	 * Should we really be doing this now if image size isn't set? 
	 */
        SensorCall();if( tif->tif_tilesize > 0 )
            {/*143*/SensorCall();tif->tif_tilesize = isTiled(tif) ? TIFFTileSize(tif) : (tmsize_t)(-1);/*144*/}   
        SensorCall();if( tif->tif_scanlinesize > 0 )
            {/*145*/SensorCall();tif->tif_scanlinesize = TIFFScanlineSize(tif);/*146*/} 
SensorCall();}

static int
JPEGVSetField(TIFF* tif, uint32 tag, va_list ap)
{
	SensorCall();JPEGState* sp = JState(tif);
	const TIFFField* fip;
	uint32 v32;

	assert(sp != NULL);

	SensorCall();switch (tag) {
	case TIFFTAG_JPEGTABLES:
		SensorCall();v32 = (uint32) va_arg(ap, uint32);
		SensorCall();if (v32 == 0) {
			/* XXX */
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
		SensorCall();_TIFFsetByteArray(&sp->jpegtables, va_arg(ap, void*),
		    (long) v32);
		sp->jpegtables_length = v32;
		TIFFSetFieldBit(tif, FIELD_JPEGTABLES);
		SensorCall();break;
	case TIFFTAG_JPEGQUALITY:
		SensorCall();sp->jpegquality = (int) va_arg(ap, int);
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}			/* pseudo tag */
	case TIFFTAG_JPEGCOLORMODE:
		SensorCall();sp->jpegcolormode = (int) va_arg(ap, int);
		JPEGResetUpsampled( tif );
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}			/* pseudo tag */
	case TIFFTAG_PHOTOMETRIC:
	{
		SensorCall();int ret_value = (*sp->vsetparent)(tif, tag, ap);
		JPEGResetUpsampled( tif );
		{int  ReplaceReturn = ret_value; SensorCall(); return ReplaceReturn;}
	}
	case TIFFTAG_JPEGTABLESMODE:
		SensorCall();sp->jpegtablesmode = (int) va_arg(ap, int);
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}			/* pseudo tag */
	case TIFFTAG_YCBCRSUBSAMPLING:
		/* mark the fact that we have a real ycbcrsubsampling! */
		SensorCall();sp->ycbcrsampling_fetched = 1;
		/* should we be recomputing upsampling info here? */
		{int  ReplaceReturn = (*sp->vsetparent)(tif, tag, ap); SensorCall(); return ReplaceReturn;}
	default:
		{int  ReplaceReturn = (*sp->vsetparent)(tif, tag, ap); SensorCall(); return ReplaceReturn;}
	}

	SensorCall();if ((fip = TIFFFieldWithTag(tif, tag))) {
		TIFFSetFieldBit(tif, fip->field_bit);
	} else {
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}

	SensorCall();tif->tif_flags |= TIFF_DIRTYDIRECT;
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

static int
JPEGVGetField(TIFF* tif, uint32 tag, va_list ap)
{
	SensorCall();JPEGState* sp = JState(tif);

	assert(sp != NULL);

	SensorCall();switch (tag) {
		case TIFFTAG_JPEGTABLES:
			SensorCall();*va_arg(ap, uint32*) = sp->jpegtables_length;
			*va_arg(ap, void**) = sp->jpegtables;
			SensorCall();break;
		case TIFFTAG_JPEGQUALITY:
			SensorCall();*va_arg(ap, int*) = sp->jpegquality;
			SensorCall();break;
		case TIFFTAG_JPEGCOLORMODE:
			SensorCall();*va_arg(ap, int*) = sp->jpegcolormode;
			SensorCall();break;
		case TIFFTAG_JPEGTABLESMODE:
			SensorCall();*va_arg(ap, int*) = sp->jpegtablesmode;
			SensorCall();break;
		default:
			{int  ReplaceReturn = (*sp->vgetparent)(tif, tag, ap); SensorCall(); return ReplaceReturn;}
	}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

static void
JPEGPrintDir(TIFF* tif, FILE* fd, long flags)
{
	SensorCall();JPEGState* sp = JState(tif);

	assert(sp != NULL);
	(void) flags;

        SensorCall();if( sp != NULL ) {
		SensorCall();if (TIFFFieldSet(tif,FIELD_JPEGTABLES))
			{/*147*/SensorCall();fprintf(fd, "  JPEG Tables: (%lu bytes)\n",
				(unsigned long) sp->jpegtables_length);/*148*/}
		SensorCall();if (sp->printdir)
			{/*149*/SensorCall();(*sp->printdir)(tif, fd, flags);/*150*/}
	}
SensorCall();}

static uint32
JPEGDefaultStripSize(TIFF* tif, uint32 s)
{
	SensorCall();JPEGState* sp = JState(tif);
	TIFFDirectory *td = &tif->tif_dir;

	s = (*sp->defsparent)(tif, s);
	SensorCall();if (s < td->td_imagelength)
		s = TIFFroundup_32(s, td->td_ycbcrsubsampling[1] * DCTSIZE);
	{uint32  ReplaceReturn = (s); SensorCall(); return ReplaceReturn;}
}

static void
JPEGDefaultTileSize(TIFF* tif, uint32* tw, uint32* th)
{
	SensorCall();JPEGState* sp = JState(tif);
	TIFFDirectory *td = &tif->tif_dir;

	(*sp->deftparent)(tif, tw, th);
	*tw = TIFFroundup_32(*tw, td->td_ycbcrsubsampling[0] * DCTSIZE);
	*th = TIFFroundup_32(*th, td->td_ycbcrsubsampling[1] * DCTSIZE);
}

/*
 * The JPEG library initialized used to be done in TIFFInitJPEG(), but
 * now that we allow a TIFF file to be opened in update mode it is necessary
 * to have some way of deciding whether compression or decompression is
 * desired other than looking at tif->tif_mode.  We accomplish this by 
 * examining {TILE/STRIP}BYTECOUNTS to see if there is a non-zero entry.
 * If so, we assume decompression is desired. 
 *
 * This is tricky, because TIFFInitJPEG() is called while the directory is
 * being read, and generally speaking the BYTECOUNTS tag won't have been read
 * at that point.  So we try to defer jpeg library initialization till we
 * do have that tag ... basically any access that might require the compressor
 * or decompressor that occurs after the reading of the directory. 
 *
 * In an ideal world compressors or decompressors would be setup
 * at the point where a single tile or strip was accessed (for read or write)
 * so that stuff like update of missing tiles, or replacement of tiles could
 * be done. However, we aren't trying to crack that nut just yet ...
 *
 * NFW, Feb 3rd, 2003.
 */

static int JPEGInitializeLibJPEG( TIFF * tif, int decompress )
{
    SensorCall();JPEGState* sp = JState(tif);

    SensorCall();if(sp->cinfo_initialized)
    {
        SensorCall();if( !decompress && sp->cinfo.comm.is_decompressor )
            {/*33*/SensorCall();TIFFjpeg_destroy( sp );/*34*/}
        else {/*35*/SensorCall();if( decompress && !sp->cinfo.comm.is_decompressor )
            {/*37*/SensorCall();TIFFjpeg_destroy( sp );/*38*/}
        else
            {/*39*/{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}/*40*/}/*36*/}

        SensorCall();sp->cinfo_initialized = 0;
    }

    /*
     * Initialize libjpeg.
     */
    SensorCall();if ( decompress ) {
        SensorCall();if (!TIFFjpeg_create_decompress(sp))
            {/*41*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*42*/}
    } else {
        SensorCall();if (!TIFFjpeg_create_compress(sp))
            {/*43*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*44*/}
    }

    SensorCall();sp->cinfo_initialized = TRUE;

    {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

int
TIFFInitJPEG(TIFF* tif, int scheme)
{
	SensorCall();JPEGState* sp;

	assert(scheme == COMPRESSION_JPEG);

	/*
	 * Merge codec-specific tag information.
	 */
	SensorCall();if (!_TIFFMergeFields(tif, jpegFields, TIFFArrayCount(jpegFields))) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata,
			     "TIFFInitJPEG",
			     "Merging JPEG codec-specific tags failed");
		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}

	/*
	 * Allocate state block so tag methods have storage to record values.
	 */
	SensorCall();tif->tif_data = (uint8*) _TIFFmalloc(sizeof (JPEGState));

	SensorCall();if (tif->tif_data == NULL) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata,
			     "TIFFInitJPEG", "No space for JPEG state block");
		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}
        SensorCall();_TIFFmemset(tif->tif_data, 0, sizeof(JPEGState));

	sp = JState(tif);
	sp->tif = tif;				/* back link */

	/*
	 * Override parent get/set field methods.
	 */
	sp->vgetparent = tif->tif_tagmethods.vgetfield;
	tif->tif_tagmethods.vgetfield = JPEGVGetField; /* hook for codec tags */
	sp->vsetparent = tif->tif_tagmethods.vsetfield;
	tif->tif_tagmethods.vsetfield = JPEGVSetField; /* hook for codec tags */
	sp->printdir = tif->tif_tagmethods.printdir;
	tif->tif_tagmethods.printdir = JPEGPrintDir;   /* hook for codec tags */

	/* Default values for codec-specific fields */
	sp->jpegtables = NULL;
	sp->jpegtables_length = 0;
	sp->jpegquality = 75;			/* Default IJG quality */
	sp->jpegcolormode = JPEGCOLORMODE_RAW;
	sp->jpegtablesmode = JPEGTABLESMODE_QUANT | JPEGTABLESMODE_HUFF;
        sp->ycbcrsampling_fetched = 0;

	/*
	 * Install codec methods.
	 */
	tif->tif_fixuptags = JPEGFixupTags;
	tif->tif_setupdecode = JPEGSetupDecode;
	tif->tif_predecode = JPEGPreDecode;
	tif->tif_decoderow = JPEGDecode;
	tif->tif_decodestrip = JPEGDecode;
	tif->tif_decodetile = JPEGDecode;
	tif->tif_setupencode = JPEGSetupEncode;
	tif->tif_preencode = JPEGPreEncode;
	tif->tif_postencode = JPEGPostEncode;
	tif->tif_encoderow = JPEGEncode;
	tif->tif_encodestrip = JPEGEncode;
	tif->tif_encodetile = JPEGEncode;  
	tif->tif_cleanup = JPEGCleanup;
	sp->defsparent = tif->tif_defstripsize;
	tif->tif_defstripsize = JPEGDefaultStripSize;
	sp->deftparent = tif->tif_deftilesize;
	tif->tif_deftilesize = JPEGDefaultTileSize;
	tif->tif_flags |= TIFF_NOBITREV;	/* no bit reversal, please */

        sp->cinfo_initialized = FALSE;

	/*
        ** Create a JPEGTables field if no directory has yet been created. 
        ** We do this just to ensure that sufficient space is reserved for
        ** the JPEGTables field.  It will be properly created the right
        ** size later. 
        */
        SensorCall();if( tif->tif_diroff == 0 )
        {
#define SIZE_OF_JPEGTABLES 2000
/*
The following line assumes incorrectly that all JPEG-in-TIFF files will have
a JPEGTABLES tag generated and causes null-filled JPEGTABLES tags to be written
when the JPEG data is placed with TIFFWriteRawStrip.  The field bit should be 
set, anyway, later when actual JPEGTABLES header is generated, so removing it 
here hopefully is harmless.
            TIFFSetFieldBit(tif, FIELD_JPEGTABLES);
*/
            SensorCall();sp->jpegtables_length = SIZE_OF_JPEGTABLES;
            sp->jpegtables = (void *) _TIFFmalloc(sp->jpegtables_length);
	    // FIXME: NULL-deref after malloc failure
	    _TIFFmemset(sp->jpegtables, 0, SIZE_OF_JPEGTABLES);
#undef SIZE_OF_JPEGTABLES
        }

	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}
#endif /* JPEG_SUPPORT */

/* vim: set ts=8 sts=8 sw=8 noet: */

/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
