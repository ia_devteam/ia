/* $Id: tif_print.c,v 1.60 2012-08-19 16:56:35 bfriesen Exp $ */

/*
 * Copyright (c) 1988-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

/*
 * TIFF Library.
 *
 * Directory Printing Support
 */
#include "tiffiop.h"
#include "/var/tmp/sensor.h"
#include <stdio.h>

#include <ctype.h>

static void
_TIFFprintAsciiBounded(FILE* fd, const char* cp, int max_chars);

static const char *photoNames[] = {
    "min-is-white",				/* PHOTOMETRIC_MINISWHITE */
    "min-is-black",				/* PHOTOMETRIC_MINISBLACK */
    "RGB color",				/* PHOTOMETRIC_RGB */
    "palette color (RGB from colormap)",	/* PHOTOMETRIC_PALETTE */
    "transparency mask",			/* PHOTOMETRIC_MASK */
    "separated",				/* PHOTOMETRIC_SEPARATED */
    "YCbCr",					/* PHOTOMETRIC_YCBCR */
    "7 (0x7)",
    "CIE L*a*b*",				/* PHOTOMETRIC_CIELAB */
    "ICC L*a*b*",				/* PHOTOMETRIC_ICCLAB */
    "ITU L*a*b*" 				/* PHOTOMETRIC_ITULAB */
};
#define	NPHOTONAMES	(sizeof (photoNames) / sizeof (photoNames[0]))

static const char *orientNames[] = {
    "0 (0x0)",
    "row 0 top, col 0 lhs",			/* ORIENTATION_TOPLEFT */
    "row 0 top, col 0 rhs",			/* ORIENTATION_TOPRIGHT */
    "row 0 bottom, col 0 rhs",			/* ORIENTATION_BOTRIGHT */
    "row 0 bottom, col 0 lhs",			/* ORIENTATION_BOTLEFT */
    "row 0 lhs, col 0 top",			/* ORIENTATION_LEFTTOP */
    "row 0 rhs, col 0 top",			/* ORIENTATION_RIGHTTOP */
    "row 0 rhs, col 0 bottom",			/* ORIENTATION_RIGHTBOT */
    "row 0 lhs, col 0 bottom",			/* ORIENTATION_LEFTBOT */
};
#define	NORIENTNAMES	(sizeof (orientNames) / sizeof (orientNames[0]))

static void
_TIFFPrintField(FILE* fd, const TIFFField *fip,
		uint32 value_count, void *raw_data)
{
	SensorCall();uint32 j;
		
	fprintf(fd, "  %s: ", fip->field_name);

	SensorCall();for(j = 0; j < value_count; j++) {
		SensorCall();if(fip->field_type == TIFF_BYTE)
			{/*87*/SensorCall();fprintf(fd, "%u", ((uint8 *) raw_data)[j]);/*88*/}
		else {/*89*/SensorCall();if(fip->field_type == TIFF_UNDEFINED)
			{/*91*/SensorCall();fprintf(fd, "0x%x",
			    (unsigned int) ((unsigned char *) raw_data)[j]);/*92*/}
		else {/*93*/SensorCall();if(fip->field_type == TIFF_SBYTE)
			{/*95*/SensorCall();fprintf(fd, "%d", ((int8 *) raw_data)[j]);/*96*/}
		else {/*97*/SensorCall();if(fip->field_type == TIFF_SHORT)
			{/*99*/SensorCall();fprintf(fd, "%u", ((uint16 *) raw_data)[j]);/*100*/}
		else {/*101*/SensorCall();if(fip->field_type == TIFF_SSHORT)
			{/*103*/SensorCall();fprintf(fd, "%d", ((int16 *) raw_data)[j]);/*104*/}
		else {/*105*/SensorCall();if(fip->field_type == TIFF_LONG)
			{/*107*/SensorCall();fprintf(fd, "%lu",
			    (unsigned long)((uint32 *) raw_data)[j]);/*108*/}
		else {/*109*/SensorCall();if(fip->field_type == TIFF_SLONG)
			{/*111*/SensorCall();fprintf(fd, "%ld", (long)((int32 *) raw_data)[j]);/*112*/}
		else {/*113*/SensorCall();if(fip->field_type == TIFF_IFD)
			{/*115*/SensorCall();fprintf(fd, "0x%lx",
				(unsigned long)((uint32 *) raw_data)[j]);/*116*/}
		else {/*117*/SensorCall();if(fip->field_type == TIFF_RATIONAL
			|| fip->field_type == TIFF_SRATIONAL
			|| fip->field_type == TIFF_FLOAT)
			{/*119*/SensorCall();fprintf(fd, "%f", ((float *) raw_data)[j]);/*120*/}
		else {/*121*/SensorCall();if(fip->field_type == TIFF_LONG8)
#if defined(__WIN32__) && (defined(_MSC_VER) || defined(__MINGW32__))
			fprintf(fd, "%I64u",
			    (unsigned __int64)((uint64 *) raw_data)[j]);
#else
			{/*123*/SensorCall();fprintf(fd, "%llu",
			    (unsigned long long)((uint64 *) raw_data)[j]);/*124*/}
#endif
		else {/*125*/SensorCall();if(fip->field_type == TIFF_SLONG8)
#if defined(__WIN32__) && (defined(_MSC_VER) || defined(__MINGW32__))
			fprintf(fd, "%I64d", (__int64)((int64 *) raw_data)[j]);
#else
			{/*127*/SensorCall();fprintf(fd, "%lld", (long long)((int64 *) raw_data)[j]);/*128*/}
#endif
		else {/*129*/SensorCall();if(fip->field_type == TIFF_IFD8)
#if defined(__WIN32__) && (defined(_MSC_VER) || defined(__MINGW32__))
			fprintf(fd, "0x%I64x",
				(unsigned __int64)((uint64 *) raw_data)[j]);
#else
			{/*131*/SensorCall();fprintf(fd, "0x%llx",
				(unsigned long long)((uint64 *) raw_data)[j]);/*132*/}
#endif
		else {/*133*/SensorCall();if(fip->field_type == TIFF_FLOAT)
			{/*135*/SensorCall();fprintf(fd, "%f", ((float *)raw_data)[j]);/*136*/}
		else {/*137*/SensorCall();if(fip->field_type == TIFF_DOUBLE)
			{/*139*/SensorCall();fprintf(fd, "%f", ((double *) raw_data)[j]);/*140*/}
		else {/*141*/SensorCall();if(fip->field_type == TIFF_ASCII) {
			SensorCall();fprintf(fd, "%s", (char *) raw_data);
			SensorCall();break;
		}
		else {
			SensorCall();fprintf(fd, "<unsupported data type in TIFFPrint>");
			SensorCall();break;
		;/*142*/}/*138*/}/*134*/}/*130*/}/*126*/}/*122*/}/*118*/}/*114*/}/*110*/}/*106*/}/*102*/}/*98*/}/*94*/}/*90*/}}

		SensorCall();if(j < value_count - 1)
			{/*143*/SensorCall();fprintf(fd, ",");/*144*/}
	}

	SensorCall();fprintf(fd, "\n");
SensorCall();}

static int
_TIFFPrettyPrintField(TIFF* tif, const TIFFField *fip, FILE* fd, uint32 tag,
		      uint32 value_count, void *raw_data)
{
        SensorCall();(void) tif;

	/* do not try to pretty print auto-defined fields */
	SensorCall();if (strncmp(fip->field_name,"Tag ", 4) == 0) {
		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}
        
	SensorCall();switch (tag)
	{
		case TIFFTAG_INKSET:
			SensorCall();if (value_count == 2 && fip->field_type == TIFF_SHORT) {
				SensorCall();fprintf(fd, "  Ink Set: ");
				SensorCall();switch (*((uint16*)raw_data)) {
				case INKSET_CMYK:
					SensorCall();fprintf(fd, "CMYK\n");
					SensorCall();break;
				default:
					SensorCall();fprintf(fd, "%u (0x%x)\n",
						*((uint16*)raw_data),
						*((uint16*)raw_data));
					SensorCall();break;
				}
				{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
			}
			{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}

		case TIFFTAG_DOTRANGE:
			SensorCall();if (value_count == 2 && fip->field_type == TIFF_SHORT) {
				SensorCall();fprintf(fd, "  Dot Range: %u-%u\n",
					((uint16*)raw_data)[0], ((uint16*)raw_data)[1]);
				{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
			}
			{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}

		case TIFFTAG_WHITEPOINT:
			SensorCall();if (value_count == 2 && fip->field_type == TIFF_RATIONAL) {
				SensorCall();fprintf(fd, "  White Point: %g-%g\n",
					((float *)raw_data)[0], ((float *)raw_data)[1]);
				{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
			} 
			{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}

		case TIFFTAG_XMLPACKET:
		{
			SensorCall();uint32 i;

			fprintf(fd, "  XMLPacket (XMP Metadata):\n" );
			SensorCall();for(i = 0; i < value_count; i++)
				{/*145*/SensorCall();fputc(((char *)raw_data)[i], fd);/*146*/}
			SensorCall();fprintf( fd, "\n" );
			{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
		}
		case TIFFTAG_RICHTIFFIPTC:
			/*
			 * XXX: for some weird reason RichTIFFIPTC tag
			 * defined as array of LONG values.
			 */
			SensorCall();fprintf(fd,
			    "  RichTIFFIPTC Data: <present>, %lu bytes\n",
			    (unsigned long) value_count * 4);
			{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}

		case TIFFTAG_PHOTOSHOP:
			SensorCall();fprintf(fd, "  Photoshop Data: <present>, %lu bytes\n",
			    (unsigned long) value_count);
			{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}

		case TIFFTAG_ICCPROFILE:
			SensorCall();fprintf(fd, "  ICC Profile: <present>, %lu bytes\n",
			    (unsigned long) value_count);
			{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}

		case TIFFTAG_STONITS:
			SensorCall();if (value_count == 1 && fip->field_type == TIFF_DOUBLE) { 
				SensorCall();fprintf(fd,
					"  Sample to Nits conversion factor: %.4e\n",
					*((double*)raw_data));
				{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
			}
			{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}

	{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

/*
 * Print the contents of the current directory
 * to the specified stdio file stream.
 */
void
TIFFPrintDirectory(TIFF* tif, FILE* fd, long flags)
{
	SensorCall();TIFFDirectory *td = &tif->tif_dir;
	char *sep;
	uint16 i;
	long l, n;

#if defined(__WIN32__) && (defined(_MSC_VER) || defined(__MINGW32__))
	fprintf(fd, "TIFF Directory at offset 0x%I64x (%I64u)\n",
		(unsigned __int64) tif->tif_diroff,
		(unsigned __int64) tif->tif_diroff);
#else
	fprintf(fd, "TIFF Directory at offset 0x%llx (%llu)\n",
		(unsigned long long) tif->tif_diroff,
		(unsigned long long) tif->tif_diroff);
#endif
	SensorCall();if (TIFFFieldSet(tif,FIELD_SUBFILETYPE)) {
		SensorCall();fprintf(fd, "  Subfile Type:");
		sep = " ";
		SensorCall();if (td->td_subfiletype & FILETYPE_REDUCEDIMAGE) {
			SensorCall();fprintf(fd, "%sreduced-resolution image", sep);
			sep = "/";
		}
		SensorCall();if (td->td_subfiletype & FILETYPE_PAGE) {
			SensorCall();fprintf(fd, "%smulti-page document", sep);
			sep = "/";
		}
		SensorCall();if (td->td_subfiletype & FILETYPE_MASK)
			{/*1*/SensorCall();fprintf(fd, "%stransparency mask", sep);/*2*/}
		SensorCall();fprintf(fd, " (%lu = 0x%lx)\n",
		    (long) td->td_subfiletype, (long) td->td_subfiletype);
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_IMAGEDIMENSIONS)) {
		SensorCall();fprintf(fd, "  Image Width: %lu Image Length: %lu",
		    (unsigned long) td->td_imagewidth, (unsigned long) td->td_imagelength);
		SensorCall();if (TIFFFieldSet(tif,FIELD_IMAGEDEPTH))
			{/*3*/SensorCall();fprintf(fd, " Image Depth: %lu",
			    (unsigned long) td->td_imagedepth);/*4*/}
		SensorCall();fprintf(fd, "\n");
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_TILEDIMENSIONS)) {
		SensorCall();fprintf(fd, "  Tile Width: %lu Tile Length: %lu",
		    (unsigned long) td->td_tilewidth, (unsigned long) td->td_tilelength);
		SensorCall();if (TIFFFieldSet(tif,FIELD_TILEDEPTH))
			{/*5*/SensorCall();fprintf(fd, " Tile Depth: %lu",
			    (unsigned long) td->td_tiledepth);/*6*/}
		SensorCall();fprintf(fd, "\n");
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_RESOLUTION)) {
		SensorCall();fprintf(fd, "  Resolution: %g, %g",
		    td->td_xresolution, td->td_yresolution);
		SensorCall();if (TIFFFieldSet(tif,FIELD_RESOLUTIONUNIT)) {
			SensorCall();switch (td->td_resolutionunit) {
			case RESUNIT_NONE:
				SensorCall();fprintf(fd, " (unitless)");
				SensorCall();break;
			case RESUNIT_INCH:
				SensorCall();fprintf(fd, " pixels/inch");
				SensorCall();break;
			case RESUNIT_CENTIMETER:
				SensorCall();fprintf(fd, " pixels/cm");
				SensorCall();break;
			default:
				SensorCall();fprintf(fd, " (unit %u = 0x%x)",
				    td->td_resolutionunit,
				    td->td_resolutionunit);
				SensorCall();break;
			}
		}
		SensorCall();fprintf(fd, "\n");
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_POSITION))
		{/*7*/SensorCall();fprintf(fd, "  Position: %g, %g\n",
		    td->td_xposition, td->td_yposition);/*8*/}
	SensorCall();if (TIFFFieldSet(tif,FIELD_BITSPERSAMPLE))
		{/*9*/SensorCall();fprintf(fd, "  Bits/Sample: %u\n", td->td_bitspersample);/*10*/}
	SensorCall();if (TIFFFieldSet(tif,FIELD_SAMPLEFORMAT)) {
		SensorCall();fprintf(fd, "  Sample Format: ");
		SensorCall();switch (td->td_sampleformat) {
		case SAMPLEFORMAT_VOID:
			SensorCall();fprintf(fd, "void\n");
			SensorCall();break;
		case SAMPLEFORMAT_INT:
			SensorCall();fprintf(fd, "signed integer\n");
			SensorCall();break;
		case SAMPLEFORMAT_UINT:
			SensorCall();fprintf(fd, "unsigned integer\n");
			SensorCall();break;
		case SAMPLEFORMAT_IEEEFP:
			SensorCall();fprintf(fd, "IEEE floating point\n");
			SensorCall();break;
		case SAMPLEFORMAT_COMPLEXINT:
			SensorCall();fprintf(fd, "complex signed integer\n");
			SensorCall();break;
		case SAMPLEFORMAT_COMPLEXIEEEFP:
			SensorCall();fprintf(fd, "complex IEEE floating point\n");
			SensorCall();break;
		default:
			SensorCall();fprintf(fd, "%u (0x%x)\n",
			    td->td_sampleformat, td->td_sampleformat);
			SensorCall();break;
		}
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_COMPRESSION)) {
		SensorCall();const TIFFCodec* c = TIFFFindCODEC(td->td_compression);
		fprintf(fd, "  Compression Scheme: ");
		SensorCall();if (c)
			{/*11*/SensorCall();fprintf(fd, "%s\n", c->name);/*12*/}
		else
			{/*13*/SensorCall();fprintf(fd, "%u (0x%x)\n",
			    td->td_compression, td->td_compression);/*14*/}
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_PHOTOMETRIC)) {
		SensorCall();fprintf(fd, "  Photometric Interpretation: ");
		SensorCall();if (td->td_photometric < NPHOTONAMES)
			{/*15*/SensorCall();fprintf(fd, "%s\n", photoNames[td->td_photometric]);/*16*/}
		else {
			SensorCall();switch (td->td_photometric) {
			case PHOTOMETRIC_LOGL:
				SensorCall();fprintf(fd, "CIE Log2(L)\n");
				SensorCall();break;
			case PHOTOMETRIC_LOGLUV:
				SensorCall();fprintf(fd, "CIE Log2(L) (u',v')\n");
				SensorCall();break;
			default:
				SensorCall();fprintf(fd, "%u (0x%x)\n",
				    td->td_photometric, td->td_photometric);
				SensorCall();break;
			}
		}
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_EXTRASAMPLES) && td->td_extrasamples) {
		SensorCall();fprintf(fd, "  Extra Samples: %u<", td->td_extrasamples);
		sep = "";
		SensorCall();for (i = 0; i < td->td_extrasamples; i++) {
			SensorCall();switch (td->td_sampleinfo[i]) {
			case EXTRASAMPLE_UNSPECIFIED:
				SensorCall();fprintf(fd, "%sunspecified", sep);
				SensorCall();break;
			case EXTRASAMPLE_ASSOCALPHA:
				SensorCall();fprintf(fd, "%sassoc-alpha", sep);
				SensorCall();break;
			case EXTRASAMPLE_UNASSALPHA:
				SensorCall();fprintf(fd, "%sunassoc-alpha", sep);
				SensorCall();break;
			default:
				SensorCall();fprintf(fd, "%s%u (0x%x)", sep,
				    td->td_sampleinfo[i], td->td_sampleinfo[i]);
				SensorCall();break;
			}
			SensorCall();sep = ", ";
		}
		SensorCall();fprintf(fd, ">\n");
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_INKNAMES)) {
		SensorCall();char* cp;
		fprintf(fd, "  Ink Names: ");
		i = td->td_samplesperpixel;
		sep = "";
		SensorCall();for (cp = td->td_inknames; 
		     i > 0 && cp < td->td_inknames + td->td_inknameslen; 
		     cp = strchr(cp,'\0')+1, i--) {
			SensorCall();int max_chars = 
				td->td_inknameslen - (cp - td->td_inknames);
			fputs(sep, fd);
			_TIFFprintAsciiBounded(fd, cp, max_chars);
			sep = ", ";
		}
                SensorCall();fputs("\n", fd);
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_THRESHHOLDING)) {
		SensorCall();fprintf(fd, "  Thresholding: ");
		SensorCall();switch (td->td_threshholding) {
		case THRESHHOLD_BILEVEL:
			SensorCall();fprintf(fd, "bilevel art scan\n");
			SensorCall();break;
		case THRESHHOLD_HALFTONE:
			SensorCall();fprintf(fd, "halftone or dithered scan\n");
			SensorCall();break;
		case THRESHHOLD_ERRORDIFFUSE:
			SensorCall();fprintf(fd, "error diffused\n");
			SensorCall();break;
		default:
			SensorCall();fprintf(fd, "%u (0x%x)\n",
			    td->td_threshholding, td->td_threshholding);
			SensorCall();break;
		}
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_FILLORDER)) {
		SensorCall();fprintf(fd, "  FillOrder: ");
		SensorCall();switch (td->td_fillorder) {
		case FILLORDER_MSB2LSB:
			SensorCall();fprintf(fd, "msb-to-lsb\n");
			SensorCall();break;
		case FILLORDER_LSB2MSB:
			SensorCall();fprintf(fd, "lsb-to-msb\n");
			SensorCall();break;
		default:
			SensorCall();fprintf(fd, "%u (0x%x)\n",
			    td->td_fillorder, td->td_fillorder);
			SensorCall();break;
		}
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_YCBCRSUBSAMPLING))
        {
		SensorCall();fprintf(fd, "  YCbCr Subsampling: %u, %u\n",
			td->td_ycbcrsubsampling[0], td->td_ycbcrsubsampling[1] );
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_YCBCRPOSITIONING)) {
		SensorCall();fprintf(fd, "  YCbCr Positioning: ");
		SensorCall();switch (td->td_ycbcrpositioning) {
		case YCBCRPOSITION_CENTERED:
			SensorCall();fprintf(fd, "centered\n");
			SensorCall();break;
		case YCBCRPOSITION_COSITED:
			SensorCall();fprintf(fd, "cosited\n");
			SensorCall();break;
		default:
			SensorCall();fprintf(fd, "%u (0x%x)\n",
			    td->td_ycbcrpositioning, td->td_ycbcrpositioning);
			SensorCall();break;
		}
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_HALFTONEHINTS))
		{/*17*/SensorCall();fprintf(fd, "  Halftone Hints: light %u dark %u\n",
		    td->td_halftonehints[0], td->td_halftonehints[1]);/*18*/}
	SensorCall();if (TIFFFieldSet(tif,FIELD_ORIENTATION)) {
		SensorCall();fprintf(fd, "  Orientation: ");
		SensorCall();if (td->td_orientation < NORIENTNAMES)
			{/*19*/SensorCall();fprintf(fd, "%s\n", orientNames[td->td_orientation]);/*20*/}
		else
			{/*21*/SensorCall();fprintf(fd, "%u (0x%x)\n",
			    td->td_orientation, td->td_orientation);/*22*/}
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_SAMPLESPERPIXEL))
		{/*23*/SensorCall();fprintf(fd, "  Samples/Pixel: %u\n", td->td_samplesperpixel);/*24*/}
	SensorCall();if (TIFFFieldSet(tif,FIELD_ROWSPERSTRIP)) {
		SensorCall();fprintf(fd, "  Rows/Strip: ");
		SensorCall();if (td->td_rowsperstrip == (uint32) -1)
			{/*25*/SensorCall();fprintf(fd, "(infinite)\n");/*26*/}
		else
			{/*27*/SensorCall();fprintf(fd, "%lu\n", (unsigned long) td->td_rowsperstrip);/*28*/}
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_MINSAMPLEVALUE))
		{/*29*/SensorCall();fprintf(fd, "  Min Sample Value: %u\n", td->td_minsamplevalue);/*30*/}
	SensorCall();if (TIFFFieldSet(tif,FIELD_MAXSAMPLEVALUE))
		{/*31*/SensorCall();fprintf(fd, "  Max Sample Value: %u\n", td->td_maxsamplevalue);/*32*/}
	SensorCall();if (TIFFFieldSet(tif,FIELD_SMINSAMPLEVALUE)) {
		SensorCall();int count = (tif->tif_flags & TIFF_PERSAMPLE) ? td->td_samplesperpixel : 1;
		fprintf(fd, "  SMin Sample Value:");
		SensorCall();for (i = 0; i < count; ++i)
			{/*33*/SensorCall();fprintf(fd, " %g", td->td_sminsamplevalue[i]);/*34*/}
		SensorCall();fprintf(fd, "\n");
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_SMAXSAMPLEVALUE)) {
		SensorCall();int count = (tif->tif_flags & TIFF_PERSAMPLE) ? td->td_samplesperpixel : 1;
		fprintf(fd, "  SMax Sample Value:");
		SensorCall();for (i = 0; i < count; ++i)
			{/*35*/SensorCall();fprintf(fd, " %g", td->td_smaxsamplevalue[i]);/*36*/}
		SensorCall();fprintf(fd, "\n");
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_PLANARCONFIG)) {
		SensorCall();fprintf(fd, "  Planar Configuration: ");
		SensorCall();switch (td->td_planarconfig) {
		case PLANARCONFIG_CONTIG:
			SensorCall();fprintf(fd, "single image plane\n");
			SensorCall();break;
		case PLANARCONFIG_SEPARATE:
			SensorCall();fprintf(fd, "separate image planes\n");
			SensorCall();break;
		default:
			SensorCall();fprintf(fd, "%u (0x%x)\n",
			    td->td_planarconfig, td->td_planarconfig);
			SensorCall();break;
		}
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_PAGENUMBER))
		{/*37*/SensorCall();fprintf(fd, "  Page Number: %u-%u\n",
		    td->td_pagenumber[0], td->td_pagenumber[1]);/*38*/}
	SensorCall();if (TIFFFieldSet(tif,FIELD_COLORMAP)) {
		SensorCall();fprintf(fd, "  Color Map: ");
		SensorCall();if (flags & TIFFPRINT_COLORMAP) {
			SensorCall();fprintf(fd, "\n");
			n = 1L<<td->td_bitspersample;
			SensorCall();for (l = 0; l < n; l++)
				{/*39*/SensorCall();fprintf(fd, "   %5lu: %5u %5u %5u\n",
				    l,
				    td->td_colormap[0][l],
				    td->td_colormap[1][l],
				    td->td_colormap[2][l]);/*40*/}
		} else
			{/*41*/SensorCall();fprintf(fd, "(present)\n");/*42*/}
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_REFBLACKWHITE)) {
		SensorCall();fprintf(fd, "  Reference Black/White:\n");
		SensorCall();for (i = 0; i < 3; i++)
		{/*43*/SensorCall();fprintf(fd, "    %2d: %5g %5g\n", i,
			td->td_refblackwhite[2*i+0],
			td->td_refblackwhite[2*i+1]);/*44*/}
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_TRANSFERFUNCTION)) {
		SensorCall();fprintf(fd, "  Transfer Function: ");
		SensorCall();if (flags & TIFFPRINT_CURVES) {
			SensorCall();fprintf(fd, "\n");
			n = 1L<<td->td_bitspersample;
			SensorCall();for (l = 0; l < n; l++) {
				SensorCall();fprintf(fd, "    %2lu: %5u",
				    l, td->td_transferfunction[0][l]);
				SensorCall();for (i = 1; i < td->td_samplesperpixel; i++)
					{/*45*/SensorCall();fprintf(fd, " %5u",
					    td->td_transferfunction[i][l]);/*46*/}
				SensorCall();fputc('\n', fd);
			}
		} else
			{/*47*/SensorCall();fprintf(fd, "(present)\n");/*48*/}
	}
	SensorCall();if (TIFFFieldSet(tif, FIELD_SUBIFD) && (td->td_subifd)) {
		SensorCall();fprintf(fd, "  SubIFD Offsets:");
		SensorCall();for (i = 0; i < td->td_nsubifd; i++)
#if defined(__WIN32__) && (defined(_MSC_VER) || defined(__MINGW32__))
			fprintf(fd, " %5I64u",
				(unsigned __int64) td->td_subifd[i]);
#else
			{/*49*/SensorCall();fprintf(fd, " %5llu",
				(unsigned long long) td->td_subifd[i]);/*50*/}
#endif
		SensorCall();fputc('\n', fd);
	}

	/*
	** Custom tag support.
	*/
	{
		SensorCall();int  i;
		short count;

		count = (short) TIFFGetTagListCount(tif);
		SensorCall();for(i = 0; i < count; i++) {
			SensorCall();uint32 tag = TIFFGetTagListEntry(tif, i);
			const TIFFField *fip;
			uint32 value_count;
			int mem_alloc = 0;
			void *raw_data;

			fip = TIFFFieldWithTag(tif, tag);
			SensorCall();if(fip == NULL)
				{/*51*/SensorCall();continue;/*52*/}

			SensorCall();if(fip->field_passcount) {
				SensorCall();if (fip->field_readcount == TIFF_VARIABLE2 ) {
					SensorCall();if(TIFFGetField(tif, tag, &value_count, &raw_data) != 1)
						{/*53*/SensorCall();continue;/*54*/}
				} else {/*55*/SensorCall();if (fip->field_readcount == TIFF_VARIABLE ) {
					SensorCall();uint16 small_value_count;
					SensorCall();if(TIFFGetField(tif, tag, &small_value_count, &raw_data) != 1)
						{/*57*/SensorCall();continue;/*58*/}
					SensorCall();value_count = small_value_count;
				} else {
					assert (fip->field_readcount == TIFF_VARIABLE
						|| fip->field_readcount == TIFF_VARIABLE2);
					SensorCall();continue;
				;/*56*/}} 
			} else {
				SensorCall();if (fip->field_readcount == TIFF_VARIABLE
				    || fip->field_readcount == TIFF_VARIABLE2)
					{/*59*/SensorCall();value_count = 1;/*60*/}
				else {/*61*/SensorCall();if (fip->field_readcount == TIFF_SPP)
					{/*63*/SensorCall();value_count = td->td_samplesperpixel;/*64*/}
				else
					{/*65*/SensorCall();value_count = fip->field_readcount;/*66*/}/*62*/}
				SensorCall();if (fip->field_tag == TIFFTAG_DOTRANGE
				    && strcmp(fip->field_name,"DotRange") == 0) {
					/* TODO: This is an evil exception and should not have been
					   handled this way ... likely best if we move it into
					   the directory structure with an explicit field in 
					   libtiff 4.1 and assign it a FIELD_ value */
					SensorCall();static uint16 dotrange[2];
					raw_data = dotrange;
					TIFFGetField(tif, tag, dotrange+0, dotrange+1);
				} else {/*67*/SensorCall();if (fip->field_type == TIFF_ASCII
					   || fip->field_readcount == TIFF_VARIABLE
					   || fip->field_readcount == TIFF_VARIABLE2
					   || fip->field_readcount == TIFF_SPP
					   || value_count > 1) {
					SensorCall();if(TIFFGetField(tif, tag, &raw_data) != 1)
						{/*69*/SensorCall();continue;/*70*/}
				} else {
					SensorCall();raw_data = _TIFFmalloc(
					    _TIFFDataSize(fip->field_type)
					    * value_count);
					mem_alloc = 1;
					SensorCall();if(TIFFGetField(tif, tag, raw_data) != 1) {
						SensorCall();_TIFFfree(raw_data);
						SensorCall();continue;
					}
				;/*68*/}}
			}

			/*
			 * Catch the tags which needs to be specially handled
			 * and pretty print them. If tag not handled in
			 * _TIFFPrettyPrintField() fall down and print it as
			 * any other tag.
			 */
			SensorCall();if (!_TIFFPrettyPrintField(tif, fip, fd, tag, value_count, raw_data))
				{/*71*/SensorCall();_TIFFPrintField(fd, fip, value_count, raw_data);/*72*/}

			SensorCall();if(mem_alloc)
				{/*73*/SensorCall();_TIFFfree(raw_data);/*74*/}
		}
	}
        
	SensorCall();if (tif->tif_tagmethods.printdir)
		{/*75*/SensorCall();(*tif->tif_tagmethods.printdir)(tif, fd, flags);/*76*/}

        SensorCall();_TIFFFillStriles( tif );
        
	SensorCall();if ((flags & TIFFPRINT_STRIPS) &&
	    TIFFFieldSet(tif,FIELD_STRIPOFFSETS)) {
		SensorCall();uint32 s;

		fprintf(fd, "  %lu %s:\n",
		    (long) td->td_nstrips,
		    isTiled(tif) ? "Tiles" : "Strips");
		SensorCall();for (s = 0; s < td->td_nstrips; s++)
#if defined(__WIN32__) && (defined(_MSC_VER) || defined(__MINGW32__))
			fprintf(fd, "    %3lu: [%8I64u, %8I64u]\n",
			    (unsigned long) s,
			    (unsigned __int64) td->td_stripoffset[s],
			    (unsigned __int64) td->td_stripbytecount[s]);
#else
			{/*77*/SensorCall();fprintf(fd, "    %3lu: [%8llu, %8llu]\n",
			    (unsigned long) s,
			    (unsigned long long) td->td_stripoffset[s],
			    (unsigned long long) td->td_stripbytecount[s]);/*78*/}
#endif
	}
SensorCall();}

void
_TIFFprintAscii(FILE* fd, const char* cp)
{
	SensorCall();_TIFFprintAsciiBounded( fd, cp, strlen(cp));
SensorCall();}

static void
_TIFFprintAsciiBounded(FILE* fd, const char* cp, int max_chars)
{
	SensorCall();for (; max_chars > 0 && *cp != '\0'; cp++, max_chars--) {
		SensorCall();const char* tp;

		SensorCall();if (isprint((int)*cp)) {
			SensorCall();fputc(*cp, fd);
			SensorCall();continue;
		}
		SensorCall();for (tp = "\tt\bb\rr\nn\vv"; *tp; tp++)
			{/*79*/SensorCall();if (*tp++ == *cp)
				{/*81*/SensorCall();break;/*82*/}/*80*/}
		SensorCall();if (*tp)
			{/*83*/SensorCall();fprintf(fd, "\\%c", *tp);/*84*/}
		else
			{/*85*/SensorCall();fprintf(fd, "\\%03o", *cp & 0xff);/*86*/}
	}
SensorCall();}

void
_TIFFprintAsciiTag(FILE* fd, const char* name, const char* value)
{
	SensorCall();fprintf(fd, "  %s: \"", name);
	_TIFFprintAscii(fd, value);
	fprintf(fd, "\"\n");
SensorCall();}

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
