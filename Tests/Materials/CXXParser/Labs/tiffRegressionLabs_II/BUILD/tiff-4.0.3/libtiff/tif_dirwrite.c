/* $Id: tif_dirwrite.c,v 1.77 2012-07-06 19:18:31 bfriesen Exp $ */

/*
 * Copyright (c) 1988-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

/*
 * TIFF Library.
 *
 * Directory Write Support Routines.
 */
#include "tiffiop.h"
#include "/var/tmp/sensor.h"

#ifdef HAVE_IEEEFP
#define TIFFCvtNativeToIEEEFloat(tif, n, fp)
#define TIFFCvtNativeToIEEEDouble(tif, n, dp)
#else
extern void TIFFCvtNativeToIEEEFloat(TIFF* tif, uint32 n, float* fp);
extern void TIFFCvtNativeToIEEEDouble(TIFF* tif, uint32 n, double* dp);
#endif

static int TIFFWriteDirectorySec(TIFF* tif, int isimage, int imagedone, uint64* pdiroff);

static int TIFFWriteDirectoryTagSampleformatArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, double* value);
#if 0
static int TIFFWriteDirectoryTagSampleformatPerSample(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, double value);
#endif

static int TIFFWriteDirectoryTagAscii(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, char* value);
static int TIFFWriteDirectoryTagUndefinedArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint8* value);
#ifdef notdef
static int TIFFWriteDirectoryTagByte(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint8 value);
#endif
static int TIFFWriteDirectoryTagByteArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint8* value);
#if 0
static int TIFFWriteDirectoryTagBytePerSample(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint8 value);
#endif
#ifdef notdef
static int TIFFWriteDirectoryTagSbyte(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int8 value);
#endif
static int TIFFWriteDirectoryTagSbyteArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, int8* value);
#if 0
static int TIFFWriteDirectoryTagSbytePerSample(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int8 value);
#endif
static int TIFFWriteDirectoryTagShort(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint16 value);
static int TIFFWriteDirectoryTagShortArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint16* value);
static int TIFFWriteDirectoryTagShortPerSample(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint16 value);
#ifdef notdef
static int TIFFWriteDirectoryTagSshort(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int16 value);
#endif
static int TIFFWriteDirectoryTagSshortArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, int16* value);
#if 0
static int TIFFWriteDirectoryTagSshortPerSample(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int16 value);
#endif
static int TIFFWriteDirectoryTagLong(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 value);
static int TIFFWriteDirectoryTagLongArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint32* value);
#if 0
static int TIFFWriteDirectoryTagLongPerSample(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 value);
#endif
#ifdef notdef
static int TIFFWriteDirectoryTagSlong(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int32 value);
#endif
static int TIFFWriteDirectoryTagSlongArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, int32* value);
#if 0
static int TIFFWriteDirectoryTagSlongPerSample(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int32 value);
#endif
#ifdef notdef
static int TIFFWriteDirectoryTagLong8(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint64 value);
#endif
static int TIFFWriteDirectoryTagLong8Array(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint64* value);
#ifdef notdef
static int TIFFWriteDirectoryTagSlong8(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int64 value);
#endif
static int TIFFWriteDirectoryTagSlong8Array(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, int64* value);
static int TIFFWriteDirectoryTagRational(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, double value);
static int TIFFWriteDirectoryTagRationalArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, float* value);
static int TIFFWriteDirectoryTagSrationalArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, float* value);
#ifdef notdef
static int TIFFWriteDirectoryTagFloat(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, float value);
#endif
static int TIFFWriteDirectoryTagFloatArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, float* value);
#if 0
static int TIFFWriteDirectoryTagFloatPerSample(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, float value);
#endif
#ifdef notdef
static int TIFFWriteDirectoryTagDouble(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, double value);
#endif
static int TIFFWriteDirectoryTagDoubleArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, double* value);
#if 0
static int TIFFWriteDirectoryTagDoublePerSample(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, double value);
#endif
static int TIFFWriteDirectoryTagIfdArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint32* value);
#ifdef notdef
static int TIFFWriteDirectoryTagIfd8Array(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint64* value);
#endif
static int TIFFWriteDirectoryTagShortLong(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 value);
static int TIFFWriteDirectoryTagLongLong8Array(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint64* value);
static int TIFFWriteDirectoryTagIfdIfd8Array(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint64* value);
#ifdef notdef
static int TIFFWriteDirectoryTagShortLongLong8Array(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint64* value);
#endif
static int TIFFWriteDirectoryTagColormap(TIFF* tif, uint32* ndir, TIFFDirEntry* dir);
static int TIFFWriteDirectoryTagTransferfunction(TIFF* tif, uint32* ndir, TIFFDirEntry* dir);
static int TIFFWriteDirectoryTagSubifd(TIFF* tif, uint32* ndir, TIFFDirEntry* dir);

static int TIFFWriteDirectoryTagCheckedAscii(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, char* value);
static int TIFFWriteDirectoryTagCheckedUndefinedArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint8* value);
#ifdef notdef
static int TIFFWriteDirectoryTagCheckedByte(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint8 value);
#endif
static int TIFFWriteDirectoryTagCheckedByteArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint8* value);
#ifdef notdef
static int TIFFWriteDirectoryTagCheckedSbyte(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int8 value);
#endif
static int TIFFWriteDirectoryTagCheckedSbyteArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, int8* value);
static int TIFFWriteDirectoryTagCheckedShort(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint16 value);
static int TIFFWriteDirectoryTagCheckedShortArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint16* value);
#ifdef notdef
static int TIFFWriteDirectoryTagCheckedSshort(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int16 value);
#endif
static int TIFFWriteDirectoryTagCheckedSshortArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, int16* value);
static int TIFFWriteDirectoryTagCheckedLong(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 value);
static int TIFFWriteDirectoryTagCheckedLongArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint32* value);
#ifdef notdef
static int TIFFWriteDirectoryTagCheckedSlong(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int32 value);
#endif
static int TIFFWriteDirectoryTagCheckedSlongArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, int32* value);
#ifdef notdef
static int TIFFWriteDirectoryTagCheckedLong8(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint64 value);
#endif
static int TIFFWriteDirectoryTagCheckedLong8Array(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint64* value);
#ifdef notdef
static int TIFFWriteDirectoryTagCheckedSlong8(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int64 value);
#endif
static int TIFFWriteDirectoryTagCheckedSlong8Array(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, int64* value);
static int TIFFWriteDirectoryTagCheckedRational(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, double value);
static int TIFFWriteDirectoryTagCheckedRationalArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, float* value);
static int TIFFWriteDirectoryTagCheckedSrationalArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, float* value);
#ifdef notdef
static int TIFFWriteDirectoryTagCheckedFloat(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, float value);
#endif
static int TIFFWriteDirectoryTagCheckedFloatArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, float* value);
#ifdef notdef
static int TIFFWriteDirectoryTagCheckedDouble(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, double value);
#endif
static int TIFFWriteDirectoryTagCheckedDoubleArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, double* value);
static int TIFFWriteDirectoryTagCheckedIfdArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint32* value);
static int TIFFWriteDirectoryTagCheckedIfd8Array(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint64* value);

static int TIFFWriteDirectoryTagData(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint16 datatype, uint32 count, uint32 datalength, void* data);

static int TIFFLinkDirectory(TIFF*);

/*
 * Write the contents of the current directory
 * to the specified file.  This routine doesn't
 * handle overwriting a directory with auxiliary
 * storage that's been changed.
 */
int
TIFFWriteDirectory(TIFF* tif)
{
	{int  ReplaceReturn = TIFFWriteDirectorySec(tif,TRUE,TRUE,NULL); SensorCall(); return ReplaceReturn;}
}

/*
 * Similar to TIFFWriteDirectory(), writes the directory out
 * but leaves all data structures in memory so that it can be
 * written again.  This will make a partially written TIFF file
 * readable before it is successfully completed/closed.
 */
int
TIFFCheckpointDirectory(TIFF* tif)
{
	SensorCall();int rc;
	/* Setup the strips arrays, if they haven't already been. */
	SensorCall();if (tif->tif_dir.td_stripoffset == NULL)
	    {/*1*/SensorCall();(void) TIFFSetupStrips(tif);/*2*/}
	SensorCall();rc = TIFFWriteDirectorySec(tif,TRUE,FALSE,NULL);
	(void) TIFFSetWriteOffset(tif, TIFFSeekFile(tif, 0, SEEK_END));
	{int  ReplaceReturn = rc; SensorCall(); return ReplaceReturn;}
}

int
TIFFWriteCustomDirectory(TIFF* tif, uint64* pdiroff)
{
	{int  ReplaceReturn = TIFFWriteDirectorySec(tif,FALSE,FALSE,pdiroff); SensorCall(); return ReplaceReturn;}
}

/*
 * Similar to TIFFWriteDirectory(), but if the directory has already
 * been written once, it is relocated to the end of the file, in case it
 * has changed in size.  Note that this will result in the loss of the
 * previously used directory space. 
 */ 
int
TIFFRewriteDirectory( TIFF *tif )
{
	SensorCall();static const char module[] = "TIFFRewriteDirectory";

	/* We don't need to do anything special if it hasn't been written. */
	SensorCall();if( tif->tif_diroff == 0 )
		{/*3*/{int  ReplaceReturn = TIFFWriteDirectory( tif ); SensorCall(); return ReplaceReturn;}/*4*/}

	/*
	 * Find and zero the pointer to this directory, so that TIFFLinkDirectory
	 * will cause it to be added after this directories current pre-link.
	 */

	SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
	{
		SensorCall();if (tif->tif_header.classic.tiff_diroff == tif->tif_diroff)
		{
			SensorCall();tif->tif_header.classic.tiff_diroff = 0;
			tif->tif_diroff = 0;

			TIFFSeekFile(tif,4,SEEK_SET);
			SensorCall();if (!WriteOK(tif, &(tif->tif_header.classic.tiff_diroff),4))
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata, tif->tif_name,
				    "Error updating TIFF header");
				{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
			}
		}
		else
		{
			SensorCall();uint32 nextdir;
			nextdir = tif->tif_header.classic.tiff_diroff;
			SensorCall();while(1) {
				SensorCall();uint16 dircount;
				uint32 nextnextdir;

				SensorCall();if (!SeekOK(tif, nextdir) ||
				    !ReadOK(tif, &dircount, 2)) {
					SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					     "Error fetching directory count");
					{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
				}
				SensorCall();if (tif->tif_flags & TIFF_SWAB)
					{/*5*/SensorCall();TIFFSwabShort(&dircount);/*6*/}
				SensorCall();(void) TIFFSeekFile(tif,
				    nextdir+2+dircount*12, SEEK_SET);
				SensorCall();if (!ReadOK(tif, &nextnextdir, 4)) {
					SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					     "Error fetching directory link");
					{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
				}
				SensorCall();if (tif->tif_flags & TIFF_SWAB)
					{/*7*/SensorCall();TIFFSwabLong(&nextnextdir);/*8*/}
				SensorCall();if (nextnextdir==tif->tif_diroff)
				{
					SensorCall();uint32 m;
					m=0;
					(void) TIFFSeekFile(tif,
					    nextdir+2+dircount*12, SEEK_SET);
					SensorCall();if (!WriteOK(tif, &m, 4)) {
						SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
						     "Error writing directory link");
						{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
					}
					SensorCall();tif->tif_diroff=0;
					SensorCall();break;
				}
				SensorCall();nextdir=nextnextdir;
			}
		}
	}
	else
	{
		SensorCall();if (tif->tif_header.big.tiff_diroff == tif->tif_diroff)
		{
			SensorCall();tif->tif_header.big.tiff_diroff = 0;
			tif->tif_diroff = 0;

			TIFFSeekFile(tif,8,SEEK_SET);
			SensorCall();if (!WriteOK(tif, &(tif->tif_header.big.tiff_diroff),8))
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata, tif->tif_name,
				    "Error updating TIFF header");
				{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
			}
		}
		else
		{
			SensorCall();uint64 nextdir;
			nextdir = tif->tif_header.big.tiff_diroff;
			SensorCall();while(1) {
				SensorCall();uint64 dircount64;
				uint16 dircount;
				uint64 nextnextdir;

				SensorCall();if (!SeekOK(tif, nextdir) ||
				    !ReadOK(tif, &dircount64, 8)) {
					SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					     "Error fetching directory count");
					{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
				}
				SensorCall();if (tif->tif_flags & TIFF_SWAB)
					{/*9*/SensorCall();TIFFSwabLong8(&dircount64);/*10*/}
				SensorCall();if (dircount64>0xFFFF)
				{
					SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					     "Sanity check on tag count failed, likely corrupt TIFF");
					{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
				}
				SensorCall();dircount=(uint16)dircount64;
				(void) TIFFSeekFile(tif,
				    nextdir+8+dircount*20, SEEK_SET);
				SensorCall();if (!ReadOK(tif, &nextnextdir, 8)) {
					SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					     "Error fetching directory link");
					{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
				}
				SensorCall();if (tif->tif_flags & TIFF_SWAB)
					{/*11*/SensorCall();TIFFSwabLong8(&nextnextdir);/*12*/}
				SensorCall();if (nextnextdir==tif->tif_diroff)
				{
					SensorCall();uint64 m;
					m=0;
					(void) TIFFSeekFile(tif,
					    nextdir+8+dircount*20, SEEK_SET);
					SensorCall();if (!WriteOK(tif, &m, 8)) {
						SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
						     "Error writing directory link");
						{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
					}
					SensorCall();tif->tif_diroff=0;
					SensorCall();break;
				}
				SensorCall();nextdir=nextnextdir;
			}
		}
	}

	/*
	 * Now use TIFFWriteDirectory() normally.
	 */

	{int  ReplaceReturn = TIFFWriteDirectory( tif ); SensorCall(); return ReplaceReturn;}
}

static int
TIFFWriteDirectorySec(TIFF* tif, int isimage, int imagedone, uint64* pdiroff)
{
	SensorCall();static const char module[] = "TIFFWriteDirectorySec";
	uint32 ndir;
	TIFFDirEntry* dir;
	uint32 dirsize;
	void* dirmem;
	uint32 m;
	SensorCall();if (tif->tif_mode == O_RDONLY)
		{/*73*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*74*/}

        SensorCall();_TIFFFillStriles( tif );
        
	/*
	 * Clear write state so that subsequent images with
	 * different characteristics get the right buffers
	 * setup for them.
	 */
	SensorCall();if (imagedone)
	{
		SensorCall();if (tif->tif_flags & TIFF_POSTENCODE)
		{
			SensorCall();tif->tif_flags &= ~TIFF_POSTENCODE;
			SensorCall();if (!(*tif->tif_postencode)(tif))
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata,module,
				    "Error post-encoding before directory write");
				{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
			}
		}
		SensorCall();(*tif->tif_close)(tif);       /* shutdown encoder */
		/*
		 * Flush any data that might have been written
		 * by the compression close+cleanup routines.  But
                 * be careful not to write stuff if we didn't add data
                 * in the previous steps as the "rawcc" data may well be
                 * a previously read tile/strip in mixed read/write mode.
		 */
		SensorCall();if (tif->tif_rawcc > 0 
		    && (tif->tif_flags & TIFF_BEENWRITING) != 0 )
		{
		    SensorCall();if( !TIFFFlushData1(tif) )
                    {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
			    "Error flushing data before directory write");
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
                    }
		}
		SensorCall();if ((tif->tif_flags & TIFF_MYBUFFER) && tif->tif_rawdata)
		{
			SensorCall();_TIFFfree(tif->tif_rawdata);
			tif->tif_rawdata = NULL;
			tif->tif_rawcc = 0;
			tif->tif_rawdatasize = 0;
                        tif->tif_rawdataoff = 0;
                        tif->tif_rawdataloaded = 0;
		}
		SensorCall();tif->tif_flags &= ~(TIFF_BEENWRITING|TIFF_BUFFERSETUP);
	}
	SensorCall();dir=NULL;
	dirmem=NULL;
	dirsize=0;
	SensorCall();while (1)
	{
		SensorCall();ndir=0;
		SensorCall();if (isimage)
		{
			SensorCall();if (TIFFFieldSet(tif,FIELD_IMAGEDIMENSIONS))
			{
				SensorCall();if (!TIFFWriteDirectoryTagShortLong(tif,&ndir,dir,TIFFTAG_IMAGEWIDTH,tif->tif_dir.td_imagewidth))
					{/*75*/SensorCall();goto bad;/*76*/}
				SensorCall();if (!TIFFWriteDirectoryTagShortLong(tif,&ndir,dir,TIFFTAG_IMAGELENGTH,tif->tif_dir.td_imagelength))
					{/*77*/SensorCall();goto bad;/*78*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_TILEDIMENSIONS))
			{
				SensorCall();if (!TIFFWriteDirectoryTagShortLong(tif,&ndir,dir,TIFFTAG_TILEWIDTH,tif->tif_dir.td_tilewidth))
					{/*79*/SensorCall();goto bad;/*80*/}
				SensorCall();if (!TIFFWriteDirectoryTagShortLong(tif,&ndir,dir,TIFFTAG_TILELENGTH,tif->tif_dir.td_tilelength))
					{/*81*/SensorCall();goto bad;/*82*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_RESOLUTION))
			{
				SensorCall();if (!TIFFWriteDirectoryTagRational(tif,&ndir,dir,TIFFTAG_XRESOLUTION,tif->tif_dir.td_xresolution))
					{/*83*/SensorCall();goto bad;/*84*/}
				SensorCall();if (!TIFFWriteDirectoryTagRational(tif,&ndir,dir,TIFFTAG_YRESOLUTION,tif->tif_dir.td_yresolution))
					{/*85*/SensorCall();goto bad;/*86*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_POSITION))
			{
				SensorCall();if (!TIFFWriteDirectoryTagRational(tif,&ndir,dir,TIFFTAG_XPOSITION,tif->tif_dir.td_xposition))
					{/*87*/SensorCall();goto bad;/*88*/}
				SensorCall();if (!TIFFWriteDirectoryTagRational(tif,&ndir,dir,TIFFTAG_YPOSITION,tif->tif_dir.td_yposition))
					{/*89*/SensorCall();goto bad;/*90*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_SUBFILETYPE))
			{
				SensorCall();if (!TIFFWriteDirectoryTagLong(tif,&ndir,dir,TIFFTAG_SUBFILETYPE,tif->tif_dir.td_subfiletype))
					{/*91*/SensorCall();goto bad;/*92*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_BITSPERSAMPLE))
			{
				SensorCall();if (!TIFFWriteDirectoryTagShortPerSample(tif,&ndir,dir,TIFFTAG_BITSPERSAMPLE,tif->tif_dir.td_bitspersample))
					{/*93*/SensorCall();goto bad;/*94*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_COMPRESSION))
			{
				SensorCall();if (!TIFFWriteDirectoryTagShort(tif,&ndir,dir,TIFFTAG_COMPRESSION,tif->tif_dir.td_compression))
					{/*95*/SensorCall();goto bad;/*96*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_PHOTOMETRIC))
			{
				SensorCall();if (!TIFFWriteDirectoryTagShort(tif,&ndir,dir,TIFFTAG_PHOTOMETRIC,tif->tif_dir.td_photometric))
					{/*97*/SensorCall();goto bad;/*98*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_THRESHHOLDING))
			{
				SensorCall();if (!TIFFWriteDirectoryTagShort(tif,&ndir,dir,TIFFTAG_THRESHHOLDING,tif->tif_dir.td_threshholding))
					{/*99*/SensorCall();goto bad;/*100*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_FILLORDER))
			{
				SensorCall();if (!TIFFWriteDirectoryTagShort(tif,&ndir,dir,TIFFTAG_FILLORDER,tif->tif_dir.td_fillorder))
					{/*101*/SensorCall();goto bad;/*102*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_ORIENTATION))
			{
				SensorCall();if (!TIFFWriteDirectoryTagShort(tif,&ndir,dir,TIFFTAG_ORIENTATION,tif->tif_dir.td_orientation))
					{/*103*/SensorCall();goto bad;/*104*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_SAMPLESPERPIXEL))
			{
				SensorCall();if (!TIFFWriteDirectoryTagShort(tif,&ndir,dir,TIFFTAG_SAMPLESPERPIXEL,tif->tif_dir.td_samplesperpixel))
					{/*105*/SensorCall();goto bad;/*106*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_ROWSPERSTRIP))
			{
				SensorCall();if (!TIFFWriteDirectoryTagShortLong(tif,&ndir,dir,TIFFTAG_ROWSPERSTRIP,tif->tif_dir.td_rowsperstrip))
					{/*107*/SensorCall();goto bad;/*108*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_MINSAMPLEVALUE))
			{
				SensorCall();if (!TIFFWriteDirectoryTagShortPerSample(tif,&ndir,dir,TIFFTAG_MINSAMPLEVALUE,tif->tif_dir.td_minsamplevalue))
					{/*109*/SensorCall();goto bad;/*110*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_MAXSAMPLEVALUE))
			{
				SensorCall();if (!TIFFWriteDirectoryTagShortPerSample(tif,&ndir,dir,TIFFTAG_MAXSAMPLEVALUE,tif->tif_dir.td_maxsamplevalue))
					{/*111*/SensorCall();goto bad;/*112*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_PLANARCONFIG))
			{
				SensorCall();if (!TIFFWriteDirectoryTagShort(tif,&ndir,dir,TIFFTAG_PLANARCONFIG,tif->tif_dir.td_planarconfig))
					{/*113*/SensorCall();goto bad;/*114*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_RESOLUTIONUNIT))
			{
				SensorCall();if (!TIFFWriteDirectoryTagShort(tif,&ndir,dir,TIFFTAG_RESOLUTIONUNIT,tif->tif_dir.td_resolutionunit))
					{/*115*/SensorCall();goto bad;/*116*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_PAGENUMBER))
			{
				SensorCall();if (!TIFFWriteDirectoryTagShortArray(tif,&ndir,dir,TIFFTAG_PAGENUMBER,2,&tif->tif_dir.td_pagenumber[0]))
					{/*117*/SensorCall();goto bad;/*118*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_STRIPBYTECOUNTS))
			{
				SensorCall();if (!isTiled(tif))
				{
					SensorCall();if (!TIFFWriteDirectoryTagLongLong8Array(tif,&ndir,dir,TIFFTAG_STRIPBYTECOUNTS,tif->tif_dir.td_nstrips,tif->tif_dir.td_stripbytecount))
						{/*119*/SensorCall();goto bad;/*120*/}
				}
				else
				{
					SensorCall();if (!TIFFWriteDirectoryTagLongLong8Array(tif,&ndir,dir,TIFFTAG_TILEBYTECOUNTS,tif->tif_dir.td_nstrips,tif->tif_dir.td_stripbytecount))
						{/*121*/SensorCall();goto bad;/*122*/}
				}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_STRIPOFFSETS))
			{
				SensorCall();if (!isTiled(tif))
				{
					/* td_stripoffset can be NULL even if td_nstrips == 1 due to OJPEG hack */
					SensorCall();if (tif->tif_dir.td_stripoffset)
					{
						SensorCall();if (!TIFFWriteDirectoryTagLongLong8Array(tif,&ndir,dir,TIFFTAG_STRIPOFFSETS,tif->tif_dir.td_nstrips,tif->tif_dir.td_stripoffset))
							{/*123*/SensorCall();goto bad;/*124*/}
					}
				}
				else
				{
					SensorCall();if (!TIFFWriteDirectoryTagLongLong8Array(tif,&ndir,dir,TIFFTAG_TILEOFFSETS,tif->tif_dir.td_nstrips,tif->tif_dir.td_stripoffset))
						{/*125*/SensorCall();goto bad;/*126*/}
				}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_COLORMAP))
			{
				SensorCall();if (!TIFFWriteDirectoryTagColormap(tif,&ndir,dir))
					{/*127*/SensorCall();goto bad;/*128*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_EXTRASAMPLES))
			{
				SensorCall();if (tif->tif_dir.td_extrasamples)
				{
					SensorCall();uint16 na;
					uint16* nb;
					TIFFGetFieldDefaulted(tif,TIFFTAG_EXTRASAMPLES,&na,&nb);
					SensorCall();if (!TIFFWriteDirectoryTagShortArray(tif,&ndir,dir,TIFFTAG_EXTRASAMPLES,na,nb))
						{/*129*/SensorCall();goto bad;/*130*/}
				}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_SAMPLEFORMAT))
			{
				SensorCall();if (!TIFFWriteDirectoryTagShortPerSample(tif,&ndir,dir,TIFFTAG_SAMPLEFORMAT,tif->tif_dir.td_sampleformat))
					{/*131*/SensorCall();goto bad;/*132*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_SMINSAMPLEVALUE))
			{
				SensorCall();if (!TIFFWriteDirectoryTagSampleformatArray(tif,&ndir,dir,TIFFTAG_SMINSAMPLEVALUE,tif->tif_dir.td_samplesperpixel,tif->tif_dir.td_sminsamplevalue))
					{/*133*/SensorCall();goto bad;/*134*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_SMAXSAMPLEVALUE))
			{
				SensorCall();if (!TIFFWriteDirectoryTagSampleformatArray(tif,&ndir,dir,TIFFTAG_SMAXSAMPLEVALUE,tif->tif_dir.td_samplesperpixel,tif->tif_dir.td_smaxsamplevalue))
					{/*135*/SensorCall();goto bad;/*136*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_IMAGEDEPTH))
			{
				SensorCall();if (!TIFFWriteDirectoryTagLong(tif,&ndir,dir,TIFFTAG_IMAGEDEPTH,tif->tif_dir.td_imagedepth))
					{/*137*/SensorCall();goto bad;/*138*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_TILEDEPTH))
			{
				SensorCall();if (!TIFFWriteDirectoryTagLong(tif,&ndir,dir,TIFFTAG_TILEDEPTH,tif->tif_dir.td_tiledepth))
					{/*139*/SensorCall();goto bad;/*140*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_HALFTONEHINTS))
			{
				SensorCall();if (!TIFFWriteDirectoryTagShortArray(tif,&ndir,dir,TIFFTAG_HALFTONEHINTS,2,&tif->tif_dir.td_halftonehints[0]))
					{/*141*/SensorCall();goto bad;/*142*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_YCBCRSUBSAMPLING))
			{
				SensorCall();if (!TIFFWriteDirectoryTagShortArray(tif,&ndir,dir,TIFFTAG_YCBCRSUBSAMPLING,2,&tif->tif_dir.td_ycbcrsubsampling[0]))
					{/*143*/SensorCall();goto bad;/*144*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_YCBCRPOSITIONING))
			{
				SensorCall();if (!TIFFWriteDirectoryTagShort(tif,&ndir,dir,TIFFTAG_YCBCRPOSITIONING,tif->tif_dir.td_ycbcrpositioning))
					{/*145*/SensorCall();goto bad;/*146*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_REFBLACKWHITE))
			{
				SensorCall();if (!TIFFWriteDirectoryTagRationalArray(tif,&ndir,dir,TIFFTAG_REFERENCEBLACKWHITE,6,tif->tif_dir.td_refblackwhite))
					{/*147*/SensorCall();goto bad;/*148*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_TRANSFERFUNCTION))
			{
				SensorCall();if (!TIFFWriteDirectoryTagTransferfunction(tif,&ndir,dir))
					{/*149*/SensorCall();goto bad;/*150*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_INKNAMES))
			{
				SensorCall();if (!TIFFWriteDirectoryTagAscii(tif,&ndir,dir,TIFFTAG_INKNAMES,tif->tif_dir.td_inknameslen,tif->tif_dir.td_inknames))
					{/*151*/SensorCall();goto bad;/*152*/}
			}
			SensorCall();if (TIFFFieldSet(tif,FIELD_SUBIFD))
			{
				SensorCall();if (!TIFFWriteDirectoryTagSubifd(tif,&ndir,dir))
					{/*153*/SensorCall();goto bad;/*154*/}
			}
			{
				SensorCall();uint32 n;
				SensorCall();for (n=0; n<tif->tif_nfields; n++) {
					SensorCall();const TIFFField* o;
					o = tif->tif_fields[n];
					SensorCall();if ((o->field_bit>=FIELD_CODEC)&&(TIFFFieldSet(tif,o->field_bit)))
					{
						SensorCall();switch (o->get_field_type)
						{
							case TIFF_SETGET_ASCII:
								{
									SensorCall();uint32 pa;
									char* pb;
									assert(o->field_type==TIFF_ASCII);
									assert(o->field_readcount==TIFF_VARIABLE);
									assert(o->field_passcount==0);
									TIFFGetField(tif,o->field_tag,&pb);
									pa=(uint32)(strlen(pb));
									SensorCall();if (!TIFFWriteDirectoryTagAscii(tif,&ndir,dir,o->field_tag,pa,pb))
										{/*155*/SensorCall();goto bad;/*156*/}
								}
								SensorCall();break;
							case TIFF_SETGET_UINT16:
								{
									SensorCall();uint16 p;
									assert(o->field_type==TIFF_SHORT);
									assert(o->field_readcount==1);
									assert(o->field_passcount==0);
									TIFFGetField(tif,o->field_tag,&p);
									SensorCall();if (!TIFFWriteDirectoryTagShort(tif,&ndir,dir,o->field_tag,p))
										{/*157*/SensorCall();goto bad;/*158*/}
								}
								SensorCall();break;
							case TIFF_SETGET_UINT32:
								{
									SensorCall();uint32 p;
									assert(o->field_type==TIFF_LONG);
									assert(o->field_readcount==1);
									assert(o->field_passcount==0);
									TIFFGetField(tif,o->field_tag,&p);
									SensorCall();if (!TIFFWriteDirectoryTagLong(tif,&ndir,dir,o->field_tag,p))
										{/*159*/SensorCall();goto bad;/*160*/}
								}
								SensorCall();break;
							case TIFF_SETGET_C32_UINT8:
								{
									SensorCall();uint32 pa;
									void* pb;
									assert(o->field_type==TIFF_UNDEFINED);
									assert(o->field_readcount==TIFF_VARIABLE2);
									assert(o->field_passcount==1);
									TIFFGetField(tif,o->field_tag,&pa,&pb);
									SensorCall();if (!TIFFWriteDirectoryTagUndefinedArray(tif,&ndir,dir,o->field_tag,pa,pb))
										{/*161*/SensorCall();goto bad;/*162*/}
								}
								SensorCall();break;
							default:
								assert(0);   /* we should never get here */
								SensorCall();break;
						}
					}
				}
			}
		}
		SensorCall();for (m=0; m<(uint32)(tif->tif_dir.td_customValueCount); m++)
		{
			SensorCall();switch (tif->tif_dir.td_customValues[m].info->field_type)
			{
				case TIFF_ASCII:
					SensorCall();if (!TIFFWriteDirectoryTagAscii(tif,&ndir,dir,tif->tif_dir.td_customValues[m].info->field_tag,tif->tif_dir.td_customValues[m].count,tif->tif_dir.td_customValues[m].value))
						{/*163*/SensorCall();goto bad;/*164*/}
					SensorCall();break;
				case TIFF_UNDEFINED:
					SensorCall();if (!TIFFWriteDirectoryTagUndefinedArray(tif,&ndir,dir,tif->tif_dir.td_customValues[m].info->field_tag,tif->tif_dir.td_customValues[m].count,tif->tif_dir.td_customValues[m].value))
						{/*165*/SensorCall();goto bad;/*166*/}
					SensorCall();break;
				case TIFF_BYTE:
					SensorCall();if (!TIFFWriteDirectoryTagByteArray(tif,&ndir,dir,tif->tif_dir.td_customValues[m].info->field_tag,tif->tif_dir.td_customValues[m].count,tif->tif_dir.td_customValues[m].value))
						{/*167*/SensorCall();goto bad;/*168*/}
					SensorCall();break;
				case TIFF_SBYTE:
					SensorCall();if (!TIFFWriteDirectoryTagSbyteArray(tif,&ndir,dir,tif->tif_dir.td_customValues[m].info->field_tag,tif->tif_dir.td_customValues[m].count,tif->tif_dir.td_customValues[m].value))
						{/*169*/SensorCall();goto bad;/*170*/}
					SensorCall();break;
				case TIFF_SHORT:
					SensorCall();if (!TIFFWriteDirectoryTagShortArray(tif,&ndir,dir,tif->tif_dir.td_customValues[m].info->field_tag,tif->tif_dir.td_customValues[m].count,tif->tif_dir.td_customValues[m].value))
						{/*171*/SensorCall();goto bad;/*172*/}
					SensorCall();break;
				case TIFF_SSHORT:
					SensorCall();if (!TIFFWriteDirectoryTagSshortArray(tif,&ndir,dir,tif->tif_dir.td_customValues[m].info->field_tag,tif->tif_dir.td_customValues[m].count,tif->tif_dir.td_customValues[m].value))
						{/*173*/SensorCall();goto bad;/*174*/}
					SensorCall();break;
				case TIFF_LONG:
					SensorCall();if (!TIFFWriteDirectoryTagLongArray(tif,&ndir,dir,tif->tif_dir.td_customValues[m].info->field_tag,tif->tif_dir.td_customValues[m].count,tif->tif_dir.td_customValues[m].value))
						{/*175*/SensorCall();goto bad;/*176*/}
					SensorCall();break;
				case TIFF_SLONG:
					SensorCall();if (!TIFFWriteDirectoryTagSlongArray(tif,&ndir,dir,tif->tif_dir.td_customValues[m].info->field_tag,tif->tif_dir.td_customValues[m].count,tif->tif_dir.td_customValues[m].value))
						{/*177*/SensorCall();goto bad;/*178*/}
					SensorCall();break;
				case TIFF_LONG8:
					SensorCall();if (!TIFFWriteDirectoryTagLong8Array(tif,&ndir,dir,tif->tif_dir.td_customValues[m].info->field_tag,tif->tif_dir.td_customValues[m].count,tif->tif_dir.td_customValues[m].value))
						{/*179*/SensorCall();goto bad;/*180*/}
					SensorCall();break;
				case TIFF_SLONG8:
					SensorCall();if (!TIFFWriteDirectoryTagSlong8Array(tif,&ndir,dir,tif->tif_dir.td_customValues[m].info->field_tag,tif->tif_dir.td_customValues[m].count,tif->tif_dir.td_customValues[m].value))
						{/*181*/SensorCall();goto bad;/*182*/}
					SensorCall();break;
				case TIFF_RATIONAL:
					SensorCall();if (!TIFFWriteDirectoryTagRationalArray(tif,&ndir,dir,tif->tif_dir.td_customValues[m].info->field_tag,tif->tif_dir.td_customValues[m].count,tif->tif_dir.td_customValues[m].value))
						{/*183*/SensorCall();goto bad;/*184*/}
					SensorCall();break;
				case TIFF_SRATIONAL:
					SensorCall();if (!TIFFWriteDirectoryTagSrationalArray(tif,&ndir,dir,tif->tif_dir.td_customValues[m].info->field_tag,tif->tif_dir.td_customValues[m].count,tif->tif_dir.td_customValues[m].value))
						{/*185*/SensorCall();goto bad;/*186*/}
					SensorCall();break;
				case TIFF_FLOAT:
					SensorCall();if (!TIFFWriteDirectoryTagFloatArray(tif,&ndir,dir,tif->tif_dir.td_customValues[m].info->field_tag,tif->tif_dir.td_customValues[m].count,tif->tif_dir.td_customValues[m].value))
						{/*187*/SensorCall();goto bad;/*188*/}
					SensorCall();break;
				case TIFF_DOUBLE:
					SensorCall();if (!TIFFWriteDirectoryTagDoubleArray(tif,&ndir,dir,tif->tif_dir.td_customValues[m].info->field_tag,tif->tif_dir.td_customValues[m].count,tif->tif_dir.td_customValues[m].value))
						{/*189*/SensorCall();goto bad;/*190*/}
					SensorCall();break;
				case TIFF_IFD:
					SensorCall();if (!TIFFWriteDirectoryTagIfdArray(tif,&ndir,dir,tif->tif_dir.td_customValues[m].info->field_tag,tif->tif_dir.td_customValues[m].count,tif->tif_dir.td_customValues[m].value))
						{/*191*/SensorCall();goto bad;/*192*/}
					SensorCall();break;
				case TIFF_IFD8:
					SensorCall();if (!TIFFWriteDirectoryTagIfdIfd8Array(tif,&ndir,dir,tif->tif_dir.td_customValues[m].info->field_tag,tif->tif_dir.td_customValues[m].count,tif->tif_dir.td_customValues[m].value))
						{/*193*/SensorCall();goto bad;/*194*/}
					SensorCall();break;
				default:
					assert(0);   /* we should never get here */
					SensorCall();break;
			}
		}
		SensorCall();if (dir!=NULL)
			{/*195*/SensorCall();break;/*196*/}
		SensorCall();dir=_TIFFmalloc(ndir*sizeof(TIFFDirEntry));
		SensorCall();if (dir==NULL)
		{
			SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
			SensorCall();goto bad;
		}
		SensorCall();if (isimage)
		{
			SensorCall();if ((tif->tif_diroff==0)&&(!TIFFLinkDirectory(tif)))
				{/*197*/SensorCall();goto bad;/*198*/}
		}
		else
			{/*199*/SensorCall();tif->tif_diroff=(TIFFSeekFile(tif,0,SEEK_END)+1)&(~1);/*200*/}
		SensorCall();if (pdiroff!=NULL)
			{/*201*/SensorCall();*pdiroff=tif->tif_diroff;/*202*/}
		SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
			{/*203*/SensorCall();dirsize=2+ndir*12+4;/*204*/}
		else
			{/*205*/SensorCall();dirsize=8+ndir*20+8;/*206*/}
		SensorCall();tif->tif_dataoff=tif->tif_diroff+dirsize;
		SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
			{/*207*/SensorCall();tif->tif_dataoff=(uint32)tif->tif_dataoff;/*208*/}
		SensorCall();if ((tif->tif_dataoff<tif->tif_diroff)||(tif->tif_dataoff<(uint64)dirsize))
		{
			SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Maximum TIFF file size exceeded");
			SensorCall();goto bad;
		}
		SensorCall();if (tif->tif_dataoff&1)
			{/*209*/SensorCall();tif->tif_dataoff++;/*210*/}
		SensorCall();if (isimage)
			{/*211*/SensorCall();tif->tif_curdir++;/*212*/}
	}
	SensorCall();if (isimage)
	{
		SensorCall();if (TIFFFieldSet(tif,FIELD_SUBIFD)&&(tif->tif_subifdoff==0))
		{
			SensorCall();uint32 na;
			TIFFDirEntry* nb;
			SensorCall();for (na=0, nb=dir; ; na++, nb++)
			{
				assert(na<ndir);
				SensorCall();if (nb->tdir_tag==TIFFTAG_SUBIFD)
					{/*213*/SensorCall();break;/*214*/}
			}
			SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
				{/*215*/SensorCall();tif->tif_subifdoff=tif->tif_diroff+2+na*12+8;/*216*/}
			else
				{/*217*/SensorCall();tif->tif_subifdoff=tif->tif_diroff+8+na*20+12;/*218*/}
		}
	}
	SensorCall();dirmem=_TIFFmalloc(dirsize);
	SensorCall();if (dirmem==NULL)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
		SensorCall();goto bad;
	}
	SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
	{
		SensorCall();uint8* n;
		uint32 nTmp;
		TIFFDirEntry* o;
		n=dirmem;
		*(uint16*)n=ndir;
		SensorCall();if (tif->tif_flags&TIFF_SWAB)
			{/*219*/SensorCall();TIFFSwabShort((uint16*)n);/*220*/}
		SensorCall();n+=2;
		o=dir;
		SensorCall();for (m=0; m<ndir; m++)
		{
			SensorCall();*(uint16*)n=o->tdir_tag;
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*221*/SensorCall();TIFFSwabShort((uint16*)n);/*222*/}
			SensorCall();n+=2;
			*(uint16*)n=o->tdir_type;
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*223*/SensorCall();TIFFSwabShort((uint16*)n);/*224*/}
			SensorCall();n+=2;
			nTmp = (uint32)o->tdir_count;
			_TIFFmemcpy(n,&nTmp,4);
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*225*/SensorCall();TIFFSwabLong((uint32*)n);/*226*/}
			SensorCall();n+=4;
			/* This is correct. The data has been */
			/* swabbed previously in TIFFWriteDirectoryTagData */
			_TIFFmemcpy(n,&o->tdir_offset,4);
			n+=4;
			o++;
		}
		SensorCall();nTmp = (uint32)tif->tif_nextdiroff;
		SensorCall();if (tif->tif_flags&TIFF_SWAB)
			{/*227*/SensorCall();TIFFSwabLong(&nTmp);/*228*/}
		SensorCall();_TIFFmemcpy(n,&nTmp,4);
	}
	else
	{
		SensorCall();uint8* n;
		TIFFDirEntry* o;
		n=dirmem;
		*(uint64*)n=ndir;
		SensorCall();if (tif->tif_flags&TIFF_SWAB)
			{/*229*/SensorCall();TIFFSwabLong8((uint64*)n);/*230*/}
		SensorCall();n+=8;
		o=dir;
		SensorCall();for (m=0; m<ndir; m++)
		{
			SensorCall();*(uint16*)n=o->tdir_tag;
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*231*/SensorCall();TIFFSwabShort((uint16*)n);/*232*/}
			SensorCall();n+=2;
			*(uint16*)n=o->tdir_type;
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*233*/SensorCall();TIFFSwabShort((uint16*)n);/*234*/}
			SensorCall();n+=2;
			_TIFFmemcpy(n,&o->tdir_count,8);
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*235*/SensorCall();TIFFSwabLong8((uint64*)n);/*236*/}
			SensorCall();n+=8;
			_TIFFmemcpy(n,&o->tdir_offset,8);
			n+=8;
			o++;
		}
		SensorCall();_TIFFmemcpy(n,&tif->tif_nextdiroff,8);
		SensorCall();if (tif->tif_flags&TIFF_SWAB)
			{/*237*/SensorCall();TIFFSwabLong8((uint64*)n);/*238*/}
	}
	SensorCall();_TIFFfree(dir);
	dir=NULL;
	SensorCall();if (!SeekOK(tif,tif->tif_diroff))
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"IO error writing directory");
		SensorCall();goto bad;
	}
	SensorCall();if (!WriteOK(tif,dirmem,(tmsize_t)dirsize))
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"IO error writing directory");
		SensorCall();goto bad;
	}
	SensorCall();_TIFFfree(dirmem);
	SensorCall();if (imagedone)
	{
		SensorCall();TIFFFreeDirectory(tif);
		tif->tif_flags &= ~TIFF_DIRTYDIRECT;
		tif->tif_flags &= ~TIFF_DIRTYSTRIP;
		(*tif->tif_cleanup)(tif);
		/*
		* Reset directory-related state for subsequent
		* directories.
		*/
		TIFFCreateDirectory(tif);
	}
	SensorCall();return(1);
bad:
	if (dir!=NULL)
		{/*239*/_TIFFfree(dir);/*240*/}
	if (dirmem!=NULL)
		{/*241*/_TIFFfree(dirmem);/*242*/}
	return(0);
}

static int
TIFFWriteDirectoryTagSampleformatArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, double* value)
{
	SensorCall();static const char module[] = "TIFFWriteDirectoryTagSampleformatArray";
	void* conv;
	uint32 i;
	int ok;
	conv = _TIFFmalloc(count*sizeof(double));
	SensorCall();if (conv == NULL)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "Out of memory");
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}

	SensorCall();switch (tif->tif_dir.td_sampleformat)
	{
		case SAMPLEFORMAT_IEEEFP:
			SensorCall();if (tif->tif_dir.td_bitspersample<=32)
			{
				SensorCall();for (i = 0; i < count; ++i)
					{/*243*/SensorCall();((float*)conv)[i] = (float)value[i];/*244*/}
				SensorCall();ok = TIFFWriteDirectoryTagFloatArray(tif,ndir,dir,tag,count,(float*)conv);
			}
			else
			{
				SensorCall();ok = TIFFWriteDirectoryTagDoubleArray(tif,ndir,dir,tag,count,value);
			}
			SensorCall();break;
		case SAMPLEFORMAT_INT:
			SensorCall();if (tif->tif_dir.td_bitspersample<=8)
			{
				SensorCall();for (i = 0; i < count; ++i)
					{/*245*/SensorCall();((int8*)conv)[i] = (int8)value[i];/*246*/}
				SensorCall();ok = TIFFWriteDirectoryTagSbyteArray(tif,ndir,dir,tag,count,(int8*)conv);
			}
			else {/*247*/SensorCall();if (tif->tif_dir.td_bitspersample<=16)
			{
				SensorCall();for (i = 0; i < count; ++i)
					{/*249*/SensorCall();((int16*)conv)[i] = (int16)value[i];/*250*/}
				SensorCall();ok = TIFFWriteDirectoryTagSshortArray(tif,ndir,dir,tag,count,(int16*)conv);
			}
			else
			{
				SensorCall();for (i = 0; i < count; ++i)
					{/*251*/SensorCall();((int32*)conv)[i] = (int32)value[i];/*252*/}
				SensorCall();ok = TIFFWriteDirectoryTagSlongArray(tif,ndir,dir,tag,count,(int32*)conv);
			;/*248*/}}
			SensorCall();break;
		case SAMPLEFORMAT_UINT:
			SensorCall();if (tif->tif_dir.td_bitspersample<=8)
			{
				SensorCall();for (i = 0; i < count; ++i)
					{/*253*/SensorCall();((uint8*)conv)[i] = (uint8)value[i];/*254*/}
				SensorCall();ok = TIFFWriteDirectoryTagByteArray(tif,ndir,dir,tag,count,(uint8*)conv);
			}
			else {/*255*/SensorCall();if (tif->tif_dir.td_bitspersample<=16)
			{
				SensorCall();for (i = 0; i < count; ++i)
					{/*257*/SensorCall();((uint16*)conv)[i] = (uint16)value[i];/*258*/}
				SensorCall();ok = TIFFWriteDirectoryTagShortArray(tif,ndir,dir,tag,count,(uint16*)conv);
			}
			else
			{
				SensorCall();for (i = 0; i < count; ++i)
					{/*259*/SensorCall();((uint32*)conv)[i] = (uint32)value[i];/*260*/}
				SensorCall();ok = TIFFWriteDirectoryTagLongArray(tif,ndir,dir,tag,count,(uint32*)conv);
			;/*256*/}}
			SensorCall();break;
		default:
			SensorCall();ok = 0;
	}

	SensorCall();_TIFFfree(conv);
	{int  ReplaceReturn = (ok); SensorCall(); return ReplaceReturn;}
}

#if 0
static int
TIFFWriteDirectoryTagSampleformatPerSample(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, double value)
{
	switch (tif->tif_dir.td_sampleformat)
	{
		case SAMPLEFORMAT_IEEEFP:
			if (tif->tif_dir.td_bitspersample<=32)
				return(TIFFWriteDirectoryTagFloatPerSample(tif,ndir,dir,tag,(float)value));
			else
				return(TIFFWriteDirectoryTagDoublePerSample(tif,ndir,dir,tag,value));
		case SAMPLEFORMAT_INT:
			if (tif->tif_dir.td_bitspersample<=8)
				return(TIFFWriteDirectoryTagSbytePerSample(tif,ndir,dir,tag,(int8)value));
			else if (tif->tif_dir.td_bitspersample<=16)
				return(TIFFWriteDirectoryTagSshortPerSample(tif,ndir,dir,tag,(int16)value));
			else
				return(TIFFWriteDirectoryTagSlongPerSample(tif,ndir,dir,tag,(int32)value));
		case SAMPLEFORMAT_UINT:
			if (tif->tif_dir.td_bitspersample<=8)
				return(TIFFWriteDirectoryTagBytePerSample(tif,ndir,dir,tag,(uint8)value));
			else if (tif->tif_dir.td_bitspersample<=16)
				return(TIFFWriteDirectoryTagShortPerSample(tif,ndir,dir,tag,(uint16)value));
			else
				return(TIFFWriteDirectoryTagLongPerSample(tif,ndir,dir,tag,(uint32)value));
		default:
			return(1);
	}
}
#endif

static int
TIFFWriteDirectoryTagAscii(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, char* value)
{
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();return(TIFFWriteDirectoryTagCheckedAscii(tif,ndir,dir,tag,count,value));
}

static int
TIFFWriteDirectoryTagUndefinedArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint8* value)
{
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();return(TIFFWriteDirectoryTagCheckedUndefinedArray(tif,ndir,dir,tag,count,value));
}

#ifdef notdef
static int
TIFFWriteDirectoryTagByte(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint8 value)
{
	if (dir==NULL)
	{
		(*ndir)++;
		return(1);
	}
	return(TIFFWriteDirectoryTagCheckedByte(tif,ndir,dir,tag,value));
}
#endif

static int
TIFFWriteDirectoryTagByteArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint8* value)
{
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();return(TIFFWriteDirectoryTagCheckedByteArray(tif,ndir,dir,tag,count,value));
}

#if 0
static int
TIFFWriteDirectoryTagBytePerSample(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint8 value)
{
	static const char module[] = "TIFFWriteDirectoryTagBytePerSample";
	uint8* m;
	uint8* na;
	uint16 nb;
	int o;
	if (dir==NULL)
	{
		(*ndir)++;
		return(1);
	}
	m=_TIFFmalloc(tif->tif_dir.td_samplesperpixel*sizeof(uint8));
	if (m==NULL)
	{
		TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
		return(0);
	}
	for (na=m, nb=0; nb<tif->tif_dir.td_samplesperpixel; na++, nb++)
		*na=value;
	o=TIFFWriteDirectoryTagCheckedByteArray(tif,ndir,dir,tag,tif->tif_dir.td_samplesperpixel,m);
	_TIFFfree(m);
	return(o);
}
#endif

#ifdef notdef
static int
TIFFWriteDirectoryTagSbyte(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int8 value)
{
	if (dir==NULL)
	{
		(*ndir)++;
		return(1);
	}
	return(TIFFWriteDirectoryTagCheckedSbyte(tif,ndir,dir,tag,value));
}
#endif

static int
TIFFWriteDirectoryTagSbyteArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, int8* value)
{
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();return(TIFFWriteDirectoryTagCheckedSbyteArray(tif,ndir,dir,tag,count,value));
}

#if 0
static int
TIFFWriteDirectoryTagSbytePerSample(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int8 value)
{
	static const char module[] = "TIFFWriteDirectoryTagSbytePerSample";
	int8* m;
	int8* na;
	uint16 nb;
	int o;
	if (dir==NULL)
	{
		(*ndir)++;
		return(1);
	}
	m=_TIFFmalloc(tif->tif_dir.td_samplesperpixel*sizeof(int8));
	if (m==NULL)
	{
		TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
		return(0);
	}
	for (na=m, nb=0; nb<tif->tif_dir.td_samplesperpixel; na++, nb++)
		*na=value;
	o=TIFFWriteDirectoryTagCheckedSbyteArray(tif,ndir,dir,tag,tif->tif_dir.td_samplesperpixel,m);
	_TIFFfree(m);
	return(o);
}
#endif

static int
TIFFWriteDirectoryTagShort(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint16 value)
{
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();return(TIFFWriteDirectoryTagCheckedShort(tif,ndir,dir,tag,value));
}

static int
TIFFWriteDirectoryTagShortArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint16* value)
{
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();return(TIFFWriteDirectoryTagCheckedShortArray(tif,ndir,dir,tag,count,value));
}

static int
TIFFWriteDirectoryTagShortPerSample(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint16 value)
{
	SensorCall();static const char module[] = "TIFFWriteDirectoryTagShortPerSample";
	uint16* m;
	uint16* na;
	uint16 nb;
	int o;
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();m=_TIFFmalloc(tif->tif_dir.td_samplesperpixel*sizeof(uint16));
	SensorCall();if (m==NULL)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
		SensorCall();return(0);
	}
	SensorCall();for (na=m, nb=0; nb<tif->tif_dir.td_samplesperpixel; na++, nb++)
		{/*261*/SensorCall();*na=value;/*262*/}
	SensorCall();o=TIFFWriteDirectoryTagCheckedShortArray(tif,ndir,dir,tag,tif->tif_dir.td_samplesperpixel,m);
	_TIFFfree(m);
	SensorCall();return(o);
}

#ifdef notdef
static int
TIFFWriteDirectoryTagSshort(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int16 value)
{
	if (dir==NULL)
	{
		(*ndir)++;
		return(1);
	}
	return(TIFFWriteDirectoryTagCheckedSshort(tif,ndir,dir,tag,value));
}
#endif

static int
TIFFWriteDirectoryTagSshortArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, int16* value)
{
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();return(TIFFWriteDirectoryTagCheckedSshortArray(tif,ndir,dir,tag,count,value));
}

#if 0
static int
TIFFWriteDirectoryTagSshortPerSample(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int16 value)
{
	static const char module[] = "TIFFWriteDirectoryTagSshortPerSample";
	int16* m;
	int16* na;
	uint16 nb;
	int o;
	if (dir==NULL)
	{
		(*ndir)++;
		return(1);
	}
	m=_TIFFmalloc(tif->tif_dir.td_samplesperpixel*sizeof(int16));
	if (m==NULL)
	{
		TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
		return(0);
	}
	for (na=m, nb=0; nb<tif->tif_dir.td_samplesperpixel; na++, nb++)
		*na=value;
	o=TIFFWriteDirectoryTagCheckedSshortArray(tif,ndir,dir,tag,tif->tif_dir.td_samplesperpixel,m);
	_TIFFfree(m);
	return(o);
}
#endif

static int
TIFFWriteDirectoryTagLong(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 value)
{
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();return(TIFFWriteDirectoryTagCheckedLong(tif,ndir,dir,tag,value));
}

static int
TIFFWriteDirectoryTagLongArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint32* value)
{
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();return(TIFFWriteDirectoryTagCheckedLongArray(tif,ndir,dir,tag,count,value));
}

#if 0
static int
TIFFWriteDirectoryTagLongPerSample(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 value)
{
	static const char module[] = "TIFFWriteDirectoryTagLongPerSample";
	uint32* m;
	uint32* na;
	uint16 nb;
	int o;
	if (dir==NULL)
	{
		(*ndir)++;
		return(1);
	}
	m=_TIFFmalloc(tif->tif_dir.td_samplesperpixel*sizeof(uint32));
	if (m==NULL)
	{
		TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
		return(0);
	}
	for (na=m, nb=0; nb<tif->tif_dir.td_samplesperpixel; na++, nb++)
		*na=value;
	o=TIFFWriteDirectoryTagCheckedLongArray(tif,ndir,dir,tag,tif->tif_dir.td_samplesperpixel,m);
	_TIFFfree(m);
	return(o);
}
#endif

#ifdef notdef
static int
TIFFWriteDirectoryTagSlong(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int32 value)
{
	if (dir==NULL)
	{
		(*ndir)++;
		return(1);
	}
	return(TIFFWriteDirectoryTagCheckedSlong(tif,ndir,dir,tag,value));
}
#endif

static int
TIFFWriteDirectoryTagSlongArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, int32* value)
{
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();return(TIFFWriteDirectoryTagCheckedSlongArray(tif,ndir,dir,tag,count,value));
}

#if 0
static int
TIFFWriteDirectoryTagSlongPerSample(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int32 value)
{
	static const char module[] = "TIFFWriteDirectoryTagSlongPerSample";
	int32* m;
	int32* na;
	uint16 nb;
	int o;
	if (dir==NULL)
	{
		(*ndir)++;
		return(1);
	}
	m=_TIFFmalloc(tif->tif_dir.td_samplesperpixel*sizeof(int32));
	if (m==NULL)
	{
		TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
		return(0);
	}
	for (na=m, nb=0; nb<tif->tif_dir.td_samplesperpixel; na++, nb++)
		*na=value;
	o=TIFFWriteDirectoryTagCheckedSlongArray(tif,ndir,dir,tag,tif->tif_dir.td_samplesperpixel,m);
	_TIFFfree(m);
	return(o);
}
#endif

#ifdef notdef
static int
TIFFWriteDirectoryTagLong8(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint64 value)
{
	if (dir==NULL)
	{
		(*ndir)++;
		return(1);
	}
	return(TIFFWriteDirectoryTagCheckedLong8(tif,ndir,dir,tag,value));
}
#endif

static int
TIFFWriteDirectoryTagLong8Array(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint64* value)
{
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();return(TIFFWriteDirectoryTagCheckedLong8Array(tif,ndir,dir,tag,count,value));
}

#ifdef notdef
static int
TIFFWriteDirectoryTagSlong8(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int64 value)
{
	if (dir==NULL)
	{
		(*ndir)++;
		return(1);
	}
	return(TIFFWriteDirectoryTagCheckedSlong8(tif,ndir,dir,tag,value));
}
#endif

static int
TIFFWriteDirectoryTagSlong8Array(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, int64* value)
{
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();return(TIFFWriteDirectoryTagCheckedSlong8Array(tif,ndir,dir,tag,count,value));
}

static int
TIFFWriteDirectoryTagRational(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, double value)
{
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();return(TIFFWriteDirectoryTagCheckedRational(tif,ndir,dir,tag,value));
}

static int
TIFFWriteDirectoryTagRationalArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, float* value)
{
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();return(TIFFWriteDirectoryTagCheckedRationalArray(tif,ndir,dir,tag,count,value));
}

static int
TIFFWriteDirectoryTagSrationalArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, float* value)
{
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();return(TIFFWriteDirectoryTagCheckedSrationalArray(tif,ndir,dir,tag,count,value));
}

#ifdef notdef
static int TIFFWriteDirectoryTagFloat(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, float value)
{
	if (dir==NULL)
	{
		(*ndir)++;
		return(1);
	}
	return(TIFFWriteDirectoryTagCheckedFloat(tif,ndir,dir,tag,value));
}
#endif

static int TIFFWriteDirectoryTagFloatArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, float* value)
{
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();return(TIFFWriteDirectoryTagCheckedFloatArray(tif,ndir,dir,tag,count,value));
}

#if 0
static int TIFFWriteDirectoryTagFloatPerSample(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, float value)
{
	static const char module[] = "TIFFWriteDirectoryTagFloatPerSample";
	float* m;
	float* na;
	uint16 nb;
	int o;
	if (dir==NULL)
	{
		(*ndir)++;
		return(1);
	}
	m=_TIFFmalloc(tif->tif_dir.td_samplesperpixel*sizeof(float));
	if (m==NULL)
	{
		TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
		return(0);
	}
	for (na=m, nb=0; nb<tif->tif_dir.td_samplesperpixel; na++, nb++)
		*na=value;
	o=TIFFWriteDirectoryTagCheckedFloatArray(tif,ndir,dir,tag,tif->tif_dir.td_samplesperpixel,m);
	_TIFFfree(m);
	return(o);
}
#endif

#ifdef notdef
static int TIFFWriteDirectoryTagDouble(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, double value)
{
	if (dir==NULL)
	{
		(*ndir)++;
		return(1);
	}
	return(TIFFWriteDirectoryTagCheckedDouble(tif,ndir,dir,tag,value));
}
#endif

static int TIFFWriteDirectoryTagDoubleArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, double* value)
{
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();return(TIFFWriteDirectoryTagCheckedDoubleArray(tif,ndir,dir,tag,count,value));
}

#if 0
static int TIFFWriteDirectoryTagDoublePerSample(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, double value)
{
	static const char module[] = "TIFFWriteDirectoryTagDoublePerSample";
	double* m;
	double* na;
	uint16 nb;
	int o;
	if (dir==NULL)
	{
		(*ndir)++;
		return(1);
	}
	m=_TIFFmalloc(tif->tif_dir.td_samplesperpixel*sizeof(double));
	if (m==NULL)
	{
		TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
		return(0);
	}
	for (na=m, nb=0; nb<tif->tif_dir.td_samplesperpixel; na++, nb++)
		*na=value;
	o=TIFFWriteDirectoryTagCheckedDoubleArray(tif,ndir,dir,tag,tif->tif_dir.td_samplesperpixel,m);
	_TIFFfree(m);
	return(o);
}
#endif

static int
TIFFWriteDirectoryTagIfdArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint32* value)
{
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();return(TIFFWriteDirectoryTagCheckedIfdArray(tif,ndir,dir,tag,count,value));
}

#ifdef notdef
static int
TIFFWriteDirectoryTagIfd8Array(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint64* value)
{
	if (dir==NULL)
	{
		(*ndir)++;
		return(1);
	}
	return(TIFFWriteDirectoryTagCheckedIfd8Array(tif,ndir,dir,tag,count,value));
}
#endif

static int
TIFFWriteDirectoryTagShortLong(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 value)
{
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();if (value<=0xFFFF)
		{/*263*/SensorCall();return(TIFFWriteDirectoryTagCheckedShort(tif,ndir,dir,tag,(uint16)value));/*264*/}
	else
		{/*265*/SensorCall();return(TIFFWriteDirectoryTagCheckedLong(tif,ndir,dir,tag,value));/*266*/}
SensorCall();}

/************************************************************************/
/*                TIFFWriteDirectoryTagLongLong8Array()                 */
/*                                                                      */
/*      Write out LONG8 array as LONG8 for BigTIFF or LONG for          */
/*      Classic TIFF with some checking.                                */
/************************************************************************/

static int
TIFFWriteDirectoryTagLongLong8Array(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint64* value)
{
    SensorCall();static const char module[] = "TIFFWriteDirectoryTagLongLong8Array";
    uint64* ma;
    uint32 mb;
    uint32* p;
    uint32* q;
    int o;

    /* is this just a counting pass? */
    SensorCall();if (dir==NULL)
    {
        SensorCall();(*ndir)++;
        SensorCall();return(1);
    }

    /* We always write LONG8 for BigTIFF, no checking needed. */
    SensorCall();if( tif->tif_flags&TIFF_BIGTIFF )
        {/*267*/{int  ReplaceReturn = TIFFWriteDirectoryTagCheckedLong8Array(tif,ndir,dir,
                                                      tag,count,value); SensorCall(); return ReplaceReturn;}/*268*/}

    /*
    ** For classic tiff we want to verify everything is in range for LONG
    ** and convert to long format.
    */

    SensorCall();p = _TIFFmalloc(count*sizeof(uint32));
    SensorCall();if (p==NULL)
    {
        SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
        SensorCall();return(0);
    }

    SensorCall();for (q=p, ma=value, mb=0; mb<count; ma++, mb++, q++)
    {
        SensorCall();if (*ma>0xFFFFFFFF)
        {
            SensorCall();TIFFErrorExt(tif->tif_clientdata,module,
                         "Attempt to write value larger than 0xFFFFFFFF in Classic TIFF file.");
            _TIFFfree(p);
            SensorCall();return(0);
        }
        SensorCall();*q= (uint32)(*ma);
    }

    SensorCall();o=TIFFWriteDirectoryTagCheckedLongArray(tif,ndir,dir,tag,count,p);
    _TIFFfree(p);

    SensorCall();return(o);
}

/************************************************************************/
/*                 TIFFWriteDirectoryTagIfdIfd8Array()                  */
/*                                                                      */
/*      Write either IFD8 or IFD array depending on file type.          */
/************************************************************************/

static int
TIFFWriteDirectoryTagIfdIfd8Array(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint64* value)
{
    SensorCall();static const char module[] = "TIFFWriteDirectoryTagIfdIfd8Array";
    uint64* ma;
    uint32 mb;
    uint32* p;
    uint32* q;
    int o;

    /* is this just a counting pass? */
    SensorCall();if (dir==NULL)
    {
        SensorCall();(*ndir)++;
        SensorCall();return(1);
    }

    /* We always write IFD8 for BigTIFF, no checking needed. */
    SensorCall();if( tif->tif_flags&TIFF_BIGTIFF )
        {/*269*/{int  ReplaceReturn = TIFFWriteDirectoryTagCheckedIfd8Array(tif,ndir,dir,
                                                     tag,count,value); SensorCall(); return ReplaceReturn;}/*270*/}

    /*
    ** For classic tiff we want to verify everything is in range for IFD
    ** and convert to long format.
    */

    SensorCall();p = _TIFFmalloc(count*sizeof(uint32));
    SensorCall();if (p==NULL)
    {
        SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
        SensorCall();return(0);
    }

    SensorCall();for (q=p, ma=value, mb=0; mb<count; ma++, mb++, q++)
    {
        SensorCall();if (*ma>0xFFFFFFFF)
        {
            SensorCall();TIFFErrorExt(tif->tif_clientdata,module,
                         "Attempt to write value larger than 0xFFFFFFFF in Classic TIFF file.");
            _TIFFfree(p);
            SensorCall();return(0);
        }
        SensorCall();*q= (uint32)(*ma);
    }

    SensorCall();o=TIFFWriteDirectoryTagCheckedIfdArray(tif,ndir,dir,tag,count,p);
    _TIFFfree(p);

    SensorCall();return(o);
}

#ifdef notdef
static int
TIFFWriteDirectoryTagShortLongLong8Array(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint64* value)
{
	static const char module[] = "TIFFWriteDirectoryTagShortLongLong8Array";
	uint64* ma;
	uint32 mb;
	uint8 n;
	int o;
	if (dir==NULL)
	{
		(*ndir)++;
		return(1);
	}
	n=0;
	for (ma=value, mb=0; mb<count; ma++, mb++)
	{
		if ((n==0)&&(*ma>0xFFFF))
			n=1;
		if ((n==1)&&(*ma>0xFFFFFFFF))
		{
			n=2;
			break;
		}
	}
	if (n==0)
	{
		uint16* p;
		uint16* q;
		p=_TIFFmalloc(count*sizeof(uint16));
		if (p==NULL)
		{
			TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
			return(0);
		}
		for (ma=value, mb=0, q=p; mb<count; ma++, mb++, q++)
			*q=(uint16)(*ma);
		o=TIFFWriteDirectoryTagCheckedShortArray(tif,ndir,dir,tag,count,p);
		_TIFFfree(p);
	}
	else if (n==1)
	{
		uint32* p;
		uint32* q;
		p=_TIFFmalloc(count*sizeof(uint32));
		if (p==NULL)
		{
			TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
			return(0);
		}
		for (ma=value, mb=0, q=p; mb<count; ma++, mb++, q++)
			*q=(uint32)(*ma);
		o=TIFFWriteDirectoryTagCheckedLongArray(tif,ndir,dir,tag,count,p);
		_TIFFfree(p);
	}
	else
	{
		assert(n==2);
		o=TIFFWriteDirectoryTagCheckedLong8Array(tif,ndir,dir,tag,count,value);
	}
	return(o);
}
#endif
static int
TIFFWriteDirectoryTagColormap(TIFF* tif, uint32* ndir, TIFFDirEntry* dir)
{
	SensorCall();static const char module[] = "TIFFWriteDirectoryTagColormap";
	uint32 m;
	uint16* n;
	int o;
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();m=(1<<tif->tif_dir.td_bitspersample);
	n=_TIFFmalloc(3*m*sizeof(uint16));
	SensorCall();if (n==NULL)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
		SensorCall();return(0);
	}
	SensorCall();_TIFFmemcpy(&n[0],tif->tif_dir.td_colormap[0],m*sizeof(uint16));
	_TIFFmemcpy(&n[m],tif->tif_dir.td_colormap[1],m*sizeof(uint16));
	_TIFFmemcpy(&n[2*m],tif->tif_dir.td_colormap[2],m*sizeof(uint16));
	o=TIFFWriteDirectoryTagCheckedShortArray(tif,ndir,dir,TIFFTAG_COLORMAP,3*m,n);
	_TIFFfree(n);
	SensorCall();return(o);
}

static int
TIFFWriteDirectoryTagTransferfunction(TIFF* tif, uint32* ndir, TIFFDirEntry* dir)
{
	SensorCall();static const char module[] = "TIFFWriteDirectoryTagTransferfunction";
	uint32 m;
	uint16 n;
	uint16* o;
	int p;
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();m=(1<<tif->tif_dir.td_bitspersample);
	n=tif->tif_dir.td_samplesperpixel-tif->tif_dir.td_extrasamples;
	/*
	 * Check if the table can be written as a single column,
	 * or if it must be written as 3 columns.  Note that we
	 * write a 3-column tag if there are 2 samples/pixel and
	 * a single column of data won't suffice--hmm.
	 */
	SensorCall();if (n>3)
		{/*271*/SensorCall();n=3;/*272*/}
	SensorCall();if (n==3)
	{
		SensorCall();if (!_TIFFmemcmp(tif->tif_dir.td_transferfunction[0],tif->tif_dir.td_transferfunction[2],m*sizeof(uint16)))
			{/*273*/SensorCall();n=2;/*274*/}
	}
	SensorCall();if (n==2)
	{
		SensorCall();if (!_TIFFmemcmp(tif->tif_dir.td_transferfunction[0],tif->tif_dir.td_transferfunction[1],m*sizeof(uint16)))
			{/*275*/SensorCall();n=1;/*276*/}
	}
	SensorCall();if (n==0)
		{/*277*/SensorCall();n=1;/*278*/}
	SensorCall();o=_TIFFmalloc(n*m*sizeof(uint16));
	SensorCall();if (o==NULL)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
		SensorCall();return(0);
	}
	SensorCall();_TIFFmemcpy(&o[0],tif->tif_dir.td_transferfunction[0],m*sizeof(uint16));
	SensorCall();if (n>1)
		{/*279*/SensorCall();_TIFFmemcpy(&o[m],tif->tif_dir.td_transferfunction[1],m*sizeof(uint16));/*280*/}
	SensorCall();if (n>2)
		{/*281*/SensorCall();_TIFFmemcpy(&o[2*m],tif->tif_dir.td_transferfunction[2],m*sizeof(uint16));/*282*/}
	SensorCall();p=TIFFWriteDirectoryTagCheckedShortArray(tif,ndir,dir,TIFFTAG_TRANSFERFUNCTION,n*m,o);
	_TIFFfree(o);
	SensorCall();return(p);
}

static int
TIFFWriteDirectoryTagSubifd(TIFF* tif, uint32* ndir, TIFFDirEntry* dir)
{
	SensorCall();static const char module[] = "TIFFWriteDirectoryTagSubifd";
	uint64 m;
	int n;
	SensorCall();if (tif->tif_dir.td_nsubifd==0)
		{/*283*/SensorCall();return(1);/*284*/}
	SensorCall();if (dir==NULL)
	{
		SensorCall();(*ndir)++;
		SensorCall();return(1);
	}
	SensorCall();m=tif->tif_dataoff;
	SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
	{
		SensorCall();uint32* o;
		uint64* pa;
		uint32* pb;
		uint16 p;
		o=_TIFFmalloc(tif->tif_dir.td_nsubifd*sizeof(uint32));
		SensorCall();if (o==NULL)
		{
			SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
			SensorCall();return(0);
		}
		SensorCall();pa=tif->tif_dir.td_subifd;
		pb=o;
		SensorCall();for (p=0; p < tif->tif_dir.td_nsubifd; p++)
		{
                        assert(pa != 0);
			assert(*pa <= 0xFFFFFFFFUL);
			SensorCall();*pb++=(uint32)(*pa++);
		}
		SensorCall();n=TIFFWriteDirectoryTagCheckedIfdArray(tif,ndir,dir,TIFFTAG_SUBIFD,tif->tif_dir.td_nsubifd,o);
		_TIFFfree(o);
	}
	else
		{/*285*/SensorCall();n=TIFFWriteDirectoryTagCheckedIfd8Array(tif,ndir,dir,TIFFTAG_SUBIFD,tif->tif_dir.td_nsubifd,tif->tif_dir.td_subifd);/*286*/}
	SensorCall();if (!n)
		{/*287*/SensorCall();return(0);/*288*/}
	/*
	 * Total hack: if this directory includes a SubIFD
	 * tag then force the next <n> directories to be
	 * written as ``sub directories'' of this one.  This
	 * is used to write things like thumbnails and
	 * image masks that one wants to keep out of the
	 * normal directory linkage access mechanism.
	 */
	SensorCall();tif->tif_flags|=TIFF_INSUBIFD;
	tif->tif_nsubifd=tif->tif_dir.td_nsubifd;
	SensorCall();if (tif->tif_dir.td_nsubifd==1)
		{/*289*/SensorCall();tif->tif_subifdoff=0;/*290*/}
	else
		{/*291*/SensorCall();tif->tif_subifdoff=m;/*292*/}
	SensorCall();return(1);
}

static int
TIFFWriteDirectoryTagCheckedAscii(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, char* value)
{
SensorCall();	assert(sizeof(char)==1);
	SensorCall();return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_ASCII,count,count,value));
}

static int
TIFFWriteDirectoryTagCheckedUndefinedArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint8* value)
{
SensorCall();	assert(sizeof(uint8)==1);
	SensorCall();return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_UNDEFINED,count,count,value));
}

#ifdef notdef
static int
TIFFWriteDirectoryTagCheckedByte(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint8 value)
{
	assert(sizeof(uint8)==1);
	return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_BYTE,1,1,&value));
}
#endif

static int
TIFFWriteDirectoryTagCheckedByteArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint8* value)
{
SensorCall();	assert(sizeof(uint8)==1);
	SensorCall();return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_BYTE,count,count,value));
}

#ifdef notdef
static int
TIFFWriteDirectoryTagCheckedSbyte(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int8 value)
{
	assert(sizeof(int8)==1);
	return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_SBYTE,1,1,&value));
}
#endif

static int
TIFFWriteDirectoryTagCheckedSbyteArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, int8* value)
{
SensorCall();	assert(sizeof(int8)==1);
	SensorCall();return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_SBYTE,count,count,value));
}

static int
TIFFWriteDirectoryTagCheckedShort(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint16 value)
{
	SensorCall();uint16 m;
	assert(sizeof(uint16)==2);
	m=value;
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*293*/SensorCall();TIFFSwabShort(&m);/*294*/}
	SensorCall();return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_SHORT,1,2,&m));
}

static int
TIFFWriteDirectoryTagCheckedShortArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint16* value)
{
	assert(count<0x80000000);
	assert(sizeof(uint16)==2);
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*295*/SensorCall();TIFFSwabArrayOfShort(value,count);/*296*/}
	SensorCall();return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_SHORT,count,count*2,value));
}

#ifdef notdef
static int
TIFFWriteDirectoryTagCheckedSshort(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int16 value)
{
	int16 m;
	assert(sizeof(int16)==2);
	m=value;
	if (tif->tif_flags&TIFF_SWAB)
		TIFFSwabShort((uint16*)(&m));
	return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_SSHORT,1,2,&m));
}
#endif

static int
TIFFWriteDirectoryTagCheckedSshortArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, int16* value)
{
	assert(count<0x80000000);
	assert(sizeof(int16)==2);
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*297*/SensorCall();TIFFSwabArrayOfShort((uint16*)value,count);/*298*/}
	SensorCall();return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_SSHORT,count,count*2,value));
}

static int
TIFFWriteDirectoryTagCheckedLong(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 value)
{
	SensorCall();uint32 m;
	assert(sizeof(uint32)==4);
	m=value;
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*299*/SensorCall();TIFFSwabLong(&m);/*300*/}
	SensorCall();return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_LONG,1,4,&m));
}

static int
TIFFWriteDirectoryTagCheckedLongArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint32* value)
{
	assert(count<0x40000000);
	assert(sizeof(uint32)==4);
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*301*/SensorCall();TIFFSwabArrayOfLong(value,count);/*302*/}
	SensorCall();return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_LONG,count,count*4,value));
}

#ifdef notdef
static int
TIFFWriteDirectoryTagCheckedSlong(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int32 value)
{
	int32 m;
	assert(sizeof(int32)==4);
	m=value;
	if (tif->tif_flags&TIFF_SWAB)
		TIFFSwabLong((uint32*)(&m));
	return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_SLONG,1,4,&m));
}
#endif

static int
TIFFWriteDirectoryTagCheckedSlongArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, int32* value)
{
	assert(count<0x40000000);
	assert(sizeof(int32)==4);
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*303*/SensorCall();TIFFSwabArrayOfLong((uint32*)value,count);/*304*/}
	SensorCall();return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_SLONG,count,count*4,value));
}

#ifdef notdef
static int
TIFFWriteDirectoryTagCheckedLong8(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint64 value)
{
	uint64 m;
	assert(sizeof(uint64)==8);
	assert(tif->tif_flags&TIFF_BIGTIFF);
	m=value;
	if (tif->tif_flags&TIFF_SWAB)
		TIFFSwabLong8(&m);
	return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_LONG8,1,8,&m));
}
#endif

static int
TIFFWriteDirectoryTagCheckedLong8Array(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint64* value)
{
	assert(count<0x20000000);
	assert(sizeof(uint64)==8);
	assert(tif->tif_flags&TIFF_BIGTIFF);
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*305*/SensorCall();TIFFSwabArrayOfLong8(value,count);/*306*/}
	SensorCall();return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_LONG8,count,count*8,value));
}

#ifdef notdef
static int
TIFFWriteDirectoryTagCheckedSlong8(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, int64 value)
{
	int64 m;
	assert(sizeof(int64)==8);
	assert(tif->tif_flags&TIFF_BIGTIFF);
	m=value;
	if (tif->tif_flags&TIFF_SWAB)
		TIFFSwabLong8((uint64*)(&m));
	return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_SLONG8,1,8,&m));
}
#endif

static int
TIFFWriteDirectoryTagCheckedSlong8Array(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, int64* value)
{
	assert(count<0x20000000);
	assert(sizeof(int64)==8);
	assert(tif->tif_flags&TIFF_BIGTIFF);
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*307*/SensorCall();TIFFSwabArrayOfLong8((uint64*)value,count);/*308*/}
	SensorCall();return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_SLONG8,count,count*8,value));
}

static int
TIFFWriteDirectoryTagCheckedRational(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, double value)
{
	SensorCall();uint32 m[2];
	assert(value>=0.0);
	assert(sizeof(uint32)==4);
	SensorCall();if (value<=0.0)
	{
		SensorCall();m[0]=0;
		m[1]=1;
	}
	else {/*309*/SensorCall();if (value==(double)(uint32)value)
	{
		SensorCall();m[0]=(uint32)value;
		m[1]=1;
	}
	else {/*311*/SensorCall();if (value<1.0)
	{
		SensorCall();m[0]=(uint32)(value*0xFFFFFFFF);
		m[1]=0xFFFFFFFF;
	}
	else
	{
		SensorCall();m[0]=0xFFFFFFFF;
		m[1]=(uint32)(0xFFFFFFFF/value);
	;/*312*/}/*310*/}}
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
	{
		SensorCall();TIFFSwabLong(&m[0]);
		TIFFSwabLong(&m[1]);
	}
	SensorCall();return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_RATIONAL,1,8,&m[0]));
}

static int
TIFFWriteDirectoryTagCheckedRationalArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, float* value)
{
	SensorCall();static const char module[] = "TIFFWriteDirectoryTagCheckedRationalArray";
	uint32* m;
	float* na;
	uint32* nb;
	uint32 nc;
	int o;
	assert(sizeof(uint32)==4);
	m=_TIFFmalloc(count*2*sizeof(uint32));
	SensorCall();if (m==NULL)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
		SensorCall();return(0);
	}
	SensorCall();for (na=value, nb=m, nc=0; nc<count; na++, nb+=2, nc++)
	{
		SensorCall();if (*na<=0.0)
		{
			SensorCall();nb[0]=0;
			nb[1]=1;
		}
		else {/*313*/SensorCall();if (*na==(float)(uint32)(*na))
		{
			SensorCall();nb[0]=(uint32)(*na);
			nb[1]=1;
		}
		else {/*315*/SensorCall();if (*na<1.0)
		{
			SensorCall();nb[0]=(uint32)((*na)*0xFFFFFFFF);
			nb[1]=0xFFFFFFFF;
		}
		else
		{
			SensorCall();nb[0]=0xFFFFFFFF;
			nb[1]=(uint32)(0xFFFFFFFF/(*na));
		;/*316*/}/*314*/}}
	}
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*317*/SensorCall();TIFFSwabArrayOfLong(m,count*2);/*318*/}
	SensorCall();o=TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_RATIONAL,count,count*8,&m[0]);
	_TIFFfree(m);
	SensorCall();return(o);
}

static int
TIFFWriteDirectoryTagCheckedSrationalArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, float* value)
{
	SensorCall();static const char module[] = "TIFFWriteDirectoryTagCheckedSrationalArray";
	int32* m;
	float* na;
	int32* nb;
	uint32 nc;
	int o;
	assert(sizeof(int32)==4);
	m=_TIFFmalloc(count*2*sizeof(int32));
	SensorCall();if (m==NULL)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
		SensorCall();return(0);
	}
	SensorCall();for (na=value, nb=m, nc=0; nc<count; na++, nb+=2, nc++)
	{
		SensorCall();if (*na<0.0)
		{
			SensorCall();if (*na==(int32)(*na))
			{
				SensorCall();nb[0]=(int32)(*na);
				nb[1]=1;
			}
			else {/*319*/SensorCall();if (*na>-1.0)
			{
				SensorCall();nb[0]=-(int32)((-*na)*0x7FFFFFFF);
				nb[1]=0x7FFFFFFF;
			}
			else
			{
				SensorCall();nb[0]=-0x7FFFFFFF;
				nb[1]=(int32)(0x7FFFFFFF/(-*na));
			;/*320*/}}
		}
		else
		{
			SensorCall();if (*na==(int32)(*na))
			{
				SensorCall();nb[0]=(int32)(*na);
				nb[1]=1;
			}
			else {/*321*/SensorCall();if (*na<1.0)
			{
				SensorCall();nb[0]=(int32)((*na)*0x7FFFFFFF);
				nb[1]=0x7FFFFFFF;
			}
			else
			{
				SensorCall();nb[0]=0x7FFFFFFF;
				nb[1]=(int32)(0x7FFFFFFF/(*na));
			;/*322*/}}
		}
	}
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*323*/SensorCall();TIFFSwabArrayOfLong((uint32*)m,count*2);/*324*/}
	SensorCall();o=TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_SRATIONAL,count,count*8,&m[0]);
	_TIFFfree(m);
	SensorCall();return(o);
}

#ifdef notdef
static int
TIFFWriteDirectoryTagCheckedFloat(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, float value)
{
	float m;
	assert(sizeof(float)==4);
	m=value;
	TIFFCvtNativeToIEEEFloat(tif,1,&m);
	if (tif->tif_flags&TIFF_SWAB)
		TIFFSwabFloat(&m);
	return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_FLOAT,1,4,&m));
}
#endif

static int
TIFFWriteDirectoryTagCheckedFloatArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, float* value)
{
	assert(count<0x40000000);
	assert(sizeof(float)==4);
	TIFFCvtNativeToIEEEFloat(tif,count,&value);
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*325*/SensorCall();TIFFSwabArrayOfFloat(value,count);/*326*/}
	SensorCall();return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_FLOAT,count,count*4,value));
}

#ifdef notdef
static int
TIFFWriteDirectoryTagCheckedDouble(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, double value)
{
	double m;
	assert(sizeof(double)==8);
	m=value;
	TIFFCvtNativeToIEEEDouble(tif,1,&m);
	if (tif->tif_flags&TIFF_SWAB)
		TIFFSwabDouble(&m);
	return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_DOUBLE,1,8,&m));
}
#endif

static int
TIFFWriteDirectoryTagCheckedDoubleArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, double* value)
{
	assert(count<0x20000000);
	assert(sizeof(double)==8);
	TIFFCvtNativeToIEEEDouble(tif,count,&value);
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*327*/SensorCall();TIFFSwabArrayOfDouble(value,count);/*328*/}
	SensorCall();return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_DOUBLE,count,count*8,value));
}

static int
TIFFWriteDirectoryTagCheckedIfdArray(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint32* value)
{
	assert(count<0x40000000);
	assert(sizeof(uint32)==4);
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*329*/SensorCall();TIFFSwabArrayOfLong(value,count);/*330*/}
	SensorCall();return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_IFD,count,count*4,value));
}

static int
TIFFWriteDirectoryTagCheckedIfd8Array(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint32 count, uint64* value)
{
	assert(count<0x20000000);
	assert(sizeof(uint64)==8);
	assert(tif->tif_flags&TIFF_BIGTIFF);
	SensorCall();if (tif->tif_flags&TIFF_SWAB)
		{/*331*/SensorCall();TIFFSwabArrayOfLong8(value,count);/*332*/}
	SensorCall();return(TIFFWriteDirectoryTagData(tif,ndir,dir,tag,TIFF_IFD8,count,count*8,value));
}

static int
TIFFWriteDirectoryTagData(TIFF* tif, uint32* ndir, TIFFDirEntry* dir, uint16 tag, uint16 datatype, uint32 count, uint32 datalength, void* data)
{
	SensorCall();static const char module[] = "TIFFWriteDirectoryTagData";
	uint32 m;
	m=0;
	SensorCall();while (m<(*ndir))
	{
		assert(dir[m].tdir_tag!=tag);
		SensorCall();if (dir[m].tdir_tag>tag)
			{/*333*/SensorCall();break;/*334*/}
		SensorCall();m++;
	}
	SensorCall();if (m<(*ndir))
	{
		SensorCall();uint32 n;
		SensorCall();for (n=*ndir; n>m; n--)
			{/*335*/SensorCall();dir[n]=dir[n-1];/*336*/}
	}
	SensorCall();dir[m].tdir_tag=tag;
	dir[m].tdir_type=datatype;
	dir[m].tdir_count=count;
	dir[m].tdir_offset.toff_long8 = 0;
	SensorCall();if (datalength<=((tif->tif_flags&TIFF_BIGTIFF)?0x8U:0x4U))
		{/*337*/SensorCall();_TIFFmemcpy(&dir[m].tdir_offset,data,datalength);/*338*/}
	else
	{
		SensorCall();uint64 na,nb;
		na=tif->tif_dataoff;
		nb=na+datalength;
		SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
			{/*339*/SensorCall();nb=(uint32)nb;/*340*/}
		SensorCall();if ((nb<na)||(nb<datalength))
		{
			SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Maximum TIFF file size exceeded");
			SensorCall();return(0);
		}
		SensorCall();if (!SeekOK(tif,na))
		{
			SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"IO error writing tag data");
			SensorCall();return(0);
		}
		assert(datalength<0x80000000UL);
		SensorCall();if (!WriteOK(tif,data,(tmsize_t)datalength))
		{
			SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"IO error writing tag data");
			SensorCall();return(0);
		}
		SensorCall();tif->tif_dataoff=nb;
		SensorCall();if (tif->tif_dataoff&1)
			{/*341*/SensorCall();tif->tif_dataoff++;/*342*/}
		SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
		{
			SensorCall();uint32 o;
			o=(uint32)na;
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*343*/SensorCall();TIFFSwabLong(&o);/*344*/}
			SensorCall();_TIFFmemcpy(&dir[m].tdir_offset,&o,4);
		}
		else
		{
			SensorCall();dir[m].tdir_offset.toff_long8 = na;
			SensorCall();if (tif->tif_flags&TIFF_SWAB)
				{/*345*/SensorCall();TIFFSwabLong8(&dir[m].tdir_offset.toff_long8);/*346*/}
		}
	}
	SensorCall();(*ndir)++;
	SensorCall();return(1);
}

/*
 * Link the current directory into the directory chain for the file.
 */
static int
TIFFLinkDirectory(TIFF* tif)
{
	SensorCall();static const char module[] = "TIFFLinkDirectory";

	tif->tif_diroff = (TIFFSeekFile(tif,0,SEEK_END)+1) &~ 1;

	/*
	 * Handle SubIFDs
	 */
	SensorCall();if (tif->tif_flags & TIFF_INSUBIFD)
	{
		SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
		{
			SensorCall();uint32 m;
			m = (uint32)tif->tif_diroff;
			SensorCall();if (tif->tif_flags & TIFF_SWAB)
				{/*347*/SensorCall();TIFFSwabLong(&m);/*348*/}
			SensorCall();(void) TIFFSeekFile(tif, tif->tif_subifdoff, SEEK_SET);
			SensorCall();if (!WriteOK(tif, &m, 4)) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				     "Error writing SubIFD directory link");
				{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
			}
			/*
			 * Advance to the next SubIFD or, if this is
			 * the last one configured, revert back to the
			 * normal directory linkage.
			 */
			SensorCall();if (--tif->tif_nsubifd)
				{/*349*/SensorCall();tif->tif_subifdoff += 4;/*350*/}
			else
				tif->tif_flags &= ~TIFF_INSUBIFD;
			{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		}
		else
		{
			SensorCall();uint64 m;
			m = tif->tif_diroff;
			SensorCall();if (tif->tif_flags & TIFF_SWAB)
				{/*351*/SensorCall();TIFFSwabLong8(&m);/*352*/}
			SensorCall();(void) TIFFSeekFile(tif, tif->tif_subifdoff, SEEK_SET);
			SensorCall();if (!WriteOK(tif, &m, 8)) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
				     "Error writing SubIFD directory link");
				{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
			}
			/*
			 * Advance to the next SubIFD or, if this is
			 * the last one configured, revert back to the
			 * normal directory linkage.
			 */
			SensorCall();if (--tif->tif_nsubifd)
				{/*353*/SensorCall();tif->tif_subifdoff += 8;/*354*/}
			else
				tif->tif_flags &= ~TIFF_INSUBIFD;
			{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		}
	}

	SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
	{
		SensorCall();uint32 m;
		uint32 nextdir;
		m = (uint32)(tif->tif_diroff);
		SensorCall();if (tif->tif_flags & TIFF_SWAB)
			{/*355*/SensorCall();TIFFSwabLong(&m);/*356*/}
		SensorCall();if (tif->tif_header.classic.tiff_diroff == 0) {
			/*
			 * First directory, overwrite offset in header.
			 */
			SensorCall();tif->tif_header.classic.tiff_diroff = (uint32) tif->tif_diroff;
			(void) TIFFSeekFile(tif,4, SEEK_SET);
			SensorCall();if (!WriteOK(tif, &m, 4)) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, tif->tif_name,
					     "Error writing TIFF header");
				{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
			}
			{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		}
		/*
		 * Not the first directory, search to the last and append.
		 */
		SensorCall();nextdir = tif->tif_header.classic.tiff_diroff;
		SensorCall();while(1) {
			SensorCall();uint16 dircount;
			uint32 nextnextdir;

			SensorCall();if (!SeekOK(tif, nextdir) ||
			    !ReadOK(tif, &dircount, 2)) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					     "Error fetching directory count");
				{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
			}
			SensorCall();if (tif->tif_flags & TIFF_SWAB)
				{/*357*/SensorCall();TIFFSwabShort(&dircount);/*358*/}
			SensorCall();(void) TIFFSeekFile(tif,
			    nextdir+2+dircount*12, SEEK_SET);
			SensorCall();if (!ReadOK(tif, &nextnextdir, 4)) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					     "Error fetching directory link");
				{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
			}
			SensorCall();if (tif->tif_flags & TIFF_SWAB)
				{/*359*/SensorCall();TIFFSwabLong(&nextnextdir);/*360*/}
			SensorCall();if (nextnextdir==0)
			{
				SensorCall();(void) TIFFSeekFile(tif,
				    nextdir+2+dircount*12, SEEK_SET);
				SensorCall();if (!WriteOK(tif, &m, 4)) {
					SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					     "Error writing directory link");
					{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
				}
				SensorCall();break;
			}
			SensorCall();nextdir=nextnextdir;
		}
	}
	else
	{
		SensorCall();uint64 m;
		uint64 nextdir;
		m = tif->tif_diroff;
		SensorCall();if (tif->tif_flags & TIFF_SWAB)
			{/*361*/SensorCall();TIFFSwabLong8(&m);/*362*/}
		SensorCall();if (tif->tif_header.big.tiff_diroff == 0) {
			/*
			 * First directory, overwrite offset in header.
			 */
			SensorCall();tif->tif_header.big.tiff_diroff = tif->tif_diroff;
			(void) TIFFSeekFile(tif,8, SEEK_SET);
			SensorCall();if (!WriteOK(tif, &m, 8)) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, tif->tif_name,
					     "Error writing TIFF header");
				{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
			}
			{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		}
		/*
		 * Not the first directory, search to the last and append.
		 */
		SensorCall();nextdir = tif->tif_header.big.tiff_diroff;
		SensorCall();while(1) {
			SensorCall();uint64 dircount64;
			uint16 dircount;
			uint64 nextnextdir;

			SensorCall();if (!SeekOK(tif, nextdir) ||
			    !ReadOK(tif, &dircount64, 8)) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					     "Error fetching directory count");
				{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
			}
			SensorCall();if (tif->tif_flags & TIFF_SWAB)
				{/*363*/SensorCall();TIFFSwabLong8(&dircount64);/*364*/}
			SensorCall();if (dircount64>0xFFFF)
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					     "Sanity check on tag count failed, likely corrupt TIFF");
				{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
			}
			SensorCall();dircount=(uint16)dircount64;
			(void) TIFFSeekFile(tif,
			    nextdir+8+dircount*20, SEEK_SET);
			SensorCall();if (!ReadOK(tif, &nextnextdir, 8)) {
				SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					     "Error fetching directory link");
				{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
			}
			SensorCall();if (tif->tif_flags & TIFF_SWAB)
				{/*365*/SensorCall();TIFFSwabLong8(&nextnextdir);/*366*/}
			SensorCall();if (nextnextdir==0)
			{
				SensorCall();(void) TIFFSeekFile(tif,
				    nextdir+8+dircount*20, SEEK_SET);
				SensorCall();if (!WriteOK(tif, &m, 8)) {
					SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
					     "Error writing directory link");
					{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
				}
				SensorCall();break;
			}
			SensorCall();nextdir=nextnextdir;
		}
	}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

/************************************************************************/
/*                          TIFFRewriteField()                          */
/*                                                                      */
/*      Rewrite a field in the directory on disk without regard to      */
/*      updating the TIFF directory structure in memory.  Currently     */
/*      only supported for field that already exist in the on-disk      */
/*      directory.  Mainly used for updating stripoffset /              */
/*      stripbytecount values after the directory is already on         */
/*      disk.                                                           */
/*                                                                      */
/*      Returns zero on failure, and one on success.                    */
/************************************************************************/

int
_TIFFRewriteField(TIFF* tif, uint16 tag, TIFFDataType in_datatype, 
                  tmsize_t count, void* data)
{
    SensorCall();static const char module[] = "TIFFResetField";
    /* const TIFFField* fip = NULL; */
    uint16 dircount;
    tmsize_t dirsize;
    uint8 direntry_raw[20];
    uint16 entry_tag = 0;
    uint16 entry_type = 0;
    uint64 entry_count = 0;
    uint64 entry_offset = 0;
    int    value_in_entry = 0;
    uint64 read_offset;
    uint8 *buf_to_write = NULL;
    TIFFDataType datatype;

/* -------------------------------------------------------------------- */
/*      Find field definition.                                          */
/* -------------------------------------------------------------------- */
    /*fip =*/ TIFFFindField(tif, tag, TIFF_ANY);

/* -------------------------------------------------------------------- */
/*      Do some checking this is a straight forward case.               */
/* -------------------------------------------------------------------- */
    SensorCall();if( isMapped(tif) )
    {
        SensorCall();TIFFErrorExt( tif->tif_clientdata, module, 
                      "Memory mapped files not currently supported for this operation." );
        {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
    }

    SensorCall();if( tif->tif_diroff == 0 )
    {
        SensorCall();TIFFErrorExt( tif->tif_clientdata, module, 
                      "Attempt to reset field on directory not already on disk." );
        {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
    }

/* -------------------------------------------------------------------- */
/*      Read the directory entry count.                                 */
/* -------------------------------------------------------------------- */
    SensorCall();if (!SeekOK(tif, tif->tif_diroff)) {
        SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
                     "%s: Seek error accessing TIFF directory",
                     tif->tif_name);
        {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
    }

    SensorCall();read_offset = tif->tif_diroff;

    SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
    {
        SensorCall();if (!ReadOK(tif, &dircount, sizeof (uint16))) {
            SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
                         "%s: Can not read TIFF directory count",
                         tif->tif_name);
            {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
        }
        SensorCall();if (tif->tif_flags & TIFF_SWAB)
            {/*13*/SensorCall();TIFFSwabShort(&dircount);/*14*/}
        SensorCall();dirsize = 12;
        read_offset += 2;
    } else {
        SensorCall();uint64 dircount64;
        SensorCall();if (!ReadOK(tif, &dircount64, sizeof (uint64))) {
            SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
                         "%s: Can not read TIFF directory count",
                         tif->tif_name);
            {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
        }
        SensorCall();if (tif->tif_flags & TIFF_SWAB)
            {/*15*/SensorCall();TIFFSwabLong8(&dircount64);/*16*/}
        SensorCall();dircount = (uint16)dircount64;
        dirsize = 20;
        read_offset += 8;
    }

/* -------------------------------------------------------------------- */
/*      Read through directory to find target tag.                      */
/* -------------------------------------------------------------------- */
    SensorCall();while( dircount > 0 )
    {
        SensorCall();if (!ReadOK(tif, direntry_raw, dirsize)) {
            SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
                         "%s: Can not read TIFF directory entry.",
                         tif->tif_name);
            {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
        }

        SensorCall();memcpy( &entry_tag, direntry_raw + 0, sizeof(uint16) );
        SensorCall();if (tif->tif_flags&TIFF_SWAB)
            {/*17*/SensorCall();TIFFSwabShort( &entry_tag );/*18*/}

        SensorCall();if( entry_tag == tag )
            {/*19*/SensorCall();break;/*20*/}

        SensorCall();read_offset += dirsize;
    }

    SensorCall();if( entry_tag != tag )
    {
        SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
                     "%s: Could not find tag %d.",
                     tif->tif_name, tag );
        {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
    }

/* -------------------------------------------------------------------- */
/*      Extract the type, count and offset for this entry.              */
/* -------------------------------------------------------------------- */
    SensorCall();memcpy( &entry_type, direntry_raw + 2, sizeof(uint16) );
    SensorCall();if (tif->tif_flags&TIFF_SWAB)
        {/*21*/SensorCall();TIFFSwabShort( &entry_type );/*22*/}

    SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
    {
        SensorCall();uint32 value;
        
        memcpy( &value, direntry_raw + 4, sizeof(uint32) );
        SensorCall();if (tif->tif_flags&TIFF_SWAB)
            {/*23*/SensorCall();TIFFSwabLong( &value );/*24*/}
        SensorCall();entry_count = value;

        memcpy( &value, direntry_raw + 8, sizeof(uint32) );
        SensorCall();if (tif->tif_flags&TIFF_SWAB)
            {/*25*/SensorCall();TIFFSwabLong( &value );/*26*/}
        SensorCall();entry_offset = value;
    }
    else
    {
        SensorCall();memcpy( &entry_count, direntry_raw + 4, sizeof(uint64) );
        SensorCall();if (tif->tif_flags&TIFF_SWAB)
            {/*27*/SensorCall();TIFFSwabLong8( &entry_count );/*28*/}

        SensorCall();memcpy( &entry_offset, direntry_raw + 12, sizeof(uint64) );
        SensorCall();if (tif->tif_flags&TIFF_SWAB)
            {/*29*/SensorCall();TIFFSwabLong8( &entry_offset );/*30*/}
    }

/* -------------------------------------------------------------------- */
/*      What data type do we want to write this as?                     */
/* -------------------------------------------------------------------- */
    SensorCall();if( TIFFDataWidth(in_datatype) == 8 && !(tif->tif_flags&TIFF_BIGTIFF) )
    {
        SensorCall();if( in_datatype == TIFF_LONG8 )
            {/*31*/SensorCall();datatype = TIFF_LONG;/*32*/}
        else {/*33*/SensorCall();if( in_datatype == TIFF_SLONG8 )
            {/*35*/SensorCall();datatype = TIFF_SLONG;/*36*/}
        else {/*37*/SensorCall();if( in_datatype == TIFF_IFD8 )
            {/*39*/SensorCall();datatype = TIFF_IFD;/*40*/}
        else
            {/*41*/SensorCall();datatype = in_datatype;/*42*/}/*38*/}/*34*/}
    }
    else 
        {/*43*/SensorCall();datatype = in_datatype;/*44*/}

/* -------------------------------------------------------------------- */
/*      Prepare buffer of actual data to write.  This includes          */
/*      swabbing as needed.                                             */
/* -------------------------------------------------------------------- */
    SensorCall();buf_to_write =
	    (uint8 *)_TIFFCheckMalloc(tif, count, TIFFDataWidth(datatype),
				      "for field buffer.");
    SensorCall();if (!buf_to_write)
        {/*45*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*46*/}

    SensorCall();if( datatype == in_datatype )
        {/*47*/SensorCall();memcpy( buf_to_write, data, count * TIFFDataWidth(datatype) );/*48*/}
    else {/*49*/SensorCall();if( datatype == TIFF_SLONG && in_datatype == TIFF_SLONG8 )
    {
	SensorCall();tmsize_t i;

        SensorCall();for( i = 0; i < count; i++ )
        {
            SensorCall();((int32 *) buf_to_write)[i] = 
                (int32) ((int64 *) data)[i];
            SensorCall();if( (int64) ((int32 *) buf_to_write)[i] != ((int64 *) data)[i] )
            {
                SensorCall();_TIFFfree( buf_to_write );
                TIFFErrorExt( tif->tif_clientdata, module, 
                              "Value exceeds 32bit range of output type." );
                {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
            }
        }
    }
    else {/*51*/SensorCall();if( (datatype == TIFF_LONG && in_datatype == TIFF_LONG8)
             || (datatype == TIFF_IFD && in_datatype == TIFF_IFD8) )
    {
	SensorCall();tmsize_t i;

        SensorCall();for( i = 0; i < count; i++ )
        {
            SensorCall();((uint32 *) buf_to_write)[i] = 
                (uint32) ((uint64 *) data)[i];
            SensorCall();if( (uint64) ((uint32 *) buf_to_write)[i] != ((uint64 *) data)[i] )
            {
                SensorCall();_TIFFfree( buf_to_write );
                TIFFErrorExt( tif->tif_clientdata, module, 
                              "Value exceeds 32bit range of output type." );
                {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
            }
        }
    ;/*52*/}/*50*/}}

    SensorCall();if( TIFFDataWidth(datatype) > 1 && (tif->tif_flags&TIFF_SWAB) )
    {
        SensorCall();if( TIFFDataWidth(datatype) == 2 )
            {/*53*/SensorCall();TIFFSwabArrayOfShort( (uint16 *) buf_to_write, count );/*54*/}
        else {/*55*/SensorCall();if( TIFFDataWidth(datatype) == 4 )
            {/*57*/SensorCall();TIFFSwabArrayOfLong( (uint32 *) buf_to_write, count );/*58*/}
        else {/*59*/SensorCall();if( TIFFDataWidth(datatype) == 8 )
            {/*61*/SensorCall();TIFFSwabArrayOfLong8( (uint64 *) buf_to_write, count );/*62*/}/*60*/}/*56*/}
    }

/* -------------------------------------------------------------------- */
/*      Is this a value that fits into the directory entry?             */
/* -------------------------------------------------------------------- */
    SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
    {
        SensorCall();if( TIFFDataWidth(datatype) * count <= 4 )
        {
            SensorCall();entry_offset = read_offset + 8;
            value_in_entry = 1;
        }
    }
    else
    {
        SensorCall();if( TIFFDataWidth(datatype) * count <= 8 )
        {
            SensorCall();entry_offset = read_offset + 12;
            value_in_entry = 1;
        }
    }

/* -------------------------------------------------------------------- */
/*      If the tag type, and count match, then we just write it out     */
/*      over the old values without altering the directory entry at     */
/*      all.                                                            */
/* -------------------------------------------------------------------- */
    SensorCall();if( entry_count == (uint64)count && entry_type == (uint16) datatype )
    {
        SensorCall();if (!SeekOK(tif, entry_offset)) {
            SensorCall();_TIFFfree( buf_to_write );
            TIFFErrorExt(tif->tif_clientdata, module,
                         "%s: Seek error accessing TIFF directory",
                         tif->tif_name);
            {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
        }
        SensorCall();if (!WriteOK(tif, buf_to_write, count*TIFFDataWidth(datatype))) {
            SensorCall();_TIFFfree( buf_to_write );
            TIFFErrorExt(tif->tif_clientdata, module,
                         "Error writing directory link");
            {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
        }

        SensorCall();_TIFFfree( buf_to_write );
        {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
    }

/* -------------------------------------------------------------------- */
/*      Otherwise, we write the new tag data at the end of the file.    */
/* -------------------------------------------------------------------- */
    SensorCall();if( !value_in_entry )
    {
        SensorCall();entry_offset = TIFFSeekFile(tif,0,SEEK_END);
        
        SensorCall();if (!WriteOK(tif, buf_to_write, count*TIFFDataWidth(datatype))) {
            SensorCall();_TIFFfree( buf_to_write );
            TIFFErrorExt(tif->tif_clientdata, module,
                         "Error writing directory link");
            {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
        }
        
        SensorCall();_TIFFfree( buf_to_write );
    }
    else
    {
        SensorCall();memcpy( &entry_offset, buf_to_write, count*TIFFDataWidth(datatype));
    }

/* -------------------------------------------------------------------- */
/*      Adjust the directory entry.                                     */
/* -------------------------------------------------------------------- */
    SensorCall();entry_type = datatype;
    memcpy( direntry_raw + 2, &entry_type, sizeof(uint16) );
    SensorCall();if (tif->tif_flags&TIFF_SWAB)
        {/*63*/SensorCall();TIFFSwabShort( (uint16 *) (direntry_raw + 2) );/*64*/}

    SensorCall();if (!(tif->tif_flags&TIFF_BIGTIFF))
    {
        SensorCall();uint32 value;

        value = (uint32) entry_count;
        memcpy( direntry_raw + 4, &value, sizeof(uint32) );
        SensorCall();if (tif->tif_flags&TIFF_SWAB)
            {/*65*/SensorCall();TIFFSwabLong( (uint32 *) (direntry_raw + 4) );/*66*/}

        SensorCall();value = (uint32) entry_offset;
        memcpy( direntry_raw + 8, &value, sizeof(uint32) );
        SensorCall();if (tif->tif_flags&TIFF_SWAB)
            {/*67*/SensorCall();TIFFSwabLong( (uint32 *) (direntry_raw + 8) );/*68*/}
    }
    else
    {
        SensorCall();memcpy( direntry_raw + 4, &entry_count, sizeof(uint64) );
        SensorCall();if (tif->tif_flags&TIFF_SWAB)
            {/*69*/SensorCall();TIFFSwabLong8( (uint64 *) (direntry_raw + 4) );/*70*/}

        SensorCall();memcpy( direntry_raw + 12, &entry_offset, sizeof(uint64) );
        SensorCall();if (tif->tif_flags&TIFF_SWAB)
            {/*71*/SensorCall();TIFFSwabLong8( (uint64 *) (direntry_raw + 12) );/*72*/}
    }

/* -------------------------------------------------------------------- */
/*      Write the directory entry out to disk.                          */
/* -------------------------------------------------------------------- */
    SensorCall();if (!SeekOK(tif, read_offset )) {
        SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
                     "%s: Seek error accessing TIFF directory",
                     tif->tif_name);
        {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
    }

    SensorCall();if (!WriteOK(tif, direntry_raw,dirsize))
    {
        SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
                     "%s: Can not write TIFF directory entry.",
                     tif->tif_name);
        {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
    }
    
    {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}
/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
