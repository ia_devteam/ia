/* $Id: tif_packbits.c,v 1.22 2012-06-20 05:25:33 fwarmerdam Exp $ */

/*
 * Copyright (c) 1988-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

#include "tiffiop.h"
#include "/var/tmp/sensor.h"
#ifdef PACKBITS_SUPPORT
/*
 * TIFF Library.
 *
 * PackBits Compression Algorithm Support
 */
#include <stdio.h>

static int
PackBitsPreEncode(TIFF* tif, uint16 s)
{
	SensorCall();(void) s;

	SensorCall();if (!(tif->tif_data = (uint8*)_TIFFmalloc(sizeof(tmsize_t))))
		{/*1*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*2*/}
	/*
	 * Calculate the scanline/tile-width size in bytes.
	 */
	SensorCall();if (isTiled(tif))
		{/*3*/SensorCall();*(tmsize_t*)tif->tif_data = TIFFTileRowSize(tif);/*4*/}
	else
		{/*5*/SensorCall();*(tmsize_t*)tif->tif_data = TIFFScanlineSize(tif);/*6*/}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

static int
PackBitsPostEncode(TIFF* tif)
{
        SensorCall();if (tif->tif_data)
            {/*7*/SensorCall();_TIFFfree(tif->tif_data);/*8*/}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

/*
 * Encode a run of pixels.
 */
static int
PackBitsEncode(TIFF* tif, uint8* buf, tmsize_t cc, uint16 s)
{
	SensorCall();unsigned char* bp = (unsigned char*) buf;
	uint8* op;
	uint8* ep;
	uint8* lastliteral;
	long n, slop;
	int b;
	enum { BASE, LITERAL, RUN, LITERAL_RUN } state;

	(void) s;
	op = tif->tif_rawcp;
	ep = tif->tif_rawdata + tif->tif_rawdatasize;
	state = BASE;
	lastliteral = 0;
	SensorCall();while (cc > 0) {
		/*
		 * Find the longest string of identical bytes.
		 */
		SensorCall();b = *bp++, cc--, n = 1;
		SensorCall();for (; cc > 0 && b == *bp; cc--, bp++)
			{/*9*/SensorCall();n++;/*10*/}
	again:
		SensorCall();if (op + 2 >= ep) {		/* insure space for new data */
			/*
			 * Be careful about writing the last
			 * literal.  Must write up to that point
			 * and then copy the remainder to the
			 * front of the buffer.
			 */
			SensorCall();if (state == LITERAL || state == LITERAL_RUN) {
				SensorCall();slop = (long)(op - lastliteral);
				tif->tif_rawcc += (tmsize_t)(lastliteral - tif->tif_rawcp);
				SensorCall();if (!TIFFFlushData1(tif))
					{/*11*/{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}/*12*/}
				SensorCall();op = tif->tif_rawcp;
				SensorCall();while (slop-- > 0)
					{/*13*/SensorCall();*op++ = *lastliteral++;/*14*/}
				SensorCall();lastliteral = tif->tif_rawcp;
			} else {
				SensorCall();tif->tif_rawcc += (tmsize_t)(op - tif->tif_rawcp);
				SensorCall();if (!TIFFFlushData1(tif))
					{/*15*/{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}/*16*/}
				SensorCall();op = tif->tif_rawcp;
			}
		}
		SensorCall();switch (state) {
		case BASE:		/* initial state, set run/literal */
			SensorCall();if (n > 1) {
				SensorCall();state = RUN;
				SensorCall();if (n > 128) {
					SensorCall();*op++ = (uint8) -127;
					*op++ = (uint8) b;
					n -= 128;
					SensorCall();goto again;
				}
				SensorCall();*op++ = (uint8)(-(n-1));
				*op++ = (uint8) b;
			} else {
				SensorCall();lastliteral = op;
				*op++ = 0;
				*op++ = (uint8) b;
				state = LITERAL;
			}
			SensorCall();break;
		case LITERAL:		/* last object was literal string */
			SensorCall();if (n > 1) {
				SensorCall();state = LITERAL_RUN;
				SensorCall();if (n > 128) {
					SensorCall();*op++ = (uint8) -127;
					*op++ = (uint8) b;
					n -= 128;
					SensorCall();goto again;
				}
				SensorCall();*op++ = (uint8)(-(n-1));	/* encode run */
				*op++ = (uint8) b;
			} else {			/* extend literal */
				SensorCall();if (++(*lastliteral) == 127)
					{/*17*/SensorCall();state = BASE;/*18*/}
				SensorCall();*op++ = (uint8) b;
			}
			SensorCall();break;
		case RUN:		/* last object was run */
			SensorCall();if (n > 1) {
				SensorCall();if (n > 128) {
					SensorCall();*op++ = (uint8) -127;
					*op++ = (uint8) b;
					n -= 128;
					SensorCall();goto again;
				}
				SensorCall();*op++ = (uint8)(-(n-1));
				*op++ = (uint8) b;
			} else {
				SensorCall();lastliteral = op;
				*op++ = 0;
				*op++ = (uint8) b;
				state = LITERAL;
			}
			SensorCall();break;
		case LITERAL_RUN:	/* literal followed by a run */
			/*
			 * Check to see if previous run should
			 * be converted to a literal, in which
			 * case we convert literal-run-literal
			 * to a single literal.
			 */
			SensorCall();if (n == 1 && op[-2] == (uint8) -1 &&
			    *lastliteral < 126) {
				SensorCall();state = (((*lastliteral) += 2) == 127 ?
				    BASE : LITERAL);
				op[-2] = op[-1];	/* replicate */
			} else
				{/*19*/SensorCall();state = RUN;/*20*/}
			SensorCall();goto again;
		}
	}
	SensorCall();tif->tif_rawcc += (tmsize_t)(op - tif->tif_rawcp);
	tif->tif_rawcp = op;
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

/*
 * Encode a rectangular chunk of pixels.  We break it up
 * into row-sized pieces to insure that encoded runs do
 * not span rows.  Otherwise, there can be problems with
 * the decoder if data is read, for example, by scanlines
 * when it was encoded by strips.
 */
static int
PackBitsEncodeChunk(TIFF* tif, uint8* bp, tmsize_t cc, uint16 s)
{
	SensorCall();tmsize_t rowsize = *(tmsize_t*)tif->tif_data;

	SensorCall();while (cc > 0) {
		SensorCall();tmsize_t chunk = rowsize;
		
		SensorCall();if( cc < chunk )
		    {/*21*/SensorCall();chunk = cc;/*22*/}

		SensorCall();if (PackBitsEncode(tif, bp, chunk, s) < 0)
		    {/*23*/{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}/*24*/}
		SensorCall();bp += chunk;
		cc -= chunk;
	}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

static int
PackBitsDecode(TIFF* tif, uint8* op, tmsize_t occ, uint16 s)
{
	SensorCall();static const char module[] = "PackBitsDecode";
	char *bp;
	tmsize_t cc;
	long n;
	int b;

	(void) s;
	bp = (char*) tif->tif_rawcp;
	cc = tif->tif_rawcc;
	SensorCall();while (cc > 0 && occ > 0) {
		SensorCall();n = (long) *bp++, cc--;
		/*
		 * Watch out for compilers that
		 * don't sign extend chars...
		 */
		SensorCall();if (n >= 128)
			{/*25*/SensorCall();n -= 256;/*26*/}
		SensorCall();if (n < 0) {		/* replicate next byte -n+1 times */
			SensorCall();if (n == -128)	/* nop */
				{/*27*/SensorCall();continue;/*28*/}
			SensorCall();n = -n + 1;
			SensorCall();if( occ < (tmsize_t)n )
			{
				SensorCall();TIFFWarningExt(tif->tif_clientdata, module,
				    "Discarding %lu bytes to avoid buffer overrun",
				    (unsigned long) ((tmsize_t)n - occ));
				n = (long)occ;
			}
			SensorCall();occ -= n;
			b = *bp++, cc--;
			SensorCall();while (n-- > 0)
				{/*29*/SensorCall();*op++ = (uint8) b;/*30*/}
		} else {		/* copy next n+1 bytes literally */
			SensorCall();if (occ < (tmsize_t)(n + 1))
			{
				SensorCall();TIFFWarningExt(tif->tif_clientdata, module,
				    "Discarding %lu bytes to avoid buffer overrun",
				    (unsigned long) ((tmsize_t)n - occ + 1));
				n = (long)occ - 1;
			}
			SensorCall();if (cc < (tmsize_t) (n+1)) 
			{
				SensorCall();TIFFWarningExt(tif->tif_clientdata, module,
					       "Terminating PackBitsDecode due to lack of data.");
				SensorCall();break;
			}
			SensorCall();_TIFFmemcpy(op, bp, ++n);
			op += n; occ -= n;
			bp += n; cc -= n;
		}
	}
	SensorCall();tif->tif_rawcp = (uint8*) bp;
	tif->tif_rawcc = cc;
	SensorCall();if (occ > 0) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
		    "Not enough data for scanline %lu",
		    (unsigned long) tif->tif_row);
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

int
TIFFInitPackBits(TIFF* tif, int scheme)
{
	SensorCall();(void) scheme;
	tif->tif_decoderow = PackBitsDecode;
	tif->tif_decodestrip = PackBitsDecode;
	tif->tif_decodetile = PackBitsDecode;
	tif->tif_preencode = PackBitsPreEncode;
	tif->tif_postencode = PackBitsPostEncode;
	tif->tif_encoderow = PackBitsEncode;
	tif->tif_encodestrip = PackBitsEncodeChunk;
	tif->tif_encodetile = PackBitsEncodeChunk;
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}
#endif /* PACKBITS_SUPPORT */

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
