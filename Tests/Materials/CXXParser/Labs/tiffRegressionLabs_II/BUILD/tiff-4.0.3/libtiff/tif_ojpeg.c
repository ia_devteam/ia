/* $Id: tif_ojpeg.c,v 1.56 2012-05-24 03:15:18 fwarmerdam Exp $ */

/* WARNING: The type of JPEG encapsulation defined by the TIFF Version 6.0
   specification is now totally obsolete and deprecated for new applications and
   images. This file was was created solely in order to read unconverted images
   still present on some users' computer systems. It will never be extended
   to write such files. Writing new-style JPEG compressed TIFFs is implemented
   in tif_jpeg.c.

   The code is carefully crafted to robustly read all gathered JPEG-in-TIFF
   testfiles, and anticipate as much as possible all other... But still, it may
   fail on some. If you encounter problems, please report them on the TIFF
   mailing list and/or to Joris Van Damme <info@awaresystems.be>.

   Please read the file called "TIFF Technical Note #2" if you need to be
   convinced this compression scheme is bad and breaks TIFF. That document
   is linked to from the LibTiff site <http://www.remotesensing.org/libtiff/>
   and from AWare Systems' TIFF section
   <http://www.awaresystems.be/imaging/tiff.html>. It is also absorbed
   in Adobe's specification supplements, marked "draft" up to this day, but
   supported by the TIFF community.

   This file interfaces with Release 6B of the JPEG Library written by the
   Independent JPEG Group. Previous versions of this file required a hack inside
   the LibJpeg library. This version no longer requires that. Remember to
   remove the hack if you update from the old version.

   Copyright (c) Joris Van Damme <info@awaresystems.be>
   Copyright (c) AWare Systems <http://www.awaresystems.be/>

   The licence agreement for this file is the same as the rest of the LibTiff
   library.

   IN NO EVENT SHALL JORIS VAN DAMME OR AWARE SYSTEMS BE LIABLE FOR
   ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
   WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
   LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
   OF THIS SOFTWARE.

   Joris Van Damme and/or AWare Systems may be available for custom
   developement. If you like what you see, and need anything similar or related,
   contact <info@awaresystems.be>.
*/

/* What is what, and what is not?

   This decoder starts with an input stream, that is essentially the JpegInterchangeFormat
   stream, if any, followed by the strile data, if any. This stream is read in
   OJPEGReadByte and related functions.

   It analyzes the start of this stream, until it encounters non-marker data, i.e.
   compressed image data. Some of the header markers it sees have no actual content,
   like the SOI marker, and APP/COM markers that really shouldn't even be there. Some
   other markers do have content, and the valuable bits and pieces of information
   in these markers are saved, checking all to verify that the stream is more or
   less within expected bounds. This happens inside the OJPEGReadHeaderInfoSecStreamXxx
   functions.

   Some OJPEG imagery contains no valid JPEG header markers. This situation is picked
   up on if we've seen no SOF marker when we're at the start of the compressed image
   data. In this case, the tables are read from JpegXxxTables tags, and the other
   bits and pieces of information is initialized to its most basic value. This is
   implemented in the OJPEGReadHeaderInfoSecTablesXxx functions.

   When this is complete, a good and valid JPEG header can be assembled, and this is
   passed through to LibJpeg. When that's done, the remainder of the input stream, i.e.
   the compressed image data, can be passed through unchanged. This is done in
   OJPEGWriteStream functions.

   LibTiff rightly expects to know the subsampling values before decompression. Just like
   in new-style JPEG-in-TIFF, though, or even more so, actually, the YCbCrsubsampling
   tag is notoriously unreliable. To correct these tag values with the ones inside
   the JPEG stream, the first part of the input stream is pre-scanned in
   OJPEGSubsamplingCorrect, making no note of any other data, reporting no warnings
   or errors, up to the point where either these values are read, or it's clear they
   aren't there. This means that some of the data is read twice, but we feel speed
   in correcting these values is important enough to warrant this sacrifice. Allthough
   there is currently no define or other configuration mechanism to disable this behaviour,
   the actual header scanning is build to robustly respond with error report if it
   should encounter an uncorrected mismatch of subsampling values. See
   OJPEGReadHeaderInfoSecStreamSof.

   The restart interval and restart markers are the most tricky part... The restart
   interval can be specified in a tag. It can also be set inside the input JPEG stream.
   It can be used inside the input JPEG stream. If reading from strile data, we've
   consistenly discovered the need to insert restart markers in between the different
   striles, as is also probably the most likely interpretation of the original TIFF 6.0
   specification. With all this setting of interval, and actual use of markers that is not
   predictable at the time of valid JPEG header assembly, the restart thing may turn
   out the Achilles heel of this implementation. Fortunately, most OJPEG writer vendors
   succeed in reading back what they write, which may be the reason why we've been able
   to discover ways that seem to work.

   Some special provision is made for planarconfig separate OJPEG files. These seem
   to consistently contain header info, a SOS marker, a plane, SOS marker, plane, SOS,
   and plane. This may or may not be a valid JPEG configuration, we don't know and don't
   care. We want LibTiff to be able to access the planes individually, without huge
   buffering inside LibJpeg, anyway. So we compose headers to feed to LibJpeg, in this
   case, that allow us to pass a single plane such that LibJpeg sees a valid
   single-channel JPEG stream. Locating subsequent SOS markers, and thus subsequent
   planes, is done inside OJPEGReadSecondarySos.

   The benefit of the scheme is... that it works, basically. We know of no other that
   does. It works without checking software tag, or otherwise going about things in an
   OJPEG flavor specific manner. Instead, it is a single scheme, that covers the cases
   with and without JpegInterchangeFormat, with and without striles, with part of
   the header in JpegInterchangeFormat and remainder in first strile, etc. It is forgiving
   and robust, may likely work with OJPEG flavors we've not seen yet, and makes most out
   of the data.

   Another nice side-effect is that a complete JPEG single valid stream is build if
   planarconfig is not separate (vast majority). We may one day use that to build
   converters to JPEG, and/or to new-style JPEG compression inside TIFF.

   A dissadvantage is the lack of random access to the individual striles. This is the
   reason for much of the complicated restart-and-position stuff inside OJPEGPreDecode.
   Applications would do well accessing all striles in order, as this will result in
   a single sequential scan of the input stream, and no restarting of LibJpeg decoding
   session.
*/

#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN

#include "tiffiop.h"
#include "/var/tmp/sensor.h"
#ifdef OJPEG_SUPPORT

/* Configuration defines here are:
 * JPEG_ENCAP_EXTERNAL: The normal way to call libjpeg, uses longjump. In some environments,
 * 	like eg LibTiffDelphi, this is not possible. For this reason, the actual calls to
 * 	libjpeg, with longjump stuff, are encapsulated in dedicated functions. When
 * 	JPEG_ENCAP_EXTERNAL is defined, these encapsulating functions are declared external
 * 	to this unit, and can be defined elsewhere to use stuff other then longjump.
 * 	The default mode, without JPEG_ENCAP_EXTERNAL, implements the call encapsulators
 * 	here, internally, with normal longjump.
 * SETJMP, LONGJMP, JMP_BUF: On some machines/environments a longjump equivalent is
 * 	conviniently available, but still it may be worthwhile to use _setjmp or sigsetjmp
 * 	in place of plain setjmp. These macros will make it easier. It is useless
 * 	to fiddle with these if you define JPEG_ENCAP_EXTERNAL.
 * OJPEG_BUFFER: Define the size of the desired buffer here. Should be small enough so as to guarantee
 * 	instant processing, optimal streaming and optimal use of processor cache, but also big
 * 	enough so as to not result in significant call overhead. It should be at least a few
 * 	bytes to accomodate some structures (this is verified in asserts), but it would not be
 * 	sensible to make it this small anyway, and it should be at most 64K since it is indexed
 * 	with uint16. We recommend 2K.
 * EGYPTIANWALK: You could also define EGYPTIANWALK here, but it is not used anywhere and has
 * 	absolutely no effect. That is why most people insist the EGYPTIANWALK is a bit silly.
 */

/* define LIBJPEG_ENCAP_EXTERNAL */
#define SETJMP(jbuf) setjmp(jbuf)
#define LONGJMP(jbuf,code) longjmp(jbuf,code)
#define JMP_BUF jmp_buf
#define OJPEG_BUFFER 2048
/* define EGYPTIANWALK */

#define JPEG_MARKER_SOF0 0xC0
#define JPEG_MARKER_SOF1 0xC1
#define JPEG_MARKER_SOF3 0xC3
#define JPEG_MARKER_DHT 0xC4
#define JPEG_MARKER_RST0 0XD0
#define JPEG_MARKER_SOI 0xD8
#define JPEG_MARKER_EOI 0xD9
#define JPEG_MARKER_SOS 0xDA
#define JPEG_MARKER_DQT 0xDB
#define JPEG_MARKER_DRI 0xDD
#define JPEG_MARKER_APP0 0xE0
#define JPEG_MARKER_COM 0xFE

#define FIELD_OJPEG_JPEGINTERCHANGEFORMAT (FIELD_CODEC+0)
#define FIELD_OJPEG_JPEGINTERCHANGEFORMATLENGTH (FIELD_CODEC+1)
#define FIELD_OJPEG_JPEGQTABLES (FIELD_CODEC+2)
#define FIELD_OJPEG_JPEGDCTABLES (FIELD_CODEC+3)
#define FIELD_OJPEG_JPEGACTABLES (FIELD_CODEC+4)
#define FIELD_OJPEG_JPEGPROC (FIELD_CODEC+5)
#define FIELD_OJPEG_JPEGRESTARTINTERVAL (FIELD_CODEC+6)

static const TIFFField ojpegFields[] = {
	{TIFFTAG_JPEGIFOFFSET,1,1,TIFF_LONG8,0,TIFF_SETGET_UINT64,TIFF_SETGET_UNDEFINED,FIELD_OJPEG_JPEGINTERCHANGEFORMAT,TRUE,FALSE,"JpegInterchangeFormat",NULL},
	{TIFFTAG_JPEGIFBYTECOUNT,1,1,TIFF_LONG8,0,TIFF_SETGET_UINT64,TIFF_SETGET_UNDEFINED,FIELD_OJPEG_JPEGINTERCHANGEFORMATLENGTH,TRUE,FALSE,"JpegInterchangeFormatLength",NULL},
	{TIFFTAG_JPEGQTABLES,TIFF_VARIABLE2,TIFF_VARIABLE2,TIFF_LONG8,0,TIFF_SETGET_C32_UINT64,TIFF_SETGET_UNDEFINED,FIELD_OJPEG_JPEGQTABLES,FALSE,TRUE,"JpegQTables",NULL},
	{TIFFTAG_JPEGDCTABLES,TIFF_VARIABLE2,TIFF_VARIABLE2,TIFF_LONG8,0,TIFF_SETGET_C32_UINT64,TIFF_SETGET_UNDEFINED,FIELD_OJPEG_JPEGDCTABLES,FALSE,TRUE,"JpegDcTables",NULL},
	{TIFFTAG_JPEGACTABLES,TIFF_VARIABLE2,TIFF_VARIABLE2,TIFF_LONG8,0,TIFF_SETGET_C32_UINT64,TIFF_SETGET_UNDEFINED,FIELD_OJPEG_JPEGACTABLES,FALSE,TRUE,"JpegAcTables",NULL},
	{TIFFTAG_JPEGPROC,1,1,TIFF_SHORT,0,TIFF_SETGET_UINT16,TIFF_SETGET_UNDEFINED,FIELD_OJPEG_JPEGPROC,FALSE,FALSE,"JpegProc",NULL},
	{TIFFTAG_JPEGRESTARTINTERVAL,1,1,TIFF_SHORT,0,TIFF_SETGET_UINT16,TIFF_SETGET_UNDEFINED,FIELD_OJPEG_JPEGRESTARTINTERVAL,FALSE,FALSE,"JpegRestartInterval",NULL},
};

#ifndef LIBJPEG_ENCAP_EXTERNAL
#include <setjmp.h>
#endif

/* We undefine FAR to avoid conflict with JPEG definition */

#ifdef FAR
#undef FAR
#endif

/*
  Libjpeg's jmorecfg.h defines INT16 and INT32, but only if XMD_H is
  not defined.  Unfortunately, the MinGW and Borland compilers include
  a typedef for INT32, which causes a conflict.  MSVC does not include
  a conficting typedef given the headers which are included.
*/
#if defined(__BORLANDC__) || defined(__MINGW32__)
# define XMD_H 1
#endif

/* Define "boolean" as unsigned char, not int, per Windows custom. */
#if defined(__WIN32__) && !defined(__MINGW32__)
# ifndef __RPCNDR_H__            /* don't conflict if rpcndr.h already read */
   typedef unsigned char boolean;
# endif
# define HAVE_BOOLEAN            /* prevent jmorecfg.h from redefining it */
#endif

#include "jpeglib.h"
#include "jerror.h"

typedef struct jpeg_error_mgr jpeg_error_mgr;
typedef struct jpeg_common_struct jpeg_common_struct;
typedef struct jpeg_decompress_struct jpeg_decompress_struct;
typedef struct jpeg_source_mgr jpeg_source_mgr;

typedef enum {
	osibsNotSetYet,
	osibsJpegInterchangeFormat,
	osibsStrile,
	osibsEof
} OJPEGStateInBufferSource;

typedef enum {
	ososSoi,
	ososQTable0,ososQTable1,ososQTable2,ososQTable3,
	ososDcTable0,ososDcTable1,ososDcTable2,ososDcTable3,
	ososAcTable0,ososAcTable1,ososAcTable2,ososAcTable3,
	ososDri,
	ososSof,
	ososSos,
	ososCompressed,
	ososRst,
	ososEoi
} OJPEGStateOutState;

typedef struct {
	TIFF* tif;
	#ifndef LIBJPEG_ENCAP_EXTERNAL
	JMP_BUF exit_jmpbuf;
	#endif
	TIFFVGetMethod vgetparent;
	TIFFVSetMethod vsetparent;
	TIFFPrintMethod printdir;
	uint64 file_size;
	uint32 image_width;
	uint32 image_length;
	uint32 strile_width;
	uint32 strile_length;
	uint32 strile_length_total;
	uint8 samples_per_pixel;
	uint8 plane_sample_offset;
	uint8 samples_per_pixel_per_plane;
	uint64 jpeg_interchange_format;
	uint64 jpeg_interchange_format_length;
	uint8 jpeg_proc;
	uint8 subsamplingcorrect;
	uint8 subsamplingcorrect_done;
	uint8 subsampling_tag;
	uint8 subsampling_hor;
	uint8 subsampling_ver;
	uint8 subsampling_force_desubsampling_inside_decompression;
	uint8 qtable_offset_count;
	uint8 dctable_offset_count;
	uint8 actable_offset_count;
	uint64 qtable_offset[3];
	uint64 dctable_offset[3];
	uint64 actable_offset[3];
	uint8* qtable[4];
	uint8* dctable[4];
	uint8* actable[4];
	uint16 restart_interval;
	uint8 restart_index;
	uint8 sof_log;
	uint8 sof_marker_id;
	uint32 sof_x;
	uint32 sof_y;
	uint8 sof_c[3];
	uint8 sof_hv[3];
	uint8 sof_tq[3];
	uint8 sos_cs[3];
	uint8 sos_tda[3];
	struct {
		uint8 log;
		OJPEGStateInBufferSource in_buffer_source;
		uint32 in_buffer_next_strile;
		uint64 in_buffer_file_pos;
		uint64 in_buffer_file_togo;
	} sos_end[3];
	uint8 readheader_done;
	uint8 writeheader_done;
	uint16 write_cursample;
	uint32 write_curstrile;
	uint8 libjpeg_session_active;
	uint8 libjpeg_jpeg_query_style;
	jpeg_error_mgr libjpeg_jpeg_error_mgr;
	jpeg_decompress_struct libjpeg_jpeg_decompress_struct;
	jpeg_source_mgr libjpeg_jpeg_source_mgr;
	uint8 subsampling_convert_log;
	uint32 subsampling_convert_ylinelen;
	uint32 subsampling_convert_ylines;
	uint32 subsampling_convert_clinelen;
	uint32 subsampling_convert_clines;
	uint32 subsampling_convert_ybuflen;
	uint32 subsampling_convert_cbuflen;
	uint32 subsampling_convert_ycbcrbuflen;
	uint8* subsampling_convert_ycbcrbuf;
	uint8* subsampling_convert_ybuf;
	uint8* subsampling_convert_cbbuf;
	uint8* subsampling_convert_crbuf;
	uint32 subsampling_convert_ycbcrimagelen;
	uint8** subsampling_convert_ycbcrimage;
	uint32 subsampling_convert_clinelenout;
	uint32 subsampling_convert_state;
	uint32 bytes_per_line;   /* if the codec outputs subsampled data, a 'line' in bytes_per_line */
	uint32 lines_per_strile; /* and lines_per_strile means subsampling_ver desubsampled rows     */
	OJPEGStateInBufferSource in_buffer_source;
	uint32 in_buffer_next_strile;
	uint32 in_buffer_strile_count;
	uint64 in_buffer_file_pos;
	uint8 in_buffer_file_pos_log;
	uint64 in_buffer_file_togo;
	uint16 in_buffer_togo;
	uint8* in_buffer_cur;
	uint8 in_buffer[OJPEG_BUFFER];
	OJPEGStateOutState out_state;
	uint8 out_buffer[OJPEG_BUFFER];
	uint8* skip_buffer;
} OJPEGState;

static int OJPEGVGetField(TIFF* tif, uint32 tag, va_list ap);
static int OJPEGVSetField(TIFF* tif, uint32 tag, va_list ap);
static void OJPEGPrintDir(TIFF* tif, FILE* fd, long flags);

static int OJPEGFixupTags(TIFF* tif);
static int OJPEGSetupDecode(TIFF* tif);
static int OJPEGPreDecode(TIFF* tif, uint16 s);
static int OJPEGPreDecodeSkipRaw(TIFF* tif);
static int OJPEGPreDecodeSkipScanlines(TIFF* tif);
static int OJPEGDecode(TIFF* tif, uint8* buf, tmsize_t cc, uint16 s);
static int OJPEGDecodeRaw(TIFF* tif, uint8* buf, tmsize_t cc);
static int OJPEGDecodeScanlines(TIFF* tif, uint8* buf, tmsize_t cc);
static void OJPEGPostDecode(TIFF* tif, uint8* buf, tmsize_t cc);
static int OJPEGSetupEncode(TIFF* tif);
static int OJPEGPreEncode(TIFF* tif, uint16 s);
static int OJPEGEncode(TIFF* tif, uint8* buf, tmsize_t cc, uint16 s);
static int OJPEGPostEncode(TIFF* tif);
static void OJPEGCleanup(TIFF* tif);

static void OJPEGSubsamplingCorrect(TIFF* tif);
static int OJPEGReadHeaderInfo(TIFF* tif);
static int OJPEGReadSecondarySos(TIFF* tif, uint16 s);
static int OJPEGWriteHeaderInfo(TIFF* tif);
static void OJPEGLibjpegSessionAbort(TIFF* tif);

static int OJPEGReadHeaderInfoSec(TIFF* tif);
static int OJPEGReadHeaderInfoSecStreamDri(TIFF* tif);
static int OJPEGReadHeaderInfoSecStreamDqt(TIFF* tif);
static int OJPEGReadHeaderInfoSecStreamDht(TIFF* tif);
static int OJPEGReadHeaderInfoSecStreamSof(TIFF* tif, uint8 marker_id);
static int OJPEGReadHeaderInfoSecStreamSos(TIFF* tif);
static int OJPEGReadHeaderInfoSecTablesQTable(TIFF* tif);
static int OJPEGReadHeaderInfoSecTablesDcTable(TIFF* tif);
static int OJPEGReadHeaderInfoSecTablesAcTable(TIFF* tif);

static int OJPEGReadBufferFill(OJPEGState* sp);
static int OJPEGReadByte(OJPEGState* sp, uint8* byte);
static int OJPEGReadBytePeek(OJPEGState* sp, uint8* byte);
static void OJPEGReadByteAdvance(OJPEGState* sp);
static int OJPEGReadWord(OJPEGState* sp, uint16* word);
static int OJPEGReadBlock(OJPEGState* sp, uint16 len, void* mem);
static void OJPEGReadSkip(OJPEGState* sp, uint16 len);

static int OJPEGWriteStream(TIFF* tif, void** mem, uint32* len);
static void OJPEGWriteStreamSoi(TIFF* tif, void** mem, uint32* len);
static void OJPEGWriteStreamQTable(TIFF* tif, uint8 table_index, void** mem, uint32* len);
static void OJPEGWriteStreamDcTable(TIFF* tif, uint8 table_index, void** mem, uint32* len);
static void OJPEGWriteStreamAcTable(TIFF* tif, uint8 table_index, void** mem, uint32* len);
static void OJPEGWriteStreamDri(TIFF* tif, void** mem, uint32* len);
static void OJPEGWriteStreamSof(TIFF* tif, void** mem, uint32* len);
static void OJPEGWriteStreamSos(TIFF* tif, void** mem, uint32* len);
static int OJPEGWriteStreamCompressed(TIFF* tif, void** mem, uint32* len);
static void OJPEGWriteStreamRst(TIFF* tif, void** mem, uint32* len);
static void OJPEGWriteStreamEoi(TIFF* tif, void** mem, uint32* len);

#ifdef LIBJPEG_ENCAP_EXTERNAL
extern int jpeg_create_decompress_encap(OJPEGState* sp, jpeg_decompress_struct* cinfo);
extern int jpeg_read_header_encap(OJPEGState* sp, jpeg_decompress_struct* cinfo, uint8 require_image);
extern int jpeg_start_decompress_encap(OJPEGState* sp, jpeg_decompress_struct* cinfo);
extern int jpeg_read_scanlines_encap(OJPEGState* sp, jpeg_decompress_struct* cinfo, void* scanlines, uint32 max_lines);
extern int jpeg_read_raw_data_encap(OJPEGState* sp, jpeg_decompress_struct* cinfo, void* data, uint32 max_lines);
extern void jpeg_encap_unwind(TIFF* tif);
#else
static int jpeg_create_decompress_encap(OJPEGState* sp, jpeg_decompress_struct* j);
static int jpeg_read_header_encap(OJPEGState* sp, jpeg_decompress_struct* cinfo, uint8 require_image);
static int jpeg_start_decompress_encap(OJPEGState* sp, jpeg_decompress_struct* cinfo);
static int jpeg_read_scanlines_encap(OJPEGState* sp, jpeg_decompress_struct* cinfo, void* scanlines, uint32 max_lines);
static int jpeg_read_raw_data_encap(OJPEGState* sp, jpeg_decompress_struct* cinfo, void* data, uint32 max_lines);
static void jpeg_encap_unwind(TIFF* tif);
#endif

static void OJPEGLibjpegJpegErrorMgrOutputMessage(jpeg_common_struct* cinfo);
static void OJPEGLibjpegJpegErrorMgrErrorExit(jpeg_common_struct* cinfo);
static void OJPEGLibjpegJpegSourceMgrInitSource(jpeg_decompress_struct* cinfo);
static boolean OJPEGLibjpegJpegSourceMgrFillInputBuffer(jpeg_decompress_struct* cinfo);
static void OJPEGLibjpegJpegSourceMgrSkipInputData(jpeg_decompress_struct* cinfo, long num_bytes);
static boolean OJPEGLibjpegJpegSourceMgrResyncToRestart(jpeg_decompress_struct* cinfo, int desired);
static void OJPEGLibjpegJpegSourceMgrTermSource(jpeg_decompress_struct* cinfo);

int
TIFFInitOJPEG(TIFF* tif, int scheme)
{
	SensorCall();static const char module[]="TIFFInitOJPEG";
	OJPEGState* sp;

	assert(scheme==COMPRESSION_OJPEG);

        /*
	 * Merge codec-specific tag information.
	 */
	SensorCall();if (!_TIFFMergeFields(tif, ojpegFields, TIFFArrayCount(ojpegFields))) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
		    "Merging Old JPEG codec-specific tags failed");
		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}

	/* state block */
	SensorCall();sp=_TIFFmalloc(sizeof(OJPEGState));
	SensorCall();if (sp==NULL)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"No space for OJPEG state block");
		SensorCall();return(0);
	}
	SensorCall();_TIFFmemset(sp,0,sizeof(OJPEGState));
	sp->tif=tif;
	sp->jpeg_proc=1;
	sp->subsampling_hor=2;
	sp->subsampling_ver=2;
	TIFFSetField(tif,TIFFTAG_YCBCRSUBSAMPLING,2,2);
	/* tif codec methods */
	tif->tif_fixuptags=OJPEGFixupTags;  
	tif->tif_setupdecode=OJPEGSetupDecode;
	tif->tif_predecode=OJPEGPreDecode;
	tif->tif_postdecode=OJPEGPostDecode;  
	tif->tif_decoderow=OJPEGDecode;  
	tif->tif_decodestrip=OJPEGDecode;  
	tif->tif_decodetile=OJPEGDecode;  
	tif->tif_setupencode=OJPEGSetupEncode;
	tif->tif_preencode=OJPEGPreEncode;
	tif->tif_postencode=OJPEGPostEncode;
	tif->tif_encoderow=OJPEGEncode;  
	tif->tif_encodestrip=OJPEGEncode;  
	tif->tif_encodetile=OJPEGEncode;  
	tif->tif_cleanup=OJPEGCleanup;
	tif->tif_data=(uint8*)sp;
	/* tif tag methods */
	sp->vgetparent=tif->tif_tagmethods.vgetfield;
	tif->tif_tagmethods.vgetfield=OJPEGVGetField;
	sp->vsetparent=tif->tif_tagmethods.vsetfield;
	tif->tif_tagmethods.vsetfield=OJPEGVSetField;
	sp->printdir=tif->tif_tagmethods.printdir;
	tif->tif_tagmethods.printdir=OJPEGPrintDir;
	/* Some OJPEG files don't have strip or tile offsets or bytecounts tags.
	   Some others do, but have totally meaningless or corrupt values
	   in these tags. In these cases, the JpegInterchangeFormat stream is
	   reliable. In any case, this decoder reads the compressed data itself,
	   from the most reliable locations, and we need to notify encapsulating
	   LibTiff not to read raw strips or tiles for us. */
	tif->tif_flags|=TIFF_NOREADRAW;
	SensorCall();return(1);
}

static int
OJPEGVGetField(TIFF* tif, uint32 tag, va_list ap)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	SensorCall();switch(tag)
	{
		case TIFFTAG_JPEGIFOFFSET:
			SensorCall();*va_arg(ap,uint64*)=(uint64)sp->jpeg_interchange_format;
			SensorCall();break;
		case TIFFTAG_JPEGIFBYTECOUNT:
			SensorCall();*va_arg(ap,uint64*)=(uint64)sp->jpeg_interchange_format_length;
			SensorCall();break;
		case TIFFTAG_YCBCRSUBSAMPLING:
			SensorCall();if (sp->subsamplingcorrect_done==0)
				{/*1*/SensorCall();OJPEGSubsamplingCorrect(tif);/*2*/}
			SensorCall();*va_arg(ap,uint16*)=(uint16)sp->subsampling_hor;
			*va_arg(ap,uint16*)=(uint16)sp->subsampling_ver;
			SensorCall();break;
		case TIFFTAG_JPEGQTABLES:
			SensorCall();*va_arg(ap,uint32*)=(uint32)sp->qtable_offset_count;
			*va_arg(ap,void**)=(void*)sp->qtable_offset; 
			SensorCall();break;
		case TIFFTAG_JPEGDCTABLES:
			SensorCall();*va_arg(ap,uint32*)=(uint32)sp->dctable_offset_count;
			*va_arg(ap,void**)=(void*)sp->dctable_offset;  
			SensorCall();break;
		case TIFFTAG_JPEGACTABLES:
			SensorCall();*va_arg(ap,uint32*)=(uint32)sp->actable_offset_count;
			*va_arg(ap,void**)=(void*)sp->actable_offset;
			SensorCall();break;
		case TIFFTAG_JPEGPROC:
			SensorCall();*va_arg(ap,uint16*)=(uint16)sp->jpeg_proc;
			SensorCall();break;
		case TIFFTAG_JPEGRESTARTINTERVAL:
			SensorCall();*va_arg(ap,uint16*)=sp->restart_interval;
			SensorCall();break;
		default:
			{int  ReplaceReturn = (*sp->vgetparent)(tif,tag,ap); SensorCall(); return ReplaceReturn;}
	}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

static int
OJPEGVSetField(TIFF* tif, uint32 tag, va_list ap)
{
	SensorCall();static const char module[]="OJPEGVSetField";
	OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint32 ma;
	uint64* mb;
	uint32 n;
	SensorCall();switch(tag)
	{
		case TIFFTAG_JPEGIFOFFSET:
			SensorCall();sp->jpeg_interchange_format=(uint64)va_arg(ap,uint64);
			SensorCall();break;
		case TIFFTAG_JPEGIFBYTECOUNT:
			SensorCall();sp->jpeg_interchange_format_length=(uint64)va_arg(ap,uint64);
			SensorCall();break;
		case TIFFTAG_YCBCRSUBSAMPLING:
			SensorCall();sp->subsampling_tag=1;
			sp->subsampling_hor=(uint8)va_arg(ap,uint16_vap);
			sp->subsampling_ver=(uint8)va_arg(ap,uint16_vap);
			tif->tif_dir.td_ycbcrsubsampling[0]=sp->subsampling_hor;
			tif->tif_dir.td_ycbcrsubsampling[1]=sp->subsampling_ver;
			SensorCall();break;
		case TIFFTAG_JPEGQTABLES:
			SensorCall();ma=(uint32)va_arg(ap,uint32);
			SensorCall();if (ma!=0)
			{
				SensorCall();if (ma>3)
				{
					SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"JpegQTables tag has incorrect count");
					SensorCall();return(0);
				}
				SensorCall();sp->qtable_offset_count=(uint8)ma;
				mb=(uint64*)va_arg(ap,uint64*);
				SensorCall();for (n=0; n<ma; n++)
					{/*3*/SensorCall();sp->qtable_offset[n]=mb[n];/*4*/}
			}
			SensorCall();break;
		case TIFFTAG_JPEGDCTABLES:
			SensorCall();ma=(uint32)va_arg(ap,uint32);
			SensorCall();if (ma!=0)
			{
				SensorCall();if (ma>3)
				{
					SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"JpegDcTables tag has incorrect count");
					SensorCall();return(0);
				}
				SensorCall();sp->dctable_offset_count=(uint8)ma;
				mb=(uint64*)va_arg(ap,uint64*);
				SensorCall();for (n=0; n<ma; n++)
					{/*5*/SensorCall();sp->dctable_offset[n]=mb[n];/*6*/}
			}
			SensorCall();break;
		case TIFFTAG_JPEGACTABLES:
			SensorCall();ma=(uint32)va_arg(ap,uint32);
			SensorCall();if (ma!=0)
			{
				SensorCall();if (ma>3)
				{
					SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"JpegAcTables tag has incorrect count");
					SensorCall();return(0);
				}
				SensorCall();sp->actable_offset_count=(uint8)ma;
				mb=(uint64*)va_arg(ap,uint64*);
				SensorCall();for (n=0; n<ma; n++)
					{/*7*/SensorCall();sp->actable_offset[n]=mb[n];/*8*/}
			}
			SensorCall();break;
		case TIFFTAG_JPEGPROC:
			SensorCall();sp->jpeg_proc=(uint8)va_arg(ap,uint16_vap);
			SensorCall();break;
		case TIFFTAG_JPEGRESTARTINTERVAL:
			SensorCall();sp->restart_interval=(uint16)va_arg(ap,uint16_vap);
			SensorCall();break;
		default:
			{int  ReplaceReturn = (*sp->vsetparent)(tif,tag,ap); SensorCall(); return ReplaceReturn;}
	}
	TIFFSetFieldBit(tif,TIFFFieldWithTag(tif,tag)->field_bit);
	SensorCall();tif->tif_flags|=TIFF_DIRTYDIRECT;
	SensorCall();return(1);
}

static void
OJPEGPrintDir(TIFF* tif, FILE* fd, long flags)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint8 m;
	(void)flags;
	assert(sp!=NULL);
	SensorCall();if (TIFFFieldSet(tif,FIELD_OJPEG_JPEGINTERCHANGEFORMAT))
		{/*9*/SensorCall();fprintf(fd,"  JpegInterchangeFormat: " TIFF_UINT64_FORMAT "\n",(TIFF_UINT64_T)sp->jpeg_interchange_format);/*10*/}  
	SensorCall();if (TIFFFieldSet(tif,FIELD_OJPEG_JPEGINTERCHANGEFORMATLENGTH))
		{/*11*/SensorCall();fprintf(fd,"  JpegInterchangeFormatLength: " TIFF_UINT64_FORMAT "\n",(TIFF_UINT64_T)sp->jpeg_interchange_format_length);/*12*/}  
	SensorCall();if (TIFFFieldSet(tif,FIELD_OJPEG_JPEGQTABLES))
	{
		SensorCall();fprintf(fd,"  JpegQTables:");
		SensorCall();for (m=0; m<sp->qtable_offset_count; m++)
			{/*13*/SensorCall();fprintf(fd," " TIFF_UINT64_FORMAT,(TIFF_UINT64_T)sp->qtable_offset[m]);/*14*/}
		SensorCall();fprintf(fd,"\n");
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_OJPEG_JPEGDCTABLES))
	{
		SensorCall();fprintf(fd,"  JpegDcTables:");
		SensorCall();for (m=0; m<sp->dctable_offset_count; m++)
			{/*15*/SensorCall();fprintf(fd," " TIFF_UINT64_FORMAT,(TIFF_UINT64_T)sp->dctable_offset[m]);/*16*/}
		SensorCall();fprintf(fd,"\n");
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_OJPEG_JPEGACTABLES))
	{
		SensorCall();fprintf(fd,"  JpegAcTables:");
		SensorCall();for (m=0; m<sp->actable_offset_count; m++)
			{/*17*/SensorCall();fprintf(fd," " TIFF_UINT64_FORMAT,(TIFF_UINT64_T)sp->actable_offset[m]);/*18*/}
		SensorCall();fprintf(fd,"\n");
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_OJPEG_JPEGPROC))
		{/*19*/SensorCall();fprintf(fd,"  JpegProc: %u\n",(unsigned int)sp->jpeg_proc);/*20*/}
	SensorCall();if (TIFFFieldSet(tif,FIELD_OJPEG_JPEGRESTARTINTERVAL))
		{/*21*/SensorCall();fprintf(fd,"  JpegRestartInterval: %u\n",(unsigned int)sp->restart_interval);/*22*/}
	SensorCall();if (sp->printdir)
		{/*23*/SensorCall();(*sp->printdir)(tif, fd, flags);/*24*/}
SensorCall();}

static int
OJPEGFixupTags(TIFF* tif)
{
	SensorCall();(void) tif;
	SensorCall();return(1);
}

static int
OJPEGSetupDecode(TIFF* tif)
{
	SensorCall();static const char module[]="OJPEGSetupDecode";
	TIFFWarningExt(tif->tif_clientdata,module,"Depreciated and troublesome old-style JPEG compression mode, please convert to new-style JPEG compression and notify vendor of writing software");
	SensorCall();return(1);
}

static int
OJPEGPreDecode(TIFF* tif, uint16 s)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint32 m;
	SensorCall();if (sp->subsamplingcorrect_done==0)
		{/*25*/SensorCall();OJPEGSubsamplingCorrect(tif);/*26*/}
	SensorCall();if (sp->readheader_done==0)
	{
		SensorCall();if (OJPEGReadHeaderInfo(tif)==0)
			{/*27*/SensorCall();return(0);/*28*/}
	}
	SensorCall();if (sp->sos_end[s].log==0)
	{
		SensorCall();if (OJPEGReadSecondarySos(tif,s)==0)
			{/*29*/SensorCall();return(0);/*30*/}
	}
	SensorCall();if isTiled(tif)
		{/*31*/SensorCall();m=tif->tif_curtile;/*32*/}
	else
		{/*33*/SensorCall();m=tif->tif_curstrip;/*34*/}
	SensorCall();if ((sp->writeheader_done!=0) && ((sp->write_cursample!=s) || (sp->write_curstrile>m)))
	{
		SensorCall();if (sp->libjpeg_session_active!=0)
			{/*35*/SensorCall();OJPEGLibjpegSessionAbort(tif);/*36*/}
		SensorCall();sp->writeheader_done=0;
	}
	SensorCall();if (sp->writeheader_done==0)
	{
		SensorCall();sp->plane_sample_offset=(uint8)s;
		sp->write_cursample=s;
		sp->write_curstrile=s*tif->tif_dir.td_stripsperimage;
		SensorCall();if ((sp->in_buffer_file_pos_log==0) ||
		    (sp->in_buffer_file_pos-sp->in_buffer_togo!=sp->sos_end[s].in_buffer_file_pos))
		{
			SensorCall();sp->in_buffer_source=sp->sos_end[s].in_buffer_source;
			sp->in_buffer_next_strile=sp->sos_end[s].in_buffer_next_strile;
			sp->in_buffer_file_pos=sp->sos_end[s].in_buffer_file_pos;
			sp->in_buffer_file_pos_log=0;
			sp->in_buffer_file_togo=sp->sos_end[s].in_buffer_file_togo;
			sp->in_buffer_togo=0;
			sp->in_buffer_cur=0;
		}
		SensorCall();if (OJPEGWriteHeaderInfo(tif)==0)
			{/*37*/SensorCall();return(0);/*38*/}
	}
	SensorCall();while (sp->write_curstrile<m)          
	{
		SensorCall();if (sp->libjpeg_jpeg_query_style==0)
		{
			SensorCall();if (OJPEGPreDecodeSkipRaw(tif)==0)
				{/*39*/SensorCall();return(0);/*40*/}
		}
		else
		{
			SensorCall();if (OJPEGPreDecodeSkipScanlines(tif)==0)
				{/*41*/SensorCall();return(0);/*42*/}
		}
		SensorCall();sp->write_curstrile++;
	}
	SensorCall();return(1);
}

static int
OJPEGPreDecodeSkipRaw(TIFF* tif)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint32 m;
	m=sp->lines_per_strile;
	SensorCall();if (sp->subsampling_convert_state!=0)
	{
		SensorCall();if (sp->subsampling_convert_clines-sp->subsampling_convert_state>=m)
		{
			SensorCall();sp->subsampling_convert_state+=m;
			SensorCall();if (sp->subsampling_convert_state==sp->subsampling_convert_clines)
				{/*43*/SensorCall();sp->subsampling_convert_state=0;/*44*/}
			SensorCall();return(1);
		}
		SensorCall();m-=sp->subsampling_convert_clines-sp->subsampling_convert_state;
		sp->subsampling_convert_state=0;
	}
	SensorCall();while (m>=sp->subsampling_convert_clines)
	{
		SensorCall();if (jpeg_read_raw_data_encap(sp,&(sp->libjpeg_jpeg_decompress_struct),sp->subsampling_convert_ycbcrimage,sp->subsampling_ver*8)==0)
			{/*45*/SensorCall();return(0);/*46*/}
		SensorCall();m-=sp->subsampling_convert_clines;
	}
	SensorCall();if (m>0)
	{
		SensorCall();if (jpeg_read_raw_data_encap(sp,&(sp->libjpeg_jpeg_decompress_struct),sp->subsampling_convert_ycbcrimage,sp->subsampling_ver*8)==0)
			{/*47*/SensorCall();return(0);/*48*/}
		SensorCall();sp->subsampling_convert_state=m;
	}
	SensorCall();return(1);
}

static int
OJPEGPreDecodeSkipScanlines(TIFF* tif)
{
	SensorCall();static const char module[]="OJPEGPreDecodeSkipScanlines";
	OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint32 m;
	SensorCall();if (sp->skip_buffer==NULL)
	{
		SensorCall();sp->skip_buffer=_TIFFmalloc(sp->bytes_per_line);
		SensorCall();if (sp->skip_buffer==NULL)
		{
			SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
			SensorCall();return(0);
		}
	}
	SensorCall();for (m=0; m<sp->lines_per_strile; m++)
	{
		SensorCall();if (jpeg_read_scanlines_encap(sp,&(sp->libjpeg_jpeg_decompress_struct),&sp->skip_buffer,1)==0)
			{/*49*/SensorCall();return(0);/*50*/}
	}
	SensorCall();return(1);
}

static int
OJPEGDecode(TIFF* tif, uint8* buf, tmsize_t cc, uint16 s)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	(void)s;
	SensorCall();if (sp->libjpeg_jpeg_query_style==0)
	{
		SensorCall();if (OJPEGDecodeRaw(tif,buf,cc)==0)
			{/*51*/SensorCall();return(0);/*52*/}
	}
	else
	{
		SensorCall();if (OJPEGDecodeScanlines(tif,buf,cc)==0)
			{/*53*/SensorCall();return(0);/*54*/}
	}
	SensorCall();return(1);
}

static int
OJPEGDecodeRaw(TIFF* tif, uint8* buf, tmsize_t cc)
{
	SensorCall();static const char module[]="OJPEGDecodeRaw";
	OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint8* m;
	tmsize_t n;
	uint8* oy;
	uint8* ocb;
	uint8* ocr;
	uint8* p;
	uint32 q;
	uint8* r;
	uint8 sx,sy;
	SensorCall();if (cc%sp->bytes_per_line!=0)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Fractional scanline not read");
		SensorCall();return(0);
	}
	assert(cc>0);
	SensorCall();m=buf;
	n=cc;
	SensorCall();do
	{
		SensorCall();if (sp->subsampling_convert_state==0)
		{
			SensorCall();if (jpeg_read_raw_data_encap(sp,&(sp->libjpeg_jpeg_decompress_struct),sp->subsampling_convert_ycbcrimage,sp->subsampling_ver*8)==0)
				{/*55*/SensorCall();return(0);/*56*/}
		}
		SensorCall();oy=sp->subsampling_convert_ybuf+sp->subsampling_convert_state*sp->subsampling_ver*sp->subsampling_convert_ylinelen;
		ocb=sp->subsampling_convert_cbbuf+sp->subsampling_convert_state*sp->subsampling_convert_clinelen;
		ocr=sp->subsampling_convert_crbuf+sp->subsampling_convert_state*sp->subsampling_convert_clinelen;
		p=m;
		SensorCall();for (q=0; q<sp->subsampling_convert_clinelenout; q++)
		{
			SensorCall();r=oy;
			SensorCall();for (sy=0; sy<sp->subsampling_ver; sy++)
			{
				SensorCall();for (sx=0; sx<sp->subsampling_hor; sx++)
					{/*57*/SensorCall();*p++=*r++;/*58*/}
				SensorCall();r+=sp->subsampling_convert_ylinelen-sp->subsampling_hor;
			}
			SensorCall();oy+=sp->subsampling_hor;
			*p++=*ocb++;
			*p++=*ocr++;
		}
		SensorCall();sp->subsampling_convert_state++;
		SensorCall();if (sp->subsampling_convert_state==sp->subsampling_convert_clines)
			{/*59*/SensorCall();sp->subsampling_convert_state=0;/*60*/}
		SensorCall();m+=sp->bytes_per_line;
		n-=sp->bytes_per_line;
	} while(n>0);
	SensorCall();return(1);
}

static int
OJPEGDecodeScanlines(TIFF* tif, uint8* buf, tmsize_t cc)
{
	SensorCall();static const char module[]="OJPEGDecodeScanlines";
	OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint8* m;
	tmsize_t n;
	SensorCall();if (cc%sp->bytes_per_line!=0)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Fractional scanline not read");
		SensorCall();return(0);
	}
	assert(cc>0);
	SensorCall();m=buf;
	n=cc;
	SensorCall();do
	{
		SensorCall();if (jpeg_read_scanlines_encap(sp,&(sp->libjpeg_jpeg_decompress_struct),&m,1)==0)
			{/*61*/SensorCall();return(0);/*62*/}
		SensorCall();m+=sp->bytes_per_line;
		n-=sp->bytes_per_line;
	} while(n>0);
	SensorCall();return(1);
}

static void
OJPEGPostDecode(TIFF* tif, uint8* buf, tmsize_t cc)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	(void)buf;
	(void)cc;
	sp->write_curstrile++;
	SensorCall();if (sp->write_curstrile%tif->tif_dir.td_stripsperimage==0)  
	{
		assert(sp->libjpeg_session_active!=0);
		SensorCall();OJPEGLibjpegSessionAbort(tif);
		sp->writeheader_done=0;
	}
SensorCall();}

static int
OJPEGSetupEncode(TIFF* tif)
{
	SensorCall();static const char module[]="OJPEGSetupEncode";
	TIFFErrorExt(tif->tif_clientdata,module,"OJPEG encoding not supported; use new-style JPEG compression instead");
	SensorCall();return(0);
}

static int
OJPEGPreEncode(TIFF* tif, uint16 s)
{
	SensorCall();static const char module[]="OJPEGPreEncode";
	(void)s;
	TIFFErrorExt(tif->tif_clientdata,module,"OJPEG encoding not supported; use new-style JPEG compression instead");
	SensorCall();return(0);
}

static int
OJPEGEncode(TIFF* tif, uint8* buf, tmsize_t cc, uint16 s)
{
	SensorCall();static const char module[]="OJPEGEncode";
	(void)buf;
	(void)cc;
	(void)s;
	TIFFErrorExt(tif->tif_clientdata,module,"OJPEG encoding not supported; use new-style JPEG compression instead");
	SensorCall();return(0);
}

static int
OJPEGPostEncode(TIFF* tif)
{
	SensorCall();static const char module[]="OJPEGPostEncode";
	TIFFErrorExt(tif->tif_clientdata,module,"OJPEG encoding not supported; use new-style JPEG compression instead");
	SensorCall();return(0);
}

static void
OJPEGCleanup(TIFF* tif)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	SensorCall();if (sp!=0)
	{
		SensorCall();tif->tif_tagmethods.vgetfield=sp->vgetparent;
		tif->tif_tagmethods.vsetfield=sp->vsetparent;
		tif->tif_tagmethods.printdir=sp->printdir;
		SensorCall();if (sp->qtable[0]!=0)
			{/*63*/SensorCall();_TIFFfree(sp->qtable[0]);/*64*/}
		SensorCall();if (sp->qtable[1]!=0)
			{/*65*/SensorCall();_TIFFfree(sp->qtable[1]);/*66*/}
		SensorCall();if (sp->qtable[2]!=0)
			{/*67*/SensorCall();_TIFFfree(sp->qtable[2]);/*68*/}
		SensorCall();if (sp->qtable[3]!=0)
			{/*69*/SensorCall();_TIFFfree(sp->qtable[3]);/*70*/}
		SensorCall();if (sp->dctable[0]!=0)
			{/*71*/SensorCall();_TIFFfree(sp->dctable[0]);/*72*/}
		SensorCall();if (sp->dctable[1]!=0)
			{/*73*/SensorCall();_TIFFfree(sp->dctable[1]);/*74*/}
		SensorCall();if (sp->dctable[2]!=0)
			{/*75*/SensorCall();_TIFFfree(sp->dctable[2]);/*76*/}
		SensorCall();if (sp->dctable[3]!=0)
			{/*77*/SensorCall();_TIFFfree(sp->dctable[3]);/*78*/}
		SensorCall();if (sp->actable[0]!=0)
			{/*79*/SensorCall();_TIFFfree(sp->actable[0]);/*80*/}
		SensorCall();if (sp->actable[1]!=0)
			{/*81*/SensorCall();_TIFFfree(sp->actable[1]);/*82*/}
		SensorCall();if (sp->actable[2]!=0)
			{/*83*/SensorCall();_TIFFfree(sp->actable[2]);/*84*/}
		SensorCall();if (sp->actable[3]!=0)
			{/*85*/SensorCall();_TIFFfree(sp->actable[3]);/*86*/}
		SensorCall();if (sp->libjpeg_session_active!=0)
			{/*87*/SensorCall();OJPEGLibjpegSessionAbort(tif);/*88*/}
		SensorCall();if (sp->subsampling_convert_ycbcrbuf!=0)
			{/*89*/SensorCall();_TIFFfree(sp->subsampling_convert_ycbcrbuf);/*90*/}
		SensorCall();if (sp->subsampling_convert_ycbcrimage!=0)
			{/*91*/SensorCall();_TIFFfree(sp->subsampling_convert_ycbcrimage);/*92*/}
		SensorCall();if (sp->skip_buffer!=0)
			{/*93*/SensorCall();_TIFFfree(sp->skip_buffer);/*94*/}
		SensorCall();_TIFFfree(sp);
		tif->tif_data=NULL;
		_TIFFSetDefaultCompressionState(tif);
	}
SensorCall();}

static void
OJPEGSubsamplingCorrect(TIFF* tif)
{
	SensorCall();static const char module[]="OJPEGSubsamplingCorrect";
	OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint8 mh;
	uint8 mv;
        _TIFFFillStriles( tif );
        
	assert(sp->subsamplingcorrect_done==0);
	SensorCall();if ((tif->tif_dir.td_samplesperpixel!=3) || ((tif->tif_dir.td_photometric!=PHOTOMETRIC_YCBCR) &&
	    (tif->tif_dir.td_photometric!=PHOTOMETRIC_ITULAB)))
	{
		SensorCall();if (sp->subsampling_tag!=0)
			{/*95*/SensorCall();TIFFWarningExt(tif->tif_clientdata,module,"Subsampling tag not appropriate for this Photometric and/or SamplesPerPixel");/*96*/}
		SensorCall();sp->subsampling_hor=1;
		sp->subsampling_ver=1;
		sp->subsampling_force_desubsampling_inside_decompression=0;
	}
	else
	{
		SensorCall();sp->subsamplingcorrect_done=1;
		mh=sp->subsampling_hor;
		mv=sp->subsampling_ver;
		sp->subsamplingcorrect=1;
		OJPEGReadHeaderInfoSec(tif);
		SensorCall();if (sp->subsampling_force_desubsampling_inside_decompression!=0)
		{
			SensorCall();sp->subsampling_hor=1;
			sp->subsampling_ver=1;
		}
		SensorCall();sp->subsamplingcorrect=0;
		SensorCall();if (((sp->subsampling_hor!=mh) || (sp->subsampling_ver!=mv)) && (sp->subsampling_force_desubsampling_inside_decompression==0))
		{
			SensorCall();if (sp->subsampling_tag==0)
				{/*97*/SensorCall();TIFFWarningExt(tif->tif_clientdata,module,"Subsampling tag is not set, yet subsampling inside JPEG data [%d,%d] does not match default values [2,2]; assuming subsampling inside JPEG data is correct",sp->subsampling_hor,sp->subsampling_ver);/*98*/}
			else
				{/*99*/SensorCall();TIFFWarningExt(tif->tif_clientdata,module,"Subsampling inside JPEG data [%d,%d] does not match subsampling tag values [%d,%d]; assuming subsampling inside JPEG data is correct",sp->subsampling_hor,sp->subsampling_ver,mh,mv);/*100*/}
		}
		SensorCall();if (sp->subsampling_force_desubsampling_inside_decompression!=0)
		{
			SensorCall();if (sp->subsampling_tag==0)
				{/*101*/SensorCall();TIFFWarningExt(tif->tif_clientdata,module,"Subsampling tag is not set, yet subsampling inside JPEG data does not match default values [2,2] (nor any other values allowed in TIFF); assuming subsampling inside JPEG data is correct and desubsampling inside JPEG decompression");/*102*/}
			else
				{/*103*/SensorCall();TIFFWarningExt(tif->tif_clientdata,module,"Subsampling inside JPEG data does not match subsampling tag values [%d,%d] (nor any other values allowed in TIFF); assuming subsampling inside JPEG data is correct and desubsampling inside JPEG decompression",mh,mv);/*104*/}
		}
		SensorCall();if (sp->subsampling_force_desubsampling_inside_decompression==0)
		{
			SensorCall();if (sp->subsampling_hor<sp->subsampling_ver)
				{/*105*/SensorCall();TIFFWarningExt(tif->tif_clientdata,module,"Subsampling values [%d,%d] are not allowed in TIFF",sp->subsampling_hor,sp->subsampling_ver);/*106*/}
		}
	}
	SensorCall();sp->subsamplingcorrect_done=1;
SensorCall();}

static int
OJPEGReadHeaderInfo(TIFF* tif)
{
	SensorCall();static const char module[]="OJPEGReadHeaderInfo";
	OJPEGState* sp=(OJPEGState*)tif->tif_data;
	assert(sp->readheader_done==0);
	sp->image_width=tif->tif_dir.td_imagewidth;
	sp->image_length=tif->tif_dir.td_imagelength;
	SensorCall();if isTiled(tif)
	{
		SensorCall();sp->strile_width=tif->tif_dir.td_tilewidth;
		sp->strile_length=tif->tif_dir.td_tilelength;
		sp->strile_length_total=((sp->image_length+sp->strile_length-1)/sp->strile_length)*sp->strile_length;
	}
	else
	{
		SensorCall();sp->strile_width=sp->image_width;
		sp->strile_length=tif->tif_dir.td_rowsperstrip;
		sp->strile_length_total=sp->image_length;
	}
	SensorCall();if (tif->tif_dir.td_samplesperpixel==1)
	{
		SensorCall();sp->samples_per_pixel=1;
		sp->plane_sample_offset=0;
		sp->samples_per_pixel_per_plane=sp->samples_per_pixel;
		sp->subsampling_hor=1;
		sp->subsampling_ver=1;
	}
	else
	{
		SensorCall();if (tif->tif_dir.td_samplesperpixel!=3)
		{
			SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"SamplesPerPixel %d not supported for this compression scheme",sp->samples_per_pixel);
			SensorCall();return(0);
		}
		SensorCall();sp->samples_per_pixel=3;
		sp->plane_sample_offset=0;
		SensorCall();if (tif->tif_dir.td_planarconfig==PLANARCONFIG_CONTIG)
			{/*107*/SensorCall();sp->samples_per_pixel_per_plane=3;/*108*/}
		else
			{/*109*/SensorCall();sp->samples_per_pixel_per_plane=1;/*110*/}
	}
	SensorCall();if (sp->strile_length<sp->image_length)
	{
		SensorCall();if (sp->strile_length%(sp->subsampling_ver*8)!=0)
		{
			SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Incompatible vertical subsampling and image strip/tile length");
			SensorCall();return(0);
		}
		SensorCall();sp->restart_interval=((sp->strile_width+sp->subsampling_hor*8-1)/(sp->subsampling_hor*8))*(sp->strile_length/(sp->subsampling_ver*8));
	}
	SensorCall();if (OJPEGReadHeaderInfoSec(tif)==0)
		{/*111*/SensorCall();return(0);/*112*/}
	SensorCall();sp->sos_end[0].log=1;
	sp->sos_end[0].in_buffer_source=sp->in_buffer_source;
	sp->sos_end[0].in_buffer_next_strile=sp->in_buffer_next_strile;
	sp->sos_end[0].in_buffer_file_pos=sp->in_buffer_file_pos-sp->in_buffer_togo;
	sp->sos_end[0].in_buffer_file_togo=sp->in_buffer_file_togo+sp->in_buffer_togo; 
	sp->readheader_done=1;
	SensorCall();return(1);
}

static int
OJPEGReadSecondarySos(TIFF* tif, uint16 s)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint8 m;
	assert(s>0);
	assert(s<3);
	assert(sp->sos_end[0].log!=0);
	assert(sp->sos_end[s].log==0);
	sp->plane_sample_offset=s-1;
	SensorCall();while(sp->sos_end[sp->plane_sample_offset].log==0)
		{/*113*/SensorCall();sp->plane_sample_offset--;/*114*/}
	SensorCall();sp->in_buffer_source=sp->sos_end[sp->plane_sample_offset].in_buffer_source;
	sp->in_buffer_next_strile=sp->sos_end[sp->plane_sample_offset].in_buffer_next_strile;
	sp->in_buffer_file_pos=sp->sos_end[sp->plane_sample_offset].in_buffer_file_pos;
	sp->in_buffer_file_pos_log=0;
	sp->in_buffer_file_togo=sp->sos_end[sp->plane_sample_offset].in_buffer_file_togo;
	sp->in_buffer_togo=0;
	sp->in_buffer_cur=0;
	SensorCall();while(sp->plane_sample_offset<s)
	{
		SensorCall();do
		{
			SensorCall();if (OJPEGReadByte(sp,&m)==0)
				{/*115*/SensorCall();return(0);/*116*/}
			SensorCall();if (m==255)
			{
				SensorCall();do
				{
					SensorCall();if (OJPEGReadByte(sp,&m)==0)
						{/*117*/SensorCall();return(0);/*118*/}
					SensorCall();if (m!=255)
						{/*119*/SensorCall();break;/*120*/}
				} while(1);
				SensorCall();if (m==JPEG_MARKER_SOS)
					{/*121*/SensorCall();break;/*122*/}
			}
		} while(1);
		SensorCall();sp->plane_sample_offset++;
		SensorCall();if (OJPEGReadHeaderInfoSecStreamSos(tif)==0)
			{/*123*/SensorCall();return(0);/*124*/}
		SensorCall();sp->sos_end[sp->plane_sample_offset].log=1;
		sp->sos_end[sp->plane_sample_offset].in_buffer_source=sp->in_buffer_source;
		sp->sos_end[sp->plane_sample_offset].in_buffer_next_strile=sp->in_buffer_next_strile;
		sp->sos_end[sp->plane_sample_offset].in_buffer_file_pos=sp->in_buffer_file_pos-sp->in_buffer_togo;
		sp->sos_end[sp->plane_sample_offset].in_buffer_file_togo=sp->in_buffer_file_togo+sp->in_buffer_togo;
	}
	SensorCall();return(1);
}

static int
OJPEGWriteHeaderInfo(TIFF* tif)
{
	SensorCall();static const char module[]="OJPEGWriteHeaderInfo";
	OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint8** m;
	uint32 n;
	/* if a previous attempt failed, don't try again */
	SensorCall();if (sp->libjpeg_session_active != 0) 
		{/*125*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*126*/}
	SensorCall();sp->out_state=ososSoi;
	sp->restart_index=0;
	jpeg_std_error(&(sp->libjpeg_jpeg_error_mgr));
	sp->libjpeg_jpeg_error_mgr.output_message=OJPEGLibjpegJpegErrorMgrOutputMessage;
	sp->libjpeg_jpeg_error_mgr.error_exit=OJPEGLibjpegJpegErrorMgrErrorExit;
	sp->libjpeg_jpeg_decompress_struct.err=&(sp->libjpeg_jpeg_error_mgr);
	sp->libjpeg_jpeg_decompress_struct.client_data=(void*)tif;
	SensorCall();if (jpeg_create_decompress_encap(sp,&(sp->libjpeg_jpeg_decompress_struct))==0)
		{/*127*/SensorCall();return(0);/*128*/}
	SensorCall();sp->libjpeg_session_active=1;
	sp->libjpeg_jpeg_source_mgr.bytes_in_buffer=0;
	sp->libjpeg_jpeg_source_mgr.init_source=OJPEGLibjpegJpegSourceMgrInitSource;
	sp->libjpeg_jpeg_source_mgr.fill_input_buffer=OJPEGLibjpegJpegSourceMgrFillInputBuffer;
	sp->libjpeg_jpeg_source_mgr.skip_input_data=OJPEGLibjpegJpegSourceMgrSkipInputData;
	sp->libjpeg_jpeg_source_mgr.resync_to_restart=OJPEGLibjpegJpegSourceMgrResyncToRestart;
	sp->libjpeg_jpeg_source_mgr.term_source=OJPEGLibjpegJpegSourceMgrTermSource;
	sp->libjpeg_jpeg_decompress_struct.src=&(sp->libjpeg_jpeg_source_mgr);
	SensorCall();if (jpeg_read_header_encap(sp,&(sp->libjpeg_jpeg_decompress_struct),1)==0)
		{/*129*/SensorCall();return(0);/*130*/}
	SensorCall();if ((sp->subsampling_force_desubsampling_inside_decompression==0) && (sp->samples_per_pixel_per_plane>1))
	{
		SensorCall();sp->libjpeg_jpeg_decompress_struct.raw_data_out=1;
#if JPEG_LIB_VERSION >= 70
		sp->libjpeg_jpeg_decompress_struct.do_fancy_upsampling=FALSE;
#endif
		sp->libjpeg_jpeg_query_style=0;
		SensorCall();if (sp->subsampling_convert_log==0)
		{
			assert(sp->subsampling_convert_ycbcrbuf==0);
			assert(sp->subsampling_convert_ycbcrimage==0);
			SensorCall();sp->subsampling_convert_ylinelen=((sp->strile_width+sp->subsampling_hor*8-1)/(sp->subsampling_hor*8)*sp->subsampling_hor*8);
			sp->subsampling_convert_ylines=sp->subsampling_ver*8;
			sp->subsampling_convert_clinelen=sp->subsampling_convert_ylinelen/sp->subsampling_hor;
			sp->subsampling_convert_clines=8;
			sp->subsampling_convert_ybuflen=sp->subsampling_convert_ylinelen*sp->subsampling_convert_ylines;
			sp->subsampling_convert_cbuflen=sp->subsampling_convert_clinelen*sp->subsampling_convert_clines;
			sp->subsampling_convert_ycbcrbuflen=sp->subsampling_convert_ybuflen+2*sp->subsampling_convert_cbuflen;
			sp->subsampling_convert_ycbcrbuf=_TIFFmalloc(sp->subsampling_convert_ycbcrbuflen);
			SensorCall();if (sp->subsampling_convert_ycbcrbuf==0)
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
				SensorCall();return(0);
			}
			SensorCall();sp->subsampling_convert_ybuf=sp->subsampling_convert_ycbcrbuf;
			sp->subsampling_convert_cbbuf=sp->subsampling_convert_ybuf+sp->subsampling_convert_ybuflen;
			sp->subsampling_convert_crbuf=sp->subsampling_convert_cbbuf+sp->subsampling_convert_cbuflen;
			sp->subsampling_convert_ycbcrimagelen=3+sp->subsampling_convert_ylines+2*sp->subsampling_convert_clines;
			sp->subsampling_convert_ycbcrimage=_TIFFmalloc(sp->subsampling_convert_ycbcrimagelen*sizeof(uint8*));
			SensorCall();if (sp->subsampling_convert_ycbcrimage==0)
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
				SensorCall();return(0);
			}
			SensorCall();m=sp->subsampling_convert_ycbcrimage;
			*m++=(uint8*)(sp->subsampling_convert_ycbcrimage+3);
			*m++=(uint8*)(sp->subsampling_convert_ycbcrimage+3+sp->subsampling_convert_ylines);
			*m++=(uint8*)(sp->subsampling_convert_ycbcrimage+3+sp->subsampling_convert_ylines+sp->subsampling_convert_clines);
			SensorCall();for (n=0; n<sp->subsampling_convert_ylines; n++)
				{/*131*/SensorCall();*m++=sp->subsampling_convert_ybuf+n*sp->subsampling_convert_ylinelen;/*132*/}
			SensorCall();for (n=0; n<sp->subsampling_convert_clines; n++)
				{/*133*/SensorCall();*m++=sp->subsampling_convert_cbbuf+n*sp->subsampling_convert_clinelen;/*134*/}
			SensorCall();for (n=0; n<sp->subsampling_convert_clines; n++)
				{/*135*/SensorCall();*m++=sp->subsampling_convert_crbuf+n*sp->subsampling_convert_clinelen;/*136*/}
			SensorCall();sp->subsampling_convert_clinelenout=((sp->strile_width+sp->subsampling_hor-1)/sp->subsampling_hor);
			sp->subsampling_convert_state=0;
			sp->bytes_per_line=sp->subsampling_convert_clinelenout*(sp->subsampling_ver*sp->subsampling_hor+2);
			sp->lines_per_strile=((sp->strile_length+sp->subsampling_ver-1)/sp->subsampling_ver);
			sp->subsampling_convert_log=1;
		}
	}
	else
	{
		SensorCall();sp->libjpeg_jpeg_decompress_struct.jpeg_color_space=JCS_UNKNOWN;
		sp->libjpeg_jpeg_decompress_struct.out_color_space=JCS_UNKNOWN;
		sp->libjpeg_jpeg_query_style=1;
		sp->bytes_per_line=sp->samples_per_pixel_per_plane*sp->strile_width;
		sp->lines_per_strile=sp->strile_length;
	}
	SensorCall();if (jpeg_start_decompress_encap(sp,&(sp->libjpeg_jpeg_decompress_struct))==0)
		{/*137*/SensorCall();return(0);/*138*/}
	SensorCall();sp->writeheader_done=1;
	SensorCall();return(1);
}

static void
OJPEGLibjpegSessionAbort(TIFF* tif)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	assert(sp->libjpeg_session_active!=0);
	jpeg_destroy((jpeg_common_struct*)(&(sp->libjpeg_jpeg_decompress_struct)));
	sp->libjpeg_session_active=0;
SensorCall();}

static int
OJPEGReadHeaderInfoSec(TIFF* tif)
{
	SensorCall();static const char module[]="OJPEGReadHeaderInfoSec";
	OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint8 m;
	uint16 n;
	uint8 o;
	SensorCall();if (sp->file_size==0)
		sp->file_size=TIFFGetFileSize(tif);
	SensorCall();if (sp->jpeg_interchange_format!=0)
	{
		SensorCall();if (sp->jpeg_interchange_format>=sp->file_size)
		{
			SensorCall();sp->jpeg_interchange_format=0;
			sp->jpeg_interchange_format_length=0;
		}
		else
		{
			SensorCall();if ((sp->jpeg_interchange_format_length==0) || (sp->jpeg_interchange_format+sp->jpeg_interchange_format_length>sp->file_size))
				{/*139*/SensorCall();sp->jpeg_interchange_format_length=sp->file_size-sp->jpeg_interchange_format;/*140*/}
		}
	}
	SensorCall();sp->in_buffer_source=osibsNotSetYet;
	sp->in_buffer_next_strile=0;
	sp->in_buffer_strile_count=tif->tif_dir.td_nstrips;
	sp->in_buffer_file_togo=0;
	sp->in_buffer_togo=0;
	SensorCall();do
	{
		SensorCall();if (OJPEGReadBytePeek(sp,&m)==0)
			{/*141*/SensorCall();return(0);/*142*/}
		SensorCall();if (m!=255)
			{/*143*/SensorCall();break;/*144*/}
		SensorCall();OJPEGReadByteAdvance(sp);
		SensorCall();do
		{
			SensorCall();if (OJPEGReadByte(sp,&m)==0)
				{/*145*/SensorCall();return(0);/*146*/}
		} while(m==255);
		SensorCall();switch(m)
		{
			case JPEG_MARKER_SOI:
				/* this type of marker has no data, and should be skipped */
				SensorCall();break;
			case JPEG_MARKER_COM:
			case JPEG_MARKER_APP0:
			case JPEG_MARKER_APP0+1:
			case JPEG_MARKER_APP0+2:
			case JPEG_MARKER_APP0+3:
			case JPEG_MARKER_APP0+4:
			case JPEG_MARKER_APP0+5:
			case JPEG_MARKER_APP0+6:
			case JPEG_MARKER_APP0+7:
			case JPEG_MARKER_APP0+8:
			case JPEG_MARKER_APP0+9:
			case JPEG_MARKER_APP0+10:
			case JPEG_MARKER_APP0+11:
			case JPEG_MARKER_APP0+12:
			case JPEG_MARKER_APP0+13:
			case JPEG_MARKER_APP0+14:
			case JPEG_MARKER_APP0+15:
				/* this type of marker has data, but it has no use to us (and no place here) and should be skipped */
				SensorCall();if (OJPEGReadWord(sp,&n)==0)
					{/*147*/SensorCall();return(0);/*148*/}
				SensorCall();if (n<2)
				{
					SensorCall();if (sp->subsamplingcorrect==0)
						{/*149*/SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Corrupt JPEG data");/*150*/}
					SensorCall();return(0);
				}
				SensorCall();if (n>2)
					{/*151*/SensorCall();OJPEGReadSkip(sp,n-2);/*152*/}
				SensorCall();break;
			case JPEG_MARKER_DRI:
				SensorCall();if (OJPEGReadHeaderInfoSecStreamDri(tif)==0)
					{/*153*/SensorCall();return(0);/*154*/}
				SensorCall();break;
			case JPEG_MARKER_DQT:
				SensorCall();if (OJPEGReadHeaderInfoSecStreamDqt(tif)==0)
					{/*155*/SensorCall();return(0);/*156*/}
				SensorCall();break;
			case JPEG_MARKER_DHT:
				SensorCall();if (OJPEGReadHeaderInfoSecStreamDht(tif)==0)
					{/*157*/SensorCall();return(0);/*158*/}
				SensorCall();break;
			case JPEG_MARKER_SOF0:
			case JPEG_MARKER_SOF1:
			case JPEG_MARKER_SOF3:
				SensorCall();if (OJPEGReadHeaderInfoSecStreamSof(tif,m)==0)
					{/*159*/SensorCall();return(0);/*160*/}
				SensorCall();if (sp->subsamplingcorrect!=0)
					{/*161*/SensorCall();return(1);/*162*/}
				SensorCall();break;
			case JPEG_MARKER_SOS:
				SensorCall();if (sp->subsamplingcorrect!=0)
					{/*163*/SensorCall();return(1);/*164*/}
				assert(sp->plane_sample_offset==0);
				SensorCall();if (OJPEGReadHeaderInfoSecStreamSos(tif)==0)
					{/*165*/SensorCall();return(0);/*166*/}
				SensorCall();break;
			default:
				SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Unknown marker type %d in JPEG data",m);
				SensorCall();return(0);
		}
	} while(m!=JPEG_MARKER_SOS);
	SensorCall();if (sp->subsamplingcorrect)
		{/*167*/SensorCall();return(1);/*168*/}
	SensorCall();if (sp->sof_log==0)
	{
		SensorCall();if (OJPEGReadHeaderInfoSecTablesQTable(tif)==0)
			{/*169*/SensorCall();return(0);/*170*/}
		SensorCall();sp->sof_marker_id=JPEG_MARKER_SOF0;
		SensorCall();for (o=0; o<sp->samples_per_pixel; o++)
			{/*171*/SensorCall();sp->sof_c[o]=o;/*172*/}
		SensorCall();sp->sof_hv[0]=((sp->subsampling_hor<<4)|sp->subsampling_ver);
		SensorCall();for (o=1; o<sp->samples_per_pixel; o++)
			{/*173*/SensorCall();sp->sof_hv[o]=17;/*174*/}
		SensorCall();sp->sof_x=sp->strile_width;
		sp->sof_y=sp->strile_length_total;
		sp->sof_log=1;
		SensorCall();if (OJPEGReadHeaderInfoSecTablesDcTable(tif)==0)
			{/*175*/SensorCall();return(0);/*176*/}
		SensorCall();if (OJPEGReadHeaderInfoSecTablesAcTable(tif)==0)
			{/*177*/SensorCall();return(0);/*178*/}
		SensorCall();for (o=1; o<sp->samples_per_pixel; o++)
			{/*179*/SensorCall();sp->sos_cs[o]=o;/*180*/}
	}
	SensorCall();return(1);
}

static int
OJPEGReadHeaderInfoSecStreamDri(TIFF* tif)
{
	/* this could easilly cause trouble in some cases... but no such cases have occured sofar */
	SensorCall();static const char module[]="OJPEGReadHeaderInfoSecStreamDri";
	OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint16 m;
	SensorCall();if (OJPEGReadWord(sp,&m)==0)
		{/*181*/SensorCall();return(0);/*182*/}
	SensorCall();if (m!=4)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Corrupt DRI marker in JPEG data");
		SensorCall();return(0);
	}
	SensorCall();if (OJPEGReadWord(sp,&m)==0)
		{/*183*/SensorCall();return(0);/*184*/}
	SensorCall();sp->restart_interval=m;
	SensorCall();return(1);
}

static int
OJPEGReadHeaderInfoSecStreamDqt(TIFF* tif)
{
	/* this is a table marker, and it is to be saved as a whole for exact pushing on the jpeg stream later on */
	SensorCall();static const char module[]="OJPEGReadHeaderInfoSecStreamDqt";
	OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint16 m;
	uint32 na;
	uint8* nb;
	uint8 o;
	SensorCall();if (OJPEGReadWord(sp,&m)==0)
		{/*185*/SensorCall();return(0);/*186*/}
	SensorCall();if (m<=2)
	{
		SensorCall();if (sp->subsamplingcorrect==0)
			{/*187*/SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Corrupt DQT marker in JPEG data");/*188*/}
		SensorCall();return(0);
	}
	SensorCall();if (sp->subsamplingcorrect!=0)
		{/*189*/SensorCall();OJPEGReadSkip(sp,m-2);/*190*/}
	else
	{
		SensorCall();m-=2;
		SensorCall();do
		{
			SensorCall();if (m<65)
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Corrupt DQT marker in JPEG data");
				SensorCall();return(0);
			}
			SensorCall();na=sizeof(uint32)+69;
			nb=_TIFFmalloc(na);
			SensorCall();if (nb==0)
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
				SensorCall();return(0);
			}
			SensorCall();*(uint32*)nb=na;
			nb[sizeof(uint32)]=255;
			nb[sizeof(uint32)+1]=JPEG_MARKER_DQT;
			nb[sizeof(uint32)+2]=0;
			nb[sizeof(uint32)+3]=67;
			SensorCall();if (OJPEGReadBlock(sp,65,&nb[sizeof(uint32)+4])==0) {
				SensorCall();_TIFFfree(nb);
				SensorCall();return(0);
			}
			SensorCall();o=nb[sizeof(uint32)+4]&15;
			SensorCall();if (3<o)
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Corrupt DQT marker in JPEG data");
				_TIFFfree(nb);
				SensorCall();return(0);
			}
			SensorCall();if (sp->qtable[o]!=0)
				{/*191*/SensorCall();_TIFFfree(sp->qtable[o]);/*192*/}
			SensorCall();sp->qtable[o]=nb;
			m-=65;
		} while(m>0);
	}
	SensorCall();return(1);
}

static int
OJPEGReadHeaderInfoSecStreamDht(TIFF* tif)
{
	/* this is a table marker, and it is to be saved as a whole for exact pushing on the jpeg stream later on */
	/* TODO: the following assumes there is only one table in this marker... but i'm not quite sure that assumption is guaranteed correct */
	SensorCall();static const char module[]="OJPEGReadHeaderInfoSecStreamDht";
	OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint16 m;
	uint32 na;
	uint8* nb;
	uint8 o;
	SensorCall();if (OJPEGReadWord(sp,&m)==0)
		{/*193*/SensorCall();return(0);/*194*/}
	SensorCall();if (m<=2)
	{
		SensorCall();if (sp->subsamplingcorrect==0)
			{/*195*/SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Corrupt DHT marker in JPEG data");/*196*/}
		SensorCall();return(0);
	}
	SensorCall();if (sp->subsamplingcorrect!=0)
	{
		SensorCall();OJPEGReadSkip(sp,m-2);
	}
	else
	{
		SensorCall();na=sizeof(uint32)+2+m;
		nb=_TIFFmalloc(na);
		SensorCall();if (nb==0)
		{
			SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
			SensorCall();return(0);
		}
		SensorCall();*(uint32*)nb=na;
		nb[sizeof(uint32)]=255;
		nb[sizeof(uint32)+1]=JPEG_MARKER_DHT;
		nb[sizeof(uint32)+2]=(m>>8);
		nb[sizeof(uint32)+3]=(m&255);
		SensorCall();if (OJPEGReadBlock(sp,m-2,&nb[sizeof(uint32)+4])==0)
			{/*197*/SensorCall();return(0);/*198*/}
		SensorCall();o=nb[sizeof(uint32)+4];
		SensorCall();if ((o&240)==0)
		{
			SensorCall();if (3<o)
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Corrupt DHT marker in JPEG data");
				SensorCall();return(0);
			}
			SensorCall();if (sp->dctable[o]!=0)
				{/*199*/SensorCall();_TIFFfree(sp->dctable[o]);/*200*/}
			SensorCall();sp->dctable[o]=nb;
		}
		else
		{
			SensorCall();if ((o&240)!=16)
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Corrupt DHT marker in JPEG data");
				SensorCall();return(0);
			}
			SensorCall();o&=15;
			SensorCall();if (3<o)
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Corrupt DHT marker in JPEG data");
				SensorCall();return(0);
			}
			SensorCall();if (sp->actable[o]!=0)
				{/*201*/SensorCall();_TIFFfree(sp->actable[o]);/*202*/}
			SensorCall();sp->actable[o]=nb;
		}
	}
	SensorCall();return(1);
}

static int
OJPEGReadHeaderInfoSecStreamSof(TIFF* tif, uint8 marker_id)
{
	/* this marker needs to be checked, and part of its data needs to be saved for regeneration later on */
	SensorCall();static const char module[]="OJPEGReadHeaderInfoSecStreamSof";
	OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint16 m;
	uint16 n;
	uint8 o;
	uint16 p;
	uint16 q;
	SensorCall();if (sp->sof_log!=0)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Corrupt JPEG data");
		SensorCall();return(0);
	}
	SensorCall();if (sp->subsamplingcorrect==0)
		{/*203*/SensorCall();sp->sof_marker_id=marker_id;/*204*/}
	/* Lf: data length */
	SensorCall();if (OJPEGReadWord(sp,&m)==0)
		{/*205*/SensorCall();return(0);/*206*/}
	SensorCall();if (m<11)
	{
		SensorCall();if (sp->subsamplingcorrect==0)
			{/*207*/SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Corrupt SOF marker in JPEG data");/*208*/}
		SensorCall();return(0);
	}
	SensorCall();m-=8;
	SensorCall();if (m%3!=0)
	{
		SensorCall();if (sp->subsamplingcorrect==0)
			{/*209*/SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Corrupt SOF marker in JPEG data");/*210*/}
		SensorCall();return(0);
	}
	SensorCall();n=m/3;
	SensorCall();if (sp->subsamplingcorrect==0)
	{
		SensorCall();if (n!=sp->samples_per_pixel)
		{
			SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"JPEG compressed data indicates unexpected number of samples");
			SensorCall();return(0);
		}
	}
	/* P: Sample precision */
	SensorCall();if (OJPEGReadByte(sp,&o)==0)
		{/*211*/SensorCall();return(0);/*212*/}
	SensorCall();if (o!=8)
	{
		SensorCall();if (sp->subsamplingcorrect==0)
			{/*213*/SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"JPEG compressed data indicates unexpected number of bits per sample");/*214*/}
		SensorCall();return(0);
	}
	/* Y: Number of lines, X: Number of samples per line */
	SensorCall();if (sp->subsamplingcorrect)
		{/*215*/SensorCall();OJPEGReadSkip(sp,4);/*216*/}
	else
	{
		/* Y: Number of lines */
		SensorCall();if (OJPEGReadWord(sp,&p)==0)
			{/*217*/SensorCall();return(0);/*218*/}
		SensorCall();if (((uint32)p<sp->image_length) && ((uint32)p<sp->strile_length_total))
		{
			SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"JPEG compressed data indicates unexpected height");
			SensorCall();return(0);
		}
		SensorCall();sp->sof_y=p;
		/* X: Number of samples per line */
		SensorCall();if (OJPEGReadWord(sp,&p)==0)
			{/*219*/SensorCall();return(0);/*220*/}
		SensorCall();if (((uint32)p<sp->image_width) && ((uint32)p<sp->strile_width))
		{
			SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"JPEG compressed data indicates unexpected width");
			SensorCall();return(0);
		}
		SensorCall();if ((uint32)p>sp->strile_width)
		{
			SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"JPEG compressed data image width exceeds expected image width");
			SensorCall();return(0);
		}
		SensorCall();sp->sof_x=p;
	}
	/* Nf: Number of image components in frame */
	SensorCall();if (OJPEGReadByte(sp,&o)==0)
		{/*221*/SensorCall();return(0);/*222*/}
	SensorCall();if (o!=n)
	{
		SensorCall();if (sp->subsamplingcorrect==0)
			{/*223*/SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Corrupt SOF marker in JPEG data");/*224*/}
		SensorCall();return(0);
	}
	/* per component stuff */
	/* TODO: double-check that flow implies that n cannot be as big as to make us overflow sof_c, sof_hv and sof_tq arrays */
	SensorCall();for (q=0; q<n; q++)
	{
		/* C: Component identifier */
		SensorCall();if (OJPEGReadByte(sp,&o)==0)
			{/*225*/SensorCall();return(0);/*226*/}
		SensorCall();if (sp->subsamplingcorrect==0)
			{/*227*/SensorCall();sp->sof_c[q]=o;/*228*/}
		/* H: Horizontal sampling factor, and V: Vertical sampling factor */
		SensorCall();if (OJPEGReadByte(sp,&o)==0)
			{/*229*/SensorCall();return(0);/*230*/}
		SensorCall();if (sp->subsamplingcorrect!=0)
		{
			SensorCall();if (q==0)
			{
				SensorCall();sp->subsampling_hor=(o>>4);
				sp->subsampling_ver=(o&15);
				SensorCall();if (((sp->subsampling_hor!=1) && (sp->subsampling_hor!=2) && (sp->subsampling_hor!=4)) ||
					((sp->subsampling_ver!=1) && (sp->subsampling_ver!=2) && (sp->subsampling_ver!=4)))
					{/*231*/SensorCall();sp->subsampling_force_desubsampling_inside_decompression=1;/*232*/}
			}
			else
			{
				SensorCall();if (o!=17)
					{/*233*/SensorCall();sp->subsampling_force_desubsampling_inside_decompression=1;/*234*/}
			}
		}
		else
		{
			SensorCall();sp->sof_hv[q]=o;
			SensorCall();if (sp->subsampling_force_desubsampling_inside_decompression==0)
			{
				SensorCall();if (q==0)
				{
					SensorCall();if (o!=((sp->subsampling_hor<<4)|sp->subsampling_ver))
					{
						SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"JPEG compressed data indicates unexpected subsampling values");
						SensorCall();return(0);
					}
				}
				else
				{
					SensorCall();if (o!=17)
					{
						SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"JPEG compressed data indicates unexpected subsampling values");
						SensorCall();return(0);
					}
				}
			}
		}
		/* Tq: Quantization table destination selector */
		SensorCall();if (OJPEGReadByte(sp,&o)==0)
			{/*235*/SensorCall();return(0);/*236*/}
		SensorCall();if (sp->subsamplingcorrect==0)
			{/*237*/SensorCall();sp->sof_tq[q]=o;/*238*/}
	}
	SensorCall();if (sp->subsamplingcorrect==0)
		{/*239*/SensorCall();sp->sof_log=1;/*240*/}
	SensorCall();return(1);
}

static int
OJPEGReadHeaderInfoSecStreamSos(TIFF* tif)
{
	/* this marker needs to be checked, and part of its data needs to be saved for regeneration later on */
	SensorCall();static const char module[]="OJPEGReadHeaderInfoSecStreamSos";
	OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint16 m;
	uint8 n;
	uint8 o;
	assert(sp->subsamplingcorrect==0);
	SensorCall();if (sp->sof_log==0)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Corrupt SOS marker in JPEG data");
		SensorCall();return(0);
	}
	/* Ls */
	SensorCall();if (OJPEGReadWord(sp,&m)==0)
		{/*241*/SensorCall();return(0);/*242*/}
	SensorCall();if (m!=6+sp->samples_per_pixel_per_plane*2)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Corrupt SOS marker in JPEG data");
		SensorCall();return(0);
	}
	/* Ns */
	SensorCall();if (OJPEGReadByte(sp,&n)==0)
		{/*243*/SensorCall();return(0);/*244*/}
	SensorCall();if (n!=sp->samples_per_pixel_per_plane)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Corrupt SOS marker in JPEG data");
		SensorCall();return(0);
	}
	/* Cs, Td, and Ta */
	SensorCall();for (o=0; o<sp->samples_per_pixel_per_plane; o++)
	{
		/* Cs */
		SensorCall();if (OJPEGReadByte(sp,&n)==0)
			{/*245*/SensorCall();return(0);/*246*/}
		SensorCall();sp->sos_cs[sp->plane_sample_offset+o]=n;
		/* Td and Ta */
		SensorCall();if (OJPEGReadByte(sp,&n)==0)
			{/*247*/SensorCall();return(0);/*248*/}
		SensorCall();sp->sos_tda[sp->plane_sample_offset+o]=n;
	}
	/* skip Ss, Se, Ah, en Al -> no check, as per Tom Lane recommendation, as per LibJpeg source */
	SensorCall();OJPEGReadSkip(sp,3);
	SensorCall();return(1);
}

static int
OJPEGReadHeaderInfoSecTablesQTable(TIFF* tif)
{
	SensorCall();static const char module[]="OJPEGReadHeaderInfoSecTablesQTable";
	OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint8 m;
	uint8 n;
	uint32 oa;
	uint8* ob;
	uint32 p;
	SensorCall();if (sp->qtable_offset[0]==0)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Missing JPEG tables");
		SensorCall();return(0);
	}
	SensorCall();sp->in_buffer_file_pos_log=0;
	SensorCall();for (m=0; m<sp->samples_per_pixel; m++)
	{
		SensorCall();if ((sp->qtable_offset[m]!=0) && ((m==0) || (sp->qtable_offset[m]!=sp->qtable_offset[m-1])))
		{
			SensorCall();for (n=0; n<m-1; n++)
			{
				SensorCall();if (sp->qtable_offset[m]==sp->qtable_offset[n])
				{
					SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Corrupt JpegQTables tag value");
					SensorCall();return(0);
				}
			}
			SensorCall();oa=sizeof(uint32)+69;
			ob=_TIFFmalloc(oa);
			SensorCall();if (ob==0)
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
				SensorCall();return(0);
			}
			SensorCall();*(uint32*)ob=oa;
			ob[sizeof(uint32)]=255;
			ob[sizeof(uint32)+1]=JPEG_MARKER_DQT;
			ob[sizeof(uint32)+2]=0;
			ob[sizeof(uint32)+3]=67;
			ob[sizeof(uint32)+4]=m;
			TIFFSeekFile(tif,sp->qtable_offset[m],SEEK_SET); 
			p=TIFFReadFile(tif,&ob[sizeof(uint32)+5],64);
			SensorCall();if (p!=64)
				{/*249*/SensorCall();return(0);/*250*/}
			SensorCall();sp->qtable[m]=ob;
			sp->sof_tq[m]=m;
		}
		else
			{/*251*/SensorCall();sp->sof_tq[m]=sp->sof_tq[m-1];/*252*/}
	}
	SensorCall();return(1);
}

static int
OJPEGReadHeaderInfoSecTablesDcTable(TIFF* tif)
{
	SensorCall();static const char module[]="OJPEGReadHeaderInfoSecTablesDcTable";
	OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint8 m;
	uint8 n;
	uint8 o[16];
	uint32 p;
	uint32 q;
	uint32 ra;
	uint8* rb;
	SensorCall();if (sp->dctable_offset[0]==0)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Missing JPEG tables");
		SensorCall();return(0);
	}
	SensorCall();sp->in_buffer_file_pos_log=0;
	SensorCall();for (m=0; m<sp->samples_per_pixel; m++)
	{
		SensorCall();if ((sp->dctable_offset[m]!=0) && ((m==0) || (sp->dctable_offset[m]!=sp->dctable_offset[m-1])))
		{
			SensorCall();for (n=0; n<m-1; n++)
			{
				SensorCall();if (sp->dctable_offset[m]==sp->dctable_offset[n])
				{
					SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Corrupt JpegDcTables tag value");
					SensorCall();return(0);
				}
			}
			TIFFSeekFile(tif,sp->dctable_offset[m],SEEK_SET);
			SensorCall();p=TIFFReadFile(tif,o,16);
			SensorCall();if (p!=16)
				{/*253*/SensorCall();return(0);/*254*/}
			SensorCall();q=0;
			SensorCall();for (n=0; n<16; n++)
				{/*255*/SensorCall();q+=o[n];/*256*/}
			SensorCall();ra=sizeof(uint32)+21+q;
			rb=_TIFFmalloc(ra);
			SensorCall();if (rb==0)
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
				SensorCall();return(0);
			}
			SensorCall();*(uint32*)rb=ra;
			rb[sizeof(uint32)]=255;
			rb[sizeof(uint32)+1]=JPEG_MARKER_DHT;
			rb[sizeof(uint32)+2]=((19+q)>>8);
			rb[sizeof(uint32)+3]=((19+q)&255);
			rb[sizeof(uint32)+4]=m;
			SensorCall();for (n=0; n<16; n++)
				{/*257*/SensorCall();rb[sizeof(uint32)+5+n]=o[n];/*258*/}
			SensorCall();p=TIFFReadFile(tif,&(rb[sizeof(uint32)+21]),q);
			SensorCall();if (p!=q)
				{/*259*/SensorCall();return(0);/*260*/}
			SensorCall();sp->dctable[m]=rb;
			sp->sos_tda[m]=(m<<4);
		}
		else
			{/*261*/SensorCall();sp->sos_tda[m]=sp->sos_tda[m-1];/*262*/}
	}
	SensorCall();return(1);
}

static int
OJPEGReadHeaderInfoSecTablesAcTable(TIFF* tif)
{
	SensorCall();static const char module[]="OJPEGReadHeaderInfoSecTablesAcTable";
	OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint8 m;
	uint8 n;
	uint8 o[16];
	uint32 p;
	uint32 q;
	uint32 ra;
	uint8* rb;
	SensorCall();if (sp->actable_offset[0]==0)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Missing JPEG tables");
		SensorCall();return(0);
	}
	SensorCall();sp->in_buffer_file_pos_log=0;
	SensorCall();for (m=0; m<sp->samples_per_pixel; m++)
	{
		SensorCall();if ((sp->actable_offset[m]!=0) && ((m==0) || (sp->actable_offset[m]!=sp->actable_offset[m-1])))
		{
			SensorCall();for (n=0; n<m-1; n++)
			{
				SensorCall();if (sp->actable_offset[m]==sp->actable_offset[n])
				{
					SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Corrupt JpegAcTables tag value");
					SensorCall();return(0);
				}
			}
			TIFFSeekFile(tif,sp->actable_offset[m],SEEK_SET);  
			SensorCall();p=TIFFReadFile(tif,o,16);
			SensorCall();if (p!=16)
				{/*263*/SensorCall();return(0);/*264*/}
			SensorCall();q=0;
			SensorCall();for (n=0; n<16; n++)
				{/*265*/SensorCall();q+=o[n];/*266*/}
			SensorCall();ra=sizeof(uint32)+21+q;
			rb=_TIFFmalloc(ra);
			SensorCall();if (rb==0)
			{
				SensorCall();TIFFErrorExt(tif->tif_clientdata,module,"Out of memory");
				SensorCall();return(0);
			}
			SensorCall();*(uint32*)rb=ra;
			rb[sizeof(uint32)]=255;
			rb[sizeof(uint32)+1]=JPEG_MARKER_DHT;
			rb[sizeof(uint32)+2]=((19+q)>>8);
			rb[sizeof(uint32)+3]=((19+q)&255);
			rb[sizeof(uint32)+4]=(16|m);
			SensorCall();for (n=0; n<16; n++)
				{/*267*/SensorCall();rb[sizeof(uint32)+5+n]=o[n];/*268*/}
			SensorCall();p=TIFFReadFile(tif,&(rb[sizeof(uint32)+21]),q);
			SensorCall();if (p!=q)
				{/*269*/SensorCall();return(0);/*270*/}
			SensorCall();sp->actable[m]=rb;
			sp->sos_tda[m]=(sp->sos_tda[m]|m);
		}
		else
			{/*271*/SensorCall();sp->sos_tda[m]=(sp->sos_tda[m]|(sp->sos_tda[m-1]&15));/*272*/}
	}
	SensorCall();return(1);
}

static int
OJPEGReadBufferFill(OJPEGState* sp)
{
	SensorCall();uint16 m;
	tmsize_t n;
	/* TODO: double-check: when subsamplingcorrect is set, no call to TIFFErrorExt or TIFFWarningExt should be made
	 * in any other case, seek or read errors should be passed through */
	SensorCall();do
	{
		SensorCall();if (sp->in_buffer_file_togo!=0)
		{
			SensorCall();if (sp->in_buffer_file_pos_log==0)
			{
				TIFFSeekFile(sp->tif,sp->in_buffer_file_pos,SEEK_SET);
				SensorCall();sp->in_buffer_file_pos_log=1;
			}
			SensorCall();m=OJPEG_BUFFER;
			SensorCall();if ((uint64)m>sp->in_buffer_file_togo)
				{/*273*/SensorCall();m=(uint16)sp->in_buffer_file_togo;/*274*/}
			SensorCall();n=TIFFReadFile(sp->tif,sp->in_buffer,(tmsize_t)m);
			SensorCall();if (n==0)
				{/*275*/SensorCall();return(0);/*276*/}
			assert(n>0);
			assert(n<=OJPEG_BUFFER);
			assert(n<65536);
			assert((uint64)n<=sp->in_buffer_file_togo);
			SensorCall();m=(uint16)n;
			sp->in_buffer_togo=m;
			sp->in_buffer_cur=sp->in_buffer;
			sp->in_buffer_file_togo-=m;
			sp->in_buffer_file_pos+=m;
			SensorCall();break;
		}
		SensorCall();sp->in_buffer_file_pos_log=0;
		SensorCall();switch(sp->in_buffer_source)
		{
			case osibsNotSetYet:
				SensorCall();if (sp->jpeg_interchange_format!=0)
				{
					SensorCall();sp->in_buffer_file_pos=sp->jpeg_interchange_format;
					sp->in_buffer_file_togo=sp->jpeg_interchange_format_length;
				}
				SensorCall();sp->in_buffer_source=osibsJpegInterchangeFormat;
				SensorCall();break;
			case osibsJpegInterchangeFormat:
				SensorCall();sp->in_buffer_source=osibsStrile;
			case osibsStrile:
				SensorCall();if (!_TIFFFillStriles( sp->tif ) 
				    || sp->tif->tif_dir.td_stripoffset == NULL
				    || sp->tif->tif_dir.td_stripbytecount == NULL)
					{/*277*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*278*/}

				SensorCall();if (sp->in_buffer_next_strile==sp->in_buffer_strile_count)
					{/*279*/SensorCall();sp->in_buffer_source=osibsEof;/*280*/}
				else
				{
					SensorCall();sp->in_buffer_file_pos=sp->tif->tif_dir.td_stripoffset[sp->in_buffer_next_strile];
					SensorCall();if (sp->in_buffer_file_pos!=0)
					{
						SensorCall();if (sp->in_buffer_file_pos>=sp->file_size)
							{/*281*/SensorCall();sp->in_buffer_file_pos=0;/*282*/}
						else {/*283*/SensorCall();if (sp->tif->tif_dir.td_stripbytecount==NULL)
							{/*285*/SensorCall();sp->in_buffer_file_togo=sp->file_size-sp->in_buffer_file_pos;/*286*/}
						else
						{
							SensorCall();if (sp->tif->tif_dir.td_stripbytecount == 0) {
								SensorCall();TIFFErrorExt(sp->tif->tif_clientdata,sp->tif->tif_name,"Strip byte counts are missing");
								SensorCall();return(0);
							}
							SensorCall();sp->in_buffer_file_togo=sp->tif->tif_dir.td_stripbytecount[sp->in_buffer_next_strile];
							SensorCall();if (sp->in_buffer_file_togo==0)
								{/*287*/SensorCall();sp->in_buffer_file_pos=0;/*288*/}
							else {/*289*/SensorCall();if (sp->in_buffer_file_pos+sp->in_buffer_file_togo>sp->file_size)
								{/*291*/SensorCall();sp->in_buffer_file_togo=sp->file_size-sp->in_buffer_file_pos;/*292*/}/*290*/}
						;/*284*/}}
					}
					SensorCall();sp->in_buffer_next_strile++;
				}
				SensorCall();break;
			default:
				SensorCall();return(0);
		}
	} while (1);
	SensorCall();return(1);
}

static int
OJPEGReadByte(OJPEGState* sp, uint8* byte)
{
	SensorCall();if (sp->in_buffer_togo==0)
	{
		SensorCall();if (OJPEGReadBufferFill(sp)==0)
			{/*293*/SensorCall();return(0);/*294*/}
		assert(sp->in_buffer_togo>0);
	}
	SensorCall();*byte=*(sp->in_buffer_cur);
	sp->in_buffer_cur++;
	sp->in_buffer_togo--;
	SensorCall();return(1);
}

static int
OJPEGReadBytePeek(OJPEGState* sp, uint8* byte)
{
	SensorCall();if (sp->in_buffer_togo==0)
	{
		SensorCall();if (OJPEGReadBufferFill(sp)==0)
			{/*295*/SensorCall();return(0);/*296*/}
		assert(sp->in_buffer_togo>0);
	}
	SensorCall();*byte=*(sp->in_buffer_cur);
	SensorCall();return(1);
}

static void
OJPEGReadByteAdvance(OJPEGState* sp)
{
SensorCall();	assert(sp->in_buffer_togo>0);
	sp->in_buffer_cur++;
	sp->in_buffer_togo--;
SensorCall();}

static int
OJPEGReadWord(OJPEGState* sp, uint16* word)
{
	SensorCall();uint8 m;
	SensorCall();if (OJPEGReadByte(sp,&m)==0)
		{/*297*/SensorCall();return(0);/*298*/}
	SensorCall();*word=(m<<8);
	SensorCall();if (OJPEGReadByte(sp,&m)==0)
		{/*299*/SensorCall();return(0);/*300*/}
	SensorCall();*word|=m;
	SensorCall();return(1);
}

static int
OJPEGReadBlock(OJPEGState* sp, uint16 len, void* mem)
{
	SensorCall();uint16 mlen;
	uint8* mmem;
	uint16 n;
	assert(len>0);
	mlen=len;
	mmem=mem;
	SensorCall();do
	{
		SensorCall();if (sp->in_buffer_togo==0)
		{
			SensorCall();if (OJPEGReadBufferFill(sp)==0)
				{/*301*/SensorCall();return(0);/*302*/}
			assert(sp->in_buffer_togo>0);
		}
		SensorCall();n=mlen;
		SensorCall();if (n>sp->in_buffer_togo)
			{/*303*/SensorCall();n=sp->in_buffer_togo;/*304*/}
		SensorCall();_TIFFmemcpy(mmem,sp->in_buffer_cur,n);
		sp->in_buffer_cur+=n;
		sp->in_buffer_togo-=n;
		mlen-=n;
		mmem+=n;
	} while(mlen>0);
	SensorCall();return(1);
}

static void
OJPEGReadSkip(OJPEGState* sp, uint16 len)
{
	SensorCall();uint16 m;
	uint16 n;
	m=len;
	n=m;
	SensorCall();if (n>sp->in_buffer_togo)
		{/*305*/SensorCall();n=sp->in_buffer_togo;/*306*/}
	SensorCall();sp->in_buffer_cur+=n;
	sp->in_buffer_togo-=n;
	m-=n;
	SensorCall();if (m>0)
	{
		assert(sp->in_buffer_togo==0);
		SensorCall();n=m;
		SensorCall();if ((uint64)n>sp->in_buffer_file_togo)
			{/*307*/SensorCall();n=(uint16)sp->in_buffer_file_togo;/*308*/}
		SensorCall();sp->in_buffer_file_pos+=n;
		sp->in_buffer_file_togo-=n;
		sp->in_buffer_file_pos_log=0;
		/* we don't skip past jpeginterchangeformat/strile block...
		 * if that is asked from us, we're dealing with totally bazurk
		 * data anyway, and we've not seen this happening on any
		 * testfile, so we might as well likely cause some other
		 * meaningless error to be passed at some later time
		 */
	}
SensorCall();}

static int
OJPEGWriteStream(TIFF* tif, void** mem, uint32* len)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	*len=0;
	SensorCall();do
	{
		assert(sp->out_state<=ososEoi);
		SensorCall();switch(sp->out_state)
		{
			case ososSoi:
				SensorCall();OJPEGWriteStreamSoi(tif,mem,len);
				SensorCall();break;
			case ososQTable0:
				SensorCall();OJPEGWriteStreamQTable(tif,0,mem,len);
				SensorCall();break;
			case ososQTable1:
				SensorCall();OJPEGWriteStreamQTable(tif,1,mem,len);
				SensorCall();break;
			case ososQTable2:
				SensorCall();OJPEGWriteStreamQTable(tif,2,mem,len);
				SensorCall();break;
			case ososQTable3:
				SensorCall();OJPEGWriteStreamQTable(tif,3,mem,len);
				SensorCall();break;
			case ososDcTable0:
				SensorCall();OJPEGWriteStreamDcTable(tif,0,mem,len);
				SensorCall();break;
			case ososDcTable1:
				SensorCall();OJPEGWriteStreamDcTable(tif,1,mem,len);
				SensorCall();break;
			case ososDcTable2:
				SensorCall();OJPEGWriteStreamDcTable(tif,2,mem,len);
				SensorCall();break;
			case ososDcTable3:
				SensorCall();OJPEGWriteStreamDcTable(tif,3,mem,len);
				SensorCall();break;
			case ososAcTable0:
				SensorCall();OJPEGWriteStreamAcTable(tif,0,mem,len);
				SensorCall();break;
			case ososAcTable1:
				SensorCall();OJPEGWriteStreamAcTable(tif,1,mem,len);
				SensorCall();break;
			case ososAcTable2:
				SensorCall();OJPEGWriteStreamAcTable(tif,2,mem,len);
				SensorCall();break;
			case ososAcTable3:
				SensorCall();OJPEGWriteStreamAcTable(tif,3,mem,len);
				SensorCall();break;
			case ososDri:
				SensorCall();OJPEGWriteStreamDri(tif,mem,len);
				SensorCall();break;
			case ososSof:
				SensorCall();OJPEGWriteStreamSof(tif,mem,len);
				SensorCall();break;
			case ososSos:
				SensorCall();OJPEGWriteStreamSos(tif,mem,len);
				SensorCall();break;
			case ososCompressed:
				SensorCall();if (OJPEGWriteStreamCompressed(tif,mem,len)==0)
					{/*309*/SensorCall();return(0);/*310*/}
				SensorCall();break;
			case ososRst:
				SensorCall();OJPEGWriteStreamRst(tif,mem,len);
				SensorCall();break;
			case ososEoi:
				SensorCall();OJPEGWriteStreamEoi(tif,mem,len);
				SensorCall();break;
		}
	} while (*len==0);
	SensorCall();return(1);
}

static void
OJPEGWriteStreamSoi(TIFF* tif, void** mem, uint32* len)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	assert(OJPEG_BUFFER>=2);
	sp->out_buffer[0]=255;
	sp->out_buffer[1]=JPEG_MARKER_SOI;
	*len=2;
	*mem=(void*)sp->out_buffer;
	sp->out_state++;
SensorCall();}

static void
OJPEGWriteStreamQTable(TIFF* tif, uint8 table_index, void** mem, uint32* len)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	SensorCall();if (sp->qtable[table_index]!=0)
	{
		SensorCall();*mem=(void*)(sp->qtable[table_index]+sizeof(uint32));
		*len=*((uint32*)sp->qtable[table_index])-sizeof(uint32);
	}
	SensorCall();sp->out_state++;
SensorCall();}

static void
OJPEGWriteStreamDcTable(TIFF* tif, uint8 table_index, void** mem, uint32* len)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	SensorCall();if (sp->dctable[table_index]!=0)
	{
		SensorCall();*mem=(void*)(sp->dctable[table_index]+sizeof(uint32));
		*len=*((uint32*)sp->dctable[table_index])-sizeof(uint32);
	}
	SensorCall();sp->out_state++;
SensorCall();}

static void
OJPEGWriteStreamAcTable(TIFF* tif, uint8 table_index, void** mem, uint32* len)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	SensorCall();if (sp->actable[table_index]!=0)
	{
		SensorCall();*mem=(void*)(sp->actable[table_index]+sizeof(uint32));
		*len=*((uint32*)sp->actable[table_index])-sizeof(uint32);
	}
	SensorCall();sp->out_state++;
SensorCall();}

static void
OJPEGWriteStreamDri(TIFF* tif, void** mem, uint32* len)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	assert(OJPEG_BUFFER>=6);
	SensorCall();if (sp->restart_interval!=0)
	{
		SensorCall();sp->out_buffer[0]=255;
		sp->out_buffer[1]=JPEG_MARKER_DRI;
		sp->out_buffer[2]=0;
		sp->out_buffer[3]=4;
		sp->out_buffer[4]=(sp->restart_interval>>8);
		sp->out_buffer[5]=(sp->restart_interval&255);
		*len=6;
		*mem=(void*)sp->out_buffer;
	}
	SensorCall();sp->out_state++;
SensorCall();}

static void
OJPEGWriteStreamSof(TIFF* tif, void** mem, uint32* len)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint8 m;
	assert(OJPEG_BUFFER>=2+8+sp->samples_per_pixel_per_plane*3);
	assert(255>=8+sp->samples_per_pixel_per_plane*3);
	sp->out_buffer[0]=255;
	sp->out_buffer[1]=sp->sof_marker_id;
	/* Lf */
	sp->out_buffer[2]=0;
	sp->out_buffer[3]=8+sp->samples_per_pixel_per_plane*3;
	/* P */
	sp->out_buffer[4]=8;
	/* Y */
	sp->out_buffer[5]=(sp->sof_y>>8);
	sp->out_buffer[6]=(sp->sof_y&255);
	/* X */
	sp->out_buffer[7]=(sp->sof_x>>8);
	sp->out_buffer[8]=(sp->sof_x&255);
	/* Nf */
	sp->out_buffer[9]=sp->samples_per_pixel_per_plane;
	SensorCall();for (m=0; m<sp->samples_per_pixel_per_plane; m++)
	{
		/* C */
		SensorCall();sp->out_buffer[10+m*3]=sp->sof_c[sp->plane_sample_offset+m];
		/* H and V */
		sp->out_buffer[10+m*3+1]=sp->sof_hv[sp->plane_sample_offset+m];
		/* Tq */
		sp->out_buffer[10+m*3+2]=sp->sof_tq[sp->plane_sample_offset+m];
	}
	SensorCall();*len=10+sp->samples_per_pixel_per_plane*3;
	*mem=(void*)sp->out_buffer;
	sp->out_state++;
SensorCall();}

static void
OJPEGWriteStreamSos(TIFF* tif, void** mem, uint32* len)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	uint8 m;
	assert(OJPEG_BUFFER>=2+6+sp->samples_per_pixel_per_plane*2);
	assert(255>=6+sp->samples_per_pixel_per_plane*2);
	sp->out_buffer[0]=255;
	sp->out_buffer[1]=JPEG_MARKER_SOS;
	/* Ls */
	sp->out_buffer[2]=0;
	sp->out_buffer[3]=6+sp->samples_per_pixel_per_plane*2;
	/* Ns */
	sp->out_buffer[4]=sp->samples_per_pixel_per_plane;
	SensorCall();for (m=0; m<sp->samples_per_pixel_per_plane; m++)
	{
		/* Cs */
		SensorCall();sp->out_buffer[5+m*2]=sp->sos_cs[sp->plane_sample_offset+m];
		/* Td and Ta */
		sp->out_buffer[5+m*2+1]=sp->sos_tda[sp->plane_sample_offset+m];
	}
	/* Ss */
	SensorCall();sp->out_buffer[5+sp->samples_per_pixel_per_plane*2]=0;
	/* Se */
	sp->out_buffer[5+sp->samples_per_pixel_per_plane*2+1]=63;
	/* Ah and Al */
	sp->out_buffer[5+sp->samples_per_pixel_per_plane*2+2]=0;
	*len=8+sp->samples_per_pixel_per_plane*2;
	*mem=(void*)sp->out_buffer;
	sp->out_state++;
SensorCall();}

static int
OJPEGWriteStreamCompressed(TIFF* tif, void** mem, uint32* len)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	SensorCall();if (sp->in_buffer_togo==0)
	{
		SensorCall();if (OJPEGReadBufferFill(sp)==0)
			{/*311*/SensorCall();return(0);/*312*/}
		assert(sp->in_buffer_togo>0);
	}
	SensorCall();*len=sp->in_buffer_togo;
	*mem=(void*)sp->in_buffer_cur;
	sp->in_buffer_togo=0;
	SensorCall();if (sp->in_buffer_file_togo==0)
	{
		SensorCall();switch(sp->in_buffer_source)
		{
			case osibsStrile:
				SensorCall();if (sp->in_buffer_next_strile<sp->in_buffer_strile_count)
					{/*313*/SensorCall();sp->out_state=ososRst;/*314*/}
				else
					{/*315*/SensorCall();sp->out_state=ososEoi;/*316*/}
				SensorCall();break;
			case osibsEof:
				SensorCall();sp->out_state=ososEoi;
				SensorCall();break;
			default:
				SensorCall();break;
		}
	}
	SensorCall();return(1);
}

static void
OJPEGWriteStreamRst(TIFF* tif, void** mem, uint32* len)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	assert(OJPEG_BUFFER>=2);
	sp->out_buffer[0]=255;
	sp->out_buffer[1]=JPEG_MARKER_RST0+sp->restart_index;
	sp->restart_index++;
	SensorCall();if (sp->restart_index==8)
		{/*317*/SensorCall();sp->restart_index=0;/*318*/}
	SensorCall();*len=2;
	*mem=(void*)sp->out_buffer;
	sp->out_state=ososCompressed;
SensorCall();}

static void
OJPEGWriteStreamEoi(TIFF* tif, void** mem, uint32* len)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	assert(OJPEG_BUFFER>=2);
	sp->out_buffer[0]=255;
	sp->out_buffer[1]=JPEG_MARKER_EOI;
	*len=2;
	*mem=(void*)sp->out_buffer;
SensorCall();}

#ifndef LIBJPEG_ENCAP_EXTERNAL
static int
jpeg_create_decompress_encap(OJPEGState* sp, jpeg_decompress_struct* cinfo)
{
	SensorCall();return(SETJMP(sp->exit_jmpbuf)?0:(jpeg_create_decompress(cinfo),1));
}
#endif

#ifndef LIBJPEG_ENCAP_EXTERNAL
static int
jpeg_read_header_encap(OJPEGState* sp, jpeg_decompress_struct* cinfo, uint8 require_image)
{
	SensorCall();return(SETJMP(sp->exit_jmpbuf)?0:(jpeg_read_header(cinfo,require_image),1));
}
#endif

#ifndef LIBJPEG_ENCAP_EXTERNAL
static int
jpeg_start_decompress_encap(OJPEGState* sp, jpeg_decompress_struct* cinfo)
{
	SensorCall();return(SETJMP(sp->exit_jmpbuf)?0:(jpeg_start_decompress(cinfo),1));
}
#endif

#ifndef LIBJPEG_ENCAP_EXTERNAL
static int
jpeg_read_scanlines_encap(OJPEGState* sp, jpeg_decompress_struct* cinfo, void* scanlines, uint32 max_lines)
{
	SensorCall();return(SETJMP(sp->exit_jmpbuf)?0:(jpeg_read_scanlines(cinfo,scanlines,max_lines),1));
}
#endif

#ifndef LIBJPEG_ENCAP_EXTERNAL
static int
jpeg_read_raw_data_encap(OJPEGState* sp, jpeg_decompress_struct* cinfo, void* data, uint32 max_lines)
{
	SensorCall();return(SETJMP(sp->exit_jmpbuf)?0:(jpeg_read_raw_data(cinfo,data,max_lines),1));
}
#endif

#ifndef LIBJPEG_ENCAP_EXTERNAL
static void
jpeg_encap_unwind(TIFF* tif)
{
	SensorCall();OJPEGState* sp=(OJPEGState*)tif->tif_data;
	LONGJMP(sp->exit_jmpbuf,1);
}
#endif

static void
OJPEGLibjpegJpegErrorMgrOutputMessage(jpeg_common_struct* cinfo)
{
	SensorCall();char buffer[JMSG_LENGTH_MAX];
	(*cinfo->err->format_message)(cinfo,buffer);
	TIFFWarningExt(((TIFF*)(cinfo->client_data))->tif_clientdata,"LibJpeg","%s",buffer);
SensorCall();}

static void
OJPEGLibjpegJpegErrorMgrErrorExit(jpeg_common_struct* cinfo)
{
	SensorCall();char buffer[JMSG_LENGTH_MAX];
	(*cinfo->err->format_message)(cinfo,buffer);
	TIFFErrorExt(((TIFF*)(cinfo->client_data))->tif_clientdata,"LibJpeg","%s",buffer);
	jpeg_encap_unwind((TIFF*)(cinfo->client_data));
SensorCall();}

static void
OJPEGLibjpegJpegSourceMgrInitSource(jpeg_decompress_struct* cinfo)
{
	SensorCall();(void)cinfo;
SensorCall();}

static boolean
OJPEGLibjpegJpegSourceMgrFillInputBuffer(jpeg_decompress_struct* cinfo)
{
	SensorCall();TIFF* tif=(TIFF*)cinfo->client_data;
	OJPEGState* sp=(OJPEGState*)tif->tif_data;
	void* mem=0;
	uint32 len=0U;
	SensorCall();if (OJPEGWriteStream(tif,&mem,&len)==0)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata,"LibJpeg","Premature end of JPEG data");
		jpeg_encap_unwind(tif);
	}
	SensorCall();sp->libjpeg_jpeg_source_mgr.bytes_in_buffer=len;
	sp->libjpeg_jpeg_source_mgr.next_input_byte=mem;
	SensorCall();return(1);
}

static void
OJPEGLibjpegJpegSourceMgrSkipInputData(jpeg_decompress_struct* cinfo, long num_bytes)
{
	SensorCall();TIFF* tif=(TIFF*)cinfo->client_data;
	(void)num_bytes;
	TIFFErrorExt(tif->tif_clientdata,"LibJpeg","Unexpected error");
	jpeg_encap_unwind(tif);
SensorCall();}

static boolean
OJPEGLibjpegJpegSourceMgrResyncToRestart(jpeg_decompress_struct* cinfo, int desired)
{
	SensorCall();TIFF* tif=(TIFF*)cinfo->client_data;
	(void)desired;
	TIFFErrorExt(tif->tif_clientdata,"LibJpeg","Unexpected error");
	jpeg_encap_unwind(tif);
	SensorCall();return(0);
}

static void
OJPEGLibjpegJpegSourceMgrTermSource(jpeg_decompress_struct* cinfo)
{
	SensorCall();(void)cinfo;
SensorCall();}

#endif


/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
