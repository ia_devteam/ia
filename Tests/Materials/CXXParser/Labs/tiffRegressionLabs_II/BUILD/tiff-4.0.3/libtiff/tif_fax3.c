/* $Id: tif_fax3.c,v 1.74 2012-06-21 02:01:31 fwarmerdam Exp $ */

/*
 * Copyright (c) 1990-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

#include "tiffiop.h"
#include "/var/tmp/sensor.h"
#ifdef CCITT_SUPPORT
/*
 * TIFF Library.
 *
 * CCITT Group 3 (T.4) and Group 4 (T.6) Compression Support.
 *
 * This file contains support for decoding and encoding TIFF
 * compression algorithms 2, 3, 4, and 32771.
 *
 * Decoder support is derived, with permission, from the code
 * in Frank Cringle's viewfax program;
 *      Copyright (C) 1990, 1995  Frank D. Cringle.
 */
#include "tif_fax3.h"
#define	G3CODES
#include "t4.h"
#include <stdio.h>

/*
 * Compression+decompression state blocks are
 * derived from this ``base state'' block.
 */
typedef struct {
	int      rw_mode;                /* O_RDONLY for decode, else encode */
	int      mode;                   /* operating mode */
	tmsize_t rowbytes;               /* bytes in a decoded scanline */
	uint32   rowpixels;              /* pixels in a scanline */

	uint16   cleanfaxdata;           /* CleanFaxData tag */
	uint32   badfaxrun;              /* BadFaxRun tag */
	uint32   badfaxlines;            /* BadFaxLines tag */
	uint32   groupoptions;           /* Group 3/4 options tag */

	TIFFVGetMethod  vgetparent;      /* super-class method */
	TIFFVSetMethod  vsetparent;      /* super-class method */
	TIFFPrintMethod printdir;        /* super-class method */
} Fax3BaseState;
#define	Fax3State(tif)		((Fax3BaseState*) (tif)->tif_data)

typedef enum { G3_1D, G3_2D } Ttag;
typedef struct {
	Fax3BaseState b;

	/* Decoder state info */
	const unsigned char* bitmap;	/* bit reversal table */
	uint32	data;			/* current i/o byte/word */
	int	bit;			/* current i/o bit in byte */
	int	EOLcnt;			/* count of EOL codes recognized */
	TIFFFaxFillFunc fill;		/* fill routine */
	uint32*	runs;			/* b&w runs for current/previous row */
	uint32*	refruns;		/* runs for reference line */
	uint32*	curruns;		/* runs for current line */

	/* Encoder state info */
	Ttag    tag;			/* encoding state */
	unsigned char*	refline;	/* reference line for 2d decoding */
	int	k;			/* #rows left that can be 2d encoded */
	int	maxk;			/* max #rows that can be 2d encoded */

	int line;
} Fax3CodecState;
#define DecoderState(tif) ((Fax3CodecState*) Fax3State(tif))
#define EncoderState(tif) ((Fax3CodecState*) Fax3State(tif))

#define is2DEncoding(sp) (sp->b.groupoptions & GROUP3OPT_2DENCODING)
#define isAligned(p,t) ((((size_t)(p)) & (sizeof (t)-1)) == 0)

/*
 * Group 3 and Group 4 Decoding.
 */

/*
 * These macros glue the TIFF library state to
 * the state expected by Frank's decoder.
 */
#define	DECLARE_STATE(tif, sp, mod)					\
    static const char module[] = mod;					\
    Fax3CodecState* sp = DecoderState(tif);				\
    int a0;				/* reference element */		\
    int lastx = sp->b.rowpixels;	/* last element in row */	\
    uint32 BitAcc;			/* bit accumulator */		\
    int BitsAvail;			/* # valid bits in BitAcc */	\
    int RunLength;			/* length of current run */	\
    unsigned char* cp;			/* next byte of input data */	\
    unsigned char* ep;			/* end of input data */		\
    uint32* pa;				/* place to stuff next run */	\
    uint32* thisrun;			/* current row's run array */	\
    int EOLcnt;				/* # EOL codes recognized */	\
    const unsigned char* bitmap = sp->bitmap;	/* input data bit reverser */	\
    const TIFFFaxTabEnt* TabEnt
#define	DECLARE_STATE_2D(tif, sp, mod)					\
    DECLARE_STATE(tif, sp, mod);					\
    int b1;				/* next change on prev line */	\
    uint32* pb				/* next run in reference line */\
/*
 * Load any state that may be changed during decoding.
 */
#define	CACHE_STATE(tif, sp) do {					\
    BitAcc = sp->data;							\
    BitsAvail = sp->bit;						\
    EOLcnt = sp->EOLcnt;						\
    cp = (unsigned char*) tif->tif_rawcp;				\
    ep = cp + tif->tif_rawcc;						\
} while (0)
/*
 * Save state possibly changed during decoding.
 */
#define	UNCACHE_STATE(tif, sp) do {					\
    sp->bit = BitsAvail;						\
    sp->data = BitAcc;							\
    sp->EOLcnt = EOLcnt;						\
    tif->tif_rawcc -= (tmsize_t)((uint8*) cp - tif->tif_rawcp);		\
    tif->tif_rawcp = (uint8*) cp;					\
} while (0)

/*
 * Setup state for decoding a strip.
 */
static int
Fax3PreDecode(TIFF* tif, uint16 s)
{
	SensorCall();Fax3CodecState* sp = DecoderState(tif);

	(void) s;
	assert(sp != NULL);
	sp->bit = 0;			/* force initial read */
	sp->data = 0;
	sp->EOLcnt = 0;			/* force initial scan for EOL */
	/*
	 * Decoder assumes lsb-to-msb bit order.  Note that we select
	 * this here rather than in Fax3SetupState so that viewers can
	 * hold the image open, fiddle with the FillOrder tag value,
	 * and then re-decode the image.  Otherwise they'd need to close
	 * and open the image to get the state reset.
	 */
	sp->bitmap =
	    TIFFGetBitRevTable(tif->tif_dir.td_fillorder != FILLORDER_LSB2MSB);
	SensorCall();if (sp->refruns) {		/* init reference line to white */
		SensorCall();sp->refruns[0] = (uint32) sp->b.rowpixels;
		sp->refruns[1] = 0;
	}
	SensorCall();sp->line = 0;
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

/*
 * Routine for handling various errors/conditions.
 * Note how they are "glued into the decoder" by
 * overriding the definitions used by the decoder.
 */

static void
Fax3Unexpected(const char* module, TIFF* tif, uint32 line, uint32 a0)
{
	SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "Bad code word at line %u of %s %u (x %u)",
	    line, isTiled(tif) ? "tile" : "strip",
	    (isTiled(tif) ? tif->tif_curtile : tif->tif_curstrip),
	    a0);
SensorCall();}
#define	unexpected(table, a0)	Fax3Unexpected(module, tif, sp->line, a0)

static void
Fax3Extension(const char* module, TIFF* tif, uint32 line, uint32 a0)
{
	SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
	    "Uncompressed data (not supported) at line %u of %s %u (x %u)",
	    line, isTiled(tif) ? "tile" : "strip",
	    (isTiled(tif) ? tif->tif_curtile : tif->tif_curstrip),
	    a0);
SensorCall();}
#define	extension(a0)	Fax3Extension(module, tif, sp->line, a0)

static void
Fax3BadLength(const char* module, TIFF* tif, uint32 line, uint32 a0, uint32 lastx)
{
	SensorCall();TIFFWarningExt(tif->tif_clientdata, module, "%s at line %u of %s %u (got %u, expected %u)",
	    a0 < lastx ? "Premature EOL" : "Line length mismatch",
	    line, isTiled(tif) ? "tile" : "strip",
	    (isTiled(tif) ? tif->tif_curtile : tif->tif_curstrip),
	    a0, lastx);
SensorCall();}
#define	badlength(a0,lastx)	Fax3BadLength(module, tif, sp->line, a0, lastx)

static void
Fax3PrematureEOF(const char* module, TIFF* tif, uint32 line, uint32 a0)
{
	SensorCall();TIFFWarningExt(tif->tif_clientdata, module, "Premature EOF at line %u of %s %u (x %u)",
	    line, isTiled(tif) ? "tile" : "strip",
	    (isTiled(tif) ? tif->tif_curtile : tif->tif_curstrip),
	    a0);
SensorCall();}
#define	prematureEOF(a0)	Fax3PrematureEOF(module, tif, sp->line, a0)

#define	Nop

/*
 * Decode the requested amount of G3 1D-encoded data.
 */
static int
Fax3Decode1D(TIFF* tif, uint8* buf, tmsize_t occ, uint16 s)
{
SensorCall();	DECLARE_STATE(tif, sp, "Fax3Decode1D");
	(void) s;
	SensorCall();if (occ % sp->b.rowbytes)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "Fractional scanlines cannot be read");
		{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
	}
	CACHE_STATE(tif, sp);
	SensorCall();thisrun = sp->curruns;
	SensorCall();while (occ > 0) {
		SensorCall();a0 = 0;
		RunLength = 0;
		pa = thisrun;
#ifdef FAX3_DEBUG
		printf("\nBitAcc=%08X, BitsAvail = %d\n", BitAcc, BitsAvail);
		printf("-------------------- %d\n", tif->tif_row);
		fflush(stdout);
#endif
		SYNC_EOL(EOF1D);
		EXPAND1D(EOF1Da);
		(*sp->fill)(buf, thisrun, pa, lastx);
		buf += sp->b.rowbytes;
		occ -= sp->b.rowbytes;
		sp->line++;
		SensorCall();continue;
	EOF1D:				/* premature EOF */
		CLEANUP_RUNS();
	EOF1Da:				/* premature EOF */
		(*sp->fill)(buf, thisrun, pa, lastx);
		UNCACHE_STATE(tif, sp);
		return (-1);
	}
	UNCACHE_STATE(tif, sp);
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

#define	SWAP(t,a,b)	{ t x; x = (a); (a) = (b); (b) = x; }
/*
 * Decode the requested amount of G3 2D-encoded data.
 */
static int
Fax3Decode2D(TIFF* tif, uint8* buf, tmsize_t occ, uint16 s)
{
SensorCall();	DECLARE_STATE_2D(tif, sp, "Fax3Decode2D");
	int is1D;			/* current line is 1d/2d-encoded */
	(void) s;
	SensorCall();if (occ % sp->b.rowbytes)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "Fractional scanlines cannot be read");
		{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
	}
	CACHE_STATE(tif, sp);
	SensorCall();while (occ > 0) {
		SensorCall();a0 = 0;
		RunLength = 0;
		pa = thisrun = sp->curruns;
#ifdef FAX3_DEBUG
		printf("\nBitAcc=%08X, BitsAvail = %d EOLcnt = %d",
		    BitAcc, BitsAvail, EOLcnt);
#endif
		SYNC_EOL(EOF2D);
		NeedBits8(1, EOF2D);
		is1D = GetBits(1);	/* 1D/2D-encoding tag bit */
		ClrBits(1);
#ifdef FAX3_DEBUG
		printf(" %s\n-------------------- %d\n",
		    is1D ? "1D" : "2D", tif->tif_row);
		fflush(stdout);
#endif
		pb = sp->refruns;
		b1 = *pb++;
		SensorCall();if (is1D)
			EXPAND1D(EOF2Da);
		else
			EXPAND2D(EOF2Da);
		SensorCall();(*sp->fill)(buf, thisrun, pa, lastx);
		SETVALUE(0);		/* imaginary change for reference */
		SWAP(uint32*, sp->curruns, sp->refruns);
		buf += sp->b.rowbytes;
		occ -= sp->b.rowbytes;
		sp->line++;
		SensorCall();continue;
	EOF2D:				/* premature EOF */
		CLEANUP_RUNS();
	EOF2Da:				/* premature EOF */
		(*sp->fill)(buf, thisrun, pa, lastx);
		UNCACHE_STATE(tif, sp);
		return (-1);
	}
	UNCACHE_STATE(tif, sp);
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}
#undef SWAP

/*
 * The ZERO & FILL macros must handle spans < 2*sizeof(long) bytes.
 * For machines with 64-bit longs this is <16 bytes; otherwise
 * this is <8 bytes.  We optimize the code here to reflect the
 * machine characteristics.
 */
#if SIZEOF_UNSIGNED_LONG == 8
# define FILL(n, cp)							    \
    switch (n) {							    \
    case 15:(cp)[14] = 0xff; case 14:(cp)[13] = 0xff; case 13: (cp)[12] = 0xff;\
    case 12:(cp)[11] = 0xff; case 11:(cp)[10] = 0xff; case 10: (cp)[9] = 0xff;\
    case  9: (cp)[8] = 0xff; case  8: (cp)[7] = 0xff; case  7: (cp)[6] = 0xff;\
    case  6: (cp)[5] = 0xff; case  5: (cp)[4] = 0xff; case  4: (cp)[3] = 0xff;\
    case  3: (cp)[2] = 0xff; case  2: (cp)[1] = 0xff;			      \
    case  1: (cp)[0] = 0xff; (cp) += (n); case 0:  ;			      \
    }
# define ZERO(n, cp)							\
    switch (n) {							\
    case 15:(cp)[14] = 0; case 14:(cp)[13] = 0; case 13: (cp)[12] = 0;	\
    case 12:(cp)[11] = 0; case 11:(cp)[10] = 0; case 10: (cp)[9] = 0;	\
    case  9: (cp)[8] = 0; case  8: (cp)[7] = 0; case  7: (cp)[6] = 0;	\
    case  6: (cp)[5] = 0; case  5: (cp)[4] = 0; case  4: (cp)[3] = 0;	\
    case  3: (cp)[2] = 0; case  2: (cp)[1] = 0;				\
    case  1: (cp)[0] = 0; (cp) += (n); case 0:  ;			\
    }
#else
# define FILL(n, cp)							    \
    switch (n) {							    \
    case 7: (cp)[6] = 0xff; case 6: (cp)[5] = 0xff; case 5: (cp)[4] = 0xff; \
    case 4: (cp)[3] = 0xff; case 3: (cp)[2] = 0xff; case 2: (cp)[1] = 0xff; \
    case 1: (cp)[0] = 0xff; (cp) += (n); case 0:  ;			    \
    }
# define ZERO(n, cp)							\
    switch (n) {							\
    case 7: (cp)[6] = 0; case 6: (cp)[5] = 0; case 5: (cp)[4] = 0;	\
    case 4: (cp)[3] = 0; case 3: (cp)[2] = 0; case 2: (cp)[1] = 0;	\
    case 1: (cp)[0] = 0; (cp) += (n); case 0:  ;			\
    }
#endif

/*
 * Bit-fill a row according to the white/black
 * runs generated during G3/G4 decoding.
 */
void
_TIFFFax3fillruns(unsigned char* buf, uint32* runs, uint32* erun, uint32 lastx)
{
	SensorCall();static const unsigned char _fillmasks[] =
	    { 0x00, 0x80, 0xc0, 0xe0, 0xf0, 0xf8, 0xfc, 0xfe, 0xff };
	unsigned char* cp;
	uint32 x, bx, run;
	int32 n, nw;
	long* lp;

	SensorCall();if ((erun-runs)&1)
	    {/*9*/SensorCall();*erun++ = 0;/*10*/}
	SensorCall();x = 0;
	SensorCall();for (; runs < erun; runs += 2) {
	    SensorCall();run = runs[0];
	    SensorCall();if (x+run > lastx || run > lastx )
		{/*11*/SensorCall();run = runs[0] = (uint32) (lastx - x);/*12*/}
	    SensorCall();if (run) {
		SensorCall();cp = buf + (x>>3);
		bx = x&7;
		SensorCall();if (run > 8-bx) {
		    SensorCall();if (bx) {			/* align to byte boundary */
			SensorCall();*cp++ &= 0xff << (8-bx);
			run -= 8-bx;
		    }
		    SensorCall();if( (n = run >> 3) != 0 ) {	/* multiple bytes to fill */
			SensorCall();if ((n/sizeof (long)) > 1) {
			    /*
			     * Align to longword boundary and fill.
			     */
			    SensorCall();for (; n && !isAligned(cp, long); n--)
				    {/*13*/SensorCall();*cp++ = 0x00;/*14*/}
			    SensorCall();lp = (long*) cp;
			    nw = (int32)(n / sizeof (long));
			    n -= nw * sizeof (long);
			    SensorCall();do {
				    SensorCall();*lp++ = 0L;
			    } while (--nw);
			    SensorCall();cp = (unsigned char*) lp;
			}
			ZERO(n, cp);
			SensorCall();run &= 7;
		    }
		    SensorCall();if (run)
			{/*15*/SensorCall();cp[0] &= 0xff >> run;/*16*/}
		} else
		    {/*17*/SensorCall();cp[0] &= ~(_fillmasks[run]>>bx);/*18*/}
		SensorCall();x += runs[0];
	    }
	    SensorCall();run = runs[1];
	    SensorCall();if (x+run > lastx || run > lastx )
		{/*19*/SensorCall();run = runs[1] = lastx - x;/*20*/}
	    SensorCall();if (run) {
		SensorCall();cp = buf + (x>>3);
		bx = x&7;
		SensorCall();if (run > 8-bx) {
		    SensorCall();if (bx) {			/* align to byte boundary */
			SensorCall();*cp++ |= 0xff >> bx;
			run -= 8-bx;
		    }
		    SensorCall();if( (n = run>>3) != 0 ) {	/* multiple bytes to fill */
			SensorCall();if ((n/sizeof (long)) > 1) {
			    /*
			     * Align to longword boundary and fill.
			     */
			    SensorCall();for (; n && !isAligned(cp, long); n--)
				{/*21*/SensorCall();*cp++ = 0xff;/*22*/}
			    SensorCall();lp = (long*) cp;
			    nw = (int32)(n / sizeof (long));
			    n -= nw * sizeof (long);
			    SensorCall();do {
				SensorCall();*lp++ = -1L;
			    } while (--nw);
			    SensorCall();cp = (unsigned char*) lp;
			}
			FILL(n, cp);
			SensorCall();run &= 7;
		    }
		    SensorCall();if (run)
			{/*23*/SensorCall();cp[0] |= 0xff00 >> run;/*24*/}
		} else
		    {/*25*/SensorCall();cp[0] |= _fillmasks[run]>>bx;/*26*/}
		SensorCall();x += runs[1];
	    }
	}
	assert(x == lastx);
}
#undef	ZERO
#undef	FILL

static int
Fax3FixupTags(TIFF* tif)
{
	SensorCall();(void) tif;
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

/*
 * Setup G3/G4-related compression/decompression state
 * before data is processed.  This routine is called once
 * per image -- it sets up different state based on whether
 * or not decoding or encoding is being done and whether
 * 1D- or 2D-encoded data is involved.
 */
static int
Fax3SetupState(TIFF* tif)
{
	SensorCall();static const char module[] = "Fax3SetupState";
	TIFFDirectory* td = &tif->tif_dir;
	Fax3BaseState* sp = Fax3State(tif);
	int needsRefLine;
	Fax3CodecState* dsp = (Fax3CodecState*) Fax3State(tif);
	tmsize_t rowbytes;
	uint32 rowpixels, nruns;

	SensorCall();if (td->td_bitspersample != 1) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
		    "Bits/sample must be 1 for Group 3/4 encoding/decoding");
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
	/*
	 * Calculate the scanline/tile widths.
	 */
	SensorCall();if (isTiled(tif)) {
		SensorCall();rowbytes = TIFFTileRowSize(tif);
		rowpixels = td->td_tilewidth;
	} else {
		SensorCall();rowbytes = TIFFScanlineSize(tif);
		rowpixels = td->td_imagewidth;
	}
	SensorCall();sp->rowbytes = rowbytes;
	sp->rowpixels = rowpixels;
	/*
	 * Allocate any additional space required for decoding/encoding.
	 */
	needsRefLine = (
	    (sp->groupoptions & GROUP3OPT_2DENCODING) ||
	    td->td_compression == COMPRESSION_CCITTFAX4
	);

	/*
	  Assure that allocation computations do not overflow.
	  
	  TIFFroundup and TIFFSafeMultiply return zero on integer overflow
	*/
	dsp->runs=(uint32*) NULL;
	nruns = TIFFroundup_32(rowpixels,32);
	SensorCall();if (needsRefLine) {
		SensorCall();nruns = TIFFSafeMultiply(uint32,nruns,2);
	}
	SensorCall();if ((nruns == 0) || (TIFFSafeMultiply(uint32,nruns,2) == 0)) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, tif->tif_name,
			     "Row pixels integer overflow (rowpixels %u)",
			     rowpixels);
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();dsp->runs = (uint32*) _TIFFCheckMalloc(tif,
					       TIFFSafeMultiply(uint32,nruns,2),
					       sizeof (uint32),
					       "for Group 3/4 run arrays");
	SensorCall();if (dsp->runs == NULL)
		{/*27*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*28*/}
	SensorCall();memset( dsp->runs, 0, TIFFSafeMultiply(uint32,nruns,2)*sizeof(uint32));
	dsp->curruns = dsp->runs;
	SensorCall();if (needsRefLine)
		{/*29*/SensorCall();dsp->refruns = dsp->runs + nruns;/*30*/}
	else
		dsp->refruns = NULL;
	SensorCall();if (td->td_compression == COMPRESSION_CCITTFAX3
	    && is2DEncoding(dsp)) {	/* NB: default is 1D routine */
		SensorCall();tif->tif_decoderow = Fax3Decode2D;
		tif->tif_decodestrip = Fax3Decode2D;
		tif->tif_decodetile = Fax3Decode2D;
	}

	SensorCall();if (needsRefLine) {		/* 2d encoding */
		SensorCall();Fax3CodecState* esp = EncoderState(tif);
		/*
		 * 2d encoding requires a scanline
		 * buffer for the ``reference line''; the
		 * scanline against which delta encoding
		 * is referenced.  The reference line must
		 * be initialized to be ``white'' (done elsewhere).
		 */
		esp->refline = (unsigned char*) _TIFFmalloc(rowbytes);
		SensorCall();if (esp->refline == NULL) {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
			    "No space for Group 3/4 reference line");
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
	} else					/* 1d encoding */
		EncoderState(tif)->refline = NULL;

	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

/*
 * CCITT Group 3 FAX Encoding.
 */

#define	Fax3FlushBits(tif, sp) {				\
	if ((tif)->tif_rawcc >= (tif)->tif_rawdatasize)		\
		(void) TIFFFlushData1(tif);			\
	*(tif)->tif_rawcp++ = (uint8) (sp)->data;		\
	(tif)->tif_rawcc++;					\
	(sp)->data = 0, (sp)->bit = 8;				\
}
#define	_FlushBits(tif) {					\
	if ((tif)->tif_rawcc >= (tif)->tif_rawdatasize)		\
		(void) TIFFFlushData1(tif);			\
	*(tif)->tif_rawcp++ = (uint8) data;		\
	(tif)->tif_rawcc++;					\
	data = 0, bit = 8;					\
}
static const int _msbmask[9] =
    { 0x00, 0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f, 0xff };
#define	_PutBits(tif, bits, length) {				\
	while (length > bit) {					\
		data |= bits >> (length - bit);			\
		length -= bit;					\
		_FlushBits(tif);				\
	}							\
        assert( length < 9 );                                   \
	data |= (bits & _msbmask[length]) << (bit - length);	\
	bit -= length;						\
	if (bit == 0)						\
		_FlushBits(tif);				\
}
	
/*
 * Write a variable-length bit-value to
 * the output stream.  Values are
 * assumed to be at most 16 bits.
 */
static void
Fax3PutBits(TIFF* tif, unsigned int bits, unsigned int length)
{
	SensorCall();Fax3CodecState* sp = EncoderState(tif);
	unsigned int bit = sp->bit;
	int data = sp->data;

	_PutBits(tif, bits, length);

	SensorCall();sp->data = data;
	sp->bit = bit;
SensorCall();}

/*
 * Write a code to the output stream.
 */
#define putcode(tif, te)	Fax3PutBits(tif, (te)->code, (te)->length)

#ifdef FAX3_DEBUG
#define	DEBUG_COLOR(w) (tab == TIFFFaxWhiteCodes ? w "W" : w "B")
#define	DEBUG_PRINT(what,len) {						\
    int t;								\
    printf("%08X/%-2d: %s%5d\t", data, bit, DEBUG_COLOR(what), len);	\
    for (t = length-1; t >= 0; t--)					\
	putchar(code & (1<<t) ? '1' : '0');				\
    putchar('\n');							\
}
#endif

/*
 * Write the sequence of codes that describes
 * the specified span of zero's or one's.  The
 * appropriate table that holds the make-up and
 * terminating codes is supplied.
 */
static void
putspan(TIFF* tif, int32 span, const tableentry* tab)
{
	SensorCall();Fax3CodecState* sp = EncoderState(tif);
	unsigned int bit = sp->bit;
	int data = sp->data;
	unsigned int code, length;

	SensorCall();while (span >= 2624) {
		SensorCall();const tableentry* te = &tab[63 + (2560>>6)];
		code = te->code, length = te->length;
#ifdef FAX3_DEBUG
		DEBUG_PRINT("MakeUp", te->runlen);
#endif
		_PutBits(tif, code, length);
		SensorCall();span -= te->runlen;
	}
	SensorCall();if (span >= 64) {
		SensorCall();const tableentry* te = &tab[63 + (span>>6)];
		assert(te->runlen == 64*(span>>6));
		code = te->code, length = te->length;
#ifdef FAX3_DEBUG
		DEBUG_PRINT("MakeUp", te->runlen);
#endif
		_PutBits(tif, code, length);
		SensorCall();span -= te->runlen;
	}
	SensorCall();code = tab[span].code, length = tab[span].length;
#ifdef FAX3_DEBUG
	DEBUG_PRINT("  Term", tab[span].runlen);
#endif
	_PutBits(tif, code, length);

	SensorCall();sp->data = data;
	sp->bit = bit;
SensorCall();}

/*
 * Write an EOL code to the output stream.  The zero-fill
 * logic for byte-aligning encoded scanlines is handled
 * here.  We also handle writing the tag bit for the next
 * scanline when doing 2d encoding.
 */
static void
Fax3PutEOL(TIFF* tif)
{
	SensorCall();Fax3CodecState* sp = EncoderState(tif);
	unsigned int bit = sp->bit;
	int data = sp->data;
	unsigned int code, length, tparm;

	SensorCall();if (sp->b.groupoptions & GROUP3OPT_FILLBITS) {
		/*
		 * Force bit alignment so EOL will terminate on
		 * a byte boundary.  That is, force the bit alignment
		 * to 16-12 = 4 before putting out the EOL code.
		 */
		SensorCall();int align = 8 - 4;
		SensorCall();if (align != sp->bit) {
			SensorCall();if (align > sp->bit)
				{/*31*/SensorCall();align = sp->bit + (8 - align);/*32*/}
			else
				{/*33*/SensorCall();align = sp->bit - align;/*34*/}
			SensorCall();code = 0;
			tparm=align; 
			_PutBits(tif, 0, tparm);
		}
	}
	SensorCall();code = EOL, length = 12;
	SensorCall();if (is2DEncoding(sp))
		{/*35*/SensorCall();code = (code<<1) | (sp->tag == G3_1D), length++;/*36*/}
	_PutBits(tif, code, length);

	SensorCall();sp->data = data;
	sp->bit = bit;
SensorCall();}

/*
 * Reset encoding state at the start of a strip.
 */
static int
Fax3PreEncode(TIFF* tif, uint16 s)
{
	SensorCall();Fax3CodecState* sp = EncoderState(tif);

	(void) s;
	assert(sp != NULL);
	sp->bit = 8;
	sp->data = 0;
	sp->tag = G3_1D;
	/*
	 * This is necessary for Group 4; otherwise it isn't
	 * needed because the first scanline of each strip ends
	 * up being copied into the refline.
	 */
	SensorCall();if (sp->refline)
		{/*37*/SensorCall();_TIFFmemset(sp->refline, 0x00, sp->b.rowbytes);/*38*/}
	SensorCall();if (is2DEncoding(sp)) {
		SensorCall();float res = tif->tif_dir.td_yresolution;
		/*
		 * The CCITT spec says that when doing 2d encoding, you
		 * should only do it on K consecutive scanlines, where K
		 * depends on the resolution of the image being encoded
		 * (2 for <= 200 lpi, 4 for > 200 lpi).  Since the directory
		 * code initializes td_yresolution to 0, this code will
		 * select a K of 2 unless the YResolution tag is set
		 * appropriately.  (Note also that we fudge a little here
		 * and use 150 lpi to avoid problems with units conversion.)
		 */
		SensorCall();if (tif->tif_dir.td_resolutionunit == RESUNIT_CENTIMETER)
			{/*39*/SensorCall();res *= 2.54f;/*40*/}		/* convert to inches */
		SensorCall();sp->maxk = (res > 150 ? 4 : 2);
		sp->k = sp->maxk-1;
	} else
		{/*41*/SensorCall();sp->k = sp->maxk = 0;/*42*/}
	SensorCall();sp->line = 0;
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

static const unsigned char zeroruns[256] = {
    8, 7, 6, 6, 5, 5, 5, 5, 4, 4, 4, 4, 4, 4, 4, 4,	/* 0x00 - 0x0f */
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,	/* 0x10 - 0x1f */
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,	/* 0x20 - 0x2f */
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,	/* 0x30 - 0x3f */
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,	/* 0x40 - 0x4f */
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,	/* 0x50 - 0x5f */
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,	/* 0x60 - 0x6f */
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,	/* 0x70 - 0x7f */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	/* 0x80 - 0x8f */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	/* 0x90 - 0x9f */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	/* 0xa0 - 0xaf */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	/* 0xb0 - 0xbf */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	/* 0xc0 - 0xcf */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	/* 0xd0 - 0xdf */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	/* 0xe0 - 0xef */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	/* 0xf0 - 0xff */
};
static const unsigned char oneruns[256] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	/* 0x00 - 0x0f */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	/* 0x10 - 0x1f */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	/* 0x20 - 0x2f */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	/* 0x30 - 0x3f */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	/* 0x40 - 0x4f */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	/* 0x50 - 0x5f */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	/* 0x60 - 0x6f */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	/* 0x70 - 0x7f */
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,	/* 0x80 - 0x8f */
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,	/* 0x90 - 0x9f */
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,	/* 0xa0 - 0xaf */
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,	/* 0xb0 - 0xbf */
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,	/* 0xc0 - 0xcf */
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,	/* 0xd0 - 0xdf */
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,	/* 0xe0 - 0xef */
    4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 7, 8,	/* 0xf0 - 0xff */
};

/*
 * On certain systems it pays to inline
 * the routines that find pixel spans.
 */
#ifdef VAXC
static	int32 find0span(unsigned char*, int32, int32);
static	int32 find1span(unsigned char*, int32, int32);
#pragma inline(find0span,find1span)
#endif

/*
 * Find a span of ones or zeros using the supplied
 * table.  The ``base'' of the bit string is supplied
 * along with the start+end bit indices.
 */
inline static int32
find0span(unsigned char* bp, int32 bs, int32 be)
{
	SensorCall();int32 bits = be - bs;
	int32 n, span;

	bp += bs>>3;
	/*
	 * Check partial byte on lhs.
	 */
	SensorCall();if (bits > 0 && (n = (bs & 7))) {
		SensorCall();span = zeroruns[(*bp << n) & 0xff];
		SensorCall();if (span > 8-n)		/* table value too generous */
			{/*43*/SensorCall();span = 8-n;/*44*/}
		SensorCall();if (span > bits)	/* constrain span to bit range */
			{/*45*/SensorCall();span = bits;/*46*/}
		SensorCall();if (n+span < 8)		/* doesn't extend to edge of byte */
			{/*47*/{int32  ReplaceReturn = (span); SensorCall(); return ReplaceReturn;}/*48*/}
		SensorCall();bits -= span;
		bp++;
	} else
		{/*49*/SensorCall();span = 0;/*50*/}
	SensorCall();if (bits >= (int32)(2 * 8 * sizeof(long))) {
		SensorCall();long* lp;
		/*
		 * Align to longword boundary and check longwords.
		 */
		SensorCall();while (!isAligned(bp, long)) {
			SensorCall();if (*bp != 0x00)
				{/*51*/{int32  ReplaceReturn = (span + zeroruns[*bp]); SensorCall(); return ReplaceReturn;}/*52*/}
			SensorCall();span += 8, bits -= 8;
			bp++;
		}
		SensorCall();lp = (long*) bp;
		SensorCall();while ((bits >= (int32)(8 * sizeof(long))) && (0 == *lp)) {
			SensorCall();span += 8*sizeof (long), bits -= 8*sizeof (long);
			lp++;
		}
		SensorCall();bp = (unsigned char*) lp;
	}
	/*
	 * Scan full bytes for all 0's.
	 */
	SensorCall();while (bits >= 8) {
		SensorCall();if (*bp != 0x00)	/* end of run */
			{/*53*/{int32  ReplaceReturn = (span + zeroruns[*bp]); SensorCall(); return ReplaceReturn;}/*54*/}
		SensorCall();span += 8, bits -= 8;
		bp++;
	}
	/*
	 * Check partial byte on rhs.
	 */
	SensorCall();if (bits > 0) {
		SensorCall();n = zeroruns[*bp];
		span += (n > bits ? bits : n);
	}
	{int32  ReplaceReturn = (span); SensorCall(); return ReplaceReturn;}
}

inline static int32
find1span(unsigned char* bp, int32 bs, int32 be)
{
	SensorCall();int32 bits = be - bs;
	int32 n, span;

	bp += bs>>3;
	/*
	 * Check partial byte on lhs.
	 */
	SensorCall();if (bits > 0 && (n = (bs & 7))) {
		SensorCall();span = oneruns[(*bp << n) & 0xff];
		SensorCall();if (span > 8-n)		/* table value too generous */
			{/*55*/SensorCall();span = 8-n;/*56*/}
		SensorCall();if (span > bits)	/* constrain span to bit range */
			{/*57*/SensorCall();span = bits;/*58*/}
		SensorCall();if (n+span < 8)		/* doesn't extend to edge of byte */
			{/*59*/{int32  ReplaceReturn = (span); SensorCall(); return ReplaceReturn;}/*60*/}
		SensorCall();bits -= span;
		bp++;
	} else
		{/*61*/SensorCall();span = 0;/*62*/}
	SensorCall();if (bits >= (int32)(2 * 8 * sizeof(long))) {
		SensorCall();long* lp;
		/*
		 * Align to longword boundary and check longwords.
		 */
		SensorCall();while (!isAligned(bp, long)) {
			SensorCall();if (*bp != 0xff)
				{/*63*/{int32  ReplaceReturn = (span + oneruns[*bp]); SensorCall(); return ReplaceReturn;}/*64*/}
			SensorCall();span += 8, bits -= 8;
			bp++;
		}
		SensorCall();lp = (long*) bp;
		SensorCall();while ((bits >= (int32)(8 * sizeof(long))) && (~0 == *lp)) {
			SensorCall();span += 8*sizeof (long), bits -= 8*sizeof (long);
			lp++;
		}
		SensorCall();bp = (unsigned char*) lp;
	}
	/*
	 * Scan full bytes for all 1's.
	 */
	SensorCall();while (bits >= 8) {
		SensorCall();if (*bp != 0xff)	/* end of run */
			{/*65*/{int32  ReplaceReturn = (span + oneruns[*bp]); SensorCall(); return ReplaceReturn;}/*66*/}
		SensorCall();span += 8, bits -= 8;
		bp++;
	}
	/*
	 * Check partial byte on rhs.
	 */
	SensorCall();if (bits > 0) {
		SensorCall();n = oneruns[*bp];
		span += (n > bits ? bits : n);
	}
	{int32  ReplaceReturn = (span); SensorCall(); return ReplaceReturn;}
}

/*
 * Return the offset of the next bit in the range
 * [bs..be] that is different from the specified
 * color.  The end, be, is returned if no such bit
 * exists.
 */
#define	finddiff(_cp, _bs, _be, _color)	\
	(_bs + (_color ? find1span(_cp,_bs,_be) : find0span(_cp,_bs,_be)))
/*
 * Like finddiff, but also check the starting bit
 * against the end in case start > end.
 */
#define	finddiff2(_cp, _bs, _be, _color) \
	(_bs < _be ? finddiff(_cp,_bs,_be,_color) : _be)

/*
 * 1d-encode a row of pixels.  The encoding is
 * a sequence of all-white or all-black spans
 * of pixels encoded with Huffman codes.
 */
static int
Fax3Encode1DRow(TIFF* tif, unsigned char* bp, uint32 bits)
{
	SensorCall();Fax3CodecState* sp = EncoderState(tif);
	int32 span;
        uint32 bs = 0;

	SensorCall();for (;;) {
		SensorCall();span = find0span(bp, bs, bits);		/* white span */
		putspan(tif, span, TIFFFaxWhiteCodes);
		bs += span;
		SensorCall();if (bs >= bits)
			{/*67*/SensorCall();break;/*68*/}
		SensorCall();span = find1span(bp, bs, bits);		/* black span */
		putspan(tif, span, TIFFFaxBlackCodes);
		bs += span;
		SensorCall();if (bs >= bits)
			{/*69*/SensorCall();break;/*70*/}
	}
	SensorCall();if (sp->b.mode & (FAXMODE_BYTEALIGN|FAXMODE_WORDALIGN)) {
		SensorCall();if (sp->bit != 8)			/* byte-align */
			Fax3FlushBits(tif, sp);
		SensorCall();if ((sp->b.mode&FAXMODE_WORDALIGN) &&
		    !isAligned(tif->tif_rawcp, uint16))
			Fax3FlushBits(tif, sp);
	}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

static const tableentry horizcode =
    { 3, 0x1, 0 };	/* 001 */
static const tableentry passcode =
    { 4, 0x1, 0 };	/* 0001 */
static const tableentry vcodes[7] = {
    { 7, 0x03, 0 },	/* 0000 011 */
    { 6, 0x03, 0 },	/* 0000 11 */
    { 3, 0x03, 0 },	/* 011 */
    { 1, 0x1, 0 },	/* 1 */
    { 3, 0x2, 0 },	/* 010 */
    { 6, 0x02, 0 },	/* 0000 10 */
    { 7, 0x02, 0 }	/* 0000 010 */
};

/*
 * 2d-encode a row of pixels.  Consult the CCITT
 * documentation for the algorithm.
 */
static int
Fax3Encode2DRow(TIFF* tif, unsigned char* bp, unsigned char* rp, uint32 bits)
{
#define	PIXEL(buf,ix)	((((buf)[(ix)>>3]) >> (7-((ix)&7))) & 1)
        SensorCall();uint32 a0 = 0;
	uint32 a1 = (PIXEL(bp, 0) != 0 ? 0 : finddiff(bp, 0, bits, 0));
	uint32 b1 = (PIXEL(rp, 0) != 0 ? 0 : finddiff(rp, 0, bits, 0));
	uint32 a2, b2;

	SensorCall();for (;;) {
		SensorCall();b2 = finddiff2(rp, b1, bits, PIXEL(rp,b1));
		SensorCall();if (b2 >= a1) {
			SensorCall();int32 d = b1 - a1;
			SensorCall();if (!(-3 <= d && d <= 3)) {	/* horizontal mode */
				SensorCall();a2 = finddiff2(bp, a1, bits, PIXEL(bp,a1));
				putcode(tif, &horizcode);
				SensorCall();if (a0+a1 == 0 || PIXEL(bp, a0) == 0) {
					SensorCall();putspan(tif, a1-a0, TIFFFaxWhiteCodes);
					putspan(tif, a2-a1, TIFFFaxBlackCodes);
				} else {
					SensorCall();putspan(tif, a1-a0, TIFFFaxBlackCodes);
					putspan(tif, a2-a1, TIFFFaxWhiteCodes);
				}
				SensorCall();a0 = a2;
			} else {			/* vertical mode */
				putcode(tif, &vcodes[d+3]);
				a0 = a1;
			}
		} else {				/* pass mode */
			putcode(tif, &passcode);
			a0 = b2;
		}
		SensorCall();if (a0 >= bits)
			{/*71*/SensorCall();break;/*72*/}
		SensorCall();a1 = finddiff(bp, a0, bits, PIXEL(bp,a0));
		b1 = finddiff(rp, a0, bits, !PIXEL(bp,a0));
		b1 = finddiff(rp, b1, bits, PIXEL(bp,a0));
	}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
#undef PIXEL
}

/*
 * Encode a buffer of pixels.
 */
static int
Fax3Encode(TIFF* tif, uint8* bp, tmsize_t cc, uint16 s)
{
	SensorCall();static const char module[] = "Fax3Encode";
	Fax3CodecState* sp = EncoderState(tif);
	(void) s;
	SensorCall();if (cc % sp->b.rowbytes)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "Fractional scanlines cannot be written");
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();while (cc > 0) {
		SensorCall();if ((sp->b.mode & FAXMODE_NOEOL) == 0)
			{/*73*/SensorCall();Fax3PutEOL(tif);/*74*/}
		SensorCall();if (is2DEncoding(sp)) {
			SensorCall();if (sp->tag == G3_1D) {
				SensorCall();if (!Fax3Encode1DRow(tif, bp, sp->b.rowpixels))
					{/*75*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*76*/}
				SensorCall();sp->tag = G3_2D;
			} else {
				SensorCall();if (!Fax3Encode2DRow(tif, bp, sp->refline,
				    sp->b.rowpixels))
					{/*77*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*78*/}
				SensorCall();sp->k--;
			}
			SensorCall();if (sp->k == 0) {
				SensorCall();sp->tag = G3_1D;
				sp->k = sp->maxk-1;
			} else
				{/*79*/SensorCall();_TIFFmemcpy(sp->refline, bp, sp->b.rowbytes);/*80*/}
		} else {
			SensorCall();if (!Fax3Encode1DRow(tif, bp, sp->b.rowpixels))
				{/*81*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*82*/}
		}
		SensorCall();bp += sp->b.rowbytes;
		cc -= sp->b.rowbytes;
	}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

static int
Fax3PostEncode(TIFF* tif)
{
	SensorCall();Fax3CodecState* sp = EncoderState(tif);

	SensorCall();if (sp->bit != 8)
		Fax3FlushBits(tif, sp);
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

static void
Fax3Close(TIFF* tif)
{
	SensorCall();if ((Fax3State(tif)->mode & FAXMODE_NORTC) == 0) {
		SensorCall();Fax3CodecState* sp = EncoderState(tif);
		unsigned int code = EOL;
		unsigned int length = 12;
		int i;

		SensorCall();if (is2DEncoding(sp))
			{/*83*/SensorCall();code = (code<<1) | (sp->tag == G3_1D), length++;/*84*/}
		SensorCall();for (i = 0; i < 6; i++)
			{/*85*/SensorCall();Fax3PutBits(tif, code, length);/*86*/}
		Fax3FlushBits(tif, sp);
	}
SensorCall();}

static void
Fax3Cleanup(TIFF* tif)
{
	SensorCall();Fax3CodecState* sp = DecoderState(tif);
	
	assert(sp != 0);

	tif->tif_tagmethods.vgetfield = sp->b.vgetparent;
	tif->tif_tagmethods.vsetfield = sp->b.vsetparent;
	tif->tif_tagmethods.printdir = sp->b.printdir;

	SensorCall();if (sp->runs)
		{/*87*/SensorCall();_TIFFfree(sp->runs);/*88*/}
	SensorCall();if (sp->refline)
		{/*89*/SensorCall();_TIFFfree(sp->refline);/*90*/}

	SensorCall();_TIFFfree(tif->tif_data);
	tif->tif_data = NULL;

	_TIFFSetDefaultCompressionState(tif);
SensorCall();}

#define	FIELD_BADFAXLINES	(FIELD_CODEC+0)
#define	FIELD_CLEANFAXDATA	(FIELD_CODEC+1)
#define	FIELD_BADFAXRUN		(FIELD_CODEC+2)

#define	FIELD_OPTIONS		(FIELD_CODEC+7)

static const TIFFField faxFields[] = {
    { TIFFTAG_FAXMODE, 0, 0, TIFF_ANY, 0, TIFF_SETGET_INT, TIFF_SETGET_UNDEFINED, FIELD_PSEUDO, FALSE, FALSE, "FaxMode", NULL },
    { TIFFTAG_FAXFILLFUNC, 0, 0, TIFF_ANY, 0, TIFF_SETGET_OTHER, TIFF_SETGET_UNDEFINED, FIELD_PSEUDO, FALSE, FALSE, "FaxFillFunc", NULL },
    { TIFFTAG_BADFAXLINES, 1, 1, TIFF_LONG, 0, TIFF_SETGET_UINT32, TIFF_SETGET_UINT32, FIELD_BADFAXLINES, TRUE, FALSE, "BadFaxLines", NULL },
    { TIFFTAG_CLEANFAXDATA, 1, 1, TIFF_SHORT, 0, TIFF_SETGET_UINT16, TIFF_SETGET_UINT16, FIELD_CLEANFAXDATA, TRUE, FALSE, "CleanFaxData", NULL },
    { TIFFTAG_CONSECUTIVEBADFAXLINES, 1, 1, TIFF_LONG, 0, TIFF_SETGET_UINT32, TIFF_SETGET_UINT32, FIELD_BADFAXRUN, TRUE, FALSE, "ConsecutiveBadFaxLines", NULL }};
static const TIFFField fax3Fields[] = {
    { TIFFTAG_GROUP3OPTIONS, 1, 1, TIFF_LONG, 0, TIFF_SETGET_UINT32, TIFF_SETGET_UINT32, FIELD_OPTIONS, FALSE, FALSE, "Group3Options", NULL },
};
static const TIFFField fax4Fields[] = {
    { TIFFTAG_GROUP4OPTIONS, 1, 1, TIFF_LONG, 0, TIFF_SETGET_UINT32, TIFF_SETGET_UINT32, FIELD_OPTIONS, FALSE, FALSE, "Group4Options", NULL },
};

static int
Fax3VSetField(TIFF* tif, uint32 tag, va_list ap)
{
	SensorCall();Fax3BaseState* sp = Fax3State(tif);
	const TIFFField* fip;

	assert(sp != 0);
	assert(sp->vsetparent != 0);

	SensorCall();switch (tag) {
	case TIFFTAG_FAXMODE:
		SensorCall();sp->mode = (int) va_arg(ap, int);
		{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}			/* NB: pseudo tag */
	case TIFFTAG_FAXFILLFUNC:
		DecoderState(tif)->fill = va_arg(ap, TIFFFaxFillFunc);
		{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}			/* NB: pseudo tag */
	case TIFFTAG_GROUP3OPTIONS:
		/* XXX: avoid reading options if compression mismatches. */
		SensorCall();if (tif->tif_dir.td_compression == COMPRESSION_CCITTFAX3)
			sp->groupoptions = (uint32) va_arg(ap, uint32);
		SensorCall();break;
	case TIFFTAG_GROUP4OPTIONS:
		/* XXX: avoid reading options if compression mismatches. */
		SensorCall();if (tif->tif_dir.td_compression == COMPRESSION_CCITTFAX4)
			sp->groupoptions = (uint32) va_arg(ap, uint32);
		SensorCall();break;
	case TIFFTAG_BADFAXLINES:
		SensorCall();sp->badfaxlines = (uint32) va_arg(ap, uint32);
		SensorCall();break;
	case TIFFTAG_CLEANFAXDATA:
		SensorCall();sp->cleanfaxdata = (uint16) va_arg(ap, uint16_vap);
		SensorCall();break;
	case TIFFTAG_CONSECUTIVEBADFAXLINES:
		SensorCall();sp->badfaxrun = (uint32) va_arg(ap, uint32);
		SensorCall();break;
	default:
		{int  ReplaceReturn = (*sp->vsetparent)(tif, tag, ap); SensorCall(); return ReplaceReturn;}
	}
	
	SensorCall();if ((fip = TIFFFieldWithTag(tif, tag)))
		TIFFSetFieldBit(tif, fip->field_bit);
	else
		{/*91*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*92*/}

	SensorCall();tif->tif_flags |= TIFF_DIRTYDIRECT;
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

static int
Fax3VGetField(TIFF* tif, uint32 tag, va_list ap)
{
	SensorCall();Fax3BaseState* sp = Fax3State(tif);

	assert(sp != 0);

	SensorCall();switch (tag) {
	case TIFFTAG_FAXMODE:
		SensorCall();*va_arg(ap, int*) = sp->mode;
		SensorCall();break;
	case TIFFTAG_FAXFILLFUNC:
		SensorCall();*va_arg(ap, TIFFFaxFillFunc*) = DecoderState(tif)->fill;
		SensorCall();break;
	case TIFFTAG_GROUP3OPTIONS:
	case TIFFTAG_GROUP4OPTIONS:
		SensorCall();*va_arg(ap, uint32*) = sp->groupoptions;
		SensorCall();break;
	case TIFFTAG_BADFAXLINES:
		SensorCall();*va_arg(ap, uint32*) = sp->badfaxlines;
		SensorCall();break;
	case TIFFTAG_CLEANFAXDATA:
		SensorCall();*va_arg(ap, uint16*) = sp->cleanfaxdata;
		SensorCall();break;
	case TIFFTAG_CONSECUTIVEBADFAXLINES:
		SensorCall();*va_arg(ap, uint32*) = sp->badfaxrun;
		SensorCall();break;
	default:
		{int  ReplaceReturn = (*sp->vgetparent)(tif, tag, ap); SensorCall(); return ReplaceReturn;}
	}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

static void
Fax3PrintDir(TIFF* tif, FILE* fd, long flags)
{
	SensorCall();Fax3BaseState* sp = Fax3State(tif);

	assert(sp != 0);

	(void) flags;
	SensorCall();if (TIFFFieldSet(tif,FIELD_OPTIONS)) {
		SensorCall();const char* sep = " ";
		SensorCall();if (tif->tif_dir.td_compression == COMPRESSION_CCITTFAX4) {
			SensorCall();fprintf(fd, "  Group 4 Options:");
			SensorCall();if (sp->groupoptions & GROUP4OPT_UNCOMPRESSED)
				{/*93*/SensorCall();fprintf(fd, "%suncompressed data", sep);/*94*/}
		} else {

			SensorCall();fprintf(fd, "  Group 3 Options:");
			SensorCall();if (sp->groupoptions & GROUP3OPT_2DENCODING)
				{/*95*/SensorCall();fprintf(fd, "%s2-d encoding", sep), sep = "+";/*96*/}
			SensorCall();if (sp->groupoptions & GROUP3OPT_FILLBITS)
				{/*97*/SensorCall();fprintf(fd, "%sEOL padding", sep), sep = "+";/*98*/}
			SensorCall();if (sp->groupoptions & GROUP3OPT_UNCOMPRESSED)
				{/*99*/SensorCall();fprintf(fd, "%suncompressed data", sep);/*100*/}
		}
		SensorCall();fprintf(fd, " (%lu = 0x%lx)\n",
                        (unsigned long) sp->groupoptions,
                        (unsigned long) sp->groupoptions);
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_CLEANFAXDATA)) {
		SensorCall();fprintf(fd, "  Fax Data:");
		SensorCall();switch (sp->cleanfaxdata) {
		case CLEANFAXDATA_CLEAN:
			SensorCall();fprintf(fd, " clean");
			SensorCall();break;
		case CLEANFAXDATA_REGENERATED:
			SensorCall();fprintf(fd, " receiver regenerated");
			SensorCall();break;
		case CLEANFAXDATA_UNCLEAN:
			SensorCall();fprintf(fd, " uncorrected errors");
			SensorCall();break;
		}
		SensorCall();fprintf(fd, " (%u = 0x%x)\n",
		    sp->cleanfaxdata, sp->cleanfaxdata);
	}
	SensorCall();if (TIFFFieldSet(tif,FIELD_BADFAXLINES))
		{/*101*/SensorCall();fprintf(fd, "  Bad Fax Lines: %lu\n",
                        (unsigned long) sp->badfaxlines);/*102*/}
	SensorCall();if (TIFFFieldSet(tif,FIELD_BADFAXRUN))
		{/*103*/SensorCall();fprintf(fd, "  Consecutive Bad Fax Lines: %lu\n",
		    (unsigned long) sp->badfaxrun);/*104*/}
	SensorCall();if (sp->printdir)
		{/*105*/SensorCall();(*sp->printdir)(tif, fd, flags);/*106*/}
SensorCall();}

static int
InitCCITTFax3(TIFF* tif)
{
	SensorCall();static const char module[] = "InitCCITTFax3";
	Fax3BaseState* sp;

	/*
	 * Merge codec-specific tag information.
	 */
	SensorCall();if (!_TIFFMergeFields(tif, faxFields, TIFFArrayCount(faxFields))) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, "InitCCITTFax3",
			"Merging common CCITT Fax codec-specific tags failed");
		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	}

	/*
	 * Allocate state block so tag methods have storage to record values.
	 */
	SensorCall();tif->tif_data = (uint8*)
		_TIFFmalloc(sizeof (Fax3CodecState));

	SensorCall();if (tif->tif_data == NULL) {
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module,
		    "No space for state block");
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}

	SensorCall();sp = Fax3State(tif);
        sp->rw_mode = tif->tif_mode;

	/*
	 * Override parent get/set field methods.
	 */
	sp->vgetparent = tif->tif_tagmethods.vgetfield;
	tif->tif_tagmethods.vgetfield = Fax3VGetField; /* hook for codec tags */
	sp->vsetparent = tif->tif_tagmethods.vsetfield;
	tif->tif_tagmethods.vsetfield = Fax3VSetField; /* hook for codec tags */
	sp->printdir = tif->tif_tagmethods.printdir;
	tif->tif_tagmethods.printdir = Fax3PrintDir;   /* hook for codec tags */
	sp->groupoptions = 0;	

	SensorCall();if (sp->rw_mode == O_RDONLY) /* FIXME: improve for in place update */
		tif->tif_flags |= TIFF_NOBITREV; /* decoder does bit reversal */
	DecoderState(tif)->runs = NULL;
	TIFFSetField(tif, TIFFTAG_FAXFILLFUNC, _TIFFFax3fillruns);
	EncoderState(tif)->refline = NULL;

	/*
	 * Install codec methods.
	 */
	tif->tif_fixuptags = Fax3FixupTags;
	tif->tif_setupdecode = Fax3SetupState;
	tif->tif_predecode = Fax3PreDecode;
	tif->tif_decoderow = Fax3Decode1D;
	tif->tif_decodestrip = Fax3Decode1D;
	tif->tif_decodetile = Fax3Decode1D;
	tif->tif_setupencode = Fax3SetupState;
	tif->tif_preencode = Fax3PreEncode;
	tif->tif_postencode = Fax3PostEncode;
	tif->tif_encoderow = Fax3Encode;
	tif->tif_encodestrip = Fax3Encode;
	tif->tif_encodetile = Fax3Encode;
	tif->tif_close = Fax3Close;
	tif->tif_cleanup = Fax3Cleanup;

	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

int
TIFFInitCCITTFax3(TIFF* tif, int scheme)
{
	SensorCall();(void) scheme;
	SensorCall();if (InitCCITTFax3(tif)) {
		/*
		 * Merge codec-specific tag information.
		 */
		SensorCall();if (!_TIFFMergeFields(tif, fax3Fields,
				      TIFFArrayCount(fax3Fields))) {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, "TIFFInitCCITTFax3",
			"Merging CCITT Fax 3 codec-specific tags failed");
			{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
		}

		/*
		 * The default format is Class/F-style w/o RTC.
		 */
		{int  ReplaceReturn = TIFFSetField(tif, TIFFTAG_FAXMODE, FAXMODE_CLASSF); SensorCall(); return ReplaceReturn;}
	} else
		{/*5*/{int  ReplaceReturn = 01; SensorCall(); return ReplaceReturn;}/*6*/}
SensorCall();}

/*
 * CCITT Group 4 (T.6) Facsimile-compatible
 * Compression Scheme Support.
 */

#define SWAP(t,a,b) { t x; x = (a); (a) = (b); (b) = x; }
/*
 * Decode the requested amount of G4-encoded data.
 */
static int
Fax4Decode(TIFF* tif, uint8* buf, tmsize_t occ, uint16 s)
{
SensorCall();	DECLARE_STATE_2D(tif, sp, "Fax4Decode");
	(void) s;
	SensorCall();if (occ % sp->b.rowbytes)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "Fractional scanlines cannot be read");
		{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
	}
	CACHE_STATE(tif, sp);
	SensorCall();while (occ > 0) {
		SensorCall();a0 = 0;
		RunLength = 0;
		pa = thisrun = sp->curruns;
		pb = sp->refruns;
		b1 = *pb++;
#ifdef FAX3_DEBUG
		printf("\nBitAcc=%08X, BitsAvail = %d\n", BitAcc, BitsAvail);
		printf("-------------------- %d\n", tif->tif_row);
		fflush(stdout);
#endif
		EXPAND2D(EOFG4);
                SensorCall();if (EOLcnt)
                    {/*107*/SensorCall();goto EOFG4;/*108*/}
		SensorCall();(*sp->fill)(buf, thisrun, pa, lastx);
		SETVALUE(0);		/* imaginary change for reference */
		SWAP(uint32*, sp->curruns, sp->refruns);
		buf += sp->b.rowbytes;
		occ -= sp->b.rowbytes;
		sp->line++;
		SensorCall();continue;
	EOFG4:
                NeedBits16( 13, BADG4 );
        BADG4:
#ifdef FAX3_DEBUG
                if( GetBits(13) != 0x1001 )
                    fputs( "Bad EOFB\n", stderr );
#endif                
                ClrBits( 13 );
		(*sp->fill)(buf, thisrun, pa, lastx);
		UNCACHE_STATE(tif, sp);
		return ( sp->line ? 1 : -1);	/* don't error on badly-terminated strips */
	}
	UNCACHE_STATE(tif, sp);
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}
#undef	SWAP

/*
 * Encode the requested amount of data.
 */
static int
Fax4Encode(TIFF* tif, uint8* bp, tmsize_t cc, uint16 s)
{
	SensorCall();static const char module[] = "Fax4Encode";
	Fax3CodecState *sp = EncoderState(tif);
	(void) s;
	SensorCall();if (cc % sp->b.rowbytes)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "Fractional scanlines cannot be written");
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();while (cc > 0) {
		SensorCall();if (!Fax3Encode2DRow(tif, bp, sp->refline, sp->b.rowpixels))
			{/*109*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*110*/}
		SensorCall();_TIFFmemcpy(sp->refline, bp, sp->b.rowbytes);
		bp += sp->b.rowbytes;
		cc -= sp->b.rowbytes;
	}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

static int
Fax4PostEncode(TIFF* tif)
{
	SensorCall();Fax3CodecState *sp = EncoderState(tif);

	/* terminate strip w/ EOFB */
	Fax3PutBits(tif, EOL, 12);
	Fax3PutBits(tif, EOL, 12);
	SensorCall();if (sp->bit != 8)
		Fax3FlushBits(tif, sp);
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

int
TIFFInitCCITTFax4(TIFF* tif, int scheme)
{
	SensorCall();(void) scheme;
	SensorCall();if (InitCCITTFax3(tif)) {		/* reuse G3 support */
		/*
		 * Merge codec-specific tag information.
		 */
		SensorCall();if (!_TIFFMergeFields(tif, fax4Fields,
				      TIFFArrayCount(fax4Fields))) {
			SensorCall();TIFFErrorExt(tif->tif_clientdata, "TIFFInitCCITTFax4",
			"Merging CCITT Fax 4 codec-specific tags failed");
			{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
		}

		SensorCall();tif->tif_decoderow = Fax4Decode;
		tif->tif_decodestrip = Fax4Decode;
		tif->tif_decodetile = Fax4Decode;
		tif->tif_encoderow = Fax4Encode;
		tif->tif_encodestrip = Fax4Encode;
		tif->tif_encodetile = Fax4Encode;
		tif->tif_postencode = Fax4PostEncode;
		/*
		 * Suppress RTC at the end of each strip.
		 */
		{int  ReplaceReturn = TIFFSetField(tif, TIFFTAG_FAXMODE, FAXMODE_NORTC); SensorCall(); return ReplaceReturn;}
	} else
		{/*7*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*8*/}
SensorCall();}

/*
 * CCITT Group 3 1-D Modified Huffman RLE Compression Support.
 * (Compression algorithms 2 and 32771)
 */

/*
 * Decode the requested amount of RLE-encoded data.
 */
static int
Fax3DecodeRLE(TIFF* tif, uint8* buf, tmsize_t occ, uint16 s)
{
SensorCall();	DECLARE_STATE(tif, sp, "Fax3DecodeRLE");
	int mode = sp->b.mode;
	(void) s;
	SensorCall();if (occ % sp->b.rowbytes)
	{
		SensorCall();TIFFErrorExt(tif->tif_clientdata, module, "Fractional scanlines cannot be read");
		{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
	}
	CACHE_STATE(tif, sp);
	SensorCall();thisrun = sp->curruns;
	SensorCall();while (occ > 0) {
		SensorCall();a0 = 0;
		RunLength = 0;
		pa = thisrun;
#ifdef FAX3_DEBUG
		printf("\nBitAcc=%08X, BitsAvail = %d\n", BitAcc, BitsAvail);
		printf("-------------------- %d\n", tif->tif_row);
		fflush(stdout);
#endif
		EXPAND1D(EOFRLE);
		(*sp->fill)(buf, thisrun, pa, lastx);
		/*
		 * Cleanup at the end of the row.
		 */
		SensorCall();if (mode & FAXMODE_BYTEALIGN) {
			SensorCall();int n = BitsAvail - (BitsAvail &~ 7);
			ClrBits(n);
		} else {/*111*/SensorCall();if (mode & FAXMODE_WORDALIGN) {
			SensorCall();int n = BitsAvail - (BitsAvail &~ 15);
			ClrBits(n);
			SensorCall();if (BitsAvail == 0 && !isAligned(cp, uint16))
			    {/*113*/SensorCall();cp++;/*114*/}
		;/*112*/}}
		SensorCall();buf += sp->b.rowbytes;
		occ -= sp->b.rowbytes;
		sp->line++;
		SensorCall();continue;
	EOFRLE:				/* premature EOF */
		(*sp->fill)(buf, thisrun, pa, lastx);
		UNCACHE_STATE(tif, sp);
		return (-1);
	}
	UNCACHE_STATE(tif, sp);
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

int
TIFFInitCCITTRLE(TIFF* tif, int scheme)
{
	SensorCall();(void) scheme;
	SensorCall();if (InitCCITTFax3(tif)) {		/* reuse G3 support */
		SensorCall();tif->tif_decoderow = Fax3DecodeRLE;
		tif->tif_decodestrip = Fax3DecodeRLE;
		tif->tif_decodetile = Fax3DecodeRLE;
		/*
		 * Suppress RTC+EOLs when encoding and byte-align data.
		 */
		{int  ReplaceReturn = TIFFSetField(tif, TIFFTAG_FAXMODE,
		    FAXMODE_NORTC|FAXMODE_NOEOL|FAXMODE_BYTEALIGN); SensorCall(); return ReplaceReturn;}
	} else
		{/*1*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*2*/}
SensorCall();}

int
TIFFInitCCITTRLEW(TIFF* tif, int scheme)
{
	SensorCall();(void) scheme;
	SensorCall();if (InitCCITTFax3(tif)) {		/* reuse G3 support */
		SensorCall();tif->tif_decoderow = Fax3DecodeRLE;
		tif->tif_decodestrip = Fax3DecodeRLE;
		tif->tif_decodetile = Fax3DecodeRLE;  
		/*
		 * Suppress RTC+EOLs when encoding and word-align data.
		 */
		{int  ReplaceReturn = TIFFSetField(tif, TIFFTAG_FAXMODE,
		    FAXMODE_NORTC|FAXMODE_NOEOL|FAXMODE_WORDALIGN); SensorCall(); return ReplaceReturn;}
	} else
		{/*3*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*4*/}
SensorCall();}
#endif /* CCITT_SUPPORT */

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
