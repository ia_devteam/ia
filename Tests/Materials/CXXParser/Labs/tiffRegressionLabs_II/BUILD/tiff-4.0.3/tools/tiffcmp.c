/* $Id: tiffcmp.c,v 1.16 2010-03-10 18:56:50 bfriesen Exp $ */

/*
 * Copyright (c) 1988-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

#include "tif_config.h"
#include "/var/tmp/sensor.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifdef NEED_LIBPORT
# include "libport.h"
#endif

#include "tiffio.h"

#ifndef HAVE_GETOPT
extern int getopt(int, char**, char*);
#endif

static	int stopondiff = 1;
static	int stoponfirsttag = 1;
static	uint16 bitspersample = 1;
static	uint16 samplesperpixel = 1;
static	uint16 sampleformat = SAMPLEFORMAT_UINT;
static	uint32 imagewidth;
static	uint32 imagelength;

static	void usage(void);
static	int tiffcmp(TIFF*, TIFF*);
static	int cmptags(TIFF*, TIFF*);
static	int ContigCompare(int, uint32, unsigned char*, unsigned char*, tsize_t);
static	int SeparateCompare(int, int, uint32, unsigned char*, unsigned char*);
static	void PrintIntDiff(uint32, int, uint32, uint32, uint32);
static	void PrintFloatDiff(uint32, int, uint32, double, double);

static	void leof(const char*, uint32, int);

int
main(int argc, char* argv[])
{
	SensorCall();TIFF *tif1, *tif2;
	int c, dirnum;
	extern int optind;
	extern char* optarg;

	SensorCall();while ((c = getopt(argc, argv, "ltz:")) != -1)
		{/*63*/SensorCall();switch (c) {
		case 'l':
			SensorCall();stopondiff = 0;
			SensorCall();break;
		case 'z':
			SensorCall();stopondiff = atoi(optarg);
			SensorCall();break;
		case 't':
			SensorCall();stoponfirsttag = 0;
			SensorCall();break;
		case '?':
			SensorCall();usage();
			/*NOTREACHED*/
		;/*64*/}}
	SensorCall();if (argc - optind < 2)
		{/*65*/SensorCall();usage();/*66*/}
	SensorCall();tif1 = TIFFOpen(argv[optind], "r");
	SensorCall();if (tif1 == NULL)
		{/*67*/{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}/*68*/}
	SensorCall();tif2 = TIFFOpen(argv[optind+1], "r");
	SensorCall();if (tif2 == NULL)
		{/*69*/{int  ReplaceReturn = (-2); SensorCall(); return ReplaceReturn;}/*70*/}
	SensorCall();dirnum = 0;
	SensorCall();while (tiffcmp(tif1, tif2)) {
		SensorCall();if (!TIFFReadDirectory(tif1)) {
			SensorCall();if (!TIFFReadDirectory(tif2))
				{/*71*/SensorCall();break;/*72*/}
			SensorCall();printf("No more directories for %s\n",
			    TIFFFileName(tif1));
			{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		} else {/*73*/SensorCall();if (!TIFFReadDirectory(tif2)) {
			SensorCall();printf("No more directories for %s\n",
			    TIFFFileName(tif2));
			{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		;/*74*/}}
		SensorCall();printf("Directory %d:\n", ++dirnum);
	}

	SensorCall();TIFFClose(tif1);
	TIFFClose(tif2);
	{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
}

char* stuff[] = {
"usage: tiffcmp [options] file1 file2",
"where options are:",
" -l		list each byte of image data that differs between the files",
" -z #		list specified number of bytes that differs between the files",
" -t		ignore any differences in directory tags",
NULL
};

static void
usage(void)
{
	SensorCall();char buf[BUFSIZ];
	int i;

	setbuf(stderr, buf);
        fprintf(stderr, "%s\n\n", TIFFGetVersion());
	SensorCall();for (i = 0; stuff[i] != NULL; i++)
		{/*1*/SensorCall();fprintf(stderr, "%s\n", stuff[i]);/*2*/}
	SensorCall();exit(-1);
SensorCall();}

#define	checkEOF(tif, row, sample) { \
	leof(TIFFFileName(tif), row, sample); \
	goto bad; \
}

static	int CheckShortTag(TIFF*, TIFF*, int, char*);
static	int CheckShort2Tag(TIFF*, TIFF*, int, char*);
static	int CheckShortArrayTag(TIFF*, TIFF*, int, char*);
static	int CheckLongTag(TIFF*, TIFF*, int, char*);
static	int CheckFloatTag(TIFF*, TIFF*, int, char*);
static	int CheckStringTag(TIFF*, TIFF*, int, char*);

static int
tiffcmp(TIFF* tif1, TIFF* tif2)
{
	SensorCall();uint16 config1, config2;
	tsize_t size1;
	uint32 row;
	tsample_t s;
	unsigned char *buf1, *buf2;

	SensorCall();if (!CheckShortTag(tif1, tif2, TIFFTAG_BITSPERSAMPLE, "BitsPerSample"))
		{/*3*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*4*/}
	SensorCall();if (!CheckShortTag(tif1, tif2, TIFFTAG_SAMPLESPERPIXEL, "SamplesPerPixel"))
		{/*5*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*6*/}
	SensorCall();if (!CheckLongTag(tif1, tif2, TIFFTAG_IMAGEWIDTH, "ImageWidth"))
		{/*7*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*8*/}
	SensorCall();if (!cmptags(tif1, tif2))
		{/*9*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*10*/}
	SensorCall();(void) TIFFGetField(tif1, TIFFTAG_BITSPERSAMPLE, &bitspersample);
	(void) TIFFGetField(tif1, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
	(void) TIFFGetField(tif1, TIFFTAG_SAMPLEFORMAT, &sampleformat);
	(void) TIFFGetField(tif1, TIFFTAG_IMAGEWIDTH, &imagewidth);
	(void) TIFFGetField(tif1, TIFFTAG_IMAGELENGTH, &imagelength);
	(void) TIFFGetField(tif1, TIFFTAG_PLANARCONFIG, &config1);
	(void) TIFFGetField(tif2, TIFFTAG_PLANARCONFIG, &config2);
	buf1 = (unsigned char *)_TIFFmalloc(size1 = TIFFScanlineSize(tif1));
	buf2 = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(tif2));
	SensorCall();if (buf1 == NULL || buf2 == NULL) {
		SensorCall();fprintf(stderr, "No space for scanline buffers\n");
		exit(-1);
	}
	SensorCall();if (config1 != config2 && bitspersample != 8 && samplesperpixel > 1) {
		SensorCall();fprintf(stderr,
"Can't handle different planar configuration w/ different bits/sample\n");
		SensorCall();goto bad;
	}
#define	pack(a,b)	((a)<<8)|(b)
	SensorCall();switch (pack(config1, config2)) {
	case pack(PLANARCONFIG_SEPARATE, PLANARCONFIG_CONTIG):
		SensorCall();for (row = 0; row < imagelength; row++) {
			if (TIFFReadScanline(tif2, buf2, row, 0) < 0)
				checkEOF(tif2, row, -1)
			SensorCall();for (s = 0; s < samplesperpixel; s++) {
				if (TIFFReadScanline(tif1, buf1, row, s) < 0)
					checkEOF(tif1, row, s)
				SensorCall();if (SeparateCompare(1, s, row, buf2, buf1) < 0)
					{/*11*/SensorCall();goto bad1;/*12*/}
			}
		}
		SensorCall();break;
	case pack(PLANARCONFIG_CONTIG, PLANARCONFIG_SEPARATE):
		SensorCall();for (row = 0; row < imagelength; row++) {
			if (TIFFReadScanline(tif1, buf1, row, 0) < 0)
				checkEOF(tif1, row, -1)
			SensorCall();for (s = 0; s < samplesperpixel; s++) {
				if (TIFFReadScanline(tif2, buf2, row, s) < 0)
					checkEOF(tif2, row, s)
				SensorCall();if (SeparateCompare(0, s, row, buf1, buf2) < 0)
					{/*13*/SensorCall();goto bad1;/*14*/}
			}
		}
		SensorCall();break;
	case pack(PLANARCONFIG_SEPARATE, PLANARCONFIG_SEPARATE):
		SensorCall();for (s = 0; s < samplesperpixel; s++)
			{/*15*/SensorCall();for (row = 0; row < imagelength; row++) {
				if (TIFFReadScanline(tif1, buf1, row, s) < 0)
					checkEOF(tif1, row, s)
				if (TIFFReadScanline(tif2, buf2, row, s) < 0)
					checkEOF(tif2, row, s)
				SensorCall();if (ContigCompare(s, row, buf1, buf2, size1) < 0)
					{/*17*/SensorCall();goto bad1;/*18*/}
			;/*16*/}}
		SensorCall();break;
	case pack(PLANARCONFIG_CONTIG, PLANARCONFIG_CONTIG):
		SensorCall();for (row = 0; row < imagelength; row++) {
			if (TIFFReadScanline(tif1, buf1, row, 0) < 0)
				checkEOF(tif1, row, -1)
			if (TIFFReadScanline(tif2, buf2, row, 0) < 0)
				checkEOF(tif2, row, -1)
			SensorCall();if (ContigCompare(-1, row, buf1, buf2, size1) < 0)
				{/*19*/SensorCall();goto bad1;/*20*/}
		}
		SensorCall();break;
	}
	SensorCall();if (buf1) {/*21*/SensorCall();_TIFFfree(buf1);/*22*/}
	SensorCall();if (buf2) {/*23*/SensorCall();_TIFFfree(buf2);/*24*/}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
bad:
	if (stopondiff)
		{/*25*/exit(1);/*26*/}
bad1:
	if (buf1) {/*27*/_TIFFfree(buf1);/*28*/}
	if (buf2) {/*29*/_TIFFfree(buf2);/*30*/}
	return (0);
}

#define	CmpShortField(tag, name) \
	if (!CheckShortTag(tif1, tif2, tag, name) && stoponfirsttag) return (0)
#define	CmpShortField2(tag, name) \
	if (!CheckShort2Tag(tif1, tif2, tag, name) && stoponfirsttag) return (0)
#define	CmpLongField(tag, name) \
	if (!CheckLongTag(tif1, tif2, tag, name) && stoponfirsttag) return (0)
#define	CmpFloatField(tag, name) \
	if (!CheckFloatTag(tif1, tif2, tag, name) && stoponfirsttag) return (0)
#define	CmpStringField(tag, name) \
	if (!CheckStringTag(tif1, tif2, tag, name) && stoponfirsttag) return (0)
#define	CmpShortArrayField(tag, name) \
	if (!CheckShortArrayTag(tif1, tif2, tag, name) && stoponfirsttag) return (0)

static int
cmptags(TIFF* tif1, TIFF* tif2)
{
SensorCall();	CmpLongField(TIFFTAG_SUBFILETYPE,	"SubFileType");
	CmpLongField(TIFFTAG_IMAGEWIDTH,	"ImageWidth");
	CmpLongField(TIFFTAG_IMAGELENGTH,	"ImageLength");
	CmpShortField(TIFFTAG_BITSPERSAMPLE,	"BitsPerSample");
	CmpShortField(TIFFTAG_COMPRESSION,	"Compression");
	CmpShortField(TIFFTAG_PREDICTOR,	"Predictor");
	CmpShortField(TIFFTAG_PHOTOMETRIC,	"PhotometricInterpretation");
	CmpShortField(TIFFTAG_THRESHHOLDING,	"Thresholding");
	CmpShortField(TIFFTAG_FILLORDER,	"FillOrder");
	CmpShortField(TIFFTAG_ORIENTATION,	"Orientation");
	CmpShortField(TIFFTAG_SAMPLESPERPIXEL,	"SamplesPerPixel");
	CmpShortField(TIFFTAG_MINSAMPLEVALUE,	"MinSampleValue");
	CmpShortField(TIFFTAG_MAXSAMPLEVALUE,	"MaxSampleValue");
	CmpShortField(TIFFTAG_SAMPLEFORMAT,	"SampleFormat");
	CmpFloatField(TIFFTAG_XRESOLUTION,	"XResolution");
	CmpFloatField(TIFFTAG_YRESOLUTION,	"YResolution");
	CmpLongField(TIFFTAG_GROUP3OPTIONS,	"Group3Options");
	CmpLongField(TIFFTAG_GROUP4OPTIONS,	"Group4Options");
	CmpShortField(TIFFTAG_RESOLUTIONUNIT,	"ResolutionUnit");
	CmpShortField(TIFFTAG_PLANARCONFIG,	"PlanarConfiguration");
	CmpLongField(TIFFTAG_ROWSPERSTRIP,	"RowsPerStrip");
	CmpFloatField(TIFFTAG_XPOSITION,	"XPosition");
	CmpFloatField(TIFFTAG_YPOSITION,	"YPosition");
	CmpShortField(TIFFTAG_GRAYRESPONSEUNIT, "GrayResponseUnit");
	CmpShortField(TIFFTAG_COLORRESPONSEUNIT, "ColorResponseUnit");
#ifdef notdef
	{ uint16 *graycurve;
	  CmpField(TIFFTAG_GRAYRESPONSECURVE, graycurve);
	}
	{ uint16 *red, *green, *blue;
	  CmpField3(TIFFTAG_COLORRESPONSECURVE, red, green, blue);
	}
	{ uint16 *red, *green, *blue;
	  CmpField3(TIFFTAG_COLORMAP, red, green, blue);
	}
#endif
	CmpShortField2(TIFFTAG_PAGENUMBER,	"PageNumber");
	CmpStringField(TIFFTAG_ARTIST,		"Artist");
	CmpStringField(TIFFTAG_IMAGEDESCRIPTION,"ImageDescription");
	CmpStringField(TIFFTAG_MAKE,		"Make");
	CmpStringField(TIFFTAG_MODEL,		"Model");
	CmpStringField(TIFFTAG_SOFTWARE,	"Software");
	CmpStringField(TIFFTAG_DATETIME,	"DateTime");
	CmpStringField(TIFFTAG_HOSTCOMPUTER,	"HostComputer");
	CmpStringField(TIFFTAG_PAGENAME,	"PageName");
	CmpStringField(TIFFTAG_DOCUMENTNAME,	"DocumentName");
	CmpShortField(TIFFTAG_MATTEING,		"Matteing");
	CmpShortArrayField(TIFFTAG_EXTRASAMPLES,"ExtraSamples");
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

static int
ContigCompare(int sample, uint32 row,
	      unsigned char* p1, unsigned char* p2, tsize_t size)
{
    SensorCall();uint32 pix;
    int ppb = 8 / bitspersample;
    int	 samples_to_test;

    SensorCall();if (memcmp(p1, p2, size) == 0)
        {/*31*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*32*/}

    SensorCall();samples_to_test = (sample == -1) ? samplesperpixel : 1;

    SensorCall();switch (bitspersample) {
      case 1: case 2: case 4: case 8: 
      {
          SensorCall();unsigned char *pix1 = p1, *pix2 = p2;

          SensorCall();for (pix = 0; pix < imagewidth; pix += ppb) {
              SensorCall();int		s;

              SensorCall();for(s = 0; s < samples_to_test; s++) {
                  SensorCall();if (*pix1 != *pix2) {
                      SensorCall();if( sample == -1 )
                          {/*33*/SensorCall();PrintIntDiff(row, s, pix, *pix1, *pix2);/*34*/}
                      else
                          {/*35*/SensorCall();PrintIntDiff(row, sample, pix, *pix1, *pix2);/*36*/}
                  }

                  SensorCall();pix1++;
                  pix2++;
              }
          }
          SensorCall();break;
      }
      case 16: 
      {
          SensorCall();uint16 *pix1 = (uint16 *)p1, *pix2 = (uint16 *)p2;

          SensorCall();for (pix = 0; pix < imagewidth; pix++) {
              SensorCall();int	s;

              SensorCall();for(s = 0; s < samples_to_test; s++) {
                  SensorCall();if (*pix1 != *pix2)
                      {/*37*/SensorCall();PrintIntDiff(row, sample, pix, *pix1, *pix2);/*38*/}
                        
                  SensorCall();pix1++;
                  pix2++;
              }
          }
          SensorCall();break;
      }
      case 32: 
	SensorCall();if (sampleformat == SAMPLEFORMAT_UINT
	    || sampleformat == SAMPLEFORMAT_INT) {
		SensorCall();uint32 *pix1 = (uint32 *)p1, *pix2 = (uint32 *)p2;

		SensorCall();for (pix = 0; pix < imagewidth; pix++) {
			SensorCall();int	s;

			SensorCall();for(s = 0; s < samples_to_test; s++) {
				SensorCall();if (*pix1 != *pix2) {
					SensorCall();PrintIntDiff(row, sample, pix,
						     *pix1, *pix2);
				}
                        
				SensorCall();pix1++;
				pix2++;
			}
		}
	} else {/*39*/SensorCall();if (sampleformat == SAMPLEFORMAT_IEEEFP) {
		SensorCall();float *pix1 = (float *)p1, *pix2 = (float *)p2;

		SensorCall();for (pix = 0; pix < imagewidth; pix++) {
			SensorCall();int	s;

			SensorCall();for(s = 0; s < samples_to_test; s++) {
				SensorCall();if (fabs(*pix1 - *pix2) < 0.000000000001) {
					SensorCall();PrintFloatDiff(row, sample, pix,
						       *pix1, *pix2);
				}
                        
				SensorCall();pix1++;
				pix2++;
			}
		}
	} else {
		  SensorCall();fprintf(stderr, "Sample format %d is not supported.\n",
			  sampleformat);
		  {int  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}
	;/*40*/}}
        SensorCall();break;
      default:
	SensorCall();fprintf(stderr, "Bit depth %d is not supported.\n", bitspersample);
	{int  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}
    }

    {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

static void
PrintIntDiff(uint32 row, int sample, uint32 pix, uint32 w1, uint32 w2)
{
	SensorCall();if (sample < 0)
		{/*47*/SensorCall();sample = 0;/*48*/}
	SensorCall();switch (bitspersample) {
	case 1:
	case 2:
	case 4:
	    {
		SensorCall();int32 mask1, mask2, s;

		mask1 =  ~((-1) << bitspersample);
		s = (8 - bitspersample);
		mask2 = mask1 << s;
		SensorCall();for (; mask2 && pix < imagewidth;
		     mask2 >>= bitspersample, s -= bitspersample, pix++) {
			SensorCall();if ((w1 & mask2) ^ (w2 & mask2)) {
				SensorCall();printf(
			"Scanline %lu, pixel %lu, sample %d: %01x %01x\n",
	    				(unsigned long) row,
					(unsigned long) pix,
					sample,
					(unsigned int)((w1 >> s) & mask1),
					(unsigned int)((w2 >> s) & mask1));
				SensorCall();if (--stopondiff == 0)
					{/*49*/SensorCall();exit(1);/*50*/}
			}
		}
		SensorCall();break;
	    }
	case 8: 
		SensorCall();printf("Scanline %lu, pixel %lu, sample %d: %02x %02x\n",
		       (unsigned long) row, (unsigned long) pix, sample,
		       (unsigned int) w1, (unsigned int) w2);
		SensorCall();if (--stopondiff == 0)
			{/*51*/SensorCall();exit(1);/*52*/}
		SensorCall();break;
	case 16:
		SensorCall();printf("Scanline %lu, pixel %lu, sample %d: %04x %04x\n",
		    (unsigned long) row, (unsigned long) pix, sample,
		    (unsigned int) w1, (unsigned int) w2);
		SensorCall();if (--stopondiff == 0)
			{/*53*/SensorCall();exit(1);/*54*/}
		SensorCall();break;
	case 32:
		SensorCall();printf("Scanline %lu, pixel %lu, sample %d: %08x %08x\n",
		    (unsigned long) row, (unsigned long) pix, sample,
		    (unsigned int) w1, (unsigned int) w2);
		SensorCall();if (--stopondiff == 0)
			{/*55*/SensorCall();exit(1);/*56*/}
		SensorCall();break;
	default:
		SensorCall();break;
	}
SensorCall();}

static void
PrintFloatDiff(uint32 row, int sample, uint32 pix, double w1, double w2)
{
	SensorCall();if (sample < 0)
		{/*57*/SensorCall();sample = 0;/*58*/}
	SensorCall();switch (bitspersample) {
	case 32: 
		SensorCall();printf("Scanline %lu, pixel %lu, sample %d: %g %g\n",
		    (long) row, (long) pix, sample, w1, w2);
		SensorCall();if (--stopondiff == 0)
			{/*59*/SensorCall();exit(1);/*60*/}
		SensorCall();break;
	default:
		SensorCall();break;
	}
SensorCall();}

static int
SeparateCompare(int reversed, int sample, uint32 row,
		unsigned char* cp1, unsigned char* p2)
{
	SensorCall();uint32 npixels = imagewidth;
	int pixel;

	cp1 += sample;
	SensorCall();for (pixel = 0; npixels-- > 0; pixel++, cp1 += samplesperpixel, p2++) {
		SensorCall();if (*cp1 != *p2) {
			SensorCall();printf("Scanline %lu, pixel %lu, sample %ld: ",
			    (long) row, (long) pixel, (long) sample);
			SensorCall();if (reversed)
				{/*41*/SensorCall();printf("%02x %02x\n", *p2, *cp1);/*42*/}
			else
				{/*43*/SensorCall();printf("%02x %02x\n", *cp1, *p2);/*44*/}
			SensorCall();if (--stopondiff == 0)
				{/*45*/SensorCall();exit(1);/*46*/}
		}
	}

	{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

static int
checkTag(TIFF* tif1, TIFF* tif2, int tag, char* name, void* p1, void* p2)
{

	SensorCall();if (TIFFGetField(tif1, tag, p1)) {
		SensorCall();if (!TIFFGetField(tif2, tag, p2)) {
			SensorCall();printf("%s tag appears only in %s\n",
			    name, TIFFFileName(tif1));
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	} else {/*97*/SensorCall();if (TIFFGetField(tif2, tag, p2)) {
		SensorCall();printf("%s tag appears only in %s\n", name, TIFFFileName(tif2));
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	;/*98*/}}
	{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
}

#define	CHECK(cmp, fmt) {				\
	switch (checkTag(tif1,tif2,tag,name,&v1,&v2)) {	\
	case 1:	if (cmp)				\
	case -1:	return (1);			\
		printf(fmt, name, v1, v2);		\
	}						\
	return (0);					\
}

static int
CheckShortTag(TIFF* tif1, TIFF* tif2, int tag, char* name)
{
	SensorCall();uint16 v1, v2;
	CHECK(v1 == v2, "%s: %u %u\n");
}

static int
CheckShort2Tag(TIFF* tif1, TIFF* tif2, int tag, char* name)
{
	SensorCall();uint16 v11, v12, v21, v22;

	SensorCall();if (TIFFGetField(tif1, tag, &v11, &v12)) {
		SensorCall();if (!TIFFGetField(tif2, tag, &v21, &v22)) {
			SensorCall();printf("%s tag appears only in %s\n",
			    name, TIFFFileName(tif1));
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
		SensorCall();if (v11 == v21 && v12 == v22)
			{/*75*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*76*/}
		SensorCall();printf("%s: <%u,%u> <%u,%u>\n", name, v11, v12, v21, v22);
	} else {/*77*/SensorCall();if (TIFFGetField(tif2, tag, &v21, &v22))
		{/*79*/SensorCall();printf("%s tag appears only in %s\n", name, TIFFFileName(tif2));/*80*/}
	else
		{/*81*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*82*/}/*78*/}
	{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
}

static int
CheckShortArrayTag(TIFF* tif1, TIFF* tif2, int tag, char* name)
{
	SensorCall();uint16 n1, *a1;
	uint16 n2, *a2;

	SensorCall();if (TIFFGetField(tif1, tag, &n1, &a1)) {
		SensorCall();if (!TIFFGetField(tif2, tag, &n2, &a2)) {
			SensorCall();printf("%s tag appears only in %s\n",
			    name, TIFFFileName(tif1));
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
		SensorCall();if (n1 == n2) {
			SensorCall();char* sep;
			uint16 i;

			SensorCall();if (memcmp(a1, a2, n1 * sizeof(uint16)) == 0)
				{/*83*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*84*/}
			SensorCall();printf("%s: value mismatch, <%u:", name, n1);
			sep = "";
			SensorCall();for (i = 0; i < n1; i++)
				{/*85*/SensorCall();printf("%s%u", sep, a1[i]), sep = ",";/*86*/}
			SensorCall();printf("> and <%u: ", n2);
			sep = "";
			SensorCall();for (i = 0; i < n2; i++)
				{/*87*/SensorCall();printf("%s%u", sep, a2[i]), sep = ",";/*88*/}
			SensorCall();printf(">\n");
		} else
			{/*89*/SensorCall();printf("%s: %u items in %s, %u items in %s", name,
			    n1, TIFFFileName(tif1),
			    n2, TIFFFileName(tif2)
			);/*90*/}
	} else {/*91*/SensorCall();if (TIFFGetField(tif2, tag, &n2, &a2))
		{/*93*/SensorCall();printf("%s tag appears only in %s\n", name, TIFFFileName(tif2));/*94*/}
	else
		{/*95*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*96*/}/*92*/}
	{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
}

static int
CheckLongTag(TIFF* tif1, TIFF* tif2, int tag, char* name)
{
	SensorCall();uint32 v1, v2;
	CHECK(v1 == v2, "%s: %u %u\n");
}

static int
CheckFloatTag(TIFF* tif1, TIFF* tif2, int tag, char* name)
{
	SensorCall();float v1, v2;
	CHECK(v1 == v2, "%s: %g %g\n");
}

static int
CheckStringTag(TIFF* tif1, TIFF* tif2, int tag, char* name)
{
	SensorCall();char *v1, *v2;
	CHECK(strcmp(v1, v2) == 0, "%s: \"%s\" \"%s\"\n");
}

static void
leof(const char* name, uint32 row, int s)
{

	SensorCall();printf("%s: EOF at scanline %lu", name, (unsigned long)row);
	SensorCall();if (s >= 0)
		{/*61*/SensorCall();printf(", sample %d", s);/*62*/}
	SensorCall();printf("\n");
SensorCall();}

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
