/* $Id: tiffinfo.c,v 1.21 2012-06-06 06:05:29 fwarmerdam Exp $ */

/*
 * Copyright (c) 1988-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

#include "tif_config.h"
#include "/var/tmp/sensor.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef HAVE_STRINGS_H
# include <strings.h>
#endif

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifdef NEED_LIBPORT
# include "libport.h"
#endif

#include "tiffiop.h"

static TIFFErrorHandler old_error_handler = 0;
static int status = 0;                  /* exit status */
static int showdata = 0;		/* show data */
static int rawdata = 0;			/* show raw/decoded data */
static int showwords = 0;		/* show data as bytes/words */
static int readdata = 0;		/* read data in file */
static int stoponerr = 1;		/* stop on first read error */

static	void usage(void);
static	void tiffinfo(TIFF*, uint16, long, int);

static void
PrivateErrorHandler(const char* module, const char* fmt, va_list ap)
{
        SensorCall();if (old_error_handler)
                {/*9*/SensorCall();(*old_error_handler)(module,fmt,ap);/*10*/}
	SensorCall();status = 1;
SensorCall();}

int
main(int argc, char* argv[])
{
	SensorCall();int dirnum = -1, multiplefiles, c;
	uint16 order = 0;
	TIFF* tif;
	extern int optind;
	extern char* optarg;
	long flags = 0;
	uint64 diroff = 0;
	int chopstrips = 0;		/* disable strip chopping */

	SensorCall();while ((c = getopt(argc, argv, "f:o:cdDSjilmrsvwz0123456789")) != -1)
		{/*11*/SensorCall();switch (c) {
		case '0': case '1': case '2': case '3':
		case '4': case '5': case '6': case '7':
		case '8': case '9':
			SensorCall();dirnum = atoi(&argv[optind-1][1]);
			SensorCall();break;
		case 'd':
			SensorCall();showdata++;
			/* fall thru... */
		case 'D':
			SensorCall();readdata++;
			SensorCall();break;
		case 'c':
			SensorCall();flags |= TIFFPRINT_COLORMAP | TIFFPRINT_CURVES;
			SensorCall();break;
		case 'f':		/* fill order */
			SensorCall();if (streq(optarg, "lsb2msb"))
				order = FILLORDER_LSB2MSB;
			else {/*13*/SensorCall();if (streq(optarg, "msb2lsb"))
				order = FILLORDER_MSB2LSB;
			else
				{/*15*/SensorCall();usage();/*16*/}/*14*/}
			SensorCall();break;
		case 'i':
			SensorCall();stoponerr = 0;
			SensorCall();break;
		case 'o':
			SensorCall();diroff = strtoul(optarg, NULL, 0);
			SensorCall();break;
		case 'j':
			SensorCall();flags |= TIFFPRINT_JPEGQTABLES |
				 TIFFPRINT_JPEGACTABLES |
				 TIFFPRINT_JPEGDCTABLES;
			SensorCall();break;
		case 'r':
			SensorCall();rawdata = 1;
			SensorCall();break;
		case 's':
			SensorCall();flags |= TIFFPRINT_STRIPS;
			SensorCall();break;
		case 'w':
			SensorCall();showwords = 1;
			SensorCall();break;
		case 'z':
			SensorCall();chopstrips = 1;
			SensorCall();break;
		case '?':
			SensorCall();usage();
			/*NOTREACHED*/
		;/*12*/}}
	SensorCall();if (optind >= argc)
		{/*17*/SensorCall();usage();/*18*/}

	SensorCall();old_error_handler = TIFFSetErrorHandler(PrivateErrorHandler);

	multiplefiles = (argc - optind > 1);
	SensorCall();for (; optind < argc; optind++) {
		SensorCall();if (multiplefiles)
			{/*19*/SensorCall();printf("%s:\n", argv[optind]);/*20*/}
		SensorCall();tif = TIFFOpen(argv[optind], chopstrips ? "rC" : "rc");
		SensorCall();if (tif != NULL) {
			SensorCall();if (dirnum != -1) {
				SensorCall();if (TIFFSetDirectory(tif, (tdir_t) dirnum))
					{/*21*/SensorCall();tiffinfo(tif, order, flags, 1);/*22*/}
			} else {/*23*/SensorCall();if (diroff != 0) {
				SensorCall();if (TIFFSetSubDirectory(tif, diroff))
					{/*25*/SensorCall();tiffinfo(tif, order, flags, 1);/*26*/}
			} else {
				SensorCall();do {
					SensorCall();toff_t offset;

					tiffinfo(tif, order, flags, 1);
					SensorCall();if (TIFFGetField(tif, TIFFTAG_EXIFIFD,
							 &offset)) {
						SensorCall();if (TIFFReadEXIFDirectory(tif, offset)) {
							SensorCall();tiffinfo(tif, order, flags, 0);
						}
					}
				} while (TIFFReadDirectory(tif));
			;/*24*/}}
			SensorCall();TIFFClose(tif);
		}
	}
	{int  ReplaceReturn = (status); SensorCall(); return ReplaceReturn;}
}

char* stuff[] = {
"usage: tiffinfo [options] input...",
"where options are:",
" -D		read data",
" -i		ignore read errors",
" -c		display data for grey/color response curve or colormap",
" -d		display raw/decoded image data",
" -f lsb2msb	force lsb-to-msb FillOrder for input",
" -f msb2lsb	force msb-to-lsb FillOrder for input",
" -j		show JPEG tables",
" -o offset	set initial directory offset",
" -r		read/display raw image data instead of decoded data",
" -s		display strip offsets and byte counts",
" -w		display raw data in words rather than bytes",
" -z		enable strip chopping",
" -#		set initial directory (first directory is # 0)",
NULL
};

static void
usage(void)
{
	SensorCall();char buf[BUFSIZ];
	int i;

	setbuf(stderr, buf);
        fprintf(stderr, "%s\n\n", TIFFGetVersion());
	SensorCall();for (i = 0; stuff[i] != NULL; i++)
		{/*1*/SensorCall();fprintf(stderr, "%s\n", stuff[i]);/*2*/}
	SensorCall();exit(-1);
SensorCall();}

static void
ShowStrip(tstrip_t strip, unsigned char* pp, uint32 nrow, tsize_t scanline)
{
	SensorCall();register tsize_t cc;

	printf("Strip %lu:\n", (unsigned long) strip);
	SensorCall();while (nrow-- > 0) {
		SensorCall();for (cc = 0; cc < scanline; cc++) {
			SensorCall();printf(" %02x", *pp++);
			SensorCall();if (((cc+1) % 24) == 0)
				{/*27*/SensorCall();putchar('\n');/*28*/}
		}
		SensorCall();putchar('\n');
	}
SensorCall();}

void
TIFFReadContigStripData(TIFF* tif)
{
	SensorCall();unsigned char *buf;
	tsize_t scanline = TIFFScanlineSize(tif);

	buf = (unsigned char *)_TIFFmalloc(TIFFStripSize(tif));
	SensorCall();if (buf) {
		SensorCall();uint32 row, h=0;
		uint32 rowsperstrip = (uint32)-1;

		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &h);
		TIFFGetField(tif, TIFFTAG_ROWSPERSTRIP, &rowsperstrip);
		SensorCall();for (row = 0; row < h; row += rowsperstrip) {
			SensorCall();uint32 nrow = (row+rowsperstrip > h ?
			    h-row : rowsperstrip);
			tstrip_t strip = TIFFComputeStrip(tif, row, 0);
			SensorCall();if (TIFFReadEncodedStrip(tif, strip, buf, nrow*scanline) < 0) {
				SensorCall();if (stoponerr)
					{/*29*/SensorCall();break;/*30*/}
			} else {/*31*/SensorCall();if (showdata)
				{/*33*/SensorCall();ShowStrip(strip, buf, nrow, scanline);/*34*/}/*32*/}
		}
		SensorCall();_TIFFfree(buf);
	}
SensorCall();}

void
TIFFReadSeparateStripData(TIFF* tif)
{
	SensorCall();unsigned char *buf;
	tsize_t scanline = TIFFScanlineSize(tif);

	buf = (unsigned char *)_TIFFmalloc(TIFFStripSize(tif));
	SensorCall();if (buf) {
		SensorCall();uint32 row, h;
		uint32 rowsperstrip = (uint32)-1;
		tsample_t s, samplesperpixel;

		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &h);
		TIFFGetField(tif, TIFFTAG_ROWSPERSTRIP, &rowsperstrip);
		TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
		SensorCall();for (row = 0; row < h; row += rowsperstrip) {
			SensorCall();for (s = 0; s < samplesperpixel; s++) {
				SensorCall();uint32 nrow = (row+rowsperstrip > h ?
				    h-row : rowsperstrip);
				tstrip_t strip = TIFFComputeStrip(tif, row, s);
				SensorCall();if (TIFFReadEncodedStrip(tif, strip, buf, nrow*scanline) < 0) {
					SensorCall();if (stoponerr)
						{/*35*/SensorCall();break;/*36*/}
				} else {/*37*/SensorCall();if (showdata)
					{/*39*/SensorCall();ShowStrip(strip, buf, nrow, scanline);/*40*/}/*38*/}
			}
		}
		SensorCall();_TIFFfree(buf);
	}
SensorCall();}

static void
ShowTile(uint32 row, uint32 col, tsample_t sample,
    unsigned char* pp, uint32 nrow, tsize_t rowsize)
{
	SensorCall();uint32 cc;

	printf("Tile (%lu,%lu", (unsigned long) row, (unsigned long) col);
	SensorCall();if (sample != (tsample_t) -1)
		{/*41*/SensorCall();printf(",%u", sample);/*42*/}
	SensorCall();printf("):\n");
	SensorCall();while (nrow-- > 0) {
	  SensorCall();for (cc = 0; cc < (uint32) rowsize; cc++) {
			SensorCall();printf(" %02x", *pp++);
			SensorCall();if (((cc+1) % 24) == 0)
				{/*43*/SensorCall();putchar('\n');/*44*/}
		}
		SensorCall();putchar('\n');
	}
SensorCall();}

void
TIFFReadContigTileData(TIFF* tif)
{
	SensorCall();unsigned char *buf;
	tsize_t rowsize = TIFFTileRowSize(tif);

	buf = (unsigned char *)_TIFFmalloc(TIFFTileSize(tif));
	SensorCall();if (buf) {
		SensorCall();uint32 tw, th, w, h;
		uint32 row, col;

		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &w);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &h);
		TIFFGetField(tif, TIFFTAG_TILEWIDTH, &tw);
		TIFFGetField(tif, TIFFTAG_TILELENGTH, &th);
		SensorCall();for (row = 0; row < h; row += th) {
			SensorCall();for (col = 0; col < w; col += tw) {
				SensorCall();if (TIFFReadTile(tif, buf, col, row, 0, 0) < 0) {
					SensorCall();if (stoponerr)
						{/*45*/SensorCall();break;/*46*/}
				} else {/*47*/SensorCall();if (showdata)
					{/*49*/SensorCall();ShowTile(row, col, (tsample_t) -1, buf, th, rowsize);/*50*/}/*48*/}
			}
		}
		SensorCall();_TIFFfree(buf);
	}
SensorCall();}

void
TIFFReadSeparateTileData(TIFF* tif)
{
	SensorCall();unsigned char *buf;
	tsize_t rowsize = TIFFTileRowSize(tif);

	buf = (unsigned char *)_TIFFmalloc(TIFFTileSize(tif));
	SensorCall();if (buf) {
		SensorCall();uint32 tw, th, w, h;
		uint32 row, col;
		tsample_t s, samplesperpixel;

		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &w);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &h);
		TIFFGetField(tif, TIFFTAG_TILEWIDTH, &tw);
		TIFFGetField(tif, TIFFTAG_TILELENGTH, &th);
		TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
		SensorCall();for (row = 0; row < h; row += th) {
			SensorCall();for (col = 0; col < w; col += tw) {
				SensorCall();for (s = 0; s < samplesperpixel; s++) {
					SensorCall();if (TIFFReadTile(tif, buf, col, row, 0, s) < 0) {
						SensorCall();if (stoponerr)
							{/*51*/SensorCall();break;/*52*/}
					} else {/*53*/SensorCall();if (showdata)
						{/*55*/SensorCall();ShowTile(row, col, s, buf, th, rowsize);/*56*/}/*54*/}
				}
			}
		}
		SensorCall();_TIFFfree(buf);
	}
SensorCall();}

void
TIFFReadData(TIFF* tif)
{
	SensorCall();uint16 config = PLANARCONFIG_CONTIG;

	TIFFGetField(tif, TIFFTAG_PLANARCONFIG, &config);
	SensorCall();if (TIFFIsTiled(tif)) {
		SensorCall();if (config == PLANARCONFIG_CONTIG)
			{/*57*/SensorCall();TIFFReadContigTileData(tif);/*58*/}
		else
			{/*59*/SensorCall();TIFFReadSeparateTileData(tif);/*60*/}
	} else {
		SensorCall();if (config == PLANARCONFIG_CONTIG)
			{/*61*/SensorCall();TIFFReadContigStripData(tif);/*62*/}
		else
			{/*63*/SensorCall();TIFFReadSeparateStripData(tif);/*64*/}
	}
SensorCall();}

static void
ShowRawBytes(unsigned char* pp, uint32 n)
{
	SensorCall();uint32 i;

	SensorCall();for (i = 0; i < n; i++) {
		SensorCall();printf(" %02x", *pp++);
		SensorCall();if (((i+1) % 24) == 0)
			{/*65*/SensorCall();printf("\n ");/*66*/}
	}
	SensorCall();putchar('\n');
SensorCall();}

static void
ShowRawWords(uint16* pp, uint32 n)
{
	SensorCall();uint32 i;

	SensorCall();for (i = 0; i < n; i++) {
		SensorCall();printf(" %04x", *pp++);
		SensorCall();if (((i+1) % 15) == 0)
			{/*67*/SensorCall();printf("\n ");/*68*/}
	}
	SensorCall();putchar('\n');
SensorCall();}

void
TIFFReadRawData(TIFF* tif, int bitrev)
{
	SensorCall();tstrip_t nstrips = TIFFNumberOfStrips(tif);
	const char* what = TIFFIsTiled(tif) ? "Tile" : "Strip";
	uint64* stripbc;

	TIFFGetField(tif, TIFFTAG_STRIPBYTECOUNTS, &stripbc);
	SensorCall();if (nstrips > 0) {
		SensorCall();uint32 bufsize = (uint32) stripbc[0];
		tdata_t buf = _TIFFmalloc(bufsize);
		tstrip_t s;

		SensorCall();for (s = 0; s < nstrips; s++) {
			SensorCall();if (stripbc[s] > bufsize) {
				SensorCall();buf = _TIFFrealloc(buf, (tmsize_t)stripbc[s]);
				bufsize = (uint32) stripbc[s];
			}
			SensorCall();if (buf == NULL) {
				SensorCall();fprintf(stderr,
				   "Cannot allocate buffer to read strip %lu\n",
				    (unsigned long) s);
				SensorCall();break;
			}
			SensorCall();if (TIFFReadRawStrip(tif, s, buf, (tmsize_t) stripbc[s]) < 0) {
				SensorCall();fprintf(stderr, "Error reading strip %lu\n",
				    (unsigned long) s);
				SensorCall();if (stoponerr)
					{/*69*/SensorCall();break;/*70*/}
			} else {/*71*/SensorCall();if (showdata) {
				SensorCall();if (bitrev) {
					SensorCall();TIFFReverseBits(buf, (tmsize_t)stripbc[s]);
					printf("%s %lu: (bit reversed)\n ",
					    what, (unsigned long) s);
				} else
					{/*73*/SensorCall();printf("%s %lu:\n ", what,
					    (unsigned long) s);/*74*/}
				SensorCall();if (showwords)
					{/*75*/SensorCall();ShowRawWords((uint16*) buf, (uint32) stripbc[s]>>1);/*76*/}
				else
					{/*77*/SensorCall();ShowRawBytes((unsigned char*) buf, (uint32) stripbc[s]);/*78*/}
			;/*72*/}}
		}
		SensorCall();if (buf != NULL)
			{/*79*/SensorCall();_TIFFfree(buf);/*80*/}
	}
SensorCall();}

static void
tiffinfo(TIFF* tif, uint16 order, long flags, int is_image)
{
	SensorCall();TIFFPrintDirectory(tif, stdout, flags);
	SensorCall();if (!readdata || !is_image)
		{/*3*/SensorCall();return;/*4*/}
	SensorCall();if (rawdata) {
		SensorCall();if (order) {
			SensorCall();uint16 o;
			TIFFGetFieldDefaulted(tif,
			    TIFFTAG_FILLORDER, &o);
			TIFFReadRawData(tif, o != order);
		} else
			{/*5*/SensorCall();TIFFReadRawData(tif, 0);/*6*/}
	} else {
		SensorCall();if (order)
			{/*7*/SensorCall();TIFFSetField(tif, TIFFTAG_FILLORDER, order);/*8*/}
		SensorCall();TIFFReadData(tif);
	}
SensorCall();}

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
