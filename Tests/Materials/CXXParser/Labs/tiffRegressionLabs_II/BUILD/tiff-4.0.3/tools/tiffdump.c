/* $Id: tiffdump.c,v 1.26 2012-06-15 21:51:54 fwarmerdam Exp $ */

/*
 * Copyright (c) 1988-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

#include "tif_config.h"
#include "/var/tmp/sensor.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifdef HAVE_FCNTL_H
# include <fcntl.h>
#endif

#ifdef HAVE_SYS_TYPES_H
# include <sys/types.h>
#endif

#ifdef HAVE_IO_H
# include <io.h>
#endif

#ifdef NEED_LIBPORT
# include "libport.h"
#endif

#ifndef HAVE_GETOPT
extern int getopt(int, char**, char*);
#endif

#include "tiffio.h"

#ifndef O_BINARY
# define O_BINARY	0
#endif

static union
{
	TIFFHeaderClassic classic;
	TIFFHeaderBig big;
	TIFFHeaderCommon common;
} hdr;
char* appname;
char* curfile;
int swabflag;
int bigendian;
int bigtiff;
uint32 maxitems = 24;   /* maximum indirect data items to print */

const char* bytefmt = "%s%#02x";	/* BYTE */
const char* sbytefmt = "%s%d";		/* SBYTE */
const char* shortfmt = "%s%u";		/* SHORT */
const char* sshortfmt = "%s%d";		/* SSHORT */
const char* longfmt = "%s%lu";		/* LONG */
const char* slongfmt = "%s%ld";		/* SLONG */
const char* ifdfmt = "%s%#04lx";	/* IFD offset */
#if defined(__WIN32__) && (defined(_MSC_VER) || defined(__MINGW32__))
const char* long8fmt = "%s%I64u";	/* LONG8 */
const char* slong8fmt = "%s%I64d";	/* SLONG8 */
const char* ifd8fmt = "%s%#08I64x";	/* IFD offset8*/
#else
const char* long8fmt = "%s%llu";	/* LONG8 */
const char* slong8fmt = "%s%lld";	/* SLONG8 */
const char* ifd8fmt = "%s%#08llx";	/* IFD offset8*/
#endif
const char* rationalfmt = "%s%g";	/* RATIONAL */
const char* srationalfmt = "%s%g";	/* SRATIONAL */
const char* floatfmt = "%s%g";		/* FLOAT */
const char* doublefmt = "%s%g";		/* DOUBLE */

static void dump(int, uint64);
extern int optind;
extern char* optarg;

void
usage()
{
	SensorCall();fprintf(stderr, "usage: %s [-h] [-o offset] [-m maxitems] file.tif ...\n", appname);
	exit(-1);
SensorCall();}

int
main(int argc, char* argv[])
{
	SensorCall();int one = 1, fd;
	int multiplefiles = (argc > 1);
	int c;
	uint64 diroff = 0;
	bigendian = (*(char *)&one == 0);

	appname = argv[0];
	SensorCall();while ((c = getopt(argc, argv, "m:o:h")) != -1) {
		SensorCall();switch (c) {
		case 'h':			/* print values in hex */
			SensorCall();shortfmt = "%s%#x";
			sshortfmt = "%s%#x";
			longfmt = "%s%#lx";
			slongfmt = "%s%#lx";
			SensorCall();break;
		case 'o':
			SensorCall();diroff = (uint64) strtoul(optarg, NULL, 0);
			SensorCall();break;
		case 'm':
			SensorCall();maxitems = strtoul(optarg, NULL, 0);
			SensorCall();break;
		default:
			SensorCall();usage();
		}
	}
	SensorCall();if (optind >= argc)
		{/*25*/SensorCall();usage();/*26*/}
	SensorCall();for (; optind < argc; optind++) {
		SensorCall();fd = open(argv[optind], O_RDONLY|O_BINARY, 0);
		SensorCall();if (fd < 0) {
			SensorCall();perror(argv[0]);
			{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
		}
		SensorCall();if (multiplefiles)
			{/*27*/SensorCall();printf("%s:\n", argv[optind]);/*28*/}
		SensorCall();curfile = argv[optind];
		swabflag = 0;
		bigtiff = 0;
		dump(fd, diroff);
		close(fd);
	}
	{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
}

#define ord(e) ((int)e)

static uint64 ReadDirectory(int, unsigned, uint64);
static void ReadError(char*);
static void Error(const char*, ...);
static void Fatal(const char*, ...);

static void
dump(int fd, uint64 diroff)
{
	SensorCall();unsigned i;

	lseek(fd, (off_t) 0, 0);
	SensorCall();if (read(fd, (char*) &hdr, sizeof (TIFFHeaderCommon)) != sizeof (TIFFHeaderCommon))
		{/*1*/SensorCall();ReadError("TIFF header");/*2*/}
	SensorCall();if (hdr.common.tiff_magic != TIFF_BIGENDIAN
	    && hdr.common.tiff_magic != TIFF_LITTLEENDIAN &&
#if HOST_BIGENDIAN
	    /* MDI is sensitive to the host byte order, unlike TIFF */
	    MDI_BIGENDIAN != hdr.common.tiff_magic
#else
	    MDI_LITTLEENDIAN != hdr.common.tiff_magic
#endif
	   ) {
		SensorCall();Fatal("Not a TIFF or MDI file, bad magic number %u (%#x)",
		    hdr.common.tiff_magic, hdr.common.tiff_magic);
	}
	SensorCall();if (hdr.common.tiff_magic == TIFF_BIGENDIAN
	    || hdr.common.tiff_magic == MDI_BIGENDIAN)
		{/*3*/SensorCall();swabflag = !bigendian;/*4*/}
	else
		{/*5*/SensorCall();swabflag = bigendian;/*6*/}
	SensorCall();if (swabflag)
		{/*7*/SensorCall();TIFFSwabShort(&hdr.common.tiff_version);/*8*/}
	SensorCall();if (hdr.common.tiff_version==42)
	{
		SensorCall();if (read(fd, (char*) &hdr.classic.tiff_diroff, 4) != 4)
			{/*9*/SensorCall();ReadError("TIFF header");/*10*/}
		SensorCall();if (swabflag)
			{/*11*/SensorCall();TIFFSwabLong(&hdr.classic.tiff_diroff);/*12*/}
		SensorCall();printf("Magic: %#x <%s-endian> Version: %#x <%s>\n",
		    hdr.classic.tiff_magic,
		    hdr.classic.tiff_magic == TIFF_BIGENDIAN ? "big" : "little",
		    42,"ClassicTIFF");
		SensorCall();if (diroff == 0)
			{/*13*/SensorCall();diroff = hdr.classic.tiff_diroff;/*14*/}
	}
	else {/*15*/SensorCall();if (hdr.common.tiff_version==43)
	{
		SensorCall();if (read(fd, (char*) &hdr.big.tiff_offsetsize, 12) != 12)
			{/*17*/SensorCall();ReadError("TIFF header");/*18*/}
		SensorCall();if (swabflag)
		{
			SensorCall();TIFFSwabShort(&hdr.big.tiff_offsetsize);
			TIFFSwabShort(&hdr.big.tiff_unused);
			TIFFSwabLong8(&hdr.big.tiff_diroff);
		}
		SensorCall();printf("Magic: %#x <%s-endian> Version: %#x <%s>\n",
		    hdr.big.tiff_magic,
		    hdr.big.tiff_magic == TIFF_BIGENDIAN ? "big" : "little",
		    43,"BigTIFF");
		printf("OffsetSize: %#x Unused: %#x\n",
		    hdr.big.tiff_offsetsize,hdr.big.tiff_unused);
		SensorCall();if (diroff == 0)
			{/*19*/SensorCall();diroff = hdr.big.tiff_diroff;/*20*/}
		SensorCall();bigtiff = 1;
	}
	else
		{/*21*/SensorCall();Fatal("Not a TIFF file, bad version number %u (%#x)",
		    hdr.common.tiff_version, hdr.common.tiff_version);/*22*/}/*16*/}
	SensorCall();for (i = 0; diroff != 0; i++) {
		SensorCall();if (i > 0)
			{/*23*/SensorCall();putchar('\n');/*24*/}
		SensorCall();diroff = ReadDirectory(fd, i, diroff);
	}
SensorCall();}

static const int datawidth[] = {
	0, /* 00 = undefined */
	1, /* 01 = TIFF_BYTE */
	1, /* 02 = TIFF_ASCII */
	2, /* 03 = TIFF_SHORT */
	4, /* 04 = TIFF_LONG */
	8, /* 05 = TIFF_RATIONAL */
	1, /* 06 = TIFF_SBYTE */
	1, /* 07 = TIFF_UNDEFINED */
	2, /* 08 = TIFF_SSHORT */
	4, /* 09 = TIFF_SLONG */
	8, /* 10 = TIFF_SRATIONAL */
	4, /* 11 = TIFF_FLOAT */
	8, /* 12 = TIFF_DOUBLE */
	4, /* 13 = TIFF_IFD */
	0, /* 14 = undefined */
	0, /* 15 = undefined */
	8, /* 16 = TIFF_LONG8 */
	8, /* 17 = TIFF_SLONG8 */
	8, /* 18 = TIFF_IFD8 */
};
#define NWIDTHS (sizeof (datawidth) / sizeof (datawidth[0]))
static void PrintTag(FILE*, uint16);
static void PrintType(FILE*, uint16);
static void PrintData(FILE*, uint16, uint32, unsigned char*);

/*
 * Read the next TIFF directory from a file
 * and convert it to the internal format.
 * We read directories sequentially.
 */
static uint64
ReadDirectory(int fd, unsigned int ix, uint64 off)
{
	SensorCall();uint16 dircount;
	uint32 direntrysize;
	void* dirmem = NULL;
	uint64 nextdiroff = 0;
	uint32 n;
	uint8* dp;

	SensorCall();if (off == 0)			/* no more directories */
		{/*29*/SensorCall();goto done;/*30*/}
#if defined(__WIN32__) && defined(_MSC_VER)
	if (_lseeki64(fd, (__int64)off, SEEK_SET) != (__int64)off) {
#else
	SensorCall();if (lseek(fd, (off_t)off, SEEK_SET) != (off_t)off) {
#endif
		SensorCall();Fatal("Seek error accessing TIFF directory");
		SensorCall();goto done;
	}
	SensorCall();if (!bigtiff) {
		SensorCall();if (read(fd, (char*) &dircount, sizeof (uint16)) != sizeof (uint16)) {
			SensorCall();ReadError("directory count");
			SensorCall();goto done;
		}
		SensorCall();if (swabflag)
			{/*31*/SensorCall();TIFFSwabShort(&dircount);/*32*/}
		SensorCall();direntrysize = 12;
	} else {
		SensorCall();uint64 dircount64 = 0;
		SensorCall();if (read(fd, (char*) &dircount64, sizeof (uint64)) != sizeof (uint64)) {
			SensorCall();ReadError("directory count");
			SensorCall();goto done;
		}
		SensorCall();if (swabflag)
			{/*33*/SensorCall();TIFFSwabLong8(&dircount64);/*34*/}
		SensorCall();if (dircount64>0xFFFF) {
			SensorCall();Error("Sanity check on directory count failed");
			SensorCall();goto done;
		}
		SensorCall();dircount = (uint16)dircount64;
		direntrysize = 20;
	}
	SensorCall();dirmem = _TIFFmalloc(dircount * direntrysize);
	SensorCall();if (dirmem == NULL) {
		SensorCall();Fatal("No space for TIFF directory");
		SensorCall();goto done;
	}
	SensorCall();n = read(fd, (char*) dirmem, dircount*direntrysize);
	SensorCall();if (n != dircount*direntrysize) {
		SensorCall();n /= direntrysize;
		Error(
#if defined(__WIN32__) && defined(_MSC_VER)
	    "Could only read %lu of %u entries in directory at offset %#I64x",
		      (unsigned long)n, dircount, (unsigned __int64) off);
#else
	    "Could only read %lu of %u entries in directory at offset %#llx",
		      (unsigned long)n, dircount, (unsigned long long) off);
#endif
		dircount = n;
		nextdiroff = 0;
	} else {
		SensorCall();if (!bigtiff) {
			SensorCall();uint32 nextdiroff32;
			SensorCall();if (read(fd, (char*) &nextdiroff32, sizeof (uint32)) != sizeof (uint32))
				{/*35*/SensorCall();nextdiroff32 = 0;/*36*/}
			SensorCall();if (swabflag)
				{/*37*/SensorCall();TIFFSwabLong(&nextdiroff32);/*38*/}
			SensorCall();nextdiroff = nextdiroff32;
		} else {
			SensorCall();if (read(fd, (char*) &nextdiroff, sizeof (uint64)) != sizeof (uint64))
				{/*39*/SensorCall();nextdiroff = 0;/*40*/}
			SensorCall();if (swabflag)
				{/*41*/SensorCall();TIFFSwabLong8(&nextdiroff);/*42*/}
		}
	}
#if defined(__WIN32__) && (defined(_MSC_VER) || defined(__MINGW32__))
	printf("Directory %u: offset %I64u (%#I64x) next %I64u (%#I64x)\n", ix,
	    (unsigned __int64)off, (unsigned __int64)off,
	    (unsigned __int64)nextdiroff, (unsigned __int64)nextdiroff);
#else
	SensorCall();printf("Directory %u: offset %llu (%#llx) next %llu (%#llx)\n", ix,
	    (unsigned long long)off, (unsigned long long)off,
	    (unsigned long long)nextdiroff, (unsigned long long)nextdiroff);
#endif
	SensorCall();for (dp = (uint8*)dirmem, n = dircount; n > 0; n--) {
		SensorCall();uint16 tag;
		uint16 type;
		uint16 typewidth;
		uint64 count;
		uint64 datasize;
		int datafits;
		void* datamem;
		uint64 dataoffset;
		int datatruncated;
		tag = *(uint16*)dp;
		SensorCall();if (swabflag)
			{/*43*/SensorCall();TIFFSwabShort(&tag);/*44*/}
		SensorCall();dp += sizeof(uint16);
		type = *(uint16*)dp;
		dp += sizeof(uint16);
		SensorCall();if (swabflag)
			{/*45*/SensorCall();TIFFSwabShort(&type);/*46*/}
		SensorCall();PrintTag(stdout, tag);
		putchar(' ');
		PrintType(stdout, type);
		putchar(' ');
		SensorCall();if (!bigtiff)
		{
			SensorCall();uint32 count32;
			count32 = *(uint32*)dp;
			SensorCall();if (swabflag)
				{/*47*/SensorCall();TIFFSwabLong(&count32);/*48*/}
			SensorCall();dp += sizeof(uint32);
			count = count32;
		}
		else
		{
			SensorCall();count = *(uint64*)dp;
			SensorCall();if (swabflag)
				{/*49*/SensorCall();TIFFSwabLong8(&count);/*50*/}
			SensorCall();dp += sizeof(uint64);
		}
#if defined(__WIN32__) && (defined(_MSC_VER) || defined(__MINGW32__))
		printf("%I64u<", (unsigned __int64)count);
#else
		SensorCall();printf("%llu<", (unsigned long long)count);
#endif
		SensorCall();if (type >= NWIDTHS)
			{/*51*/SensorCall();typewidth = 0;/*52*/}
		else
			{/*53*/SensorCall();typewidth = datawidth[type];/*54*/}
		SensorCall();datasize = count*typewidth;
		datafits = 1;
		datamem = dp;
		dataoffset = 0;
		datatruncated = 0;
		SensorCall();if (!bigtiff)
		{
			SensorCall();if (datasize>4)
			{
				SensorCall();uint32 dataoffset32;
				datafits = 0;
				datamem = NULL;
				dataoffset32 = *(uint32*)dp;
				SensorCall();if (swabflag)
					{/*55*/SensorCall();TIFFSwabLong(&dataoffset32);/*56*/}
				SensorCall();dataoffset = dataoffset32;
			}
			SensorCall();dp += sizeof(uint32);
		}
		else
		{
			SensorCall();if (datasize>8)
			{
				SensorCall();datafits = 0;
				datamem = NULL;
				dataoffset = *(uint64*)dp;
				SensorCall();if (swabflag)
					{/*57*/SensorCall();TIFFSwabLong8(&dataoffset);/*58*/}
			}
			SensorCall();dp += sizeof(uint64);
		}
		SensorCall();if (datasize>0x10000)
		{
			SensorCall();datatruncated = 1;
			count = 0x10000/typewidth;
			datasize = count*typewidth;
		}
		SensorCall();if (count>maxitems)
		{
			SensorCall();datatruncated = 1;
			count = maxitems;
			datasize = count*typewidth;
		}
		SensorCall();if (!datafits)
		{
			SensorCall();datamem = _TIFFmalloc((uint32)datasize);
			SensorCall();if (datamem) {
#if defined(__WIN32__) && defined(_MSC_VER)
				if (_lseeki64(fd, (__int64)dataoffset, SEEK_SET)
				    != (__int64)dataoffset)
#else
				SensorCall();if (lseek(fd, (off_t)dataoffset, 0) !=
				    (off_t)dataoffset)
#endif
				{
					SensorCall();Error(
				"Seek error accessing tag %u value", tag);
					_TIFFfree(datamem);
					datamem = NULL;
				}
				SensorCall();if (read(fd, datamem, (size_t)datasize) != (TIFF_SSIZE_T)datasize)
				{
					SensorCall();Error(
				"Read error accessing tag %u value", tag);
					_TIFFfree(datamem);
					datamem = NULL;
				}
			} else
				{/*59*/SensorCall();Error("No space for data for tag %u",tag);/*60*/}
		}
		SensorCall();if (datamem)
		{
			SensorCall();if (swabflag)
			{
				SensorCall();switch (type)
				{
					case TIFF_BYTE:
					case TIFF_ASCII:
					case TIFF_SBYTE:
					case TIFF_UNDEFINED:
						SensorCall();break;
					case TIFF_SHORT:
					case TIFF_SSHORT:
						SensorCall();TIFFSwabArrayOfShort((uint16*)datamem,(tmsize_t)count);
						SensorCall();break;
					case TIFF_LONG:
					case TIFF_SLONG:
					case TIFF_FLOAT:
					case TIFF_IFD:
						SensorCall();TIFFSwabArrayOfLong((uint32*)datamem,(tmsize_t)count);
						SensorCall();break;
					case TIFF_RATIONAL:
					case TIFF_SRATIONAL:
						SensorCall();TIFFSwabArrayOfLong((uint32*)datamem,(tmsize_t)count*2);
						SensorCall();break;
					case TIFF_DOUBLE:
					case TIFF_LONG8:
					case TIFF_SLONG8:
					case TIFF_IFD8:
						SensorCall();TIFFSwabArrayOfLong8((uint64*)datamem,(tmsize_t)count);
						SensorCall();break;
				}
			}
			SensorCall();PrintData(stdout,type,(uint32)count,datamem);
			SensorCall();if (datatruncated)
				{/*61*/SensorCall();printf(" ...");/*62*/}
			SensorCall();if (!datafits)
				{/*63*/SensorCall();_TIFFfree(datamem);/*64*/}
		}
		SensorCall();printf(">\n");
	}
done:
	SensorCall();if (dirmem)
		{/*65*/SensorCall();_TIFFfree((char *)dirmem);/*66*/}
	{uint64  ReplaceReturn = (nextdiroff); SensorCall(); return ReplaceReturn;}
}

static const struct tagname {
	uint16 tag;
	const char* name;
} tagnames[] = {
    { TIFFTAG_SUBFILETYPE,	"SubFileType" },
    { TIFFTAG_OSUBFILETYPE,	"OldSubFileType" },
    { TIFFTAG_IMAGEWIDTH,	"ImageWidth" },
    { TIFFTAG_IMAGELENGTH,	"ImageLength" },
    { TIFFTAG_BITSPERSAMPLE,	"BitsPerSample" },
    { TIFFTAG_COMPRESSION,	"Compression" },
    { TIFFTAG_PHOTOMETRIC,	"Photometric" },
    { TIFFTAG_THRESHHOLDING,	"Threshholding" },
    { TIFFTAG_CELLWIDTH,	"CellWidth" },
    { TIFFTAG_CELLLENGTH,	"CellLength" },
    { TIFFTAG_FILLORDER,	"FillOrder" },
    { TIFFTAG_DOCUMENTNAME,	"DocumentName" },
    { TIFFTAG_IMAGEDESCRIPTION,	"ImageDescription" },
    { TIFFTAG_MAKE,		"Make" },
    { TIFFTAG_MODEL,		"Model" },
    { TIFFTAG_STRIPOFFSETS,	"StripOffsets" },
    { TIFFTAG_ORIENTATION,	"Orientation" },
    { TIFFTAG_SAMPLESPERPIXEL,	"SamplesPerPixel" },
    { TIFFTAG_ROWSPERSTRIP,	"RowsPerStrip" },
    { TIFFTAG_STRIPBYTECOUNTS,	"StripByteCounts" },
    { TIFFTAG_MINSAMPLEVALUE,	"MinSampleValue" },
    { TIFFTAG_MAXSAMPLEVALUE,	"MaxSampleValue" },
    { TIFFTAG_XRESOLUTION,	"XResolution" },
    { TIFFTAG_YRESOLUTION,	"YResolution" },
    { TIFFTAG_PLANARCONFIG,	"PlanarConfig" },
    { TIFFTAG_PAGENAME,		"PageName" },
    { TIFFTAG_XPOSITION,	"XPosition" },
    { TIFFTAG_YPOSITION,	"YPosition" },
    { TIFFTAG_FREEOFFSETS,	"FreeOffsets" },
    { TIFFTAG_FREEBYTECOUNTS,	"FreeByteCounts" },
    { TIFFTAG_GRAYRESPONSEUNIT,	"GrayResponseUnit" },
    { TIFFTAG_GRAYRESPONSECURVE,"GrayResponseCurve" },
    { TIFFTAG_GROUP3OPTIONS,	"Group3Options" },
    { TIFFTAG_GROUP4OPTIONS,	"Group4Options" },
    { TIFFTAG_RESOLUTIONUNIT,	"ResolutionUnit" },
    { TIFFTAG_PAGENUMBER,	"PageNumber" },
    { TIFFTAG_COLORRESPONSEUNIT,"ColorResponseUnit" },
    { TIFFTAG_TRANSFERFUNCTION,	"TransferFunction" },
    { TIFFTAG_SOFTWARE,		"Software" },
    { TIFFTAG_DATETIME,		"DateTime" },
    { TIFFTAG_ARTIST,		"Artist" },
    { TIFFTAG_HOSTCOMPUTER,	"HostComputer" },
    { TIFFTAG_PREDICTOR,	"Predictor" },
    { TIFFTAG_WHITEPOINT,	"Whitepoint" },
    { TIFFTAG_PRIMARYCHROMATICITIES,"PrimaryChromaticities" },
    { TIFFTAG_COLORMAP,		"Colormap" },
    { TIFFTAG_HALFTONEHINTS,	"HalftoneHints" },
    { TIFFTAG_TILEWIDTH,	"TileWidth" },
    { TIFFTAG_TILELENGTH,	"TileLength" },
    { TIFFTAG_TILEOFFSETS,	"TileOffsets" },
    { TIFFTAG_TILEBYTECOUNTS,	"TileByteCounts" },
    { TIFFTAG_BADFAXLINES,	"BadFaxLines" },
    { TIFFTAG_CLEANFAXDATA,	"CleanFaxData" },
    { TIFFTAG_CONSECUTIVEBADFAXLINES, "ConsecutiveBadFaxLines" },
    { TIFFTAG_SUBIFD,		"SubIFD" },
    { TIFFTAG_INKSET,		"InkSet" },
    { TIFFTAG_INKNAMES,		"InkNames" },
    { TIFFTAG_NUMBEROFINKS,	"NumberOfInks" },
    { TIFFTAG_DOTRANGE,		"DotRange" },
    { TIFFTAG_TARGETPRINTER,	"TargetPrinter" },
    { TIFFTAG_EXTRASAMPLES,	"ExtraSamples" },
    { TIFFTAG_SAMPLEFORMAT,	"SampleFormat" },
    { TIFFTAG_SMINSAMPLEVALUE,	"SMinSampleValue" },
    { TIFFTAG_SMAXSAMPLEVALUE,	"SMaxSampleValue" },
    { TIFFTAG_JPEGPROC,		"JPEGProcessingMode" },
    { TIFFTAG_JPEGIFOFFSET,	"JPEGInterchangeFormat" },
    { TIFFTAG_JPEGIFBYTECOUNT,	"JPEGInterchangeFormatLength" },
    { TIFFTAG_JPEGRESTARTINTERVAL,"JPEGRestartInterval" },
    { TIFFTAG_JPEGLOSSLESSPREDICTORS,"JPEGLosslessPredictors" },
    { TIFFTAG_JPEGPOINTTRANSFORM,"JPEGPointTransform" },
    { TIFFTAG_JPEGTABLES,       "JPEGTables" },
    { TIFFTAG_JPEGQTABLES,	"JPEGQTables" },
    { TIFFTAG_JPEGDCTABLES,	"JPEGDCTables" },
    { TIFFTAG_JPEGACTABLES,	"JPEGACTables" },
    { TIFFTAG_YCBCRCOEFFICIENTS,"YCbCrCoefficients" },
    { TIFFTAG_YCBCRSUBSAMPLING,	"YCbCrSubsampling" },
    { TIFFTAG_YCBCRPOSITIONING,	"YCbCrPositioning" },
    { TIFFTAG_REFERENCEBLACKWHITE, "ReferenceBlackWhite" },
    { TIFFTAG_REFPTS,		"IgReferencePoints (Island Graphics)" },
    { TIFFTAG_REGIONTACKPOINT,	"IgRegionTackPoint (Island Graphics)" },
    { TIFFTAG_REGIONWARPCORNERS,"IgRegionWarpCorners (Island Graphics)" },
    { TIFFTAG_REGIONAFFINE,	"IgRegionAffine (Island Graphics)" },
    { TIFFTAG_MATTEING,		"OBSOLETE Matteing (Silicon Graphics)" },
    { TIFFTAG_DATATYPE,		"OBSOLETE DataType (Silicon Graphics)" },
    { TIFFTAG_IMAGEDEPTH,	"ImageDepth (Silicon Graphics)" },
    { TIFFTAG_TILEDEPTH,	"TileDepth (Silicon Graphics)" },
    { 32768,			"OLD BOGUS Matteing tag" },
    { TIFFTAG_COPYRIGHT,	"Copyright" },
    { TIFFTAG_ICCPROFILE,	"ICC Profile" },
    { TIFFTAG_JBIGOPTIONS,	"JBIG Options" },
    { TIFFTAG_STONITS,		"StoNits" },
};
#define	NTAGS	(sizeof (tagnames) / sizeof (tagnames[0]))

static void
PrintTag(FILE* fd, uint16 tag)
{
	SensorCall();const struct tagname *tp;

	SensorCall();for (tp = tagnames; tp < &tagnames[NTAGS]; tp++)
		{/*67*/SensorCall();if (tp->tag == tag) {
			SensorCall();fprintf(fd, "%s (%u)", tp->name, tag);
			SensorCall();return;
		;/*68*/}}
	SensorCall();fprintf(fd, "%u (%#x)", tag, tag);
SensorCall();}

static void
PrintType(FILE* fd, uint16 type)
{
	SensorCall();static const char *typenames[] = {
	    "0",
	    "BYTE",
	    "ASCII",
	    "SHORT",
	    "LONG",
	    "RATIONAL",
	    "SBYTE",
	    "UNDEFINED",
	    "SSHORT",
	    "SLONG",
	    "SRATIONAL",
	    "FLOAT",
	    "DOUBLE",
	    "IFD",
	    "14",
	    "15",
	    "LONG8",
	    "SLONG8",
	    "IFD8"
	};
#define	NTYPES	(sizeof (typenames) / sizeof (typenames[0]))

	SensorCall();if (type < NTYPES)
		{/*69*/SensorCall();fprintf(fd, "%s (%u)", typenames[type], type);/*70*/}
	else
		{/*71*/SensorCall();fprintf(fd, "%u (%#x)", type, type);/*72*/}
SensorCall();}
#undef	NTYPES

#include <ctype.h>

static void
PrintASCII(FILE* fd, uint32 cc, const unsigned char* cp)
{
	SensorCall();for (; cc > 0; cc--, cp++) {
		SensorCall();const char* tp;

		SensorCall();if (isprint(*cp)) {
			SensorCall();fputc(*cp, fd);
			SensorCall();continue;
		}
		SensorCall();for (tp = "\tt\bb\rr\nn\vv"; *tp; tp++)
			{/*99*/SensorCall();if (*tp++ == *cp)
				{/*101*/SensorCall();break;/*102*/}/*100*/}
		SensorCall();if (*tp)
			{/*103*/SensorCall();fprintf(fd, "\\%c", *tp);/*104*/}
		else {/*105*/SensorCall();if (*cp)
			{/*107*/SensorCall();fprintf(fd, "\\%03o", *cp);/*108*/}
		else
			{/*109*/SensorCall();fprintf(fd, "\\0");/*110*/}/*106*/}
	}
SensorCall();}

static void
PrintData(FILE* fd, uint16 type, uint32 count, unsigned char* data)
{
	SensorCall();char* sep = "";

	SensorCall();switch (type) {
	case TIFF_BYTE:
		SensorCall();while (count-- > 0)
			{/*73*/SensorCall();fprintf(fd, bytefmt, sep, *data++), sep = " ";/*74*/}
		SensorCall();break;
	case TIFF_SBYTE:
		SensorCall();while (count-- > 0)
			{/*75*/SensorCall();fprintf(fd, sbytefmt, sep, *(char *)data++), sep = " ";/*76*/}
		SensorCall();break;
	case TIFF_UNDEFINED:
		SensorCall();while (count-- > 0)
			{/*77*/SensorCall();fprintf(fd, bytefmt, sep, *data++), sep = " ";/*78*/}
		SensorCall();break;
	case TIFF_ASCII:
		SensorCall();PrintASCII(fd, count, data);
		SensorCall();break;
	case TIFF_SHORT: {
		SensorCall();uint16 *wp = (uint16*)data;
		SensorCall();while (count-- > 0)
			{/*79*/SensorCall();fprintf(fd, shortfmt, sep, *wp++), sep = " ";/*80*/}
		SensorCall();break;
	}
	case TIFF_SSHORT: {
		SensorCall();int16 *wp = (int16*)data;
		SensorCall();while (count-- > 0)
			{/*81*/SensorCall();fprintf(fd, sshortfmt, sep, *wp++), sep = " ";/*82*/}
		SensorCall();break;
	}
	case TIFF_LONG: {
		SensorCall();uint32 *lp = (uint32*)data;
		SensorCall();while (count-- > 0) {
			SensorCall();fprintf(fd, longfmt, sep, (unsigned long) *lp++);
			sep = " ";
		}
		SensorCall();break;
	}
	case TIFF_SLONG: {
		SensorCall();int32 *lp = (int32*)data;
		SensorCall();while (count-- > 0)
			{/*83*/SensorCall();fprintf(fd, slongfmt, sep, (long) *lp++), sep = " ";/*84*/}
		SensorCall();break;
	}
	case TIFF_LONG8: {
		SensorCall();uint64 *llp = (uint64*)data;
		SensorCall();while (count-- > 0) {
#if defined(__WIN32__) && defined(_MSC_VER)
			fprintf(fd, long8fmt, sep, (unsigned __int64) *llp++);
#else
			SensorCall();fprintf(fd, long8fmt, sep, (unsigned long long) *llp++);
#endif
			sep = " ";
		}
		SensorCall();break;
	}
	case TIFF_SLONG8: {
		SensorCall();int64 *llp = (int64*)data;
		SensorCall();while (count-- > 0)
#if defined(__WIN32__) && defined(_MSC_VER)
			fprintf(fd, slong8fmt, sep, (__int64) *llp++), sep = " ";
#else
			{/*85*/SensorCall();fprintf(fd, slong8fmt, sep, (long long) *llp++), sep = " ";/*86*/}
#endif
		SensorCall();break;
	}
	case TIFF_RATIONAL: {
		SensorCall();uint32 *lp = (uint32*)data;
		SensorCall();while (count-- > 0) {
			SensorCall();if (lp[1] == 0)
				{/*87*/SensorCall();fprintf(fd, "%sNan (%lu/%lu)", sep,
				    (unsigned long) lp[0],
				    (unsigned long) lp[1]);/*88*/}
			else
				{/*89*/SensorCall();fprintf(fd, rationalfmt, sep,
				    (double)lp[0] / (double)lp[1]);/*90*/}
			SensorCall();sep = " ";
			lp += 2;
		}
		SensorCall();break;
	}
	case TIFF_SRATIONAL: {
		SensorCall();int32 *lp = (int32*)data;
		SensorCall();while (count-- > 0) {
			SensorCall();if (lp[1] == 0)
				{/*91*/SensorCall();fprintf(fd, "%sNan (%ld/%ld)", sep,
				    (long) lp[0], (long) lp[1]);/*92*/}
			else
				{/*93*/SensorCall();fprintf(fd, srationalfmt, sep,
				    (double)lp[0] / (double)lp[1]);/*94*/}
			SensorCall();sep = " ";
			lp += 2;
		}
		SensorCall();break;
	}
	case TIFF_FLOAT: {
		SensorCall();float *fp = (float *)data;
		SensorCall();while (count-- > 0)
			{/*95*/SensorCall();fprintf(fd, floatfmt, sep, *fp++), sep = " ";/*96*/}
		SensorCall();break;
	}
	case TIFF_DOUBLE: {
		SensorCall();double *dp = (double *)data;
		SensorCall();while (count-- > 0)
			{/*97*/SensorCall();fprintf(fd, doublefmt, sep, *dp++), sep = " ";/*98*/}
		SensorCall();break;
	}
	case TIFF_IFD: {
		SensorCall();uint32 *lp = (uint32*)data;
		SensorCall();while (count-- > 0) {
			SensorCall();fprintf(fd, ifdfmt, sep, (unsigned long) *lp++);
			sep = " ";
		}
		SensorCall();break;
	}
	case TIFF_IFD8: {
		SensorCall();uint64 *llp = (uint64*)data;
		SensorCall();while (count-- > 0) {
#if defined(__WIN32__) && defined(_MSC_VER)
			fprintf(fd, ifd8fmt, sep, (unsigned __int64) *llp++);
#else
			SensorCall();fprintf(fd, ifd8fmt, sep, (unsigned long long) *llp++);
#endif
			sep = " ";
		}
		SensorCall();break;
	}
	}
SensorCall();}

static void
ReadError(char* what)
{
	SensorCall();Fatal("Error while reading %s", what);
SensorCall();}

#include <stdarg.h>

static void
vError(FILE* fd, const char* fmt, va_list ap)
{
	SensorCall();fprintf(fd, "%s: ", curfile);
	vfprintf(fd, fmt, ap);
	fprintf(fd, ".\n");
SensorCall();}

static void
Error(const char* fmt, ...)
{
	SensorCall();va_list ap;
	va_start(ap, fmt);
	vError(stderr, fmt, ap);
	va_end(ap);
}

static void
Fatal(const char* fmt, ...)
{
	SensorCall();va_list ap;
	va_start(ap, fmt);
	vError(stderr, fmt, ap);
	va_end(ap);
	exit(-1);
SensorCall();}

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
