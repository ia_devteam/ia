/* $Id: bmp2tiff.c,v 1.23 2010-03-10 18:56:49 bfriesen Exp $
 *
 * Project:  libtiff tools
 * Purpose:  Convert Windows BMP files in TIFF.
 * Author:   Andrey Kiselev, dron@ak4719.spb.edu
 *
 ******************************************************************************
 * Copyright (c) 2004, Andrey Kiselev <dron@ak4719.spb.edu>
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

#include "tif_config.h"
#include "/var/tmp/sensor.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#if HAVE_FCNTL_H
# include <fcntl.h>
#endif

#if HAVE_SYS_TYPES_H
# include <sys/types.h>
#endif

#if HAVE_IO_H
# include <io.h>
#endif

#ifdef NEED_LIBPORT
# include "libport.h"
#endif

#include "tiffio.h"

#ifndef O_BINARY
# define O_BINARY 0
#endif

enum BMPType
{
    BMPT_WIN4,      /* BMP used in Windows 3.0/NT 3.51/95 */
    BMPT_WIN5,      /* BMP used in Windows NT 4.0/98/Me/2000/XP */
    BMPT_OS21,      /* BMP used in OS/2 PM 1.x */
    BMPT_OS22       /* BMP used in OS/2 PM 2.x */
};

/*
 * Bitmap file consists of a BMPFileHeader structure followed by a
 * BMPInfoHeader structure. An array of BMPColorEntry structures (also called
 * a colour table) follows the bitmap information header structure. The colour
 * table is followed by a second array of indexes into the colour table (the
 * actual bitmap data). Data may be comressed, for 4-bpp and 8-bpp used RLE
 * compression.
 *
 * +---------------------+
 * | BMPFileHeader       |
 * +---------------------+
 * | BMPInfoHeader       |
 * +---------------------+
 * | BMPColorEntry array |
 * +---------------------+
 * | Colour-index array  |
 * +---------------------+
 *
 * All numbers stored in Intel order with least significant byte first.
 */

enum BMPComprMethod
{
    BMPC_RGB = 0L,          /* Uncompressed */
    BMPC_RLE8 = 1L,         /* RLE for 8 bpp images */
    BMPC_RLE4 = 2L,         /* RLE for 4 bpp images */
    BMPC_BITFIELDS = 3L,    /* Bitmap is not compressed and the colour table
			     * consists of three DWORD color masks that specify
			     * the red, green, and blue components of each
			     * pixel. This is valid when used with
			     * 16- and 32-bpp bitmaps. */
    BMPC_JPEG = 4L,         /* Indicates that the image is a JPEG image. */
    BMPC_PNG = 5L           /* Indicates that the image is a PNG image. */
};

enum BMPLCSType                 /* Type of logical color space. */
{
    BMPLT_CALIBRATED_RGB = 0,	/* This value indicates that endpoints and
				 * gamma values are given in the appropriate
				 * fields. */
    BMPLT_DEVICE_RGB = 1,
    BMPLT_DEVICE_CMYK = 2
};

typedef struct
{
    int32   iCIEX;
    int32   iCIEY;
    int32   iCIEZ;
} BMPCIEXYZ;

typedef struct                  /* This structure contains the x, y, and z */
{				/* coordinates of the three colors that */
				/* correspond */
    BMPCIEXYZ   iCIERed;        /* to the red, green, and blue endpoints for */
    BMPCIEXYZ   iCIEGreen;      /* a specified logical color space. */
    BMPCIEXYZ	iCIEBlue;
} BMPCIEXYZTriple;

typedef struct
{
    char	bType[2];       /* Signature "BM" */
    uint32	iSize;          /* Size in bytes of the bitmap file. Should
				 * always be ignored while reading because
				 * of error in Windows 3.0 SDK's description
				 * of this field */
    uint16	iReserved1;     /* Reserved, set as 0 */
    uint16	iReserved2;     /* Reserved, set as 0 */
    uint32	iOffBits;       /* Offset of the image from file start in bytes */
} BMPFileHeader;

/* File header size in bytes: */
const int       BFH_SIZE = 14;

typedef struct
{
    uint32	iSize;          /* Size of BMPInfoHeader structure in bytes.
				 * Should be used to determine start of the
				 * colour table */
    int32	iWidth;         /* Image width */
    int32	iHeight;        /* Image height. If positive, image has bottom
				 * left origin, if negative --- top left. */
    int16	iPlanes;        /* Number of image planes (must be set to 1) */
    int16	iBitCount;      /* Number of bits per pixel (1, 4, 8, 16, 24
				 * or 32). If 0 then the number of bits per
				 * pixel is specified or is implied by the
				 * JPEG or PNG format. */
    uint32	iCompression;	/* Compression method */
    uint32	iSizeImage;     /* Size of uncomressed image in bytes. May
				 * be 0 for BMPC_RGB bitmaps. If iCompression
				 * is BI_JPEG or BI_PNG, iSizeImage indicates
				 * the size of the JPEG or PNG image buffer. */
    int32	iXPelsPerMeter; /* X resolution, pixels per meter (0 if not used) */
    int32	iYPelsPerMeter; /* Y resolution, pixels per meter (0 if not used) */
    uint32	iClrUsed;       /* Size of colour table. If 0, iBitCount should
				 * be used to calculate this value
				 * (1<<iBitCount). This value should be
				 * unsigned for proper shifting. */
    int32	iClrImportant;  /* Number of important colours. If 0, all
				 * colours are required */

    /*
     * Fields above should be used for bitmaps, compatible with Windows NT 3.51
     * and earlier. Windows 98/Me, Windows 2000/XP introduces additional fields:
     */

    int32	iRedMask;       /* Colour mask that specifies the red component
				 * of each pixel, valid only if iCompression
				 * is set to BI_BITFIELDS. */
    int32	iGreenMask;     /* The same for green component */
    int32	iBlueMask;      /* The same for blue component */
    int32	iAlphaMask;     /* Colour mask that specifies the alpha
				 * component of each pixel. */
    uint32	iCSType;        /* Colour space of the DIB. */
    BMPCIEXYZTriple sEndpoints; /* This member is ignored unless the iCSType
				 * member specifies BMPLT_CALIBRATED_RGB. */
    int32	iGammaRed;      /* Toned response curve for red. This member
				 * is ignored unless color values are
				 * calibrated RGB values and iCSType is set to
				 * BMPLT_CALIBRATED_RGB. Specified
				 * in 16^16 format. */
    int32	iGammaGreen;    /* Toned response curve for green. */
    int32	iGammaBlue;     /* Toned response curve for blue. */
} BMPInfoHeader;

/*
 * Info header size in bytes:
 */
const unsigned int  BIH_WIN4SIZE = 40; /* for BMPT_WIN4 */
const unsigned int  BIH_WIN5SIZE = 57; /* for BMPT_WIN5 */
const unsigned int  BIH_OS21SIZE = 12; /* for BMPT_OS21 */
const unsigned int  BIH_OS22SIZE = 64; /* for BMPT_OS22 */

/*
 * We will use plain byte array instead of this structure, but declaration
 * provided for reference
 */
typedef struct
{
    char       bBlue;
    char       bGreen;
    char       bRed;
    char       bReserved;      /* Must be 0 */
} BMPColorEntry;

static	uint16 compression = (uint16) -1;
static	int jpegcolormode = JPEGCOLORMODE_RGB;
static	int quality = 75;		/* JPEG quality */
static	uint16 predictor = 0;

static void usage(void);
static int processCompressOptions(char*);
static void rearrangePixels(char *, uint32, uint32);

int
main(int argc, char* argv[])
{
	SensorCall();uint32	width, length;
	uint16	nbands = 1;		/* number of bands in input image */
        uint16	depth = 8;		/* bits per pixel in input image */
	uint32	rowsperstrip = (uint32) -1;
        uint16	photometric = PHOTOMETRIC_MINISBLACK;
	int	fd = 0;
	struct stat instat;
	char	*outfilename = NULL, *infilename = NULL;
	TIFF	*out = NULL;

	BMPFileHeader file_hdr;
        BMPInfoHeader info_hdr;
        int     bmp_type;
        uint32  clr_tbl_size, n_clr_elems = 3;
        unsigned char *clr_tbl;
	unsigned short *red_tbl = NULL, *green_tbl = NULL, *blue_tbl = NULL;
	uint32	row, clr;

	int	c;
	extern int optind;
	extern char* optarg;

	SensorCall();while ((c = getopt(argc, argv, "c:r:o:h")) != -1) {
		SensorCall();switch (c) {
		case 'c':		/* compression scheme */
			SensorCall();if (!processCompressOptions(optarg))
				{/*23*/SensorCall();usage();/*24*/}
			SensorCall();break;
		case 'r':		/* rows/strip */
			SensorCall();rowsperstrip = atoi(optarg);
			SensorCall();break;
		case 'o':
			SensorCall();outfilename = optarg;
			SensorCall();break;
		case 'h':
			SensorCall();usage();
		default:
			SensorCall();break;
		}
	}

	SensorCall();if (argc - optind < 2)
		{/*25*/SensorCall();usage();/*26*/}

	SensorCall();if (outfilename == NULL)
		{/*27*/SensorCall();outfilename = argv[argc-1];/*28*/}
	SensorCall();out = TIFFOpen(outfilename, "w");
	SensorCall();if (out == NULL) {
		SensorCall();TIFFError(infilename, "Cannot open file %s for output",
			  outfilename);
		SensorCall();goto bad3;
	}
	

	SensorCall();while (optind < argc-1) {
		SensorCall();infilename = argv[optind];
		optind++;
	    
		fd = open(infilename, O_RDONLY|O_BINARY, 0);
		SensorCall();if (fd < 0) {
			SensorCall();TIFFError(infilename, "Cannot open input file");
			{int  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}
		}

		SensorCall();read(fd, file_hdr.bType, 2);
		SensorCall();if(file_hdr.bType[0] != 'B' || file_hdr.bType[1] != 'M') {
			SensorCall();TIFFError(infilename, "File is not BMP");
			SensorCall();goto bad;
		}

/* -------------------------------------------------------------------- */
/*      Read the BMPFileHeader. We need iOffBits value only             */
/* -------------------------------------------------------------------- */
		SensorCall();lseek(fd, 10, SEEK_SET);
		read(fd, &file_hdr.iOffBits, 4);
#ifdef WORDS_BIGENDIAN
		TIFFSwabLong(&file_hdr.iOffBits);
#endif
		fstat(fd, &instat);
		file_hdr.iSize = instat.st_size;

/* -------------------------------------------------------------------- */
/*      Read the BMPInfoHeader.                                         */
/* -------------------------------------------------------------------- */

		lseek(fd, BFH_SIZE, SEEK_SET);
		read(fd, &info_hdr.iSize, 4);
#ifdef WORDS_BIGENDIAN
		TIFFSwabLong(&info_hdr.iSize);
#endif

		SensorCall();if (info_hdr.iSize == BIH_WIN4SIZE)
			{/*29*/SensorCall();bmp_type = BMPT_WIN4;/*30*/}
		else {/*31*/SensorCall();if (info_hdr.iSize == BIH_OS21SIZE)
			{/*33*/SensorCall();bmp_type = BMPT_OS21;/*34*/}
		else {/*35*/SensorCall();if (info_hdr.iSize == BIH_OS22SIZE
			 || info_hdr.iSize == 16)
			{/*37*/SensorCall();bmp_type = BMPT_OS22;/*38*/}
		else
			{/*39*/SensorCall();bmp_type = BMPT_WIN5;/*40*/}/*36*/}/*32*/}

		SensorCall();if (bmp_type == BMPT_WIN4
		    || bmp_type == BMPT_WIN5
		    || bmp_type == BMPT_OS22) {
			SensorCall();read(fd, &info_hdr.iWidth, 4);
			read(fd, &info_hdr.iHeight, 4);
			read(fd, &info_hdr.iPlanes, 2);
			read(fd, &info_hdr.iBitCount, 2);
			read(fd, &info_hdr.iCompression, 4);
			read(fd, &info_hdr.iSizeImage, 4);
			read(fd, &info_hdr.iXPelsPerMeter, 4);
			read(fd, &info_hdr.iYPelsPerMeter, 4);
			read(fd, &info_hdr.iClrUsed, 4);
			read(fd, &info_hdr.iClrImportant, 4);
#ifdef WORDS_BIGENDIAN
			TIFFSwabLong((uint32*) &info_hdr.iWidth);
			TIFFSwabLong((uint32*) &info_hdr.iHeight);
			TIFFSwabShort((uint16*) &info_hdr.iPlanes);
			TIFFSwabShort((uint16*) &info_hdr.iBitCount);
			TIFFSwabLong((uint32*) &info_hdr.iCompression);
			TIFFSwabLong((uint32*) &info_hdr.iSizeImage);
			TIFFSwabLong((uint32*) &info_hdr.iXPelsPerMeter);
			TIFFSwabLong((uint32*) &info_hdr.iYPelsPerMeter);
			TIFFSwabLong((uint32*) &info_hdr.iClrUsed);
			TIFFSwabLong((uint32*) &info_hdr.iClrImportant);
#endif
			n_clr_elems = 4;
		}

		SensorCall();if (bmp_type == BMPT_OS22) {
			/* 
			 * FIXME: different info in different documents
			 * regarding this!
			 */
			 SensorCall();n_clr_elems = 3;
		}

		SensorCall();if (bmp_type == BMPT_OS21) {
			SensorCall();int16  iShort;

			read(fd, &iShort, 2);
#ifdef WORDS_BIGENDIAN
			TIFFSwabShort((uint16*) &iShort);
#endif
			info_hdr.iWidth = iShort;
			read(fd, &iShort, 2);
#ifdef WORDS_BIGENDIAN
			TIFFSwabShort((uint16*) &iShort);
#endif
			info_hdr.iHeight = iShort;
			read(fd, &iShort, 2);
#ifdef WORDS_BIGENDIAN
			TIFFSwabShort((uint16*) &iShort);
#endif
			info_hdr.iPlanes = iShort;
			read(fd, &iShort, 2);
#ifdef WORDS_BIGENDIAN
			TIFFSwabShort((uint16*) &iShort);
#endif
			info_hdr.iBitCount = iShort;
			info_hdr.iCompression = BMPC_RGB;
			n_clr_elems = 3;
		}

		SensorCall();if (info_hdr.iBitCount != 1  && info_hdr.iBitCount != 4  &&
		    info_hdr.iBitCount != 8  && info_hdr.iBitCount != 16 &&
		    info_hdr.iBitCount != 24 && info_hdr.iBitCount != 32) {
		    SensorCall();TIFFError(infilename,
			      "Cannot process BMP file with bit count %d",
			      info_hdr.iBitCount);
		    close(fd);
		    {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
		}

        SensorCall();if (info_hdr.iCompression == BMPC_RLE4 && info_hdr.iBitCount != 4)
        {
            SensorCall();TIFFError(infilename,
              "Cannot process BMP file with bit count %d and RLE 4-bit/pixel compression",
              info_hdr.iBitCount);
            close(fd);
            {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
        }
 
        SensorCall();if (info_hdr.iCompression == BMPC_RLE8 && info_hdr.iBitCount != 8)
        {
            SensorCall();TIFFError(infilename,
              "Cannot process BMP file with bit count %d and RLE 8-bit/pixel compression",
              info_hdr.iBitCount);
            close(fd);
            {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
        }

		SensorCall();width = info_hdr.iWidth;
		length = (info_hdr.iHeight > 0) ? info_hdr.iHeight : -info_hdr.iHeight;
        SensorCall();if( width <= 0 || length <= 0 )
        {
            SensorCall();TIFFError(infilename,
                  "Invalid dimensions of BMP file" );
            close(fd);
            {int  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}
        }
 

		SensorCall();switch (info_hdr.iBitCount)
		{
			case 1:
			case 4:
			case 8:
				SensorCall();nbands = 1;
				depth = info_hdr.iBitCount;
				photometric = PHOTOMETRIC_PALETTE;
				/* Allocate memory for colour table and read it. */
				SensorCall();if (info_hdr.iClrUsed)
				    {/*41*/SensorCall();clr_tbl_size =
					    ((uint32)(1<<depth)<info_hdr.iClrUsed)
					    ? (uint32) (1 << depth)
					    : info_hdr.iClrUsed;/*42*/}
				else
				    {/*43*/SensorCall();clr_tbl_size = 1 << depth;/*44*/}
				SensorCall();clr_tbl = (unsigned char *)
					_TIFFmalloc(n_clr_elems * clr_tbl_size);
				SensorCall();if (!clr_tbl) {
					SensorCall();TIFFError(infilename,
					"Can't allocate space for color table");
					SensorCall();goto bad;
				}

				SensorCall();lseek(fd, BFH_SIZE + info_hdr.iSize, SEEK_SET);
				read(fd, clr_tbl, n_clr_elems * clr_tbl_size);

				red_tbl = (unsigned short*)
					_TIFFmalloc(((tmsize_t)1)<<depth * sizeof(unsigned short));
				SensorCall();if (!red_tbl) {
					SensorCall();TIFFError(infilename,
				"Can't allocate space for red component table");
					_TIFFfree(clr_tbl);
					SensorCall();goto bad1;
				}
				SensorCall();green_tbl = (unsigned short*)
					_TIFFmalloc(((tmsize_t)1)<<depth * sizeof(unsigned short));
				SensorCall();if (!green_tbl) {
					SensorCall();TIFFError(infilename,
				"Can't allocate space for green component table");
					_TIFFfree(clr_tbl);
					SensorCall();goto bad2;
				}
				SensorCall();blue_tbl = (unsigned short*)
					_TIFFmalloc(((tmsize_t)1)<<depth * sizeof(unsigned short));
				SensorCall();if (!blue_tbl) {
					SensorCall();TIFFError(infilename,
				"Can't allocate space for blue component table");
					_TIFFfree(clr_tbl);
					SensorCall();goto bad3;
				}

				SensorCall();for(clr = 0; clr < clr_tbl_size; clr++) {
				    SensorCall();red_tbl[clr] = 257*clr_tbl[clr*n_clr_elems+2];
				    green_tbl[clr] = 257*clr_tbl[clr*n_clr_elems+1];
				    blue_tbl[clr] = 257*clr_tbl[clr*n_clr_elems];
				}

				SensorCall();_TIFFfree(clr_tbl);
				SensorCall();break;
			case 16:
			case 24:
				SensorCall();nbands = 3;
				depth = info_hdr.iBitCount / nbands;
				photometric = PHOTOMETRIC_RGB;
				SensorCall();break;
			case 32:
				SensorCall();nbands = 3;
				depth = 8;
				photometric = PHOTOMETRIC_RGB;
				SensorCall();break;
			default:
				SensorCall();break;
		}

/* -------------------------------------------------------------------- */
/*  Create output file.                                                 */
/* -------------------------------------------------------------------- */

		SensorCall();TIFFSetField(out, TIFFTAG_IMAGEWIDTH, width);
		TIFFSetField(out, TIFFTAG_IMAGELENGTH, length);
		TIFFSetField(out, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
		TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, nbands);
		TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, depth);
		TIFFSetField(out, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
		TIFFSetField(out, TIFFTAG_PHOTOMETRIC, photometric);
		TIFFSetField(out, TIFFTAG_ROWSPERSTRIP,
			     TIFFDefaultStripSize(out, rowsperstrip));
		
		SensorCall();if (red_tbl && green_tbl && blue_tbl) {
			SensorCall();TIFFSetField(out, TIFFTAG_COLORMAP,
				     red_tbl, green_tbl, blue_tbl);
		}
		
		SensorCall();if (compression == (uint16) -1)
			compression = COMPRESSION_PACKBITS;
		SensorCall();TIFFSetField(out, TIFFTAG_COMPRESSION, compression);
		SensorCall();switch (compression) {
		case COMPRESSION_JPEG:
			SensorCall();if (photometric == PHOTOMETRIC_RGB
			    && jpegcolormode == JPEGCOLORMODE_RGB)
				photometric = PHOTOMETRIC_YCBCR;
			SensorCall();TIFFSetField(out, TIFFTAG_JPEGQUALITY, quality);
			TIFFSetField(out, TIFFTAG_JPEGCOLORMODE, jpegcolormode);
			SensorCall();break;
		case COMPRESSION_LZW:
		case COMPRESSION_DEFLATE:
			SensorCall();if (predictor != 0)
				{/*45*/SensorCall();TIFFSetField(out, TIFFTAG_PREDICTOR, predictor);/*46*/}
			SensorCall();break;
		}

/* -------------------------------------------------------------------- */
/*  Read uncompressed image data.                                       */
/* -------------------------------------------------------------------- */

		SensorCall();if (info_hdr.iCompression == BMPC_RGB) {
			SensorCall();uint32 offset, size;
			char *scanbuf;

			/* XXX: Avoid integer overflow. We can calculate size
			 * in one step using
			 *
			 *  size = ((width * info_hdr.iBitCount + 31) & ~31) / 8
			 *
			 * formulae, but we should check for overflow
			 * conditions during calculation.
			 */
			size = width * info_hdr.iBitCount + 31;
			SensorCall();if (!width || !info_hdr.iBitCount
			    || (size - 31) / info_hdr.iBitCount != width ) {
				SensorCall();TIFFError(infilename,
					  "Wrong image parameters; can't "
					  "allocate space for scanline buffer");
				SensorCall();goto bad3;
			}
			SensorCall();size = (size & ~31) / 8;

			scanbuf = (char *) _TIFFmalloc(size);
			SensorCall();if (!scanbuf) {
				SensorCall();TIFFError(infilename,
				"Can't allocate space for scanline buffer");
				SensorCall();goto bad3;
			}

			SensorCall();for (row = 0; row < length; row++) {
				SensorCall();if (info_hdr.iHeight > 0)
					{/*47*/SensorCall();offset = file_hdr.iOffBits+(length-row-1)*size;/*48*/}
				else
					{/*49*/SensorCall();offset = file_hdr.iOffBits + row * size;/*50*/}
				SensorCall();if (lseek(fd, offset, SEEK_SET) == (off_t)-1) {
					SensorCall();TIFFError(infilename,
						  "scanline %lu: Seek error",
						  (unsigned long) row);
					SensorCall();break;
				}

				SensorCall();if (read(fd, scanbuf, size) < 0) {
					SensorCall();TIFFError(infilename,
						  "scanline %lu: Read error",
						  (unsigned long) row);
					SensorCall();break;
				}

				SensorCall();rearrangePixels(scanbuf, width, info_hdr.iBitCount);

				SensorCall();if (TIFFWriteScanline(out, scanbuf, row, 0)<0) {
					SensorCall();TIFFError(infilename,
						  "scanline %lu: Write error",
						  (unsigned long) row);
					SensorCall();break;
				}
			}

			SensorCall();_TIFFfree(scanbuf);

/* -------------------------------------------------------------------- */
/*  Read compressed image data.                                         */
/* -------------------------------------------------------------------- */

		} else {/*51*/SensorCall();if ( info_hdr.iCompression == BMPC_RLE8
			    || info_hdr.iCompression == BMPC_RLE4 ) {
			SensorCall();uint32		i, j, k, runlength;
			uint32		compr_size, uncompr_size;
			uint32      bits = 0;
			unsigned char   *comprbuf;
			unsigned char   *uncomprbuf;

			compr_size = file_hdr.iSize - file_hdr.iOffBits;

			bits = info_hdr.iBitCount;

			SensorCall();if (bits > 8) // bit depth is > 8bit, adjust size
			{
				SensorCall();uncompr_size = width * length * (bits / 8);
				/* Detect int overflow */
				SensorCall();if (uncompr_size / width / (bits / 8) != length) {
					SensorCall();TIFFError(infilename,
							   "Invalid dimensions of BMP file");
					close(fd);
					{int  ReplaceReturn = -1; SensorCall(); return ReplaceReturn;}
				}
			}
			else
				{/*53*/SensorCall();uncompr_size = width * length;/*54*/}
			SensorCall();comprbuf = (unsigned char *) _TIFFmalloc( compr_size );
			SensorCall();if (!comprbuf) {
				SensorCall();TIFFError(infilename,
			"Can't allocate space for compressed scanline buffer");
				SensorCall();goto bad3;
			}
			SensorCall();uncomprbuf = (unsigned char *)_TIFFmalloc(uncompr_size);
			SensorCall();if (!uncomprbuf) {
				SensorCall();TIFFError(infilename,
			"Can't allocate space for uncompressed scanline buffer");
				SensorCall();goto bad3;
			}

			SensorCall();lseek(fd, file_hdr.iOffBits, SEEK_SET);
			read(fd, comprbuf, compr_size);
			i = 0;
			j = 0;
			SensorCall();if (info_hdr.iBitCount == 8) {		/* RLE8 */
			    SensorCall();while(j < uncompr_size && i < compr_size) {
				SensorCall();if ( comprbuf[i] ) {
				    SensorCall();runlength = comprbuf[i++];
				    SensorCall();while( runlength > 0
					   && j < uncompr_size
					   && i < compr_size ) {
					SensorCall();uncomprbuf[j++] = comprbuf[i];
					runlength--;
				    }
				    SensorCall();i++;
				} else {
				    SensorCall();i++;
				    SensorCall();if (comprbuf[i] == 0) /* Next scanline */
					{/*55*/SensorCall();i++;/*56*/}
				    else {/*57*/SensorCall();if (comprbuf[i] == 1) /* End of image */
					{/*59*/SensorCall();break;/*60*/}
				    else {/*61*/SensorCall();if (comprbuf[i] == 2) { /* Move to... */
					SensorCall();i++;
					SensorCall();if (i < compr_size - 1) {
					    SensorCall();j+=comprbuf[i]+comprbuf[i+1]*width;
					    i += 2;
					}
					else
					    {/*63*/SensorCall();break;/*64*/}
				    } else {            /* Absolute mode */
					SensorCall();runlength = comprbuf[i++];
					SensorCall();for (k = 0; k < runlength && j < uncompr_size && i < compr_size; k++)
					    {/*65*/SensorCall();uncomprbuf[j++] = comprbuf[i++];/*66*/}
					SensorCall();if ( k & 0x01 )
					    {/*67*/SensorCall();i++;/*68*/}
				    ;/*62*/}/*58*/}}
				}
			    }
			}
			else {				    /* RLE4 */
			    SensorCall();while( j < uncompr_size && i < compr_size ) {
				SensorCall();if ( comprbuf[i] ) {
				    SensorCall();runlength = comprbuf[i++];
				    SensorCall();while( runlength > 0 && j < uncompr_size && i < compr_size ) {
					SensorCall();if ( runlength & 0x01 )
					    {/*69*/SensorCall();uncomprbuf[j++] = (comprbuf[i] & 0xF0) >> 4;/*70*/}
					else
					    {/*71*/SensorCall();uncomprbuf[j++] = comprbuf[i] & 0x0F;/*72*/}
					SensorCall();runlength--;
				    }
				    SensorCall();i++;
				} else {
				    SensorCall();i++;
				    SensorCall();if (comprbuf[i] == 0) /* Next scanline */
					{/*73*/SensorCall();i++;/*74*/}
				    else {/*75*/SensorCall();if (comprbuf[i] == 1) /* End of image */
					{/*77*/SensorCall();break;/*78*/}
				    else {/*79*/SensorCall();if (comprbuf[i] == 2) { /* Move to... */
					SensorCall();i++;
					SensorCall();if (i < compr_size - 1) {
					    SensorCall();j+=comprbuf[i]+comprbuf[i+1]*width;
					    i += 2;
					}
					else
					    {/*81*/SensorCall();break;/*82*/}
				    } else {            /* Absolute mode */
					SensorCall();runlength = comprbuf[i++];
					SensorCall();for (k = 0; k < runlength && j < uncompr_size && i < compr_size; k++) {
					    SensorCall();if (k & 0x01)
						{/*83*/SensorCall();uncomprbuf[j++] = comprbuf[i++] & 0x0F;/*84*/}
					    else
						{/*85*/SensorCall();uncomprbuf[j++] = (comprbuf[i] & 0xF0) >> 4;/*86*/}
					}
					SensorCall();if (k & 0x01)
					    {/*87*/SensorCall();i++;/*88*/}
				    ;/*80*/}/*76*/}}
				}
			    }
			}

			SensorCall();_TIFFfree(comprbuf);

			SensorCall();for (row = 0; row < length; row++) {
				SensorCall();if (TIFFWriteScanline(out,
					uncomprbuf + (length - row - 1) * width,
					row, 0) < 0) {
					SensorCall();TIFFError(infilename,
						"scanline %lu: Write error.\n",
						  (unsigned long) row);
				}
			}

			SensorCall();_TIFFfree(uncomprbuf);
		;/*52*/}}
		SensorCall();TIFFWriteDirectory(out);
		SensorCall();if (blue_tbl) {
		  SensorCall();_TIFFfree(blue_tbl);
		  blue_tbl=NULL;
		}
		SensorCall();if (green_tbl) {
		  SensorCall();_TIFFfree(green_tbl);
		  green_tbl=NULL;
		}
		SensorCall();if (red_tbl) {
		  SensorCall();_TIFFfree(red_tbl);
		  red_tbl=NULL;
		}
	}

bad3:
	SensorCall();if (blue_tbl)
		{/*89*/SensorCall();_TIFFfree(blue_tbl);/*90*/}
bad2:
	SensorCall();if (green_tbl)
		{/*91*/SensorCall();_TIFFfree(green_tbl);/*92*/}
bad1:
	SensorCall();if (red_tbl)
		{/*93*/SensorCall();_TIFFfree(red_tbl);/*94*/}
bad:
        SensorCall();close(fd);

	SensorCall();if (out)
		{/*95*/SensorCall();TIFFClose(out);/*96*/}
        {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

/*
 * Image data in BMP file stored in BGR (or ABGR) format. We should rearrange
 * pixels to RGB (RGBA) format.
 */
static void
rearrangePixels(char *buf, uint32 width, uint32 bit_count)
{
	SensorCall();char tmp;
	uint32 i;

        SensorCall();switch(bit_count) {
		case 16:    /* FIXME: need a sample file */
                        SensorCall();break;
                case 24:
			SensorCall();for (i = 0; i < width; i++, buf += 3) {
				SensorCall();tmp = *buf;
				*buf = *(buf + 2);
				*(buf + 2) = tmp;
			}
                        SensorCall();break;
                case 32:
			{
				SensorCall();char	*buf1 = buf;

				SensorCall();for (i = 0; i < width; i++, buf += 4) {
					SensorCall();tmp = *buf;
					*buf1++ = *(buf + 2);
					*buf1++ = *(buf + 1);
					*buf1++ = tmp;
				}
			}
                        SensorCall();break;
                default:
                        SensorCall();break;
        }
SensorCall();}

static int
processCompressOptions(char* opt)
{
	SensorCall();if (strcmp(opt, "none") == 0)
		compression = COMPRESSION_NONE;
	else {/*3*/SensorCall();if (strcmp(opt, "packbits") == 0)
		compression = COMPRESSION_PACKBITS;
	else {/*5*/SensorCall();if (strncmp(opt, "jpeg", 4) == 0) {
		SensorCall();char* cp = strchr(opt, ':');

                compression = COMPRESSION_JPEG;
                SensorCall();while( cp )
                {
                    SensorCall();if (isdigit((int)cp[1]))
			{/*7*/SensorCall();quality = atoi(cp+1);/*8*/}
                    else {/*9*/SensorCall();if (cp[1] == 'r' )
			jpegcolormode = JPEGCOLORMODE_RAW;
                    else
                        {/*11*/SensorCall();usage();/*12*/}/*10*/}

                    SensorCall();cp = strchr(cp+1,':');
                }
	} else {/*13*/SensorCall();if (strncmp(opt, "lzw", 3) == 0) {
		SensorCall();char* cp = strchr(opt, ':');
		SensorCall();if (cp)
			{/*15*/SensorCall();predictor = atoi(cp+1);/*16*/}
		SensorCall();compression = COMPRESSION_LZW;
	} else {/*17*/SensorCall();if (strncmp(opt, "zip", 3) == 0) {
		SensorCall();char* cp = strchr(opt, ':');
		SensorCall();if (cp)
			{/*19*/SensorCall();predictor = atoi(cp+1);/*20*/}
		SensorCall();compression = COMPRESSION_DEFLATE;
	} else
		{/*21*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*22*/}/*18*/}/*14*/}/*6*/}/*4*/}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

static char* stuff[] = {
"bmp2tiff --- convert Windows BMP files to TIFF",
"usage: bmp2tiff [options] input.bmp [input2.bmp ...] output.tif",
"where options are:",
" -r #		make each strip have no more than # rows",
"",
" -c lzw[:opts]	compress output with Lempel-Ziv & Welch encoding",
" -c zip[:opts]	compress output with deflate encoding",
" -c jpeg[:opts]compress output with JPEG encoding",
" -c packbits	compress output with packbits encoding",
" -c none	use no compression algorithm on output",
"",
"JPEG options:",
" #		set compression quality level (0-100, default 75)",
" r		output color image as RGB rather than YCbCr",
"For example, -c jpeg:r:50 to get JPEG-encoded RGB data with 50% comp. quality",
"",
"LZW and deflate options:",
" #		set predictor value",
"For example, -c lzw:2 to get LZW-encoded data with horizontal differencing",
" -o out.tif	write output to out.tif",
" -h		this help message",
NULL
};

static void
usage(void)
{
	SensorCall();char buf[BUFSIZ];
	int i;

	setbuf(stderr, buf);
        fprintf(stderr, "%s\n\n", TIFFGetVersion());
	SensorCall();for (i = 0; stuff[i] != NULL; i++)
		{/*1*/SensorCall();fprintf(stderr, "%s\n", stuff[i]);/*2*/}
	SensorCall();exit(-1);
SensorCall();}

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
