/******************************************************************************
 * $Id: tiffset.c,v 1.17 2012-07-29 15:45:30 tgl Exp $
 *
 * Project:  libtiff tools
 * Purpose:  Mainline for setting metadata in existing TIFF files.
 * Author:   Frank Warmerdam, warmerdam@pobox.com
 *
 ******************************************************************************
 * Copyright (c) 2000, Frank Warmerdam
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 ******************************************************************************
 */


#include <stdio.h>
#include "/var/tmp/sensor.h"
#include <string.h>
#include <stdlib.h>

#include "tiffio.h"

static char* usageMsg[] = {
"usage: tiffset [options] filename",
"where options are:",
" -s <tagname> [count] <value>...   set the tag value",
" -d <dirno> set the directory",
" -sd <diroff> set the subdirectory",
" -sf <tagname> <filename>  read the tag value from file (for ASCII tags only)",
NULL
};

static void
usage(void)
{
	SensorCall();int i;
	SensorCall();for (i = 0; usageMsg[i]; i++)
		{/*1*/SensorCall();fprintf(stderr, "%s\n", usageMsg[i]);/*2*/}
	SensorCall();exit(-1);
SensorCall();}

static const TIFFField *
GetField(TIFF *tiff, const char *tagname)
{
    SensorCall();const TIFFField *fip;

    SensorCall();if( atoi(tagname) > 0 )
        {/*3*/SensorCall();fip = TIFFFieldWithTag(tiff, (ttag_t)atoi(tagname));/*4*/}
    else
        {/*5*/SensorCall();fip = TIFFFieldWithName(tiff, tagname);/*6*/}

    SensorCall();if (!fip) {
        SensorCall();fprintf( stderr, "Field name \"%s\" is not recognised.\n", tagname );
        {const TIFFField * ReplaceReturn = (TIFFField *)NULL; SensorCall(); return ReplaceReturn;}
    }

    {const TIFFField * ReplaceReturn = fip; SensorCall(); return ReplaceReturn;}
}

int
main(int argc, char* argv[])
{
    SensorCall();TIFF *tiff;
    int  arg_index;

    SensorCall();if (argc < 2)
        {/*7*/SensorCall();usage();/*8*/}

    SensorCall();tiff = TIFFOpen(argv[argc-1], "r+");
    SensorCall();if (tiff == NULL)
        {/*9*/{int  ReplaceReturn = 2; SensorCall(); return ReplaceReturn;}/*10*/}

    SensorCall();for( arg_index = 1; arg_index < argc-1; arg_index++ ) {
	SensorCall();if (strcmp(argv[arg_index],"-d") == 0 && arg_index < argc-2) {
	    SensorCall();arg_index++;
	    SensorCall();if( TIFFSetDirectory(tiff, atoi(argv[arg_index]) ) != 1 )
            {
               SensorCall();fprintf( stderr, "Failed to set directory=%s\n", argv[arg_index] );
               {int  ReplaceReturn = 6; SensorCall(); return ReplaceReturn;}
            }
	    SensorCall();arg_index++;
	}
	SensorCall();if (strcmp(argv[arg_index],"-sd") == 0 && arg_index < argc-2) {
	    SensorCall();arg_index++;
	    SensorCall();if( TIFFSetSubDirectory(tiff, atoi(argv[arg_index]) ) != 1 )
            {
               SensorCall();fprintf( stderr, "Failed to set sub directory=%s\n", argv[arg_index] );
               {int  ReplaceReturn = 7; SensorCall(); return ReplaceReturn;}
            }
	    SensorCall();arg_index++;
	}
        SensorCall();if (strcmp(argv[arg_index],"-s") == 0 && arg_index < argc-3) {
            SensorCall();const TIFFField *fip;
            const char *tagname;

            arg_index++;
            tagname = argv[arg_index];
            fip = GetField(tiff, tagname);

            SensorCall();if (!fip)
                {/*11*/{int  ReplaceReturn = 3; SensorCall(); return ReplaceReturn;}/*12*/}

            SensorCall();arg_index++;
            SensorCall();if (TIFFFieldDataType(fip) == TIFF_ASCII) {
                SensorCall();if (TIFFSetField(tiff, TIFFFieldTag(fip), argv[arg_index]) != 1)
                    {/*13*/SensorCall();fprintf( stderr, "Failed to set %s=%s\n",
                             TIFFFieldName(fip), argv[arg_index] );/*14*/}
            } else {/*15*/SensorCall();if (TIFFFieldWriteCount(fip) > 0
		       || TIFFFieldWriteCount(fip) == TIFF_VARIABLE) {
                SensorCall();int     ret = 1;
                short   wc;

                SensorCall();if (TIFFFieldWriteCount(fip) == TIFF_VARIABLE)
                        {/*17*/SensorCall();wc = atoi(argv[arg_index++]);/*18*/}
                else
                        {/*19*/SensorCall();wc = TIFFFieldWriteCount(fip);/*20*/}

                SensorCall();if (argc - arg_index < wc) {
                    SensorCall();fprintf( stderr,
                             "Number of tag values is not enough. "
                             "Expected %d values for %s tag, got %d\n",
                             wc, TIFFFieldName(fip), argc - arg_index);
                    {int  ReplaceReturn = 4; SensorCall(); return ReplaceReturn;}
                }
                    
                SensorCall();if (wc > 1) {
                        SensorCall();int     i, size;
                        void    *array;

                        SensorCall();switch (TIFFFieldDataType(fip)) {
                                /*
                                 * XXX: We can't use TIFFDataWidth()
                                 * to determine the space needed to store
                                 * the value. For TIFF_RATIONAL values
                                 * TIFFDataWidth() returns 8, but we use 4-byte
                                 * float to represent rationals.
                                 */
                                case TIFF_BYTE:
                                case TIFF_ASCII:
                                case TIFF_SBYTE:
                                case TIFF_UNDEFINED:
				default:
                                    SensorCall();size = 1;
                                    SensorCall();break;

                                case TIFF_SHORT:
                                case TIFF_SSHORT:
                                    SensorCall();size = 2;
                                    SensorCall();break;

                                case TIFF_LONG:
                                case TIFF_SLONG:
                                case TIFF_FLOAT:
                                case TIFF_IFD:
                                case TIFF_RATIONAL:
                                case TIFF_SRATIONAL:
                                    SensorCall();size = 4;
                                    SensorCall();break;

                                case TIFF_DOUBLE:
                                    SensorCall();size = 8;
                                    SensorCall();break;
                        }

                        SensorCall();array = _TIFFmalloc(wc * size);
                        SensorCall();if (!array) {
                                SensorCall();fprintf(stderr, "No space for %s tag\n",
                                        tagname);
                                {int  ReplaceReturn = 4; SensorCall(); return ReplaceReturn;}
                        }

                        SensorCall();switch (TIFFFieldDataType(fip)) {
                            case TIFF_BYTE:
                                SensorCall();for (i = 0; i < wc; i++)
                                    {/*21*/SensorCall();((uint8 *)array)[i] = atoi(argv[arg_index+i]);/*22*/}
                                SensorCall();break;
                            case TIFF_SHORT:
                                SensorCall();for (i = 0; i < wc; i++)
                                    {/*23*/SensorCall();((uint16 *)array)[i] = atoi(argv[arg_index+i]);/*24*/}
                                SensorCall();break;
                            case TIFF_SBYTE:
                                SensorCall();for (i = 0; i < wc; i++)
                                    {/*25*/SensorCall();((int8 *)array)[i] = atoi(argv[arg_index+i]);/*26*/}
                                SensorCall();break;
                            case TIFF_SSHORT:
                                SensorCall();for (i = 0; i < wc; i++)
                                    {/*27*/SensorCall();((int16 *)array)[i] = atoi(argv[arg_index+i]);/*28*/}
                                SensorCall();break;
                            case TIFF_LONG:
                                SensorCall();for (i = 0; i < wc; i++)
                                    {/*29*/SensorCall();((uint32 *)array)[i] = atol(argv[arg_index+i]);/*30*/}
                                SensorCall();break;
                            case TIFF_SLONG:
                            case TIFF_IFD:
                                SensorCall();for (i = 0; i < wc; i++)
                                    {/*31*/SensorCall();((uint32 *)array)[i] = atol(argv[arg_index+i]);/*32*/}
                                SensorCall();break;
                            case TIFF_DOUBLE:
                                SensorCall();for (i = 0; i < wc; i++)
                                    {/*33*/SensorCall();((double *)array)[i] = atof(argv[arg_index+i]);/*34*/}
                                SensorCall();break;
                            case TIFF_RATIONAL:
                            case TIFF_SRATIONAL:
                            case TIFF_FLOAT:
                                SensorCall();for (i = 0; i < wc; i++)
                                    {/*35*/SensorCall();((float *)array)[i] = (float)atof(argv[arg_index+i]);/*36*/}
                                SensorCall();break;
                            default:
                                SensorCall();break;
                        }
                
                        SensorCall();if (TIFFFieldPassCount(fip)) {
                                SensorCall();ret = TIFFSetField(tiff, TIFFFieldTag(fip),
                                                   wc, array);
                        } else {/*37*/SensorCall();if (TIFFFieldTag(fip) == TIFFTAG_PAGENUMBER
				   || TIFFFieldTag(fip) == TIFFTAG_HALFTONEHINTS
				   || TIFFFieldTag(fip) == TIFFTAG_YCBCRSUBSAMPLING
				   || TIFFFieldTag(fip) == TIFFTAG_DOTRANGE) {
       				SensorCall();if (TIFFFieldDataType(fip) == TIFF_BYTE) {
					SensorCall();ret = TIFFSetField(tiff, TIFFFieldTag(fip),
						((uint8 *)array)[0], ((uint8 *)array)[1]);
				} else {/*39*/SensorCall();if (TIFFFieldDataType(fip) == TIFF_SHORT) {
					SensorCall();ret = TIFFSetField(tiff, TIFFFieldTag(fip),
						((uint16 *)array)[0], ((uint16 *)array)[1]);
				;/*40*/}}
			} else {
                                SensorCall();ret = TIFFSetField(tiff, TIFFFieldTag(fip),
                                                   array);
                        ;/*38*/}}

                        SensorCall();_TIFFfree(array);
                } else {
                        SensorCall();switch (TIFFFieldDataType(fip)) {
                            case TIFF_BYTE:
                            case TIFF_SHORT:
                            case TIFF_SBYTE:
                            case TIFF_SSHORT:
                                SensorCall();ret = TIFFSetField(tiff, TIFFFieldTag(fip),
                                                   atoi(argv[arg_index++]));
                                SensorCall();break;
                            case TIFF_LONG:
                            case TIFF_SLONG:
                            case TIFF_IFD:
                                SensorCall();ret = TIFFSetField(tiff, TIFFFieldTag(fip),
                                                   atol(argv[arg_index++]));
                                SensorCall();break;
                            case TIFF_DOUBLE:
                                SensorCall();ret = TIFFSetField(tiff, TIFFFieldTag(fip),
                                                   atof(argv[arg_index++]));
                                SensorCall();break;
                            case TIFF_RATIONAL:
                            case TIFF_SRATIONAL:
                            case TIFF_FLOAT:
                                SensorCall();ret = TIFFSetField(tiff, TIFFFieldTag(fip),
                                                   (float)atof(argv[arg_index++]));
                                SensorCall();break;
                            default:
                                SensorCall();break;
                        }
                }

                SensorCall();if (ret != 1)
                    {/*41*/SensorCall();fprintf(stderr, "Failed to set %s\n", TIFFFieldName(fip));/*42*/}
                SensorCall();arg_index += wc;
            ;/*16*/}}
        } else {/*43*/SensorCall();if (strcmp(argv[arg_index],"-sf") == 0 && arg_index < argc-3) {
            SensorCall();FILE    *fp;
            const TIFFField *fip;
            char    *text;
            size_t  len;

            arg_index++;
            fip = GetField(tiff, argv[arg_index]);

            SensorCall();if (!fip)
                {/*45*/{int  ReplaceReturn = 3; SensorCall(); return ReplaceReturn;}/*46*/}

            SensorCall();if (TIFFFieldDataType(fip) != TIFF_ASCII) {
                SensorCall();fprintf( stderr,
                         "Only ASCII tags can be set from file. "
                         "%s is not ASCII tag.\n", TIFFFieldName(fip) );
                {int  ReplaceReturn = 5; SensorCall(); return ReplaceReturn;}
            }

            SensorCall();arg_index++;
            fp = fopen( argv[arg_index], "rt" );
            SensorCall();if(fp == NULL) {
                SensorCall();perror( argv[arg_index] );
                SensorCall();continue;
            }

            SensorCall();text = (char *) malloc(1000000);
            len = fread( text, 1, 999999, fp );
            text[len] = '\0';

            fclose( fp );

            SensorCall();if(TIFFSetField( tiff, TIFFFieldTag(fip), text ) != 1) {
                SensorCall();fprintf(stderr, "Failed to set %s from file %s\n", 
                        TIFFFieldName(fip), argv[arg_index]);
            }

            SensorCall();_TIFFfree( text );
            arg_index++;
        } else {
            SensorCall();fprintf(stderr, "Unrecognised option: %s\n",
                    argv[arg_index]);
            usage();
        ;/*44*/}}
    }

    SensorCall();TIFFRewriteDirectory(tiff);
    TIFFClose(tiff);
    {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
