/* $Id: thumbnail.c,v 1.16 2010-07-02 12:02:56 dron Exp $ */

/*
 * Copyright (c) 1994-1997 Sam Leffler
 * Copyright (c) 1994-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

#include "tif_config.h"
#include "/var/tmp/sensor.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifdef NEED_LIBPORT
# include "libport.h"
#endif

#include "tiffio.h"

#ifndef HAVE_GETOPT
extern int getopt(int, char**, char*);
#endif

#define	streq(a,b)	(strcmp(a,b) == 0)

#ifndef TIFFhowmany8
# define TIFFhowmany8(x) (((x)&0x07)?((uint32)(x)>>3)+1:(uint32)(x)>>3)
#endif

typedef enum {
    EXP50,
    EXP60,
    EXP70,
    EXP80,
    EXP90,
    EXP,
    LINEAR
} Contrast;

static	uint32 tnw = 216;		/* thumbnail width */
static	uint32 tnh = 274;		/* thumbnail height */
static	Contrast contrast = LINEAR;	/* current contrast */
static	uint8* thumbnail;

static	int cpIFD(TIFF*, TIFF*);
static	int generateThumbnail(TIFF*, TIFF*);
static	void initScale();
static	void usage(void);

extern	char* optarg;
extern	int optind;

int
main(int argc, char* argv[])
{
    SensorCall();TIFF* in;
    TIFF* out;
    int c;

    SensorCall();while ((c = getopt(argc, argv, "w:h:c:")) != -1) {
	SensorCall();switch (c) {
	case 'w':	SensorCall();tnw = strtoul(optarg, NULL, 0); SensorCall();break;
	case 'h':	SensorCall();tnh = strtoul(optarg, NULL, 0); SensorCall();break;
	case 'c':	SensorCall();contrast = streq(optarg, "exp50") ? EXP50 :
				   streq(optarg, "exp60") ? EXP60 :
				   streq(optarg, "exp70") ? EXP70 :
				   streq(optarg, "exp80") ? EXP80 :
				   streq(optarg, "exp90") ? EXP90 :
				   streq(optarg, "exp")   ? EXP :
				   streq(optarg, "linear")? LINEAR :
							    EXP;
			SensorCall();break;
	default:	SensorCall();usage();
	}
    }
    SensorCall();if (argc-optind != 2)
	{/*9*/SensorCall();usage();/*10*/}

    SensorCall();out = TIFFOpen(argv[optind+1], "w");
    SensorCall();if (out == NULL)
	{/*11*/{int  ReplaceReturn = 2; SensorCall(); return ReplaceReturn;}/*12*/}
    SensorCall();in = TIFFOpen(argv[optind], "r");
    SensorCall();if( in == NULL )
        {/*13*/{int  ReplaceReturn = 2; SensorCall(); return ReplaceReturn;}/*14*/}

    SensorCall();thumbnail = (uint8*) _TIFFmalloc(tnw * tnh);
    SensorCall();if (!thumbnail) {
	    SensorCall();TIFFError(TIFFFileName(in),
		      "Can't allocate space for thumbnail buffer.");
	    {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
    }

    SensorCall();if (in != NULL) {
	SensorCall();initScale();
	SensorCall();do {
	    SensorCall();if (!generateThumbnail(in, out))
		{/*15*/SensorCall();goto bad;/*16*/}
	    SensorCall();if (!cpIFD(in, out) || !TIFFWriteDirectory(out))
		{/*17*/SensorCall();goto bad;/*18*/}
	} while (TIFFReadDirectory(in));
	SensorCall();(void) TIFFClose(in);
    }
    SensorCall();(void) TIFFClose(out);
    {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
bad:
    (void) TIFFClose(out);
    return 1;
}

#define	CopyField(tag, v) \
    if (TIFFGetField(in, tag, &v)) TIFFSetField(out, tag, v)
#define	CopyField2(tag, v1, v2) \
    if (TIFFGetField(in, tag, &v1, &v2)) TIFFSetField(out, tag, v1, v2)
#define	CopyField3(tag, v1, v2, v3) \
    if (TIFFGetField(in, tag, &v1, &v2, &v3)) TIFFSetField(out, tag, v1, v2, v3)
#define	CopyField4(tag, v1, v2, v3, v4) \
    if (TIFFGetField(in, tag, &v1, &v2, &v3, &v4)) TIFFSetField(out, tag, v1, v2, v3, v4)

static void
cpTag(TIFF* in, TIFF* out, uint16 tag, uint16 count, TIFFDataType type)
{
	SensorCall();switch (type) {
	case TIFF_SHORT:
		SensorCall();if (count == 1) {
			SensorCall();uint16 shortv;
			CopyField(tag, shortv);
		} else {/*19*/SensorCall();if (count == 2) {
			SensorCall();uint16 shortv1, shortv2;
			CopyField2(tag, shortv1, shortv2);
		} else {/*21*/SensorCall();if (count == 4) {
			SensorCall();uint16 *tr, *tg, *tb, *ta;
			CopyField4(tag, tr, tg, tb, ta);
		} else {/*23*/SensorCall();if (count == (uint16) -1) {
			SensorCall();uint16 shortv1;
			uint16* shortav;
			CopyField2(tag, shortv1, shortav);
		;/*24*/}/*22*/}/*20*/}}
		SensorCall();break;
	case TIFF_LONG:
		{ SensorCall();uint32 longv;
		  CopyField(tag, longv);
		}
		SensorCall();break;
	case TIFF_LONG8:
		{ SensorCall();uint64 longv8;
		  CopyField(tag, longv8);
		}
		SensorCall();break;
	case TIFF_SLONG8:
		{ SensorCall();int64 longv8;
		  CopyField(tag, longv8);
		}
		SensorCall();break;
	case TIFF_RATIONAL:
		SensorCall();if (count == 1) {
			SensorCall();float floatv;
			CopyField(tag, floatv);
		} else {/*25*/SensorCall();if (count == (uint16) -1) {
			SensorCall();float* floatav;
			CopyField(tag, floatav);
		;/*26*/}}
		SensorCall();break;
	case TIFF_ASCII:
		{ SensorCall();char* stringv;
		  CopyField(tag, stringv);
		}
		SensorCall();break;
	case TIFF_DOUBLE:
		SensorCall();if (count == 1) {
			SensorCall();double doublev;
			CopyField(tag, doublev);
		} else {/*27*/SensorCall();if (count == (uint16) -1) {
			SensorCall();double* doubleav;
			CopyField(tag, doubleav);
		;/*28*/}}
		SensorCall();break;
	case TIFF_IFD8:
		{ SensorCall();toff_t ifd8;
		  CopyField(tag, ifd8);
		}
		SensorCall();break;          default:
                SensorCall();TIFFError(TIFFFileName(in),
                          "Data type %d is not supported, tag %d skipped.",
                          tag, type);
	}
SensorCall();}

#undef CopyField4
#undef CopyField3
#undef CopyField2
#undef CopyField

static struct cpTag {
    uint16	tag;
    uint16	count;
    TIFFDataType type;
} tags[] = {
    { TIFFTAG_IMAGEWIDTH,		1, TIFF_LONG },
    { TIFFTAG_IMAGELENGTH,		1, TIFF_LONG },
    { TIFFTAG_BITSPERSAMPLE,		1, TIFF_SHORT },
    { TIFFTAG_COMPRESSION,		1, TIFF_SHORT },
    { TIFFTAG_FILLORDER,		1, TIFF_SHORT },
    { TIFFTAG_SAMPLESPERPIXEL,		1, TIFF_SHORT },
    { TIFFTAG_ROWSPERSTRIP,		1, TIFF_LONG },
    { TIFFTAG_PLANARCONFIG,		1, TIFF_SHORT },
    { TIFFTAG_GROUP3OPTIONS,		1, TIFF_LONG },
    { TIFFTAG_SUBFILETYPE,		1, TIFF_LONG },
    { TIFFTAG_PHOTOMETRIC,		1, TIFF_SHORT },
    { TIFFTAG_THRESHHOLDING,		1, TIFF_SHORT },
    { TIFFTAG_DOCUMENTNAME,		1, TIFF_ASCII },
    { TIFFTAG_IMAGEDESCRIPTION,		1, TIFF_ASCII },
    { TIFFTAG_MAKE,			1, TIFF_ASCII },
    { TIFFTAG_MODEL,			1, TIFF_ASCII },
    { TIFFTAG_ORIENTATION,		1, TIFF_SHORT },
    { TIFFTAG_MINSAMPLEVALUE,		1, TIFF_SHORT },
    { TIFFTAG_MAXSAMPLEVALUE,		1, TIFF_SHORT },
    { TIFFTAG_XRESOLUTION,		1, TIFF_RATIONAL },
    { TIFFTAG_YRESOLUTION,		1, TIFF_RATIONAL },
    { TIFFTAG_PAGENAME,			1, TIFF_ASCII },
    { TIFFTAG_XPOSITION,		1, TIFF_RATIONAL },
    { TIFFTAG_YPOSITION,		1, TIFF_RATIONAL },
    { TIFFTAG_GROUP4OPTIONS,		1, TIFF_LONG },
    { TIFFTAG_RESOLUTIONUNIT,		1, TIFF_SHORT },
    { TIFFTAG_PAGENUMBER,		2, TIFF_SHORT },
    { TIFFTAG_SOFTWARE,			1, TIFF_ASCII },
    { TIFFTAG_DATETIME,			1, TIFF_ASCII },
    { TIFFTAG_ARTIST,			1, TIFF_ASCII },
    { TIFFTAG_HOSTCOMPUTER,		1, TIFF_ASCII },
    { TIFFTAG_WHITEPOINT,		2, TIFF_RATIONAL },
    { TIFFTAG_PRIMARYCHROMATICITIES,	(uint16) -1,TIFF_RATIONAL },
    { TIFFTAG_HALFTONEHINTS,		2, TIFF_SHORT },
    // disable BADFAXLINES, CVE-2016-3632
    //{ TIFFTAG_BADFAXLINES,		1, TIFF_LONG },
    { TIFFTAG_CLEANFAXDATA,		1, TIFF_SHORT },
    { TIFFTAG_CONSECUTIVEBADFAXLINES,	1, TIFF_LONG },
    { TIFFTAG_INKSET,			1, TIFF_SHORT },
    // disable INKNAMES tag, http://bugzilla.maptools.org/show_bug.cgi?id=2484 (CVE-2014-8127)
    //{ TIFFTAG_INKNAMES,			1, TIFF_ASCII },
    { TIFFTAG_DOTRANGE,			2, TIFF_SHORT },
    { TIFFTAG_TARGETPRINTER,		1, TIFF_ASCII },
    { TIFFTAG_SAMPLEFORMAT,		1, TIFF_SHORT },
    { TIFFTAG_YCBCRCOEFFICIENTS,	(uint16) -1,TIFF_RATIONAL },
    { TIFFTAG_YCBCRSUBSAMPLING,		2, TIFF_SHORT },
    { TIFFTAG_YCBCRPOSITIONING,		1, TIFF_SHORT },
    { TIFFTAG_REFERENCEBLACKWHITE,	(uint16) -1,TIFF_RATIONAL },
    { TIFFTAG_EXTRASAMPLES,		(uint16) -1, TIFF_SHORT },
};
#define	NTAGS	(sizeof (tags) / sizeof (tags[0]))

static void
cpTags(TIFF* in, TIFF* out)
{
    SensorCall();struct cpTag *p;
    SensorCall();for (p = tags; p < &tags[NTAGS]; p++)
	{/*29*/SensorCall();cpTag(in, out, p->tag, p->count, p->type);/*30*/}
SensorCall();}
#undef NTAGS

static int
cpStrips(TIFF* in, TIFF* out)
{
    SensorCall();tsize_t bufsize  = TIFFStripSize(in);
    unsigned char *buf = (unsigned char *)_TIFFmalloc(bufsize);

    SensorCall();if (buf) {
	SensorCall();tstrip_t s, ns = TIFFNumberOfStrips(in);
	uint64 *bytecounts;

	TIFFGetField(in, TIFFTAG_STRIPBYTECOUNTS, &bytecounts);
	SensorCall();for (s = 0; s < ns; s++) {
	  SensorCall();if (bytecounts[s] > (uint64) bufsize) {
		SensorCall();buf = (unsigned char *)_TIFFrealloc(buf, (tmsize_t)bytecounts[s]);
		SensorCall();if (!buf)
		    {/*31*/SensorCall();goto bad;/*32*/}
		SensorCall();bufsize = (tmsize_t)bytecounts[s];
	    }
	    SensorCall();if (TIFFReadRawStrip(in, s, buf, (tmsize_t)bytecounts[s]) < 0 ||
		TIFFWriteRawStrip(out, s, buf, (tmsize_t)bytecounts[s]) < 0) {
		SensorCall();_TIFFfree(buf);
		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	    }
	}
	SensorCall();_TIFFfree(buf);
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
    }

bad:
	SensorCall();TIFFError(TIFFFileName(in),
		  "Can't allocate space for strip buffer.");
	{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

static int
cpTiles(TIFF* in, TIFF* out)
{
    SensorCall();tsize_t bufsize = TIFFTileSize(in);
    unsigned char *buf = (unsigned char *)_TIFFmalloc(bufsize);

    SensorCall();if (buf) {
	SensorCall();ttile_t t, nt = TIFFNumberOfTiles(in);
	uint64 *bytecounts;

	TIFFGetField(in, TIFFTAG_TILEBYTECOUNTS, &bytecounts);
	SensorCall();for (t = 0; t < nt; t++) {
	    SensorCall();if (bytecounts[t] > (uint64) bufsize) {
		SensorCall();buf = (unsigned char *)_TIFFrealloc(buf, (tmsize_t)bytecounts[t]);
		SensorCall();if (!buf)
		    {/*33*/SensorCall();goto bad;/*34*/}
		SensorCall();bufsize = (tmsize_t)bytecounts[t];
	    }
	    SensorCall();if (TIFFReadRawTile(in, t, buf, (tmsize_t)bytecounts[t]) < 0 ||
		TIFFWriteRawTile(out, t, buf, (tmsize_t)bytecounts[t]) < 0) {
		SensorCall();_TIFFfree(buf);
		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
	    }
	}
	SensorCall();_TIFFfree(buf);
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
    }

bad:
    SensorCall();TIFFError(TIFFFileName(in),
		  "Can't allocate space for tile buffer.");
	{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
}

static int
cpIFD(TIFF* in, TIFF* out)
{
    SensorCall();cpTags(in, out);
    SensorCall();if (TIFFIsTiled(in)) {
	SensorCall();if (!cpTiles(in, out))
	    {/*1*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*2*/}
    } else {
	SensorCall();if (!cpStrips(in, out))
	    {/*3*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*4*/}
    }
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

static	uint16	photometric;		/* current photometric of raster */
static	uint16	filterWidth;		/* filter width in pixels */
static	uint32	stepSrcWidth;		/* src image stepping width */
static	uint32	stepDstWidth;		/* dest stepping width */
static	uint8* src0;			/* horizontal bit stepping (start) */
static	uint8* src1;			/* horizontal bit stepping (middle) */
static	uint8* src2;			/* horizontal bit stepping (end) */
static	uint32* rowoff;			/* row offset for stepping */
static	uint8 cmap[256];		/* colormap indexes */
static	uint8 bits[256];		/* count of bits set */

static void
setupBitsTables()
{
    SensorCall();int i;
    SensorCall();for (i = 0; i < 256; i++) {
	SensorCall();int n = 0;
	SensorCall();if (i&0x01) {/*35*/SensorCall();n++;/*36*/}
	SensorCall();if (i&0x02) {/*37*/SensorCall();n++;/*38*/}
	SensorCall();if (i&0x04) {/*39*/SensorCall();n++;/*40*/}
	SensorCall();if (i&0x08) {/*41*/SensorCall();n++;/*42*/}
	SensorCall();if (i&0x10) {/*43*/SensorCall();n++;/*44*/}
	SensorCall();if (i&0x20) {/*45*/SensorCall();n++;/*46*/}
	SensorCall();if (i&0x40) {/*47*/SensorCall();n++;/*48*/}
	SensorCall();if (i&0x80) {/*49*/SensorCall();n++;/*50*/}
	SensorCall();bits[i] = n;
    }
SensorCall();}

static int clamp(float v, int low, int high)
    { {int  ReplaceReturn = (v < low ? low : v > high ? high : (int)v); SensorCall(); return ReplaceReturn;} }

#ifndef M_E
#define M_E		2.7182818284590452354
#endif

static void
expFill(float pct[], uint32 p, uint32 n)
{
    SensorCall();uint32 i;
    uint32 c = (p * n) / 100;
    SensorCall();for (i = 1; i < c; i++)
	{/*51*/SensorCall();pct[i] = (float) (1-exp(i/((double)(n-1)))/ M_E);/*52*/}
    SensorCall();for (; i < n; i++)
	{/*53*/SensorCall();pct[i] = 0.;/*54*/}
SensorCall();}

static void
setupCmap()
{
    SensorCall();float pct[256];			/* known to be large enough */
    uint32 i;
    pct[0] = 1;				/* force white */
    SensorCall();switch (contrast) {
    case EXP50: SensorCall();expFill(pct, 50, 256); SensorCall();break;
    case EXP60:	SensorCall();expFill(pct, 60, 256); SensorCall();break;
    case EXP70:	SensorCall();expFill(pct, 70, 256); SensorCall();break;
    case EXP80:	SensorCall();expFill(pct, 80, 256); SensorCall();break;
    case EXP90:	SensorCall();expFill(pct, 90, 256); SensorCall();break;
    case EXP:	SensorCall();expFill(pct, 100, 256); SensorCall();break;
    case LINEAR:
	SensorCall();for (i = 1; i < 256; i++)
	    {/*55*/SensorCall();pct[i] = 1-((float)i)/(256-1);/*56*/}
	SensorCall();break;
    }
    SensorCall();switch (photometric) {
    case PHOTOMETRIC_MINISWHITE:
	SensorCall();for (i = 0; i < 256; i++)
	    {/*57*/SensorCall();cmap[i] = clamp(255*pct[(256-1)-i], 0, 255);/*58*/}
	SensorCall();break;
    case PHOTOMETRIC_MINISBLACK:
	SensorCall();for (i = 0; i < 256; i++)
	    {/*59*/SensorCall();cmap[i] = clamp(255*pct[i], 0, 255);/*60*/}
	SensorCall();break;
    }
SensorCall();}

static void
initScale()
{
    SensorCall();src0 = (uint8*) _TIFFmalloc(sizeof (uint8) * tnw);
    src1 = (uint8*) _TIFFmalloc(sizeof (uint8) * tnw);
    src2 = (uint8*) _TIFFmalloc(sizeof (uint8) * tnw);
    rowoff = (uint32*) _TIFFmalloc(sizeof (uint32) * tnw);
    filterWidth = 0;
    stepDstWidth = stepSrcWidth = 0;
    setupBitsTables();
SensorCall();}

/*
 * Calculate the horizontal accumulation parameteres
 * according to the widths of the src and dst images.
 */
static void
setupStepTables(uint32 sw)
{
    SensorCall();if (stepSrcWidth != sw || stepDstWidth != tnw) {
	SensorCall();int step = sw;
	int limit = tnw;
	int err = 0;
	uint32 sx = 0;
	uint32 x;
	int fw;
	uint8 b;
	SensorCall();for (x = 0; x < tnw; x++) {
	    SensorCall();uint32 sx0 = sx;
	    err += step;
	    SensorCall();while (err >= limit) {
		SensorCall();err -= limit;
		sx++;
	    }
	    SensorCall();rowoff[x] = sx0 >> 3;
	    fw = sx - sx0;		/* width */
	    b = (fw < 8) ? 0xff<<(8-fw) : 0xff;
	    src0[x] = b >> (sx0&7);
	    fw -= 8 - (sx0&7);
	    SensorCall();if (fw < 0)
		{/*61*/SensorCall();fw = 0;/*62*/}
	    SensorCall();src1[x] = fw >> 3;
	    fw -= (fw>>3)<<3;
	    src2[x] = 0xff << (8-fw);
	}
	SensorCall();stepSrcWidth = sw;
	stepDstWidth = tnw;
    }
SensorCall();}

static void
setrow(uint8* row, uint32 nrows, const uint8* rows[])
{
    SensorCall();uint32 x;
    uint32 area = nrows * filterWidth;
    SensorCall();for (x = 0; x < tnw; x++) {
	SensorCall();uint32 mask0 = src0[x];
	uint32 fw = src1[x];
	uint32 mask1 = src1[x];
	uint32 off = rowoff[x];
	uint32 acc = 0;
	uint32 y, i;
	SensorCall();for (y = 0; y < nrows; y++) {
	    SensorCall();const uint8* src = rows[y] + off;
	    acc += bits[*src++ & mask0];
	    SensorCall();switch (fw) {
	    default:
		SensorCall();for (i = fw; i > 8; i--)
		    {/*63*/SensorCall();acc += bits[*src++];/*64*/}
		/* fall thru... */
	    case 8: SensorCall();acc += bits[*src++];
	    case 7: SensorCall();acc += bits[*src++];
	    case 6: SensorCall();acc += bits[*src++];
	    case 5: SensorCall();acc += bits[*src++];
	    case 4: SensorCall();acc += bits[*src++];
	    case 3: SensorCall();acc += bits[*src++];
	    case 2: SensorCall();acc += bits[*src++];
	    case 1: SensorCall();acc += bits[*src++];
	    case 0: SensorCall();break;
	    }
	    SensorCall();acc += bits[*src & mask1];
	}
	SensorCall();*row++ = cmap[(255*acc)/area];
    }
SensorCall();}

/*
 * Install the specified image.  The
 * image is resized to fit the display page using
 * a box filter.  The resultant pixels are mapped
 * with a user-selectable contrast curve.
 */
static void
setImage1(const uint8* br, uint32 rw, uint32 rh)
{
    SensorCall();int step = rh;
    int limit = tnh;
    int err = 0;
    int bpr = TIFFhowmany8(rw);
    int sy = 0;
    uint8* row = thumbnail;
    uint32 dy;
    SensorCall();for (dy = 0; dy < tnh; dy++) {
	SensorCall();const uint8* rows[256];
	uint32 nrows = 1;
	fprintf(stderr, "bpr=%d, sy=%d, bpr*sy=%d\n", bpr, sy, bpr*sy);
	rows[0] = br + bpr*sy;
	err += step;
	SensorCall();while (err >= limit) {
	    SensorCall();err -= limit;
	    sy++;
	    SensorCall();if (err >= limit)
		{/*65*/SensorCall();rows[nrows++] = br + bpr*sy;/*66*/}
	}
	SensorCall();setrow(row, nrows, rows);
	row += tnw;
    }
SensorCall();}

static void
setImage(const uint8* br, uint32 rw, uint32 rh)
{
    SensorCall();filterWidth = (uint16) ceil((double) rw / (double) tnw);
    setupStepTables(rw);
    setImage1(br, rw, rh);
SensorCall();}

static int
generateThumbnail(TIFF* in, TIFF* out)
{
    SensorCall();unsigned char* raster;
    unsigned char* rp;
    uint32 sw, sh, rps;
    uint16 bps, spp;
    tsize_t rowsize, rastersize;
    tstrip_t s, ns = TIFFNumberOfStrips(in);
    toff_t diroff[1];

    TIFFGetField(in, TIFFTAG_IMAGEWIDTH, &sw);
    TIFFGetField(in, TIFFTAG_IMAGELENGTH, &sh);
    TIFFGetFieldDefaulted(in, TIFFTAG_BITSPERSAMPLE, &bps);
    TIFFGetFieldDefaulted(in, TIFFTAG_SAMPLESPERPIXEL, &spp);
    TIFFGetFieldDefaulted(in, TIFFTAG_ROWSPERSTRIP, &rps);
    SensorCall();if (spp != 1 || bps != 1)
	{/*5*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*6*/}
    SensorCall();rowsize = TIFFScanlineSize(in);
    rastersize = sh * rowsize;
    fprintf(stderr, "rastersize=%u\n", (unsigned int)rastersize);
    raster = (unsigned char*)_TIFFmalloc(rastersize + 3);
    SensorCall();if (!raster) {
	    SensorCall();TIFFError(TIFFFileName(in),
		      "Can't allocate space for raster buffer.");
	    {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
    }
    SensorCall();rp = raster;
    SensorCall();for (s = 0; s < ns; s++) {
	SensorCall();(void) TIFFReadEncodedStrip(in, s, rp, -1);
	rp += rps * rowsize;
    }
    SensorCall();TIFFGetField(in, TIFFTAG_PHOTOMETRIC, &photometric);
    setupCmap();
    setImage(raster, sw, sh);
    _TIFFfree(raster);

    TIFFSetField(out, TIFFTAG_SUBFILETYPE, FILETYPE_REDUCEDIMAGE);
    TIFFSetField(out, TIFFTAG_IMAGEWIDTH, (uint32) tnw);
    TIFFSetField(out, TIFFTAG_IMAGELENGTH, (uint32) tnh);
    TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, (uint16) 8);
    TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, (uint16) 1);
    TIFFSetField(out, TIFFTAG_COMPRESSION, COMPRESSION_PACKBITS);
    TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISWHITE);
    TIFFSetField(out, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    TIFFSetField(out, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
    cpTag(in, out, TIFFTAG_SOFTWARE,		(uint16) -1, TIFF_ASCII);
    cpTag(in, out, TIFFTAG_IMAGEDESCRIPTION,	(uint16) -1, TIFF_ASCII);
    cpTag(in, out, TIFFTAG_DATETIME,		(uint16) -1, TIFF_ASCII);
    cpTag(in, out, TIFFTAG_HOSTCOMPUTER,	(uint16) -1, TIFF_ASCII);
    diroff[0] = 0UL;
    TIFFSetField(out, TIFFTAG_SUBIFD, 1, diroff);
    {int  ReplaceReturn = (TIFFWriteEncodedStrip(out, 0, thumbnail, tnw*tnh) != -1 &&
            TIFFWriteDirectory(out) != -1); SensorCall(); return ReplaceReturn;}
}

char* stuff[] = {
"usage: thumbnail [options] input.tif output.tif",
"where options are:",
" -h #		specify thumbnail image height (default is 274)",
" -w #		specify thumbnail image width (default is 216)",
"",
" -c linear	use linear contrast curve",
" -c exp50	use 50% exponential contrast curve",
" -c exp60	use 60% exponential contrast curve",
" -c exp70	use 70% exponential contrast curve",
" -c exp80	use 80% exponential contrast curve",
" -c exp90	use 90% exponential contrast curve",
" -c exp		use pure exponential contrast curve",
NULL
};

static void
usage(void)
{
	SensorCall();char buf[BUFSIZ];
	int i;

	setbuf(stderr, buf);
        fprintf(stderr, "%s\n\n", TIFFGetVersion());
	SensorCall();for (i = 0; stuff[i] != NULL; i++)
		{/*7*/SensorCall();fprintf(stderr, "%s\n", stuff[i]);/*8*/}
	SensorCall();exit(-1);
SensorCall();}

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
