/* $Id: pal2rgb.c,v 1.13 2010-07-02 12:02:56 dron Exp $ */

/*
 * Copyright (c) 1988-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

#include "tif_config.h"
#include "/var/tmp/sensor.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifdef NEED_LIBPORT
# include "libport.h"
#endif

#include "tiffio.h"

#define	streq(a,b)	(strcmp(a,b) == 0)
#define	strneq(a,b,n)	(strncmp(a,b,n) == 0)

static	void usage(void);
static	void cpTags(TIFF* in, TIFF* out);

static int
checkcmap(int n, uint16* r, uint16* g, uint16* b)
{
	SensorCall();while (n-- > 0)
	    {/*5*/SensorCall();if (*r++ >= 256 || *g++ >= 256 || *b++ >= 256)
		{/*7*/{int  ReplaceReturn = (16); SensorCall(); return ReplaceReturn;}/*8*/}/*6*/}
	SensorCall();fprintf(stderr, "Warning, assuming 8-bit colormap.\n");
	{int  ReplaceReturn = (8); SensorCall(); return ReplaceReturn;}
}

#define	CopyField(tag, v) \
    if (TIFFGetField(in, tag, &v)) TIFFSetField(out, tag, v)
#define	CopyField3(tag, v1, v2, v3) \
    if (TIFFGetField(in, tag, &v1, &v2, &v3)) TIFFSetField(out, tag, v1, v2, v3)

static	uint16 compression = (uint16) -1;
static	uint16 predictor = 0;
static	int quality = 75;	/* JPEG quality */
static	int jpegcolormode = JPEGCOLORMODE_RGB;
static	int processCompressOptions(char*);

int
main(int argc, char* argv[])
{
	SensorCall();uint16 bitspersample, shortv;
	uint32 imagewidth, imagelength;
	uint16 config = PLANARCONFIG_CONTIG;
	uint32 rowsperstrip = (uint32) -1;
	uint16 photometric = PHOTOMETRIC_RGB;
	uint16 *rmap, *gmap, *bmap;
	uint32 row;
	int cmap = -1;
	TIFF *in, *out;
	int c;
	extern int optind;
	extern char* optarg;

	SensorCall();while ((c = getopt(argc, argv, "C:c:p:r:")) != -1)
		{/*29*/SensorCall();switch (c) {
		case 'C':		/* force colormap interpretation */
			SensorCall();cmap = atoi(optarg);
			SensorCall();break;
		case 'c':		/* compression scheme */
			SensorCall();if (!processCompressOptions(optarg))
				{/*31*/SensorCall();usage();/*32*/}
			SensorCall();break;
		case 'p':		/* planar configuration */
			SensorCall();if (streq(optarg, "separate"))
				config = PLANARCONFIG_SEPARATE;
			else {/*33*/SensorCall();if (streq(optarg, "contig"))
				config = PLANARCONFIG_CONTIG;
			else
				{/*35*/SensorCall();usage();/*36*/}/*34*/}
			SensorCall();break;
		case 'r':		/* rows/strip */
			SensorCall();rowsperstrip = atoi(optarg);
			SensorCall();break;
		case '?':
			SensorCall();usage();
			/*NOTREACHED*/
		;/*30*/}}
	SensorCall();if (argc - optind != 2)
		{/*37*/SensorCall();usage();/*38*/}
	SensorCall();in = TIFFOpen(argv[optind], "r");
	SensorCall();if (in == NULL)
		{/*39*/{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}/*40*/}
	SensorCall();if (!TIFFGetField(in, TIFFTAG_PHOTOMETRIC, &shortv) ||
	    shortv != PHOTOMETRIC_PALETTE) {
		SensorCall();fprintf(stderr, "%s: Expecting a palette image.\n",
		    argv[optind]);
		{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();if (!TIFFGetField(in, TIFFTAG_COLORMAP, &rmap, &gmap, &bmap)) {
		SensorCall();fprintf(stderr,
		    "%s: No colormap (not a valid palette image).\n",
		    argv[optind]);
		{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();bitspersample = 0;
	TIFFGetField(in, TIFFTAG_BITSPERSAMPLE, &bitspersample);
	SensorCall();if (bitspersample != 8) {
		SensorCall();fprintf(stderr, "%s: Sorry, can only handle 8-bit images.\n",
		    argv[optind]);
		{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();out = TIFFOpen(argv[optind+1], "w");
	SensorCall();if (out == NULL)
		{/*41*/{int  ReplaceReturn = (-2); SensorCall(); return ReplaceReturn;}/*42*/}
	SensorCall();cpTags(in, out);
	TIFFGetField(in, TIFFTAG_IMAGEWIDTH, &imagewidth);
	TIFFGetField(in, TIFFTAG_IMAGELENGTH, &imagelength);
	SensorCall();if (compression != (uint16)-1)
		{/*43*/SensorCall();TIFFSetField(out, TIFFTAG_COMPRESSION, compression);/*44*/}
	else
		{/*45*/SensorCall();TIFFGetField(in, TIFFTAG_COMPRESSION, &compression);/*46*/}
	SensorCall();switch (compression) {
	case COMPRESSION_JPEG:
		SensorCall();if (jpegcolormode == JPEGCOLORMODE_RGB)
			photometric = PHOTOMETRIC_YCBCR;
		else
			photometric = PHOTOMETRIC_RGB;
		SensorCall();TIFFSetField(out, TIFFTAG_JPEGQUALITY, quality);
		TIFFSetField(out, TIFFTAG_JPEGCOLORMODE, jpegcolormode);
		SensorCall();break;
	case COMPRESSION_LZW:
	case COMPRESSION_DEFLATE:
		SensorCall();if (predictor != 0)
			{/*47*/SensorCall();TIFFSetField(out, TIFFTAG_PREDICTOR, predictor);/*48*/}
		SensorCall();break;
	}
	SensorCall();TIFFSetField(out, TIFFTAG_PHOTOMETRIC, photometric);
	TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, 3);
	TIFFSetField(out, TIFFTAG_PLANARCONFIG, config);
	TIFFSetField(out, TIFFTAG_ROWSPERSTRIP,
	    rowsperstrip = TIFFDefaultStripSize(out, rowsperstrip));
	(void) TIFFGetField(in, TIFFTAG_PLANARCONFIG, &shortv);
	SensorCall();if (cmap == -1)
		{/*49*/SensorCall();cmap = checkcmap(1<<bitspersample, rmap, gmap, bmap);/*50*/}
	SensorCall();if (cmap == 16) {
		/*
		 * Convert 16-bit colormap to 8-bit.
		 */
		SensorCall();int i;

		SensorCall();for (i = (1<<bitspersample)-1; i >= 0; i--) {
#define	CVT(x)		(((x) * 255) / ((1L<<16)-1))
			SensorCall();rmap[i] = CVT(rmap[i]);
			gmap[i] = CVT(gmap[i]);
			bmap[i] = CVT(bmap[i]);
		}
	}
	{ SensorCall();unsigned char *ibuf, *obuf;
	  register unsigned char* pp;
	  register uint32 x;
	  ibuf = (unsigned char*)_TIFFmalloc(TIFFScanlineSize(in));
	  obuf = (unsigned char*)_TIFFmalloc(TIFFScanlineSize(out));
	  SensorCall();switch (config) {
	  case PLANARCONFIG_CONTIG:
		SensorCall();for (row = 0; row < imagelength; row++) {
			SensorCall();if (!TIFFReadScanline(in, ibuf, row, 0))
				{/*51*/SensorCall();goto done;/*52*/}
			SensorCall();pp = obuf;
			SensorCall();for (x = 0; x < imagewidth; x++) {
				SensorCall();*pp++ = (unsigned char) rmap[ibuf[x]];
				*pp++ = (unsigned char) gmap[ibuf[x]];
				*pp++ = (unsigned char) bmap[ibuf[x]];
			}
			SensorCall();if (!TIFFWriteScanline(out, obuf, row, 0))
				{/*53*/SensorCall();goto done;/*54*/}
		}
		SensorCall();break;
	  case PLANARCONFIG_SEPARATE:
		SensorCall();for (row = 0; row < imagelength; row++) {
			SensorCall();if (!TIFFReadScanline(in, ibuf, row, 0))
				{/*55*/SensorCall();goto done;/*56*/}
			SensorCall();for (pp = obuf, x = 0; x < imagewidth; x++)
				{/*57*/SensorCall();*pp++ = (unsigned char) rmap[ibuf[x]];/*58*/}
			SensorCall();if (!TIFFWriteScanline(out, obuf, row, 0))
				{/*59*/SensorCall();goto done;/*60*/}
			SensorCall();for (pp = obuf, x = 0; x < imagewidth; x++)
				{/*61*/SensorCall();*pp++ = (unsigned char) gmap[ibuf[x]];/*62*/}
			SensorCall();if (!TIFFWriteScanline(out, obuf, row, 0))
				{/*63*/SensorCall();goto done;/*64*/}
			SensorCall();for (pp = obuf, x = 0; x < imagewidth; x++)
				{/*65*/SensorCall();*pp++ = (unsigned char) bmap[ibuf[x]];/*66*/}
			SensorCall();if (!TIFFWriteScanline(out, obuf, row, 0))
				{/*67*/SensorCall();goto done;/*68*/}
		}
		SensorCall();break;
	  }
	  SensorCall();_TIFFfree(ibuf);
	  _TIFFfree(obuf);
	}
done:
	(void) TIFFClose(in);
	(void) TIFFClose(out);
	{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
}

static int
processCompressOptions(char* opt)
{
	SensorCall();if (streq(opt, "none"))
		compression = COMPRESSION_NONE;
	else {/*9*/SensorCall();if (streq(opt, "packbits"))
		compression = COMPRESSION_PACKBITS;
	else {/*11*/SensorCall();if (strneq(opt, "jpeg", 4)) {
		SensorCall();char* cp = strchr(opt, ':');

                compression = COMPRESSION_JPEG;
                SensorCall();while( cp )
                {
                    SensorCall();if (isdigit((int)cp[1]))
			{/*13*/SensorCall();quality = atoi(cp+1);/*14*/}
                    else {/*15*/SensorCall();if (cp[1] == 'r' )
			jpegcolormode = JPEGCOLORMODE_RAW;
                    else
                        {/*17*/SensorCall();usage();/*18*/}/*16*/}

                    SensorCall();cp = strchr(cp+1,':');
                }
	} else {/*19*/SensorCall();if (strneq(opt, "lzw", 3)) {
		SensorCall();char* cp = strchr(opt, ':');
		SensorCall();if (cp)
			{/*21*/SensorCall();predictor = atoi(cp+1);/*22*/}
		SensorCall();compression = COMPRESSION_LZW;
	} else {/*23*/SensorCall();if (strneq(opt, "zip", 3)) {
		SensorCall();char* cp = strchr(opt, ':');
		SensorCall();if (cp)
			{/*25*/SensorCall();predictor = atoi(cp+1);/*26*/}
		SensorCall();compression = COMPRESSION_DEFLATE;
	} else
		{/*27*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*28*/}/*24*/}/*20*/}/*12*/}/*10*/}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

#define	CopyField(tag, v) \
    if (TIFFGetField(in, tag, &v)) TIFFSetField(out, tag, v)
#define	CopyField2(tag, v1, v2) \
    if (TIFFGetField(in, tag, &v1, &v2)) TIFFSetField(out, tag, v1, v2)
#define	CopyField3(tag, v1, v2, v3) \
    if (TIFFGetField(in, tag, &v1, &v2, &v3)) TIFFSetField(out, tag, v1, v2, v3)
#define	CopyField4(tag, v1, v2, v3, v4) \
    if (TIFFGetField(in, tag, &v1, &v2, &v3, &v4)) TIFFSetField(out, tag, v1, v2, v3, v4)

static void
cpTag(TIFF* in, TIFF* out, uint16 tag, uint16 count, TIFFDataType type)
{
	SensorCall();switch (type) {
	case TIFF_SHORT:
		SensorCall();if (count == 1) {
			SensorCall();uint16 shortv;
			CopyField(tag, shortv);
		} else {/*69*/SensorCall();if (count == 2) {
			SensorCall();uint16 shortv1, shortv2;
			CopyField2(tag, shortv1, shortv2);
		} else {/*71*/SensorCall();if (count == 4) {
			SensorCall();uint16 *tr, *tg, *tb, *ta;
			CopyField4(tag, tr, tg, tb, ta);
		} else {/*73*/SensorCall();if (count == (uint16) -1) {
			SensorCall();uint16 shortv1;
			uint16* shortav;
			CopyField2(tag, shortv1, shortav);
		;/*74*/}/*72*/}/*70*/}}
		SensorCall();break;
	case TIFF_LONG:
		{ SensorCall();uint32 longv;
		  CopyField(tag, longv);
		}
		SensorCall();break;
	case TIFF_RATIONAL:
		SensorCall();if (count == 1) {
			SensorCall();float floatv;
			CopyField(tag, floatv);
		} else {/*75*/SensorCall();if (count == (uint16) -1) {
			SensorCall();float* floatav;
			CopyField(tag, floatav);
		;/*76*/}}
		SensorCall();break;
	case TIFF_ASCII:
		{ SensorCall();char* stringv;
		  CopyField(tag, stringv);
		}
		SensorCall();break;
	case TIFF_DOUBLE:
		SensorCall();if (count == 1) {
			SensorCall();double doublev;
			CopyField(tag, doublev);
		} else {/*77*/SensorCall();if (count == (uint16) -1) {
			SensorCall();double* doubleav;
			CopyField(tag, doubleav);
		;/*78*/}}
		SensorCall();break;
          default:
                SensorCall();TIFFError(TIFFFileName(in),
                          "Data type %d is not supported, tag %d skipped.",
                          tag, type);
	}
SensorCall();}

#undef CopyField4
#undef CopyField3
#undef CopyField2
#undef CopyField

static struct cpTag {
    uint16	tag;
    uint16	count;
    TIFFDataType type;
} tags[] = {
    { TIFFTAG_IMAGEWIDTH,		1, TIFF_LONG },
    { TIFFTAG_IMAGELENGTH,		1, TIFF_LONG },
    { TIFFTAG_BITSPERSAMPLE,		1, TIFF_SHORT },
    { TIFFTAG_COMPRESSION,		1, TIFF_SHORT },
    { TIFFTAG_FILLORDER,		1, TIFF_SHORT },
    { TIFFTAG_ROWSPERSTRIP,		1, TIFF_LONG },
    { TIFFTAG_GROUP3OPTIONS,		1, TIFF_LONG },
    { TIFFTAG_SUBFILETYPE,		1, TIFF_LONG },
    { TIFFTAG_THRESHHOLDING,		1, TIFF_SHORT },
    { TIFFTAG_DOCUMENTNAME,		1, TIFF_ASCII },
    { TIFFTAG_IMAGEDESCRIPTION,		1, TIFF_ASCII },
    { TIFFTAG_MAKE,			1, TIFF_ASCII },
    { TIFFTAG_MODEL,			1, TIFF_ASCII },
    { TIFFTAG_ORIENTATION,		1, TIFF_SHORT },
    { TIFFTAG_MINSAMPLEVALUE,		1, TIFF_SHORT },
    { TIFFTAG_MAXSAMPLEVALUE,		1, TIFF_SHORT },
    { TIFFTAG_XRESOLUTION,		1, TIFF_RATIONAL },
    { TIFFTAG_YRESOLUTION,		1, TIFF_RATIONAL },
    { TIFFTAG_PAGENAME,			1, TIFF_ASCII },
    { TIFFTAG_XPOSITION,		1, TIFF_RATIONAL },
    { TIFFTAG_YPOSITION,		1, TIFF_RATIONAL },
    { TIFFTAG_GROUP4OPTIONS,		1, TIFF_LONG },
    { TIFFTAG_RESOLUTIONUNIT,		1, TIFF_SHORT },
    { TIFFTAG_PAGENUMBER,		2, TIFF_SHORT },
    { TIFFTAG_SOFTWARE,			1, TIFF_ASCII },
    { TIFFTAG_DATETIME,			1, TIFF_ASCII },
    { TIFFTAG_ARTIST,			1, TIFF_ASCII },
    { TIFFTAG_HOSTCOMPUTER,		1, TIFF_ASCII },
    { TIFFTAG_WHITEPOINT,		2, TIFF_RATIONAL },
    { TIFFTAG_PRIMARYCHROMATICITIES,	(uint16) -1,TIFF_RATIONAL },
    { TIFFTAG_HALFTONEHINTS,		2, TIFF_SHORT },
    { TIFFTAG_BADFAXLINES,		1, TIFF_LONG },
    { TIFFTAG_CLEANFAXDATA,		1, TIFF_SHORT },
    { TIFFTAG_CONSECUTIVEBADFAXLINES,	1, TIFF_LONG },
    { TIFFTAG_INKSET,			1, TIFF_SHORT },
    // disable INKNAMES tag, http://bugzilla.maptools.org/show_bug.cgi?id=2484 (CVE-2014-8127)
    //{ TIFFTAG_INKNAMES,			1, TIFF_ASCII },
    { TIFFTAG_DOTRANGE,			2, TIFF_SHORT },
    { TIFFTAG_TARGETPRINTER,		1, TIFF_ASCII },
    { TIFFTAG_SAMPLEFORMAT,		1, TIFF_SHORT },
    { TIFFTAG_YCBCRCOEFFICIENTS,	(uint16) -1,TIFF_RATIONAL },
    { TIFFTAG_YCBCRSUBSAMPLING,		2, TIFF_SHORT },
    { TIFFTAG_YCBCRPOSITIONING,		1, TIFF_SHORT },
    { TIFFTAG_REFERENCEBLACKWHITE,	(uint16) -1,TIFF_RATIONAL },
};
#define	NTAGS	(sizeof (tags) / sizeof (tags[0]))

static void
cpTags(TIFF* in, TIFF* out)
{
    SensorCall();struct cpTag *p;
    SensorCall();for (p = tags; p < &tags[NTAGS]; p++)
	{/*3*/SensorCall();cpTag(in, out, p->tag, p->count, p->type);/*4*/}
SensorCall();}
#undef NTAGS

char* stuff[] = {
"usage: pal2rgb [options] input.tif output.tif",
"where options are:",
" -p contig	pack samples contiguously (e.g. RGBRGB...)",
" -p separate	store samples separately (e.g. RRR...GGG...BBB...)",
" -r #		make each strip have no more than # rows",
" -C 8		assume 8-bit colormap values (instead of 16-bit)",
" -C 16		assume 16-bit colormap values",
"",
" -c lzw[:opts]	compress output with Lempel-Ziv & Welch encoding",
" -c zip[:opts]	compress output with deflate encoding",
" -c packbits	compress output with packbits encoding",
" -c none	use no compression algorithm on output",
"",
"LZW and deflate options:",
" #		set predictor value",
"For example, -c lzw:2 to get LZW-encoded data with horizontal differencing",
NULL
};

static void
usage(void)
{
	SensorCall();char buf[BUFSIZ];
	int i;

	setbuf(stderr, buf);
        fprintf(stderr, "%s\n\n", TIFFGetVersion());
	SensorCall();for (i = 0; stuff[i] != NULL; i++)
		{/*1*/SensorCall();fprintf(stderr, "%s\n", stuff[i]);/*2*/}
	SensorCall();exit(-1);
SensorCall();}

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
