/* $Id: tiff2ps.c,v 1.49 2011-05-31 17:10:18 bfriesen Exp $ */

/*
 * Copyright (c) 1988-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

#include "tif_config.h"
#include "/var/tmp/sensor.h"

#include <stdio.h>
#include <stdlib.h>			/* for atof */
#include <math.h>
#include <time.h>
#include <string.h>

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifdef NEED_LIBPORT
# include "libport.h"
#endif

#include "tiffio.h"

/*
 * Revision history
 *
 * 2010-Sep-17
 *    Richard Nolde: Reinstate code from Feb 2009 that never got
 *    accepted into CVS with major modifications to handle -H and -W
 *    options. Replaced original PlaceImage function with several
 *    new functions that make support for multiple output pages
 *    from a single image easier to understand. Added additional
 *    warning messages for incompatible command line options.
 *    Add new command line options to specify PageOrientation
 *    Document Structuring Comment for landscape or portrait
 *    and code to determine the values from ouput width and height
 *    if not specified on the command line.
 *    Add new command line option to specify document creator
 *    as an alterntive to the string "tiff2ps" following model
 *    of patch submitted by Thomas Jarosch for specifiying a
 *    document title which is also supported now.
 *
 * 2009-Feb-11
 *    Richard Nolde: Added support for rotations of 90, 180, 270
 *    and auto using -r <90|180|270|auto>.  Auto picks the best
 *    fit for the image on the specified paper size (eg portrait
 *    or landscape) if -h or -w is specified. Rotation is in
 *    degrees counterclockwise since that is how Postscript does
 *    it. The auto opption rotates the image 90 degrees ccw to
 *    produce landscape if that is a better fit than portait.
 *
 *    Cleaned up code in TIFF2PS and broke into smaller functions
 *    to simplify rotations.
 *
 *    Identified incompatible options and returned errors, eg
 *    -i for imagemask operator is only available for Level2 or
 *    Level3 Postscript in the current implmentation since there
 *    is a difference in the way the operands are called for Level1
 *    and there is no function to provide the Level1 version.
 *    -H was not handled properly if -h and/or -w were specified.
 *    It should only clip the masked images if the scaled image
 *    exceeds the maxPageHeight specified with -H.
 *  
 *    New design allows for all of the following combinations:
 *    Conversion of TIFF to Postscript with optional rotations
 *    of 90, 180, 270, or auto degrees counterclockwise
 *    Conversion of TIFF to Postscript with entire image scaled
 *    to maximum of values spedified with -h or -w while
 *    maintaining aspect ratio. Same rotations apply.
 *    Conversion of TIFF to Postscript with clipping of output
 *    viewport to height specified with -H, producing multiple
 *    pages at this height and original width as needed.
 *    Same rotations apply.
 *    Conversion of TIFF to Postscript with image scaled to 
 *    maximum specified by -h and -w and the resulting scaled
 *    image is presented in an output viewport clipped by -H height.
 *    The same rotations apply.
 *
 *    Added maxPageWidth option using -W flag. MaxPageHeight and
 *    MaxPageWidth are mutually exclusive since the aspect ratio
 *    cannot be maintained if you set both.
 *    Rewrote PlaceImage to allow maxPageHeight and maxPageWidth
 *    options to work with values smaller or larger than the
 *    physical paper size and still preserve the aspect ratio.
 *    This is accomplished by creating multiple pages across
 *    as well as down if need be.
 *
 * 2001-Mar-21
 *    I (Bruce A. Mallett) added this revision history comment ;)
 *
 *    Fixed PS_Lvl2page() code which outputs non-ASCII85 raw
 *    data.  Moved test for when to output a line break to
 *    *after* the output of a character.  This just serves
 *    to fix an eye-nuisance where the first line of raw
 *    data was one character shorter than subsequent lines.
 *
 *    Added an experimental ASCII85 encoder which can be used
 *    only when there is a single buffer of bytes to be encoded.
 *    This version is much faster at encoding a straight-line
 *    buffer of data because it can avoid a lot of the loop
 *    overhead of the byte-by-byte version.  To use this version
 *    you need to define EXP_ASCII85ENCODER (experimental ...).
 *
 *    Added bug fix given by Michael Schmidt to PS_Lvl2page()
 *    in which an end-of-data marker ('>') was not being output
 *    when producing non-ASCII85 encoded PostScript Level 2
 *    data.
 *
 *    Fixed PS_Lvl2colorspace() so that it no longer assumes that
 *    a TIFF having more than 2 planes is a CMYK.  This routine
 *    no longer looks at the samples per pixel but instead looks
 *    at the "photometric" value.  This change allows support of
 *    CMYK TIFFs.
 *
 *    Modified the PostScript L2 imaging loop so as to test if
 *    the input stream is still open before attempting to do a
 *    flushfile on it.  This was done because some RIPs close
 *    the stream after doing the image operation.
 *
 *    Got rid of the realloc() being done inside a loop in the
 *    PSRawDataBW() routine.  The code now walks through the
 *    byte-size array outside the loop to determine the largest
 *    size memory block that will be needed.
 *
 *    Added "-m" switch to ask tiff2ps to, where possible, use the
 *    "imagemask" operator instead of the "image" operator.
 *
 *    Added the "-i #" switch to allow interpolation to be disabled.
 *
 *    Unrolled a loop or two to improve performance.
 */

/*
 * Define EXP_ASCII85ENCODER if you want to use an experimental
 * version of the ASCII85 encoding routine.  The advantage of
 * using this routine is that tiff2ps will convert to ASCII85
 * encoding at between 3 and 4 times the speed as compared to
 * using the old (non-experimental) encoder.  The disadvantage
 * is that you will be using a new (and unproven) encoding
 * routine.  So user beware, you have been warned!
 */

#define	EXP_ASCII85ENCODER

/*
 * NB: this code assumes uint32 works with printf's %l[ud].
 */
#ifndef TRUE
#define	TRUE	1
#define	FALSE	0
#endif

int	ascii85 = FALSE;		/* use ASCII85 encoding */
int	interpolate = TRUE;		/* interpolate level2 image */
int	level2 = FALSE;			/* generate PostScript level 2 */
int	level3 = FALSE;			/* generate PostScript level 3 */
int	printAll = FALSE;		/* print all images in file */
int	generateEPSF = TRUE;		/* generate Encapsulated PostScript */
int	PSduplex = FALSE;		/* enable duplex printing */
int	PStumble = FALSE;		/* enable top edge binding */
int	PSavoiddeadzone = TRUE;		/* enable avoiding printer deadzone */
double	maxPageHeight = 0;		/* maximum height to select from image and print per page */
double	maxPageWidth  = 0;		/* maximum width  to select from image and print per page */
double	splitOverlap = 0;		/* amount for split pages to overlag */
int	rotation = 0;                   /* optional value for rotation angle */
int     auto_rotate = 0;                /* rotate image for best fit on the page */
char	*filename = NULL;		/* input filename */
char    *title = NULL;                  /* optional document title string */
char    *creator = NULL;                /* optional document creator string */
char    pageOrientation[12];            /* set optional PageOrientation DSC to Landscape or Portrait */
int	useImagemask = FALSE;		/* Use imagemask instead of image operator */
uint16	res_unit = 0;			/* Resolution units: 2 - inches, 3 - cm */

/*
 * ASCII85 Encoding Support.
 */
unsigned char ascii85buf[10];
int	ascii85count;
int	ascii85breaklen;

int	TIFF2PS(FILE*, TIFF*, double, double, double, double, int);
void	PSpage(FILE*, TIFF*, uint32, uint32);
void	PSColorContigPreamble(FILE*, uint32, uint32, int);
void	PSColorSeparatePreamble(FILE*, uint32, uint32, int);
void	PSDataColorContig(FILE*, TIFF*, uint32, uint32, int);
void	PSDataColorSeparate(FILE*, TIFF*, uint32, uint32, int);
void	PSDataPalette(FILE*, TIFF*, uint32, uint32);
void	PSDataBW(FILE*, TIFF*, uint32, uint32);
void	PSRawDataBW(FILE*, TIFF*, uint32, uint32);
void	Ascii85Init(void);
void	Ascii85Put(unsigned char code, FILE* fd);
void	Ascii85Flush(FILE* fd);
void    PSHead(FILE*, double, double, double, double);
void	PSTail(FILE*, int);
int     psStart(FILE *, int, int, int *, double *, double, double, double,
                double, double, double, double, double, double, double);
int     psPageSize(FILE *, int, double, double, double, double, double, double);
int     psRotateImage(FILE *, int, double, double, double, double);
int     psMaskImage(FILE *, TIFF *, int, int, int *, double, double,
		    double, double, double, double, double, double, double);
int     psScaleImage(FILE *, double, int, int, double, double, double, double,
                     double, double);
int     get_viewport (double, double, double, double, double *, double *, int);
int     exportMaskedImage(FILE *, double, double, double, double, int, int,
			  double, double, double, int, int);

#if	defined( EXP_ASCII85ENCODER)
tsize_t Ascii85EncodeBlock( uint8 * ascii85_p, unsigned f_eod, const uint8 * raw_p, tsize_t raw_l );
#endif

static	void usage(int);

int
main(int argc, char* argv[])
{
	SensorCall();int dirnum = -1, c, np = 0;
	int centered = 0;
	double bottommargin = 0;
	double leftmargin = 0;
	double pageWidth = 0;
	double pageHeight = 0;
	uint32 diroff = 0;
	extern char *optarg;
	extern int optind;
	FILE* output = stdout;

        pageOrientation[0] = '\0';

	SensorCall();while ((c = getopt(argc, argv, "b:d:h:H:W:L:i:w:l:o:O:P:C:r:t:acemxyzps1238DT")) != -1)
		{/*117*/SensorCall();switch (c) {
		case 'b':
			SensorCall();bottommargin = atof(optarg);
			SensorCall();break;
		case 'c':
			SensorCall();centered = 1;
			SensorCall();break;
                case 'C':
                        SensorCall();creator = optarg;
                        SensorCall();break;
		case 'd': /* without -a, this only processes one image at this IFD */
			SensorCall();dirnum = atoi(optarg);
			SensorCall();break;
		case 'D':
			SensorCall();PSduplex = TRUE;
			SensorCall();break;
		case 'i':
			SensorCall();interpolate = atoi(optarg) ? TRUE:FALSE;
			SensorCall();break;
		case 'T':
			SensorCall();PStumble = TRUE;
			SensorCall();break;
		case 'e':
                        SensorCall();PSavoiddeadzone = FALSE;
			generateEPSF = TRUE;
			SensorCall();break;
		case 'h':
			SensorCall();pageHeight = atof(optarg);
			SensorCall();break;
		case 'H':
			SensorCall();maxPageHeight = atof(optarg);
			SensorCall();break;
		case 'W':
			SensorCall();maxPageWidth = atof(optarg);
			SensorCall();break;
		case 'L':
			SensorCall();splitOverlap = atof(optarg);
			SensorCall();break;
		case 'm':
			SensorCall();useImagemask = TRUE;
			SensorCall();break;
		case 'o':
		        SensorCall();switch (optarg[0])
                          {
                          case '0':
                          case '1':
                          case '2':
                          case '3':
                          case '4':
                          case '5':
                          case '6':
                          case '7':
                          case '8':
                          case '9': SensorCall();diroff = (uint32) strtoul(optarg, NULL, 0);
			          SensorCall();break;
                          default: SensorCall();TIFFError ("-o", "Offset must be a numeric value.");
			    exit (1);
			  }
			SensorCall();break;
		case 'O':		/* XXX too bad -o is already taken */
			SensorCall();output = fopen(optarg, "w");
			SensorCall();if (output == NULL) {
				SensorCall();fprintf(stderr,
				    "%s: %s: Cannot open output file.\n",
				    argv[0], optarg);
				exit(-2);
			}
			SensorCall();break;
		case 'P':
                        SensorCall();switch (optarg[0])
                          {
                          case 'l':
                          case 'L': SensorCall();strcpy (pageOrientation, "Landscape");
			            SensorCall();break; 
                          case 'p':
                          case 'P': SensorCall();strcpy (pageOrientation, "Portrait");
			            SensorCall();break; 
                          default: SensorCall();TIFFError ("-P", "Page orientation must be Landscape or Portrait");
			           exit (-1);
			  }
			SensorCall();break;
		case 'l':
			SensorCall();leftmargin = atof(optarg);
			SensorCall();break;
		case 'a': /* removed fall through to generate warning below, R Nolde 09-01-2010 */
			SensorCall();printAll = TRUE;
			SensorCall();break;
		case 'p':
			SensorCall();generateEPSF = FALSE;
			SensorCall();break;
		case 'r':
                        SensorCall();if (strcmp (optarg, "auto") == 0)
			  {
                          SensorCall();rotation = 0;
                          auto_rotate = TRUE;
                          }
                        else
			  {
 			  SensorCall();rotation = atoi(optarg);
                          auto_rotate = FALSE;
			  }
                        SensorCall();switch (rotation)
                          {
			  case   0:
                          case  90:
                          case 180:
                          case 270:
			    SensorCall();break;
			  default:
                            SensorCall();fprintf (stderr, "Rotation angle must be 90, 180, 270 (degrees ccw) or auto\n");
			    exit (-1);
			  }
			SensorCall();break;
		case 's':
			SensorCall();printAll = FALSE;
			SensorCall();break;
                case 't':
                        SensorCall();title = optarg;
                        SensorCall();break;
		case 'w':
			SensorCall();pageWidth = atof(optarg);
			SensorCall();break;
		case 'z':
			SensorCall();PSavoiddeadzone = FALSE;
			SensorCall();break;
		case '1':
			SensorCall();level2 = FALSE;
			level3 = FALSE;
			ascii85 = FALSE;
			SensorCall();break;
		case '2':
			SensorCall();level2 = TRUE;
			ascii85 = TRUE;			/* default to yes */
			SensorCall();break;
		case '3':
			SensorCall();level3 = TRUE;
			ascii85 = TRUE;			/* default to yes */
			SensorCall();break;
		case '8':
			SensorCall();ascii85 = FALSE;
			SensorCall();break;
		case 'x':
			SensorCall();res_unit = RESUNIT_CENTIMETER;
			SensorCall();break;
		case 'y':
			SensorCall();res_unit = RESUNIT_INCH;
			SensorCall();break;
		case '?':
			SensorCall();usage(-1);
		;/*118*/}}

        SensorCall();if (useImagemask == TRUE)
          {
	  SensorCall();if ((level2 == FALSE) && (level3 == FALSE))
            {
	    SensorCall();TIFFError ("-m "," imagemask operator requres Postscript Level2 or Level3");
	    exit (1);
            }
          }

        SensorCall();if (pageWidth && (maxPageWidth > pageWidth))
	  {
	  SensorCall();TIFFError ("-W", "Max viewport width cannot exceed page width");
	  exit (1);
          }

	SensorCall();if (pageHeight && (maxPageHeight > pageHeight))
	  {
	  SensorCall();TIFFError ("-H", "Max viewport height cannot exceed page height");
	  exit (1);
          }

        /* auto rotate requires a specified page width and height */
        SensorCall();if (auto_rotate == TRUE)
          {
	  SensorCall();if ((pageWidth == 0) || (pageHeight == 0))
	    {/*119*/SensorCall();TIFFWarning ("-r auto", " requires page height and width specified with -h and -w");/*120*/}

          SensorCall();if ((maxPageWidth > 0) || (maxPageHeight > 0))
            {
	    SensorCall();TIFFError ("-r auto", " is incompatible with maximum page width/height specified by -H or -W");
            exit (1);
            }
          }
        SensorCall();if ((maxPageWidth > 0) && (maxPageHeight > 0))
            {
	    SensorCall();TIFFError ("-H and -W", " Use only one of -H or -W to define a viewport");
            exit (1);
            }

        SensorCall();if ((generateEPSF == TRUE) && (printAll == TRUE))
          {
	  SensorCall();TIFFError(" -e and -a", "Warning: Cannot generate Encapsulated Postscript for multiple images");
	  generateEPSF = FALSE;
          }

        SensorCall();if ((generateEPSF == TRUE) && (PSduplex == TRUE))
          {
	  SensorCall();TIFFError(" -e and -D", "Warning: Encapsulated Postscript does not support Duplex option");
	  PSduplex = FALSE;
          }

        SensorCall();if ((generateEPSF == TRUE) && (PStumble == TRUE))
          {
	  SensorCall();TIFFError(" -e and -T", "Warning: Encapsulated Postscript does not support Top Edge Binding option");
	  PStumble = FALSE;
          }

        if ((generateEPSF == TRUE) && (PSavoiddeadzone == TRUE))
	  PSavoiddeadzone = FALSE;

	SensorCall();for (; argc - optind > 0; optind++) {
		SensorCall();TIFF* tif = TIFFOpen(filename = argv[optind], "r");
		SensorCall();if (tif != NULL) {
			SensorCall();if (dirnum != -1
                            && !TIFFSetDirectory(tif, (tdir_t)dirnum))
				{/*123*/{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}/*124*/}
			else {/*125*/SensorCall();if (diroff != 0 &&
			    !TIFFSetSubDirectory(tif, diroff))
				{/*127*/{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}/*128*/}/*126*/}
			SensorCall();np = TIFF2PS(output, tif, pageWidth, pageHeight,
				     leftmargin, bottommargin, centered);
                        SensorCall();if (np < 0)
                          {
			  SensorCall();TIFFError("Error", "Unable to process %s", filename);
                          }
			SensorCall();TIFFClose(tif);
		}
	}
	SensorCall();if (np)
		{/*129*/SensorCall();PSTail(output, np);/*130*/}
	else
		{/*131*/SensorCall();usage(-1);/*132*/}
	SensorCall();if (output != stdout)
		{/*133*/SensorCall();fclose(output);/*134*/}
	{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
}

static	uint16 samplesperpixel;
static	uint16 bitspersample;
static	uint16 planarconfiguration;
static	uint16 photometric;
static	uint16 compression;
static	uint16 extrasamples;
static	int alpha;

static int
checkImage(TIFF* tif)
{
	SensorCall();switch (photometric) {
	case PHOTOMETRIC_YCBCR:
		SensorCall();if ((compression == COMPRESSION_JPEG || compression == COMPRESSION_OJPEG)
			&& planarconfiguration == PLANARCONFIG_CONTIG) {
			/* can rely on libjpeg to convert to RGB */
			SensorCall();TIFFSetField(tif, TIFFTAG_JPEGCOLORMODE,
				     JPEGCOLORMODE_RGB);
			photometric = PHOTOMETRIC_RGB;
		} else {
			SensorCall();if (level2 || level3)
				{/*135*/SensorCall();break;/*136*/}
			SensorCall();TIFFError(filename, "Can not handle image with %s",
			    "PhotometricInterpretation=YCbCr");
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
		/* fall thru... */
	case PHOTOMETRIC_RGB:
		SensorCall();if (alpha && bitspersample != 8) {
			SensorCall();TIFFError(filename,
			    "Can not handle %d-bit/sample RGB image with alpha",
			    bitspersample);
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
		/* fall thru... */
	case PHOTOMETRIC_SEPARATED:
	case PHOTOMETRIC_PALETTE:
	case PHOTOMETRIC_MINISBLACK:
	case PHOTOMETRIC_MINISWHITE:
		SensorCall();break;
	case PHOTOMETRIC_LOGL:
	case PHOTOMETRIC_LOGLUV:
		SensorCall();if (compression != COMPRESSION_SGILOG &&
		    compression != COMPRESSION_SGILOG24) {
			SensorCall();TIFFError(filename,
		    "Can not handle %s data with compression other than SGILog",
			    (photometric == PHOTOMETRIC_LOGL) ?
				"LogL" : "LogLuv"
			);
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
		/* rely on library to convert to RGB/greyscale */
		SensorCall();TIFFSetField(tif, TIFFTAG_SGILOGDATAFMT, SGILOGDATAFMT_8BIT);
		photometric = (photometric == PHOTOMETRIC_LOGL) ?
		    PHOTOMETRIC_MINISBLACK : PHOTOMETRIC_RGB;
		bitspersample = 8;
		SensorCall();break;
	case PHOTOMETRIC_CIELAB:
		/* fall thru... */
	default:
		SensorCall();TIFFError(filename,
		    "Can not handle image with PhotometricInterpretation=%d",
		    photometric);
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();switch (bitspersample) {
	case 1: case 2:
	case 4: case 8:
	case 16:
		SensorCall();break;
	default:
		SensorCall();TIFFError(filename, "Can not handle %d-bit/sample image",
		    bitspersample);
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();if (planarconfiguration == PLANARCONFIG_SEPARATE && extrasamples > 0)
		{/*137*/SensorCall();TIFFWarning(filename, "Ignoring extra samples");/*138*/}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

#define PS_UNIT_SIZE	72.0F
#define	PSUNITS(npix,res)	((npix) * (PS_UNIT_SIZE / (res)))

static	char RGBcolorimage[] = "\
/bwproc {\n\
    rgbproc\n\
    dup length 3 idiv string 0 3 0\n\
    5 -1 roll {\n\
	add 2 1 roll 1 sub dup 0 eq {\n\
	    pop 3 idiv\n\
	    3 -1 roll\n\
	    dup 4 -1 roll\n\
	    dup 3 1 roll\n\
	    5 -1 roll put\n\
	    1 add 3 0\n\
	} { 2 1 roll } ifelse\n\
    } forall\n\
    pop pop pop\n\
} def\n\
/colorimage where {pop} {\n\
    /colorimage {pop pop /rgbproc exch def {bwproc} image} bind def\n\
} ifelse\n\
";

/*
 * Adobe Photoshop requires a comment line of the form:
 *
 * %ImageData: <cols> <rows> <depth>  <main channels> <pad channels>
 *	<block size> <1 for binary|2 for hex> "data start"
 *
 * It is claimed to be part of some future revision of the EPS spec.
 */
static void
PhotoshopBanner(FILE* fd, uint32 w, uint32 h, int bs, int nc, char* startline)
{
	SensorCall();fprintf(fd, "%%ImageData: %ld %ld %d %d 0 %d 2 \"",
	    (long) w, (long) h, bitspersample, nc, bs);
	fprintf(fd, startline, nc);
	fprintf(fd, "\"\n");
SensorCall();}

/*   Convert pixel width and height pw, ph, to points pprw, pprh 
 *   using image resolution and resolution units from TIFF tags.
 *   pw : image width in pixels
 *   ph : image height in pixels
 * pprw : image width in PS units (72 dpi)
 * pprh : image height in PS units (72 dpi)
 */
static void
setupPageState(TIFF* tif, uint32* pw, uint32* ph, double* pprw, double* pprh)
{
	SensorCall();float xres = 0.0F, yres = 0.0F;

	TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, pw);
	TIFFGetField(tif, TIFFTAG_IMAGELENGTH, ph);
	SensorCall();if (res_unit == 0)	/* Not specified as command line option */
		if (!TIFFGetFieldDefaulted(tif, TIFFTAG_RESOLUTIONUNIT, &res_unit))
			res_unit = RESUNIT_INCH;
	/*
	 * Calculate printable area.
	 */
	if (!TIFFGetField(tif, TIFFTAG_XRESOLUTION, &xres)
            || fabs(xres) < 0.0000001)
		xres = PS_UNIT_SIZE;
	if (!TIFFGetField(tif, TIFFTAG_YRESOLUTION, &yres)
            || fabs(yres) < 0.0000001)
		yres = PS_UNIT_SIZE;
	SensorCall();switch (res_unit) {
	case RESUNIT_CENTIMETER:
		SensorCall();xres *= 2.54F, yres *= 2.54F;
		SensorCall();break;
	case RESUNIT_INCH:
		SensorCall();break;
	case RESUNIT_NONE:	/* Subsequent code assumes we have converted to inches! */
		SensorCall();res_unit = RESUNIT_INCH;
		SensorCall();break;
	default:	/* Last ditch guess for unspecified RESUNIT case
			 * check that the resolution is not inches before scaling it.
			 * Moved to end of function with additional check, RJN, 08-31-2010
			 * if (xres != PS_UNIT_SIZE || yres != PS_UNIT_SIZE)
			 * xres *= PS_UNIT_SIZE, yres *= PS_UNIT_SIZE;
			 */
		SensorCall();break;
	}
	/* This is a hack to deal with images that have no meaningful Resolution Size
	 * but may have x and/or y resolutions of 1 pixel per undefined unit.
	 */
	if ((xres > 1.0) && (xres != PS_UNIT_SIZE))
		*pprw = PSUNITS(*pw, xres);
	else
		*pprw = PSUNITS(*pw, PS_UNIT_SIZE);
	if ((yres > 1.0) && (yres != PS_UNIT_SIZE))
		*pprh = PSUNITS(*ph, yres);
	else
		*pprh = PSUNITS(*ph, PS_UNIT_SIZE);
}

static int
isCCITTCompression(TIFF* tif)
{
    SensorCall();uint16 compress;
    TIFFGetField(tif, TIFFTAG_COMPRESSION, &compress);
    {int  ReplaceReturn = (compress == COMPRESSION_CCITTFAX3 ||
	    compress == COMPRESSION_CCITTFAX4 ||
	    compress == COMPRESSION_CCITTRLE ||
	    compress == COMPRESSION_CCITTRLEW); SensorCall(); return ReplaceReturn;}
}

static	tsize_t tf_bytesperrow;
static	tsize_t ps_bytesperrow;
static	tsize_t	tf_rowsperstrip;
static	tsize_t	tf_numberstrips;
static	char *hex = "0123456789abcdef";

/*
 * Pagewidth and pageheight are the output size in points,
 * may refer to values specified with -h and -w, or to
 * values read from the image if neither -h nor -w are used.
 * Imagewidth and imageheight are image size in points.
 * Ximages and Yimages are number of pages across and down.
 * Only one of maxPageHeight or maxPageWidth can be used.
 * These are global variables unfortunately.
 */
int get_subimage_count(double pagewidth,  double pageheight,
		       double imagewidth, double imageheight,
		       int *ximages, int *yimages,
		       int rotation, double scale)
{
	SensorCall();int pages = 1;
	double splitheight = 0;  /* Requested Max Height in points */
	double splitwidth  = 0;  /* Requested Max Width in points */
	double overlap     = 0;  /* Repeated edge width in points */

	splitheight = maxPageHeight * PS_UNIT_SIZE;
	splitwidth  = maxPageWidth  * PS_UNIT_SIZE;
	overlap     = splitOverlap  * PS_UNIT_SIZE;
	pagewidth  *= PS_UNIT_SIZE;
	pageheight *= PS_UNIT_SIZE;

	SensorCall();if ((imagewidth < 1.0) || (imageheight < 1.0))
	{
		SensorCall();TIFFError("get_subimage_count", "Invalid image width or height");
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}

  SensorCall();switch (rotation)
    {
    case 0:
    case 180: SensorCall();if (splitheight > 0) /* -H maxPageHeight */
                {
               SensorCall();if (imageheight > splitheight) /* More than one vertical image segment */
                 {
                 SensorCall();if (pagewidth)
                   {/*151*/SensorCall();*ximages = (int)ceil((scale * imagewidth)  / (pagewidth - overlap));/*152*/}
                  else
                   {/*153*/SensorCall();*ximages = 1;/*154*/}
                 SensorCall();*yimages = (int)ceil((scale * imageheight) / (splitheight - overlap)); /* Max vert pages needed */
                 }
                else
                 {
                 SensorCall();if (pagewidth)
                   {/*155*/SensorCall();*ximages = (int)ceil((scale * imagewidth) / (pagewidth - overlap));/*156*/}    /* Max horz pages needed */
                  else
                   {/*157*/SensorCall();*ximages = 1;/*158*/}
                 SensorCall();*yimages = 1;                                                     /* Max vert pages needed */
                 }
               }
              else
               {
                SensorCall();if (splitwidth > 0) /* -W maxPageWidth */
                 {
                 SensorCall();if (imagewidth >splitwidth)
                   {
                   SensorCall();*ximages = (int)ceil((scale * imagewidth)  / (splitwidth - overlap));   /* Max horz pages needed */
                    SensorCall();if (pageheight)
                     {/*159*/SensorCall();*yimages = (int)ceil((scale * imageheight) / (pageheight - overlap));/*160*/} /* Max vert pages needed */
                    else
                     {/*161*/SensorCall();*yimages = 1;/*162*/}
                   }
                  else
                   {
                   SensorCall();*ximages = 1;                                                     /* Max vert pages needed */
                    SensorCall();if (pageheight)
                     {/*163*/SensorCall();*yimages = (int)ceil((scale * imageheight) / (pageheight - overlap));/*164*/} /* Max vert pages needed */
                    else
                     {/*165*/SensorCall();*yimages = 1;/*166*/}
                   }
                 }
                else
                 {
                 SensorCall();*ximages = 1;
                 *yimages = 1;
                 }
               }
             SensorCall();break;
    case 90:
    case 270: SensorCall();if (splitheight > 0) /* -H maxPageHeight */
                {
               SensorCall();if (imagewidth > splitheight) /* More than one vertical image segment */
                 {
                 SensorCall();*yimages = (int)ceil((scale * imagewidth) / (splitheight - overlap)); /* Max vert pages needed */
                  SensorCall();if (pagewidth)
                   {/*167*/SensorCall();*ximages = (int)ceil((scale * imageheight) / (pagewidth - overlap));/*168*/}   /* Max horz pages needed */
                  else
                   {/*169*/SensorCall();*ximages = 1;/*170*/}
                 }
                else
                 {
                 SensorCall();*yimages = 1;                                                     /* Max vert pages needed */
                  SensorCall();if (pagewidth)
                   {/*171*/SensorCall();*ximages = (int)ceil((scale * imageheight) / (pagewidth - overlap));/*172*/}    /* Max horz pages needed */
                  else
                   {/*173*/SensorCall();*ximages = 1;/*174*/}
                 }
               }
              else
               {
                SensorCall();if (splitwidth > 0) /* -W maxPageWidth */
                 {
                 SensorCall();if (imageheight > splitwidth)
                   {
                   SensorCall();if (pageheight)
                     {/*175*/SensorCall();*yimages = (int)ceil((scale * imagewidth) / (pageheight - overlap));/*176*/} /* Max vert pages needed */
                    else
                     {/*177*/SensorCall();*yimages = 1;/*178*/}
                   SensorCall();*ximages = (int)ceil((scale * imageheight)  / (splitwidth - overlap));   /* Max horz pages needed */
                   }
                  else
                   {
                   SensorCall();if (pageheight)
                     {/*179*/SensorCall();*yimages = (int)ceil((scale * imagewidth) / (pageheight - overlap));/*180*/}  /* Max horz pages needed */
                    else
                     {/*181*/SensorCall();*yimages = 1;/*182*/}
                   SensorCall();*ximages = 1;                                                     /* Max vert pages needed */
                   }
                 }
                else
                 {
                 SensorCall();*ximages = 1;
                 *yimages = 1;
                 }
               }
             SensorCall();break;
    default:  SensorCall();*ximages = 1;
             *yimages = 1;
  }
  SensorCall();pages = (*ximages) * (*yimages);
  {int  ReplaceReturn = (pages); SensorCall(); return ReplaceReturn;}
  }

/* New version of PlaceImage that handles only the translation and rotation
 * for a single output page.
 */
int exportMaskedImage(FILE *fp, double pagewidth, double pageheight,
                     double imagewidth, double imageheight,
                      int row, int column,
                      double left_offset, double bott_offset,
                     double scale, int center, int rotation)
  {
  SensorCall();double xtran = 0.0;
  double ytran = 0.0;

  double xscale = 1.0;
  double yscale = 1.0;

  double splitheight    = 0;  /* Requested Max Height in points */
  double splitwidth     = 0;  /* Requested Max Width in points */
  double overlap        = 0;  /* Repeated edge width in points */
  double subimage_height = 0.0;

  splitheight = maxPageHeight * PS_UNIT_SIZE;
  splitwidth  = maxPageWidth  * PS_UNIT_SIZE;
  overlap     = splitOverlap  * PS_UNIT_SIZE;
  xscale = scale * imagewidth;
  yscale = scale * imageheight;

  SensorCall();if ((xscale < 0.0) || (yscale < 0.0))
    {
    SensorCall();TIFFError("exportMaskedImage", "Invalid parameters.");
    {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }

  /* If images are cropped to a vewport with -H or -W, the output pages are shifted to
   * the top of each output page rather than the Postscript default lower edge.
   */
  SensorCall();switch (rotation)
    {
    case 0:
    case 180: SensorCall();if (splitheight > 0) /* -H maxPageHeight */
                {
               SensorCall();if (splitheight < imageheight) /* More than one vertical image segments */
                 {
                 SensorCall();xtran = -1.0 * column * (pagewidth - overlap);
                  subimage_height = imageheight - ((splitheight - overlap) * row);
                 ytran  = pageheight - subimage_height * (pageheight / splitheight);
                  }
                else  /* Only one page in vertical direction */
                 {
                 SensorCall();xtran = -1.0 * column * (pagewidth - overlap);
                  ytran = splitheight - imageheight;
                 }
               }
              else
               {
                SensorCall();if (splitwidth > 0) /* maxPageWidth */
                 {
                 SensorCall();if (splitwidth < imagewidth)
                   {
                   SensorCall();xtran = -1.0  * column * splitwidth;
                   ytran = -1.0 * row * (pageheight - overlap);
                    }
                  else /* Only one page in horizontal direction */
                   {
                    SensorCall();ytran = -1.0 * row * (pageheight - overlap);
                    xtran = 0;
                   }
                 }
                else    /* Simple case, no splitting */
                 {
                 SensorCall();ytran = pageheight - imageheight;
                 xtran = 0;
                  }
                }
              SensorCall();bott_offset += ytran / (center ? 2 : 1);
              left_offset += xtran / (center ? 2 : 1);
              SensorCall();break;
    case  90:
    case 270:  SensorCall();if (splitheight > 0) /* -H maxPageHeight */
                {
               SensorCall();if (splitheight < imagewidth) /* More than one vertical image segments */
                 {
                 SensorCall();xtran = -1.0 * column * (pageheight - overlap);
                 /* Commented code places image at bottom of page instead of top.
                     ytran = -1.0 * row * splitheight;
                   */
                  SensorCall();if (row == 0)
                    {/*107*/SensorCall();ytran = -1.0 * (imagewidth - splitheight);/*108*/}
                  else
                    {/*109*/SensorCall();ytran = -1.0 * (imagewidth - (splitheight - overlap) * (row + 1));/*110*/}
                  }
                else  /* Only one page in vertical direction */
                 {
                  SensorCall();xtran = -1.0 * column * (pageheight - overlap);
                  ytran = splitheight - imagewidth;
                 }
		}
              else
               {
                SensorCall();if (splitwidth > 0) /* maxPageWidth */
                 {
                 SensorCall();if (splitwidth < imageheight)
                   {
                    SensorCall();xtran = -1.0  * column * splitwidth;
                    ytran = -1.0 * row * (pagewidth - overlap);
                    }
                  else /* Only one page in horizontal direction */
                   {
                    SensorCall();ytran = -1.0 * row * (pagewidth - overlap);
                    xtran = 0;
                   }
                 }
                else    /* Simple case, no splitting */
                 {
                 SensorCall();ytran = pageheight - imageheight;
                 xtran = 0; /* pagewidth  - imagewidth; */
                  }
                }
              SensorCall();bott_offset += ytran / (center ? 2 : 1);
              left_offset += xtran / (center ? 2 : 1);
              SensorCall();break;
    default:  SensorCall();xtran = 0;
             ytran = 0;
    }

  SensorCall();switch (rotation)
    {
    case   0: SensorCall();fprintf(fp, "%f %f translate\n", left_offset, bott_offset);
              fprintf(fp, "%f %f scale\n", xscale, yscale);
             SensorCall();break;
    case 180: SensorCall();fprintf(fp, "%f %f translate\n", left_offset, bott_offset);
              fprintf(fp, "%f %f scale\n1 1 translate 180 rotate\n",  xscale, yscale);
              SensorCall();break;
    case  90: SensorCall();fprintf(fp, "%f %f translate\n", left_offset, bott_offset);
              fprintf(fp, "%f %f scale\n1 0 translate 90 rotate\n", yscale, xscale);
              SensorCall();break;
    case 270: SensorCall();fprintf(fp, "%f %f translate\n", left_offset, bott_offset);
              fprintf(fp, "%f %f scale\n0 1 translate 270 rotate\n", yscale, xscale);
              SensorCall();break;
    default:  SensorCall();TIFFError ("exportMaskedImage", "Unsupported rotation angle %d. No rotation", rotation);
             fprintf( fp, "%f %f scale\n", xscale, yscale);
              SensorCall();break;
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }

/* Rotate an image without scaling or clipping */
int  psRotateImage (FILE * fd, int rotation, double pswidth, double psheight,
                    double left_offset, double bottom_offset)
  {
  SensorCall();if ((left_offset != 0.0) || (bottom_offset != 0))
    {/*99*/SensorCall();fprintf (fd, "%f %f translate\n", left_offset, bottom_offset);/*100*/}

  /* Exchange width and height for 90/270 rotations */
  SensorCall();switch (rotation)
    {
    case   0: SensorCall();fprintf (fd, "%f %f scale\n", pswidth, psheight);
              SensorCall();break;
    case  90: SensorCall();fprintf (fd, "%f %f scale\n1 0 translate 90 rotate\n", psheight, pswidth);
              SensorCall();break;
    case 180: SensorCall();fprintf (fd, "%f %f scale\n1 1 translate 180 rotate\n", pswidth, psheight);
              SensorCall();break;
    case 270: SensorCall();fprintf (fd, "%f %f scale\n0 1 translate 270 rotate\n", psheight, pswidth);
              SensorCall();break;
    default:  SensorCall();TIFFError ("psRotateImage", "Unsupported rotation %d.", rotation);
             fprintf( fd, "%f %f scale\n", pswidth, psheight);
              {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }

/* Scale and rotate an image to a single output page. */
int psScaleImage(FILE * fd, double scale, int rotation, int center,
                 double reqwidth, double reqheight, double pswidth, double psheight,
                 double left_offset, double bottom_offset)
  {
  SensorCall();double hcenter = 0.0, vcenter = 0.0;

  /* Adjust offsets for centering */
  SensorCall();if (center)
    {
    SensorCall();switch (rotation)
      {
      case   90: SensorCall();vcenter = (reqheight - pswidth * scale) / 2;
                hcenter = (reqwidth - psheight * scale) / 2;
                 fprintf (fd, "%f %f translate\n", hcenter, vcenter);
                 fprintf (fd, "%f %f scale\n1 0 translate 90 rotate\n", psheight * scale, pswidth * scale);
                 SensorCall();break;
      case  180: SensorCall();hcenter = (reqwidth - pswidth * scale) / 2;
                vcenter = (reqheight - psheight * scale) / 2;
                 fprintf (fd, "%f %f translate\n", hcenter, vcenter);
                 fprintf (fd, "%f %f scale\n1 1 translate 180 rotate\n", pswidth * scale, psheight * scale);
                 SensorCall();break;
      case  270: SensorCall();vcenter = (reqheight - pswidth * scale) / 2;
                hcenter = (reqwidth - psheight * scale) / 2;
                 fprintf (fd, "%f %f translate\n", hcenter, vcenter);
                 fprintf (fd, "%f %f scale\n0 1 translate 270 rotate\n", psheight * scale, pswidth * scale);
                 SensorCall();break;
      case    0:
      default:   SensorCall();hcenter = (reqwidth - pswidth * scale) / 2;
                vcenter = (reqheight - psheight * scale) / 2;
                 fprintf (fd, "%f %f translate\n", hcenter, vcenter);
                 fprintf (fd, "%f %f scale\n", pswidth * scale, psheight * scale);
                 SensorCall();break;
      }
    }
  else  /* Not centered */
    {
    SensorCall();switch (rotation)
      {
      case 0:   SensorCall();fprintf (fd, "%f %f translate\n", left_offset ? left_offset : 0.0,
                         bottom_offset ? bottom_offset : reqheight - (psheight * scale));
                fprintf (fd, "%f %f scale\n", pswidth * scale, psheight * scale);
                SensorCall();break;
      case 90:  SensorCall();fprintf (fd, "%f %f translate\n", left_offset ? left_offset : 0.0,
                         bottom_offset ? bottom_offset : reqheight - (pswidth * scale));
                fprintf (fd, "%f %f scale\n1 0 translate 90 rotate\n", psheight * scale, pswidth * scale);
                SensorCall();break;
      case 180: SensorCall();fprintf (fd, "%f %f translate\n", left_offset ? left_offset : 0.0,
                         bottom_offset ? bottom_offset : reqheight - (psheight * scale));
                fprintf (fd, "%f %f scale\n1 1 translate 180 rotate\n", pswidth * scale, psheight * scale);
                SensorCall();break;
      case 270: SensorCall();fprintf (fd, "%f %f translate\n", left_offset ? left_offset : 0.0,
                         bottom_offset ? bottom_offset : reqheight - (pswidth * scale));
                fprintf (fd, "%f %f scale\n0 1 translate 270 rotate\n", psheight * scale, pswidth * scale);
                SensorCall();break;
      default:  SensorCall();TIFFError ("psScaleImage", "Unsupported rotation  %d", rotation);
               fprintf (fd, "%f %f scale\n", pswidth * scale, psheight * scale);
                {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
      }
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }

/* This controls the visible portion of the page which is displayed.
 * N.B. Setting maxPageHeight no longer sets pageheight if not set explicitly
 */
int psPageSize (FILE * fd, int rotation, double pgwidth, double pgheight,
                double reqwidth, double reqheight, double pswidth, double psheight)
  {
  SensorCall();double xscale = 1.0, yscale = 1.0, scale = 1.0;
  double splitheight;
  double splitwidth;
  double new_width;
  double new_height;

  splitheight = maxPageHeight * PS_UNIT_SIZE;
  splitwidth  = maxPageWidth  * PS_UNIT_SIZE;

  SensorCall();switch (rotation)
    {
    case   0:
    case 180: SensorCall();if ((splitheight > 0) || (splitwidth > 0))
                {
               SensorCall();if (pgwidth != 0 || pgheight != 0)
                  {
                 SensorCall();xscale = reqwidth / (splitwidth ? splitwidth : pswidth);
                 yscale = reqheight / (splitheight ? splitheight : psheight);
                  scale = (xscale < yscale) ? xscale : yscale;
                  }
                SensorCall();new_width = splitwidth ? splitwidth : scale * pswidth;
                new_height = splitheight ? splitheight : scale * psheight;
                SensorCall();if (strlen(pageOrientation))
                  {/*75*/SensorCall();fprintf (fd, "%%%%PageOrientation: %s\n", pageOrientation);/*76*/}
                else
                  {/*77*/SensorCall();fprintf (fd, "%%%%PageOrientation: %s\n", (new_width > new_height) ? "Landscape" : "Portrait");/*78*/}
                SensorCall();fprintf (fd, "%%%%PageBoundingBox: 0 0 %ld %ld\n", (long)new_width, (long)new_height);
                fprintf (fd, "1 dict begin /PageSize [ %f %f ] def currentdict end setpagedevice\n",
                       new_width, new_height);
                }
             else /* No viewport defined with -H or -W */
                {
                SensorCall();if ((pgwidth == 0) && (pgheight == 0)) /* Image not scaled */
                  {
                  SensorCall();if (strlen(pageOrientation))
                    {/*79*/SensorCall();fprintf (fd, "%%%%PageOrientation: %s\n", pageOrientation);/*80*/}
                  else
                    {/*81*/SensorCall();fprintf (fd, "%%%%PageOrientation: %s\n", (pswidth > psheight) ? "Landscape" : "Portrait");/*82*/}
                 SensorCall();fprintf (fd, "%%%%PageBoundingBox: 0 0 %ld %ld\n", (long)pswidth, (long)psheight);
                  fprintf(fd, "1 dict begin /PageSize [ %f %f ] def currentdict end setpagedevice\n",
                          pswidth, psheight);
                  }
               else /* Image scaled */
                  {
                  SensorCall();if (strlen(pageOrientation))
                    {/*83*/SensorCall();fprintf (fd, "%%%%PageOrientation: %s\n", pageOrientation);/*84*/}
                  else
                    {/*85*/SensorCall();fprintf (fd, "%%%%PageOrientation: %s\n", (reqwidth > reqheight) ? "Landscape" : "Portrait");/*86*/}
                 SensorCall();fprintf (fd, "%%%%PageBoundingBox: 0 0 %ld %ld\n", (long)reqwidth, (long)reqheight);
                  fprintf(fd, "1 dict begin /PageSize [ %f %f ] def currentdict end setpagedevice\n",
                           reqwidth, reqheight);
                  }
                }
             SensorCall();break;
    case  90:
    case 270: SensorCall();if ((splitheight > 0) || (splitwidth > 0))
               {
               SensorCall();if (pgwidth != 0 || pgheight != 0)
                  {
                 SensorCall();xscale = reqwidth / (splitwidth ? splitwidth : pswidth);
                 yscale = reqheight / (splitheight ? splitheight : psheight);
                  scale = (xscale < yscale) ? xscale : yscale;
                  }
                SensorCall();new_width = splitwidth ? splitwidth : scale * psheight;
                new_height = splitheight ? splitheight : scale * pswidth;

                SensorCall();if (strlen(pageOrientation))
                  {/*87*/SensorCall();fprintf (fd, "%%%%PageOrientation: %s\n", pageOrientation);/*88*/}
                else
                  {/*89*/SensorCall();fprintf (fd, "%%%%PageOrientation: %s\n", (new_width > new_height) ? "Landscape" : "Portrait");/*90*/}
                SensorCall();fprintf (fd, "%%%%PageBoundingBox: 0 0 %ld %ld\n", (long)new_width, (long)new_height);
                fprintf (fd, "1 dict begin /PageSize [ %f %f ] def currentdict end setpagedevice\n",
                       new_width, new_height);
                }
              else
                {
                SensorCall();if ((pgwidth == 0) && (pgheight == 0)) /* Image not scaled */
                  {
                  SensorCall();if (strlen(pageOrientation))
                    {/*91*/SensorCall();fprintf (fd, "%%%%PageOrientation: %s\n", pageOrientation);/*92*/}
                  else
                    {/*93*/SensorCall();fprintf (fd, "%%%%PageOrientation: %s\n", (psheight > pswidth) ? "Landscape" : "Portrait");/*94*/}
                 SensorCall();fprintf (fd, "%%%%PageBoundingBox: 0 0 %ld %ld\n", (long)psheight, (long)pswidth);
                  fprintf(fd, "1 dict begin /PageSize [ %f %f ] def currentdict end setpagedevice\n",
                         psheight, pswidth);
                  }
               else /* Image scaled */
                  {
                  SensorCall();if (strlen(pageOrientation))
                    {/*95*/SensorCall();fprintf (fd, "%%%%PageOrientation: %s\n", pageOrientation);/*96*/}
                  else
                    {/*97*/SensorCall();fprintf (fd, "%%%%PageOrientation: %s\n", (reqwidth > reqheight) ? "Landscape" : "Portrait");/*98*/}
                 SensorCall();fprintf (fd, "%%%%PageBoundingBox: 0 0 %ld %ld\n", (long)reqwidth, (long)reqheight);
                  fprintf(fd, "1 dict begin /PageSize [ %f %f ] def currentdict end setpagedevice\n",
                          reqwidth, reqheight);
                  }
               }
             SensorCall();break;
    default:  SensorCall();TIFFError ("psPageSize", "Invalid rotation %d", rotation);
      {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }
  SensorCall();fputs("<<\n  /Policies <<\n    /PageSize 3\n  >>\n>> setpagedevice\n", fd);

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end psPageSize */

/* Mask an image as a series of pages, each only showing a section defined
 * by the maxPageHeight or maxPageWidth options.
 */
int psMaskImage(FILE *fd, TIFF *tif, int rotation, int center,
                int *npages, double pixwidth, double pixheight,
               double left_margin, double bottom_margin,
                double pgwidth, double pgheight,
               double pswidth, double psheight, double scale)
  {
  SensorCall();int i, j;
  int ximages = 1, yimages = 1;
  int pages = *npages;
  double view_width = 0;
  double view_height = 0;

  SensorCall();if (get_viewport (pgwidth, pgheight, pswidth, psheight, &view_width, &view_height, rotation))
    {
    SensorCall();TIFFError ("get_viewport", "Unable to set image viewport");
    {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();if (get_subimage_count(pgwidth, pgheight, pswidth, psheight,
                        &ximages, &yimages, rotation, scale) < 1)
    {
    SensorCall();TIFFError("get_subimage_count", "Invalid image count: %d columns, %d rows", ximages, yimages);
    {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();for (i = 0; i < yimages; i++)
    {
    SensorCall();for (j = 0; j < ximages; j++)
       {
       SensorCall();pages++;
       *npages = pages;
       fprintf(fd, "%%%%Page: %d %d\n", pages, pages);

       /* Write out the PageSize info for non EPS files */
       SensorCall();if (!generateEPSF && ( level2 || level3 ))
         {
         SensorCall();if (psPageSize(fd, rotation, pgwidth, pgheight,
                        view_width, view_height, pswidth, psheight))
           {/*101*/{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}/*102*/}
        }
       SensorCall();fprintf(fd, "gsave\n");
       fprintf(fd, "100 dict begin\n");
       SensorCall();if (exportMaskedImage(fd, view_width, view_height, pswidth, psheight,
                            i, j, left_margin, bottom_margin,
                            scale, center, rotation))
        {
        SensorCall();TIFFError("exportMaskedImage", "Invalid image parameters.");
        {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
        }
       SensorCall();PSpage(fd, tif, pixwidth, pixheight);
       fprintf(fd, "end\n");
       fprintf(fd, "grestore\n");
       fprintf(fd, "showpage\n");
       }
    }

  {int  ReplaceReturn = (pages); SensorCall(); return ReplaceReturn;}
  }

/* Compute scale factor and write out file header */
int psStart(FILE *fd, int npages, int auto_rotate, int *rotation, double *scale,
            double ox, double oy, double pgwidth, double pgheight,
           double reqwidth, double reqheight, double pswidth, double psheight,
           double left_offset, double bottom_offset)
  {
  SensorCall();double maxsource = 0.0;    /* Used for auto rotations */
  double maxtarget = 0.0;
  double xscale = 1.0, yscale = 1.0;
  double splitheight;
  double splitwidth;
  double view_width = 0.0, view_height = 0.0;
  double page_width = 0.0, page_height = 0.0;

  /* Splitheight and splitwidth are in inches */
  splitheight = maxPageHeight * PS_UNIT_SIZE;
  splitwidth  = maxPageWidth * PS_UNIT_SIZE;

  page_width = pgwidth * PS_UNIT_SIZE;
  page_height = pgheight * PS_UNIT_SIZE;

  /* If user has specified a page width and height and requested the
   * image to be auto-rotated to fit on that media, we match the
   * longest dimension of the image to the longest dimension of the
   * target media but we have to ignore auto rotate if user specified
   * maxPageHeight since this makes life way too complicated. */
  SensorCall();if (auto_rotate)
    {
    SensorCall();if ((splitheight != 0) || (splitwidth != 0))
      {
      SensorCall();TIFFError ("psStart", "Auto-rotate is incompatible with page splitting ");
      {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
      }

    /* Find longest edges in image and output media */
    SensorCall();maxsource = (pswidth >= psheight) ? pswidth : psheight;
    maxtarget = (reqwidth >= reqheight) ? reqwidth : reqheight;

    SensorCall();if (((maxsource == pswidth) && (maxtarget != reqwidth)) ||
        ((maxsource == psheight) && (maxtarget != reqheight)))
      {  /* optimal orientaion does not match input orientation */
      SensorCall();*rotation = 90;
      xscale = (reqwidth - left_offset)/psheight;
      yscale = (reqheight - bottom_offset)/pswidth;
      }
    else /* optimal orientaion matches input orientation */
      {
      SensorCall();xscale = (reqwidth - left_offset)/pswidth;
      yscale = (reqheight - bottom_offset)/psheight;
      }
    SensorCall();*scale = (xscale < yscale) ? xscale : yscale;

    /* Do not scale image beyound original size */
    SensorCall();if (*scale > 1.0)
      {/*63*/SensorCall();*scale = 1.0;/*64*/}

    /* Set the size of the displayed image to requested page size
     * and optimal orientation.
     */
    SensorCall();if (!npages)
      {/*65*/SensorCall();PSHead(fd, reqwidth, reqheight, ox, oy);/*66*/}

    {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
    }

  /* N.B. If pgwidth or pgheight are set from maxPageHeight/Width,
   * we have a problem with the tests below under splitheight.
   */

  SensorCall();switch (*rotation)  /* Auto rotate has NOT been specified */
    {
    case   0:
    case 180: SensorCall();if ((splitheight != 0)  || (splitwidth != 0))
                {  /* Viewport clipped to maxPageHeight or maxPageWidth */
                SensorCall();if ((page_width != 0) || (page_height != 0)) /* Image scaled */
                  {
                 SensorCall();xscale = (reqwidth  - left_offset) / (page_width ? page_width : pswidth);
                 yscale = (reqheight - bottom_offset) / (page_height ? page_height : psheight);
                  *scale = (xscale < yscale) ? xscale : yscale;
                  /*
                  if (*scale > 1.0)
                    *scale = 1.0;
                   */
                 }
                else       /* Image clipped but not scaled */
                 {/*67*/SensorCall();*scale = 1.0;/*68*/}

                SensorCall();view_width = splitwidth ? splitwidth : *scale * pswidth;
                view_height = splitheight ? splitheight: *scale * psheight;
               }
              else   /* Viewport not clipped to maxPageHeight or maxPageWidth */
                {
                SensorCall();if ((page_width != 0) || (page_height != 0))
                  {   /* Image scaled  */
                  SensorCall();xscale = (reqwidth - left_offset) / pswidth;
                  yscale = (reqheight - bottom_offset) / psheight;

                  view_width = reqwidth;
                  view_height = reqheight;
                 }
                else
                  {  /* Image not scaled  */
                  SensorCall();xscale = (pswidth - left_offset)/pswidth;
                  yscale = (psheight - bottom_offset)/psheight;

                  view_width = pswidth;
                  view_height = psheight;
                 }
               }
             SensorCall();break;
    case  90:
    case 270: SensorCall();if ((splitheight != 0) || (splitwidth != 0))
                {  /* Viewport clipped to maxPageHeight or maxPageWidth */
                SensorCall();if ((page_width != 0) || (page_height != 0)) /* Image scaled */
                  {
                 SensorCall();xscale = (reqwidth - left_offset)/ psheight;
                 yscale = (reqheight - bottom_offset)/ pswidth;
                  *scale = (xscale < yscale) ? xscale : yscale;
                  /*
                  if (*scale > 1.0)
                    *scale = 1.0;
                 */
                 }
                else  /* Image clipped but not scaled */
                 {/*69*/SensorCall();*scale = 1.0;/*70*/}
                SensorCall();view_width = splitwidth ? splitwidth : *scale * psheight;
                view_height = splitheight ? splitheight : *scale * pswidth;
               }
              else /* Viewport not clipped to maxPageHeight or maxPageWidth */
                {
                SensorCall();if ((page_width != 0) || (page_height != 0)) /* Image scaled */
                  {
                  SensorCall();xscale = (reqwidth - left_offset) / psheight;
                  yscale = (reqheight - bottom_offset) / pswidth;

                 view_width = reqwidth;
                 view_height = reqheight;
                 }
                else
                  {
                  SensorCall();xscale = (pswidth  - left_offset)/ psheight;
                 yscale = (psheight  - bottom_offset)/ pswidth;

                 view_width = psheight;
                 view_height = pswidth;
                  }
                }
              SensorCall();break;
    default:  SensorCall();TIFFError ("psPageSize", "Invalid rotation %d", *rotation);
              {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();if (!npages)
    {/*71*/SensorCall();PSHead(fd, (page_width ? page_width : view_width), (page_height ? page_height : view_height), ox, oy);/*72*/}

  SensorCall();*scale = (xscale < yscale) ? xscale : yscale;
  SensorCall();if (*scale > 1.0)
    {/*73*/SensorCall();*scale = 1.0;/*74*/}

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }

int get_viewport (double pgwidth, double pgheight, double pswidth, double psheight,
                  double *view_width, double *view_height, int rotation)
  {
  /* Only one of maxPageHeight or maxPageWidth can be specified */
  SensorCall();if (maxPageHeight != 0)   /* Clip the viewport to maxPageHeight on each page */
    {
    SensorCall();*view_height = maxPageHeight * PS_UNIT_SIZE;
    /*
     * if (res_unit == RESUNIT_CENTIMETER)
     * *view_height /= 2.54F;
     */
    }
  else
    {
    SensorCall();if (pgheight != 0) /* User has set PageHeight with -h flag */
      {
      SensorCall();*view_height = pgheight * PS_UNIT_SIZE; /* Postscript size for Page Height in inches */
      /* if (res_unit == RESUNIT_CENTIMETER)
       *  *view_height /= 2.54F;
       */
      }
    else /* If no width or height are specified, use the original size from image */
      {/*103*/SensorCall();switch (rotation)
        {
        default:
        case   0:
        case 180: SensorCall();*view_height = psheight;
                 SensorCall();break;
        case  90:
        case 270: SensorCall();*view_height = pswidth;
                 SensorCall();break;
       ;/*104*/}}
    }

  SensorCall();if (maxPageWidth != 0)   /* Clip the viewport to maxPageWidth on each page */
    {
    SensorCall();*view_width = maxPageWidth * PS_UNIT_SIZE;
    /* if (res_unit == RESUNIT_CENTIMETER)
     *  *view_width /= 2.54F;
     */
    }
  else
    {
    SensorCall();if (pgwidth != 0)  /* User has set PageWidth with -w flag */
      {
      SensorCall();*view_width = pgwidth * PS_UNIT_SIZE; /* Postscript size for Page Width in inches */
      /* if (res_unit == RESUNIT_CENTIMETER)
       * *view_width /= 2.54F;
       */
      }
    else  /* If no width or height are specified, use the original size from image */
      {/*105*/SensorCall();switch (rotation)
        {
        default:
        case   0:
        case 180: SensorCall();*view_width = pswidth;
                 SensorCall();break;
        case  90:
        case 270: SensorCall();*view_width = psheight; /* (*view_height / psheight) * psheight; */
                 SensorCall();break;
       ;/*106*/}}
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }

/* pgwidth and pgheight specify page width and height in inches from -h and -w flags
 * lm and bm are the LeftMargin and BottomMargin in inches
 * center causes the image to be centered on the page if the paper size is
 * larger than the image size
 * returns the sequence number of the page processed or -1 on error
 */

int TIFF2PS(FILE* fd, TIFF* tif, double pgwidth, double pgheight, double lm, double bm, int center)
  {
  SensorCall();uint32 pixwidth = 0, pixheight = 0;  /* Image width and height in pixels */
  double ox = 0.0, oy = 0.0;  /* Offset from current Postscript origin */
  double pswidth, psheight;   /* Original raw image width and height in points */
  double view_width, view_height; /* Viewport width and height in points */
  double scale = 1.0;
  double left_offset = lm * PS_UNIT_SIZE;
  double bottom_offset = bm * PS_UNIT_SIZE;
  uint32 subfiletype;
  uint16* sampleinfo;
  static int npages = 0;

  SensorCall();if (!TIFFGetField(tif, TIFFTAG_XPOSITION, &ox))
     {/*1*/SensorCall();ox = 0;/*2*/}
  SensorCall();if (!TIFFGetField(tif, TIFFTAG_YPOSITION, &oy))
     {/*3*/SensorCall();oy = 0;/*4*/}

  /* Consolidated all the tag information into one code segment, Richard Nolde */
  SensorCall();do {
     SensorCall();tf_numberstrips = TIFFNumberOfStrips(tif);
     TIFFGetFieldDefaulted(tif, TIFFTAG_ROWSPERSTRIP, &tf_rowsperstrip);
     TIFFGetFieldDefaulted(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);
     TIFFGetFieldDefaulted(tif, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
     TIFFGetFieldDefaulted(tif, TIFFTAG_PLANARCONFIG, &planarconfiguration);
     TIFFGetField(tif, TIFFTAG_COMPRESSION, &compression);
     TIFFGetFieldDefaulted(tif, TIFFTAG_EXTRASAMPLES, &extrasamples, &sampleinfo);
     alpha = (extrasamples == 1 && sampleinfo[0] == EXTRASAMPLE_ASSOCALPHA);
     SensorCall();if (!TIFFGetField(tif, TIFFTAG_PHOTOMETRIC, &photometric))
       {
       SensorCall();switch (samplesperpixel - extrasamples)
             {
            case 1: SensorCall();if (isCCITTCompression(tif))
                      photometric = PHOTOMETRIC_MINISWHITE;
                    else
                       photometric = PHOTOMETRIC_MINISBLACK;
                    SensorCall();break;
            case 3: SensorCall();photometric = PHOTOMETRIC_RGB;
                    SensorCall();break;
            case 4: SensorCall();photometric = PHOTOMETRIC_SEPARATED;
                    SensorCall();break;
            }
       }

     /* Read image tags for width and height in pixels pixwidth, pixheight,
      * and convert to points pswidth, psheight
      */
     SensorCall();setupPageState(tif, &pixwidth, &pixheight, &pswidth, &psheight);
     view_width = pswidth;
     view_height = psheight;

     SensorCall();if (get_viewport (pgwidth, pgheight, pswidth, psheight, &view_width, &view_height, rotation))
       {
       SensorCall();TIFFError("get_viewport", "Unable to set image viewport");
       {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
       }

     /* Write the Postscript file header with Bounding Box and Page Size definitions */
     SensorCall();if (psStart(fd, npages, auto_rotate, &rotation, &scale, ox, oy,
                pgwidth, pgheight, view_width, view_height, pswidth, psheight,
                 left_offset, bottom_offset))
       {/*5*/{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}/*6*/}

     SensorCall();if (checkImage(tif))  /* Aborts if unsupported image parameters */
       {
       SensorCall();tf_bytesperrow = TIFFScanlineSize(tif);

       /* Set viewport clipping and scaling options */
       SensorCall();if ((maxPageHeight) || (maxPageWidth)  || (pgwidth != 0) || (pgheight != 0))
         {
        SensorCall();if ((maxPageHeight) || (maxPageWidth)) /* used -H or -W  option */
           {
          SensorCall();if (psMaskImage(fd, tif, rotation, center, &npages, pixwidth, pixheight,
                          left_offset, bottom_offset, pgwidth, pgheight,
                           pswidth, psheight, scale) < 0)
            {/*7*/{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}/*8*/}
          }
         else  /* N.B. Setting maxPageHeight no longer sets pgheight */
           {
           SensorCall();if (pgwidth != 0 || pgheight != 0)
             {
             /* User did not specify a maxium page height or width using -H or -W flag
              * but did use -h or -w flag to scale to a specific size page.
              */
             SensorCall();npages++;
             fprintf(fd, "%%%%Page: %d %d\n", npages, npages);

             SensorCall();if (!generateEPSF && ( level2 || level3 ))
               {
              /* Write out the PageSize info for non EPS files */
              SensorCall();if (psPageSize(fd, rotation, pgwidth, pgheight,
                              view_width, view_height, pswidth, psheight))
                {/*9*/{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}/*10*/}
               }
             SensorCall();fprintf(fd, "gsave\n");
             fprintf(fd, "100 dict begin\n");
             SensorCall();if (psScaleImage(fd, scale, rotation, center, view_width, view_height,
                              pswidth, psheight, left_offset, bottom_offset))
              {/*11*/{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}/*12*/}

             SensorCall();PSpage(fd, tif, pixwidth, pixheight);
             fprintf(fd, "end\n");
             fprintf(fd, "grestore\n");
             fprintf(fd, "showpage\n");
            }
          }
        }
       else  /* Simple rotation: user did not use -H, -W, -h or -w */
         {
         SensorCall();npages++;
         fprintf(fd, "%%%%Page: %d %d\n", npages, npages);

         SensorCall();if (!generateEPSF && ( level2 || level3 ))
           {
          /* Write out the PageSize info for non EPS files */
          SensorCall();if (psPageSize(fd, rotation, pgwidth, pgheight,
                          view_width, view_height, pswidth, psheight))
           {/*13*/{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}/*14*/}
         }
         SensorCall();fprintf(fd, "gsave\n");
         fprintf(fd, "100 dict begin\n");
        SensorCall();if (psRotateImage(fd, rotation, pswidth, psheight, left_offset, bottom_offset))
           {/*15*/{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}/*16*/}

         SensorCall();PSpage(fd, tif, pixwidth, pixheight);
         fprintf(fd, "end\n");
         fprintf(fd, "grestore\n");
         fprintf(fd, "showpage\n");
         }
       }
  SensorCall();if (generateEPSF)
    {/*17*/SensorCall();break;/*18*/}
  SensorCall();TIFFGetFieldDefaulted(tif, TIFFTAG_SUBFILETYPE, &subfiletype);
  } while (((subfiletype & FILETYPE_PAGE) || printAll) && TIFFReadDirectory(tif));

SensorCall();return(npages);
}

static char DuplexPreamble[] = "\
%%BeginFeature: *Duplex True\n\
systemdict begin\n\
  /languagelevel where { pop languagelevel } { 1 } ifelse\n\
  2 ge { 1 dict dup /Duplex true put setpagedevice }\n\
  { statusdict /setduplex known { statusdict begin setduplex true end } if\n\
  } ifelse\n\
end\n\
%%EndFeature\n\
";

static char TumblePreamble[] = "\
%%BeginFeature: *Tumble True\n\
systemdict begin\n\
  /languagelevel where { pop languagelevel } { 1 } ifelse\n\
  2 ge { 1 dict dup /Tumble true put setpagedevice }\n\
  { statusdict /settumble known { statusdict begin true settumble end } if\n\
  } ifelse\n\
end\n\
%%EndFeature\n\
";

static char AvoidDeadZonePreamble[] = "\
gsave newpath clippath pathbbox grestore\n\
  4 2 roll 2 copy translate\n\
  exch 3 1 roll sub 3 1 roll sub exch\n\
  currentpagedevice /PageSize get aload pop\n\
  exch 3 1 roll div 3 1 roll div abs exch abs\n\
  2 copy gt { exch } if pop\n\
  dup 1 lt { dup scale } { pop } ifelse\n\
";

void
PSHead(FILE *fd, double pagewidth, double pageheight, double xoff, double yoff)
{
	SensorCall();time_t t;

	t = time(0);
	fprintf(fd, "%%!PS-Adobe-3.0%s\n", generateEPSF ? " EPSF-3.0" : "");
	fprintf(fd, "%%%%Creator: %s\n", creator ? creator : "tiff2ps");
        fprintf(fd, "%%%%Title: %s\n", title ? title : filename);
	fprintf(fd, "%%%%CreationDate: %s", ctime(&t));
	fprintf(fd, "%%%%DocumentData: Clean7Bit\n");
	/* NB: should use PageBoundingBox for each page instead of BoundingBox *
         * PageBoundingBox DSC added in PSPageSize function, R Nolde 09-01-2010
         */
	fprintf(fd, "%%%%Origin: %ld %ld\n", (long) xoff, (long) yoff);
        fprintf(fd, "%%%%BoundingBox: 0 0 %ld %ld\n",
	       (long) ceil(pagewidth), (long) ceil(pageheight));

	fprintf(fd, "%%%%LanguageLevel: %d\n", (level3 ? 3 : (level2 ? 2 : 1)));
        SensorCall();if (generateEPSF == TRUE)
	  {/*51*/SensorCall();fprintf(fd, "%%%%Pages: 1 1\n");/*52*/}
        else
	  {/*53*/SensorCall();fprintf(fd, "%%%%Pages: (atend)\n");/*54*/}
	SensorCall();fprintf(fd, "%%%%EndComments\n");
        SensorCall();if (generateEPSF == FALSE)
          {
  	  SensorCall();fprintf(fd, "%%%%BeginSetup\n");
	  SensorCall();if (PSduplex)
		{/*55*/SensorCall();fprintf(fd, "%s", DuplexPreamble);/*56*/}
	  SensorCall();if (PStumble)
		{/*57*/SensorCall();fprintf(fd, "%s", TumblePreamble);/*58*/}
	  SensorCall();if (PSavoiddeadzone && (level2 || level3))
		{/*59*/SensorCall();fprintf(fd, "%s", AvoidDeadZonePreamble);/*60*/}
	  SensorCall();fprintf(fd, "%%%%EndSetup\n");
	  }
SensorCall();}

void
PSTail(FILE *fd, int npages)
{
	SensorCall();fprintf(fd, "%%%%Trailer\n");
        SensorCall();if (generateEPSF == FALSE)
	  {/*61*/SensorCall();fprintf(fd, "%%%%Pages: %d\n", npages);/*62*/}
	SensorCall();fprintf(fd, "%%%%EOF\n");
SensorCall();}

static int
checkcmap(TIFF* tif, int n, uint16* r, uint16* g, uint16* b)
{
	SensorCall();(void) tif;
	SensorCall();while (n-- > 0)
		{/*183*/SensorCall();if (*r++ >= 256 || *g++ >= 256 || *b++ >= 256)
			{/*185*/{int  ReplaceReturn = (16); SensorCall(); return ReplaceReturn;}/*186*/}/*184*/}
	SensorCall();TIFFWarning(filename, "Assuming 8-bit colormap");
	{int  ReplaceReturn = (8); SensorCall(); return ReplaceReturn;}
}

static void
PS_Lvl2colorspace(FILE* fd, TIFF* tif)
{
	SensorCall();uint16 *rmap, *gmap, *bmap;
	int i, num_colors;
	const char * colorspace_p;

	SensorCall();switch ( photometric )
	{
	case PHOTOMETRIC_SEPARATED:
		SensorCall();colorspace_p = "CMYK";
		SensorCall();break;

	case PHOTOMETRIC_RGB:
		SensorCall();colorspace_p = "RGB";
		SensorCall();break;

	default:
		SensorCall();colorspace_p = "Gray";
	}

	/*
	 * Set up PostScript Level 2 colorspace according to
	 * section 4.8 in the PostScript refenence manual.
	 */
	SensorCall();fputs("% PostScript Level 2 only.\n", fd);
	SensorCall();if (photometric != PHOTOMETRIC_PALETTE) {
		SensorCall();if (photometric == PHOTOMETRIC_YCBCR) {
		    /* MORE CODE HERE */
		}
		SensorCall();fprintf(fd, "/Device%s setcolorspace\n", colorspace_p );
		SensorCall();return;
	}

	/*
	 * Set up an indexed/palette colorspace
	 */
	SensorCall();num_colors = (1 << bitspersample);
	SensorCall();if (!TIFFGetField(tif, TIFFTAG_COLORMAP, &rmap, &gmap, &bmap)) {
		SensorCall();TIFFError(filename,
			"Palette image w/o \"Colormap\" tag");
		SensorCall();return;
	}
	SensorCall();if (checkcmap(tif, num_colors, rmap, gmap, bmap) == 16) {
		/*
		 * Convert colormap to 8-bits values.
		 */
#define	CVT(x)		(((x) * 255) / ((1L<<16)-1))
		SensorCall();for (i = 0; i < num_colors; i++) {
			SensorCall();rmap[i] = CVT(rmap[i]);
			gmap[i] = CVT(gmap[i]);
			bmap[i] = CVT(bmap[i]);
		}
#undef CVT
	}
	SensorCall();fprintf(fd, "[ /Indexed /DeviceRGB %d", num_colors - 1);
	SensorCall();if (ascii85) {
		SensorCall();Ascii85Init();
		fputs("\n<~", fd);
		ascii85breaklen -= 2;
	} else
		{/*187*/SensorCall();fputs(" <", fd);/*188*/}
	SensorCall();for (i = 0; i < num_colors; i++) {
		SensorCall();if (ascii85) {
			SensorCall();Ascii85Put((unsigned char)rmap[i], fd);
			Ascii85Put((unsigned char)gmap[i], fd);
			Ascii85Put((unsigned char)bmap[i], fd);
		} else {
			SensorCall();fputs((i % 8) ? " " : "\n  ", fd);
			fprintf(fd, "%02x%02x%02x",
			    rmap[i], gmap[i], bmap[i]);
		}
	}
	SensorCall();if (ascii85)
		{/*189*/SensorCall();Ascii85Flush(fd);/*190*/}
	else
		{/*191*/SensorCall();fputs(">\n", fd);/*192*/}
	SensorCall();fputs("] setcolorspace\n", fd);
SensorCall();}

static int
PS_Lvl2ImageDict(FILE* fd, TIFF* tif, uint32 w, uint32 h)
{
	SensorCall();int use_rawdata;
	uint32 tile_width, tile_height;
	uint16 predictor, minsamplevalue, maxsamplevalue;
	int repeat_count;
	char im_h[64], im_x[64], im_y[64];
	char * imageOp = "image";

	SensorCall();if ( useImagemask && (bitspersample == 1) )
		{/*193*/SensorCall();imageOp = "imagemask";/*194*/}

	SensorCall();(void)strcpy(im_x, "0");
	(void)snprintf(im_y, sizeof(im_y), "%lu", (long) h);
	(void)snprintf(im_h, sizeof(im_h), "%lu", (long) h);
	tile_width = w;
	tile_height = h;
	SensorCall();if (TIFFIsTiled(tif)) {
		SensorCall();repeat_count = TIFFNumberOfTiles(tif);
		TIFFGetField(tif, TIFFTAG_TILEWIDTH, &tile_width);
		TIFFGetField(tif, TIFFTAG_TILELENGTH, &tile_height);
		SensorCall();if (tile_width > w || tile_height > h ||
		    (w % tile_width) != 0 || (h % tile_height != 0)) {
			/*
			 * The tiles does not fit image width and height.
			 * Set up a clip rectangle for the image unit square.
			 */
			SensorCall();fputs("0 0 1 1 rectclip\n", fd);
		}
		SensorCall();if (tile_width < w) {
			SensorCall();fputs("/im_x 0 def\n", fd);
			(void)strcpy(im_x, "im_x neg");
		}
		SensorCall();if (tile_height < h) {
			SensorCall();fputs("/im_y 0 def\n", fd);
			(void)snprintf(im_y, sizeof(im_y), "%lu im_y sub", (unsigned long) h);
		}
	} else {
		SensorCall();repeat_count = tf_numberstrips;
		tile_height = tf_rowsperstrip;
		SensorCall();if (tile_height > h)
			{/*195*/SensorCall();tile_height = h;/*196*/}
		SensorCall();if (repeat_count > 1) {
			SensorCall();fputs("/im_y 0 def\n", fd);
			fprintf(fd, "/im_h %lu def\n",
			    (unsigned long) tile_height);
			(void)strcpy(im_h, "im_h");
			(void)snprintf(im_y, sizeof(im_y), "%lu im_y sub", (unsigned long) h);
		}
	}

	/*
	 * Output start of exec block
	 */
	SensorCall();fputs("{ % exec\n", fd);

	SensorCall();if (repeat_count > 1)
		{/*197*/SensorCall();fprintf(fd, "%d { %% repeat\n", repeat_count);/*198*/}

	/*
	 * Output filter options and image dictionary.
	 */
	SensorCall();if (ascii85)
		{/*199*/SensorCall();fputs(" /im_stream currentfile /ASCII85Decode filter def\n",
		    fd);/*200*/}
	SensorCall();fputs(" <<\n", fd);
	fputs("  /ImageType 1\n", fd);
	fprintf(fd, "  /Width %lu\n", (unsigned long) tile_width);
	/*
	 * Workaround for some software that may crash when last strip
	 * of image contains fewer number of scanlines than specified
	 * by the `/Height' variable. So for stripped images with multiple
	 * strips we will set `/Height' as `im_h', because one is 
	 * recalculated for each strip - including the (smaller) final strip.
	 * For tiled images and images with only one strip `/Height' will
	 * contain number of scanlines in tile (or image height in case of
	 * one-stripped image).
	 */
	SensorCall();if (TIFFIsTiled(tif) || tf_numberstrips == 1)
		{/*201*/SensorCall();fprintf(fd, "  /Height %lu\n", (unsigned long) tile_height);/*202*/}
	else
		{/*203*/SensorCall();fprintf(fd, "  /Height im_h\n");/*204*/}
	
	SensorCall();if (planarconfiguration == PLANARCONFIG_SEPARATE && samplesperpixel > 1)
		{/*205*/SensorCall();fputs("  /MultipleDataSources true\n", fd);/*206*/}
	SensorCall();fprintf(fd, "  /ImageMatrix [ %lu 0 0 %ld %s %s ]\n",
	    (unsigned long) w, - (long)h, im_x, im_y);
	fprintf(fd, "  /BitsPerComponent %d\n", bitspersample);
	fprintf(fd, "  /Interpolate %s\n", interpolate ? "true" : "false");

	SensorCall();switch (samplesperpixel - extrasamples) {
	case 1:
		SensorCall();switch (photometric) {
		case PHOTOMETRIC_MINISBLACK:
			SensorCall();fputs("  /Decode [0 1]\n", fd);
			SensorCall();break;
		case PHOTOMETRIC_MINISWHITE:
			SensorCall();switch (compression) {
			case COMPRESSION_CCITTRLE:
			case COMPRESSION_CCITTRLEW:
			case COMPRESSION_CCITTFAX3:
			case COMPRESSION_CCITTFAX4:
				/*
				 * Manage inverting with /Blackis1 flag
				 * since there migth be uncompressed parts
				 */
				SensorCall();fputs("  /Decode [0 1]\n", fd);
				SensorCall();break;
			default:
				/*
				 * ERROR...
				 */
				SensorCall();fputs("  /Decode [1 0]\n", fd);
				SensorCall();break;
			}
			SensorCall();break;
		case PHOTOMETRIC_PALETTE:
			SensorCall();TIFFGetFieldDefaulted(tif, TIFFTAG_MINSAMPLEVALUE,
			    &minsamplevalue);
			TIFFGetFieldDefaulted(tif, TIFFTAG_MAXSAMPLEVALUE,
			    &maxsamplevalue);
			fprintf(fd, "  /Decode [%u %u]\n",
				    minsamplevalue, maxsamplevalue);
			SensorCall();break;
		default:
			/*
			 * ERROR ?
			 */
			SensorCall();fputs("  /Decode [0 1]\n", fd);
			SensorCall();break;
		}
		SensorCall();break;
	case 3:
		SensorCall();switch (photometric) {
		case PHOTOMETRIC_RGB:
			SensorCall();fputs("  /Decode [0 1 0 1 0 1]\n", fd);
			SensorCall();break;
		case PHOTOMETRIC_MINISWHITE:
		case PHOTOMETRIC_MINISBLACK:
		default:
			/*
			 * ERROR??
			 */
			SensorCall();fputs("  /Decode [0 1 0 1 0 1]\n", fd);
			SensorCall();break;
		}
		SensorCall();break;
	case 4:
		/*
		 * ERROR??
		 */
		SensorCall();fputs("  /Decode [0 1 0 1 0 1 0 1]\n", fd);
		SensorCall();break;
	}
	SensorCall();fputs("  /DataSource", fd);
	SensorCall();if (planarconfiguration == PLANARCONFIG_SEPARATE &&
	    samplesperpixel > 1)
		{/*207*/SensorCall();fputs(" [", fd);/*208*/}
	SensorCall();if (ascii85)
		{/*209*/SensorCall();fputs(" im_stream", fd);/*210*/}
	else
		{/*211*/SensorCall();fputs(" currentfile /ASCIIHexDecode filter", fd);/*212*/}

	SensorCall();use_rawdata = TRUE;
	SensorCall();switch (compression) {
	case COMPRESSION_NONE:		/* 1: uncompressed */
		SensorCall();break;
	case COMPRESSION_CCITTRLE:	/* 2: CCITT modified Huffman RLE */
	case COMPRESSION_CCITTRLEW:	/* 32771: #1 w/ word alignment */
	case COMPRESSION_CCITTFAX3:	/* 3: CCITT Group 3 fax encoding */
	case COMPRESSION_CCITTFAX4:	/* 4: CCITT Group 4 fax encoding */
		SensorCall();fputs("\n\t<<\n", fd);
		SensorCall();if (compression == COMPRESSION_CCITTFAX3) {
			SensorCall();uint32 g3_options;

			fputs("\t /EndOfLine true\n", fd);
			fputs("\t /EndOfBlock false\n", fd);
			SensorCall();if (!TIFFGetField(tif, TIFFTAG_GROUP3OPTIONS,
					    &g3_options))
				{/*213*/SensorCall();g3_options = 0;/*214*/}
			SensorCall();if (g3_options & GROUP3OPT_2DENCODING)
				{/*215*/SensorCall();fprintf(fd, "\t /K %s\n", im_h);/*216*/}
			SensorCall();if (g3_options & GROUP3OPT_UNCOMPRESSED)
				{/*217*/SensorCall();fputs("\t /Uncompressed true\n", fd);/*218*/}
			SensorCall();if (g3_options & GROUP3OPT_FILLBITS)
				{/*219*/SensorCall();fputs("\t /EncodedByteAlign true\n", fd);/*220*/}
		}
		SensorCall();if (compression == COMPRESSION_CCITTFAX4) {
			SensorCall();uint32 g4_options;

			fputs("\t /K -1\n", fd);
			TIFFGetFieldDefaulted(tif, TIFFTAG_GROUP4OPTIONS,
					       &g4_options);
			SensorCall();if (g4_options & GROUP4OPT_UNCOMPRESSED)
				{/*221*/SensorCall();fputs("\t /Uncompressed true\n", fd);/*222*/}
		}
		SensorCall();if (!(tile_width == w && w == 1728U))
			{/*223*/SensorCall();fprintf(fd, "\t /Columns %lu\n",
			    (unsigned long) tile_width);/*224*/}
		SensorCall();fprintf(fd, "\t /Rows %s\n", im_h);
		SensorCall();if (compression == COMPRESSION_CCITTRLE ||
		    compression == COMPRESSION_CCITTRLEW) {
			SensorCall();fputs("\t /EncodedByteAlign true\n", fd);
			fputs("\t /EndOfBlock false\n", fd);
		}
		SensorCall();if (photometric == PHOTOMETRIC_MINISBLACK)
			{/*225*/SensorCall();fputs("\t /BlackIs1 true\n", fd);/*226*/}
		SensorCall();fprintf(fd, "\t>> /CCITTFaxDecode filter");
		SensorCall();break;
	case COMPRESSION_LZW:	/* 5: Lempel-Ziv & Welch */
		SensorCall();TIFFGetFieldDefaulted(tif, TIFFTAG_PREDICTOR, &predictor);
		SensorCall();if (predictor == 2) {
			SensorCall();fputs("\n\t<<\n", fd);
			fprintf(fd, "\t /Predictor %u\n", predictor);
			fprintf(fd, "\t /Columns %lu\n",
			    (unsigned long) tile_width);
			fprintf(fd, "\t /Colors %u\n", samplesperpixel);
			fprintf(fd, "\t /BitsPerComponent %u\n",
			    bitspersample);
			fputs("\t>>", fd);
		}
		SensorCall();fputs(" /LZWDecode filter", fd);
		SensorCall();break;
	case COMPRESSION_DEFLATE:	/* 5: ZIP */
	case COMPRESSION_ADOBE_DEFLATE:
		SensorCall();if ( level3 ) {
			 SensorCall();TIFFGetFieldDefaulted(tif, TIFFTAG_PREDICTOR, &predictor);
			 SensorCall();if (predictor > 1) {
				SensorCall();fprintf(fd, "\t %% PostScript Level 3 only.");
				fputs("\n\t<<\n", fd);
				fprintf(fd, "\t /Predictor %u\n", predictor);
				fprintf(fd, "\t /Columns %lu\n",
					(unsigned long) tile_width);
				fprintf(fd, "\t /Colors %u\n", samplesperpixel);
					fprintf(fd, "\t /BitsPerComponent %u\n",
					bitspersample);
				fputs("\t>>", fd);
			 }
			 SensorCall();fputs(" /FlateDecode filter", fd);
		} else {
			SensorCall();use_rawdata = FALSE ;
		}
		SensorCall();break;
	case COMPRESSION_PACKBITS:	/* 32773: Macintosh RLE */
		SensorCall();fputs(" /RunLengthDecode filter", fd);
		use_rawdata = TRUE;
	    SensorCall();break;
	case COMPRESSION_OJPEG:		/* 6: !6.0 JPEG */
	case COMPRESSION_JPEG:		/* 7: %JPEG DCT compression */
#ifdef notdef
		/*
		 * Code not tested yet
		 */
		fputs(" /DCTDecode filter", fd);
		use_rawdata = TRUE;
#else
		SensorCall();use_rawdata = FALSE;
#endif
		SensorCall();break;
	case COMPRESSION_NEXT:		/* 32766: NeXT 2-bit RLE */
	case COMPRESSION_THUNDERSCAN:	/* 32809: ThunderScan RLE */
	case COMPRESSION_PIXARFILM:	/* 32908: Pixar companded 10bit LZW */
	case COMPRESSION_JBIG:		/* 34661: ISO JBIG */
		SensorCall();use_rawdata = FALSE;
		SensorCall();break;
	case COMPRESSION_SGILOG:	/* 34676: SGI LogL or LogLuv */
	case COMPRESSION_SGILOG24:	/* 34677: SGI 24-bit LogLuv */
		SensorCall();use_rawdata = FALSE;
		SensorCall();break;
	default:
		/*
		 * ERROR...
		 */
		SensorCall();use_rawdata = FALSE;
		SensorCall();break;
	}
	SensorCall();if (planarconfiguration == PLANARCONFIG_SEPARATE &&
	    samplesperpixel > 1) {
		SensorCall();uint16 i;

		/*
		 * NOTE: This code does not work yet...
		 */
		SensorCall();for (i = 1; i < samplesperpixel; i++)
			{/*227*/SensorCall();fputs(" dup", fd);/*228*/}
		SensorCall();fputs(" ]", fd);
	}

	SensorCall();fprintf( fd, "\n >> %s\n", imageOp );
	SensorCall();if (ascii85)
		{/*229*/SensorCall();fputs(" im_stream status { im_stream flushfile } if\n", fd);/*230*/}
	SensorCall();if (repeat_count > 1) {
		SensorCall();if (tile_width < w) {
			SensorCall();fprintf(fd, " /im_x im_x %lu add def\n",
			    (unsigned long) tile_width);
			SensorCall();if (tile_height < h) {
				SensorCall();fprintf(fd, " im_x %lu ge {\n",
				    (unsigned long) w);
				fputs("  /im_x 0 def\n", fd);
				fprintf(fd, " /im_y im_y %lu add def\n",
				    (unsigned long) tile_height);
				fputs(" } if\n", fd);
			}
		}
		SensorCall();if (tile_height < h) {
			SensorCall();if (tile_width >= w) {
				SensorCall();fprintf(fd, " /im_y im_y %lu add def\n",
				    (unsigned long) tile_height);
				SensorCall();if (!TIFFIsTiled(tif)) {
					SensorCall();fprintf(fd, " /im_h %lu im_y sub",
					    (unsigned long) h);
					fprintf(fd, " dup %lu gt { pop",
					    (unsigned long) tile_height);
					fprintf(fd, " %lu } if def\n",
					    (unsigned long) tile_height);
				}
			}
		}
		SensorCall();fputs("} repeat\n", fd);
	}
	/*
	 * End of exec function
	 */
	SensorCall();fputs("}\n", fd);

	SensorCall();return(use_rawdata);
}

/* Flip the byte order of buffers with 16 bit samples */
static void
PS_FlipBytes(unsigned char* buf, tsize_t count)
{
	SensorCall();int i;
	unsigned char temp;

	SensorCall();if (count <= 0 || bitspersample <= 8) {
		SensorCall();return;
	}

	SensorCall();count--;

	SensorCall();for (i = 0; i < count; i += 2) {
		SensorCall();temp = buf[i];
		buf[i] = buf[i + 1];
		buf[i + 1] = temp;
	}
SensorCall();}

#define MAXLINE		36

int
PS_Lvl2page(FILE* fd, TIFF* tif, uint32 w, uint32 h)
{
	SensorCall();uint16 fillorder;
	int use_rawdata, tiled_image, breaklen = MAXLINE;
	uint32 chunk_no, num_chunks;
        uint64 *bc;
	unsigned char *buf_data, *cp;
	tsize_t chunk_size, byte_count;

#if defined( EXP_ASCII85ENCODER )
	tsize_t			ascii85_l;	/* Length, in bytes, of ascii85_p[] data */
	uint8		*	ascii85_p = 0;	/* Holds ASCII85 encoded data */
#endif

	PS_Lvl2colorspace(fd, tif);
	use_rawdata = PS_Lvl2ImageDict(fd, tif, w, h);

/* See http://bugzilla.remotesensing.org/show_bug.cgi?id=80 */
#ifdef ENABLE_BROKEN_BEGINENDDATA
	fputs("%%BeginData:\n", fd);
#endif
	fputs("exec\n", fd);

	tiled_image = TIFFIsTiled(tif);
	SensorCall();if (tiled_image) {
		SensorCall();num_chunks = TIFFNumberOfTiles(tif);
		TIFFGetField(tif, TIFFTAG_TILEBYTECOUNTS, &bc);
	} else {
		SensorCall();num_chunks = TIFFNumberOfStrips(tif);
		TIFFGetField(tif, TIFFTAG_STRIPBYTECOUNTS, &bc);
	}

	SensorCall();if (use_rawdata) {
		SensorCall();chunk_size = (tsize_t) bc[0];
		SensorCall();for (chunk_no = 1; chunk_no < num_chunks; chunk_no++)
			{/*231*/SensorCall();if ((tsize_t) bc[chunk_no] > chunk_size)
				{/*233*/SensorCall();chunk_size = (tsize_t) bc[chunk_no];/*234*/}/*232*/}
	} else {
		SensorCall();if (tiled_image)
			{/*235*/SensorCall();chunk_size = TIFFTileSize(tif);/*236*/}
		else
			{/*237*/SensorCall();chunk_size = TIFFStripSize(tif);/*238*/}
	}
	SensorCall();buf_data = (unsigned char *)_TIFFmalloc(chunk_size);
	SensorCall();if (!buf_data) {
		SensorCall();TIFFError(filename, "Can't alloc %lu bytes for %s.",
			(unsigned long) chunk_size, tiled_image ? "tiles" : "strips");
		SensorCall();return(FALSE);
	}

#if defined( EXP_ASCII85ENCODER )
	SensorCall();if ( ascii85 ) {
	    /*
	     * Allocate a buffer to hold the ASCII85 encoded data.  Note
	     * that it is allocated with sufficient room to hold the
	     * encoded data (5*chunk_size/4) plus the EOD marker (+8)
	     * and formatting line breaks.  The line breaks are more
	     * than taken care of by using 6*chunk_size/4 rather than
	     * 5*chunk_size/4.
	     */

	    SensorCall();ascii85_p = _TIFFmalloc( (chunk_size+(chunk_size/2)) + 8 );

	    SensorCall();if ( !ascii85_p ) {
		SensorCall();_TIFFfree( buf_data );

		TIFFError( filename, "Cannot allocate ASCII85 encoding buffer." );
		{int  ReplaceReturn = ( FALSE ); SensorCall(); return ReplaceReturn;}
	    }
	}
#endif

	SensorCall();TIFFGetFieldDefaulted(tif, TIFFTAG_FILLORDER, &fillorder);
	SensorCall();for (chunk_no = 0; chunk_no < num_chunks; chunk_no++) {
		if (ascii85)
			Ascii85Init();
		else
			breaklen = MAXLINE;
		SensorCall();if (use_rawdata) {
			SensorCall();if (tiled_image)
				{/*243*/SensorCall();byte_count = TIFFReadRawTile(tif, chunk_no,
						  buf_data, chunk_size);/*244*/}
			else
				{/*245*/SensorCall();byte_count = TIFFReadRawStrip(tif, chunk_no,
						  buf_data, chunk_size);/*246*/}
			SensorCall();if (fillorder == FILLORDER_LSB2MSB)
			    {/*247*/SensorCall();TIFFReverseBits(buf_data, byte_count);/*248*/}
		} else {
			SensorCall();if (tiled_image)
				{/*249*/SensorCall();byte_count = TIFFReadEncodedTile(tif,
						chunk_no, buf_data,
						chunk_size);/*250*/}
			else
				{/*251*/SensorCall();byte_count = TIFFReadEncodedStrip(tif,
						chunk_no, buf_data,
						chunk_size);/*252*/}
		}
		SensorCall();if (byte_count < 0) {
			SensorCall();TIFFError(filename, "Can't read %s %d.",
				tiled_image ? "tile" : "strip", chunk_no);
			SensorCall();if (ascii85)
				{/*253*/SensorCall();Ascii85Put('\0', fd);/*254*/}
		}
		/*
		 * for 16 bits, the two bytes must be most significant
		 * byte first
		 */
		SensorCall();if (bitspersample == 16 && !TIFFIsBigEndian(tif)) {
			SensorCall();PS_FlipBytes(buf_data, byte_count);
		}
		/*
		 * For images with alpha, matte against a white background;
		 * i.e. Cback * (1 - Aimage) where Cback = 1. We will fill the
		 * lower part of the buffer with the modified values.
		 *
		 * XXX: needs better solution
		 */
		SensorCall();if (alpha) {
			SensorCall();int adjust, i, j = 0;
			int ncomps = samplesperpixel - extrasamples;
			SensorCall();for (i = 0; i < byte_count; i+=samplesperpixel) {
				SensorCall();adjust = 255 - buf_data[i + ncomps];
				SensorCall();switch (ncomps) {
					case 1:
						SensorCall();buf_data[j++] = buf_data[i] + adjust;
						SensorCall();break;
					case 2:
						SensorCall();buf_data[j++] = buf_data[i] + adjust;
						buf_data[j++] = buf_data[i+1] + adjust;
						SensorCall();break;
					case 3:
						SensorCall();buf_data[j++] = buf_data[i] + adjust;
						buf_data[j++] = buf_data[i+1] + adjust;
						buf_data[j++] = buf_data[i+2] + adjust;
						SensorCall();break;
				}
			}
			SensorCall();byte_count -= j;
		}

		SensorCall();if (ascii85) {
#if defined( EXP_ASCII85ENCODER )
			SensorCall();ascii85_l = Ascii85EncodeBlock(ascii85_p, 1, buf_data, byte_count );

			SensorCall();if ( ascii85_l > 0 )
				{/*255*/SensorCall();fwrite( ascii85_p, ascii85_l, 1, fd );/*256*/}
#else
			for (cp = buf_data; byte_count > 0; byte_count--)
				Ascii85Put(*cp++, fd);
#endif
		}
		else
		{
			SensorCall();for (cp = buf_data; byte_count > 0; byte_count--) {
				putc(hex[((*cp)>>4)&0xf], fd);
				putc(hex[(*cp)&0xf], fd);
				SensorCall();cp++;

				SensorCall();if (--breaklen <= 0) {
					putc('\n', fd);
					SensorCall();breaklen = MAXLINE;
				}
			}
		}

		SensorCall();if ( !ascii85 ) {
			SensorCall();if ( level2 || level3 )
				putc( '>', fd );
			putc('\n', fd);
		}
#if !defined( EXP_ASCII85ENCODER )
		else
			Ascii85Flush(fd);
#endif
	}

#if defined( EXP_ASCII85ENCODER )
	SensorCall();if ( ascii85_p )
	    {/*257*/SensorCall();_TIFFfree( ascii85_p );/*258*/}
#endif
       
	SensorCall();_TIFFfree(buf_data);
#ifdef ENABLE_BROKEN_BEGINENDDATA
	fputs("%%EndData\n", fd);
#endif
	SensorCall();return(TRUE);
}

void
PSpage(FILE* fd, TIFF* tif, uint32 w, uint32 h)
{
	SensorCall();char	*	imageOp = "image";

	SensorCall();if ( useImagemask && (bitspersample == 1) )
		{/*19*/SensorCall();imageOp = "imagemask";/*20*/}

	SensorCall();if ((level2 || level3) && PS_Lvl2page(fd, tif, w, h))
		{/*21*/SensorCall();return;/*22*/}
	SensorCall();ps_bytesperrow = tf_bytesperrow - (extrasamples * bitspersample / 8)*w;
	SensorCall();switch (photometric) {
	case PHOTOMETRIC_RGB:
		SensorCall();if (planarconfiguration == PLANARCONFIG_CONTIG) {
			SensorCall();fprintf(fd, "%s", RGBcolorimage);
			PSColorContigPreamble(fd, w, h, 3);
			PSDataColorContig(fd, tif, w, h, 3);
		} else {
			SensorCall();PSColorSeparatePreamble(fd, w, h, 3);
			PSDataColorSeparate(fd, tif, w, h, 3);
		}
		SensorCall();break;
	case PHOTOMETRIC_SEPARATED:
		/* XXX should emit CMYKcolorimage */
		SensorCall();if (planarconfiguration == PLANARCONFIG_CONTIG) {
			SensorCall();PSColorContigPreamble(fd, w, h, 4);
			PSDataColorContig(fd, tif, w, h, 4);
		} else {
			SensorCall();PSColorSeparatePreamble(fd, w, h, 4);
			PSDataColorSeparate(fd, tif, w, h, 4);
		}
		SensorCall();break;
	case PHOTOMETRIC_PALETTE:
		SensorCall();fprintf(fd, "%s", RGBcolorimage);
		PhotoshopBanner(fd, w, h, 1, 3, "false 3 colorimage");
		fprintf(fd, "/scanLine %ld string def\n",
		    (long) ps_bytesperrow * 3L);
		fprintf(fd, "%lu %lu 8\n",
		    (unsigned long) w, (unsigned long) h);
		fprintf(fd, "[%lu 0 0 -%lu 0 %lu]\n",
		    (unsigned long) w, (unsigned long) h, (unsigned long) h);
		fprintf(fd, "{currentfile scanLine readhexstring pop} bind\n");
		fprintf(fd, "false 3 colorimage\n");
		PSDataPalette(fd, tif, w, h);
		SensorCall();break;
	case PHOTOMETRIC_MINISBLACK:
	case PHOTOMETRIC_MINISWHITE:
		SensorCall();PhotoshopBanner(fd, w, h, 1, 1, imageOp);
		fprintf(fd, "/scanLine %ld string def\n",
		    (long) ps_bytesperrow);
		fprintf(fd, "%lu %lu %d\n",
		    (unsigned long) w, (unsigned long) h, bitspersample);
		fprintf(fd, "[%lu 0 0 -%lu 0 %lu]\n",
		    (unsigned long) w, (unsigned long) h, (unsigned long) h);
		fprintf(fd,
		    "{currentfile scanLine readhexstring pop} bind\n");
		fprintf(fd, "%s\n", imageOp);
		PSDataBW(fd, tif, w, h);
		SensorCall();break;
	}
	putc('\n', fd);
}

void
PSColorContigPreamble(FILE* fd, uint32 w, uint32 h, int nc)
{
	SensorCall();ps_bytesperrow = nc * (tf_bytesperrow / samplesperpixel);
	PhotoshopBanner(fd, w, h, 1, nc, "false %d colorimage");
	fprintf(fd, "/line %ld string def\n", (long) ps_bytesperrow);
	fprintf(fd, "%lu %lu %d\n",
	    (unsigned long) w, (unsigned long) h, bitspersample);
	fprintf(fd, "[%lu 0 0 -%lu 0 %lu]\n",
	    (unsigned long) w, (unsigned long) h, (unsigned long) h);
	fprintf(fd, "{currentfile line readhexstring pop} bind\n");
	fprintf(fd, "false %d colorimage\n", nc);
SensorCall();}

void
PSColorSeparatePreamble(FILE* fd, uint32 w, uint32 h, int nc)
{
	SensorCall();int i;

	PhotoshopBanner(fd, w, h, ps_bytesperrow, nc, "true %d colorimage");
	SensorCall();for (i = 0; i < nc; i++)
		{/*23*/SensorCall();fprintf(fd, "/line%d %ld string def\n",
		    i, (long) ps_bytesperrow);/*24*/}
	SensorCall();fprintf(fd, "%lu %lu %d\n",
	    (unsigned long) w, (unsigned long) h, bitspersample);
	fprintf(fd, "[%lu 0 0 -%lu 0 %lu] \n",
	    (unsigned long) w, (unsigned long) h, (unsigned long) h);
	SensorCall();for (i = 0; i < nc; i++)
		{/*25*/SensorCall();fprintf(fd, "{currentfile line%d readhexstring pop}bind\n", i);/*26*/}
	SensorCall();fprintf(fd, "true %d colorimage\n", nc);
SensorCall();}

#define	DOBREAK(len, howmany, fd) \
	if (((len) -= (howmany)) <= 0) {	\
		putc('\n', fd);			\
		(len) = MAXLINE-(howmany);	\
	}
#define	PUTHEX(c,fd)	putc(hex[((c)>>4)&0xf],fd); putc(hex[(c)&0xf],fd)

void
PSDataColorContig(FILE* fd, TIFF* tif, uint32 w, uint32 h, int nc)
{
	SensorCall();uint32 row;
	int breaklen = MAXLINE, es = samplesperpixel - nc;
	tsize_t cc;
	unsigned char *tf_buf;
	unsigned char *cp, c;

	(void) w;
	tf_buf = (unsigned char *) _TIFFmalloc(tf_bytesperrow);
	SensorCall();if (tf_buf == NULL) {
		SensorCall();TIFFError(filename, "No space for scanline buffer");
		SensorCall();return;
	}
	SensorCall();for (row = 0; row < h; row++) {
		SensorCall();if (TIFFReadScanline(tif, tf_buf, row, 0) < 0)
			{/*27*/SensorCall();break;/*28*/}
		SensorCall();cp = tf_buf;
		/*
		 * for 16 bits, the two bytes must be most significant
		 * byte first
		 */
		SensorCall();if (bitspersample == 16 && !HOST_BIGENDIAN) {
			SensorCall();PS_FlipBytes(cp, tf_bytesperrow);
		}
		SensorCall();if (alpha) {
			SensorCall();int adjust;
			cc = 0;
			SensorCall();for (; cc < tf_bytesperrow; cc += samplesperpixel) {
				DOBREAK(breaklen, nc, fd);
				/*
				 * For images with alpha, matte against
				 * a white background; i.e.
				 *    Cback * (1 - Aimage)
				 * where Cback = 1.
				 */
				SensorCall();adjust = 255 - cp[nc];
				SensorCall();switch (nc) {
				case 4: SensorCall();c = *cp++ + adjust; PUTHEX(c,fd);
				case 3: SensorCall();c = *cp++ + adjust; PUTHEX(c,fd);
				case 2: SensorCall();c = *cp++ + adjust; PUTHEX(c,fd);
				case 1: SensorCall();c = *cp++ + adjust; PUTHEX(c,fd);
				}
				SensorCall();cp += es;
			}
		} else {
			SensorCall();cc = 0;
			SensorCall();for (; cc < tf_bytesperrow; cc += samplesperpixel) {
				DOBREAK(breaklen, nc, fd);
				SensorCall();switch (nc) {
				case 4: SensorCall();c = *cp++; PUTHEX(c,fd);
				case 3: SensorCall();c = *cp++; PUTHEX(c,fd);
				case 2: SensorCall();c = *cp++; PUTHEX(c,fd);
				case 1: SensorCall();c = *cp++; PUTHEX(c,fd);
				}
				SensorCall();cp += es;
			}
		}
	}
	SensorCall();_TIFFfree((char *) tf_buf);
SensorCall();}

void
PSDataColorSeparate(FILE* fd, TIFF* tif, uint32 w, uint32 h, int nc)
{
	SensorCall();uint32 row;
	int breaklen = MAXLINE;
	tsize_t cc;
	tsample_t s, maxs;
	unsigned char *tf_buf;
	unsigned char *cp, c;

	(void) w;
	tf_buf = (unsigned char *) _TIFFmalloc(tf_bytesperrow);
	SensorCall();if (tf_buf == NULL) {
		SensorCall();TIFFError(filename, "No space for scanline buffer");
		SensorCall();return;
	}
	SensorCall();maxs = (samplesperpixel > nc ? nc : samplesperpixel);
	SensorCall();for (row = 0; row < h; row++) {
		SensorCall();for (s = 0; s < maxs; s++) {
			SensorCall();if (TIFFReadScanline(tif, tf_buf, row, s) < 0)
				{/*29*/SensorCall();break;/*30*/}
			SensorCall();for (cp = tf_buf, cc = 0; cc < tf_bytesperrow; cc++) {
				DOBREAK(breaklen, 1, fd);
				SensorCall();c = *cp++;
				PUTHEX(c,fd);
			}
		}
	}
	SensorCall();_TIFFfree((char *) tf_buf);
SensorCall();}

#define	PUTRGBHEX(c,fd) \
	PUTHEX(rmap[c],fd); PUTHEX(gmap[c],fd); PUTHEX(bmap[c],fd)

void
PSDataPalette(FILE* fd, TIFF* tif, uint32 w, uint32 h)
{
	SensorCall();uint16 *rmap, *gmap, *bmap;
	uint32 row;
	int breaklen = MAXLINE, nc;
	tsize_t cc;
	unsigned char *tf_buf;
	unsigned char *cp, c;

	(void) w;
	SensorCall();if (!TIFFGetField(tif, TIFFTAG_COLORMAP, &rmap, &gmap, &bmap)) {
		SensorCall();TIFFError(filename, "Palette image w/o \"Colormap\" tag");
		SensorCall();return;
	}
	SensorCall();switch (bitspersample) {
	case 8:	case 4: case 2: case 1:
		SensorCall();break;
	default:
		SensorCall();TIFFError(filename, "Depth %d not supported", bitspersample);
		SensorCall();return;
	}
	SensorCall();nc = 3 * (8 / bitspersample);
	tf_buf = (unsigned char *) _TIFFmalloc(tf_bytesperrow);
	SensorCall();if (tf_buf == NULL) {
		SensorCall();TIFFError(filename, "No space for scanline buffer");
		SensorCall();return;
	}
	SensorCall();if (checkcmap(tif, 1<<bitspersample, rmap, gmap, bmap) == 16) {
		SensorCall();int i;
#define	CVT(x)		((unsigned short) (((x) * 255) / ((1U<<16)-1)))
		SensorCall();for (i = (1<<bitspersample)-1; i >= 0; i--) {
			SensorCall();rmap[i] = CVT(rmap[i]);
			gmap[i] = CVT(gmap[i]);
			bmap[i] = CVT(bmap[i]);
		}
#undef CVT
	}
	SensorCall();for (row = 0; row < h; row++) {
		SensorCall();if (TIFFReadScanline(tif, tf_buf, row, 0) < 0)
			{/*31*/SensorCall();break;/*32*/}
		SensorCall();for (cp = tf_buf, cc = 0; cc < tf_bytesperrow; cc++) {
			DOBREAK(breaklen, nc, fd);
			SensorCall();switch (bitspersample) {
			case 8:
				SensorCall();c = *cp++; PUTRGBHEX(c, fd);
				SensorCall();break;
			case 4:
				SensorCall();c = *cp++; PUTRGBHEX(c&0xf, fd);
				c >>= 4;   PUTRGBHEX(c, fd);
				SensorCall();break;
			case 2:
				SensorCall();c = *cp++; PUTRGBHEX(c&0x3, fd);
				c >>= 2;   PUTRGBHEX(c&0x3, fd);
				c >>= 2;   PUTRGBHEX(c&0x3, fd);
				c >>= 2;   PUTRGBHEX(c, fd);
				SensorCall();break;
			case 1:
				SensorCall();c = *cp++; PUTRGBHEX(c&0x1, fd);
				c >>= 1;   PUTRGBHEX(c&0x1, fd);
				c >>= 1;   PUTRGBHEX(c&0x1, fd);
				c >>= 1;   PUTRGBHEX(c&0x1, fd);
				c >>= 1;   PUTRGBHEX(c&0x1, fd);
				c >>= 1;   PUTRGBHEX(c&0x1, fd);
				c >>= 1;   PUTRGBHEX(c&0x1, fd);
				c >>= 1;   PUTRGBHEX(c, fd);
				SensorCall();break;
			}
		}
	}
	SensorCall();_TIFFfree((char *) tf_buf);
SensorCall();}

void
PSDataBW(FILE* fd, TIFF* tif, uint32 w, uint32 h)
{
	SensorCall();int breaklen = MAXLINE;
	unsigned char* tf_buf;
	unsigned char* cp;
	tsize_t stripsize = TIFFStripSize(tif);
	tstrip_t s;

#if defined( EXP_ASCII85ENCODER )
	tsize_t	ascii85_l;		/* Length, in bytes, of ascii85_p[] data */
	uint8	*ascii85_p = 0;		/* Holds ASCII85 encoded data */
#endif

	(void) w; (void) h;
	tf_buf = (unsigned char *) _TIFFmalloc(stripsize);
	SensorCall();if (tf_buf == NULL) {
		SensorCall();TIFFError(filename, "No space for scanline buffer");
		SensorCall();return;
	}

	// FIXME
	SensorCall();memset(tf_buf, 0, stripsize);

#if defined( EXP_ASCII85ENCODER )
	SensorCall();if ( ascii85 ) {
	    /*
	     * Allocate a buffer to hold the ASCII85 encoded data.  Note
	     * that it is allocated with sufficient room to hold the
	     * encoded data (5*stripsize/4) plus the EOD marker (+8)
	     * and formatting line breaks.  The line breaks are more
	     * than taken care of by using 6*stripsize/4 rather than
	     * 5*stripsize/4.
	     */

	    SensorCall();ascii85_p = _TIFFmalloc( (stripsize+(stripsize/2)) + 8 );

	    SensorCall();if ( !ascii85_p ) {
		SensorCall();_TIFFfree( tf_buf );

		TIFFError( filename, "Cannot allocate ASCII85 encoding buffer." );
		SensorCall();return;
	    }
	}
#endif

	SensorCall();if (ascii85)
		{/*33*/SensorCall();Ascii85Init();/*34*/}

	SensorCall();for (s = 0; s < TIFFNumberOfStrips(tif); s++) {
		SensorCall();tmsize_t cc = TIFFReadEncodedStrip(tif, s, tf_buf, stripsize);
		SensorCall();if (cc < 0) {
			SensorCall();TIFFError(filename, "Can't read strip");
			SensorCall();break;
		}
		SensorCall();cp = tf_buf;
		SensorCall();if (photometric == PHOTOMETRIC_MINISWHITE) {
			SensorCall();for (cp += cc; --cp >= tf_buf;)
				{/*35*/SensorCall();*cp = ~*cp;/*36*/}
			SensorCall();cp++;
		}
		/*
		 * for 16 bits, the two bytes must be most significant
		 * byte first
		 */
		SensorCall();if (bitspersample == 16 && !HOST_BIGENDIAN) {
			SensorCall();PS_FlipBytes(cp, cc);
		}
		SensorCall();if (ascii85) {
#if defined( EXP_ASCII85ENCODER )
			SensorCall();if (alpha) {
				SensorCall();int adjust, i;
				SensorCall();for (i = 0; i < cc; i+=2) {
					SensorCall();adjust = 255 - cp[i + 1];
				    cp[i / 2] = cp[i] + adjust;
				}
				SensorCall();cc /= 2;
			}

			SensorCall();ascii85_l = Ascii85EncodeBlock( ascii85_p, 1, cp, cc );

			SensorCall();if ( ascii85_l > 0 )
			    {/*37*/SensorCall();fwrite( ascii85_p, ascii85_l, 1, fd );/*38*/}
#else
			while (cc-- > 0)
				Ascii85Put(*cp++, fd);
#endif /* EXP_ASCII85_ENCODER */
		} else {
			SensorCall();unsigned char c;

			SensorCall();if (alpha) {
				SensorCall();int adjust;
				SensorCall();while (cc-- > 0) {
					DOBREAK(breaklen, 1, fd);
					/*
					 * For images with alpha, matte against
					 * a white background; i.e.
					 *    Cback * (1 - Aimage)
					 * where Cback = 1.
					 */
					SensorCall();adjust = 255 - cp[1];
					c = *cp++ + adjust; PUTHEX(c,fd);
					cp++, cc--;
				}
			} else {
				SensorCall();while (cc-- > 0) {
					SensorCall();c = *cp++;
					DOBREAK(breaklen, 1, fd);
					PUTHEX(c, fd);
				}
			}
		}
	}

	SensorCall();if ( !ascii85 )
	{
	    SensorCall();if ( level2 || level3)
		{/*39*/SensorCall();fputs(">\n", fd);/*40*/}
	}
#if !defined( EXP_ASCII85ENCODER )
	else
	    Ascii85Flush(fd);
#else
	SensorCall();if ( ascii85_p )
	    {/*41*/SensorCall();_TIFFfree( ascii85_p );/*42*/}
#endif

	SensorCall();_TIFFfree(tf_buf);
SensorCall();}

void
PSRawDataBW(FILE* fd, TIFF* tif, uint32 w, uint32 h)
{
	SensorCall();uint64 *bc;
	uint32 bufsize;
	int breaklen = MAXLINE;
	tmsize_t cc;
	uint16 fillorder;
	unsigned char *tf_buf;
	unsigned char *cp, c;
	tstrip_t s;

#if defined( EXP_ASCII85ENCODER )
	tsize_t 		ascii85_l;		/* Length, in bytes, of ascii85_p[] data */
	uint8		*	ascii85_p = 0;		/* Holds ASCII85 encoded data */
#endif

	(void) w; (void) h;
	TIFFGetFieldDefaulted(tif, TIFFTAG_FILLORDER, &fillorder);
	TIFFGetField(tif, TIFFTAG_STRIPBYTECOUNTS, &bc);

	/*
	 * Find largest strip:
	 */

	bufsize = (uint32) bc[0];

	SensorCall();for ( s = 0; ++s < (tstrip_t)tf_numberstrips; ) {
		SensorCall();if ( bc[s] > bufsize )
			{/*43*/SensorCall();bufsize = (uint32) bc[s];/*44*/}
	}

	SensorCall();tf_buf = (unsigned char*) _TIFFmalloc(bufsize);
	SensorCall();if (tf_buf == NULL) {
		SensorCall();TIFFError(filename, "No space for strip buffer");
		SensorCall();return;
	}

#if defined( EXP_ASCII85ENCODER )
	SensorCall();if ( ascii85 ) {
	    /*
	     * Allocate a buffer to hold the ASCII85 encoded data.  Note
	     * that it is allocated with sufficient room to hold the
	     * encoded data (5*bufsize/4) plus the EOD marker (+8)
	     * and formatting line breaks.  The line breaks are more
	     * than taken care of by using 6*bufsize/4 rather than
	     * 5*bufsize/4.
	     */

	    SensorCall();ascii85_p = _TIFFmalloc( (bufsize+(bufsize/2)) + 8 );

	    SensorCall();if ( !ascii85_p ) {
		SensorCall();_TIFFfree( tf_buf );

		TIFFError( filename, "Cannot allocate ASCII85 encoding buffer." );
		SensorCall();return;
	    }
	}
#endif

	SensorCall();for (s = 0; s < (tstrip_t) tf_numberstrips; s++) {
		SensorCall();cc = TIFFReadRawStrip(tif, s, tf_buf, (tmsize_t) bc[s]);
		SensorCall();if (cc < 0) {
			SensorCall();TIFFError(filename, "Can't read strip");
			SensorCall();break;
		}
		SensorCall();if (fillorder == FILLORDER_LSB2MSB)
			{/*45*/SensorCall();TIFFReverseBits(tf_buf, cc);/*46*/}
		SensorCall();if (!ascii85) {
			SensorCall();for (cp = tf_buf; cc > 0; cc--) {
				DOBREAK(breaklen, 1, fd);
				SensorCall();c = *cp++;
				PUTHEX(c, fd);
			}
			SensorCall();fputs(">\n", fd);
			breaklen = MAXLINE;
		} else {
			SensorCall();Ascii85Init();
#if defined( EXP_ASCII85ENCODER )
			ascii85_l = Ascii85EncodeBlock( ascii85_p, 1, tf_buf, cc );

			SensorCall();if ( ascii85_l > 0 )
				{/*47*/SensorCall();fwrite( ascii85_p, ascii85_l, 1, fd );/*48*/}
#else
			for (cp = tf_buf; cc > 0; cc--)
				Ascii85Put(*cp++, fd);
			Ascii85Flush(fd);
#endif	/* EXP_ASCII85ENCODER */
		}
	}
	SensorCall();_TIFFfree((char *) tf_buf);

#if defined( EXP_ASCII85ENCODER )
	SensorCall();if ( ascii85_p )
		{/*49*/SensorCall();_TIFFfree( ascii85_p );/*50*/}
#endif
SensorCall();}

void
Ascii85Init(void)
{
	SensorCall();ascii85breaklen = 2*MAXLINE;
	ascii85count = 0;
SensorCall();}

static char*
Ascii85Encode(unsigned char* raw)
{
	SensorCall();static char encoded[6];
	uint32 word;

	word = (((raw[0]<<8)+raw[1])<<16) + (raw[2]<<8) + raw[3];
	SensorCall();if (word != 0L) {
		SensorCall();uint32 q;
		uint16 w1;

		q = word / (85L*85*85*85);	/* actually only a byte */
		encoded[0] = (char) (q + '!');

		word -= q * (85L*85*85*85); q = word / (85L*85*85);
		encoded[1] = (char) (q + '!');

		word -= q * (85L*85*85); q = word / (85*85);
		encoded[2] = (char) (q + '!');

		w1 = (uint16) (word - q*(85L*85));
		encoded[3] = (char) ((w1 / 85) + '!');
		encoded[4] = (char) ((w1 % 85) + '!');
		encoded[5] = '\0';
	} else
		{/*259*/SensorCall();encoded[0] = 'z', encoded[1] = '\0';/*260*/}
	{char * ReplaceReturn = (encoded); SensorCall(); return ReplaceReturn;}
}

void
Ascii85Put(unsigned char code, FILE* fd)
{
	SensorCall();ascii85buf[ascii85count++] = code;
	SensorCall();if (ascii85count >= 4) {
		SensorCall();unsigned char* p;
		int n;

		SensorCall();for (n = ascii85count, p = ascii85buf; n >= 4; n -= 4, p += 4) {
			SensorCall();char* cp;
			SensorCall();for (cp = Ascii85Encode(p); *cp; cp++) {
				putc(*cp, fd);
				SensorCall();if (--ascii85breaklen == 0) {
					putc('\n', fd);
					SensorCall();ascii85breaklen = 2*MAXLINE;
				}
			}
		}
		SensorCall();_TIFFmemcpy(ascii85buf, p, n);
		ascii85count = n;
	}
SensorCall();}

void
Ascii85Flush(FILE* fd)
{
	SensorCall();if (ascii85count > 0) {
		SensorCall();char* res;
		_TIFFmemset(&ascii85buf[ascii85count], 0, 3);
		res = Ascii85Encode(ascii85buf);
		fwrite(res[0] == 'z' ? "!!!!" : res, ascii85count + 1, 1, fd);
	}
	SensorCall();fputs("~>\n", fd);
SensorCall();}
#if	defined( EXP_ASCII85ENCODER)

#define A85BREAKCNTR    ascii85breaklen
#define A85BREAKLEN     (2*MAXLINE)

/*****************************************************************************
*
* Name:         Ascii85EncodeBlock( ascii85_p, f_eod, raw_p, raw_l )
*
* Description:  This routine will encode the raw data in the buffer described
*               by raw_p and raw_l into ASCII85 format and store the encoding
*               in the buffer given by ascii85_p.
*
* Parameters:   ascii85_p   -   A buffer supplied by the caller which will
*                               contain the encoded ASCII85 data.
*               f_eod       -   Flag: Nz means to end the encoded buffer with
*                               an End-Of-Data marker.
*               raw_p       -   Pointer to the buffer of data to be encoded
*               raw_l       -   Number of bytes in raw_p[] to be encoded
*
* Returns:      (int)   <   0   Error, see errno
*                       >=  0   Number of bytes written to ascii85_p[].
*
* Notes:        An external variable given by A85BREAKCNTR is used to
*               determine when to insert newline characters into the
*               encoded data.  As each byte is placed into ascii85_p this
*               external is decremented.  If the variable is decrement to
*               or past zero then a newline is inserted into ascii85_p
*               and the A85BREAKCNTR is then reset to A85BREAKLEN.
*                   Note:  for efficiency reasons the A85BREAKCNTR variable
*                          is not actually checked on *every* character
*                          placed into ascii85_p but often only for every
*                          5 characters.
*
*               THE CALLER IS RESPONSIBLE FOR ENSURING THAT ASCII85_P[] IS
*               SUFFICIENTLY LARGE TO THE ENCODED DATA!
*                   You will need at least 5 * (raw_l/4) bytes plus space for
*                   newline characters and space for an EOD marker (if
*                   requested).  A safe calculation is to use 6*(raw_l/4) + 8
*                   to size ascii85_p.
*
*****************************************************************************/

tsize_t Ascii85EncodeBlock( uint8 * ascii85_p, unsigned f_eod, const uint8 * raw_p, tsize_t raw_l )

{
    SensorCall();char                        ascii85[5];     /* Encoded 5 tuple */
    tsize_t                     ascii85_l;      /* Number of bytes written to ascii85_p[] */
    int                         rc;             /* Return code */
    uint32                      val32;          /* Unencoded 4 tuple */

    ascii85_l = 0;                              /* Nothing written yet */

    SensorCall();if ( raw_p )
    {
        SensorCall();--raw_p;                                /* Prepare for pre-increment fetches */

        SensorCall();for ( ; raw_l > 3; raw_l -= 4 )
        {
            SensorCall();val32  = *(++raw_p) << 24;
            val32 += *(++raw_p) << 16;
            val32 += *(++raw_p) <<  8;
            val32 += *(++raw_p);
    
            SensorCall();if ( val32 == 0 )                   /* Special case */
            {
                SensorCall();ascii85_p[ascii85_l] = 'z';
                rc = 1;
            }
    
            else
            {
                SensorCall();ascii85[4] = (char) ((val32 % 85) + 33);
                val32 /= 85;
    
                ascii85[3] = (char) ((val32 % 85) + 33);
                val32 /= 85;
    
                ascii85[2] = (char) ((val32 % 85) + 33);
                val32 /= 85;
    
                ascii85[1] = (char) ((val32 % 85) + 33);
                ascii85[0] = (char) ((val32 / 85) + 33);

                _TIFFmemcpy( &ascii85_p[ascii85_l], ascii85, sizeof(ascii85) );
                rc = sizeof(ascii85);
            }
    
            SensorCall();ascii85_l += rc;
    
            SensorCall();if ( (A85BREAKCNTR -= rc) <= 0 )
            {
                SensorCall();ascii85_p[ascii85_l] = '\n';
                ++ascii85_l;
                A85BREAKCNTR = A85BREAKLEN;
            }
        }
    
        /*
         * Output any straggler bytes:
         */
    
        SensorCall();if ( raw_l > 0 )
        {
            SensorCall();tsize_t         len;                /* Output this many bytes */
    
            len = raw_l + 1;
            val32 = *++raw_p << 24;             /* Prime the pump */
    
            SensorCall();if ( --raw_l > 0 )  {/*111*/SensorCall();val32 += *(++raw_p) << 16;/*112*/}
            SensorCall();if ( --raw_l > 0 )  {/*113*/SensorCall();val32 += *(++raw_p) <<  8;/*114*/}
    
            SensorCall();val32 /= 85;
    
            ascii85[3] = (char) ((val32 % 85) + 33);
            val32 /= 85;
    
            ascii85[2] = (char) ((val32 % 85) + 33);
            val32 /= 85;
    
            ascii85[1] = (char) ((val32 % 85) + 33);
            ascii85[0] = (char) ((val32 / 85) + 33);
    
            _TIFFmemcpy( &ascii85_p[ascii85_l], ascii85, len );
            ascii85_l += len;
        }
    }

    /*
     * If requested add an ASCII85 End Of Data marker:
     */

    SensorCall();if ( f_eod )
    {
        SensorCall();ascii85_p[ascii85_l++] = '~';
        ascii85_p[ascii85_l++] = '>';
        ascii85_p[ascii85_l++] = '\n';
    }

    {tsize_t  ReplaceReturn = ( ascii85_l ); SensorCall(); return ReplaceReturn;}

}   /* Ascii85EncodeBlock() */

#endif	/* EXP_ASCII85ENCODER */


char* stuff[] = {
"usage: tiff2ps [options] input.tif ...",
"where options are:",
" -1            generate PostScript Level 1 (default)",
" -2            generate PostScript Level 2",
" -3            generate PostScript Level 3",
" -8            disable use of ASCII85 encoding with PostScript Level 2/3",
" -a            convert all directories in file (default is first), Not EPS",
" -b #          set the bottom margin to # inches",
" -c            center image (-b and -l still add to this)",
" -d #          set initial directory to # counting from zero",
" -D            enable duplex printing (two pages per sheet of paper)",
" -e            generate Encapsulated PostScript (EPS) (implies -z)",
" -h #          set printed page height to # inches (no default)",
" -w #          set printed page width to # inches (no default)",
" -H #          split image if height is more than # inches",
" -P L or P     set optional PageOrientation DSC comment to Landscape or Portrait",
" -W #          split image if width is more than # inches",
" -L #          overLap split images by # inches",
" -i #          enable/disable (Nz/0) pixel interpolation (default: enable)",
" -l #          set the left margin to # inches",
" -m            use \"imagemask\" operator instead of \"image\"",
" -o #          convert directory at file offset # bytes",
" -O file       write PostScript to file instead of standard output",
" -p            generate regular PostScript",
" -r # or auto  rotate by 90, 180, 270 degrees or auto",
" -s            generate PostScript for a single image",
" -t name       set postscript document title. Otherwise the filename is used",
" -T            print pages for top edge binding",
" -x            override resolution units as centimeters",
" -y            override resolution units as inches",
" -z            enable printing in the deadzone (only for PostScript Level 2/3)",
NULL
};

static void
usage(int code)
{
	SensorCall();char buf[BUFSIZ];
	int i;

	setbuf(stderr, buf);
        fprintf(stderr, "%s\n\n", TIFFGetVersion());
	SensorCall();for (i = 0; stuff[i] != NULL; i++)
		{/*115*/SensorCall();fprintf(stderr, "%s\n", stuff[i]);/*116*/}
	SensorCall();exit(code);
SensorCall();}

