/* $Id: rgb2ycbcr.c,v 1.14 2011-05-31 17:03:16 bfriesen Exp $ */

/*
 * Copyright (c) 1991-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

#include "tif_config.h"
#include "/var/tmp/sensor.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifdef NEED_LIBPORT
# include "libport.h"
#endif

#include "tiffiop.h"
#include "tiffio.h"

#define	streq(a,b)	(strcmp(a,b) == 0)
#define	CopyField(tag, v) \
    if (TIFFGetField(in, tag, &v)) TIFFSetField(out, tag, v)

#ifndef howmany
#define	howmany(x, y)	(((x)+((y)-1))/(y))
#endif
#define	roundup(x, y)	(howmany(x,y)*((uint32)(y)))

#define	LumaRed		ycbcrCoeffs[0]
#define	LumaGreen	ycbcrCoeffs[1]
#define	LumaBlue	ycbcrCoeffs[2]

uint16	compression = COMPRESSION_PACKBITS;
uint32	rowsperstrip = (uint32) -1;

uint16	horizSubSampling = 2;		/* YCbCr horizontal subsampling */
uint16	vertSubSampling = 2;		/* YCbCr vertical subsampling */
float	ycbcrCoeffs[3] = { .299F, .587F, .114F };
/* default coding range is CCIR Rec 601-1 with no headroom/footroom */
float	refBlackWhite[6] = { 0.F, 255.F, 128.F, 255.F, 128.F, 255.F };

static	int tiffcvt(TIFF* in, TIFF* out);
static	void usage(int code);
static	void setupLumaTables(void);

int
main(int argc, char* argv[])
{
	SensorCall();TIFF *in, *out;
	int c;
	extern int optind;
	extern char *optarg;

	SensorCall();while ((c = getopt(argc, argv, "c:h:r:v:z")) != -1)
		{/*5*/SensorCall();switch (c) {
		case 'c':
			SensorCall();if (streq(optarg, "none"))
			    compression = COMPRESSION_NONE;
			else {/*7*/SensorCall();if (streq(optarg, "packbits"))
			    compression = COMPRESSION_PACKBITS;
			else {/*9*/SensorCall();if (streq(optarg, "lzw"))
			    compression = COMPRESSION_LZW;
			else {/*11*/SensorCall();if (streq(optarg, "jpeg"))
			    compression = COMPRESSION_JPEG;
			else {/*13*/SensorCall();if (streq(optarg, "zip"))
			    compression = COMPRESSION_ADOBE_DEFLATE;
			else
			    {/*15*/SensorCall();usage(-1);/*16*/}/*14*/}/*12*/}/*10*/}/*8*/}
			SensorCall();break;
		case 'h':
			SensorCall();horizSubSampling = atoi(optarg);
			SensorCall();break;
		case 'v':
			SensorCall();vertSubSampling = atoi(optarg);
			SensorCall();break;
		case 'r':
			SensorCall();rowsperstrip = atoi(optarg);
			SensorCall();break;
		case 'z':	/* CCIR Rec 601-1 w/ headroom/footroom */
			SensorCall();refBlackWhite[0] = 16.;
			refBlackWhite[1] = 235.;
			refBlackWhite[2] = 128.;
			refBlackWhite[3] = 240.;
			refBlackWhite[4] = 128.;
			refBlackWhite[5] = 240.;
			SensorCall();break;
		case '?':
			SensorCall();usage(0);
			/*NOTREACHED*/
		;/*6*/}}
	SensorCall();if (argc - optind < 2)
		{/*17*/SensorCall();usage(-1);/*18*/}
	SensorCall();out = TIFFOpen(argv[argc-1], "w");
	SensorCall();if (out == NULL)
		{/*19*/{int  ReplaceReturn = (-2); SensorCall(); return ReplaceReturn;}/*20*/}
	SensorCall();setupLumaTables();
	SensorCall();for (; optind < argc-1; optind++) {
		SensorCall();in = TIFFOpen(argv[optind], "r");
		SensorCall();if (in != NULL) {
			SensorCall();do {
				SensorCall();if (!tiffcvt(in, out) ||
				    !TIFFWriteDirectory(out)) {
					SensorCall();(void) TIFFClose(out);
					{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
				}
			} while (TIFFReadDirectory(in));
			SensorCall();(void) TIFFClose(in);
		}
	}
	SensorCall();(void) TIFFClose(out);
	{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
}

float	*lumaRed;
float	*lumaGreen;
float	*lumaBlue;
float	D1, D2;
int	Yzero;

static float*
setupLuma(float c)
{
	SensorCall();float *v = (float *)_TIFFmalloc(256 * sizeof (float));
	int i;
	SensorCall();for (i = 0; i < 256; i++)
		{/*21*/SensorCall();v[i] = c * i;/*22*/}
	{float * ReplaceReturn = (v); SensorCall(); return ReplaceReturn;}
}

static unsigned
V2Code(float f, float RB, float RW, int CR)
{
	SensorCall();unsigned int c = (unsigned int)((((f)*(RW-RB)/CR)+RB)+.5);
	{unsigned int  ReplaceReturn = (c > 255 ? 255 : c); SensorCall(); return ReplaceReturn;}
}

static void
setupLumaTables(void)
{
	SensorCall();lumaRed = setupLuma(LumaRed);
	lumaGreen = setupLuma(LumaGreen);
	lumaBlue = setupLuma(LumaBlue);
	D1 = 1.F/(2.F - 2.F*LumaBlue);
	D2 = 1.F/(2.F - 2.F*LumaRed);
	Yzero = V2Code(0, refBlackWhite[0], refBlackWhite[1], 255);
SensorCall();}

static void
cvtClump(unsigned char* op, uint32* raster, uint32 ch, uint32 cw, uint32 w)
{
	SensorCall();float Y, Cb = 0, Cr = 0;
	uint32 j, k;
	/*
	 * Convert ch-by-cw block of RGB
	 * to YCbCr and sample accordingly.
	 */
	SensorCall();for (k = 0; k < ch; k++) {
		SensorCall();for (j = 0; j < cw; j++) {
			SensorCall();uint32 RGB = (raster - k*w)[j];
			Y = lumaRed[TIFFGetR(RGB)] +
			    lumaGreen[TIFFGetG(RGB)] +
			    lumaBlue[TIFFGetB(RGB)];
			/* accumulate chrominance */
			Cb += (TIFFGetB(RGB) - Y) * D1;
			Cr += (TIFFGetR(RGB) - Y) * D2;
			/* emit luminence */
			*op++ = V2Code(Y,
			    refBlackWhite[0], refBlackWhite[1], 255);
		}
		SensorCall();for (; j < horizSubSampling; j++)
			{/*23*/SensorCall();*op++ = Yzero;/*24*/}
	}
	SensorCall();for (; k < vertSubSampling; k++) {
		SensorCall();for (j = 0; j < horizSubSampling; j++)
			{/*25*/SensorCall();*op++ = Yzero;/*26*/}
	}
	/* emit sampled chrominance values */
	SensorCall();*op++ = V2Code(Cb / (ch*cw), refBlackWhite[2], refBlackWhite[3], 127);
	*op++ = V2Code(Cr / (ch*cw), refBlackWhite[4], refBlackWhite[5], 127);
SensorCall();}
#undef LumaRed
#undef LumaGreen
#undef LumaBlue
#undef V2Code

/*
 * Convert a strip of RGB data to YCbCr and
 * sample to generate the output data.
 */
static void
cvtStrip(unsigned char* op, uint32* raster, uint32 nrows, uint32 width)
{
	SensorCall();uint32 x;
	int clumpSize = vertSubSampling * horizSubSampling + 2;
	uint32 *tp;

	SensorCall();for (; nrows >= vertSubSampling; nrows -= vertSubSampling) {
		SensorCall();tp = raster;
		SensorCall();for (x = width; x >= horizSubSampling; x -= horizSubSampling) {
			SensorCall();cvtClump(op, tp,
			    vertSubSampling, horizSubSampling, width);
			op += clumpSize;
			tp += horizSubSampling;
		}
		SensorCall();if (x > 0) {
			SensorCall();cvtClump(op, tp, vertSubSampling, x, width);
			op += clumpSize;
		}
		SensorCall();raster -= vertSubSampling*width;
	}
	SensorCall();if (nrows > 0) {
		SensorCall();tp = raster;
		SensorCall();for (x = width; x >= horizSubSampling; x -= horizSubSampling) {
			SensorCall();cvtClump(op, tp, nrows, horizSubSampling, width);
			op += clumpSize;
			tp += horizSubSampling;
		}
		SensorCall();if (x > 0)
			{/*27*/SensorCall();cvtClump(op, tp, nrows, x, width);/*28*/}
	}
SensorCall();}

static int
cvtRaster(TIFF* tif, uint32* raster, uint32 width, uint32 height)
{
	SensorCall();uint32 y;
	tstrip_t strip = 0;
	tsize_t cc, acc;
	unsigned char* buf;
	uint32 rwidth = roundup(width, horizSubSampling);
	uint32 rheight = roundup(height, vertSubSampling);
	uint32 nrows = (rowsperstrip > rheight ? rheight : rowsperstrip);
        uint32 rnrows = roundup(nrows,vertSubSampling);

	cc = rnrows*rwidth +
	    2*((rnrows*rwidth) / (horizSubSampling*vertSubSampling));
	buf = (unsigned char*)_TIFFmalloc(cc);
	// FIXME unchecked malloc
	SensorCall();for (y = height; (int32) y > 0; y -= nrows) {
		SensorCall();uint32 nr = (y > nrows ? nrows : y);
		cvtStrip(buf, raster + (y-1)*width, nr, width);
		nr = roundup(nr, vertSubSampling);
		acc = nr*rwidth +
			2*((nr*rwidth)/(horizSubSampling*vertSubSampling));
		SensorCall();if (!TIFFWriteEncodedStrip(tif, strip++, buf, acc)) {
			SensorCall();_TIFFfree(buf);
			{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
		}
	}
	SensorCall();_TIFFfree(buf);
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

static int
tiffcvt(TIFF* in, TIFF* out)
{
	SensorCall();uint32 width, height;		/* image width & height */
	uint32* raster;			/* retrieve RGBA image */
	uint16 shortv;
	float floatv;
	char *stringv;
	uint32 longv;
	int result;
	size_t pixel_count;

	TIFFGetField(in, TIFFTAG_IMAGEWIDTH, &width);
	TIFFGetField(in, TIFFTAG_IMAGELENGTH, &height);
	pixel_count = width * height;

 	/* XXX: Check the integer overflow. */
 	SensorCall();if (!width || !height || pixel_count / width != height) {
 		SensorCall();TIFFError(TIFFFileName(in),
 			  "Malformed input file; "
 			  "can't allocate buffer for raster of %lux%lu size",
 			  (unsigned long)width, (unsigned long)height);
 		{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
 	}
 
 	SensorCall();raster = (uint32*)_TIFFCheckMalloc(in, pixel_count, sizeof(uint32),
 					   "raster buffer");
  	SensorCall();if (raster == 0) {
 		SensorCall();TIFFError(TIFFFileName(in),
 			  "Failed to allocate buffer (%lu elements of %lu each)",
 			  (unsigned long)pixel_count,
 			  (unsigned long)sizeof(uint32));
  		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  	}

	SensorCall();if (!TIFFReadRGBAImage(in, width, height, raster, 0)) {
		SensorCall();_TIFFfree(raster);
		{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
	}

	CopyField(TIFFTAG_SUBFILETYPE, longv);
	SensorCall();TIFFSetField(out, TIFFTAG_IMAGEWIDTH, width);
	TIFFSetField(out, TIFFTAG_IMAGELENGTH, height);
	TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, 8);
	TIFFSetField(out, TIFFTAG_COMPRESSION, compression);
	TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_YCBCR);
	SensorCall();if (compression == COMPRESSION_JPEG)
		{/*1*/SensorCall();TIFFSetField(out, TIFFTAG_JPEGCOLORMODE, JPEGCOLORMODE_RAW);/*2*/}
	CopyField(TIFFTAG_FILLORDER, shortv);
	SensorCall();TIFFSetField(out, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
	TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, 3);
	CopyField(TIFFTAG_XRESOLUTION, floatv);
	CopyField(TIFFTAG_YRESOLUTION, floatv);
	CopyField(TIFFTAG_RESOLUTIONUNIT, shortv);
	SensorCall();TIFFSetField(out, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
	{ char buf[2048];
	  char *cp = strrchr(TIFFFileName(in), '/');
	  snprintf(buf, sizeof(buf), "YCbCr conversion of %s",
		   cp ? cp+1 : TIFFFileName(in));
	  TIFFSetField(out, TIFFTAG_IMAGEDESCRIPTION, buf);
	}
	TIFFSetField(out, TIFFTAG_SOFTWARE, TIFFGetVersion());
	CopyField(TIFFTAG_DOCUMENTNAME, stringv);

	SensorCall();TIFFSetField(out, TIFFTAG_REFERENCEBLACKWHITE, refBlackWhite);
	TIFFSetField(out, TIFFTAG_YCBCRSUBSAMPLING,
	    horizSubSampling, vertSubSampling);
	TIFFSetField(out, TIFFTAG_YCBCRPOSITIONING, YCBCRPOSITION_CENTERED);
	TIFFSetField(out, TIFFTAG_YCBCRCOEFFICIENTS, ycbcrCoeffs);
	rowsperstrip = TIFFDefaultStripSize(out, rowsperstrip);
	TIFFSetField(out, TIFFTAG_ROWSPERSTRIP, rowsperstrip);

	result = cvtRaster(out, raster, width, height);
        _TIFFfree(raster);
        {int  ReplaceReturn = result; SensorCall(); return ReplaceReturn;}
}

char* stuff[] = {
    "usage: rgb2ycbcr [-c comp] [-r rows] [-h N] [-v N] input... output\n",
    "where comp is one of the following compression algorithms:\n",
    " jpeg\t\tJPEG encoding\n",
    " lzw\t\tLempel-Ziv & Welch encoding\n",
    " zip\t\tdeflate encoding\n",
    " packbits\tPackBits encoding (default)\n",
    " none\t\tno compression\n",
    "and the other options are:\n",
    " -r\trows/strip\n",
    " -h\thorizontal sampling factor (1,2,4)\n",
    " -v\tvertical sampling factor (1,2,4)\n",
    NULL
};

static void
usage(int code)
{
	SensorCall();char buf[BUFSIZ];
	int i;

	setbuf(stderr, buf);
       
 fprintf(stderr, "%s\n\n", TIFFGetVersion());
	SensorCall();for (i = 0; stuff[i] != NULL; i++)
		{/*3*/SensorCall();fprintf(stderr, "%s\n", stuff[i]);/*4*/}
	SensorCall();exit(code);
SensorCall();}

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
