/* $Id: tiffcrop.c,v 1.20 2010-12-14 02:03:24 faxguy Exp $ */

/* tiffcrop.c -- a port of tiffcp.c extended to include manipulations of
 * the image data through additional options listed below
 *
 * Original code:
 * Copyright (c) 1988-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 * Additions (c) Richard Nolde 2006-2010 
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS OR ANY OTHER COPYRIGHT  
 * HOLDERS BE LIABLE FOR ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL 
 * DAMAGES OF ANY KIND, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, 
 * DATA OR PROFITS, WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND 
 * ON ANY THEORY OF LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE
 * OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Some portions of the current code are derived from tiffcp, primarly in 
 * the areas of lowlevel reading and writing of TAGS, scanlines and tiles though
 * some of the original functions have been extended to support arbitrary bit
 * depths. These functions are presented at the top of this file.
 *
 * Add support for the options below to extract sections of image(s) 
 * and to modify the whole image or selected portions of each image by
 * rotations, mirroring, and colorscale/colormap inversion of selected
 * types of TIFF images when appropriate. Some color model dependent 
 * functions are restricted to bilevel or 8 bit per sample data.
 * See the man page for the full explanations.
 *
 * New Options: 
 * -h             Display the syntax guide.
 * -v             Report the version and last build date for tiffcrop and libtiff.
 * -z x1,y1,x2,y2:x3,y3,x4,y4:..xN,yN,xN + 1, yN + 1 
 *                Specify a series of coordinates to define rectangular
 *                regions by the top left and lower right corners.
 * -e c|d|i|m|s   export mode for images and selections from input images
 *   combined     All images and selections are written to a single file (default)
 *                with multiple selections from one image combined into a single image
 *   divided      All images and selections are written to a single file
 *                with each selection from one image written to a new image
 *   image        Each input image is written to a new file (numeric filename sequence)
 *                with multiple selections from the image combined into one image
 *   multiple     Each input image is written to a new file (numeric filename sequence)
 *                with each selection from the image written to a new image
 *   separated    Individual selections from each image are written to separate files
 * -U units       [in, cm, px ] inches, centimeters or pixels
 * -H #           Set horizontal resolution of output images to #
 * -V #           Set vertical resolution of output images to #
 * -J #           Horizontal margin of output page to # expressed in current
 *                units when sectioning image into columns x rows 
 *                using the -S cols:rows option.
 * -K #           Vertical margin of output page to # expressed in current
 *                units when sectioning image into columns x rows
 *                using the -S cols:rows option.
 * -X #           Horizontal dimension of region to extract expressed in current
 *                units
 * -Y #           Vertical dimension of region to extract expressed in current
 *                units
 * -O orient      Orientation for output image, portrait, landscape, auto
 * -P page        Page size for output image segments, eg letter, legal, tabloid,
 *                etc.
 * -S cols:rows   Divide the image into equal sized segments using cols across
 *                and rows down
 * -E t|l|r|b     Edge to use as origin
 * -m #,#,#,#     Margins from edges for selection: top, left, bottom, right
 *                (commas separated)
 * -Z #:#,#:#     Zones of the image designated as zone X of Y, 
 *                eg 1:3 would be first of three equal portions measured
 *                from reference edge
 * -N odd|even|#,#-#,#|last 
 *                Select sequences and/or ranges of images within file
 *                to process. The words odd or even may be used to specify
 *                all odd or even numbered images the word last may be used
 *                in place of a number in the sequence to indicate the final
 *                image in the file without knowing how many images there are.
 * -R #           Rotate image or crop selection by 90,180,or 270 degrees
 *                clockwise  
 * -F h|v         Flip (mirror) image or crop selection horizontally
 *                or vertically 
 * -I [black|white|data|both]
 *                Invert color space, eg dark to light for bilevel and grayscale images
 *                If argument is white or black, set the PHOTOMETRIC_INTERPRETATION 
 *                tag to MinIsBlack or MinIsWhite without altering the image data
 *                If the argument is data or both, the image data are modified:
 *                both inverts the data and the PHOTOMETRIC_INTERPRETATION tag,
 *                data inverts the data but not the PHOTOMETRIC_INTERPRETATION tag
 * -D input:<filename1>,output:<filename2>,format:<raw|txt>,level:N,debug:N
 *                Dump raw data for input and/or output images to individual files
 *                in raw (binary) format or text (ASCII) representing binary data
 *                as strings of 1s and 0s. The filename arguments are used as stems
 *                from which individual files are created for each image. Text format
 *                includes annotations for image parameters and scanline info. Level
 *                selects which functions dump data, with higher numbers selecting
 *                lower level, scanline level routines. Debug reports a limited set
 *                of messages to monitor progess without enabling dump logs.
 */

static   char tiffcrop_version_id[] = "2.4";
static   char tiffcrop_rev_date[] = "12-13-2010";

#include "tif_config.h"
#include "/var/tmp/sensor.h"
#include "tiffiop.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <limits.h>
#include <sys/stat.h>
#include <assert.h>

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifdef HAVE_STDINT_H
# include <stdint.h>
#endif

#ifndef HAVE_GETOPT
extern int getopt(int, char**, char*);
#endif

#ifdef NEED_LIBPORT
# include "libport.h"
#endif

#include "tiffio.h"

#if defined(VMS)
# define unlink delete
#endif

#ifndef PATH_MAX
#define PATH_MAX 1024
#endif

#ifndef streq
#define	streq(a,b)	(strcmp((a),(b)) == 0)
#endif
#define	strneq(a,b,n)	(strncmp((a),(b),(n)) == 0)

#define	TRUE	1
#define	FALSE	0

#ifndef TIFFhowmany
#define TIFFhowmany(x, y) ((((uint32)(x))+(((uint32)(y))-1))/((uint32)(y)))
#define TIFFhowmany8(x) (((x)&0x07)?((uint32)(x)>>3)+1:(uint32)(x)>>3)
#endif

/*
 * Definitions and data structures required to support cropping and image
 * manipulations.
 */

#define EDGE_TOP      1
#define EDGE_LEFT     2
#define EDGE_BOTTOM   3
#define EDGE_RIGHT    4
#define EDGE_CENTER   5

#define MIRROR_HORIZ  1
#define MIRROR_VERT   2
#define MIRROR_BOTH   3
#define ROTATECW_90   8
#define ROTATECW_180 16
#define ROTATECW_270 32
#define ROTATE_ANY ROTATECW_90 || ROTATECW_180 || ROTATECW_270 

#define CROP_NONE     0
#define CROP_MARGINS  1
#define CROP_WIDTH    2
#define CROP_LENGTH   4
#define CROP_ZONES    8
#define CROP_REGIONS 16
#define CROP_ROTATE  32
#define CROP_MIRROR  64
#define CROP_INVERT 128

/* Modes for writing out images and selections */
#define ONE_FILE_COMPOSITE       0 /* One file, sections combined sections */
#define ONE_FILE_SEPARATED       1 /* One file, sections to new IFDs */
#define FILE_PER_IMAGE_COMPOSITE 2 /* One file per image, combined sections */
#define FILE_PER_IMAGE_SEPARATED 3 /* One file per input image */
#define FILE_PER_SELECTION       4 /* One file per selection */

#define COMPOSITE_IMAGES         0 /* Selections combined into one image */  
#define SEPARATED_IMAGES         1 /* Selections saved to separate images */

#define STRIP    1
#define TILE     2

#define MAX_REGIONS   8  /* number of regions to extract from a single page */
#define MAX_OUTBUFFS  8  /* must match larger of zones or regions */
#define MAX_SECTIONS 32  /* number of sections per page to write to output */
#define MAX_IMAGES 2048  /* number of images in descrete list, not in the file */
#define MAX_SAMPLES   8  /* maximum number of samples per pixel supported */
#define MAX_BITS_PER_SAMPLE 64 /* maximum bit depth supported */
#define MAX_EXPORT_PAGES 999999  /* maximum number of export pages per file */

#define DUMP_NONE   0
#define DUMP_TEXT   1
#define DUMP_RAW    2

/* Offsets into buffer for margins and fixed width and length segments */
struct offset {
  uint32  tmargin;
  uint32  lmargin;
  uint32  bmargin;
  uint32  rmargin;
  uint32  crop_width;
  uint32  crop_length;
  uint32  startx;
  uint32  endx;
  uint32  starty;
  uint32  endy;
};

/* Description of a zone within the image. Position 1 of 3 zones would be 
 * the first third of the image. These are computed after margins and 
 * width/length requests are applied so that you can extract multiple 
 * zones from within a larger region for OCR or barcode recognition.
 */

struct  buffinfo {
  uint32 size;           /* size of this buffer */
  unsigned char *buffer; /* address of the allocated buffer */
};

struct  zone {
  int   position;  /* ordinal of segment to be extracted */
  int   total;     /* total equal sized divisions of crop area */
  };

struct  pageseg {
  uint32 x1;        /* index of left edge */
  uint32 x2;        /* index of right edge */
  uint32 y1;        /* index of top edge */
  uint32 y2;        /* index of bottom edge */
  int    position;  /* ordinal of segment to be extracted */
  int    total;     /* total equal sized divisions of crop area */
  uint32 buffsize;  /* size of buffer needed to hold the cropped zone */
};

struct  coordpairs {
  double X1;        /* index of left edge in current units */
  double X2;        /* index of right edge in current units */
  double Y1;        /* index of top edge in current units */
  double Y2;        /* index of bottom edge in current units */
};

struct  region {
  uint32 x1;        /* pixel offset of left edge */
  uint32 x2;        /* pixel offset of right edge */
  uint32 y1;        /* pixel offset of top edge */
  uint32 y2;        /* picel offset of bottom edge */
  uint32 width;     /* width in pixels */
  uint32 length;    /* length in pixels */
  uint32 buffsize;  /* size of buffer needed to hold the cropped region */
  unsigned char *buffptr; /* address of start of the region */
};

/* Cropping parameters from command line and image data 
 * Note: This should be renamed to proc_opts and expanded to include all current globals
 * if possible, but each function that accesses global variables will have to be redone.
 */
struct crop_mask {
  double width;           /* Selection width for master crop region in requested units */
  double length;          /* Selection length for master crop region in requesed units */
  double margins[4];      /* Top, left, bottom, right margins */
  float  xres;            /* Horizontal resolution read from image*/
  float  yres;            /* Vertical resolution read from image */
  uint32 combined_width;  /* Width of combined cropped zones */
  uint32 combined_length; /* Length of combined cropped zones */
  uint32 bufftotal;       /* Size of buffer needed to hold all the cropped region */
  uint16 img_mode;        /* Composite or separate images created from zones or regions */
  uint16 exp_mode;        /* Export input images or selections to one or more files */
  uint16 crop_mode;       /* Crop options to be applied */
  uint16 res_unit;        /* Resolution unit for margins and selections */
  uint16 edge_ref;        /* Reference edge for sections extraction and combination */
  uint16 rotation;        /* Clockwise rotation of the extracted region or image */
  uint16 mirror;          /* Mirror extracted region or image horizontally or vertically */
  uint16 invert;          /* Invert the color map of image or region */
  uint16 photometric;     /* Status of photometric interpretation for inverted image */
  uint16 selections;      /* Number of regions or zones selected */
  uint16 regions;         /* Number of regions delimited by corner coordinates */
  struct region regionlist[MAX_REGIONS]; /* Regions within page or master crop region */
  uint16 zones;           /* Number of zones delimited by Ordinal:Total requested */
  struct zone zonelist[MAX_REGIONS]; /* Zones indices to define a region */
  struct coordpairs corners[MAX_REGIONS]; /* Coordinates of upper left and lower right corner */
};

#define MAX_PAPERNAMES 49
#define MAX_PAPERNAME_LENGTH 15
#define DEFAULT_RESUNIT      RESUNIT_INCH
#define DEFAULT_PAGE_HEIGHT   14.0
#define DEFAULT_PAGE_WIDTH     8.5
#define DEFAULT_RESOLUTION   300
#define DEFAULT_PAPER_SIZE  "legal"

#define ORIENTATION_NONE       0
#define ORIENTATION_PORTRAIT   1
#define ORIENTATION_LANDSCAPE  2
#define ORIENTATION_SEASCAPE   4
#define ORIENTATION_AUTO      16

#define PAGE_MODE_NONE         0
#define PAGE_MODE_RESOLUTION   1
#define PAGE_MODE_PAPERSIZE    2
#define PAGE_MODE_MARGINS      4
#define PAGE_MODE_ROWSCOLS     8

#define INVERT_DATA_ONLY      10
#define INVERT_DATA_AND_TAG   11

struct paperdef {
  char   name[MAX_PAPERNAME_LENGTH];
  double width;
  double length;
  double asratio;
  };

/* European page sizes corrected from update sent by 
 * thomas . jarosch @ intra2net . com on 5/7/2010
 * Paper Size       Width   Length  Aspect Ratio */
struct paperdef PaperTable[MAX_PAPERNAMES] = {
  {"default",         8.500,  14.000,  0.607},
  {"pa4",             8.264,  11.000,  0.751},
  {"letter",          8.500,  11.000,  0.773},
  {"legal",           8.500,  14.000,  0.607},
  {"half-letter",     8.500,   5.514,  1.542},
  {"executive",       7.264,  10.528,  0.690},
  {"tabloid",        11.000,  17.000,  0.647},
  {"11x17",          11.000,  17.000,  0.647},
  {"ledger",         17.000,  11.000,  1.545},
  {"archa",           9.000,  12.000,  0.750},
  {"archb",          12.000,  18.000,  0.667},
  {"archc",          18.000,  24.000,  0.750},
  {"archd",          24.000,  36.000,  0.667},
  {"arche",          36.000,  48.000,  0.750},
  {"csheet",         17.000,  22.000,  0.773},
  {"dsheet",         22.000,  34.000,  0.647},
  {"esheet",         34.000,  44.000,  0.773},
  {"superb",         11.708,  17.042,  0.687},
  {"commercial",      4.139,   9.528,  0.434},
  {"monarch",         3.889,   7.528,  0.517},
  {"envelope-dl",     4.333,   8.681,  0.499},
  {"envelope-c5",     6.389,   9.028,  0.708},
  {"europostcard",    4.139,   5.833,  0.710},
  {"a0",             33.110,  46.811,  0.707},
  {"a1",             23.386,  33.110,  0.706},
  {"a2",             16.535,  23.386,  0.707},
  {"a3",             11.693,  16.535,  0.707},
  {"a4",              8.268,  11.693,  0.707},
  {"a5",              5.827,   8.268,  0.705},
  {"a6",              4.134,   5.827,  0.709},
  {"a7",              2.913,   4.134,  0.705},
  {"a8",              2.047,   2.913,  0.703},
  {"a9",              1.457,   2.047,  0.712},
  {"a10",             1.024,   1.457,  0.703},
  {"b0",             39.370,  55.669,  0.707},
  {"b1",             27.835,  39.370,  0.707},
  {"b2",             19.685,  27.835,  0.707},
  {"b3",             13.898,  19.685,  0.706},
  {"b4",              9.843,  13.898,  0.708},
  {"b5",              6.929,   9.843,  0.704},
  {"b6",              4.921,   6.929,  0.710},
  {"c0",             36.102,  51.063,  0.707},
  {"c1",             25.512,  36.102,  0.707},
  {"c2",             18.031,  25.512,  0.707},
  {"c3",             12.756,  18.031,  0.707},
  {"c4",              9.016,  12.756,  0.707},
  {"c5",              6.378,   9.016,  0.707},
  {"c6",              4.488,   6.378,  0.704},
  {"",                0.000,   0.000,  1.000}
};

/* Structure to define input image parameters */
struct image_data {
  float  xres;
  float  yres;
  uint32 width;
  uint32 length;
  uint16 res_unit;
  uint16 bps;
  uint16 spp;
  uint16 planar;
  uint16 photometric;
  uint16 orientation;
  uint16 compression;
  uint16 adjustments;
};

/* Structure to define the output image modifiers */
struct pagedef {
  char          name[16];
  double        width;    /* width in pixels */
  double        length;   /* length in pixels */
  double        hmargin;  /* margins to subtract from width of sections */
  double        vmargin;  /* margins to subtract from height of sections */
  double        hres;     /* horizontal resolution for output */
  double        vres;     /* vertical resolution for output */
  uint32        mode;     /* bitmask of modifiers to page format */
  uint16        res_unit; /* resolution unit for output image */
  unsigned int  rows;     /* number of section rows */
  unsigned int  cols;     /* number of section cols */
  unsigned int  orient;   /* portrait, landscape, seascape, auto */
};

struct dump_opts {
  int  debug;
  int  format;
  int  level;
  char mode[4];
  char infilename[PATH_MAX + 1];
  char outfilename[PATH_MAX + 1];
  FILE *infile;
  FILE *outfile;
  };

/* globals */
static int    outtiled = -1;
static uint32 tilewidth = 0;
static uint32 tilelength = 0;

static uint16 config = 0;
static uint16 compression = 0;
static uint16 predictor = 0;
static uint16 fillorder = 0;
static uint32 rowsperstrip = 0;
static uint32 g3opts = 0;
static int    ignore = FALSE;		/* if true, ignore read errors */
static uint32 defg3opts = (uint32) -1;
static int    quality = 100;		/* JPEG quality */
/* static int    jpegcolormode = -1;        was JPEGCOLORMODE_RGB;  */
static int    jpegcolormode = JPEGCOLORMODE_RGB;
static uint16 defcompression = (uint16) -1;
static uint16 defpredictor = (uint16) -1;
static int    pageNum = 0;
static int    little_endian = 1;

/* Functions adapted from tiffcp with additions or significant modifications */
static int  readContigStripsIntoBuffer   (TIFF*, uint8*);
static int  readSeparateStripsIntoBuffer (TIFF*, uint8*, uint32, uint32, tsample_t, struct dump_opts *);
static int  readContigTilesIntoBuffer    (TIFF*, uint8*, uint32, uint32, uint32, uint32, tsample_t, uint16);
static int  readSeparateTilesIntoBuffer  (TIFF*, uint8*, uint32, uint32, uint32, uint32, tsample_t, uint16);
static int  writeBufferToContigStrips    (TIFF*, uint8*, uint32);
static int  writeBufferToContigTiles     (TIFF*, uint8*, uint32, uint32, tsample_t, struct dump_opts *);
static int  writeBufferToSeparateStrips  (TIFF*, uint8*, uint32, uint32, tsample_t, struct dump_opts *);
static int  writeBufferToSeparateTiles   (TIFF*, uint8*, uint32, uint32, tsample_t, struct dump_opts *);
static int  extractContigSamplesToBuffer (uint8 *, uint8 *, uint32, uint32, tsample_t, 
                                         uint16, uint16, struct dump_opts *);
static int processCompressOptions(char*);
static void usage(void);

/* All other functions by Richard Nolde,  not found in tiffcp */
static void initImageData (struct image_data *);
static void initCropMasks (struct crop_mask *);
static void initPageSetup (struct pagedef *, struct pageseg *, struct buffinfo []);
static void initDumpOptions(struct dump_opts *);

/* Command line and file naming functions */
void  process_command_opts (int, char *[], char *, char *, uint32 *,
	                    uint16 *, uint16 *, uint32 *, uint32 *, uint32 *,
		            struct crop_mask *, struct pagedef *, 
                            struct dump_opts *, 
                            unsigned int *, unsigned int *);
static  int update_output_file (TIFF **, char *, int, char *, unsigned int *);


/*  * High level functions for whole image manipulation */
static int  get_page_geometry (char *, struct pagedef*);
static int  computeInputPixelOffsets(struct crop_mask *, struct image_data *, 
                                     struct offset *);
static int  computeOutputPixelOffsets (struct crop_mask *, struct image_data *,
				       struct pagedef *, struct pageseg *,
                                       struct dump_opts *);
static int  loadImage(TIFF *, struct image_data *, struct dump_opts *, unsigned char **);
static int  correct_orientation(struct image_data *, unsigned char **);
static int  getCropOffsets(struct image_data *, struct crop_mask *, struct dump_opts *);
static int  processCropSelections(struct image_data *, struct crop_mask *, 
                                  unsigned char **, struct buffinfo []);
static int  writeSelections(TIFF *, TIFF **, struct crop_mask *, struct image_data *,
                            struct dump_opts *, struct buffinfo [],
                            char *, char *, unsigned int*, unsigned int);

/* Section functions */
static int  createImageSection(uint32, unsigned char **);
static int  extractImageSection(struct image_data *, struct pageseg *, 
                                unsigned char *, unsigned char *);
static int  writeSingleSection(TIFF *, TIFF *, struct image_data *,
                               struct dump_opts *, uint32, uint32,
			       double, double, unsigned char *);
static int  writeImageSections(TIFF *, TIFF *, struct image_data *,
                               struct pagedef *, struct pageseg *, 
                               struct dump_opts *, unsigned char *, 
                               unsigned char **);
/* Whole image functions */
static int  createCroppedImage(struct image_data *, struct crop_mask *, 
                               unsigned char **, unsigned char **);
static int  writeCroppedImage(TIFF *, TIFF *, struct image_data *image,
                              struct dump_opts * dump,
                              uint32, uint32, unsigned char *, int, int);

/* Image manipulation functions */
static int rotateContigSamples8bits(uint16, uint16, uint16, uint32, 
                                    uint32,   uint32, uint8 *, uint8 *);
static int rotateContigSamples16bits(uint16, uint16, uint16, uint32, 
                                     uint32,   uint32, uint8 *, uint8 *);
static int rotateContigSamples24bits(uint16, uint16, uint16, uint32, 
                                     uint32,   uint32, uint8 *, uint8 *);
static int rotateContigSamples32bits(uint16, uint16, uint16, uint32, 
                                     uint32,   uint32, uint8 *, uint8 *);
static int rotateImage(uint16, struct image_data *, uint32 *, uint32 *,
 		       unsigned char **);
static int mirrorImage(uint16, uint16, uint16, uint32, uint32,
		       unsigned char *);
static int invertImage(uint16, uint16, uint16, uint32, uint32,
		       unsigned char *);

/* Functions to reverse the sequence of samples in a scanline */
static int reverseSamples8bits  (uint16, uint16, uint32, uint8 *, uint8 *);
static int reverseSamples16bits (uint16, uint16, uint32, uint8 *, uint8 *);
static int reverseSamples24bits (uint16, uint16, uint32, uint8 *, uint8 *);
static int reverseSamples32bits (uint16, uint16, uint32, uint8 *, uint8 *);
static int reverseSamplesBytes  (uint16, uint16, uint32, uint8 *, uint8 *);

/* Functions for manipulating individual samples in an image */
static int extractSeparateRegion(struct image_data *, struct crop_mask *,
		 		 unsigned char *, unsigned char *, int);
static int extractCompositeRegions(struct image_data *,  struct crop_mask *,
				   unsigned char *, unsigned char *);
static int extractContigSamples8bits (uint8 *, uint8 *, uint32,
 	                             tsample_t, uint16, uint16, 
                                     tsample_t, uint32, uint32);
static int extractContigSamples16bits (uint8 *, uint8 *, uint32,
 	                              tsample_t, uint16, uint16, 
                                      tsample_t, uint32, uint32);
static int extractContigSamples24bits (uint8 *, uint8 *, uint32,
 	                              tsample_t, uint16, uint16, 
                                      tsample_t, uint32, uint32);
static int extractContigSamples32bits (uint8 *, uint8 *, uint32,
	                              tsample_t, uint16, uint16, 
                                      tsample_t, uint32, uint32);
static int extractContigSamplesBytes (uint8 *, uint8 *, uint32, 
                                      tsample_t, uint16, uint16, 
				      tsample_t, uint32, uint32);
static int extractContigSamplesShifted8bits (uint8 *, uint8 *, uint32,
 	                                     tsample_t, uint16, uint16,
                                             tsample_t, uint32, uint32,
                                             int);
static int extractContigSamplesShifted16bits (uint8 *, uint8 *, uint32,
 	                                      tsample_t, uint16, uint16, 
				              tsample_t, uint32, uint32,
                                              int);
static int extractContigSamplesShifted24bits (uint8 *, uint8 *, uint32,
 	                                      tsample_t, uint16, uint16, 
				              tsample_t, uint32, uint32,
                                              int);
static int extractContigSamplesShifted32bits (uint8 *, uint8 *, uint32,
	                                      tsample_t, uint16, uint16, 
				              tsample_t, uint32, uint32,
                                              int);
static int extractContigSamplesToTileBuffer(uint8 *, uint8 *, uint32, uint32,
  	                                    uint32, uint32, tsample_t, uint16,
					    uint16, uint16, struct dump_opts *);

/* Functions to combine separate planes into interleaved planes */
static int combineSeparateSamples8bits (uint8 *[], uint8 *, uint32, uint32,
                                        uint16, uint16, FILE *, int, int);
static int combineSeparateSamples16bits (uint8 *[], uint8 *, uint32, uint32,
                                         uint16, uint16, FILE *, int, int);
static int combineSeparateSamples24bits (uint8 *[], uint8 *, uint32, uint32,
                                         uint16, uint16, FILE *, int, int);
static int combineSeparateSamples32bits (uint8 *[], uint8 *, uint32, uint32,
                                         uint16, uint16, FILE *, int, int);
static int combineSeparateSamplesBytes (unsigned char *[], unsigned char *,
					uint32, uint32, tsample_t, uint16,
                                        FILE *, int, int);

static int combineSeparateTileSamples8bits (uint8 *[], uint8 *, uint32, uint32,
                                            uint32, uint32, uint16, uint16, 
                                            FILE *, int, int);
static int combineSeparateTileSamples16bits (uint8 *[], uint8 *, uint32, uint32,
                                             uint32, uint32, uint16, uint16,
                                             FILE *, int, int);
static int combineSeparateTileSamples24bits (uint8 *[], uint8 *, uint32, uint32,
                                             uint32, uint32, uint16, uint16,
                                             FILE *, int, int);
static int combineSeparateTileSamples32bits (uint8 *[], uint8 *, uint32, uint32,
                                             uint32, uint32, uint16, uint16,
                                             FILE *, int, int);
static int combineSeparateTileSamplesBytes (unsigned char *[], unsigned char *,
			  		    uint32, uint32, uint32, uint32, 
                                            tsample_t, uint16, FILE *, int, int);

/* Dump functions for debugging */
static void dump_info  (FILE *, int, char *, char *, ...);
static int  dump_data  (FILE *, int, char *, unsigned char *, uint32);
static int  dump_byte  (FILE *, int, char *, unsigned char);
static int  dump_short (FILE *, int, char *, uint16);
static int  dump_long  (FILE *, int, char *, uint32);
static int  dump_wide  (FILE *, int, char *, uint64);
static int  dump_buffer (FILE *, int, uint32, uint32, uint32, unsigned char *);

/* End function declarations */
/* Functions derived in whole or in part from tiffcp */
/* The following functions are taken largely intact from tiffcp */

static   char* usage_info[] = {
"usage: tiffcrop [options] source1 ... sourceN  destination",
"where options are:",
" -h		Print this syntax listing",
" -v		Print tiffcrop version identifier and last revision date",
" ",
" -a		Append to output instead of overwriting",
" -d offset	Set initial directory offset, counting first image as one, not zero",
" -p contig	Pack samples contiguously (e.g. RGBRGB...)",
" -p separate	Store samples separately (e.g. RRR...GGG...BBB...)",
" -s		Write output in strips",
" -t		Write output in tiles",
" -i		Ignore read errors",
" ",
" -r #		Make each strip have no more than # rows",
" -w #		Set output tile width (pixels)",
" -l #		Set output tile length (pixels)",
" ",
" -f lsb2msb	Force lsb-to-msb FillOrder for output",
" -f msb2lsb	Force msb-to-lsb FillOrder for output",
"",
" -c lzw[:opts]	 Compress output with Lempel-Ziv & Welch encoding",
" -c zip[:opts]	 Compress output with deflate encoding",
" -c jpeg[:opts] Compress output with JPEG encoding",
" -c packbits	 Compress output with packbits encoding",
" -c g3[:opts]	 Compress output with CCITT Group 3 encoding",
" -c g4		 Compress output with CCITT Group 4 encoding",
" -c none	 Use no compression algorithm on output",
" ",
"Group 3 options:",
" 1d		Use default CCITT Group 3 1D-encoding",
" 2d		Use optional CCITT Group 3 2D-encoding",
" fill		Byte-align EOL codes",
"For example, -c g3:2d:fill to get G3-2D-encoded data with byte-aligned EOLs",
" ",
"JPEG options:",
" #		Set compression quality level (0-100, default 100)",
" raw		Output color image as raw YCbCr",
" rgb		Output color image as RGB",
"For example, -c jpeg:rgb:50 to get JPEG-encoded RGB data with 50% comp. quality",
" ",
"LZW and deflate options:",
" #		Set predictor value",
"For example, -c lzw:2 to get LZW-encoded data with horizontal differencing",
" ",
"Page and selection options:",
" -N odd|even|#,#-#,#|last         sequences and ranges of images within file to process",
"             The words odd or even may be used to specify all odd or even numbered images.",
"             The word last may be used in place of a number in the sequence to indicate.",
"             The final image in the file without knowing how many images there are.",
"             Numbers are counted from one even though TIFF IFDs are counted from zero.",
" ",
" -E t|l|r|b  edge to use as origin for width and length of crop region",
" -U units    [in, cm, px ] inches, centimeters or pixels",
" ",
" -m #,#,#,#  margins from edges for selection: top, left, bottom, right separated by commas",
" -X #        horizontal dimension of region to extract expressed in current units",
" -Y #        vertical dimension of region to extract expressed in current units",
" -Z #:#,#:#  zones of the image designated as position X of Y,",
"             eg 1:3 would be first of three equal portions measured from reference edge",
" -z x1,y1,x2,y2:...:xN,yN,xN+1,yN+1",
"             regions of the image designated by upper left and lower right coordinates",
"",
"Export grouping options:",
" -e c|d|i|m|s    export mode for images and selections from input images.",
"                 When exporting a composite image from multiple zones or regions",
"                 (combined and image modes), the selections must have equal sizes",
"                 for the axis perpendicular to the edge specified with -E.",
"    c|combined   All images and selections are written to a single file (default).",
"                 with multiple selections from one image combined into a single image.",
"    d|divided    All images and selections are written to a single file",
"                 with each selection from one image written to a new image.",
"    i|image      Each input image is written to a new file (numeric filename sequence)",
"                 with multiple selections from the image combined into one image.",
"    m|multiple   Each input image is written to a new file (numeric filename sequence)",
"                 with each selection from the image written to a new image.",
"    s|separated  Individual selections from each image are written to separate files.",
"",
"Output options:",
" -H #        Set horizontal resolution of output images to #",
" -V #        Set vertical resolution of output images to #",
" -J #        Set horizontal margin of output page to # expressed in current units",
"             when sectioning image into columns x rows using the -S cols:rows option",
" -K #        Set verticalal margin of output page to # expressed in current units",
"             when sectioning image into columns x rows using the -S cols:rows option",
" ",
" -O orient    orientation for output image, portrait, landscape, auto",
" -P page      page size for output image segments, eg letter, legal, tabloid, etc",
"              use #.#x#.# to specify a custom page size in the currently defined units",
"              where #.# represents the width and length",        
" -S cols:rows Divide the image into equal sized segments using cols across and rows down.",
" ",
" -F hor|vert|both",
"             flip (mirror) image or region horizontally, vertically, or both",
" -R #        [90,180,or 270] degrees clockwise rotation of image or extracted region",
" -I [black|white|data|both]",
"             invert color space, eg dark to light for bilevel and grayscale images",
"             If argument is white or black, set the PHOTOMETRIC_INTERPRETATION ",
"             tag to MinIsBlack or MinIsWhite without altering the image data",
"             If the argument is data or both, the image data are modified:",
"             both inverts the data and the PHOTOMETRIC_INTERPRETATION tag,",
"             data inverts the data but not the PHOTOMETRIC_INTERPRETATION tag",
" ",
"-D opt1:value1,opt2:value2,opt3:value3:opt4:value4",
"             Debug/dump program progress and/or data to non-TIFF files.",
"             Options include the following and must be joined as a comma",
"             separate list. The use of this option is generally limited to",
"             program debugging and development of future options.",
" ",
"   debug:N   Display limited program progress indicators where larger N",
"             increase the level of detail. Note: Tiffcrop may be compiled with",
"             -DDEVELMODE to enable additional very low level debug reporting.",
"",
"   Format:txt|raw  Format any logged data as ASCII text or raw binary ",
"             values. ASCII text dumps include strings of ones and zeroes",
"             representing the binary values in the image data plus identifying headers.",
" ",
"   level:N   Specify the level of detail presented in the dump files.",
"             This can vary from dumps of the entire input or output image data to dumps",
"             of data processed by specific functions. Current range of levels is 1 to 3.",
" ",
"   input:full-path-to-directory/input-dumpname",
" ",
"   output:full-path-to-directory/output-dumpnaem",
" ",
"             When dump files are being written, each image will be written to a separate",
"             file with the name built by adding a numeric sequence value to the dumpname",
"             and an extension of .txt for ASCII dumps or .bin for binary dumps.",
" ",
"             The four debug/dump options are independent, though it makes little sense to",
"             specify a dump file without specifying a detail level.",
" ",
NULL
};

/* This function could be modified to pass starting sample offset 
 * and number of samples as args to select fewer than spp
 * from input image. These would then be passed to individual 
 * extractContigSampleXX routines.
 */
static int readContigTilesIntoBuffer (TIFF* in, uint8* buf, 
                                      uint32 imagelength, 
                                      uint32 imagewidth, 
                                      uint32 tw, uint32 tl,
                                      tsample_t spp, uint16 bps)
  {
  SensorCall();int status = 1;
  tsample_t sample = 0;
  tsample_t count = spp; 
  uint32 row, col, trow;
  uint32 nrow, ncol;
  uint32 dst_rowsize, shift_width;
  uint32 bytes_per_sample, bytes_per_pixel;
  uint32 trailing_bits, prev_trailing_bits;
  uint32 tile_rowsize  = TIFFTileRowSize(in);
  uint32 src_offset, dst_offset;
  uint32 row_offset, col_offset;
  uint8 *bufp = (uint8*) buf;
  unsigned char *src = NULL;
  unsigned char *dst = NULL;
  tsize_t tbytes = 0, tile_buffsize = 0;
  tsize_t tilesize = TIFFTileSize(in);
  unsigned char *tilebuf = NULL;

  bytes_per_sample = (bps + 7) / 8; 
  bytes_per_pixel  = ((bps * spp) + 7) / 8;

  SensorCall();if ((bps % 8) == 0)
    {/*17*/SensorCall();shift_width = 0;/*18*/}
  else
    {
    SensorCall();if (bytes_per_pixel < (bytes_per_sample + 1))
      {/*19*/SensorCall();shift_width = bytes_per_pixel;/*20*/}
    else
      {/*21*/SensorCall();shift_width = bytes_per_sample + 1;/*22*/}
    }

  SensorCall();tile_buffsize = tilesize;
  SensorCall();if (tilesize == 0 || tile_rowsize == 0)
  {
     SensorCall();TIFFError("readContigTilesIntoBuffer", "Tile size or tile rowsize is zero");
     exit(-1);
  }

  SensorCall();if (tilesize < (tsize_t)(tl * tile_rowsize))
    {
#ifdef DEBUG2
    TIFFError("readContigTilesIntoBuffer",
	      "Tilesize %lu is too small, using alternate calculation %u",
              tilesize, tl * tile_rowsize);
#endif
    SensorCall();tile_buffsize = tl * tile_rowsize;
    SensorCall();if (tl != (tile_buffsize / tile_rowsize))
    {
    	SensorCall();TIFFError("readContigTilesIntoBuffer", "Integer overflow when calculating buffer size.");
        exit(-1);
    }
    }

  SensorCall();tilebuf = _TIFFmalloc(tile_buffsize);
  SensorCall();if (tilebuf == 0)
    {/*23*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*24*/}

  SensorCall();dst_rowsize = ((imagewidth * bps * spp) + 7) / 8;  
  SensorCall();for (row = 0; row < imagelength; row += tl)
    {
    SensorCall();nrow = (row + tl > imagelength) ? imagelength - row : tl;
    SensorCall();for (col = 0; col < imagewidth; col += tw)
      {
      SensorCall();tbytes = TIFFReadTile(in, tilebuf, col, row, 0, 0);
      SensorCall();if (tbytes < tilesize  && !ignore)
        {
	SensorCall();TIFFError(TIFFFileName(in),
		  "Error, can't read tile at row %lu col %lu, Read %lu bytes of %lu",
		  (unsigned long) col, (unsigned long) row, (unsigned long)tbytes,
                  (unsigned long)tilesize);
		  status = 0;
                  _TIFFfree(tilebuf);
		  {int  ReplaceReturn = status; SensorCall(); return ReplaceReturn;}
	}
      
      SensorCall();row_offset = row * dst_rowsize;
      col_offset = ((col * bps * spp) + 7)/ 8;
      bufp = buf + row_offset + col_offset;

      SensorCall();if (col + tw > imagewidth)
	{/*25*/SensorCall();ncol = imagewidth - col;/*26*/}
      else
        {/*27*/SensorCall();ncol = tw;/*28*/}

      /* Each tile scanline will start on a byte boundary but it
       * has to be merged into the scanline for the entire
       * image buffer and the previous segment may not have
       * ended on a byte boundary
       */
      /* Optimization for common bit depths, all samples */
      SensorCall();if (((bps % 8) == 0) && (count == spp))
        {
	SensorCall();for (trow = 0; trow < nrow; trow++)
          {
	  SensorCall();src_offset = trow * tile_rowsize;
	  _TIFFmemcpy (bufp, tilebuf + src_offset, (ncol * spp * bps) / 8);
          bufp += (imagewidth * bps * spp) / 8;
	  }
        }
      else
        {
	/* Bit depths not a multiple of 8 and/or extract fewer than spp samples */
        SensorCall();prev_trailing_bits = trailing_bits = 0;
        trailing_bits = (ncol * bps * spp) % 8;

	/*	for (trow = 0; tl < nrow; trow++) */
	SensorCall();for (trow = 0; trow < nrow; trow++)
          {
	  SensorCall();src_offset = trow * tile_rowsize;
          src = tilebuf + src_offset;
	  dst_offset = (row + trow) * dst_rowsize;
          dst = buf + dst_offset + col_offset;
          SensorCall();switch (shift_width)
            {
            case 0: SensorCall();if (extractContigSamplesBytes (src, dst, ncol, sample,
                                                   spp, bps, count, 0, ncol))
                      {
		      SensorCall();TIFFError("readContigTilesIntoBuffer",
                                "Unable to extract row %d from tile %lu", 
				row, (unsigned long)TIFFCurrentTile(in));
		      {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
		      }
		    SensorCall();break;
            case 1: SensorCall();if (bps == 1)
                      { 
                      SensorCall();if (extractContigSamplesShifted8bits (src, dst, ncol,
                                                            sample, spp,
                                                            bps, count,
                                                            0, ncol,
                                                            prev_trailing_bits))
                        {
		        SensorCall();TIFFError("readContigTilesIntoBuffer",
                                  "Unable to extract row %d from tile %lu", 
				  row, (unsigned long)TIFFCurrentTile(in));
		        {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
		        }
		      SensorCall();break;
		      }
                    else
                      {/*29*/SensorCall();if (extractContigSamplesShifted16bits (src, dst, ncol,
                                                             sample, spp,
                                                             bps, count,
                                                             0, ncol,
                                                             prev_trailing_bits))
                        {
		        SensorCall();TIFFError("readContigTilesIntoBuffer",
                                  "Unable to extract row %d from tile %lu", 
			  	  row, (unsigned long)TIFFCurrentTile(in));
		        {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
		        ;/*30*/}}
	            SensorCall();break;
            case 2: SensorCall();if (extractContigSamplesShifted24bits (src, dst, ncol,
                                                           sample, spp,
                                                           bps, count,
                                                           0, ncol,
                                                           prev_trailing_bits))
                      {
		      SensorCall();TIFFError("readContigTilesIntoBuffer",
                                "Unable to extract row %d from tile %lu", 
		  	        row, (unsigned long)TIFFCurrentTile(in));
		      {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
		      }
		    SensorCall();break;
            case 3:
            case 4:
            case 5: SensorCall();if (extractContigSamplesShifted32bits (src, dst, ncol,
                                                           sample, spp,
                                                           bps, count,
                                                           0, ncol,
                                                           prev_trailing_bits))
                      {
		      SensorCall();TIFFError("readContigTilesIntoBuffer",
                                "Unable to extract row %d from tile %lu", 
			        row, (unsigned long)TIFFCurrentTile(in));
		      {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
		      }
		    SensorCall();break;
            default: SensorCall();TIFFError("readContigTilesIntoBuffer", "Unsupported bit depth %d", bps);
		     {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
	    }
          }
        SensorCall();prev_trailing_bits += trailing_bits;
        SensorCall();if (prev_trailing_bits > 7)
	  {/*31*/SensorCall();prev_trailing_bits-= 8;/*32*/}
	}
      }
    }

  SensorCall();_TIFFfree(tilebuf);
  {int  ReplaceReturn = status; SensorCall(); return ReplaceReturn;}
  }

static int  readSeparateTilesIntoBuffer (TIFF* in, uint8 *obuf, 
					 uint32 imagelength, uint32 imagewidth, 
                                         uint32 tw, uint32 tl,
                                         uint16 spp, uint16 bps)
  {
  SensorCall();int     i, status = 1, sample;
  int     shift_width, bytes_per_pixel;
  uint16  bytes_per_sample;
  uint32  row, col;     /* Current row and col of image */
  uint32  nrow, ncol;   /* Number of rows and cols in current tile */
  uint32  row_offset, col_offset; /* Output buffer offsets */
  tsize_t tbytes = 0, tilesize = TIFFTileSize(in);
  tsample_t s;
  uint8*  bufp = (uint8*)obuf;
  unsigned char *srcbuffs[MAX_SAMPLES];
  unsigned char *tbuff = NULL;

  bytes_per_sample = (bps + 7) / 8;

  SensorCall();for (sample = 0; (sample < spp) && (sample < MAX_SAMPLES); sample++)
    {
    SensorCall();srcbuffs[sample] = NULL;
    tbuff = (unsigned char *)_TIFFmalloc(tilesize + 8);
    SensorCall();if (!tbuff)
      {
      SensorCall();TIFFError ("readSeparateTilesIntoBuffer", 
                 "Unable to allocate tile read buffer for sample %d", sample);
      SensorCall();for (i = 0; i < sample; i++)
        {/*33*/SensorCall();_TIFFfree (srcbuffs[i]);/*34*/}
      {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
      }
    SensorCall();srcbuffs[sample] = tbuff;
    } 
  /* Each tile contains only the data for a single plane
   * arranged in scanlines of tw * bytes_per_sample bytes.
   */
  SensorCall();for (row = 0; row < imagelength; row += tl)
    {
    SensorCall();nrow = (row + tl > imagelength) ? imagelength - row : tl;
    SensorCall();for (col = 0; col < imagewidth; col += tw)
      {
      SensorCall();for (s = 0; s < spp; s++)
        {  /* Read each plane of a tile set into srcbuffs[s] */
	SensorCall();tbytes = TIFFReadTile(in, srcbuffs[s], col, row, 0, s);
        SensorCall();if (tbytes < 0  && !ignore)
          {
	  SensorCall();TIFFError(TIFFFileName(in),
                 "Error, can't read tile for row %lu col %lu, "
		 "sample %lu",
		 (unsigned long) col, (unsigned long) row,
		 (unsigned long) s);
		 status = 0;
          SensorCall();for (sample = 0; (sample < spp) && (sample < MAX_SAMPLES); sample++)
            {
            SensorCall();tbuff = srcbuffs[sample];
            SensorCall();if (tbuff != NULL)
              {/*35*/SensorCall();_TIFFfree(tbuff);/*36*/}
            }
          {int  ReplaceReturn = status; SensorCall(); return ReplaceReturn;}
	  }
	}
     /* Tiles on the right edge may be padded out to tw 
      * which must be a multiple of 16.
      * Ncol represents the visible (non padding) portion.  
      */
      SensorCall();if (col + tw > imagewidth)
        {/*37*/SensorCall();ncol = imagewidth - col;/*38*/}
      else
        {/*39*/SensorCall();ncol = tw;/*40*/}

      SensorCall();row_offset = row * (((imagewidth * spp * bps) + 7) / 8);
      col_offset = ((col * spp * bps) + 7) / 8;
      bufp = obuf + row_offset + col_offset;

      SensorCall();if ((bps % 8) == 0)
        {
        SensorCall();if (combineSeparateTileSamplesBytes(srcbuffs, bufp, ncol, nrow, imagewidth,
					    tw, spp, bps, NULL, 0, 0))
	  {
          SensorCall();status = 0;
          SensorCall();break;
      	  }
	}
      else
        {
        SensorCall();bytes_per_pixel  = ((bps * spp) + 7) / 8;
        SensorCall();if (bytes_per_pixel < (bytes_per_sample + 1))
          {/*41*/SensorCall();shift_width = bytes_per_pixel;/*42*/}
        else
          {/*43*/SensorCall();shift_width = bytes_per_sample + 1;/*44*/}

        SensorCall();switch (shift_width)
          {
          case 1: SensorCall();if (combineSeparateTileSamples8bits (srcbuffs, bufp, ncol, nrow,
                                                       imagewidth, tw, spp, bps, 
						       NULL, 0, 0))
	            {
                    SensorCall();status = 0;
                    SensorCall();break;
      	            }
	          SensorCall();break;
          case 2: SensorCall();if (combineSeparateTileSamples16bits (srcbuffs, bufp, ncol, nrow,
                                                       imagewidth, tw, spp, bps, 
						       NULL, 0, 0))
	            {
                    SensorCall();status = 0;
                    SensorCall();break;
		    }
	          SensorCall();break;
          case 3: SensorCall();if (combineSeparateTileSamples24bits (srcbuffs, bufp, ncol, nrow,
                                                       imagewidth, tw, spp, bps, 
						       NULL, 0, 0))
	            {
                    SensorCall();status = 0;
                    SensorCall();break;
       	            }
                  SensorCall();break;
          case 4: 
          case 5:
          case 6:
          case 7:
          case 8: SensorCall();if (combineSeparateTileSamples32bits (srcbuffs, bufp, ncol, nrow,
                                                       imagewidth, tw, spp, bps, 
						       NULL, 0, 0))
	            {
                    SensorCall();status = 0;
                    SensorCall();break;
		    }
	          SensorCall();break;
          default: SensorCall();TIFFError ("readSeparateTilesIntoBuffer", "Unsupported bit depth: %d", bps);
                  status = 0;
                  SensorCall();break;
          }
        }
      }
    }

  SensorCall();for (sample = 0; (sample < spp) && (sample < MAX_SAMPLES); sample++)
    {
    SensorCall();tbuff = srcbuffs[sample];
    SensorCall();if (tbuff != NULL)
      {/*45*/SensorCall();_TIFFfree(tbuff);/*46*/}
    }
 
  {int  ReplaceReturn = status; SensorCall(); return ReplaceReturn;}
  }

static int writeBufferToContigStrips(TIFF* out, uint8* buf, uint32 imagelength)
  {
  SensorCall();uint32 row, nrows, rowsperstrip;
  tstrip_t strip = 0;
  tsize_t stripsize;

  TIFFGetFieldDefaulted(out, TIFFTAG_ROWSPERSTRIP, &rowsperstrip);
  SensorCall();for (row = 0; row < imagelength; row += rowsperstrip)
    {
    SensorCall();nrows = (row + rowsperstrip > imagelength) ?
	     imagelength - row : rowsperstrip;
    stripsize = TIFFVStripSize(out, nrows);
    SensorCall();if (TIFFWriteEncodedStrip(out, strip++, buf, stripsize) < 0)
      {
      SensorCall();TIFFError(TIFFFileName(out), "Error, can't write strip %u", strip - 1);
      {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
      }
    SensorCall();buf += stripsize;
    }

  {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
  }

/* Abandon plans to modify code so that plannar orientation separate images
 * do not have all samples for each channel written before all samples
 * for the next channel have been abandoned.
 * Libtiff internals seem to depend on all data for a given sample
 * being contiguous within a strip or tile when PLANAR_CONFIG is 
 * separate. All strips or tiles of a given plane are written
 * before any strips or tiles of a different plane are stored.
 */
static int 
writeBufferToSeparateStrips (TIFF* out, uint8* buf, 
			     uint32 length, uint32 width, uint16 spp,
			     struct dump_opts *dump)
  {
  SensorCall();uint8   *src;
  uint16   bps;
  uint32   row, nrows, rowsize, rowsperstrip;
  uint32   bytes_per_sample;
  tsample_t s;
  tstrip_t strip = 0;
  tsize_t  stripsize = TIFFStripSize(out);
  tsize_t  rowstripsize,  scanlinesize = TIFFScanlineSize(out);
  tsize_t  total_bytes = 0;
  tdata_t  obuf;

  (void) TIFFGetFieldDefaulted(out, TIFFTAG_ROWSPERSTRIP, &rowsperstrip);
  (void) TIFFGetField(out, TIFFTAG_BITSPERSAMPLE, &bps);
  bytes_per_sample = (bps + 7) / 8;
  rowsize = ((bps * spp * width) + 7) / 8; /* source has interleaved samples */
  rowstripsize = rowsperstrip * bytes_per_sample * (width + 1); 

  obuf = _TIFFmalloc (rowstripsize);
  SensorCall();if (obuf == NULL)
    {/*55*/{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}/*56*/}
  
  SensorCall();for (s = 0; s < spp; s++)
    {
    SensorCall();for (row = 0; row < length; row += rowsperstrip)
      {
      SensorCall();nrows = (row + rowsperstrip > length) ? length - row : rowsperstrip;

      stripsize = TIFFVStripSize(out, nrows);
      src = buf + (row * rowsize);
      total_bytes += stripsize;
      memset (obuf, '\0', rowstripsize);
      SensorCall();if (extractContigSamplesToBuffer(obuf, src, nrows, width, s, spp, bps, dump))
        {
        SensorCall();_TIFFfree(obuf);
        {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
	}
      SensorCall();if ((dump->outfile != NULL) && (dump->level == 1))
        {
        SensorCall();dump_info(dump->outfile, dump->format,"", 
                  "Sample %2d, Strip: %2d, bytes: %4d, Row %4d, bytes: %4d, Input offset: %6d", 
                  s + 1, strip + 1, stripsize, row + 1, scanlinesize, src - buf);
        dump_buffer(dump->outfile, dump->format, nrows, scanlinesize, row, obuf);
	}

      SensorCall();if (TIFFWriteEncodedStrip(out, strip++, obuf, stripsize) < 0)
        {
	SensorCall();TIFFError(TIFFFileName(out), "Error, can't write strip %u", strip - 1);
	_TIFFfree(obuf);
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
	}
      }
    }      

  SensorCall();_TIFFfree(obuf);
  {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

/* Extract all planes from contiguous buffer into a single tile buffer 
 * to be written out as a tile.
 */
static int writeBufferToContigTiles (TIFF* out, uint8* buf, uint32 imagelength,
				       uint32 imagewidth, tsample_t spp, 
                                       struct dump_opts* dump)
  {
  SensorCall();uint16 bps;
  uint32 tl, tw;
  uint32 row, col, nrow, ncol;
  uint32 src_rowsize, col_offset;
  uint32 tile_rowsize  = TIFFTileRowSize(out);
  uint8* bufp = (uint8*) buf;
  tsize_t tile_buffsize = 0;
  tsize_t tilesize = TIFFTileSize(out);
  unsigned char *tilebuf = NULL;

  SensorCall();if( !TIFFGetField(out, TIFFTAG_TILELENGTH, &tl) ||
      !TIFFGetField(out, TIFFTAG_TILEWIDTH, &tw) ||
      !TIFFGetField(out, TIFFTAG_BITSPERSAMPLE, &bps) )
      {/*47*/{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}/*48*/}

  SensorCall();if (tilesize == 0 || tile_rowsize == 0 || tl == 0 || tw == 0)
  {
    SensorCall();TIFFError("writeBufferToContigTiles", "Tile size, tile row size, tile width, or tile length is zero");
    exit(-1);
  }
  
  SensorCall();tile_buffsize = tilesize;
  SensorCall();if (tilesize < (tsize_t)(tl * tile_rowsize))
    {
#ifdef DEBUG2
    TIFFError("writeBufferToContigTiles",
	      "Tilesize %lu is too small, using alternate calculation %u",
              tilesize, tl * tile_rowsize);
#endif
    SensorCall();tile_buffsize = tl * tile_rowsize;
    SensorCall();if (tl != tile_buffsize / tile_rowsize)
    {
	SensorCall();TIFFError("writeBufferToContigTiles", "Integer overflow when calculating buffer size");
	exit(-1);
    }
    }

  SensorCall();tilebuf = _TIFFmalloc(tile_buffsize);
  SensorCall();if (tilebuf == 0)
    {/*49*/{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}/*50*/}

  SensorCall();src_rowsize = ((imagewidth * spp * bps) + 7) / 8;
  SensorCall();for (row = 0; row < imagelength; row += tl)
    {
    SensorCall();nrow = (row + tl > imagelength) ? imagelength - row : tl;
    SensorCall();for (col = 0; col < imagewidth; col += tw)
      {
      /* Calculate visible portion of tile. */
      SensorCall();if (col + tw > imagewidth)
	{/*51*/SensorCall();ncol = imagewidth - col;/*52*/}
      else
        {/*53*/SensorCall();ncol = tw;/*54*/}

      SensorCall();col_offset = (((col * bps * spp) + 7) / 8);
      bufp = buf + (row * src_rowsize) + col_offset;
      SensorCall();if (extractContigSamplesToTileBuffer(tilebuf, bufp, nrow, ncol, imagewidth,
					   tw, 0, spp, spp, bps, dump) > 0)
        {
	SensorCall();TIFFError("writeBufferToContigTiles", 
                  "Unable to extract data to tile for row %lu, col %lu",
                  (unsigned long) row, (unsigned long)col);
	_TIFFfree(tilebuf);
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
        }

      SensorCall();if (TIFFWriteTile(out, tilebuf, col, row, 0, 0) < 0)
        {
	SensorCall();TIFFError("writeBufferToContigTiles",
	          "Cannot write tile at %lu %lu",
	          (unsigned long) col, (unsigned long) row);
	 _TIFFfree(tilebuf);
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
	}
      }
    }
  SensorCall();_TIFFfree(tilebuf);

  {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
  } /* end writeBufferToContigTiles */

/* Extract each plane from contiguous buffer into a single tile buffer 
 * to be written out as a tile.
 */
static int writeBufferToSeparateTiles (TIFF* out, uint8* buf, uint32 imagelength,
				       uint32 imagewidth, tsample_t spp, 
                                       struct dump_opts * dump)
  {
  SensorCall();tdata_t obuf = _TIFFmalloc(TIFFTileSize(out));
  uint32 tl, tw;
  uint32 row, col, nrow, ncol;
  uint32 src_rowsize, col_offset;
  uint16 bps;
  tsample_t s;
  uint8* bufp = (uint8*) buf;

  SensorCall();if (obuf == NULL)
    {/*57*/{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}/*58*/}

  SensorCall();TIFFGetField(out, TIFFTAG_TILELENGTH, &tl);
  TIFFGetField(out, TIFFTAG_TILEWIDTH, &tw);
  TIFFGetField(out, TIFFTAG_BITSPERSAMPLE, &bps);
  src_rowsize = ((imagewidth * spp * bps) + 7) / 8;
         
  SensorCall();for (row = 0; row < imagelength; row += tl)
    {
    SensorCall();nrow = (row + tl > imagelength) ? imagelength - row : tl;
    SensorCall();for (col = 0; col < imagewidth; col += tw)
      {
      /* Calculate visible portion of tile. */
      SensorCall();if (col + tw > imagewidth)
	{/*59*/SensorCall();ncol = imagewidth - col;/*60*/}
      else
        {/*61*/SensorCall();ncol = tw;/*62*/}

      SensorCall();col_offset = (((col * bps * spp) + 7) / 8);
      bufp = buf + (row * src_rowsize) + col_offset;

      SensorCall();for (s = 0; s < spp; s++)
        {
	SensorCall();if (extractContigSamplesToTileBuffer(obuf, bufp, nrow, ncol, imagewidth,
					     tw, s, 1, spp, bps, dump) > 0)
          {
	  SensorCall();TIFFError("writeBufferToSeparateTiles", 
                    "Unable to extract data to tile for row %lu, col %lu sample %d",
                    (unsigned long) row, (unsigned long)col, (int)s);
	  _TIFFfree(obuf);
	  {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
          }

	SensorCall();if (TIFFWriteTile(out, obuf, col, row, 0, s) < 0)
          {
	   SensorCall();TIFFError("writeBufferToseparateTiles",
	             "Cannot write tile at %lu %lu sample %lu",
	             (unsigned long) col, (unsigned long) row,
	             (unsigned long) s);
	   _TIFFfree(obuf);
	   {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
	  }
	}
      }
    }
  SensorCall();_TIFFfree(obuf);

  {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
  } /* end writeBufferToSeparateTiles */

static void
processG3Options(char* cp)
{
	SensorCall();if( (cp = strchr(cp, ':')) ) {
		SensorCall();if (defg3opts == (uint32) -1)
			{/*627*/SensorCall();defg3opts = 0;/*628*/}
		SensorCall();do {
			SensorCall();cp++;
			SensorCall();if (strneq(cp, "1d", 2))
				defg3opts &= ~GROUP3OPT_2DENCODING;
			else {/*629*/SensorCall();if (strneq(cp, "2d", 2))
				defg3opts |= GROUP3OPT_2DENCODING;
			else {/*631*/SensorCall();if (strneq(cp, "fill", 4))
				defg3opts |= GROUP3OPT_FILLBITS;
			else
				{/*633*/SensorCall();usage();/*634*/}/*632*/}/*630*/}
		} while( (cp = strchr(cp, ':')) );
	}
SensorCall();}

static int
processCompressOptions(char* opt)
  {
  SensorCall();char* cp = NULL;

  SensorCall();if (strneq(opt, "none",4))
    {
    SensorCall();defcompression = COMPRESSION_NONE;
    }
  else {/*83*/SensorCall();if (streq(opt, "packbits"))
    {
    SensorCall();defcompression = COMPRESSION_PACKBITS;
    }
  else {/*85*/SensorCall();if (strneq(opt, "jpeg", 4))
    {
    SensorCall();cp = strchr(opt, ':');
    defcompression = COMPRESSION_JPEG;

    SensorCall();while (cp)
      {
      SensorCall();if (isdigit((int)cp[1]))
	{/*87*/SensorCall();quality = atoi(cp + 1);/*88*/}
      else {/*89*/SensorCall();if (strneq(cp + 1, "raw", 3 ))
	jpegcolormode = JPEGCOLORMODE_RAW;
      else {/*91*/SensorCall();if (strneq(cp + 1, "rgb", 3 ))
	jpegcolormode = JPEGCOLORMODE_RGB;
      else
	{/*93*/SensorCall();usage();/*94*/}/*92*/}/*90*/}
      SensorCall();cp = strchr(cp + 1, ':');
      }
    }
  else {/*95*/SensorCall();if (strneq(opt, "g3", 2))
    {
    SensorCall();processG3Options(opt);
    defcompression = COMPRESSION_CCITTFAX3;
    }
  else {/*97*/SensorCall();if (streq(opt, "g4"))
    {
    SensorCall();defcompression = COMPRESSION_CCITTFAX4;
    }
  else {/*99*/SensorCall();if (strneq(opt, "lzw", 3))
    {
    SensorCall();cp = strchr(opt, ':');
    SensorCall();if (cp)
      {/*101*/SensorCall();defpredictor = atoi(cp+1);/*102*/}
    SensorCall();defcompression = COMPRESSION_LZW;
    }
  else {/*103*/SensorCall();if (strneq(opt, "zip", 3))
    {
    SensorCall();cp = strchr(opt, ':');
    SensorCall();if (cp)
      {/*105*/SensorCall();defpredictor = atoi(cp+1);/*106*/}
    SensorCall();defcompression = COMPRESSION_ADOBE_DEFLATE;
   }
  else
    {/*107*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*108*/}/*104*/}/*100*/}/*98*/}/*96*/}/*86*/}/*84*/}

  {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
  }

static void
usage(void)
  {
  SensorCall();int i;

  fprintf(stderr, "\n%s\n", TIFFGetVersion());
  SensorCall();for (i = 0; usage_info[i] != NULL; i++)
    {/*109*/SensorCall();fprintf(stderr, "%s\n", usage_info[i]);/*110*/}
  SensorCall();exit(-1);
  SensorCall();}

#define	CopyField(tag, v) \
    if (TIFFGetField(in, tag, &v)) TIFFSetField(out, tag, v)
#define	CopyField2(tag, v1, v2) \
    if (TIFFGetField(in, tag, &v1, &v2)) TIFFSetField(out, tag, v1, v2)
#define	CopyField3(tag, v1, v2, v3) \
    if (TIFFGetField(in, tag, &v1, &v2, &v3)) TIFFSetField(out, tag, v1, v2, v3)
#define	CopyField4(tag, v1, v2, v3, v4) \
    if (TIFFGetField(in, tag, &v1, &v2, &v3, &v4)) TIFFSetField(out, tag, v1, v2, v3, v4)

static void
cpTag(TIFF* in, TIFF* out, uint16 tag, uint16 count, TIFFDataType type)
{
	SensorCall();switch (type) {
	case TIFF_SHORT:
		SensorCall();if (count == 1) {
			SensorCall();uint16 shortv;
			CopyField(tag, shortv);
		} else {/*635*/SensorCall();if (count == 2) {
			SensorCall();uint16 shortv1, shortv2;
			CopyField2(tag, shortv1, shortv2);
		} else {/*637*/SensorCall();if (count == 4) {
			SensorCall();uint16 *tr, *tg, *tb, *ta;
			CopyField4(tag, tr, tg, tb, ta);
		} else {/*639*/SensorCall();if (count == (uint16) -1) {
			SensorCall();uint16 shortv1;
			uint16* shortav;
			CopyField2(tag, shortv1, shortav);
		;/*640*/}/*638*/}/*636*/}}
		SensorCall();break;
	case TIFF_LONG:
		{ SensorCall();uint32 longv;
		  CopyField(tag, longv);
		}
		SensorCall();break;
	case TIFF_RATIONAL:
		SensorCall();if (count == 1) {
			SensorCall();float floatv;
			CopyField(tag, floatv);
		} else {/*641*/SensorCall();if (count == (uint16) -1) {
			SensorCall();float* floatav;
			CopyField(tag, floatav);
		;/*642*/}}
		SensorCall();break;
	case TIFF_ASCII:
		{ SensorCall();char* stringv;
		  CopyField(tag, stringv);
		}
		SensorCall();break;
	case TIFF_DOUBLE:
		SensorCall();if (count == 1) {
			SensorCall();double doublev;
			CopyField(tag, doublev);
		} else {/*643*/SensorCall();if (count == (uint16) -1) {
			SensorCall();double* doubleav;
			CopyField(tag, doubleav);
		;/*644*/}}
		SensorCall();break;
          default:
                SensorCall();TIFFError(TIFFFileName(in),
                          "Data type %d is not supported, tag %d skipped",
                          tag, type);
	}
SensorCall();}

static struct cpTag {
	uint16	tag;
	uint16	count;
	TIFFDataType type;
} tags[] = {
	{ TIFFTAG_SUBFILETYPE,		1, TIFF_LONG },
	{ TIFFTAG_THRESHHOLDING,	1, TIFF_SHORT },
	{ TIFFTAG_DOCUMENTNAME,		1, TIFF_ASCII },
	{ TIFFTAG_IMAGEDESCRIPTION,	1, TIFF_ASCII },
	{ TIFFTAG_MAKE,			1, TIFF_ASCII },
	{ TIFFTAG_MODEL,		1, TIFF_ASCII },
	{ TIFFTAG_MINSAMPLEVALUE,	1, TIFF_SHORT },
	{ TIFFTAG_MAXSAMPLEVALUE,	1, TIFF_SHORT },
	{ TIFFTAG_XRESOLUTION,		1, TIFF_RATIONAL },
	{ TIFFTAG_YRESOLUTION,		1, TIFF_RATIONAL },
	{ TIFFTAG_PAGENAME,		1, TIFF_ASCII },
	{ TIFFTAG_XPOSITION,		1, TIFF_RATIONAL },
	{ TIFFTAG_YPOSITION,		1, TIFF_RATIONAL },
	{ TIFFTAG_RESOLUTIONUNIT,	1, TIFF_SHORT },
	{ TIFFTAG_SOFTWARE,		1, TIFF_ASCII },
	{ TIFFTAG_DATETIME,		1, TIFF_ASCII },
	{ TIFFTAG_ARTIST,		1, TIFF_ASCII },
	{ TIFFTAG_HOSTCOMPUTER,		1, TIFF_ASCII },
	{ TIFFTAG_WHITEPOINT,		(uint16) -1, TIFF_RATIONAL },
	{ TIFFTAG_PRIMARYCHROMATICITIES,(uint16) -1,TIFF_RATIONAL },
	{ TIFFTAG_HALFTONEHINTS,	2, TIFF_SHORT },
	{ TIFFTAG_INKSET,		1, TIFF_SHORT },
	{ TIFFTAG_DOTRANGE,		2, TIFF_SHORT },
	{ TIFFTAG_TARGETPRINTER,	1, TIFF_ASCII },
	{ TIFFTAG_SAMPLEFORMAT,		1, TIFF_SHORT },
	{ TIFFTAG_YCBCRCOEFFICIENTS,	(uint16) -1,TIFF_RATIONAL },
	{ TIFFTAG_YCBCRSUBSAMPLING,	2, TIFF_SHORT },
	{ TIFFTAG_YCBCRPOSITIONING,	1, TIFF_SHORT },
	{ TIFFTAG_REFERENCEBLACKWHITE,	(uint16) -1,TIFF_RATIONAL },
	{ TIFFTAG_EXTRASAMPLES,		(uint16) -1, TIFF_SHORT },
	{ TIFFTAG_SMINSAMPLEVALUE,	1, TIFF_DOUBLE },
	{ TIFFTAG_SMAXSAMPLEVALUE,	1, TIFF_DOUBLE },
	{ TIFFTAG_STONITS,		1, TIFF_DOUBLE },
};
#define	NTAGS	(sizeof (tags) / sizeof (tags[0]))

#define	CopyTag(tag, count, type)	cpTag(in, out, tag, count, type)

/* Functions written by Richard Nolde, with exceptions noted. */
void  process_command_opts (int argc, char *argv[], char *mp, char *mode, uint32 *dirnum,
	                    uint16 *defconfig, uint16 *deffillorder, uint32 *deftilewidth,
                            uint32 *deftilelength, uint32 *defrowsperstrip,
		            struct crop_mask *crop_data, struct pagedef *page, 
                            struct dump_opts *dump,
                            unsigned int     *imagelist, unsigned int   *image_count )
    {
    SensorCall();int   c, good_args = 0;
    char *opt_offset   = NULL;    /* Position in string of value sought */
    char *opt_ptr      = NULL;    /* Pointer to next token in option set */
    char *sep          = NULL;    /* Pointer to a token separator */
    unsigned int  i, j, start, end;
    extern int   optind;
    extern char* optarg;

    *mp++ = 'w';
    *mp = '\0';
    SensorCall();while ((c = getopt(argc, argv,
       "ac:d:e:f:hil:m:p:r:stvw:z:BCD:E:F:H:I:J:K:LMN:O:P:R:S:U:V:X:Y:Z:")) != -1)
      {
    SensorCall();good_args++;
    SensorCall();switch (c) {
      case 'a': SensorCall();mode[0] = 'a';	/* append to output */
		SensorCall();break;
      case 'c':	SensorCall();if (!processCompressOptions(optarg)) /* compression scheme */
		  {
		  SensorCall();TIFFError ("Unknown compression option", "%s", optarg);
                  TIFFError ("For valid options type", "tiffcrop -h");
                  exit (-1);
                  }
		SensorCall();break;
      case 'd':	SensorCall();start = strtoul(optarg, NULL, 0); /* initial IFD offset */
	        SensorCall();if (start == 0)
                  {
		  SensorCall();TIFFError ("","Directory offset must be greater than zero");
		  TIFFError ("For valid options type", "tiffcrop -h");
                  exit (-1);
		  }
	        SensorCall();*dirnum = start - 1;
		SensorCall();break;
      case 'e': SensorCall();switch (tolower(optarg[0])) /* image export modes*/
                  {
		  case 'c': SensorCall();crop_data->exp_mode = ONE_FILE_COMPOSITE;
 		            crop_data->img_mode = COMPOSITE_IMAGES;
		            SensorCall();break; /* Composite */
		  case 'd': SensorCall();crop_data->exp_mode = ONE_FILE_SEPARATED;
 		            crop_data->img_mode = SEPARATED_IMAGES;
		            SensorCall();break; /* Divided */
		  case 'i': SensorCall();crop_data->exp_mode = FILE_PER_IMAGE_COMPOSITE;
 		            crop_data->img_mode = COMPOSITE_IMAGES;
		            SensorCall();break; /* Image */
		  case 'm': SensorCall();crop_data->exp_mode = FILE_PER_IMAGE_SEPARATED;
 		            crop_data->img_mode = SEPARATED_IMAGES;
		            SensorCall();break; /* Multiple */
		  case 's': SensorCall();crop_data->exp_mode = FILE_PER_SELECTION;
 		            crop_data->img_mode = SEPARATED_IMAGES;
		            SensorCall();break; /* Sections */
		  default:  SensorCall();TIFFError ("Unknown export mode","%s", optarg);
                            TIFFError ("For valid options type", "tiffcrop -h");
                            exit (-1);
                  }
	        SensorCall();break;
      case 'f':	SensorCall();if (streq(optarg, "lsb2msb"))	   /* fill order */
		  *deffillorder = FILLORDER_LSB2MSB;
		else {/*113*/SensorCall();if (streq(optarg, "msb2lsb"))
		  *deffillorder = FILLORDER_MSB2LSB;
		else
		  {
		  SensorCall();TIFFError ("Unknown fill order", "%s", optarg);
                  TIFFError ("For valid options type", "tiffcrop -h");
                  exit (-1);
                  ;/*114*/}}
		SensorCall();break;
      case 'h':	SensorCall();usage();
		SensorCall();break;
      case 'i':	SensorCall();ignore = TRUE;		/* ignore errors */
		SensorCall();break;
      case 'l':	SensorCall();outtiled = TRUE;	 /* tile length */
		*deftilelength = atoi(optarg);
		SensorCall();break;
      case 'p': /* planar configuration */
		SensorCall();if (streq(optarg, "separate"))
		  *defconfig = PLANARCONFIG_SEPARATE;
		else {/*115*/SensorCall();if (streq(optarg, "contig"))
		  *defconfig = PLANARCONFIG_CONTIG;
		else
		  {
		  SensorCall();TIFFError ("Unkown planar configuration", "%s", optarg);
                  TIFFError ("For valid options type", "tiffcrop -h");
                  exit (-1);
                  ;/*116*/}}
		SensorCall();break;
      case 'r':	/* rows/strip */
		SensorCall();*defrowsperstrip = atol(optarg);
		SensorCall();break;
      case 's':	/* generate stripped output */
		SensorCall();outtiled = FALSE;
		SensorCall();break;
      case 't':	/* generate tiled output */
		SensorCall();outtiled = TRUE;
		SensorCall();break;
      case 'v': SensorCall();TIFFError("Library Release", "%s", TIFFGetVersion());
                TIFFError ("Tiffcrop version", "%s, last updated: %s", 
			   tiffcrop_version_id, tiffcrop_rev_date);
 	        TIFFError ("Tiffcp code", "Copyright (c) 1988-1997 Sam Leffler");
		TIFFError ("           ", "Copyright (c) 1991-1997 Silicon Graphics, Inc");
                TIFFError ("Tiffcrop additions", "Copyright (c) 2007-2010 Richard Nolde");
	        exit (0);
		SensorCall();break;
      case 'w':	/* tile width */
		SensorCall();outtiled = TRUE;
		*deftilewidth = atoi(optarg);
		SensorCall();break;
      case 'z': /* regions of an image specified as x1,y1,x2,y2:x3,y3,x4,y4 etc */
	        SensorCall();crop_data->crop_mode |= CROP_REGIONS;
		SensorCall();for (i = 0, opt_ptr = strtok (optarg, ":");
                   ((opt_ptr != NULL) &&  (i < MAX_REGIONS));
                    (opt_ptr = strtok (NULL, ":")), i++)
                    {
		    SensorCall();crop_data->regions++;
                    SensorCall();if (sscanf(opt_ptr, "%lf,%lf,%lf,%lf",
			       &crop_data->corners[i].X1, &crop_data->corners[i].Y1,
			       &crop_data->corners[i].X2, &crop_data->corners[i].Y2) != 4)
                      {
                      SensorCall();TIFFError ("Unable to parse coordinates for region", "%d %s", i, optarg);
		      TIFFError ("For valid options type", "tiffcrop -h");
                      exit (-1);
		      }
                    }
                /*  check for remaining elements over MAX_REGIONS */
                SensorCall();if ((opt_ptr != NULL) && (i >= MAX_REGIONS))
                  {
		  SensorCall();TIFFError ("Region list exceeds limit of", "%d regions %s", MAX_REGIONS, optarg);
		  TIFFError ("For valid options type", "tiffcrop -h");
                  exit (-1);;
                  }
		SensorCall();break;
      /* options for file open modes */
      case 'B': SensorCall();*mp++ = 'b'; *mp = '\0';
		SensorCall();break;
      case 'L': SensorCall();*mp++ = 'l'; *mp = '\0';
		SensorCall();break;
      case 'M': SensorCall();*mp++ = 'm'; *mp = '\0';
		SensorCall();break;
      case 'C': SensorCall();*mp++ = 'c'; *mp = '\0';
		SensorCall();break;
      /* options for Debugging / data dump */
      case 'D': SensorCall();for (i = 0, opt_ptr = strtok (optarg, ",");
                    (opt_ptr != NULL);
                    (opt_ptr = strtok (NULL, ",")), i++)
                    {
		    SensorCall();opt_offset = strpbrk(opt_ptr, ":=");
                    SensorCall();if (opt_offset == NULL)
                      {
                      SensorCall();TIFFError("Invalid dump option", "%s", optarg);
                      TIFFError ("For valid options type", "tiffcrop -h");
                      exit (-1);
		      }
                      
                    SensorCall();*opt_offset = '\0';
                    /* convert option to lowercase */
                    end = strlen (opt_ptr);
                    SensorCall();for (i = 0; i < end; i++)
                      {/*117*/SensorCall();*(opt_ptr + i) = tolower(*(opt_ptr + i));/*118*/}
                    /* Look for dump format specification */
                    SensorCall();if (strncmp(opt_ptr, "for", 3) == 0)
                      {
		      /* convert value to lowercase */
                      SensorCall();end = strlen (opt_offset + 1);
                      SensorCall();for (i = 1; i <= end; i++)
                        {/*119*/SensorCall();*(opt_offset + i) = tolower(*(opt_offset + i));/*120*/}
                      /* check dump format value */
		      SensorCall();if (strncmp (opt_offset + 1, "txt", 3) == 0)
                        {
                        SensorCall();dump->format = DUMP_TEXT;
                        strcpy (dump->mode, "w");
                        }
                      else
                        {
		        SensorCall();if (strncmp(opt_offset + 1, "raw", 3) == 0)
                          {
                          SensorCall();dump->format = DUMP_RAW;
                          strcpy (dump->mode, "wb");
                          }
                        else
                          {
                          SensorCall();TIFFError("parse_command_opts", "Unknown dump format %s", opt_offset + 1);
                          TIFFError ("For valid options type", "tiffcrop -h");
                          exit (-1);
		          }
			}
                      }
		    else
                      { /* Look for dump level specification */
                      SensorCall();if (strncmp (opt_ptr, "lev", 3) == 0)
                        {/*121*/SensorCall();dump->level = atoi(opt_offset + 1);/*122*/}
                        /* Look for input data dump file name */
                      SensorCall();if (strncmp (opt_ptr, "in", 2) == 0)
		        {
                        SensorCall();strncpy (dump->infilename, opt_offset + 1, PATH_MAX - 20);
                        dump->infilename[PATH_MAX - 20] = '\0';
                        }
                        /* Look for output data dump file name */
                      SensorCall();if (strncmp (opt_ptr, "out", 3) == 0)
			{
                        SensorCall();strncpy (dump->outfilename, opt_offset + 1, PATH_MAX - 20);
                        dump->outfilename[PATH_MAX - 20] = '\0';
                        }
                      SensorCall();if (strncmp (opt_ptr, "deb", 3) == 0)
			{/*123*/SensorCall();dump->debug = atoi(opt_offset + 1);/*124*/}
		      }
                    }
	        SensorCall();if ((strlen(dump->infilename)) || (strlen(dump->outfilename)))
                  {
		  SensorCall();if (dump->level == 1)
                    {/*125*/SensorCall();TIFFError("","Defaulting to dump level 1, no data.");/*126*/}
	          SensorCall();if (dump->format == DUMP_NONE)
                    {
		    SensorCall();TIFFError("", "You must specify a dump format for dump files");
		    TIFFError ("For valid options type", "tiffcrop -h");
		    exit (-1);
		    }
                  }
	        SensorCall();break;

      /* image manipulation routine options */
      case 'm': /* margins to exclude from selection, uppercase M was already used */
		/* order of values must be TOP, LEFT, BOTTOM, RIGHT */
		SensorCall();crop_data->crop_mode |= CROP_MARGINS;
                SensorCall();for (i = 0, opt_ptr = strtok (optarg, ",:");
                    ((opt_ptr != NULL) &&  (i < 4));
                     (opt_ptr = strtok (NULL, ",:")), i++)
                    {
		    SensorCall();crop_data->margins[i] = atof(opt_ptr);
                    }
		SensorCall();break;
      case 'E':	/* edge reference */
		SensorCall();switch (tolower(optarg[0]))
                  {
		  case 't': SensorCall();crop_data->edge_ref = EDGE_TOP;
                            SensorCall();break;
                  case 'b': SensorCall();crop_data->edge_ref = EDGE_BOTTOM;
                             SensorCall();break;
                  case 'l': SensorCall();crop_data->edge_ref = EDGE_LEFT;
                            SensorCall();break;
                  case 'r': SensorCall();crop_data->edge_ref = EDGE_RIGHT;
                            SensorCall();break;
		  default:  SensorCall();TIFFError ("Edge reference must be top, bottom, left, or right", "%s", optarg);
			    TIFFError ("For valid options type", "tiffcrop -h");
                            exit (-1);
		  }
		SensorCall();break;
      case 'F': /* flip eg mirror image or cropped segment, M was already used */
		SensorCall();crop_data->crop_mode |= CROP_MIRROR;
		SensorCall();switch (tolower(optarg[0]))
                  {
		  case  'h': SensorCall();crop_data->mirror = MIRROR_HORIZ;
                             SensorCall();break;
                  case  'v': SensorCall();crop_data->mirror = MIRROR_VERT;
                             SensorCall();break;
                  case  'b': SensorCall();crop_data->mirror = MIRROR_BOTH;
                             SensorCall();break;
		  default:   SensorCall();TIFFError ("Flip mode must be horiz, vert, or both", "%s", optarg);
			     TIFFError ("For valid options type", "tiffcrop -h");
                             exit (-1);
		  }
		SensorCall();break;
      case 'H': /* set horizontal resolution to new value */
		SensorCall();page->hres = atof (optarg);
                page->mode |= PAGE_MODE_RESOLUTION;
		SensorCall();break;
      case 'I': /* invert the color space, eg black to white */
		SensorCall();crop_data->crop_mode |= CROP_INVERT;
                /* The PHOTOMETIC_INTERPRETATION tag may be updated */
                SensorCall();if (streq(optarg, "black"))
                  {
		  SensorCall();crop_data->photometric = PHOTOMETRIC_MINISBLACK;
		  SensorCall();continue;
                  }
                SensorCall();if (streq(optarg, "white"))
                  {
		  SensorCall();crop_data->photometric = PHOTOMETRIC_MINISWHITE;
                  SensorCall();continue;
                  }
                SensorCall();if (streq(optarg, "data")) 
                  {
		  SensorCall();crop_data->photometric = INVERT_DATA_ONLY;
                  SensorCall();continue;
                  }
                SensorCall();if (streq(optarg, "both"))
                  {
		  SensorCall();crop_data->photometric = INVERT_DATA_AND_TAG;
                  SensorCall();continue;
                  }

		SensorCall();TIFFError("Missing or unknown option for inverting PHOTOMETRIC_INTERPRETATION", "%s", optarg);
		TIFFError ("For valid options type", "tiffcrop -h");
                exit (-1);
		SensorCall();break;
      case 'J': /* horizontal margin for sectioned ouput pages */ 
		SensorCall();page->hmargin = atof(optarg);
                page->mode |= PAGE_MODE_MARGINS;
		SensorCall();break;
      case 'K': /* vertical margin for sectioned ouput pages*/ 
                SensorCall();page->vmargin = atof(optarg);
                page->mode |= PAGE_MODE_MARGINS;
		SensorCall();break;
      case 'N':	/* list of images to process */
                SensorCall();for (i = 0, opt_ptr = strtok (optarg, ",");
                    ((opt_ptr != NULL) &&  (i < MAX_IMAGES));
                     (opt_ptr = strtok (NULL, ",")))
                     { /* We do not know how many images are in file yet 
			* so we build a list to include the maximum allowed
                        * and follow it until we hit the end of the file.
                        * Image count is not accurate for odd, even, last
                        * so page numbers won't be valid either.
                        */
		     SensorCall();if (streq(opt_ptr, "odd"))
                       {
		       SensorCall();for (j = 1; j <= MAX_IMAGES; j += 2)
			 {/*127*/SensorCall();imagelist[i++] = j;/*128*/}
                       SensorCall();*image_count = (MAX_IMAGES - 1) / 2;
                       SensorCall();break;
		       }
		     else
                       {
		       SensorCall();if (streq(opt_ptr, "even"))
                         {
			 SensorCall();for (j = 2; j <= MAX_IMAGES; j += 2)
			   {/*129*/SensorCall();imagelist[i++] = j;/*130*/}
                         SensorCall();*image_count = MAX_IMAGES / 2;
                         SensorCall();break;
			 }
		       else
                         {
			 SensorCall();if (streq(opt_ptr, "last"))
			   {/*131*/SensorCall();imagelist[i++] = MAX_IMAGES;/*132*/}
			 else  /* single value between commas */
			   {
			   SensorCall();sep = strpbrk(opt_ptr, ":-");
			   SensorCall();if (!sep)
			     {/*133*/SensorCall();imagelist[i++] = atoi(opt_ptr);/*134*/}
                           else
                             {
			     SensorCall();*sep = '\0';
                             start = atoi (opt_ptr);
                             SensorCall();if (!strcmp((sep + 1), "last"))
			       {/*135*/SensorCall();end = MAX_IMAGES;/*136*/}
                             else
                               {/*137*/SensorCall();end = atoi (sep + 1);/*138*/}
                             SensorCall();for (j = start; j <= end && j - start + i < MAX_IMAGES; j++)
			       {/*139*/SensorCall();imagelist[i++] = j;/*140*/}
			     }
			   }
			 }
		      }
		    }
                SensorCall();*image_count = i;
		SensorCall();break;
      case 'O': /* page orientation */ 
		SensorCall();switch (tolower(optarg[0]))
                  {
		  case  'a': SensorCall();page->orient = ORIENTATION_AUTO;
                             SensorCall();break;
		  case  'p': SensorCall();page->orient = ORIENTATION_PORTRAIT;
                             SensorCall();break;
		  case  'l': SensorCall();page->orient = ORIENTATION_LANDSCAPE;
                             SensorCall();break;
		  default:  SensorCall();TIFFError ("Orientation must be portrait, landscape, or auto.", "%s", optarg);
			    TIFFError ("For valid options type", "tiffcrop -h");
                            exit (-1);
		  }
		SensorCall();break;
      case 'P': /* page size selection */ 
	        SensorCall();if (sscanf(optarg, "%lfx%lf", &page->width, &page->length) == 2)
                  {
                  SensorCall();strcpy (page->name, "Custom"); 
                  page->mode |= PAGE_MODE_PAPERSIZE;
                  SensorCall();break;
                  }
                SensorCall();if (get_page_geometry (optarg, page))
                  {
		  SensorCall();if (!strcmp(optarg, "list"))
                    {
		    SensorCall();TIFFError("", "Name            Width   Length (in inches)");
                    SensorCall();for (i = 0; i < MAX_PAPERNAMES - 1; i++)
                      {/*141*/SensorCall();TIFFError ("", "%-15.15s %5.2f   %5.2f", 
			       PaperTable[i].name, PaperTable[i].width, 
                               PaperTable[i].length);/*142*/}
		    SensorCall();exit (-1);                   
                    }
     
		  SensorCall();TIFFError ("Invalid paper size", "%s", optarg);
                  TIFFError ("", "Select one of:");
		  TIFFError("", "Name            Width   Length (in inches)");
                  SensorCall();for (i = 0; i < MAX_PAPERNAMES - 1; i++)
                    {/*143*/SensorCall();TIFFError ("", "%-15.15s %5.2f   %5.2f", 
			       PaperTable[i].name, PaperTable[i].width, 
                               PaperTable[i].length);/*144*/}
		  SensorCall();exit (-1);
		  }
		else
                  {
                  SensorCall();page->mode |= PAGE_MODE_PAPERSIZE;
		  }
		SensorCall();break;
      case 'R': /* rotate image or cropped segment */
		SensorCall();crop_data->crop_mode |= CROP_ROTATE;
		SensorCall();switch (strtoul(optarg, NULL, 0))
                  {
		  case  90:  SensorCall();crop_data->rotation = (uint16)90;
                             SensorCall();break;
                  case  180: SensorCall();crop_data->rotation = (uint16)180;
                             SensorCall();break;
                  case  270: SensorCall();crop_data->rotation = (uint16)270;
                             SensorCall();break;
		  default:   SensorCall();TIFFError ("Rotation must be 90, 180, or 270 degrees clockwise", "%s", optarg);
			     TIFFError ("For valid options type", "tiffcrop -h");
                             exit (-1);
		  }
		SensorCall();break;
      case 'S':	/* subdivide into Cols:Rows sections, eg 3:2 would be 3 across and 2 down */
		SensorCall();sep = strpbrk(optarg, ",:");
		SensorCall();if (sep)
                  {
                  SensorCall();*sep = '\0';
                  page->cols = atoi(optarg);
                  page->rows = atoi(sep +1);
		  }
                else
                  {
                  SensorCall();page->cols = atoi(optarg);
                  page->rows = atoi(optarg);
		  }
                SensorCall();if ((page->cols * page->rows) > MAX_SECTIONS)
                  {
		  SensorCall();TIFFError ("Limit for subdivisions, ie rows x columns, exceeded", "%d", MAX_SECTIONS);
		  exit (-1);
                  }
                SensorCall();page->mode |= PAGE_MODE_ROWSCOLS;
		SensorCall();break;
      case 'U':	/* units for measurements and offsets */
		SensorCall();if (streq(optarg, "in"))
                  {
		  SensorCall();crop_data->res_unit = RESUNIT_INCH;
		  page->res_unit = RESUNIT_INCH;
		  }
		else {/*145*/SensorCall();if (streq(optarg, "cm"))
		  {
		  SensorCall();crop_data->res_unit = RESUNIT_CENTIMETER;
		  page->res_unit = RESUNIT_CENTIMETER;
		  }
		else {/*147*/SensorCall();if (streq(optarg, "px"))
		  {
		  SensorCall();crop_data->res_unit = RESUNIT_NONE;
		  page->res_unit = RESUNIT_NONE;
		  }
		else
                  {
		  SensorCall();TIFFError ("Illegal unit of measure","%s", optarg);
		  TIFFError ("For valid options type", "tiffcrop -h");
                  exit (-1);
		  ;/*148*/}/*146*/}}
		SensorCall();break;
      case 'V': /* set vertical resolution to new value */
		SensorCall();page->vres = atof (optarg);
                page->mode |= PAGE_MODE_RESOLUTION;
		SensorCall();break;
      case 'X':	/* selection width */
		SensorCall();crop_data->crop_mode |= CROP_WIDTH;
		crop_data->width = atof(optarg);
		SensorCall();break;
      case 'Y':	/* selection length */
		SensorCall();crop_data->crop_mode |= CROP_LENGTH;
		crop_data->length = atof(optarg);
		SensorCall();break;
      case 'Z': /* zones of an image X:Y read as zone X of Y */
		SensorCall();crop_data->crop_mode |= CROP_ZONES;
		SensorCall();for (i = 0, opt_ptr = strtok (optarg, ",");
                   ((opt_ptr != NULL) &&  (i < MAX_REGIONS));
                    (opt_ptr = strtok (NULL, ",")), i++)
                    {
		    SensorCall();crop_data->zones++;
		    opt_offset = strchr(opt_ptr, ':');
                    *opt_offset = '\0';
                    crop_data->zonelist[i].position = atoi(opt_ptr);
                    crop_data->zonelist[i].total    = atoi(opt_offset + 1);
                    }
                /*  check for remaining elements over MAX_REGIONS */
                SensorCall();if ((opt_ptr != NULL) && (i >= MAX_REGIONS))
                  {
		  SensorCall();TIFFError("Zone list exceeds region limit", "%d",  MAX_REGIONS);
		  exit (-1);
                  }
		SensorCall();break;
    case '?':	SensorCall();TIFFError ("For valid options type", "tiffcrop -h");
                exit (-1);
		/*NOTREACHED*/
      }
    }
  SensorCall();}  /* end process_command_opts */

/* Start a new output file if one has not been previously opened or
 * autoindex is set to non-zero. Update page and file counters
 * so TIFFTAG PAGENUM will be correct in image.
 */
static int 
update_output_file (TIFF **tiffout, char *mode, int autoindex,
                    char *outname, unsigned int *page)
  {
  SensorCall();static int findex = 0;    /* file sequence indicator */
  char  *sep;
  char   filenum[16];
  char   export_ext[16];
  char   exportname[PATH_MAX];

  SensorCall();if (autoindex && (*tiffout != NULL))
    {   
    /* Close any export file that was previously opened */
    SensorCall();TIFFClose (*tiffout);
    *tiffout = NULL;
    }

  SensorCall();strcpy (export_ext, ".tiff");
  memset (exportname, '\0', PATH_MAX);

  /* Leave room for page number portion of the new filename */
  strncpy (exportname, outname, PATH_MAX - 16);
  SensorCall();if (*tiffout == NULL)   /* This is a new export file */
    {
    SensorCall();if (autoindex)
      { /* create a new filename for each export */
      SensorCall();findex++;
      SensorCall();if ((sep = strstr(exportname, ".tif")) || (sep = strstr(exportname, ".TIF")))
        {
        SensorCall();strncpy (export_ext, sep, 5);
        *sep = '\0';
        }
      else
        {/*149*/SensorCall();strncpy (export_ext, ".tiff", 5);/*150*/}
      SensorCall();export_ext[5] = '\0';

      /* MAX_EXPORT_PAGES limited to 6 digits to prevent string overflow of pathname */
      SensorCall();if (findex > MAX_EXPORT_PAGES)
	{
	SensorCall();TIFFError("update_output_file", "Maximum of %d pages per file exceeded", MAX_EXPORT_PAGES);
        {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
        }

      SensorCall();snprintf(filenum, sizeof(filenum), "-%03d%s", findex, export_ext);
      filenum[14] = '\0';
      strncat (exportname, filenum, 15);
      }
    SensorCall();exportname[PATH_MAX - 1] = '\0';

    *tiffout = TIFFOpen(exportname, mode);
    SensorCall();if (*tiffout == NULL)
      {
      SensorCall();TIFFError("update_output_file", "Unable to open output file %s", exportname);
      {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
      }
    SensorCall();*page = 0; 

    {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
    }
  else 
    {/*151*/SensorCall();(*page)++;/*152*/}

  {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
  } /* end update_output_file */


int
main(int argc, char* argv[])
  {
  SensorCall();extern int optind;
  uint16 defconfig = (uint16) -1;
  uint16 deffillorder = 0;
  uint32 deftilewidth = (uint32) 0;
  uint32 deftilelength = (uint32) 0;
  uint32 defrowsperstrip = (uint32) 0;
  uint32 dirnum = 0;

  TIFF *in = NULL;
  TIFF *out = NULL;
  char  mode[10];
  char *mp = mode;

  /** RJN additions **/
  struct image_data image;     /* Image parameters for one image */
  struct crop_mask  crop;      /* Cropping parameters for all images */
  struct pagedef    page;      /* Page definition for output pages */
  struct pageseg    sections[MAX_SECTIONS];  /* Sections of one output page */
  struct buffinfo   seg_buffs[MAX_SECTIONS]; /* Segment buffer sizes and pointers */
  struct dump_opts  dump;                  /* Data dump options */
  unsigned char *read_buff    = NULL;      /* Input image data buffer */
  unsigned char *crop_buff    = NULL;      /* Crop area buffer */
  unsigned char *sect_buff    = NULL;      /* Image section buffer */
  unsigned char *sect_src     = NULL;      /* Image section buffer pointer */
  unsigned int  imagelist[MAX_IMAGES + 1]; /* individually specified images */
  unsigned int  image_count  = 0;
  unsigned int  dump_images  = 0;
  unsigned int  next_image   = 0;
  unsigned int  next_page    = 0;
  unsigned int  total_pages  = 0;
  unsigned int  total_images = 0;
  unsigned int  end_of_input = FALSE;
  int    seg, length;
  char   temp_filename[PATH_MAX + 1];

  little_endian = *((unsigned char *)&little_endian) & '1';

  initImageData(&image);
  initCropMasks(&crop);
  initPageSetup(&page, sections, seg_buffs);
  initDumpOptions(&dump);

  process_command_opts (argc, argv, mp, mode, &dirnum, &defconfig, 
                        &deffillorder, &deftilewidth, &deftilelength, &defrowsperstrip,
	                &crop, &page, &dump, imagelist, &image_count);

  SensorCall();if (argc - optind < 2)
    {/*645*/SensorCall();usage();/*646*/}

  SensorCall();if ((argc - optind) == 2)
    {/*647*/SensorCall();pageNum = -1;/*648*/}
  else
    {/*649*/SensorCall();total_images = 0;/*650*/}
  /* read multiple input files and write to output file(s) */
  SensorCall();while (optind < argc - 1)
    {
    SensorCall();in = TIFFOpen (argv[optind], "r");
    SensorCall();if (in == NULL)
      {/*651*/{int  ReplaceReturn = (-3); SensorCall(); return ReplaceReturn;}/*652*/}

    /* If only one input file is specified, we can use directory count */
    SensorCall();total_images = TIFFNumberOfDirectories(in); 
    SensorCall();if (image_count == 0)
      {
      SensorCall();dirnum = 0;
      total_pages = total_images; /* Only valid with single input file */
      }
    else
      {
      SensorCall();dirnum = (tdir_t)(imagelist[next_image] - 1);
      next_image++;

      /* Total pages only valid for enumerated list of pages not derived
       * using odd, even, or last keywords.
       */
      SensorCall();if (image_count >  total_images)
	{/*653*/SensorCall();image_count = total_images;/*654*/}
      
      SensorCall();total_pages = image_count;
      }

    /* MAX_IMAGES is used for special case "last" in selection list */
    SensorCall();if (dirnum == (MAX_IMAGES - 1))
      {/*655*/SensorCall();dirnum = total_images - 1;/*656*/}

    SensorCall();if (dirnum > (total_images))
      {
      SensorCall();TIFFError (TIFFFileName(in), 
      "Invalid image number %d, File contains only %d images", 
		 (int)dirnum + 1, total_images);
      SensorCall();if (out != NULL)
        {/*657*/SensorCall();(void) TIFFClose(out);/*658*/}
      {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
      }

    SensorCall();if (dirnum != 0 && !TIFFSetDirectory(in, (tdir_t)dirnum))
      {
      SensorCall();TIFFError(TIFFFileName(in),"Error, setting subdirectory at %d", dirnum);
      SensorCall();if (out != NULL)
        {/*659*/SensorCall();(void) TIFFClose(out);/*660*/}
      {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
      }

    SensorCall();end_of_input = FALSE;
    SensorCall();while (end_of_input == FALSE)
      {
      SensorCall();config = defconfig;
      compression = defcompression;
      predictor = defpredictor;
      fillorder = deffillorder;
      rowsperstrip = defrowsperstrip;
      tilewidth = deftilewidth;
      tilelength = deftilelength;
      g3opts = defg3opts;

      SensorCall();if (dump.format != DUMP_NONE)
        {
        /* manage input and/or output dump files here */
	SensorCall();dump_images++;
        length = strlen(dump.infilename);
        SensorCall();if (length > 0)
          {
          SensorCall();if (dump.infile != NULL)
            {/*661*/SensorCall();fclose (dump.infile);/*662*/}

          /* dump.infilename is guaranteed to be NUL termimated and have 20 bytes 
             fewer than PATH_MAX */ 
          SensorCall();snprintf(temp_filename, sizeof(temp_filename), "%s-read-%03d.%s",
		   dump.infilename, dump_images,
                  (dump.format == DUMP_TEXT) ? "txt" : "raw");
          SensorCall();if ((dump.infile = fopen(temp_filename, dump.mode)) == NULL)
            {
	    SensorCall();TIFFError ("Unable to open dump file for writing", "%s", temp_filename);
	    exit (-1);
            }
          SensorCall();dump_info(dump.infile, dump.format, "Reading image","%d from %s", 
                    dump_images, TIFFFileName(in));
          } 
        SensorCall();length = strlen(dump.outfilename);
        SensorCall();if (length > 0)
          {
          SensorCall();if (dump.outfile != NULL)
            {/*663*/SensorCall();fclose (dump.outfile);/*664*/}

          /* dump.outfilename is guaranteed to be NUL termimated and have 20 bytes 
             fewer than PATH_MAX */ 
          SensorCall();snprintf(temp_filename, sizeof(temp_filename), "%s-write-%03d.%s",
		   dump.outfilename, dump_images,
                  (dump.format == DUMP_TEXT) ? "txt" : "raw");
          SensorCall();if ((dump.outfile = fopen(temp_filename, dump.mode)) == NULL)
            {
	      SensorCall();TIFFError ("Unable to open dump file for writing", "%s", temp_filename);
	    exit (-1);
            }
          SensorCall();dump_info(dump.outfile, dump.format, "Writing image","%d from %s", 
                    dump_images, TIFFFileName(in));
          } 
        }

      SensorCall();if (dump.debug)
         {/*665*/SensorCall();TIFFError("main", "Reading image %4d of %4d total pages.", dirnum + 1, total_pages);/*666*/}

      SensorCall();if (loadImage(in, &image, &dump, &read_buff))
        {
        SensorCall();TIFFError("main", "Unable to load source image");
        exit (-1);
        }

      /* Correct the image orientation if it was not ORIENTATION_TOPLEFT.
       */
      SensorCall();if (image.adjustments != 0)
        {
	SensorCall();if (correct_orientation(&image, &read_buff))
	    {/*667*/SensorCall();TIFFError("main", "Unable to correct image orientation");/*668*/}
        }

      SensorCall();if (getCropOffsets(&image, &crop, &dump))
        {
        SensorCall();TIFFError("main", "Unable to define crop regions");
        exit (-1);
	}

      SensorCall();if (crop.selections > 0)
        {
        SensorCall();if (processCropSelections(&image, &crop, &read_buff, seg_buffs))
          {
          SensorCall();TIFFError("main", "Unable to process image selections");
          exit (-1);
	  }
	}
      else  /* Single image segment without zones or regions */
        {
        SensorCall();if (createCroppedImage(&image, &crop, &read_buff, &crop_buff))
          {
          SensorCall();TIFFError("main", "Unable to create output image");
          exit (-1);
	  }
	}
      SensorCall();if (page.mode == PAGE_MODE_NONE)
        {  /* Whole image or sections not based on output page size */
        SensorCall();if (crop.selections > 0)
          {
	  SensorCall();writeSelections(in, &out, &crop, &image, &dump, seg_buffs,
                          mp, argv[argc - 1], &next_page, total_pages);
          }
	else  /* One file all images and sections */
          {
	  SensorCall();if (update_output_file (&out, mp, crop.exp_mode, argv[argc - 1],
                                  &next_page))
             {/*669*/SensorCall();exit (1);/*670*/}
          SensorCall();if (writeCroppedImage(in, out, &image, &dump,crop.combined_width, 
                                crop.combined_length, crop_buff, next_page, total_pages))
            {
             SensorCall();TIFFError("main", "Unable to write new image");
             exit (-1);
	    }
          }
	}
      else
        {
	/* If we used a crop buffer, our data is there, otherwise it is
         * in the read_buffer
         */
	SensorCall();if (crop_buff != NULL)  
	  {/*671*/SensorCall();sect_src = crop_buff;/*672*/}
        else
          {/*673*/SensorCall();sect_src = read_buff;/*674*/}
        /* Break input image into pages or rows and columns */
        SensorCall();if (computeOutputPixelOffsets(&crop, &image, &page, sections, &dump))
          {
          SensorCall();TIFFError("main", "Unable to compute output section data");
          exit (-1);
	  }
        /* If there are multiple files on the command line, the final one is assumed 
         * to be the output filename into which the images are written.
         */
	SensorCall();if (update_output_file (&out, mp, crop.exp_mode, argv[argc - 1], &next_page))
          {/*675*/SensorCall();exit (1);/*676*/}

	SensorCall();if (writeImageSections(in, out, &image, &page, sections, &dump, sect_src, &sect_buff))
          {
          SensorCall();TIFFError("main", "Unable to write image sections");
          exit (-1);
	  }
        }

      /* No image list specified, just read the next image */
      SensorCall();if (image_count == 0)
        {/*677*/SensorCall();dirnum++;/*678*/}
      else
        {
	SensorCall();dirnum = (tdir_t)(imagelist[next_image] - 1);
        next_image++;
        }

      SensorCall();if (dirnum == MAX_IMAGES - 1)
        {/*679*/SensorCall();dirnum = TIFFNumberOfDirectories(in) - 1;/*680*/}

      if (!TIFFSetDirectory(in, (tdir_t)dirnum))
        end_of_input = TRUE;
      }
    SensorCall();TIFFClose(in);
    optind++;
    }

  /* If we did not use the read buffer as the crop buffer */
  SensorCall();if (read_buff)
    {/*683*/SensorCall();_TIFFfree(read_buff);/*684*/}

  SensorCall();if (crop_buff)
    {/*685*/SensorCall();_TIFFfree(crop_buff);/*686*/}

  SensorCall();if (sect_buff)
    {/*687*/SensorCall();_TIFFfree(sect_buff);/*688*/}

   /* Clean up any segment buffers used for zones or regions */
  SensorCall();for (seg = 0; seg < crop.selections; seg++)
    {/*689*/SensorCall();_TIFFfree (seg_buffs[seg].buffer);/*690*/}

  SensorCall();if (dump.format != DUMP_NONE)
    {
    SensorCall();if (dump.infile != NULL)
     {/*691*/SensorCall();fclose (dump.infile);/*692*/}

    SensorCall();if (dump.outfile != NULL)
      {
      SensorCall();dump_info (dump.outfile, dump.format, "", "Completed run for %s", TIFFFileName(out));
      fclose (dump.outfile);
      }
    }

  SensorCall();TIFFClose(out);

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end main */


/* Debugging functions */
static int dump_data (FILE *dumpfile, int format, char *dump_tag, unsigned char *data, uint32 count)
  {
  SensorCall();int j, k;
  uint32 i;
  char  dump_array[10];
  unsigned char bitset;

  SensorCall();if (dumpfile == NULL)
    {
    SensorCall();TIFFError ("", "Invalid FILE pointer for dump file");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();if (format == DUMP_TEXT)
    {
    SensorCall();fprintf (dumpfile," %s  ", dump_tag);
    SensorCall();for (i = 0; i < count; i++)
      {
      SensorCall();for (j = 0, k = 7; j < 8; j++, k--)
        {
	SensorCall();bitset = (*(data + i)) & (((unsigned char)1 << k)) ? 1 : 0;
        sprintf(&dump_array[j], (bitset) ? "1" : "0");
        }
      SensorCall();dump_array[8] = '\0';
      fprintf (dumpfile," %s", dump_array);
      }
    SensorCall();fprintf (dumpfile,"\n");
    }
  else
    {
    SensorCall();if ((fwrite (data, 1, count, dumpfile)) != count)
      {
      SensorCall();TIFFError ("", "Unable to write binary data to dump file");
      {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
      }
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }

static int dump_byte (FILE *dumpfile, int format, char *dump_tag, unsigned char data)
  {
  SensorCall();int j, k;
  char  dump_array[10];
  unsigned char bitset;

  SensorCall();if (dumpfile == NULL)
    {
    SensorCall();TIFFError ("", "Invalid FILE pointer for dump file");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();if (format == DUMP_TEXT)
    {
    SensorCall();fprintf (dumpfile," %s  ", dump_tag);
    SensorCall();for (j = 0, k = 7; j < 8; j++, k--)
      {
      SensorCall();bitset = data & (((unsigned char)1 << k)) ? 1 : 0;
      sprintf(&dump_array[j], (bitset) ? "1" : "0");
      }
    SensorCall();dump_array[8] = '\0';
    fprintf (dumpfile," %s\n", dump_array);
    }
  else
    {
    SensorCall();if ((fwrite (&data, 1, 1, dumpfile)) != 1)
      {
      SensorCall();TIFFError ("", "Unable to write binary data to dump file");
      {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
      }
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }

static int dump_short (FILE *dumpfile, int format, char *dump_tag, uint16 data)
  {
  SensorCall();int j, k;
  char  dump_array[20];
  unsigned char bitset;

  SensorCall();if (dumpfile == NULL)
    {
    SensorCall();TIFFError ("", "Invalid FILE pointer for dump file");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();if (format == DUMP_TEXT)
    {
    SensorCall();fprintf (dumpfile," %s  ", dump_tag);
    SensorCall();for (j = 0, k = 15; k >= 0; j++, k--)
      {
      SensorCall();bitset = data & (((unsigned char)1 << k)) ? 1 : 0;
      sprintf(&dump_array[j], (bitset) ? "1" : "0");
      SensorCall();if ((k % 8) == 0)
          {/*615*/SensorCall();sprintf(&dump_array[++j], " ");/*616*/}
      }
    SensorCall();dump_array[17] = '\0';
    fprintf (dumpfile," %s\n", dump_array);
    }
  else
    {
    SensorCall();if ((fwrite (&data, 2, 1, dumpfile)) != 2)
      {
      SensorCall();TIFFError ("", "Unable to write binary data to dump file");
      {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
      }
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }

static int dump_long (FILE *dumpfile, int format, char *dump_tag, uint32 data)
  {
  SensorCall();int j, k;
  char  dump_array[40];
  unsigned char bitset;

  SensorCall();if (dumpfile == NULL)
    {
    SensorCall();TIFFError ("", "Invalid FILE pointer for dump file");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();if (format == DUMP_TEXT)
    {
    SensorCall();fprintf (dumpfile," %s  ", dump_tag);
    SensorCall();for (j = 0, k = 31; k >= 0; j++, k--)
      {
      SensorCall();bitset = data & (((uint32)1 << k)) ? 1 : 0;
      sprintf(&dump_array[j], (bitset) ? "1" : "0");
      SensorCall();if ((k % 8) == 0)
          {/*617*/SensorCall();sprintf(&dump_array[++j], " ");/*618*/}
      }
    SensorCall();dump_array[35] = '\0';
    fprintf (dumpfile," %s\n", dump_array);
    }
  else
    {
    SensorCall();if ((fwrite (&data, 4, 1, dumpfile)) != 4)
      {
      SensorCall();TIFFError ("", "Unable to write binary data to dump file");
      {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
      }
    }
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }

static int dump_wide (FILE *dumpfile, int format, char *dump_tag, uint64 data)
  {
  SensorCall();int j, k;
  char  dump_array[80];
  unsigned char bitset;

  SensorCall();if (dumpfile == NULL)
    {
    SensorCall();TIFFError ("", "Invalid FILE pointer for dump file");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();if (format == DUMP_TEXT)
    {
    SensorCall();fprintf (dumpfile," %s  ", dump_tag);
    SensorCall();for (j = 0, k = 63; k >= 0; j++, k--)
      {
      SensorCall();bitset = data & (((uint64)1 << k)) ? 1 : 0;
      sprintf(&dump_array[j], (bitset) ? "1" : "0");
      SensorCall();if ((k % 8) == 0)
          {/*619*/SensorCall();sprintf(&dump_array[++j], " ");/*620*/}
      }
    SensorCall();dump_array[71] = '\0';
    fprintf (dumpfile," %s\n", dump_array);
    }
  else
    {
    SensorCall();if ((fwrite (&data, 8, 1, dumpfile)) != 8)
      {
      SensorCall();TIFFError ("", "Unable to write binary data to dump file");
      {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
      }
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }

static void dump_info(FILE *dumpfile, int format, char *prefix, char *msg, ...)
  {
  SensorCall();if (format == DUMP_TEXT)
    {
    SensorCall();va_list ap;
    va_start(ap, msg);
    fprintf(dumpfile, "%s ", prefix);
    vfprintf(dumpfile, msg, ap);
    fprintf(dumpfile, "\n");
    }
  SensorCall();}

static int dump_buffer (FILE* dumpfile, int format, uint32 rows, uint32 width, 
                 uint32 row, unsigned char *buff)
  {
  SensorCall();int j, k;
  uint32 i;
  unsigned char * dump_ptr;

  SensorCall();if (dumpfile == NULL)
    {
    SensorCall();TIFFError ("", "Invalid FILE pointer for dump file");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();for (i = 0; i < rows; i++)
    {
    SensorCall();dump_ptr = buff + (i * width);
    SensorCall();if (format == DUMP_TEXT)
      {/*621*/SensorCall();dump_info (dumpfile, format, "", 
                 "Row %4d, %d bytes at offset %d",
	         row + i + 1, width, row * width);/*622*/}
     
    SensorCall();for (j = 0, k = width; k >= 10; j += 10, k -= 10, dump_ptr += 10)
      {/*623*/SensorCall();dump_data (dumpfile, format, "", dump_ptr, 10);/*624*/}
    SensorCall();if (k > 0)
      {/*625*/SensorCall();dump_data (dumpfile, format, "", dump_ptr, k);/*626*/}
    }
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }

/* Extract one or more samples from an interleaved buffer. If count == 1,
 * only the sample plane indicated by sample will be extracted.  If count > 1, 
 * count samples beginning at sample will be extracted. Portions of a 
 * scanline can be extracted by specifying a start and end value.
 */

static int 
extractContigSamplesBytes (uint8 *in, uint8 *out, uint32 cols, 
                           tsample_t sample, uint16 spp, uint16 bps, 
                           tsample_t count, uint32 start, uint32 end)
  {
  SensorCall();int i, bytes_per_sample, sindex;
  uint32 col, dst_rowsize, bit_offset;
  uint32 src_byte, src_bit;
  uint8 *src = in;
  uint8 *dst = out;

  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("extractContigSamplesBytes","Invalid input or output buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();if ((start > end) || (start > cols))
    {
    SensorCall();TIFFError ("extractContigSamplesBytes", 
               "Invalid start column value %d ignored", start);
    start = 0;
    }
  SensorCall();if ((end == 0) || (end > cols))
    {
    SensorCall();TIFFError ("extractContigSamplesBytes", 
               "Invalid end column value %d ignored", end);
    end = cols;
    }

  SensorCall();dst_rowsize = (bps * (end - start) * count) / 8;

  bytes_per_sample = (bps + 7) / 8; 
  /* Optimize case for copying all samples */
  SensorCall();if (count == spp)
    {
    SensorCall();src = in + (start * spp * bytes_per_sample);
    _TIFFmemcpy (dst, src, dst_rowsize);
    }
  else
    {
    SensorCall();for (col = start; col < end; col++)
      {
      SensorCall();for (sindex = sample; (sindex < spp) && (sindex < (sample + count)); sindex++)
        {
        SensorCall();bit_offset = col * bps * spp;
        SensorCall();if (sindex == 0)
          {
          SensorCall();src_byte = bit_offset / 8;
          src_bit  = bit_offset % 8;
          }
        else
          {
          SensorCall();src_byte = (bit_offset + (sindex * bps)) / 8;
          src_bit  = (bit_offset + (sindex * bps)) % 8;
          }
        SensorCall();src = in + src_byte;
        SensorCall();for (i = 0; i < bytes_per_sample; i++)
            {/*553*/SensorCall();*dst++ = *src++;/*554*/}
        }
      }
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end extractContigSamplesBytes */

static int
extractContigSamples8bits (uint8 *in, uint8 *out, uint32 cols,
                           tsample_t sample, uint16 spp, uint16 bps, 
                           tsample_t count, uint32 start, uint32 end)
  {
  SensorCall();int    ready_bits = 0, sindex = 0;
  uint32 col, src_byte, src_bit, bit_offset;
  uint8  maskbits = 0, matchbits = 0;
  uint8  buff1 = 0, buff2 = 0;
  uint8 *src = in;
  uint8 *dst = out;

  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("extractContigSamples8bits","Invalid input or output buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();if ((start > end) || (start > cols))
    {
    SensorCall();TIFFError ("extractContigSamples8bits", 
               "Invalid start column value %d ignored", start);
    start = 0;
    }
  SensorCall();if ((end == 0) || (end > cols))
    {
    SensorCall();TIFFError ("extractContigSamples8bits", 
               "Invalid end column value %d ignored", end);
    end = cols;
    }
  
  SensorCall();ready_bits = 0;
  maskbits =  (uint8)-1 >> ( 8 - bps);
  buff1 = buff2 = 0;
  SensorCall();for (col = start; col < end; col++)
    {    /* Compute src byte(s) and bits within byte(s) */
    SensorCall();bit_offset = col * bps * spp;
    SensorCall();for (sindex = sample; (sindex < spp) && (sindex < (sample + count)); sindex++)
      {
      SensorCall();if (sindex == 0)
        {
        SensorCall();src_byte = bit_offset / 8;
        src_bit  = bit_offset % 8;
        }
      else
        {
        SensorCall();src_byte = (bit_offset + (sindex * bps)) / 8;
        src_bit  = (bit_offset + (sindex * bps)) % 8;
        }

      SensorCall();src = in + src_byte;
      matchbits = maskbits << (8 - src_bit - bps); 
      buff1 = ((*src) & matchbits) << (src_bit);

      /* If we have a full buffer's worth, write it out */
      SensorCall();if (ready_bits >= 8)
        {
        SensorCall();*dst++ = buff2;
        buff2 = buff1;
        ready_bits -= 8;
        }
      else
        {/*543*/SensorCall();buff2 = (buff2 | (buff1 >> ready_bits));/*544*/}
      SensorCall();ready_bits += bps;
      }
    }

  SensorCall();while (ready_bits > 0)
    {
    SensorCall();buff1 = (buff2 & ((unsigned int)255 << (8 - ready_bits)));
    *dst++ = buff1;
    ready_bits -= 8;
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end extractContigSamples8bits */

static int
extractContigSamples16bits (uint8 *in, uint8 *out, uint32 cols, 
                            tsample_t sample, uint16 spp, uint16 bps, 
                            tsample_t count, uint32 start, uint32 end)
  {
  SensorCall();int    ready_bits = 0, sindex = 0;
  uint32 col, src_byte, src_bit, bit_offset;
  uint16 maskbits = 0, matchbits = 0;
  uint16 buff1 = 0, buff2 = 0;
  uint8  bytebuff = 0;
  uint8 *src = in;
  uint8 *dst = out;

  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("extractContigSamples16bits","Invalid input or output buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();if ((start > end) || (start > cols))
    {
    SensorCall();TIFFError ("extractContigSamples16bits", 
               "Invalid start column value %d ignored", start);
    start = 0;
    }
  SensorCall();if ((end == 0) || (end > cols))
    {
    SensorCall();TIFFError ("extractContigSamples16bits", 
               "Invalid end column value %d ignored", end);
    end = cols;
    }

  SensorCall();ready_bits = 0;
  maskbits = (uint16)-1 >> (16 - bps);

  SensorCall();for (col = start; col < end; col++)
    {    /* Compute src byte(s) and bits within byte(s) */
    SensorCall();bit_offset = col * bps * spp;
    SensorCall();for (sindex = sample; (sindex < spp) && (sindex < (sample + count)); sindex++)
      {
      SensorCall();if (sindex == 0)
        {
        SensorCall();src_byte = bit_offset / 8;
        src_bit  = bit_offset % 8;
        }
      else
        {
        SensorCall();src_byte = (bit_offset + (sindex * bps)) / 8;
        src_bit  = (bit_offset + (sindex * bps)) % 8;
        }

      SensorCall();src = in + src_byte;
      matchbits = maskbits << (16 - src_bit - bps); 

      SensorCall();if (little_endian)
        {/*545*/SensorCall();buff1 = (src[0] << 8) | src[1];/*546*/}
      else
        {/*547*/SensorCall();buff1 = (src[1] << 8) | src[0];/*548*/}

      SensorCall();buff1 = (buff1 & matchbits) << (src_bit);
      SensorCall();if (ready_bits < 8) /* add another bps bits to the buffer */
        { 
        SensorCall();bytebuff = 0;
        buff2 = (buff2 | (buff1 >> ready_bits));
        }
      else /* If we have a full buffer's worth, write it out */
        {
        SensorCall();bytebuff = (buff2 >> 8);
        *dst++ = bytebuff;
        ready_bits -= 8;
        /* shift in new bits */
        buff2 = ((buff2 << 8) | (buff1 >> ready_bits));
        }
      SensorCall();ready_bits += bps;
      }
    }

  /* catch any trailing bits at the end of the line */
  SensorCall();while (ready_bits > 0)
    {
    SensorCall();bytebuff = (buff2 >> 8);
    *dst++ = bytebuff;
    ready_bits -= 8;
    }
  
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end extractContigSamples16bits */


static int
extractContigSamples24bits (uint8 *in, uint8 *out, uint32 cols,
 	                    tsample_t sample, uint16 spp, uint16 bps, 
                            tsample_t count, uint32 start, uint32 end)
  {
  SensorCall();int    ready_bits = 0, sindex = 0;
  uint32 col, src_byte, src_bit, bit_offset;
  uint32 maskbits = 0, matchbits = 0;
  uint32 buff1 = 0, buff2 = 0;
  uint8  bytebuff1 = 0, bytebuff2 = 0;
  uint8 *src = in;
  uint8 *dst = out;

  SensorCall();if ((in == NULL) || (out == NULL))
    {
    SensorCall();TIFFError("extractContigSamples24bits","Invalid input or output buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();if ((start > end) || (start > cols))
    {
    SensorCall();TIFFError ("extractContigSamples24bits", 
               "Invalid start column value %d ignored", start);
    start = 0;
    }
  SensorCall();if ((end == 0) || (end > cols))
    {
    SensorCall();TIFFError ("extractContigSamples24bits", 
               "Invalid end column value %d ignored", end);
    end = cols;
    }

  SensorCall();ready_bits = 0;
  maskbits =  (uint32)-1 >> ( 32 - bps);
  SensorCall();for (col = start; col < end; col++)
    {
    /* Compute src byte(s) and bits within byte(s) */
    SensorCall();bit_offset = col * bps * spp;
    SensorCall();for (sindex = sample; (sindex < spp) && (sindex < (sample + count)); sindex++)
      {
      SensorCall();if (sindex == 0)
        {
        SensorCall();src_byte = bit_offset / 8;
        src_bit  = bit_offset % 8;
        }
      else
        {
        SensorCall();src_byte = (bit_offset + (sindex * bps)) / 8;
        src_bit  = (bit_offset + (sindex * bps)) % 8;
        }

      SensorCall();src = in + src_byte;
      matchbits = maskbits << (32 - src_bit - bps); 
      SensorCall();if (little_endian)
	{/*549*/SensorCall();buff1 = (src[0] << 24) | (src[1] << 16) | (src[2] << 8) | src[3];/*550*/}
      else
	{/*551*/SensorCall();buff1 = (src[3] << 24) | (src[2] << 16) | (src[1] << 8) | src[0];/*552*/}
      SensorCall();buff1 = (buff1 & matchbits) << (src_bit);

      SensorCall();if (ready_bits < 16) /* add another bps bits to the buffer */
        {
        SensorCall();bytebuff1 = bytebuff2 = 0;
        buff2 = (buff2 | (buff1 >> ready_bits));
        }
      else /* If we have a full buffer's worth, write it out */
        {
        SensorCall();bytebuff1 = (buff2 >> 24);
        *dst++ = bytebuff1;
        bytebuff2 = (buff2 >> 16);
        *dst++ = bytebuff2;
        ready_bits -= 16;

        /* shift in new bits */
        buff2 = ((buff2 << 16) | (buff1 >> ready_bits));
        }
      SensorCall();ready_bits += bps;
      }
    }

  /* catch any trailing bits at the end of the line */
  SensorCall();while (ready_bits > 0)
    {
    SensorCall();bytebuff1 = (buff2 >> 24);
    *dst++ = bytebuff1;

    buff2 = (buff2 << 8);
    bytebuff2 = bytebuff1;
    ready_bits -= 8;
    } 
  
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end extractContigSamples24bits */

static int
extractContigSamples32bits (uint8 *in, uint8 *out, uint32 cols,
                            tsample_t sample, uint16 spp, uint16 bps, 
 			    tsample_t count, uint32 start, uint32 end)
  {
  SensorCall();int    ready_bits = 0, sindex = 0, shift_width = 0;
  uint32 col, src_byte, src_bit, bit_offset;
  uint32 longbuff1 = 0, longbuff2 = 0;
  uint64 maskbits = 0, matchbits = 0;
  uint64 buff1 = 0, buff2 = 0, buff3 = 0;
  uint8  bytebuff1 = 0, bytebuff2 = 0, bytebuff3 = 0, bytebuff4 = 0;
  uint8 *src = in;
  uint8 *dst = out;

  SensorCall();if ((in == NULL) || (out == NULL))
    {
    SensorCall();TIFFError("extractContigSamples32bits","Invalid input or output buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }


  SensorCall();if ((start > end) || (start > cols))
    {
    SensorCall();TIFFError ("extractContigSamples32bits", 
               "Invalid start column value %d ignored", start);
    start = 0;
    }
  SensorCall();if ((end == 0) || (end > cols))
    {
    SensorCall();TIFFError ("extractContigSamples32bits", 
               "Invalid end column value %d ignored", end);
    end = cols;
    }

  SensorCall();shift_width = ((bps + 7) / 8) + 1; 
  ready_bits = 0;
  maskbits =  (uint64)-1 >> ( 64 - bps);
  SensorCall();for (col = start; col < end; col++)
    {
    /* Compute src byte(s) and bits within byte(s) */
    SensorCall();bit_offset = col * bps * spp;
    SensorCall();for (sindex = sample; (sindex < spp) && (sindex < (sample + count)); sindex++)
      {
      SensorCall();if (sindex == 0)
        {
        SensorCall();src_byte = bit_offset / 8;
        src_bit  = bit_offset % 8;
        }
      else
        {
        SensorCall();src_byte = (bit_offset + (sindex * bps)) / 8;
        src_bit  = (bit_offset + (sindex * bps)) % 8;
        }

      SensorCall();src = in + src_byte;
      matchbits = maskbits << (64 - src_bit - bps); 
      SensorCall();if (little_endian)
        {
	SensorCall();longbuff1 = (src[0] << 24) | (src[1] << 16)  | (src[2] << 8) | src[3];
	longbuff2 = longbuff1;
        }
      else
        {
	SensorCall();longbuff1 = (src[3] << 24) | (src[2] << 16) | (src[1] << 8) | src[0];
	longbuff2 = longbuff1;
	}

      SensorCall();buff3 = ((uint64)longbuff1 << 32) | longbuff2;
      buff1 = (buff3 & matchbits) << (src_bit);

      /* If we have a full buffer's worth, write it out */
      SensorCall();if (ready_bits >= 32)
        {
        SensorCall();bytebuff1 = (buff2 >> 56);
        *dst++ = bytebuff1;
        bytebuff2 = (buff2 >> 48);
        *dst++ = bytebuff2;
        bytebuff3 = (buff2 >> 40);
        *dst++ = bytebuff3;
        bytebuff4 = (buff2 >> 32);
        *dst++ = bytebuff4;
        ready_bits -= 32;
                    
        /* shift in new bits */
        buff2 = ((buff2 << 32) | (buff1 >> ready_bits));
        }
      else
        { /* add another bps bits to the buffer */
        SensorCall();bytebuff1 = bytebuff2 = bytebuff3 = bytebuff4 = 0;
        buff2 = (buff2 | (buff1 >> ready_bits));
        }
      SensorCall();ready_bits += bps;
      }
    }
  SensorCall();while (ready_bits > 0)
    {
    SensorCall();bytebuff1 = (buff2 >> 56);
    *dst++ = bytebuff1;
    buff2 = (buff2 << 8);
    ready_bits -= 8;
    }
  
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end extractContigSamples32bits */

static int
extractContigSamplesShifted8bits (uint8 *in, uint8 *out, uint32 cols,
                                  tsample_t sample, uint16 spp, uint16 bps, 
			          tsample_t count, uint32 start, uint32 end,
 	                          int shift)
  {
  SensorCall();int    ready_bits = 0, sindex = 0;
  uint32 col, src_byte, src_bit, bit_offset;
  uint8  maskbits = 0, matchbits = 0;
  uint8  buff1 = 0, buff2 = 0;
  uint8 *src = in;
  uint8 *dst = out;

  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("extractContigSamplesShifted8bits","Invalid input or output buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();if ((start > end) || (start > cols))
    {
    SensorCall();TIFFError ("extractContigSamplesShifted8bits", 
               "Invalid start column value %d ignored", start);
    start = 0;
    }
  SensorCall();if ((end == 0) || (end > cols))
    {
    SensorCall();TIFFError ("extractContigSamplesShifted8bits", 
               "Invalid end column value %d ignored", end);
    end = cols;
    }

  SensorCall();ready_bits = shift;
  maskbits =  (uint8)-1 >> ( 8 - bps);
  buff1 = buff2 = 0;
  SensorCall();for (col = start; col < end; col++)
    {    /* Compute src byte(s) and bits within byte(s) */
    SensorCall();bit_offset = col * bps * spp;
    SensorCall();for (sindex = sample; (sindex < spp) && (sindex < (sample + count)); sindex++)
      {
      SensorCall();if (sindex == 0)
        {
        SensorCall();src_byte = bit_offset / 8;
        src_bit  = bit_offset % 8;
        }
      else
        {
        SensorCall();src_byte = (bit_offset + (sindex * bps)) / 8;
        src_bit  = (bit_offset + (sindex * bps)) % 8;
        }

      SensorCall();src = in + src_byte;
      matchbits = maskbits << (8 - src_bit - bps); 
      buff1 = ((*src) & matchbits) << (src_bit);
      SensorCall();if ((col == start) && (sindex == sample))
        {/*555*/SensorCall();buff2 = *src & ((uint8)-1) << (shift);/*556*/}

      /* If we have a full buffer's worth, write it out */
      SensorCall();if (ready_bits >= 8)
        {
        SensorCall();*dst++ |= buff2;
        buff2 = buff1;
        ready_bits -= 8;
        }
      else
	{/*557*/SensorCall();buff2 = buff2 | (buff1 >> ready_bits);/*558*/}
      SensorCall();ready_bits += bps;
      }
    }

  SensorCall();while (ready_bits > 0)
    {
    SensorCall();buff1 = (buff2 & ((unsigned int)255 << (8 - ready_bits)));
    *dst++ = buff1;
    ready_bits -= 8;
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end extractContigSamplesShifted8bits */

static int
extractContigSamplesShifted16bits (uint8 *in, uint8 *out, uint32 cols, 
                                   tsample_t sample, uint16 spp, uint16 bps, 
  			           tsample_t count, uint32 start, uint32 end,
 	                           int shift)
  {
  SensorCall();int    ready_bits = 0, sindex = 0;
  uint32 col, src_byte, src_bit, bit_offset;
  uint16 maskbits = 0, matchbits = 0;
  uint16 buff1 = 0, buff2 = 0;
  uint8  bytebuff = 0;
  uint8 *src = in;
  uint8 *dst = out;
  
  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("extractContigSamplesShifted16bits","Invalid input or output buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();if ((start > end) || (start > cols))
    {
    SensorCall();TIFFError ("extractContigSamplesShifted16bits", 
               "Invalid start column value %d ignored", start);
    start = 0;
    }
  SensorCall();if ((end == 0) || (end > cols))
    {
    SensorCall();TIFFError ("extractContigSamplesShifted16bits", 
               "Invalid end column value %d ignored", end);
    end = cols;
    }

  SensorCall();ready_bits = shift;
  maskbits = (uint16)-1 >> (16 - bps);
  SensorCall();for (col = start; col < end; col++)
    {    /* Compute src byte(s) and bits within byte(s) */
    SensorCall();bit_offset = col * bps * spp;
    SensorCall();for (sindex = sample; (sindex < spp) && (sindex < (sample + count)); sindex++)
      {
      SensorCall();if (sindex == 0)
        {
        SensorCall();src_byte = bit_offset / 8;
        src_bit  = bit_offset % 8;
        }
      else
        {
        SensorCall();src_byte = (bit_offset + (sindex * bps)) / 8;
        src_bit  = (bit_offset + (sindex * bps)) % 8;
        }

      SensorCall();src = in + src_byte;
      matchbits = maskbits << (16 - src_bit - bps); 
      SensorCall();if (little_endian)
        {/*559*/SensorCall();buff1 = (src[0] << 8) | src[1];/*560*/}
      else
        {/*561*/SensorCall();buff1 = (src[1] << 8) | src[0];/*562*/}

      SensorCall();if ((col == start) && (sindex == sample))
        {/*563*/SensorCall();buff2 = buff1 & ((uint16)-1) << (8 - shift);/*564*/}

      SensorCall();buff1 = (buff1 & matchbits) << (src_bit);

      SensorCall();if (ready_bits < 8) /* add another bps bits to the buffer */
        {/*565*/SensorCall();buff2 = buff2 | (buff1 >> ready_bits);/*566*/}
      else  /* If we have a full buffer's worth, write it out */
        {
        SensorCall();bytebuff = (buff2 >> 8);
        *dst++ = bytebuff;
        ready_bits -= 8;
        /* shift in new bits */
        buff2 = ((buff2 << 8) | (buff1 >> ready_bits));
        }

      SensorCall();ready_bits += bps;
      }
    }

  /* catch any trailing bits at the end of the line */
  SensorCall();while (ready_bits > 0)
    {
    SensorCall();bytebuff = (buff2 >> 8);
    *dst++ = bytebuff;
    ready_bits -= 8;
    }
  
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end extractContigSamplesShifted16bits */


static int
extractContigSamplesShifted24bits (uint8 *in, uint8 *out, uint32 cols,
 	                           tsample_t sample, uint16 spp, uint16 bps, 
                                   tsample_t count, uint32 start, uint32 end,
	                           int shift)
  {
  SensorCall();int    ready_bits = 0, sindex = 0;
  uint32 col, src_byte, src_bit, bit_offset;
  uint32 maskbits = 0, matchbits = 0;
  uint32 buff1 = 0, buff2 = 0;
  uint8  bytebuff1 = 0, bytebuff2 = 0;
  uint8 *src = in;
  uint8 *dst = out;

  SensorCall();if ((in == NULL) || (out == NULL))
    {
    SensorCall();TIFFError("extractContigSamplesShifted24bits","Invalid input or output buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();if ((start > end) || (start > cols))
    {
    SensorCall();TIFFError ("extractContigSamplesShifted24bits", 
               "Invalid start column value %d ignored", start);
    start = 0;
    }
  SensorCall();if ((end == 0) || (end > cols))
    {
    SensorCall();TIFFError ("extractContigSamplesShifted24bits", 
               "Invalid end column value %d ignored", end);
    end = cols;
    }

  SensorCall();ready_bits = shift;
  maskbits =  (uint32)-1 >> ( 32 - bps);
  SensorCall();for (col = start; col < end; col++)
    {
    /* Compute src byte(s) and bits within byte(s) */
    SensorCall();bit_offset = col * bps * spp;
    SensorCall();for (sindex = sample; (sindex < spp) && (sindex < (sample + count)); sindex++)
      {
      SensorCall();if (sindex == 0)
        {
        SensorCall();src_byte = bit_offset / 8;
        src_bit  = bit_offset % 8;
        }
      else
        {
        SensorCall();src_byte = (bit_offset + (sindex * bps)) / 8;
        src_bit  = (bit_offset + (sindex * bps)) % 8;
        }

      SensorCall();src = in + src_byte;
      matchbits = maskbits << (32 - src_bit - bps); 
      SensorCall();if (little_endian)
	{/*567*/SensorCall();buff1 = (src[0] << 24) | (src[1] << 16) | (src[2] << 8) | src[3];/*568*/}
      else
	{/*569*/SensorCall();buff1 = (src[3] << 24) | (src[2] << 16) | (src[1] << 8) | src[0];/*570*/}

      SensorCall();if ((col == start) && (sindex == sample))
        {/*571*/SensorCall();buff2 = buff1 & ((uint32)-1) << (16 - shift);/*572*/}

      SensorCall();buff1 = (buff1 & matchbits) << (src_bit);

      SensorCall();if (ready_bits < 16)  /* add another bps bits to the buffer */
        {
        SensorCall();bytebuff1 = bytebuff2 = 0;
        buff2 = (buff2 | (buff1 >> ready_bits));
        }
      else /* If we have a full buffer's worth, write it out */
        {
        SensorCall();bytebuff1 = (buff2 >> 24);
        *dst++ = bytebuff1;
        bytebuff2 = (buff2 >> 16);
        *dst++ = bytebuff2;
        ready_bits -= 16;

        /* shift in new bits */
        buff2 = ((buff2 << 16) | (buff1 >> ready_bits));
        }
      SensorCall();ready_bits += bps;
      }
    }

  /* catch any trailing bits at the end of the line */
  SensorCall();while (ready_bits > 0)
    {
    SensorCall();bytebuff1 = (buff2 >> 24);
    *dst++ = bytebuff1;

    buff2 = (buff2 << 8);
    bytebuff2 = bytebuff1;
    ready_bits -= 8;
    }
   
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end extractContigSamplesShifted24bits */

static int
extractContigSamplesShifted32bits (uint8 *in, uint8 *out, uint32 cols,
                                   tsample_t sample, uint16 spp, uint16 bps, 
 			           tsample_t count, uint32 start, uint32 end,
	                           int shift)
  {
  SensorCall();int    ready_bits = 0, sindex = 0, shift_width = 0;
  uint32 col, src_byte, src_bit, bit_offset;
  uint32 longbuff1 = 0, longbuff2 = 0;
  uint64 maskbits = 0, matchbits = 0;
  uint64 buff1 = 0, buff2 = 0, buff3 = 0;
  uint8  bytebuff1 = 0, bytebuff2 = 0, bytebuff3 = 0, bytebuff4 = 0;
  uint8 *src = in;
  uint8 *dst = out;

  SensorCall();if ((in == NULL) || (out == NULL))
    {
    SensorCall();TIFFError("extractContigSamplesShifted32bits","Invalid input or output buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }


  SensorCall();if ((start > end) || (start > cols))
    {
    SensorCall();TIFFError ("extractContigSamplesShifted32bits", 
               "Invalid start column value %d ignored", start);
    start = 0;
    }
  SensorCall();if ((end == 0) || (end > cols))
    {
    SensorCall();TIFFError ("extractContigSamplesShifted32bits", 
               "Invalid end column value %d ignored", end);
    end = cols;
    }

  SensorCall();shift_width = ((bps + 7) / 8) + 1; 
  ready_bits = shift;
  maskbits =  (uint64)-1 >> ( 64 - bps);
  SensorCall();for (col = start; col < end; col++)
    {
    /* Compute src byte(s) and bits within byte(s) */
    SensorCall();bit_offset = col * bps * spp;
    SensorCall();for (sindex = sample; (sindex < spp) && (sindex < (sample + count)); sindex++)
      {
      SensorCall();if (sindex == 0)
        {
        SensorCall();src_byte = bit_offset / 8;
        src_bit  = bit_offset % 8;
        }
      else
        {
        SensorCall();src_byte = (bit_offset + (sindex * bps)) / 8;
        src_bit  = (bit_offset + (sindex * bps)) % 8;
        }

      SensorCall();src = in + src_byte;
      matchbits = maskbits << (64 - src_bit - bps); 
      SensorCall();if (little_endian)
        {
	SensorCall();longbuff1 = (src[0] << 24) | (src[1] << 16) | (src[2] << 8) | src[3];
	longbuff2 = longbuff1;
        }
      else
        {
	SensorCall();longbuff1 = (src[3] << 24) | (src[2] << 16) | (src[1] << 8) | src[0];
	longbuff2 = longbuff1;
	}

      SensorCall();buff3 = ((uint64)longbuff1 << 32) | longbuff2;
      SensorCall();if ((col == start) && (sindex == sample))
        {/*573*/SensorCall();buff2 = buff3 & ((uint64)-1) << (32 - shift);/*574*/}

      SensorCall();buff1 = (buff3 & matchbits) << (src_bit);

      SensorCall();if (ready_bits < 32)
        { /* add another bps bits to the buffer */
        SensorCall();bytebuff1 = bytebuff2 = bytebuff3 = bytebuff4 = 0;
        buff2 = (buff2 | (buff1 >> ready_bits));
        }
      else  /* If we have a full buffer's worth, write it out */
        {
        SensorCall();bytebuff1 = (buff2 >> 56);
        *dst++ = bytebuff1;
        bytebuff2 = (buff2 >> 48);
        *dst++ = bytebuff2;
        bytebuff3 = (buff2 >> 40);
        *dst++ = bytebuff3;
        bytebuff4 = (buff2 >> 32);
        *dst++ = bytebuff4;
        ready_bits -= 32;
                    
        /* shift in new bits */
        buff2 = ((buff2 << 32) | (buff1 >> ready_bits));
        }
      SensorCall();ready_bits += bps;
      }
    }
  SensorCall();while (ready_bits > 0)
    {
    SensorCall();bytebuff1 = (buff2 >> 56);
    *dst++ = bytebuff1;
    buff2 = (buff2 << 8);
    ready_bits -= 8;
    }
  
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end extractContigSamplesShifted32bits */

static int
extractContigSamplesToBuffer(uint8 *out, uint8 *in, uint32 rows, uint32 cols,
  	                     tsample_t sample, uint16 spp, uint16 bps, 
                             struct dump_opts *dump)
  {
  SensorCall();int    shift_width, bytes_per_sample, bytes_per_pixel;
  uint32 src_rowsize, src_offset, row, first_col = 0;
  uint32 dst_rowsize, dst_offset;
  tsample_t count = 1;
  uint8 *src, *dst;

  bytes_per_sample = (bps + 7) / 8; 
  bytes_per_pixel  = ((bps * spp) + 7) / 8;
  SensorCall();if ((bps % 8) == 0)
    {/*63*/SensorCall();shift_width = 0;/*64*/}
  else
    {
    SensorCall();if (bytes_per_pixel < (bytes_per_sample + 1))
      {/*65*/SensorCall();shift_width = bytes_per_pixel;/*66*/}
    else
      {/*67*/SensorCall();shift_width = bytes_per_sample + 1;/*68*/}
    }
  SensorCall();src_rowsize = ((bps * spp * cols) + 7) / 8;
  dst_rowsize = ((bps * cols) + 7) / 8;

  SensorCall();if ((dump->outfile != NULL) && (dump->level == 4))
    {
    SensorCall();dump_info  (dump->outfile, dump->format, "extractContigSamplesToBuffer", 
                "Sample %d, %d rows", sample + 1, rows + 1);
    }
  SensorCall();for (row = 0; row < rows; row++)
    {
    SensorCall();src_offset = row * src_rowsize;
    dst_offset = row * dst_rowsize;
    src = in + src_offset;
    dst = out + dst_offset;

    /* pack the data into the scanline */
    SensorCall();switch (shift_width)
      {  
      case 0: SensorCall();if (extractContigSamplesBytes (src, dst, cols, sample,
                                             spp, bps,  count, first_col, cols))  
                {/*69*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*70*/}
 	      SensorCall();break;
      case 1: SensorCall();if (bps == 1)
                {
                SensorCall();if (extractContigSamples8bits (src, dst, cols, sample,
                                               spp, bps, count, first_col, cols))
	          {/*71*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*72*/}
	        SensorCall();break;
		}
	      else
                 {/*73*/SensorCall();if (extractContigSamples16bits (src, dst, cols, sample,
                                                 spp, bps, count, first_col, cols))
	         {/*75*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*76*/}/*74*/}
	      SensorCall();break;
      case 2: SensorCall();if (extractContigSamples24bits (src, dst, cols, sample,
                                              spp, bps,  count, first_col, cols))
	         {/*77*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*78*/}
	      SensorCall();break;
      case 3:
      case 4: 
      case 5: SensorCall();if (extractContigSamples32bits (src, dst, cols, sample,
                                              spp, bps,  count, first_col, cols))
	         {/*79*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*80*/}
	      SensorCall();break;
      default: SensorCall();TIFFError ("extractContigSamplesToBuffer", "Unsupported bit depth: %d", bps);
	       {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
      }
    SensorCall();if ((dump->outfile != NULL) && (dump->level == 4))
      {/*81*/SensorCall();dump_buffer(dump->outfile, dump->format, 1, dst_rowsize, row, dst);/*82*/}
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end extractContigSamplesToBuffer */

static int
extractContigSamplesToTileBuffer(uint8 *out, uint8 *in, uint32 rows, uint32 cols,
  	                         uint32 imagewidth, uint32 tilewidth, tsample_t sample,
				 uint16 count, uint16 spp, uint16 bps, struct dump_opts *dump)
  {
  SensorCall();int    shift_width, bytes_per_sample, bytes_per_pixel;
  uint32 src_rowsize, src_offset, row;
  uint32 dst_rowsize, dst_offset;
  uint8 *src, *dst;

  bytes_per_sample = (bps + 7) / 8; 
  bytes_per_pixel  = ((bps * spp) + 7) / 8;
  SensorCall();if ((bps % 8) == 0)
    {/*575*/SensorCall();shift_width = 0;/*576*/}
  else
    {
    SensorCall();if (bytes_per_pixel < (bytes_per_sample + 1))
      {/*577*/SensorCall();shift_width = bytes_per_pixel;/*578*/}
    else
      {/*579*/SensorCall();shift_width = bytes_per_sample + 1;/*580*/}
    }

  SensorCall();if ((dump->outfile != NULL) && (dump->level == 4))
    {
    SensorCall();dump_info  (dump->outfile, dump->format, "extractContigSamplesToTileBuffer", 
                "Sample %d, %d rows", sample + 1, rows + 1);
    }

  SensorCall();src_rowsize = ((bps * spp * imagewidth) + 7) / 8;
  dst_rowsize = ((bps * tilewidth * count) + 7) / 8;

  SensorCall();for (row = 0; row < rows; row++)
    {
    SensorCall();src_offset = row * src_rowsize;
    dst_offset = row * dst_rowsize;
    src = in + src_offset;
    dst = out + dst_offset;

    /* pack the data into the scanline */
    SensorCall();switch (shift_width)
      {  
      case 0: SensorCall();if (extractContigSamplesBytes (src, dst, cols, sample,
                                             spp, bps,  count, 0, cols))  
                {/*581*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*582*/}
 	      SensorCall();break;
      case 1: SensorCall();if (bps == 1)
                {
                SensorCall();if (extractContigSamples8bits (src, dst, cols, sample,
                                               spp, bps, count, 0, cols))
	          {/*583*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*584*/}
	        SensorCall();break;
		}
	      else
                 {/*585*/SensorCall();if (extractContigSamples16bits (src, dst, cols, sample,
                                                 spp, bps, count, 0, cols))
	         {/*587*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*588*/}/*586*/}
	      SensorCall();break;
      case 2: SensorCall();if (extractContigSamples24bits (src, dst, cols, sample,
                                              spp, bps,  count, 0, cols))
	         {/*589*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*590*/}
	      SensorCall();break;
      case 3:
      case 4: 
      case 5: SensorCall();if (extractContigSamples32bits (src, dst, cols, sample,
                                              spp, bps,  count, 0, cols))
	         {/*591*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*592*/}
	      SensorCall();break;
      default: SensorCall();TIFFError ("extractContigSamplesToTileBuffer", "Unsupported bit depth: %d", bps);
	       {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
      }
    SensorCall();if ((dump->outfile != NULL) && (dump->level == 4))
      {/*593*/SensorCall();dump_buffer(dump->outfile, dump->format, 1, dst_rowsize, row, dst);/*594*/}
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end extractContigSamplesToTileBuffer */

static int readContigStripsIntoBuffer (TIFF* in, uint8* buf)
  {
  SensorCall();uint8* bufp = buf;
  int32  bytes_read = 0;
  uint16 strip, nstrips   = TIFFNumberOfStrips(in);
  uint32 stripsize = TIFFStripSize(in);
  uint32 rows = 0;
  uint32 rps = TIFFGetFieldDefaulted(in, TIFFTAG_ROWSPERSTRIP, &rps);
  tsize_t scanline_size = TIFFScanlineSize(in);

  SensorCall();for (strip = 0; strip < nstrips; strip++)
    {
    SensorCall();bytes_read = TIFFReadEncodedStrip (in, strip, bufp, -1);
    rows = bytes_read / scanline_size;
    SensorCall();if ((strip < (nstrips - 1)) && (bytes_read != (int32)stripsize))
      {/*1*/SensorCall();TIFFError("", "Strip %d: read %lu bytes, strip size %lu",
		(int)strip + 1, (unsigned long) bytes_read, (unsigned long)stripsize);/*2*/}

    SensorCall();if (bytes_read < 0 && !ignore)
      {
      SensorCall();TIFFError("", "Error reading strip %lu after %lu rows",
		(unsigned long) strip, (unsigned long)rows);
      {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
      }
    SensorCall();bufp += bytes_read;
    }

 {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
  } /* end readContigStripsIntoBuffer */

static int 
combineSeparateSamplesBytes (unsigned char *srcbuffs[], unsigned char *out,
                             uint32 cols, uint32 rows, uint16 spp, uint16 bps,
                             FILE *dumpfile, int format, int level)
  {
  SensorCall();int i, bytes_per_sample;
  uint32 row, col, col_offset, src_rowsize, dst_rowsize, row_offset;
  unsigned char *src;
  unsigned char *dst;
  tsample_t s;

  src = srcbuffs[0];
  dst = out;
  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("combineSeparateSamplesBytes","Invalid buffer address");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();bytes_per_sample = (bps + 7) / 8; 

  src_rowsize = ((bps * cols) + 7) / 8;
  dst_rowsize = ((bps * spp * cols) + 7) / 8;
  SensorCall();for (row = 0; row < rows; row++)
    {
    SensorCall();if ((dumpfile != NULL) && (level == 2))
      {
      SensorCall();for (s = 0; s < spp; s++)
        {
        SensorCall();dump_info (dumpfile, format, "combineSeparateSamplesBytes","Input data, Sample %d", s);
        dump_buffer(dumpfile, format, 1, cols, row, srcbuffs[s] + (row * src_rowsize));
        }
      }
    SensorCall();dst = out + (row * dst_rowsize);
    row_offset = row * src_rowsize;
    SensorCall();for (col = 0; col < cols; col++)
      {
      SensorCall();col_offset = row_offset + (col * (bps / 8)); 
      SensorCall();for (s = 0; (s < spp) && (s < MAX_SAMPLES); s++)
        {
        SensorCall();src = srcbuffs[s] + col_offset; 
        SensorCall();for (i = 0; i < bytes_per_sample; i++)
          {/*603*/SensorCall();*(dst + i) = *(src + i);/*604*/}
        SensorCall();src += bytes_per_sample;
        dst += bytes_per_sample;
        }   
      }

    SensorCall();if ((dumpfile != NULL) && (level == 2))
      {
      SensorCall();dump_info (dumpfile, format, "combineSeparateSamplesBytes","Output data, combined samples");
      dump_buffer(dumpfile, format, 1, dst_rowsize, row, out + (row * dst_rowsize));
      }
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end combineSeparateSamplesBytes */

static int
combineSeparateSamples8bits (uint8 *in[], uint8 *out, uint32 cols,
                            uint32 rows, uint16 spp, uint16 bps, 
 	                    FILE *dumpfile, int format, int level)
  {
  SensorCall();int    ready_bits = 0;
  int    bytes_per_sample = 0;
  uint32 src_rowsize, dst_rowsize, src_offset; 
  uint32 bit_offset;
  uint32 row, col, src_byte = 0, src_bit = 0;
  uint8  maskbits = 0, matchbits = 0;
  uint8  buff1 = 0, buff2 = 0;
  tsample_t s;
  unsigned char *src = in[0];
  unsigned char *dst = out;
  char           action[32];

  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("combineSeparateSamples8bits","Invalid input or output buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();bytes_per_sample = (bps + 7) / 8; 
  src_rowsize = ((bps * cols) + 7) / 8;
  dst_rowsize = ((bps * cols * spp) + 7) / 8;
  maskbits =  (uint8)-1 >> ( 8 - bps);

  SensorCall();for (row = 0; row < rows; row++)
    {
    SensorCall();ready_bits = 0;
    buff1 = buff2 = 0;
    dst = out + (row * dst_rowsize);
    src_offset = row * src_rowsize;
    SensorCall();for (col = 0; col < cols; col++)
      {
      /* Compute src byte(s) and bits within byte(s) */
      SensorCall();bit_offset = col * bps;
      src_byte = bit_offset / 8;
      src_bit  = bit_offset % 8;

      matchbits = maskbits << (8 - src_bit - bps); 
      /* load up next sample from each plane */
      SensorCall();for (s = 0; s < spp; s++)
        {
	SensorCall();src = in[s] + src_offset + src_byte;
        buff1 = ((*src) & matchbits) << (src_bit);

        /* If we have a full buffer's worth, write it out */
        SensorCall();if (ready_bits >= 8)
          {
          SensorCall();*dst++ = buff2;
          buff2 = buff1;
          ready_bits -= 8;
          strcpy (action, "Flush");
          }
        else
          {
          SensorCall();buff2 = (buff2 | (buff1 >> ready_bits));
          strcpy (action, "Update");
          }
        SensorCall();ready_bits += bps;
 
        SensorCall();if ((dumpfile != NULL) && (level == 3))
          {
          SensorCall();dump_info (dumpfile, format, "",
                   "Row %3d, Col %3d, Samples %d, Src byte offset %3d  bit offset %2d  Dst offset %3d",
		   row + 1, col + 1, s, src_byte, src_bit, dst - out);
          dump_byte (dumpfile, format, "Match bits", matchbits);
          dump_byte (dumpfile, format, "Src   bits", *src);
          dump_byte (dumpfile, format, "Buff1 bits", buff1);
          dump_byte (dumpfile, format, "Buff2 bits", buff2);
          dump_info (dumpfile, format, "","%s", action); 
	  }
        }
      }

    SensorCall();if (ready_bits > 0)
      {
      SensorCall();buff1 = (buff2 & ((unsigned int)255 << (8 - ready_bits)));
      *dst++ = buff1;
      SensorCall();if ((dumpfile != NULL) && (level == 3))
        {
        SensorCall();dump_info (dumpfile, format, "",
	         "Row %3d, Col %3d, Src byte offset %3d  bit offset %2d  Dst offset %3d",
	         row + 1, col + 1, src_byte, src_bit, dst - out);
                 dump_byte (dumpfile, format, "Final bits", buff1);
        }
      }

    SensorCall();if ((dumpfile != NULL) && (level >= 2))
      {
      SensorCall();dump_info (dumpfile, format, "combineSeparateSamples8bits","Output data");
      dump_buffer(dumpfile, format, 1, dst_rowsize, row, out + (row * dst_rowsize));
      }
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end combineSeparateSamples8bits */

static int
combineSeparateSamples16bits (uint8 *in[], uint8 *out, uint32 cols,
                              uint32 rows, uint16 spp, uint16 bps, 
 	                      FILE *dumpfile, int format, int level)
  {
  SensorCall();int    ready_bits = 0, bytes_per_sample = 0;
  uint32 src_rowsize, dst_rowsize; 
  uint32 bit_offset, src_offset;
  uint32 row, col, src_byte = 0, src_bit = 0;
  uint16 maskbits = 0, matchbits = 0;
  uint16 buff1 = 0, buff2 = 0;
  uint8  bytebuff = 0;
  tsample_t s;
  unsigned char *src = in[0];
  unsigned char *dst = out;
  char           action[8];

  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("combineSeparateSamples16bits","Invalid input or output buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();bytes_per_sample = (bps + 7) / 8; 
  src_rowsize = ((bps * cols) + 7) / 8;
  dst_rowsize = ((bps * cols * spp) + 7) / 8;
  maskbits = (uint16)-1 >> (16 - bps);

  SensorCall();for (row = 0; row < rows; row++)
    {
    SensorCall();ready_bits = 0;
    buff1 = buff2 = 0;
    dst = out + (row * dst_rowsize);
    src_offset = row * src_rowsize;
    SensorCall();for (col = 0; col < cols; col++)
      {
      /* Compute src byte(s) and bits within byte(s) */
      SensorCall();bit_offset = col * bps;
      src_byte = bit_offset / 8;
      src_bit  = bit_offset % 8;

      matchbits = maskbits << (16 - src_bit - bps); 
      SensorCall();for (s = 0; s < spp; s++)
        {
	SensorCall();src = in[s] + src_offset + src_byte;
        SensorCall();if (little_endian)
          {/*595*/SensorCall();buff1 = (src[0] << 8) | src[1];/*596*/}
        else
          {/*597*/SensorCall();buff1 = (src[1] << 8) | src[0];/*598*/}

	SensorCall();buff1 = (buff1 & matchbits) << (src_bit);

	/* If we have a full buffer's worth, write it out */
	SensorCall();if (ready_bits >= 8)
	  {
	    SensorCall();bytebuff = (buff2 >> 8);
	    *dst++ = bytebuff;
	    ready_bits -= 8;
	    /* shift in new bits */
	    buff2 = ((buff2 << 8) | (buff1 >> ready_bits));
	    strcpy (action, "Flush");
	  }
	else
	  { /* add another bps bits to the buffer */
	    SensorCall();bytebuff = 0;
	    buff2 = (buff2 | (buff1 >> ready_bits));
	    strcpy (action, "Update");
	  }
	SensorCall();ready_bits += bps;

	SensorCall();if ((dumpfile != NULL) && (level == 3))
	  {
	  SensorCall();dump_info (dumpfile, format, "",
		       "Row %3d, Col %3d, Samples %d, Src byte offset %3d  bit offset %2d  Dst offset %3d",
		       row + 1, col + 1, s, src_byte, src_bit, dst - out);

	  dump_short (dumpfile, format, "Match bits", matchbits);
	  dump_data  (dumpfile, format, "Src   bits", src, 2);
	  dump_short (dumpfile, format, "Buff1 bits", buff1);
	  dump_short (dumpfile, format, "Buff2 bits", buff2);
	  dump_byte  (dumpfile, format, "Write byte", bytebuff);
	  dump_info  (dumpfile, format, "","Ready bits:  %d, %s", ready_bits, action); 
	  }
	}
      }

    /* catch any trailing bits at the end of the line */
    SensorCall();if (ready_bits > 0)
      {
      SensorCall();bytebuff = (buff2 >> 8);
      *dst++ = bytebuff;
      SensorCall();if ((dumpfile != NULL) && (level == 3))
	{
	SensorCall();dump_info (dumpfile, format, "",
		       "Row %3d, Col %3d, Src byte offset %3d  bit offset %2d  Dst offset %3d",
		       row + 1, col + 1, src_byte, src_bit, dst - out);
	dump_byte (dumpfile, format, "Final bits", bytebuff);
	}
      }

    SensorCall();if ((dumpfile != NULL) && (level == 2))
      {
      SensorCall();dump_info (dumpfile, format, "combineSeparateSamples16bits","Output data");
      dump_buffer(dumpfile, format, 1, dst_rowsize, row, out + (row * dst_rowsize));
      }
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end combineSeparateSamples16bits */

static int
combineSeparateSamples24bits (uint8 *in[], uint8 *out, uint32 cols,
                              uint32 rows, uint16 spp, uint16 bps, 
	                      FILE *dumpfile, int format, int level)
  {
  SensorCall();int    ready_bits = 0, bytes_per_sample = 0;
  uint32 src_rowsize, dst_rowsize; 
  uint32 bit_offset, src_offset;
  uint32 row, col, src_byte = 0, src_bit = 0;
  uint32 maskbits = 0, matchbits = 0;
  uint32 buff1 = 0, buff2 = 0;
  uint8  bytebuff1 = 0, bytebuff2 = 0;
  tsample_t s;
  unsigned char *src = in[0];
  unsigned char *dst = out;
  char           action[8];

  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("combineSeparateSamples24bits","Invalid input or output buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();bytes_per_sample = (bps + 7) / 8; 
  src_rowsize = ((bps * cols) + 7) / 8;
  dst_rowsize = ((bps * cols * spp) + 7) / 8;
  maskbits =  (uint32)-1 >> ( 32 - bps);

  SensorCall();for (row = 0; row < rows; row++)
    {
    SensorCall();ready_bits = 0;
    buff1 = buff2 = 0;
    dst = out + (row * dst_rowsize);
    src_offset = row * src_rowsize;
    SensorCall();for (col = 0; col < cols; col++)
      {
      /* Compute src byte(s) and bits within byte(s) */
      SensorCall();bit_offset = col * bps;
      src_byte = bit_offset / 8;
      src_bit  = bit_offset % 8;

      matchbits = maskbits << (32 - src_bit - bps); 
      SensorCall();for (s = 0; s < spp; s++)
        {
	SensorCall();src = in[s] + src_offset + src_byte;
        SensorCall();if (little_endian)
	  {/*599*/SensorCall();buff1 = (src[0] << 24) | (src[1] << 16) | (src[2] << 8) | src[3];/*600*/}
        else
	  {/*601*/SensorCall();buff1 = (src[3] << 24) | (src[2] << 16) | (src[1] << 8) | src[0];/*602*/}
	SensorCall();buff1 = (buff1 & matchbits) << (src_bit);

	/* If we have a full buffer's worth, write it out */
	SensorCall();if (ready_bits >= 16)
	  {
	    SensorCall();bytebuff1 = (buff2 >> 24);
	    *dst++ = bytebuff1;
	    bytebuff2 = (buff2 >> 16);
	    *dst++ = bytebuff2;
	    ready_bits -= 16;

	    /* shift in new bits */
	    buff2 = ((buff2 << 16) | (buff1 >> ready_bits));
	    strcpy (action, "Flush");
	  }
	else
	  { /* add another bps bits to the buffer */
	    SensorCall();bytebuff1 = bytebuff2 = 0;
	    buff2 = (buff2 | (buff1 >> ready_bits));
	    strcpy (action, "Update");
	  }
	SensorCall();ready_bits += bps;

	SensorCall();if ((dumpfile != NULL) && (level == 3))
	  {
	  SensorCall();dump_info (dumpfile, format, "",
		       "Row %3d, Col %3d, Samples %d, Src byte offset %3d  bit offset %2d  Dst offset %3d",
		       row + 1, col + 1, s, src_byte, src_bit, dst - out);
	  dump_long (dumpfile, format, "Match bits ", matchbits);
	  dump_data (dumpfile, format, "Src   bits ", src, 4);
	  dump_long (dumpfile, format, "Buff1 bits ", buff1);
	  dump_long (dumpfile, format, "Buff2 bits ", buff2);
	  dump_byte (dumpfile, format, "Write bits1", bytebuff1);
	  dump_byte (dumpfile, format, "Write bits2", bytebuff2);
	  dump_info (dumpfile, format, "","Ready bits:   %d, %s", ready_bits, action); 
	  }
	}
      }

    /* catch any trailing bits at the end of the line */
    SensorCall();while (ready_bits > 0)
      {
	SensorCall();bytebuff1 = (buff2 >> 24);
	*dst++ = bytebuff1;

	buff2 = (buff2 << 8);
	bytebuff2 = bytebuff1;
	ready_bits -= 8;
      }
 
    SensorCall();if ((dumpfile != NULL) && (level == 3))
      {
      SensorCall();dump_info (dumpfile, format, "",
		   "Row %3d, Col %3d, Src byte offset %3d  bit offset %2d  Dst offset %3d",
		   row + 1, col + 1, src_byte, src_bit, dst - out);

      dump_long (dumpfile, format, "Match bits ", matchbits);
      dump_data (dumpfile, format, "Src   bits ", src, 4);
      dump_long (dumpfile, format, "Buff1 bits ", buff1);
      dump_long (dumpfile, format, "Buff2 bits ", buff2);
      dump_byte (dumpfile, format, "Write bits1", bytebuff1);
      dump_byte (dumpfile, format, "Write bits2", bytebuff2);
      dump_info (dumpfile, format, "", "Ready bits:  %2d", ready_bits); 
      }

    SensorCall();if ((dumpfile != NULL) && (level == 2))
      {
      SensorCall();dump_info (dumpfile, format, "combineSeparateSamples24bits","Output data");
      dump_buffer(dumpfile, format, 1, dst_rowsize, row, out + (row * dst_rowsize));
      }
    }
  
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end combineSeparateSamples24bits */

static int
combineSeparateSamples32bits (uint8 *in[], uint8 *out, uint32 cols,
                              uint32 rows, uint16 spp, uint16 bps, 
	                      FILE *dumpfile, int format, int level)
  {
  SensorCall();int    ready_bits = 0, bytes_per_sample = 0, shift_width = 0;
  uint32 src_rowsize, dst_rowsize, bit_offset, src_offset;
  uint32 src_byte = 0, src_bit = 0;
  uint32 row, col;
  uint32 longbuff1 = 0, longbuff2 = 0;
  uint64 maskbits = 0, matchbits = 0;
  uint64 buff1 = 0, buff2 = 0, buff3 = 0;
  uint8  bytebuff1 = 0, bytebuff2 = 0, bytebuff3 = 0, bytebuff4 = 0;
  tsample_t s;
  unsigned char *src = in[0];
  unsigned char *dst = out;
  char           action[8];

  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("combineSeparateSamples32bits","Invalid input or output buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();bytes_per_sample = (bps + 7) / 8; 
  src_rowsize = ((bps * cols) + 7) / 8;
  dst_rowsize = ((bps * cols * spp) + 7) / 8;
  maskbits =  (uint64)-1 >> ( 64 - bps);
  shift_width = ((bps + 7) / 8) + 1; 

  SensorCall();for (row = 0; row < rows; row++)
    {
    SensorCall();ready_bits = 0;
    buff1 = buff2 = 0;
    dst = out + (row * dst_rowsize);
    src_offset = row * src_rowsize;
    SensorCall();for (col = 0; col < cols; col++)
      {
      /* Compute src byte(s) and bits within byte(s) */
      SensorCall();bit_offset = col * bps;
      src_byte = bit_offset / 8;
      src_bit  = bit_offset % 8;

      matchbits = maskbits << (64 - src_bit - bps); 
      SensorCall();for (s = 0; s < spp; s++)
	{
	SensorCall();src = in[s] + src_offset + src_byte;
	SensorCall();if (little_endian)
	  {
	  SensorCall();longbuff1 = (src[0] << 24) | (src[1] << 16) | (src[2] << 8) | src[3];
          longbuff2 = longbuff1;
	  }
	else
	  {
	  SensorCall();longbuff1 = (src[3] << 24) | (src[2] << 16) | (src[1] << 8) | src[0];
          longbuff2 = longbuff1;
	  }
	SensorCall();buff3 = ((uint64)longbuff1 << 32) | longbuff2;
	buff1 = (buff3 & matchbits) << (src_bit);

	/* If we have a full buffer's worth, write it out */
	SensorCall();if (ready_bits >= 32)
	  {
	  SensorCall();bytebuff1 = (buff2 >> 56);
	  *dst++ = bytebuff1;
	  bytebuff2 = (buff2 >> 48);
	  *dst++ = bytebuff2;
	  bytebuff3 = (buff2 >> 40);
	  *dst++ = bytebuff3;
	  bytebuff4 = (buff2 >> 32);
	  *dst++ = bytebuff4;
	  ready_bits -= 32;
                    
	  /* shift in new bits */
	  buff2 = ((buff2 << 32) | (buff1 >> ready_bits));
	  strcpy (action, "Flush");
	  }
	else
	  { /* add another bps bits to the buffer */
	  SensorCall();bytebuff1 = bytebuff2 = bytebuff3 = bytebuff4 = 0;
	  buff2 = (buff2 | (buff1 >> ready_bits));
	  strcpy (action, "Update");
	  }
	SensorCall();ready_bits += bps;

	SensorCall();if ((dumpfile != NULL) && (level == 3))
	  { 
	  SensorCall();dump_info (dumpfile, format, "",
		     "Row %3d, Col %3d, Sample %d, Src byte offset %3d  bit offset %2d  Dst offset %3d",
		     row + 1, col + 1, s, src_byte, src_bit, dst - out);
	  dump_wide (dumpfile, format, "Match bits ", matchbits);
	  dump_data (dumpfile, format, "Src   bits ", src, 8);
	  dump_wide (dumpfile, format, "Buff1 bits ", buff1);
	  dump_wide (dumpfile, format, "Buff2 bits ", buff2);
	  dump_info (dumpfile, format, "", "Ready bits:   %d, %s", ready_bits, action); 
	  }
	}
      }
    SensorCall();while (ready_bits > 0)
      {
      SensorCall();bytebuff1 = (buff2 >> 56);
      *dst++ = bytebuff1;
      buff2 = (buff2 << 8);
      ready_bits -= 8;
      }

    SensorCall();if ((dumpfile != NULL) && (level == 3))
      {
      SensorCall();dump_info (dumpfile, format, "",
	         "Row %3d, Col %3d, Src byte offset %3d  bit offset %2d  Dst offset %3d",
		 row + 1, col + 1, src_byte, src_bit, dst - out);

      dump_long (dumpfile, format, "Match bits ", matchbits);
      dump_data (dumpfile, format, "Src   bits ", src, 4);
      dump_long (dumpfile, format, "Buff1 bits ", buff1);
      dump_long (dumpfile, format, "Buff2 bits ", buff2);
      dump_byte (dumpfile, format, "Write bits1", bytebuff1);
      dump_byte (dumpfile, format, "Write bits2", bytebuff2);
      dump_info (dumpfile, format, "", "Ready bits:  %2d", ready_bits); 
      }

    SensorCall();if ((dumpfile != NULL) && (level == 2))
      {
      SensorCall();dump_info (dumpfile, format, "combineSeparateSamples32bits","Output data");
      dump_buffer(dumpfile, format, 1, dst_rowsize, row, out);
      }
    }
  
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end combineSeparateSamples32bits */

static int 
combineSeparateTileSamplesBytes (unsigned char *srcbuffs[], unsigned char *out,
                                 uint32 cols, uint32 rows, uint32 imagewidth,
                                 uint32 tw, uint16 spp, uint16 bps,
                                 FILE *dumpfile, int format, int level)
  {
  SensorCall();int i, bytes_per_sample;
  uint32 row, col, col_offset, src_rowsize, dst_rowsize, src_offset;
  unsigned char *src;
  unsigned char *dst;
  tsample_t s;

  src = srcbuffs[0];
  dst = out;
  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("combineSeparateTileSamplesBytes","Invalid buffer address");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();bytes_per_sample = (bps + 7) / 8; 
  src_rowsize = ((bps * tw) + 7) / 8;
  dst_rowsize = imagewidth * bytes_per_sample * spp;
  SensorCall();for (row = 0; row < rows; row++)
    {
    SensorCall();if ((dumpfile != NULL) && (level == 2))
      {
      SensorCall();for (s = 0; s < spp; s++)
        {
        SensorCall();dump_info (dumpfile, format, "combineSeparateTileSamplesBytes","Input data, Sample %d", s);
        dump_buffer(dumpfile, format, 1, cols, row, srcbuffs[s] + (row * src_rowsize));
        }
      }
    SensorCall();dst = out + (row * dst_rowsize);
    src_offset = row * src_rowsize;
#ifdef DEVELMODE
    TIFFError("","Tile row %4d, Src offset %6d   Dst offset %6d", 
              row, src_offset, dst - out);
#endif
    SensorCall();for (col = 0; col < cols; col++)
      {
      SensorCall();col_offset = src_offset + (col * (bps / 8)); 
      SensorCall();for (s = 0; (s < spp) && (s < MAX_SAMPLES); s++)
        {
        SensorCall();src = srcbuffs[s] + col_offset; 
        SensorCall();for (i = 0; i < bytes_per_sample; i++)
          {/*613*/SensorCall();*(dst + i) = *(src + i);/*614*/}
        SensorCall();dst += bytes_per_sample;
        }   
      }

    SensorCall();if ((dumpfile != NULL) && (level == 2))
      {
      SensorCall();dump_info (dumpfile, format, "combineSeparateTileSamplesBytes","Output data, combined samples");
      dump_buffer(dumpfile, format, 1, dst_rowsize, row, out + (row * dst_rowsize));
      }
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end combineSeparateTileSamplesBytes */

static int
combineSeparateTileSamples8bits (uint8 *in[], uint8 *out, uint32 cols,
                                 uint32 rows, uint32 imagewidth, 
                                 uint32 tw, uint16 spp, uint16 bps, 
 	                         FILE *dumpfile, int format, int level)
  {
  SensorCall();int    ready_bits = 0;
  uint32 src_rowsize, dst_rowsize, src_offset; 
  uint32 bit_offset;
  uint32 row, col, src_byte = 0, src_bit = 0;
  uint8  maskbits = 0, matchbits = 0;
  uint8  buff1 = 0, buff2 = 0;
  tsample_t s;
  unsigned char *src = in[0];
  unsigned char *dst = out;
  char           action[32];

  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("combineSeparateTileSamples8bits","Invalid input or output buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();src_rowsize = ((bps * tw) + 7) / 8;
  dst_rowsize = ((imagewidth * bps * spp) + 7) / 8;
  maskbits =  (uint8)-1 >> ( 8 - bps);

  SensorCall();for (row = 0; row < rows; row++)
    {
    SensorCall();ready_bits = 0;
    buff1 = buff2 = 0;
    dst = out + (row * dst_rowsize);
    src_offset = row * src_rowsize;
    SensorCall();for (col = 0; col < cols; col++)
      {
      /* Compute src byte(s) and bits within byte(s) */
      SensorCall();bit_offset = col * bps;
      src_byte = bit_offset / 8;
      src_bit  = bit_offset % 8;

      matchbits = maskbits << (8 - src_bit - bps); 
      /* load up next sample from each plane */
      SensorCall();for (s = 0; s < spp; s++)
        {
	SensorCall();src = in[s] + src_offset + src_byte;
        buff1 = ((*src) & matchbits) << (src_bit);

        /* If we have a full buffer's worth, write it out */
        SensorCall();if (ready_bits >= 8)
          {
          SensorCall();*dst++ = buff2;
          buff2 = buff1;
          ready_bits -= 8;
          strcpy (action, "Flush");
          }
        else
          {
          SensorCall();buff2 = (buff2 | (buff1 >> ready_bits));
          strcpy (action, "Update");
          }
        SensorCall();ready_bits += bps;
 
        SensorCall();if ((dumpfile != NULL) && (level == 3))
          {
          SensorCall();dump_info (dumpfile, format, "",
                   "Row %3d, Col %3d, Samples %d, Src byte offset %3d  bit offset %2d  Dst offset %3d",
		   row + 1, col + 1, s, src_byte, src_bit, dst - out);
          dump_byte (dumpfile, format, "Match bits", matchbits);
          dump_byte (dumpfile, format, "Src   bits", *src);
          dump_byte (dumpfile, format, "Buff1 bits", buff1);
          dump_byte (dumpfile, format, "Buff2 bits", buff2);
          dump_info (dumpfile, format, "","%s", action); 
	  }
        }
      }

    SensorCall();if (ready_bits > 0)
      {
      SensorCall();buff1 = (buff2 & ((unsigned int)255 << (8 - ready_bits)));
      *dst++ = buff1;
      SensorCall();if ((dumpfile != NULL) && (level == 3))
        {
        SensorCall();dump_info (dumpfile, format, "",
	         "Row %3d, Col %3d, Src byte offset %3d  bit offset %2d  Dst offset %3d",
	         row + 1, col + 1, src_byte, src_bit, dst - out);
                 dump_byte (dumpfile, format, "Final bits", buff1);
        }
      }

    SensorCall();if ((dumpfile != NULL) && (level >= 2))
      {
      SensorCall();dump_info (dumpfile, format, "combineSeparateTileSamples8bits","Output data");
      dump_buffer(dumpfile, format, 1, dst_rowsize, row, out + (row * dst_rowsize));
      }
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end combineSeparateTileSamples8bits */

static int
combineSeparateTileSamples16bits (uint8 *in[], uint8 *out, uint32 cols,
                                  uint32 rows, uint32 imagewidth, 
                                  uint32 tw, uint16 spp, uint16 bps, 
 	                          FILE *dumpfile, int format, int level)
  {
  SensorCall();int    ready_bits = 0;
  uint32 src_rowsize, dst_rowsize; 
  uint32 bit_offset, src_offset;
  uint32 row, col, src_byte = 0, src_bit = 0;
  uint16 maskbits = 0, matchbits = 0;
  uint16 buff1 = 0, buff2 = 0;
  uint8  bytebuff = 0;
  tsample_t s;
  unsigned char *src = in[0];
  unsigned char *dst = out;
  char           action[8];

  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("combineSeparateTileSamples16bits","Invalid input or output buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();src_rowsize = ((bps * tw) + 7) / 8;
  dst_rowsize = ((imagewidth * bps * spp) + 7) / 8;
  maskbits = (uint16)-1 >> (16 - bps);

  SensorCall();for (row = 0; row < rows; row++)
    {
    SensorCall();ready_bits = 0;
    buff1 = buff2 = 0;
    dst = out + (row * dst_rowsize);
    src_offset = row * src_rowsize;
    SensorCall();for (col = 0; col < cols; col++)
      {
      /* Compute src byte(s) and bits within byte(s) */
      SensorCall();bit_offset = col * bps;
      src_byte = bit_offset / 8;
      src_bit  = bit_offset % 8;

      matchbits = maskbits << (16 - src_bit - bps); 
      SensorCall();for (s = 0; s < spp; s++)
        {
	SensorCall();src = in[s] + src_offset + src_byte;
        SensorCall();if (little_endian)
          {/*605*/SensorCall();buff1 = (src[0] << 8) | src[1];/*606*/}
        else
          {/*607*/SensorCall();buff1 = (src[1] << 8) | src[0];/*608*/}
	SensorCall();buff1 = (buff1 & matchbits) << (src_bit);

	/* If we have a full buffer's worth, write it out */
	SensorCall();if (ready_bits >= 8)
	  {
	    SensorCall();bytebuff = (buff2 >> 8);
	    *dst++ = bytebuff;
	    ready_bits -= 8;
	    /* shift in new bits */
	    buff2 = ((buff2 << 8) | (buff1 >> ready_bits));
	    strcpy (action, "Flush");
	  }
	else
	  { /* add another bps bits to the buffer */
	    SensorCall();bytebuff = 0;
	    buff2 = (buff2 | (buff1 >> ready_bits));
	    strcpy (action, "Update");
	  }
	SensorCall();ready_bits += bps;

	SensorCall();if ((dumpfile != NULL) && (level == 3))
	  {
	  SensorCall();dump_info (dumpfile, format, "",
		       "Row %3d, Col %3d, Samples %d, Src byte offset %3d  bit offset %2d  Dst offset %3d",
		       row + 1, col + 1, s, src_byte, src_bit, dst - out);

	  dump_short (dumpfile, format, "Match bits", matchbits);
	  dump_data  (dumpfile, format, "Src   bits", src, 2);
	  dump_short (dumpfile, format, "Buff1 bits", buff1);
	  dump_short (dumpfile, format, "Buff2 bits", buff2);
	  dump_byte  (dumpfile, format, "Write byte", bytebuff);
	  dump_info  (dumpfile, format, "","Ready bits:  %d, %s", ready_bits, action); 
	  }
	}
      }

    /* catch any trailing bits at the end of the line */
    SensorCall();if (ready_bits > 0)
      {
      SensorCall();bytebuff = (buff2 >> 8);
      *dst++ = bytebuff;
      SensorCall();if ((dumpfile != NULL) && (level == 3))
	{
	SensorCall();dump_info (dumpfile, format, "",
		       "Row %3d, Col %3d, Src byte offset %3d  bit offset %2d  Dst offset %3d",
		       row + 1, col + 1, src_byte, src_bit, dst - out);
	dump_byte (dumpfile, format, "Final bits", bytebuff);
	}
      }

    SensorCall();if ((dumpfile != NULL) && (level == 2))
      {
      SensorCall();dump_info (dumpfile, format, "combineSeparateTileSamples16bits","Output data");
      dump_buffer(dumpfile, format, 1, dst_rowsize, row, out + (row * dst_rowsize));
      }
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end combineSeparateTileSamples16bits */

static int
combineSeparateTileSamples24bits (uint8 *in[], uint8 *out, uint32 cols,
                                  uint32 rows, uint32 imagewidth, 
                                  uint32 tw, uint16 spp, uint16 bps, 
 	                          FILE *dumpfile, int format, int level)
  {
  SensorCall();int    ready_bits = 0;
  uint32 src_rowsize, dst_rowsize; 
  uint32 bit_offset, src_offset;
  uint32 row, col, src_byte = 0, src_bit = 0;
  uint32 maskbits = 0, matchbits = 0;
  uint32 buff1 = 0, buff2 = 0;
  uint8  bytebuff1 = 0, bytebuff2 = 0;
  tsample_t s;
  unsigned char *src = in[0];
  unsigned char *dst = out;
  char           action[8];

  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("combineSeparateTileSamples24bits","Invalid input or output buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();src_rowsize = ((bps * tw) + 7) / 8;
  dst_rowsize = ((imagewidth * bps * spp) + 7) / 8;
  maskbits =  (uint32)-1 >> ( 32 - bps);

  SensorCall();for (row = 0; row < rows; row++)
    {
    SensorCall();ready_bits = 0;
    buff1 = buff2 = 0;
    dst = out + (row * dst_rowsize);
    src_offset = row * src_rowsize;
    SensorCall();for (col = 0; col < cols; col++)
      {
      /* Compute src byte(s) and bits within byte(s) */
      SensorCall();bit_offset = col * bps;
      src_byte = bit_offset / 8;
      src_bit  = bit_offset % 8;

      matchbits = maskbits << (32 - src_bit - bps); 
      SensorCall();for (s = 0; s < spp; s++)
        {
	SensorCall();src = in[s] + src_offset + src_byte;
        SensorCall();if (little_endian)
	  {/*609*/SensorCall();buff1 = (src[0] << 24) | (src[1] << 16) | (src[2] << 8) | src[3];/*610*/}
        else
	  {/*611*/SensorCall();buff1 = (src[3] << 24) | (src[2] << 16) | (src[1] << 8) | src[0];/*612*/}
	SensorCall();buff1 = (buff1 & matchbits) << (src_bit);

	/* If we have a full buffer's worth, write it out */
	SensorCall();if (ready_bits >= 16)
	  {
	    SensorCall();bytebuff1 = (buff2 >> 24);
	    *dst++ = bytebuff1;
	    bytebuff2 = (buff2 >> 16);
	    *dst++ = bytebuff2;
	    ready_bits -= 16;

	    /* shift in new bits */
	    buff2 = ((buff2 << 16) | (buff1 >> ready_bits));
	    strcpy (action, "Flush");
	  }
	else
	  { /* add another bps bits to the buffer */
	    SensorCall();bytebuff1 = bytebuff2 = 0;
	    buff2 = (buff2 | (buff1 >> ready_bits));
	    strcpy (action, "Update");
	  }
	SensorCall();ready_bits += bps;

	SensorCall();if ((dumpfile != NULL) && (level == 3))
	  {
	  SensorCall();dump_info (dumpfile, format, "",
		       "Row %3d, Col %3d, Samples %d, Src byte offset %3d  bit offset %2d  Dst offset %3d",
		       row + 1, col + 1, s, src_byte, src_bit, dst - out);
	  dump_long (dumpfile, format, "Match bits ", matchbits);
	  dump_data (dumpfile, format, "Src   bits ", src, 4);
	  dump_long (dumpfile, format, "Buff1 bits ", buff1);
	  dump_long (dumpfile, format, "Buff2 bits ", buff2);
	  dump_byte (dumpfile, format, "Write bits1", bytebuff1);
	  dump_byte (dumpfile, format, "Write bits2", bytebuff2);
	  dump_info (dumpfile, format, "","Ready bits:   %d, %s", ready_bits, action); 
	  }
	}
      }

    /* catch any trailing bits at the end of the line */
    SensorCall();while (ready_bits > 0)
      {
	SensorCall();bytebuff1 = (buff2 >> 24);
	*dst++ = bytebuff1;

	buff2 = (buff2 << 8);
	bytebuff2 = bytebuff1;
	ready_bits -= 8;
      }
 
    SensorCall();if ((dumpfile != NULL) && (level == 3))
      {
      SensorCall();dump_info (dumpfile, format, "",
		   "Row %3d, Col %3d, Src byte offset %3d  bit offset %2d  Dst offset %3d",
		   row + 1, col + 1, src_byte, src_bit, dst - out);

      dump_long (dumpfile, format, "Match bits ", matchbits);
      dump_data (dumpfile, format, "Src   bits ", src, 4);
      dump_long (dumpfile, format, "Buff1 bits ", buff1);
      dump_long (dumpfile, format, "Buff2 bits ", buff2);
      dump_byte (dumpfile, format, "Write bits1", bytebuff1);
      dump_byte (dumpfile, format, "Write bits2", bytebuff2);
      dump_info (dumpfile, format, "", "Ready bits:  %2d", ready_bits); 
      }

    SensorCall();if ((dumpfile != NULL) && (level == 2))
      {
      SensorCall();dump_info (dumpfile, format, "combineSeparateTileSamples24bits","Output data");
      dump_buffer(dumpfile, format, 1, dst_rowsize, row, out + (row * dst_rowsize));
      }
    }
  
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end combineSeparateTileSamples24bits */

static int
combineSeparateTileSamples32bits (uint8 *in[], uint8 *out, uint32 cols,
                                  uint32 rows, uint32 imagewidth, 
                                  uint32 tw, uint16 spp, uint16 bps, 
 	                          FILE *dumpfile, int format, int level)
  {
  SensorCall();int    ready_bits = 0, shift_width = 0;
  uint32 src_rowsize, dst_rowsize, bit_offset, src_offset;
  uint32 src_byte = 0, src_bit = 0;
  uint32 row, col;
  uint32 longbuff1 = 0, longbuff2 = 0;
  uint64 maskbits = 0, matchbits = 0;
  uint64 buff1 = 0, buff2 = 0, buff3 = 0;
  uint8  bytebuff1 = 0, bytebuff2 = 0, bytebuff3 = 0, bytebuff4 = 0;
  tsample_t s;
  unsigned char *src = in[0];
  unsigned char *dst = out;
  char           action[8];

  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("combineSeparateTileSamples32bits","Invalid input or output buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();src_rowsize = ((bps * tw) + 7) / 8;
  dst_rowsize = ((imagewidth * bps * spp) + 7) / 8;
  maskbits =  (uint64)-1 >> ( 64 - bps);
  shift_width = ((bps + 7) / 8) + 1; 

  SensorCall();for (row = 0; row < rows; row++)
    {
    SensorCall();ready_bits = 0;
    buff1 = buff2 = 0;
    dst = out + (row * dst_rowsize);
    src_offset = row * src_rowsize;
    SensorCall();for (col = 0; col < cols; col++)
      {
      /* Compute src byte(s) and bits within byte(s) */
      SensorCall();bit_offset = col * bps;
      src_byte = bit_offset / 8;
      src_bit  = bit_offset % 8;

      matchbits = maskbits << (64 - src_bit - bps); 
      SensorCall();for (s = 0; s < spp; s++)
	{
	SensorCall();src = in[s] + src_offset + src_byte;
	SensorCall();if (little_endian)
	  {
	  SensorCall();longbuff1 = (src[0] << 24) | (src[1] << 16) | (src[2] << 8) | src[3];
	  longbuff2 = longbuff1;
	  }
	else
	  {
	  SensorCall();longbuff1 = (src[3] << 24) | (src[2] << 16) | (src[1] << 8) | src[0];
          longbuff2 = longbuff1;
	  }

	SensorCall();buff3 = ((uint64)longbuff1 << 32) | longbuff2;
	buff1 = (buff3 & matchbits) << (src_bit);

	/* If we have a full buffer's worth, write it out */
	SensorCall();if (ready_bits >= 32)
	  {
	  SensorCall();bytebuff1 = (buff2 >> 56);
	  *dst++ = bytebuff1;
	  bytebuff2 = (buff2 >> 48);
	  *dst++ = bytebuff2;
	  bytebuff3 = (buff2 >> 40);
	  *dst++ = bytebuff3;
	  bytebuff4 = (buff2 >> 32);
	  *dst++ = bytebuff4;
	  ready_bits -= 32;
                    
	  /* shift in new bits */
	  buff2 = ((buff2 << 32) | (buff1 >> ready_bits));
	  strcpy (action, "Flush");
	  }
	else
	  { /* add another bps bits to the buffer */
	  SensorCall();bytebuff1 = bytebuff2 = bytebuff3 = bytebuff4 = 0;
	  buff2 = (buff2 | (buff1 >> ready_bits));
	  strcpy (action, "Update");
	  }
	SensorCall();ready_bits += bps;

	SensorCall();if ((dumpfile != NULL) && (level == 3))
	  { 
	  SensorCall();dump_info (dumpfile, format, "",
		     "Row %3d, Col %3d, Sample %d, Src byte offset %3d  bit offset %2d  Dst offset %3d",
		     row + 1, col + 1, s, src_byte, src_bit, dst - out);
	  dump_wide (dumpfile, format, "Match bits ", matchbits);
	  dump_data (dumpfile, format, "Src   bits ", src, 8);
	  dump_wide (dumpfile, format, "Buff1 bits ", buff1);
	  dump_wide (dumpfile, format, "Buff2 bits ", buff2);
	  dump_info (dumpfile, format, "", "Ready bits:   %d, %s", ready_bits, action); 
	  }
	}
      }
    SensorCall();while (ready_bits > 0)
      {
      SensorCall();bytebuff1 = (buff2 >> 56);
      *dst++ = bytebuff1;
      buff2 = (buff2 << 8);
      ready_bits -= 8;
      }

    SensorCall();if ((dumpfile != NULL) && (level == 3))
      {
      SensorCall();dump_info (dumpfile, format, "",
	         "Row %3d, Col %3d, Src byte offset %3d  bit offset %2d  Dst offset %3d",
		 row + 1, col + 1, src_byte, src_bit, dst - out);

      dump_long (dumpfile, format, "Match bits ", matchbits);
      dump_data (dumpfile, format, "Src   bits ", src, 4);
      dump_long (dumpfile, format, "Buff1 bits ", buff1);
      dump_long (dumpfile, format, "Buff2 bits ", buff2);
      dump_byte (dumpfile, format, "Write bits1", bytebuff1);
      dump_byte (dumpfile, format, "Write bits2", bytebuff2);
      dump_info (dumpfile, format, "", "Ready bits:  %2d", ready_bits); 
      }

    SensorCall();if ((dumpfile != NULL) && (level == 2))
      {
      SensorCall();dump_info (dumpfile, format, "combineSeparateTileSamples32bits","Output data");
      dump_buffer(dumpfile, format, 1, dst_rowsize, row, out);
      }
    }
  
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end combineSeparateTileSamples32bits */


static int readSeparateStripsIntoBuffer (TIFF *in, uint8 *obuf, uint32 length, 
                                         uint32 width, uint16 spp,
                                         struct dump_opts *dump)
  {
  SensorCall();int i, j, bytes_per_sample, bytes_per_pixel, shift_width, result = 1;
  int32  bytes_read = 0;
  uint16 bps, nstrips, planar, strips_per_sample;
  uint32 src_rowsize, dst_rowsize, rows_processed, rps;
  uint32 rows_this_strip = 0;
  tsample_t s;
  tstrip_t  strip;
  tsize_t scanlinesize = TIFFScanlineSize(in);
  tsize_t stripsize    = TIFFStripSize(in);
  unsigned char *srcbuffs[MAX_SAMPLES];
  unsigned char *buff = NULL;
  unsigned char *dst = NULL;

  SensorCall();if (obuf == NULL)
    {
    SensorCall();TIFFError("readSeparateStripsIntoBuffer","Invalid buffer argument");
    {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();memset (srcbuffs, '\0', sizeof(srcbuffs));
  TIFFGetField(in, TIFFTAG_BITSPERSAMPLE, &bps);
  TIFFGetFieldDefaulted(in, TIFFTAG_PLANARCONFIG, &planar);
  TIFFGetFieldDefaulted(in, TIFFTAG_ROWSPERSTRIP, &rps);
  SensorCall();if (rps > length)
    {/*3*/SensorCall();rps = length;/*4*/}

  SensorCall();bytes_per_sample = (bps + 7) / 8; 
  bytes_per_pixel  = ((bps * spp) + 7) / 8;
  SensorCall();if (bytes_per_pixel < (bytes_per_sample + 1))
    {/*5*/SensorCall();shift_width = bytes_per_pixel;/*6*/}
  else
    {/*7*/SensorCall();shift_width = bytes_per_sample + 1;/*8*/}

  SensorCall();src_rowsize = ((bps * width) + 7) / 8;
  dst_rowsize = ((bps * width * spp) + 7) / 8;
  dst = obuf;

  SensorCall();if ((dump->infile != NULL) && (dump->level == 3))
    {
    SensorCall();dump_info  (dump->infile, dump->format, "", 
                "Image width %d, length %d, Scanline size, %4d bytes",
                width, length,  scanlinesize);
    dump_info  (dump->infile, dump->format, "", 
                "Bits per sample %d, Samples per pixel %d, Shift width %d",
		bps, spp, shift_width);
    }

  /* Libtiff seems to assume/require that data for separate planes are 
   * written one complete plane after another and not interleaved in any way.
   * Multiple scanlines and possibly strips of the same plane must be 
   * written before data for any other plane.
   */
  SensorCall();nstrips = TIFFNumberOfStrips(in);
  strips_per_sample = nstrips /spp;

  SensorCall();for (s = 0; (s < spp) && (s < MAX_SAMPLES); s++)
    {
    SensorCall();srcbuffs[s] = NULL;
    buff = _TIFFmalloc(stripsize);
    SensorCall();if (!buff)
      {
      SensorCall();TIFFError ("readSeparateStripsIntoBuffer", 
                 "Unable to allocate strip read buffer for sample %d", s);
      SensorCall();for (i = 0; i < s; i++)
        {/*9*/SensorCall();_TIFFfree (srcbuffs[i]);/*10*/}
      {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
      }
    SensorCall();srcbuffs[s] = buff;
    }

  SensorCall();rows_processed = 0;
  SensorCall();for (j = 0; (j < strips_per_sample) && (result == 1); j++)
    {
    SensorCall();for (s = 0; (s < spp) && (s < MAX_SAMPLES); s++)
      {
      SensorCall();buff = srcbuffs[s];
      strip = (s * strips_per_sample) + j; 
      bytes_read = TIFFReadEncodedStrip (in, strip, buff, stripsize);
      rows_this_strip = bytes_read / src_rowsize;
      SensorCall();if (bytes_read < 0 && !ignore)
        {
        SensorCall();TIFFError(TIFFFileName(in),
	          "Error, can't read strip %lu for sample %d",
         	   (unsigned long) strip, s + 1);
        result = 0;
        SensorCall();break;
        }
#ifdef DEVELMODE
      TIFFError("", "Strip %2d, read %5d bytes for %4d scanlines, shift width %d", 
		strip, bytes_read, rows_this_strip, shift_width);
#endif
      }

    SensorCall();if (rps > rows_this_strip)
      {/*11*/SensorCall();rps = rows_this_strip;/*12*/}
    SensorCall();dst = obuf + (dst_rowsize * rows_processed);
    SensorCall();if ((bps % 8) == 0)
      {
      SensorCall();if (combineSeparateSamplesBytes (srcbuffs, dst, width, rps,
                                       spp, bps, dump->infile, 
                                       dump->format, dump->level))
        {
        SensorCall();result = 0;
        SensorCall();break;
	}
      }
    else
      {
      SensorCall();switch (shift_width)
        {
        case 1: SensorCall();if (combineSeparateSamples8bits (srcbuffs, dst, width, rps,
                                                 spp, bps, dump->infile,
                                                 dump->format, dump->level))
	          {
                  SensorCall();result = 0;
                  SensorCall();break;
      	          }
	        SensorCall();break;
        case 2: SensorCall();if (combineSeparateSamples16bits (srcbuffs, dst, width, rps,
                                                  spp, bps, dump->infile,
                                                  dump->format, dump->level))
	          {
                  SensorCall();result = 0;
                  SensorCall();break;
		  }
	        SensorCall();break;
        case 3: SensorCall();if (combineSeparateSamples24bits (srcbuffs, dst, width, rps,
                                                  spp, bps, dump->infile,
                                                  dump->format, dump->level))
	          {
                  SensorCall();result = 0;
                  SensorCall();break;
       	          }
                SensorCall();break;
        case 4: 
        case 5:
        case 6:
        case 7:
        case 8: SensorCall();if (combineSeparateSamples32bits (srcbuffs, dst, width, rps,
                                                  spp, bps, dump->infile,
                                                  dump->format, dump->level))
	          {
                  SensorCall();result = 0;
                  SensorCall();break;
		  }
	        SensorCall();break;
        default: SensorCall();TIFFError ("readSeparateStripsIntoBuffer", "Unsupported bit depth: %d", bps);
                  result = 0;
                  SensorCall();break;
        }
      }
 
    SensorCall();if ((rows_processed + rps) > length)
      {
      SensorCall();rows_processed = length;
      rps = length - rows_processed;
      }
    else
      {/*13*/SensorCall();rows_processed += rps;/*14*/}
    }

  /* free any buffers allocated for each plane or scanline and 
   * any temporary buffers 
   */
  SensorCall();for (s = 0; (s < spp) && (s < MAX_SAMPLES); s++)
    {
    SensorCall();buff = srcbuffs[s];
    SensorCall();if (buff != NULL)
      {/*15*/SensorCall();_TIFFfree(buff);/*16*/}
    }

  {int  ReplaceReturn = (result); SensorCall(); return ReplaceReturn;}
  } /* end readSeparateStripsIntoBuffer */

static int
get_page_geometry (char *name, struct pagedef *page)
    {
    SensorCall();char *ptr;
    int n; 

    SensorCall();for (ptr = name; *ptr; ptr++)
      {/*153*/SensorCall();*ptr = (char)tolower((int)*ptr);/*154*/}

    SensorCall();for (n = 0; n < MAX_PAPERNAMES; n++)
      {
      SensorCall();if (strcmp(name, PaperTable[n].name) == 0)
        {
	SensorCall();page->width = PaperTable[n].width;
	page->length = PaperTable[n].length;
        strncpy (page->name, PaperTable[n].name, 15);
        page->name[15] = '\0';
        {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
        }
      }

  {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
  }


static void
initPageSetup (struct pagedef *page, struct pageseg *pagelist, 
               struct buffinfo seg_buffs[])
   {
   SensorCall();int i; 

   strcpy (page->name, "");
   page->mode = PAGE_MODE_NONE;
   page->res_unit = RESUNIT_NONE;
   page->hres = 0.0;
   page->vres = 0.0;
   page->width = 0.0;
   page->length = 0.0;
   page->hmargin = 0.0;
   page->vmargin = 0.0;
   page->rows = 0;
   page->cols = 0;
   page->orient = ORIENTATION_NONE;

   SensorCall();for (i = 0; i < MAX_SECTIONS; i++)
     {
     SensorCall();pagelist[i].x1 = (uint32)0;
     pagelist[i].x2 = (uint32)0;
     pagelist[i].y1 = (uint32)0;
     pagelist[i].y2 = (uint32)0;
     pagelist[i].buffsize = (uint32)0;
     pagelist[i].position = 0;
     pagelist[i].total = 0;
     }

   SensorCall();for (i = 0; i < MAX_OUTBUFFS; i++)
     {
     SensorCall();seg_buffs[i].size = 0;
     seg_buffs[i].buffer = NULL;
     }
   SensorCall();}

static void
initImageData (struct image_data *image)
  {
  SensorCall();image->xres = 0.0;
  image->yres = 0.0;
  image->width = 0;
  image->length = 0;
  image->res_unit = RESUNIT_NONE;
  image->bps = 0;
  image->spp = 0;
  image->planar = 0;
  image->photometric = 0;
  image->orientation = 0;
  image->compression = COMPRESSION_NONE;
  image->adjustments = 0;
  SensorCall();}

static void
initCropMasks (struct crop_mask *cps)
   {
   SensorCall();int i;

   cps->crop_mode = CROP_NONE;
   cps->res_unit  = RESUNIT_NONE;
   cps->edge_ref  = EDGE_TOP;
   cps->width = 0;
   cps->length = 0;
   SensorCall();for (i = 0; i < 4; i++)
     {/*111*/SensorCall();cps->margins[i] = 0.0;/*112*/}
   SensorCall();cps->bufftotal = (uint32)0;
   cps->combined_width = (uint32)0;
   cps->combined_length = (uint32)0;
   cps->rotation = (uint16)0;
   cps->photometric = INVERT_DATA_AND_TAG;
   cps->mirror   = (uint16)0;
   cps->invert   = (uint16)0;
   cps->zones    = (uint32)0;
   cps->regions  = (uint32)0;
   SensorCall();for (i = 0; i < MAX_REGIONS; i++)
     {
     SensorCall();cps->corners[i].X1 = 0.0;
     cps->corners[i].X2 = 0.0;
     cps->corners[i].Y1 = 0.0;
     cps->corners[i].Y2 = 0.0;
     cps->regionlist[i].x1 = 0;
     cps->regionlist[i].x2 = 0;
     cps->regionlist[i].y1 = 0;
     cps->regionlist[i].y2 = 0;
     cps->regionlist[i].width = 0;
     cps->regionlist[i].length = 0;
     cps->regionlist[i].buffsize = 0;
     cps->regionlist[i].buffptr = NULL;
     cps->zonelist[i].position = 0;
     cps->zonelist[i].total = 0;
     }
   SensorCall();cps->exp_mode = ONE_FILE_COMPOSITE;
   cps->img_mode = COMPOSITE_IMAGES;
   SensorCall();}

static void initDumpOptions(struct dump_opts *dump)
  {
  SensorCall();dump->debug  = 0;
  dump->format = DUMP_NONE;
  dump->level  = 1;
  sprintf (dump->mode, "w");
  memset (dump->infilename, '\0', PATH_MAX + 1);
  memset (dump->outfilename, '\0',PATH_MAX + 1);
  dump->infile = NULL;
  dump->outfile = NULL;
  SensorCall();}

/* Compute pixel offsets into the image for margins and fixed regions */
static int
computeInputPixelOffsets(struct crop_mask *crop, struct image_data *image,
                         struct offset *off)
  {
  SensorCall();double scale;
  float xres, yres;
  /* Values for these offsets are in pixels from start of image, not bytes,
   * and are indexed from zero to width - 1 or length - 1 */
  uint32 tmargin, bmargin, lmargin, rmargin;
  uint32 startx, endx;   /* offsets of first and last columns to extract */
  uint32 starty, endy;   /* offsets of first and last row to extract */
  uint32 width, length, crop_width, crop_length; 
  uint32 i, max_width, max_length, zwidth, zlength, buffsize;
  uint32 x1, x2, y1, y2;

  SensorCall();if (image->res_unit != RESUNIT_INCH && image->res_unit != RESUNIT_CENTIMETER)
    {
    SensorCall();xres = 1.0;
    yres = 1.0;
    }
  else
    {
    SensorCall();if (((image->xres == 0) || (image->yres == 0)) && 
         (crop->res_unit != RESUNIT_NONE) &&
	((crop->crop_mode & CROP_REGIONS) || (crop->crop_mode & CROP_MARGINS) ||
 	 (crop->crop_mode & CROP_LENGTH)  || (crop->crop_mode & CROP_WIDTH)))
      {
      SensorCall();TIFFError("computeInputPixelOffsets", "Cannot compute margins or fixed size sections without image resolution");
      TIFFError("computeInputPixelOffsets", "Specify units in pixels and try again");
      {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
      }
    SensorCall();xres = image->xres;
    yres = image->yres;
    }

  /* Translate user units to image units */
  SensorCall();scale = 1.0;
  SensorCall();switch (crop->res_unit) {
    case RESUNIT_CENTIMETER:
         SensorCall();if (image->res_unit == RESUNIT_INCH)
	   {/*155*/SensorCall();scale = 1.0/2.54;/*156*/}
	 SensorCall();break;
    case RESUNIT_INCH:
	 SensorCall();if (image->res_unit == RESUNIT_CENTIMETER)
	     {/*157*/SensorCall();scale = 2.54;/*158*/}
	 SensorCall();break;
    case RESUNIT_NONE: /* Dimensions in pixels */
    default:
    SensorCall();break;
    }

  SensorCall();if (crop->crop_mode & CROP_REGIONS)
    {
    SensorCall();max_width = max_length = 0;
    SensorCall();for (i = 0; i < crop->regions; i++)
      {
      SensorCall();if ((crop->res_unit == RESUNIT_INCH) || (crop->res_unit == RESUNIT_CENTIMETER))
        {
	SensorCall();x1 = (uint32) (crop->corners[i].X1 * scale * xres);
	x2 = (uint32) (crop->corners[i].X2 * scale * xres);
	y1 = (uint32) (crop->corners[i].Y1 * scale * yres);
	y2 = (uint32) (crop->corners[i].Y2 * scale * yres);
        }
      else
        {
	SensorCall();x1 = (uint32) (crop->corners[i].X1);
	x2 = (uint32) (crop->corners[i].X2);
	y1 = (uint32) (crop->corners[i].Y1);
	y2 = (uint32) (crop->corners[i].Y2);       
	}
      SensorCall();if (x1 < 1)
        {/*159*/SensorCall();crop->regionlist[i].x1 = 0;/*160*/}
      else
        {/*161*/SensorCall();crop->regionlist[i].x1 = (uint32) (x1 - 1);/*162*/}

      SensorCall();if (x2 > image->width - 1)
        {/*163*/SensorCall();crop->regionlist[i].x2 = image->width - 1;/*164*/}
      else
        {/*165*/SensorCall();crop->regionlist[i].x2 = (uint32) (x2 - 1);/*166*/}
      SensorCall();zwidth  = crop->regionlist[i].x2 - crop->regionlist[i].x1 + 1; 

      SensorCall();if (y1 < 1)
        {/*167*/SensorCall();crop->regionlist[i].y1 = 0;/*168*/}
      else
        {/*169*/SensorCall();crop->regionlist[i].y1 = (uint32) (y1 - 1);/*170*/}

      SensorCall();if (y2 > image->length - 1)
        {/*171*/SensorCall();crop->regionlist[i].y2 = image->length - 1;/*172*/}
      else
        {/*173*/SensorCall();crop->regionlist[i].y2 = (uint32) (y2 - 1);/*174*/}

      SensorCall();zlength = crop->regionlist[i].y2 - crop->regionlist[i].y1 + 1; 

      SensorCall();if (zwidth > max_width)
        {/*175*/SensorCall();max_width = zwidth;/*176*/}
      SensorCall();if (zlength > max_length)
        {/*177*/SensorCall();max_length = zlength;/*178*/}

      SensorCall();buffsize = (uint32)
          (((zwidth * image->bps * image->spp + 7 ) / 8) * (zlength + 1));

      crop->regionlist[i].buffsize = buffsize;
      crop->bufftotal += buffsize;
      SensorCall();if (crop->img_mode == COMPOSITE_IMAGES)
        {
        SensorCall();switch (crop->edge_ref)
          {
          case EDGE_LEFT:
          case EDGE_RIGHT:
               SensorCall();crop->combined_length = zlength;
               crop->combined_width += zwidth;
               SensorCall();break;
          case EDGE_BOTTOM:
          case EDGE_TOP:  /* width from left, length from top */
          default:
               SensorCall();crop->combined_width = zwidth;
               crop->combined_length += zlength;
	       SensorCall();break;
          }
	}
      }
    {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
    }
  
  /* Convert crop margins into offsets into image
   * Margins are expressed as pixel rows and columns, not bytes
   */
  SensorCall();if (crop->crop_mode & CROP_MARGINS)
    {
    SensorCall();if (crop->res_unit != RESUNIT_INCH && crop->res_unit != RESUNIT_CENTIMETER)
      { /* User has specified pixels as reference unit */
      SensorCall();tmargin = (uint32)(crop->margins[0]);
      lmargin = (uint32)(crop->margins[1]);
      bmargin = (uint32)(crop->margins[2]);
      rmargin = (uint32)(crop->margins[3]);
      }
    else
      { /* inches or centimeters specified */
      SensorCall();tmargin = (uint32)(crop->margins[0] * scale * yres);
      lmargin = (uint32)(crop->margins[1] * scale * xres);
      bmargin = (uint32)(crop->margins[2] * scale * yres);
      rmargin = (uint32)(crop->margins[3] * scale * xres);
      }

    SensorCall();if ((lmargin + rmargin) > image->width)
      {
      SensorCall();TIFFError("computeInputPixelOffsets", "Combined left and right margins exceed image width");
      lmargin = (uint32) 0;
      rmargin = (uint32) 0;
      {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
      }
    SensorCall();if ((tmargin + bmargin) > image->length)
      {
      SensorCall();TIFFError("computeInputPixelOffsets", "Combined top and bottom margins exceed image length"); 
      tmargin = (uint32) 0; 
      bmargin = (uint32) 0;
      {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
      }
    }
  else
    { /* no margins requested */
    SensorCall();tmargin = (uint32) 0;
    lmargin = (uint32) 0;
    bmargin = (uint32) 0;
    rmargin = (uint32) 0;
    }

  /* Width, height, and margins are expressed as pixel offsets into image */
  SensorCall();if (crop->res_unit != RESUNIT_INCH && crop->res_unit != RESUNIT_CENTIMETER)
    {
    SensorCall();if (crop->crop_mode & CROP_WIDTH)
      {/*179*/SensorCall();width = (uint32)crop->width;/*180*/}
    else
      {/*181*/SensorCall();width = image->width - lmargin - rmargin;/*182*/}

    SensorCall();if (crop->crop_mode & CROP_LENGTH)
      {/*183*/SensorCall();length  = (uint32)crop->length;/*184*/}
    else
      {/*185*/SensorCall();length = image->length - tmargin - bmargin;/*186*/}
    }
  else
    {
    SensorCall();if (crop->crop_mode & CROP_WIDTH)
      {/*187*/SensorCall();width = (uint32)(crop->width * scale * image->xres);/*188*/}
    else
      {/*189*/SensorCall();width = image->width - lmargin - rmargin;/*190*/}

    SensorCall();if (crop->crop_mode & CROP_LENGTH)
      {/*191*/SensorCall();length  = (uint32)(crop->length * scale * image->yres);/*192*/}
    else
      {/*193*/SensorCall();length = image->length - tmargin - bmargin;/*194*/}
    }

  SensorCall();off->tmargin = tmargin;
  off->bmargin = bmargin;
  off->lmargin = lmargin;
  off->rmargin = rmargin;

  /* Calculate regions defined by margins, width, and length. 
   * Coordinates expressed as 0 to imagewidth - 1, imagelength - 1,
   * since they are used to compute offsets into buffers */
  SensorCall();switch (crop->edge_ref) {
    case EDGE_BOTTOM:
         SensorCall();startx = lmargin;
         SensorCall();if ((startx + width) >= (image->width - rmargin))
           {/*195*/SensorCall();endx = image->width - rmargin - 1;/*196*/}
         else
           {/*197*/SensorCall();endx = startx + width - 1;/*198*/}

         SensorCall();endy = image->length - bmargin - 1;
         SensorCall();if ((endy - length) <= tmargin)
           {/*199*/SensorCall();starty = tmargin;/*200*/}
         else
           {/*201*/SensorCall();starty = endy - length + 1;/*202*/}
         SensorCall();break;
    case EDGE_RIGHT:
         SensorCall();endx = image->width - rmargin - 1;
         SensorCall();if ((endx - width) <= lmargin)
           {/*203*/SensorCall();startx = lmargin;/*204*/}
         else
           {/*205*/SensorCall();startx = endx - width + 1;/*206*/}

         SensorCall();starty = tmargin;
         SensorCall();if ((starty + length) >= (image->length - bmargin))
           {/*207*/SensorCall();endy = image->length - bmargin - 1;/*208*/}
         else
           {/*209*/SensorCall();endy = starty + length - 1;/*210*/}
         SensorCall();break;
    case EDGE_TOP:  /* width from left, length from top */
    case EDGE_LEFT:
    default:
         SensorCall();startx = lmargin;
         SensorCall();if ((startx + width) >= (image->width - rmargin))
           {/*211*/SensorCall();endx = image->width - rmargin - 1;/*212*/}
         else
           {/*213*/SensorCall();endx = startx + width - 1;/*214*/}

         SensorCall();starty = tmargin;
         SensorCall();if ((starty + length) >= (image->length - bmargin))
           {/*215*/SensorCall();endy = image->length - bmargin - 1;/*216*/}
         else
           {/*217*/SensorCall();endy = starty + length - 1;/*218*/}
         SensorCall();break;
    }
  SensorCall();off->startx = startx;
  off->starty = starty;
  off->endx   = endx;
  off->endy   = endy;

  crop_width  = endx - startx + 1;
  crop_length = endy - starty + 1;

  SensorCall();if (crop_width <= 0)
    {
    SensorCall();TIFFError("computeInputPixelOffsets", 
               "Invalid left/right margins and /or image crop width requested");
    {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }
  SensorCall();if (crop_width > image->width)
    {/*219*/SensorCall();crop_width = image->width;/*220*/}

  SensorCall();if (crop_length <= 0)
    {
    SensorCall();TIFFError("computeInputPixelOffsets", 
              "Invalid top/bottom margins and /or image crop length requested");
    {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }
  SensorCall();if (crop_length > image->length)
    {/*221*/SensorCall();crop_length = image->length;/*222*/}

  SensorCall();off->crop_width = crop_width;
  off->crop_length = crop_length;

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end computeInputPixelOffsets */

/* 
 * Translate crop options into pixel offsets for one or more regions of the image.
 * Options are applied in this order: margins, specific width and length, zones,
 * but all are optional. Margins are relative to each edge. Width, length and
 * zones are relative to the specified reference edge. Zones are expressed as
 * X:Y where X is the ordinal value in a set of Y equal sized portions. eg.
 * 2:3 would indicate the middle third of the region qualified by margins and
 * any explicit width and length specified. Regions are specified by coordinates
 * of the top left and lower right corners with range 1 to width or height.
 */

static int
getCropOffsets(struct image_data *image, struct crop_mask *crop, struct dump_opts *dump)
  {
  SensorCall();struct offset offsets;
  int    i;
  int32  test;
  uint32 seg, total, need_buff = 0;
  uint32 buffsize;
  uint32 zwidth, zlength;

  memset(&offsets, '\0', sizeof(struct offset));
  crop->bufftotal = 0;
  crop->combined_width  = (uint32)0;
  crop->combined_length = (uint32)0;
  crop->selections = 0;

  /* Compute pixel offsets if margins or fixed width or length specified */
  SensorCall();if ((crop->crop_mode & CROP_MARGINS) ||
      (crop->crop_mode & CROP_REGIONS) ||
      (crop->crop_mode & CROP_LENGTH)  || 
      (crop->crop_mode & CROP_WIDTH))
    {
    SensorCall();if (computeInputPixelOffsets(crop, image, &offsets))
      {
      SensorCall();TIFFError ("getCropOffsets", "Unable to compute crop margins");
      {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
      }
    SensorCall();need_buff = TRUE;
    crop->selections = crop->regions;
    /* Regions are only calculated from top and left edges with no margins */
    SensorCall();if (crop->crop_mode & CROP_REGIONS)
      {/*293*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*294*/}
    }
  else
    { /* cropped area is the full image */
    SensorCall();offsets.tmargin = 0;
    offsets.lmargin = 0;
    offsets.bmargin = 0;
    offsets.rmargin = 0;
    offsets.crop_width = image->width;
    offsets.crop_length = image->length;
    offsets.startx = 0;
    offsets.endx = image->width - 1;
    offsets.starty = 0;
    offsets.endy = image->length - 1;
    need_buff = FALSE;
    }

  SensorCall();if (dump->outfile != NULL)
    {
    SensorCall();dump_info (dump->outfile, dump->format, "", "Margins: Top: %d  Left: %d  Bottom: %d  Right: %d", 
           offsets.tmargin, offsets.lmargin, offsets.bmargin, offsets.rmargin); 
    dump_info (dump->outfile, dump->format, "", "Crop region within margins: Adjusted Width:  %6d  Length: %6d", 
           offsets.crop_width, offsets.crop_length);
    }

  SensorCall();if (!(crop->crop_mode & CROP_ZONES)) /* no crop zones requested */
    {
    SensorCall();if (need_buff == FALSE)  /* No margins or fixed width or length areas */
      {
      SensorCall();crop->selections = 0;
      crop->combined_width  = image->width;
      crop->combined_length = image->length;
      {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
      }
    else 
      {
      /* Use one region for margins and fixed width or length areas
       * even though it was not formally declared as a region.
       */
      SensorCall();crop->selections = 1;
      crop->zones = 1;
      crop->zonelist[0].total = 1;
      crop->zonelist[0].position = 1;
      }
    }     
  else
    {/*295*/SensorCall();crop->selections = crop->zones;/*296*/}

  SensorCall();for (i = 0; i < crop->zones; i++)
    {
    SensorCall();seg = crop->zonelist[i].position;
    total = crop->zonelist[i].total;

    SensorCall();switch (crop->edge_ref) 
      {
      case EDGE_LEFT: /* zones from left to right, length from top */
           SensorCall();zlength = offsets.crop_length;
	   crop->regionlist[i].y1 = offsets.starty;
           crop->regionlist[i].y2 = offsets.endy;

           crop->regionlist[i].x1 = offsets.startx + 
                                  (uint32)(offsets.crop_width * 1.0 * (seg - 1) / total);
           test = (int32)offsets.startx + 
                  (int32)(offsets.crop_width * 1.0 * seg / total);
           SensorCall();if (test < 1 )
             {/*297*/SensorCall();crop->regionlist[i].x2 = 0;/*298*/}
           else
	     {
	     SensorCall();if (test > (int32)(image->width - 1))
               {/*299*/SensorCall();crop->regionlist[i].x2 = image->width - 1;/*300*/}
             else
	       {/*301*/SensorCall();crop->regionlist[i].x2 = test - 1;/*302*/}
             }
           SensorCall();zwidth = crop->regionlist[i].x2 - crop->regionlist[i].x1  + 1;

	   /* This is passed to extractCropZone or extractCompositeZones */
           crop->combined_length = (uint32)zlength;
           SensorCall();if (crop->exp_mode == COMPOSITE_IMAGES)
             {/*303*/SensorCall();crop->combined_width += (uint32)zwidth;/*304*/}
           else
             {/*305*/SensorCall();crop->combined_width = (uint32)zwidth;/*306*/}
           SensorCall();break;
      case EDGE_BOTTOM: /* width from left, zones from bottom to top */
           SensorCall();zwidth = offsets.crop_width;
	   crop->regionlist[i].x1 = offsets.startx;
           crop->regionlist[i].x2 = offsets.endx;

           test = offsets.endy - (uint32)(offsets.crop_length * 1.0 * seg / total);
           SensorCall();if (test < 1 )
	     {/*307*/SensorCall();crop->regionlist[i].y1 = 0;/*308*/}
           else
	     {/*309*/SensorCall();crop->regionlist[i].y1 = test + 1;/*310*/}

           SensorCall();test = offsets.endy - (offsets.crop_length * 1.0 * (seg - 1) / total);
           SensorCall();if (test < 1 )
             {/*311*/SensorCall();crop->regionlist[i].y2 = 0;/*312*/}
           else
	     {
             SensorCall();if (test > (int32)(image->length - 1))
               {/*313*/SensorCall();crop->regionlist[i].y2 = image->length - 1;/*314*/}
             else 
               {/*315*/SensorCall();crop->regionlist[i].y2 = test;/*316*/}
	     }
           SensorCall();zlength = crop->regionlist[i].y2 - crop->regionlist[i].y1 + 1;

	   /* This is passed to extractCropZone or extractCompositeZones */
           SensorCall();if (crop->exp_mode == COMPOSITE_IMAGES)
             {/*317*/SensorCall();crop->combined_length += (uint32)zlength;/*318*/}
           else
             {/*319*/SensorCall();crop->combined_length = (uint32)zlength;/*320*/}
           SensorCall();crop->combined_width = (uint32)zwidth;
           SensorCall();break;
      case EDGE_RIGHT: /* zones from right to left, length from top */
           SensorCall();zlength = offsets.crop_length;
	   crop->regionlist[i].y1 = offsets.starty;
           crop->regionlist[i].y2 = offsets.endy;

           crop->regionlist[i].x1 = offsets.startx +
                                  (uint32)(offsets.crop_width  * (total - seg) * 1.0 / total);
           test = offsets.startx + 
	          (offsets.crop_width * (total - seg + 1) * 1.0 / total);
           SensorCall();if (test < 1 )
             {/*321*/SensorCall();crop->regionlist[i].x2 = 0;/*322*/}
           else
	     {
	     SensorCall();if (test > (int32)(image->width - 1))
               {/*323*/SensorCall();crop->regionlist[i].x2 = image->width - 1;/*324*/}
             else
               {/*325*/SensorCall();crop->regionlist[i].x2 = test - 1;/*326*/}
             }
           SensorCall();zwidth = crop->regionlist[i].x2 - crop->regionlist[i].x1  + 1;

	   /* This is passed to extractCropZone or extractCompositeZones */
           crop->combined_length = (uint32)zlength;
           SensorCall();if (crop->exp_mode == COMPOSITE_IMAGES)
             {/*327*/SensorCall();crop->combined_width += (uint32)zwidth;/*328*/}
           else
             {/*329*/SensorCall();crop->combined_width = (uint32)zwidth;/*330*/}
           SensorCall();break;
      case EDGE_TOP: /* width from left, zones from top to bottom */
      default:
           SensorCall();zwidth = offsets.crop_width;
	   crop->regionlist[i].x1 = offsets.startx;
           crop->regionlist[i].x2 = offsets.endx;

           crop->regionlist[i].y1 = offsets.starty + (uint32)(offsets.crop_length * 1.0 * (seg - 1) / total);
           test = offsets.starty + (uint32)(offsets.crop_length * 1.0 * seg / total);
           SensorCall();if (test < 1 )
             {/*331*/SensorCall();crop->regionlist[i].y2 = 0;/*332*/}
           else
	     {
	     SensorCall();if (test > (int32)(image->length - 1))
	       {/*333*/SensorCall();crop->regionlist[i].y2 = image->length - 1;/*334*/}
             else
	       {/*335*/SensorCall();crop->regionlist[i].y2 = test - 1;/*336*/}
	     }
           SensorCall();zlength = crop->regionlist[i].y2 - crop->regionlist[i].y1 + 1;

	   /* This is passed to extractCropZone or extractCompositeZones */
           SensorCall();if (crop->exp_mode == COMPOSITE_IMAGES)
             {/*337*/SensorCall();crop->combined_length += (uint32)zlength;/*338*/}
           else
             {/*339*/SensorCall();crop->combined_length = (uint32)zlength;/*340*/}
           SensorCall();crop->combined_width = (uint32)zwidth;
           SensorCall();break;
      } /* end switch statement */

    SensorCall();buffsize = (uint32)
          ((((zwidth * image->bps * image->spp) + 7 ) / 8) * (zlength + 1));
    crop->regionlist[i].width = (uint32) zwidth;
    crop->regionlist[i].length = (uint32) zlength;
    crop->regionlist[i].buffsize = buffsize;
    crop->bufftotal += buffsize;


  SensorCall();if (dump->outfile != NULL)
    {/*341*/SensorCall();dump_info (dump->outfile, dump->format, "",  "Zone %d, width: %4d, length: %4d, x1: %4d  x2: %4d  y1: %4d  y2: %4d",
                    i + 1, (uint32)zwidth, (uint32)zlength,
		    crop->regionlist[i].x1, crop->regionlist[i].x2, 
                    crop->regionlist[i].y1, crop->regionlist[i].y2);/*342*/}
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end getCropOffsets */


static int
computeOutputPixelOffsets (struct crop_mask *crop, struct image_data *image,
                           struct pagedef *page, struct pageseg *sections,
                           struct dump_opts* dump)
  {
  SensorCall();double scale;
  double pwidth, plength;          /* Output page width and length in user units*/
  uint32 iwidth, ilength;          /* Input image width and length in pixels*/
  uint32 owidth, olength;          /* Output image width and length in pixels*/
  uint32 orows, ocols;             /* rows and cols for output */
  uint32 hmargin, vmargin;         /* Horizontal and vertical margins */
  uint32 x1, x2, y1, y2, line_bytes;
  unsigned int orientation;
  uint32 i, j, k;
 
  scale = 1.0;
  SensorCall();if (page->res_unit == RESUNIT_NONE)
    {/*223*/SensorCall();page->res_unit = image->res_unit;/*224*/}

  SensorCall();switch (image->res_unit) {
    case RESUNIT_CENTIMETER:
         SensorCall();if (page->res_unit == RESUNIT_INCH)
	   {/*225*/SensorCall();scale = 1.0/2.54;/*226*/}
	 SensorCall();break;
    case RESUNIT_INCH:
	 SensorCall();if (page->res_unit == RESUNIT_CENTIMETER)
	     {/*227*/SensorCall();scale = 2.54;/*228*/}
	 SensorCall();break;
    case RESUNIT_NONE: /* Dimensions in pixels */
    default:
    SensorCall();break;
    }

  /* get width, height, resolutions of input image selection */
  SensorCall();if (crop->combined_width > 0)
    {/*229*/SensorCall();iwidth = crop->combined_width;/*230*/}
  else
    {/*231*/SensorCall();iwidth = image->width;/*232*/}
  SensorCall();if (crop->combined_length > 0)
    {/*233*/SensorCall();ilength = crop->combined_length;/*234*/}
  else
    {/*235*/SensorCall();ilength = image->length;/*236*/}

  SensorCall();if (page->hres <= 1.0)
    {/*237*/SensorCall();page->hres = image->xres;/*238*/}
  SensorCall();if (page->vres <= 1.0)
    {/*239*/SensorCall();page->vres = image->yres;/*240*/}

  SensorCall();if ((page->hres < 1.0) || (page->vres < 1.0))
    {
    SensorCall();TIFFError("computeOutputPixelOffsets",
    "Invalid horizontal or vertical resolution specified or read from input image");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  /* If no page sizes are being specified, we just use the input image size to
   * calculate maximum margins that can be taken from image.
   */
  SensorCall();if (page->width <= 0)
    {/*241*/SensorCall();pwidth = iwidth;/*242*/}
  else
    {/*243*/SensorCall();pwidth = page->width;/*244*/}

  SensorCall();if (page->length <= 0)
    {/*245*/SensorCall();plength = ilength;/*246*/}
  else
    {/*247*/SensorCall();plength = page->length;/*248*/}

  SensorCall();if (dump->debug)
    {
    SensorCall();TIFFError("", "Page size: %s, Vres: %3.2f, Hres: %3.2f, "
                   "Hmargin: %3.2f, Vmargin: %3.2f",
	     page->name, page->vres, page->hres,
             page->hmargin, page->vmargin);
    TIFFError("", "Res_unit: %d, Scale: %3.2f, Page width: %3.2f, length: %3.2f", 
           page->res_unit, scale, pwidth, plength);
    }

  /* compute margins at specified unit and resolution */
  SensorCall();if (page->mode & PAGE_MODE_MARGINS)
    {
    SensorCall();if (page->res_unit == RESUNIT_INCH || page->res_unit == RESUNIT_CENTIMETER)
      { /* inches or centimeters specified */
      SensorCall();hmargin = (uint32)(page->hmargin * scale * page->hres * ((image->bps + 7)/ 8));
      vmargin = (uint32)(page->vmargin * scale * page->vres * ((image->bps + 7)/ 8));
      }
    else
      { /* Otherwise user has specified pixels as reference unit */
      SensorCall();hmargin = (uint32)(page->hmargin * scale * ((image->bps + 7)/ 8));
      vmargin = (uint32)(page->vmargin * scale * ((image->bps + 7)/ 8));
      }

    SensorCall();if ((hmargin * 2.0) > (pwidth * page->hres))
      {
      SensorCall();TIFFError("computeOutputPixelOffsets", 
                "Combined left and right margins exceed page width");
      hmargin = (uint32) 0;
      {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
      }
    SensorCall();if ((vmargin * 2.0) > (plength * page->vres))
      {
      SensorCall();TIFFError("computeOutputPixelOffsets", 
                "Combined top and bottom margins exceed page length"); 
      vmargin = (uint32) 0; 
      {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
      }
    }
  else
    {
    SensorCall();hmargin = 0;
    vmargin = 0;
    }

  SensorCall();if (page->mode & PAGE_MODE_ROWSCOLS )
    {
    /* Maybe someday but not for now */
    SensorCall();if (page->mode & PAGE_MODE_MARGINS)
      {/*249*/SensorCall();TIFFError("computeOutputPixelOffsets", 
      "Output margins cannot be specified with rows and columns");/*250*/} 

    SensorCall();owidth  = TIFFhowmany(iwidth, page->cols);
    olength = TIFFhowmany(ilength, page->rows);
    }
  else
    {
    SensorCall();if (page->mode & PAGE_MODE_PAPERSIZE )
      {
      SensorCall();owidth  = (uint32)((pwidth * page->hres) - (hmargin * 2));
      olength = (uint32)((plength * page->vres) - (vmargin * 2));
      }
    else
      {
      SensorCall();owidth = (uint32)(iwidth - (hmargin * 2 * page->hres));
      olength = (uint32)(ilength - (vmargin * 2 * page->vres));
      }
    }

  SensorCall();if (owidth > iwidth)
    {/*251*/SensorCall();owidth = iwidth;/*252*/}
  SensorCall();if (olength > ilength)
    {/*253*/SensorCall();olength = ilength;/*254*/}

  /* Compute the number of pages required for Portrait or Landscape */
  SensorCall();switch (page->orient)
    {
    case ORIENTATION_NONE:
    case ORIENTATION_PORTRAIT:
         SensorCall();ocols = TIFFhowmany(iwidth, owidth);
         orows = TIFFhowmany(ilength, olength);
         orientation = ORIENTATION_PORTRAIT;
         SensorCall();break;

    case ORIENTATION_LANDSCAPE:
         SensorCall();ocols = TIFFhowmany(iwidth, olength);
         orows = TIFFhowmany(ilength, owidth);
         x1 = olength;
         olength = owidth;
         owidth = x1;
         orientation = ORIENTATION_LANDSCAPE;
         SensorCall();break;

    case ORIENTATION_AUTO:
    default:
         SensorCall();x1 = TIFFhowmany(iwidth, owidth);
         x2 = TIFFhowmany(ilength, olength); 
         y1 = TIFFhowmany(iwidth, olength);
         y2 = TIFFhowmany(ilength, owidth); 

         SensorCall();if ( (x1 * x2) < (y1 * y2))
           { /* Portrait */
           SensorCall();ocols = x1;
           orows = x2;
           orientation = ORIENTATION_PORTRAIT;
	   }
         else
           { /* Landscape */
           SensorCall();ocols = y1;
           orows = y2;
           x1 = olength;
           olength = owidth;
           owidth = x1;
           orientation = ORIENTATION_LANDSCAPE;
           }
    }

  SensorCall();if (ocols < 1)
    {/*255*/SensorCall();ocols = 1;/*256*/}
  SensorCall();if (orows < 1)
    {/*257*/SensorCall();orows = 1;/*258*/}

  /* If user did not specify rows and cols, set them from calcuation */
  SensorCall();if (page->rows < 1)
    {/*259*/SensorCall();page->rows = orows;/*260*/}
  SensorCall();if (page->cols < 1)
    {/*261*/SensorCall();page->cols = ocols;/*262*/}

  SensorCall();line_bytes = TIFFhowmany8(owidth * image->bps) * image->spp;

  SensorCall();if ((page->rows * page->cols) > MAX_SECTIONS)
   {
   SensorCall();TIFFError("computeOutputPixelOffsets",
	     "Rows and Columns exceed maximum sections\nIncrease resolution or reduce sections");
   {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
   }

  /* build the list of offsets for each output section */
  SensorCall();for (k = 0, i = 0 && k <= MAX_SECTIONS; i < orows; i++)
    {
    SensorCall();y1 = (uint32)(olength * i);
    y2 = (uint32)(olength * (i +  1) - 1);
    SensorCall();if (y2 >= ilength)
      {/*263*/SensorCall();y2 = ilength - 1;/*264*/}
    SensorCall();for (j = 0; j < ocols; j++, k++)
      {
      SensorCall();x1 = (uint32)(owidth * j); 
      x2 = (uint32)(owidth * (j + 1) - 1);
      SensorCall();if (x2 >= iwidth)
        {/*265*/SensorCall();x2 = iwidth - 1;/*266*/}
      SensorCall();sections[k].x1 = x1;
      sections[k].x2 = x2;
      sections[k].y1 = y1;
      sections[k].y2 = y2;
      sections[k].buffsize = line_bytes * olength;
      sections[k].position = k + 1;
      sections[k].total = orows * ocols;
      } 
    } 
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end computeOutputPixelOffsets */

static int
loadImage(TIFF* in, struct image_data *image, struct dump_opts *dump, unsigned char **read_ptr)
  {
  SensorCall();uint32   i;
  float    xres = 0.0, yres = 0.0;
  uint16   nstrips = 0, ntiles = 0, planar = 0;
  uint16   bps = 0, spp = 0, res_unit = 0;
  uint16   orientation = 0;
  uint16   input_compression = 0, input_photometric = 0;
  uint16   subsampling_horiz, subsampling_vert;
  uint32   width = 0, length = 0;
  uint32   stsize = 0, tlsize = 0, buffsize = 0, scanlinesize = 0;
  uint32   tw = 0, tl = 0;       /* Tile width and length */
  uint32   tile_rowsize = 0;
  unsigned char *read_buff = NULL;
  unsigned char *new_buff  = NULL;
  int      readunit = 0;
  static   uint32  prev_readsize = 0;

  TIFFGetFieldDefaulted(in, TIFFTAG_BITSPERSAMPLE, &bps);
  TIFFGetFieldDefaulted(in, TIFFTAG_SAMPLESPERPIXEL, &spp);
  TIFFGetFieldDefaulted(in, TIFFTAG_PLANARCONFIG, &planar);
  TIFFGetFieldDefaulted(in, TIFFTAG_ORIENTATION, &orientation);
  SensorCall();if (! TIFFGetFieldDefaulted(in, TIFFTAG_PHOTOMETRIC, &input_photometric))
    {/*267*/SensorCall();TIFFError("loadImage","Image lacks Photometric interpreation tag");/*268*/}
  SensorCall();if (! TIFFGetField(in, TIFFTAG_IMAGEWIDTH,  &width))
    {/*269*/SensorCall();TIFFError("loadimage","Image lacks image width tag");/*270*/}
  SensorCall();if(! TIFFGetField(in, TIFFTAG_IMAGELENGTH, &length))
    {/*271*/SensorCall();TIFFError("loadimage","Image lacks image length tag");/*272*/}
  SensorCall();TIFFGetFieldDefaulted(in, TIFFTAG_XRESOLUTION, &xres);
  TIFFGetFieldDefaulted(in, TIFFTAG_YRESOLUTION, &yres);
  SensorCall();if (!TIFFGetFieldDefaulted(in, TIFFTAG_RESOLUTIONUNIT, &res_unit))
    res_unit = RESUNIT_INCH;
  SensorCall();if (!TIFFGetField(in, TIFFTAG_COMPRESSION, &input_compression))
    input_compression = COMPRESSION_NONE;

#ifdef DEBUG2
  char compressionid[16];

  switch (input_compression)
    {
    case COMPRESSION_NONE:	/* 1  dump mode */
	 strcpy (compressionid, "None/dump");
         break;         
    case COMPRESSION_CCITTRLE:	  /* 2 CCITT modified Huffman RLE */
	 strcpy (compressionid, "Huffman RLE");
         break;         
    case COMPRESSION_CCITTFAX3:	  /* 3 CCITT Group 3 fax encoding */
	 strcpy (compressionid, "Group3 Fax");
         break;         
    case COMPRESSION_CCITTFAX4:	  /* 4 CCITT Group 4 fax encoding */
	 strcpy (compressionid, "Group4 Fax");
         break;         
    case COMPRESSION_LZW:	  /* 5 Lempel-Ziv  & Welch */
	 strcpy (compressionid, "LZW");
         break;         
    case COMPRESSION_OJPEG:	  /* 6 !6.0 JPEG */
	 strcpy (compressionid, "Old Jpeg");
         break;         
    case COMPRESSION_JPEG:	  /* 7 %JPEG DCT compression */
	 strcpy (compressionid, "New Jpeg");
         break;         
    case COMPRESSION_NEXT:	  /* 32766 NeXT 2-bit RLE */
	 strcpy (compressionid, "Next RLE");
         break;         
    case COMPRESSION_CCITTRLEW:   /* 32771 #1 w/ word alignment */
	 strcpy (compressionid, "CITTRLEW");
         break;         
    case COMPRESSION_PACKBITS:	  /* 32773 Macintosh RLE */
	 strcpy (compressionid, "Mac Packbits");
         break;         
    case COMPRESSION_THUNDERSCAN: /* 32809 ThunderScan RLE */
	 strcpy (compressionid, "Thunderscan");
         break;         
    case COMPRESSION_IT8CTPAD:	  /* 32895 IT8 CT w/padding */
	 strcpy (compressionid, "IT8 padded");
         break;         
    case COMPRESSION_IT8LW:	  /* 32896 IT8 Linework RLE */
	 strcpy (compressionid, "IT8 RLE");
         break;         
    case COMPRESSION_IT8MP:	  /* 32897 IT8 Monochrome picture */
	 strcpy (compressionid, "IT8 mono");
         break;         
    case COMPRESSION_IT8BL:	  /* 32898 IT8 Binary line art */
	 strcpy (compressionid, "IT8 lineart");
         break;         
    case COMPRESSION_PIXARFILM:	  /* 32908 Pixar companded 10bit LZW */
	 strcpy (compressionid, "Pixar 10 bit");
         break;         
    case COMPRESSION_PIXARLOG:	  /* 32909 Pixar companded 11bit ZIP */
	 strcpy (compressionid, "Pixar 11bit");
         break;         
    case COMPRESSION_DEFLATE:	  /* 32946 Deflate compression */
	 strcpy (compressionid, "Deflate");
         break;         
    case COMPRESSION_ADOBE_DEFLATE: /* 8 Deflate compression */
	 strcpy (compressionid, "Adobe deflate");
         break;         
    default:
	 strcpy (compressionid, "None/unknown");
         break;         
    }
  TIFFError("loadImage", "Input compression %s", compressionid);
#endif

  SensorCall();scanlinesize = TIFFScanlineSize(in);
  image->bps = bps;
  image->spp = spp;
  image->planar = planar;
  image->width = width;
  image->length = length;
  image->xres = xres;
  image->yres = yres;
  image->res_unit = res_unit;
  image->compression = input_compression;
  image->photometric = input_photometric;
#ifdef DEBUG2
  char photometricid[12];

  switch (input_photometric)
    {
    case PHOTOMETRIC_MINISWHITE:
         strcpy (photometricid, "MinIsWhite");
         break;
    case PHOTOMETRIC_MINISBLACK:
         strcpy (photometricid, "MinIsBlack");
         break;
    case PHOTOMETRIC_RGB:
         strcpy (photometricid, "RGB");
         break;
    case PHOTOMETRIC_PALETTE:
         strcpy (photometricid, "Palette");
         break;
    case PHOTOMETRIC_MASK:
         strcpy (photometricid, "Mask");
         break;
    case PHOTOMETRIC_SEPARATED:
         strcpy (photometricid, "Separated");
         break;
    case PHOTOMETRIC_YCBCR:
         strcpy (photometricid, "YCBCR");
         break;
    case PHOTOMETRIC_CIELAB:
         strcpy (photometricid, "CIELab");
         break;
    case PHOTOMETRIC_ICCLAB:
         strcpy (photometricid, "ICCLab");
         break;
    case PHOTOMETRIC_ITULAB:
         strcpy (photometricid, "ITULab");
         break;
    case PHOTOMETRIC_LOGL:
         strcpy (photometricid, "LogL");
         break;
    case PHOTOMETRIC_LOGLUV:
         strcpy (photometricid, "LOGLuv");
         break;
    default:
         strcpy (photometricid, "Unknown");
         break;
    }
  TIFFError("loadImage", "Input photometric interpretation %s", photometricid);

#endif
  image->orientation = orientation;
  SensorCall();switch (orientation)
    {
    case 0:
    case ORIENTATION_TOPLEFT:
         SensorCall();image->adjustments = 0;
	 SensorCall();break;
    case ORIENTATION_TOPRIGHT:
         SensorCall();image->adjustments = MIRROR_HORIZ;
	 SensorCall();break;
    case ORIENTATION_BOTRIGHT:
         SensorCall();image->adjustments = ROTATECW_180;
	 SensorCall();break;
    case ORIENTATION_BOTLEFT:
         SensorCall();image->adjustments = MIRROR_VERT; 
	 SensorCall();break;
    case ORIENTATION_LEFTTOP:
         SensorCall();image->adjustments = MIRROR_VERT | ROTATECW_90;
	 SensorCall();break;
    case ORIENTATION_RIGHTTOP:
         SensorCall();image->adjustments = ROTATECW_90;
	 SensorCall();break;
    case ORIENTATION_RIGHTBOT:
         SensorCall();image->adjustments = MIRROR_VERT | ROTATECW_270;
	 SensorCall();break; 
    case ORIENTATION_LEFTBOT:
         SensorCall();image->adjustments = ROTATECW_270;
	 SensorCall();break;
    default:
         SensorCall();image->adjustments = 0;
         image->orientation = ORIENTATION_TOPLEFT;
   }

  SensorCall();if ((bps == 0) || (spp == 0))
    {
    SensorCall();TIFFError("loadImage", "Invalid samples per pixel (%d) or bits per sample (%d)",
	       spp, bps);
    {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();if (TIFFIsTiled(in))
    {
    SensorCall();readunit = TILE;
    tlsize = TIFFTileSize(in);
    ntiles = TIFFNumberOfTiles(in);
    TIFFGetField(in, TIFFTAG_TILEWIDTH, &tw);
    TIFFGetField(in, TIFFTAG_TILELENGTH, &tl);

    tile_rowsize  = TIFFTileRowSize(in);      
    SensorCall();if (ntiles == 0 || tlsize == 0 || tile_rowsize == 0)
    {
	SensorCall();TIFFError("loadImage", "File appears to be tiled, but the number of tiles, tile size, or tile rowsize is zero.");
	exit(-1);
    }
    SensorCall();buffsize = tlsize * ntiles;
    SensorCall();if (tlsize != (buffsize / ntiles))
    {
	SensorCall();TIFFError("loadImage", "Integer overflow when calculating buffer size");
	exit(-1);
    }

    SensorCall();if (buffsize < (uint32)(ntiles * tl * tile_rowsize))
      {
      SensorCall();buffsize = ntiles * tl * tile_rowsize;
      SensorCall();if (ntiles != (buffsize / tl / tile_rowsize))
      {
	SensorCall();TIFFError("loadImage", "Integer overflow when calculating buffer size");
	exit(-1);
      }
      
#ifdef DEBUG2
      TIFFError("loadImage",
	        "Tilesize %u is too small, using ntiles * tilelength * tilerowsize %lu",
                tlsize, (unsigned long)buffsize);
#endif
      }
    
    SensorCall();if (dump->infile != NULL)
      {/*273*/SensorCall();dump_info (dump->infile, dump->format, "", 
                 "Tilesize: %u, Number of Tiles: %u, Tile row size: %u",
                 tlsize, ntiles, tile_rowsize);/*274*/}
    }
  else
    {
    SensorCall();readunit = STRIP;
    TIFFGetFieldDefaulted(in, TIFFTAG_ROWSPERSTRIP, &rowsperstrip);
    stsize = TIFFStripSize(in);
    nstrips = TIFFNumberOfStrips(in);
    SensorCall();if (nstrips == 0 || stsize == 0)
    {
	SensorCall();TIFFError("loadImage", "File appears to be striped, but the number of stipes or stripe size is zero.");
	exit(-1);
    }

    SensorCall();buffsize = stsize * nstrips;
    SensorCall();if (stsize != (buffsize / nstrips))
    {
	SensorCall();TIFFError("loadImage", "Integer overflow when calculating buffer size");
	exit(-1);
    }
    SensorCall();uint32 buffsize_check;
    buffsize_check = ((length * width * spp * bps) + 7);
    SensorCall();if (length != ((buffsize_check - 7) / width / spp / bps))
    {
	SensorCall();TIFFError("loadImage", "Integer overflow detected.");
	exit(-1);
    }
    SensorCall();if (buffsize < (uint32) (((length * width * spp * bps) + 7) / 8))
      {
      SensorCall();buffsize =  ((length * width * spp * bps) + 7) / 8;
#ifdef DEBUG2
      TIFFError("loadImage",
	        "Stripsize %u is too small, using imagelength * width * spp * bps / 8 = %lu",
                stsize, (unsigned long)buffsize);
#endif
      }
    
    SensorCall();if (dump->infile != NULL)
      {/*275*/SensorCall();dump_info (dump->infile, dump->format, "",
                 "Stripsize: %u, Number of Strips: %u, Rows per Strip: %u, Scanline size: %u",
		 stsize, nstrips, rowsperstrip, scanlinesize);/*276*/}
    }
  
  SensorCall();if (input_compression == COMPRESSION_JPEG)
    {  /* Force conversion to RGB */
    SensorCall();jpegcolormode = JPEGCOLORMODE_RGB;
    TIFFSetField(in, TIFFTAG_JPEGCOLORMODE, JPEGCOLORMODE_RGB);
    }
  /* The clause up to the read statement is taken from Tom Lane's tiffcp patch */
  else 
    {   /* Otherwise, can't handle subsampled input */
    SensorCall();if (input_photometric == PHOTOMETRIC_YCBCR)
      {
      SensorCall();TIFFGetFieldDefaulted(in, TIFFTAG_YCBCRSUBSAMPLING,
 		           &subsampling_horiz, &subsampling_vert);
      SensorCall();if (subsampling_horiz != 1 || subsampling_vert != 1)
        {
	SensorCall();TIFFError("loadImage", 
		"Can't copy/convert subsampled image with subsampling %d horiz %d vert",
                subsampling_horiz, subsampling_vert);
        {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
        }
	}
    }
 
  SensorCall();read_buff = *read_ptr;
  SensorCall();if (!read_buff)
    {/*277*/SensorCall();read_buff = (unsigned char *)_TIFFmalloc(buffsize);/*278*/}
  else
    {
    SensorCall();if (prev_readsize < buffsize)
      {
      SensorCall();new_buff = _TIFFrealloc(read_buff, buffsize);
      SensorCall();if (!new_buff)
        {
	SensorCall();free (read_buff);
        read_buff = (unsigned char *)_TIFFmalloc(buffsize);
        }
      else
        {/*279*/SensorCall();read_buff = new_buff;/*280*/}
      }
    }

  SensorCall();if (!read_buff)
    {
    SensorCall();TIFFError("loadImage", "Unable to allocate/reallocate read buffer");
    {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();prev_readsize = buffsize;
  *read_ptr = read_buff;

  /* N.B. The read functions used copy separate plane data into a buffer as interleaved
   * samples rather than separate planes so the same logic works to extract regions
   * regardless of the way the data are organized in the input file.
   */
  SensorCall();switch (readunit) {
    case STRIP:
         SensorCall();if (planar == PLANARCONFIG_CONTIG)
           {
	     SensorCall();if (!(readContigStripsIntoBuffer(in, read_buff)))
	     {
	     SensorCall();TIFFError("loadImage", "Unable to read contiguous strips into buffer");
	     {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
             }
           }
         else
           {
	   SensorCall();if (!(readSeparateStripsIntoBuffer(in, read_buff, length, width, spp, dump)))
	     {
	     SensorCall();TIFFError("loadImage", "Unable to read separate strips into buffer");
	     {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
             }
           }
         SensorCall();break;

    case TILE:
         SensorCall();if (planar == PLANARCONFIG_CONTIG)
           {
	   SensorCall();if (!(readContigTilesIntoBuffer(in, read_buff, length, width, tw, tl, spp, bps)))
	     {
	     SensorCall();TIFFError("loadImage", "Unable to read contiguous tiles into buffer");
	     {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
             }
           }
         else
           {
	   SensorCall();if (!(readSeparateTilesIntoBuffer(in, read_buff, length, width, tw, tl, spp, bps)))
	     {
	     SensorCall();TIFFError("loadImage", "Unable to read separate tiles into buffer");
	     {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
             }
           }
         SensorCall();break;
    default: SensorCall();TIFFError("loadImage", "Unsupported image file format");
          {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
          break;
    }
  SensorCall();if ((dump->infile != NULL) && (dump->level == 2))
    {
    SensorCall();dump_info  (dump->infile, dump->format, "loadImage", 
                "Image width %d, length %d, Raw image data, %4d bytes",
                width, length,  buffsize);
    dump_info  (dump->infile, dump->format, "", 
                "Bits per sample %d, Samples per pixel %d", bps, spp);

    SensorCall();for (i = 0; i < length; i++)
      {/*281*/SensorCall();dump_buffer(dump->infile, dump->format, 1, scanlinesize, 
                  i, read_buff + (i * scanlinesize));/*282*/}
    }
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }   /* end loadImage */

static int  correct_orientation(struct image_data *image, unsigned char **work_buff_ptr)
  {
  SensorCall();uint16 mirror, rotation;
  unsigned char *work_buff;

  work_buff = *work_buff_ptr;
  SensorCall();if ((image == NULL) || (work_buff == NULL))
    {
    SensorCall();TIFFError ("correct_orientatin", "Invalid image or buffer pointer");
    {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();if ((image->adjustments & MIRROR_HORIZ) || (image->adjustments & MIRROR_VERT))
    {
    SensorCall();mirror = (uint16)(image->adjustments & MIRROR_BOTH);
    SensorCall();if (mirrorImage(image->spp, image->bps, mirror, 
        image->width, image->length, work_buff))
      {
      SensorCall();TIFFError ("correct_orientation", "Unable to mirror image");
      {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
      }
    }

  SensorCall();if (image->adjustments & ROTATE_ANY)
    {
    SensorCall();if (image->adjustments & ROTATECW_90)
      {/*283*/SensorCall();rotation = (uint16) 90;/*284*/}
    else
    {/*285*/SensorCall();if (image->adjustments & ROTATECW_180)
      {/*287*/SensorCall();rotation = (uint16) 180;/*288*/}
    else
    {/*289*/SensorCall();if (image->adjustments & ROTATECW_270)
      {/*291*/SensorCall();rotation = (uint16) 270;/*292*/}
    else
      {
      SensorCall();TIFFError ("correct_orientation", "Invalid rotation value: %d", 
                  image->adjustments & ROTATE_ANY);
      {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
      ;/*290*/}/*286*/}}
 
    SensorCall();if (rotateImage(rotation, image, &image->width, &image->length, work_buff_ptr))
      {
      SensorCall();TIFFError ("correct_orientation", "Unable to rotate image");
      {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
      }
    SensorCall();image->orientation = ORIENTATION_TOPLEFT;
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end correct_orientation */


/* Extract multiple zones from an image and combine into a single composite image */
static int
extractCompositeRegions(struct image_data *image,  struct crop_mask *crop, 
                        unsigned char *read_buff, unsigned char *crop_buff)
  {
  SensorCall();int       shift_width, bytes_per_sample, bytes_per_pixel;
  uint32    i, trailing_bits, prev_trailing_bits;
  uint32    row, first_row, last_row, first_col, last_col;
  uint32    src_rowsize, dst_rowsize, src_offset, dst_offset;
  uint32    crop_width, crop_length, img_width, img_length;
  uint32    prev_length, prev_width, composite_width;
  uint16    bps, spp;
  uint8    *src, *dst;
  tsample_t count, sample = 0;   /* Update to extract one or more samples */

  img_width = image->width;
  img_length = image->length;
  bps = image->bps;
  spp = image->spp;
  count = spp;

  bytes_per_sample = (bps + 7) / 8; 
  bytes_per_pixel  = ((bps * spp) + 7) / 8;
  SensorCall();if ((bps % 8) == 0)
    {/*529*/SensorCall();shift_width = 0;/*530*/}
  else
    {
    SensorCall();if (bytes_per_pixel < (bytes_per_sample + 1))
      {/*531*/SensorCall();shift_width = bytes_per_pixel;/*532*/}
    else
      {/*533*/SensorCall();shift_width = bytes_per_sample + 1;/*534*/}
    }
  SensorCall();src = read_buff;
  dst = crop_buff;

  /* These are setup for adding additional sections */
  prev_width = prev_length = 0;
  prev_trailing_bits = trailing_bits = 0;
  composite_width = crop->combined_width;
  crop->combined_width = 0;
  crop->combined_length = 0;

  SensorCall();for (i = 0; i < crop->selections; i++)
    {
    /* rows, columns, width, length are expressed in pixels */
    SensorCall();first_row = crop->regionlist[i].y1;
    last_row  = crop->regionlist[i].y2;
    first_col = crop->regionlist[i].x1;
    last_col  = crop->regionlist[i].x2;

    crop_width = last_col - first_col + 1;
    crop_length = last_row - first_row + 1;

    /* These should not be needed for composite images */
    crop->regionlist[i].width = crop_width;
    crop->regionlist[i].length = crop_length;
    crop->regionlist[i].buffptr = crop_buff;

    src_rowsize = ((img_width * bps * spp) + 7) / 8;
    dst_rowsize = (((crop_width * bps * count) + 7) / 8);

    SensorCall();switch (crop->edge_ref)
      {
      default:
      case EDGE_TOP:
      case EDGE_BOTTOM:
	   SensorCall();if ((i > 0) && (crop_width != crop->regionlist[i - 1].width))
             {
	     SensorCall();TIFFError ("extractCompositeRegions", 
                          "Only equal width regions can be combined for -E top or bottom");
	     {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
             }

           SensorCall();crop->combined_width = crop_width;
           crop->combined_length += crop_length;

           SensorCall();for (row = first_row; row <= last_row; row++)
             {
	     SensorCall();src_offset = row * src_rowsize;
	     dst_offset = (row - first_row) * dst_rowsize;
             src = read_buff + src_offset;
             dst = crop_buff + dst_offset + (prev_length * dst_rowsize);
             SensorCall();switch (shift_width)
               {
               case 0: SensorCall();if (extractContigSamplesBytes (src, dst, img_width, sample,
                                                      spp, bps, count, first_col,
                                                      last_col + 1))
                         {
		         SensorCall();TIFFError("extractCompositeRegions",
                                   "Unable to extract row %d", row);
		         {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		         }
		       SensorCall();break;
               case 1: SensorCall();if (bps == 1)
                         { 
                         SensorCall();if (extractContigSamplesShifted8bits (src, dst, img_width,
                                                               sample, spp, bps, count, 
                                                               first_col, last_col + 1,
                                                               prev_trailing_bits))
                           {
		           SensorCall();TIFFError("extractCompositeRegions",
                                     "Unable to extract row %d", row);
		           {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		           }
		         SensorCall();break;
			 }
                       else
                         {/*535*/SensorCall();if (extractContigSamplesShifted16bits (src, dst, img_width,
                                                                sample, spp, bps, count, 
                                                                first_col, last_col + 1,
                                                                prev_trailing_bits))
                           {
		           SensorCall();TIFFError("extractCompositeRegions",
                                     "Unable to extract row %d", row);
		           {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		           ;/*536*/}}
		        SensorCall();break;
               case 2:  SensorCall();if (extractContigSamplesShifted24bits (src, dst, img_width,
                                                               sample, spp, bps, count, 
                                                               first_col, last_col + 1,
                                                               prev_trailing_bits))
                          {
		          SensorCall();TIFFError("extractCompositeRegions",
                                    "Unable to extract row %d", row);
		          {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		          }
		        SensorCall();break;
               case 3:
               case 4:
               case 5:  SensorCall();if (extractContigSamplesShifted32bits (src, dst, img_width,
                                                               sample, spp, bps, count, 
                                                               first_col, last_col + 1,
                                                               prev_trailing_bits))
                          {
		          SensorCall();TIFFError("extractCompositeRegions",
                                    "Unable to extract row %d", row);
		          {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		          }
		        SensorCall();break;
               default: SensorCall();TIFFError("extractCompositeRegions", "Unsupported bit depth %d", bps);
		        {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	       }
             }
           SensorCall();prev_length += crop_length;
	   SensorCall();break;
      case EDGE_LEFT:  /* splice the pieces of each row together, side by side */
      case EDGE_RIGHT:
	   SensorCall();if ((i > 0) && (crop_length != crop->regionlist[i - 1].length))
             {
	     SensorCall();TIFFError ("extractCompositeRegions", 
                          "Only equal length regions can be combined for -E left or right");
	     {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
             }
           SensorCall();crop->combined_width += crop_width;
           crop->combined_length = crop_length;
           dst_rowsize = (((composite_width * bps * count) + 7) / 8);
           trailing_bits = (crop_width * bps * count) % 8;
           SensorCall();for (row = first_row; row <= last_row; row++)
             {
	     SensorCall();src_offset = row * src_rowsize;
	     dst_offset = (row - first_row) * dst_rowsize;
             src = read_buff + src_offset;
             dst = crop_buff + dst_offset + prev_width;

             SensorCall();switch (shift_width)
               {
               case 0: SensorCall();if (extractContigSamplesBytes (src, dst, img_width,
                                                      sample, spp, bps, count,
                                                      first_col, last_col + 1))
                         {
		         SensorCall();TIFFError("extractCompositeRegions",
                                   "Unable to extract row %d", row);
		         {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		         }
		       SensorCall();break;
               case 1: SensorCall();if (bps == 1)
                         { 
                         SensorCall();if (extractContigSamplesShifted8bits (src, dst, img_width,
                                                               sample, spp, bps, count, 
                                                               first_col, last_col + 1,
                                                               prev_trailing_bits))
                           {
		           SensorCall();TIFFError("extractCompositeRegions",
                                     "Unable to extract row %d", row);
		           {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		           }
		         SensorCall();break;
			 }
                       else
                         {/*537*/SensorCall();if (extractContigSamplesShifted16bits (src, dst, img_width,
                                                                sample, spp, bps, count, 
                                                                first_col, last_col + 1,
                                                                prev_trailing_bits))
                           {
		           SensorCall();TIFFError("extractCompositeRegions",
                                     "Unable to extract row %d", row);
		           {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		           ;/*538*/}}
		        SensorCall();break;
              case 2:  SensorCall();if (extractContigSamplesShifted24bits (src, dst, img_width,
                                                               sample, spp, bps, count, 
                                                               first_col, last_col + 1,
                                                               prev_trailing_bits))
                          {
		          SensorCall();TIFFError("extractCompositeRegions",
                                    "Unable to extract row %d", row);
		          {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		          }
		        SensorCall();break;
               case 3:
               case 4:
               case 5:  SensorCall();if (extractContigSamplesShifted32bits (src, dst, img_width,
                                                               sample, spp, bps, count, 
                                                               first_col, last_col + 1,
                                                               prev_trailing_bits))
                          {
		          SensorCall();TIFFError("extractCompositeRegions",
                                    "Unable to extract row %d", row);
		          {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		          }
		        SensorCall();break;
               default: SensorCall();TIFFError("extractCompositeRegions", "Unsupported bit depth %d", bps);
		        {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	       }
	     }
	   SensorCall();prev_width += (crop_width * bps * count) / 8;
           prev_trailing_bits += trailing_bits;
           SensorCall();if (prev_trailing_bits > 7)
	     {/*539*/SensorCall();prev_trailing_bits-= 8;/*540*/}
	   SensorCall();break;
      }
    }
  SensorCall();if (crop->combined_width != composite_width)
    {/*541*/SensorCall();TIFFError("combineSeparateRegions","Combined width does not match composite width");/*542*/}
      
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }  /* end extractCompositeRegions */

/* Copy a single region of input buffer to an output buffer. 
 * The read functions used copy separate plane data into a buffer 
 * as interleaved samples rather than separate planes so the same
 * logic works to extract regions regardless of the way the data 
 * are organized in the input file. This function can be used to
 * extract one or more samples from the input image by updating the 
 * parameters for starting sample and number of samples to copy in the
 * fifth and eighth arguments of the call to extractContigSamples.
 * They would be passed as new elements of the crop_mask struct.
 */

static int
extractSeparateRegion(struct image_data *image,  struct crop_mask *crop,
                      unsigned char *read_buff, unsigned char *crop_buff,
                      int region)
  {
  SensorCall();int     shift_width, prev_trailing_bits = 0;
  uint32  bytes_per_sample, bytes_per_pixel;
  uint32  src_rowsize, dst_rowsize;
  uint32  row, first_row, last_row, first_col, last_col;
  uint32  src_offset, dst_offset;
  uint32  crop_width, crop_length, img_width, img_length;
  uint16  bps, spp;
  uint8  *src, *dst;
  tsample_t count, sample = 0;   /* Update to extract more or more samples */

  img_width = image->width;
  img_length = image->length;
  bps = image->bps;
  spp = image->spp;
  count = spp;

  bytes_per_sample = (bps + 7) / 8; 
  bytes_per_pixel  = ((bps * spp) + 7) / 8;
  SensorCall();if ((bps % 8) == 0)
    {/*521*/SensorCall();shift_width = 0;/*522*/} /* Byte aligned data only */
  else
    {
    SensorCall();if (bytes_per_pixel < (bytes_per_sample + 1))
      {/*523*/SensorCall();shift_width = bytes_per_pixel;/*524*/}
    else
      {/*525*/SensorCall();shift_width = bytes_per_sample + 1;/*526*/}
    }

  /* rows, columns, width, length are expressed in pixels */
  SensorCall();first_row = crop->regionlist[region].y1;
  last_row  = crop->regionlist[region].y2;
  first_col = crop->regionlist[region].x1;
  last_col  = crop->regionlist[region].x2;

  crop_width = last_col - first_col + 1;
  crop_length = last_row - first_row + 1;

  crop->regionlist[region].width = crop_width;
  crop->regionlist[region].length = crop_length;
  crop->regionlist[region].buffptr = crop_buff;

  src = read_buff;
  dst = crop_buff;
  src_rowsize = ((img_width * bps * spp) + 7) / 8;
  dst_rowsize = (((crop_width * bps * spp) + 7) / 8);

  SensorCall();for (row = first_row; row <= last_row; row++)
    {
    SensorCall();src_offset = row * src_rowsize;
    dst_offset = (row  - first_row) * dst_rowsize;
    src = read_buff + src_offset;
    dst = crop_buff + dst_offset;

    SensorCall();switch (shift_width)
      {
      case 0: SensorCall();if (extractContigSamplesBytes (src, dst, img_width, sample,
                                             spp, bps, count, first_col,
                                             last_col + 1))
                {
	        SensorCall();TIFFError("extractSeparateRegion",
                          "Unable to extract row %d", row);
	        {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
	        }
	      SensorCall();break;
      case 1: SensorCall();if (bps == 1)
                { 
                SensorCall();if (extractContigSamplesShifted8bits (src, dst, img_width,
                                                      sample, spp, bps, count, 
                                                      first_col, last_col + 1,
                                                      prev_trailing_bits))
                  {
		  SensorCall();TIFFError("extractSeparateRegion",
                            "Unable to extract row %d", row);
		  {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		  }
		  SensorCall();break;
		}
              else
                {/*527*/SensorCall();if (extractContigSamplesShifted16bits (src, dst, img_width,
                                                       sample, spp, bps, count, 
                                                       first_col, last_col + 1,
                                                       prev_trailing_bits))
                  {
		  SensorCall();TIFFError("extractSeparateRegion",
                            "Unable to extract row %d", row);
		  {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		  ;/*528*/}}
	      SensorCall();break;
      case 2:  SensorCall();if (extractContigSamplesShifted24bits (src, dst, img_width,
                                                     sample, spp, bps, count, 
                                                     first_col, last_col + 1,
                                                     prev_trailing_bits))
                {
		SensorCall();TIFFError("extractSeparateRegion",
                          "Unable to extract row %d", row);
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		}
	      SensorCall();break;
      case 3:
      case 4:
      case 5:  SensorCall();if (extractContigSamplesShifted32bits (src, dst, img_width,
                                                     sample, spp, bps, count, 
                                                     first_col, last_col + 1,
                                                     prev_trailing_bits))
                {
		SensorCall();TIFFError("extractSeparateRegion",
                          "Unable to extract row %d", row);
		{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		}
	      SensorCall();break;
      default: SensorCall();TIFFError("extractSeparateRegion", "Unsupported bit depth %d", bps);
	       {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
      }
    }
          
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }  /* end extractSeparateRegion */

static int
extractImageSection(struct image_data *image, struct pageseg *section, 
                    unsigned char *src_buff, unsigned char *sect_buff)
  {
  SensorCall();unsigned  char  bytebuff1, bytebuff2;
  unsigned  char *src, *dst;

  uint32    img_width, img_length, img_rowsize;
  uint32    j, shift1, shift2, trailing_bits;
  uint32    row, first_row, last_row, first_col, last_col;
  uint32    src_offset, dst_offset, row_offset, col_offset;
  uint32    offset1, offset2, full_bytes;
  uint32    sect_width, sect_length;
  uint16    bps, spp;

#ifdef DEVELMODE
  int      k;
  unsigned char bitset;
  static char *bitarray = NULL;
#endif

  img_width = image->width;
  img_length = image->length;
  bps = image->bps;
  spp = image->spp;

  src = src_buff;
  dst = sect_buff;
  src_offset = 0;
  dst_offset = 0;

#ifdef DEVELMODE
  if (bitarray == NULL)
    {
    if ((bitarray = (char *)malloc(img_width)) == NULL)
      {
      TIFFError ("", "DEBUG: Unable to allocate debugging bitarray");
      return (-1);
      }
    }
#endif

  /* rows, columns, width, length are expressed in pixels */
  first_row = section->y1;
  last_row  = section->y2;
  first_col = section->x1;
  last_col  = section->x2;

  sect_width = last_col - first_col + 1;
  sect_length = last_row - first_row + 1;
  img_rowsize = ((img_width * bps + 7) / 8) * spp;
  full_bytes = (sect_width * spp * bps) / 8;   /* number of COMPLETE bytes per row in section */
  trailing_bits = (sect_width * bps) % 8;

#ifdef DEVELMODE
    TIFFError ("", "First row: %d, last row: %d, First col: %d, last col: %d\n",
           first_row, last_row, first_col, last_col);
    TIFFError ("", "Image width: %d, Image length: %d, bps: %d, spp: %d\n",
	   img_width, img_length, bps, spp);
    TIFFError ("", "Sect  width: %d,  Sect length: %d, full bytes: %d trailing bits %d\n", 
           sect_width, sect_length, full_bytes, trailing_bits);
#endif

  SensorCall();if ((bps % 8) == 0)
    {
    SensorCall();col_offset = first_col * spp * bps / 8;
    SensorCall();for (row = first_row; row <= last_row; row++)
      {
      /* row_offset = row * img_width * spp * bps / 8; */
      SensorCall();row_offset = row * img_rowsize;
      src_offset = row_offset + col_offset;

#ifdef DEVELMODE
        TIFFError ("", "Src offset: %8d, Dst offset: %8d", src_offset, dst_offset); 
#endif
      _TIFFmemcpy (sect_buff + dst_offset, src_buff + src_offset, full_bytes);
      dst_offset += full_bytes;
      }        
    }
  else
    { /* bps != 8 */
    SensorCall();shift1  = spp * ((first_col * bps) % 8);
    shift2  = spp * ((last_col * bps) % 8);
    SensorCall();for (row = first_row; row <= last_row; row++)
      {
      /* pull out the first byte */
      SensorCall();row_offset = row * img_rowsize;
      offset1 = row_offset + (first_col * bps / 8);
      offset2 = row_offset + (last_col * bps / 8);

#ifdef DEVELMODE
      for (j = 0, k = 7; j < 8; j++, k--)
        {
        bitset = *(src_buff + offset1) & (((unsigned char)1 << k)) ? 1 : 0;
        sprintf(&bitarray[j], (bitset) ? "1" : "0");
        }
      sprintf(&bitarray[8], " ");
      sprintf(&bitarray[9], " ");
      for (j = 10, k = 7; j < 18; j++, k--)
        {
        bitset = *(src_buff + offset2) & (((unsigned char)1 << k)) ? 1 : 0;
        sprintf(&bitarray[j], (bitset) ? "1" : "0");
        }
      bitarray[18] = '\0';
      TIFFError ("", "Row: %3d Offset1: %d,  Shift1: %d,    Offset2: %d,  Shift2:  %d\n", 
                 row, offset1, shift1, offset2, shift2); 
#endif

      bytebuff1 = bytebuff2 = 0;
      SensorCall();if (shift1 == 0) /* the region is byte and sample alligned */
        {
	SensorCall();_TIFFmemcpy (sect_buff + dst_offset, src_buff + offset1, full_bytes);

#ifdef DEVELMODE
	TIFFError ("", "        Alligned data src offset1: %8d, Dst offset: %8d\n", offset1, dst_offset); 
	sprintf(&bitarray[18], "\n");
	sprintf(&bitarray[19], "\t");
        for (j = 20, k = 7; j < 28; j++, k--)
          {
          bitset = *(sect_buff + dst_offset) & (((unsigned char)1 << k)) ? 1 : 0;
          sprintf(&bitarray[j], (bitset) ? "1" : "0");
          }
        bitarray[28] = ' ';
        bitarray[29] = ' ';
#endif
        dst_offset += full_bytes;

        SensorCall();if (trailing_bits != 0)
          {
	  SensorCall();bytebuff2 = src_buff[offset2] & ((unsigned char)255 << (7 - shift2));
          sect_buff[dst_offset] = bytebuff2;
#ifdef DEVELMODE
	  TIFFError ("", "        Trailing bits src offset:  %8d, Dst offset: %8d\n", 
                              offset2, dst_offset); 
          for (j = 30, k = 7; j < 38; j++, k--)
            {
            bitset = *(sect_buff + dst_offset) & (((unsigned char)1 << k)) ? 1 : 0;
            sprintf(&bitarray[j], (bitset) ? "1" : "0");
            }
          bitarray[38] = '\0';
          TIFFError ("", "\tFirst and last bytes before and after masking:\n\t%s\n\n", bitarray);
#endif
          dst_offset++;
          }
        }
      else   /* each destination byte will have to be built from two source bytes*/
        {
#ifdef DEVELMODE
	  TIFFError ("", "        Unalligned data src offset: %8d, Dst offset: %8d\n", offset1 , dst_offset); 
#endif
        SensorCall();for (j = 0; j <= full_bytes; j++) 
          {
	  SensorCall();bytebuff1 = src_buff[offset1 + j] & ((unsigned char)255 >> shift1);
	  bytebuff2 = src_buff[offset1 + j + 1] & ((unsigned char)255 << (7 - shift1));
          sect_buff[dst_offset + j] = (bytebuff1 << shift1) | (bytebuff2 >> (8 - shift1));
          }
#ifdef DEVELMODE
	sprintf(&bitarray[18], "\n");
	sprintf(&bitarray[19], "\t");
        for (j = 20, k = 7; j < 28; j++, k--)
          {
          bitset = *(sect_buff + dst_offset) & (((unsigned char)1 << k)) ? 1 : 0;
          sprintf(&bitarray[j], (bitset) ? "1" : "0");
          }
        bitarray[28] = ' ';
        bitarray[29] = ' ';
#endif
        SensorCall();dst_offset += full_bytes;

        SensorCall();if (trailing_bits != 0)
          {
#ifdef DEVELMODE
	    TIFFError ("", "        Trailing bits   src offset: %8d, Dst offset: %8d\n", offset1 + full_bytes, dst_offset); 
#endif
	  SensorCall();if (shift2 > shift1)
            {
	    SensorCall();bytebuff1 = src_buff[offset1 + full_bytes] & ((unsigned char)255 << (7 - shift2));
            bytebuff2 = bytebuff1 & ((unsigned char)255 << shift1);
            sect_buff[dst_offset] = bytebuff2;
#ifdef DEVELMODE
	    TIFFError ("", "        Shift2 > Shift1\n"); 
#endif
            }
          else
            {
	    SensorCall();if (shift2 < shift1)
              {
              SensorCall();bytebuff2 = ((unsigned char)255 << (shift1 - shift2 - 1));
	      sect_buff[dst_offset] &= bytebuff2;
#ifdef DEVELMODE
	      TIFFError ("", "        Shift2 < Shift1\n"); 
#endif
              }
#ifdef DEVELMODE
            else
	      TIFFError ("", "        Shift2 == Shift1\n"); 
#endif
            }
	  }
#ifdef DEVELMODE
	  sprintf(&bitarray[28], " ");
	  sprintf(&bitarray[29], " ");
          for (j = 30, k = 7; j < 38; j++, k--)
            {
            bitset = *(sect_buff + dst_offset) & (((unsigned char)1 << k)) ? 1 : 0;
            sprintf(&bitarray[j], (bitset) ? "1" : "0");
            }
          bitarray[38] = '\0';
          TIFFError ("", "\tFirst and last bytes before and after masking:\n\t%s\n\n", bitarray);
#endif
        SensorCall();dst_offset++;
        }
      }
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end extractImageSection */

static int 
writeSelections(TIFF *in, TIFF **out, struct crop_mask *crop, 
                struct image_data *image, struct dump_opts *dump,
                struct buffinfo seg_buffs[], char *mp, char *filename, 
                unsigned int *page, unsigned int total_pages)
  {
  SensorCall();int i, page_count;
  int autoindex = 0;
  unsigned char *crop_buff = NULL;

  /* Where we open a new file depends on the export mode */  
  SensorCall();switch (crop->exp_mode)
    {
    case ONE_FILE_COMPOSITE: /* Regions combined into single image */
         SensorCall();autoindex = 0;
         crop_buff = seg_buffs[0].buffer;
         SensorCall();if (update_output_file (out, mp, autoindex, filename, page))
           {/*353*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*354*/}
         SensorCall();page_count = total_pages;
         SensorCall();if (writeCroppedImage(in, *out, image, dump,
                               crop->combined_width, 
                               crop->combined_length,
                               crop_buff, *page, total_pages))
            {
             SensorCall();TIFFError("writeRegions", "Unable to write new image");
             {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
             }
	 SensorCall();break;
    case ONE_FILE_SEPARATED: /* Regions as separated images */
         SensorCall();autoindex = 0;
         SensorCall();if (update_output_file (out, mp, autoindex, filename, page))
           {/*355*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*356*/}
         SensorCall();page_count = crop->selections * total_pages;
         SensorCall();for (i = 0; i < crop->selections; i++)
           {
           SensorCall();crop_buff = seg_buffs[i].buffer;
           SensorCall();if (writeCroppedImage(in, *out, image, dump,
                                 crop->regionlist[i].width, 
                                 crop->regionlist[i].length, 
                                 crop_buff, *page, page_count))
             {
             SensorCall();TIFFError("writeRegions", "Unable to write new image");
             {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
             }
	   }
         SensorCall();break;
    case FILE_PER_IMAGE_COMPOSITE: /* Regions as composite image */
         SensorCall();autoindex = 1;
         SensorCall();if (update_output_file (out, mp, autoindex, filename, page))
           {/*357*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*358*/}

         SensorCall();crop_buff = seg_buffs[0].buffer;
         SensorCall();if (writeCroppedImage(in, *out, image, dump,
                               crop->combined_width, 
                               crop->combined_length, 
                               crop_buff, *page, total_pages))
           {
           SensorCall();TIFFError("writeRegions", "Unable to write new image");
           {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
           }
         SensorCall();break;
    case FILE_PER_IMAGE_SEPARATED: /* Regions as separated images */
         SensorCall();autoindex = 1;
         page_count = crop->selections;
         SensorCall();if (update_output_file (out, mp, autoindex, filename, page))
           {/*359*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*360*/}
                
         SensorCall();for (i = 0; i < crop->selections; i++)
           {
           SensorCall();crop_buff = seg_buffs[i].buffer;
           /* Write the current region to the current file */
           SensorCall();if (writeCroppedImage(in, *out, image, dump,
                                 crop->regionlist[i].width, 
                                 crop->regionlist[i].length, 
                                 crop_buff, *page, page_count))
             {
             SensorCall();TIFFError("writeRegions", "Unable to write new image");
             {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
             }
           }
         SensorCall();break;
    case FILE_PER_SELECTION:
         SensorCall();autoindex = 1;
	 page_count = 1;
         SensorCall();for (i = 0; i < crop->selections; i++)
           {
           SensorCall();if (update_output_file (out, mp, autoindex, filename, page))
             {/*361*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*362*/}

           SensorCall();crop_buff = seg_buffs[i].buffer;
           /* Write the current region to the current file */
           SensorCall();if (writeCroppedImage(in, *out, image, dump,
                                 crop->regionlist[i].width, 
                                 crop->regionlist[i].length, 
                                 crop_buff, *page, page_count))
             {
             SensorCall();TIFFError("writeRegions", "Unable to write new image");
             {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
             }
           }
	 SensorCall();break;
    default: {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end writeRegions */

static int
writeImageSections(TIFF *in, TIFF *out, struct image_data *image,
		   struct pagedef *page, struct pageseg *sections,
		   struct dump_opts * dump, unsigned char *src_buff,
                   unsigned char **sect_buff_ptr)
  {
  SensorCall();double  hres, vres;
  uint32  i, k, width, length, sectsize;
  unsigned char *sect_buff = *sect_buff_ptr;

  hres = page->hres;
  vres = page->vres;

  k = page->cols * page->rows;
  SensorCall();if ((k < 1) || (k > MAX_SECTIONS))
   {
   SensorCall();TIFFError("writeImageSections",
	     "%d Rows and Columns exceed maximum sections\nIncrease resolution or reduce sections", k);
   {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
   }

  SensorCall();for (i = 0; i < k; i++)
    {
    SensorCall();width  = sections[i].x2 - sections[i].x1 + 1;
    length = sections[i].y2 - sections[i].y1 + 1;
    sectsize = (uint32)
	    ceil((width * image->bps + 7) / (double)8) * image->spp * length;
    /* allocate a buffer if we don't have one already */
    SensorCall();if (createImageSection(sectsize, sect_buff_ptr))
      {
      SensorCall();TIFFError("writeImageSections", "Unable to allocate section buffer");
      exit (-1);
      }
    SensorCall();sect_buff = *sect_buff_ptr;

    SensorCall();if (extractImageSection (image, &sections[i], src_buff, sect_buff))
      {
      SensorCall();TIFFError("writeImageSections", "Unable to extract image sections");
      exit (-1);
      }

  /* call the write routine here instead of outside the loop */
    SensorCall();if (writeSingleSection(in, out, image, dump, width, length, hres, vres, sect_buff))
      {
      SensorCall();TIFFError("writeImageSections", "Unable to write image section");
      exit (-1);
      }
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end writeImageSections */

/* Code in this function is heavily indebted to code in tiffcp
 * with modifications by Richard Nolde to handle orientation correctly.
 * It will have to be updated significantly if support is added to
 * extract one or more samples from original image since the 
 * original code assumes we are always copying all samples.
 */
static int  
writeSingleSection(TIFF *in, TIFF *out, struct image_data *image,
                   struct dump_opts *dump, uint32 width, uint32 length,
                   double hres, double vres,
                   unsigned char *sect_buff)
  {
  SensorCall();uint16 bps, spp;
  uint16 input_compression, input_photometric;
  uint16 input_planar;
  struct cpTag* p;

  /*  Calling this seems to reset the compression mode on the TIFF *in file.
  TIFFGetField(in, TIFFTAG_JPEGCOLORMODE, &input_jpeg_colormode);
  */
  input_compression = image->compression;
  input_photometric = image->photometric;

  spp = image->spp;
  bps = image->bps;
  TIFFSetField(out, TIFFTAG_IMAGEWIDTH, width);
  TIFFSetField(out, TIFFTAG_IMAGELENGTH, length);
  TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, bps);
  TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, spp);

#ifdef DEBUG2
  TIFFError("writeSingleSection", "Input compression: %s",
	    (input_compression == COMPRESSION_OJPEG) ? "Old Jpeg" :
	    ((input_compression == COMPRESSION_JPEG) ?  "New Jpeg" : "Non Jpeg"));
#endif
  /* This is the global variable compression which is set 
   * if the user has specified a command line option for 
   * a compression option.  Should be passed around in one
   * of the parameters instead of as a global. If no user
   * option specified it will still be (uint16) -1. */
  SensorCall();if (compression != (uint16)-1)
    {/*365*/SensorCall();TIFFSetField(out, TIFFTAG_COMPRESSION, compression);/*366*/}
  else
    { /* OJPEG is no longer supported for writing so upgrade to JPEG */
    SensorCall();if (input_compression == COMPRESSION_OJPEG)
      {
      SensorCall();compression = COMPRESSION_JPEG;
      jpegcolormode = JPEGCOLORMODE_RAW;
      TIFFSetField(out, TIFFTAG_COMPRESSION, COMPRESSION_JPEG);
      }
    else /* Use the compression from the input file */
      {/*367*/SensorCall();TIFFSetField(out, TIFFTAG_COMPRESSION, compression);/*368*/}
    }

  SensorCall();if (compression == COMPRESSION_JPEG)
    {
    SensorCall();if ((input_photometric == PHOTOMETRIC_PALETTE) ||  /* color map indexed */
        (input_photometric == PHOTOMETRIC_MASK))       /* holdout mask */
      {
      SensorCall();TIFFError ("writeSingleSection",
                 "JPEG compression cannot be used with %s image data",
		 (input_photometric == PHOTOMETRIC_PALETTE) ?
                 "palette" : "mask");
      {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
      }
    SensorCall();if ((input_photometric == PHOTOMETRIC_RGB) &&
	(jpegcolormode == JPEGCOLORMODE_RGB))
      {/*369*/SensorCall();TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_YCBCR);/*370*/}
    else
	{/*371*/SensorCall();TIFFSetField(out, TIFFTAG_PHOTOMETRIC, input_photometric);/*372*/}
    }
  else
    {
    SensorCall();if (compression == COMPRESSION_SGILOG || compression == COMPRESSION_SGILOG24)
      {/*373*/SensorCall();TIFFSetField(out, TIFFTAG_PHOTOMETRIC, spp == 1 ?
			PHOTOMETRIC_LOGL : PHOTOMETRIC_LOGLUV);/*374*/}
    else
      {/*375*/SensorCall();TIFFSetField(out, TIFFTAG_PHOTOMETRIC, image->photometric);/*376*/}
    }

#ifdef DEBUG2
  TIFFError("writeSingleSection", "Input photometric: %s",
	    (input_photometric == PHOTOMETRIC_RGB) ? "RGB" :
	    ((input_photometric == PHOTOMETRIC_YCBCR) ?  "YCbCr" : "Not RGB or YCbCr"));
#endif

  SensorCall();if (((input_photometric == PHOTOMETRIC_LOGL) ||
       (input_photometric ==  PHOTOMETRIC_LOGLUV)) &&
      ((compression != COMPRESSION_SGILOG) && 
       (compression != COMPRESSION_SGILOG24)))
    {
    SensorCall();TIFFError("writeSingleSection",
              "LogL and LogLuv source data require SGI_LOG or SGI_LOG24 compression");
    {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }

  if (fillorder != 0)
    TIFFSetField(out, TIFFTAG_FILLORDER, fillorder);
  else
    CopyTag(TIFFTAG_FILLORDER, 1, TIFF_SHORT);

  /* The loadimage function reads input orientation and sets
   * image->orientation. The correct_image_orientation function
   * applies the required rotation and mirror operations to 
   * present the data in TOPLEFT orientation and updates 
   * image->orientation if any transforms are performed, 
   * as per EXIF standard.
   */
  SensorCall();TIFFSetField(out, TIFFTAG_ORIENTATION, image->orientation);

  /*
   * Choose tiles/strip for the output image according to
   * the command line arguments (-tiles, -strips) and the
   * structure of the input image.
   */
  SensorCall();if (outtiled == -1)
    {/*379*/SensorCall();outtiled = TIFFIsTiled(in);/*380*/}
  SensorCall();if (outtiled) {
    /*
     * Setup output file's tile width&height.  If either
     * is not specified, use either the value from the
     * input image or, if nothing is defined, use the
     * library default.
     */
    SensorCall();if (tilewidth == (uint32) 0)
      {/*381*/SensorCall();TIFFGetField(in, TIFFTAG_TILEWIDTH, &tilewidth);/*382*/}
    SensorCall();if (tilelength == (uint32) 0)
      {/*383*/SensorCall();TIFFGetField(in, TIFFTAG_TILELENGTH, &tilelength);/*384*/}

    SensorCall();if (tilewidth == 0 || tilelength == 0)
      {/*385*/SensorCall();TIFFDefaultTileSize(out, &tilewidth, &tilelength);/*386*/}
    SensorCall();TIFFDefaultTileSize(out, &tilewidth, &tilelength);
    TIFFSetField(out, TIFFTAG_TILEWIDTH, tilewidth);
    TIFFSetField(out, TIFFTAG_TILELENGTH, tilelength);
    } else {
       /*
	* RowsPerStrip is left unspecified: use either the
	* value from the input image or, if nothing is defined,
	* use the library default.
	*/
	SensorCall();if (rowsperstrip == (uint32) 0)
          {
	  SensorCall();if (!TIFFGetField(in, TIFFTAG_ROWSPERSTRIP, &rowsperstrip))
	    {/*387*/SensorCall();rowsperstrip = TIFFDefaultStripSize(out, rowsperstrip);/*388*/}
          SensorCall();if (compression != COMPRESSION_JPEG)
            {
  	    SensorCall();if (rowsperstrip > length)
	      {/*389*/SensorCall();rowsperstrip = length;/*390*/}
	    }
	  }
	else 
          {/*391*/SensorCall();if (rowsperstrip == (uint32) -1)
	    {/*393*/SensorCall();rowsperstrip = length;/*394*/}/*392*/}
	SensorCall();TIFFSetField(out, TIFFTAG_ROWSPERSTRIP, rowsperstrip);
	}

  SensorCall();TIFFGetFieldDefaulted(in, TIFFTAG_PLANARCONFIG, &input_planar);
  if (config != (uint16) -1)
    TIFFSetField(out, TIFFTAG_PLANARCONFIG, config);
  else
    CopyField(TIFFTAG_PLANARCONFIG, config);
  if (spp <= 4)
    CopyTag(TIFFTAG_TRANSFERFUNCTION, 4, TIFF_SHORT);
  CopyTag(TIFFTAG_COLORMAP, 4, TIFF_SHORT);

/* SMinSampleValue & SMaxSampleValue */
  SensorCall();switch (compression) {
    /* These are references to GLOBAL variables set by defaults
     * and /or the compression flag
     */
    case COMPRESSION_JPEG:
         SensorCall();if (((bps % 8) == 0) || ((bps % 12) == 0))
	   {
           SensorCall();TIFFSetField(out, TIFFTAG_JPEGQUALITY, quality);
	   TIFFSetField(out, TIFFTAG_JPEGCOLORMODE, JPEGCOLORMODE_RGB);
           }
         else
           {
	   SensorCall();TIFFError("writeSingleSection",
                     "JPEG compression requires 8 or 12 bits per sample");
           {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
           }
	 SensorCall();break;
   case COMPRESSION_LZW:
   case COMPRESSION_ADOBE_DEFLATE:
   case COMPRESSION_DEFLATE:
	if (predictor != (uint16)-1)
          TIFFSetField(out, TIFFTAG_PREDICTOR, predictor);
	else
	  CopyField(TIFFTAG_PREDICTOR, predictor);
	SensorCall();break;
   case COMPRESSION_CCITTFAX3:
   case COMPRESSION_CCITTFAX4:
	if (compression == COMPRESSION_CCITTFAX3) {
          if (g3opts != (uint32) -1)
	    TIFFSetField(out, TIFFTAG_GROUP3OPTIONS, g3opts);
	  else
	    CopyField(TIFFTAG_GROUP3OPTIONS, g3opts);
	} else
	    CopyTag(TIFFTAG_GROUP4OPTIONS, 1, TIFF_LONG);
	    CopyTag(TIFFTAG_BADFAXLINES, 1, TIFF_LONG);
	    CopyTag(TIFFTAG_CLEANFAXDATA, 1, TIFF_LONG);
	    CopyTag(TIFFTAG_CONSECUTIVEBADFAXLINES, 1, TIFF_LONG);
	    CopyTag(TIFFTAG_FAXRECVPARAMS, 1, TIFF_LONG);
	    CopyTag(TIFFTAG_FAXRECVTIME, 1, TIFF_LONG);
	    CopyTag(TIFFTAG_FAXSUBADDRESS, 1, TIFF_ASCII);
	SensorCall();break;
   }
   { SensorCall();uint32 len32;
     void** data;
     SensorCall();if (TIFFGetField(in, TIFFTAG_ICCPROFILE, &len32, &data))
       {/*401*/SensorCall();TIFFSetField(out, TIFFTAG_ICCPROFILE, len32, data);/*402*/}
   }
   { SensorCall();uint16 ninks;
     const char* inknames;
     SensorCall();if (TIFFGetField(in, TIFFTAG_NUMBEROFINKS, &ninks)) {
       SensorCall();TIFFSetField(out, TIFFTAG_NUMBEROFINKS, ninks);
       SensorCall();if (TIFFGetField(in, TIFFTAG_INKNAMES, &inknames)) {
	 SensorCall();int inknameslen = strlen(inknames) + 1;
	 const char* cp = inknames;
	 SensorCall();while (ninks > 1) {
	   SensorCall();cp = strchr(cp, '\0');
	   SensorCall();if (cp) {
	     SensorCall();cp++;
	     inknameslen += (strlen(cp) + 1);
	   }
	   SensorCall();ninks--;
         }
	 SensorCall();TIFFSetField(out, TIFFTAG_INKNAMES, inknameslen, inknames);
       }
     }
   }
   {
   SensorCall();unsigned short pg0, pg1;
   SensorCall();if (TIFFGetField(in, TIFFTAG_PAGENUMBER, &pg0, &pg1)) {
     SensorCall();if (pageNum < 0) /* only one input file */
	{/*403*/SensorCall();TIFFSetField(out, TIFFTAG_PAGENUMBER, pg0, pg1);/*404*/}
     else 
	{/*405*/SensorCall();TIFFSetField(out, TIFFTAG_PAGENUMBER, pageNum++, 0);/*406*/}
     }
   }

  for (p = tags; p < &tags[NTAGS]; p++)
		CopyTag(p->tag, p->count, p->type);

  /* Update these since they are overwritten from input res by loop above */
  SensorCall();TIFFSetField(out, TIFFTAG_XRESOLUTION, (float)hres);
  TIFFSetField(out, TIFFTAG_YRESOLUTION, (float)vres);

  /* Compute the tile or strip dimensions and write to disk */
  SensorCall();if (outtiled)
    {
    SensorCall();if (config == PLANARCONFIG_CONTIG)
      {/*407*/SensorCall();writeBufferToContigTiles (out, sect_buff, length, width, spp, dump);/*408*/}
    else
      {/*409*/SensorCall();writeBufferToSeparateTiles (out, sect_buff, length, width, spp, dump);/*410*/}
    }
  else
    {
    SensorCall();if (config == PLANARCONFIG_CONTIG)
      {/*411*/SensorCall();writeBufferToContigStrips (out, sect_buff, length);/*412*/}
    else
      {/*413*/SensorCall();writeBufferToSeparateStrips(out, sect_buff, length, width, spp, dump);/*414*/}
    }

  SensorCall();if (!TIFFWriteDirectory(out))
    {
    SensorCall();TIFFClose(out);
    {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end writeSingleSection */


/* Create a buffer to write one section at a time */
static int
createImageSection(uint32 sectsize, unsigned char **sect_buff_ptr)
  {
  SensorCall();unsigned  char *sect_buff = NULL;
  unsigned  char *new_buff  = NULL;
  static    uint32  prev_sectsize = 0;
  
  sect_buff = *sect_buff_ptr;

  SensorCall();if (!sect_buff)
    {
    SensorCall();sect_buff = (unsigned char *)_TIFFmalloc(sectsize);
    *sect_buff_ptr = sect_buff;
    _TIFFmemset(sect_buff, 0, sectsize);
    }
  else
    {
    SensorCall();if (prev_sectsize < sectsize)
      {
      SensorCall();new_buff = _TIFFrealloc(sect_buff, sectsize);
      SensorCall();if (!new_buff)
        {
	SensorCall();free (sect_buff);
        sect_buff = (unsigned char *)_TIFFmalloc(sectsize);
        }
      else
        {/*363*/SensorCall();sect_buff = new_buff;/*364*/}

      SensorCall();_TIFFmemset(sect_buff, 0, sectsize);
      }
    }

  SensorCall();if (!sect_buff)
    {
    SensorCall();TIFFError("createImageSection", "Unable to allocate/reallocate section buffer");
    {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }
  SensorCall();prev_sectsize = sectsize;
  *sect_buff_ptr = sect_buff;

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }  /* end createImageSection */


/* Process selections defined by regions, zones, margins, or fixed sized areas */
static int
processCropSelections(struct image_data *image, struct crop_mask *crop, 
                      unsigned char **read_buff_ptr, struct buffinfo seg_buffs[])
  {
  SensorCall();int       i;
  uint32    width, length, total_width, total_length;
  tsize_t   cropsize;
  unsigned  char *crop_buff = NULL;
  unsigned  char *read_buff = NULL;
  unsigned  char *next_buff = NULL;
  tsize_t   prev_cropsize = 0;

  read_buff = *read_buff_ptr;

  SensorCall();if (crop->img_mode == COMPOSITE_IMAGES)
    {
    SensorCall();cropsize = crop->bufftotal;
    crop_buff = seg_buffs[0].buffer; 
    SensorCall();if (!crop_buff)
      {/*343*/SensorCall();crop_buff = (unsigned char *)_TIFFmalloc(cropsize);/*344*/}
    else
      {
      SensorCall();prev_cropsize = seg_buffs[0].size;
      SensorCall();if (prev_cropsize < cropsize)
        {
        SensorCall();next_buff = _TIFFrealloc(crop_buff, cropsize);
        SensorCall();if (! next_buff)
          {
          SensorCall();_TIFFfree (crop_buff);
          crop_buff = (unsigned char *)_TIFFmalloc(cropsize);
          }
        else
          {/*345*/SensorCall();crop_buff = next_buff;/*346*/}
        }
      }

    SensorCall();if (!crop_buff)
      {
      SensorCall();TIFFError("processCropSelections", "Unable to allocate/reallocate crop buffer");
      {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
      }
 
    SensorCall();_TIFFmemset(crop_buff, 0, cropsize);
    seg_buffs[0].buffer = crop_buff;
    seg_buffs[0].size = cropsize;

    /* Checks for matching width or length as required */
    SensorCall();if (extractCompositeRegions(image, crop, read_buff, crop_buff) != 0)
      {/*347*/{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}/*348*/}

    SensorCall();if (crop->crop_mode & CROP_INVERT)
      {
      SensorCall();switch (crop->photometric)
        {
        /* Just change the interpretation */
        case PHOTOMETRIC_MINISWHITE:
        case PHOTOMETRIC_MINISBLACK:
	     SensorCall();image->photometric = crop->photometric;
	     SensorCall();break;
        case INVERT_DATA_ONLY:
        case INVERT_DATA_AND_TAG:
             SensorCall();if (invertImage(image->photometric, image->spp, image->bps, 
                             crop->combined_width, crop->combined_length, crop_buff))
               {
               SensorCall();TIFFError("processCropSelections", 
                         "Failed to invert colorspace for composite regions");
               {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
               }
             SensorCall();if (crop->photometric == INVERT_DATA_AND_TAG)
               {
               SensorCall();switch (image->photometric)
                 {
                 case PHOTOMETRIC_MINISWHITE:
 	              SensorCall();image->photometric = PHOTOMETRIC_MINISBLACK;
	              SensorCall();break;
                 case PHOTOMETRIC_MINISBLACK:
 	              SensorCall();image->photometric = PHOTOMETRIC_MINISWHITE;
	              SensorCall();break;
                 default:
	              SensorCall();break;
	         }
	       }
             SensorCall();break;
        default: SensorCall();break;
        }
      }

    /* Mirror and Rotate will not work with multiple regions unless they are the same width */
    SensorCall();if (crop->crop_mode & CROP_MIRROR)
      {
      SensorCall();if (mirrorImage(image->spp, image->bps, crop->mirror, 
                      crop->combined_width, crop->combined_length, crop_buff))
        {
        SensorCall();TIFFError("processCropSelections", "Failed to mirror composite regions %s", 
	         (crop->rotation == MIRROR_HORIZ) ? "horizontally" : "vertically");
        {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
        }
      }

    SensorCall();if (crop->crop_mode & CROP_ROTATE) /* rotate should be last as it can reallocate the buffer */
      {
      SensorCall();if (rotateImage(crop->rotation, image, &crop->combined_width, 
                      &crop->combined_length, &crop_buff))
        {
        SensorCall();TIFFError("processCropSelections", 
                  "Failed to rotate composite regions by %d degrees", crop->rotation);
        {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
        }
      SensorCall();seg_buffs[0].buffer = crop_buff;
      seg_buffs[0].size = (((crop->combined_width * image->bps + 7 ) / 8)
                            * image->spp) * crop->combined_length; 
      }
    }
  else  /* Separated Images */
    {
    SensorCall();total_width = total_length = 0;
    SensorCall();for (i = 0; i < crop->selections; i++)
      {
      SensorCall();cropsize = crop->bufftotal;
      crop_buff = seg_buffs[i].buffer; 
      SensorCall();if (!crop_buff)
        {/*349*/SensorCall();crop_buff = (unsigned char *)_TIFFmalloc(cropsize);/*350*/}
      else
        {
        SensorCall();prev_cropsize = seg_buffs[0].size;
        SensorCall();if (prev_cropsize < cropsize)
          {
          SensorCall();next_buff = _TIFFrealloc(crop_buff, cropsize);
          SensorCall();if (! next_buff)
            {
            SensorCall();_TIFFfree (crop_buff);
            crop_buff = (unsigned char *)_TIFFmalloc(cropsize);
            }
          else
            {/*351*/SensorCall();crop_buff = next_buff;/*352*/}
          }
        }

      SensorCall();if (!crop_buff)
        {
        SensorCall();TIFFError("processCropSelections", "Unable to allocate/reallocate crop buffer");
        {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
        }
 
      SensorCall();_TIFFmemset(crop_buff, 0, cropsize);
      seg_buffs[i].buffer = crop_buff;
      seg_buffs[i].size = cropsize;

      SensorCall();if (extractSeparateRegion(image, crop, read_buff, crop_buff, i))
        {
	SensorCall();TIFFError("processCropSelections", "Unable to extract cropped region %d from image", i);
        {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
        }
    
      SensorCall();width  = crop->regionlist[i].width;
      length = crop->regionlist[i].length;

      SensorCall();if (crop->crop_mode & CROP_INVERT)
        {
        SensorCall();switch (crop->photometric)
          {
          /* Just change the interpretation */
          case PHOTOMETRIC_MINISWHITE:
          case PHOTOMETRIC_MINISBLACK:
	       SensorCall();image->photometric = crop->photometric;
	       SensorCall();break;
          case INVERT_DATA_ONLY:
          case INVERT_DATA_AND_TAG:
               SensorCall();if (invertImage(image->photometric, image->spp, image->bps, 
                               width, length, crop_buff))
                 {
                 SensorCall();TIFFError("processCropSelections", 
                           "Failed to invert colorspace for region");
                 {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
                 }
               SensorCall();if (crop->photometric == INVERT_DATA_AND_TAG)
                 {
                 SensorCall();switch (image->photometric)
                   {
                   case PHOTOMETRIC_MINISWHITE:
 	                SensorCall();image->photometric = PHOTOMETRIC_MINISBLACK;
	                SensorCall();break;
                   case PHOTOMETRIC_MINISBLACK:
 	                SensorCall();image->photometric = PHOTOMETRIC_MINISWHITE;
	                SensorCall();break;
                   default:
	                SensorCall();break;
	           }
	         }
               SensorCall();break;
          default: SensorCall();break;
          }
        }

      SensorCall();if (crop->crop_mode & CROP_MIRROR)
        {
        SensorCall();if (mirrorImage(image->spp, image->bps, crop->mirror, 
                        width, length, crop_buff))
          {
          SensorCall();TIFFError("processCropSelections", "Failed to mirror crop region %s", 
	           (crop->rotation == MIRROR_HORIZ) ? "horizontally" : "vertically");
          {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
          }
        }

      SensorCall();if (crop->crop_mode & CROP_ROTATE) /* rotate should be last as it can reallocate the buffer */
        {
	SensorCall();if (rotateImage(crop->rotation, image, &crop->regionlist[i].width, 
			&crop->regionlist[i].length, &crop_buff))
          {
          SensorCall();TIFFError("processCropSelections", 
                    "Failed to rotate crop region by %d degrees", crop->rotation);
          {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
          }
        SensorCall();total_width  += crop->regionlist[i].width;
        total_length += crop->regionlist[i].length;
        crop->combined_width = total_width;
        crop->combined_length = total_length;
        seg_buffs[i].buffer = crop_buff;
        seg_buffs[i].size = (((crop->regionlist[i].width * image->bps + 7 ) / 8)
                               * image->spp) * crop->regionlist[i].length; 
        }
      }
    }
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end processCropSelections */

/* Copy the crop section of the data from the current image into a buffer
 * and adjust the IFD values to reflect the new size. If no cropping is
 * required, use the origial read buffer as the crop buffer.
 *
 * There is quite a bit of redundancy between this routine and the more
 * specialized processCropSelections, but this provides
 * the most optimized path when no Zones or Regions are required.
 */
static int
createCroppedImage(struct image_data *image, struct crop_mask *crop, 
                   unsigned char **read_buff_ptr, unsigned char **crop_buff_ptr)
  {
  SensorCall();tsize_t   cropsize;
  unsigned  char *read_buff = NULL;
  unsigned  char *crop_buff = NULL;
  unsigned  char *new_buff  = NULL;
  static    tsize_t  prev_cropsize = 0;

  read_buff = *read_buff_ptr;

  /* process full image, no crop buffer needed */
  crop_buff = read_buff;
  *crop_buff_ptr = read_buff;
  crop->combined_width = image->width;
  crop->combined_length = image->length;

  cropsize = crop->bufftotal;
  crop_buff = *crop_buff_ptr;
  SensorCall();if (!crop_buff)
    {
    SensorCall();crop_buff = (unsigned char *)_TIFFmalloc(cropsize);
    *crop_buff_ptr = crop_buff;
    _TIFFmemset(crop_buff, 0, cropsize);
    prev_cropsize = cropsize;
    }
  else
    {
    SensorCall();if (prev_cropsize < cropsize)
      {
      SensorCall();new_buff = _TIFFrealloc(crop_buff, cropsize);
      SensorCall();if (!new_buff)
        {
	SensorCall();free (crop_buff);
        crop_buff = (unsigned char *)_TIFFmalloc(cropsize);
        }
      else
        {/*415*/SensorCall();crop_buff = new_buff;/*416*/}
      SensorCall();_TIFFmemset(crop_buff, 0, cropsize);
      }
    }

  SensorCall();if (!crop_buff)
    {
    SensorCall();TIFFError("createCroppedImage", "Unable to allocate/reallocate crop buffer");
    {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }
  SensorCall();*crop_buff_ptr = crop_buff;

  SensorCall();if (crop->crop_mode & CROP_INVERT)
    {
    SensorCall();switch (crop->photometric)
      {
      /* Just change the interpretation */
      case PHOTOMETRIC_MINISWHITE:
      case PHOTOMETRIC_MINISBLACK:
	   SensorCall();image->photometric = crop->photometric;
	   SensorCall();break;
      case INVERT_DATA_ONLY:
      case INVERT_DATA_AND_TAG:
           SensorCall();if (invertImage(image->photometric, image->spp, image->bps, 
                           crop->combined_width, crop->combined_length, crop_buff))
             {
             SensorCall();TIFFError("createCroppedImage", 
                       "Failed to invert colorspace for image or cropped selection");
             {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
             }
           SensorCall();if (crop->photometric == INVERT_DATA_AND_TAG)
             {
             SensorCall();switch (image->photometric)
               {
               case PHOTOMETRIC_MINISWHITE:
 	            SensorCall();image->photometric = PHOTOMETRIC_MINISBLACK;
	            SensorCall();break;
               case PHOTOMETRIC_MINISBLACK:
 	            SensorCall();image->photometric = PHOTOMETRIC_MINISWHITE;
	            SensorCall();break;
               default:
	            SensorCall();break;
	       }
	     }
           SensorCall();break;
      default: SensorCall();break;
      }
    }

  SensorCall();if (crop->crop_mode & CROP_MIRROR)
    {
    SensorCall();if (mirrorImage(image->spp, image->bps, crop->mirror, 
                    crop->combined_width, crop->combined_length, crop_buff))
      {
      SensorCall();TIFFError("createCroppedImage", "Failed to mirror image or cropped selection %s", 
	       (crop->rotation == MIRROR_HORIZ) ? "horizontally" : "vertically");
      {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
      }
    }

  SensorCall();if (crop->crop_mode & CROP_ROTATE) /* rotate should be last as it can reallocate the buffer */
    {
    SensorCall();if (rotateImage(crop->rotation, image, &crop->combined_width, 
                    &crop->combined_length, crop_buff_ptr))
      {
      SensorCall();TIFFError("createCroppedImage", 
                "Failed to rotate image or cropped selection by %d degrees", crop->rotation);
      {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
      }
    }

  SensorCall();if (crop_buff == read_buff) /* we used the read buffer for the crop buffer */
    *read_buff_ptr = NULL;    /* so we don't try to free it later */

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end createCroppedImage */


/* Code in this function is heavily indebted to code in tiffcp
 * with modifications by Richard Nolde to handle orientation correctly.
 * It will have to be updated significantly if support is added to
 * extract one or more samples from original image since the 
 * original code assumes we are always copying all samples.
 * Use of global variables for config, compression and others
 * should be replaced by addition to the crop_mask struct (which
 * will be renamed to proc_opts indicating that is controlls
 * user supplied processing options, not just cropping) and 
 * then passed in as an argument.
 */
static int  
writeCroppedImage(TIFF *in, TIFF *out, struct image_data *image, 
                  struct dump_opts *dump, uint32 width, uint32 length, 
                  unsigned char *crop_buff, int pagenum, int total_pages)
  {
  SensorCall();uint16 bps, spp;
  uint16 input_compression, input_photometric;
  uint16 input_planar;
  struct cpTag* p;

  input_compression = image->compression;
  input_photometric = image->photometric;
  spp = image->spp;
  bps = image->bps;

  TIFFSetField(out, TIFFTAG_IMAGEWIDTH, width);
  TIFFSetField(out, TIFFTAG_IMAGELENGTH, length);
  TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, bps);
  TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, spp);

#ifdef DEBUG2
  TIFFError("writeCroppedImage", "Input compression: %s",
	    (input_compression == COMPRESSION_OJPEG) ? "Old Jpeg" :
	    ((input_compression == COMPRESSION_JPEG) ?  "New Jpeg" : "Non Jpeg"));
#endif

  SensorCall();if (compression != (uint16)-1)
    {/*417*/SensorCall();TIFFSetField(out, TIFFTAG_COMPRESSION, compression);/*418*/}
  else
    {
    if (input_compression == COMPRESSION_OJPEG)
      {
      compression = COMPRESSION_JPEG;
      jpegcolormode = JPEGCOLORMODE_RAW;
      TIFFSetField(out, TIFFTAG_COMPRESSION, COMPRESSION_JPEG);
      }
    else
      CopyField(TIFFTAG_COMPRESSION, compression);
    }

  SensorCall();if (compression == COMPRESSION_JPEG)
    {
    SensorCall();if ((input_photometric == PHOTOMETRIC_PALETTE) ||  /* color map indexed */
        (input_photometric == PHOTOMETRIC_MASK))       /* $holdout mask */
      {
      SensorCall();TIFFError ("writeCroppedImage",
                 "JPEG compression cannot be used with %s image data",
      	        (input_photometric == PHOTOMETRIC_PALETTE) ?
                 "palette" : "mask");
      {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
      }
    SensorCall();if ((input_photometric == PHOTOMETRIC_RGB) &&
	(jpegcolormode == JPEGCOLORMODE_RGB))
      {/*419*/SensorCall();TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_YCBCR);/*420*/}
    else
	{/*421*/SensorCall();TIFFSetField(out, TIFFTAG_PHOTOMETRIC, input_photometric);/*422*/}
    }
  else
    {
    SensorCall();if (compression == COMPRESSION_SGILOG || compression == COMPRESSION_SGILOG24)
      {
      SensorCall();TIFFSetField(out, TIFFTAG_PHOTOMETRIC, spp == 1 ?
			PHOTOMETRIC_LOGL : PHOTOMETRIC_LOGLUV);
      }
    else
      {
      SensorCall();if (input_compression == COMPRESSION_SGILOG ||
          input_compression == COMPRESSION_SGILOG24)
        {
        SensorCall();TIFFSetField(out, TIFFTAG_PHOTOMETRIC, spp == 1 ?
			  PHOTOMETRIC_LOGL : PHOTOMETRIC_LOGLUV);
        }
      else
        {/*423*/SensorCall();TIFFSetField(out, TIFFTAG_PHOTOMETRIC, image->photometric);/*424*/}
      }
    }

  SensorCall();if (((input_photometric == PHOTOMETRIC_LOGL) ||
       (input_photometric ==  PHOTOMETRIC_LOGLUV)) &&
      ((compression != COMPRESSION_SGILOG) && 
       (compression != COMPRESSION_SGILOG24)))
    {
    SensorCall();TIFFError("writeCroppedImage",
              "LogL and LogLuv source data require SGI_LOG or SGI_LOG24 compression");
    {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }

  if (fillorder != 0)
    TIFFSetField(out, TIFFTAG_FILLORDER, fillorder);
  else
    CopyTag(TIFFTAG_FILLORDER, 1, TIFF_SHORT);

  /* The loadimage function reads input orientation and sets
   * image->orientation. The correct_image_orientation function
   * applies the required rotation and mirror operations to 
   * present the data in TOPLEFT orientation and updates 
   * image->orientation if any transforms are performed, 
   * as per EXIF standard. 
   */
  SensorCall();TIFFSetField(out, TIFFTAG_ORIENTATION, image->orientation);
	
  /*
   * Choose tiles/strip for the output image according to
   * the command line arguments (-tiles, -strips) and the
   * structure of the input image.
   */
  SensorCall();if (outtiled == -1)
    {/*427*/SensorCall();outtiled = TIFFIsTiled(in);/*428*/}
  SensorCall();if (outtiled) {
    /*
     * Setup output file's tile width&height.  If either
     * is not specified, use either the value from the
     * input image or, if nothing is defined, use the
     * library default.
     */
    SensorCall();if (tilewidth == (uint32) 0)
      {/*429*/SensorCall();TIFFGetField(in, TIFFTAG_TILEWIDTH, &tilewidth);/*430*/}
    SensorCall();if (tilelength == (uint32) 0)
      {/*431*/SensorCall();TIFFGetField(in, TIFFTAG_TILELENGTH, &tilelength);/*432*/}

    SensorCall();if (tilewidth == 0 || tilelength == 0)
      {/*433*/SensorCall();TIFFDefaultTileSize(out, &tilewidth, &tilelength);/*434*/}
    SensorCall();TIFFSetField(out, TIFFTAG_TILEWIDTH, tilewidth);
    TIFFSetField(out, TIFFTAG_TILELENGTH, tilelength);
    } else {
       /*
	* RowsPerStrip is left unspecified: use either the
	* value from the input image or, if nothing is defined,
	* use the library default.
	*/
	SensorCall();if (rowsperstrip == (uint32) 0)
          {
	  SensorCall();if (!TIFFGetField(in, TIFFTAG_ROWSPERSTRIP, &rowsperstrip))
	    {/*435*/SensorCall();rowsperstrip = TIFFDefaultStripSize(out, rowsperstrip);/*436*/}
          SensorCall();if (compression != COMPRESSION_JPEG)
            {
  	    SensorCall();if (rowsperstrip > length)
	      {/*437*/SensorCall();rowsperstrip = length;/*438*/}
	    }
	  }
	else 
          {/*439*/SensorCall();if (rowsperstrip == (uint32) -1)
	    {/*441*/SensorCall();rowsperstrip = length;/*442*/}/*440*/}
	SensorCall();TIFFSetField(out, TIFFTAG_ROWSPERSTRIP, rowsperstrip);
	}

  SensorCall();TIFFGetFieldDefaulted(in, TIFFTAG_PLANARCONFIG, &input_planar);
  if (config != (uint16) -1)
    TIFFSetField(out, TIFFTAG_PLANARCONFIG, config);
  else
    CopyField(TIFFTAG_PLANARCONFIG, config);
  if (spp <= 4)
    CopyTag(TIFFTAG_TRANSFERFUNCTION, 4, TIFF_SHORT);
  CopyTag(TIFFTAG_COLORMAP, 4, TIFF_SHORT);

/* SMinSampleValue & SMaxSampleValue */
  SensorCall();switch (compression) {
    case COMPRESSION_JPEG:
         SensorCall();if (((bps % 8) == 0) || ((bps % 12) == 0))
	   {
           SensorCall();TIFFSetField(out, TIFFTAG_JPEGQUALITY, quality);
	   TIFFSetField(out, TIFFTAG_JPEGCOLORMODE, JPEGCOLORMODE_RGB);
           }
         else
           {
	   SensorCall();TIFFError("writeCroppedImage",
                     "JPEG compression requires 8 or 12 bits per sample");
           {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
           }
	 SensorCall();break;
   case COMPRESSION_LZW:
   case COMPRESSION_ADOBE_DEFLATE:
   case COMPRESSION_DEFLATE:
	if (predictor != (uint16)-1)
          TIFFSetField(out, TIFFTAG_PREDICTOR, predictor);
	else
	  CopyField(TIFFTAG_PREDICTOR, predictor);
	SensorCall();break;
   case COMPRESSION_CCITTFAX3:
   case COMPRESSION_CCITTFAX4:
        SensorCall();if (bps != 1)
          {
	  SensorCall();TIFFError("writeCroppedImage",
            "Group 3/4 compression is not usable with bps > 1");
          {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
	  }
	if (compression == COMPRESSION_CCITTFAX3) {
          if (g3opts != (uint32) -1)
	    TIFFSetField(out, TIFFTAG_GROUP3OPTIONS, g3opts);
	  else
	    CopyField(TIFFTAG_GROUP3OPTIONS, g3opts);
	} else
	    CopyTag(TIFFTAG_GROUP4OPTIONS, 1, TIFF_LONG);
	    CopyTag(TIFFTAG_BADFAXLINES, 1, TIFF_LONG);
	    CopyTag(TIFFTAG_CLEANFAXDATA, 1, TIFF_LONG);
	    CopyTag(TIFFTAG_CONSECUTIVEBADFAXLINES, 1, TIFF_LONG);
	    CopyTag(TIFFTAG_FAXRECVPARAMS, 1, TIFF_LONG);
	    CopyTag(TIFFTAG_FAXRECVTIME, 1, TIFF_LONG);
	    CopyTag(TIFFTAG_FAXSUBADDRESS, 1, TIFF_ASCII);
	 SensorCall();break;
    case COMPRESSION_NONE:
         SensorCall();break;
    default: SensorCall();break;
   }
   { SensorCall();uint32 len32;
     void** data;
     SensorCall();if (TIFFGetField(in, TIFFTAG_ICCPROFILE, &len32, &data))
       {/*449*/SensorCall();TIFFSetField(out, TIFFTAG_ICCPROFILE, len32, data);/*450*/}
   }
   { SensorCall();uint16 ninks;
     const char* inknames;
     SensorCall();if (TIFFGetField(in, TIFFTAG_NUMBEROFINKS, &ninks)) {
       SensorCall();TIFFSetField(out, TIFFTAG_NUMBEROFINKS, ninks);
       SensorCall();if (TIFFGetField(in, TIFFTAG_INKNAMES, &inknames)) {
	 SensorCall();int inknameslen = strlen(inknames) + 1;
	 const char* cp = inknames;
	 SensorCall();while (ninks > 1) {
	   SensorCall();cp = strchr(cp, '\0');
	   SensorCall();if (cp) {
	     SensorCall();cp++;
	     inknameslen += (strlen(cp) + 1);
	   }
	   SensorCall();ninks--;
         }
	 SensorCall();TIFFSetField(out, TIFFTAG_INKNAMES, inknameslen, inknames);
       }
     }
   }
   {
   SensorCall();unsigned short pg0, pg1;
   SensorCall();if (TIFFGetField(in, TIFFTAG_PAGENUMBER, &pg0, &pg1)) {
     SensorCall();TIFFSetField(out, TIFFTAG_PAGENUMBER, pagenum, total_pages);
     }
   }

  for (p = tags; p < &tags[NTAGS]; p++)
		CopyTag(p->tag, p->count, p->type);

  /* Compute the tile or strip dimensions and write to disk */
  SensorCall();if (outtiled)
    {
    SensorCall();if (config == PLANARCONFIG_CONTIG)
      {
      SensorCall();if (writeBufferToContigTiles (out, crop_buff, length, width, spp, dump))
        {/*451*/SensorCall();TIFFError("","Unable to write contiguous tile data for page %d", pagenum);/*452*/}
      }
    else
      {
      SensorCall();if (writeBufferToSeparateTiles (out, crop_buff, length, width, spp, dump))
        {/*453*/SensorCall();TIFFError("","Unable to write separate tile data for page %d", pagenum);/*454*/}
      }
    }
  else
    {
    SensorCall();if (config == PLANARCONFIG_CONTIG)
      {
      SensorCall();if (writeBufferToContigStrips (out, crop_buff, length))
        {/*455*/SensorCall();TIFFError("","Unable to write contiguous strip data for page %d", pagenum);/*456*/}
      }
    else
      {
      SensorCall();if (writeBufferToSeparateStrips(out, crop_buff, length, width, spp, dump))
        {/*457*/SensorCall();TIFFError("","Unable to write separate strip data for page %d", pagenum);/*458*/}
      }
    }

  SensorCall();if (!TIFFWriteDirectory(out))
    {
    SensorCall();TIFFError("","Failed to write IFD for page number %d", pagenum);
    TIFFClose(out);
    {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end writeCroppedImage */

static int
rotateContigSamples8bits(uint16 rotation, uint16 spp, uint16 bps, uint32 width, 
                         uint32 length,   uint32 col, uint8 *src, uint8 *dst)
  {
  SensorCall();int      ready_bits = 0;
  uint32   src_byte = 0, src_bit = 0;
  uint32   row, rowsize = 0, bit_offset = 0;
  uint8    matchbits = 0, maskbits = 0;
  uint8    buff1 = 0, buff2 = 0;
  uint8   *next;
  tsample_t sample;

  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("rotateContigSamples8bits","Invalid src or destination buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();rowsize = ((bps * spp * width) + 7) / 8;
  ready_bits = 0;
  maskbits =  (uint8)-1 >> ( 8 - bps);
  buff1 = buff2 = 0;

  SensorCall();for (row = 0; row < length ; row++)
    {
    SensorCall();bit_offset = col * bps * spp;
    SensorCall();for (sample = 0; sample < spp; sample++)
      {
      SensorCall();if (sample == 0)
        {
        SensorCall();src_byte = bit_offset / 8;
        src_bit  = bit_offset % 8;
        }
      else
        {
        SensorCall();src_byte = (bit_offset + (sample * bps)) / 8;
        src_bit  = (bit_offset + (sample * bps)) % 8;
        }

      SensorCall();switch (rotation)
	{
        case  90: SensorCall();next = src + src_byte - (row * rowsize);
                  SensorCall();break;
        case 270: SensorCall();next = src + src_byte + (row * rowsize);
	          SensorCall();break;
	default:  SensorCall();TIFFError("rotateContigSamples8bits", "Invalid rotation %d", rotation);
                  {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
        }
      SensorCall();matchbits = maskbits << (8 - src_bit - bps); 
      buff1 = ((*next) & matchbits) << (src_bit);

       /* If we have a full buffer's worth, write it out */
      SensorCall();if (ready_bits >= 8)
        {
        SensorCall();*dst++ = buff2;
        buff2 = buff1;
        ready_bits -= 8;
        }
      else
        {
        SensorCall();buff2 = (buff2 | (buff1 >> ready_bits));
        }
      SensorCall();ready_bits += bps;
      }
    }

  SensorCall();if (ready_bits > 0)
    {
    SensorCall();buff1 = (buff2 & ((unsigned int)255 << (8 - ready_bits)));
    *dst++ = buff1;
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }  /* end rotateContigSamples8bits */


static int
rotateContigSamples16bits(uint16 rotation, uint16 spp, uint16 bps, uint32 width, 
                         uint32 length,   uint32 col, uint8 *src, uint8 *dst)
  {
  SensorCall();int      ready_bits = 0;
  uint32   row, rowsize, bit_offset;
  uint32   src_byte = 0, src_bit = 0;
  uint16   matchbits = 0, maskbits = 0;
  uint16   buff1 = 0, buff2 = 0;
  uint8    bytebuff = 0;
  uint8   *next;
  tsample_t sample;

  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("rotateContigSamples16bits","Invalid src or destination buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();rowsize = ((bps * spp * width) + 7) / 8;
  ready_bits = 0;
  maskbits =  (uint16)-1 >> (16 - bps);
  buff1 = buff2 = 0;
  SensorCall();for (row = 0; row < length; row++)
    {
    SensorCall();bit_offset = col * bps * spp;
    SensorCall();for (sample = 0; sample < spp; sample++)
      {
      SensorCall();if (sample == 0)
        {
        SensorCall();src_byte = bit_offset / 8;
        src_bit  = bit_offset % 8;
        }
      else
        {
        SensorCall();src_byte = (bit_offset + (sample * bps)) / 8;
        src_bit  = (bit_offset + (sample * bps)) % 8;
        }

      SensorCall();switch (rotation)
	{
        case  90: SensorCall();next = src + src_byte - (row * rowsize);
                  SensorCall();break;
        case 270: SensorCall();next = src + src_byte + (row * rowsize);
	          SensorCall();break;
	default:  SensorCall();TIFFError("rotateContigSamples8bits", "Invalid rotation %d", rotation);
                  {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
        }
      SensorCall();matchbits = maskbits << (16 - src_bit - bps); 
      SensorCall();if (little_endian)
        {/*459*/SensorCall();buff1 = (next[0] << 8) | next[1];/*460*/}
      else
        {/*461*/SensorCall();buff1 = (next[1] << 8) | next[0];/*462*/}

      SensorCall();buff1 = (buff1 & matchbits) << (src_bit);

      /* If we have a full buffer's worth, write it out */
      SensorCall();if (ready_bits >= 8)
        {
        SensorCall();bytebuff = (buff2 >> 8);
        *dst++ = bytebuff;
        ready_bits -= 8;
        /* shift in new bits */
        buff2 = ((buff2 << 8) | (buff1 >> ready_bits));
        }
      else
        { /* add another bps bits to the buffer */
        SensorCall();bytebuff = 0;
        buff2 = (buff2 | (buff1 >> ready_bits));
        }
      SensorCall();ready_bits += bps;
      }
    }

  SensorCall();if (ready_bits > 0)
    {
    SensorCall();bytebuff = (buff2 >> 8);
    *dst++ = bytebuff;
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }  /* end rotateContigSamples16bits */

static int
rotateContigSamples24bits(uint16 rotation, uint16 spp, uint16 bps, uint32 width, 
                          uint32 length,   uint32 col, uint8 *src, uint8 *dst)
  {
  SensorCall();int      ready_bits = 0;
  uint32   row, rowsize, bit_offset;
  uint32   src_byte = 0, src_bit = 0;
  uint32   matchbits = 0, maskbits = 0;
  uint32   buff1 = 0, buff2 = 0;
  uint8    bytebuff1 = 0, bytebuff2 = 0;
  uint8   *next;
  tsample_t sample;


  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("rotateContigSamples24bits","Invalid src or destination buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();rowsize = ((bps * spp * width) + 7) / 8;
  ready_bits = 0;
  maskbits =  (uint32)-1 >> (32 - bps);
  buff1 = buff2 = 0;
  SensorCall();for (row = 0; row < length; row++)
    {
    SensorCall();bit_offset = col * bps * spp;
    SensorCall();for (sample = 0; sample < spp; sample++)
      {
      SensorCall();if (sample == 0)
        {
        SensorCall();src_byte = bit_offset / 8;
        src_bit  = bit_offset % 8;
        }
      else
        {
        SensorCall();src_byte = (bit_offset + (sample * bps)) / 8;
        src_bit  = (bit_offset + (sample * bps)) % 8;
        }

      SensorCall();switch (rotation)
	{
        case  90: SensorCall();next = src + src_byte - (row * rowsize);
                  SensorCall();break;
        case 270: SensorCall();next = src + src_byte + (row * rowsize);
	          SensorCall();break;
	default:  SensorCall();TIFFError("rotateContigSamples8bits", "Invalid rotation %d", rotation);
                  {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
        }
      SensorCall();matchbits = maskbits << (32 - src_bit - bps); 
      SensorCall();if (little_endian)
	{/*463*/SensorCall();buff1 = (next[0] << 24) | (next[1] << 16) | (next[2] << 8) | next[3];/*464*/}
      else
	{/*465*/SensorCall();buff1 = (next[3] << 24) | (next[2] << 16) | (next[1] << 8) | next[0];/*466*/}
      SensorCall();buff1 = (buff1 & matchbits) << (src_bit);

      /* If we have a full buffer's worth, write it out */
      SensorCall();if (ready_bits >= 16)
        {
        SensorCall();bytebuff1 = (buff2 >> 24);
        *dst++ = bytebuff1;
        bytebuff2 = (buff2 >> 16);
        *dst++ = bytebuff2;
        ready_bits -= 16;

        /* shift in new bits */
        buff2 = ((buff2 << 16) | (buff1 >> ready_bits));
        }
      else
        { /* add another bps bits to the buffer */
        SensorCall();bytebuff1 = bytebuff2 = 0;
        buff2 = (buff2 | (buff1 >> ready_bits));
        }
      SensorCall();ready_bits += bps;
      }
    }

 /* catch any trailing bits at the end of the line */
  SensorCall();while (ready_bits > 0)
    {
    SensorCall();bytebuff1 = (buff2 >> 24);
    *dst++ = bytebuff1;

    buff2 = (buff2 << 8);
    bytebuff2 = bytebuff1;
    ready_bits -= 8;
    }
 
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }  /* end rotateContigSamples24bits */

static int
rotateContigSamples32bits(uint16 rotation, uint16 spp, uint16 bps, uint32 width, 
                          uint32 length,   uint32 col, uint8 *src, uint8 *dst)
  {
  SensorCall();int    ready_bits = 0, shift_width = 0;
  int    bytes_per_sample, bytes_per_pixel;
  uint32 row, rowsize, bit_offset;
  uint32 src_byte, src_bit;
  uint32 longbuff1 = 0, longbuff2 = 0;
  uint64 maskbits = 0, matchbits = 0;
  uint64 buff1 = 0, buff2 = 0, buff3 = 0;
  uint8  bytebuff1 = 0, bytebuff2 = 0, bytebuff3 = 0, bytebuff4 = 0;
  uint8   *next;
  tsample_t sample;


  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("rotateContigSamples24bits","Invalid src or destination buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();bytes_per_sample = (bps + 7) / 8;
  bytes_per_pixel  = ((bps * spp) + 7) / 8;
  SensorCall();if (bytes_per_pixel < (bytes_per_sample + 1))
    {/*467*/SensorCall();shift_width = bytes_per_pixel;/*468*/}
  else
    {/*469*/SensorCall();shift_width = bytes_per_sample + 1;/*470*/}

  SensorCall();rowsize = ((bps * spp * width) + 7) / 8;
  ready_bits = 0;
  maskbits =  (uint64)-1 >> (64 - bps);
  buff1 = buff2 = 0;
  SensorCall();for (row = 0; row < length; row++)
    {
    SensorCall();bit_offset = col * bps * spp;
    SensorCall();for (sample = 0; sample < spp; sample++)
      {
      SensorCall();if (sample == 0)
        {
        SensorCall();src_byte = bit_offset / 8;
        src_bit  = bit_offset % 8;
        }
      else
        {
        SensorCall();src_byte = (bit_offset + (sample * bps)) / 8;
        src_bit  = (bit_offset + (sample * bps)) % 8;
        }

      SensorCall();switch (rotation)
	{
        case  90: SensorCall();next = src + src_byte - (row * rowsize);
                  SensorCall();break;
        case 270: SensorCall();next = src + src_byte + (row * rowsize);
	          SensorCall();break;
	default:  SensorCall();TIFFError("rotateContigSamples8bits", "Invalid rotation %d", rotation);
                  {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
        }
      SensorCall();matchbits = maskbits << (64 - src_bit - bps); 
      SensorCall();if (little_endian)
        {
	SensorCall();longbuff1 = (next[0] << 24) | (next[1] << 16) | (next[2] << 8) | next[3];
        longbuff2 = longbuff1;
        }
      else
        {
	SensorCall();longbuff1 = (next[3] << 24) | (next[2] << 16) | (next[1] << 8) | next[0];
        longbuff2 = longbuff1;
	}

      SensorCall();buff3 = ((uint64)longbuff1 << 32) | longbuff2;
      buff1 = (buff3 & matchbits) << (src_bit);

      SensorCall();if (ready_bits < 32)
        { /* add another bps bits to the buffer */
        SensorCall();bytebuff1 = bytebuff2 = bytebuff3 = bytebuff4 = 0;
        buff2 = (buff2 | (buff1 >> ready_bits));
        }
      else /* If we have a full buffer's worth, write it out */
        {
        SensorCall();bytebuff1 = (buff2 >> 56);
        *dst++ = bytebuff1;
        bytebuff2 = (buff2 >> 48);
        *dst++ = bytebuff2;
        bytebuff3 = (buff2 >> 40);
        *dst++ = bytebuff3;
        bytebuff4 = (buff2 >> 32);
        *dst++ = bytebuff4;
        ready_bits -= 32;
                    
        /* shift in new bits */
        buff2 = ((buff2 << 32) | (buff1 >> ready_bits));
        }
      SensorCall();ready_bits += bps;
      }
    }
  SensorCall();while (ready_bits > 0)
    {
    SensorCall();bytebuff1 = (buff2 >> 56);
    *dst++ = bytebuff1;
    buff2 = (buff2 << 8);
    ready_bits -= 8;
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end rotateContigSamples32bits */


/* Rotate an image by a multiple of 90 degrees clockwise */
static int
rotateImage(uint16 rotation, struct image_data *image, uint32 *img_width, 
            uint32 *img_length, unsigned char **ibuff_ptr)
  {
  SensorCall();int      shift_width;
  uint32   bytes_per_pixel, bytes_per_sample;
  uint32   row, rowsize, src_offset, dst_offset;
  uint32   i, col, width, length;
  uint32   colsize, buffsize, col_offset, pix_offset;
  unsigned char *ibuff;
  unsigned char *src;
  unsigned char *dst;
  uint16   spp, bps;
  float    res_temp;
  unsigned char *rbuff = NULL;

  width  = *img_width;
  length = *img_length;
  spp = image->spp;
  bps = image->bps;

  rowsize = ((bps * spp * width) + 7) / 8;
  colsize = ((bps * spp * length) + 7) / 8;
  SensorCall();if ((colsize * width) > (rowsize * length))
    {/*471*/SensorCall();buffsize = (colsize + 1) * width;/*472*/}
  else
    {/*473*/SensorCall();buffsize = (rowsize + 1) * length;/*474*/}

  SensorCall();bytes_per_sample = (bps + 7) / 8;
  bytes_per_pixel  = ((bps * spp) + 7) / 8;
  SensorCall();if (bytes_per_pixel < (bytes_per_sample + 1))
    {/*475*/SensorCall();shift_width = bytes_per_pixel;/*476*/}
  else
    {/*477*/SensorCall();shift_width = bytes_per_sample + 1;/*478*/}

  SensorCall();switch (rotation)
    {
    case 0:
    case 360: {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
    case 90:
    case 180:
    case 270: SensorCall();break;
    default:  SensorCall();TIFFError("rotateImage", "Invalid rotation angle %d", rotation);
              {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();if (!(rbuff = (unsigned char *)_TIFFmalloc(buffsize)))
    {
    SensorCall();TIFFError("rotateImage", "Unable to allocate rotation buffer of %1u bytes", buffsize);
    {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }
  SensorCall();_TIFFmemset(rbuff, '\0', buffsize);

  ibuff = *ibuff_ptr;
  SensorCall();switch (rotation)
    {
    case 180: SensorCall();if ((bps % 8) == 0) /* byte alligned data */
                { 
                SensorCall();src = ibuff;
                pix_offset = (spp * bps) / 8;
                SensorCall();for (row = 0; row < length; row++)
                   {
		   SensorCall();dst_offset = (length - row - 1) * rowsize;
                   SensorCall();for (col = 0; col < width; col++)
                     { 
		     SensorCall();col_offset = (width - col - 1) * pix_offset;
                     dst = rbuff + dst_offset + col_offset;

		     SensorCall();for (i = 0; i  < bytes_per_pixel; i++)
		       {/*479*/SensorCall();*dst++ = *src++;/*480*/}
                     }
                   }
                }
	      else
                { /* non 8 bit per sample data */ 
                SensorCall();for (row = 0; row < length; row++)
                  {
		  SensorCall();src_offset = row * rowsize;
		  dst_offset = (length - row - 1) * rowsize;
		  src = ibuff + src_offset;
                  dst = rbuff + dst_offset;
                  SensorCall();switch (shift_width)
                    {
                    case 1: SensorCall();if (bps == 1)
			      {
                              SensorCall();if (reverseSamples8bits(spp, bps, width, src, dst))
                                {
		                SensorCall();_TIFFfree(rbuff);
                                {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
                                }
                              SensorCall();break;
                              }
                            SensorCall();if (reverseSamples16bits(spp, bps, width, src, dst))
                              {
		              SensorCall();_TIFFfree(rbuff);
                              {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
                              }
                             SensorCall();break;
                    case 2: SensorCall();if (reverseSamples24bits(spp, bps, width, src, dst))
                              {
		              SensorCall();_TIFFfree(rbuff);
                              {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
                              }
                             SensorCall();break;
                    case 3: 
                    case 4: 
                    case 5: SensorCall();if (reverseSamples32bits(spp, bps, width, src, dst))
                              {
		              SensorCall();_TIFFfree(rbuff);
                              {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
                              }
                             SensorCall();break;
                    default: SensorCall();TIFFError("rotateImage","Unsupported bit depth %d", bps);
		             _TIFFfree(rbuff);
                             {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}      
                    }
		  }
		}
              SensorCall();_TIFFfree(ibuff);
              *(ibuff_ptr) = rbuff;
              SensorCall();break;

    case 90:  SensorCall();if ((bps % 8) == 0) /* byte aligned data */
                {
                SensorCall();for (col = 0; col < width; col++)
                  {
		  SensorCall();src_offset = ((length - 1) * rowsize) + (col * bytes_per_pixel);
                  dst_offset = col * colsize;
		  src = ibuff + src_offset;
		  dst = rbuff + dst_offset;
                  SensorCall();for (row = length; row > 0; row--)
                    {
                    SensorCall();for (i = 0; i < bytes_per_pixel; i++)
                      {/*481*/SensorCall();*dst++ = *(src + i);/*482*/}
		    SensorCall();src -= rowsize;
                    }
		  }
		}
              else
                { /* non 8 bit per sample data */ 
                SensorCall();for (col = 0; col < width; col++)
                  {
		  SensorCall();src_offset = (length - 1) * rowsize;
                  dst_offset = col * colsize;
		  src = ibuff + src_offset;
		  dst = rbuff + dst_offset;
                  SensorCall();switch (shift_width)
                    {
                    case 1: SensorCall();if (bps == 1)
			      {
                              SensorCall();if (rotateContigSamples8bits(rotation, spp, bps, width, 
				   	                 length, col, src, dst))
                                {
		                SensorCall();_TIFFfree(rbuff);
                                {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
                                }
                              SensorCall();break;
                              }
                            SensorCall();if (rotateContigSamples16bits(rotation, spp, bps, width, 
				   	                 length, col, src, dst))
                              {
	                      SensorCall();_TIFFfree(rbuff);
                              {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
		              }
		            SensorCall();break;
                    case 2: SensorCall();if (rotateContigSamples24bits(rotation, spp, bps, width, 
					                  length, col, src, dst))
                              {
		              SensorCall();_TIFFfree(rbuff);
                              {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
                              }
                             SensorCall();break;
                    case 3: 
                    case 4: 
                    case 5: SensorCall();if (rotateContigSamples32bits(rotation, spp, bps, width, 
					                  length, col, src, dst))
                              {
		              SensorCall();_TIFFfree(rbuff);
                              {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
                              }
                             SensorCall();break;
                    default: SensorCall();TIFFError("rotateImage","Unsupported bit depth %d", bps);
		             _TIFFfree(rbuff);
                             {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}      
		    }
		  }
		}
              SensorCall();_TIFFfree(ibuff);
              *(ibuff_ptr) = rbuff;

              *img_width = length;
              *img_length = width;
              image->width = length;
              image->length = width;
              res_temp = image->xres;
              image->xres = image->yres;
              image->yres = res_temp;
	      SensorCall();break;

    case 270: SensorCall();if ((bps % 8) == 0) /* byte aligned data */
                {
                SensorCall();for (col = 0; col < width; col++)
                  {
		  SensorCall();src_offset = col * bytes_per_pixel;
                  dst_offset = (width - col - 1) * colsize;
		  src = ibuff + src_offset;
		  dst = rbuff + dst_offset;
                  SensorCall();for (row = length; row > 0; row--)
                    {
                    SensorCall();for (i = 0; i < bytes_per_pixel; i++)
                      {/*483*/SensorCall();*dst++ = *(src + i);/*484*/}
		    SensorCall();src += rowsize;
                    }
		  }
		}
              else
                { /* non 8 bit per sample data */ 
                SensorCall();for (col = 0; col < width; col++)
                  {
		  SensorCall();src_offset = 0;
                  dst_offset = (width - col - 1) * colsize;
		  src = ibuff + src_offset;
		  dst = rbuff + dst_offset;
                  SensorCall();switch (shift_width)
                    {
                    case 1: SensorCall();if (bps == 1)
			      {
                              SensorCall();if (rotateContigSamples8bits(rotation, spp, bps, width, 
				   	                 length, col, src, dst))
                                {
		                SensorCall();_TIFFfree(rbuff);
                                {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
                                }
                              SensorCall();break;
                              }
                            SensorCall();if (rotateContigSamples16bits(rotation, spp, bps, width, 
				   	                 length, col, src, dst))
                              {
	                      SensorCall();_TIFFfree(rbuff);
                              {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
		              }
		            SensorCall();break;
                    case 2: SensorCall();if (rotateContigSamples24bits(rotation, spp, bps, width, 
					                  length, col, src, dst))
                              {
		              SensorCall();_TIFFfree(rbuff);
                              {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
                              }
                             SensorCall();break;
                    case 3: 
                    case 4: 
                    case 5: SensorCall();if (rotateContigSamples32bits(rotation, spp, bps, width, 
					                  length, col, src, dst))
                              {
		              SensorCall();_TIFFfree(rbuff);
                              {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
                              }
                             SensorCall();break;
                    default: SensorCall();TIFFError("rotateImage","Unsupported bit depth %d", bps);
		             _TIFFfree(rbuff);
                             {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}      
		    }
		  }
		}
              SensorCall();_TIFFfree(ibuff);
              *(ibuff_ptr) = rbuff;

              *img_width = length;
              *img_length = width;
              image->width = length;
              image->length = width;
              res_temp = image->xres;
              image->xres = image->yres;
              image->yres = res_temp;
              SensorCall();break;
    default:
              SensorCall();break;
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end rotateImage */

static int
reverseSamples8bits (uint16 spp, uint16 bps, uint32 width, 
                     uint8 *ibuff, uint8 *obuff)
  {
  SensorCall();int      ready_bits = 0;
  uint32   col;
  uint32   src_byte, src_bit;
  uint32   bit_offset = 0;
  uint8    match_bits = 0, mask_bits = 0;
  uint8    buff1 = 0, buff2 = 0;
  unsigned char *src;
  unsigned char *dst;
  tsample_t sample;

  SensorCall();if ((ibuff == NULL) || (obuff == NULL))
    {
    SensorCall();TIFFError("reverseSamples8bits","Invalid image or work buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();ready_bits = 0;
  mask_bits =  (uint8)-1 >> ( 8 - bps);
  dst = obuff;
  SensorCall();for (col = width; col > 0; col--)
    {
    /* Compute src byte(s) and bits within byte(s) */
    SensorCall();bit_offset = (col - 1) * bps * spp;
    SensorCall();for (sample = 0; sample < spp; sample++)
      {
      SensorCall();if (sample == 0)
        {
        SensorCall();src_byte = bit_offset / 8;
        src_bit  = bit_offset % 8;
        }
      else
        {
        SensorCall();src_byte = (bit_offset + (sample * bps)) / 8;
        src_bit  = (bit_offset + (sample * bps)) % 8;
        }

      SensorCall();src = ibuff + src_byte;
      match_bits = mask_bits << (8 - src_bit - bps); 
      buff1 = ((*src) & match_bits) << (src_bit);

      SensorCall();if (ready_bits < 8)
        {/*507*/SensorCall();buff2 = (buff2 | (buff1 >> ready_bits));/*508*/}
      else  /* If we have a full buffer's worth, write it out */
        {
        SensorCall();*dst++ = buff2;
        buff2 = buff1;
        ready_bits -= 8;
        }
      SensorCall();ready_bits += bps;
      }
    }
  SensorCall();if (ready_bits > 0)
    {
    SensorCall();buff1 = (buff2 & ((unsigned int)255 << (8 - ready_bits)));
    *dst++ = buff1;
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end reverseSamples8bits */


static int
reverseSamples16bits (uint16 spp, uint16 bps, uint32 width, 
                      uint8 *ibuff, uint8 *obuff)
  {
  SensorCall();int      ready_bits = 0;
  uint32   col;
  uint32   src_byte = 0, high_bit = 0;
  uint32   bit_offset = 0;
  uint16   match_bits = 0, mask_bits = 0;
  uint16   buff1 = 0, buff2 = 0;
  uint8    bytebuff = 0;
  unsigned char *src;
  unsigned char *dst;
  tsample_t sample;

  SensorCall();if ((ibuff == NULL) || (obuff == NULL))
    {
    SensorCall();TIFFError("reverseSample16bits","Invalid image or work buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();ready_bits = 0;
  mask_bits =  (uint16)-1 >> (16 - bps);
  dst = obuff;
  SensorCall();for (col = width; col > 0; col--)
    {
    /* Compute src byte(s) and bits within byte(s) */
    SensorCall();bit_offset = (col - 1) * bps * spp;
    SensorCall();for (sample = 0; sample < spp; sample++)
      {
      SensorCall();if (sample == 0)
        {
        SensorCall();src_byte = bit_offset / 8;
        high_bit  = bit_offset % 8;
        }
      else
        {
        SensorCall();src_byte = (bit_offset + (sample * bps)) / 8;
        high_bit  = (bit_offset + (sample * bps)) % 8;
        }

      SensorCall();src = ibuff + src_byte;
      match_bits = mask_bits << (16 - high_bit - bps); 
      SensorCall();if (little_endian)
        {/*509*/SensorCall();buff1 = (src[0] << 8) | src[1];/*510*/}
      else
        {/*511*/SensorCall();buff1 = (src[1] << 8) | src[0];/*512*/}
      SensorCall();buff1 = (buff1 & match_bits) << (high_bit);
      
      SensorCall();if (ready_bits < 8)
        { /* add another bps bits to the buffer */
        SensorCall();bytebuff = 0;
        buff2 = (buff2 | (buff1 >> ready_bits));
        }
      else /* If we have a full buffer's worth, write it out */
        {
        SensorCall();bytebuff = (buff2 >> 8);
        *dst++ = bytebuff;
        ready_bits -= 8;
        /* shift in new bits */
        buff2 = ((buff2 << 8) | (buff1 >> ready_bits));
        }
      SensorCall();ready_bits += bps;
      }
    }

  SensorCall();if (ready_bits > 0)
    {
    SensorCall();bytebuff = (buff2 >> 8);
    *dst++ = bytebuff;
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end reverseSamples16bits */

static int
reverseSamples24bits (uint16 spp, uint16 bps, uint32 width, 
                      uint8 *ibuff, uint8 *obuff)
  {
  SensorCall();int      ready_bits = 0;
  uint32   col;
  uint32   src_byte = 0, high_bit = 0;
  uint32   bit_offset = 0;
  uint32   match_bits = 0, mask_bits = 0;
  uint32   buff1 = 0, buff2 = 0;
  uint8    bytebuff1 = 0, bytebuff2 = 0;
  unsigned char *src;
  unsigned char *dst;
  tsample_t sample;

  SensorCall();if ((ibuff == NULL) || (obuff == NULL))
    {
    SensorCall();TIFFError("reverseSamples24bits","Invalid image or work buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();ready_bits = 0;
  mask_bits =  (uint32)-1 >> (32 - bps);
  dst = obuff;
  SensorCall();for (col = width; col > 0; col--)
    {
    /* Compute src byte(s) and bits within byte(s) */
    SensorCall();bit_offset = (col - 1) * bps * spp;
    SensorCall();for (sample = 0; sample < spp; sample++)
      {
      SensorCall();if (sample == 0)
        {
        SensorCall();src_byte = bit_offset / 8;
        high_bit  = bit_offset % 8;
        }
      else
        {
        SensorCall();src_byte = (bit_offset + (sample * bps)) / 8;
        high_bit  = (bit_offset + (sample * bps)) % 8;
        }

      SensorCall();src = ibuff + src_byte;
      match_bits = mask_bits << (32 - high_bit - bps); 
      SensorCall();if (little_endian)
	{/*513*/SensorCall();buff1 = (src[0] << 24) | (src[1] << 16) | (src[2] << 8) | src[3];/*514*/}
      else
	{/*515*/SensorCall();buff1 = (src[3] << 24) | (src[2] << 16) | (src[1] << 8) | src[0];/*516*/}
      SensorCall();buff1 = (buff1 & match_bits) << (high_bit);

      SensorCall();if (ready_bits < 16)
        { /* add another bps bits to the buffer */
        SensorCall();bytebuff1 = bytebuff2 = 0;
        buff2 = (buff2 | (buff1 >> ready_bits));
        }
      else /* If we have a full buffer's worth, write it out */
        {
        SensorCall();bytebuff1 = (buff2 >> 24);
        *dst++ = bytebuff1;
        bytebuff2 = (buff2 >> 16);
        *dst++ = bytebuff2;
        ready_bits -= 16;

        /* shift in new bits */
        buff2 = ((buff2 << 16) | (buff1 >> ready_bits));
        }
      SensorCall();ready_bits += bps;
      }
    }

 /* catch any trailing bits at the end of the line */
  SensorCall();while (ready_bits > 0)
    {
    SensorCall();bytebuff1 = (buff2 >> 24);
    *dst++ = bytebuff1;

    buff2 = (buff2 << 8);
    bytebuff2 = bytebuff1;
    ready_bits -= 8;
    }
 
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end reverseSamples24bits */


static int
reverseSamples32bits (uint16 spp, uint16 bps, uint32 width, 
                      uint8 *ibuff, uint8 *obuff)
  {
  SensorCall();int    ready_bits = 0, shift_width = 0;
  int    bytes_per_sample, bytes_per_pixel;
  uint32 bit_offset;
  uint32 src_byte = 0, high_bit = 0;
  uint32 col;
  uint32 longbuff1 = 0, longbuff2 = 0;
  uint64 mask_bits = 0, match_bits = 0;
  uint64 buff1 = 0, buff2 = 0, buff3 = 0;
  uint8  bytebuff1 = 0, bytebuff2 = 0, bytebuff3 = 0, bytebuff4 = 0;
  unsigned char *src;
  unsigned char *dst;
  tsample_t sample;

  SensorCall();if ((ibuff == NULL) || (obuff == NULL))
    {
    SensorCall();TIFFError("reverseSamples32bits","Invalid image or work buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();ready_bits = 0;
  mask_bits =  (uint64)-1 >> (64 - bps);
  dst = obuff;

  bytes_per_sample = (bps + 7) / 8;
  bytes_per_pixel  = ((bps * spp) + 7) / 8;
  SensorCall();if (bytes_per_pixel < (bytes_per_sample + 1))
    {/*517*/SensorCall();shift_width = bytes_per_pixel;/*518*/}
  else
    {/*519*/SensorCall();shift_width = bytes_per_sample + 1;/*520*/}

  SensorCall();for (col = width; col > 0; col--)
    {
    /* Compute src byte(s) and bits within byte(s) */
    SensorCall();bit_offset = (col - 1) * bps * spp;
    SensorCall();for (sample = 0; sample < spp; sample++)
      {
      SensorCall();if (sample == 0)
        {
        SensorCall();src_byte = bit_offset / 8;
        high_bit  = bit_offset % 8;
        }
      else
        {
        SensorCall();src_byte = (bit_offset + (sample * bps)) / 8;
        high_bit  = (bit_offset + (sample * bps)) % 8;
        }

      SensorCall();src = ibuff + src_byte;
      match_bits = mask_bits << (64 - high_bit - bps); 
      SensorCall();if (little_endian)
        {
	SensorCall();longbuff1 = (src[0] << 24) | (src[1] << 16) | (src[2] << 8) | src[3];
        longbuff2 = longbuff1;
        }
      else
        {
	SensorCall();longbuff1 = (src[3] << 24) | (src[2] << 16) | (src[1] << 8) | src[0];
        longbuff2 = longbuff1;
	}
      SensorCall();buff3 = ((uint64)longbuff1 << 32) | longbuff2;
      buff1 = (buff3 & match_bits) << (high_bit);

      SensorCall();if (ready_bits < 32)
        { /* add another bps bits to the buffer */
        SensorCall();bytebuff1 = bytebuff2 = bytebuff3 = bytebuff4 = 0;
        buff2 = (buff2 | (buff1 >> ready_bits));
        }
      else /* If we have a full buffer's worth, write it out */
        {
        SensorCall();bytebuff1 = (buff2 >> 56);
        *dst++ = bytebuff1;
        bytebuff2 = (buff2 >> 48);
        *dst++ = bytebuff2;
        bytebuff3 = (buff2 >> 40);
        *dst++ = bytebuff3;
        bytebuff4 = (buff2 >> 32);
        *dst++ = bytebuff4;
        ready_bits -= 32;
                    
        /* shift in new bits */
        buff2 = ((buff2 << 32) | (buff1 >> ready_bits));
        }
      SensorCall();ready_bits += bps;
      }
    }
  SensorCall();while (ready_bits > 0)
    {
    SensorCall();bytebuff1 = (buff2 >> 56);
    *dst++ = bytebuff1;
    buff2 = (buff2 << 8);
    ready_bits -= 8;
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end reverseSamples32bits */

static int
reverseSamplesBytes (uint16 spp, uint16 bps, uint32 width, 
                     uint8 *src, uint8 *dst)
  {
  SensorCall();int i;
  uint32  col, bytes_per_pixel, col_offset;
  uint8   bytebuff1;
  unsigned char swapbuff[32];
  
  SensorCall();if ((src == NULL) || (dst == NULL))
    {
    SensorCall();TIFFError("reverseSamplesBytes","Invalid input or output buffer");
    {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();bytes_per_pixel  = ((bps * spp) + 7) / 8;
  SensorCall();switch (bps / 8)
     {
     case 8:  /* Use memcpy for multiple bytes per sample data */
     case 4:
     case 3:
     case 2: SensorCall();for (col = 0; col < (width / 2); col++)
               {
	       SensorCall();col_offset = col * bytes_per_pixel;                     
	       _TIFFmemcpy (swapbuff, src + col_offset, bytes_per_pixel);
	       _TIFFmemcpy (src + col_offset, dst - col_offset - bytes_per_pixel, bytes_per_pixel);
	       _TIFFmemcpy (dst - col_offset - bytes_per_pixel, swapbuff, bytes_per_pixel);
               }
	     SensorCall();break;
     case 1: /* Use byte copy only for single byte per sample data */
             SensorCall();for (col = 0; col < (width / 2); col++)
               { 
	       SensorCall();for (i = 0; i < spp; i++)
                  {
		  SensorCall();bytebuff1 = *src;
		  *src++ = *(dst - spp + i);
                  *(dst - spp + i) = bytebuff1;
		  }
		SensorCall();dst -= spp;
                }
	     SensorCall();break;
     default: SensorCall();TIFFError("reverseSamplesBytes","Unsupported bit depth %d", bps);
       {int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
     }
  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  } /* end reverseSamplesBytes */


/* Mirror an image horizontally or vertically */
static int
mirrorImage(uint16 spp, uint16 bps, uint16 mirror, uint32 width, uint32 length, unsigned char *ibuff)
  {
  SensorCall();int      shift_width;
  uint32   bytes_per_pixel, bytes_per_sample;
  uint32   row, rowsize, row_offset;
  unsigned char *line_buff = NULL;
  unsigned char *src;
  unsigned char *dst;

  src = ibuff;
  rowsize = ((width * bps * spp) + 7) / 8;
  SensorCall();switch (mirror)
    {
    case MIRROR_BOTH:
    case MIRROR_VERT: 
             SensorCall();line_buff = (unsigned char *)_TIFFmalloc(rowsize);
             SensorCall();if (line_buff == NULL)
               {
	       SensorCall();TIFFError ("mirrorImage", "Unable to allocate mirror line buffer of %1u bytes", rowsize);
               {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
               }

             SensorCall();dst = ibuff + (rowsize * (length - 1));
             SensorCall();for (row = 0; row < length / 2; row++)
               {
	      SensorCall();_TIFFmemcpy(line_buff, src, rowsize);
	      _TIFFmemcpy(src, dst,  rowsize);
	      _TIFFmemcpy(dst, line_buff, rowsize);
               src += (rowsize);
               dst -= (rowsize);                                 
               }
             SensorCall();if (line_buff)
               {/*485*/SensorCall();_TIFFfree(line_buff);/*486*/}
             SensorCall();if (mirror == MIRROR_VERT)
               {/*487*/SensorCall();break;/*488*/}
    case MIRROR_HORIZ :
              SensorCall();if ((bps % 8) == 0) /* byte alligned data */
                { 
                SensorCall();for (row = 0; row < length; row++)
                  {
		  SensorCall();row_offset = row * rowsize;
                  src = ibuff + row_offset;
                  dst = ibuff + row_offset + rowsize;
                  SensorCall();if (reverseSamplesBytes(spp, bps, width, src, dst))
                    {
		    {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
                    }
		  }
		}
	      else
                { /* non 8 bit per sample  data */
                SensorCall();if (!(line_buff = (unsigned char *)_TIFFmalloc(rowsize + 1)))
                  {
                  SensorCall();TIFFError("mirrorImage", "Unable to allocate mirror line buffer");
                  {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
                  }
                SensorCall();bytes_per_sample = (bps + 7) / 8;
                bytes_per_pixel  = ((bps * spp) + 7) / 8;
                SensorCall();if (bytes_per_pixel < (bytes_per_sample + 1))
                  {/*489*/SensorCall();shift_width = bytes_per_pixel;/*490*/}
                else
                  {/*491*/SensorCall();shift_width = bytes_per_sample + 1;/*492*/}

                SensorCall();for (row = 0; row < length; row++)
                  {
		  SensorCall();row_offset = row * rowsize;
                  src = ibuff + row_offset;
                  _TIFFmemset (line_buff, '\0', rowsize);
                  SensorCall();switch (shift_width)
                    {
                    case 1: SensorCall();if (reverseSamples16bits(spp, bps, width, src, line_buff))
                              {
		              SensorCall();_TIFFfree(line_buff);
                              {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
                              }
                             SensorCall();_TIFFmemcpy (src, line_buff, rowsize);
                             SensorCall();break;
                    case 2: SensorCall();if (reverseSamples24bits(spp, bps, width, src, line_buff))
                              {
		              SensorCall();_TIFFfree(line_buff);
                              {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
                              }
                             SensorCall();_TIFFmemcpy (src, line_buff, rowsize);
                             SensorCall();break;
                    case 3: 
                    case 4: 
                    case 5: SensorCall();if (reverseSamples32bits(spp, bps, width, src, line_buff))
                              {
		              SensorCall();_TIFFfree(line_buff);
                              {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
                              }
                             SensorCall();_TIFFmemcpy (src, line_buff, rowsize);
                             SensorCall();break;
                    default: SensorCall();TIFFError("mirrorImage","Unsupported bit depth %d", bps);
		             _TIFFfree(line_buff);
                             {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}      
                    }
		  }
                SensorCall();if (line_buff)
                  {/*493*/SensorCall();_TIFFfree(line_buff);/*494*/}
		}
             SensorCall();break;

    default: SensorCall();TIFFError ("mirrorImage", "Invalid mirror axis %d", mirror);
             {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
             break;
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }

/* Invert the light and dark values for a bilevel or grayscale image */
static int
invertImage(uint16 photometric, uint16 spp, uint16 bps, uint32 width, uint32 length, unsigned char *work_buff)
  {
  SensorCall();uint32   row, col;
  unsigned char  bytebuff1, bytebuff2, bytebuff3, bytebuff4;
  unsigned char *src;
  uint16        *src_uint16;
  uint32        *src_uint32;

  SensorCall();if (spp != 1)
    {
    SensorCall();TIFFError("invertImage", "Image inversion not supported for more than one sample per pixel");
    {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();if (photometric !=  PHOTOMETRIC_MINISWHITE && photometric !=  PHOTOMETRIC_MINISBLACK)
    {
    SensorCall();TIFFError("invertImage", "Only black and white and grayscale images can be inverted");
    {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();src = work_buff;
  SensorCall();if (src == NULL)
    {
    SensorCall();TIFFError ("invertImage", "Invalid crop buffer passed to invertImage");
    {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }

  SensorCall();switch (bps)
    {
    case 32: SensorCall();src_uint32 = (uint32 *)src;
             SensorCall();for (row = 0; row < length; row++)
               {/*495*/SensorCall();for (col = 0; col < width; col++)
                 {
		 SensorCall();*src_uint32 = (uint32)0xFFFFFFFF - *src_uint32;
                  src_uint32++;
                 ;/*496*/}}
            SensorCall();break;
    case 16: SensorCall();src_uint16 = (uint16 *)src;
             SensorCall();for (row = 0; row < length; row++)
               {/*497*/SensorCall();for (col = 0; col < width; col++)
                 {
		 SensorCall();*src_uint16 = (uint16)0xFFFF - *src_uint16;
                  src_uint16++;
                 ;/*498*/}}
            SensorCall();break;
    case 8: SensorCall();for (row = 0; row < length; row++)
              {/*499*/SensorCall();for (col = 0; col < width; col++)
                {
		SensorCall();*src = (uint8)255 - *src;
                 src++;
                ;/*500*/}}
            SensorCall();break;
    case 4: SensorCall();for (row = 0; row < length; row++)
              {/*501*/SensorCall();for (col = 0; col < width; col++)
                {
		SensorCall();bytebuff1 = 16 - (uint8)(*src & 240 >> 4);
		bytebuff2 = 16 - (*src & 15);
		*src = bytebuff1 << 4 & bytebuff2;
                src++;
                ;/*502*/}}
            SensorCall();break;
    case 2: SensorCall();for (row = 0; row < length; row++)
              {/*503*/SensorCall();for (col = 0; col < width; col++)
                {
		SensorCall();bytebuff1 = 4 - (uint8)(*src & 192 >> 6);
		bytebuff2 = 4 - (uint8)(*src & 48  >> 4);
		bytebuff3 = 4 - (uint8)(*src & 12  >> 2);
		bytebuff4 = 4 - (uint8)(*src & 3);
		*src = (bytebuff1 << 6) || (bytebuff2 << 4) || (bytebuff3 << 2) || bytebuff4;
                src++;
                ;/*504*/}}
            SensorCall();break;
    case 1: SensorCall();for (row = 0; row < length; row++)
              {/*505*/SensorCall();for (col = 0; col < width; col += 8 /(spp * bps))
                {
                SensorCall();*src = ~(*src);
                src++;
                ;/*506*/}}
            SensorCall();break;
    default: SensorCall();TIFFError("invertImage", "Unsupported bit depth %d", bps);
      {int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
    }

  {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
  }

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
