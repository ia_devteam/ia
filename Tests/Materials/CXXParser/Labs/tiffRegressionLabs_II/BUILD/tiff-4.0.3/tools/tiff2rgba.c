/* $Id: tiff2rgba.c,v 1.19 2011-02-23 21:46:09 fwarmerdam Exp $ */

/*
 * Copyright (c) 1991-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#include "tif_config.h"
#include "/var/tmp/sensor.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifdef NEED_LIBPORT
# include "libport.h"
#endif

#include "tiffiop.h"
#include "tiffio.h"

#define	streq(a,b)	(strcmp(a,b) == 0)
#define	CopyField(tag, v) \
    if (TIFFGetField(in, tag, &v)) TIFFSetField(out, tag, v)

#ifndef howmany
#define	howmany(x, y)	(((x)+((y)-1))/(y))
#endif
#define	roundup(x, y)	(howmany(x,y)*((uint32)(y)))

uint16 compression = COMPRESSION_PACKBITS;
uint32 rowsperstrip = (uint32) -1;
int process_by_block = 0; /* default is whole image at once */
int no_alpha = 0;
int bigtiff_output = 0;


static int tiffcvt(TIFF* in, TIFF* out);
static void usage(int code);

int
main(int argc, char* argv[])
{
	SensorCall();TIFF *in, *out;
	int c;
	extern int optind;
	extern char *optarg;

	SensorCall();while ((c = getopt(argc, argv, "c:r:t:bn8")) != -1)
		{/*15*/SensorCall();switch (c) {
			case 'b':
				SensorCall();process_by_block = 1;
				SensorCall();break;

			case 'c':
				SensorCall();if (streq(optarg, "none"))
					compression = COMPRESSION_NONE;
				else {/*17*/SensorCall();if (streq(optarg, "packbits"))
					compression = COMPRESSION_PACKBITS;
				else {/*19*/SensorCall();if (streq(optarg, "lzw"))
					compression = COMPRESSION_LZW;
				else {/*21*/SensorCall();if (streq(optarg, "jpeg"))
					compression = COMPRESSION_JPEG;
				else {/*23*/SensorCall();if (streq(optarg, "zip"))
					compression = COMPRESSION_DEFLATE;
				else
					{/*25*/SensorCall();usage(-1);/*26*/}/*24*/}/*22*/}/*20*/}/*18*/}
				SensorCall();break;

			case 'r':
				SensorCall();rowsperstrip = atoi(optarg);
				SensorCall();break;

			case 't':
				SensorCall();rowsperstrip = atoi(optarg);
				SensorCall();break;

			case 'n':
				SensorCall();no_alpha = 1;
				SensorCall();break;

			case '8':
				SensorCall();bigtiff_output = 1;
				SensorCall();break;

			case '?':
				SensorCall();usage(0);
				/*NOTREACHED*/
		;/*16*/}}

	SensorCall();if (argc - optind < 2)
		{/*27*/SensorCall();usage(-1);/*28*/}

	SensorCall();out = TIFFOpen(argv[argc-1], bigtiff_output?"w8":"w");
	SensorCall();if (out == NULL)
		{/*29*/{int  ReplaceReturn = (-2); SensorCall(); return ReplaceReturn;}/*30*/}

	SensorCall();for (; optind < argc-1; optind++) {
		SensorCall();in = TIFFOpen(argv[optind], "r");
		SensorCall();if (in != NULL) {
			SensorCall();do {
				SensorCall();if (!tiffcvt(in, out) ||
				    !TIFFWriteDirectory(out)) {
					SensorCall();(void) TIFFClose(out);
					(void) TIFFClose(in);
					{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
				}
			} while (TIFFReadDirectory(in));
			SensorCall();(void) TIFFClose(in);
		}
	}
	SensorCall();(void) TIFFClose(out);
	{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
}

static int
cvt_by_tile( TIFF *in, TIFF *out )

{
    SensorCall();uint32* raster;			/* retrieve RGBA image */
    uint32  width, height;		/* image width & height */
    uint32  tile_width, tile_height;
    uint32  row, col;
    uint32  *wrk_line;
    int	    ok = 1;
    uint32  rastersize, wrk_linesize;

    TIFFGetField(in, TIFFTAG_IMAGEWIDTH, &width);
    TIFFGetField(in, TIFFTAG_IMAGELENGTH, &height);

    SensorCall();if( !TIFFGetField(in, TIFFTAG_TILEWIDTH, &tile_width)
        || !TIFFGetField(in, TIFFTAG_TILELENGTH, &tile_height) ) {
        SensorCall();TIFFError(TIFFFileName(in), "Source image not tiled");
        {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
    }
    
    SensorCall();TIFFSetField(out, TIFFTAG_TILEWIDTH, tile_width );
    TIFFSetField(out, TIFFTAG_TILELENGTH, tile_height );

    /*
     * Allocate tile buffer
     */
    rastersize = tile_width * tile_height * sizeof (uint32);
    SensorCall();if (width != (tile_width / tile_height) / sizeof( uint32))
    {
	SensorCall();TIFFError(TIFFFileName(in), "Integer overflow when calculating raster buffer");
	exit(-1);
    }
    SensorCall();raster = (uint32*)_TIFFmalloc(rastersize);
    SensorCall();if (raster == 0) {
        SensorCall();TIFFError(TIFFFileName(in), "No space for raster buffer");
        {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
    }

    /*
     * Allocate a scanline buffer for swapping during the vertical
     * mirroring pass.
     */
    SensorCall();wrk_linesize = tile_width * sizeof (uint32);
    SensorCall();if (wrk_linesize != tile_width / sizeof (uint32))
    {
        SensorCall();TIFFError(TIFFFileName(in), "Integer overflow when calculating wrk_line buffer");
	exit(-1);
    }
    SensorCall();wrk_line = (uint32*)_TIFFmalloc(wrk_linesize);
    SensorCall();if (!wrk_line) {
        SensorCall();TIFFError(TIFFFileName(in), "No space for raster scanline buffer");
        ok = 0;
    }
    
    /*
     * Loop over the tiles.
     */
    SensorCall();for( row = 0; ok && row < height; row += tile_height )
    {
        SensorCall();for( col = 0; ok && col < width; col += tile_width )
        {
            SensorCall();uint32 i_row;

            /* Read the tile into an RGBA array */
            SensorCall();if (!TIFFReadRGBATile(in, col, row, raster)) {
                SensorCall();ok = 0;
                SensorCall();break;
            }


	    /*
	     * XXX: raster array has 4-byte unsigned integer type, that is why
	     * we should rearrange it here.
	     */
#if HOST_BIGENDIAN
	    TIFFSwabArrayOfLong(raster, tile_width * tile_height);
#endif

            /*
             * For some reason the TIFFReadRGBATile() function chooses the
             * lower left corner as the origin.  Vertically mirror scanlines.
             */
            SensorCall();for( i_row = 0; i_row < tile_height / 2; i_row++ )
            {
                SensorCall();uint32	*top_line, *bottom_line;

                top_line = raster + tile_width * i_row;
                bottom_line = raster + tile_width * (tile_height-i_row-1);

                _TIFFmemcpy(wrk_line, top_line, 4*tile_width);
                _TIFFmemcpy(top_line, bottom_line, 4*tile_width);
                _TIFFmemcpy(bottom_line, wrk_line, 4*tile_width);
            }

            /*
             * Write out the result in a tile.
             */

            SensorCall();if( TIFFWriteEncodedTile( out,
                                      TIFFComputeTile( out, col, row, 0, 0),
                                      raster,
                                      4 * tile_width * tile_height ) == -1 )
            {
                SensorCall();ok = 0;
                SensorCall();break;
            }
        }
    }

    SensorCall();_TIFFfree( raster );
    _TIFFfree( wrk_line );

    {int  ReplaceReturn = ok; SensorCall(); return ReplaceReturn;}
}

static int
cvt_by_strip( TIFF *in, TIFF *out )

{
    SensorCall();uint32* raster;			/* retrieve RGBA image */
    uint32  width, height;		/* image width & height */
    uint32  row;
    uint32  *wrk_line;
    int	    ok = 1;
    uint32  rastersize, wrk_linesize;

    TIFFGetField(in, TIFFTAG_IMAGEWIDTH, &width);
    TIFFGetField(in, TIFFTAG_IMAGELENGTH, &height);

    SensorCall();if( !TIFFGetField(in, TIFFTAG_ROWSPERSTRIP, &rowsperstrip) ) {
        SensorCall();TIFFError(TIFFFileName(in), "Source image not in strips");
        {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
    }
    
    SensorCall();TIFFSetField(out, TIFFTAG_ROWSPERSTRIP, rowsperstrip);

    /*
     * Allocate strip buffer
     */
    rastersize = width * rowsperstrip * sizeof (uint32);
    SensorCall();if (width != (rastersize / rowsperstrip) / sizeof( uint32))
    {
	SensorCall();TIFFError(TIFFFileName(in), "Integer overflow when calculating raster buffer");
	exit(-1);
    }
    SensorCall();raster = (uint32*)_TIFFmalloc(rastersize);
    SensorCall();if (raster == 0) {
        SensorCall();TIFFError(TIFFFileName(in), "No space for raster buffer");
        {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
    }

    /*
     * Allocate a scanline buffer for swapping during the vertical
     * mirroring pass.
     */
    SensorCall();wrk_linesize = width * sizeof (uint32);
    SensorCall();if (wrk_linesize != width / sizeof (uint32))
    {
        SensorCall();TIFFError(TIFFFileName(in), "Integer overflow when calculating wrk_line buffer");
	exit(-1);
    }
    SensorCall();wrk_line = (uint32*)_TIFFmalloc(wrk_linesize);
    SensorCall();if (!wrk_line) {
        SensorCall();TIFFError(TIFFFileName(in), "No space for raster scanline buffer");
        ok = 0;
    }
    
    /*
     * Loop over the strips.
     */
    SensorCall();for( row = 0; ok && row < height; row += rowsperstrip )
    {
        SensorCall();int	rows_to_write, i_row;

        /* Read the strip into an RGBA array */
        SensorCall();if (!TIFFReadRGBAStrip(in, row, raster)) {
            SensorCall();ok = 0;
            SensorCall();break;
        }

	/*
	 * XXX: raster array has 4-byte unsigned integer type, that is why
	 * we should rearrange it here.
	 */
#if HOST_BIGENDIAN
	TIFFSwabArrayOfLong(raster, width * rowsperstrip);
#endif

        /*
         * Figure out the number of scanlines actually in this strip.
         */
        SensorCall();if( row + rowsperstrip > height )
            {/*31*/SensorCall();rows_to_write = height - row;/*32*/}
        else
            {/*33*/SensorCall();rows_to_write = rowsperstrip;/*34*/}

        /*
         * For some reason the TIFFReadRGBAStrip() function chooses the
         * lower left corner as the origin.  Vertically mirror scanlines.
         */

        SensorCall();for( i_row = 0; i_row < rows_to_write / 2; i_row++ )
        {
            SensorCall();uint32	*top_line, *bottom_line;

            top_line = raster + width * i_row;
            bottom_line = raster + width * (rows_to_write-i_row-1);

            _TIFFmemcpy(wrk_line, top_line, 4*width);
            _TIFFmemcpy(top_line, bottom_line, 4*width);
            _TIFFmemcpy(bottom_line, wrk_line, 4*width);
        }

        /*
         * Write out the result in a strip
         */

        SensorCall();if( TIFFWriteEncodedStrip( out, row / rowsperstrip, raster,
                                   4 * rows_to_write * width ) == -1 )
        {
            SensorCall();ok = 0;
            SensorCall();break;
        }
    }

    SensorCall();_TIFFfree( raster );
    _TIFFfree( wrk_line );

    {int  ReplaceReturn = ok; SensorCall(); return ReplaceReturn;}
}

/*
 * cvt_whole_image()
 *
 * read the whole image into one big RGBA buffer and then write out
 * strips from that.  This is using the traditional TIFFReadRGBAImage()
 * API that we trust.
 */

static int
cvt_whole_image( TIFF *in, TIFF *out )

{
    SensorCall();uint32* raster;			/* retrieve RGBA image */
    uint32  width, height;		/* image width & height */
    uint32  row;
    size_t pixel_count;
        
    TIFFGetField(in, TIFFTAG_IMAGEWIDTH, &width);
    TIFFGetField(in, TIFFTAG_IMAGELENGTH, &height);
    pixel_count = width * height;

    /* XXX: Check the integer overflow. */
    SensorCall();if (!width || !height || pixel_count / width != height) {
        SensorCall();TIFFError(TIFFFileName(in),
		  "Malformed input file; can't allocate buffer for raster of %lux%lu size",
		  (unsigned long)width, (unsigned long)height);
        {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
    }

    SensorCall();rowsperstrip = TIFFDefaultStripSize(out, rowsperstrip);
    TIFFSetField(out, TIFFTAG_ROWSPERSTRIP, rowsperstrip);

    raster = (uint32*)_TIFFCheckMalloc(in, pixel_count, sizeof(uint32), "raster buffer");
    SensorCall();if (raster == 0) {
        SensorCall();TIFFError(TIFFFileName(in), "Failed to allocate buffer (%lu elements of %lu each)",
		  (unsigned long)pixel_count, (unsigned long)sizeof(uint32));
        {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
    }

    /* Read the image in one chunk into an RGBA array */
    SensorCall();if (!TIFFReadRGBAImageOriented(in, width, height, raster,
                                   ORIENTATION_TOPLEFT, 0)) {
        SensorCall();_TIFFfree(raster);
        {int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
    }

    /*
     * XXX: raster array has 4-byte unsigned integer type, that is why
     * we should rearrange it here.
     */
#if HOST_BIGENDIAN
    TIFFSwabArrayOfLong(raster, width * height);
#endif

    /*
     * Do we want to strip away alpha components?
     */
    SensorCall();if (no_alpha)
    {
        SensorCall();size_t count = pixel_count;
        unsigned char *src, *dst;

	src = dst = (unsigned char *) raster;
        SensorCall();while (count > 0)
        {
	    SensorCall();*(dst++) = *(src++);
	    *(dst++) = *(src++);
	    *(dst++) = *(src++);
	    src++;
	    count--;
        }
    }

    /*
     * Write out the result in strips
     */
    SensorCall();for (row = 0; row < height; row += rowsperstrip)
    {
        SensorCall();unsigned char * raster_strip;
        int	rows_to_write;
        int	bytes_per_pixel;

        SensorCall();if (no_alpha)
        {
            SensorCall();raster_strip = ((unsigned char *) raster) + 3 * row * width;
            bytes_per_pixel = 3;
        }
        else
        {
            SensorCall();raster_strip = (unsigned char *) (raster + row * width);
            bytes_per_pixel = 4;
        }

        SensorCall();if( row + rowsperstrip > height )
            {/*35*/SensorCall();rows_to_write = height - row;/*36*/}
        else
            {/*37*/SensorCall();rows_to_write = rowsperstrip;/*38*/}

        SensorCall();if( TIFFWriteEncodedStrip( out, row / rowsperstrip, raster_strip,
                             bytes_per_pixel * rows_to_write * width ) == -1 )
        {
            SensorCall();_TIFFfree( raster );
            {int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
        }
    }

    SensorCall();_TIFFfree( raster );

    {int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}


static int
tiffcvt(TIFF* in, TIFF* out)
{
	SensorCall();uint32 width, height;		/* image width & height */
	uint16 shortv;
	float floatv;
	char *stringv;
	uint32 longv;
        uint16 v[1];

	TIFFGetField(in, TIFFTAG_IMAGEWIDTH, &width);
	TIFFGetField(in, TIFFTAG_IMAGELENGTH, &height);

	CopyField(TIFFTAG_SUBFILETYPE, longv);
	SensorCall();TIFFSetField(out, TIFFTAG_IMAGEWIDTH, width);
	TIFFSetField(out, TIFFTAG_IMAGELENGTH, height);
	TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, 8);
	TIFFSetField(out, TIFFTAG_COMPRESSION, compression);
	TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);

	CopyField(TIFFTAG_FILLORDER, shortv);
	SensorCall();TIFFSetField(out, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);

        SensorCall();if( no_alpha )
            {/*1*/SensorCall();TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, 3);/*2*/}
        else
            {/*3*/SensorCall();TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, 4);/*4*/}

        SensorCall();if( !no_alpha )
        {
            SensorCall();v[0] = EXTRASAMPLE_ASSOCALPHA;
            TIFFSetField(out, TIFFTAG_EXTRASAMPLES, 1, v);
        }

	CopyField(TIFFTAG_XRESOLUTION, floatv);
	CopyField(TIFFTAG_YRESOLUTION, floatv);
	CopyField(TIFFTAG_RESOLUTIONUNIT, shortv);
	SensorCall();TIFFSetField(out, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
	TIFFSetField(out, TIFFTAG_SOFTWARE, TIFFGetVersion());
	CopyField(TIFFTAG_DOCUMENTNAME, stringv);

        SensorCall();if( process_by_block && TIFFIsTiled( in ) )
            {/*5*/SensorCall();return( cvt_by_tile( in, out ) );/*6*/}
        else {/*7*/SensorCall();if( process_by_block )
            {/*9*/SensorCall();return( cvt_by_strip( in, out ) );/*10*/}
        else
            {/*11*/SensorCall();return( cvt_whole_image( in, out ) );/*12*/}/*8*/}
SensorCall();}

static char* stuff[] = {
    "usage: tiff2rgba [-c comp] [-r rows] [-b] [-n] [-8] input... output",
    "where comp is one of the following compression algorithms:",
    " jpeg\t\tJPEG encoding",
    " zip\t\tLempel-Ziv & Welch encoding",
    " lzw\t\tLempel-Ziv & Welch encoding",
    " packbits\tPackBits encoding",
    " none\t\tno compression",
    "and the other options are:",
    " -r\trows/strip",
    " -b (progress by block rather than as a whole image)",
    " -n don't emit alpha component.",
    " -8 write BigTIFF file instead of ClassicTIFF",
    NULL
};

static void
usage(int code)
{
	SensorCall();char buf[BUFSIZ];
	int i;

	setbuf(stderr, buf);
        fprintf(stderr, "%s\n\n", TIFFGetVersion());
	SensorCall();for (i = 0; stuff[i] != NULL; i++)
		{/*13*/SensorCall();fprintf(stderr, "%s\n", stuff[i]);/*14*/}
	SensorCall();exit(code);
SensorCall();}

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
