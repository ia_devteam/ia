/* $Id: tiffmedian.c,v 1.10 2010-03-10 18:56:50 bfriesen Exp $ */

/*
 * Apply median cut on an image.
 *
 * tiffmedian [-c n] [-f] input output
 *     -C n		- set colortable size.  Default is 256.
 *     -f		- use Floyd-Steinberg dithering.
 *     -c lzw		- compress output with LZW 
 *     -c none		- use no compression on output
 *     -c packbits	- use packbits compression on output
 *     -r n		- create output with n rows/strip of data
 * (by default the compression scheme and rows/strip are taken
 *  from the input file)
 *
 * Notes:
 *
 * [1] Floyd-Steinberg dither:
 *  I should point out that the actual fractions we used were, assuming
 *  you are at X, moving left to right:
 *
 *		    X     7/16
 *	     3/16   5/16  1/16    
 *
 *  Note that the error goes to four neighbors, not three.  I think this
 *  will probably do better (at least for black and white) than the
 *  3/8-3/8-1/4 distribution, at the cost of greater processing.  I have
 *  seen the 3/8-3/8-1/4 distribution described as "our" algorithm before,
 *  but I have no idea who the credit really belongs to.

 *  Also, I should add that if you do zig-zag scanning (see my immediately
 *  previous message), it is sufficient (but not quite as good) to send
 *  half the error one pixel ahead (e.g. to the right on lines you scan
 *  left to right), and half one pixel straight down.  Again, this is for
 *  black and white;  I've not tried it with color.
 *  -- 
 *					    Lou Steinberg
 *
 * [2] Color Image Quantization for Frame Buffer Display, Paul Heckbert,
 *	Siggraph '82 proceedings, pp. 297-307
 */

#include "tif_config.h"
#include "/var/tmp/sensor.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifdef NEED_LIBPORT
# include "libport.h"
#endif

#include "tiffio.h"

#define	MAX_CMAP_SIZE	256

#define	streq(a,b)	(strcmp(a,b) == 0)
#define	strneq(a,b,n)	(strncmp(a,b,n) == 0)

#define	COLOR_DEPTH	8
#define	MAX_COLOR	256

#define	B_DEPTH		5		/* # bits/pixel to use */
#define	B_LEN		(1L<<B_DEPTH)

#define	C_DEPTH		2
#define	C_LEN		(1L<<C_DEPTH)	/* # cells/color to use */

#define	COLOR_SHIFT	(COLOR_DEPTH-B_DEPTH)

typedef	struct colorbox {
	struct	colorbox *next, *prev;
	int	rmin, rmax;
	int	gmin, gmax;
	int	bmin, bmax;
	uint32	total;
} Colorbox;

typedef struct {
	int	num_ents;
	int	entries[MAX_CMAP_SIZE][2];
} C_cell;

uint16	rm[MAX_CMAP_SIZE], gm[MAX_CMAP_SIZE], bm[MAX_CMAP_SIZE];
int	num_colors;
uint32	histogram[B_LEN][B_LEN][B_LEN];
Colorbox *freeboxes;
Colorbox *usedboxes;
C_cell	**ColorCells;
TIFF	*in, *out;
uint32	rowsperstrip = (uint32) -1;
uint16	compression = (uint16) -1;
uint16	bitspersample = 1;
uint16	samplesperpixel;
uint32	imagewidth;
uint32	imagelength;
uint16	predictor = 0;

static	void get_histogram(TIFF*, Colorbox*);
static	void splitbox(Colorbox*);
static	void shrinkbox(Colorbox*);
static	void map_colortable(void);
static	void quant(TIFF*, TIFF*);
static	void quant_fsdither(TIFF*, TIFF*);
static	Colorbox* largest_box(void);

static	void usage(void);
static	int processCompressOptions(char*);

#define	CopyField(tag, v) \
	if (TIFFGetField(in, tag, &v)) TIFFSetField(out, tag, v)

int
main(int argc, char* argv[])
{
	SensorCall();int i, dither = 0;
	uint16 shortv, config, photometric;
	Colorbox *box_list, *ptr;
	float floatv;
	uint32 longv;
	int c;
	extern int optind;
	extern char* optarg;

	num_colors = MAX_CMAP_SIZE;
	SensorCall();while ((c = getopt(argc, argv, "c:C:r:f")) != -1)
		{/*97*/SensorCall();switch (c) {
		case 'c':		/* compression scheme */
			SensorCall();if (!processCompressOptions(optarg))
				{/*99*/SensorCall();usage();/*100*/}
			SensorCall();break;
		case 'C':		/* set colormap size */
			SensorCall();num_colors = atoi(optarg);
			SensorCall();if (num_colors > MAX_CMAP_SIZE) {
				SensorCall();fprintf(stderr,
				   "-c: colormap too big, max %d\n",
				   MAX_CMAP_SIZE);
				usage();
			}
			SensorCall();break;
		case 'f':		/* dither */
			SensorCall();dither = 1;
			SensorCall();break;
		case 'r':		/* rows/strip */
			SensorCall();rowsperstrip = atoi(optarg);
			SensorCall();break;
		case '?':
			SensorCall();usage();
			/*NOTREACHED*/
		;/*98*/}}
	SensorCall();if (argc - optind != 2)
		{/*101*/SensorCall();usage();/*102*/}
	SensorCall();in = TIFFOpen(argv[optind], "r");
	SensorCall();if (in == NULL)
		{/*103*/{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}/*104*/}
	SensorCall();TIFFGetField(in, TIFFTAG_IMAGEWIDTH, &imagewidth);
	TIFFGetField(in, TIFFTAG_IMAGELENGTH, &imagelength);
	TIFFGetField(in, TIFFTAG_BITSPERSAMPLE, &bitspersample);
	TIFFGetField(in, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
	SensorCall();if (bitspersample != 8 && bitspersample != 16) {
		SensorCall();fprintf(stderr, "%s: Image must have at least 8-bits/sample\n",
		    argv[optind]);
		{int  ReplaceReturn = (-3); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();if (!TIFFGetField(in, TIFFTAG_PHOTOMETRIC, &photometric) ||
	    photometric != PHOTOMETRIC_RGB || samplesperpixel < 3) {
		SensorCall();fprintf(stderr, "%s: Image must have RGB data\n", argv[optind]);
		{int  ReplaceReturn = (-4); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();TIFFGetField(in, TIFFTAG_PLANARCONFIG, &config);
	SensorCall();if (config != PLANARCONFIG_CONTIG) {
		SensorCall();fprintf(stderr, "%s: Can only handle contiguous data packing\n",
		    argv[optind]);
		{int  ReplaceReturn = (-5); SensorCall(); return ReplaceReturn;}
	}

	/*
	 * STEP 1:  create empty boxes
	 */
	SensorCall();usedboxes = NULL;
	box_list = freeboxes = (Colorbox *)_TIFFmalloc(num_colors*sizeof (Colorbox));
	freeboxes[0].next = &freeboxes[1];
	freeboxes[0].prev = NULL;
	SensorCall();for (i = 1; i < num_colors-1; ++i) {
		SensorCall();freeboxes[i].next = &freeboxes[i+1];
		freeboxes[i].prev = &freeboxes[i-1];
	}
	SensorCall();freeboxes[num_colors-1].next = NULL;
	freeboxes[num_colors-1].prev = &freeboxes[num_colors-2];

	/*
	 * STEP 2: get histogram, initialize first box
	 */
	ptr = freeboxes;
	freeboxes = ptr->next;
	SensorCall();if (freeboxes)
		freeboxes->prev = NULL;
	SensorCall();ptr->next = usedboxes;
	usedboxes = ptr;
	SensorCall();if (ptr->next)
		{/*105*/SensorCall();ptr->next->prev = ptr;/*106*/}
	SensorCall();get_histogram(in, ptr);

	/*
	 * STEP 3: continually subdivide boxes until no more free
	 * boxes remain or until all colors assigned.
	 */
	SensorCall();while (freeboxes != NULL) {
		SensorCall();ptr = largest_box();
		SensorCall();if (ptr != NULL)
			{/*107*/SensorCall();splitbox(ptr);/*108*/}
		else
			freeboxes = NULL;
	}

	/*
	 * STEP 4: assign colors to all boxes
	 */
	SensorCall();for (i = 0, ptr = usedboxes; ptr != NULL; ++i, ptr = ptr->next) {
		SensorCall();rm[i] = ((ptr->rmin + ptr->rmax) << COLOR_SHIFT) / 2;
		gm[i] = ((ptr->gmin + ptr->gmax) << COLOR_SHIFT) / 2;
		bm[i] = ((ptr->bmin + ptr->bmax) << COLOR_SHIFT) / 2;
	}

	/* We're done with the boxes now */
	SensorCall();_TIFFfree(box_list);
	freeboxes = usedboxes = NULL;

	/*
	 * STEP 5: scan histogram and map all values to closest color
	 */
	/* 5a: create cell list as described in Heckbert[2] */
	ColorCells = (C_cell **)_TIFFmalloc(C_LEN*C_LEN*C_LEN*sizeof (C_cell*));
	_TIFFmemset(ColorCells, 0, C_LEN*C_LEN*C_LEN*sizeof (C_cell*));
	/* 5b: create mapping from truncated pixel space to color
	   table entries */
	map_colortable();

	/*
	 * STEP 6: scan image, match input values to table entries
	 */
	out = TIFFOpen(argv[optind+1], "w");
	SensorCall();if (out == NULL)
		{/*109*/{int  ReplaceReturn = (-2); SensorCall(); return ReplaceReturn;}/*110*/}

	CopyField(TIFFTAG_SUBFILETYPE, longv);
	CopyField(TIFFTAG_IMAGEWIDTH, longv);
	SensorCall();TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, (short)COLOR_DEPTH);
	if (compression != (uint16)-1) {
		TIFFSetField(out, TIFFTAG_COMPRESSION, compression);
		switch (compression) {
		case COMPRESSION_LZW:
		case COMPRESSION_DEFLATE:
			if (predictor != 0)
				TIFFSetField(out, TIFFTAG_PREDICTOR, predictor);
			break;
		}
	} else
		CopyField(TIFFTAG_COMPRESSION, compression);
	SensorCall();TIFFSetField(out, TIFFTAG_PHOTOMETRIC, (short)PHOTOMETRIC_PALETTE);
	CopyField(TIFFTAG_ORIENTATION, shortv);
	SensorCall();TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, (short)1);
	CopyField(TIFFTAG_PLANARCONFIG, shortv);
	SensorCall();TIFFSetField(out, TIFFTAG_ROWSPERSTRIP,
	    TIFFDefaultStripSize(out, rowsperstrip));
	CopyField(TIFFTAG_MINSAMPLEVALUE, shortv);
	CopyField(TIFFTAG_MAXSAMPLEVALUE, shortv);
	CopyField(TIFFTAG_RESOLUTIONUNIT, shortv);
	CopyField(TIFFTAG_XRESOLUTION, floatv);
	CopyField(TIFFTAG_YRESOLUTION, floatv);
	CopyField(TIFFTAG_XPOSITION, floatv);
	CopyField(TIFFTAG_YPOSITION, floatv);

	SensorCall();if (dither)
		{/*113*/SensorCall();quant_fsdither(in, out);/*114*/}
	else
		{/*115*/SensorCall();quant(in, out);/*116*/}
	/*
	 * Scale colormap to TIFF-required 16-bit values.
	 */
#define	SCALE(x)	(((x)*((1L<<16)-1))/255)
	SensorCall();for (i = 0; i < MAX_CMAP_SIZE; ++i) {
		SensorCall();rm[i] = SCALE(rm[i]);
		gm[i] = SCALE(gm[i]);
		bm[i] = SCALE(bm[i]);
	}
	SensorCall();TIFFSetField(out, TIFFTAG_COLORMAP, rm, gm, bm);
	(void) TIFFClose(out);
	{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
}

static int
processCompressOptions(char* opt)
{
	SensorCall();if (streq(opt, "none"))
		compression = COMPRESSION_NONE;
	else {/*85*/SensorCall();if (streq(opt, "packbits"))
		compression = COMPRESSION_PACKBITS;
	else {/*87*/SensorCall();if (strneq(opt, "lzw", 3)) {
		SensorCall();char* cp = strchr(opt, ':');
		SensorCall();if (cp)
			{/*89*/SensorCall();predictor = atoi(cp+1);/*90*/}
		SensorCall();compression = COMPRESSION_LZW;
	} else {/*91*/SensorCall();if (strneq(opt, "zip", 3)) {
		SensorCall();char* cp = strchr(opt, ':');
		SensorCall();if (cp)
			{/*93*/SensorCall();predictor = atoi(cp+1);/*94*/}
		SensorCall();compression = COMPRESSION_DEFLATE;
	} else
		{/*95*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*96*/}/*92*/}/*88*/}/*86*/}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

char* stuff[] = {
"usage: tiffmedian [options] input.tif output.tif",
"where options are:",
" -r #		make each strip have no more than # rows",
" -C #		create a colormap with # entries",
" -f		use Floyd-Steinberg dithering",
" -c lzw[:opts]	compress output with Lempel-Ziv & Welch encoding",
" -c zip[:opts]	compress output with deflate encoding",
" -c packbits	compress output with packbits encoding",
" -c none	use no compression algorithm on output",
"",
"LZW and deflate options:",
" #		set predictor value",
"For example, -c lzw:2 to get LZW-encoded data with horizontal differencing",
NULL
};

static void
usage(void)
{
	SensorCall();char buf[BUFSIZ];
	int i;

	setbuf(stderr, buf);
        fprintf(stderr, "%s\n\n", TIFFGetVersion());
	SensorCall();for (i = 0; stuff[i] != NULL; i++)
		{/*83*/SensorCall();fprintf(stderr, "%s\n", stuff[i]);/*84*/}
	SensorCall();exit(-1);
SensorCall();}

static void
get_histogram(TIFF* in, Colorbox* box)
{
	SensorCall();register unsigned char *inptr;
	register int red, green, blue;
	register uint32 j, i;
	unsigned char *inputline;

	inputline = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(in));
	SensorCall();if (inputline == NULL) {
		SensorCall();fprintf(stderr, "No space for scanline buffer\n");
		exit(-1);
	}
	SensorCall();box->rmin = box->gmin = box->bmin = 999;
	box->rmax = box->gmax = box->bmax = -1;
	box->total = imagewidth * imagelength;

	{ register uint32 *ptr = &histogram[0][0][0];
	  SensorCall();for (i = B_LEN*B_LEN*B_LEN; i-- > 0;)
		{/*1*/SensorCall();*ptr++ = 0;/*2*/}
	}
	SensorCall();for (i = 0; i < imagelength; i++) {
		SensorCall();if (TIFFReadScanline(in, inputline, i, 0) <= 0)
			{/*3*/SensorCall();break;/*4*/}
		SensorCall();inptr = inputline;
		SensorCall();for (j = imagewidth; j-- > 0;) {
			SensorCall();red = *inptr++ >> COLOR_SHIFT;
			green = *inptr++ >> COLOR_SHIFT;
			blue = *inptr++ >> COLOR_SHIFT;
			SensorCall();if (red < box->rmin)
				{/*5*/SensorCall();box->rmin = red;/*6*/}
		        SensorCall();if (red > box->rmax)
				{/*7*/SensorCall();box->rmax = red;/*8*/}
		        SensorCall();if (green < box->gmin)
				{/*9*/SensorCall();box->gmin = green;/*10*/}
		        SensorCall();if (green > box->gmax)
				{/*11*/SensorCall();box->gmax = green;/*12*/}
		        SensorCall();if (blue < box->bmin)
				{/*13*/SensorCall();box->bmin = blue;/*14*/}
		        SensorCall();if (blue > box->bmax)
				{/*15*/SensorCall();box->bmax = blue;/*16*/}
		        SensorCall();histogram[red][green][blue]++;
		}
	}
	SensorCall();_TIFFfree(inputline);
SensorCall();}

static Colorbox *
largest_box(void)
{
	SensorCall();register Colorbox *p, *b;
	register uint32 size;

	b = NULL;
	size = 0;
	SensorCall();for (p = usedboxes; p != NULL; p = p->next)
		{/*79*/SensorCall();if ((p->rmax > p->rmin || p->gmax > p->gmin ||
		    p->bmax > p->bmin) &&  p->total > size)
		        {/*81*/SensorCall();size = (b = p)->total;/*82*/}/*80*/}
	{Colorbox * ReplaceReturn = (b); SensorCall(); return ReplaceReturn;}
}

static void
splitbox(Colorbox* ptr)
{
	SensorCall();uint32		hist2[B_LEN];
	int		first=0, last=0;
	register Colorbox	*new;
	register uint32	*iptr, *histp;
	register int	i, j;
	register int	ir,ig,ib;
	register uint32 sum, sum1, sum2;
	enum { RED, GREEN, BLUE } axis;

	/*
	 * See which axis is the largest, do a histogram along that
	 * axis.  Split at median point.  Contract both new boxes to
	 * fit points and return
	 */
	i = ptr->rmax - ptr->rmin;
	SensorCall();if (i >= ptr->gmax - ptr->gmin && i >= ptr->bmax - ptr->bmin)
		{/*17*/SensorCall();axis = RED;/*18*/}
	else {/*19*/SensorCall();if (ptr->gmax - ptr->gmin >= ptr->bmax - ptr->bmin)
		{/*21*/SensorCall();axis = GREEN;/*22*/}
	else
		{/*23*/SensorCall();axis = BLUE;/*24*/}/*20*/}
	/* get histogram along longest axis */
	SensorCall();switch (axis) {
	case RED:
		SensorCall();histp = &hist2[ptr->rmin];
	        SensorCall();for (ir = ptr->rmin; ir <= ptr->rmax; ++ir) {
			SensorCall();*histp = 0;
			SensorCall();for (ig = ptr->gmin; ig <= ptr->gmax; ++ig) {
				SensorCall();iptr = &histogram[ir][ig][ptr->bmin];
				SensorCall();for (ib = ptr->bmin; ib <= ptr->bmax; ++ib)
					{/*25*/SensorCall();*histp += *iptr++;/*26*/}
			}
			SensorCall();histp++;
	        }
	        SensorCall();first = ptr->rmin;
		last = ptr->rmax;
	        SensorCall();break;
	case GREEN:
	        SensorCall();histp = &hist2[ptr->gmin];
	        SensorCall();for (ig = ptr->gmin; ig <= ptr->gmax; ++ig) {
			SensorCall();*histp = 0;
			SensorCall();for (ir = ptr->rmin; ir <= ptr->rmax; ++ir) {
				SensorCall();iptr = &histogram[ir][ig][ptr->bmin];
				SensorCall();for (ib = ptr->bmin; ib <= ptr->bmax; ++ib)
					{/*27*/SensorCall();*histp += *iptr++;/*28*/}
			}
			SensorCall();histp++;
	        }
	        SensorCall();first = ptr->gmin;
		last = ptr->gmax;
	        SensorCall();break;
	case BLUE:
	        SensorCall();histp = &hist2[ptr->bmin];
	        SensorCall();for (ib = ptr->bmin; ib <= ptr->bmax; ++ib) {
			SensorCall();*histp = 0;
			SensorCall();for (ir = ptr->rmin; ir <= ptr->rmax; ++ir) {
				SensorCall();iptr = &histogram[ir][ptr->gmin][ib];
				SensorCall();for (ig = ptr->gmin; ig <= ptr->gmax; ++ig) {
					SensorCall();*histp += *iptr;
					iptr += B_LEN;
				}
			}
			SensorCall();histp++;
	        }
	        SensorCall();first = ptr->bmin;
		last = ptr->bmax;
	        SensorCall();break;
	}
	/* find median point */
	SensorCall();sum2 = ptr->total / 2;
	histp = &hist2[first];
	sum = 0;
	SensorCall();for (i = first; i <= last && (sum += *histp++) < sum2; ++i)
		;
	SensorCall();if (i == first)
		{/*31*/SensorCall();i++;/*32*/}

	/* Create new box, re-allocate points */
	SensorCall();new = freeboxes;
	freeboxes = new->next;
	SensorCall();if (freeboxes)
		freeboxes->prev = NULL;
	SensorCall();if (usedboxes)
		{/*33*/SensorCall();usedboxes->prev = new;/*34*/}
	SensorCall();new->next = usedboxes;
	usedboxes = new;

	histp = &hist2[first];
	SensorCall();for (sum1 = 0, j = first; j < i; j++)
		{/*35*/SensorCall();sum1 += *histp++;/*36*/}
	SensorCall();for (sum2 = 0, j = i; j <= last; j++)
	    {/*37*/SensorCall();sum2 += *histp++;/*38*/}
	SensorCall();new->total = sum1;
	ptr->total = sum2;

	new->rmin = ptr->rmin;
	new->rmax = ptr->rmax;
	new->gmin = ptr->gmin;
	new->gmax = ptr->gmax;
	new->bmin = ptr->bmin;
	new->bmax = ptr->bmax;
	SensorCall();switch (axis) {
	case RED:
		SensorCall();new->rmax = i-1;
	        ptr->rmin = i;
	        SensorCall();break;
	case GREEN:
	        SensorCall();new->gmax = i-1;
	        ptr->gmin = i;
	        SensorCall();break;
	case BLUE:
	        SensorCall();new->bmax = i-1;
	        ptr->bmin = i;
	        SensorCall();break;
	}
	SensorCall();shrinkbox(new);
	shrinkbox(ptr);
SensorCall();}

static void
shrinkbox(Colorbox* box)
{
	SensorCall();register uint32 *histp;
	register int	ir, ig, ib;

	SensorCall();if (box->rmax > box->rmin) {
		SensorCall();for (ir = box->rmin; ir <= box->rmax; ++ir)
			{/*39*/SensorCall();for (ig = box->gmin; ig <= box->gmax; ++ig) {
				SensorCall();histp = &histogram[ir][ig][box->bmin];
			        SensorCall();for (ib = box->bmin; ib <= box->bmax; ++ib)
					{/*41*/SensorCall();if (*histp++ != 0) {
						SensorCall();box->rmin = ir;
						SensorCall();goto have_rmin;
					;/*42*/}}
			;/*40*/}}
	have_rmin:
		SensorCall();if (box->rmax > box->rmin)
			{/*43*/SensorCall();for (ir = box->rmax; ir >= box->rmin; --ir)
				{/*45*/SensorCall();for (ig = box->gmin; ig <= box->gmax; ++ig) {
					SensorCall();histp = &histogram[ir][ig][box->bmin];
					ib = box->bmin;
					SensorCall();for (; ib <= box->bmax; ++ib)
						{/*47*/SensorCall();if (*histp++ != 0) {
							SensorCall();box->rmax = ir;
							SensorCall();goto have_rmax;
						;/*48*/}}
			        ;/*46*/}/*44*/}}
	}
have_rmax:
	SensorCall();if (box->gmax > box->gmin) {
		SensorCall();for (ig = box->gmin; ig <= box->gmax; ++ig)
			{/*49*/SensorCall();for (ir = box->rmin; ir <= box->rmax; ++ir) {
				SensorCall();histp = &histogram[ir][ig][box->bmin];
			        SensorCall();for (ib = box->bmin; ib <= box->bmax; ++ib)
				{/*51*/SensorCall();if (*histp++ != 0) {
					SensorCall();box->gmin = ig;
					SensorCall();goto have_gmin;
				;/*52*/}}
			;/*50*/}}
	have_gmin:
		SensorCall();if (box->gmax > box->gmin)
			{/*53*/SensorCall();for (ig = box->gmax; ig >= box->gmin; --ig)
				{/*55*/SensorCall();for (ir = box->rmin; ir <= box->rmax; ++ir) {
					SensorCall();histp = &histogram[ir][ig][box->bmin];
					ib = box->bmin;
					SensorCall();for (; ib <= box->bmax; ++ib)
						{/*57*/SensorCall();if (*histp++ != 0) {
							SensorCall();box->gmax = ig;
							SensorCall();goto have_gmax;
						;/*58*/}}
			        ;/*56*/}/*54*/}}
	}
have_gmax:
	SensorCall();if (box->bmax > box->bmin) {
		SensorCall();for (ib = box->bmin; ib <= box->bmax; ++ib)
			{/*59*/SensorCall();for (ir = box->rmin; ir <= box->rmax; ++ir) {
				SensorCall();histp = &histogram[ir][box->gmin][ib];
			        SensorCall();for (ig = box->gmin; ig <= box->gmax; ++ig) {
					SensorCall();if (*histp != 0) {
						SensorCall();box->bmin = ib;
						SensorCall();goto have_bmin;
					}
					SensorCall();histp += B_LEN;
			        }
		        ;/*60*/}}
	have_bmin:
		SensorCall();if (box->bmax > box->bmin)
			{/*61*/SensorCall();for (ib = box->bmax; ib >= box->bmin; --ib)
				{/*63*/SensorCall();for (ir = box->rmin; ir <= box->rmax; ++ir) {
					SensorCall();histp = &histogram[ir][box->gmin][ib];
					ig = box->gmin;
					SensorCall();for (; ig <= box->gmax; ++ig) {
						SensorCall();if (*histp != 0) {
							SensorCall();box->bmax = ib;
							SensorCall();goto have_bmax;
						}
						SensorCall();histp += B_LEN;
					}
			        ;/*64*/}/*62*/}}
	}
have_bmax:
	;
SensorCall();}

static C_cell *
create_colorcell(int red, int green, int blue)
{
	SensorCall();register int ir, ig, ib, i;
	register C_cell *ptr;
	int mindist, next_n;
	register int tmp, dist, n;

	ir = red >> (COLOR_DEPTH-C_DEPTH);
	ig = green >> (COLOR_DEPTH-C_DEPTH);
	ib = blue >> (COLOR_DEPTH-C_DEPTH);
	ptr = (C_cell *)_TIFFmalloc(sizeof (C_cell));
	*(ColorCells + ir*C_LEN*C_LEN + ig*C_LEN + ib) = ptr;
	ptr->num_ents = 0;

	/*
	 * Step 1: find all colors inside this cell, while we're at
	 *	   it, find distance of centermost point to furthest corner
	 */
	mindist = 99999999;
	SensorCall();for (i = 0; i < num_colors; ++i) {
		SensorCall();if (rm[i]>>(COLOR_DEPTH-C_DEPTH) != ir  ||
		    gm[i]>>(COLOR_DEPTH-C_DEPTH) != ig  ||
		    bm[i]>>(COLOR_DEPTH-C_DEPTH) != ib)
			{/*117*/SensorCall();continue;/*118*/}
		SensorCall();ptr->entries[ptr->num_ents][0] = i;
		ptr->entries[ptr->num_ents][1] = 0;
		++ptr->num_ents;
	        tmp = rm[i] - red;
	        SensorCall();if (tmp < (MAX_COLOR/C_LEN/2))
			{/*119*/SensorCall();tmp = MAX_COLOR/C_LEN-1 - tmp;/*120*/}
	        SensorCall();dist = tmp*tmp;
	        tmp = gm[i] - green;
	        SensorCall();if (tmp < (MAX_COLOR/C_LEN/2))
			{/*121*/SensorCall();tmp = MAX_COLOR/C_LEN-1 - tmp;/*122*/}
	        SensorCall();dist += tmp*tmp;
	        tmp = bm[i] - blue;
	        SensorCall();if (tmp < (MAX_COLOR/C_LEN/2))
			{/*123*/SensorCall();tmp = MAX_COLOR/C_LEN-1 - tmp;/*124*/}
	        SensorCall();dist += tmp*tmp;
	        SensorCall();if (dist < mindist)
			{/*125*/SensorCall();mindist = dist;/*126*/}
	}

	/*
	 * Step 3: find all points within that distance to cell.
	 */
	SensorCall();for (i = 0; i < num_colors; ++i) {
		SensorCall();if (rm[i] >> (COLOR_DEPTH-C_DEPTH) == ir  &&
		    gm[i] >> (COLOR_DEPTH-C_DEPTH) == ig  &&
		    bm[i] >> (COLOR_DEPTH-C_DEPTH) == ib)
			{/*127*/SensorCall();continue;/*128*/}
		SensorCall();dist = 0;
	        SensorCall();if ((tmp = red - rm[i]) > 0 ||
		    (tmp = rm[i] - (red + MAX_COLOR/C_LEN-1)) > 0 )
			{/*129*/SensorCall();dist += tmp*tmp;/*130*/}
	        SensorCall();if ((tmp = green - gm[i]) > 0 ||
		    (tmp = gm[i] - (green + MAX_COLOR/C_LEN-1)) > 0 )
			{/*131*/SensorCall();dist += tmp*tmp;/*132*/}
	        SensorCall();if ((tmp = blue - bm[i]) > 0 ||
		    (tmp = bm[i] - (blue + MAX_COLOR/C_LEN-1)) > 0 )
			{/*133*/SensorCall();dist += tmp*tmp;/*134*/}
	        SensorCall();if (dist < mindist) {
			SensorCall();ptr->entries[ptr->num_ents][0] = i;
			ptr->entries[ptr->num_ents][1] = dist;
			++ptr->num_ents;
	        }
	}

	/*
	 * Sort color cells by distance, use cheap exchange sort
	 */
	SensorCall();for (n = ptr->num_ents - 1; n > 0; n = next_n) {
		SensorCall();next_n = 0;
		SensorCall();for (i = 0; i < n; ++i)
			{/*135*/SensorCall();if (ptr->entries[i][1] > ptr->entries[i+1][1]) {
				SensorCall();tmp = ptr->entries[i][0];
				ptr->entries[i][0] = ptr->entries[i+1][0];
				ptr->entries[i+1][0] = tmp;
				tmp = ptr->entries[i][1];
				ptr->entries[i][1] = ptr->entries[i+1][1];
				ptr->entries[i+1][1] = tmp;
				next_n = i;
		        ;/*136*/}}
	}
	{C_cell * ReplaceReturn = (ptr); SensorCall(); return ReplaceReturn;}
}

static void
map_colortable(void)
{
	SensorCall();register uint32 *histp = &histogram[0][0][0];
	register C_cell *cell;
	register int j, tmp, d2, dist;
	int ir, ig, ib, i;

	SensorCall();for (ir = 0; ir < B_LEN; ++ir)
		{/*65*/SensorCall();for (ig = 0; ig < B_LEN; ++ig)
			{/*67*/SensorCall();for (ib = 0; ib < B_LEN; ++ib, histp++) {
				SensorCall();if (*histp == 0) {
					SensorCall();*histp = -1;
					SensorCall();continue;
				}
				SensorCall();cell = *(ColorCells +
				    (((ir>>(B_DEPTH-C_DEPTH)) << C_DEPTH*2) +
				    ((ig>>(B_DEPTH-C_DEPTH)) << C_DEPTH) +
				    (ib>>(B_DEPTH-C_DEPTH))));
				SensorCall();if (cell == NULL )
					{/*69*/SensorCall();cell = create_colorcell(
					    ir << COLOR_SHIFT,
					    ig << COLOR_SHIFT,
					    ib << COLOR_SHIFT);/*70*/}
				SensorCall();dist = 9999999;
				SensorCall();for (i = 0; i < cell->num_ents &&
				    dist > cell->entries[i][1]; ++i) {
					SensorCall();j = cell->entries[i][0];
					d2 = rm[j] - (ir << COLOR_SHIFT);
					d2 *= d2;
					tmp = gm[j] - (ig << COLOR_SHIFT);
					d2 += tmp*tmp;
					tmp = bm[j] - (ib << COLOR_SHIFT);
					d2 += tmp*tmp;
					SensorCall();if (d2 < dist) {
						SensorCall();dist = d2;
						*histp = j;
					}
				}
			;/*68*/}/*66*/}}
SensorCall();}

/*
 * straight quantization.  Each pixel is mapped to the colors
 * closest to it.  Color values are rounded to the nearest color
 * table entry.
 */
static void
quant(TIFF* in, TIFF* out)
{
	SensorCall();unsigned char	*outline, *inputline;
	register unsigned char	*outptr, *inptr;
	register uint32 i, j;
	register int red, green, blue;

	inputline = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(in));
	outline = (unsigned char *)_TIFFmalloc(imagewidth);
	SensorCall();for (i = 0; i < imagelength; i++) {
		SensorCall();if (TIFFReadScanline(in, inputline, i, 0) <= 0)
			{/*71*/SensorCall();break;/*72*/}
		SensorCall();inptr = inputline;
		outptr = outline;
		SensorCall();for (j = 0; j < imagewidth; j++) {
			SensorCall();red = *inptr++ >> COLOR_SHIFT;
			green = *inptr++ >> COLOR_SHIFT;
			blue = *inptr++ >> COLOR_SHIFT;
			*outptr++ = (unsigned char)histogram[red][green][blue];
		}
		SensorCall();if (TIFFWriteScanline(out, outline, i, 0) < 0)
			{/*73*/SensorCall();break;/*74*/}
	}
	SensorCall();_TIFFfree(inputline);
	_TIFFfree(outline);
SensorCall();}

#define	SWAP(type,a,b)	{ type p; p = a; a = b; b = p; }

#define	GetInputLine(tif, row, bad)				\
	if (TIFFReadScanline(tif, inputline, row, 0) <= 0)	\
		bad;						\
	inptr = inputline;					\
	nextptr = nextline;					\
	for (j = 0; j < imagewidth; ++j) {			\
		*nextptr++ = *inptr++;				\
		*nextptr++ = *inptr++;				\
		*nextptr++ = *inptr++;				\
	}
#define	GetComponent(raw, cshift, c)				\
	cshift = raw;						\
	if (cshift < 0)						\
		cshift = 0;					\
	else if (cshift >= MAX_COLOR)				\
		cshift = MAX_COLOR-1;				\
	c = cshift;						\
	cshift >>= COLOR_SHIFT;

static void
quant_fsdither(TIFF* in, TIFF* out)
{
	SensorCall();unsigned char *outline, *inputline, *inptr;
	short *thisline, *nextline;
	register unsigned char	*outptr;
	register short *thisptr, *nextptr;
	register uint32 i, j;
	uint32 imax, jmax;
	int lastline, lastpixel;

	imax = imagelength - 1;
	jmax = imagewidth - 1;
	inputline = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(in));
	thisline = (short *)_TIFFmalloc(imagewidth * 3 * sizeof (short));
	nextline = (short *)_TIFFmalloc(imagewidth * 3 * sizeof (short));
	outline = (unsigned char *) _TIFFmalloc(TIFFScanlineSize(out));

	GetInputLine(in, 0, goto bad);		/* get first line */
	SensorCall();for (i = 1; i <= imagelength; ++i) {
SensorCall();		SWAP(short *, thisline, nextline);
		lastline = (i >= imax);
		SensorCall();if (i <= imax)
			GetInputLine(in, i, break);
		SensorCall();thisptr = thisline;
		nextptr = nextline;
		outptr = outline;
		SensorCall();for (j = 0; j < imagewidth; ++j) {
			SensorCall();int red, green, blue;
			register int oval, r2, g2, b2;

			lastpixel = (j == jmax);
SensorCall();			GetComponent(*thisptr++, r2, red);
SensorCall();			GetComponent(*thisptr++, g2, green);
SensorCall();			GetComponent(*thisptr++, b2, blue);
			oval = histogram[r2][g2][b2];
			SensorCall();if (oval == -1) {
				SensorCall();int ci;
				register int cj, tmp, d2, dist;
				register C_cell	*cell;

				cell = *(ColorCells +
				    (((r2>>(B_DEPTH-C_DEPTH)) << C_DEPTH*2) +
				    ((g2>>(B_DEPTH-C_DEPTH)) << C_DEPTH ) +
				    (b2>>(B_DEPTH-C_DEPTH))));
				SensorCall();if (cell == NULL)
					{/*75*/SensorCall();cell = create_colorcell(red,
					    green, blue);/*76*/}
				SensorCall();dist = 9999999;
				SensorCall();for (ci = 0; ci < cell->num_ents && dist > cell->entries[ci][1]; ++ci) {
					SensorCall();cj = cell->entries[ci][0];
					d2 = (rm[cj] >> COLOR_SHIFT) - r2;
					d2 *= d2;
					tmp = (gm[cj] >> COLOR_SHIFT) - g2;
					d2 += tmp*tmp;
					tmp = (bm[cj] >> COLOR_SHIFT) - b2;
					d2 += tmp*tmp;
					SensorCall();if (d2 < dist) {
						SensorCall();dist = d2;
						oval = cj;
					}
				}
				SensorCall();histogram[r2][g2][b2] = oval;
			}
			SensorCall();*outptr++ = oval;
			red -= rm[oval];
			green -= gm[oval];
			blue -= bm[oval];
			SensorCall();if (!lastpixel) {
				SensorCall();thisptr[0] += blue * 7 / 16;
				thisptr[1] += green * 7 / 16;
				thisptr[2] += red * 7 / 16;
			}
			SensorCall();if (!lastline) {
				SensorCall();if (j != 0) {
					SensorCall();nextptr[-3] += blue * 3 / 16;
					nextptr[-2] += green * 3 / 16;
					nextptr[-1] += red * 3 / 16;
				}
				SensorCall();nextptr[0] += blue * 5 / 16;
				nextptr[1] += green * 5 / 16;
				nextptr[2] += red * 5 / 16;
				SensorCall();if (!lastpixel) {
					SensorCall();nextptr[3] += blue / 16;
				        nextptr[4] += green / 16;
				        nextptr[5] += red / 16;
				}
				SensorCall();nextptr += 3;
			}
		}
		SensorCall();if (TIFFWriteScanline(out, outline, i-1, 0) < 0)
			{/*77*/SensorCall();break;/*78*/}
	}
bad:
	SensorCall();_TIFFfree(inputline);
	_TIFFfree(thisline);
	_TIFFfree(nextline);
	_TIFFfree(outline);
SensorCall();}
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
