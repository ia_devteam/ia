/* $Id: tiff2bw.c,v 1.15 2010-07-02 12:02:56 dron Exp $ */

/*
 * Copyright (c) 1988-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

#include "tif_config.h"
#include "/var/tmp/sensor.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifdef NEED_LIBPORT
# include "libport.h"
#endif

#include "tiffio.h"

#define	streq(a,b)	(strcmp((a),(b)) == 0)
#define	strneq(a,b,n)	(strncmp(a,b,n) == 0)

/* x% weighting -> fraction of full color */
#define	PCT(x)	(((x)*255+127)/100)
int	RED = PCT(30);		/* 30% */
int	GREEN = PCT(59);	/* 59% */
int	BLUE = PCT(11);		/* 11% */

static	void usage(void);
static	int processCompressOptions(char*);

static void
compresscontig(unsigned char* out, unsigned char* rgb, uint32 n)
{
	SensorCall();register int v, red = RED, green = GREEN, blue = BLUE;

	SensorCall();while (n-- > 0) {
		SensorCall();v = red*(*rgb++);
		v += green*(*rgb++);
		v += blue*(*rgb++);
		*out++ = v>>8;
	}
SensorCall();}

static void
compresssep(unsigned char* out,
	    unsigned char* r, unsigned char* g, unsigned char* b, uint32 n)
{
	SensorCall();register uint32 red = RED, green = GREEN, blue = BLUE;

	SensorCall();while (n-- > 0)
		{/*23*/SensorCall();*out++ = (unsigned char)
			((red*(*r++) + green*(*g++) + blue*(*b++)) >> 8);/*24*/}
SensorCall();}

static int
checkcmap(TIFF* tif, int n, uint16* r, uint16* g, uint16* b)
{
	SensorCall();while (n-- > 0)
		{/*25*/SensorCall();if (*r++ >= 256 || *g++ >= 256 || *b++ >= 256)
			{/*27*/{int  ReplaceReturn = (16); SensorCall(); return ReplaceReturn;}/*28*/}/*26*/}
	SensorCall();TIFFWarning(TIFFFileName(tif), "Assuming 8-bit colormap");
	{int  ReplaceReturn = (8); SensorCall(); return ReplaceReturn;}
}

static void
compresspalette(unsigned char* out, unsigned char* data, uint32 n, uint16* rmap, uint16* gmap, uint16* bmap)
{
	SensorCall();register int v, red = RED, green = GREEN, blue = BLUE;

	SensorCall();while (n-- > 0) {
		SensorCall();unsigned int ix = *data++;
		v = red*rmap[ix];
		v += green*gmap[ix];
		v += blue*bmap[ix];
		*out++ = v>>8;
	}
SensorCall();}

static	uint16 compression = (uint16) -1;
static	uint16 predictor = 0;
static	int jpegcolormode = JPEGCOLORMODE_RGB;
static	int quality = 75;		/* JPEG quality */

static	void cpTags(TIFF* in, TIFF* out);

int
main(int argc, char* argv[])
{
	SensorCall();uint32 rowsperstrip = (uint32) -1;
	TIFF *in, *out;
	uint32 w, h;
	uint16 samplesperpixel;
	uint16 bitspersample;
	uint16 config;
	uint16 photometric;
	uint16* red;
	uint16* green;
	uint16* blue;
	tsize_t rowsize;
	register uint32 row;
	register tsample_t s;
	unsigned char *inbuf, *outbuf;
	char thing[1024];
	int c;
	extern int optind;
	extern char *optarg;

	SensorCall();while ((c = getopt(argc, argv, "c:r:R:G:B:")) != -1)
		{/*31*/SensorCall();switch (c) {
		case 'c':		/* compression scheme */
			SensorCall();if (!processCompressOptions(optarg))
				{/*33*/SensorCall();usage();/*34*/}
			SensorCall();break;
		case 'r':		/* rows/strip */
			SensorCall();rowsperstrip = atoi(optarg);
			SensorCall();break;
		case 'R':
			SensorCall();RED = PCT(atoi(optarg));
			SensorCall();break;
		case 'G':
			SensorCall();GREEN = PCT(atoi(optarg));
			SensorCall();break;
		case 'B':
			SensorCall();BLUE = PCT(atoi(optarg));
			SensorCall();break;
		case '?':
			SensorCall();usage();
			/*NOTREACHED*/
		;/*32*/}}
	SensorCall();if (argc - optind < 2)
		{/*35*/SensorCall();usage();/*36*/}
	SensorCall();in = TIFFOpen(argv[optind], "r");
	SensorCall();if (in == NULL)
		{/*37*/{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}/*38*/}
	SensorCall();photometric = 0;
	TIFFGetField(in, TIFFTAG_PHOTOMETRIC, &photometric);
	SensorCall();if (photometric != PHOTOMETRIC_RGB && photometric != PHOTOMETRIC_PALETTE ) {
		SensorCall();fprintf(stderr,
	    "%s: Bad photometric; can only handle RGB and Palette images.\n",
		    argv[optind]);
		{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();TIFFGetField(in, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
	SensorCall();if (samplesperpixel != 1 && samplesperpixel != 3) {
		SensorCall();fprintf(stderr, "%s: Bad samples/pixel %u.\n",
		    argv[optind], samplesperpixel);
		{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();if( photometric == PHOTOMETRIC_RGB && samplesperpixel != 3) {
		SensorCall();fprintf(stderr, "%s: Bad samples/pixel %u for PHOTOMETRIC_RGB.\n",
		    argv[optind], samplesperpixel);
		{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();TIFFGetField(in, TIFFTAG_BITSPERSAMPLE, &bitspersample);
	SensorCall();if (bitspersample != 8) {
		SensorCall();fprintf(stderr,
		    " %s: Sorry, only handle 8-bit samples.\n", argv[optind]);
		{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();TIFFGetField(in, TIFFTAG_IMAGEWIDTH, &w);
	TIFFGetField(in, TIFFTAG_IMAGELENGTH, &h);
	TIFFGetField(in, TIFFTAG_PLANARCONFIG, &config);

	out = TIFFOpen(argv[optind+1], "w");
	SensorCall();if (out == NULL)
		{/*39*/{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}/*40*/}
	SensorCall();TIFFSetField(out, TIFFTAG_IMAGEWIDTH, w);
	TIFFSetField(out, TIFFTAG_IMAGELENGTH, h);
	TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, 8);
	TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, 1);
	TIFFSetField(out, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
	cpTags(in, out);
	SensorCall();if (compression != (uint16) -1) {
		SensorCall();TIFFSetField(out, TIFFTAG_COMPRESSION, compression);
		SensorCall();switch (compression) {
		case COMPRESSION_JPEG:
			SensorCall();TIFFSetField(out, TIFFTAG_JPEGQUALITY, quality);
			TIFFSetField(out, TIFFTAG_JPEGCOLORMODE, jpegcolormode);
			SensorCall();break;
		case COMPRESSION_LZW:
		case COMPRESSION_DEFLATE:
			SensorCall();if (predictor != 0)
				{/*41*/SensorCall();TIFFSetField(out, TIFFTAG_PREDICTOR, predictor);/*42*/}
			SensorCall();break;
		}
	}
	SensorCall();TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
	snprintf(thing, sizeof(thing), "B&W version of %s", argv[optind]);
	TIFFSetField(out, TIFFTAG_IMAGEDESCRIPTION, thing);
	TIFFSetField(out, TIFFTAG_SOFTWARE, "tiff2bw");
	outbuf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(out));
	TIFFSetField(out, TIFFTAG_ROWSPERSTRIP,
	    TIFFDefaultStripSize(out, rowsperstrip));

#define	pack(a,b)	((a)<<8 | (b))
	SensorCall();switch (pack(photometric, config)) {
	case pack(PHOTOMETRIC_PALETTE, PLANARCONFIG_CONTIG):
	case pack(PHOTOMETRIC_PALETTE, PLANARCONFIG_SEPARATE):
		SensorCall();TIFFGetField(in, TIFFTAG_COLORMAP, &red, &green, &blue);
		/*
		 * Convert 16-bit colormap to 8-bit (unless it looks
		 * like an old-style 8-bit colormap).
		 */
		SensorCall();if (checkcmap(in, 1<<bitspersample, red, green, blue) == 16) {
			SensorCall();int i;
#define	CVT(x)		(((x) * 255L) / ((1L<<16)-1))
			SensorCall();for (i = (1<<bitspersample)-1; i >= 0; i--) {
				SensorCall();red[i] = CVT(red[i]);
				green[i] = CVT(green[i]);
				blue[i] = CVT(blue[i]);
			}
#undef CVT
		}
		SensorCall();inbuf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(in));
		SensorCall();for (row = 0; row < h; row++) {
			SensorCall();if (TIFFReadScanline(in, inbuf, row, 0) < 0)
				{/*43*/SensorCall();break;/*44*/}
			SensorCall();compresspalette(outbuf, inbuf, w, red, green, blue);
			SensorCall();if (TIFFWriteScanline(out, outbuf, row, 0) < 0)
				{/*45*/SensorCall();break;/*46*/}
		}
		SensorCall();break;
	case pack(PHOTOMETRIC_RGB, PLANARCONFIG_CONTIG):
		SensorCall();inbuf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(in));
		SensorCall();for (row = 0; row < h; row++) {
			SensorCall();if (TIFFReadScanline(in, inbuf, row, 0) < 0)
				{/*47*/SensorCall();break;/*48*/}
			SensorCall();compresscontig(outbuf, inbuf, w);
			SensorCall();if (TIFFWriteScanline(out, outbuf, row, 0) < 0)
				{/*49*/SensorCall();break;/*50*/}
		}
		SensorCall();break;
	case pack(PHOTOMETRIC_RGB, PLANARCONFIG_SEPARATE):
		SensorCall();rowsize = TIFFScanlineSize(in);
		inbuf = (unsigned char *)_TIFFmalloc(3*rowsize);
		SensorCall();for (row = 0; row < h; row++) {
			SensorCall();for (s = 0; s < 3; s++)
				{/*51*/SensorCall();if (TIFFReadScanline(in,
				    inbuf+s*rowsize, row, s) < 0)
					 {/*53*/{int  ReplaceReturn = (-1); SensorCall(); return ReplaceReturn;}/*54*/}/*52*/}
			SensorCall();compresssep(outbuf,
			    inbuf, inbuf+rowsize, inbuf+2*rowsize, w);
			SensorCall();if (TIFFWriteScanline(out, outbuf, row, 0) < 0)
				{/*55*/SensorCall();break;/*56*/}
		}
		SensorCall();break;
	}
#undef pack
	SensorCall();TIFFClose(out);
	{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
}

static int
processCompressOptions(char* opt)
{
	SensorCall();if (streq(opt, "none"))
		compression = COMPRESSION_NONE;
	else {/*3*/SensorCall();if (streq(opt, "packbits"))
		compression = COMPRESSION_PACKBITS;
	else {/*5*/SensorCall();if (strneq(opt, "jpeg", 4)) {
		SensorCall();char* cp = strchr(opt, ':');

                compression = COMPRESSION_JPEG;
                SensorCall();while( cp )
                {
                    SensorCall();if (isdigit((int)cp[1]))
			{/*7*/SensorCall();quality = atoi(cp+1);/*8*/}
                    else {/*9*/SensorCall();if (cp[1] == 'r' )
			jpegcolormode = JPEGCOLORMODE_RAW;
                    else
                        {/*11*/SensorCall();usage();/*12*/}/*10*/}

                    SensorCall();cp = strchr(cp+1,':');
                }
	} else {/*13*/SensorCall();if (strneq(opt, "lzw", 3)) {
		SensorCall();char* cp = strchr(opt, ':');
		SensorCall();if (cp)
			{/*15*/SensorCall();predictor = atoi(cp+1);/*16*/}
		SensorCall();compression = COMPRESSION_LZW;
	} else {/*17*/SensorCall();if (strneq(opt, "zip", 3)) {
		SensorCall();char* cp = strchr(opt, ':');
		SensorCall();if (cp)
			{/*19*/SensorCall();predictor = atoi(cp+1);/*20*/}
		SensorCall();compression = COMPRESSION_DEFLATE;
	} else
		{/*21*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*22*/}/*18*/}/*14*/}/*6*/}/*4*/}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

#define	CopyField(tag, v) \
    if (TIFFGetField(in, tag, &v)) TIFFSetField(out, tag, v)
#define	CopyField2(tag, v1, v2) \
    if (TIFFGetField(in, tag, &v1, &v2)) TIFFSetField(out, tag, v1, v2)
#define	CopyField3(tag, v1, v2, v3) \
    if (TIFFGetField(in, tag, &v1, &v2, &v3)) TIFFSetField(out, tag, v1, v2, v3)
#define	CopyField4(tag, v1, v2, v3, v4) \
    if (TIFFGetField(in, tag, &v1, &v2, &v3, &v4)) TIFFSetField(out, tag, v1, v2, v3, v4)

static void
cpTag(TIFF* in, TIFF* out, uint16 tag, uint16 count, TIFFDataType type)
{
	SensorCall();switch (type) {
	case TIFF_SHORT:
		SensorCall();if (count == 1) {
			SensorCall();uint16 shortv;
			CopyField(tag, shortv);
		} else {/*57*/SensorCall();if (count == 2) {
			SensorCall();uint16 shortv1, shortv2;
			CopyField2(tag, shortv1, shortv2);
		} else {/*59*/SensorCall();if (count == 4) {
			SensorCall();uint16 *tr, *tg, *tb, *ta;
			CopyField4(tag, tr, tg, tb, ta);
		} else {/*61*/SensorCall();if (count == (uint16) -1) {
			SensorCall();uint16 shortv1;
			uint16* shortav;
			CopyField2(tag, shortv1, shortav);
		;/*62*/}/*60*/}/*58*/}}
		SensorCall();break;
	case TIFF_LONG:
		{ SensorCall();uint32 longv;
		  CopyField(tag, longv);
		}
		SensorCall();break;
	case TIFF_RATIONAL:
		SensorCall();if (count == 1) {
			SensorCall();float floatv;
			CopyField(tag, floatv);
		} else {/*63*/SensorCall();if (count == (uint16) -1) {
			SensorCall();float* floatav;
			CopyField(tag, floatav);
		;/*64*/}}
		SensorCall();break;
	case TIFF_ASCII:
		{ SensorCall();char* stringv;
		  CopyField(tag, stringv);
		}
		SensorCall();break;
	case TIFF_DOUBLE:
		SensorCall();if (count == 1) {
			SensorCall();double doublev;
			CopyField(tag, doublev);
		} else {/*65*/SensorCall();if (count == (uint16) -1) {
			SensorCall();double* doubleav;
			CopyField(tag, doubleav);
		;/*66*/}}
		SensorCall();break;
          default:
                SensorCall();TIFFError(TIFFFileName(in),
                          "Data type %d is not supported, tag %d skipped.",
                          tag, type);
	}
SensorCall();}

#undef CopyField4
#undef CopyField3
#undef CopyField2
#undef CopyField

static struct cpTag {
	uint16	tag;
	uint16	count;
	TIFFDataType type;
} tags[] = {
	{ TIFFTAG_SUBFILETYPE,		1, TIFF_LONG },
	{ TIFFTAG_THRESHHOLDING,	1, TIFF_SHORT },
	{ TIFFTAG_DOCUMENTNAME,		1, TIFF_ASCII },
	{ TIFFTAG_IMAGEDESCRIPTION,	1, TIFF_ASCII },
	{ TIFFTAG_MAKE,			1, TIFF_ASCII },
	{ TIFFTAG_MODEL,		1, TIFF_ASCII },
	{ TIFFTAG_MINSAMPLEVALUE,	1, TIFF_SHORT },
	{ TIFFTAG_MAXSAMPLEVALUE,	1, TIFF_SHORT },
	{ TIFFTAG_XRESOLUTION,		1, TIFF_RATIONAL },
	{ TIFFTAG_YRESOLUTION,		1, TIFF_RATIONAL },
	{ TIFFTAG_PAGENAME,		1, TIFF_ASCII },
	{ TIFFTAG_XPOSITION,		1, TIFF_RATIONAL },
	{ TIFFTAG_YPOSITION,		1, TIFF_RATIONAL },
	{ TIFFTAG_RESOLUTIONUNIT,	1, TIFF_SHORT },
	{ TIFFTAG_SOFTWARE,		1, TIFF_ASCII },
	{ TIFFTAG_DATETIME,		1, TIFF_ASCII },
	{ TIFFTAG_ARTIST,		1, TIFF_ASCII },
	{ TIFFTAG_HOSTCOMPUTER,		1, TIFF_ASCII },
	{ TIFFTAG_WHITEPOINT,		2, TIFF_RATIONAL },
	{ TIFFTAG_PRIMARYCHROMATICITIES,(uint16) -1,TIFF_RATIONAL },
	{ TIFFTAG_HALFTONEHINTS,	2, TIFF_SHORT },
	{ TIFFTAG_INKSET,		1, TIFF_SHORT },
	{ TIFFTAG_DOTRANGE,		2, TIFF_SHORT },
	{ TIFFTAG_TARGETPRINTER,	1, TIFF_ASCII },
	{ TIFFTAG_SAMPLEFORMAT,		1, TIFF_SHORT },
	{ TIFFTAG_YCBCRCOEFFICIENTS,	(uint16) -1,TIFF_RATIONAL },
	{ TIFFTAG_YCBCRSUBSAMPLING,	2, TIFF_SHORT },
	{ TIFFTAG_YCBCRPOSITIONING,	1, TIFF_SHORT },
	{ TIFFTAG_REFERENCEBLACKWHITE,	(uint16) -1,TIFF_RATIONAL },
	{ TIFFTAG_EXTRASAMPLES,		(uint16) -1, TIFF_SHORT },
	{ TIFFTAG_SMINSAMPLEVALUE,	1, TIFF_DOUBLE },
	{ TIFFTAG_SMAXSAMPLEVALUE,	1, TIFF_DOUBLE },
	{ TIFFTAG_STONITS,		1, TIFF_DOUBLE },
};
#define	NTAGS	(sizeof (tags) / sizeof (tags[0]))

static void
cpTags(TIFF* in, TIFF* out)
{
    SensorCall();struct cpTag *p;
    SensorCall();for (p = tags; p < &tags[NTAGS]; p++)
	{/*29*/SensorCall();cpTag(in, out, p->tag, p->count, p->type);/*30*/}
SensorCall();}
#undef NTAGS

char* stuff[] = {
"usage: tiff2bw [options] input.tif output.tif",
"where options are:",
" -R %		use #% from red channel",
" -G %		use #% from green channel",
" -B %		use #% from blue channel",
"",
" -r #		make each strip have no more than # rows",
"",
" -c lzw[:opts]	compress output with Lempel-Ziv & Welch encoding",
" -c zip[:opts]	compress output with deflate encoding",
" -c packbits	compress output with packbits encoding",
" -c g3[:opts]	compress output with CCITT Group 3 encoding",
" -c g4		compress output with CCITT Group 4 encoding",
" -c none	use no compression algorithm on output",
"",
"LZW and deflate options:",
" #		set predictor value",
"For example, -c lzw:2 to get LZW-encoded data with horizontal differencing",
NULL
};

static void
usage(void)
{
	SensorCall();char buf[BUFSIZ];
	int i;

	setbuf(stderr, buf);
        fprintf(stderr, "%s\n\n", TIFFGetVersion());
	SensorCall();for (i = 0; stuff[i] != NULL; i++)
		{/*1*/SensorCall();fprintf(stderr, "%s\n", stuff[i]);/*2*/}
	SensorCall();exit(-1);
SensorCall();}

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
