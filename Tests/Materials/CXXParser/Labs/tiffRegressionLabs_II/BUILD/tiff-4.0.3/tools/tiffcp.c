/* $Id: tiffcp.c,v 1.49 2010-12-23 13:38:47 dron Exp $ */

/*
 * Copyright (c) 1988-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 *  Revised:  2/18/01 BAR -- added syntax for extracting single images from
 *                          multi-image TIFF files.
 *
 *    New syntax is:  sourceFileName,image#
 *
 * image# ranges from 0..<n-1> where n is the # of images in the file.
 * There may be no white space between the comma and the filename or
 * image number.
 *
 *    Example:   tiffcp source.tif,1 destination.tif
 *
 * Copies the 2nd image in source.tif to the destination.
 *
 *****
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 * OF THIS SOFTWARE.
 */

#include "tif_config.h"
#include "/var/tmp/sensor.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ctype.h>
#include <assert.h>

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#include "tiffio.h"

#ifndef HAVE_GETOPT
extern int getopt(int, char**, char*);
#endif

#if defined(VMS)
# define unlink delete
#endif

#define	streq(a,b)	(strcmp(a,b) == 0)
#define	strneq(a,b,n)	(strncmp(a,b,n) == 0)

#define	TRUE	1
#define	FALSE	0

static int outtiled = -1;
static uint32 tilewidth;
static uint32 tilelength;

static uint16 config;
static uint16 compression;
static uint16 predictor;
static int preset;
static uint16 fillorder;
static uint16 orientation;
static uint32 rowsperstrip;
static uint32 g3opts;
static int ignore = FALSE;		/* if true, ignore read errors */
static uint32 defg3opts = (uint32) -1;
static int quality = 75;		/* JPEG quality */
static int jpegcolormode = JPEGCOLORMODE_RGB;
static uint16 defcompression = (uint16) -1;
static uint16 defpredictor = (uint16) -1;
static int defpreset =  -1;

static int tiffcp(TIFF*, TIFF*);
static int processCompressOptions(char*);
static void usage(void);

static char comma = ',';  /* (default) comma separator character */
static TIFF* bias = NULL;
static int pageNum = 0;
static int pageInSeq = 0;

static int nextSrcImage (TIFF *tif, char **imageSpec)
/*
  seek to the next image specified in *imageSpec
  returns 1 if success, 0 if no more images to process
  *imageSpec=NULL if subsequent images should be processed in sequence
*/
{
	SensorCall();if (**imageSpec == comma) {  /* if not @comma, we've done all images */
		SensorCall();char *start = *imageSpec + 1;
		tdir_t nextImage = (tdir_t)strtol(start, imageSpec, 0);
		SensorCall();if (start == *imageSpec) {/*79*/SensorCall();nextImage = TIFFCurrentDirectory (tif);/*80*/}
		SensorCall();if (**imageSpec)
		{
			SensorCall();if (**imageSpec == comma) {
				/* a trailing comma denotes remaining images in sequence */
				SensorCall();if ((*imageSpec)[1] == '\0') *imageSpec = NULL;
			}else{
				SensorCall();fprintf (stderr,
				    "Expected a %c separated image # list after %s\n",
				    comma, TIFFFileName (tif));
				exit (-4);   /* syntax error */
			}
		}
		SensorCall();if (TIFFSetDirectory (tif, nextImage)) {/*81*/{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}/*82*/}
		SensorCall();fprintf (stderr, "%s%c%d not found!\n",
		    TIFFFileName(tif), comma, (int) nextImage);
	}
	{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
}

  
static TIFF* openSrcImage (char **imageSpec)
/*
  imageSpec points to a pointer to a filename followed by optional ,image#'s
  Open the TIFF file and assign *imageSpec to either NULL if there are
  no images specified, or a pointer to the next image number text
*/
{
	SensorCall();TIFF *tif;
	char *fn = *imageSpec;
	*imageSpec = strchr (fn, comma);
	SensorCall();if (*imageSpec) {  /* there is at least one image number specifier */
		SensorCall();**imageSpec = '\0';
		tif = TIFFOpen (fn, "r");
		/* but, ignore any single trailing comma */
		SensorCall();if (!(*imageSpec)[1]) {SensorCall();*imageSpec = NULL; {TIFF * ReplaceReturn = tif; SensorCall(); return ReplaceReturn;}}
		SensorCall();if (tif) {
			SensorCall();**imageSpec = comma;  /* replace the comma */
			SensorCall();if (!nextSrcImage(tif, imageSpec)) {
				SensorCall();TIFFClose (tif);
				tif = NULL;
			}
		}
	}else
		{/*83*/SensorCall();tif = TIFFOpen (fn, "r");/*84*/}
	{TIFF * ReplaceReturn = tif; SensorCall(); return ReplaceReturn;}
}

int
main(int argc, char* argv[])
{
	SensorCall();uint16 defconfig = (uint16) -1;
	uint16 deffillorder = 0;
	uint32 deftilewidth = (uint32) -1;
	uint32 deftilelength = (uint32) -1;
	uint32 defrowsperstrip = (uint32) 0;
	uint64 diroff = 0;
	TIFF* in;
	TIFF* out;
	char mode[10];
	char* mp = mode;
	int c;
	extern int optind;
	extern char* optarg;

	*mp++ = 'w';
	*mp = '\0';
	SensorCall();while ((c = getopt(argc, argv, ",:b:c:f:l:o:z:p:r:w:aistBLMC8x")) != -1)
		{/*85*/SensorCall();switch (c) {
		case ',':
			SensorCall();if (optarg[0] != '=') {/*87*/SensorCall();usage();/*88*/}
			SensorCall();comma = optarg[1];
			SensorCall();break;
		case 'b':   /* this file is bias image subtracted from others */
			SensorCall();if (bias) {
				SensorCall();fputs ("Only 1 bias image may be specified\n", stderr);
				exit (-2);
			}
			{
				SensorCall();uint16 samples = (uint16) -1;
				char **biasFn = &optarg;
				bias = openSrcImage (biasFn);
				SensorCall();if (!bias) {/*89*/SensorCall();exit (-5);/*90*/}
				SensorCall();if (TIFFIsTiled (bias)) {
					SensorCall();fputs ("Bias image must be organized in strips\n", stderr);
					exit (-7);
				}
				SensorCall();TIFFGetField(bias, TIFFTAG_SAMPLESPERPIXEL, &samples);
				SensorCall();if (samples != 1) {
					SensorCall();fputs ("Bias image must be monochrome\n", stderr);
					exit (-7);
				}
			}
			SensorCall();break;
		case 'a':   /* append to output */
			SensorCall();mode[0] = 'a';
			SensorCall();break;
		case 'c':   /* compression scheme */
			SensorCall();if (!processCompressOptions(optarg))
				{/*91*/SensorCall();usage();/*92*/}
			SensorCall();break;
		case 'f':   /* fill order */
			SensorCall();if (streq(optarg, "lsb2msb"))
				deffillorder = FILLORDER_LSB2MSB;
			else {/*93*/SensorCall();if (streq(optarg, "msb2lsb"))
				deffillorder = FILLORDER_MSB2LSB;
			else
				{/*95*/SensorCall();usage();/*96*/}/*94*/}
			SensorCall();break;
		case 'i':   /* ignore errors */
			SensorCall();ignore = TRUE;
			SensorCall();break;
		case 'l':   /* tile length */
			SensorCall();outtiled = TRUE;
			deftilelength = atoi(optarg);
			SensorCall();break;
		case 'o':   /* initial directory offset */
			SensorCall();diroff = strtoul(optarg, NULL, 0);
			SensorCall();break;
		case 'p':   /* planar configuration */
			SensorCall();if (streq(optarg, "separate"))
				defconfig = PLANARCONFIG_SEPARATE;
			else {/*97*/SensorCall();if (streq(optarg, "contig"))
				defconfig = PLANARCONFIG_CONTIG;
			else
				{/*99*/SensorCall();usage();/*100*/}/*98*/}
			SensorCall();break;
		case 'r':   /* rows/strip */
			SensorCall();defrowsperstrip = atol(optarg);
			SensorCall();break;
		case 's':   /* generate stripped output */
			SensorCall();outtiled = FALSE;
			SensorCall();break;
		case 't':   /* generate tiled output */
			SensorCall();outtiled = TRUE;
			SensorCall();break;
		case 'w':   /* tile width */
			SensorCall();outtiled = TRUE;
			deftilewidth = atoi(optarg);
			SensorCall();break;
		case 'B':
			SensorCall();*mp++ = 'b'; *mp = '\0';
			SensorCall();break;
		case 'L':
			SensorCall();*mp++ = 'l'; *mp = '\0';
			SensorCall();break;
		case 'M':
			SensorCall();*mp++ = 'm'; *mp = '\0';
			SensorCall();break;
		case 'C':
			SensorCall();*mp++ = 'c'; *mp = '\0';
			SensorCall();break;
		case '8':
			SensorCall();*mp++ = '8'; *mp = '\0';
			SensorCall();break;
		case 'x':
			SensorCall();pageInSeq = 1;
			SensorCall();break;
		case '?':
			SensorCall();usage();
			/*NOTREACHED*/
		;/*86*/}}
	SensorCall();if (argc - optind < 2)
		{/*101*/SensorCall();usage();/*102*/}
	SensorCall();out = TIFFOpen(argv[argc-1], mode);
	SensorCall();if (out == NULL)
		{/*103*/{int  ReplaceReturn = (-2); SensorCall(); return ReplaceReturn;}/*104*/}
	SensorCall();if ((argc - optind) == 2)
		{/*105*/SensorCall();pageNum = -1;/*106*/}
	SensorCall();for (; optind < argc-1 ; optind++) {
		SensorCall();char *imageCursor = argv[optind];
		in = openSrcImage (&imageCursor);
		SensorCall();if (in == NULL) {
			SensorCall();(void) TIFFClose(out);
			{int  ReplaceReturn = (-3); SensorCall(); return ReplaceReturn;}
		}
		SensorCall();if (diroff != 0 && !TIFFSetSubDirectory(in, diroff)) {
			SensorCall();TIFFError(TIFFFileName(in),
			    "Error, setting subdirectory at " TIFF_UINT64_FORMAT, diroff);
			(void) TIFFClose(in);
			(void) TIFFClose(out);
			{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
		}
		SensorCall();for (;;) {
			SensorCall();config = defconfig;
			compression = defcompression;
			predictor = defpredictor;
                        preset = defpreset;
			fillorder = deffillorder;
			rowsperstrip = defrowsperstrip;
			tilewidth = deftilewidth;
			tilelength = deftilelength;
			g3opts = defg3opts;
			SensorCall();if (!tiffcp(in, out) || !TIFFWriteDirectory(out)) {
				SensorCall();(void) TIFFClose(in);
				(void) TIFFClose(out);
				{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
			}
			SensorCall();if (imageCursor) { /* seek next image directory */
				SensorCall();if (!nextSrcImage(in, &imageCursor)) {/*107*/SensorCall();break;/*108*/}
			}else
				{/*109*/SensorCall();if (!TIFFReadDirectory(in)) {/*111*/SensorCall();break;/*112*/}/*110*/}
		}
		SensorCall();(void) TIFFClose(in);
	}

	SensorCall();(void) TIFFClose(out);
	{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}
}

static void
processZIPOptions(char* cp)
{
	SensorCall();if ( (cp = strchr(cp, ':')) ) {
		SensorCall();do {
			SensorCall();cp++;
			SensorCall();if (isdigit((int)*cp))
				{/*113*/SensorCall();defpredictor = atoi(cp);/*114*/}
			else {/*115*/SensorCall();if (*cp == 'p')
				{/*117*/SensorCall();defpreset = atoi(++cp);/*118*/}
			else
				{/*119*/SensorCall();usage();/*120*/}/*116*/}
		} while( (cp = strchr(cp, ':')) );
	}
SensorCall();}

static void
processG3Options(char* cp)
{
	SensorCall();if( (cp = strchr(cp, ':')) ) {
		SensorCall();if (defg3opts == (uint32) -1)
			{/*121*/SensorCall();defg3opts = 0;/*122*/}
		SensorCall();do {
			SensorCall();cp++;
			SensorCall();if (strneq(cp, "1d", 2))
				defg3opts &= ~GROUP3OPT_2DENCODING;
			else {/*123*/SensorCall();if (strneq(cp, "2d", 2))
				defg3opts |= GROUP3OPT_2DENCODING;
			else {/*125*/SensorCall();if (strneq(cp, "fill", 4))
				defg3opts |= GROUP3OPT_FILLBITS;
			else
				{/*127*/SensorCall();usage();/*128*/}/*126*/}/*124*/}
		} while( (cp = strchr(cp, ':')) );
	}
SensorCall();}

static int
processCompressOptions(char* opt)
{
	SensorCall();if (streq(opt, "none")) {
		SensorCall();defcompression = COMPRESSION_NONE;
	} else {/*49*/SensorCall();if (streq(opt, "packbits")) {
		SensorCall();defcompression = COMPRESSION_PACKBITS;
	} else {/*51*/SensorCall();if (strneq(opt, "jpeg", 4)) {
		SensorCall();char* cp = strchr(opt, ':');

		defcompression = COMPRESSION_JPEG;
		SensorCall();while( cp )
		{
			SensorCall();if (isdigit((int)cp[1]))
				{/*53*/SensorCall();quality = atoi(cp+1);/*54*/}
			else {/*55*/SensorCall();if (cp[1] == 'r' )
				jpegcolormode = JPEGCOLORMODE_RAW;
			else
				{/*57*/SensorCall();usage();/*58*/}/*56*/}

			SensorCall();cp = strchr(cp+1,':');
		}
	} else {/*59*/SensorCall();if (strneq(opt, "g3", 2)) {
		SensorCall();processG3Options(opt);
		defcompression = COMPRESSION_CCITTFAX3;
	} else {/*61*/SensorCall();if (streq(opt, "g4")) {
		SensorCall();defcompression = COMPRESSION_CCITTFAX4;
	} else {/*63*/SensorCall();if (strneq(opt, "lzw", 3)) {
		SensorCall();char* cp = strchr(opt, ':');
		SensorCall();if (cp)
			{/*65*/SensorCall();defpredictor = atoi(cp+1);/*66*/}
		SensorCall();defcompression = COMPRESSION_LZW;
	} else {/*67*/SensorCall();if (strneq(opt, "zip", 3)) {
		SensorCall();processZIPOptions(opt);
		defcompression = COMPRESSION_ADOBE_DEFLATE;
	} else {/*69*/SensorCall();if (strneq(opt, "lzma", 4)) {
		SensorCall();processZIPOptions(opt);
		defcompression = COMPRESSION_LZMA;
	} else {/*71*/SensorCall();if (strneq(opt, "jbig", 4)) {
		SensorCall();defcompression = COMPRESSION_JBIG;
	} else {/*73*/SensorCall();if (strneq(opt, "sgilog", 6)) {
		SensorCall();defcompression = COMPRESSION_SGILOG;
	} else
		{/*75*/{int  ReplaceReturn = (0); SensorCall(); return ReplaceReturn;}/*76*/}/*74*/}/*72*/}/*70*/}/*68*/}/*64*/}/*62*/}/*60*/}/*52*/}/*50*/}
	{int  ReplaceReturn = (1); SensorCall(); return ReplaceReturn;}
}

char* stuff[] = {
"usage: tiffcp [options] input... output",
"where options are:",
" -a              append to output instead of overwriting",
" -o offset       set initial directory offset",
" -p contig       pack samples contiguously (e.g. RGBRGB...)",
" -p separate     store samples separately (e.g. RRR...GGG...BBB...)",
" -s              write output in strips",
" -t              write output in tiles",
" -8              write BigTIFF instead of default ClassicTIFF",
" -B              write big-endian instead of native byte order",
" -L              write little-endian instead of native byte order",
" -M              disable use of memory-mapped files",
" -C              disable strip chopping",
" -i              ignore read errors",
" -b file[,#]     bias (dark) monochrome image to be subtracted from all others",
" -,=%            use % rather than , to separate image #'s (per Note below)",
"",
" -r #            make each strip have no more than # rows",
" -w #            set output tile width (pixels)",
" -l #            set output tile length (pixels)",
"",
" -f lsb2msb      force lsb-to-msb FillOrder for output",
" -f msb2lsb      force msb-to-lsb FillOrder for output",
"",
" -c lzw[:opts]   compress output with Lempel-Ziv & Welch encoding",
" -c zip[:opts]   compress output with deflate encoding",
" -c lzma[:opts]  compress output with LZMA2 encoding",
" -c jpeg[:opts]  compress output with JPEG encoding",
" -c jbig         compress output with ISO JBIG encoding",
" -c packbits     compress output with packbits encoding",
" -c g3[:opts]    compress output with CCITT Group 3 encoding",
" -c g4           compress output with CCITT Group 4 encoding",
" -c sgilog       compress output with SGILOG encoding",
" -c none         use no compression algorithm on output",
" -x              force the merged tiff pages in sequence",
"",
"Group 3 options:",
" 1d              use default CCITT Group 3 1D-encoding",
" 2d              use optional CCITT Group 3 2D-encoding",
" fill            byte-align EOL codes",
"For example, -c g3:2d:fill to get G3-2D-encoded data with byte-aligned EOLs",
"",
"JPEG options:",
" #               set compression quality level (0-100, default 75)",
" r               output color image as RGB rather than YCbCr",
"For example, -c jpeg:r:50 to get JPEG-encoded RGB data with 50% comp. quality",
"",
"LZW, Deflate (ZIP) and LZMA2 options:",
" #               set predictor value",
" p#              set compression level (preset)",
"For example, -c lzw:2 to get LZW-encoded data with horizontal differencing,",
"-c zip:3:p9 for Deflate encoding with maximum compression level and floating",
"point predictor.",
"",
"Note that input filenames may be of the form filename,x,y,z",
"where x, y, and z specify image numbers in the filename to copy.",
"example:  tiffcp -c none -b esp.tif,1 esp.tif,0 test.tif",
"  subtract 2nd image in esp.tif from 1st yielding uncompressed result test.tif",
NULL
};

static void
usage(void)
{
	SensorCall();char buf[BUFSIZ];
	int i;

	setbuf(stderr, buf);
	fprintf(stderr, "%s\n\n", TIFFGetVersion());
	SensorCall();for (i = 0; stuff[i] != NULL; i++)
		{/*77*/SensorCall();fprintf(stderr, "%s\n", stuff[i]);/*78*/}
	SensorCall();exit(-1);
SensorCall();}

#define	CopyField(tag, v) \
    if (TIFFGetField(in, tag, &v)) TIFFSetField(out, tag, v)
#define	CopyField2(tag, v1, v2) \
    if (TIFFGetField(in, tag, &v1, &v2)) TIFFSetField(out, tag, v1, v2)
#define	CopyField3(tag, v1, v2, v3) \
    if (TIFFGetField(in, tag, &v1, &v2, &v3)) TIFFSetField(out, tag, v1, v2, v3)
#define	CopyField4(tag, v1, v2, v3, v4) \
    if (TIFFGetField(in, tag, &v1, &v2, &v3, &v4)) TIFFSetField(out, tag, v1, v2, v3, v4)

static void
cpTag(TIFF* in, TIFF* out, uint16 tag, uint16 count, TIFFDataType type)
{
	SensorCall();switch (type) {
	case TIFF_SHORT:
		SensorCall();if (count == 1) {
			SensorCall();uint16 shortv;
			CopyField(tag, shortv);
		} else {/*129*/SensorCall();if (count == 2) {
			SensorCall();uint16 shortv1, shortv2;
			CopyField2(tag, shortv1, shortv2);
		} else {/*131*/SensorCall();if (count == 4) {
			SensorCall();uint16 *tr, *tg, *tb, *ta;
			CopyField4(tag, tr, tg, tb, ta);
		} else {/*133*/SensorCall();if (count == (uint16) -1) {
			SensorCall();uint16 shortv1;
			uint16* shortav;
			CopyField2(tag, shortv1, shortav);
		;/*134*/}/*132*/}/*130*/}}
		SensorCall();break;
	case TIFF_LONG:
		{ SensorCall();uint32 longv;
		  CopyField(tag, longv);
		}
		SensorCall();break;
	case TIFF_RATIONAL:
		SensorCall();if (count == 1) {
			SensorCall();float floatv;
			CopyField(tag, floatv);
		} else {/*135*/SensorCall();if (count == (uint16) -1) {
			SensorCall();float* floatav;
			CopyField(tag, floatav);
		;/*136*/}}
		SensorCall();break;
	case TIFF_ASCII:
		{ SensorCall();char* stringv;
		  CopyField(tag, stringv);
		}
		SensorCall();break;
	case TIFF_DOUBLE:
		SensorCall();if (count == 1) {
			SensorCall();double doublev;
			CopyField(tag, doublev);
		} else {/*137*/SensorCall();if (count == (uint16) -1) {
			SensorCall();double* doubleav;
			CopyField(tag, doubleav);
		;/*138*/}}
		SensorCall();break;
	default:
		SensorCall();TIFFError(TIFFFileName(in),
		    "Data type %d is not supported, tag %d skipped.",
		    tag, type);
	}
SensorCall();}

static struct cpTag {
	uint16 tag;
	uint16 count;
	TIFFDataType type;
} tags[] = {
	{ TIFFTAG_SUBFILETYPE,		1, TIFF_LONG },
	{ TIFFTAG_THRESHHOLDING,	1, TIFF_SHORT },
	{ TIFFTAG_DOCUMENTNAME,		1, TIFF_ASCII },
	{ TIFFTAG_IMAGEDESCRIPTION,	1, TIFF_ASCII },
	{ TIFFTAG_MAKE,			1, TIFF_ASCII },
	{ TIFFTAG_MODEL,		1, TIFF_ASCII },
	{ TIFFTAG_MINSAMPLEVALUE,	1, TIFF_SHORT },
	{ TIFFTAG_MAXSAMPLEVALUE,	1, TIFF_SHORT },
	{ TIFFTAG_XRESOLUTION,		1, TIFF_RATIONAL },
	{ TIFFTAG_YRESOLUTION,		1, TIFF_RATIONAL },
	{ TIFFTAG_PAGENAME,		1, TIFF_ASCII },
	{ TIFFTAG_XPOSITION,		1, TIFF_RATIONAL },
	{ TIFFTAG_YPOSITION,		1, TIFF_RATIONAL },
	{ TIFFTAG_RESOLUTIONUNIT,	1, TIFF_SHORT },
	{ TIFFTAG_SOFTWARE,		1, TIFF_ASCII },
	{ TIFFTAG_DATETIME,		1, TIFF_ASCII },
	{ TIFFTAG_ARTIST,		1, TIFF_ASCII },
	{ TIFFTAG_HOSTCOMPUTER,		1, TIFF_ASCII },
	{ TIFFTAG_WHITEPOINT,		(uint16) -1, TIFF_RATIONAL },
	{ TIFFTAG_PRIMARYCHROMATICITIES,(uint16) -1,TIFF_RATIONAL },
	{ TIFFTAG_HALFTONEHINTS,	2, TIFF_SHORT },
	{ TIFFTAG_INKSET,		1, TIFF_SHORT },
	{ TIFFTAG_DOTRANGE,		2, TIFF_SHORT },
	{ TIFFTAG_TARGETPRINTER,	1, TIFF_ASCII },
	{ TIFFTAG_SAMPLEFORMAT,		1, TIFF_SHORT },
	{ TIFFTAG_YCBCRCOEFFICIENTS,	(uint16) -1,TIFF_RATIONAL },
	{ TIFFTAG_YCBCRSUBSAMPLING,	2, TIFF_SHORT },
	{ TIFFTAG_YCBCRPOSITIONING,	1, TIFF_SHORT },
	{ TIFFTAG_REFERENCEBLACKWHITE,	(uint16) -1,TIFF_RATIONAL },
	{ TIFFTAG_EXTRASAMPLES,		(uint16) -1, TIFF_SHORT },
	{ TIFFTAG_SMINSAMPLEVALUE,	1, TIFF_DOUBLE },
	{ TIFFTAG_SMAXSAMPLEVALUE,	1, TIFF_DOUBLE },
	{ TIFFTAG_STONITS,		1, TIFF_DOUBLE },
};
#define	NTAGS	(sizeof (tags) / sizeof (tags[0]))

#define	CopyTag(tag, count, type)	cpTag(in, out, tag, count, type)

typedef int (*copyFunc)
    (TIFF* in, TIFF* out, uint32 l, uint32 w, uint16 samplesperpixel);
static	copyFunc pickCopyFunc(TIFF*, TIFF*, uint16, uint16);

/* PODD */

static int
tiffcp(TIFF* in, TIFF* out)
{
	SensorCall();uint16 bitspersample, samplesperpixel;
	uint16 input_compression, input_photometric;
	copyFunc cf;
	uint32 width, length;
	struct cpTag* p;

	CopyField(TIFFTAG_IMAGEWIDTH, width);
	CopyField(TIFFTAG_IMAGELENGTH, length);
	CopyField(TIFFTAG_BITSPERSAMPLE, bitspersample);
	CopyField(TIFFTAG_SAMPLESPERPIXEL, samplesperpixel);
	if (compression != (uint16)-1)
		TIFFSetField(out, TIFFTAG_COMPRESSION, compression);
	else
		CopyField(TIFFTAG_COMPRESSION, compression);
	SensorCall();TIFFGetFieldDefaulted(in, TIFFTAG_COMPRESSION, &input_compression);
	TIFFGetFieldDefaulted(in, TIFFTAG_PHOTOMETRIC, &input_photometric);
	SensorCall();if (input_compression == COMPRESSION_JPEG) {
		/* Force conversion to RGB */
		SensorCall();TIFFSetField(in, TIFFTAG_JPEGCOLORMODE, JPEGCOLORMODE_RGB);
	} else {/*3*/SensorCall();if (input_photometric == PHOTOMETRIC_YCBCR) {
		/* Otherwise, can't handle subsampled input */
		SensorCall();uint16 subsamplinghor,subsamplingver;

		TIFFGetFieldDefaulted(in, TIFFTAG_YCBCRSUBSAMPLING,
				      &subsamplinghor, &subsamplingver);
		SensorCall();if (subsamplinghor!=1 || subsamplingver!=1) {
			SensorCall();fprintf(stderr, "tiffcp: %s: Can't copy/convert subsampled image.\n",
				TIFFFileName(in));
			{int  ReplaceReturn = FALSE; SensorCall(); return ReplaceReturn;}
		}
	;/*4*/}}
	if (compression == COMPRESSION_JPEG) {
		if (input_photometric == PHOTOMETRIC_RGB &&
		    jpegcolormode == JPEGCOLORMODE_RGB)
		  TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_YCBCR);
		else
		  TIFFSetField(out, TIFFTAG_PHOTOMETRIC, input_photometric);
	}
	else if (compression == COMPRESSION_SGILOG
	    || compression == COMPRESSION_SGILOG24)
		TIFFSetField(out, TIFFTAG_PHOTOMETRIC,
		    samplesperpixel == 1 ?
		    PHOTOMETRIC_LOGL : PHOTOMETRIC_LOGLUV);
	else
		CopyTag(TIFFTAG_PHOTOMETRIC, 1, TIFF_SHORT);
	if (fillorder != 0)
		TIFFSetField(out, TIFFTAG_FILLORDER, fillorder);
	else
		CopyTag(TIFFTAG_FILLORDER, 1, TIFF_SHORT);
	/*
	 * Will copy `Orientation' tag from input image
	 */
	SensorCall();TIFFGetFieldDefaulted(in, TIFFTAG_ORIENTATION, &orientation);
	SensorCall();switch (orientation) {
		case ORIENTATION_BOTRIGHT:
		case ORIENTATION_RIGHTBOT:	/* XXX */
			SensorCall();TIFFWarning(TIFFFileName(in), "using bottom-left orientation");
			orientation = ORIENTATION_BOTLEFT;
		/* fall thru... */
		case ORIENTATION_LEFTBOT:	/* XXX */
		case ORIENTATION_BOTLEFT:
			SensorCall();break;
		case ORIENTATION_TOPRIGHT:
		case ORIENTATION_RIGHTTOP:	/* XXX */
		default:
			SensorCall();TIFFWarning(TIFFFileName(in), "using top-left orientation");
			orientation = ORIENTATION_TOPLEFT;
		/* fall thru... */
		case ORIENTATION_LEFTTOP:	/* XXX */
		case ORIENTATION_TOPLEFT:
			SensorCall();break;
	}
	SensorCall();TIFFSetField(out, TIFFTAG_ORIENTATION, orientation);
	/*
	 * Choose tiles/strip for the output image according to
	 * the command line arguments (-tiles, -strips) and the
	 * structure of the input image.
	 */
	SensorCall();if (outtiled == -1)
		{/*15*/SensorCall();outtiled = TIFFIsTiled(in);/*16*/}
	SensorCall();if (outtiled) {
		/*
		 * Setup output file's tile width&height.  If either
		 * is not specified, use either the value from the
		 * input image or, if nothing is defined, use the
		 * library default.
		 */
		SensorCall();if (tilewidth == (uint32) -1)
			{/*17*/SensorCall();TIFFGetField(in, TIFFTAG_TILEWIDTH, &tilewidth);/*18*/}
		SensorCall();if (tilelength == (uint32) -1)
			{/*19*/SensorCall();TIFFGetField(in, TIFFTAG_TILELENGTH, &tilelength);/*20*/}
		SensorCall();TIFFDefaultTileSize(out, &tilewidth, &tilelength);
		TIFFSetField(out, TIFFTAG_TILEWIDTH, tilewidth);
		TIFFSetField(out, TIFFTAG_TILELENGTH, tilelength);
	} else {
		/*
		 * RowsPerStrip is left unspecified: use either the
		 * value from the input image or, if nothing is defined,
		 * use the library default.
		 */
		SensorCall();if (rowsperstrip == (uint32) 0) {
			SensorCall();if (!TIFFGetField(in, TIFFTAG_ROWSPERSTRIP,
			    &rowsperstrip)) {
				SensorCall();rowsperstrip =
				    TIFFDefaultStripSize(out, rowsperstrip);
			}
			SensorCall();if (rowsperstrip > length && rowsperstrip != (uint32)-1)
				{/*21*/SensorCall();rowsperstrip = length;/*22*/}
		}
		else {/*23*/SensorCall();if (rowsperstrip == (uint32) -1)
			{/*25*/SensorCall();rowsperstrip = length;/*26*/}/*24*/}
		SensorCall();TIFFSetField(out, TIFFTAG_ROWSPERSTRIP, rowsperstrip);
	}
	if (config != (uint16) -1)
		TIFFSetField(out, TIFFTAG_PLANARCONFIG, config);
	else
		CopyField(TIFFTAG_PLANARCONFIG, config);
	if (samplesperpixel <= 4)
		CopyTag(TIFFTAG_TRANSFERFUNCTION, 4, TIFF_SHORT);
	CopyTag(TIFFTAG_COLORMAP, 4, TIFF_SHORT);
/* SMinSampleValue & SMaxSampleValue */
	SensorCall();switch (compression) {
		case COMPRESSION_JPEG:
			SensorCall();TIFFSetField(out, TIFFTAG_JPEGQUALITY, quality);
			TIFFSetField(out, TIFFTAG_JPEGCOLORMODE, jpegcolormode);
			SensorCall();break;
		case COMPRESSION_JBIG:
			CopyTag(TIFFTAG_FAXRECVPARAMS, 1, TIFF_LONG);
			CopyTag(TIFFTAG_FAXRECVTIME, 1, TIFF_LONG);
			CopyTag(TIFFTAG_FAXSUBADDRESS, 1, TIFF_ASCII);
			CopyTag(TIFFTAG_FAXDCS, 1, TIFF_ASCII);
			SensorCall();break;
		case COMPRESSION_LZW:
		case COMPRESSION_ADOBE_DEFLATE:
		case COMPRESSION_DEFLATE:
                case COMPRESSION_LZMA:
			if (predictor != (uint16)-1)
				TIFFSetField(out, TIFFTAG_PREDICTOR, predictor);
			else
				CopyField(TIFFTAG_PREDICTOR, predictor);
			SensorCall();if (preset != -1) {
                                SensorCall();if (compression == COMPRESSION_ADOBE_DEFLATE
                                         || compression == COMPRESSION_DEFLATE)
                                        {/*31*/SensorCall();TIFFSetField(out, TIFFTAG_ZIPQUALITY, preset);/*32*/}
				else {/*33*/SensorCall();if (compression == COMPRESSION_LZMA)
					{/*35*/SensorCall();TIFFSetField(out, TIFFTAG_LZMAPRESET, preset);/*36*/}/*34*/}
                        }
			SensorCall();break;
		case COMPRESSION_CCITTFAX3:
		case COMPRESSION_CCITTFAX4:
			if (compression == COMPRESSION_CCITTFAX3) {
				if (g3opts != (uint32) -1)
					TIFFSetField(out, TIFFTAG_GROUP3OPTIONS,
					    g3opts);
				else
					CopyField(TIFFTAG_GROUP3OPTIONS, g3opts);
			} else
				CopyTag(TIFFTAG_GROUP4OPTIONS, 1, TIFF_LONG);
			CopyTag(TIFFTAG_BADFAXLINES, 1, TIFF_LONG);
			CopyTag(TIFFTAG_CLEANFAXDATA, 1, TIFF_LONG);
			CopyTag(TIFFTAG_CONSECUTIVEBADFAXLINES, 1, TIFF_LONG);
			CopyTag(TIFFTAG_FAXRECVPARAMS, 1, TIFF_LONG);
			CopyTag(TIFFTAG_FAXRECVTIME, 1, TIFF_LONG);
			CopyTag(TIFFTAG_FAXSUBADDRESS, 1, TIFF_ASCII);
			SensorCall();break;
	}
	{
		SensorCall();uint32 len32;
		void** data;
		SensorCall();if (TIFFGetField(in, TIFFTAG_ICCPROFILE, &len32, &data))
			{/*39*/SensorCall();TIFFSetField(out, TIFFTAG_ICCPROFILE, len32, data);/*40*/}
	}
	{
		SensorCall();uint16 ninks;
		const char* inknames;
		SensorCall();if (TIFFGetField(in, TIFFTAG_NUMBEROFINKS, &ninks)) {
			SensorCall();TIFFSetField(out, TIFFTAG_NUMBEROFINKS, ninks);
			SensorCall();if (TIFFGetField(in, TIFFTAG_INKNAMES, &inknames)) {
				SensorCall();int inknameslen = strlen(inknames) + 1;
				const char* cp = inknames;
				SensorCall();while (ninks > 1) {
					SensorCall();cp = strchr(cp, '\0');
                                        cp++;
                                        inknameslen += (strlen(cp) + 1);
					ninks--;
				}
				SensorCall();TIFFSetField(out, TIFFTAG_INKNAMES, inknameslen, inknames);
			}
		}
	}
	{
		SensorCall();unsigned short pg0, pg1;

		SensorCall();if (pageInSeq == 1) {
			SensorCall();if (pageNum < 0) /* only one input file */ {
				SensorCall();if (TIFFGetField(in, TIFFTAG_PAGENUMBER, &pg0, &pg1))
					{/*41*/SensorCall();TIFFSetField(out, TIFFTAG_PAGENUMBER, pg0, pg1);/*42*/}
			} else
				{/*43*/SensorCall();TIFFSetField(out, TIFFTAG_PAGENUMBER, pageNum++, 0);/*44*/}

		} else {
			SensorCall();if (TIFFGetField(in, TIFFTAG_PAGENUMBER, &pg0, &pg1)) {
				SensorCall();if (pageNum < 0) /* only one input file */
					{/*45*/SensorCall();TIFFSetField(out, TIFFTAG_PAGENUMBER, pg0, pg1);/*46*/}
				else
					{/*47*/SensorCall();TIFFSetField(out, TIFFTAG_PAGENUMBER, pageNum++, 0);/*48*/}
			}
		}
	}

	for (p = tags; p < &tags[NTAGS]; p++)
		CopyTag(p->tag, p->count, p->type);

	SensorCall();cf = pickCopyFunc(in, out, bitspersample, samplesperpixel);
	{int  ReplaceReturn = (cf ? (*cf)(in, out, length, width, samplesperpixel) : FALSE); SensorCall(); return ReplaceReturn;}
}

/*
 * Copy Functions.
 */
#define	DECLAREcpFunc(x) \
static int x(TIFF* in, TIFF* out, \
    uint32 imagelength, uint32 imagewidth, tsample_t spp)

#define	DECLAREreadFunc(x) \
static int x(TIFF* in, \
    uint8* buf, uint32 imagelength, uint32 imagewidth, tsample_t spp)
typedef int (*readFunc)(TIFF*, uint8*, uint32, uint32, tsample_t);

#define	DECLAREwriteFunc(x) \
static int x(TIFF* out, \
    uint8* buf, uint32 imagelength, uint32 imagewidth, tsample_t spp)
typedef int (*writeFunc)(TIFF*, uint8*, uint32, uint32, tsample_t);

/*
 * Contig -> contig by scanline for rows/strip change.
 */
DECLAREcpFunc(cpContig2ContigByRow)
{
	SensorCall();tsize_t scanlinesize = TIFFScanlineSize(in);
	tdata_t buf;
	uint32 row;

	buf = _TIFFmalloc(scanlinesize);
	SensorCall();if (!buf)
		{/*143*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*144*/}
	SensorCall();_TIFFmemset(buf, 0, scanlinesize);
	(void) imagewidth; (void) spp;
	SensorCall();for (row = 0; row < imagelength; row++) {
		SensorCall();if (TIFFReadScanline(in, buf, row, 0) < 0 && !ignore) {
			SensorCall();TIFFError(TIFFFileName(in),
				  "Error, can't read scanline %lu",
				  (unsigned long) row);
			SensorCall();goto bad;
		}
		SensorCall();if (TIFFWriteScanline(out, buf, row, 0) < 0) {
			SensorCall();TIFFError(TIFFFileName(out),
				  "Error, can't write scanline %lu",
				  (unsigned long) row);
			SensorCall();goto bad;
		}
	}
	SensorCall();_TIFFfree(buf);
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
bad:
	_TIFFfree(buf);
	return 0;
}


typedef void biasFn (void *image, void *bias, uint32 pixels);

#define subtract(bits) \
static void subtract##bits (void *i, void *b, uint32 pixels)\
{\
   uint##bits *image = i;\
   uint##bits *bias = b;\
   while (pixels--) {\
     *image = *image > *bias ? *image-*bias : 0;\
     image++, bias++; \
   } \
}

subtract(8)
subtract(16)
subtract(32)

static biasFn *lineSubtractFn (unsigned bits)
{
	SensorCall();switch (bits) {
		case  8:  {biasFn * ReplaceReturn = subtract8; SensorCall(); return ReplaceReturn;}
		case 16:  {biasFn * ReplaceReturn = subtract16; SensorCall(); return ReplaceReturn;}
		case 32:  {biasFn * ReplaceReturn = subtract32; SensorCall(); return ReplaceReturn;}
	}
	{biasFn * ReplaceReturn = NULL; SensorCall(); return ReplaceReturn;}
}

/*
 * Contig -> contig by scanline while subtracting a bias image.
 */
DECLAREcpFunc(cpBiasedContig2Contig)
{
	if (spp == 1) {
		tsize_t biasSize = TIFFScanlineSize(bias);
		tsize_t bufSize = TIFFScanlineSize(in);
		tdata_t buf, biasBuf;
		uint32 biasWidth = 0, biasLength = 0;
		TIFFGetField(bias, TIFFTAG_IMAGEWIDTH, &biasWidth);
		TIFFGetField(bias, TIFFTAG_IMAGELENGTH, &biasLength);
		if (biasSize == bufSize &&
		    imagelength == biasLength && imagewidth == biasWidth) {
			uint16 sampleBits = 0;
			biasFn *subtractLine;
			TIFFGetField(in, TIFFTAG_BITSPERSAMPLE, &sampleBits);
			subtractLine = lineSubtractFn (sampleBits);
			if (subtractLine) {
				uint32 row;
				buf = _TIFFmalloc(bufSize);
				biasBuf = _TIFFmalloc(bufSize);
				for (row = 0; row < imagelength; row++) {
					if (TIFFReadScanline(in, buf, row, 0) < 0
					    && !ignore) {
						TIFFError(TIFFFileName(in),
						    "Error, can't read scanline %lu",
						    (unsigned long) row);
						goto bad;
					}
					if (TIFFReadScanline(bias, biasBuf, row, 0) < 0
					    && !ignore) {
						TIFFError(TIFFFileName(in),
						    "Error, can't read biased scanline %lu",
						    (unsigned long) row);
						goto bad;
					}
					subtractLine (buf, biasBuf, imagewidth);
					if (TIFFWriteScanline(out, buf, row, 0) < 0) {
						TIFFError(TIFFFileName(out),
						    "Error, can't write scanline %lu",
						    (unsigned long) row);
						goto bad;
					}
				}

				_TIFFfree(buf);
				_TIFFfree(biasBuf);
				TIFFSetDirectory(bias,
				    TIFFCurrentDirectory(bias)); /* rewind */
				return 1;
bad:
				_TIFFfree(buf);
				_TIFFfree(biasBuf);
				return 0;
			} else {
				TIFFError(TIFFFileName(in),
				    "No support for biasing %d bit pixels\n",
				    sampleBits);
				return 0;
			}
		}
		TIFFError(TIFFFileName(in),
		    "Bias image %s,%d\nis not the same size as %s,%d\n",
		    TIFFFileName(bias), TIFFCurrentDirectory(bias),
		    TIFFFileName(in), TIFFCurrentDirectory(in));
		return 0;
	} else {
		TIFFError(TIFFFileName(in),
		    "Can't bias %s,%d as it has >1 Sample/Pixel\n",
		    TIFFFileName(in), TIFFCurrentDirectory(in));
		return 0;
	}

}


/*
 * Strip -> strip for change in encoding.
 */
DECLAREcpFunc(cpDecodedStrips)
{
	tsize_t stripsize  = TIFFStripSize(in);
	tdata_t buf = _TIFFmalloc(stripsize);

	(void) imagewidth; (void) spp;
	if (buf) {
		tstrip_t s, ns = TIFFNumberOfStrips(in);
		uint32 row = 0;
		_TIFFmemset(buf, 0, stripsize);
		for (s = 0; s < ns; s++) {
			tsize_t cc = (row + rowsperstrip > imagelength) ?
			    TIFFVStripSize(in, imagelength - row) : stripsize;
			if (TIFFReadEncodedStrip(in, s, buf, cc) < 0
			    && !ignore) {
				TIFFError(TIFFFileName(in),
				    "Error, can't read strip %lu",
				    (unsigned long) s);
				goto bad;
			}
			if (TIFFWriteEncodedStrip(out, s, buf, cc) < 0) {
				TIFFError(TIFFFileName(out),
				    "Error, can't write strip %lu",
				    (unsigned long) s);
				goto bad;
			}
			row += rowsperstrip;
		}
		_TIFFfree(buf);
		return 1;
	} else {
		TIFFError(TIFFFileName(in),
		    "Error, can't allocate memory buffer of size %lu "
		    "to read strips", (unsigned long) stripsize);
		return 0;
	}

bad:
	_TIFFfree(buf);
	return 0;
}

/*
 * Separate -> separate by row for rows/strip change.
 */
DECLAREcpFunc(cpSeparate2SeparateByRow)
{
	tsize_t scanlinesize = TIFFScanlineSize(in);
	tdata_t buf;
	uint32 row;
	tsample_t s;

	(void) imagewidth;
	buf = _TIFFmalloc(scanlinesize);
	if (!buf)
		{/*145*/return 0;/*146*/}
	_TIFFmemset(buf, 0, scanlinesize);
	for (s = 0; s < spp; s++) {
		for (row = 0; row < imagelength; row++) {
			if (TIFFReadScanline(in, buf, row, s) < 0 && !ignore) {
				TIFFError(TIFFFileName(in),
				    "Error, can't read scanline %lu",
				    (unsigned long) row);
				goto bad;
			}
			if (TIFFWriteScanline(out, buf, row, s) < 0) {
				TIFFError(TIFFFileName(out),
				    "Error, can't write scanline %lu",
				    (unsigned long) row);
				goto bad;
			}
		}
	}
	_TIFFfree(buf);
	return 1;
bad:
	_TIFFfree(buf);
	return 0;
}

/*
 * Contig -> separate by row.
 */
DECLAREcpFunc(cpContig2SeparateByRow)
{
	tsize_t scanlinesizein = TIFFScanlineSize(in);
	tsize_t scanlinesizeout = TIFFScanlineSize(out);
	tdata_t inbuf;
	tdata_t outbuf;
	register uint8 *inp, *outp;
	register uint32 n;
	uint32 row;
	tsample_t s;

	inbuf = _TIFFmalloc(scanlinesizein);
	outbuf = _TIFFmalloc(scanlinesizeout);
	if (!inbuf || !outbuf)
		{/*147*/return 0;/*148*/}
	_TIFFmemset(inbuf, 0, scanlinesizein);
	_TIFFmemset(outbuf, 0, scanlinesizeout);
	/* unpack channels */
	for (s = 0; s < spp; s++) {
		for (row = 0; row < imagelength; row++) {
			if (TIFFReadScanline(in, inbuf, row, 0) < 0
			    && !ignore) {
				TIFFError(TIFFFileName(in),
				    "Error, can't read scanline %lu",
				    (unsigned long) row);
				goto bad;
			}
			inp = ((uint8*)inbuf) + s;
			outp = (uint8*)outbuf;
			for (n = imagewidth; n-- > 0;) {
				*outp++ = *inp;
				inp += spp;
			}
			if (TIFFWriteScanline(out, outbuf, row, s) < 0) {
				TIFFError(TIFFFileName(out),
				    "Error, can't write scanline %lu",
				    (unsigned long) row);
				goto bad;
			}
		}
	}
	if (inbuf) {/*149*/_TIFFfree(inbuf);/*150*/}
	if (outbuf) {/*151*/_TIFFfree(outbuf);/*152*/}
	return 1;
bad:
	if (inbuf) {/*153*/_TIFFfree(inbuf);/*154*/}
	if (outbuf) {/*155*/_TIFFfree(outbuf);/*156*/}
	return 0;
}

/*
 * Separate -> contig by row.
 */
DECLAREcpFunc(cpSeparate2ContigByRow)
{
	tsize_t scanlinesizein = TIFFScanlineSize(in);
	tsize_t scanlinesizeout = TIFFScanlineSize(out);
	tdata_t inbuf;
	tdata_t outbuf;
	register uint8 *inp, *outp;
	register uint32 n;
	uint32 row;
	tsample_t s;

	inbuf = _TIFFmalloc(scanlinesizein);
	outbuf = _TIFFmalloc(scanlinesizeout);
	if (!inbuf || !outbuf)
		{/*157*/return 0;/*158*/}
	_TIFFmemset(inbuf, 0, scanlinesizein);
	_TIFFmemset(outbuf, 0, scanlinesizeout);
	for (row = 0; row < imagelength; row++) {
		/* merge channels */
		for (s = 0; s < spp; s++) {
			if (TIFFReadScanline(in, inbuf, row, s) < 0
			    && !ignore) {
				TIFFError(TIFFFileName(in),
				    "Error, can't read scanline %lu",
				    (unsigned long) row);
				goto bad;
			}
			inp = (uint8*)inbuf;
			outp = ((uint8*)outbuf) + s;
			for (n = imagewidth; n-- > 0;) {
				*outp = *inp++;
				outp += spp;
			}
		}
		if (TIFFWriteScanline(out, outbuf, row, 0) < 0) {
			TIFFError(TIFFFileName(out),
			    "Error, can't write scanline %lu",
			    (unsigned long) row);
			goto bad;
		}
	}
	if (inbuf) {/*159*/_TIFFfree(inbuf);/*160*/}
	if (outbuf) {/*161*/_TIFFfree(outbuf);/*162*/}
	return 1;
bad:
	if (inbuf) {/*163*/_TIFFfree(inbuf);/*164*/}
	if (outbuf) {/*165*/_TIFFfree(outbuf);/*166*/}
	return 0;
}

static void
cpStripToTile(uint8* out, uint8* in,
    uint32 rows, uint32 cols, int outskew, int inskew)
{
	SensorCall();while (rows-- > 0) {
		SensorCall();uint32 j = cols;
		SensorCall();while (j-- > 0)
			{/*167*/SensorCall();*out++ = *in++;/*168*/}
		SensorCall();out += outskew;
		in += inskew;
	}
SensorCall();}

static void
cpContigBufToSeparateBuf(uint8* out, uint8* in,
    uint32 rows, uint32 cols, int outskew, int inskew, tsample_t spp,
    int bytes_per_sample )
{
	SensorCall();while (rows-- > 0) {
		SensorCall();uint32 j = cols;
		SensorCall();while (j-- > 0)
		{
			SensorCall();int n = bytes_per_sample;

			SensorCall();while( n-- ) {
				SensorCall();*out++ = *in++;
			}
			SensorCall();in += (spp-1) * bytes_per_sample;
		}
		SensorCall();out += outskew;
		in += inskew;
	}
SensorCall();}

static void
cpSeparateBufToContigBuf(uint8* out, uint8* in,
    uint32 rows, uint32 cols, int outskew, int inskew, tsample_t spp,
    int bytes_per_sample)
{
	SensorCall();while (rows-- > 0) {
		SensorCall();uint32 j = cols;
		SensorCall();while (j-- > 0) {
			SensorCall();int n = bytes_per_sample;

			SensorCall();while( n-- ) {
				SensorCall();*out++ = *in++;
			}
			SensorCall();out += (spp-1)*bytes_per_sample;
		}
		SensorCall();out += outskew;
		in += inskew;
	}
SensorCall();}

static int
cpImage(TIFF* in, TIFF* out, readFunc fin, writeFunc fout,
	uint32 imagelength, uint32 imagewidth, tsample_t spp)
{
	SensorCall();int status = 0;
	tdata_t buf = NULL;
	tsize_t scanlinesize = TIFFRasterScanlineSize(in);
	tsize_t bytes = scanlinesize * (tsize_t)imagelength;
	/*
	 * XXX: Check for integer overflow.
	 */
	SensorCall();if (scanlinesize
	    && imagelength
	    && bytes / (tsize_t)imagelength == scanlinesize) {
		SensorCall();buf = _TIFFmalloc(bytes);
		SensorCall();if (buf) {
			SensorCall();if ((*fin)(in, (uint8*)buf, imagelength,
			    imagewidth, spp)) {
				SensorCall();status = (*fout)(out, (uint8*)buf,
				    imagelength, imagewidth, spp);
			}
			SensorCall();_TIFFfree(buf);
		} else {
			SensorCall();TIFFError(TIFFFileName(in),
			    "Error, can't allocate space for image buffer");
		}
	} else {
		SensorCall();TIFFError(TIFFFileName(in), "Error, no space for image buffer");
	}

	{int  ReplaceReturn = status; SensorCall(); return ReplaceReturn;}
}

DECLAREreadFunc(readContigStripsIntoBuffer)
{
	SensorCall();tsize_t scanlinesize = TIFFScanlineSize(in);
	uint8* bufp = buf;
	uint32 row;

	(void) imagewidth; (void) spp;
	SensorCall();for (row = 0; row < imagelength; row++) {
		SensorCall();if (TIFFReadScanline(in, (tdata_t) bufp, row, 0) < 0
		    && !ignore) {
			SensorCall();TIFFError(TIFFFileName(in),
			    "Error, can't read scanline %lu",
			    (unsigned long) row);
			{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
		}
		SensorCall();bufp += scanlinesize;
	}

	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

DECLAREreadFunc(readSeparateStripsIntoBuffer)
{
	int status = 1;
	tsize_t scanlinesize = TIFFScanlineSize(in);
	tdata_t scanline;
	if (!scanlinesize)
		{/*169*/return 0;/*170*/}

	scanline = _TIFFmalloc(scanlinesize);
	if (!scanline)
		{/*171*/return 0;/*172*/}
	_TIFFmemset(scanline, 0, scanlinesize);
	(void) imagewidth;
	if (scanline) {
		uint8* bufp = (uint8*) buf;
		uint32 row;
		tsample_t s;
		for (row = 0; row < imagelength; row++) {
			/* merge channels */
			for (s = 0; s < spp; s++) {
				uint8* bp = bufp + s;
				tsize_t n = scanlinesize;
				uint8* sbuf = scanline;

				if (TIFFReadScanline(in, scanline, row, s) < 0
				    && !ignore) {
					TIFFError(TIFFFileName(in),
					    "Error, can't read scanline %lu",
					    (unsigned long) row);
					    status = 0;
					goto done;
				}
				while (n-- > 0)
					{/*173*/*bp = *sbuf++, bp += spp;/*174*/}
			}
			bufp += scanlinesize * spp;
		}
	}

done:
	_TIFFfree(scanline);
	return status;
}

DECLAREreadFunc(readContigTilesIntoBuffer)
{
	int status = 1;
	tsize_t tilesize = TIFFTileSize(in);
	tdata_t tilebuf;
	uint32 imagew = TIFFScanlineSize(in);
	uint32 tilew  = TIFFTileRowSize(in);
	int iskew = imagew - tilew;
	uint8* bufp = (uint8*) buf;
	uint32 tw, tl;
	uint32 row;

	(void) spp;
	tilebuf = _TIFFmalloc(tilesize);
	if (tilebuf == 0)
		{/*175*/return 0;/*176*/}
	_TIFFmemset(tilebuf, 0, tilesize);
	(void) TIFFGetField(in, TIFFTAG_TILEWIDTH, &tw);
	(void) TIFFGetField(in, TIFFTAG_TILELENGTH, &tl);
        
	for (row = 0; row < imagelength; row += tl) {
		uint32 nrow = (row+tl > imagelength) ? imagelength-row : tl;
		uint32 colb = 0;
		uint32 col;

		for (col = 0; col < imagewidth; col += tw) {
			if (TIFFReadTile(in, tilebuf, col, row, 0, 0) < 0
			    && !ignore) {
				TIFFError(TIFFFileName(in),
				    "Error, can't read tile at %lu %lu",
				    (unsigned long) col,
				    (unsigned long) row);
				status = 0;
				goto done;
			}
			if (colb + tilew > imagew) {
				uint32 width = imagew - colb;
				uint32 oskew = tilew - width;
				cpStripToTile(bufp + colb,
				    tilebuf, nrow, width,
				    oskew + iskew, oskew );
			} else
				{/*177*/cpStripToTile(bufp + colb,
				    tilebuf, nrow, tilew,
				    iskew, 0);/*178*/}
			colb += tilew;
		}
		bufp += imagew * nrow;
	}
done:
	_TIFFfree(tilebuf);
	return status;
}

DECLAREreadFunc(readSeparateTilesIntoBuffer)
{
	int status = 1;
	uint32 imagew = TIFFRasterScanlineSize(in);
	uint32 tilew = TIFFTileRowSize(in);
	int iskew  = imagew - tilew*spp;
	tsize_t tilesize = TIFFTileSize(in);
	tdata_t tilebuf;
	uint8* bufp = (uint8*) buf;
	uint32 tw, tl;
	uint32 row;
	uint16 bps, bytes_per_sample;

	tilebuf = _TIFFmalloc(tilesize);
	if (tilebuf == 0)
		{/*179*/return 0;/*180*/}
	_TIFFmemset(tilebuf, 0, tilesize);
	(void) TIFFGetField(in, TIFFTAG_TILEWIDTH, &tw);
	(void) TIFFGetField(in, TIFFTAG_TILELENGTH, &tl);
	(void) TIFFGetField(in, TIFFTAG_BITSPERSAMPLE, &bps);
	assert( bps % 8 == 0 );
	bytes_per_sample = bps/8;

	for (row = 0; row < imagelength; row += tl) {
		uint32 nrow = (row+tl > imagelength) ? imagelength-row : tl;
		uint32 colb = 0;
		uint32 col;

		for (col = 0; col < imagewidth; col += tw) {
			tsample_t s;

			for (s = 0; s < spp; s++) {
				if (TIFFReadTile(in, tilebuf, col, row, 0, s) < 0
				    && !ignore) {
					TIFFError(TIFFFileName(in),
					    "Error, can't read tile at %lu %lu, "
					    "sample %lu",
					    (unsigned long) col,
					    (unsigned long) row,
					    (unsigned long) s);
					status = 0;
					goto done;
				}
				/*
				 * Tile is clipped horizontally.  Calculate
				 * visible portion and skewing factors.
				 */
				if (colb + tilew*spp > imagew) {
					uint32 width = imagew - colb;
					int oskew = tilew*spp - width;
					cpSeparateBufToContigBuf(
					    bufp+colb+s*bytes_per_sample,
					    tilebuf, nrow,
					    width/(spp*bytes_per_sample),
					    oskew + iskew,
					    oskew/spp, spp,
					    bytes_per_sample);
				} else
					{/*181*/cpSeparateBufToContigBuf(
					    bufp+colb+s*bytes_per_sample,
					    tilebuf, nrow, tw,
					    iskew, 0, spp,
					    bytes_per_sample);/*182*/}
			}
			colb += tilew*spp;
		}
		bufp += imagew * nrow;
	}
done:
	_TIFFfree(tilebuf);
	return status;
}

DECLAREwriteFunc(writeBufferToContigStrips)
{
	SensorCall();uint32 row, rowsperstrip;
	tstrip_t strip = 0;

	(void) imagewidth; (void) spp;
	(void) TIFFGetFieldDefaulted(out, TIFFTAG_ROWSPERSTRIP, &rowsperstrip);
	SensorCall();for (row = 0; row < imagelength; row += rowsperstrip) {
		SensorCall();uint32 nrows = (row+rowsperstrip > imagelength) ?
		    imagelength-row : rowsperstrip;
		tsize_t stripsize = TIFFVStripSize(out, nrows);
		SensorCall();if (TIFFWriteEncodedStrip(out, strip++, buf, stripsize) < 0) {
			SensorCall();TIFFError(TIFFFileName(out),
			    "Error, can't write strip %u", strip - 1);
			{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}
		}
		SensorCall();buf += stripsize;
	}
	{int  ReplaceReturn = 1; SensorCall(); return ReplaceReturn;}
}

DECLAREwriteFunc(writeBufferToSeparateStrips)
{
	uint32 rowsize = imagewidth * spp;
	uint32 rowsperstrip;
	tsize_t stripsize = TIFFStripSize(out);
	tdata_t obuf;
	tstrip_t strip = 0;
	tsample_t s;

	obuf = _TIFFmalloc(stripsize);
	if (obuf == NULL)
		{/*183*/return (0);/*184*/}
	_TIFFmemset(obuf, 0, stripsize);
	(void) TIFFGetFieldDefaulted(out, TIFFTAG_ROWSPERSTRIP, &rowsperstrip);
	for (s = 0; s < spp; s++) {
		uint32 row;
		for (row = 0; row < imagelength; row += rowsperstrip) {
			uint32 nrows = (row+rowsperstrip > imagelength) ?
			    imagelength-row : rowsperstrip;
			tsize_t stripsize = TIFFVStripSize(out, nrows);

			cpContigBufToSeparateBuf(
			    obuf, (uint8*) buf + row*rowsize + s,
			    nrows, imagewidth, 0, 0, spp, 1);
			if (TIFFWriteEncodedStrip(out, strip++, obuf, stripsize) < 0) {
				TIFFError(TIFFFileName(out),
				    "Error, can't write strip %u",
				    strip - 1);
				_TIFFfree(obuf);
				return 0;
			}
		}
	}
	_TIFFfree(obuf);
	return 1;

}

DECLAREwriteFunc(writeBufferToContigTiles)
{
	uint32 imagew = TIFFScanlineSize(out);
	uint32 tilew  = TIFFTileRowSize(out);
	int iskew = imagew - tilew;
	tsize_t tilesize = TIFFTileSize(out);
	tdata_t obuf;
	uint8* bufp = (uint8*) buf;
	uint32 tl, tw;
	uint32 row;

	(void) spp;

	obuf = _TIFFmalloc(TIFFTileSize(out));
	if (obuf == NULL)
		{/*185*/return 0;/*186*/}
	_TIFFmemset(obuf, 0, tilesize);
	(void) TIFFGetField(out, TIFFTAG_TILELENGTH, &tl);
	(void) TIFFGetField(out, TIFFTAG_TILEWIDTH, &tw);
	for (row = 0; row < imagelength; row += tilelength) {
		uint32 nrow = (row+tl > imagelength) ? imagelength-row : tl;
		uint32 colb = 0;
		uint32 col;

		for (col = 0; col < imagewidth; col += tw) {
			/*
			 * Tile is clipped horizontally.  Calculate
			 * visible portion and skewing factors.
			 */
			if (colb + tilew > imagew) {
				uint32 width = imagew - colb;
				int oskew = tilew - width;
				cpStripToTile(obuf, bufp + colb, nrow, width,
				    oskew, oskew + iskew);
			} else
				{/*187*/cpStripToTile(obuf, bufp + colb, nrow, tilew,
				    0, iskew);/*188*/}
			if (TIFFWriteTile(out, obuf, col, row, 0, 0) < 0) {
				TIFFError(TIFFFileName(out),
				    "Error, can't write tile at %lu %lu",
				    (unsigned long) col,
				    (unsigned long) row);
				_TIFFfree(obuf);
				return 0;
			}
			colb += tilew;
		}
		bufp += nrow * imagew;
	}
	_TIFFfree(obuf);
	return 1;
}

DECLAREwriteFunc(writeBufferToSeparateTiles)
{
	uint32 imagew = TIFFScanlineSize(out);
	tsize_t tilew  = TIFFTileRowSize(out);
	uint32 iimagew = TIFFRasterScanlineSize(out);
	int iskew = iimagew - tilew*spp;
	tsize_t tilesize = TIFFTileSize(out);
	tdata_t obuf;
	uint8* bufp = (uint8*) buf;
	uint32 tl, tw;
	uint32 row;
	uint16 bps, bytes_per_sample;

	obuf = _TIFFmalloc(TIFFTileSize(out));
	if (obuf == NULL)
		{/*189*/return 0;/*190*/}
	_TIFFmemset(obuf, 0, tilesize);
	(void) TIFFGetField(out, TIFFTAG_TILELENGTH, &tl);
	(void) TIFFGetField(out, TIFFTAG_TILEWIDTH, &tw);
	(void) TIFFGetField(out, TIFFTAG_BITSPERSAMPLE, &bps);
	assert( bps % 8 == 0 );
	bytes_per_sample = bps/8;

	for (row = 0; row < imagelength; row += tl) {
		uint32 nrow = (row+tl > imagelength) ? imagelength-row : tl;
		uint32 colb = 0;
		uint32 col;

		for (col = 0; col < imagewidth; col += tw) {
			tsample_t s;
			for (s = 0; s < spp; s++) {
				/*
				 * Tile is clipped horizontally.  Calculate
				 * visible portion and skewing factors.
				 */
				if (colb + tilew > imagew) {
					uint32 width = (imagew - colb);
					int oskew = tilew - width;

					cpContigBufToSeparateBuf(obuf,
					    bufp + (colb*spp) + s,
					    nrow, width/bytes_per_sample,
					    oskew, (oskew*spp)+iskew, spp,
					    bytes_per_sample);
				} else
					{/*191*/cpContigBufToSeparateBuf(obuf,
					    bufp + (colb*spp) + s,
					    nrow, tilewidth,
					    0, iskew, spp,
					    bytes_per_sample);/*192*/}
				if (TIFFWriteTile(out, obuf, col, row, 0, s) < 0) {
					TIFFError(TIFFFileName(out),
					    "Error, can't write tile at %lu %lu "
					    "sample %lu",
					    (unsigned long) col,
					    (unsigned long) row,
					    (unsigned long) s);
					_TIFFfree(obuf);
					return 0;
				}
			}
			colb += tilew;
		}
		bufp += nrow * iimagew;
	}
	_TIFFfree(obuf);
	return 1;
}

/*
 * Contig strips -> contig tiles.
 */
DECLAREcpFunc(cpContigStrips2ContigTiles)
{
	return cpImage(in, out,
	    readContigStripsIntoBuffer,
	    writeBufferToContigTiles,
	    imagelength, imagewidth, spp);
}

/*
 * Contig strips -> separate tiles.
 */
DECLAREcpFunc(cpContigStrips2SeparateTiles)
{
	return cpImage(in, out,
	    readContigStripsIntoBuffer,
	    writeBufferToSeparateTiles,
	    imagelength, imagewidth, spp);
}

/*
 * Separate strips -> contig tiles.
 */
DECLAREcpFunc(cpSeparateStrips2ContigTiles)
{
	return cpImage(in, out,
	    readSeparateStripsIntoBuffer,
	    writeBufferToContigTiles,
	    imagelength, imagewidth, spp);
}

/*
 * Separate strips -> separate tiles.
 */
DECLAREcpFunc(cpSeparateStrips2SeparateTiles)
{
	return cpImage(in, out,
	    readSeparateStripsIntoBuffer,
	    writeBufferToSeparateTiles,
	    imagelength, imagewidth, spp);
}

/*
 * Contig strips -> contig tiles.
 */
DECLAREcpFunc(cpContigTiles2ContigTiles)
{
	return cpImage(in, out,
	    readContigTilesIntoBuffer,
	    writeBufferToContigTiles,
	    imagelength, imagewidth, spp);
}

/*
 * Contig tiles -> separate tiles.
 */
DECLAREcpFunc(cpContigTiles2SeparateTiles)
{
	return cpImage(in, out,
	    readContigTilesIntoBuffer,
	    writeBufferToSeparateTiles,
	    imagelength, imagewidth, spp);
}

/*
 * Separate tiles -> contig tiles.
 */
DECLAREcpFunc(cpSeparateTiles2ContigTiles)
{
	return cpImage(in, out,
	    readSeparateTilesIntoBuffer,
	    writeBufferToContigTiles,
	    imagelength, imagewidth, spp);
}

/*
 * Separate tiles -> separate tiles (tile dimension change).
 */
DECLAREcpFunc(cpSeparateTiles2SeparateTiles)
{
	return cpImage(in, out,
	    readSeparateTilesIntoBuffer,
	    writeBufferToSeparateTiles,
	    imagelength, imagewidth, spp);
}

/*
 * Contig tiles -> contig tiles (tile dimension change).
 */
DECLAREcpFunc(cpContigTiles2ContigStrips)
{
	return cpImage(in, out,
	    readContigTilesIntoBuffer,
	    writeBufferToContigStrips,
	    imagelength, imagewidth, spp);
}

/*
 * Contig tiles -> separate strips.
 */
DECLAREcpFunc(cpContigTiles2SeparateStrips)
{
	return cpImage(in, out,
	    readContigTilesIntoBuffer,
	    writeBufferToSeparateStrips,
	    imagelength, imagewidth, spp);
}

/*
 * Separate tiles -> contig strips.
 */
DECLAREcpFunc(cpSeparateTiles2ContigStrips)
{
	return cpImage(in, out,
	    readSeparateTilesIntoBuffer,
	    writeBufferToContigStrips,
	    imagelength, imagewidth, spp);
}

/*
 * Separate tiles -> separate strips.
 */
DECLAREcpFunc(cpSeparateTiles2SeparateStrips)
{
	return cpImage(in, out,
	    readSeparateTilesIntoBuffer,
	    writeBufferToSeparateStrips,
	    imagelength, imagewidth, spp);
}

/*
 * Select the appropriate copy function to use.
 */
static copyFunc
pickCopyFunc(TIFF* in, TIFF* out, uint16 bitspersample, uint16 samplesperpixel)
{
	SensorCall();uint16 shortv;
	uint32 w, l, tw, tl;
	int bychunk;

	(void) TIFFGetField(in, TIFFTAG_PLANARCONFIG, &shortv);
	SensorCall();if (shortv != config && bitspersample != 8 && samplesperpixel > 1) {
		SensorCall();fprintf(stderr,
		    "%s: Cannot handle different planar configuration w/ bits/sample != 8\n",
		    TIFFFileName(in));
		{copyFunc  ReplaceReturn = (NULL); SensorCall(); return ReplaceReturn;}
	}
	SensorCall();TIFFGetField(in, TIFFTAG_IMAGEWIDTH, &w);
	TIFFGetField(in, TIFFTAG_IMAGELENGTH, &l);
	SensorCall();if (!(TIFFIsTiled(out) || TIFFIsTiled(in))) {
		SensorCall();uint32 irps = (uint32) -1L;
		TIFFGetField(in, TIFFTAG_ROWSPERSTRIP, &irps);
		/* if biased, force decoded copying to allow image subtraction */
		bychunk = !bias && (rowsperstrip == irps);
	}else{  /* either in or out is tiled */
		SensorCall();if (bias) {
			SensorCall();fprintf(stderr,
			    "%s: Cannot handle tiled configuration w/bias image\n",
			TIFFFileName(in));
			{copyFunc  ReplaceReturn = (NULL); SensorCall(); return ReplaceReturn;}
		}
		SensorCall();if (TIFFIsTiled(out)) {
			SensorCall();if (!TIFFGetField(in, TIFFTAG_TILEWIDTH, &tw))
				{/*139*/SensorCall();tw = w;/*140*/}
			SensorCall();if (!TIFFGetField(in, TIFFTAG_TILELENGTH, &tl))
				{/*141*/SensorCall();tl = l;/*142*/}
			SensorCall();bychunk = (tw == tilewidth && tl == tilelength);
		} else {  /* out's not, so in must be tiled */
			SensorCall();TIFFGetField(in, TIFFTAG_TILEWIDTH, &tw);
			TIFFGetField(in, TIFFTAG_TILELENGTH, &tl);
			bychunk = (tw == w && tl == rowsperstrip);
		}
	}
#define	T 1
#define	F 0
#define pack(a,b,c,d,e)	((long)(((a)<<11)|((b)<<3)|((c)<<2)|((d)<<1)|(e)))
	SensorCall();switch(pack(shortv,config,TIFFIsTiled(in),TIFFIsTiled(out),bychunk)) {
		/* Strips -> Tiles */
		case pack(PLANARCONFIG_CONTIG,   PLANARCONFIG_CONTIG,   F,T,F):
		case pack(PLANARCONFIG_CONTIG,   PLANARCONFIG_CONTIG,   F,T,T):
			{copyFunc  ReplaceReturn = cpContigStrips2ContigTiles; SensorCall(); return ReplaceReturn;}
		case pack(PLANARCONFIG_CONTIG,   PLANARCONFIG_SEPARATE, F,T,F):
		case pack(PLANARCONFIG_CONTIG,   PLANARCONFIG_SEPARATE, F,T,T):
			{copyFunc  ReplaceReturn = cpContigStrips2SeparateTiles; SensorCall(); return ReplaceReturn;}
		case pack(PLANARCONFIG_SEPARATE, PLANARCONFIG_CONTIG,   F,T,F):
		case pack(PLANARCONFIG_SEPARATE, PLANARCONFIG_CONTIG,   F,T,T):
			{copyFunc  ReplaceReturn = cpSeparateStrips2ContigTiles; SensorCall(); return ReplaceReturn;}
		case pack(PLANARCONFIG_SEPARATE, PLANARCONFIG_SEPARATE, F,T,F):
		case pack(PLANARCONFIG_SEPARATE, PLANARCONFIG_SEPARATE, F,T,T):
			{copyFunc  ReplaceReturn = cpSeparateStrips2SeparateTiles; SensorCall(); return ReplaceReturn;}
		/* Tiles -> Tiles */
		case pack(PLANARCONFIG_CONTIG,   PLANARCONFIG_CONTIG,   T,T,F):
		case pack(PLANARCONFIG_CONTIG,   PLANARCONFIG_CONTIG,   T,T,T):
			{copyFunc  ReplaceReturn = cpContigTiles2ContigTiles; SensorCall(); return ReplaceReturn;}
		case pack(PLANARCONFIG_CONTIG,   PLANARCONFIG_SEPARATE, T,T,F):
		case pack(PLANARCONFIG_CONTIG,   PLANARCONFIG_SEPARATE, T,T,T):
			{copyFunc  ReplaceReturn = cpContigTiles2SeparateTiles; SensorCall(); return ReplaceReturn;}
		case pack(PLANARCONFIG_SEPARATE, PLANARCONFIG_CONTIG,   T,T,F):
		case pack(PLANARCONFIG_SEPARATE, PLANARCONFIG_CONTIG,   T,T,T):
			{copyFunc  ReplaceReturn = cpSeparateTiles2ContigTiles; SensorCall(); return ReplaceReturn;}
		case pack(PLANARCONFIG_SEPARATE, PLANARCONFIG_SEPARATE, T,T,F):
		case pack(PLANARCONFIG_SEPARATE, PLANARCONFIG_SEPARATE, T,T,T):
			{copyFunc  ReplaceReturn = cpSeparateTiles2SeparateTiles; SensorCall(); return ReplaceReturn;}
		/* Tiles -> Strips */
		case pack(PLANARCONFIG_CONTIG,   PLANARCONFIG_CONTIG,   T,F,F):
		case pack(PLANARCONFIG_CONTIG,   PLANARCONFIG_CONTIG,   T,F,T):
			{copyFunc  ReplaceReturn = cpContigTiles2ContigStrips; SensorCall(); return ReplaceReturn;}
		case pack(PLANARCONFIG_CONTIG,   PLANARCONFIG_SEPARATE, T,F,F):
		case pack(PLANARCONFIG_CONTIG,   PLANARCONFIG_SEPARATE, T,F,T):
			{copyFunc  ReplaceReturn = cpContigTiles2SeparateStrips; SensorCall(); return ReplaceReturn;}
		case pack(PLANARCONFIG_SEPARATE, PLANARCONFIG_CONTIG,   T,F,F):
		case pack(PLANARCONFIG_SEPARATE, PLANARCONFIG_CONTIG,   T,F,T):
			{copyFunc  ReplaceReturn = cpSeparateTiles2ContigStrips; SensorCall(); return ReplaceReturn;}
		case pack(PLANARCONFIG_SEPARATE, PLANARCONFIG_SEPARATE, T,F,F):
		case pack(PLANARCONFIG_SEPARATE, PLANARCONFIG_SEPARATE, T,F,T):
			{copyFunc  ReplaceReturn = cpSeparateTiles2SeparateStrips; SensorCall(); return ReplaceReturn;}
		/* Strips -> Strips */
		case pack(PLANARCONFIG_CONTIG,   PLANARCONFIG_CONTIG,   F,F,F):
			{copyFunc  ReplaceReturn = bias ? cpBiasedContig2Contig : cpContig2ContigByRow; SensorCall(); return ReplaceReturn;}
		case pack(PLANARCONFIG_CONTIG,   PLANARCONFIG_CONTIG,   F,F,T):
			{copyFunc  ReplaceReturn = cpDecodedStrips; SensorCall(); return ReplaceReturn;}
		case pack(PLANARCONFIG_CONTIG, PLANARCONFIG_SEPARATE,   F,F,F):
		case pack(PLANARCONFIG_CONTIG, PLANARCONFIG_SEPARATE,   F,F,T):
			{copyFunc  ReplaceReturn = cpContig2SeparateByRow; SensorCall(); return ReplaceReturn;}
		case pack(PLANARCONFIG_SEPARATE, PLANARCONFIG_CONTIG,   F,F,F):
		case pack(PLANARCONFIG_SEPARATE, PLANARCONFIG_CONTIG,   F,F,T):
			{copyFunc  ReplaceReturn = cpSeparate2ContigByRow; SensorCall(); return ReplaceReturn;}
		case pack(PLANARCONFIG_SEPARATE, PLANARCONFIG_SEPARATE, F,F,F):
		case pack(PLANARCONFIG_SEPARATE, PLANARCONFIG_SEPARATE, F,F,T):
			{copyFunc  ReplaceReturn = cpSeparate2SeparateByRow; SensorCall(); return ReplaceReturn;}
	}
#undef pack
#undef F
#undef T
	SensorCall();fprintf(stderr, "tiffcp: %s: Don't know how to copy/convert image.\n",
	    TIFFFileName(in));
	{copyFunc  ReplaceReturn = (NULL); SensorCall(); return ReplaceReturn;}
}

/* vim: set ts=8 sts=8 sw=8 noet: */
/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * fill-column: 78
 * End:
 */
