from sensors import Sensor


class Test:

    def __init__(self):
        Sensor(1)
        return

    def func1(self):
        Sensor(2)
        return


class TestSecond:

    def __init__(self):
        Sensor(3)
        return

    def func2(self):
        Sensor(4)
        return


    class TestThird(Test):

        def __init__(self):
            Sensor(5)
            Test.__init__(self)
            Sensor(6)

        def func3(self):
            Sensor(7)
            return
