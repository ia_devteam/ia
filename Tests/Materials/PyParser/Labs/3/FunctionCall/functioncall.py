from sensors import Sensor


Sensor(4)


def func1():
    Sensor(1)
    return


def func2(a):
    Sensor(2)
    return


def func3():
    Sensor(3)
    return


func2(func1())
func1()
Sensor(5)
