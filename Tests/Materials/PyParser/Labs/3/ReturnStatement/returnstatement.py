from sensors import Sensor


def func1():
    Sensor(1)
    return


def func2():
    py_ass_parser2 = 5
    Sensor(2)
    return py_ass_parser2


def func3():
    py_ass_parser3 = func1()
    Sensor(3)
    return py_ass_parser3


def func4():
    py_ass_parser4 = func1() == func2()
    Sensor(4)
    return py_ass_parser4
