from sensors import Sensor


Sensor(2628)
"""Wrapper functions for Tcl/Tk.

Tkinter provides classes which allow the display, positioning and
control of widgets. Toplevel widgets are Tk and Toplevel. Other
widgets are Frame, Label, Entry, Text, Canvas, Button, Radiobutton,
Checkbutton, Scale, Listbox, Scrollbar, OptionMenu, Spinbox
LabelFrame and PanedWindow.

Properties of the widgets are specified with keyword arguments.
Keyword arguments have the same name as the corresponding resource
under Tk.

Widgets are positioned with one of the geometry managers Place, Pack
or Grid. These managers can be called with methods place, pack, grid
available in every Widget.

Actions are bound to events by resources (e.g. keyword argument
command) or with the method bind.

Example (Hello, World):
import Tkinter
from Tkconstants import *
tk = Tkinter.Tk()
frame = Tkinter.Frame(tk, relief=RIDGE, borderwidth=2)
frame.pack(fill=BOTH,expand=1)
label = Tkinter.Label(frame, text="Hello, World")
label.pack(fill=X, expand=1)
button = Tkinter.Button(frame,text="Exit",command=tk.destroy)
button.pack(side=BOTTOM)
tk.mainloop()
"""
__version__ = '$Revision: 81008 $'
import sys
if sys.platform == 'win32':
    Sensor(1561)
    import FixTk
import _tkinter
tkinter = _tkinter
TclError = _tkinter.TclError
from types import *
from Tkconstants import *
import re
wantobjects = 1
TkVersion = float(_tkinter.TK_VERSION)
TclVersion = float(_tkinter.TCL_VERSION)
READABLE = _tkinter.READABLE
WRITABLE = _tkinter.WRITABLE
EXCEPTION = _tkinter.EXCEPTION
try:
    _tkinter.createfilehandler
except AttributeError:
    _tkinter.createfilehandler = None
try:
    _tkinter.deletefilehandler
except AttributeError:
    _tkinter.deletefilehandler = None
_magic_re = re.compile('([\\\\{}])')
_space_re = re.compile('([\\s])')


def _join(value):
    Sensor(1562)
    """Internal function."""
    py_ass_parser1563 = ' '.join(map(_stringify, value))
    Sensor(1563)
    return py_ass_parser1563


def _stringify(value):
    Sensor(1564)
    """Internal function."""
    if isinstance(value, (list, tuple)):
        if len(value) == 1:
            value = _stringify(value[0])
            if value[0] == '{':
                value = '{%s}' % value
        else:
            value = '{%s}' % _join(value)
    else:
        if isinstance(value, basestring):
            value = unicode(value)
        else:
            value = str(value)
        if not value:
            value = '{}'
        elif _magic_re.search(value):
            value = _magic_re.sub('\\\\\\1', value)
            value = _space_re.sub('\\\\\\1', value)
        elif value[0] == '"' or _space_re.search(value):
            value = '{%s}' % value
    py_ass_parser1565 = value
    Sensor(1565)
    return py_ass_parser1565


def _flatten(tuple):
    Sensor(1566)
    """Internal function."""
    res = ()
    for item in tuple:
        if type(item) in (TupleType, ListType):
            res = res + _flatten(item)
        elif item is not None:
            res = res + (item,)
    py_ass_parser1567 = res
    Sensor(1567)
    return py_ass_parser1567


try:
    _flatten = _tkinter._flatten
except AttributeError:
    pass


def _cnfmerge(cnfs):
    Sensor(1568)
    """Internal function."""
    if type(cnfs) is DictionaryType:
        py_ass_parser1569 = cnfs
        Sensor(1569)
        return py_ass_parser1569
    elif type(cnfs) in (NoneType, StringType):
        py_ass_parser1570 = cnfs
        Sensor(1570)
        return py_ass_parser1570
    else:
        cnf = {}
        for c in _flatten(cnfs):
            try:
                cnf.update(c)
            except (AttributeError, TypeError) as msg:
                print '_cnfmerge: fallback due to:', msg
                for k, v in c.items():
                    cnf[k] = v
        py_ass_parser1571 = cnf
        Sensor(1571)
        return py_ass_parser1571
    Sensor(1572)


Sensor(1573)
try:
    _cnfmerge = _tkinter._cnfmerge
except AttributeError:
    pass


class Event:
    """Container for the properties of an event.

    Instances of this type are generated if one of the following events occurs:

    KeyPress, KeyRelease - for keyboard events
    ButtonPress, ButtonRelease, Motion, Enter, Leave, MouseWheel - for mouse events
    Visibility, Unmap, Map, Expose, FocusIn, FocusOut, Circulate,
    Colormap, Gravity, Reparent, Property, Destroy, Activate,
    Deactivate - for window events.

    If a callback function for one of these events is registered
    using bind, bind_all, bind_class, or tag_bind, the callback is
    called with an Event as first argument. It will have the
    following attributes (in braces are the event types for which
    the attribute is valid):

        serial - serial number of event
    num - mouse button pressed (ButtonPress, ButtonRelease)
    focus - whether the window has the focus (Enter, Leave)
    height - height of the exposed window (Configure, Expose)
    width - width of the exposed window (Configure, Expose)
    keycode - keycode of the pressed key (KeyPress, KeyRelease)
    state - state of the event as a number (ButtonPress, ButtonRelease,
                            Enter, KeyPress, KeyRelease,
                            Leave, Motion)
    state - state as a string (Visibility)
    time - when the event occurred
    x - x-position of the mouse
    y - y-position of the mouse
    x_root - x-position of the mouse on the screen
             (ButtonPress, ButtonRelease, KeyPress, KeyRelease, Motion)
    y_root - y-position of the mouse on the screen
             (ButtonPress, ButtonRelease, KeyPress, KeyRelease, Motion)
    char - pressed character (KeyPress, KeyRelease)
    send_event - see X/Windows documentation
    keysym - keysym of the event as a string (KeyPress, KeyRelease)
    keysym_num - keysym of the event as a number (KeyPress, KeyRelease)
    type - type of the event as a number
    widget - widget in which the event occurred
    delta - delta of wheel movement (MouseWheel)
    """
    pass


_support_default_root = 1
_default_root = None


def NoDefaultRoot():
    Sensor(1574)
    """Inhibit setting of default root window.

    Call this function to inhibit that the first instance of
    Tk is used for windows without an explicit parent window.
    """
    global _support_default_root
    _support_default_root = 0
    global _default_root
    _default_root = None
    del _default_root
    Sensor(1575)


def _tkerror(err):
    Sensor(1576)
    """Internal function."""
    pass
    Sensor(1577)


def _exit(code=0):
    Sensor(1578)
    """Internal function. Calling it will raise the exception SystemExit."""
    try:
        code = int(code)
    except ValueError:
        pass
    raise SystemExit, code


_varnum = 0


class Variable:
    """Class to define value holders for e.g. buttons.

    Subclasses StringVar, IntVar, DoubleVar, BooleanVar are specializations
    that constrain the type of the value returned from get()."""
    _default = ''

    def __init__(self, master=None, value=None, name=None):
        Sensor(1579)
        """Construct a variable

        MASTER can be given as master widget.
        VALUE is an optional value (defaults to "")
        NAME is an optional Tcl name (defaults to PY_VARnum).

        If NAME matches an existing variable and VALUE is omitted
        then the existing value is retained.
        """
        global _varnum
        if not master:
            master = _default_root
        self._master = master
        self._tk = master.tk
        if name:
            self._name = name
        else:
            self._name = 'PY_VAR' + repr(_varnum)
            _varnum += 1
        if value is not None:
            self.set(value)
        elif not self._tk.call('info', 'exists', self._name):
            self.set(self._default)
        Sensor(1580)

    def __del__(self):
        Sensor(1581)
        """Unset the variable in Tcl."""
        self._tk.globalunsetvar(self._name)
        Sensor(1582)

    def __str__(self):
        Sensor(1583)
        """Return the name of the variable in Tcl."""
        py_ass_parser1584 = self._name
        Sensor(1584)
        return py_ass_parser1584

    def set(self, value):
        Sensor(1585)
        """Set the variable to VALUE."""
        py_ass_parser1586 = self._tk.globalsetvar(self._name, value)
        Sensor(1586)
        return py_ass_parser1586

    def get(self):
        Sensor(1587)
        """Return value of variable."""
        py_ass_parser1588 = self._tk.globalgetvar(self._name)
        Sensor(1588)
        return py_ass_parser1588

    def trace_variable(self, mode, callback):
        Sensor(1589)
        """Define a trace callback for the variable.

        MODE is one of "r", "w", "u" for read, write, undefine.
        CALLBACK must be a function which is called when
        the variable is read, written or undefined.

        Return the name of the callback.
        """
        cbname = self._master._register(callback)
        self._tk.call('trace', 'variable', self._name, mode, cbname)
        py_ass_parser1590 = cbname
        Sensor(1590)
        return py_ass_parser1590
    trace = trace_variable

    def trace_vdelete(self, mode, cbname):
        Sensor(1591)
        """Delete the trace callback for a variable.

        MODE is one of "r", "w", "u" for read, write, undefine.
        CBNAME is the name of the callback returned from trace_variable or trace.
        """
        self._tk.call('trace', 'vdelete', self._name, mode, cbname)
        self._master.deletecommand(cbname)
        Sensor(1592)

    def trace_vinfo(self):
        Sensor(1593)
        """Return all trace callback information."""
        py_ass_parser1594 = map(self._tk.split, self._tk.splitlist(self._tk
            .call('trace', 'vinfo', self._name)))
        Sensor(1594)
        return py_ass_parser1594

    def __eq__(self, other):
        Sensor(1595)
        """Comparison for equality (==).

        Note: if the Variable's master matters to behavior
        also compare self._master == other._master
        """
        py_ass_parser1596 = (self.__class__.__name__ == other.__class__.
            __name__ and self._name == other._name)
        Sensor(1596)
        return py_ass_parser1596


class StringVar(Variable):
    """Value holder for strings variables."""
    _default = ''

    def __init__(self, master=None, value=None, name=None):
        Sensor(1597)
        """Construct a string variable.

        MASTER can be given as master widget.
        VALUE is an optional value (defaults to "")
        NAME is an optional Tcl name (defaults to PY_VARnum).

        If NAME matches an existing variable and VALUE is omitted
        then the existing value is retained.
        """
        Variable.__init__(self, master, value, name)
        Sensor(1598)

    def get(self):
        Sensor(1599)
        """Return value of variable as string."""
        value = self._tk.globalgetvar(self._name)
        if isinstance(value, basestring):
            py_ass_parser1600 = value
            Sensor(1600)
            return py_ass_parser1600
        py_ass_parser1601 = str(value)
        Sensor(1601)
        return py_ass_parser1601


class IntVar(Variable):
    """Value holder for integer variables."""
    _default = 0

    def __init__(self, master=None, value=None, name=None):
        Sensor(1602)
        """Construct an integer variable.

        MASTER can be given as master widget.
        VALUE is an optional value (defaults to 0)
        NAME is an optional Tcl name (defaults to PY_VARnum).

        If NAME matches an existing variable and VALUE is omitted
        then the existing value is retained.
        """
        Variable.__init__(self, master, value, name)
        Sensor(1603)

    def set(self, value):
        Sensor(1604)
        """Set the variable to value, converting booleans to integers."""
        if isinstance(value, bool):
            value = int(value)
        py_ass_parser1605 = Variable.set(self, value)
        Sensor(1605)
        return py_ass_parser1605

    def get(self):
        Sensor(1606)
        """Return the value of the variable as an integer."""
        py_ass_parser1607 = getint(self._tk.globalgetvar(self._name))
        Sensor(1607)
        return py_ass_parser1607


class DoubleVar(Variable):
    """Value holder for float variables."""
    _default = 0.0

    def __init__(self, master=None, value=None, name=None):
        Sensor(1608)
        """Construct a float variable.

        MASTER can be given as master widget.
        VALUE is an optional value (defaults to 0.0)
        NAME is an optional Tcl name (defaults to PY_VARnum).

        If NAME matches an existing variable and VALUE is omitted
        then the existing value is retained.
        """
        Variable.__init__(self, master, value, name)
        Sensor(1609)

    def get(self):
        Sensor(1610)
        """Return the value of the variable as a float."""
        py_ass_parser1611 = getdouble(self._tk.globalgetvar(self._name))
        Sensor(1611)
        return py_ass_parser1611


class BooleanVar(Variable):
    """Value holder for boolean variables."""
    _default = False

    def __init__(self, master=None, value=None, name=None):
        Sensor(1612)
        """Construct a boolean variable.

        MASTER can be given as master widget.
        VALUE is an optional value (defaults to False)
        NAME is an optional Tcl name (defaults to PY_VARnum).

        If NAME matches an existing variable and VALUE is omitted
        then the existing value is retained.
        """
        Variable.__init__(self, master, value, name)
        Sensor(1613)

    def get(self):
        Sensor(1614)
        """Return the value of the variable as a bool."""
        py_ass_parser1615 = self._tk.getboolean(self._tk.globalgetvar(self.
            _name))
        Sensor(1615)
        return py_ass_parser1615


def mainloop(n=0):
    Sensor(1616)
    """Run the main loop of Tcl."""
    _default_root.tk.mainloop(n)
    Sensor(1617)


getint = int
getdouble = float


def getboolean(s):
    Sensor(1618)
    """Convert true and false to integer values 1 and 0."""
    py_ass_parser1619 = _default_root.tk.getboolean(s)
    Sensor(1619)
    return py_ass_parser1619


class Misc:
    """Internal class.

    Base class which defines methods common for interior widgets."""
    _tclCommands = None

    def destroy(self):
        Sensor(1620)
        """Internal function.

        Delete all Tcl commands created for
        this widget in the Tcl interpreter."""
        if self._tclCommands is not None:
            for name in self._tclCommands:
                self.tk.deletecommand(name)
            self._tclCommands = None
        Sensor(1621)

    def deletecommand(self, name):
        Sensor(1622)
        """Internal function.

        Delete the Tcl command provided in NAME."""
        self.tk.deletecommand(name)
        try:
            self._tclCommands.remove(name)
        except ValueError:
            pass
        Sensor(1623)

    def tk_strictMotif(self, boolean=None):
        Sensor(1624)
        """Set Tcl internal variable, whether the look and feel
        should adhere to Motif.

        A parameter of 1 means adhere to Motif (e.g. no color
        change if mouse passes over slider).
        Returns the set value."""
        py_ass_parser1625 = self.tk.getboolean(self.tk.call('set',
            'tk_strictMotif', boolean))
        Sensor(1625)
        return py_ass_parser1625

    def tk_bisque(self):
        Sensor(1626)
        """Change the color scheme to light brown as used in Tk 3.6 and before."""
        self.tk.call('tk_bisque')
        Sensor(1627)

    def tk_setPalette(self, *args, **kw):
        Sensor(1628)
        """Set a new color scheme for all widget elements.

        A single color as argument will cause that all colors of Tk
        widget elements are derived from this.
        Alternatively several keyword parameters and its associated
        colors can be given. The following keywords are valid:
        activeBackground, foreground, selectColor,
        activeForeground, highlightBackground, selectBackground,
        background, highlightColor, selectForeground,
        disabledForeground, insertBackground, troughColor."""
        self.tk.call(('tk_setPalette',) + _flatten(args) + _flatten(kw.items())
            )
        Sensor(1629)

    def tk_menuBar(self, *args):
        Sensor(1630)
        """Do not use. Needed in Tk 3.6 and earlier."""
        pass
        Sensor(1631)

    def wait_variable(self, name='PY_VAR'):
        Sensor(1632)
        """Wait until the variable is modified.

        A parameter of type IntVar, StringVar, DoubleVar or
        BooleanVar must be given."""
        self.tk.call('tkwait', 'variable', name)
        Sensor(1633)
    waitvar = wait_variable

    def wait_window(self, window=None):
        Sensor(1634)
        """Wait until a WIDGET is destroyed.

        If no parameter is given self is used."""
        if window is None:
            window = self
        self.tk.call('tkwait', 'window', window._w)
        Sensor(1635)

    def wait_visibility(self, window=None):
        Sensor(1636)
        """Wait until the visibility of a WIDGET changes
        (e.g. it appears).

        If no parameter is given self is used."""
        if window is None:
            window = self
        self.tk.call('tkwait', 'visibility', window._w)
        Sensor(1637)

    def setvar(self, name='PY_VAR', value='1'):
        Sensor(1638)
        """Set Tcl variable NAME to VALUE."""
        self.tk.setvar(name, value)
        Sensor(1639)

    def getvar(self, name='PY_VAR'):
        Sensor(1640)
        """Return value of Tcl variable NAME."""
        py_ass_parser1641 = self.tk.getvar(name)
        Sensor(1641)
        return py_ass_parser1641
    getint = int
    getdouble = float

    def getboolean(self, s):
        Sensor(1642)
        """Return a boolean value for Tcl boolean values true and false given as parameter."""
        py_ass_parser1643 = self.tk.getboolean(s)
        Sensor(1643)
        return py_ass_parser1643

    def focus_set(self):
        Sensor(1644)
        """Direct input focus to this widget.

        If the application currently does not have the focus
        this widget will get the focus if the application gets
        the focus through the window manager."""
        self.tk.call('focus', self._w)
        Sensor(1645)
    focus = focus_set

    def focus_force(self):
        Sensor(1646)
        """Direct input focus to this widget even if the
        application does not have the focus. Use with
        caution!"""
        self.tk.call('focus', '-force', self._w)
        Sensor(1647)

    def focus_get(self):
        Sensor(1648)
        """Return the widget which has currently the focus in the
        application.

        Use focus_displayof to allow working with several
        displays. Return None if application does not have
        the focus."""
        name = self.tk.call('focus')
        if name == 'none' or not name:
            py_ass_parser1649 = None
            Sensor(1649)
            return py_ass_parser1649
        py_ass_parser1650 = self._nametowidget(name)
        Sensor(1650)
        return py_ass_parser1650

    def focus_displayof(self):
        Sensor(1651)
        """Return the widget which has currently the focus on the
        display where this widget is located.

        Return None if the application does not have the focus."""
        name = self.tk.call('focus', '-displayof', self._w)
        if name == 'none' or not name:
            py_ass_parser1652 = None
            Sensor(1652)
            return py_ass_parser1652
        py_ass_parser1653 = self._nametowidget(name)
        Sensor(1653)
        return py_ass_parser1653

    def focus_lastfor(self):
        Sensor(1654)
        """Return the widget which would have the focus if top level
        for this widget gets the focus from the window manager."""
        name = self.tk.call('focus', '-lastfor', self._w)
        if name == 'none' or not name:
            py_ass_parser1655 = None
            Sensor(1655)
            return py_ass_parser1655
        py_ass_parser1656 = self._nametowidget(name)
        Sensor(1656)
        return py_ass_parser1656

    def tk_focusFollowsMouse(self):
        Sensor(1657)
        """The widget under mouse will get automatically focus. Can not
        be disabled easily."""
        self.tk.call('tk_focusFollowsMouse')
        Sensor(1658)

    def tk_focusNext(self):
        Sensor(1659)
        """Return the next widget in the focus order which follows
        widget which has currently the focus.

        The focus order first goes to the next child, then to
        the children of the child recursively and then to the
        next sibling which is higher in the stacking order.  A
        widget is omitted if it has the takefocus resource set
        to 0."""
        name = self.tk.call('tk_focusNext', self._w)
        if not name:
            py_ass_parser1660 = None
            Sensor(1660)
            return py_ass_parser1660
        py_ass_parser1661 = self._nametowidget(name)
        Sensor(1661)
        return py_ass_parser1661

    def tk_focusPrev(self):
        Sensor(1662)
        """Return previous widget in the focus order. See tk_focusNext for details."""
        name = self.tk.call('tk_focusPrev', self._w)
        if not name:
            py_ass_parser1663 = None
            Sensor(1663)
            return py_ass_parser1663
        py_ass_parser1664 = self._nametowidget(name)
        Sensor(1664)
        return py_ass_parser1664

    def after(self, ms, func=None, *args):
        Sensor(1665)
        """Call function once after given time.

        MS specifies the time in milliseconds. FUNC gives the
        function which shall be called. Additional parameters
        are given as parameters to the function call.  Return
        identifier to cancel scheduling with after_cancel."""
        if not func:
            self.tk.call('after', ms)
        else:

            def callit():
                Sensor(1666)
                try:
                    func(*args)
                finally:
                    try:
                        self.deletecommand(name)
                    except TclError:
                        pass
                Sensor(1667)
            Sensor(1668)
            name = self._register(callit)
            py_ass_parser1669 = self.tk.call('after', ms, name)
            Sensor(1669)
            return py_ass_parser1669
        Sensor(1670)

    def after_idle(self, func, *args):
        Sensor(1671)
        """Call FUNC once if the Tcl main loop has no event to
        process.

        Return an identifier to cancel the scheduling with
        after_cancel."""
        py_ass_parser1672 = self.after('idle', func, *args)
        Sensor(1672)
        return py_ass_parser1672

    def after_cancel(self, id):
        Sensor(1673)
        """Cancel scheduling of function identified with ID.

        Identifier returned by after or after_idle must be
        given as first parameter."""
        try:
            data = self.tk.call('after', 'info', id)
            script = self.tk.splitlist(data)[0]
            self.deletecommand(script)
        except TclError:
            pass
        self.tk.call('after', 'cancel', id)
        Sensor(1674)

    def bell(self, displayof=0):
        Sensor(1675)
        """Ring a display's bell."""
        self.tk.call(('bell',) + self._displayof(displayof))
        Sensor(1676)

    def clipboard_get(self, **kw):
        Sensor(1677)
        """Retrieve data from the clipboard on window's display.

        The window keyword defaults to the root window of the Tkinter
        application.

        The type keyword specifies the form in which the data is
        to be returned and should be an atom name such as STRING
        or FILE_NAME.  Type defaults to STRING, except on X11, where the default
        is to try UTF8_STRING and fall back to STRING.

        This command is equivalent to:

        selection_get(CLIPBOARD)
        """
        if 'type' not in kw and self._windowingsystem == 'x11':
            try:
                kw['type'] = 'UTF8_STRING'
                py_ass_parser1678 = self.tk.call(('clipboard', 'get') +
                    self._options(kw))
                Sensor(1678)
                return py_ass_parser1678
            except TclError:
                del kw['type']
        py_ass_parser1679 = self.tk.call(('clipboard', 'get') + self.
            _options(kw))
        Sensor(1679)
        return py_ass_parser1679

    def clipboard_clear(self, **kw):
        Sensor(1680)
        """Clear the data in the Tk clipboard.

        A widget specified for the optional displayof keyword
        argument specifies the target display."""
        if 'displayof' not in kw:
            kw['displayof'] = self._w
        self.tk.call(('clipboard', 'clear') + self._options(kw))
        Sensor(1681)

    def clipboard_append(self, string, **kw):
        Sensor(1682)
        """Append STRING to the Tk clipboard.

        A widget specified at the optional displayof keyword
        argument specifies the target display. The clipboard
        can be retrieved with selection_get."""
        if 'displayof' not in kw:
            kw['displayof'] = self._w
        self.tk.call(('clipboard', 'append') + self._options(kw) + ('--',
            string))
        Sensor(1683)

    def grab_current(self):
        Sensor(1684)
        """Return widget which has currently the grab in this application
        or None."""
        name = self.tk.call('grab', 'current', self._w)
        if not name:
            py_ass_parser1685 = None
            Sensor(1685)
            return py_ass_parser1685
        py_ass_parser1686 = self._nametowidget(name)
        Sensor(1686)
        return py_ass_parser1686

    def grab_release(self):
        Sensor(1687)
        """Release grab for this widget if currently set."""
        self.tk.call('grab', 'release', self._w)
        Sensor(1688)

    def grab_set(self):
        Sensor(1689)
        """Set grab for this widget.

        A grab directs all events to this and descendant
        widgets in the application."""
        self.tk.call('grab', 'set', self._w)
        Sensor(1690)

    def grab_set_global(self):
        Sensor(1691)
        """Set global grab for this widget.

        A global grab directs all events to this and
        descendant widgets on the display. Use with caution -
        other applications do not get events anymore."""
        self.tk.call('grab', 'set', '-global', self._w)
        Sensor(1692)

    def grab_status(self):
        Sensor(1693)
        """Return None, "local" or "global" if this widget has
        no, a local or a global grab."""
        status = self.tk.call('grab', 'status', self._w)
        if status == 'none':
            status = None
        py_ass_parser1694 = status
        Sensor(1694)
        return py_ass_parser1694

    def option_add(self, pattern, value, priority=None):
        Sensor(1695)
        """Set a VALUE (second parameter) for an option
        PATTERN (first parameter).

        An optional third parameter gives the numeric priority
        (defaults to 80)."""
        self.tk.call('option', 'add', pattern, value, priority)
        Sensor(1696)

    def option_clear(self):
        Sensor(1697)
        """Clear the option database.

        It will be reloaded if option_add is called."""
        self.tk.call('option', 'clear')
        Sensor(1698)

    def option_get(self, name, className):
        Sensor(1699)
        """Return the value for an option NAME for this widget
        with CLASSNAME.

        Values with higher priority override lower values."""
        py_ass_parser1700 = self.tk.call('option', 'get', self._w, name,
            className)
        Sensor(1700)
        return py_ass_parser1700

    def option_readfile(self, fileName, priority=None):
        Sensor(1701)
        """Read file FILENAME into the option database.

        An optional second parameter gives the numeric
        priority."""
        self.tk.call('option', 'readfile', fileName, priority)
        Sensor(1702)

    def selection_clear(self, **kw):
        Sensor(1703)
        """Clear the current X selection."""
        if 'displayof' not in kw:
            kw['displayof'] = self._w
        self.tk.call(('selection', 'clear') + self._options(kw))
        Sensor(1704)

    def selection_get(self, **kw):
        Sensor(1705)
        """Return the contents of the current X selection.

        A keyword parameter selection specifies the name of
        the selection and defaults to PRIMARY.  A keyword
        parameter displayof specifies a widget on the display
        to use. A keyword parameter type specifies the form of data to be
        fetched, defaulting to STRING except on X11, where UTF8_STRING is tried
        before STRING."""
        if 'displayof' not in kw:
            kw['displayof'] = self._w
        if 'type' not in kw and self._windowingsystem == 'x11':
            try:
                kw['type'] = 'UTF8_STRING'
                py_ass_parser1706 = self.tk.call(('selection', 'get') +
                    self._options(kw))
                Sensor(1706)
                return py_ass_parser1706
            except TclError:
                del kw['type']
        py_ass_parser1707 = self.tk.call(('selection', 'get') + self.
            _options(kw))
        Sensor(1707)
        return py_ass_parser1707

    def selection_handle(self, command, **kw):
        Sensor(1708)
        """Specify a function COMMAND to call if the X
        selection owned by this widget is queried by another
        application.

        This function must return the contents of the
        selection. The function will be called with the
        arguments OFFSET and LENGTH which allows the chunking
        of very long selections. The following keyword
        parameters can be provided:
        selection - name of the selection (default PRIMARY),
        type - type of the selection (e.g. STRING, FILE_NAME)."""
        name = self._register(command)
        self.tk.call(('selection', 'handle') + self._options(kw) + (self._w,
            name))
        Sensor(1709)

    def selection_own(self, **kw):
        Sensor(1710)
        """Become owner of X selection.

        A keyword parameter selection specifies the name of
        the selection (default PRIMARY)."""
        self.tk.call(('selection', 'own') + self._options(kw) + (self._w,))
        Sensor(1711)

    def selection_own_get(self, **kw):
        Sensor(1712)
        """Return owner of X selection.

        The following keyword parameter can
        be provided:
        selection - name of the selection (default PRIMARY),
        type - type of the selection (e.g. STRING, FILE_NAME)."""
        if 'displayof' not in kw:
            kw['displayof'] = self._w
        name = self.tk.call(('selection', 'own') + self._options(kw))
        if not name:
            py_ass_parser1713 = None
            Sensor(1713)
            return py_ass_parser1713
        py_ass_parser1714 = self._nametowidget(name)
        Sensor(1714)
        return py_ass_parser1714

    def send(self, interp, cmd, *args):
        Sensor(1715)
        """Send Tcl command CMD to different interpreter INTERP to be executed."""
        py_ass_parser1716 = self.tk.call(('send', interp, cmd) + args)
        Sensor(1716)
        return py_ass_parser1716

    def lower(self, belowThis=None):
        Sensor(1717)
        """Lower this widget in the stacking order."""
        self.tk.call('lower', self._w, belowThis)
        Sensor(1718)

    def tkraise(self, aboveThis=None):
        Sensor(1719)
        """Raise this widget in the stacking order."""
        self.tk.call('raise', self._w, aboveThis)
        Sensor(1720)
    lift = tkraise

    def colormodel(self, value=None):
        Sensor(1721)
        """Useless. Not implemented in Tk."""
        py_ass_parser1722 = self.tk.call('tk', 'colormodel', self._w, value)
        Sensor(1722)
        return py_ass_parser1722

    def winfo_atom(self, name, displayof=0):
        Sensor(1723)
        """Return integer which represents atom NAME."""
        args = ('winfo', 'atom') + self._displayof(displayof) + (name,)
        py_ass_parser1724 = getint(self.tk.call(args))
        Sensor(1724)
        return py_ass_parser1724

    def winfo_atomname(self, id, displayof=0):
        Sensor(1725)
        """Return name of atom with identifier ID."""
        args = ('winfo', 'atomname') + self._displayof(displayof) + (id,)
        py_ass_parser1726 = self.tk.call(args)
        Sensor(1726)
        return py_ass_parser1726

    def winfo_cells(self):
        Sensor(1727)
        """Return number of cells in the colormap for this widget."""
        py_ass_parser1728 = getint(self.tk.call('winfo', 'cells', self._w))
        Sensor(1728)
        return py_ass_parser1728

    def winfo_children(self):
        Sensor(1729)
        """Return a list of all widgets which are children of this widget."""
        result = []
        for child in self.tk.splitlist(self.tk.call('winfo', 'children',
            self._w)):
            try:
                result.append(self._nametowidget(child))
            except KeyError:
                pass
        py_ass_parser1730 = result
        Sensor(1730)
        return py_ass_parser1730

    def winfo_class(self):
        Sensor(1731)
        """Return window class name of this widget."""
        py_ass_parser1732 = self.tk.call('winfo', 'class', self._w)
        Sensor(1732)
        return py_ass_parser1732

    def winfo_colormapfull(self):
        Sensor(1733)
        """Return true if at the last color request the colormap was full."""
        py_ass_parser1734 = self.tk.getboolean(self.tk.call('winfo',
            'colormapfull', self._w))
        Sensor(1734)
        return py_ass_parser1734

    def winfo_containing(self, rootX, rootY, displayof=0):
        Sensor(1735)
        """Return the widget which is at the root coordinates ROOTX, ROOTY."""
        args = ('winfo', 'containing') + self._displayof(displayof) + (rootX,
            rootY)
        name = self.tk.call(args)
        if not name:
            py_ass_parser1736 = None
            Sensor(1736)
            return py_ass_parser1736
        py_ass_parser1737 = self._nametowidget(name)
        Sensor(1737)
        return py_ass_parser1737

    def winfo_depth(self):
        Sensor(1738)
        """Return the number of bits per pixel."""
        py_ass_parser1739 = getint(self.tk.call('winfo', 'depth', self._w))
        Sensor(1739)
        return py_ass_parser1739

    def winfo_exists(self):
        Sensor(1740)
        """Return true if this widget exists."""
        py_ass_parser1741 = getint(self.tk.call('winfo', 'exists', self._w))
        Sensor(1741)
        return py_ass_parser1741

    def winfo_fpixels(self, number):
        Sensor(1742)
        """Return the number of pixels for the given distance NUMBER
        (e.g. "3c") as float."""
        py_ass_parser1743 = getdouble(self.tk.call('winfo', 'fpixels', self
            ._w, number))
        Sensor(1743)
        return py_ass_parser1743

    def winfo_geometry(self):
        Sensor(1744)
        """Return geometry string for this widget in the form "widthxheight+X+Y"."""
        py_ass_parser1745 = self.tk.call('winfo', 'geometry', self._w)
        Sensor(1745)
        return py_ass_parser1745

    def winfo_height(self):
        Sensor(1746)
        """Return height of this widget."""
        py_ass_parser1747 = getint(self.tk.call('winfo', 'height', self._w))
        Sensor(1747)
        return py_ass_parser1747

    def winfo_id(self):
        Sensor(1748)
        """Return identifier ID for this widget."""
        py_ass_parser1749 = self.tk.getint(self.tk.call('winfo', 'id', self._w)
            )
        Sensor(1749)
        return py_ass_parser1749

    def winfo_interps(self, displayof=0):
        Sensor(1750)
        """Return the name of all Tcl interpreters for this display."""
        args = ('winfo', 'interps') + self._displayof(displayof)
        py_ass_parser1751 = self.tk.splitlist(self.tk.call(args))
        Sensor(1751)
        return py_ass_parser1751

    def winfo_ismapped(self):
        Sensor(1752)
        """Return true if this widget is mapped."""
        py_ass_parser1753 = getint(self.tk.call('winfo', 'ismapped', self._w))
        Sensor(1753)
        return py_ass_parser1753

    def winfo_manager(self):
        Sensor(1754)
        """Return the window mananger name for this widget."""
        py_ass_parser1755 = self.tk.call('winfo', 'manager', self._w)
        Sensor(1755)
        return py_ass_parser1755

    def winfo_name(self):
        Sensor(1756)
        """Return the name of this widget."""
        py_ass_parser1757 = self.tk.call('winfo', 'name', self._w)
        Sensor(1757)
        return py_ass_parser1757

    def winfo_parent(self):
        Sensor(1758)
        """Return the name of the parent of this widget."""
        py_ass_parser1759 = self.tk.call('winfo', 'parent', self._w)
        Sensor(1759)
        return py_ass_parser1759

    def winfo_pathname(self, id, displayof=0):
        Sensor(1760)
        """Return the pathname of the widget given by ID."""
        args = ('winfo', 'pathname') + self._displayof(displayof) + (id,)
        py_ass_parser1761 = self.tk.call(args)
        Sensor(1761)
        return py_ass_parser1761

    def winfo_pixels(self, number):
        Sensor(1762)
        """Rounded integer value of winfo_fpixels."""
        py_ass_parser1763 = getint(self.tk.call('winfo', 'pixels', self._w,
            number))
        Sensor(1763)
        return py_ass_parser1763

    def winfo_pointerx(self):
        Sensor(1764)
        """Return the x coordinate of the pointer on the root window."""
        py_ass_parser1765 = getint(self.tk.call('winfo', 'pointerx', self._w))
        Sensor(1765)
        return py_ass_parser1765

    def winfo_pointerxy(self):
        Sensor(1766)
        """Return a tuple of x and y coordinates of the pointer on the root window."""
        py_ass_parser1767 = self._getints(self.tk.call('winfo', 'pointerxy',
            self._w))
        Sensor(1767)
        return py_ass_parser1767

    def winfo_pointery(self):
        Sensor(1768)
        """Return the y coordinate of the pointer on the root window."""
        py_ass_parser1769 = getint(self.tk.call('winfo', 'pointery', self._w))
        Sensor(1769)
        return py_ass_parser1769

    def winfo_reqheight(self):
        Sensor(1770)
        """Return requested height of this widget."""
        py_ass_parser1771 = getint(self.tk.call('winfo', 'reqheight', self._w))
        Sensor(1771)
        return py_ass_parser1771

    def winfo_reqwidth(self):
        Sensor(1772)
        """Return requested width of this widget."""
        py_ass_parser1773 = getint(self.tk.call('winfo', 'reqwidth', self._w))
        Sensor(1773)
        return py_ass_parser1773

    def winfo_rgb(self, color):
        Sensor(1774)
        """Return tuple of decimal values for red, green, blue for
        COLOR in this widget."""
        py_ass_parser1775 = self._getints(self.tk.call('winfo', 'rgb', self
            ._w, color))
        Sensor(1775)
        return py_ass_parser1775

    def winfo_rootx(self):
        Sensor(1776)
        """Return x coordinate of upper left corner of this widget on the
        root window."""
        py_ass_parser1777 = getint(self.tk.call('winfo', 'rootx', self._w))
        Sensor(1777)
        return py_ass_parser1777

    def winfo_rooty(self):
        Sensor(1778)
        """Return y coordinate of upper left corner of this widget on the
        root window."""
        py_ass_parser1779 = getint(self.tk.call('winfo', 'rooty', self._w))
        Sensor(1779)
        return py_ass_parser1779

    def winfo_screen(self):
        Sensor(1780)
        """Return the screen name of this widget."""
        py_ass_parser1781 = self.tk.call('winfo', 'screen', self._w)
        Sensor(1781)
        return py_ass_parser1781

    def winfo_screencells(self):
        Sensor(1782)
        """Return the number of the cells in the colormap of the screen
        of this widget."""
        py_ass_parser1783 = getint(self.tk.call('winfo', 'screencells',
            self._w))
        Sensor(1783)
        return py_ass_parser1783

    def winfo_screendepth(self):
        Sensor(1784)
        """Return the number of bits per pixel of the root window of the
        screen of this widget."""
        py_ass_parser1785 = getint(self.tk.call('winfo', 'screendepth',
            self._w))
        Sensor(1785)
        return py_ass_parser1785

    def winfo_screenheight(self):
        Sensor(1786)
        """Return the number of pixels of the height of the screen of this widget
        in pixel."""
        py_ass_parser1787 = getint(self.tk.call('winfo', 'screenheight',
            self._w))
        Sensor(1787)
        return py_ass_parser1787

    def winfo_screenmmheight(self):
        Sensor(1788)
        """Return the number of pixels of the height of the screen of
        this widget in mm."""
        py_ass_parser1789 = getint(self.tk.call('winfo', 'screenmmheight',
            self._w))
        Sensor(1789)
        return py_ass_parser1789

    def winfo_screenmmwidth(self):
        Sensor(1790)
        """Return the number of pixels of the width of the screen of
        this widget in mm."""
        py_ass_parser1791 = getint(self.tk.call('winfo', 'screenmmwidth',
            self._w))
        Sensor(1791)
        return py_ass_parser1791

    def winfo_screenvisual(self):
        Sensor(1792)
        """Return one of the strings directcolor, grayscale, pseudocolor,
        staticcolor, staticgray, or truecolor for the default
        colormodel of this screen."""
        py_ass_parser1793 = self.tk.call('winfo', 'screenvisual', self._w)
        Sensor(1793)
        return py_ass_parser1793

    def winfo_screenwidth(self):
        Sensor(1794)
        """Return the number of pixels of the width of the screen of
        this widget in pixel."""
        py_ass_parser1795 = getint(self.tk.call('winfo', 'screenwidth',
            self._w))
        Sensor(1795)
        return py_ass_parser1795

    def winfo_server(self):
        Sensor(1796)
        """Return information of the X-Server of the screen of this widget in
        the form "XmajorRminor vendor vendorVersion"."""
        py_ass_parser1797 = self.tk.call('winfo', 'server', self._w)
        Sensor(1797)
        return py_ass_parser1797

    def winfo_toplevel(self):
        Sensor(1798)
        """Return the toplevel widget of this widget."""
        py_ass_parser1799 = self._nametowidget(self.tk.call('winfo',
            'toplevel', self._w))
        Sensor(1799)
        return py_ass_parser1799

    def winfo_viewable(self):
        Sensor(1800)
        """Return true if the widget and all its higher ancestors are mapped."""
        py_ass_parser1801 = getint(self.tk.call('winfo', 'viewable', self._w))
        Sensor(1801)
        return py_ass_parser1801

    def winfo_visual(self):
        Sensor(1802)
        """Return one of the strings directcolor, grayscale, pseudocolor,
        staticcolor, staticgray, or truecolor for the
        colormodel of this widget."""
        py_ass_parser1803 = self.tk.call('winfo', 'visual', self._w)
        Sensor(1803)
        return py_ass_parser1803

    def winfo_visualid(self):
        Sensor(1804)
        """Return the X identifier for the visual for this widget."""
        py_ass_parser1805 = self.tk.call('winfo', 'visualid', self._w)
        Sensor(1805)
        return py_ass_parser1805

    def winfo_visualsavailable(self, includeids=0):
        Sensor(1806)
        """Return a list of all visuals available for the screen
        of this widget.

        Each item in the list consists of a visual name (see winfo_visual), a
        depth and if INCLUDEIDS=1 is given also the X identifier."""
        data = self.tk.split(self.tk.call('winfo', 'visualsavailable', self
            ._w, includeids and 'includeids' or None))
        if type(data) is StringType:
            data = [self.tk.split(data)]
        py_ass_parser1807 = map(self.__winfo_parseitem, data)
        Sensor(1807)
        return py_ass_parser1807

    def __winfo_parseitem(self, t):
        Sensor(1808)
        """Internal function."""
        py_ass_parser1809 = t[:1] + tuple(map(self.__winfo_getint, t[1:]))
        Sensor(1809)
        return py_ass_parser1809

    def __winfo_getint(self, x):
        Sensor(1810)
        """Internal function."""
        py_ass_parser1811 = int(x, 0)
        Sensor(1811)
        return py_ass_parser1811

    def winfo_vrootheight(self):
        Sensor(1812)
        """Return the height of the virtual root window associated with this
        widget in pixels. If there is no virtual root window return the
        height of the screen."""
        py_ass_parser1813 = getint(self.tk.call('winfo', 'vrootheight',
            self._w))
        Sensor(1813)
        return py_ass_parser1813

    def winfo_vrootwidth(self):
        Sensor(1814)
        """Return the width of the virtual root window associated with this
        widget in pixel. If there is no virtual root window return the
        width of the screen."""
        py_ass_parser1815 = getint(self.tk.call('winfo', 'vrootwidth', self._w)
            )
        Sensor(1815)
        return py_ass_parser1815

    def winfo_vrootx(self):
        Sensor(1816)
        """Return the x offset of the virtual root relative to the root
        window of the screen of this widget."""
        py_ass_parser1817 = getint(self.tk.call('winfo', 'vrootx', self._w))
        Sensor(1817)
        return py_ass_parser1817

    def winfo_vrooty(self):
        Sensor(1818)
        """Return the y offset of the virtual root relative to the root
        window of the screen of this widget."""
        py_ass_parser1819 = getint(self.tk.call('winfo', 'vrooty', self._w))
        Sensor(1819)
        return py_ass_parser1819

    def winfo_width(self):
        Sensor(1820)
        """Return the width of this widget."""
        py_ass_parser1821 = getint(self.tk.call('winfo', 'width', self._w))
        Sensor(1821)
        return py_ass_parser1821

    def winfo_x(self):
        Sensor(1822)
        """Return the x coordinate of the upper left corner of this widget
        in the parent."""
        py_ass_parser1823 = getint(self.tk.call('winfo', 'x', self._w))
        Sensor(1823)
        return py_ass_parser1823

    def winfo_y(self):
        Sensor(1824)
        """Return the y coordinate of the upper left corner of this widget
        in the parent."""
        py_ass_parser1825 = getint(self.tk.call('winfo', 'y', self._w))
        Sensor(1825)
        return py_ass_parser1825

    def update(self):
        Sensor(1826)
        """Enter event loop until all pending events have been processed by Tcl."""
        self.tk.call('update')
        Sensor(1827)

    def update_idletasks(self):
        Sensor(1828)
        """Enter event loop until all idle callbacks have been called. This
        will update the display of windows but not process events caused by
        the user."""
        self.tk.call('update', 'idletasks')
        Sensor(1829)

    def bindtags(self, tagList=None):
        Sensor(1830)
        """Set or get the list of bindtags for this widget.

        With no argument return the list of all bindtags associated with
        this widget. With a list of strings as argument the bindtags are
        set to this list. The bindtags determine in which order events are
        processed (see bind)."""
        if tagList is None:
            py_ass_parser1831 = self.tk.splitlist(self.tk.call('bindtags',
                self._w))
            Sensor(1831)
            return py_ass_parser1831
        else:
            self.tk.call('bindtags', self._w, tagList)
        Sensor(1832)

    def _bind(self, what, sequence, func, add, needcleanup=1):
        Sensor(1833)
        """Internal function."""
        if type(func) is StringType:
            self.tk.call(what + (sequence, func))
        elif func:
            funcid = self._register(func, self._substitute, needcleanup)
            cmd = '%sif {"[%s %s]" == "break"} break\n' % (add and '+' or
                '', funcid, self._subst_format_str)
            self.tk.call(what + (sequence, cmd))
            py_ass_parser1834 = funcid
            Sensor(1834)
            return py_ass_parser1834
        elif sequence:
            py_ass_parser1835 = self.tk.call(what + (sequence,))
            Sensor(1835)
            return py_ass_parser1835
        else:
            py_ass_parser1836 = self.tk.splitlist(self.tk.call(what))
            Sensor(1836)
            return py_ass_parser1836
        Sensor(1837)

    def bind(self, sequence=None, func=None, add=None):
        Sensor(1838)
        """Bind to this widget at event SEQUENCE a call to function FUNC.

        SEQUENCE is a string of concatenated event
        patterns. An event pattern is of the form
        <MODIFIER-MODIFIER-TYPE-DETAIL> where MODIFIER is one
        of Control, Mod2, M2, Shift, Mod3, M3, Lock, Mod4, M4,
        Button1, B1, Mod5, M5 Button2, B2, Meta, M, Button3,
        B3, Alt, Button4, B4, Double, Button5, B5 Triple,
        Mod1, M1. TYPE is one of Activate, Enter, Map,
        ButtonPress, Button, Expose, Motion, ButtonRelease
        FocusIn, MouseWheel, Circulate, FocusOut, Property,
        Colormap, Gravity Reparent, Configure, KeyPress, Key,
        Unmap, Deactivate, KeyRelease Visibility, Destroy,
        Leave and DETAIL is the button number for ButtonPress,
        ButtonRelease and DETAIL is the Keysym for KeyPress and
        KeyRelease. Examples are
        <Control-Button-1> for pressing Control and mouse button 1 or
        <Alt-A> for pressing A and the Alt key (KeyPress can be omitted).
        An event pattern can also be a virtual event of the form
        <<AString>> where AString can be arbitrary. This
        event can be generated by event_generate.
        If events are concatenated they must appear shortly
        after each other.

        FUNC will be called if the event sequence occurs with an
        instance of Event as argument. If the return value of FUNC is
        "break" no further bound function is invoked.

        An additional boolean parameter ADD specifies whether FUNC will
        be called additionally to the other bound function or whether
        it will replace the previous function.

        Bind will return an identifier to allow deletion of the bound function with
        unbind without memory leak.

        If FUNC or SEQUENCE is omitted the bound function or list
        of bound events are returned."""
        py_ass_parser1839 = self._bind(('bind', self._w), sequence, func, add)
        Sensor(1839)
        return py_ass_parser1839

    def unbind(self, sequence, funcid=None):
        Sensor(1840)
        """Unbind for this widget for event SEQUENCE  the
        function identified with FUNCID."""
        self.tk.call('bind', self._w, sequence, '')
        if funcid:
            self.deletecommand(funcid)
        Sensor(1841)

    def bind_all(self, sequence=None, func=None, add=None):
        Sensor(1842)
        """Bind to all widgets at an event SEQUENCE a call to function FUNC.
        An additional boolean parameter ADD specifies whether FUNC will
        be called additionally to the other bound function or whether
        it will replace the previous function. See bind for the return value."""
        py_ass_parser1843 = self._bind(('bind', 'all'), sequence, func, add, 0)
        Sensor(1843)
        return py_ass_parser1843

    def unbind_all(self, sequence):
        Sensor(1844)
        """Unbind for all widgets for event SEQUENCE all functions."""
        self.tk.call('bind', 'all', sequence, '')
        Sensor(1845)

    def bind_class(self, className, sequence=None, func=None, add=None):
        Sensor(1846)
        """Bind to widgets with bindtag CLASSNAME at event
        SEQUENCE a call of function FUNC. An additional
        boolean parameter ADD specifies whether FUNC will be
        called additionally to the other bound function or
        whether it will replace the previous function. See bind for
        the return value."""
        py_ass_parser1847 = self._bind(('bind', className), sequence, func,
            add, 0)
        Sensor(1847)
        return py_ass_parser1847

    def unbind_class(self, className, sequence):
        Sensor(1848)
        """Unbind for a all widgets with bindtag CLASSNAME for event SEQUENCE
        all functions."""
        self.tk.call('bind', className, sequence, '')
        Sensor(1849)

    def mainloop(self, n=0):
        Sensor(1850)
        """Call the mainloop of Tk."""
        self.tk.mainloop(n)
        Sensor(1851)

    def quit(self):
        Sensor(1852)
        """Quit the Tcl interpreter. All widgets will be destroyed."""
        self.tk.quit()
        Sensor(1853)

    def _getints(self, string):
        Sensor(1854)
        """Internal function."""
        if string:
            py_ass_parser1855 = tuple(map(getint, self.tk.splitlist(string)))
            Sensor(1855)
            return py_ass_parser1855
        Sensor(1856)

    def _getdoubles(self, string):
        Sensor(1857)
        """Internal function."""
        if string:
            py_ass_parser1858 = tuple(map(getdouble, self.tk.splitlist(string))
                )
            Sensor(1858)
            return py_ass_parser1858
        Sensor(1859)

    def _getboolean(self, string):
        Sensor(1860)
        """Internal function."""
        if string:
            py_ass_parser1861 = self.tk.getboolean(string)
            Sensor(1861)
            return py_ass_parser1861
        Sensor(1862)

    def _displayof(self, displayof):
        Sensor(1863)
        """Internal function."""
        if displayof:
            py_ass_parser1864 = '-displayof', displayof
            Sensor(1864)
            return py_ass_parser1864
        if displayof is None:
            py_ass_parser1865 = '-displayof', self._w
            Sensor(1865)
            return py_ass_parser1865
        py_ass_parser1866 = ()
        Sensor(1866)
        return py_ass_parser1866

    @property
    def _windowingsystem(self):
        Sensor(1867)
        """Internal function."""
        try:
            py_ass_parser1868 = self._root()._windowingsystem_cached
            Sensor(1868)
            return py_ass_parser1868
        except AttributeError:
            ws = self._root()._windowingsystem_cached = self.tk.call('tk',
                'windowingsystem')
            py_ass_parser1869 = ws
            Sensor(1869)
            return py_ass_parser1869
        Sensor(1870)

    def _options(self, cnf, kw=None):
        Sensor(1871)
        """Internal function."""
        if kw:
            cnf = _cnfmerge((cnf, kw))
        else:
            cnf = _cnfmerge(cnf)
        res = ()
        for k, v in cnf.items():
            if v is not None:
                if k[-1] == '_':
                    k = k[:-1]
                if hasattr(v, '__call__'):
                    v = self._register(v)
                elif isinstance(v, (tuple, list)):
                    nv = []
                    for item in v:
                        if not isinstance(item, (basestring, int)):
                            break
                        elif isinstance(item, int):
                            nv.append('%d' % item)
                        else:
                            nv.append(_stringify(item))
                    else:
                        v = ' '.join(nv)
                res = res + ('-' + k, v)
        py_ass_parser1872 = res
        Sensor(1872)
        return py_ass_parser1872

    def nametowidget(self, name):
        Sensor(1873)
        """Return the Tkinter instance of a widget identified by
        its Tcl name NAME."""
        name = str(name).split('.')
        w = self
        if not name[0]:
            w = w._root()
            name = name[1:]
        for n in name:
            if not n:
                break
            w = w.children[n]
        py_ass_parser1874 = w
        Sensor(1874)
        return py_ass_parser1874
    _nametowidget = nametowidget

    def _register(self, func, subst=None, needcleanup=1):
        Sensor(1875)
        """Return a newly created Tcl function. If this
        function is called, the Python function FUNC will
        be executed. An optional function SUBST can
        be given which will be executed before FUNC."""
        f = CallWrapper(func, subst, self).__call__
        name = repr(id(f))
        try:
            func = func.im_func
        except AttributeError:
            pass
        try:
            name = name + func.__name__
        except AttributeError:
            pass
        self.tk.createcommand(name, f)
        if needcleanup:
            if self._tclCommands is None:
                self._tclCommands = []
            self._tclCommands.append(name)
        py_ass_parser1876 = name
        Sensor(1876)
        return py_ass_parser1876
    register = _register

    def _root(self):
        Sensor(1877)
        """Internal function."""
        w = self
        while w.master:
            w = w.master
        py_ass_parser1878 = w
        Sensor(1878)
        return py_ass_parser1878
    _subst_format = ('%#', '%b', '%f', '%h', '%k', '%s', '%t', '%w', '%x',
        '%y', '%A', '%E', '%K', '%N', '%W', '%T', '%X', '%Y', '%D')
    _subst_format_str = ' '.join(_subst_format)

    def _substitute(self, *args):
        Sensor(1879)
        """Internal function."""
        if len(args) != len(self._subst_format):
            py_ass_parser1880 = args
            Sensor(1880)
            return py_ass_parser1880
        getboolean = self.tk.getboolean
        getint = int

        def getint_event(s):
            Sensor(1881)
            """Tk changed behavior in 8.4.2, returning "??" rather more often."""
            try:
                py_ass_parser1882 = int(s)
                Sensor(1882)
                return py_ass_parser1882
            except ValueError:
                py_ass_parser1883 = s
                Sensor(1883)
                return py_ass_parser1883
            Sensor(1884)
        nsign, b, f, h, k, s, t, w, x, y, A, E, K, N, W, T, X, Y, D = args
        e = Event()
        e.serial = getint(nsign)
        e.num = getint_event(b)
        Sensor(1885)
        try:
            e.focus = getboolean(f)
        except TclError:
            pass
        e.height = getint_event(h)
        e.keycode = getint_event(k)
        e.state = getint_event(s)
        e.time = getint_event(t)
        e.width = getint_event(w)
        e.x = getint_event(x)
        e.y = getint_event(y)
        e.char = A
        try:
            e.send_event = getboolean(E)
        except TclError:
            pass
        e.keysym = K
        e.keysym_num = getint_event(N)
        e.type = T
        try:
            e.widget = self._nametowidget(W)
        except KeyError:
            e.widget = W
        e.x_root = getint_event(X)
        e.y_root = getint_event(Y)
        try:
            e.delta = getint(D)
        except ValueError:
            e.delta = 0
        py_ass_parser1886 = e,
        Sensor(1886)
        return py_ass_parser1886

    def _report_exception(self):
        Sensor(1887)
        """Internal function."""
        import sys
        exc, val, tb = sys.exc_type, sys.exc_value, sys.exc_traceback
        root = self._root()
        root.report_callback_exception(exc, val, tb)
        Sensor(1888)

    def _configure(self, cmd, cnf, kw):
        Sensor(1889)
        """Internal function."""
        if kw:
            cnf = _cnfmerge((cnf, kw))
        elif cnf:
            cnf = _cnfmerge(cnf)
        if cnf is None:
            cnf = {}
            for x in self.tk.split(self.tk.call(_flatten((self._w, cmd)))):
                cnf[x[0][1:]] = (x[0][1:],) + x[1:]
            py_ass_parser1890 = cnf
            Sensor(1890)
            return py_ass_parser1890
        if type(cnf) is StringType:
            x = self.tk.split(self.tk.call(_flatten((self._w, cmd, '-' + cnf)))
                )
            py_ass_parser1891 = (x[0][1:],) + x[1:]
            Sensor(1891)
            return py_ass_parser1891
        self.tk.call(_flatten((self._w, cmd)) + self._options(cnf))
        Sensor(1892)

    def configure(self, cnf=None, **kw):
        Sensor(1893)
        """Configure resources of a widget.

        The values for resources are specified as keyword
        arguments. To get an overview about
        the allowed keyword arguments call the method keys.
        """
        py_ass_parser1894 = self._configure('configure', cnf, kw)
        Sensor(1894)
        return py_ass_parser1894
    config = configure

    def cget(self, key):
        Sensor(1895)
        """Return the resource value for a KEY given as string."""
        py_ass_parser1896 = self.tk.call(self._w, 'cget', '-' + key)
        Sensor(1896)
        return py_ass_parser1896
    __getitem__ = cget

    def __setitem__(self, key, value):
        Sensor(1897)
        self.configure({key: value})
        Sensor(1898)

    def __contains__(self, key):
        Sensor(1899)
        raise TypeError("Tkinter objects don't support 'in' tests.")

    def keys(self):
        Sensor(1900)
        """Return a list of all resource names of this widget."""
        py_ass_parser1901 = map(lambda x: x[0][1:], self.tk.split(self.tk.
            call(self._w, 'configure')))
        Sensor(1901)
        return py_ass_parser1901

    def __str__(self):
        Sensor(1902)
        """Return the window path name of this widget."""
        py_ass_parser1903 = self._w
        Sensor(1903)
        return py_ass_parser1903
    _noarg_ = ['_noarg_']

    def pack_propagate(self, flag=_noarg_):
        Sensor(1904)
        """Set or get the status for propagation of geometry information.

        A boolean argument specifies whether the geometry information
        of the slaves will determine the size of this widget. If no argument
        is given the current setting will be returned.
        """
        if flag is Misc._noarg_:
            py_ass_parser1905 = self._getboolean(self.tk.call('pack',
                'propagate', self._w))
            Sensor(1905)
            return py_ass_parser1905
        else:
            self.tk.call('pack', 'propagate', self._w, flag)
        Sensor(1906)
    propagate = pack_propagate

    def pack_slaves(self):
        Sensor(1907)
        """Return a list of all slaves of this widget
        in its packing order."""
        py_ass_parser1908 = map(self._nametowidget, self.tk.splitlist(self.
            tk.call('pack', 'slaves', self._w)))
        Sensor(1908)
        return py_ass_parser1908
    slaves = pack_slaves

    def place_slaves(self):
        Sensor(1909)
        """Return a list of all slaves of this widget
        in its packing order."""
        py_ass_parser1910 = map(self._nametowidget, self.tk.splitlist(self.
            tk.call('place', 'slaves', self._w)))
        Sensor(1910)
        return py_ass_parser1910

    def grid_bbox(self, column=None, row=None, col2=None, row2=None):
        Sensor(1911)
        """Return a tuple of integer coordinates for the bounding
        box of this widget controlled by the geometry manager grid.

        If COLUMN, ROW is given the bounding box applies from
        the cell with row and column 0 to the specified
        cell. If COL2 and ROW2 are given the bounding box
        starts at that cell.

        The returned integers specify the offset of the upper left
        corner in the master widget and the width and height.
        """
        args = 'grid', 'bbox', self._w
        if column is not None and row is not None:
            args = args + (column, row)
        if col2 is not None and row2 is not None:
            args = args + (col2, row2)
        py_ass_parser1912 = self._getints(self.tk.call(*args)) or None
        Sensor(1912)
        return py_ass_parser1912
    bbox = grid_bbox

    def _grid_configure(self, command, index, cnf, kw):
        Sensor(1913)
        """Internal function."""
        if type(cnf) is StringType and not kw:
            if cnf[-1:] == '_':
                cnf = cnf[:-1]
            if cnf[:1] != '-':
                cnf = '-' + cnf
            options = cnf,
        else:
            options = self._options(cnf, kw)
        if not options:
            res = self.tk.call('grid', command, self._w, index)
            words = self.tk.splitlist(res)
            dict = {}
            for i in range(0, len(words), 2):
                key = words[i][1:]
                value = words[i + 1]
                if not value:
                    value = None
                elif '.' in value:
                    value = getdouble(value)
                else:
                    value = getint(value)
                dict[key] = value
            py_ass_parser1914 = dict
            Sensor(1914)
            return py_ass_parser1914
        res = self.tk.call(('grid', command, self._w, index) + options)
        if len(options) == 1:
            if not res:
                py_ass_parser1915 = None
                Sensor(1915)
                return py_ass_parser1915
            if '.' in res:
                py_ass_parser1916 = getdouble(res)
                Sensor(1916)
                return py_ass_parser1916
            py_ass_parser1917 = getint(res)
            Sensor(1917)
            return py_ass_parser1917
        Sensor(1918)

    def grid_columnconfigure(self, index, cnf={}, **kw):
        Sensor(1919)
        """Configure column INDEX of a grid.

        Valid resources are minsize (minimum size of the column),
        weight (how much does additional space propagate to this column)
        and pad (how much space to let additionally)."""
        py_ass_parser1920 = self._grid_configure('columnconfigure', index,
            cnf, kw)
        Sensor(1920)
        return py_ass_parser1920
    columnconfigure = grid_columnconfigure

    def grid_location(self, x, y):
        Sensor(1921)
        """Return a tuple of column and row which identify the cell
        at which the pixel at position X and Y inside the master
        widget is located."""
        py_ass_parser1922 = self._getints(self.tk.call('grid', 'location',
            self._w, x, y)) or None
        Sensor(1922)
        return py_ass_parser1922

    def grid_propagate(self, flag=_noarg_):
        Sensor(1923)
        """Set or get the status for propagation of geometry information.

        A boolean argument specifies whether the geometry information
        of the slaves will determine the size of this widget. If no argument
        is given, the current setting will be returned.
        """
        if flag is Misc._noarg_:
            py_ass_parser1924 = self._getboolean(self.tk.call('grid',
                'propagate', self._w))
            Sensor(1924)
            return py_ass_parser1924
        else:
            self.tk.call('grid', 'propagate', self._w, flag)
        Sensor(1925)

    def grid_rowconfigure(self, index, cnf={}, **kw):
        Sensor(1926)
        """Configure row INDEX of a grid.

        Valid resources are minsize (minimum size of the row),
        weight (how much does additional space propagate to this row)
        and pad (how much space to let additionally)."""
        py_ass_parser1927 = self._grid_configure('rowconfigure', index, cnf, kw
            )
        Sensor(1927)
        return py_ass_parser1927
    rowconfigure = grid_rowconfigure

    def grid_size(self):
        Sensor(1928)
        """Return a tuple of the number of column and rows in the grid."""
        py_ass_parser1929 = self._getints(self.tk.call('grid', 'size', self._w)
            ) or None
        Sensor(1929)
        return py_ass_parser1929
    size = grid_size

    def grid_slaves(self, row=None, column=None):
        Sensor(1930)
        """Return a list of all slaves of this widget
        in its packing order."""
        args = ()
        if row is not None:
            args = args + ('-row', row)
        if column is not None:
            args = args + ('-column', column)
        py_ass_parser1931 = map(self._nametowidget, self.tk.splitlist(self.
            tk.call(('grid', 'slaves', self._w) + args)))
        Sensor(1931)
        return py_ass_parser1931

    def event_add(self, virtual, *sequences):
        Sensor(1932)
        """Bind a virtual event VIRTUAL (of the form <<Name>>)
        to an event SEQUENCE such that the virtual event is triggered
        whenever SEQUENCE occurs."""
        args = ('event', 'add', virtual) + sequences
        self.tk.call(args)
        Sensor(1933)

    def event_delete(self, virtual, *sequences):
        Sensor(1934)
        """Unbind a virtual event VIRTUAL from SEQUENCE."""
        args = ('event', 'delete', virtual) + sequences
        self.tk.call(args)
        Sensor(1935)

    def event_generate(self, sequence, **kw):
        Sensor(1936)
        """Generate an event SEQUENCE. Additional
        keyword arguments specify parameter of the event
        (e.g. x, y, rootx, rooty)."""
        args = 'event', 'generate', self._w, sequence
        for k, v in kw.items():
            args = args + ('-%s' % k, str(v))
        self.tk.call(args)
        Sensor(1937)

    def event_info(self, virtual=None):
        Sensor(1938)
        """Return a list of all virtual events or the information
        about the SEQUENCE bound to the virtual event VIRTUAL."""
        py_ass_parser1939 = self.tk.splitlist(self.tk.call('event', 'info',
            virtual))
        Sensor(1939)
        return py_ass_parser1939

    def image_names(self):
        Sensor(1940)
        """Return a list of all existing image names."""
        py_ass_parser1941 = self.tk.call('image', 'names')
        Sensor(1941)
        return py_ass_parser1941

    def image_types(self):
        Sensor(1942)
        """Return a list of all available image types (e.g. phote bitmap)."""
        py_ass_parser1943 = self.tk.call('image', 'types')
        Sensor(1943)
        return py_ass_parser1943


class CallWrapper:
    """Internal class. Stores function to call when some user
    defined Tcl function is called e.g. after an event occurred."""

    def __init__(self, func, subst, widget):
        Sensor(1944)
        """Store FUNC, SUBST and WIDGET as members."""
        self.func = func
        self.subst = subst
        self.widget = widget
        Sensor(1945)

    def __call__(self, *args):
        Sensor(1946)
        """Apply first function SUBST to arguments, than FUNC."""
        try:
            if self.subst:
                args = self.subst(*args)
            py_ass_parser1947 = self.func(*args)
            Sensor(1947)
            return py_ass_parser1947
        except SystemExit as msg:
            raise SystemExit, msg
        except:
            self.widget._report_exception()
        Sensor(1948)


class XView:
    """Mix-in class for querying and changing the horizontal position
    of a widget's window."""

    def xview(self, *args):
        Sensor(1949)
        """Query and change the horizontal position of the view."""
        res = self.tk.call(self._w, 'xview', *args)
        if not args:
            py_ass_parser1950 = self._getdoubles(res)
            Sensor(1950)
            return py_ass_parser1950
        Sensor(1951)

    def xview_moveto(self, fraction):
        Sensor(1952)
        """Adjusts the view in the window so that FRACTION of the
        total width of the canvas is off-screen to the left."""
        self.tk.call(self._w, 'xview', 'moveto', fraction)
        Sensor(1953)

    def xview_scroll(self, number, what):
        Sensor(1954)
        """Shift the x-view according to NUMBER which is measured in "units"
        or "pages" (WHAT)."""
        self.tk.call(self._w, 'xview', 'scroll', number, what)
        Sensor(1955)


class YView:
    """Mix-in class for querying and changing the vertical position
    of a widget's window."""

    def yview(self, *args):
        Sensor(1956)
        """Query and change the vertical position of the view."""
        res = self.tk.call(self._w, 'yview', *args)
        if not args:
            py_ass_parser1957 = self._getdoubles(res)
            Sensor(1957)
            return py_ass_parser1957
        Sensor(1958)

    def yview_moveto(self, fraction):
        Sensor(1959)
        """Adjusts the view in the window so that FRACTION of the
        total height of the canvas is off-screen to the top."""
        self.tk.call(self._w, 'yview', 'moveto', fraction)
        Sensor(1960)

    def yview_scroll(self, number, what):
        Sensor(1961)
        """Shift the y-view according to NUMBER which is measured in
        "units" or "pages" (WHAT)."""
        self.tk.call(self._w, 'yview', 'scroll', number, what)
        Sensor(1962)


class Wm:
    """Provides functions for the communication with the window manager."""

    def wm_aspect(self, minNumer=None, minDenom=None, maxNumer=None,
        maxDenom=None):
        Sensor(1963)
        """Instruct the window manager to set the aspect ratio (width/height)
        of this widget to be between MINNUMER/MINDENOM and MAXNUMER/MAXDENOM. Return a tuple
        of the actual values if no argument is given."""
        py_ass_parser1964 = self._getints(self.tk.call('wm', 'aspect', self
            ._w, minNumer, minDenom, maxNumer, maxDenom))
        Sensor(1964)
        return py_ass_parser1964
    aspect = wm_aspect

    def wm_attributes(self, *args):
        Sensor(1965)
        """This subcommand returns or sets platform specific attributes

        The first form returns a list of the platform specific flags and
        their values. The second form returns the value for the specific
        option. The third form sets one or more of the values. The values
        are as follows:

        On Windows, -disabled gets or sets whether the window is in a
        disabled state. -toolwindow gets or sets the style of the window
        to toolwindow (as defined in the MSDN). -topmost gets or sets
        whether this is a topmost window (displays above all other
        windows).

        On Macintosh, XXXXX

        On Unix, there are currently no special attribute values.
        """
        args = ('wm', 'attributes', self._w) + args
        py_ass_parser1966 = self.tk.call(args)
        Sensor(1966)
        return py_ass_parser1966
    attributes = wm_attributes

    def wm_client(self, name=None):
        Sensor(1967)
        """Store NAME in WM_CLIENT_MACHINE property of this widget. Return
        current value."""
        py_ass_parser1968 = self.tk.call('wm', 'client', self._w, name)
        Sensor(1968)
        return py_ass_parser1968
    client = wm_client

    def wm_colormapwindows(self, *wlist):
        Sensor(1969)
        """Store list of window names (WLIST) into WM_COLORMAPWINDOWS property
        of this widget. This list contains windows whose colormaps differ from their
        parents. Return current list of widgets if WLIST is empty."""
        if len(wlist) > 1:
            wlist = wlist,
        args = ('wm', 'colormapwindows', self._w) + wlist
        py_ass_parser1970 = map(self._nametowidget, self.tk.call(args))
        Sensor(1970)
        return py_ass_parser1970
    colormapwindows = wm_colormapwindows

    def wm_command(self, value=None):
        Sensor(1971)
        """Store VALUE in WM_COMMAND property. It is the command
        which shall be used to invoke the application. Return current
        command if VALUE is None."""
        py_ass_parser1972 = self.tk.call('wm', 'command', self._w, value)
        Sensor(1972)
        return py_ass_parser1972
    command = wm_command

    def wm_deiconify(self):
        Sensor(1973)
        """Deiconify this widget. If it was never mapped it will not be mapped.
        On Windows it will raise this widget and give it the focus."""
        py_ass_parser1974 = self.tk.call('wm', 'deiconify', self._w)
        Sensor(1974)
        return py_ass_parser1974
    deiconify = wm_deiconify

    def wm_focusmodel(self, model=None):
        Sensor(1975)
        """Set focus model to MODEL. "active" means that this widget will claim
        the focus itself, "passive" means that the window manager shall give
        the focus. Return current focus model if MODEL is None."""
        py_ass_parser1976 = self.tk.call('wm', 'focusmodel', self._w, model)
        Sensor(1976)
        return py_ass_parser1976
    focusmodel = wm_focusmodel

    def wm_frame(self):
        Sensor(1977)
        """Return identifier for decorative frame of this widget if present."""
        py_ass_parser1978 = self.tk.call('wm', 'frame', self._w)
        Sensor(1978)
        return py_ass_parser1978
    frame = wm_frame

    def wm_geometry(self, newGeometry=None):
        Sensor(1979)
        """Set geometry to NEWGEOMETRY of the form =widthxheight+x+y. Return
        current value if None is given."""
        py_ass_parser1980 = self.tk.call('wm', 'geometry', self._w, newGeometry
            )
        Sensor(1980)
        return py_ass_parser1980
    geometry = wm_geometry

    def wm_grid(self, baseWidth=None, baseHeight=None, widthInc=None,
        heightInc=None):
        Sensor(1981)
        """Instruct the window manager that this widget shall only be
        resized on grid boundaries. WIDTHINC and HEIGHTINC are the width and
        height of a grid unit in pixels. BASEWIDTH and BASEHEIGHT are the
        number of grid units requested in Tk_GeometryRequest."""
        py_ass_parser1982 = self._getints(self.tk.call('wm', 'grid', self.
            _w, baseWidth, baseHeight, widthInc, heightInc))
        Sensor(1982)
        return py_ass_parser1982
    grid = wm_grid

    def wm_group(self, pathName=None):
        Sensor(1983)
        """Set the group leader widgets for related widgets to PATHNAME. Return
        the group leader of this widget if None is given."""
        py_ass_parser1984 = self.tk.call('wm', 'group', self._w, pathName)
        Sensor(1984)
        return py_ass_parser1984
    group = wm_group

    def wm_iconbitmap(self, bitmap=None, default=None):
        Sensor(1985)
        """Set bitmap for the iconified widget to BITMAP. Return
        the bitmap if None is given.

        Under Windows, the DEFAULT parameter can be used to set the icon
        for the widget and any descendents that don't have an icon set
        explicitly.  DEFAULT can be the relative path to a .ico file
        (example: root.iconbitmap(default='myicon.ico') ).  See Tk
        documentation for more information."""
        if default:
            py_ass_parser1986 = self.tk.call('wm', 'iconbitmap', self._w,
                '-default', default)
            Sensor(1986)
            return py_ass_parser1986
        else:
            py_ass_parser1987 = self.tk.call('wm', 'iconbitmap', self._w,
                bitmap)
            Sensor(1987)
            return py_ass_parser1987
        Sensor(1988)
    iconbitmap = wm_iconbitmap

    def wm_iconify(self):
        Sensor(1989)
        """Display widget as icon."""
        py_ass_parser1990 = self.tk.call('wm', 'iconify', self._w)
        Sensor(1990)
        return py_ass_parser1990
    iconify = wm_iconify

    def wm_iconmask(self, bitmap=None):
        Sensor(1991)
        """Set mask for the icon bitmap of this widget. Return the
        mask if None is given."""
        py_ass_parser1992 = self.tk.call('wm', 'iconmask', self._w, bitmap)
        Sensor(1992)
        return py_ass_parser1992
    iconmask = wm_iconmask

    def wm_iconname(self, newName=None):
        Sensor(1993)
        """Set the name of the icon for this widget. Return the name if
        None is given."""
        py_ass_parser1994 = self.tk.call('wm', 'iconname', self._w, newName)
        Sensor(1994)
        return py_ass_parser1994
    iconname = wm_iconname

    def wm_iconposition(self, x=None, y=None):
        Sensor(1995)
        """Set the position of the icon of this widget to X and Y. Return
        a tuple of the current values of X and X if None is given."""
        py_ass_parser1996 = self._getints(self.tk.call('wm', 'iconposition',
            self._w, x, y))
        Sensor(1996)
        return py_ass_parser1996
    iconposition = wm_iconposition

    def wm_iconwindow(self, pathName=None):
        Sensor(1997)
        """Set widget PATHNAME to be displayed instead of icon. Return the current
        value if None is given."""
        py_ass_parser1998 = self.tk.call('wm', 'iconwindow', self._w, pathName)
        Sensor(1998)
        return py_ass_parser1998
    iconwindow = wm_iconwindow

    def wm_maxsize(self, width=None, height=None):
        Sensor(1999)
        """Set max WIDTH and HEIGHT for this widget. If the window is gridded
        the values are given in grid units. Return the current values if None
        is given."""
        py_ass_parser2000 = self._getints(self.tk.call('wm', 'maxsize',
            self._w, width, height))
        Sensor(2000)
        return py_ass_parser2000
    maxsize = wm_maxsize

    def wm_minsize(self, width=None, height=None):
        Sensor(2001)
        """Set min WIDTH and HEIGHT for this widget. If the window is gridded
        the values are given in grid units. Return the current values if None
        is given."""
        py_ass_parser2002 = self._getints(self.tk.call('wm', 'minsize',
            self._w, width, height))
        Sensor(2002)
        return py_ass_parser2002
    minsize = wm_minsize

    def wm_overrideredirect(self, boolean=None):
        Sensor(2003)
        """Instruct the window manager to ignore this widget
        if BOOLEAN is given with 1. Return the current value if None
        is given."""
        py_ass_parser2004 = self._getboolean(self.tk.call('wm',
            'overrideredirect', self._w, boolean))
        Sensor(2004)
        return py_ass_parser2004
    overrideredirect = wm_overrideredirect

    def wm_positionfrom(self, who=None):
        Sensor(2005)
        """Instruct the window manager that the position of this widget shall
        be defined by the user if WHO is "user", and by its own policy if WHO is
        "program"."""
        py_ass_parser2006 = self.tk.call('wm', 'positionfrom', self._w, who)
        Sensor(2006)
        return py_ass_parser2006
    positionfrom = wm_positionfrom

    def wm_protocol(self, name=None, func=None):
        Sensor(2007)
        """Bind function FUNC to command NAME for this widget.
        Return the function bound to NAME if None is given. NAME could be
        e.g. "WM_SAVE_YOURSELF" or "WM_DELETE_WINDOW"."""
        if hasattr(func, '__call__'):
            command = self._register(func)
        else:
            command = func
        py_ass_parser2008 = self.tk.call('wm', 'protocol', self._w, name,
            command)
        Sensor(2008)
        return py_ass_parser2008
    protocol = wm_protocol

    def wm_resizable(self, width=None, height=None):
        Sensor(2009)
        """Instruct the window manager whether this width can be resized
        in WIDTH or HEIGHT. Both values are boolean values."""
        py_ass_parser2010 = self.tk.call('wm', 'resizable', self._w, width,
            height)
        Sensor(2010)
        return py_ass_parser2010
    resizable = wm_resizable

    def wm_sizefrom(self, who=None):
        Sensor(2011)
        """Instruct the window manager that the size of this widget shall
        be defined by the user if WHO is "user", and by its own policy if WHO is
        "program"."""
        py_ass_parser2012 = self.tk.call('wm', 'sizefrom', self._w, who)
        Sensor(2012)
        return py_ass_parser2012
    sizefrom = wm_sizefrom

    def wm_state(self, newstate=None):
        Sensor(2013)
        """Query or set the state of this widget as one of normal, icon,
        iconic (see wm_iconwindow), withdrawn, or zoomed (Windows only)."""
        py_ass_parser2014 = self.tk.call('wm', 'state', self._w, newstate)
        Sensor(2014)
        return py_ass_parser2014
    state = wm_state

    def wm_title(self, string=None):
        Sensor(2015)
        """Set the title of this widget."""
        py_ass_parser2016 = self.tk.call('wm', 'title', self._w, string)
        Sensor(2016)
        return py_ass_parser2016
    title = wm_title

    def wm_transient(self, master=None):
        Sensor(2017)
        """Instruct the window manager that this widget is transient
        with regard to widget MASTER."""
        py_ass_parser2018 = self.tk.call('wm', 'transient', self._w, master)
        Sensor(2018)
        return py_ass_parser2018
    transient = wm_transient

    def wm_withdraw(self):
        Sensor(2019)
        """Withdraw this widget from the screen such that it is unmapped
        and forgotten by the window manager. Re-draw it with wm_deiconify."""
        py_ass_parser2020 = self.tk.call('wm', 'withdraw', self._w)
        Sensor(2020)
        return py_ass_parser2020
    withdraw = wm_withdraw


class Tk(Misc, Wm):
    """Toplevel widget of Tk which represents mostly the main window
    of an application. It has an associated Tcl interpreter."""
    _w = '.'

    def __init__(self, screenName=None, baseName=None, className='Tk',
        useTk=1, sync=0, use=None):
        Sensor(2021)
        """Return a new Toplevel widget on screen SCREENNAME. A new Tcl interpreter will
        be created. BASENAME will be used for the identification of the profile file (see
        readprofile).
        It is constructed from sys.argv[0] without extensions if None is given. CLASSNAME
        is the name of the widget class."""
        self.master = None
        self.children = {}
        self._tkloaded = 0
        self.tk = None
        if baseName is None:
            import sys, os
            baseName = os.path.basename(sys.argv[0])
            baseName, ext = os.path.splitext(baseName)
            if ext not in ('.py', '.pyc', '.pyo'):
                baseName = baseName + ext
        interactive = 0
        self.tk = _tkinter.create(screenName, baseName, className,
            interactive, wantobjects, useTk, sync, use)
        if useTk:
            self._loadtk()
        if not sys.flags.ignore_environment:
            self.readprofile(baseName, className)
        Sensor(2022)

    def loadtk(self):
        Sensor(2023)
        if not self._tkloaded:
            self.tk.loadtk()
            self._loadtk()
        Sensor(2024)

    def _loadtk(self):
        Sensor(2025)
        self._tkloaded = 1
        global _default_root
        tk_version = self.tk.getvar('tk_version')
        if tk_version != _tkinter.TK_VERSION:
            raise RuntimeError, "tk.h version (%s) doesn't match libtk.a version (%s)" % (
                _tkinter.TK_VERSION, tk_version)
        tcl_version = str(self.tk.getvar('tcl_version'))
        if tcl_version != _tkinter.TCL_VERSION:
            raise RuntimeError, "tcl.h version (%s) doesn't match libtcl.a version (%s)" % (
                _tkinter.TCL_VERSION, tcl_version)
        if TkVersion < 4.0:
            raise RuntimeError, 'Tk 4.0 or higher is required; found Tk %s' % str(
                TkVersion)
        if self._tclCommands is None:
            self._tclCommands = []
        self.tk.createcommand('tkerror', _tkerror)
        self.tk.createcommand('exit', _exit)
        self._tclCommands.append('tkerror')
        self._tclCommands.append('exit')
        if _support_default_root and not _default_root:
            _default_root = self
        self.protocol('WM_DELETE_WINDOW', self.destroy)
        Sensor(2026)

    def destroy(self):
        Sensor(2027)
        """Destroy this and all descendants widgets. This will
        end the application of this Tcl interpreter."""
        for c in self.children.values():
            c.destroy()
        self.tk.call('destroy', self._w)
        Misc.destroy(self)
        global _default_root
        if _support_default_root and _default_root is self:
            _default_root = None
        Sensor(2028)

    def readprofile(self, baseName, className):
        Sensor(2029)
        """Internal function. It reads BASENAME.tcl and CLASSNAME.tcl into
        the Tcl Interpreter and calls execfile on BASENAME.py and CLASSNAME.py if
        such a file exists in the home directory."""
        import os
        if 'HOME' in os.environ:
            home = os.environ['HOME']
        else:
            home = os.curdir
        class_tcl = os.path.join(home, '.%s.tcl' % className)
        class_py = os.path.join(home, '.%s.py' % className)
        base_tcl = os.path.join(home, '.%s.tcl' % baseName)
        base_py = os.path.join(home, '.%s.py' % baseName)
        dir = {'self': self}
        exec 'from Tkinter import *' in dir
        if os.path.isfile(class_tcl):
            self.tk.call('source', class_tcl)
        if os.path.isfile(class_py):
            execfile(class_py, dir)
        if os.path.isfile(base_tcl):
            self.tk.call('source', base_tcl)
        if os.path.isfile(base_py):
            execfile(base_py, dir)
        Sensor(2030)

    def report_callback_exception(self, exc, val, tb):
        Sensor(2031)
        """Internal function. It reports exception on sys.stderr."""
        import traceback, sys
        sys.stderr.write('Exception in Tkinter callback\n')
        sys.last_type = exc
        sys.last_value = val
        sys.last_traceback = tb
        traceback.print_exception(exc, val, tb)
        Sensor(2032)

    def __getattr__(self, attr):
        Sensor(2033)
        """Delegate attribute access to the interpreter object"""
        py_ass_parser2034 = getattr(self.tk, attr)
        Sensor(2034)
        return py_ass_parser2034


def Tcl(screenName=None, baseName=None, className='Tk', useTk=0):
    py_ass_parser2035 = Tk(screenName, baseName, className, useTk)
    Sensor(2035)
    return py_ass_parser2035


class Pack:
    """Geometry manager Pack.

    Base class to use the methods pack_* in every widget."""

    def pack_configure(self, cnf={}, **kw):
        Sensor(2036)
        """Pack a widget in the parent widget. Use as options:
        after=widget - pack it after you have packed widget
        anchor=NSEW (or subset) - position widget according to
                                  given direction
        before=widget - pack it before you will pack widget
        expand=bool - expand widget if parent size grows
        fill=NONE or X or Y or BOTH - fill widget if widget grows
        in=master - use master to contain this widget
        in_=master - see 'in' option description
        ipadx=amount - add internal padding in x direction
        ipady=amount - add internal padding in y direction
        padx=amount - add padding in x direction
        pady=amount - add padding in y direction
        side=TOP or BOTTOM or LEFT or RIGHT -  where to add this widget.
        """
        self.tk.call(('pack', 'configure', self._w) + self._options(cnf, kw))
        Sensor(2037)
    pack = configure = config = pack_configure

    def pack_forget(self):
        Sensor(2038)
        """Unmap this widget and do not use it for the packing order."""
        self.tk.call('pack', 'forget', self._w)
        Sensor(2039)
    forget = pack_forget

    def pack_info(self):
        Sensor(2040)
        """Return information about the packing options
        for this widget."""
        words = self.tk.splitlist(self.tk.call('pack', 'info', self._w))
        dict = {}
        for i in range(0, len(words), 2):
            key = words[i][1:]
            value = words[i + 1]
            if value[:1] == '.':
                value = self._nametowidget(value)
            dict[key] = value
        py_ass_parser2041 = dict
        Sensor(2041)
        return py_ass_parser2041
    info = pack_info
    propagate = pack_propagate = Misc.pack_propagate
    slaves = pack_slaves = Misc.pack_slaves


class Place:
    """Geometry manager Place.

    Base class to use the methods place_* in every widget."""

    def place_configure(self, cnf={}, **kw):
        Sensor(2042)
        """Place a widget in the parent widget. Use as options:
        in=master - master relative to which the widget is placed
        in_=master - see 'in' option description
        x=amount - locate anchor of this widget at position x of master
        y=amount - locate anchor of this widget at position y of master
        relx=amount - locate anchor of this widget between 0.0 and 1.0
                      relative to width of master (1.0 is right edge)
        rely=amount - locate anchor of this widget between 0.0 and 1.0
                      relative to height of master (1.0 is bottom edge)
        anchor=NSEW (or subset) - position anchor according to given direction
        width=amount - width of this widget in pixel
        height=amount - height of this widget in pixel
        relwidth=amount - width of this widget between 0.0 and 1.0
                          relative to width of master (1.0 is the same width
                          as the master)
        relheight=amount - height of this widget between 0.0 and 1.0
                           relative to height of master (1.0 is the same
                           height as the master)
        bordermode="inside" or "outside" - whether to take border width of
                                           master widget into account
        """
        self.tk.call(('place', 'configure', self._w) + self._options(cnf, kw))
        Sensor(2043)
    place = configure = config = place_configure

    def place_forget(self):
        Sensor(2044)
        """Unmap this widget."""
        self.tk.call('place', 'forget', self._w)
        Sensor(2045)
    forget = place_forget

    def place_info(self):
        Sensor(2046)
        """Return information about the placing options
        for this widget."""
        words = self.tk.splitlist(self.tk.call('place', 'info', self._w))
        dict = {}
        for i in range(0, len(words), 2):
            key = words[i][1:]
            value = words[i + 1]
            if value[:1] == '.':
                value = self._nametowidget(value)
            dict[key] = value
        py_ass_parser2047 = dict
        Sensor(2047)
        return py_ass_parser2047
    info = place_info
    slaves = place_slaves = Misc.place_slaves


class Grid:
    """Geometry manager Grid.

    Base class to use the methods grid_* in every widget."""

    def grid_configure(self, cnf={}, **kw):
        Sensor(2048)
        """Position a widget in the parent widget in a grid. Use as options:
        column=number - use cell identified with given column (starting with 0)
        columnspan=number - this widget will span several columns
        in=master - use master to contain this widget
        in_=master - see 'in' option description
        ipadx=amount - add internal padding in x direction
        ipady=amount - add internal padding in y direction
        padx=amount - add padding in x direction
        pady=amount - add padding in y direction
        row=number - use cell identified with given row (starting with 0)
        rowspan=number - this widget will span several rows
        sticky=NSEW - if cell is larger on which sides will this
                      widget stick to the cell boundary
        """
        self.tk.call(('grid', 'configure', self._w) + self._options(cnf, kw))
        Sensor(2049)
    grid = configure = config = grid_configure
    bbox = grid_bbox = Misc.grid_bbox
    columnconfigure = grid_columnconfigure = Misc.grid_columnconfigure

    def grid_forget(self):
        Sensor(2050)
        """Unmap this widget."""
        self.tk.call('grid', 'forget', self._w)
        Sensor(2051)
    forget = grid_forget

    def grid_remove(self):
        Sensor(2052)
        """Unmap this widget but remember the grid options."""
        self.tk.call('grid', 'remove', self._w)
        Sensor(2053)

    def grid_info(self):
        Sensor(2054)
        """Return information about the options
        for positioning this widget in a grid."""
        words = self.tk.splitlist(self.tk.call('grid', 'info', self._w))
        dict = {}
        for i in range(0, len(words), 2):
            key = words[i][1:]
            value = words[i + 1]
            if value[:1] == '.':
                value = self._nametowidget(value)
            dict[key] = value
        py_ass_parser2055 = dict
        Sensor(2055)
        return py_ass_parser2055
    info = grid_info
    location = grid_location = Misc.grid_location
    propagate = grid_propagate = Misc.grid_propagate
    rowconfigure = grid_rowconfigure = Misc.grid_rowconfigure
    size = grid_size = Misc.grid_size
    slaves = grid_slaves = Misc.grid_slaves


class BaseWidget(Misc):
    """Internal class."""

    def _setup(self, master, cnf):
        Sensor(2056)
        """Internal function. Sets up information about children."""
        if _support_default_root:
            global _default_root
            if not master:
                if not _default_root:
                    _default_root = Tk()
                master = _default_root
        self.master = master
        self.tk = master.tk
        name = None
        if 'name' in cnf:
            name = cnf['name']
            del cnf['name']
        if not name:
            name = repr(id(self))
        self._name = name
        if master._w == '.':
            self._w = '.' + name
        else:
            self._w = master._w + '.' + name
        self.children = {}
        if self._name in self.master.children:
            self.master.children[self._name].destroy()
        self.master.children[self._name] = self
        Sensor(2057)

    def __init__(self, master, widgetName, cnf={}, kw={}, extra=()):
        Sensor(2058)
        """Construct a widget with the parent widget MASTER, a name WIDGETNAME
        and appropriate options."""
        if kw:
            cnf = _cnfmerge((cnf, kw))
        self.widgetName = widgetName
        BaseWidget._setup(self, master, cnf)
        if self._tclCommands is None:
            self._tclCommands = []
        classes = []
        for k in cnf.keys():
            if type(k) is ClassType:
                classes.append((k, cnf[k]))
                del cnf[k]
        self.tk.call((widgetName, self._w) + extra + self._options(cnf))
        for k, v in classes:
            k.configure(self, v)
        Sensor(2059)

    def destroy(self):
        Sensor(2060)
        """Destroy this and all descendants widgets."""
        for c in self.children.values():
            c.destroy()
        self.tk.call('destroy', self._w)
        if self._name in self.master.children:
            del self.master.children[self._name]
        Misc.destroy(self)
        Sensor(2061)

    def _do(self, name, args=()):
        py_ass_parser2062 = self.tk.call((self._w, name) + args)
        Sensor(2062)
        return py_ass_parser2062


class Widget(BaseWidget, Pack, Place, Grid):
    """Internal class.

    Base class for a widget which can be positioned with the geometry managers
    Pack, Place or Grid."""
    pass


class Toplevel(BaseWidget, Wm):
    """Toplevel widget, e.g. for dialogs."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2063)
        """Construct a toplevel widget with the parent MASTER.

        Valid resource names: background, bd, bg, borderwidth, class,
        colormap, container, cursor, height, highlightbackground,
        highlightcolor, highlightthickness, menu, relief, screen, takefocus,
        use, visual, width."""
        if kw:
            cnf = _cnfmerge((cnf, kw))
        extra = ()
        for wmkey in ['screen', 'class_', 'class', 'visual', 'colormap']:
            if wmkey in cnf:
                val = cnf[wmkey]
                if wmkey[-1] == '_':
                    opt = '-' + wmkey[:-1]
                else:
                    opt = '-' + wmkey
                extra = extra + (opt, val)
                del cnf[wmkey]
        BaseWidget.__init__(self, master, 'toplevel', cnf, {}, extra)
        root = self._root()
        self.iconname(root.iconname())
        self.title(root.title())
        self.protocol('WM_DELETE_WINDOW', self.destroy)
        Sensor(2064)


class Button(Widget):
    """Button widget."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2065)
        """Construct a button widget with the parent MASTER.

        STANDARD OPTIONS

            activebackground, activeforeground, anchor,
            background, bitmap, borderwidth, cursor,
            disabledforeground, font, foreground
            highlightbackground, highlightcolor,
            highlightthickness, image, justify,
            padx, pady, relief, repeatdelay,
            repeatinterval, takefocus, text,
            textvariable, underline, wraplength

        WIDGET-SPECIFIC OPTIONS

            command, compound, default, height,
            overrelief, state, width
        """
        Widget.__init__(self, master, 'button', cnf, kw)
        Sensor(2066)

    def tkButtonEnter(self, *dummy):
        Sensor(2067)
        self.tk.call('tkButtonEnter', self._w)
        Sensor(2068)

    def tkButtonLeave(self, *dummy):
        Sensor(2069)
        self.tk.call('tkButtonLeave', self._w)
        Sensor(2070)

    def tkButtonDown(self, *dummy):
        Sensor(2071)
        self.tk.call('tkButtonDown', self._w)
        Sensor(2072)

    def tkButtonUp(self, *dummy):
        Sensor(2073)
        self.tk.call('tkButtonUp', self._w)
        Sensor(2074)

    def tkButtonInvoke(self, *dummy):
        Sensor(2075)
        self.tk.call('tkButtonInvoke', self._w)
        Sensor(2076)

    def flash(self):
        Sensor(2077)
        """Flash the button.

        This is accomplished by redisplaying
        the button several times, alternating between active and
        normal colors. At the end of the flash the button is left
        in the same normal/active state as when the command was
        invoked. This command is ignored if the button's state is
        disabled.
        """
        self.tk.call(self._w, 'flash')
        Sensor(2078)

    def invoke(self):
        Sensor(2079)
        """Invoke the command associated with the button.

        The return value is the return value from the command,
        or an empty string if there is no command associated with
        the button. This command is ignored if the button's state
        is disabled.
        """
        py_ass_parser2080 = self.tk.call(self._w, 'invoke')
        Sensor(2080)
        return py_ass_parser2080


def AtEnd():
    py_ass_parser2081 = 'end'
    Sensor(2081)
    return py_ass_parser2081


def AtInsert(*args):
    Sensor(2082)
    s = 'insert'
    for a in args:
        if a:
            s = s + (' ' + a)
    py_ass_parser2083 = s
    Sensor(2083)
    return py_ass_parser2083


def AtSelFirst():
    py_ass_parser2084 = 'sel.first'
    Sensor(2084)
    return py_ass_parser2084


def AtSelLast():
    py_ass_parser2085 = 'sel.last'
    Sensor(2085)
    return py_ass_parser2085


def At(x, y=None):
    Sensor(2086)
    if y is None:
        py_ass_parser2087 = '@%r' % (x,)
        Sensor(2087)
        return py_ass_parser2087
    else:
        py_ass_parser2088 = '@%r,%r' % (x, y)
        Sensor(2088)
        return py_ass_parser2088
    Sensor(2089)


class Canvas(Widget, XView, YView):
    """Canvas widget to display graphical elements like lines or text."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2090)
        """Construct a canvas widget with the parent MASTER.

        Valid resource names: background, bd, bg, borderwidth, closeenough,
        confine, cursor, height, highlightbackground, highlightcolor,
        highlightthickness, insertbackground, insertborderwidth,
        insertofftime, insertontime, insertwidth, offset, relief,
        scrollregion, selectbackground, selectborderwidth, selectforeground,
        state, takefocus, width, xscrollcommand, xscrollincrement,
        yscrollcommand, yscrollincrement."""
        Widget.__init__(self, master, 'canvas', cnf, kw)
        Sensor(2091)

    def addtag(self, *args):
        Sensor(2092)
        """Internal function."""
        self.tk.call((self._w, 'addtag') + args)
        Sensor(2093)

    def addtag_above(self, newtag, tagOrId):
        Sensor(2094)
        """Add tag NEWTAG to all items above TAGORID."""
        self.addtag(newtag, 'above', tagOrId)
        Sensor(2095)

    def addtag_all(self, newtag):
        Sensor(2096)
        """Add tag NEWTAG to all items."""
        self.addtag(newtag, 'all')
        Sensor(2097)

    def addtag_below(self, newtag, tagOrId):
        Sensor(2098)
        """Add tag NEWTAG to all items below TAGORID."""
        self.addtag(newtag, 'below', tagOrId)
        Sensor(2099)

    def addtag_closest(self, newtag, x, y, halo=None, start=None):
        Sensor(2100)
        """Add tag NEWTAG to item which is closest to pixel at X, Y.
        If several match take the top-most.
        All items closer than HALO are considered overlapping (all are
        closests). If START is specified the next below this tag is taken."""
        self.addtag(newtag, 'closest', x, y, halo, start)
        Sensor(2101)

    def addtag_enclosed(self, newtag, x1, y1, x2, y2):
        Sensor(2102)
        """Add tag NEWTAG to all items in the rectangle defined
        by X1,Y1,X2,Y2."""
        self.addtag(newtag, 'enclosed', x1, y1, x2, y2)
        Sensor(2103)

    def addtag_overlapping(self, newtag, x1, y1, x2, y2):
        Sensor(2104)
        """Add tag NEWTAG to all items which overlap the rectangle
        defined by X1,Y1,X2,Y2."""
        self.addtag(newtag, 'overlapping', x1, y1, x2, y2)
        Sensor(2105)

    def addtag_withtag(self, newtag, tagOrId):
        Sensor(2106)
        """Add tag NEWTAG to all items with TAGORID."""
        self.addtag(newtag, 'withtag', tagOrId)
        Sensor(2107)

    def bbox(self, *args):
        Sensor(2108)
        """Return a tuple of X1,Y1,X2,Y2 coordinates for a rectangle
        which encloses all items with tags specified as arguments."""
        py_ass_parser2109 = self._getints(self.tk.call((self._w, 'bbox') +
            args)) or None
        Sensor(2109)
        return py_ass_parser2109

    def tag_unbind(self, tagOrId, sequence, funcid=None):
        Sensor(2110)
        """Unbind for all items with TAGORID for event SEQUENCE  the
        function identified with FUNCID."""
        self.tk.call(self._w, 'bind', tagOrId, sequence, '')
        if funcid:
            self.deletecommand(funcid)
        Sensor(2111)

    def tag_bind(self, tagOrId, sequence=None, func=None, add=None):
        Sensor(2112)
        """Bind to all items with TAGORID at event SEQUENCE a call to function FUNC.

        An additional boolean parameter ADD specifies whether FUNC will be
        called additionally to the other bound function or whether it will
        replace the previous function. See bind for the return value."""
        py_ass_parser2113 = self._bind((self._w, 'bind', tagOrId), sequence,
            func, add)
        Sensor(2113)
        return py_ass_parser2113

    def canvasx(self, screenx, gridspacing=None):
        Sensor(2114)
        """Return the canvas x coordinate of pixel position SCREENX rounded
        to nearest multiple of GRIDSPACING units."""
        py_ass_parser2115 = getdouble(self.tk.call(self._w, 'canvasx',
            screenx, gridspacing))
        Sensor(2115)
        return py_ass_parser2115

    def canvasy(self, screeny, gridspacing=None):
        Sensor(2116)
        """Return the canvas y coordinate of pixel position SCREENY rounded
        to nearest multiple of GRIDSPACING units."""
        py_ass_parser2117 = getdouble(self.tk.call(self._w, 'canvasy',
            screeny, gridspacing))
        Sensor(2117)
        return py_ass_parser2117

    def coords(self, *args):
        Sensor(2118)
        """Return a list of coordinates for the item given in ARGS."""
        py_ass_parser2119 = map(getdouble, self.tk.splitlist(self.tk.call((
            self._w, 'coords') + args)))
        Sensor(2119)
        return py_ass_parser2119

    def _create(self, itemType, args, kw):
        Sensor(2120)
        """Internal function."""
        args = _flatten(args)
        cnf = args[-1]
        if type(cnf) in (DictionaryType, TupleType):
            args = args[:-1]
        else:
            cnf = {}
        py_ass_parser2121 = getint(self.tk.call(self._w, 'create', itemType,
            *(args + self._options(cnf, kw))))
        Sensor(2121)
        return py_ass_parser2121

    def create_arc(self, *args, **kw):
        Sensor(2122)
        """Create arc shaped region with coordinates x1,y1,x2,y2."""
        py_ass_parser2123 = self._create('arc', args, kw)
        Sensor(2123)
        return py_ass_parser2123

    def create_bitmap(self, *args, **kw):
        Sensor(2124)
        """Create bitmap with coordinates x1,y1."""
        py_ass_parser2125 = self._create('bitmap', args, kw)
        Sensor(2125)
        return py_ass_parser2125

    def create_image(self, *args, **kw):
        Sensor(2126)
        """Create image item with coordinates x1,y1."""
        py_ass_parser2127 = self._create('image', args, kw)
        Sensor(2127)
        return py_ass_parser2127

    def create_line(self, *args, **kw):
        Sensor(2128)
        """Create line with coordinates x1,y1,...,xn,yn."""
        py_ass_parser2129 = self._create('line', args, kw)
        Sensor(2129)
        return py_ass_parser2129

    def create_oval(self, *args, **kw):
        Sensor(2130)
        """Create oval with coordinates x1,y1,x2,y2."""
        py_ass_parser2131 = self._create('oval', args, kw)
        Sensor(2131)
        return py_ass_parser2131

    def create_polygon(self, *args, **kw):
        Sensor(2132)
        """Create polygon with coordinates x1,y1,...,xn,yn."""
        py_ass_parser2133 = self._create('polygon', args, kw)
        Sensor(2133)
        return py_ass_parser2133

    def create_rectangle(self, *args, **kw):
        Sensor(2134)
        """Create rectangle with coordinates x1,y1,x2,y2."""
        py_ass_parser2135 = self._create('rectangle', args, kw)
        Sensor(2135)
        return py_ass_parser2135

    def create_text(self, *args, **kw):
        Sensor(2136)
        """Create text with coordinates x1,y1."""
        py_ass_parser2137 = self._create('text', args, kw)
        Sensor(2137)
        return py_ass_parser2137

    def create_window(self, *args, **kw):
        Sensor(2138)
        """Create window with coordinates x1,y1,x2,y2."""
        py_ass_parser2139 = self._create('window', args, kw)
        Sensor(2139)
        return py_ass_parser2139

    def dchars(self, *args):
        Sensor(2140)
        """Delete characters of text items identified by tag or id in ARGS (possibly
        several times) from FIRST to LAST character (including)."""
        self.tk.call((self._w, 'dchars') + args)
        Sensor(2141)

    def delete(self, *args):
        Sensor(2142)
        """Delete items identified by all tag or ids contained in ARGS."""
        self.tk.call((self._w, 'delete') + args)
        Sensor(2143)

    def dtag(self, *args):
        Sensor(2144)
        """Delete tag or id given as last arguments in ARGS from items
        identified by first argument in ARGS."""
        self.tk.call((self._w, 'dtag') + args)
        Sensor(2145)

    def find(self, *args):
        Sensor(2146)
        """Internal function."""
        py_ass_parser2147 = self._getints(self.tk.call((self._w, 'find') +
            args)) or ()
        Sensor(2147)
        return py_ass_parser2147

    def find_above(self, tagOrId):
        Sensor(2148)
        """Return items above TAGORID."""
        py_ass_parser2149 = self.find('above', tagOrId)
        Sensor(2149)
        return py_ass_parser2149

    def find_all(self):
        Sensor(2150)
        """Return all items."""
        py_ass_parser2151 = self.find('all')
        Sensor(2151)
        return py_ass_parser2151

    def find_below(self, tagOrId):
        Sensor(2152)
        """Return all items below TAGORID."""
        py_ass_parser2153 = self.find('below', tagOrId)
        Sensor(2153)
        return py_ass_parser2153

    def find_closest(self, x, y, halo=None, start=None):
        Sensor(2154)
        """Return item which is closest to pixel at X, Y.
        If several match take the top-most.
        All items closer than HALO are considered overlapping (all are
        closests). If START is specified the next below this tag is taken."""
        py_ass_parser2155 = self.find('closest', x, y, halo, start)
        Sensor(2155)
        return py_ass_parser2155

    def find_enclosed(self, x1, y1, x2, y2):
        Sensor(2156)
        """Return all items in rectangle defined
        by X1,Y1,X2,Y2."""
        py_ass_parser2157 = self.find('enclosed', x1, y1, x2, y2)
        Sensor(2157)
        return py_ass_parser2157

    def find_overlapping(self, x1, y1, x2, y2):
        Sensor(2158)
        """Return all items which overlap the rectangle
        defined by X1,Y1,X2,Y2."""
        py_ass_parser2159 = self.find('overlapping', x1, y1, x2, y2)
        Sensor(2159)
        return py_ass_parser2159

    def find_withtag(self, tagOrId):
        Sensor(2160)
        """Return all items with TAGORID."""
        py_ass_parser2161 = self.find('withtag', tagOrId)
        Sensor(2161)
        return py_ass_parser2161

    def focus(self, *args):
        Sensor(2162)
        """Set focus to the first item specified in ARGS."""
        py_ass_parser2163 = self.tk.call((self._w, 'focus') + args)
        Sensor(2163)
        return py_ass_parser2163

    def gettags(self, *args):
        Sensor(2164)
        """Return tags associated with the first item specified in ARGS."""
        py_ass_parser2165 = self.tk.splitlist(self.tk.call((self._w,
            'gettags') + args))
        Sensor(2165)
        return py_ass_parser2165

    def icursor(self, *args):
        Sensor(2166)
        """Set cursor at position POS in the item identified by TAGORID.
        In ARGS TAGORID must be first."""
        self.tk.call((self._w, 'icursor') + args)
        Sensor(2167)

    def index(self, *args):
        Sensor(2168)
        """Return position of cursor as integer in item specified in ARGS."""
        py_ass_parser2169 = getint(self.tk.call((self._w, 'index') + args))
        Sensor(2169)
        return py_ass_parser2169

    def insert(self, *args):
        Sensor(2170)
        """Insert TEXT in item TAGORID at position POS. ARGS must
        be TAGORID POS TEXT."""
        self.tk.call((self._w, 'insert') + args)
        Sensor(2171)

    def itemcget(self, tagOrId, option):
        Sensor(2172)
        """Return the resource value for an OPTION for item TAGORID."""
        py_ass_parser2173 = self.tk.call((self._w, 'itemcget') + (tagOrId, 
            '-' + option))
        Sensor(2173)
        return py_ass_parser2173

    def itemconfigure(self, tagOrId, cnf=None, **kw):
        Sensor(2174)
        """Configure resources of an item TAGORID.

        The values for resources are specified as keyword
        arguments. To get an overview about
        the allowed keyword arguments call the method without arguments.
        """
        py_ass_parser2175 = self._configure(('itemconfigure', tagOrId), cnf, kw
            )
        Sensor(2175)
        return py_ass_parser2175
    itemconfig = itemconfigure

    def tag_lower(self, *args):
        Sensor(2176)
        """Lower an item TAGORID given in ARGS
        (optional below another item)."""
        self.tk.call((self._w, 'lower') + args)
        Sensor(2177)
    lower = tag_lower

    def move(self, *args):
        Sensor(2178)
        """Move an item TAGORID given in ARGS."""
        self.tk.call((self._w, 'move') + args)
        Sensor(2179)

    def postscript(self, cnf={}, **kw):
        Sensor(2180)
        """Print the contents of the canvas to a postscript
        file. Valid options: colormap, colormode, file, fontmap,
        height, pageanchor, pageheight, pagewidth, pagex, pagey,
        rotate, witdh, x, y."""
        py_ass_parser2181 = self.tk.call((self._w, 'postscript') + self.
            _options(cnf, kw))
        Sensor(2181)
        return py_ass_parser2181

    def tag_raise(self, *args):
        Sensor(2182)
        """Raise an item TAGORID given in ARGS
        (optional above another item)."""
        self.tk.call((self._w, 'raise') + args)
        Sensor(2183)
    lift = tkraise = tag_raise

    def scale(self, *args):
        Sensor(2184)
        """Scale item TAGORID with XORIGIN, YORIGIN, XSCALE, YSCALE."""
        self.tk.call((self._w, 'scale') + args)
        Sensor(2185)

    def scan_mark(self, x, y):
        Sensor(2186)
        """Remember the current X, Y coordinates."""
        self.tk.call(self._w, 'scan', 'mark', x, y)
        Sensor(2187)

    def scan_dragto(self, x, y, gain=10):
        Sensor(2188)
        """Adjust the view of the canvas to GAIN times the
        difference between X and Y and the coordinates given in
        scan_mark."""
        self.tk.call(self._w, 'scan', 'dragto', x, y, gain)
        Sensor(2189)

    def select_adjust(self, tagOrId, index):
        Sensor(2190)
        """Adjust the end of the selection near the cursor of an item TAGORID to index."""
        self.tk.call(self._w, 'select', 'adjust', tagOrId, index)
        Sensor(2191)

    def select_clear(self):
        Sensor(2192)
        """Clear the selection if it is in this widget."""
        self.tk.call(self._w, 'select', 'clear')
        Sensor(2193)

    def select_from(self, tagOrId, index):
        Sensor(2194)
        """Set the fixed end of a selection in item TAGORID to INDEX."""
        self.tk.call(self._w, 'select', 'from', tagOrId, index)
        Sensor(2195)

    def select_item(self):
        Sensor(2196)
        """Return the item which has the selection."""
        py_ass_parser2197 = self.tk.call(self._w, 'select', 'item') or None
        Sensor(2197)
        return py_ass_parser2197

    def select_to(self, tagOrId, index):
        Sensor(2198)
        """Set the variable end of a selection in item TAGORID to INDEX."""
        self.tk.call(self._w, 'select', 'to', tagOrId, index)
        Sensor(2199)

    def type(self, tagOrId):
        Sensor(2200)
        """Return the type of the item TAGORID."""
        py_ass_parser2201 = self.tk.call(self._w, 'type', tagOrId) or None
        Sensor(2201)
        return py_ass_parser2201


class Checkbutton(Widget):
    """Checkbutton widget which is either in on- or off-state."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2202)
        """Construct a checkbutton widget with the parent MASTER.

        Valid resource names: activebackground, activeforeground, anchor,
        background, bd, bg, bitmap, borderwidth, command, cursor,
        disabledforeground, fg, font, foreground, height,
        highlightbackground, highlightcolor, highlightthickness, image,
        indicatoron, justify, offvalue, onvalue, padx, pady, relief,
        selectcolor, selectimage, state, takefocus, text, textvariable,
        underline, variable, width, wraplength."""
        Widget.__init__(self, master, 'checkbutton', cnf, kw)
        Sensor(2203)

    def deselect(self):
        Sensor(2204)
        """Put the button in off-state."""
        self.tk.call(self._w, 'deselect')
        Sensor(2205)

    def flash(self):
        Sensor(2206)
        """Flash the button."""
        self.tk.call(self._w, 'flash')
        Sensor(2207)

    def invoke(self):
        Sensor(2208)
        """Toggle the button and invoke a command if given as resource."""
        py_ass_parser2209 = self.tk.call(self._w, 'invoke')
        Sensor(2209)
        return py_ass_parser2209

    def select(self):
        Sensor(2210)
        """Put the button in on-state."""
        self.tk.call(self._w, 'select')
        Sensor(2211)

    def toggle(self):
        Sensor(2212)
        """Toggle the button."""
        self.tk.call(self._w, 'toggle')
        Sensor(2213)


class Entry(Widget, XView):
    """Entry widget which allows to display simple text."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2214)
        """Construct an entry widget with the parent MASTER.

        Valid resource names: background, bd, bg, borderwidth, cursor,
        exportselection, fg, font, foreground, highlightbackground,
        highlightcolor, highlightthickness, insertbackground,
        insertborderwidth, insertofftime, insertontime, insertwidth,
        invalidcommand, invcmd, justify, relief, selectbackground,
        selectborderwidth, selectforeground, show, state, takefocus,
        textvariable, validate, validatecommand, vcmd, width,
        xscrollcommand."""
        Widget.__init__(self, master, 'entry', cnf, kw)
        Sensor(2215)

    def delete(self, first, last=None):
        Sensor(2216)
        """Delete text from FIRST to LAST (not included)."""
        self.tk.call(self._w, 'delete', first, last)
        Sensor(2217)

    def get(self):
        Sensor(2218)
        """Return the text."""
        py_ass_parser2219 = self.tk.call(self._w, 'get')
        Sensor(2219)
        return py_ass_parser2219

    def icursor(self, index):
        Sensor(2220)
        """Insert cursor at INDEX."""
        self.tk.call(self._w, 'icursor', index)
        Sensor(2221)

    def index(self, index):
        Sensor(2222)
        """Return position of cursor."""
        py_ass_parser2223 = getint(self.tk.call(self._w, 'index', index))
        Sensor(2223)
        return py_ass_parser2223

    def insert(self, index, string):
        Sensor(2224)
        """Insert STRING at INDEX."""
        self.tk.call(self._w, 'insert', index, string)
        Sensor(2225)

    def scan_mark(self, x):
        Sensor(2226)
        """Remember the current X, Y coordinates."""
        self.tk.call(self._w, 'scan', 'mark', x)
        Sensor(2227)

    def scan_dragto(self, x):
        Sensor(2228)
        """Adjust the view of the canvas to 10 times the
        difference between X and Y and the coordinates given in
        scan_mark."""
        self.tk.call(self._w, 'scan', 'dragto', x)
        Sensor(2229)

    def selection_adjust(self, index):
        Sensor(2230)
        """Adjust the end of the selection near the cursor to INDEX."""
        self.tk.call(self._w, 'selection', 'adjust', index)
        Sensor(2231)
    select_adjust = selection_adjust

    def selection_clear(self):
        Sensor(2232)
        """Clear the selection if it is in this widget."""
        self.tk.call(self._w, 'selection', 'clear')
        Sensor(2233)
    select_clear = selection_clear

    def selection_from(self, index):
        Sensor(2234)
        """Set the fixed end of a selection to INDEX."""
        self.tk.call(self._w, 'selection', 'from', index)
        Sensor(2235)
    select_from = selection_from

    def selection_present(self):
        Sensor(2236)
        """Return True if there are characters selected in the entry, False
        otherwise."""
        py_ass_parser2237 = self.tk.getboolean(self.tk.call(self._w,
            'selection', 'present'))
        Sensor(2237)
        return py_ass_parser2237
    select_present = selection_present

    def selection_range(self, start, end):
        Sensor(2238)
        """Set the selection from START to END (not included)."""
        self.tk.call(self._w, 'selection', 'range', start, end)
        Sensor(2239)
    select_range = selection_range

    def selection_to(self, index):
        Sensor(2240)
        """Set the variable end of a selection to INDEX."""
        self.tk.call(self._w, 'selection', 'to', index)
        Sensor(2241)
    select_to = selection_to


class Frame(Widget):
    """Frame widget which may contain other widgets and can have a 3D border."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2242)
        """Construct a frame widget with the parent MASTER.

        Valid resource names: background, bd, bg, borderwidth, class,
        colormap, container, cursor, height, highlightbackground,
        highlightcolor, highlightthickness, relief, takefocus, visual, width."""
        cnf = _cnfmerge((cnf, kw))
        extra = ()
        if 'class_' in cnf:
            extra = '-class', cnf['class_']
            del cnf['class_']
        elif 'class' in cnf:
            extra = '-class', cnf['class']
            del cnf['class']
        Widget.__init__(self, master, 'frame', cnf, {}, extra)
        Sensor(2243)


class Label(Widget):
    """Label widget which can display text and bitmaps."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2244)
        """Construct a label widget with the parent MASTER.

        STANDARD OPTIONS

            activebackground, activeforeground, anchor,
            background, bitmap, borderwidth, cursor,
            disabledforeground, font, foreground,
            highlightbackground, highlightcolor,
            highlightthickness, image, justify,
            padx, pady, relief, takefocus, text,
            textvariable, underline, wraplength

        WIDGET-SPECIFIC OPTIONS

            height, state, width

        """
        Widget.__init__(self, master, 'label', cnf, kw)
        Sensor(2245)


class Listbox(Widget, XView, YView):
    """Listbox widget which can display a list of strings."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2246)
        """Construct a listbox widget with the parent MASTER.

        Valid resource names: background, bd, bg, borderwidth, cursor,
        exportselection, fg, font, foreground, height, highlightbackground,
        highlightcolor, highlightthickness, relief, selectbackground,
        selectborderwidth, selectforeground, selectmode, setgrid, takefocus,
        width, xscrollcommand, yscrollcommand, listvariable."""
        Widget.__init__(self, master, 'listbox', cnf, kw)
        Sensor(2247)

    def activate(self, index):
        Sensor(2248)
        """Activate item identified by INDEX."""
        self.tk.call(self._w, 'activate', index)
        Sensor(2249)

    def bbox(self, *args):
        Sensor(2250)
        """Return a tuple of X1,Y1,X2,Y2 coordinates for a rectangle
        which encloses the item identified by index in ARGS."""
        py_ass_parser2251 = self._getints(self.tk.call((self._w, 'bbox') +
            args)) or None
        Sensor(2251)
        return py_ass_parser2251

    def curselection(self):
        Sensor(2252)
        """Return list of indices of currently selected item."""
        py_ass_parser2253 = self.tk.splitlist(self.tk.call(self._w,
            'curselection'))
        Sensor(2253)
        return py_ass_parser2253

    def delete(self, first, last=None):
        Sensor(2254)
        """Delete items from FIRST to LAST (not included)."""
        self.tk.call(self._w, 'delete', first, last)
        Sensor(2255)

    def get(self, first, last=None):
        Sensor(2256)
        """Get list of items from FIRST to LAST (not included)."""
        if last:
            py_ass_parser2257 = self.tk.splitlist(self.tk.call(self._w,
                'get', first, last))
            Sensor(2257)
            return py_ass_parser2257
        else:
            py_ass_parser2258 = self.tk.call(self._w, 'get', first)
            Sensor(2258)
            return py_ass_parser2258
        Sensor(2259)

    def index(self, index):
        Sensor(2260)
        """Return index of item identified with INDEX."""
        i = self.tk.call(self._w, 'index', index)
        if i == 'none':
            py_ass_parser2261 = None
            Sensor(2261)
            return py_ass_parser2261
        py_ass_parser2262 = getint(i)
        Sensor(2262)
        return py_ass_parser2262

    def insert(self, index, *elements):
        Sensor(2263)
        """Insert ELEMENTS at INDEX."""
        self.tk.call((self._w, 'insert', index) + elements)
        Sensor(2264)

    def nearest(self, y):
        Sensor(2265)
        """Get index of item which is nearest to y coordinate Y."""
        py_ass_parser2266 = getint(self.tk.call(self._w, 'nearest', y))
        Sensor(2266)
        return py_ass_parser2266

    def scan_mark(self, x, y):
        Sensor(2267)
        """Remember the current X, Y coordinates."""
        self.tk.call(self._w, 'scan', 'mark', x, y)
        Sensor(2268)

    def scan_dragto(self, x, y):
        Sensor(2269)
        """Adjust the view of the listbox to 10 times the
        difference between X and Y and the coordinates given in
        scan_mark."""
        self.tk.call(self._w, 'scan', 'dragto', x, y)
        Sensor(2270)

    def see(self, index):
        Sensor(2271)
        """Scroll such that INDEX is visible."""
        self.tk.call(self._w, 'see', index)
        Sensor(2272)

    def selection_anchor(self, index):
        Sensor(2273)
        """Set the fixed end oft the selection to INDEX."""
        self.tk.call(self._w, 'selection', 'anchor', index)
        Sensor(2274)
    select_anchor = selection_anchor

    def selection_clear(self, first, last=None):
        Sensor(2275)
        """Clear the selection from FIRST to LAST (not included)."""
        self.tk.call(self._w, 'selection', 'clear', first, last)
        Sensor(2276)
    select_clear = selection_clear

    def selection_includes(self, index):
        Sensor(2277)
        """Return 1 if INDEX is part of the selection."""
        py_ass_parser2278 = self.tk.getboolean(self.tk.call(self._w,
            'selection', 'includes', index))
        Sensor(2278)
        return py_ass_parser2278
    select_includes = selection_includes

    def selection_set(self, first, last=None):
        Sensor(2279)
        """Set the selection from FIRST to LAST (not included) without
        changing the currently selected elements."""
        self.tk.call(self._w, 'selection', 'set', first, last)
        Sensor(2280)
    select_set = selection_set

    def size(self):
        Sensor(2281)
        """Return the number of elements in the listbox."""
        py_ass_parser2282 = getint(self.tk.call(self._w, 'size'))
        Sensor(2282)
        return py_ass_parser2282

    def itemcget(self, index, option):
        Sensor(2283)
        """Return the resource value for an ITEM and an OPTION."""
        py_ass_parser2284 = self.tk.call((self._w, 'itemcget') + (index, 
            '-' + option))
        Sensor(2284)
        return py_ass_parser2284

    def itemconfigure(self, index, cnf=None, **kw):
        Sensor(2285)
        """Configure resources of an ITEM.

        The values for resources are specified as keyword arguments.
        To get an overview about the allowed keyword arguments
        call the method without arguments.
        Valid resource names: background, bg, foreground, fg,
        selectbackground, selectforeground."""
        py_ass_parser2286 = self._configure(('itemconfigure', index), cnf, kw)
        Sensor(2286)
        return py_ass_parser2286
    itemconfig = itemconfigure


class Menu(Widget):
    """Menu widget which allows to display menu bars, pull-down menus and pop-up menus."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2287)
        """Construct menu widget with the parent MASTER.

        Valid resource names: activebackground, activeborderwidth,
        activeforeground, background, bd, bg, borderwidth, cursor,
        disabledforeground, fg, font, foreground, postcommand, relief,
        selectcolor, takefocus, tearoff, tearoffcommand, title, type."""
        Widget.__init__(self, master, 'menu', cnf, kw)
        Sensor(2288)

    def tk_bindForTraversal(self):
        Sensor(2289)
        pass
        Sensor(2290)

    def tk_mbPost(self):
        Sensor(2291)
        self.tk.call('tk_mbPost', self._w)
        Sensor(2292)

    def tk_mbUnpost(self):
        Sensor(2293)
        self.tk.call('tk_mbUnpost')
        Sensor(2294)

    def tk_traverseToMenu(self, char):
        Sensor(2295)
        self.tk.call('tk_traverseToMenu', self._w, char)
        Sensor(2296)

    def tk_traverseWithinMenu(self, char):
        Sensor(2297)
        self.tk.call('tk_traverseWithinMenu', self._w, char)
        Sensor(2298)

    def tk_getMenuButtons(self):
        py_ass_parser2299 = self.tk.call('tk_getMenuButtons', self._w)
        Sensor(2299)
        return py_ass_parser2299

    def tk_nextMenu(self, count):
        Sensor(2300)
        self.tk.call('tk_nextMenu', count)
        Sensor(2301)

    def tk_nextMenuEntry(self, count):
        Sensor(2302)
        self.tk.call('tk_nextMenuEntry', count)
        Sensor(2303)

    def tk_invokeMenu(self):
        Sensor(2304)
        self.tk.call('tk_invokeMenu', self._w)
        Sensor(2305)

    def tk_firstMenu(self):
        Sensor(2306)
        self.tk.call('tk_firstMenu', self._w)
        Sensor(2307)

    def tk_mbButtonDown(self):
        Sensor(2308)
        self.tk.call('tk_mbButtonDown', self._w)
        Sensor(2309)

    def tk_popup(self, x, y, entry=''):
        Sensor(2310)
        """Post the menu at position X,Y with entry ENTRY."""
        self.tk.call('tk_popup', self._w, x, y, entry)
        Sensor(2311)

    def activate(self, index):
        Sensor(2312)
        """Activate entry at INDEX."""
        self.tk.call(self._w, 'activate', index)
        Sensor(2313)

    def add(self, itemType, cnf={}, **kw):
        Sensor(2314)
        """Internal function."""
        self.tk.call((self._w, 'add', itemType) + self._options(cnf, kw))
        Sensor(2315)

    def add_cascade(self, cnf={}, **kw):
        Sensor(2316)
        """Add hierarchical menu item."""
        self.add('cascade', cnf or kw)
        Sensor(2317)

    def add_checkbutton(self, cnf={}, **kw):
        Sensor(2318)
        """Add checkbutton menu item."""
        self.add('checkbutton', cnf or kw)
        Sensor(2319)

    def add_command(self, cnf={}, **kw):
        Sensor(2320)
        """Add command menu item."""
        self.add('command', cnf or kw)
        Sensor(2321)

    def add_radiobutton(self, cnf={}, **kw):
        Sensor(2322)
        """Addd radio menu item."""
        self.add('radiobutton', cnf or kw)
        Sensor(2323)

    def add_separator(self, cnf={}, **kw):
        Sensor(2324)
        """Add separator."""
        self.add('separator', cnf or kw)
        Sensor(2325)

    def insert(self, index, itemType, cnf={}, **kw):
        Sensor(2326)
        """Internal function."""
        self.tk.call((self._w, 'insert', index, itemType) + self._options(
            cnf, kw))
        Sensor(2327)

    def insert_cascade(self, index, cnf={}, **kw):
        Sensor(2328)
        """Add hierarchical menu item at INDEX."""
        self.insert(index, 'cascade', cnf or kw)
        Sensor(2329)

    def insert_checkbutton(self, index, cnf={}, **kw):
        Sensor(2330)
        """Add checkbutton menu item at INDEX."""
        self.insert(index, 'checkbutton', cnf or kw)
        Sensor(2331)

    def insert_command(self, index, cnf={}, **kw):
        Sensor(2332)
        """Add command menu item at INDEX."""
        self.insert(index, 'command', cnf or kw)
        Sensor(2333)

    def insert_radiobutton(self, index, cnf={}, **kw):
        Sensor(2334)
        """Addd radio menu item at INDEX."""
        self.insert(index, 'radiobutton', cnf or kw)
        Sensor(2335)

    def insert_separator(self, index, cnf={}, **kw):
        Sensor(2336)
        """Add separator at INDEX."""
        self.insert(index, 'separator', cnf or kw)
        Sensor(2337)

    def delete(self, index1, index2=None):
        Sensor(2338)
        """Delete menu items between INDEX1 and INDEX2 (included)."""
        if index2 is None:
            index2 = index1
        num_index1, num_index2 = self.index(index1), self.index(index2)
        if num_index1 is None or num_index2 is None:
            num_index1, num_index2 = 0, -1
        for i in range(num_index1, num_index2 + 1):
            if 'command' in self.entryconfig(i):
                c = str(self.entrycget(i, 'command'))
                if c:
                    self.deletecommand(c)
        self.tk.call(self._w, 'delete', index1, index2)
        Sensor(2339)

    def entrycget(self, index, option):
        Sensor(2340)
        """Return the resource value of an menu item for OPTION at INDEX."""
        py_ass_parser2341 = self.tk.call(self._w, 'entrycget', index, '-' +
            option)
        Sensor(2341)
        return py_ass_parser2341

    def entryconfigure(self, index, cnf=None, **kw):
        Sensor(2342)
        """Configure a menu item at INDEX."""
        py_ass_parser2343 = self._configure(('entryconfigure', index), cnf, kw)
        Sensor(2343)
        return py_ass_parser2343
    entryconfig = entryconfigure

    def index(self, index):
        Sensor(2344)
        """Return the index of a menu item identified by INDEX."""
        i = self.tk.call(self._w, 'index', index)
        if i == 'none':
            py_ass_parser2345 = None
            Sensor(2345)
            return py_ass_parser2345
        py_ass_parser2346 = getint(i)
        Sensor(2346)
        return py_ass_parser2346

    def invoke(self, index):
        Sensor(2347)
        """Invoke a menu item identified by INDEX and execute
        the associated command."""
        py_ass_parser2348 = self.tk.call(self._w, 'invoke', index)
        Sensor(2348)
        return py_ass_parser2348

    def post(self, x, y):
        Sensor(2349)
        """Display a menu at position X,Y."""
        self.tk.call(self._w, 'post', x, y)
        Sensor(2350)

    def type(self, index):
        Sensor(2351)
        """Return the type of the menu item at INDEX."""
        py_ass_parser2352 = self.tk.call(self._w, 'type', index)
        Sensor(2352)
        return py_ass_parser2352

    def unpost(self):
        Sensor(2353)
        """Unmap a menu."""
        self.tk.call(self._w, 'unpost')
        Sensor(2354)

    def yposition(self, index):
        Sensor(2355)
        """Return the y-position of the topmost pixel of the menu item at INDEX."""
        py_ass_parser2356 = getint(self.tk.call(self._w, 'yposition', index))
        Sensor(2356)
        return py_ass_parser2356


class Menubutton(Widget):
    """Menubutton widget, obsolete since Tk8.0."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2357)
        Widget.__init__(self, master, 'menubutton', cnf, kw)
        Sensor(2358)


class Message(Widget):
    """Message widget to display multiline text. Obsolete since Label does it too."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2359)
        Widget.__init__(self, master, 'message', cnf, kw)
        Sensor(2360)


class Radiobutton(Widget):
    """Radiobutton widget which shows only one of several buttons in on-state."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2361)
        """Construct a radiobutton widget with the parent MASTER.

        Valid resource names: activebackground, activeforeground, anchor,
        background, bd, bg, bitmap, borderwidth, command, cursor,
        disabledforeground, fg, font, foreground, height,
        highlightbackground, highlightcolor, highlightthickness, image,
        indicatoron, justify, padx, pady, relief, selectcolor, selectimage,
        state, takefocus, text, textvariable, underline, value, variable,
        width, wraplength."""
        Widget.__init__(self, master, 'radiobutton', cnf, kw)
        Sensor(2362)

    def deselect(self):
        Sensor(2363)
        """Put the button in off-state."""
        self.tk.call(self._w, 'deselect')
        Sensor(2364)

    def flash(self):
        Sensor(2365)
        """Flash the button."""
        self.tk.call(self._w, 'flash')
        Sensor(2366)

    def invoke(self):
        Sensor(2367)
        """Toggle the button and invoke a command if given as resource."""
        py_ass_parser2368 = self.tk.call(self._w, 'invoke')
        Sensor(2368)
        return py_ass_parser2368

    def select(self):
        Sensor(2369)
        """Put the button in on-state."""
        self.tk.call(self._w, 'select')
        Sensor(2370)


class Scale(Widget):
    """Scale widget which can display a numerical scale."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2371)
        """Construct a scale widget with the parent MASTER.

        Valid resource names: activebackground, background, bigincrement, bd,
        bg, borderwidth, command, cursor, digits, fg, font, foreground, from,
        highlightbackground, highlightcolor, highlightthickness, label,
        length, orient, relief, repeatdelay, repeatinterval, resolution,
        showvalue, sliderlength, sliderrelief, state, takefocus,
        tickinterval, to, troughcolor, variable, width."""
        Widget.__init__(self, master, 'scale', cnf, kw)
        Sensor(2372)

    def get(self):
        Sensor(2373)
        """Get the current value as integer or float."""
        value = self.tk.call(self._w, 'get')
        try:
            py_ass_parser2374 = getint(value)
            Sensor(2374)
            return py_ass_parser2374
        except ValueError:
            py_ass_parser2375 = getdouble(value)
            Sensor(2375)
            return py_ass_parser2375
        Sensor(2376)

    def set(self, value):
        Sensor(2377)
        """Set the value to VALUE."""
        self.tk.call(self._w, 'set', value)
        Sensor(2378)

    def coords(self, value=None):
        Sensor(2379)
        """Return a tuple (X,Y) of the point along the centerline of the
        trough that corresponds to VALUE or the current value if None is
        given."""
        py_ass_parser2380 = self._getints(self.tk.call(self._w, 'coords',
            value))
        Sensor(2380)
        return py_ass_parser2380

    def identify(self, x, y):
        Sensor(2381)
        """Return where the point X,Y lies. Valid return values are "slider",
        "though1" and "though2"."""
        py_ass_parser2382 = self.tk.call(self._w, 'identify', x, y)
        Sensor(2382)
        return py_ass_parser2382


class Scrollbar(Widget):
    """Scrollbar widget which displays a slider at a certain position."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2383)
        """Construct a scrollbar widget with the parent MASTER.

        Valid resource names: activebackground, activerelief,
        background, bd, bg, borderwidth, command, cursor,
        elementborderwidth, highlightbackground,
        highlightcolor, highlightthickness, jump, orient,
        relief, repeatdelay, repeatinterval, takefocus,
        troughcolor, width."""
        Widget.__init__(self, master, 'scrollbar', cnf, kw)
        Sensor(2384)

    def activate(self, index):
        Sensor(2385)
        """Display the element at INDEX with activebackground and activerelief.
        INDEX can be "arrow1","slider" or "arrow2"."""
        self.tk.call(self._w, 'activate', index)
        Sensor(2386)

    def delta(self, deltax, deltay):
        Sensor(2387)
        """Return the fractional change of the scrollbar setting if it
        would be moved by DELTAX or DELTAY pixels."""
        py_ass_parser2388 = getdouble(self.tk.call(self._w, 'delta', deltax,
            deltay))
        Sensor(2388)
        return py_ass_parser2388

    def fraction(self, x, y):
        Sensor(2389)
        """Return the fractional value which corresponds to a slider
        position of X,Y."""
        py_ass_parser2390 = getdouble(self.tk.call(self._w, 'fraction', x, y))
        Sensor(2390)
        return py_ass_parser2390

    def identify(self, x, y):
        Sensor(2391)
        """Return the element under position X,Y as one of
        "arrow1","slider","arrow2" or ""."""
        py_ass_parser2392 = self.tk.call(self._w, 'identify', x, y)
        Sensor(2392)
        return py_ass_parser2392

    def get(self):
        Sensor(2393)
        """Return the current fractional values (upper and lower end)
        of the slider position."""
        py_ass_parser2394 = self._getdoubles(self.tk.call(self._w, 'get'))
        Sensor(2394)
        return py_ass_parser2394

    def set(self, *args):
        Sensor(2395)
        """Set the fractional values of the slider position (upper and
        lower ends as value between 0 and 1)."""
        self.tk.call((self._w, 'set') + args)
        Sensor(2396)


class Text(Widget, XView, YView):
    """Text widget which can display text in various forms."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2397)
        """Construct a text widget with the parent MASTER.

        STANDARD OPTIONS

            background, borderwidth, cursor,
            exportselection, font, foreground,
            highlightbackground, highlightcolor,
            highlightthickness, insertbackground,
            insertborderwidth, insertofftime,
            insertontime, insertwidth, padx, pady,
            relief, selectbackground,
            selectborderwidth, selectforeground,
            setgrid, takefocus,
            xscrollcommand, yscrollcommand,

        WIDGET-SPECIFIC OPTIONS

            autoseparators, height, maxundo,
            spacing1, spacing2, spacing3,
            state, tabs, undo, width, wrap,

        """
        Widget.__init__(self, master, 'text', cnf, kw)
        Sensor(2398)

    def bbox(self, *args):
        Sensor(2399)
        """Return a tuple of (x,y,width,height) which gives the bounding
        box of the visible part of the character at the index in ARGS."""
        py_ass_parser2400 = self._getints(self.tk.call((self._w, 'bbox') +
            args)) or None
        Sensor(2400)
        return py_ass_parser2400

    def tk_textSelectTo(self, index):
        Sensor(2401)
        self.tk.call('tk_textSelectTo', self._w, index)
        Sensor(2402)

    def tk_textBackspace(self):
        Sensor(2403)
        self.tk.call('tk_textBackspace', self._w)
        Sensor(2404)

    def tk_textIndexCloser(self, a, b, c):
        Sensor(2405)
        self.tk.call('tk_textIndexCloser', self._w, a, b, c)
        Sensor(2406)

    def tk_textResetAnchor(self, index):
        Sensor(2407)
        self.tk.call('tk_textResetAnchor', self._w, index)
        Sensor(2408)

    def compare(self, index1, op, index2):
        Sensor(2409)
        """Return whether between index INDEX1 and index INDEX2 the
        relation OP is satisfied. OP is one of <, <=, ==, >=, >, or !=."""
        py_ass_parser2410 = self.tk.getboolean(self.tk.call(self._w,
            'compare', index1, op, index2))
        Sensor(2410)
        return py_ass_parser2410

    def debug(self, boolean=None):
        Sensor(2411)
        """Turn on the internal consistency checks of the B-Tree inside the text
        widget according to BOOLEAN."""
        py_ass_parser2412 = self.tk.getboolean(self.tk.call(self._w,
            'debug', boolean))
        Sensor(2412)
        return py_ass_parser2412

    def delete(self, index1, index2=None):
        Sensor(2413)
        """Delete the characters between INDEX1 and INDEX2 (not included)."""
        self.tk.call(self._w, 'delete', index1, index2)
        Sensor(2414)

    def dlineinfo(self, index):
        Sensor(2415)
        """Return tuple (x,y,width,height,baseline) giving the bounding box
        and baseline position of the visible part of the line containing
        the character at INDEX."""
        py_ass_parser2416 = self._getints(self.tk.call(self._w, 'dlineinfo',
            index))
        Sensor(2416)
        return py_ass_parser2416

    def dump(self, index1, index2=None, command=None, **kw):
        Sensor(2417)
        """Return the contents of the widget between index1 and index2.

        The type of contents returned in filtered based on the keyword
        parameters; if 'all', 'image', 'mark', 'tag', 'text', or 'window' are
        given and true, then the corresponding items are returned. The result
        is a list of triples of the form (key, value, index). If none of the
        keywords are true then 'all' is used by default.

        If the 'command' argument is given, it is called once for each element
        of the list of triples, with the values of each triple serving as the
        arguments to the function. In this case the list is not returned."""
        args = []
        func_name = None
        result = None
        if not command:
            result = []

            def append_triple(key, value, index, result=result):
                Sensor(2418)
                result.append((key, value, index))
                Sensor(2419)
            command = append_triple
        Sensor(2420)
        try:
            if not isinstance(command, str):
                func_name = command = self._register(command)
            args += ['-command', command]
            for key in kw:
                if kw[key]:
                    args.append('-' + key)
            args.append(index1)
            if index2:
                args.append(index2)
            self.tk.call(self._w, 'dump', *args)
            py_ass_parser2421 = result
            Sensor(2421)
            return py_ass_parser2421
        finally:
            if func_name:
                self.deletecommand(func_name)
        Sensor(2422)

    def edit(self, *args):
        Sensor(2423)
        """Internal method

        This method controls the undo mechanism and
        the modified flag. The exact behavior of the
        command depends on the option argument that
        follows the edit argument. The following forms
        of the command are currently supported:

        edit_modified, edit_redo, edit_reset, edit_separator
        and edit_undo

        """
        py_ass_parser2424 = self.tk.call(self._w, 'edit', *args)
        Sensor(2424)
        return py_ass_parser2424

    def edit_modified(self, arg=None):
        Sensor(2425)
        """Get or Set the modified flag

        If arg is not specified, returns the modified
        flag of the widget. The insert, delete, edit undo and
        edit redo commands or the user can set or clear the
        modified flag. If boolean is specified, sets the
        modified flag of the widget to arg.
        """
        py_ass_parser2426 = self.edit('modified', arg)
        Sensor(2426)
        return py_ass_parser2426

    def edit_redo(self):
        Sensor(2427)
        """Redo the last undone edit

        When the undo option is true, reapplies the last
        undone edits provided no other edits were done since
        then. Generates an error when the redo stack is empty.
        Does nothing when the undo option is false.
        """
        py_ass_parser2428 = self.edit('redo')
        Sensor(2428)
        return py_ass_parser2428

    def edit_reset(self):
        Sensor(2429)
        """Clears the undo and redo stacks
        """
        py_ass_parser2430 = self.edit('reset')
        Sensor(2430)
        return py_ass_parser2430

    def edit_separator(self):
        Sensor(2431)
        """Inserts a separator (boundary) on the undo stack.

        Does nothing when the undo option is false
        """
        py_ass_parser2432 = self.edit('separator')
        Sensor(2432)
        return py_ass_parser2432

    def edit_undo(self):
        Sensor(2433)
        """Undoes the last edit action

        If the undo option is true. An edit action is defined
        as all the insert and delete commands that are recorded
        on the undo stack in between two separators. Generates
        an error when the undo stack is empty. Does nothing
        when the undo option is false
        """
        py_ass_parser2434 = self.edit('undo')
        Sensor(2434)
        return py_ass_parser2434

    def get(self, index1, index2=None):
        Sensor(2435)
        """Return the text from INDEX1 to INDEX2 (not included)."""
        py_ass_parser2436 = self.tk.call(self._w, 'get', index1, index2)
        Sensor(2436)
        return py_ass_parser2436

    def image_cget(self, index, option):
        Sensor(2437)
        """Return the value of OPTION of an embedded image at INDEX."""
        if option[:1] != '-':
            option = '-' + option
        if option[-1:] == '_':
            option = option[:-1]
        py_ass_parser2438 = self.tk.call(self._w, 'image', 'cget', index,
            option)
        Sensor(2438)
        return py_ass_parser2438

    def image_configure(self, index, cnf=None, **kw):
        Sensor(2439)
        """Configure an embedded image at INDEX."""
        py_ass_parser2440 = self._configure(('image', 'configure', index),
            cnf, kw)
        Sensor(2440)
        return py_ass_parser2440

    def image_create(self, index, cnf={}, **kw):
        Sensor(2441)
        """Create an embedded image at INDEX."""
        py_ass_parser2442 = self.tk.call(self._w, 'image', 'create', index,
            *self._options(cnf, kw))
        Sensor(2442)
        return py_ass_parser2442

    def image_names(self):
        Sensor(2443)
        """Return all names of embedded images in this widget."""
        py_ass_parser2444 = self.tk.call(self._w, 'image', 'names')
        Sensor(2444)
        return py_ass_parser2444

    def index(self, index):
        Sensor(2445)
        """Return the index in the form line.char for INDEX."""
        py_ass_parser2446 = str(self.tk.call(self._w, 'index', index))
        Sensor(2446)
        return py_ass_parser2446

    def insert(self, index, chars, *args):
        Sensor(2447)
        """Insert CHARS before the characters at INDEX. An additional
        tag can be given in ARGS. Additional CHARS and tags can follow in ARGS."""
        self.tk.call((self._w, 'insert', index, chars) + args)
        Sensor(2448)

    def mark_gravity(self, markName, direction=None):
        Sensor(2449)
        """Change the gravity of a mark MARKNAME to DIRECTION (LEFT or RIGHT).
        Return the current value if None is given for DIRECTION."""
        py_ass_parser2450 = self.tk.call((self._w, 'mark', 'gravity',
            markName, direction))
        Sensor(2450)
        return py_ass_parser2450

    def mark_names(self):
        Sensor(2451)
        """Return all mark names."""
        py_ass_parser2452 = self.tk.splitlist(self.tk.call(self._w, 'mark',
            'names'))
        Sensor(2452)
        return py_ass_parser2452

    def mark_set(self, markName, index):
        Sensor(2453)
        """Set mark MARKNAME before the character at INDEX."""
        self.tk.call(self._w, 'mark', 'set', markName, index)
        Sensor(2454)

    def mark_unset(self, *markNames):
        Sensor(2455)
        """Delete all marks in MARKNAMES."""
        self.tk.call((self._w, 'mark', 'unset') + markNames)
        Sensor(2456)

    def mark_next(self, index):
        Sensor(2457)
        """Return the name of the next mark after INDEX."""
        py_ass_parser2458 = self.tk.call(self._w, 'mark', 'next', index
            ) or None
        Sensor(2458)
        return py_ass_parser2458

    def mark_previous(self, index):
        Sensor(2459)
        """Return the name of the previous mark before INDEX."""
        py_ass_parser2460 = self.tk.call(self._w, 'mark', 'previous', index
            ) or None
        Sensor(2460)
        return py_ass_parser2460

    def scan_mark(self, x, y):
        Sensor(2461)
        """Remember the current X, Y coordinates."""
        self.tk.call(self._w, 'scan', 'mark', x, y)
        Sensor(2462)

    def scan_dragto(self, x, y):
        Sensor(2463)
        """Adjust the view of the text to 10 times the
        difference between X and Y and the coordinates given in
        scan_mark."""
        self.tk.call(self._w, 'scan', 'dragto', x, y)
        Sensor(2464)

    def search(self, pattern, index, stopindex=None, forwards=None,
        backwards=None, exact=None, regexp=None, nocase=None, count=None,
        elide=None):
        Sensor(2465)
        """Search PATTERN beginning from INDEX until STOPINDEX.
        Return the index of the first character of a match or an
        empty string."""
        args = [self._w, 'search']
        if forwards:
            args.append('-forwards')
        if backwards:
            args.append('-backwards')
        if exact:
            args.append('-exact')
        if regexp:
            args.append('-regexp')
        if nocase:
            args.append('-nocase')
        if elide:
            args.append('-elide')
        if count:
            args.append('-count')
            args.append(count)
        if pattern and pattern[0] == '-':
            args.append('--')
        args.append(pattern)
        args.append(index)
        if stopindex:
            args.append(stopindex)
        py_ass_parser2466 = str(self.tk.call(tuple(args)))
        Sensor(2466)
        return py_ass_parser2466

    def see(self, index):
        Sensor(2467)
        """Scroll such that the character at INDEX is visible."""
        self.tk.call(self._w, 'see', index)
        Sensor(2468)

    def tag_add(self, tagName, index1, *args):
        Sensor(2469)
        """Add tag TAGNAME to all characters between INDEX1 and index2 in ARGS.
        Additional pairs of indices may follow in ARGS."""
        self.tk.call((self._w, 'tag', 'add', tagName, index1) + args)
        Sensor(2470)

    def tag_unbind(self, tagName, sequence, funcid=None):
        Sensor(2471)
        """Unbind for all characters with TAGNAME for event SEQUENCE  the
        function identified with FUNCID."""
        self.tk.call(self._w, 'tag', 'bind', tagName, sequence, '')
        if funcid:
            self.deletecommand(funcid)
        Sensor(2472)

    def tag_bind(self, tagName, sequence, func, add=None):
        Sensor(2473)
        """Bind to all characters with TAGNAME at event SEQUENCE a call to function FUNC.

        An additional boolean parameter ADD specifies whether FUNC will be
        called additionally to the other bound function or whether it will
        replace the previous function. See bind for the return value."""
        py_ass_parser2474 = self._bind((self._w, 'tag', 'bind', tagName),
            sequence, func, add)
        Sensor(2474)
        return py_ass_parser2474

    def tag_cget(self, tagName, option):
        Sensor(2475)
        """Return the value of OPTION for tag TAGNAME."""
        if option[:1] != '-':
            option = '-' + option
        if option[-1:] == '_':
            option = option[:-1]
        py_ass_parser2476 = self.tk.call(self._w, 'tag', 'cget', tagName,
            option)
        Sensor(2476)
        return py_ass_parser2476

    def tag_configure(self, tagName, cnf=None, **kw):
        Sensor(2477)
        """Configure a tag TAGNAME."""
        py_ass_parser2478 = self._configure(('tag', 'configure', tagName),
            cnf, kw)
        Sensor(2478)
        return py_ass_parser2478
    tag_config = tag_configure

    def tag_delete(self, *tagNames):
        Sensor(2479)
        """Delete all tags in TAGNAMES."""
        self.tk.call((self._w, 'tag', 'delete') + tagNames)
        Sensor(2480)

    def tag_lower(self, tagName, belowThis=None):
        Sensor(2481)
        """Change the priority of tag TAGNAME such that it is lower
        than the priority of BELOWTHIS."""
        self.tk.call(self._w, 'tag', 'lower', tagName, belowThis)
        Sensor(2482)

    def tag_names(self, index=None):
        Sensor(2483)
        """Return a list of all tag names."""
        py_ass_parser2484 = self.tk.splitlist(self.tk.call(self._w, 'tag',
            'names', index))
        Sensor(2484)
        return py_ass_parser2484

    def tag_nextrange(self, tagName, index1, index2=None):
        Sensor(2485)
        """Return a list of start and end index for the first sequence of
        characters between INDEX1 and INDEX2 which all have tag TAGNAME.
        The text is searched forward from INDEX1."""
        py_ass_parser2486 = self.tk.splitlist(self.tk.call(self._w, 'tag',
            'nextrange', tagName, index1, index2))
        Sensor(2486)
        return py_ass_parser2486

    def tag_prevrange(self, tagName, index1, index2=None):
        Sensor(2487)
        """Return a list of start and end index for the first sequence of
        characters between INDEX1 and INDEX2 which all have tag TAGNAME.
        The text is searched backwards from INDEX1."""
        py_ass_parser2488 = self.tk.splitlist(self.tk.call(self._w, 'tag',
            'prevrange', tagName, index1, index2))
        Sensor(2488)
        return py_ass_parser2488

    def tag_raise(self, tagName, aboveThis=None):
        Sensor(2489)
        """Change the priority of tag TAGNAME such that it is higher
        than the priority of ABOVETHIS."""
        self.tk.call(self._w, 'tag', 'raise', tagName, aboveThis)
        Sensor(2490)

    def tag_ranges(self, tagName):
        Sensor(2491)
        """Return a list of ranges of text which have tag TAGNAME."""
        py_ass_parser2492 = self.tk.splitlist(self.tk.call(self._w, 'tag',
            'ranges', tagName))
        Sensor(2492)
        return py_ass_parser2492

    def tag_remove(self, tagName, index1, index2=None):
        Sensor(2493)
        """Remove tag TAGNAME from all characters between INDEX1 and INDEX2."""
        self.tk.call(self._w, 'tag', 'remove', tagName, index1, index2)
        Sensor(2494)

    def window_cget(self, index, option):
        Sensor(2495)
        """Return the value of OPTION of an embedded window at INDEX."""
        if option[:1] != '-':
            option = '-' + option
        if option[-1:] == '_':
            option = option[:-1]
        py_ass_parser2496 = self.tk.call(self._w, 'window', 'cget', index,
            option)
        Sensor(2496)
        return py_ass_parser2496

    def window_configure(self, index, cnf=None, **kw):
        Sensor(2497)
        """Configure an embedded window at INDEX."""
        py_ass_parser2498 = self._configure(('window', 'configure', index),
            cnf, kw)
        Sensor(2498)
        return py_ass_parser2498
    window_config = window_configure

    def window_create(self, index, cnf={}, **kw):
        Sensor(2499)
        """Create a window at INDEX."""
        self.tk.call((self._w, 'window', 'create', index) + self._options(
            cnf, kw))
        Sensor(2500)

    def window_names(self):
        Sensor(2501)
        """Return all names of embedded windows in this widget."""
        py_ass_parser2502 = self.tk.splitlist(self.tk.call(self._w,
            'window', 'names'))
        Sensor(2502)
        return py_ass_parser2502

    def yview_pickplace(self, *what):
        Sensor(2503)
        """Obsolete function, use see."""
        self.tk.call((self._w, 'yview', '-pickplace') + what)
        Sensor(2504)


class _setit:
    """Internal class. It wraps the command in the widget OptionMenu."""

    def __init__(self, var, value, callback=None):
        Sensor(2505)
        self.__value = value
        self.__var = var
        self.__callback = callback
        Sensor(2506)

    def __call__(self, *args):
        Sensor(2507)
        self.__var.set(self.__value)
        if self.__callback:
            self.__callback(self.__value, *args)
        Sensor(2508)


class OptionMenu(Menubutton):
    """OptionMenu which allows the user to select a value from a menu."""

    def __init__(self, master, variable, value, *values, **kwargs):
        Sensor(2509)
        """Construct an optionmenu widget with the parent MASTER, with
        the resource textvariable set to VARIABLE, the initially selected
        value VALUE, the other menu values VALUES and an additional
        keyword argument command."""
        kw = {'borderwidth': 2, 'textvariable': variable, 'indicatoron': 1,
            'relief': RAISED, 'anchor': 'c', 'highlightthickness': 2}
        Widget.__init__(self, master, 'menubutton', kw)
        self.widgetName = 'tk_optionMenu'
        menu = self.__menu = Menu(self, name='menu', tearoff=0)
        self.menuname = menu._w
        callback = kwargs.get('command')
        if 'command' in kwargs:
            del kwargs['command']
        if kwargs:
            raise TclError, 'unknown option -' + kwargs.keys()[0]
        menu.add_command(label=value, command=_setit(variable, value, callback)
            )
        for v in values:
            menu.add_command(label=v, command=_setit(variable, v, callback))
        self['menu'] = menu
        Sensor(2510)

    def __getitem__(self, name):
        Sensor(2511)
        if name == 'menu':
            py_ass_parser2512 = self.__menu
            Sensor(2512)
            return py_ass_parser2512
        py_ass_parser2513 = Widget.__getitem__(self, name)
        Sensor(2513)
        return py_ass_parser2513

    def destroy(self):
        Sensor(2514)
        """Destroy this widget and the associated menu."""
        Menubutton.destroy(self)
        self.__menu = None
        Sensor(2515)


class Image:
    """Base class for images."""
    _last_id = 0

    def __init__(self, imgtype, name=None, cnf={}, master=None, **kw):
        Sensor(2516)
        self.name = None
        if not master:
            master = _default_root
            if not master:
                raise RuntimeError, 'Too early to create image'
        self.tk = master.tk
        if not name:
            Image._last_id += 1
            name = 'pyimage%r' % (Image._last_id,)
            if name[0] == '-':
                name = '_' + name[1:]
        if kw and cnf:
            cnf = _cnfmerge((cnf, kw))
        elif kw:
            cnf = kw
        options = ()
        for k, v in cnf.items():
            if hasattr(v, '__call__'):
                v = self._register(v)
            options = options + ('-' + k, v)
        self.tk.call(('image', 'create', imgtype, name) + options)
        self.name = name
        Sensor(2517)

    def __str__(self):
        py_ass_parser2518 = self.name
        Sensor(2518)
        return py_ass_parser2518

    def __del__(self):
        Sensor(2519)
        if self.name:
            try:
                self.tk.call('image', 'delete', self.name)
            except TclError:
                pass
        Sensor(2520)

    def __setitem__(self, key, value):
        Sensor(2521)
        self.tk.call(self.name, 'configure', '-' + key, value)
        Sensor(2522)

    def __getitem__(self, key):
        py_ass_parser2523 = self.tk.call(self.name, 'configure', '-' + key)
        Sensor(2523)
        return py_ass_parser2523

    def configure(self, **kw):
        Sensor(2524)
        """Configure the image."""
        res = ()
        for k, v in _cnfmerge(kw).items():
            if v is not None:
                if k[-1] == '_':
                    k = k[:-1]
                if hasattr(v, '__call__'):
                    v = self._register(v)
                res = res + ('-' + k, v)
        self.tk.call((self.name, 'config') + res)
        Sensor(2525)
    config = configure

    def height(self):
        Sensor(2526)
        """Return the height of the image."""
        py_ass_parser2527 = getint(self.tk.call('image', 'height', self.name))
        Sensor(2527)
        return py_ass_parser2527

    def type(self):
        Sensor(2528)
        """Return the type of the imgage, e.g. "photo" or "bitmap"."""
        py_ass_parser2529 = self.tk.call('image', 'type', self.name)
        Sensor(2529)
        return py_ass_parser2529

    def width(self):
        Sensor(2530)
        """Return the width of the image."""
        py_ass_parser2531 = getint(self.tk.call('image', 'width', self.name))
        Sensor(2531)
        return py_ass_parser2531


class PhotoImage(Image):
    """Widget which can display colored images in GIF, PPM/PGM format."""

    def __init__(self, name=None, cnf={}, master=None, **kw):
        Sensor(2532)
        """Create an image with NAME.

        Valid resource names: data, format, file, gamma, height, palette,
        width."""
        Image.__init__(self, 'photo', name, cnf, master, **kw)
        Sensor(2533)

    def blank(self):
        Sensor(2534)
        """Display a transparent image."""
        self.tk.call(self.name, 'blank')
        Sensor(2535)

    def cget(self, option):
        Sensor(2536)
        """Return the value of OPTION."""
        py_ass_parser2537 = self.tk.call(self.name, 'cget', '-' + option)
        Sensor(2537)
        return py_ass_parser2537

    def __getitem__(self, key):
        py_ass_parser2538 = self.tk.call(self.name, 'cget', '-' + key)
        Sensor(2538)
        return py_ass_parser2538

    def copy(self):
        Sensor(2539)
        """Return a new PhotoImage with the same image as this widget."""
        destImage = PhotoImage()
        self.tk.call(destImage, 'copy', self.name)
        py_ass_parser2540 = destImage
        Sensor(2540)
        return py_ass_parser2540

    def zoom(self, x, y=''):
        Sensor(2541)
        """Return a new PhotoImage with the same image as this widget
        but zoom it with X and Y."""
        destImage = PhotoImage()
        if y == '':
            y = x
        self.tk.call(destImage, 'copy', self.name, '-zoom', x, y)
        py_ass_parser2542 = destImage
        Sensor(2542)
        return py_ass_parser2542

    def subsample(self, x, y=''):
        Sensor(2543)
        """Return a new PhotoImage based on the same image as this widget
        but use only every Xth or Yth pixel."""
        destImage = PhotoImage()
        if y == '':
            y = x
        self.tk.call(destImage, 'copy', self.name, '-subsample', x, y)
        py_ass_parser2544 = destImage
        Sensor(2544)
        return py_ass_parser2544

    def get(self, x, y):
        Sensor(2545)
        """Return the color (red, green, blue) of the pixel at X,Y."""
        py_ass_parser2546 = self.tk.call(self.name, 'get', x, y)
        Sensor(2546)
        return py_ass_parser2546

    def put(self, data, to=None):
        Sensor(2547)
        """Put row formatted colors to image starting from
        position TO, e.g. image.put("{red green} {blue yellow}", to=(4,6))"""
        args = self.name, 'put', data
        if to:
            if to[0] == '-to':
                to = to[1:]
            args = args + ('-to',) + tuple(to)
        self.tk.call(args)
        Sensor(2548)

    def write(self, filename, format=None, from_coords=None):
        Sensor(2549)
        """Write image to file FILENAME in FORMAT starting from
        position FROM_COORDS."""
        args = self.name, 'write', filename
        if format:
            args = args + ('-format', format)
        if from_coords:
            args = args + ('-from',) + tuple(from_coords)
        self.tk.call(args)
        Sensor(2550)


class BitmapImage(Image):
    """Widget which can display a bitmap."""

    def __init__(self, name=None, cnf={}, master=None, **kw):
        Sensor(2551)
        """Create a bitmap with NAME.

        Valid resource names: background, data, file, foreground, maskdata, maskfile."""
        Image.__init__(self, 'bitmap', name, cnf, master, **kw)
        Sensor(2552)


def image_names():
    py_ass_parser2553 = _default_root.tk.call('image', 'names')
    Sensor(2553)
    return py_ass_parser2553


def image_types():
    py_ass_parser2554 = _default_root.tk.call('image', 'types')
    Sensor(2554)
    return py_ass_parser2554


class Spinbox(Widget, XView):
    """spinbox widget."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2555)
        """Construct a spinbox widget with the parent MASTER.

        STANDARD OPTIONS

            activebackground, background, borderwidth,
            cursor, exportselection, font, foreground,
            highlightbackground, highlightcolor,
            highlightthickness, insertbackground,
            insertborderwidth, insertofftime,
            insertontime, insertwidth, justify, relief,
            repeatdelay, repeatinterval,
            selectbackground, selectborderwidth
            selectforeground, takefocus, textvariable
            xscrollcommand.

        WIDGET-SPECIFIC OPTIONS

            buttonbackground, buttoncursor,
            buttondownrelief, buttonuprelief,
            command, disabledbackground,
            disabledforeground, format, from,
            invalidcommand, increment,
            readonlybackground, state, to,
            validate, validatecommand values,
            width, wrap,
        """
        Widget.__init__(self, master, 'spinbox', cnf, kw)
        Sensor(2556)

    def bbox(self, index):
        Sensor(2557)
        """Return a tuple of X1,Y1,X2,Y2 coordinates for a
        rectangle which encloses the character given by index.

        The first two elements of the list give the x and y
        coordinates of the upper-left corner of the screen
        area covered by the character (in pixels relative
        to the widget) and the last two elements give the
        width and height of the character, in pixels. The
        bounding box may refer to a region outside the
        visible area of the window.
        """
        py_ass_parser2558 = self.tk.call(self._w, 'bbox', index)
        Sensor(2558)
        return py_ass_parser2558

    def delete(self, first, last=None):
        Sensor(2559)
        """Delete one or more elements of the spinbox.

        First is the index of the first character to delete,
        and last is the index of the character just after
        the last one to delete. If last isn't specified it
        defaults to first+1, i.e. a single character is
        deleted.  This command returns an empty string.
        """
        py_ass_parser2560 = self.tk.call(self._w, 'delete', first, last)
        Sensor(2560)
        return py_ass_parser2560

    def get(self):
        Sensor(2561)
        """Returns the spinbox's string"""
        py_ass_parser2562 = self.tk.call(self._w, 'get')
        Sensor(2562)
        return py_ass_parser2562

    def icursor(self, index):
        Sensor(2563)
        """Alter the position of the insertion cursor.

        The insertion cursor will be displayed just before
        the character given by index. Returns an empty string
        """
        py_ass_parser2564 = self.tk.call(self._w, 'icursor', index)
        Sensor(2564)
        return py_ass_parser2564

    def identify(self, x, y):
        Sensor(2565)
        """Returns the name of the widget at position x, y

        Return value is one of: none, buttondown, buttonup, entry
        """
        py_ass_parser2566 = self.tk.call(self._w, 'identify', x, y)
        Sensor(2566)
        return py_ass_parser2566

    def index(self, index):
        Sensor(2567)
        """Returns the numerical index corresponding to index
        """
        py_ass_parser2568 = self.tk.call(self._w, 'index', index)
        Sensor(2568)
        return py_ass_parser2568

    def insert(self, index, s):
        Sensor(2569)
        """Insert string s at index

         Returns an empty string.
        """
        py_ass_parser2570 = self.tk.call(self._w, 'insert', index, s)
        Sensor(2570)
        return py_ass_parser2570

    def invoke(self, element):
        Sensor(2571)
        """Causes the specified element to be invoked

        The element could be buttondown or buttonup
        triggering the action associated with it.
        """
        py_ass_parser2572 = self.tk.call(self._w, 'invoke', element)
        Sensor(2572)
        return py_ass_parser2572

    def scan(self, *args):
        Sensor(2573)
        """Internal function."""
        py_ass_parser2574 = self._getints(self.tk.call((self._w, 'scan') +
            args)) or ()
        Sensor(2574)
        return py_ass_parser2574

    def scan_mark(self, x):
        Sensor(2575)
        """Records x and the current view in the spinbox window;

        used in conjunction with later scan dragto commands.
        Typically this command is associated with a mouse button
        press in the widget. It returns an empty string.
        """
        py_ass_parser2576 = self.scan('mark', x)
        Sensor(2576)
        return py_ass_parser2576

    def scan_dragto(self, x):
        Sensor(2577)
        """Compute the difference between the given x argument
        and the x argument to the last scan mark command

        It then adjusts the view left or right by 10 times the
        difference in x-coordinates. This command is typically
        associated with mouse motion events in the widget, to
        produce the effect of dragging the spinbox at high speed
        through the window. The return value is an empty string.
        """
        py_ass_parser2578 = self.scan('dragto', x)
        Sensor(2578)
        return py_ass_parser2578

    def selection(self, *args):
        Sensor(2579)
        """Internal function."""
        py_ass_parser2580 = self._getints(self.tk.call((self._w,
            'selection') + args)) or ()
        Sensor(2580)
        return py_ass_parser2580

    def selection_adjust(self, index):
        Sensor(2581)
        """Locate the end of the selection nearest to the character
        given by index,

        Then adjust that end of the selection to be at index
        (i.e including but not going beyond index). The other
        end of the selection is made the anchor point for future
        select to commands. If the selection isn't currently in
        the spinbox, then a new selection is created to include
        the characters between index and the most recent selection
        anchor point, inclusive. Returns an empty string.
        """
        py_ass_parser2582 = self.selection('adjust', index)
        Sensor(2582)
        return py_ass_parser2582

    def selection_clear(self):
        Sensor(2583)
        """Clear the selection

        If the selection isn't in this widget then the
        command has no effect. Returns an empty string.
        """
        py_ass_parser2584 = self.selection('clear')
        Sensor(2584)
        return py_ass_parser2584

    def selection_element(self, element=None):
        Sensor(2585)
        """Sets or gets the currently selected element.

        If a spinbutton element is specified, it will be
        displayed depressed
        """
        py_ass_parser2586 = self.selection('element', element)
        Sensor(2586)
        return py_ass_parser2586


class LabelFrame(Widget):
    """labelframe widget."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2587)
        """Construct a labelframe widget with the parent MASTER.

        STANDARD OPTIONS

            borderwidth, cursor, font, foreground,
            highlightbackground, highlightcolor,
            highlightthickness, padx, pady, relief,
            takefocus, text

        WIDGET-SPECIFIC OPTIONS

            background, class, colormap, container,
            height, labelanchor, labelwidget,
            visual, width
        """
        Widget.__init__(self, master, 'labelframe', cnf, kw)
        Sensor(2588)


class PanedWindow(Widget):
    """panedwindow widget."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2589)
        """Construct a panedwindow widget with the parent MASTER.

        STANDARD OPTIONS

            background, borderwidth, cursor, height,
            orient, relief, width

        WIDGET-SPECIFIC OPTIONS

            handlepad, handlesize, opaqueresize,
            sashcursor, sashpad, sashrelief,
            sashwidth, showhandle,
        """
        Widget.__init__(self, master, 'panedwindow', cnf, kw)
        Sensor(2590)

    def add(self, child, **kw):
        Sensor(2591)
        """Add a child widget to the panedwindow in a new pane.

        The child argument is the name of the child widget
        followed by pairs of arguments that specify how to
        manage the windows. The possible options and values
        are the ones accepted by the paneconfigure method.
        """
        self.tk.call((self._w, 'add', child) + self._options(kw))
        Sensor(2592)

    def remove(self, child):
        Sensor(2593)
        """Remove the pane containing child from the panedwindow

        All geometry management options for child will be forgotten.
        """
        self.tk.call(self._w, 'forget', child)
        Sensor(2594)
    forget = remove

    def identify(self, x, y):
        Sensor(2595)
        """Identify the panedwindow component at point x, y

        If the point is over a sash or a sash handle, the result
        is a two element list containing the index of the sash or
        handle, and a word indicating whether it is over a sash
        or a handle, such as {0 sash} or {2 handle}. If the point
        is over any other part of the panedwindow, the result is
        an empty list.
        """
        py_ass_parser2596 = self.tk.call(self._w, 'identify', x, y)
        Sensor(2596)
        return py_ass_parser2596

    def proxy(self, *args):
        Sensor(2597)
        """Internal function."""
        py_ass_parser2598 = self._getints(self.tk.call((self._w, 'proxy') +
            args)) or ()
        Sensor(2598)
        return py_ass_parser2598

    def proxy_coord(self):
        Sensor(2599)
        """Return the x and y pair of the most recent proxy location
        """
        py_ass_parser2600 = self.proxy('coord')
        Sensor(2600)
        return py_ass_parser2600

    def proxy_forget(self):
        Sensor(2601)
        """Remove the proxy from the display.
        """
        py_ass_parser2602 = self.proxy('forget')
        Sensor(2602)
        return py_ass_parser2602

    def proxy_place(self, x, y):
        Sensor(2603)
        """Place the proxy at the given x and y coordinates.
        """
        py_ass_parser2604 = self.proxy('place', x, y)
        Sensor(2604)
        return py_ass_parser2604

    def sash(self, *args):
        Sensor(2605)
        """Internal function."""
        py_ass_parser2606 = self._getints(self.tk.call((self._w, 'sash') +
            args)) or ()
        Sensor(2606)
        return py_ass_parser2606

    def sash_coord(self, index):
        Sensor(2607)
        """Return the current x and y pair for the sash given by index.

        Index must be an integer between 0 and 1 less than the
        number of panes in the panedwindow. The coordinates given are
        those of the top left corner of the region containing the sash.
        pathName sash dragto index x y This command computes the
        difference between the given coordinates and the coordinates
        given to the last sash coord command for the given sash. It then
        moves that sash the computed difference. The return value is the
        empty string.
        """
        py_ass_parser2608 = self.sash('coord', index)
        Sensor(2608)
        return py_ass_parser2608

    def sash_mark(self, index):
        Sensor(2609)
        """Records x and y for the sash given by index;

        Used in conjunction with later dragto commands to move the sash.
        """
        py_ass_parser2610 = self.sash('mark', index)
        Sensor(2610)
        return py_ass_parser2610

    def sash_place(self, index, x, y):
        Sensor(2611)
        """Place the sash given by index at the given coordinates
        """
        py_ass_parser2612 = self.sash('place', index, x, y)
        Sensor(2612)
        return py_ass_parser2612

    def panecget(self, child, option):
        Sensor(2613)
        """Query a management option for window.

        Option may be any value allowed by the paneconfigure subcommand
        """
        py_ass_parser2614 = self.tk.call((self._w, 'panecget') + (child, 
            '-' + option))
        Sensor(2614)
        return py_ass_parser2614

    def paneconfigure(self, tagOrId, cnf=None, **kw):
        Sensor(2615)
        """Query or modify the management options for window.

        If no option is specified, returns a list describing all
        of the available options for pathName.  If option is
        specified with no value, then the command returns a list
        describing the one named option (this list will be identical
        to the corresponding sublist of the value returned if no
        option is specified). If one or more option-value pairs are
        specified, then the command modifies the given widget
        option(s) to have the given value(s); in this case the
        command returns an empty string. The following options
        are supported:

        after window
            Insert the window after the window specified. window
            should be the name of a window already managed by pathName.
        before window
            Insert the window before the window specified. window
            should be the name of a window already managed by pathName.
        height size
            Specify a height for the window. The height will be the
            outer dimension of the window including its border, if
            any. If size is an empty string, or if -height is not
            specified, then the height requested internally by the
            window will be used initially; the height may later be
            adjusted by the movement of sashes in the panedwindow.
            Size may be any value accepted by Tk_GetPixels.
        minsize n
            Specifies that the size of the window cannot be made
            less than n. This constraint only affects the size of
            the widget in the paned dimension -- the x dimension
            for horizontal panedwindows, the y dimension for
            vertical panedwindows. May be any value accepted by
            Tk_GetPixels.
        padx n
            Specifies a non-negative value indicating how much
            extra space to leave on each side of the window in
            the X-direction. The value may have any of the forms
            accepted by Tk_GetPixels.
        pady n
            Specifies a non-negative value indicating how much
            extra space to leave on each side of the window in
            the Y-direction. The value may have any of the forms
            accepted by Tk_GetPixels.
        sticky style
            If a window's pane is larger than the requested
            dimensions of the window, this option may be used
            to position (or stretch) the window within its pane.
            Style is a string that contains zero or more of the
            characters n, s, e or w. The string can optionally
            contains spaces or commas, but they are ignored. Each
            letter refers to a side (north, south, east, or west)
            that the window will "stick" to. If both n and s
            (or e and w) are specified, the window will be
            stretched to fill the entire height (or width) of
            its cavity.
        width size
            Specify a width for the window. The width will be
            the outer dimension of the window including its
            border, if any. If size is an empty string, or
            if -width is not specified, then the width requested
            internally by the window will be used initially; the
            width may later be adjusted by the movement of sashes
            in the panedwindow. Size may be any value accepted by
            Tk_GetPixels.

        """
        if cnf is None and not kw:
            cnf = {}
            for x in self.tk.split(self.tk.call(self._w, 'paneconfigure',
                tagOrId)):
                cnf[x[0][1:]] = (x[0][1:],) + x[1:]
            py_ass_parser2616 = cnf
            Sensor(2616)
            return py_ass_parser2616
        if type(cnf) == StringType and not kw:
            x = self.tk.split(self.tk.call(self._w, 'paneconfigure',
                tagOrId, '-' + cnf))
            py_ass_parser2617 = (x[0][1:],) + x[1:]
            Sensor(2617)
            return py_ass_parser2617
        self.tk.call((self._w, 'paneconfigure', tagOrId) + self._options(
            cnf, kw))
        Sensor(2618)
    paneconfig = paneconfigure

    def panes(self):
        Sensor(2619)
        """Returns an ordered list of the child panes."""
        py_ass_parser2620 = self.tk.call(self._w, 'panes')
        Sensor(2620)
        return py_ass_parser2620


class Studbutton(Button):

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2621)
        Widget.__init__(self, master, 'studbutton', cnf, kw)
        self.bind('<Any-Enter>', self.tkButtonEnter)
        self.bind('<Any-Leave>', self.tkButtonLeave)
        self.bind('<1>', self.tkButtonDown)
        self.bind('<ButtonRelease-1>', self.tkButtonUp)
        Sensor(2622)


class Tributton(Button):

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(2623)
        Widget.__init__(self, master, 'tributton', cnf, kw)
        self.bind('<Any-Enter>', self.tkButtonEnter)
        self.bind('<Any-Leave>', self.tkButtonLeave)
        self.bind('<1>', self.tkButtonDown)
        self.bind('<ButtonRelease-1>', self.tkButtonUp)
        self['fg'] = self['bg']
        self['activebackground'] = self['bg']
        Sensor(2624)


def _test():
    Sensor(2625)
    root = Tk()
    text = 'This is Tcl/Tk version %s' % TclVersion
    if TclVersion >= 8.1:
        try:
            text = text + unicode('\nThis should be a cedilla: \xe7',
                'iso-8859-1')
        except NameError:
            pass
    label = Label(root, text=text)
    label.pack()
    test = Button(root, text='Click me!', command=lambda root=root: root.
        test.configure(text='[%s]' % root.test['text']))
    test.pack()
    root.test = test
    quit = Button(root, text='QUIT', command=root.destroy)
    quit.pack()
    root.iconify()
    root.update()
    root.deiconify()
    root.mainloop()
    Sensor(2626)


Sensor(2627)
if __name__ == '__main__':
    _test()
Sensor(2629)
