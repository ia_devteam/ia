from sensors import Sensor


Sensor(1559)
import __builtin__
import gc
import sys
import types
import unittest
import weakref
from copy import deepcopy
from test import test_support


class OperatorsTest(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        Sensor(821)
        unittest.TestCase.__init__(self, *args, **kwargs)
        self.binops = {'add': '+', 'sub': '-', 'mul': '*', 'div': '/',
            'divmod': 'divmod', 'pow': '**', 'lshift': '<<', 'rshift': '>>',
            'and': '&', 'xor': '^', 'or': '|', 'cmp': 'cmp', 'lt': '<',
            'le': '<=', 'eq': '==', 'ne': '!=', 'gt': '>', 'ge': '>='}
        for name, expr in self.binops.items():
            if expr.islower():
                expr = expr + '(a, b)'
            else:
                expr = 'a %s b' % expr
            self.binops[name] = expr
        self.unops = {'pos': '+', 'neg': '-', 'abs': 'abs', 'invert': '~',
            'int': 'int', 'long': 'long', 'float': 'float', 'oct': 'oct',
            'hex': 'hex'}
        for name, expr in self.unops.items():
            if expr.islower():
                expr = expr + '(a)'
            else:
                expr = '%s a' % expr
            self.unops[name] = expr
        Sensor(822)

    def unop_test(self, a, res, expr='len(a)', meth='__len__'):
        Sensor(823)
        d = {'a': a}
        self.assertEqual(eval(expr, d), res)
        t = type(a)
        m = getattr(t, meth)
        while meth not in t.__dict__:
            t = t.__bases__[0]
        self.assertEqual(getattr(m, 'im_func', m), t.__dict__[meth])
        self.assertEqual(m(a), res)
        bm = getattr(a, meth)
        self.assertEqual(bm(), res)
        Sensor(824)

    def binop_test(self, a, b, res, expr='a+b', meth='__add__'):
        Sensor(825)
        d = {'a': a, 'b': b}
        if meth == '__div__' and 1 / 2 == 0.5:
            meth = '__truediv__'
        if meth == '__divmod__':
            pass
        self.assertEqual(eval(expr, d), res)
        t = type(a)
        m = getattr(t, meth)
        while meth not in t.__dict__:
            t = t.__bases__[0]
        self.assertEqual(getattr(m, 'im_func', m), t.__dict__[meth])
        self.assertEqual(m(a, b), res)
        bm = getattr(a, meth)
        self.assertEqual(bm(b), res)
        Sensor(826)

    def ternop_test(self, a, b, c, res, expr='a[b:c]', meth='__getslice__'):
        Sensor(827)
        d = {'a': a, 'b': b, 'c': c}
        self.assertEqual(eval(expr, d), res)
        t = type(a)
        m = getattr(t, meth)
        while meth not in t.__dict__:
            t = t.__bases__[0]
        self.assertEqual(getattr(m, 'im_func', m), t.__dict__[meth])
        self.assertEqual(m(a, b, c), res)
        bm = getattr(a, meth)
        self.assertEqual(bm(b, c), res)
        Sensor(828)

    def setop_test(self, a, b, res, stmt='a+=b', meth='__iadd__'):
        Sensor(829)
        d = {'a': deepcopy(a), 'b': b}
        exec stmt in d
        self.assertEqual(d['a'], res)
        t = type(a)
        m = getattr(t, meth)
        while meth not in t.__dict__:
            t = t.__bases__[0]
        self.assertEqual(getattr(m, 'im_func', m), t.__dict__[meth])
        d['a'] = deepcopy(a)
        m(d['a'], b)
        self.assertEqual(d['a'], res)
        d['a'] = deepcopy(a)
        bm = getattr(d['a'], meth)
        bm(b)
        self.assertEqual(d['a'], res)
        Sensor(830)

    def set2op_test(self, a, b, c, res, stmt='a[b]=c', meth='__setitem__'):
        Sensor(831)
        d = {'a': deepcopy(a), 'b': b, 'c': c}
        exec stmt in d
        self.assertEqual(d['a'], res)
        t = type(a)
        m = getattr(t, meth)
        while meth not in t.__dict__:
            t = t.__bases__[0]
        self.assertEqual(getattr(m, 'im_func', m), t.__dict__[meth])
        d['a'] = deepcopy(a)
        m(d['a'], b, c)
        self.assertEqual(d['a'], res)
        d['a'] = deepcopy(a)
        bm = getattr(d['a'], meth)
        bm(b, c)
        self.assertEqual(d['a'], res)
        Sensor(832)

    def set3op_test(self, a, b, c, d, res, stmt='a[b:c]=d', meth='__setslice__'
        ):
        Sensor(833)
        dictionary = {'a': deepcopy(a), 'b': b, 'c': c, 'd': d}
        exec stmt in dictionary
        self.assertEqual(dictionary['a'], res)
        t = type(a)
        while meth not in t.__dict__:
            t = t.__bases__[0]
        m = getattr(t, meth)
        self.assertEqual(getattr(m, 'im_func', m), t.__dict__[meth])
        dictionary['a'] = deepcopy(a)
        m(dictionary['a'], b, c, d)
        self.assertEqual(dictionary['a'], res)
        dictionary['a'] = deepcopy(a)
        bm = getattr(dictionary['a'], meth)
        bm(b, c, d)
        self.assertEqual(dictionary['a'], res)
        Sensor(834)

    def test_lists(self):
        Sensor(835)
        self.binop_test([1], [2], [1, 2], 'a+b', '__add__')
        self.binop_test([1, 2, 3], 2, 1, 'b in a', '__contains__')
        self.binop_test([1, 2, 3], 4, 0, 'b in a', '__contains__')
        self.binop_test([1, 2, 3], 1, 2, 'a[b]', '__getitem__')
        self.ternop_test([1, 2, 3], 0, 2, [1, 2], 'a[b:c]', '__getslice__')
        self.setop_test([1], [2], [1, 2], 'a+=b', '__iadd__')
        self.setop_test([1, 2], 3, [1, 2, 1, 2, 1, 2], 'a*=b', '__imul__')
        self.unop_test([1, 2, 3], 3, 'len(a)', '__len__')
        self.binop_test([1, 2], 3, [1, 2, 1, 2, 1, 2], 'a*b', '__mul__')
        self.binop_test([1, 2], 3, [1, 2, 1, 2, 1, 2], 'b*a', '__rmul__')
        self.set2op_test([1, 2], 1, 3, [1, 3], 'a[b]=c', '__setitem__')
        self.set3op_test([1, 2, 3, 4], 1, 3, [5, 6], [1, 5, 6, 4],
            'a[b:c]=d', '__setslice__')
        Sensor(836)

    def test_dicts(self):
        Sensor(837)
        if hasattr(dict, '__cmp__'):
            self.binop_test({(1): 2}, {(2): 1}, -1, 'cmp(a,b)', '__cmp__')
        else:
            self.binop_test({(1): 2}, {(2): 1}, True, 'a < b', '__lt__')
        self.binop_test({(1): 2, (3): 4}, 1, 1, 'b in a', '__contains__')
        self.binop_test({(1): 2, (3): 4}, 2, 0, 'b in a', '__contains__')
        self.binop_test({(1): 2, (3): 4}, 1, 2, 'a[b]', '__getitem__')
        d = {(1): 2, (3): 4}
        l1 = []
        for i in d.keys():
            l1.append(i)
        l = []
        for i in iter(d):
            l.append(i)
        self.assertEqual(l, l1)
        l = []
        for i in d.__iter__():
            l.append(i)
        self.assertEqual(l, l1)
        l = []
        for i in dict.__iter__(d):
            l.append(i)
        self.assertEqual(l, l1)
        d = {(1): 2, (3): 4}
        self.unop_test(d, 2, 'len(a)', '__len__')
        self.assertEqual(eval(repr(d), {}), d)
        self.assertEqual(eval(d.__repr__(), {}), d)
        self.set2op_test({(1): 2, (3): 4}, 2, 3, {(1): 2, (2): 3, (3): 4},
            'a[b]=c', '__setitem__')
        Sensor(838)

    def number_operators(self, a, b, skip=[]):
        Sensor(839)
        dict = {'a': a, 'b': b}
        for name, expr in self.binops.items():
            if name not in skip:
                name = '__%s__' % name
                if hasattr(a, name):
                    res = eval(expr, dict)
                    self.binop_test(a, b, res, expr, name)
        for name, expr in self.unops.items():
            if name not in skip:
                name = '__%s__' % name
                if hasattr(a, name):
                    res = eval(expr, dict)
                    self.unop_test(a, res, expr, name)
        Sensor(840)

    def test_ints(self):
        Sensor(841)
        self.number_operators(100, 3)
        self.assertEqual((1).__nonzero__(), 1)
        self.assertEqual((0).__nonzero__(), 0)


        class C(int):

            def __add__(self, other):
                py_ass_parser842 = NotImplemented
                Sensor(842)
                return py_ass_parser842
        self.assertEqual(C(5L), 5)
        try:
            C() + ''
        except TypeError:
            pass
        else:
            self.fail('NotImplemented should have caused TypeError')
        try:
            C(sys.maxint + 1)
        except OverflowError:
            pass
        else:
            self.fail('should have raised OverflowError')
        Sensor(843)

    def test_longs(self):
        Sensor(844)
        self.number_operators(100L, 3L)
        Sensor(845)

    def test_floats(self):
        Sensor(846)
        self.number_operators(100.0, 3.0)
        Sensor(847)

    def test_complexes(self):
        Sensor(848)
        self.number_operators(100j, 3j, skip=['lt', 'le', 'gt', 'ge', 'int',
            'long', 'float'])


        class Number(complex):
            __slots__ = ['prec']

            def __new__(cls, *args, **kwds):
                Sensor(849)
                result = complex.__new__(cls, *args)
                result.prec = kwds.get('prec', 12)
                py_ass_parser850 = result
                Sensor(850)
                return py_ass_parser850

            def __repr__(self):
                Sensor(851)
                prec = self.prec
                if self.imag == 0.0:
                    py_ass_parser852 = '%.*g' % (prec, self.real)
                    Sensor(852)
                    return py_ass_parser852
                if self.real == 0.0:
                    py_ass_parser853 = '%.*gj' % (prec, self.imag)
                    Sensor(853)
                    return py_ass_parser853
                py_ass_parser854 = '(%.*g+%.*gj)' % (prec, self.real, prec,
                    self.imag)
                Sensor(854)
                return py_ass_parser854
            __str__ = __repr__
        a = Number(3.14, prec=6)
        self.assertEqual(repr(a), '3.14')
        self.assertEqual(a.prec, 6)
        a = Number(a, prec=2)
        self.assertEqual(repr(a), '3.1')
        self.assertEqual(a.prec, 2)
        a = Number(234.5)
        self.assertEqual(repr(a), '234.5')
        self.assertEqual(a.prec, 12)
        Sensor(855)

    @test_support.impl_detail("the module 'xxsubtype' is internal")
    def test_spam_lists(self):
        import copy, xxsubtype as spam

        def spamlist(l, memo=None):
            import xxsubtype as spam
            py_ass_parser856 = spam.spamlist(l)
            Sensor(856)
            return py_ass_parser856
        copy._deepcopy_dispatch[spam.spamlist] = spamlist
        self.binop_test(spamlist([1]), spamlist([2]), spamlist([1, 2]),
            'a+b', '__add__')
        self.binop_test(spamlist([1, 2, 3]), 2, 1, 'b in a', '__contains__')
        self.binop_test(spamlist([1, 2, 3]), 4, 0, 'b in a', '__contains__')
        self.binop_test(spamlist([1, 2, 3]), 1, 2, 'a[b]', '__getitem__')
        self.ternop_test(spamlist([1, 2, 3]), 0, 2, spamlist([1, 2]),
            'a[b:c]', '__getslice__')
        self.setop_test(spamlist([1]), spamlist([2]), spamlist([1, 2]),
            'a+=b', '__iadd__')
        self.setop_test(spamlist([1, 2]), 3, spamlist([1, 2, 1, 2, 1, 2]),
            'a*=b', '__imul__')
        self.unop_test(spamlist([1, 2, 3]), 3, 'len(a)', '__len__')
        self.binop_test(spamlist([1, 2]), 3, spamlist([1, 2, 1, 2, 1, 2]),
            'a*b', '__mul__')
        self.binop_test(spamlist([1, 2]), 3, spamlist([1, 2, 1, 2, 1, 2]),
            'b*a', '__rmul__')
        self.set2op_test(spamlist([1, 2]), 1, 3, spamlist([1, 3]), 'a[b]=c',
            '__setitem__')
        self.set3op_test(spamlist([1, 2, 3, 4]), 1, 3, spamlist([5, 6]),
            spamlist([1, 5, 6, 4]), 'a[b:c]=d', '__setslice__')


        class C(spam.spamlist):

            def foo(self):
                py_ass_parser857 = 1
                Sensor(857)
                return py_ass_parser857
        a = C()
        self.assertEqual(a, [])
        self.assertEqual(a.foo(), 1)
        a.append(100)
        self.assertEqual(a, [100])
        self.assertEqual(a.getstate(), 0)
        a.setstate(42)
        self.assertEqual(a.getstate(), 42)
        Sensor(858)

    @test_support.impl_detail("the module 'xxsubtype' is internal")
    def test_spam_dicts(self):
        import copy, xxsubtype as spam

        def spamdict(d, memo=None):
            import xxsubtype as spam
            Sensor(859)
            sd = spam.spamdict()
            for k, v in d.items():
                sd[k] = v
            py_ass_parser860 = sd
            Sensor(860)
            return py_ass_parser860
        copy._deepcopy_dispatch[spam.spamdict] = spamdict
        self.binop_test(spamdict({(1): 2}), spamdict({(2): 1}), -1,
            'cmp(a,b)', '__cmp__')
        self.binop_test(spamdict({(1): 2, (3): 4}), 1, 1, 'b in a',
            '__contains__')
        self.binop_test(spamdict({(1): 2, (3): 4}), 2, 0, 'b in a',
            '__contains__')
        self.binop_test(spamdict({(1): 2, (3): 4}), 1, 2, 'a[b]', '__getitem__'
            )
        d = spamdict({(1): 2, (3): 4})
        l1 = []
        for i in d.keys():
            l1.append(i)
        l = []
        for i in iter(d):
            l.append(i)
        self.assertEqual(l, l1)
        l = []
        for i in d.__iter__():
            l.append(i)
        self.assertEqual(l, l1)
        l = []
        for i in type(spamdict({})).__iter__(d):
            l.append(i)
        self.assertEqual(l, l1)
        straightd = {(1): 2, (3): 4}
        spamd = spamdict(straightd)
        self.unop_test(spamd, 2, 'len(a)', '__len__')
        self.unop_test(spamd, repr(straightd), 'repr(a)', '__repr__')
        self.set2op_test(spamdict({(1): 2, (3): 4}), 2, 3, spamdict({(1): 2,
            (2): 3, (3): 4}), 'a[b]=c', '__setitem__')


        class C(spam.spamdict):

            def foo(self):
                py_ass_parser861 = 1
                Sensor(861)
                return py_ass_parser861
        a = C()
        self.assertEqual(a.items(), [])
        self.assertEqual(a.foo(), 1)
        a['foo'] = 'bar'
        self.assertEqual(a.items(), [('foo', 'bar')])
        self.assertEqual(a.getstate(), 0)
        a.setstate(100)
        self.assertEqual(a.getstate(), 100)
        Sensor(862)


class ClassPropertiesAndMethods(unittest.TestCase):

    def test_python_dicts(self):
        Sensor(863)
        self.assertTrue(issubclass(dict, dict))
        self.assertIsInstance({}, dict)
        d = dict()
        self.assertEqual(d, {})
        self.assertTrue(d.__class__ is dict)
        self.assertIsInstance(d, dict)


        class C(dict):
            state = -1

            def __init__(self_local, *a, **kw):
                Sensor(864)
                if a:
                    self.assertEqual(len(a), 1)
                    self_local.state = a[0]
                if kw:
                    for k, v in kw.items():
                        self_local[v] = k
                Sensor(865)

            def __getitem__(self, key):
                py_ass_parser866 = self.get(key, 0)
                Sensor(866)
                return py_ass_parser866

            def __setitem__(self_local, key, value):
                Sensor(867)
                self.assertIsInstance(key, type(0))
                dict.__setitem__(self_local, key, value)
                Sensor(868)

            def setstate(self, state):
                Sensor(869)
                self.state = state
                Sensor(870)

            def getstate(self):
                py_ass_parser871 = self.state
                Sensor(871)
                return py_ass_parser871
        self.assertTrue(issubclass(C, dict))
        a1 = C(12)
        self.assertEqual(a1.state, 12)
        a2 = C(foo=1, bar=2)
        self.assertEqual(a2[1] == 'foo' and a2[2], 'bar')
        a = C()
        self.assertEqual(a.state, -1)
        self.assertEqual(a.getstate(), -1)
        a.setstate(0)
        self.assertEqual(a.state, 0)
        self.assertEqual(a.getstate(), 0)
        a.setstate(10)
        self.assertEqual(a.state, 10)
        self.assertEqual(a.getstate(), 10)
        self.assertEqual(a[42], 0)
        a[42] = 24
        self.assertEqual(a[42], 24)
        N = 50
        for i in range(N):
            a[i] = C()
            for j in range(N):
                a[i][j] = i * j
        for i in range(N):
            for j in range(N):
                self.assertEqual(a[i][j], i * j)
        Sensor(872)

    def test_python_lists(self):


        class C(list):

            def __getitem__(self, i):
                py_ass_parser873 = list.__getitem__(self, i) + 100
                Sensor(873)
                return py_ass_parser873

            def __getslice__(self, i, j):
                py_ass_parser874 = i, j
                Sensor(874)
                return py_ass_parser874
        a = C()
        a.extend([0, 1, 2])
        self.assertEqual(a[0], 100)
        self.assertEqual(a[1], 101)
        self.assertEqual(a[2], 102)
        self.assertEqual(a[100:200], (100, 200))
        Sensor(875)

    def test_metaclass(self):


        class C:
            __metaclass__ = type

            def __init__(self):
                Sensor(876)
                self.__state = 0
                Sensor(877)

            def getstate(self):
                py_ass_parser878 = self.__state
                Sensor(878)
                return py_ass_parser878

            def setstate(self, state):
                Sensor(879)
                self.__state = state
                Sensor(880)
        Sensor(881)
        a = C()
        self.assertEqual(a.getstate(), 0)
        a.setstate(10)
        self.assertEqual(a.getstate(), 10)


        class D:


            class __metaclass__(type):

                def myself(cls):
                    py_ass_parser882 = cls
                    Sensor(882)
                    return py_ass_parser882
        self.assertEqual(D.myself(), D)
        d = D()
        self.assertEqual(d.__class__, D)


        class M1(type):

            def __new__(cls, name, bases, dict):
                Sensor(883)
                dict['__spam__'] = 1
                py_ass_parser884 = type.__new__(cls, name, bases, dict)
                Sensor(884)
                return py_ass_parser884


        class C:
            __metaclass__ = M1
        self.assertEqual(C.__spam__, 1)
        c = C()
        self.assertEqual(c.__spam__, 1)


        class _instance(object):
            pass


        class M2(object):

            @staticmethod
            def __new__(cls, name, bases, dict):
                Sensor(885)
                self = object.__new__(cls)
                self.name = name
                self.bases = bases
                self.dict = dict
                py_ass_parser886 = self
                Sensor(886)
                return py_ass_parser886

            def __call__(self):
                Sensor(887)
                it = _instance()
                for key in self.dict:
                    if key.startswith('__'):
                        continue
                    setattr(it, key, self.dict[key].__get__(it, self))
                py_ass_parser888 = it
                Sensor(888)
                return py_ass_parser888


        class C:
            __metaclass__ = M2

            def spam(self):
                py_ass_parser889 = 42
                Sensor(889)
                return py_ass_parser889
        self.assertEqual(C.name, 'C')
        self.assertEqual(C.bases, ())
        self.assertIn('spam', C.dict)
        c = C()
        self.assertEqual(c.spam(), 42)


        class autosuper(type):

            def __new__(metaclass, name, bases, dict):
                Sensor(890)
                cls = super(autosuper, metaclass).__new__(metaclass, name,
                    bases, dict)
                while name[:1] == '_':
                    name = name[1:]
                if name:
                    name = '_%s__super' % name
                else:
                    name = '__super'
                setattr(cls, name, super(cls))
                py_ass_parser891 = cls
                Sensor(891)
                return py_ass_parser891


        class A:
            __metaclass__ = autosuper

            def meth(self):
                py_ass_parser892 = 'A'
                Sensor(892)
                return py_ass_parser892


        class B(A):

            def meth(self):
                py_ass_parser893 = 'B' + self.__super.meth()
                Sensor(893)
                return py_ass_parser893


        class C(A):

            def meth(self):
                py_ass_parser894 = 'C' + self.__super.meth()
                Sensor(894)
                return py_ass_parser894


        class D(C, B):

            def meth(self):
                py_ass_parser895 = 'D' + self.__super.meth()
                Sensor(895)
                return py_ass_parser895
        self.assertEqual(D().meth(), 'DCBA')


        class E(B, C):

            def meth(self):
                py_ass_parser896 = 'E' + self.__super.meth()
                Sensor(896)
                return py_ass_parser896
        self.assertEqual(E().meth(), 'EBCA')


        class autoproperty(type):

            def __new__(metaclass, name, bases, dict):
                Sensor(897)
                hits = {}
                for key, val in dict.iteritems():
                    if key.startswith('_get_'):
                        key = key[5:]
                        get, set = hits.get(key, (None, None))
                        get = val
                        hits[key] = get, set
                    elif key.startswith('_set_'):
                        key = key[5:]
                        get, set = hits.get(key, (None, None))
                        set = val
                        hits[key] = get, set
                for key, (get, set) in hits.iteritems():
                    dict[key] = property(get, set)
                py_ass_parser898 = super(autoproperty, metaclass).__new__(
                    metaclass, name, bases, dict)
                Sensor(898)
                return py_ass_parser898


        class A:
            __metaclass__ = autoproperty

            def _get_x(self):
                py_ass_parser899 = -self.__x
                Sensor(899)
                return py_ass_parser899

            def _set_x(self, x):
                Sensor(900)
                self.__x = -x
                Sensor(901)
        a = A()
        self.assertTrue(not hasattr(a, 'x'))
        a.x = 12
        self.assertEqual(a.x, 12)
        self.assertEqual(a._A__x, -12)


        class multimetaclass(autoproperty, autosuper):
            pass


        class A:
            __metaclass__ = multimetaclass

            def _get_x(self):
                py_ass_parser902 = 'A'
                Sensor(902)
                return py_ass_parser902


        class B(A):

            def _get_x(self):
                py_ass_parser903 = 'B' + self.__super._get_x()
                Sensor(903)
                return py_ass_parser903


        class C(A):

            def _get_x(self):
                py_ass_parser904 = 'C' + self.__super._get_x()
                Sensor(904)
                return py_ass_parser904


        class D(C, B):

            def _get_x(self):
                py_ass_parser905 = 'D' + self.__super._get_x()
                Sensor(905)
                return py_ass_parser905
        self.assertEqual(D().x, 'DCBA')


        class T(type):
            counter = 0

            def __init__(self, *args):
                Sensor(906)
                T.counter += 1
                Sensor(907)


        class C:
            __metaclass__ = T
        self.assertEqual(T.counter, 1)
        a = C()
        self.assertEqual(type(a), C)
        self.assertEqual(T.counter, 1)


        class C(object):
            pass
        c = C()
        Sensor(908)
        try:
            c()
        except TypeError:
            pass
        else:
            self.fail('calling object w/o call method should raise TypeError')


        class A(type):

            def __new__(*args, **kwargs):
                py_ass_parser909 = type.__new__(*args, **kwargs)
                Sensor(909)
                return py_ass_parser909


        class B(object):
            pass


        class C(object):
            __metaclass__ = A


        class D(B, C):
            pass
        Sensor(910)

    def test_module_subclasses(self):
        Sensor(911)
        log = []
        MT = type(sys)


        class MM(MT):

            def __init__(self, name):
                Sensor(912)
                MT.__init__(self, name)
                Sensor(913)

            def __getattribute__(self, name):
                Sensor(914)
                log.append(('getattr', name))
                py_ass_parser915 = MT.__getattribute__(self, name)
                Sensor(915)
                return py_ass_parser915

            def __setattr__(self, name, value):
                Sensor(916)
                log.append(('setattr', name, value))
                MT.__setattr__(self, name, value)
                Sensor(917)

            def __delattr__(self, name):
                Sensor(918)
                log.append(('delattr', name))
                MT.__delattr__(self, name)
                Sensor(919)
        a = MM('a')
        a.foo = 12
        x = a.foo
        del a.foo
        self.assertEqual(log, [('setattr', 'foo', 12), ('getattr', 'foo'),
            ('delattr', 'foo')])
        Sensor(920)
        try:
            Sensor(921)


            class Module(types.ModuleType, str):
                pass
        except TypeError:
            pass
        else:
            self.fail(
                'inheriting from ModuleType and str at the same time should fail'
                )
        Sensor(922)

    def test_multiple_inheritence(self):


        class C(object):

            def __init__(self):
                Sensor(923)
                self.__state = 0
                Sensor(924)

            def getstate(self):
                py_ass_parser925 = self.__state
                Sensor(925)
                return py_ass_parser925

            def setstate(self, state):
                Sensor(926)
                self.__state = state
                Sensor(927)
        Sensor(928)
        a = C()
        self.assertEqual(a.getstate(), 0)
        a.setstate(10)
        self.assertEqual(a.getstate(), 10)


        class D(dict, C):

            def __init__(self):
                Sensor(929)
                type({}).__init__(self)
                C.__init__(self)
                Sensor(930)
        d = D()
        self.assertEqual(d.keys(), [])
        d['hello'] = 'world'
        self.assertEqual(d.items(), [('hello', 'world')])
        self.assertEqual(d['hello'], 'world')
        self.assertEqual(d.getstate(), 0)
        d.setstate(10)
        self.assertEqual(d.getstate(), 10)
        self.assertEqual(D.__mro__, (D, dict, C, object))


        class Node(object):

            def __int__(self):
                py_ass_parser931 = int(self.foo())
                Sensor(931)
                return py_ass_parser931

            def foo(self):
                py_ass_parser932 = '23'
                Sensor(932)
                return py_ass_parser932


        class Frag(Node, list):

            def foo(self):
                py_ass_parser933 = '42'
                Sensor(933)
                return py_ass_parser933
        self.assertEqual(Node().__int__(), 23)
        self.assertEqual(int(Node()), 23)
        self.assertEqual(Frag().__int__(), 42)
        self.assertEqual(int(Frag()), 42)


        class A:
            x = 1


        class B(A):
            pass


        class C(A):
            x = 2


        class D(B, C):
            pass
        self.assertEqual(D.x, 1)


        class E(D, object):
            pass
        self.assertEqual(E.__mro__, (E, D, B, A, C, object))
        self.assertEqual(E.x, 1)


        class F(B, C, object):
            pass
        self.assertEqual(F.__mro__, (F, B, C, A, object))
        self.assertEqual(F.x, 2)


        class C:

            def cmethod(self):
                py_ass_parser934 = 'C a'
                Sensor(934)
                return py_ass_parser934

            def all_method(self):
                py_ass_parser935 = 'C b'
                Sensor(935)
                return py_ass_parser935


        class M1(C, object):

            def m1method(self):
                py_ass_parser936 = 'M1 a'
                Sensor(936)
                return py_ass_parser936

            def all_method(self):
                py_ass_parser937 = 'M1 b'
                Sensor(937)
                return py_ass_parser937
        self.assertEqual(M1.__mro__, (M1, C, object))
        m = M1()
        self.assertEqual(m.cmethod(), 'C a')
        self.assertEqual(m.m1method(), 'M1 a')
        self.assertEqual(m.all_method(), 'M1 b')


        class D(C):

            def dmethod(self):
                py_ass_parser938 = 'D a'
                Sensor(938)
                return py_ass_parser938

            def all_method(self):
                py_ass_parser939 = 'D b'
                Sensor(939)
                return py_ass_parser939


        class M2(D, object):

            def m2method(self):
                py_ass_parser940 = 'M2 a'
                Sensor(940)
                return py_ass_parser940

            def all_method(self):
                py_ass_parser941 = 'M2 b'
                Sensor(941)
                return py_ass_parser941
        self.assertEqual(M2.__mro__, (M2, D, C, object))
        m = M2()
        self.assertEqual(m.cmethod(), 'C a')
        self.assertEqual(m.dmethod(), 'D a')
        self.assertEqual(m.m2method(), 'M2 a')
        self.assertEqual(m.all_method(), 'M2 b')


        class M3(M1, M2, object):

            def m3method(self):
                py_ass_parser942 = 'M3 a'
                Sensor(942)
                return py_ass_parser942

            def all_method(self):
                py_ass_parser943 = 'M3 b'
                Sensor(943)
                return py_ass_parser943
        self.assertEqual(M3.__mro__, (M3, M1, M2, D, C, object))
        m = M3()
        self.assertEqual(m.cmethod(), 'C a')
        self.assertEqual(m.dmethod(), 'D a')
        self.assertEqual(m.m1method(), 'M1 a')
        self.assertEqual(m.m2method(), 'M2 a')
        self.assertEqual(m.m3method(), 'M3 a')
        self.assertEqual(m.all_method(), 'M3 b')


        class Classic:
            pass
        try:
            Sensor(944)


            class New(Classic):
                __metaclass__ = type
        except TypeError:
            pass
        else:
            self.fail("new class with only classic bases - shouldn't be")
        Sensor(945)

    def test_diamond_inheritence(self):


        class A(object):

            def spam(self):
                py_ass_parser946 = 'A'
                Sensor(946)
                return py_ass_parser946
        self.assertEqual(A().spam(), 'A')


        class B(A):

            def boo(self):
                py_ass_parser947 = 'B'
                Sensor(947)
                return py_ass_parser947

            def spam(self):
                py_ass_parser948 = 'B'
                Sensor(948)
                return py_ass_parser948
        self.assertEqual(B().spam(), 'B')
        self.assertEqual(B().boo(), 'B')


        class C(A):

            def boo(self):
                py_ass_parser949 = 'C'
                Sensor(949)
                return py_ass_parser949
        self.assertEqual(C().spam(), 'A')
        self.assertEqual(C().boo(), 'C')


        class D(B, C):
            pass
        self.assertEqual(D().spam(), 'B')
        self.assertEqual(D().boo(), 'B')
        self.assertEqual(D.__mro__, (D, B, C, A, object))


        class E(C, B):
            pass
        self.assertEqual(E().spam(), 'B')
        self.assertEqual(E().boo(), 'C')
        self.assertEqual(E.__mro__, (E, C, B, A, object))
        try:
            Sensor(950)


            class F(D, E):
                pass
        except TypeError:
            pass
        else:
            self.fail('expected MRO order disagreement (F)')
        try:
            Sensor(951)


            class G(E, D):
                pass
        except TypeError:
            pass
        else:
            self.fail('expected MRO order disagreement (G)')
        Sensor(952)

    def test_ex5_from_c3_switch(self):


        class A(object):
            pass


        class B(object):
            pass


        class C(object):
            pass


        class X(A):
            pass


        class Y(A):
            pass


        class Z(X, B, Y, C):
            pass
        Sensor(953)
        self.assertEqual(Z.__mro__, (Z, X, B, Y, A, C, object))
        Sensor(954)

    def test_monotonicity(self):


        class Boat(object):
            pass


        class DayBoat(Boat):
            pass


        class WheelBoat(Boat):
            pass


        class EngineLess(DayBoat):
            pass


        class SmallMultihull(DayBoat):
            pass


        class PedalWheelBoat(EngineLess, WheelBoat):
            pass


        class SmallCatamaran(SmallMultihull):
            pass


        class Pedalo(PedalWheelBoat, SmallCatamaran):
            pass
        Sensor(955)
        self.assertEqual(PedalWheelBoat.__mro__, (PedalWheelBoat,
            EngineLess, DayBoat, WheelBoat, Boat, object))
        self.assertEqual(SmallCatamaran.__mro__, (SmallCatamaran,
            SmallMultihull, DayBoat, Boat, object))
        self.assertEqual(Pedalo.__mro__, (Pedalo, PedalWheelBoat,
            EngineLess, SmallCatamaran, SmallMultihull, DayBoat, WheelBoat,
            Boat, object))
        Sensor(956)

    def test_consistency_with_epg(self):


        class Pane(object):
            pass


        class ScrollingMixin(object):
            pass


        class EditingMixin(object):
            pass


        class ScrollablePane(Pane, ScrollingMixin):
            pass


        class EditablePane(Pane, EditingMixin):
            pass


        class EditableScrollablePane(ScrollablePane, EditablePane):
            pass
        Sensor(957)
        self.assertEqual(EditableScrollablePane.__mro__, (
            EditableScrollablePane, ScrollablePane, EditablePane, Pane,
            ScrollingMixin, EditingMixin, object))
        Sensor(958)

    def test_mro_disagreement(self):
        Sensor(959)
        mro_err_msg = (
            'Cannot create a consistent method resolution\norder (MRO) for bases '
            )

        def raises(exc, expected, callable, *args):
            Sensor(960)
            try:
                callable(*args)
            except exc as msg:
                if test_support.check_impl_detail():
                    if not str(msg).startswith(expected):
                        self.fail('Message %r, expected %r' % (str(msg),
                            expected))
            else:
                self.fail('Expected %s' % exc)
            Sensor(961)


        class A(object):
            pass


        class B(A):
            pass


        class C(object):
            pass
        raises(TypeError, 'duplicate base class A', type, 'X', (A, A), {})
        raises(TypeError, mro_err_msg, type, 'X', (A, B), {})
        raises(TypeError, mro_err_msg, type, 'X', (A, C, B), {})


        class GridLayout(object):
            pass


        class HorizontalGrid(GridLayout):
            pass


        class VerticalGrid(GridLayout):
            pass


        class HVGrid(HorizontalGrid, VerticalGrid):
            pass


        class VHGrid(VerticalGrid, HorizontalGrid):
            pass
        raises(TypeError, mro_err_msg, type, 'ConfusedGrid', (HVGrid,
            VHGrid), {})
        Sensor(962)

    def test_object_class(self):
        Sensor(963)
        a = object()
        self.assertEqual(a.__class__, object)
        self.assertEqual(type(a), object)
        b = object()
        self.assertNotEqual(a, b)
        self.assertFalse(hasattr(a, 'foo'))
        try:
            a.foo = 12
        except (AttributeError, TypeError):
            pass
        else:
            self.fail('object() should not allow setting a foo attribute')
        self.assertFalse(hasattr(object(), '__dict__'))


        class Cdict(object):
            pass
        x = Cdict()
        self.assertEqual(x.__dict__, {})
        x.foo = 1
        self.assertEqual(x.foo, 1)
        self.assertEqual(x.__dict__, {'foo': 1})
        Sensor(964)

    def test_slots(self):


        class C0(object):
            __slots__ = []
        Sensor(965)
        x = C0()
        self.assertFalse(hasattr(x, '__dict__'))
        self.assertFalse(hasattr(x, 'foo'))


        class C1(object):
            __slots__ = ['a']
        x = C1()
        self.assertFalse(hasattr(x, '__dict__'))
        self.assertFalse(hasattr(x, 'a'))
        x.a = 1
        self.assertEqual(x.a, 1)
        x.a = None
        self.assertEqual(x.a, None)
        del x.a
        self.assertFalse(hasattr(x, 'a'))


        class C3(object):
            __slots__ = ['a', 'b', 'c']
        x = C3()
        self.assertFalse(hasattr(x, '__dict__'))
        self.assertFalse(hasattr(x, 'a'))
        self.assertFalse(hasattr(x, 'b'))
        self.assertFalse(hasattr(x, 'c'))
        x.a = 1
        x.b = 2
        x.c = 3
        self.assertEqual(x.a, 1)
        self.assertEqual(x.b, 2)
        self.assertEqual(x.c, 3)


        class C4(object):
            """Validate name mangling"""
            __slots__ = ['__a']

            def __init__(self, value):
                Sensor(966)
                self.__a = value
                Sensor(967)

            def get(self):
                py_ass_parser968 = self.__a
                Sensor(968)
                return py_ass_parser968
        x = C4(5)
        self.assertFalse(hasattr(x, '__dict__'))
        self.assertFalse(hasattr(x, '__a'))
        self.assertEqual(x.get(), 5)
        try:
            x.__a = 6
        except AttributeError:
            pass
        else:
            self.fail('Double underscored names not mangled')
        try:
            Sensor(969)


            class C(object):
                __slots__ = [None]
        except TypeError:
            pass
        else:
            self.fail('[None] slots not caught')
        try:
            Sensor(970)


            class C(object):
                __slots__ = ['foo bar']
        except TypeError:
            pass
        else:
            self.fail("['foo bar'] slots not caught")
        try:
            Sensor(971)


            class C(object):
                __slots__ = ['foo\x00bar']
        except TypeError:
            pass
        else:
            self.fail("['foo\\0bar'] slots not caught")
        try:
            Sensor(972)


            class C(object):
                __slots__ = ['1']
        except TypeError:
            pass
        else:
            self.fail("['1'] slots not caught")
        try:
            Sensor(973)


            class C(object):
                __slots__ = ['']
        except TypeError:
            pass
        else:
            self.fail("[''] slots not caught")


        class C(object):
            __slots__ = ['a', 'a_b', '_a', 'A0123456789Z']


        class C(object):
            __slots__ = 'abc'
        c = C()
        c.abc = 5
        self.assertEqual(c.abc, 5)
        try:
            unicode
        except NameError:
            pass
        else:


            class C(object):
                __slots__ = unicode('abc')
            c = C()
            c.abc = 5
            self.assertEqual(c.abc, 5)
            slots = unicode('foo'), unicode('bar')


            class C(object):
                __slots__ = slots
            x = C()
            x.foo = 5
            self.assertEqual(x.foo, 5)
            self.assertEqual(type(slots[0]), unicode)
            try:
                Sensor(974)


                class C(object):
                    __slots__ = [unichr(128)]
            except (TypeError, UnicodeEncodeError):
                pass
            else:
                self.fail('[unichr(128)] slots not caught')


        class Counted(object):
            counter = 0

            def __init__(self):
                Sensor(975)
                Counted.counter += 1
                Sensor(976)

            def __del__(self):
                Sensor(977)
                Counted.counter -= 1
                Sensor(978)


        class C(object):
            __slots__ = ['a', 'b', 'c']
        Sensor(979)
        x = C()
        x.a = Counted()
        x.b = Counted()
        x.c = Counted()
        self.assertEqual(Counted.counter, 3)
        del x
        test_support.gc_collect()
        self.assertEqual(Counted.counter, 0)


        class D(C):
            pass
        x = D()
        x.a = Counted()
        x.z = Counted()
        self.assertEqual(Counted.counter, 2)
        del x
        test_support.gc_collect()
        self.assertEqual(Counted.counter, 0)


        class E(D):
            __slots__ = ['e']
        x = E()
        x.a = Counted()
        x.z = Counted()
        x.e = Counted()
        self.assertEqual(Counted.counter, 3)
        del x
        test_support.gc_collect()
        self.assertEqual(Counted.counter, 0)


        class F(object):
            __slots__ = ['a', 'b']
        s = F()
        s.a = [Counted(), s]
        self.assertEqual(Counted.counter, 1)
        s = None
        test_support.gc_collect()
        self.assertEqual(Counted.counter, 0)
        if hasattr(gc, 'get_objects'):


            class G(object):

                def __cmp__(self, other):
                    py_ass_parser980 = 0
                    Sensor(980)
                    return py_ass_parser980
                __hash__ = None
            g = G()
            orig_objects = len(gc.get_objects())
            for i in xrange(10):
                g == g
            new_objects = len(gc.get_objects())
            self.assertEqual(orig_objects, new_objects)


        class H(object):
            __slots__ = ['a', 'b']

            def __init__(self):
                Sensor(981)
                self.a = 1
                self.b = 2
                Sensor(982)

            def __del__(self_):
                Sensor(983)
                self.assertEqual(self_.a, 1)
                self.assertEqual(self_.b, 2)
                Sensor(984)
        with test_support.captured_output('stderr') as s:
            Sensor(985)
            h = H()
            del h
        self.assertEqual(s.getvalue(), '')


        class X(object):
            __slots__ = 'a'
        with self.assertRaises(AttributeError):
            del X().a
        Sensor(986)

    def test_slots_special(self):


        class D(object):
            __slots__ = ['__dict__']
        Sensor(987)
        a = D()
        self.assertTrue(hasattr(a, '__dict__'))
        self.assertFalse(hasattr(a, '__weakref__'))
        a.foo = 42
        self.assertEqual(a.__dict__, {'foo': 42})


        class W(object):
            __slots__ = ['__weakref__']
        a = W()
        self.assertTrue(hasattr(a, '__weakref__'))
        self.assertFalse(hasattr(a, '__dict__'))
        try:
            a.foo = 42
        except AttributeError:
            pass
        else:
            self.fail("shouldn't be allowed to set a.foo")


        class C1(W, D):
            __slots__ = []
        a = C1()
        self.assertTrue(hasattr(a, '__dict__'))
        self.assertTrue(hasattr(a, '__weakref__'))
        a.foo = 42
        self.assertEqual(a.__dict__, {'foo': 42})


        class C2(D, W):
            __slots__ = []
        a = C2()
        self.assertTrue(hasattr(a, '__dict__'))
        self.assertTrue(hasattr(a, '__weakref__'))
        a.foo = 42
        self.assertEqual(a.__dict__, {'foo': 42})
        Sensor(988)

    def test_slots_descriptor(self):
        import abc


        class MyABC:
            __metaclass__ = abc.ABCMeta
            __slots__ = 'a'


        class Unrelated(object):
            pass
        Sensor(989)
        MyABC.register(Unrelated)
        u = Unrelated()
        self.assertIsInstance(u, MyABC)
        self.assertRaises(TypeError, MyABC.a.__set__, u, 3)
        Sensor(990)

    def test_metaclass_cmp(self):


        class M(type):

            def __cmp__(self, other):
                py_ass_parser991 = -1
                Sensor(991)
                return py_ass_parser991


        class X(object):
            __metaclass__ = M
        self.assertTrue(X < M)
        Sensor(992)

    def test_dynamics(self):


        class D(object):
            pass


        class E(D):
            pass


        class F(D):
            pass
        Sensor(993)
        D.foo = 1
        self.assertEqual(D.foo, 1)
        self.assertEqual(E.foo, 1)
        self.assertEqual(F.foo, 1)


        class C(object):
            pass
        a = C()
        self.assertFalse(hasattr(a, 'foobar'))
        C.foobar = 2
        self.assertEqual(a.foobar, 2)
        C.method = lambda self: 42
        self.assertEqual(a.method(), 42)
        C.__repr__ = lambda self: 'C()'
        self.assertEqual(repr(a), 'C()')
        C.__int__ = lambda self: 100
        self.assertEqual(int(a), 100)
        self.assertEqual(a.foobar, 2)
        self.assertFalse(hasattr(a, 'spam'))

        def mygetattr(self, name):
            Sensor(994)
            if name == 'spam':
                py_ass_parser995 = 'spam'
                Sensor(995)
                return py_ass_parser995
            raise AttributeError
        C.__getattr__ = mygetattr
        self.assertEqual(a.spam, 'spam')
        a.new = 12
        self.assertEqual(a.new, 12)

        def mysetattr(self, name, value):
            Sensor(996)
            if name == 'spam':
                raise AttributeError
            py_ass_parser997 = object.__setattr__(self, name, value)
            Sensor(997)
            return py_ass_parser997
        C.__setattr__ = mysetattr
        try:
            a.spam = 'not spam'
        except AttributeError:
            pass
        else:
            self.fail('expected AttributeError')
        self.assertEqual(a.spam, 'spam')


        class D(C):
            pass
        d = D()
        d.foo = 1
        self.assertEqual(d.foo, 1)


        class I(int):
            pass
        self.assertEqual('a' * I(2), 'aa')
        self.assertEqual(I(2) * 'a', 'aa')
        self.assertEqual(2 * I(3), 6)
        self.assertEqual(I(3) * 2, 6)
        self.assertEqual(I(3) * I(2), 6)


        class L(long):
            pass
        self.assertEqual('a' * L(2L), 'aa')
        self.assertEqual(L(2L) * 'a', 'aa')
        self.assertEqual(2 * L(3), 6)
        self.assertEqual(L(3) * 2, 6)
        self.assertEqual(L(3) * L(2), 6)


        class dynamicmetaclass(type):
            pass


        class someclass:
            __metaclass__ = dynamicmetaclass
        self.assertNotEqual(someclass, object)
        Sensor(998)

    def test_errors(self):
        Sensor(999)
        try:
            Sensor(1000)


            class C(list, dict):
                pass
        except TypeError:
            pass
        else:
            self.fail('inheritance from both list and dict should be illegal')
        try:
            Sensor(1001)


            class C(object, None):
                pass
        except TypeError:
            pass
        else:
            self.fail('inheritance from non-type should be illegal')


        class Classic:
            pass
        try:
            Sensor(1002)


            class C(type(len)):
                pass
        except TypeError:
            pass
        else:
            self.fail('inheritance from CFunction should be illegal')
        try:
            Sensor(1003)


            class C(object):
                __slots__ = 1
        except TypeError:
            pass
        else:
            self.fail('__slots__ = 1 should be illegal')
        try:
            Sensor(1004)


            class C(object):
                __slots__ = [1]
        except TypeError:
            pass
        else:
            self.fail('__slots__ = [1] should be illegal')


        class M1(type):
            pass


        class M2(type):
            pass


        class A1(object):
            __metaclass__ = M1


        class A2(object):
            __metaclass__ = M2
        try:
            Sensor(1005)


            class B(A1, A2):
                pass
        except TypeError:
            pass
        else:
            self.fail('finding the most derived metaclass should have failed')
        Sensor(1006)

    def test_classmethods(self):


        class C(object):

            def foo(*a):
                py_ass_parser1007 = a
                Sensor(1007)
                return py_ass_parser1007
            goo = classmethod(foo)
        c = C()
        self.assertEqual(C.goo(1), (C, 1))
        self.assertEqual(c.goo(1), (C, 1))
        self.assertEqual(c.foo(1), (c, 1))


        class D(C):
            pass
        d = D()
        self.assertEqual(D.goo(1), (D, 1))
        self.assertEqual(d.goo(1), (D, 1))
        self.assertEqual(d.foo(1), (d, 1))
        self.assertEqual(D.foo(d, 1), (d, 1))

        def f(cls, arg):
            py_ass_parser1008 = cls, arg
            Sensor(1008)
            return py_ass_parser1008
        ff = classmethod(f)
        self.assertEqual(ff.__get__(0, int)(42), (int, 42))
        self.assertEqual(ff.__get__(0)(42), (int, 42))
        self.assertEqual(C.goo.im_self, C)
        self.assertEqual(D.goo.im_self, D)
        self.assertEqual(super(D, D).goo.im_self, D)
        self.assertEqual(super(D, d).goo.im_self, D)
        self.assertEqual(super(D, D).goo(), (D,))
        self.assertEqual(super(D, d).goo(), (D,))
        meth = classmethod(1).__get__(1)
        self.assertRaises(TypeError, meth)
        try:
            classmethod(f, kw=1)
        except TypeError:
            pass
        else:
            self.fail("classmethod shouldn't accept keyword args")
        Sensor(1009)

    @test_support.impl_detail("the module 'xxsubtype' is internal")
    def test_classmethods_in_c(self):
        import xxsubtype as spam
        Sensor(1010)
        a = 1, 2, 3
        d = {'abc': 123}
        x, a1, d1 = spam.spamlist.classmeth(*a, **d)
        self.assertEqual(x, spam.spamlist)
        self.assertEqual(a, a1)
        self.assertEqual(d, d1)
        x, a1, d1 = spam.spamlist().classmeth(*a, **d)
        self.assertEqual(x, spam.spamlist)
        self.assertEqual(a, a1)
        self.assertEqual(d, d1)
        spam_cm = spam.spamlist.__dict__['classmeth']
        x2, a2, d2 = spam_cm(spam.spamlist, *a, **d)
        self.assertEqual(x2, spam.spamlist)
        self.assertEqual(a2, a1)
        self.assertEqual(d2, d1)


        class SubSpam(spam.spamlist):
            pass
        x2, a2, d2 = spam_cm(SubSpam, *a, **d)
        self.assertEqual(x2, SubSpam)
        self.assertEqual(a2, a1)
        self.assertEqual(d2, d1)
        with self.assertRaises(TypeError):
            spam_cm()
        with self.assertRaises(TypeError):
            spam_cm(spam.spamlist())
        with self.assertRaises(TypeError):
            spam_cm(list)
        Sensor(1011)

    def test_staticmethods(self):


        class C(object):

            def foo(*a):
                py_ass_parser1012 = a
                Sensor(1012)
                return py_ass_parser1012
            goo = staticmethod(foo)
        c = C()
        self.assertEqual(C.goo(1), (1,))
        self.assertEqual(c.goo(1), (1,))
        self.assertEqual(c.foo(1), (c, 1))


        class D(C):
            pass
        d = D()
        self.assertEqual(D.goo(1), (1,))
        self.assertEqual(d.goo(1), (1,))
        self.assertEqual(d.foo(1), (d, 1))
        self.assertEqual(D.foo(d, 1), (d, 1))
        Sensor(1013)

    @test_support.impl_detail("the module 'xxsubtype' is internal")
    def test_staticmethods_in_c(self):
        import xxsubtype as spam
        Sensor(1014)
        a = 1, 2, 3
        d = {'abc': 123}
        x, a1, d1 = spam.spamlist.staticmeth(*a, **d)
        self.assertEqual(x, None)
        self.assertEqual(a, a1)
        self.assertEqual(d, d1)
        x, a1, d2 = spam.spamlist().staticmeth(*a, **d)
        self.assertEqual(x, None)
        self.assertEqual(a, a1)
        self.assertEqual(d, d1)
        Sensor(1015)

    def test_classic(self):


        class C:

            def foo(*a):
                py_ass_parser1016 = a
                Sensor(1016)
                return py_ass_parser1016
            goo = classmethod(foo)
        c = C()
        self.assertEqual(C.goo(1), (C, 1))
        self.assertEqual(c.goo(1), (C, 1))
        self.assertEqual(c.foo(1), (c, 1))


        class D(C):
            pass
        d = D()
        self.assertEqual(D.goo(1), (D, 1))
        self.assertEqual(d.goo(1), (D, 1))
        self.assertEqual(d.foo(1), (d, 1))
        self.assertEqual(D.foo(d, 1), (d, 1))


        class E:
            foo = C.foo
        self.assertEqual(E().foo, C.foo)
        self.assertTrue(repr(C.foo.__get__(C())).startswith('<bound method '))
        Sensor(1017)

    def test_compattr(self):


        class C(object):


            class computed_attribute(object):

                def __init__(self, get, set=None, delete=None):
                    Sensor(1018)
                    self.__get = get
                    self.__set = set
                    self.__delete = delete
                    Sensor(1019)

                def __get__(self, obj, type=None):
                    py_ass_parser1020 = self.__get(obj)
                    Sensor(1020)
                    return py_ass_parser1020

                def __set__(self, obj, value):
                    py_ass_parser1021 = self.__set(obj, value)
                    Sensor(1021)
                    return py_ass_parser1021

                def __delete__(self, obj):
                    py_ass_parser1022 = self.__delete(obj)
                    Sensor(1022)
                    return py_ass_parser1022

            def __init__(self):
                Sensor(1023)
                self.__x = 0
                Sensor(1024)

            def __get_x(self):
                Sensor(1025)
                x = self.__x
                self.__x = x + 1
                py_ass_parser1026 = x
                Sensor(1026)
                return py_ass_parser1026

            def __set_x(self, x):
                Sensor(1027)
                self.__x = x
                Sensor(1028)

            def __delete_x(self):
                Sensor(1029)
                del self.__x
                Sensor(1030)
            x = computed_attribute(__get_x, __set_x, __delete_x)
        Sensor(1031)
        a = C()
        self.assertEqual(a.x, 0)
        self.assertEqual(a.x, 1)
        a.x = 10
        self.assertEqual(a.x, 10)
        self.assertEqual(a.x, 11)
        del a.x
        self.assertEqual(hasattr(a, 'x'), 0)
        Sensor(1032)

    def test_newslots(self):


        class C(list):

            def __new__(cls):
                Sensor(1033)
                self = list.__new__(cls)
                self.foo = 1
                py_ass_parser1034 = self
                Sensor(1034)
                return py_ass_parser1034

            def __init__(self):
                Sensor(1035)
                self.foo = self.foo + 2
                Sensor(1036)
        Sensor(1037)
        a = C()
        self.assertEqual(a.foo, 3)
        self.assertEqual(a.__class__, C)


        class D(C):
            pass
        b = D()
        self.assertEqual(b.foo, 3)
        self.assertEqual(b.__class__, D)
        Sensor(1038)

    def test_altmro(self):


        class A(object):

            def f(self):
                py_ass_parser1039 = 'A'
                Sensor(1039)
                return py_ass_parser1039


        class B(A):
            pass


        class C(A):

            def f(self):
                py_ass_parser1040 = 'C'
                Sensor(1040)
                return py_ass_parser1040


        class D(B, C):
            pass
        self.assertEqual(D.mro(), [D, B, C, A, object])
        self.assertEqual(D.__mro__, (D, B, C, A, object))
        self.assertEqual(D().f(), 'C')


        class PerverseMetaType(type):

            def mro(cls):
                Sensor(1041)
                L = type.mro(cls)
                L.reverse()
                py_ass_parser1042 = L
                Sensor(1042)
                return py_ass_parser1042


        class X(D, B, C, A):
            __metaclass__ = PerverseMetaType
        self.assertEqual(X.__mro__, (object, A, C, B, D, X))
        self.assertEqual(X().f(), 'A')
        try:


            class X(object):


                class __metaclass__(type):

                    def mro(self):
                        py_ass_parser1043 = [self, dict, object]
                        Sensor(1043)
                        return py_ass_parser1043
            x = object.__new__(X)
            x[5] = 6
        except TypeError:
            pass
        else:
            self.fail('devious mro() return not caught')
        try:
            Sensor(1045)


            class X(object):


                class __metaclass__(type):

                    def mro(self):
                        py_ass_parser1044 = [1]
                        Sensor(1044)
                        return py_ass_parser1044
        except TypeError:
            pass
        else:
            self.fail('non-class mro() return not caught')
        try:
            Sensor(1047)


            class X(object):


                class __metaclass__(type):

                    def mro(self):
                        py_ass_parser1046 = 1
                        Sensor(1046)
                        return py_ass_parser1046
        except TypeError:
            pass
        else:
            self.fail('non-sequence mro() return not caught')
        Sensor(1048)

    def test_overloading(self):


        class B(object):
            """Intermediate class because object doesn't have a __setattr__"""


        class C(B):

            def __getattr__(self, name):
                Sensor(1049)
                if name == 'foo':
                    py_ass_parser1050 = 'getattr', name
                    Sensor(1050)
                    return py_ass_parser1050
                else:
                    raise AttributeError
                Sensor(1051)

            def __setattr__(self, name, value):
                Sensor(1052)
                if name == 'foo':
                    self.setattr = name, value
                else:
                    py_ass_parser1053 = B.__setattr__(self, name, value)
                    Sensor(1053)
                    return py_ass_parser1053
                Sensor(1054)

            def __delattr__(self, name):
                Sensor(1055)
                if name == 'foo':
                    self.delattr = name
                else:
                    py_ass_parser1056 = B.__delattr__(self, name)
                    Sensor(1056)
                    return py_ass_parser1056
                Sensor(1057)

            def __getitem__(self, key):
                py_ass_parser1058 = 'getitem', key
                Sensor(1058)
                return py_ass_parser1058

            def __setitem__(self, key, value):
                Sensor(1059)
                self.setitem = key, value
                Sensor(1060)

            def __delitem__(self, key):
                Sensor(1061)
                self.delitem = key
                Sensor(1062)

            def __getslice__(self, i, j):
                py_ass_parser1063 = 'getslice', i, j
                Sensor(1063)
                return py_ass_parser1063

            def __setslice__(self, i, j, value):
                Sensor(1064)
                self.setslice = i, j, value
                Sensor(1065)

            def __delslice__(self, i, j):
                Sensor(1066)
                self.delslice = i, j
                Sensor(1067)
        Sensor(1068)
        a = C()
        self.assertEqual(a.foo, ('getattr', 'foo'))
        a.foo = 12
        self.assertEqual(a.setattr, ('foo', 12))
        del a.foo
        self.assertEqual(a.delattr, 'foo')
        self.assertEqual(a[12], ('getitem', 12))
        a[12] = 21
        self.assertEqual(a.setitem, (12, 21))
        del a[12]
        self.assertEqual(a.delitem, 12)
        self.assertEqual(a[0:10], ('getslice', 0, 10))
        a[0:10] = 'foo'
        self.assertEqual(a.setslice, (0, 10, 'foo'))
        del a[0:10]
        self.assertEqual(a.delslice, (0, 10))
        Sensor(1069)

    def test_methods(self):


        class C(object):

            def __init__(self, x):
                Sensor(1070)
                self.x = x
                Sensor(1071)

            def foo(self):
                py_ass_parser1072 = self.x
                Sensor(1072)
                return py_ass_parser1072
        c1 = C(1)
        self.assertEqual(c1.foo(), 1)


        class D(C):
            boo = C.foo
            goo = c1.foo
        d2 = D(2)
        self.assertEqual(d2.foo(), 2)
        self.assertEqual(d2.boo(), 2)
        self.assertEqual(d2.goo(), 1)


        class E(object):
            foo = C.foo
        self.assertEqual(E().foo, C.foo)
        self.assertTrue(repr(C.foo.__get__(C(1))).startswith('<bound method '))
        Sensor(1073)

    def test_special_method_lookup(self):

        def run_context(manager):
            with manager:
                Sensor(1074)
                pass
            Sensor(1075)

        def iden(self):
            py_ass_parser1076 = self
            Sensor(1076)
            return py_ass_parser1076

        def hello(self):
            py_ass_parser1077 = 'hello'
            Sensor(1077)
            return py_ass_parser1077

        def empty_seq(self):
            py_ass_parser1078 = []
            Sensor(1078)
            return py_ass_parser1078

        def zero(self):
            py_ass_parser1079 = 0
            Sensor(1079)
            return py_ass_parser1079

        def complex_num(self):
            py_ass_parser1080 = 1j
            Sensor(1080)
            return py_ass_parser1080

        def stop(self):
            Sensor(1081)
            raise StopIteration

        def return_true(self, thing=None):
            py_ass_parser1082 = True
            Sensor(1082)
            return py_ass_parser1082

        def do_isinstance(obj):
            py_ass_parser1083 = isinstance(int, obj)
            Sensor(1083)
            return py_ass_parser1083

        def do_issubclass(obj):
            py_ass_parser1084 = issubclass(int, obj)
            Sensor(1084)
            return py_ass_parser1084

        def swallow(*args):
            Sensor(1085)
            pass
            Sensor(1086)

        def do_dict_missing(checker):


            class DictSub(checker.__class__, dict):
                pass
            Sensor(1087)
            self.assertEqual(DictSub()['hi'], 4)
            Sensor(1088)

        def some_number(self_, key):
            Sensor(1089)
            self.assertEqual(key, 'hi')
            py_ass_parser1090 = 4
            Sensor(1090)
            return py_ass_parser1090

        def format_impl(self, spec):
            py_ass_parser1091 = 'hello'
            Sensor(1091)
            return py_ass_parser1091
        specials = [('__unicode__', unicode, hello, set(), {}), (
            '__reversed__', reversed, empty_seq, set(), {}), (
            '__length_hint__', list, zero, set(), {'__iter__': iden, 'next':
            stop}), ('__sizeof__', sys.getsizeof, zero, set(), {}), (
            '__instancecheck__', do_isinstance, return_true, set(), {}), (
            '__missing__', do_dict_missing, some_number, set(('__class__',)
            ), {}), ('__subclasscheck__', do_issubclass, return_true, set((
            '__bases__',)), {}), ('__enter__', run_context, iden, set(), {
            '__exit__': swallow}), ('__exit__', run_context, swallow, set(),
            {'__enter__': iden}), ('__complex__', complex, complex_num, set
            (), {}), ('__format__', format, format_impl, set(), {}), (
            '__dir__', dir, empty_seq, set(), {})]


        class Checker(object):

            def __getattr__(self, attr, test=self):
                Sensor(1092)
                test.fail('__getattr__ called with {0}'.format(attr))
                Sensor(1093)

            def __getattribute__(self, attr, test=self):
                Sensor(1094)
                if attr not in ok:
                    test.fail('__getattribute__ called with {0}'.format(attr))
                py_ass_parser1095 = object.__getattribute__(self, attr)
                Sensor(1095)
                return py_ass_parser1095


        class SpecialDescr(object):

            def __init__(self, impl):
                Sensor(1096)
                self.impl = impl
                Sensor(1097)

            def __get__(self, obj, owner):
                Sensor(1098)
                record.append(1)
                py_ass_parser1099 = self.impl.__get__(obj, owner)
                Sensor(1099)
                return py_ass_parser1099


        class MyException(Exception):
            pass


        class ErrDescr(object):

            def __get__(self, obj, owner):
                Sensor(1100)
                raise MyException
        for name, runner, meth_impl, ok, env in specials:


            class X(Checker):
                pass
            for attr, obj in env.iteritems():
                setattr(X, attr, obj)
            setattr(X, name, meth_impl)
            runner(X())
            record = []


            class X(Checker):
                pass
            for attr, obj in env.iteritems():
                setattr(X, attr, obj)
            setattr(X, name, SpecialDescr(meth_impl))
            runner(X())
            self.assertEqual(record, [1], name)


            class X(Checker):
                pass
            for attr, obj in env.iteritems():
                setattr(X, attr, obj)
            setattr(X, name, ErrDescr())
            try:
                runner(X())
            except MyException:
                pass
            else:
                self.fail("{0!r} didn't raise".format(name))
        Sensor(1101)

    def test_specials(self):


        class C(object):

            def __getitem__(self, i):
                Sensor(1102)
                if 0 <= i < 10:
                    py_ass_parser1103 = i
                    Sensor(1103)
                    return py_ass_parser1103
                raise IndexError
        c1 = C()
        c2 = C()
        self.assertTrue(not not c1)
        self.assertNotEqual(id(c1), id(c2))
        hash(c1)
        hash(c2)
        self.assertEqual(cmp(c1, c2), cmp(id(c1), id(c2)))
        self.assertEqual(c1, c1)
        self.assertTrue(c1 != c2)
        self.assertTrue(not c1 != c1)
        self.assertTrue(not c1 == c2)
        self.assertTrue(str(c1).find('C object at ') >= 0)
        self.assertEqual(str(c1), repr(c1))
        self.assertNotIn(-1, c1)
        for i in range(10):
            self.assertIn(i, c1)
        self.assertNotIn(10, c1)


        class D(object):

            def __getitem__(self, i):
                Sensor(1104)
                if 0 <= i < 10:
                    py_ass_parser1105 = i
                    Sensor(1105)
                    return py_ass_parser1105
                raise IndexError
        d1 = D()
        d2 = D()
        self.assertTrue(not not d1)
        self.assertNotEqual(id(d1), id(d2))
        hash(d1)
        hash(d2)
        self.assertEqual(cmp(d1, d2), cmp(id(d1), id(d2)))
        self.assertEqual(d1, d1)
        self.assertNotEqual(d1, d2)
        self.assertTrue(not d1 != d1)
        self.assertTrue(not d1 == d2)
        self.assertTrue(str(d1).find('D object at ') >= 0)
        self.assertEqual(str(d1), repr(d1))
        self.assertNotIn(-1, d1)
        for i in range(10):
            self.assertIn(i, d1)
        self.assertNotIn(10, d1)


        class Proxy(object):

            def __init__(self, x):
                Sensor(1106)
                self.x = x
                Sensor(1107)

            def __nonzero__(self):
                py_ass_parser1108 = not not self.x
                Sensor(1108)
                return py_ass_parser1108

            def __hash__(self):
                py_ass_parser1109 = hash(self.x)
                Sensor(1109)
                return py_ass_parser1109

            def __eq__(self, other):
                py_ass_parser1110 = self.x == other
                Sensor(1110)
                return py_ass_parser1110

            def __ne__(self, other):
                py_ass_parser1111 = self.x != other
                Sensor(1111)
                return py_ass_parser1111

            def __cmp__(self, other):
                py_ass_parser1112 = cmp(self.x, other.x)
                Sensor(1112)
                return py_ass_parser1112

            def __str__(self):
                py_ass_parser1113 = 'Proxy:%s' % self.x
                Sensor(1113)
                return py_ass_parser1113

            def __repr__(self):
                py_ass_parser1114 = 'Proxy(%r)' % self.x
                Sensor(1114)
                return py_ass_parser1114

            def __contains__(self, value):
                py_ass_parser1115 = value in self.x
                Sensor(1115)
                return py_ass_parser1115
        p0 = Proxy(0)
        p1 = Proxy(1)
        p_1 = Proxy(-1)
        self.assertFalse(p0)
        self.assertTrue(not not p1)
        self.assertEqual(hash(p0), hash(0))
        self.assertEqual(p0, p0)
        self.assertNotEqual(p0, p1)
        self.assertTrue(not p0 != p0)
        self.assertEqual(not p0, p1)
        self.assertEqual(cmp(p0, p1), -1)
        self.assertEqual(cmp(p0, p0), 0)
        self.assertEqual(cmp(p0, p_1), 1)
        self.assertEqual(str(p0), 'Proxy:0')
        self.assertEqual(repr(p0), 'Proxy(0)')
        p10 = Proxy(range(10))
        self.assertNotIn(-1, p10)
        for i in range(10):
            self.assertIn(i, p10)
        self.assertNotIn(10, p10)


        class DProxy(object):

            def __init__(self, x):
                Sensor(1116)
                self.x = x
                Sensor(1117)

            def __nonzero__(self):
                py_ass_parser1118 = not not self.x
                Sensor(1118)
                return py_ass_parser1118

            def __hash__(self):
                py_ass_parser1119 = hash(self.x)
                Sensor(1119)
                return py_ass_parser1119

            def __eq__(self, other):
                py_ass_parser1120 = self.x == other
                Sensor(1120)
                return py_ass_parser1120

            def __ne__(self, other):
                py_ass_parser1121 = self.x != other
                Sensor(1121)
                return py_ass_parser1121

            def __cmp__(self, other):
                py_ass_parser1122 = cmp(self.x, other.x)
                Sensor(1122)
                return py_ass_parser1122

            def __str__(self):
                py_ass_parser1123 = 'DProxy:%s' % self.x
                Sensor(1123)
                return py_ass_parser1123

            def __repr__(self):
                py_ass_parser1124 = 'DProxy(%r)' % self.x
                Sensor(1124)
                return py_ass_parser1124

            def __contains__(self, value):
                py_ass_parser1125 = value in self.x
                Sensor(1125)
                return py_ass_parser1125
        p0 = DProxy(0)
        p1 = DProxy(1)
        p_1 = DProxy(-1)
        self.assertFalse(p0)
        self.assertTrue(not not p1)
        self.assertEqual(hash(p0), hash(0))
        self.assertEqual(p0, p0)
        self.assertNotEqual(p0, p1)
        self.assertNotEqual(not p0, p0)
        self.assertEqual(not p0, p1)
        self.assertEqual(cmp(p0, p1), -1)
        self.assertEqual(cmp(p0, p0), 0)
        self.assertEqual(cmp(p0, p_1), 1)
        self.assertEqual(str(p0), 'DProxy:0')
        self.assertEqual(repr(p0), 'DProxy(0)')
        p10 = DProxy(range(10))
        self.assertNotIn(-1, p10)
        for i in range(10):
            self.assertIn(i, p10)
        self.assertNotIn(10, p10)

        def unsafecmp(a, b):
            Sensor(1126)
            if not hasattr(a, '__cmp__'):
                return
            try:
                a.__class__.__cmp__(a, b)
            except TypeError:
                pass
            else:
                self.fail("shouldn't allow %s.__cmp__(%r, %r)" % (a.
                    __class__, a, b))
            Sensor(1127)
        unsafecmp(u'123', '123')
        unsafecmp('123', u'123')
        unsafecmp(1, 1.0)
        unsafecmp(1.0, 1)
        unsafecmp(1, 1L)
        unsafecmp(1L, 1)
        Sensor(1128)

    @test_support.impl_detail('custom logic for printing to real file objects')
    def test_recursions_1(self):


        class Letter(str):

            def __new__(cls, letter):
                Sensor(1129)
                if letter == 'EPS':
                    py_ass_parser1130 = str.__new__(cls)
                    Sensor(1130)
                    return py_ass_parser1130
                py_ass_parser1131 = str.__new__(cls, letter)
                Sensor(1131)
                return py_ass_parser1131

            def __str__(self):
                Sensor(1132)
                if not self:
                    py_ass_parser1133 = 'EPS'
                    Sensor(1133)
                    return py_ass_parser1133
                py_ass_parser1134 = self
                Sensor(1134)
                return py_ass_parser1134
        test_stdout = sys.stdout
        sys.stdout = test_support.get_original_stdout()
        try:
            try:
                print Letter('w')
            except RuntimeError:
                pass
            else:
                self.fail('expected a RuntimeError for print recursion')
        finally:
            sys.stdout = test_stdout
        Sensor(1135)

    def test_recursions_2(self):


        class A(object):
            pass
        Sensor(1136)
        A.__mul__ = types.MethodType(lambda self, x: self * x, None, A)
        try:
            A() * 2
        except RuntimeError:
            pass
        else:
            self.fail('expected a RuntimeError')
        Sensor(1137)

    def test_weakrefs(self):
        import weakref


        class C(object):
            pass
        Sensor(1138)
        c = C()
        r = weakref.ref(c)
        self.assertEqual(r(), c)
        del c
        test_support.gc_collect()
        self.assertEqual(r(), None)
        del r


        class NoWeak(object):
            __slots__ = ['foo']
        no = NoWeak()
        try:
            weakref.ref(no)
        except TypeError as msg:
            self.assertTrue(str(msg).find('weak reference') >= 0)
        else:
            self.fail('weakref.ref(no) should be illegal')


        class Weak(object):
            __slots__ = ['foo', '__weakref__']
        yes = Weak()
        r = weakref.ref(yes)
        self.assertEqual(r(), yes)
        del yes
        test_support.gc_collect()
        self.assertEqual(r(), None)
        del r
        Sensor(1139)

    def test_properties(self):


        class C(object):

            def getx(self):
                py_ass_parser1140 = self.__x
                Sensor(1140)
                return py_ass_parser1140

            def setx(self, value):
                Sensor(1141)
                self.__x = value
                Sensor(1142)

            def delx(self):
                Sensor(1143)
                del self.__x
                Sensor(1144)
            x = property(getx, setx, delx, doc="I'm the x property.")
        Sensor(1145)
        a = C()
        self.assertFalse(hasattr(a, 'x'))
        a.x = 42
        self.assertEqual(a._C__x, 42)
        self.assertEqual(a.x, 42)
        del a.x
        self.assertFalse(hasattr(a, 'x'))
        self.assertFalse(hasattr(a, '_C__x'))
        C.x.__set__(a, 100)
        self.assertEqual(C.x.__get__(a), 100)
        C.x.__delete__(a)
        self.assertFalse(hasattr(a, 'x'))
        raw = C.__dict__['x']
        self.assertIsInstance(raw, property)
        attrs = dir(raw)
        self.assertIn('__doc__', attrs)
        self.assertIn('fget', attrs)
        self.assertIn('fset', attrs)
        self.assertIn('fdel', attrs)
        self.assertEqual(raw.__doc__, "I'm the x property.")
        self.assertTrue(raw.fget is C.__dict__['getx'])
        self.assertTrue(raw.fset is C.__dict__['setx'])
        self.assertTrue(raw.fdel is C.__dict__['delx'])
        for attr in ('__doc__', 'fget', 'fset', 'fdel'):
            try:
                setattr(raw, attr, 42)
            except TypeError as msg:
                if str(msg).find('readonly') < 0:
                    self.fail(
                        'when setting readonly attr %r on a property, got unexpected TypeError msg %r'
                         % (attr, str(msg)))
            else:
                self.fail(
                    'expected TypeError from trying to set readonly %r attr on a property'
                     % attr)


        class D(object):
            __getitem__ = property(lambda s: 1 / 0)
        d = D()
        try:
            for i in d:
                str(i)
        except ZeroDivisionError:
            pass
        else:
            self.fail('expected ZeroDivisionError from bad property')
        Sensor(1146)

    @unittest.skipIf(sys.flags.optimize >= 2,
        'Docstrings are omitted with -O2 and above')
    def test_properties_doc_attrib(self):


        class E(object):

            def getter(self):
                Sensor(1147)
                """getter method"""
                py_ass_parser1148 = 0
                Sensor(1148)
                return py_ass_parser1148

            def setter(self_, value):
                Sensor(1149)
                """setter method"""
                pass
                Sensor(1150)
            prop = property(getter)
            self.assertEqual(prop.__doc__, 'getter method')
            prop2 = property(fset=setter)
            self.assertEqual(prop2.__doc__, None)
        Sensor(1151)

    def test_testcapi_no_segfault(self):
        Sensor(1152)
        try:
            Sensor(1153)
            import _testcapi
        except ImportError:
            pass
        else:
            Sensor(1154)


            class X(object):
                p = property(_testcapi.test_with_docstring)
        Sensor(1155)

    def test_properties_plus(self):


        class C(object):
            foo = property(doc='hello')

            @foo.getter
            def foo(self):
                py_ass_parser1156 = self._foo
                Sensor(1156)
                return py_ass_parser1156

            @foo.setter
            def foo(self, value):
                Sensor(1157)
                self._foo = abs(value)
                Sensor(1158)

            @foo.deleter
            def foo(self):
                Sensor(1159)
                del self._foo
                Sensor(1160)
        Sensor(1161)
        c = C()
        self.assertEqual(C.foo.__doc__, 'hello')
        self.assertFalse(hasattr(c, 'foo'))
        c.foo = -42
        self.assertTrue(hasattr(c, '_foo'))
        self.assertEqual(c._foo, 42)
        self.assertEqual(c.foo, 42)
        del c.foo
        self.assertFalse(hasattr(c, '_foo'))
        self.assertFalse(hasattr(c, 'foo'))


        class D(C):

            @C.foo.deleter
            def foo(self):
                Sensor(1162)
                try:
                    del self._foo
                except AttributeError:
                    pass
                Sensor(1163)
        d = D()
        d.foo = 24
        self.assertEqual(d.foo, 24)
        del d.foo
        del d.foo


        class E(object):

            @property
            def foo(self):
                py_ass_parser1164 = self._foo
                Sensor(1164)
                return py_ass_parser1164

            @foo.setter
            def foo(self, value):
                Sensor(1165)
                raise RuntimeError

            @foo.setter
            def foo(self, value):
                Sensor(1166)
                self._foo = abs(value)
                Sensor(1167)

            @foo.deleter
            def foo(self, value=None):
                Sensor(1168)
                del self._foo
                Sensor(1169)
        e = E()
        e.foo = -42
        self.assertEqual(e.foo, 42)
        del e.foo


        class F(E):

            @E.foo.deleter
            def foo(self):
                Sensor(1170)
                del self._foo
                Sensor(1171)

            @foo.setter
            def foo(self, value):
                Sensor(1172)
                self._foo = max(0, value)
                Sensor(1173)
        f = F()
        f.foo = -10
        self.assertEqual(f.foo, 0)
        del f.foo
        Sensor(1174)

    def test_dict_constructors(self):
        Sensor(1175)
        d = dict()
        self.assertEqual(d, {})
        d = dict({})
        self.assertEqual(d, {})
        d = dict({(1): 2, 'a': 'b'})
        self.assertEqual(d, {(1): 2, 'a': 'b'})
        self.assertEqual(d, dict(d.items()))
        self.assertEqual(d, dict(d.iteritems()))
        d = dict({'one': 1, 'two': 2})
        self.assertEqual(d, dict(one=1, two=2))
        self.assertEqual(d, dict(**d))
        self.assertEqual(d, dict({'one': 1}, two=2))
        self.assertEqual(d, dict([('two', 2)], one=1))
        self.assertEqual(d, dict([('one', 100), ('two', 200)], **d))
        self.assertEqual(d, dict(**d))
        for badarg in (0, 0L, 0j, '0', [0], (0,)):
            try:
                dict(badarg)
            except TypeError:
                pass
            except ValueError:
                if badarg == '0':
                    pass
                else:
                    self.fail('no TypeError from dict(%r)' % badarg)
            else:
                self.fail('no TypeError from dict(%r)' % badarg)
        try:
            dict({}, {})
        except TypeError:
            pass
        else:
            self.fail('no TypeError from dict({}, {})')


        class Mapping:
            dict = {(1): 2, (3): 4, 'a': 1j}
        try:
            dict(Mapping())
        except TypeError:
            pass
        else:
            self.fail('no TypeError from dict(incomplete mapping)')
        Mapping.keys = lambda self: self.dict.keys()
        Mapping.__getitem__ = lambda self, i: self.dict[i]
        d = dict(Mapping())
        self.assertEqual(d, Mapping.dict)


        class AddressBookEntry:

            def __init__(self, first, last):
                Sensor(1176)
                self.first = first
                self.last = last
                Sensor(1177)

            def __iter__(self):
                py_ass_parser1178 = iter([self.first, self.last])
                Sensor(1178)
                return py_ass_parser1178
        d = dict([AddressBookEntry('Tim', 'Warsaw'), AddressBookEntry(
            'Barry', 'Peters'), AddressBookEntry('Tim', 'Peters'),
            AddressBookEntry('Barry', 'Warsaw')])
        self.assertEqual(d, {'Barry': 'Warsaw', 'Tim': 'Peters'})
        d = dict(zip(range(4), range(1, 5)))
        self.assertEqual(d, dict([(i, i + 1) for i in range(4)]))
        for bad in ([('tooshort',)], [('too', 'long', 'by 1')]):
            try:
                dict(bad)
            except ValueError:
                pass
            else:
                self.fail('no ValueError from dict(%r)' % bad)
        Sensor(1179)

    def test_dir(self):
        Sensor(1180)
        junk = 12
        self.assertEqual(dir(), ['junk', 'self'])
        del junk
        for arg in (2, 2L, 2j, 2.0, [2], '2', u'2', (2,), {(2): 2}, type,
            self.test_dir):
            dir(arg)


        class C:
            Cdata = 1

            def Cmethod(self):
                Sensor(1181)
                pass
                Sensor(1182)
        Sensor(1183)
        cstuff = ['Cdata', 'Cmethod', '__doc__', '__module__']
        self.assertEqual(dir(C), cstuff)
        self.assertIn('im_self', dir(C.Cmethod))
        c = C()
        self.assertEqual(dir(c), cstuff)
        c.cdata = 2
        c.cmethod = lambda self: 0
        self.assertEqual(dir(c), cstuff + ['cdata', 'cmethod'])
        self.assertIn('im_self', dir(c.Cmethod))


        class A(C):
            Adata = 1

            def Amethod(self):
                Sensor(1184)
                pass
                Sensor(1185)
        astuff = ['Adata', 'Amethod'] + cstuff
        self.assertEqual(dir(A), astuff)
        self.assertIn('im_self', dir(A.Amethod))
        a = A()
        self.assertEqual(dir(a), astuff)
        self.assertIn('im_self', dir(a.Amethod))
        a.adata = 42
        a.amethod = lambda self: 3
        self.assertEqual(dir(a), astuff + ['adata', 'amethod'])

        def interesting(strings):
            py_ass_parser1186 = [s for s in strings if not s.startswith('_')]
            Sensor(1186)
            return py_ass_parser1186


        class C(object):
            Cdata = 1

            def Cmethod(self):
                Sensor(1187)
                pass
                Sensor(1188)
        cstuff = ['Cdata', 'Cmethod']
        self.assertEqual(interesting(dir(C)), cstuff)
        c = C()
        self.assertEqual(interesting(dir(c)), cstuff)
        self.assertIn('im_self', dir(C.Cmethod))
        c.cdata = 2
        c.cmethod = lambda self: 0
        self.assertEqual(interesting(dir(c)), cstuff + ['cdata', 'cmethod'])
        self.assertIn('im_self', dir(c.Cmethod))


        class A(C):
            Adata = 1

            def Amethod(self):
                Sensor(1189)
                pass
                Sensor(1190)
        astuff = ['Adata', 'Amethod'] + cstuff
        self.assertEqual(interesting(dir(A)), astuff)
        self.assertIn('im_self', dir(A.Amethod))
        a = A()
        self.assertEqual(interesting(dir(a)), astuff)
        a.adata = 42
        a.amethod = lambda self: 3
        self.assertEqual(interesting(dir(a)), astuff + ['adata', 'amethod'])
        self.assertIn('im_self', dir(a.Amethod))


        class M(type(sys)):
            pass
        minstance = M('m')
        minstance.b = 2
        minstance.a = 1
        names = [x for x in dir(minstance) if x not in ['__name__', '__doc__']]
        self.assertEqual(names, ['a', 'b'])


        class M2(M):

            def getdict(self):
                py_ass_parser1191 = 'Not a dict!'
                Sensor(1191)
                return py_ass_parser1191
            __dict__ = property(getdict)
        m2instance = M2('m2')
        m2instance.b = 2
        m2instance.a = 1
        self.assertEqual(m2instance.__dict__, 'Not a dict!')
        try:
            dir(m2instance)
        except TypeError:
            pass
        self.assertEqual(dir(NotImplemented), dir(Ellipsis))
        if test_support.check_impl_detail():
            self.assertEqual(dir(None), dir(Ellipsis))


        class Wrapper(object):

            def __init__(self, obj):
                Sensor(1192)
                self.__obj = obj
                Sensor(1193)

            def __repr__(self):
                py_ass_parser1194 = 'Wrapper(%s)' % repr(self.__obj)
                Sensor(1194)
                return py_ass_parser1194

            def __getitem__(self, key):
                py_ass_parser1195 = Wrapper(self.__obj[key])
                Sensor(1195)
                return py_ass_parser1195

            def __len__(self):
                py_ass_parser1196 = len(self.__obj)
                Sensor(1196)
                return py_ass_parser1196

            def __getattr__(self, name):
                py_ass_parser1197 = Wrapper(getattr(self.__obj, name))
                Sensor(1197)
                return py_ass_parser1197


        class C(object):

            def __getclass(self):
                py_ass_parser1198 = Wrapper(type(self))
                Sensor(1198)
                return py_ass_parser1198
            __class__ = property(__getclass)
        dir(C())
        Sensor(1199)

    def test_supers(self):


        class A(object):

            def meth(self, a):
                py_ass_parser1200 = 'A(%r)' % a
                Sensor(1200)
                return py_ass_parser1200
        self.assertEqual(A().meth(1), 'A(1)')


        class B(A):

            def __init__(self):
                Sensor(1201)
                self.__super = super(B, self)
                Sensor(1202)

            def meth(self, a):
                py_ass_parser1203 = 'B(%r)' % a + self.__super.meth(a)
                Sensor(1203)
                return py_ass_parser1203
        self.assertEqual(B().meth(2), 'B(2)A(2)')


        class C(A):

            def meth(self, a):
                py_ass_parser1204 = 'C(%r)' % a + self.__super.meth(a)
                Sensor(1204)
                return py_ass_parser1204
        C._C__super = super(C)
        self.assertEqual(C().meth(3), 'C(3)A(3)')


        class D(C, B):

            def meth(self, a):
                py_ass_parser1205 = 'D(%r)' % a + super(D, self).meth(a)
                Sensor(1205)
                return py_ass_parser1205
        self.assertEqual(D().meth(4), 'D(4)C(4)B(4)A(4)')


        class mysuper(super):

            def __init__(self, *args):
                py_ass_parser1206 = super(mysuper, self).__init__(*args)
                Sensor(1206)
                return py_ass_parser1206


        class E(D):

            def meth(self, a):
                py_ass_parser1207 = 'E(%r)' % a + mysuper(E, self).meth(a)
                Sensor(1207)
                return py_ass_parser1207
        self.assertEqual(E().meth(5), 'E(5)D(5)C(5)B(5)A(5)')


        class F(E):

            def meth(self, a):
                Sensor(1208)
                s = self.__super
                py_ass_parser1209 = 'F(%r)[%s]' % (a, s.__class__.__name__
                    ) + s.meth(a)
                Sensor(1209)
                return py_ass_parser1209
        F._F__super = mysuper(F)
        self.assertEqual(F().meth(6), 'F(6)[mysuper]E(6)D(6)C(6)B(6)A(6)')
        try:
            super(D, 42)
        except TypeError:
            pass
        else:
            self.fail("shouldn't allow super(D, 42)")
        try:
            super(D, C())
        except TypeError:
            pass
        else:
            self.fail("shouldn't allow super(D, C())")
        try:
            super(D).__get__(12)
        except TypeError:
            pass
        else:
            self.fail("shouldn't allow super(D).__get__(12)")
        try:
            super(D).__get__(C())
        except TypeError:
            pass
        else:
            self.fail("shouldn't allow super(D).__get__(C())")


        class DDbase(object):

            def getx(self):
                py_ass_parser1210 = 42
                Sensor(1210)
                return py_ass_parser1210
            x = property(getx)


        class DDsub(DDbase):

            def getx(self):
                py_ass_parser1211 = 'hello'
                Sensor(1211)
                return py_ass_parser1211
            x = property(getx)
        dd = DDsub()
        self.assertEqual(dd.x, 'hello')
        self.assertEqual(super(DDsub, dd).x, 42)


        class Base(object):
            aProp = property(lambda self: 'foo')


        class Sub(Base):

            @classmethod
            def test(klass):
                py_ass_parser1212 = super(Sub, klass).aProp
                Sensor(1212)
                return py_ass_parser1212
        self.assertEqual(Sub.test(), Base.aProp)
        try:
            super(Base, kw=1)
        except TypeError:
            pass
        else:
            self.assertEqual("super shouldn't accept keyword args")
        Sensor(1213)

    def test_basic_inheritance(self):


        class hexint(int):

            def __repr__(self):
                py_ass_parser1214 = hex(self)
                Sensor(1214)
                return py_ass_parser1214

            def __add__(self, other):
                py_ass_parser1215 = hexint(int.__add__(self, other))
                Sensor(1215)
                return py_ass_parser1215
        self.assertEqual(repr(hexint(7) + 9), '0x10')
        self.assertEqual(repr(hexint(1000) + 7), '0x3ef')
        a = hexint(12345)
        self.assertEqual(a, 12345)
        self.assertEqual(int(a), 12345)
        self.assertTrue(int(a).__class__ is int)
        self.assertEqual(hash(a), hash(12345))
        self.assertTrue((+a).__class__ is int)
        self.assertTrue((a >> 0).__class__ is int)
        self.assertTrue((a << 0).__class__ is int)
        self.assertTrue((hexint(0) << 12).__class__ is int)
        self.assertTrue((hexint(0) >> 12).__class__ is int)


        class octlong(long):
            __slots__ = []

            def __str__(self):
                Sensor(1216)
                s = oct(self)
                if s[-1] == 'L':
                    s = s[:-1]
                py_ass_parser1217 = s
                Sensor(1217)
                return py_ass_parser1217

            def __add__(self, other):
                py_ass_parser1218 = self.__class__(super(octlong, self).
                    __add__(other))
                Sensor(1218)
                return py_ass_parser1218
            __radd__ = __add__
        self.assertEqual(str(octlong(3) + 5), '010')
        self.assertEqual(str(5 + octlong(3000)), '05675')
        a = octlong(12345)
        self.assertEqual(a, 12345L)
        self.assertEqual(long(a), 12345L)
        self.assertEqual(hash(a), hash(12345L))
        self.assertTrue(long(a).__class__ is long)
        self.assertTrue((+a).__class__ is long)
        self.assertTrue((-a).__class__ is long)
        self.assertTrue((-octlong(0)).__class__ is long)
        self.assertTrue((a >> 0).__class__ is long)
        self.assertTrue((a << 0).__class__ is long)
        self.assertTrue((a - 0).__class__ is long)
        self.assertTrue((a * 1).__class__ is long)
        self.assertTrue((a ** 1).__class__ is long)
        self.assertTrue((a // 1).__class__ is long)
        self.assertTrue((1 * a).__class__ is long)
        self.assertTrue((a | 0).__class__ is long)
        self.assertTrue((a ^ 0).__class__ is long)
        self.assertTrue((a & -1L).__class__ is long)
        self.assertTrue((octlong(0) << 12).__class__ is long)
        self.assertTrue((octlong(0) >> 12).__class__ is long)
        self.assertTrue(abs(octlong(0)).__class__ is long)


        class longclone(long):
            pass
        a = longclone(1)
        self.assertTrue((a + 0).__class__ is long)
        self.assertTrue((0 + a).__class__ is long)
        a = longclone(-1)
        self.assertEqual(a.__dict__, {})
        self.assertEqual(long(a), -1)


        class precfloat(float):
            __slots__ = ['prec']

            def __init__(self, value=0.0, prec=12):
                Sensor(1219)
                self.prec = int(prec)
                Sensor(1220)

            def __repr__(self):
                py_ass_parser1221 = '%.*g' % (self.prec, self)
                Sensor(1221)
                return py_ass_parser1221
        self.assertEqual(repr(precfloat(1.1)), '1.1')
        a = precfloat(12345)
        self.assertEqual(a, 12345.0)
        self.assertEqual(float(a), 12345.0)
        self.assertTrue(float(a).__class__ is float)
        self.assertEqual(hash(a), hash(12345.0))
        self.assertTrue((+a).__class__ is float)


        class madcomplex(complex):

            def __repr__(self):
                py_ass_parser1222 = '%.17gj%+.17g' % (self.imag, self.real)
                Sensor(1222)
                return py_ass_parser1222
        a = madcomplex(-3, 4)
        self.assertEqual(repr(a), '4j-3')
        base = complex(-3, 4)
        self.assertEqual(base.__class__, complex)
        self.assertEqual(a, base)
        self.assertEqual(complex(a), base)
        self.assertEqual(complex(a).__class__, complex)
        a = madcomplex(a)
        self.assertEqual(repr(a), '4j-3')
        self.assertEqual(a, base)
        self.assertEqual(complex(a), base)
        self.assertEqual(complex(a).__class__, complex)
        self.assertEqual(hash(a), hash(base))
        self.assertEqual((+a).__class__, complex)
        self.assertEqual((a + 0).__class__, complex)
        self.assertEqual(a + 0, base)
        self.assertEqual((a - 0).__class__, complex)
        self.assertEqual(a - 0, base)
        self.assertEqual((a * 1).__class__, complex)
        self.assertEqual(a * 1, base)
        self.assertEqual((a / 1).__class__, complex)
        self.assertEqual(a / 1, base)


        class madtuple(tuple):
            _rev = None

            def rev(self):
                Sensor(1223)
                if self._rev is not None:
                    py_ass_parser1224 = self._rev
                    Sensor(1224)
                    return py_ass_parser1224
                L = list(self)
                L.reverse()
                self._rev = self.__class__(L)
                py_ass_parser1225 = self._rev
                Sensor(1225)
                return py_ass_parser1225
        a = madtuple((1, 2, 3, 4, 5, 6, 7, 8, 9, 0))
        self.assertEqual(a, (1, 2, 3, 4, 5, 6, 7, 8, 9, 0))
        self.assertEqual(a.rev(), madtuple((0, 9, 8, 7, 6, 5, 4, 3, 2, 1)))
        self.assertEqual(a.rev().rev(), madtuple((1, 2, 3, 4, 5, 6, 7, 8, 9,
            0)))
        for i in range(512):
            t = madtuple(range(i))
            u = t.rev()
            v = u.rev()
            self.assertEqual(v, t)
        a = madtuple((1, 2, 3, 4, 5))
        self.assertEqual(tuple(a), (1, 2, 3, 4, 5))
        self.assertTrue(tuple(a).__class__ is tuple)
        self.assertEqual(hash(a), hash((1, 2, 3, 4, 5)))
        self.assertTrue(a[:].__class__ is tuple)
        self.assertTrue((a * 1).__class__ is tuple)
        self.assertTrue((a * 0).__class__ is tuple)
        self.assertTrue((a + ()).__class__ is tuple)
        a = madtuple(())
        self.assertEqual(tuple(a), ())
        self.assertTrue(tuple(a).__class__ is tuple)
        self.assertTrue((a + a).__class__ is tuple)
        self.assertTrue((a * 0).__class__ is tuple)
        self.assertTrue((a * 1).__class__ is tuple)
        self.assertTrue((a * 2).__class__ is tuple)
        self.assertTrue(a[:].__class__ is tuple)


        class madstring(str):
            _rev = None

            def rev(self):
                Sensor(1226)
                if self._rev is not None:
                    py_ass_parser1227 = self._rev
                    Sensor(1227)
                    return py_ass_parser1227
                L = list(self)
                L.reverse()
                self._rev = self.__class__(''.join(L))
                py_ass_parser1228 = self._rev
                Sensor(1228)
                return py_ass_parser1228
        s = madstring('abcdefghijklmnopqrstuvwxyz')
        self.assertEqual(s, 'abcdefghijklmnopqrstuvwxyz')
        self.assertEqual(s.rev(), madstring('zyxwvutsrqponmlkjihgfedcba'))
        self.assertEqual(s.rev().rev(), madstring('abcdefghijklmnopqrstuvwxyz')
            )
        for i in range(256):
            s = madstring(''.join(map(chr, range(i))))
            t = s.rev()
            u = t.rev()
            self.assertEqual(u, s)
        s = madstring('12345')
        self.assertEqual(str(s), '12345')
        self.assertTrue(str(s).__class__ is str)
        base = '\x00' * 5
        s = madstring(base)
        self.assertEqual(s, base)
        self.assertEqual(str(s), base)
        self.assertTrue(str(s).__class__ is str)
        self.assertEqual(hash(s), hash(base))
        self.assertEqual({s: 1}[base], 1)
        self.assertEqual({base: 1}[s], 1)
        self.assertTrue((s + '').__class__ is str)
        self.assertEqual(s + '', base)
        self.assertTrue(('' + s).__class__ is str)
        self.assertEqual('' + s, base)
        self.assertTrue((s * 0).__class__ is str)
        self.assertEqual(s * 0, '')
        self.assertTrue((s * 1).__class__ is str)
        self.assertEqual(s * 1, base)
        self.assertTrue((s * 2).__class__ is str)
        self.assertEqual(s * 2, base + base)
        self.assertTrue(s[:].__class__ is str)
        self.assertEqual(s[:], base)
        self.assertTrue(s[0:0].__class__ is str)
        self.assertEqual(s[0:0], '')
        self.assertTrue(s.strip().__class__ is str)
        self.assertEqual(s.strip(), base)
        self.assertTrue(s.lstrip().__class__ is str)
        self.assertEqual(s.lstrip(), base)
        self.assertTrue(s.rstrip().__class__ is str)
        self.assertEqual(s.rstrip(), base)
        identitytab = ''.join([chr(i) for i in range(256)])
        self.assertTrue(s.translate(identitytab).__class__ is str)
        self.assertEqual(s.translate(identitytab), base)
        self.assertTrue(s.translate(identitytab, 'x').__class__ is str)
        self.assertEqual(s.translate(identitytab, 'x'), base)
        self.assertEqual(s.translate(identitytab, '\x00'), '')
        self.assertTrue(s.replace('x', 'x').__class__ is str)
        self.assertEqual(s.replace('x', 'x'), base)
        self.assertTrue(s.ljust(len(s)).__class__ is str)
        self.assertEqual(s.ljust(len(s)), base)
        self.assertTrue(s.rjust(len(s)).__class__ is str)
        self.assertEqual(s.rjust(len(s)), base)
        self.assertTrue(s.center(len(s)).__class__ is str)
        self.assertEqual(s.center(len(s)), base)
        self.assertTrue(s.lower().__class__ is str)
        self.assertEqual(s.lower(), base)


        class madunicode(unicode):
            _rev = None

            def rev(self):
                Sensor(1229)
                if self._rev is not None:
                    py_ass_parser1230 = self._rev
                    Sensor(1230)
                    return py_ass_parser1230
                L = list(self)
                L.reverse()
                self._rev = self.__class__(u''.join(L))
                py_ass_parser1231 = self._rev
                Sensor(1231)
                return py_ass_parser1231
        u = madunicode('ABCDEF')
        self.assertEqual(u, u'ABCDEF')
        self.assertEqual(u.rev(), madunicode(u'FEDCBA'))
        self.assertEqual(u.rev().rev(), madunicode(u'ABCDEF'))
        base = u'12345'
        u = madunicode(base)
        self.assertEqual(unicode(u), base)
        self.assertTrue(unicode(u).__class__ is unicode)
        self.assertEqual(hash(u), hash(base))
        self.assertEqual({u: 1}[base], 1)
        self.assertEqual({base: 1}[u], 1)
        self.assertTrue(u.strip().__class__ is unicode)
        self.assertEqual(u.strip(), base)
        self.assertTrue(u.lstrip().__class__ is unicode)
        self.assertEqual(u.lstrip(), base)
        self.assertTrue(u.rstrip().__class__ is unicode)
        self.assertEqual(u.rstrip(), base)
        self.assertTrue(u.replace(u'x', u'x').__class__ is unicode)
        self.assertEqual(u.replace(u'x', u'x'), base)
        self.assertTrue(u.replace(u'xy', u'xy').__class__ is unicode)
        self.assertEqual(u.replace(u'xy', u'xy'), base)
        self.assertTrue(u.center(len(u)).__class__ is unicode)
        self.assertEqual(u.center(len(u)), base)
        self.assertTrue(u.ljust(len(u)).__class__ is unicode)
        self.assertEqual(u.ljust(len(u)), base)
        self.assertTrue(u.rjust(len(u)).__class__ is unicode)
        self.assertEqual(u.rjust(len(u)), base)
        self.assertTrue(u.lower().__class__ is unicode)
        self.assertEqual(u.lower(), base)
        self.assertTrue(u.upper().__class__ is unicode)
        self.assertEqual(u.upper(), base)
        self.assertTrue(u.capitalize().__class__ is unicode)
        self.assertEqual(u.capitalize(), base)
        self.assertTrue(u.title().__class__ is unicode)
        self.assertEqual(u.title(), base)
        self.assertTrue((u + u'').__class__ is unicode)
        self.assertEqual(u + u'', base)
        self.assertTrue((u'' + u).__class__ is unicode)
        self.assertEqual(u'' + u, base)
        self.assertTrue((u * 0).__class__ is unicode)
        self.assertEqual(u * 0, u'')
        self.assertTrue((u * 1).__class__ is unicode)
        self.assertEqual(u * 1, base)
        self.assertTrue((u * 2).__class__ is unicode)
        self.assertEqual(u * 2, base + base)
        self.assertTrue(u[:].__class__ is unicode)
        self.assertEqual(u[:], base)
        self.assertTrue(u[0:0].__class__ is unicode)
        self.assertEqual(u[0:0], u'')


        class sublist(list):
            pass
        a = sublist(range(5))
        self.assertEqual(a, range(5))
        a.append('hello')
        self.assertEqual(a, range(5) + ['hello'])
        a[5] = 5
        self.assertEqual(a, range(6))
        a.extend(range(6, 20))
        self.assertEqual(a, range(20))
        a[-5:] = []
        self.assertEqual(a, range(15))
        del a[10:15]
        self.assertEqual(len(a), 10)
        self.assertEqual(a, range(10))
        self.assertEqual(list(a), range(10))
        self.assertEqual(a[0], 0)
        self.assertEqual(a[9], 9)
        self.assertEqual(a[-10], 0)
        self.assertEqual(a[-1], 9)
        self.assertEqual(a[:5], range(5))


        class CountedInput(file):
            """Counts lines read by self.readline().

            self.lineno is the 0-based ordinal of the last line read, up to
            a maximum of one greater than the number of lines in the file.

            self.ateof is true if and only if the final "" line has been read,
            at which point self.lineno stops incrementing, and further calls
            to readline() continue to return "".
            """
            lineno = 0
            ateof = 0

            def readline(self):
                Sensor(1232)
                if self.ateof:
                    py_ass_parser1233 = ''
                    Sensor(1233)
                    return py_ass_parser1233
                s = file.readline(self)
                self.lineno += 1
                if s == '':
                    self.ateof = 1
                py_ass_parser1234 = s
                Sensor(1234)
                return py_ass_parser1234
        f = file(name=test_support.TESTFN, mode='w')
        lines = ['a\n', 'b\n', 'c\n']
        try:
            f.writelines(lines)
            f.close()
            f = CountedInput(test_support.TESTFN)
            for i, expected in zip(range(1, 5) + [4], lines + 2 * ['']):
                got = f.readline()
                self.assertEqual(expected, got)
                self.assertEqual(f.lineno, i)
                self.assertEqual(f.ateof, i > len(lines))
            f.close()
        finally:
            try:
                f.close()
            except:
                pass
            test_support.unlink(test_support.TESTFN)
        Sensor(1235)

    def test_keywords(self):
        Sensor(1236)
        self.assertEqual(int(x=1), 1)
        self.assertEqual(float(x=2), 2.0)
        self.assertEqual(long(x=3), 3L)
        self.assertEqual(complex(imag=42, real=666), complex(666, 42))
        self.assertEqual(str(object=500), '500')
        self.assertEqual(unicode(string='abc', errors='strict'), u'abc')
        self.assertEqual(tuple(sequence=range(3)), (0, 1, 2))
        self.assertEqual(list(sequence=(0, 1, 2)), range(3))
        for constructor in (int, float, long, complex, str, unicode, tuple,
            list, file):
            try:
                constructor(bogus_keyword_arg=1)
            except TypeError:
                pass
            else:
                self.fail(
                    'expected TypeError from bogus keyword argument to %r' %
                    constructor)
        Sensor(1237)

    def test_str_subclass_as_dict_key(self):


        class cistr(str):
            """Sublcass of str that computes __eq__ case-insensitively.

            Also computes a hash code of the string in canonical form.
            """

            def __init__(self, value):
                Sensor(1238)
                self.canonical = value.lower()
                self.hashcode = hash(self.canonical)
                Sensor(1239)

            def __eq__(self, other):
                Sensor(1240)
                if not isinstance(other, cistr):
                    other = cistr(other)
                py_ass_parser1241 = self.canonical == other.canonical
                Sensor(1241)
                return py_ass_parser1241

            def __hash__(self):
                py_ass_parser1242 = self.hashcode
                Sensor(1242)
                return py_ass_parser1242
        self.assertEqual(cistr('ABC'), 'abc')
        self.assertEqual('aBc', cistr('ABC'))
        self.assertEqual(str(cistr('ABC')), 'ABC')
        d = {cistr('one'): 1, cistr('two'): 2, cistr('tHree'): 3}
        self.assertEqual(d[cistr('one')], 1)
        self.assertEqual(d[cistr('tWo')], 2)
        self.assertEqual(d[cistr('THrEE')], 3)
        self.assertIn(cistr('ONe'), d)
        self.assertEqual(d.get(cistr('thrEE')), 3)
        Sensor(1243)

    def test_classic_comparisons(self):


        class classic:
            pass
        Sensor(1244)
        for base in (classic, int, object):


            class C(base):

                def __init__(self, value):
                    Sensor(1245)
                    self.value = int(value)
                    Sensor(1246)

                def __cmp__(self, other):
                    Sensor(1247)
                    if isinstance(other, C):
                        py_ass_parser1248 = cmp(self.value, other.value)
                        Sensor(1248)
                        return py_ass_parser1248
                    if isinstance(other, int) or isinstance(other, long):
                        py_ass_parser1249 = cmp(self.value, other)
                        Sensor(1249)
                        return py_ass_parser1249
                    py_ass_parser1250 = NotImplemented
                    Sensor(1250)
                    return py_ass_parser1250
                __hash__ = None
            c1 = C(1)
            c2 = C(2)
            c3 = C(3)
            self.assertEqual(c1, 1)
            c = {(1): c1, (2): c2, (3): c3}
            for x in (1, 2, 3):
                for y in (1, 2, 3):
                    self.assertTrue(cmp(c[x], c[y]) == cmp(x, y), 
                        'x=%d, y=%d' % (x, y))
                    for op in ('<', '<=', '==', '!=', '>', '>='):
                        self.assertTrue(eval('c[x] %s c[y]' % op) == eval(
                            'x %s y' % op), 'x=%d, y=%d' % (x, y))
                    self.assertTrue(cmp(c[x], y) == cmp(x, y), 'x=%d, y=%d' %
                        (x, y))
                    self.assertTrue(cmp(x, c[y]) == cmp(x, y), 'x=%d, y=%d' %
                        (x, y))
        Sensor(1251)

    def test_rich_comparisons(self):


        class Z(complex):
            pass
        Sensor(1252)
        z = Z(1)
        self.assertEqual(z, 1 + 0j)
        self.assertEqual(1 + 0j, z)


        class ZZ(complex):

            def __eq__(self, other):
                Sensor(1253)
                try:
                    py_ass_parser1254 = abs(self - other) <= 1e-06
                    Sensor(1254)
                    return py_ass_parser1254
                except:
                    py_ass_parser1255 = NotImplemented
                    Sensor(1255)
                    return py_ass_parser1255
                Sensor(1256)
            __hash__ = None
        zz = ZZ(1.0000003)
        self.assertEqual(zz, 1 + 0j)
        self.assertEqual(1 + 0j, zz)


        class classic:
            pass
        Sensor(1257)
        for base in (classic, int, object, list):


            class C(base):

                def __init__(self, value):
                    Sensor(1258)
                    self.value = int(value)
                    Sensor(1259)

                def __cmp__(self_, other):
                    Sensor(1260)
                    self.fail("shouldn't call __cmp__")
                    Sensor(1261)
                __hash__ = None

                def __eq__(self, other):
                    Sensor(1262)
                    if isinstance(other, C):
                        py_ass_parser1263 = self.value == other.value
                        Sensor(1263)
                        return py_ass_parser1263
                    if isinstance(other, int) or isinstance(other, long):
                        py_ass_parser1264 = self.value == other
                        Sensor(1264)
                        return py_ass_parser1264
                    py_ass_parser1265 = NotImplemented
                    Sensor(1265)
                    return py_ass_parser1265

                def __ne__(self, other):
                    Sensor(1266)
                    if isinstance(other, C):
                        py_ass_parser1267 = self.value != other.value
                        Sensor(1267)
                        return py_ass_parser1267
                    if isinstance(other, int) or isinstance(other, long):
                        py_ass_parser1268 = self.value != other
                        Sensor(1268)
                        return py_ass_parser1268
                    py_ass_parser1269 = NotImplemented
                    Sensor(1269)
                    return py_ass_parser1269

                def __lt__(self, other):
                    Sensor(1270)
                    if isinstance(other, C):
                        py_ass_parser1271 = self.value < other.value
                        Sensor(1271)
                        return py_ass_parser1271
                    if isinstance(other, int) or isinstance(other, long):
                        py_ass_parser1272 = self.value < other
                        Sensor(1272)
                        return py_ass_parser1272
                    py_ass_parser1273 = NotImplemented
                    Sensor(1273)
                    return py_ass_parser1273

                def __le__(self, other):
                    Sensor(1274)
                    if isinstance(other, C):
                        py_ass_parser1275 = self.value <= other.value
                        Sensor(1275)
                        return py_ass_parser1275
                    if isinstance(other, int) or isinstance(other, long):
                        py_ass_parser1276 = self.value <= other
                        Sensor(1276)
                        return py_ass_parser1276
                    py_ass_parser1277 = NotImplemented
                    Sensor(1277)
                    return py_ass_parser1277

                def __gt__(self, other):
                    Sensor(1278)
                    if isinstance(other, C):
                        py_ass_parser1279 = self.value > other.value
                        Sensor(1279)
                        return py_ass_parser1279
                    if isinstance(other, int) or isinstance(other, long):
                        py_ass_parser1280 = self.value > other
                        Sensor(1280)
                        return py_ass_parser1280
                    py_ass_parser1281 = NotImplemented
                    Sensor(1281)
                    return py_ass_parser1281

                def __ge__(self, other):
                    Sensor(1282)
                    if isinstance(other, C):
                        py_ass_parser1283 = self.value >= other.value
                        Sensor(1283)
                        return py_ass_parser1283
                    if isinstance(other, int) or isinstance(other, long):
                        py_ass_parser1284 = self.value >= other
                        Sensor(1284)
                        return py_ass_parser1284
                    py_ass_parser1285 = NotImplemented
                    Sensor(1285)
                    return py_ass_parser1285
            c1 = C(1)
            c2 = C(2)
            c3 = C(3)
            self.assertEqual(c1, 1)
            c = {(1): c1, (2): c2, (3): c3}
            for x in (1, 2, 3):
                for y in (1, 2, 3):
                    for op in ('<', '<=', '==', '!=', '>', '>='):
                        self.assertTrue(eval('c[x] %s c[y]' % op) == eval(
                            'x %s y' % op), 'x=%d, y=%d' % (x, y))
                        self.assertTrue(eval('c[x] %s y' % op) == eval(
                            'x %s y' % op), 'x=%d, y=%d' % (x, y))
                        self.assertTrue(eval('x %s c[y]' % op) == eval(
                            'x %s y' % op), 'x=%d, y=%d' % (x, y))
        Sensor(1286)

    def test_coercions(self):


        class I(int):
            pass
        Sensor(1287)
        coerce(I(0), 0)
        coerce(0, I(0))


        class L(long):
            pass
        coerce(L(0), 0)
        coerce(L(0), 0L)
        coerce(0, L(0))
        coerce(0L, L(0))


        class F(float):
            pass
        coerce(F(0), 0)
        coerce(F(0), 0L)
        coerce(F(0), 0.0)
        coerce(0, F(0))
        coerce(0L, F(0))
        coerce(0.0, F(0))


        class C(complex):
            pass
        coerce(C(0), 0)
        coerce(C(0), 0L)
        coerce(C(0), 0.0)
        coerce(C(0), 0j)
        coerce(0, C(0))
        coerce(0L, C(0))
        coerce(0.0, C(0))
        coerce(0j, C(0))
        Sensor(1288)

    def test_descrdoc(self):

        def check(descr, what):
            Sensor(1289)
            self.assertEqual(descr.__doc__, what)
            Sensor(1290)
        Sensor(1291)
        check(file.closed, 'True if the file is closed')
        check(file.name, 'file name')
        Sensor(1292)

    def test_doc_descriptor(self):


        class DocDescr(object):

            def __get__(self, object, otype):
                Sensor(1293)
                if object:
                    object = object.__class__.__name__ + ' instance'
                if otype:
                    otype = otype.__name__
                py_ass_parser1294 = 'object=%s; type=%s' % (object, otype)
                Sensor(1294)
                return py_ass_parser1294


        class OldClass:
            __doc__ = DocDescr()


        class NewClass(object):
            __doc__ = DocDescr()
        self.assertEqual(OldClass.__doc__, 'object=None; type=OldClass')
        self.assertEqual(OldClass().__doc__,
            'object=OldClass instance; type=OldClass')
        self.assertEqual(NewClass.__doc__, 'object=None; type=NewClass')
        self.assertEqual(NewClass().__doc__,
            'object=NewClass instance; type=NewClass')
        Sensor(1295)

    def test_set_class(self):


        class C(object):
            pass


        class D(object):
            pass


        class E(object):
            pass


        class F(D, E):
            pass
        Sensor(1296)
        for cls in (C, D, E, F):
            for cls2 in (C, D, E, F):
                x = cls()
                x.__class__ = cls2
                self.assertTrue(x.__class__ is cls2)
                x.__class__ = cls
                self.assertTrue(x.__class__ is cls)

        def cant(x, C):
            Sensor(1297)
            try:
                x.__class__ = C
            except TypeError:
                pass
            else:
                self.fail("shouldn't allow %r.__class__ = %r" % (x, C))
            try:
                delattr(x, '__class__')
            except (TypeError, AttributeError):
                pass
            else:
                self.fail("shouldn't allow del %r.__class__" % x)
            Sensor(1298)
        Sensor(1299)
        cant(C(), list)
        cant(list(), C)
        cant(C(), 1)
        cant(C(), object)
        cant(object(), list)
        cant(list(), object)


        class Int(int):
            __slots__ = []
        cant(2, Int)
        cant(Int(), int)
        cant(True, int)
        cant(2, bool)
        o = object()
        cant(o, type(1))
        cant(o, type(None))
        del o


        class G(object):
            __slots__ = ['a', 'b']


        class H(object):
            __slots__ = ['b', 'a']
        try:
            unicode
        except NameError:
            Sensor(1300)


            class I(object):
                __slots__ = ['a', 'b']
        else:
            Sensor(1301)


            class I(object):
                __slots__ = [unicode('a'), unicode('b')]


        class J(object):
            __slots__ = ['c', 'b']


        class K(object):
            __slots__ = ['a', 'b', 'd']


        class L(H):
            __slots__ = ['e']


        class M(I):
            __slots__ = ['e']


        class N(J):
            __slots__ = ['__weakref__']


        class P(J):
            __slots__ = ['__dict__']


        class Q(J):
            pass


        class R(J):
            __slots__ = ['__dict__', '__weakref__']
        for cls, cls2 in ((G, H), (G, I), (I, H), (Q, R), (R, Q)):
            x = cls()
            x.a = 1
            x.__class__ = cls2
            self.assertTrue(x.__class__ is cls2, 
                'assigning %r as __class__ for %r silently failed' % (cls2, x))
            self.assertEqual(x.a, 1)
            x.__class__ = cls
            self.assertTrue(x.__class__ is cls, 
                'assigning %r as __class__ for %r silently failed' % (cls, x))
            self.assertEqual(x.a, 1)
        for cls in (G, J, K, L, M, N, P, R, list, Int):
            for cls2 in (G, J, K, L, M, N, P, R, list, Int):
                if cls is cls2:
                    continue
                cant(cls(), cls2)


        class O(object):
            pass


        class A(object):

            def __del__(self):
                Sensor(1302)
                self.__class__ = O
                Sensor(1303)
        Sensor(1304)
        l = [A() for x in range(100)]
        del l
        Sensor(1305)

    def test_set_dict(self):


        class C(object):
            pass
        Sensor(1306)
        a = C()
        a.__dict__ = {'b': 1}
        self.assertEqual(a.b, 1)

        def cant(x, dict):
            Sensor(1307)
            try:
                x.__dict__ = dict
            except (AttributeError, TypeError):
                pass
            else:
                self.fail("shouldn't allow %r.__dict__ = %r" % (x, dict))
            Sensor(1308)
        cant(a, None)
        cant(a, [])
        cant(a, 1)
        del a.__dict__


        class Base(object):
            pass

        def verify_dict_readonly(x):
            Sensor(1309)
            """
            x has to be an instance of a class inheriting from Base.
            """
            cant(x, {})
            try:
                del x.__dict__
            except (AttributeError, TypeError):
                pass
            else:
                self.fail("shouldn't allow del %r.__dict__" % x)
            dict_descr = Base.__dict__['__dict__']
            try:
                dict_descr.__set__(x, {})
            except (AttributeError, TypeError):
                pass
            else:
                self.fail("dict_descr allowed access to %r's dict" % x)
            Sensor(1310)


        class Meta1(type, Base):
            pass


        class Meta2(Base, type):
            pass


        class D(object):
            __metaclass__ = Meta1


        class E(object):
            __metaclass__ = Meta2
        Sensor(1311)
        for cls in (C, D, E):
            verify_dict_readonly(cls)
            class_dict = cls.__dict__
            try:
                class_dict['spam'] = 'eggs'
            except TypeError:
                pass
            else:
                self.fail("%r's __dict__ can be modified" % cls)


        class Module1(types.ModuleType, Base):
            pass


        class Module2(Base, types.ModuleType):
            pass
        for ModuleType in (Module1, Module2):
            mod = ModuleType('spam')
            verify_dict_readonly(mod)
            mod.__dict__['spam'] = 'eggs'

        def can_delete_dict(e):
            Sensor(1312)
            try:
                del e.__dict__
            except (TypeError, AttributeError):
                py_ass_parser1313 = False
                Sensor(1313)
                return py_ass_parser1313
            else:
                py_ass_parser1314 = True
                Sensor(1314)
                return py_ass_parser1314
            Sensor(1315)


        class Exception1(Exception, Base):
            pass


        class Exception2(Base, Exception):
            pass
        Sensor(1316)
        for ExceptionType in (Exception, Exception1, Exception2):
            e = ExceptionType()
            e.__dict__ = {'a': 1}
            self.assertEqual(e.a, 1)
            self.assertEqual(can_delete_dict(e), can_delete_dict(ValueError()))
        Sensor(1317)

    def test_pickles(self):
        import pickle, cPickle

        def sorteditems(d):
            Sensor(1318)
            L = d.items()
            L.sort()
            py_ass_parser1319 = L
            Sensor(1319)
            return py_ass_parser1319
        global C


        class C(object):

            def __init__(self, a, b):
                Sensor(1320)
                super(C, self).__init__()
                self.a = a
                self.b = b
                Sensor(1321)

            def __repr__(self):
                py_ass_parser1322 = 'C(%r, %r)' % (self.a, self.b)
                Sensor(1322)
                return py_ass_parser1322
        global C1


        class C1(list):

            def __new__(cls, a, b):
                py_ass_parser1323 = super(C1, cls).__new__(cls)
                Sensor(1323)
                return py_ass_parser1323

            def __getnewargs__(self):
                py_ass_parser1324 = self.a, self.b
                Sensor(1324)
                return py_ass_parser1324

            def __init__(self, a, b):
                Sensor(1325)
                self.a = a
                self.b = b
                Sensor(1326)

            def __repr__(self):
                py_ass_parser1327 = 'C1(%r, %r)<%r>' % (self.a, self.b,
                    list(self))
                Sensor(1327)
                return py_ass_parser1327
        global C2


        class C2(int):

            def __new__(cls, a, b, val=0):
                py_ass_parser1328 = super(C2, cls).__new__(cls, val)
                Sensor(1328)
                return py_ass_parser1328

            def __getnewargs__(self):
                py_ass_parser1329 = self.a, self.b, int(self)
                Sensor(1329)
                return py_ass_parser1329

            def __init__(self, a, b, val=0):
                Sensor(1330)
                self.a = a
                self.b = b
                Sensor(1331)

            def __repr__(self):
                py_ass_parser1332 = 'C2(%r, %r)<%r>' % (self.a, self.b, int
                    (self))
                Sensor(1332)
                return py_ass_parser1332
        global C3


        class C3(object):

            def __init__(self, foo):
                Sensor(1333)
                self.foo = foo
                Sensor(1334)

            def __getstate__(self):
                py_ass_parser1335 = self.foo
                Sensor(1335)
                return py_ass_parser1335

            def __setstate__(self, foo):
                Sensor(1336)
                self.foo = foo
                Sensor(1337)
        global C4classic, C4


        class C4classic:
            pass


        class C4(C4classic, object):
            pass
        Sensor(1338)
        for p in (pickle, cPickle):
            for bin in (0, 1):
                for cls in (C, C1, C2):
                    s = p.dumps(cls, bin)
                    cls2 = p.loads(s)
                    self.assertTrue(cls2 is cls)
                a = C1(1, 2)
                a.append(42)
                a.append(24)
                b = C2('hello', 'world', 42)
                s = p.dumps((a, b), bin)
                x, y = p.loads(s)
                self.assertEqual(x.__class__, a.__class__)
                self.assertEqual(sorteditems(x.__dict__), sorteditems(a.
                    __dict__))
                self.assertEqual(y.__class__, b.__class__)
                self.assertEqual(sorteditems(y.__dict__), sorteditems(b.
                    __dict__))
                self.assertEqual(repr(x), repr(a))
                self.assertEqual(repr(y), repr(b))
                u = C3(42)
                s = p.dumps(u, bin)
                v = p.loads(s)
                self.assertEqual(u.__class__, v.__class__)
                self.assertEqual(u.foo, v.foo)
                u = C4()
                u.foo = 42
                s = p.dumps(u, bin)
                v = p.loads(s)
                self.assertEqual(u.__class__, v.__class__)
                self.assertEqual(u.foo, v.foo)
        import copy
        for cls in (C, C1, C2):
            cls2 = copy.deepcopy(cls)
            self.assertTrue(cls2 is cls)
        a = C1(1, 2)
        a.append(42)
        a.append(24)
        b = C2('hello', 'world', 42)
        x, y = copy.deepcopy((a, b))
        self.assertEqual(x.__class__, a.__class__)
        self.assertEqual(sorteditems(x.__dict__), sorteditems(a.__dict__))
        self.assertEqual(y.__class__, b.__class__)
        self.assertEqual(sorteditems(y.__dict__), sorteditems(b.__dict__))
        self.assertEqual(repr(x), repr(a))
        self.assertEqual(repr(y), repr(b))
        Sensor(1339)

    def test_pickle_slots(self):
        import pickle, cPickle
        global B, C, D, E


        class B(object):
            pass
        Sensor(1340)
        for base in [object, B]:


            class C(base):
                __slots__ = ['a']


            class D(C):
                pass
            try:
                pickle.dumps(C())
            except TypeError:
                pass
            else:
                self.fail('should fail: pickle C instance - %s' % base)
            try:
                cPickle.dumps(C())
            except TypeError:
                pass
            else:
                self.fail('should fail: cPickle C instance - %s' % base)
            try:
                pickle.dumps(C())
            except TypeError:
                pass
            else:
                self.fail('should fail: pickle D instance - %s' % base)
            try:
                cPickle.dumps(D())
            except TypeError:
                pass
            else:
                self.fail('should fail: cPickle D instance - %s' % base)


            class C(base):
                __slots__ = ['a']

                def __getstate__(self):
                    Sensor(1341)
                    try:
                        d = self.__dict__.copy()
                    except AttributeError:
                        d = {}
                    for cls in self.__class__.__mro__:
                        for sn in cls.__dict__.get('__slots__', ()):
                            try:
                                d[sn] = getattr(self, sn)
                            except AttributeError:
                                pass
                    py_ass_parser1342 = d
                    Sensor(1342)
                    return py_ass_parser1342

                def __setstate__(self, d):
                    Sensor(1343)
                    for k, v in d.items():
                        setattr(self, k, v)
                    Sensor(1344)


            class D(C):
                pass
            Sensor(1345)
            x = C()
            y = pickle.loads(pickle.dumps(x))
            self.assertEqual(hasattr(y, 'a'), 0)
            y = cPickle.loads(cPickle.dumps(x))
            self.assertEqual(hasattr(y, 'a'), 0)
            x.a = 42
            y = pickle.loads(pickle.dumps(x))
            self.assertEqual(y.a, 42)
            y = cPickle.loads(cPickle.dumps(x))
            self.assertEqual(y.a, 42)
            x = D()
            x.a = 42
            x.b = 100
            y = pickle.loads(pickle.dumps(x))
            self.assertEqual(y.a + y.b, 142)
            y = cPickle.loads(cPickle.dumps(x))
            self.assertEqual(y.a + y.b, 142)


            class E(C):
                __slots__ = ['b']
            x = E()
            x.a = 42
            x.b = 'foo'
            y = pickle.loads(pickle.dumps(x))
            self.assertEqual(y.a, x.a)
            self.assertEqual(y.b, x.b)
            y = cPickle.loads(cPickle.dumps(x))
            self.assertEqual(y.a, x.a)
            self.assertEqual(y.b, x.b)
        Sensor(1346)

    def test_binary_operator_override(self):


        class I(int):

            def __repr__(self):
                py_ass_parser1347 = 'I(%r)' % int(self)
                Sensor(1347)
                return py_ass_parser1347

            def __add__(self, other):
                py_ass_parser1348 = I(int(self) + int(other))
                Sensor(1348)
                return py_ass_parser1348
            __radd__ = __add__

            def __pow__(self, other, mod=None):
                Sensor(1349)
                if mod is None:
                    py_ass_parser1350 = I(pow(int(self), int(other)))
                    Sensor(1350)
                    return py_ass_parser1350
                else:
                    py_ass_parser1351 = I(pow(int(self), int(other), int(mod)))
                    Sensor(1351)
                    return py_ass_parser1351
                Sensor(1352)

            def __rpow__(self, other, mod=None):
                Sensor(1353)
                if mod is None:
                    py_ass_parser1354 = I(pow(int(other), int(self), mod))
                    Sensor(1354)
                    return py_ass_parser1354
                else:
                    py_ass_parser1355 = I(pow(int(other), int(self), int(mod)))
                    Sensor(1355)
                    return py_ass_parser1355
                Sensor(1356)
        Sensor(1357)
        self.assertEqual(repr(I(1) + I(2)), 'I(3)')
        self.assertEqual(repr(I(1) + 2), 'I(3)')
        self.assertEqual(repr(1 + I(2)), 'I(3)')
        self.assertEqual(repr(I(2) ** I(3)), 'I(8)')
        self.assertEqual(repr(2 ** I(3)), 'I(8)')
        self.assertEqual(repr(I(2) ** 3), 'I(8)')
        self.assertEqual(repr(pow(I(2), I(3), I(5))), 'I(3)')


        class S(str):

            def __eq__(self, other):
                py_ass_parser1358 = self.lower() == other.lower()
                Sensor(1358)
                return py_ass_parser1358
            __hash__ = None
        Sensor(1359)

    def test_subclass_propagation(self):


        class A(object):
            pass


        class B(A):
            pass


        class C(A):
            pass


        class D(B, C):
            pass
        Sensor(1360)
        d = D()
        orig_hash = hash(d)
        A.__hash__ = lambda self: 42
        self.assertEqual(hash(d), 42)
        C.__hash__ = lambda self: 314
        self.assertEqual(hash(d), 314)
        B.__hash__ = lambda self: 144
        self.assertEqual(hash(d), 144)
        D.__hash__ = lambda self: 100
        self.assertEqual(hash(d), 100)
        D.__hash__ = None
        self.assertRaises(TypeError, hash, d)
        del D.__hash__
        self.assertEqual(hash(d), 144)
        B.__hash__ = None
        self.assertRaises(TypeError, hash, d)
        del B.__hash__
        self.assertEqual(hash(d), 314)
        C.__hash__ = None
        self.assertRaises(TypeError, hash, d)
        del C.__hash__
        self.assertEqual(hash(d), 42)
        A.__hash__ = None
        self.assertRaises(TypeError, hash, d)
        del A.__hash__
        self.assertEqual(hash(d), orig_hash)
        d.foo = 42
        d.bar = 42
        self.assertEqual(d.foo, 42)
        self.assertEqual(d.bar, 42)

        def __getattribute__(self, name):
            Sensor(1361)
            if name == 'foo':
                py_ass_parser1362 = 24
                Sensor(1362)
                return py_ass_parser1362
            py_ass_parser1363 = object.__getattribute__(self, name)
            Sensor(1363)
            return py_ass_parser1363
        A.__getattribute__ = __getattribute__
        self.assertEqual(d.foo, 24)
        self.assertEqual(d.bar, 42)

        def __getattr__(self, name):
            Sensor(1364)
            if name in ('spam', 'foo', 'bar'):
                py_ass_parser1365 = 'hello'
                Sensor(1365)
                return py_ass_parser1365
            raise AttributeError, name
        B.__getattr__ = __getattr__
        self.assertEqual(d.spam, 'hello')
        self.assertEqual(d.foo, 24)
        self.assertEqual(d.bar, 42)
        del A.__getattribute__
        self.assertEqual(d.foo, 42)
        del d.foo
        self.assertEqual(d.foo, 'hello')
        self.assertEqual(d.bar, 42)
        del B.__getattr__
        try:
            d.foo
        except AttributeError:
            pass
        else:
            self.fail('d.foo should be undefined now')


        class A(object):
            pass


        class B(A):
            pass
        del B
        test_support.gc_collect()
        A.__setitem__ = lambda *a: None
        Sensor(1366)

    def test_buffer_inheritance(self):
        import binascii


        class MyStr(str):
            pass
        Sensor(1367)
        base = 'abc'
        m = MyStr(base)
        self.assertEqual(binascii.b2a_hex(m), binascii.b2a_hex(base))


        class MyUni(unicode):
            pass
        base = u'abc'
        m = MyUni(base)
        self.assertEqual(binascii.b2a_hex(m), binascii.b2a_hex(base))


        class MyInt(int):
            pass
        m = MyInt(42)
        try:
            binascii.b2a_hex(m)
            self.fail('subclass of int should not have a buffer interface')
        except TypeError:
            pass
        Sensor(1368)

    def test_str_of_str_subclass(self):
        import binascii
        import cStringIO


        class octetstring(str):

            def __str__(self):
                py_ass_parser1369 = binascii.b2a_hex(self)
                Sensor(1369)
                return py_ass_parser1369

            def __repr__(self):
                py_ass_parser1370 = self + ' repr'
                Sensor(1370)
                return py_ass_parser1370
        o = octetstring('A')
        self.assertEqual(type(o), octetstring)
        self.assertEqual(type(str(o)), str)
        self.assertEqual(type(repr(o)), str)
        self.assertEqual(ord(o), 65)
        self.assertEqual(str(o), '41')
        self.assertEqual(repr(o), 'A repr')
        self.assertEqual(o.__str__(), '41')
        self.assertEqual(o.__repr__(), 'A repr')
        capture = cStringIO.StringIO()
        print  >> capture, o
        print  >> capture, str(o)
        self.assertEqual(capture.getvalue(), '41\n41\n')
        capture.close()
        Sensor(1371)

    def test_keyword_arguments(self):

        def f(a):
            py_ass_parser1372 = a
            Sensor(1372)
            return py_ass_parser1372
        self.assertEqual(f.__call__(a=42), 42)
        a = []
        list.__init__(a, sequence=[0, 1, 2])
        self.assertEqual(a, [0, 1, 2])
        Sensor(1373)

    def test_recursive_call(self):


        class A(object):
            pass
        Sensor(1374)
        A.__call__ = A()
        try:
            A()()
        except RuntimeError:
            pass
        else:
            self.fail('Recursion limit should have been reached for __call__()'
                )
        Sensor(1375)

    def test_delete_hook(self):
        Sensor(1376)
        log = []


        class C(object):

            def __del__(self):
                Sensor(1377)
                log.append(1)
                Sensor(1378)
        c = C()
        self.assertEqual(log, [])
        del c
        test_support.gc_collect()
        self.assertEqual(log, [1])


        class D(object):
            pass
        d = D()
        Sensor(1379)
        try:
            del d[0]
        except TypeError:
            pass
        else:
            self.fail("invalid del() didn't raise TypeError")
        Sensor(1380)

    def test_hash_inheritance(self):


        class mydict(dict):
            pass
        Sensor(1381)
        d = mydict()
        try:
            hash(d)
        except TypeError:
            pass
        else:
            self.fail('hash() of dict subclass should fail')


        class mylist(list):
            pass
        d = mylist()
        try:
            hash(d)
        except TypeError:
            pass
        else:
            self.fail('hash() of list subclass should fail')
        Sensor(1382)

    def test_str_operations(self):
        Sensor(1383)
        try:
            'a' + 5
        except TypeError:
            pass
        else:
            self.fail("'' + 5 doesn't raise TypeError")
        try:
            """""".split('')
        except ValueError:
            pass
        else:
            self.fail("''.split('') doesn't raise ValueError")
        try:
            """""".join([0])
        except TypeError:
            pass
        else:
            self.fail("''.join([0]) doesn't raise TypeError")
        try:
            """""".rindex('5')
        except ValueError:
            pass
        else:
            self.fail("''.rindex('5') doesn't raise ValueError")
        try:
            '%(n)s' % None
        except TypeError:
            pass
        else:
            self.fail("'%(n)s' % None doesn't raise TypeError")
        try:
            '%(n' % {}
        except ValueError:
            pass
        else:
            self.fail("'%(n' % {} '' doesn't raise ValueError")
        try:
            '%*s' % 'abc'
        except TypeError:
            pass
        else:
            self.fail("'%*s' % ('abc') doesn't raise TypeError")
        try:
            '%*.*s' % ('abc', 5)
        except TypeError:
            pass
        else:
            self.fail("'%*.*s' % ('abc', 5) doesn't raise TypeError")
        try:
            '%s' % (1, 2)
        except TypeError:
            pass
        else:
            self.fail("'%s' % (1, 2) doesn't raise TypeError")
        try:
            '%' % None
        except ValueError:
            pass
        else:
            self.fail("'%' % None doesn't raise ValueError")
        self.assertEqual('534253'.isdigit(), 1)
        self.assertEqual('534253x'.isdigit(), 0)
        self.assertEqual('%c' % 5, '\x05')
        self.assertEqual('%c' % '5', '5')
        Sensor(1384)

    def test_deepcopy_recursive(self):


        class Node:
            pass
        Sensor(1385)
        a = Node()
        b = Node()
        a.b = b
        b.a = a
        z = deepcopy(a)
        Sensor(1386)

    def test_unintialized_modules(self):
        from types import ModuleType as M
        Sensor(1387)
        m = M.__new__(M)
        str(m)
        self.assertEqual(hasattr(m, '__name__'), 0)
        self.assertEqual(hasattr(m, '__file__'), 0)
        self.assertEqual(hasattr(m, 'foo'), 0)
        self.assertFalse(m.__dict__)
        m.foo = 1
        self.assertEqual(m.__dict__, {'foo': 1})
        Sensor(1388)

    def test_funny_new(self):


        class C(object):

            def __new__(cls, arg):
                Sensor(1389)
                if isinstance(arg, str):
                    py_ass_parser1390 = [1, 2, 3]
                    Sensor(1390)
                    return py_ass_parser1390
                elif isinstance(arg, int):
                    py_ass_parser1391 = object.__new__(D)
                    Sensor(1391)
                    return py_ass_parser1391
                else:
                    py_ass_parser1392 = object.__new__(cls)
                    Sensor(1392)
                    return py_ass_parser1392
                Sensor(1393)


        class D(C):

            def __init__(self, arg):
                Sensor(1394)
                self.foo = arg
                Sensor(1395)
        Sensor(1396)
        self.assertEqual(C('1'), [1, 2, 3])
        self.assertEqual(D('1'), [1, 2, 3])
        d = D(None)
        self.assertEqual(d.foo, None)
        d = C(1)
        self.assertEqual(isinstance(d, D), True)
        self.assertEqual(d.foo, 1)
        d = D(1)
        self.assertEqual(isinstance(d, D), True)
        self.assertEqual(d.foo, 1)
        Sensor(1397)

    def test_imul_bug(self):


        class C(object):

            def __imul__(self, other):
                py_ass_parser1398 = self, other
                Sensor(1398)
                return py_ass_parser1398
        x = C()
        y = x
        y *= 1.0
        self.assertEqual(y, (x, 1.0))
        y = x
        y *= 2
        self.assertEqual(y, (x, 2))
        y = x
        y *= 3L
        self.assertEqual(y, (x, 3L))
        y = x
        y *= 1L << 100
        self.assertEqual(y, (x, 1L << 100))
        y = x
        y *= None
        self.assertEqual(y, (x, None))
        y = x
        y *= 'foo'
        self.assertEqual(y, (x, 'foo'))
        Sensor(1399)

    def test_copy_setstate(self):
        import copy


        class C(object):

            def __init__(self, foo=None):
                Sensor(1400)
                self.foo = foo
                self.__foo = foo
                Sensor(1401)

            def setfoo(self, foo=None):
                Sensor(1402)
                self.foo = foo
                Sensor(1403)

            def getfoo(self):
                py_ass_parser1404 = self.__foo
                Sensor(1404)
                return py_ass_parser1404

            def __getstate__(self):
                py_ass_parser1405 = [self.foo]
                Sensor(1405)
                return py_ass_parser1405

            def __setstate__(self_, lst):
                Sensor(1406)
                self.assertEqual(len(lst), 1)
                self_.__foo = self_.foo = lst[0]
                Sensor(1407)
        Sensor(1408)
        a = C(42)
        a.setfoo(24)
        self.assertEqual(a.foo, 24)
        self.assertEqual(a.getfoo(), 42)
        b = copy.copy(a)
        self.assertEqual(b.foo, 24)
        self.assertEqual(b.getfoo(), 24)
        b = copy.deepcopy(a)
        self.assertEqual(b.foo, 24)
        self.assertEqual(b.getfoo(), 24)
        Sensor(1409)

    def test_slices(self):
        Sensor(1410)
        self.assertEqual('hello'[:4], 'hell')
        self.assertEqual('hello'[slice(4)], 'hell')
        self.assertEqual(str.__getitem__('hello', slice(4)), 'hell')


        class S(str):

            def __getitem__(self, x):
                py_ass_parser1411 = str.__getitem__(self, x)
                Sensor(1411)
                return py_ass_parser1411
        self.assertEqual(S('hello')[:4], 'hell')
        self.assertEqual(S('hello')[slice(4)], 'hell')
        self.assertEqual(S('hello').__getitem__(slice(4)), 'hell')
        self.assertEqual((1, 2, 3)[:2], (1, 2))
        self.assertEqual((1, 2, 3)[slice(2)], (1, 2))
        self.assertEqual(tuple.__getitem__((1, 2, 3), slice(2)), (1, 2))


        class T(tuple):

            def __getitem__(self, x):
                py_ass_parser1412 = tuple.__getitem__(self, x)
                Sensor(1412)
                return py_ass_parser1412
        self.assertEqual(T((1, 2, 3))[:2], (1, 2))
        self.assertEqual(T((1, 2, 3))[slice(2)], (1, 2))
        self.assertEqual(T((1, 2, 3)).__getitem__(slice(2)), (1, 2))
        self.assertEqual([1, 2, 3][:2], [1, 2])
        self.assertEqual([1, 2, 3][slice(2)], [1, 2])
        self.assertEqual(list.__getitem__([1, 2, 3], slice(2)), [1, 2])


        class L(list):

            def __getitem__(self, x):
                py_ass_parser1413 = list.__getitem__(self, x)
                Sensor(1413)
                return py_ass_parser1413
        self.assertEqual(L([1, 2, 3])[:2], [1, 2])
        self.assertEqual(L([1, 2, 3])[slice(2)], [1, 2])
        self.assertEqual(L([1, 2, 3]).__getitem__(slice(2)), [1, 2])
        a = L([1, 2, 3])
        a[slice(1, 3)] = [3, 2]
        self.assertEqual(a, [1, 3, 2])
        a[slice(0, 2, 1)] = [3, 1]
        self.assertEqual(a, [3, 1, 2])
        a.__setitem__(slice(1, 3), [2, 1])
        self.assertEqual(a, [3, 2, 1])
        a.__setitem__(slice(0, 2, 1), [2, 3])
        self.assertEqual(a, [2, 3, 1])
        Sensor(1414)

    def test_subtype_resurrection(self):


        class C(object):
            container = []

            def __del__(self):
                Sensor(1415)
                C.container.append(self)
                Sensor(1416)
        Sensor(1417)
        c = C()
        c.attr = 42
        del c
        test_support.gc_collect()
        self.assertEqual(len(C.container), 1)
        del C.container[-1]
        if test_support.check_impl_detail():
            test_support.gc_collect()
            self.assertEqual(len(C.container), 1)
            self.assertEqual(C.container[-1].attr, 42)
        del C.__del__
        Sensor(1418)

    def test_slots_trash(self):


        class trash(object):
            __slots__ = ['x']

            def __init__(self, x):
                Sensor(1419)
                self.x = x
                Sensor(1420)
        Sensor(1421)
        o = None
        for i in xrange(50000):
            o = trash(o)
        del o
        Sensor(1422)

    def test_slots_multiple_inheritance(self):


        class A(object):
            __slots__ = ()


        class B(object):
            pass


        class C(A, B):
            __slots__ = ()
        Sensor(1423)
        if test_support.check_impl_detail():
            self.assertEqual(C.__basicsize__, B.__basicsize__)
        self.assertTrue(hasattr(C, '__dict__'))
        self.assertTrue(hasattr(C, '__weakref__'))
        C().x = 2
        Sensor(1424)

    def test_rmul(self):


        class C(object):

            def __mul__(self, other):
                py_ass_parser1425 = 'mul'
                Sensor(1425)
                return py_ass_parser1425

            def __rmul__(self, other):
                py_ass_parser1426 = 'rmul'
                Sensor(1426)
                return py_ass_parser1426
        a = C()
        self.assertEqual(a * 2, 'mul')
        self.assertEqual(a * 2.2, 'mul')
        self.assertEqual(2 * a, 'rmul')
        self.assertEqual(2.2 * a, 'rmul')
        Sensor(1427)

    def test_ipow(self):


        class C(object):

            def __ipow__(self, other):
                Sensor(1428)
                pass
                Sensor(1429)
        Sensor(1430)
        a = C()
        a **= 2
        Sensor(1431)

    def test_mutable_bases(self):


        class C(object):
            pass


        class C2(object):

            def __getattribute__(self, attr):
                Sensor(1432)
                if attr == 'a':
                    py_ass_parser1433 = 2
                    Sensor(1433)
                    return py_ass_parser1433
                else:
                    py_ass_parser1434 = super(C2, self).__getattribute__(attr)
                    Sensor(1434)
                    return py_ass_parser1434
                Sensor(1435)

            def meth(self):
                py_ass_parser1436 = 1
                Sensor(1436)
                return py_ass_parser1436


        class D(C):
            pass


        class E(D):
            pass
        d = D()
        e = E()
        D.__bases__ = C,
        D.__bases__ = C2,
        self.assertEqual(d.meth(), 1)
        self.assertEqual(e.meth(), 1)
        self.assertEqual(d.a, 2)
        self.assertEqual(e.a, 2)
        self.assertEqual(C2.__subclasses__(), [D])
        try:
            del D.__bases__
        except (TypeError, AttributeError):
            pass
        else:
            self.fail("shouldn't be able to delete .__bases__")
        try:
            D.__bases__ = ()
        except TypeError as msg:
            if str(msg) == "a new-style class can't have only classic bases":
                self.fail('wrong error message for .__bases__ = ()')
        else:
            self.fail("shouldn't be able to set .__bases__ to ()")
        try:
            D.__bases__ = D,
        except TypeError:
            pass
        else:
            self.fail("shouldn't be able to create inheritance cycles")
        try:
            D.__bases__ = C, C
        except TypeError:
            pass
        else:
            self.fail("didn't detect repeated base classes")
        try:
            D.__bases__ = E,
        except TypeError:
            pass
        else:
            self.fail("shouldn't be able to create inheritance cycles")


        class Classic:

            def meth2(self):
                py_ass_parser1437 = 3
                Sensor(1437)
                return py_ass_parser1437
        D.__bases__ = C, Classic
        self.assertEqual(d.meth2(), 3)
        self.assertEqual(e.meth2(), 3)
        try:
            d.a
        except AttributeError:
            pass
        else:
            self.fail('attribute should have vanished')
        try:
            D.__bases__ = Classic,
        except TypeError:
            pass
        else:
            self.fail('new-style class must have a new-style base')
        Sensor(1438)

    def test_builtin_bases(self):
        Sensor(1439)
        builtin_types = [tp for tp in __builtin__.__dict__.itervalues() if
            isinstance(tp, type)]
        for tp in builtin_types:
            object.__getattribute__(tp, '__bases__')
            if tp is not object:
                self.assertEqual(len(tp.__bases__), 1, tp)


        class L(list):
            pass


        class C(object):
            pass


        class D(C):
            pass
        try:
            L.__bases__ = dict,
        except TypeError:
            pass
        else:
            self.fail("shouldn't turn list subclass into dict subclass")
        try:
            list.__bases__ = dict,
        except TypeError:
            pass
        else:
            self.fail("shouldn't be able to assign to list.__bases__")
        try:
            D.__bases__ = C, list
        except TypeError:
            pass
        else:
            assert 0, 'best_base calculation found wanting'
        Sensor(1440)

    def test_mutable_bases_with_failing_mro(self):


        class WorkOnce(type):

            def __new__(self, name, bases, ns):
                Sensor(1441)
                self.flag = 0
                py_ass_parser1442 = super(WorkOnce, self).__new__(WorkOnce,
                    name, bases, ns)
                Sensor(1442)
                return py_ass_parser1442

            def mro(self):
                Sensor(1443)
                if self.flag > 0:
                    raise RuntimeError, 'bozo'
                else:
                    self.flag += 1
                    py_ass_parser1444 = type.mro(self)
                    Sensor(1444)
                    return py_ass_parser1444
                Sensor(1445)


        class WorkAlways(type):

            def mro(self):
                py_ass_parser1446 = type.mro(self)
                Sensor(1446)
                return py_ass_parser1446


        class C(object):
            pass


        class C2(object):
            pass


        class D(C):
            pass


        class E(D):
            pass


        class F(D):
            __metaclass__ = WorkOnce


        class G(D):
            __metaclass__ = WorkAlways
        E_mro_before = E.__mro__
        D_mro_before = D.__mro__
        try:
            D.__bases__ = C2,
        except RuntimeError:
            self.assertEqual(E.__mro__, E_mro_before)
            self.assertEqual(D.__mro__, D_mro_before)
        else:
            self.fail('exception not propagated')
        Sensor(1447)

    def test_mutable_bases_catch_mro_conflict(self):


        class A(object):
            pass


        class B(object):
            pass


        class C(A, B):
            pass


        class D(A, B):
            pass


        class E(C, D):
            pass
        Sensor(1448)
        try:
            C.__bases__ = B, A
        except TypeError:
            pass
        else:
            self.fail("didn't catch MRO conflict")
        Sensor(1449)

    def test_mutable_names(self):


        class C(object):
            pass
        Sensor(1450)
        mod = C.__module__
        C.__name__ = 'D'
        self.assertEqual((C.__module__, C.__name__), (mod, 'D'))
        C.__name__ = 'D.E'
        self.assertEqual((C.__module__, C.__name__), (mod, 'D.E'))
        Sensor(1451)

    def test_evil_type_name(self):


        class Nasty(str):

            def __del__(self):
                Sensor(1452)
                C.__name__ = 'other'
                Sensor(1453)


        class C(object):
            pass
        Sensor(1454)
        C.__name__ = Nasty('abc')
        C.__name__ = 'normal'
        Sensor(1455)

    def test_subclass_right_op(self):


        class B(int):

            def __floordiv__(self, other):
                py_ass_parser1456 = 'B.__floordiv__'
                Sensor(1456)
                return py_ass_parser1456

            def __rfloordiv__(self, other):
                py_ass_parser1457 = 'B.__rfloordiv__'
                Sensor(1457)
                return py_ass_parser1457
        self.assertEqual(B(1) // 1, 'B.__floordiv__')
        self.assertEqual(1 // B(1), 'B.__rfloordiv__')


        class C(object):

            def __floordiv__(self, other):
                py_ass_parser1458 = 'C.__floordiv__'
                Sensor(1458)
                return py_ass_parser1458

            def __rfloordiv__(self, other):
                py_ass_parser1459 = 'C.__rfloordiv__'
                Sensor(1459)
                return py_ass_parser1459
        self.assertEqual(C() // 1, 'C.__floordiv__')
        self.assertEqual(1 // C(), 'C.__rfloordiv__')


        class D(C):

            def __floordiv__(self, other):
                py_ass_parser1460 = 'D.__floordiv__'
                Sensor(1460)
                return py_ass_parser1460

            def __rfloordiv__(self, other):
                py_ass_parser1461 = 'D.__rfloordiv__'
                Sensor(1461)
                return py_ass_parser1461
        self.assertEqual(D() // C(), 'D.__floordiv__')
        self.assertEqual(C() // D(), 'D.__rfloordiv__')


        class E(C):
            pass
        self.assertEqual(E.__rfloordiv__, C.__rfloordiv__)
        self.assertEqual(E() // 1, 'C.__floordiv__')
        self.assertEqual(1 // E(), 'C.__rfloordiv__')
        self.assertEqual(E() // C(), 'C.__floordiv__')
        self.assertEqual(C() // E(), 'C.__floordiv__')
        Sensor(1462)

    @test_support.impl_detail('testing an internal kind of method object')
    def test_meth_class_get(self):
        Sensor(1463)
        arg = [1, 2, 3]
        res = {(1): None, (2): None, (3): None}
        self.assertEqual(dict.fromkeys(arg), res)
        self.assertEqual({}.fromkeys(arg), res)
        descr = dict.__dict__['fromkeys']
        self.assertEqual(descr.__get__(None, dict)(arg), res)
        self.assertEqual(descr.__get__({})(arg), res)
        try:
            descr.__get__(None, None)
        except TypeError:
            pass
        else:
            self.fail("shouldn't have allowed descr.__get__(None, None)")
        try:
            descr.__get__(42)
        except TypeError:
            pass
        else:
            self.fail("shouldn't have allowed descr.__get__(42)")
        try:
            descr.__get__(None, 42)
        except TypeError:
            pass
        else:
            self.fail("shouldn't have allowed descr.__get__(None, 42)")
        try:
            descr.__get__(None, int)
        except TypeError:
            pass
        else:
            self.fail("shouldn't have allowed descr.__get__(None, int)")
        Sensor(1464)

    def test_isinst_isclass(self):


        class Proxy(object):

            def __init__(self, obj):
                Sensor(1465)
                self.__obj = obj
                Sensor(1466)

            def __getattribute__(self, name):
                Sensor(1467)
                if name.startswith('_Proxy__'):
                    py_ass_parser1468 = object.__getattribute__(self, name)
                    Sensor(1468)
                    return py_ass_parser1468
                else:
                    py_ass_parser1469 = getattr(self.__obj, name)
                    Sensor(1469)
                    return py_ass_parser1469
                Sensor(1470)


        class C:
            pass
        Sensor(1471)
        a = C()
        pa = Proxy(a)
        self.assertIsInstance(a, C)
        self.assertIsInstance(pa, C)


        class D(C):
            pass
        a = D()
        pa = Proxy(a)
        self.assertIsInstance(a, C)
        self.assertIsInstance(pa, C)


        class C(object):
            pass
        a = C()
        pa = Proxy(a)
        self.assertIsInstance(a, C)
        self.assertIsInstance(pa, C)


        class D(C):
            pass
        a = D()
        pa = Proxy(a)
        self.assertIsInstance(a, C)
        self.assertIsInstance(pa, C)
        Sensor(1472)

    def test_proxy_super(self):


        class Proxy(object):

            def __init__(self, obj):
                Sensor(1473)
                self.__obj = obj
                Sensor(1474)

            def __getattribute__(self, name):
                Sensor(1475)
                if name.startswith('_Proxy__'):
                    py_ass_parser1476 = object.__getattribute__(self, name)
                    Sensor(1476)
                    return py_ass_parser1476
                else:
                    py_ass_parser1477 = getattr(self.__obj, name)
                    Sensor(1477)
                    return py_ass_parser1477
                Sensor(1478)


        class B(object):

            def f(self):
                py_ass_parser1479 = 'B.f'
                Sensor(1479)
                return py_ass_parser1479


        class C(B):

            def f(self):
                py_ass_parser1480 = super(C, self).f() + '->C.f'
                Sensor(1480)
                return py_ass_parser1480
        obj = C()
        p = Proxy(obj)
        self.assertEqual(C.__dict__['f'](p), 'B.f->C.f')
        Sensor(1481)

    def test_carloverre(self):
        Sensor(1482)
        try:
            object.__setattr__(str, 'foo', 42)
        except TypeError:
            pass
        else:
            self.fail('Carlo Verre __setattr__ succeeded!')
        try:
            object.__delattr__(str, 'lower')
        except TypeError:
            pass
        else:
            self.fail('Carlo Verre __delattr__ succeeded!')
        Sensor(1483)

    def test_weakref_segfault(self):
        import weakref


        class Provoker:

            def __init__(self, referrent):
                Sensor(1484)
                self.ref = weakref.ref(referrent)
                Sensor(1485)

            def __del__(self):
                Sensor(1486)
                x = self.ref()
                Sensor(1487)


        class Oops(object):
            pass
        Sensor(1488)
        o = Oops()
        o.whatever = Provoker(o)
        del o
        Sensor(1489)

    def test_wrapper_segfault(self):
        Sensor(1490)
        f = lambda : None
        for i in xrange(1000000):
            f = f.__call__
        f = None
        Sensor(1491)

    def test_file_fault(self):
        Sensor(1492)
        test_stdout = sys.stdout


        class StdoutGuard:

            def __getattr__(self, attr):
                Sensor(1493)
                sys.stdout = sys.__stdout__
                raise RuntimeError('Premature access to sys.stdout.%s' % attr)
        sys.stdout = StdoutGuard()
        try:
            try:
                print 'Oops!'
            except RuntimeError:
                pass
        finally:
            sys.stdout = test_stdout
        Sensor(1494)

    def test_vicious_descriptor_nonsense(self):


        class Evil(object):

            def __hash__(self):
                py_ass_parser1495 = hash('attr')
                Sensor(1495)
                return py_ass_parser1495

            def __eq__(self, other):
                Sensor(1496)
                del C.attr
                py_ass_parser1497 = 0
                Sensor(1497)
                return py_ass_parser1497


        class Descr(object):

            def __get__(self, ob, type=None):
                py_ass_parser1498 = 1
                Sensor(1498)
                return py_ass_parser1498


        class C(object):
            attr = Descr()
        c = C()
        c.__dict__[Evil()] = 0
        self.assertEqual(c.attr, 1)
        test_support.gc_collect()
        self.assertEqual(hasattr(c, 'attr'), False)
        Sensor(1499)

    def test_init(self):


        class Foo(object):

            def __init__(self):
                py_ass_parser1500 = 10
                Sensor(1500)
                return py_ass_parser1500
        try:
            Foo()
        except TypeError:
            pass
        else:
            self.fail('did not test __init__() for None return')
        Sensor(1501)

    def test_method_wrapper(self):
        Sensor(1502)
        l = []
        self.assertEqual(l.__add__, l.__add__)
        self.assertEqual(l.__add__, [].__add__)
        self.assertTrue(l.__add__ != [5].__add__)
        self.assertTrue(l.__add__ != l.__mul__)
        self.assertTrue(l.__add__.__name__ == '__add__')
        if hasattr(l.__add__, '__self__'):
            self.assertTrue(l.__add__.__self__ is l)
            self.assertTrue(l.__add__.__objclass__ is list)
        else:
            self.assertTrue(l.__add__.im_self is l)
            self.assertTrue(l.__add__.im_class is list)
        self.assertEqual(l.__add__.__doc__, list.__add__.__doc__)
        try:
            hash(l.__add__)
        except TypeError:
            pass
        else:
            self.fail('no TypeError from hash([].__add__)')
        t = ()
        t += 7,
        self.assertEqual(t.__add__, (7,).__add__)
        self.assertEqual(hash(t.__add__), hash((7,).__add__))
        Sensor(1503)

    def test_not_implemented(self):
        import operator

        def specialmethod(self, other):
            py_ass_parser1504 = NotImplemented
            Sensor(1504)
            return py_ass_parser1504

        def check(expr, x, y):
            Sensor(1505)
            try:
                exec expr in {'x': x, 'y': y, 'operator': operator}
            except TypeError:
                pass
            else:
                self.fail('no TypeError from %r' % (expr,))
            Sensor(1506)
        Sensor(1507)
        N1 = sys.maxint + 1L
        N2 = sys.maxint
        for metaclass in [type, types.ClassType]:
            for name, expr, iexpr in [('__add__', 'x + y', 'x += y'), (
                '__sub__', 'x - y', 'x -= y'), ('__mul__', 'x * y',
                'x *= y'), ('__truediv__', 'operator.truediv(x, y)', None),
                ('__floordiv__', 'operator.floordiv(x, y)', None), (
                '__div__', 'x / y', 'x /= y'), ('__mod__', 'x % y',
                'x %= y'), ('__divmod__', 'divmod(x, y)', None), ('__pow__',
                'x ** y', 'x **= y'), ('__lshift__', 'x << y', 'x <<= y'),
                ('__rshift__', 'x >> y', 'x >>= y'), ('__and__', 'x & y',
                'x &= y'), ('__or__', 'x | y', 'x |= y'), ('__xor__',
                'x ^ y', 'x ^= y'), ('__coerce__', 'coerce(x, y)', None)]:
                if name == '__coerce__':
                    rname = name
                else:
                    rname = '__r' + name[2:]
                A = metaclass('A', (), {name: specialmethod})
                B = metaclass('B', (), {rname: specialmethod})
                a = A()
                b = B()
                check(expr, a, a)
                check(expr, a, b)
                check(expr, b, a)
                check(expr, b, b)
                check(expr, a, N1)
                check(expr, a, N2)
                check(expr, N1, b)
                check(expr, N2, b)
                if iexpr:
                    check(iexpr, a, a)
                    check(iexpr, a, b)
                    check(iexpr, b, a)
                    check(iexpr, b, b)
                    check(iexpr, a, N1)
                    check(iexpr, a, N2)
                    iname = '__i' + name[2:]
                    C = metaclass('C', (), {iname: specialmethod})
                    c = C()
                    check(iexpr, c, a)
                    check(iexpr, c, b)
                    check(iexpr, c, N1)
                    check(iexpr, c, N2)
        Sensor(1508)

    def test_assign_slice(self):


        class C(object):

            def __setslice__(self, start, stop, value):
                Sensor(1509)
                self.value = value
                Sensor(1510)
        Sensor(1511)
        c = C()
        c[1:2] = 3
        self.assertEqual(c.value, 3)
        Sensor(1512)

    def test_set_and_no_get(self):


        class Descr(object):

            def __init__(self, name):
                Sensor(1513)
                self.name = name
                Sensor(1514)

            def __set__(self, obj, value):
                Sensor(1515)
                obj.__dict__[self.name] = value
                Sensor(1516)
        Sensor(1517)
        descr = Descr('a')


        class X(object):
            a = descr
        x = X()
        self.assertIs(x.a, descr)
        x.a = 42
        self.assertEqual(x.a, 42)


        class Meta(type):
            pass


        class X(object):
            __metaclass__ = Meta
        X.a = 42
        Meta.a = Descr('a')
        self.assertEqual(X.a, 42)
        Sensor(1518)

    def test_getattr_hooks(self):


        class Descriptor(object):
            counter = 0

            def __get__(self, obj, objtype=None):

                def getter(name):
                    Sensor(1519)
                    self.counter += 1
                    raise AttributeError(name)
                py_ass_parser1520 = getter
                Sensor(1520)
                return py_ass_parser1520
        descr = Descriptor()


        class A(object):
            __getattribute__ = descr


        class B(object):
            __getattr__ = descr


        class C(object):
            __getattribute__ = descr
            __getattr__ = descr
        self.assertRaises(AttributeError, getattr, A(), 'attr')
        self.assertEqual(descr.counter, 1)
        self.assertRaises(AttributeError, getattr, B(), 'attr')
        self.assertEqual(descr.counter, 2)
        self.assertRaises(AttributeError, getattr, C(), 'attr')
        self.assertEqual(descr.counter, 4)


        class EvilGetattribute(object):

            def __getattr__(self, name):
                Sensor(1521)
                raise AttributeError(name)

            def __getattribute__(self, name):
                Sensor(1522)
                del EvilGetattribute.__getattr__
                for i in range(5):
                    gc.collect()
                raise AttributeError(name)
        self.assertRaises(AttributeError, getattr, EvilGetattribute(), 'attr')
        Sensor(1523)

    def test_type___getattribute__(self):
        Sensor(1524)
        self.assertRaises(TypeError, type.__getattribute__, list, type)
        Sensor(1525)

    def test_abstractmethods(self):
        Sensor(1526)
        self.assertRaises(AttributeError, getattr, type, '__abstractmethods__')


        class meta(type):
            pass
        self.assertRaises(AttributeError, getattr, meta, '__abstractmethods__')


        class X(object):
            pass
        with self.assertRaises(AttributeError):
            del X.__abstractmethods__
        Sensor(1527)

    def test_proxy_call(self):


        class FakeStr(object):
            __class__ = str
        Sensor(1528)
        fake_str = FakeStr()
        self.assertTrue(isinstance(fake_str, str))
        with self.assertRaises(TypeError):
            str.split(fake_str)
        with self.assertRaises(TypeError):
            str.__add__(fake_str, 'abc')
        Sensor(1529)

    def test_repr_as_str(self):


        class Foo(object):
            pass
        Sensor(1530)
        Foo.__repr__ = Foo.__str__
        foo = Foo()
        self.assertRaises(RuntimeError, str, foo)
        self.assertRaises(RuntimeError, repr, foo)
        Sensor(1531)

    def test_mixing_slot_wrappers(self):


        class X(dict):
            __setattr__ = dict.__setitem__
        Sensor(1532)
        x = X()
        x.y = 42
        self.assertEqual(x['y'], 42)
        Sensor(1533)

    def test_cycle_through_dict(self):


        class X(dict):

            def __init__(self):
                Sensor(1534)
                dict.__init__(self)
                self.__dict__ = self
                Sensor(1535)
        Sensor(1536)
        x = X()
        x.attr = 42
        wr = weakref.ref(x)
        del x
        test_support.gc_collect()
        self.assertIsNone(wr())
        for o in gc.get_objects():
            self.assertIsNot(type(o), X)
        Sensor(1537)


class DictProxyTests(unittest.TestCase):

    def setUp(self):


        class C(object):

            def meth(self):
                Sensor(1538)
                pass
                Sensor(1539)
        Sensor(1540)
        self.C = C
        Sensor(1541)

    def test_repr(self):
        Sensor(1542)
        self.assertIn('dict_proxy({', repr(vars(self.C)))
        self.assertIn("'meth':", repr(vars(self.C)))
        Sensor(1543)

    def test_iter_keys(self):
        Sensor(1544)
        keys = [key for key in self.C.__dict__.iterkeys()]
        keys.sort()
        self.assertEqual(keys, ['__dict__', '__doc__', '__module__',
            '__weakref__', 'meth'])
        Sensor(1545)

    def test_iter_values(self):
        Sensor(1546)
        values = [values for values in self.C.__dict__.itervalues()]
        self.assertEqual(len(values), 5)
        Sensor(1547)

    def test_iter_items(self):
        Sensor(1548)
        keys = [key for key, value in self.C.__dict__.iteritems()]
        keys.sort()
        self.assertEqual(keys, ['__dict__', '__doc__', '__module__',
            '__weakref__', 'meth'])
        Sensor(1549)

    def test_dict_type_with_metaclass(self):


        class B(object):
            pass


        class M(type):
            pass


        class C:
            __metaclass__ = M
        Sensor(1550)
        self.assertEqual(type(C.__dict__), type(B.__dict__))
        Sensor(1551)


class PTypesLongInitTest(unittest.TestCase):

    def test_pytype_long_ready(self):


        class UserLong(object):

            def __pow__(self, *args):
                Sensor(1552)
                pass
                Sensor(1553)
        Sensor(1554)
        try:
            pow(0L, UserLong(), 0L)
        except:
            pass
        type.mro(tuple)
        Sensor(1555)


def test_main():
    Sensor(1556)
    deprecations = [('complex divmod\\(\\), // and % are deprecated$',
        DeprecationWarning)]
    if sys.py3kwarning:
        deprecations += [('classic (int|long) division', DeprecationWarning
            ), ('coerce.. not supported', DeprecationWarning), (
            '.+__(get|set|del)slice__ has been removed', DeprecationWarning)]
    with test_support.check_warnings(*deprecations):
        test_support.run_unittest(PTypesLongInitTest, OperatorsTest,
            ClassPropertiesAndMethods, DictProxyTests)
    Sensor(1557)


Sensor(1558)
if __name__ == '__main__':
    test_main()
Sensor(1560)
