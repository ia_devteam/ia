from sensors import Sensor


Sensor(3448)
"""Wrapper functions for Tcl/Tk.

Tkinter provides classes which allow the display, positioning and
control of widgets. Toplevel widgets are Tk and Toplevel. Other
widgets are Frame, Label, Entry, Text, Canvas, Button, Radiobutton,
Checkbutton, Scale, Listbox, Scrollbar, OptionMenu, Spinbox
LabelFrame and PanedWindow.

Properties of the widgets are specified with keyword arguments.
Keyword arguments have the same name as the corresponding resource
under Tk.

Widgets are positioned with one of the geometry managers Place, Pack
or Grid. These managers can be called with methods place, pack, grid
available in every Widget.

Actions are bound to events by resources (e.g. keyword argument
command) or with the method bind.

Example (Hello, World):
import Tkinter
from Tkconstants import *
tk = Tkinter.Tk()
frame = Tkinter.Frame(tk, relief=RIDGE, borderwidth=2)
frame.pack(fill=BOTH,expand=1)
label = Tkinter.Label(frame, text="Hello, World")
label.pack(fill=X, expand=1)
button = Tkinter.Button(frame,text="Exit",command=tk.destroy)
button.pack(side=BOTTOM)
tk.mainloop()
"""
__version__ = '$Revision: 81008 $'
import sys
Sensor(3449)
if sys.platform == 'win32':
    Sensor(3450)
    import FixTk
import _tkinter
Sensor(3451)
tkinter = _tkinter
TclError = _tkinter.TclError
from types import *
from Tkconstants import *
import re
wantobjects = 1
TkVersion = float(_tkinter.TK_VERSION)
TclVersion = float(_tkinter.TCL_VERSION)
READABLE = _tkinter.READABLE
WRITABLE = _tkinter.WRITABLE
EXCEPTION = _tkinter.EXCEPTION
Sensor(3452)
try:
    Sensor(3453)
    _tkinter.createfilehandler
except AttributeError:
    Sensor(3454)
    _tkinter.createfilehandler = None
Sensor(3455)
try:
    Sensor(3456)
    _tkinter.deletefilehandler
except AttributeError:
    Sensor(3457)
    _tkinter.deletefilehandler = None
Sensor(3458)
_magic_re = re.compile('([\\\\{}])')
_space_re = re.compile('([\\s])')


def _join(value):
    Sensor(3459)
    """Internal function."""
    py_ass_parser3460 = ' '.join(map(_stringify, value))
    Sensor(3460)
    return py_ass_parser3460


def _stringify(value):
    Sensor(3461)
    """Internal function."""
    Sensor(3462)
    if isinstance(value, (list, tuple)):
        Sensor(3463)
        if len(value) == 1:
            Sensor(3464)
            value = _stringify(value[0])
            Sensor(3465)
            if value[0] == '{':
                Sensor(3466)
                value = '{%s}' % value
        else:
            Sensor(3467)
            value = '{%s}' % _join(value)
    else:
        Sensor(3468)
        if isinstance(value, basestring):
            Sensor(3469)
            value = unicode(value)
        else:
            Sensor(3470)
            value = str(value)
        Sensor(3471)
        if not value:
            Sensor(3472)
            value = '{}'
        else:
            Sensor(3473)
            if _magic_re.search(value):
                Sensor(3474)
                value = _magic_re.sub('\\\\\\1', value)
                value = _space_re.sub('\\\\\\1', value)
            else:
                Sensor(3475)
                if value[0] == '"' or _space_re.search(value):
                    Sensor(3476)
                    value = '{%s}' % value
    py_ass_parser3477 = value
    Sensor(3477)
    return py_ass_parser3477


def _flatten(tuple):
    Sensor(3478)
    """Internal function."""
    res = ()
    Sensor(3479)
    for item in tuple:
        Sensor(3480)
        if type(item) in (TupleType, ListType):
            Sensor(3481)
            res = res + _flatten(item)
        else:
            Sensor(3482)
            if item is not None:
                Sensor(3483)
                res = res + (item,)
    py_ass_parser3484 = res
    Sensor(3484)
    return py_ass_parser3484


Sensor(3485)
try:
    Sensor(3486)
    _flatten = _tkinter._flatten
except AttributeError:
    Sensor(3487)
    pass


def _cnfmerge(cnfs):
    Sensor(3488)
    """Internal function."""
    Sensor(3489)
    if type(cnfs) is DictionaryType:
        py_ass_parser3490 = cnfs
        Sensor(3490)
        return py_ass_parser3490
    else:
        Sensor(3491)
        if type(cnfs) in (NoneType, StringType):
            py_ass_parser3492 = cnfs
            Sensor(3492)
            return py_ass_parser3492
        else:
            Sensor(3493)
            cnf = {}
            Sensor(3494)
            for c in _flatten(cnfs):
                Sensor(3495)
                try:
                    Sensor(3496)
                    cnf.update(c)
                except (AttributeError, TypeError) as msg:
                    Sensor(3497)
                    print '_cnfmerge: fallback due to:', msg
                    Sensor(3498)
                    for k, v in c.items():
                        Sensor(3499)
                        cnf[k] = v
            py_ass_parser3500 = cnf
            Sensor(3500)
            return py_ass_parser3500
    Sensor(3501)


Sensor(3502)
try:
    Sensor(3503)
    _cnfmerge = _tkinter._cnfmerge
except AttributeError:
    Sensor(3504)
    pass


class Event:
    """Container for the properties of an event.

    Instances of this type are generated if one of the following events occurs:

    KeyPress, KeyRelease - for keyboard events
    ButtonPress, ButtonRelease, Motion, Enter, Leave, MouseWheel - for mouse events
    Visibility, Unmap, Map, Expose, FocusIn, FocusOut, Circulate,
    Colormap, Gravity, Reparent, Property, Destroy, Activate,
    Deactivate - for window events.

    If a callback function for one of these events is registered
    using bind, bind_all, bind_class, or tag_bind, the callback is
    called with an Event as first argument. It will have the
    following attributes (in braces are the event types for which
    the attribute is valid):

        serial - serial number of event
    num - mouse button pressed (ButtonPress, ButtonRelease)
    focus - whether the window has the focus (Enter, Leave)
    height - height of the exposed window (Configure, Expose)
    width - width of the exposed window (Configure, Expose)
    keycode - keycode of the pressed key (KeyPress, KeyRelease)
    state - state of the event as a number (ButtonPress, ButtonRelease,
                            Enter, KeyPress, KeyRelease,
                            Leave, Motion)
    state - state as a string (Visibility)
    time - when the event occurred
    x - x-position of the mouse
    y - y-position of the mouse
    x_root - x-position of the mouse on the screen
             (ButtonPress, ButtonRelease, KeyPress, KeyRelease, Motion)
    y_root - y-position of the mouse on the screen
             (ButtonPress, ButtonRelease, KeyPress, KeyRelease, Motion)
    char - pressed character (KeyPress, KeyRelease)
    send_event - see X/Windows documentation
    keysym - keysym of the event as a string (KeyPress, KeyRelease)
    keysym_num - keysym of the event as a number (KeyPress, KeyRelease)
    type - type of the event as a number
    widget - widget in which the event occurred
    delta - delta of wheel movement (MouseWheel)
    """
    pass


Sensor(3505)
_support_default_root = 1
_default_root = None


def NoDefaultRoot():
    Sensor(3506)
    """Inhibit setting of default root window.

    Call this function to inhibit that the first instance of
    Tk is used for windows without an explicit parent window.
    """
    global _support_default_root
    _support_default_root = 0
    global _default_root
    _default_root = None
    del _default_root
    Sensor(3507)


def _tkerror(err):
    Sensor(3508)
    """Internal function."""
    pass
    Sensor(3509)


def _exit(code=0):
    Sensor(3510)
    """Internal function. Calling it will raise the exception SystemExit."""
    Sensor(3511)
    try:
        Sensor(3512)
        code = int(code)
    except ValueError:
        Sensor(3513)
        pass
    Sensor(3514)
    raise SystemExit, code


_varnum = 0


class Variable:
    """Class to define value holders for e.g. buttons.

    Subclasses StringVar, IntVar, DoubleVar, BooleanVar are specializations
    that constrain the type of the value returned from get()."""
    _default = ''

    def __init__(self, master=None, value=None, name=None):
        Sensor(3515)
        """Construct a variable

        MASTER can be given as master widget.
        VALUE is an optional value (defaults to "")
        NAME is an optional Tcl name (defaults to PY_VARnum).

        If NAME matches an existing variable and VALUE is omitted
        then the existing value is retained.
        """
        global _varnum
        Sensor(3516)
        if not master:
            Sensor(3517)
            master = _default_root
        Sensor(3518)
        self._master = master
        self._tk = master.tk
        Sensor(3519)
        if name:
            Sensor(3520)
            self._name = name
        else:
            Sensor(3521)
            self._name = 'PY_VAR' + repr(_varnum)
            _varnum += 1
        Sensor(3522)
        if value is not None:
            Sensor(3523)
            self.set(value)
        else:
            Sensor(3524)
            if not self._tk.call('info', 'exists', self._name):
                Sensor(3525)
                self.set(self._default)
        Sensor(3526)

    def __del__(self):
        Sensor(3527)
        """Unset the variable in Tcl."""
        self._tk.globalunsetvar(self._name)
        Sensor(3528)

    def __str__(self):
        Sensor(3529)
        """Return the name of the variable in Tcl."""
        py_ass_parser3530 = self._name
        Sensor(3530)
        return py_ass_parser3530

    def set(self, value):
        Sensor(3531)
        """Set the variable to VALUE."""
        py_ass_parser3532 = self._tk.globalsetvar(self._name, value)
        Sensor(3532)
        return py_ass_parser3532

    def get(self):
        Sensor(3533)
        """Return value of variable."""
        py_ass_parser3534 = self._tk.globalgetvar(self._name)
        Sensor(3534)
        return py_ass_parser3534

    def trace_variable(self, mode, callback):
        Sensor(3535)
        """Define a trace callback for the variable.

        MODE is one of "r", "w", "u" for read, write, undefine.
        CALLBACK must be a function which is called when
        the variable is read, written or undefined.

        Return the name of the callback.
        """
        cbname = self._master._register(callback)
        self._tk.call('trace', 'variable', self._name, mode, cbname)
        py_ass_parser3536 = cbname
        Sensor(3536)
        return py_ass_parser3536
    trace = trace_variable

    def trace_vdelete(self, mode, cbname):
        Sensor(3537)
        """Delete the trace callback for a variable.

        MODE is one of "r", "w", "u" for read, write, undefine.
        CBNAME is the name of the callback returned from trace_variable or trace.
        """
        self._tk.call('trace', 'vdelete', self._name, mode, cbname)
        self._master.deletecommand(cbname)
        Sensor(3538)

    def trace_vinfo(self):
        Sensor(3539)
        """Return all trace callback information."""
        py_ass_parser3540 = map(self._tk.split, self._tk.splitlist(self._tk
            .call('trace', 'vinfo', self._name)))
        Sensor(3540)
        return py_ass_parser3540

    def __eq__(self, other):
        Sensor(3541)
        """Comparison for equality (==).

        Note: if the Variable's master matters to behavior
        also compare self._master == other._master
        """
        py_ass_parser3542 = (self.__class__.__name__ == other.__class__.
            __name__ and self._name == other._name)
        Sensor(3542)
        return py_ass_parser3542


class StringVar(Variable):
    """Value holder for strings variables."""
    _default = ''

    def __init__(self, master=None, value=None, name=None):
        Sensor(3543)
        """Construct a string variable.

        MASTER can be given as master widget.
        VALUE is an optional value (defaults to "")
        NAME is an optional Tcl name (defaults to PY_VARnum).

        If NAME matches an existing variable and VALUE is omitted
        then the existing value is retained.
        """
        Variable.__init__(self, master, value, name)
        Sensor(3544)

    def get(self):
        Sensor(3545)
        """Return value of variable as string."""
        value = self._tk.globalgetvar(self._name)
        Sensor(3546)
        if isinstance(value, basestring):
            py_ass_parser3547 = value
            Sensor(3547)
            return py_ass_parser3547
        py_ass_parser3548 = str(value)
        Sensor(3548)
        return py_ass_parser3548


class IntVar(Variable):
    """Value holder for integer variables."""
    _default = 0

    def __init__(self, master=None, value=None, name=None):
        Sensor(3549)
        """Construct an integer variable.

        MASTER can be given as master widget.
        VALUE is an optional value (defaults to 0)
        NAME is an optional Tcl name (defaults to PY_VARnum).

        If NAME matches an existing variable and VALUE is omitted
        then the existing value is retained.
        """
        Variable.__init__(self, master, value, name)
        Sensor(3550)

    def set(self, value):
        Sensor(3551)
        """Set the variable to value, converting booleans to integers."""
        Sensor(3552)
        if isinstance(value, bool):
            Sensor(3553)
            value = int(value)
        py_ass_parser3554 = Variable.set(self, value)
        Sensor(3554)
        return py_ass_parser3554

    def get(self):
        Sensor(3555)
        """Return the value of the variable as an integer."""
        py_ass_parser3556 = getint(self._tk.globalgetvar(self._name))
        Sensor(3556)
        return py_ass_parser3556


class DoubleVar(Variable):
    """Value holder for float variables."""
    _default = 0.0

    def __init__(self, master=None, value=None, name=None):
        Sensor(3557)
        """Construct a float variable.

        MASTER can be given as master widget.
        VALUE is an optional value (defaults to 0.0)
        NAME is an optional Tcl name (defaults to PY_VARnum).

        If NAME matches an existing variable and VALUE is omitted
        then the existing value is retained.
        """
        Variable.__init__(self, master, value, name)
        Sensor(3558)

    def get(self):
        Sensor(3559)
        """Return the value of the variable as a float."""
        py_ass_parser3560 = getdouble(self._tk.globalgetvar(self._name))
        Sensor(3560)
        return py_ass_parser3560


class BooleanVar(Variable):
    """Value holder for boolean variables."""
    _default = False

    def __init__(self, master=None, value=None, name=None):
        Sensor(3561)
        """Construct a boolean variable.

        MASTER can be given as master widget.
        VALUE is an optional value (defaults to False)
        NAME is an optional Tcl name (defaults to PY_VARnum).

        If NAME matches an existing variable and VALUE is omitted
        then the existing value is retained.
        """
        Variable.__init__(self, master, value, name)
        Sensor(3562)

    def get(self):
        Sensor(3563)
        """Return the value of the variable as a bool."""
        py_ass_parser3564 = self._tk.getboolean(self._tk.globalgetvar(self.
            _name))
        Sensor(3564)
        return py_ass_parser3564


def mainloop(n=0):
    Sensor(3565)
    """Run the main loop of Tcl."""
    _default_root.tk.mainloop(n)
    Sensor(3566)


getint = int
getdouble = float


def getboolean(s):
    Sensor(3567)
    """Convert true and false to integer values 1 and 0."""
    py_ass_parser3568 = _default_root.tk.getboolean(s)
    Sensor(3568)
    return py_ass_parser3568


class Misc:
    """Internal class.

    Base class which defines methods common for interior widgets."""
    _tclCommands = None

    def destroy(self):
        Sensor(3569)
        """Internal function.

        Delete all Tcl commands created for
        this widget in the Tcl interpreter."""
        Sensor(3570)
        if self._tclCommands is not None:
            Sensor(3571)
            for name in self._tclCommands:
                Sensor(3572)
                self.tk.deletecommand(name)
            Sensor(3573)
            self._tclCommands = None
        Sensor(3574)

    def deletecommand(self, name):
        Sensor(3575)
        """Internal function.

        Delete the Tcl command provided in NAME."""
        self.tk.deletecommand(name)
        Sensor(3576)
        try:
            Sensor(3577)
            self._tclCommands.remove(name)
        except ValueError:
            Sensor(3578)
            pass
        Sensor(3579)

    def tk_strictMotif(self, boolean=None):
        Sensor(3580)
        """Set Tcl internal variable, whether the look and feel
        should adhere to Motif.

        A parameter of 1 means adhere to Motif (e.g. no color
        change if mouse passes over slider).
        Returns the set value."""
        py_ass_parser3581 = self.tk.getboolean(self.tk.call('set',
            'tk_strictMotif', boolean))
        Sensor(3581)
        return py_ass_parser3581

    def tk_bisque(self):
        Sensor(3582)
        """Change the color scheme to light brown as used in Tk 3.6 and before."""
        self.tk.call('tk_bisque')
        Sensor(3583)

    def tk_setPalette(self, *args, **kw):
        Sensor(3584)
        """Set a new color scheme for all widget elements.

        A single color as argument will cause that all colors of Tk
        widget elements are derived from this.
        Alternatively several keyword parameters and its associated
        colors can be given. The following keywords are valid:
        activeBackground, foreground, selectColor,
        activeForeground, highlightBackground, selectBackground,
        background, highlightColor, selectForeground,
        disabledForeground, insertBackground, troughColor."""
        self.tk.call(('tk_setPalette',) + _flatten(args) + _flatten(kw.items())
            )
        Sensor(3585)

    def tk_menuBar(self, *args):
        Sensor(3586)
        """Do not use. Needed in Tk 3.6 and earlier."""
        pass
        Sensor(3587)

    def wait_variable(self, name='PY_VAR'):
        Sensor(3588)
        """Wait until the variable is modified.

        A parameter of type IntVar, StringVar, DoubleVar or
        BooleanVar must be given."""
        self.tk.call('tkwait', 'variable', name)
        Sensor(3589)
    waitvar = wait_variable

    def wait_window(self, window=None):
        Sensor(3590)
        """Wait until a WIDGET is destroyed.

        If no parameter is given self is used."""
        Sensor(3591)
        if window is None:
            Sensor(3592)
            window = self
        Sensor(3593)
        self.tk.call('tkwait', 'window', window._w)
        Sensor(3594)

    def wait_visibility(self, window=None):
        Sensor(3595)
        """Wait until the visibility of a WIDGET changes
        (e.g. it appears).

        If no parameter is given self is used."""
        Sensor(3596)
        if window is None:
            Sensor(3597)
            window = self
        Sensor(3598)
        self.tk.call('tkwait', 'visibility', window._w)
        Sensor(3599)

    def setvar(self, name='PY_VAR', value='1'):
        Sensor(3600)
        """Set Tcl variable NAME to VALUE."""
        self.tk.setvar(name, value)
        Sensor(3601)

    def getvar(self, name='PY_VAR'):
        Sensor(3602)
        """Return value of Tcl variable NAME."""
        py_ass_parser3603 = self.tk.getvar(name)
        Sensor(3603)
        return py_ass_parser3603
    getint = int
    getdouble = float

    def getboolean(self, s):
        Sensor(3604)
        """Return a boolean value for Tcl boolean values true and false given as parameter."""
        py_ass_parser3605 = self.tk.getboolean(s)
        Sensor(3605)
        return py_ass_parser3605

    def focus_set(self):
        Sensor(3606)
        """Direct input focus to this widget.

        If the application currently does not have the focus
        this widget will get the focus if the application gets
        the focus through the window manager."""
        self.tk.call('focus', self._w)
        Sensor(3607)
    focus = focus_set

    def focus_force(self):
        Sensor(3608)
        """Direct input focus to this widget even if the
        application does not have the focus. Use with
        caution!"""
        self.tk.call('focus', '-force', self._w)
        Sensor(3609)

    def focus_get(self):
        Sensor(3610)
        """Return the widget which has currently the focus in the
        application.

        Use focus_displayof to allow working with several
        displays. Return None if application does not have
        the focus."""
        name = self.tk.call('focus')
        Sensor(3611)
        if name == 'none' or not name:
            py_ass_parser3612 = None
            Sensor(3612)
            return py_ass_parser3612
        py_ass_parser3613 = self._nametowidget(name)
        Sensor(3613)
        return py_ass_parser3613

    def focus_displayof(self):
        Sensor(3614)
        """Return the widget which has currently the focus on the
        display where this widget is located.

        Return None if the application does not have the focus."""
        name = self.tk.call('focus', '-displayof', self._w)
        Sensor(3615)
        if name == 'none' or not name:
            py_ass_parser3616 = None
            Sensor(3616)
            return py_ass_parser3616
        py_ass_parser3617 = self._nametowidget(name)
        Sensor(3617)
        return py_ass_parser3617

    def focus_lastfor(self):
        Sensor(3618)
        """Return the widget which would have the focus if top level
        for this widget gets the focus from the window manager."""
        name = self.tk.call('focus', '-lastfor', self._w)
        Sensor(3619)
        if name == 'none' or not name:
            py_ass_parser3620 = None
            Sensor(3620)
            return py_ass_parser3620
        py_ass_parser3621 = self._nametowidget(name)
        Sensor(3621)
        return py_ass_parser3621

    def tk_focusFollowsMouse(self):
        Sensor(3622)
        """The widget under mouse will get automatically focus. Can not
        be disabled easily."""
        self.tk.call('tk_focusFollowsMouse')
        Sensor(3623)

    def tk_focusNext(self):
        Sensor(3624)
        """Return the next widget in the focus order which follows
        widget which has currently the focus.

        The focus order first goes to the next child, then to
        the children of the child recursively and then to the
        next sibling which is higher in the stacking order.  A
        widget is omitted if it has the takefocus resource set
        to 0."""
        name = self.tk.call('tk_focusNext', self._w)
        Sensor(3625)
        if not name:
            py_ass_parser3626 = None
            Sensor(3626)
            return py_ass_parser3626
        py_ass_parser3627 = self._nametowidget(name)
        Sensor(3627)
        return py_ass_parser3627

    def tk_focusPrev(self):
        Sensor(3628)
        """Return previous widget in the focus order. See tk_focusNext for details."""
        name = self.tk.call('tk_focusPrev', self._w)
        Sensor(3629)
        if not name:
            py_ass_parser3630 = None
            Sensor(3630)
            return py_ass_parser3630
        py_ass_parser3631 = self._nametowidget(name)
        Sensor(3631)
        return py_ass_parser3631

    def after(self, ms, func=None, *args):
        Sensor(3632)
        """Call function once after given time.

        MS specifies the time in milliseconds. FUNC gives the
        function which shall be called. Additional parameters
        are given as parameters to the function call.  Return
        identifier to cancel scheduling with after_cancel."""
        Sensor(3633)
        if not func:
            Sensor(3634)
            self.tk.call('after', ms)
        else:

            def callit():
                Sensor(3635)
                try:
                    Sensor(3636)
                    func(*args)
                finally:
                    Sensor(3637)
                    try:
                        Sensor(3638)
                        self.deletecommand(name)
                    except TclError:
                        Sensor(3639)
                        pass
                Sensor(3640)
            Sensor(3641)
            name = self._register(callit)
            py_ass_parser3642 = self.tk.call('after', ms, name)
            Sensor(3642)
            return py_ass_parser3642
        Sensor(3643)

    def after_idle(self, func, *args):
        Sensor(3644)
        """Call FUNC once if the Tcl main loop has no event to
        process.

        Return an identifier to cancel the scheduling with
        after_cancel."""
        py_ass_parser3645 = self.after('idle', func, *args)
        Sensor(3645)
        return py_ass_parser3645

    def after_cancel(self, id):
        Sensor(3646)
        """Cancel scheduling of function identified with ID.

        Identifier returned by after or after_idle must be
        given as first parameter."""
        Sensor(3647)
        try:
            Sensor(3648)
            data = self.tk.call('after', 'info', id)
            script = self.tk.splitlist(data)[0]
            self.deletecommand(script)
        except TclError:
            Sensor(3649)
            pass
        Sensor(3650)
        self.tk.call('after', 'cancel', id)
        Sensor(3651)

    def bell(self, displayof=0):
        Sensor(3652)
        """Ring a display's bell."""
        self.tk.call(('bell',) + self._displayof(displayof))
        Sensor(3653)

    def clipboard_get(self, **kw):
        Sensor(3654)
        """Retrieve data from the clipboard on window's display.

        The window keyword defaults to the root window of the Tkinter
        application.

        The type keyword specifies the form in which the data is
        to be returned and should be an atom name such as STRING
        or FILE_NAME.  Type defaults to STRING, except on X11, where the default
        is to try UTF8_STRING and fall back to STRING.

        This command is equivalent to:

        selection_get(CLIPBOARD)
        """
        Sensor(3655)
        if 'type' not in kw and self._windowingsystem == 'x11':
            Sensor(3656)
            try:
                Sensor(3657)
                kw['type'] = 'UTF8_STRING'
                py_ass_parser3658 = self.tk.call(('clipboard', 'get') +
                    self._options(kw))
                Sensor(3658)
                return py_ass_parser3658
            except TclError:
                Sensor(3659)
                del kw['type']
        py_ass_parser3660 = self.tk.call(('clipboard', 'get') + self.
            _options(kw))
        Sensor(3660)
        return py_ass_parser3660

    def clipboard_clear(self, **kw):
        Sensor(3661)
        """Clear the data in the Tk clipboard.

        A widget specified for the optional displayof keyword
        argument specifies the target display."""
        Sensor(3662)
        if 'displayof' not in kw:
            Sensor(3663)
            kw['displayof'] = self._w
        Sensor(3664)
        self.tk.call(('clipboard', 'clear') + self._options(kw))
        Sensor(3665)

    def clipboard_append(self, string, **kw):
        Sensor(3666)
        """Append STRING to the Tk clipboard.

        A widget specified at the optional displayof keyword
        argument specifies the target display. The clipboard
        can be retrieved with selection_get."""
        Sensor(3667)
        if 'displayof' not in kw:
            Sensor(3668)
            kw['displayof'] = self._w
        Sensor(3669)
        self.tk.call(('clipboard', 'append') + self._options(kw) + ('--',
            string))
        Sensor(3670)

    def grab_current(self):
        Sensor(3671)
        """Return widget which has currently the grab in this application
        or None."""
        name = self.tk.call('grab', 'current', self._w)
        Sensor(3672)
        if not name:
            py_ass_parser3673 = None
            Sensor(3673)
            return py_ass_parser3673
        py_ass_parser3674 = self._nametowidget(name)
        Sensor(3674)
        return py_ass_parser3674

    def grab_release(self):
        Sensor(3675)
        """Release grab for this widget if currently set."""
        self.tk.call('grab', 'release', self._w)
        Sensor(3676)

    def grab_set(self):
        Sensor(3677)
        """Set grab for this widget.

        A grab directs all events to this and descendant
        widgets in the application."""
        self.tk.call('grab', 'set', self._w)
        Sensor(3678)

    def grab_set_global(self):
        Sensor(3679)
        """Set global grab for this widget.

        A global grab directs all events to this and
        descendant widgets on the display. Use with caution -
        other applications do not get events anymore."""
        self.tk.call('grab', 'set', '-global', self._w)
        Sensor(3680)

    def grab_status(self):
        Sensor(3681)
        """Return None, "local" or "global" if this widget has
        no, a local or a global grab."""
        status = self.tk.call('grab', 'status', self._w)
        Sensor(3682)
        if status == 'none':
            Sensor(3683)
            status = None
        py_ass_parser3684 = status
        Sensor(3684)
        return py_ass_parser3684

    def option_add(self, pattern, value, priority=None):
        Sensor(3685)
        """Set a VALUE (second parameter) for an option
        PATTERN (first parameter).

        An optional third parameter gives the numeric priority
        (defaults to 80)."""
        self.tk.call('option', 'add', pattern, value, priority)
        Sensor(3686)

    def option_clear(self):
        Sensor(3687)
        """Clear the option database.

        It will be reloaded if option_add is called."""
        self.tk.call('option', 'clear')
        Sensor(3688)

    def option_get(self, name, className):
        Sensor(3689)
        """Return the value for an option NAME for this widget
        with CLASSNAME.

        Values with higher priority override lower values."""
        py_ass_parser3690 = self.tk.call('option', 'get', self._w, name,
            className)
        Sensor(3690)
        return py_ass_parser3690

    def option_readfile(self, fileName, priority=None):
        Sensor(3691)
        """Read file FILENAME into the option database.

        An optional second parameter gives the numeric
        priority."""
        self.tk.call('option', 'readfile', fileName, priority)
        Sensor(3692)

    def selection_clear(self, **kw):
        Sensor(3693)
        """Clear the current X selection."""
        Sensor(3694)
        if 'displayof' not in kw:
            Sensor(3695)
            kw['displayof'] = self._w
        Sensor(3696)
        self.tk.call(('selection', 'clear') + self._options(kw))
        Sensor(3697)

    def selection_get(self, **kw):
        Sensor(3698)
        """Return the contents of the current X selection.

        A keyword parameter selection specifies the name of
        the selection and defaults to PRIMARY.  A keyword
        parameter displayof specifies a widget on the display
        to use. A keyword parameter type specifies the form of data to be
        fetched, defaulting to STRING except on X11, where UTF8_STRING is tried
        before STRING."""
        Sensor(3699)
        if 'displayof' not in kw:
            Sensor(3700)
            kw['displayof'] = self._w
        Sensor(3701)
        if 'type' not in kw and self._windowingsystem == 'x11':
            Sensor(3702)
            try:
                Sensor(3703)
                kw['type'] = 'UTF8_STRING'
                py_ass_parser3704 = self.tk.call(('selection', 'get') +
                    self._options(kw))
                Sensor(3704)
                return py_ass_parser3704
            except TclError:
                Sensor(3705)
                del kw['type']
        py_ass_parser3706 = self.tk.call(('selection', 'get') + self.
            _options(kw))
        Sensor(3706)
        return py_ass_parser3706

    def selection_handle(self, command, **kw):
        Sensor(3707)
        """Specify a function COMMAND to call if the X
        selection owned by this widget is queried by another
        application.

        This function must return the contents of the
        selection. The function will be called with the
        arguments OFFSET and LENGTH which allows the chunking
        of very long selections. The following keyword
        parameters can be provided:
        selection - name of the selection (default PRIMARY),
        type - type of the selection (e.g. STRING, FILE_NAME)."""
        name = self._register(command)
        self.tk.call(('selection', 'handle') + self._options(kw) + (self._w,
            name))
        Sensor(3708)

    def selection_own(self, **kw):
        Sensor(3709)
        """Become owner of X selection.

        A keyword parameter selection specifies the name of
        the selection (default PRIMARY)."""
        self.tk.call(('selection', 'own') + self._options(kw) + (self._w,))
        Sensor(3710)

    def selection_own_get(self, **kw):
        Sensor(3711)
        """Return owner of X selection.

        The following keyword parameter can
        be provided:
        selection - name of the selection (default PRIMARY),
        type - type of the selection (e.g. STRING, FILE_NAME)."""
        Sensor(3712)
        if 'displayof' not in kw:
            Sensor(3713)
            kw['displayof'] = self._w
        Sensor(3714)
        name = self.tk.call(('selection', 'own') + self._options(kw))
        Sensor(3715)
        if not name:
            py_ass_parser3716 = None
            Sensor(3716)
            return py_ass_parser3716
        py_ass_parser3717 = self._nametowidget(name)
        Sensor(3717)
        return py_ass_parser3717

    def send(self, interp, cmd, *args):
        Sensor(3718)
        """Send Tcl command CMD to different interpreter INTERP to be executed."""
        py_ass_parser3719 = self.tk.call(('send', interp, cmd) + args)
        Sensor(3719)
        return py_ass_parser3719

    def lower(self, belowThis=None):
        Sensor(3720)
        """Lower this widget in the stacking order."""
        self.tk.call('lower', self._w, belowThis)
        Sensor(3721)

    def tkraise(self, aboveThis=None):
        Sensor(3722)
        """Raise this widget in the stacking order."""
        self.tk.call('raise', self._w, aboveThis)
        Sensor(3723)
    lift = tkraise

    def colormodel(self, value=None):
        Sensor(3724)
        """Useless. Not implemented in Tk."""
        py_ass_parser3725 = self.tk.call('tk', 'colormodel', self._w, value)
        Sensor(3725)
        return py_ass_parser3725

    def winfo_atom(self, name, displayof=0):
        Sensor(3726)
        """Return integer which represents atom NAME."""
        args = ('winfo', 'atom') + self._displayof(displayof) + (name,)
        py_ass_parser3727 = getint(self.tk.call(args))
        Sensor(3727)
        return py_ass_parser3727

    def winfo_atomname(self, id, displayof=0):
        Sensor(3728)
        """Return name of atom with identifier ID."""
        args = ('winfo', 'atomname') + self._displayof(displayof) + (id,)
        py_ass_parser3729 = self.tk.call(args)
        Sensor(3729)
        return py_ass_parser3729

    def winfo_cells(self):
        Sensor(3730)
        """Return number of cells in the colormap for this widget."""
        py_ass_parser3731 = getint(self.tk.call('winfo', 'cells', self._w))
        Sensor(3731)
        return py_ass_parser3731

    def winfo_children(self):
        Sensor(3732)
        """Return a list of all widgets which are children of this widget."""
        result = []
        Sensor(3733)
        for child in self.tk.splitlist(self.tk.call('winfo', 'children',
            self._w)):
            Sensor(3734)
            try:
                Sensor(3735)
                result.append(self._nametowidget(child))
            except KeyError:
                Sensor(3736)
                pass
        py_ass_parser3737 = result
        Sensor(3737)
        return py_ass_parser3737

    def winfo_class(self):
        Sensor(3738)
        """Return window class name of this widget."""
        py_ass_parser3739 = self.tk.call('winfo', 'class', self._w)
        Sensor(3739)
        return py_ass_parser3739

    def winfo_colormapfull(self):
        Sensor(3740)
        """Return true if at the last color request the colormap was full."""
        py_ass_parser3741 = self.tk.getboolean(self.tk.call('winfo',
            'colormapfull', self._w))
        Sensor(3741)
        return py_ass_parser3741

    def winfo_containing(self, rootX, rootY, displayof=0):
        Sensor(3742)
        """Return the widget which is at the root coordinates ROOTX, ROOTY."""
        args = ('winfo', 'containing') + self._displayof(displayof) + (rootX,
            rootY)
        name = self.tk.call(args)
        Sensor(3743)
        if not name:
            py_ass_parser3744 = None
            Sensor(3744)
            return py_ass_parser3744
        py_ass_parser3745 = self._nametowidget(name)
        Sensor(3745)
        return py_ass_parser3745

    def winfo_depth(self):
        Sensor(3746)
        """Return the number of bits per pixel."""
        py_ass_parser3747 = getint(self.tk.call('winfo', 'depth', self._w))
        Sensor(3747)
        return py_ass_parser3747

    def winfo_exists(self):
        Sensor(3748)
        """Return true if this widget exists."""
        py_ass_parser3749 = getint(self.tk.call('winfo', 'exists', self._w))
        Sensor(3749)
        return py_ass_parser3749

    def winfo_fpixels(self, number):
        Sensor(3750)
        """Return the number of pixels for the given distance NUMBER
        (e.g. "3c") as float."""
        py_ass_parser3751 = getdouble(self.tk.call('winfo', 'fpixels', self
            ._w, number))
        Sensor(3751)
        return py_ass_parser3751

    def winfo_geometry(self):
        Sensor(3752)
        """Return geometry string for this widget in the form "widthxheight+X+Y"."""
        py_ass_parser3753 = self.tk.call('winfo', 'geometry', self._w)
        Sensor(3753)
        return py_ass_parser3753

    def winfo_height(self):
        Sensor(3754)
        """Return height of this widget."""
        py_ass_parser3755 = getint(self.tk.call('winfo', 'height', self._w))
        Sensor(3755)
        return py_ass_parser3755

    def winfo_id(self):
        Sensor(3756)
        """Return identifier ID for this widget."""
        py_ass_parser3757 = self.tk.getint(self.tk.call('winfo', 'id', self._w)
            )
        Sensor(3757)
        return py_ass_parser3757

    def winfo_interps(self, displayof=0):
        Sensor(3758)
        """Return the name of all Tcl interpreters for this display."""
        args = ('winfo', 'interps') + self._displayof(displayof)
        py_ass_parser3759 = self.tk.splitlist(self.tk.call(args))
        Sensor(3759)
        return py_ass_parser3759

    def winfo_ismapped(self):
        Sensor(3760)
        """Return true if this widget is mapped."""
        py_ass_parser3761 = getint(self.tk.call('winfo', 'ismapped', self._w))
        Sensor(3761)
        return py_ass_parser3761

    def winfo_manager(self):
        Sensor(3762)
        """Return the window mananger name for this widget."""
        py_ass_parser3763 = self.tk.call('winfo', 'manager', self._w)
        Sensor(3763)
        return py_ass_parser3763

    def winfo_name(self):
        Sensor(3764)
        """Return the name of this widget."""
        py_ass_parser3765 = self.tk.call('winfo', 'name', self._w)
        Sensor(3765)
        return py_ass_parser3765

    def winfo_parent(self):
        Sensor(3766)
        """Return the name of the parent of this widget."""
        py_ass_parser3767 = self.tk.call('winfo', 'parent', self._w)
        Sensor(3767)
        return py_ass_parser3767

    def winfo_pathname(self, id, displayof=0):
        Sensor(3768)
        """Return the pathname of the widget given by ID."""
        args = ('winfo', 'pathname') + self._displayof(displayof) + (id,)
        py_ass_parser3769 = self.tk.call(args)
        Sensor(3769)
        return py_ass_parser3769

    def winfo_pixels(self, number):
        Sensor(3770)
        """Rounded integer value of winfo_fpixels."""
        py_ass_parser3771 = getint(self.tk.call('winfo', 'pixels', self._w,
            number))
        Sensor(3771)
        return py_ass_parser3771

    def winfo_pointerx(self):
        Sensor(3772)
        """Return the x coordinate of the pointer on the root window."""
        py_ass_parser3773 = getint(self.tk.call('winfo', 'pointerx', self._w))
        Sensor(3773)
        return py_ass_parser3773

    def winfo_pointerxy(self):
        Sensor(3774)
        """Return a tuple of x and y coordinates of the pointer on the root window."""
        py_ass_parser3775 = self._getints(self.tk.call('winfo', 'pointerxy',
            self._w))
        Sensor(3775)
        return py_ass_parser3775

    def winfo_pointery(self):
        Sensor(3776)
        """Return the y coordinate of the pointer on the root window."""
        py_ass_parser3777 = getint(self.tk.call('winfo', 'pointery', self._w))
        Sensor(3777)
        return py_ass_parser3777

    def winfo_reqheight(self):
        Sensor(3778)
        """Return requested height of this widget."""
        py_ass_parser3779 = getint(self.tk.call('winfo', 'reqheight', self._w))
        Sensor(3779)
        return py_ass_parser3779

    def winfo_reqwidth(self):
        Sensor(3780)
        """Return requested width of this widget."""
        py_ass_parser3781 = getint(self.tk.call('winfo', 'reqwidth', self._w))
        Sensor(3781)
        return py_ass_parser3781

    def winfo_rgb(self, color):
        Sensor(3782)
        """Return tuple of decimal values for red, green, blue for
        COLOR in this widget."""
        py_ass_parser3783 = self._getints(self.tk.call('winfo', 'rgb', self
            ._w, color))
        Sensor(3783)
        return py_ass_parser3783

    def winfo_rootx(self):
        Sensor(3784)
        """Return x coordinate of upper left corner of this widget on the
        root window."""
        py_ass_parser3785 = getint(self.tk.call('winfo', 'rootx', self._w))
        Sensor(3785)
        return py_ass_parser3785

    def winfo_rooty(self):
        Sensor(3786)
        """Return y coordinate of upper left corner of this widget on the
        root window."""
        py_ass_parser3787 = getint(self.tk.call('winfo', 'rooty', self._w))
        Sensor(3787)
        return py_ass_parser3787

    def winfo_screen(self):
        Sensor(3788)
        """Return the screen name of this widget."""
        py_ass_parser3789 = self.tk.call('winfo', 'screen', self._w)
        Sensor(3789)
        return py_ass_parser3789

    def winfo_screencells(self):
        Sensor(3790)
        """Return the number of the cells in the colormap of the screen
        of this widget."""
        py_ass_parser3791 = getint(self.tk.call('winfo', 'screencells',
            self._w))
        Sensor(3791)
        return py_ass_parser3791

    def winfo_screendepth(self):
        Sensor(3792)
        """Return the number of bits per pixel of the root window of the
        screen of this widget."""
        py_ass_parser3793 = getint(self.tk.call('winfo', 'screendepth',
            self._w))
        Sensor(3793)
        return py_ass_parser3793

    def winfo_screenheight(self):
        Sensor(3794)
        """Return the number of pixels of the height of the screen of this widget
        in pixel."""
        py_ass_parser3795 = getint(self.tk.call('winfo', 'screenheight',
            self._w))
        Sensor(3795)
        return py_ass_parser3795

    def winfo_screenmmheight(self):
        Sensor(3796)
        """Return the number of pixels of the height of the screen of
        this widget in mm."""
        py_ass_parser3797 = getint(self.tk.call('winfo', 'screenmmheight',
            self._w))
        Sensor(3797)
        return py_ass_parser3797

    def winfo_screenmmwidth(self):
        Sensor(3798)
        """Return the number of pixels of the width of the screen of
        this widget in mm."""
        py_ass_parser3799 = getint(self.tk.call('winfo', 'screenmmwidth',
            self._w))
        Sensor(3799)
        return py_ass_parser3799

    def winfo_screenvisual(self):
        Sensor(3800)
        """Return one of the strings directcolor, grayscale, pseudocolor,
        staticcolor, staticgray, or truecolor for the default
        colormodel of this screen."""
        py_ass_parser3801 = self.tk.call('winfo', 'screenvisual', self._w)
        Sensor(3801)
        return py_ass_parser3801

    def winfo_screenwidth(self):
        Sensor(3802)
        """Return the number of pixels of the width of the screen of
        this widget in pixel."""
        py_ass_parser3803 = getint(self.tk.call('winfo', 'screenwidth',
            self._w))
        Sensor(3803)
        return py_ass_parser3803

    def winfo_server(self):
        Sensor(3804)
        """Return information of the X-Server of the screen of this widget in
        the form "XmajorRminor vendor vendorVersion"."""
        py_ass_parser3805 = self.tk.call('winfo', 'server', self._w)
        Sensor(3805)
        return py_ass_parser3805

    def winfo_toplevel(self):
        Sensor(3806)
        """Return the toplevel widget of this widget."""
        py_ass_parser3807 = self._nametowidget(self.tk.call('winfo',
            'toplevel', self._w))
        Sensor(3807)
        return py_ass_parser3807

    def winfo_viewable(self):
        Sensor(3808)
        """Return true if the widget and all its higher ancestors are mapped."""
        py_ass_parser3809 = getint(self.tk.call('winfo', 'viewable', self._w))
        Sensor(3809)
        return py_ass_parser3809

    def winfo_visual(self):
        Sensor(3810)
        """Return one of the strings directcolor, grayscale, pseudocolor,
        staticcolor, staticgray, or truecolor for the
        colormodel of this widget."""
        py_ass_parser3811 = self.tk.call('winfo', 'visual', self._w)
        Sensor(3811)
        return py_ass_parser3811

    def winfo_visualid(self):
        Sensor(3812)
        """Return the X identifier for the visual for this widget."""
        py_ass_parser3813 = self.tk.call('winfo', 'visualid', self._w)
        Sensor(3813)
        return py_ass_parser3813

    def winfo_visualsavailable(self, includeids=0):
        Sensor(3814)
        """Return a list of all visuals available for the screen
        of this widget.

        Each item in the list consists of a visual name (see winfo_visual), a
        depth and if INCLUDEIDS=1 is given also the X identifier."""
        data = self.tk.split(self.tk.call('winfo', 'visualsavailable', self
            ._w, includeids and 'includeids' or None))
        Sensor(3815)
        if type(data) is StringType:
            Sensor(3816)
            data = [self.tk.split(data)]
        py_ass_parser3817 = map(self.__winfo_parseitem, data)
        Sensor(3817)
        return py_ass_parser3817

    def __winfo_parseitem(self, t):
        Sensor(3818)
        """Internal function."""
        py_ass_parser3819 = t[:1] + tuple(map(self.__winfo_getint, t[1:]))
        Sensor(3819)
        return py_ass_parser3819

    def __winfo_getint(self, x):
        Sensor(3820)
        """Internal function."""
        py_ass_parser3821 = int(x, 0)
        Sensor(3821)
        return py_ass_parser3821

    def winfo_vrootheight(self):
        Sensor(3822)
        """Return the height of the virtual root window associated with this
        widget in pixels. If there is no virtual root window return the
        height of the screen."""
        py_ass_parser3823 = getint(self.tk.call('winfo', 'vrootheight',
            self._w))
        Sensor(3823)
        return py_ass_parser3823

    def winfo_vrootwidth(self):
        Sensor(3824)
        """Return the width of the virtual root window associated with this
        widget in pixel. If there is no virtual root window return the
        width of the screen."""
        py_ass_parser3825 = getint(self.tk.call('winfo', 'vrootwidth', self._w)
            )
        Sensor(3825)
        return py_ass_parser3825

    def winfo_vrootx(self):
        Sensor(3826)
        """Return the x offset of the virtual root relative to the root
        window of the screen of this widget."""
        py_ass_parser3827 = getint(self.tk.call('winfo', 'vrootx', self._w))
        Sensor(3827)
        return py_ass_parser3827

    def winfo_vrooty(self):
        Sensor(3828)
        """Return the y offset of the virtual root relative to the root
        window of the screen of this widget."""
        py_ass_parser3829 = getint(self.tk.call('winfo', 'vrooty', self._w))
        Sensor(3829)
        return py_ass_parser3829

    def winfo_width(self):
        Sensor(3830)
        """Return the width of this widget."""
        py_ass_parser3831 = getint(self.tk.call('winfo', 'width', self._w))
        Sensor(3831)
        return py_ass_parser3831

    def winfo_x(self):
        Sensor(3832)
        """Return the x coordinate of the upper left corner of this widget
        in the parent."""
        py_ass_parser3833 = getint(self.tk.call('winfo', 'x', self._w))
        Sensor(3833)
        return py_ass_parser3833

    def winfo_y(self):
        Sensor(3834)
        """Return the y coordinate of the upper left corner of this widget
        in the parent."""
        py_ass_parser3835 = getint(self.tk.call('winfo', 'y', self._w))
        Sensor(3835)
        return py_ass_parser3835

    def update(self):
        Sensor(3836)
        """Enter event loop until all pending events have been processed by Tcl."""
        self.tk.call('update')
        Sensor(3837)

    def update_idletasks(self):
        Sensor(3838)
        """Enter event loop until all idle callbacks have been called. This
        will update the display of windows but not process events caused by
        the user."""
        self.tk.call('update', 'idletasks')
        Sensor(3839)

    def bindtags(self, tagList=None):
        Sensor(3840)
        """Set or get the list of bindtags for this widget.

        With no argument return the list of all bindtags associated with
        this widget. With a list of strings as argument the bindtags are
        set to this list. The bindtags determine in which order events are
        processed (see bind)."""
        Sensor(3841)
        if tagList is None:
            py_ass_parser3842 = self.tk.splitlist(self.tk.call('bindtags',
                self._w))
            Sensor(3842)
            return py_ass_parser3842
        else:
            Sensor(3843)
            self.tk.call('bindtags', self._w, tagList)
        Sensor(3844)

    def _bind(self, what, sequence, func, add, needcleanup=1):
        Sensor(3845)
        """Internal function."""
        Sensor(3846)
        if type(func) is StringType:
            Sensor(3847)
            self.tk.call(what + (sequence, func))
        else:
            Sensor(3848)
            if func:
                Sensor(3849)
                funcid = self._register(func, self._substitute, needcleanup)
                cmd = '%sif {"[%s %s]" == "break"} break\n' % (add and '+' or
                    '', funcid, self._subst_format_str)
                self.tk.call(what + (sequence, cmd))
                py_ass_parser3850 = funcid
                Sensor(3850)
                return py_ass_parser3850
            else:
                Sensor(3851)
                if sequence:
                    py_ass_parser3852 = self.tk.call(what + (sequence,))
                    Sensor(3852)
                    return py_ass_parser3852
                else:
                    py_ass_parser3853 = self.tk.splitlist(self.tk.call(what))
                    Sensor(3853)
                    return py_ass_parser3853
        Sensor(3854)

    def bind(self, sequence=None, func=None, add=None):
        Sensor(3855)
        """Bind to this widget at event SEQUENCE a call to function FUNC.

        SEQUENCE is a string of concatenated event
        patterns. An event pattern is of the form
        <MODIFIER-MODIFIER-TYPE-DETAIL> where MODIFIER is one
        of Control, Mod2, M2, Shift, Mod3, M3, Lock, Mod4, M4,
        Button1, B1, Mod5, M5 Button2, B2, Meta, M, Button3,
        B3, Alt, Button4, B4, Double, Button5, B5 Triple,
        Mod1, M1. TYPE is one of Activate, Enter, Map,
        ButtonPress, Button, Expose, Motion, ButtonRelease
        FocusIn, MouseWheel, Circulate, FocusOut, Property,
        Colormap, Gravity Reparent, Configure, KeyPress, Key,
        Unmap, Deactivate, KeyRelease Visibility, Destroy,
        Leave and DETAIL is the button number for ButtonPress,
        ButtonRelease and DETAIL is the Keysym for KeyPress and
        KeyRelease. Examples are
        <Control-Button-1> for pressing Control and mouse button 1 or
        <Alt-A> for pressing A and the Alt key (KeyPress can be omitted).
        An event pattern can also be a virtual event of the form
        <<AString>> where AString can be arbitrary. This
        event can be generated by event_generate.
        If events are concatenated they must appear shortly
        after each other.

        FUNC will be called if the event sequence occurs with an
        instance of Event as argument. If the return value of FUNC is
        "break" no further bound function is invoked.

        An additional boolean parameter ADD specifies whether FUNC will
        be called additionally to the other bound function or whether
        it will replace the previous function.

        Bind will return an identifier to allow deletion of the bound function with
        unbind without memory leak.

        If FUNC or SEQUENCE is omitted the bound function or list
        of bound events are returned."""
        py_ass_parser3856 = self._bind(('bind', self._w), sequence, func, add)
        Sensor(3856)
        return py_ass_parser3856

    def unbind(self, sequence, funcid=None):
        Sensor(3857)
        """Unbind for this widget for event SEQUENCE  the
        function identified with FUNCID."""
        self.tk.call('bind', self._w, sequence, '')
        Sensor(3858)
        if funcid:
            Sensor(3859)
            self.deletecommand(funcid)
        Sensor(3860)

    def bind_all(self, sequence=None, func=None, add=None):
        Sensor(3861)
        """Bind to all widgets at an event SEQUENCE a call to function FUNC.
        An additional boolean parameter ADD specifies whether FUNC will
        be called additionally to the other bound function or whether
        it will replace the previous function. See bind for the return value."""
        py_ass_parser3862 = self._bind(('bind', 'all'), sequence, func, add, 0)
        Sensor(3862)
        return py_ass_parser3862

    def unbind_all(self, sequence):
        Sensor(3863)
        """Unbind for all widgets for event SEQUENCE all functions."""
        self.tk.call('bind', 'all', sequence, '')
        Sensor(3864)

    def bind_class(self, className, sequence=None, func=None, add=None):
        Sensor(3865)
        """Bind to widgets with bindtag CLASSNAME at event
        SEQUENCE a call of function FUNC. An additional
        boolean parameter ADD specifies whether FUNC will be
        called additionally to the other bound function or
        whether it will replace the previous function. See bind for
        the return value."""
        py_ass_parser3866 = self._bind(('bind', className), sequence, func,
            add, 0)
        Sensor(3866)
        return py_ass_parser3866

    def unbind_class(self, className, sequence):
        Sensor(3867)
        """Unbind for a all widgets with bindtag CLASSNAME for event SEQUENCE
        all functions."""
        self.tk.call('bind', className, sequence, '')
        Sensor(3868)

    def mainloop(self, n=0):
        Sensor(3869)
        """Call the mainloop of Tk."""
        self.tk.mainloop(n)
        Sensor(3870)

    def quit(self):
        Sensor(3871)
        """Quit the Tcl interpreter. All widgets will be destroyed."""
        self.tk.quit()
        Sensor(3872)

    def _getints(self, string):
        Sensor(3873)
        """Internal function."""
        Sensor(3874)
        if string:
            py_ass_parser3875 = tuple(map(getint, self.tk.splitlist(string)))
            Sensor(3875)
            return py_ass_parser3875
        Sensor(3876)

    def _getdoubles(self, string):
        Sensor(3877)
        """Internal function."""
        Sensor(3878)
        if string:
            py_ass_parser3879 = tuple(map(getdouble, self.tk.splitlist(string))
                )
            Sensor(3879)
            return py_ass_parser3879
        Sensor(3880)

    def _getboolean(self, string):
        Sensor(3881)
        """Internal function."""
        Sensor(3882)
        if string:
            py_ass_parser3883 = self.tk.getboolean(string)
            Sensor(3883)
            return py_ass_parser3883
        Sensor(3884)

    def _displayof(self, displayof):
        Sensor(3885)
        """Internal function."""
        Sensor(3886)
        if displayof:
            py_ass_parser3887 = '-displayof', displayof
            Sensor(3887)
            return py_ass_parser3887
        Sensor(3888)
        if displayof is None:
            py_ass_parser3889 = '-displayof', self._w
            Sensor(3889)
            return py_ass_parser3889
        py_ass_parser3890 = ()
        Sensor(3890)
        return py_ass_parser3890

    @property
    def _windowingsystem(self):
        Sensor(3891)
        """Internal function."""
        Sensor(3892)
        try:
            py_ass_parser3893 = self._root()._windowingsystem_cached
            Sensor(3893)
            return py_ass_parser3893
        except AttributeError:
            Sensor(3894)
            ws = self._root()._windowingsystem_cached = self.tk.call('tk',
                'windowingsystem')
            py_ass_parser3895 = ws
            Sensor(3895)
            return py_ass_parser3895
        Sensor(3896)

    def _options(self, cnf, kw=None):
        Sensor(3897)
        """Internal function."""
        Sensor(3898)
        if kw:
            Sensor(3899)
            cnf = _cnfmerge((cnf, kw))
        else:
            Sensor(3900)
            cnf = _cnfmerge(cnf)
        Sensor(3901)
        res = ()
        Sensor(3902)
        for k, v in cnf.items():
            Sensor(3903)
            if v is not None:
                Sensor(3904)
                if k[-1] == '_':
                    Sensor(3905)
                    k = k[:-1]
                Sensor(3906)
                if hasattr(v, '__call__'):
                    Sensor(3907)
                    v = self._register(v)
                else:
                    Sensor(3908)
                    if isinstance(v, (tuple, list)):
                        Sensor(3909)
                        nv = []
                        Sensor(3910)
                        for item in v:
                            Sensor(3911)
                            if not isinstance(item, (basestring, int)):
                                Sensor(3912)
                                break
                            else:
                                Sensor(3913)
                                if isinstance(item, int):
                                    Sensor(3914)
                                    nv.append('%d' % item)
                                else:
                                    Sensor(3915)
                                    nv.append(_stringify(item))
                        else:
                            Sensor(3916)
                            v = ' '.join(nv)
                Sensor(3917)
                res = res + ('-' + k, v)
        py_ass_parser3918 = res
        Sensor(3918)
        return py_ass_parser3918

    def nametowidget(self, name):
        Sensor(3919)
        """Return the Tkinter instance of a widget identified by
        its Tcl name NAME."""
        name = str(name).split('.')
        w = self
        Sensor(3920)
        if not name[0]:
            Sensor(3921)
            w = w._root()
            name = name[1:]
        Sensor(3922)
        for n in name:
            Sensor(3923)
            if not n:
                Sensor(3924)
                break
            Sensor(3925)
            w = w.children[n]
        py_ass_parser3926 = w
        Sensor(3926)
        return py_ass_parser3926
    _nametowidget = nametowidget

    def _register(self, func, subst=None, needcleanup=1):
        Sensor(3927)
        """Return a newly created Tcl function. If this
        function is called, the Python function FUNC will
        be executed. An optional function SUBST can
        be given which will be executed before FUNC."""
        f = CallWrapper(func, subst, self).__call__
        name = repr(id(f))
        Sensor(3928)
        try:
            Sensor(3929)
            func = func.im_func
        except AttributeError:
            Sensor(3930)
            pass
        Sensor(3931)
        try:
            Sensor(3932)
            name = name + func.__name__
        except AttributeError:
            Sensor(3933)
            pass
        Sensor(3934)
        self.tk.createcommand(name, f)
        Sensor(3935)
        if needcleanup:
            Sensor(3936)
            if self._tclCommands is None:
                Sensor(3937)
                self._tclCommands = []
            Sensor(3938)
            self._tclCommands.append(name)
        py_ass_parser3939 = name
        Sensor(3939)
        return py_ass_parser3939
    register = _register

    def _root(self):
        Sensor(3940)
        """Internal function."""
        w = self
        Sensor(3941)
        while w.master:
            Sensor(3942)
            w = w.master
        py_ass_parser3943 = w
        Sensor(3943)
        return py_ass_parser3943
    _subst_format = ('%#', '%b', '%f', '%h', '%k', '%s', '%t', '%w', '%x',
        '%y', '%A', '%E', '%K', '%N', '%W', '%T', '%X', '%Y', '%D')
    _subst_format_str = ' '.join(_subst_format)

    def _substitute(self, *args):
        Sensor(3944)
        """Internal function."""
        Sensor(3945)
        if len(args) != len(self._subst_format):
            py_ass_parser3946 = args
            Sensor(3946)
            return py_ass_parser3946
        Sensor(3947)
        getboolean = self.tk.getboolean
        getint = int

        def getint_event(s):
            Sensor(3948)
            """Tk changed behavior in 8.4.2, returning "??" rather more often."""
            Sensor(3949)
            try:
                py_ass_parser3950 = int(s)
                Sensor(3950)
                return py_ass_parser3950
            except ValueError:
                py_ass_parser3951 = s
                Sensor(3951)
                return py_ass_parser3951
            Sensor(3952)
        nsign, b, f, h, k, s, t, w, x, y, A, E, K, N, W, T, X, Y, D = args
        e = Event()
        e.serial = getint(nsign)
        e.num = getint_event(b)
        Sensor(3953)
        try:
            Sensor(3954)
            e.focus = getboolean(f)
        except TclError:
            Sensor(3955)
            pass
        Sensor(3956)
        e.height = getint_event(h)
        e.keycode = getint_event(k)
        e.state = getint_event(s)
        e.time = getint_event(t)
        e.width = getint_event(w)
        e.x = getint_event(x)
        e.y = getint_event(y)
        e.char = A
        Sensor(3957)
        try:
            Sensor(3958)
            e.send_event = getboolean(E)
        except TclError:
            Sensor(3959)
            pass
        Sensor(3960)
        e.keysym = K
        e.keysym_num = getint_event(N)
        e.type = T
        Sensor(3961)
        try:
            Sensor(3962)
            e.widget = self._nametowidget(W)
        except KeyError:
            Sensor(3963)
            e.widget = W
        Sensor(3964)
        e.x_root = getint_event(X)
        e.y_root = getint_event(Y)
        Sensor(3965)
        try:
            Sensor(3966)
            e.delta = getint(D)
        except ValueError:
            Sensor(3967)
            e.delta = 0
        py_ass_parser3968 = e,
        Sensor(3968)
        return py_ass_parser3968

    def _report_exception(self):
        Sensor(3969)
        """Internal function."""
        import sys
        exc, val, tb = sys.exc_type, sys.exc_value, sys.exc_traceback
        root = self._root()
        root.report_callback_exception(exc, val, tb)
        Sensor(3970)

    def _configure(self, cmd, cnf, kw):
        Sensor(3971)
        """Internal function."""
        Sensor(3972)
        if kw:
            Sensor(3973)
            cnf = _cnfmerge((cnf, kw))
        else:
            Sensor(3974)
            if cnf:
                Sensor(3975)
                cnf = _cnfmerge(cnf)
        Sensor(3976)
        if cnf is None:
            Sensor(3977)
            cnf = {}
            Sensor(3978)
            for x in self.tk.split(self.tk.call(_flatten((self._w, cmd)))):
                Sensor(3979)
                cnf[x[0][1:]] = (x[0][1:],) + x[1:]
            py_ass_parser3980 = cnf
            Sensor(3980)
            return py_ass_parser3980
        Sensor(3981)
        if type(cnf) is StringType:
            Sensor(3982)
            x = self.tk.split(self.tk.call(_flatten((self._w, cmd, '-' + cnf)))
                )
            py_ass_parser3983 = (x[0][1:],) + x[1:]
            Sensor(3983)
            return py_ass_parser3983
        Sensor(3984)
        self.tk.call(_flatten((self._w, cmd)) + self._options(cnf))
        Sensor(3985)

    def configure(self, cnf=None, **kw):
        Sensor(3986)
        """Configure resources of a widget.

        The values for resources are specified as keyword
        arguments. To get an overview about
        the allowed keyword arguments call the method keys.
        """
        py_ass_parser3987 = self._configure('configure', cnf, kw)
        Sensor(3987)
        return py_ass_parser3987
    config = configure

    def cget(self, key):
        Sensor(3988)
        """Return the resource value for a KEY given as string."""
        py_ass_parser3989 = self.tk.call(self._w, 'cget', '-' + key)
        Sensor(3989)
        return py_ass_parser3989
    __getitem__ = cget

    def __setitem__(self, key, value):
        Sensor(3990)
        self.configure({key: value})
        Sensor(3991)

    def __contains__(self, key):
        Sensor(3992)
        raise TypeError("Tkinter objects don't support 'in' tests.")

    def keys(self):
        Sensor(3993)
        """Return a list of all resource names of this widget."""
        py_ass_parser3994 = map(lambda x: x[0][1:], self.tk.split(self.tk.
            call(self._w, 'configure')))
        Sensor(3994)
        return py_ass_parser3994

    def __str__(self):
        Sensor(3995)
        """Return the window path name of this widget."""
        py_ass_parser3996 = self._w
        Sensor(3996)
        return py_ass_parser3996
    _noarg_ = ['_noarg_']

    def pack_propagate(self, flag=_noarg_):
        Sensor(3997)
        """Set or get the status for propagation of geometry information.

        A boolean argument specifies whether the geometry information
        of the slaves will determine the size of this widget. If no argument
        is given the current setting will be returned.
        """
        Sensor(3998)
        if flag is Misc._noarg_:
            py_ass_parser3999 = self._getboolean(self.tk.call('pack',
                'propagate', self._w))
            Sensor(3999)
            return py_ass_parser3999
        else:
            Sensor(4000)
            self.tk.call('pack', 'propagate', self._w, flag)
        Sensor(4001)
    propagate = pack_propagate

    def pack_slaves(self):
        Sensor(4002)
        """Return a list of all slaves of this widget
        in its packing order."""
        py_ass_parser4003 = map(self._nametowidget, self.tk.splitlist(self.
            tk.call('pack', 'slaves', self._w)))
        Sensor(4003)
        return py_ass_parser4003
    slaves = pack_slaves

    def place_slaves(self):
        Sensor(4004)
        """Return a list of all slaves of this widget
        in its packing order."""
        py_ass_parser4005 = map(self._nametowidget, self.tk.splitlist(self.
            tk.call('place', 'slaves', self._w)))
        Sensor(4005)
        return py_ass_parser4005

    def grid_bbox(self, column=None, row=None, col2=None, row2=None):
        Sensor(4006)
        """Return a tuple of integer coordinates for the bounding
        box of this widget controlled by the geometry manager grid.

        If COLUMN, ROW is given the bounding box applies from
        the cell with row and column 0 to the specified
        cell. If COL2 and ROW2 are given the bounding box
        starts at that cell.

        The returned integers specify the offset of the upper left
        corner in the master widget and the width and height.
        """
        args = 'grid', 'bbox', self._w
        Sensor(4007)
        if column is not None and row is not None:
            Sensor(4008)
            args = args + (column, row)
        Sensor(4009)
        if col2 is not None and row2 is not None:
            Sensor(4010)
            args = args + (col2, row2)
        py_ass_parser4011 = self._getints(self.tk.call(*args)) or None
        Sensor(4011)
        return py_ass_parser4011
    bbox = grid_bbox

    def _grid_configure(self, command, index, cnf, kw):
        Sensor(4012)
        """Internal function."""
        Sensor(4013)
        if type(cnf) is StringType and not kw:
            Sensor(4014)
            if cnf[-1:] == '_':
                Sensor(4015)
                cnf = cnf[:-1]
            Sensor(4016)
            if cnf[:1] != '-':
                Sensor(4017)
                cnf = '-' + cnf
            Sensor(4018)
            options = cnf,
        else:
            Sensor(4019)
            options = self._options(cnf, kw)
        Sensor(4020)
        if not options:
            Sensor(4021)
            res = self.tk.call('grid', command, self._w, index)
            words = self.tk.splitlist(res)
            dict = {}
            Sensor(4022)
            for i in range(0, len(words), 2):
                Sensor(4023)
                key = words[i][1:]
                value = words[i + 1]
                Sensor(4024)
                if not value:
                    Sensor(4025)
                    value = None
                else:
                    Sensor(4026)
                    if '.' in value:
                        Sensor(4027)
                        value = getdouble(value)
                    else:
                        Sensor(4028)
                        value = getint(value)
                Sensor(4029)
                dict[key] = value
            py_ass_parser4030 = dict
            Sensor(4030)
            return py_ass_parser4030
        Sensor(4031)
        res = self.tk.call(('grid', command, self._w, index) + options)
        Sensor(4032)
        if len(options) == 1:
            Sensor(4033)
            if not res:
                py_ass_parser4034 = None
                Sensor(4034)
                return py_ass_parser4034
            Sensor(4035)
            if '.' in res:
                py_ass_parser4036 = getdouble(res)
                Sensor(4036)
                return py_ass_parser4036
            py_ass_parser4037 = getint(res)
            Sensor(4037)
            return py_ass_parser4037
        Sensor(4038)

    def grid_columnconfigure(self, index, cnf={}, **kw):
        Sensor(4039)
        """Configure column INDEX of a grid.

        Valid resources are minsize (minimum size of the column),
        weight (how much does additional space propagate to this column)
        and pad (how much space to let additionally)."""
        py_ass_parser4040 = self._grid_configure('columnconfigure', index,
            cnf, kw)
        Sensor(4040)
        return py_ass_parser4040
    columnconfigure = grid_columnconfigure

    def grid_location(self, x, y):
        Sensor(4041)
        """Return a tuple of column and row which identify the cell
        at which the pixel at position X and Y inside the master
        widget is located."""
        py_ass_parser4042 = self._getints(self.tk.call('grid', 'location',
            self._w, x, y)) or None
        Sensor(4042)
        return py_ass_parser4042

    def grid_propagate(self, flag=_noarg_):
        Sensor(4043)
        """Set or get the status for propagation of geometry information.

        A boolean argument specifies whether the geometry information
        of the slaves will determine the size of this widget. If no argument
        is given, the current setting will be returned.
        """
        Sensor(4044)
        if flag is Misc._noarg_:
            py_ass_parser4045 = self._getboolean(self.tk.call('grid',
                'propagate', self._w))
            Sensor(4045)
            return py_ass_parser4045
        else:
            Sensor(4046)
            self.tk.call('grid', 'propagate', self._w, flag)
        Sensor(4047)

    def grid_rowconfigure(self, index, cnf={}, **kw):
        Sensor(4048)
        """Configure row INDEX of a grid.

        Valid resources are minsize (minimum size of the row),
        weight (how much does additional space propagate to this row)
        and pad (how much space to let additionally)."""
        py_ass_parser4049 = self._grid_configure('rowconfigure', index, cnf, kw
            )
        Sensor(4049)
        return py_ass_parser4049
    rowconfigure = grid_rowconfigure

    def grid_size(self):
        Sensor(4050)
        """Return a tuple of the number of column and rows in the grid."""
        py_ass_parser4051 = self._getints(self.tk.call('grid', 'size', self._w)
            ) or None
        Sensor(4051)
        return py_ass_parser4051
    size = grid_size

    def grid_slaves(self, row=None, column=None):
        Sensor(4052)
        """Return a list of all slaves of this widget
        in its packing order."""
        args = ()
        Sensor(4053)
        if row is not None:
            Sensor(4054)
            args = args + ('-row', row)
        Sensor(4055)
        if column is not None:
            Sensor(4056)
            args = args + ('-column', column)
        py_ass_parser4057 = map(self._nametowidget, self.tk.splitlist(self.
            tk.call(('grid', 'slaves', self._w) + args)))
        Sensor(4057)
        return py_ass_parser4057

    def event_add(self, virtual, *sequences):
        Sensor(4058)
        """Bind a virtual event VIRTUAL (of the form <<Name>>)
        to an event SEQUENCE such that the virtual event is triggered
        whenever SEQUENCE occurs."""
        args = ('event', 'add', virtual) + sequences
        self.tk.call(args)
        Sensor(4059)

    def event_delete(self, virtual, *sequences):
        Sensor(4060)
        """Unbind a virtual event VIRTUAL from SEQUENCE."""
        args = ('event', 'delete', virtual) + sequences
        self.tk.call(args)
        Sensor(4061)

    def event_generate(self, sequence, **kw):
        Sensor(4062)
        """Generate an event SEQUENCE. Additional
        keyword arguments specify parameter of the event
        (e.g. x, y, rootx, rooty)."""
        args = 'event', 'generate', self._w, sequence
        Sensor(4063)
        for k, v in kw.items():
            Sensor(4064)
            args = args + ('-%s' % k, str(v))
        Sensor(4065)
        self.tk.call(args)
        Sensor(4066)

    def event_info(self, virtual=None):
        Sensor(4067)
        """Return a list of all virtual events or the information
        about the SEQUENCE bound to the virtual event VIRTUAL."""
        py_ass_parser4068 = self.tk.splitlist(self.tk.call('event', 'info',
            virtual))
        Sensor(4068)
        return py_ass_parser4068

    def image_names(self):
        Sensor(4069)
        """Return a list of all existing image names."""
        py_ass_parser4070 = self.tk.call('image', 'names')
        Sensor(4070)
        return py_ass_parser4070

    def image_types(self):
        Sensor(4071)
        """Return a list of all available image types (e.g. phote bitmap)."""
        py_ass_parser4072 = self.tk.call('image', 'types')
        Sensor(4072)
        return py_ass_parser4072


class CallWrapper:
    """Internal class. Stores function to call when some user
    defined Tcl function is called e.g. after an event occurred."""

    def __init__(self, func, subst, widget):
        Sensor(4073)
        """Store FUNC, SUBST and WIDGET as members."""
        self.func = func
        self.subst = subst
        self.widget = widget
        Sensor(4074)

    def __call__(self, *args):
        Sensor(4075)
        """Apply first function SUBST to arguments, than FUNC."""
        Sensor(4076)
        try:
            Sensor(4077)
            if self.subst:
                Sensor(4078)
                args = self.subst(*args)
            py_ass_parser4079 = self.func(*args)
            Sensor(4079)
            return py_ass_parser4079
        except SystemExit as msg:
            Sensor(4080)
            raise SystemExit, msg
        except:
            Sensor(4081)
            self.widget._report_exception()
        Sensor(4082)


class XView:
    """Mix-in class for querying and changing the horizontal position
    of a widget's window."""

    def xview(self, *args):
        Sensor(4083)
        """Query and change the horizontal position of the view."""
        res = self.tk.call(self._w, 'xview', *args)
        Sensor(4084)
        if not args:
            py_ass_parser4085 = self._getdoubles(res)
            Sensor(4085)
            return py_ass_parser4085
        Sensor(4086)

    def xview_moveto(self, fraction):
        Sensor(4087)
        """Adjusts the view in the window so that FRACTION of the
        total width of the canvas is off-screen to the left."""
        self.tk.call(self._w, 'xview', 'moveto', fraction)
        Sensor(4088)

    def xview_scroll(self, number, what):
        Sensor(4089)
        """Shift the x-view according to NUMBER which is measured in "units"
        or "pages" (WHAT)."""
        self.tk.call(self._w, 'xview', 'scroll', number, what)
        Sensor(4090)


class YView:
    """Mix-in class for querying and changing the vertical position
    of a widget's window."""

    def yview(self, *args):
        Sensor(4091)
        """Query and change the vertical position of the view."""
        res = self.tk.call(self._w, 'yview', *args)
        Sensor(4092)
        if not args:
            py_ass_parser4093 = self._getdoubles(res)
            Sensor(4093)
            return py_ass_parser4093
        Sensor(4094)

    def yview_moveto(self, fraction):
        Sensor(4095)
        """Adjusts the view in the window so that FRACTION of the
        total height of the canvas is off-screen to the top."""
        self.tk.call(self._w, 'yview', 'moveto', fraction)
        Sensor(4096)

    def yview_scroll(self, number, what):
        Sensor(4097)
        """Shift the y-view according to NUMBER which is measured in
        "units" or "pages" (WHAT)."""
        self.tk.call(self._w, 'yview', 'scroll', number, what)
        Sensor(4098)


class Wm:
    """Provides functions for the communication with the window manager."""

    def wm_aspect(self, minNumer=None, minDenom=None, maxNumer=None,
        maxDenom=None):
        Sensor(4099)
        """Instruct the window manager to set the aspect ratio (width/height)
        of this widget to be between MINNUMER/MINDENOM and MAXNUMER/MAXDENOM. Return a tuple
        of the actual values if no argument is given."""
        py_ass_parser4100 = self._getints(self.tk.call('wm', 'aspect', self
            ._w, minNumer, minDenom, maxNumer, maxDenom))
        Sensor(4100)
        return py_ass_parser4100
    aspect = wm_aspect

    def wm_attributes(self, *args):
        Sensor(4101)
        """This subcommand returns or sets platform specific attributes

        The first form returns a list of the platform specific flags and
        their values. The second form returns the value for the specific
        option. The third form sets one or more of the values. The values
        are as follows:

        On Windows, -disabled gets or sets whether the window is in a
        disabled state. -toolwindow gets or sets the style of the window
        to toolwindow (as defined in the MSDN). -topmost gets or sets
        whether this is a topmost window (displays above all other
        windows).

        On Macintosh, XXXXX

        On Unix, there are currently no special attribute values.
        """
        args = ('wm', 'attributes', self._w) + args
        py_ass_parser4102 = self.tk.call(args)
        Sensor(4102)
        return py_ass_parser4102
    attributes = wm_attributes

    def wm_client(self, name=None):
        Sensor(4103)
        """Store NAME in WM_CLIENT_MACHINE property of this widget. Return
        current value."""
        py_ass_parser4104 = self.tk.call('wm', 'client', self._w, name)
        Sensor(4104)
        return py_ass_parser4104
    client = wm_client

    def wm_colormapwindows(self, *wlist):
        Sensor(4105)
        """Store list of window names (WLIST) into WM_COLORMAPWINDOWS property
        of this widget. This list contains windows whose colormaps differ from their
        parents. Return current list of widgets if WLIST is empty."""
        Sensor(4106)
        if len(wlist) > 1:
            Sensor(4107)
            wlist = wlist,
        Sensor(4108)
        args = ('wm', 'colormapwindows', self._w) + wlist
        py_ass_parser4109 = map(self._nametowidget, self.tk.call(args))
        Sensor(4109)
        return py_ass_parser4109
    colormapwindows = wm_colormapwindows

    def wm_command(self, value=None):
        Sensor(4110)
        """Store VALUE in WM_COMMAND property. It is the command
        which shall be used to invoke the application. Return current
        command if VALUE is None."""
        py_ass_parser4111 = self.tk.call('wm', 'command', self._w, value)
        Sensor(4111)
        return py_ass_parser4111
    command = wm_command

    def wm_deiconify(self):
        Sensor(4112)
        """Deiconify this widget. If it was never mapped it will not be mapped.
        On Windows it will raise this widget and give it the focus."""
        py_ass_parser4113 = self.tk.call('wm', 'deiconify', self._w)
        Sensor(4113)
        return py_ass_parser4113
    deiconify = wm_deiconify

    def wm_focusmodel(self, model=None):
        Sensor(4114)
        """Set focus model to MODEL. "active" means that this widget will claim
        the focus itself, "passive" means that the window manager shall give
        the focus. Return current focus model if MODEL is None."""
        py_ass_parser4115 = self.tk.call('wm', 'focusmodel', self._w, model)
        Sensor(4115)
        return py_ass_parser4115
    focusmodel = wm_focusmodel

    def wm_frame(self):
        Sensor(4116)
        """Return identifier for decorative frame of this widget if present."""
        py_ass_parser4117 = self.tk.call('wm', 'frame', self._w)
        Sensor(4117)
        return py_ass_parser4117
    frame = wm_frame

    def wm_geometry(self, newGeometry=None):
        Sensor(4118)
        """Set geometry to NEWGEOMETRY of the form =widthxheight+x+y. Return
        current value if None is given."""
        py_ass_parser4119 = self.tk.call('wm', 'geometry', self._w, newGeometry
            )
        Sensor(4119)
        return py_ass_parser4119
    geometry = wm_geometry

    def wm_grid(self, baseWidth=None, baseHeight=None, widthInc=None,
        heightInc=None):
        Sensor(4120)
        """Instruct the window manager that this widget shall only be
        resized on grid boundaries. WIDTHINC and HEIGHTINC are the width and
        height of a grid unit in pixels. BASEWIDTH and BASEHEIGHT are the
        number of grid units requested in Tk_GeometryRequest."""
        py_ass_parser4121 = self._getints(self.tk.call('wm', 'grid', self.
            _w, baseWidth, baseHeight, widthInc, heightInc))
        Sensor(4121)
        return py_ass_parser4121
    grid = wm_grid

    def wm_group(self, pathName=None):
        Sensor(4122)
        """Set the group leader widgets for related widgets to PATHNAME. Return
        the group leader of this widget if None is given."""
        py_ass_parser4123 = self.tk.call('wm', 'group', self._w, pathName)
        Sensor(4123)
        return py_ass_parser4123
    group = wm_group

    def wm_iconbitmap(self, bitmap=None, default=None):
        Sensor(4124)
        """Set bitmap for the iconified widget to BITMAP. Return
        the bitmap if None is given.

        Under Windows, the DEFAULT parameter can be used to set the icon
        for the widget and any descendents that don't have an icon set
        explicitly.  DEFAULT can be the relative path to a .ico file
        (example: root.iconbitmap(default='myicon.ico') ).  See Tk
        documentation for more information."""
        Sensor(4125)
        if default:
            py_ass_parser4126 = self.tk.call('wm', 'iconbitmap', self._w,
                '-default', default)
            Sensor(4126)
            return py_ass_parser4126
        else:
            py_ass_parser4127 = self.tk.call('wm', 'iconbitmap', self._w,
                bitmap)
            Sensor(4127)
            return py_ass_parser4127
        Sensor(4128)
    iconbitmap = wm_iconbitmap

    def wm_iconify(self):
        Sensor(4129)
        """Display widget as icon."""
        py_ass_parser4130 = self.tk.call('wm', 'iconify', self._w)
        Sensor(4130)
        return py_ass_parser4130
    iconify = wm_iconify

    def wm_iconmask(self, bitmap=None):
        Sensor(4131)
        """Set mask for the icon bitmap of this widget. Return the
        mask if None is given."""
        py_ass_parser4132 = self.tk.call('wm', 'iconmask', self._w, bitmap)
        Sensor(4132)
        return py_ass_parser4132
    iconmask = wm_iconmask

    def wm_iconname(self, newName=None):
        Sensor(4133)
        """Set the name of the icon for this widget. Return the name if
        None is given."""
        py_ass_parser4134 = self.tk.call('wm', 'iconname', self._w, newName)
        Sensor(4134)
        return py_ass_parser4134
    iconname = wm_iconname

    def wm_iconposition(self, x=None, y=None):
        Sensor(4135)
        """Set the position of the icon of this widget to X and Y. Return
        a tuple of the current values of X and X if None is given."""
        py_ass_parser4136 = self._getints(self.tk.call('wm', 'iconposition',
            self._w, x, y))
        Sensor(4136)
        return py_ass_parser4136
    iconposition = wm_iconposition

    def wm_iconwindow(self, pathName=None):
        Sensor(4137)
        """Set widget PATHNAME to be displayed instead of icon. Return the current
        value if None is given."""
        py_ass_parser4138 = self.tk.call('wm', 'iconwindow', self._w, pathName)
        Sensor(4138)
        return py_ass_parser4138
    iconwindow = wm_iconwindow

    def wm_maxsize(self, width=None, height=None):
        Sensor(4139)
        """Set max WIDTH and HEIGHT for this widget. If the window is gridded
        the values are given in grid units. Return the current values if None
        is given."""
        py_ass_parser4140 = self._getints(self.tk.call('wm', 'maxsize',
            self._w, width, height))
        Sensor(4140)
        return py_ass_parser4140
    maxsize = wm_maxsize

    def wm_minsize(self, width=None, height=None):
        Sensor(4141)
        """Set min WIDTH and HEIGHT for this widget. If the window is gridded
        the values are given in grid units. Return the current values if None
        is given."""
        py_ass_parser4142 = self._getints(self.tk.call('wm', 'minsize',
            self._w, width, height))
        Sensor(4142)
        return py_ass_parser4142
    minsize = wm_minsize

    def wm_overrideredirect(self, boolean=None):
        Sensor(4143)
        """Instruct the window manager to ignore this widget
        if BOOLEAN is given with 1. Return the current value if None
        is given."""
        py_ass_parser4144 = self._getboolean(self.tk.call('wm',
            'overrideredirect', self._w, boolean))
        Sensor(4144)
        return py_ass_parser4144
    overrideredirect = wm_overrideredirect

    def wm_positionfrom(self, who=None):
        Sensor(4145)
        """Instruct the window manager that the position of this widget shall
        be defined by the user if WHO is "user", and by its own policy if WHO is
        "program"."""
        py_ass_parser4146 = self.tk.call('wm', 'positionfrom', self._w, who)
        Sensor(4146)
        return py_ass_parser4146
    positionfrom = wm_positionfrom

    def wm_protocol(self, name=None, func=None):
        Sensor(4147)
        """Bind function FUNC to command NAME for this widget.
        Return the function bound to NAME if None is given. NAME could be
        e.g. "WM_SAVE_YOURSELF" or "WM_DELETE_WINDOW"."""
        Sensor(4148)
        if hasattr(func, '__call__'):
            Sensor(4149)
            command = self._register(func)
        else:
            Sensor(4150)
            command = func
        py_ass_parser4151 = self.tk.call('wm', 'protocol', self._w, name,
            command)
        Sensor(4151)
        return py_ass_parser4151
    protocol = wm_protocol

    def wm_resizable(self, width=None, height=None):
        Sensor(4152)
        """Instruct the window manager whether this width can be resized
        in WIDTH or HEIGHT. Both values are boolean values."""
        py_ass_parser4153 = self.tk.call('wm', 'resizable', self._w, width,
            height)
        Sensor(4153)
        return py_ass_parser4153
    resizable = wm_resizable

    def wm_sizefrom(self, who=None):
        Sensor(4154)
        """Instruct the window manager that the size of this widget shall
        be defined by the user if WHO is "user", and by its own policy if WHO is
        "program"."""
        py_ass_parser4155 = self.tk.call('wm', 'sizefrom', self._w, who)
        Sensor(4155)
        return py_ass_parser4155
    sizefrom = wm_sizefrom

    def wm_state(self, newstate=None):
        Sensor(4156)
        """Query or set the state of this widget as one of normal, icon,
        iconic (see wm_iconwindow), withdrawn, or zoomed (Windows only)."""
        py_ass_parser4157 = self.tk.call('wm', 'state', self._w, newstate)
        Sensor(4157)
        return py_ass_parser4157
    state = wm_state

    def wm_title(self, string=None):
        Sensor(4158)
        """Set the title of this widget."""
        py_ass_parser4159 = self.tk.call('wm', 'title', self._w, string)
        Sensor(4159)
        return py_ass_parser4159
    title = wm_title

    def wm_transient(self, master=None):
        Sensor(4160)
        """Instruct the window manager that this widget is transient
        with regard to widget MASTER."""
        py_ass_parser4161 = self.tk.call('wm', 'transient', self._w, master)
        Sensor(4161)
        return py_ass_parser4161
    transient = wm_transient

    def wm_withdraw(self):
        Sensor(4162)
        """Withdraw this widget from the screen such that it is unmapped
        and forgotten by the window manager. Re-draw it with wm_deiconify."""
        py_ass_parser4163 = self.tk.call('wm', 'withdraw', self._w)
        Sensor(4163)
        return py_ass_parser4163
    withdraw = wm_withdraw


class Tk(Misc, Wm):
    """Toplevel widget of Tk which represents mostly the main window
    of an application. It has an associated Tcl interpreter."""
    _w = '.'

    def __init__(self, screenName=None, baseName=None, className='Tk',
        useTk=1, sync=0, use=None):
        Sensor(4164)
        """Return a new Toplevel widget on screen SCREENNAME. A new Tcl interpreter will
        be created. BASENAME will be used for the identification of the profile file (see
        readprofile).
        It is constructed from sys.argv[0] without extensions if None is given. CLASSNAME
        is the name of the widget class."""
        self.master = None
        self.children = {}
        self._tkloaded = 0
        self.tk = None
        Sensor(4165)
        if baseName is None:
            import sys, os
            Sensor(4166)
            baseName = os.path.basename(sys.argv[0])
            baseName, ext = os.path.splitext(baseName)
            Sensor(4167)
            if ext not in ('.py', '.pyc', '.pyo'):
                Sensor(4168)
                baseName = baseName + ext
        Sensor(4169)
        interactive = 0
        self.tk = _tkinter.create(screenName, baseName, className,
            interactive, wantobjects, useTk, sync, use)
        Sensor(4170)
        if useTk:
            Sensor(4171)
            self._loadtk()
        Sensor(4172)
        if not sys.flags.ignore_environment:
            Sensor(4173)
            self.readprofile(baseName, className)
        Sensor(4174)

    def loadtk(self):
        Sensor(4175)
        if not self._tkloaded:
            Sensor(4176)
            self.tk.loadtk()
            self._loadtk()
        Sensor(4177)

    def _loadtk(self):
        Sensor(4178)
        self._tkloaded = 1
        global _default_root
        tk_version = self.tk.getvar('tk_version')
        Sensor(4179)
        if tk_version != _tkinter.TK_VERSION:
            Sensor(4180)
            raise RuntimeError, "tk.h version (%s) doesn't match libtk.a version (%s)" % (
                _tkinter.TK_VERSION, tk_version)
        Sensor(4181)
        tcl_version = str(self.tk.getvar('tcl_version'))
        Sensor(4182)
        if tcl_version != _tkinter.TCL_VERSION:
            Sensor(4183)
            raise RuntimeError, "tcl.h version (%s) doesn't match libtcl.a version (%s)" % (
                _tkinter.TCL_VERSION, tcl_version)
        Sensor(4184)
        if TkVersion < 4.0:
            Sensor(4185)
            raise RuntimeError, 'Tk 4.0 or higher is required; found Tk %s' % str(
                TkVersion)
        Sensor(4186)
        if self._tclCommands is None:
            Sensor(4187)
            self._tclCommands = []
        Sensor(4188)
        self.tk.createcommand('tkerror', _tkerror)
        self.tk.createcommand('exit', _exit)
        self._tclCommands.append('tkerror')
        self._tclCommands.append('exit')
        Sensor(4189)
        if _support_default_root and not _default_root:
            Sensor(4190)
            _default_root = self
        Sensor(4191)
        self.protocol('WM_DELETE_WINDOW', self.destroy)
        Sensor(4192)

    def destroy(self):
        Sensor(4193)
        """Destroy this and all descendants widgets. This will
        end the application of this Tcl interpreter."""
        Sensor(4194)
        for c in self.children.values():
            Sensor(4195)
            c.destroy()
        Sensor(4196)
        self.tk.call('destroy', self._w)
        Misc.destroy(self)
        global _default_root
        Sensor(4197)
        if _support_default_root and _default_root is self:
            Sensor(4198)
            _default_root = None
        Sensor(4199)

    def readprofile(self, baseName, className):
        Sensor(4200)
        """Internal function. It reads BASENAME.tcl and CLASSNAME.tcl into
        the Tcl Interpreter and calls execfile on BASENAME.py and CLASSNAME.py if
        such a file exists in the home directory."""
        import os
        Sensor(4201)
        if 'HOME' in os.environ:
            Sensor(4202)
            home = os.environ['HOME']
        else:
            Sensor(4203)
            home = os.curdir
        Sensor(4204)
        class_tcl = os.path.join(home, '.%s.tcl' % className)
        class_py = os.path.join(home, '.%s.py' % className)
        base_tcl = os.path.join(home, '.%s.tcl' % baseName)
        base_py = os.path.join(home, '.%s.py' % baseName)
        dir = {'self': self}
        exec 'from Tkinter import *' in dir
        Sensor(4205)
        if os.path.isfile(class_tcl):
            Sensor(4206)
            self.tk.call('source', class_tcl)
        Sensor(4207)
        if os.path.isfile(class_py):
            Sensor(4208)
            execfile(class_py, dir)
        Sensor(4209)
        if os.path.isfile(base_tcl):
            Sensor(4210)
            self.tk.call('source', base_tcl)
        Sensor(4211)
        if os.path.isfile(base_py):
            Sensor(4212)
            execfile(base_py, dir)
        Sensor(4213)

    def report_callback_exception(self, exc, val, tb):
        Sensor(4214)
        """Internal function. It reports exception on sys.stderr."""
        import traceback, sys
        sys.stderr.write('Exception in Tkinter callback\n')
        sys.last_type = exc
        sys.last_value = val
        sys.last_traceback = tb
        traceback.print_exception(exc, val, tb)
        Sensor(4215)

    def __getattr__(self, attr):
        Sensor(4216)
        """Delegate attribute access to the interpreter object"""
        py_ass_parser4217 = getattr(self.tk, attr)
        Sensor(4217)
        return py_ass_parser4217


def Tcl(screenName=None, baseName=None, className='Tk', useTk=0):
    py_ass_parser4218 = Tk(screenName, baseName, className, useTk)
    Sensor(4218)
    return py_ass_parser4218


class Pack:
    """Geometry manager Pack.

    Base class to use the methods pack_* in every widget."""

    def pack_configure(self, cnf={}, **kw):
        Sensor(4219)
        """Pack a widget in the parent widget. Use as options:
        after=widget - pack it after you have packed widget
        anchor=NSEW (or subset) - position widget according to
                                  given direction
        before=widget - pack it before you will pack widget
        expand=bool - expand widget if parent size grows
        fill=NONE or X or Y or BOTH - fill widget if widget grows
        in=master - use master to contain this widget
        in_=master - see 'in' option description
        ipadx=amount - add internal padding in x direction
        ipady=amount - add internal padding in y direction
        padx=amount - add padding in x direction
        pady=amount - add padding in y direction
        side=TOP or BOTTOM or LEFT or RIGHT -  where to add this widget.
        """
        self.tk.call(('pack', 'configure', self._w) + self._options(cnf, kw))
        Sensor(4220)
    pack = configure = config = pack_configure

    def pack_forget(self):
        Sensor(4221)
        """Unmap this widget and do not use it for the packing order."""
        self.tk.call('pack', 'forget', self._w)
        Sensor(4222)
    forget = pack_forget

    def pack_info(self):
        Sensor(4223)
        """Return information about the packing options
        for this widget."""
        words = self.tk.splitlist(self.tk.call('pack', 'info', self._w))
        dict = {}
        Sensor(4224)
        for i in range(0, len(words), 2):
            Sensor(4225)
            key = words[i][1:]
            value = words[i + 1]
            Sensor(4226)
            if value[:1] == '.':
                Sensor(4227)
                value = self._nametowidget(value)
            Sensor(4228)
            dict[key] = value
        py_ass_parser4229 = dict
        Sensor(4229)
        return py_ass_parser4229
    info = pack_info
    propagate = pack_propagate = Misc.pack_propagate
    slaves = pack_slaves = Misc.pack_slaves


class Place:
    """Geometry manager Place.

    Base class to use the methods place_* in every widget."""

    def place_configure(self, cnf={}, **kw):
        Sensor(4230)
        """Place a widget in the parent widget. Use as options:
        in=master - master relative to which the widget is placed
        in_=master - see 'in' option description
        x=amount - locate anchor of this widget at position x of master
        y=amount - locate anchor of this widget at position y of master
        relx=amount - locate anchor of this widget between 0.0 and 1.0
                      relative to width of master (1.0 is right edge)
        rely=amount - locate anchor of this widget between 0.0 and 1.0
                      relative to height of master (1.0 is bottom edge)
        anchor=NSEW (or subset) - position anchor according to given direction
        width=amount - width of this widget in pixel
        height=amount - height of this widget in pixel
        relwidth=amount - width of this widget between 0.0 and 1.0
                          relative to width of master (1.0 is the same width
                          as the master)
        relheight=amount - height of this widget between 0.0 and 1.0
                           relative to height of master (1.0 is the same
                           height as the master)
        bordermode="inside" or "outside" - whether to take border width of
                                           master widget into account
        """
        self.tk.call(('place', 'configure', self._w) + self._options(cnf, kw))
        Sensor(4231)
    place = configure = config = place_configure

    def place_forget(self):
        Sensor(4232)
        """Unmap this widget."""
        self.tk.call('place', 'forget', self._w)
        Sensor(4233)
    forget = place_forget

    def place_info(self):
        Sensor(4234)
        """Return information about the placing options
        for this widget."""
        words = self.tk.splitlist(self.tk.call('place', 'info', self._w))
        dict = {}
        Sensor(4235)
        for i in range(0, len(words), 2):
            Sensor(4236)
            key = words[i][1:]
            value = words[i + 1]
            Sensor(4237)
            if value[:1] == '.':
                Sensor(4238)
                value = self._nametowidget(value)
            Sensor(4239)
            dict[key] = value
        py_ass_parser4240 = dict
        Sensor(4240)
        return py_ass_parser4240
    info = place_info
    slaves = place_slaves = Misc.place_slaves


class Grid:
    """Geometry manager Grid.

    Base class to use the methods grid_* in every widget."""

    def grid_configure(self, cnf={}, **kw):
        Sensor(4241)
        """Position a widget in the parent widget in a grid. Use as options:
        column=number - use cell identified with given column (starting with 0)
        columnspan=number - this widget will span several columns
        in=master - use master to contain this widget
        in_=master - see 'in' option description
        ipadx=amount - add internal padding in x direction
        ipady=amount - add internal padding in y direction
        padx=amount - add padding in x direction
        pady=amount - add padding in y direction
        row=number - use cell identified with given row (starting with 0)
        rowspan=number - this widget will span several rows
        sticky=NSEW - if cell is larger on which sides will this
                      widget stick to the cell boundary
        """
        self.tk.call(('grid', 'configure', self._w) + self._options(cnf, kw))
        Sensor(4242)
    grid = configure = config = grid_configure
    bbox = grid_bbox = Misc.grid_bbox
    columnconfigure = grid_columnconfigure = Misc.grid_columnconfigure

    def grid_forget(self):
        Sensor(4243)
        """Unmap this widget."""
        self.tk.call('grid', 'forget', self._w)
        Sensor(4244)
    forget = grid_forget

    def grid_remove(self):
        Sensor(4245)
        """Unmap this widget but remember the grid options."""
        self.tk.call('grid', 'remove', self._w)
        Sensor(4246)

    def grid_info(self):
        Sensor(4247)
        """Return information about the options
        for positioning this widget in a grid."""
        words = self.tk.splitlist(self.tk.call('grid', 'info', self._w))
        dict = {}
        Sensor(4248)
        for i in range(0, len(words), 2):
            Sensor(4249)
            key = words[i][1:]
            value = words[i + 1]
            Sensor(4250)
            if value[:1] == '.':
                Sensor(4251)
                value = self._nametowidget(value)
            Sensor(4252)
            dict[key] = value
        py_ass_parser4253 = dict
        Sensor(4253)
        return py_ass_parser4253
    info = grid_info
    location = grid_location = Misc.grid_location
    propagate = grid_propagate = Misc.grid_propagate
    rowconfigure = grid_rowconfigure = Misc.grid_rowconfigure
    size = grid_size = Misc.grid_size
    slaves = grid_slaves = Misc.grid_slaves


class BaseWidget(Misc):
    """Internal class."""

    def _setup(self, master, cnf):
        Sensor(4254)
        """Internal function. Sets up information about children."""
        Sensor(4255)
        if _support_default_root:
            global _default_root
            Sensor(4256)
            if not master:
                Sensor(4257)
                if not _default_root:
                    Sensor(4258)
                    _default_root = Tk()
                Sensor(4259)
                master = _default_root
        Sensor(4260)
        self.master = master
        self.tk = master.tk
        name = None
        Sensor(4261)
        if 'name' in cnf:
            Sensor(4262)
            name = cnf['name']
            del cnf['name']
        Sensor(4263)
        if not name:
            Sensor(4264)
            name = repr(id(self))
        Sensor(4265)
        self._name = name
        Sensor(4266)
        if master._w == '.':
            Sensor(4267)
            self._w = '.' + name
        else:
            Sensor(4268)
            self._w = master._w + '.' + name
        Sensor(4269)
        self.children = {}
        Sensor(4270)
        if self._name in self.master.children:
            Sensor(4271)
            self.master.children[self._name].destroy()
        Sensor(4272)
        self.master.children[self._name] = self
        Sensor(4273)

    def __init__(self, master, widgetName, cnf={}, kw={}, extra=()):
        Sensor(4274)
        """Construct a widget with the parent widget MASTER, a name WIDGETNAME
        and appropriate options."""
        Sensor(4275)
        if kw:
            Sensor(4276)
            cnf = _cnfmerge((cnf, kw))
        Sensor(4277)
        self.widgetName = widgetName
        BaseWidget._setup(self, master, cnf)
        Sensor(4278)
        if self._tclCommands is None:
            Sensor(4279)
            self._tclCommands = []
        Sensor(4280)
        classes = []
        Sensor(4281)
        for k in cnf.keys():
            Sensor(4282)
            if type(k) is ClassType:
                Sensor(4283)
                classes.append((k, cnf[k]))
                del cnf[k]
        Sensor(4284)
        self.tk.call((widgetName, self._w) + extra + self._options(cnf))
        Sensor(4285)
        for k, v in classes:
            Sensor(4286)
            k.configure(self, v)
        Sensor(4287)

    def destroy(self):
        Sensor(4288)
        """Destroy this and all descendants widgets."""
        Sensor(4289)
        for c in self.children.values():
            Sensor(4290)
            c.destroy()
        Sensor(4291)
        self.tk.call('destroy', self._w)
        Sensor(4292)
        if self._name in self.master.children:
            Sensor(4293)
            del self.master.children[self._name]
        Sensor(4294)
        Misc.destroy(self)
        Sensor(4295)

    def _do(self, name, args=()):
        py_ass_parser4296 = self.tk.call((self._w, name) + args)
        Sensor(4296)
        return py_ass_parser4296


class Widget(BaseWidget, Pack, Place, Grid):
    """Internal class.

    Base class for a widget which can be positioned with the geometry managers
    Pack, Place or Grid."""
    pass


class Toplevel(BaseWidget, Wm):
    """Toplevel widget, e.g. for dialogs."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(4297)
        """Construct a toplevel widget with the parent MASTER.

        Valid resource names: background, bd, bg, borderwidth, class,
        colormap, container, cursor, height, highlightbackground,
        highlightcolor, highlightthickness, menu, relief, screen, takefocus,
        use, visual, width."""
        Sensor(4298)
        if kw:
            Sensor(4299)
            cnf = _cnfmerge((cnf, kw))
        Sensor(4300)
        extra = ()
        Sensor(4301)
        for wmkey in ['screen', 'class_', 'class', 'visual', 'colormap']:
            Sensor(4302)
            if wmkey in cnf:
                Sensor(4303)
                val = cnf[wmkey]
                Sensor(4304)
                if wmkey[-1] == '_':
                    Sensor(4305)
                    opt = '-' + wmkey[:-1]
                else:
                    Sensor(4306)
                    opt = '-' + wmkey
                Sensor(4307)
                extra = extra + (opt, val)
                del cnf[wmkey]
        Sensor(4308)
        BaseWidget.__init__(self, master, 'toplevel', cnf, {}, extra)
        root = self._root()
        self.iconname(root.iconname())
        self.title(root.title())
        self.protocol('WM_DELETE_WINDOW', self.destroy)
        Sensor(4309)


class Button(Widget):
    """Button widget."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(4310)
        """Construct a button widget with the parent MASTER.

        STANDARD OPTIONS

            activebackground, activeforeground, anchor,
            background, bitmap, borderwidth, cursor,
            disabledforeground, font, foreground
            highlightbackground, highlightcolor,
            highlightthickness, image, justify,
            padx, pady, relief, repeatdelay,
            repeatinterval, takefocus, text,
            textvariable, underline, wraplength

        WIDGET-SPECIFIC OPTIONS

            command, compound, default, height,
            overrelief, state, width
        """
        Widget.__init__(self, master, 'button', cnf, kw)
        Sensor(4311)

    def tkButtonEnter(self, *dummy):
        Sensor(4312)
        self.tk.call('tkButtonEnter', self._w)
        Sensor(4313)

    def tkButtonLeave(self, *dummy):
        Sensor(4314)
        self.tk.call('tkButtonLeave', self._w)
        Sensor(4315)

    def tkButtonDown(self, *dummy):
        Sensor(4316)
        self.tk.call('tkButtonDown', self._w)
        Sensor(4317)

    def tkButtonUp(self, *dummy):
        Sensor(4318)
        self.tk.call('tkButtonUp', self._w)
        Sensor(4319)

    def tkButtonInvoke(self, *dummy):
        Sensor(4320)
        self.tk.call('tkButtonInvoke', self._w)
        Sensor(4321)

    def flash(self):
        Sensor(4322)
        """Flash the button.

        This is accomplished by redisplaying
        the button several times, alternating between active and
        normal colors. At the end of the flash the button is left
        in the same normal/active state as when the command was
        invoked. This command is ignored if the button's state is
        disabled.
        """
        self.tk.call(self._w, 'flash')
        Sensor(4323)

    def invoke(self):
        Sensor(4324)
        """Invoke the command associated with the button.

        The return value is the return value from the command,
        or an empty string if there is no command associated with
        the button. This command is ignored if the button's state
        is disabled.
        """
        py_ass_parser4325 = self.tk.call(self._w, 'invoke')
        Sensor(4325)
        return py_ass_parser4325


def AtEnd():
    py_ass_parser4326 = 'end'
    Sensor(4326)
    return py_ass_parser4326


def AtInsert(*args):
    Sensor(4327)
    s = 'insert'
    Sensor(4328)
    for a in args:
        Sensor(4329)
        if a:
            Sensor(4330)
            s = s + (' ' + a)
    py_ass_parser4331 = s
    Sensor(4331)
    return py_ass_parser4331


def AtSelFirst():
    py_ass_parser4332 = 'sel.first'
    Sensor(4332)
    return py_ass_parser4332


def AtSelLast():
    py_ass_parser4333 = 'sel.last'
    Sensor(4333)
    return py_ass_parser4333


def At(x, y=None):
    Sensor(4334)
    if y is None:
        py_ass_parser4335 = '@%r' % (x,)
        Sensor(4335)
        return py_ass_parser4335
    else:
        py_ass_parser4336 = '@%r,%r' % (x, y)
        Sensor(4336)
        return py_ass_parser4336
    Sensor(4337)


class Canvas(Widget, XView, YView):
    """Canvas widget to display graphical elements like lines or text."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(4338)
        """Construct a canvas widget with the parent MASTER.

        Valid resource names: background, bd, bg, borderwidth, closeenough,
        confine, cursor, height, highlightbackground, highlightcolor,
        highlightthickness, insertbackground, insertborderwidth,
        insertofftime, insertontime, insertwidth, offset, relief,
        scrollregion, selectbackground, selectborderwidth, selectforeground,
        state, takefocus, width, xscrollcommand, xscrollincrement,
        yscrollcommand, yscrollincrement."""
        Widget.__init__(self, master, 'canvas', cnf, kw)
        Sensor(4339)

    def addtag(self, *args):
        Sensor(4340)
        """Internal function."""
        self.tk.call((self._w, 'addtag') + args)
        Sensor(4341)

    def addtag_above(self, newtag, tagOrId):
        Sensor(4342)
        """Add tag NEWTAG to all items above TAGORID."""
        self.addtag(newtag, 'above', tagOrId)
        Sensor(4343)

    def addtag_all(self, newtag):
        Sensor(4344)
        """Add tag NEWTAG to all items."""
        self.addtag(newtag, 'all')
        Sensor(4345)

    def addtag_below(self, newtag, tagOrId):
        Sensor(4346)
        """Add tag NEWTAG to all items below TAGORID."""
        self.addtag(newtag, 'below', tagOrId)
        Sensor(4347)

    def addtag_closest(self, newtag, x, y, halo=None, start=None):
        Sensor(4348)
        """Add tag NEWTAG to item which is closest to pixel at X, Y.
        If several match take the top-most.
        All items closer than HALO are considered overlapping (all are
        closests). If START is specified the next below this tag is taken."""
        self.addtag(newtag, 'closest', x, y, halo, start)
        Sensor(4349)

    def addtag_enclosed(self, newtag, x1, y1, x2, y2):
        Sensor(4350)
        """Add tag NEWTAG to all items in the rectangle defined
        by X1,Y1,X2,Y2."""
        self.addtag(newtag, 'enclosed', x1, y1, x2, y2)
        Sensor(4351)

    def addtag_overlapping(self, newtag, x1, y1, x2, y2):
        Sensor(4352)
        """Add tag NEWTAG to all items which overlap the rectangle
        defined by X1,Y1,X2,Y2."""
        self.addtag(newtag, 'overlapping', x1, y1, x2, y2)
        Sensor(4353)

    def addtag_withtag(self, newtag, tagOrId):
        Sensor(4354)
        """Add tag NEWTAG to all items with TAGORID."""
        self.addtag(newtag, 'withtag', tagOrId)
        Sensor(4355)

    def bbox(self, *args):
        Sensor(4356)
        """Return a tuple of X1,Y1,X2,Y2 coordinates for a rectangle
        which encloses all items with tags specified as arguments."""
        py_ass_parser4357 = self._getints(self.tk.call((self._w, 'bbox') +
            args)) or None
        Sensor(4357)
        return py_ass_parser4357

    def tag_unbind(self, tagOrId, sequence, funcid=None):
        Sensor(4358)
        """Unbind for all items with TAGORID for event SEQUENCE  the
        function identified with FUNCID."""
        self.tk.call(self._w, 'bind', tagOrId, sequence, '')
        Sensor(4359)
        if funcid:
            Sensor(4360)
            self.deletecommand(funcid)
        Sensor(4361)

    def tag_bind(self, tagOrId, sequence=None, func=None, add=None):
        Sensor(4362)
        """Bind to all items with TAGORID at event SEQUENCE a call to function FUNC.

        An additional boolean parameter ADD specifies whether FUNC will be
        called additionally to the other bound function or whether it will
        replace the previous function. See bind for the return value."""
        py_ass_parser4363 = self._bind((self._w, 'bind', tagOrId), sequence,
            func, add)
        Sensor(4363)
        return py_ass_parser4363

    def canvasx(self, screenx, gridspacing=None):
        Sensor(4364)
        """Return the canvas x coordinate of pixel position SCREENX rounded
        to nearest multiple of GRIDSPACING units."""
        py_ass_parser4365 = getdouble(self.tk.call(self._w, 'canvasx',
            screenx, gridspacing))
        Sensor(4365)
        return py_ass_parser4365

    def canvasy(self, screeny, gridspacing=None):
        Sensor(4366)
        """Return the canvas y coordinate of pixel position SCREENY rounded
        to nearest multiple of GRIDSPACING units."""
        py_ass_parser4367 = getdouble(self.tk.call(self._w, 'canvasy',
            screeny, gridspacing))
        Sensor(4367)
        return py_ass_parser4367

    def coords(self, *args):
        Sensor(4368)
        """Return a list of coordinates for the item given in ARGS."""
        py_ass_parser4369 = map(getdouble, self.tk.splitlist(self.tk.call((
            self._w, 'coords') + args)))
        Sensor(4369)
        return py_ass_parser4369

    def _create(self, itemType, args, kw):
        Sensor(4370)
        """Internal function."""
        args = _flatten(args)
        cnf = args[-1]
        Sensor(4371)
        if type(cnf) in (DictionaryType, TupleType):
            Sensor(4372)
            args = args[:-1]
        else:
            Sensor(4373)
            cnf = {}
        py_ass_parser4374 = getint(self.tk.call(self._w, 'create', itemType,
            *(args + self._options(cnf, kw))))
        Sensor(4374)
        return py_ass_parser4374

    def create_arc(self, *args, **kw):
        Sensor(4375)
        """Create arc shaped region with coordinates x1,y1,x2,y2."""
        py_ass_parser4376 = self._create('arc', args, kw)
        Sensor(4376)
        return py_ass_parser4376

    def create_bitmap(self, *args, **kw):
        Sensor(4377)
        """Create bitmap with coordinates x1,y1."""
        py_ass_parser4378 = self._create('bitmap', args, kw)
        Sensor(4378)
        return py_ass_parser4378

    def create_image(self, *args, **kw):
        Sensor(4379)
        """Create image item with coordinates x1,y1."""
        py_ass_parser4380 = self._create('image', args, kw)
        Sensor(4380)
        return py_ass_parser4380

    def create_line(self, *args, **kw):
        Sensor(4381)
        """Create line with coordinates x1,y1,...,xn,yn."""
        py_ass_parser4382 = self._create('line', args, kw)
        Sensor(4382)
        return py_ass_parser4382

    def create_oval(self, *args, **kw):
        Sensor(4383)
        """Create oval with coordinates x1,y1,x2,y2."""
        py_ass_parser4384 = self._create('oval', args, kw)
        Sensor(4384)
        return py_ass_parser4384

    def create_polygon(self, *args, **kw):
        Sensor(4385)
        """Create polygon with coordinates x1,y1,...,xn,yn."""
        py_ass_parser4386 = self._create('polygon', args, kw)
        Sensor(4386)
        return py_ass_parser4386

    def create_rectangle(self, *args, **kw):
        Sensor(4387)
        """Create rectangle with coordinates x1,y1,x2,y2."""
        py_ass_parser4388 = self._create('rectangle', args, kw)
        Sensor(4388)
        return py_ass_parser4388

    def create_text(self, *args, **kw):
        Sensor(4389)
        """Create text with coordinates x1,y1."""
        py_ass_parser4390 = self._create('text', args, kw)
        Sensor(4390)
        return py_ass_parser4390

    def create_window(self, *args, **kw):
        Sensor(4391)
        """Create window with coordinates x1,y1,x2,y2."""
        py_ass_parser4392 = self._create('window', args, kw)
        Sensor(4392)
        return py_ass_parser4392

    def dchars(self, *args):
        Sensor(4393)
        """Delete characters of text items identified by tag or id in ARGS (possibly
        several times) from FIRST to LAST character (including)."""
        self.tk.call((self._w, 'dchars') + args)
        Sensor(4394)

    def delete(self, *args):
        Sensor(4395)
        """Delete items identified by all tag or ids contained in ARGS."""
        self.tk.call((self._w, 'delete') + args)
        Sensor(4396)

    def dtag(self, *args):
        Sensor(4397)
        """Delete tag or id given as last arguments in ARGS from items
        identified by first argument in ARGS."""
        self.tk.call((self._w, 'dtag') + args)
        Sensor(4398)

    def find(self, *args):
        Sensor(4399)
        """Internal function."""
        py_ass_parser4400 = self._getints(self.tk.call((self._w, 'find') +
            args)) or ()
        Sensor(4400)
        return py_ass_parser4400

    def find_above(self, tagOrId):
        Sensor(4401)
        """Return items above TAGORID."""
        py_ass_parser4402 = self.find('above', tagOrId)
        Sensor(4402)
        return py_ass_parser4402

    def find_all(self):
        Sensor(4403)
        """Return all items."""
        py_ass_parser4404 = self.find('all')
        Sensor(4404)
        return py_ass_parser4404

    def find_below(self, tagOrId):
        Sensor(4405)
        """Return all items below TAGORID."""
        py_ass_parser4406 = self.find('below', tagOrId)
        Sensor(4406)
        return py_ass_parser4406

    def find_closest(self, x, y, halo=None, start=None):
        Sensor(4407)
        """Return item which is closest to pixel at X, Y.
        If several match take the top-most.
        All items closer than HALO are considered overlapping (all are
        closests). If START is specified the next below this tag is taken."""
        py_ass_parser4408 = self.find('closest', x, y, halo, start)
        Sensor(4408)
        return py_ass_parser4408

    def find_enclosed(self, x1, y1, x2, y2):
        Sensor(4409)
        """Return all items in rectangle defined
        by X1,Y1,X2,Y2."""
        py_ass_parser4410 = self.find('enclosed', x1, y1, x2, y2)
        Sensor(4410)
        return py_ass_parser4410

    def find_overlapping(self, x1, y1, x2, y2):
        Sensor(4411)
        """Return all items which overlap the rectangle
        defined by X1,Y1,X2,Y2."""
        py_ass_parser4412 = self.find('overlapping', x1, y1, x2, y2)
        Sensor(4412)
        return py_ass_parser4412

    def find_withtag(self, tagOrId):
        Sensor(4413)
        """Return all items with TAGORID."""
        py_ass_parser4414 = self.find('withtag', tagOrId)
        Sensor(4414)
        return py_ass_parser4414

    def focus(self, *args):
        Sensor(4415)
        """Set focus to the first item specified in ARGS."""
        py_ass_parser4416 = self.tk.call((self._w, 'focus') + args)
        Sensor(4416)
        return py_ass_parser4416

    def gettags(self, *args):
        Sensor(4417)
        """Return tags associated with the first item specified in ARGS."""
        py_ass_parser4418 = self.tk.splitlist(self.tk.call((self._w,
            'gettags') + args))
        Sensor(4418)
        return py_ass_parser4418

    def icursor(self, *args):
        Sensor(4419)
        """Set cursor at position POS in the item identified by TAGORID.
        In ARGS TAGORID must be first."""
        self.tk.call((self._w, 'icursor') + args)
        Sensor(4420)

    def index(self, *args):
        Sensor(4421)
        """Return position of cursor as integer in item specified in ARGS."""
        py_ass_parser4422 = getint(self.tk.call((self._w, 'index') + args))
        Sensor(4422)
        return py_ass_parser4422

    def insert(self, *args):
        Sensor(4423)
        """Insert TEXT in item TAGORID at position POS. ARGS must
        be TAGORID POS TEXT."""
        self.tk.call((self._w, 'insert') + args)
        Sensor(4424)

    def itemcget(self, tagOrId, option):
        Sensor(4425)
        """Return the resource value for an OPTION for item TAGORID."""
        py_ass_parser4426 = self.tk.call((self._w, 'itemcget') + (tagOrId, 
            '-' + option))
        Sensor(4426)
        return py_ass_parser4426

    def itemconfigure(self, tagOrId, cnf=None, **kw):
        Sensor(4427)
        """Configure resources of an item TAGORID.

        The values for resources are specified as keyword
        arguments. To get an overview about
        the allowed keyword arguments call the method without arguments.
        """
        py_ass_parser4428 = self._configure(('itemconfigure', tagOrId), cnf, kw
            )
        Sensor(4428)
        return py_ass_parser4428
    itemconfig = itemconfigure

    def tag_lower(self, *args):
        Sensor(4429)
        """Lower an item TAGORID given in ARGS
        (optional below another item)."""
        self.tk.call((self._w, 'lower') + args)
        Sensor(4430)
    lower = tag_lower

    def move(self, *args):
        Sensor(4431)
        """Move an item TAGORID given in ARGS."""
        self.tk.call((self._w, 'move') + args)
        Sensor(4432)

    def postscript(self, cnf={}, **kw):
        Sensor(4433)
        """Print the contents of the canvas to a postscript
        file. Valid options: colormap, colormode, file, fontmap,
        height, pageanchor, pageheight, pagewidth, pagex, pagey,
        rotate, witdh, x, y."""
        py_ass_parser4434 = self.tk.call((self._w, 'postscript') + self.
            _options(cnf, kw))
        Sensor(4434)
        return py_ass_parser4434

    def tag_raise(self, *args):
        Sensor(4435)
        """Raise an item TAGORID given in ARGS
        (optional above another item)."""
        self.tk.call((self._w, 'raise') + args)
        Sensor(4436)
    lift = tkraise = tag_raise

    def scale(self, *args):
        Sensor(4437)
        """Scale item TAGORID with XORIGIN, YORIGIN, XSCALE, YSCALE."""
        self.tk.call((self._w, 'scale') + args)
        Sensor(4438)

    def scan_mark(self, x, y):
        Sensor(4439)
        """Remember the current X, Y coordinates."""
        self.tk.call(self._w, 'scan', 'mark', x, y)
        Sensor(4440)

    def scan_dragto(self, x, y, gain=10):
        Sensor(4441)
        """Adjust the view of the canvas to GAIN times the
        difference between X and Y and the coordinates given in
        scan_mark."""
        self.tk.call(self._w, 'scan', 'dragto', x, y, gain)
        Sensor(4442)

    def select_adjust(self, tagOrId, index):
        Sensor(4443)
        """Adjust the end of the selection near the cursor of an item TAGORID to index."""
        self.tk.call(self._w, 'select', 'adjust', tagOrId, index)
        Sensor(4444)

    def select_clear(self):
        Sensor(4445)
        """Clear the selection if it is in this widget."""
        self.tk.call(self._w, 'select', 'clear')
        Sensor(4446)

    def select_from(self, tagOrId, index):
        Sensor(4447)
        """Set the fixed end of a selection in item TAGORID to INDEX."""
        self.tk.call(self._w, 'select', 'from', tagOrId, index)
        Sensor(4448)

    def select_item(self):
        Sensor(4449)
        """Return the item which has the selection."""
        py_ass_parser4450 = self.tk.call(self._w, 'select', 'item') or None
        Sensor(4450)
        return py_ass_parser4450

    def select_to(self, tagOrId, index):
        Sensor(4451)
        """Set the variable end of a selection in item TAGORID to INDEX."""
        self.tk.call(self._w, 'select', 'to', tagOrId, index)
        Sensor(4452)

    def type(self, tagOrId):
        Sensor(4453)
        """Return the type of the item TAGORID."""
        py_ass_parser4454 = self.tk.call(self._w, 'type', tagOrId) or None
        Sensor(4454)
        return py_ass_parser4454


class Checkbutton(Widget):
    """Checkbutton widget which is either in on- or off-state."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(4455)
        """Construct a checkbutton widget with the parent MASTER.

        Valid resource names: activebackground, activeforeground, anchor,
        background, bd, bg, bitmap, borderwidth, command, cursor,
        disabledforeground, fg, font, foreground, height,
        highlightbackground, highlightcolor, highlightthickness, image,
        indicatoron, justify, offvalue, onvalue, padx, pady, relief,
        selectcolor, selectimage, state, takefocus, text, textvariable,
        underline, variable, width, wraplength."""
        Widget.__init__(self, master, 'checkbutton', cnf, kw)
        Sensor(4456)

    def deselect(self):
        Sensor(4457)
        """Put the button in off-state."""
        self.tk.call(self._w, 'deselect')
        Sensor(4458)

    def flash(self):
        Sensor(4459)
        """Flash the button."""
        self.tk.call(self._w, 'flash')
        Sensor(4460)

    def invoke(self):
        Sensor(4461)
        """Toggle the button and invoke a command if given as resource."""
        py_ass_parser4462 = self.tk.call(self._w, 'invoke')
        Sensor(4462)
        return py_ass_parser4462

    def select(self):
        Sensor(4463)
        """Put the button in on-state."""
        self.tk.call(self._w, 'select')
        Sensor(4464)

    def toggle(self):
        Sensor(4465)
        """Toggle the button."""
        self.tk.call(self._w, 'toggle')
        Sensor(4466)


class Entry(Widget, XView):
    """Entry widget which allows to display simple text."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(4467)
        """Construct an entry widget with the parent MASTER.

        Valid resource names: background, bd, bg, borderwidth, cursor,
        exportselection, fg, font, foreground, highlightbackground,
        highlightcolor, highlightthickness, insertbackground,
        insertborderwidth, insertofftime, insertontime, insertwidth,
        invalidcommand, invcmd, justify, relief, selectbackground,
        selectborderwidth, selectforeground, show, state, takefocus,
        textvariable, validate, validatecommand, vcmd, width,
        xscrollcommand."""
        Widget.__init__(self, master, 'entry', cnf, kw)
        Sensor(4468)

    def delete(self, first, last=None):
        Sensor(4469)
        """Delete text from FIRST to LAST (not included)."""
        self.tk.call(self._w, 'delete', first, last)
        Sensor(4470)

    def get(self):
        Sensor(4471)
        """Return the text."""
        py_ass_parser4472 = self.tk.call(self._w, 'get')
        Sensor(4472)
        return py_ass_parser4472

    def icursor(self, index):
        Sensor(4473)
        """Insert cursor at INDEX."""
        self.tk.call(self._w, 'icursor', index)
        Sensor(4474)

    def index(self, index):
        Sensor(4475)
        """Return position of cursor."""
        py_ass_parser4476 = getint(self.tk.call(self._w, 'index', index))
        Sensor(4476)
        return py_ass_parser4476

    def insert(self, index, string):
        Sensor(4477)
        """Insert STRING at INDEX."""
        self.tk.call(self._w, 'insert', index, string)
        Sensor(4478)

    def scan_mark(self, x):
        Sensor(4479)
        """Remember the current X, Y coordinates."""
        self.tk.call(self._w, 'scan', 'mark', x)
        Sensor(4480)

    def scan_dragto(self, x):
        Sensor(4481)
        """Adjust the view of the canvas to 10 times the
        difference between X and Y and the coordinates given in
        scan_mark."""
        self.tk.call(self._w, 'scan', 'dragto', x)
        Sensor(4482)

    def selection_adjust(self, index):
        Sensor(4483)
        """Adjust the end of the selection near the cursor to INDEX."""
        self.tk.call(self._w, 'selection', 'adjust', index)
        Sensor(4484)
    select_adjust = selection_adjust

    def selection_clear(self):
        Sensor(4485)
        """Clear the selection if it is in this widget."""
        self.tk.call(self._w, 'selection', 'clear')
        Sensor(4486)
    select_clear = selection_clear

    def selection_from(self, index):
        Sensor(4487)
        """Set the fixed end of a selection to INDEX."""
        self.tk.call(self._w, 'selection', 'from', index)
        Sensor(4488)
    select_from = selection_from

    def selection_present(self):
        Sensor(4489)
        """Return True if there are characters selected in the entry, False
        otherwise."""
        py_ass_parser4490 = self.tk.getboolean(self.tk.call(self._w,
            'selection', 'present'))
        Sensor(4490)
        return py_ass_parser4490
    select_present = selection_present

    def selection_range(self, start, end):
        Sensor(4491)
        """Set the selection from START to END (not included)."""
        self.tk.call(self._w, 'selection', 'range', start, end)
        Sensor(4492)
    select_range = selection_range

    def selection_to(self, index):
        Sensor(4493)
        """Set the variable end of a selection to INDEX."""
        self.tk.call(self._w, 'selection', 'to', index)
        Sensor(4494)
    select_to = selection_to


class Frame(Widget):
    """Frame widget which may contain other widgets and can have a 3D border."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(4495)
        """Construct a frame widget with the parent MASTER.

        Valid resource names: background, bd, bg, borderwidth, class,
        colormap, container, cursor, height, highlightbackground,
        highlightcolor, highlightthickness, relief, takefocus, visual, width."""
        cnf = _cnfmerge((cnf, kw))
        extra = ()
        Sensor(4496)
        if 'class_' in cnf:
            Sensor(4497)
            extra = '-class', cnf['class_']
            del cnf['class_']
        else:
            Sensor(4498)
            if 'class' in cnf:
                Sensor(4499)
                extra = '-class', cnf['class']
                del cnf['class']
        Sensor(4500)
        Widget.__init__(self, master, 'frame', cnf, {}, extra)
        Sensor(4501)


class Label(Widget):
    """Label widget which can display text and bitmaps."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(4502)
        """Construct a label widget with the parent MASTER.

        STANDARD OPTIONS

            activebackground, activeforeground, anchor,
            background, bitmap, borderwidth, cursor,
            disabledforeground, font, foreground,
            highlightbackground, highlightcolor,
            highlightthickness, image, justify,
            padx, pady, relief, takefocus, text,
            textvariable, underline, wraplength

        WIDGET-SPECIFIC OPTIONS

            height, state, width

        """
        Widget.__init__(self, master, 'label', cnf, kw)
        Sensor(4503)


class Listbox(Widget, XView, YView):
    """Listbox widget which can display a list of strings."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(4504)
        """Construct a listbox widget with the parent MASTER.

        Valid resource names: background, bd, bg, borderwidth, cursor,
        exportselection, fg, font, foreground, height, highlightbackground,
        highlightcolor, highlightthickness, relief, selectbackground,
        selectborderwidth, selectforeground, selectmode, setgrid, takefocus,
        width, xscrollcommand, yscrollcommand, listvariable."""
        Widget.__init__(self, master, 'listbox', cnf, kw)
        Sensor(4505)

    def activate(self, index):
        Sensor(4506)
        """Activate item identified by INDEX."""
        self.tk.call(self._w, 'activate', index)
        Sensor(4507)

    def bbox(self, *args):
        Sensor(4508)
        """Return a tuple of X1,Y1,X2,Y2 coordinates for a rectangle
        which encloses the item identified by index in ARGS."""
        py_ass_parser4509 = self._getints(self.tk.call((self._w, 'bbox') +
            args)) or None
        Sensor(4509)
        return py_ass_parser4509

    def curselection(self):
        Sensor(4510)
        """Return list of indices of currently selected item."""
        py_ass_parser4511 = self.tk.splitlist(self.tk.call(self._w,
            'curselection'))
        Sensor(4511)
        return py_ass_parser4511

    def delete(self, first, last=None):
        Sensor(4512)
        """Delete items from FIRST to LAST (not included)."""
        self.tk.call(self._w, 'delete', first, last)
        Sensor(4513)

    def get(self, first, last=None):
        Sensor(4514)
        """Get list of items from FIRST to LAST (not included)."""
        Sensor(4515)
        if last:
            py_ass_parser4516 = self.tk.splitlist(self.tk.call(self._w,
                'get', first, last))
            Sensor(4516)
            return py_ass_parser4516
        else:
            py_ass_parser4517 = self.tk.call(self._w, 'get', first)
            Sensor(4517)
            return py_ass_parser4517
        Sensor(4518)

    def index(self, index):
        Sensor(4519)
        """Return index of item identified with INDEX."""
        i = self.tk.call(self._w, 'index', index)
        Sensor(4520)
        if i == 'none':
            py_ass_parser4521 = None
            Sensor(4521)
            return py_ass_parser4521
        py_ass_parser4522 = getint(i)
        Sensor(4522)
        return py_ass_parser4522

    def insert(self, index, *elements):
        Sensor(4523)
        """Insert ELEMENTS at INDEX."""
        self.tk.call((self._w, 'insert', index) + elements)
        Sensor(4524)

    def nearest(self, y):
        Sensor(4525)
        """Get index of item which is nearest to y coordinate Y."""
        py_ass_parser4526 = getint(self.tk.call(self._w, 'nearest', y))
        Sensor(4526)
        return py_ass_parser4526

    def scan_mark(self, x, y):
        Sensor(4527)
        """Remember the current X, Y coordinates."""
        self.tk.call(self._w, 'scan', 'mark', x, y)
        Sensor(4528)

    def scan_dragto(self, x, y):
        Sensor(4529)
        """Adjust the view of the listbox to 10 times the
        difference between X and Y and the coordinates given in
        scan_mark."""
        self.tk.call(self._w, 'scan', 'dragto', x, y)
        Sensor(4530)

    def see(self, index):
        Sensor(4531)
        """Scroll such that INDEX is visible."""
        self.tk.call(self._w, 'see', index)
        Sensor(4532)

    def selection_anchor(self, index):
        Sensor(4533)
        """Set the fixed end oft the selection to INDEX."""
        self.tk.call(self._w, 'selection', 'anchor', index)
        Sensor(4534)
    select_anchor = selection_anchor

    def selection_clear(self, first, last=None):
        Sensor(4535)
        """Clear the selection from FIRST to LAST (not included)."""
        self.tk.call(self._w, 'selection', 'clear', first, last)
        Sensor(4536)
    select_clear = selection_clear

    def selection_includes(self, index):
        Sensor(4537)
        """Return 1 if INDEX is part of the selection."""
        py_ass_parser4538 = self.tk.getboolean(self.tk.call(self._w,
            'selection', 'includes', index))
        Sensor(4538)
        return py_ass_parser4538
    select_includes = selection_includes

    def selection_set(self, first, last=None):
        Sensor(4539)
        """Set the selection from FIRST to LAST (not included) without
        changing the currently selected elements."""
        self.tk.call(self._w, 'selection', 'set', first, last)
        Sensor(4540)
    select_set = selection_set

    def size(self):
        Sensor(4541)
        """Return the number of elements in the listbox."""
        py_ass_parser4542 = getint(self.tk.call(self._w, 'size'))
        Sensor(4542)
        return py_ass_parser4542

    def itemcget(self, index, option):
        Sensor(4543)
        """Return the resource value for an ITEM and an OPTION."""
        py_ass_parser4544 = self.tk.call((self._w, 'itemcget') + (index, 
            '-' + option))
        Sensor(4544)
        return py_ass_parser4544

    def itemconfigure(self, index, cnf=None, **kw):
        Sensor(4545)
        """Configure resources of an ITEM.

        The values for resources are specified as keyword arguments.
        To get an overview about the allowed keyword arguments
        call the method without arguments.
        Valid resource names: background, bg, foreground, fg,
        selectbackground, selectforeground."""
        py_ass_parser4546 = self._configure(('itemconfigure', index), cnf, kw)
        Sensor(4546)
        return py_ass_parser4546
    itemconfig = itemconfigure


class Menu(Widget):
    """Menu widget which allows to display menu bars, pull-down menus and pop-up menus."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(4547)
        """Construct menu widget with the parent MASTER.

        Valid resource names: activebackground, activeborderwidth,
        activeforeground, background, bd, bg, borderwidth, cursor,
        disabledforeground, fg, font, foreground, postcommand, relief,
        selectcolor, takefocus, tearoff, tearoffcommand, title, type."""
        Widget.__init__(self, master, 'menu', cnf, kw)
        Sensor(4548)

    def tk_bindForTraversal(self):
        Sensor(4549)
        pass
        Sensor(4550)

    def tk_mbPost(self):
        Sensor(4551)
        self.tk.call('tk_mbPost', self._w)
        Sensor(4552)

    def tk_mbUnpost(self):
        Sensor(4553)
        self.tk.call('tk_mbUnpost')
        Sensor(4554)

    def tk_traverseToMenu(self, char):
        Sensor(4555)
        self.tk.call('tk_traverseToMenu', self._w, char)
        Sensor(4556)

    def tk_traverseWithinMenu(self, char):
        Sensor(4557)
        self.tk.call('tk_traverseWithinMenu', self._w, char)
        Sensor(4558)

    def tk_getMenuButtons(self):
        py_ass_parser4559 = self.tk.call('tk_getMenuButtons', self._w)
        Sensor(4559)
        return py_ass_parser4559

    def tk_nextMenu(self, count):
        Sensor(4560)
        self.tk.call('tk_nextMenu', count)
        Sensor(4561)

    def tk_nextMenuEntry(self, count):
        Sensor(4562)
        self.tk.call('tk_nextMenuEntry', count)
        Sensor(4563)

    def tk_invokeMenu(self):
        Sensor(4564)
        self.tk.call('tk_invokeMenu', self._w)
        Sensor(4565)

    def tk_firstMenu(self):
        Sensor(4566)
        self.tk.call('tk_firstMenu', self._w)
        Sensor(4567)

    def tk_mbButtonDown(self):
        Sensor(4568)
        self.tk.call('tk_mbButtonDown', self._w)
        Sensor(4569)

    def tk_popup(self, x, y, entry=''):
        Sensor(4570)
        """Post the menu at position X,Y with entry ENTRY."""
        self.tk.call('tk_popup', self._w, x, y, entry)
        Sensor(4571)

    def activate(self, index):
        Sensor(4572)
        """Activate entry at INDEX."""
        self.tk.call(self._w, 'activate', index)
        Sensor(4573)

    def add(self, itemType, cnf={}, **kw):
        Sensor(4574)
        """Internal function."""
        self.tk.call((self._w, 'add', itemType) + self._options(cnf, kw))
        Sensor(4575)

    def add_cascade(self, cnf={}, **kw):
        Sensor(4576)
        """Add hierarchical menu item."""
        self.add('cascade', cnf or kw)
        Sensor(4577)

    def add_checkbutton(self, cnf={}, **kw):
        Sensor(4578)
        """Add checkbutton menu item."""
        self.add('checkbutton', cnf or kw)
        Sensor(4579)

    def add_command(self, cnf={}, **kw):
        Sensor(4580)
        """Add command menu item."""
        self.add('command', cnf or kw)
        Sensor(4581)

    def add_radiobutton(self, cnf={}, **kw):
        Sensor(4582)
        """Addd radio menu item."""
        self.add('radiobutton', cnf or kw)
        Sensor(4583)

    def add_separator(self, cnf={}, **kw):
        Sensor(4584)
        """Add separator."""
        self.add('separator', cnf or kw)
        Sensor(4585)

    def insert(self, index, itemType, cnf={}, **kw):
        Sensor(4586)
        """Internal function."""
        self.tk.call((self._w, 'insert', index, itemType) + self._options(
            cnf, kw))
        Sensor(4587)

    def insert_cascade(self, index, cnf={}, **kw):
        Sensor(4588)
        """Add hierarchical menu item at INDEX."""
        self.insert(index, 'cascade', cnf or kw)
        Sensor(4589)

    def insert_checkbutton(self, index, cnf={}, **kw):
        Sensor(4590)
        """Add checkbutton menu item at INDEX."""
        self.insert(index, 'checkbutton', cnf or kw)
        Sensor(4591)

    def insert_command(self, index, cnf={}, **kw):
        Sensor(4592)
        """Add command menu item at INDEX."""
        self.insert(index, 'command', cnf or kw)
        Sensor(4593)

    def insert_radiobutton(self, index, cnf={}, **kw):
        Sensor(4594)
        """Addd radio menu item at INDEX."""
        self.insert(index, 'radiobutton', cnf or kw)
        Sensor(4595)

    def insert_separator(self, index, cnf={}, **kw):
        Sensor(4596)
        """Add separator at INDEX."""
        self.insert(index, 'separator', cnf or kw)
        Sensor(4597)

    def delete(self, index1, index2=None):
        Sensor(4598)
        """Delete menu items between INDEX1 and INDEX2 (included)."""
        Sensor(4599)
        if index2 is None:
            Sensor(4600)
            index2 = index1
        Sensor(4601)
        num_index1, num_index2 = self.index(index1), self.index(index2)
        Sensor(4602)
        if num_index1 is None or num_index2 is None:
            Sensor(4603)
            num_index1, num_index2 = 0, -1
        Sensor(4604)
        for i in range(num_index1, num_index2 + 1):
            Sensor(4605)
            if 'command' in self.entryconfig(i):
                Sensor(4606)
                c = str(self.entrycget(i, 'command'))
                Sensor(4607)
                if c:
                    Sensor(4608)
                    self.deletecommand(c)
        Sensor(4609)
        self.tk.call(self._w, 'delete', index1, index2)
        Sensor(4610)

    def entrycget(self, index, option):
        Sensor(4611)
        """Return the resource value of an menu item for OPTION at INDEX."""
        py_ass_parser4612 = self.tk.call(self._w, 'entrycget', index, '-' +
            option)
        Sensor(4612)
        return py_ass_parser4612

    def entryconfigure(self, index, cnf=None, **kw):
        Sensor(4613)
        """Configure a menu item at INDEX."""
        py_ass_parser4614 = self._configure(('entryconfigure', index), cnf, kw)
        Sensor(4614)
        return py_ass_parser4614
    entryconfig = entryconfigure

    def index(self, index):
        Sensor(4615)
        """Return the index of a menu item identified by INDEX."""
        i = self.tk.call(self._w, 'index', index)
        Sensor(4616)
        if i == 'none':
            py_ass_parser4617 = None
            Sensor(4617)
            return py_ass_parser4617
        py_ass_parser4618 = getint(i)
        Sensor(4618)
        return py_ass_parser4618

    def invoke(self, index):
        Sensor(4619)
        """Invoke a menu item identified by INDEX and execute
        the associated command."""
        py_ass_parser4620 = self.tk.call(self._w, 'invoke', index)
        Sensor(4620)
        return py_ass_parser4620

    def post(self, x, y):
        Sensor(4621)
        """Display a menu at position X,Y."""
        self.tk.call(self._w, 'post', x, y)
        Sensor(4622)

    def type(self, index):
        Sensor(4623)
        """Return the type of the menu item at INDEX."""
        py_ass_parser4624 = self.tk.call(self._w, 'type', index)
        Sensor(4624)
        return py_ass_parser4624

    def unpost(self):
        Sensor(4625)
        """Unmap a menu."""
        self.tk.call(self._w, 'unpost')
        Sensor(4626)

    def yposition(self, index):
        Sensor(4627)
        """Return the y-position of the topmost pixel of the menu item at INDEX."""
        py_ass_parser4628 = getint(self.tk.call(self._w, 'yposition', index))
        Sensor(4628)
        return py_ass_parser4628


class Menubutton(Widget):
    """Menubutton widget, obsolete since Tk8.0."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(4629)
        Widget.__init__(self, master, 'menubutton', cnf, kw)
        Sensor(4630)


class Message(Widget):
    """Message widget to display multiline text. Obsolete since Label does it too."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(4631)
        Widget.__init__(self, master, 'message', cnf, kw)
        Sensor(4632)


class Radiobutton(Widget):
    """Radiobutton widget which shows only one of several buttons in on-state."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(4633)
        """Construct a radiobutton widget with the parent MASTER.

        Valid resource names: activebackground, activeforeground, anchor,
        background, bd, bg, bitmap, borderwidth, command, cursor,
        disabledforeground, fg, font, foreground, height,
        highlightbackground, highlightcolor, highlightthickness, image,
        indicatoron, justify, padx, pady, relief, selectcolor, selectimage,
        state, takefocus, text, textvariable, underline, value, variable,
        width, wraplength."""
        Widget.__init__(self, master, 'radiobutton', cnf, kw)
        Sensor(4634)

    def deselect(self):
        Sensor(4635)
        """Put the button in off-state."""
        self.tk.call(self._w, 'deselect')
        Sensor(4636)

    def flash(self):
        Sensor(4637)
        """Flash the button."""
        self.tk.call(self._w, 'flash')
        Sensor(4638)

    def invoke(self):
        Sensor(4639)
        """Toggle the button and invoke a command if given as resource."""
        py_ass_parser4640 = self.tk.call(self._w, 'invoke')
        Sensor(4640)
        return py_ass_parser4640

    def select(self):
        Sensor(4641)
        """Put the button in on-state."""
        self.tk.call(self._w, 'select')
        Sensor(4642)


class Scale(Widget):
    """Scale widget which can display a numerical scale."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(4643)
        """Construct a scale widget with the parent MASTER.

        Valid resource names: activebackground, background, bigincrement, bd,
        bg, borderwidth, command, cursor, digits, fg, font, foreground, from,
        highlightbackground, highlightcolor, highlightthickness, label,
        length, orient, relief, repeatdelay, repeatinterval, resolution,
        showvalue, sliderlength, sliderrelief, state, takefocus,
        tickinterval, to, troughcolor, variable, width."""
        Widget.__init__(self, master, 'scale', cnf, kw)
        Sensor(4644)

    def get(self):
        Sensor(4645)
        """Get the current value as integer or float."""
        value = self.tk.call(self._w, 'get')
        Sensor(4646)
        try:
            py_ass_parser4647 = getint(value)
            Sensor(4647)
            return py_ass_parser4647
        except ValueError:
            py_ass_parser4648 = getdouble(value)
            Sensor(4648)
            return py_ass_parser4648
        Sensor(4649)

    def set(self, value):
        Sensor(4650)
        """Set the value to VALUE."""
        self.tk.call(self._w, 'set', value)
        Sensor(4651)

    def coords(self, value=None):
        Sensor(4652)
        """Return a tuple (X,Y) of the point along the centerline of the
        trough that corresponds to VALUE or the current value if None is
        given."""
        py_ass_parser4653 = self._getints(self.tk.call(self._w, 'coords',
            value))
        Sensor(4653)
        return py_ass_parser4653

    def identify(self, x, y):
        Sensor(4654)
        """Return where the point X,Y lies. Valid return values are "slider",
        "though1" and "though2"."""
        py_ass_parser4655 = self.tk.call(self._w, 'identify', x, y)
        Sensor(4655)
        return py_ass_parser4655


class Scrollbar(Widget):
    """Scrollbar widget which displays a slider at a certain position."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(4656)
        """Construct a scrollbar widget with the parent MASTER.

        Valid resource names: activebackground, activerelief,
        background, bd, bg, borderwidth, command, cursor,
        elementborderwidth, highlightbackground,
        highlightcolor, highlightthickness, jump, orient,
        relief, repeatdelay, repeatinterval, takefocus,
        troughcolor, width."""
        Widget.__init__(self, master, 'scrollbar', cnf, kw)
        Sensor(4657)

    def activate(self, index):
        Sensor(4658)
        """Display the element at INDEX with activebackground and activerelief.
        INDEX can be "arrow1","slider" or "arrow2"."""
        self.tk.call(self._w, 'activate', index)
        Sensor(4659)

    def delta(self, deltax, deltay):
        Sensor(4660)
        """Return the fractional change of the scrollbar setting if it
        would be moved by DELTAX or DELTAY pixels."""
        py_ass_parser4661 = getdouble(self.tk.call(self._w, 'delta', deltax,
            deltay))
        Sensor(4661)
        return py_ass_parser4661

    def fraction(self, x, y):
        Sensor(4662)
        """Return the fractional value which corresponds to a slider
        position of X,Y."""
        py_ass_parser4663 = getdouble(self.tk.call(self._w, 'fraction', x, y))
        Sensor(4663)
        return py_ass_parser4663

    def identify(self, x, y):
        Sensor(4664)
        """Return the element under position X,Y as one of
        "arrow1","slider","arrow2" or ""."""
        py_ass_parser4665 = self.tk.call(self._w, 'identify', x, y)
        Sensor(4665)
        return py_ass_parser4665

    def get(self):
        Sensor(4666)
        """Return the current fractional values (upper and lower end)
        of the slider position."""
        py_ass_parser4667 = self._getdoubles(self.tk.call(self._w, 'get'))
        Sensor(4667)
        return py_ass_parser4667

    def set(self, *args):
        Sensor(4668)
        """Set the fractional values of the slider position (upper and
        lower ends as value between 0 and 1)."""
        self.tk.call((self._w, 'set') + args)
        Sensor(4669)


class Text(Widget, XView, YView):
    """Text widget which can display text in various forms."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(4670)
        """Construct a text widget with the parent MASTER.

        STANDARD OPTIONS

            background, borderwidth, cursor,
            exportselection, font, foreground,
            highlightbackground, highlightcolor,
            highlightthickness, insertbackground,
            insertborderwidth, insertofftime,
            insertontime, insertwidth, padx, pady,
            relief, selectbackground,
            selectborderwidth, selectforeground,
            setgrid, takefocus,
            xscrollcommand, yscrollcommand,

        WIDGET-SPECIFIC OPTIONS

            autoseparators, height, maxundo,
            spacing1, spacing2, spacing3,
            state, tabs, undo, width, wrap,

        """
        Widget.__init__(self, master, 'text', cnf, kw)
        Sensor(4671)

    def bbox(self, *args):
        Sensor(4672)
        """Return a tuple of (x,y,width,height) which gives the bounding
        box of the visible part of the character at the index in ARGS."""
        py_ass_parser4673 = self._getints(self.tk.call((self._w, 'bbox') +
            args)) or None
        Sensor(4673)
        return py_ass_parser4673

    def tk_textSelectTo(self, index):
        Sensor(4674)
        self.tk.call('tk_textSelectTo', self._w, index)
        Sensor(4675)

    def tk_textBackspace(self):
        Sensor(4676)
        self.tk.call('tk_textBackspace', self._w)
        Sensor(4677)

    def tk_textIndexCloser(self, a, b, c):
        Sensor(4678)
        self.tk.call('tk_textIndexCloser', self._w, a, b, c)
        Sensor(4679)

    def tk_textResetAnchor(self, index):
        Sensor(4680)
        self.tk.call('tk_textResetAnchor', self._w, index)
        Sensor(4681)

    def compare(self, index1, op, index2):
        Sensor(4682)
        """Return whether between index INDEX1 and index INDEX2 the
        relation OP is satisfied. OP is one of <, <=, ==, >=, >, or !=."""
        py_ass_parser4683 = self.tk.getboolean(self.tk.call(self._w,
            'compare', index1, op, index2))
        Sensor(4683)
        return py_ass_parser4683

    def debug(self, boolean=None):
        Sensor(4684)
        """Turn on the internal consistency checks of the B-Tree inside the text
        widget according to BOOLEAN."""
        py_ass_parser4685 = self.tk.getboolean(self.tk.call(self._w,
            'debug', boolean))
        Sensor(4685)
        return py_ass_parser4685

    def delete(self, index1, index2=None):
        Sensor(4686)
        """Delete the characters between INDEX1 and INDEX2 (not included)."""
        self.tk.call(self._w, 'delete', index1, index2)
        Sensor(4687)

    def dlineinfo(self, index):
        Sensor(4688)
        """Return tuple (x,y,width,height,baseline) giving the bounding box
        and baseline position of the visible part of the line containing
        the character at INDEX."""
        py_ass_parser4689 = self._getints(self.tk.call(self._w, 'dlineinfo',
            index))
        Sensor(4689)
        return py_ass_parser4689

    def dump(self, index1, index2=None, command=None, **kw):
        Sensor(4690)
        """Return the contents of the widget between index1 and index2.

        The type of contents returned in filtered based on the keyword
        parameters; if 'all', 'image', 'mark', 'tag', 'text', or 'window' are
        given and true, then the corresponding items are returned. The result
        is a list of triples of the form (key, value, index). If none of the
        keywords are true then 'all' is used by default.

        If the 'command' argument is given, it is called once for each element
        of the list of triples, with the values of each triple serving as the
        arguments to the function. In this case the list is not returned."""
        args = []
        func_name = None
        result = None
        Sensor(4691)
        if not command:
            Sensor(4692)
            result = []

            def append_triple(key, value, index, result=result):
                Sensor(4693)
                result.append((key, value, index))
                Sensor(4694)
            command = append_triple
        Sensor(4695)
        try:
            Sensor(4696)
            if not isinstance(command, str):
                Sensor(4697)
                func_name = command = self._register(command)
            Sensor(4698)
            args += ['-command', command]
            Sensor(4699)
            for key in kw:
                Sensor(4700)
                if kw[key]:
                    Sensor(4701)
                    args.append('-' + key)
            Sensor(4702)
            args.append(index1)
            Sensor(4703)
            if index2:
                Sensor(4704)
                args.append(index2)
            Sensor(4705)
            self.tk.call(self._w, 'dump', *args)
            py_ass_parser4706 = result
            Sensor(4706)
            return py_ass_parser4706
        finally:
            Sensor(4707)
            if func_name:
                Sensor(4708)
                self.deletecommand(func_name)
        Sensor(4709)

    def edit(self, *args):
        Sensor(4710)
        """Internal method

        This method controls the undo mechanism and
        the modified flag. The exact behavior of the
        command depends on the option argument that
        follows the edit argument. The following forms
        of the command are currently supported:

        edit_modified, edit_redo, edit_reset, edit_separator
        and edit_undo

        """
        py_ass_parser4711 = self.tk.call(self._w, 'edit', *args)
        Sensor(4711)
        return py_ass_parser4711

    def edit_modified(self, arg=None):
        Sensor(4712)
        """Get or Set the modified flag

        If arg is not specified, returns the modified
        flag of the widget. The insert, delete, edit undo and
        edit redo commands or the user can set or clear the
        modified flag. If boolean is specified, sets the
        modified flag of the widget to arg.
        """
        py_ass_parser4713 = self.edit('modified', arg)
        Sensor(4713)
        return py_ass_parser4713

    def edit_redo(self):
        Sensor(4714)
        """Redo the last undone edit

        When the undo option is true, reapplies the last
        undone edits provided no other edits were done since
        then. Generates an error when the redo stack is empty.
        Does nothing when the undo option is false.
        """
        py_ass_parser4715 = self.edit('redo')
        Sensor(4715)
        return py_ass_parser4715

    def edit_reset(self):
        Sensor(4716)
        """Clears the undo and redo stacks
        """
        py_ass_parser4717 = self.edit('reset')
        Sensor(4717)
        return py_ass_parser4717

    def edit_separator(self):
        Sensor(4718)
        """Inserts a separator (boundary) on the undo stack.

        Does nothing when the undo option is false
        """
        py_ass_parser4719 = self.edit('separator')
        Sensor(4719)
        return py_ass_parser4719

    def edit_undo(self):
        Sensor(4720)
        """Undoes the last edit action

        If the undo option is true. An edit action is defined
        as all the insert and delete commands that are recorded
        on the undo stack in between two separators. Generates
        an error when the undo stack is empty. Does nothing
        when the undo option is false
        """
        py_ass_parser4721 = self.edit('undo')
        Sensor(4721)
        return py_ass_parser4721

    def get(self, index1, index2=None):
        Sensor(4722)
        """Return the text from INDEX1 to INDEX2 (not included)."""
        py_ass_parser4723 = self.tk.call(self._w, 'get', index1, index2)
        Sensor(4723)
        return py_ass_parser4723

    def image_cget(self, index, option):
        Sensor(4724)
        """Return the value of OPTION of an embedded image at INDEX."""
        Sensor(4725)
        if option[:1] != '-':
            Sensor(4726)
            option = '-' + option
        Sensor(4727)
        if option[-1:] == '_':
            Sensor(4728)
            option = option[:-1]
        py_ass_parser4729 = self.tk.call(self._w, 'image', 'cget', index,
            option)
        Sensor(4729)
        return py_ass_parser4729

    def image_configure(self, index, cnf=None, **kw):
        Sensor(4730)
        """Configure an embedded image at INDEX."""
        py_ass_parser4731 = self._configure(('image', 'configure', index),
            cnf, kw)
        Sensor(4731)
        return py_ass_parser4731

    def image_create(self, index, cnf={}, **kw):
        Sensor(4732)
        """Create an embedded image at INDEX."""
        py_ass_parser4733 = self.tk.call(self._w, 'image', 'create', index,
            *self._options(cnf, kw))
        Sensor(4733)
        return py_ass_parser4733

    def image_names(self):
        Sensor(4734)
        """Return all names of embedded images in this widget."""
        py_ass_parser4735 = self.tk.call(self._w, 'image', 'names')
        Sensor(4735)
        return py_ass_parser4735

    def index(self, index):
        Sensor(4736)
        """Return the index in the form line.char for INDEX."""
        py_ass_parser4737 = str(self.tk.call(self._w, 'index', index))
        Sensor(4737)
        return py_ass_parser4737

    def insert(self, index, chars, *args):
        Sensor(4738)
        """Insert CHARS before the characters at INDEX. An additional
        tag can be given in ARGS. Additional CHARS and tags can follow in ARGS."""
        self.tk.call((self._w, 'insert', index, chars) + args)
        Sensor(4739)

    def mark_gravity(self, markName, direction=None):
        Sensor(4740)
        """Change the gravity of a mark MARKNAME to DIRECTION (LEFT or RIGHT).
        Return the current value if None is given for DIRECTION."""
        py_ass_parser4741 = self.tk.call((self._w, 'mark', 'gravity',
            markName, direction))
        Sensor(4741)
        return py_ass_parser4741

    def mark_names(self):
        Sensor(4742)
        """Return all mark names."""
        py_ass_parser4743 = self.tk.splitlist(self.tk.call(self._w, 'mark',
            'names'))
        Sensor(4743)
        return py_ass_parser4743

    def mark_set(self, markName, index):
        Sensor(4744)
        """Set mark MARKNAME before the character at INDEX."""
        self.tk.call(self._w, 'mark', 'set', markName, index)
        Sensor(4745)

    def mark_unset(self, *markNames):
        Sensor(4746)
        """Delete all marks in MARKNAMES."""
        self.tk.call((self._w, 'mark', 'unset') + markNames)
        Sensor(4747)

    def mark_next(self, index):
        Sensor(4748)
        """Return the name of the next mark after INDEX."""
        py_ass_parser4749 = self.tk.call(self._w, 'mark', 'next', index
            ) or None
        Sensor(4749)
        return py_ass_parser4749

    def mark_previous(self, index):
        Sensor(4750)
        """Return the name of the previous mark before INDEX."""
        py_ass_parser4751 = self.tk.call(self._w, 'mark', 'previous', index
            ) or None
        Sensor(4751)
        return py_ass_parser4751

    def scan_mark(self, x, y):
        Sensor(4752)
        """Remember the current X, Y coordinates."""
        self.tk.call(self._w, 'scan', 'mark', x, y)
        Sensor(4753)

    def scan_dragto(self, x, y):
        Sensor(4754)
        """Adjust the view of the text to 10 times the
        difference between X and Y and the coordinates given in
        scan_mark."""
        self.tk.call(self._w, 'scan', 'dragto', x, y)
        Sensor(4755)

    def search(self, pattern, index, stopindex=None, forwards=None,
        backwards=None, exact=None, regexp=None, nocase=None, count=None,
        elide=None):
        Sensor(4756)
        """Search PATTERN beginning from INDEX until STOPINDEX.
        Return the index of the first character of a match or an
        empty string."""
        args = [self._w, 'search']
        Sensor(4757)
        if forwards:
            Sensor(4758)
            args.append('-forwards')
        Sensor(4759)
        if backwards:
            Sensor(4760)
            args.append('-backwards')
        Sensor(4761)
        if exact:
            Sensor(4762)
            args.append('-exact')
        Sensor(4763)
        if regexp:
            Sensor(4764)
            args.append('-regexp')
        Sensor(4765)
        if nocase:
            Sensor(4766)
            args.append('-nocase')
        Sensor(4767)
        if elide:
            Sensor(4768)
            args.append('-elide')
        Sensor(4769)
        if count:
            Sensor(4770)
            args.append('-count')
            args.append(count)
        Sensor(4771)
        if pattern and pattern[0] == '-':
            Sensor(4772)
            args.append('--')
        Sensor(4773)
        args.append(pattern)
        args.append(index)
        Sensor(4774)
        if stopindex:
            Sensor(4775)
            args.append(stopindex)
        py_ass_parser4776 = str(self.tk.call(tuple(args)))
        Sensor(4776)
        return py_ass_parser4776

    def see(self, index):
        Sensor(4777)
        """Scroll such that the character at INDEX is visible."""
        self.tk.call(self._w, 'see', index)
        Sensor(4778)

    def tag_add(self, tagName, index1, *args):
        Sensor(4779)
        """Add tag TAGNAME to all characters between INDEX1 and index2 in ARGS.
        Additional pairs of indices may follow in ARGS."""
        self.tk.call((self._w, 'tag', 'add', tagName, index1) + args)
        Sensor(4780)

    def tag_unbind(self, tagName, sequence, funcid=None):
        Sensor(4781)
        """Unbind for all characters with TAGNAME for event SEQUENCE  the
        function identified with FUNCID."""
        self.tk.call(self._w, 'tag', 'bind', tagName, sequence, '')
        Sensor(4782)
        if funcid:
            Sensor(4783)
            self.deletecommand(funcid)
        Sensor(4784)

    def tag_bind(self, tagName, sequence, func, add=None):
        Sensor(4785)
        """Bind to all characters with TAGNAME at event SEQUENCE a call to function FUNC.

        An additional boolean parameter ADD specifies whether FUNC will be
        called additionally to the other bound function or whether it will
        replace the previous function. See bind for the return value."""
        py_ass_parser4786 = self._bind((self._w, 'tag', 'bind', tagName),
            sequence, func, add)
        Sensor(4786)
        return py_ass_parser4786

    def tag_cget(self, tagName, option):
        Sensor(4787)
        """Return the value of OPTION for tag TAGNAME."""
        Sensor(4788)
        if option[:1] != '-':
            Sensor(4789)
            option = '-' + option
        Sensor(4790)
        if option[-1:] == '_':
            Sensor(4791)
            option = option[:-1]
        py_ass_parser4792 = self.tk.call(self._w, 'tag', 'cget', tagName,
            option)
        Sensor(4792)
        return py_ass_parser4792

    def tag_configure(self, tagName, cnf=None, **kw):
        Sensor(4793)
        """Configure a tag TAGNAME."""
        py_ass_parser4794 = self._configure(('tag', 'configure', tagName),
            cnf, kw)
        Sensor(4794)
        return py_ass_parser4794
    tag_config = tag_configure

    def tag_delete(self, *tagNames):
        Sensor(4795)
        """Delete all tags in TAGNAMES."""
        self.tk.call((self._w, 'tag', 'delete') + tagNames)
        Sensor(4796)

    def tag_lower(self, tagName, belowThis=None):
        Sensor(4797)
        """Change the priority of tag TAGNAME such that it is lower
        than the priority of BELOWTHIS."""
        self.tk.call(self._w, 'tag', 'lower', tagName, belowThis)
        Sensor(4798)

    def tag_names(self, index=None):
        Sensor(4799)
        """Return a list of all tag names."""
        py_ass_parser4800 = self.tk.splitlist(self.tk.call(self._w, 'tag',
            'names', index))
        Sensor(4800)
        return py_ass_parser4800

    def tag_nextrange(self, tagName, index1, index2=None):
        Sensor(4801)
        """Return a list of start and end index for the first sequence of
        characters between INDEX1 and INDEX2 which all have tag TAGNAME.
        The text is searched forward from INDEX1."""
        py_ass_parser4802 = self.tk.splitlist(self.tk.call(self._w, 'tag',
            'nextrange', tagName, index1, index2))
        Sensor(4802)
        return py_ass_parser4802

    def tag_prevrange(self, tagName, index1, index2=None):
        Sensor(4803)
        """Return a list of start and end index for the first sequence of
        characters between INDEX1 and INDEX2 which all have tag TAGNAME.
        The text is searched backwards from INDEX1."""
        py_ass_parser4804 = self.tk.splitlist(self.tk.call(self._w, 'tag',
            'prevrange', tagName, index1, index2))
        Sensor(4804)
        return py_ass_parser4804

    def tag_raise(self, tagName, aboveThis=None):
        Sensor(4805)
        """Change the priority of tag TAGNAME such that it is higher
        than the priority of ABOVETHIS."""
        self.tk.call(self._w, 'tag', 'raise', tagName, aboveThis)
        Sensor(4806)

    def tag_ranges(self, tagName):
        Sensor(4807)
        """Return a list of ranges of text which have tag TAGNAME."""
        py_ass_parser4808 = self.tk.splitlist(self.tk.call(self._w, 'tag',
            'ranges', tagName))
        Sensor(4808)
        return py_ass_parser4808

    def tag_remove(self, tagName, index1, index2=None):
        Sensor(4809)
        """Remove tag TAGNAME from all characters between INDEX1 and INDEX2."""
        self.tk.call(self._w, 'tag', 'remove', tagName, index1, index2)
        Sensor(4810)

    def window_cget(self, index, option):
        Sensor(4811)
        """Return the value of OPTION of an embedded window at INDEX."""
        Sensor(4812)
        if option[:1] != '-':
            Sensor(4813)
            option = '-' + option
        Sensor(4814)
        if option[-1:] == '_':
            Sensor(4815)
            option = option[:-1]
        py_ass_parser4816 = self.tk.call(self._w, 'window', 'cget', index,
            option)
        Sensor(4816)
        return py_ass_parser4816

    def window_configure(self, index, cnf=None, **kw):
        Sensor(4817)
        """Configure an embedded window at INDEX."""
        py_ass_parser4818 = self._configure(('window', 'configure', index),
            cnf, kw)
        Sensor(4818)
        return py_ass_parser4818
    window_config = window_configure

    def window_create(self, index, cnf={}, **kw):
        Sensor(4819)
        """Create a window at INDEX."""
        self.tk.call((self._w, 'window', 'create', index) + self._options(
            cnf, kw))
        Sensor(4820)

    def window_names(self):
        Sensor(4821)
        """Return all names of embedded windows in this widget."""
        py_ass_parser4822 = self.tk.splitlist(self.tk.call(self._w,
            'window', 'names'))
        Sensor(4822)
        return py_ass_parser4822

    def yview_pickplace(self, *what):
        Sensor(4823)
        """Obsolete function, use see."""
        self.tk.call((self._w, 'yview', '-pickplace') + what)
        Sensor(4824)


class _setit:
    """Internal class. It wraps the command in the widget OptionMenu."""

    def __init__(self, var, value, callback=None):
        Sensor(4825)
        self.__value = value
        self.__var = var
        self.__callback = callback
        Sensor(4826)

    def __call__(self, *args):
        Sensor(4827)
        self.__var.set(self.__value)
        Sensor(4828)
        if self.__callback:
            Sensor(4829)
            self.__callback(self.__value, *args)
        Sensor(4830)


class OptionMenu(Menubutton):
    """OptionMenu which allows the user to select a value from a menu."""

    def __init__(self, master, variable, value, *values, **kwargs):
        Sensor(4831)
        """Construct an optionmenu widget with the parent MASTER, with
        the resource textvariable set to VARIABLE, the initially selected
        value VALUE, the other menu values VALUES and an additional
        keyword argument command."""
        kw = {'borderwidth': 2, 'textvariable': variable, 'indicatoron': 1,
            'relief': RAISED, 'anchor': 'c', 'highlightthickness': 2}
        Widget.__init__(self, master, 'menubutton', kw)
        self.widgetName = 'tk_optionMenu'
        menu = self.__menu = Menu(self, name='menu', tearoff=0)
        self.menuname = menu._w
        callback = kwargs.get('command')
        Sensor(4832)
        if 'command' in kwargs:
            Sensor(4833)
            del kwargs['command']
        Sensor(4834)
        if kwargs:
            Sensor(4835)
            raise TclError, 'unknown option -' + kwargs.keys()[0]
        Sensor(4836)
        menu.add_command(label=value, command=_setit(variable, value, callback)
            )
        Sensor(4837)
        for v in values:
            Sensor(4838)
            menu.add_command(label=v, command=_setit(variable, v, callback))
        Sensor(4839)
        self['menu'] = menu
        Sensor(4840)

    def __getitem__(self, name):
        Sensor(4841)
        if name == 'menu':
            py_ass_parser4842 = self.__menu
            Sensor(4842)
            return py_ass_parser4842
        py_ass_parser4843 = Widget.__getitem__(self, name)
        Sensor(4843)
        return py_ass_parser4843

    def destroy(self):
        Sensor(4844)
        """Destroy this widget and the associated menu."""
        Menubutton.destroy(self)
        self.__menu = None
        Sensor(4845)


class Image:
    """Base class for images."""
    _last_id = 0

    def __init__(self, imgtype, name=None, cnf={}, master=None, **kw):
        Sensor(4846)
        self.name = None
        Sensor(4847)
        if not master:
            Sensor(4848)
            master = _default_root
            Sensor(4849)
            if not master:
                Sensor(4850)
                raise RuntimeError, 'Too early to create image'
        Sensor(4851)
        self.tk = master.tk
        Sensor(4852)
        if not name:
            Sensor(4853)
            Image._last_id += 1
            name = 'pyimage%r' % (Image._last_id,)
            Sensor(4854)
            if name[0] == '-':
                Sensor(4855)
                name = '_' + name[1:]
        Sensor(4856)
        if kw and cnf:
            Sensor(4857)
            cnf = _cnfmerge((cnf, kw))
        else:
            Sensor(4858)
            if kw:
                Sensor(4859)
                cnf = kw
        Sensor(4860)
        options = ()
        Sensor(4861)
        for k, v in cnf.items():
            Sensor(4862)
            if hasattr(v, '__call__'):
                Sensor(4863)
                v = self._register(v)
            Sensor(4864)
            options = options + ('-' + k, v)
        Sensor(4865)
        self.tk.call(('image', 'create', imgtype, name) + options)
        self.name = name
        Sensor(4866)

    def __str__(self):
        py_ass_parser4867 = self.name
        Sensor(4867)
        return py_ass_parser4867

    def __del__(self):
        Sensor(4868)
        if self.name:
            Sensor(4869)
            try:
                Sensor(4870)
                self.tk.call('image', 'delete', self.name)
            except TclError:
                Sensor(4871)
                pass
        Sensor(4872)

    def __setitem__(self, key, value):
        Sensor(4873)
        self.tk.call(self.name, 'configure', '-' + key, value)
        Sensor(4874)

    def __getitem__(self, key):
        py_ass_parser4875 = self.tk.call(self.name, 'configure', '-' + key)
        Sensor(4875)
        return py_ass_parser4875

    def configure(self, **kw):
        Sensor(4876)
        """Configure the image."""
        res = ()
        Sensor(4877)
        for k, v in _cnfmerge(kw).items():
            Sensor(4878)
            if v is not None:
                Sensor(4879)
                if k[-1] == '_':
                    Sensor(4880)
                    k = k[:-1]
                Sensor(4881)
                if hasattr(v, '__call__'):
                    Sensor(4882)
                    v = self._register(v)
                Sensor(4883)
                res = res + ('-' + k, v)
        Sensor(4884)
        self.tk.call((self.name, 'config') + res)
        Sensor(4885)
    config = configure

    def height(self):
        Sensor(4886)
        """Return the height of the image."""
        py_ass_parser4887 = getint(self.tk.call('image', 'height', self.name))
        Sensor(4887)
        return py_ass_parser4887

    def type(self):
        Sensor(4888)
        """Return the type of the imgage, e.g. "photo" or "bitmap"."""
        py_ass_parser4889 = self.tk.call('image', 'type', self.name)
        Sensor(4889)
        return py_ass_parser4889

    def width(self):
        Sensor(4890)
        """Return the width of the image."""
        py_ass_parser4891 = getint(self.tk.call('image', 'width', self.name))
        Sensor(4891)
        return py_ass_parser4891


class PhotoImage(Image):
    """Widget which can display colored images in GIF, PPM/PGM format."""

    def __init__(self, name=None, cnf={}, master=None, **kw):
        Sensor(4892)
        """Create an image with NAME.

        Valid resource names: data, format, file, gamma, height, palette,
        width."""
        Image.__init__(self, 'photo', name, cnf, master, **kw)
        Sensor(4893)

    def blank(self):
        Sensor(4894)
        """Display a transparent image."""
        self.tk.call(self.name, 'blank')
        Sensor(4895)

    def cget(self, option):
        Sensor(4896)
        """Return the value of OPTION."""
        py_ass_parser4897 = self.tk.call(self.name, 'cget', '-' + option)
        Sensor(4897)
        return py_ass_parser4897

    def __getitem__(self, key):
        py_ass_parser4898 = self.tk.call(self.name, 'cget', '-' + key)
        Sensor(4898)
        return py_ass_parser4898

    def copy(self):
        Sensor(4899)
        """Return a new PhotoImage with the same image as this widget."""
        destImage = PhotoImage()
        self.tk.call(destImage, 'copy', self.name)
        py_ass_parser4900 = destImage
        Sensor(4900)
        return py_ass_parser4900

    def zoom(self, x, y=''):
        Sensor(4901)
        """Return a new PhotoImage with the same image as this widget
        but zoom it with X and Y."""
        destImage = PhotoImage()
        Sensor(4902)
        if y == '':
            Sensor(4903)
            y = x
        Sensor(4904)
        self.tk.call(destImage, 'copy', self.name, '-zoom', x, y)
        py_ass_parser4905 = destImage
        Sensor(4905)
        return py_ass_parser4905

    def subsample(self, x, y=''):
        Sensor(4906)
        """Return a new PhotoImage based on the same image as this widget
        but use only every Xth or Yth pixel."""
        destImage = PhotoImage()
        Sensor(4907)
        if y == '':
            Sensor(4908)
            y = x
        Sensor(4909)
        self.tk.call(destImage, 'copy', self.name, '-subsample', x, y)
        py_ass_parser4910 = destImage
        Sensor(4910)
        return py_ass_parser4910

    def get(self, x, y):
        Sensor(4911)
        """Return the color (red, green, blue) of the pixel at X,Y."""
        py_ass_parser4912 = self.tk.call(self.name, 'get', x, y)
        Sensor(4912)
        return py_ass_parser4912

    def put(self, data, to=None):
        Sensor(4913)
        """Put row formatted colors to image starting from
        position TO, e.g. image.put("{red green} {blue yellow}", to=(4,6))"""
        args = self.name, 'put', data
        Sensor(4914)
        if to:
            Sensor(4915)
            if to[0] == '-to':
                Sensor(4916)
                to = to[1:]
            Sensor(4917)
            args = args + ('-to',) + tuple(to)
        Sensor(4918)
        self.tk.call(args)
        Sensor(4919)

    def write(self, filename, format=None, from_coords=None):
        Sensor(4920)
        """Write image to file FILENAME in FORMAT starting from
        position FROM_COORDS."""
        args = self.name, 'write', filename
        Sensor(4921)
        if format:
            Sensor(4922)
            args = args + ('-format', format)
        Sensor(4923)
        if from_coords:
            Sensor(4924)
            args = args + ('-from',) + tuple(from_coords)
        Sensor(4925)
        self.tk.call(args)
        Sensor(4926)


class BitmapImage(Image):
    """Widget which can display a bitmap."""

    def __init__(self, name=None, cnf={}, master=None, **kw):
        Sensor(4927)
        """Create a bitmap with NAME.

        Valid resource names: background, data, file, foreground, maskdata, maskfile."""
        Image.__init__(self, 'bitmap', name, cnf, master, **kw)
        Sensor(4928)


def image_names():
    py_ass_parser4929 = _default_root.tk.call('image', 'names')
    Sensor(4929)
    return py_ass_parser4929


def image_types():
    py_ass_parser4930 = _default_root.tk.call('image', 'types')
    Sensor(4930)
    return py_ass_parser4930


class Spinbox(Widget, XView):
    """spinbox widget."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(4931)
        """Construct a spinbox widget with the parent MASTER.

        STANDARD OPTIONS

            activebackground, background, borderwidth,
            cursor, exportselection, font, foreground,
            highlightbackground, highlightcolor,
            highlightthickness, insertbackground,
            insertborderwidth, insertofftime,
            insertontime, insertwidth, justify, relief,
            repeatdelay, repeatinterval,
            selectbackground, selectborderwidth
            selectforeground, takefocus, textvariable
            xscrollcommand.

        WIDGET-SPECIFIC OPTIONS

            buttonbackground, buttoncursor,
            buttondownrelief, buttonuprelief,
            command, disabledbackground,
            disabledforeground, format, from,
            invalidcommand, increment,
            readonlybackground, state, to,
            validate, validatecommand values,
            width, wrap,
        """
        Widget.__init__(self, master, 'spinbox', cnf, kw)
        Sensor(4932)

    def bbox(self, index):
        Sensor(4933)
        """Return a tuple of X1,Y1,X2,Y2 coordinates for a
        rectangle which encloses the character given by index.

        The first two elements of the list give the x and y
        coordinates of the upper-left corner of the screen
        area covered by the character (in pixels relative
        to the widget) and the last two elements give the
        width and height of the character, in pixels. The
        bounding box may refer to a region outside the
        visible area of the window.
        """
        py_ass_parser4934 = self.tk.call(self._w, 'bbox', index)
        Sensor(4934)
        return py_ass_parser4934

    def delete(self, first, last=None):
        Sensor(4935)
        """Delete one or more elements of the spinbox.

        First is the index of the first character to delete,
        and last is the index of the character just after
        the last one to delete. If last isn't specified it
        defaults to first+1, i.e. a single character is
        deleted.  This command returns an empty string.
        """
        py_ass_parser4936 = self.tk.call(self._w, 'delete', first, last)
        Sensor(4936)
        return py_ass_parser4936

    def get(self):
        Sensor(4937)
        """Returns the spinbox's string"""
        py_ass_parser4938 = self.tk.call(self._w, 'get')
        Sensor(4938)
        return py_ass_parser4938

    def icursor(self, index):
        Sensor(4939)
        """Alter the position of the insertion cursor.

        The insertion cursor will be displayed just before
        the character given by index. Returns an empty string
        """
        py_ass_parser4940 = self.tk.call(self._w, 'icursor', index)
        Sensor(4940)
        return py_ass_parser4940

    def identify(self, x, y):
        Sensor(4941)
        """Returns the name of the widget at position x, y

        Return value is one of: none, buttondown, buttonup, entry
        """
        py_ass_parser4942 = self.tk.call(self._w, 'identify', x, y)
        Sensor(4942)
        return py_ass_parser4942

    def index(self, index):
        Sensor(4943)
        """Returns the numerical index corresponding to index
        """
        py_ass_parser4944 = self.tk.call(self._w, 'index', index)
        Sensor(4944)
        return py_ass_parser4944

    def insert(self, index, s):
        Sensor(4945)
        """Insert string s at index

         Returns an empty string.
        """
        py_ass_parser4946 = self.tk.call(self._w, 'insert', index, s)
        Sensor(4946)
        return py_ass_parser4946

    def invoke(self, element):
        Sensor(4947)
        """Causes the specified element to be invoked

        The element could be buttondown or buttonup
        triggering the action associated with it.
        """
        py_ass_parser4948 = self.tk.call(self._w, 'invoke', element)
        Sensor(4948)
        return py_ass_parser4948

    def scan(self, *args):
        Sensor(4949)
        """Internal function."""
        py_ass_parser4950 = self._getints(self.tk.call((self._w, 'scan') +
            args)) or ()
        Sensor(4950)
        return py_ass_parser4950

    def scan_mark(self, x):
        Sensor(4951)
        """Records x and the current view in the spinbox window;

        used in conjunction with later scan dragto commands.
        Typically this command is associated with a mouse button
        press in the widget. It returns an empty string.
        """
        py_ass_parser4952 = self.scan('mark', x)
        Sensor(4952)
        return py_ass_parser4952

    def scan_dragto(self, x):
        Sensor(4953)
        """Compute the difference between the given x argument
        and the x argument to the last scan mark command

        It then adjusts the view left or right by 10 times the
        difference in x-coordinates. This command is typically
        associated with mouse motion events in the widget, to
        produce the effect of dragging the spinbox at high speed
        through the window. The return value is an empty string.
        """
        py_ass_parser4954 = self.scan('dragto', x)
        Sensor(4954)
        return py_ass_parser4954

    def selection(self, *args):
        Sensor(4955)
        """Internal function."""
        py_ass_parser4956 = self._getints(self.tk.call((self._w,
            'selection') + args)) or ()
        Sensor(4956)
        return py_ass_parser4956

    def selection_adjust(self, index):
        Sensor(4957)
        """Locate the end of the selection nearest to the character
        given by index,

        Then adjust that end of the selection to be at index
        (i.e including but not going beyond index). The other
        end of the selection is made the anchor point for future
        select to commands. If the selection isn't currently in
        the spinbox, then a new selection is created to include
        the characters between index and the most recent selection
        anchor point, inclusive. Returns an empty string.
        """
        py_ass_parser4958 = self.selection('adjust', index)
        Sensor(4958)
        return py_ass_parser4958

    def selection_clear(self):
        Sensor(4959)
        """Clear the selection

        If the selection isn't in this widget then the
        command has no effect. Returns an empty string.
        """
        py_ass_parser4960 = self.selection('clear')
        Sensor(4960)
        return py_ass_parser4960

    def selection_element(self, element=None):
        Sensor(4961)
        """Sets or gets the currently selected element.

        If a spinbutton element is specified, it will be
        displayed depressed
        """
        py_ass_parser4962 = self.selection('element', element)
        Sensor(4962)
        return py_ass_parser4962


class LabelFrame(Widget):
    """labelframe widget."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(4963)
        """Construct a labelframe widget with the parent MASTER.

        STANDARD OPTIONS

            borderwidth, cursor, font, foreground,
            highlightbackground, highlightcolor,
            highlightthickness, padx, pady, relief,
            takefocus, text

        WIDGET-SPECIFIC OPTIONS

            background, class, colormap, container,
            height, labelanchor, labelwidget,
            visual, width
        """
        Widget.__init__(self, master, 'labelframe', cnf, kw)
        Sensor(4964)


class PanedWindow(Widget):
    """panedwindow widget."""

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(4965)
        """Construct a panedwindow widget with the parent MASTER.

        STANDARD OPTIONS

            background, borderwidth, cursor, height,
            orient, relief, width

        WIDGET-SPECIFIC OPTIONS

            handlepad, handlesize, opaqueresize,
            sashcursor, sashpad, sashrelief,
            sashwidth, showhandle,
        """
        Widget.__init__(self, master, 'panedwindow', cnf, kw)
        Sensor(4966)

    def add(self, child, **kw):
        Sensor(4967)
        """Add a child widget to the panedwindow in a new pane.

        The child argument is the name of the child widget
        followed by pairs of arguments that specify how to
        manage the windows. The possible options and values
        are the ones accepted by the paneconfigure method.
        """
        self.tk.call((self._w, 'add', child) + self._options(kw))
        Sensor(4968)

    def remove(self, child):
        Sensor(4969)
        """Remove the pane containing child from the panedwindow

        All geometry management options for child will be forgotten.
        """
        self.tk.call(self._w, 'forget', child)
        Sensor(4970)
    forget = remove

    def identify(self, x, y):
        Sensor(4971)
        """Identify the panedwindow component at point x, y

        If the point is over a sash or a sash handle, the result
        is a two element list containing the index of the sash or
        handle, and a word indicating whether it is over a sash
        or a handle, such as {0 sash} or {2 handle}. If the point
        is over any other part of the panedwindow, the result is
        an empty list.
        """
        py_ass_parser4972 = self.tk.call(self._w, 'identify', x, y)
        Sensor(4972)
        return py_ass_parser4972

    def proxy(self, *args):
        Sensor(4973)
        """Internal function."""
        py_ass_parser4974 = self._getints(self.tk.call((self._w, 'proxy') +
            args)) or ()
        Sensor(4974)
        return py_ass_parser4974

    def proxy_coord(self):
        Sensor(4975)
        """Return the x and y pair of the most recent proxy location
        """
        py_ass_parser4976 = self.proxy('coord')
        Sensor(4976)
        return py_ass_parser4976

    def proxy_forget(self):
        Sensor(4977)
        """Remove the proxy from the display.
        """
        py_ass_parser4978 = self.proxy('forget')
        Sensor(4978)
        return py_ass_parser4978

    def proxy_place(self, x, y):
        Sensor(4979)
        """Place the proxy at the given x and y coordinates.
        """
        py_ass_parser4980 = self.proxy('place', x, y)
        Sensor(4980)
        return py_ass_parser4980

    def sash(self, *args):
        Sensor(4981)
        """Internal function."""
        py_ass_parser4982 = self._getints(self.tk.call((self._w, 'sash') +
            args)) or ()
        Sensor(4982)
        return py_ass_parser4982

    def sash_coord(self, index):
        Sensor(4983)
        """Return the current x and y pair for the sash given by index.

        Index must be an integer between 0 and 1 less than the
        number of panes in the panedwindow. The coordinates given are
        those of the top left corner of the region containing the sash.
        pathName sash dragto index x y This command computes the
        difference between the given coordinates and the coordinates
        given to the last sash coord command for the given sash. It then
        moves that sash the computed difference. The return value is the
        empty string.
        """
        py_ass_parser4984 = self.sash('coord', index)
        Sensor(4984)
        return py_ass_parser4984

    def sash_mark(self, index):
        Sensor(4985)
        """Records x and y for the sash given by index;

        Used in conjunction with later dragto commands to move the sash.
        """
        py_ass_parser4986 = self.sash('mark', index)
        Sensor(4986)
        return py_ass_parser4986

    def sash_place(self, index, x, y):
        Sensor(4987)
        """Place the sash given by index at the given coordinates
        """
        py_ass_parser4988 = self.sash('place', index, x, y)
        Sensor(4988)
        return py_ass_parser4988

    def panecget(self, child, option):
        Sensor(4989)
        """Query a management option for window.

        Option may be any value allowed by the paneconfigure subcommand
        """
        py_ass_parser4990 = self.tk.call((self._w, 'panecget') + (child, 
            '-' + option))
        Sensor(4990)
        return py_ass_parser4990

    def paneconfigure(self, tagOrId, cnf=None, **kw):
        Sensor(4991)
        """Query or modify the management options for window.

        If no option is specified, returns a list describing all
        of the available options for pathName.  If option is
        specified with no value, then the command returns a list
        describing the one named option (this list will be identical
        to the corresponding sublist of the value returned if no
        option is specified). If one or more option-value pairs are
        specified, then the command modifies the given widget
        option(s) to have the given value(s); in this case the
        command returns an empty string. The following options
        are supported:

        after window
            Insert the window after the window specified. window
            should be the name of a window already managed by pathName.
        before window
            Insert the window before the window specified. window
            should be the name of a window already managed by pathName.
        height size
            Specify a height for the window. The height will be the
            outer dimension of the window including its border, if
            any. If size is an empty string, or if -height is not
            specified, then the height requested internally by the
            window will be used initially; the height may later be
            adjusted by the movement of sashes in the panedwindow.
            Size may be any value accepted by Tk_GetPixels.
        minsize n
            Specifies that the size of the window cannot be made
            less than n. This constraint only affects the size of
            the widget in the paned dimension -- the x dimension
            for horizontal panedwindows, the y dimension for
            vertical panedwindows. May be any value accepted by
            Tk_GetPixels.
        padx n
            Specifies a non-negative value indicating how much
            extra space to leave on each side of the window in
            the X-direction. The value may have any of the forms
            accepted by Tk_GetPixels.
        pady n
            Specifies a non-negative value indicating how much
            extra space to leave on each side of the window in
            the Y-direction. The value may have any of the forms
            accepted by Tk_GetPixels.
        sticky style
            If a window's pane is larger than the requested
            dimensions of the window, this option may be used
            to position (or stretch) the window within its pane.
            Style is a string that contains zero or more of the
            characters n, s, e or w. The string can optionally
            contains spaces or commas, but they are ignored. Each
            letter refers to a side (north, south, east, or west)
            that the window will "stick" to. If both n and s
            (or e and w) are specified, the window will be
            stretched to fill the entire height (or width) of
            its cavity.
        width size
            Specify a width for the window. The width will be
            the outer dimension of the window including its
            border, if any. If size is an empty string, or
            if -width is not specified, then the width requested
            internally by the window will be used initially; the
            width may later be adjusted by the movement of sashes
            in the panedwindow. Size may be any value accepted by
            Tk_GetPixels.

        """
        Sensor(4992)
        if cnf is None and not kw:
            Sensor(4993)
            cnf = {}
            Sensor(4994)
            for x in self.tk.split(self.tk.call(self._w, 'paneconfigure',
                tagOrId)):
                Sensor(4995)
                cnf[x[0][1:]] = (x[0][1:],) + x[1:]
            py_ass_parser4996 = cnf
            Sensor(4996)
            return py_ass_parser4996
        Sensor(4997)
        if type(cnf) == StringType and not kw:
            Sensor(4998)
            x = self.tk.split(self.tk.call(self._w, 'paneconfigure',
                tagOrId, '-' + cnf))
            py_ass_parser4999 = (x[0][1:],) + x[1:]
            Sensor(4999)
            return py_ass_parser4999
        Sensor(5000)
        self.tk.call((self._w, 'paneconfigure', tagOrId) + self._options(
            cnf, kw))
        Sensor(5001)
    paneconfig = paneconfigure

    def panes(self):
        Sensor(5002)
        """Returns an ordered list of the child panes."""
        py_ass_parser5003 = self.tk.call(self._w, 'panes')
        Sensor(5003)
        return py_ass_parser5003


class Studbutton(Button):

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(5004)
        Widget.__init__(self, master, 'studbutton', cnf, kw)
        self.bind('<Any-Enter>', self.tkButtonEnter)
        self.bind('<Any-Leave>', self.tkButtonLeave)
        self.bind('<1>', self.tkButtonDown)
        self.bind('<ButtonRelease-1>', self.tkButtonUp)
        Sensor(5005)


class Tributton(Button):

    def __init__(self, master=None, cnf={}, **kw):
        Sensor(5006)
        Widget.__init__(self, master, 'tributton', cnf, kw)
        self.bind('<Any-Enter>', self.tkButtonEnter)
        self.bind('<Any-Leave>', self.tkButtonLeave)
        self.bind('<1>', self.tkButtonDown)
        self.bind('<ButtonRelease-1>', self.tkButtonUp)
        self['fg'] = self['bg']
        self['activebackground'] = self['bg']
        Sensor(5007)


def _test():
    Sensor(5008)
    root = Tk()
    text = 'This is Tcl/Tk version %s' % TclVersion
    Sensor(5009)
    if TclVersion >= 8.1:
        Sensor(5010)
        try:
            Sensor(5011)
            text = text + unicode('\nThis should be a cedilla: \xe7',
                'iso-8859-1')
        except NameError:
            Sensor(5012)
            pass
    Sensor(5013)
    label = Label(root, text=text)
    label.pack()
    test = Button(root, text='Click me!', command=lambda root=root: root.
        test.configure(text='[%s]' % root.test['text']))
    test.pack()
    root.test = test
    quit = Button(root, text='QUIT', command=root.destroy)
    quit.pack()
    root.iconify()
    root.update()
    root.deiconify()
    root.mainloop()
    Sensor(5014)


Sensor(5015)
if __name__ == '__main__':
    Sensor(5016)
    _test()
Sensor(5017)
