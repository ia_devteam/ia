from sensors import Sensor


Sensor(3446)
import __builtin__
import gc
import sys
import types
import unittest
import weakref
from copy import deepcopy
from test import test_support


class OperatorsTest(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        Sensor(2035)
        unittest.TestCase.__init__(self, *args, **kwargs)
        self.binops = {'add': '+', 'sub': '-', 'mul': '*', 'div': '/',
            'divmod': 'divmod', 'pow': '**', 'lshift': '<<', 'rshift': '>>',
            'and': '&', 'xor': '^', 'or': '|', 'cmp': 'cmp', 'lt': '<',
            'le': '<=', 'eq': '==', 'ne': '!=', 'gt': '>', 'ge': '>='}
        Sensor(2036)
        for name, expr in self.binops.items():
            Sensor(2037)
            if expr.islower():
                Sensor(2038)
                expr = expr + '(a, b)'
            else:
                Sensor(2039)
                expr = 'a %s b' % expr
            Sensor(2040)
            self.binops[name] = expr
        Sensor(2041)
        self.unops = {'pos': '+', 'neg': '-', 'abs': 'abs', 'invert': '~',
            'int': 'int', 'long': 'long', 'float': 'float', 'oct': 'oct',
            'hex': 'hex'}
        Sensor(2042)
        for name, expr in self.unops.items():
            Sensor(2043)
            if expr.islower():
                Sensor(2044)
                expr = expr + '(a)'
            else:
                Sensor(2045)
                expr = '%s a' % expr
            Sensor(2046)
            self.unops[name] = expr
        Sensor(2047)

    def unop_test(self, a, res, expr='len(a)', meth='__len__'):
        Sensor(2048)
        d = {'a': a}
        self.assertEqual(eval(expr, d), res)
        t = type(a)
        m = getattr(t, meth)
        Sensor(2049)
        while meth not in t.__dict__:
            Sensor(2050)
            t = t.__bases__[0]
        Sensor(2051)
        self.assertEqual(getattr(m, 'im_func', m), t.__dict__[meth])
        self.assertEqual(m(a), res)
        bm = getattr(a, meth)
        self.assertEqual(bm(), res)
        Sensor(2052)

    def binop_test(self, a, b, res, expr='a+b', meth='__add__'):
        Sensor(2053)
        d = {'a': a, 'b': b}
        Sensor(2054)
        if meth == '__div__' and 1 / 2 == 0.5:
            Sensor(2055)
            meth = '__truediv__'
        Sensor(2056)
        if meth == '__divmod__':
            Sensor(2057)
            pass
        Sensor(2058)
        self.assertEqual(eval(expr, d), res)
        t = type(a)
        m = getattr(t, meth)
        Sensor(2059)
        while meth not in t.__dict__:
            Sensor(2060)
            t = t.__bases__[0]
        Sensor(2061)
        self.assertEqual(getattr(m, 'im_func', m), t.__dict__[meth])
        self.assertEqual(m(a, b), res)
        bm = getattr(a, meth)
        self.assertEqual(bm(b), res)
        Sensor(2062)

    def ternop_test(self, a, b, c, res, expr='a[b:c]', meth='__getslice__'):
        Sensor(2063)
        d = {'a': a, 'b': b, 'c': c}
        self.assertEqual(eval(expr, d), res)
        t = type(a)
        m = getattr(t, meth)
        Sensor(2064)
        while meth not in t.__dict__:
            Sensor(2065)
            t = t.__bases__[0]
        Sensor(2066)
        self.assertEqual(getattr(m, 'im_func', m), t.__dict__[meth])
        self.assertEqual(m(a, b, c), res)
        bm = getattr(a, meth)
        self.assertEqual(bm(b, c), res)
        Sensor(2067)

    def setop_test(self, a, b, res, stmt='a+=b', meth='__iadd__'):
        Sensor(2068)
        d = {'a': deepcopy(a), 'b': b}
        exec stmt in d
        self.assertEqual(d['a'], res)
        t = type(a)
        m = getattr(t, meth)
        Sensor(2069)
        while meth not in t.__dict__:
            Sensor(2070)
            t = t.__bases__[0]
        Sensor(2071)
        self.assertEqual(getattr(m, 'im_func', m), t.__dict__[meth])
        d['a'] = deepcopy(a)
        m(d['a'], b)
        self.assertEqual(d['a'], res)
        d['a'] = deepcopy(a)
        bm = getattr(d['a'], meth)
        bm(b)
        self.assertEqual(d['a'], res)
        Sensor(2072)

    def set2op_test(self, a, b, c, res, stmt='a[b]=c', meth='__setitem__'):
        Sensor(2073)
        d = {'a': deepcopy(a), 'b': b, 'c': c}
        exec stmt in d
        self.assertEqual(d['a'], res)
        t = type(a)
        m = getattr(t, meth)
        Sensor(2074)
        while meth not in t.__dict__:
            Sensor(2075)
            t = t.__bases__[0]
        Sensor(2076)
        self.assertEqual(getattr(m, 'im_func', m), t.__dict__[meth])
        d['a'] = deepcopy(a)
        m(d['a'], b, c)
        self.assertEqual(d['a'], res)
        d['a'] = deepcopy(a)
        bm = getattr(d['a'], meth)
        bm(b, c)
        self.assertEqual(d['a'], res)
        Sensor(2077)

    def set3op_test(self, a, b, c, d, res, stmt='a[b:c]=d', meth='__setslice__'
        ):
        Sensor(2078)
        dictionary = {'a': deepcopy(a), 'b': b, 'c': c, 'd': d}
        exec stmt in dictionary
        self.assertEqual(dictionary['a'], res)
        t = type(a)
        Sensor(2079)
        while meth not in t.__dict__:
            Sensor(2080)
            t = t.__bases__[0]
        Sensor(2081)
        m = getattr(t, meth)
        self.assertEqual(getattr(m, 'im_func', m), t.__dict__[meth])
        dictionary['a'] = deepcopy(a)
        m(dictionary['a'], b, c, d)
        self.assertEqual(dictionary['a'], res)
        dictionary['a'] = deepcopy(a)
        bm = getattr(dictionary['a'], meth)
        bm(b, c, d)
        self.assertEqual(dictionary['a'], res)
        Sensor(2082)

    def test_lists(self):
        Sensor(2083)
        self.binop_test([1], [2], [1, 2], 'a+b', '__add__')
        self.binop_test([1, 2, 3], 2, 1, 'b in a', '__contains__')
        self.binop_test([1, 2, 3], 4, 0, 'b in a', '__contains__')
        self.binop_test([1, 2, 3], 1, 2, 'a[b]', '__getitem__')
        self.ternop_test([1, 2, 3], 0, 2, [1, 2], 'a[b:c]', '__getslice__')
        self.setop_test([1], [2], [1, 2], 'a+=b', '__iadd__')
        self.setop_test([1, 2], 3, [1, 2, 1, 2, 1, 2], 'a*=b', '__imul__')
        self.unop_test([1, 2, 3], 3, 'len(a)', '__len__')
        self.binop_test([1, 2], 3, [1, 2, 1, 2, 1, 2], 'a*b', '__mul__')
        self.binop_test([1, 2], 3, [1, 2, 1, 2, 1, 2], 'b*a', '__rmul__')
        self.set2op_test([1, 2], 1, 3, [1, 3], 'a[b]=c', '__setitem__')
        self.set3op_test([1, 2, 3, 4], 1, 3, [5, 6], [1, 5, 6, 4],
            'a[b:c]=d', '__setslice__')
        Sensor(2084)

    def test_dicts(self):
        Sensor(2085)
        if hasattr(dict, '__cmp__'):
            Sensor(2086)
            self.binop_test({(1): 2}, {(2): 1}, -1, 'cmp(a,b)', '__cmp__')
        else:
            Sensor(2087)
            self.binop_test({(1): 2}, {(2): 1}, True, 'a < b', '__lt__')
        Sensor(2088)
        self.binop_test({(1): 2, (3): 4}, 1, 1, 'b in a', '__contains__')
        self.binop_test({(1): 2, (3): 4}, 2, 0, 'b in a', '__contains__')
        self.binop_test({(1): 2, (3): 4}, 1, 2, 'a[b]', '__getitem__')
        d = {(1): 2, (3): 4}
        l1 = []
        Sensor(2089)
        for i in d.keys():
            Sensor(2090)
            l1.append(i)
        Sensor(2091)
        l = []
        Sensor(2092)
        for i in iter(d):
            Sensor(2093)
            l.append(i)
        Sensor(2094)
        self.assertEqual(l, l1)
        l = []
        Sensor(2095)
        for i in d.__iter__():
            Sensor(2096)
            l.append(i)
        Sensor(2097)
        self.assertEqual(l, l1)
        l = []
        Sensor(2098)
        for i in dict.__iter__(d):
            Sensor(2099)
            l.append(i)
        Sensor(2100)
        self.assertEqual(l, l1)
        d = {(1): 2, (3): 4}
        self.unop_test(d, 2, 'len(a)', '__len__')
        self.assertEqual(eval(repr(d), {}), d)
        self.assertEqual(eval(d.__repr__(), {}), d)
        self.set2op_test({(1): 2, (3): 4}, 2, 3, {(1): 2, (2): 3, (3): 4},
            'a[b]=c', '__setitem__')
        Sensor(2101)

    def number_operators(self, a, b, skip=[]):
        Sensor(2102)
        dict = {'a': a, 'b': b}
        Sensor(2103)
        for name, expr in self.binops.items():
            Sensor(2104)
            if name not in skip:
                Sensor(2105)
                name = '__%s__' % name
                Sensor(2106)
                if hasattr(a, name):
                    Sensor(2107)
                    res = eval(expr, dict)
                    self.binop_test(a, b, res, expr, name)
        Sensor(2108)
        for name, expr in self.unops.items():
            Sensor(2109)
            if name not in skip:
                Sensor(2110)
                name = '__%s__' % name
                Sensor(2111)
                if hasattr(a, name):
                    Sensor(2112)
                    res = eval(expr, dict)
                    self.unop_test(a, res, expr, name)
        Sensor(2113)

    def test_ints(self):
        Sensor(2114)
        self.number_operators(100, 3)
        self.assertEqual((1).__nonzero__(), 1)
        self.assertEqual((0).__nonzero__(), 0)


        class C(int):

            def __add__(self, other):
                py_ass_parser2115 = NotImplemented
                Sensor(2115)
                return py_ass_parser2115
        self.assertEqual(C(5L), 5)
        Sensor(2116)
        try:
            Sensor(2117)
            C() + ''
        except TypeError:
            Sensor(2118)
            pass
        else:
            Sensor(2119)
            self.fail('NotImplemented should have caused TypeError')
        Sensor(2120)
        try:
            Sensor(2121)
            C(sys.maxint + 1)
        except OverflowError:
            Sensor(2122)
            pass
        else:
            Sensor(2123)
            self.fail('should have raised OverflowError')
        Sensor(2124)

    def test_longs(self):
        Sensor(2125)
        self.number_operators(100L, 3L)
        Sensor(2126)

    def test_floats(self):
        Sensor(2127)
        self.number_operators(100.0, 3.0)
        Sensor(2128)

    def test_complexes(self):
        Sensor(2129)
        self.number_operators(100j, 3j, skip=['lt', 'le', 'gt', 'ge', 'int',
            'long', 'float'])


        class Number(complex):
            __slots__ = ['prec']

            def __new__(cls, *args, **kwds):
                Sensor(2130)
                result = complex.__new__(cls, *args)
                result.prec = kwds.get('prec', 12)
                py_ass_parser2131 = result
                Sensor(2131)
                return py_ass_parser2131

            def __repr__(self):
                Sensor(2132)
                prec = self.prec
                Sensor(2133)
                if self.imag == 0.0:
                    py_ass_parser2134 = '%.*g' % (prec, self.real)
                    Sensor(2134)
                    return py_ass_parser2134
                Sensor(2135)
                if self.real == 0.0:
                    py_ass_parser2136 = '%.*gj' % (prec, self.imag)
                    Sensor(2136)
                    return py_ass_parser2136
                py_ass_parser2137 = '(%.*g+%.*gj)' % (prec, self.real, prec,
                    self.imag)
                Sensor(2137)
                return py_ass_parser2137
            __str__ = __repr__
        a = Number(3.14, prec=6)
        self.assertEqual(repr(a), '3.14')
        self.assertEqual(a.prec, 6)
        a = Number(a, prec=2)
        self.assertEqual(repr(a), '3.1')
        self.assertEqual(a.prec, 2)
        a = Number(234.5)
        self.assertEqual(repr(a), '234.5')
        self.assertEqual(a.prec, 12)
        Sensor(2138)

    @test_support.impl_detail("the module 'xxsubtype' is internal")
    def test_spam_lists(self):
        import copy, xxsubtype as spam

        def spamlist(l, memo=None):
            import xxsubtype as spam
            py_ass_parser2139 = spam.spamlist(l)
            Sensor(2139)
            return py_ass_parser2139
        Sensor(2140)
        copy._deepcopy_dispatch[spam.spamlist] = spamlist
        self.binop_test(spamlist([1]), spamlist([2]), spamlist([1, 2]),
            'a+b', '__add__')
        self.binop_test(spamlist([1, 2, 3]), 2, 1, 'b in a', '__contains__')
        self.binop_test(spamlist([1, 2, 3]), 4, 0, 'b in a', '__contains__')
        self.binop_test(spamlist([1, 2, 3]), 1, 2, 'a[b]', '__getitem__')
        self.ternop_test(spamlist([1, 2, 3]), 0, 2, spamlist([1, 2]),
            'a[b:c]', '__getslice__')
        self.setop_test(spamlist([1]), spamlist([2]), spamlist([1, 2]),
            'a+=b', '__iadd__')
        self.setop_test(spamlist([1, 2]), 3, spamlist([1, 2, 1, 2, 1, 2]),
            'a*=b', '__imul__')
        self.unop_test(spamlist([1, 2, 3]), 3, 'len(a)', '__len__')
        self.binop_test(spamlist([1, 2]), 3, spamlist([1, 2, 1, 2, 1, 2]),
            'a*b', '__mul__')
        self.binop_test(spamlist([1, 2]), 3, spamlist([1, 2, 1, 2, 1, 2]),
            'b*a', '__rmul__')
        self.set2op_test(spamlist([1, 2]), 1, 3, spamlist([1, 3]), 'a[b]=c',
            '__setitem__')
        self.set3op_test(spamlist([1, 2, 3, 4]), 1, 3, spamlist([5, 6]),
            spamlist([1, 5, 6, 4]), 'a[b:c]=d', '__setslice__')


        class C(spam.spamlist):

            def foo(self):
                py_ass_parser2141 = 1
                Sensor(2141)
                return py_ass_parser2141
        a = C()
        self.assertEqual(a, [])
        self.assertEqual(a.foo(), 1)
        a.append(100)
        self.assertEqual(a, [100])
        self.assertEqual(a.getstate(), 0)
        a.setstate(42)
        self.assertEqual(a.getstate(), 42)
        Sensor(2142)

    @test_support.impl_detail("the module 'xxsubtype' is internal")
    def test_spam_dicts(self):
        import copy, xxsubtype as spam

        def spamdict(d, memo=None):
            import xxsubtype as spam
            Sensor(2143)
            sd = spam.spamdict()
            Sensor(2144)
            for k, v in d.items():
                Sensor(2145)
                sd[k] = v
            py_ass_parser2146 = sd
            Sensor(2146)
            return py_ass_parser2146
        Sensor(2147)
        copy._deepcopy_dispatch[spam.spamdict] = spamdict
        self.binop_test(spamdict({(1): 2}), spamdict({(2): 1}), -1,
            'cmp(a,b)', '__cmp__')
        self.binop_test(spamdict({(1): 2, (3): 4}), 1, 1, 'b in a',
            '__contains__')
        self.binop_test(spamdict({(1): 2, (3): 4}), 2, 0, 'b in a',
            '__contains__')
        self.binop_test(spamdict({(1): 2, (3): 4}), 1, 2, 'a[b]', '__getitem__'
            )
        d = spamdict({(1): 2, (3): 4})
        l1 = []
        Sensor(2148)
        for i in d.keys():
            Sensor(2149)
            l1.append(i)
        Sensor(2150)
        l = []
        Sensor(2151)
        for i in iter(d):
            Sensor(2152)
            l.append(i)
        Sensor(2153)
        self.assertEqual(l, l1)
        l = []
        Sensor(2154)
        for i in d.__iter__():
            Sensor(2155)
            l.append(i)
        Sensor(2156)
        self.assertEqual(l, l1)
        l = []
        Sensor(2157)
        for i in type(spamdict({})).__iter__(d):
            Sensor(2158)
            l.append(i)
        Sensor(2159)
        self.assertEqual(l, l1)
        straightd = {(1): 2, (3): 4}
        spamd = spamdict(straightd)
        self.unop_test(spamd, 2, 'len(a)', '__len__')
        self.unop_test(spamd, repr(straightd), 'repr(a)', '__repr__')
        self.set2op_test(spamdict({(1): 2, (3): 4}), 2, 3, spamdict({(1): 2,
            (2): 3, (3): 4}), 'a[b]=c', '__setitem__')


        class C(spam.spamdict):

            def foo(self):
                py_ass_parser2160 = 1
                Sensor(2160)
                return py_ass_parser2160
        a = C()
        self.assertEqual(a.items(), [])
        self.assertEqual(a.foo(), 1)
        a['foo'] = 'bar'
        self.assertEqual(a.items(), [('foo', 'bar')])
        self.assertEqual(a.getstate(), 0)
        a.setstate(100)
        self.assertEqual(a.getstate(), 100)
        Sensor(2161)


class ClassPropertiesAndMethods(unittest.TestCase):

    def test_python_dicts(self):
        Sensor(2162)
        self.assertTrue(issubclass(dict, dict))
        self.assertIsInstance({}, dict)
        d = dict()
        self.assertEqual(d, {})
        self.assertTrue(d.__class__ is dict)
        self.assertIsInstance(d, dict)


        class C(dict):
            state = -1

            def __init__(self_local, *a, **kw):
                Sensor(2163)
                if a:
                    Sensor(2164)
                    self.assertEqual(len(a), 1)
                    self_local.state = a[0]
                Sensor(2165)
                if kw:
                    Sensor(2166)
                    for k, v in kw.items():
                        Sensor(2167)
                        self_local[v] = k
                Sensor(2168)

            def __getitem__(self, key):
                py_ass_parser2169 = self.get(key, 0)
                Sensor(2169)
                return py_ass_parser2169

            def __setitem__(self_local, key, value):
                Sensor(2170)
                self.assertIsInstance(key, type(0))
                dict.__setitem__(self_local, key, value)
                Sensor(2171)

            def setstate(self, state):
                Sensor(2172)
                self.state = state
                Sensor(2173)

            def getstate(self):
                py_ass_parser2174 = self.state
                Sensor(2174)
                return py_ass_parser2174
        self.assertTrue(issubclass(C, dict))
        a1 = C(12)
        self.assertEqual(a1.state, 12)
        a2 = C(foo=1, bar=2)
        self.assertEqual(a2[1] == 'foo' and a2[2], 'bar')
        a = C()
        self.assertEqual(a.state, -1)
        self.assertEqual(a.getstate(), -1)
        a.setstate(0)
        self.assertEqual(a.state, 0)
        self.assertEqual(a.getstate(), 0)
        a.setstate(10)
        self.assertEqual(a.state, 10)
        self.assertEqual(a.getstate(), 10)
        self.assertEqual(a[42], 0)
        a[42] = 24
        self.assertEqual(a[42], 24)
        N = 50
        Sensor(2175)
        for i in range(N):
            Sensor(2176)
            a[i] = C()
            Sensor(2177)
            for j in range(N):
                Sensor(2178)
                a[i][j] = i * j
        Sensor(2179)
        for i in range(N):
            Sensor(2180)
            for j in range(N):
                Sensor(2181)
                self.assertEqual(a[i][j], i * j)
        Sensor(2182)

    def test_python_lists(self):


        class C(list):

            def __getitem__(self, i):
                py_ass_parser2183 = list.__getitem__(self, i) + 100
                Sensor(2183)
                return py_ass_parser2183

            def __getslice__(self, i, j):
                py_ass_parser2184 = i, j
                Sensor(2184)
                return py_ass_parser2184
        Sensor(2185)
        a = C()
        a.extend([0, 1, 2])
        self.assertEqual(a[0], 100)
        self.assertEqual(a[1], 101)
        self.assertEqual(a[2], 102)
        self.assertEqual(a[100:200], (100, 200))
        Sensor(2186)

    def test_metaclass(self):


        class C:
            __metaclass__ = type

            def __init__(self):
                Sensor(2187)
                self.__state = 0
                Sensor(2188)

            def getstate(self):
                py_ass_parser2189 = self.__state
                Sensor(2189)
                return py_ass_parser2189

            def setstate(self, state):
                Sensor(2190)
                self.__state = state
                Sensor(2191)
        Sensor(2192)
        a = C()
        self.assertEqual(a.getstate(), 0)
        a.setstate(10)
        self.assertEqual(a.getstate(), 10)


        class D:


            class __metaclass__(type):

                def myself(cls):
                    py_ass_parser2193 = cls
                    Sensor(2193)
                    return py_ass_parser2193
        self.assertEqual(D.myself(), D)
        d = D()
        self.assertEqual(d.__class__, D)


        class M1(type):

            def __new__(cls, name, bases, dict):
                Sensor(2194)
                dict['__spam__'] = 1
                py_ass_parser2195 = type.__new__(cls, name, bases, dict)
                Sensor(2195)
                return py_ass_parser2195


        class C:
            __metaclass__ = M1
        self.assertEqual(C.__spam__, 1)
        c = C()
        self.assertEqual(c.__spam__, 1)


        class _instance(object):
            pass


        class M2(object):

            @staticmethod
            def __new__(cls, name, bases, dict):
                Sensor(2196)
                self = object.__new__(cls)
                self.name = name
                self.bases = bases
                self.dict = dict
                py_ass_parser2197 = self
                Sensor(2197)
                return py_ass_parser2197

            def __call__(self):
                Sensor(2198)
                it = _instance()
                Sensor(2199)
                for key in self.dict:
                    Sensor(2200)
                    if key.startswith('__'):
                        Sensor(2201)
                        continue
                    Sensor(2202)
                    setattr(it, key, self.dict[key].__get__(it, self))
                py_ass_parser2203 = it
                Sensor(2203)
                return py_ass_parser2203


        class C:
            __metaclass__ = M2

            def spam(self):
                py_ass_parser2204 = 42
                Sensor(2204)
                return py_ass_parser2204
        self.assertEqual(C.name, 'C')
        self.assertEqual(C.bases, ())
        self.assertIn('spam', C.dict)
        c = C()
        self.assertEqual(c.spam(), 42)


        class autosuper(type):

            def __new__(metaclass, name, bases, dict):
                Sensor(2205)
                cls = super(autosuper, metaclass).__new__(metaclass, name,
                    bases, dict)
                Sensor(2206)
                while name[:1] == '_':
                    Sensor(2207)
                    name = name[1:]
                Sensor(2208)
                if name:
                    Sensor(2209)
                    name = '_%s__super' % name
                else:
                    Sensor(2210)
                    name = '__super'
                Sensor(2211)
                setattr(cls, name, super(cls))
                py_ass_parser2212 = cls
                Sensor(2212)
                return py_ass_parser2212


        class A:
            __metaclass__ = autosuper

            def meth(self):
                py_ass_parser2213 = 'A'
                Sensor(2213)
                return py_ass_parser2213


        class B(A):

            def meth(self):
                py_ass_parser2214 = 'B' + self.__super.meth()
                Sensor(2214)
                return py_ass_parser2214


        class C(A):

            def meth(self):
                py_ass_parser2215 = 'C' + self.__super.meth()
                Sensor(2215)
                return py_ass_parser2215


        class D(C, B):

            def meth(self):
                py_ass_parser2216 = 'D' + self.__super.meth()
                Sensor(2216)
                return py_ass_parser2216
        self.assertEqual(D().meth(), 'DCBA')


        class E(B, C):

            def meth(self):
                py_ass_parser2217 = 'E' + self.__super.meth()
                Sensor(2217)
                return py_ass_parser2217
        self.assertEqual(E().meth(), 'EBCA')


        class autoproperty(type):

            def __new__(metaclass, name, bases, dict):
                Sensor(2218)
                hits = {}
                Sensor(2219)
                for key, val in dict.iteritems():
                    Sensor(2220)
                    if key.startswith('_get_'):
                        Sensor(2221)
                        key = key[5:]
                        get, set = hits.get(key, (None, None))
                        get = val
                        hits[key] = get, set
                    else:
                        Sensor(2222)
                        if key.startswith('_set_'):
                            Sensor(2223)
                            key = key[5:]
                            get, set = hits.get(key, (None, None))
                            set = val
                            hits[key] = get, set
                Sensor(2224)
                for key, (get, set) in hits.iteritems():
                    Sensor(2225)
                    dict[key] = property(get, set)
                py_ass_parser2226 = super(autoproperty, metaclass).__new__(
                    metaclass, name, bases, dict)
                Sensor(2226)
                return py_ass_parser2226


        class A:
            __metaclass__ = autoproperty

            def _get_x(self):
                py_ass_parser2227 = -self.__x
                Sensor(2227)
                return py_ass_parser2227

            def _set_x(self, x):
                Sensor(2228)
                self.__x = -x
                Sensor(2229)
        a = A()
        self.assertTrue(not hasattr(a, 'x'))
        a.x = 12
        self.assertEqual(a.x, 12)
        self.assertEqual(a._A__x, -12)


        class multimetaclass(autoproperty, autosuper):
            pass


        class A:
            __metaclass__ = multimetaclass

            def _get_x(self):
                py_ass_parser2230 = 'A'
                Sensor(2230)
                return py_ass_parser2230


        class B(A):

            def _get_x(self):
                py_ass_parser2231 = 'B' + self.__super._get_x()
                Sensor(2231)
                return py_ass_parser2231


        class C(A):

            def _get_x(self):
                py_ass_parser2232 = 'C' + self.__super._get_x()
                Sensor(2232)
                return py_ass_parser2232


        class D(C, B):

            def _get_x(self):
                py_ass_parser2233 = 'D' + self.__super._get_x()
                Sensor(2233)
                return py_ass_parser2233
        self.assertEqual(D().x, 'DCBA')


        class T(type):
            counter = 0

            def __init__(self, *args):
                Sensor(2234)
                T.counter += 1
                Sensor(2235)


        class C:
            __metaclass__ = T
        self.assertEqual(T.counter, 1)
        a = C()
        self.assertEqual(type(a), C)
        self.assertEqual(T.counter, 1)


        class C(object):
            pass
        c = C()
        Sensor(2236)
        try:
            Sensor(2237)
            c()
        except TypeError:
            Sensor(2238)
            pass
        else:
            Sensor(2239)
            self.fail('calling object w/o call method should raise TypeError')


        class A(type):

            def __new__(*args, **kwargs):
                py_ass_parser2240 = type.__new__(*args, **kwargs)
                Sensor(2240)
                return py_ass_parser2240


        class B(object):
            pass


        class C(object):
            __metaclass__ = A


        class D(B, C):
            pass
        Sensor(2241)

    def test_module_subclasses(self):
        Sensor(2242)
        log = []
        MT = type(sys)


        class MM(MT):

            def __init__(self, name):
                Sensor(2243)
                MT.__init__(self, name)
                Sensor(2244)

            def __getattribute__(self, name):
                Sensor(2245)
                log.append(('getattr', name))
                py_ass_parser2246 = MT.__getattribute__(self, name)
                Sensor(2246)
                return py_ass_parser2246

            def __setattr__(self, name, value):
                Sensor(2247)
                log.append(('setattr', name, value))
                MT.__setattr__(self, name, value)
                Sensor(2248)

            def __delattr__(self, name):
                Sensor(2249)
                log.append(('delattr', name))
                MT.__delattr__(self, name)
                Sensor(2250)
        a = MM('a')
        a.foo = 12
        x = a.foo
        del a.foo
        self.assertEqual(log, [('setattr', 'foo', 12), ('getattr', 'foo'),
            ('delattr', 'foo')])
        Sensor(2251)
        try:
            Sensor(2252)


            class Module(types.ModuleType, str):
                pass
        except TypeError:
            Sensor(2253)
            pass
        else:
            Sensor(2254)
            self.fail(
                'inheriting from ModuleType and str at the same time should fail'
                )
        Sensor(2255)

    def test_multiple_inheritence(self):


        class C(object):

            def __init__(self):
                Sensor(2256)
                self.__state = 0
                Sensor(2257)

            def getstate(self):
                py_ass_parser2258 = self.__state
                Sensor(2258)
                return py_ass_parser2258

            def setstate(self, state):
                Sensor(2259)
                self.__state = state
                Sensor(2260)
        Sensor(2261)
        a = C()
        self.assertEqual(a.getstate(), 0)
        a.setstate(10)
        self.assertEqual(a.getstate(), 10)


        class D(dict, C):

            def __init__(self):
                Sensor(2262)
                type({}).__init__(self)
                C.__init__(self)
                Sensor(2263)
        d = D()
        self.assertEqual(d.keys(), [])
        d['hello'] = 'world'
        self.assertEqual(d.items(), [('hello', 'world')])
        self.assertEqual(d['hello'], 'world')
        self.assertEqual(d.getstate(), 0)
        d.setstate(10)
        self.assertEqual(d.getstate(), 10)
        self.assertEqual(D.__mro__, (D, dict, C, object))


        class Node(object):

            def __int__(self):
                py_ass_parser2264 = int(self.foo())
                Sensor(2264)
                return py_ass_parser2264

            def foo(self):
                py_ass_parser2265 = '23'
                Sensor(2265)
                return py_ass_parser2265


        class Frag(Node, list):

            def foo(self):
                py_ass_parser2266 = '42'
                Sensor(2266)
                return py_ass_parser2266
        self.assertEqual(Node().__int__(), 23)
        self.assertEqual(int(Node()), 23)
        self.assertEqual(Frag().__int__(), 42)
        self.assertEqual(int(Frag()), 42)


        class A:
            x = 1


        class B(A):
            pass


        class C(A):
            x = 2


        class D(B, C):
            pass
        self.assertEqual(D.x, 1)


        class E(D, object):
            pass
        self.assertEqual(E.__mro__, (E, D, B, A, C, object))
        self.assertEqual(E.x, 1)


        class F(B, C, object):
            pass
        self.assertEqual(F.__mro__, (F, B, C, A, object))
        self.assertEqual(F.x, 2)


        class C:

            def cmethod(self):
                py_ass_parser2267 = 'C a'
                Sensor(2267)
                return py_ass_parser2267

            def all_method(self):
                py_ass_parser2268 = 'C b'
                Sensor(2268)
                return py_ass_parser2268


        class M1(C, object):

            def m1method(self):
                py_ass_parser2269 = 'M1 a'
                Sensor(2269)
                return py_ass_parser2269

            def all_method(self):
                py_ass_parser2270 = 'M1 b'
                Sensor(2270)
                return py_ass_parser2270
        self.assertEqual(M1.__mro__, (M1, C, object))
        m = M1()
        self.assertEqual(m.cmethod(), 'C a')
        self.assertEqual(m.m1method(), 'M1 a')
        self.assertEqual(m.all_method(), 'M1 b')


        class D(C):

            def dmethod(self):
                py_ass_parser2271 = 'D a'
                Sensor(2271)
                return py_ass_parser2271

            def all_method(self):
                py_ass_parser2272 = 'D b'
                Sensor(2272)
                return py_ass_parser2272


        class M2(D, object):

            def m2method(self):
                py_ass_parser2273 = 'M2 a'
                Sensor(2273)
                return py_ass_parser2273

            def all_method(self):
                py_ass_parser2274 = 'M2 b'
                Sensor(2274)
                return py_ass_parser2274
        self.assertEqual(M2.__mro__, (M2, D, C, object))
        m = M2()
        self.assertEqual(m.cmethod(), 'C a')
        self.assertEqual(m.dmethod(), 'D a')
        self.assertEqual(m.m2method(), 'M2 a')
        self.assertEqual(m.all_method(), 'M2 b')


        class M3(M1, M2, object):

            def m3method(self):
                py_ass_parser2275 = 'M3 a'
                Sensor(2275)
                return py_ass_parser2275

            def all_method(self):
                py_ass_parser2276 = 'M3 b'
                Sensor(2276)
                return py_ass_parser2276
        self.assertEqual(M3.__mro__, (M3, M1, M2, D, C, object))
        m = M3()
        self.assertEqual(m.cmethod(), 'C a')
        self.assertEqual(m.dmethod(), 'D a')
        self.assertEqual(m.m1method(), 'M1 a')
        self.assertEqual(m.m2method(), 'M2 a')
        self.assertEqual(m.m3method(), 'M3 a')
        self.assertEqual(m.all_method(), 'M3 b')


        class Classic:
            pass
        Sensor(2277)
        try:
            Sensor(2278)


            class New(Classic):
                __metaclass__ = type
        except TypeError:
            Sensor(2279)
            pass
        else:
            Sensor(2280)
            self.fail("new class with only classic bases - shouldn't be")
        Sensor(2281)

    def test_diamond_inheritence(self):


        class A(object):

            def spam(self):
                py_ass_parser2282 = 'A'
                Sensor(2282)
                return py_ass_parser2282
        Sensor(2283)
        self.assertEqual(A().spam(), 'A')


        class B(A):

            def boo(self):
                py_ass_parser2284 = 'B'
                Sensor(2284)
                return py_ass_parser2284

            def spam(self):
                py_ass_parser2285 = 'B'
                Sensor(2285)
                return py_ass_parser2285
        self.assertEqual(B().spam(), 'B')
        self.assertEqual(B().boo(), 'B')


        class C(A):

            def boo(self):
                py_ass_parser2286 = 'C'
                Sensor(2286)
                return py_ass_parser2286
        self.assertEqual(C().spam(), 'A')
        self.assertEqual(C().boo(), 'C')


        class D(B, C):
            pass
        self.assertEqual(D().spam(), 'B')
        self.assertEqual(D().boo(), 'B')
        self.assertEqual(D.__mro__, (D, B, C, A, object))


        class E(C, B):
            pass
        self.assertEqual(E().spam(), 'B')
        self.assertEqual(E().boo(), 'C')
        self.assertEqual(E.__mro__, (E, C, B, A, object))
        Sensor(2287)
        try:
            Sensor(2288)


            class F(D, E):
                pass
        except TypeError:
            Sensor(2289)
            pass
        else:
            Sensor(2290)
            self.fail('expected MRO order disagreement (F)')
        Sensor(2291)
        try:
            Sensor(2292)


            class G(E, D):
                pass
        except TypeError:
            Sensor(2293)
            pass
        else:
            Sensor(2294)
            self.fail('expected MRO order disagreement (G)')
        Sensor(2295)

    def test_ex5_from_c3_switch(self):


        class A(object):
            pass


        class B(object):
            pass


        class C(object):
            pass


        class X(A):
            pass


        class Y(A):
            pass


        class Z(X, B, Y, C):
            pass
        Sensor(2296)
        self.assertEqual(Z.__mro__, (Z, X, B, Y, A, C, object))
        Sensor(2297)

    def test_monotonicity(self):


        class Boat(object):
            pass


        class DayBoat(Boat):
            pass


        class WheelBoat(Boat):
            pass


        class EngineLess(DayBoat):
            pass


        class SmallMultihull(DayBoat):
            pass


        class PedalWheelBoat(EngineLess, WheelBoat):
            pass


        class SmallCatamaran(SmallMultihull):
            pass


        class Pedalo(PedalWheelBoat, SmallCatamaran):
            pass
        Sensor(2298)
        self.assertEqual(PedalWheelBoat.__mro__, (PedalWheelBoat,
            EngineLess, DayBoat, WheelBoat, Boat, object))
        self.assertEqual(SmallCatamaran.__mro__, (SmallCatamaran,
            SmallMultihull, DayBoat, Boat, object))
        self.assertEqual(Pedalo.__mro__, (Pedalo, PedalWheelBoat,
            EngineLess, SmallCatamaran, SmallMultihull, DayBoat, WheelBoat,
            Boat, object))
        Sensor(2299)

    def test_consistency_with_epg(self):


        class Pane(object):
            pass


        class ScrollingMixin(object):
            pass


        class EditingMixin(object):
            pass


        class ScrollablePane(Pane, ScrollingMixin):
            pass


        class EditablePane(Pane, EditingMixin):
            pass


        class EditableScrollablePane(ScrollablePane, EditablePane):
            pass
        Sensor(2300)
        self.assertEqual(EditableScrollablePane.__mro__, (
            EditableScrollablePane, ScrollablePane, EditablePane, Pane,
            ScrollingMixin, EditingMixin, object))
        Sensor(2301)

    def test_mro_disagreement(self):
        Sensor(2302)
        mro_err_msg = (
            'Cannot create a consistent method resolution\norder (MRO) for bases '
            )

        def raises(exc, expected, callable, *args):
            Sensor(2303)
            try:
                Sensor(2304)
                callable(*args)
            except exc as msg:
                Sensor(2305)
                if test_support.check_impl_detail():
                    Sensor(2306)
                    if not str(msg).startswith(expected):
                        Sensor(2307)
                        self.fail('Message %r, expected %r' % (str(msg),
                            expected))
            else:
                Sensor(2308)
                self.fail('Expected %s' % exc)
            Sensor(2309)


        class A(object):
            pass


        class B(A):
            pass


        class C(object):
            pass
        raises(TypeError, 'duplicate base class A', type, 'X', (A, A), {})
        raises(TypeError, mro_err_msg, type, 'X', (A, B), {})
        raises(TypeError, mro_err_msg, type, 'X', (A, C, B), {})


        class GridLayout(object):
            pass


        class HorizontalGrid(GridLayout):
            pass


        class VerticalGrid(GridLayout):
            pass


        class HVGrid(HorizontalGrid, VerticalGrid):
            pass


        class VHGrid(VerticalGrid, HorizontalGrid):
            pass
        raises(TypeError, mro_err_msg, type, 'ConfusedGrid', (HVGrid,
            VHGrid), {})
        Sensor(2310)

    def test_object_class(self):
        Sensor(2311)
        a = object()
        self.assertEqual(a.__class__, object)
        self.assertEqual(type(a), object)
        b = object()
        self.assertNotEqual(a, b)
        self.assertFalse(hasattr(a, 'foo'))
        Sensor(2312)
        try:
            Sensor(2313)
            a.foo = 12
        except (AttributeError, TypeError):
            Sensor(2314)
            pass
        else:
            Sensor(2315)
            self.fail('object() should not allow setting a foo attribute')
        Sensor(2316)
        self.assertFalse(hasattr(object(), '__dict__'))


        class Cdict(object):
            pass
        x = Cdict()
        self.assertEqual(x.__dict__, {})
        x.foo = 1
        self.assertEqual(x.foo, 1)
        self.assertEqual(x.__dict__, {'foo': 1})
        Sensor(2317)

    def test_slots(self):


        class C0(object):
            __slots__ = []
        Sensor(2318)
        x = C0()
        self.assertFalse(hasattr(x, '__dict__'))
        self.assertFalse(hasattr(x, 'foo'))


        class C1(object):
            __slots__ = ['a']
        x = C1()
        self.assertFalse(hasattr(x, '__dict__'))
        self.assertFalse(hasattr(x, 'a'))
        x.a = 1
        self.assertEqual(x.a, 1)
        x.a = None
        self.assertEqual(x.a, None)
        del x.a
        self.assertFalse(hasattr(x, 'a'))


        class C3(object):
            __slots__ = ['a', 'b', 'c']
        x = C3()
        self.assertFalse(hasattr(x, '__dict__'))
        self.assertFalse(hasattr(x, 'a'))
        self.assertFalse(hasattr(x, 'b'))
        self.assertFalse(hasattr(x, 'c'))
        x.a = 1
        x.b = 2
        x.c = 3
        self.assertEqual(x.a, 1)
        self.assertEqual(x.b, 2)
        self.assertEqual(x.c, 3)


        class C4(object):
            """Validate name mangling"""
            __slots__ = ['__a']

            def __init__(self, value):
                Sensor(2319)
                self.__a = value
                Sensor(2320)

            def get(self):
                py_ass_parser2321 = self.__a
                Sensor(2321)
                return py_ass_parser2321
        x = C4(5)
        self.assertFalse(hasattr(x, '__dict__'))
        self.assertFalse(hasattr(x, '__a'))
        self.assertEqual(x.get(), 5)
        Sensor(2322)
        try:
            Sensor(2323)
            x.__a = 6
        except AttributeError:
            Sensor(2324)
            pass
        else:
            Sensor(2325)
            self.fail('Double underscored names not mangled')
        Sensor(2326)
        try:
            Sensor(2327)


            class C(object):
                __slots__ = [None]
        except TypeError:
            Sensor(2328)
            pass
        else:
            Sensor(2329)
            self.fail('[None] slots not caught')
        Sensor(2330)
        try:
            Sensor(2331)


            class C(object):
                __slots__ = ['foo bar']
        except TypeError:
            Sensor(2332)
            pass
        else:
            Sensor(2333)
            self.fail("['foo bar'] slots not caught")
        Sensor(2334)
        try:
            Sensor(2335)


            class C(object):
                __slots__ = ['foo\x00bar']
        except TypeError:
            Sensor(2336)
            pass
        else:
            Sensor(2337)
            self.fail("['foo\\0bar'] slots not caught")
        Sensor(2338)
        try:
            Sensor(2339)


            class C(object):
                __slots__ = ['1']
        except TypeError:
            Sensor(2340)
            pass
        else:
            Sensor(2341)
            self.fail("['1'] slots not caught")
        Sensor(2342)
        try:
            Sensor(2343)


            class C(object):
                __slots__ = ['']
        except TypeError:
            Sensor(2344)
            pass
        else:
            Sensor(2345)
            self.fail("[''] slots not caught")


        class C(object):
            __slots__ = ['a', 'a_b', '_a', 'A0123456789Z']


        class C(object):
            __slots__ = 'abc'
        Sensor(2346)
        c = C()
        c.abc = 5
        self.assertEqual(c.abc, 5)
        Sensor(2347)
        try:
            Sensor(2348)
            unicode
        except NameError:
            Sensor(2349)
            pass
        else:


            class C(object):
                __slots__ = unicode('abc')
            Sensor(2350)
            c = C()
            c.abc = 5
            self.assertEqual(c.abc, 5)
            slots = unicode('foo'), unicode('bar')


            class C(object):
                __slots__ = slots
            x = C()
            x.foo = 5
            self.assertEqual(x.foo, 5)
            self.assertEqual(type(slots[0]), unicode)
            Sensor(2351)
            try:
                Sensor(2352)


                class C(object):
                    __slots__ = [unichr(128)]
            except (TypeError, UnicodeEncodeError):
                Sensor(2353)
                pass
            else:
                Sensor(2354)
                self.fail('[unichr(128)] slots not caught')


        class Counted(object):
            counter = 0

            def __init__(self):
                Sensor(2355)
                Counted.counter += 1
                Sensor(2356)

            def __del__(self):
                Sensor(2357)
                Counted.counter -= 1
                Sensor(2358)


        class C(object):
            __slots__ = ['a', 'b', 'c']
        Sensor(2359)
        x = C()
        x.a = Counted()
        x.b = Counted()
        x.c = Counted()
        self.assertEqual(Counted.counter, 3)
        del x
        test_support.gc_collect()
        self.assertEqual(Counted.counter, 0)


        class D(C):
            pass
        x = D()
        x.a = Counted()
        x.z = Counted()
        self.assertEqual(Counted.counter, 2)
        del x
        test_support.gc_collect()
        self.assertEqual(Counted.counter, 0)


        class E(D):
            __slots__ = ['e']
        x = E()
        x.a = Counted()
        x.z = Counted()
        x.e = Counted()
        self.assertEqual(Counted.counter, 3)
        del x
        test_support.gc_collect()
        self.assertEqual(Counted.counter, 0)


        class F(object):
            __slots__ = ['a', 'b']
        s = F()
        s.a = [Counted(), s]
        self.assertEqual(Counted.counter, 1)
        s = None
        test_support.gc_collect()
        self.assertEqual(Counted.counter, 0)
        Sensor(2360)
        if hasattr(gc, 'get_objects'):


            class G(object):

                def __cmp__(self, other):
                    py_ass_parser2361 = 0
                    Sensor(2361)
                    return py_ass_parser2361
                __hash__ = None
            Sensor(2362)
            g = G()
            orig_objects = len(gc.get_objects())
            Sensor(2363)
            for i in xrange(10):
                Sensor(2364)
                g == g
            Sensor(2365)
            new_objects = len(gc.get_objects())
            self.assertEqual(orig_objects, new_objects)


        class H(object):
            __slots__ = ['a', 'b']

            def __init__(self):
                Sensor(2366)
                self.a = 1
                self.b = 2
                Sensor(2367)

            def __del__(self_):
                Sensor(2368)
                self.assertEqual(self_.a, 1)
                self.assertEqual(self_.b, 2)
                Sensor(2369)
        with test_support.captured_output('stderr') as s:
            Sensor(2370)
            h = H()
            del h
        self.assertEqual(s.getvalue(), '')


        class X(object):
            __slots__ = 'a'
        with self.assertRaises(AttributeError):
            del X().a
        Sensor(2371)

    def test_slots_special(self):


        class D(object):
            __slots__ = ['__dict__']
        Sensor(2372)
        a = D()
        self.assertTrue(hasattr(a, '__dict__'))
        self.assertFalse(hasattr(a, '__weakref__'))
        a.foo = 42
        self.assertEqual(a.__dict__, {'foo': 42})


        class W(object):
            __slots__ = ['__weakref__']
        a = W()
        self.assertTrue(hasattr(a, '__weakref__'))
        self.assertFalse(hasattr(a, '__dict__'))
        Sensor(2373)
        try:
            Sensor(2374)
            a.foo = 42
        except AttributeError:
            Sensor(2375)
            pass
        else:
            Sensor(2376)
            self.fail("shouldn't be allowed to set a.foo")


        class C1(W, D):
            __slots__ = []
        Sensor(2377)
        a = C1()
        self.assertTrue(hasattr(a, '__dict__'))
        self.assertTrue(hasattr(a, '__weakref__'))
        a.foo = 42
        self.assertEqual(a.__dict__, {'foo': 42})


        class C2(D, W):
            __slots__ = []
        a = C2()
        self.assertTrue(hasattr(a, '__dict__'))
        self.assertTrue(hasattr(a, '__weakref__'))
        a.foo = 42
        self.assertEqual(a.__dict__, {'foo': 42})
        Sensor(2378)

    def test_slots_descriptor(self):
        import abc


        class MyABC:
            __metaclass__ = abc.ABCMeta
            __slots__ = 'a'


        class Unrelated(object):
            pass
        Sensor(2379)
        MyABC.register(Unrelated)
        u = Unrelated()
        self.assertIsInstance(u, MyABC)
        self.assertRaises(TypeError, MyABC.a.__set__, u, 3)
        Sensor(2380)

    def test_metaclass_cmp(self):


        class M(type):

            def __cmp__(self, other):
                py_ass_parser2381 = -1
                Sensor(2381)
                return py_ass_parser2381


        class X(object):
            __metaclass__ = M
        Sensor(2382)
        self.assertTrue(X < M)
        Sensor(2383)

    def test_dynamics(self):


        class D(object):
            pass


        class E(D):
            pass


        class F(D):
            pass
        Sensor(2384)
        D.foo = 1
        self.assertEqual(D.foo, 1)
        self.assertEqual(E.foo, 1)
        self.assertEqual(F.foo, 1)


        class C(object):
            pass
        a = C()
        self.assertFalse(hasattr(a, 'foobar'))
        C.foobar = 2
        self.assertEqual(a.foobar, 2)
        C.method = lambda self: 42
        self.assertEqual(a.method(), 42)
        C.__repr__ = lambda self: 'C()'
        self.assertEqual(repr(a), 'C()')
        C.__int__ = lambda self: 100
        self.assertEqual(int(a), 100)
        self.assertEqual(a.foobar, 2)
        self.assertFalse(hasattr(a, 'spam'))

        def mygetattr(self, name):
            Sensor(2385)
            if name == 'spam':
                py_ass_parser2386 = 'spam'
                Sensor(2386)
                return py_ass_parser2386
            Sensor(2387)
            raise AttributeError
        C.__getattr__ = mygetattr
        self.assertEqual(a.spam, 'spam')
        a.new = 12
        self.assertEqual(a.new, 12)

        def mysetattr(self, name, value):
            Sensor(2388)
            if name == 'spam':
                Sensor(2389)
                raise AttributeError
            py_ass_parser2390 = object.__setattr__(self, name, value)
            Sensor(2390)
            return py_ass_parser2390
        C.__setattr__ = mysetattr
        Sensor(2391)
        try:
            Sensor(2392)
            a.spam = 'not spam'
        except AttributeError:
            Sensor(2393)
            pass
        else:
            Sensor(2394)
            self.fail('expected AttributeError')
        Sensor(2395)
        self.assertEqual(a.spam, 'spam')


        class D(C):
            pass
        d = D()
        d.foo = 1
        self.assertEqual(d.foo, 1)


        class I(int):
            pass
        self.assertEqual('a' * I(2), 'aa')
        self.assertEqual(I(2) * 'a', 'aa')
        self.assertEqual(2 * I(3), 6)
        self.assertEqual(I(3) * 2, 6)
        self.assertEqual(I(3) * I(2), 6)


        class L(long):
            pass
        self.assertEqual('a' * L(2L), 'aa')
        self.assertEqual(L(2L) * 'a', 'aa')
        self.assertEqual(2 * L(3), 6)
        self.assertEqual(L(3) * 2, 6)
        self.assertEqual(L(3) * L(2), 6)


        class dynamicmetaclass(type):
            pass


        class someclass:
            __metaclass__ = dynamicmetaclass
        self.assertNotEqual(someclass, object)
        Sensor(2396)

    def test_errors(self):
        Sensor(2397)
        try:
            Sensor(2398)


            class C(list, dict):
                pass
        except TypeError:
            Sensor(2399)
            pass
        else:
            Sensor(2400)
            self.fail('inheritance from both list and dict should be illegal')
        Sensor(2401)
        try:
            Sensor(2402)


            class C(object, None):
                pass
        except TypeError:
            Sensor(2403)
            pass
        else:
            Sensor(2404)
            self.fail('inheritance from non-type should be illegal')


        class Classic:
            pass
        Sensor(2405)
        try:
            Sensor(2406)


            class C(type(len)):
                pass
        except TypeError:
            Sensor(2407)
            pass
        else:
            Sensor(2408)
            self.fail('inheritance from CFunction should be illegal')
        Sensor(2409)
        try:
            Sensor(2410)


            class C(object):
                __slots__ = 1
        except TypeError:
            Sensor(2411)
            pass
        else:
            Sensor(2412)
            self.fail('__slots__ = 1 should be illegal')
        Sensor(2413)
        try:
            Sensor(2414)


            class C(object):
                __slots__ = [1]
        except TypeError:
            Sensor(2415)
            pass
        else:
            Sensor(2416)
            self.fail('__slots__ = [1] should be illegal')


        class M1(type):
            pass


        class M2(type):
            pass


        class A1(object):
            __metaclass__ = M1


        class A2(object):
            __metaclass__ = M2
        Sensor(2417)
        try:
            Sensor(2418)


            class B(A1, A2):
                pass
        except TypeError:
            Sensor(2419)
            pass
        else:
            Sensor(2420)
            self.fail('finding the most derived metaclass should have failed')
        Sensor(2421)

    def test_classmethods(self):


        class C(object):

            def foo(*a):
                py_ass_parser2422 = a
                Sensor(2422)
                return py_ass_parser2422
            goo = classmethod(foo)
        Sensor(2423)
        c = C()
        self.assertEqual(C.goo(1), (C, 1))
        self.assertEqual(c.goo(1), (C, 1))
        self.assertEqual(c.foo(1), (c, 1))


        class D(C):
            pass
        d = D()
        self.assertEqual(D.goo(1), (D, 1))
        self.assertEqual(d.goo(1), (D, 1))
        self.assertEqual(d.foo(1), (d, 1))
        self.assertEqual(D.foo(d, 1), (d, 1))

        def f(cls, arg):
            py_ass_parser2424 = cls, arg
            Sensor(2424)
            return py_ass_parser2424
        ff = classmethod(f)
        self.assertEqual(ff.__get__(0, int)(42), (int, 42))
        self.assertEqual(ff.__get__(0)(42), (int, 42))
        self.assertEqual(C.goo.im_self, C)
        self.assertEqual(D.goo.im_self, D)
        self.assertEqual(super(D, D).goo.im_self, D)
        self.assertEqual(super(D, d).goo.im_self, D)
        self.assertEqual(super(D, D).goo(), (D,))
        self.assertEqual(super(D, d).goo(), (D,))
        meth = classmethod(1).__get__(1)
        self.assertRaises(TypeError, meth)
        Sensor(2425)
        try:
            Sensor(2426)
            classmethod(f, kw=1)
        except TypeError:
            Sensor(2427)
            pass
        else:
            Sensor(2428)
            self.fail("classmethod shouldn't accept keyword args")
        Sensor(2429)

    @test_support.impl_detail("the module 'xxsubtype' is internal")
    def test_classmethods_in_c(self):
        import xxsubtype as spam
        Sensor(2430)
        a = 1, 2, 3
        d = {'abc': 123}
        x, a1, d1 = spam.spamlist.classmeth(*a, **d)
        self.assertEqual(x, spam.spamlist)
        self.assertEqual(a, a1)
        self.assertEqual(d, d1)
        x, a1, d1 = spam.spamlist().classmeth(*a, **d)
        self.assertEqual(x, spam.spamlist)
        self.assertEqual(a, a1)
        self.assertEqual(d, d1)
        spam_cm = spam.spamlist.__dict__['classmeth']
        x2, a2, d2 = spam_cm(spam.spamlist, *a, **d)
        self.assertEqual(x2, spam.spamlist)
        self.assertEqual(a2, a1)
        self.assertEqual(d2, d1)


        class SubSpam(spam.spamlist):
            pass
        x2, a2, d2 = spam_cm(SubSpam, *a, **d)
        self.assertEqual(x2, SubSpam)
        self.assertEqual(a2, a1)
        self.assertEqual(d2, d1)
        with self.assertRaises(TypeError):
            spam_cm()
        with self.assertRaises(TypeError):
            spam_cm(spam.spamlist())
        with self.assertRaises(TypeError):
            spam_cm(list)
        Sensor(2431)

    def test_staticmethods(self):


        class C(object):

            def foo(*a):
                py_ass_parser2432 = a
                Sensor(2432)
                return py_ass_parser2432
            goo = staticmethod(foo)
        Sensor(2433)
        c = C()
        self.assertEqual(C.goo(1), (1,))
        self.assertEqual(c.goo(1), (1,))
        self.assertEqual(c.foo(1), (c, 1))


        class D(C):
            pass
        d = D()
        self.assertEqual(D.goo(1), (1,))
        self.assertEqual(d.goo(1), (1,))
        self.assertEqual(d.foo(1), (d, 1))
        self.assertEqual(D.foo(d, 1), (d, 1))
        Sensor(2434)

    @test_support.impl_detail("the module 'xxsubtype' is internal")
    def test_staticmethods_in_c(self):
        import xxsubtype as spam
        Sensor(2435)
        a = 1, 2, 3
        d = {'abc': 123}
        x, a1, d1 = spam.spamlist.staticmeth(*a, **d)
        self.assertEqual(x, None)
        self.assertEqual(a, a1)
        self.assertEqual(d, d1)
        x, a1, d2 = spam.spamlist().staticmeth(*a, **d)
        self.assertEqual(x, None)
        self.assertEqual(a, a1)
        self.assertEqual(d, d1)
        Sensor(2436)

    def test_classic(self):


        class C:

            def foo(*a):
                py_ass_parser2437 = a
                Sensor(2437)
                return py_ass_parser2437
            goo = classmethod(foo)
        Sensor(2438)
        c = C()
        self.assertEqual(C.goo(1), (C, 1))
        self.assertEqual(c.goo(1), (C, 1))
        self.assertEqual(c.foo(1), (c, 1))


        class D(C):
            pass
        d = D()
        self.assertEqual(D.goo(1), (D, 1))
        self.assertEqual(d.goo(1), (D, 1))
        self.assertEqual(d.foo(1), (d, 1))
        self.assertEqual(D.foo(d, 1), (d, 1))


        class E:
            foo = C.foo
        self.assertEqual(E().foo, C.foo)
        self.assertTrue(repr(C.foo.__get__(C())).startswith('<bound method '))
        Sensor(2439)

    def test_compattr(self):


        class C(object):


            class computed_attribute(object):

                def __init__(self, get, set=None, delete=None):
                    Sensor(2440)
                    self.__get = get
                    self.__set = set
                    self.__delete = delete
                    Sensor(2441)

                def __get__(self, obj, type=None):
                    py_ass_parser2442 = self.__get(obj)
                    Sensor(2442)
                    return py_ass_parser2442

                def __set__(self, obj, value):
                    py_ass_parser2443 = self.__set(obj, value)
                    Sensor(2443)
                    return py_ass_parser2443

                def __delete__(self, obj):
                    py_ass_parser2444 = self.__delete(obj)
                    Sensor(2444)
                    return py_ass_parser2444

            def __init__(self):
                Sensor(2445)
                self.__x = 0
                Sensor(2446)

            def __get_x(self):
                Sensor(2447)
                x = self.__x
                self.__x = x + 1
                py_ass_parser2448 = x
                Sensor(2448)
                return py_ass_parser2448

            def __set_x(self, x):
                Sensor(2449)
                self.__x = x
                Sensor(2450)

            def __delete_x(self):
                Sensor(2451)
                del self.__x
                Sensor(2452)
            x = computed_attribute(__get_x, __set_x, __delete_x)
        Sensor(2453)
        a = C()
        self.assertEqual(a.x, 0)
        self.assertEqual(a.x, 1)
        a.x = 10
        self.assertEqual(a.x, 10)
        self.assertEqual(a.x, 11)
        del a.x
        self.assertEqual(hasattr(a, 'x'), 0)
        Sensor(2454)

    def test_newslots(self):


        class C(list):

            def __new__(cls):
                Sensor(2455)
                self = list.__new__(cls)
                self.foo = 1
                py_ass_parser2456 = self
                Sensor(2456)
                return py_ass_parser2456

            def __init__(self):
                Sensor(2457)
                self.foo = self.foo + 2
                Sensor(2458)
        Sensor(2459)
        a = C()
        self.assertEqual(a.foo, 3)
        self.assertEqual(a.__class__, C)


        class D(C):
            pass
        b = D()
        self.assertEqual(b.foo, 3)
        self.assertEqual(b.__class__, D)
        Sensor(2460)

    def test_altmro(self):


        class A(object):

            def f(self):
                py_ass_parser2461 = 'A'
                Sensor(2461)
                return py_ass_parser2461


        class B(A):
            pass


        class C(A):

            def f(self):
                py_ass_parser2462 = 'C'
                Sensor(2462)
                return py_ass_parser2462


        class D(B, C):
            pass
        Sensor(2463)
        self.assertEqual(D.mro(), [D, B, C, A, object])
        self.assertEqual(D.__mro__, (D, B, C, A, object))
        self.assertEqual(D().f(), 'C')


        class PerverseMetaType(type):

            def mro(cls):
                Sensor(2464)
                L = type.mro(cls)
                L.reverse()
                py_ass_parser2465 = L
                Sensor(2465)
                return py_ass_parser2465


        class X(D, B, C, A):
            __metaclass__ = PerverseMetaType
        self.assertEqual(X.__mro__, (object, A, C, B, D, X))
        self.assertEqual(X().f(), 'A')
        Sensor(2466)
        try:


            class X(object):


                class __metaclass__(type):

                    def mro(self):
                        py_ass_parser2467 = [self, dict, object]
                        Sensor(2467)
                        return py_ass_parser2467
            Sensor(2468)
            x = object.__new__(X)
            x[5] = 6
        except TypeError:
            Sensor(2469)
            pass
        else:
            Sensor(2470)
            self.fail('devious mro() return not caught')
        Sensor(2471)
        try:
            Sensor(2473)


            class X(object):


                class __metaclass__(type):

                    def mro(self):
                        py_ass_parser2472 = [1]
                        Sensor(2472)
                        return py_ass_parser2472
        except TypeError:
            Sensor(2474)
            pass
        else:
            Sensor(2475)
            self.fail('non-class mro() return not caught')
        Sensor(2476)
        try:
            Sensor(2478)


            class X(object):


                class __metaclass__(type):

                    def mro(self):
                        py_ass_parser2477 = 1
                        Sensor(2477)
                        return py_ass_parser2477
        except TypeError:
            Sensor(2479)
            pass
        else:
            Sensor(2480)
            self.fail('non-sequence mro() return not caught')
        Sensor(2481)

    def test_overloading(self):


        class B(object):
            """Intermediate class because object doesn't have a __setattr__"""


        class C(B):

            def __getattr__(self, name):
                Sensor(2482)
                if name == 'foo':
                    py_ass_parser2483 = 'getattr', name
                    Sensor(2483)
                    return py_ass_parser2483
                else:
                    Sensor(2484)
                    raise AttributeError
                Sensor(2485)

            def __setattr__(self, name, value):
                Sensor(2486)
                if name == 'foo':
                    Sensor(2487)
                    self.setattr = name, value
                else:
                    py_ass_parser2488 = B.__setattr__(self, name, value)
                    Sensor(2488)
                    return py_ass_parser2488
                Sensor(2489)

            def __delattr__(self, name):
                Sensor(2490)
                if name == 'foo':
                    Sensor(2491)
                    self.delattr = name
                else:
                    py_ass_parser2492 = B.__delattr__(self, name)
                    Sensor(2492)
                    return py_ass_parser2492
                Sensor(2493)

            def __getitem__(self, key):
                py_ass_parser2494 = 'getitem', key
                Sensor(2494)
                return py_ass_parser2494

            def __setitem__(self, key, value):
                Sensor(2495)
                self.setitem = key, value
                Sensor(2496)

            def __delitem__(self, key):
                Sensor(2497)
                self.delitem = key
                Sensor(2498)

            def __getslice__(self, i, j):
                py_ass_parser2499 = 'getslice', i, j
                Sensor(2499)
                return py_ass_parser2499

            def __setslice__(self, i, j, value):
                Sensor(2500)
                self.setslice = i, j, value
                Sensor(2501)

            def __delslice__(self, i, j):
                Sensor(2502)
                self.delslice = i, j
                Sensor(2503)
        Sensor(2504)
        a = C()
        self.assertEqual(a.foo, ('getattr', 'foo'))
        a.foo = 12
        self.assertEqual(a.setattr, ('foo', 12))
        del a.foo
        self.assertEqual(a.delattr, 'foo')
        self.assertEqual(a[12], ('getitem', 12))
        a[12] = 21
        self.assertEqual(a.setitem, (12, 21))
        del a[12]
        self.assertEqual(a.delitem, 12)
        self.assertEqual(a[0:10], ('getslice', 0, 10))
        a[0:10] = 'foo'
        self.assertEqual(a.setslice, (0, 10, 'foo'))
        del a[0:10]
        self.assertEqual(a.delslice, (0, 10))
        Sensor(2505)

    def test_methods(self):


        class C(object):

            def __init__(self, x):
                Sensor(2506)
                self.x = x
                Sensor(2507)

            def foo(self):
                py_ass_parser2508 = self.x
                Sensor(2508)
                return py_ass_parser2508
        Sensor(2509)
        c1 = C(1)
        self.assertEqual(c1.foo(), 1)


        class D(C):
            boo = C.foo
            goo = c1.foo
        d2 = D(2)
        self.assertEqual(d2.foo(), 2)
        self.assertEqual(d2.boo(), 2)
        self.assertEqual(d2.goo(), 1)


        class E(object):
            foo = C.foo
        self.assertEqual(E().foo, C.foo)
        self.assertTrue(repr(C.foo.__get__(C(1))).startswith('<bound method '))
        Sensor(2510)

    def test_special_method_lookup(self):

        def run_context(manager):
            with manager:
                Sensor(2511)
                pass
            Sensor(2512)

        def iden(self):
            py_ass_parser2513 = self
            Sensor(2513)
            return py_ass_parser2513

        def hello(self):
            py_ass_parser2514 = 'hello'
            Sensor(2514)
            return py_ass_parser2514

        def empty_seq(self):
            py_ass_parser2515 = []
            Sensor(2515)
            return py_ass_parser2515

        def zero(self):
            py_ass_parser2516 = 0
            Sensor(2516)
            return py_ass_parser2516

        def complex_num(self):
            py_ass_parser2517 = 1j
            Sensor(2517)
            return py_ass_parser2517

        def stop(self):
            Sensor(2518)
            raise StopIteration

        def return_true(self, thing=None):
            py_ass_parser2519 = True
            Sensor(2519)
            return py_ass_parser2519

        def do_isinstance(obj):
            py_ass_parser2520 = isinstance(int, obj)
            Sensor(2520)
            return py_ass_parser2520

        def do_issubclass(obj):
            py_ass_parser2521 = issubclass(int, obj)
            Sensor(2521)
            return py_ass_parser2521

        def swallow(*args):
            Sensor(2522)
            pass
            Sensor(2523)

        def do_dict_missing(checker):


            class DictSub(checker.__class__, dict):
                pass
            Sensor(2524)
            self.assertEqual(DictSub()['hi'], 4)
            Sensor(2525)

        def some_number(self_, key):
            Sensor(2526)
            self.assertEqual(key, 'hi')
            py_ass_parser2527 = 4
            Sensor(2527)
            return py_ass_parser2527

        def format_impl(self, spec):
            py_ass_parser2528 = 'hello'
            Sensor(2528)
            return py_ass_parser2528
        Sensor(2529)
        specials = [('__unicode__', unicode, hello, set(), {}), (
            '__reversed__', reversed, empty_seq, set(), {}), (
            '__length_hint__', list, zero, set(), {'__iter__': iden, 'next':
            stop}), ('__sizeof__', sys.getsizeof, zero, set(), {}), (
            '__instancecheck__', do_isinstance, return_true, set(), {}), (
            '__missing__', do_dict_missing, some_number, set(('__class__',)
            ), {}), ('__subclasscheck__', do_issubclass, return_true, set((
            '__bases__',)), {}), ('__enter__', run_context, iden, set(), {
            '__exit__': swallow}), ('__exit__', run_context, swallow, set(),
            {'__enter__': iden}), ('__complex__', complex, complex_num, set
            (), {}), ('__format__', format, format_impl, set(), {}), (
            '__dir__', dir, empty_seq, set(), {})]


        class Checker(object):

            def __getattr__(self, attr, test=self):
                Sensor(2530)
                test.fail('__getattr__ called with {0}'.format(attr))
                Sensor(2531)

            def __getattribute__(self, attr, test=self):
                Sensor(2532)
                if attr not in ok:
                    Sensor(2533)
                    test.fail('__getattribute__ called with {0}'.format(attr))
                py_ass_parser2534 = object.__getattribute__(self, attr)
                Sensor(2534)
                return py_ass_parser2534


        class SpecialDescr(object):

            def __init__(self, impl):
                Sensor(2535)
                self.impl = impl
                Sensor(2536)

            def __get__(self, obj, owner):
                Sensor(2537)
                record.append(1)
                py_ass_parser2538 = self.impl.__get__(obj, owner)
                Sensor(2538)
                return py_ass_parser2538


        class MyException(Exception):
            pass


        class ErrDescr(object):

            def __get__(self, obj, owner):
                Sensor(2539)
                raise MyException
        Sensor(2540)
        for name, runner, meth_impl, ok, env in specials:


            class X(Checker):
                pass
            Sensor(2541)
            for attr, obj in env.iteritems():
                Sensor(2542)
                setattr(X, attr, obj)
            Sensor(2543)
            setattr(X, name, meth_impl)
            runner(X())
            record = []


            class X(Checker):
                pass
            Sensor(2544)
            for attr, obj in env.iteritems():
                Sensor(2545)
                setattr(X, attr, obj)
            Sensor(2546)
            setattr(X, name, SpecialDescr(meth_impl))
            runner(X())
            self.assertEqual(record, [1], name)


            class X(Checker):
                pass
            Sensor(2547)
            for attr, obj in env.iteritems():
                Sensor(2548)
                setattr(X, attr, obj)
            Sensor(2549)
            setattr(X, name, ErrDescr())
            Sensor(2550)
            try:
                Sensor(2551)
                runner(X())
            except MyException:
                Sensor(2552)
                pass
            else:
                Sensor(2553)
                self.fail("{0!r} didn't raise".format(name))
        Sensor(2554)

    def test_specials(self):


        class C(object):

            def __getitem__(self, i):
                Sensor(2555)
                if 0 <= i < 10:
                    py_ass_parser2556 = i
                    Sensor(2556)
                    return py_ass_parser2556
                Sensor(2557)
                raise IndexError
        Sensor(2558)
        c1 = C()
        c2 = C()
        self.assertTrue(not not c1)
        self.assertNotEqual(id(c1), id(c2))
        hash(c1)
        hash(c2)
        self.assertEqual(cmp(c1, c2), cmp(id(c1), id(c2)))
        self.assertEqual(c1, c1)
        self.assertTrue(c1 != c2)
        self.assertTrue(not c1 != c1)
        self.assertTrue(not c1 == c2)
        self.assertTrue(str(c1).find('C object at ') >= 0)
        self.assertEqual(str(c1), repr(c1))
        self.assertNotIn(-1, c1)
        Sensor(2559)
        for i in range(10):
            Sensor(2560)
            self.assertIn(i, c1)
        Sensor(2561)
        self.assertNotIn(10, c1)


        class D(object):

            def __getitem__(self, i):
                Sensor(2562)
                if 0 <= i < 10:
                    py_ass_parser2563 = i
                    Sensor(2563)
                    return py_ass_parser2563
                Sensor(2564)
                raise IndexError
        d1 = D()
        d2 = D()
        self.assertTrue(not not d1)
        self.assertNotEqual(id(d1), id(d2))
        hash(d1)
        hash(d2)
        self.assertEqual(cmp(d1, d2), cmp(id(d1), id(d2)))
        self.assertEqual(d1, d1)
        self.assertNotEqual(d1, d2)
        self.assertTrue(not d1 != d1)
        self.assertTrue(not d1 == d2)
        self.assertTrue(str(d1).find('D object at ') >= 0)
        self.assertEqual(str(d1), repr(d1))
        self.assertNotIn(-1, d1)
        Sensor(2565)
        for i in range(10):
            Sensor(2566)
            self.assertIn(i, d1)
        Sensor(2567)
        self.assertNotIn(10, d1)


        class Proxy(object):

            def __init__(self, x):
                Sensor(2568)
                self.x = x
                Sensor(2569)

            def __nonzero__(self):
                py_ass_parser2570 = not not self.x
                Sensor(2570)
                return py_ass_parser2570

            def __hash__(self):
                py_ass_parser2571 = hash(self.x)
                Sensor(2571)
                return py_ass_parser2571

            def __eq__(self, other):
                py_ass_parser2572 = self.x == other
                Sensor(2572)
                return py_ass_parser2572

            def __ne__(self, other):
                py_ass_parser2573 = self.x != other
                Sensor(2573)
                return py_ass_parser2573

            def __cmp__(self, other):
                py_ass_parser2574 = cmp(self.x, other.x)
                Sensor(2574)
                return py_ass_parser2574

            def __str__(self):
                py_ass_parser2575 = 'Proxy:%s' % self.x
                Sensor(2575)
                return py_ass_parser2575

            def __repr__(self):
                py_ass_parser2576 = 'Proxy(%r)' % self.x
                Sensor(2576)
                return py_ass_parser2576

            def __contains__(self, value):
                py_ass_parser2577 = value in self.x
                Sensor(2577)
                return py_ass_parser2577
        p0 = Proxy(0)
        p1 = Proxy(1)
        p_1 = Proxy(-1)
        self.assertFalse(p0)
        self.assertTrue(not not p1)
        self.assertEqual(hash(p0), hash(0))
        self.assertEqual(p0, p0)
        self.assertNotEqual(p0, p1)
        self.assertTrue(not p0 != p0)
        self.assertEqual(not p0, p1)
        self.assertEqual(cmp(p0, p1), -1)
        self.assertEqual(cmp(p0, p0), 0)
        self.assertEqual(cmp(p0, p_1), 1)
        self.assertEqual(str(p0), 'Proxy:0')
        self.assertEqual(repr(p0), 'Proxy(0)')
        p10 = Proxy(range(10))
        self.assertNotIn(-1, p10)
        Sensor(2578)
        for i in range(10):
            Sensor(2579)
            self.assertIn(i, p10)
        Sensor(2580)
        self.assertNotIn(10, p10)


        class DProxy(object):

            def __init__(self, x):
                Sensor(2581)
                self.x = x
                Sensor(2582)

            def __nonzero__(self):
                py_ass_parser2583 = not not self.x
                Sensor(2583)
                return py_ass_parser2583

            def __hash__(self):
                py_ass_parser2584 = hash(self.x)
                Sensor(2584)
                return py_ass_parser2584

            def __eq__(self, other):
                py_ass_parser2585 = self.x == other
                Sensor(2585)
                return py_ass_parser2585

            def __ne__(self, other):
                py_ass_parser2586 = self.x != other
                Sensor(2586)
                return py_ass_parser2586

            def __cmp__(self, other):
                py_ass_parser2587 = cmp(self.x, other.x)
                Sensor(2587)
                return py_ass_parser2587

            def __str__(self):
                py_ass_parser2588 = 'DProxy:%s' % self.x
                Sensor(2588)
                return py_ass_parser2588

            def __repr__(self):
                py_ass_parser2589 = 'DProxy(%r)' % self.x
                Sensor(2589)
                return py_ass_parser2589

            def __contains__(self, value):
                py_ass_parser2590 = value in self.x
                Sensor(2590)
                return py_ass_parser2590
        p0 = DProxy(0)
        p1 = DProxy(1)
        p_1 = DProxy(-1)
        self.assertFalse(p0)
        self.assertTrue(not not p1)
        self.assertEqual(hash(p0), hash(0))
        self.assertEqual(p0, p0)
        self.assertNotEqual(p0, p1)
        self.assertNotEqual(not p0, p0)
        self.assertEqual(not p0, p1)
        self.assertEqual(cmp(p0, p1), -1)
        self.assertEqual(cmp(p0, p0), 0)
        self.assertEqual(cmp(p0, p_1), 1)
        self.assertEqual(str(p0), 'DProxy:0')
        self.assertEqual(repr(p0), 'DProxy(0)')
        p10 = DProxy(range(10))
        self.assertNotIn(-1, p10)
        Sensor(2591)
        for i in range(10):
            Sensor(2592)
            self.assertIn(i, p10)
        Sensor(2593)
        self.assertNotIn(10, p10)

        def unsafecmp(a, b):
            Sensor(2594)
            if not hasattr(a, '__cmp__'):
                Sensor(2595)
                return
            Sensor(2596)
            try:
                Sensor(2597)
                a.__class__.__cmp__(a, b)
            except TypeError:
                Sensor(2598)
                pass
            else:
                Sensor(2599)
                self.fail("shouldn't allow %s.__cmp__(%r, %r)" % (a.
                    __class__, a, b))
            Sensor(2600)
        unsafecmp(u'123', '123')
        unsafecmp('123', u'123')
        unsafecmp(1, 1.0)
        unsafecmp(1.0, 1)
        unsafecmp(1, 1L)
        unsafecmp(1L, 1)
        Sensor(2601)

    @test_support.impl_detail('custom logic for printing to real file objects')
    def test_recursions_1(self):


        class Letter(str):

            def __new__(cls, letter):
                Sensor(2602)
                if letter == 'EPS':
                    py_ass_parser2603 = str.__new__(cls)
                    Sensor(2603)
                    return py_ass_parser2603
                py_ass_parser2604 = str.__new__(cls, letter)
                Sensor(2604)
                return py_ass_parser2604

            def __str__(self):
                Sensor(2605)
                if not self:
                    py_ass_parser2606 = 'EPS'
                    Sensor(2606)
                    return py_ass_parser2606
                py_ass_parser2607 = self
                Sensor(2607)
                return py_ass_parser2607
        Sensor(2608)
        test_stdout = sys.stdout
        sys.stdout = test_support.get_original_stdout()
        try:
            Sensor(2609)
            try:
                Sensor(2610)
                print Letter('w')
            except RuntimeError:
                Sensor(2611)
                pass
            else:
                Sensor(2612)
                self.fail('expected a RuntimeError for print recursion')
        finally:
            Sensor(2613)
            sys.stdout = test_stdout
        Sensor(2614)

    def test_recursions_2(self):


        class A(object):
            pass
        Sensor(2615)
        A.__mul__ = types.MethodType(lambda self, x: self * x, None, A)
        Sensor(2616)
        try:
            Sensor(2617)
            A() * 2
        except RuntimeError:
            Sensor(2618)
            pass
        else:
            Sensor(2619)
            self.fail('expected a RuntimeError')
        Sensor(2620)

    def test_weakrefs(self):
        import weakref


        class C(object):
            pass
        Sensor(2621)
        c = C()
        r = weakref.ref(c)
        self.assertEqual(r(), c)
        del c
        test_support.gc_collect()
        self.assertEqual(r(), None)
        del r


        class NoWeak(object):
            __slots__ = ['foo']
        no = NoWeak()
        Sensor(2622)
        try:
            Sensor(2623)
            weakref.ref(no)
        except TypeError as msg:
            Sensor(2624)
            self.assertTrue(str(msg).find('weak reference') >= 0)
        else:
            Sensor(2625)
            self.fail('weakref.ref(no) should be illegal')


        class Weak(object):
            __slots__ = ['foo', '__weakref__']
        Sensor(2626)
        yes = Weak()
        r = weakref.ref(yes)
        self.assertEqual(r(), yes)
        del yes
        test_support.gc_collect()
        self.assertEqual(r(), None)
        del r
        Sensor(2627)

    def test_properties(self):


        class C(object):

            def getx(self):
                py_ass_parser2628 = self.__x
                Sensor(2628)
                return py_ass_parser2628

            def setx(self, value):
                Sensor(2629)
                self.__x = value
                Sensor(2630)

            def delx(self):
                Sensor(2631)
                del self.__x
                Sensor(2632)
            x = property(getx, setx, delx, doc="I'm the x property.")
        Sensor(2633)
        a = C()
        self.assertFalse(hasattr(a, 'x'))
        a.x = 42
        self.assertEqual(a._C__x, 42)
        self.assertEqual(a.x, 42)
        del a.x
        self.assertFalse(hasattr(a, 'x'))
        self.assertFalse(hasattr(a, '_C__x'))
        C.x.__set__(a, 100)
        self.assertEqual(C.x.__get__(a), 100)
        C.x.__delete__(a)
        self.assertFalse(hasattr(a, 'x'))
        raw = C.__dict__['x']
        self.assertIsInstance(raw, property)
        attrs = dir(raw)
        self.assertIn('__doc__', attrs)
        self.assertIn('fget', attrs)
        self.assertIn('fset', attrs)
        self.assertIn('fdel', attrs)
        self.assertEqual(raw.__doc__, "I'm the x property.")
        self.assertTrue(raw.fget is C.__dict__['getx'])
        self.assertTrue(raw.fset is C.__dict__['setx'])
        self.assertTrue(raw.fdel is C.__dict__['delx'])
        Sensor(2634)
        for attr in ('__doc__', 'fget', 'fset', 'fdel'):
            Sensor(2635)
            try:
                Sensor(2636)
                setattr(raw, attr, 42)
            except TypeError as msg:
                Sensor(2637)
                if str(msg).find('readonly') < 0:
                    Sensor(2638)
                    self.fail(
                        'when setting readonly attr %r on a property, got unexpected TypeError msg %r'
                         % (attr, str(msg)))
            else:
                Sensor(2639)
                self.fail(
                    'expected TypeError from trying to set readonly %r attr on a property'
                     % attr)


        class D(object):
            __getitem__ = property(lambda s: 1 / 0)
        Sensor(2640)
        d = D()
        Sensor(2641)
        try:
            Sensor(2642)
            for i in d:
                Sensor(2643)
                str(i)
        except ZeroDivisionError:
            Sensor(2644)
            pass
        else:
            Sensor(2645)
            self.fail('expected ZeroDivisionError from bad property')
        Sensor(2646)

    @unittest.skipIf(sys.flags.optimize >= 2,
        'Docstrings are omitted with -O2 and above')
    def test_properties_doc_attrib(self):


        class E(object):

            def getter(self):
                Sensor(2647)
                """getter method"""
                py_ass_parser2648 = 0
                Sensor(2648)
                return py_ass_parser2648

            def setter(self_, value):
                Sensor(2649)
                """setter method"""
                pass
                Sensor(2650)
            prop = property(getter)
            self.assertEqual(prop.__doc__, 'getter method')
            prop2 = property(fset=setter)
            self.assertEqual(prop2.__doc__, None)
        Sensor(2651)

    def test_testcapi_no_segfault(self):
        Sensor(2652)
        try:
            Sensor(2653)
            import _testcapi
        except ImportError:
            Sensor(2654)
            pass
        else:
            Sensor(2655)


            class X(object):
                p = property(_testcapi.test_with_docstring)
        Sensor(2656)

    def test_properties_plus(self):


        class C(object):
            foo = property(doc='hello')

            @foo.getter
            def foo(self):
                py_ass_parser2657 = self._foo
                Sensor(2657)
                return py_ass_parser2657

            @foo.setter
            def foo(self, value):
                Sensor(2658)
                self._foo = abs(value)
                Sensor(2659)

            @foo.deleter
            def foo(self):
                Sensor(2660)
                del self._foo
                Sensor(2661)
        Sensor(2662)
        c = C()
        self.assertEqual(C.foo.__doc__, 'hello')
        self.assertFalse(hasattr(c, 'foo'))
        c.foo = -42
        self.assertTrue(hasattr(c, '_foo'))
        self.assertEqual(c._foo, 42)
        self.assertEqual(c.foo, 42)
        del c.foo
        self.assertFalse(hasattr(c, '_foo'))
        self.assertFalse(hasattr(c, 'foo'))


        class D(C):

            @C.foo.deleter
            def foo(self):
                Sensor(2663)
                try:
                    Sensor(2664)
                    del self._foo
                except AttributeError:
                    Sensor(2665)
                    pass
                Sensor(2666)
        d = D()
        d.foo = 24
        self.assertEqual(d.foo, 24)
        del d.foo
        del d.foo


        class E(object):

            @property
            def foo(self):
                py_ass_parser2667 = self._foo
                Sensor(2667)
                return py_ass_parser2667

            @foo.setter
            def foo(self, value):
                Sensor(2668)
                raise RuntimeError

            @foo.setter
            def foo(self, value):
                Sensor(2669)
                self._foo = abs(value)
                Sensor(2670)

            @foo.deleter
            def foo(self, value=None):
                Sensor(2671)
                del self._foo
                Sensor(2672)
        e = E()
        e.foo = -42
        self.assertEqual(e.foo, 42)
        del e.foo


        class F(E):

            @E.foo.deleter
            def foo(self):
                Sensor(2673)
                del self._foo
                Sensor(2674)

            @foo.setter
            def foo(self, value):
                Sensor(2675)
                self._foo = max(0, value)
                Sensor(2676)
        f = F()
        f.foo = -10
        self.assertEqual(f.foo, 0)
        del f.foo
        Sensor(2677)

    def test_dict_constructors(self):
        Sensor(2678)
        d = dict()
        self.assertEqual(d, {})
        d = dict({})
        self.assertEqual(d, {})
        d = dict({(1): 2, 'a': 'b'})
        self.assertEqual(d, {(1): 2, 'a': 'b'})
        self.assertEqual(d, dict(d.items()))
        self.assertEqual(d, dict(d.iteritems()))
        d = dict({'one': 1, 'two': 2})
        self.assertEqual(d, dict(one=1, two=2))
        self.assertEqual(d, dict(**d))
        self.assertEqual(d, dict({'one': 1}, two=2))
        self.assertEqual(d, dict([('two', 2)], one=1))
        self.assertEqual(d, dict([('one', 100), ('two', 200)], **d))
        self.assertEqual(d, dict(**d))
        Sensor(2679)
        for badarg in (0, 0L, 0j, '0', [0], (0,)):
            Sensor(2680)
            try:
                Sensor(2681)
                dict(badarg)
            except TypeError:
                Sensor(2682)
                pass
            except ValueError:
                Sensor(2683)
                if badarg == '0':
                    Sensor(2684)
                    pass
                else:
                    Sensor(2685)
                    self.fail('no TypeError from dict(%r)' % badarg)
            else:
                Sensor(2686)
                self.fail('no TypeError from dict(%r)' % badarg)
        Sensor(2687)
        try:
            Sensor(2688)
            dict({}, {})
        except TypeError:
            Sensor(2689)
            pass
        else:
            Sensor(2690)
            self.fail('no TypeError from dict({}, {})')


        class Mapping:
            dict = {(1): 2, (3): 4, 'a': 1j}
        Sensor(2691)
        try:
            Sensor(2692)
            dict(Mapping())
        except TypeError:
            Sensor(2693)
            pass
        else:
            Sensor(2694)
            self.fail('no TypeError from dict(incomplete mapping)')
        Sensor(2695)
        Mapping.keys = lambda self: self.dict.keys()
        Mapping.__getitem__ = lambda self, i: self.dict[i]
        d = dict(Mapping())
        self.assertEqual(d, Mapping.dict)


        class AddressBookEntry:

            def __init__(self, first, last):
                Sensor(2696)
                self.first = first
                self.last = last
                Sensor(2697)

            def __iter__(self):
                py_ass_parser2698 = iter([self.first, self.last])
                Sensor(2698)
                return py_ass_parser2698
        d = dict([AddressBookEntry('Tim', 'Warsaw'), AddressBookEntry(
            'Barry', 'Peters'), AddressBookEntry('Tim', 'Peters'),
            AddressBookEntry('Barry', 'Warsaw')])
        self.assertEqual(d, {'Barry': 'Warsaw', 'Tim': 'Peters'})
        d = dict(zip(range(4), range(1, 5)))
        self.assertEqual(d, dict([(i, i + 1) for i in range(4)]))
        Sensor(2699)
        for bad in ([('tooshort',)], [('too', 'long', 'by 1')]):
            Sensor(2700)
            try:
                Sensor(2701)
                dict(bad)
            except ValueError:
                Sensor(2702)
                pass
            else:
                Sensor(2703)
                self.fail('no ValueError from dict(%r)' % bad)
        Sensor(2704)

    def test_dir(self):
        Sensor(2705)
        junk = 12
        self.assertEqual(dir(), ['junk', 'self'])
        del junk
        Sensor(2706)
        for arg in (2, 2L, 2j, 2.0, [2], '2', u'2', (2,), {(2): 2}, type,
            self.test_dir):
            Sensor(2707)
            dir(arg)


        class C:
            Cdata = 1

            def Cmethod(self):
                Sensor(2708)
                pass
                Sensor(2709)
        Sensor(2710)
        cstuff = ['Cdata', 'Cmethod', '__doc__', '__module__']
        self.assertEqual(dir(C), cstuff)
        self.assertIn('im_self', dir(C.Cmethod))
        c = C()
        self.assertEqual(dir(c), cstuff)
        c.cdata = 2
        c.cmethod = lambda self: 0
        self.assertEqual(dir(c), cstuff + ['cdata', 'cmethod'])
        self.assertIn('im_self', dir(c.Cmethod))


        class A(C):
            Adata = 1

            def Amethod(self):
                Sensor(2711)
                pass
                Sensor(2712)
        astuff = ['Adata', 'Amethod'] + cstuff
        self.assertEqual(dir(A), astuff)
        self.assertIn('im_self', dir(A.Amethod))
        a = A()
        self.assertEqual(dir(a), astuff)
        self.assertIn('im_self', dir(a.Amethod))
        a.adata = 42
        a.amethod = lambda self: 3
        self.assertEqual(dir(a), astuff + ['adata', 'amethod'])

        def interesting(strings):
            py_ass_parser2713 = [s for s in strings if not s.startswith('_')]
            Sensor(2713)
            return py_ass_parser2713


        class C(object):
            Cdata = 1

            def Cmethod(self):
                Sensor(2714)
                pass
                Sensor(2715)
        cstuff = ['Cdata', 'Cmethod']
        self.assertEqual(interesting(dir(C)), cstuff)
        c = C()
        self.assertEqual(interesting(dir(c)), cstuff)
        self.assertIn('im_self', dir(C.Cmethod))
        c.cdata = 2
        c.cmethod = lambda self: 0
        self.assertEqual(interesting(dir(c)), cstuff + ['cdata', 'cmethod'])
        self.assertIn('im_self', dir(c.Cmethod))


        class A(C):
            Adata = 1

            def Amethod(self):
                Sensor(2716)
                pass
                Sensor(2717)
        astuff = ['Adata', 'Amethod'] + cstuff
        self.assertEqual(interesting(dir(A)), astuff)
        self.assertIn('im_self', dir(A.Amethod))
        a = A()
        self.assertEqual(interesting(dir(a)), astuff)
        a.adata = 42
        a.amethod = lambda self: 3
        self.assertEqual(interesting(dir(a)), astuff + ['adata', 'amethod'])
        self.assertIn('im_self', dir(a.Amethod))


        class M(type(sys)):
            pass
        minstance = M('m')
        minstance.b = 2
        minstance.a = 1
        names = [x for x in dir(minstance) if x not in ['__name__', '__doc__']]
        self.assertEqual(names, ['a', 'b'])


        class M2(M):

            def getdict(self):
                py_ass_parser2718 = 'Not a dict!'
                Sensor(2718)
                return py_ass_parser2718
            __dict__ = property(getdict)
        m2instance = M2('m2')
        m2instance.b = 2
        m2instance.a = 1
        self.assertEqual(m2instance.__dict__, 'Not a dict!')
        Sensor(2719)
        try:
            Sensor(2720)
            dir(m2instance)
        except TypeError:
            Sensor(2721)
            pass
        Sensor(2722)
        self.assertEqual(dir(NotImplemented), dir(Ellipsis))
        Sensor(2723)
        if test_support.check_impl_detail():
            Sensor(2724)
            self.assertEqual(dir(None), dir(Ellipsis))


        class Wrapper(object):

            def __init__(self, obj):
                Sensor(2725)
                self.__obj = obj
                Sensor(2726)

            def __repr__(self):
                py_ass_parser2727 = 'Wrapper(%s)' % repr(self.__obj)
                Sensor(2727)
                return py_ass_parser2727

            def __getitem__(self, key):
                py_ass_parser2728 = Wrapper(self.__obj[key])
                Sensor(2728)
                return py_ass_parser2728

            def __len__(self):
                py_ass_parser2729 = len(self.__obj)
                Sensor(2729)
                return py_ass_parser2729

            def __getattr__(self, name):
                py_ass_parser2730 = Wrapper(getattr(self.__obj, name))
                Sensor(2730)
                return py_ass_parser2730


        class C(object):

            def __getclass(self):
                py_ass_parser2731 = Wrapper(type(self))
                Sensor(2731)
                return py_ass_parser2731
            __class__ = property(__getclass)
        Sensor(2732)
        dir(C())
        Sensor(2733)

    def test_supers(self):


        class A(object):

            def meth(self, a):
                py_ass_parser2734 = 'A(%r)' % a
                Sensor(2734)
                return py_ass_parser2734
        Sensor(2735)
        self.assertEqual(A().meth(1), 'A(1)')


        class B(A):

            def __init__(self):
                Sensor(2736)
                self.__super = super(B, self)
                Sensor(2737)

            def meth(self, a):
                py_ass_parser2738 = 'B(%r)' % a + self.__super.meth(a)
                Sensor(2738)
                return py_ass_parser2738
        self.assertEqual(B().meth(2), 'B(2)A(2)')


        class C(A):

            def meth(self, a):
                py_ass_parser2739 = 'C(%r)' % a + self.__super.meth(a)
                Sensor(2739)
                return py_ass_parser2739
        C._C__super = super(C)
        self.assertEqual(C().meth(3), 'C(3)A(3)')


        class D(C, B):

            def meth(self, a):
                py_ass_parser2740 = 'D(%r)' % a + super(D, self).meth(a)
                Sensor(2740)
                return py_ass_parser2740
        self.assertEqual(D().meth(4), 'D(4)C(4)B(4)A(4)')


        class mysuper(super):

            def __init__(self, *args):
                py_ass_parser2741 = super(mysuper, self).__init__(*args)
                Sensor(2741)
                return py_ass_parser2741


        class E(D):

            def meth(self, a):
                py_ass_parser2742 = 'E(%r)' % a + mysuper(E, self).meth(a)
                Sensor(2742)
                return py_ass_parser2742
        self.assertEqual(E().meth(5), 'E(5)D(5)C(5)B(5)A(5)')


        class F(E):

            def meth(self, a):
                Sensor(2743)
                s = self.__super
                py_ass_parser2744 = 'F(%r)[%s]' % (a, s.__class__.__name__
                    ) + s.meth(a)
                Sensor(2744)
                return py_ass_parser2744
        F._F__super = mysuper(F)
        self.assertEqual(F().meth(6), 'F(6)[mysuper]E(6)D(6)C(6)B(6)A(6)')
        Sensor(2745)
        try:
            Sensor(2746)
            super(D, 42)
        except TypeError:
            Sensor(2747)
            pass
        else:
            Sensor(2748)
            self.fail("shouldn't allow super(D, 42)")
        Sensor(2749)
        try:
            Sensor(2750)
            super(D, C())
        except TypeError:
            Sensor(2751)
            pass
        else:
            Sensor(2752)
            self.fail("shouldn't allow super(D, C())")
        Sensor(2753)
        try:
            Sensor(2754)
            super(D).__get__(12)
        except TypeError:
            Sensor(2755)
            pass
        else:
            Sensor(2756)
            self.fail("shouldn't allow super(D).__get__(12)")
        Sensor(2757)
        try:
            Sensor(2758)
            super(D).__get__(C())
        except TypeError:
            Sensor(2759)
            pass
        else:
            Sensor(2760)
            self.fail("shouldn't allow super(D).__get__(C())")


        class DDbase(object):

            def getx(self):
                py_ass_parser2761 = 42
                Sensor(2761)
                return py_ass_parser2761
            x = property(getx)


        class DDsub(DDbase):

            def getx(self):
                py_ass_parser2762 = 'hello'
                Sensor(2762)
                return py_ass_parser2762
            x = property(getx)
        Sensor(2763)
        dd = DDsub()
        self.assertEqual(dd.x, 'hello')
        self.assertEqual(super(DDsub, dd).x, 42)


        class Base(object):
            aProp = property(lambda self: 'foo')


        class Sub(Base):

            @classmethod
            def test(klass):
                py_ass_parser2764 = super(Sub, klass).aProp
                Sensor(2764)
                return py_ass_parser2764
        self.assertEqual(Sub.test(), Base.aProp)
        Sensor(2765)
        try:
            Sensor(2766)
            super(Base, kw=1)
        except TypeError:
            Sensor(2767)
            pass
        else:
            Sensor(2768)
            self.assertEqual("super shouldn't accept keyword args")
        Sensor(2769)

    def test_basic_inheritance(self):


        class hexint(int):

            def __repr__(self):
                py_ass_parser2770 = hex(self)
                Sensor(2770)
                return py_ass_parser2770

            def __add__(self, other):
                py_ass_parser2771 = hexint(int.__add__(self, other))
                Sensor(2771)
                return py_ass_parser2771
        Sensor(2772)
        self.assertEqual(repr(hexint(7) + 9), '0x10')
        self.assertEqual(repr(hexint(1000) + 7), '0x3ef')
        a = hexint(12345)
        self.assertEqual(a, 12345)
        self.assertEqual(int(a), 12345)
        self.assertTrue(int(a).__class__ is int)
        self.assertEqual(hash(a), hash(12345))
        self.assertTrue((+a).__class__ is int)
        self.assertTrue((a >> 0).__class__ is int)
        self.assertTrue((a << 0).__class__ is int)
        self.assertTrue((hexint(0) << 12).__class__ is int)
        self.assertTrue((hexint(0) >> 12).__class__ is int)


        class octlong(long):
            __slots__ = []

            def __str__(self):
                Sensor(2773)
                s = oct(self)
                Sensor(2774)
                if s[-1] == 'L':
                    Sensor(2775)
                    s = s[:-1]
                py_ass_parser2776 = s
                Sensor(2776)
                return py_ass_parser2776

            def __add__(self, other):
                py_ass_parser2777 = self.__class__(super(octlong, self).
                    __add__(other))
                Sensor(2777)
                return py_ass_parser2777
            __radd__ = __add__
        self.assertEqual(str(octlong(3) + 5), '010')
        self.assertEqual(str(5 + octlong(3000)), '05675')
        a = octlong(12345)
        self.assertEqual(a, 12345L)
        self.assertEqual(long(a), 12345L)
        self.assertEqual(hash(a), hash(12345L))
        self.assertTrue(long(a).__class__ is long)
        self.assertTrue((+a).__class__ is long)
        self.assertTrue((-a).__class__ is long)
        self.assertTrue((-octlong(0)).__class__ is long)
        self.assertTrue((a >> 0).__class__ is long)
        self.assertTrue((a << 0).__class__ is long)
        self.assertTrue((a - 0).__class__ is long)
        self.assertTrue((a * 1).__class__ is long)
        self.assertTrue((a ** 1).__class__ is long)
        self.assertTrue((a // 1).__class__ is long)
        self.assertTrue((1 * a).__class__ is long)
        self.assertTrue((a | 0).__class__ is long)
        self.assertTrue((a ^ 0).__class__ is long)
        self.assertTrue((a & -1L).__class__ is long)
        self.assertTrue((octlong(0) << 12).__class__ is long)
        self.assertTrue((octlong(0) >> 12).__class__ is long)
        self.assertTrue(abs(octlong(0)).__class__ is long)


        class longclone(long):
            pass
        a = longclone(1)
        self.assertTrue((a + 0).__class__ is long)
        self.assertTrue((0 + a).__class__ is long)
        a = longclone(-1)
        self.assertEqual(a.__dict__, {})
        self.assertEqual(long(a), -1)


        class precfloat(float):
            __slots__ = ['prec']

            def __init__(self, value=0.0, prec=12):
                Sensor(2778)
                self.prec = int(prec)
                Sensor(2779)

            def __repr__(self):
                py_ass_parser2780 = '%.*g' % (self.prec, self)
                Sensor(2780)
                return py_ass_parser2780
        self.assertEqual(repr(precfloat(1.1)), '1.1')
        a = precfloat(12345)
        self.assertEqual(a, 12345.0)
        self.assertEqual(float(a), 12345.0)
        self.assertTrue(float(a).__class__ is float)
        self.assertEqual(hash(a), hash(12345.0))
        self.assertTrue((+a).__class__ is float)


        class madcomplex(complex):

            def __repr__(self):
                py_ass_parser2781 = '%.17gj%+.17g' % (self.imag, self.real)
                Sensor(2781)
                return py_ass_parser2781
        a = madcomplex(-3, 4)
        self.assertEqual(repr(a), '4j-3')
        base = complex(-3, 4)
        self.assertEqual(base.__class__, complex)
        self.assertEqual(a, base)
        self.assertEqual(complex(a), base)
        self.assertEqual(complex(a).__class__, complex)
        a = madcomplex(a)
        self.assertEqual(repr(a), '4j-3')
        self.assertEqual(a, base)
        self.assertEqual(complex(a), base)
        self.assertEqual(complex(a).__class__, complex)
        self.assertEqual(hash(a), hash(base))
        self.assertEqual((+a).__class__, complex)
        self.assertEqual((a + 0).__class__, complex)
        self.assertEqual(a + 0, base)
        self.assertEqual((a - 0).__class__, complex)
        self.assertEqual(a - 0, base)
        self.assertEqual((a * 1).__class__, complex)
        self.assertEqual(a * 1, base)
        self.assertEqual((a / 1).__class__, complex)
        self.assertEqual(a / 1, base)


        class madtuple(tuple):
            _rev = None

            def rev(self):
                Sensor(2782)
                if self._rev is not None:
                    py_ass_parser2783 = self._rev
                    Sensor(2783)
                    return py_ass_parser2783
                Sensor(2784)
                L = list(self)
                L.reverse()
                self._rev = self.__class__(L)
                py_ass_parser2785 = self._rev
                Sensor(2785)
                return py_ass_parser2785
        a = madtuple((1, 2, 3, 4, 5, 6, 7, 8, 9, 0))
        self.assertEqual(a, (1, 2, 3, 4, 5, 6, 7, 8, 9, 0))
        self.assertEqual(a.rev(), madtuple((0, 9, 8, 7, 6, 5, 4, 3, 2, 1)))
        self.assertEqual(a.rev().rev(), madtuple((1, 2, 3, 4, 5, 6, 7, 8, 9,
            0)))
        Sensor(2786)
        for i in range(512):
            Sensor(2787)
            t = madtuple(range(i))
            u = t.rev()
            v = u.rev()
            self.assertEqual(v, t)
        Sensor(2788)
        a = madtuple((1, 2, 3, 4, 5))
        self.assertEqual(tuple(a), (1, 2, 3, 4, 5))
        self.assertTrue(tuple(a).__class__ is tuple)
        self.assertEqual(hash(a), hash((1, 2, 3, 4, 5)))
        self.assertTrue(a[:].__class__ is tuple)
        self.assertTrue((a * 1).__class__ is tuple)
        self.assertTrue((a * 0).__class__ is tuple)
        self.assertTrue((a + ()).__class__ is tuple)
        a = madtuple(())
        self.assertEqual(tuple(a), ())
        self.assertTrue(tuple(a).__class__ is tuple)
        self.assertTrue((a + a).__class__ is tuple)
        self.assertTrue((a * 0).__class__ is tuple)
        self.assertTrue((a * 1).__class__ is tuple)
        self.assertTrue((a * 2).__class__ is tuple)
        self.assertTrue(a[:].__class__ is tuple)


        class madstring(str):
            _rev = None

            def rev(self):
                Sensor(2789)
                if self._rev is not None:
                    py_ass_parser2790 = self._rev
                    Sensor(2790)
                    return py_ass_parser2790
                Sensor(2791)
                L = list(self)
                L.reverse()
                self._rev = self.__class__(''.join(L))
                py_ass_parser2792 = self._rev
                Sensor(2792)
                return py_ass_parser2792
        s = madstring('abcdefghijklmnopqrstuvwxyz')
        self.assertEqual(s, 'abcdefghijklmnopqrstuvwxyz')
        self.assertEqual(s.rev(), madstring('zyxwvutsrqponmlkjihgfedcba'))
        self.assertEqual(s.rev().rev(), madstring('abcdefghijklmnopqrstuvwxyz')
            )
        Sensor(2793)
        for i in range(256):
            Sensor(2794)
            s = madstring(''.join(map(chr, range(i))))
            t = s.rev()
            u = t.rev()
            self.assertEqual(u, s)
        Sensor(2795)
        s = madstring('12345')
        self.assertEqual(str(s), '12345')
        self.assertTrue(str(s).__class__ is str)
        base = '\x00' * 5
        s = madstring(base)
        self.assertEqual(s, base)
        self.assertEqual(str(s), base)
        self.assertTrue(str(s).__class__ is str)
        self.assertEqual(hash(s), hash(base))
        self.assertEqual({s: 1}[base], 1)
        self.assertEqual({base: 1}[s], 1)
        self.assertTrue((s + '').__class__ is str)
        self.assertEqual(s + '', base)
        self.assertTrue(('' + s).__class__ is str)
        self.assertEqual('' + s, base)
        self.assertTrue((s * 0).__class__ is str)
        self.assertEqual(s * 0, '')
        self.assertTrue((s * 1).__class__ is str)
        self.assertEqual(s * 1, base)
        self.assertTrue((s * 2).__class__ is str)
        self.assertEqual(s * 2, base + base)
        self.assertTrue(s[:].__class__ is str)
        self.assertEqual(s[:], base)
        self.assertTrue(s[0:0].__class__ is str)
        self.assertEqual(s[0:0], '')
        self.assertTrue(s.strip().__class__ is str)
        self.assertEqual(s.strip(), base)
        self.assertTrue(s.lstrip().__class__ is str)
        self.assertEqual(s.lstrip(), base)
        self.assertTrue(s.rstrip().__class__ is str)
        self.assertEqual(s.rstrip(), base)
        identitytab = ''.join([chr(i) for i in range(256)])
        self.assertTrue(s.translate(identitytab).__class__ is str)
        self.assertEqual(s.translate(identitytab), base)
        self.assertTrue(s.translate(identitytab, 'x').__class__ is str)
        self.assertEqual(s.translate(identitytab, 'x'), base)
        self.assertEqual(s.translate(identitytab, '\x00'), '')
        self.assertTrue(s.replace('x', 'x').__class__ is str)
        self.assertEqual(s.replace('x', 'x'), base)
        self.assertTrue(s.ljust(len(s)).__class__ is str)
        self.assertEqual(s.ljust(len(s)), base)
        self.assertTrue(s.rjust(len(s)).__class__ is str)
        self.assertEqual(s.rjust(len(s)), base)
        self.assertTrue(s.center(len(s)).__class__ is str)
        self.assertEqual(s.center(len(s)), base)
        self.assertTrue(s.lower().__class__ is str)
        self.assertEqual(s.lower(), base)


        class madunicode(unicode):
            _rev = None

            def rev(self):
                Sensor(2796)
                if self._rev is not None:
                    py_ass_parser2797 = self._rev
                    Sensor(2797)
                    return py_ass_parser2797
                Sensor(2798)
                L = list(self)
                L.reverse()
                self._rev = self.__class__(u''.join(L))
                py_ass_parser2799 = self._rev
                Sensor(2799)
                return py_ass_parser2799
        u = madunicode('ABCDEF')
        self.assertEqual(u, u'ABCDEF')
        self.assertEqual(u.rev(), madunicode(u'FEDCBA'))
        self.assertEqual(u.rev().rev(), madunicode(u'ABCDEF'))
        base = u'12345'
        u = madunicode(base)
        self.assertEqual(unicode(u), base)
        self.assertTrue(unicode(u).__class__ is unicode)
        self.assertEqual(hash(u), hash(base))
        self.assertEqual({u: 1}[base], 1)
        self.assertEqual({base: 1}[u], 1)
        self.assertTrue(u.strip().__class__ is unicode)
        self.assertEqual(u.strip(), base)
        self.assertTrue(u.lstrip().__class__ is unicode)
        self.assertEqual(u.lstrip(), base)
        self.assertTrue(u.rstrip().__class__ is unicode)
        self.assertEqual(u.rstrip(), base)
        self.assertTrue(u.replace(u'x', u'x').__class__ is unicode)
        self.assertEqual(u.replace(u'x', u'x'), base)
        self.assertTrue(u.replace(u'xy', u'xy').__class__ is unicode)
        self.assertEqual(u.replace(u'xy', u'xy'), base)
        self.assertTrue(u.center(len(u)).__class__ is unicode)
        self.assertEqual(u.center(len(u)), base)
        self.assertTrue(u.ljust(len(u)).__class__ is unicode)
        self.assertEqual(u.ljust(len(u)), base)
        self.assertTrue(u.rjust(len(u)).__class__ is unicode)
        self.assertEqual(u.rjust(len(u)), base)
        self.assertTrue(u.lower().__class__ is unicode)
        self.assertEqual(u.lower(), base)
        self.assertTrue(u.upper().__class__ is unicode)
        self.assertEqual(u.upper(), base)
        self.assertTrue(u.capitalize().__class__ is unicode)
        self.assertEqual(u.capitalize(), base)
        self.assertTrue(u.title().__class__ is unicode)
        self.assertEqual(u.title(), base)
        self.assertTrue((u + u'').__class__ is unicode)
        self.assertEqual(u + u'', base)
        self.assertTrue((u'' + u).__class__ is unicode)
        self.assertEqual(u'' + u, base)
        self.assertTrue((u * 0).__class__ is unicode)
        self.assertEqual(u * 0, u'')
        self.assertTrue((u * 1).__class__ is unicode)
        self.assertEqual(u * 1, base)
        self.assertTrue((u * 2).__class__ is unicode)
        self.assertEqual(u * 2, base + base)
        self.assertTrue(u[:].__class__ is unicode)
        self.assertEqual(u[:], base)
        self.assertTrue(u[0:0].__class__ is unicode)
        self.assertEqual(u[0:0], u'')


        class sublist(list):
            pass
        a = sublist(range(5))
        self.assertEqual(a, range(5))
        a.append('hello')
        self.assertEqual(a, range(5) + ['hello'])
        a[5] = 5
        self.assertEqual(a, range(6))
        a.extend(range(6, 20))
        self.assertEqual(a, range(20))
        a[-5:] = []
        self.assertEqual(a, range(15))
        del a[10:15]
        self.assertEqual(len(a), 10)
        self.assertEqual(a, range(10))
        self.assertEqual(list(a), range(10))
        self.assertEqual(a[0], 0)
        self.assertEqual(a[9], 9)
        self.assertEqual(a[-10], 0)
        self.assertEqual(a[-1], 9)
        self.assertEqual(a[:5], range(5))


        class CountedInput(file):
            """Counts lines read by self.readline().

            self.lineno is the 0-based ordinal of the last line read, up to
            a maximum of one greater than the number of lines in the file.

            self.ateof is true if and only if the final "" line has been read,
            at which point self.lineno stops incrementing, and further calls
            to readline() continue to return "".
            """
            lineno = 0
            ateof = 0

            def readline(self):
                Sensor(2800)
                if self.ateof:
                    py_ass_parser2801 = ''
                    Sensor(2801)
                    return py_ass_parser2801
                Sensor(2802)
                s = file.readline(self)
                self.lineno += 1
                Sensor(2803)
                if s == '':
                    Sensor(2804)
                    self.ateof = 1
                py_ass_parser2805 = s
                Sensor(2805)
                return py_ass_parser2805
        f = file(name=test_support.TESTFN, mode='w')
        lines = ['a\n', 'b\n', 'c\n']
        Sensor(2806)
        try:
            Sensor(2807)
            f.writelines(lines)
            f.close()
            f = CountedInput(test_support.TESTFN)
            Sensor(2808)
            for i, expected in zip(range(1, 5) + [4], lines + 2 * ['']):
                Sensor(2809)
                got = f.readline()
                self.assertEqual(expected, got)
                self.assertEqual(f.lineno, i)
                self.assertEqual(f.ateof, i > len(lines))
            Sensor(2810)
            f.close()
        finally:
            Sensor(2811)
            try:
                Sensor(2812)
                f.close()
            except:
                Sensor(2813)
                pass
            Sensor(2814)
            test_support.unlink(test_support.TESTFN)
        Sensor(2815)

    def test_keywords(self):
        Sensor(2816)
        self.assertEqual(int(x=1), 1)
        self.assertEqual(float(x=2), 2.0)
        self.assertEqual(long(x=3), 3L)
        self.assertEqual(complex(imag=42, real=666), complex(666, 42))
        self.assertEqual(str(object=500), '500')
        self.assertEqual(unicode(string='abc', errors='strict'), u'abc')
        self.assertEqual(tuple(sequence=range(3)), (0, 1, 2))
        self.assertEqual(list(sequence=(0, 1, 2)), range(3))
        Sensor(2817)
        for constructor in (int, float, long, complex, str, unicode, tuple,
            list, file):
            Sensor(2818)
            try:
                Sensor(2819)
                constructor(bogus_keyword_arg=1)
            except TypeError:
                Sensor(2820)
                pass
            else:
                Sensor(2821)
                self.fail(
                    'expected TypeError from bogus keyword argument to %r' %
                    constructor)
        Sensor(2822)

    def test_str_subclass_as_dict_key(self):


        class cistr(str):
            """Sublcass of str that computes __eq__ case-insensitively.

            Also computes a hash code of the string in canonical form.
            """

            def __init__(self, value):
                Sensor(2823)
                self.canonical = value.lower()
                self.hashcode = hash(self.canonical)
                Sensor(2824)

            def __eq__(self, other):
                Sensor(2825)
                if not isinstance(other, cistr):
                    Sensor(2826)
                    other = cistr(other)
                py_ass_parser2827 = self.canonical == other.canonical
                Sensor(2827)
                return py_ass_parser2827

            def __hash__(self):
                py_ass_parser2828 = self.hashcode
                Sensor(2828)
                return py_ass_parser2828
        Sensor(2829)
        self.assertEqual(cistr('ABC'), 'abc')
        self.assertEqual('aBc', cistr('ABC'))
        self.assertEqual(str(cistr('ABC')), 'ABC')
        d = {cistr('one'): 1, cistr('two'): 2, cistr('tHree'): 3}
        self.assertEqual(d[cistr('one')], 1)
        self.assertEqual(d[cistr('tWo')], 2)
        self.assertEqual(d[cistr('THrEE')], 3)
        self.assertIn(cistr('ONe'), d)
        self.assertEqual(d.get(cistr('thrEE')), 3)
        Sensor(2830)

    def test_classic_comparisons(self):


        class classic:
            pass
        Sensor(2831)
        for base in (classic, int, object):


            class C(base):

                def __init__(self, value):
                    Sensor(2832)
                    self.value = int(value)
                    Sensor(2833)

                def __cmp__(self, other):
                    Sensor(2834)
                    if isinstance(other, C):
                        py_ass_parser2835 = cmp(self.value, other.value)
                        Sensor(2835)
                        return py_ass_parser2835
                    Sensor(2836)
                    if isinstance(other, int) or isinstance(other, long):
                        py_ass_parser2837 = cmp(self.value, other)
                        Sensor(2837)
                        return py_ass_parser2837
                    py_ass_parser2838 = NotImplemented
                    Sensor(2838)
                    return py_ass_parser2838
                __hash__ = None
            Sensor(2839)
            c1 = C(1)
            c2 = C(2)
            c3 = C(3)
            self.assertEqual(c1, 1)
            c = {(1): c1, (2): c2, (3): c3}
            Sensor(2840)
            for x in (1, 2, 3):
                Sensor(2841)
                for y in (1, 2, 3):
                    Sensor(2842)
                    self.assertTrue(cmp(c[x], c[y]) == cmp(x, y), 
                        'x=%d, y=%d' % (x, y))
                    Sensor(2843)
                    for op in ('<', '<=', '==', '!=', '>', '>='):
                        Sensor(2844)
                        self.assertTrue(eval('c[x] %s c[y]' % op) == eval(
                            'x %s y' % op), 'x=%d, y=%d' % (x, y))
                    Sensor(2845)
                    self.assertTrue(cmp(c[x], y) == cmp(x, y), 'x=%d, y=%d' %
                        (x, y))
                    self.assertTrue(cmp(x, c[y]) == cmp(x, y), 'x=%d, y=%d' %
                        (x, y))
        Sensor(2846)

    def test_rich_comparisons(self):


        class Z(complex):
            pass
        Sensor(2847)
        z = Z(1)
        self.assertEqual(z, 1 + 0j)
        self.assertEqual(1 + 0j, z)


        class ZZ(complex):

            def __eq__(self, other):
                Sensor(2848)
                try:
                    py_ass_parser2849 = abs(self - other) <= 1e-06
                    Sensor(2849)
                    return py_ass_parser2849
                except:
                    py_ass_parser2850 = NotImplemented
                    Sensor(2850)
                    return py_ass_parser2850
                Sensor(2851)
            __hash__ = None
        zz = ZZ(1.0000003)
        self.assertEqual(zz, 1 + 0j)
        self.assertEqual(1 + 0j, zz)


        class classic:
            pass
        Sensor(2852)
        for base in (classic, int, object, list):


            class C(base):

                def __init__(self, value):
                    Sensor(2853)
                    self.value = int(value)
                    Sensor(2854)

                def __cmp__(self_, other):
                    Sensor(2855)
                    self.fail("shouldn't call __cmp__")
                    Sensor(2856)
                __hash__ = None

                def __eq__(self, other):
                    Sensor(2857)
                    if isinstance(other, C):
                        py_ass_parser2858 = self.value == other.value
                        Sensor(2858)
                        return py_ass_parser2858
                    Sensor(2859)
                    if isinstance(other, int) or isinstance(other, long):
                        py_ass_parser2860 = self.value == other
                        Sensor(2860)
                        return py_ass_parser2860
                    py_ass_parser2861 = NotImplemented
                    Sensor(2861)
                    return py_ass_parser2861

                def __ne__(self, other):
                    Sensor(2862)
                    if isinstance(other, C):
                        py_ass_parser2863 = self.value != other.value
                        Sensor(2863)
                        return py_ass_parser2863
                    Sensor(2864)
                    if isinstance(other, int) or isinstance(other, long):
                        py_ass_parser2865 = self.value != other
                        Sensor(2865)
                        return py_ass_parser2865
                    py_ass_parser2866 = NotImplemented
                    Sensor(2866)
                    return py_ass_parser2866

                def __lt__(self, other):
                    Sensor(2867)
                    if isinstance(other, C):
                        py_ass_parser2868 = self.value < other.value
                        Sensor(2868)
                        return py_ass_parser2868
                    Sensor(2869)
                    if isinstance(other, int) or isinstance(other, long):
                        py_ass_parser2870 = self.value < other
                        Sensor(2870)
                        return py_ass_parser2870
                    py_ass_parser2871 = NotImplemented
                    Sensor(2871)
                    return py_ass_parser2871

                def __le__(self, other):
                    Sensor(2872)
                    if isinstance(other, C):
                        py_ass_parser2873 = self.value <= other.value
                        Sensor(2873)
                        return py_ass_parser2873
                    Sensor(2874)
                    if isinstance(other, int) or isinstance(other, long):
                        py_ass_parser2875 = self.value <= other
                        Sensor(2875)
                        return py_ass_parser2875
                    py_ass_parser2876 = NotImplemented
                    Sensor(2876)
                    return py_ass_parser2876

                def __gt__(self, other):
                    Sensor(2877)
                    if isinstance(other, C):
                        py_ass_parser2878 = self.value > other.value
                        Sensor(2878)
                        return py_ass_parser2878
                    Sensor(2879)
                    if isinstance(other, int) or isinstance(other, long):
                        py_ass_parser2880 = self.value > other
                        Sensor(2880)
                        return py_ass_parser2880
                    py_ass_parser2881 = NotImplemented
                    Sensor(2881)
                    return py_ass_parser2881

                def __ge__(self, other):
                    Sensor(2882)
                    if isinstance(other, C):
                        py_ass_parser2883 = self.value >= other.value
                        Sensor(2883)
                        return py_ass_parser2883
                    Sensor(2884)
                    if isinstance(other, int) or isinstance(other, long):
                        py_ass_parser2885 = self.value >= other
                        Sensor(2885)
                        return py_ass_parser2885
                    py_ass_parser2886 = NotImplemented
                    Sensor(2886)
                    return py_ass_parser2886
            Sensor(2887)
            c1 = C(1)
            c2 = C(2)
            c3 = C(3)
            self.assertEqual(c1, 1)
            c = {(1): c1, (2): c2, (3): c3}
            Sensor(2888)
            for x in (1, 2, 3):
                Sensor(2889)
                for y in (1, 2, 3):
                    Sensor(2890)
                    for op in ('<', '<=', '==', '!=', '>', '>='):
                        Sensor(2891)
                        self.assertTrue(eval('c[x] %s c[y]' % op) == eval(
                            'x %s y' % op), 'x=%d, y=%d' % (x, y))
                        self.assertTrue(eval('c[x] %s y' % op) == eval(
                            'x %s y' % op), 'x=%d, y=%d' % (x, y))
                        self.assertTrue(eval('x %s c[y]' % op) == eval(
                            'x %s y' % op), 'x=%d, y=%d' % (x, y))
        Sensor(2892)

    def test_coercions(self):


        class I(int):
            pass
        Sensor(2893)
        coerce(I(0), 0)
        coerce(0, I(0))


        class L(long):
            pass
        coerce(L(0), 0)
        coerce(L(0), 0L)
        coerce(0, L(0))
        coerce(0L, L(0))


        class F(float):
            pass
        coerce(F(0), 0)
        coerce(F(0), 0L)
        coerce(F(0), 0.0)
        coerce(0, F(0))
        coerce(0L, F(0))
        coerce(0.0, F(0))


        class C(complex):
            pass
        coerce(C(0), 0)
        coerce(C(0), 0L)
        coerce(C(0), 0.0)
        coerce(C(0), 0j)
        coerce(0, C(0))
        coerce(0L, C(0))
        coerce(0.0, C(0))
        coerce(0j, C(0))
        Sensor(2894)

    def test_descrdoc(self):

        def check(descr, what):
            Sensor(2895)
            self.assertEqual(descr.__doc__, what)
            Sensor(2896)
        Sensor(2897)
        check(file.closed, 'True if the file is closed')
        check(file.name, 'file name')
        Sensor(2898)

    def test_doc_descriptor(self):


        class DocDescr(object):

            def __get__(self, object, otype):
                Sensor(2899)
                if object:
                    Sensor(2900)
                    object = object.__class__.__name__ + ' instance'
                Sensor(2901)
                if otype:
                    Sensor(2902)
                    otype = otype.__name__
                py_ass_parser2903 = 'object=%s; type=%s' % (object, otype)
                Sensor(2903)
                return py_ass_parser2903


        class OldClass:
            __doc__ = DocDescr()


        class NewClass(object):
            __doc__ = DocDescr()
        Sensor(2904)
        self.assertEqual(OldClass.__doc__, 'object=None; type=OldClass')
        self.assertEqual(OldClass().__doc__,
            'object=OldClass instance; type=OldClass')
        self.assertEqual(NewClass.__doc__, 'object=None; type=NewClass')
        self.assertEqual(NewClass().__doc__,
            'object=NewClass instance; type=NewClass')
        Sensor(2905)

    def test_set_class(self):


        class C(object):
            pass


        class D(object):
            pass


        class E(object):
            pass


        class F(D, E):
            pass
        Sensor(2906)
        for cls in (C, D, E, F):
            Sensor(2907)
            for cls2 in (C, D, E, F):
                Sensor(2908)
                x = cls()
                x.__class__ = cls2
                self.assertTrue(x.__class__ is cls2)
                x.__class__ = cls
                self.assertTrue(x.__class__ is cls)

        def cant(x, C):
            Sensor(2909)
            try:
                Sensor(2910)
                x.__class__ = C
            except TypeError:
                Sensor(2911)
                pass
            else:
                Sensor(2912)
                self.fail("shouldn't allow %r.__class__ = %r" % (x, C))
            Sensor(2913)
            try:
                Sensor(2914)
                delattr(x, '__class__')
            except (TypeError, AttributeError):
                Sensor(2915)
                pass
            else:
                Sensor(2916)
                self.fail("shouldn't allow del %r.__class__" % x)
            Sensor(2917)
        Sensor(2918)
        cant(C(), list)
        cant(list(), C)
        cant(C(), 1)
        cant(C(), object)
        cant(object(), list)
        cant(list(), object)


        class Int(int):
            __slots__ = []
        cant(2, Int)
        cant(Int(), int)
        cant(True, int)
        cant(2, bool)
        o = object()
        cant(o, type(1))
        cant(o, type(None))
        del o


        class G(object):
            __slots__ = ['a', 'b']


        class H(object):
            __slots__ = ['b', 'a']
        Sensor(2919)
        try:
            Sensor(2920)
            unicode
        except NameError:
            Sensor(2921)


            class I(object):
                __slots__ = ['a', 'b']
        else:
            Sensor(2922)


            class I(object):
                __slots__ = [unicode('a'), unicode('b')]


        class J(object):
            __slots__ = ['c', 'b']


        class K(object):
            __slots__ = ['a', 'b', 'd']


        class L(H):
            __slots__ = ['e']


        class M(I):
            __slots__ = ['e']


        class N(J):
            __slots__ = ['__weakref__']


        class P(J):
            __slots__ = ['__dict__']


        class Q(J):
            pass


        class R(J):
            __slots__ = ['__dict__', '__weakref__']
        Sensor(2923)
        for cls, cls2 in ((G, H), (G, I), (I, H), (Q, R), (R, Q)):
            Sensor(2924)
            x = cls()
            x.a = 1
            x.__class__ = cls2
            self.assertTrue(x.__class__ is cls2, 
                'assigning %r as __class__ for %r silently failed' % (cls2, x))
            self.assertEqual(x.a, 1)
            x.__class__ = cls
            self.assertTrue(x.__class__ is cls, 
                'assigning %r as __class__ for %r silently failed' % (cls, x))
            self.assertEqual(x.a, 1)
        Sensor(2925)
        for cls in (G, J, K, L, M, N, P, R, list, Int):
            Sensor(2926)
            for cls2 in (G, J, K, L, M, N, P, R, list, Int):
                Sensor(2927)
                if cls is cls2:
                    Sensor(2928)
                    continue
                Sensor(2929)
                cant(cls(), cls2)


        class O(object):
            pass


        class A(object):

            def __del__(self):
                Sensor(2930)
                self.__class__ = O
                Sensor(2931)
        Sensor(2932)
        l = [A() for x in range(100)]
        del l
        Sensor(2933)

    def test_set_dict(self):


        class C(object):
            pass
        Sensor(2934)
        a = C()
        a.__dict__ = {'b': 1}
        self.assertEqual(a.b, 1)

        def cant(x, dict):
            Sensor(2935)
            try:
                Sensor(2936)
                x.__dict__ = dict
            except (AttributeError, TypeError):
                Sensor(2937)
                pass
            else:
                Sensor(2938)
                self.fail("shouldn't allow %r.__dict__ = %r" % (x, dict))
            Sensor(2939)
        cant(a, None)
        cant(a, [])
        cant(a, 1)
        del a.__dict__


        class Base(object):
            pass

        def verify_dict_readonly(x):
            Sensor(2940)
            """
            x has to be an instance of a class inheriting from Base.
            """
            cant(x, {})
            Sensor(2941)
            try:
                Sensor(2942)
                del x.__dict__
            except (AttributeError, TypeError):
                Sensor(2943)
                pass
            else:
                Sensor(2944)
                self.fail("shouldn't allow del %r.__dict__" % x)
            Sensor(2945)
            dict_descr = Base.__dict__['__dict__']
            Sensor(2946)
            try:
                Sensor(2947)
                dict_descr.__set__(x, {})
            except (AttributeError, TypeError):
                Sensor(2948)
                pass
            else:
                Sensor(2949)
                self.fail("dict_descr allowed access to %r's dict" % x)
            Sensor(2950)


        class Meta1(type, Base):
            pass


        class Meta2(Base, type):
            pass


        class D(object):
            __metaclass__ = Meta1


        class E(object):
            __metaclass__ = Meta2
        Sensor(2951)
        for cls in (C, D, E):
            Sensor(2952)
            verify_dict_readonly(cls)
            class_dict = cls.__dict__
            Sensor(2953)
            try:
                Sensor(2954)
                class_dict['spam'] = 'eggs'
            except TypeError:
                Sensor(2955)
                pass
            else:
                Sensor(2956)
                self.fail("%r's __dict__ can be modified" % cls)


        class Module1(types.ModuleType, Base):
            pass


        class Module2(Base, types.ModuleType):
            pass
        Sensor(2957)
        for ModuleType in (Module1, Module2):
            Sensor(2958)
            mod = ModuleType('spam')
            verify_dict_readonly(mod)
            mod.__dict__['spam'] = 'eggs'

        def can_delete_dict(e):
            Sensor(2959)
            try:
                Sensor(2960)
                del e.__dict__
            except (TypeError, AttributeError):
                py_ass_parser2961 = False
                Sensor(2961)
                return py_ass_parser2961
            else:
                py_ass_parser2962 = True
                Sensor(2962)
                return py_ass_parser2962
            Sensor(2963)


        class Exception1(Exception, Base):
            pass


        class Exception2(Base, Exception):
            pass
        Sensor(2964)
        for ExceptionType in (Exception, Exception1, Exception2):
            Sensor(2965)
            e = ExceptionType()
            e.__dict__ = {'a': 1}
            self.assertEqual(e.a, 1)
            self.assertEqual(can_delete_dict(e), can_delete_dict(ValueError()))
        Sensor(2966)

    def test_pickles(self):
        import pickle, cPickle

        def sorteditems(d):
            Sensor(2967)
            L = d.items()
            L.sort()
            py_ass_parser2968 = L
            Sensor(2968)
            return py_ass_parser2968
        global C


        class C(object):

            def __init__(self, a, b):
                Sensor(2969)
                super(C, self).__init__()
                self.a = a
                self.b = b
                Sensor(2970)

            def __repr__(self):
                py_ass_parser2971 = 'C(%r, %r)' % (self.a, self.b)
                Sensor(2971)
                return py_ass_parser2971
        global C1


        class C1(list):

            def __new__(cls, a, b):
                py_ass_parser2972 = super(C1, cls).__new__(cls)
                Sensor(2972)
                return py_ass_parser2972

            def __getnewargs__(self):
                py_ass_parser2973 = self.a, self.b
                Sensor(2973)
                return py_ass_parser2973

            def __init__(self, a, b):
                Sensor(2974)
                self.a = a
                self.b = b
                Sensor(2975)

            def __repr__(self):
                py_ass_parser2976 = 'C1(%r, %r)<%r>' % (self.a, self.b,
                    list(self))
                Sensor(2976)
                return py_ass_parser2976
        global C2


        class C2(int):

            def __new__(cls, a, b, val=0):
                py_ass_parser2977 = super(C2, cls).__new__(cls, val)
                Sensor(2977)
                return py_ass_parser2977

            def __getnewargs__(self):
                py_ass_parser2978 = self.a, self.b, int(self)
                Sensor(2978)
                return py_ass_parser2978

            def __init__(self, a, b, val=0):
                Sensor(2979)
                self.a = a
                self.b = b
                Sensor(2980)

            def __repr__(self):
                py_ass_parser2981 = 'C2(%r, %r)<%r>' % (self.a, self.b, int
                    (self))
                Sensor(2981)
                return py_ass_parser2981
        global C3


        class C3(object):

            def __init__(self, foo):
                Sensor(2982)
                self.foo = foo
                Sensor(2983)

            def __getstate__(self):
                py_ass_parser2984 = self.foo
                Sensor(2984)
                return py_ass_parser2984

            def __setstate__(self, foo):
                Sensor(2985)
                self.foo = foo
                Sensor(2986)
        global C4classic, C4


        class C4classic:
            pass


        class C4(C4classic, object):
            pass
        Sensor(2987)
        for p in (pickle, cPickle):
            Sensor(2988)
            for bin in (0, 1):
                Sensor(2989)
                for cls in (C, C1, C2):
                    Sensor(2990)
                    s = p.dumps(cls, bin)
                    cls2 = p.loads(s)
                    self.assertTrue(cls2 is cls)
                Sensor(2991)
                a = C1(1, 2)
                a.append(42)
                a.append(24)
                b = C2('hello', 'world', 42)
                s = p.dumps((a, b), bin)
                x, y = p.loads(s)
                self.assertEqual(x.__class__, a.__class__)
                self.assertEqual(sorteditems(x.__dict__), sorteditems(a.
                    __dict__))
                self.assertEqual(y.__class__, b.__class__)
                self.assertEqual(sorteditems(y.__dict__), sorteditems(b.
                    __dict__))
                self.assertEqual(repr(x), repr(a))
                self.assertEqual(repr(y), repr(b))
                u = C3(42)
                s = p.dumps(u, bin)
                v = p.loads(s)
                self.assertEqual(u.__class__, v.__class__)
                self.assertEqual(u.foo, v.foo)
                u = C4()
                u.foo = 42
                s = p.dumps(u, bin)
                v = p.loads(s)
                self.assertEqual(u.__class__, v.__class__)
                self.assertEqual(u.foo, v.foo)
        import copy
        Sensor(2992)
        for cls in (C, C1, C2):
            Sensor(2993)
            cls2 = copy.deepcopy(cls)
            self.assertTrue(cls2 is cls)
        Sensor(2994)
        a = C1(1, 2)
        a.append(42)
        a.append(24)
        b = C2('hello', 'world', 42)
        x, y = copy.deepcopy((a, b))
        self.assertEqual(x.__class__, a.__class__)
        self.assertEqual(sorteditems(x.__dict__), sorteditems(a.__dict__))
        self.assertEqual(y.__class__, b.__class__)
        self.assertEqual(sorteditems(y.__dict__), sorteditems(b.__dict__))
        self.assertEqual(repr(x), repr(a))
        self.assertEqual(repr(y), repr(b))
        Sensor(2995)

    def test_pickle_slots(self):
        import pickle, cPickle
        global B, C, D, E


        class B(object):
            pass
        Sensor(2996)
        for base in [object, B]:


            class C(base):
                __slots__ = ['a']


            class D(C):
                pass
            Sensor(2997)
            try:
                Sensor(2998)
                pickle.dumps(C())
            except TypeError:
                Sensor(2999)
                pass
            else:
                Sensor(3000)
                self.fail('should fail: pickle C instance - %s' % base)
            Sensor(3001)
            try:
                Sensor(3002)
                cPickle.dumps(C())
            except TypeError:
                Sensor(3003)
                pass
            else:
                Sensor(3004)
                self.fail('should fail: cPickle C instance - %s' % base)
            Sensor(3005)
            try:
                Sensor(3006)
                pickle.dumps(C())
            except TypeError:
                Sensor(3007)
                pass
            else:
                Sensor(3008)
                self.fail('should fail: pickle D instance - %s' % base)
            Sensor(3009)
            try:
                Sensor(3010)
                cPickle.dumps(D())
            except TypeError:
                Sensor(3011)
                pass
            else:
                Sensor(3012)
                self.fail('should fail: cPickle D instance - %s' % base)


            class C(base):
                __slots__ = ['a']

                def __getstate__(self):
                    Sensor(3013)
                    try:
                        Sensor(3014)
                        d = self.__dict__.copy()
                    except AttributeError:
                        Sensor(3015)
                        d = {}
                    Sensor(3016)
                    for cls in self.__class__.__mro__:
                        Sensor(3017)
                        for sn in cls.__dict__.get('__slots__', ()):
                            Sensor(3018)
                            try:
                                Sensor(3019)
                                d[sn] = getattr(self, sn)
                            except AttributeError:
                                Sensor(3020)
                                pass
                    py_ass_parser3021 = d
                    Sensor(3021)
                    return py_ass_parser3021

                def __setstate__(self, d):
                    Sensor(3022)
                    for k, v in d.items():
                        Sensor(3023)
                        setattr(self, k, v)
                    Sensor(3024)


            class D(C):
                pass
            Sensor(3025)
            x = C()
            y = pickle.loads(pickle.dumps(x))
            self.assertEqual(hasattr(y, 'a'), 0)
            y = cPickle.loads(cPickle.dumps(x))
            self.assertEqual(hasattr(y, 'a'), 0)
            x.a = 42
            y = pickle.loads(pickle.dumps(x))
            self.assertEqual(y.a, 42)
            y = cPickle.loads(cPickle.dumps(x))
            self.assertEqual(y.a, 42)
            x = D()
            x.a = 42
            x.b = 100
            y = pickle.loads(pickle.dumps(x))
            self.assertEqual(y.a + y.b, 142)
            y = cPickle.loads(cPickle.dumps(x))
            self.assertEqual(y.a + y.b, 142)


            class E(C):
                __slots__ = ['b']
            x = E()
            x.a = 42
            x.b = 'foo'
            y = pickle.loads(pickle.dumps(x))
            self.assertEqual(y.a, x.a)
            self.assertEqual(y.b, x.b)
            y = cPickle.loads(cPickle.dumps(x))
            self.assertEqual(y.a, x.a)
            self.assertEqual(y.b, x.b)
        Sensor(3026)

    def test_binary_operator_override(self):


        class I(int):

            def __repr__(self):
                py_ass_parser3027 = 'I(%r)' % int(self)
                Sensor(3027)
                return py_ass_parser3027

            def __add__(self, other):
                py_ass_parser3028 = I(int(self) + int(other))
                Sensor(3028)
                return py_ass_parser3028
            __radd__ = __add__

            def __pow__(self, other, mod=None):
                Sensor(3029)
                if mod is None:
                    py_ass_parser3030 = I(pow(int(self), int(other)))
                    Sensor(3030)
                    return py_ass_parser3030
                else:
                    py_ass_parser3031 = I(pow(int(self), int(other), int(mod)))
                    Sensor(3031)
                    return py_ass_parser3031
                Sensor(3032)

            def __rpow__(self, other, mod=None):
                Sensor(3033)
                if mod is None:
                    py_ass_parser3034 = I(pow(int(other), int(self), mod))
                    Sensor(3034)
                    return py_ass_parser3034
                else:
                    py_ass_parser3035 = I(pow(int(other), int(self), int(mod)))
                    Sensor(3035)
                    return py_ass_parser3035
                Sensor(3036)
        Sensor(3037)
        self.assertEqual(repr(I(1) + I(2)), 'I(3)')
        self.assertEqual(repr(I(1) + 2), 'I(3)')
        self.assertEqual(repr(1 + I(2)), 'I(3)')
        self.assertEqual(repr(I(2) ** I(3)), 'I(8)')
        self.assertEqual(repr(2 ** I(3)), 'I(8)')
        self.assertEqual(repr(I(2) ** 3), 'I(8)')
        self.assertEqual(repr(pow(I(2), I(3), I(5))), 'I(3)')


        class S(str):

            def __eq__(self, other):
                py_ass_parser3038 = self.lower() == other.lower()
                Sensor(3038)
                return py_ass_parser3038
            __hash__ = None
        Sensor(3039)

    def test_subclass_propagation(self):


        class A(object):
            pass


        class B(A):
            pass


        class C(A):
            pass


        class D(B, C):
            pass
        Sensor(3040)
        d = D()
        orig_hash = hash(d)
        A.__hash__ = lambda self: 42
        self.assertEqual(hash(d), 42)
        C.__hash__ = lambda self: 314
        self.assertEqual(hash(d), 314)
        B.__hash__ = lambda self: 144
        self.assertEqual(hash(d), 144)
        D.__hash__ = lambda self: 100
        self.assertEqual(hash(d), 100)
        D.__hash__ = None
        self.assertRaises(TypeError, hash, d)
        del D.__hash__
        self.assertEqual(hash(d), 144)
        B.__hash__ = None
        self.assertRaises(TypeError, hash, d)
        del B.__hash__
        self.assertEqual(hash(d), 314)
        C.__hash__ = None
        self.assertRaises(TypeError, hash, d)
        del C.__hash__
        self.assertEqual(hash(d), 42)
        A.__hash__ = None
        self.assertRaises(TypeError, hash, d)
        del A.__hash__
        self.assertEqual(hash(d), orig_hash)
        d.foo = 42
        d.bar = 42
        self.assertEqual(d.foo, 42)
        self.assertEqual(d.bar, 42)

        def __getattribute__(self, name):
            Sensor(3041)
            if name == 'foo':
                py_ass_parser3042 = 24
                Sensor(3042)
                return py_ass_parser3042
            py_ass_parser3043 = object.__getattribute__(self, name)
            Sensor(3043)
            return py_ass_parser3043
        A.__getattribute__ = __getattribute__
        self.assertEqual(d.foo, 24)
        self.assertEqual(d.bar, 42)

        def __getattr__(self, name):
            Sensor(3044)
            if name in ('spam', 'foo', 'bar'):
                py_ass_parser3045 = 'hello'
                Sensor(3045)
                return py_ass_parser3045
            Sensor(3046)
            raise AttributeError, name
        B.__getattr__ = __getattr__
        self.assertEqual(d.spam, 'hello')
        self.assertEqual(d.foo, 24)
        self.assertEqual(d.bar, 42)
        del A.__getattribute__
        self.assertEqual(d.foo, 42)
        del d.foo
        self.assertEqual(d.foo, 'hello')
        self.assertEqual(d.bar, 42)
        del B.__getattr__
        Sensor(3047)
        try:
            Sensor(3048)
            d.foo
        except AttributeError:
            Sensor(3049)
            pass
        else:
            Sensor(3050)
            self.fail('d.foo should be undefined now')


        class A(object):
            pass


        class B(A):
            pass
        Sensor(3051)
        del B
        test_support.gc_collect()
        A.__setitem__ = lambda *a: None
        Sensor(3052)

    def test_buffer_inheritance(self):
        import binascii


        class MyStr(str):
            pass
        Sensor(3053)
        base = 'abc'
        m = MyStr(base)
        self.assertEqual(binascii.b2a_hex(m), binascii.b2a_hex(base))


        class MyUni(unicode):
            pass
        base = u'abc'
        m = MyUni(base)
        self.assertEqual(binascii.b2a_hex(m), binascii.b2a_hex(base))


        class MyInt(int):
            pass
        m = MyInt(42)
        Sensor(3054)
        try:
            Sensor(3055)
            binascii.b2a_hex(m)
            self.fail('subclass of int should not have a buffer interface')
        except TypeError:
            Sensor(3056)
            pass
        Sensor(3057)

    def test_str_of_str_subclass(self):
        import binascii
        import cStringIO


        class octetstring(str):

            def __str__(self):
                py_ass_parser3058 = binascii.b2a_hex(self)
                Sensor(3058)
                return py_ass_parser3058

            def __repr__(self):
                py_ass_parser3059 = self + ' repr'
                Sensor(3059)
                return py_ass_parser3059
        Sensor(3060)
        o = octetstring('A')
        self.assertEqual(type(o), octetstring)
        self.assertEqual(type(str(o)), str)
        self.assertEqual(type(repr(o)), str)
        self.assertEqual(ord(o), 65)
        self.assertEqual(str(o), '41')
        self.assertEqual(repr(o), 'A repr')
        self.assertEqual(o.__str__(), '41')
        self.assertEqual(o.__repr__(), 'A repr')
        capture = cStringIO.StringIO()
        print  >> capture, o
        print  >> capture, str(o)
        self.assertEqual(capture.getvalue(), '41\n41\n')
        capture.close()
        Sensor(3061)

    def test_keyword_arguments(self):

        def f(a):
            py_ass_parser3062 = a
            Sensor(3062)
            return py_ass_parser3062
        Sensor(3063)
        self.assertEqual(f.__call__(a=42), 42)
        a = []
        list.__init__(a, sequence=[0, 1, 2])
        self.assertEqual(a, [0, 1, 2])
        Sensor(3064)

    def test_recursive_call(self):


        class A(object):
            pass
        Sensor(3065)
        A.__call__ = A()
        Sensor(3066)
        try:
            Sensor(3067)
            A()()
        except RuntimeError:
            Sensor(3068)
            pass
        else:
            Sensor(3069)
            self.fail('Recursion limit should have been reached for __call__()'
                )
        Sensor(3070)

    def test_delete_hook(self):
        Sensor(3071)
        log = []


        class C(object):

            def __del__(self):
                Sensor(3072)
                log.append(1)
                Sensor(3073)
        c = C()
        self.assertEqual(log, [])
        del c
        test_support.gc_collect()
        self.assertEqual(log, [1])


        class D(object):
            pass
        d = D()
        Sensor(3074)
        try:
            Sensor(3075)
            del d[0]
        except TypeError:
            Sensor(3076)
            pass
        else:
            Sensor(3077)
            self.fail("invalid del() didn't raise TypeError")
        Sensor(3078)

    def test_hash_inheritance(self):


        class mydict(dict):
            pass
        Sensor(3079)
        d = mydict()
        Sensor(3080)
        try:
            Sensor(3081)
            hash(d)
        except TypeError:
            Sensor(3082)
            pass
        else:
            Sensor(3083)
            self.fail('hash() of dict subclass should fail')


        class mylist(list):
            pass
        Sensor(3084)
        d = mylist()
        Sensor(3085)
        try:
            Sensor(3086)
            hash(d)
        except TypeError:
            Sensor(3087)
            pass
        else:
            Sensor(3088)
            self.fail('hash() of list subclass should fail')
        Sensor(3089)

    def test_str_operations(self):
        Sensor(3090)
        try:
            Sensor(3091)
            'a' + 5
        except TypeError:
            Sensor(3092)
            pass
        else:
            Sensor(3093)
            self.fail("'' + 5 doesn't raise TypeError")
        Sensor(3094)
        try:
            Sensor(3095)
            """""".split('')
        except ValueError:
            Sensor(3096)
            pass
        else:
            Sensor(3097)
            self.fail("''.split('') doesn't raise ValueError")
        Sensor(3098)
        try:
            Sensor(3099)
            """""".join([0])
        except TypeError:
            Sensor(3100)
            pass
        else:
            Sensor(3101)
            self.fail("''.join([0]) doesn't raise TypeError")
        Sensor(3102)
        try:
            Sensor(3103)
            """""".rindex('5')
        except ValueError:
            Sensor(3104)
            pass
        else:
            Sensor(3105)
            self.fail("''.rindex('5') doesn't raise ValueError")
        Sensor(3106)
        try:
            Sensor(3107)
            '%(n)s' % None
        except TypeError:
            Sensor(3108)
            pass
        else:
            Sensor(3109)
            self.fail("'%(n)s' % None doesn't raise TypeError")
        Sensor(3110)
        try:
            Sensor(3111)
            '%(n' % {}
        except ValueError:
            Sensor(3112)
            pass
        else:
            Sensor(3113)
            self.fail("'%(n' % {} '' doesn't raise ValueError")
        Sensor(3114)
        try:
            Sensor(3115)
            '%*s' % 'abc'
        except TypeError:
            Sensor(3116)
            pass
        else:
            Sensor(3117)
            self.fail("'%*s' % ('abc') doesn't raise TypeError")
        Sensor(3118)
        try:
            Sensor(3119)
            '%*.*s' % ('abc', 5)
        except TypeError:
            Sensor(3120)
            pass
        else:
            Sensor(3121)
            self.fail("'%*.*s' % ('abc', 5) doesn't raise TypeError")
        Sensor(3122)
        try:
            Sensor(3123)
            '%s' % (1, 2)
        except TypeError:
            Sensor(3124)
            pass
        else:
            Sensor(3125)
            self.fail("'%s' % (1, 2) doesn't raise TypeError")
        Sensor(3126)
        try:
            Sensor(3127)
            '%' % None
        except ValueError:
            Sensor(3128)
            pass
        else:
            Sensor(3129)
            self.fail("'%' % None doesn't raise ValueError")
        Sensor(3130)
        self.assertEqual('534253'.isdigit(), 1)
        self.assertEqual('534253x'.isdigit(), 0)
        self.assertEqual('%c' % 5, '\x05')
        self.assertEqual('%c' % '5', '5')
        Sensor(3131)

    def test_deepcopy_recursive(self):


        class Node:
            pass
        Sensor(3132)
        a = Node()
        b = Node()
        a.b = b
        b.a = a
        z = deepcopy(a)
        Sensor(3133)

    def test_unintialized_modules(self):
        from types import ModuleType as M
        Sensor(3134)
        m = M.__new__(M)
        str(m)
        self.assertEqual(hasattr(m, '__name__'), 0)
        self.assertEqual(hasattr(m, '__file__'), 0)
        self.assertEqual(hasattr(m, 'foo'), 0)
        self.assertFalse(m.__dict__)
        m.foo = 1
        self.assertEqual(m.__dict__, {'foo': 1})
        Sensor(3135)

    def test_funny_new(self):


        class C(object):

            def __new__(cls, arg):
                Sensor(3136)
                if isinstance(arg, str):
                    py_ass_parser3137 = [1, 2, 3]
                    Sensor(3137)
                    return py_ass_parser3137
                else:
                    Sensor(3138)
                    if isinstance(arg, int):
                        py_ass_parser3139 = object.__new__(D)
                        Sensor(3139)
                        return py_ass_parser3139
                    else:
                        py_ass_parser3140 = object.__new__(cls)
                        Sensor(3140)
                        return py_ass_parser3140
                Sensor(3141)


        class D(C):

            def __init__(self, arg):
                Sensor(3142)
                self.foo = arg
                Sensor(3143)
        Sensor(3144)
        self.assertEqual(C('1'), [1, 2, 3])
        self.assertEqual(D('1'), [1, 2, 3])
        d = D(None)
        self.assertEqual(d.foo, None)
        d = C(1)
        self.assertEqual(isinstance(d, D), True)
        self.assertEqual(d.foo, 1)
        d = D(1)
        self.assertEqual(isinstance(d, D), True)
        self.assertEqual(d.foo, 1)
        Sensor(3145)

    def test_imul_bug(self):


        class C(object):

            def __imul__(self, other):
                py_ass_parser3146 = self, other
                Sensor(3146)
                return py_ass_parser3146
        Sensor(3147)
        x = C()
        y = x
        y *= 1.0
        self.assertEqual(y, (x, 1.0))
        y = x
        y *= 2
        self.assertEqual(y, (x, 2))
        y = x
        y *= 3L
        self.assertEqual(y, (x, 3L))
        y = x
        y *= 1L << 100
        self.assertEqual(y, (x, 1L << 100))
        y = x
        y *= None
        self.assertEqual(y, (x, None))
        y = x
        y *= 'foo'
        self.assertEqual(y, (x, 'foo'))
        Sensor(3148)

    def test_copy_setstate(self):
        import copy


        class C(object):

            def __init__(self, foo=None):
                Sensor(3149)
                self.foo = foo
                self.__foo = foo
                Sensor(3150)

            def setfoo(self, foo=None):
                Sensor(3151)
                self.foo = foo
                Sensor(3152)

            def getfoo(self):
                py_ass_parser3153 = self.__foo
                Sensor(3153)
                return py_ass_parser3153

            def __getstate__(self):
                py_ass_parser3154 = [self.foo]
                Sensor(3154)
                return py_ass_parser3154

            def __setstate__(self_, lst):
                Sensor(3155)
                self.assertEqual(len(lst), 1)
                self_.__foo = self_.foo = lst[0]
                Sensor(3156)
        Sensor(3157)
        a = C(42)
        a.setfoo(24)
        self.assertEqual(a.foo, 24)
        self.assertEqual(a.getfoo(), 42)
        b = copy.copy(a)
        self.assertEqual(b.foo, 24)
        self.assertEqual(b.getfoo(), 24)
        b = copy.deepcopy(a)
        self.assertEqual(b.foo, 24)
        self.assertEqual(b.getfoo(), 24)
        Sensor(3158)

    def test_slices(self):
        Sensor(3159)
        self.assertEqual('hello'[:4], 'hell')
        self.assertEqual('hello'[slice(4)], 'hell')
        self.assertEqual(str.__getitem__('hello', slice(4)), 'hell')


        class S(str):

            def __getitem__(self, x):
                py_ass_parser3160 = str.__getitem__(self, x)
                Sensor(3160)
                return py_ass_parser3160
        self.assertEqual(S('hello')[:4], 'hell')
        self.assertEqual(S('hello')[slice(4)], 'hell')
        self.assertEqual(S('hello').__getitem__(slice(4)), 'hell')
        self.assertEqual((1, 2, 3)[:2], (1, 2))
        self.assertEqual((1, 2, 3)[slice(2)], (1, 2))
        self.assertEqual(tuple.__getitem__((1, 2, 3), slice(2)), (1, 2))


        class T(tuple):

            def __getitem__(self, x):
                py_ass_parser3161 = tuple.__getitem__(self, x)
                Sensor(3161)
                return py_ass_parser3161
        self.assertEqual(T((1, 2, 3))[:2], (1, 2))
        self.assertEqual(T((1, 2, 3))[slice(2)], (1, 2))
        self.assertEqual(T((1, 2, 3)).__getitem__(slice(2)), (1, 2))
        self.assertEqual([1, 2, 3][:2], [1, 2])
        self.assertEqual([1, 2, 3][slice(2)], [1, 2])
        self.assertEqual(list.__getitem__([1, 2, 3], slice(2)), [1, 2])


        class L(list):

            def __getitem__(self, x):
                py_ass_parser3162 = list.__getitem__(self, x)
                Sensor(3162)
                return py_ass_parser3162
        self.assertEqual(L([1, 2, 3])[:2], [1, 2])
        self.assertEqual(L([1, 2, 3])[slice(2)], [1, 2])
        self.assertEqual(L([1, 2, 3]).__getitem__(slice(2)), [1, 2])
        a = L([1, 2, 3])
        a[slice(1, 3)] = [3, 2]
        self.assertEqual(a, [1, 3, 2])
        a[slice(0, 2, 1)] = [3, 1]
        self.assertEqual(a, [3, 1, 2])
        a.__setitem__(slice(1, 3), [2, 1])
        self.assertEqual(a, [3, 2, 1])
        a.__setitem__(slice(0, 2, 1), [2, 3])
        self.assertEqual(a, [2, 3, 1])
        Sensor(3163)

    def test_subtype_resurrection(self):


        class C(object):
            container = []

            def __del__(self):
                Sensor(3164)
                C.container.append(self)
                Sensor(3165)
        Sensor(3166)
        c = C()
        c.attr = 42
        del c
        test_support.gc_collect()
        self.assertEqual(len(C.container), 1)
        del C.container[-1]
        Sensor(3167)
        if test_support.check_impl_detail():
            Sensor(3168)
            test_support.gc_collect()
            self.assertEqual(len(C.container), 1)
            self.assertEqual(C.container[-1].attr, 42)
        Sensor(3169)
        del C.__del__
        Sensor(3170)

    def test_slots_trash(self):


        class trash(object):
            __slots__ = ['x']

            def __init__(self, x):
                Sensor(3171)
                self.x = x
                Sensor(3172)
        Sensor(3173)
        o = None
        Sensor(3174)
        for i in xrange(50000):
            Sensor(3175)
            o = trash(o)
        Sensor(3176)
        del o
        Sensor(3177)

    def test_slots_multiple_inheritance(self):


        class A(object):
            __slots__ = ()


        class B(object):
            pass


        class C(A, B):
            __slots__ = ()
        Sensor(3178)
        if test_support.check_impl_detail():
            Sensor(3179)
            self.assertEqual(C.__basicsize__, B.__basicsize__)
        Sensor(3180)
        self.assertTrue(hasattr(C, '__dict__'))
        self.assertTrue(hasattr(C, '__weakref__'))
        C().x = 2
        Sensor(3181)

    def test_rmul(self):


        class C(object):

            def __mul__(self, other):
                py_ass_parser3182 = 'mul'
                Sensor(3182)
                return py_ass_parser3182

            def __rmul__(self, other):
                py_ass_parser3183 = 'rmul'
                Sensor(3183)
                return py_ass_parser3183
        Sensor(3184)
        a = C()
        self.assertEqual(a * 2, 'mul')
        self.assertEqual(a * 2.2, 'mul')
        self.assertEqual(2 * a, 'rmul')
        self.assertEqual(2.2 * a, 'rmul')
        Sensor(3185)

    def test_ipow(self):


        class C(object):

            def __ipow__(self, other):
                Sensor(3186)
                pass
                Sensor(3187)
        Sensor(3188)
        a = C()
        a **= 2
        Sensor(3189)

    def test_mutable_bases(self):


        class C(object):
            pass


        class C2(object):

            def __getattribute__(self, attr):
                Sensor(3190)
                if attr == 'a':
                    py_ass_parser3191 = 2
                    Sensor(3191)
                    return py_ass_parser3191
                else:
                    py_ass_parser3192 = super(C2, self).__getattribute__(attr)
                    Sensor(3192)
                    return py_ass_parser3192
                Sensor(3193)

            def meth(self):
                py_ass_parser3194 = 1
                Sensor(3194)
                return py_ass_parser3194


        class D(C):
            pass


        class E(D):
            pass
        Sensor(3195)
        d = D()
        e = E()
        D.__bases__ = C,
        D.__bases__ = C2,
        self.assertEqual(d.meth(), 1)
        self.assertEqual(e.meth(), 1)
        self.assertEqual(d.a, 2)
        self.assertEqual(e.a, 2)
        self.assertEqual(C2.__subclasses__(), [D])
        Sensor(3196)
        try:
            Sensor(3197)
            del D.__bases__
        except (TypeError, AttributeError):
            Sensor(3198)
            pass
        else:
            Sensor(3199)
            self.fail("shouldn't be able to delete .__bases__")
        Sensor(3200)
        try:
            Sensor(3201)
            D.__bases__ = ()
        except TypeError as msg:
            Sensor(3202)
            if str(msg) == "a new-style class can't have only classic bases":
                Sensor(3203)
                self.fail('wrong error message for .__bases__ = ()')
        else:
            Sensor(3204)
            self.fail("shouldn't be able to set .__bases__ to ()")
        Sensor(3205)
        try:
            Sensor(3206)
            D.__bases__ = D,
        except TypeError:
            Sensor(3207)
            pass
        else:
            Sensor(3208)
            self.fail("shouldn't be able to create inheritance cycles")
        Sensor(3209)
        try:
            Sensor(3210)
            D.__bases__ = C, C
        except TypeError:
            Sensor(3211)
            pass
        else:
            Sensor(3212)
            self.fail("didn't detect repeated base classes")
        Sensor(3213)
        try:
            Sensor(3214)
            D.__bases__ = E,
        except TypeError:
            Sensor(3215)
            pass
        else:
            Sensor(3216)
            self.fail("shouldn't be able to create inheritance cycles")


        class Classic:

            def meth2(self):
                py_ass_parser3217 = 3
                Sensor(3217)
                return py_ass_parser3217
        Sensor(3218)
        D.__bases__ = C, Classic
        self.assertEqual(d.meth2(), 3)
        self.assertEqual(e.meth2(), 3)
        Sensor(3219)
        try:
            Sensor(3220)
            d.a
        except AttributeError:
            Sensor(3221)
            pass
        else:
            Sensor(3222)
            self.fail('attribute should have vanished')
        Sensor(3223)
        try:
            Sensor(3224)
            D.__bases__ = Classic,
        except TypeError:
            Sensor(3225)
            pass
        else:
            Sensor(3226)
            self.fail('new-style class must have a new-style base')
        Sensor(3227)

    def test_builtin_bases(self):
        Sensor(3228)
        builtin_types = [tp for tp in __builtin__.__dict__.itervalues() if
            isinstance(tp, type)]
        Sensor(3229)
        for tp in builtin_types:
            Sensor(3230)
            object.__getattribute__(tp, '__bases__')
            Sensor(3231)
            if tp is not object:
                Sensor(3232)
                self.assertEqual(len(tp.__bases__), 1, tp)


        class L(list):
            pass


        class C(object):
            pass


        class D(C):
            pass
        Sensor(3233)
        try:
            Sensor(3234)
            L.__bases__ = dict,
        except TypeError:
            Sensor(3235)
            pass
        else:
            Sensor(3236)
            self.fail("shouldn't turn list subclass into dict subclass")
        Sensor(3237)
        try:
            Sensor(3238)
            list.__bases__ = dict,
        except TypeError:
            Sensor(3239)
            pass
        else:
            Sensor(3240)
            self.fail("shouldn't be able to assign to list.__bases__")
        Sensor(3241)
        try:
            Sensor(3242)
            D.__bases__ = C, list
        except TypeError:
            Sensor(3243)
            pass
        else:
            Sensor(3244)
            assert 0, 'best_base calculation found wanting'
        Sensor(3245)

    def test_mutable_bases_with_failing_mro(self):


        class WorkOnce(type):

            def __new__(self, name, bases, ns):
                Sensor(3246)
                self.flag = 0
                py_ass_parser3247 = super(WorkOnce, self).__new__(WorkOnce,
                    name, bases, ns)
                Sensor(3247)
                return py_ass_parser3247

            def mro(self):
                Sensor(3248)
                if self.flag > 0:
                    Sensor(3249)
                    raise RuntimeError, 'bozo'
                else:
                    Sensor(3250)
                    self.flag += 1
                    py_ass_parser3251 = type.mro(self)
                    Sensor(3251)
                    return py_ass_parser3251
                Sensor(3252)


        class WorkAlways(type):

            def mro(self):
                py_ass_parser3253 = type.mro(self)
                Sensor(3253)
                return py_ass_parser3253


        class C(object):
            pass


        class C2(object):
            pass


        class D(C):
            pass


        class E(D):
            pass


        class F(D):
            __metaclass__ = WorkOnce


        class G(D):
            __metaclass__ = WorkAlways
        Sensor(3254)
        E_mro_before = E.__mro__
        D_mro_before = D.__mro__
        Sensor(3255)
        try:
            Sensor(3256)
            D.__bases__ = C2,
        except RuntimeError:
            Sensor(3257)
            self.assertEqual(E.__mro__, E_mro_before)
            self.assertEqual(D.__mro__, D_mro_before)
        else:
            Sensor(3258)
            self.fail('exception not propagated')
        Sensor(3259)

    def test_mutable_bases_catch_mro_conflict(self):


        class A(object):
            pass


        class B(object):
            pass


        class C(A, B):
            pass


        class D(A, B):
            pass


        class E(C, D):
            pass
        Sensor(3260)
        try:
            Sensor(3261)
            C.__bases__ = B, A
        except TypeError:
            Sensor(3262)
            pass
        else:
            Sensor(3263)
            self.fail("didn't catch MRO conflict")
        Sensor(3264)

    def test_mutable_names(self):


        class C(object):
            pass
        Sensor(3265)
        mod = C.__module__
        C.__name__ = 'D'
        self.assertEqual((C.__module__, C.__name__), (mod, 'D'))
        C.__name__ = 'D.E'
        self.assertEqual((C.__module__, C.__name__), (mod, 'D.E'))
        Sensor(3266)

    def test_evil_type_name(self):


        class Nasty(str):

            def __del__(self):
                Sensor(3267)
                C.__name__ = 'other'
                Sensor(3268)


        class C(object):
            pass
        Sensor(3269)
        C.__name__ = Nasty('abc')
        C.__name__ = 'normal'
        Sensor(3270)

    def test_subclass_right_op(self):


        class B(int):

            def __floordiv__(self, other):
                py_ass_parser3271 = 'B.__floordiv__'
                Sensor(3271)
                return py_ass_parser3271

            def __rfloordiv__(self, other):
                py_ass_parser3272 = 'B.__rfloordiv__'
                Sensor(3272)
                return py_ass_parser3272
        Sensor(3273)
        self.assertEqual(B(1) // 1, 'B.__floordiv__')
        self.assertEqual(1 // B(1), 'B.__rfloordiv__')


        class C(object):

            def __floordiv__(self, other):
                py_ass_parser3274 = 'C.__floordiv__'
                Sensor(3274)
                return py_ass_parser3274

            def __rfloordiv__(self, other):
                py_ass_parser3275 = 'C.__rfloordiv__'
                Sensor(3275)
                return py_ass_parser3275
        self.assertEqual(C() // 1, 'C.__floordiv__')
        self.assertEqual(1 // C(), 'C.__rfloordiv__')


        class D(C):

            def __floordiv__(self, other):
                py_ass_parser3276 = 'D.__floordiv__'
                Sensor(3276)
                return py_ass_parser3276

            def __rfloordiv__(self, other):
                py_ass_parser3277 = 'D.__rfloordiv__'
                Sensor(3277)
                return py_ass_parser3277
        self.assertEqual(D() // C(), 'D.__floordiv__')
        self.assertEqual(C() // D(), 'D.__rfloordiv__')


        class E(C):
            pass
        self.assertEqual(E.__rfloordiv__, C.__rfloordiv__)
        self.assertEqual(E() // 1, 'C.__floordiv__')
        self.assertEqual(1 // E(), 'C.__rfloordiv__')
        self.assertEqual(E() // C(), 'C.__floordiv__')
        self.assertEqual(C() // E(), 'C.__floordiv__')
        Sensor(3278)

    @test_support.impl_detail('testing an internal kind of method object')
    def test_meth_class_get(self):
        Sensor(3279)
        arg = [1, 2, 3]
        res = {(1): None, (2): None, (3): None}
        self.assertEqual(dict.fromkeys(arg), res)
        self.assertEqual({}.fromkeys(arg), res)
        descr = dict.__dict__['fromkeys']
        self.assertEqual(descr.__get__(None, dict)(arg), res)
        self.assertEqual(descr.__get__({})(arg), res)
        Sensor(3280)
        try:
            Sensor(3281)
            descr.__get__(None, None)
        except TypeError:
            Sensor(3282)
            pass
        else:
            Sensor(3283)
            self.fail("shouldn't have allowed descr.__get__(None, None)")
        Sensor(3284)
        try:
            Sensor(3285)
            descr.__get__(42)
        except TypeError:
            Sensor(3286)
            pass
        else:
            Sensor(3287)
            self.fail("shouldn't have allowed descr.__get__(42)")
        Sensor(3288)
        try:
            Sensor(3289)
            descr.__get__(None, 42)
        except TypeError:
            Sensor(3290)
            pass
        else:
            Sensor(3291)
            self.fail("shouldn't have allowed descr.__get__(None, 42)")
        Sensor(3292)
        try:
            Sensor(3293)
            descr.__get__(None, int)
        except TypeError:
            Sensor(3294)
            pass
        else:
            Sensor(3295)
            self.fail("shouldn't have allowed descr.__get__(None, int)")
        Sensor(3296)

    def test_isinst_isclass(self):


        class Proxy(object):

            def __init__(self, obj):
                Sensor(3297)
                self.__obj = obj
                Sensor(3298)

            def __getattribute__(self, name):
                Sensor(3299)
                if name.startswith('_Proxy__'):
                    py_ass_parser3300 = object.__getattribute__(self, name)
                    Sensor(3300)
                    return py_ass_parser3300
                else:
                    py_ass_parser3301 = getattr(self.__obj, name)
                    Sensor(3301)
                    return py_ass_parser3301
                Sensor(3302)


        class C:
            pass
        Sensor(3303)
        a = C()
        pa = Proxy(a)
        self.assertIsInstance(a, C)
        self.assertIsInstance(pa, C)


        class D(C):
            pass
        a = D()
        pa = Proxy(a)
        self.assertIsInstance(a, C)
        self.assertIsInstance(pa, C)


        class C(object):
            pass
        a = C()
        pa = Proxy(a)
        self.assertIsInstance(a, C)
        self.assertIsInstance(pa, C)


        class D(C):
            pass
        a = D()
        pa = Proxy(a)
        self.assertIsInstance(a, C)
        self.assertIsInstance(pa, C)
        Sensor(3304)

    def test_proxy_super(self):


        class Proxy(object):

            def __init__(self, obj):
                Sensor(3305)
                self.__obj = obj
                Sensor(3306)

            def __getattribute__(self, name):
                Sensor(3307)
                if name.startswith('_Proxy__'):
                    py_ass_parser3308 = object.__getattribute__(self, name)
                    Sensor(3308)
                    return py_ass_parser3308
                else:
                    py_ass_parser3309 = getattr(self.__obj, name)
                    Sensor(3309)
                    return py_ass_parser3309
                Sensor(3310)


        class B(object):

            def f(self):
                py_ass_parser3311 = 'B.f'
                Sensor(3311)
                return py_ass_parser3311


        class C(B):

            def f(self):
                py_ass_parser3312 = super(C, self).f() + '->C.f'
                Sensor(3312)
                return py_ass_parser3312
        Sensor(3313)
        obj = C()
        p = Proxy(obj)
        self.assertEqual(C.__dict__['f'](p), 'B.f->C.f')
        Sensor(3314)

    def test_carloverre(self):
        Sensor(3315)
        try:
            Sensor(3316)
            object.__setattr__(str, 'foo', 42)
        except TypeError:
            Sensor(3317)
            pass
        else:
            Sensor(3318)
            self.fail('Carlo Verre __setattr__ succeeded!')
        Sensor(3319)
        try:
            Sensor(3320)
            object.__delattr__(str, 'lower')
        except TypeError:
            Sensor(3321)
            pass
        else:
            Sensor(3322)
            self.fail('Carlo Verre __delattr__ succeeded!')
        Sensor(3323)

    def test_weakref_segfault(self):
        import weakref


        class Provoker:

            def __init__(self, referrent):
                Sensor(3324)
                self.ref = weakref.ref(referrent)
                Sensor(3325)

            def __del__(self):
                Sensor(3326)
                x = self.ref()
                Sensor(3327)


        class Oops(object):
            pass
        Sensor(3328)
        o = Oops()
        o.whatever = Provoker(o)
        del o
        Sensor(3329)

    def test_wrapper_segfault(self):
        Sensor(3330)
        f = lambda : None
        Sensor(3331)
        for i in xrange(1000000):
            Sensor(3332)
            f = f.__call__
        Sensor(3333)
        f = None
        Sensor(3334)

    def test_file_fault(self):
        Sensor(3335)
        test_stdout = sys.stdout


        class StdoutGuard:

            def __getattr__(self, attr):
                Sensor(3336)
                sys.stdout = sys.__stdout__
                Sensor(3337)
                raise RuntimeError('Premature access to sys.stdout.%s' % attr)
        sys.stdout = StdoutGuard()
        try:
            Sensor(3338)
            try:
                Sensor(3339)
                print 'Oops!'
            except RuntimeError:
                Sensor(3340)
                pass
        finally:
            Sensor(3341)
            sys.stdout = test_stdout
        Sensor(3342)

    def test_vicious_descriptor_nonsense(self):


        class Evil(object):

            def __hash__(self):
                py_ass_parser3343 = hash('attr')
                Sensor(3343)
                return py_ass_parser3343

            def __eq__(self, other):
                Sensor(3344)
                del C.attr
                py_ass_parser3345 = 0
                Sensor(3345)
                return py_ass_parser3345


        class Descr(object):

            def __get__(self, ob, type=None):
                py_ass_parser3346 = 1
                Sensor(3346)
                return py_ass_parser3346


        class C(object):
            attr = Descr()
        Sensor(3347)
        c = C()
        c.__dict__[Evil()] = 0
        self.assertEqual(c.attr, 1)
        test_support.gc_collect()
        self.assertEqual(hasattr(c, 'attr'), False)
        Sensor(3348)

    def test_init(self):


        class Foo(object):

            def __init__(self):
                py_ass_parser3349 = 10
                Sensor(3349)
                return py_ass_parser3349
        Sensor(3350)
        try:
            Sensor(3351)
            Foo()
        except TypeError:
            Sensor(3352)
            pass
        else:
            Sensor(3353)
            self.fail('did not test __init__() for None return')
        Sensor(3354)

    def test_method_wrapper(self):
        Sensor(3355)
        l = []
        self.assertEqual(l.__add__, l.__add__)
        self.assertEqual(l.__add__, [].__add__)
        self.assertTrue(l.__add__ != [5].__add__)
        self.assertTrue(l.__add__ != l.__mul__)
        self.assertTrue(l.__add__.__name__ == '__add__')
        Sensor(3356)
        if hasattr(l.__add__, '__self__'):
            Sensor(3357)
            self.assertTrue(l.__add__.__self__ is l)
            self.assertTrue(l.__add__.__objclass__ is list)
        else:
            Sensor(3358)
            self.assertTrue(l.__add__.im_self is l)
            self.assertTrue(l.__add__.im_class is list)
        Sensor(3359)
        self.assertEqual(l.__add__.__doc__, list.__add__.__doc__)
        Sensor(3360)
        try:
            Sensor(3361)
            hash(l.__add__)
        except TypeError:
            Sensor(3362)
            pass
        else:
            Sensor(3363)
            self.fail('no TypeError from hash([].__add__)')
        Sensor(3364)
        t = ()
        t += 7,
        self.assertEqual(t.__add__, (7,).__add__)
        self.assertEqual(hash(t.__add__), hash((7,).__add__))
        Sensor(3365)

    def test_not_implemented(self):
        import operator

        def specialmethod(self, other):
            py_ass_parser3366 = NotImplemented
            Sensor(3366)
            return py_ass_parser3366

        def check(expr, x, y):
            Sensor(3367)
            try:
                Sensor(3368)
                exec expr in {'x': x, 'y': y, 'operator': operator}
            except TypeError:
                Sensor(3369)
                pass
            else:
                Sensor(3370)
                self.fail('no TypeError from %r' % (expr,))
            Sensor(3371)
        Sensor(3372)
        N1 = sys.maxint + 1L
        N2 = sys.maxint
        Sensor(3373)
        for metaclass in [type, types.ClassType]:
            Sensor(3374)
            for name, expr, iexpr in [('__add__', 'x + y', 'x += y'), (
                '__sub__', 'x - y', 'x -= y'), ('__mul__', 'x * y',
                'x *= y'), ('__truediv__', 'operator.truediv(x, y)', None),
                ('__floordiv__', 'operator.floordiv(x, y)', None), (
                '__div__', 'x / y', 'x /= y'), ('__mod__', 'x % y',
                'x %= y'), ('__divmod__', 'divmod(x, y)', None), ('__pow__',
                'x ** y', 'x **= y'), ('__lshift__', 'x << y', 'x <<= y'),
                ('__rshift__', 'x >> y', 'x >>= y'), ('__and__', 'x & y',
                'x &= y'), ('__or__', 'x | y', 'x |= y'), ('__xor__',
                'x ^ y', 'x ^= y'), ('__coerce__', 'coerce(x, y)', None)]:
                Sensor(3375)
                if name == '__coerce__':
                    Sensor(3376)
                    rname = name
                else:
                    Sensor(3377)
                    rname = '__r' + name[2:]
                Sensor(3378)
                A = metaclass('A', (), {name: specialmethod})
                B = metaclass('B', (), {rname: specialmethod})
                a = A()
                b = B()
                check(expr, a, a)
                check(expr, a, b)
                check(expr, b, a)
                check(expr, b, b)
                check(expr, a, N1)
                check(expr, a, N2)
                check(expr, N1, b)
                check(expr, N2, b)
                Sensor(3379)
                if iexpr:
                    Sensor(3380)
                    check(iexpr, a, a)
                    check(iexpr, a, b)
                    check(iexpr, b, a)
                    check(iexpr, b, b)
                    check(iexpr, a, N1)
                    check(iexpr, a, N2)
                    iname = '__i' + name[2:]
                    C = metaclass('C', (), {iname: specialmethod})
                    c = C()
                    check(iexpr, c, a)
                    check(iexpr, c, b)
                    check(iexpr, c, N1)
                    check(iexpr, c, N2)
        Sensor(3381)

    def test_assign_slice(self):


        class C(object):

            def __setslice__(self, start, stop, value):
                Sensor(3382)
                self.value = value
                Sensor(3383)
        Sensor(3384)
        c = C()
        c[1:2] = 3
        self.assertEqual(c.value, 3)
        Sensor(3385)

    def test_set_and_no_get(self):


        class Descr(object):

            def __init__(self, name):
                Sensor(3386)
                self.name = name
                Sensor(3387)

            def __set__(self, obj, value):
                Sensor(3388)
                obj.__dict__[self.name] = value
                Sensor(3389)
        Sensor(3390)
        descr = Descr('a')


        class X(object):
            a = descr
        x = X()
        self.assertIs(x.a, descr)
        x.a = 42
        self.assertEqual(x.a, 42)


        class Meta(type):
            pass


        class X(object):
            __metaclass__ = Meta
        X.a = 42
        Meta.a = Descr('a')
        self.assertEqual(X.a, 42)
        Sensor(3391)

    def test_getattr_hooks(self):


        class Descriptor(object):
            counter = 0

            def __get__(self, obj, objtype=None):

                def getter(name):
                    Sensor(3392)
                    self.counter += 1
                    Sensor(3393)
                    raise AttributeError(name)
                py_ass_parser3394 = getter
                Sensor(3394)
                return py_ass_parser3394
        Sensor(3395)
        descr = Descriptor()


        class A(object):
            __getattribute__ = descr


        class B(object):
            __getattr__ = descr


        class C(object):
            __getattribute__ = descr
            __getattr__ = descr
        self.assertRaises(AttributeError, getattr, A(), 'attr')
        self.assertEqual(descr.counter, 1)
        self.assertRaises(AttributeError, getattr, B(), 'attr')
        self.assertEqual(descr.counter, 2)
        self.assertRaises(AttributeError, getattr, C(), 'attr')
        self.assertEqual(descr.counter, 4)


        class EvilGetattribute(object):

            def __getattr__(self, name):
                Sensor(3396)
                raise AttributeError(name)

            def __getattribute__(self, name):
                Sensor(3397)
                del EvilGetattribute.__getattr__
                Sensor(3398)
                for i in range(5):
                    Sensor(3399)
                    gc.collect()
                Sensor(3400)
                raise AttributeError(name)
        self.assertRaises(AttributeError, getattr, EvilGetattribute(), 'attr')
        Sensor(3401)

    def test_type___getattribute__(self):
        Sensor(3402)
        self.assertRaises(TypeError, type.__getattribute__, list, type)
        Sensor(3403)

    def test_abstractmethods(self):
        Sensor(3404)
        self.assertRaises(AttributeError, getattr, type, '__abstractmethods__')


        class meta(type):
            pass
        self.assertRaises(AttributeError, getattr, meta, '__abstractmethods__')


        class X(object):
            pass
        with self.assertRaises(AttributeError):
            del X.__abstractmethods__
        Sensor(3405)

    def test_proxy_call(self):


        class FakeStr(object):
            __class__ = str
        Sensor(3406)
        fake_str = FakeStr()
        self.assertTrue(isinstance(fake_str, str))
        with self.assertRaises(TypeError):
            str.split(fake_str)
        with self.assertRaises(TypeError):
            str.__add__(fake_str, 'abc')
        Sensor(3407)

    def test_repr_as_str(self):


        class Foo(object):
            pass
        Sensor(3408)
        Foo.__repr__ = Foo.__str__
        foo = Foo()
        self.assertRaises(RuntimeError, str, foo)
        self.assertRaises(RuntimeError, repr, foo)
        Sensor(3409)

    def test_mixing_slot_wrappers(self):


        class X(dict):
            __setattr__ = dict.__setitem__
        Sensor(3410)
        x = X()
        x.y = 42
        self.assertEqual(x['y'], 42)
        Sensor(3411)

    def test_cycle_through_dict(self):


        class X(dict):

            def __init__(self):
                Sensor(3412)
                dict.__init__(self)
                self.__dict__ = self
                Sensor(3413)
        Sensor(3414)
        x = X()
        x.attr = 42
        wr = weakref.ref(x)
        del x
        test_support.gc_collect()
        self.assertIsNone(wr())
        Sensor(3415)
        for o in gc.get_objects():
            Sensor(3416)
            self.assertIsNot(type(o), X)
        Sensor(3417)


class DictProxyTests(unittest.TestCase):

    def setUp(self):


        class C(object):

            def meth(self):
                Sensor(3418)
                pass
                Sensor(3419)
        Sensor(3420)
        self.C = C
        Sensor(3421)

    def test_repr(self):
        Sensor(3422)
        self.assertIn('dict_proxy({', repr(vars(self.C)))
        self.assertIn("'meth':", repr(vars(self.C)))
        Sensor(3423)

    def test_iter_keys(self):
        Sensor(3424)
        keys = [key for key in self.C.__dict__.iterkeys()]
        keys.sort()
        self.assertEqual(keys, ['__dict__', '__doc__', '__module__',
            '__weakref__', 'meth'])
        Sensor(3425)

    def test_iter_values(self):
        Sensor(3426)
        values = [values for values in self.C.__dict__.itervalues()]
        self.assertEqual(len(values), 5)
        Sensor(3427)

    def test_iter_items(self):
        Sensor(3428)
        keys = [key for key, value in self.C.__dict__.iteritems()]
        keys.sort()
        self.assertEqual(keys, ['__dict__', '__doc__', '__module__',
            '__weakref__', 'meth'])
        Sensor(3429)

    def test_dict_type_with_metaclass(self):


        class B(object):
            pass


        class M(type):
            pass


        class C:
            __metaclass__ = M
        Sensor(3430)
        self.assertEqual(type(C.__dict__), type(B.__dict__))
        Sensor(3431)


class PTypesLongInitTest(unittest.TestCase):

    def test_pytype_long_ready(self):


        class UserLong(object):

            def __pow__(self, *args):
                Sensor(3432)
                pass
                Sensor(3433)
        Sensor(3434)
        try:
            Sensor(3435)
            pow(0L, UserLong(), 0L)
        except:
            Sensor(3436)
            pass
        Sensor(3437)
        type.mro(tuple)
        Sensor(3438)


def test_main():
    Sensor(3439)
    deprecations = [('complex divmod\\(\\), // and % are deprecated$',
        DeprecationWarning)]
    Sensor(3440)
    if sys.py3kwarning:
        Sensor(3441)
        deprecations += [('classic (int|long) division', DeprecationWarning
            ), ('coerce.. not supported', DeprecationWarning), (
            '.+__(get|set|del)slice__ has been removed', DeprecationWarning)]
    with test_support.check_warnings(*deprecations):
        Sensor(3442)
        test_support.run_unittest(PTypesLongInitTest, OperatorsTest,
            ClassPropertiesAndMethods, DictProxyTests)
    Sensor(3443)


Sensor(3444)
if __name__ == '__main__':
    Sensor(3445)
    test_main()
Sensor(3447)
