from sensors import Sensor


Sensor(1)
"""
This is a Py2.3 implementation of decimal floating point arithmetic based on
the General Decimal Arithmetic Specification:

    http://speleotrove.com/decimal/decarith.html

and IEEE standard 854-1987:

    www.cs.berkeley.edu/~ejr/projects/754/private/drafts/854-1987/dir.html

Decimal floating point has finite precision with arbitrarily large bounds.

The purpose of this module is to support arithmetic using familiar
"schoolhouse" rules and to avoid some of the tricky representation
issues associated with binary floating point.  The package is especially
useful for financial applications or for contexts where users have
expectations that are at odds with binary floating point (for instance,
in binary floating point, 1.00 % 0.1 gives 0.09999999999999995 instead
of the expected Decimal('0.00') returned by decimal floating point).

Here are some examples of using the decimal module:

>>> from decimal import *
>>> setcontext(ExtendedContext)
>>> Decimal(0)
Decimal('0')
>>> Decimal('1')
Decimal('1')
>>> Decimal('-.0123')
Decimal('-0.0123')
>>> Decimal(123456)
Decimal('123456')
>>> Decimal('123.45e12345678901234567890')
Decimal('1.2345E+12345678901234567892')
>>> Decimal('1.33') + Decimal('1.27')
Decimal('2.60')
>>> Decimal('12.34') + Decimal('3.87') - Decimal('18.41')
Decimal('-2.20')
>>> dig = Decimal(1)
>>> print dig / Decimal(3)
0.333333333
>>> getcontext().prec = 18
>>> print dig / Decimal(3)
0.333333333333333333
>>> print dig.sqrt()
1
>>> print Decimal(3).sqrt()
1.73205080756887729
>>> print Decimal(3) ** 123
4.85192780976896427E+58
>>> inf = Decimal(1) / Decimal(0)
>>> print inf
Infinity
>>> neginf = Decimal(-1) / Decimal(0)
>>> print neginf
-Infinity
>>> print neginf + inf
NaN
>>> print neginf * inf
-Infinity
>>> print dig / 0
Infinity
>>> getcontext().traps[DivisionByZero] = 1
>>> print dig / 0
Traceback (most recent call last):
  ...
  ...
  ...
DivisionByZero: x / 0
>>> c = Context()
>>> c.traps[InvalidOperation] = 0
>>> print c.flags[InvalidOperation]
0
>>> c.divide(Decimal(0), Decimal(0))
Decimal('NaN')
>>> c.traps[InvalidOperation] = 1
>>> print c.flags[InvalidOperation]
1
>>> c.flags[InvalidOperation] = 0
>>> print c.flags[InvalidOperation]
0
>>> print c.divide(Decimal(0), Decimal(0))
Traceback (most recent call last):
  ...
  ...
  ...
InvalidOperation: 0 / 0
>>> print c.flags[InvalidOperation]
1
>>> c.flags[InvalidOperation] = 0
>>> c.traps[InvalidOperation] = 0
>>> print c.divide(Decimal(0), Decimal(0))
NaN
>>> print c.flags[InvalidOperation]
1
>>>
"""
__all__ = ['Decimal', 'Context', 'DefaultContext', 'BasicContext',
    'ExtendedContext', 'DecimalException', 'Clamped', 'InvalidOperation',
    'DivisionByZero', 'Inexact', 'Rounded', 'Subnormal', 'Overflow',
    'Underflow', 'ROUND_DOWN', 'ROUND_HALF_UP', 'ROUND_HALF_EVEN',
    'ROUND_CEILING', 'ROUND_FLOOR', 'ROUND_UP', 'ROUND_HALF_DOWN',
    'ROUND_05UP', 'setcontext', 'getcontext', 'localcontext']
__version__ = '1.70'
import copy as _copy
import math as _math
import numbers as _numbers
Sensor(2)
try:
    from collections import namedtuple as _namedtuple
    Sensor(3)
    DecimalTuple = _namedtuple('DecimalTuple', 'sign digits exponent')
except ImportError:
    Sensor(4)
    DecimalTuple = lambda *args: args
Sensor(5)
ROUND_DOWN = 'ROUND_DOWN'
ROUND_HALF_UP = 'ROUND_HALF_UP'
ROUND_HALF_EVEN = 'ROUND_HALF_EVEN'
ROUND_CEILING = 'ROUND_CEILING'
ROUND_FLOOR = 'ROUND_FLOOR'
ROUND_UP = 'ROUND_UP'
ROUND_HALF_DOWN = 'ROUND_HALF_DOWN'
ROUND_05UP = 'ROUND_05UP'


class DecimalException(ArithmeticError):
    """Base exception class.

    Used exceptions derive from this.
    If an exception derives from another exception besides this (such as
    Underflow (Inexact, Rounded, Subnormal) that indicates that it is only
    called if the others are present.  This isn't actually used for
    anything, though.

    handle  -- Called when context._raise_error is called and the
               trap_enabler is not set.  First argument is self, second is the
               context.  More arguments can be given, those being after
               the explanation in _raise_error (For example,
               context._raise_error(NewError, '(-x)!', self._sign) would
               call NewError().handle(context, self._sign).)

    To define a new exception, it should be sufficient to have it derive
    from DecimalException.
    """

    def handle(self, context, *args):
        Sensor(6)
        pass
        Sensor(7)


class Clamped(DecimalException):
    """Exponent of a 0 changed to fit bounds.

    This occurs and signals clamped if the exponent of a result has been
    altered in order to fit the constraints of a specific concrete
    representation.  This may occur when the exponent of a zero result would
    be outside the bounds of a representation, or when a large normal
    number would have an encoded exponent that cannot be represented.  In
    this latter case, the exponent is reduced to fit and the corresponding
    number of zero digits are appended to the coefficient ("fold-down").
    """


class InvalidOperation(DecimalException):
    """An invalid operation was performed.

    Various bad things cause this:

    Something creates a signaling NaN
    -INF + INF
    0 * (+-)INF
    (+-)INF / (+-)INF
    x % 0
    (+-)INF % x
    x._rescale( non-integer )
    sqrt(-x) , x > 0
    0 ** 0
    x ** (non-integer)
    x ** (+-)INF
    An operand is invalid

    The result of the operation after these is a quiet positive NaN,
    except when the cause is a signaling NaN, in which case the result is
    also a quiet NaN, but with the original sign, and an optional
    diagnostic information.
    """

    def handle(self, context, *args):
        Sensor(8)
        if args:
            Sensor(9)
            ans = _dec_from_triple(args[0]._sign, args[0]._int, 'n', True)
            py_ass_parser10 = ans._fix_nan(context)
            Sensor(10)
            return py_ass_parser10
        py_ass_parser11 = _NaN
        Sensor(11)
        return py_ass_parser11


class ConversionSyntax(InvalidOperation):
    """Trying to convert badly formed string.

    This occurs and signals invalid-operation if an string is being
    converted to a number and it does not conform to the numeric string
    syntax.  The result is [0,qNaN].
    """

    def handle(self, context, *args):
        py_ass_parser12 = _NaN
        Sensor(12)
        return py_ass_parser12


class DivisionByZero(DecimalException, ZeroDivisionError):
    """Division by 0.

    This occurs and signals division-by-zero if division of a finite number
    by zero was attempted (during a divide-integer or divide operation, or a
    power operation with negative right-hand operand), and the dividend was
    not zero.

    The result of the operation is [sign,inf], where sign is the exclusive
    or of the signs of the operands for divide, or is 1 for an odd power of
    -0, for power.
    """

    def handle(self, context, sign, *args):
        py_ass_parser13 = _SignedInfinity[sign]
        Sensor(13)
        return py_ass_parser13


class DivisionImpossible(InvalidOperation):
    """Cannot perform the division adequately.

    This occurs and signals invalid-operation if the integer result of a
    divide-integer or remainder operation had too many digits (would be
    longer than precision).  The result is [0,qNaN].
    """

    def handle(self, context, *args):
        py_ass_parser14 = _NaN
        Sensor(14)
        return py_ass_parser14


class DivisionUndefined(InvalidOperation, ZeroDivisionError):
    """Undefined result of division.

    This occurs and signals invalid-operation if division by zero was
    attempted (during a divide-integer, divide, or remainder operation), and
    the dividend is also zero.  The result is [0,qNaN].
    """

    def handle(self, context, *args):
        py_ass_parser15 = _NaN
        Sensor(15)
        return py_ass_parser15


class Inexact(DecimalException):
    """Had to round, losing information.

    This occurs and signals inexact whenever the result of an operation is
    not exact (that is, it needed to be rounded and any discarded digits
    were non-zero), or if an overflow or underflow condition occurs.  The
    result in all cases is unchanged.

    The inexact signal may be tested (or trapped) to determine if a given
    operation (or sequence of operations) was inexact.
    """


class InvalidContext(InvalidOperation):
    """Invalid context.  Unknown rounding, for example.

    This occurs and signals invalid-operation if an invalid context was
    detected during an operation.  This can occur if contexts are not checked
    on creation and either the precision exceeds the capability of the
    underlying concrete representation or an unknown or unsupported rounding
    was specified.  These aspects of the context need only be checked when
    the values are required to be used.  The result is [0,qNaN].
    """

    def handle(self, context, *args):
        py_ass_parser16 = _NaN
        Sensor(16)
        return py_ass_parser16


class Rounded(DecimalException):
    """Number got rounded (not  necessarily changed during rounding).

    This occurs and signals rounded whenever the result of an operation is
    rounded (that is, some zero or non-zero digits were discarded from the
    coefficient), or if an overflow or underflow condition occurs.  The
    result in all cases is unchanged.

    The rounded signal may be tested (or trapped) to determine if a given
    operation (or sequence of operations) caused a loss of precision.
    """


class Subnormal(DecimalException):
    """Exponent < Emin before rounding.

    This occurs and signals subnormal whenever the result of a conversion or
    operation is subnormal (that is, its adjusted exponent is less than
    Emin, before any rounding).  The result in all cases is unchanged.

    The subnormal signal may be tested (or trapped) to determine if a given
    or operation (or sequence of operations) yielded a subnormal result.
    """


class Overflow(Inexact, Rounded):
    """Numerical overflow.

    This occurs and signals overflow if the adjusted exponent of a result
    (from a conversion or from an operation that is not an attempt to divide
    by zero), after rounding, would be greater than the largest value that
    can be handled by the implementation (the value Emax).

    The result depends on the rounding mode:

    For round-half-up and round-half-even (and for round-half-down and
    round-up, if implemented), the result of the operation is [sign,inf],
    where sign is the sign of the intermediate result.  For round-down, the
    result is the largest finite number that can be represented in the
    current precision, with the sign of the intermediate result.  For
    round-ceiling, the result is the same as for round-down if the sign of
    the intermediate result is 1, or is [0,inf] otherwise.  For round-floor,
    the result is the same as for round-down if the sign of the intermediate
    result is 0, or is [1,inf] otherwise.  In all cases, Inexact and Rounded
    will also be raised.
    """

    def handle(self, context, sign, *args):
        Sensor(17)
        if context.rounding in (ROUND_HALF_UP, ROUND_HALF_EVEN,
            ROUND_HALF_DOWN, ROUND_UP):
            py_ass_parser18 = _SignedInfinity[sign]
            Sensor(18)
            return py_ass_parser18
        Sensor(19)
        if sign == 0:
            Sensor(20)
            if context.rounding == ROUND_CEILING:
                py_ass_parser21 = _SignedInfinity[sign]
                Sensor(21)
                return py_ass_parser21
            py_ass_parser22 = _dec_from_triple(sign, '9' * context.prec, 
                context.Emax - context.prec + 1)
            Sensor(22)
            return py_ass_parser22
        Sensor(23)
        if sign == 1:
            Sensor(24)
            if context.rounding == ROUND_FLOOR:
                py_ass_parser25 = _SignedInfinity[sign]
                Sensor(25)
                return py_ass_parser25
            py_ass_parser26 = _dec_from_triple(sign, '9' * context.prec, 
                context.Emax - context.prec + 1)
            Sensor(26)
            return py_ass_parser26
        Sensor(27)


class Underflow(Inexact, Rounded, Subnormal):
    """Numerical underflow with result rounded to 0.

    This occurs and signals underflow if a result is inexact and the
    adjusted exponent of the result would be smaller (more negative) than
    the smallest value that can be handled by the implementation (the value
    Emin).  That is, the result is both inexact and subnormal.

    The result after an underflow will be a subnormal number rounded, if
    necessary, so that its exponent is not less than Etiny.  This may result
    in 0 with the sign of the intermediate result and an exponent of Etiny.

    In all cases, Inexact, Rounded, and Subnormal will also be raised.
    """


_signals = [Clamped, DivisionByZero, Inexact, Overflow, Rounded, Underflow,
    InvalidOperation, Subnormal]
_condition_map = {ConversionSyntax: InvalidOperation, DivisionImpossible:
    InvalidOperation, DivisionUndefined: InvalidOperation, InvalidContext:
    InvalidOperation}
Sensor(28)
try:
    Sensor(29)
    import threading
except ImportError:
    import sys


    class MockThreading(object):

        def local(self, sys=sys):
            py_ass_parser30 = sys.modules[__name__]
            Sensor(30)
            return py_ass_parser30
    Sensor(31)
    threading = MockThreading()
    del sys, MockThreading
Sensor(32)
try:
    Sensor(33)
    threading.local
except AttributeError:
    Sensor(34)
    if hasattr(threading.currentThread(), '__decimal_context__'):
        Sensor(35)
        del threading.currentThread().__decimal_context__

    def setcontext(context):
        Sensor(36)
        """Set this thread's context to context."""
        Sensor(37)
        if context in (DefaultContext, BasicContext, ExtendedContext):
            Sensor(38)
            context = context.copy()
            context.clear_flags()
        Sensor(39)
        threading.currentThread().__decimal_context__ = context
        Sensor(40)

    def getcontext():
        Sensor(41)
        """Returns this thread's context.

        If this thread does not yet have a context, returns
        a new context and sets this thread's context.
        New contexts are copies of DefaultContext.
        """
        Sensor(42)
        try:
            py_ass_parser43 = threading.currentThread().__decimal_context__
            Sensor(43)
            return py_ass_parser43
        except AttributeError:
            Sensor(44)
            context = Context()
            threading.currentThread().__decimal_context__ = context
            py_ass_parser45 = context
            Sensor(45)
            return py_ass_parser45
        Sensor(46)
else:
    Sensor(47)
    local = threading.local()
    Sensor(48)
    if hasattr(local, '__decimal_context__'):
        Sensor(49)
        del local.__decimal_context__

    def getcontext(_local=local):
        Sensor(50)
        """Returns this thread's context.

        If this thread does not yet have a context, returns
        a new context and sets this thread's context.
        New contexts are copies of DefaultContext.
        """
        Sensor(51)
        try:
            py_ass_parser52 = _local.__decimal_context__
            Sensor(52)
            return py_ass_parser52
        except AttributeError:
            Sensor(53)
            context = Context()
            _local.__decimal_context__ = context
            py_ass_parser54 = context
            Sensor(54)
            return py_ass_parser54
        Sensor(55)

    def setcontext(context, _local=local):
        Sensor(56)
        """Set this thread's context to context."""
        Sensor(57)
        if context in (DefaultContext, BasicContext, ExtendedContext):
            Sensor(58)
            context = context.copy()
            context.clear_flags()
        Sensor(59)
        _local.__decimal_context__ = context
        Sensor(60)
    Sensor(61)
    del threading, local


def localcontext(ctx=None):
    Sensor(62)
    """Return a context manager for a copy of the supplied context

    Uses a copy of the current context if no context is specified
    The returned context manager creates a local decimal context
    in a with statement:
        def sin(x):
             with localcontext() as ctx:
                 ctx.prec += 2
                 # Rest of sin calculation algorithm
                 # uses a precision 2 greater than normal
             return +s  # Convert result to normal precision

         def sin(x):
             with localcontext(ExtendedContext):
                 # Rest of sin calculation algorithm
                 # uses the Extended Context from the
                 # General Decimal Arithmetic Specification
             return +s  # Convert result to normal context

    >>> setcontext(DefaultContext)
    >>> print getcontext().prec
    28
    >>> with localcontext():
    ...     ctx = getcontext()
    ...     ctx.prec += 2
    ...     print ctx.prec
    ...
    30
    >>> with localcontext(ExtendedContext):
    ...     print getcontext().prec
    ...
    9
    >>> print getcontext().prec
    28
    """
    Sensor(63)
    if ctx is None:
        Sensor(64)
        ctx = getcontext()
    py_ass_parser65 = _ContextManager(ctx)
    Sensor(65)
    return py_ass_parser65


class Decimal(object):
    """Floating point class for decimal arithmetic."""
    __slots__ = '_exp', '_int', '_sign', '_is_special'

    def __new__(cls, value='0', context=None):
        Sensor(66)
        """Create a decimal point instance.

        >>> Decimal('3.14')              # string input
        Decimal('3.14')
        >>> Decimal((0, (3, 1, 4), -2))  # tuple (sign, digit_tuple, exponent)
        Decimal('3.14')
        >>> Decimal(314)                 # int or long
        Decimal('314')
        >>> Decimal(Decimal(314))        # another decimal instance
        Decimal('314')
        >>> Decimal('  3.14  \\n')        # leading and trailing whitespace okay
        Decimal('3.14')
        """
        self = object.__new__(cls)
        Sensor(67)
        if isinstance(value, basestring):
            Sensor(68)
            m = _parser(value.strip())
            Sensor(69)
            if m is None:
                Sensor(70)
                if context is None:
                    Sensor(71)
                    context = getcontext()
                py_ass_parser72 = context._raise_error(ConversionSyntax, 
                    'Invalid literal for Decimal: %r' % value)
                Sensor(72)
                return py_ass_parser72
            Sensor(73)
            if m.group('sign') == '-':
                Sensor(74)
                self._sign = 1
            else:
                Sensor(75)
                self._sign = 0
            Sensor(76)
            intpart = m.group('int')
            Sensor(77)
            if intpart is not None:
                Sensor(78)
                fracpart = m.group('frac') or ''
                exp = int(m.group('exp') or '0')
                self._int = str(int(intpart + fracpart))
                self._exp = exp - len(fracpart)
                self._is_special = False
            else:
                Sensor(79)
                diag = m.group('diag')
                Sensor(80)
                if diag is not None:
                    Sensor(81)
                    self._int = str(int(diag or '0')).lstrip('0')
                    Sensor(82)
                    if m.group('signal'):
                        Sensor(83)
                        self._exp = 'N'
                    else:
                        Sensor(84)
                        self._exp = 'n'
                else:
                    Sensor(85)
                    self._int = '0'
                    self._exp = 'F'
                Sensor(86)
                self._is_special = True
            py_ass_parser87 = self
            Sensor(87)
            return py_ass_parser87
        Sensor(88)
        if isinstance(value, (int, long)):
            Sensor(89)
            if value >= 0:
                Sensor(90)
                self._sign = 0
            else:
                Sensor(91)
                self._sign = 1
            Sensor(92)
            self._exp = 0
            self._int = str(abs(value))
            self._is_special = False
            py_ass_parser93 = self
            Sensor(93)
            return py_ass_parser93
        Sensor(94)
        if isinstance(value, Decimal):
            Sensor(95)
            self._exp = value._exp
            self._sign = value._sign
            self._int = value._int
            self._is_special = value._is_special
            py_ass_parser96 = self
            Sensor(96)
            return py_ass_parser96
        Sensor(97)
        if isinstance(value, _WorkRep):
            Sensor(98)
            self._sign = value.sign
            self._int = str(value.int)
            self._exp = int(value.exp)
            self._is_special = False
            py_ass_parser99 = self
            Sensor(99)
            return py_ass_parser99
        Sensor(100)
        if isinstance(value, (list, tuple)):
            Sensor(101)
            if len(value) != 3:
                Sensor(102)
                raise ValueError(
                    'Invalid tuple size in creation of Decimal from list or tuple.  The list or tuple should have exactly three elements.'
                    )
            Sensor(103)
            if not (isinstance(value[0], (int, long)) and value[0] in (0, 1)):
                Sensor(104)
                raise ValueError(
                    'Invalid sign.  The first value in the tuple should be an integer; either 0 for a positive number or 1 for a negative number.'
                    )
            Sensor(105)
            self._sign = value[0]
            Sensor(106)
            if value[2] == 'F':
                Sensor(107)
                self._int = '0'
                self._exp = value[2]
                self._is_special = True
            else:
                Sensor(108)
                digits = []
                Sensor(109)
                for digit in value[1]:
                    Sensor(110)
                    if isinstance(digit, (int, long)) and 0 <= digit <= 9:
                        Sensor(111)
                        if digits or digit != 0:
                            Sensor(112)
                            digits.append(digit)
                    else:
                        Sensor(113)
                        raise ValueError(
                            'The second value in the tuple must be composed of integers in the range 0 through 9.'
                            )
                Sensor(114)
                if value[2] in ('n', 'N'):
                    Sensor(115)
                    self._int = ''.join(map(str, digits))
                    self._exp = value[2]
                    self._is_special = True
                else:
                    Sensor(116)
                    if isinstance(value[2], (int, long)):
                        Sensor(117)
                        self._int = ''.join(map(str, digits or [0]))
                        self._exp = value[2]
                        self._is_special = False
                    else:
                        Sensor(118)
                        raise ValueError(
                            "The third value in the tuple must be an integer, or one of the strings 'F', 'n', 'N'."
                            )
            py_ass_parser119 = self
            Sensor(119)
            return py_ass_parser119
        Sensor(120)
        if isinstance(value, float):
            Sensor(121)
            value = Decimal.from_float(value)
            self._exp = value._exp
            self._sign = value._sign
            self._int = value._int
            self._is_special = value._is_special
            py_ass_parser122 = self
            Sensor(122)
            return py_ass_parser122
        Sensor(123)
        raise TypeError('Cannot convert %r to Decimal' % value)

    def from_float(cls, f):
        Sensor(124)
        """Converts a float to a decimal number, exactly.

        Note that Decimal.from_float(0.1) is not the same as Decimal('0.1').
        Since 0.1 is not exactly representable in binary floating point, the
        value is stored as the nearest representable value which is
        0x1.999999999999ap-4.  The exact equivalent of the value in decimal
        is 0.1000000000000000055511151231257827021181583404541015625.

        >>> Decimal.from_float(0.1)
        Decimal('0.1000000000000000055511151231257827021181583404541015625')
        >>> Decimal.from_float(float('nan'))
        Decimal('NaN')
        >>> Decimal.from_float(float('inf'))
        Decimal('Infinity')
        >>> Decimal.from_float(-float('inf'))
        Decimal('-Infinity')
        >>> Decimal.from_float(-0.0)
        Decimal('-0')

        """
        Sensor(125)
        if isinstance(f, (int, long)):
            py_ass_parser126 = cls(f)
            Sensor(126)
            return py_ass_parser126
        Sensor(127)
        if _math.isinf(f) or _math.isnan(f):
            py_ass_parser128 = cls(repr(f))
            Sensor(128)
            return py_ass_parser128
        Sensor(129)
        if _math.copysign(1.0, f) == 1.0:
            Sensor(130)
            sign = 0
        else:
            Sensor(131)
            sign = 1
        Sensor(132)
        n, d = abs(f).as_integer_ratio()
        k = d.bit_length() - 1
        result = _dec_from_triple(sign, str(n * 5 ** k), -k)
        Sensor(133)
        if cls is Decimal:
            py_ass_parser134 = result
            Sensor(134)
            return py_ass_parser134
        else:
            py_ass_parser135 = cls(result)
            Sensor(135)
            return py_ass_parser135
        Sensor(136)
    from_float = classmethod(from_float)

    def _isnan(self):
        Sensor(137)
        """Returns whether the number is not actually one.

        0 if a number
        1 if NaN
        2 if sNaN
        """
        Sensor(138)
        if self._is_special:
            Sensor(139)
            exp = self._exp
            Sensor(140)
            if exp == 'n':
                py_ass_parser141 = 1
                Sensor(141)
                return py_ass_parser141
            else:
                Sensor(142)
                if exp == 'N':
                    py_ass_parser143 = 2
                    Sensor(143)
                    return py_ass_parser143
        py_ass_parser144 = 0
        Sensor(144)
        return py_ass_parser144

    def _isinfinity(self):
        Sensor(145)
        """Returns whether the number is infinite

        0 if finite or not a number
        1 if +INF
        -1 if -INF
        """
        Sensor(146)
        if self._exp == 'F':
            Sensor(147)
            if self._sign:
                py_ass_parser148 = -1
                Sensor(148)
                return py_ass_parser148
            py_ass_parser149 = 1
            Sensor(149)
            return py_ass_parser149
        py_ass_parser150 = 0
        Sensor(150)
        return py_ass_parser150

    def _check_nans(self, other=None, context=None):
        Sensor(151)
        """Returns whether the number is not actually one.

        if self, other are sNaN, signal
        if self, other are NaN return nan
        return 0

        Done before operations.
        """
        self_is_nan = self._isnan()
        Sensor(152)
        if other is None:
            Sensor(153)
            other_is_nan = False
        else:
            Sensor(154)
            other_is_nan = other._isnan()
        Sensor(155)
        if self_is_nan or other_is_nan:
            Sensor(156)
            if context is None:
                Sensor(157)
                context = getcontext()
            Sensor(158)
            if self_is_nan == 2:
                py_ass_parser159 = context._raise_error(InvalidOperation,
                    'sNaN', self)
                Sensor(159)
                return py_ass_parser159
            Sensor(160)
            if other_is_nan == 2:
                py_ass_parser161 = context._raise_error(InvalidOperation,
                    'sNaN', other)
                Sensor(161)
                return py_ass_parser161
            Sensor(162)
            if self_is_nan:
                py_ass_parser163 = self._fix_nan(context)
                Sensor(163)
                return py_ass_parser163
            py_ass_parser164 = other._fix_nan(context)
            Sensor(164)
            return py_ass_parser164
        py_ass_parser165 = 0
        Sensor(165)
        return py_ass_parser165

    def _compare_check_nans(self, other, context):
        Sensor(166)
        """Version of _check_nans used for the signaling comparisons
        compare_signal, __le__, __lt__, __ge__, __gt__.

        Signal InvalidOperation if either self or other is a (quiet
        or signaling) NaN.  Signaling NaNs take precedence over quiet
        NaNs.

        Return 0 if neither operand is a NaN.

        """
        Sensor(167)
        if context is None:
            Sensor(168)
            context = getcontext()
        Sensor(169)
        if self._is_special or other._is_special:
            Sensor(170)
            if self.is_snan():
                py_ass_parser171 = context._raise_error(InvalidOperation,
                    'comparison involving sNaN', self)
                Sensor(171)
                return py_ass_parser171
            else:
                Sensor(172)
                if other.is_snan():
                    py_ass_parser173 = context._raise_error(InvalidOperation,
                        'comparison involving sNaN', other)
                    Sensor(173)
                    return py_ass_parser173
                else:
                    Sensor(174)
                    if self.is_qnan():
                        py_ass_parser175 = context._raise_error(
                            InvalidOperation, 'comparison involving NaN', self)
                        Sensor(175)
                        return py_ass_parser175
                    else:
                        Sensor(176)
                        if other.is_qnan():
                            py_ass_parser177 = context._raise_error(
                                InvalidOperation,
                                'comparison involving NaN', other)
                            Sensor(177)
                            return py_ass_parser177
        py_ass_parser178 = 0
        Sensor(178)
        return py_ass_parser178

    def __nonzero__(self):
        Sensor(179)
        """Return True if self is nonzero; otherwise return False.

        NaNs and infinities are considered nonzero.
        """
        py_ass_parser180 = self._is_special or self._int != '0'
        Sensor(180)
        return py_ass_parser180

    def _cmp(self, other):
        Sensor(181)
        """Compare the two non-NaN decimal instances self and other.

        Returns -1 if self < other, 0 if self == other and 1
        if self > other.  This routine is for internal use only."""
        Sensor(182)
        if self._is_special or other._is_special:
            Sensor(183)
            self_inf = self._isinfinity()
            other_inf = other._isinfinity()
            Sensor(184)
            if self_inf == other_inf:
                py_ass_parser185 = 0
                Sensor(185)
                return py_ass_parser185
            else:
                Sensor(186)
                if self_inf < other_inf:
                    py_ass_parser187 = -1
                    Sensor(187)
                    return py_ass_parser187
                else:
                    py_ass_parser188 = 1
                    Sensor(188)
                    return py_ass_parser188
        Sensor(189)
        if not self:
            Sensor(190)
            if not other:
                py_ass_parser191 = 0
                Sensor(191)
                return py_ass_parser191
            else:
                py_ass_parser192 = -(-1) ** other._sign
                Sensor(192)
                return py_ass_parser192
        Sensor(193)
        if not other:
            py_ass_parser194 = (-1) ** self._sign
            Sensor(194)
            return py_ass_parser194
        Sensor(195)
        if other._sign < self._sign:
            py_ass_parser196 = -1
            Sensor(196)
            return py_ass_parser196
        Sensor(197)
        if self._sign < other._sign:
            py_ass_parser198 = 1
            Sensor(198)
            return py_ass_parser198
        Sensor(199)
        self_adjusted = self.adjusted()
        other_adjusted = other.adjusted()
        Sensor(200)
        if self_adjusted == other_adjusted:
            Sensor(201)
            self_padded = self._int + '0' * (self._exp - other._exp)
            other_padded = other._int + '0' * (other._exp - self._exp)
            Sensor(202)
            if self_padded == other_padded:
                py_ass_parser203 = 0
                Sensor(203)
                return py_ass_parser203
            else:
                Sensor(204)
                if self_padded < other_padded:
                    py_ass_parser205 = -(-1) ** self._sign
                    Sensor(205)
                    return py_ass_parser205
                else:
                    py_ass_parser206 = (-1) ** self._sign
                    Sensor(206)
                    return py_ass_parser206
        else:
            Sensor(207)
            if self_adjusted > other_adjusted:
                py_ass_parser208 = (-1) ** self._sign
                Sensor(208)
                return py_ass_parser208
            else:
                py_ass_parser209 = -(-1) ** self._sign
                Sensor(209)
                return py_ass_parser209
        Sensor(210)

    def __eq__(self, other, context=None):
        Sensor(211)
        other = _convert_other(other, allow_float=True)
        Sensor(212)
        if other is NotImplemented:
            py_ass_parser213 = other
            Sensor(213)
            return py_ass_parser213
        Sensor(214)
        if self._check_nans(other, context):
            py_ass_parser215 = False
            Sensor(215)
            return py_ass_parser215
        py_ass_parser216 = self._cmp(other) == 0
        Sensor(216)
        return py_ass_parser216

    def __ne__(self, other, context=None):
        Sensor(217)
        other = _convert_other(other, allow_float=True)
        Sensor(218)
        if other is NotImplemented:
            py_ass_parser219 = other
            Sensor(219)
            return py_ass_parser219
        Sensor(220)
        if self._check_nans(other, context):
            py_ass_parser221 = True
            Sensor(221)
            return py_ass_parser221
        py_ass_parser222 = self._cmp(other) != 0
        Sensor(222)
        return py_ass_parser222

    def __lt__(self, other, context=None):
        Sensor(223)
        other = _convert_other(other, allow_float=True)
        Sensor(224)
        if other is NotImplemented:
            py_ass_parser225 = other
            Sensor(225)
            return py_ass_parser225
        Sensor(226)
        ans = self._compare_check_nans(other, context)
        Sensor(227)
        if ans:
            py_ass_parser228 = False
            Sensor(228)
            return py_ass_parser228
        py_ass_parser229 = self._cmp(other) < 0
        Sensor(229)
        return py_ass_parser229

    def __le__(self, other, context=None):
        Sensor(230)
        other = _convert_other(other, allow_float=True)
        Sensor(231)
        if other is NotImplemented:
            py_ass_parser232 = other
            Sensor(232)
            return py_ass_parser232
        Sensor(233)
        ans = self._compare_check_nans(other, context)
        Sensor(234)
        if ans:
            py_ass_parser235 = False
            Sensor(235)
            return py_ass_parser235
        py_ass_parser236 = self._cmp(other) <= 0
        Sensor(236)
        return py_ass_parser236

    def __gt__(self, other, context=None):
        Sensor(237)
        other = _convert_other(other, allow_float=True)
        Sensor(238)
        if other is NotImplemented:
            py_ass_parser239 = other
            Sensor(239)
            return py_ass_parser239
        Sensor(240)
        ans = self._compare_check_nans(other, context)
        Sensor(241)
        if ans:
            py_ass_parser242 = False
            Sensor(242)
            return py_ass_parser242
        py_ass_parser243 = self._cmp(other) > 0
        Sensor(243)
        return py_ass_parser243

    def __ge__(self, other, context=None):
        Sensor(244)
        other = _convert_other(other, allow_float=True)
        Sensor(245)
        if other is NotImplemented:
            py_ass_parser246 = other
            Sensor(246)
            return py_ass_parser246
        Sensor(247)
        ans = self._compare_check_nans(other, context)
        Sensor(248)
        if ans:
            py_ass_parser249 = False
            Sensor(249)
            return py_ass_parser249
        py_ass_parser250 = self._cmp(other) >= 0
        Sensor(250)
        return py_ass_parser250

    def compare(self, other, context=None):
        Sensor(251)
        """Compares one to another.

        -1 => a < b
        0  => a = b
        1  => a > b
        NaN => one is NaN
        Like __cmp__, but returns Decimal instances.
        """
        other = _convert_other(other, raiseit=True)
        Sensor(252)
        if self._is_special or other and other._is_special:
            Sensor(253)
            ans = self._check_nans(other, context)
            Sensor(254)
            if ans:
                py_ass_parser255 = ans
                Sensor(255)
                return py_ass_parser255
        py_ass_parser256 = Decimal(self._cmp(other))
        Sensor(256)
        return py_ass_parser256

    def __hash__(self):
        Sensor(257)
        """x.__hash__() <==> hash(x)"""
        Sensor(258)
        if self._is_special:
            Sensor(259)
            if self.is_snan():
                Sensor(260)
                raise TypeError('Cannot hash a signaling NaN value.')
            else:
                Sensor(261)
                if self.is_nan():
                    py_ass_parser262 = 0
                    Sensor(262)
                    return py_ass_parser262
                else:
                    Sensor(263)
                    if self._sign:
                        py_ass_parser264 = -271828
                        Sensor(264)
                        return py_ass_parser264
                    else:
                        py_ass_parser265 = 314159
                        Sensor(265)
                        return py_ass_parser265
        Sensor(266)
        self_as_float = float(self)
        Sensor(267)
        if Decimal.from_float(self_as_float) == self:
            py_ass_parser268 = hash(self_as_float)
            Sensor(268)
            return py_ass_parser268
        Sensor(269)
        if self._isinteger():
            Sensor(270)
            op = _WorkRep(self.to_integral_value())
            py_ass_parser271 = hash((-1) ** op.sign * op.int * pow(10, op.
                exp, 2 ** 64 - 1))
            Sensor(271)
            return py_ass_parser271
        py_ass_parser272 = hash((self._sign, self._exp + len(self._int),
            self._int.rstrip('0')))
        Sensor(272)
        return py_ass_parser272

    def as_tuple(self):
        Sensor(273)
        """Represents the number as a triple tuple.

        To show the internals exactly as they are.
        """
        py_ass_parser274 = DecimalTuple(self._sign, tuple(map(int, self.
            _int)), self._exp)
        Sensor(274)
        return py_ass_parser274

    def __repr__(self):
        Sensor(275)
        """Represents the number as an instance of Decimal."""
        py_ass_parser276 = "Decimal('%s')" % str(self)
        Sensor(276)
        return py_ass_parser276

    def __str__(self, eng=False, context=None):
        Sensor(277)
        """Return string representation of the number in scientific notation.

        Captures all of the information in the underlying representation.
        """
        sign = ['', '-'][self._sign]
        Sensor(278)
        if self._is_special:
            Sensor(279)
            if self._exp == 'F':
                py_ass_parser280 = sign + 'Infinity'
                Sensor(280)
                return py_ass_parser280
            else:
                Sensor(281)
                if self._exp == 'n':
                    py_ass_parser282 = sign + 'NaN' + self._int
                    Sensor(282)
                    return py_ass_parser282
                else:
                    py_ass_parser283 = sign + 'sNaN' + self._int
                    Sensor(283)
                    return py_ass_parser283
        Sensor(284)
        leftdigits = self._exp + len(self._int)
        Sensor(285)
        if self._exp <= 0 and leftdigits > -6:
            Sensor(286)
            dotplace = leftdigits
        else:
            Sensor(287)
            if not eng:
                Sensor(288)
                dotplace = 1
            else:
                Sensor(289)
                if self._int == '0':
                    Sensor(290)
                    dotplace = (leftdigits + 1) % 3 - 1
                else:
                    Sensor(291)
                    dotplace = (leftdigits - 1) % 3 + 1
        Sensor(292)
        if dotplace <= 0:
            Sensor(293)
            intpart = '0'
            fracpart = '.' + '0' * -dotplace + self._int
        else:
            Sensor(294)
            if dotplace >= len(self._int):
                Sensor(295)
                intpart = self._int + '0' * (dotplace - len(self._int))
                fracpart = ''
            else:
                Sensor(296)
                intpart = self._int[:dotplace]
                fracpart = '.' + self._int[dotplace:]
        Sensor(297)
        if leftdigits == dotplace:
            Sensor(298)
            exp = ''
        else:
            Sensor(299)
            if context is None:
                Sensor(300)
                context = getcontext()
            Sensor(301)
            exp = ['e', 'E'][context.capitals] + '%+d' % (leftdigits - dotplace
                )
        py_ass_parser302 = sign + intpart + fracpart + exp
        Sensor(302)
        return py_ass_parser302

    def to_eng_string(self, context=None):
        Sensor(303)
        """Convert to engineering-type string.

        Engineering notation has an exponent which is a multiple of 3, so there
        are up to 3 digits left of the decimal place.

        Same rules for when in exponential and when as a value as in __str__.
        """
        py_ass_parser304 = self.__str__(eng=True, context=context)
        Sensor(304)
        return py_ass_parser304

    def __neg__(self, context=None):
        Sensor(305)
        """Returns a copy with the sign switched.

        Rounds, if it has reason.
        """
        Sensor(306)
        if self._is_special:
            Sensor(307)
            ans = self._check_nans(context=context)
            Sensor(308)
            if ans:
                py_ass_parser309 = ans
                Sensor(309)
                return py_ass_parser309
        Sensor(310)
        if context is None:
            Sensor(311)
            context = getcontext()
        Sensor(312)
        if not self and context.rounding != ROUND_FLOOR:
            Sensor(313)
            ans = self.copy_abs()
        else:
            Sensor(314)
            ans = self.copy_negate()
        py_ass_parser315 = ans._fix(context)
        Sensor(315)
        return py_ass_parser315

    def __pos__(self, context=None):
        Sensor(316)
        """Returns a copy, unless it is a sNaN.

        Rounds the number (if more then precision digits)
        """
        Sensor(317)
        if self._is_special:
            Sensor(318)
            ans = self._check_nans(context=context)
            Sensor(319)
            if ans:
                py_ass_parser320 = ans
                Sensor(320)
                return py_ass_parser320
        Sensor(321)
        if context is None:
            Sensor(322)
            context = getcontext()
        Sensor(323)
        if not self and context.rounding != ROUND_FLOOR:
            Sensor(324)
            ans = self.copy_abs()
        else:
            Sensor(325)
            ans = Decimal(self)
        py_ass_parser326 = ans._fix(context)
        Sensor(326)
        return py_ass_parser326

    def __abs__(self, round=True, context=None):
        Sensor(327)
        """Returns the absolute value of self.

        If the keyword argument 'round' is false, do not round.  The
        expression self.__abs__(round=False) is equivalent to
        self.copy_abs().
        """
        Sensor(328)
        if not round:
            py_ass_parser329 = self.copy_abs()
            Sensor(329)
            return py_ass_parser329
        Sensor(330)
        if self._is_special:
            Sensor(331)
            ans = self._check_nans(context=context)
            Sensor(332)
            if ans:
                py_ass_parser333 = ans
                Sensor(333)
                return py_ass_parser333
        Sensor(334)
        if self._sign:
            Sensor(335)
            ans = self.__neg__(context=context)
        else:
            Sensor(336)
            ans = self.__pos__(context=context)
        py_ass_parser337 = ans
        Sensor(337)
        return py_ass_parser337

    def __add__(self, other, context=None):
        Sensor(338)
        """Returns self + other.

        -INF + INF (or the reverse) cause InvalidOperation errors.
        """
        other = _convert_other(other)
        Sensor(339)
        if other is NotImplemented:
            py_ass_parser340 = other
            Sensor(340)
            return py_ass_parser340
        Sensor(341)
        if context is None:
            Sensor(342)
            context = getcontext()
        Sensor(343)
        if self._is_special or other._is_special:
            Sensor(344)
            ans = self._check_nans(other, context)
            Sensor(345)
            if ans:
                py_ass_parser346 = ans
                Sensor(346)
                return py_ass_parser346
            Sensor(347)
            if self._isinfinity():
                Sensor(348)
                if self._sign != other._sign and other._isinfinity():
                    py_ass_parser349 = context._raise_error(InvalidOperation,
                        '-INF + INF')
                    Sensor(349)
                    return py_ass_parser349
                py_ass_parser350 = Decimal(self)
                Sensor(350)
                return py_ass_parser350
            Sensor(351)
            if other._isinfinity():
                py_ass_parser352 = Decimal(other)
                Sensor(352)
                return py_ass_parser352
        Sensor(353)
        exp = min(self._exp, other._exp)
        negativezero = 0
        Sensor(354)
        if context.rounding == ROUND_FLOOR and self._sign != other._sign:
            Sensor(355)
            negativezero = 1
        Sensor(356)
        if not self and not other:
            Sensor(357)
            sign = min(self._sign, other._sign)
            Sensor(358)
            if negativezero:
                Sensor(359)
                sign = 1
            Sensor(360)
            ans = _dec_from_triple(sign, '0', exp)
            ans = ans._fix(context)
            py_ass_parser361 = ans
            Sensor(361)
            return py_ass_parser361
        Sensor(362)
        if not self:
            Sensor(363)
            exp = max(exp, other._exp - context.prec - 1)
            ans = other._rescale(exp, context.rounding)
            ans = ans._fix(context)
            py_ass_parser364 = ans
            Sensor(364)
            return py_ass_parser364
        Sensor(365)
        if not other:
            Sensor(366)
            exp = max(exp, self._exp - context.prec - 1)
            ans = self._rescale(exp, context.rounding)
            ans = ans._fix(context)
            py_ass_parser367 = ans
            Sensor(367)
            return py_ass_parser367
        Sensor(368)
        op1 = _WorkRep(self)
        op2 = _WorkRep(other)
        op1, op2 = _normalize(op1, op2, context.prec)
        result = _WorkRep()
        Sensor(369)
        if op1.sign != op2.sign:
            Sensor(370)
            if op1.int == op2.int:
                Sensor(371)
                ans = _dec_from_triple(negativezero, '0', exp)
                ans = ans._fix(context)
                py_ass_parser372 = ans
                Sensor(372)
                return py_ass_parser372
            Sensor(373)
            if op1.int < op2.int:
                Sensor(374)
                op1, op2 = op2, op1
            Sensor(375)
            if op1.sign == 1:
                Sensor(376)
                result.sign = 1
                op1.sign, op2.sign = op2.sign, op1.sign
            else:
                Sensor(377)
                result.sign = 0
        else:
            Sensor(378)
            if op1.sign == 1:
                Sensor(379)
                result.sign = 1
                op1.sign, op2.sign = 0, 0
            else:
                Sensor(380)
                result.sign = 0
        Sensor(381)
        if op2.sign == 0:
            Sensor(382)
            result.int = op1.int + op2.int
        else:
            Sensor(383)
            result.int = op1.int - op2.int
        Sensor(384)
        result.exp = op1.exp
        ans = Decimal(result)
        ans = ans._fix(context)
        py_ass_parser385 = ans
        Sensor(385)
        return py_ass_parser385
    __radd__ = __add__

    def __sub__(self, other, context=None):
        Sensor(386)
        """Return self - other"""
        other = _convert_other(other)
        Sensor(387)
        if other is NotImplemented:
            py_ass_parser388 = other
            Sensor(388)
            return py_ass_parser388
        Sensor(389)
        if self._is_special or other._is_special:
            Sensor(390)
            ans = self._check_nans(other, context=context)
            Sensor(391)
            if ans:
                py_ass_parser392 = ans
                Sensor(392)
                return py_ass_parser392
        py_ass_parser393 = self.__add__(other.copy_negate(), context=context)
        Sensor(393)
        return py_ass_parser393

    def __rsub__(self, other, context=None):
        Sensor(394)
        """Return other - self"""
        other = _convert_other(other)
        Sensor(395)
        if other is NotImplemented:
            py_ass_parser396 = other
            Sensor(396)
            return py_ass_parser396
        py_ass_parser397 = other.__sub__(self, context=context)
        Sensor(397)
        return py_ass_parser397

    def __mul__(self, other, context=None):
        Sensor(398)
        """Return self * other.

        (+-) INF * 0 (or its reverse) raise InvalidOperation.
        """
        other = _convert_other(other)
        Sensor(399)
        if other is NotImplemented:
            py_ass_parser400 = other
            Sensor(400)
            return py_ass_parser400
        Sensor(401)
        if context is None:
            Sensor(402)
            context = getcontext()
        Sensor(403)
        resultsign = self._sign ^ other._sign
        Sensor(404)
        if self._is_special or other._is_special:
            Sensor(405)
            ans = self._check_nans(other, context)
            Sensor(406)
            if ans:
                py_ass_parser407 = ans
                Sensor(407)
                return py_ass_parser407
            Sensor(408)
            if self._isinfinity():
                Sensor(409)
                if not other:
                    py_ass_parser410 = context._raise_error(InvalidOperation,
                        '(+-)INF * 0')
                    Sensor(410)
                    return py_ass_parser410
                py_ass_parser411 = _SignedInfinity[resultsign]
                Sensor(411)
                return py_ass_parser411
            Sensor(412)
            if other._isinfinity():
                Sensor(413)
                if not self:
                    py_ass_parser414 = context._raise_error(InvalidOperation,
                        '0 * (+-)INF')
                    Sensor(414)
                    return py_ass_parser414
                py_ass_parser415 = _SignedInfinity[resultsign]
                Sensor(415)
                return py_ass_parser415
        Sensor(416)
        resultexp = self._exp + other._exp
        Sensor(417)
        if not self or not other:
            Sensor(418)
            ans = _dec_from_triple(resultsign, '0', resultexp)
            ans = ans._fix(context)
            py_ass_parser419 = ans
            Sensor(419)
            return py_ass_parser419
        Sensor(420)
        if self._int == '1':
            Sensor(421)
            ans = _dec_from_triple(resultsign, other._int, resultexp)
            ans = ans._fix(context)
            py_ass_parser422 = ans
            Sensor(422)
            return py_ass_parser422
        Sensor(423)
        if other._int == '1':
            Sensor(424)
            ans = _dec_from_triple(resultsign, self._int, resultexp)
            ans = ans._fix(context)
            py_ass_parser425 = ans
            Sensor(425)
            return py_ass_parser425
        Sensor(426)
        op1 = _WorkRep(self)
        op2 = _WorkRep(other)
        ans = _dec_from_triple(resultsign, str(op1.int * op2.int), resultexp)
        ans = ans._fix(context)
        py_ass_parser427 = ans
        Sensor(427)
        return py_ass_parser427
    __rmul__ = __mul__

    def __truediv__(self, other, context=None):
        Sensor(428)
        """Return self / other."""
        other = _convert_other(other)
        Sensor(429)
        if other is NotImplemented:
            py_ass_parser430 = NotImplemented
            Sensor(430)
            return py_ass_parser430
        Sensor(431)
        if context is None:
            Sensor(432)
            context = getcontext()
        Sensor(433)
        sign = self._sign ^ other._sign
        Sensor(434)
        if self._is_special or other._is_special:
            Sensor(435)
            ans = self._check_nans(other, context)
            Sensor(436)
            if ans:
                py_ass_parser437 = ans
                Sensor(437)
                return py_ass_parser437
            Sensor(438)
            if self._isinfinity() and other._isinfinity():
                py_ass_parser439 = context._raise_error(InvalidOperation,
                    '(+-)INF/(+-)INF')
                Sensor(439)
                return py_ass_parser439
            Sensor(440)
            if self._isinfinity():
                py_ass_parser441 = _SignedInfinity[sign]
                Sensor(441)
                return py_ass_parser441
            Sensor(442)
            if other._isinfinity():
                Sensor(443)
                context._raise_error(Clamped, 'Division by infinity')
                py_ass_parser444 = _dec_from_triple(sign, '0', context.Etiny())
                Sensor(444)
                return py_ass_parser444
        Sensor(445)
        if not other:
            Sensor(446)
            if not self:
                py_ass_parser447 = context._raise_error(DivisionUndefined,
                    '0 / 0')
                Sensor(447)
                return py_ass_parser447
            py_ass_parser448 = context._raise_error(DivisionByZero, 'x / 0',
                sign)
            Sensor(448)
            return py_ass_parser448
        Sensor(449)
        if not self:
            Sensor(450)
            exp = self._exp - other._exp
            coeff = 0
        else:
            Sensor(451)
            shift = len(other._int) - len(self._int) + context.prec + 1
            exp = self._exp - other._exp - shift
            op1 = _WorkRep(self)
            op2 = _WorkRep(other)
            Sensor(452)
            if shift >= 0:
                Sensor(453)
                coeff, remainder = divmod(op1.int * 10 ** shift, op2.int)
            else:
                Sensor(454)
                coeff, remainder = divmod(op1.int, op2.int * 10 ** -shift)
            Sensor(455)
            if remainder:
                Sensor(456)
                if coeff % 5 == 0:
                    Sensor(457)
                    coeff += 1
            else:
                Sensor(458)
                ideal_exp = self._exp - other._exp
                Sensor(459)
                while exp < ideal_exp and coeff % 10 == 0:
                    Sensor(460)
                    coeff //= 10
                    exp += 1
        Sensor(461)
        ans = _dec_from_triple(sign, str(coeff), exp)
        py_ass_parser462 = ans._fix(context)
        Sensor(462)
        return py_ass_parser462

    def _divide(self, other, context):
        Sensor(463)
        """Return (self // other, self % other), to context.prec precision.

        Assumes that neither self nor other is a NaN, that self is not
        infinite and that other is nonzero.
        """
        sign = self._sign ^ other._sign
        Sensor(464)
        if other._isinfinity():
            Sensor(465)
            ideal_exp = self._exp
        else:
            Sensor(466)
            ideal_exp = min(self._exp, other._exp)
        Sensor(467)
        expdiff = self.adjusted() - other.adjusted()
        Sensor(468)
        if not self or other._isinfinity() or expdiff <= -2:
            py_ass_parser469 = _dec_from_triple(sign, '0', 0), self._rescale(
                ideal_exp, context.rounding)
            Sensor(469)
            return py_ass_parser469
        Sensor(470)
        if expdiff <= context.prec:
            Sensor(471)
            op1 = _WorkRep(self)
            op2 = _WorkRep(other)
            Sensor(472)
            if op1.exp >= op2.exp:
                Sensor(473)
                op1.int *= 10 ** (op1.exp - op2.exp)
            else:
                Sensor(474)
                op2.int *= 10 ** (op2.exp - op1.exp)
            Sensor(475)
            q, r = divmod(op1.int, op2.int)
            Sensor(476)
            if q < 10 ** context.prec:
                py_ass_parser477 = _dec_from_triple(sign, str(q), 0
                    ), _dec_from_triple(self._sign, str(r), ideal_exp)
                Sensor(477)
                return py_ass_parser477
        Sensor(478)
        ans = context._raise_error(DivisionImpossible,
            'quotient too large in //, % or divmod')
        py_ass_parser479 = ans, ans
        Sensor(479)
        return py_ass_parser479

    def __rtruediv__(self, other, context=None):
        Sensor(480)
        """Swaps self/other and returns __truediv__."""
        other = _convert_other(other)
        Sensor(481)
        if other is NotImplemented:
            py_ass_parser482 = other
            Sensor(482)
            return py_ass_parser482
        py_ass_parser483 = other.__truediv__(self, context=context)
        Sensor(483)
        return py_ass_parser483
    __div__ = __truediv__
    __rdiv__ = __rtruediv__

    def __divmod__(self, other, context=None):
        Sensor(484)
        """
        Return (self // other, self % other)
        """
        other = _convert_other(other)
        Sensor(485)
        if other is NotImplemented:
            py_ass_parser486 = other
            Sensor(486)
            return py_ass_parser486
        Sensor(487)
        if context is None:
            Sensor(488)
            context = getcontext()
        Sensor(489)
        ans = self._check_nans(other, context)
        Sensor(490)
        if ans:
            py_ass_parser491 = ans, ans
            Sensor(491)
            return py_ass_parser491
        Sensor(492)
        sign = self._sign ^ other._sign
        Sensor(493)
        if self._isinfinity():
            Sensor(494)
            if other._isinfinity():
                Sensor(495)
                ans = context._raise_error(InvalidOperation, 'divmod(INF, INF)'
                    )
                py_ass_parser496 = ans, ans
                Sensor(496)
                return py_ass_parser496
            else:
                py_ass_parser497 = _SignedInfinity[sign], context._raise_error(
                    InvalidOperation, 'INF % x')
                Sensor(497)
                return py_ass_parser497
        Sensor(498)
        if not other:
            Sensor(499)
            if not self:
                Sensor(500)
                ans = context._raise_error(DivisionUndefined, 'divmod(0, 0)')
                py_ass_parser501 = ans, ans
                Sensor(501)
                return py_ass_parser501
            else:
                py_ass_parser502 = context._raise_error(DivisionByZero,
                    'x // 0', sign), context._raise_error(InvalidOperation,
                    'x % 0')
                Sensor(502)
                return py_ass_parser502
        Sensor(503)
        quotient, remainder = self._divide(other, context)
        remainder = remainder._fix(context)
        py_ass_parser504 = quotient, remainder
        Sensor(504)
        return py_ass_parser504

    def __rdivmod__(self, other, context=None):
        Sensor(505)
        """Swaps self/other and returns __divmod__."""
        other = _convert_other(other)
        Sensor(506)
        if other is NotImplemented:
            py_ass_parser507 = other
            Sensor(507)
            return py_ass_parser507
        py_ass_parser508 = other.__divmod__(self, context=context)
        Sensor(508)
        return py_ass_parser508

    def __mod__(self, other, context=None):
        Sensor(509)
        """
        self % other
        """
        other = _convert_other(other)
        Sensor(510)
        if other is NotImplemented:
            py_ass_parser511 = other
            Sensor(511)
            return py_ass_parser511
        Sensor(512)
        if context is None:
            Sensor(513)
            context = getcontext()
        Sensor(514)
        ans = self._check_nans(other, context)
        Sensor(515)
        if ans:
            py_ass_parser516 = ans
            Sensor(516)
            return py_ass_parser516
        Sensor(517)
        if self._isinfinity():
            py_ass_parser518 = context._raise_error(InvalidOperation, 'INF % x'
                )
            Sensor(518)
            return py_ass_parser518
        else:
            Sensor(519)
            if not other:
                Sensor(520)
                if self:
                    py_ass_parser521 = context._raise_error(InvalidOperation,
                        'x % 0')
                    Sensor(521)
                    return py_ass_parser521
                else:
                    py_ass_parser522 = context._raise_error(DivisionUndefined,
                        '0 % 0')
                    Sensor(522)
                    return py_ass_parser522
        Sensor(523)
        remainder = self._divide(other, context)[1]
        remainder = remainder._fix(context)
        py_ass_parser524 = remainder
        Sensor(524)
        return py_ass_parser524

    def __rmod__(self, other, context=None):
        Sensor(525)
        """Swaps self/other and returns __mod__."""
        other = _convert_other(other)
        Sensor(526)
        if other is NotImplemented:
            py_ass_parser527 = other
            Sensor(527)
            return py_ass_parser527
        py_ass_parser528 = other.__mod__(self, context=context)
        Sensor(528)
        return py_ass_parser528

    def remainder_near(self, other, context=None):
        Sensor(529)
        """
        Remainder nearest to 0-  abs(remainder-near) <= other/2
        """
        Sensor(530)
        if context is None:
            Sensor(531)
            context = getcontext()
        Sensor(532)
        other = _convert_other(other, raiseit=True)
        ans = self._check_nans(other, context)
        Sensor(533)
        if ans:
            py_ass_parser534 = ans
            Sensor(534)
            return py_ass_parser534
        Sensor(535)
        if self._isinfinity():
            py_ass_parser536 = context._raise_error(InvalidOperation,
                'remainder_near(infinity, x)')
            Sensor(536)
            return py_ass_parser536
        Sensor(537)
        if not other:
            Sensor(538)
            if self:
                py_ass_parser539 = context._raise_error(InvalidOperation,
                    'remainder_near(x, 0)')
                Sensor(539)
                return py_ass_parser539
            else:
                py_ass_parser540 = context._raise_error(DivisionUndefined,
                    'remainder_near(0, 0)')
                Sensor(540)
                return py_ass_parser540
        Sensor(541)
        if other._isinfinity():
            Sensor(542)
            ans = Decimal(self)
            py_ass_parser543 = ans._fix(context)
            Sensor(543)
            return py_ass_parser543
        Sensor(544)
        ideal_exponent = min(self._exp, other._exp)
        Sensor(545)
        if not self:
            Sensor(546)
            ans = _dec_from_triple(self._sign, '0', ideal_exponent)
            py_ass_parser547 = ans._fix(context)
            Sensor(547)
            return py_ass_parser547
        Sensor(548)
        expdiff = self.adjusted() - other.adjusted()
        Sensor(549)
        if expdiff >= context.prec + 1:
            py_ass_parser550 = context._raise_error(DivisionImpossible)
            Sensor(550)
            return py_ass_parser550
        Sensor(551)
        if expdiff <= -2:
            Sensor(552)
            ans = self._rescale(ideal_exponent, context.rounding)
            py_ass_parser553 = ans._fix(context)
            Sensor(553)
            return py_ass_parser553
        Sensor(554)
        op1 = _WorkRep(self)
        op2 = _WorkRep(other)
        Sensor(555)
        if op1.exp >= op2.exp:
            Sensor(556)
            op1.int *= 10 ** (op1.exp - op2.exp)
        else:
            Sensor(557)
            op2.int *= 10 ** (op2.exp - op1.exp)
        Sensor(558)
        q, r = divmod(op1.int, op2.int)
        Sensor(559)
        if 2 * r + (q & 1) > op2.int:
            Sensor(560)
            r -= op2.int
            q += 1
        Sensor(561)
        if q >= 10 ** context.prec:
            py_ass_parser562 = context._raise_error(DivisionImpossible)
            Sensor(562)
            return py_ass_parser562
        Sensor(563)
        sign = self._sign
        Sensor(564)
        if r < 0:
            Sensor(565)
            sign = 1 - sign
            r = -r
        Sensor(566)
        ans = _dec_from_triple(sign, str(r), ideal_exponent)
        py_ass_parser567 = ans._fix(context)
        Sensor(567)
        return py_ass_parser567

    def __floordiv__(self, other, context=None):
        Sensor(568)
        """self // other"""
        other = _convert_other(other)
        Sensor(569)
        if other is NotImplemented:
            py_ass_parser570 = other
            Sensor(570)
            return py_ass_parser570
        Sensor(571)
        if context is None:
            Sensor(572)
            context = getcontext()
        Sensor(573)
        ans = self._check_nans(other, context)
        Sensor(574)
        if ans:
            py_ass_parser575 = ans
            Sensor(575)
            return py_ass_parser575
        Sensor(576)
        if self._isinfinity():
            Sensor(577)
            if other._isinfinity():
                py_ass_parser578 = context._raise_error(InvalidOperation,
                    'INF // INF')
                Sensor(578)
                return py_ass_parser578
            else:
                py_ass_parser579 = _SignedInfinity[self._sign ^ other._sign]
                Sensor(579)
                return py_ass_parser579
        Sensor(580)
        if not other:
            Sensor(581)
            if self:
                py_ass_parser582 = context._raise_error(DivisionByZero,
                    'x // 0', self._sign ^ other._sign)
                Sensor(582)
                return py_ass_parser582
            else:
                py_ass_parser583 = context._raise_error(DivisionUndefined,
                    '0 // 0')
                Sensor(583)
                return py_ass_parser583
        py_ass_parser584 = self._divide(other, context)[0]
        Sensor(584)
        return py_ass_parser584

    def __rfloordiv__(self, other, context=None):
        Sensor(585)
        """Swaps self/other and returns __floordiv__."""
        other = _convert_other(other)
        Sensor(586)
        if other is NotImplemented:
            py_ass_parser587 = other
            Sensor(587)
            return py_ass_parser587
        py_ass_parser588 = other.__floordiv__(self, context=context)
        Sensor(588)
        return py_ass_parser588

    def __float__(self):
        Sensor(589)
        """Float representation."""
        Sensor(590)
        if self._isnan():
            Sensor(591)
            if self.is_snan():
                Sensor(592)
                raise ValueError('Cannot convert signaling NaN to float')
            Sensor(593)
            s = '-nan' if self._sign else 'nan'
        else:
            Sensor(594)
            s = str(self)
        py_ass_parser595 = float(s)
        Sensor(595)
        return py_ass_parser595

    def __int__(self):
        Sensor(596)
        """Converts self to an int, truncating if necessary."""
        Sensor(597)
        if self._is_special:
            Sensor(598)
            if self._isnan():
                Sensor(599)
                raise ValueError('Cannot convert NaN to integer')
            else:
                Sensor(600)
                if self._isinfinity():
                    Sensor(601)
                    raise OverflowError('Cannot convert infinity to integer')
        Sensor(602)
        s = (-1) ** self._sign
        Sensor(603)
        if self._exp >= 0:
            py_ass_parser604 = s * int(self._int) * 10 ** self._exp
            Sensor(604)
            return py_ass_parser604
        else:
            py_ass_parser605 = s * int(self._int[:self._exp] or '0')
            Sensor(605)
            return py_ass_parser605
        Sensor(606)
    __trunc__ = __int__

    def real(self):
        py_ass_parser607 = self
        Sensor(607)
        return py_ass_parser607
    real = property(real)

    def imag(self):
        py_ass_parser608 = Decimal(0)
        Sensor(608)
        return py_ass_parser608
    imag = property(imag)

    def conjugate(self):
        py_ass_parser609 = self
        Sensor(609)
        return py_ass_parser609

    def __complex__(self):
        py_ass_parser610 = complex(float(self))
        Sensor(610)
        return py_ass_parser610

    def __long__(self):
        Sensor(611)
        """Converts to a long.

        Equivalent to long(int(self))
        """
        py_ass_parser612 = long(self.__int__())
        Sensor(612)
        return py_ass_parser612

    def _fix_nan(self, context):
        Sensor(613)
        """Decapitate the payload of a NaN to fit the context"""
        payload = self._int
        max_payload_len = context.prec - context._clamp
        Sensor(614)
        if len(payload) > max_payload_len:
            Sensor(615)
            payload = payload[len(payload) - max_payload_len:].lstrip('0')
            py_ass_parser616 = _dec_from_triple(self._sign, payload, self.
                _exp, True)
            Sensor(616)
            return py_ass_parser616
        py_ass_parser617 = Decimal(self)
        Sensor(617)
        return py_ass_parser617

    def _fix(self, context):
        Sensor(618)
        """Round if it is necessary to keep self within prec precision.

        Rounds and fixes the exponent.  Does not raise on a sNaN.

        Arguments:
        self - Decimal instance
        context - context used.
        """
        Sensor(619)
        if self._is_special:
            Sensor(620)
            if self._isnan():
                py_ass_parser621 = self._fix_nan(context)
                Sensor(621)
                return py_ass_parser621
            else:
                py_ass_parser622 = Decimal(self)
                Sensor(622)
                return py_ass_parser622
        Sensor(623)
        Etiny = context.Etiny()
        Etop = context.Etop()
        Sensor(624)
        if not self:
            Sensor(625)
            exp_max = [context.Emax, Etop][context._clamp]
            new_exp = min(max(self._exp, Etiny), exp_max)
            Sensor(626)
            if new_exp != self._exp:
                Sensor(627)
                context._raise_error(Clamped)
                py_ass_parser628 = _dec_from_triple(self._sign, '0', new_exp)
                Sensor(628)
                return py_ass_parser628
            else:
                py_ass_parser629 = Decimal(self)
                Sensor(629)
                return py_ass_parser629
        Sensor(630)
        exp_min = len(self._int) + self._exp - context.prec
        Sensor(631)
        if exp_min > Etop:
            Sensor(632)
            ans = context._raise_error(Overflow, 'above Emax', self._sign)
            context._raise_error(Inexact)
            context._raise_error(Rounded)
            py_ass_parser633 = ans
            Sensor(633)
            return py_ass_parser633
        Sensor(634)
        self_is_subnormal = exp_min < Etiny
        Sensor(635)
        if self_is_subnormal:
            Sensor(636)
            exp_min = Etiny
        Sensor(637)
        if self._exp < exp_min:
            Sensor(638)
            digits = len(self._int) + self._exp - exp_min
            Sensor(639)
            if digits < 0:
                Sensor(640)
                self = _dec_from_triple(self._sign, '1', exp_min - 1)
                digits = 0
            Sensor(641)
            rounding_method = self._pick_rounding_function[context.rounding]
            changed = rounding_method(self, digits)
            coeff = self._int[:digits] or '0'
            Sensor(642)
            if changed > 0:
                Sensor(643)
                coeff = str(int(coeff) + 1)
                Sensor(644)
                if len(coeff) > context.prec:
                    Sensor(645)
                    coeff = coeff[:-1]
                    exp_min += 1
            Sensor(646)
            if exp_min > Etop:
                Sensor(647)
                ans = context._raise_error(Overflow, 'above Emax', self._sign)
            else:
                Sensor(648)
                ans = _dec_from_triple(self._sign, coeff, exp_min)
            Sensor(649)
            if changed and self_is_subnormal:
                Sensor(650)
                context._raise_error(Underflow)
            Sensor(651)
            if self_is_subnormal:
                Sensor(652)
                context._raise_error(Subnormal)
            Sensor(653)
            if changed:
                Sensor(654)
                context._raise_error(Inexact)
            Sensor(655)
            context._raise_error(Rounded)
            Sensor(656)
            if not ans:
                Sensor(657)
                context._raise_error(Clamped)
            py_ass_parser658 = ans
            Sensor(658)
            return py_ass_parser658
        Sensor(659)
        if self_is_subnormal:
            Sensor(660)
            context._raise_error(Subnormal)
        Sensor(661)
        if context._clamp == 1 and self._exp > Etop:
            Sensor(662)
            context._raise_error(Clamped)
            self_padded = self._int + '0' * (self._exp - Etop)
            py_ass_parser663 = _dec_from_triple(self._sign, self_padded, Etop)
            Sensor(663)
            return py_ass_parser663
        py_ass_parser664 = Decimal(self)
        Sensor(664)
        return py_ass_parser664

    def _round_down(self, prec):
        Sensor(665)
        """Also known as round-towards-0, truncate."""
        Sensor(666)
        if _all_zeros(self._int, prec):
            py_ass_parser667 = 0
            Sensor(667)
            return py_ass_parser667
        else:
            py_ass_parser668 = -1
            Sensor(668)
            return py_ass_parser668
        Sensor(669)

    def _round_up(self, prec):
        Sensor(670)
        """Rounds away from 0."""
        py_ass_parser671 = -self._round_down(prec)
        Sensor(671)
        return py_ass_parser671

    def _round_half_up(self, prec):
        Sensor(672)
        """Rounds 5 up (away from 0)"""
        Sensor(673)
        if self._int[prec] in '56789':
            py_ass_parser674 = 1
            Sensor(674)
            return py_ass_parser674
        else:
            Sensor(675)
            if _all_zeros(self._int, prec):
                py_ass_parser676 = 0
                Sensor(676)
                return py_ass_parser676
            else:
                py_ass_parser677 = -1
                Sensor(677)
                return py_ass_parser677
        Sensor(678)

    def _round_half_down(self, prec):
        Sensor(679)
        """Round 5 down"""
        Sensor(680)
        if _exact_half(self._int, prec):
            py_ass_parser681 = -1
            Sensor(681)
            return py_ass_parser681
        else:
            py_ass_parser682 = self._round_half_up(prec)
            Sensor(682)
            return py_ass_parser682
        Sensor(683)

    def _round_half_even(self, prec):
        Sensor(684)
        """Round 5 to even, rest to nearest."""
        Sensor(685)
        if _exact_half(self._int, prec) and (prec == 0 or self._int[prec - 
            1] in '02468'):
            py_ass_parser686 = -1
            Sensor(686)
            return py_ass_parser686
        else:
            py_ass_parser687 = self._round_half_up(prec)
            Sensor(687)
            return py_ass_parser687
        Sensor(688)

    def _round_ceiling(self, prec):
        Sensor(689)
        """Rounds up (not away from 0 if negative.)"""
        Sensor(690)
        if self._sign:
            py_ass_parser691 = self._round_down(prec)
            Sensor(691)
            return py_ass_parser691
        else:
            py_ass_parser692 = -self._round_down(prec)
            Sensor(692)
            return py_ass_parser692
        Sensor(693)

    def _round_floor(self, prec):
        Sensor(694)
        """Rounds down (not towards 0 if negative)"""
        Sensor(695)
        if not self._sign:
            py_ass_parser696 = self._round_down(prec)
            Sensor(696)
            return py_ass_parser696
        else:
            py_ass_parser697 = -self._round_down(prec)
            Sensor(697)
            return py_ass_parser697
        Sensor(698)

    def _round_05up(self, prec):
        Sensor(699)
        """Round down unless digit prec-1 is 0 or 5."""
        Sensor(700)
        if prec and self._int[prec - 1] not in '05':
            py_ass_parser701 = self._round_down(prec)
            Sensor(701)
            return py_ass_parser701
        else:
            py_ass_parser702 = -self._round_down(prec)
            Sensor(702)
            return py_ass_parser702
        Sensor(703)
    _pick_rounding_function = dict(ROUND_DOWN=_round_down, ROUND_UP=
        _round_up, ROUND_HALF_UP=_round_half_up, ROUND_HALF_DOWN=
        _round_half_down, ROUND_HALF_EVEN=_round_half_even, ROUND_CEILING=
        _round_ceiling, ROUND_FLOOR=_round_floor, ROUND_05UP=_round_05up)

    def fma(self, other, third, context=None):
        Sensor(704)
        """Fused multiply-add.

        Returns self*other+third with no rounding of the intermediate
        product self*other.

        self and other are multiplied together, with no rounding of
        the result.  The third operand is then added to the result,
        and a single final rounding is performed.
        """
        other = _convert_other(other, raiseit=True)
        Sensor(705)
        if self._is_special or other._is_special:
            Sensor(706)
            if context is None:
                Sensor(707)
                context = getcontext()
            Sensor(708)
            if self._exp == 'N':
                py_ass_parser709 = context._raise_error(InvalidOperation,
                    'sNaN', self)
                Sensor(709)
                return py_ass_parser709
            Sensor(710)
            if other._exp == 'N':
                py_ass_parser711 = context._raise_error(InvalidOperation,
                    'sNaN', other)
                Sensor(711)
                return py_ass_parser711
            Sensor(712)
            if self._exp == 'n':
                Sensor(713)
                product = self
            else:
                Sensor(714)
                if other._exp == 'n':
                    Sensor(715)
                    product = other
                else:
                    Sensor(716)
                    if self._exp == 'F':
                        Sensor(717)
                        if not other:
                            py_ass_parser718 = context._raise_error(
                                InvalidOperation, 'INF * 0 in fma')
                            Sensor(718)
                            return py_ass_parser718
                        Sensor(719)
                        product = _SignedInfinity[self._sign ^ other._sign]
                    else:
                        Sensor(720)
                        if other._exp == 'F':
                            Sensor(721)
                            if not self:
                                py_ass_parser722 = context._raise_error(
                                    InvalidOperation, '0 * INF in fma')
                                Sensor(722)
                                return py_ass_parser722
                            Sensor(723)
                            product = _SignedInfinity[self._sign ^ other._sign]
        else:
            Sensor(724)
            product = _dec_from_triple(self._sign ^ other._sign, str(int(
                self._int) * int(other._int)), self._exp + other._exp)
        Sensor(725)
        third = _convert_other(third, raiseit=True)
        py_ass_parser726 = product.__add__(third, context)
        Sensor(726)
        return py_ass_parser726

    def _power_modulo(self, other, modulo, context=None):
        Sensor(727)
        """Three argument version of __pow__"""
        other = _convert_other(other, raiseit=True)
        modulo = _convert_other(modulo, raiseit=True)
        Sensor(728)
        if context is None:
            Sensor(729)
            context = getcontext()
        Sensor(730)
        self_is_nan = self._isnan()
        other_is_nan = other._isnan()
        modulo_is_nan = modulo._isnan()
        Sensor(731)
        if self_is_nan or other_is_nan or modulo_is_nan:
            Sensor(732)
            if self_is_nan == 2:
                py_ass_parser733 = context._raise_error(InvalidOperation,
                    'sNaN', self)
                Sensor(733)
                return py_ass_parser733
            Sensor(734)
            if other_is_nan == 2:
                py_ass_parser735 = context._raise_error(InvalidOperation,
                    'sNaN', other)
                Sensor(735)
                return py_ass_parser735
            Sensor(736)
            if modulo_is_nan == 2:
                py_ass_parser737 = context._raise_error(InvalidOperation,
                    'sNaN', modulo)
                Sensor(737)
                return py_ass_parser737
            Sensor(738)
            if self_is_nan:
                py_ass_parser739 = self._fix_nan(context)
                Sensor(739)
                return py_ass_parser739
            Sensor(740)
            if other_is_nan:
                py_ass_parser741 = other._fix_nan(context)
                Sensor(741)
                return py_ass_parser741
            py_ass_parser742 = modulo._fix_nan(context)
            Sensor(742)
            return py_ass_parser742
        Sensor(743)
        if not (self._isinteger() and other._isinteger() and modulo.
            _isinteger()):
            py_ass_parser744 = context._raise_error(InvalidOperation,
                'pow() 3rd argument not allowed unless all arguments are integers'
                )
            Sensor(744)
            return py_ass_parser744
        Sensor(745)
        if other < 0:
            py_ass_parser746 = context._raise_error(InvalidOperation,
                'pow() 2nd argument cannot be negative when 3rd argument specified'
                )
            Sensor(746)
            return py_ass_parser746
        Sensor(747)
        if not modulo:
            py_ass_parser748 = context._raise_error(InvalidOperation,
                'pow() 3rd argument cannot be 0')
            Sensor(748)
            return py_ass_parser748
        Sensor(749)
        if modulo.adjusted() >= context.prec:
            py_ass_parser750 = context._raise_error(InvalidOperation,
                'insufficient precision: pow() 3rd argument must not have more than precision digits'
                )
            Sensor(750)
            return py_ass_parser750
        Sensor(751)
        if not other and not self:
            py_ass_parser752 = context._raise_error(InvalidOperation,
                'at least one of pow() 1st argument and 2nd argument must be nonzero ;0**0 is not defined'
                )
            Sensor(752)
            return py_ass_parser752
        Sensor(753)
        if other._iseven():
            Sensor(754)
            sign = 0
        else:
            Sensor(755)
            sign = self._sign
        Sensor(756)
        modulo = abs(int(modulo))
        base = _WorkRep(self.to_integral_value())
        exponent = _WorkRep(other.to_integral_value())
        base = base.int % modulo * pow(10, base.exp, modulo) % modulo
        Sensor(757)
        for i in xrange(exponent.exp):
            Sensor(758)
            base = pow(base, 10, modulo)
        Sensor(759)
        base = pow(base, exponent.int, modulo)
        py_ass_parser760 = _dec_from_triple(sign, str(base), 0)
        Sensor(760)
        return py_ass_parser760

    def _power_exact(self, other, p):
        Sensor(761)
        """Attempt to compute self**other exactly.

        Given Decimals self and other and an integer p, attempt to
        compute an exact result for the power self**other, with p
        digits of precision.  Return None if self**other is not
        exactly representable in p digits.

        Assumes that elimination of special cases has already been
        performed: self and other must both be nonspecial; self must
        be positive and not numerically equal to 1; other must be
        nonzero.  For efficiency, other._exp should not be too large,
        so that 10**abs(other._exp) is a feasible calculation."""
        x = _WorkRep(self)
        xc, xe = x.int, x.exp
        Sensor(762)
        while xc % 10 == 0:
            Sensor(763)
            xc //= 10
            xe += 1
        Sensor(764)
        y = _WorkRep(other)
        yc, ye = y.int, y.exp
        Sensor(765)
        while yc % 10 == 0:
            Sensor(766)
            yc //= 10
            ye += 1
        Sensor(767)
        if xc == 1:
            Sensor(768)
            xe *= yc
            Sensor(769)
            while xe % 10 == 0:
                Sensor(770)
                xe //= 10
                ye += 1
            Sensor(771)
            if ye < 0:
                py_ass_parser772 = None
                Sensor(772)
                return py_ass_parser772
            Sensor(773)
            exponent = xe * 10 ** ye
            Sensor(774)
            if y.sign == 1:
                Sensor(775)
                exponent = -exponent
            Sensor(776)
            if other._isinteger() and other._sign == 0:
                Sensor(777)
                ideal_exponent = self._exp * int(other)
                zeros = min(exponent - ideal_exponent, p - 1)
            else:
                Sensor(778)
                zeros = 0
            py_ass_parser779 = _dec_from_triple(0, '1' + '0' * zeros, 
                exponent - zeros)
            Sensor(779)
            return py_ass_parser779
        Sensor(780)
        if y.sign == 1:
            Sensor(781)
            last_digit = xc % 10
            Sensor(782)
            if last_digit in (2, 4, 6, 8):
                Sensor(783)
                if xc & -xc != xc:
                    py_ass_parser784 = None
                    Sensor(784)
                    return py_ass_parser784
                Sensor(785)
                e = _nbits(xc) - 1
                emax = p * 93 // 65
                Sensor(786)
                if ye >= len(str(emax)):
                    py_ass_parser787 = None
                    Sensor(787)
                    return py_ass_parser787
                Sensor(788)
                e = _decimal_lshift_exact(e * yc, ye)
                xe = _decimal_lshift_exact(xe * yc, ye)
                Sensor(789)
                if e is None or xe is None:
                    py_ass_parser790 = None
                    Sensor(790)
                    return py_ass_parser790
                Sensor(791)
                if e > emax:
                    py_ass_parser792 = None
                    Sensor(792)
                    return py_ass_parser792
                Sensor(793)
                xc = 5 ** e
            else:
                Sensor(794)
                if last_digit == 5:
                    Sensor(795)
                    e = _nbits(xc) * 28 // 65
                    xc, remainder = divmod(5 ** e, xc)
                    Sensor(796)
                    if remainder:
                        py_ass_parser797 = None
                        Sensor(797)
                        return py_ass_parser797
                    Sensor(798)
                    while xc % 5 == 0:
                        Sensor(799)
                        xc //= 5
                        e -= 1
                    Sensor(800)
                    emax = p * 10 // 3
                    Sensor(801)
                    if ye >= len(str(emax)):
                        py_ass_parser802 = None
                        Sensor(802)
                        return py_ass_parser802
                    Sensor(803)
                    e = _decimal_lshift_exact(e * yc, ye)
                    xe = _decimal_lshift_exact(xe * yc, ye)
                    Sensor(804)
                    if e is None or xe is None:
                        py_ass_parser805 = None
                        Sensor(805)
                        return py_ass_parser805
                    Sensor(806)
                    if e > emax:
                        py_ass_parser807 = None
                        Sensor(807)
                        return py_ass_parser807
                    Sensor(808)
                    xc = 2 ** e
                else:
                    py_ass_parser809 = None
                    Sensor(809)
                    return py_ass_parser809
            Sensor(810)
            if xc >= 10 ** p:
                py_ass_parser811 = None
                Sensor(811)
                return py_ass_parser811
            Sensor(812)
            xe = -e - xe
            py_ass_parser813 = _dec_from_triple(0, str(xc), xe)
            Sensor(813)
            return py_ass_parser813
        Sensor(814)
        if ye >= 0:
            Sensor(815)
            m, n = yc * 10 ** ye, 1
        else:
            Sensor(816)
            if xe != 0 and len(str(abs(yc * xe))) <= -ye:
                py_ass_parser817 = None
                Sensor(817)
                return py_ass_parser817
            Sensor(818)
            xc_bits = _nbits(xc)
            Sensor(819)
            if xc != 1 and len(str(abs(yc) * xc_bits)) <= -ye:
                py_ass_parser820 = None
                Sensor(820)
                return py_ass_parser820
            Sensor(821)
            m, n = yc, 10 ** -ye
            Sensor(822)
            while m % 2 == n % 2 == 0:
                Sensor(823)
                m //= 2
                n //= 2
            Sensor(824)
            while m % 5 == n % 5 == 0:
                Sensor(825)
                m //= 5
                n //= 5
        Sensor(826)
        if n > 1:
            Sensor(827)
            if xc != 1 and xc_bits <= n:
                py_ass_parser828 = None
                Sensor(828)
                return py_ass_parser828
            Sensor(829)
            xe, rem = divmod(xe, n)
            Sensor(830)
            if rem != 0:
                py_ass_parser831 = None
                Sensor(831)
                return py_ass_parser831
            Sensor(832)
            a = 1L << -(-_nbits(xc) // n)
            Sensor(833)
            while True:
                Sensor(834)
                q, r = divmod(xc, a ** (n - 1))
                Sensor(835)
                if a <= q:
                    Sensor(836)
                    break
                else:
                    Sensor(837)
                    a = (a * (n - 1) + q) // n
            Sensor(838)
            if not (a == q and r == 0):
                py_ass_parser839 = None
                Sensor(839)
                return py_ass_parser839
            Sensor(840)
            xc = a
        Sensor(841)
        if xc > 1 and m > p * 100 // _log10_lb(xc):
            py_ass_parser842 = None
            Sensor(842)
            return py_ass_parser842
        Sensor(843)
        xc = xc ** m
        xe *= m
        Sensor(844)
        if xc > 10 ** p:
            py_ass_parser845 = None
            Sensor(845)
            return py_ass_parser845
        Sensor(846)
        str_xc = str(xc)
        Sensor(847)
        if other._isinteger() and other._sign == 0:
            Sensor(848)
            ideal_exponent = self._exp * int(other)
            zeros = min(xe - ideal_exponent, p - len(str_xc))
        else:
            Sensor(849)
            zeros = 0
        py_ass_parser850 = _dec_from_triple(0, str_xc + '0' * zeros, xe - zeros
            )
        Sensor(850)
        return py_ass_parser850

    def __pow__(self, other, modulo=None, context=None):
        Sensor(851)
        """Return self ** other [ % modulo].

        With two arguments, compute self**other.

        With three arguments, compute (self**other) % modulo.  For the
        three argument form, the following restrictions on the
        arguments hold:

         - all three arguments must be integral
         - other must be nonnegative
         - either self or other (or both) must be nonzero
         - modulo must be nonzero and must have at most p digits,
           where p is the context precision.

        If any of these restrictions is violated the InvalidOperation
        flag is raised.

        The result of pow(self, other, modulo) is identical to the
        result that would be obtained by computing (self**other) %
        modulo with unbounded precision, but is computed more
        efficiently.  It is always exact.
        """
        Sensor(852)
        if modulo is not None:
            py_ass_parser853 = self._power_modulo(other, modulo, context)
            Sensor(853)
            return py_ass_parser853
        Sensor(854)
        other = _convert_other(other)
        Sensor(855)
        if other is NotImplemented:
            py_ass_parser856 = other
            Sensor(856)
            return py_ass_parser856
        Sensor(857)
        if context is None:
            Sensor(858)
            context = getcontext()
        Sensor(859)
        ans = self._check_nans(other, context)
        Sensor(860)
        if ans:
            py_ass_parser861 = ans
            Sensor(861)
            return py_ass_parser861
        Sensor(862)
        if not other:
            Sensor(863)
            if not self:
                py_ass_parser864 = context._raise_error(InvalidOperation,
                    '0 ** 0')
                Sensor(864)
                return py_ass_parser864
            else:
                py_ass_parser865 = _One
                Sensor(865)
                return py_ass_parser865
        Sensor(866)
        result_sign = 0
        Sensor(867)
        if self._sign == 1:
            Sensor(868)
            if other._isinteger():
                Sensor(869)
                if not other._iseven():
                    Sensor(870)
                    result_sign = 1
            else:
                Sensor(871)
                if self:
                    py_ass_parser872 = context._raise_error(InvalidOperation,
                        'x ** y with x negative and y not an integer')
                    Sensor(872)
                    return py_ass_parser872
            Sensor(873)
            self = self.copy_negate()
        Sensor(874)
        if not self:
            Sensor(875)
            if other._sign == 0:
                py_ass_parser876 = _dec_from_triple(result_sign, '0', 0)
                Sensor(876)
                return py_ass_parser876
            else:
                py_ass_parser877 = _SignedInfinity[result_sign]
                Sensor(877)
                return py_ass_parser877
        Sensor(878)
        if self._isinfinity():
            Sensor(879)
            if other._sign == 0:
                py_ass_parser880 = _SignedInfinity[result_sign]
                Sensor(880)
                return py_ass_parser880
            else:
                py_ass_parser881 = _dec_from_triple(result_sign, '0', 0)
                Sensor(881)
                return py_ass_parser881
        Sensor(882)
        if self == _One:
            Sensor(883)
            if other._isinteger():
                Sensor(884)
                if other._sign == 1:
                    Sensor(885)
                    multiplier = 0
                else:
                    Sensor(886)
                    if other > context.prec:
                        Sensor(887)
                        multiplier = context.prec
                    else:
                        Sensor(888)
                        multiplier = int(other)
                Sensor(889)
                exp = self._exp * multiplier
                Sensor(890)
                if exp < 1 - context.prec:
                    Sensor(891)
                    exp = 1 - context.prec
                    context._raise_error(Rounded)
            else:
                Sensor(892)
                context._raise_error(Inexact)
                context._raise_error(Rounded)
                exp = 1 - context.prec
            py_ass_parser893 = _dec_from_triple(result_sign, '1' + '0' * -
                exp, exp)
            Sensor(893)
            return py_ass_parser893
        Sensor(894)
        self_adj = self.adjusted()
        Sensor(895)
        if other._isinfinity():
            Sensor(896)
            if (other._sign == 0) == (self_adj < 0):
                py_ass_parser897 = _dec_from_triple(result_sign, '0', 0)
                Sensor(897)
                return py_ass_parser897
            else:
                py_ass_parser898 = _SignedInfinity[result_sign]
                Sensor(898)
                return py_ass_parser898
        Sensor(899)
        ans = None
        exact = False
        bound = self._log10_exp_bound() + other.adjusted()
        Sensor(900)
        if (self_adj >= 0) == (other._sign == 0):
            Sensor(901)
            if bound >= len(str(context.Emax)):
                Sensor(902)
                ans = _dec_from_triple(result_sign, '1', context.Emax + 1)
        else:
            Sensor(903)
            Etiny = context.Etiny()
            Sensor(904)
            if bound >= len(str(-Etiny)):
                Sensor(905)
                ans = _dec_from_triple(result_sign, '1', Etiny - 1)
        Sensor(906)
        if ans is None:
            Sensor(907)
            ans = self._power_exact(other, context.prec + 1)
            Sensor(908)
            if ans is not None:
                Sensor(909)
                if result_sign == 1:
                    Sensor(910)
                    ans = _dec_from_triple(1, ans._int, ans._exp)
                Sensor(911)
                exact = True
        Sensor(912)
        if ans is None:
            Sensor(913)
            p = context.prec
            x = _WorkRep(self)
            xc, xe = x.int, x.exp
            y = _WorkRep(other)
            yc, ye = y.int, y.exp
            Sensor(914)
            if y.sign == 1:
                Sensor(915)
                yc = -yc
            Sensor(916)
            extra = 3
            Sensor(917)
            while True:
                Sensor(918)
                coeff, exp = _dpower(xc, xe, yc, ye, p + extra)
                Sensor(919)
                if coeff % (5 * 10 ** (len(str(coeff)) - p - 1)):
                    Sensor(920)
                    break
                Sensor(921)
                extra += 3
            Sensor(922)
            ans = _dec_from_triple(result_sign, str(coeff), exp)
        Sensor(923)
        if exact and not other._isinteger():
            Sensor(924)
            if len(ans._int) <= context.prec:
                Sensor(925)
                expdiff = context.prec + 1 - len(ans._int)
                ans = _dec_from_triple(ans._sign, ans._int + '0' * expdiff,
                    ans._exp - expdiff)
            Sensor(926)
            newcontext = context.copy()
            newcontext.clear_flags()
            Sensor(927)
            for exception in _signals:
                Sensor(928)
                newcontext.traps[exception] = 0
            Sensor(929)
            ans = ans._fix(newcontext)
            newcontext._raise_error(Inexact)
            Sensor(930)
            if newcontext.flags[Subnormal]:
                Sensor(931)
                newcontext._raise_error(Underflow)
            Sensor(932)
            if newcontext.flags[Overflow]:
                Sensor(933)
                context._raise_error(Overflow, 'above Emax', ans._sign)
            Sensor(934)
            for exception in (Underflow, Subnormal, Inexact, Rounded, Clamped):
                Sensor(935)
                if newcontext.flags[exception]:
                    Sensor(936)
                    context._raise_error(exception)
        else:
            Sensor(937)
            ans = ans._fix(context)
        py_ass_parser938 = ans
        Sensor(938)
        return py_ass_parser938

    def __rpow__(self, other, context=None):
        Sensor(939)
        """Swaps self/other and returns __pow__."""
        other = _convert_other(other)
        Sensor(940)
        if other is NotImplemented:
            py_ass_parser941 = other
            Sensor(941)
            return py_ass_parser941
        py_ass_parser942 = other.__pow__(self, context=context)
        Sensor(942)
        return py_ass_parser942

    def normalize(self, context=None):
        Sensor(943)
        """Normalize- strip trailing 0s, change anything equal to 0 to 0e0"""
        Sensor(944)
        if context is None:
            Sensor(945)
            context = getcontext()
        Sensor(946)
        if self._is_special:
            Sensor(947)
            ans = self._check_nans(context=context)
            Sensor(948)
            if ans:
                py_ass_parser949 = ans
                Sensor(949)
                return py_ass_parser949
        Sensor(950)
        dup = self._fix(context)
        Sensor(951)
        if dup._isinfinity():
            py_ass_parser952 = dup
            Sensor(952)
            return py_ass_parser952
        Sensor(953)
        if not dup:
            py_ass_parser954 = _dec_from_triple(dup._sign, '0', 0)
            Sensor(954)
            return py_ass_parser954
        Sensor(955)
        exp_max = [context.Emax, context.Etop()][context._clamp]
        end = len(dup._int)
        exp = dup._exp
        Sensor(956)
        while dup._int[end - 1] == '0' and exp < exp_max:
            Sensor(957)
            exp += 1
            end -= 1
        py_ass_parser958 = _dec_from_triple(dup._sign, dup._int[:end], exp)
        Sensor(958)
        return py_ass_parser958

    def quantize(self, exp, rounding=None, context=None, watchexp=True):
        Sensor(959)
        """Quantize self so its exponent is the same as that of exp.

        Similar to self._rescale(exp._exp) but with error checking.
        """
        exp = _convert_other(exp, raiseit=True)
        Sensor(960)
        if context is None:
            Sensor(961)
            context = getcontext()
        Sensor(962)
        if rounding is None:
            Sensor(963)
            rounding = context.rounding
        Sensor(964)
        if self._is_special or exp._is_special:
            Sensor(965)
            ans = self._check_nans(exp, context)
            Sensor(966)
            if ans:
                py_ass_parser967 = ans
                Sensor(967)
                return py_ass_parser967
            Sensor(968)
            if exp._isinfinity() or self._isinfinity():
                Sensor(969)
                if exp._isinfinity() and self._isinfinity():
                    py_ass_parser970 = Decimal(self)
                    Sensor(970)
                    return py_ass_parser970
                py_ass_parser971 = context._raise_error(InvalidOperation,
                    'quantize with one INF')
                Sensor(971)
                return py_ass_parser971
        Sensor(972)
        if not watchexp:
            Sensor(973)
            ans = self._rescale(exp._exp, rounding)
            Sensor(974)
            if ans._exp > self._exp:
                Sensor(975)
                context._raise_error(Rounded)
                Sensor(976)
                if ans != self:
                    Sensor(977)
                    context._raise_error(Inexact)
            py_ass_parser978 = ans
            Sensor(978)
            return py_ass_parser978
        Sensor(979)
        if not context.Etiny() <= exp._exp <= context.Emax:
            py_ass_parser980 = context._raise_error(InvalidOperation,
                'target exponent out of bounds in quantize')
            Sensor(980)
            return py_ass_parser980
        Sensor(981)
        if not self:
            Sensor(982)
            ans = _dec_from_triple(self._sign, '0', exp._exp)
            py_ass_parser983 = ans._fix(context)
            Sensor(983)
            return py_ass_parser983
        Sensor(984)
        self_adjusted = self.adjusted()
        Sensor(985)
        if self_adjusted > context.Emax:
            py_ass_parser986 = context._raise_error(InvalidOperation,
                'exponent of quantize result too large for current context')
            Sensor(986)
            return py_ass_parser986
        Sensor(987)
        if self_adjusted - exp._exp + 1 > context.prec:
            py_ass_parser988 = context._raise_error(InvalidOperation,
                'quantize result has too many digits for current context')
            Sensor(988)
            return py_ass_parser988
        Sensor(989)
        ans = self._rescale(exp._exp, rounding)
        Sensor(990)
        if ans.adjusted() > context.Emax:
            py_ass_parser991 = context._raise_error(InvalidOperation,
                'exponent of quantize result too large for current context')
            Sensor(991)
            return py_ass_parser991
        Sensor(992)
        if len(ans._int) > context.prec:
            py_ass_parser993 = context._raise_error(InvalidOperation,
                'quantize result has too many digits for current context')
            Sensor(993)
            return py_ass_parser993
        Sensor(994)
        if ans and ans.adjusted() < context.Emin:
            Sensor(995)
            context._raise_error(Subnormal)
        Sensor(996)
        if ans._exp > self._exp:
            Sensor(997)
            if ans != self:
                Sensor(998)
                context._raise_error(Inexact)
            Sensor(999)
            context._raise_error(Rounded)
        Sensor(1000)
        ans = ans._fix(context)
        py_ass_parser1001 = ans
        Sensor(1001)
        return py_ass_parser1001

    def same_quantum(self, other):
        Sensor(1002)
        """Return True if self and other have the same exponent; otherwise
        return False.

        If either operand is a special value, the following rules are used:
           * return True if both operands are infinities
           * return True if both operands are NaNs
           * otherwise, return False.
        """
        other = _convert_other(other, raiseit=True)
        Sensor(1003)
        if self._is_special or other._is_special:
            py_ass_parser1004 = self.is_nan() and other.is_nan(
                ) or self.is_infinite() and other.is_infinite()
            Sensor(1004)
            return py_ass_parser1004
        py_ass_parser1005 = self._exp == other._exp
        Sensor(1005)
        return py_ass_parser1005

    def _rescale(self, exp, rounding):
        Sensor(1006)
        """Rescale self so that the exponent is exp, either by padding with zeros
        or by truncating digits, using the given rounding mode.

        Specials are returned without change.  This operation is
        quiet: it raises no flags, and uses no information from the
        context.

        exp = exp to scale to (an integer)
        rounding = rounding mode
        """
        Sensor(1007)
        if self._is_special:
            py_ass_parser1008 = Decimal(self)
            Sensor(1008)
            return py_ass_parser1008
        Sensor(1009)
        if not self:
            py_ass_parser1010 = _dec_from_triple(self._sign, '0', exp)
            Sensor(1010)
            return py_ass_parser1010
        Sensor(1011)
        if self._exp >= exp:
            py_ass_parser1012 = _dec_from_triple(self._sign, self._int + 
                '0' * (self._exp - exp), exp)
            Sensor(1012)
            return py_ass_parser1012
        Sensor(1013)
        digits = len(self._int) + self._exp - exp
        Sensor(1014)
        if digits < 0:
            Sensor(1015)
            self = _dec_from_triple(self._sign, '1', exp - 1)
            digits = 0
        Sensor(1016)
        this_function = self._pick_rounding_function[rounding]
        changed = this_function(self, digits)
        coeff = self._int[:digits] or '0'
        Sensor(1017)
        if changed == 1:
            Sensor(1018)
            coeff = str(int(coeff) + 1)
        py_ass_parser1019 = _dec_from_triple(self._sign, coeff, exp)
        Sensor(1019)
        return py_ass_parser1019

    def _round(self, places, rounding):
        Sensor(1020)
        """Round a nonzero, nonspecial Decimal to a fixed number of
        significant figures, using the given rounding mode.

        Infinities, NaNs and zeros are returned unaltered.

        This operation is quiet: it raises no flags, and uses no
        information from the context.

        """
        Sensor(1021)
        if places <= 0:
            Sensor(1022)
            raise ValueError('argument should be at least 1 in _round')
        Sensor(1023)
        if self._is_special or not self:
            py_ass_parser1024 = Decimal(self)
            Sensor(1024)
            return py_ass_parser1024
        Sensor(1025)
        ans = self._rescale(self.adjusted() + 1 - places, rounding)
        Sensor(1026)
        if ans.adjusted() != self.adjusted():
            Sensor(1027)
            ans = ans._rescale(ans.adjusted() + 1 - places, rounding)
        py_ass_parser1028 = ans
        Sensor(1028)
        return py_ass_parser1028

    def to_integral_exact(self, rounding=None, context=None):
        Sensor(1029)
        """Rounds to a nearby integer.

        If no rounding mode is specified, take the rounding mode from
        the context.  This method raises the Rounded and Inexact flags
        when appropriate.

        See also: to_integral_value, which does exactly the same as
        this method except that it doesn't raise Inexact or Rounded.
        """
        Sensor(1030)
        if self._is_special:
            Sensor(1031)
            ans = self._check_nans(context=context)
            Sensor(1032)
            if ans:
                py_ass_parser1033 = ans
                Sensor(1033)
                return py_ass_parser1033
            py_ass_parser1034 = Decimal(self)
            Sensor(1034)
            return py_ass_parser1034
        Sensor(1035)
        if self._exp >= 0:
            py_ass_parser1036 = Decimal(self)
            Sensor(1036)
            return py_ass_parser1036
        Sensor(1037)
        if not self:
            py_ass_parser1038 = _dec_from_triple(self._sign, '0', 0)
            Sensor(1038)
            return py_ass_parser1038
        Sensor(1039)
        if context is None:
            Sensor(1040)
            context = getcontext()
        Sensor(1041)
        if rounding is None:
            Sensor(1042)
            rounding = context.rounding
        Sensor(1043)
        ans = self._rescale(0, rounding)
        Sensor(1044)
        if ans != self:
            Sensor(1045)
            context._raise_error(Inexact)
        Sensor(1046)
        context._raise_error(Rounded)
        py_ass_parser1047 = ans
        Sensor(1047)
        return py_ass_parser1047

    def to_integral_value(self, rounding=None, context=None):
        Sensor(1048)
        """Rounds to the nearest integer, without raising inexact, rounded."""
        Sensor(1049)
        if context is None:
            Sensor(1050)
            context = getcontext()
        Sensor(1051)
        if rounding is None:
            Sensor(1052)
            rounding = context.rounding
        Sensor(1053)
        if self._is_special:
            Sensor(1054)
            ans = self._check_nans(context=context)
            Sensor(1055)
            if ans:
                py_ass_parser1056 = ans
                Sensor(1056)
                return py_ass_parser1056
            py_ass_parser1057 = Decimal(self)
            Sensor(1057)
            return py_ass_parser1057
        Sensor(1058)
        if self._exp >= 0:
            py_ass_parser1059 = Decimal(self)
            Sensor(1059)
            return py_ass_parser1059
        else:
            py_ass_parser1060 = self._rescale(0, rounding)
            Sensor(1060)
            return py_ass_parser1060
        Sensor(1061)
    to_integral = to_integral_value

    def sqrt(self, context=None):
        Sensor(1062)
        """Return the square root of self."""
        Sensor(1063)
        if context is None:
            Sensor(1064)
            context = getcontext()
        Sensor(1065)
        if self._is_special:
            Sensor(1066)
            ans = self._check_nans(context=context)
            Sensor(1067)
            if ans:
                py_ass_parser1068 = ans
                Sensor(1068)
                return py_ass_parser1068
            Sensor(1069)
            if self._isinfinity() and self._sign == 0:
                py_ass_parser1070 = Decimal(self)
                Sensor(1070)
                return py_ass_parser1070
        Sensor(1071)
        if not self:
            Sensor(1072)
            ans = _dec_from_triple(self._sign, '0', self._exp // 2)
            py_ass_parser1073 = ans._fix(context)
            Sensor(1073)
            return py_ass_parser1073
        Sensor(1074)
        if self._sign == 1:
            py_ass_parser1075 = context._raise_error(InvalidOperation,
                'sqrt(-x), x > 0')
            Sensor(1075)
            return py_ass_parser1075
        Sensor(1076)
        prec = context.prec + 1
        op = _WorkRep(self)
        e = op.exp >> 1
        Sensor(1077)
        if op.exp & 1:
            Sensor(1078)
            c = op.int * 10
            l = (len(self._int) >> 1) + 1
        else:
            Sensor(1079)
            c = op.int
            l = len(self._int) + 1 >> 1
        Sensor(1080)
        shift = prec - l
        Sensor(1081)
        if shift >= 0:
            Sensor(1082)
            c *= 100 ** shift
            exact = True
        else:
            Sensor(1083)
            c, remainder = divmod(c, 100 ** -shift)
            exact = not remainder
        Sensor(1084)
        e -= shift
        n = 10 ** prec
        Sensor(1085)
        while True:
            Sensor(1086)
            q = c // n
            Sensor(1087)
            if n <= q:
                Sensor(1088)
                break
            else:
                Sensor(1089)
                n = n + q >> 1
        Sensor(1090)
        exact = exact and n * n == c
        Sensor(1091)
        if exact:
            Sensor(1092)
            if shift >= 0:
                Sensor(1093)
                n //= 10 ** shift
            else:
                Sensor(1094)
                n *= 10 ** -shift
            Sensor(1095)
            e += shift
        else:
            Sensor(1096)
            if n % 5 == 0:
                Sensor(1097)
                n += 1
        Sensor(1098)
        ans = _dec_from_triple(0, str(n), e)
        context = context._shallow_copy()
        rounding = context._set_rounding(ROUND_HALF_EVEN)
        ans = ans._fix(context)
        context.rounding = rounding
        py_ass_parser1099 = ans
        Sensor(1099)
        return py_ass_parser1099

    def max(self, other, context=None):
        Sensor(1100)
        """Returns the larger value.

        Like max(self, other) except if one is not a number, returns
        NaN (and signals if one is sNaN).  Also rounds.
        """
        other = _convert_other(other, raiseit=True)
        Sensor(1101)
        if context is None:
            Sensor(1102)
            context = getcontext()
        Sensor(1103)
        if self._is_special or other._is_special:
            Sensor(1104)
            sn = self._isnan()
            on = other._isnan()
            Sensor(1105)
            if sn or on:
                Sensor(1106)
                if on == 1 and sn == 0:
                    py_ass_parser1107 = self._fix(context)
                    Sensor(1107)
                    return py_ass_parser1107
                Sensor(1108)
                if sn == 1 and on == 0:
                    py_ass_parser1109 = other._fix(context)
                    Sensor(1109)
                    return py_ass_parser1109
                py_ass_parser1110 = self._check_nans(other, context)
                Sensor(1110)
                return py_ass_parser1110
        Sensor(1111)
        c = self._cmp(other)
        Sensor(1112)
        if c == 0:
            Sensor(1113)
            c = self.compare_total(other)
        Sensor(1114)
        if c == -1:
            Sensor(1115)
            ans = other
        else:
            Sensor(1116)
            ans = self
        py_ass_parser1117 = ans._fix(context)
        Sensor(1117)
        return py_ass_parser1117

    def min(self, other, context=None):
        Sensor(1118)
        """Returns the smaller value.

        Like min(self, other) except if one is not a number, returns
        NaN (and signals if one is sNaN).  Also rounds.
        """
        other = _convert_other(other, raiseit=True)
        Sensor(1119)
        if context is None:
            Sensor(1120)
            context = getcontext()
        Sensor(1121)
        if self._is_special or other._is_special:
            Sensor(1122)
            sn = self._isnan()
            on = other._isnan()
            Sensor(1123)
            if sn or on:
                Sensor(1124)
                if on == 1 and sn == 0:
                    py_ass_parser1125 = self._fix(context)
                    Sensor(1125)
                    return py_ass_parser1125
                Sensor(1126)
                if sn == 1 and on == 0:
                    py_ass_parser1127 = other._fix(context)
                    Sensor(1127)
                    return py_ass_parser1127
                py_ass_parser1128 = self._check_nans(other, context)
                Sensor(1128)
                return py_ass_parser1128
        Sensor(1129)
        c = self._cmp(other)
        Sensor(1130)
        if c == 0:
            Sensor(1131)
            c = self.compare_total(other)
        Sensor(1132)
        if c == -1:
            Sensor(1133)
            ans = self
        else:
            Sensor(1134)
            ans = other
        py_ass_parser1135 = ans._fix(context)
        Sensor(1135)
        return py_ass_parser1135

    def _isinteger(self):
        Sensor(1136)
        """Returns whether self is an integer"""
        Sensor(1137)
        if self._is_special:
            py_ass_parser1138 = False
            Sensor(1138)
            return py_ass_parser1138
        Sensor(1139)
        if self._exp >= 0:
            py_ass_parser1140 = True
            Sensor(1140)
            return py_ass_parser1140
        Sensor(1141)
        rest = self._int[self._exp:]
        py_ass_parser1142 = rest == '0' * len(rest)
        Sensor(1142)
        return py_ass_parser1142

    def _iseven(self):
        Sensor(1143)
        """Returns True if self is even.  Assumes self is an integer."""
        Sensor(1144)
        if not self or self._exp > 0:
            py_ass_parser1145 = True
            Sensor(1145)
            return py_ass_parser1145
        py_ass_parser1146 = self._int[-1 + self._exp] in '02468'
        Sensor(1146)
        return py_ass_parser1146

    def adjusted(self):
        Sensor(1147)
        """Return the adjusted exponent of self"""
        Sensor(1148)
        try:
            py_ass_parser1149 = self._exp + len(self._int) - 1
            Sensor(1149)
            return py_ass_parser1149
        except TypeError:
            py_ass_parser1150 = 0
            Sensor(1150)
            return py_ass_parser1150
        Sensor(1151)

    def canonical(self, context=None):
        Sensor(1152)
        """Returns the same Decimal object.

        As we do not have different encodings for the same number, the
        received object already is in its canonical form.
        """
        py_ass_parser1153 = self
        Sensor(1153)
        return py_ass_parser1153

    def compare_signal(self, other, context=None):
        Sensor(1154)
        """Compares self to the other operand numerically.

        It's pretty much like compare(), but all NaNs signal, with signaling
        NaNs taking precedence over quiet NaNs.
        """
        other = _convert_other(other, raiseit=True)
        ans = self._compare_check_nans(other, context)
        Sensor(1155)
        if ans:
            py_ass_parser1156 = ans
            Sensor(1156)
            return py_ass_parser1156
        py_ass_parser1157 = self.compare(other, context=context)
        Sensor(1157)
        return py_ass_parser1157

    def compare_total(self, other):
        Sensor(1158)
        """Compares self to other using the abstract representations.

        This is not like the standard compare, which use their numerical
        value. Note that a total ordering is defined for all possible abstract
        representations.
        """
        other = _convert_other(other, raiseit=True)
        Sensor(1159)
        if self._sign and not other._sign:
            py_ass_parser1160 = _NegativeOne
            Sensor(1160)
            return py_ass_parser1160
        Sensor(1161)
        if not self._sign and other._sign:
            py_ass_parser1162 = _One
            Sensor(1162)
            return py_ass_parser1162
        Sensor(1163)
        sign = self._sign
        self_nan = self._isnan()
        other_nan = other._isnan()
        Sensor(1164)
        if self_nan or other_nan:
            Sensor(1165)
            if self_nan == other_nan:
                Sensor(1166)
                self_key = len(self._int), self._int
                other_key = len(other._int), other._int
                Sensor(1167)
                if self_key < other_key:
                    Sensor(1168)
                    if sign:
                        py_ass_parser1169 = _One
                        Sensor(1169)
                        return py_ass_parser1169
                    else:
                        py_ass_parser1170 = _NegativeOne
                        Sensor(1170)
                        return py_ass_parser1170
                Sensor(1171)
                if self_key > other_key:
                    Sensor(1172)
                    if sign:
                        py_ass_parser1173 = _NegativeOne
                        Sensor(1173)
                        return py_ass_parser1173
                    else:
                        py_ass_parser1174 = _One
                        Sensor(1174)
                        return py_ass_parser1174
                py_ass_parser1175 = _Zero
                Sensor(1175)
                return py_ass_parser1175
            Sensor(1176)
            if sign:
                Sensor(1177)
                if self_nan == 1:
                    py_ass_parser1178 = _NegativeOne
                    Sensor(1178)
                    return py_ass_parser1178
                Sensor(1179)
                if other_nan == 1:
                    py_ass_parser1180 = _One
                    Sensor(1180)
                    return py_ass_parser1180
                Sensor(1181)
                if self_nan == 2:
                    py_ass_parser1182 = _NegativeOne
                    Sensor(1182)
                    return py_ass_parser1182
                Sensor(1183)
                if other_nan == 2:
                    py_ass_parser1184 = _One
                    Sensor(1184)
                    return py_ass_parser1184
            else:
                Sensor(1185)
                if self_nan == 1:
                    py_ass_parser1186 = _One
                    Sensor(1186)
                    return py_ass_parser1186
                Sensor(1187)
                if other_nan == 1:
                    py_ass_parser1188 = _NegativeOne
                    Sensor(1188)
                    return py_ass_parser1188
                Sensor(1189)
                if self_nan == 2:
                    py_ass_parser1190 = _One
                    Sensor(1190)
                    return py_ass_parser1190
                Sensor(1191)
                if other_nan == 2:
                    py_ass_parser1192 = _NegativeOne
                    Sensor(1192)
                    return py_ass_parser1192
        Sensor(1193)
        if self < other:
            py_ass_parser1194 = _NegativeOne
            Sensor(1194)
            return py_ass_parser1194
        Sensor(1195)
        if self > other:
            py_ass_parser1196 = _One
            Sensor(1196)
            return py_ass_parser1196
        Sensor(1197)
        if self._exp < other._exp:
            Sensor(1198)
            if sign:
                py_ass_parser1199 = _One
                Sensor(1199)
                return py_ass_parser1199
            else:
                py_ass_parser1200 = _NegativeOne
                Sensor(1200)
                return py_ass_parser1200
        Sensor(1201)
        if self._exp > other._exp:
            Sensor(1202)
            if sign:
                py_ass_parser1203 = _NegativeOne
                Sensor(1203)
                return py_ass_parser1203
            else:
                py_ass_parser1204 = _One
                Sensor(1204)
                return py_ass_parser1204
        py_ass_parser1205 = _Zero
        Sensor(1205)
        return py_ass_parser1205

    def compare_total_mag(self, other):
        Sensor(1206)
        """Compares self to other using abstract repr., ignoring sign.

        Like compare_total, but with operand's sign ignored and assumed to be 0.
        """
        other = _convert_other(other, raiseit=True)
        s = self.copy_abs()
        o = other.copy_abs()
        py_ass_parser1207 = s.compare_total(o)
        Sensor(1207)
        return py_ass_parser1207

    def copy_abs(self):
        Sensor(1208)
        """Returns a copy with the sign set to 0. """
        py_ass_parser1209 = _dec_from_triple(0, self._int, self._exp, self.
            _is_special)
        Sensor(1209)
        return py_ass_parser1209

    def copy_negate(self):
        Sensor(1210)
        """Returns a copy with the sign inverted."""
        Sensor(1211)
        if self._sign:
            py_ass_parser1212 = _dec_from_triple(0, self._int, self._exp,
                self._is_special)
            Sensor(1212)
            return py_ass_parser1212
        else:
            py_ass_parser1213 = _dec_from_triple(1, self._int, self._exp,
                self._is_special)
            Sensor(1213)
            return py_ass_parser1213
        Sensor(1214)

    def copy_sign(self, other):
        Sensor(1215)
        """Returns self with the sign of other."""
        other = _convert_other(other, raiseit=True)
        py_ass_parser1216 = _dec_from_triple(other._sign, self._int, self.
            _exp, self._is_special)
        Sensor(1216)
        return py_ass_parser1216

    def exp(self, context=None):
        Sensor(1217)
        """Returns e ** self."""
        Sensor(1218)
        if context is None:
            Sensor(1219)
            context = getcontext()
        Sensor(1220)
        ans = self._check_nans(context=context)
        Sensor(1221)
        if ans:
            py_ass_parser1222 = ans
            Sensor(1222)
            return py_ass_parser1222
        Sensor(1223)
        if self._isinfinity() == -1:
            py_ass_parser1224 = _Zero
            Sensor(1224)
            return py_ass_parser1224
        Sensor(1225)
        if not self:
            py_ass_parser1226 = _One
            Sensor(1226)
            return py_ass_parser1226
        Sensor(1227)
        if self._isinfinity() == 1:
            py_ass_parser1228 = Decimal(self)
            Sensor(1228)
            return py_ass_parser1228
        Sensor(1229)
        p = context.prec
        adj = self.adjusted()
        Sensor(1230)
        if self._sign == 0 and adj > len(str((context.Emax + 1) * 3)):
            Sensor(1231)
            ans = _dec_from_triple(0, '1', context.Emax + 1)
        else:
            Sensor(1232)
            if self._sign == 1 and adj > len(str((-context.Etiny() + 1) * 3)):
                Sensor(1233)
                ans = _dec_from_triple(0, '1', context.Etiny() - 1)
            else:
                Sensor(1234)
                if self._sign == 0 and adj < -p:
                    Sensor(1235)
                    ans = _dec_from_triple(0, '1' + '0' * (p - 1) + '1', -p)
                else:
                    Sensor(1236)
                    if self._sign == 1 and adj < -p - 1:
                        Sensor(1237)
                        ans = _dec_from_triple(0, '9' * (p + 1), -p - 1)
                    else:
                        Sensor(1238)
                        op = _WorkRep(self)
                        c, e = op.int, op.exp
                        Sensor(1239)
                        if op.sign == 1:
                            Sensor(1240)
                            c = -c
                        Sensor(1241)
                        extra = 3
                        Sensor(1242)
                        while True:
                            Sensor(1243)
                            coeff, exp = _dexp(c, e, p + extra)
                            Sensor(1244)
                            if coeff % (5 * 10 ** (len(str(coeff)) - p - 1)):
                                Sensor(1245)
                                break
                            Sensor(1246)
                            extra += 3
                        Sensor(1247)
                        ans = _dec_from_triple(0, str(coeff), exp)
        Sensor(1248)
        context = context._shallow_copy()
        rounding = context._set_rounding(ROUND_HALF_EVEN)
        ans = ans._fix(context)
        context.rounding = rounding
        py_ass_parser1249 = ans
        Sensor(1249)
        return py_ass_parser1249

    def is_canonical(self):
        Sensor(1250)
        """Return True if self is canonical; otherwise return False.

        Currently, the encoding of a Decimal instance is always
        canonical, so this method returns True for any Decimal.
        """
        py_ass_parser1251 = True
        Sensor(1251)
        return py_ass_parser1251

    def is_finite(self):
        Sensor(1252)
        """Return True if self is finite; otherwise return False.

        A Decimal instance is considered finite if it is neither
        infinite nor a NaN.
        """
        py_ass_parser1253 = not self._is_special
        Sensor(1253)
        return py_ass_parser1253

    def is_infinite(self):
        Sensor(1254)
        """Return True if self is infinite; otherwise return False."""
        py_ass_parser1255 = self._exp == 'F'
        Sensor(1255)
        return py_ass_parser1255

    def is_nan(self):
        Sensor(1256)
        """Return True if self is a qNaN or sNaN; otherwise return False."""
        py_ass_parser1257 = self._exp in ('n', 'N')
        Sensor(1257)
        return py_ass_parser1257

    def is_normal(self, context=None):
        Sensor(1258)
        """Return True if self is a normal number; otherwise return False."""
        Sensor(1259)
        if self._is_special or not self:
            py_ass_parser1260 = False
            Sensor(1260)
            return py_ass_parser1260
        Sensor(1261)
        if context is None:
            Sensor(1262)
            context = getcontext()
        py_ass_parser1263 = context.Emin <= self.adjusted()
        Sensor(1263)
        return py_ass_parser1263

    def is_qnan(self):
        Sensor(1264)
        """Return True if self is a quiet NaN; otherwise return False."""
        py_ass_parser1265 = self._exp == 'n'
        Sensor(1265)
        return py_ass_parser1265

    def is_signed(self):
        Sensor(1266)
        """Return True if self is negative; otherwise return False."""
        py_ass_parser1267 = self._sign == 1
        Sensor(1267)
        return py_ass_parser1267

    def is_snan(self):
        Sensor(1268)
        """Return True if self is a signaling NaN; otherwise return False."""
        py_ass_parser1269 = self._exp == 'N'
        Sensor(1269)
        return py_ass_parser1269

    def is_subnormal(self, context=None):
        Sensor(1270)
        """Return True if self is subnormal; otherwise return False."""
        Sensor(1271)
        if self._is_special or not self:
            py_ass_parser1272 = False
            Sensor(1272)
            return py_ass_parser1272
        Sensor(1273)
        if context is None:
            Sensor(1274)
            context = getcontext()
        py_ass_parser1275 = self.adjusted() < context.Emin
        Sensor(1275)
        return py_ass_parser1275

    def is_zero(self):
        Sensor(1276)
        """Return True if self is a zero; otherwise return False."""
        py_ass_parser1277 = not self._is_special and self._int == '0'
        Sensor(1277)
        return py_ass_parser1277

    def _ln_exp_bound(self):
        Sensor(1278)
        """Compute a lower bound for the adjusted exponent of self.ln().
        In other words, compute r such that self.ln() >= 10**r.  Assumes
        that self is finite and positive and that self != 1.
        """
        adj = self._exp + len(self._int) - 1
        Sensor(1279)
        if adj >= 1:
            py_ass_parser1280 = len(str(adj * 23 // 10)) - 1
            Sensor(1280)
            return py_ass_parser1280
        Sensor(1281)
        if adj <= -2:
            py_ass_parser1282 = len(str((-1 - adj) * 23 // 10)) - 1
            Sensor(1282)
            return py_ass_parser1282
        Sensor(1283)
        op = _WorkRep(self)
        c, e = op.int, op.exp
        Sensor(1284)
        if adj == 0:
            Sensor(1285)
            num = str(c - 10 ** -e)
            den = str(c)
            py_ass_parser1286 = len(num) - len(den) - (num < den)
            Sensor(1286)
            return py_ass_parser1286
        py_ass_parser1287 = e + len(str(10 ** -e - c)) - 1
        Sensor(1287)
        return py_ass_parser1287

    def ln(self, context=None):
        Sensor(1288)
        """Returns the natural (base e) logarithm of self."""
        Sensor(1289)
        if context is None:
            Sensor(1290)
            context = getcontext()
        Sensor(1291)
        ans = self._check_nans(context=context)
        Sensor(1292)
        if ans:
            py_ass_parser1293 = ans
            Sensor(1293)
            return py_ass_parser1293
        Sensor(1294)
        if not self:
            py_ass_parser1295 = _NegativeInfinity
            Sensor(1295)
            return py_ass_parser1295
        Sensor(1296)
        if self._isinfinity() == 1:
            py_ass_parser1297 = _Infinity
            Sensor(1297)
            return py_ass_parser1297
        Sensor(1298)
        if self == _One:
            py_ass_parser1299 = _Zero
            Sensor(1299)
            return py_ass_parser1299
        Sensor(1300)
        if self._sign == 1:
            py_ass_parser1301 = context._raise_error(InvalidOperation,
                'ln of a negative value')
            Sensor(1301)
            return py_ass_parser1301
        Sensor(1302)
        op = _WorkRep(self)
        c, e = op.int, op.exp
        p = context.prec
        places = p - self._ln_exp_bound() + 2
        Sensor(1303)
        while True:
            Sensor(1304)
            coeff = _dlog(c, e, places)
            Sensor(1305)
            if coeff % (5 * 10 ** (len(str(abs(coeff))) - p - 1)):
                Sensor(1306)
                break
            Sensor(1307)
            places += 3
        Sensor(1308)
        ans = _dec_from_triple(int(coeff < 0), str(abs(coeff)), -places)
        context = context._shallow_copy()
        rounding = context._set_rounding(ROUND_HALF_EVEN)
        ans = ans._fix(context)
        context.rounding = rounding
        py_ass_parser1309 = ans
        Sensor(1309)
        return py_ass_parser1309

    def _log10_exp_bound(self):
        Sensor(1310)
        """Compute a lower bound for the adjusted exponent of self.log10().
        In other words, find r such that self.log10() >= 10**r.
        Assumes that self is finite and positive and that self != 1.
        """
        adj = self._exp + len(self._int) - 1
        Sensor(1311)
        if adj >= 1:
            py_ass_parser1312 = len(str(adj)) - 1
            Sensor(1312)
            return py_ass_parser1312
        Sensor(1313)
        if adj <= -2:
            py_ass_parser1314 = len(str(-1 - adj)) - 1
            Sensor(1314)
            return py_ass_parser1314
        Sensor(1315)
        op = _WorkRep(self)
        c, e = op.int, op.exp
        Sensor(1316)
        if adj == 0:
            Sensor(1317)
            num = str(c - 10 ** -e)
            den = str(231 * c)
            py_ass_parser1318 = len(num) - len(den) - (num < den) + 2
            Sensor(1318)
            return py_ass_parser1318
        Sensor(1319)
        num = str(10 ** -e - c)
        py_ass_parser1320 = len(num) + e - (num < '231') - 1
        Sensor(1320)
        return py_ass_parser1320

    def log10(self, context=None):
        Sensor(1321)
        """Returns the base 10 logarithm of self."""
        Sensor(1322)
        if context is None:
            Sensor(1323)
            context = getcontext()
        Sensor(1324)
        ans = self._check_nans(context=context)
        Sensor(1325)
        if ans:
            py_ass_parser1326 = ans
            Sensor(1326)
            return py_ass_parser1326
        Sensor(1327)
        if not self:
            py_ass_parser1328 = _NegativeInfinity
            Sensor(1328)
            return py_ass_parser1328
        Sensor(1329)
        if self._isinfinity() == 1:
            py_ass_parser1330 = _Infinity
            Sensor(1330)
            return py_ass_parser1330
        Sensor(1331)
        if self._sign == 1:
            py_ass_parser1332 = context._raise_error(InvalidOperation,
                'log10 of a negative value')
            Sensor(1332)
            return py_ass_parser1332
        Sensor(1333)
        if self._int[0] == '1' and self._int[1:] == '0' * (len(self._int) - 1):
            Sensor(1334)
            ans = Decimal(self._exp + len(self._int) - 1)
        else:
            Sensor(1335)
            op = _WorkRep(self)
            c, e = op.int, op.exp
            p = context.prec
            places = p - self._log10_exp_bound() + 2
            Sensor(1336)
            while True:
                Sensor(1337)
                coeff = _dlog10(c, e, places)
                Sensor(1338)
                if coeff % (5 * 10 ** (len(str(abs(coeff))) - p - 1)):
                    Sensor(1339)
                    break
                Sensor(1340)
                places += 3
            Sensor(1341)
            ans = _dec_from_triple(int(coeff < 0), str(abs(coeff)), -places)
        Sensor(1342)
        context = context._shallow_copy()
        rounding = context._set_rounding(ROUND_HALF_EVEN)
        ans = ans._fix(context)
        context.rounding = rounding
        py_ass_parser1343 = ans
        Sensor(1343)
        return py_ass_parser1343

    def logb(self, context=None):
        Sensor(1344)
        """ Returns the exponent of the magnitude of self's MSD.

        The result is the integer which is the exponent of the magnitude
        of the most significant digit of self (as though it were truncated
        to a single digit while maintaining the value of that digit and
        without limiting the resulting exponent).
        """
        ans = self._check_nans(context=context)
        Sensor(1345)
        if ans:
            py_ass_parser1346 = ans
            Sensor(1346)
            return py_ass_parser1346
        Sensor(1347)
        if context is None:
            Sensor(1348)
            context = getcontext()
        Sensor(1349)
        if self._isinfinity():
            py_ass_parser1350 = _Infinity
            Sensor(1350)
            return py_ass_parser1350
        Sensor(1351)
        if not self:
            py_ass_parser1352 = context._raise_error(DivisionByZero,
                'logb(0)', 1)
            Sensor(1352)
            return py_ass_parser1352
        Sensor(1353)
        ans = Decimal(self.adjusted())
        py_ass_parser1354 = ans._fix(context)
        Sensor(1354)
        return py_ass_parser1354

    def _islogical(self):
        Sensor(1355)
        """Return True if self is a logical operand.

        For being logical, it must be a finite number with a sign of 0,
        an exponent of 0, and a coefficient whose digits must all be
        either 0 or 1.
        """
        Sensor(1356)
        if self._sign != 0 or self._exp != 0:
            py_ass_parser1357 = False
            Sensor(1357)
            return py_ass_parser1357
        Sensor(1358)
        for dig in self._int:
            Sensor(1359)
            if dig not in '01':
                py_ass_parser1360 = False
                Sensor(1360)
                return py_ass_parser1360
        py_ass_parser1361 = True
        Sensor(1361)
        return py_ass_parser1361

    def _fill_logical(self, context, opa, opb):
        Sensor(1362)
        dif = context.prec - len(opa)
        Sensor(1363)
        if dif > 0:
            Sensor(1364)
            opa = '0' * dif + opa
        else:
            Sensor(1365)
            if dif < 0:
                Sensor(1366)
                opa = opa[-context.prec:]
        Sensor(1367)
        dif = context.prec - len(opb)
        Sensor(1368)
        if dif > 0:
            Sensor(1369)
            opb = '0' * dif + opb
        else:
            Sensor(1370)
            if dif < 0:
                Sensor(1371)
                opb = opb[-context.prec:]
        py_ass_parser1372 = opa, opb
        Sensor(1372)
        return py_ass_parser1372

    def logical_and(self, other, context=None):
        Sensor(1373)
        """Applies an 'and' operation between self and other's digits."""
        Sensor(1374)
        if context is None:
            Sensor(1375)
            context = getcontext()
        Sensor(1376)
        other = _convert_other(other, raiseit=True)
        Sensor(1377)
        if not self._islogical() or not other._islogical():
            py_ass_parser1378 = context._raise_error(InvalidOperation)
            Sensor(1378)
            return py_ass_parser1378
        Sensor(1379)
        opa, opb = self._fill_logical(context, self._int, other._int)
        result = ''.join([str(int(a) & int(b)) for a, b in zip(opa, opb)])
        py_ass_parser1380 = _dec_from_triple(0, result.lstrip('0') or '0', 0)
        Sensor(1380)
        return py_ass_parser1380

    def logical_invert(self, context=None):
        Sensor(1381)
        """Invert all its digits."""
        Sensor(1382)
        if context is None:
            Sensor(1383)
            context = getcontext()
        py_ass_parser1384 = self.logical_xor(_dec_from_triple(0, '1' *
            context.prec, 0), context)
        Sensor(1384)
        return py_ass_parser1384

    def logical_or(self, other, context=None):
        Sensor(1385)
        """Applies an 'or' operation between self and other's digits."""
        Sensor(1386)
        if context is None:
            Sensor(1387)
            context = getcontext()
        Sensor(1388)
        other = _convert_other(other, raiseit=True)
        Sensor(1389)
        if not self._islogical() or not other._islogical():
            py_ass_parser1390 = context._raise_error(InvalidOperation)
            Sensor(1390)
            return py_ass_parser1390
        Sensor(1391)
        opa, opb = self._fill_logical(context, self._int, other._int)
        result = ''.join([str(int(a) | int(b)) for a, b in zip(opa, opb)])
        py_ass_parser1392 = _dec_from_triple(0, result.lstrip('0') or '0', 0)
        Sensor(1392)
        return py_ass_parser1392

    def logical_xor(self, other, context=None):
        Sensor(1393)
        """Applies an 'xor' operation between self and other's digits."""
        Sensor(1394)
        if context is None:
            Sensor(1395)
            context = getcontext()
        Sensor(1396)
        other = _convert_other(other, raiseit=True)
        Sensor(1397)
        if not self._islogical() or not other._islogical():
            py_ass_parser1398 = context._raise_error(InvalidOperation)
            Sensor(1398)
            return py_ass_parser1398
        Sensor(1399)
        opa, opb = self._fill_logical(context, self._int, other._int)
        result = ''.join([str(int(a) ^ int(b)) for a, b in zip(opa, opb)])
        py_ass_parser1400 = _dec_from_triple(0, result.lstrip('0') or '0', 0)
        Sensor(1400)
        return py_ass_parser1400

    def max_mag(self, other, context=None):
        Sensor(1401)
        """Compares the values numerically with their sign ignored."""
        other = _convert_other(other, raiseit=True)
        Sensor(1402)
        if context is None:
            Sensor(1403)
            context = getcontext()
        Sensor(1404)
        if self._is_special or other._is_special:
            Sensor(1405)
            sn = self._isnan()
            on = other._isnan()
            Sensor(1406)
            if sn or on:
                Sensor(1407)
                if on == 1 and sn == 0:
                    py_ass_parser1408 = self._fix(context)
                    Sensor(1408)
                    return py_ass_parser1408
                Sensor(1409)
                if sn == 1 and on == 0:
                    py_ass_parser1410 = other._fix(context)
                    Sensor(1410)
                    return py_ass_parser1410
                py_ass_parser1411 = self._check_nans(other, context)
                Sensor(1411)
                return py_ass_parser1411
        Sensor(1412)
        c = self.copy_abs()._cmp(other.copy_abs())
        Sensor(1413)
        if c == 0:
            Sensor(1414)
            c = self.compare_total(other)
        Sensor(1415)
        if c == -1:
            Sensor(1416)
            ans = other
        else:
            Sensor(1417)
            ans = self
        py_ass_parser1418 = ans._fix(context)
        Sensor(1418)
        return py_ass_parser1418

    def min_mag(self, other, context=None):
        Sensor(1419)
        """Compares the values numerically with their sign ignored."""
        other = _convert_other(other, raiseit=True)
        Sensor(1420)
        if context is None:
            Sensor(1421)
            context = getcontext()
        Sensor(1422)
        if self._is_special or other._is_special:
            Sensor(1423)
            sn = self._isnan()
            on = other._isnan()
            Sensor(1424)
            if sn or on:
                Sensor(1425)
                if on == 1 and sn == 0:
                    py_ass_parser1426 = self._fix(context)
                    Sensor(1426)
                    return py_ass_parser1426
                Sensor(1427)
                if sn == 1 and on == 0:
                    py_ass_parser1428 = other._fix(context)
                    Sensor(1428)
                    return py_ass_parser1428
                py_ass_parser1429 = self._check_nans(other, context)
                Sensor(1429)
                return py_ass_parser1429
        Sensor(1430)
        c = self.copy_abs()._cmp(other.copy_abs())
        Sensor(1431)
        if c == 0:
            Sensor(1432)
            c = self.compare_total(other)
        Sensor(1433)
        if c == -1:
            Sensor(1434)
            ans = self
        else:
            Sensor(1435)
            ans = other
        py_ass_parser1436 = ans._fix(context)
        Sensor(1436)
        return py_ass_parser1436

    def next_minus(self, context=None):
        Sensor(1437)
        """Returns the largest representable number smaller than itself."""
        Sensor(1438)
        if context is None:
            Sensor(1439)
            context = getcontext()
        Sensor(1440)
        ans = self._check_nans(context=context)
        Sensor(1441)
        if ans:
            py_ass_parser1442 = ans
            Sensor(1442)
            return py_ass_parser1442
        Sensor(1443)
        if self._isinfinity() == -1:
            py_ass_parser1444 = _NegativeInfinity
            Sensor(1444)
            return py_ass_parser1444
        Sensor(1445)
        if self._isinfinity() == 1:
            py_ass_parser1446 = _dec_from_triple(0, '9' * context.prec,
                context.Etop())
            Sensor(1446)
            return py_ass_parser1446
        Sensor(1447)
        context = context.copy()
        context._set_rounding(ROUND_FLOOR)
        context._ignore_all_flags()
        new_self = self._fix(context)
        Sensor(1448)
        if new_self != self:
            py_ass_parser1449 = new_self
            Sensor(1449)
            return py_ass_parser1449
        py_ass_parser1450 = self.__sub__(_dec_from_triple(0, '1', context.
            Etiny() - 1), context)
        Sensor(1450)
        return py_ass_parser1450

    def next_plus(self, context=None):
        Sensor(1451)
        """Returns the smallest representable number larger than itself."""
        Sensor(1452)
        if context is None:
            Sensor(1453)
            context = getcontext()
        Sensor(1454)
        ans = self._check_nans(context=context)
        Sensor(1455)
        if ans:
            py_ass_parser1456 = ans
            Sensor(1456)
            return py_ass_parser1456
        Sensor(1457)
        if self._isinfinity() == 1:
            py_ass_parser1458 = _Infinity
            Sensor(1458)
            return py_ass_parser1458
        Sensor(1459)
        if self._isinfinity() == -1:
            py_ass_parser1460 = _dec_from_triple(1, '9' * context.prec,
                context.Etop())
            Sensor(1460)
            return py_ass_parser1460
        Sensor(1461)
        context = context.copy()
        context._set_rounding(ROUND_CEILING)
        context._ignore_all_flags()
        new_self = self._fix(context)
        Sensor(1462)
        if new_self != self:
            py_ass_parser1463 = new_self
            Sensor(1463)
            return py_ass_parser1463
        py_ass_parser1464 = self.__add__(_dec_from_triple(0, '1', context.
            Etiny() - 1), context)
        Sensor(1464)
        return py_ass_parser1464

    def next_toward(self, other, context=None):
        Sensor(1465)
        """Returns the number closest to self, in the direction towards other.

        The result is the closest representable number to self
        (excluding self) that is in the direction towards other,
        unless both have the same value.  If the two operands are
        numerically equal, then the result is a copy of self with the
        sign set to be the same as the sign of other.
        """
        other = _convert_other(other, raiseit=True)
        Sensor(1466)
        if context is None:
            Sensor(1467)
            context = getcontext()
        Sensor(1468)
        ans = self._check_nans(other, context)
        Sensor(1469)
        if ans:
            py_ass_parser1470 = ans
            Sensor(1470)
            return py_ass_parser1470
        Sensor(1471)
        comparison = self._cmp(other)
        Sensor(1472)
        if comparison == 0:
            py_ass_parser1473 = self.copy_sign(other)
            Sensor(1473)
            return py_ass_parser1473
        Sensor(1474)
        if comparison == -1:
            Sensor(1475)
            ans = self.next_plus(context)
        else:
            Sensor(1476)
            ans = self.next_minus(context)
        Sensor(1477)
        if ans._isinfinity():
            Sensor(1478)
            context._raise_error(Overflow,
                'Infinite result from next_toward', ans._sign)
            context._raise_error(Inexact)
            context._raise_error(Rounded)
        else:
            Sensor(1479)
            if ans.adjusted() < context.Emin:
                Sensor(1480)
                context._raise_error(Underflow)
                context._raise_error(Subnormal)
                context._raise_error(Inexact)
                context._raise_error(Rounded)
                Sensor(1481)
                if not ans:
                    Sensor(1482)
                    context._raise_error(Clamped)
        py_ass_parser1483 = ans
        Sensor(1483)
        return py_ass_parser1483

    def number_class(self, context=None):
        Sensor(1484)
        """Returns an indication of the class of self.

        The class is one of the following strings:
          sNaN
          NaN
          -Infinity
          -Normal
          -Subnormal
          -Zero
          +Zero
          +Subnormal
          +Normal
          +Infinity
        """
        Sensor(1485)
        if self.is_snan():
            py_ass_parser1486 = 'sNaN'
            Sensor(1486)
            return py_ass_parser1486
        Sensor(1487)
        if self.is_qnan():
            py_ass_parser1488 = 'NaN'
            Sensor(1488)
            return py_ass_parser1488
        Sensor(1489)
        inf = self._isinfinity()
        Sensor(1490)
        if inf == 1:
            py_ass_parser1491 = '+Infinity'
            Sensor(1491)
            return py_ass_parser1491
        Sensor(1492)
        if inf == -1:
            py_ass_parser1493 = '-Infinity'
            Sensor(1493)
            return py_ass_parser1493
        Sensor(1494)
        if self.is_zero():
            Sensor(1495)
            if self._sign:
                py_ass_parser1496 = '-Zero'
                Sensor(1496)
                return py_ass_parser1496
            else:
                py_ass_parser1497 = '+Zero'
                Sensor(1497)
                return py_ass_parser1497
        Sensor(1498)
        if context is None:
            Sensor(1499)
            context = getcontext()
        Sensor(1500)
        if self.is_subnormal(context=context):
            Sensor(1501)
            if self._sign:
                py_ass_parser1502 = '-Subnormal'
                Sensor(1502)
                return py_ass_parser1502
            else:
                py_ass_parser1503 = '+Subnormal'
                Sensor(1503)
                return py_ass_parser1503
        Sensor(1504)
        if self._sign:
            py_ass_parser1505 = '-Normal'
            Sensor(1505)
            return py_ass_parser1505
        else:
            py_ass_parser1506 = '+Normal'
            Sensor(1506)
            return py_ass_parser1506
        Sensor(1507)

    def radix(self):
        Sensor(1508)
        """Just returns 10, as this is Decimal, :)"""
        py_ass_parser1509 = Decimal(10)
        Sensor(1509)
        return py_ass_parser1509

    def rotate(self, other, context=None):
        Sensor(1510)
        """Returns a rotated copy of self, value-of-other times."""
        Sensor(1511)
        if context is None:
            Sensor(1512)
            context = getcontext()
        Sensor(1513)
        other = _convert_other(other, raiseit=True)
        ans = self._check_nans(other, context)
        Sensor(1514)
        if ans:
            py_ass_parser1515 = ans
            Sensor(1515)
            return py_ass_parser1515
        Sensor(1516)
        if other._exp != 0:
            py_ass_parser1517 = context._raise_error(InvalidOperation)
            Sensor(1517)
            return py_ass_parser1517
        Sensor(1518)
        if not -context.prec <= int(other) <= context.prec:
            py_ass_parser1519 = context._raise_error(InvalidOperation)
            Sensor(1519)
            return py_ass_parser1519
        Sensor(1520)
        if self._isinfinity():
            py_ass_parser1521 = Decimal(self)
            Sensor(1521)
            return py_ass_parser1521
        Sensor(1522)
        torot = int(other)
        rotdig = self._int
        topad = context.prec - len(rotdig)
        Sensor(1523)
        if topad > 0:
            Sensor(1524)
            rotdig = '0' * topad + rotdig
        else:
            Sensor(1525)
            if topad < 0:
                Sensor(1526)
                rotdig = rotdig[-topad:]
        Sensor(1527)
        rotated = rotdig[torot:] + rotdig[:torot]
        py_ass_parser1528 = _dec_from_triple(self._sign, rotated.lstrip('0'
            ) or '0', self._exp)
        Sensor(1528)
        return py_ass_parser1528

    def scaleb(self, other, context=None):
        Sensor(1529)
        """Returns self operand after adding the second value to its exp."""
        Sensor(1530)
        if context is None:
            Sensor(1531)
            context = getcontext()
        Sensor(1532)
        other = _convert_other(other, raiseit=True)
        ans = self._check_nans(other, context)
        Sensor(1533)
        if ans:
            py_ass_parser1534 = ans
            Sensor(1534)
            return py_ass_parser1534
        Sensor(1535)
        if other._exp != 0:
            py_ass_parser1536 = context._raise_error(InvalidOperation)
            Sensor(1536)
            return py_ass_parser1536
        Sensor(1537)
        liminf = -2 * (context.Emax + context.prec)
        limsup = 2 * (context.Emax + context.prec)
        Sensor(1538)
        if not liminf <= int(other) <= limsup:
            py_ass_parser1539 = context._raise_error(InvalidOperation)
            Sensor(1539)
            return py_ass_parser1539
        Sensor(1540)
        if self._isinfinity():
            py_ass_parser1541 = Decimal(self)
            Sensor(1541)
            return py_ass_parser1541
        Sensor(1542)
        d = _dec_from_triple(self._sign, self._int, self._exp + int(other))
        d = d._fix(context)
        py_ass_parser1543 = d
        Sensor(1543)
        return py_ass_parser1543

    def shift(self, other, context=None):
        Sensor(1544)
        """Returns a shifted copy of self, value-of-other times."""
        Sensor(1545)
        if context is None:
            Sensor(1546)
            context = getcontext()
        Sensor(1547)
        other = _convert_other(other, raiseit=True)
        ans = self._check_nans(other, context)
        Sensor(1548)
        if ans:
            py_ass_parser1549 = ans
            Sensor(1549)
            return py_ass_parser1549
        Sensor(1550)
        if other._exp != 0:
            py_ass_parser1551 = context._raise_error(InvalidOperation)
            Sensor(1551)
            return py_ass_parser1551
        Sensor(1552)
        if not -context.prec <= int(other) <= context.prec:
            py_ass_parser1553 = context._raise_error(InvalidOperation)
            Sensor(1553)
            return py_ass_parser1553
        Sensor(1554)
        if self._isinfinity():
            py_ass_parser1555 = Decimal(self)
            Sensor(1555)
            return py_ass_parser1555
        Sensor(1556)
        torot = int(other)
        rotdig = self._int
        topad = context.prec - len(rotdig)
        Sensor(1557)
        if topad > 0:
            Sensor(1558)
            rotdig = '0' * topad + rotdig
        else:
            Sensor(1559)
            if topad < 0:
                Sensor(1560)
                rotdig = rotdig[-topad:]
        Sensor(1561)
        if torot < 0:
            Sensor(1562)
            shifted = rotdig[:torot]
        else:
            Sensor(1563)
            shifted = rotdig + '0' * torot
            shifted = shifted[-context.prec:]
        py_ass_parser1564 = _dec_from_triple(self._sign, shifted.lstrip('0'
            ) or '0', self._exp)
        Sensor(1564)
        return py_ass_parser1564

    def __reduce__(self):
        py_ass_parser1565 = self.__class__, (str(self),)
        Sensor(1565)
        return py_ass_parser1565

    def __copy__(self):
        Sensor(1566)
        if type(self) is Decimal:
            py_ass_parser1567 = self
            Sensor(1567)
            return py_ass_parser1567
        py_ass_parser1568 = self.__class__(str(self))
        Sensor(1568)
        return py_ass_parser1568

    def __deepcopy__(self, memo):
        Sensor(1569)
        if type(self) is Decimal:
            py_ass_parser1570 = self
            Sensor(1570)
            return py_ass_parser1570
        py_ass_parser1571 = self.__class__(str(self))
        Sensor(1571)
        return py_ass_parser1571

    def __format__(self, specifier, context=None, _localeconv=None):
        Sensor(1572)
        """Format a Decimal instance according to the given specifier.

        The specifier should be a standard format specifier, with the
        form described in PEP 3101.  Formatting types 'e', 'E', 'f',
        'F', 'g', 'G', 'n' and '%' are supported.  If the formatting
        type is omitted it defaults to 'g' or 'G', depending on the
        value of context.capitals.
        """
        Sensor(1573)
        if context is None:
            Sensor(1574)
            context = getcontext()
        Sensor(1575)
        spec = _parse_format_specifier(specifier, _localeconv=_localeconv)
        Sensor(1576)
        if self._is_special:
            Sensor(1577)
            sign = _format_sign(self._sign, spec)
            body = str(self.copy_abs())
            py_ass_parser1578 = _format_align(sign, body, spec)
            Sensor(1578)
            return py_ass_parser1578
        Sensor(1579)
        if spec['type'] is None:
            Sensor(1580)
            spec['type'] = ['g', 'G'][context.capitals]
        Sensor(1581)
        if spec['type'] == '%':
            Sensor(1582)
            self = _dec_from_triple(self._sign, self._int, self._exp + 2)
        Sensor(1583)
        rounding = context.rounding
        precision = spec['precision']
        Sensor(1584)
        if precision is not None:
            Sensor(1585)
            if spec['type'] in 'eE':
                Sensor(1586)
                self = self._round(precision + 1, rounding)
            else:
                Sensor(1587)
                if spec['type'] in 'fF%':
                    Sensor(1588)
                    self = self._rescale(-precision, rounding)
                else:
                    Sensor(1589)
                    if spec['type'] in 'gG' and len(self._int) > precision:
                        Sensor(1590)
                        self = self._round(precision, rounding)
        Sensor(1591)
        if not self and self._exp > 0 and spec['type'] in 'fF%':
            Sensor(1592)
            self = self._rescale(0, rounding)
        Sensor(1593)
        leftdigits = self._exp + len(self._int)
        Sensor(1594)
        if spec['type'] in 'eE':
            Sensor(1595)
            if not self and precision is not None:
                Sensor(1596)
                dotplace = 1 - precision
            else:
                Sensor(1597)
                dotplace = 1
        else:
            Sensor(1598)
            if spec['type'] in 'fF%':
                Sensor(1599)
                dotplace = leftdigits
            else:
                Sensor(1600)
                if spec['type'] in 'gG':
                    Sensor(1601)
                    if self._exp <= 0 and leftdigits > -6:
                        Sensor(1602)
                        dotplace = leftdigits
                    else:
                        Sensor(1603)
                        dotplace = 1
        Sensor(1604)
        if dotplace < 0:
            Sensor(1605)
            intpart = '0'
            fracpart = '0' * -dotplace + self._int
        else:
            Sensor(1606)
            if dotplace > len(self._int):
                Sensor(1607)
                intpart = self._int + '0' * (dotplace - len(self._int))
                fracpart = ''
            else:
                Sensor(1608)
                intpart = self._int[:dotplace] or '0'
                fracpart = self._int[dotplace:]
        Sensor(1609)
        exp = leftdigits - dotplace
        py_ass_parser1610 = _format_number(self._sign, intpart, fracpart,
            exp, spec)
        Sensor(1610)
        return py_ass_parser1610


def _dec_from_triple(sign, coefficient, exponent, special=False):
    Sensor(1611)
    """Create a decimal instance directly, without any validation,
    normalization (e.g. removal of leading zeros) or argument
    conversion.

    This function is for *internal use only*.
    """
    self = object.__new__(Decimal)
    self._sign = sign
    self._int = coefficient
    self._exp = exponent
    self._is_special = special
    py_ass_parser1612 = self
    Sensor(1612)
    return py_ass_parser1612


Sensor(1613)
_numbers.Number.register(Decimal)


class _ContextManager(object):
    """Context manager class to support localcontext().

      Sets a copy of the supplied context in __enter__() and restores
      the previous decimal context in __exit__()
    """

    def __init__(self, new_context):
        Sensor(1614)
        self.new_context = new_context.copy()
        Sensor(1615)

    def __enter__(self):
        Sensor(1616)
        self.saved_context = getcontext()
        setcontext(self.new_context)
        py_ass_parser1617 = self.new_context
        Sensor(1617)
        return py_ass_parser1617

    def __exit__(self, t, v, tb):
        Sensor(1618)
        setcontext(self.saved_context)
        Sensor(1619)


class Context(object):
    """Contains the context for a Decimal instance.

    Contains:
    prec - precision (for use in rounding, division, square roots..)
    rounding - rounding type (how you round)
    traps - If traps[exception] = 1, then the exception is
                    raised when it is caused.  Otherwise, a value is
                    substituted in.
    flags  - When an exception is caused, flags[exception] is set.
             (Whether or not the trap_enabler is set)
             Should be reset by user of Decimal instance.
    Emin -   Minimum exponent
    Emax -   Maximum exponent
    capitals -      If 1, 1*10^1 is printed as 1E+1.
                    If 0, printed as 1e1
    _clamp - If 1, change exponents if too high (Default 0)
    """

    def __init__(self, prec=None, rounding=None, traps=None, flags=None,
        Emin=None, Emax=None, capitals=None, _clamp=0, _ignored_flags=None):
        Sensor(1620)
        try:
            Sensor(1621)
            dc = DefaultContext
        except NameError:
            Sensor(1622)
            pass
        Sensor(1623)
        self.prec = prec if prec is not None else dc.prec
        self.rounding = rounding if rounding is not None else dc.rounding
        self.Emin = Emin if Emin is not None else dc.Emin
        self.Emax = Emax if Emax is not None else dc.Emax
        self.capitals = capitals if capitals is not None else dc.capitals
        self._clamp = _clamp if _clamp is not None else dc._clamp
        Sensor(1624)
        if _ignored_flags is None:
            Sensor(1625)
            self._ignored_flags = []
        else:
            Sensor(1626)
            self._ignored_flags = _ignored_flags
        Sensor(1627)
        if traps is None:
            Sensor(1628)
            self.traps = dc.traps.copy()
        else:
            Sensor(1629)
            if not isinstance(traps, dict):
                Sensor(1630)
                self.traps = dict((s, int(s in traps)) for s in _signals)
            else:
                Sensor(1631)
                self.traps = traps
        Sensor(1632)
        if flags is None:
            Sensor(1633)
            self.flags = dict.fromkeys(_signals, 0)
        else:
            Sensor(1634)
            if not isinstance(flags, dict):
                Sensor(1635)
                self.flags = dict((s, int(s in flags)) for s in _signals)
            else:
                Sensor(1636)
                self.flags = flags
        Sensor(1637)

    def __repr__(self):
        Sensor(1638)
        """Show the current context."""
        s = []
        s.append(
            'Context(prec=%(prec)d, rounding=%(rounding)s, Emin=%(Emin)d, Emax=%(Emax)d, capitals=%(capitals)d'
             % vars(self))
        names = [f.__name__ for f, v in self.flags.items() if v]
        s.append('flags=[' + ', '.join(names) + ']')
        names = [t.__name__ for t, v in self.traps.items() if v]
        s.append('traps=[' + ', '.join(names) + ']')
        py_ass_parser1639 = ', '.join(s) + ')'
        Sensor(1639)
        return py_ass_parser1639

    def clear_flags(self):
        Sensor(1640)
        """Reset all flags to zero"""
        Sensor(1641)
        for flag in self.flags:
            Sensor(1642)
            self.flags[flag] = 0
        Sensor(1643)

    def _shallow_copy(self):
        Sensor(1644)
        """Returns a shallow copy from self."""
        nc = Context(self.prec, self.rounding, self.traps, self.flags, self
            .Emin, self.Emax, self.capitals, self._clamp, self._ignored_flags)
        py_ass_parser1645 = nc
        Sensor(1645)
        return py_ass_parser1645

    def copy(self):
        Sensor(1646)
        """Returns a deep copy from self."""
        nc = Context(self.prec, self.rounding, self.traps.copy(), self.
            flags.copy(), self.Emin, self.Emax, self.capitals, self._clamp,
            self._ignored_flags)
        py_ass_parser1647 = nc
        Sensor(1647)
        return py_ass_parser1647
    __copy__ = copy

    def _raise_error(self, condition, explanation=None, *args):
        Sensor(1648)
        """Handles an error

        If the flag is in _ignored_flags, returns the default response.
        Otherwise, it sets the flag, then, if the corresponding
        trap_enabler is set, it reraises the exception.  Otherwise, it returns
        the default value after setting the flag.
        """
        error = _condition_map.get(condition, condition)
        Sensor(1649)
        if error in self._ignored_flags:
            py_ass_parser1650 = error().handle(self, *args)
            Sensor(1650)
            return py_ass_parser1650
        Sensor(1651)
        self.flags[error] = 1
        Sensor(1652)
        if not self.traps[error]:
            py_ass_parser1653 = condition().handle(self, *args)
            Sensor(1653)
            return py_ass_parser1653
        Sensor(1654)
        raise error(explanation)

    def _ignore_all_flags(self):
        Sensor(1655)
        """Ignore all flags, if they are raised"""
        py_ass_parser1656 = self._ignore_flags(*_signals)
        Sensor(1656)
        return py_ass_parser1656

    def _ignore_flags(self, *flags):
        Sensor(1657)
        """Ignore the flags, if they are raised"""
        self._ignored_flags = self._ignored_flags + list(flags)
        py_ass_parser1658 = list(flags)
        Sensor(1658)
        return py_ass_parser1658

    def _regard_flags(self, *flags):
        Sensor(1659)
        """Stop ignoring the flags, if they are raised"""
        Sensor(1660)
        if flags and isinstance(flags[0], (tuple, list)):
            Sensor(1661)
            flags = flags[0]
        Sensor(1662)
        for flag in flags:
            Sensor(1663)
            self._ignored_flags.remove(flag)
        Sensor(1664)
    __hash__ = None

    def Etiny(self):
        Sensor(1665)
        """Returns Etiny (= Emin - prec + 1)"""
        py_ass_parser1666 = int(self.Emin - self.prec + 1)
        Sensor(1666)
        return py_ass_parser1666

    def Etop(self):
        Sensor(1667)
        """Returns maximum exponent (= Emax - prec + 1)"""
        py_ass_parser1668 = int(self.Emax - self.prec + 1)
        Sensor(1668)
        return py_ass_parser1668

    def _set_rounding(self, type):
        Sensor(1669)
        """Sets the rounding type.

        Sets the rounding type, and returns the current (previous)
        rounding type.  Often used like:

        context = context.copy()
        # so you don't change the calling context
        # if an error occurs in the middle.
        rounding = context._set_rounding(ROUND_UP)
        val = self.__sub__(other, context=context)
        context._set_rounding(rounding)

        This will make it round up for that operation.
        """
        rounding = self.rounding
        self.rounding = type
        py_ass_parser1670 = rounding
        Sensor(1670)
        return py_ass_parser1670

    def create_decimal(self, num='0'):
        Sensor(1671)
        """Creates a new Decimal instance but using self as context.

        This method implements the to-number operation of the
        IBM Decimal specification."""
        Sensor(1672)
        if isinstance(num, basestring) and num != num.strip():
            py_ass_parser1673 = self._raise_error(ConversionSyntax,
                'no trailing or leading whitespace is permitted.')
            Sensor(1673)
            return py_ass_parser1673
        Sensor(1674)
        d = Decimal(num, context=self)
        Sensor(1675)
        if d._isnan() and len(d._int) > self.prec - self._clamp:
            py_ass_parser1676 = self._raise_error(ConversionSyntax,
                'diagnostic info too long in NaN')
            Sensor(1676)
            return py_ass_parser1676
        py_ass_parser1677 = d._fix(self)
        Sensor(1677)
        return py_ass_parser1677

    def create_decimal_from_float(self, f):
        Sensor(1678)
        """Creates a new Decimal instance from a float but rounding using self
        as the context.

        >>> context = Context(prec=5, rounding=ROUND_DOWN)
        >>> context.create_decimal_from_float(3.1415926535897932)
        Decimal('3.1415')
        >>> context = Context(prec=5, traps=[Inexact])
        >>> context.create_decimal_from_float(3.1415926535897932)
        Traceback (most recent call last):
            ...
        Inexact: None

        """
        d = Decimal.from_float(f)
        py_ass_parser1679 = d._fix(self)
        Sensor(1679)
        return py_ass_parser1679

    def abs(self, a):
        Sensor(1680)
        """Returns the absolute value of the operand.

        If the operand is negative, the result is the same as using the minus
        operation on the operand.  Otherwise, the result is the same as using
        the plus operation on the operand.

        >>> ExtendedContext.abs(Decimal('2.1'))
        Decimal('2.1')
        >>> ExtendedContext.abs(Decimal('-100'))
        Decimal('100')
        >>> ExtendedContext.abs(Decimal('101.5'))
        Decimal('101.5')
        >>> ExtendedContext.abs(Decimal('-101.5'))
        Decimal('101.5')
        >>> ExtendedContext.abs(-1)
        Decimal('1')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1681 = a.__abs__(context=self)
        Sensor(1681)
        return py_ass_parser1681

    def add(self, a, b):
        Sensor(1682)
        """Return the sum of the two operands.

        >>> ExtendedContext.add(Decimal('12'), Decimal('7.00'))
        Decimal('19.00')
        >>> ExtendedContext.add(Decimal('1E+2'), Decimal('1.01E+4'))
        Decimal('1.02E+4')
        >>> ExtendedContext.add(1, Decimal(2))
        Decimal('3')
        >>> ExtendedContext.add(Decimal(8), 5)
        Decimal('13')
        >>> ExtendedContext.add(5, 5)
        Decimal('10')
        """
        a = _convert_other(a, raiseit=True)
        r = a.__add__(b, context=self)
        Sensor(1683)
        if r is NotImplemented:
            Sensor(1684)
            raise TypeError('Unable to convert %s to Decimal' % b)
        else:
            py_ass_parser1685 = r
            Sensor(1685)
            return py_ass_parser1685
        Sensor(1686)

    def _apply(self, a):
        py_ass_parser1687 = str(a._fix(self))
        Sensor(1687)
        return py_ass_parser1687

    def canonical(self, a):
        Sensor(1688)
        """Returns the same Decimal object.

        As we do not have different encodings for the same number, the
        received object already is in its canonical form.

        >>> ExtendedContext.canonical(Decimal('2.50'))
        Decimal('2.50')
        """
        py_ass_parser1689 = a.canonical(context=self)
        Sensor(1689)
        return py_ass_parser1689

    def compare(self, a, b):
        Sensor(1690)
        """Compares values numerically.

        If the signs of the operands differ, a value representing each operand
        ('-1' if the operand is less than zero, '0' if the operand is zero or
        negative zero, or '1' if the operand is greater than zero) is used in
        place of that operand for the comparison instead of the actual
        operand.

        The comparison is then effected by subtracting the second operand from
        the first and then returning a value according to the result of the
        subtraction: '-1' if the result is less than zero, '0' if the result is
        zero or negative zero, or '1' if the result is greater than zero.

        >>> ExtendedContext.compare(Decimal('2.1'), Decimal('3'))
        Decimal('-1')
        >>> ExtendedContext.compare(Decimal('2.1'), Decimal('2.1'))
        Decimal('0')
        >>> ExtendedContext.compare(Decimal('2.1'), Decimal('2.10'))
        Decimal('0')
        >>> ExtendedContext.compare(Decimal('3'), Decimal('2.1'))
        Decimal('1')
        >>> ExtendedContext.compare(Decimal('2.1'), Decimal('-3'))
        Decimal('1')
        >>> ExtendedContext.compare(Decimal('-3'), Decimal('2.1'))
        Decimal('-1')
        >>> ExtendedContext.compare(1, 2)
        Decimal('-1')
        >>> ExtendedContext.compare(Decimal(1), 2)
        Decimal('-1')
        >>> ExtendedContext.compare(1, Decimal(2))
        Decimal('-1')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1691 = a.compare(b, context=self)
        Sensor(1691)
        return py_ass_parser1691

    def compare_signal(self, a, b):
        Sensor(1692)
        """Compares the values of the two operands numerically.

        It's pretty much like compare(), but all NaNs signal, with signaling
        NaNs taking precedence over quiet NaNs.

        >>> c = ExtendedContext
        >>> c.compare_signal(Decimal('2.1'), Decimal('3'))
        Decimal('-1')
        >>> c.compare_signal(Decimal('2.1'), Decimal('2.1'))
        Decimal('0')
        >>> c.flags[InvalidOperation] = 0
        >>> print c.flags[InvalidOperation]
        0
        >>> c.compare_signal(Decimal('NaN'), Decimal('2.1'))
        Decimal('NaN')
        >>> print c.flags[InvalidOperation]
        1
        >>> c.flags[InvalidOperation] = 0
        >>> print c.flags[InvalidOperation]
        0
        >>> c.compare_signal(Decimal('sNaN'), Decimal('2.1'))
        Decimal('NaN')
        >>> print c.flags[InvalidOperation]
        1
        >>> c.compare_signal(-1, 2)
        Decimal('-1')
        >>> c.compare_signal(Decimal(-1), 2)
        Decimal('-1')
        >>> c.compare_signal(-1, Decimal(2))
        Decimal('-1')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1693 = a.compare_signal(b, context=self)
        Sensor(1693)
        return py_ass_parser1693

    def compare_total(self, a, b):
        Sensor(1694)
        """Compares two operands using their abstract representation.

        This is not like the standard compare, which use their numerical
        value. Note that a total ordering is defined for all possible abstract
        representations.

        >>> ExtendedContext.compare_total(Decimal('12.73'), Decimal('127.9'))
        Decimal('-1')
        >>> ExtendedContext.compare_total(Decimal('-127'),  Decimal('12'))
        Decimal('-1')
        >>> ExtendedContext.compare_total(Decimal('12.30'), Decimal('12.3'))
        Decimal('-1')
        >>> ExtendedContext.compare_total(Decimal('12.30'), Decimal('12.30'))
        Decimal('0')
        >>> ExtendedContext.compare_total(Decimal('12.3'),  Decimal('12.300'))
        Decimal('1')
        >>> ExtendedContext.compare_total(Decimal('12.3'),  Decimal('NaN'))
        Decimal('-1')
        >>> ExtendedContext.compare_total(1, 2)
        Decimal('-1')
        >>> ExtendedContext.compare_total(Decimal(1), 2)
        Decimal('-1')
        >>> ExtendedContext.compare_total(1, Decimal(2))
        Decimal('-1')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1695 = a.compare_total(b)
        Sensor(1695)
        return py_ass_parser1695

    def compare_total_mag(self, a, b):
        Sensor(1696)
        """Compares two operands using their abstract representation ignoring sign.

        Like compare_total, but with operand's sign ignored and assumed to be 0.
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1697 = a.compare_total_mag(b)
        Sensor(1697)
        return py_ass_parser1697

    def copy_abs(self, a):
        Sensor(1698)
        """Returns a copy of the operand with the sign set to 0.

        >>> ExtendedContext.copy_abs(Decimal('2.1'))
        Decimal('2.1')
        >>> ExtendedContext.copy_abs(Decimal('-100'))
        Decimal('100')
        >>> ExtendedContext.copy_abs(-1)
        Decimal('1')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1699 = a.copy_abs()
        Sensor(1699)
        return py_ass_parser1699

    def copy_decimal(self, a):
        Sensor(1700)
        """Returns a copy of the decimal object.

        >>> ExtendedContext.copy_decimal(Decimal('2.1'))
        Decimal('2.1')
        >>> ExtendedContext.copy_decimal(Decimal('-1.00'))
        Decimal('-1.00')
        >>> ExtendedContext.copy_decimal(1)
        Decimal('1')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1701 = Decimal(a)
        Sensor(1701)
        return py_ass_parser1701

    def copy_negate(self, a):
        Sensor(1702)
        """Returns a copy of the operand with the sign inverted.

        >>> ExtendedContext.copy_negate(Decimal('101.5'))
        Decimal('-101.5')
        >>> ExtendedContext.copy_negate(Decimal('-101.5'))
        Decimal('101.5')
        >>> ExtendedContext.copy_negate(1)
        Decimal('-1')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1703 = a.copy_negate()
        Sensor(1703)
        return py_ass_parser1703

    def copy_sign(self, a, b):
        Sensor(1704)
        """Copies the second operand's sign to the first one.

        In detail, it returns a copy of the first operand with the sign
        equal to the sign of the second operand.

        >>> ExtendedContext.copy_sign(Decimal( '1.50'), Decimal('7.33'))
        Decimal('1.50')
        >>> ExtendedContext.copy_sign(Decimal('-1.50'), Decimal('7.33'))
        Decimal('1.50')
        >>> ExtendedContext.copy_sign(Decimal( '1.50'), Decimal('-7.33'))
        Decimal('-1.50')
        >>> ExtendedContext.copy_sign(Decimal('-1.50'), Decimal('-7.33'))
        Decimal('-1.50')
        >>> ExtendedContext.copy_sign(1, -2)
        Decimal('-1')
        >>> ExtendedContext.copy_sign(Decimal(1), -2)
        Decimal('-1')
        >>> ExtendedContext.copy_sign(1, Decimal(-2))
        Decimal('-1')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1705 = a.copy_sign(b)
        Sensor(1705)
        return py_ass_parser1705

    def divide(self, a, b):
        Sensor(1706)
        """Decimal division in a specified context.

        >>> ExtendedContext.divide(Decimal('1'), Decimal('3'))
        Decimal('0.333333333')
        >>> ExtendedContext.divide(Decimal('2'), Decimal('3'))
        Decimal('0.666666667')
        >>> ExtendedContext.divide(Decimal('5'), Decimal('2'))
        Decimal('2.5')
        >>> ExtendedContext.divide(Decimal('1'), Decimal('10'))
        Decimal('0.1')
        >>> ExtendedContext.divide(Decimal('12'), Decimal('12'))
        Decimal('1')
        >>> ExtendedContext.divide(Decimal('8.00'), Decimal('2'))
        Decimal('4.00')
        >>> ExtendedContext.divide(Decimal('2.400'), Decimal('2.0'))
        Decimal('1.20')
        >>> ExtendedContext.divide(Decimal('1000'), Decimal('100'))
        Decimal('10')
        >>> ExtendedContext.divide(Decimal('1000'), Decimal('1'))
        Decimal('1000')
        >>> ExtendedContext.divide(Decimal('2.40E+6'), Decimal('2'))
        Decimal('1.20E+6')
        >>> ExtendedContext.divide(5, 5)
        Decimal('1')
        >>> ExtendedContext.divide(Decimal(5), 5)
        Decimal('1')
        >>> ExtendedContext.divide(5, Decimal(5))
        Decimal('1')
        """
        a = _convert_other(a, raiseit=True)
        r = a.__div__(b, context=self)
        Sensor(1707)
        if r is NotImplemented:
            Sensor(1708)
            raise TypeError('Unable to convert %s to Decimal' % b)
        else:
            py_ass_parser1709 = r
            Sensor(1709)
            return py_ass_parser1709
        Sensor(1710)

    def divide_int(self, a, b):
        Sensor(1711)
        """Divides two numbers and returns the integer part of the result.

        >>> ExtendedContext.divide_int(Decimal('2'), Decimal('3'))
        Decimal('0')
        >>> ExtendedContext.divide_int(Decimal('10'), Decimal('3'))
        Decimal('3')
        >>> ExtendedContext.divide_int(Decimal('1'), Decimal('0.3'))
        Decimal('3')
        >>> ExtendedContext.divide_int(10, 3)
        Decimal('3')
        >>> ExtendedContext.divide_int(Decimal(10), 3)
        Decimal('3')
        >>> ExtendedContext.divide_int(10, Decimal(3))
        Decimal('3')
        """
        a = _convert_other(a, raiseit=True)
        r = a.__floordiv__(b, context=self)
        Sensor(1712)
        if r is NotImplemented:
            Sensor(1713)
            raise TypeError('Unable to convert %s to Decimal' % b)
        else:
            py_ass_parser1714 = r
            Sensor(1714)
            return py_ass_parser1714
        Sensor(1715)

    def divmod(self, a, b):
        Sensor(1716)
        """Return (a // b, a % b).

        >>> ExtendedContext.divmod(Decimal(8), Decimal(3))
        (Decimal('2'), Decimal('2'))
        >>> ExtendedContext.divmod(Decimal(8), Decimal(4))
        (Decimal('2'), Decimal('0'))
        >>> ExtendedContext.divmod(8, 4)
        (Decimal('2'), Decimal('0'))
        >>> ExtendedContext.divmod(Decimal(8), 4)
        (Decimal('2'), Decimal('0'))
        >>> ExtendedContext.divmod(8, Decimal(4))
        (Decimal('2'), Decimal('0'))
        """
        a = _convert_other(a, raiseit=True)
        r = a.__divmod__(b, context=self)
        Sensor(1717)
        if r is NotImplemented:
            Sensor(1718)
            raise TypeError('Unable to convert %s to Decimal' % b)
        else:
            py_ass_parser1719 = r
            Sensor(1719)
            return py_ass_parser1719
        Sensor(1720)

    def exp(self, a):
        Sensor(1721)
        """Returns e ** a.

        >>> c = ExtendedContext.copy()
        >>> c.Emin = -999
        >>> c.Emax = 999
        >>> c.exp(Decimal('-Infinity'))
        Decimal('0')
        >>> c.exp(Decimal('-1'))
        Decimal('0.367879441')
        >>> c.exp(Decimal('0'))
        Decimal('1')
        >>> c.exp(Decimal('1'))
        Decimal('2.71828183')
        >>> c.exp(Decimal('0.693147181'))
        Decimal('2.00000000')
        >>> c.exp(Decimal('+Infinity'))
        Decimal('Infinity')
        >>> c.exp(10)
        Decimal('22026.4658')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1722 = a.exp(context=self)
        Sensor(1722)
        return py_ass_parser1722

    def fma(self, a, b, c):
        Sensor(1723)
        """Returns a multiplied by b, plus c.

        The first two operands are multiplied together, using multiply,
        the third operand is then added to the result of that
        multiplication, using add, all with only one final rounding.

        >>> ExtendedContext.fma(Decimal('3'), Decimal('5'), Decimal('7'))
        Decimal('22')
        >>> ExtendedContext.fma(Decimal('3'), Decimal('-5'), Decimal('7'))
        Decimal('-8')
        >>> ExtendedContext.fma(Decimal('888565290'), Decimal('1557.96930'), Decimal('-86087.7578'))
        Decimal('1.38435736E+12')
        >>> ExtendedContext.fma(1, 3, 4)
        Decimal('7')
        >>> ExtendedContext.fma(1, Decimal(3), 4)
        Decimal('7')
        >>> ExtendedContext.fma(1, 3, Decimal(4))
        Decimal('7')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1724 = a.fma(b, c, context=self)
        Sensor(1724)
        return py_ass_parser1724

    def is_canonical(self, a):
        Sensor(1725)
        """Return True if the operand is canonical; otherwise return False.

        Currently, the encoding of a Decimal instance is always
        canonical, so this method returns True for any Decimal.

        >>> ExtendedContext.is_canonical(Decimal('2.50'))
        True
        """
        py_ass_parser1726 = a.is_canonical()
        Sensor(1726)
        return py_ass_parser1726

    def is_finite(self, a):
        Sensor(1727)
        """Return True if the operand is finite; otherwise return False.

        A Decimal instance is considered finite if it is neither
        infinite nor a NaN.

        >>> ExtendedContext.is_finite(Decimal('2.50'))
        True
        >>> ExtendedContext.is_finite(Decimal('-0.3'))
        True
        >>> ExtendedContext.is_finite(Decimal('0'))
        True
        >>> ExtendedContext.is_finite(Decimal('Inf'))
        False
        >>> ExtendedContext.is_finite(Decimal('NaN'))
        False
        >>> ExtendedContext.is_finite(1)
        True
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1728 = a.is_finite()
        Sensor(1728)
        return py_ass_parser1728

    def is_infinite(self, a):
        Sensor(1729)
        """Return True if the operand is infinite; otherwise return False.

        >>> ExtendedContext.is_infinite(Decimal('2.50'))
        False
        >>> ExtendedContext.is_infinite(Decimal('-Inf'))
        True
        >>> ExtendedContext.is_infinite(Decimal('NaN'))
        False
        >>> ExtendedContext.is_infinite(1)
        False
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1730 = a.is_infinite()
        Sensor(1730)
        return py_ass_parser1730

    def is_nan(self, a):
        Sensor(1731)
        """Return True if the operand is a qNaN or sNaN;
        otherwise return False.

        >>> ExtendedContext.is_nan(Decimal('2.50'))
        False
        >>> ExtendedContext.is_nan(Decimal('NaN'))
        True
        >>> ExtendedContext.is_nan(Decimal('-sNaN'))
        True
        >>> ExtendedContext.is_nan(1)
        False
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1732 = a.is_nan()
        Sensor(1732)
        return py_ass_parser1732

    def is_normal(self, a):
        Sensor(1733)
        """Return True if the operand is a normal number;
        otherwise return False.

        >>> c = ExtendedContext.copy()
        >>> c.Emin = -999
        >>> c.Emax = 999
        >>> c.is_normal(Decimal('2.50'))
        True
        >>> c.is_normal(Decimal('0.1E-999'))
        False
        >>> c.is_normal(Decimal('0.00'))
        False
        >>> c.is_normal(Decimal('-Inf'))
        False
        >>> c.is_normal(Decimal('NaN'))
        False
        >>> c.is_normal(1)
        True
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1734 = a.is_normal(context=self)
        Sensor(1734)
        return py_ass_parser1734

    def is_qnan(self, a):
        Sensor(1735)
        """Return True if the operand is a quiet NaN; otherwise return False.

        >>> ExtendedContext.is_qnan(Decimal('2.50'))
        False
        >>> ExtendedContext.is_qnan(Decimal('NaN'))
        True
        >>> ExtendedContext.is_qnan(Decimal('sNaN'))
        False
        >>> ExtendedContext.is_qnan(1)
        False
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1736 = a.is_qnan()
        Sensor(1736)
        return py_ass_parser1736

    def is_signed(self, a):
        Sensor(1737)
        """Return True if the operand is negative; otherwise return False.

        >>> ExtendedContext.is_signed(Decimal('2.50'))
        False
        >>> ExtendedContext.is_signed(Decimal('-12'))
        True
        >>> ExtendedContext.is_signed(Decimal('-0'))
        True
        >>> ExtendedContext.is_signed(8)
        False
        >>> ExtendedContext.is_signed(-8)
        True
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1738 = a.is_signed()
        Sensor(1738)
        return py_ass_parser1738

    def is_snan(self, a):
        Sensor(1739)
        """Return True if the operand is a signaling NaN;
        otherwise return False.

        >>> ExtendedContext.is_snan(Decimal('2.50'))
        False
        >>> ExtendedContext.is_snan(Decimal('NaN'))
        False
        >>> ExtendedContext.is_snan(Decimal('sNaN'))
        True
        >>> ExtendedContext.is_snan(1)
        False
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1740 = a.is_snan()
        Sensor(1740)
        return py_ass_parser1740

    def is_subnormal(self, a):
        Sensor(1741)
        """Return True if the operand is subnormal; otherwise return False.

        >>> c = ExtendedContext.copy()
        >>> c.Emin = -999
        >>> c.Emax = 999
        >>> c.is_subnormal(Decimal('2.50'))
        False
        >>> c.is_subnormal(Decimal('0.1E-999'))
        True
        >>> c.is_subnormal(Decimal('0.00'))
        False
        >>> c.is_subnormal(Decimal('-Inf'))
        False
        >>> c.is_subnormal(Decimal('NaN'))
        False
        >>> c.is_subnormal(1)
        False
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1742 = a.is_subnormal(context=self)
        Sensor(1742)
        return py_ass_parser1742

    def is_zero(self, a):
        Sensor(1743)
        """Return True if the operand is a zero; otherwise return False.

        >>> ExtendedContext.is_zero(Decimal('0'))
        True
        >>> ExtendedContext.is_zero(Decimal('2.50'))
        False
        >>> ExtendedContext.is_zero(Decimal('-0E+2'))
        True
        >>> ExtendedContext.is_zero(1)
        False
        >>> ExtendedContext.is_zero(0)
        True
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1744 = a.is_zero()
        Sensor(1744)
        return py_ass_parser1744

    def ln(self, a):
        Sensor(1745)
        """Returns the natural (base e) logarithm of the operand.

        >>> c = ExtendedContext.copy()
        >>> c.Emin = -999
        >>> c.Emax = 999
        >>> c.ln(Decimal('0'))
        Decimal('-Infinity')
        >>> c.ln(Decimal('1.000'))
        Decimal('0')
        >>> c.ln(Decimal('2.71828183'))
        Decimal('1.00000000')
        >>> c.ln(Decimal('10'))
        Decimal('2.30258509')
        >>> c.ln(Decimal('+Infinity'))
        Decimal('Infinity')
        >>> c.ln(1)
        Decimal('0')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1746 = a.ln(context=self)
        Sensor(1746)
        return py_ass_parser1746

    def log10(self, a):
        Sensor(1747)
        """Returns the base 10 logarithm of the operand.

        >>> c = ExtendedContext.copy()
        >>> c.Emin = -999
        >>> c.Emax = 999
        >>> c.log10(Decimal('0'))
        Decimal('-Infinity')
        >>> c.log10(Decimal('0.001'))
        Decimal('-3')
        >>> c.log10(Decimal('1.000'))
        Decimal('0')
        >>> c.log10(Decimal('2'))
        Decimal('0.301029996')
        >>> c.log10(Decimal('10'))
        Decimal('1')
        >>> c.log10(Decimal('70'))
        Decimal('1.84509804')
        >>> c.log10(Decimal('+Infinity'))
        Decimal('Infinity')
        >>> c.log10(0)
        Decimal('-Infinity')
        >>> c.log10(1)
        Decimal('0')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1748 = a.log10(context=self)
        Sensor(1748)
        return py_ass_parser1748

    def logb(self, a):
        Sensor(1749)
        """ Returns the exponent of the magnitude of the operand's MSD.

        The result is the integer which is the exponent of the magnitude
        of the most significant digit of the operand (as though the
        operand were truncated to a single digit while maintaining the
        value of that digit and without limiting the resulting exponent).

        >>> ExtendedContext.logb(Decimal('250'))
        Decimal('2')
        >>> ExtendedContext.logb(Decimal('2.50'))
        Decimal('0')
        >>> ExtendedContext.logb(Decimal('0.03'))
        Decimal('-2')
        >>> ExtendedContext.logb(Decimal('0'))
        Decimal('-Infinity')
        >>> ExtendedContext.logb(1)
        Decimal('0')
        >>> ExtendedContext.logb(10)
        Decimal('1')
        >>> ExtendedContext.logb(100)
        Decimal('2')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1750 = a.logb(context=self)
        Sensor(1750)
        return py_ass_parser1750

    def logical_and(self, a, b):
        Sensor(1751)
        """Applies the logical operation 'and' between each operand's digits.

        The operands must be both logical numbers.

        >>> ExtendedContext.logical_and(Decimal('0'), Decimal('0'))
        Decimal('0')
        >>> ExtendedContext.logical_and(Decimal('0'), Decimal('1'))
        Decimal('0')
        >>> ExtendedContext.logical_and(Decimal('1'), Decimal('0'))
        Decimal('0')
        >>> ExtendedContext.logical_and(Decimal('1'), Decimal('1'))
        Decimal('1')
        >>> ExtendedContext.logical_and(Decimal('1100'), Decimal('1010'))
        Decimal('1000')
        >>> ExtendedContext.logical_and(Decimal('1111'), Decimal('10'))
        Decimal('10')
        >>> ExtendedContext.logical_and(110, 1101)
        Decimal('100')
        >>> ExtendedContext.logical_and(Decimal(110), 1101)
        Decimal('100')
        >>> ExtendedContext.logical_and(110, Decimal(1101))
        Decimal('100')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1752 = a.logical_and(b, context=self)
        Sensor(1752)
        return py_ass_parser1752

    def logical_invert(self, a):
        Sensor(1753)
        """Invert all the digits in the operand.

        The operand must be a logical number.

        >>> ExtendedContext.logical_invert(Decimal('0'))
        Decimal('111111111')
        >>> ExtendedContext.logical_invert(Decimal('1'))
        Decimal('111111110')
        >>> ExtendedContext.logical_invert(Decimal('111111111'))
        Decimal('0')
        >>> ExtendedContext.logical_invert(Decimal('101010101'))
        Decimal('10101010')
        >>> ExtendedContext.logical_invert(1101)
        Decimal('111110010')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1754 = a.logical_invert(context=self)
        Sensor(1754)
        return py_ass_parser1754

    def logical_or(self, a, b):
        Sensor(1755)
        """Applies the logical operation 'or' between each operand's digits.

        The operands must be both logical numbers.

        >>> ExtendedContext.logical_or(Decimal('0'), Decimal('0'))
        Decimal('0')
        >>> ExtendedContext.logical_or(Decimal('0'), Decimal('1'))
        Decimal('1')
        >>> ExtendedContext.logical_or(Decimal('1'), Decimal('0'))
        Decimal('1')
        >>> ExtendedContext.logical_or(Decimal('1'), Decimal('1'))
        Decimal('1')
        >>> ExtendedContext.logical_or(Decimal('1100'), Decimal('1010'))
        Decimal('1110')
        >>> ExtendedContext.logical_or(Decimal('1110'), Decimal('10'))
        Decimal('1110')
        >>> ExtendedContext.logical_or(110, 1101)
        Decimal('1111')
        >>> ExtendedContext.logical_or(Decimal(110), 1101)
        Decimal('1111')
        >>> ExtendedContext.logical_or(110, Decimal(1101))
        Decimal('1111')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1756 = a.logical_or(b, context=self)
        Sensor(1756)
        return py_ass_parser1756

    def logical_xor(self, a, b):
        Sensor(1757)
        """Applies the logical operation 'xor' between each operand's digits.

        The operands must be both logical numbers.

        >>> ExtendedContext.logical_xor(Decimal('0'), Decimal('0'))
        Decimal('0')
        >>> ExtendedContext.logical_xor(Decimal('0'), Decimal('1'))
        Decimal('1')
        >>> ExtendedContext.logical_xor(Decimal('1'), Decimal('0'))
        Decimal('1')
        >>> ExtendedContext.logical_xor(Decimal('1'), Decimal('1'))
        Decimal('0')
        >>> ExtendedContext.logical_xor(Decimal('1100'), Decimal('1010'))
        Decimal('110')
        >>> ExtendedContext.logical_xor(Decimal('1111'), Decimal('10'))
        Decimal('1101')
        >>> ExtendedContext.logical_xor(110, 1101)
        Decimal('1011')
        >>> ExtendedContext.logical_xor(Decimal(110), 1101)
        Decimal('1011')
        >>> ExtendedContext.logical_xor(110, Decimal(1101))
        Decimal('1011')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1758 = a.logical_xor(b, context=self)
        Sensor(1758)
        return py_ass_parser1758

    def max(self, a, b):
        Sensor(1759)
        """max compares two values numerically and returns the maximum.

        If either operand is a NaN then the general rules apply.
        Otherwise, the operands are compared as though by the compare
        operation.  If they are numerically equal then the left-hand operand
        is chosen as the result.  Otherwise the maximum (closer to positive
        infinity) of the two operands is chosen as the result.

        >>> ExtendedContext.max(Decimal('3'), Decimal('2'))
        Decimal('3')
        >>> ExtendedContext.max(Decimal('-10'), Decimal('3'))
        Decimal('3')
        >>> ExtendedContext.max(Decimal('1.0'), Decimal('1'))
        Decimal('1')
        >>> ExtendedContext.max(Decimal('7'), Decimal('NaN'))
        Decimal('7')
        >>> ExtendedContext.max(1, 2)
        Decimal('2')
        >>> ExtendedContext.max(Decimal(1), 2)
        Decimal('2')
        >>> ExtendedContext.max(1, Decimal(2))
        Decimal('2')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1760 = a.max(b, context=self)
        Sensor(1760)
        return py_ass_parser1760

    def max_mag(self, a, b):
        Sensor(1761)
        """Compares the values numerically with their sign ignored.

        >>> ExtendedContext.max_mag(Decimal('7'), Decimal('NaN'))
        Decimal('7')
        >>> ExtendedContext.max_mag(Decimal('7'), Decimal('-10'))
        Decimal('-10')
        >>> ExtendedContext.max_mag(1, -2)
        Decimal('-2')
        >>> ExtendedContext.max_mag(Decimal(1), -2)
        Decimal('-2')
        >>> ExtendedContext.max_mag(1, Decimal(-2))
        Decimal('-2')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1762 = a.max_mag(b, context=self)
        Sensor(1762)
        return py_ass_parser1762

    def min(self, a, b):
        Sensor(1763)
        """min compares two values numerically and returns the minimum.

        If either operand is a NaN then the general rules apply.
        Otherwise, the operands are compared as though by the compare
        operation.  If they are numerically equal then the left-hand operand
        is chosen as the result.  Otherwise the minimum (closer to negative
        infinity) of the two operands is chosen as the result.

        >>> ExtendedContext.min(Decimal('3'), Decimal('2'))
        Decimal('2')
        >>> ExtendedContext.min(Decimal('-10'), Decimal('3'))
        Decimal('-10')
        >>> ExtendedContext.min(Decimal('1.0'), Decimal('1'))
        Decimal('1.0')
        >>> ExtendedContext.min(Decimal('7'), Decimal('NaN'))
        Decimal('7')
        >>> ExtendedContext.min(1, 2)
        Decimal('1')
        >>> ExtendedContext.min(Decimal(1), 2)
        Decimal('1')
        >>> ExtendedContext.min(1, Decimal(29))
        Decimal('1')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1764 = a.min(b, context=self)
        Sensor(1764)
        return py_ass_parser1764

    def min_mag(self, a, b):
        Sensor(1765)
        """Compares the values numerically with their sign ignored.

        >>> ExtendedContext.min_mag(Decimal('3'), Decimal('-2'))
        Decimal('-2')
        >>> ExtendedContext.min_mag(Decimal('-3'), Decimal('NaN'))
        Decimal('-3')
        >>> ExtendedContext.min_mag(1, -2)
        Decimal('1')
        >>> ExtendedContext.min_mag(Decimal(1), -2)
        Decimal('1')
        >>> ExtendedContext.min_mag(1, Decimal(-2))
        Decimal('1')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1766 = a.min_mag(b, context=self)
        Sensor(1766)
        return py_ass_parser1766

    def minus(self, a):
        Sensor(1767)
        """Minus corresponds to unary prefix minus in Python.

        The operation is evaluated using the same rules as subtract; the
        operation minus(a) is calculated as subtract('0', a) where the '0'
        has the same exponent as the operand.

        >>> ExtendedContext.minus(Decimal('1.3'))
        Decimal('-1.3')
        >>> ExtendedContext.minus(Decimal('-1.3'))
        Decimal('1.3')
        >>> ExtendedContext.minus(1)
        Decimal('-1')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1768 = a.__neg__(context=self)
        Sensor(1768)
        return py_ass_parser1768

    def multiply(self, a, b):
        Sensor(1769)
        """multiply multiplies two operands.

        If either operand is a special value then the general rules apply.
        Otherwise, the operands are multiplied together
        ('long multiplication'), resulting in a number which may be as long as
        the sum of the lengths of the two operands.

        >>> ExtendedContext.multiply(Decimal('1.20'), Decimal('3'))
        Decimal('3.60')
        >>> ExtendedContext.multiply(Decimal('7'), Decimal('3'))
        Decimal('21')
        >>> ExtendedContext.multiply(Decimal('0.9'), Decimal('0.8'))
        Decimal('0.72')
        >>> ExtendedContext.multiply(Decimal('0.9'), Decimal('-0'))
        Decimal('-0.0')
        >>> ExtendedContext.multiply(Decimal('654321'), Decimal('654321'))
        Decimal('4.28135971E+11')
        >>> ExtendedContext.multiply(7, 7)
        Decimal('49')
        >>> ExtendedContext.multiply(Decimal(7), 7)
        Decimal('49')
        >>> ExtendedContext.multiply(7, Decimal(7))
        Decimal('49')
        """
        a = _convert_other(a, raiseit=True)
        r = a.__mul__(b, context=self)
        Sensor(1770)
        if r is NotImplemented:
            Sensor(1771)
            raise TypeError('Unable to convert %s to Decimal' % b)
        else:
            py_ass_parser1772 = r
            Sensor(1772)
            return py_ass_parser1772
        Sensor(1773)

    def next_minus(self, a):
        Sensor(1774)
        """Returns the largest representable number smaller than a.

        >>> c = ExtendedContext.copy()
        >>> c.Emin = -999
        >>> c.Emax = 999
        >>> ExtendedContext.next_minus(Decimal('1'))
        Decimal('0.999999999')
        >>> c.next_minus(Decimal('1E-1007'))
        Decimal('0E-1007')
        >>> ExtendedContext.next_minus(Decimal('-1.00000003'))
        Decimal('-1.00000004')
        >>> c.next_minus(Decimal('Infinity'))
        Decimal('9.99999999E+999')
        >>> c.next_minus(1)
        Decimal('0.999999999')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1775 = a.next_minus(context=self)
        Sensor(1775)
        return py_ass_parser1775

    def next_plus(self, a):
        Sensor(1776)
        """Returns the smallest representable number larger than a.

        >>> c = ExtendedContext.copy()
        >>> c.Emin = -999
        >>> c.Emax = 999
        >>> ExtendedContext.next_plus(Decimal('1'))
        Decimal('1.00000001')
        >>> c.next_plus(Decimal('-1E-1007'))
        Decimal('-0E-1007')
        >>> ExtendedContext.next_plus(Decimal('-1.00000003'))
        Decimal('-1.00000002')
        >>> c.next_plus(Decimal('-Infinity'))
        Decimal('-9.99999999E+999')
        >>> c.next_plus(1)
        Decimal('1.00000001')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1777 = a.next_plus(context=self)
        Sensor(1777)
        return py_ass_parser1777

    def next_toward(self, a, b):
        Sensor(1778)
        """Returns the number closest to a, in direction towards b.

        The result is the closest representable number from the first
        operand (but not the first operand) that is in the direction
        towards the second operand, unless the operands have the same
        value.

        >>> c = ExtendedContext.copy()
        >>> c.Emin = -999
        >>> c.Emax = 999
        >>> c.next_toward(Decimal('1'), Decimal('2'))
        Decimal('1.00000001')
        >>> c.next_toward(Decimal('-1E-1007'), Decimal('1'))
        Decimal('-0E-1007')
        >>> c.next_toward(Decimal('-1.00000003'), Decimal('0'))
        Decimal('-1.00000002')
        >>> c.next_toward(Decimal('1'), Decimal('0'))
        Decimal('0.999999999')
        >>> c.next_toward(Decimal('1E-1007'), Decimal('-100'))
        Decimal('0E-1007')
        >>> c.next_toward(Decimal('-1.00000003'), Decimal('-10'))
        Decimal('-1.00000004')
        >>> c.next_toward(Decimal('0.00'), Decimal('-0.0000'))
        Decimal('-0.00')
        >>> c.next_toward(0, 1)
        Decimal('1E-1007')
        >>> c.next_toward(Decimal(0), 1)
        Decimal('1E-1007')
        >>> c.next_toward(0, Decimal(1))
        Decimal('1E-1007')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1779 = a.next_toward(b, context=self)
        Sensor(1779)
        return py_ass_parser1779

    def normalize(self, a):
        Sensor(1780)
        """normalize reduces an operand to its simplest form.

        Essentially a plus operation with all trailing zeros removed from the
        result.

        >>> ExtendedContext.normalize(Decimal('2.1'))
        Decimal('2.1')
        >>> ExtendedContext.normalize(Decimal('-2.0'))
        Decimal('-2')
        >>> ExtendedContext.normalize(Decimal('1.200'))
        Decimal('1.2')
        >>> ExtendedContext.normalize(Decimal('-120'))
        Decimal('-1.2E+2')
        >>> ExtendedContext.normalize(Decimal('120.00'))
        Decimal('1.2E+2')
        >>> ExtendedContext.normalize(Decimal('0.00'))
        Decimal('0')
        >>> ExtendedContext.normalize(6)
        Decimal('6')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1781 = a.normalize(context=self)
        Sensor(1781)
        return py_ass_parser1781

    def number_class(self, a):
        Sensor(1782)
        """Returns an indication of the class of the operand.

        The class is one of the following strings:
          -sNaN
          -NaN
          -Infinity
          -Normal
          -Subnormal
          -Zero
          +Zero
          +Subnormal
          +Normal
          +Infinity

        >>> c = Context(ExtendedContext)
        >>> c.Emin = -999
        >>> c.Emax = 999
        >>> c.number_class(Decimal('Infinity'))
        '+Infinity'
        >>> c.number_class(Decimal('1E-10'))
        '+Normal'
        >>> c.number_class(Decimal('2.50'))
        '+Normal'
        >>> c.number_class(Decimal('0.1E-999'))
        '+Subnormal'
        >>> c.number_class(Decimal('0'))
        '+Zero'
        >>> c.number_class(Decimal('-0'))
        '-Zero'
        >>> c.number_class(Decimal('-0.1E-999'))
        '-Subnormal'
        >>> c.number_class(Decimal('-1E-10'))
        '-Normal'
        >>> c.number_class(Decimal('-2.50'))
        '-Normal'
        >>> c.number_class(Decimal('-Infinity'))
        '-Infinity'
        >>> c.number_class(Decimal('NaN'))
        'NaN'
        >>> c.number_class(Decimal('-NaN'))
        'NaN'
        >>> c.number_class(Decimal('sNaN'))
        'sNaN'
        >>> c.number_class(123)
        '+Normal'
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1783 = a.number_class(context=self)
        Sensor(1783)
        return py_ass_parser1783

    def plus(self, a):
        Sensor(1784)
        """Plus corresponds to unary prefix plus in Python.

        The operation is evaluated using the same rules as add; the
        operation plus(a) is calculated as add('0', a) where the '0'
        has the same exponent as the operand.

        >>> ExtendedContext.plus(Decimal('1.3'))
        Decimal('1.3')
        >>> ExtendedContext.plus(Decimal('-1.3'))
        Decimal('-1.3')
        >>> ExtendedContext.plus(-1)
        Decimal('-1')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1785 = a.__pos__(context=self)
        Sensor(1785)
        return py_ass_parser1785

    def power(self, a, b, modulo=None):
        Sensor(1786)
        """Raises a to the power of b, to modulo if given.

        With two arguments, compute a**b.  If a is negative then b
        must be integral.  The result will be inexact unless b is
        integral and the result is finite and can be expressed exactly
        in 'precision' digits.

        With three arguments, compute (a**b) % modulo.  For the
        three argument form, the following restrictions on the
        arguments hold:

         - all three arguments must be integral
         - b must be nonnegative
         - at least one of a or b must be nonzero
         - modulo must be nonzero and have at most 'precision' digits

        The result of pow(a, b, modulo) is identical to the result
        that would be obtained by computing (a**b) % modulo with
        unbounded precision, but is computed more efficiently.  It is
        always exact.

        >>> c = ExtendedContext.copy()
        >>> c.Emin = -999
        >>> c.Emax = 999
        >>> c.power(Decimal('2'), Decimal('3'))
        Decimal('8')
        >>> c.power(Decimal('-2'), Decimal('3'))
        Decimal('-8')
        >>> c.power(Decimal('2'), Decimal('-3'))
        Decimal('0.125')
        >>> c.power(Decimal('1.7'), Decimal('8'))
        Decimal('69.7575744')
        >>> c.power(Decimal('10'), Decimal('0.301029996'))
        Decimal('2.00000000')
        >>> c.power(Decimal('Infinity'), Decimal('-1'))
        Decimal('0')
        >>> c.power(Decimal('Infinity'), Decimal('0'))
        Decimal('1')
        >>> c.power(Decimal('Infinity'), Decimal('1'))
        Decimal('Infinity')
        >>> c.power(Decimal('-Infinity'), Decimal('-1'))
        Decimal('-0')
        >>> c.power(Decimal('-Infinity'), Decimal('0'))
        Decimal('1')
        >>> c.power(Decimal('-Infinity'), Decimal('1'))
        Decimal('-Infinity')
        >>> c.power(Decimal('-Infinity'), Decimal('2'))
        Decimal('Infinity')
        >>> c.power(Decimal('0'), Decimal('0'))
        Decimal('NaN')

        >>> c.power(Decimal('3'), Decimal('7'), Decimal('16'))
        Decimal('11')
        >>> c.power(Decimal('-3'), Decimal('7'), Decimal('16'))
        Decimal('-11')
        >>> c.power(Decimal('-3'), Decimal('8'), Decimal('16'))
        Decimal('1')
        >>> c.power(Decimal('3'), Decimal('7'), Decimal('-16'))
        Decimal('11')
        >>> c.power(Decimal('23E12345'), Decimal('67E189'), Decimal('123456789'))
        Decimal('11729830')
        >>> c.power(Decimal('-0'), Decimal('17'), Decimal('1729'))
        Decimal('-0')
        >>> c.power(Decimal('-23'), Decimal('0'), Decimal('65537'))
        Decimal('1')
        >>> ExtendedContext.power(7, 7)
        Decimal('823543')
        >>> ExtendedContext.power(Decimal(7), 7)
        Decimal('823543')
        >>> ExtendedContext.power(7, Decimal(7), 2)
        Decimal('1')
        """
        a = _convert_other(a, raiseit=True)
        r = a.__pow__(b, modulo, context=self)
        Sensor(1787)
        if r is NotImplemented:
            Sensor(1788)
            raise TypeError('Unable to convert %s to Decimal' % b)
        else:
            py_ass_parser1789 = r
            Sensor(1789)
            return py_ass_parser1789
        Sensor(1790)

    def quantize(self, a, b):
        Sensor(1791)
        """Returns a value equal to 'a' (rounded), having the exponent of 'b'.

        The coefficient of the result is derived from that of the left-hand
        operand.  It may be rounded using the current rounding setting (if the
        exponent is being increased), multiplied by a positive power of ten (if
        the exponent is being decreased), or is unchanged (if the exponent is
        already equal to that of the right-hand operand).

        Unlike other operations, if the length of the coefficient after the
        quantize operation would be greater than precision then an Invalid
        operation condition is raised.  This guarantees that, unless there is
        an error condition, the exponent of the result of a quantize is always
        equal to that of the right-hand operand.

        Also unlike other operations, quantize will never raise Underflow, even
        if the result is subnormal and inexact.

        >>> ExtendedContext.quantize(Decimal('2.17'), Decimal('0.001'))
        Decimal('2.170')
        >>> ExtendedContext.quantize(Decimal('2.17'), Decimal('0.01'))
        Decimal('2.17')
        >>> ExtendedContext.quantize(Decimal('2.17'), Decimal('0.1'))
        Decimal('2.2')
        >>> ExtendedContext.quantize(Decimal('2.17'), Decimal('1e+0'))
        Decimal('2')
        >>> ExtendedContext.quantize(Decimal('2.17'), Decimal('1e+1'))
        Decimal('0E+1')
        >>> ExtendedContext.quantize(Decimal('-Inf'), Decimal('Infinity'))
        Decimal('-Infinity')
        >>> ExtendedContext.quantize(Decimal('2'), Decimal('Infinity'))
        Decimal('NaN')
        >>> ExtendedContext.quantize(Decimal('-0.1'), Decimal('1'))
        Decimal('-0')
        >>> ExtendedContext.quantize(Decimal('-0'), Decimal('1e+5'))
        Decimal('-0E+5')
        >>> ExtendedContext.quantize(Decimal('+35236450.6'), Decimal('1e-2'))
        Decimal('NaN')
        >>> ExtendedContext.quantize(Decimal('-35236450.6'), Decimal('1e-2'))
        Decimal('NaN')
        >>> ExtendedContext.quantize(Decimal('217'), Decimal('1e-1'))
        Decimal('217.0')
        >>> ExtendedContext.quantize(Decimal('217'), Decimal('1e-0'))
        Decimal('217')
        >>> ExtendedContext.quantize(Decimal('217'), Decimal('1e+1'))
        Decimal('2.2E+2')
        >>> ExtendedContext.quantize(Decimal('217'), Decimal('1e+2'))
        Decimal('2E+2')
        >>> ExtendedContext.quantize(1, 2)
        Decimal('1')
        >>> ExtendedContext.quantize(Decimal(1), 2)
        Decimal('1')
        >>> ExtendedContext.quantize(1, Decimal(2))
        Decimal('1')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1792 = a.quantize(b, context=self)
        Sensor(1792)
        return py_ass_parser1792

    def radix(self):
        Sensor(1793)
        """Just returns 10, as this is Decimal, :)

        >>> ExtendedContext.radix()
        Decimal('10')
        """
        py_ass_parser1794 = Decimal(10)
        Sensor(1794)
        return py_ass_parser1794

    def remainder(self, a, b):
        Sensor(1795)
        """Returns the remainder from integer division.

        The result is the residue of the dividend after the operation of
        calculating integer division as described for divide-integer, rounded
        to precision digits if necessary.  The sign of the result, if
        non-zero, is the same as that of the original dividend.

        This operation will fail under the same conditions as integer division
        (that is, if integer division on the same two operands would fail, the
        remainder cannot be calculated).

        >>> ExtendedContext.remainder(Decimal('2.1'), Decimal('3'))
        Decimal('2.1')
        >>> ExtendedContext.remainder(Decimal('10'), Decimal('3'))
        Decimal('1')
        >>> ExtendedContext.remainder(Decimal('-10'), Decimal('3'))
        Decimal('-1')
        >>> ExtendedContext.remainder(Decimal('10.2'), Decimal('1'))
        Decimal('0.2')
        >>> ExtendedContext.remainder(Decimal('10'), Decimal('0.3'))
        Decimal('0.1')
        >>> ExtendedContext.remainder(Decimal('3.6'), Decimal('1.3'))
        Decimal('1.0')
        >>> ExtendedContext.remainder(22, 6)
        Decimal('4')
        >>> ExtendedContext.remainder(Decimal(22), 6)
        Decimal('4')
        >>> ExtendedContext.remainder(22, Decimal(6))
        Decimal('4')
        """
        a = _convert_other(a, raiseit=True)
        r = a.__mod__(b, context=self)
        Sensor(1796)
        if r is NotImplemented:
            Sensor(1797)
            raise TypeError('Unable to convert %s to Decimal' % b)
        else:
            py_ass_parser1798 = r
            Sensor(1798)
            return py_ass_parser1798
        Sensor(1799)

    def remainder_near(self, a, b):
        Sensor(1800)
        """Returns to be "a - b * n", where n is the integer nearest the exact
        value of "x / b" (if two integers are equally near then the even one
        is chosen).  If the result is equal to 0 then its sign will be the
        sign of a.

        This operation will fail under the same conditions as integer division
        (that is, if integer division on the same two operands would fail, the
        remainder cannot be calculated).

        >>> ExtendedContext.remainder_near(Decimal('2.1'), Decimal('3'))
        Decimal('-0.9')
        >>> ExtendedContext.remainder_near(Decimal('10'), Decimal('6'))
        Decimal('-2')
        >>> ExtendedContext.remainder_near(Decimal('10'), Decimal('3'))
        Decimal('1')
        >>> ExtendedContext.remainder_near(Decimal('-10'), Decimal('3'))
        Decimal('-1')
        >>> ExtendedContext.remainder_near(Decimal('10.2'), Decimal('1'))
        Decimal('0.2')
        >>> ExtendedContext.remainder_near(Decimal('10'), Decimal('0.3'))
        Decimal('0.1')
        >>> ExtendedContext.remainder_near(Decimal('3.6'), Decimal('1.3'))
        Decimal('-0.3')
        >>> ExtendedContext.remainder_near(3, 11)
        Decimal('3')
        >>> ExtendedContext.remainder_near(Decimal(3), 11)
        Decimal('3')
        >>> ExtendedContext.remainder_near(3, Decimal(11))
        Decimal('3')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1801 = a.remainder_near(b, context=self)
        Sensor(1801)
        return py_ass_parser1801

    def rotate(self, a, b):
        Sensor(1802)
        """Returns a rotated copy of a, b times.

        The coefficient of the result is a rotated copy of the digits in
        the coefficient of the first operand.  The number of places of
        rotation is taken from the absolute value of the second operand,
        with the rotation being to the left if the second operand is
        positive or to the right otherwise.

        >>> ExtendedContext.rotate(Decimal('34'), Decimal('8'))
        Decimal('400000003')
        >>> ExtendedContext.rotate(Decimal('12'), Decimal('9'))
        Decimal('12')
        >>> ExtendedContext.rotate(Decimal('123456789'), Decimal('-2'))
        Decimal('891234567')
        >>> ExtendedContext.rotate(Decimal('123456789'), Decimal('0'))
        Decimal('123456789')
        >>> ExtendedContext.rotate(Decimal('123456789'), Decimal('+2'))
        Decimal('345678912')
        >>> ExtendedContext.rotate(1333333, 1)
        Decimal('13333330')
        >>> ExtendedContext.rotate(Decimal(1333333), 1)
        Decimal('13333330')
        >>> ExtendedContext.rotate(1333333, Decimal(1))
        Decimal('13333330')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1803 = a.rotate(b, context=self)
        Sensor(1803)
        return py_ass_parser1803

    def same_quantum(self, a, b):
        Sensor(1804)
        """Returns True if the two operands have the same exponent.

        The result is never affected by either the sign or the coefficient of
        either operand.

        >>> ExtendedContext.same_quantum(Decimal('2.17'), Decimal('0.001'))
        False
        >>> ExtendedContext.same_quantum(Decimal('2.17'), Decimal('0.01'))
        True
        >>> ExtendedContext.same_quantum(Decimal('2.17'), Decimal('1'))
        False
        >>> ExtendedContext.same_quantum(Decimal('Inf'), Decimal('-Inf'))
        True
        >>> ExtendedContext.same_quantum(10000, -1)
        True
        >>> ExtendedContext.same_quantum(Decimal(10000), -1)
        True
        >>> ExtendedContext.same_quantum(10000, Decimal(-1))
        True
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1805 = a.same_quantum(b)
        Sensor(1805)
        return py_ass_parser1805

    def scaleb(self, a, b):
        Sensor(1806)
        """Returns the first operand after adding the second value its exp.

        >>> ExtendedContext.scaleb(Decimal('7.50'), Decimal('-2'))
        Decimal('0.0750')
        >>> ExtendedContext.scaleb(Decimal('7.50'), Decimal('0'))
        Decimal('7.50')
        >>> ExtendedContext.scaleb(Decimal('7.50'), Decimal('3'))
        Decimal('7.50E+3')
        >>> ExtendedContext.scaleb(1, 4)
        Decimal('1E+4')
        >>> ExtendedContext.scaleb(Decimal(1), 4)
        Decimal('1E+4')
        >>> ExtendedContext.scaleb(1, Decimal(4))
        Decimal('1E+4')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1807 = a.scaleb(b, context=self)
        Sensor(1807)
        return py_ass_parser1807

    def shift(self, a, b):
        Sensor(1808)
        """Returns a shifted copy of a, b times.

        The coefficient of the result is a shifted copy of the digits
        in the coefficient of the first operand.  The number of places
        to shift is taken from the absolute value of the second operand,
        with the shift being to the left if the second operand is
        positive or to the right otherwise.  Digits shifted into the
        coefficient are zeros.

        >>> ExtendedContext.shift(Decimal('34'), Decimal('8'))
        Decimal('400000000')
        >>> ExtendedContext.shift(Decimal('12'), Decimal('9'))
        Decimal('0')
        >>> ExtendedContext.shift(Decimal('123456789'), Decimal('-2'))
        Decimal('1234567')
        >>> ExtendedContext.shift(Decimal('123456789'), Decimal('0'))
        Decimal('123456789')
        >>> ExtendedContext.shift(Decimal('123456789'), Decimal('+2'))
        Decimal('345678900')
        >>> ExtendedContext.shift(88888888, 2)
        Decimal('888888800')
        >>> ExtendedContext.shift(Decimal(88888888), 2)
        Decimal('888888800')
        >>> ExtendedContext.shift(88888888, Decimal(2))
        Decimal('888888800')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1809 = a.shift(b, context=self)
        Sensor(1809)
        return py_ass_parser1809

    def sqrt(self, a):
        Sensor(1810)
        """Square root of a non-negative number to context precision.

        If the result must be inexact, it is rounded using the round-half-even
        algorithm.

        >>> ExtendedContext.sqrt(Decimal('0'))
        Decimal('0')
        >>> ExtendedContext.sqrt(Decimal('-0'))
        Decimal('-0')
        >>> ExtendedContext.sqrt(Decimal('0.39'))
        Decimal('0.624499800')
        >>> ExtendedContext.sqrt(Decimal('100'))
        Decimal('10')
        >>> ExtendedContext.sqrt(Decimal('1'))
        Decimal('1')
        >>> ExtendedContext.sqrt(Decimal('1.0'))
        Decimal('1.0')
        >>> ExtendedContext.sqrt(Decimal('1.00'))
        Decimal('1.0')
        >>> ExtendedContext.sqrt(Decimal('7'))
        Decimal('2.64575131')
        >>> ExtendedContext.sqrt(Decimal('10'))
        Decimal('3.16227766')
        >>> ExtendedContext.sqrt(2)
        Decimal('1.41421356')
        >>> ExtendedContext.prec
        9
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1811 = a.sqrt(context=self)
        Sensor(1811)
        return py_ass_parser1811

    def subtract(self, a, b):
        Sensor(1812)
        """Return the difference between the two operands.

        >>> ExtendedContext.subtract(Decimal('1.3'), Decimal('1.07'))
        Decimal('0.23')
        >>> ExtendedContext.subtract(Decimal('1.3'), Decimal('1.30'))
        Decimal('0.00')
        >>> ExtendedContext.subtract(Decimal('1.3'), Decimal('2.07'))
        Decimal('-0.77')
        >>> ExtendedContext.subtract(8, 5)
        Decimal('3')
        >>> ExtendedContext.subtract(Decimal(8), 5)
        Decimal('3')
        >>> ExtendedContext.subtract(8, Decimal(5))
        Decimal('3')
        """
        a = _convert_other(a, raiseit=True)
        r = a.__sub__(b, context=self)
        Sensor(1813)
        if r is NotImplemented:
            Sensor(1814)
            raise TypeError('Unable to convert %s to Decimal' % b)
        else:
            py_ass_parser1815 = r
            Sensor(1815)
            return py_ass_parser1815
        Sensor(1816)

    def to_eng_string(self, a):
        Sensor(1817)
        """Converts a number to a string, using scientific notation.

        The operation is not affected by the context.
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1818 = a.to_eng_string(context=self)
        Sensor(1818)
        return py_ass_parser1818

    def to_sci_string(self, a):
        Sensor(1819)
        """Converts a number to a string, using scientific notation.

        The operation is not affected by the context.
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1820 = a.__str__(context=self)
        Sensor(1820)
        return py_ass_parser1820

    def to_integral_exact(self, a):
        Sensor(1821)
        """Rounds to an integer.

        When the operand has a negative exponent, the result is the same
        as using the quantize() operation using the given operand as the
        left-hand-operand, 1E+0 as the right-hand-operand, and the precision
        of the operand as the precision setting; Inexact and Rounded flags
        are allowed in this operation.  The rounding mode is taken from the
        context.

        >>> ExtendedContext.to_integral_exact(Decimal('2.1'))
        Decimal('2')
        >>> ExtendedContext.to_integral_exact(Decimal('100'))
        Decimal('100')
        >>> ExtendedContext.to_integral_exact(Decimal('100.0'))
        Decimal('100')
        >>> ExtendedContext.to_integral_exact(Decimal('101.5'))
        Decimal('102')
        >>> ExtendedContext.to_integral_exact(Decimal('-101.5'))
        Decimal('-102')
        >>> ExtendedContext.to_integral_exact(Decimal('10E+5'))
        Decimal('1.0E+6')
        >>> ExtendedContext.to_integral_exact(Decimal('7.89E+77'))
        Decimal('7.89E+77')
        >>> ExtendedContext.to_integral_exact(Decimal('-Inf'))
        Decimal('-Infinity')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1822 = a.to_integral_exact(context=self)
        Sensor(1822)
        return py_ass_parser1822

    def to_integral_value(self, a):
        Sensor(1823)
        """Rounds to an integer.

        When the operand has a negative exponent, the result is the same
        as using the quantize() operation using the given operand as the
        left-hand-operand, 1E+0 as the right-hand-operand, and the precision
        of the operand as the precision setting, except that no flags will
        be set.  The rounding mode is taken from the context.

        >>> ExtendedContext.to_integral_value(Decimal('2.1'))
        Decimal('2')
        >>> ExtendedContext.to_integral_value(Decimal('100'))
        Decimal('100')
        >>> ExtendedContext.to_integral_value(Decimal('100.0'))
        Decimal('100')
        >>> ExtendedContext.to_integral_value(Decimal('101.5'))
        Decimal('102')
        >>> ExtendedContext.to_integral_value(Decimal('-101.5'))
        Decimal('-102')
        >>> ExtendedContext.to_integral_value(Decimal('10E+5'))
        Decimal('1.0E+6')
        >>> ExtendedContext.to_integral_value(Decimal('7.89E+77'))
        Decimal('7.89E+77')
        >>> ExtendedContext.to_integral_value(Decimal('-Inf'))
        Decimal('-Infinity')
        """
        a = _convert_other(a, raiseit=True)
        py_ass_parser1824 = a.to_integral_value(context=self)
        Sensor(1824)
        return py_ass_parser1824
    to_integral = to_integral_value


class _WorkRep(object):
    __slots__ = 'sign', 'int', 'exp'

    def __init__(self, value=None):
        Sensor(1825)
        if value is None:
            Sensor(1826)
            self.sign = None
            self.int = 0
            self.exp = None
        else:
            Sensor(1827)
            if isinstance(value, Decimal):
                Sensor(1828)
                self.sign = value._sign
                self.int = int(value._int)
                self.exp = value._exp
            else:
                Sensor(1829)
                self.sign = value[0]
                self.int = value[1]
                self.exp = value[2]
        Sensor(1830)

    def __repr__(self):
        py_ass_parser1831 = '(%r, %r, %r)' % (self.sign, self.int, self.exp)
        Sensor(1831)
        return py_ass_parser1831
    __str__ = __repr__


def _normalize(op1, op2, prec=0):
    Sensor(1832)
    """Normalizes op1, op2 to have the same exp and length of coefficient.

    Done during addition.
    """
    Sensor(1833)
    if op1.exp < op2.exp:
        Sensor(1834)
        tmp = op2
        other = op1
    else:
        Sensor(1835)
        tmp = op1
        other = op2
    Sensor(1836)
    tmp_len = len(str(tmp.int))
    other_len = len(str(other.int))
    exp = tmp.exp + min(-1, tmp_len - prec - 2)
    Sensor(1837)
    if other_len + other.exp - 1 < exp:
        Sensor(1838)
        other.int = 1
        other.exp = exp
    Sensor(1839)
    tmp.int *= 10 ** (tmp.exp - other.exp)
    tmp.exp = other.exp
    py_ass_parser1840 = op1, op2
    Sensor(1840)
    return py_ass_parser1840


def _nbits(n, correction={'0': 4, '1': 3, '2': 2, '3': 2, '4': 1, '5': 1,
    '6': 1, '7': 1, '8': 0, '9': 0, 'a': 0, 'b': 0, 'c': 0, 'd': 0, 'e': 0,
    'f': 0}):
    Sensor(1841)
    """Number of bits in binary representation of the positive integer n,
    or 0 if n == 0.
    """
    Sensor(1842)
    if n < 0:
        Sensor(1843)
        raise ValueError('The argument to _nbits should be nonnegative.')
    Sensor(1844)
    hex_n = '%x' % n
    py_ass_parser1845 = 4 * len(hex_n) - correction[hex_n[0]]
    Sensor(1845)
    return py_ass_parser1845


def _decimal_lshift_exact(n, e):
    Sensor(1846)
    """ Given integers n and e, return n * 10**e if it's an integer, else None.

    The computation is designed to avoid computing large powers of 10
    unnecessarily.

    >>> _decimal_lshift_exact(3, 4)
    30000
    >>> _decimal_lshift_exact(300, -999999999)  # returns None

    """
    Sensor(1847)
    if n == 0:
        py_ass_parser1848 = 0
        Sensor(1848)
        return py_ass_parser1848
    else:
        Sensor(1849)
        if e >= 0:
            py_ass_parser1850 = n * 10 ** e
            Sensor(1850)
            return py_ass_parser1850
        else:
            Sensor(1851)
            str_n = str(abs(n))
            val_n = len(str_n) - len(str_n.rstrip('0'))
            py_ass_parser1852 = None if val_n < -e else n // 10 ** -e
            Sensor(1852)
            return py_ass_parser1852
    Sensor(1853)


def _sqrt_nearest(n, a):
    Sensor(1854)
    """Closest integer to the square root of the positive integer n.  a is
    an initial approximation to the square root.  Any positive integer
    will do for a, but the closer a is to the square root of n the
    faster convergence will be.

    """
    Sensor(1855)
    if n <= 0 or a <= 0:
        Sensor(1856)
        raise ValueError('Both arguments to _sqrt_nearest should be positive.')
    Sensor(1857)
    b = 0
    Sensor(1858)
    while a != b:
        Sensor(1859)
        b, a = a, a - -n // a >> 1
    py_ass_parser1860 = a
    Sensor(1860)
    return py_ass_parser1860


def _rshift_nearest(x, shift):
    Sensor(1861)
    """Given an integer x and a nonnegative integer shift, return closest
    integer to x / 2**shift; use round-to-even in case of a tie.

    """
    b, q = 1L << shift, x >> shift
    py_ass_parser1862 = q + (2 * (x & b - 1) + (q & 1) > b)
    Sensor(1862)
    return py_ass_parser1862


def _div_nearest(a, b):
    Sensor(1863)
    """Closest integer to a/b, a and b positive integers; rounds to even
    in the case of a tie.

    """
    q, r = divmod(a, b)
    py_ass_parser1864 = q + (2 * r + (q & 1) > b)
    Sensor(1864)
    return py_ass_parser1864


def _ilog(x, M, L=8):
    Sensor(1865)
    """Integer approximation to M*log(x/M), with absolute error boundable
    in terms only of x/M.

    Given positive integers x and M, return an integer approximation to
    M * log(x/M).  For L = 8 and 0.1 <= x/M <= 10 the difference
    between the approximation and the exact result is at most 22.  For
    L = 8 and 1.0 <= x/M <= 10.0 the difference is at most 15.  In
    both cases these are upper bounds on the error; it will usually be
    much smaller."""
    y = x - M
    R = 0
    Sensor(1866)
    while R <= L and long(abs(y)) << L - R >= M or R > L and abs(y
        ) >> R - L >= M:
        Sensor(1867)
        y = _div_nearest(long(M * y) << 1, M + _sqrt_nearest(M * (M +
            _rshift_nearest(y, R)), M))
        R += 1
    Sensor(1868)
    T = -int(-10 * len(str(M)) // (3 * L))
    yshift = _rshift_nearest(y, R)
    w = _div_nearest(M, T)
    Sensor(1869)
    for k in xrange(T - 1, 0, -1):
        Sensor(1870)
        w = _div_nearest(M, k) - _div_nearest(yshift * w, M)
    py_ass_parser1871 = _div_nearest(w * y, M)
    Sensor(1871)
    return py_ass_parser1871


def _dlog10(c, e, p):
    Sensor(1872)
    """Given integers c, e and p with c > 0, p >= 0, compute an integer
    approximation to 10**p * log10(c*10**e), with an absolute error of
    at most 1.  Assumes that c*10**e is not exactly 1."""
    p += 2
    l = len(str(c))
    f = e + l - (e + l >= 1)
    Sensor(1873)
    if p > 0:
        Sensor(1874)
        M = 10 ** p
        k = e + p - f
        Sensor(1875)
        if k >= 0:
            Sensor(1876)
            c *= 10 ** k
        else:
            Sensor(1877)
            c = _div_nearest(c, 10 ** -k)
        Sensor(1878)
        log_d = _ilog(c, M)
        log_10 = _log10_digits(p)
        log_d = _div_nearest(log_d * M, log_10)
        log_tenpower = f * M
    else:
        Sensor(1879)
        log_d = 0
        log_tenpower = _div_nearest(f, 10 ** -p)
    py_ass_parser1880 = _div_nearest(log_tenpower + log_d, 100)
    Sensor(1880)
    return py_ass_parser1880


def _dlog(c, e, p):
    Sensor(1881)
    """Given integers c, e and p with c > 0, compute an integer
    approximation to 10**p * log(c*10**e), with an absolute error of
    at most 1.  Assumes that c*10**e is not exactly 1."""
    p += 2
    l = len(str(c))
    f = e + l - (e + l >= 1)
    Sensor(1882)
    if p > 0:
        Sensor(1883)
        k = e + p - f
        Sensor(1884)
        if k >= 0:
            Sensor(1885)
            c *= 10 ** k
        else:
            Sensor(1886)
            c = _div_nearest(c, 10 ** -k)
        Sensor(1887)
        log_d = _ilog(c, 10 ** p)
    else:
        Sensor(1888)
        log_d = 0
    Sensor(1889)
    if f:
        Sensor(1890)
        extra = len(str(abs(f))) - 1
        Sensor(1891)
        if p + extra >= 0:
            Sensor(1892)
            f_log_ten = _div_nearest(f * _log10_digits(p + extra), 10 ** extra)
        else:
            Sensor(1893)
            f_log_ten = 0
    else:
        Sensor(1894)
        f_log_ten = 0
    py_ass_parser1895 = _div_nearest(f_log_ten + log_d, 100)
    Sensor(1895)
    return py_ass_parser1895


class _Log10Memoize(object):
    """Class to compute, store, and allow retrieval of, digits of the
    constant log(10) = 2.302585....  This constant is needed by
    Decimal.ln, Decimal.log10, Decimal.exp and Decimal.__pow__."""

    def __init__(self):
        Sensor(1896)
        self.digits = '23025850929940456840179914546843642076011014886'
        Sensor(1897)

    def getdigits(self, p):
        Sensor(1898)
        """Given an integer p >= 0, return floor(10**p)*log(10).

        For example, self.getdigits(3) returns 2302.
        """
        Sensor(1899)
        if p < 0:
            Sensor(1900)
            raise ValueError('p should be nonnegative')
        Sensor(1901)
        if p >= len(self.digits):
            Sensor(1902)
            extra = 3
            Sensor(1903)
            while True:
                Sensor(1904)
                M = 10 ** (p + extra + 2)
                digits = str(_div_nearest(_ilog(10 * M, M), 100))
                Sensor(1905)
                if digits[-extra:] != '0' * extra:
                    Sensor(1906)
                    break
                Sensor(1907)
                extra += 3
            Sensor(1908)
            self.digits = digits.rstrip('0')[:-1]
        py_ass_parser1909 = int(self.digits[:p + 1])
        Sensor(1909)
        return py_ass_parser1909


_log10_digits = _Log10Memoize().getdigits


def _iexp(x, M, L=8):
    Sensor(1910)
    """Given integers x and M, M > 0, such that x/M is small in absolute
    value, compute an integer approximation to M*exp(x/M).  For 0 <=
    x/M <= 2.4, the absolute error in the result is bounded by 60 (and
    is usually much smaller)."""
    R = _nbits((long(x) << L) // M)
    T = -int(-10 * len(str(M)) // (3 * L))
    y = _div_nearest(x, T)
    Mshift = long(M) << R
    Sensor(1911)
    for i in xrange(T - 1, 0, -1):
        Sensor(1912)
        y = _div_nearest(x * (Mshift + y), Mshift * i)
    Sensor(1913)
    for k in xrange(R - 1, -1, -1):
        Sensor(1914)
        Mshift = long(M) << k + 2
        y = _div_nearest(y * (y + Mshift), Mshift)
    py_ass_parser1915 = M + y
    Sensor(1915)
    return py_ass_parser1915


def _dexp(c, e, p):
    Sensor(1916)
    """Compute an approximation to exp(c*10**e), with p decimal places of
    precision.

    Returns integers d, f such that:

      10**(p-1) <= d <= 10**p, and
      (d-1)*10**f < exp(c*10**e) < (d+1)*10**f

    In other words, d*10**f is an approximation to exp(c*10**e) with p
    digits of precision, and with an error in d of at most 1.  This is
    almost, but not quite, the same as the error being < 1ulp: when d
    = 10**(p-1) the error could be up to 10 ulp."""
    p += 2
    extra = max(0, e + len(str(c)) - 1)
    q = p + extra
    shift = e + q
    Sensor(1917)
    if shift >= 0:
        Sensor(1918)
        cshift = c * 10 ** shift
    else:
        Sensor(1919)
        cshift = c // 10 ** -shift
    Sensor(1920)
    quot, rem = divmod(cshift, _log10_digits(q))
    rem = _div_nearest(rem, 10 ** extra)
    py_ass_parser1921 = _div_nearest(_iexp(rem, 10 ** p), 1000), quot - p + 3
    Sensor(1921)
    return py_ass_parser1921


def _dpower(xc, xe, yc, ye, p):
    Sensor(1922)
    """Given integers xc, xe, yc and ye representing Decimals x = xc*10**xe and
    y = yc*10**ye, compute x**y.  Returns a pair of integers (c, e) such that:

      10**(p-1) <= c <= 10**p, and
      (c-1)*10**e < x**y < (c+1)*10**e

    in other words, c*10**e is an approximation to x**y with p digits
    of precision, and with an error in c of at most 1.  (This is
    almost, but not quite, the same as the error being < 1ulp: when c
    == 10**(p-1) we can only guarantee error < 10ulp.)

    We assume that: x is positive and not equal to 1, and y is nonzero.
    """
    b = len(str(abs(yc))) + ye
    lxc = _dlog(xc, xe, p + b + 1)
    shift = ye - b
    Sensor(1923)
    if shift >= 0:
        Sensor(1924)
        pc = lxc * yc * 10 ** shift
    else:
        Sensor(1925)
        pc = _div_nearest(lxc * yc, 10 ** -shift)
    Sensor(1926)
    if pc == 0:
        Sensor(1927)
        if (len(str(xc)) + xe >= 1) == (yc > 0):
            Sensor(1928)
            coeff, exp = 10 ** (p - 1) + 1, 1 - p
        else:
            Sensor(1929)
            coeff, exp = 10 ** p - 1, -p
    else:
        Sensor(1930)
        coeff, exp = _dexp(pc, -(p + 1), p + 1)
        coeff = _div_nearest(coeff, 10)
        exp += 1
    py_ass_parser1931 = coeff, exp
    Sensor(1931)
    return py_ass_parser1931


def _log10_lb(c, correction={'1': 100, '2': 70, '3': 53, '4': 40, '5': 31,
    '6': 23, '7': 16, '8': 10, '9': 5}):
    Sensor(1932)
    """Compute a lower bound for 100*log10(c) for a positive integer c."""
    Sensor(1933)
    if c <= 0:
        Sensor(1934)
        raise ValueError('The argument to _log10_lb should be nonnegative.')
    Sensor(1935)
    str_c = str(c)
    py_ass_parser1936 = 100 * len(str_c) - correction[str_c[0]]
    Sensor(1936)
    return py_ass_parser1936


def _convert_other(other, raiseit=False, allow_float=False):
    Sensor(1937)
    """Convert other to Decimal.

    Verifies that it's ok to use in an implicit construction.
    If allow_float is true, allow conversion from float;  this
    is used in the comparison methods (__eq__ and friends).

    """
    Sensor(1938)
    if isinstance(other, Decimal):
        py_ass_parser1939 = other
        Sensor(1939)
        return py_ass_parser1939
    Sensor(1940)
    if isinstance(other, (int, long)):
        py_ass_parser1941 = Decimal(other)
        Sensor(1941)
        return py_ass_parser1941
    Sensor(1942)
    if allow_float and isinstance(other, float):
        py_ass_parser1943 = Decimal.from_float(other)
        Sensor(1943)
        return py_ass_parser1943
    Sensor(1944)
    if raiseit:
        Sensor(1945)
        raise TypeError('Unable to convert %s to Decimal' % other)
    py_ass_parser1946 = NotImplemented
    Sensor(1946)
    return py_ass_parser1946


DefaultContext = Context(prec=28, rounding=ROUND_HALF_EVEN, traps=[
    DivisionByZero, Overflow, InvalidOperation], flags=[], Emax=999999999,
    Emin=-999999999, capitals=1)
BasicContext = Context(prec=9, rounding=ROUND_HALF_UP, traps=[
    DivisionByZero, Overflow, InvalidOperation, Clamped, Underflow], flags=[])
ExtendedContext = Context(prec=9, rounding=ROUND_HALF_EVEN, traps=[], flags=[])
import re
_parser = re.compile(
    """        # A numeric string consists of:
#    \\s*
    (?P<sign>[-+])?              # an optional sign, followed by either...
    (
        (?=\\d|\\.\\d)              # ...a number (with at least one digit)
        (?P<int>\\d*)             # having a (possibly empty) integer part
        (\\.(?P<frac>\\d*))?       # followed by an optional fractional part
        (E(?P<exp>[-+]?\\d+))?    # followed by an optional exponent, or...
    |
        Inf(inity)?              # ...an infinity, or...
    |
        (?P<signal>s)?           # ...an (optionally signaling)
        NaN                      # NaN
        (?P<diag>\\d*)            # with (possibly empty) diagnostic info.
    )
#    \\s*
    \\Z
"""
    , re.VERBOSE | re.IGNORECASE | re.UNICODE).match
_all_zeros = re.compile('0*$').match
_exact_half = re.compile('50*$').match
_parse_format_specifier_regex = re.compile(
    """\\A
(?:
   (?P<fill>.)?
   (?P<align>[<>=^])
)?
(?P<sign>[-+ ])?
(?P<zeropad>0)?
(?P<minimumwidth>(?!0)\\d+)?
(?P<thousands_sep>,)?
(?:\\.(?P<precision>0|(?!0)\\d+))?
(?P<type>[eEfFgGn%])?
\\Z
"""
    , re.VERBOSE)
del re
Sensor(1947)
try:
    Sensor(1948)
    import locale as _locale
except ImportError:
    Sensor(1949)
    pass


def _parse_format_specifier(format_spec, _localeconv=None):
    Sensor(1950)
    """Parse and validate a format specifier.

    Turns a standard numeric format specifier into a dict, with the
    following entries:

      fill: fill character to pad field to minimum width
      align: alignment type, either '<', '>', '=' or '^'
      sign: either '+', '-' or ' '
      minimumwidth: nonnegative integer giving minimum width
      zeropad: boolean, indicating whether to pad with zeros
      thousands_sep: string to use as thousands separator, or ''
      grouping: grouping for thousands separators, in format
        used by localeconv
      decimal_point: string to use for decimal point
      precision: nonnegative integer giving precision, or None
      type: one of the characters 'eEfFgG%', or None
      unicode: boolean (always True for Python 3.x)

    """
    m = _parse_format_specifier_regex.match(format_spec)
    Sensor(1951)
    if m is None:
        Sensor(1952)
        raise ValueError('Invalid format specifier: ' + format_spec)
    Sensor(1953)
    format_dict = m.groupdict()
    fill = format_dict['fill']
    align = format_dict['align']
    format_dict['zeropad'] = format_dict['zeropad'] is not None
    Sensor(1954)
    if format_dict['zeropad']:
        Sensor(1955)
        if fill is not None:
            Sensor(1956)
            raise ValueError(
                "Fill character conflicts with '0' in format specifier: " +
                format_spec)
        Sensor(1957)
        if align is not None:
            Sensor(1958)
            raise ValueError(
                "Alignment conflicts with '0' in format specifier: " +
                format_spec)
    Sensor(1959)
    format_dict['fill'] = fill or ' '
    format_dict['align'] = align or '>'
    Sensor(1960)
    if format_dict['sign'] is None:
        Sensor(1961)
        format_dict['sign'] = '-'
    Sensor(1962)
    format_dict['minimumwidth'] = int(format_dict['minimumwidth'] or '0')
    Sensor(1963)
    if format_dict['precision'] is not None:
        Sensor(1964)
        format_dict['precision'] = int(format_dict['precision'])
    Sensor(1965)
    if format_dict['precision'] == 0:
        Sensor(1966)
        if format_dict['type'] is None or format_dict['type'] in 'gG':
            Sensor(1967)
            format_dict['precision'] = 1
    Sensor(1968)
    if format_dict['type'] == 'n':
        Sensor(1969)
        format_dict['type'] = 'g'
        Sensor(1970)
        if _localeconv is None:
            Sensor(1971)
            _localeconv = _locale.localeconv()
        Sensor(1972)
        if format_dict['thousands_sep'] is not None:
            Sensor(1973)
            raise ValueError(
                "Explicit thousands separator conflicts with 'n' type in format specifier: "
                 + format_spec)
        Sensor(1974)
        format_dict['thousands_sep'] = _localeconv['thousands_sep']
        format_dict['grouping'] = _localeconv['grouping']
        format_dict['decimal_point'] = _localeconv['decimal_point']
    else:
        Sensor(1975)
        if format_dict['thousands_sep'] is None:
            Sensor(1976)
            format_dict['thousands_sep'] = ''
        Sensor(1977)
        format_dict['grouping'] = [3, 0]
        format_dict['decimal_point'] = '.'
    Sensor(1978)
    format_dict['unicode'] = isinstance(format_spec, unicode)
    py_ass_parser1979 = format_dict
    Sensor(1979)
    return py_ass_parser1979


def _format_align(sign, body, spec):
    Sensor(1980)
    """Given an unpadded, non-aligned numeric string 'body' and sign
    string 'sign', add padding and alignment conforming to the given
    format specifier dictionary 'spec' (as produced by
    parse_format_specifier).

    Also converts result to unicode if necessary.

    """
    minimumwidth = spec['minimumwidth']
    fill = spec['fill']
    padding = fill * (minimumwidth - len(sign) - len(body))
    align = spec['align']
    Sensor(1981)
    if align == '<':
        Sensor(1982)
        result = sign + body + padding
    else:
        Sensor(1983)
        if align == '>':
            Sensor(1984)
            result = padding + sign + body
        else:
            Sensor(1985)
            if align == '=':
                Sensor(1986)
                result = sign + padding + body
            else:
                Sensor(1987)
                if align == '^':
                    Sensor(1988)
                    half = len(padding) // 2
                    result = padding[:half] + sign + body + padding[half:]
                else:
                    Sensor(1989)
                    raise ValueError('Unrecognised alignment field')
    Sensor(1990)
    if spec['unicode']:
        Sensor(1991)
        result = unicode(result)
    py_ass_parser1992 = result
    Sensor(1992)
    return py_ass_parser1992


def _group_lengths(grouping):
    Sensor(1993)
    """Convert a localeconv-style grouping into a (possibly infinite)
    iterable of integers representing group lengths.

    """
    from itertools import chain, repeat
    Sensor(1994)
    if not grouping:
        py_ass_parser1995 = []
        Sensor(1995)
        return py_ass_parser1995
    else:
        Sensor(1996)
        if grouping[-1] == 0 and len(grouping) >= 2:
            py_ass_parser1997 = chain(grouping[:-1], repeat(grouping[-2]))
            Sensor(1997)
            return py_ass_parser1997
        else:
            Sensor(1998)
            if grouping[-1] == _locale.CHAR_MAX:
                py_ass_parser1999 = grouping[:-1]
                Sensor(1999)
                return py_ass_parser1999
            else:
                Sensor(2000)
                raise ValueError('unrecognised format for grouping')
    Sensor(2001)


def _insert_thousands_sep(digits, spec, min_width=1):
    Sensor(2002)
    """Insert thousands separators into a digit string.

    spec is a dictionary whose keys should include 'thousands_sep' and
    'grouping'; typically it's the result of parsing the format
    specifier using _parse_format_specifier.

    The min_width keyword argument gives the minimum length of the
    result, which will be padded on the left with zeros if necessary.

    If necessary, the zero padding adds an extra '0' on the left to
    avoid a leading thousands separator.  For example, inserting
    commas every three digits in '123456', with min_width=8, gives
    '0,123,456', even though that has length 9.

    """
    sep = spec['thousands_sep']
    grouping = spec['grouping']
    groups = []
    Sensor(2003)
    for l in _group_lengths(grouping):
        Sensor(2004)
        if l <= 0:
            Sensor(2005)
            raise ValueError('group length should be positive')
        Sensor(2006)
        l = min(max(len(digits), min_width, 1), l)
        groups.append('0' * (l - len(digits)) + digits[-l:])
        digits = digits[:-l]
        min_width -= l
        Sensor(2007)
        if not digits and min_width <= 0:
            Sensor(2008)
            break
        Sensor(2009)
        min_width -= len(sep)
    else:
        Sensor(2010)
        l = max(len(digits), min_width, 1)
        groups.append('0' * (l - len(digits)) + digits[-l:])
    py_ass_parser2011 = sep.join(reversed(groups))
    Sensor(2011)
    return py_ass_parser2011


def _format_sign(is_negative, spec):
    Sensor(2012)
    """Determine sign character."""
    Sensor(2013)
    if is_negative:
        py_ass_parser2014 = '-'
        Sensor(2014)
        return py_ass_parser2014
    else:
        Sensor(2015)
        if spec['sign'] in ' +':
            py_ass_parser2016 = spec['sign']
            Sensor(2016)
            return py_ass_parser2016
        else:
            py_ass_parser2017 = ''
            Sensor(2017)
            return py_ass_parser2017
    Sensor(2018)


def _format_number(is_negative, intpart, fracpart, exp, spec):
    Sensor(2019)
    """Format a number, given the following data:

    is_negative: true if the number is negative, else false
    intpart: string of digits that must appear before the decimal point
    fracpart: string of digits that must come after the point
    exp: exponent, as an integer
    spec: dictionary resulting from parsing the format specifier

    This function uses the information in spec to:
      insert separators (decimal separator and thousands separators)
      format the sign
      format the exponent
      add trailing '%' for the '%' type
      zero-pad if necessary
      fill and align if necessary
    """
    sign = _format_sign(is_negative, spec)
    Sensor(2020)
    if fracpart:
        Sensor(2021)
        fracpart = spec['decimal_point'] + fracpart
    Sensor(2022)
    if exp != 0 or spec['type'] in 'eE':
        Sensor(2023)
        echar = {'E': 'E', 'e': 'e', 'G': 'E', 'g': 'e'}[spec['type']]
        fracpart += '{0}{1:+}'.format(echar, exp)
    Sensor(2024)
    if spec['type'] == '%':
        Sensor(2025)
        fracpart += '%'
    Sensor(2026)
    if spec['zeropad']:
        Sensor(2027)
        min_width = spec['minimumwidth'] - len(fracpart) - len(sign)
    else:
        Sensor(2028)
        min_width = 0
    Sensor(2029)
    intpart = _insert_thousands_sep(intpart, spec, min_width)
    py_ass_parser2030 = _format_align(sign, intpart + fracpart, spec)
    Sensor(2030)
    return py_ass_parser2030


Sensor(2031)
_Infinity = Decimal('Inf')
_NegativeInfinity = Decimal('-Inf')
_NaN = Decimal('NaN')
_Zero = Decimal(0)
_One = Decimal(1)
_NegativeOne = Decimal(-1)
_SignedInfinity = _Infinity, _NegativeInfinity
Sensor(2032)
if __name__ == '__main__':
    import doctest, sys
    Sensor(2033)
    doctest.testmod(sys.modules[__name__])
Sensor(2034)
