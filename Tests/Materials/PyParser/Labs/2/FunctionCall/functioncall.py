from sensors import Sensor


Sensor(5)


def func1():
    Sensor(1)
    return


def func2(a):
    Sensor(2)
    return


def func3():
    Sensor(3)
    return


Sensor(4)
func2(func1())
func1()
Sensor(6)
