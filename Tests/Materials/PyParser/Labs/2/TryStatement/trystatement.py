from sensors import Sensor


Sensor(12)


def test():
    Sensor(1)
    a = 0 / 0
    Sensor(2)


Sensor(3)
try:
    Sensor(4)
    test()
finally:
    Sensor(5)
    test()
try:
    Sensor(6)
    try:
        Sensor(7)
        test()
    except ValueError:
        Sensor(8)
        test()
    except ValueError:
        Sensor(9)
        test()
    else:
        Sensor(10)
        test()
finally:
    Sensor(11)
    test()
Sensor(13)
