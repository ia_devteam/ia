using System;
using Interop = System.Runtime.InteropServices;
namespace Ionic.Crc
{
  [Interop.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d0000C")]
  [Interop.ComVisible(true)]
  [Interop.ClassInterface(Interop.ClassInterfaceType.AutoDispatch)]
  public class CRC32
  {
    public Int64 TotalBytesRead {
      get {
        Int64 RNTRNTRNT_737 = _TotalBytesRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6450uL);
        return RNTRNTRNT_737;
      }
    }
    public Int32 Crc32Result {
      get {
        Int32 RNTRNTRNT_738 = unchecked((Int32)(~_register));
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6451uL);
        return RNTRNTRNT_738;
      }
    }
    public Int32 GetCrc32(System.IO.Stream input)
    {
      Int32 RNTRNTRNT_739 = GetCrc32AndCopy(input, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6452uL);
      return RNTRNTRNT_739;
    }
    public Int32 GetCrc32AndCopy(System.IO.Stream input, System.IO.Stream output)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6453uL);
      if (input == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6454uL);
        throw new Exception("The input stream must not be null.");
      }
      unchecked {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6455uL);
        byte[] buffer = new byte[BUFFER_SIZE];
        int readSize = BUFFER_SIZE;
        _TotalBytesRead = 0;
        int count = input.Read(buffer, 0, readSize);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6456uL);
        if (output != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6457uL);
          output.Write(buffer, 0, count);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6458uL);
        _TotalBytesRead += count;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6459uL);
        while (count > 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6460uL);
          SlurpBlock(buffer, 0, count);
          count = input.Read(buffer, 0, readSize);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6461uL);
          if (output != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6462uL);
            output.Write(buffer, 0, count);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6463uL);
          _TotalBytesRead += count;
        }
        Int32 RNTRNTRNT_740 = (Int32)(~_register);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6464uL);
        return RNTRNTRNT_740;
      }
    }
    public Int32 ComputeCrc32(Int32 W, byte B)
    {
      Int32 RNTRNTRNT_741 = _InternalComputeCrc32((UInt32)W, B);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6465uL);
      return RNTRNTRNT_741;
    }
    internal Int32 _InternalComputeCrc32(UInt32 W, byte B)
    {
      Int32 RNTRNTRNT_742 = (Int32)(crc32Table[(W ^ B) & 0xff] ^ (W >> 8));
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6466uL);
      return RNTRNTRNT_742;
    }
    public void SlurpBlock(byte[] block, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6467uL);
      if (block == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6468uL);
        throw new Exception("The data buffer must not be null.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6469uL);
      for (int i = 0; i < count; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6470uL);
        int x = offset + i;
        byte b = block[x];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6471uL);
        if (this.reverseBits) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6472uL);
          UInt32 temp = (_register >> 24) ^ b;
          _register = (_register << 8) ^ crc32Table[temp];
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6473uL);
          UInt32 temp = (_register & 0xff) ^ b;
          _register = (_register >> 8) ^ crc32Table[temp];
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6474uL);
      _TotalBytesRead += count;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6475uL);
    }
    public void UpdateCRC(byte b)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6476uL);
      if (this.reverseBits) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6477uL);
        UInt32 temp = (_register >> 24) ^ b;
        _register = (_register << 8) ^ crc32Table[temp];
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6478uL);
        UInt32 temp = (_register & 0xff) ^ b;
        _register = (_register >> 8) ^ crc32Table[temp];
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6479uL);
    }
    public void UpdateCRC(byte b, int n)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6480uL);
      while (n-- > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6481uL);
        if (this.reverseBits) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6482uL);
          uint temp = (_register >> 24) ^ b;
          _register = (_register << 8) ^ crc32Table[(temp >= 0) ? temp : (temp + 256)];
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6483uL);
          UInt32 temp = (_register & 0xff) ^ b;
          _register = (_register >> 8) ^ crc32Table[(temp >= 0) ? temp : (temp + 256)];
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6484uL);
    }
    private static uint ReverseBits(uint data)
    {
      unchecked {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6485uL);
        uint ret = data;
        ret = (ret & 0x55555555) << 1 | (ret >> 1) & 0x55555555;
        ret = (ret & 0x33333333) << 2 | (ret >> 2) & 0x33333333;
        ret = (ret & 0xf0f0f0f) << 4 | (ret >> 4) & 0xf0f0f0f;
        ret = (ret << 24) | ((ret & 0xff00) << 8) | ((ret >> 8) & 0xff00) | (ret >> 24);
        System.UInt32 RNTRNTRNT_743 = ret;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6486uL);
        return RNTRNTRNT_743;
      }
    }
    private static byte ReverseBits(byte data)
    {
      unchecked {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6487uL);
        uint u = (uint)data * 0x20202;
        uint m = 0x1044010;
        uint s = u & m;
        uint t = (u << 2) & (m << 1);
        System.Byte RNTRNTRNT_744 = (byte)((0x1001001 * (s + t)) >> 24);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6488uL);
        return RNTRNTRNT_744;
      }
    }
    private void GenerateLookupTable()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6489uL);
      crc32Table = new UInt32[256];
      unchecked {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6490uL);
        UInt32 dwCrc;
        byte i = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6491uL);
        do {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6492uL);
          dwCrc = i;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6493uL);
          for (byte j = 8; j > 0; j--) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6494uL);
            if ((dwCrc & 1) == 1) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6495uL);
              dwCrc = (dwCrc >> 1) ^ dwPolynomial;
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6496uL);
              dwCrc >>= 1;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6497uL);
          if (reverseBits) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6498uL);
            crc32Table[ReverseBits(i)] = ReverseBits(dwCrc);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6499uL);
            crc32Table[i] = dwCrc;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6500uL);
          i++;
        } while (i != 0);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6501uL);
    }
    private uint gf2_matrix_times(uint[] matrix, uint vec)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6502uL);
      uint sum = 0;
      int i = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6503uL);
      while (vec != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6504uL);
        if ((vec & 0x1) == 0x1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6505uL);
          sum ^= matrix[i];
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6506uL);
        vec >>= 1;
        i++;
      }
      System.UInt32 RNTRNTRNT_745 = sum;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6507uL);
      return RNTRNTRNT_745;
    }
    private void gf2_matrix_square(uint[] square, uint[] mat)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6508uL);
      for (int i = 0; i < 32; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6509uL);
        square[i] = gf2_matrix_times(mat, mat[i]);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6510uL);
    }
    public void Combine(int crc, int length)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6511uL);
      uint[] even = new uint[32];
      uint[] odd = new uint[32];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6512uL);
      if (length == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6513uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6514uL);
      uint crc1 = ~_register;
      uint crc2 = (uint)crc;
      odd[0] = this.dwPolynomial;
      uint row = 1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6515uL);
      for (int i = 1; i < 32; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6516uL);
        odd[i] = row;
        row <<= 1;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6517uL);
      gf2_matrix_square(even, odd);
      gf2_matrix_square(odd, even);
      uint len2 = (uint)length;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6518uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6519uL);
        gf2_matrix_square(even, odd);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6520uL);
        if ((len2 & 1) == 1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6521uL);
          crc1 = gf2_matrix_times(even, crc1);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6522uL);
        len2 >>= 1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6523uL);
        if (len2 == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6524uL);
          break;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6525uL);
        gf2_matrix_square(odd, even);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6526uL);
        if ((len2 & 1) == 1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6527uL);
          crc1 = gf2_matrix_times(odd, crc1);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6528uL);
        len2 >>= 1;
      } while (len2 != 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6529uL);
      crc1 ^= crc2;
      _register = ~crc1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6530uL);
      return;
    }
    public CRC32() : this(false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6531uL);
    }
    public CRC32(bool reverseBits) : this(unchecked((int)0xedb88320u), reverseBits)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6532uL);
    }
    public CRC32(int polynomial, bool reverseBits)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6533uL);
      this.reverseBits = reverseBits;
      this.dwPolynomial = (uint)polynomial;
      this.GenerateLookupTable();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6534uL);
    }
    public void Reset()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6535uL);
      _register = 0xffffffffu;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6536uL);
    }
    private UInt32 dwPolynomial;
    private Int64 _TotalBytesRead;
    private bool reverseBits;
    private UInt32[] crc32Table;
    private const int BUFFER_SIZE = 8192;
    private UInt32 _register = 0xffffffffu;
  }
  public class CrcCalculatorStream : System.IO.Stream, System.IDisposable
  {
    private static readonly Int64 UnsetLengthLimit = -99;
    internal System.IO.Stream _innerStream;
    private CRC32 _Crc32;
    private Int64 _lengthLimit = -99;
    private bool _leaveOpen;
    public CrcCalculatorStream(System.IO.Stream stream) : this(true, CrcCalculatorStream.UnsetLengthLimit, stream, null)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6537uL);
    }
    public CrcCalculatorStream(System.IO.Stream stream, bool leaveOpen) : this(leaveOpen, CrcCalculatorStream.UnsetLengthLimit, stream, null)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6538uL);
    }
    public CrcCalculatorStream(System.IO.Stream stream, Int64 length) : this(true, length, stream, null)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6539uL);
      if (length < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6540uL);
        throw new ArgumentException("length");
      }
    }
    public CrcCalculatorStream(System.IO.Stream stream, Int64 length, bool leaveOpen) : this(leaveOpen, length, stream, null)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6541uL);
      if (length < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6542uL);
        throw new ArgumentException("length");
      }
    }
    public CrcCalculatorStream(System.IO.Stream stream, Int64 length, bool leaveOpen, CRC32 crc32) : this(leaveOpen, length, stream, crc32)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6543uL);
      if (length < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6544uL);
        throw new ArgumentException("length");
      }
    }
    private CrcCalculatorStream(bool leaveOpen, Int64 length, System.IO.Stream stream, CRC32 crc32) : base()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6545uL);
      _innerStream = stream;
      _Crc32 = crc32 ?? new CRC32();
      _lengthLimit = length;
      _leaveOpen = leaveOpen;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6546uL);
    }
    public Int64 TotalBytesSlurped {
      get {
        Int64 RNTRNTRNT_746 = _Crc32.TotalBytesRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6547uL);
        return RNTRNTRNT_746;
      }
    }
    public Int32 Crc {
      get {
        Int32 RNTRNTRNT_747 = _Crc32.Crc32Result;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6548uL);
        return RNTRNTRNT_747;
      }
    }
    public bool LeaveOpen {
      get {
        System.Boolean RNTRNTRNT_748 = _leaveOpen;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6549uL);
        return RNTRNTRNT_748;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6550uL);
        _leaveOpen = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6551uL);
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6552uL);
      int bytesToRead = count;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6553uL);
      if (_lengthLimit != CrcCalculatorStream.UnsetLengthLimit) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6554uL);
        if (_Crc32.TotalBytesRead >= _lengthLimit) {
          System.Int32 RNTRNTRNT_749 = 0;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6555uL);
          return RNTRNTRNT_749;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6556uL);
        Int64 bytesRemaining = _lengthLimit - _Crc32.TotalBytesRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6557uL);
        if (bytesRemaining < count) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6558uL);
          bytesToRead = (int)bytesRemaining;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6559uL);
      int n = _innerStream.Read(buffer, offset, bytesToRead);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6560uL);
      if (n > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6561uL);
        _Crc32.SlurpBlock(buffer, offset, n);
      }
      System.Int32 RNTRNTRNT_750 = n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6562uL);
      return RNTRNTRNT_750;
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6563uL);
      if (count > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6564uL);
        _Crc32.SlurpBlock(buffer, offset, count);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6565uL);
      _innerStream.Write(buffer, offset, count);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6566uL);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_751 = _innerStream.CanRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6567uL);
        return RNTRNTRNT_751;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_752 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6568uL);
        return RNTRNTRNT_752;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_753 = _innerStream.CanWrite;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6569uL);
        return RNTRNTRNT_753;
      }
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6570uL);
      _innerStream.Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6571uL);
    }
    public override long Length {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6572uL);
        if (_lengthLimit == CrcCalculatorStream.UnsetLengthLimit) {
          System.Int64 RNTRNTRNT_754 = _innerStream.Length;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6573uL);
          return RNTRNTRNT_754;
        } else {
          System.Int64 RNTRNTRNT_755 = _lengthLimit;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6574uL);
          return RNTRNTRNT_755;
        }
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_756 = _Crc32.TotalBytesRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6575uL);
        return RNTRNTRNT_756;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6576uL);
        throw new NotSupportedException();
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6577uL);
      throw new NotSupportedException();
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6578uL);
      throw new NotSupportedException();
    }
    void IDisposable.Dispose()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6579uL);
      Close();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6580uL);
    }
    public override void Close()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6581uL);
      base.Close();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6582uL);
      if (!_leaveOpen) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6583uL);
        _innerStream.Close();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6584uL);
    }
  }
}
