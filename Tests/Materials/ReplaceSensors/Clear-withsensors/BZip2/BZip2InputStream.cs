using System;
using System.IO;
namespace Ionic.BZip2
{
  public class BZip2InputStream : System.IO.Stream
  {
    bool _disposed;
    bool _leaveOpen;
    Int64 totalBytesRead;
    private int last;
    private int origPtr;
    private int blockSize100k;
    private bool blockRandomised;
    private int bsBuff;
    private int bsLive;
    private readonly Ionic.Crc.CRC32 crc = new Ionic.Crc.CRC32(true);
    private int nInUse;
    private Stream input;
    private int currentChar = -1;
    enum CState
    {
      EOF = 0,
      START_BLOCK = 1,
      RAND_PART_A = 2,
      RAND_PART_B = 3,
      RAND_PART_C = 4,
      NO_RAND_PART_A = 5,
      NO_RAND_PART_B = 6,
      NO_RAND_PART_C = 7
    }
    private CState currentState = CState.START_BLOCK;
    private uint storedBlockCRC, storedCombinedCRC;
    private uint computedBlockCRC, computedCombinedCRC;
    private int su_count;
    private int su_ch2;
    private int su_chPrev;
    private int su_i2;
    private int su_j2;
    private int su_rNToGo;
    private int su_rTPos;
    private int su_tPos;
    private char su_z;
    private BZip2InputStream.DecompressionState data;
    public BZip2InputStream(Stream input) : this(input, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4086uL);
    }
    public BZip2InputStream(Stream input, bool leaveOpen) : base()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4087uL);
      this.input = input;
      this._leaveOpen = leaveOpen;
      init();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4088uL);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4089uL);
      if (offset < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4090uL);
        throw new IndexOutOfRangeException(String.Format("offset ({0}) must be > 0", offset));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4091uL);
      if (count < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4092uL);
        throw new IndexOutOfRangeException(String.Format("count ({0}) must be > 0", count));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4093uL);
      if (offset + count > buffer.Length) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4094uL);
        throw new IndexOutOfRangeException(String.Format("offset({0}) count({1}) bLength({2})", offset, count, buffer.Length));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4095uL);
      if (this.input == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4096uL);
        throw new IOException("the stream is not open");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4097uL);
      int hi = offset + count;
      int destOffset = offset;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4098uL);
      for (int b; (destOffset < hi) && ((b = ReadByte()) >= 0);) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4099uL);
        buffer[destOffset++] = (byte)b;
      }
      System.Int32 RNTRNTRNT_486 = (destOffset == offset) ? -1 : (destOffset - offset);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4100uL);
      return RNTRNTRNT_486;
    }
    private void MakeMaps()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4101uL);
      bool[] inUse = this.data.inUse;
      byte[] seqToUnseq = this.data.seqToUnseq;
      int n = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4102uL);
      for (int i = 0; i < 256; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4103uL);
        if (inUse[i]) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4104uL);
          seqToUnseq[n++] = (byte)i;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4105uL);
      this.nInUse = n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4106uL);
    }
    public override int ReadByte()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4107uL);
      int retChar = this.currentChar;
      totalBytesRead++;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4108uL);
      switch (this.currentState) {
        case CState.EOF:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4110uL);
          
          {
            System.Int32 RNTRNTRNT_487 = -1;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4109uL);
            return RNTRNTRNT_487;
          }

        case CState.START_BLOCK:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4112uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4111uL);
            throw new IOException("bad state");
          }

        case CState.RAND_PART_A:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4114uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4113uL);
            throw new IOException("bad state");
          }

        case CState.RAND_PART_B:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4117uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4115uL);
            SetupRandPartB();
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4116uL);
            break;
          }

        case CState.RAND_PART_C:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4120uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4118uL);
            SetupRandPartC();
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4119uL);
            break;
          }

        case CState.NO_RAND_PART_A:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4122uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4121uL);
            throw new IOException("bad state");
          }

        case CState.NO_RAND_PART_B:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4125uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4123uL);
            SetupNoRandPartB();
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4124uL);
            break;
          }

        case CState.NO_RAND_PART_C:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4128uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4126uL);
            SetupNoRandPartC();
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4127uL);
            break;
          }

        default:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4130uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4129uL);
            throw new IOException("bad state");
          }

      }
      System.Int32 RNTRNTRNT_488 = retChar;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4131uL);
      return RNTRNTRNT_488;
    }
    public override bool CanRead {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4132uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4133uL);
          throw new ObjectDisposedException("BZip2Stream");
        }
        System.Boolean RNTRNTRNT_489 = this.input.CanRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4134uL);
        return RNTRNTRNT_489;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_490 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4135uL);
        return RNTRNTRNT_490;
      }
    }
    public override bool CanWrite {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4136uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4137uL);
          throw new ObjectDisposedException("BZip2Stream");
        }
        System.Boolean RNTRNTRNT_491 = input.CanWrite;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4138uL);
        return RNTRNTRNT_491;
      }
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4139uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4140uL);
        throw new ObjectDisposedException("BZip2Stream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4141uL);
      input.Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4142uL);
    }
    public override long Length {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4143uL);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_492 = this.totalBytesRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4144uL);
        return RNTRNTRNT_492;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4145uL);
        throw new NotImplementedException();
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4146uL);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4147uL);
      throw new NotImplementedException();
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4148uL);
      throw new NotImplementedException();
    }
    protected override void Dispose(bool disposing)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4149uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4150uL);
        if (!_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4151uL);
          if (disposing && (this.input != null)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4152uL);
            this.input.Close();
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4153uL);
          _disposed = true;
        }
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4154uL);
        base.Dispose(disposing);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4155uL);
    }
    void init()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4156uL);
      if (null == this.input) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4157uL);
        throw new IOException("No input Stream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4158uL);
      if (!this.input.CanRead) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4159uL);
        throw new IOException("Unreadable input Stream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4160uL);
      CheckMagicChar('B', 0);
      CheckMagicChar('Z', 1);
      CheckMagicChar('h', 2);
      int blockSize = this.input.ReadByte();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4161uL);
      if ((blockSize < '1') || (blockSize > '9')) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4162uL);
        throw new IOException("Stream is not BZip2 formatted: illegal " + "blocksize " + (char)blockSize);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4163uL);
      this.blockSize100k = blockSize - '0';
      InitBlock();
      SetupBlock();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4164uL);
    }
    void CheckMagicChar(char expected, int position)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4165uL);
      int magic = this.input.ReadByte();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4166uL);
      if (magic != (int)expected) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4167uL);
        var msg = String.Format("Not a valid BZip2 stream. byte {0}, expected '{1}', got '{2}'", position, (int)expected, magic);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4168uL);
        throw new IOException(msg);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4169uL);
    }
    void InitBlock()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4170uL);
      char magic0 = bsGetUByte();
      char magic1 = bsGetUByte();
      char magic2 = bsGetUByte();
      char magic3 = bsGetUByte();
      char magic4 = bsGetUByte();
      char magic5 = bsGetUByte();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4171uL);
      if (magic0 == 0x17 && magic1 == 0x72 && magic2 == 0x45 && magic3 == 0x38 && magic4 == 0x50 && magic5 == 0x90) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4172uL);
        complete();
      } else if (magic0 != 0x31 || magic1 != 0x41 || magic2 != 0x59 || magic3 != 0x26 || magic4 != 0x53 || magic5 != 0x59) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4177uL);
        this.currentState = CState.EOF;
        var msg = String.Format("bad block header at offset 0x{0:X}", this.input.Position);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4178uL);
        throw new IOException(msg);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4173uL);
        this.storedBlockCRC = bsGetInt();
        this.blockRandomised = (GetBits(1) == 1);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4174uL);
        if (this.data == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4175uL);
          this.data = new DecompressionState(this.blockSize100k);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4176uL);
        getAndMoveToFrontDecode();
        this.crc.Reset();
        this.currentState = CState.START_BLOCK;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4179uL);
    }
    private void EndBlock()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4180uL);
      this.computedBlockCRC = (uint)this.crc.Crc32Result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4181uL);
      if (this.storedBlockCRC != this.computedBlockCRC) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4182uL);
        var msg = String.Format("BZip2 CRC error (expected {0:X8}, computed {1:X8})", this.storedBlockCRC, this.computedBlockCRC);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4183uL);
        throw new IOException(msg);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4184uL);
      this.computedCombinedCRC = (this.computedCombinedCRC << 1) | (this.computedCombinedCRC >> 31);
      this.computedCombinedCRC ^= this.computedBlockCRC;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4185uL);
    }
    private void complete()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4186uL);
      this.storedCombinedCRC = bsGetInt();
      this.currentState = CState.EOF;
      this.data = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4187uL);
      if (this.storedCombinedCRC != this.computedCombinedCRC) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4188uL);
        var msg = String.Format("BZip2 CRC error (expected {0:X8}, computed {1:X8})", this.storedCombinedCRC, this.computedCombinedCRC);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4189uL);
        throw new IOException(msg);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4190uL);
    }
    public override void Close()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4191uL);
      Stream inShadow = this.input;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4192uL);
      if (inShadow != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4193uL);
        try {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4194uL);
          if (!this._leaveOpen) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4195uL);
            inShadow.Close();
          }
        } finally {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4196uL);
          this.data = null;
          this.input = null;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4197uL);
    }
    private int GetBits(int n)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4198uL);
      int bsLiveShadow = this.bsLive;
      int bsBuffShadow = this.bsBuff;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4199uL);
      if (bsLiveShadow < n) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4200uL);
        do {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4201uL);
          int thech = this.input.ReadByte();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4202uL);
          if (thech < 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4203uL);
            throw new IOException("unexpected end of stream");
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4204uL);
          bsBuffShadow = (bsBuffShadow << 8) | thech;
          bsLiveShadow += 8;
        } while (bsLiveShadow < n);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4205uL);
        this.bsBuff = bsBuffShadow;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4206uL);
      this.bsLive = bsLiveShadow - n;
      System.Int32 RNTRNTRNT_493 = (bsBuffShadow >> (bsLiveShadow - n)) & ((1 << n) - 1);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4207uL);
      return RNTRNTRNT_493;
    }
    private bool bsGetBit()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4208uL);
      int bit = GetBits(1);
      System.Boolean RNTRNTRNT_494 = bit != 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4209uL);
      return RNTRNTRNT_494;
    }
    private char bsGetUByte()
    {
      System.Char RNTRNTRNT_495 = (char)GetBits(8);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4210uL);
      return RNTRNTRNT_495;
    }
    private uint bsGetInt()
    {
      System.UInt32 RNTRNTRNT_496 = (uint)((((((GetBits(8) << 8) | GetBits(8)) << 8) | GetBits(8)) << 8) | GetBits(8));
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4211uL);
      return RNTRNTRNT_496;
    }
    private static void hbCreateDecodeTables(int[] limit, int[] bbase, int[] perm, char[] length, int minLen, int maxLen, int alphaSize)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4212uL);
      for (int i = minLen, pp = 0; i <= maxLen; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4213uL);
        for (int j = 0; j < alphaSize; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4214uL);
          if (length[j] == i) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4215uL);
            perm[pp++] = j;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4216uL);
      for (int i = BZip2.MaxCodeLength; --i > 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4217uL);
        bbase[i] = 0;
        limit[i] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4218uL);
      for (int i = 0; i < alphaSize; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4219uL);
        bbase[length[i] + 1]++;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4220uL);
      for (int i = 1, b = bbase[0]; i < BZip2.MaxCodeLength; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4221uL);
        b += bbase[i];
        bbase[i] = b;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4222uL);
      for (int i = minLen, vec = 0, b = bbase[i]; i <= maxLen; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4223uL);
        int nb = bbase[i + 1];
        vec += nb - b;
        b = nb;
        limit[i] = vec - 1;
        vec <<= 1;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4224uL);
      for (int i = minLen + 1; i <= maxLen; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4225uL);
        bbase[i] = ((limit[i - 1] + 1) << 1) - bbase[i];
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4226uL);
    }
    private void recvDecodingTables()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4227uL);
      var s = this.data;
      bool[] inUse = s.inUse;
      byte[] pos = s.recvDecodingTables_pos;
      int inUse16 = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4228uL);
      for (int i = 0; i < 16; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4229uL);
        if (bsGetBit()) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4230uL);
          inUse16 |= 1 << i;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4231uL);
      for (int i = 256; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4232uL);
        inUse[i] = false;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4233uL);
      for (int i = 0; i < 16; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4234uL);
        if ((inUse16 & (1 << i)) != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4235uL);
          int i16 = i << 4;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4236uL);
          for (int j = 0; j < 16; j++) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4237uL);
            if (bsGetBit()) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4238uL);
              inUse[i16 + j] = true;
            }
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4239uL);
      MakeMaps();
      int alphaSize = this.nInUse + 2;
      int nGroups = GetBits(3);
      int nSelectors = GetBits(15);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4240uL);
      for (int i = 0; i < nSelectors; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4241uL);
        int j = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4242uL);
        while (bsGetBit()) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4243uL);
          j++;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4244uL);
        s.selectorMtf[i] = (byte)j;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4245uL);
      for (int v = nGroups; --v >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4246uL);
        pos[v] = (byte)v;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4247uL);
      for (int i = 0; i < nSelectors; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4248uL);
        int v = s.selectorMtf[i];
        byte tmp = pos[v];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4249uL);
        while (v > 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4250uL);
          pos[v] = pos[v - 1];
          v--;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4251uL);
        pos[0] = tmp;
        s.selector[i] = tmp;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4252uL);
      char[][] len = s.temp_charArray2d;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4253uL);
      for (int t = 0; t < nGroups; t++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4254uL);
        int curr = GetBits(5);
        char[] len_t = len[t];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4255uL);
        for (int i = 0; i < alphaSize; i++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4256uL);
          while (bsGetBit()) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4257uL);
            curr += bsGetBit() ? -1 : 1;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4258uL);
          len_t[i] = (char)curr;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4259uL);
      createHuffmanDecodingTables(alphaSize, nGroups);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4260uL);
    }
    private void createHuffmanDecodingTables(int alphaSize, int nGroups)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4261uL);
      var s = this.data;
      char[][] len = s.temp_charArray2d;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4262uL);
      for (int t = 0; t < nGroups; t++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4263uL);
        int minLen = 32;
        int maxLen = 0;
        char[] len_t = len[t];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4264uL);
        for (int i = alphaSize; --i >= 0;) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4265uL);
          char lent = len_t[i];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4266uL);
          if (lent > maxLen) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4267uL);
            maxLen = lent;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4268uL);
          if (lent < minLen) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4269uL);
            minLen = lent;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4270uL);
        hbCreateDecodeTables(s.gLimit[t], s.gBase[t], s.gPerm[t], len[t], minLen, maxLen, alphaSize);
        s.gMinlen[t] = minLen;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4271uL);
    }
    private void getAndMoveToFrontDecode()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4272uL);
      var s = this.data;
      this.origPtr = GetBits(24);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4273uL);
      if (this.origPtr < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4274uL);
        throw new IOException("BZ_DATA_ERROR");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4275uL);
      if (this.origPtr > 10 + BZip2.BlockSizeMultiple * this.blockSize100k) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4276uL);
        throw new IOException("BZ_DATA_ERROR");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4277uL);
      recvDecodingTables();
      byte[] yy = s.getAndMoveToFrontDecode_yy;
      int limitLast = this.blockSize100k * BZip2.BlockSizeMultiple;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4278uL);
      for (int i = 256; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4279uL);
        yy[i] = (byte)i;
        s.unzftab[i] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4280uL);
      int groupNo = 0;
      int groupPos = BZip2.G_SIZE - 1;
      int eob = this.nInUse + 1;
      int nextSym = getAndMoveToFrontDecode0(0);
      int bsBuffShadow = this.bsBuff;
      int bsLiveShadow = this.bsLive;
      int lastShadow = -1;
      int zt = s.selector[groupNo] & 0xff;
      int[] base_zt = s.gBase[zt];
      int[] limit_zt = s.gLimit[zt];
      int[] perm_zt = s.gPerm[zt];
      int minLens_zt = s.gMinlen[zt];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4281uL);
      while (nextSym != eob) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4282uL);
        if ((nextSym == BZip2.RUNA) || (nextSym == BZip2.RUNB)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4283uL);
          int es = -1;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4284uL);
          for (int n = 1; true; n <<= 1) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4285uL);
            if (nextSym == BZip2.RUNA) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4286uL);
              es += n;
            } else if (nextSym == BZip2.RUNB) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4288uL);
              es += n << 1;
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4287uL);
              break;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4289uL);
            if (groupPos == 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4290uL);
              groupPos = BZip2.G_SIZE - 1;
              zt = s.selector[++groupNo] & 0xff;
              base_zt = s.gBase[zt];
              limit_zt = s.gLimit[zt];
              perm_zt = s.gPerm[zt];
              minLens_zt = s.gMinlen[zt];
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4291uL);
              groupPos--;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4292uL);
            int zn = minLens_zt;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4293uL);
            while (bsLiveShadow < zn) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4294uL);
              int thech = this.input.ReadByte();
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4295uL);
              if (thech >= 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4296uL);
                bsBuffShadow = (bsBuffShadow << 8) | thech;
                bsLiveShadow += 8;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4297uL);
                continue;
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4298uL);
                throw new IOException("unexpected end of stream");
              }
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4299uL);
            int zvec = (bsBuffShadow >> (bsLiveShadow - zn)) & ((1 << zn) - 1);
            bsLiveShadow -= zn;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4300uL);
            while (zvec > limit_zt[zn]) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4301uL);
              zn++;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4302uL);
              while (bsLiveShadow < 1) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4303uL);
                int thech = this.input.ReadByte();
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4304uL);
                if (thech >= 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4305uL);
                  bsBuffShadow = (bsBuffShadow << 8) | thech;
                  bsLiveShadow += 8;
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4306uL);
                  continue;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4307uL);
                  throw new IOException("unexpected end of stream");
                }
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4308uL);
              bsLiveShadow--;
              zvec = (zvec << 1) | ((bsBuffShadow >> bsLiveShadow) & 1);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4309uL);
            nextSym = perm_zt[zvec - base_zt[zn]];
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4310uL);
          byte ch = s.seqToUnseq[yy[0]];
          s.unzftab[ch & 0xff] += es + 1;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4311uL);
          while (es-- >= 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4312uL);
            s.ll8[++lastShadow] = ch;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4313uL);
          if (lastShadow >= limitLast) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4314uL);
            throw new IOException("block overrun");
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4315uL);
          if (++lastShadow >= limitLast) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4316uL);
            throw new IOException("block overrun");
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4317uL);
          byte tmp = yy[nextSym - 1];
          s.unzftab[s.seqToUnseq[tmp] & 0xff]++;
          s.ll8[lastShadow] = s.seqToUnseq[tmp];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4318uL);
          if (nextSym <= 16) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4319uL);
            for (int j = nextSym - 1; j > 0;) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4320uL);
              yy[j] = yy[--j];
            }
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4321uL);
            System.Buffer.BlockCopy(yy, 0, yy, 1, nextSym - 1);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4322uL);
          yy[0] = tmp;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4323uL);
          if (groupPos == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4324uL);
            groupPos = BZip2.G_SIZE - 1;
            zt = s.selector[++groupNo] & 0xff;
            base_zt = s.gBase[zt];
            limit_zt = s.gLimit[zt];
            perm_zt = s.gPerm[zt];
            minLens_zt = s.gMinlen[zt];
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4325uL);
            groupPos--;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4326uL);
          int zn = minLens_zt;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4327uL);
          while (bsLiveShadow < zn) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4328uL);
            int thech = this.input.ReadByte();
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4329uL);
            if (thech >= 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4330uL);
              bsBuffShadow = (bsBuffShadow << 8) | thech;
              bsLiveShadow += 8;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4331uL);
              continue;
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4332uL);
              throw new IOException("unexpected end of stream");
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4333uL);
          int zvec = (bsBuffShadow >> (bsLiveShadow - zn)) & ((1 << zn) - 1);
          bsLiveShadow -= zn;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4334uL);
          while (zvec > limit_zt[zn]) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4335uL);
            zn++;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4336uL);
            while (bsLiveShadow < 1) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4337uL);
              int thech = this.input.ReadByte();
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4338uL);
              if (thech >= 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4339uL);
                bsBuffShadow = (bsBuffShadow << 8) | thech;
                bsLiveShadow += 8;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4340uL);
                continue;
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4341uL);
                throw new IOException("unexpected end of stream");
              }
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4342uL);
            bsLiveShadow--;
            zvec = (zvec << 1) | ((bsBuffShadow >> bsLiveShadow) & 1);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4343uL);
          nextSym = perm_zt[zvec - base_zt[zn]];
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4344uL);
      this.last = lastShadow;
      this.bsLive = bsLiveShadow;
      this.bsBuff = bsBuffShadow;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4345uL);
    }
    private int getAndMoveToFrontDecode0(int groupNo)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4346uL);
      var s = this.data;
      int zt = s.selector[groupNo] & 0xff;
      int[] limit_zt = s.gLimit[zt];
      int zn = s.gMinlen[zt];
      int zvec = GetBits(zn);
      int bsLiveShadow = this.bsLive;
      int bsBuffShadow = this.bsBuff;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4347uL);
      while (zvec > limit_zt[zn]) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4348uL);
        zn++;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4349uL);
        while (bsLiveShadow < 1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4350uL);
          int thech = this.input.ReadByte();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4351uL);
          if (thech >= 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4352uL);
            bsBuffShadow = (bsBuffShadow << 8) | thech;
            bsLiveShadow += 8;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4353uL);
            continue;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4354uL);
            throw new IOException("unexpected end of stream");
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4355uL);
        bsLiveShadow--;
        zvec = (zvec << 1) | ((bsBuffShadow >> bsLiveShadow) & 1);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4356uL);
      this.bsLive = bsLiveShadow;
      this.bsBuff = bsBuffShadow;
      System.Int32 RNTRNTRNT_497 = s.gPerm[zt][zvec - s.gBase[zt][zn]];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4357uL);
      return RNTRNTRNT_497;
    }
    private void SetupBlock()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4358uL);
      if (this.data == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4359uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4360uL);
      int i;
      var s = this.data;
      int[] tt = s.initTT(this.last + 1);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4361uL);
      for (i = 0; i <= 255; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4362uL);
        if (s.unzftab[i] < 0 || s.unzftab[i] > this.last) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4363uL);
          throw new Exception("BZ_DATA_ERROR");
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4364uL);
      s.cftab[0] = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4365uL);
      for (i = 1; i <= 256; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4366uL);
        s.cftab[i] = s.unzftab[i - 1];
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4367uL);
      for (i = 1; i <= 256; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4368uL);
        s.cftab[i] += s.cftab[i - 1];
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4369uL);
      for (i = 0; i <= 256; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4370uL);
        if (s.cftab[i] < 0 || s.cftab[i] > this.last + 1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4371uL);
          var msg = String.Format("BZ_DATA_ERROR: cftab[{0}]={1} last={2}", i, s.cftab[i], this.last);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4372uL);
          throw new Exception(msg);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4373uL);
      for (i = 1; i <= 256; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4374uL);
        if (s.cftab[i - 1] > s.cftab[i]) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4375uL);
          throw new Exception("BZ_DATA_ERROR");
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4376uL);
      int lastShadow;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4377uL);
      for (i = 0,lastShadow = this.last; i <= lastShadow; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4378uL);
        tt[s.cftab[s.ll8[i] & 0xff]++] = i;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4379uL);
      if ((this.origPtr < 0) || (this.origPtr >= tt.Length)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4380uL);
        throw new IOException("stream corrupted");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4381uL);
      this.su_tPos = tt[this.origPtr];
      this.su_count = 0;
      this.su_i2 = 0;
      this.su_ch2 = 256;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4382uL);
      if (this.blockRandomised) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4383uL);
        this.su_rNToGo = 0;
        this.su_rTPos = 0;
        SetupRandPartA();
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4384uL);
        SetupNoRandPartA();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4385uL);
    }
    private void SetupRandPartA()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4386uL);
      if (this.su_i2 <= this.last) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4387uL);
        this.su_chPrev = this.su_ch2;
        int su_ch2Shadow = this.data.ll8[this.su_tPos] & 0xff;
        this.su_tPos = this.data.tt[this.su_tPos];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4388uL);
        if (this.su_rNToGo == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4389uL);
          this.su_rNToGo = Rand.Rnums(this.su_rTPos) - 1;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4390uL);
          if (++this.su_rTPos == 512) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4391uL);
            this.su_rTPos = 0;
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4392uL);
          this.su_rNToGo--;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4393uL);
        this.su_ch2 = su_ch2Shadow ^= (this.su_rNToGo == 1) ? 1 : 0;
        this.su_i2++;
        this.currentChar = su_ch2Shadow;
        this.currentState = CState.RAND_PART_B;
        this.crc.UpdateCRC((byte)su_ch2Shadow);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4394uL);
        EndBlock();
        InitBlock();
        SetupBlock();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4395uL);
    }
    private void SetupNoRandPartA()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4396uL);
      if (this.su_i2 <= this.last) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4397uL);
        this.su_chPrev = this.su_ch2;
        int su_ch2Shadow = this.data.ll8[this.su_tPos] & 0xff;
        this.su_ch2 = su_ch2Shadow;
        this.su_tPos = this.data.tt[this.su_tPos];
        this.su_i2++;
        this.currentChar = su_ch2Shadow;
        this.currentState = CState.NO_RAND_PART_B;
        this.crc.UpdateCRC((byte)su_ch2Shadow);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4398uL);
        this.currentState = CState.NO_RAND_PART_A;
        EndBlock();
        InitBlock();
        SetupBlock();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4399uL);
    }
    private void SetupRandPartB()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4400uL);
      if (this.su_ch2 != this.su_chPrev) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4401uL);
        this.currentState = CState.RAND_PART_A;
        this.su_count = 1;
        SetupRandPartA();
      } else if (++this.su_count >= 4) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4403uL);
        this.su_z = (char)(this.data.ll8[this.su_tPos] & 0xff);
        this.su_tPos = this.data.tt[this.su_tPos];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4404uL);
        if (this.su_rNToGo == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4405uL);
          this.su_rNToGo = Rand.Rnums(this.su_rTPos) - 1;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4406uL);
          if (++this.su_rTPos == 512) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4407uL);
            this.su_rTPos = 0;
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4408uL);
          this.su_rNToGo--;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4409uL);
        this.su_j2 = 0;
        this.currentState = CState.RAND_PART_C;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4410uL);
        if (this.su_rNToGo == 1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4411uL);
          this.su_z ^= (char)1;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4412uL);
        SetupRandPartC();
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4402uL);
        this.currentState = CState.RAND_PART_A;
        SetupRandPartA();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4413uL);
    }
    private void SetupRandPartC()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4414uL);
      if (this.su_j2 < this.su_z) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4415uL);
        this.currentChar = this.su_ch2;
        this.crc.UpdateCRC((byte)this.su_ch2);
        this.su_j2++;
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4416uL);
        this.currentState = CState.RAND_PART_A;
        this.su_i2++;
        this.su_count = 0;
        SetupRandPartA();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4417uL);
    }
    private void SetupNoRandPartB()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4418uL);
      if (this.su_ch2 != this.su_chPrev) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4419uL);
        this.su_count = 1;
        SetupNoRandPartA();
      } else if (++this.su_count >= 4) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4421uL);
        this.su_z = (char)(this.data.ll8[this.su_tPos] & 0xff);
        this.su_tPos = this.data.tt[this.su_tPos];
        this.su_j2 = 0;
        SetupNoRandPartC();
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4420uL);
        SetupNoRandPartA();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4422uL);
    }
    private void SetupNoRandPartC()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4423uL);
      if (this.su_j2 < this.su_z) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4424uL);
        int su_ch2Shadow = this.su_ch2;
        this.currentChar = su_ch2Shadow;
        this.crc.UpdateCRC((byte)su_ch2Shadow);
        this.su_j2++;
        this.currentState = CState.NO_RAND_PART_C;
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4425uL);
        this.su_i2++;
        this.su_count = 0;
        SetupNoRandPartA();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4426uL);
    }
    private sealed class DecompressionState
    {
      public readonly bool[] inUse = new bool[256];
      public readonly byte[] seqToUnseq = new byte[256];
      public readonly byte[] selector = new byte[BZip2.MaxSelectors];
      public readonly byte[] selectorMtf = new byte[BZip2.MaxSelectors];
      public readonly int[] unzftab;
      public readonly int[][] gLimit;
      public readonly int[][] gBase;
      public readonly int[][] gPerm;
      public readonly int[] gMinlen;
      public readonly int[] cftab;
      public readonly byte[] getAndMoveToFrontDecode_yy;
      public readonly char[][] temp_charArray2d;
      public readonly byte[] recvDecodingTables_pos;
      public int[] tt;
      public byte[] ll8;
      public DecompressionState(int blockSize100k)
      {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4427uL);
        this.unzftab = new int[256];
        this.gLimit = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.gBase = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.gPerm = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.gMinlen = new int[BZip2.NGroups];
        this.cftab = new int[257];
        this.getAndMoveToFrontDecode_yy = new byte[256];
        this.temp_charArray2d = BZip2.InitRectangularArray<char>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.recvDecodingTables_pos = new byte[BZip2.NGroups];
        this.ll8 = new byte[blockSize100k * BZip2.BlockSizeMultiple];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4428uL);
      }
      public int[] initTT(int length)
      {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4429uL);
        int[] ttShadow = this.tt;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4430uL);
        if ((ttShadow == null) || (ttShadow.Length < length)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4431uL);
          this.tt = ttShadow = new int[length];
        }
        System.Int32[] RNTRNTRNT_498 = ttShadow;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4432uL);
        return RNTRNTRNT_498;
      }
    }
  }
  static internal class BZip2
  {
    static internal T[][] InitRectangularArray<T>(int d1, int d2)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4433uL);
      var x = new T[d1][];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4434uL);
      for (int i = 0; i < d1; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4435uL);
        x[i] = new T[d2];
      }
      T[][] RNTRNTRNT_499 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4436uL);
      return RNTRNTRNT_499;
    }
    public static readonly int BlockSizeMultiple = 100000;
    public static readonly int MinBlockSize = 1;
    public static readonly int MaxBlockSize = 9;
    public static readonly int MaxAlphaSize = 258;
    public static readonly int MaxCodeLength = 23;
    public static readonly char RUNA = (char)0;
    public static readonly char RUNB = (char)1;
    public static readonly int NGroups = 6;
    public static readonly int G_SIZE = 50;
    public static readonly int N_ITERS = 4;
    public static readonly int MaxSelectors = (2 + (900000 / G_SIZE));
    public static readonly int NUM_OVERSHOOT_BYTES = 20;
    static internal readonly int QSORT_STACK_SIZE = 1000;
  }
}
