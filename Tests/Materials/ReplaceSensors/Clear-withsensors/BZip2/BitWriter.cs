using System;
using System.IO;
namespace Ionic.BZip2
{
  internal class BitWriter
  {
    uint accumulator;
    int nAccumulatedBits;
    Stream output;
    int totalBytesWrittenOut;
    public BitWriter(Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3609uL);
      this.output = s;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3610uL);
    }
    public byte RemainingBits {
      get {
        System.Byte RNTRNTRNT_469 = (byte)(this.accumulator >> (32 - this.nAccumulatedBits) & 0xff);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3611uL);
        return RNTRNTRNT_469;
      }
    }
    public int NumRemainingBits {
      get {
        System.Int32 RNTRNTRNT_470 = this.nAccumulatedBits;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3612uL);
        return RNTRNTRNT_470;
      }
    }
    public int TotalBytesWrittenOut {
      get {
        System.Int32 RNTRNTRNT_471 = this.totalBytesWrittenOut;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3613uL);
        return RNTRNTRNT_471;
      }
    }
    public void Reset()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3614uL);
      this.accumulator = 0;
      this.nAccumulatedBits = 0;
      this.totalBytesWrittenOut = 0;
      this.output.Seek(0, SeekOrigin.Begin);
      this.output.SetLength(0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3615uL);
    }
    public void WriteBits(int nbits, uint value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3616uL);
      int nAccumulated = this.nAccumulatedBits;
      uint u = this.accumulator;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3617uL);
      while (nAccumulated >= 8) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3618uL);
        this.output.WriteByte((byte)(u >> 24 & 0xff));
        this.totalBytesWrittenOut++;
        u <<= 8;
        nAccumulated -= 8;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3619uL);
      this.accumulator = u | (value << (32 - nAccumulated - nbits));
      this.nAccumulatedBits = nAccumulated + nbits;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3620uL);
    }
    public void WriteByte(byte b)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3621uL);
      WriteBits(8, b);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3622uL);
    }
    public void WriteInt(uint u)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3623uL);
      WriteBits(8, (u >> 24) & 0xff);
      WriteBits(8, (u >> 16) & 0xff);
      WriteBits(8, (u >> 8) & 0xff);
      WriteBits(8, u & 0xff);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3624uL);
    }
    public void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3625uL);
      WriteBits(0, 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3626uL);
    }
    public void FinishAndPad()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3627uL);
      Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3628uL);
      if (this.NumRemainingBits > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3629uL);
        byte b = (byte)((this.accumulator >> 24) & 0xff);
        this.output.WriteByte(b);
        this.totalBytesWrittenOut++;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3630uL);
    }
  }
}
