using System;
using System.IO;
namespace Ionic.BZip2
{
  internal class BZip2Compressor
  {
    private int blockSize100k;
    private int currentByte = -1;
    private int runLength = 0;
    private int last;
    private int outBlockFillThreshold;
    private CompressionState cstate;
    private readonly Ionic.Crc.CRC32 crc = new Ionic.Crc.CRC32(true);
    BitWriter bw;
    int runs;
    private int workDone;
    private int workLimit;
    private bool firstAttempt;
    private bool blockRandomised;
    private int origPtr;
    private int nInUse;
    private int nMTF;
    private static readonly int SETMASK = (1 << 21);
    private static readonly int CLEARMASK = (~SETMASK);
    private static readonly byte GREATER_ICOST = 15;
    private static readonly byte LESSER_ICOST = 0;
    private static readonly int SMALL_THRESH = 20;
    private static readonly int DEPTH_THRESH = 10;
    private static readonly int WORK_FACTOR = 30;
    private static readonly int[] increments = {
      1,
      4,
      13,
      40,
      121,
      364,
      1093,
      3280,
      9841,
      29524,
      88573,
      265720,
      797161,
      2391484
    };
    public BZip2Compressor(BitWriter writer) : this(writer, BZip2.MaxBlockSize)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3631uL);
    }
    public BZip2Compressor(BitWriter writer, int blockSize)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3632uL);
      this.blockSize100k = blockSize;
      this.bw = writer;
      this.outBlockFillThreshold = (blockSize * BZip2.BlockSizeMultiple) - 20;
      this.cstate = new CompressionState(blockSize);
      Reset();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3633uL);
    }
    void Reset()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3634uL);
      this.crc.Reset();
      this.currentByte = -1;
      this.runLength = 0;
      this.last = -1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3635uL);
      for (int i = 256; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3636uL);
        this.cstate.inUse[i] = false;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3637uL);
    }
    public int BlockSize {
      get {
        System.Int32 RNTRNTRNT_472 = this.blockSize100k;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3638uL);
        return RNTRNTRNT_472;
      }
    }
    public uint Crc32 { get; private set; }
    public int AvailableBytesOut { get; private set; }
    public int UncompressedBytes {
      get {
        System.Int32 RNTRNTRNT_473 = this.last + 1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3639uL);
        return RNTRNTRNT_473;
      }
    }
    public int Fill(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3640uL);
      if (this.last >= this.outBlockFillThreshold) {
        System.Int32 RNTRNTRNT_474 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3641uL);
        return RNTRNTRNT_474;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3642uL);
      int bytesWritten = 0;
      int limit = offset + count;
      int rc;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3643uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3644uL);
        rc = write0(buffer[offset++]);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3645uL);
        if (rc > 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3646uL);
          bytesWritten++;
        }
      } while (offset < limit && rc == 1);
      System.Int32 RNTRNTRNT_475 = bytesWritten;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3647uL);
      return RNTRNTRNT_475;
    }
    private int write0(byte b)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3648uL);
      bool rc;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3649uL);
      if (this.currentByte == -1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3650uL);
        this.currentByte = b;
        this.runLength++;
        System.Int32 RNTRNTRNT_476 = 1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3651uL);
        return RNTRNTRNT_476;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3652uL);
      if (this.currentByte == b) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3653uL);
        if (++this.runLength > 254) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3654uL);
          rc = AddRunToOutputBlock(false);
          this.currentByte = -1;
          this.runLength = 0;
          System.Int32 RNTRNTRNT_477 = (rc) ? 2 : 1;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3655uL);
          return RNTRNTRNT_477;
        }
        System.Int32 RNTRNTRNT_478 = 1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3656uL);
        return RNTRNTRNT_478;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3657uL);
      rc = AddRunToOutputBlock(false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3658uL);
      if (rc) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3659uL);
        this.currentByte = -1;
        this.runLength = 0;
        System.Int32 RNTRNTRNT_479 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3660uL);
        return RNTRNTRNT_479;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3661uL);
      this.runLength = 1;
      this.currentByte = b;
      System.Int32 RNTRNTRNT_480 = 1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3662uL);
      return RNTRNTRNT_480;
    }
    private bool AddRunToOutputBlock(bool final)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3663uL);
      runs++;
      int previousLast = this.last;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3664uL);
      if (previousLast >= this.outBlockFillThreshold && !final) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3665uL);
        var msg = String.Format("block overrun(final={2}): {0} >= threshold ({1})", previousLast, this.outBlockFillThreshold, final);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3666uL);
        throw new Exception(msg);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3667uL);
      byte b = (byte)this.currentByte;
      byte[] block = this.cstate.block;
      this.cstate.inUse[b] = true;
      int rl = this.runLength;
      this.crc.UpdateCRC(b, rl);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3668uL);
      switch (rl) {
        case 1:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3671uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3669uL);
            block[previousLast + 2] = b;
            this.last = previousLast + 1;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3670uL);
            break;
          }

        case 2:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3674uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3672uL);
            block[previousLast + 2] = b;
            block[previousLast + 3] = b;
            this.last = previousLast + 2;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3673uL);
            break;
          }

        case 3:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3677uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3675uL);
            block[previousLast + 2] = b;
            block[previousLast + 3] = b;
            block[previousLast + 4] = b;
            this.last = previousLast + 3;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3676uL);
            break;
          }

        default:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3680uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3678uL);
            rl -= 4;
            this.cstate.inUse[rl] = true;
            block[previousLast + 2] = b;
            block[previousLast + 3] = b;
            block[previousLast + 4] = b;
            block[previousLast + 5] = b;
            block[previousLast + 6] = (byte)rl;
            this.last = previousLast + 5;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3679uL);
            break;
          }

      }
      System.Boolean RNTRNTRNT_481 = (this.last >= this.outBlockFillThreshold);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3681uL);
      return RNTRNTRNT_481;
    }
    public void CompressAndWrite()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3682uL);
      if (this.runLength > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3683uL);
        AddRunToOutputBlock(true);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3684uL);
      this.currentByte = -1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3685uL);
      if (this.last == -1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3686uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3687uL);
      blockSort();
      this.bw.WriteByte(0x31);
      this.bw.WriteByte(0x41);
      this.bw.WriteByte(0x59);
      this.bw.WriteByte(0x26);
      this.bw.WriteByte(0x53);
      this.bw.WriteByte(0x59);
      this.Crc32 = (uint)this.crc.Crc32Result;
      this.bw.WriteInt(this.Crc32);
      this.bw.WriteBits(1, (this.blockRandomised) ? 1u : 0u);
      moveToFrontCodeAndSend();
      Reset();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3688uL);
    }
    private void randomiseBlock()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3689uL);
      bool[] inUse = this.cstate.inUse;
      byte[] block = this.cstate.block;
      int lastShadow = this.last;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3690uL);
      for (int i = 256; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3691uL);
        inUse[i] = false;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3692uL);
      int rNToGo = 0;
      int rTPos = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3693uL);
      for (int i = 0, j = 1; i <= lastShadow; i = j,j++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3694uL);
        if (rNToGo == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3695uL);
          rNToGo = (char)Rand.Rnums(rTPos);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3696uL);
          if (++rTPos == 512) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3697uL);
            rTPos = 0;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3698uL);
        rNToGo--;
        block[j] ^= (byte)((rNToGo == 1) ? 1 : 0);
        inUse[block[j] & 0xff] = true;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3699uL);
      this.blockRandomised = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3700uL);
    }
    private void mainSort()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3701uL);
      CompressionState dataShadow = this.cstate;
      int[] runningOrder = dataShadow.mainSort_runningOrder;
      int[] copy = dataShadow.mainSort_copy;
      bool[] bigDone = dataShadow.mainSort_bigDone;
      int[] ftab = dataShadow.ftab;
      byte[] block = dataShadow.block;
      int[] fmap = dataShadow.fmap;
      char[] quadrant = dataShadow.quadrant;
      int lastShadow = this.last;
      int workLimitShadow = this.workLimit;
      bool firstAttemptShadow = this.firstAttempt;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3702uL);
      for (int i = 65537; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3703uL);
        ftab[i] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3704uL);
      for (int i = 0; i < BZip2.NUM_OVERSHOOT_BYTES; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3705uL);
        block[lastShadow + i + 2] = block[(i % (lastShadow + 1)) + 1];
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3706uL);
      for (int i = lastShadow + BZip2.NUM_OVERSHOOT_BYTES + 1; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3707uL);
        quadrant[i] = '\0';
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3708uL);
      block[0] = block[lastShadow + 1];
      int c1 = block[0] & 0xff;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3709uL);
      for (int i = 0; i <= lastShadow; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3710uL);
        int c2 = block[i + 1] & 0xff;
        ftab[(c1 << 8) + c2]++;
        c1 = c2;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3711uL);
      for (int i = 1; i <= 65536; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3712uL);
        ftab[i] += ftab[i - 1];
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3713uL);
      c1 = block[1] & 0xff;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3714uL);
      for (int i = 0; i < lastShadow; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3715uL);
        int c2 = block[i + 2] & 0xff;
        fmap[--ftab[(c1 << 8) + c2]] = i;
        c1 = c2;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3716uL);
      fmap[--ftab[((block[lastShadow + 1] & 0xff) << 8) + (block[1] & 0xff)]] = lastShadow;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3717uL);
      for (int i = 256; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3718uL);
        bigDone[i] = false;
        runningOrder[i] = i;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3719uL);
      for (int h = 364; h != 1;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3720uL);
        h /= 3;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3721uL);
        for (int i = h; i <= 255; i++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3722uL);
          int vv = runningOrder[i];
          int a = ftab[(vv + 1) << 8] - ftab[vv << 8];
          int b = h - 1;
          int j = i;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3723uL);
          for (int ro = runningOrder[j - h]; (ftab[(ro + 1) << 8] - ftab[ro << 8]) > a; ro = runningOrder[j - h]) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3724uL);
            runningOrder[j] = ro;
            j -= h;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3725uL);
            if (j <= b) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3726uL);
              break;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3727uL);
          runningOrder[j] = vv;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3728uL);
      for (int i = 0; i <= 255; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3729uL);
        int ss = runningOrder[i];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3730uL);
        for (int j = 0; j <= 255; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3731uL);
          int sb = (ss << 8) + j;
          int ftab_sb = ftab[sb];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3732uL);
          if ((ftab_sb & SETMASK) != SETMASK) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3733uL);
            int lo = ftab_sb & CLEARMASK;
            int hi = (ftab[sb + 1] & CLEARMASK) - 1;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3734uL);
            if (hi > lo) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3735uL);
              mainQSort3(dataShadow, lo, hi, 2);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3736uL);
              if (firstAttemptShadow && (this.workDone > workLimitShadow)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3737uL);
                return;
              }
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3738uL);
            ftab[sb] = ftab_sb | SETMASK;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3739uL);
        for (int j = 0; j <= 255; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3740uL);
          copy[j] = ftab[(j << 8) + ss] & CLEARMASK;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3741uL);
        for (int j = ftab[ss << 8] & CLEARMASK, hj = (ftab[(ss + 1) << 8] & CLEARMASK); j < hj; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3742uL);
          int fmap_j = fmap[j];
          c1 = block[fmap_j] & 0xff;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3743uL);
          if (!bigDone[c1]) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3744uL);
            fmap[copy[c1]] = (fmap_j == 0) ? lastShadow : (fmap_j - 1);
            copy[c1]++;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3745uL);
        for (int j = 256; --j >= 0;) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3746uL);
          ftab[(j << 8) + ss] |= SETMASK;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3747uL);
        bigDone[ss] = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3748uL);
        if (i < 255) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3749uL);
          int bbStart = ftab[ss << 8] & CLEARMASK;
          int bbSize = (ftab[(ss + 1) << 8] & CLEARMASK) - bbStart;
          int shifts = 0;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3750uL);
          while ((bbSize >> shifts) > 65534) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3751uL);
            shifts++;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3752uL);
          for (int j = 0; j < bbSize; j++) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3753uL);
            int a2update = fmap[bbStart + j];
            char qVal = (char)(j >> shifts);
            quadrant[a2update] = qVal;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3754uL);
            if (a2update < BZip2.NUM_OVERSHOOT_BYTES) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3755uL);
              quadrant[a2update + lastShadow + 1] = qVal;
            }
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3756uL);
    }
    private void blockSort()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3757uL);
      this.workLimit = WORK_FACTOR * this.last;
      this.workDone = 0;
      this.blockRandomised = false;
      this.firstAttempt = true;
      mainSort();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3758uL);
      if (this.firstAttempt && (this.workDone > this.workLimit)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3759uL);
        randomiseBlock();
        this.workLimit = this.workDone = 0;
        this.firstAttempt = false;
        mainSort();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3760uL);
      int[] fmap = this.cstate.fmap;
      this.origPtr = -1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3761uL);
      for (int i = 0, lastShadow = this.last; i <= lastShadow; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3762uL);
        if (fmap[i] == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3763uL);
          this.origPtr = i;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3764uL);
          break;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3765uL);
    }
    private bool mainSimpleSort(CompressionState dataShadow, int lo, int hi, int d)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3766uL);
      int bigN = hi - lo + 1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3767uL);
      if (bigN < 2) {
        System.Boolean RNTRNTRNT_482 = this.firstAttempt && (this.workDone > this.workLimit);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3768uL);
        return RNTRNTRNT_482;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3769uL);
      int hp = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3770uL);
      while (increments[hp] < bigN) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3771uL);
        hp++;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3772uL);
      int[] fmap = dataShadow.fmap;
      char[] quadrant = dataShadow.quadrant;
      byte[] block = dataShadow.block;
      int lastShadow = this.last;
      int lastPlus1 = lastShadow + 1;
      bool firstAttemptShadow = this.firstAttempt;
      int workLimitShadow = this.workLimit;
      int workDoneShadow = this.workDone;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3773uL);
      while (--hp >= 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3774uL);
        int h = increments[hp];
        int mj = lo + h - 1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3775uL);
        for (int i = lo + h; i <= hi;) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3776uL);
          for (int k = 3; (i <= hi) && (--k >= 0); i++) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3777uL);
            int v = fmap[i];
            int vd = v + d;
            int j = i;
            bool onceRunned = false;
            int a = 0;
            HAMMER:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3778uL);
            while (true) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3779uL);
              if (onceRunned) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3780uL);
                fmap[j] = a;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3781uL);
                if ((j -= h) <= mj) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3782uL);
                  goto END_HAMMER;
                }
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3783uL);
                onceRunned = true;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3784uL);
              a = fmap[j - h];
              int i1 = a + d;
              int i2 = vd;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3785uL);
              if (block[i1 + 1] == block[i2 + 1]) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3786uL);
                if (block[i1 + 2] == block[i2 + 2]) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3787uL);
                  if (block[i1 + 3] == block[i2 + 3]) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3788uL);
                    if (block[i1 + 4] == block[i2 + 4]) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3789uL);
                      if (block[i1 + 5] == block[i2 + 5]) {
                        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3790uL);
                        if (block[(i1 += 6)] == block[(i2 += 6)]) {
                          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3791uL);
                          int x = lastShadow;
                          X:
                          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3792uL);
                          while (x > 0) {
                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3793uL);
                            x -= 4;
                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3794uL);
                            if (block[i1 + 1] == block[i2 + 1]) {
                              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3795uL);
                              if (quadrant[i1] == quadrant[i2]) {
                                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3796uL);
                                if (block[i1 + 2] == block[i2 + 2]) {
                                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3797uL);
                                  if (quadrant[i1 + 1] == quadrant[i2 + 1]) {
                                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3798uL);
                                    if (block[i1 + 3] == block[i2 + 3]) {
                                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3799uL);
                                      if (quadrant[i1 + 2] == quadrant[i2 + 2]) {
                                        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3800uL);
                                        if (block[i1 + 4] == block[i2 + 4]) {
                                          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3801uL);
                                          if (quadrant[i1 + 3] == quadrant[i2 + 3]) {
                                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3802uL);
                                            if ((i1 += 4) >= lastPlus1) {
                                              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3803uL);
                                              i1 -= lastPlus1;
                                            }
                                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3804uL);
                                            if ((i2 += 4) >= lastPlus1) {
                                              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3805uL);
                                              i2 -= lastPlus1;
                                            }
                                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3806uL);
                                            workDoneShadow++;
                                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3807uL);
                                            goto X;
                                          } else if ((quadrant[i1 + 3] > quadrant[i2 + 3])) {
                                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3809uL);
                                            goto HAMMER;
                                          } else {
                                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3808uL);
                                            goto END_HAMMER;
                                          }
                                        } else if ((block[i1 + 4] & 0xff) > (block[i2 + 4] & 0xff)) {
                                          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3811uL);
                                          goto HAMMER;
                                        } else {
                                          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3810uL);
                                          goto END_HAMMER;
                                        }
                                      } else if ((quadrant[i1 + 2] > quadrant[i2 + 2])) {
                                        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3813uL);
                                        goto HAMMER;
                                      } else {
                                        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3812uL);
                                        goto END_HAMMER;
                                      }
                                    } else if ((block[i1 + 3] & 0xff) > (block[i2 + 3] & 0xff)) {
                                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3815uL);
                                      goto HAMMER;
                                    } else {
                                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3814uL);
                                      goto END_HAMMER;
                                    }
                                  } else if ((quadrant[i1 + 1] > quadrant[i2 + 1])) {
                                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3817uL);
                                    goto HAMMER;
                                  } else {
                                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3816uL);
                                    goto END_HAMMER;
                                  }
                                } else if ((block[i1 + 2] & 0xff) > (block[i2 + 2] & 0xff)) {
                                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3819uL);
                                  goto HAMMER;
                                } else {
                                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3818uL);
                                  goto END_HAMMER;
                                }
                              } else if ((quadrant[i1] > quadrant[i2])) {
                                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3821uL);
                                goto HAMMER;
                              } else {
                                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3820uL);
                                goto END_HAMMER;
                              }
                            } else if ((block[i1 + 1] & 0xff) > (block[i2 + 1] & 0xff)) {
                              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3823uL);
                              goto HAMMER;
                            } else {
                              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3822uL);
                              goto END_HAMMER;
                            }
                          }
                          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3824uL);
                          goto END_HAMMER;
                        } else {
                          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3825uL);
                          if ((block[i1] & 0xff) > (block[i2] & 0xff)) {
                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3826uL);
                            goto HAMMER;
                          } else {
                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3827uL);
                            goto END_HAMMER;
                          }
                        }
                      } else if ((block[i1 + 5] & 0xff) > (block[i2 + 5] & 0xff)) {
                        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3829uL);
                        goto HAMMER;
                      } else {
                        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3828uL);
                        goto END_HAMMER;
                      }
                    } else if ((block[i1 + 4] & 0xff) > (block[i2 + 4] & 0xff)) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3831uL);
                      goto HAMMER;
                    } else {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3830uL);
                      goto END_HAMMER;
                    }
                  } else if ((block[i1 + 3] & 0xff) > (block[i2 + 3] & 0xff)) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3833uL);
                    goto HAMMER;
                  } else {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3832uL);
                    goto END_HAMMER;
                  }
                } else if ((block[i1 + 2] & 0xff) > (block[i2 + 2] & 0xff)) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3835uL);
                  goto HAMMER;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3834uL);
                  goto END_HAMMER;
                }
              } else if ((block[i1 + 1] & 0xff) > (block[i2 + 1] & 0xff)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3837uL);
                goto HAMMER;
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3836uL);
                goto END_HAMMER;
              }
            }
            END_HAMMER:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3838uL);
            fmap[j] = v;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3839uL);
          if (firstAttemptShadow && (i <= hi) && (workDoneShadow > workLimitShadow)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3840uL);
            goto END_HP;
          }
        }
      }
      END_HP:
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3841uL);
      this.workDone = workDoneShadow;
      System.Boolean RNTRNTRNT_483 = firstAttemptShadow && (workDoneShadow > workLimitShadow);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3842uL);
      return RNTRNTRNT_483;
    }
    private static void vswap(int[] fmap, int p1, int p2, int n)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3843uL);
      n += p1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3844uL);
      while (p1 < n) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3845uL);
        int t = fmap[p1];
        fmap[p1++] = fmap[p2];
        fmap[p2++] = t;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3846uL);
    }
    private static byte med3(byte a, byte b, byte c)
    {
      System.Byte RNTRNTRNT_484 = (a < b) ? (b < c ? b : a < c ? c : a) : (b > c ? b : a > c ? c : a);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3847uL);
      return RNTRNTRNT_484;
    }
    private void mainQSort3(CompressionState dataShadow, int loSt, int hiSt, int dSt)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3848uL);
      int[] stack_ll = dataShadow.stack_ll;
      int[] stack_hh = dataShadow.stack_hh;
      int[] stack_dd = dataShadow.stack_dd;
      int[] fmap = dataShadow.fmap;
      byte[] block = dataShadow.block;
      stack_ll[0] = loSt;
      stack_hh[0] = hiSt;
      stack_dd[0] = dSt;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3849uL);
      for (int sp = 1; --sp >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3850uL);
        int lo = stack_ll[sp];
        int hi = stack_hh[sp];
        int d = stack_dd[sp];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3851uL);
        if ((hi - lo < SMALL_THRESH) || (d > DEPTH_THRESH)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3852uL);
          if (mainSimpleSort(dataShadow, lo, hi, d)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3853uL);
            return;
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3854uL);
          int d1 = d + 1;
          int med = med3(block[fmap[lo] + d1], block[fmap[hi] + d1], block[fmap[(lo + hi) >> 1] + d1]) & 0xff;
          int unLo = lo;
          int unHi = hi;
          int ltLo = lo;
          int gtHi = hi;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3855uL);
          while (true) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3856uL);
            while (unLo <= unHi) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3857uL);
              int n = (block[fmap[unLo] + d1] & 0xff) - med;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3858uL);
              if (n == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3859uL);
                int temp = fmap[unLo];
                fmap[unLo++] = fmap[ltLo];
                fmap[ltLo++] = temp;
              } else if (n < 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3861uL);
                unLo++;
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3860uL);
                break;
              }
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3862uL);
            while (unLo <= unHi) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3863uL);
              int n = (block[fmap[unHi] + d1] & 0xff) - med;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3864uL);
              if (n == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3865uL);
                int temp = fmap[unHi];
                fmap[unHi--] = fmap[gtHi];
                fmap[gtHi--] = temp;
              } else if (n > 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3867uL);
                unHi--;
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3866uL);
                break;
              }
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3868uL);
            if (unLo <= unHi) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3869uL);
              int temp = fmap[unLo];
              fmap[unLo++] = fmap[unHi];
              fmap[unHi--] = temp;
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3870uL);
              break;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3871uL);
          if (gtHi < ltLo) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3872uL);
            stack_ll[sp] = lo;
            stack_hh[sp] = hi;
            stack_dd[sp] = d1;
            sp++;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3873uL);
            int n = ((ltLo - lo) < (unLo - ltLo)) ? (ltLo - lo) : (unLo - ltLo);
            vswap(fmap, lo, unLo - n, n);
            int m = ((hi - gtHi) < (gtHi - unHi)) ? (hi - gtHi) : (gtHi - unHi);
            vswap(fmap, unLo, hi - m + 1, m);
            n = lo + unLo - ltLo - 1;
            m = hi - (gtHi - unHi) + 1;
            stack_ll[sp] = lo;
            stack_hh[sp] = n;
            stack_dd[sp] = d;
            sp++;
            stack_ll[sp] = n + 1;
            stack_hh[sp] = m - 1;
            stack_dd[sp] = d1;
            sp++;
            stack_ll[sp] = m;
            stack_hh[sp] = hi;
            stack_dd[sp] = d;
            sp++;
          }
        }
      }
    }
    private void generateMTFValues()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3874uL);
      int lastShadow = this.last;
      CompressionState dataShadow = this.cstate;
      bool[] inUse = dataShadow.inUse;
      byte[] block = dataShadow.block;
      int[] fmap = dataShadow.fmap;
      char[] sfmap = dataShadow.sfmap;
      int[] mtfFreq = dataShadow.mtfFreq;
      byte[] unseqToSeq = dataShadow.unseqToSeq;
      byte[] yy = dataShadow.generateMTFValues_yy;
      int nInUseShadow = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3875uL);
      for (int i = 0; i < 256; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3876uL);
        if (inUse[i]) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3877uL);
          unseqToSeq[i] = (byte)nInUseShadow;
          nInUseShadow++;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3878uL);
      this.nInUse = nInUseShadow;
      int eob = nInUseShadow + 1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3879uL);
      for (int i = eob; i >= 0; i--) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3880uL);
        mtfFreq[i] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3881uL);
      for (int i = nInUseShadow; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3882uL);
        yy[i] = (byte)i;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3883uL);
      int wr = 0;
      int zPend = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3884uL);
      for (int i = 0; i <= lastShadow; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3885uL);
        byte ll_i = unseqToSeq[block[fmap[i]] & 0xff];
        byte tmp = yy[0];
        int j = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3886uL);
        while (ll_i != tmp) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3887uL);
          j++;
          byte tmp2 = tmp;
          tmp = yy[j];
          yy[j] = tmp2;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3888uL);
        yy[0] = tmp;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3889uL);
        if (j == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3890uL);
          zPend++;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3891uL);
          if (zPend > 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3892uL);
            zPend--;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3893uL);
            while (true) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3894uL);
              if ((zPend & 1) == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3895uL);
                sfmap[wr] = BZip2.RUNA;
                wr++;
                mtfFreq[BZip2.RUNA]++;
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3896uL);
                sfmap[wr] = BZip2.RUNB;
                wr++;
                mtfFreq[BZip2.RUNB]++;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3897uL);
              if (zPend >= 2) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3898uL);
                zPend = (zPend - 2) >> 1;
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3899uL);
                break;
              }
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3900uL);
            zPend = 0;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3901uL);
          sfmap[wr] = (char)(j + 1);
          wr++;
          mtfFreq[j + 1]++;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3902uL);
      if (zPend > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3903uL);
        zPend--;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3904uL);
        while (true) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3905uL);
          if ((zPend & 1) == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3906uL);
            sfmap[wr] = BZip2.RUNA;
            wr++;
            mtfFreq[BZip2.RUNA]++;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3907uL);
            sfmap[wr] = BZip2.RUNB;
            wr++;
            mtfFreq[BZip2.RUNB]++;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3908uL);
          if (zPend >= 2) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3909uL);
            zPend = (zPend - 2) >> 1;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3910uL);
            break;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3911uL);
      sfmap[wr] = (char)eob;
      mtfFreq[eob]++;
      this.nMTF = wr + 1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3912uL);
    }
    private static void hbAssignCodes(int[] code, byte[] length, int minLen, int maxLen, int alphaSize)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3913uL);
      int vec = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3914uL);
      for (int n = minLen; n <= maxLen; n++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3915uL);
        for (int i = 0; i < alphaSize; i++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3916uL);
          if ((length[i] & 0xff) == n) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3917uL);
            code[i] = vec;
            vec++;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3918uL);
        vec <<= 1;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3919uL);
    }
    private void sendMTFValues()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3920uL);
      byte[][] len = this.cstate.sendMTFValues_len;
      int alphaSize = this.nInUse + 2;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3921uL);
      for (int t = BZip2.NGroups; --t >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3922uL);
        byte[] len_t = len[t];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3923uL);
        for (int v = alphaSize; --v >= 0;) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3924uL);
          len_t[v] = GREATER_ICOST;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3925uL);
      int nGroups = (this.nMTF < 200) ? 2 : (this.nMTF < 600) ? 3 : (this.nMTF < 1200) ? 4 : (this.nMTF < 2400) ? 5 : 6;
      sendMTFValues0(nGroups, alphaSize);
      int nSelectors = sendMTFValues1(nGroups, alphaSize);
      sendMTFValues2(nGroups, nSelectors);
      sendMTFValues3(nGroups, alphaSize);
      sendMTFValues4();
      sendMTFValues5(nGroups, nSelectors);
      sendMTFValues6(nGroups, alphaSize);
      sendMTFValues7(nSelectors);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3926uL);
    }
    private void sendMTFValues0(int nGroups, int alphaSize)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3927uL);
      byte[][] len = this.cstate.sendMTFValues_len;
      int[] mtfFreq = this.cstate.mtfFreq;
      int remF = this.nMTF;
      int gs = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3928uL);
      for (int nPart = nGroups; nPart > 0; nPart--) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3929uL);
        int tFreq = remF / nPart;
        int ge = gs - 1;
        int aFreq = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3930uL);
        for (int a = alphaSize - 1; (aFreq < tFreq) && (ge < a);) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3931uL);
          aFreq += mtfFreq[++ge];
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3932uL);
        if ((ge > gs) && (nPart != nGroups) && (nPart != 1) && (((nGroups - nPart) & 1) != 0)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3933uL);
          aFreq -= mtfFreq[ge--];
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3934uL);
        byte[] len_np = len[nPart - 1];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3935uL);
        for (int v = alphaSize; --v >= 0;) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3936uL);
          if ((v >= gs) && (v <= ge)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3937uL);
            len_np[v] = LESSER_ICOST;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3938uL);
            len_np[v] = GREATER_ICOST;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3939uL);
        gs = ge + 1;
        remF -= aFreq;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3940uL);
    }
    private static void hbMakeCodeLengths(byte[] len, int[] freq, CompressionState state1, int alphaSize, int maxLen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3941uL);
      int[] heap = state1.heap;
      int[] weight = state1.weight;
      int[] parent = state1.parent;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3942uL);
      for (int i = alphaSize; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3943uL);
        weight[i + 1] = (freq[i] == 0 ? 1 : freq[i]) << 8;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3944uL);
      for (bool tooLong = true; tooLong;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3945uL);
        tooLong = false;
        int nNodes = alphaSize;
        int nHeap = 0;
        heap[0] = 0;
        weight[0] = 0;
        parent[0] = -2;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3946uL);
        for (int i = 1; i <= alphaSize; i++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3947uL);
          parent[i] = -1;
          nHeap++;
          heap[nHeap] = i;
          int zz = nHeap;
          int tmp = heap[zz];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3948uL);
          while (weight[tmp] < weight[heap[zz >> 1]]) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3949uL);
            heap[zz] = heap[zz >> 1];
            zz >>= 1;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3950uL);
          heap[zz] = tmp;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3951uL);
        while (nHeap > 1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3952uL);
          int n1 = heap[1];
          heap[1] = heap[nHeap];
          nHeap--;
          int yy = 0;
          int zz = 1;
          int tmp = heap[1];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3953uL);
          while (true) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3954uL);
            yy = zz << 1;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3955uL);
            if (yy > nHeap) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3956uL);
              break;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3957uL);
            if ((yy < nHeap) && (weight[heap[yy + 1]] < weight[heap[yy]])) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3958uL);
              yy++;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3959uL);
            if (weight[tmp] < weight[heap[yy]]) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3960uL);
              break;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3961uL);
            heap[zz] = heap[yy];
            zz = yy;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3962uL);
          heap[zz] = tmp;
          int n2 = heap[1];
          heap[1] = heap[nHeap];
          nHeap--;
          yy = 0;
          zz = 1;
          tmp = heap[1];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3963uL);
          while (true) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3964uL);
            yy = zz << 1;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3965uL);
            if (yy > nHeap) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3966uL);
              break;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3967uL);
            if ((yy < nHeap) && (weight[heap[yy + 1]] < weight[heap[yy]])) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3968uL);
              yy++;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3969uL);
            if (weight[tmp] < weight[heap[yy]]) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3970uL);
              break;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3971uL);
            heap[zz] = heap[yy];
            zz = yy;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3972uL);
          heap[zz] = tmp;
          nNodes++;
          parent[n1] = parent[n2] = nNodes;
          int weight_n1 = weight[n1];
          int weight_n2 = weight[n2];
          weight[nNodes] = (int)(((uint)weight_n1 & 0xffffff00u) + ((uint)weight_n2 & 0xffffff00u)) | (1 + (((weight_n1 & 0xff) > (weight_n2 & 0xff)) ? (weight_n1 & 0xff) : (weight_n2 & 0xff)));
          parent[nNodes] = -1;
          nHeap++;
          heap[nHeap] = nNodes;
          tmp = 0;
          zz = nHeap;
          tmp = heap[zz];
          int weight_tmp = weight[tmp];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3973uL);
          while (weight_tmp < weight[heap[zz >> 1]]) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3974uL);
            heap[zz] = heap[zz >> 1];
            zz >>= 1;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3975uL);
          heap[zz] = tmp;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3976uL);
        for (int i = 1; i <= alphaSize; i++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3977uL);
          int j = 0;
          int k = i;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3978uL);
          for (int parent_k; (parent_k = parent[k]) >= 0;) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3979uL);
            k = parent_k;
            j++;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3980uL);
          len[i - 1] = (byte)j;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3981uL);
          if (j > maxLen) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3982uL);
            tooLong = true;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3983uL);
        if (tooLong) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3984uL);
          for (int i = 1; i < alphaSize; i++) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3985uL);
            int j = weight[i] >> 8;
            j = 1 + (j >> 1);
            weight[i] = j << 8;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3986uL);
    }
    private int sendMTFValues1(int nGroups, int alphaSize)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3987uL);
      CompressionState dataShadow = this.cstate;
      int[][] rfreq = dataShadow.sendMTFValues_rfreq;
      int[] fave = dataShadow.sendMTFValues_fave;
      short[] cost = dataShadow.sendMTFValues_cost;
      char[] sfmap = dataShadow.sfmap;
      byte[] selector = dataShadow.selector;
      byte[][] len = dataShadow.sendMTFValues_len;
      byte[] len_0 = len[0];
      byte[] len_1 = len[1];
      byte[] len_2 = len[2];
      byte[] len_3 = len[3];
      byte[] len_4 = len[4];
      byte[] len_5 = len[5];
      int nMTFShadow = this.nMTF;
      int nSelectors = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3988uL);
      for (int iter = 0; iter < BZip2.N_ITERS; iter++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3989uL);
        for (int t = nGroups; --t >= 0;) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3990uL);
          fave[t] = 0;
          int[] rfreqt = rfreq[t];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3991uL);
          for (int i = alphaSize; --i >= 0;) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3992uL);
            rfreqt[i] = 0;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3993uL);
        nSelectors = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3994uL);
        for (int gs = 0; gs < this.nMTF;) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3995uL);
          int ge = Math.Min(gs + BZip2.G_SIZE - 1, nMTFShadow - 1);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3996uL);
          if (nGroups == BZip2.NGroups) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3997uL);
            int[] c = new int[6];
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3998uL);
            for (int i = gs; i <= ge; i++) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3999uL);
              int icv = sfmap[i];
              c[0] += len_0[icv] & 0xff;
              c[1] += len_1[icv] & 0xff;
              c[2] += len_2[icv] & 0xff;
              c[3] += len_3[icv] & 0xff;
              c[4] += len_4[icv] & 0xff;
              c[5] += len_5[icv] & 0xff;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4000uL);
            cost[0] = (short)c[0];
            cost[1] = (short)c[1];
            cost[2] = (short)c[2];
            cost[3] = (short)c[3];
            cost[4] = (short)c[4];
            cost[5] = (short)c[5];
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4001uL);
            for (int t = nGroups; --t >= 0;) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4002uL);
              cost[t] = 0;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4003uL);
            for (int i = gs; i <= ge; i++) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4004uL);
              int icv = sfmap[i];
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4005uL);
              for (int t = nGroups; --t >= 0;) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4006uL);
                cost[t] += (short)(len[t][icv] & 0xff);
              }
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4007uL);
          int bt = -1;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4008uL);
          for (int t = nGroups, bc = 999999999; --t >= 0;) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4009uL);
            int cost_t = cost[t];
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4010uL);
            if (cost_t < bc) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4011uL);
              bc = cost_t;
              bt = t;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4012uL);
          fave[bt]++;
          selector[nSelectors] = (byte)bt;
          nSelectors++;
          int[] rfreq_bt = rfreq[bt];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4013uL);
          for (int i = gs; i <= ge; i++) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4014uL);
            rfreq_bt[sfmap[i]]++;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4015uL);
          gs = ge + 1;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4016uL);
        for (int t = 0; t < nGroups; t++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4017uL);
          hbMakeCodeLengths(len[t], rfreq[t], this.cstate, alphaSize, 20);
        }
      }
      System.Int32 RNTRNTRNT_485 = nSelectors;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4018uL);
      return RNTRNTRNT_485;
    }
    private void sendMTFValues2(int nGroups, int nSelectors)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4019uL);
      CompressionState dataShadow = this.cstate;
      byte[] pos = dataShadow.sendMTFValues2_pos;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4020uL);
      for (int i = nGroups; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4021uL);
        pos[i] = (byte)i;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4022uL);
      for (int i = 0; i < nSelectors; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4023uL);
        byte ll_i = dataShadow.selector[i];
        byte tmp = pos[0];
        int j = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4024uL);
        while (ll_i != tmp) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4025uL);
          j++;
          byte tmp2 = tmp;
          tmp = pos[j];
          pos[j] = tmp2;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4026uL);
        pos[0] = tmp;
        dataShadow.selectorMtf[i] = (byte)j;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4027uL);
    }
    private void sendMTFValues3(int nGroups, int alphaSize)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4028uL);
      int[][] code = this.cstate.sendMTFValues_code;
      byte[][] len = this.cstate.sendMTFValues_len;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4029uL);
      for (int t = 0; t < nGroups; t++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4030uL);
        int minLen = 32;
        int maxLen = 0;
        byte[] len_t = len[t];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4031uL);
        for (int i = alphaSize; --i >= 0;) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4032uL);
          int l = len_t[i] & 0xff;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4033uL);
          if (l > maxLen) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4034uL);
            maxLen = l;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4035uL);
          if (l < minLen) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4036uL);
            minLen = l;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4037uL);
        hbAssignCodes(code[t], len[t], minLen, maxLen, alphaSize);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4038uL);
    }
    private void sendMTFValues4()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4039uL);
      bool[] inUse = this.cstate.inUse;
      bool[] inUse16 = this.cstate.sentMTFValues4_inUse16;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4040uL);
      for (int i = 16; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4041uL);
        inUse16[i] = false;
        int i16 = i * 16;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4042uL);
        for (int j = 16; --j >= 0;) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4043uL);
          if (inUse[i16 + j]) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4044uL);
            inUse16[i] = true;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4045uL);
      uint u = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4046uL);
      for (int i = 0; i < 16; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4047uL);
        if (inUse16[i]) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4048uL);
          u |= 1u << (16 - i - 1);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4049uL);
      this.bw.WriteBits(16, u);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4050uL);
      for (int i = 0; i < 16; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4051uL);
        if (inUse16[i]) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4052uL);
          int i16 = i * 16;
          u = 0;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4053uL);
          for (int j = 0; j < 16; j++) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4054uL);
            if (inUse[i16 + j]) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4055uL);
              u |= 1u << (16 - j - 1);
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4056uL);
          this.bw.WriteBits(16, u);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4057uL);
    }
    private void sendMTFValues5(int nGroups, int nSelectors)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4058uL);
      this.bw.WriteBits(3, (uint)nGroups);
      this.bw.WriteBits(15, (uint)nSelectors);
      byte[] selectorMtf = this.cstate.selectorMtf;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4059uL);
      for (int i = 0; i < nSelectors; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4060uL);
        for (int j = 0, hj = selectorMtf[i] & 0xff; j < hj; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4061uL);
          this.bw.WriteBits(1, 1);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4062uL);
        this.bw.WriteBits(1, 0);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4063uL);
    }
    private void sendMTFValues6(int nGroups, int alphaSize)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4064uL);
      byte[][] len = this.cstate.sendMTFValues_len;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4065uL);
      for (int t = 0; t < nGroups; t++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4066uL);
        byte[] len_t = len[t];
        uint curr = (uint)(len_t[0] & 0xff);
        this.bw.WriteBits(5, curr);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4067uL);
        for (int i = 0; i < alphaSize; i++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4068uL);
          int lti = len_t[i] & 0xff;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4069uL);
          while (curr < lti) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4070uL);
            this.bw.WriteBits(2, 2u);
            curr++;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4071uL);
          while (curr > lti) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4072uL);
            this.bw.WriteBits(2, 3u);
            curr--;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4073uL);
          this.bw.WriteBits(1, 0u);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4074uL);
    }
    private void sendMTFValues7(int nSelectors)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4075uL);
      byte[][] len = this.cstate.sendMTFValues_len;
      int[][] code = this.cstate.sendMTFValues_code;
      byte[] selector = this.cstate.selector;
      char[] sfmap = this.cstate.sfmap;
      int nMTFShadow = this.nMTF;
      int selCtr = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4076uL);
      for (int gs = 0; gs < nMTFShadow;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4077uL);
        int ge = Math.Min(gs + BZip2.G_SIZE - 1, nMTFShadow - 1);
        int ix = selector[selCtr] & 0xff;
        int[] code_selCtr = code[ix];
        byte[] len_selCtr = len[ix];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4078uL);
        while (gs <= ge) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4079uL);
          int sfmap_i = sfmap[gs];
          int n = len_selCtr[sfmap_i] & 0xff;
          this.bw.WriteBits(n, (uint)code_selCtr[sfmap_i]);
          gs++;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4080uL);
        gs = ge + 1;
        selCtr++;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4081uL);
    }
    private void moveToFrontCodeAndSend()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4082uL);
      this.bw.WriteBits(24, (uint)this.origPtr);
      generateMTFValues();
      sendMTFValues();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4083uL);
    }
    private class CompressionState
    {
      public readonly bool[] inUse = new bool[256];
      public readonly byte[] unseqToSeq = new byte[256];
      public readonly int[] mtfFreq = new int[BZip2.MaxAlphaSize];
      public readonly byte[] selector = new byte[BZip2.MaxSelectors];
      public readonly byte[] selectorMtf = new byte[BZip2.MaxSelectors];
      public readonly byte[] generateMTFValues_yy = new byte[256];
      public byte[][] sendMTFValues_len;
      public int[][] sendMTFValues_rfreq;
      public readonly int[] sendMTFValues_fave = new int[BZip2.NGroups];
      public readonly short[] sendMTFValues_cost = new short[BZip2.NGroups];
      public int[][] sendMTFValues_code;
      public readonly byte[] sendMTFValues2_pos = new byte[BZip2.NGroups];
      public readonly bool[] sentMTFValues4_inUse16 = new bool[16];
      public readonly int[] stack_ll = new int[BZip2.QSORT_STACK_SIZE];
      public readonly int[] stack_hh = new int[BZip2.QSORT_STACK_SIZE];
      public readonly int[] stack_dd = new int[BZip2.QSORT_STACK_SIZE];
      public readonly int[] mainSort_runningOrder = new int[256];
      public readonly int[] mainSort_copy = new int[256];
      public readonly bool[] mainSort_bigDone = new bool[256];
      public int[] heap = new int[BZip2.MaxAlphaSize + 2];
      public int[] weight = new int[BZip2.MaxAlphaSize * 2];
      public int[] parent = new int[BZip2.MaxAlphaSize * 2];
      public readonly int[] ftab = new int[65537];
      public byte[] block;
      public int[] fmap;
      public char[] sfmap;
      public char[] quadrant;
      public CompressionState(int blockSize100k)
      {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4084uL);
        int n = blockSize100k * BZip2.BlockSizeMultiple;
        this.block = new byte[(n + 1 + BZip2.NUM_OVERSHOOT_BYTES)];
        this.fmap = new int[n];
        this.sfmap = new char[2 * n];
        this.quadrant = this.sfmap;
        this.sendMTFValues_len = BZip2.InitRectangularArray<byte>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.sendMTFValues_rfreq = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.sendMTFValues_code = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4085uL);
      }
    }
  }
}
