using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;
namespace Ionic.BZip2
{
  internal class WorkItem
  {
    public int index;
    public BZip2Compressor Compressor { get; private set; }
    public MemoryStream ms;
    public int ordinal;
    public BitWriter bw;
    public WorkItem(int ix, int blockSize)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4498uL);
      this.ms = new MemoryStream();
      this.bw = new BitWriter(ms);
      this.Compressor = new BZip2Compressor(bw, blockSize);
      this.index = ix;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4499uL);
    }
  }
  public class ParallelBZip2OutputStream : System.IO.Stream
  {
    private static readonly int BufferPairsPerCore = 4;
    private int _maxWorkers;
    private bool firstWriteDone;
    private int lastFilled;
    private int lastWritten;
    private int latestCompressed;
    private int currentlyFilling;
    private volatile Exception pendingException;
    private bool handlingException;
    private bool emitting;
    private System.Collections.Generic.Queue<int> toWrite;
    private System.Collections.Generic.Queue<int> toFill;
    private System.Collections.Generic.List<WorkItem> pool;
    private object latestLock = new object();
    private object eLock = new object();
    private object outputLock = new object();
    private AutoResetEvent newlyCompressedBlob;
    long totalBytesWrittenIn;
    long totalBytesWrittenOut;
    bool leaveOpen;
    uint combinedCRC;
    Stream output;
    BitWriter bw;
    int blockSize100k;
    private TraceBits desiredTrace = TraceBits.Crc | TraceBits.Write;
    public ParallelBZip2OutputStream(Stream output) : this(output, BZip2.MaxBlockSize, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4500uL);
    }
    public ParallelBZip2OutputStream(Stream output, int blockSize) : this(output, blockSize, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4501uL);
    }
    public ParallelBZip2OutputStream(Stream output, bool leaveOpen) : this(output, BZip2.MaxBlockSize, leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4502uL);
    }
    public ParallelBZip2OutputStream(Stream output, int blockSize, bool leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4503uL);
      if (blockSize < BZip2.MinBlockSize || blockSize > BZip2.MaxBlockSize) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4504uL);
        var msg = String.Format("blockSize={0} is out of range; must be between {1} and {2}", blockSize, BZip2.MinBlockSize, BZip2.MaxBlockSize);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4505uL);
        throw new ArgumentException(msg, "blockSize");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4506uL);
      this.output = output;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4507uL);
      if (!this.output.CanWrite) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4508uL);
        throw new ArgumentException("The stream is not writable.", "output");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4509uL);
      this.bw = new BitWriter(this.output);
      this.blockSize100k = blockSize;
      this.leaveOpen = leaveOpen;
      this.combinedCRC = 0;
      this.MaxWorkers = 16;
      EmitHeader();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4510uL);
    }
    private void InitializePoolOfWorkItems()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4511uL);
      this.toWrite = new Queue<int>();
      this.toFill = new Queue<int>();
      this.pool = new System.Collections.Generic.List<WorkItem>();
      int nWorkers = BufferPairsPerCore * Environment.ProcessorCount;
      nWorkers = Math.Min(nWorkers, this.MaxWorkers);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4512uL);
      for (int i = 0; i < nWorkers; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4513uL);
        this.pool.Add(new WorkItem(i, this.blockSize100k));
        this.toFill.Enqueue(i);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4514uL);
      this.newlyCompressedBlob = new AutoResetEvent(false);
      this.currentlyFilling = -1;
      this.lastFilled = -1;
      this.lastWritten = -1;
      this.latestCompressed = -1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4515uL);
    }
    public int MaxWorkers {
      get {
        System.Int32 RNTRNTRNT_505 = _maxWorkers;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4516uL);
        return RNTRNTRNT_505;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4517uL);
        if (value < 4) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4518uL);
          throw new ArgumentException("MaxWorkers", "Value must be 4 or greater.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4519uL);
        _maxWorkers = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4520uL);
      }
    }
    public override void Close()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4521uL);
      if (this.pendingException != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4522uL);
        this.handlingException = true;
        var pe = this.pendingException;
        this.pendingException = null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4523uL);
        throw pe;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4524uL);
      if (this.handlingException) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4525uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4526uL);
      if (output == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4527uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4528uL);
      Stream o = this.output;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4529uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4530uL);
        FlushOutput(true);
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4531uL);
        this.output = null;
        this.bw = null;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4532uL);
      if (!leaveOpen) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4533uL);
        o.Close();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4534uL);
    }
    private void FlushOutput(bool lastInput)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4535uL);
      if (this.emitting) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4536uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4537uL);
      if (this.currentlyFilling >= 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4538uL);
        WorkItem workitem = this.pool[this.currentlyFilling];
        CompressOne(workitem);
        this.currentlyFilling = -1;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4539uL);
      if (lastInput) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4540uL);
        EmitPendingBuffers(true, false);
        EmitTrailer();
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4541uL);
        EmitPendingBuffers(false, false);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4542uL);
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4543uL);
      if (this.output != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4544uL);
        FlushOutput(false);
        this.bw.Flush();
        this.output.Flush();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4545uL);
    }
    private void EmitHeader()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4546uL);
      var magic = new byte[] {
        (byte)'B',
        (byte)'Z',
        (byte)'h',
        (byte)('0' + this.blockSize100k)
      };
      this.output.Write(magic, 0, magic.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4547uL);
    }
    private void EmitTrailer()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4548uL);
      TraceOutput(TraceBits.Write, "total written out: {0} (0x{0:X})", this.bw.TotalBytesWrittenOut);
      this.bw.WriteByte(0x17);
      this.bw.WriteByte(0x72);
      this.bw.WriteByte(0x45);
      this.bw.WriteByte(0x38);
      this.bw.WriteByte(0x50);
      this.bw.WriteByte(0x90);
      this.bw.WriteInt(this.combinedCRC);
      this.bw.FinishAndPad();
      TraceOutput(TraceBits.Write, "final total : {0} (0x{0:X})", this.bw.TotalBytesWrittenOut);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4549uL);
    }
    public int BlockSize {
      get {
        System.Int32 RNTRNTRNT_506 = this.blockSize100k;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4550uL);
        return RNTRNTRNT_506;
      }
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4551uL);
      bool mustWait = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4552uL);
      if (this.output == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4553uL);
        throw new IOException("the stream is not open");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4554uL);
      if (this.pendingException != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4555uL);
        this.handlingException = true;
        var pe = this.pendingException;
        this.pendingException = null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4556uL);
        throw pe;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4557uL);
      if (offset < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4558uL);
        throw new IndexOutOfRangeException(String.Format("offset ({0}) must be > 0", offset));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4559uL);
      if (count < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4560uL);
        throw new IndexOutOfRangeException(String.Format("count ({0}) must be > 0", count));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4561uL);
      if (offset + count > buffer.Length) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4562uL);
        throw new IndexOutOfRangeException(String.Format("offset({0}) count({1}) bLength({2})", offset, count, buffer.Length));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4563uL);
      if (count == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4564uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4565uL);
      if (!this.firstWriteDone) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4566uL);
        InitializePoolOfWorkItems();
        this.firstWriteDone = true;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4567uL);
      int bytesWritten = 0;
      int bytesRemaining = count;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4568uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4569uL);
        EmitPendingBuffers(false, mustWait);
        mustWait = false;
        int ix = -1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4570uL);
        if (this.currentlyFilling >= 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4571uL);
          ix = this.currentlyFilling;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4572uL);
          if (this.toFill.Count == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4573uL);
            mustWait = true;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4574uL);
            continue;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4575uL);
          ix = this.toFill.Dequeue();
          ++this.lastFilled;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4576uL);
        WorkItem workitem = this.pool[ix];
        workitem.ordinal = this.lastFilled;
        int n = workitem.Compressor.Fill(buffer, offset, bytesRemaining);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4577uL);
        if (n != bytesRemaining) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4578uL);
          if (!ThreadPool.QueueUserWorkItem(CompressOne, workitem)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4579uL);
            throw new Exception("Cannot enqueue workitem");
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4580uL);
          this.currentlyFilling = -1;
          offset += n;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4581uL);
          this.currentlyFilling = ix;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4582uL);
        bytesRemaining -= n;
        bytesWritten += n;
      } while (bytesRemaining > 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4583uL);
      totalBytesWrittenIn += bytesWritten;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4584uL);
      return;
    }
    private void EmitPendingBuffers(bool doAll, bool mustWait)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4585uL);
      if (emitting) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4586uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4587uL);
      emitting = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4588uL);
      if (doAll || mustWait) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4589uL);
        this.newlyCompressedBlob.WaitOne();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4590uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4591uL);
        int firstSkip = -1;
        int millisecondsToWait = doAll ? 200 : (mustWait ? -1 : 0);
        int nextToWrite = -1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4592uL);
        do {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4593uL);
          if (Monitor.TryEnter(this.toWrite, millisecondsToWait)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4594uL);
            nextToWrite = -1;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4595uL);
            try {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4596uL);
              if (this.toWrite.Count > 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4597uL);
                nextToWrite = this.toWrite.Dequeue();
              }
            } finally {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4598uL);
              Monitor.Exit(this.toWrite);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4599uL);
            if (nextToWrite >= 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4600uL);
              WorkItem workitem = this.pool[nextToWrite];
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4601uL);
              if (workitem.ordinal != this.lastWritten + 1) {
                lock (this.toWrite) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4602uL);
                  this.toWrite.Enqueue(nextToWrite);
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4603uL);
                if (firstSkip == nextToWrite) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4604uL);
                  this.newlyCompressedBlob.WaitOne();
                  firstSkip = -1;
                } else if (firstSkip == -1) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4605uL);
                  firstSkip = nextToWrite;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4606uL);
                continue;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4607uL);
              firstSkip = -1;
              TraceOutput(TraceBits.Write, "Writing block {0}", workitem.ordinal);
              var bw2 = workitem.bw;
              bw2.Flush();
              var ms = workitem.ms;
              ms.Seek(0, SeekOrigin.Begin);
              int n;
              int y = -1;
              long totOut = 0;
              var buffer = new byte[1024];
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4608uL);
              while ((n = ms.Read(buffer, 0, buffer.Length)) > 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4609uL);
                y = n;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4610uL);
                for (int k = 0; k < n; k++) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4611uL);
                  this.bw.WriteByte(buffer[k]);
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4612uL);
                totOut += n;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4613uL);
              TraceOutput(TraceBits.Write, " remaining bits: {0} 0x{1:X}", bw2.NumRemainingBits, bw2.RemainingBits);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4614uL);
              if (bw2.NumRemainingBits > 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4615uL);
                this.bw.WriteBits(bw2.NumRemainingBits, bw2.RemainingBits);
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4616uL);
              TraceOutput(TraceBits.Crc, " combined CRC (before): {0:X8}", this.combinedCRC);
              this.combinedCRC = (this.combinedCRC << 1) | (this.combinedCRC >> 31);
              this.combinedCRC ^= (uint)workitem.Compressor.Crc32;
              TraceOutput(TraceBits.Crc, " block    CRC         : {0:X8}", workitem.Compressor.Crc32);
              TraceOutput(TraceBits.Crc, " combined CRC (after) : {0:X8}", this.combinedCRC);
              TraceOutput(TraceBits.Write, "total written out: {0} (0x{0:X})", this.bw.TotalBytesWrittenOut);
              TraceOutput(TraceBits.Write | TraceBits.Crc, "");
              this.totalBytesWrittenOut += totOut;
              bw2.Reset();
              this.lastWritten = workitem.ordinal;
              workitem.ordinal = -1;
              this.toFill.Enqueue(workitem.index);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4617uL);
              if (millisecondsToWait == -1) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4618uL);
                millisecondsToWait = 0;
              }
            }
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4619uL);
            nextToWrite = -1;
          }
        } while (nextToWrite >= 0);
      } while (doAll && (this.lastWritten != this.latestCompressed));
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4620uL);
      if (doAll) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4621uL);
        TraceOutput(TraceBits.Crc, " combined CRC (final) : {0:X8}", this.combinedCRC);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4622uL);
      emitting = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4623uL);
    }
    private void CompressOne(Object wi)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4624uL);
      WorkItem workitem = (WorkItem)wi;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4625uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4626uL);
        workitem.Compressor.CompressAndWrite();
        lock (this.latestLock) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4627uL);
          if (workitem.ordinal > this.latestCompressed) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4628uL);
            this.latestCompressed = workitem.ordinal;
          }
        }
        lock (this.toWrite) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4629uL);
          this.toWrite.Enqueue(workitem.index);
        }
        this.newlyCompressedBlob.Set();
      } catch (System.Exception exc1) {
        lock (this.eLock) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4630uL);
          if (this.pendingException != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4631uL);
            this.pendingException = exc1;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4632uL);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_507 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4633uL);
        return RNTRNTRNT_507;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_508 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4634uL);
        return RNTRNTRNT_508;
      }
    }
    public override bool CanWrite {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4635uL);
        if (this.output == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4636uL);
          throw new ObjectDisposedException("BZip2Stream");
        }
        System.Boolean RNTRNTRNT_509 = this.output.CanWrite;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4637uL);
        return RNTRNTRNT_509;
      }
    }
    public override long Length {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4638uL);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_510 = this.totalBytesWrittenIn;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4639uL);
        return RNTRNTRNT_510;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4640uL);
        throw new NotImplementedException();
      }
    }
    public Int64 BytesWrittenOut {
      get {
        Int64 RNTRNTRNT_511 = totalBytesWrittenOut;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4641uL);
        return RNTRNTRNT_511;
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4642uL);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4643uL);
      throw new NotImplementedException();
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4644uL);
      throw new NotImplementedException();
    }
    [Flags()]
    enum TraceBits : uint
    {
      None = 0,
      Crc = 1,
      Write = 2,
      All = 0xffffffffu
    }
    [System.Diagnostics.ConditionalAttribute("Trace")]
    private void TraceOutput(TraceBits bits, string format, params object[] varParams)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4645uL);
      if ((bits & this.desiredTrace) != 0) {
        lock (outputLock) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4646uL);
          int tid = Thread.CurrentThread.GetHashCode();
          Console.ForegroundColor = (ConsoleColor)(tid % 8 + 10);
          Console.Write("{0:000} PBOS ", tid);
          Console.WriteLine(format, varParams);
          Console.ResetColor();
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4647uL);
    }
  }
}
