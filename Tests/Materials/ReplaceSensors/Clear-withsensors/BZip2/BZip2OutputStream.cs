using System;
using System.IO;
namespace Ionic.BZip2
{
  public class BZip2OutputStream : System.IO.Stream
  {
    int totalBytesWrittenIn;
    bool leaveOpen;
    BZip2Compressor compressor;
    uint combinedCRC;
    Stream output;
    BitWriter bw;
    int blockSize100k;
    private TraceBits desiredTrace = TraceBits.Crc | TraceBits.Write;
    public BZip2OutputStream(Stream output) : this(output, BZip2.MaxBlockSize, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4437uL);
    }
    public BZip2OutputStream(Stream output, int blockSize) : this(output, blockSize, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4438uL);
    }
    public BZip2OutputStream(Stream output, bool leaveOpen) : this(output, BZip2.MaxBlockSize, leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4439uL);
    }
    public BZip2OutputStream(Stream output, int blockSize, bool leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4440uL);
      if (blockSize < BZip2.MinBlockSize || blockSize > BZip2.MaxBlockSize) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4441uL);
        var msg = String.Format("blockSize={0} is out of range; must be between {1} and {2}", blockSize, BZip2.MinBlockSize, BZip2.MaxBlockSize);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4442uL);
        throw new ArgumentException(msg, "blockSize");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4443uL);
      this.output = output;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4444uL);
      if (!this.output.CanWrite) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4445uL);
        throw new ArgumentException("The stream is not writable.", "output");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4446uL);
      this.bw = new BitWriter(this.output);
      this.blockSize100k = blockSize;
      this.compressor = new BZip2Compressor(this.bw, blockSize);
      this.leaveOpen = leaveOpen;
      this.combinedCRC = 0;
      EmitHeader();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4447uL);
    }
    public override void Close()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4448uL);
      if (output != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4449uL);
        Stream o = this.output;
        Finish();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4450uL);
        if (!leaveOpen) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4451uL);
          o.Close();
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4452uL);
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4453uL);
      if (this.output != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4454uL);
        this.bw.Flush();
        this.output.Flush();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4455uL);
    }
    private void EmitHeader()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4456uL);
      var magic = new byte[] {
        (byte)'B',
        (byte)'Z',
        (byte)'h',
        (byte)('0' + this.blockSize100k)
      };
      this.output.Write(magic, 0, magic.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4457uL);
    }
    private void EmitTrailer()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4458uL);
      TraceOutput(TraceBits.Write, "total written out: {0} (0x{0:X})", this.bw.TotalBytesWrittenOut);
      this.bw.WriteByte(0x17);
      this.bw.WriteByte(0x72);
      this.bw.WriteByte(0x45);
      this.bw.WriteByte(0x38);
      this.bw.WriteByte(0x50);
      this.bw.WriteByte(0x90);
      this.bw.WriteInt(this.combinedCRC);
      this.bw.FinishAndPad();
      TraceOutput(TraceBits.Write, "final total: {0} (0x{0:X})", this.bw.TotalBytesWrittenOut);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4459uL);
    }
    void Finish()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4460uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4461uL);
        var totalBefore = this.bw.TotalBytesWrittenOut;
        this.compressor.CompressAndWrite();
        TraceOutput(TraceBits.Write, "out block length (bytes): {0} (0x{0:X})", this.bw.TotalBytesWrittenOut - totalBefore);
        TraceOutput(TraceBits.Crc, " combined CRC (before): {0:X8}", this.combinedCRC);
        this.combinedCRC = (this.combinedCRC << 1) | (this.combinedCRC >> 31);
        this.combinedCRC ^= (uint)compressor.Crc32;
        TraceOutput(TraceBits.Crc, " block    CRC         : {0:X8}", this.compressor.Crc32);
        TraceOutput(TraceBits.Crc, " combined CRC (final) : {0:X8}", this.combinedCRC);
        EmitTrailer();
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4462uL);
        this.output = null;
        this.compressor = null;
        this.bw = null;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4463uL);
    }
    public int BlockSize {
      get {
        System.Int32 RNTRNTRNT_500 = this.blockSize100k;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4464uL);
        return RNTRNTRNT_500;
      }
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4465uL);
      if (offset < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4466uL);
        throw new IndexOutOfRangeException(String.Format("offset ({0}) must be > 0", offset));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4467uL);
      if (count < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4468uL);
        throw new IndexOutOfRangeException(String.Format("count ({0}) must be > 0", count));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4469uL);
      if (offset + count > buffer.Length) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4470uL);
        throw new IndexOutOfRangeException(String.Format("offset({0}) count({1}) bLength({2})", offset, count, buffer.Length));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4471uL);
      if (this.output == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4472uL);
        throw new IOException("the stream is not open");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4473uL);
      if (count == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4474uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4475uL);
      int bytesWritten = 0;
      int bytesRemaining = count;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4476uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4477uL);
        int n = compressor.Fill(buffer, offset, bytesRemaining);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4478uL);
        if (n != bytesRemaining) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4479uL);
          var totalBefore = this.bw.TotalBytesWrittenOut;
          this.compressor.CompressAndWrite();
          TraceOutput(TraceBits.Write, "out block length (bytes): {0} (0x{0:X})", this.bw.TotalBytesWrittenOut - totalBefore);
          TraceOutput(TraceBits.Write, " remaining: {0} 0x{1:X}", this.bw.NumRemainingBits, this.bw.RemainingBits);
          TraceOutput(TraceBits.Crc, " combined CRC (before): {0:X8}", this.combinedCRC);
          this.combinedCRC = (this.combinedCRC << 1) | (this.combinedCRC >> 31);
          this.combinedCRC ^= (uint)compressor.Crc32;
          TraceOutput(TraceBits.Crc, " block    CRC         : {0:X8}", compressor.Crc32);
          TraceOutput(TraceBits.Crc, " combined CRC (after) : {0:X8}", this.combinedCRC);
          offset += n;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4480uL);
        bytesRemaining -= n;
        bytesWritten += n;
      } while (bytesRemaining > 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4481uL);
      totalBytesWrittenIn += bytesWritten;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4482uL);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_501 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4483uL);
        return RNTRNTRNT_501;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_502 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4484uL);
        return RNTRNTRNT_502;
      }
    }
    public override bool CanWrite {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4485uL);
        if (this.output == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4486uL);
          throw new ObjectDisposedException("BZip2Stream");
        }
        System.Boolean RNTRNTRNT_503 = this.output.CanWrite;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4487uL);
        return RNTRNTRNT_503;
      }
    }
    public override long Length {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4488uL);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_504 = this.totalBytesWrittenIn;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4489uL);
        return RNTRNTRNT_504;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4490uL);
        throw new NotImplementedException();
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4491uL);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4492uL);
      throw new NotImplementedException();
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4493uL);
      throw new NotImplementedException();
    }
    [Flags()]
    enum TraceBits : uint
    {
      None = 0,
      Crc = 1,
      Write = 2,
      All = 0xffffffffu
    }
    [System.Diagnostics.ConditionalAttribute("Trace")]
    private void TraceOutput(TraceBits bits, string format, params object[] varParams)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4494uL);
      if ((bits & this.desiredTrace) != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4496uL);
        {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4495uL);
          int tid = System.Threading.Thread.CurrentThread.GetHashCode();
          Console.ForegroundColor = (ConsoleColor)(tid % 8 + 10);
          Console.Write("{0:000} PBOS ", tid);
          Console.WriteLine(format, varParams);
          Console.ResetColor();
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4497uL);
    }
  }
}
