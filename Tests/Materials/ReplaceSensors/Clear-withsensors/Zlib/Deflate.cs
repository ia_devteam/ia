using System;
namespace Ionic.Zlib
{
  internal enum BlockState
  {
    NeedMore = 0,
    BlockDone,
    FinishStarted,
    FinishDone
  }
  internal enum DeflateFlavor
  {
    Store,
    Fast,
    Slow
  }
  internal sealed class DeflateManager
  {
    private static readonly int MEM_LEVEL_MAX = 9;
    private static readonly int MEM_LEVEL_DEFAULT = 8;
    internal delegate BlockState CompressFunc(FlushType flush);
    internal class Config
    {
      internal int GoodLength;
      internal int MaxLazy;
      internal int NiceLength;
      internal int MaxChainLength;
      internal DeflateFlavor Flavor;
      private Config(int goodLength, int maxLazy, int niceLength, int maxChainLength, DeflateFlavor flavor)
      {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4649uL);
        this.GoodLength = goodLength;
        this.MaxLazy = maxLazy;
        this.NiceLength = niceLength;
        this.MaxChainLength = maxChainLength;
        this.Flavor = flavor;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4650uL);
      }
      public static Config Lookup(CompressionLevel level)
      {
        Config RNTRNTRNT_513 = Table[(int)level];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4651uL);
        return RNTRNTRNT_513;
      }
      static Config()
      {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4652uL);
        Table = new Config[] {
          new Config(0, 0, 0, 0, DeflateFlavor.Store),
          new Config(4, 4, 8, 4, DeflateFlavor.Fast),
          new Config(4, 5, 16, 8, DeflateFlavor.Fast),
          new Config(4, 6, 32, 32, DeflateFlavor.Fast),
          new Config(4, 4, 16, 16, DeflateFlavor.Slow),
          new Config(8, 16, 32, 32, DeflateFlavor.Slow),
          new Config(8, 16, 128, 128, DeflateFlavor.Slow),
          new Config(8, 32, 128, 256, DeflateFlavor.Slow),
          new Config(32, 128, 258, 1024, DeflateFlavor.Slow),
          new Config(32, 258, 258, 4096, DeflateFlavor.Slow)
        };
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4653uL);
      }
      private static readonly Config[] Table;
    }
    private CompressFunc DeflateFunction;
    private static readonly System.String[] _ErrorMessage = new System.String[] {
      "need dictionary",
      "stream end",
      "",
      "file error",
      "stream error",
      "data error",
      "insufficient memory",
      "buffer error",
      "incompatible version",
      ""
    };
    private static readonly int PRESET_DICT = 0x20;
    private static readonly int INIT_STATE = 42;
    private static readonly int BUSY_STATE = 113;
    private static readonly int FINISH_STATE = 666;
    private static readonly int Z_DEFLATED = 8;
    private static readonly int STORED_BLOCK = 0;
    private static readonly int STATIC_TREES = 1;
    private static readonly int DYN_TREES = 2;
    private static readonly int Z_BINARY = 0;
    private static readonly int Z_ASCII = 1;
    private static readonly int Z_UNKNOWN = 2;
    private static readonly int Buf_size = 8 * 2;
    private static readonly int MIN_MATCH = 3;
    private static readonly int MAX_MATCH = 258;
    private static readonly int MIN_LOOKAHEAD = (MAX_MATCH + MIN_MATCH + 1);
    private static readonly int HEAP_SIZE = (2 * InternalConstants.L_CODES + 1);
    private static readonly int END_BLOCK = 256;
    internal ZlibCodec _codec;
    internal int status;
    internal byte[] pending;
    internal int nextPending;
    internal int pendingCount;
    internal sbyte data_type;
    internal int last_flush;
    internal int w_size;
    internal int w_bits;
    internal int w_mask;
    internal byte[] window;
    internal int window_size;
    internal short[] prev;
    internal short[] head;
    internal int ins_h;
    internal int hash_size;
    internal int hash_bits;
    internal int hash_mask;
    internal int hash_shift;
    internal int block_start;
    Config config;
    internal int match_length;
    internal int prev_match;
    internal int match_available;
    internal int strstart;
    internal int match_start;
    internal int lookahead;
    internal int prev_length;
    internal CompressionLevel compressionLevel;
    internal CompressionStrategy compressionStrategy;
    internal short[] dyn_ltree;
    internal short[] dyn_dtree;
    internal short[] bl_tree;
    internal Tree treeLiterals = new Tree();
    internal Tree treeDistances = new Tree();
    internal Tree treeBitLengths = new Tree();
    internal short[] bl_count = new short[InternalConstants.MAX_BITS + 1];
    internal int[] heap = new int[2 * InternalConstants.L_CODES + 1];
    internal int heap_len;
    internal int heap_max;
    internal sbyte[] depth = new sbyte[2 * InternalConstants.L_CODES + 1];
    internal int _lengthOffset;
    internal int lit_bufsize;
    internal int last_lit;
    internal int _distanceOffset;
    internal int opt_len;
    internal int static_len;
    internal int matches;
    internal int last_eob_len;
    internal short bi_buf;
    internal int bi_valid;
    internal DeflateManager()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4654uL);
      dyn_ltree = new short[HEAP_SIZE * 2];
      dyn_dtree = new short[(2 * InternalConstants.D_CODES + 1) * 2];
      bl_tree = new short[(2 * InternalConstants.BL_CODES + 1) * 2];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4655uL);
    }
    private void _InitializeLazyMatch()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4656uL);
      window_size = 2 * w_size;
      Array.Clear(head, 0, hash_size);
      config = Config.Lookup(compressionLevel);
      SetDeflater();
      strstart = 0;
      block_start = 0;
      lookahead = 0;
      match_length = prev_length = MIN_MATCH - 1;
      match_available = 0;
      ins_h = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4657uL);
    }
    private void _InitializeTreeData()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4658uL);
      treeLiterals.dyn_tree = dyn_ltree;
      treeLiterals.staticTree = StaticTree.Literals;
      treeDistances.dyn_tree = dyn_dtree;
      treeDistances.staticTree = StaticTree.Distances;
      treeBitLengths.dyn_tree = bl_tree;
      treeBitLengths.staticTree = StaticTree.BitLengths;
      bi_buf = 0;
      bi_valid = 0;
      last_eob_len = 8;
      _InitializeBlocks();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4659uL);
    }
    internal void _InitializeBlocks()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4660uL);
      for (int i = 0; i < InternalConstants.L_CODES; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4661uL);
        dyn_ltree[i * 2] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4662uL);
      for (int i = 0; i < InternalConstants.D_CODES; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4663uL);
        dyn_dtree[i * 2] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4664uL);
      for (int i = 0; i < InternalConstants.BL_CODES; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4665uL);
        bl_tree[i * 2] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4666uL);
      dyn_ltree[END_BLOCK * 2] = 1;
      opt_len = static_len = 0;
      last_lit = matches = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4667uL);
    }
    internal void pqdownheap(short[] tree, int k)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4668uL);
      int v = heap[k];
      int j = k << 1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4669uL);
      while (j <= heap_len) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4670uL);
        if (j < heap_len && _IsSmaller(tree, heap[j + 1], heap[j], depth)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4671uL);
          j++;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4672uL);
        if (_IsSmaller(tree, v, heap[j], depth)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4673uL);
          break;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4674uL);
        heap[k] = heap[j];
        k = j;
        j <<= 1;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4675uL);
      heap[k] = v;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4676uL);
    }
    static internal bool _IsSmaller(short[] tree, int n, int m, sbyte[] depth)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4677uL);
      short tn2 = tree[n * 2];
      short tm2 = tree[m * 2];
      System.Boolean RNTRNTRNT_514 = (tn2 < tm2 || (tn2 == tm2 && depth[n] <= depth[m]));
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4678uL);
      return RNTRNTRNT_514;
    }
    internal void scan_tree(short[] tree, int max_code)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4679uL);
      int n;
      int prevlen = -1;
      int curlen;
      int nextlen = (int)tree[0 * 2 + 1];
      int count = 0;
      int max_count = 7;
      int min_count = 4;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4680uL);
      if (nextlen == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4681uL);
        max_count = 138;
        min_count = 3;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4682uL);
      tree[(max_code + 1) * 2 + 1] = (short)0x7fff;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4683uL);
      for (n = 0; n <= max_code; n++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4684uL);
        curlen = nextlen;
        nextlen = (int)tree[(n + 1) * 2 + 1];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4685uL);
        if (++count < max_count && curlen == nextlen) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4686uL);
          continue;
        } else if (count < min_count) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4692uL);
          bl_tree[curlen * 2] = (short)(bl_tree[curlen * 2] + count);
        } else if (curlen != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4689uL);
          if (curlen != prevlen) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4690uL);
            bl_tree[curlen * 2]++;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4691uL);
          bl_tree[InternalConstants.REP_3_6 * 2]++;
        } else if (count <= 10) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4688uL);
          bl_tree[InternalConstants.REPZ_3_10 * 2]++;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4687uL);
          bl_tree[InternalConstants.REPZ_11_138 * 2]++;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4693uL);
        count = 0;
        prevlen = curlen;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4694uL);
        if (nextlen == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4695uL);
          max_count = 138;
          min_count = 3;
        } else if (curlen == nextlen) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4697uL);
          max_count = 6;
          min_count = 3;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4696uL);
          max_count = 7;
          min_count = 4;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4698uL);
    }
    internal int build_bl_tree()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4699uL);
      int max_blindex;
      scan_tree(dyn_ltree, treeLiterals.max_code);
      scan_tree(dyn_dtree, treeDistances.max_code);
      treeBitLengths.build_tree(this);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4700uL);
      for (max_blindex = InternalConstants.BL_CODES - 1; max_blindex >= 3; max_blindex--) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4701uL);
        if (bl_tree[Tree.bl_order[max_blindex] * 2 + 1] != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4702uL);
          break;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4703uL);
      opt_len += 3 * (max_blindex + 1) + 5 + 5 + 4;
      System.Int32 RNTRNTRNT_515 = max_blindex;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4704uL);
      return RNTRNTRNT_515;
    }
    internal void send_all_trees(int lcodes, int dcodes, int blcodes)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4705uL);
      int rank;
      send_bits(lcodes - 257, 5);
      send_bits(dcodes - 1, 5);
      send_bits(blcodes - 4, 4);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4706uL);
      for (rank = 0; rank < blcodes; rank++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4707uL);
        send_bits(bl_tree[Tree.bl_order[rank] * 2 + 1], 3);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4708uL);
      send_tree(dyn_ltree, lcodes - 1);
      send_tree(dyn_dtree, dcodes - 1);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4709uL);
    }
    internal void send_tree(short[] tree, int max_code)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4710uL);
      int n;
      int prevlen = -1;
      int curlen;
      int nextlen = tree[0 * 2 + 1];
      int count = 0;
      int max_count = 7;
      int min_count = 4;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4711uL);
      if (nextlen == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4712uL);
        max_count = 138;
        min_count = 3;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4713uL);
      for (n = 0; n <= max_code; n++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4714uL);
        curlen = nextlen;
        nextlen = tree[(n + 1) * 2 + 1];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4715uL);
        if (++count < max_count && curlen == nextlen) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4716uL);
          continue;
        } else if (count < min_count) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4722uL);
          do {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4723uL);
            send_code(curlen, bl_tree);
          } while (--count != 0);
        } else if (curlen != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4719uL);
          if (curlen != prevlen) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4720uL);
            send_code(curlen, bl_tree);
            count--;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4721uL);
          send_code(InternalConstants.REP_3_6, bl_tree);
          send_bits(count - 3, 2);
        } else if (count <= 10) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4718uL);
          send_code(InternalConstants.REPZ_3_10, bl_tree);
          send_bits(count - 3, 3);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4717uL);
          send_code(InternalConstants.REPZ_11_138, bl_tree);
          send_bits(count - 11, 7);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4724uL);
        count = 0;
        prevlen = curlen;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4725uL);
        if (nextlen == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4726uL);
          max_count = 138;
          min_count = 3;
        } else if (curlen == nextlen) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4728uL);
          max_count = 6;
          min_count = 3;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4727uL);
          max_count = 7;
          min_count = 4;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4729uL);
    }
    private void put_bytes(byte[] p, int start, int len)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4730uL);
      Array.Copy(p, start, pending, pendingCount, len);
      pendingCount += len;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4731uL);
    }
    internal void send_code(int c, short[] tree)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4732uL);
      int c2 = c * 2;
      send_bits((tree[c2] & 0xffff), (tree[c2 + 1] & 0xffff));
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4733uL);
    }
    internal void send_bits(int value, int length)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4734uL);
      int len = length;
      unchecked {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4735uL);
        if (bi_valid > (int)Buf_size - len) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4736uL);
          bi_buf |= (short)((value << bi_valid) & 0xffff);
          pending[pendingCount++] = (byte)bi_buf;
          pending[pendingCount++] = (byte)(bi_buf >> 8);
          bi_buf = (short)((uint)value >> (Buf_size - bi_valid));
          bi_valid += len - Buf_size;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4737uL);
          bi_buf |= (short)((value << bi_valid) & 0xffff);
          bi_valid += len;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4738uL);
    }
    internal void _tr_align()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4739uL);
      send_bits(STATIC_TREES << 1, 3);
      send_code(END_BLOCK, StaticTree.lengthAndLiteralsTreeCodes);
      bi_flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4740uL);
      if (1 + last_eob_len + 10 - bi_valid < 9) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4741uL);
        send_bits(STATIC_TREES << 1, 3);
        send_code(END_BLOCK, StaticTree.lengthAndLiteralsTreeCodes);
        bi_flush();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4742uL);
      last_eob_len = 7;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4743uL);
    }
    internal bool _tr_tally(int dist, int lc)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4744uL);
      pending[_distanceOffset + last_lit * 2] = unchecked((byte)((uint)dist >> 8));
      pending[_distanceOffset + last_lit * 2 + 1] = unchecked((byte)dist);
      pending[_lengthOffset + last_lit] = unchecked((byte)lc);
      last_lit++;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4745uL);
      if (dist == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4746uL);
        dyn_ltree[lc * 2]++;
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4747uL);
        matches++;
        dist--;
        dyn_ltree[(Tree.LengthCode[lc] + InternalConstants.LITERALS + 1) * 2]++;
        dyn_dtree[Tree.DistanceCode(dist) * 2]++;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4748uL);
      if ((last_lit & 0x1fff) == 0 && (int)compressionLevel > 2) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4749uL);
        int out_length = last_lit << 3;
        int in_length = strstart - block_start;
        int dcode;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4750uL);
        for (dcode = 0; dcode < InternalConstants.D_CODES; dcode++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4751uL);
          out_length = (int)(out_length + (int)dyn_dtree[dcode * 2] * (5L + Tree.ExtraDistanceBits[dcode]));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4752uL);
        out_length >>= 3;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4753uL);
        if ((matches < (last_lit / 2)) && out_length < in_length / 2) {
          System.Boolean RNTRNTRNT_516 = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4754uL);
          return RNTRNTRNT_516;
        }
      }
      System.Boolean RNTRNTRNT_517 = (last_lit == lit_bufsize - 1) || (last_lit == lit_bufsize);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4755uL);
      return RNTRNTRNT_517;
    }
    internal void send_compressed_block(short[] ltree, short[] dtree)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4756uL);
      int distance;
      int lc;
      int lx = 0;
      int code;
      int extra;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4757uL);
      if (last_lit != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4758uL);
        do {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4759uL);
          int ix = _distanceOffset + lx * 2;
          distance = ((pending[ix] << 8) & 0xff00) | (pending[ix + 1] & 0xff);
          lc = (pending[_lengthOffset + lx]) & 0xff;
          lx++;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4760uL);
          if (distance == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4761uL);
            send_code(lc, ltree);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4762uL);
            code = Tree.LengthCode[lc];
            send_code(code + InternalConstants.LITERALS + 1, ltree);
            extra = Tree.ExtraLengthBits[code];
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4763uL);
            if (extra != 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4764uL);
              lc -= Tree.LengthBase[code];
              send_bits(lc, extra);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4765uL);
            distance--;
            code = Tree.DistanceCode(distance);
            send_code(code, dtree);
            extra = Tree.ExtraDistanceBits[code];
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4766uL);
            if (extra != 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4767uL);
              distance -= Tree.DistanceBase[code];
              send_bits(distance, extra);
            }
          }
        } while (lx < last_lit);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4768uL);
      send_code(END_BLOCK, ltree);
      last_eob_len = ltree[END_BLOCK * 2 + 1];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4769uL);
    }
    internal void set_data_type()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4770uL);
      int n = 0;
      int ascii_freq = 0;
      int bin_freq = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4771uL);
      while (n < 7) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4772uL);
        bin_freq += dyn_ltree[n * 2];
        n++;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4773uL);
      while (n < 128) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4774uL);
        ascii_freq += dyn_ltree[n * 2];
        n++;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4775uL);
      while (n < InternalConstants.LITERALS) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4776uL);
        bin_freq += dyn_ltree[n * 2];
        n++;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4777uL);
      data_type = (sbyte)(bin_freq > (ascii_freq >> 2) ? Z_BINARY : Z_ASCII);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4778uL);
    }
    internal void bi_flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4779uL);
      if (bi_valid == 16) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4780uL);
        pending[pendingCount++] = (byte)bi_buf;
        pending[pendingCount++] = (byte)(bi_buf >> 8);
        bi_buf = 0;
        bi_valid = 0;
      } else if (bi_valid >= 8) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4781uL);
        pending[pendingCount++] = (byte)bi_buf;
        bi_buf >>= 8;
        bi_valid -= 8;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4782uL);
    }
    internal void bi_windup()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4783uL);
      if (bi_valid > 8) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4784uL);
        pending[pendingCount++] = (byte)bi_buf;
        pending[pendingCount++] = (byte)(bi_buf >> 8);
      } else if (bi_valid > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4785uL);
        pending[pendingCount++] = (byte)bi_buf;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4786uL);
      bi_buf = 0;
      bi_valid = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4787uL);
    }
    internal void copy_block(int buf, int len, bool header)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4788uL);
      bi_windup();
      last_eob_len = 8;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4789uL);
      if (header) {
        unchecked {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4790uL);
          pending[pendingCount++] = (byte)len;
          pending[pendingCount++] = (byte)(len >> 8);
          pending[pendingCount++] = (byte)~len;
          pending[pendingCount++] = (byte)(~len >> 8);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4791uL);
      put_bytes(window, buf, len);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4792uL);
    }
    internal void flush_block_only(bool eof)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4793uL);
      _tr_flush_block(block_start >= 0 ? block_start : -1, strstart - block_start, eof);
      block_start = strstart;
      _codec.flush_pending();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4794uL);
    }
    internal BlockState DeflateNone(FlushType flush)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4795uL);
      int max_block_size = 0xffff;
      int max_start;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4796uL);
      if (max_block_size > pending.Length - 5) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4797uL);
        max_block_size = pending.Length - 5;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4798uL);
      while (true) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4799uL);
        if (lookahead <= 1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4800uL);
          _fillWindow();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4801uL);
          if (lookahead == 0 && flush == FlushType.None) {
            BlockState RNTRNTRNT_518 = BlockState.NeedMore;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4802uL);
            return RNTRNTRNT_518;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4803uL);
          if (lookahead == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4804uL);
            break;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4805uL);
        strstart += lookahead;
        lookahead = 0;
        max_start = block_start + max_block_size;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4806uL);
        if (strstart == 0 || strstart >= max_start) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4807uL);
          lookahead = (int)(strstart - max_start);
          strstart = (int)max_start;
          flush_block_only(false);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4808uL);
          if (_codec.AvailableBytesOut == 0) {
            BlockState RNTRNTRNT_519 = BlockState.NeedMore;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4809uL);
            return RNTRNTRNT_519;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4810uL);
        if (strstart - block_start >= w_size - MIN_LOOKAHEAD) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4811uL);
          flush_block_only(false);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4812uL);
          if (_codec.AvailableBytesOut == 0) {
            BlockState RNTRNTRNT_520 = BlockState.NeedMore;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4813uL);
            return RNTRNTRNT_520;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4814uL);
      flush_block_only(flush == FlushType.Finish);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4815uL);
      if (_codec.AvailableBytesOut == 0) {
        BlockState RNTRNTRNT_521 = (flush == FlushType.Finish) ? BlockState.FinishStarted : BlockState.NeedMore;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4816uL);
        return RNTRNTRNT_521;
      }
      BlockState RNTRNTRNT_522 = flush == FlushType.Finish ? BlockState.FinishDone : BlockState.BlockDone;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4817uL);
      return RNTRNTRNT_522;
    }
    internal void _tr_stored_block(int buf, int stored_len, bool eof)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4818uL);
      send_bits((STORED_BLOCK << 1) + (eof ? 1 : 0), 3);
      copy_block(buf, stored_len, true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4819uL);
    }
    internal void _tr_flush_block(int buf, int stored_len, bool eof)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4820uL);
      int opt_lenb, static_lenb;
      int max_blindex = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4821uL);
      if (compressionLevel > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4822uL);
        if (data_type == Z_UNKNOWN) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4823uL);
          set_data_type();
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4824uL);
        treeLiterals.build_tree(this);
        treeDistances.build_tree(this);
        max_blindex = build_bl_tree();
        opt_lenb = (opt_len + 3 + 7) >> 3;
        static_lenb = (static_len + 3 + 7) >> 3;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4825uL);
        if (static_lenb <= opt_lenb) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4826uL);
          opt_lenb = static_lenb;
        }
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4827uL);
        opt_lenb = static_lenb = stored_len + 5;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4828uL);
      if (stored_len + 4 <= opt_lenb && buf != -1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4829uL);
        _tr_stored_block(buf, stored_len, eof);
      } else if (static_lenb == opt_lenb) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4831uL);
        send_bits((STATIC_TREES << 1) + (eof ? 1 : 0), 3);
        send_compressed_block(StaticTree.lengthAndLiteralsTreeCodes, StaticTree.distTreeCodes);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4830uL);
        send_bits((DYN_TREES << 1) + (eof ? 1 : 0), 3);
        send_all_trees(treeLiterals.max_code + 1, treeDistances.max_code + 1, max_blindex + 1);
        send_compressed_block(dyn_ltree, dyn_dtree);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4832uL);
      _InitializeBlocks();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4833uL);
      if (eof) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4834uL);
        bi_windup();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4835uL);
    }
    private void _fillWindow()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4836uL);
      int n, m;
      int p;
      int more;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4837uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4838uL);
        more = (window_size - lookahead - strstart);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4839uL);
        if (more == 0 && strstart == 0 && lookahead == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4840uL);
          more = w_size;
        } else if (more == -1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4848uL);
          more--;
        } else if (strstart >= w_size + w_size - MIN_LOOKAHEAD) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4841uL);
          Array.Copy(window, w_size, window, 0, w_size);
          match_start -= w_size;
          strstart -= w_size;
          block_start -= w_size;
          n = hash_size;
          p = n;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4842uL);
          do {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4843uL);
            m = (head[--p] & 0xffff);
            head[p] = (short)((m >= w_size) ? (m - w_size) : 0);
          } while (--n != 0);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4844uL);
          n = w_size;
          p = n;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4845uL);
          do {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4846uL);
            m = (prev[--p] & 0xffff);
            prev[p] = (short)((m >= w_size) ? (m - w_size) : 0);
          } while (--n != 0);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4847uL);
          more += w_size;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4849uL);
        if (_codec.AvailableBytesIn == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4850uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4851uL);
        n = _codec.read_buf(window, strstart + lookahead, more);
        lookahead += n;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4852uL);
        if (lookahead >= MIN_MATCH) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4853uL);
          ins_h = window[strstart] & 0xff;
          ins_h = (((ins_h) << hash_shift) ^ (window[strstart + 1] & 0xff)) & hash_mask;
        }
      } while (lookahead < MIN_LOOKAHEAD && _codec.AvailableBytesIn != 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4854uL);
    }
    internal BlockState DeflateFast(FlushType flush)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4855uL);
      int hash_head = 0;
      bool bflush;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4856uL);
      while (true) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4857uL);
        if (lookahead < MIN_LOOKAHEAD) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4858uL);
          _fillWindow();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4859uL);
          if (lookahead < MIN_LOOKAHEAD && flush == FlushType.None) {
            BlockState RNTRNTRNT_523 = BlockState.NeedMore;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4860uL);
            return RNTRNTRNT_523;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4861uL);
          if (lookahead == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4862uL);
            break;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4863uL);
        if (lookahead >= MIN_MATCH) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4864uL);
          ins_h = (((ins_h) << hash_shift) ^ (window[(strstart) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
          hash_head = (head[ins_h] & 0xffff);
          prev[strstart & w_mask] = head[ins_h];
          head[ins_h] = unchecked((short)strstart);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4865uL);
        if (hash_head != 0L && ((strstart - hash_head) & 0xffff) <= w_size - MIN_LOOKAHEAD) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4866uL);
          if (compressionStrategy != CompressionStrategy.HuffmanOnly) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4867uL);
            match_length = longest_match(hash_head);
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4868uL);
        if (match_length >= MIN_MATCH) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4869uL);
          bflush = _tr_tally(strstart - match_start, match_length - MIN_MATCH);
          lookahead -= match_length;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4870uL);
          if (match_length <= config.MaxLazy && lookahead >= MIN_MATCH) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4871uL);
            match_length--;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4872uL);
            do {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4873uL);
              strstart++;
              ins_h = ((ins_h << hash_shift) ^ (window[(strstart) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
              hash_head = (head[ins_h] & 0xffff);
              prev[strstart & w_mask] = head[ins_h];
              head[ins_h] = unchecked((short)strstart);
            } while (--match_length != 0);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4874uL);
            strstart++;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4875uL);
            strstart += match_length;
            match_length = 0;
            ins_h = window[strstart] & 0xff;
            ins_h = (((ins_h) << hash_shift) ^ (window[strstart + 1] & 0xff)) & hash_mask;
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4876uL);
          bflush = _tr_tally(0, window[strstart] & 0xff);
          lookahead--;
          strstart++;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4877uL);
        if (bflush) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4878uL);
          flush_block_only(false);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4879uL);
          if (_codec.AvailableBytesOut == 0) {
            BlockState RNTRNTRNT_524 = BlockState.NeedMore;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4880uL);
            return RNTRNTRNT_524;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4881uL);
      flush_block_only(flush == FlushType.Finish);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4882uL);
      if (_codec.AvailableBytesOut == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4883uL);
        if (flush == FlushType.Finish) {
          BlockState RNTRNTRNT_525 = BlockState.FinishStarted;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4884uL);
          return RNTRNTRNT_525;
        } else {
          BlockState RNTRNTRNT_526 = BlockState.NeedMore;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4885uL);
          return RNTRNTRNT_526;
        }
      }
      BlockState RNTRNTRNT_527 = flush == FlushType.Finish ? BlockState.FinishDone : BlockState.BlockDone;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4886uL);
      return RNTRNTRNT_527;
    }
    internal BlockState DeflateSlow(FlushType flush)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4887uL);
      int hash_head = 0;
      bool bflush;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4888uL);
      while (true) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4889uL);
        if (lookahead < MIN_LOOKAHEAD) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4890uL);
          _fillWindow();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4891uL);
          if (lookahead < MIN_LOOKAHEAD && flush == FlushType.None) {
            BlockState RNTRNTRNT_528 = BlockState.NeedMore;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4892uL);
            return RNTRNTRNT_528;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4893uL);
          if (lookahead == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4894uL);
            break;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4895uL);
        if (lookahead >= MIN_MATCH) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4896uL);
          ins_h = (((ins_h) << hash_shift) ^ (window[(strstart) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
          hash_head = (head[ins_h] & 0xffff);
          prev[strstart & w_mask] = head[ins_h];
          head[ins_h] = unchecked((short)strstart);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4897uL);
        prev_length = match_length;
        prev_match = match_start;
        match_length = MIN_MATCH - 1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4898uL);
        if (hash_head != 0 && prev_length < config.MaxLazy && ((strstart - hash_head) & 0xffff) <= w_size - MIN_LOOKAHEAD) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4899uL);
          if (compressionStrategy != CompressionStrategy.HuffmanOnly) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4900uL);
            match_length = longest_match(hash_head);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4901uL);
          if (match_length <= 5 && (compressionStrategy == CompressionStrategy.Filtered || (match_length == MIN_MATCH && strstart - match_start > 4096))) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4902uL);
            match_length = MIN_MATCH - 1;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4903uL);
        if (prev_length >= MIN_MATCH && match_length <= prev_length) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4904uL);
          int max_insert = strstart + lookahead - MIN_MATCH;
          bflush = _tr_tally(strstart - 1 - prev_match, prev_length - MIN_MATCH);
          lookahead -= (prev_length - 1);
          prev_length -= 2;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4905uL);
          do {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4906uL);
            if (++strstart <= max_insert) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4907uL);
              ins_h = (((ins_h) << hash_shift) ^ (window[(strstart) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
              hash_head = (head[ins_h] & 0xffff);
              prev[strstart & w_mask] = head[ins_h];
              head[ins_h] = unchecked((short)strstart);
            }
          } while (--prev_length != 0);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4908uL);
          match_available = 0;
          match_length = MIN_MATCH - 1;
          strstart++;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4909uL);
          if (bflush) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4910uL);
            flush_block_only(false);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4911uL);
            if (_codec.AvailableBytesOut == 0) {
              BlockState RNTRNTRNT_529 = BlockState.NeedMore;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4912uL);
              return RNTRNTRNT_529;
            }
          }
        } else if (match_available != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4914uL);
          bflush = _tr_tally(0, window[strstart - 1] & 0xff);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4915uL);
          if (bflush) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4916uL);
            flush_block_only(false);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4917uL);
          strstart++;
          lookahead--;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4918uL);
          if (_codec.AvailableBytesOut == 0) {
            BlockState RNTRNTRNT_530 = BlockState.NeedMore;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4919uL);
            return RNTRNTRNT_530;
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4913uL);
          match_available = 1;
          strstart++;
          lookahead--;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4920uL);
      if (match_available != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4921uL);
        bflush = _tr_tally(0, window[strstart - 1] & 0xff);
        match_available = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4922uL);
      flush_block_only(flush == FlushType.Finish);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4923uL);
      if (_codec.AvailableBytesOut == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4924uL);
        if (flush == FlushType.Finish) {
          BlockState RNTRNTRNT_531 = BlockState.FinishStarted;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4925uL);
          return RNTRNTRNT_531;
        } else {
          BlockState RNTRNTRNT_532 = BlockState.NeedMore;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4926uL);
          return RNTRNTRNT_532;
        }
      }
      BlockState RNTRNTRNT_533 = flush == FlushType.Finish ? BlockState.FinishDone : BlockState.BlockDone;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4927uL);
      return RNTRNTRNT_533;
    }
    internal int longest_match(int cur_match)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4928uL);
      int chain_length = config.MaxChainLength;
      int scan = strstart;
      int match;
      int len;
      int best_len = prev_length;
      int limit = strstart > (w_size - MIN_LOOKAHEAD) ? strstart - (w_size - MIN_LOOKAHEAD) : 0;
      int niceLength = config.NiceLength;
      int wmask = w_mask;
      int strend = strstart + MAX_MATCH;
      byte scan_end1 = window[scan + best_len - 1];
      byte scan_end = window[scan + best_len];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4929uL);
      if (prev_length >= config.GoodLength) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4930uL);
        chain_length >>= 2;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4931uL);
      if (niceLength > lookahead) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4932uL);
        niceLength = lookahead;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4933uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4934uL);
        match = cur_match;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4935uL);
        if (window[match + best_len] != scan_end || window[match + best_len - 1] != scan_end1 || window[match] != window[scan] || window[++match] != window[scan + 1]) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4936uL);
          continue;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4937uL);
        scan += 2;
        match++;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4938uL);
        do {
        } while (window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && scan < strend);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4939uL);
        len = MAX_MATCH - (int)(strend - scan);
        scan = strend - MAX_MATCH;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4940uL);
        if (len > best_len) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4941uL);
          match_start = cur_match;
          best_len = len;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4942uL);
          if (len >= niceLength) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4943uL);
            break;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4944uL);
          scan_end1 = window[scan + best_len - 1];
          scan_end = window[scan + best_len];
        }
      } while ((cur_match = (prev[cur_match & wmask] & 0xffff)) > limit && --chain_length != 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4945uL);
      if (best_len <= lookahead) {
        System.Int32 RNTRNTRNT_534 = best_len;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4946uL);
        return RNTRNTRNT_534;
      }
      System.Int32 RNTRNTRNT_535 = lookahead;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4947uL);
      return RNTRNTRNT_535;
    }
    private bool Rfc1950BytesEmitted = false;
    private bool _WantRfc1950HeaderBytes = true;
    internal bool WantRfc1950HeaderBytes {
      get {
        System.Boolean RNTRNTRNT_536 = _WantRfc1950HeaderBytes;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4948uL);
        return RNTRNTRNT_536;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4949uL);
        _WantRfc1950HeaderBytes = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4950uL);
      }
    }
    internal int Initialize(ZlibCodec codec, CompressionLevel level)
    {
      System.Int32 RNTRNTRNT_537 = Initialize(codec, level, ZlibConstants.WindowBitsMax);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4951uL);
      return RNTRNTRNT_537;
    }
    internal int Initialize(ZlibCodec codec, CompressionLevel level, int bits)
    {
      System.Int32 RNTRNTRNT_538 = Initialize(codec, level, bits, MEM_LEVEL_DEFAULT, CompressionStrategy.Default);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4952uL);
      return RNTRNTRNT_538;
    }
    internal int Initialize(ZlibCodec codec, CompressionLevel level, int bits, CompressionStrategy compressionStrategy)
    {
      System.Int32 RNTRNTRNT_539 = Initialize(codec, level, bits, MEM_LEVEL_DEFAULT, compressionStrategy);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4953uL);
      return RNTRNTRNT_539;
    }
    internal int Initialize(ZlibCodec codec, CompressionLevel level, int windowBits, int memLevel, CompressionStrategy strategy)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4954uL);
      _codec = codec;
      _codec.Message = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4955uL);
      if (windowBits < 9 || windowBits > 15) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4956uL);
        throw new ZlibException("windowBits must be in the range 9..15.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4957uL);
      if (memLevel < 1 || memLevel > MEM_LEVEL_MAX) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4958uL);
        throw new ZlibException(String.Format("memLevel must be in the range 1.. {0}", MEM_LEVEL_MAX));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4959uL);
      _codec.dstate = this;
      w_bits = windowBits;
      w_size = 1 << w_bits;
      w_mask = w_size - 1;
      hash_bits = memLevel + 7;
      hash_size = 1 << hash_bits;
      hash_mask = hash_size - 1;
      hash_shift = ((hash_bits + MIN_MATCH - 1) / MIN_MATCH);
      window = new byte[w_size * 2];
      prev = new short[w_size];
      head = new short[hash_size];
      lit_bufsize = 1 << (memLevel + 6);
      pending = new byte[lit_bufsize * 4];
      _distanceOffset = lit_bufsize;
      _lengthOffset = (1 + 2) * lit_bufsize;
      this.compressionLevel = level;
      this.compressionStrategy = strategy;
      Reset();
      System.Int32 RNTRNTRNT_540 = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4960uL);
      return RNTRNTRNT_540;
    }
    internal void Reset()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4961uL);
      _codec.TotalBytesIn = _codec.TotalBytesOut = 0;
      _codec.Message = null;
      pendingCount = 0;
      nextPending = 0;
      Rfc1950BytesEmitted = false;
      status = (WantRfc1950HeaderBytes) ? INIT_STATE : BUSY_STATE;
      _codec._Adler32 = Adler.Adler32(0, null, 0, 0);
      last_flush = (int)FlushType.None;
      _InitializeTreeData();
      _InitializeLazyMatch();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4962uL);
    }
    internal int End()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4963uL);
      if (status != INIT_STATE && status != BUSY_STATE && status != FINISH_STATE) {
        System.Int32 RNTRNTRNT_541 = ZlibConstants.Z_STREAM_ERROR;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4964uL);
        return RNTRNTRNT_541;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4965uL);
      pending = null;
      head = null;
      prev = null;
      window = null;
      System.Int32 RNTRNTRNT_542 = status == BUSY_STATE ? ZlibConstants.Z_DATA_ERROR : ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4966uL);
      return RNTRNTRNT_542;
    }
    private void SetDeflater()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4967uL);
      switch (config.Flavor) {
        case DeflateFlavor.Store:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4970uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4968uL);
            DeflateFunction = DeflateNone;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4969uL);
            break;
          }

        case DeflateFlavor.Fast:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4973uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4971uL);
            DeflateFunction = DeflateFast;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4972uL);
            break;
          }

        case DeflateFlavor.Slow:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4976uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4974uL);
            DeflateFunction = DeflateSlow;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4975uL);
            break;
          }

      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4977uL);
    }
    internal int SetParams(CompressionLevel level, CompressionStrategy strategy)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4978uL);
      int result = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4979uL);
      if (compressionLevel != level) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4980uL);
        Config newConfig = Config.Lookup(level);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4981uL);
        if (newConfig.Flavor != config.Flavor && _codec.TotalBytesIn != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4982uL);
          result = _codec.Deflate(FlushType.Partial);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4983uL);
        compressionLevel = level;
        config = newConfig;
        SetDeflater();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4984uL);
      compressionStrategy = strategy;
      System.Int32 RNTRNTRNT_543 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4985uL);
      return RNTRNTRNT_543;
    }
    internal int SetDictionary(byte[] dictionary)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4986uL);
      int length = dictionary.Length;
      int index = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4987uL);
      if (dictionary == null || status != INIT_STATE) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4988uL);
        throw new ZlibException("Stream error.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4989uL);
      _codec._Adler32 = Adler.Adler32(_codec._Adler32, dictionary, 0, dictionary.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4990uL);
      if (length < MIN_MATCH) {
        System.Int32 RNTRNTRNT_544 = ZlibConstants.Z_OK;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4991uL);
        return RNTRNTRNT_544;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4992uL);
      if (length > w_size - MIN_LOOKAHEAD) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4993uL);
        length = w_size - MIN_LOOKAHEAD;
        index = dictionary.Length - length;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4994uL);
      Array.Copy(dictionary, index, window, 0, length);
      strstart = length;
      block_start = length;
      ins_h = window[0] & 0xff;
      ins_h = (((ins_h) << hash_shift) ^ (window[1] & 0xff)) & hash_mask;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4995uL);
      for (int n = 0; n <= length - MIN_MATCH; n++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4996uL);
        ins_h = (((ins_h) << hash_shift) ^ (window[(n) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
        prev[n & w_mask] = head[ins_h];
        head[ins_h] = (short)n;
      }
      System.Int32 RNTRNTRNT_545 = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4997uL);
      return RNTRNTRNT_545;
    }
    internal int Deflate(FlushType flush)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4998uL);
      int old_flush;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4999uL);
      if (_codec.OutputBuffer == null || (_codec.InputBuffer == null && _codec.AvailableBytesIn != 0) || (status == FINISH_STATE && flush != FlushType.Finish)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5000uL);
        _codec.Message = _ErrorMessage[ZlibConstants.Z_NEED_DICT - (ZlibConstants.Z_STREAM_ERROR)];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5001uL);
        throw new ZlibException(String.Format("Something is fishy. [{0}]", _codec.Message));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5002uL);
      if (_codec.AvailableBytesOut == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5003uL);
        _codec.Message = _ErrorMessage[ZlibConstants.Z_NEED_DICT - (ZlibConstants.Z_BUF_ERROR)];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5004uL);
        throw new ZlibException("OutputBuffer is full (AvailableBytesOut == 0)");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5005uL);
      old_flush = last_flush;
      last_flush = (int)flush;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5006uL);
      if (status == INIT_STATE) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5007uL);
        int header = (Z_DEFLATED + ((w_bits - 8) << 4)) << 8;
        int level_flags = (((int)compressionLevel - 1) & 0xff) >> 1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5008uL);
        if (level_flags > 3) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5009uL);
          level_flags = 3;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5010uL);
        header |= (level_flags << 6);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5011uL);
        if (strstart != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5012uL);
          header |= PRESET_DICT;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5013uL);
        header += 31 - (header % 31);
        status = BUSY_STATE;
        unchecked {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5014uL);
          pending[pendingCount++] = (byte)(header >> 8);
          pending[pendingCount++] = (byte)header;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5015uL);
        if (strstart != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5016uL);
          pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff000000u) >> 24);
          pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff0000) >> 16);
          pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff00) >> 8);
          pending[pendingCount++] = (byte)(_codec._Adler32 & 0xff);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5017uL);
        _codec._Adler32 = Adler.Adler32(0, null, 0, 0);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5018uL);
      if (pendingCount != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5019uL);
        _codec.flush_pending();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5020uL);
        if (_codec.AvailableBytesOut == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5021uL);
          last_flush = -1;
          System.Int32 RNTRNTRNT_546 = ZlibConstants.Z_OK;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5022uL);
          return RNTRNTRNT_546;
        }
      } else if (_codec.AvailableBytesIn == 0 && (int)flush <= old_flush && flush != FlushType.Finish) {
        System.Int32 RNTRNTRNT_547 = ZlibConstants.Z_OK;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5023uL);
        return RNTRNTRNT_547;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5024uL);
      if (status == FINISH_STATE && _codec.AvailableBytesIn != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5025uL);
        _codec.Message = _ErrorMessage[ZlibConstants.Z_NEED_DICT - (ZlibConstants.Z_BUF_ERROR)];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5026uL);
        throw new ZlibException("status == FINISH_STATE && _codec.AvailableBytesIn != 0");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5027uL);
      if (_codec.AvailableBytesIn != 0 || lookahead != 0 || (flush != FlushType.None && status != FINISH_STATE)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5028uL);
        BlockState bstate = DeflateFunction(flush);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5029uL);
        if (bstate == BlockState.FinishStarted || bstate == BlockState.FinishDone) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5030uL);
          status = FINISH_STATE;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5031uL);
        if (bstate == BlockState.NeedMore || bstate == BlockState.FinishStarted) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5032uL);
          if (_codec.AvailableBytesOut == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5033uL);
            last_flush = -1;
          }
          System.Int32 RNTRNTRNT_548 = ZlibConstants.Z_OK;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5034uL);
          return RNTRNTRNT_548;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5035uL);
        if (bstate == BlockState.BlockDone) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5036uL);
          if (flush == FlushType.Partial) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5037uL);
            _tr_align();
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5038uL);
            _tr_stored_block(0, 0, false);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5039uL);
            if (flush == FlushType.Full) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5040uL);
              for (int i = 0; i < hash_size; i++) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5041uL);
                head[i] = 0;
              }
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5042uL);
          _codec.flush_pending();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5043uL);
          if (_codec.AvailableBytesOut == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5044uL);
            last_flush = -1;
            System.Int32 RNTRNTRNT_549 = ZlibConstants.Z_OK;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5045uL);
            return RNTRNTRNT_549;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5046uL);
      if (flush != FlushType.Finish) {
        System.Int32 RNTRNTRNT_550 = ZlibConstants.Z_OK;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5047uL);
        return RNTRNTRNT_550;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5048uL);
      if (!WantRfc1950HeaderBytes || Rfc1950BytesEmitted) {
        System.Int32 RNTRNTRNT_551 = ZlibConstants.Z_STREAM_END;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5049uL);
        return RNTRNTRNT_551;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5050uL);
      pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff000000u) >> 24);
      pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff0000) >> 16);
      pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff00) >> 8);
      pending[pendingCount++] = (byte)(_codec._Adler32 & 0xff);
      _codec.flush_pending();
      Rfc1950BytesEmitted = true;
      System.Int32 RNTRNTRNT_552 = pendingCount != 0 ? ZlibConstants.Z_OK : ZlibConstants.Z_STREAM_END;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5051uL);
      return RNTRNTRNT_552;
    }
  }
}
