using System;
using System.IO;
namespace Ionic.Zlib
{
  public class ZlibStream : System.IO.Stream
  {
    internal ZlibBaseStream _baseStream;
    bool _disposed;
    public ZlibStream(System.IO.Stream stream, CompressionMode mode) : this(stream, mode, CompressionLevel.Default, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6383uL);
    }
    public ZlibStream(System.IO.Stream stream, CompressionMode mode, CompressionLevel level) : this(stream, mode, level, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6384uL);
    }
    public ZlibStream(System.IO.Stream stream, CompressionMode mode, bool leaveOpen) : this(stream, mode, CompressionLevel.Default, leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6385uL);
    }
    public ZlibStream(System.IO.Stream stream, CompressionMode mode, CompressionLevel level, bool leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6386uL);
      _baseStream = new ZlibBaseStream(stream, mode, level, ZlibStreamFlavor.ZLIB, leaveOpen);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6387uL);
    }
    public virtual FlushType FlushMode {
      get {
        FlushType RNTRNTRNT_722 = (this._baseStream._flushMode);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6388uL);
        return RNTRNTRNT_722;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6389uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6390uL);
          throw new ObjectDisposedException("ZlibStream");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6391uL);
        this._baseStream._flushMode = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6392uL);
      }
    }
    public int BufferSize {
      get {
        System.Int32 RNTRNTRNT_723 = this._baseStream._bufferSize;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6393uL);
        return RNTRNTRNT_723;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6394uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6395uL);
          throw new ObjectDisposedException("ZlibStream");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6396uL);
        if (this._baseStream._workingBuffer != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6397uL);
          throw new ZlibException("The working buffer is already set.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6398uL);
        if (value < ZlibConstants.WorkingBufferSizeMin) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6399uL);
          throw new ZlibException(String.Format("Don't be silly. {0} bytes?? Use a bigger buffer, at least {1}.", value, ZlibConstants.WorkingBufferSizeMin));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6400uL);
        this._baseStream._bufferSize = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6401uL);
      }
    }
    public virtual long TotalIn {
      get {
        System.Int64 RNTRNTRNT_724 = this._baseStream._z.TotalBytesIn;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6402uL);
        return RNTRNTRNT_724;
      }
    }
    public virtual long TotalOut {
      get {
        System.Int64 RNTRNTRNT_725 = this._baseStream._z.TotalBytesOut;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6403uL);
        return RNTRNTRNT_725;
      }
    }
    protected override void Dispose(bool disposing)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6404uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6405uL);
        if (!_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6406uL);
          if (disposing && (this._baseStream != null)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6407uL);
            this._baseStream.Close();
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6408uL);
          _disposed = true;
        }
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6409uL);
        base.Dispose(disposing);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6410uL);
    }
    public override bool CanRead {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6411uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6412uL);
          throw new ObjectDisposedException("ZlibStream");
        }
        System.Boolean RNTRNTRNT_726 = _baseStream._stream.CanRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6413uL);
        return RNTRNTRNT_726;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_727 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6414uL);
        return RNTRNTRNT_727;
      }
    }
    public override bool CanWrite {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6415uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6416uL);
          throw new ObjectDisposedException("ZlibStream");
        }
        System.Boolean RNTRNTRNT_728 = _baseStream._stream.CanWrite;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6417uL);
        return RNTRNTRNT_728;
      }
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6418uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6419uL);
        throw new ObjectDisposedException("ZlibStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6420uL);
      _baseStream.Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6421uL);
    }
    public override long Length {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6422uL);
        throw new NotSupportedException();
      }
    }
    public override long Position {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6423uL);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Writer) {
          System.Int64 RNTRNTRNT_729 = this._baseStream._z.TotalBytesOut;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6424uL);
          return RNTRNTRNT_729;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6425uL);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Reader) {
          System.Int64 RNTRNTRNT_730 = this._baseStream._z.TotalBytesIn;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6426uL);
          return RNTRNTRNT_730;
        }
        System.Int64 RNTRNTRNT_731 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6427uL);
        return RNTRNTRNT_731;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6428uL);
        throw new NotSupportedException();
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6429uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6430uL);
        throw new ObjectDisposedException("ZlibStream");
      }
      System.Int32 RNTRNTRNT_732 = _baseStream.Read(buffer, offset, count);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6431uL);
      return RNTRNTRNT_732;
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6432uL);
      throw new NotSupportedException();
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6433uL);
      throw new NotSupportedException();
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6434uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6435uL);
        throw new ObjectDisposedException("ZlibStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6436uL);
      _baseStream.Write(buffer, offset, count);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6437uL);
    }
    public static byte[] CompressString(String s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6438uL);
      using (var ms = new MemoryStream()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6439uL);
        Stream compressor = new ZlibStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressString(s, compressor);
        System.Byte[] RNTRNTRNT_733 = ms.ToArray();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6440uL);
        return RNTRNTRNT_733;
      }
    }
    public static byte[] CompressBuffer(byte[] b)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6441uL);
      using (var ms = new MemoryStream()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6442uL);
        Stream compressor = new ZlibStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressBuffer(b, compressor);
        System.Byte[] RNTRNTRNT_734 = ms.ToArray();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6443uL);
        return RNTRNTRNT_734;
      }
    }
    public static String UncompressString(byte[] compressed)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6444uL);
      using (var input = new MemoryStream(compressed)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6445uL);
        Stream decompressor = new ZlibStream(input, CompressionMode.Decompress);
        String RNTRNTRNT_735 = ZlibBaseStream.UncompressString(compressed, decompressor);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6446uL);
        return RNTRNTRNT_735;
      }
    }
    public static byte[] UncompressBuffer(byte[] compressed)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6447uL);
      using (var input = new MemoryStream(compressed)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6448uL);
        Stream decompressor = new ZlibStream(input, CompressionMode.Decompress);
        System.Byte[] RNTRNTRNT_736 = ZlibBaseStream.UncompressBuffer(compressed, decompressor);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6449uL);
        return RNTRNTRNT_736;
      }
    }
  }
}
