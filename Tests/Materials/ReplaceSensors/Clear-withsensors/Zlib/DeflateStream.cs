using System;
namespace Ionic.Zlib
{
  public class DeflateStream : System.IO.Stream
  {
    internal ZlibBaseStream _baseStream;
    internal System.IO.Stream _innerStream;
    bool _disposed;
    public DeflateStream(System.IO.Stream stream, CompressionMode mode) : this(stream, mode, CompressionLevel.Default, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5052uL);
    }
    public DeflateStream(System.IO.Stream stream, CompressionMode mode, CompressionLevel level) : this(stream, mode, level, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5053uL);
    }
    public DeflateStream(System.IO.Stream stream, CompressionMode mode, bool leaveOpen) : this(stream, mode, CompressionLevel.Default, leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5054uL);
    }
    public DeflateStream(System.IO.Stream stream, CompressionMode mode, CompressionLevel level, bool leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5055uL);
      _innerStream = stream;
      _baseStream = new ZlibBaseStream(stream, mode, level, ZlibStreamFlavor.DEFLATE, leaveOpen);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5056uL);
    }
    public virtual FlushType FlushMode {
      get {
        FlushType RNTRNTRNT_553 = (this._baseStream._flushMode);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5057uL);
        return RNTRNTRNT_553;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5058uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5059uL);
          throw new ObjectDisposedException("DeflateStream");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5060uL);
        this._baseStream._flushMode = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5061uL);
      }
    }
    public int BufferSize {
      get {
        System.Int32 RNTRNTRNT_554 = this._baseStream._bufferSize;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5062uL);
        return RNTRNTRNT_554;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5063uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5064uL);
          throw new ObjectDisposedException("DeflateStream");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5065uL);
        if (this._baseStream._workingBuffer != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5066uL);
          throw new ZlibException("The working buffer is already set.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5067uL);
        if (value < ZlibConstants.WorkingBufferSizeMin) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5068uL);
          throw new ZlibException(String.Format("Don't be silly. {0} bytes?? Use a bigger buffer, at least {1}.", value, ZlibConstants.WorkingBufferSizeMin));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5069uL);
        this._baseStream._bufferSize = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5070uL);
      }
    }
    public CompressionStrategy Strategy {
      get {
        CompressionStrategy RNTRNTRNT_555 = this._baseStream.Strategy;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5071uL);
        return RNTRNTRNT_555;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5072uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5073uL);
          throw new ObjectDisposedException("DeflateStream");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5074uL);
        this._baseStream.Strategy = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5075uL);
      }
    }
    public virtual long TotalIn {
      get {
        System.Int64 RNTRNTRNT_556 = this._baseStream._z.TotalBytesIn;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5076uL);
        return RNTRNTRNT_556;
      }
    }
    public virtual long TotalOut {
      get {
        System.Int64 RNTRNTRNT_557 = this._baseStream._z.TotalBytesOut;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5077uL);
        return RNTRNTRNT_557;
      }
    }
    protected override void Dispose(bool disposing)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5078uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5079uL);
        if (!_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5080uL);
          if (disposing && (this._baseStream != null)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5081uL);
            this._baseStream.Close();
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5082uL);
          _disposed = true;
        }
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5083uL);
        base.Dispose(disposing);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5084uL);
    }
    public override bool CanRead {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5085uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5086uL);
          throw new ObjectDisposedException("DeflateStream");
        }
        System.Boolean RNTRNTRNT_558 = _baseStream._stream.CanRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5087uL);
        return RNTRNTRNT_558;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_559 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5088uL);
        return RNTRNTRNT_559;
      }
    }
    public override bool CanWrite {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5089uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5090uL);
          throw new ObjectDisposedException("DeflateStream");
        }
        System.Boolean RNTRNTRNT_560 = _baseStream._stream.CanWrite;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5091uL);
        return RNTRNTRNT_560;
      }
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5092uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5093uL);
        throw new ObjectDisposedException("DeflateStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5094uL);
      _baseStream.Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5095uL);
    }
    public override long Length {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5096uL);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5097uL);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Writer) {
          System.Int64 RNTRNTRNT_561 = this._baseStream._z.TotalBytesOut;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5098uL);
          return RNTRNTRNT_561;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5099uL);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Reader) {
          System.Int64 RNTRNTRNT_562 = this._baseStream._z.TotalBytesIn;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5100uL);
          return RNTRNTRNT_562;
        }
        System.Int64 RNTRNTRNT_563 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5101uL);
        return RNTRNTRNT_563;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5102uL);
        throw new NotImplementedException();
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5103uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5104uL);
        throw new ObjectDisposedException("DeflateStream");
      }
      System.Int32 RNTRNTRNT_564 = _baseStream.Read(buffer, offset, count);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5105uL);
      return RNTRNTRNT_564;
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5106uL);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5107uL);
      throw new NotImplementedException();
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5108uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5109uL);
        throw new ObjectDisposedException("DeflateStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5110uL);
      _baseStream.Write(buffer, offset, count);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5111uL);
    }
    public static byte[] CompressString(String s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5112uL);
      using (var ms = new System.IO.MemoryStream()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5113uL);
        System.IO.Stream compressor = new DeflateStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressString(s, compressor);
        System.Byte[] RNTRNTRNT_565 = ms.ToArray();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5114uL);
        return RNTRNTRNT_565;
      }
    }
    public static byte[] CompressBuffer(byte[] b)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5115uL);
      using (var ms = new System.IO.MemoryStream()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5116uL);
        System.IO.Stream compressor = new DeflateStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressBuffer(b, compressor);
        System.Byte[] RNTRNTRNT_566 = ms.ToArray();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5117uL);
        return RNTRNTRNT_566;
      }
    }
    public static String UncompressString(byte[] compressed)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5118uL);
      using (var input = new System.IO.MemoryStream(compressed)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5119uL);
        System.IO.Stream decompressor = new DeflateStream(input, CompressionMode.Decompress);
        String RNTRNTRNT_567 = ZlibBaseStream.UncompressString(compressed, decompressor);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5120uL);
        return RNTRNTRNT_567;
      }
    }
    public static byte[] UncompressBuffer(byte[] compressed)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5121uL);
      using (var input = new System.IO.MemoryStream(compressed)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5122uL);
        System.IO.Stream decompressor = new DeflateStream(input, CompressionMode.Decompress);
        System.Byte[] RNTRNTRNT_568 = ZlibBaseStream.UncompressBuffer(compressed, decompressor);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5123uL);
        return RNTRNTRNT_568;
      }
    }
  }
}
