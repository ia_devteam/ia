using System;
namespace Ionic.Zlib
{
  sealed class InflateBlocks
  {
    private const int MANY = 1440;
    static internal readonly int[] border = new int[] {
      16,
      17,
      18,
      0,
      8,
      7,
      9,
      6,
      10,
      5,
      11,
      4,
      12,
      3,
      13,
      2,
      14,
      1,
      15
    };
    private enum InflateBlockMode
    {
      TYPE = 0,
      LENS = 1,
      STORED = 2,
      TABLE = 3,
      BTREE = 4,
      DTREE = 5,
      CODES = 6,
      DRY = 7,
      DONE = 8,
      BAD = 9
    }
    private InflateBlockMode mode;
    internal int left;
    internal int table;
    internal int index;
    internal int[] blens;
    internal int[] bb = new int[1];
    internal int[] tb = new int[1];
    internal InflateCodes codes = new InflateCodes();
    internal int last;
    internal ZlibCodec _codec;
    internal int bitk;
    internal int bitb;
    internal int[] hufts;
    internal byte[] window;
    internal int end;
    internal int readAt;
    internal int writeAt;
    internal System.Object checkfn;
    internal uint check;
    internal InfTree inftree = new InfTree();
    internal InflateBlocks(ZlibCodec codec, System.Object checkfn, int w)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5232uL);
      _codec = codec;
      hufts = new int[MANY * 3];
      window = new byte[w];
      end = w;
      this.checkfn = checkfn;
      mode = InflateBlockMode.TYPE;
      Reset();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5233uL);
    }
    internal uint Reset()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5234uL);
      uint oldCheck = check;
      mode = InflateBlockMode.TYPE;
      bitk = 0;
      bitb = 0;
      readAt = writeAt = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5235uL);
      if (checkfn != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5236uL);
        _codec._Adler32 = check = Adler.Adler32(0, null, 0, 0);
      }
      System.UInt32 RNTRNTRNT_588 = oldCheck;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5237uL);
      return RNTRNTRNT_588;
    }
    internal int Process(int r)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5238uL);
      int t;
      int b;
      int k;
      int p;
      int n;
      int q;
      int m;
      p = _codec.NextIn;
      n = _codec.AvailableBytesIn;
      b = bitb;
      k = bitk;
      q = writeAt;
      m = (int)(q < readAt ? readAt - q - 1 : end - q);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5239uL);
      while (true) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5240uL);
        switch (mode) {
          case InflateBlockMode.TYPE:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5262uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5241uL);
              while (k < (3)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5242uL);
                if (n != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5243uL);
                  r = ZlibConstants.Z_OK;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5244uL);
                  bitb = b;
                  bitk = k;
                  _codec.AvailableBytesIn = n;
                  _codec.TotalBytesIn += p - _codec.NextIn;
                  _codec.NextIn = p;
                  writeAt = q;
                  System.Int32 RNTRNTRNT_589 = Flush(r);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5245uL);
                  return RNTRNTRNT_589;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5246uL);
                n--;
                b |= (_codec.InputBuffer[p++] & 0xff) << k;
                k += 8;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5247uL);
              t = (int)(b & 7);
              last = t & 1;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5248uL);
              switch ((uint)t >> 1) {
                case 0:
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5251uL);
                  
                  {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5249uL);
                    b >>= 3;
                    k -= (3);
                    t = k & 7;
                    b >>= t;
                    k -= t;
                    mode = InflateBlockMode.LENS;
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5250uL);
                    break;
                  }

                case 1:
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5254uL);
                  
                  {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5252uL);
                    int[] bl = new int[1];
                    int[] bd = new int[1];
                    int[][] tl = new int[1][];
                    int[][] td = new int[1][];
                    InfTree.inflate_trees_fixed(bl, bd, tl, td, _codec);
                    codes.Init(bl[0], bd[0], tl[0], 0, td[0], 0);
                    b >>= 3;
                    k -= 3;
                    mode = InflateBlockMode.CODES;
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5253uL);
                    break;
                  }

                case 2:
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5257uL);
                  
                  {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5255uL);
                    b >>= 3;
                    k -= 3;
                    mode = InflateBlockMode.TABLE;
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5256uL);
                    break;
                  }

                case 3:
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5260uL);
                  
                  {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5258uL);
                    b >>= 3;
                    k -= 3;
                    mode = InflateBlockMode.BAD;
                    _codec.Message = "invalid block type";
                    r = ZlibConstants.Z_DATA_ERROR;
                    bitb = b;
                    bitk = k;
                    _codec.AvailableBytesIn = n;
                    _codec.TotalBytesIn += p - _codec.NextIn;
                    _codec.NextIn = p;
                    writeAt = q;
                    System.Int32 RNTRNTRNT_590 = Flush(r);
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5259uL);
                    return RNTRNTRNT_590;
                  }

              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5261uL);
              break;
            }

          case InflateBlockMode.LENS:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5273uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5263uL);
              while (k < (32)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5264uL);
                if (n != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5265uL);
                  r = ZlibConstants.Z_OK;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5266uL);
                  bitb = b;
                  bitk = k;
                  _codec.AvailableBytesIn = n;
                  _codec.TotalBytesIn += p - _codec.NextIn;
                  _codec.NextIn = p;
                  writeAt = q;
                  System.Int32 RNTRNTRNT_591 = Flush(r);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5267uL);
                  return RNTRNTRNT_591;
                }
                ;
                n--;
                b |= (_codec.InputBuffer[p++] & 0xff) << k;
                k += 8;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5268uL);
              if ((((~b) >> 16) & 0xffff) != (b & 0xffff)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5269uL);
                mode = InflateBlockMode.BAD;
                _codec.Message = "invalid stored block lengths";
                r = ZlibConstants.Z_DATA_ERROR;
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_592 = Flush(r);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5270uL);
                return RNTRNTRNT_592;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5271uL);
              left = (b & 0xffff);
              b = k = 0;
              mode = left != 0 ? InflateBlockMode.STORED : (last != 0 ? InflateBlockMode.DRY : InflateBlockMode.TYPE);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5272uL);
              break;
            }

          case InflateBlockMode.STORED:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5297uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5274uL);
              if (n == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5275uL);
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_593 = Flush(r);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5276uL);
                return RNTRNTRNT_593;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5277uL);
              if (m == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5278uL);
                if (q == end && readAt != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5279uL);
                  q = 0;
                  m = (int)(q < readAt ? readAt - q - 1 : end - q);
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5280uL);
                if (m == 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5281uL);
                  writeAt = q;
                  r = Flush(r);
                  q = writeAt;
                  m = (int)(q < readAt ? readAt - q - 1 : end - q);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5282uL);
                  if (q == end && readAt != 0) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5283uL);
                    q = 0;
                    m = (int)(q < readAt ? readAt - q - 1 : end - q);
                  }
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5284uL);
                  if (m == 0) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5285uL);
                    bitb = b;
                    bitk = k;
                    _codec.AvailableBytesIn = n;
                    _codec.TotalBytesIn += p - _codec.NextIn;
                    _codec.NextIn = p;
                    writeAt = q;
                    System.Int32 RNTRNTRNT_594 = Flush(r);
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5286uL);
                    return RNTRNTRNT_594;
                  }
                }
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5287uL);
              r = ZlibConstants.Z_OK;
              t = left;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5288uL);
              if (t > n) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5289uL);
                t = n;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5290uL);
              if (t > m) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5291uL);
                t = m;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5292uL);
              Array.Copy(_codec.InputBuffer, p, window, q, t);
              p += t;
              n -= t;
              q += t;
              m -= t;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5293uL);
              if ((left -= t) != 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5294uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5295uL);
              mode = last != 0 ? InflateBlockMode.DRY : InflateBlockMode.TYPE;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5296uL);
              break;
            }

          case InflateBlockMode.TABLE:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5314uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5298uL);
              while (k < (14)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5299uL);
                if (n != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5300uL);
                  r = ZlibConstants.Z_OK;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5301uL);
                  bitb = b;
                  bitk = k;
                  _codec.AvailableBytesIn = n;
                  _codec.TotalBytesIn += p - _codec.NextIn;
                  _codec.NextIn = p;
                  writeAt = q;
                  System.Int32 RNTRNTRNT_595 = Flush(r);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5302uL);
                  return RNTRNTRNT_595;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5303uL);
                n--;
                b |= (_codec.InputBuffer[p++] & 0xff) << k;
                k += 8;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5304uL);
              table = t = (b & 0x3fff);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5305uL);
              if ((t & 0x1f) > 29 || ((t >> 5) & 0x1f) > 29) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5306uL);
                mode = InflateBlockMode.BAD;
                _codec.Message = "too many length or distance symbols";
                r = ZlibConstants.Z_DATA_ERROR;
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_596 = Flush(r);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5307uL);
                return RNTRNTRNT_596;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5308uL);
              t = 258 + (t & 0x1f) + ((t >> 5) & 0x1f);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5309uL);
              if (blens == null || blens.Length < t) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5310uL);
                blens = new int[t];
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5311uL);
                Array.Clear(blens, 0, t);
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5312uL);
              b >>= 14;
              k -= 14;
              index = 0;
              mode = InflateBlockMode.BTREE;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5313uL);
              goto case InflateBlockMode.BTREE;
            }

          case InflateBlockMode.BTREE:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5334uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5315uL);
              while (index < 4 + (table >> 10)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5316uL);
                while (k < (3)) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5317uL);
                  if (n != 0) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5318uL);
                    r = ZlibConstants.Z_OK;
                  } else {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5319uL);
                    bitb = b;
                    bitk = k;
                    _codec.AvailableBytesIn = n;
                    _codec.TotalBytesIn += p - _codec.NextIn;
                    _codec.NextIn = p;
                    writeAt = q;
                    System.Int32 RNTRNTRNT_597 = Flush(r);
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5320uL);
                    return RNTRNTRNT_597;
                  }
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5321uL);
                  n--;
                  b |= (_codec.InputBuffer[p++] & 0xff) << k;
                  k += 8;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5322uL);
                blens[border[index++]] = b & 7;
                b >>= 3;
                k -= 3;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5323uL);
              while (index < 19) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5324uL);
                blens[border[index++]] = 0;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5325uL);
              bb[0] = 7;
              t = inftree.inflate_trees_bits(blens, bb, tb, hufts, _codec);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5326uL);
              if (t != ZlibConstants.Z_OK) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5327uL);
                r = t;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5328uL);
                if (r == ZlibConstants.Z_DATA_ERROR) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5329uL);
                  blens = null;
                  mode = InflateBlockMode.BAD;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5330uL);
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_598 = Flush(r);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5331uL);
                return RNTRNTRNT_598;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5332uL);
              index = 0;
              mode = InflateBlockMode.DTREE;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5333uL);
              goto case InflateBlockMode.DTREE;
            }

          case InflateBlockMode.DTREE:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5365uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5335uL);
              while (true) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5336uL);
                t = table;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5337uL);
                if (!(index < 258 + (t & 0x1f) + ((t >> 5) & 0x1f))) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5338uL);
                  break;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5339uL);
                int i, j, c;
                t = bb[0];
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5340uL);
                while (k < t) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5341uL);
                  if (n != 0) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5342uL);
                    r = ZlibConstants.Z_OK;
                  } else {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5343uL);
                    bitb = b;
                    bitk = k;
                    _codec.AvailableBytesIn = n;
                    _codec.TotalBytesIn += p - _codec.NextIn;
                    _codec.NextIn = p;
                    writeAt = q;
                    System.Int32 RNTRNTRNT_599 = Flush(r);
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5344uL);
                    return RNTRNTRNT_599;
                  }
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5345uL);
                  n--;
                  b |= (_codec.InputBuffer[p++] & 0xff) << k;
                  k += 8;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5346uL);
                t = hufts[(tb[0] + (b & InternalInflateConstants.InflateMask[t])) * 3 + 1];
                c = hufts[(tb[0] + (b & InternalInflateConstants.InflateMask[t])) * 3 + 2];
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5347uL);
                if (c < 16) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5348uL);
                  b >>= t;
                  k -= t;
                  blens[index++] = c;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5349uL);
                  i = c == 18 ? 7 : c - 14;
                  j = c == 18 ? 11 : 3;
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5350uL);
                  while (k < (t + i)) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5351uL);
                    if (n != 0) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5352uL);
                      r = ZlibConstants.Z_OK;
                    } else {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5353uL);
                      bitb = b;
                      bitk = k;
                      _codec.AvailableBytesIn = n;
                      _codec.TotalBytesIn += p - _codec.NextIn;
                      _codec.NextIn = p;
                      writeAt = q;
                      System.Int32 RNTRNTRNT_600 = Flush(r);
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5354uL);
                      return RNTRNTRNT_600;
                    }
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5355uL);
                    n--;
                    b |= (_codec.InputBuffer[p++] & 0xff) << k;
                    k += 8;
                  }
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5356uL);
                  b >>= t;
                  k -= t;
                  j += (b & InternalInflateConstants.InflateMask[i]);
                  b >>= i;
                  k -= i;
                  i = index;
                  t = table;
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5357uL);
                  if (i + j > 258 + (t & 0x1f) + ((t >> 5) & 0x1f) || (c == 16 && i < 1)) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5358uL);
                    blens = null;
                    mode = InflateBlockMode.BAD;
                    _codec.Message = "invalid bit length repeat";
                    r = ZlibConstants.Z_DATA_ERROR;
                    bitb = b;
                    bitk = k;
                    _codec.AvailableBytesIn = n;
                    _codec.TotalBytesIn += p - _codec.NextIn;
                    _codec.NextIn = p;
                    writeAt = q;
                    System.Int32 RNTRNTRNT_601 = Flush(r);
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5359uL);
                    return RNTRNTRNT_601;
                  }
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5360uL);
                  c = (c == 16) ? blens[i - 1] : 0;
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5361uL);
                  do {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5362uL);
                    blens[i++] = c;
                  } while (--j != 0);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5363uL);
                  index = i;
                }
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5364uL);
              tb[0] = -1;
            }

            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5373uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5366uL);
              int[] bl = new int[] { 9 };
              int[] bd = new int[] { 6 };
              int[] tl = new int[1];
              int[] td = new int[1];
              t = table;
              t = inftree.inflate_trees_dynamic(257 + (t & 0x1f), 1 + ((t >> 5) & 0x1f), blens, bl, bd, tl, td, hufts, _codec);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5367uL);
              if (t != ZlibConstants.Z_OK) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5368uL);
                if (t == ZlibConstants.Z_DATA_ERROR) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5369uL);
                  blens = null;
                  mode = InflateBlockMode.BAD;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5370uL);
                r = t;
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_602 = Flush(r);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5371uL);
                return RNTRNTRNT_602;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5372uL);
              codes.Init(bl[0], bd[0], hufts, tl[0], hufts, td[0]);
            }

            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5376uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5374uL);
              mode = InflateBlockMode.CODES;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5375uL);
              goto case InflateBlockMode.CODES;
            }

          case InflateBlockMode.CODES:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5386uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5377uL);
              bitb = b;
              bitk = k;
              _codec.AvailableBytesIn = n;
              _codec.TotalBytesIn += p - _codec.NextIn;
              _codec.NextIn = p;
              writeAt = q;
              r = codes.Process(this, r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5378uL);
              if (r != ZlibConstants.Z_STREAM_END) {
                System.Int32 RNTRNTRNT_603 = Flush(r);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5379uL);
                return RNTRNTRNT_603;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5380uL);
              r = ZlibConstants.Z_OK;
              p = _codec.NextIn;
              n = _codec.AvailableBytesIn;
              b = bitb;
              k = bitk;
              q = writeAt;
              m = (int)(q < readAt ? readAt - q - 1 : end - q);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5381uL);
              if (last == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5382uL);
                mode = InflateBlockMode.TYPE;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5383uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5384uL);
              mode = InflateBlockMode.DRY;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5385uL);
              goto case InflateBlockMode.DRY;
            }

          case InflateBlockMode.DRY:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5393uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5387uL);
              writeAt = q;
              r = Flush(r);
              q = writeAt;
              m = (int)(q < readAt ? readAt - q - 1 : end - q);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5388uL);
              if (readAt != writeAt) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5389uL);
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_604 = Flush(r);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5390uL);
                return RNTRNTRNT_604;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5391uL);
              mode = InflateBlockMode.DONE;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5392uL);
              goto case InflateBlockMode.DONE;
            }

          case InflateBlockMode.DONE:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5396uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5394uL);
              r = ZlibConstants.Z_STREAM_END;
              bitb = b;
              bitk = k;
              _codec.AvailableBytesIn = n;
              _codec.TotalBytesIn += p - _codec.NextIn;
              _codec.NextIn = p;
              writeAt = q;
              System.Int32 RNTRNTRNT_605 = Flush(r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5395uL);
              return RNTRNTRNT_605;
            }

          case InflateBlockMode.BAD:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5399uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5397uL);
              r = ZlibConstants.Z_DATA_ERROR;
              bitb = b;
              bitk = k;
              _codec.AvailableBytesIn = n;
              _codec.TotalBytesIn += p - _codec.NextIn;
              _codec.NextIn = p;
              writeAt = q;
              System.Int32 RNTRNTRNT_606 = Flush(r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5398uL);
              return RNTRNTRNT_606;
            }

          default:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5402uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5400uL);
              r = ZlibConstants.Z_STREAM_ERROR;
              bitb = b;
              bitk = k;
              _codec.AvailableBytesIn = n;
              _codec.TotalBytesIn += p - _codec.NextIn;
              _codec.NextIn = p;
              writeAt = q;
              System.Int32 RNTRNTRNT_607 = Flush(r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5401uL);
              return RNTRNTRNT_607;
            }

        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5403uL);
    }
    internal void Free()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5404uL);
      Reset();
      window = null;
      hufts = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5405uL);
    }
    internal void SetDictionary(byte[] d, int start, int n)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5406uL);
      Array.Copy(d, start, window, 0, n);
      readAt = writeAt = n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5407uL);
    }
    internal int SyncPoint()
    {
      System.Int32 RNTRNTRNT_608 = mode == InflateBlockMode.LENS ? 1 : 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5408uL);
      return RNTRNTRNT_608;
    }
    internal int Flush(int r)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5409uL);
      int nBytes;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5410uL);
      for (int pass = 0; pass < 2; pass++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5411uL);
        if (pass == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5412uL);
          nBytes = (int)((readAt <= writeAt ? writeAt : end) - readAt);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5413uL);
          nBytes = writeAt - readAt;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5414uL);
        if (nBytes == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5415uL);
          if (r == ZlibConstants.Z_BUF_ERROR) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5416uL);
            r = ZlibConstants.Z_OK;
          }
          System.Int32 RNTRNTRNT_609 = r;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5417uL);
          return RNTRNTRNT_609;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5418uL);
        if (nBytes > _codec.AvailableBytesOut) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5419uL);
          nBytes = _codec.AvailableBytesOut;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5420uL);
        if (nBytes != 0 && r == ZlibConstants.Z_BUF_ERROR) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5421uL);
          r = ZlibConstants.Z_OK;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5422uL);
        _codec.AvailableBytesOut -= nBytes;
        _codec.TotalBytesOut += nBytes;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5423uL);
        if (checkfn != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5424uL);
          _codec._Adler32 = check = Adler.Adler32(check, window, readAt, nBytes);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5425uL);
        Array.Copy(window, readAt, _codec.OutputBuffer, _codec.NextOut, nBytes);
        _codec.NextOut += nBytes;
        readAt += nBytes;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5426uL);
        if (readAt == end && pass == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5427uL);
          readAt = 0;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5428uL);
          if (writeAt == end) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5429uL);
            writeAt = 0;
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5430uL);
          pass++;
        }
      }
      System.Int32 RNTRNTRNT_610 = r;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5431uL);
      return RNTRNTRNT_610;
    }
  }
  static internal class InternalInflateConstants
  {
    static internal readonly int[] InflateMask = new int[] {
      0x0,
      0x1,
      0x3,
      0x7,
      0xf,
      0x1f,
      0x3f,
      0x7f,
      0xff,
      0x1ff,
      0x3ff,
      0x7ff,
      0xfff,
      0x1fff,
      0x3fff,
      0x7fff,
      0xffff
    };
  }
  sealed class InflateCodes
  {
    private const int START = 0;
    private const int LEN = 1;
    private const int LENEXT = 2;
    private const int DIST = 3;
    private const int DISTEXT = 4;
    private const int COPY = 5;
    private const int LIT = 6;
    private const int WASH = 7;
    private const int END = 8;
    private const int BADCODE = 9;
    internal int mode;
    internal int len;
    internal int[] tree;
    internal int tree_index = 0;
    internal int need;
    internal int lit;
    internal int bitsToGet;
    internal int dist;
    internal byte lbits;
    internal byte dbits;
    internal int[] ltree;
    internal int ltree_index;
    internal int[] dtree;
    internal int dtree_index;
    internal InflateCodes()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5432uL);
    }
    internal void Init(int bl, int bd, int[] tl, int tl_index, int[] td, int td_index)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5433uL);
      mode = START;
      lbits = (byte)bl;
      dbits = (byte)bd;
      ltree = tl;
      ltree_index = tl_index;
      dtree = td;
      dtree_index = td_index;
      tree = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5434uL);
    }
    internal int Process(InflateBlocks blocks, int r)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5435uL);
      int j;
      int tindex;
      int e;
      int b = 0;
      int k = 0;
      int p = 0;
      int n;
      int q;
      int m;
      int f;
      ZlibCodec z = blocks._codec;
      p = z.NextIn;
      n = z.AvailableBytesIn;
      b = blocks.bitb;
      k = blocks.bitk;
      q = blocks.writeAt;
      m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5436uL);
      while (true) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5437uL);
        switch (mode) {
          case START:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5445uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5438uL);
              if (m >= 258 && n >= 10) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5439uL);
                blocks.bitb = b;
                blocks.bitk = k;
                z.AvailableBytesIn = n;
                z.TotalBytesIn += p - z.NextIn;
                z.NextIn = p;
                blocks.writeAt = q;
                r = InflateFast(lbits, dbits, ltree, ltree_index, dtree, dtree_index, blocks, z);
                p = z.NextIn;
                n = z.AvailableBytesIn;
                b = blocks.bitb;
                k = blocks.bitk;
                q = blocks.writeAt;
                m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5440uL);
                if (r != ZlibConstants.Z_OK) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5441uL);
                  mode = (r == ZlibConstants.Z_STREAM_END) ? WASH : BADCODE;
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5442uL);
                  break;
                }
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5443uL);
              need = lbits;
              tree = ltree;
              tree_index = ltree_index;
              mode = LEN;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5444uL);
              goto case LEN;
            }

          case LEN:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5468uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5446uL);
              j = need;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5447uL);
              while (k < j) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5448uL);
                if (n != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5449uL);
                  r = ZlibConstants.Z_OK;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5450uL);
                  blocks.bitb = b;
                  blocks.bitk = k;
                  z.AvailableBytesIn = n;
                  z.TotalBytesIn += p - z.NextIn;
                  z.NextIn = p;
                  blocks.writeAt = q;
                  System.Int32 RNTRNTRNT_611 = blocks.Flush(r);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5451uL);
                  return RNTRNTRNT_611;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5452uL);
                n--;
                b |= (z.InputBuffer[p++] & 0xff) << k;
                k += 8;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5453uL);
              tindex = (tree_index + (b & InternalInflateConstants.InflateMask[j])) * 3;
              b >>= (tree[tindex + 1]);
              k -= (tree[tindex + 1]);
              e = tree[tindex];
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5454uL);
              if (e == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5455uL);
                lit = tree[tindex + 2];
                mode = LIT;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5456uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5457uL);
              if ((e & 16) != 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5458uL);
                bitsToGet = e & 15;
                len = tree[tindex + 2];
                mode = LENEXT;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5459uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5460uL);
              if ((e & 64) == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5461uL);
                need = e;
                tree_index = tindex / 3 + tree[tindex + 2];
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5462uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5463uL);
              if ((e & 32) != 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5464uL);
                mode = WASH;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5465uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5466uL);
              mode = BADCODE;
              z.Message = "invalid literal/length code";
              r = ZlibConstants.Z_DATA_ERROR;
              blocks.bitb = b;
              blocks.bitk = k;
              z.AvailableBytesIn = n;
              z.TotalBytesIn += p - z.NextIn;
              z.NextIn = p;
              blocks.writeAt = q;
              System.Int32 RNTRNTRNT_612 = blocks.Flush(r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5467uL);
              return RNTRNTRNT_612;
            }

          case LENEXT:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5478uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5469uL);
              j = bitsToGet;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5470uL);
              while (k < j) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5471uL);
                if (n != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5472uL);
                  r = ZlibConstants.Z_OK;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5473uL);
                  blocks.bitb = b;
                  blocks.bitk = k;
                  z.AvailableBytesIn = n;
                  z.TotalBytesIn += p - z.NextIn;
                  z.NextIn = p;
                  blocks.writeAt = q;
                  System.Int32 RNTRNTRNT_613 = blocks.Flush(r);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5474uL);
                  return RNTRNTRNT_613;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5475uL);
                n--;
                b |= (z.InputBuffer[p++] & 0xff) << k;
                k += 8;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5476uL);
              len += (b & InternalInflateConstants.InflateMask[j]);
              b >>= j;
              k -= j;
              need = dbits;
              tree = dtree;
              tree_index = dtree_index;
              mode = DIST;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5477uL);
              goto case DIST;
            }

          case DIST:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5495uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5479uL);
              j = need;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5480uL);
              while (k < j) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5481uL);
                if (n != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5482uL);
                  r = ZlibConstants.Z_OK;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5483uL);
                  blocks.bitb = b;
                  blocks.bitk = k;
                  z.AvailableBytesIn = n;
                  z.TotalBytesIn += p - z.NextIn;
                  z.NextIn = p;
                  blocks.writeAt = q;
                  System.Int32 RNTRNTRNT_614 = blocks.Flush(r);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5484uL);
                  return RNTRNTRNT_614;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5485uL);
                n--;
                b |= (z.InputBuffer[p++] & 0xff) << k;
                k += 8;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5486uL);
              tindex = (tree_index + (b & InternalInflateConstants.InflateMask[j])) * 3;
              b >>= tree[tindex + 1];
              k -= tree[tindex + 1];
              e = (tree[tindex]);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5487uL);
              if ((e & 0x10) != 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5488uL);
                bitsToGet = e & 15;
                dist = tree[tindex + 2];
                mode = DISTEXT;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5489uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5490uL);
              if ((e & 64) == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5491uL);
                need = e;
                tree_index = tindex / 3 + tree[tindex + 2];
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5492uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5493uL);
              mode = BADCODE;
              z.Message = "invalid distance code";
              r = ZlibConstants.Z_DATA_ERROR;
              blocks.bitb = b;
              blocks.bitk = k;
              z.AvailableBytesIn = n;
              z.TotalBytesIn += p - z.NextIn;
              z.NextIn = p;
              blocks.writeAt = q;
              System.Int32 RNTRNTRNT_615 = blocks.Flush(r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5494uL);
              return RNTRNTRNT_615;
            }

          case DISTEXT:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5505uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5496uL);
              j = bitsToGet;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5497uL);
              while (k < j) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5498uL);
                if (n != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5499uL);
                  r = ZlibConstants.Z_OK;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5500uL);
                  blocks.bitb = b;
                  blocks.bitk = k;
                  z.AvailableBytesIn = n;
                  z.TotalBytesIn += p - z.NextIn;
                  z.NextIn = p;
                  blocks.writeAt = q;
                  System.Int32 RNTRNTRNT_616 = blocks.Flush(r);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5501uL);
                  return RNTRNTRNT_616;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5502uL);
                n--;
                b |= (z.InputBuffer[p++] & 0xff) << k;
                k += 8;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5503uL);
              dist += (b & InternalInflateConstants.InflateMask[j]);
              b >>= j;
              k -= j;
              mode = COPY;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5504uL);
              goto case COPY;
            }

          case COPY:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5526uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5506uL);
              f = q - dist;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5507uL);
              while (f < 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5508uL);
                f += blocks.end;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5509uL);
              while (len != 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5510uL);
                if (m == 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5511uL);
                  if (q == blocks.end && blocks.readAt != 0) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5512uL);
                    q = 0;
                    m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                  }
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5513uL);
                  if (m == 0) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5514uL);
                    blocks.writeAt = q;
                    r = blocks.Flush(r);
                    q = blocks.writeAt;
                    m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5515uL);
                    if (q == blocks.end && blocks.readAt != 0) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5516uL);
                      q = 0;
                      m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                    }
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5517uL);
                    if (m == 0) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5518uL);
                      blocks.bitb = b;
                      blocks.bitk = k;
                      z.AvailableBytesIn = n;
                      z.TotalBytesIn += p - z.NextIn;
                      z.NextIn = p;
                      blocks.writeAt = q;
                      System.Int32 RNTRNTRNT_617 = blocks.Flush(r);
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5519uL);
                      return RNTRNTRNT_617;
                    }
                  }
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5520uL);
                blocks.window[q++] = blocks.window[f++];
                m--;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5521uL);
                if (f == blocks.end) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5522uL);
                  f = 0;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5523uL);
                len--;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5524uL);
              mode = START;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5525uL);
              break;
            }

          case LIT:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5539uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5527uL);
              if (m == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5528uL);
                if (q == blocks.end && blocks.readAt != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5529uL);
                  q = 0;
                  m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5530uL);
                if (m == 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5531uL);
                  blocks.writeAt = q;
                  r = blocks.Flush(r);
                  q = blocks.writeAt;
                  m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5532uL);
                  if (q == blocks.end && blocks.readAt != 0) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5533uL);
                    q = 0;
                    m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                  }
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5534uL);
                  if (m == 0) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5535uL);
                    blocks.bitb = b;
                    blocks.bitk = k;
                    z.AvailableBytesIn = n;
                    z.TotalBytesIn += p - z.NextIn;
                    z.NextIn = p;
                    blocks.writeAt = q;
                    System.Int32 RNTRNTRNT_618 = blocks.Flush(r);
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5536uL);
                    return RNTRNTRNT_618;
                  }
                }
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5537uL);
              r = ZlibConstants.Z_OK;
              blocks.window[q++] = (byte)lit;
              m--;
              mode = START;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5538uL);
              break;
            }

          case WASH:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5548uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5540uL);
              if (k > 7) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5541uL);
                k -= 8;
                n++;
                p--;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5542uL);
              blocks.writeAt = q;
              r = blocks.Flush(r);
              q = blocks.writeAt;
              m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5543uL);
              if (blocks.readAt != blocks.writeAt) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5544uL);
                blocks.bitb = b;
                blocks.bitk = k;
                z.AvailableBytesIn = n;
                z.TotalBytesIn += p - z.NextIn;
                z.NextIn = p;
                blocks.writeAt = q;
                System.Int32 RNTRNTRNT_619 = blocks.Flush(r);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5545uL);
                return RNTRNTRNT_619;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5546uL);
              mode = END;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5547uL);
              goto case END;
            }

          case END:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5551uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5549uL);
              r = ZlibConstants.Z_STREAM_END;
              blocks.bitb = b;
              blocks.bitk = k;
              z.AvailableBytesIn = n;
              z.TotalBytesIn += p - z.NextIn;
              z.NextIn = p;
              blocks.writeAt = q;
              System.Int32 RNTRNTRNT_620 = blocks.Flush(r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5550uL);
              return RNTRNTRNT_620;
            }

          case BADCODE:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5554uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5552uL);
              r = ZlibConstants.Z_DATA_ERROR;
              blocks.bitb = b;
              blocks.bitk = k;
              z.AvailableBytesIn = n;
              z.TotalBytesIn += p - z.NextIn;
              z.NextIn = p;
              blocks.writeAt = q;
              System.Int32 RNTRNTRNT_621 = blocks.Flush(r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5553uL);
              return RNTRNTRNT_621;
            }

          default:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5557uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5555uL);
              r = ZlibConstants.Z_STREAM_ERROR;
              blocks.bitb = b;
              blocks.bitk = k;
              z.AvailableBytesIn = n;
              z.TotalBytesIn += p - z.NextIn;
              z.NextIn = p;
              blocks.writeAt = q;
              System.Int32 RNTRNTRNT_622 = blocks.Flush(r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5556uL);
              return RNTRNTRNT_622;
            }

        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5558uL);
    }
    internal int InflateFast(int bl, int bd, int[] tl, int tl_index, int[] td, int td_index, InflateBlocks s, ZlibCodec z)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5559uL);
      int t;
      int[] tp;
      int tp_index;
      int e;
      int b;
      int k;
      int p;
      int n;
      int q;
      int m;
      int ml;
      int md;
      int c;
      int d;
      int r;
      int tp_index_t_3;
      p = z.NextIn;
      n = z.AvailableBytesIn;
      b = s.bitb;
      k = s.bitk;
      q = s.writeAt;
      m = q < s.readAt ? s.readAt - q - 1 : s.end - q;
      ml = InternalInflateConstants.InflateMask[bl];
      md = InternalInflateConstants.InflateMask[bd];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5560uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5561uL);
        while (k < (20)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5562uL);
          n--;
          b |= (z.InputBuffer[p++] & 0xff) << k;
          k += 8;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5563uL);
        t = b & ml;
        tp = tl;
        tp_index = tl_index;
        tp_index_t_3 = (tp_index + t) * 3;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5564uL);
        if ((e = tp[tp_index_t_3]) == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5565uL);
          b >>= (tp[tp_index_t_3 + 1]);
          k -= (tp[tp_index_t_3 + 1]);
          s.window[q++] = (byte)tp[tp_index_t_3 + 2];
          m--;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5566uL);
          continue;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5567uL);
        do {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5568uL);
          b >>= (tp[tp_index_t_3 + 1]);
          k -= (tp[tp_index_t_3 + 1]);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5569uL);
          if ((e & 16) != 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5570uL);
            e &= 15;
            c = tp[tp_index_t_3 + 2] + ((int)b & InternalInflateConstants.InflateMask[e]);
            b >>= e;
            k -= e;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5571uL);
            while (k < 15) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5572uL);
              n--;
              b |= (z.InputBuffer[p++] & 0xff) << k;
              k += 8;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5573uL);
            t = b & md;
            tp = td;
            tp_index = td_index;
            tp_index_t_3 = (tp_index + t) * 3;
            e = tp[tp_index_t_3];
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5574uL);
            do {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5575uL);
              b >>= (tp[tp_index_t_3 + 1]);
              k -= (tp[tp_index_t_3 + 1]);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5576uL);
              if ((e & 16) != 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5577uL);
                e &= 15;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5578uL);
                while (k < e) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5579uL);
                  n--;
                  b |= (z.InputBuffer[p++] & 0xff) << k;
                  k += 8;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5580uL);
                d = tp[tp_index_t_3 + 2] + (b & InternalInflateConstants.InflateMask[e]);
                b >>= e;
                k -= e;
                m -= c;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5581uL);
                if (q >= d) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5582uL);
                  r = q - d;
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5583uL);
                  if (q - r > 0 && 2 > (q - r)) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5584uL);
                    s.window[q++] = s.window[r++];
                    s.window[q++] = s.window[r++];
                    c -= 2;
                  } else {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5585uL);
                    Array.Copy(s.window, r, s.window, q, 2);
                    q += 2;
                    r += 2;
                    c -= 2;
                  }
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5586uL);
                  r = q - d;
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5587uL);
                  do {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5588uL);
                    r += s.end;
                  } while (r < 0);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5589uL);
                  e = s.end - r;
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5590uL);
                  if (c > e) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5591uL);
                    c -= e;
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5592uL);
                    if (q - r > 0 && e > (q - r)) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5593uL);
                      do {
                        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5594uL);
                        s.window[q++] = s.window[r++];
                      } while (--e != 0);
                    } else {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5595uL);
                      Array.Copy(s.window, r, s.window, q, e);
                      q += e;
                      r += e;
                      e = 0;
                    }
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5596uL);
                    r = 0;
                  }
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5597uL);
                if (q - r > 0 && c > (q - r)) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5598uL);
                  do {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5599uL);
                    s.window[q++] = s.window[r++];
                  } while (--c != 0);
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5600uL);
                  Array.Copy(s.window, r, s.window, q, c);
                  q += c;
                  r += c;
                  c = 0;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5601uL);
                break;
              } else if ((e & 64) == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5604uL);
                t += tp[tp_index_t_3 + 2];
                t += (b & InternalInflateConstants.InflateMask[e]);
                tp_index_t_3 = (tp_index + t) * 3;
                e = tp[tp_index_t_3];
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5602uL);
                z.Message = "invalid distance code";
                c = z.AvailableBytesIn - n;
                c = (k >> 3) < c ? k >> 3 : c;
                n += c;
                p -= c;
                k -= (c << 3);
                s.bitb = b;
                s.bitk = k;
                z.AvailableBytesIn = n;
                z.TotalBytesIn += p - z.NextIn;
                z.NextIn = p;
                s.writeAt = q;
                System.Int32 RNTRNTRNT_623 = ZlibConstants.Z_DATA_ERROR;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5603uL);
                return RNTRNTRNT_623;
              }
            } while (true);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5605uL);
            break;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5606uL);
          if ((e & 64) == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5607uL);
            t += tp[tp_index_t_3 + 2];
            t += (b & InternalInflateConstants.InflateMask[e]);
            tp_index_t_3 = (tp_index + t) * 3;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5608uL);
            if ((e = tp[tp_index_t_3]) == 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5609uL);
              b >>= (tp[tp_index_t_3 + 1]);
              k -= (tp[tp_index_t_3 + 1]);
              s.window[q++] = (byte)tp[tp_index_t_3 + 2];
              m--;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5610uL);
              break;
            }
          } else if ((e & 32) != 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5613uL);
            c = z.AvailableBytesIn - n;
            c = (k >> 3) < c ? k >> 3 : c;
            n += c;
            p -= c;
            k -= (c << 3);
            s.bitb = b;
            s.bitk = k;
            z.AvailableBytesIn = n;
            z.TotalBytesIn += p - z.NextIn;
            z.NextIn = p;
            s.writeAt = q;
            System.Int32 RNTRNTRNT_625 = ZlibConstants.Z_STREAM_END;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5614uL);
            return RNTRNTRNT_625;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5611uL);
            z.Message = "invalid literal/length code";
            c = z.AvailableBytesIn - n;
            c = (k >> 3) < c ? k >> 3 : c;
            n += c;
            p -= c;
            k -= (c << 3);
            s.bitb = b;
            s.bitk = k;
            z.AvailableBytesIn = n;
            z.TotalBytesIn += p - z.NextIn;
            z.NextIn = p;
            s.writeAt = q;
            System.Int32 RNTRNTRNT_624 = ZlibConstants.Z_DATA_ERROR;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5612uL);
            return RNTRNTRNT_624;
          }
        } while (true);
      } while (m >= 258 && n >= 10);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5615uL);
      c = z.AvailableBytesIn - n;
      c = (k >> 3) < c ? k >> 3 : c;
      n += c;
      p -= c;
      k -= (c << 3);
      s.bitb = b;
      s.bitk = k;
      z.AvailableBytesIn = n;
      z.TotalBytesIn += p - z.NextIn;
      z.NextIn = p;
      s.writeAt = q;
      System.Int32 RNTRNTRNT_626 = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5616uL);
      return RNTRNTRNT_626;
    }
  }
  internal sealed class InflateManager
  {
    private const int PRESET_DICT = 0x20;
    private const int Z_DEFLATED = 8;
    private enum InflateManagerMode
    {
      METHOD = 0,
      FLAG = 1,
      DICT4 = 2,
      DICT3 = 3,
      DICT2 = 4,
      DICT1 = 5,
      DICT0 = 6,
      BLOCKS = 7,
      CHECK4 = 8,
      CHECK3 = 9,
      CHECK2 = 10,
      CHECK1 = 11,
      DONE = 12,
      BAD = 13
    }
    private InflateManagerMode mode;
    internal ZlibCodec _codec;
    internal int method;
    internal uint computedCheck;
    internal uint expectedCheck;
    internal int marker;
    private bool _handleRfc1950HeaderBytes = true;
    internal bool HandleRfc1950HeaderBytes {
      get {
        System.Boolean RNTRNTRNT_627 = _handleRfc1950HeaderBytes;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5617uL);
        return RNTRNTRNT_627;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5618uL);
        _handleRfc1950HeaderBytes = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5619uL);
      }
    }
    internal int wbits;
    internal InflateBlocks blocks;
    public InflateManager()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5620uL);
    }
    public InflateManager(bool expectRfc1950HeaderBytes)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5621uL);
      _handleRfc1950HeaderBytes = expectRfc1950HeaderBytes;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5622uL);
    }
    internal int Reset()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5623uL);
      _codec.TotalBytesIn = _codec.TotalBytesOut = 0;
      _codec.Message = null;
      mode = HandleRfc1950HeaderBytes ? InflateManagerMode.METHOD : InflateManagerMode.BLOCKS;
      blocks.Reset();
      System.Int32 RNTRNTRNT_628 = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5624uL);
      return RNTRNTRNT_628;
    }
    internal int End()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5625uL);
      if (blocks != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5626uL);
        blocks.Free();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5627uL);
      blocks = null;
      System.Int32 RNTRNTRNT_629 = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5628uL);
      return RNTRNTRNT_629;
    }
    internal int Initialize(ZlibCodec codec, int w)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5629uL);
      _codec = codec;
      _codec.Message = null;
      blocks = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5630uL);
      if (w < 8 || w > 15) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5631uL);
        End();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5632uL);
        throw new ZlibException("Bad window size.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5633uL);
      wbits = w;
      blocks = new InflateBlocks(codec, HandleRfc1950HeaderBytes ? this : null, 1 << w);
      Reset();
      System.Int32 RNTRNTRNT_630 = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5634uL);
      return RNTRNTRNT_630;
    }
    internal int Inflate(FlushType flush)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5635uL);
      int b;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5636uL);
      if (_codec.InputBuffer == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5637uL);
        throw new ZlibException("InputBuffer is null. ");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5638uL);
      int f = ZlibConstants.Z_OK;
      int r = ZlibConstants.Z_BUF_ERROR;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5639uL);
      while (true) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5640uL);
        switch (mode) {
          case InflateManagerMode.METHOD:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5652uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5641uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_631 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5642uL);
                return RNTRNTRNT_631;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5643uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5644uL);
              if (((method = _codec.InputBuffer[_codec.NextIn++]) & 0xf) != Z_DEFLATED) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5645uL);
                mode = InflateManagerMode.BAD;
                _codec.Message = String.Format("unknown compression method (0x{0:X2})", method);
                marker = 5;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5646uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5647uL);
              if ((method >> 4) + 8 > wbits) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5648uL);
                mode = InflateManagerMode.BAD;
                _codec.Message = String.Format("invalid window size ({0})", (method >> 4) + 8);
                marker = 5;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5649uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5650uL);
              mode = InflateManagerMode.FLAG;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5651uL);
              break;
            }

          case InflateManagerMode.FLAG:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5661uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5653uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_632 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5654uL);
                return RNTRNTRNT_632;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5655uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              b = (_codec.InputBuffer[_codec.NextIn++]) & 0xff;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5656uL);
              if ((((method << 8) + b) % 31) != 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5657uL);
                mode = InflateManagerMode.BAD;
                _codec.Message = "incorrect header check";
                marker = 5;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5658uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5659uL);
              mode = ((b & PRESET_DICT) == 0) ? InflateManagerMode.BLOCKS : InflateManagerMode.DICT4;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5660uL);
              break;
            }

          case InflateManagerMode.DICT4:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5666uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5662uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_633 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5663uL);
                return RNTRNTRNT_633;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5664uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              expectedCheck = (uint)((_codec.InputBuffer[_codec.NextIn++] << 24) & 0xff000000u);
              mode = InflateManagerMode.DICT3;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5665uL);
              break;
            }

          case InflateManagerMode.DICT3:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5671uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5667uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_634 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5668uL);
                return RNTRNTRNT_634;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5669uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              expectedCheck += (uint)((_codec.InputBuffer[_codec.NextIn++] << 16) & 0xff0000);
              mode = InflateManagerMode.DICT2;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5670uL);
              break;
            }

          case InflateManagerMode.DICT2:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5676uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5672uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_635 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5673uL);
                return RNTRNTRNT_635;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5674uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              expectedCheck += (uint)((_codec.InputBuffer[_codec.NextIn++] << 8) & 0xff00);
              mode = InflateManagerMode.DICT1;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5675uL);
              break;
            }

          case InflateManagerMode.DICT1:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5681uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5677uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_636 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5678uL);
                return RNTRNTRNT_636;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5679uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              expectedCheck += (uint)(_codec.InputBuffer[_codec.NextIn++] & 0xff);
              _codec._Adler32 = expectedCheck;
              mode = InflateManagerMode.DICT0;
              System.Int32 RNTRNTRNT_637 = ZlibConstants.Z_NEED_DICT;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5680uL);
              return RNTRNTRNT_637;
            }

          case InflateManagerMode.DICT0:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5684uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5682uL);
              mode = InflateManagerMode.BAD;
              _codec.Message = "need dictionary";
              marker = 0;
              System.Int32 RNTRNTRNT_638 = ZlibConstants.Z_STREAM_ERROR;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5683uL);
              return RNTRNTRNT_638;
            }

          case InflateManagerMode.BLOCKS:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5699uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5685uL);
              r = blocks.Process(r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5686uL);
              if (r == ZlibConstants.Z_DATA_ERROR) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5687uL);
                mode = InflateManagerMode.BAD;
                marker = 0;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5688uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5689uL);
              if (r == ZlibConstants.Z_OK) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5690uL);
                r = f;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5691uL);
              if (r != ZlibConstants.Z_STREAM_END) {
                System.Int32 RNTRNTRNT_639 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5692uL);
                return RNTRNTRNT_639;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5693uL);
              r = f;
              computedCheck = blocks.Reset();
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5694uL);
              if (!HandleRfc1950HeaderBytes) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5695uL);
                mode = InflateManagerMode.DONE;
                System.Int32 RNTRNTRNT_640 = ZlibConstants.Z_STREAM_END;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5696uL);
                return RNTRNTRNT_640;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5697uL);
              mode = InflateManagerMode.CHECK4;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5698uL);
              break;
            }

          case InflateManagerMode.CHECK4:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5704uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5700uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_641 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5701uL);
                return RNTRNTRNT_641;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5702uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              expectedCheck = (uint)((_codec.InputBuffer[_codec.NextIn++] << 24) & 0xff000000u);
              mode = InflateManagerMode.CHECK3;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5703uL);
              break;
            }

          case InflateManagerMode.CHECK3:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5709uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5705uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_642 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5706uL);
                return RNTRNTRNT_642;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5707uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              expectedCheck += (uint)((_codec.InputBuffer[_codec.NextIn++] << 16) & 0xff0000);
              mode = InflateManagerMode.CHECK2;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5708uL);
              break;
            }

          case InflateManagerMode.CHECK2:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5714uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5710uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_643 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5711uL);
                return RNTRNTRNT_643;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5712uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              expectedCheck += (uint)((_codec.InputBuffer[_codec.NextIn++] << 8) & 0xff00);
              mode = InflateManagerMode.CHECK1;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5713uL);
              break;
            }

          case InflateManagerMode.CHECK1:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5723uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5715uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_644 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5716uL);
                return RNTRNTRNT_644;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5717uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              expectedCheck += (uint)(_codec.InputBuffer[_codec.NextIn++] & 0xff);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5718uL);
              if (computedCheck != expectedCheck) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5719uL);
                mode = InflateManagerMode.BAD;
                _codec.Message = "incorrect data check";
                marker = 5;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5720uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5721uL);
              mode = InflateManagerMode.DONE;
              System.Int32 RNTRNTRNT_645 = ZlibConstants.Z_STREAM_END;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5722uL);
              return RNTRNTRNT_645;
            }

          case InflateManagerMode.DONE:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5725uL);
            
            {
              System.Int32 RNTRNTRNT_646 = ZlibConstants.Z_STREAM_END;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5724uL);
              return RNTRNTRNT_646;
            }

          case InflateManagerMode.BAD:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5727uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5726uL);
              throw new ZlibException(String.Format("Bad state ({0})", _codec.Message));
            }

          default:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5729uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5728uL);
              throw new ZlibException("Stream error.");
            }

        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5730uL);
    }
    internal int SetDictionary(byte[] dictionary)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5731uL);
      int index = 0;
      int length = dictionary.Length;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5732uL);
      if (mode != InflateManagerMode.DICT0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5733uL);
        throw new ZlibException("Stream error.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5734uL);
      if (Adler.Adler32(1, dictionary, 0, dictionary.Length) != _codec._Adler32) {
        System.Int32 RNTRNTRNT_647 = ZlibConstants.Z_DATA_ERROR;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5735uL);
        return RNTRNTRNT_647;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5736uL);
      _codec._Adler32 = Adler.Adler32(0, null, 0, 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5737uL);
      if (length >= (1 << wbits)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5738uL);
        length = (1 << wbits) - 1;
        index = dictionary.Length - length;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5739uL);
      blocks.SetDictionary(dictionary, index, length);
      mode = InflateManagerMode.BLOCKS;
      System.Int32 RNTRNTRNT_648 = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5740uL);
      return RNTRNTRNT_648;
    }
    private static readonly byte[] mark = new byte[] {
      0,
      0,
      0xff,
      0xff
    };
    internal int Sync()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5741uL);
      int n;
      int p;
      int m;
      long r, w;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5742uL);
      if (mode != InflateManagerMode.BAD) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5743uL);
        mode = InflateManagerMode.BAD;
        marker = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5744uL);
      if ((n = _codec.AvailableBytesIn) == 0) {
        System.Int32 RNTRNTRNT_649 = ZlibConstants.Z_BUF_ERROR;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5745uL);
        return RNTRNTRNT_649;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5746uL);
      p = _codec.NextIn;
      m = marker;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5747uL);
      while (n != 0 && m < 4) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5748uL);
        if (_codec.InputBuffer[p] == mark[m]) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5749uL);
          m++;
        } else if (_codec.InputBuffer[p] != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5751uL);
          m = 0;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5750uL);
          m = 4 - m;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5752uL);
        p++;
        n--;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5753uL);
      _codec.TotalBytesIn += p - _codec.NextIn;
      _codec.NextIn = p;
      _codec.AvailableBytesIn = n;
      marker = m;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5754uL);
      if (m != 4) {
        System.Int32 RNTRNTRNT_650 = ZlibConstants.Z_DATA_ERROR;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5755uL);
        return RNTRNTRNT_650;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5756uL);
      r = _codec.TotalBytesIn;
      w = _codec.TotalBytesOut;
      Reset();
      _codec.TotalBytesIn = r;
      _codec.TotalBytesOut = w;
      mode = InflateManagerMode.BLOCKS;
      System.Int32 RNTRNTRNT_651 = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5757uL);
      return RNTRNTRNT_651;
    }
    internal int SyncPoint(ZlibCodec z)
    {
      System.Int32 RNTRNTRNT_652 = blocks.SyncPoint();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5758uL);
      return RNTRNTRNT_652;
    }
  }
}
