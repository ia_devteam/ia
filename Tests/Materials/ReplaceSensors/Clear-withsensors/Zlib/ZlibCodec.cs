using System;
using Interop = System.Runtime.InteropServices;
namespace Ionic.Zlib
{
  [Interop.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d0000D")]
  [Interop.ComVisible(true)]
  [Interop.ClassInterface(Interop.ClassInterfaceType.AutoDispatch)]
  public sealed class ZlibCodec
  {
    public byte[] InputBuffer;
    public int NextIn;
    public int AvailableBytesIn;
    public long TotalBytesIn;
    public byte[] OutputBuffer;
    public int NextOut;
    public int AvailableBytesOut;
    public long TotalBytesOut;
    public System.String Message;
    internal DeflateManager dstate;
    internal InflateManager istate;
    internal uint _Adler32;
    public CompressionLevel CompressLevel = CompressionLevel.Default;
    public int WindowBits = ZlibConstants.WindowBitsDefault;
    public CompressionStrategy Strategy = CompressionStrategy.Default;
    public int Adler32 {
      get {
        System.Int32 RNTRNTRNT_701 = (int)_Adler32;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6301uL);
        return RNTRNTRNT_701;
      }
    }
    public ZlibCodec()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6302uL);
    }
    public ZlibCodec(CompressionMode mode)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6303uL);
      if (mode == CompressionMode.Compress) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6304uL);
        int rc = InitializeDeflate();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6305uL);
        if (rc != ZlibConstants.Z_OK) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6306uL);
          throw new ZlibException("Cannot initialize for deflate.");
        }
      } else if (mode == CompressionMode.Decompress) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6308uL);
        int rc = InitializeInflate();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6309uL);
        if (rc != ZlibConstants.Z_OK) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6310uL);
          throw new ZlibException("Cannot initialize for inflate.");
        }
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6307uL);
        throw new ZlibException("Invalid ZlibStreamFlavor.");
      }
    }
    public int InitializeInflate()
    {
      System.Int32 RNTRNTRNT_702 = InitializeInflate(this.WindowBits);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6311uL);
      return RNTRNTRNT_702;
    }
    public int InitializeInflate(bool expectRfc1950Header)
    {
      System.Int32 RNTRNTRNT_703 = InitializeInflate(this.WindowBits, expectRfc1950Header);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6312uL);
      return RNTRNTRNT_703;
    }
    public int InitializeInflate(int windowBits)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6313uL);
      this.WindowBits = windowBits;
      System.Int32 RNTRNTRNT_704 = InitializeInflate(windowBits, true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6314uL);
      return RNTRNTRNT_704;
    }
    public int InitializeInflate(int windowBits, bool expectRfc1950Header)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6315uL);
      this.WindowBits = windowBits;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6316uL);
      if (dstate != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6317uL);
        throw new ZlibException("You may not call InitializeInflate() after calling InitializeDeflate().");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6318uL);
      istate = new InflateManager(expectRfc1950Header);
      System.Int32 RNTRNTRNT_705 = istate.Initialize(this, windowBits);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6319uL);
      return RNTRNTRNT_705;
    }
    public int Inflate(FlushType flush)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6320uL);
      if (istate == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6321uL);
        throw new ZlibException("No Inflate State!");
      }
      System.Int32 RNTRNTRNT_706 = istate.Inflate(flush);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6322uL);
      return RNTRNTRNT_706;
    }
    public int EndInflate()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6323uL);
      if (istate == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6324uL);
        throw new ZlibException("No Inflate State!");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6325uL);
      int ret = istate.End();
      istate = null;
      System.Int32 RNTRNTRNT_707 = ret;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6326uL);
      return RNTRNTRNT_707;
    }
    public int SyncInflate()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6327uL);
      if (istate == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6328uL);
        throw new ZlibException("No Inflate State!");
      }
      System.Int32 RNTRNTRNT_708 = istate.Sync();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6329uL);
      return RNTRNTRNT_708;
    }
    public int InitializeDeflate()
    {
      System.Int32 RNTRNTRNT_709 = _InternalInitializeDeflate(true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6330uL);
      return RNTRNTRNT_709;
    }
    public int InitializeDeflate(CompressionLevel level)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6331uL);
      this.CompressLevel = level;
      System.Int32 RNTRNTRNT_710 = _InternalInitializeDeflate(true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6332uL);
      return RNTRNTRNT_710;
    }
    public int InitializeDeflate(CompressionLevel level, bool wantRfc1950Header)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6333uL);
      this.CompressLevel = level;
      System.Int32 RNTRNTRNT_711 = _InternalInitializeDeflate(wantRfc1950Header);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6334uL);
      return RNTRNTRNT_711;
    }
    public int InitializeDeflate(CompressionLevel level, int bits)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6335uL);
      this.CompressLevel = level;
      this.WindowBits = bits;
      System.Int32 RNTRNTRNT_712 = _InternalInitializeDeflate(true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6336uL);
      return RNTRNTRNT_712;
    }
    public int InitializeDeflate(CompressionLevel level, int bits, bool wantRfc1950Header)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6337uL);
      this.CompressLevel = level;
      this.WindowBits = bits;
      System.Int32 RNTRNTRNT_713 = _InternalInitializeDeflate(wantRfc1950Header);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6338uL);
      return RNTRNTRNT_713;
    }
    private int _InternalInitializeDeflate(bool wantRfc1950Header)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6339uL);
      if (istate != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6340uL);
        throw new ZlibException("You may not call InitializeDeflate() after calling InitializeInflate().");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6341uL);
      dstate = new DeflateManager();
      dstate.WantRfc1950HeaderBytes = wantRfc1950Header;
      System.Int32 RNTRNTRNT_714 = dstate.Initialize(this, this.CompressLevel, this.WindowBits, this.Strategy);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6342uL);
      return RNTRNTRNT_714;
    }
    public int Deflate(FlushType flush)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6343uL);
      if (dstate == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6344uL);
        throw new ZlibException("No Deflate State!");
      }
      System.Int32 RNTRNTRNT_715 = dstate.Deflate(flush);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6345uL);
      return RNTRNTRNT_715;
    }
    public int EndDeflate()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6346uL);
      if (dstate == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6347uL);
        throw new ZlibException("No Deflate State!");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6348uL);
      dstate = null;
      System.Int32 RNTRNTRNT_716 = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6349uL);
      return RNTRNTRNT_716;
    }
    public void ResetDeflate()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6350uL);
      if (dstate == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6351uL);
        throw new ZlibException("No Deflate State!");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6352uL);
      dstate.Reset();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6353uL);
    }
    public int SetDeflateParams(CompressionLevel level, CompressionStrategy strategy)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6354uL);
      if (dstate == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6355uL);
        throw new ZlibException("No Deflate State!");
      }
      System.Int32 RNTRNTRNT_717 = dstate.SetParams(level, strategy);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6356uL);
      return RNTRNTRNT_717;
    }
    public int SetDictionary(byte[] dictionary)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6357uL);
      if (istate != null) {
        System.Int32 RNTRNTRNT_718 = istate.SetDictionary(dictionary);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6358uL);
        return RNTRNTRNT_718;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6359uL);
      if (dstate != null) {
        System.Int32 RNTRNTRNT_719 = dstate.SetDictionary(dictionary);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6360uL);
        return RNTRNTRNT_719;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6361uL);
      throw new ZlibException("No Inflate or Deflate state!");
    }
    internal void flush_pending()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6362uL);
      int len = dstate.pendingCount;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6363uL);
      if (len > AvailableBytesOut) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6364uL);
        len = AvailableBytesOut;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6365uL);
      if (len == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6366uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6367uL);
      if (dstate.pending.Length <= dstate.nextPending || OutputBuffer.Length <= NextOut || dstate.pending.Length < (dstate.nextPending + len) || OutputBuffer.Length < (NextOut + len)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6368uL);
        throw new ZlibException(String.Format("Invalid State. (pending.Length={0}, pendingCount={1})", dstate.pending.Length, dstate.pendingCount));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6369uL);
      Array.Copy(dstate.pending, dstate.nextPending, OutputBuffer, NextOut, len);
      NextOut += len;
      dstate.nextPending += len;
      TotalBytesOut += len;
      AvailableBytesOut -= len;
      dstate.pendingCount -= len;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6370uL);
      if (dstate.pendingCount == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6371uL);
        dstate.nextPending = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6372uL);
    }
    internal int read_buf(byte[] buf, int start, int size)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6373uL);
      int len = AvailableBytesIn;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6374uL);
      if (len > size) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6375uL);
        len = size;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6376uL);
      if (len == 0) {
        System.Int32 RNTRNTRNT_720 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6377uL);
        return RNTRNTRNT_720;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6378uL);
      AvailableBytesIn -= len;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6379uL);
      if (dstate.WantRfc1950HeaderBytes) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6380uL);
        _Adler32 = Adler.Adler32(_Adler32, InputBuffer, NextIn, len);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6381uL);
      Array.Copy(InputBuffer, NextIn, buf, start, len);
      NextIn += len;
      TotalBytesIn += len;
      System.Int32 RNTRNTRNT_721 = len;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6382uL);
      return RNTRNTRNT_721;
    }
  }
}
