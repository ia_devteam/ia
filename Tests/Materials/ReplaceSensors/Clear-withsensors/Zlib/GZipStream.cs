using System;
using System.IO;
namespace Ionic.Zlib
{
  public class GZipStream : System.IO.Stream
  {
    public String Comment {
      get {
        String RNTRNTRNT_569 = _Comment;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5124uL);
        return RNTRNTRNT_569;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5125uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5126uL);
          throw new ObjectDisposedException("GZipStream");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5127uL);
        _Comment = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5128uL);
      }
    }
    public String FileName {
      get {
        String RNTRNTRNT_570 = _FileName;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5129uL);
        return RNTRNTRNT_570;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5130uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5131uL);
          throw new ObjectDisposedException("GZipStream");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5132uL);
        _FileName = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5133uL);
        if (_FileName == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5134uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5135uL);
        if (_FileName.IndexOf("/") != -1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5136uL);
          _FileName = _FileName.Replace("/", "\\");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5137uL);
        if (_FileName.EndsWith("\\")) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5138uL);
          throw new Exception("Illegal filename");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5139uL);
        if (_FileName.IndexOf("\\") != -1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5140uL);
          _FileName = Path.GetFileName(_FileName);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5141uL);
      }
    }
    public DateTime? LastModified;
    public int Crc32 {
      get {
        System.Int32 RNTRNTRNT_571 = _Crc32;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5142uL);
        return RNTRNTRNT_571;
      }
    }
    private int _headerByteCount;
    internal ZlibBaseStream _baseStream;
    bool _disposed;
    bool _firstReadDone;
    string _FileName;
    string _Comment;
    int _Crc32;
    public GZipStream(Stream stream, CompressionMode mode) : this(stream, mode, CompressionLevel.Default, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5143uL);
    }
    public GZipStream(Stream stream, CompressionMode mode, CompressionLevel level) : this(stream, mode, level, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5144uL);
    }
    public GZipStream(Stream stream, CompressionMode mode, bool leaveOpen) : this(stream, mode, CompressionLevel.Default, leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5145uL);
    }
    public GZipStream(Stream stream, CompressionMode mode, CompressionLevel level, bool leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5146uL);
      _baseStream = new ZlibBaseStream(stream, mode, level, ZlibStreamFlavor.GZIP, leaveOpen);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5147uL);
    }
    public virtual FlushType FlushMode {
      get {
        FlushType RNTRNTRNT_572 = (this._baseStream._flushMode);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5148uL);
        return RNTRNTRNT_572;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5149uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5150uL);
          throw new ObjectDisposedException("GZipStream");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5151uL);
        this._baseStream._flushMode = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5152uL);
      }
    }
    public int BufferSize {
      get {
        System.Int32 RNTRNTRNT_573 = this._baseStream._bufferSize;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5153uL);
        return RNTRNTRNT_573;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5154uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5155uL);
          throw new ObjectDisposedException("GZipStream");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5156uL);
        if (this._baseStream._workingBuffer != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5157uL);
          throw new ZlibException("The working buffer is already set.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5158uL);
        if (value < ZlibConstants.WorkingBufferSizeMin) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5159uL);
          throw new ZlibException(String.Format("Don't be silly. {0} bytes?? Use a bigger buffer, at least {1}.", value, ZlibConstants.WorkingBufferSizeMin));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5160uL);
        this._baseStream._bufferSize = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5161uL);
      }
    }
    public virtual long TotalIn {
      get {
        System.Int64 RNTRNTRNT_574 = this._baseStream._z.TotalBytesIn;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5162uL);
        return RNTRNTRNT_574;
      }
    }
    public virtual long TotalOut {
      get {
        System.Int64 RNTRNTRNT_575 = this._baseStream._z.TotalBytesOut;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5163uL);
        return RNTRNTRNT_575;
      }
    }
    protected override void Dispose(bool disposing)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5164uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5165uL);
        if (!_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5166uL);
          if (disposing && (this._baseStream != null)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5167uL);
            this._baseStream.Close();
            this._Crc32 = _baseStream.Crc32;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5168uL);
          _disposed = true;
        }
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5169uL);
        base.Dispose(disposing);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5170uL);
    }
    public override bool CanRead {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5171uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5172uL);
          throw new ObjectDisposedException("GZipStream");
        }
        System.Boolean RNTRNTRNT_576 = _baseStream._stream.CanRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5173uL);
        return RNTRNTRNT_576;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_577 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5174uL);
        return RNTRNTRNT_577;
      }
    }
    public override bool CanWrite {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5175uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5176uL);
          throw new ObjectDisposedException("GZipStream");
        }
        System.Boolean RNTRNTRNT_578 = _baseStream._stream.CanWrite;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5177uL);
        return RNTRNTRNT_578;
      }
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5178uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5179uL);
        throw new ObjectDisposedException("GZipStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5180uL);
      _baseStream.Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5181uL);
    }
    public override long Length {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5182uL);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5183uL);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Writer) {
          System.Int64 RNTRNTRNT_579 = this._baseStream._z.TotalBytesOut + _headerByteCount;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5184uL);
          return RNTRNTRNT_579;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5185uL);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Reader) {
          System.Int64 RNTRNTRNT_580 = this._baseStream._z.TotalBytesIn + this._baseStream._gzipHeaderByteCount;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5186uL);
          return RNTRNTRNT_580;
        }
        System.Int64 RNTRNTRNT_581 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5187uL);
        return RNTRNTRNT_581;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5188uL);
        throw new NotImplementedException();
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5189uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5190uL);
        throw new ObjectDisposedException("GZipStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5191uL);
      int n = _baseStream.Read(buffer, offset, count);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5192uL);
      if (!_firstReadDone) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5193uL);
        _firstReadDone = true;
        FileName = _baseStream._GzipFileName;
        Comment = _baseStream._GzipComment;
      }
      System.Int32 RNTRNTRNT_582 = n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5194uL);
      return RNTRNTRNT_582;
    }
    public override long Seek(long offset, SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5195uL);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5196uL);
      throw new NotImplementedException();
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5197uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5198uL);
        throw new ObjectDisposedException("GZipStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5199uL);
      if (_baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Undefined) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5200uL);
        if (_baseStream._wantCompress) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5201uL);
          _headerByteCount = EmitHeader();
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5202uL);
          throw new InvalidOperationException();
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5203uL);
      _baseStream.Write(buffer, offset, count);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5204uL);
    }
    static internal readonly System.DateTime _unixEpoch = new System.DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    static internal readonly System.Text.Encoding iso8859dash1 = System.Text.Encoding.GetEncoding("iso-8859-1");
    private int EmitHeader()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5205uL);
      byte[] commentBytes = (Comment == null) ? null : iso8859dash1.GetBytes(Comment);
      byte[] filenameBytes = (FileName == null) ? null : iso8859dash1.GetBytes(FileName);
      int cbLength = (Comment == null) ? 0 : commentBytes.Length + 1;
      int fnLength = (FileName == null) ? 0 : filenameBytes.Length + 1;
      int bufferLength = 10 + cbLength + fnLength;
      byte[] header = new byte[bufferLength];
      int i = 0;
      header[i++] = 0x1f;
      header[i++] = 0x8b;
      header[i++] = 8;
      byte flag = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5206uL);
      if (Comment != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5207uL);
        flag ^= 0x10;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5208uL);
      if (FileName != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5209uL);
        flag ^= 0x8;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5210uL);
      header[i++] = flag;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5211uL);
      if (!LastModified.HasValue) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5212uL);
        LastModified = DateTime.Now;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5213uL);
      System.TimeSpan delta = LastModified.Value - _unixEpoch;
      Int32 timet = (Int32)delta.TotalSeconds;
      Array.Copy(BitConverter.GetBytes(timet), 0, header, i, 4);
      i += 4;
      header[i++] = 0;
      header[i++] = 0xff;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5214uL);
      if (fnLength != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5215uL);
        Array.Copy(filenameBytes, 0, header, i, fnLength - 1);
        i += fnLength - 1;
        header[i++] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5216uL);
      if (cbLength != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5217uL);
        Array.Copy(commentBytes, 0, header, i, cbLength - 1);
        i += cbLength - 1;
        header[i++] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5218uL);
      _baseStream._stream.Write(header, 0, header.Length);
      System.Int32 RNTRNTRNT_583 = header.Length;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5219uL);
      return RNTRNTRNT_583;
    }
    public static byte[] CompressString(String s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5220uL);
      using (var ms = new MemoryStream()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5221uL);
        System.IO.Stream compressor = new GZipStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressString(s, compressor);
        System.Byte[] RNTRNTRNT_584 = ms.ToArray();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5222uL);
        return RNTRNTRNT_584;
      }
    }
    public static byte[] CompressBuffer(byte[] b)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5223uL);
      using (var ms = new MemoryStream()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5224uL);
        System.IO.Stream compressor = new GZipStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressBuffer(b, compressor);
        System.Byte[] RNTRNTRNT_585 = ms.ToArray();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5225uL);
        return RNTRNTRNT_585;
      }
    }
    public static String UncompressString(byte[] compressed)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5226uL);
      using (var input = new MemoryStream(compressed)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5227uL);
        Stream decompressor = new GZipStream(input, CompressionMode.Decompress);
        String RNTRNTRNT_586 = ZlibBaseStream.UncompressString(compressed, decompressor);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5228uL);
        return RNTRNTRNT_586;
      }
    }
    public static byte[] UncompressBuffer(byte[] compressed)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5229uL);
      using (var input = new System.IO.MemoryStream(compressed)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5230uL);
        System.IO.Stream decompressor = new GZipStream(input, CompressionMode.Decompress);
        System.Byte[] RNTRNTRNT_587 = ZlibBaseStream.UncompressBuffer(compressed, decompressor);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5231uL);
        return RNTRNTRNT_587;
      }
    }
  }
}
