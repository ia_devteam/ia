using System;
using System.IO;
namespace Ionic.Zlib
{
  internal enum ZlibStreamFlavor
  {
    ZLIB = 1950,
    DEFLATE = 1951,
    GZIP = 1952
  }
  internal class ZlibBaseStream : System.IO.Stream
  {
    protected internal ZlibCodec _z = null;
    protected internal StreamMode _streamMode = StreamMode.Undefined;
    protected internal FlushType _flushMode;
    protected internal ZlibStreamFlavor _flavor;
    protected internal CompressionMode _compressionMode;
    protected internal CompressionLevel _level;
    protected internal bool _leaveOpen;
    protected internal byte[] _workingBuffer;
    protected internal int _bufferSize = ZlibConstants.WorkingBufferSizeDefault;
    protected internal byte[] _buf1 = new byte[1];
    protected internal System.IO.Stream _stream;
    protected internal CompressionStrategy Strategy = CompressionStrategy.Default;
    Ionic.Crc.CRC32 crc;
    protected internal string _GzipFileName;
    protected internal string _GzipComment;
    protected internal DateTime _GzipMtime;
    protected internal int _gzipHeaderByteCount;
    internal int Crc32 {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6106uL);
        if (crc == null) {
          System.Int32 RNTRNTRNT_682 = 0;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6107uL);
          return RNTRNTRNT_682;
        }
        System.Int32 RNTRNTRNT_683 = crc.Crc32Result;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6108uL);
        return RNTRNTRNT_683;
      }
    }
    public ZlibBaseStream(System.IO.Stream stream, CompressionMode compressionMode, CompressionLevel level, ZlibStreamFlavor flavor, bool leaveOpen) : base()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6109uL);
      this._flushMode = FlushType.None;
      this._stream = stream;
      this._leaveOpen = leaveOpen;
      this._compressionMode = compressionMode;
      this._flavor = flavor;
      this._level = level;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6110uL);
      if (flavor == ZlibStreamFlavor.GZIP) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6111uL);
        this.crc = new Ionic.Crc.CRC32();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6112uL);
    }
    protected internal bool _wantCompress {
      get {
        System.Boolean RNTRNTRNT_684 = (this._compressionMode == CompressionMode.Compress);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6113uL);
        return RNTRNTRNT_684;
      }
    }
    private ZlibCodec z {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6114uL);
        if (_z == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6115uL);
          bool wantRfc1950Header = (this._flavor == ZlibStreamFlavor.ZLIB);
          _z = new ZlibCodec();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6116uL);
          if (this._compressionMode == CompressionMode.Decompress) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6117uL);
            _z.InitializeInflate(wantRfc1950Header);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6118uL);
            _z.Strategy = Strategy;
            _z.InitializeDeflate(this._level, wantRfc1950Header);
          }
        }
        ZlibCodec RNTRNTRNT_685 = _z;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6119uL);
        return RNTRNTRNT_685;
      }
    }
    private byte[] workingBuffer {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6120uL);
        if (_workingBuffer == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6121uL);
          _workingBuffer = new byte[_bufferSize];
        }
        System.Byte[] RNTRNTRNT_686 = _workingBuffer;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6122uL);
        return RNTRNTRNT_686;
      }
    }
    public override void Write(System.Byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6123uL);
      if (crc != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6124uL);
        crc.SlurpBlock(buffer, offset, count);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6125uL);
      if (_streamMode == StreamMode.Undefined) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6126uL);
        _streamMode = StreamMode.Writer;
      } else if (_streamMode != StreamMode.Writer) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6127uL);
        throw new ZlibException("Cannot Write after Reading.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6128uL);
      if (count == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6129uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6130uL);
      z.InputBuffer = buffer;
      _z.NextIn = offset;
      _z.AvailableBytesIn = count;
      bool done = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6131uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6132uL);
        _z.OutputBuffer = workingBuffer;
        _z.NextOut = 0;
        _z.AvailableBytesOut = _workingBuffer.Length;
        int rc = (_wantCompress) ? _z.Deflate(_flushMode) : _z.Inflate(_flushMode);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6133uL);
        if (rc != ZlibConstants.Z_OK && rc != ZlibConstants.Z_STREAM_END) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6134uL);
          throw new ZlibException((_wantCompress ? "de" : "in") + "flating: " + _z.Message);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6135uL);
        _stream.Write(_workingBuffer, 0, _workingBuffer.Length - _z.AvailableBytesOut);
        done = _z.AvailableBytesIn == 0 && _z.AvailableBytesOut != 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6136uL);
        if (_flavor == ZlibStreamFlavor.GZIP && !_wantCompress) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6137uL);
          done = (_z.AvailableBytesIn == 8 && _z.AvailableBytesOut != 0);
        }
      } while (!done);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6138uL);
    }
    private void finish()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6139uL);
      if (_z == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6140uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6141uL);
      if (_streamMode == StreamMode.Writer) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6142uL);
        bool done = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6143uL);
        do {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6144uL);
          _z.OutputBuffer = workingBuffer;
          _z.NextOut = 0;
          _z.AvailableBytesOut = _workingBuffer.Length;
          int rc = (_wantCompress) ? _z.Deflate(FlushType.Finish) : _z.Inflate(FlushType.Finish);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6145uL);
          if (rc != ZlibConstants.Z_STREAM_END && rc != ZlibConstants.Z_OK) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6146uL);
            string verb = (_wantCompress ? "de" : "in") + "flating";
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6147uL);
            if (_z.Message == null) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6148uL);
              throw new ZlibException(String.Format("{0}: (rc = {1})", verb, rc));
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6149uL);
              throw new ZlibException(verb + ": " + _z.Message);
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6150uL);
          if (_workingBuffer.Length - _z.AvailableBytesOut > 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6151uL);
            _stream.Write(_workingBuffer, 0, _workingBuffer.Length - _z.AvailableBytesOut);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6152uL);
          done = _z.AvailableBytesIn == 0 && _z.AvailableBytesOut != 0;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6153uL);
          if (_flavor == ZlibStreamFlavor.GZIP && !_wantCompress) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6154uL);
            done = (_z.AvailableBytesIn == 8 && _z.AvailableBytesOut != 0);
          }
        } while (!done);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6155uL);
        Flush();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6156uL);
        if (_flavor == ZlibStreamFlavor.GZIP) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6157uL);
          if (_wantCompress) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6158uL);
            int c1 = crc.Crc32Result;
            _stream.Write(BitConverter.GetBytes(c1), 0, 4);
            int c2 = (Int32)(crc.TotalBytesRead & 0xffffffffu);
            _stream.Write(BitConverter.GetBytes(c2), 0, 4);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6159uL);
            throw new ZlibException("Writing with decompression is not supported.");
          }
        }
      } else if (_streamMode == StreamMode.Reader) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6160uL);
        if (_flavor == ZlibStreamFlavor.GZIP) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6161uL);
          if (!_wantCompress) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6162uL);
            if (_z.TotalBytesOut == 0L) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6163uL);
              return;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6164uL);
            byte[] trailer = new byte[8];
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6165uL);
            if (_z.AvailableBytesIn < 8) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6166uL);
              Array.Copy(_z.InputBuffer, _z.NextIn, trailer, 0, _z.AvailableBytesIn);
              int bytesNeeded = 8 - _z.AvailableBytesIn;
              int bytesRead = _stream.Read(trailer, _z.AvailableBytesIn, bytesNeeded);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6167uL);
              if (bytesNeeded != bytesRead) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6168uL);
                throw new ZlibException(String.Format("Missing or incomplete GZIP trailer. Expected 8 bytes, got {0}.", _z.AvailableBytesIn + bytesRead));
              }
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6169uL);
              Array.Copy(_z.InputBuffer, _z.NextIn, trailer, 0, trailer.Length);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6170uL);
            Int32 crc32_expected = BitConverter.ToInt32(trailer, 0);
            Int32 crc32_actual = crc.Crc32Result;
            Int32 isize_expected = BitConverter.ToInt32(trailer, 4);
            Int32 isize_actual = (Int32)(_z.TotalBytesOut & 0xffffffffu);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6171uL);
            if (crc32_actual != crc32_expected) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6172uL);
              throw new ZlibException(String.Format("Bad CRC32 in GZIP trailer. (actual({0:X8})!=expected({1:X8}))", crc32_actual, crc32_expected));
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6173uL);
            if (isize_actual != isize_expected) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6174uL);
              throw new ZlibException(String.Format("Bad size in GZIP trailer. (actual({0})!=expected({1}))", isize_actual, isize_expected));
            }
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6175uL);
            throw new ZlibException("Reading with compression is not supported.");
          }
        }
      }
    }
    private void end()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6176uL);
      if (z == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6177uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6178uL);
      if (_wantCompress) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6179uL);
        _z.EndDeflate();
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6180uL);
        _z.EndInflate();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6181uL);
      _z = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6182uL);
    }
    public override void Close()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6183uL);
      if (_stream == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6184uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6185uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6186uL);
        finish();
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6187uL);
        end();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6188uL);
        if (!_leaveOpen) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6189uL);
          _stream.Close();
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6190uL);
        _stream = null;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6191uL);
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6192uL);
      _stream.Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6193uL);
    }
    public override System.Int64 Seek(System.Int64 offset, System.IO.SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6194uL);
      throw new NotImplementedException();
    }
    public override void SetLength(System.Int64 value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6195uL);
      _stream.SetLength(value);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6196uL);
    }
    private bool nomoreinput = false;
    private string ReadZeroTerminatedString()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6197uL);
      var list = new System.Collections.Generic.List<byte>();
      bool done = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6198uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6199uL);
        int n = _stream.Read(_buf1, 0, 1);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6200uL);
        if (n != 1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6201uL);
          throw new ZlibException("Unexpected EOF reading GZIP header.");
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6202uL);
          if (_buf1[0] == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6203uL);
            done = true;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6204uL);
            list.Add(_buf1[0]);
          }
        }
      } while (!done);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6205uL);
      byte[] a = list.ToArray();
      System.String RNTRNTRNT_687 = GZipStream.iso8859dash1.GetString(a, 0, a.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6206uL);
      return RNTRNTRNT_687;
    }
    private int _ReadAndValidateGzipHeader()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6207uL);
      int totalBytesRead = 0;
      byte[] header = new byte[10];
      int n = _stream.Read(header, 0, header.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6208uL);
      if (n == 0) {
        System.Int32 RNTRNTRNT_688 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6209uL);
        return RNTRNTRNT_688;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6210uL);
      if (n != 10) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6211uL);
        throw new ZlibException("Not a valid GZIP stream.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6212uL);
      if (header[0] != 0x1f || header[1] != 0x8b || header[2] != 8) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6213uL);
        throw new ZlibException("Bad GZIP header.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6214uL);
      Int32 timet = BitConverter.ToInt32(header, 4);
      _GzipMtime = GZipStream._unixEpoch.AddSeconds(timet);
      totalBytesRead += n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6215uL);
      if ((header[3] & 0x4) == 0x4) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6216uL);
        n = _stream.Read(header, 0, 2);
        totalBytesRead += n;
        Int16 extraLength = (Int16)(header[0] + header[1] * 256);
        byte[] extra = new byte[extraLength];
        n = _stream.Read(extra, 0, extra.Length);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6217uL);
        if (n != extraLength) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6218uL);
          throw new ZlibException("Unexpected end-of-file reading GZIP header.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6219uL);
        totalBytesRead += n;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6220uL);
      if ((header[3] & 0x8) == 0x8) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6221uL);
        _GzipFileName = ReadZeroTerminatedString();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6222uL);
      if ((header[3] & 0x10) == 0x10) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6223uL);
        _GzipComment = ReadZeroTerminatedString();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6224uL);
      if ((header[3] & 0x2) == 0x2) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6225uL);
        Read(_buf1, 0, 1);
      }
      System.Int32 RNTRNTRNT_689 = totalBytesRead;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6226uL);
      return RNTRNTRNT_689;
    }
    public override System.Int32 Read(System.Byte[] buffer, System.Int32 offset, System.Int32 count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6227uL);
      if (_streamMode == StreamMode.Undefined) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6228uL);
        if (!this._stream.CanRead) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6229uL);
          throw new ZlibException("The stream is not readable.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6230uL);
        _streamMode = StreamMode.Reader;
        z.AvailableBytesIn = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6231uL);
        if (_flavor == ZlibStreamFlavor.GZIP) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6232uL);
          _gzipHeaderByteCount = _ReadAndValidateGzipHeader();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6233uL);
          if (_gzipHeaderByteCount == 0) {
            System.Int32 RNTRNTRNT_690 = 0;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6234uL);
            return RNTRNTRNT_690;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6235uL);
      if (_streamMode != StreamMode.Reader) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6236uL);
        throw new ZlibException("Cannot Read after Writing.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6237uL);
      if (count == 0) {
        System.Int32 RNTRNTRNT_691 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6238uL);
        return RNTRNTRNT_691;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6239uL);
      if (nomoreinput && _wantCompress) {
        System.Int32 RNTRNTRNT_692 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6240uL);
        return RNTRNTRNT_692;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6241uL);
      if (buffer == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6242uL);
        throw new ArgumentNullException("buffer");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6243uL);
      if (count < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6244uL);
        throw new ArgumentOutOfRangeException("count");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6245uL);
      if (offset < buffer.GetLowerBound(0)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6246uL);
        throw new ArgumentOutOfRangeException("offset");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6247uL);
      if ((offset + count) > buffer.GetLength(0)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6248uL);
        throw new ArgumentOutOfRangeException("count");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6249uL);
      int rc = 0;
      _z.OutputBuffer = buffer;
      _z.NextOut = offset;
      _z.AvailableBytesOut = count;
      _z.InputBuffer = workingBuffer;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6250uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6251uL);
        if ((_z.AvailableBytesIn == 0) && (!nomoreinput)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6252uL);
          _z.NextIn = 0;
          _z.AvailableBytesIn = _stream.Read(_workingBuffer, 0, _workingBuffer.Length);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6253uL);
          if (_z.AvailableBytesIn == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6254uL);
            nomoreinput = true;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6255uL);
        rc = (_wantCompress) ? _z.Deflate(_flushMode) : _z.Inflate(_flushMode);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6256uL);
        if (nomoreinput && (rc == ZlibConstants.Z_BUF_ERROR)) {
          System.Int32 RNTRNTRNT_693 = 0;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6257uL);
          return RNTRNTRNT_693;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6258uL);
        if (rc != ZlibConstants.Z_OK && rc != ZlibConstants.Z_STREAM_END) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6259uL);
          throw new ZlibException(String.Format("{0}flating:  rc={1}  msg={2}", (_wantCompress ? "de" : "in"), rc, _z.Message));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6260uL);
        if ((nomoreinput || rc == ZlibConstants.Z_STREAM_END) && (_z.AvailableBytesOut == count)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6261uL);
          break;
        }
      } while (_z.AvailableBytesOut > 0 && !nomoreinput && rc == ZlibConstants.Z_OK);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6262uL);
      if (_z.AvailableBytesOut > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6263uL);
        if (rc == ZlibConstants.Z_OK && _z.AvailableBytesIn == 0) {
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6264uL);
        if (nomoreinput) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6265uL);
          if (_wantCompress) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6266uL);
            rc = _z.Deflate(FlushType.Finish);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6267uL);
            if (rc != ZlibConstants.Z_OK && rc != ZlibConstants.Z_STREAM_END) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6268uL);
              throw new ZlibException(String.Format("Deflating:  rc={0}  msg={1}", rc, _z.Message));
            }
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6269uL);
      rc = (count - _z.AvailableBytesOut);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6270uL);
      if (crc != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6271uL);
        crc.SlurpBlock(buffer, offset, rc);
      }
      System.Int32 RNTRNTRNT_694 = rc;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6272uL);
      return RNTRNTRNT_694;
    }
    public override System.Boolean CanRead {
      get {
        System.Boolean RNTRNTRNT_695 = this._stream.CanRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6273uL);
        return RNTRNTRNT_695;
      }
    }
    public override System.Boolean CanSeek {
      get {
        System.Boolean RNTRNTRNT_696 = this._stream.CanSeek;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6274uL);
        return RNTRNTRNT_696;
      }
    }
    public override System.Boolean CanWrite {
      get {
        System.Boolean RNTRNTRNT_697 = this._stream.CanWrite;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6275uL);
        return RNTRNTRNT_697;
      }
    }
    public override System.Int64 Length {
      get {
        System.Int64 RNTRNTRNT_698 = _stream.Length;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6276uL);
        return RNTRNTRNT_698;
      }
    }
    public override long Position {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6277uL);
        throw new NotImplementedException();
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6278uL);
        throw new NotImplementedException();
      }
    }
    internal enum StreamMode
    {
      Writer,
      Reader,
      Undefined
    }
    public static void CompressString(String s, Stream compressor)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6279uL);
      byte[] uncompressed = System.Text.Encoding.UTF8.GetBytes(s);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6280uL);
      using (compressor) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6281uL);
        compressor.Write(uncompressed, 0, uncompressed.Length);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6282uL);
    }
    public static void CompressBuffer(byte[] b, Stream compressor)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6283uL);
      using (compressor) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6284uL);
        compressor.Write(b, 0, b.Length);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6285uL);
    }
    public static String UncompressString(byte[] compressed, Stream decompressor)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6286uL);
      byte[] working = new byte[1024];
      var encoding = System.Text.Encoding.UTF8;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6287uL);
      using (var output = new MemoryStream()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6288uL);
        using (decompressor) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6289uL);
          int n;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6290uL);
          while ((n = decompressor.Read(working, 0, working.Length)) != 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6291uL);
            output.Write(working, 0, n);
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6292uL);
        output.Seek(0, SeekOrigin.Begin);
        var sr = new StreamReader(output, encoding);
        String RNTRNTRNT_699 = sr.ReadToEnd();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6293uL);
        return RNTRNTRNT_699;
      }
    }
    public static byte[] UncompressBuffer(byte[] compressed, Stream decompressor)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6294uL);
      byte[] working = new byte[1024];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6295uL);
      using (var output = new MemoryStream()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6296uL);
        using (decompressor) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6297uL);
          int n;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6298uL);
          while ((n = decompressor.Read(working, 0, working.Length)) != 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6299uL);
            output.Write(working, 0, n);
          }
        }
        System.Byte[] RNTRNTRNT_700 = output.ToArray();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6300uL);
        return RNTRNTRNT_700;
      }
    }
  }
}
