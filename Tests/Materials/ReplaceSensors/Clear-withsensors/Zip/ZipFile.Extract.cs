using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public partial class ZipFile
  {
    public void ExtractAll(string path)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3008uL);
      _InternalExtractAll(path, true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3009uL);
    }
    public void ExtractAll(string path, ExtractExistingFileAction extractExistingFile)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3010uL);
      ExtractExistingFile = extractExistingFile;
      _InternalExtractAll(path, true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3011uL);
    }
    private void _InternalExtractAll(string path, bool overrideExtractExistingProperty)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3012uL);
      bool header = Verbose;
      _inExtractAll = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3013uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3014uL);
        OnExtractAllStarted(path);
        int n = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3015uL);
        foreach (ZipEntry e in _entries.Values) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3016uL);
          if (header) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3017uL);
            StatusMessageTextWriter.WriteLine("\n{1,-22} {2,-8} {3,4}   {4,-8}  {0}", "Name", "Modified", "Size", "Ratio", "Packed");
            StatusMessageTextWriter.WriteLine(new System.String('-', 72));
            header = false;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3018uL);
          if (Verbose) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3019uL);
            StatusMessageTextWriter.WriteLine("{1,-22} {2,-8} {3,4:F0}%   {4,-8} {0}", e.FileName, e.LastModified.ToString("yyyy-MM-dd HH:mm:ss"), e.UncompressedSize, e.CompressionRatio, e.CompressedSize);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3020uL);
            if (!String.IsNullOrEmpty(e.Comment)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3021uL);
              StatusMessageTextWriter.WriteLine("  Comment: {0}", e.Comment);
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3022uL);
          e.Password = _Password;
          OnExtractEntry(n, true, e, path);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3023uL);
          if (overrideExtractExistingProperty) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3024uL);
            e.ExtractExistingFile = this.ExtractExistingFile;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3025uL);
          e.Extract(path);
          n++;
          OnExtractEntry(n, false, e, path);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3026uL);
          if (_extractOperationCanceled) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3027uL);
            break;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3028uL);
        if (!_extractOperationCanceled) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3029uL);
          foreach (ZipEntry e in _entries.Values) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3030uL);
            if ((e.IsDirectory) || (e.FileName.EndsWith("/"))) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3031uL);
              string outputFile = (e.FileName.StartsWith("/")) ? Path.Combine(path, e.FileName.Substring(1)) : Path.Combine(path, e.FileName);
              e._SetTimes(outputFile, false);
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3032uL);
          OnExtractAllCompleted(path);
        }
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3033uL);
        _inExtractAll = false;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3034uL);
    }
  }
}
