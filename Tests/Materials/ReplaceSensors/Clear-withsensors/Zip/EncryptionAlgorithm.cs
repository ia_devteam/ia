namespace Ionic.Zip
{
  public enum EncryptionAlgorithm
  {
    None = 0,
    PkzipWeak,
    WinZipAes128,
    WinZipAes256,
    Unsupported = 4
  }
}
