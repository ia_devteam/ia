using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
namespace Ionic.Zip
{
  [Serializable()]
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d0000B")]
  public class BadPasswordException : ZipException
  {
    public BadPasswordException()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(923uL);
    }
    public BadPasswordException(String message) : base(message)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(924uL);
    }
    public BadPasswordException(String message, Exception innerException) : base(message, innerException)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(925uL);
    }
    protected BadPasswordException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(926uL);
    }
  }
  [Serializable()]
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d0000A")]
  public class BadReadException : ZipException
  {
    public BadReadException()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(927uL);
    }
    public BadReadException(String message) : base(message)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(928uL);
    }
    public BadReadException(String message, Exception innerException) : base(message, innerException)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(929uL);
    }
    protected BadReadException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(930uL);
    }
  }
  [Serializable()]
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d00009")]
  public class BadCrcException : ZipException
  {
    public BadCrcException()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(931uL);
    }
    public BadCrcException(String message) : base(message)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(932uL);
    }
    protected BadCrcException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(933uL);
    }
  }
  [Serializable()]
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d00008")]
  public class SfxGenerationException : ZipException
  {
    public SfxGenerationException()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(934uL);
    }
    public SfxGenerationException(String message) : base(message)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(935uL);
    }
    protected SfxGenerationException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(936uL);
    }
  }
  [Serializable()]
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d00007")]
  public class BadStateException : ZipException
  {
    public BadStateException()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(937uL);
    }
    public BadStateException(String message) : base(message)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(938uL);
    }
    public BadStateException(String message, Exception innerException) : base(message, innerException)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(939uL);
    }
    protected BadStateException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(940uL);
    }
  }
  [Serializable()]
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d00006")]
  public class ZipException : Exception
  {
    public ZipException()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(941uL);
    }
    public ZipException(String message) : base(message)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(942uL);
    }
    public ZipException(String message, Exception innerException) : base(message, innerException)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(943uL);
    }
    protected ZipException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(944uL);
    }
  }
}
