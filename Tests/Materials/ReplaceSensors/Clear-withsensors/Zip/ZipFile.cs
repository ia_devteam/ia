using System;
using System.IO;
using System.Collections.Generic;
using Interop = System.Runtime.InteropServices;
namespace Ionic.Zip
{
  [Interop.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d00005")]
  [Interop.ComVisible(true)]
  [Interop.ClassInterface(Interop.ClassInterfaceType.AutoDispatch)]
  public partial class ZipFile : System.Collections.IEnumerable, System.Collections.Generic.IEnumerable<ZipEntry>, IDisposable
  {
    public bool FullScan { get; set; }
    public bool SortEntriesBeforeSaving { get; set; }
    public bool AddDirectoryWillTraverseReparsePoints { get; set; }
    public int BufferSize {
      get {
        System.Int32 RNTRNTRNT_371 = _BufferSize;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2691uL);
        return RNTRNTRNT_371;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2692uL);
        _BufferSize = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2693uL);
      }
    }
    public int CodecBufferSize { get; set; }
    public bool FlattenFoldersOnExtract { get; set; }
    public Ionic.Zlib.CompressionStrategy Strategy {
      get {
        Ionic.Zlib.CompressionStrategy RNTRNTRNT_372 = _Strategy;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2694uL);
        return RNTRNTRNT_372;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2695uL);
        _Strategy = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2696uL);
      }
    }
    public string Name {
      get {
        System.String RNTRNTRNT_373 = _name;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2697uL);
        return RNTRNTRNT_373;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2698uL);
        _name = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2699uL);
      }
    }
    public Ionic.Zlib.CompressionLevel CompressionLevel { get; set; }
    public Ionic.Zip.CompressionMethod CompressionMethod {
      get {
        Ionic.Zip.CompressionMethod RNTRNTRNT_374 = _compressionMethod;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2700uL);
        return RNTRNTRNT_374;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2701uL);
        _compressionMethod = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2702uL);
      }
    }
    public string Comment {
      get {
        System.String RNTRNTRNT_375 = _Comment;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2703uL);
        return RNTRNTRNT_375;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2704uL);
        _Comment = value;
        _contentsChanged = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2705uL);
      }
    }
    public bool EmitTimesInWindowsFormatWhenSaving {
      get {
        System.Boolean RNTRNTRNT_376 = _emitNtfsTimes;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2706uL);
        return RNTRNTRNT_376;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2707uL);
        _emitNtfsTimes = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2708uL);
      }
    }
    public bool EmitTimesInUnixFormatWhenSaving {
      get {
        System.Boolean RNTRNTRNT_377 = _emitUnixTimes;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2709uL);
        return RNTRNTRNT_377;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2710uL);
        _emitUnixTimes = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2711uL);
      }
    }
    internal bool Verbose {
      get {
        System.Boolean RNTRNTRNT_378 = (_StatusMessageTextWriter != null);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2712uL);
        return RNTRNTRNT_378;
      }
    }
    public bool ContainsEntry(string name)
    {
      System.Boolean RNTRNTRNT_379 = _entries.ContainsKey(SharedUtilities.NormalizePathForUseInZipFile(name));
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2713uL);
      return RNTRNTRNT_379;
    }
    public bool CaseSensitiveRetrieval {
      get {
        System.Boolean RNTRNTRNT_380 = _CaseSensitiveRetrieval;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2714uL);
        return RNTRNTRNT_380;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2715uL);
        if (value != _CaseSensitiveRetrieval) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2716uL);
          _CaseSensitiveRetrieval = value;
          _initEntriesDictionary();
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2717uL);
      }
    }
    [Obsolete("Beginning with v1.9.1.6 of DotNetZip, this property is obsolete.  It will be removed in a future version of the library. Your applications should  use AlternateEncoding and AlternateEncodingUsage instead.")]
    public bool UseUnicodeAsNecessary {
      get {
        System.Boolean RNTRNTRNT_381 = (_alternateEncoding == System.Text.Encoding.GetEncoding("UTF-8")) && (_alternateEncodingUsage == ZipOption.AsNecessary);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2718uL);
        return RNTRNTRNT_381;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2719uL);
        if (value) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2720uL);
          _alternateEncoding = System.Text.Encoding.GetEncoding("UTF-8");
          _alternateEncodingUsage = ZipOption.AsNecessary;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2721uL);
          _alternateEncoding = Ionic.Zip.ZipFile.DefaultEncoding;
          _alternateEncodingUsage = ZipOption.Never;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2722uL);
      }
    }
    public Zip64Option UseZip64WhenSaving {
      get {
        Zip64Option RNTRNTRNT_382 = _zip64;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2723uL);
        return RNTRNTRNT_382;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2724uL);
        _zip64 = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2725uL);
      }
    }
    public Nullable<bool> RequiresZip64 {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2726uL);
        if (_entries.Count > 65534) {
          Nullable<System.Boolean> RNTRNTRNT_383 = new Nullable<bool>(true);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2727uL);
          return RNTRNTRNT_383;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2728uL);
        if (!_hasBeenSaved || _contentsChanged) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2729uL);
          return null;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2730uL);
        foreach (ZipEntry e in _entries.Values) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2731uL);
          if (e.RequiresZip64.Value) {
            Nullable<System.Boolean> RNTRNTRNT_384 = new Nullable<bool>(true);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2732uL);
            return RNTRNTRNT_384;
          }
        }
        Nullable<System.Boolean> RNTRNTRNT_385 = new Nullable<bool>(false);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2733uL);
        return RNTRNTRNT_385;
      }
    }
    public Nullable<bool> OutputUsedZip64 {
      get {
        Nullable<System.Boolean> RNTRNTRNT_386 = _OutputUsesZip64;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2734uL);
        return RNTRNTRNT_386;
      }
    }
    public Nullable<bool> InputUsesZip64 {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2735uL);
        if (_entries.Count > 65534) {
          Nullable<System.Boolean> RNTRNTRNT_387 = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2736uL);
          return RNTRNTRNT_387;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2737uL);
        foreach (ZipEntry e in this) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2738uL);
          if (e.Source != ZipEntrySource.ZipFile) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2739uL);
            return null;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2740uL);
          if (e._InputUsesZip64) {
            Nullable<System.Boolean> RNTRNTRNT_388 = true;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2741uL);
            return RNTRNTRNT_388;
          }
        }
        Nullable<System.Boolean> RNTRNTRNT_389 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2742uL);
        return RNTRNTRNT_389;
      }
    }
    [Obsolete("use AlternateEncoding instead.")]
    public System.Text.Encoding ProvisionalAlternateEncoding {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2743uL);
        if (_alternateEncodingUsage == ZipOption.AsNecessary) {
          System.Text.Encoding RNTRNTRNT_390 = _alternateEncoding;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2744uL);
          return RNTRNTRNT_390;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2745uL);
        return null;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2746uL);
        _alternateEncoding = value;
        _alternateEncodingUsage = ZipOption.AsNecessary;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2747uL);
      }
    }
    public System.Text.Encoding AlternateEncoding {
      get {
        System.Text.Encoding RNTRNTRNT_391 = _alternateEncoding;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2748uL);
        return RNTRNTRNT_391;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2749uL);
        _alternateEncoding = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2750uL);
      }
    }
    public ZipOption AlternateEncodingUsage {
      get {
        ZipOption RNTRNTRNT_392 = _alternateEncodingUsage;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2751uL);
        return RNTRNTRNT_392;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2752uL);
        _alternateEncodingUsage = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2753uL);
      }
    }
    public static System.Text.Encoding DefaultEncoding {
      get {
        System.Text.Encoding RNTRNTRNT_393 = _defaultEncoding;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2754uL);
        return RNTRNTRNT_393;
      }
    }
    public TextWriter StatusMessageTextWriter {
      get {
        TextWriter RNTRNTRNT_394 = _StatusMessageTextWriter;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2755uL);
        return RNTRNTRNT_394;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2756uL);
        _StatusMessageTextWriter = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2757uL);
      }
    }
    public String TempFileFolder {
      get {
        String RNTRNTRNT_395 = _TempFileFolder;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2758uL);
        return RNTRNTRNT_395;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2759uL);
        _TempFileFolder = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2760uL);
        if (value == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2761uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2762uL);
        if (!Directory.Exists(value)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2763uL);
          throw new FileNotFoundException(String.Format("That directory ({0}) does not exist.", value));
        }
      }
    }
    public String Password {
      private get {
        String RNTRNTRNT_396 = _Password;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2764uL);
        return RNTRNTRNT_396;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2765uL);
        _Password = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2766uL);
        if (_Password == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2767uL);
          Encryption = EncryptionAlgorithm.None;
        } else if (Encryption == EncryptionAlgorithm.None) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2768uL);
          Encryption = EncryptionAlgorithm.PkzipWeak;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2769uL);
      }
    }
    public ExtractExistingFileAction ExtractExistingFile { get; set; }
    public ZipErrorAction ZipErrorAction {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2770uL);
        if (ZipError != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2771uL);
          _zipErrorAction = ZipErrorAction.InvokeErrorEvent;
        }
        ZipErrorAction RNTRNTRNT_397 = _zipErrorAction;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2772uL);
        return RNTRNTRNT_397;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2773uL);
        _zipErrorAction = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2774uL);
        if (_zipErrorAction != ZipErrorAction.InvokeErrorEvent && ZipError != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2775uL);
          ZipError = null;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2776uL);
      }
    }
    public EncryptionAlgorithm Encryption {
      get {
        EncryptionAlgorithm RNTRNTRNT_398 = _Encryption;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2777uL);
        return RNTRNTRNT_398;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2778uL);
        if (value == EncryptionAlgorithm.Unsupported) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2779uL);
          throw new InvalidOperationException("You may not set Encryption to that value.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2780uL);
        _Encryption = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2781uL);
      }
    }
    public SetCompressionCallback SetCompression { get; set; }
    public Int32 MaxOutputSegmentSize {
      get {
        Int32 RNTRNTRNT_399 = _maxOutputSegmentSize;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2782uL);
        return RNTRNTRNT_399;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2783uL);
        if (value < 65536 && value != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2784uL);
          throw new ZipException("The minimum acceptable segment size is 65536.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2785uL);
        _maxOutputSegmentSize = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2786uL);
      }
    }
    public Int32 NumberOfSegmentsForMostRecentSave {
      get {
        Int32 RNTRNTRNT_400 = unchecked((Int32)_numberOfSegmentsForMostRecentSave + 1);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2787uL);
        return RNTRNTRNT_400;
      }
    }
    public long ParallelDeflateThreshold {
      get {
        System.Int64 RNTRNTRNT_401 = _ParallelDeflateThreshold;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2788uL);
        return RNTRNTRNT_401;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2789uL);
        if ((value != 0) && (value != -1) && (value < 64 * 1024)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2790uL);
          throw new ArgumentOutOfRangeException("ParallelDeflateThreshold should be -1, 0, or > 65536");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2791uL);
        _ParallelDeflateThreshold = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2792uL);
      }
    }
    public int ParallelDeflateMaxBufferPairs {
      get {
        System.Int32 RNTRNTRNT_402 = _maxBufferPairs;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2793uL);
        return RNTRNTRNT_402;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2794uL);
        if (value < 4) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2795uL);
          throw new ArgumentOutOfRangeException("ParallelDeflateMaxBufferPairs", "Value must be 4 or greater.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2796uL);
        _maxBufferPairs = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2797uL);
      }
    }
    public override String ToString()
    {
      String RNTRNTRNT_403 = String.Format("ZipFile::{0}", Name);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2798uL);
      return RNTRNTRNT_403;
    }
    public static System.Version LibraryVersion {
      get {
        System.Version RNTRNTRNT_404 = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2799uL);
        return RNTRNTRNT_404;
      }
    }
    internal void NotifyEntryChanged()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2800uL);
      _contentsChanged = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2801uL);
    }
    internal Stream StreamForDiskNumber(uint diskNumber)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2802uL);
      if (diskNumber + 1 == this._diskNumberWithCd || (diskNumber == 0 && this._diskNumberWithCd == 0)) {
        Stream RNTRNTRNT_405 = this.ReadStream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2803uL);
        return RNTRNTRNT_405;
      }
      Stream RNTRNTRNT_406 = ZipSegmentedStream.ForReading(this._readName ?? this._name, diskNumber, _diskNumberWithCd);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2804uL);
      return RNTRNTRNT_406;
    }
    internal void Reset(bool whileSaving)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2805uL);
      if (_JustSaved) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2806uL);
        using (ZipFile x = new ZipFile()) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2807uL);
          x._readName = x._name = whileSaving ? (this._readName ?? this._name) : this._name;
          x.AlternateEncoding = this.AlternateEncoding;
          x.AlternateEncodingUsage = this.AlternateEncodingUsage;
          ReadIntoInstance(x);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2808uL);
          foreach (ZipEntry e1 in x) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2809uL);
            foreach (ZipEntry e2 in this) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2810uL);
              if (e1.FileName == e2.FileName) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2811uL);
                e2.CopyMetaData(e1);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2812uL);
                break;
              }
            }
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2813uL);
        _JustSaved = false;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2814uL);
    }
    public ZipFile(string fileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2815uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2816uL);
        _InitInstance(fileName, null);
      } catch (Exception e1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2817uL);
        throw new ZipException(String.Format("Could not read {0} as a zip file", fileName), e1);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2818uL);
    }
    public ZipFile(string fileName, System.Text.Encoding encoding)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2819uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2820uL);
        AlternateEncoding = encoding;
        AlternateEncodingUsage = ZipOption.Always;
        _InitInstance(fileName, null);
      } catch (Exception e1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2821uL);
        throw new ZipException(String.Format("{0} is not a valid zip file", fileName), e1);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2822uL);
    }
    public ZipFile()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2823uL);
      _InitInstance(null, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2824uL);
    }
    public ZipFile(System.Text.Encoding encoding)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2825uL);
      AlternateEncoding = encoding;
      AlternateEncodingUsage = ZipOption.Always;
      _InitInstance(null, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2826uL);
    }
    public ZipFile(string fileName, TextWriter statusMessageWriter)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2827uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2828uL);
        _InitInstance(fileName, statusMessageWriter);
      } catch (Exception e1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2829uL);
        throw new ZipException(String.Format("{0} is not a valid zip file", fileName), e1);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2830uL);
    }
    public ZipFile(string fileName, TextWriter statusMessageWriter, System.Text.Encoding encoding)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2831uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2832uL);
        AlternateEncoding = encoding;
        AlternateEncodingUsage = ZipOption.Always;
        _InitInstance(fileName, statusMessageWriter);
      } catch (Exception e1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2833uL);
        throw new ZipException(String.Format("{0} is not a valid zip file", fileName), e1);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2834uL);
    }
    public void Initialize(string fileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2835uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2836uL);
        _InitInstance(fileName, null);
      } catch (Exception e1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2837uL);
        throw new ZipException(String.Format("{0} is not a valid zip file", fileName), e1);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2838uL);
    }
    private void _initEntriesDictionary()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2839uL);
      StringComparer sc = (CaseSensitiveRetrieval) ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase;
      _entries = (_entries == null) ? new Dictionary<String, ZipEntry>(sc) : new Dictionary<String, ZipEntry>(_entries, sc);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2840uL);
    }
    private void _InitInstance(string zipFileName, TextWriter statusMessageWriter)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2841uL);
      _name = zipFileName;
      _StatusMessageTextWriter = statusMessageWriter;
      _contentsChanged = true;
      AddDirectoryWillTraverseReparsePoints = true;
      CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
      ParallelDeflateThreshold = 512 * 1024;
      _initEntriesDictionary();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2842uL);
      if (File.Exists(_name)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2843uL);
        if (FullScan) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2844uL);
          ReadIntoInstance_Orig(this);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2845uL);
          ReadIntoInstance(this);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2846uL);
        this._fileAlreadyExists = true;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2847uL);
      return;
    }
    private List<ZipEntry> ZipEntriesAsList {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2848uL);
        if (_zipEntriesAsList == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2849uL);
          _zipEntriesAsList = new List<ZipEntry>(_entries.Values);
        }
        List<ZipEntry> RNTRNTRNT_407 = _zipEntriesAsList;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2850uL);
        return RNTRNTRNT_407;
      }
    }
    public ZipEntry this[int ix] {
      get {
        ZipEntry RNTRNTRNT_408 = ZipEntriesAsList[ix];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2851uL);
        return RNTRNTRNT_408;
      }
    }
    public ZipEntry this[String fileName] {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2852uL);
        var key = SharedUtilities.NormalizePathForUseInZipFile(fileName);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2853uL);
        if (_entries.ContainsKey(key)) {
          ZipEntry RNTRNTRNT_409 = _entries[key];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2854uL);
          return RNTRNTRNT_409;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2855uL);
        key = key.Replace("/", "\\");
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2856uL);
        if (_entries.ContainsKey(key)) {
          ZipEntry RNTRNTRNT_410 = _entries[key];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2857uL);
          return RNTRNTRNT_410;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2858uL);
        return null;
      }
    }
    public System.Collections.Generic.ICollection<String> EntryFileNames {
      get {
        System.Collections.Generic.ICollection<String> RNTRNTRNT_411 = _entries.Keys;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2859uL);
        return RNTRNTRNT_411;
      }
    }
    public System.Collections.Generic.ICollection<ZipEntry> Entries {
      get {
        System.Collections.Generic.ICollection<ZipEntry> RNTRNTRNT_412 = _entries.Values;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2860uL);
        return RNTRNTRNT_412;
      }
    }
    public System.Collections.Generic.ICollection<ZipEntry> EntriesSorted {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2861uL);
        var coll = new System.Collections.Generic.List<ZipEntry>();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2862uL);
        foreach (var e in this.Entries) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2863uL);
          coll.Add(e);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2864uL);
        StringComparison sc = (CaseSensitiveRetrieval) ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase;
        coll.Sort((x, y) => { return String.Compare(x.FileName, y.FileName, sc); });
        System.Collections.Generic.ICollection<ZipEntry> RNTRNTRNT_413 = coll.AsReadOnly();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2865uL);
        return RNTRNTRNT_413;
      }
    }
    public int Count {
      get {
        System.Int32 RNTRNTRNT_414 = _entries.Count;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2866uL);
        return RNTRNTRNT_414;
      }
    }
    public void RemoveEntry(ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2867uL);
      if (entry == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2868uL);
        throw new ArgumentNullException("entry");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2869uL);
      _entries.Remove(SharedUtilities.NormalizePathForUseInZipFile(entry.FileName));
      _zipEntriesAsList = null;
      _contentsChanged = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2870uL);
    }
    public void RemoveEntry(String fileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2871uL);
      string modifiedName = ZipEntry.NameInArchive(fileName, null);
      ZipEntry e = this[modifiedName];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2872uL);
      if (e == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2873uL);
        throw new ArgumentException("The entry you specified was not found in the zip archive.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2874uL);
      RemoveEntry(e);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2875uL);
    }
    public void Dispose()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2876uL);
      Dispose(true);
      GC.SuppressFinalize(this);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2877uL);
    }
    protected virtual void Dispose(bool disposeManagedResources)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2878uL);
      if (!this._disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2879uL);
        if (disposeManagedResources) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2880uL);
          if (_ReadStreamIsOurs) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2881uL);
            if (_readstream != null) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2882uL);
              _readstream.Dispose();
              _readstream = null;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2883uL);
          if ((_temporaryFileName != null) && (_name != null)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2884uL);
            if (_writestream != null) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2885uL);
              _writestream.Dispose();
              _writestream = null;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2886uL);
          if (this.ParallelDeflater != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2887uL);
            this.ParallelDeflater.Dispose();
            this.ParallelDeflater = null;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2888uL);
        this._disposed = true;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2889uL);
    }
    internal Stream ReadStream {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2890uL);
        if (_readstream == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2891uL);
          if (_readName != null || _name != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2892uL);
            _readstream = File.Open(_readName ?? _name, FileMode.Open, FileAccess.Read, FileShare.Read | FileShare.Write);
            _ReadStreamIsOurs = true;
          }
        }
        Stream RNTRNTRNT_415 = _readstream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2893uL);
        return RNTRNTRNT_415;
      }
    }
    private Stream WriteStream {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2894uL);
        if (_writestream != null) {
          Stream RNTRNTRNT_416 = _writestream;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2895uL);
          return RNTRNTRNT_416;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2896uL);
        if (_name == null) {
          Stream RNTRNTRNT_417 = _writestream;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2897uL);
          return RNTRNTRNT_417;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2898uL);
        if (_maxOutputSegmentSize != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2899uL);
          _writestream = ZipSegmentedStream.ForWriting(this._name, _maxOutputSegmentSize);
          Stream RNTRNTRNT_418 = _writestream;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2900uL);
          return RNTRNTRNT_418;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2901uL);
        SharedUtilities.CreateAndOpenUniqueTempFile(TempFileFolder ?? Path.GetDirectoryName(_name), out _writestream, out _temporaryFileName);
        Stream RNTRNTRNT_419 = _writestream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2902uL);
        return RNTRNTRNT_419;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2903uL);
        if (value != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2904uL);
          throw new ZipException("Cannot set the stream to a non-null value.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2905uL);
        _writestream = null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2906uL);
      }
    }
    private TextWriter _StatusMessageTextWriter;
    private bool _CaseSensitiveRetrieval;
    private Stream _readstream;
    private Stream _writestream;
    private UInt16 _versionMadeBy;
    private UInt16 _versionNeededToExtract;
    private UInt32 _diskNumberWithCd;
    private Int32 _maxOutputSegmentSize;
    private UInt32 _numberOfSegmentsForMostRecentSave;
    private ZipErrorAction _zipErrorAction;
    private bool _disposed;
    private System.Collections.Generic.Dictionary<String, ZipEntry> _entries;
    private List<ZipEntry> _zipEntriesAsList;
    private string _name;
    private string _readName;
    private string _Comment;
    internal string _Password;
    private bool _emitNtfsTimes = true;
    private bool _emitUnixTimes;
    private Ionic.Zlib.CompressionStrategy _Strategy = Ionic.Zlib.CompressionStrategy.Default;
    private Ionic.Zip.CompressionMethod _compressionMethod = Ionic.Zip.CompressionMethod.Deflate;
    private bool _fileAlreadyExists;
    private string _temporaryFileName;
    private bool _contentsChanged;
    private bool _hasBeenSaved;
    private String _TempFileFolder;
    private bool _ReadStreamIsOurs = true;
    private object LOCK = new object();
    private bool _saveOperationCanceled;
    private bool _extractOperationCanceled;
    private bool _addOperationCanceled;
    private EncryptionAlgorithm _Encryption;
    private bool _JustSaved;
    private long _locEndOfCDS = -1;
    private uint _OffsetOfCentralDirectory;
    private Int64 _OffsetOfCentralDirectory64;
    private Nullable<bool> _OutputUsesZip64;
    internal bool _inExtractAll;
    private System.Text.Encoding _alternateEncoding = System.Text.Encoding.GetEncoding("IBM437");
    private ZipOption _alternateEncodingUsage = ZipOption.Never;
    private static System.Text.Encoding _defaultEncoding = System.Text.Encoding.GetEncoding("IBM437");
    private int _BufferSize = BufferSizeDefault;
    internal Ionic.Zlib.ParallelDeflateOutputStream ParallelDeflater;
    private long _ParallelDeflateThreshold;
    private int _maxBufferPairs = 16;
    internal Zip64Option _zip64 = Zip64Option.Default;
    private bool _SavingSfx;
    public static readonly int BufferSizeDefault = 32768;
  }
  public enum Zip64Option
  {
    Default = 0,
    Never = 0,
    AsNecessary = 1,
    Always
  }
  public enum ZipOption
  {
    Default = 0,
    Never = 0,
    AsNecessary = 1,
    Always
  }
  enum AddOrUpdateAction
  {
    AddOnly = 0,
    AddOrUpdate
  }
}
