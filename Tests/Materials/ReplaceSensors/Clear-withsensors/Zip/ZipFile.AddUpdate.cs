using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public partial class ZipFile
  {
    public ZipEntry AddItem(string fileOrDirectoryName)
    {
      ZipEntry RNTRNTRNT_340 = AddItem(fileOrDirectoryName, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2480uL);
      return RNTRNTRNT_340;
    }
    public ZipEntry AddItem(String fileOrDirectoryName, String directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2481uL);
      if (File.Exists(fileOrDirectoryName)) {
        ZipEntry RNTRNTRNT_341 = AddFile(fileOrDirectoryName, directoryPathInArchive);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2482uL);
        return RNTRNTRNT_341;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2483uL);
      if (Directory.Exists(fileOrDirectoryName)) {
        ZipEntry RNTRNTRNT_342 = AddDirectory(fileOrDirectoryName, directoryPathInArchive);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2484uL);
        return RNTRNTRNT_342;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2485uL);
      throw new FileNotFoundException(String.Format("That file or directory ({0}) does not exist!", fileOrDirectoryName));
    }
    public ZipEntry AddFile(string fileName)
    {
      ZipEntry RNTRNTRNT_343 = AddFile(fileName, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2486uL);
      return RNTRNTRNT_343;
    }
    public ZipEntry AddFile(string fileName, String directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2487uL);
      string nameInArchive = ZipEntry.NameInArchive(fileName, directoryPathInArchive);
      ZipEntry ze = ZipEntry.CreateFromFile(fileName, nameInArchive);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2488uL);
      if (Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2489uL);
        StatusMessageTextWriter.WriteLine("adding {0}...", fileName);
      }
      ZipEntry RNTRNTRNT_344 = _InternalAddEntry(ze);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2490uL);
      return RNTRNTRNT_344;
    }
    public void RemoveEntries(System.Collections.Generic.ICollection<ZipEntry> entriesToRemove)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2491uL);
      if (entriesToRemove == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2492uL);
        throw new ArgumentNullException("entriesToRemove");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2493uL);
      foreach (ZipEntry e in entriesToRemove) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2494uL);
        this.RemoveEntry(e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2495uL);
    }
    public void RemoveEntries(System.Collections.Generic.ICollection<String> entriesToRemove)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2496uL);
      if (entriesToRemove == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2497uL);
        throw new ArgumentNullException("entriesToRemove");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2498uL);
      foreach (String e in entriesToRemove) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2499uL);
        this.RemoveEntry(e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2500uL);
    }
    public void AddFiles(System.Collections.Generic.IEnumerable<String> fileNames)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2501uL);
      this.AddFiles(fileNames, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2502uL);
    }
    public void UpdateFiles(System.Collections.Generic.IEnumerable<String> fileNames)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2503uL);
      this.UpdateFiles(fileNames, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2504uL);
    }
    public void AddFiles(System.Collections.Generic.IEnumerable<String> fileNames, String directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2505uL);
      AddFiles(fileNames, false, directoryPathInArchive);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2506uL);
    }
    public void AddFiles(System.Collections.Generic.IEnumerable<String> fileNames, bool preserveDirHierarchy, String directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2507uL);
      if (fileNames == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2508uL);
        throw new ArgumentNullException("fileNames");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2509uL);
      _addOperationCanceled = false;
      OnAddStarted();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2510uL);
      if (preserveDirHierarchy) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2511uL);
        foreach (var f in fileNames) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2512uL);
          if (_addOperationCanceled) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2513uL);
            break;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2514uL);
          if (directoryPathInArchive != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2515uL);
            string s = Path.GetFullPath(Path.Combine(directoryPathInArchive, Path.GetDirectoryName(f)));
            this.AddFile(f, s);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2516uL);
            this.AddFile(f, null);
          }
        }
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2517uL);
        foreach (var f in fileNames) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2518uL);
          if (_addOperationCanceled) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2519uL);
            break;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2520uL);
          this.AddFile(f, directoryPathInArchive);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2521uL);
      if (!_addOperationCanceled) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2522uL);
        OnAddCompleted();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2523uL);
    }
    public void UpdateFiles(System.Collections.Generic.IEnumerable<String> fileNames, String directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2524uL);
      if (fileNames == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2525uL);
        throw new ArgumentNullException("fileNames");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2526uL);
      OnAddStarted();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2527uL);
      foreach (var f in fileNames) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2528uL);
        this.UpdateFile(f, directoryPathInArchive);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2529uL);
      OnAddCompleted();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2530uL);
    }
    public ZipEntry UpdateFile(string fileName)
    {
      ZipEntry RNTRNTRNT_345 = UpdateFile(fileName, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2531uL);
      return RNTRNTRNT_345;
    }
    public ZipEntry UpdateFile(string fileName, String directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2532uL);
      var key = ZipEntry.NameInArchive(fileName, directoryPathInArchive);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2533uL);
      if (this[key] != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2534uL);
        this.RemoveEntry(key);
      }
      ZipEntry RNTRNTRNT_346 = this.AddFile(fileName, directoryPathInArchive);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2535uL);
      return RNTRNTRNT_346;
    }
    public ZipEntry UpdateDirectory(string directoryName)
    {
      ZipEntry RNTRNTRNT_347 = UpdateDirectory(directoryName, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2536uL);
      return RNTRNTRNT_347;
    }
    public ZipEntry UpdateDirectory(string directoryName, String directoryPathInArchive)
    {
      ZipEntry RNTRNTRNT_348 = this.AddOrUpdateDirectoryImpl(directoryName, directoryPathInArchive, AddOrUpdateAction.AddOrUpdate);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2537uL);
      return RNTRNTRNT_348;
    }
    public void UpdateItem(string itemName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2538uL);
      UpdateItem(itemName, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2539uL);
    }
    public void UpdateItem(string itemName, string directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2540uL);
      if (File.Exists(itemName)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2541uL);
        UpdateFile(itemName, directoryPathInArchive);
      } else if (Directory.Exists(itemName)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2543uL);
        UpdateDirectory(itemName, directoryPathInArchive);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2542uL);
        throw new FileNotFoundException(String.Format("That file or directory ({0}) does not exist!", itemName));
      }
    }
    public ZipEntry AddEntry(string entryName, string content)
    {
      ZipEntry RNTRNTRNT_349 = AddEntry(entryName, content, System.Text.Encoding.Default);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2544uL);
      return RNTRNTRNT_349;
    }
    public ZipEntry AddEntry(string entryName, string content, System.Text.Encoding encoding)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2545uL);
      var ms = new MemoryStream();
      var sw = new StreamWriter(ms, encoding);
      sw.Write(content);
      sw.Flush();
      ms.Seek(0, SeekOrigin.Begin);
      ZipEntry RNTRNTRNT_350 = AddEntry(entryName, ms);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2546uL);
      return RNTRNTRNT_350;
    }
    public ZipEntry AddEntry(string entryName, Stream stream)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2547uL);
      ZipEntry ze = ZipEntry.CreateForStream(entryName, stream);
      ze.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2548uL);
      if (Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2549uL);
        StatusMessageTextWriter.WriteLine("adding {0}...", entryName);
      }
      ZipEntry RNTRNTRNT_351 = _InternalAddEntry(ze);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2550uL);
      return RNTRNTRNT_351;
    }
    public ZipEntry AddEntry(string entryName, WriteDelegate writer)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2551uL);
      ZipEntry ze = ZipEntry.CreateForWriter(entryName, writer);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2552uL);
      if (Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2553uL);
        StatusMessageTextWriter.WriteLine("adding {0}...", entryName);
      }
      ZipEntry RNTRNTRNT_352 = _InternalAddEntry(ze);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2554uL);
      return RNTRNTRNT_352;
    }
    public ZipEntry AddEntry(string entryName, OpenDelegate opener, CloseDelegate closer)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2555uL);
      ZipEntry ze = ZipEntry.CreateForJitStreamProvider(entryName, opener, closer);
      ze.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2556uL);
      if (Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2557uL);
        StatusMessageTextWriter.WriteLine("adding {0}...", entryName);
      }
      ZipEntry RNTRNTRNT_353 = _InternalAddEntry(ze);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2558uL);
      return RNTRNTRNT_353;
    }
    private ZipEntry _InternalAddEntry(ZipEntry ze)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2559uL);
      ze._container = new ZipContainer(this);
      ze.CompressionMethod = this.CompressionMethod;
      ze.CompressionLevel = this.CompressionLevel;
      ze.ExtractExistingFile = this.ExtractExistingFile;
      ze.ZipErrorAction = this.ZipErrorAction;
      ze.SetCompression = this.SetCompression;
      ze.AlternateEncoding = this.AlternateEncoding;
      ze.AlternateEncodingUsage = this.AlternateEncodingUsage;
      ze.Password = this._Password;
      ze.Encryption = this.Encryption;
      ze.EmitTimesInWindowsFormatWhenSaving = this._emitNtfsTimes;
      ze.EmitTimesInUnixFormatWhenSaving = this._emitUnixTimes;
      InternalAddEntry(ze.FileName, ze);
      AfterAddEntry(ze);
      ZipEntry RNTRNTRNT_354 = ze;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2560uL);
      return RNTRNTRNT_354;
    }
    public ZipEntry UpdateEntry(string entryName, string content)
    {
      ZipEntry RNTRNTRNT_355 = UpdateEntry(entryName, content, System.Text.Encoding.Default);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2561uL);
      return RNTRNTRNT_355;
    }
    public ZipEntry UpdateEntry(string entryName, string content, System.Text.Encoding encoding)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2562uL);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_356 = AddEntry(entryName, content, encoding);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2563uL);
      return RNTRNTRNT_356;
    }
    public ZipEntry UpdateEntry(string entryName, WriteDelegate writer)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2564uL);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_357 = AddEntry(entryName, writer);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2565uL);
      return RNTRNTRNT_357;
    }
    public ZipEntry UpdateEntry(string entryName, OpenDelegate opener, CloseDelegate closer)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2566uL);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_358 = AddEntry(entryName, opener, closer);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2567uL);
      return RNTRNTRNT_358;
    }
    public ZipEntry UpdateEntry(string entryName, Stream stream)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2568uL);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_359 = AddEntry(entryName, stream);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2569uL);
      return RNTRNTRNT_359;
    }
    private void RemoveEntryForUpdate(string entryName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2570uL);
      if (String.IsNullOrEmpty(entryName)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2571uL);
        throw new ArgumentNullException("entryName");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2572uL);
      string directoryPathInArchive = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2573uL);
      if (entryName.IndexOf('\\') != -1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2574uL);
        directoryPathInArchive = Path.GetDirectoryName(entryName);
        entryName = Path.GetFileName(entryName);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2575uL);
      var key = ZipEntry.NameInArchive(entryName, directoryPathInArchive);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2576uL);
      if (this[key] != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2577uL);
        this.RemoveEntry(key);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2578uL);
    }
    public ZipEntry AddEntry(string entryName, byte[] byteContent)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2579uL);
      if (byteContent == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2580uL);
        throw new ArgumentException("bad argument", "byteContent");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2581uL);
      var ms = new MemoryStream(byteContent);
      ZipEntry RNTRNTRNT_360 = AddEntry(entryName, ms);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2582uL);
      return RNTRNTRNT_360;
    }
    public ZipEntry UpdateEntry(string entryName, byte[] byteContent)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2583uL);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_361 = AddEntry(entryName, byteContent);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2584uL);
      return RNTRNTRNT_361;
    }
    public ZipEntry AddDirectory(string directoryName)
    {
      ZipEntry RNTRNTRNT_362 = AddDirectory(directoryName, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2585uL);
      return RNTRNTRNT_362;
    }
    public ZipEntry AddDirectory(string directoryName, string directoryPathInArchive)
    {
      ZipEntry RNTRNTRNT_363 = AddOrUpdateDirectoryImpl(directoryName, directoryPathInArchive, AddOrUpdateAction.AddOnly);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2586uL);
      return RNTRNTRNT_363;
    }
    public ZipEntry AddDirectoryByName(string directoryNameInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2587uL);
      ZipEntry dir = ZipEntry.CreateFromNothing(directoryNameInArchive);
      dir._container = new ZipContainer(this);
      dir.MarkAsDirectory();
      dir.AlternateEncoding = this.AlternateEncoding;
      dir.AlternateEncodingUsage = this.AlternateEncodingUsage;
      dir.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
      dir.EmitTimesInWindowsFormatWhenSaving = _emitNtfsTimes;
      dir.EmitTimesInUnixFormatWhenSaving = _emitUnixTimes;
      dir._Source = ZipEntrySource.Stream;
      InternalAddEntry(dir.FileName, dir);
      AfterAddEntry(dir);
      ZipEntry RNTRNTRNT_364 = dir;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2588uL);
      return RNTRNTRNT_364;
    }
    private ZipEntry AddOrUpdateDirectoryImpl(string directoryName, string rootDirectoryPathInArchive, AddOrUpdateAction action)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2589uL);
      if (rootDirectoryPathInArchive == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2590uL);
        rootDirectoryPathInArchive = "";
      }
      ZipEntry RNTRNTRNT_365 = AddOrUpdateDirectoryImpl(directoryName, rootDirectoryPathInArchive, action, true, 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2591uL);
      return RNTRNTRNT_365;
    }
    internal void InternalAddEntry(String name, ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2592uL);
      _entries.Add(name, entry);
      _zipEntriesAsList = null;
      _contentsChanged = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2593uL);
    }
    private ZipEntry AddOrUpdateDirectoryImpl(string directoryName, string rootDirectoryPathInArchive, AddOrUpdateAction action, bool recurse, int level)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2594uL);
      if (Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2595uL);
        StatusMessageTextWriter.WriteLine("{0} {1}...", (action == AddOrUpdateAction.AddOnly) ? "adding" : "Adding or updating", directoryName);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2596uL);
      if (level == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2597uL);
        _addOperationCanceled = false;
        OnAddStarted();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2598uL);
      if (_addOperationCanceled) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2599uL);
        return null;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2600uL);
      string dirForEntries = rootDirectoryPathInArchive;
      ZipEntry baseDir = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2601uL);
      if (level > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2602uL);
        int f = directoryName.Length;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2603uL);
        for (int i = level; i > 0; i--) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2604uL);
          f = directoryName.LastIndexOfAny("/\\".ToCharArray(), f - 1, f - 1);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2605uL);
        dirForEntries = directoryName.Substring(f + 1);
        dirForEntries = Path.Combine(rootDirectoryPathInArchive, dirForEntries);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2606uL);
      if (level > 0 || rootDirectoryPathInArchive != "") {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2607uL);
        baseDir = ZipEntry.CreateFromFile(directoryName, dirForEntries);
        baseDir._container = new ZipContainer(this);
        baseDir.AlternateEncoding = this.AlternateEncoding;
        baseDir.AlternateEncodingUsage = this.AlternateEncodingUsage;
        baseDir.MarkAsDirectory();
        baseDir.EmitTimesInWindowsFormatWhenSaving = _emitNtfsTimes;
        baseDir.EmitTimesInUnixFormatWhenSaving = _emitUnixTimes;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2608uL);
        if (!_entries.ContainsKey(baseDir.FileName)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2609uL);
          InternalAddEntry(baseDir.FileName, baseDir);
          AfterAddEntry(baseDir);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2610uL);
        dirForEntries = baseDir.FileName;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2611uL);
      if (!_addOperationCanceled) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2612uL);
        String[] filenames = Directory.GetFiles(directoryName);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2613uL);
        if (recurse) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2614uL);
          foreach (String filename in filenames) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2615uL);
            if (_addOperationCanceled) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2616uL);
              break;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2617uL);
            if (action == AddOrUpdateAction.AddOnly) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2618uL);
              AddFile(filename, dirForEntries);
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2619uL);
              UpdateFile(filename, dirForEntries);
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2620uL);
          if (!_addOperationCanceled) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2621uL);
            String[] dirnames = Directory.GetDirectories(directoryName);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2622uL);
            foreach (String dir in dirnames) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2623uL);
              FileAttributes fileAttrs = System.IO.File.GetAttributes(dir);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2624uL);
              if (this.AddDirectoryWillTraverseReparsePoints || ((fileAttrs & FileAttributes.ReparsePoint) == 0)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2625uL);
                AddOrUpdateDirectoryImpl(dir, rootDirectoryPathInArchive, action, recurse, level + 1);
              }
            }
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2626uL);
      if (level == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2627uL);
        OnAddCompleted();
      }
      ZipEntry RNTRNTRNT_366 = baseDir;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2628uL);
      return RNTRNTRNT_366;
    }
  }
}
