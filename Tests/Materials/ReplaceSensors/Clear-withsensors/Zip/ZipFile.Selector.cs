using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  partial class ZipFile
  {
    public void AddSelectedFiles(String selectionCriteria)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3355uL);
      this.AddSelectedFiles(selectionCriteria, ".", null, false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3356uL);
    }
    public void AddSelectedFiles(String selectionCriteria, bool recurseDirectories)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3357uL);
      this.AddSelectedFiles(selectionCriteria, ".", null, recurseDirectories);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3358uL);
    }
    public void AddSelectedFiles(String selectionCriteria, String directoryOnDisk)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3359uL);
      this.AddSelectedFiles(selectionCriteria, directoryOnDisk, null, false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3360uL);
    }
    public void AddSelectedFiles(String selectionCriteria, String directoryOnDisk, bool recurseDirectories)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3361uL);
      this.AddSelectedFiles(selectionCriteria, directoryOnDisk, null, recurseDirectories);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3362uL);
    }
    public void AddSelectedFiles(String selectionCriteria, String directoryOnDisk, String directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3363uL);
      this.AddSelectedFiles(selectionCriteria, directoryOnDisk, directoryPathInArchive, false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3364uL);
    }
    public void AddSelectedFiles(String selectionCriteria, String directoryOnDisk, String directoryPathInArchive, bool recurseDirectories)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3365uL);
      _AddOrUpdateSelectedFiles(selectionCriteria, directoryOnDisk, directoryPathInArchive, recurseDirectories, false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3366uL);
    }
    public void UpdateSelectedFiles(String selectionCriteria, String directoryOnDisk, String directoryPathInArchive, bool recurseDirectories)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3367uL);
      _AddOrUpdateSelectedFiles(selectionCriteria, directoryOnDisk, directoryPathInArchive, recurseDirectories, true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3368uL);
    }
    private string EnsureendInSlash(string s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3369uL);
      if (s.EndsWith("\\")) {
        System.String RNTRNTRNT_448 = s;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3370uL);
        return RNTRNTRNT_448;
      }
      System.String RNTRNTRNT_449 = s + "\\";
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3371uL);
      return RNTRNTRNT_449;
    }
    private void _AddOrUpdateSelectedFiles(String selectionCriteria, String directoryOnDisk, String directoryPathInArchive, bool recurseDirectories, bool wantUpdate)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3372uL);
      if (directoryOnDisk == null && (Directory.Exists(selectionCriteria))) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3373uL);
        directoryOnDisk = selectionCriteria;
        selectionCriteria = "*.*";
      } else if (String.IsNullOrEmpty(directoryOnDisk)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3374uL);
        directoryOnDisk = ".";
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3375uL);
      while (directoryOnDisk.EndsWith("\\")) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3376uL);
        directoryOnDisk = directoryOnDisk.Substring(0, directoryOnDisk.Length - 1);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3377uL);
      if (Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3378uL);
        StatusMessageTextWriter.WriteLine("adding selection '{0}' from dir '{1}'...", selectionCriteria, directoryOnDisk);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3379uL);
      Ionic.FileSelector ff = new Ionic.FileSelector(selectionCriteria, AddDirectoryWillTraverseReparsePoints);
      var itemsToAdd = ff.SelectFiles(directoryOnDisk, recurseDirectories);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3380uL);
      if (Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3381uL);
        StatusMessageTextWriter.WriteLine("found {0} files...", itemsToAdd.Count);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3382uL);
      OnAddStarted();
      AddOrUpdateAction action = (wantUpdate) ? AddOrUpdateAction.AddOrUpdate : AddOrUpdateAction.AddOnly;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3383uL);
      foreach (var item in itemsToAdd) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3384uL);
        string dirInArchive = (directoryPathInArchive == null) ? null : ReplaceLeadingDirectory(Path.GetDirectoryName(item), directoryOnDisk, directoryPathInArchive);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3385uL);
        if (File.Exists(item)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3386uL);
          if (wantUpdate) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3387uL);
            this.UpdateFile(item, dirInArchive);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3388uL);
            this.AddFile(item, dirInArchive);
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3389uL);
          AddOrUpdateDirectoryImpl(item, dirInArchive, action, false, 0);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3390uL);
      OnAddCompleted();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3391uL);
    }
    private static string ReplaceLeadingDirectory(string original, string pattern, string replacement)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3392uL);
      string upperString = original.ToUpper();
      string upperPattern = pattern.ToUpper();
      int p1 = upperString.IndexOf(upperPattern);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3393uL);
      if (p1 != 0) {
        System.String RNTRNTRNT_450 = original;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3394uL);
        return RNTRNTRNT_450;
      }
      System.String RNTRNTRNT_451 = replacement + original.Substring(upperPattern.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3395uL);
      return RNTRNTRNT_451;
    }
    public ICollection<ZipEntry> SelectEntries(String selectionCriteria)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3396uL);
      Ionic.FileSelector ff = new Ionic.FileSelector(selectionCriteria, AddDirectoryWillTraverseReparsePoints);
      ICollection<ZipEntry> RNTRNTRNT_452 = ff.SelectEntries(this);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3397uL);
      return RNTRNTRNT_452;
    }
    public ICollection<ZipEntry> SelectEntries(String selectionCriteria, string directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3398uL);
      Ionic.FileSelector ff = new Ionic.FileSelector(selectionCriteria, AddDirectoryWillTraverseReparsePoints);
      ICollection<ZipEntry> RNTRNTRNT_453 = ff.SelectEntries(this, directoryPathInArchive);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3399uL);
      return RNTRNTRNT_453;
    }
    public int RemoveSelectedEntries(String selectionCriteria)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3400uL);
      var selection = this.SelectEntries(selectionCriteria);
      this.RemoveEntries(selection);
      System.Int32 RNTRNTRNT_454 = selection.Count;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3401uL);
      return RNTRNTRNT_454;
    }
    public int RemoveSelectedEntries(String selectionCriteria, string directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3402uL);
      var selection = this.SelectEntries(selectionCriteria, directoryPathInArchive);
      this.RemoveEntries(selection);
      System.Int32 RNTRNTRNT_455 = selection.Count;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3403uL);
      return RNTRNTRNT_455;
    }
    public void ExtractSelectedEntries(String selectionCriteria)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3404uL);
      foreach (ZipEntry e in SelectEntries(selectionCriteria)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3405uL);
        e.Password = _Password;
        e.Extract();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3406uL);
    }
    public void ExtractSelectedEntries(String selectionCriteria, ExtractExistingFileAction extractExistingFile)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3407uL);
      foreach (ZipEntry e in SelectEntries(selectionCriteria)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3408uL);
        e.Password = _Password;
        e.Extract(extractExistingFile);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3409uL);
    }
    public void ExtractSelectedEntries(String selectionCriteria, String directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3410uL);
      foreach (ZipEntry e in SelectEntries(selectionCriteria, directoryPathInArchive)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3411uL);
        e.Password = _Password;
        e.Extract();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3412uL);
    }
    public void ExtractSelectedEntries(String selectionCriteria, string directoryInArchive, string extractDirectory)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3413uL);
      foreach (ZipEntry e in SelectEntries(selectionCriteria, directoryInArchive)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3414uL);
        e.Password = _Password;
        e.Extract(extractDirectory);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3415uL);
    }
    public void ExtractSelectedEntries(String selectionCriteria, string directoryPathInArchive, string extractDirectory, ExtractExistingFileAction extractExistingFile)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3416uL);
      foreach (ZipEntry e in SelectEntries(selectionCriteria, directoryPathInArchive)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3417uL);
        e.Password = _Password;
        e.Extract(extractDirectory, extractExistingFile);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3418uL);
    }
  }
}
namespace Ionic
{
  internal abstract partial class SelectionCriterion
  {
    internal abstract bool Evaluate(Ionic.Zip.ZipEntry entry);
  }
  internal partial class NameCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3419uL);
      string transformedFileName = entry.FileName.Replace("/", "\\");
      System.Boolean RNTRNTRNT_456 = _Evaluate(transformedFileName);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3420uL);
      return RNTRNTRNT_456;
    }
  }
  internal partial class SizeCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      System.Boolean RNTRNTRNT_457 = _Evaluate(entry.UncompressedSize);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3421uL);
      return RNTRNTRNT_457;
    }
  }
  internal partial class TimeCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3422uL);
      DateTime x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3423uL);
      switch (Which) {
        case WhichTime.atime:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3426uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3424uL);
            x = entry.AccessedTime;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3425uL);
            break;
          }

        case WhichTime.mtime:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3429uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3427uL);
            x = entry.ModifiedTime;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3428uL);
            break;
          }

        case WhichTime.ctime:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3432uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3430uL);
            x = entry.CreationTime;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3431uL);
            break;
          }

        default:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3434uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3433uL);
            throw new ArgumentException("??time");
          }

      }
      System.Boolean RNTRNTRNT_458 = _Evaluate(x);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3435uL);
      return RNTRNTRNT_458;
    }
  }
  internal partial class TypeCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3436uL);
      bool result = (ObjectType == 'D') ? entry.IsDirectory : !entry.IsDirectory;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3437uL);
      if (Operator != ComparisonOperator.EqualTo) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3438uL);
        result = !result;
      }
      System.Boolean RNTRNTRNT_459 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3439uL);
      return RNTRNTRNT_459;
    }
  }
  internal partial class AttributesCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3440uL);
      FileAttributes fileAttrs = entry.Attributes;
      System.Boolean RNTRNTRNT_460 = _Evaluate(fileAttrs);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3441uL);
      return RNTRNTRNT_460;
    }
  }
  internal partial class CompoundCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3442uL);
      bool result = Left.Evaluate(entry);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3443uL);
      switch (Conjunction) {
        case LogicalConjunction.AND:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3447uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3444uL);
            if (result) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3445uL);
              result = Right.Evaluate(entry);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3446uL);
            break;
          }

        case LogicalConjunction.OR:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3451uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3448uL);
            if (!result) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3449uL);
              result = Right.Evaluate(entry);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3450uL);
            break;
          }

        case LogicalConjunction.XOR:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3454uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3452uL);
            result ^= Right.Evaluate(entry);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3453uL);
            break;
          }

      }
      System.Boolean RNTRNTRNT_461 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3455uL);
      return RNTRNTRNT_461;
    }
  }
  public partial class FileSelector
  {
    private bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3456uL);
      bool result = _Criterion.Evaluate(entry);
      System.Boolean RNTRNTRNT_462 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3457uL);
      return RNTRNTRNT_462;
    }
    public ICollection<Ionic.Zip.ZipEntry> SelectEntries(Ionic.Zip.ZipFile zip)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3458uL);
      if (zip == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3459uL);
        throw new ArgumentNullException("zip");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3460uL);
      var list = new List<Ionic.Zip.ZipEntry>();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3461uL);
      foreach (Ionic.Zip.ZipEntry e in zip) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3462uL);
        if (this.Evaluate(e)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3463uL);
          list.Add(e);
        }
      }
      ICollection<Ionic.Zip.ZipEntry> RNTRNTRNT_463 = list;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3464uL);
      return RNTRNTRNT_463;
    }
    public ICollection<Ionic.Zip.ZipEntry> SelectEntries(Ionic.Zip.ZipFile zip, string directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3465uL);
      if (zip == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3466uL);
        throw new ArgumentNullException("zip");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3467uL);
      var list = new List<Ionic.Zip.ZipEntry>();
      string slashSwapped = (directoryPathInArchive == null) ? null : directoryPathInArchive.Replace("/", "\\");
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3468uL);
      if (slashSwapped != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3469uL);
        while (slashSwapped.EndsWith("\\")) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3470uL);
          slashSwapped = slashSwapped.Substring(0, slashSwapped.Length - 1);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3471uL);
      foreach (Ionic.Zip.ZipEntry e in zip) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3472uL);
        if (directoryPathInArchive == null || (Path.GetDirectoryName(e.FileName) == directoryPathInArchive) || (Path.GetDirectoryName(e.FileName) == slashSwapped)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3473uL);
          if (this.Evaluate(e)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3474uL);
            list.Add(e);
          }
        }
      }
      ICollection<Ionic.Zip.ZipEntry> RNTRNTRNT_464 = list;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3475uL);
      return RNTRNTRNT_464;
    }
  }
}
