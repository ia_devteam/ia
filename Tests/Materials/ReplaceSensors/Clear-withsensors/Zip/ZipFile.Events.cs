using System;
using System.IO;
namespace Ionic.Zip
{
  public partial class ZipFile
  {
    private string ArchiveNameForEvent {
      get {
        System.String RNTRNTRNT_420 = (_name != null) ? _name : "(stream)";
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2907uL);
        return RNTRNTRNT_420;
      }
    }
    public event EventHandler<SaveProgressEventArgs> SaveProgress;
    internal bool OnSaveBlock(ZipEntry entry, Int64 bytesXferred, Int64 totalBytesToXfer)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2908uL);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2909uL);
      if (sp != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2910uL);
        var e = SaveProgressEventArgs.ByteUpdate(ArchiveNameForEvent, entry, bytesXferred, totalBytesToXfer);
        sp(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2911uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2912uL);
          _saveOperationCanceled = true;
        }
      }
      System.Boolean RNTRNTRNT_421 = _saveOperationCanceled;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2913uL);
      return RNTRNTRNT_421;
    }
    private void OnSaveEntry(int current, ZipEntry entry, bool before)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2914uL);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2915uL);
      if (sp != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2916uL);
        var e = new SaveProgressEventArgs(ArchiveNameForEvent, before, _entries.Count, current, entry);
        sp(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2917uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2918uL);
          _saveOperationCanceled = true;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2919uL);
    }
    private void OnSaveEvent(ZipProgressEventType eventFlavor)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2920uL);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2921uL);
      if (sp != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2922uL);
        var e = new SaveProgressEventArgs(ArchiveNameForEvent, eventFlavor);
        sp(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2923uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2924uL);
          _saveOperationCanceled = true;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2925uL);
    }
    private void OnSaveStarted()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2926uL);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2927uL);
      if (sp != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2928uL);
        var e = SaveProgressEventArgs.Started(ArchiveNameForEvent);
        sp(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2929uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2930uL);
          _saveOperationCanceled = true;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2931uL);
    }
    private void OnSaveCompleted()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2932uL);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2933uL);
      if (sp != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2934uL);
        var e = SaveProgressEventArgs.Completed(ArchiveNameForEvent);
        sp(this, e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2935uL);
    }
    public event EventHandler<ReadProgressEventArgs> ReadProgress;
    private void OnReadStarted()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2936uL);
      EventHandler<ReadProgressEventArgs> rp = ReadProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2937uL);
      if (rp != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2938uL);
        var e = ReadProgressEventArgs.Started(ArchiveNameForEvent);
        rp(this, e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2939uL);
    }
    private void OnReadCompleted()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2940uL);
      EventHandler<ReadProgressEventArgs> rp = ReadProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2941uL);
      if (rp != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2942uL);
        var e = ReadProgressEventArgs.Completed(ArchiveNameForEvent);
        rp(this, e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2943uL);
    }
    internal void OnReadBytes(ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2944uL);
      EventHandler<ReadProgressEventArgs> rp = ReadProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2945uL);
      if (rp != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2946uL);
        var e = ReadProgressEventArgs.ByteUpdate(ArchiveNameForEvent, entry, ReadStream.Position, LengthOfReadStream);
        rp(this, e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2947uL);
    }
    internal void OnReadEntry(bool before, ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2948uL);
      EventHandler<ReadProgressEventArgs> rp = ReadProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2949uL);
      if (rp != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2950uL);
        ReadProgressEventArgs e = (before) ? ReadProgressEventArgs.Before(ArchiveNameForEvent, _entries.Count) : ReadProgressEventArgs.After(ArchiveNameForEvent, entry, _entries.Count);
        rp(this, e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2951uL);
    }
    private Int64 _lengthOfReadStream = -99;
    private Int64 LengthOfReadStream {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2952uL);
        if (_lengthOfReadStream == -99) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2953uL);
          _lengthOfReadStream = (_ReadStreamIsOurs) ? SharedUtilities.GetFileLength(_name) : -1L;
        }
        Int64 RNTRNTRNT_422 = _lengthOfReadStream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2954uL);
        return RNTRNTRNT_422;
      }
    }
    public event EventHandler<ExtractProgressEventArgs> ExtractProgress;
    private void OnExtractEntry(int current, bool before, ZipEntry currentEntry, string path)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2955uL);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2956uL);
      if (ep != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2957uL);
        var e = new ExtractProgressEventArgs(ArchiveNameForEvent, before, _entries.Count, current, currentEntry, path);
        ep(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2958uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2959uL);
          _extractOperationCanceled = true;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2960uL);
    }
    internal bool OnExtractBlock(ZipEntry entry, Int64 bytesWritten, Int64 totalBytesToWrite)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2961uL);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2962uL);
      if (ep != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2963uL);
        var e = ExtractProgressEventArgs.ByteUpdate(ArchiveNameForEvent, entry, bytesWritten, totalBytesToWrite);
        ep(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2964uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2965uL);
          _extractOperationCanceled = true;
        }
      }
      System.Boolean RNTRNTRNT_423 = _extractOperationCanceled;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2966uL);
      return RNTRNTRNT_423;
    }
    internal bool OnSingleEntryExtract(ZipEntry entry, string path, bool before)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2967uL);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2968uL);
      if (ep != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2969uL);
        var e = (before) ? ExtractProgressEventArgs.BeforeExtractEntry(ArchiveNameForEvent, entry, path) : ExtractProgressEventArgs.AfterExtractEntry(ArchiveNameForEvent, entry, path);
        ep(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2970uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2971uL);
          _extractOperationCanceled = true;
        }
      }
      System.Boolean RNTRNTRNT_424 = _extractOperationCanceled;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2972uL);
      return RNTRNTRNT_424;
    }
    internal bool OnExtractExisting(ZipEntry entry, string path)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2973uL);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2974uL);
      if (ep != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2975uL);
        var e = ExtractProgressEventArgs.ExtractExisting(ArchiveNameForEvent, entry, path);
        ep(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2976uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2977uL);
          _extractOperationCanceled = true;
        }
      }
      System.Boolean RNTRNTRNT_425 = _extractOperationCanceled;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2978uL);
      return RNTRNTRNT_425;
    }
    private void OnExtractAllCompleted(string path)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2979uL);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2980uL);
      if (ep != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2981uL);
        var e = ExtractProgressEventArgs.ExtractAllCompleted(ArchiveNameForEvent, path);
        ep(this, e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2982uL);
    }
    private void OnExtractAllStarted(string path)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2983uL);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2984uL);
      if (ep != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2985uL);
        var e = ExtractProgressEventArgs.ExtractAllStarted(ArchiveNameForEvent, path);
        ep(this, e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2986uL);
    }
    public event EventHandler<AddProgressEventArgs> AddProgress;
    private void OnAddStarted()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2987uL);
      EventHandler<AddProgressEventArgs> ap = AddProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2988uL);
      if (ap != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2989uL);
        var e = AddProgressEventArgs.Started(ArchiveNameForEvent);
        ap(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2990uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2991uL);
          _addOperationCanceled = true;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2992uL);
    }
    private void OnAddCompleted()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2993uL);
      EventHandler<AddProgressEventArgs> ap = AddProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2994uL);
      if (ap != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2995uL);
        var e = AddProgressEventArgs.Completed(ArchiveNameForEvent);
        ap(this, e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2996uL);
    }
    internal void AfterAddEntry(ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2997uL);
      EventHandler<AddProgressEventArgs> ap = AddProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2998uL);
      if (ap != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2999uL);
        var e = AddProgressEventArgs.AfterEntry(ArchiveNameForEvent, entry, _entries.Count);
        ap(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3000uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3001uL);
          _addOperationCanceled = true;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3002uL);
    }
    public event EventHandler<ZipErrorEventArgs> ZipError;
    internal bool OnZipErrorSaving(ZipEntry entry, Exception exc)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3003uL);
      if (ZipError != null) {
        lock (LOCK) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3004uL);
          var e = ZipErrorEventArgs.Saving(this.Name, entry, exc);
          ZipError(this, e);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3005uL);
          if (e.Cancel) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3006uL);
            _saveOperationCanceled = true;
          }
        }
      }
      System.Boolean RNTRNTRNT_426 = _saveOperationCanceled;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3007uL);
      return RNTRNTRNT_426;
    }
  }
}
