using System;
using System.IO;
using System.Collections.Generic;
using System.Security.Cryptography;
namespace Ionic.Zip
{
  internal class WinZipAesCrypto
  {
    internal byte[] _Salt;
    internal byte[] _providedPv;
    internal byte[] _generatedPv;
    internal int _KeyStrengthInBits;
    private byte[] _MacInitializationVector;
    private byte[] _StoredMac;
    private byte[] _keyBytes;
    private Int16 PasswordVerificationStored;
    private Int16 PasswordVerificationGenerated;
    private int Rfc2898KeygenIterations = 1000;
    private string _Password;
    private bool _cryptoGenerated;
    private WinZipAesCrypto(string password, int KeyStrengthInBits)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(769uL);
      _Password = password;
      _KeyStrengthInBits = KeyStrengthInBits;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(770uL);
    }
    public static WinZipAesCrypto Generate(string password, int KeyStrengthInBits)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(771uL);
      WinZipAesCrypto c = new WinZipAesCrypto(password, KeyStrengthInBits);
      int saltSizeInBytes = c._KeyStrengthInBytes / 2;
      c._Salt = new byte[saltSizeInBytes];
      Random rnd = new Random();
      rnd.NextBytes(c._Salt);
      WinZipAesCrypto RNTRNTRNT_133 = c;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(772uL);
      return RNTRNTRNT_133;
    }
    public static WinZipAesCrypto ReadFromStream(string password, int KeyStrengthInBits, Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(773uL);
      WinZipAesCrypto c = new WinZipAesCrypto(password, KeyStrengthInBits);
      int saltSizeInBytes = c._KeyStrengthInBytes / 2;
      c._Salt = new byte[saltSizeInBytes];
      c._providedPv = new byte[2];
      s.Read(c._Salt, 0, c._Salt.Length);
      s.Read(c._providedPv, 0, c._providedPv.Length);
      c.PasswordVerificationStored = (Int16)(c._providedPv[0] + c._providedPv[1] * 256);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(774uL);
      if (password != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(775uL);
        c.PasswordVerificationGenerated = (Int16)(c.GeneratedPV[0] + c.GeneratedPV[1] * 256);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(776uL);
        if (c.PasswordVerificationGenerated != c.PasswordVerificationStored) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(777uL);
          throw new BadPasswordException("bad password");
        }
      }
      WinZipAesCrypto RNTRNTRNT_134 = c;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(778uL);
      return RNTRNTRNT_134;
    }
    public byte[] GeneratedPV {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(779uL);
        if (!_cryptoGenerated) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(780uL);
          _GenerateCryptoBytes();
        }
        System.Byte[] RNTRNTRNT_135 = _generatedPv;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(781uL);
        return RNTRNTRNT_135;
      }
    }
    public byte[] Salt {
      get {
        System.Byte[] RNTRNTRNT_136 = _Salt;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(782uL);
        return RNTRNTRNT_136;
      }
    }
    private int _KeyStrengthInBytes {
      get {
        System.Int32 RNTRNTRNT_137 = _KeyStrengthInBits / 8;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(783uL);
        return RNTRNTRNT_137;
      }
    }
    public int SizeOfEncryptionMetadata {
      get {
        System.Int32 RNTRNTRNT_138 = _KeyStrengthInBytes / 2 + 10 + 2;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(784uL);
        return RNTRNTRNT_138;
      }
    }
    public string Password {
      private get {
        System.String RNTRNTRNT_139 = _Password;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(785uL);
        return RNTRNTRNT_139;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(786uL);
        _Password = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(787uL);
        if (_Password != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(788uL);
          PasswordVerificationGenerated = (Int16)(GeneratedPV[0] + GeneratedPV[1] * 256);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(789uL);
          if (PasswordVerificationGenerated != PasswordVerificationStored) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(790uL);
            throw new Ionic.Zip.BadPasswordException();
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(791uL);
      }
    }
    private void _GenerateCryptoBytes()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(792uL);
      System.Security.Cryptography.Rfc2898DeriveBytes rfc2898 = new System.Security.Cryptography.Rfc2898DeriveBytes(_Password, Salt, Rfc2898KeygenIterations);
      _keyBytes = rfc2898.GetBytes(_KeyStrengthInBytes);
      _MacInitializationVector = rfc2898.GetBytes(_KeyStrengthInBytes);
      _generatedPv = rfc2898.GetBytes(2);
      _cryptoGenerated = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(793uL);
    }
    public byte[] KeyBytes {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(794uL);
        if (!_cryptoGenerated) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(795uL);
          _GenerateCryptoBytes();
        }
        System.Byte[] RNTRNTRNT_140 = _keyBytes;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(796uL);
        return RNTRNTRNT_140;
      }
    }
    public byte[] MacIv {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(797uL);
        if (!_cryptoGenerated) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(798uL);
          _GenerateCryptoBytes();
        }
        System.Byte[] RNTRNTRNT_141 = _MacInitializationVector;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(799uL);
        return RNTRNTRNT_141;
      }
    }
    public byte[] CalculatedMac;
    public void ReadAndVerifyMac(System.IO.Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(800uL);
      bool invalid = false;
      _StoredMac = new byte[10];
      s.Read(_StoredMac, 0, _StoredMac.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(801uL);
      if (_StoredMac.Length != CalculatedMac.Length) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(802uL);
        invalid = true;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(803uL);
      if (!invalid) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(804uL);
        for (int i = 0; i < _StoredMac.Length; i++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(805uL);
          if (_StoredMac[i] != CalculatedMac[i]) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(806uL);
            invalid = true;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(807uL);
      if (invalid) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(808uL);
        throw new Ionic.Zip.BadStateException("The MAC does not match.");
      }
    }
  }
  internal class WinZipAesCipherStream : Stream
  {
    private WinZipAesCrypto _params;
    private System.IO.Stream _s;
    private CryptoMode _mode;
    private int _nonce;
    private bool _finalBlock;
    internal HMACSHA1 _mac;
    internal RijndaelManaged _aesCipher;
    internal ICryptoTransform _xform;
    private const int BLOCK_SIZE_IN_BYTES = 16;
    private byte[] counter = new byte[BLOCK_SIZE_IN_BYTES];
    private byte[] counterOut = new byte[BLOCK_SIZE_IN_BYTES];
    private long _length;
    private long _totalBytesXferred;
    private byte[] _PendingWriteBlock;
    private int _pendingCount;
    private byte[] _iobuf;
    internal WinZipAesCipherStream(System.IO.Stream s, WinZipAesCrypto cryptoParams, long length, CryptoMode mode) : this(s, cryptoParams, mode)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(809uL);
      _length = length;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(810uL);
    }
    internal WinZipAesCipherStream(System.IO.Stream s, WinZipAesCrypto cryptoParams, CryptoMode mode) : base()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(811uL);
      TraceOutput("-------------------------------------------------------");
      TraceOutput("Create {0:X8}", this.GetHashCode());
      _params = cryptoParams;
      _s = s;
      _mode = mode;
      _nonce = 1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(812uL);
      if (_params == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(813uL);
        throw new BadPasswordException("Supply a password to use AES encryption.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(814uL);
      int keySizeInBits = _params.KeyBytes.Length * 8;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(815uL);
      if (keySizeInBits != 256 && keySizeInBits != 128 && keySizeInBits != 192) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(816uL);
        throw new ArgumentOutOfRangeException("keysize", "size of key must be 128, 192, or 256");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(817uL);
      _mac = new HMACSHA1(_params.MacIv);
      _aesCipher = new System.Security.Cryptography.RijndaelManaged();
      _aesCipher.BlockSize = 128;
      _aesCipher.KeySize = keySizeInBits;
      _aesCipher.Mode = CipherMode.ECB;
      _aesCipher.Padding = PaddingMode.None;
      byte[] iv = new byte[BLOCK_SIZE_IN_BYTES];
      _xform = _aesCipher.CreateEncryptor(_params.KeyBytes, iv);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(818uL);
      if (_mode == CryptoMode.Encrypt) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(819uL);
        _iobuf = new byte[2048];
        _PendingWriteBlock = new byte[BLOCK_SIZE_IN_BYTES];
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(820uL);
    }
    private void XorInPlace(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(821uL);
      for (int i = 0; i < count; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(822uL);
        buffer[offset + i] = (byte)(counterOut[i] ^ buffer[offset + i]);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(823uL);
    }
    private void WriteTransformOneBlock(byte[] buffer, int offset)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(824uL);
      System.Array.Copy(BitConverter.GetBytes(_nonce++), 0, counter, 0, 4);
      _xform.TransformBlock(counter, 0, BLOCK_SIZE_IN_BYTES, counterOut, 0);
      XorInPlace(buffer, offset, BLOCK_SIZE_IN_BYTES);
      _mac.TransformBlock(buffer, offset, BLOCK_SIZE_IN_BYTES, null, 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(825uL);
    }
    private void WriteTransformBlocks(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(826uL);
      int posn = offset;
      int last = count + offset;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(827uL);
      while (posn < buffer.Length && posn < last) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(828uL);
        WriteTransformOneBlock(buffer, posn);
        posn += BLOCK_SIZE_IN_BYTES;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(829uL);
    }
    private void WriteTransformFinalBlock()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(830uL);
      if (_pendingCount == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(831uL);
        throw new InvalidOperationException("No bytes available.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(832uL);
      if (_finalBlock) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(833uL);
        throw new InvalidOperationException("The final block has already been transformed.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(834uL);
      System.Array.Copy(BitConverter.GetBytes(_nonce++), 0, counter, 0, 4);
      counterOut = _xform.TransformFinalBlock(counter, 0, BLOCK_SIZE_IN_BYTES);
      XorInPlace(_PendingWriteBlock, 0, _pendingCount);
      _mac.TransformFinalBlock(_PendingWriteBlock, 0, _pendingCount);
      _finalBlock = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(835uL);
    }
    private int ReadTransformOneBlock(byte[] buffer, int offset, int last)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(836uL);
      if (_finalBlock) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(837uL);
        throw new NotSupportedException();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(838uL);
      int bytesRemaining = last - offset;
      int bytesToRead = (bytesRemaining > BLOCK_SIZE_IN_BYTES) ? BLOCK_SIZE_IN_BYTES : bytesRemaining;
      System.Array.Copy(BitConverter.GetBytes(_nonce++), 0, counter, 0, 4);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(839uL);
      if ((bytesToRead == bytesRemaining) && (_length > 0) && (_totalBytesXferred + last == _length)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(840uL);
        _mac.TransformFinalBlock(buffer, offset, bytesToRead);
        counterOut = _xform.TransformFinalBlock(counter, 0, BLOCK_SIZE_IN_BYTES);
        _finalBlock = true;
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(841uL);
        _mac.TransformBlock(buffer, offset, bytesToRead, null, 0);
        _xform.TransformBlock(counter, 0, BLOCK_SIZE_IN_BYTES, counterOut, 0);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(842uL);
      XorInPlace(buffer, offset, bytesToRead);
      System.Int32 RNTRNTRNT_142 = bytesToRead;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(843uL);
      return RNTRNTRNT_142;
    }
    private void ReadTransformBlocks(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(844uL);
      int posn = offset;
      int last = count + offset;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(845uL);
      while (posn < buffer.Length && posn < last) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(846uL);
        int n = ReadTransformOneBlock(buffer, posn, last);
        posn += n;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(847uL);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(848uL);
      if (_mode == CryptoMode.Encrypt) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(849uL);
        throw new NotSupportedException();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(850uL);
      if (buffer == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(851uL);
        throw new ArgumentNullException("buffer");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(852uL);
      if (offset < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(853uL);
        throw new ArgumentOutOfRangeException("offset", "Must not be less than zero.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(854uL);
      if (count < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(855uL);
        throw new ArgumentOutOfRangeException("count", "Must not be less than zero.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(856uL);
      if (buffer.Length < offset + count) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(857uL);
        throw new ArgumentException("The buffer is too small");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(858uL);
      int bytesToRead = count;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(859uL);
      if (_totalBytesXferred >= _length) {
        System.Int32 RNTRNTRNT_143 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(860uL);
        return RNTRNTRNT_143;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(861uL);
      long bytesRemaining = _length - _totalBytesXferred;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(862uL);
      if (bytesRemaining < count) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(863uL);
        bytesToRead = (int)bytesRemaining;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(864uL);
      int n = _s.Read(buffer, offset, bytesToRead);
      ReadTransformBlocks(buffer, offset, bytesToRead);
      _totalBytesXferred += n;
      System.Int32 RNTRNTRNT_144 = n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(865uL);
      return RNTRNTRNT_144;
    }
    public byte[] FinalAuthentication {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(866uL);
        if (!_finalBlock) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(867uL);
          if (_totalBytesXferred != 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(868uL);
            throw new BadStateException("The final hash has not been computed.");
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(869uL);
          byte[] b = {
            
          };
          _mac.ComputeHash(b);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(870uL);
        byte[] macBytes10 = new byte[10];
        System.Array.Copy(_mac.Hash, 0, macBytes10, 0, 10);
        System.Byte[] RNTRNTRNT_145 = macBytes10;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(871uL);
        return RNTRNTRNT_145;
      }
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(872uL);
      if (_finalBlock) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(873uL);
        throw new InvalidOperationException("The final block has already been transformed.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(874uL);
      if (_mode == CryptoMode.Decrypt) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(875uL);
        throw new NotSupportedException();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(876uL);
      if (buffer == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(877uL);
        throw new ArgumentNullException("buffer");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(878uL);
      if (offset < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(879uL);
        throw new ArgumentOutOfRangeException("offset", "Must not be less than zero.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(880uL);
      if (count < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(881uL);
        throw new ArgumentOutOfRangeException("count", "Must not be less than zero.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(882uL);
      if (buffer.Length < offset + count) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(883uL);
        throw new ArgumentException("The offset and count are too large");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(884uL);
      if (count == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(885uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(886uL);
      TraceOutput("Write off({0}) count({1})", offset, count);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(887uL);
      if (count + _pendingCount <= BLOCK_SIZE_IN_BYTES) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(888uL);
        Buffer.BlockCopy(buffer, offset, _PendingWriteBlock, _pendingCount, count);
        _pendingCount += count;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(889uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(890uL);
      int bytesRemaining = count;
      int curOffset = offset;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(891uL);
      if (_pendingCount != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(892uL);
        int fillCount = BLOCK_SIZE_IN_BYTES - _pendingCount;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(893uL);
        if (fillCount > 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(894uL);
          Buffer.BlockCopy(buffer, offset, _PendingWriteBlock, _pendingCount, fillCount);
          bytesRemaining -= fillCount;
          curOffset += fillCount;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(895uL);
        WriteTransformOneBlock(_PendingWriteBlock, 0);
        _s.Write(_PendingWriteBlock, 0, BLOCK_SIZE_IN_BYTES);
        _totalBytesXferred += BLOCK_SIZE_IN_BYTES;
        _pendingCount = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(896uL);
      int blocksToXform = (bytesRemaining - 1) / BLOCK_SIZE_IN_BYTES;
      _pendingCount = bytesRemaining - (blocksToXform * BLOCK_SIZE_IN_BYTES);
      Buffer.BlockCopy(buffer, curOffset + bytesRemaining - _pendingCount, _PendingWriteBlock, 0, _pendingCount);
      bytesRemaining -= _pendingCount;
      _totalBytesXferred += bytesRemaining;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(897uL);
      if (blocksToXform > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(898uL);
        do {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(899uL);
          int c = _iobuf.Length;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(900uL);
          if (c > bytesRemaining) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(901uL);
            c = bytesRemaining;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(902uL);
          Buffer.BlockCopy(buffer, curOffset, _iobuf, 0, c);
          WriteTransformBlocks(_iobuf, 0, c);
          _s.Write(_iobuf, 0, c);
          bytesRemaining -= c;
          curOffset += c;
        } while (bytesRemaining > 0);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(903uL);
    }
    public override void Close()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(904uL);
      TraceOutput("Close {0:X8}", this.GetHashCode());
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(905uL);
      if (_pendingCount > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(906uL);
        WriteTransformFinalBlock();
        _s.Write(_PendingWriteBlock, 0, _pendingCount);
        _totalBytesXferred += _pendingCount;
        _pendingCount = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(907uL);
      _s.Close();
      TraceOutput("-------------------------------------------------------");
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(908uL);
    }
    public override bool CanRead {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(909uL);
        if (_mode != CryptoMode.Decrypt) {
          System.Boolean RNTRNTRNT_146 = false;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(910uL);
          return RNTRNTRNT_146;
        }
        System.Boolean RNTRNTRNT_147 = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(911uL);
        return RNTRNTRNT_147;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_148 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(912uL);
        return RNTRNTRNT_148;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_149 = (_mode == CryptoMode.Encrypt);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(913uL);
        return RNTRNTRNT_149;
      }
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(914uL);
      _s.Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(915uL);
    }
    public override long Length {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(916uL);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(917uL);
        throw new NotImplementedException();
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(918uL);
        throw new NotImplementedException();
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(919uL);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(920uL);
      throw new NotImplementedException();
    }
    [System.Diagnostics.ConditionalAttribute("Trace")]
    private void TraceOutput(string format, params object[] varParams)
    {
      lock (_outputLock) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(921uL);
        int tid = System.Threading.Thread.CurrentThread.GetHashCode();
        Console.ForegroundColor = (ConsoleColor)(tid % 8 + 8);
        Console.Write("{0:000} WZACS ", tid);
        Console.WriteLine(format, varParams);
        Console.ResetColor();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(922uL);
    }
    private object _outputLock = new Object();
  }
}
