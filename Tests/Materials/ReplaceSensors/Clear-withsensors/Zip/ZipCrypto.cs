using System;
namespace Ionic.Zip
{
  internal class ZipCrypto
  {
    private ZipCrypto()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1119uL);
    }
    public static ZipCrypto ForWrite(string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1120uL);
      ZipCrypto z = new ZipCrypto();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1121uL);
      if (password == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1122uL);
        throw new BadPasswordException("This entry requires a password.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1123uL);
      z.InitCipher(password);
      ZipCrypto RNTRNTRNT_183 = z;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1124uL);
      return RNTRNTRNT_183;
    }
    public static ZipCrypto ForRead(string password, ZipEntry e)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1125uL);
      System.IO.Stream s = e._archiveStream;
      e._WeakEncryptionHeader = new byte[12];
      byte[] eh = e._WeakEncryptionHeader;
      ZipCrypto z = new ZipCrypto();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1126uL);
      if (password == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1127uL);
        throw new BadPasswordException("This entry requires a password.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1128uL);
      z.InitCipher(password);
      ZipEntry.ReadWeakEncryptionHeader(s, eh);
      byte[] DecryptedHeader = z.DecryptMessage(eh, eh.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1129uL);
      if (DecryptedHeader[11] != (byte)((e._Crc32 >> 24) & 0xff)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1130uL);
        if ((e._BitField & 0x8) != 0x8) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1131uL);
          throw new BadPasswordException("The password did not match.");
        } else if (DecryptedHeader[11] != (byte)((e._TimeBlob >> 8) & 0xff)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1132uL);
          throw new BadPasswordException("The password did not match.");
        }
      } else {
      }
      ZipCrypto RNTRNTRNT_184 = z;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1133uL);
      return RNTRNTRNT_184;
    }
    private byte MagicByte {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1134uL);
        UInt16 t = (UInt16)((UInt16)(_Keys[2] & 0xffff) | 2);
        System.Byte RNTRNTRNT_185 = (byte)((t * (t ^ 1)) >> 8);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1135uL);
        return RNTRNTRNT_185;
      }
    }
    public byte[] DecryptMessage(byte[] cipherText, int length)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1136uL);
      if (cipherText == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1137uL);
        throw new ArgumentNullException("cipherText");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1138uL);
      if (length > cipherText.Length) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1139uL);
        throw new ArgumentOutOfRangeException("length", "Bad length during Decryption: the length parameter must be smaller than or equal to the size of the destination array.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1140uL);
      byte[] plainText = new byte[length];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1141uL);
      for (int i = 0; i < length; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1142uL);
        byte C = (byte)(cipherText[i] ^ MagicByte);
        UpdateKeys(C);
        plainText[i] = C;
      }
      System.Byte[] RNTRNTRNT_186 = plainText;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1143uL);
      return RNTRNTRNT_186;
    }
    public byte[] EncryptMessage(byte[] plainText, int length)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1144uL);
      if (plainText == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1145uL);
        throw new ArgumentNullException("plaintext");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1146uL);
      if (length > plainText.Length) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1147uL);
        throw new ArgumentOutOfRangeException("length", "Bad length during Encryption: The length parameter must be smaller than or equal to the size of the destination array.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1148uL);
      byte[] cipherText = new byte[length];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1149uL);
      for (int i = 0; i < length; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1150uL);
        byte C = plainText[i];
        cipherText[i] = (byte)(plainText[i] ^ MagicByte);
        UpdateKeys(C);
      }
      System.Byte[] RNTRNTRNT_187 = cipherText;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1151uL);
      return RNTRNTRNT_187;
    }
    public void InitCipher(string passphrase)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1152uL);
      byte[] p = SharedUtilities.StringToByteArray(passphrase);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1153uL);
      for (int i = 0; i < passphrase.Length; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1154uL);
        UpdateKeys(p[i]);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1155uL);
    }
    private void UpdateKeys(byte byteValue)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1156uL);
      _Keys[0] = (UInt32)crc32.ComputeCrc32((int)_Keys[0], byteValue);
      _Keys[1] = _Keys[1] + (byte)_Keys[0];
      _Keys[1] = _Keys[1] * 0x8088405 + 1;
      _Keys[2] = (UInt32)crc32.ComputeCrc32((int)_Keys[2], (byte)(_Keys[1] >> 24));
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1157uL);
    }
    private UInt32[] _Keys = {
      0x12345678,
      0x23456789,
      0x34567890
    };
    private Ionic.Crc.CRC32 crc32 = new Ionic.Crc.CRC32();
  }
  internal enum CryptoMode
  {
    Encrypt,
    Decrypt
  }
  internal class ZipCipherStream : System.IO.Stream
  {
    private ZipCrypto _cipher;
    private System.IO.Stream _s;
    private CryptoMode _mode;
    public ZipCipherStream(System.IO.Stream s, ZipCrypto cipher, CryptoMode mode) : base()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1158uL);
      _cipher = cipher;
      _s = s;
      _mode = mode;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1159uL);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1160uL);
      if (_mode == CryptoMode.Encrypt) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1161uL);
        throw new NotSupportedException("This stream does not encrypt via Read()");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1162uL);
      if (buffer == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1163uL);
        throw new ArgumentNullException("buffer");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1164uL);
      byte[] db = new byte[count];
      int n = _s.Read(db, 0, count);
      byte[] decrypted = _cipher.DecryptMessage(db, n);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1165uL);
      for (int i = 0; i < n; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1166uL);
        buffer[offset + i] = decrypted[i];
      }
      System.Int32 RNTRNTRNT_188 = n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1167uL);
      return RNTRNTRNT_188;
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1168uL);
      if (_mode == CryptoMode.Decrypt) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1169uL);
        throw new NotSupportedException("This stream does not Decrypt via Write()");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1170uL);
      if (buffer == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1171uL);
        throw new ArgumentNullException("buffer");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1172uL);
      if (count == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1173uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1174uL);
      byte[] plaintext = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1175uL);
      if (offset != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1176uL);
        plaintext = new byte[count];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1177uL);
        for (int i = 0; i < count; i++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1178uL);
          plaintext[i] = buffer[offset + i];
        }
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1179uL);
        plaintext = buffer;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1180uL);
      byte[] encrypted = _cipher.EncryptMessage(plaintext, count);
      _s.Write(encrypted, 0, encrypted.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1181uL);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_189 = (_mode == CryptoMode.Decrypt);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1182uL);
        return RNTRNTRNT_189;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_190 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1183uL);
        return RNTRNTRNT_190;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_191 = (_mode == CryptoMode.Encrypt);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1184uL);
        return RNTRNTRNT_191;
      }
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1185uL);
    }
    public override long Length {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1186uL);
        throw new NotSupportedException();
      }
    }
    public override long Position {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1187uL);
        throw new NotSupportedException();
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1188uL);
        throw new NotSupportedException();
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1189uL);
      throw new NotSupportedException();
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1190uL);
      throw new NotSupportedException();
    }
  }
}
