using System;
using System.IO;
using System.Security.Permissions;
namespace Ionic.Zip
{
  static internal class SharedUtilities
  {
    public static Int64 GetFileLength(string fileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(945uL);
      if (!File.Exists(fileName)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(946uL);
        throw new System.IO.FileNotFoundException(fileName);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(947uL);
      long fileLength = 0L;
      FileShare fs = FileShare.ReadWrite;
      fs |= FileShare.Delete;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(948uL);
      using (var s = File.Open(fileName, FileMode.Open, FileAccess.Read, fs)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(949uL);
        fileLength = s.Length;
      }
      Int64 RNTRNTRNT_150 = fileLength;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(950uL);
      return RNTRNTRNT_150;
    }
    [System.Diagnostics.Conditional("NETCF")]
    public static void Workaround_Ladybug318918(Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(951uL);
      s.Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(952uL);
    }
    private static System.Text.RegularExpressions.Regex doubleDotRegex1 = new System.Text.RegularExpressions.Regex("^(.*/)?([^/\\\\.]+/\\\\.\\\\./)(.+)$");
    private static string SimplifyFwdSlashPath(string path)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(953uL);
      if (path.StartsWith("./")) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(954uL);
        path = path.Substring(2);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(955uL);
      path = path.Replace("/./", "/");
      path = doubleDotRegex1.Replace(path, "$1$3");
      System.String RNTRNTRNT_151 = path;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(956uL);
      return RNTRNTRNT_151;
    }
    public static string NormalizePathForUseInZipFile(string pathName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(957uL);
      if (String.IsNullOrEmpty(pathName)) {
        System.String RNTRNTRNT_152 = pathName;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(958uL);
        return RNTRNTRNT_152;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(959uL);
      if ((pathName.Length >= 2) && ((pathName[1] == ':') && (pathName[2] == '\\'))) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(960uL);
        pathName = pathName.Substring(3);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(961uL);
      pathName = pathName.Replace('\\', '/');
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(962uL);
      while (pathName.StartsWith("/")) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(963uL);
        pathName = pathName.Substring(1);
      }
      System.String RNTRNTRNT_153 = SimplifyFwdSlashPath(pathName);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(964uL);
      return RNTRNTRNT_153;
    }
    static System.Text.Encoding ibm437 = System.Text.Encoding.GetEncoding("IBM437");
    static System.Text.Encoding utf8 = System.Text.Encoding.GetEncoding("UTF-8");
    static internal byte[] StringToByteArray(string value, System.Text.Encoding encoding)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(965uL);
      byte[] a = encoding.GetBytes(value);
      System.Byte[] RNTRNTRNT_154 = a;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(966uL);
      return RNTRNTRNT_154;
    }
    static internal byte[] StringToByteArray(string value)
    {
      System.Byte[] RNTRNTRNT_155 = StringToByteArray(value, ibm437);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(967uL);
      return RNTRNTRNT_155;
    }
    static internal string Utf8StringFromBuffer(byte[] buf)
    {
      System.String RNTRNTRNT_156 = StringFromBuffer(buf, utf8);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(968uL);
      return RNTRNTRNT_156;
    }
    static internal string StringFromBuffer(byte[] buf, System.Text.Encoding encoding)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(969uL);
      string s = encoding.GetString(buf, 0, buf.Length);
      System.String RNTRNTRNT_157 = s;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(970uL);
      return RNTRNTRNT_157;
    }
    static internal int ReadSignature(System.IO.Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(971uL);
      int x = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(972uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(973uL);
        x = _ReadFourBytes(s, "n/a");
      } catch (BadReadException) {
      }
      System.Int32 RNTRNTRNT_158 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(974uL);
      return RNTRNTRNT_158;
    }
    static internal int ReadEntrySignature(System.IO.Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(975uL);
      int x = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(976uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(977uL);
        x = _ReadFourBytes(s, "n/a");
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(978uL);
        if (x == ZipConstants.ZipEntryDataDescriptorSignature) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(979uL);
          s.Seek(12, SeekOrigin.Current);
          Workaround_Ladybug318918(s);
          x = _ReadFourBytes(s, "n/a");
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(980uL);
          if (x != ZipConstants.ZipEntrySignature) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(981uL);
            s.Seek(8, SeekOrigin.Current);
            Workaround_Ladybug318918(s);
            x = _ReadFourBytes(s, "n/a");
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(982uL);
            if (x != ZipConstants.ZipEntrySignature) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(983uL);
              s.Seek(-24, SeekOrigin.Current);
              Workaround_Ladybug318918(s);
              x = _ReadFourBytes(s, "n/a");
            }
          }
        }
      } catch (BadReadException) {
      }
      System.Int32 RNTRNTRNT_159 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(984uL);
      return RNTRNTRNT_159;
    }
    static internal int ReadInt(System.IO.Stream s)
    {
      System.Int32 RNTRNTRNT_160 = _ReadFourBytes(s, "Could not read block - no data!  (position 0x{0:X8})");
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(985uL);
      return RNTRNTRNT_160;
    }
    private static int _ReadFourBytes(System.IO.Stream s, string message)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(986uL);
      int n = 0;
      byte[] block = new byte[4];
      n = s.Read(block, 0, block.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(987uL);
      if (n != block.Length) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(988uL);
        throw new BadReadException(String.Format(message, s.Position));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(989uL);
      int data = unchecked((((block[3] * 256 + block[2]) * 256) + block[1]) * 256 + block[0]);
      System.Int32 RNTRNTRNT_161 = data;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(990uL);
      return RNTRNTRNT_161;
    }
    static internal long FindSignature(System.IO.Stream stream, int SignatureToFind)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(991uL);
      long startingPosition = stream.Position;
      int BATCH_SIZE = 65536;
      byte[] targetBytes = new byte[4];
      targetBytes[0] = (byte)(SignatureToFind >> 24);
      targetBytes[1] = (byte)((SignatureToFind & 0xff0000) >> 16);
      targetBytes[2] = (byte)((SignatureToFind & 0xff00) >> 8);
      targetBytes[3] = (byte)(SignatureToFind & 0xff);
      byte[] batch = new byte[BATCH_SIZE];
      int n = 0;
      bool success = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(992uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(993uL);
        n = stream.Read(batch, 0, batch.Length);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(994uL);
        if (n != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(995uL);
          for (int i = 0; i < n; i++) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(996uL);
            if (batch[i] == targetBytes[3]) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(997uL);
              long curPosition = stream.Position;
              stream.Seek(i - n, System.IO.SeekOrigin.Current);
              Workaround_Ladybug318918(stream);
              int sig = ReadSignature(stream);
              success = (sig == SignatureToFind);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(998uL);
              if (!success) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(999uL);
                stream.Seek(curPosition, System.IO.SeekOrigin.Begin);
                Workaround_Ladybug318918(stream);
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1000uL);
                break;
              }
            }
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1001uL);
          break;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1002uL);
        if (success) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1003uL);
          break;
        }
      } while (true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1004uL);
      if (!success) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1005uL);
        stream.Seek(startingPosition, System.IO.SeekOrigin.Begin);
        Workaround_Ladybug318918(stream);
        System.Int64 RNTRNTRNT_162 = -1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1006uL);
        return RNTRNTRNT_162;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1007uL);
      long bytesRead = (stream.Position - startingPosition) - 4;
      System.Int64 RNTRNTRNT_163 = bytesRead;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1008uL);
      return RNTRNTRNT_163;
    }
    static internal DateTime AdjustTime_Reverse(DateTime time)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1009uL);
      if (time.Kind == DateTimeKind.Utc) {
        DateTime RNTRNTRNT_164 = time;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1010uL);
        return RNTRNTRNT_164;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1011uL);
      DateTime adjusted = time;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1012uL);
      if (DateTime.Now.IsDaylightSavingTime() && !time.IsDaylightSavingTime()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1013uL);
        adjusted = time - new System.TimeSpan(1, 0, 0);
      } else if (!DateTime.Now.IsDaylightSavingTime() && time.IsDaylightSavingTime()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1014uL);
        adjusted = time + new System.TimeSpan(1, 0, 0);
      }
      DateTime RNTRNTRNT_165 = adjusted;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1015uL);
      return RNTRNTRNT_165;
    }
    static internal DateTime PackedToDateTime(Int32 packedDateTime)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1016uL);
      if (packedDateTime == 0xffff || packedDateTime == 0) {
        DateTime RNTRNTRNT_166 = new System.DateTime(1995, 1, 1, 0, 0, 0, 0);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1017uL);
        return RNTRNTRNT_166;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1018uL);
      Int16 packedTime = unchecked((Int16)(packedDateTime & 0xffff));
      Int16 packedDate = unchecked((Int16)((packedDateTime & 0xffff0000u) >> 16));
      int year = 1980 + ((packedDate & 0xfe00) >> 9);
      int month = (packedDate & 0x1e0) >> 5;
      int day = packedDate & 0x1f;
      int hour = (packedTime & 0xf800) >> 11;
      int minute = (packedTime & 0x7e0) >> 5;
      int second = (packedTime & 0x1f) * 2;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1019uL);
      if (second >= 60) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1020uL);
        minute++;
        second = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1021uL);
      if (minute >= 60) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1022uL);
        hour++;
        minute = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1023uL);
      if (hour >= 24) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1024uL);
        day++;
        hour = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1025uL);
      DateTime d = System.DateTime.Now;
      bool success = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1026uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1027uL);
        d = new System.DateTime(year, month, day, hour, minute, second, 0);
        success = true;
      } catch (System.ArgumentOutOfRangeException) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1028uL);
        if (year == 1980 && (month == 0 || day == 0)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1029uL);
          try {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1030uL);
            d = new System.DateTime(1980, 1, 1, hour, minute, second, 0);
            success = true;
          } catch (System.ArgumentOutOfRangeException) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1031uL);
            try {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1032uL);
              d = new System.DateTime(1980, 1, 1, 0, 0, 0, 0);
              success = true;
            } catch (System.ArgumentOutOfRangeException) {
            }
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1033uL);
          try {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1034uL);
            while (year < 1980) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1035uL);
              year++;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1036uL);
            while (year > 2030) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1037uL);
              year--;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1038uL);
            while (month < 1) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1039uL);
              month++;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1040uL);
            while (month > 12) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1041uL);
              month--;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1042uL);
            while (day < 1) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1043uL);
              day++;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1044uL);
            while (day > 28) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1045uL);
              day--;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1046uL);
            while (minute < 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1047uL);
              minute++;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1048uL);
            while (minute > 59) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1049uL);
              minute--;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1050uL);
            while (second < 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1051uL);
              second++;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1052uL);
            while (second > 59) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1053uL);
              second--;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1054uL);
            d = new System.DateTime(year, month, day, hour, minute, second, 0);
            success = true;
          } catch (System.ArgumentOutOfRangeException) {
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1055uL);
      if (!success) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1056uL);
        string msg = String.Format("y({0}) m({1}) d({2}) h({3}) m({4}) s({5})", year, month, day, hour, minute, second);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1057uL);
        throw new ZipException(String.Format("Bad date/time format in the zip file. ({0})", msg));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1058uL);
      d = DateTime.SpecifyKind(d, DateTimeKind.Local);
      DateTime RNTRNTRNT_167 = d;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1059uL);
      return RNTRNTRNT_167;
    }
    static internal Int32 DateTimeToPacked(DateTime time)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1060uL);
      time = time.ToLocalTime();
      UInt16 packedDate = (UInt16)((time.Day & 0x1f) | ((time.Month << 5) & 0x1e0) | (((time.Year - 1980) << 9) & 0xfe00));
      UInt16 packedTime = (UInt16)((time.Second / 2 & 0x1f) | ((time.Minute << 5) & 0x7e0) | ((time.Hour << 11) & 0xf800));
      Int32 result = (Int32)(((UInt32)(packedDate << 16)) | packedTime);
      Int32 RNTRNTRNT_168 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1061uL);
      return RNTRNTRNT_168;
    }
    public static void CreateAndOpenUniqueTempFile(string dir, out Stream fs, out string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1062uL);
      for (int i = 0; i < 3; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1063uL);
        try {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1064uL);
          filename = Path.Combine(dir, InternalGetTempFileName());
          fs = new FileStream(filename, FileMode.CreateNew);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1065uL);
          return;
        } catch (IOException) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1066uL);
          if (i == 2) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1067uL);
            throw;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1068uL);
      throw new IOException();
    }
    public static string InternalGetTempFileName()
    {
      System.String RNTRNTRNT_169 = "DotNetZip-" + Path.GetRandomFileName().Substring(0, 8) + ".tmp";
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1069uL);
      return RNTRNTRNT_169;
    }
    static internal int ReadWithRetry(System.IO.Stream s, byte[] buffer, int offset, int count, string FileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1070uL);
      int n = 0;
      bool done = false;
      int retries = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1071uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1072uL);
        try {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1073uL);
          n = s.Read(buffer, offset, count);
          done = true;
        } catch (System.IO.IOException ioexc1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1074uL);
          var p = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1075uL);
          if (p.IsUnrestricted()) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1076uL);
            uint hresult = _HRForException(ioexc1);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1077uL);
            if (hresult != 0x80070021u) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1078uL);
              throw new System.IO.IOException(String.Format("Cannot read file {0}", FileName), ioexc1);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1079uL);
            retries++;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1080uL);
            if (retries > 10) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1081uL);
              throw new System.IO.IOException(String.Format("Cannot read file {0}, at offset 0x{1:X8} after 10 retries", FileName, offset), ioexc1);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1082uL);
            System.Threading.Thread.Sleep(250 + retries * 550);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1083uL);
            throw;
          }
        }
      } while (!done);
      System.Int32 RNTRNTRNT_170 = n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1084uL);
      return RNTRNTRNT_170;
    }
    private static uint _HRForException(System.Exception ex1)
    {
      System.UInt32 RNTRNTRNT_171 = unchecked((uint)System.Runtime.InteropServices.Marshal.GetHRForException(ex1));
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1085uL);
      return RNTRNTRNT_171;
    }
  }
  public class CountingStream : System.IO.Stream
  {
    private System.IO.Stream _s;
    private Int64 _bytesWritten;
    private Int64 _bytesRead;
    private Int64 _initialOffset;
    public CountingStream(System.IO.Stream stream) : base()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1086uL);
      _s = stream;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1087uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1088uL);
        _initialOffset = _s.Position;
      } catch {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1089uL);
        _initialOffset = 0L;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1090uL);
    }
    public Stream WrappedStream {
      get {
        Stream RNTRNTRNT_172 = _s;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1091uL);
        return RNTRNTRNT_172;
      }
    }
    public Int64 BytesWritten {
      get {
        Int64 RNTRNTRNT_173 = _bytesWritten;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1092uL);
        return RNTRNTRNT_173;
      }
    }
    public Int64 BytesRead {
      get {
        Int64 RNTRNTRNT_174 = _bytesRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1093uL);
        return RNTRNTRNT_174;
      }
    }
    public void Adjust(Int64 delta)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1094uL);
      _bytesWritten -= delta;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1095uL);
      if (_bytesWritten < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1096uL);
        throw new InvalidOperationException();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1097uL);
      if (_s as CountingStream != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1098uL);
        ((CountingStream)_s).Adjust(delta);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1099uL);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1100uL);
      int n = _s.Read(buffer, offset, count);
      _bytesRead += n;
      System.Int32 RNTRNTRNT_175 = n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1101uL);
      return RNTRNTRNT_175;
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1102uL);
      if (count == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1103uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1104uL);
      _s.Write(buffer, offset, count);
      _bytesWritten += count;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1105uL);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_176 = _s.CanRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1106uL);
        return RNTRNTRNT_176;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_177 = _s.CanSeek;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1107uL);
        return RNTRNTRNT_177;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_178 = _s.CanWrite;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1108uL);
        return RNTRNTRNT_178;
      }
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1109uL);
      _s.Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1110uL);
    }
    public override long Length {
      get {
        System.Int64 RNTRNTRNT_179 = _s.Length;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1111uL);
        return RNTRNTRNT_179;
      }
    }
    public long ComputedPosition {
      get {
        System.Int64 RNTRNTRNT_180 = _initialOffset + _bytesWritten;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1112uL);
        return RNTRNTRNT_180;
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_181 = _s.Position;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1113uL);
        return RNTRNTRNT_181;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1114uL);
        _s.Seek(value, System.IO.SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_s);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1115uL);
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      System.Int64 RNTRNTRNT_182 = _s.Seek(offset, origin);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1116uL);
      return RNTRNTRNT_182;
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1117uL);
      _s.SetLength(value);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1118uL);
    }
  }
}
