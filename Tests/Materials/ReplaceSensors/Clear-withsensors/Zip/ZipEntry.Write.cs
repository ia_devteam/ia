using System;
using System.IO;
using RE = System.Text.RegularExpressions;
namespace Ionic.Zip
{
  public partial class ZipEntry
  {
    internal void WriteCentralDirectoryEntry(Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2003uL);
      byte[] bytes = new byte[4096];
      int i = 0;
      bytes[i++] = (byte)(ZipConstants.ZipDirEntrySignature & 0xff);
      bytes[i++] = (byte)((ZipConstants.ZipDirEntrySignature & 0xff00) >> 8);
      bytes[i++] = (byte)((ZipConstants.ZipDirEntrySignature & 0xff0000) >> 16);
      bytes[i++] = (byte)((ZipConstants.ZipDirEntrySignature & 0xff000000u) >> 24);
      bytes[i++] = (byte)(_VersionMadeBy & 0xff);
      bytes[i++] = (byte)((_VersionMadeBy & 0xff00) >> 8);
      Int16 vNeeded = (Int16)(VersionNeeded != 0 ? VersionNeeded : 20);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2004uL);
      if (_OutputUsesZip64 == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2005uL);
        _OutputUsesZip64 = new Nullable<bool>(_container.Zip64 == Zip64Option.Always);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2006uL);
      Int16 versionNeededToExtract = (Int16)(_OutputUsesZip64.Value ? 45 : vNeeded);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2007uL);
      if (this.CompressionMethod == Ionic.Zip.CompressionMethod.BZip2) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2008uL);
        versionNeededToExtract = 46;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2009uL);
      bytes[i++] = (byte)(versionNeededToExtract & 0xff);
      bytes[i++] = (byte)((versionNeededToExtract & 0xff00) >> 8);
      bytes[i++] = (byte)(_BitField & 0xff);
      bytes[i++] = (byte)((_BitField & 0xff00) >> 8);
      bytes[i++] = (byte)(_CompressionMethod & 0xff);
      bytes[i++] = (byte)((_CompressionMethod & 0xff00) >> 8);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2010uL);
      if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2011uL);
        i -= 2;
        bytes[i++] = 0x63;
        bytes[i++] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2012uL);
      bytes[i++] = (byte)(_TimeBlob & 0xff);
      bytes[i++] = (byte)((_TimeBlob & 0xff00) >> 8);
      bytes[i++] = (byte)((_TimeBlob & 0xff0000) >> 16);
      bytes[i++] = (byte)((_TimeBlob & 0xff000000u) >> 24);
      bytes[i++] = (byte)(_Crc32 & 0xff);
      bytes[i++] = (byte)((_Crc32 & 0xff00) >> 8);
      bytes[i++] = (byte)((_Crc32 & 0xff0000) >> 16);
      bytes[i++] = (byte)((_Crc32 & 0xff000000u) >> 24);
      int j = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2013uL);
      if (_OutputUsesZip64.Value) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2014uL);
        for (j = 0; j < 8; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2015uL);
          bytes[i++] = 0xff;
        }
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2016uL);
        bytes[i++] = (byte)(_CompressedSize & 0xff);
        bytes[i++] = (byte)((_CompressedSize & 0xff00) >> 8);
        bytes[i++] = (byte)((_CompressedSize & 0xff0000) >> 16);
        bytes[i++] = (byte)((_CompressedSize & 0xff000000u) >> 24);
        bytes[i++] = (byte)(_UncompressedSize & 0xff);
        bytes[i++] = (byte)((_UncompressedSize & 0xff00) >> 8);
        bytes[i++] = (byte)((_UncompressedSize & 0xff0000) >> 16);
        bytes[i++] = (byte)((_UncompressedSize & 0xff000000u) >> 24);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2017uL);
      byte[] fileNameBytes = GetEncodedFileNameBytes();
      Int16 filenameLength = (Int16)fileNameBytes.Length;
      bytes[i++] = (byte)(filenameLength & 0xff);
      bytes[i++] = (byte)((filenameLength & 0xff00) >> 8);
      _presumeZip64 = _OutputUsesZip64.Value;
      _Extra = ConstructExtraField(true);
      Int16 extraFieldLength = (Int16)((_Extra == null) ? 0 : _Extra.Length);
      bytes[i++] = (byte)(extraFieldLength & 0xff);
      bytes[i++] = (byte)((extraFieldLength & 0xff00) >> 8);
      int commentLength = (_CommentBytes == null) ? 0 : _CommentBytes.Length;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2018uL);
      if (commentLength + i > bytes.Length) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2019uL);
        commentLength = bytes.Length - i;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2020uL);
      bytes[i++] = (byte)(commentLength & 0xff);
      bytes[i++] = (byte)((commentLength & 0xff00) >> 8);
      bool segmented = (this._container.ZipFile != null) && (this._container.ZipFile.MaxOutputSegmentSize != 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2021uL);
      if (segmented) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2022uL);
        bytes[i++] = (byte)(_diskNumber & 0xff);
        bytes[i++] = (byte)((_diskNumber & 0xff00) >> 8);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2023uL);
        bytes[i++] = 0;
        bytes[i++] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2024uL);
      bytes[i++] = (byte)((_IsText) ? 1 : 0);
      bytes[i++] = 0;
      bytes[i++] = (byte)(_ExternalFileAttrs & 0xff);
      bytes[i++] = (byte)((_ExternalFileAttrs & 0xff00) >> 8);
      bytes[i++] = (byte)((_ExternalFileAttrs & 0xff0000) >> 16);
      bytes[i++] = (byte)((_ExternalFileAttrs & 0xff000000u) >> 24);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2025uL);
      if (_RelativeOffsetOfLocalHeader > 0xffffffffL) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2026uL);
        bytes[i++] = 0xff;
        bytes[i++] = 0xff;
        bytes[i++] = 0xff;
        bytes[i++] = 0xff;
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2027uL);
        bytes[i++] = (byte)(_RelativeOffsetOfLocalHeader & 0xff);
        bytes[i++] = (byte)((_RelativeOffsetOfLocalHeader & 0xff00) >> 8);
        bytes[i++] = (byte)((_RelativeOffsetOfLocalHeader & 0xff0000) >> 16);
        bytes[i++] = (byte)((_RelativeOffsetOfLocalHeader & 0xff000000u) >> 24);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2028uL);
      Buffer.BlockCopy(fileNameBytes, 0, bytes, i, filenameLength);
      i += filenameLength;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2029uL);
      if (_Extra != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2030uL);
        byte[] h = _Extra;
        int offx = 0;
        Buffer.BlockCopy(h, offx, bytes, i, extraFieldLength);
        i += extraFieldLength;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2031uL);
      if (commentLength != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2032uL);
        Buffer.BlockCopy(_CommentBytes, 0, bytes, i, commentLength);
        i += commentLength;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2033uL);
      s.Write(bytes, 0, i);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2034uL);
    }
    private byte[] ConstructExtraField(bool forCentralDirectory)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2035uL);
      var listOfBlocks = new System.Collections.Generic.List<byte[]>();
      byte[] block;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2036uL);
      if (_container.Zip64 == Zip64Option.Always || (_container.Zip64 == Zip64Option.AsNecessary && (!forCentralDirectory || _entryRequiresZip64.Value))) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2037uL);
        int sz = 4 + (forCentralDirectory ? 28 : 16);
        block = new byte[sz];
        int i = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2038uL);
        if (_presumeZip64 || forCentralDirectory) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2039uL);
          block[i++] = 0x1;
          block[i++] = 0x0;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2040uL);
          block[i++] = 0x99;
          block[i++] = 0x99;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2041uL);
        block[i++] = (byte)(sz - 4);
        block[i++] = 0x0;
        Array.Copy(BitConverter.GetBytes(_UncompressedSize), 0, block, i, 8);
        i += 8;
        Array.Copy(BitConverter.GetBytes(_CompressedSize), 0, block, i, 8);
        i += 8;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2042uL);
        if (forCentralDirectory) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2043uL);
          Array.Copy(BitConverter.GetBytes(_RelativeOffsetOfLocalHeader), 0, block, i, 8);
          i += 8;
          Array.Copy(BitConverter.GetBytes(0), 0, block, i, 4);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2044uL);
        listOfBlocks.Add(block);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2045uL);
      if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2046uL);
        block = new byte[4 + 7];
        int i = 0;
        block[i++] = 0x1;
        block[i++] = 0x99;
        block[i++] = 0x7;
        block[i++] = 0x0;
        block[i++] = 0x1;
        block[i++] = 0x0;
        block[i++] = 0x41;
        block[i++] = 0x45;
        int keystrength = GetKeyStrengthInBits(Encryption);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2047uL);
        if (keystrength == 128) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2048uL);
          block[i] = 1;
        } else if (keystrength == 256) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2050uL);
          block[i] = 3;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2049uL);
          block[i] = 0xff;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2051uL);
        i++;
        block[i++] = (byte)(_CompressionMethod & 0xff);
        block[i++] = (byte)(_CompressionMethod & 0xff00);
        listOfBlocks.Add(block);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2052uL);
      if (_ntfsTimesAreSet && _emitNtfsTimes) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2053uL);
        block = new byte[32 + 4];
        int i = 0;
        block[i++] = 0xa;
        block[i++] = 0x0;
        block[i++] = 32;
        block[i++] = 0;
        i += 4;
        block[i++] = 0x1;
        block[i++] = 0x0;
        block[i++] = 24;
        block[i++] = 0;
        Int64 z = _Mtime.ToFileTime();
        Array.Copy(BitConverter.GetBytes(z), 0, block, i, 8);
        i += 8;
        z = _Atime.ToFileTime();
        Array.Copy(BitConverter.GetBytes(z), 0, block, i, 8);
        i += 8;
        z = _Ctime.ToFileTime();
        Array.Copy(BitConverter.GetBytes(z), 0, block, i, 8);
        i += 8;
        listOfBlocks.Add(block);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2054uL);
      if (_ntfsTimesAreSet && _emitUnixTimes) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2055uL);
        int len = 5 + 4;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2056uL);
        if (!forCentralDirectory) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2057uL);
          len += 8;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2058uL);
        block = new byte[len];
        int i = 0;
        block[i++] = 0x55;
        block[i++] = 0x54;
        block[i++] = unchecked((byte)(len - 4));
        block[i++] = 0;
        block[i++] = 0x7;
        Int32 z = unchecked((int)((_Mtime - _unixEpoch).TotalSeconds));
        Array.Copy(BitConverter.GetBytes(z), 0, block, i, 4);
        i += 4;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2059uL);
        if (!forCentralDirectory) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2060uL);
          z = unchecked((int)((_Atime - _unixEpoch).TotalSeconds));
          Array.Copy(BitConverter.GetBytes(z), 0, block, i, 4);
          i += 4;
          z = unchecked((int)((_Ctime - _unixEpoch).TotalSeconds));
          Array.Copy(BitConverter.GetBytes(z), 0, block, i, 4);
          i += 4;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2061uL);
        listOfBlocks.Add(block);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2062uL);
      byte[] aggregateBlock = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2063uL);
      if (listOfBlocks.Count > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2064uL);
        int totalLength = 0;
        int i, current = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2065uL);
        for (i = 0; i < listOfBlocks.Count; i++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2066uL);
          totalLength += listOfBlocks[i].Length;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2067uL);
        aggregateBlock = new byte[totalLength];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2068uL);
        for (i = 0; i < listOfBlocks.Count; i++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2069uL);
          System.Array.Copy(listOfBlocks[i], 0, aggregateBlock, current, listOfBlocks[i].Length);
          current += listOfBlocks[i].Length;
        }
      }
      System.Byte[] RNTRNTRNT_314 = aggregateBlock;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2070uL);
      return RNTRNTRNT_314;
    }
    private string NormalizeFileName()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2071uL);
      string SlashFixed = FileName.Replace("\\", "/");
      string s1 = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2072uL);
      if ((_TrimVolumeFromFullyQualifiedPaths) && (FileName.Length >= 3) && (FileName[1] == ':') && (SlashFixed[2] == '/')) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2073uL);
        s1 = SlashFixed.Substring(3);
      } else if ((FileName.Length >= 4) && ((SlashFixed[0] == '/') && (SlashFixed[1] == '/'))) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2076uL);
        int n = SlashFixed.IndexOf('/', 2);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2077uL);
        if (n == -1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2078uL);
          throw new ArgumentException("The path for that entry appears to be badly formatted");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2079uL);
        s1 = SlashFixed.Substring(n + 1);
      } else if ((FileName.Length >= 3) && ((SlashFixed[0] == '.') && (SlashFixed[1] == '/'))) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2075uL);
        s1 = SlashFixed.Substring(2);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2074uL);
        s1 = SlashFixed;
      }
      System.String RNTRNTRNT_315 = s1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2080uL);
      return RNTRNTRNT_315;
    }
    private byte[] GetEncodedFileNameBytes()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2081uL);
      var s1 = NormalizeFileName();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2082uL);
      switch (AlternateEncodingUsage) {
        case ZipOption.Always:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2087uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2083uL);
            if (!(_Comment == null || _Comment.Length == 0)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2084uL);
              _CommentBytes = AlternateEncoding.GetBytes(_Comment);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2085uL);
            _actualEncoding = AlternateEncoding;
            System.Byte[] RNTRNTRNT_316 = AlternateEncoding.GetBytes(s1);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2086uL);
            return RNTRNTRNT_316;
          }

        case ZipOption.Never:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2092uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2088uL);
            if (!(_Comment == null || _Comment.Length == 0)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2089uL);
              _CommentBytes = ibm437.GetBytes(_Comment);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2090uL);
            _actualEncoding = ibm437;
            System.Byte[] RNTRNTRNT_317 = ibm437.GetBytes(s1);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2091uL);
            return RNTRNTRNT_317;
          }

      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2093uL);
      byte[] result = ibm437.GetBytes(s1);
      string s2 = ibm437.GetString(result, 0, result.Length);
      _CommentBytes = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2094uL);
      if (s2 != s1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2095uL);
        result = AlternateEncoding.GetBytes(s1);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2096uL);
        if (_Comment != null && _Comment.Length != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2097uL);
          _CommentBytes = AlternateEncoding.GetBytes(_Comment);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2098uL);
        _actualEncoding = AlternateEncoding;
        System.Byte[] RNTRNTRNT_318 = result;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2099uL);
        return RNTRNTRNT_318;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2100uL);
      _actualEncoding = ibm437;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2101uL);
      if (_Comment == null || _Comment.Length == 0) {
        System.Byte[] RNTRNTRNT_319 = result;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2102uL);
        return RNTRNTRNT_319;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2103uL);
      byte[] cbytes = ibm437.GetBytes(_Comment);
      string c2 = ibm437.GetString(cbytes, 0, cbytes.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2104uL);
      if (c2 != Comment) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2105uL);
        result = AlternateEncoding.GetBytes(s1);
        _CommentBytes = AlternateEncoding.GetBytes(_Comment);
        _actualEncoding = AlternateEncoding;
        System.Byte[] RNTRNTRNT_320 = result;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2106uL);
        return RNTRNTRNT_320;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2107uL);
      _CommentBytes = cbytes;
      System.Byte[] RNTRNTRNT_321 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2108uL);
      return RNTRNTRNT_321;
    }
    private bool WantReadAgain()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2109uL);
      if (_UncompressedSize < 0x10) {
        System.Boolean RNTRNTRNT_322 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2110uL);
        return RNTRNTRNT_322;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2111uL);
      if (_CompressionMethod == 0x0) {
        System.Boolean RNTRNTRNT_323 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2112uL);
        return RNTRNTRNT_323;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2113uL);
      if (CompressionLevel == Ionic.Zlib.CompressionLevel.None) {
        System.Boolean RNTRNTRNT_324 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2114uL);
        return RNTRNTRNT_324;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2115uL);
      if (_CompressedSize < _UncompressedSize) {
        System.Boolean RNTRNTRNT_325 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2116uL);
        return RNTRNTRNT_325;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2117uL);
      if (this._Source == ZipEntrySource.Stream && !this._sourceStream.CanSeek) {
        System.Boolean RNTRNTRNT_326 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2118uL);
        return RNTRNTRNT_326;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2119uL);
      if (_aesCrypto_forWrite != null && (CompressedSize - _aesCrypto_forWrite.SizeOfEncryptionMetadata) <= UncompressedSize + 0x10) {
        System.Boolean RNTRNTRNT_327 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2120uL);
        return RNTRNTRNT_327;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2121uL);
      if (_zipCrypto_forWrite != null && (CompressedSize - 12) <= UncompressedSize) {
        System.Boolean RNTRNTRNT_328 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2122uL);
        return RNTRNTRNT_328;
      }
      System.Boolean RNTRNTRNT_329 = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2123uL);
      return RNTRNTRNT_329;
    }
    private void MaybeUnsetCompressionMethodForWriting(int cycle)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2124uL);
      if (cycle > 1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2125uL);
        _CompressionMethod = 0x0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2126uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2127uL);
      if (IsDirectory) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2128uL);
        _CompressionMethod = 0x0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2129uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2130uL);
      if (this._Source == ZipEntrySource.ZipFile) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2131uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2132uL);
      if (this._Source == ZipEntrySource.Stream) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2133uL);
        if (_sourceStream != null && _sourceStream.CanSeek) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2134uL);
          long fileLength = _sourceStream.Length;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2135uL);
          if (fileLength == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2136uL);
            _CompressionMethod = 0x0;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2137uL);
            return;
          }
        }
      } else if ((this._Source == ZipEntrySource.FileSystem) && (SharedUtilities.GetFileLength(LocalFileName) == 0L)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2138uL);
        _CompressionMethod = 0x0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2139uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2140uL);
      if (SetCompression != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2141uL);
        CompressionLevel = SetCompression(LocalFileName, _FileNameInArchive);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2142uL);
      if (CompressionLevel == (short)Ionic.Zlib.CompressionLevel.None && CompressionMethod == Ionic.Zip.CompressionMethod.Deflate) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2143uL);
        _CompressionMethod = 0x0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2144uL);
      return;
    }
    internal void WriteHeader(Stream s, int cycle)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2145uL);
      var counter = s as CountingStream;
      _future_ROLH = (counter != null) ? counter.ComputedPosition : s.Position;
      int j = 0, i = 0;
      byte[] block = new byte[30];
      block[i++] = (byte)(ZipConstants.ZipEntrySignature & 0xff);
      block[i++] = (byte)((ZipConstants.ZipEntrySignature & 0xff00) >> 8);
      block[i++] = (byte)((ZipConstants.ZipEntrySignature & 0xff0000) >> 16);
      block[i++] = (byte)((ZipConstants.ZipEntrySignature & 0xff000000u) >> 24);
      _presumeZip64 = (_container.Zip64 == Zip64Option.Always || (_container.Zip64 == Zip64Option.AsNecessary && !s.CanSeek));
      Int16 VersionNeededToExtract = (Int16)(_presumeZip64 ? 45 : 20);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2146uL);
      if (this.CompressionMethod == Ionic.Zip.CompressionMethod.BZip2) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2147uL);
        VersionNeededToExtract = 46;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2148uL);
      block[i++] = (byte)(VersionNeededToExtract & 0xff);
      block[i++] = (byte)((VersionNeededToExtract & 0xff00) >> 8);
      byte[] fileNameBytes = GetEncodedFileNameBytes();
      Int16 filenameLength = (Int16)fileNameBytes.Length;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2149uL);
      if (_Encryption == EncryptionAlgorithm.None) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2150uL);
        _BitField &= ~1;
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2151uL);
        _BitField |= 1;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2152uL);
      if (_actualEncoding.CodePage == System.Text.Encoding.UTF8.CodePage) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2153uL);
        _BitField |= 0x800;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2154uL);
      if (IsDirectory || cycle == 99) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2155uL);
        _BitField &= ~0x8;
        _BitField &= ~0x1;
        Encryption = EncryptionAlgorithm.None;
        Password = null;
      } else if (!s.CanSeek) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2156uL);
        _BitField |= 0x8;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2157uL);
      block[i++] = (byte)(_BitField & 0xff);
      block[i++] = (byte)((_BitField & 0xff00) >> 8);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2158uL);
      if (this.__FileDataPosition == -1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2159uL);
        _CompressedSize = 0;
        _crcCalculated = false;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2160uL);
      MaybeUnsetCompressionMethodForWriting(cycle);
      block[i++] = (byte)(_CompressionMethod & 0xff);
      block[i++] = (byte)((_CompressionMethod & 0xff00) >> 8);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2161uL);
      if (cycle == 99) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2162uL);
        SetZip64Flags();
      } else if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2163uL);
        i -= 2;
        block[i++] = 0x63;
        block[i++] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2164uL);
      _TimeBlob = Ionic.Zip.SharedUtilities.DateTimeToPacked(LastModified);
      block[i++] = (byte)(_TimeBlob & 0xff);
      block[i++] = (byte)((_TimeBlob & 0xff00) >> 8);
      block[i++] = (byte)((_TimeBlob & 0xff0000) >> 16);
      block[i++] = (byte)((_TimeBlob & 0xff000000u) >> 24);
      block[i++] = (byte)(_Crc32 & 0xff);
      block[i++] = (byte)((_Crc32 & 0xff00) >> 8);
      block[i++] = (byte)((_Crc32 & 0xff0000) >> 16);
      block[i++] = (byte)((_Crc32 & 0xff000000u) >> 24);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2165uL);
      if (_presumeZip64) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2166uL);
        for (j = 0; j < 8; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2167uL);
          block[i++] = 0xff;
        }
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2168uL);
        block[i++] = (byte)(_CompressedSize & 0xff);
        block[i++] = (byte)((_CompressedSize & 0xff00) >> 8);
        block[i++] = (byte)((_CompressedSize & 0xff0000) >> 16);
        block[i++] = (byte)((_CompressedSize & 0xff000000u) >> 24);
        block[i++] = (byte)(_UncompressedSize & 0xff);
        block[i++] = (byte)((_UncompressedSize & 0xff00) >> 8);
        block[i++] = (byte)((_UncompressedSize & 0xff0000) >> 16);
        block[i++] = (byte)((_UncompressedSize & 0xff000000u) >> 24);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2169uL);
      block[i++] = (byte)(filenameLength & 0xff);
      block[i++] = (byte)((filenameLength & 0xff00) >> 8);
      _Extra = ConstructExtraField(false);
      Int16 extraFieldLength = (Int16)((_Extra == null) ? 0 : _Extra.Length);
      block[i++] = (byte)(extraFieldLength & 0xff);
      block[i++] = (byte)((extraFieldLength & 0xff00) >> 8);
      byte[] bytes = new byte[i + filenameLength + extraFieldLength];
      Buffer.BlockCopy(block, 0, bytes, 0, i);
      Buffer.BlockCopy(fileNameBytes, 0, bytes, i, fileNameBytes.Length);
      i += fileNameBytes.Length;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2170uL);
      if (_Extra != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2171uL);
        Buffer.BlockCopy(_Extra, 0, bytes, i, _Extra.Length);
        i += _Extra.Length;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2172uL);
      _LengthOfHeader = i;
      var zss = s as ZipSegmentedStream;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2173uL);
      if (zss != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2174uL);
        zss.ContiguousWrite = true;
        UInt32 requiredSegment = zss.ComputeSegment(i);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2175uL);
        if (requiredSegment != zss.CurrentSegment) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2176uL);
          _future_ROLH = 0;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2177uL);
          _future_ROLH = zss.Position;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2178uL);
        _diskNumber = requiredSegment;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2179uL);
      if (_container.Zip64 == Zip64Option.Never && (uint)_RelativeOffsetOfLocalHeader >= 0xffffffffu) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2180uL);
        throw new ZipException("Offset within the zip archive exceeds 0xFFFFFFFF. Consider setting the UseZip64WhenSaving property on the ZipFile instance.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2181uL);
      s.Write(bytes, 0, i);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2182uL);
      if (zss != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2183uL);
        zss.ContiguousWrite = false;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2184uL);
      _EntryHeader = bytes;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2185uL);
    }
    private Int32 FigureCrc32()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2186uL);
      if (_crcCalculated == false) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2187uL);
        Stream input = null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2188uL);
        if (this._Source == ZipEntrySource.WriteDelegate) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2189uL);
          var output = new Ionic.Crc.CrcCalculatorStream(Stream.Null);
          this._WriteDelegate(this.FileName, output);
          _Crc32 = output.Crc;
        } else if (this._Source == ZipEntrySource.ZipFile) {
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2190uL);
          if (this._Source == ZipEntrySource.Stream) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2191uL);
            PrepSourceStream();
            input = this._sourceStream;
          } else if (this._Source == ZipEntrySource.JitStream) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2193uL);
            if (this._sourceStream == null) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2194uL);
              _sourceStream = this._OpenDelegate(this.FileName);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2195uL);
            PrepSourceStream();
            input = this._sourceStream;
          } else if (this._Source == ZipEntrySource.ZipOutputStream) {
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2192uL);
            input = File.Open(LocalFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2196uL);
          var crc32 = new Ionic.Crc.CRC32();
          _Crc32 = crc32.GetCrc32(input);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2197uL);
          if (_sourceStream == null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2198uL);
            input.Dispose();
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2199uL);
        _crcCalculated = true;
      }
      Int32 RNTRNTRNT_330 = _Crc32;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2200uL);
      return RNTRNTRNT_330;
    }
    private void PrepSourceStream()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2201uL);
      if (_sourceStream == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2202uL);
        throw new ZipException(String.Format("The input stream is null for entry '{0}'.", FileName));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2203uL);
      if (this._sourceStreamOriginalPosition != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2204uL);
        this._sourceStream.Position = this._sourceStreamOriginalPosition.Value;
      } else if (this._sourceStream.CanSeek) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2207uL);
        this._sourceStreamOriginalPosition = new Nullable<Int64>(this._sourceStream.Position);
      } else if (this.Encryption == EncryptionAlgorithm.PkzipWeak) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2205uL);
        if (this._Source != ZipEntrySource.ZipFile && ((this._BitField & 0x8) != 0x8)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2206uL);
          throw new ZipException("It is not possible to use PKZIP encryption on a non-seekable input stream");
        }
      }
    }
    internal void CopyMetaData(ZipEntry source)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2208uL);
      this.__FileDataPosition = source.__FileDataPosition;
      this.CompressionMethod = source.CompressionMethod;
      this._CompressionMethod_FromZipFile = source._CompressionMethod_FromZipFile;
      this._CompressedFileDataSize = source._CompressedFileDataSize;
      this._UncompressedSize = source._UncompressedSize;
      this._BitField = source._BitField;
      this._Source = source._Source;
      this._LastModified = source._LastModified;
      this._Mtime = source._Mtime;
      this._Atime = source._Atime;
      this._Ctime = source._Ctime;
      this._ntfsTimesAreSet = source._ntfsTimesAreSet;
      this._emitUnixTimes = source._emitUnixTimes;
      this._emitNtfsTimes = source._emitNtfsTimes;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2209uL);
    }
    private void OnWriteBlock(Int64 bytesXferred, Int64 totalBytesToXfer)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2210uL);
      if (_container.ZipFile != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2211uL);
        _ioOperationCanceled = _container.ZipFile.OnSaveBlock(this, bytesXferred, totalBytesToXfer);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2212uL);
    }
    private void _WriteEntryData(Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2213uL);
      Stream input = null;
      long fdp = -1L;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2214uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2215uL);
        fdp = s.Position;
      } catch (Exception) {
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2216uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2217uL);
        long fileLength = SetInputAndFigureFileLength(ref input);
        CountingStream entryCounter = new CountingStream(s);
        Stream encryptor;
        Stream compressor;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2218uL);
        if (fileLength != 0L) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2219uL);
          encryptor = MaybeApplyEncryption(entryCounter);
          compressor = MaybeApplyCompression(encryptor, fileLength);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2220uL);
          encryptor = compressor = entryCounter;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2221uL);
        var output = new Ionic.Crc.CrcCalculatorStream(compressor, true);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2222uL);
        if (this._Source == ZipEntrySource.WriteDelegate) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2223uL);
          this._WriteDelegate(this.FileName, output);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2224uL);
          byte[] buffer = new byte[BufferSize];
          int n;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2225uL);
          while ((n = SharedUtilities.ReadWithRetry(input, buffer, 0, buffer.Length, FileName)) != 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2226uL);
            output.Write(buffer, 0, n);
            OnWriteBlock(output.TotalBytesSlurped, fileLength);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2227uL);
            if (_ioOperationCanceled) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2228uL);
              break;
            }
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2229uL);
        FinishOutputStream(s, entryCounter, encryptor, compressor, output);
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2230uL);
        if (this._Source == ZipEntrySource.JitStream) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2231uL);
          if (this._CloseDelegate != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2232uL);
            this._CloseDelegate(this.FileName, input);
          }
        } else if ((input as FileStream) != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2233uL);
          input.Dispose();
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2234uL);
      if (_ioOperationCanceled) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2235uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2236uL);
      this.__FileDataPosition = fdp;
      PostProcessOutput(s);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2237uL);
    }
    private long SetInputAndFigureFileLength(ref Stream input)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2238uL);
      long fileLength = -1L;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2239uL);
      if (this._Source == ZipEntrySource.Stream) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2240uL);
        PrepSourceStream();
        input = this._sourceStream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2241uL);
        try {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2242uL);
          fileLength = this._sourceStream.Length;
        } catch (NotSupportedException) {
        }
      } else if (this._Source == ZipEntrySource.ZipFile) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2249uL);
        string pwd = (_Encryption_FromZipFile == EncryptionAlgorithm.None) ? null : (this._Password ?? this._container.Password);
        this._sourceStream = InternalOpenReader(pwd);
        PrepSourceStream();
        input = this._sourceStream;
        fileLength = this._sourceStream.Length;
      } else if (this._Source == ZipEntrySource.JitStream) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2244uL);
        if (this._sourceStream == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2245uL);
          _sourceStream = this._OpenDelegate(this.FileName);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2246uL);
        PrepSourceStream();
        input = this._sourceStream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2247uL);
        try {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2248uL);
          fileLength = this._sourceStream.Length;
        } catch (NotSupportedException) {
        }
      } else if (this._Source == ZipEntrySource.FileSystem) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2243uL);
        FileShare fs = FileShare.ReadWrite;
        fs |= FileShare.Delete;
        input = File.Open(LocalFileName, FileMode.Open, FileAccess.Read, fs);
        fileLength = input.Length;
      }
      System.Int64 RNTRNTRNT_331 = fileLength;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2250uL);
      return RNTRNTRNT_331;
    }
    internal void FinishOutputStream(Stream s, CountingStream entryCounter, Stream encryptor, Stream compressor, Ionic.Crc.CrcCalculatorStream output)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2251uL);
      if (output == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2252uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2253uL);
      output.Close();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2254uL);
      if ((compressor as Ionic.Zlib.DeflateStream) != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2255uL);
        compressor.Close();
      } else if ((compressor as Ionic.BZip2.BZip2OutputStream) != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2258uL);
        compressor.Close();
      } else if ((compressor as Ionic.BZip2.ParallelBZip2OutputStream) != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2257uL);
        compressor.Close();
      } else if ((compressor as Ionic.Zlib.ParallelDeflateOutputStream) != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2256uL);
        compressor.Close();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2259uL);
      encryptor.Flush();
      encryptor.Close();
      _LengthOfTrailer = 0;
      _UncompressedSize = output.TotalBytesSlurped;
      WinZipAesCipherStream wzacs = encryptor as WinZipAesCipherStream;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2260uL);
      if (wzacs != null && _UncompressedSize > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2261uL);
        s.Write(wzacs.FinalAuthentication, 0, 10);
        _LengthOfTrailer += 10;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2262uL);
      _CompressedFileDataSize = entryCounter.BytesWritten;
      _CompressedSize = _CompressedFileDataSize;
      _Crc32 = output.Crc;
      StoreRelativeOffset();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2263uL);
    }
    internal void PostProcessOutput(Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2264uL);
      var s1 = s as CountingStream;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2265uL);
      if (_UncompressedSize == 0 && _CompressedSize == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2266uL);
        if (this._Source == ZipEntrySource.ZipOutputStream) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2267uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2268uL);
        if (_Password != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2269uL);
          int headerBytesToRetract = 0;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2270uL);
          if (Encryption == EncryptionAlgorithm.PkzipWeak) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2271uL);
            headerBytesToRetract = 12;
          } else if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2272uL);
            headerBytesToRetract = _aesCrypto_forWrite._Salt.Length + _aesCrypto_forWrite.GeneratedPV.Length;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2273uL);
          if (this._Source == ZipEntrySource.ZipOutputStream && !s.CanSeek) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2274uL);
            throw new ZipException("Zero bytes written, encryption in use, and non-seekable output.");
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2275uL);
          if (Encryption != EncryptionAlgorithm.None) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2276uL);
            s.Seek(-1 * headerBytesToRetract, SeekOrigin.Current);
            s.SetLength(s.Position);
            Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2277uL);
            if (s1 != null) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2278uL);
              s1.Adjust(headerBytesToRetract);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2279uL);
            _LengthOfHeader -= headerBytesToRetract;
            __FileDataPosition -= headerBytesToRetract;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2280uL);
          _Password = null;
          _BitField &= ~(0x1);
          int j = 6;
          _EntryHeader[j++] = (byte)(_BitField & 0xff);
          _EntryHeader[j++] = (byte)((_BitField & 0xff00) >> 8);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2281uL);
          if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2282uL);
            Int16 fnLength = (short)(_EntryHeader[26] + _EntryHeader[27] * 256);
            int offx = 30 + fnLength;
            int aesIndex = FindExtraFieldSegment(_EntryHeader, offx, 0x9901);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2283uL);
            if (aesIndex >= 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2284uL);
              _EntryHeader[aesIndex++] = 0x99;
              _EntryHeader[aesIndex++] = 0x99;
            }
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2285uL);
        CompressionMethod = 0;
        Encryption = EncryptionAlgorithm.None;
      } else if (_zipCrypto_forWrite != null || _aesCrypto_forWrite != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2286uL);
        if (Encryption == EncryptionAlgorithm.PkzipWeak) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2287uL);
          _CompressedSize += 12;
        } else if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2288uL);
          _CompressedSize += _aesCrypto_forWrite.SizeOfEncryptionMetadata;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2289uL);
      int i = 8;
      _EntryHeader[i++] = (byte)(_CompressionMethod & 0xff);
      _EntryHeader[i++] = (byte)((_CompressionMethod & 0xff00) >> 8);
      i = 14;
      _EntryHeader[i++] = (byte)(_Crc32 & 0xff);
      _EntryHeader[i++] = (byte)((_Crc32 & 0xff00) >> 8);
      _EntryHeader[i++] = (byte)((_Crc32 & 0xff0000) >> 16);
      _EntryHeader[i++] = (byte)((_Crc32 & 0xff000000u) >> 24);
      SetZip64Flags();
      Int16 filenameLength = (short)(_EntryHeader[26] + _EntryHeader[27] * 256);
      Int16 extraFieldLength = (short)(_EntryHeader[28] + _EntryHeader[29] * 256);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2290uL);
      if (_OutputUsesZip64.Value) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2291uL);
        _EntryHeader[4] = (byte)(45 & 0xff);
        _EntryHeader[5] = 0x0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2292uL);
        for (int j = 0; j < 8; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2293uL);
          _EntryHeader[i++] = 0xff;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2294uL);
        i = 30 + filenameLength;
        _EntryHeader[i++] = 0x1;
        _EntryHeader[i++] = 0x0;
        i += 2;
        Array.Copy(BitConverter.GetBytes(_UncompressedSize), 0, _EntryHeader, i, 8);
        i += 8;
        Array.Copy(BitConverter.GetBytes(_CompressedSize), 0, _EntryHeader, i, 8);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2295uL);
        _EntryHeader[4] = (byte)(20 & 0xff);
        _EntryHeader[5] = 0x0;
        i = 18;
        _EntryHeader[i++] = (byte)(_CompressedSize & 0xff);
        _EntryHeader[i++] = (byte)((_CompressedSize & 0xff00) >> 8);
        _EntryHeader[i++] = (byte)((_CompressedSize & 0xff0000) >> 16);
        _EntryHeader[i++] = (byte)((_CompressedSize & 0xff000000u) >> 24);
        _EntryHeader[i++] = (byte)(_UncompressedSize & 0xff);
        _EntryHeader[i++] = (byte)((_UncompressedSize & 0xff00) >> 8);
        _EntryHeader[i++] = (byte)((_UncompressedSize & 0xff0000) >> 16);
        _EntryHeader[i++] = (byte)((_UncompressedSize & 0xff000000u) >> 24);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2296uL);
        if (extraFieldLength != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2297uL);
          i = 30 + filenameLength;
          Int16 DataSize = (short)(_EntryHeader[i + 2] + _EntryHeader[i + 3] * 256);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2298uL);
          if (DataSize == 16) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2299uL);
            _EntryHeader[i++] = 0x99;
            _EntryHeader[i++] = 0x99;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2300uL);
      if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2301uL);
        i = 8;
        _EntryHeader[i++] = 0x63;
        _EntryHeader[i++] = 0;
        i = 30 + filenameLength;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2302uL);
        do {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2303uL);
          UInt16 HeaderId = (UInt16)(_EntryHeader[i] + _EntryHeader[i + 1] * 256);
          Int16 DataSize = (short)(_EntryHeader[i + 2] + _EntryHeader[i + 3] * 256);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2304uL);
          if (HeaderId != 0x9901) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2305uL);
            i += DataSize + 4;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2306uL);
            i += 9;
            _EntryHeader[i++] = (byte)(_CompressionMethod & 0xff);
            _EntryHeader[i++] = (byte)(_CompressionMethod & 0xff00);
          }
        } while (i < (extraFieldLength - 30 - filenameLength));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2307uL);
      if ((_BitField & 0x8) != 0x8 || (this._Source == ZipEntrySource.ZipOutputStream && s.CanSeek)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2308uL);
        var zss = s as ZipSegmentedStream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2309uL);
        if (zss != null && _diskNumber != zss.CurrentSegment) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2310uL);
          using (Stream hseg = ZipSegmentedStream.ForUpdate(this._container.ZipFile.Name, _diskNumber)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2311uL);
            hseg.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
            hseg.Write(_EntryHeader, 0, _EntryHeader.Length);
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2312uL);
          s.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
          s.Write(_EntryHeader, 0, _EntryHeader.Length);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2313uL);
          if (s1 != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2314uL);
            s1.Adjust(_EntryHeader.Length);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2315uL);
          s.Seek(_CompressedSize, SeekOrigin.Current);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2316uL);
      if (((_BitField & 0x8) == 0x8) && !IsDirectory) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2317uL);
        byte[] Descriptor = new byte[16 + (_OutputUsesZip64.Value ? 8 : 0)];
        i = 0;
        Array.Copy(BitConverter.GetBytes(ZipConstants.ZipEntryDataDescriptorSignature), 0, Descriptor, i, 4);
        i += 4;
        Array.Copy(BitConverter.GetBytes(_Crc32), 0, Descriptor, i, 4);
        i += 4;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2318uL);
        if (_OutputUsesZip64.Value) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2319uL);
          Array.Copy(BitConverter.GetBytes(_CompressedSize), 0, Descriptor, i, 8);
          i += 8;
          Array.Copy(BitConverter.GetBytes(_UncompressedSize), 0, Descriptor, i, 8);
          i += 8;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2320uL);
          Descriptor[i++] = (byte)(_CompressedSize & 0xff);
          Descriptor[i++] = (byte)((_CompressedSize & 0xff00) >> 8);
          Descriptor[i++] = (byte)((_CompressedSize & 0xff0000) >> 16);
          Descriptor[i++] = (byte)((_CompressedSize & 0xff000000u) >> 24);
          Descriptor[i++] = (byte)(_UncompressedSize & 0xff);
          Descriptor[i++] = (byte)((_UncompressedSize & 0xff00) >> 8);
          Descriptor[i++] = (byte)((_UncompressedSize & 0xff0000) >> 16);
          Descriptor[i++] = (byte)((_UncompressedSize & 0xff000000u) >> 24);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2321uL);
        s.Write(Descriptor, 0, Descriptor.Length);
        _LengthOfTrailer += Descriptor.Length;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2322uL);
    }
    private void SetZip64Flags()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2323uL);
      _entryRequiresZip64 = new Nullable<bool>(_CompressedSize >= 0xffffffffu || _UncompressedSize >= 0xffffffffu || _RelativeOffsetOfLocalHeader >= 0xffffffffu);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2324uL);
      if (_container.Zip64 == Zip64Option.Never && _entryRequiresZip64.Value) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2325uL);
        throw new ZipException("Compressed or Uncompressed size, or offset exceeds the maximum value. Consider setting the UseZip64WhenSaving property on the ZipFile instance.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2326uL);
      _OutputUsesZip64 = new Nullable<bool>(_container.Zip64 == Zip64Option.Always || _entryRequiresZip64.Value);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2327uL);
    }
    internal void PrepOutputStream(Stream s, long streamLength, out CountingStream outputCounter, out Stream encryptor, out Stream compressor, out Ionic.Crc.CrcCalculatorStream output)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2328uL);
      TraceWriteLine("PrepOutputStream: e({0}) comp({1}) crypto({2}) zf({3})", FileName, CompressionLevel, Encryption, _container.Name);
      outputCounter = new CountingStream(s);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2329uL);
      if (streamLength != 0L) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2330uL);
        encryptor = MaybeApplyEncryption(outputCounter);
        compressor = MaybeApplyCompression(encryptor, streamLength);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2331uL);
        encryptor = compressor = outputCounter;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2332uL);
      output = new Ionic.Crc.CrcCalculatorStream(compressor, true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2333uL);
    }
    private Stream MaybeApplyCompression(Stream s, long streamLength)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2334uL);
      if (_CompressionMethod == 0x8 && CompressionLevel != Ionic.Zlib.CompressionLevel.None) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2335uL);
        if (_container.ParallelDeflateThreshold == 0L || (streamLength > _container.ParallelDeflateThreshold && _container.ParallelDeflateThreshold > 0L)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2336uL);
          if (_container.ParallelDeflater == null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2337uL);
            _container.ParallelDeflater = new Ionic.Zlib.ParallelDeflateOutputStream(s, CompressionLevel, _container.Strategy, true);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2338uL);
            if (_container.CodecBufferSize > 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2339uL);
              _container.ParallelDeflater.BufferSize = _container.CodecBufferSize;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2340uL);
            if (_container.ParallelDeflateMaxBufferPairs > 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2341uL);
              _container.ParallelDeflater.MaxBufferPairs = _container.ParallelDeflateMaxBufferPairs;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2342uL);
          Ionic.Zlib.ParallelDeflateOutputStream o1 = _container.ParallelDeflater;
          o1.Reset(s);
          Stream RNTRNTRNT_332 = o1;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2343uL);
          return RNTRNTRNT_332;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2344uL);
        var o = new Ionic.Zlib.DeflateStream(s, Ionic.Zlib.CompressionMode.Compress, CompressionLevel, true);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2345uL);
        if (_container.CodecBufferSize > 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2346uL);
          o.BufferSize = _container.CodecBufferSize;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2347uL);
        o.Strategy = _container.Strategy;
        Stream RNTRNTRNT_333 = o;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2348uL);
        return RNTRNTRNT_333;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2349uL);
      if (_CompressionMethod == 0xc) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2350uL);
        if (_container.ParallelDeflateThreshold == 0L || (streamLength > _container.ParallelDeflateThreshold && _container.ParallelDeflateThreshold > 0L)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2351uL);
          var o1 = new Ionic.BZip2.ParallelBZip2OutputStream(s, true);
          Stream RNTRNTRNT_334 = o1;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2352uL);
          return RNTRNTRNT_334;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2353uL);
        var o = new Ionic.BZip2.BZip2OutputStream(s, true);
        Stream RNTRNTRNT_335 = o;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2354uL);
        return RNTRNTRNT_335;
      }
      Stream RNTRNTRNT_336 = s;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2355uL);
      return RNTRNTRNT_336;
    }
    private Stream MaybeApplyEncryption(Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2356uL);
      if (Encryption == EncryptionAlgorithm.PkzipWeak) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2357uL);
        TraceWriteLine("MaybeApplyEncryption: e({0}) PKZIP", FileName);
        Stream RNTRNTRNT_337 = new ZipCipherStream(s, _zipCrypto_forWrite, CryptoMode.Encrypt);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2358uL);
        return RNTRNTRNT_337;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2359uL);
      if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2360uL);
        TraceWriteLine("MaybeApplyEncryption: e({0}) AES", FileName);
        Stream RNTRNTRNT_338 = new WinZipAesCipherStream(s, _aesCrypto_forWrite, CryptoMode.Encrypt);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2361uL);
        return RNTRNTRNT_338;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2362uL);
      TraceWriteLine("MaybeApplyEncryption: e({0}) None", FileName);
      Stream RNTRNTRNT_339 = s;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2363uL);
      return RNTRNTRNT_339;
    }
    private void OnZipErrorWhileSaving(Exception e)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2364uL);
      if (_container.ZipFile != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2365uL);
        _ioOperationCanceled = _container.ZipFile.OnZipErrorSaving(this, e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2366uL);
    }
    internal void Write(Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2367uL);
      var cs1 = s as CountingStream;
      var zss1 = s as ZipSegmentedStream;
      bool done = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2368uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2369uL);
        try {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2370uL);
          if (_Source == ZipEntrySource.ZipFile && !_restreamRequiredOnSave) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2371uL);
            CopyThroughOneEntry(s);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2372uL);
            return;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2373uL);
          if (IsDirectory) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2374uL);
            WriteHeader(s, 1);
            StoreRelativeOffset();
            _entryRequiresZip64 = new Nullable<bool>(_RelativeOffsetOfLocalHeader >= 0xffffffffu);
            _OutputUsesZip64 = new Nullable<bool>(_container.Zip64 == Zip64Option.Always || _entryRequiresZip64.Value);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2375uL);
            if (zss1 != null) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2376uL);
              _diskNumber = zss1.CurrentSegment;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2377uL);
            return;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2378uL);
          bool readAgain = true;
          int nCycles = 0;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2379uL);
          do {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2380uL);
            nCycles++;
            WriteHeader(s, nCycles);
            WriteSecurityMetadata(s);
            _WriteEntryData(s);
            _TotalEntrySize = _LengthOfHeader + _CompressedFileDataSize + _LengthOfTrailer;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2381uL);
            if (nCycles > 1) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2382uL);
              readAgain = false;
            } else if (!s.CanSeek) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2384uL);
              readAgain = false;
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2383uL);
              readAgain = WantReadAgain();
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2385uL);
            if (readAgain) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2386uL);
              if (zss1 != null) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2387uL);
                zss1.TruncateBackward(_diskNumber, _RelativeOffsetOfLocalHeader);
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2388uL);
                s.Seek(_RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2389uL);
              s.SetLength(s.Position);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2390uL);
              if (cs1 != null) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2391uL);
                cs1.Adjust(_TotalEntrySize);
              }
            }
          } while (readAgain);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2392uL);
          _skippedDuringSave = false;
          done = true;
        } catch (System.Exception exc1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2393uL);
          ZipErrorAction orig = this.ZipErrorAction;
          int loop = 0;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2394uL);
          do {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2395uL);
            if (ZipErrorAction == ZipErrorAction.Throw) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2396uL);
              throw;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2397uL);
            if (ZipErrorAction == ZipErrorAction.Skip || ZipErrorAction == ZipErrorAction.Retry) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2398uL);
              long p1 = (cs1 != null) ? cs1.ComputedPosition : s.Position;
              long delta = p1 - _future_ROLH;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2399uL);
              if (delta > 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2400uL);
                s.Seek(delta, SeekOrigin.Current);
                long p2 = s.Position;
                s.SetLength(s.Position);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2401uL);
                if (cs1 != null) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2402uL);
                  cs1.Adjust(p1 - p2);
                }
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2403uL);
              if (ZipErrorAction == ZipErrorAction.Skip) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2404uL);
                WriteStatus("Skipping file {0} (exception: {1})", LocalFileName, exc1.ToString());
                _skippedDuringSave = true;
                done = true;
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2405uL);
                this.ZipErrorAction = orig;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2406uL);
              break;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2407uL);
            if (loop > 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2408uL);
              throw;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2409uL);
            if (ZipErrorAction == ZipErrorAction.InvokeErrorEvent) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2410uL);
              OnZipErrorWhileSaving(exc1);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2411uL);
              if (_ioOperationCanceled) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2412uL);
                done = true;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2413uL);
                break;
              }
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2414uL);
            loop++;
          } while (true);
        }
      } while (!done);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2415uL);
    }
    internal void StoreRelativeOffset()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2416uL);
      _RelativeOffsetOfLocalHeader = _future_ROLH;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2417uL);
    }
    internal void NotifySaveComplete()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2418uL);
      _Encryption_FromZipFile = _Encryption;
      _CompressionMethod_FromZipFile = _CompressionMethod;
      _restreamRequiredOnSave = false;
      _metadataChanged = false;
      _Source = ZipEntrySource.ZipFile;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2419uL);
    }
    internal void WriteSecurityMetadata(Stream outstream)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2420uL);
      if (Encryption == EncryptionAlgorithm.None) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2421uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2422uL);
      string pwd = this._Password;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2423uL);
      if (this._Source == ZipEntrySource.ZipFile && pwd == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2424uL);
        pwd = this._container.Password;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2425uL);
      if (pwd == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2426uL);
        _zipCrypto_forWrite = null;
        _aesCrypto_forWrite = null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2427uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2428uL);
      TraceWriteLine("WriteSecurityMetadata: e({0}) crypto({1}) pw({2})", FileName, Encryption.ToString(), pwd);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2429uL);
      if (Encryption == EncryptionAlgorithm.PkzipWeak) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2430uL);
        _zipCrypto_forWrite = ZipCrypto.ForWrite(pwd);
        var rnd = new System.Random();
        byte[] encryptionHeader = new byte[12];
        rnd.NextBytes(encryptionHeader);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2431uL);
        if ((this._BitField & 0x8) == 0x8) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2432uL);
          _TimeBlob = Ionic.Zip.SharedUtilities.DateTimeToPacked(LastModified);
          encryptionHeader[11] = (byte)((this._TimeBlob >> 8) & 0xff);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2433uL);
          FigureCrc32();
          encryptionHeader[11] = (byte)((this._Crc32 >> 24) & 0xff);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2434uL);
        byte[] cipherText = _zipCrypto_forWrite.EncryptMessage(encryptionHeader, encryptionHeader.Length);
        outstream.Write(cipherText, 0, cipherText.Length);
        _LengthOfHeader += cipherText.Length;
      } else if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2435uL);
        int keystrength = GetKeyStrengthInBits(Encryption);
        _aesCrypto_forWrite = WinZipAesCrypto.Generate(pwd, keystrength);
        outstream.Write(_aesCrypto_forWrite.Salt, 0, _aesCrypto_forWrite._Salt.Length);
        outstream.Write(_aesCrypto_forWrite.GeneratedPV, 0, _aesCrypto_forWrite.GeneratedPV.Length);
        _LengthOfHeader += _aesCrypto_forWrite._Salt.Length + _aesCrypto_forWrite.GeneratedPV.Length;
        TraceWriteLine("WriteSecurityMetadata: AES e({0}) keybits({1}) _LOH({2})", FileName, keystrength, _LengthOfHeader);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2436uL);
    }
    private void CopyThroughOneEntry(Stream outStream)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2437uL);
      if (this.LengthOfHeader == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2438uL);
        throw new BadStateException("Bad header length.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2439uL);
      bool needRecompute = _metadataChanged || (this.ArchiveStream is ZipSegmentedStream) || (outStream is ZipSegmentedStream) || (_InputUsesZip64 && _container.UseZip64WhenSaving == Zip64Option.Never) || (!_InputUsesZip64 && _container.UseZip64WhenSaving == Zip64Option.Always);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2440uL);
      if (needRecompute) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2441uL);
        CopyThroughWithRecompute(outStream);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2442uL);
        CopyThroughWithNoChange(outStream);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2443uL);
      _entryRequiresZip64 = new Nullable<bool>(_CompressedSize >= 0xffffffffu || _UncompressedSize >= 0xffffffffu || _RelativeOffsetOfLocalHeader >= 0xffffffffu);
      _OutputUsesZip64 = new Nullable<bool>(_container.Zip64 == Zip64Option.Always || _entryRequiresZip64.Value);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2444uL);
    }
    private void CopyThroughWithRecompute(Stream outstream)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2445uL);
      int n;
      byte[] bytes = new byte[BufferSize];
      var input = new CountingStream(this.ArchiveStream);
      long origRelativeOffsetOfHeader = _RelativeOffsetOfLocalHeader;
      int origLengthOfHeader = LengthOfHeader;
      WriteHeader(outstream, 0);
      StoreRelativeOffset();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2446uL);
      if (!this.FileName.EndsWith("/")) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2447uL);
        long pos = origRelativeOffsetOfHeader + origLengthOfHeader;
        int len = GetLengthOfCryptoHeaderBytes(_Encryption_FromZipFile);
        pos -= len;
        _LengthOfHeader += len;
        input.Seek(pos, SeekOrigin.Begin);
        long remaining = this._CompressedSize;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2448uL);
        while (remaining > 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2449uL);
          len = (remaining > bytes.Length) ? bytes.Length : (int)remaining;
          n = input.Read(bytes, 0, len);
          outstream.Write(bytes, 0, n);
          remaining -= n;
          OnWriteBlock(input.BytesRead, this._CompressedSize);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2450uL);
          if (_ioOperationCanceled) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2451uL);
            break;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2452uL);
        if ((this._BitField & 0x8) == 0x8) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2453uL);
          int size = 16;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2454uL);
          if (_InputUsesZip64) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2455uL);
            size += 8;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2456uL);
          byte[] Descriptor = new byte[size];
          input.Read(Descriptor, 0, size);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2457uL);
          if (_InputUsesZip64 && _container.UseZip64WhenSaving == Zip64Option.Never) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2458uL);
            outstream.Write(Descriptor, 0, 8);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2459uL);
            if (_CompressedSize > 0xffffffffu) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2460uL);
              throw new InvalidOperationException("ZIP64 is required");
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2461uL);
            outstream.Write(Descriptor, 8, 4);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2462uL);
            if (_UncompressedSize > 0xffffffffu) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2463uL);
              throw new InvalidOperationException("ZIP64 is required");
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2464uL);
            outstream.Write(Descriptor, 16, 4);
            _LengthOfTrailer -= 8;
          } else if (!_InputUsesZip64 && _container.UseZip64WhenSaving == Zip64Option.Always) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2466uL);
            byte[] pad = new byte[4];
            outstream.Write(Descriptor, 0, 8);
            outstream.Write(Descriptor, 8, 4);
            outstream.Write(pad, 0, 4);
            outstream.Write(Descriptor, 12, 4);
            outstream.Write(pad, 0, 4);
            _LengthOfTrailer += 8;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2465uL);
            outstream.Write(Descriptor, 0, size);
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2467uL);
      _TotalEntrySize = _LengthOfHeader + _CompressedFileDataSize + _LengthOfTrailer;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2468uL);
    }
    private void CopyThroughWithNoChange(Stream outstream)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2469uL);
      int n;
      byte[] bytes = new byte[BufferSize];
      var input = new CountingStream(this.ArchiveStream);
      input.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2470uL);
      if (this._TotalEntrySize == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2471uL);
        this._TotalEntrySize = this._LengthOfHeader + this._CompressedFileDataSize + _LengthOfTrailer;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2472uL);
      var counter = outstream as CountingStream;
      _RelativeOffsetOfLocalHeader = (counter != null) ? counter.ComputedPosition : outstream.Position;
      long remaining = this._TotalEntrySize;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2473uL);
      while (remaining > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2474uL);
        int len = (remaining > bytes.Length) ? bytes.Length : (int)remaining;
        n = input.Read(bytes, 0, len);
        outstream.Write(bytes, 0, n);
        remaining -= n;
        OnWriteBlock(input.BytesRead, this._TotalEntrySize);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2475uL);
        if (_ioOperationCanceled) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2476uL);
          break;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2477uL);
    }
    [System.Diagnostics.ConditionalAttribute("Trace")]
    private void TraceWriteLine(string format, params object[] varParams)
    {
      lock (_outputLock) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2478uL);
        int tid = System.Threading.Thread.CurrentThread.GetHashCode();
        Console.ForegroundColor = (ConsoleColor)(tid % 8 + 8);
        Console.Write("{0:000} ZipEntry.Write ", tid);
        Console.WriteLine(format, varParams);
        Console.ResetColor();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2479uL);
    }
    private object _outputLock = new Object();
  }
}
