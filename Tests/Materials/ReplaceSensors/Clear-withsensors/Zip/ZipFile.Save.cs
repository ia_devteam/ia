using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public partial class ZipFile
  {
    private void DeleteFileWithRetry(string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3191uL);
      bool done = false;
      int nRetries = 3;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3192uL);
      for (int i = 0; i < nRetries && !done; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3193uL);
        try {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3194uL);
          File.Delete(filename);
          done = true;
        } catch (System.UnauthorizedAccessException) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3195uL);
          Console.WriteLine("************************************************** Retry delete.");
          System.Threading.Thread.Sleep(200 + i * 200);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3196uL);
    }
    public void Save()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3197uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3198uL);
        bool thisSaveUsedZip64 = false;
        _saveOperationCanceled = false;
        _numberOfSegmentsForMostRecentSave = 0;
        OnSaveStarted();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3199uL);
        if (WriteStream == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3200uL);
          throw new BadStateException("You haven't specified where to save the zip.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3201uL);
        if (_name != null && _name.EndsWith(".exe") && !_SavingSfx) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3202uL);
          throw new BadStateException("You specified an EXE for a plain zip file.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3203uL);
        if (!_contentsChanged) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3204uL);
          OnSaveCompleted();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3205uL);
          if (Verbose) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3206uL);
            StatusMessageTextWriter.WriteLine("No save is necessary....");
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3207uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3208uL);
        Reset(true);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3209uL);
        if (Verbose) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3210uL);
          StatusMessageTextWriter.WriteLine("saving....");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3211uL);
        if (_entries.Count >= 0xffff && _zip64 == Zip64Option.Never) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3212uL);
          throw new ZipException("The number of entries is 65535 or greater. Consider setting the UseZip64WhenSaving property on the ZipFile instance.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3213uL);
        int n = 0;
        ICollection<ZipEntry> c = (SortEntriesBeforeSaving) ? EntriesSorted : Entries;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3214uL);
        foreach (ZipEntry e in c) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3215uL);
          OnSaveEntry(n, e, true);
          e.Write(WriteStream);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3216uL);
          if (_saveOperationCanceled) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3217uL);
            break;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3218uL);
          n++;
          OnSaveEntry(n, e, false);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3219uL);
          if (_saveOperationCanceled) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3220uL);
            break;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3221uL);
          if (e.IncludedInMostRecentSave) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3222uL);
            thisSaveUsedZip64 |= e.OutputUsedZip64.Value;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3223uL);
        if (_saveOperationCanceled) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3224uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3225uL);
        var zss = WriteStream as ZipSegmentedStream;
        _numberOfSegmentsForMostRecentSave = (zss != null) ? zss.CurrentSegment : 1;
        bool directoryNeededZip64 = ZipOutput.WriteCentralDirectoryStructure(WriteStream, c, _numberOfSegmentsForMostRecentSave, _zip64, Comment, new ZipContainer(this));
        OnSaveEvent(ZipProgressEventType.Saving_AfterSaveTempArchive);
        _hasBeenSaved = true;
        _contentsChanged = false;
        thisSaveUsedZip64 |= directoryNeededZip64;
        _OutputUsesZip64 = new Nullable<bool>(thisSaveUsedZip64);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3226uL);
        if (_name != null && (_temporaryFileName != null || zss != null)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3227uL);
          WriteStream.Dispose();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3228uL);
          if (_saveOperationCanceled) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3229uL);
            return;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3230uL);
          if (_fileAlreadyExists && this._readstream != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3231uL);
            this._readstream.Close();
            this._readstream = null;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3232uL);
            foreach (var e in c) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3233uL);
              var zss1 = e._archiveStream as ZipSegmentedStream;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3234uL);
              if (zss1 != null) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3235uL);
                zss1.Dispose();
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3236uL);
              e._archiveStream = null;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3237uL);
          string tmpName = null;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3238uL);
          if (File.Exists(_name)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3239uL);
            tmpName = _name + "." + Path.GetRandomFileName();
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3240uL);
            if (File.Exists(tmpName)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3241uL);
              DeleteFileWithRetry(tmpName);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3242uL);
            File.Move(_name, tmpName);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3243uL);
          OnSaveEvent(ZipProgressEventType.Saving_BeforeRenameTempArchive);
          File.Move((zss != null) ? zss.CurrentTempName : _temporaryFileName, _name);
          OnSaveEvent(ZipProgressEventType.Saving_AfterRenameTempArchive);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3244uL);
          if (tmpName != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3245uL);
            try {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3246uL);
              if (File.Exists(tmpName)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3247uL);
                File.Delete(tmpName);
              }
            } catch {
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3248uL);
          _fileAlreadyExists = true;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3249uL);
        NotifyEntriesSaveComplete(c);
        OnSaveCompleted();
        _JustSaved = true;
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3250uL);
        CleanupAfterSaveOperation();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3251uL);
      return;
    }
    private static void NotifyEntriesSaveComplete(ICollection<ZipEntry> c)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3252uL);
      foreach (ZipEntry e in c) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3253uL);
        e.NotifySaveComplete();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3254uL);
    }
    private void RemoveTempFile()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3255uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3256uL);
        if (File.Exists(_temporaryFileName)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3257uL);
          File.Delete(_temporaryFileName);
        }
      } catch (IOException ex1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3258uL);
        if (Verbose) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3259uL);
          StatusMessageTextWriter.WriteLine("ZipFile::Save: could not delete temp file: {0}.", ex1.Message);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3260uL);
    }
    private void CleanupAfterSaveOperation()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3261uL);
      if (_name != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3262uL);
        if (_writestream != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3263uL);
          try {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3264uL);
            _writestream.Dispose();
          } catch (System.IO.IOException) {
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3265uL);
        _writestream = null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3266uL);
        if (_temporaryFileName != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3267uL);
          RemoveTempFile();
          _temporaryFileName = null;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3268uL);
    }
    public void Save(String fileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3269uL);
      if (_name == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3270uL);
        _writestream = null;
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3271uL);
        _readName = _name;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3272uL);
      _name = fileName;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3273uL);
      if (Directory.Exists(_name)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3274uL);
        throw new ZipException("Bad Directory", new System.ArgumentException("That name specifies an existing directory. Please specify a filename.", "fileName"));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3275uL);
      _contentsChanged = true;
      _fileAlreadyExists = File.Exists(_name);
      Save();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3276uL);
    }
    public void Save(Stream outputStream)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3277uL);
      if (outputStream == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3278uL);
        throw new ArgumentNullException("outputStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3279uL);
      if (!outputStream.CanWrite) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3280uL);
        throw new ArgumentException("Must be a writable stream.", "outputStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3281uL);
      _name = null;
      _writestream = new CountingStream(outputStream);
      _contentsChanged = true;
      _fileAlreadyExists = false;
      Save();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3282uL);
    }
  }
  static internal class ZipOutput
  {
    public static bool WriteCentralDirectoryStructure(Stream s, ICollection<ZipEntry> entries, uint numSegments, Zip64Option zip64, String comment, ZipContainer container)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3283uL);
      var zss = s as ZipSegmentedStream;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3284uL);
      if (zss != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3285uL);
        zss.ContiguousWrite = true;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3286uL);
      Int64 aLength = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3287uL);
      using (var ms = new MemoryStream()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3288uL);
        foreach (ZipEntry e in entries) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3289uL);
          if (e.IncludedInMostRecentSave) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3290uL);
            e.WriteCentralDirectoryEntry(ms);
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3291uL);
        var a = ms.ToArray();
        s.Write(a, 0, a.Length);
        aLength = a.Length;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3292uL);
      var output = s as CountingStream;
      long Finish = (output != null) ? output.ComputedPosition : s.Position;
      long Start = Finish - aLength;
      UInt32 startSegment = (zss != null) ? zss.CurrentSegment : 0;
      Int64 SizeOfCentralDirectory = Finish - Start;
      int countOfEntries = CountEntries(entries);
      bool needZip64CentralDirectory = zip64 == Zip64Option.Always || countOfEntries >= 0xffff || SizeOfCentralDirectory > 0xffffffffu || Start > 0xffffffffu;
      byte[] a2 = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3293uL);
      if (needZip64CentralDirectory) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3294uL);
        if (zip64 == Zip64Option.Never) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3295uL);
          System.Diagnostics.StackFrame sf = new System.Diagnostics.StackFrame(1);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3296uL);
          if (sf.GetMethod().DeclaringType == typeof(ZipFile)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3297uL);
            throw new ZipException("The archive requires a ZIP64 Central Directory. Consider setting the ZipFile.UseZip64WhenSaving property.");
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3298uL);
            throw new ZipException("The archive requires a ZIP64 Central Directory. Consider setting the ZipOutputStream.EnableZip64 property.");
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3299uL);
        var a = GenZip64EndOfCentralDirectory(Start, Finish, countOfEntries, numSegments);
        a2 = GenCentralDirectoryFooter(Start, Finish, zip64, countOfEntries, comment, container);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3300uL);
        if (startSegment != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3301uL);
          UInt32 thisSegment = zss.ComputeSegment(a.Length + a2.Length);
          int i = 16;
          Array.Copy(BitConverter.GetBytes(thisSegment), 0, a, i, 4);
          i += 4;
          Array.Copy(BitConverter.GetBytes(thisSegment), 0, a, i, 4);
          i = 60;
          Array.Copy(BitConverter.GetBytes(thisSegment), 0, a, i, 4);
          i += 4;
          i += 8;
          Array.Copy(BitConverter.GetBytes(thisSegment), 0, a, i, 4);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3302uL);
        s.Write(a, 0, a.Length);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3303uL);
        a2 = GenCentralDirectoryFooter(Start, Finish, zip64, countOfEntries, comment, container);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3304uL);
      if (startSegment != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3305uL);
        UInt16 thisSegment = (UInt16)zss.ComputeSegment(a2.Length);
        int i = 4;
        Array.Copy(BitConverter.GetBytes(thisSegment), 0, a2, i, 2);
        i += 2;
        Array.Copy(BitConverter.GetBytes(thisSegment), 0, a2, i, 2);
        i += 2;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3306uL);
      s.Write(a2, 0, a2.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3307uL);
      if (zss != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3308uL);
        zss.ContiguousWrite = false;
      }
      System.Boolean RNTRNTRNT_439 = needZip64CentralDirectory;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3309uL);
      return RNTRNTRNT_439;
    }
    private static System.Text.Encoding GetEncoding(ZipContainer container, string t)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3310uL);
      switch (container.AlternateEncodingUsage) {
        case ZipOption.Always:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3312uL);
          
          {
            System.Text.Encoding RNTRNTRNT_440 = container.AlternateEncoding;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3311uL);
            return RNTRNTRNT_440;
          }

        case ZipOption.Never:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3314uL);
          
          {
            System.Text.Encoding RNTRNTRNT_441 = container.DefaultEncoding;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3313uL);
            return RNTRNTRNT_441;
          }

      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3315uL);
      var e = container.DefaultEncoding;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3316uL);
      if (t == null) {
        System.Text.Encoding RNTRNTRNT_442 = e;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3317uL);
        return RNTRNTRNT_442;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3318uL);
      var bytes = e.GetBytes(t);
      var t2 = e.GetString(bytes, 0, bytes.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3319uL);
      if (t2.Equals(t)) {
        System.Text.Encoding RNTRNTRNT_443 = e;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3320uL);
        return RNTRNTRNT_443;
      }
      System.Text.Encoding RNTRNTRNT_444 = container.AlternateEncoding;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3321uL);
      return RNTRNTRNT_444;
    }
    private static byte[] GenCentralDirectoryFooter(long StartOfCentralDirectory, long EndOfCentralDirectory, Zip64Option zip64, int entryCount, string comment, ZipContainer container)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3322uL);
      System.Text.Encoding encoding = GetEncoding(container, comment);
      int j = 0;
      int bufferLength = 22;
      byte[] block = null;
      Int16 commentLength = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3323uL);
      if ((comment != null) && (comment.Length != 0)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3324uL);
        block = encoding.GetBytes(comment);
        commentLength = (Int16)block.Length;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3325uL);
      bufferLength += commentLength;
      byte[] bytes = new byte[bufferLength];
      int i = 0;
      byte[] sig = BitConverter.GetBytes(ZipConstants.EndOfCentralDirectorySignature);
      Array.Copy(sig, 0, bytes, i, 4);
      i += 4;
      bytes[i++] = 0;
      bytes[i++] = 0;
      bytes[i++] = 0;
      bytes[i++] = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3326uL);
      if (entryCount >= 0xffff || zip64 == Zip64Option.Always) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3327uL);
        for (j = 0; j < 4; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3328uL);
          bytes[i++] = 0xff;
        }
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3329uL);
        bytes[i++] = (byte)(entryCount & 0xff);
        bytes[i++] = (byte)((entryCount & 0xff00) >> 8);
        bytes[i++] = (byte)(entryCount & 0xff);
        bytes[i++] = (byte)((entryCount & 0xff00) >> 8);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3330uL);
      Int64 SizeOfCentralDirectory = EndOfCentralDirectory - StartOfCentralDirectory;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3331uL);
      if (SizeOfCentralDirectory >= 0xffffffffu || StartOfCentralDirectory >= 0xffffffffu) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3332uL);
        for (j = 0; j < 8; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3333uL);
          bytes[i++] = 0xff;
        }
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3334uL);
        bytes[i++] = (byte)(SizeOfCentralDirectory & 0xff);
        bytes[i++] = (byte)((SizeOfCentralDirectory & 0xff00) >> 8);
        bytes[i++] = (byte)((SizeOfCentralDirectory & 0xff0000) >> 16);
        bytes[i++] = (byte)((SizeOfCentralDirectory & 0xff000000u) >> 24);
        bytes[i++] = (byte)(StartOfCentralDirectory & 0xff);
        bytes[i++] = (byte)((StartOfCentralDirectory & 0xff00) >> 8);
        bytes[i++] = (byte)((StartOfCentralDirectory & 0xff0000) >> 16);
        bytes[i++] = (byte)((StartOfCentralDirectory & 0xff000000u) >> 24);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3335uL);
      if ((comment == null) || (comment.Length == 0)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3336uL);
        bytes[i++] = (byte)0;
        bytes[i++] = (byte)0;
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3337uL);
        if (commentLength + i + 2 > bytes.Length) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3338uL);
          commentLength = (Int16)(bytes.Length - i - 2);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3339uL);
        bytes[i++] = (byte)(commentLength & 0xff);
        bytes[i++] = (byte)((commentLength & 0xff00) >> 8);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3340uL);
        if (commentLength != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3341uL);
          for (j = 0; (j < commentLength) && (i + j < bytes.Length); j++) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3342uL);
            bytes[i + j] = block[j];
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3343uL);
          i += j;
        }
      }
      System.Byte[] RNTRNTRNT_445 = bytes;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3344uL);
      return RNTRNTRNT_445;
    }
    private static byte[] GenZip64EndOfCentralDirectory(long StartOfCentralDirectory, long EndOfCentralDirectory, int entryCount, uint numSegments)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3345uL);
      const int bufferLength = 12 + 44 + 20;
      byte[] bytes = new byte[bufferLength];
      int i = 0;
      byte[] sig = BitConverter.GetBytes(ZipConstants.Zip64EndOfCentralDirectoryRecordSignature);
      Array.Copy(sig, 0, bytes, i, 4);
      i += 4;
      long DataSize = 44;
      Array.Copy(BitConverter.GetBytes(DataSize), 0, bytes, i, 8);
      i += 8;
      bytes[i++] = 45;
      bytes[i++] = 0x0;
      bytes[i++] = 45;
      bytes[i++] = 0x0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3346uL);
      for (int j = 0; j < 8; j++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3347uL);
        bytes[i++] = 0x0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3348uL);
      long numberOfEntries = entryCount;
      Array.Copy(BitConverter.GetBytes(numberOfEntries), 0, bytes, i, 8);
      i += 8;
      Array.Copy(BitConverter.GetBytes(numberOfEntries), 0, bytes, i, 8);
      i += 8;
      Int64 SizeofCentraldirectory = EndOfCentralDirectory - StartOfCentralDirectory;
      Array.Copy(BitConverter.GetBytes(SizeofCentraldirectory), 0, bytes, i, 8);
      i += 8;
      Array.Copy(BitConverter.GetBytes(StartOfCentralDirectory), 0, bytes, i, 8);
      i += 8;
      sig = BitConverter.GetBytes(ZipConstants.Zip64EndOfCentralDirectoryLocatorSignature);
      Array.Copy(sig, 0, bytes, i, 4);
      i += 4;
      uint x2 = (numSegments == 0) ? 0 : (uint)(numSegments - 1);
      Array.Copy(BitConverter.GetBytes(x2), 0, bytes, i, 4);
      i += 4;
      Array.Copy(BitConverter.GetBytes(EndOfCentralDirectory), 0, bytes, i, 8);
      i += 8;
      Array.Copy(BitConverter.GetBytes(numSegments), 0, bytes, i, 4);
      i += 4;
      System.Byte[] RNTRNTRNT_446 = bytes;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3349uL);
      return RNTRNTRNT_446;
    }
    private static int CountEntries(ICollection<ZipEntry> _entries)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3350uL);
      int count = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3351uL);
      foreach (var entry in _entries) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3352uL);
        if (entry.IncludedInMostRecentSave) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3353uL);
          count++;
        }
      }
      System.Int32 RNTRNTRNT_447 = count;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3354uL);
      return RNTRNTRNT_447;
    }
  }
}
