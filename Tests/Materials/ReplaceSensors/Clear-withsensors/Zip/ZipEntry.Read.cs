using System;
using System.IO;
namespace Ionic.Zip
{
  public partial class ZipEntry
  {
    private int _readExtraDepth;
    private void ReadExtraField()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1831uL);
      _readExtraDepth++;
      long posn = this.ArchiveStream.Position;
      this.ArchiveStream.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      byte[] block = new byte[30];
      this.ArchiveStream.Read(block, 0, block.Length);
      int i = 26;
      Int16 filenameLength = (short)(block[i++] + block[i++] * 256);
      Int16 extraFieldLength = (short)(block[i++] + block[i++] * 256);
      this.ArchiveStream.Seek(filenameLength, SeekOrigin.Current);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      ProcessExtraField(this.ArchiveStream, extraFieldLength);
      this.ArchiveStream.Seek(posn, SeekOrigin.Begin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      _readExtraDepth--;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1832uL);
    }
    private static bool ReadHeader(ZipEntry ze, System.Text.Encoding defaultEncoding)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1833uL);
      int bytesRead = 0;
      ze._RelativeOffsetOfLocalHeader = ze.ArchiveStream.Position;
      int signature = Ionic.Zip.SharedUtilities.ReadEntrySignature(ze.ArchiveStream);
      bytesRead += 4;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1834uL);
      if (ZipEntry.IsNotValidSig(signature)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1835uL);
        ze.ArchiveStream.Seek(-4, SeekOrigin.Current);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(ze.ArchiveStream);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1836uL);
        if (ZipEntry.IsNotValidZipDirEntrySig(signature) && (signature != ZipConstants.EndOfCentralDirectorySignature)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1837uL);
          throw new BadReadException(String.Format("  Bad signature (0x{0:X8}) at position  0x{1:X8}", signature, ze.ArchiveStream.Position));
        }
        System.Boolean RNTRNTRNT_296 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1838uL);
        return RNTRNTRNT_296;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1839uL);
      byte[] block = new byte[26];
      int n = ze.ArchiveStream.Read(block, 0, block.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1840uL);
      if (n != block.Length) {
        System.Boolean RNTRNTRNT_297 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1841uL);
        return RNTRNTRNT_297;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1842uL);
      bytesRead += n;
      int i = 0;
      ze._VersionNeeded = (Int16)(block[i++] + block[i++] * 256);
      ze._BitField = (Int16)(block[i++] + block[i++] * 256);
      ze._CompressionMethod_FromZipFile = ze._CompressionMethod = (Int16)(block[i++] + block[i++] * 256);
      ze._TimeBlob = block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256;
      ze._LastModified = Ionic.Zip.SharedUtilities.PackedToDateTime(ze._TimeBlob);
      ze._timestamp |= ZipEntryTimestamp.DOS;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1843uL);
      if ((ze._BitField & 0x1) == 0x1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1844uL);
        ze._Encryption_FromZipFile = ze._Encryption = EncryptionAlgorithm.PkzipWeak;
        ze._sourceIsEncrypted = true;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1848uL);
      {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1845uL);
        ze._Crc32 = (Int32)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
        ze._CompressedSize = (uint)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
        ze._UncompressedSize = (uint)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1846uL);
        if ((uint)ze._CompressedSize == 0xffffffffu || (uint)ze._UncompressedSize == 0xffffffffu) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1847uL);
          ze._InputUsesZip64 = true;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1849uL);
      Int16 filenameLength = (short)(block[i++] + block[i++] * 256);
      Int16 extraFieldLength = (short)(block[i++] + block[i++] * 256);
      block = new byte[filenameLength];
      n = ze.ArchiveStream.Read(block, 0, block.Length);
      bytesRead += n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1850uL);
      if ((ze._BitField & 0x800) == 0x800) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1851uL);
        ze.AlternateEncoding = System.Text.Encoding.UTF8;
        ze.AlternateEncodingUsage = ZipOption.Always;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1852uL);
      ze._FileNameInArchive = ze.AlternateEncoding.GetString(block, 0, block.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1853uL);
      if (ze._FileNameInArchive.EndsWith("/")) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1854uL);
        ze.MarkAsDirectory();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1855uL);
      bytesRead += ze.ProcessExtraField(ze.ArchiveStream, extraFieldLength);
      ze._LengthOfTrailer = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1856uL);
      if (!ze._FileNameInArchive.EndsWith("/") && (ze._BitField & 0x8) == 0x8) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1857uL);
        long posn = ze.ArchiveStream.Position;
        bool wantMore = true;
        long SizeOfDataRead = 0;
        int tries = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1858uL);
        while (wantMore) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1859uL);
          tries++;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1860uL);
          if (ze._container.ZipFile != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1861uL);
            ze._container.ZipFile.OnReadBytes(ze);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1862uL);
          long d = Ionic.Zip.SharedUtilities.FindSignature(ze.ArchiveStream, ZipConstants.ZipEntryDataDescriptorSignature);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1863uL);
          if (d == -1) {
            System.Boolean RNTRNTRNT_298 = false;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1864uL);
            return RNTRNTRNT_298;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1865uL);
          SizeOfDataRead += d;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1866uL);
          if (ze._InputUsesZip64) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1867uL);
            block = new byte[20];
            n = ze.ArchiveStream.Read(block, 0, block.Length);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1868uL);
            if (n != 20) {
              System.Boolean RNTRNTRNT_299 = false;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1869uL);
              return RNTRNTRNT_299;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1870uL);
            i = 0;
            ze._Crc32 = (Int32)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
            ze._CompressedSize = BitConverter.ToInt64(block, i);
            i += 8;
            ze._UncompressedSize = BitConverter.ToInt64(block, i);
            i += 8;
            ze._LengthOfTrailer += 24;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1871uL);
            block = new byte[12];
            n = ze.ArchiveStream.Read(block, 0, block.Length);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1872uL);
            if (n != 12) {
              System.Boolean RNTRNTRNT_300 = false;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1873uL);
              return RNTRNTRNT_300;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1874uL);
            i = 0;
            ze._Crc32 = (Int32)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
            ze._CompressedSize = (uint)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
            ze._UncompressedSize = (uint)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
            ze._LengthOfTrailer += 16;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1875uL);
          wantMore = (SizeOfDataRead != ze._CompressedSize);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1876uL);
          if (wantMore) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1877uL);
            ze.ArchiveStream.Seek(-12, SeekOrigin.Current);
            Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(ze.ArchiveStream);
            SizeOfDataRead += 4;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1878uL);
        ze.ArchiveStream.Seek(posn, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(ze.ArchiveStream);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1879uL);
      ze._CompressedFileDataSize = ze._CompressedSize;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1880uL);
      if ((ze._BitField & 0x1) == 0x1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1881uL);
        if (ze.Encryption == EncryptionAlgorithm.WinZipAes128 || ze.Encryption == EncryptionAlgorithm.WinZipAes256) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1882uL);
          int bits = ZipEntry.GetKeyStrengthInBits(ze._Encryption_FromZipFile);
          ze._aesCrypto_forExtract = WinZipAesCrypto.ReadFromStream(null, bits, ze.ArchiveStream);
          bytesRead += ze._aesCrypto_forExtract.SizeOfEncryptionMetadata - 10;
          ze._CompressedFileDataSize -= ze._aesCrypto_forExtract.SizeOfEncryptionMetadata;
          ze._LengthOfTrailer += 10;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1883uL);
          ze._WeakEncryptionHeader = new byte[12];
          bytesRead += ZipEntry.ReadWeakEncryptionHeader(ze._archiveStream, ze._WeakEncryptionHeader);
          ze._CompressedFileDataSize -= 12;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1884uL);
      ze._LengthOfHeader = bytesRead;
      ze._TotalEntrySize = ze._LengthOfHeader + ze._CompressedFileDataSize + ze._LengthOfTrailer;
      System.Boolean RNTRNTRNT_301 = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1885uL);
      return RNTRNTRNT_301;
    }
    static internal int ReadWeakEncryptionHeader(Stream s, byte[] buffer)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1886uL);
      int additionalBytesRead = s.Read(buffer, 0, 12);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1887uL);
      if (additionalBytesRead != 12) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1888uL);
        throw new ZipException(String.Format("Unexpected end of data at position 0x{0:X8}", s.Position));
      }
      System.Int32 RNTRNTRNT_302 = additionalBytesRead;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1889uL);
      return RNTRNTRNT_302;
    }
    private static bool IsNotValidSig(int signature)
    {
      System.Boolean RNTRNTRNT_303 = (signature != ZipConstants.ZipEntrySignature);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1890uL);
      return RNTRNTRNT_303;
    }
    static internal ZipEntry ReadEntry(ZipContainer zc, bool first)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1891uL);
      ZipFile zf = zc.ZipFile;
      Stream s = zc.ReadStream;
      System.Text.Encoding defaultEncoding = zc.AlternateEncoding;
      ZipEntry entry = new ZipEntry();
      entry._Source = ZipEntrySource.ZipFile;
      entry._container = zc;
      entry._archiveStream = s;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1892uL);
      if (zf != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1893uL);
        zf.OnReadEntry(true, null);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1894uL);
      if (first) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1895uL);
        HandlePK00Prefix(s);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1896uL);
      if (!ReadHeader(entry, defaultEncoding)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1897uL);
        return null;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1898uL);
      entry.__FileDataPosition = entry.ArchiveStream.Position;
      s.Seek(entry._CompressedFileDataSize + entry._LengthOfTrailer, SeekOrigin.Current);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
      HandleUnexpectedDataDescriptor(entry);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1899uL);
      if (zf != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1900uL);
        zf.OnReadBytes(entry);
        zf.OnReadEntry(false, entry);
      }
      ZipEntry RNTRNTRNT_304 = entry;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1901uL);
      return RNTRNTRNT_304;
    }
    static internal void HandlePK00Prefix(Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1902uL);
      uint datum = (uint)Ionic.Zip.SharedUtilities.ReadInt(s);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1903uL);
      if (datum != ZipConstants.PackedToRemovableMedia) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1904uL);
        s.Seek(-4, SeekOrigin.Current);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1905uL);
    }
    private static void HandleUnexpectedDataDescriptor(ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1906uL);
      Stream s = entry.ArchiveStream;
      uint datum = (uint)Ionic.Zip.SharedUtilities.ReadInt(s);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1907uL);
      if (datum == entry._Crc32) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1908uL);
        int sz = Ionic.Zip.SharedUtilities.ReadInt(s);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1909uL);
        if (sz == entry._CompressedSize) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1910uL);
          sz = Ionic.Zip.SharedUtilities.ReadInt(s);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1911uL);
          if (sz == entry._UncompressedSize) {
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1912uL);
            s.Seek(-12, SeekOrigin.Current);
            Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1913uL);
          s.Seek(-8, SeekOrigin.Current);
          Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
        }
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1914uL);
        s.Seek(-4, SeekOrigin.Current);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1915uL);
    }
    static internal int FindExtraFieldSegment(byte[] extra, int offx, UInt16 targetHeaderId)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1916uL);
      int j = offx;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1917uL);
      while (j + 3 < extra.Length) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1918uL);
        UInt16 headerId = (UInt16)(extra[j++] + extra[j++] * 256);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1919uL);
        if (headerId == targetHeaderId) {
          System.Int32 RNTRNTRNT_305 = j - 2;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1920uL);
          return RNTRNTRNT_305;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1921uL);
        Int16 dataSize = (short)(extra[j++] + extra[j++] * 256);
        j += dataSize;
      }
      System.Int32 RNTRNTRNT_306 = -1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1922uL);
      return RNTRNTRNT_306;
    }
    internal int ProcessExtraField(Stream s, Int16 extraFieldLength)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1923uL);
      int additionalBytesRead = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1924uL);
      if (extraFieldLength > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1925uL);
        byte[] buffer = this._Extra = new byte[extraFieldLength];
        additionalBytesRead = s.Read(buffer, 0, buffer.Length);
        long posn = s.Position - additionalBytesRead;
        int j = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1926uL);
        while (j + 3 < buffer.Length) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1927uL);
          int start = j;
          UInt16 headerId = (UInt16)(buffer[j++] + buffer[j++] * 256);
          Int16 dataSize = (short)(buffer[j++] + buffer[j++] * 256);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1928uL);
          switch (headerId) {
            case 0xa:
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1931uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1929uL);
                j = ProcessExtraFieldWindowsTimes(buffer, j, dataSize, posn);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1930uL);
                break;
              }

            case 0x5455:
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1934uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1932uL);
                j = ProcessExtraFieldUnixTimes(buffer, j, dataSize, posn);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1933uL);
                break;
              }

            case 0x5855:
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1937uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1935uL);
                j = ProcessExtraFieldInfoZipTimes(buffer, j, dataSize, posn);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1936uL);
                break;
              }

            case 0x7855:
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1939uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1938uL);
                break;
              }

            case 0x7875:
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1941uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1940uL);
                break;
              }

            case 0x1:
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1944uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1942uL);
                j = ProcessExtraFieldZip64(buffer, j, dataSize, posn);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1943uL);
                break;
              }

            case 0x9901:
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1947uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1945uL);
                j = ProcessExtraFieldWinZipAes(buffer, j, dataSize, posn);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1946uL);
                break;
              }

            case 0x17:
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1950uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1948uL);
                j = ProcessExtraFieldPkwareStrongEncryption(buffer, j);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1949uL);
                break;
              }

          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1951uL);
          j = start + dataSize + 4;
        }
      }
      System.Int32 RNTRNTRNT_307 = additionalBytesRead;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1952uL);
      return RNTRNTRNT_307;
    }
    private int ProcessExtraFieldPkwareStrongEncryption(byte[] Buffer, int j)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1953uL);
      j += 2;
      _UnsupportedAlgorithmId = (UInt16)(Buffer[j++] + Buffer[j++] * 256);
      _Encryption_FromZipFile = _Encryption = EncryptionAlgorithm.Unsupported;
      System.Int32 RNTRNTRNT_308 = j;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1954uL);
      return RNTRNTRNT_308;
    }
    private int ProcessExtraFieldWinZipAes(byte[] buffer, int j, Int16 dataSize, long posn)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1955uL);
      if (this._CompressionMethod == 0x63) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1956uL);
        if ((this._BitField & 0x1) != 0x1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1957uL);
          throw new BadReadException(String.Format("  Inconsistent metadata at position 0x{0:X16}", posn));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1958uL);
        this._sourceIsEncrypted = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1959uL);
        if (dataSize != 7) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1960uL);
          throw new BadReadException(String.Format("  Inconsistent size (0x{0:X4}) in WinZip AES field at position 0x{1:X16}", dataSize, posn));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1961uL);
        this._WinZipAesMethod = BitConverter.ToInt16(buffer, j);
        j += 2;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1962uL);
        if (this._WinZipAesMethod != 0x1 && this._WinZipAesMethod != 0x2) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1963uL);
          throw new BadReadException(String.Format("  Unexpected vendor version number (0x{0:X4}) for WinZip AES metadata at position 0x{1:X16}", this._WinZipAesMethod, posn));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1964uL);
        Int16 vendorId = BitConverter.ToInt16(buffer, j);
        j += 2;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1965uL);
        if (vendorId != 0x4541) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1966uL);
          throw new BadReadException(String.Format("  Unexpected vendor ID (0x{0:X4}) for WinZip AES metadata at position 0x{1:X16}", vendorId, posn));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1967uL);
        int keystrength = (buffer[j] == 1) ? 128 : (buffer[j] == 3) ? 256 : -1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1968uL);
        if (keystrength < 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1969uL);
          throw new BadReadException(String.Format("Invalid key strength ({0})", keystrength));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1970uL);
        _Encryption_FromZipFile = this._Encryption = (keystrength == 128) ? EncryptionAlgorithm.WinZipAes128 : EncryptionAlgorithm.WinZipAes256;
        j++;
        this._CompressionMethod_FromZipFile = this._CompressionMethod = BitConverter.ToInt16(buffer, j);
        j += 2;
      }
      System.Int32 RNTRNTRNT_309 = j;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1971uL);
      return RNTRNTRNT_309;
    }
    private delegate T Func<T>();
    private int ProcessExtraFieldZip64(byte[] buffer, int j, Int16 dataSize, long posn)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1972uL);
      this._InputUsesZip64 = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1973uL);
      if (dataSize > 28) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1974uL);
        throw new BadReadException(String.Format("  Inconsistent size (0x{0:X4}) for ZIP64 extra field at position 0x{1:X16}", dataSize, posn));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1975uL);
      int remainingData = dataSize;
      var slurp = new Func<Int64>(() =>
      {
        if (remainingData < 8)
          throw new BadReadException(String.Format("  Missing data for ZIP64 extra field, position 0x{0:X16}", posn));
        var x = BitConverter.ToInt64(buffer, j);
        j += 8;
        remainingData -= 8;
        return x;
      });
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1976uL);
      if (this._UncompressedSize == 0xffffffffu) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1977uL);
        this._UncompressedSize = slurp();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1978uL);
      if (this._CompressedSize == 0xffffffffu) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1979uL);
        this._CompressedSize = slurp();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1980uL);
      if (this._RelativeOffsetOfLocalHeader == 0xffffffffu) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1981uL);
        this._RelativeOffsetOfLocalHeader = slurp();
      }
      System.Int32 RNTRNTRNT_310 = j;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1982uL);
      return RNTRNTRNT_310;
    }
    private int ProcessExtraFieldInfoZipTimes(byte[] buffer, int j, Int16 dataSize, long posn)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1983uL);
      if (dataSize != 12 && dataSize != 8) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1984uL);
        throw new BadReadException(String.Format("  Unexpected size (0x{0:X4}) for InfoZip v1 extra field at position 0x{1:X16}", dataSize, posn));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1985uL);
      Int32 timet = BitConverter.ToInt32(buffer, j);
      this._Mtime = _unixEpoch.AddSeconds(timet);
      j += 4;
      timet = BitConverter.ToInt32(buffer, j);
      this._Atime = _unixEpoch.AddSeconds(timet);
      j += 4;
      this._Ctime = DateTime.UtcNow;
      _ntfsTimesAreSet = true;
      _timestamp |= ZipEntryTimestamp.InfoZip1;
      System.Int32 RNTRNTRNT_311 = j;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1986uL);
      return RNTRNTRNT_311;
    }
    private int ProcessExtraFieldUnixTimes(byte[] buffer, int j, Int16 dataSize, long posn)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1987uL);
      if (dataSize != 13 && dataSize != 9 && dataSize != 5) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1988uL);
        throw new BadReadException(String.Format("  Unexpected size (0x{0:X4}) for Extended Timestamp extra field at position 0x{1:X16}", dataSize, posn));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1989uL);
      int remainingData = dataSize;
      var slurp = new Func<DateTime>(() =>
      {
        Int32 timet = BitConverter.ToInt32(buffer, j);
        j += 4;
        remainingData -= 4;
        return _unixEpoch.AddSeconds(timet);
      });
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1990uL);
      if (dataSize == 13 || _readExtraDepth > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1991uL);
        byte flag = buffer[j++];
        remainingData--;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1992uL);
        if ((flag & 0x1) != 0 && remainingData >= 4) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1993uL);
          this._Mtime = slurp();
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1994uL);
        this._Atime = ((flag & 0x2) != 0 && remainingData >= 4) ? slurp() : DateTime.UtcNow;
        this._Ctime = ((flag & 0x4) != 0 && remainingData >= 4) ? slurp() : DateTime.UtcNow;
        _timestamp |= ZipEntryTimestamp.Unix;
        _ntfsTimesAreSet = true;
        _emitUnixTimes = true;
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1995uL);
        ReadExtraField();
      }
      System.Int32 RNTRNTRNT_312 = j;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1996uL);
      return RNTRNTRNT_312;
    }
    private int ProcessExtraFieldWindowsTimes(byte[] buffer, int j, Int16 dataSize, long posn)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1997uL);
      if (dataSize != 32) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1998uL);
        throw new BadReadException(String.Format("  Unexpected size (0x{0:X4}) for NTFS times extra field at position 0x{1:X16}", dataSize, posn));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1999uL);
      j += 4;
      Int16 timetag = (Int16)(buffer[j] + buffer[j + 1] * 256);
      Int16 addlsize = (Int16)(buffer[j + 2] + buffer[j + 3] * 256);
      j += 4;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2000uL);
      if (timetag == 0x1 && addlsize == 24) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2001uL);
        Int64 z = BitConverter.ToInt64(buffer, j);
        this._Mtime = DateTime.FromFileTimeUtc(z);
        j += 8;
        z = BitConverter.ToInt64(buffer, j);
        this._Atime = DateTime.FromFileTimeUtc(z);
        j += 8;
        z = BitConverter.ToInt64(buffer, j);
        this._Ctime = DateTime.FromFileTimeUtc(z);
        j += 8;
        _ntfsTimesAreSet = true;
        _timestamp |= ZipEntryTimestamp.Windows;
        _emitNtfsTimes = true;
      }
      System.Int32 RNTRNTRNT_313 = j;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2002uL);
      return RNTRNTRNT_313;
    }
  }
}
