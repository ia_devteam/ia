using System;
using System.Collections.Generic;
using System.Text;
namespace Ionic.Zip
{
  public delegate void WriteDelegate(string entryName, System.IO.Stream stream);
  public delegate System.IO.Stream OpenDelegate(string entryName);
  public delegate void CloseDelegate(string entryName, System.IO.Stream stream);
  public delegate Ionic.Zlib.CompressionLevel SetCompressionCallback(string localFileName, string fileNameInArchive);
  public enum ZipProgressEventType
  {
    Adding_Started,
    Adding_AfterAddEntry,
    Adding_Completed,
    Reading_Started,
    Reading_BeforeReadEntry,
    Reading_AfterReadEntry,
    Reading_Completed,
    Reading_ArchiveBytesRead,
    Saving_Started,
    Saving_BeforeWriteEntry,
    Saving_AfterWriteEntry,
    Saving_Completed,
    Saving_AfterSaveTempArchive,
    Saving_BeforeRenameTempArchive,
    Saving_AfterRenameTempArchive,
    Saving_AfterCompileSelfExtractor,
    Saving_EntryBytesRead,
    Extracting_BeforeExtractEntry,
    Extracting_AfterExtractEntry,
    Extracting_ExtractEntryWouldOverwrite,
    Extracting_EntryBytesWritten,
    Extracting_BeforeExtractAll,
    Extracting_AfterExtractAll,
    Error_Saving
  }
  public class ZipProgressEventArgs : EventArgs
  {
    private int _entriesTotal;
    private bool _cancel;
    private ZipEntry _latestEntry;
    private ZipProgressEventType _flavor;
    private String _archiveName;
    private Int64 _bytesTransferred;
    private Int64 _totalBytesToTransfer;
    internal ZipProgressEventArgs()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1191uL);
    }
    internal ZipProgressEventArgs(string archiveName, ZipProgressEventType flavor)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1192uL);
      this._archiveName = archiveName;
      this._flavor = flavor;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1193uL);
    }
    public int EntriesTotal {
      get {
        System.Int32 RNTRNTRNT_192 = _entriesTotal;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1194uL);
        return RNTRNTRNT_192;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1195uL);
        _entriesTotal = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1196uL);
      }
    }
    public ZipEntry CurrentEntry {
      get {
        ZipEntry RNTRNTRNT_193 = _latestEntry;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1197uL);
        return RNTRNTRNT_193;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1198uL);
        _latestEntry = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1199uL);
      }
    }
    public bool Cancel {
      get {
        System.Boolean RNTRNTRNT_194 = _cancel;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1200uL);
        return RNTRNTRNT_194;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1201uL);
        _cancel = _cancel || value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1202uL);
      }
    }
    public ZipProgressEventType EventType {
      get {
        ZipProgressEventType RNTRNTRNT_195 = _flavor;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1203uL);
        return RNTRNTRNT_195;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1204uL);
        _flavor = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1205uL);
      }
    }
    public String ArchiveName {
      get {
        String RNTRNTRNT_196 = _archiveName;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1206uL);
        return RNTRNTRNT_196;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1207uL);
        _archiveName = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1208uL);
      }
    }
    public Int64 BytesTransferred {
      get {
        Int64 RNTRNTRNT_197 = _bytesTransferred;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1209uL);
        return RNTRNTRNT_197;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1210uL);
        _bytesTransferred = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1211uL);
      }
    }
    public Int64 TotalBytesToTransfer {
      get {
        Int64 RNTRNTRNT_198 = _totalBytesToTransfer;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1212uL);
        return RNTRNTRNT_198;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1213uL);
        _totalBytesToTransfer = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1214uL);
      }
    }
  }
  public class ReadProgressEventArgs : ZipProgressEventArgs
  {
    internal ReadProgressEventArgs()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1215uL);
    }
    private ReadProgressEventArgs(string archiveName, ZipProgressEventType flavor) : base(archiveName, flavor)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1216uL);
    }
    static internal ReadProgressEventArgs Before(string archiveName, int entriesTotal)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1217uL);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_BeforeReadEntry);
      x.EntriesTotal = entriesTotal;
      ReadProgressEventArgs RNTRNTRNT_199 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1218uL);
      return RNTRNTRNT_199;
    }
    static internal ReadProgressEventArgs After(string archiveName, ZipEntry entry, int entriesTotal)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1219uL);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_AfterReadEntry);
      x.EntriesTotal = entriesTotal;
      x.CurrentEntry = entry;
      ReadProgressEventArgs RNTRNTRNT_200 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1220uL);
      return RNTRNTRNT_200;
    }
    static internal ReadProgressEventArgs Started(string archiveName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1221uL);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_Started);
      ReadProgressEventArgs RNTRNTRNT_201 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1222uL);
      return RNTRNTRNT_201;
    }
    static internal ReadProgressEventArgs ByteUpdate(string archiveName, ZipEntry entry, Int64 bytesXferred, Int64 totalBytes)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1223uL);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_ArchiveBytesRead);
      x.CurrentEntry = entry;
      x.BytesTransferred = bytesXferred;
      x.TotalBytesToTransfer = totalBytes;
      ReadProgressEventArgs RNTRNTRNT_202 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1224uL);
      return RNTRNTRNT_202;
    }
    static internal ReadProgressEventArgs Completed(string archiveName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1225uL);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_Completed);
      ReadProgressEventArgs RNTRNTRNT_203 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1226uL);
      return RNTRNTRNT_203;
    }
  }
  public class AddProgressEventArgs : ZipProgressEventArgs
  {
    internal AddProgressEventArgs()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1227uL);
    }
    private AddProgressEventArgs(string archiveName, ZipProgressEventType flavor) : base(archiveName, flavor)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1228uL);
    }
    static internal AddProgressEventArgs AfterEntry(string archiveName, ZipEntry entry, int entriesTotal)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1229uL);
      var x = new AddProgressEventArgs(archiveName, ZipProgressEventType.Adding_AfterAddEntry);
      x.EntriesTotal = entriesTotal;
      x.CurrentEntry = entry;
      AddProgressEventArgs RNTRNTRNT_204 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1230uL);
      return RNTRNTRNT_204;
    }
    static internal AddProgressEventArgs Started(string archiveName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1231uL);
      var x = new AddProgressEventArgs(archiveName, ZipProgressEventType.Adding_Started);
      AddProgressEventArgs RNTRNTRNT_205 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1232uL);
      return RNTRNTRNT_205;
    }
    static internal AddProgressEventArgs Completed(string archiveName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1233uL);
      var x = new AddProgressEventArgs(archiveName, ZipProgressEventType.Adding_Completed);
      AddProgressEventArgs RNTRNTRNT_206 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1234uL);
      return RNTRNTRNT_206;
    }
  }
  public class SaveProgressEventArgs : ZipProgressEventArgs
  {
    private int _entriesSaved;
    internal SaveProgressEventArgs(string archiveName, bool before, int entriesTotal, int entriesSaved, ZipEntry entry) : base(archiveName, (before) ? ZipProgressEventType.Saving_BeforeWriteEntry : ZipProgressEventType.Saving_AfterWriteEntry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1235uL);
      this.EntriesTotal = entriesTotal;
      this.CurrentEntry = entry;
      this._entriesSaved = entriesSaved;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1236uL);
    }
    internal SaveProgressEventArgs()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1237uL);
    }
    internal SaveProgressEventArgs(string archiveName, ZipProgressEventType flavor) : base(archiveName, flavor)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1238uL);
    }
    static internal SaveProgressEventArgs ByteUpdate(string archiveName, ZipEntry entry, Int64 bytesXferred, Int64 totalBytes)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1239uL);
      var x = new SaveProgressEventArgs(archiveName, ZipProgressEventType.Saving_EntryBytesRead);
      x.ArchiveName = archiveName;
      x.CurrentEntry = entry;
      x.BytesTransferred = bytesXferred;
      x.TotalBytesToTransfer = totalBytes;
      SaveProgressEventArgs RNTRNTRNT_207 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1240uL);
      return RNTRNTRNT_207;
    }
    static internal SaveProgressEventArgs Started(string archiveName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1241uL);
      var x = new SaveProgressEventArgs(archiveName, ZipProgressEventType.Saving_Started);
      SaveProgressEventArgs RNTRNTRNT_208 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1242uL);
      return RNTRNTRNT_208;
    }
    static internal SaveProgressEventArgs Completed(string archiveName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1243uL);
      var x = new SaveProgressEventArgs(archiveName, ZipProgressEventType.Saving_Completed);
      SaveProgressEventArgs RNTRNTRNT_209 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1244uL);
      return RNTRNTRNT_209;
    }
    public int EntriesSaved {
      get {
        System.Int32 RNTRNTRNT_210 = _entriesSaved;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1245uL);
        return RNTRNTRNT_210;
      }
    }
  }
  public class ExtractProgressEventArgs : ZipProgressEventArgs
  {
    private int _entriesExtracted;
    private string _target;
    internal ExtractProgressEventArgs(string archiveName, bool before, int entriesTotal, int entriesExtracted, ZipEntry entry, string extractLocation) : base(archiveName, (before) ? ZipProgressEventType.Extracting_BeforeExtractEntry : ZipProgressEventType.Extracting_AfterExtractEntry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1246uL);
      this.EntriesTotal = entriesTotal;
      this.CurrentEntry = entry;
      this._entriesExtracted = entriesExtracted;
      this._target = extractLocation;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1247uL);
    }
    internal ExtractProgressEventArgs(string archiveName, ZipProgressEventType flavor) : base(archiveName, flavor)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1248uL);
    }
    internal ExtractProgressEventArgs()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1249uL);
    }
    static internal ExtractProgressEventArgs BeforeExtractEntry(string archiveName, ZipEntry entry, string extractLocation)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1250uL);
      var x = new ExtractProgressEventArgs {
        ArchiveName = archiveName,
        EventType = ZipProgressEventType.Extracting_BeforeExtractEntry,
        CurrentEntry = entry,
        _target = extractLocation
      };
      ExtractProgressEventArgs RNTRNTRNT_211 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1251uL);
      return RNTRNTRNT_211;
    }
    static internal ExtractProgressEventArgs ExtractExisting(string archiveName, ZipEntry entry, string extractLocation)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1252uL);
      var x = new ExtractProgressEventArgs {
        ArchiveName = archiveName,
        EventType = ZipProgressEventType.Extracting_ExtractEntryWouldOverwrite,
        CurrentEntry = entry,
        _target = extractLocation
      };
      ExtractProgressEventArgs RNTRNTRNT_212 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1253uL);
      return RNTRNTRNT_212;
    }
    static internal ExtractProgressEventArgs AfterExtractEntry(string archiveName, ZipEntry entry, string extractLocation)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1254uL);
      var x = new ExtractProgressEventArgs {
        ArchiveName = archiveName,
        EventType = ZipProgressEventType.Extracting_AfterExtractEntry,
        CurrentEntry = entry,
        _target = extractLocation
      };
      ExtractProgressEventArgs RNTRNTRNT_213 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1255uL);
      return RNTRNTRNT_213;
    }
    static internal ExtractProgressEventArgs ExtractAllStarted(string archiveName, string extractLocation)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1256uL);
      var x = new ExtractProgressEventArgs(archiveName, ZipProgressEventType.Extracting_BeforeExtractAll);
      x._target = extractLocation;
      ExtractProgressEventArgs RNTRNTRNT_214 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1257uL);
      return RNTRNTRNT_214;
    }
    static internal ExtractProgressEventArgs ExtractAllCompleted(string archiveName, string extractLocation)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1258uL);
      var x = new ExtractProgressEventArgs(archiveName, ZipProgressEventType.Extracting_AfterExtractAll);
      x._target = extractLocation;
      ExtractProgressEventArgs RNTRNTRNT_215 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1259uL);
      return RNTRNTRNT_215;
    }
    static internal ExtractProgressEventArgs ByteUpdate(string archiveName, ZipEntry entry, Int64 bytesWritten, Int64 totalBytes)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1260uL);
      var x = new ExtractProgressEventArgs(archiveName, ZipProgressEventType.Extracting_EntryBytesWritten);
      x.ArchiveName = archiveName;
      x.CurrentEntry = entry;
      x.BytesTransferred = bytesWritten;
      x.TotalBytesToTransfer = totalBytes;
      ExtractProgressEventArgs RNTRNTRNT_216 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1261uL);
      return RNTRNTRNT_216;
    }
    public int EntriesExtracted {
      get {
        System.Int32 RNTRNTRNT_217 = _entriesExtracted;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1262uL);
        return RNTRNTRNT_217;
      }
    }
    public String ExtractLocation {
      get {
        String RNTRNTRNT_218 = _target;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1263uL);
        return RNTRNTRNT_218;
      }
    }
  }
  public class ZipErrorEventArgs : ZipProgressEventArgs
  {
    private Exception _exc;
    private ZipErrorEventArgs()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1264uL);
    }
    static internal ZipErrorEventArgs Saving(string archiveName, ZipEntry entry, Exception exception)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1265uL);
      var x = new ZipErrorEventArgs {
        EventType = ZipProgressEventType.Error_Saving,
        ArchiveName = archiveName,
        CurrentEntry = entry,
        _exc = exception
      };
      ZipErrorEventArgs RNTRNTRNT_219 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1266uL);
      return RNTRNTRNT_219;
    }
    public Exception Exception {
      get {
        Exception RNTRNTRNT_220 = _exc;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1267uL);
        return RNTRNTRNT_220;
      }
    }
    public String FileName {
      get {
        String RNTRNTRNT_221 = CurrentEntry.LocalFileName;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1268uL);
        return RNTRNTRNT_221;
      }
    }
  }
}
