using System;
using System.Reflection;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public enum SelfExtractorFlavor
  {
    ConsoleApplication = 0,
    WinFormsApplication
  }
  public class SelfExtractorSaveOptions
  {
    public SelfExtractorFlavor Flavor { get; set; }
    public String PostExtractCommandLine { get; set; }
    public String DefaultExtractDirectory { get; set; }
    public string IconFile { get; set; }
    public bool Quiet { get; set; }
    public Ionic.Zip.ExtractExistingFileAction ExtractExistingFile { get; set; }
    public bool RemoveUnpackedFilesAfterExecute { get; set; }
    public Version FileVersion { get; set; }
    public String ProductVersion { get; set; }
    public String Copyright { get; set; }
    public String Description { get; set; }
    public String ProductName { get; set; }
    public String SfxExeWindowTitle { get; set; }
    public string AdditionalCompilerSwitches { get; set; }
  }
  partial class ZipFile
  {
    class ExtractorSettings
    {
      public SelfExtractorFlavor Flavor;
      public List<string> ReferencedAssemblies;
      public List<string> CopyThroughResources;
      public List<string> ResourcesToCompile;
    }
    private static ExtractorSettings[] SettingsList = {
      new ExtractorSettings {
        Flavor = SelfExtractorFlavor.WinFormsApplication,
        ReferencedAssemblies = new List<string> {
          "System.dll",
          "System.Windows.Forms.dll",
          "System.Drawing.dll"
        },
        CopyThroughResources = new List<string> {
          "Ionic.Zip.WinFormsSelfExtractorStub.resources",
          "Ionic.Zip.Forms.PasswordDialog.resources",
          "Ionic.Zip.Forms.ZipContentsDialog.resources"
        },
        ResourcesToCompile = new List<string> {
          "WinFormsSelfExtractorStub.cs",
          "WinFormsSelfExtractorStub.Designer.cs",
          "PasswordDialog.cs",
          "PasswordDialog.Designer.cs",
          "ZipContentsDialog.cs",
          "ZipContentsDialog.Designer.cs",
          "FolderBrowserDialogEx.cs"
        }
      },
      new ExtractorSettings {
        Flavor = SelfExtractorFlavor.ConsoleApplication,
        ReferencedAssemblies = new List<string> { "System.dll" },
        CopyThroughResources = null,
        ResourcesToCompile = new List<string> { "CommandLineSelfExtractorStub.cs" }
      }
    };
    public void SaveSelfExtractor(string exeToGenerate, SelfExtractorFlavor flavor)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3476uL);
      SelfExtractorSaveOptions options = new SelfExtractorSaveOptions();
      options.Flavor = flavor;
      SaveSelfExtractor(exeToGenerate, options);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3477uL);
    }
    public void SaveSelfExtractor(string exeToGenerate, SelfExtractorSaveOptions options)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3478uL);
      if (_name == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3479uL);
        _writestream = null;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3480uL);
      _SavingSfx = true;
      _name = exeToGenerate;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3481uL);
      if (Directory.Exists(_name)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3482uL);
        throw new ZipException("Bad Directory", new System.ArgumentException("That name specifies an existing directory. Please specify a filename.", "exeToGenerate"));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3483uL);
      _contentsChanged = true;
      _fileAlreadyExists = File.Exists(_name);
      _SaveSfxStub(exeToGenerate, options);
      Save();
      _SavingSfx = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3484uL);
    }
    private static void ExtractResourceToFile(Assembly a, string resourceName, string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3485uL);
      int n = 0;
      byte[] bytes = new byte[1024];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3486uL);
      using (Stream instream = a.GetManifestResourceStream(resourceName)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3487uL);
        if (instream == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3488uL);
          throw new ZipException(String.Format("missing resource '{0}'", resourceName));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3489uL);
        using (FileStream outstream = File.OpenWrite(filename)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3490uL);
          do {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3491uL);
            n = instream.Read(bytes, 0, bytes.Length);
            outstream.Write(bytes, 0, n);
          } while (n > 0);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3492uL);
    }
    private void _SaveSfxStub(string exeToGenerate, SelfExtractorSaveOptions options)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3493uL);
      string nameOfIconFile = null;
      string stubExe = null;
      string unpackedResourceDir = null;
      string tmpDir = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3494uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3495uL);
        if (File.Exists(exeToGenerate)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3496uL);
          if (Verbose) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3497uL);
            StatusMessageTextWriter.WriteLine("The existing file ({0}) will be overwritten.", exeToGenerate);
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3498uL);
        if (!exeToGenerate.EndsWith(".exe")) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3499uL);
          if (Verbose) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3500uL);
            StatusMessageTextWriter.WriteLine("Warning: The generated self-extracting file will not have an .exe extension.");
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3501uL);
        tmpDir = TempFileFolder ?? Path.GetDirectoryName(exeToGenerate);
        stubExe = GenerateTempPathname(tmpDir, "exe");
        Assembly a1 = typeof(ZipFile).Assembly;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3502uL);
        using (var csharp = new Microsoft.CSharp.CSharpCodeProvider()) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3503uL);
          ExtractorSettings settings = null;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3504uL);
          foreach (var x in SettingsList) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3505uL);
            if (x.Flavor == options.Flavor) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3506uL);
              settings = x;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3507uL);
              break;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3508uL);
          if (settings == null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3509uL);
            throw new BadStateException(String.Format("While saving a Self-Extracting Zip, Cannot find that flavor ({0})?", options.Flavor));
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3510uL);
          var cp = new System.CodeDom.Compiler.CompilerParameters();
          cp.ReferencedAssemblies.Add(a1.Location);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3511uL);
          if (settings.ReferencedAssemblies != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3512uL);
            foreach (string ra in settings.ReferencedAssemblies) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3513uL);
              cp.ReferencedAssemblies.Add(ra);
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3514uL);
          cp.GenerateInMemory = false;
          cp.GenerateExecutable = true;
          cp.IncludeDebugInformation = false;
          cp.CompilerOptions = "";
          Assembly a2 = Assembly.GetExecutingAssembly();
          var sb = new System.Text.StringBuilder();
          string sourceFile = GenerateTempPathname(tmpDir, "cs");
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3515uL);
          using (ZipFile zip = ZipFile.Read(a2.GetManifestResourceStream("Ionic.Zip.Resources.ZippedResources.zip"))) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3516uL);
            unpackedResourceDir = GenerateTempPathname(tmpDir, "tmp");
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3517uL);
            if (String.IsNullOrEmpty(options.IconFile)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3518uL);
              System.IO.Directory.CreateDirectory(unpackedResourceDir);
              ZipEntry e = zip["zippedFile.ico"];
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3519uL);
              if ((e.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3520uL);
                e.Attributes ^= FileAttributes.ReadOnly;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3521uL);
              e.Extract(unpackedResourceDir);
              nameOfIconFile = Path.Combine(unpackedResourceDir, "zippedFile.ico");
              cp.CompilerOptions += String.Format("/win32icon:\"{0}\"", nameOfIconFile);
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3522uL);
              cp.CompilerOptions += String.Format("/win32icon:\"{0}\"", options.IconFile);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3523uL);
            cp.OutputAssembly = stubExe;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3524uL);
            if (options.Flavor == SelfExtractorFlavor.WinFormsApplication) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3525uL);
              cp.CompilerOptions += " /target:winexe";
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3526uL);
            if (!String.IsNullOrEmpty(options.AdditionalCompilerSwitches)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3527uL);
              cp.CompilerOptions += " " + options.AdditionalCompilerSwitches;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3528uL);
            if (String.IsNullOrEmpty(cp.CompilerOptions)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3529uL);
              cp.CompilerOptions = null;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3530uL);
            if ((settings.CopyThroughResources != null) && (settings.CopyThroughResources.Count != 0)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3531uL);
              if (!Directory.Exists(unpackedResourceDir)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3532uL);
                System.IO.Directory.CreateDirectory(unpackedResourceDir);
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3533uL);
              foreach (string re in settings.CopyThroughResources) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3534uL);
                string filename = Path.Combine(unpackedResourceDir, re);
                ExtractResourceToFile(a2, re, filename);
                cp.EmbeddedResources.Add(filename);
              }
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3535uL);
            cp.EmbeddedResources.Add(a1.Location);
            sb.Append("// " + Path.GetFileName(sourceFile) + "\n").Append("// --------------------------------------------\n//\n").Append("// This SFX source file was generated by DotNetZip ").Append(ZipFile.LibraryVersion.ToString()).Append("\n//         at ").Append(System.DateTime.Now.ToString("yyyy MMMM dd  HH:mm:ss")).Append("\n//\n// --------------------------------------------\n\n\n");
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3536uL);
            if (!String.IsNullOrEmpty(options.Description)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3537uL);
              sb.Append("[assembly: System.Reflection.AssemblyTitle(\"" + options.Description.Replace("\"", "") + "\")]\n");
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3538uL);
              sb.Append("[assembly: System.Reflection.AssemblyTitle(\"DotNetZip SFX Archive\")]\n");
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3539uL);
            if (!String.IsNullOrEmpty(options.ProductVersion)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3540uL);
              sb.Append("[assembly: System.Reflection.AssemblyInformationalVersion(\"" + options.ProductVersion.Replace("\"", "") + "\")]\n");
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3541uL);
            string copyright = (String.IsNullOrEmpty(options.Copyright)) ? "Extractor: Copyright � Dino Chiesa 2008-2011" : options.Copyright.Replace("\"", "");
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3542uL);
            if (!String.IsNullOrEmpty(options.ProductName)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3543uL);
              sb.Append("[assembly: System.Reflection.AssemblyProduct(\"").Append(options.ProductName.Replace("\"", "")).Append("\")]\n");
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3544uL);
              sb.Append("[assembly: System.Reflection.AssemblyProduct(\"DotNetZip\")]\n");
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3545uL);
            sb.Append("[assembly: System.Reflection.AssemblyCopyright(\"" + copyright + "\")]\n").Append(String.Format("[assembly: System.Reflection.AssemblyVersion(\"{0}\")]\n", ZipFile.LibraryVersion.ToString()));
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3546uL);
            if (options.FileVersion != null) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3547uL);
              sb.Append(String.Format("[assembly: System.Reflection.AssemblyFileVersion(\"{0}\")]\n", options.FileVersion.ToString()));
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3548uL);
            sb.Append("\n\n\n");
            string extractLoc = options.DefaultExtractDirectory;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3549uL);
            if (extractLoc != null) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3550uL);
              extractLoc = extractLoc.Replace("\"", "").Replace("\\", "\\\\");
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3551uL);
            string postExCmdLine = options.PostExtractCommandLine;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3552uL);
            if (postExCmdLine != null) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3553uL);
              postExCmdLine = postExCmdLine.Replace("\\", "\\\\");
              postExCmdLine = postExCmdLine.Replace("\"", "\\\"");
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3554uL);
            foreach (string rc in settings.ResourcesToCompile) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3555uL);
              using (Stream s = zip[rc].OpenReader()) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3556uL);
                if (s == null) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3557uL);
                  throw new ZipException(String.Format("missing resource '{0}'", rc));
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3558uL);
                using (StreamReader sr = new StreamReader(s)) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3559uL);
                  while (sr.Peek() >= 0) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3560uL);
                    string line = sr.ReadLine();
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3561uL);
                    if (extractLoc != null) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3562uL);
                      line = line.Replace("@@EXTRACTLOCATION", extractLoc);
                    }
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3563uL);
                    line = line.Replace("@@REMOVE_AFTER_EXECUTE", options.RemoveUnpackedFilesAfterExecute.ToString());
                    line = line.Replace("@@QUIET", options.Quiet.ToString());
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3564uL);
                    if (!String.IsNullOrEmpty(options.SfxExeWindowTitle)) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3565uL);
                      line = line.Replace("@@SFX_EXE_WINDOW_TITLE", options.SfxExeWindowTitle);
                    }
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3566uL);
                    line = line.Replace("@@EXTRACT_EXISTING_FILE", ((int)options.ExtractExistingFile).ToString());
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3567uL);
                    if (postExCmdLine != null) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3568uL);
                      line = line.Replace("@@POST_UNPACK_CMD_LINE", postExCmdLine);
                    }
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3569uL);
                    sb.Append(line).Append("\n");
                  }
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3570uL);
                sb.Append("\n\n");
              }
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3571uL);
          string LiteralSource = sb.ToString();
          var cr = csharp.CompileAssemblyFromSource(cp, LiteralSource);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3572uL);
          if (cr == null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3573uL);
            throw new SfxGenerationException("Cannot compile the extraction logic!");
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3574uL);
          if (Verbose) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3575uL);
            foreach (string output in cr.Output) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3576uL);
              StatusMessageTextWriter.WriteLine(output);
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3577uL);
          if (cr.Errors.Count != 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3578uL);
            using (TextWriter tw = new StreamWriter(sourceFile)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3579uL);
              tw.Write(LiteralSource);
              tw.Write("\n\n\n// ------------------------------------------------------------------\n");
              tw.Write("// Errors during compilation: \n//\n");
              string p = Path.GetFileName(sourceFile);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3580uL);
              foreach (System.CodeDom.Compiler.CompilerError error in cr.Errors) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3581uL);
                tw.Write(String.Format("//   {0}({1},{2}): {3} {4}: {5}\n//\n", p, error.Line, error.Column, error.IsWarning ? "Warning" : "error", error.ErrorNumber, error.ErrorText));
              }
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3582uL);
            throw new SfxGenerationException(String.Format("Errors compiling the extraction logic!  {0}", sourceFile));
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3583uL);
          OnSaveEvent(ZipProgressEventType.Saving_AfterCompileSelfExtractor);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3584uL);
          using (System.IO.Stream input = System.IO.File.OpenRead(stubExe)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3585uL);
            byte[] buffer = new byte[4000];
            int n = 1;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3586uL);
            while (n != 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3587uL);
              n = input.Read(buffer, 0, buffer.Length);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3588uL);
              if (n != 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3589uL);
                WriteStream.Write(buffer, 0, n);
              }
            }
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3590uL);
        OnSaveEvent(ZipProgressEventType.Saving_AfterSaveTempArchive);
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3591uL);
        try {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3592uL);
          if (Directory.Exists(unpackedResourceDir)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3593uL);
            try {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3594uL);
              Directory.Delete(unpackedResourceDir, true);
            } catch (System.IO.IOException exc1) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3595uL);
              StatusMessageTextWriter.WriteLine("Warning: Exception: {0}", exc1);
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3596uL);
          if (File.Exists(stubExe)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3597uL);
            try {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3598uL);
              File.Delete(stubExe);
            } catch (System.IO.IOException exc1) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3599uL);
              StatusMessageTextWriter.WriteLine("Warning: Exception: {0}", exc1);
            }
          }
        } catch (System.IO.IOException) {
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3600uL);
      return;
    }
    static internal string GenerateTempPathname(string dir, string extension)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3601uL);
      string candidate = null;
      String AppName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3602uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3603uL);
        string uuid = System.Guid.NewGuid().ToString();
        string Name = String.Format("{0}-{1}-{2}.{3}", AppName, System.DateTime.Now.ToString("yyyyMMMdd-HHmmss"), uuid, extension);
        candidate = System.IO.Path.Combine(dir, Name);
      } while (System.IO.File.Exists(candidate) || System.IO.Directory.Exists(candidate));
      System.String RNTRNTRNT_465 = candidate;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3604uL);
      return RNTRNTRNT_465;
    }
  }
}
