using System;
using System.IO;
namespace Ionic.Zip
{
  public partial class ZipEntry
  {
    public void Extract()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1533uL);
      InternalExtract(".", null, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1534uL);
    }
    public void Extract(ExtractExistingFileAction extractExistingFile)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1535uL);
      ExtractExistingFile = extractExistingFile;
      InternalExtract(".", null, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1536uL);
    }
    public void Extract(Stream stream)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1537uL);
      InternalExtract(null, stream, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1538uL);
    }
    public void Extract(string baseDirectory)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1539uL);
      InternalExtract(baseDirectory, null, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1540uL);
    }
    public void Extract(string baseDirectory, ExtractExistingFileAction extractExistingFile)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1541uL);
      ExtractExistingFile = extractExistingFile;
      InternalExtract(baseDirectory, null, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1542uL);
    }
    public void ExtractWithPassword(string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1543uL);
      InternalExtract(".", null, password);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1544uL);
    }
    public void ExtractWithPassword(string baseDirectory, string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1545uL);
      InternalExtract(baseDirectory, null, password);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1546uL);
    }
    public void ExtractWithPassword(ExtractExistingFileAction extractExistingFile, string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1547uL);
      ExtractExistingFile = extractExistingFile;
      InternalExtract(".", null, password);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1548uL);
    }
    public void ExtractWithPassword(string baseDirectory, ExtractExistingFileAction extractExistingFile, string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1549uL);
      ExtractExistingFile = extractExistingFile;
      InternalExtract(baseDirectory, null, password);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1550uL);
    }
    public void ExtractWithPassword(Stream stream, string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1551uL);
      InternalExtract(null, stream, password);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1552uL);
    }
    public Ionic.Crc.CrcCalculatorStream OpenReader()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1553uL);
      if (_container.ZipFile == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1554uL);
        throw new InvalidOperationException("Use OpenReader() only with ZipFile.");
      }
      Ionic.Crc.CrcCalculatorStream RNTRNTRNT_279 = InternalOpenReader(this._Password ?? this._container.Password);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1555uL);
      return RNTRNTRNT_279;
    }
    public Ionic.Crc.CrcCalculatorStream OpenReader(string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1556uL);
      if (_container.ZipFile == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1557uL);
        throw new InvalidOperationException("Use OpenReader() only with ZipFile.");
      }
      Ionic.Crc.CrcCalculatorStream RNTRNTRNT_280 = InternalOpenReader(password);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1558uL);
      return RNTRNTRNT_280;
    }
    internal Ionic.Crc.CrcCalculatorStream InternalOpenReader(string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1559uL);
      ValidateCompression();
      ValidateEncryption();
      SetupCryptoForExtract(password);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1560uL);
      if (this._Source != ZipEntrySource.ZipFile) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1561uL);
        throw new BadStateException("You must call ZipFile.Save before calling OpenReader");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1562uL);
      Int64 LeftToRead = (_CompressionMethod_FromZipFile == (short)CompressionMethod.None) ? this._CompressedFileDataSize : this.UncompressedSize;
      Stream input = this.ArchiveStream;
      this.ArchiveStream.Seek(this.FileDataPosition, SeekOrigin.Begin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      _inputDecryptorStream = GetExtractDecryptor(input);
      Stream input3 = GetExtractDecompressor(_inputDecryptorStream);
      Ionic.Crc.CrcCalculatorStream RNTRNTRNT_281 = new Ionic.Crc.CrcCalculatorStream(input3, LeftToRead);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1563uL);
      return RNTRNTRNT_281;
    }
    private void OnExtractProgress(Int64 bytesWritten, Int64 totalBytesToWrite)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1564uL);
      if (_container.ZipFile != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1565uL);
        _ioOperationCanceled = _container.ZipFile.OnExtractBlock(this, bytesWritten, totalBytesToWrite);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1566uL);
    }
    private void OnBeforeExtract(string path)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1567uL);
      if (_container.ZipFile != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1568uL);
        if (!_container.ZipFile._inExtractAll) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1569uL);
          _ioOperationCanceled = _container.ZipFile.OnSingleEntryExtract(this, path, true);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1570uL);
    }
    private void OnAfterExtract(string path)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1571uL);
      if (_container.ZipFile != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1572uL);
        if (!_container.ZipFile._inExtractAll) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1573uL);
          _container.ZipFile.OnSingleEntryExtract(this, path, false);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1574uL);
    }
    private void OnExtractExisting(string path)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1575uL);
      if (_container.ZipFile != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1576uL);
        _ioOperationCanceled = _container.ZipFile.OnExtractExisting(this, path);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1577uL);
    }
    private static void ReallyDelete(string fileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1578uL);
      if ((File.GetAttributes(fileName) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1579uL);
        File.SetAttributes(fileName, FileAttributes.Normal);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1580uL);
      File.Delete(fileName);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1581uL);
    }
    private void WriteStatus(string format, params Object[] args)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1582uL);
      if (_container.ZipFile != null && _container.ZipFile.Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1583uL);
        _container.ZipFile.StatusMessageTextWriter.WriteLine(format, args);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1584uL);
    }
    private void InternalExtract(string baseDir, Stream outstream, string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1585uL);
      if (_container == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1586uL);
        throw new BadStateException("This entry is an orphan");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1587uL);
      if (_container.ZipFile == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1588uL);
        throw new InvalidOperationException("Use Extract() only with ZipFile.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1589uL);
      _container.ZipFile.Reset(false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1590uL);
      if (this._Source != ZipEntrySource.ZipFile) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1591uL);
        throw new BadStateException("You must call ZipFile.Save before calling any Extract method");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1592uL);
      OnBeforeExtract(baseDir);
      _ioOperationCanceled = false;
      string targetFileName = null;
      Stream output = null;
      bool fileExistsBeforeExtraction = false;
      bool checkLaterForResetDirTimes = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1593uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1594uL);
        ValidateCompression();
        ValidateEncryption();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1595uL);
        if (ValidateOutput(baseDir, outstream, out targetFileName)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1596uL);
          WriteStatus("extract dir {0}...", targetFileName);
          OnAfterExtract(baseDir);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1597uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1598uL);
        if (targetFileName != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1599uL);
          if (File.Exists(targetFileName)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1600uL);
            fileExistsBeforeExtraction = true;
            int rc = CheckExtractExistingFile(baseDir, targetFileName);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1601uL);
            if (rc == 2) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1602uL);
              goto ExitTry;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1603uL);
            if (rc == 1) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1604uL);
              return;
            }
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1605uL);
        string p = password ?? this._Password ?? this._container.Password;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1606uL);
        if (_Encryption_FromZipFile != EncryptionAlgorithm.None) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1607uL);
          if (p == null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1608uL);
            throw new BadPasswordException();
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1609uL);
          SetupCryptoForExtract(p);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1610uL);
        if (targetFileName != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1611uL);
          WriteStatus("extract file {0}...", targetFileName);
          targetFileName += ".tmp";
          var dirName = Path.GetDirectoryName(targetFileName);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1612uL);
          if (!Directory.Exists(dirName)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1613uL);
            Directory.CreateDirectory(dirName);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1614uL);
            if (_container.ZipFile != null) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1615uL);
              checkLaterForResetDirTimes = _container.ZipFile._inExtractAll;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1616uL);
          output = new FileStream(targetFileName, FileMode.CreateNew);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1617uL);
          WriteStatus("extract entry {0} to stream...", FileName);
          output = outstream;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1618uL);
        if (_ioOperationCanceled) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1619uL);
          goto ExitTry;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1620uL);
        Int32 ActualCrc32 = ExtractOne(output);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1621uL);
        if (_ioOperationCanceled) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1622uL);
          goto ExitTry;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1623uL);
        VerifyCrcAfterExtract(ActualCrc32);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1624uL);
        if (targetFileName != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1625uL);
          output.Close();
          output = null;
          string tmpName = targetFileName;
          string zombie = null;
          targetFileName = tmpName.Substring(0, tmpName.Length - 4);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1626uL);
          if (fileExistsBeforeExtraction) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1627uL);
            zombie = targetFileName + ".PendingOverwrite";
            File.Move(targetFileName, zombie);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1628uL);
          File.Move(tmpName, targetFileName);
          _SetTimes(targetFileName, true);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1629uL);
          if (zombie != null && File.Exists(zombie)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1630uL);
            ReallyDelete(zombie);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1631uL);
          if (checkLaterForResetDirTimes) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1632uL);
            if (this.FileName.IndexOf('/') != -1) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1633uL);
              string dirname = Path.GetDirectoryName(this.FileName);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1634uL);
              if (this._container.ZipFile[dirname] == null) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1635uL);
                _SetTimes(Path.GetDirectoryName(targetFileName), false);
              }
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1636uL);
          if ((_VersionMadeBy & 0xff00) == 0xa00 || (_VersionMadeBy & 0xff00) == 0x0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1637uL);
            File.SetAttributes(targetFileName, (FileAttributes)_ExternalFileAttrs);
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1638uL);
        OnAfterExtract(baseDir);
        ExitTry:
        ;
      } catch (Exception) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1639uL);
        _ioOperationCanceled = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1640uL);
        throw;
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1641uL);
        if (_ioOperationCanceled) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1642uL);
          if (targetFileName != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1643uL);
            try {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1644uL);
              if (output != null) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1645uL);
                output.Close();
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1646uL);
              if (File.Exists(targetFileName) && !fileExistsBeforeExtraction) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1647uL);
                File.Delete(targetFileName);
              }
            } finally {
            }
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1648uL);
    }
    internal void VerifyCrcAfterExtract(Int32 actualCrc32)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1649uL);
      if (actualCrc32 != _Crc32) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1650uL);
        if ((Encryption != EncryptionAlgorithm.WinZipAes128 && Encryption != EncryptionAlgorithm.WinZipAes256) || _WinZipAesMethod != 0x2) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1651uL);
          throw new BadCrcException("CRC error: the file being extracted appears to be corrupted. " + String.Format("Expected 0x{0:X8}, Actual 0x{1:X8}", _Crc32, actualCrc32));
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1652uL);
      if (this.UncompressedSize == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1653uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1654uL);
      if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1655uL);
        WinZipAesCipherStream wzs = _inputDecryptorStream as WinZipAesCipherStream;
        _aesCrypto_forExtract.CalculatedMac = wzs.FinalAuthentication;
        _aesCrypto_forExtract.ReadAndVerifyMac(this.ArchiveStream);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1656uL);
    }
    private int CheckExtractExistingFile(string baseDir, string targetFileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1657uL);
      int loop = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1658uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1659uL);
        switch (ExtractExistingFile) {
          case ExtractExistingFileAction.OverwriteSilently:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1662uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1660uL);
              WriteStatus("the file {0} exists; will overwrite it...", targetFileName);
              System.Int32 RNTRNTRNT_282 = 0;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1661uL);
              return RNTRNTRNT_282;
            }

          case ExtractExistingFileAction.DoNotOverwrite:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1665uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1663uL);
              WriteStatus("the file {0} exists; not extracting entry...", FileName);
              OnAfterExtract(baseDir);
              System.Int32 RNTRNTRNT_283 = 1;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1664uL);
              return RNTRNTRNT_283;
            }

          case ExtractExistingFileAction.InvokeExtractProgressEvent:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1672uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1666uL);
              if (loop > 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1667uL);
                throw new ZipException(String.Format("The file {0} already exists.", targetFileName));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1668uL);
              OnExtractExisting(baseDir);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1669uL);
              if (_ioOperationCanceled) {
                System.Int32 RNTRNTRNT_284 = 2;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1670uL);
                return RNTRNTRNT_284;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1671uL);
              break;
            }

          case ExtractExistingFileAction.Throw:
          default:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1674uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1673uL);
              throw new ZipException(String.Format("The file {0} already exists.", targetFileName));
            }

        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1675uL);
        loop++;
      } while (true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1676uL);
    }
    private void _CheckRead(int nbytes)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1677uL);
      if (nbytes == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1678uL);
        throw new BadReadException(String.Format("bad read of entry {0} from compressed archive.", this.FileName));
      }
    }
    private Stream _inputDecryptorStream;
    private Int32 ExtractOne(Stream output)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1679uL);
      Int32 CrcResult = 0;
      Stream input = this.ArchiveStream;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1680uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1681uL);
        input.Seek(this.FileDataPosition, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(input);
        byte[] bytes = new byte[BufferSize];
        Int64 LeftToRead = (_CompressionMethod_FromZipFile != (short)CompressionMethod.None) ? this.UncompressedSize : this._CompressedFileDataSize;
        _inputDecryptorStream = GetExtractDecryptor(input);
        Stream input3 = GetExtractDecompressor(_inputDecryptorStream);
        Int64 bytesWritten = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1682uL);
        using (var s1 = new Ionic.Crc.CrcCalculatorStream(input3)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1683uL);
          while (LeftToRead > 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1684uL);
            int len = (LeftToRead > bytes.Length) ? bytes.Length : (int)LeftToRead;
            int n = s1.Read(bytes, 0, len);
            _CheckRead(n);
            output.Write(bytes, 0, n);
            LeftToRead -= n;
            bytesWritten += n;
            OnExtractProgress(bytesWritten, UncompressedSize);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1685uL);
            if (_ioOperationCanceled) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1686uL);
              break;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1687uL);
          CrcResult = s1.Crc;
        }
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1688uL);
        var zss = input as ZipSegmentedStream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1689uL);
        if (zss != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1690uL);
          zss.Dispose();
          _archiveStream = null;
        }
      }
      Int32 RNTRNTRNT_285 = CrcResult;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1691uL);
      return RNTRNTRNT_285;
    }
    internal Stream GetExtractDecompressor(Stream input2)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1692uL);
      switch (_CompressionMethod_FromZipFile) {
        case (short)CompressionMethod.None:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1694uL);
          
          {
            Stream RNTRNTRNT_286 = input2;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1693uL);
            return RNTRNTRNT_286;
          }

        case (short)CompressionMethod.Deflate:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1696uL);
          
          {
            Stream RNTRNTRNT_287 = new Ionic.Zlib.DeflateStream(input2, Ionic.Zlib.CompressionMode.Decompress, true);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1695uL);
            return RNTRNTRNT_287;
          }

        case (short)CompressionMethod.BZip2:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1698uL);
          
          {
            Stream RNTRNTRNT_288 = new Ionic.BZip2.BZip2InputStream(input2, true);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1697uL);
            return RNTRNTRNT_288;
          }

      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1699uL);
      return null;
    }
    internal Stream GetExtractDecryptor(Stream input)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1700uL);
      Stream input2 = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1701uL);
      if (_Encryption_FromZipFile == EncryptionAlgorithm.PkzipWeak) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1702uL);
        input2 = new ZipCipherStream(input, _zipCrypto_forExtract, CryptoMode.Decrypt);
      } else if (_Encryption_FromZipFile == EncryptionAlgorithm.WinZipAes128 || _Encryption_FromZipFile == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1704uL);
        input2 = new WinZipAesCipherStream(input, _aesCrypto_forExtract, _CompressedFileDataSize, CryptoMode.Decrypt);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1703uL);
        input2 = input;
      }
      Stream RNTRNTRNT_289 = input2;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1705uL);
      return RNTRNTRNT_289;
    }
    internal void _SetTimes(string fileOrDirectory, bool isFile)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1706uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1707uL);
        if (_ntfsTimesAreSet) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1708uL);
          if (isFile) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1709uL);
            if (File.Exists(fileOrDirectory)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1710uL);
              File.SetCreationTimeUtc(fileOrDirectory, _Ctime);
              File.SetLastAccessTimeUtc(fileOrDirectory, _Atime);
              File.SetLastWriteTimeUtc(fileOrDirectory, _Mtime);
            }
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1711uL);
            if (Directory.Exists(fileOrDirectory)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1712uL);
              Directory.SetCreationTimeUtc(fileOrDirectory, _Ctime);
              Directory.SetLastAccessTimeUtc(fileOrDirectory, _Atime);
              Directory.SetLastWriteTimeUtc(fileOrDirectory, _Mtime);
            }
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1713uL);
          DateTime AdjustedLastModified = Ionic.Zip.SharedUtilities.AdjustTime_Reverse(LastModified);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1714uL);
          if (isFile) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1715uL);
            File.SetLastWriteTime(fileOrDirectory, AdjustedLastModified);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1716uL);
            Directory.SetLastWriteTime(fileOrDirectory, AdjustedLastModified);
          }
        }
      } catch (System.IO.IOException ioexc1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1717uL);
        WriteStatus("failed to set time on {0}: {1}", fileOrDirectory, ioexc1.Message);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1718uL);
    }
    private string UnsupportedAlgorithm {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1719uL);
        string alg = String.Empty;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1720uL);
        switch (_UnsupportedAlgorithmId) {
          case 0:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1723uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1721uL);
              alg = "--";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1722uL);
              break;
            }

          case 0x6601:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1726uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1724uL);
              alg = "DES";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1725uL);
              break;
            }

          case 0x6602:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1729uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1727uL);
              alg = "RC2";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1728uL);
              break;
            }

          case 0x6603:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1732uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1730uL);
              alg = "3DES-168";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1731uL);
              break;
            }

          case 0x6609:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1735uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1733uL);
              alg = "3DES-112";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1734uL);
              break;
            }

          case 0x660e:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1738uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1736uL);
              alg = "PKWare AES128";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1737uL);
              break;
            }

          case 0x660f:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1741uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1739uL);
              alg = "PKWare AES192";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1740uL);
              break;
            }

          case 0x6610:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1744uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1742uL);
              alg = "PKWare AES256";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1743uL);
              break;
            }

          case 0x6702:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1747uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1745uL);
              alg = "RC2";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1746uL);
              break;
            }

          case 0x6720:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1750uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1748uL);
              alg = "Blowfish";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1749uL);
              break;
            }

          case 0x6721:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1753uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1751uL);
              alg = "Twofish";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1752uL);
              break;
            }

          case 0x6801:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1756uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1754uL);
              alg = "RC4";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1755uL);
              break;
            }

          case 0xffff:
          default:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1759uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1757uL);
              alg = String.Format("Unknown (0x{0:X4})", _UnsupportedAlgorithmId);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1758uL);
              break;
            }

        }
        System.String RNTRNTRNT_290 = alg;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1760uL);
        return RNTRNTRNT_290;
      }
    }
    private string UnsupportedCompressionMethod {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1761uL);
        string meth = String.Empty;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1762uL);
        switch ((int)_CompressionMethod) {
          case 0:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1765uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1763uL);
              meth = "Store";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1764uL);
              break;
            }

          case 1:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1768uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1766uL);
              meth = "Shrink";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1767uL);
              break;
            }

          case 8:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1771uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1769uL);
              meth = "DEFLATE";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1770uL);
              break;
            }

          case 9:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1774uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1772uL);
              meth = "Deflate64";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1773uL);
              break;
            }

          case 12:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1777uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1775uL);
              meth = "BZIP2";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1776uL);
              break;
            }

          case 14:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1780uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1778uL);
              meth = "LZMA";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1779uL);
              break;
            }

          case 19:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1783uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1781uL);
              meth = "LZ77";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1782uL);
              break;
            }

          case 98:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1786uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1784uL);
              meth = "PPMd";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1785uL);
              break;
            }

          default:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1789uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1787uL);
              meth = String.Format("Unknown (0x{0:X4})", _CompressionMethod);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1788uL);
              break;
            }

        }
        System.String RNTRNTRNT_291 = meth;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1790uL);
        return RNTRNTRNT_291;
      }
    }
    internal void ValidateEncryption()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1791uL);
      if (Encryption != EncryptionAlgorithm.PkzipWeak && Encryption != EncryptionAlgorithm.WinZipAes128 && Encryption != EncryptionAlgorithm.WinZipAes256 && Encryption != EncryptionAlgorithm.None) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1792uL);
        if (_UnsupportedAlgorithmId != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1793uL);
          throw new ZipException(String.Format("Cannot extract: Entry {0} is encrypted with an algorithm not supported by DotNetZip: {1}", FileName, UnsupportedAlgorithm));
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1794uL);
          throw new ZipException(String.Format("Cannot extract: Entry {0} uses an unsupported encryption algorithm ({1:X2})", FileName, (int)Encryption));
        }
      }
    }
    private void ValidateCompression()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1795uL);
      if ((_CompressionMethod_FromZipFile != (short)CompressionMethod.None) && (_CompressionMethod_FromZipFile != (short)CompressionMethod.Deflate) && (_CompressionMethod_FromZipFile != (short)CompressionMethod.BZip2)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1796uL);
        throw new ZipException(String.Format("Entry {0} uses an unsupported compression method (0x{1:X2}, {2})", FileName, _CompressionMethod_FromZipFile, UnsupportedCompressionMethod));
      }
    }
    private void SetupCryptoForExtract(string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1797uL);
      if (_Encryption_FromZipFile == EncryptionAlgorithm.None) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1798uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1799uL);
      if (_Encryption_FromZipFile == EncryptionAlgorithm.PkzipWeak) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1800uL);
        if (password == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1801uL);
          throw new ZipException("Missing password.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1802uL);
        this.ArchiveStream.Seek(this.FileDataPosition - 12, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
        _zipCrypto_forExtract = ZipCrypto.ForRead(password, this);
      } else if (_Encryption_FromZipFile == EncryptionAlgorithm.WinZipAes128 || _Encryption_FromZipFile == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1803uL);
        if (password == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1804uL);
          throw new ZipException("Missing password.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1805uL);
        if (_aesCrypto_forExtract != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1806uL);
          _aesCrypto_forExtract.Password = password;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1807uL);
          int sizeOfSaltAndPv = GetLengthOfCryptoHeaderBytes(_Encryption_FromZipFile);
          this.ArchiveStream.Seek(this.FileDataPosition - sizeOfSaltAndPv, SeekOrigin.Begin);
          Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
          int keystrength = GetKeyStrengthInBits(_Encryption_FromZipFile);
          _aesCrypto_forExtract = WinZipAesCrypto.ReadFromStream(password, keystrength, this.ArchiveStream);
        }
      }
    }
    private bool ValidateOutput(string basedir, Stream outstream, out string outFileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1808uL);
      if (basedir != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1809uL);
        string f = this.FileName.Replace("\\", "/");
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1810uL);
        if (f.IndexOf(':') == 1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1811uL);
          f = f.Substring(2);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1812uL);
        if (f.StartsWith("/")) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1813uL);
          f = f.Substring(1);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1814uL);
        if (_container.ZipFile.FlattenFoldersOnExtract) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1815uL);
          outFileName = Path.Combine(basedir, (f.IndexOf('/') != -1) ? Path.GetFileName(f) : f);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1816uL);
          outFileName = Path.Combine(basedir, f);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1817uL);
        outFileName = outFileName.Replace("/", "\\");
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1818uL);
        if ((IsDirectory) || (FileName.EndsWith("/"))) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1819uL);
          if (!Directory.Exists(outFileName)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1820uL);
            Directory.CreateDirectory(outFileName);
            _SetTimes(outFileName, false);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1821uL);
            if (ExtractExistingFile == ExtractExistingFileAction.OverwriteSilently) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1822uL);
              _SetTimes(outFileName, false);
            }
          }
          System.Boolean RNTRNTRNT_292 = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1823uL);
          return RNTRNTRNT_292;
        }
        System.Boolean RNTRNTRNT_293 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1824uL);
        return RNTRNTRNT_293;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1825uL);
      if (outstream != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1826uL);
        outFileName = null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1827uL);
        if ((IsDirectory) || (FileName.EndsWith("/"))) {
          System.Boolean RNTRNTRNT_294 = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1828uL);
          return RNTRNTRNT_294;
        }
        System.Boolean RNTRNTRNT_295 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1829uL);
        return RNTRNTRNT_295;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1830uL);
      throw new ArgumentNullException("outstream");
    }
  }
}
