using System;
using System.IO;
using System.Text;
using System.Reflection;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Collections.Generic;
namespace Ionic
{
  internal enum LogicalConjunction
  {
    NONE,
    AND,
    OR,
    XOR
  }
  internal enum WhichTime
  {
    atime,
    mtime,
    ctime
  }
  internal enum ComparisonOperator
  {
    [Description(">")]
    GreaterThan,
    [Description(">=")]
    GreaterThanOrEqualTo,
    [Description("<")]
    LesserThan,
    [Description("<=")]
    LesserThanOrEqualTo,
    [Description("=")]
    EqualTo,
    [Description("!=")]
    NotEqualTo
  }
  internal abstract partial class SelectionCriterion
  {
    internal virtual bool Verbose { get; set; }
    internal abstract bool Evaluate(string filename);
    [System.Diagnostics.Conditional("SelectorTrace")]
    protected static void CriterionTrace(string format, params object[] args)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(420uL);
    }
  }
  internal partial class SizeCriterion : SelectionCriterion
  {
    internal ComparisonOperator Operator;
    internal Int64 Size;
    public override String ToString()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(421uL);
      StringBuilder sb = new StringBuilder();
      sb.Append("size ").Append(EnumUtil.GetDescription(Operator)).Append(" ").Append(Size.ToString());
      String RNTRNTRNT_100 = sb.ToString();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(422uL);
      return RNTRNTRNT_100;
    }
    internal override bool Evaluate(string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(423uL);
      System.IO.FileInfo fi = new System.IO.FileInfo(filename);
      CriterionTrace("SizeCriterion::Evaluate('{0}' [{1}])", filename, this.ToString());
      System.Boolean RNTRNTRNT_101 = _Evaluate(fi.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(424uL);
      return RNTRNTRNT_101;
    }
    private bool _Evaluate(Int64 Length)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(425uL);
      bool result = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(426uL);
      switch (Operator) {
        case ComparisonOperator.GreaterThanOrEqualTo:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(429uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(427uL);
            result = Length >= Size;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(428uL);
            break;
          }

        case ComparisonOperator.GreaterThan:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(432uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(430uL);
            result = Length > Size;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(431uL);
            break;
          }

        case ComparisonOperator.LesserThanOrEqualTo:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(435uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(433uL);
            result = Length <= Size;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(434uL);
            break;
          }

        case ComparisonOperator.LesserThan:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(438uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(436uL);
            result = Length < Size;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(437uL);
            break;
          }

        case ComparisonOperator.EqualTo:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(441uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(439uL);
            result = Length == Size;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(440uL);
            break;
          }

        case ComparisonOperator.NotEqualTo:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(444uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(442uL);
            result = Length != Size;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(443uL);
            break;
          }

        default:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(446uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(445uL);
            throw new ArgumentException("Operator");
          }

      }
      System.Boolean RNTRNTRNT_102 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(447uL);
      return RNTRNTRNT_102;
    }
  }
  internal partial class TimeCriterion : SelectionCriterion
  {
    internal ComparisonOperator Operator;
    internal WhichTime Which;
    internal DateTime Time;
    public override String ToString()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(448uL);
      StringBuilder sb = new StringBuilder();
      sb.Append(Which.ToString()).Append(" ").Append(EnumUtil.GetDescription(Operator)).Append(" ").Append(Time.ToString("yyyy-MM-dd-HH:mm:ss"));
      String RNTRNTRNT_103 = sb.ToString();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(449uL);
      return RNTRNTRNT_103;
    }
    internal override bool Evaluate(string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(450uL);
      DateTime x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(451uL);
      switch (Which) {
        case WhichTime.atime:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(454uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(452uL);
            x = System.IO.File.GetLastAccessTime(filename).ToUniversalTime();
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(453uL);
            break;
          }

        case WhichTime.mtime:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(457uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(455uL);
            x = System.IO.File.GetLastWriteTime(filename).ToUniversalTime();
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(456uL);
            break;
          }

        case WhichTime.ctime:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(460uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(458uL);
            x = System.IO.File.GetCreationTime(filename).ToUniversalTime();
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(459uL);
            break;
          }

        default:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(462uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(461uL);
            throw new ArgumentException("Operator");
          }

      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(463uL);
      CriterionTrace("TimeCriterion({0},{1})= {2}", filename, Which.ToString(), x);
      System.Boolean RNTRNTRNT_104 = _Evaluate(x);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(464uL);
      return RNTRNTRNT_104;
    }
    private bool _Evaluate(DateTime x)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(465uL);
      bool result = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(466uL);
      switch (Operator) {
        case ComparisonOperator.GreaterThanOrEqualTo:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(469uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(467uL);
            result = (x >= Time);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(468uL);
            break;
          }

        case ComparisonOperator.GreaterThan:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(472uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(470uL);
            result = (x > Time);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(471uL);
            break;
          }

        case ComparisonOperator.LesserThanOrEqualTo:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(475uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(473uL);
            result = (x <= Time);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(474uL);
            break;
          }

        case ComparisonOperator.LesserThan:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(478uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(476uL);
            result = (x < Time);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(477uL);
            break;
          }

        case ComparisonOperator.EqualTo:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(481uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(479uL);
            result = (x == Time);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(480uL);
            break;
          }

        case ComparisonOperator.NotEqualTo:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(484uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(482uL);
            result = (x != Time);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(483uL);
            break;
          }

        default:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(486uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(485uL);
            throw new ArgumentException("Operator");
          }

      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(487uL);
      CriterionTrace("TimeCriterion: {0}", result);
      System.Boolean RNTRNTRNT_105 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(488uL);
      return RNTRNTRNT_105;
    }
  }
  internal partial class NameCriterion : SelectionCriterion
  {
    private Regex _re;
    private String _regexString;
    internal ComparisonOperator Operator;
    private string _MatchingFileSpec;
    internal virtual string MatchingFileSpec {
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(489uL);
        if (Directory.Exists(value)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(490uL);
          _MatchingFileSpec = ".\\" + value + "\\*.*";
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(491uL);
          _MatchingFileSpec = value;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(492uL);
        _regexString = "^" + Regex.Escape(_MatchingFileSpec).Replace("\\\\\\*\\.\\*", "\\\\([^\\.]+|.*\\.[^\\\\\\.]*)").Replace("\\.\\*", "\\.[^\\\\\\.]*").Replace("\\*", ".*").Replace("\\?", "[^\\\\\\.]") + "$";
        CriterionTrace("NameCriterion regexString({0})", _regexString);
        _re = new Regex(_regexString, RegexOptions.IgnoreCase);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(493uL);
      }
    }
    public override String ToString()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(494uL);
      StringBuilder sb = new StringBuilder();
      sb.Append("name ").Append(EnumUtil.GetDescription(Operator)).Append(" '").Append(_MatchingFileSpec).Append("'");
      String RNTRNTRNT_106 = sb.ToString();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(495uL);
      return RNTRNTRNT_106;
    }
    internal override bool Evaluate(string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(496uL);
      CriterionTrace("NameCriterion::Evaluate('{0}' pattern[{1}])", filename, _MatchingFileSpec);
      System.Boolean RNTRNTRNT_107 = _Evaluate(filename);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(497uL);
      return RNTRNTRNT_107;
    }
    private bool _Evaluate(string fullpath)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(498uL);
      CriterionTrace("NameCriterion::Evaluate({0})", fullpath);
      String f = (_MatchingFileSpec.IndexOf('\\') == -1) ? System.IO.Path.GetFileName(fullpath) : fullpath;
      bool result = _re.IsMatch(f);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(499uL);
      if (Operator != ComparisonOperator.EqualTo) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500uL);
        result = !result;
      }
      System.Boolean RNTRNTRNT_108 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501uL);
      return RNTRNTRNT_108;
    }
  }
  internal partial class TypeCriterion : SelectionCriterion
  {
    private char ObjectType;
    internal ComparisonOperator Operator;
    internal string AttributeString {
      get {
        System.String RNTRNTRNT_109 = ObjectType.ToString();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502uL);
        return RNTRNTRNT_109;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503uL);
        if (value.Length != 1 || (value[0] != 'D' && value[0] != 'F')) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504uL);
          throw new ArgumentException("Specify a single character: either D or F");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505uL);
        ObjectType = value[0];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506uL);
      }
    }
    public override String ToString()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(507uL);
      StringBuilder sb = new StringBuilder();
      sb.Append("type ").Append(EnumUtil.GetDescription(Operator)).Append(" ").Append(AttributeString);
      String RNTRNTRNT_110 = sb.ToString();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(508uL);
      return RNTRNTRNT_110;
    }
    internal override bool Evaluate(string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(509uL);
      CriterionTrace("TypeCriterion::Evaluate({0})", filename);
      bool result = (ObjectType == 'D') ? Directory.Exists(filename) : File.Exists(filename);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(510uL);
      if (Operator != ComparisonOperator.EqualTo) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(511uL);
        result = !result;
      }
      System.Boolean RNTRNTRNT_111 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(512uL);
      return RNTRNTRNT_111;
    }
  }
  internal partial class AttributesCriterion : SelectionCriterion
  {
    private FileAttributes _Attributes;
    internal ComparisonOperator Operator;
    internal string AttributeString {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(513uL);
        string result = "";
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(514uL);
        if ((_Attributes & FileAttributes.Hidden) != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(515uL);
          result += "H";
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(516uL);
        if ((_Attributes & FileAttributes.System) != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(517uL);
          result += "S";
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(518uL);
        if ((_Attributes & FileAttributes.ReadOnly) != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(519uL);
          result += "R";
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(520uL);
        if ((_Attributes & FileAttributes.Archive) != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(521uL);
          result += "A";
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(522uL);
        if ((_Attributes & FileAttributes.ReparsePoint) != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(523uL);
          result += "L";
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(524uL);
        if ((_Attributes & FileAttributes.NotContentIndexed) != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(525uL);
          result += "I";
        }
        System.String RNTRNTRNT_112 = result;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(526uL);
        return RNTRNTRNT_112;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(527uL);
        _Attributes = FileAttributes.Normal;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(528uL);
        foreach (char c in value.ToUpper()) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(529uL);
          switch (c) {
            case 'H':
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(534uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(530uL);
                if ((_Attributes & FileAttributes.Hidden) != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(531uL);
                  throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(532uL);
                _Attributes |= FileAttributes.Hidden;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(533uL);
                break;
              }

            case 'R':
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(539uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(535uL);
                if ((_Attributes & FileAttributes.ReadOnly) != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(536uL);
                  throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(537uL);
                _Attributes |= FileAttributes.ReadOnly;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(538uL);
                break;
              }

            case 'S':
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(544uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(540uL);
                if ((_Attributes & FileAttributes.System) != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(541uL);
                  throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(542uL);
                _Attributes |= FileAttributes.System;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(543uL);
                break;
              }

            case 'A':
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(549uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(545uL);
                if ((_Attributes & FileAttributes.Archive) != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(546uL);
                  throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(547uL);
                _Attributes |= FileAttributes.Archive;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(548uL);
                break;
              }

            case 'I':
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(554uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(550uL);
                if ((_Attributes & FileAttributes.NotContentIndexed) != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(551uL);
                  throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(552uL);
                _Attributes |= FileAttributes.NotContentIndexed;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(553uL);
                break;
              }

            case 'L':
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(559uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(555uL);
                if ((_Attributes & FileAttributes.ReparsePoint) != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(556uL);
                  throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(557uL);
                _Attributes |= FileAttributes.ReparsePoint;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(558uL);
                break;
              }

            default:
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(561uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(560uL);
                throw new ArgumentException(value);
              }

          }
        }
      }
    }
    public override String ToString()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(562uL);
      StringBuilder sb = new StringBuilder();
      sb.Append("attributes ").Append(EnumUtil.GetDescription(Operator)).Append(" ").Append(AttributeString);
      String RNTRNTRNT_113 = sb.ToString();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(563uL);
      return RNTRNTRNT_113;
    }
    private bool _EvaluateOne(FileAttributes fileAttrs, FileAttributes criterionAttrs)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(564uL);
      bool result = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(565uL);
      if ((_Attributes & criterionAttrs) == criterionAttrs) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(566uL);
        result = ((fileAttrs & criterionAttrs) == criterionAttrs);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(567uL);
        result = true;
      }
      System.Boolean RNTRNTRNT_114 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(568uL);
      return RNTRNTRNT_114;
    }
    internal override bool Evaluate(string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(569uL);
      if (Directory.Exists(filename)) {
        System.Boolean RNTRNTRNT_115 = (Operator != ComparisonOperator.EqualTo);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(570uL);
        return RNTRNTRNT_115;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(571uL);
      FileAttributes fileAttrs = System.IO.File.GetAttributes(filename);
      System.Boolean RNTRNTRNT_116 = _Evaluate(fileAttrs);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(572uL);
      return RNTRNTRNT_116;
    }
    private bool _Evaluate(FileAttributes fileAttrs)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(573uL);
      bool result = _EvaluateOne(fileAttrs, FileAttributes.Hidden);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(574uL);
      if (result) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(575uL);
        result = _EvaluateOne(fileAttrs, FileAttributes.System);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(576uL);
      if (result) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(577uL);
        result = _EvaluateOne(fileAttrs, FileAttributes.ReadOnly);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(578uL);
      if (result) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(579uL);
        result = _EvaluateOne(fileAttrs, FileAttributes.Archive);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(580uL);
      if (result) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(581uL);
        result = _EvaluateOne(fileAttrs, FileAttributes.NotContentIndexed);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(582uL);
      if (result) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(583uL);
        result = _EvaluateOne(fileAttrs, FileAttributes.ReparsePoint);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(584uL);
      if (Operator != ComparisonOperator.EqualTo) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(585uL);
        result = !result;
      }
      System.Boolean RNTRNTRNT_117 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(586uL);
      return RNTRNTRNT_117;
    }
  }
  internal partial class CompoundCriterion : SelectionCriterion
  {
    internal LogicalConjunction Conjunction;
    internal SelectionCriterion Left;
    private SelectionCriterion _Right;
    internal SelectionCriterion Right {
      get {
        SelectionCriterion RNTRNTRNT_118 = _Right;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(587uL);
        return RNTRNTRNT_118;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(588uL);
        _Right = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(589uL);
        if (value == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(590uL);
          Conjunction = LogicalConjunction.NONE;
        } else if (Conjunction == LogicalConjunction.NONE) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(591uL);
          Conjunction = LogicalConjunction.AND;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(592uL);
      }
    }
    internal override bool Evaluate(string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(593uL);
      bool result = Left.Evaluate(filename);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(594uL);
      switch (Conjunction) {
        case LogicalConjunction.AND:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(598uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(595uL);
            if (result) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(596uL);
              result = Right.Evaluate(filename);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(597uL);
            break;
          }

        case LogicalConjunction.OR:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(602uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(599uL);
            if (!result) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(600uL);
              result = Right.Evaluate(filename);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(601uL);
            break;
          }

        case LogicalConjunction.XOR:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(605uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(603uL);
            result ^= Right.Evaluate(filename);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(604uL);
            break;
          }

        default:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(607uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(606uL);
            throw new ArgumentException("Conjunction");
          }

      }
      System.Boolean RNTRNTRNT_119 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(608uL);
      return RNTRNTRNT_119;
    }
    public override String ToString()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(609uL);
      StringBuilder sb = new StringBuilder();
      sb.Append("(").Append((Left != null) ? Left.ToString() : "null").Append(" ").Append(Conjunction.ToString()).Append(" ").Append((Right != null) ? Right.ToString() : "null").Append(")");
      String RNTRNTRNT_120 = sb.ToString();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(610uL);
      return RNTRNTRNT_120;
    }
  }
  public partial class FileSelector
  {
    internal SelectionCriterion _Criterion;
    public FileSelector(String selectionCriteria) : this(selectionCriteria, true)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(611uL);
    }
    public FileSelector(String selectionCriteria, bool traverseDirectoryReparsePoints)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(612uL);
      if (!String.IsNullOrEmpty(selectionCriteria)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(613uL);
        _Criterion = _ParseCriterion(selectionCriteria);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(614uL);
      TraverseReparsePoints = traverseDirectoryReparsePoints;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(615uL);
    }
    public String SelectionCriteria {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(616uL);
        if (_Criterion == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(617uL);
          return null;
        }
        String RNTRNTRNT_121 = _Criterion.ToString();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(618uL);
        return RNTRNTRNT_121;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(619uL);
        if (value == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(620uL);
          _Criterion = null;
        } else if (value.Trim() == "") {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(622uL);
          _Criterion = null;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(621uL);
          _Criterion = _ParseCriterion(value);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(623uL);
      }
    }
    public bool TraverseReparsePoints { get; set; }
    private enum ParseState
    {
      Start,
      OpenParen,
      CriterionDone,
      ConjunctionPending,
      Whitespace
    }
    private static class RegexAssertions
    {
      public static readonly String PrecededByOddNumberOfSingleQuotes = "(?<=(?:[^']*'[^']*')*'[^']*)";
      public static readonly String FollowedByOddNumberOfSingleQuotesAndLineEnd = "(?=[^']*'(?:[^']*'[^']*')*[^']*$)";
      public static readonly String PrecededByEvenNumberOfSingleQuotes = "(?<=(?:[^']*'[^']*')*[^']*)";
      public static readonly String FollowedByEvenNumberOfSingleQuotesAndLineEnd = "(?=(?:[^']*'[^']*')*[^']*$)";
    }
    private static string NormalizeCriteriaExpression(string source)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(624uL);
      string[][] prPairs = {
        new string[] {
          "([^']*)\\(\\(([^']+)",
          "$1( ($2"
        },
        new string[] {
          "(.)\\)\\)",
          "$1) )"
        },
        new string[] {
          "\\((\\S)",
          "( $1"
        },
        new string[] {
          "(\\S)\\)",
          "$1 )"
        },
        new string[] {
          "^\\)",
          " )"
        },
        new string[] {
          "(\\S)\\(",
          "$1 ("
        },
        new string[] {
          "\\)(\\S)",
          ") $1"
        },
        new string[] {
          "(=)('[^']*')",
          "$1 $2"
        },
        new string[] {
          "([^ !><])(>|<|!=|=)",
          "$1 $2"
        },
        new string[] {
          "(>|<|!=|=)([^ =])",
          "$1 $2"
        },
        new string[] {
          "/",
          "\\"
        }
      };
      string interim = source;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(625uL);
      for (int i = 0; i < prPairs.Length; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(626uL);
        string pattern = RegexAssertions.PrecededByEvenNumberOfSingleQuotes + prPairs[i][0] + RegexAssertions.FollowedByEvenNumberOfSingleQuotesAndLineEnd;
        interim = Regex.Replace(interim, pattern, prPairs[i][1]);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(627uL);
      var regexPattern = "/" + RegexAssertions.FollowedByOddNumberOfSingleQuotesAndLineEnd;
      interim = Regex.Replace(interim, regexPattern, "\\");
      regexPattern = " " + RegexAssertions.FollowedByOddNumberOfSingleQuotesAndLineEnd;
      System.String RNTRNTRNT_122 = Regex.Replace(interim, regexPattern, "\u0006");
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(628uL);
      return RNTRNTRNT_122;
    }
    private static SelectionCriterion _ParseCriterion(String s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(629uL);
      if (s == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(630uL);
        return null;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(631uL);
      s = NormalizeCriteriaExpression(s);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(632uL);
      if (s.IndexOf(" ") == -1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(633uL);
        s = "name = " + s;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(634uL);
      string[] tokens = s.Trim().Split(' ', '\t');
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(635uL);
      if (tokens.Length < 3) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(636uL);
        throw new ArgumentException(s);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(637uL);
      SelectionCriterion current = null;
      LogicalConjunction pendingConjunction = LogicalConjunction.NONE;
      ParseState state;
      var stateStack = new System.Collections.Generic.Stack<ParseState>();
      var critStack = new System.Collections.Generic.Stack<SelectionCriterion>();
      stateStack.Push(ParseState.Start);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(638uL);
      for (int i = 0; i < tokens.Length; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(639uL);
        string tok1 = tokens[i].ToLower();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(640uL);
        switch (tok1) {
          case "and":
          case "xor":
          case "or":
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(648uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(641uL);
              state = stateStack.Peek();
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(642uL);
              if (state != ParseState.CriterionDone) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(643uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(644uL);
              if (tokens.Length <= i + 3) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(645uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(646uL);
              pendingConjunction = (LogicalConjunction)Enum.Parse(typeof(LogicalConjunction), tokens[i].ToUpper(), true);
              current = new CompoundCriterion {
                Left = current,
                Right = null,
                Conjunction = pendingConjunction
              };
              stateStack.Push(state);
              stateStack.Push(ParseState.ConjunctionPending);
              critStack.Push(current);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(647uL);
              break;
            }

          case "(":
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(656uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(649uL);
              state = stateStack.Peek();
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(650uL);
              if (state != ParseState.Start && state != ParseState.ConjunctionPending && state != ParseState.OpenParen) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(651uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(652uL);
              if (tokens.Length <= i + 4) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(653uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(654uL);
              stateStack.Push(ParseState.OpenParen);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(655uL);
              break;
            }

          case ")":
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(662uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(657uL);
              state = stateStack.Pop();
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(658uL);
              if (stateStack.Peek() != ParseState.OpenParen) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(659uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(660uL);
              stateStack.Pop();
              stateStack.Push(ParseState.CriterionDone);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(661uL);
              break;
            }

          case "atime":
          case "ctime":
          case "mtime":
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(677uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(663uL);
              if (tokens.Length <= i + 2) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(664uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(665uL);
              DateTime t;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(666uL);
              try {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(667uL);
                t = DateTime.ParseExact(tokens[i + 2], "yyyy-MM-dd-HH:mm:ss", null);
              } catch (FormatException) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(668uL);
                try {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(669uL);
                  t = DateTime.ParseExact(tokens[i + 2], "yyyy/MM/dd-HH:mm:ss", null);
                } catch (FormatException) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(670uL);
                  try {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(671uL);
                    t = DateTime.ParseExact(tokens[i + 2], "yyyy/MM/dd", null);
                  } catch (FormatException) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(672uL);
                    try {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(673uL);
                      t = DateTime.ParseExact(tokens[i + 2], "MM/dd/yyyy", null);
                    } catch (FormatException) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(674uL);
                      t = DateTime.ParseExact(tokens[i + 2], "yyyy-MM-dd", null);
                    }
                  }
                }
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(675uL);
              t = DateTime.SpecifyKind(t, DateTimeKind.Local).ToUniversalTime();
              current = new TimeCriterion {
                Which = (WhichTime)Enum.Parse(typeof(WhichTime), tokens[i], true),
                Operator = (ComparisonOperator)EnumUtil.Parse(typeof(ComparisonOperator), tokens[i + 1]),
                Time = t
              };
              i += 2;
              stateStack.Push(ParseState.CriterionDone);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(676uL);
              break;
            }

          case "length":
          case "size":
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(691uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(678uL);
              if (tokens.Length <= i + 2) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(679uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(680uL);
              Int64 sz = 0;
              string v = tokens[i + 2];
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(681uL);
              if (v.ToUpper().EndsWith("K")) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(682uL);
                sz = Int64.Parse(v.Substring(0, v.Length - 1)) * 1024;
              } else if (v.ToUpper().EndsWith("KB")) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(688uL);
                sz = Int64.Parse(v.Substring(0, v.Length - 2)) * 1024;
              } else if (v.ToUpper().EndsWith("M")) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(687uL);
                sz = Int64.Parse(v.Substring(0, v.Length - 1)) * 1024 * 1024;
              } else if (v.ToUpper().EndsWith("MB")) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(686uL);
                sz = Int64.Parse(v.Substring(0, v.Length - 2)) * 1024 * 1024;
              } else if (v.ToUpper().EndsWith("G")) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(685uL);
                sz = Int64.Parse(v.Substring(0, v.Length - 1)) * 1024 * 1024 * 1024;
              } else if (v.ToUpper().EndsWith("GB")) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(684uL);
                sz = Int64.Parse(v.Substring(0, v.Length - 2)) * 1024 * 1024 * 1024;
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(683uL);
                sz = Int64.Parse(tokens[i + 2]);
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(689uL);
              current = new SizeCriterion {
                Size = sz,
                Operator = (ComparisonOperator)EnumUtil.Parse(typeof(ComparisonOperator), tokens[i + 1])
              };
              i += 2;
              stateStack.Push(ParseState.CriterionDone);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(690uL);
              break;
            }

          case "filename":
          case "name":
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(701uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(692uL);
              if (tokens.Length <= i + 2) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(693uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(694uL);
              ComparisonOperator c = (ComparisonOperator)EnumUtil.Parse(typeof(ComparisonOperator), tokens[i + 1]);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(695uL);
              if (c != ComparisonOperator.NotEqualTo && c != ComparisonOperator.EqualTo) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(696uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(697uL);
              string m = tokens[i + 2];
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(698uL);
              if (m.StartsWith("'") && m.EndsWith("'")) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(699uL);
                m = m.Substring(1, m.Length - 2).Replace("\u0006", " ");
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(700uL);
              current = new NameCriterion {
                MatchingFileSpec = m,
                Operator = c
              };
              i += 2;
              stateStack.Push(ParseState.CriterionDone);
            }

            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(703uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(702uL);
              break;
            }

          case "attrs":
          case "attributes":
          case "type":
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(710uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(704uL);
              if (tokens.Length <= i + 2) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(705uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(706uL);
              ComparisonOperator c = (ComparisonOperator)EnumUtil.Parse(typeof(ComparisonOperator), tokens[i + 1]);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(707uL);
              if (c != ComparisonOperator.NotEqualTo && c != ComparisonOperator.EqualTo) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(708uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(709uL);
              current = (tok1 == "type") ? (SelectionCriterion)new TypeCriterion {
                AttributeString = tokens[i + 2],
                Operator = c
              } : (SelectionCriterion)new AttributesCriterion {
                AttributeString = tokens[i + 2],
                Operator = c
              };
              i += 2;
              stateStack.Push(ParseState.CriterionDone);
            }

            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(712uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(711uL);
              break;
            }

          case "":
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(715uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(713uL);
              stateStack.Push(ParseState.Whitespace);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(714uL);
              break;
            }

          default:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(717uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(716uL);
              throw new ArgumentException("'" + tokens[i] + "'");
            }

        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(718uL);
        state = stateStack.Peek();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(719uL);
        if (state == ParseState.CriterionDone) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(720uL);
          stateStack.Pop();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(721uL);
          if (stateStack.Peek() == ParseState.ConjunctionPending) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(722uL);
            while (stateStack.Peek() == ParseState.ConjunctionPending) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(723uL);
              var cc = critStack.Pop() as CompoundCriterion;
              cc.Right = current;
              current = cc;
              stateStack.Pop();
              state = stateStack.Pop();
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(724uL);
              if (state != ParseState.CriterionDone) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(725uL);
                throw new ArgumentException("??");
              }
            }
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(726uL);
            stateStack.Push(ParseState.CriterionDone);
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(727uL);
        if (state == ParseState.Whitespace) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(728uL);
          stateStack.Pop();
        }
      }
      SelectionCriterion RNTRNTRNT_123 = current;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(729uL);
      return RNTRNTRNT_123;
    }
    public override String ToString()
    {
      String RNTRNTRNT_124 = "FileSelector(" + _Criterion.ToString() + ")";
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(730uL);
      return RNTRNTRNT_124;
    }
    private bool Evaluate(string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(731uL);
      SelectorTrace("Evaluate({0})", filename);
      bool result = _Criterion.Evaluate(filename);
      System.Boolean RNTRNTRNT_125 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(732uL);
      return RNTRNTRNT_125;
    }
    [System.Diagnostics.Conditional("SelectorTrace")]
    private void SelectorTrace(string format, params object[] args)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(733uL);
      if (_Criterion != null && _Criterion.Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(734uL);
        System.Console.WriteLine(format, args);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(735uL);
    }
    public System.Collections.Generic.ICollection<String> SelectFiles(String directory)
    {
      System.Collections.Generic.ICollection<String> RNTRNTRNT_126 = SelectFiles(directory, false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(736uL);
      return RNTRNTRNT_126;
    }
    public System.Collections.ObjectModel.ReadOnlyCollection<String> SelectFiles(String directory, bool recurseDirectories)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(737uL);
      if (_Criterion == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(738uL);
        throw new ArgumentException("SelectionCriteria has not been set");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(739uL);
      var list = new List<String>();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(740uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(741uL);
        if (Directory.Exists(directory)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(742uL);
          String[] filenames = Directory.GetFiles(directory);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(743uL);
          foreach (String filename in filenames) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(744uL);
            if (Evaluate(filename)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(745uL);
              list.Add(filename);
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(746uL);
          if (recurseDirectories) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(747uL);
            String[] dirnames = Directory.GetDirectories(directory);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(748uL);
            foreach (String dir in dirnames) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(749uL);
              if (this.TraverseReparsePoints || ((File.GetAttributes(dir) & FileAttributes.ReparsePoint) == 0)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(750uL);
                if (Evaluate(dir)) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(751uL);
                  list.Add(dir);
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(752uL);
                list.AddRange(this.SelectFiles(dir, recurseDirectories));
              }
            }
          }
        }
      } catch (System.UnauthorizedAccessException) {
      } catch (System.IO.IOException) {
      }
      System.Collections.ObjectModel.ReadOnlyCollection<String> RNTRNTRNT_127 = list.AsReadOnly();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(753uL);
      return RNTRNTRNT_127;
    }
  }
  internal sealed class EnumUtil
  {
    private EnumUtil()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(754uL);
    }
    static internal string GetDescription(System.Enum value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(755uL);
      FieldInfo fi = value.GetType().GetField(value.ToString());
      var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(756uL);
      if (attributes.Length > 0) {
        System.String RNTRNTRNT_128 = attributes[0].Description;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(757uL);
        return RNTRNTRNT_128;
      } else {
        System.String RNTRNTRNT_129 = value.ToString();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(758uL);
        return RNTRNTRNT_129;
      }
    }
    static internal object Parse(Type enumType, string stringRepresentation)
    {
      System.Object RNTRNTRNT_130 = Parse(enumType, stringRepresentation, false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(759uL);
      return RNTRNTRNT_130;
    }
    static internal object Parse(Type enumType, string stringRepresentation, bool ignoreCase)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(760uL);
      if (ignoreCase) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(761uL);
        stringRepresentation = stringRepresentation.ToLower();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(762uL);
      foreach (System.Enum enumVal in System.Enum.GetValues(enumType)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(763uL);
        string description = GetDescription(enumVal);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(764uL);
        if (ignoreCase) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(765uL);
          description = description.ToLower();
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(766uL);
        if (description == stringRepresentation) {
          System.Object RNTRNTRNT_131 = enumVal;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(767uL);
          return RNTRNTRNT_131;
        }
      }
      System.Object RNTRNTRNT_132 = System.Enum.Parse(enumType, stringRepresentation, ignoreCase);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(768uL);
      return RNTRNTRNT_132;
    }
  }
}
