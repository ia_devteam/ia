using System;
using System.Threading;
using System.Collections.Generic;
using System.IO;
using Ionic.Zip;
namespace Ionic.Zip
{
  public class ZipOutputStream : Stream
  {
    public ZipOutputStream(Stream stream) : this(stream, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1uL);
    }
    public ZipOutputStream(String fileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(2uL);
      Stream stream = File.Open(fileName, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
      _Init(stream, false, fileName);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3uL);
    }
    public ZipOutputStream(Stream stream, bool leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(4uL);
      _Init(stream, leaveOpen, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(5uL);
    }
    private void _Init(Stream stream, bool leaveOpen, string name)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(6uL);
      _outputStream = stream.CanRead ? stream : new CountingStream(stream);
      CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
      CompressionMethod = Ionic.Zip.CompressionMethod.Deflate;
      _encryption = EncryptionAlgorithm.None;
      _entriesWritten = new Dictionary<String, ZipEntry>(StringComparer.Ordinal);
      _zip64 = Zip64Option.Never;
      _leaveUnderlyingStreamOpen = leaveOpen;
      Strategy = Ionic.Zlib.CompressionStrategy.Default;
      _name = name ?? "(stream)";
      ParallelDeflateThreshold = -1L;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(7uL);
    }
    public override String ToString()
    {
      String RNTRNTRNT_1 = String.Format("ZipOutputStream::{0}(leaveOpen({1})))", _name, _leaveUnderlyingStreamOpen);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(8uL);
      return RNTRNTRNT_1;
    }
    public String Password {
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(9uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(10uL);
          _exceptionPending = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(11uL);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(12uL);
        _password = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(13uL);
        if (_password == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(14uL);
          _encryption = EncryptionAlgorithm.None;
        } else if (_encryption == EncryptionAlgorithm.None) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(15uL);
          _encryption = EncryptionAlgorithm.PkzipWeak;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(16uL);
      }
    }
    public EncryptionAlgorithm Encryption {
      get {
        EncryptionAlgorithm RNTRNTRNT_2 = _encryption;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(17uL);
        return RNTRNTRNT_2;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(18uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(19uL);
          _exceptionPending = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(20uL);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(21uL);
        if (value == EncryptionAlgorithm.Unsupported) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(22uL);
          _exceptionPending = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(23uL);
          throw new InvalidOperationException("You may not set Encryption to that value.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(24uL);
        _encryption = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(25uL);
      }
    }
    public int CodecBufferSize { get; set; }
    public Ionic.Zlib.CompressionStrategy Strategy { get; set; }
    public ZipEntryTimestamp Timestamp {
      get {
        ZipEntryTimestamp RNTRNTRNT_3 = _timestamp;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(26uL);
        return RNTRNTRNT_3;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(27uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(28uL);
          _exceptionPending = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(29uL);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(30uL);
        _timestamp = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(31uL);
      }
    }
    public Ionic.Zlib.CompressionLevel CompressionLevel { get; set; }
    public Ionic.Zip.CompressionMethod CompressionMethod { get; set; }
    public string Comment {
      get {
        System.String RNTRNTRNT_4 = _comment;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(32uL);
        return RNTRNTRNT_4;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(33uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(34uL);
          _exceptionPending = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(35uL);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(36uL);
        _comment = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(37uL);
      }
    }
    public Zip64Option EnableZip64 {
      get {
        Zip64Option RNTRNTRNT_5 = _zip64;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(38uL);
        return RNTRNTRNT_5;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(39uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(40uL);
          _exceptionPending = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(41uL);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(42uL);
        _zip64 = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(43uL);
      }
    }
    public bool OutputUsedZip64 {
      get {
        System.Boolean RNTRNTRNT_6 = _anyEntriesUsedZip64 || _directoryNeededZip64;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(44uL);
        return RNTRNTRNT_6;
      }
    }
    public bool IgnoreCase {
      get {
        System.Boolean RNTRNTRNT_7 = !_DontIgnoreCase;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(45uL);
        return RNTRNTRNT_7;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(46uL);
        _DontIgnoreCase = !value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(47uL);
      }
    }
    [Obsolete("Beginning with v1.9.1.6 of DotNetZip, this property is obsolete. It will be removed in a future version of the library. Use AlternateEncoding and AlternateEncodingUsage instead.")]
    public bool UseUnicodeAsNecessary {
      get {
        System.Boolean RNTRNTRNT_8 = (_alternateEncoding == System.Text.Encoding.UTF8) && (AlternateEncodingUsage == ZipOption.AsNecessary);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(48uL);
        return RNTRNTRNT_8;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(49uL);
        if (value) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(50uL);
          _alternateEncoding = System.Text.Encoding.UTF8;
          _alternateEncodingUsage = ZipOption.AsNecessary;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(51uL);
          _alternateEncoding = Ionic.Zip.ZipOutputStream.DefaultEncoding;
          _alternateEncodingUsage = ZipOption.Never;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(52uL);
      }
    }
    [Obsolete("use AlternateEncoding and AlternateEncodingUsage instead.")]
    public System.Text.Encoding ProvisionalAlternateEncoding {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(53uL);
        if (_alternateEncodingUsage == ZipOption.AsNecessary) {
          System.Text.Encoding RNTRNTRNT_9 = _alternateEncoding;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(54uL);
          return RNTRNTRNT_9;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(55uL);
        return null;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(56uL);
        _alternateEncoding = value;
        _alternateEncodingUsage = ZipOption.AsNecessary;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(57uL);
      }
    }
    public System.Text.Encoding AlternateEncoding {
      get {
        System.Text.Encoding RNTRNTRNT_10 = _alternateEncoding;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(58uL);
        return RNTRNTRNT_10;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(59uL);
        _alternateEncoding = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(60uL);
      }
    }
    public ZipOption AlternateEncodingUsage {
      get {
        ZipOption RNTRNTRNT_11 = _alternateEncodingUsage;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(61uL);
        return RNTRNTRNT_11;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(62uL);
        _alternateEncodingUsage = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(63uL);
      }
    }
    public static System.Text.Encoding DefaultEncoding {
      get {
        System.Text.Encoding RNTRNTRNT_12 = System.Text.Encoding.GetEncoding("IBM437");
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(64uL);
        return RNTRNTRNT_12;
      }
    }
    public long ParallelDeflateThreshold {
      get {
        System.Int64 RNTRNTRNT_13 = _ParallelDeflateThreshold;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(65uL);
        return RNTRNTRNT_13;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(66uL);
        if ((value != 0) && (value != -1) && (value < 64 * 1024)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(67uL);
          throw new ArgumentOutOfRangeException("value must be greater than 64k, or 0, or -1");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(68uL);
        _ParallelDeflateThreshold = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(69uL);
      }
    }
    public int ParallelDeflateMaxBufferPairs {
      get {
        System.Int32 RNTRNTRNT_14 = _maxBufferPairs;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(70uL);
        return RNTRNTRNT_14;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(71uL);
        if (value < 4) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(72uL);
          throw new ArgumentOutOfRangeException("ParallelDeflateMaxBufferPairs", "Value must be 4 or greater.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(73uL);
        _maxBufferPairs = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(74uL);
      }
    }
    private void InsureUniqueEntry(ZipEntry ze1)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(75uL);
      if (_entriesWritten.ContainsKey(ze1.FileName)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(76uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(77uL);
        throw new ArgumentException(String.Format("The entry '{0}' already exists in the zip archive.", ze1.FileName));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(78uL);
    }
    internal Stream OutputStream {
      get {
        Stream RNTRNTRNT_15 = _outputStream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(79uL);
        return RNTRNTRNT_15;
      }
    }
    internal String Name {
      get {
        String RNTRNTRNT_16 = _name;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(80uL);
        return RNTRNTRNT_16;
      }
    }
    public bool ContainsEntry(string name)
    {
      System.Boolean RNTRNTRNT_17 = _entriesWritten.ContainsKey(SharedUtilities.NormalizePathForUseInZipFile(name));
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(81uL);
      return RNTRNTRNT_17;
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(82uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(83uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(84uL);
        throw new System.InvalidOperationException("The stream has been closed.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(85uL);
      if (buffer == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(86uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(87uL);
        throw new System.ArgumentNullException("buffer");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(88uL);
      if (_currentEntry == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(89uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(90uL);
        throw new System.InvalidOperationException("You must call PutNextEntry() before calling Write().");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(91uL);
      if (_currentEntry.IsDirectory) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(92uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(93uL);
        throw new System.InvalidOperationException("You cannot Write() data for an entry that is a directory.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(94uL);
      if (_needToWriteEntryHeader) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(95uL);
        _InitiateCurrentEntry(false);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(96uL);
      if (count != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(97uL);
        _entryOutputStream.Write(buffer, offset, count);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(98uL);
    }
    public ZipEntry PutNextEntry(String entryName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(99uL);
      if (String.IsNullOrEmpty(entryName)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(100uL);
        throw new ArgumentNullException("entryName");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(101uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(102uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(103uL);
        throw new System.InvalidOperationException("The stream has been closed.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(104uL);
      _FinishCurrentEntry();
      _currentEntry = ZipEntry.CreateForZipOutputStream(entryName);
      _currentEntry._container = new ZipContainer(this);
      _currentEntry._BitField |= 0x8;
      _currentEntry.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
      _currentEntry.CompressionLevel = this.CompressionLevel;
      _currentEntry.CompressionMethod = this.CompressionMethod;
      _currentEntry.Password = _password;
      _currentEntry.Encryption = this.Encryption;
      _currentEntry.AlternateEncoding = this.AlternateEncoding;
      _currentEntry.AlternateEncodingUsage = this.AlternateEncodingUsage;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(105uL);
      if (entryName.EndsWith("/")) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(106uL);
        _currentEntry.MarkAsDirectory();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(107uL);
      _currentEntry.EmitTimesInWindowsFormatWhenSaving = ((_timestamp & ZipEntryTimestamp.Windows) != 0);
      _currentEntry.EmitTimesInUnixFormatWhenSaving = ((_timestamp & ZipEntryTimestamp.Unix) != 0);
      InsureUniqueEntry(_currentEntry);
      _needToWriteEntryHeader = true;
      ZipEntry RNTRNTRNT_18 = _currentEntry;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(108uL);
      return RNTRNTRNT_18;
    }
    private void _InitiateCurrentEntry(bool finishing)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(109uL);
      _entriesWritten.Add(_currentEntry.FileName, _currentEntry);
      _entryCount++;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(110uL);
      if (_entryCount > 65534 && _zip64 == Zip64Option.Never) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(111uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(112uL);
        throw new System.InvalidOperationException("Too many entries. Consider setting ZipOutputStream.EnableZip64.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(113uL);
      _currentEntry.WriteHeader(_outputStream, finishing ? 99 : 0);
      _currentEntry.StoreRelativeOffset();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(114uL);
      if (!_currentEntry.IsDirectory) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(115uL);
        _currentEntry.WriteSecurityMetadata(_outputStream);
        _currentEntry.PrepOutputStream(_outputStream, finishing ? 0 : -1, out _outputCounter, out _encryptor, out _deflater, out _entryOutputStream);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(116uL);
      _needToWriteEntryHeader = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(117uL);
    }
    private void _FinishCurrentEntry()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(118uL);
      if (_currentEntry != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(119uL);
        if (_needToWriteEntryHeader) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(120uL);
          _InitiateCurrentEntry(true);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(121uL);
        _currentEntry.FinishOutputStream(_outputStream, _outputCounter, _encryptor, _deflater, _entryOutputStream);
        _currentEntry.PostProcessOutput(_outputStream);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(122uL);
        if (_currentEntry.OutputUsedZip64 != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(123uL);
          _anyEntriesUsedZip64 |= _currentEntry.OutputUsedZip64.Value;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(124uL);
        _outputCounter = null;
        _encryptor = _deflater = null;
        _entryOutputStream = null;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(125uL);
    }
    protected override void Dispose(bool disposing)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(126uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(127uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(128uL);
      if (disposing) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(129uL);
        if (!_exceptionPending) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(130uL);
          _FinishCurrentEntry();
          _directoryNeededZip64 = ZipOutput.WriteCentralDirectoryStructure(_outputStream, _entriesWritten.Values, 1, _zip64, Comment, new ZipContainer(this));
          Stream wrappedStream = null;
          CountingStream cs = _outputStream as CountingStream;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(131uL);
          if (cs != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(132uL);
            wrappedStream = cs.WrappedStream;
            cs.Dispose();
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(133uL);
            wrappedStream = _outputStream;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(134uL);
          if (!_leaveUnderlyingStreamOpen) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(135uL);
            wrappedStream.Dispose();
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(136uL);
          _outputStream = null;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(137uL);
      _disposed = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(138uL);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_19 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(139uL);
        return RNTRNTRNT_19;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_20 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(140uL);
        return RNTRNTRNT_20;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_21 = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(141uL);
        return RNTRNTRNT_21;
      }
    }
    public override long Length {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(142uL);
        throw new NotSupportedException();
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_22 = _outputStream.Position;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(143uL);
        return RNTRNTRNT_22;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(144uL);
        throw new NotSupportedException();
      }
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(145uL);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(146uL);
      throw new NotSupportedException("Read");
    }
    public override long Seek(long offset, SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(147uL);
      throw new NotSupportedException("Seek");
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(148uL);
      throw new NotSupportedException();
    }
    private EncryptionAlgorithm _encryption;
    private ZipEntryTimestamp _timestamp;
    internal String _password;
    private String _comment;
    private Stream _outputStream;
    private ZipEntry _currentEntry;
    internal Zip64Option _zip64;
    private Dictionary<String, ZipEntry> _entriesWritten;
    private int _entryCount;
    private ZipOption _alternateEncodingUsage = ZipOption.Never;
    private System.Text.Encoding _alternateEncoding = System.Text.Encoding.GetEncoding("IBM437");
    private bool _leaveUnderlyingStreamOpen;
    private bool _disposed;
    private bool _exceptionPending;
    private bool _anyEntriesUsedZip64, _directoryNeededZip64;
    private CountingStream _outputCounter;
    private Stream _encryptor;
    private Stream _deflater;
    private Ionic.Crc.CrcCalculatorStream _entryOutputStream;
    private bool _needToWriteEntryHeader;
    private string _name;
    private bool _DontIgnoreCase;
    internal Ionic.Zlib.ParallelDeflateOutputStream ParallelDeflater;
    private long _ParallelDeflateThreshold;
    private int _maxBufferPairs = 16;
  }
  internal class ZipContainer
  {
    private ZipFile _zf;
    private ZipOutputStream _zos;
    private ZipInputStream _zis;
    public ZipContainer(Object o)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(149uL);
      _zf = (o as ZipFile);
      _zos = (o as ZipOutputStream);
      _zis = (o as ZipInputStream);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(150uL);
    }
    public ZipFile ZipFile {
      get {
        ZipFile RNTRNTRNT_23 = _zf;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(151uL);
        return RNTRNTRNT_23;
      }
    }
    public ZipOutputStream ZipOutputStream {
      get {
        ZipOutputStream RNTRNTRNT_24 = _zos;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(152uL);
        return RNTRNTRNT_24;
      }
    }
    public string Name {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(153uL);
        if (_zf != null) {
          System.String RNTRNTRNT_25 = _zf.Name;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(154uL);
          return RNTRNTRNT_25;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(155uL);
        if (_zis != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(156uL);
          throw new NotSupportedException();
        }
        System.String RNTRNTRNT_26 = _zos.Name;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(157uL);
        return RNTRNTRNT_26;
      }
    }
    public string Password {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(158uL);
        if (_zf != null) {
          System.String RNTRNTRNT_27 = _zf._Password;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(159uL);
          return RNTRNTRNT_27;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(160uL);
        if (_zis != null) {
          System.String RNTRNTRNT_28 = _zis._Password;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(161uL);
          return RNTRNTRNT_28;
        }
        System.String RNTRNTRNT_29 = _zos._password;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(162uL);
        return RNTRNTRNT_29;
      }
    }
    public Zip64Option Zip64 {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(163uL);
        if (_zf != null) {
          Zip64Option RNTRNTRNT_30 = _zf._zip64;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(164uL);
          return RNTRNTRNT_30;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(165uL);
        if (_zis != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(166uL);
          throw new NotSupportedException();
        }
        Zip64Option RNTRNTRNT_31 = _zos._zip64;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(167uL);
        return RNTRNTRNT_31;
      }
    }
    public int BufferSize {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(168uL);
        if (_zf != null) {
          System.Int32 RNTRNTRNT_32 = _zf.BufferSize;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(169uL);
          return RNTRNTRNT_32;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(170uL);
        if (_zis != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(171uL);
          throw new NotSupportedException();
        }
        System.Int32 RNTRNTRNT_33 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(172uL);
        return RNTRNTRNT_33;
      }
    }
    public Ionic.Zlib.ParallelDeflateOutputStream ParallelDeflater {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(173uL);
        if (_zf != null) {
          Ionic.Zlib.ParallelDeflateOutputStream RNTRNTRNT_34 = _zf.ParallelDeflater;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(174uL);
          return RNTRNTRNT_34;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(175uL);
        if (_zis != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(176uL);
          return null;
        }
        Ionic.Zlib.ParallelDeflateOutputStream RNTRNTRNT_35 = _zos.ParallelDeflater;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(177uL);
        return RNTRNTRNT_35;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(178uL);
        if (_zf != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(179uL);
          _zf.ParallelDeflater = value;
        } else if (_zos != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(180uL);
          _zos.ParallelDeflater = value;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(181uL);
      }
    }
    public long ParallelDeflateThreshold {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(182uL);
        if (_zf != null) {
          System.Int64 RNTRNTRNT_36 = _zf.ParallelDeflateThreshold;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(183uL);
          return RNTRNTRNT_36;
        }
        System.Int64 RNTRNTRNT_37 = _zos.ParallelDeflateThreshold;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(184uL);
        return RNTRNTRNT_37;
      }
    }
    public int ParallelDeflateMaxBufferPairs {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(185uL);
        if (_zf != null) {
          System.Int32 RNTRNTRNT_38 = _zf.ParallelDeflateMaxBufferPairs;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(186uL);
          return RNTRNTRNT_38;
        }
        System.Int32 RNTRNTRNT_39 = _zos.ParallelDeflateMaxBufferPairs;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(187uL);
        return RNTRNTRNT_39;
      }
    }
    public int CodecBufferSize {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(188uL);
        if (_zf != null) {
          System.Int32 RNTRNTRNT_40 = _zf.CodecBufferSize;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(189uL);
          return RNTRNTRNT_40;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(190uL);
        if (_zis != null) {
          System.Int32 RNTRNTRNT_41 = _zis.CodecBufferSize;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(191uL);
          return RNTRNTRNT_41;
        }
        System.Int32 RNTRNTRNT_42 = _zos.CodecBufferSize;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(192uL);
        return RNTRNTRNT_42;
      }
    }
    public Ionic.Zlib.CompressionStrategy Strategy {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(193uL);
        if (_zf != null) {
          Ionic.Zlib.CompressionStrategy RNTRNTRNT_43 = _zf.Strategy;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(194uL);
          return RNTRNTRNT_43;
        }
        Ionic.Zlib.CompressionStrategy RNTRNTRNT_44 = _zos.Strategy;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(195uL);
        return RNTRNTRNT_44;
      }
    }
    public Zip64Option UseZip64WhenSaving {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(196uL);
        if (_zf != null) {
          Zip64Option RNTRNTRNT_45 = _zf.UseZip64WhenSaving;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(197uL);
          return RNTRNTRNT_45;
        }
        Zip64Option RNTRNTRNT_46 = _zos.EnableZip64;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(198uL);
        return RNTRNTRNT_46;
      }
    }
    public System.Text.Encoding AlternateEncoding {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(199uL);
        if (_zf != null) {
          System.Text.Encoding RNTRNTRNT_47 = _zf.AlternateEncoding;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(200uL);
          return RNTRNTRNT_47;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(201uL);
        if (_zos != null) {
          System.Text.Encoding RNTRNTRNT_48 = _zos.AlternateEncoding;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(202uL);
          return RNTRNTRNT_48;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(203uL);
        return null;
      }
    }
    public System.Text.Encoding DefaultEncoding {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(204uL);
        if (_zf != null) {
          System.Text.Encoding RNTRNTRNT_49 = ZipFile.DefaultEncoding;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(205uL);
          return RNTRNTRNT_49;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(206uL);
        if (_zos != null) {
          System.Text.Encoding RNTRNTRNT_50 = ZipOutputStream.DefaultEncoding;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(207uL);
          return RNTRNTRNT_50;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(208uL);
        return null;
      }
    }
    public ZipOption AlternateEncodingUsage {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(209uL);
        if (_zf != null) {
          ZipOption RNTRNTRNT_51 = _zf.AlternateEncodingUsage;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(210uL);
          return RNTRNTRNT_51;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(211uL);
        if (_zos != null) {
          ZipOption RNTRNTRNT_52 = _zos.AlternateEncodingUsage;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(212uL);
          return RNTRNTRNT_52;
        }
        ZipOption RNTRNTRNT_53 = ZipOption.Never;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(213uL);
        return RNTRNTRNT_53;
      }
    }
    public Stream ReadStream {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(214uL);
        if (_zf != null) {
          Stream RNTRNTRNT_54 = _zf.ReadStream;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(215uL);
          return RNTRNTRNT_54;
        }
        Stream RNTRNTRNT_55 = _zis.ReadStream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(216uL);
        return RNTRNTRNT_55;
      }
    }
  }
}
