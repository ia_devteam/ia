using System;
namespace Ionic.Zip
{
  static class ZipConstants
  {
    public const UInt32 PackedToRemovableMedia = 0x30304b50;
    public const UInt32 Zip64EndOfCentralDirectoryRecordSignature = 0x6064b50;
    public const UInt32 Zip64EndOfCentralDirectoryLocatorSignature = 0x7064b50;
    public const UInt32 EndOfCentralDirectorySignature = 0x6054b50;
    public const int ZipEntrySignature = 0x4034b50;
    public const int ZipEntryDataDescriptorSignature = 0x8074b50;
    public const int SplitArchiveSignature = 0x8074b50;
    public const int ZipDirEntrySignature = 0x2014b50;
    public const int AesKeySize = 192;
    public const int AesBlockSize = 128;
    public const UInt16 AesAlgId128 = 0x660e;
    public const UInt16 AesAlgId192 = 0x660f;
    public const UInt16 AesAlgId256 = 0x6610;
  }
}
