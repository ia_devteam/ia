using System;
using System.IO;
using Interop = System.Runtime.InteropServices;
namespace Ionic.Zip
{
  [Interop.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d00004")]
  [Interop.ComVisible(true)]
  [Interop.ClassInterface(Interop.ClassInterfaceType.AutoDispatch)]
  public partial class ZipEntry
  {
    public ZipEntry()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1338uL);
      _CompressionMethod = (Int16)CompressionMethod.Deflate;
      _CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
      _Encryption = EncryptionAlgorithm.None;
      _Source = ZipEntrySource.None;
      AlternateEncoding = System.Text.Encoding.GetEncoding("IBM437");
      AlternateEncodingUsage = ZipOption.Never;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1339uL);
    }
    public DateTime LastModified {
      get {
        DateTime RNTRNTRNT_227 = _LastModified.ToLocalTime();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1340uL);
        return RNTRNTRNT_227;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1341uL);
        _LastModified = (value.Kind == DateTimeKind.Unspecified) ? DateTime.SpecifyKind(value, DateTimeKind.Local) : value.ToLocalTime();
        _Mtime = Ionic.Zip.SharedUtilities.AdjustTime_Reverse(_LastModified).ToUniversalTime();
        _metadataChanged = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1342uL);
      }
    }
    private int BufferSize {
      get {
        System.Int32 RNTRNTRNT_228 = this._container.BufferSize;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1343uL);
        return RNTRNTRNT_228;
      }
    }
    public DateTime ModifiedTime {
      get {
        DateTime RNTRNTRNT_229 = _Mtime;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1344uL);
        return RNTRNTRNT_229;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1345uL);
        SetEntryTimes(_Ctime, _Atime, value);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1346uL);
      }
    }
    public DateTime AccessedTime {
      get {
        DateTime RNTRNTRNT_230 = _Atime;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1347uL);
        return RNTRNTRNT_230;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1348uL);
        SetEntryTimes(_Ctime, value, _Mtime);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1349uL);
      }
    }
    public DateTime CreationTime {
      get {
        DateTime RNTRNTRNT_231 = _Ctime;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1350uL);
        return RNTRNTRNT_231;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1351uL);
        SetEntryTimes(value, _Atime, _Mtime);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1352uL);
      }
    }
    public void SetEntryTimes(DateTime created, DateTime accessed, DateTime modified)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1353uL);
      _ntfsTimesAreSet = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1354uL);
      if (created == _zeroHour && created.Kind == _zeroHour.Kind) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1355uL);
        created = _win32Epoch;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1356uL);
      if (accessed == _zeroHour && accessed.Kind == _zeroHour.Kind) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1357uL);
        accessed = _win32Epoch;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1358uL);
      if (modified == _zeroHour && modified.Kind == _zeroHour.Kind) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1359uL);
        modified = _win32Epoch;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1360uL);
      _Ctime = created.ToUniversalTime();
      _Atime = accessed.ToUniversalTime();
      _Mtime = modified.ToUniversalTime();
      _LastModified = _Mtime;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1361uL);
      if (!_emitUnixTimes && !_emitNtfsTimes) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1362uL);
        _emitNtfsTimes = true;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1363uL);
      _metadataChanged = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1364uL);
    }
    public bool EmitTimesInWindowsFormatWhenSaving {
      get {
        System.Boolean RNTRNTRNT_232 = _emitNtfsTimes;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1365uL);
        return RNTRNTRNT_232;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1366uL);
        _emitNtfsTimes = value;
        _metadataChanged = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1367uL);
      }
    }
    public bool EmitTimesInUnixFormatWhenSaving {
      get {
        System.Boolean RNTRNTRNT_233 = _emitUnixTimes;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1368uL);
        return RNTRNTRNT_233;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1369uL);
        _emitUnixTimes = value;
        _metadataChanged = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1370uL);
      }
    }
    public ZipEntryTimestamp Timestamp {
      get {
        ZipEntryTimestamp RNTRNTRNT_234 = _timestamp;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1371uL);
        return RNTRNTRNT_234;
      }
    }
    public System.IO.FileAttributes Attributes {
      get {
        System.IO.FileAttributes RNTRNTRNT_235 = (System.IO.FileAttributes)_ExternalFileAttrs;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1372uL);
        return RNTRNTRNT_235;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1373uL);
        _ExternalFileAttrs = (int)value;
        _VersionMadeBy = (0 << 8) + 45;
        _metadataChanged = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1374uL);
      }
    }
    internal string LocalFileName {
      get {
        System.String RNTRNTRNT_236 = _LocalFileName;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1375uL);
        return RNTRNTRNT_236;
      }
    }
    public string FileName {
      get {
        System.String RNTRNTRNT_237 = _FileNameInArchive;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1376uL);
        return RNTRNTRNT_237;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1377uL);
        if (_container.ZipFile == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1378uL);
          throw new ZipException("Cannot rename; this is not supported in ZipOutputStream/ZipInputStream.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1379uL);
        if (String.IsNullOrEmpty(value)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1380uL);
          throw new ZipException("The FileName must be non empty and non-null.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1381uL);
        var filename = ZipEntry.NameInArchive(value, null);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1382uL);
        if (_FileNameInArchive == filename) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1383uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1384uL);
        this._container.ZipFile.RemoveEntry(this);
        this._container.ZipFile.InternalAddEntry(filename, this);
        _FileNameInArchive = filename;
        _container.ZipFile.NotifyEntryChanged();
        _metadataChanged = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1385uL);
      }
    }
    public Stream InputStream {
      get {
        Stream RNTRNTRNT_238 = _sourceStream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1386uL);
        return RNTRNTRNT_238;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1387uL);
        if (this._Source != ZipEntrySource.Stream) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1388uL);
          throw new ZipException("You must not set the input stream for this entry.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1389uL);
        _sourceWasJitProvided = true;
        _sourceStream = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1390uL);
      }
    }
    public bool InputStreamWasJitProvided {
      get {
        System.Boolean RNTRNTRNT_239 = _sourceWasJitProvided;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1391uL);
        return RNTRNTRNT_239;
      }
    }
    public ZipEntrySource Source {
      get {
        ZipEntrySource RNTRNTRNT_240 = _Source;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1392uL);
        return RNTRNTRNT_240;
      }
    }
    public Int16 VersionNeeded {
      get {
        Int16 RNTRNTRNT_241 = _VersionNeeded;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1393uL);
        return RNTRNTRNT_241;
      }
    }
    public string Comment {
      get {
        System.String RNTRNTRNT_242 = _Comment;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1394uL);
        return RNTRNTRNT_242;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1395uL);
        _Comment = value;
        _metadataChanged = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1396uL);
      }
    }
    public Nullable<bool> RequiresZip64 {
      get {
        Nullable<System.Boolean> RNTRNTRNT_243 = _entryRequiresZip64;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1397uL);
        return RNTRNTRNT_243;
      }
    }
    public Nullable<bool> OutputUsedZip64 {
      get {
        Nullable<System.Boolean> RNTRNTRNT_244 = _OutputUsesZip64;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1398uL);
        return RNTRNTRNT_244;
      }
    }
    public Int16 BitField {
      get {
        Int16 RNTRNTRNT_245 = _BitField;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1399uL);
        return RNTRNTRNT_245;
      }
    }
    public CompressionMethod CompressionMethod {
      get {
        CompressionMethod RNTRNTRNT_246 = (CompressionMethod)_CompressionMethod;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1400uL);
        return RNTRNTRNT_246;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1401uL);
        if (value == (CompressionMethod)_CompressionMethod) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1402uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1403uL);
        if (value != CompressionMethod.None && value != CompressionMethod.Deflate && value != CompressionMethod.BZip2) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1404uL);
          throw new InvalidOperationException("Unsupported compression method.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1405uL);
        _CompressionMethod = (Int16)value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1406uL);
        if (_CompressionMethod == (Int16)Ionic.Zip.CompressionMethod.None) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1407uL);
          _CompressionLevel = Ionic.Zlib.CompressionLevel.None;
        } else if (CompressionLevel == Ionic.Zlib.CompressionLevel.None) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1408uL);
          _CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1409uL);
        if (_container.ZipFile != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1410uL);
          _container.ZipFile.NotifyEntryChanged();
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1411uL);
        _restreamRequiredOnSave = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1412uL);
      }
    }
    public Ionic.Zlib.CompressionLevel CompressionLevel {
      get {
        Ionic.Zlib.CompressionLevel RNTRNTRNT_247 = _CompressionLevel;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1413uL);
        return RNTRNTRNT_247;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1414uL);
        if (_CompressionMethod != (short)CompressionMethod.Deflate && _CompressionMethod != (short)CompressionMethod.None) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1415uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1416uL);
        if (value == Ionic.Zlib.CompressionLevel.Default && _CompressionMethod == (short)CompressionMethod.Deflate) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1417uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1418uL);
        _CompressionLevel = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1419uL);
        if (value == Ionic.Zlib.CompressionLevel.None && _CompressionMethod == (short)CompressionMethod.None) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1420uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1421uL);
        if (_CompressionLevel == Ionic.Zlib.CompressionLevel.None) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1422uL);
          _CompressionMethod = (short)Ionic.Zip.CompressionMethod.None;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1423uL);
          _CompressionMethod = (short)Ionic.Zip.CompressionMethod.Deflate;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1424uL);
        if (_container.ZipFile != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1425uL);
          _container.ZipFile.NotifyEntryChanged();
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1426uL);
        _restreamRequiredOnSave = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1427uL);
      }
    }
    public Int64 CompressedSize {
      get {
        Int64 RNTRNTRNT_248 = _CompressedSize;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1428uL);
        return RNTRNTRNT_248;
      }
    }
    public Int64 UncompressedSize {
      get {
        Int64 RNTRNTRNT_249 = _UncompressedSize;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1429uL);
        return RNTRNTRNT_249;
      }
    }
    public Double CompressionRatio {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1430uL);
        if (UncompressedSize == 0) {
          Double RNTRNTRNT_250 = 0;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1431uL);
          return RNTRNTRNT_250;
        }
        Double RNTRNTRNT_251 = 100 * (1.0 - (1.0 * CompressedSize) / (1.0 * UncompressedSize));
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1432uL);
        return RNTRNTRNT_251;
      }
    }
    public Int32 Crc {
      get {
        Int32 RNTRNTRNT_252 = _Crc32;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1433uL);
        return RNTRNTRNT_252;
      }
    }
    public bool IsDirectory {
      get {
        System.Boolean RNTRNTRNT_253 = _IsDirectory;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1434uL);
        return RNTRNTRNT_253;
      }
    }
    public bool UsesEncryption {
      get {
        System.Boolean RNTRNTRNT_254 = (_Encryption_FromZipFile != EncryptionAlgorithm.None);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1435uL);
        return RNTRNTRNT_254;
      }
    }
    public EncryptionAlgorithm Encryption {
      get {
        EncryptionAlgorithm RNTRNTRNT_255 = _Encryption;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1436uL);
        return RNTRNTRNT_255;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1437uL);
        if (value == _Encryption) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1438uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1439uL);
        if (value == EncryptionAlgorithm.Unsupported) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1440uL);
          throw new InvalidOperationException("You may not set Encryption to that value.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1441uL);
        _Encryption = value;
        _restreamRequiredOnSave = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1442uL);
        if (_container.ZipFile != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1443uL);
          _container.ZipFile.NotifyEntryChanged();
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1444uL);
      }
    }
    public string Password {
      private get {
        System.String RNTRNTRNT_256 = _Password;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1445uL);
        return RNTRNTRNT_256;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1446uL);
        _Password = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1447uL);
        if (_Password == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1448uL);
          _Encryption = EncryptionAlgorithm.None;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1449uL);
          if (this._Source == ZipEntrySource.ZipFile && !_sourceIsEncrypted) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1450uL);
            _restreamRequiredOnSave = true;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1451uL);
          if (Encryption == EncryptionAlgorithm.None) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1452uL);
            _Encryption = EncryptionAlgorithm.PkzipWeak;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1453uL);
      }
    }
    internal bool IsChanged {
      get {
        System.Boolean RNTRNTRNT_257 = _restreamRequiredOnSave | _metadataChanged;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1454uL);
        return RNTRNTRNT_257;
      }
    }
    public ExtractExistingFileAction ExtractExistingFile { get; set; }
    public ZipErrorAction ZipErrorAction { get; set; }
    public bool IncludedInMostRecentSave {
      get {
        System.Boolean RNTRNTRNT_258 = !_skippedDuringSave;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1455uL);
        return RNTRNTRNT_258;
      }
    }
    public SetCompressionCallback SetCompression { get; set; }
    [Obsolete("Beginning with v1.9.1.6 of DotNetZip, this property is obsolete.  It will be removed in a future version of the library. Your applications should  use AlternateEncoding and AlternateEncodingUsage instead.")]
    public bool UseUnicodeAsNecessary {
      get {
        System.Boolean RNTRNTRNT_259 = (AlternateEncoding == System.Text.Encoding.GetEncoding("UTF-8")) && (AlternateEncodingUsage == ZipOption.AsNecessary);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1456uL);
        return RNTRNTRNT_259;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1457uL);
        if (value) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1458uL);
          AlternateEncoding = System.Text.Encoding.GetEncoding("UTF-8");
          AlternateEncodingUsage = ZipOption.AsNecessary;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1459uL);
          AlternateEncoding = Ionic.Zip.ZipFile.DefaultEncoding;
          AlternateEncodingUsage = ZipOption.Never;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1460uL);
      }
    }
    [Obsolete("This property is obsolete since v1.9.1.6. Use AlternateEncoding and AlternateEncodingUsage instead.", true)]
    public System.Text.Encoding ProvisionalAlternateEncoding { get; set; }
    public System.Text.Encoding AlternateEncoding { get; set; }
    public ZipOption AlternateEncodingUsage { get; set; }
    static internal string NameInArchive(String filename, string directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1461uL);
      string result = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1462uL);
      if (directoryPathInArchive == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1463uL);
        result = filename;
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1464uL);
        if (String.IsNullOrEmpty(directoryPathInArchive)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1465uL);
          result = Path.GetFileName(filename);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1466uL);
          result = Path.Combine(directoryPathInArchive, Path.GetFileName(filename));
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1467uL);
      result = SharedUtilities.NormalizePathForUseInZipFile(result);
      System.String RNTRNTRNT_260 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1468uL);
      return RNTRNTRNT_260;
    }
    static internal ZipEntry CreateFromNothing(String nameInArchive)
    {
      ZipEntry RNTRNTRNT_261 = Create(nameInArchive, ZipEntrySource.None, null, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1469uL);
      return RNTRNTRNT_261;
    }
    static internal ZipEntry CreateFromFile(String filename, string nameInArchive)
    {
      ZipEntry RNTRNTRNT_262 = Create(nameInArchive, ZipEntrySource.FileSystem, filename, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1470uL);
      return RNTRNTRNT_262;
    }
    static internal ZipEntry CreateForStream(String entryName, Stream s)
    {
      ZipEntry RNTRNTRNT_263 = Create(entryName, ZipEntrySource.Stream, s, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1471uL);
      return RNTRNTRNT_263;
    }
    static internal ZipEntry CreateForWriter(String entryName, WriteDelegate d)
    {
      ZipEntry RNTRNTRNT_264 = Create(entryName, ZipEntrySource.WriteDelegate, d, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1472uL);
      return RNTRNTRNT_264;
    }
    static internal ZipEntry CreateForJitStreamProvider(string nameInArchive, OpenDelegate opener, CloseDelegate closer)
    {
      ZipEntry RNTRNTRNT_265 = Create(nameInArchive, ZipEntrySource.JitStream, opener, closer);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1473uL);
      return RNTRNTRNT_265;
    }
    static internal ZipEntry CreateForZipOutputStream(string nameInArchive)
    {
      ZipEntry RNTRNTRNT_266 = Create(nameInArchive, ZipEntrySource.ZipOutputStream, null, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1474uL);
      return RNTRNTRNT_266;
    }
    private static ZipEntry Create(string nameInArchive, ZipEntrySource source, Object arg1, Object arg2)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1475uL);
      if (String.IsNullOrEmpty(nameInArchive)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1476uL);
        throw new Ionic.Zip.ZipException("The entry name must be non-null and non-empty.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1477uL);
      ZipEntry entry = new ZipEntry();
      entry._VersionMadeBy = (0 << 8) + 45;
      entry._Source = source;
      entry._Mtime = entry._Atime = entry._Ctime = DateTime.UtcNow;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1478uL);
      if (source == ZipEntrySource.Stream) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1479uL);
        entry._sourceStream = (arg1 as Stream);
      } else if (source == ZipEntrySource.WriteDelegate) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1492uL);
        entry._WriteDelegate = (arg1 as WriteDelegate);
      } else if (source == ZipEntrySource.JitStream) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1491uL);
        entry._OpenDelegate = (arg1 as OpenDelegate);
        entry._CloseDelegate = (arg2 as CloseDelegate);
      } else if (source == ZipEntrySource.ZipOutputStream) {
      } else if (source == ZipEntrySource.None) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1490uL);
        entry._Source = ZipEntrySource.FileSystem;
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1480uL);
        String filename = (arg1 as String);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1481uL);
        if (String.IsNullOrEmpty(filename)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1482uL);
          throw new Ionic.Zip.ZipException("The filename must be non-null and non-empty.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1483uL);
        try {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1484uL);
          entry._Mtime = File.GetLastWriteTime(filename).ToUniversalTime();
          entry._Ctime = File.GetCreationTime(filename).ToUniversalTime();
          entry._Atime = File.GetLastAccessTime(filename).ToUniversalTime();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1485uL);
          if (File.Exists(filename) || Directory.Exists(filename)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1486uL);
            entry._ExternalFileAttrs = (int)File.GetAttributes(filename);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1487uL);
          entry._ntfsTimesAreSet = true;
          entry._LocalFileName = Path.GetFullPath(filename);
        } catch (System.IO.PathTooLongException ptle) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1488uL);
          var msg = String.Format("The path is too long, filename={0}", filename);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1489uL);
          throw new ZipException(msg, ptle);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1493uL);
      entry._LastModified = entry._Mtime;
      entry._FileNameInArchive = SharedUtilities.NormalizePathForUseInZipFile(nameInArchive);
      ZipEntry RNTRNTRNT_267 = entry;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1494uL);
      return RNTRNTRNT_267;
    }
    internal void MarkAsDirectory()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1495uL);
      _IsDirectory = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1496uL);
      if (!_FileNameInArchive.EndsWith("/")) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1497uL);
        _FileNameInArchive += "/";
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1498uL);
    }
    public bool IsText {
      get {
        System.Boolean RNTRNTRNT_268 = _IsText;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1499uL);
        return RNTRNTRNT_268;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1500uL);
        _IsText = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1501uL);
      }
    }
    public override String ToString()
    {
      String RNTRNTRNT_269 = String.Format("ZipEntry::{0}", FileName);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1502uL);
      return RNTRNTRNT_269;
    }
    internal Stream ArchiveStream {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1503uL);
        if (_archiveStream == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1504uL);
          if (_container.ZipFile != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1505uL);
            var zf = _container.ZipFile;
            zf.Reset(false);
            _archiveStream = zf.StreamForDiskNumber(_diskNumber);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1506uL);
            _archiveStream = _container.ZipOutputStream.OutputStream;
          }
        }
        Stream RNTRNTRNT_270 = _archiveStream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1507uL);
        return RNTRNTRNT_270;
      }
    }
    private void SetFdpLoh()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1508uL);
      long origPosition = this.ArchiveStream.Position;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1509uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1510uL);
        this.ArchiveStream.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      } catch (System.IO.IOException exc1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1511uL);
        string description = String.Format("Exception seeking  entry({0}) offset(0x{1:X8}) len(0x{2:X8})", this.FileName, this._RelativeOffsetOfLocalHeader, this.ArchiveStream.Length);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1512uL);
        throw new BadStateException(description, exc1);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1513uL);
      byte[] block = new byte[30];
      this.ArchiveStream.Read(block, 0, block.Length);
      Int16 filenameLength = (short)(block[26] + block[27] * 256);
      Int16 extraFieldLength = (short)(block[28] + block[29] * 256);
      this.ArchiveStream.Seek(filenameLength + extraFieldLength, SeekOrigin.Current);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      this._LengthOfHeader = 30 + extraFieldLength + filenameLength + GetLengthOfCryptoHeaderBytes(_Encryption_FromZipFile);
      this.__FileDataPosition = _RelativeOffsetOfLocalHeader + _LengthOfHeader;
      this.ArchiveStream.Seek(origPosition, SeekOrigin.Begin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1514uL);
    }
    private static int GetKeyStrengthInBits(EncryptionAlgorithm a)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1515uL);
      if (a == EncryptionAlgorithm.WinZipAes256) {
        System.Int32 RNTRNTRNT_271 = 256;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1516uL);
        return RNTRNTRNT_271;
      } else if (a == EncryptionAlgorithm.WinZipAes128) {
        System.Int32 RNTRNTRNT_272 = 128;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1517uL);
        return RNTRNTRNT_272;
      }
      System.Int32 RNTRNTRNT_273 = -1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1518uL);
      return RNTRNTRNT_273;
    }
    static internal int GetLengthOfCryptoHeaderBytes(EncryptionAlgorithm a)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1519uL);
      if (a == EncryptionAlgorithm.None) {
        System.Int32 RNTRNTRNT_274 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1520uL);
        return RNTRNTRNT_274;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1521uL);
      if (a == EncryptionAlgorithm.WinZipAes128 || a == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1522uL);
        int KeyStrengthInBits = GetKeyStrengthInBits(a);
        int sizeOfSaltAndPv = ((KeyStrengthInBits / 8 / 2) + 2);
        System.Int32 RNTRNTRNT_275 = sizeOfSaltAndPv;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1523uL);
        return RNTRNTRNT_275;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1524uL);
      if (a == EncryptionAlgorithm.PkzipWeak) {
        System.Int32 RNTRNTRNT_276 = 12;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1525uL);
        return RNTRNTRNT_276;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1526uL);
      throw new ZipException("internal error");
    }
    internal long FileDataPosition {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1527uL);
        if (__FileDataPosition == -1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1528uL);
          SetFdpLoh();
        }
        System.Int64 RNTRNTRNT_277 = __FileDataPosition;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1529uL);
        return RNTRNTRNT_277;
      }
    }
    private int LengthOfHeader {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1530uL);
        if (_LengthOfHeader == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1531uL);
          SetFdpLoh();
        }
        System.Int32 RNTRNTRNT_278 = _LengthOfHeader;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(1532uL);
        return RNTRNTRNT_278;
      }
    }
    private ZipCrypto _zipCrypto_forExtract;
    private ZipCrypto _zipCrypto_forWrite;
    private WinZipAesCrypto _aesCrypto_forExtract;
    private WinZipAesCrypto _aesCrypto_forWrite;
    private Int16 _WinZipAesMethod;
    internal DateTime _LastModified;
    private DateTime _Mtime, _Atime, _Ctime;
    private bool _ntfsTimesAreSet;
    private bool _emitNtfsTimes = true;
    private bool _emitUnixTimes;
    private bool _TrimVolumeFromFullyQualifiedPaths = true;
    internal string _LocalFileName;
    private string _FileNameInArchive;
    internal Int16 _VersionNeeded;
    internal Int16 _BitField;
    internal Int16 _CompressionMethod;
    private Int16 _CompressionMethod_FromZipFile;
    private Ionic.Zlib.CompressionLevel _CompressionLevel;
    internal string _Comment;
    private bool _IsDirectory;
    private byte[] _CommentBytes;
    internal Int64 _CompressedSize;
    internal Int64 _CompressedFileDataSize;
    internal Int64 _UncompressedSize;
    internal Int32 _TimeBlob;
    private bool _crcCalculated;
    internal Int32 _Crc32;
    internal byte[] _Extra;
    private bool _metadataChanged;
    private bool _restreamRequiredOnSave;
    private bool _sourceIsEncrypted;
    private bool _skippedDuringSave;
    private UInt32 _diskNumber;
    private static System.Text.Encoding ibm437 = System.Text.Encoding.GetEncoding("IBM437");
    private System.Text.Encoding _actualEncoding;
    internal ZipContainer _container;
    private long __FileDataPosition = -1;
    private byte[] _EntryHeader;
    internal Int64 _RelativeOffsetOfLocalHeader;
    private Int64 _future_ROLH;
    private Int64 _TotalEntrySize;
    private int _LengthOfHeader;
    private int _LengthOfTrailer;
    internal bool _InputUsesZip64;
    private UInt32 _UnsupportedAlgorithmId;
    internal string _Password;
    internal ZipEntrySource _Source;
    internal EncryptionAlgorithm _Encryption;
    internal EncryptionAlgorithm _Encryption_FromZipFile;
    internal byte[] _WeakEncryptionHeader;
    internal Stream _archiveStream;
    private Stream _sourceStream;
    private Nullable<Int64> _sourceStreamOriginalPosition;
    private bool _sourceWasJitProvided;
    private bool _ioOperationCanceled;
    private bool _presumeZip64;
    private Nullable<bool> _entryRequiresZip64;
    private Nullable<bool> _OutputUsesZip64;
    private bool _IsText;
    private ZipEntryTimestamp _timestamp;
    private static System.DateTime _unixEpoch = new System.DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    private static System.DateTime _win32Epoch = System.DateTime.FromFileTimeUtc(0L);
    private static System.DateTime _zeroHour = new System.DateTime(1, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    private WriteDelegate _WriteDelegate;
    private OpenDelegate _OpenDelegate;
    private CloseDelegate _CloseDelegate;
  }
  [Flags()]
  public enum ZipEntryTimestamp
  {
    None = 0,
    DOS = 1,
    Windows = 2,
    Unix = 4,
    InfoZip1 = 8
  }
  public enum CompressionMethod
  {
    None = 0,
    Deflate = 8,
    BZip2 = 12
  }
}
