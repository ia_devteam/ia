using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public class ReadOptions
  {
    public EventHandler<ReadProgressEventArgs> ReadProgress { get; set; }
    public TextWriter StatusMessageWriter { get; set; }
    public System.Text.Encoding Encoding { get; set; }
  }
  public partial class ZipFile
  {
    public static ZipFile Read(string fileName)
    {
      ZipFile RNTRNTRNT_427 = ZipFile.Read(fileName, null, null, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3035uL);
      return RNTRNTRNT_427;
    }
    public static ZipFile Read(string fileName, ReadOptions options)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3036uL);
      if (options == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3037uL);
        throw new ArgumentNullException("options");
      }
      ZipFile RNTRNTRNT_428 = Read(fileName, options.StatusMessageWriter, options.Encoding, options.ReadProgress);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3038uL);
      return RNTRNTRNT_428;
    }
    private static ZipFile Read(string fileName, TextWriter statusMessageWriter, System.Text.Encoding encoding, EventHandler<ReadProgressEventArgs> readProgress)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3039uL);
      ZipFile zf = new ZipFile();
      zf.AlternateEncoding = encoding ?? DefaultEncoding;
      zf.AlternateEncodingUsage = ZipOption.Always;
      zf._StatusMessageTextWriter = statusMessageWriter;
      zf._name = fileName;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3040uL);
      if (readProgress != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3041uL);
        zf.ReadProgress = readProgress;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3042uL);
      if (zf.Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3043uL);
        zf._StatusMessageTextWriter.WriteLine("reading from {0}...", fileName);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3044uL);
      ReadIntoInstance(zf);
      zf._fileAlreadyExists = true;
      ZipFile RNTRNTRNT_429 = zf;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3045uL);
      return RNTRNTRNT_429;
    }
    public static ZipFile Read(Stream zipStream)
    {
      ZipFile RNTRNTRNT_430 = Read(zipStream, null, null, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3046uL);
      return RNTRNTRNT_430;
    }
    public static ZipFile Read(Stream zipStream, ReadOptions options)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3047uL);
      if (options == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3048uL);
        throw new ArgumentNullException("options");
      }
      ZipFile RNTRNTRNT_431 = Read(zipStream, options.StatusMessageWriter, options.Encoding, options.ReadProgress);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3049uL);
      return RNTRNTRNT_431;
    }
    private static ZipFile Read(Stream zipStream, TextWriter statusMessageWriter, System.Text.Encoding encoding, EventHandler<ReadProgressEventArgs> readProgress)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3050uL);
      if (zipStream == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3051uL);
        throw new ArgumentNullException("zipStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3052uL);
      ZipFile zf = new ZipFile();
      zf._StatusMessageTextWriter = statusMessageWriter;
      zf._alternateEncoding = encoding ?? ZipFile.DefaultEncoding;
      zf._alternateEncodingUsage = ZipOption.Always;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3053uL);
      if (readProgress != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3054uL);
        zf.ReadProgress += readProgress;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3055uL);
      zf._readstream = (zipStream.Position == 0L) ? zipStream : new OffsetStream(zipStream);
      zf._ReadStreamIsOurs = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3056uL);
      if (zf.Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3057uL);
        zf._StatusMessageTextWriter.WriteLine("reading from stream...");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3058uL);
      ReadIntoInstance(zf);
      ZipFile RNTRNTRNT_432 = zf;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3059uL);
      return RNTRNTRNT_432;
    }
    private static void ReadIntoInstance(ZipFile zf)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3060uL);
      Stream s = zf.ReadStream;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3061uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3062uL);
        zf._readName = zf._name;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3063uL);
        if (!s.CanSeek) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3064uL);
          ReadIntoInstance_Orig(zf);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3065uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3066uL);
        zf.OnReadStarted();
        uint datum = ReadFirstFourBytes(s);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3067uL);
        if (datum == ZipConstants.EndOfCentralDirectorySignature) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3068uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3069uL);
        int nTries = 0;
        bool success = false;
        long posn = s.Length - 64;
        long maxSeekback = Math.Max(s.Length - 0x4000, 10);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3070uL);
        do {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3071uL);
          if (posn < 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3072uL);
            posn = 0;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3073uL);
          s.Seek(posn, SeekOrigin.Begin);
          long bytesRead = SharedUtilities.FindSignature(s, (int)ZipConstants.EndOfCentralDirectorySignature);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3074uL);
          if (bytesRead != -1) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3075uL);
            success = true;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3076uL);
            if (posn == 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3077uL);
              break;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3078uL);
            nTries++;
            posn -= (32 * (nTries + 1) * nTries);
          }
        } while (!success && posn > maxSeekback);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3079uL);
        if (success) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3080uL);
          zf._locEndOfCDS = s.Position - 4;
          byte[] block = new byte[16];
          s.Read(block, 0, block.Length);
          zf._diskNumberWithCd = BitConverter.ToUInt16(block, 2);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3081uL);
          if (zf._diskNumberWithCd == 0xffff) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3082uL);
            throw new ZipException("Spanned archives with more than 65534 segments are not supported at this time.");
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3083uL);
          zf._diskNumberWithCd++;
          int i = 12;
          uint offset32 = (uint)BitConverter.ToUInt32(block, i);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3084uL);
          if (offset32 == 0xffffffffu) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3085uL);
            Zip64SeekToCentralDirectory(zf);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3086uL);
            zf._OffsetOfCentralDirectory = offset32;
            s.Seek(offset32, SeekOrigin.Begin);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3087uL);
          ReadCentralDirectory(zf);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3088uL);
          s.Seek(0L, SeekOrigin.Begin);
          ReadIntoInstance_Orig(zf);
        }
      } catch (Exception ex1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3089uL);
        if (zf._ReadStreamIsOurs && zf._readstream != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3090uL);
          try {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3091uL);
            zf._readstream.Dispose();
            zf._readstream = null;
          } finally {
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3092uL);
        throw new ZipException("Cannot read that as a ZipFile", ex1);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3093uL);
      zf._contentsChanged = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3094uL);
    }
    private static void Zip64SeekToCentralDirectory(ZipFile zf)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3095uL);
      Stream s = zf.ReadStream;
      byte[] block = new byte[16];
      s.Seek(-40, SeekOrigin.Current);
      s.Read(block, 0, 16);
      Int64 offset64 = BitConverter.ToInt64(block, 8);
      zf._OffsetOfCentralDirectory = 0xffffffffu;
      zf._OffsetOfCentralDirectory64 = offset64;
      s.Seek(offset64, SeekOrigin.Begin);
      uint datum = (uint)Ionic.Zip.SharedUtilities.ReadInt(s);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3096uL);
      if (datum != ZipConstants.Zip64EndOfCentralDirectoryRecordSignature) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3097uL);
        throw new BadReadException(String.Format("  Bad signature (0x{0:X8}) looking for ZIP64 EoCD Record at position 0x{1:X8}", datum, s.Position));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3098uL);
      s.Read(block, 0, 8);
      Int64 Size = BitConverter.ToInt64(block, 0);
      block = new byte[Size];
      s.Read(block, 0, block.Length);
      offset64 = BitConverter.ToInt64(block, 36);
      s.Seek(offset64, SeekOrigin.Begin);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3099uL);
    }
    private static uint ReadFirstFourBytes(Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3100uL);
      uint datum = (uint)Ionic.Zip.SharedUtilities.ReadInt(s);
      System.UInt32 RNTRNTRNT_433 = datum;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3101uL);
      return RNTRNTRNT_433;
    }
    private static void ReadCentralDirectory(ZipFile zf)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3102uL);
      bool inputUsesZip64 = false;
      ZipEntry de;
      var previouslySeen = new Dictionary<String, object>();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3103uL);
      while ((de = ZipEntry.ReadDirEntry(zf, previouslySeen)) != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3104uL);
        de.ResetDirEntry();
        zf.OnReadEntry(true, null);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3105uL);
        if (zf.Verbose) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3106uL);
          zf.StatusMessageTextWriter.WriteLine("entry {0}", de.FileName);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3107uL);
        zf._entries.Add(de.FileName, de);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3108uL);
        if (de._InputUsesZip64) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3109uL);
          inputUsesZip64 = true;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3110uL);
        previouslySeen.Add(de.FileName, null);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3111uL);
      if (inputUsesZip64) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3112uL);
        zf.UseZip64WhenSaving = Zip64Option.Always;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3113uL);
      if (zf._locEndOfCDS > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3114uL);
        zf.ReadStream.Seek(zf._locEndOfCDS, SeekOrigin.Begin);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3115uL);
      ReadCentralDirectoryFooter(zf);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3116uL);
      if (zf.Verbose && !String.IsNullOrEmpty(zf.Comment)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3117uL);
        zf.StatusMessageTextWriter.WriteLine("Zip file Comment: {0}", zf.Comment);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3118uL);
      if (zf.Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3119uL);
        zf.StatusMessageTextWriter.WriteLine("read in {0} entries.", zf._entries.Count);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3120uL);
      zf.OnReadCompleted();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3121uL);
    }
    private static void ReadIntoInstance_Orig(ZipFile zf)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3122uL);
      zf.OnReadStarted();
      zf._entries = new System.Collections.Generic.Dictionary<String, ZipEntry>();
      ZipEntry e;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3123uL);
      if (zf.Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3124uL);
        if (zf.Name == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3125uL);
          zf.StatusMessageTextWriter.WriteLine("Reading zip from stream...");
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3126uL);
          zf.StatusMessageTextWriter.WriteLine("Reading zip {0}...", zf.Name);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3127uL);
      bool firstEntry = true;
      ZipContainer zc = new ZipContainer(zf);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3128uL);
      while ((e = ZipEntry.ReadEntry(zc, firstEntry)) != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3129uL);
        if (zf.Verbose) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3130uL);
          zf.StatusMessageTextWriter.WriteLine("  {0}", e.FileName);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3131uL);
        zf._entries.Add(e.FileName, e);
        firstEntry = false;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3132uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3133uL);
        ZipEntry de;
        var previouslySeen = new Dictionary<String, Object>();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3134uL);
        while ((de = ZipEntry.ReadDirEntry(zf, previouslySeen)) != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3135uL);
          ZipEntry e1 = zf._entries[de.FileName];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3136uL);
          if (e1 != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3137uL);
            e1._Comment = de.Comment;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3138uL);
            if (de.IsDirectory) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3139uL);
              e1.MarkAsDirectory();
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3140uL);
          previouslySeen.Add(de.FileName, null);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3141uL);
        if (zf._locEndOfCDS > 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3142uL);
          zf.ReadStream.Seek(zf._locEndOfCDS, SeekOrigin.Begin);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3143uL);
        ReadCentralDirectoryFooter(zf);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3144uL);
        if (zf.Verbose && !String.IsNullOrEmpty(zf.Comment)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3145uL);
          zf.StatusMessageTextWriter.WriteLine("Zip file Comment: {0}", zf.Comment);
        }
      } catch (ZipException) {
      } catch (IOException) {
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3146uL);
      zf.OnReadCompleted();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3147uL);
    }
    private static void ReadCentralDirectoryFooter(ZipFile zf)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3148uL);
      Stream s = zf.ReadStream;
      int signature = Ionic.Zip.SharedUtilities.ReadSignature(s);
      byte[] block = null;
      int j = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3149uL);
      if (signature == ZipConstants.Zip64EndOfCentralDirectoryRecordSignature) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3150uL);
        block = new byte[8 + 44];
        s.Read(block, 0, block.Length);
        Int64 DataSize = BitConverter.ToInt64(block, 0);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3151uL);
        if (DataSize < 44) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3152uL);
          throw new ZipException("Bad size in the ZIP64 Central Directory.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3153uL);
        zf._versionMadeBy = BitConverter.ToUInt16(block, j);
        j += 2;
        zf._versionNeededToExtract = BitConverter.ToUInt16(block, j);
        j += 2;
        zf._diskNumberWithCd = BitConverter.ToUInt32(block, j);
        j += 2;
        block = new byte[DataSize - 44];
        s.Read(block, 0, block.Length);
        signature = Ionic.Zip.SharedUtilities.ReadSignature(s);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3154uL);
        if (signature != ZipConstants.Zip64EndOfCentralDirectoryLocatorSignature) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3155uL);
          throw new ZipException("Inconsistent metadata in the ZIP64 Central Directory.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3156uL);
        block = new byte[16];
        s.Read(block, 0, block.Length);
        signature = Ionic.Zip.SharedUtilities.ReadSignature(s);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3157uL);
      if (signature != ZipConstants.EndOfCentralDirectorySignature) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3158uL);
        s.Seek(-4, SeekOrigin.Current);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3159uL);
        throw new BadReadException(String.Format("Bad signature ({0:X8}) at position 0x{1:X8}", signature, s.Position));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3160uL);
      block = new byte[16];
      zf.ReadStream.Read(block, 0, block.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3161uL);
      if (zf._diskNumberWithCd == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3162uL);
        zf._diskNumberWithCd = BitConverter.ToUInt16(block, 2);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3163uL);
      ReadZipFileComment(zf);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3164uL);
    }
    private static void ReadZipFileComment(ZipFile zf)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3165uL);
      byte[] block = new byte[2];
      zf.ReadStream.Read(block, 0, block.Length);
      Int16 commentLength = (short)(block[0] + block[1] * 256);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3166uL);
      if (commentLength > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3167uL);
        block = new byte[commentLength];
        zf.ReadStream.Read(block, 0, block.Length);
        string s1 = zf.AlternateEncoding.GetString(block, 0, block.Length);
        zf.Comment = s1;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3168uL);
    }
    public static bool IsZipFile(string fileName)
    {
      System.Boolean RNTRNTRNT_434 = IsZipFile(fileName, false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3169uL);
      return RNTRNTRNT_434;
    }
    public static bool IsZipFile(string fileName, bool testExtract)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3170uL);
      bool result = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3171uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3172uL);
        if (!File.Exists(fileName)) {
          System.Boolean RNTRNTRNT_435 = false;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3173uL);
          return RNTRNTRNT_435;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3174uL);
        using (var s = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3175uL);
          result = IsZipFile(s, testExtract);
        }
      } catch (IOException) {
      } catch (ZipException) {
      }
      System.Boolean RNTRNTRNT_436 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3176uL);
      return RNTRNTRNT_436;
    }
    public static bool IsZipFile(Stream stream, bool testExtract)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3177uL);
      if (stream == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3178uL);
        throw new ArgumentNullException("stream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3179uL);
      bool result = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3180uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3181uL);
        if (!stream.CanRead) {
          System.Boolean RNTRNTRNT_437 = false;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3182uL);
          return RNTRNTRNT_437;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3183uL);
        var bitBucket = Stream.Null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3184uL);
        using (ZipFile zip1 = ZipFile.Read(stream, null, null, null)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3185uL);
          if (testExtract) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3186uL);
            foreach (var e in zip1) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3187uL);
              if (!e.IsDirectory) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3188uL);
                e.Extract(bitBucket);
              }
            }
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3189uL);
        result = true;
      } catch (IOException) {
      } catch (ZipException) {
      }
      System.Boolean RNTRNTRNT_438 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(3190uL);
      return RNTRNTRNT_438;
    }
  }
}
