using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;
namespace Ionic.BZip2
{
  internal class WorkItem
  {
    public int index;
    public BZip2Compressor Compressor { get; private set; }
    public MemoryStream ms;
    public int ordinal;
    public BitWriter bw;
    public WorkItem(int ix, int blockSize)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504498uL);
      this.ms = new MemoryStream();
      this.bw = new BitWriter(ms);
      this.Compressor = new BZip2Compressor(bw, blockSize);
      this.index = ix;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504499uL);
    }
  }
  public class ParallelBZip2OutputStream : System.IO.Stream
  {
    private static readonly int BufferPairsPerCore = 4;
    private int _maxWorkers;
    private bool firstWriteDone;
    private int lastFilled;
    private int lastWritten;
    private int latestCompressed;
    private int currentlyFilling;
    private volatile Exception pendingException;
    private bool handlingException;
    private bool emitting;
    private System.Collections.Generic.Queue<int> toWrite;
    private System.Collections.Generic.Queue<int> toFill;
    private System.Collections.Generic.List<WorkItem> pool;
    private object latestLock = new object();
    private object eLock = new object();
    private object outputLock = new object();
    private AutoResetEvent newlyCompressedBlob;
    long totalBytesWrittenIn;
    long totalBytesWrittenOut;
    bool leaveOpen;
    uint combinedCRC;
    Stream output;
    BitWriter bw;
    int blockSize100k;
    private TraceBits desiredTrace = TraceBits.Crc | TraceBits.Write;
    public ParallelBZip2OutputStream(Stream output) : this(output, BZip2.MaxBlockSize, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504500uL);
    }
    public ParallelBZip2OutputStream(Stream output, int blockSize) : this(output, blockSize, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504501uL);
    }
    public ParallelBZip2OutputStream(Stream output, bool leaveOpen) : this(output, BZip2.MaxBlockSize, leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504502uL);
    }
    public ParallelBZip2OutputStream(Stream output, int blockSize, bool leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504503uL);
      if (blockSize < BZip2.MinBlockSize || blockSize > BZip2.MaxBlockSize) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504504uL);
        var msg = String.Format("blockSize={0} is out of range; must be between {1} and {2}", blockSize, BZip2.MinBlockSize, BZip2.MaxBlockSize);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504505uL);
        throw new ArgumentException(msg, "blockSize");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504506uL);
      this.output = output;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504507uL);
      if (!this.output.CanWrite) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504508uL);
        throw new ArgumentException("The stream is not writable.", "output");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504509uL);
      this.bw = new BitWriter(this.output);
      this.blockSize100k = blockSize;
      this.leaveOpen = leaveOpen;
      this.combinedCRC = 0;
      this.MaxWorkers = 16;
      EmitHeader();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504510uL);
    }
    private void InitializePoolOfWorkItems()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504511uL);
      this.toWrite = new Queue<int>();
      this.toFill = new Queue<int>();
      this.pool = new System.Collections.Generic.List<WorkItem>();
      int nWorkers = BufferPairsPerCore * Environment.ProcessorCount;
      nWorkers = Math.Min(nWorkers, this.MaxWorkers);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504512uL);
      for (int i = 0; i < nWorkers; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504513uL);
        this.pool.Add(new WorkItem(i, this.blockSize100k));
        this.toFill.Enqueue(i);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504514uL);
      this.newlyCompressedBlob = new AutoResetEvent(false);
      this.currentlyFilling = -1;
      this.lastFilled = -1;
      this.lastWritten = -1;
      this.latestCompressed = -1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504515uL);
    }
    public int MaxWorkers {
      get {
        System.Int32 RNTRNTRNT_505 = _maxWorkers;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504516uL);
        return RNTRNTRNT_505;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504517uL);
        if (value < 4) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504518uL);
          throw new ArgumentException("MaxWorkers", "Value must be 4 or greater.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504519uL);
        _maxWorkers = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504520uL);
      }
    }
    public override void Close()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504521uL);
      if (this.pendingException != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504522uL);
        this.handlingException = true;
        var pe = this.pendingException;
        this.pendingException = null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504523uL);
        throw pe;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504524uL);
      if (this.handlingException) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504525uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504526uL);
      if (output == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504527uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504528uL);
      Stream o = this.output;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504529uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504530uL);
        FlushOutput(true);
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504531uL);
        this.output = null;
        this.bw = null;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504532uL);
      if (!leaveOpen) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504533uL);
        o.Close();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504534uL);
    }
    private void FlushOutput(bool lastInput)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504535uL);
      if (this.emitting) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504536uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504537uL);
      if (this.currentlyFilling >= 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504538uL);
        WorkItem workitem = this.pool[this.currentlyFilling];
        CompressOne(workitem);
        this.currentlyFilling = -1;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504539uL);
      if (lastInput) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504540uL);
        EmitPendingBuffers(true, false);
        EmitTrailer();
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504541uL);
        EmitPendingBuffers(false, false);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504542uL);
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504543uL);
      if (this.output != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504544uL);
        FlushOutput(false);
        this.bw.Flush();
        this.output.Flush();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504545uL);
    }
    private void EmitHeader()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504546uL);
      var magic = new byte[] {
        (byte)'B',
        (byte)'Z',
        (byte)'h',
        (byte)('0' + this.blockSize100k)
      };
      this.output.Write(magic, 0, magic.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504547uL);
    }
    private void EmitTrailer()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504548uL);
      TraceOutput(TraceBits.Write, "total written out: {0} (0x{0:X})", this.bw.TotalBytesWrittenOut);
      this.bw.WriteByte(0x17);
      this.bw.WriteByte(0x72);
      this.bw.WriteByte(0x45);
      this.bw.WriteByte(0x38);
      this.bw.WriteByte(0x50);
      this.bw.WriteByte(0x90);
      this.bw.WriteInt(this.combinedCRC);
      this.bw.FinishAndPad();
      TraceOutput(TraceBits.Write, "final total : {0} (0x{0:X})", this.bw.TotalBytesWrittenOut);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504549uL);
    }
    public int BlockSize {
      get {
        System.Int32 RNTRNTRNT_506 = this.blockSize100k;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504550uL);
        return RNTRNTRNT_506;
      }
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504551uL);
      bool mustWait = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504552uL);
      if (this.output == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504553uL);
        throw new IOException("the stream is not open");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504554uL);
      if (this.pendingException != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504555uL);
        this.handlingException = true;
        var pe = this.pendingException;
        this.pendingException = null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504556uL);
        throw pe;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504557uL);
      if (offset < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504558uL);
        throw new IndexOutOfRangeException(String.Format("offset ({0}) must be > 0", offset));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504559uL);
      if (count < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504560uL);
        throw new IndexOutOfRangeException(String.Format("count ({0}) must be > 0", count));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504561uL);
      if (offset + count > buffer.Length) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504562uL);
        throw new IndexOutOfRangeException(String.Format("offset({0}) count({1}) bLength({2})", offset, count, buffer.Length));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504563uL);
      if (count == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504564uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504565uL);
      if (!this.firstWriteDone) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504566uL);
        InitializePoolOfWorkItems();
        this.firstWriteDone = true;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504567uL);
      int bytesWritten = 0;
      int bytesRemaining = count;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504568uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504569uL);
        EmitPendingBuffers(false, mustWait);
        mustWait = false;
        int ix = -1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504570uL);
        if (this.currentlyFilling >= 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504571uL);
          ix = this.currentlyFilling;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504572uL);
          if (this.toFill.Count == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504573uL);
            mustWait = true;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504574uL);
            continue;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504575uL);
          ix = this.toFill.Dequeue();
          ++this.lastFilled;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504576uL);
        WorkItem workitem = this.pool[ix];
        workitem.ordinal = this.lastFilled;
        int n = workitem.Compressor.Fill(buffer, offset, bytesRemaining);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504577uL);
        if (n != bytesRemaining) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504578uL);
          if (!ThreadPool.QueueUserWorkItem(CompressOne, workitem)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504579uL);
            throw new Exception("Cannot enqueue workitem");
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504580uL);
          this.currentlyFilling = -1;
          offset += n;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504581uL);
          this.currentlyFilling = ix;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504582uL);
        bytesRemaining -= n;
        bytesWritten += n;
      } while (bytesRemaining > 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504583uL);
      totalBytesWrittenIn += bytesWritten;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504584uL);
      return;
    }
    private void EmitPendingBuffers(bool doAll, bool mustWait)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504585uL);
      if (emitting) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504586uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504587uL);
      emitting = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504588uL);
      if (doAll || mustWait) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504589uL);
        this.newlyCompressedBlob.WaitOne();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504590uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504591uL);
        int firstSkip = -1;
        int millisecondsToWait = doAll ? 200 : (mustWait ? -1 : 0);
        int nextToWrite = -1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504592uL);
        do {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504593uL);
          if (Monitor.TryEnter(this.toWrite, millisecondsToWait)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504594uL);
            nextToWrite = -1;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504595uL);
            try {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504596uL);
              if (this.toWrite.Count > 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504597uL);
                nextToWrite = this.toWrite.Dequeue();
              }
            } finally {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504598uL);
              Monitor.Exit(this.toWrite);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504599uL);
            if (nextToWrite >= 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504600uL);
              WorkItem workitem = this.pool[nextToWrite];
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504601uL);
              if (workitem.ordinal != this.lastWritten + 1) {
                lock (this.toWrite) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504602uL);
                  this.toWrite.Enqueue(nextToWrite);
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504603uL);
                if (firstSkip == nextToWrite) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504604uL);
                  this.newlyCompressedBlob.WaitOne();
                  firstSkip = -1;
                } else if (firstSkip == -1) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504605uL);
                  firstSkip = nextToWrite;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504606uL);
                continue;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504607uL);
              firstSkip = -1;
              TraceOutput(TraceBits.Write, "Writing block {0}", workitem.ordinal);
              var bw2 = workitem.bw;
              bw2.Flush();
              var ms = workitem.ms;
              ms.Seek(0, SeekOrigin.Begin);
              int n;
              int y = -1;
              long totOut = 0;
              var buffer = new byte[1024];
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504608uL);
              while ((n = ms.Read(buffer, 0, buffer.Length)) > 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504609uL);
                y = n;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504610uL);
                for (int k = 0; k < n; k++) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504611uL);
                  this.bw.WriteByte(buffer[k]);
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504612uL);
                totOut += n;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504613uL);
              TraceOutput(TraceBits.Write, " remaining bits: {0} 0x{1:X}", bw2.NumRemainingBits, bw2.RemainingBits);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504614uL);
              if (bw2.NumRemainingBits > 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504615uL);
                this.bw.WriteBits(bw2.NumRemainingBits, bw2.RemainingBits);
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504616uL);
              TraceOutput(TraceBits.Crc, " combined CRC (before): {0:X8}", this.combinedCRC);
              this.combinedCRC = (this.combinedCRC << 1) | (this.combinedCRC >> 31);
              this.combinedCRC ^= (uint)workitem.Compressor.Crc32;
              TraceOutput(TraceBits.Crc, " block    CRC         : {0:X8}", workitem.Compressor.Crc32);
              TraceOutput(TraceBits.Crc, " combined CRC (after) : {0:X8}", this.combinedCRC);
              TraceOutput(TraceBits.Write, "total written out: {0} (0x{0:X})", this.bw.TotalBytesWrittenOut);
              TraceOutput(TraceBits.Write | TraceBits.Crc, "");
              this.totalBytesWrittenOut += totOut;
              bw2.Reset();
              this.lastWritten = workitem.ordinal;
              workitem.ordinal = -1;
              this.toFill.Enqueue(workitem.index);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504617uL);
              if (millisecondsToWait == -1) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504618uL);
                millisecondsToWait = 0;
              }
            }
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504619uL);
            nextToWrite = -1;
          }
        } while (nextToWrite >= 0);
      } while (doAll && (this.lastWritten != this.latestCompressed));
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504620uL);
      if (doAll) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504621uL);
        TraceOutput(TraceBits.Crc, " combined CRC (final) : {0:X8}", this.combinedCRC);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504622uL);
      emitting = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504623uL);
    }
    private void CompressOne(Object wi)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504624uL);
      WorkItem workitem = (WorkItem)wi;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504625uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504626uL);
        workitem.Compressor.CompressAndWrite();
        lock (this.latestLock) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504627uL);
          if (workitem.ordinal > this.latestCompressed) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504628uL);
            this.latestCompressed = workitem.ordinal;
          }
        }
        lock (this.toWrite) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504629uL);
          this.toWrite.Enqueue(workitem.index);
        }
        this.newlyCompressedBlob.Set();
      } catch (System.Exception exc1) {
        lock (this.eLock) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504630uL);
          if (this.pendingException != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504631uL);
            this.pendingException = exc1;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504632uL);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_507 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504633uL);
        return RNTRNTRNT_507;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_508 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504634uL);
        return RNTRNTRNT_508;
      }
    }
    public override bool CanWrite {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504635uL);
        if (this.output == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504636uL);
          throw new ObjectDisposedException("BZip2Stream");
        }
        System.Boolean RNTRNTRNT_509 = this.output.CanWrite;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504637uL);
        return RNTRNTRNT_509;
      }
    }
    public override long Length {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504638uL);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_510 = this.totalBytesWrittenIn;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504639uL);
        return RNTRNTRNT_510;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504640uL);
        throw new NotImplementedException();
      }
    }
    public Int64 BytesWrittenOut {
      get {
        Int64 RNTRNTRNT_511 = totalBytesWrittenOut;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504641uL);
        return RNTRNTRNT_511;
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504642uL);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504643uL);
      throw new NotImplementedException();
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504644uL);
      throw new NotImplementedException();
    }
    [Flags()]
    enum TraceBits : uint
    {
      None = 0,
      Crc = 1,
      Write = 2,
      All = 0xffffffffu
    }
    [System.Diagnostics.ConditionalAttribute("Trace")]
    private void TraceOutput(TraceBits bits, string format, params object[] varParams)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504645uL);
      if ((bits & this.desiredTrace) != 0) {
        lock (outputLock) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504646uL);
          int tid = Thread.CurrentThread.GetHashCode();
          Console.ForegroundColor = (ConsoleColor)(tid % 8 + 10);
          Console.Write("{0:000} PBOS ", tid);
          Console.WriteLine(format, varParams);
          Console.ResetColor();
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504647uL);
    }
  }
}
