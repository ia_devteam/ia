using System;
using System.IO;
namespace Ionic.BZip2
{
  internal class BitWriter
  {
    uint accumulator;
    int nAccumulatedBits;
    Stream output;
    int totalBytesWrittenOut;
    public BitWriter(Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503609uL);
      this.output = s;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503610uL);
    }
    public byte RemainingBits {
      get {
        System.Byte RNTRNTRNT_469 = (byte)(this.accumulator >> (32 - this.nAccumulatedBits) & 0xff);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503611uL);
        return RNTRNTRNT_469;
      }
    }
    public int NumRemainingBits {
      get {
        System.Int32 RNTRNTRNT_470 = this.nAccumulatedBits;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503612uL);
        return RNTRNTRNT_470;
      }
    }
    public int TotalBytesWrittenOut {
      get {
        System.Int32 RNTRNTRNT_471 = this.totalBytesWrittenOut;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503613uL);
        return RNTRNTRNT_471;
      }
    }
    public void Reset()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503614uL);
      this.accumulator = 0;
      this.nAccumulatedBits = 0;
      this.totalBytesWrittenOut = 0;
      this.output.Seek(0, SeekOrigin.Begin);
      this.output.SetLength(0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503615uL);
    }
    public void WriteBits(int nbits, uint value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503616uL);
      int nAccumulated = this.nAccumulatedBits;
      uint u = this.accumulator;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503617uL);
      while (nAccumulated >= 8) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503618uL);
        this.output.WriteByte((byte)(u >> 24 & 0xff));
        this.totalBytesWrittenOut++;
        u <<= 8;
        nAccumulated -= 8;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503619uL);
      this.accumulator = u | (value << (32 - nAccumulated - nbits));
      this.nAccumulatedBits = nAccumulated + nbits;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503620uL);
    }
    public void WriteByte(byte b)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503621uL);
      WriteBits(8, b);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503622uL);
    }
    public void WriteInt(uint u)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503623uL);
      WriteBits(8, (u >> 24) & 0xff);
      WriteBits(8, (u >> 16) & 0xff);
      WriteBits(8, (u >> 8) & 0xff);
      WriteBits(8, u & 0xff);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503624uL);
    }
    public void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503625uL);
      WriteBits(0, 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503626uL);
    }
    public void FinishAndPad()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503627uL);
      Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503628uL);
      if (this.NumRemainingBits > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503629uL);
        byte b = (byte)((this.accumulator >> 24) & 0xff);
        this.output.WriteByte(b);
        this.totalBytesWrittenOut++;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503630uL);
    }
  }
}
