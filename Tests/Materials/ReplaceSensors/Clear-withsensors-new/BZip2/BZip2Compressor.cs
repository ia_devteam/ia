using System;
using System.IO;
namespace Ionic.BZip2
{
  internal class BZip2Compressor
  {
    private int blockSize100k;
    private int currentByte = -1;
    private int runLength = 0;
    private int last;
    private int outBlockFillThreshold;
    private CompressionState cstate;
    private readonly Ionic.Crc.CRC32 crc = new Ionic.Crc.CRC32(true);
    BitWriter bw;
    int runs;
    private int workDone;
    private int workLimit;
    private bool firstAttempt;
    private bool blockRandomised;
    private int origPtr;
    private int nInUse;
    private int nMTF;
    private static readonly int SETMASK = (1 << 21);
    private static readonly int CLEARMASK = (~SETMASK);
    private static readonly byte GREATER_ICOST = 15;
    private static readonly byte LESSER_ICOST = 0;
    private static readonly int SMALL_THRESH = 20;
    private static readonly int DEPTH_THRESH = 10;
    private static readonly int WORK_FACTOR = 30;
    private static readonly int[] increments = {
      1,
      4,
      13,
      40,
      121,
      364,
      1093,
      3280,
      9841,
      29524,
      88573,
      265720,
      797161,
      2391484
    };
    public BZip2Compressor(BitWriter writer) : this(writer, BZip2.MaxBlockSize)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503631uL);
    }
    public BZip2Compressor(BitWriter writer, int blockSize)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503632uL);
      this.blockSize100k = blockSize;
      this.bw = writer;
      this.outBlockFillThreshold = (blockSize * BZip2.BlockSizeMultiple) - 20;
      this.cstate = new CompressionState(blockSize);
      Reset();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503633uL);
    }
    void Reset()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503634uL);
      this.crc.Reset();
      this.currentByte = -1;
      this.runLength = 0;
      this.last = -1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503635uL);
      for (int i = 256; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503636uL);
        this.cstate.inUse[i] = false;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503637uL);
    }
    public int BlockSize {
      get {
        System.Int32 RNTRNTRNT_472 = this.blockSize100k;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503638uL);
        return RNTRNTRNT_472;
      }
    }
    public uint Crc32 { get; private set; }
    public int AvailableBytesOut { get; private set; }
    public int UncompressedBytes {
      get {
        System.Int32 RNTRNTRNT_473 = this.last + 1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503639uL);
        return RNTRNTRNT_473;
      }
    }
    public int Fill(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503640uL);
      if (this.last >= this.outBlockFillThreshold) {
        System.Int32 RNTRNTRNT_474 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503641uL);
        return RNTRNTRNT_474;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503642uL);
      int bytesWritten = 0;
      int limit = offset + count;
      int rc;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503643uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503644uL);
        rc = write0(buffer[offset++]);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503645uL);
        if (rc > 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503646uL);
          bytesWritten++;
        }
      } while (offset < limit && rc == 1);
      System.Int32 RNTRNTRNT_475 = bytesWritten;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503647uL);
      return RNTRNTRNT_475;
    }
    private int write0(byte b)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503648uL);
      bool rc;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503649uL);
      if (this.currentByte == -1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503650uL);
        this.currentByte = b;
        this.runLength++;
        System.Int32 RNTRNTRNT_476 = 1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503651uL);
        return RNTRNTRNT_476;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503652uL);
      if (this.currentByte == b) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503653uL);
        if (++this.runLength > 254) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503654uL);
          rc = AddRunToOutputBlock(false);
          this.currentByte = -1;
          this.runLength = 0;
          System.Int32 RNTRNTRNT_477 = (rc) ? 2 : 1;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503655uL);
          return RNTRNTRNT_477;
        }
        System.Int32 RNTRNTRNT_478 = 1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503656uL);
        return RNTRNTRNT_478;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503657uL);
      rc = AddRunToOutputBlock(false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503658uL);
      if (rc) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503659uL);
        this.currentByte = -1;
        this.runLength = 0;
        System.Int32 RNTRNTRNT_479 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503660uL);
        return RNTRNTRNT_479;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503661uL);
      this.runLength = 1;
      this.currentByte = b;
      System.Int32 RNTRNTRNT_480 = 1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503662uL);
      return RNTRNTRNT_480;
    }
    private bool AddRunToOutputBlock(bool final)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503663uL);
      runs++;
      int previousLast = this.last;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503664uL);
      if (previousLast >= this.outBlockFillThreshold && !final) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503665uL);
        var msg = String.Format("block overrun(final={2}): {0} >= threshold ({1})", previousLast, this.outBlockFillThreshold, final);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503666uL);
        throw new Exception(msg);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503667uL);
      byte b = (byte)this.currentByte;
      byte[] block = this.cstate.block;
      this.cstate.inUse[b] = true;
      int rl = this.runLength;
      this.crc.UpdateCRC(b, rl);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503668uL);
      switch (rl) {
        case 1:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503671uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503669uL);
            block[previousLast + 2] = b;
            this.last = previousLast + 1;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503670uL);
            break;
          }

        case 2:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503674uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503672uL);
            block[previousLast + 2] = b;
            block[previousLast + 3] = b;
            this.last = previousLast + 2;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503673uL);
            break;
          }

        case 3:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503677uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503675uL);
            block[previousLast + 2] = b;
            block[previousLast + 3] = b;
            block[previousLast + 4] = b;
            this.last = previousLast + 3;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503676uL);
            break;
          }

        default:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503680uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503678uL);
            rl -= 4;
            this.cstate.inUse[rl] = true;
            block[previousLast + 2] = b;
            block[previousLast + 3] = b;
            block[previousLast + 4] = b;
            block[previousLast + 5] = b;
            block[previousLast + 6] = (byte)rl;
            this.last = previousLast + 5;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503679uL);
            break;
          }

      }
      System.Boolean RNTRNTRNT_481 = (this.last >= this.outBlockFillThreshold);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503681uL);
      return RNTRNTRNT_481;
    }
    public void CompressAndWrite()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503682uL);
      if (this.runLength > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503683uL);
        AddRunToOutputBlock(true);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503684uL);
      this.currentByte = -1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503685uL);
      if (this.last == -1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503686uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503687uL);
      blockSort();
      this.bw.WriteByte(0x31);
      this.bw.WriteByte(0x41);
      this.bw.WriteByte(0x59);
      this.bw.WriteByte(0x26);
      this.bw.WriteByte(0x53);
      this.bw.WriteByte(0x59);
      this.Crc32 = (uint)this.crc.Crc32Result;
      this.bw.WriteInt(this.Crc32);
      this.bw.WriteBits(1, (this.blockRandomised) ? 1u : 0u);
      moveToFrontCodeAndSend();
      Reset();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503688uL);
    }
    private void randomiseBlock()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503689uL);
      bool[] inUse = this.cstate.inUse;
      byte[] block = this.cstate.block;
      int lastShadow = this.last;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503690uL);
      for (int i = 256; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503691uL);
        inUse[i] = false;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503692uL);
      int rNToGo = 0;
      int rTPos = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503693uL);
      for (int i = 0, j = 1; i <= lastShadow; i = j,j++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503694uL);
        if (rNToGo == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503695uL);
          rNToGo = (char)Rand.Rnums(rTPos);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503696uL);
          if (++rTPos == 512) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503697uL);
            rTPos = 0;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503698uL);
        rNToGo--;
        block[j] ^= (byte)((rNToGo == 1) ? 1 : 0);
        inUse[block[j] & 0xff] = true;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503699uL);
      this.blockRandomised = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503700uL);
    }
    private void mainSort()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503701uL);
      CompressionState dataShadow = this.cstate;
      int[] runningOrder = dataShadow.mainSort_runningOrder;
      int[] copy = dataShadow.mainSort_copy;
      bool[] bigDone = dataShadow.mainSort_bigDone;
      int[] ftab = dataShadow.ftab;
      byte[] block = dataShadow.block;
      int[] fmap = dataShadow.fmap;
      char[] quadrant = dataShadow.quadrant;
      int lastShadow = this.last;
      int workLimitShadow = this.workLimit;
      bool firstAttemptShadow = this.firstAttempt;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503702uL);
      for (int i = 65537; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503703uL);
        ftab[i] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503704uL);
      for (int i = 0; i < BZip2.NUM_OVERSHOOT_BYTES; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503705uL);
        block[lastShadow + i + 2] = block[(i % (lastShadow + 1)) + 1];
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503706uL);
      for (int i = lastShadow + BZip2.NUM_OVERSHOOT_BYTES + 1; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503707uL);
        quadrant[i] = '\0';
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503708uL);
      block[0] = block[lastShadow + 1];
      int c1 = block[0] & 0xff;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503709uL);
      for (int i = 0; i <= lastShadow; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503710uL);
        int c2 = block[i + 1] & 0xff;
        ftab[(c1 << 8) + c2]++;
        c1 = c2;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503711uL);
      for (int i = 1; i <= 65536; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503712uL);
        ftab[i] += ftab[i - 1];
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503713uL);
      c1 = block[1] & 0xff;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503714uL);
      for (int i = 0; i < lastShadow; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503715uL);
        int c2 = block[i + 2] & 0xff;
        fmap[--ftab[(c1 << 8) + c2]] = i;
        c1 = c2;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503716uL);
      fmap[--ftab[((block[lastShadow + 1] & 0xff) << 8) + (block[1] & 0xff)]] = lastShadow;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503717uL);
      for (int i = 256; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503718uL);
        bigDone[i] = false;
        runningOrder[i] = i;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503719uL);
      for (int h = 364; h != 1;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503720uL);
        h /= 3;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503721uL);
        for (int i = h; i <= 255; i++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503722uL);
          int vv = runningOrder[i];
          int a = ftab[(vv + 1) << 8] - ftab[vv << 8];
          int b = h - 1;
          int j = i;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503723uL);
          for (int ro = runningOrder[j - h]; (ftab[(ro + 1) << 8] - ftab[ro << 8]) > a; ro = runningOrder[j - h]) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503724uL);
            runningOrder[j] = ro;
            j -= h;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503725uL);
            if (j <= b) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503726uL);
              break;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503727uL);
          runningOrder[j] = vv;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503728uL);
      for (int i = 0; i <= 255; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503729uL);
        int ss = runningOrder[i];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503730uL);
        for (int j = 0; j <= 255; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503731uL);
          int sb = (ss << 8) + j;
          int ftab_sb = ftab[sb];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503732uL);
          if ((ftab_sb & SETMASK) != SETMASK) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503733uL);
            int lo = ftab_sb & CLEARMASK;
            int hi = (ftab[sb + 1] & CLEARMASK) - 1;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503734uL);
            if (hi > lo) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503735uL);
              mainQSort3(dataShadow, lo, hi, 2);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503736uL);
              if (firstAttemptShadow && (this.workDone > workLimitShadow)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503737uL);
                return;
              }
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503738uL);
            ftab[sb] = ftab_sb | SETMASK;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503739uL);
        for (int j = 0; j <= 255; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503740uL);
          copy[j] = ftab[(j << 8) + ss] & CLEARMASK;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503741uL);
        for (int j = ftab[ss << 8] & CLEARMASK, hj = (ftab[(ss + 1) << 8] & CLEARMASK); j < hj; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503742uL);
          int fmap_j = fmap[j];
          c1 = block[fmap_j] & 0xff;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503743uL);
          if (!bigDone[c1]) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503744uL);
            fmap[copy[c1]] = (fmap_j == 0) ? lastShadow : (fmap_j - 1);
            copy[c1]++;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503745uL);
        for (int j = 256; --j >= 0;) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503746uL);
          ftab[(j << 8) + ss] |= SETMASK;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503747uL);
        bigDone[ss] = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503748uL);
        if (i < 255) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503749uL);
          int bbStart = ftab[ss << 8] & CLEARMASK;
          int bbSize = (ftab[(ss + 1) << 8] & CLEARMASK) - bbStart;
          int shifts = 0;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503750uL);
          while ((bbSize >> shifts) > 65534) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503751uL);
            shifts++;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503752uL);
          for (int j = 0; j < bbSize; j++) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503753uL);
            int a2update = fmap[bbStart + j];
            char qVal = (char)(j >> shifts);
            quadrant[a2update] = qVal;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503754uL);
            if (a2update < BZip2.NUM_OVERSHOOT_BYTES) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503755uL);
              quadrant[a2update + lastShadow + 1] = qVal;
            }
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503756uL);
    }
    private void blockSort()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503757uL);
      this.workLimit = WORK_FACTOR * this.last;
      this.workDone = 0;
      this.blockRandomised = false;
      this.firstAttempt = true;
      mainSort();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503758uL);
      if (this.firstAttempt && (this.workDone > this.workLimit)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503759uL);
        randomiseBlock();
        this.workLimit = this.workDone = 0;
        this.firstAttempt = false;
        mainSort();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503760uL);
      int[] fmap = this.cstate.fmap;
      this.origPtr = -1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503761uL);
      for (int i = 0, lastShadow = this.last; i <= lastShadow; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503762uL);
        if (fmap[i] == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503763uL);
          this.origPtr = i;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503764uL);
          break;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503765uL);
    }
    private bool mainSimpleSort(CompressionState dataShadow, int lo, int hi, int d)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503766uL);
      int bigN = hi - lo + 1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503767uL);
      if (bigN < 2) {
        System.Boolean RNTRNTRNT_482 = this.firstAttempt && (this.workDone > this.workLimit);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503768uL);
        return RNTRNTRNT_482;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503769uL);
      int hp = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503770uL);
      while (increments[hp] < bigN) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503771uL);
        hp++;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503772uL);
      int[] fmap = dataShadow.fmap;
      char[] quadrant = dataShadow.quadrant;
      byte[] block = dataShadow.block;
      int lastShadow = this.last;
      int lastPlus1 = lastShadow + 1;
      bool firstAttemptShadow = this.firstAttempt;
      int workLimitShadow = this.workLimit;
      int workDoneShadow = this.workDone;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503773uL);
      while (--hp >= 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503774uL);
        int h = increments[hp];
        int mj = lo + h - 1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503775uL);
        for (int i = lo + h; i <= hi;) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503776uL);
          for (int k = 3; (i <= hi) && (--k >= 0); i++) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503777uL);
            int v = fmap[i];
            int vd = v + d;
            int j = i;
            bool onceRunned = false;
            int a = 0;
            HAMMER:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503778uL);
            while (true) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503779uL);
              if (onceRunned) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503780uL);
                fmap[j] = a;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503781uL);
                if ((j -= h) <= mj) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503782uL);
                  goto END_HAMMER;
                }
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503783uL);
                onceRunned = true;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503784uL);
              a = fmap[j - h];
              int i1 = a + d;
              int i2 = vd;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503785uL);
              if (block[i1 + 1] == block[i2 + 1]) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503786uL);
                if (block[i1 + 2] == block[i2 + 2]) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503787uL);
                  if (block[i1 + 3] == block[i2 + 3]) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503788uL);
                    if (block[i1 + 4] == block[i2 + 4]) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503789uL);
                      if (block[i1 + 5] == block[i2 + 5]) {
                        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503790uL);
                        if (block[(i1 += 6)] == block[(i2 += 6)]) {
                          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503791uL);
                          int x = lastShadow;
                          X:
                          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503792uL);
                          while (x > 0) {
                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503793uL);
                            x -= 4;
                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503794uL);
                            if (block[i1 + 1] == block[i2 + 1]) {
                              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503795uL);
                              if (quadrant[i1] == quadrant[i2]) {
                                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503796uL);
                                if (block[i1 + 2] == block[i2 + 2]) {
                                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503797uL);
                                  if (quadrant[i1 + 1] == quadrant[i2 + 1]) {
                                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503798uL);
                                    if (block[i1 + 3] == block[i2 + 3]) {
                                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503799uL);
                                      if (quadrant[i1 + 2] == quadrant[i2 + 2]) {
                                        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503800uL);
                                        if (block[i1 + 4] == block[i2 + 4]) {
                                          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503801uL);
                                          if (quadrant[i1 + 3] == quadrant[i2 + 3]) {
                                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503802uL);
                                            if ((i1 += 4) >= lastPlus1) {
                                              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503803uL);
                                              i1 -= lastPlus1;
                                            }
                                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503804uL);
                                            if ((i2 += 4) >= lastPlus1) {
                                              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503805uL);
                                              i2 -= lastPlus1;
                                            }
                                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503806uL);
                                            workDoneShadow++;
                                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503807uL);
                                            goto X;
                                          } else if ((quadrant[i1 + 3] > quadrant[i2 + 3])) {
                                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503809uL);
                                            goto HAMMER;
                                          } else {
                                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503808uL);
                                            goto END_HAMMER;
                                          }
                                        } else if ((block[i1 + 4] & 0xff) > (block[i2 + 4] & 0xff)) {
                                          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503811uL);
                                          goto HAMMER;
                                        } else {
                                          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503810uL);
                                          goto END_HAMMER;
                                        }
                                      } else if ((quadrant[i1 + 2] > quadrant[i2 + 2])) {
                                        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503813uL);
                                        goto HAMMER;
                                      } else {
                                        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503812uL);
                                        goto END_HAMMER;
                                      }
                                    } else if ((block[i1 + 3] & 0xff) > (block[i2 + 3] & 0xff)) {
                                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503815uL);
                                      goto HAMMER;
                                    } else {
                                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503814uL);
                                      goto END_HAMMER;
                                    }
                                  } else if ((quadrant[i1 + 1] > quadrant[i2 + 1])) {
                                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503817uL);
                                    goto HAMMER;
                                  } else {
                                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503816uL);
                                    goto END_HAMMER;
                                  }
                                } else if ((block[i1 + 2] & 0xff) > (block[i2 + 2] & 0xff)) {
                                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503819uL);
                                  goto HAMMER;
                                } else {
                                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503818uL);
                                  goto END_HAMMER;
                                }
                              } else if ((quadrant[i1] > quadrant[i2])) {
                                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503821uL);
                                goto HAMMER;
                              } else {
                                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503820uL);
                                goto END_HAMMER;
                              }
                            } else if ((block[i1 + 1] & 0xff) > (block[i2 + 1] & 0xff)) {
                              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503823uL);
                              goto HAMMER;
                            } else {
                              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503822uL);
                              goto END_HAMMER;
                            }
                          }
                          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503824uL);
                          goto END_HAMMER;
                        } else {
                          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503825uL);
                          if ((block[i1] & 0xff) > (block[i2] & 0xff)) {
                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503826uL);
                            goto HAMMER;
                          } else {
                            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503827uL);
                            goto END_HAMMER;
                          }
                        }
                      } else if ((block[i1 + 5] & 0xff) > (block[i2 + 5] & 0xff)) {
                        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503829uL);
                        goto HAMMER;
                      } else {
                        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503828uL);
                        goto END_HAMMER;
                      }
                    } else if ((block[i1 + 4] & 0xff) > (block[i2 + 4] & 0xff)) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503831uL);
                      goto HAMMER;
                    } else {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503830uL);
                      goto END_HAMMER;
                    }
                  } else if ((block[i1 + 3] & 0xff) > (block[i2 + 3] & 0xff)) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503833uL);
                    goto HAMMER;
                  } else {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503832uL);
                    goto END_HAMMER;
                  }
                } else if ((block[i1 + 2] & 0xff) > (block[i2 + 2] & 0xff)) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503835uL);
                  goto HAMMER;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503834uL);
                  goto END_HAMMER;
                }
              } else if ((block[i1 + 1] & 0xff) > (block[i2 + 1] & 0xff)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503837uL);
                goto HAMMER;
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503836uL);
                goto END_HAMMER;
              }
            }
            END_HAMMER:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503838uL);
            fmap[j] = v;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503839uL);
          if (firstAttemptShadow && (i <= hi) && (workDoneShadow > workLimitShadow)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503840uL);
            goto END_HP;
          }
        }
      }
      END_HP:
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503841uL);
      this.workDone = workDoneShadow;
      System.Boolean RNTRNTRNT_483 = firstAttemptShadow && (workDoneShadow > workLimitShadow);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503842uL);
      return RNTRNTRNT_483;
    }
    private static void vswap(int[] fmap, int p1, int p2, int n)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503843uL);
      n += p1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503844uL);
      while (p1 < n) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503845uL);
        int t = fmap[p1];
        fmap[p1++] = fmap[p2];
        fmap[p2++] = t;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503846uL);
    }
    private static byte med3(byte a, byte b, byte c)
    {
      System.Byte RNTRNTRNT_484 = (a < b) ? (b < c ? b : a < c ? c : a) : (b > c ? b : a > c ? c : a);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503847uL);
      return RNTRNTRNT_484;
    }
    private void mainQSort3(CompressionState dataShadow, int loSt, int hiSt, int dSt)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503848uL);
      int[] stack_ll = dataShadow.stack_ll;
      int[] stack_hh = dataShadow.stack_hh;
      int[] stack_dd = dataShadow.stack_dd;
      int[] fmap = dataShadow.fmap;
      byte[] block = dataShadow.block;
      stack_ll[0] = loSt;
      stack_hh[0] = hiSt;
      stack_dd[0] = dSt;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503849uL);
      for (int sp = 1; --sp >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503850uL);
        int lo = stack_ll[sp];
        int hi = stack_hh[sp];
        int d = stack_dd[sp];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503851uL);
        if ((hi - lo < SMALL_THRESH) || (d > DEPTH_THRESH)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503852uL);
          if (mainSimpleSort(dataShadow, lo, hi, d)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503853uL);
            return;
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503854uL);
          int d1 = d + 1;
          int med = med3(block[fmap[lo] + d1], block[fmap[hi] + d1], block[fmap[(lo + hi) >> 1] + d1]) & 0xff;
          int unLo = lo;
          int unHi = hi;
          int ltLo = lo;
          int gtHi = hi;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503855uL);
          while (true) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503856uL);
            while (unLo <= unHi) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503857uL);
              int n = (block[fmap[unLo] + d1] & 0xff) - med;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503858uL);
              if (n == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503859uL);
                int temp = fmap[unLo];
                fmap[unLo++] = fmap[ltLo];
                fmap[ltLo++] = temp;
              } else if (n < 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503861uL);
                unLo++;
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503860uL);
                break;
              }
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503862uL);
            while (unLo <= unHi) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503863uL);
              int n = (block[fmap[unHi] + d1] & 0xff) - med;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503864uL);
              if (n == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503865uL);
                int temp = fmap[unHi];
                fmap[unHi--] = fmap[gtHi];
                fmap[gtHi--] = temp;
              } else if (n > 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503867uL);
                unHi--;
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503866uL);
                break;
              }
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503868uL);
            if (unLo <= unHi) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503869uL);
              int temp = fmap[unLo];
              fmap[unLo++] = fmap[unHi];
              fmap[unHi--] = temp;
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503870uL);
              break;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503871uL);
          if (gtHi < ltLo) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503872uL);
            stack_ll[sp] = lo;
            stack_hh[sp] = hi;
            stack_dd[sp] = d1;
            sp++;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503873uL);
            int n = ((ltLo - lo) < (unLo - ltLo)) ? (ltLo - lo) : (unLo - ltLo);
            vswap(fmap, lo, unLo - n, n);
            int m = ((hi - gtHi) < (gtHi - unHi)) ? (hi - gtHi) : (gtHi - unHi);
            vswap(fmap, unLo, hi - m + 1, m);
            n = lo + unLo - ltLo - 1;
            m = hi - (gtHi - unHi) + 1;
            stack_ll[sp] = lo;
            stack_hh[sp] = n;
            stack_dd[sp] = d;
            sp++;
            stack_ll[sp] = n + 1;
            stack_hh[sp] = m - 1;
            stack_dd[sp] = d1;
            sp++;
            stack_ll[sp] = m;
            stack_hh[sp] = hi;
            stack_dd[sp] = d;
            sp++;
          }
        }
      }
    }
    private void generateMTFValues()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503874uL);
      int lastShadow = this.last;
      CompressionState dataShadow = this.cstate;
      bool[] inUse = dataShadow.inUse;
      byte[] block = dataShadow.block;
      int[] fmap = dataShadow.fmap;
      char[] sfmap = dataShadow.sfmap;
      int[] mtfFreq = dataShadow.mtfFreq;
      byte[] unseqToSeq = dataShadow.unseqToSeq;
      byte[] yy = dataShadow.generateMTFValues_yy;
      int nInUseShadow = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503875uL);
      for (int i = 0; i < 256; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503876uL);
        if (inUse[i]) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503877uL);
          unseqToSeq[i] = (byte)nInUseShadow;
          nInUseShadow++;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503878uL);
      this.nInUse = nInUseShadow;
      int eob = nInUseShadow + 1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503879uL);
      for (int i = eob; i >= 0; i--) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503880uL);
        mtfFreq[i] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503881uL);
      for (int i = nInUseShadow; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503882uL);
        yy[i] = (byte)i;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503883uL);
      int wr = 0;
      int zPend = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503884uL);
      for (int i = 0; i <= lastShadow; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503885uL);
        byte ll_i = unseqToSeq[block[fmap[i]] & 0xff];
        byte tmp = yy[0];
        int j = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503886uL);
        while (ll_i != tmp) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503887uL);
          j++;
          byte tmp2 = tmp;
          tmp = yy[j];
          yy[j] = tmp2;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503888uL);
        yy[0] = tmp;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503889uL);
        if (j == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503890uL);
          zPend++;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503891uL);
          if (zPend > 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503892uL);
            zPend--;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503893uL);
            while (true) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503894uL);
              if ((zPend & 1) == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503895uL);
                sfmap[wr] = BZip2.RUNA;
                wr++;
                mtfFreq[BZip2.RUNA]++;
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503896uL);
                sfmap[wr] = BZip2.RUNB;
                wr++;
                mtfFreq[BZip2.RUNB]++;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503897uL);
              if (zPend >= 2) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503898uL);
                zPend = (zPend - 2) >> 1;
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503899uL);
                break;
              }
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503900uL);
            zPend = 0;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503901uL);
          sfmap[wr] = (char)(j + 1);
          wr++;
          mtfFreq[j + 1]++;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503902uL);
      if (zPend > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503903uL);
        zPend--;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503904uL);
        while (true) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503905uL);
          if ((zPend & 1) == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503906uL);
            sfmap[wr] = BZip2.RUNA;
            wr++;
            mtfFreq[BZip2.RUNA]++;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503907uL);
            sfmap[wr] = BZip2.RUNB;
            wr++;
            mtfFreq[BZip2.RUNB]++;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503908uL);
          if (zPend >= 2) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503909uL);
            zPend = (zPend - 2) >> 1;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503910uL);
            break;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503911uL);
      sfmap[wr] = (char)eob;
      mtfFreq[eob]++;
      this.nMTF = wr + 1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503912uL);
    }
    private static void hbAssignCodes(int[] code, byte[] length, int minLen, int maxLen, int alphaSize)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503913uL);
      int vec = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503914uL);
      for (int n = minLen; n <= maxLen; n++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503915uL);
        for (int i = 0; i < alphaSize; i++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503916uL);
          if ((length[i] & 0xff) == n) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503917uL);
            code[i] = vec;
            vec++;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503918uL);
        vec <<= 1;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503919uL);
    }
    private void sendMTFValues()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503920uL);
      byte[][] len = this.cstate.sendMTFValues_len;
      int alphaSize = this.nInUse + 2;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503921uL);
      for (int t = BZip2.NGroups; --t >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503922uL);
        byte[] len_t = len[t];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503923uL);
        for (int v = alphaSize; --v >= 0;) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503924uL);
          len_t[v] = GREATER_ICOST;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503925uL);
      int nGroups = (this.nMTF < 200) ? 2 : (this.nMTF < 600) ? 3 : (this.nMTF < 1200) ? 4 : (this.nMTF < 2400) ? 5 : 6;
      sendMTFValues0(nGroups, alphaSize);
      int nSelectors = sendMTFValues1(nGroups, alphaSize);
      sendMTFValues2(nGroups, nSelectors);
      sendMTFValues3(nGroups, alphaSize);
      sendMTFValues4();
      sendMTFValues5(nGroups, nSelectors);
      sendMTFValues6(nGroups, alphaSize);
      sendMTFValues7(nSelectors);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503926uL);
    }
    private void sendMTFValues0(int nGroups, int alphaSize)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503927uL);
      byte[][] len = this.cstate.sendMTFValues_len;
      int[] mtfFreq = this.cstate.mtfFreq;
      int remF = this.nMTF;
      int gs = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503928uL);
      for (int nPart = nGroups; nPart > 0; nPart--) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503929uL);
        int tFreq = remF / nPart;
        int ge = gs - 1;
        int aFreq = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503930uL);
        for (int a = alphaSize - 1; (aFreq < tFreq) && (ge < a);) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503931uL);
          aFreq += mtfFreq[++ge];
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503932uL);
        if ((ge > gs) && (nPart != nGroups) && (nPart != 1) && (((nGroups - nPart) & 1) != 0)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503933uL);
          aFreq -= mtfFreq[ge--];
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503934uL);
        byte[] len_np = len[nPart - 1];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503935uL);
        for (int v = alphaSize; --v >= 0;) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503936uL);
          if ((v >= gs) && (v <= ge)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503937uL);
            len_np[v] = LESSER_ICOST;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503938uL);
            len_np[v] = GREATER_ICOST;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503939uL);
        gs = ge + 1;
        remF -= aFreq;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503940uL);
    }
    private static void hbMakeCodeLengths(byte[] len, int[] freq, CompressionState state1, int alphaSize, int maxLen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503941uL);
      int[] heap = state1.heap;
      int[] weight = state1.weight;
      int[] parent = state1.parent;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503942uL);
      for (int i = alphaSize; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503943uL);
        weight[i + 1] = (freq[i] == 0 ? 1 : freq[i]) << 8;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503944uL);
      for (bool tooLong = true; tooLong;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503945uL);
        tooLong = false;
        int nNodes = alphaSize;
        int nHeap = 0;
        heap[0] = 0;
        weight[0] = 0;
        parent[0] = -2;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503946uL);
        for (int i = 1; i <= alphaSize; i++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503947uL);
          parent[i] = -1;
          nHeap++;
          heap[nHeap] = i;
          int zz = nHeap;
          int tmp = heap[zz];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503948uL);
          while (weight[tmp] < weight[heap[zz >> 1]]) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503949uL);
            heap[zz] = heap[zz >> 1];
            zz >>= 1;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503950uL);
          heap[zz] = tmp;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503951uL);
        while (nHeap > 1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503952uL);
          int n1 = heap[1];
          heap[1] = heap[nHeap];
          nHeap--;
          int yy = 0;
          int zz = 1;
          int tmp = heap[1];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503953uL);
          while (true) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503954uL);
            yy = zz << 1;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503955uL);
            if (yy > nHeap) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503956uL);
              break;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503957uL);
            if ((yy < nHeap) && (weight[heap[yy + 1]] < weight[heap[yy]])) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503958uL);
              yy++;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503959uL);
            if (weight[tmp] < weight[heap[yy]]) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503960uL);
              break;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503961uL);
            heap[zz] = heap[yy];
            zz = yy;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503962uL);
          heap[zz] = tmp;
          int n2 = heap[1];
          heap[1] = heap[nHeap];
          nHeap--;
          yy = 0;
          zz = 1;
          tmp = heap[1];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503963uL);
          while (true) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503964uL);
            yy = zz << 1;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503965uL);
            if (yy > nHeap) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503966uL);
              break;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503967uL);
            if ((yy < nHeap) && (weight[heap[yy + 1]] < weight[heap[yy]])) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503968uL);
              yy++;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503969uL);
            if (weight[tmp] < weight[heap[yy]]) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503970uL);
              break;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503971uL);
            heap[zz] = heap[yy];
            zz = yy;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503972uL);
          heap[zz] = tmp;
          nNodes++;
          parent[n1] = parent[n2] = nNodes;
          int weight_n1 = weight[n1];
          int weight_n2 = weight[n2];
          weight[nNodes] = (int)(((uint)weight_n1 & 0xffffff00u) + ((uint)weight_n2 & 0xffffff00u)) | (1 + (((weight_n1 & 0xff) > (weight_n2 & 0xff)) ? (weight_n1 & 0xff) : (weight_n2 & 0xff)));
          parent[nNodes] = -1;
          nHeap++;
          heap[nHeap] = nNodes;
          tmp = 0;
          zz = nHeap;
          tmp = heap[zz];
          int weight_tmp = weight[tmp];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503973uL);
          while (weight_tmp < weight[heap[zz >> 1]]) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503974uL);
            heap[zz] = heap[zz >> 1];
            zz >>= 1;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503975uL);
          heap[zz] = tmp;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503976uL);
        for (int i = 1; i <= alphaSize; i++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503977uL);
          int j = 0;
          int k = i;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503978uL);
          for (int parent_k; (parent_k = parent[k]) >= 0;) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503979uL);
            k = parent_k;
            j++;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503980uL);
          len[i - 1] = (byte)j;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503981uL);
          if (j > maxLen) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503982uL);
            tooLong = true;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503983uL);
        if (tooLong) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503984uL);
          for (int i = 1; i < alphaSize; i++) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503985uL);
            int j = weight[i] >> 8;
            j = 1 + (j >> 1);
            weight[i] = j << 8;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503986uL);
    }
    private int sendMTFValues1(int nGroups, int alphaSize)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503987uL);
      CompressionState dataShadow = this.cstate;
      int[][] rfreq = dataShadow.sendMTFValues_rfreq;
      int[] fave = dataShadow.sendMTFValues_fave;
      short[] cost = dataShadow.sendMTFValues_cost;
      char[] sfmap = dataShadow.sfmap;
      byte[] selector = dataShadow.selector;
      byte[][] len = dataShadow.sendMTFValues_len;
      byte[] len_0 = len[0];
      byte[] len_1 = len[1];
      byte[] len_2 = len[2];
      byte[] len_3 = len[3];
      byte[] len_4 = len[4];
      byte[] len_5 = len[5];
      int nMTFShadow = this.nMTF;
      int nSelectors = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503988uL);
      for (int iter = 0; iter < BZip2.N_ITERS; iter++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503989uL);
        for (int t = nGroups; --t >= 0;) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503990uL);
          fave[t] = 0;
          int[] rfreqt = rfreq[t];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503991uL);
          for (int i = alphaSize; --i >= 0;) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503992uL);
            rfreqt[i] = 0;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503993uL);
        nSelectors = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503994uL);
        for (int gs = 0; gs < this.nMTF;) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503995uL);
          int ge = Math.Min(gs + BZip2.G_SIZE - 1, nMTFShadow - 1);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503996uL);
          if (nGroups == BZip2.NGroups) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503997uL);
            int[] c = new int[6];
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503998uL);
            for (int i = gs; i <= ge; i++) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503999uL);
              int icv = sfmap[i];
              c[0] += len_0[icv] & 0xff;
              c[1] += len_1[icv] & 0xff;
              c[2] += len_2[icv] & 0xff;
              c[3] += len_3[icv] & 0xff;
              c[4] += len_4[icv] & 0xff;
              c[5] += len_5[icv] & 0xff;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504000uL);
            cost[0] = (short)c[0];
            cost[1] = (short)c[1];
            cost[2] = (short)c[2];
            cost[3] = (short)c[3];
            cost[4] = (short)c[4];
            cost[5] = (short)c[5];
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504001uL);
            for (int t = nGroups; --t >= 0;) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504002uL);
              cost[t] = 0;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504003uL);
            for (int i = gs; i <= ge; i++) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504004uL);
              int icv = sfmap[i];
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504005uL);
              for (int t = nGroups; --t >= 0;) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504006uL);
                cost[t] += (short)(len[t][icv] & 0xff);
              }
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504007uL);
          int bt = -1;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504008uL);
          for (int t = nGroups, bc = 999999999; --t >= 0;) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504009uL);
            int cost_t = cost[t];
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504010uL);
            if (cost_t < bc) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504011uL);
              bc = cost_t;
              bt = t;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504012uL);
          fave[bt]++;
          selector[nSelectors] = (byte)bt;
          nSelectors++;
          int[] rfreq_bt = rfreq[bt];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504013uL);
          for (int i = gs; i <= ge; i++) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504014uL);
            rfreq_bt[sfmap[i]]++;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504015uL);
          gs = ge + 1;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504016uL);
        for (int t = 0; t < nGroups; t++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504017uL);
          hbMakeCodeLengths(len[t], rfreq[t], this.cstate, alphaSize, 20);
        }
      }
      System.Int32 RNTRNTRNT_485 = nSelectors;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504018uL);
      return RNTRNTRNT_485;
    }
    private void sendMTFValues2(int nGroups, int nSelectors)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504019uL);
      CompressionState dataShadow = this.cstate;
      byte[] pos = dataShadow.sendMTFValues2_pos;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504020uL);
      for (int i = nGroups; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504021uL);
        pos[i] = (byte)i;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504022uL);
      for (int i = 0; i < nSelectors; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504023uL);
        byte ll_i = dataShadow.selector[i];
        byte tmp = pos[0];
        int j = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504024uL);
        while (ll_i != tmp) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504025uL);
          j++;
          byte tmp2 = tmp;
          tmp = pos[j];
          pos[j] = tmp2;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504026uL);
        pos[0] = tmp;
        dataShadow.selectorMtf[i] = (byte)j;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504027uL);
    }
    private void sendMTFValues3(int nGroups, int alphaSize)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504028uL);
      int[][] code = this.cstate.sendMTFValues_code;
      byte[][] len = this.cstate.sendMTFValues_len;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504029uL);
      for (int t = 0; t < nGroups; t++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504030uL);
        int minLen = 32;
        int maxLen = 0;
        byte[] len_t = len[t];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504031uL);
        for (int i = alphaSize; --i >= 0;) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504032uL);
          int l = len_t[i] & 0xff;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504033uL);
          if (l > maxLen) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504034uL);
            maxLen = l;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504035uL);
          if (l < minLen) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504036uL);
            minLen = l;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504037uL);
        hbAssignCodes(code[t], len[t], minLen, maxLen, alphaSize);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504038uL);
    }
    private void sendMTFValues4()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504039uL);
      bool[] inUse = this.cstate.inUse;
      bool[] inUse16 = this.cstate.sentMTFValues4_inUse16;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504040uL);
      for (int i = 16; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504041uL);
        inUse16[i] = false;
        int i16 = i * 16;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504042uL);
        for (int j = 16; --j >= 0;) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504043uL);
          if (inUse[i16 + j]) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504044uL);
            inUse16[i] = true;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504045uL);
      uint u = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504046uL);
      for (int i = 0; i < 16; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504047uL);
        if (inUse16[i]) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504048uL);
          u |= 1u << (16 - i - 1);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504049uL);
      this.bw.WriteBits(16, u);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504050uL);
      for (int i = 0; i < 16; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504051uL);
        if (inUse16[i]) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504052uL);
          int i16 = i * 16;
          u = 0;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504053uL);
          for (int j = 0; j < 16; j++) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504054uL);
            if (inUse[i16 + j]) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504055uL);
              u |= 1u << (16 - j - 1);
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504056uL);
          this.bw.WriteBits(16, u);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504057uL);
    }
    private void sendMTFValues5(int nGroups, int nSelectors)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504058uL);
      this.bw.WriteBits(3, (uint)nGroups);
      this.bw.WriteBits(15, (uint)nSelectors);
      byte[] selectorMtf = this.cstate.selectorMtf;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504059uL);
      for (int i = 0; i < nSelectors; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504060uL);
        for (int j = 0, hj = selectorMtf[i] & 0xff; j < hj; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504061uL);
          this.bw.WriteBits(1, 1);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504062uL);
        this.bw.WriteBits(1, 0);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504063uL);
    }
    private void sendMTFValues6(int nGroups, int alphaSize)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504064uL);
      byte[][] len = this.cstate.sendMTFValues_len;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504065uL);
      for (int t = 0; t < nGroups; t++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504066uL);
        byte[] len_t = len[t];
        uint curr = (uint)(len_t[0] & 0xff);
        this.bw.WriteBits(5, curr);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504067uL);
        for (int i = 0; i < alphaSize; i++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504068uL);
          int lti = len_t[i] & 0xff;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504069uL);
          while (curr < lti) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504070uL);
            this.bw.WriteBits(2, 2u);
            curr++;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504071uL);
          while (curr > lti) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504072uL);
            this.bw.WriteBits(2, 3u);
            curr--;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504073uL);
          this.bw.WriteBits(1, 0u);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504074uL);
    }
    private void sendMTFValues7(int nSelectors)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504075uL);
      byte[][] len = this.cstate.sendMTFValues_len;
      int[][] code = this.cstate.sendMTFValues_code;
      byte[] selector = this.cstate.selector;
      char[] sfmap = this.cstate.sfmap;
      int nMTFShadow = this.nMTF;
      int selCtr = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504076uL);
      for (int gs = 0; gs < nMTFShadow;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504077uL);
        int ge = Math.Min(gs + BZip2.G_SIZE - 1, nMTFShadow - 1);
        int ix = selector[selCtr] & 0xff;
        int[] code_selCtr = code[ix];
        byte[] len_selCtr = len[ix];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504078uL);
        while (gs <= ge) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504079uL);
          int sfmap_i = sfmap[gs];
          int n = len_selCtr[sfmap_i] & 0xff;
          this.bw.WriteBits(n, (uint)code_selCtr[sfmap_i]);
          gs++;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504080uL);
        gs = ge + 1;
        selCtr++;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504081uL);
    }
    private void moveToFrontCodeAndSend()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504082uL);
      this.bw.WriteBits(24, (uint)this.origPtr);
      generateMTFValues();
      sendMTFValues();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504083uL);
    }
    private class CompressionState
    {
      public readonly bool[] inUse = new bool[256];
      public readonly byte[] unseqToSeq = new byte[256];
      public readonly int[] mtfFreq = new int[BZip2.MaxAlphaSize];
      public readonly byte[] selector = new byte[BZip2.MaxSelectors];
      public readonly byte[] selectorMtf = new byte[BZip2.MaxSelectors];
      public readonly byte[] generateMTFValues_yy = new byte[256];
      public byte[][] sendMTFValues_len;
      public int[][] sendMTFValues_rfreq;
      public readonly int[] sendMTFValues_fave = new int[BZip2.NGroups];
      public readonly short[] sendMTFValues_cost = new short[BZip2.NGroups];
      public int[][] sendMTFValues_code;
      public readonly byte[] sendMTFValues2_pos = new byte[BZip2.NGroups];
      public readonly bool[] sentMTFValues4_inUse16 = new bool[16];
      public readonly int[] stack_ll = new int[BZip2.QSORT_STACK_SIZE];
      public readonly int[] stack_hh = new int[BZip2.QSORT_STACK_SIZE];
      public readonly int[] stack_dd = new int[BZip2.QSORT_STACK_SIZE];
      public readonly int[] mainSort_runningOrder = new int[256];
      public readonly int[] mainSort_copy = new int[256];
      public readonly bool[] mainSort_bigDone = new bool[256];
      public int[] heap = new int[BZip2.MaxAlphaSize + 2];
      public int[] weight = new int[BZip2.MaxAlphaSize * 2];
      public int[] parent = new int[BZip2.MaxAlphaSize * 2];
      public readonly int[] ftab = new int[65537];
      public byte[] block;
      public int[] fmap;
      public char[] sfmap;
      public char[] quadrant;
      public CompressionState(int blockSize100k)
      {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504084uL);
        int n = blockSize100k * BZip2.BlockSizeMultiple;
        this.block = new byte[(n + 1 + BZip2.NUM_OVERSHOOT_BYTES)];
        this.fmap = new int[n];
        this.sfmap = new char[2 * n];
        this.quadrant = this.sfmap;
        this.sendMTFValues_len = BZip2.InitRectangularArray<byte>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.sendMTFValues_rfreq = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.sendMTFValues_code = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504085uL);
      }
    }
  }
}
