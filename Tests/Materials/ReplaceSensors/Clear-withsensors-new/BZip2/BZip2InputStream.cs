using System;
using System.IO;
namespace Ionic.BZip2
{
  public class BZip2InputStream : System.IO.Stream
  {
    bool _disposed;
    bool _leaveOpen;
    Int64 totalBytesRead;
    private int last;
    private int origPtr;
    private int blockSize100k;
    private bool blockRandomised;
    private int bsBuff;
    private int bsLive;
    private readonly Ionic.Crc.CRC32 crc = new Ionic.Crc.CRC32(true);
    private int nInUse;
    private Stream input;
    private int currentChar = -1;
    enum CState
    {
      EOF = 0,
      START_BLOCK = 1,
      RAND_PART_A = 2,
      RAND_PART_B = 3,
      RAND_PART_C = 4,
      NO_RAND_PART_A = 5,
      NO_RAND_PART_B = 6,
      NO_RAND_PART_C = 7
    }
    private CState currentState = CState.START_BLOCK;
    private uint storedBlockCRC, storedCombinedCRC;
    private uint computedBlockCRC, computedCombinedCRC;
    private int su_count;
    private int su_ch2;
    private int su_chPrev;
    private int su_i2;
    private int su_j2;
    private int su_rNToGo;
    private int su_rTPos;
    private int su_tPos;
    private char su_z;
    private BZip2InputStream.DecompressionState data;
    public BZip2InputStream(Stream input) : this(input, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504086uL);
    }
    public BZip2InputStream(Stream input, bool leaveOpen) : base()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504087uL);
      this.input = input;
      this._leaveOpen = leaveOpen;
      init();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504088uL);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504089uL);
      if (offset < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504090uL);
        throw new IndexOutOfRangeException(String.Format("offset ({0}) must be > 0", offset));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504091uL);
      if (count < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504092uL);
        throw new IndexOutOfRangeException(String.Format("count ({0}) must be > 0", count));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504093uL);
      if (offset + count > buffer.Length) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504094uL);
        throw new IndexOutOfRangeException(String.Format("offset({0}) count({1}) bLength({2})", offset, count, buffer.Length));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504095uL);
      if (this.input == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504096uL);
        throw new IOException("the stream is not open");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504097uL);
      int hi = offset + count;
      int destOffset = offset;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504098uL);
      for (int b; (destOffset < hi) && ((b = ReadByte()) >= 0);) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504099uL);
        buffer[destOffset++] = (byte)b;
      }
      System.Int32 RNTRNTRNT_486 = (destOffset == offset) ? -1 : (destOffset - offset);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504100uL);
      return RNTRNTRNT_486;
    }
    private void MakeMaps()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504101uL);
      bool[] inUse = this.data.inUse;
      byte[] seqToUnseq = this.data.seqToUnseq;
      int n = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504102uL);
      for (int i = 0; i < 256; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504103uL);
        if (inUse[i]) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504104uL);
          seqToUnseq[n++] = (byte)i;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504105uL);
      this.nInUse = n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504106uL);
    }
    public override int ReadByte()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504107uL);
      int retChar = this.currentChar;
      totalBytesRead++;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504108uL);
      switch (this.currentState) {
        case CState.EOF:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504110uL);
          
          {
            System.Int32 RNTRNTRNT_487 = -1;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504109uL);
            return RNTRNTRNT_487;
          }

        case CState.START_BLOCK:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504112uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504111uL);
            throw new IOException("bad state");
          }

        case CState.RAND_PART_A:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504114uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504113uL);
            throw new IOException("bad state");
          }

        case CState.RAND_PART_B:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504117uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504115uL);
            SetupRandPartB();
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504116uL);
            break;
          }

        case CState.RAND_PART_C:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504120uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504118uL);
            SetupRandPartC();
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504119uL);
            break;
          }

        case CState.NO_RAND_PART_A:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504122uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504121uL);
            throw new IOException("bad state");
          }

        case CState.NO_RAND_PART_B:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504125uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504123uL);
            SetupNoRandPartB();
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504124uL);
            break;
          }

        case CState.NO_RAND_PART_C:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504128uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504126uL);
            SetupNoRandPartC();
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504127uL);
            break;
          }

        default:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504130uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504129uL);
            throw new IOException("bad state");
          }

      }
      System.Int32 RNTRNTRNT_488 = retChar;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504131uL);
      return RNTRNTRNT_488;
    }
    public override bool CanRead {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504132uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504133uL);
          throw new ObjectDisposedException("BZip2Stream");
        }
        System.Boolean RNTRNTRNT_489 = this.input.CanRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504134uL);
        return RNTRNTRNT_489;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_490 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504135uL);
        return RNTRNTRNT_490;
      }
    }
    public override bool CanWrite {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504136uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504137uL);
          throw new ObjectDisposedException("BZip2Stream");
        }
        System.Boolean RNTRNTRNT_491 = input.CanWrite;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504138uL);
        return RNTRNTRNT_491;
      }
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504139uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504140uL);
        throw new ObjectDisposedException("BZip2Stream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504141uL);
      input.Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504142uL);
    }
    public override long Length {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504143uL);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_492 = this.totalBytesRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504144uL);
        return RNTRNTRNT_492;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504145uL);
        throw new NotImplementedException();
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504146uL);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504147uL);
      throw new NotImplementedException();
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504148uL);
      throw new NotImplementedException();
    }
    protected override void Dispose(bool disposing)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504149uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504150uL);
        if (!_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504151uL);
          if (disposing && (this.input != null)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504152uL);
            this.input.Close();
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504153uL);
          _disposed = true;
        }
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504154uL);
        base.Dispose(disposing);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504155uL);
    }
    void init()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504156uL);
      if (null == this.input) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504157uL);
        throw new IOException("No input Stream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504158uL);
      if (!this.input.CanRead) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504159uL);
        throw new IOException("Unreadable input Stream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504160uL);
      CheckMagicChar('B', 0);
      CheckMagicChar('Z', 1);
      CheckMagicChar('h', 2);
      int blockSize = this.input.ReadByte();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504161uL);
      if ((blockSize < '1') || (blockSize > '9')) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504162uL);
        throw new IOException("Stream is not BZip2 formatted: illegal " + "blocksize " + (char)blockSize);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504163uL);
      this.blockSize100k = blockSize - '0';
      InitBlock();
      SetupBlock();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504164uL);
    }
    void CheckMagicChar(char expected, int position)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504165uL);
      int magic = this.input.ReadByte();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504166uL);
      if (magic != (int)expected) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504167uL);
        var msg = String.Format("Not a valid BZip2 stream. byte {0}, expected '{1}', got '{2}'", position, (int)expected, magic);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504168uL);
        throw new IOException(msg);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504169uL);
    }
    void InitBlock()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504170uL);
      char magic0 = bsGetUByte();
      char magic1 = bsGetUByte();
      char magic2 = bsGetUByte();
      char magic3 = bsGetUByte();
      char magic4 = bsGetUByte();
      char magic5 = bsGetUByte();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504171uL);
      if (magic0 == 0x17 && magic1 == 0x72 && magic2 == 0x45 && magic3 == 0x38 && magic4 == 0x50 && magic5 == 0x90) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504172uL);
        complete();
      } else if (magic0 != 0x31 || magic1 != 0x41 || magic2 != 0x59 || magic3 != 0x26 || magic4 != 0x53 || magic5 != 0x59) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504177uL);
        this.currentState = CState.EOF;
        var msg = String.Format("bad block header at offset 0x{0:X}", this.input.Position);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504178uL);
        throw new IOException(msg);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504173uL);
        this.storedBlockCRC = bsGetInt();
        this.blockRandomised = (GetBits(1) == 1);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504174uL);
        if (this.data == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504175uL);
          this.data = new DecompressionState(this.blockSize100k);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504176uL);
        getAndMoveToFrontDecode();
        this.crc.Reset();
        this.currentState = CState.START_BLOCK;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504179uL);
    }
    private void EndBlock()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504180uL);
      this.computedBlockCRC = (uint)this.crc.Crc32Result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504181uL);
      if (this.storedBlockCRC != this.computedBlockCRC) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504182uL);
        var msg = String.Format("BZip2 CRC error (expected {0:X8}, computed {1:X8})", this.storedBlockCRC, this.computedBlockCRC);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504183uL);
        throw new IOException(msg);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504184uL);
      this.computedCombinedCRC = (this.computedCombinedCRC << 1) | (this.computedCombinedCRC >> 31);
      this.computedCombinedCRC ^= this.computedBlockCRC;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504185uL);
    }
    private void complete()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504186uL);
      this.storedCombinedCRC = bsGetInt();
      this.currentState = CState.EOF;
      this.data = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504187uL);
      if (this.storedCombinedCRC != this.computedCombinedCRC) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504188uL);
        var msg = String.Format("BZip2 CRC error (expected {0:X8}, computed {1:X8})", this.storedCombinedCRC, this.computedCombinedCRC);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504189uL);
        throw new IOException(msg);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504190uL);
    }
    public override void Close()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504191uL);
      Stream inShadow = this.input;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504192uL);
      if (inShadow != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504193uL);
        try {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504194uL);
          if (!this._leaveOpen) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504195uL);
            inShadow.Close();
          }
        } finally {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504196uL);
          this.data = null;
          this.input = null;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504197uL);
    }
    private int GetBits(int n)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504198uL);
      int bsLiveShadow = this.bsLive;
      int bsBuffShadow = this.bsBuff;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504199uL);
      if (bsLiveShadow < n) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504200uL);
        do {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504201uL);
          int thech = this.input.ReadByte();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504202uL);
          if (thech < 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504203uL);
            throw new IOException("unexpected end of stream");
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504204uL);
          bsBuffShadow = (bsBuffShadow << 8) | thech;
          bsLiveShadow += 8;
        } while (bsLiveShadow < n);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504205uL);
        this.bsBuff = bsBuffShadow;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504206uL);
      this.bsLive = bsLiveShadow - n;
      System.Int32 RNTRNTRNT_493 = (bsBuffShadow >> (bsLiveShadow - n)) & ((1 << n) - 1);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504207uL);
      return RNTRNTRNT_493;
    }
    private bool bsGetBit()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504208uL);
      int bit = GetBits(1);
      System.Boolean RNTRNTRNT_494 = bit != 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504209uL);
      return RNTRNTRNT_494;
    }
    private char bsGetUByte()
    {
      System.Char RNTRNTRNT_495 = (char)GetBits(8);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504210uL);
      return RNTRNTRNT_495;
    }
    private uint bsGetInt()
    {
      System.UInt32 RNTRNTRNT_496 = (uint)((((((GetBits(8) << 8) | GetBits(8)) << 8) | GetBits(8)) << 8) | GetBits(8));
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504211uL);
      return RNTRNTRNT_496;
    }
    private static void hbCreateDecodeTables(int[] limit, int[] bbase, int[] perm, char[] length, int minLen, int maxLen, int alphaSize)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504212uL);
      for (int i = minLen, pp = 0; i <= maxLen; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504213uL);
        for (int j = 0; j < alphaSize; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504214uL);
          if (length[j] == i) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504215uL);
            perm[pp++] = j;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504216uL);
      for (int i = BZip2.MaxCodeLength; --i > 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504217uL);
        bbase[i] = 0;
        limit[i] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504218uL);
      for (int i = 0; i < alphaSize; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504219uL);
        bbase[length[i] + 1]++;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504220uL);
      for (int i = 1, b = bbase[0]; i < BZip2.MaxCodeLength; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504221uL);
        b += bbase[i];
        bbase[i] = b;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504222uL);
      for (int i = minLen, vec = 0, b = bbase[i]; i <= maxLen; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504223uL);
        int nb = bbase[i + 1];
        vec += nb - b;
        b = nb;
        limit[i] = vec - 1;
        vec <<= 1;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504224uL);
      for (int i = minLen + 1; i <= maxLen; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504225uL);
        bbase[i] = ((limit[i - 1] + 1) << 1) - bbase[i];
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504226uL);
    }
    private void recvDecodingTables()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504227uL);
      var s = this.data;
      bool[] inUse = s.inUse;
      byte[] pos = s.recvDecodingTables_pos;
      int inUse16 = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504228uL);
      for (int i = 0; i < 16; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504229uL);
        if (bsGetBit()) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504230uL);
          inUse16 |= 1 << i;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504231uL);
      for (int i = 256; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504232uL);
        inUse[i] = false;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504233uL);
      for (int i = 0; i < 16; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504234uL);
        if ((inUse16 & (1 << i)) != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504235uL);
          int i16 = i << 4;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504236uL);
          for (int j = 0; j < 16; j++) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504237uL);
            if (bsGetBit()) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504238uL);
              inUse[i16 + j] = true;
            }
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504239uL);
      MakeMaps();
      int alphaSize = this.nInUse + 2;
      int nGroups = GetBits(3);
      int nSelectors = GetBits(15);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504240uL);
      for (int i = 0; i < nSelectors; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504241uL);
        int j = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504242uL);
        while (bsGetBit()) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504243uL);
          j++;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504244uL);
        s.selectorMtf[i] = (byte)j;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504245uL);
      for (int v = nGroups; --v >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504246uL);
        pos[v] = (byte)v;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504247uL);
      for (int i = 0; i < nSelectors; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504248uL);
        int v = s.selectorMtf[i];
        byte tmp = pos[v];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504249uL);
        while (v > 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504250uL);
          pos[v] = pos[v - 1];
          v--;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504251uL);
        pos[0] = tmp;
        s.selector[i] = tmp;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504252uL);
      char[][] len = s.temp_charArray2d;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504253uL);
      for (int t = 0; t < nGroups; t++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504254uL);
        int curr = GetBits(5);
        char[] len_t = len[t];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504255uL);
        for (int i = 0; i < alphaSize; i++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504256uL);
          while (bsGetBit()) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504257uL);
            curr += bsGetBit() ? -1 : 1;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504258uL);
          len_t[i] = (char)curr;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504259uL);
      createHuffmanDecodingTables(alphaSize, nGroups);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504260uL);
    }
    private void createHuffmanDecodingTables(int alphaSize, int nGroups)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504261uL);
      var s = this.data;
      char[][] len = s.temp_charArray2d;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504262uL);
      for (int t = 0; t < nGroups; t++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504263uL);
        int minLen = 32;
        int maxLen = 0;
        char[] len_t = len[t];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504264uL);
        for (int i = alphaSize; --i >= 0;) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504265uL);
          char lent = len_t[i];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504266uL);
          if (lent > maxLen) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504267uL);
            maxLen = lent;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504268uL);
          if (lent < minLen) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504269uL);
            minLen = lent;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504270uL);
        hbCreateDecodeTables(s.gLimit[t], s.gBase[t], s.gPerm[t], len[t], minLen, maxLen, alphaSize);
        s.gMinlen[t] = minLen;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504271uL);
    }
    private void getAndMoveToFrontDecode()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504272uL);
      var s = this.data;
      this.origPtr = GetBits(24);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504273uL);
      if (this.origPtr < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504274uL);
        throw new IOException("BZ_DATA_ERROR");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504275uL);
      if (this.origPtr > 10 + BZip2.BlockSizeMultiple * this.blockSize100k) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504276uL);
        throw new IOException("BZ_DATA_ERROR");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504277uL);
      recvDecodingTables();
      byte[] yy = s.getAndMoveToFrontDecode_yy;
      int limitLast = this.blockSize100k * BZip2.BlockSizeMultiple;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504278uL);
      for (int i = 256; --i >= 0;) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504279uL);
        yy[i] = (byte)i;
        s.unzftab[i] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504280uL);
      int groupNo = 0;
      int groupPos = BZip2.G_SIZE - 1;
      int eob = this.nInUse + 1;
      int nextSym = getAndMoveToFrontDecode0(0);
      int bsBuffShadow = this.bsBuff;
      int bsLiveShadow = this.bsLive;
      int lastShadow = -1;
      int zt = s.selector[groupNo] & 0xff;
      int[] base_zt = s.gBase[zt];
      int[] limit_zt = s.gLimit[zt];
      int[] perm_zt = s.gPerm[zt];
      int minLens_zt = s.gMinlen[zt];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504281uL);
      while (nextSym != eob) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504282uL);
        if ((nextSym == BZip2.RUNA) || (nextSym == BZip2.RUNB)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504283uL);
          int es = -1;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504284uL);
          for (int n = 1; true; n <<= 1) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504285uL);
            if (nextSym == BZip2.RUNA) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504286uL);
              es += n;
            } else if (nextSym == BZip2.RUNB) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504288uL);
              es += n << 1;
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504287uL);
              break;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504289uL);
            if (groupPos == 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504290uL);
              groupPos = BZip2.G_SIZE - 1;
              zt = s.selector[++groupNo] & 0xff;
              base_zt = s.gBase[zt];
              limit_zt = s.gLimit[zt];
              perm_zt = s.gPerm[zt];
              minLens_zt = s.gMinlen[zt];
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504291uL);
              groupPos--;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504292uL);
            int zn = minLens_zt;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504293uL);
            while (bsLiveShadow < zn) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504294uL);
              int thech = this.input.ReadByte();
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504295uL);
              if (thech >= 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504296uL);
                bsBuffShadow = (bsBuffShadow << 8) | thech;
                bsLiveShadow += 8;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504297uL);
                continue;
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504298uL);
                throw new IOException("unexpected end of stream");
              }
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504299uL);
            int zvec = (bsBuffShadow >> (bsLiveShadow - zn)) & ((1 << zn) - 1);
            bsLiveShadow -= zn;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504300uL);
            while (zvec > limit_zt[zn]) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504301uL);
              zn++;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504302uL);
              while (bsLiveShadow < 1) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504303uL);
                int thech = this.input.ReadByte();
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504304uL);
                if (thech >= 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504305uL);
                  bsBuffShadow = (bsBuffShadow << 8) | thech;
                  bsLiveShadow += 8;
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504306uL);
                  continue;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504307uL);
                  throw new IOException("unexpected end of stream");
                }
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504308uL);
              bsLiveShadow--;
              zvec = (zvec << 1) | ((bsBuffShadow >> bsLiveShadow) & 1);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504309uL);
            nextSym = perm_zt[zvec - base_zt[zn]];
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504310uL);
          byte ch = s.seqToUnseq[yy[0]];
          s.unzftab[ch & 0xff] += es + 1;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504311uL);
          while (es-- >= 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504312uL);
            s.ll8[++lastShadow] = ch;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504313uL);
          if (lastShadow >= limitLast) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504314uL);
            throw new IOException("block overrun");
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504315uL);
          if (++lastShadow >= limitLast) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504316uL);
            throw new IOException("block overrun");
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504317uL);
          byte tmp = yy[nextSym - 1];
          s.unzftab[s.seqToUnseq[tmp] & 0xff]++;
          s.ll8[lastShadow] = s.seqToUnseq[tmp];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504318uL);
          if (nextSym <= 16) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504319uL);
            for (int j = nextSym - 1; j > 0;) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504320uL);
              yy[j] = yy[--j];
            }
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504321uL);
            System.Buffer.BlockCopy(yy, 0, yy, 1, nextSym - 1);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504322uL);
          yy[0] = tmp;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504323uL);
          if (groupPos == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504324uL);
            groupPos = BZip2.G_SIZE - 1;
            zt = s.selector[++groupNo] & 0xff;
            base_zt = s.gBase[zt];
            limit_zt = s.gLimit[zt];
            perm_zt = s.gPerm[zt];
            minLens_zt = s.gMinlen[zt];
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504325uL);
            groupPos--;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504326uL);
          int zn = minLens_zt;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504327uL);
          while (bsLiveShadow < zn) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504328uL);
            int thech = this.input.ReadByte();
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504329uL);
            if (thech >= 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504330uL);
              bsBuffShadow = (bsBuffShadow << 8) | thech;
              bsLiveShadow += 8;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504331uL);
              continue;
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504332uL);
              throw new IOException("unexpected end of stream");
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504333uL);
          int zvec = (bsBuffShadow >> (bsLiveShadow - zn)) & ((1 << zn) - 1);
          bsLiveShadow -= zn;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504334uL);
          while (zvec > limit_zt[zn]) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504335uL);
            zn++;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504336uL);
            while (bsLiveShadow < 1) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504337uL);
              int thech = this.input.ReadByte();
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504338uL);
              if (thech >= 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504339uL);
                bsBuffShadow = (bsBuffShadow << 8) | thech;
                bsLiveShadow += 8;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504340uL);
                continue;
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504341uL);
                throw new IOException("unexpected end of stream");
              }
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504342uL);
            bsLiveShadow--;
            zvec = (zvec << 1) | ((bsBuffShadow >> bsLiveShadow) & 1);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504343uL);
          nextSym = perm_zt[zvec - base_zt[zn]];
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504344uL);
      this.last = lastShadow;
      this.bsLive = bsLiveShadow;
      this.bsBuff = bsBuffShadow;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504345uL);
    }
    private int getAndMoveToFrontDecode0(int groupNo)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504346uL);
      var s = this.data;
      int zt = s.selector[groupNo] & 0xff;
      int[] limit_zt = s.gLimit[zt];
      int zn = s.gMinlen[zt];
      int zvec = GetBits(zn);
      int bsLiveShadow = this.bsLive;
      int bsBuffShadow = this.bsBuff;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504347uL);
      while (zvec > limit_zt[zn]) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504348uL);
        zn++;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504349uL);
        while (bsLiveShadow < 1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504350uL);
          int thech = this.input.ReadByte();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504351uL);
          if (thech >= 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504352uL);
            bsBuffShadow = (bsBuffShadow << 8) | thech;
            bsLiveShadow += 8;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504353uL);
            continue;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504354uL);
            throw new IOException("unexpected end of stream");
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504355uL);
        bsLiveShadow--;
        zvec = (zvec << 1) | ((bsBuffShadow >> bsLiveShadow) & 1);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504356uL);
      this.bsLive = bsLiveShadow;
      this.bsBuff = bsBuffShadow;
      System.Int32 RNTRNTRNT_497 = s.gPerm[zt][zvec - s.gBase[zt][zn]];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504357uL);
      return RNTRNTRNT_497;
    }
    private void SetupBlock()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504358uL);
      if (this.data == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504359uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504360uL);
      int i;
      var s = this.data;
      int[] tt = s.initTT(this.last + 1);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504361uL);
      for (i = 0; i <= 255; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504362uL);
        if (s.unzftab[i] < 0 || s.unzftab[i] > this.last) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504363uL);
          throw new Exception("BZ_DATA_ERROR");
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504364uL);
      s.cftab[0] = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504365uL);
      for (i = 1; i <= 256; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504366uL);
        s.cftab[i] = s.unzftab[i - 1];
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504367uL);
      for (i = 1; i <= 256; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504368uL);
        s.cftab[i] += s.cftab[i - 1];
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504369uL);
      for (i = 0; i <= 256; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504370uL);
        if (s.cftab[i] < 0 || s.cftab[i] > this.last + 1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504371uL);
          var msg = String.Format("BZ_DATA_ERROR: cftab[{0}]={1} last={2}", i, s.cftab[i], this.last);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504372uL);
          throw new Exception(msg);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504373uL);
      for (i = 1; i <= 256; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504374uL);
        if (s.cftab[i - 1] > s.cftab[i]) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504375uL);
          throw new Exception("BZ_DATA_ERROR");
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504376uL);
      int lastShadow;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504377uL);
      for (i = 0,lastShadow = this.last; i <= lastShadow; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504378uL);
        tt[s.cftab[s.ll8[i] & 0xff]++] = i;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504379uL);
      if ((this.origPtr < 0) || (this.origPtr >= tt.Length)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504380uL);
        throw new IOException("stream corrupted");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504381uL);
      this.su_tPos = tt[this.origPtr];
      this.su_count = 0;
      this.su_i2 = 0;
      this.su_ch2 = 256;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504382uL);
      if (this.blockRandomised) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504383uL);
        this.su_rNToGo = 0;
        this.su_rTPos = 0;
        SetupRandPartA();
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504384uL);
        SetupNoRandPartA();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504385uL);
    }
    private void SetupRandPartA()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504386uL);
      if (this.su_i2 <= this.last) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504387uL);
        this.su_chPrev = this.su_ch2;
        int su_ch2Shadow = this.data.ll8[this.su_tPos] & 0xff;
        this.su_tPos = this.data.tt[this.su_tPos];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504388uL);
        if (this.su_rNToGo == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504389uL);
          this.su_rNToGo = Rand.Rnums(this.su_rTPos) - 1;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504390uL);
          if (++this.su_rTPos == 512) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504391uL);
            this.su_rTPos = 0;
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504392uL);
          this.su_rNToGo--;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504393uL);
        this.su_ch2 = su_ch2Shadow ^= (this.su_rNToGo == 1) ? 1 : 0;
        this.su_i2++;
        this.currentChar = su_ch2Shadow;
        this.currentState = CState.RAND_PART_B;
        this.crc.UpdateCRC((byte)su_ch2Shadow);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504394uL);
        EndBlock();
        InitBlock();
        SetupBlock();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504395uL);
    }
    private void SetupNoRandPartA()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504396uL);
      if (this.su_i2 <= this.last) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504397uL);
        this.su_chPrev = this.su_ch2;
        int su_ch2Shadow = this.data.ll8[this.su_tPos] & 0xff;
        this.su_ch2 = su_ch2Shadow;
        this.su_tPos = this.data.tt[this.su_tPos];
        this.su_i2++;
        this.currentChar = su_ch2Shadow;
        this.currentState = CState.NO_RAND_PART_B;
        this.crc.UpdateCRC((byte)su_ch2Shadow);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504398uL);
        this.currentState = CState.NO_RAND_PART_A;
        EndBlock();
        InitBlock();
        SetupBlock();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504399uL);
    }
    private void SetupRandPartB()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504400uL);
      if (this.su_ch2 != this.su_chPrev) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504401uL);
        this.currentState = CState.RAND_PART_A;
        this.su_count = 1;
        SetupRandPartA();
      } else if (++this.su_count >= 4) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504403uL);
        this.su_z = (char)(this.data.ll8[this.su_tPos] & 0xff);
        this.su_tPos = this.data.tt[this.su_tPos];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504404uL);
        if (this.su_rNToGo == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504405uL);
          this.su_rNToGo = Rand.Rnums(this.su_rTPos) - 1;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504406uL);
          if (++this.su_rTPos == 512) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504407uL);
            this.su_rTPos = 0;
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504408uL);
          this.su_rNToGo--;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504409uL);
        this.su_j2 = 0;
        this.currentState = CState.RAND_PART_C;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504410uL);
        if (this.su_rNToGo == 1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504411uL);
          this.su_z ^= (char)1;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504412uL);
        SetupRandPartC();
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504402uL);
        this.currentState = CState.RAND_PART_A;
        SetupRandPartA();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504413uL);
    }
    private void SetupRandPartC()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504414uL);
      if (this.su_j2 < this.su_z) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504415uL);
        this.currentChar = this.su_ch2;
        this.crc.UpdateCRC((byte)this.su_ch2);
        this.su_j2++;
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504416uL);
        this.currentState = CState.RAND_PART_A;
        this.su_i2++;
        this.su_count = 0;
        SetupRandPartA();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504417uL);
    }
    private void SetupNoRandPartB()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504418uL);
      if (this.su_ch2 != this.su_chPrev) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504419uL);
        this.su_count = 1;
        SetupNoRandPartA();
      } else if (++this.su_count >= 4) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504421uL);
        this.su_z = (char)(this.data.ll8[this.su_tPos] & 0xff);
        this.su_tPos = this.data.tt[this.su_tPos];
        this.su_j2 = 0;
        SetupNoRandPartC();
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504420uL);
        SetupNoRandPartA();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504422uL);
    }
    private void SetupNoRandPartC()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504423uL);
      if (this.su_j2 < this.su_z) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504424uL);
        int su_ch2Shadow = this.su_ch2;
        this.currentChar = su_ch2Shadow;
        this.crc.UpdateCRC((byte)su_ch2Shadow);
        this.su_j2++;
        this.currentState = CState.NO_RAND_PART_C;
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504425uL);
        this.su_i2++;
        this.su_count = 0;
        SetupNoRandPartA();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504426uL);
    }
    private sealed class DecompressionState
    {
      public readonly bool[] inUse = new bool[256];
      public readonly byte[] seqToUnseq = new byte[256];
      public readonly byte[] selector = new byte[BZip2.MaxSelectors];
      public readonly byte[] selectorMtf = new byte[BZip2.MaxSelectors];
      public readonly int[] unzftab;
      public readonly int[][] gLimit;
      public readonly int[][] gBase;
      public readonly int[][] gPerm;
      public readonly int[] gMinlen;
      public readonly int[] cftab;
      public readonly byte[] getAndMoveToFrontDecode_yy;
      public readonly char[][] temp_charArray2d;
      public readonly byte[] recvDecodingTables_pos;
      public int[] tt;
      public byte[] ll8;
      public DecompressionState(int blockSize100k)
      {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504427uL);
        this.unzftab = new int[256];
        this.gLimit = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.gBase = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.gPerm = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.gMinlen = new int[BZip2.NGroups];
        this.cftab = new int[257];
        this.getAndMoveToFrontDecode_yy = new byte[256];
        this.temp_charArray2d = BZip2.InitRectangularArray<char>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.recvDecodingTables_pos = new byte[BZip2.NGroups];
        this.ll8 = new byte[blockSize100k * BZip2.BlockSizeMultiple];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504428uL);
      }
      public int[] initTT(int length)
      {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504429uL);
        int[] ttShadow = this.tt;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504430uL);
        if ((ttShadow == null) || (ttShadow.Length < length)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504431uL);
          this.tt = ttShadow = new int[length];
        }
        System.Int32[] RNTRNTRNT_498 = ttShadow;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504432uL);
        return RNTRNTRNT_498;
      }
    }
  }
  static internal class BZip2
  {
    static internal T[][] InitRectangularArray<T>(int d1, int d2)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504433uL);
      var x = new T[d1][];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504434uL);
      for (int i = 0; i < d1; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504435uL);
        x[i] = new T[d2];
      }
      T[][] RNTRNTRNT_499 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504436uL);
      return RNTRNTRNT_499;
    }
    public static readonly int BlockSizeMultiple = 100000;
    public static readonly int MinBlockSize = 1;
    public static readonly int MaxBlockSize = 9;
    public static readonly int MaxAlphaSize = 258;
    public static readonly int MaxCodeLength = 23;
    public static readonly char RUNA = (char)0;
    public static readonly char RUNB = (char)1;
    public static readonly int NGroups = 6;
    public static readonly int G_SIZE = 50;
    public static readonly int N_ITERS = 4;
    public static readonly int MaxSelectors = (2 + (900000 / G_SIZE));
    public static readonly int NUM_OVERSHOOT_BYTES = 20;
    static internal readonly int QSORT_STACK_SIZE = 1000;
  }
}
