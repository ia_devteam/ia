using System;
namespace Ionic.Zlib
{
  public class DeflateStream : System.IO.Stream
  {
    internal ZlibBaseStream _baseStream;
    internal System.IO.Stream _innerStream;
    bool _disposed;
    public DeflateStream(System.IO.Stream stream, CompressionMode mode) : this(stream, mode, CompressionLevel.Default, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505052uL);
    }
    public DeflateStream(System.IO.Stream stream, CompressionMode mode, CompressionLevel level) : this(stream, mode, level, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505053uL);
    }
    public DeflateStream(System.IO.Stream stream, CompressionMode mode, bool leaveOpen) : this(stream, mode, CompressionLevel.Default, leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505054uL);
    }
    public DeflateStream(System.IO.Stream stream, CompressionMode mode, CompressionLevel level, bool leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505055uL);
      _innerStream = stream;
      _baseStream = new ZlibBaseStream(stream, mode, level, ZlibStreamFlavor.DEFLATE, leaveOpen);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505056uL);
    }
    public virtual FlushType FlushMode {
      get {
        FlushType RNTRNTRNT_553 = (this._baseStream._flushMode);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505057uL);
        return RNTRNTRNT_553;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505058uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505059uL);
          throw new ObjectDisposedException("DeflateStream");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505060uL);
        this._baseStream._flushMode = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505061uL);
      }
    }
    public int BufferSize {
      get {
        System.Int32 RNTRNTRNT_554 = this._baseStream._bufferSize;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505062uL);
        return RNTRNTRNT_554;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505063uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505064uL);
          throw new ObjectDisposedException("DeflateStream");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505065uL);
        if (this._baseStream._workingBuffer != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505066uL);
          throw new ZlibException("The working buffer is already set.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505067uL);
        if (value < ZlibConstants.WorkingBufferSizeMin) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505068uL);
          throw new ZlibException(String.Format("Don't be silly. {0} bytes?? Use a bigger buffer, at least {1}.", value, ZlibConstants.WorkingBufferSizeMin));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505069uL);
        this._baseStream._bufferSize = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505070uL);
      }
    }
    public CompressionStrategy Strategy {
      get {
        CompressionStrategy RNTRNTRNT_555 = this._baseStream.Strategy;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505071uL);
        return RNTRNTRNT_555;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505072uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505073uL);
          throw new ObjectDisposedException("DeflateStream");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505074uL);
        this._baseStream.Strategy = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505075uL);
      }
    }
    public virtual long TotalIn {
      get {
        System.Int64 RNTRNTRNT_556 = this._baseStream._z.TotalBytesIn;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505076uL);
        return RNTRNTRNT_556;
      }
    }
    public virtual long TotalOut {
      get {
        System.Int64 RNTRNTRNT_557 = this._baseStream._z.TotalBytesOut;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505077uL);
        return RNTRNTRNT_557;
      }
    }
    protected override void Dispose(bool disposing)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505078uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505079uL);
        if (!_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505080uL);
          if (disposing && (this._baseStream != null)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505081uL);
            this._baseStream.Close();
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505082uL);
          _disposed = true;
        }
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505083uL);
        base.Dispose(disposing);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505084uL);
    }
    public override bool CanRead {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505085uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505086uL);
          throw new ObjectDisposedException("DeflateStream");
        }
        System.Boolean RNTRNTRNT_558 = _baseStream._stream.CanRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505087uL);
        return RNTRNTRNT_558;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_559 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505088uL);
        return RNTRNTRNT_559;
      }
    }
    public override bool CanWrite {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505089uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505090uL);
          throw new ObjectDisposedException("DeflateStream");
        }
        System.Boolean RNTRNTRNT_560 = _baseStream._stream.CanWrite;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505091uL);
        return RNTRNTRNT_560;
      }
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505092uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505093uL);
        throw new ObjectDisposedException("DeflateStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505094uL);
      _baseStream.Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505095uL);
    }
    public override long Length {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505096uL);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505097uL);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Writer) {
          System.Int64 RNTRNTRNT_561 = this._baseStream._z.TotalBytesOut;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505098uL);
          return RNTRNTRNT_561;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505099uL);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Reader) {
          System.Int64 RNTRNTRNT_562 = this._baseStream._z.TotalBytesIn;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505100uL);
          return RNTRNTRNT_562;
        }
        System.Int64 RNTRNTRNT_563 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505101uL);
        return RNTRNTRNT_563;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505102uL);
        throw new NotImplementedException();
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505103uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505104uL);
        throw new ObjectDisposedException("DeflateStream");
      }
      System.Int32 RNTRNTRNT_564 = _baseStream.Read(buffer, offset, count);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505105uL);
      return RNTRNTRNT_564;
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505106uL);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505107uL);
      throw new NotImplementedException();
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505108uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505109uL);
        throw new ObjectDisposedException("DeflateStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505110uL);
      _baseStream.Write(buffer, offset, count);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505111uL);
    }
    public static byte[] CompressString(String s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505112uL);
      using (var ms = new System.IO.MemoryStream()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505113uL);
        System.IO.Stream compressor = new DeflateStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressString(s, compressor);
        System.Byte[] RNTRNTRNT_565 = ms.ToArray();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505114uL);
        return RNTRNTRNT_565;
      }
    }
    public static byte[] CompressBuffer(byte[] b)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505115uL);
      using (var ms = new System.IO.MemoryStream()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505116uL);
        System.IO.Stream compressor = new DeflateStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressBuffer(b, compressor);
        System.Byte[] RNTRNTRNT_566 = ms.ToArray();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505117uL);
        return RNTRNTRNT_566;
      }
    }
    public static String UncompressString(byte[] compressed)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505118uL);
      using (var input = new System.IO.MemoryStream(compressed)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505119uL);
        System.IO.Stream decompressor = new DeflateStream(input, CompressionMode.Decompress);
        String RNTRNTRNT_567 = ZlibBaseStream.UncompressString(compressed, decompressor);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505120uL);
        return RNTRNTRNT_567;
      }
    }
    public static byte[] UncompressBuffer(byte[] compressed)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505121uL);
      using (var input = new System.IO.MemoryStream(compressed)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505122uL);
        System.IO.Stream decompressor = new DeflateStream(input, CompressionMode.Decompress);
        System.Byte[] RNTRNTRNT_568 = ZlibBaseStream.UncompressBuffer(compressed, decompressor);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505123uL);
        return RNTRNTRNT_568;
      }
    }
  }
}
