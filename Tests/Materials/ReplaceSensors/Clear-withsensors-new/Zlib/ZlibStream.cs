using System;
using System.IO;
namespace Ionic.Zlib
{
  public class ZlibStream : System.IO.Stream
  {
    internal ZlibBaseStream _baseStream;
    bool _disposed;
    public ZlibStream(System.IO.Stream stream, CompressionMode mode) : this(stream, mode, CompressionLevel.Default, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506383uL);
    }
    public ZlibStream(System.IO.Stream stream, CompressionMode mode, CompressionLevel level) : this(stream, mode, level, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506384uL);
    }
    public ZlibStream(System.IO.Stream stream, CompressionMode mode, bool leaveOpen) : this(stream, mode, CompressionLevel.Default, leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506385uL);
    }
    public ZlibStream(System.IO.Stream stream, CompressionMode mode, CompressionLevel level, bool leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506386uL);
      _baseStream = new ZlibBaseStream(stream, mode, level, ZlibStreamFlavor.ZLIB, leaveOpen);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506387uL);
    }
    public virtual FlushType FlushMode {
      get {
        FlushType RNTRNTRNT_722 = (this._baseStream._flushMode);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506388uL);
        return RNTRNTRNT_722;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506389uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506390uL);
          throw new ObjectDisposedException("ZlibStream");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506391uL);
        this._baseStream._flushMode = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506392uL);
      }
    }
    public int BufferSize {
      get {
        System.Int32 RNTRNTRNT_723 = this._baseStream._bufferSize;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506393uL);
        return RNTRNTRNT_723;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506394uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506395uL);
          throw new ObjectDisposedException("ZlibStream");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506396uL);
        if (this._baseStream._workingBuffer != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506397uL);
          throw new ZlibException("The working buffer is already set.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506398uL);
        if (value < ZlibConstants.WorkingBufferSizeMin) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506399uL);
          throw new ZlibException(String.Format("Don't be silly. {0} bytes?? Use a bigger buffer, at least {1}.", value, ZlibConstants.WorkingBufferSizeMin));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506400uL);
        this._baseStream._bufferSize = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506401uL);
      }
    }
    public virtual long TotalIn {
      get {
        System.Int64 RNTRNTRNT_724 = this._baseStream._z.TotalBytesIn;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506402uL);
        return RNTRNTRNT_724;
      }
    }
    public virtual long TotalOut {
      get {
        System.Int64 RNTRNTRNT_725 = this._baseStream._z.TotalBytesOut;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506403uL);
        return RNTRNTRNT_725;
      }
    }
    protected override void Dispose(bool disposing)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506404uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506405uL);
        if (!_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506406uL);
          if (disposing && (this._baseStream != null)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506407uL);
            this._baseStream.Close();
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506408uL);
          _disposed = true;
        }
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506409uL);
        base.Dispose(disposing);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506410uL);
    }
    public override bool CanRead {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506411uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506412uL);
          throw new ObjectDisposedException("ZlibStream");
        }
        System.Boolean RNTRNTRNT_726 = _baseStream._stream.CanRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506413uL);
        return RNTRNTRNT_726;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_727 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506414uL);
        return RNTRNTRNT_727;
      }
    }
    public override bool CanWrite {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506415uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506416uL);
          throw new ObjectDisposedException("ZlibStream");
        }
        System.Boolean RNTRNTRNT_728 = _baseStream._stream.CanWrite;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506417uL);
        return RNTRNTRNT_728;
      }
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506418uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506419uL);
        throw new ObjectDisposedException("ZlibStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506420uL);
      _baseStream.Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506421uL);
    }
    public override long Length {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506422uL);
        throw new NotSupportedException();
      }
    }
    public override long Position {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506423uL);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Writer) {
          System.Int64 RNTRNTRNT_729 = this._baseStream._z.TotalBytesOut;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506424uL);
          return RNTRNTRNT_729;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506425uL);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Reader) {
          System.Int64 RNTRNTRNT_730 = this._baseStream._z.TotalBytesIn;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506426uL);
          return RNTRNTRNT_730;
        }
        System.Int64 RNTRNTRNT_731 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506427uL);
        return RNTRNTRNT_731;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506428uL);
        throw new NotSupportedException();
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506429uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506430uL);
        throw new ObjectDisposedException("ZlibStream");
      }
      System.Int32 RNTRNTRNT_732 = _baseStream.Read(buffer, offset, count);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506431uL);
      return RNTRNTRNT_732;
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506432uL);
      throw new NotSupportedException();
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506433uL);
      throw new NotSupportedException();
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506434uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506435uL);
        throw new ObjectDisposedException("ZlibStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506436uL);
      _baseStream.Write(buffer, offset, count);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506437uL);
    }
    public static byte[] CompressString(String s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506438uL);
      using (var ms = new MemoryStream()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506439uL);
        Stream compressor = new ZlibStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressString(s, compressor);
        System.Byte[] RNTRNTRNT_733 = ms.ToArray();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506440uL);
        return RNTRNTRNT_733;
      }
    }
    public static byte[] CompressBuffer(byte[] b)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506441uL);
      using (var ms = new MemoryStream()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506442uL);
        Stream compressor = new ZlibStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressBuffer(b, compressor);
        System.Byte[] RNTRNTRNT_734 = ms.ToArray();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506443uL);
        return RNTRNTRNT_734;
      }
    }
    public static String UncompressString(byte[] compressed)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506444uL);
      using (var input = new MemoryStream(compressed)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506445uL);
        Stream decompressor = new ZlibStream(input, CompressionMode.Decompress);
        String RNTRNTRNT_735 = ZlibBaseStream.UncompressString(compressed, decompressor);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506446uL);
        return RNTRNTRNT_735;
      }
    }
    public static byte[] UncompressBuffer(byte[] compressed)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506447uL);
      using (var input = new MemoryStream(compressed)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506448uL);
        Stream decompressor = new ZlibStream(input, CompressionMode.Decompress);
        System.Byte[] RNTRNTRNT_736 = ZlibBaseStream.UncompressBuffer(compressed, decompressor);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506449uL);
        return RNTRNTRNT_736;
      }
    }
  }
}
