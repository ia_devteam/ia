using System;
namespace Ionic.Zlib
{
  internal enum BlockState
  {
    NeedMore = 0,
    BlockDone,
    FinishStarted,
    FinishDone
  }
  internal enum DeflateFlavor
  {
    Store,
    Fast,
    Slow
  }
  internal sealed class DeflateManager
  {
    private static readonly int MEM_LEVEL_MAX = 9;
    private static readonly int MEM_LEVEL_DEFAULT = 8;
    internal delegate BlockState CompressFunc(FlushType flush);
    internal class Config
    {
      internal int GoodLength;
      internal int MaxLazy;
      internal int NiceLength;
      internal int MaxChainLength;
      internal DeflateFlavor Flavor;
      private Config(int goodLength, int maxLazy, int niceLength, int maxChainLength, DeflateFlavor flavor)
      {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504649uL);
        this.GoodLength = goodLength;
        this.MaxLazy = maxLazy;
        this.NiceLength = niceLength;
        this.MaxChainLength = maxChainLength;
        this.Flavor = flavor;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504650uL);
      }
      public static Config Lookup(CompressionLevel level)
      {
        Config RNTRNTRNT_513 = Table[(int)level];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504651uL);
        return RNTRNTRNT_513;
      }
      static Config()
      {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504652uL);
        Table = new Config[] {
          new Config(0, 0, 0, 0, DeflateFlavor.Store),
          new Config(4, 4, 8, 4, DeflateFlavor.Fast),
          new Config(4, 5, 16, 8, DeflateFlavor.Fast),
          new Config(4, 6, 32, 32, DeflateFlavor.Fast),
          new Config(4, 4, 16, 16, DeflateFlavor.Slow),
          new Config(8, 16, 32, 32, DeflateFlavor.Slow),
          new Config(8, 16, 128, 128, DeflateFlavor.Slow),
          new Config(8, 32, 128, 256, DeflateFlavor.Slow),
          new Config(32, 128, 258, 1024, DeflateFlavor.Slow),
          new Config(32, 258, 258, 4096, DeflateFlavor.Slow)
        };
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504653uL);
      }
      private static readonly Config[] Table;
    }
    private CompressFunc DeflateFunction;
    private static readonly System.String[] _ErrorMessage = new System.String[] {
      "need dictionary",
      "stream end",
      "",
      "file error",
      "stream error",
      "data error",
      "insufficient memory",
      "buffer error",
      "incompatible version",
      ""
    };
    private static readonly int PRESET_DICT = 0x20;
    private static readonly int INIT_STATE = 42;
    private static readonly int BUSY_STATE = 113;
    private static readonly int FINISH_STATE = 666;
    private static readonly int Z_DEFLATED = 8;
    private static readonly int STORED_BLOCK = 0;
    private static readonly int STATIC_TREES = 1;
    private static readonly int DYN_TREES = 2;
    private static readonly int Z_BINARY = 0;
    private static readonly int Z_ASCII = 1;
    private static readonly int Z_UNKNOWN = 2;
    private static readonly int Buf_size = 8 * 2;
    private static readonly int MIN_MATCH = 3;
    private static readonly int MAX_MATCH = 258;
    private static readonly int MIN_LOOKAHEAD = (MAX_MATCH + MIN_MATCH + 1);
    private static readonly int HEAP_SIZE = (2 * InternalConstants.L_CODES + 1);
    private static readonly int END_BLOCK = 256;
    internal ZlibCodec _codec;
    internal int status;
    internal byte[] pending;
    internal int nextPending;
    internal int pendingCount;
    internal sbyte data_type;
    internal int last_flush;
    internal int w_size;
    internal int w_bits;
    internal int w_mask;
    internal byte[] window;
    internal int window_size;
    internal short[] prev;
    internal short[] head;
    internal int ins_h;
    internal int hash_size;
    internal int hash_bits;
    internal int hash_mask;
    internal int hash_shift;
    internal int block_start;
    Config config;
    internal int match_length;
    internal int prev_match;
    internal int match_available;
    internal int strstart;
    internal int match_start;
    internal int lookahead;
    internal int prev_length;
    internal CompressionLevel compressionLevel;
    internal CompressionStrategy compressionStrategy;
    internal short[] dyn_ltree;
    internal short[] dyn_dtree;
    internal short[] bl_tree;
    internal Tree treeLiterals = new Tree();
    internal Tree treeDistances = new Tree();
    internal Tree treeBitLengths = new Tree();
    internal short[] bl_count = new short[InternalConstants.MAX_BITS + 1];
    internal int[] heap = new int[2 * InternalConstants.L_CODES + 1];
    internal int heap_len;
    internal int heap_max;
    internal sbyte[] depth = new sbyte[2 * InternalConstants.L_CODES + 1];
    internal int _lengthOffset;
    internal int lit_bufsize;
    internal int last_lit;
    internal int _distanceOffset;
    internal int opt_len;
    internal int static_len;
    internal int matches;
    internal int last_eob_len;
    internal short bi_buf;
    internal int bi_valid;
    internal DeflateManager()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504654uL);
      dyn_ltree = new short[HEAP_SIZE * 2];
      dyn_dtree = new short[(2 * InternalConstants.D_CODES + 1) * 2];
      bl_tree = new short[(2 * InternalConstants.BL_CODES + 1) * 2];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504655uL);
    }
    private void _InitializeLazyMatch()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504656uL);
      window_size = 2 * w_size;
      Array.Clear(head, 0, hash_size);
      config = Config.Lookup(compressionLevel);
      SetDeflater();
      strstart = 0;
      block_start = 0;
      lookahead = 0;
      match_length = prev_length = MIN_MATCH - 1;
      match_available = 0;
      ins_h = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504657uL);
    }
    private void _InitializeTreeData()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504658uL);
      treeLiterals.dyn_tree = dyn_ltree;
      treeLiterals.staticTree = StaticTree.Literals;
      treeDistances.dyn_tree = dyn_dtree;
      treeDistances.staticTree = StaticTree.Distances;
      treeBitLengths.dyn_tree = bl_tree;
      treeBitLengths.staticTree = StaticTree.BitLengths;
      bi_buf = 0;
      bi_valid = 0;
      last_eob_len = 8;
      _InitializeBlocks();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504659uL);
    }
    internal void _InitializeBlocks()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504660uL);
      for (int i = 0; i < InternalConstants.L_CODES; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504661uL);
        dyn_ltree[i * 2] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504662uL);
      for (int i = 0; i < InternalConstants.D_CODES; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504663uL);
        dyn_dtree[i * 2] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504664uL);
      for (int i = 0; i < InternalConstants.BL_CODES; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504665uL);
        bl_tree[i * 2] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504666uL);
      dyn_ltree[END_BLOCK * 2] = 1;
      opt_len = static_len = 0;
      last_lit = matches = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504667uL);
    }
    internal void pqdownheap(short[] tree, int k)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504668uL);
      int v = heap[k];
      int j = k << 1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504669uL);
      while (j <= heap_len) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504670uL);
        if (j < heap_len && _IsSmaller(tree, heap[j + 1], heap[j], depth)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504671uL);
          j++;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504672uL);
        if (_IsSmaller(tree, v, heap[j], depth)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504673uL);
          break;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504674uL);
        heap[k] = heap[j];
        k = j;
        j <<= 1;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504675uL);
      heap[k] = v;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504676uL);
    }
    static internal bool _IsSmaller(short[] tree, int n, int m, sbyte[] depth)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504677uL);
      short tn2 = tree[n * 2];
      short tm2 = tree[m * 2];
      System.Boolean RNTRNTRNT_514 = (tn2 < tm2 || (tn2 == tm2 && depth[n] <= depth[m]));
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504678uL);
      return RNTRNTRNT_514;
    }
    internal void scan_tree(short[] tree, int max_code)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504679uL);
      int n;
      int prevlen = -1;
      int curlen;
      int nextlen = (int)tree[0 * 2 + 1];
      int count = 0;
      int max_count = 7;
      int min_count = 4;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504680uL);
      if (nextlen == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504681uL);
        max_count = 138;
        min_count = 3;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504682uL);
      tree[(max_code + 1) * 2 + 1] = (short)0x7fff;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504683uL);
      for (n = 0; n <= max_code; n++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504684uL);
        curlen = nextlen;
        nextlen = (int)tree[(n + 1) * 2 + 1];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504685uL);
        if (++count < max_count && curlen == nextlen) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504686uL);
          continue;
        } else if (count < min_count) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504692uL);
          bl_tree[curlen * 2] = (short)(bl_tree[curlen * 2] + count);
        } else if (curlen != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504689uL);
          if (curlen != prevlen) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504690uL);
            bl_tree[curlen * 2]++;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504691uL);
          bl_tree[InternalConstants.REP_3_6 * 2]++;
        } else if (count <= 10) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504688uL);
          bl_tree[InternalConstants.REPZ_3_10 * 2]++;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504687uL);
          bl_tree[InternalConstants.REPZ_11_138 * 2]++;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504693uL);
        count = 0;
        prevlen = curlen;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504694uL);
        if (nextlen == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504695uL);
          max_count = 138;
          min_count = 3;
        } else if (curlen == nextlen) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504697uL);
          max_count = 6;
          min_count = 3;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504696uL);
          max_count = 7;
          min_count = 4;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504698uL);
    }
    internal int build_bl_tree()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504699uL);
      int max_blindex;
      scan_tree(dyn_ltree, treeLiterals.max_code);
      scan_tree(dyn_dtree, treeDistances.max_code);
      treeBitLengths.build_tree(this);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504700uL);
      for (max_blindex = InternalConstants.BL_CODES - 1; max_blindex >= 3; max_blindex--) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504701uL);
        if (bl_tree[Tree.bl_order[max_blindex] * 2 + 1] != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504702uL);
          break;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504703uL);
      opt_len += 3 * (max_blindex + 1) + 5 + 5 + 4;
      System.Int32 RNTRNTRNT_515 = max_blindex;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504704uL);
      return RNTRNTRNT_515;
    }
    internal void send_all_trees(int lcodes, int dcodes, int blcodes)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504705uL);
      int rank;
      send_bits(lcodes - 257, 5);
      send_bits(dcodes - 1, 5);
      send_bits(blcodes - 4, 4);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504706uL);
      for (rank = 0; rank < blcodes; rank++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504707uL);
        send_bits(bl_tree[Tree.bl_order[rank] * 2 + 1], 3);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504708uL);
      send_tree(dyn_ltree, lcodes - 1);
      send_tree(dyn_dtree, dcodes - 1);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504709uL);
    }
    internal void send_tree(short[] tree, int max_code)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504710uL);
      int n;
      int prevlen = -1;
      int curlen;
      int nextlen = tree[0 * 2 + 1];
      int count = 0;
      int max_count = 7;
      int min_count = 4;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504711uL);
      if (nextlen == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504712uL);
        max_count = 138;
        min_count = 3;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504713uL);
      for (n = 0; n <= max_code; n++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504714uL);
        curlen = nextlen;
        nextlen = tree[(n + 1) * 2 + 1];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504715uL);
        if (++count < max_count && curlen == nextlen) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504716uL);
          continue;
        } else if (count < min_count) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504722uL);
          do {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504723uL);
            send_code(curlen, bl_tree);
          } while (--count != 0);
        } else if (curlen != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504719uL);
          if (curlen != prevlen) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504720uL);
            send_code(curlen, bl_tree);
            count--;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504721uL);
          send_code(InternalConstants.REP_3_6, bl_tree);
          send_bits(count - 3, 2);
        } else if (count <= 10) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504718uL);
          send_code(InternalConstants.REPZ_3_10, bl_tree);
          send_bits(count - 3, 3);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504717uL);
          send_code(InternalConstants.REPZ_11_138, bl_tree);
          send_bits(count - 11, 7);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504724uL);
        count = 0;
        prevlen = curlen;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504725uL);
        if (nextlen == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504726uL);
          max_count = 138;
          min_count = 3;
        } else if (curlen == nextlen) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504728uL);
          max_count = 6;
          min_count = 3;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504727uL);
          max_count = 7;
          min_count = 4;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504729uL);
    }
    private void put_bytes(byte[] p, int start, int len)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504730uL);
      Array.Copy(p, start, pending, pendingCount, len);
      pendingCount += len;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504731uL);
    }
    internal void send_code(int c, short[] tree)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504732uL);
      int c2 = c * 2;
      send_bits((tree[c2] & 0xffff), (tree[c2 + 1] & 0xffff));
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504733uL);
    }
    internal void send_bits(int value, int length)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504734uL);
      int len = length;
      unchecked {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504735uL);
        if (bi_valid > (int)Buf_size - len) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504736uL);
          bi_buf |= (short)((value << bi_valid) & 0xffff);
          pending[pendingCount++] = (byte)bi_buf;
          pending[pendingCount++] = (byte)(bi_buf >> 8);
          bi_buf = (short)((uint)value >> (Buf_size - bi_valid));
          bi_valid += len - Buf_size;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504737uL);
          bi_buf |= (short)((value << bi_valid) & 0xffff);
          bi_valid += len;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504738uL);
    }
    internal void _tr_align()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504739uL);
      send_bits(STATIC_TREES << 1, 3);
      send_code(END_BLOCK, StaticTree.lengthAndLiteralsTreeCodes);
      bi_flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504740uL);
      if (1 + last_eob_len + 10 - bi_valid < 9) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504741uL);
        send_bits(STATIC_TREES << 1, 3);
        send_code(END_BLOCK, StaticTree.lengthAndLiteralsTreeCodes);
        bi_flush();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504742uL);
      last_eob_len = 7;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504743uL);
    }
    internal bool _tr_tally(int dist, int lc)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504744uL);
      pending[_distanceOffset + last_lit * 2] = unchecked((byte)((uint)dist >> 8));
      pending[_distanceOffset + last_lit * 2 + 1] = unchecked((byte)dist);
      pending[_lengthOffset + last_lit] = unchecked((byte)lc);
      last_lit++;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504745uL);
      if (dist == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504746uL);
        dyn_ltree[lc * 2]++;
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504747uL);
        matches++;
        dist--;
        dyn_ltree[(Tree.LengthCode[lc] + InternalConstants.LITERALS + 1) * 2]++;
        dyn_dtree[Tree.DistanceCode(dist) * 2]++;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504748uL);
      if ((last_lit & 0x1fff) == 0 && (int)compressionLevel > 2) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504749uL);
        int out_length = last_lit << 3;
        int in_length = strstart - block_start;
        int dcode;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504750uL);
        for (dcode = 0; dcode < InternalConstants.D_CODES; dcode++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504751uL);
          out_length = (int)(out_length + (int)dyn_dtree[dcode * 2] * (5L + Tree.ExtraDistanceBits[dcode]));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504752uL);
        out_length >>= 3;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504753uL);
        if ((matches < (last_lit / 2)) && out_length < in_length / 2) {
          System.Boolean RNTRNTRNT_516 = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504754uL);
          return RNTRNTRNT_516;
        }
      }
      System.Boolean RNTRNTRNT_517 = (last_lit == lit_bufsize - 1) || (last_lit == lit_bufsize);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504755uL);
      return RNTRNTRNT_517;
    }
    internal void send_compressed_block(short[] ltree, short[] dtree)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504756uL);
      int distance;
      int lc;
      int lx = 0;
      int code;
      int extra;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504757uL);
      if (last_lit != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504758uL);
        do {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504759uL);
          int ix = _distanceOffset + lx * 2;
          distance = ((pending[ix] << 8) & 0xff00) | (pending[ix + 1] & 0xff);
          lc = (pending[_lengthOffset + lx]) & 0xff;
          lx++;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504760uL);
          if (distance == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504761uL);
            send_code(lc, ltree);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504762uL);
            code = Tree.LengthCode[lc];
            send_code(code + InternalConstants.LITERALS + 1, ltree);
            extra = Tree.ExtraLengthBits[code];
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504763uL);
            if (extra != 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504764uL);
              lc -= Tree.LengthBase[code];
              send_bits(lc, extra);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504765uL);
            distance--;
            code = Tree.DistanceCode(distance);
            send_code(code, dtree);
            extra = Tree.ExtraDistanceBits[code];
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504766uL);
            if (extra != 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504767uL);
              distance -= Tree.DistanceBase[code];
              send_bits(distance, extra);
            }
          }
        } while (lx < last_lit);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504768uL);
      send_code(END_BLOCK, ltree);
      last_eob_len = ltree[END_BLOCK * 2 + 1];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504769uL);
    }
    internal void set_data_type()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504770uL);
      int n = 0;
      int ascii_freq = 0;
      int bin_freq = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504771uL);
      while (n < 7) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504772uL);
        bin_freq += dyn_ltree[n * 2];
        n++;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504773uL);
      while (n < 128) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504774uL);
        ascii_freq += dyn_ltree[n * 2];
        n++;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504775uL);
      while (n < InternalConstants.LITERALS) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504776uL);
        bin_freq += dyn_ltree[n * 2];
        n++;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504777uL);
      data_type = (sbyte)(bin_freq > (ascii_freq >> 2) ? Z_BINARY : Z_ASCII);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504778uL);
    }
    internal void bi_flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504779uL);
      if (bi_valid == 16) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504780uL);
        pending[pendingCount++] = (byte)bi_buf;
        pending[pendingCount++] = (byte)(bi_buf >> 8);
        bi_buf = 0;
        bi_valid = 0;
      } else if (bi_valid >= 8) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504781uL);
        pending[pendingCount++] = (byte)bi_buf;
        bi_buf >>= 8;
        bi_valid -= 8;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504782uL);
    }
    internal void bi_windup()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504783uL);
      if (bi_valid > 8) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504784uL);
        pending[pendingCount++] = (byte)bi_buf;
        pending[pendingCount++] = (byte)(bi_buf >> 8);
      } else if (bi_valid > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504785uL);
        pending[pendingCount++] = (byte)bi_buf;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504786uL);
      bi_buf = 0;
      bi_valid = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504787uL);
    }
    internal void copy_block(int buf, int len, bool header)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504788uL);
      bi_windup();
      last_eob_len = 8;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504789uL);
      if (header) {
        unchecked {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504790uL);
          pending[pendingCount++] = (byte)len;
          pending[pendingCount++] = (byte)(len >> 8);
          pending[pendingCount++] = (byte)~len;
          pending[pendingCount++] = (byte)(~len >> 8);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504791uL);
      put_bytes(window, buf, len);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504792uL);
    }
    internal void flush_block_only(bool eof)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504793uL);
      _tr_flush_block(block_start >= 0 ? block_start : -1, strstart - block_start, eof);
      block_start = strstart;
      _codec.flush_pending();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504794uL);
    }
    internal BlockState DeflateNone(FlushType flush)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504795uL);
      int max_block_size = 0xffff;
      int max_start;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504796uL);
      if (max_block_size > pending.Length - 5) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504797uL);
        max_block_size = pending.Length - 5;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504798uL);
      while (true) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504799uL);
        if (lookahead <= 1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504800uL);
          _fillWindow();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504801uL);
          if (lookahead == 0 && flush == FlushType.None) {
            BlockState RNTRNTRNT_518 = BlockState.NeedMore;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504802uL);
            return RNTRNTRNT_518;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504803uL);
          if (lookahead == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504804uL);
            break;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504805uL);
        strstart += lookahead;
        lookahead = 0;
        max_start = block_start + max_block_size;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504806uL);
        if (strstart == 0 || strstart >= max_start) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504807uL);
          lookahead = (int)(strstart - max_start);
          strstart = (int)max_start;
          flush_block_only(false);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504808uL);
          if (_codec.AvailableBytesOut == 0) {
            BlockState RNTRNTRNT_519 = BlockState.NeedMore;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504809uL);
            return RNTRNTRNT_519;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504810uL);
        if (strstart - block_start >= w_size - MIN_LOOKAHEAD) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504811uL);
          flush_block_only(false);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504812uL);
          if (_codec.AvailableBytesOut == 0) {
            BlockState RNTRNTRNT_520 = BlockState.NeedMore;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504813uL);
            return RNTRNTRNT_520;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504814uL);
      flush_block_only(flush == FlushType.Finish);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504815uL);
      if (_codec.AvailableBytesOut == 0) {
        BlockState RNTRNTRNT_521 = (flush == FlushType.Finish) ? BlockState.FinishStarted : BlockState.NeedMore;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504816uL);
        return RNTRNTRNT_521;
      }
      BlockState RNTRNTRNT_522 = flush == FlushType.Finish ? BlockState.FinishDone : BlockState.BlockDone;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504817uL);
      return RNTRNTRNT_522;
    }
    internal void _tr_stored_block(int buf, int stored_len, bool eof)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504818uL);
      send_bits((STORED_BLOCK << 1) + (eof ? 1 : 0), 3);
      copy_block(buf, stored_len, true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504819uL);
    }
    internal void _tr_flush_block(int buf, int stored_len, bool eof)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504820uL);
      int opt_lenb, static_lenb;
      int max_blindex = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504821uL);
      if (compressionLevel > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504822uL);
        if (data_type == Z_UNKNOWN) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504823uL);
          set_data_type();
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504824uL);
        treeLiterals.build_tree(this);
        treeDistances.build_tree(this);
        max_blindex = build_bl_tree();
        opt_lenb = (opt_len + 3 + 7) >> 3;
        static_lenb = (static_len + 3 + 7) >> 3;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504825uL);
        if (static_lenb <= opt_lenb) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504826uL);
          opt_lenb = static_lenb;
        }
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504827uL);
        opt_lenb = static_lenb = stored_len + 5;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504828uL);
      if (stored_len + 4 <= opt_lenb && buf != -1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504829uL);
        _tr_stored_block(buf, stored_len, eof);
      } else if (static_lenb == opt_lenb) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504831uL);
        send_bits((STATIC_TREES << 1) + (eof ? 1 : 0), 3);
        send_compressed_block(StaticTree.lengthAndLiteralsTreeCodes, StaticTree.distTreeCodes);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504830uL);
        send_bits((DYN_TREES << 1) + (eof ? 1 : 0), 3);
        send_all_trees(treeLiterals.max_code + 1, treeDistances.max_code + 1, max_blindex + 1);
        send_compressed_block(dyn_ltree, dyn_dtree);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504832uL);
      _InitializeBlocks();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504833uL);
      if (eof) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504834uL);
        bi_windup();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504835uL);
    }
    private void _fillWindow()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504836uL);
      int n, m;
      int p;
      int more;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504837uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504838uL);
        more = (window_size - lookahead - strstart);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504839uL);
        if (more == 0 && strstart == 0 && lookahead == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504840uL);
          more = w_size;
        } else if (more == -1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504848uL);
          more--;
        } else if (strstart >= w_size + w_size - MIN_LOOKAHEAD) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504841uL);
          Array.Copy(window, w_size, window, 0, w_size);
          match_start -= w_size;
          strstart -= w_size;
          block_start -= w_size;
          n = hash_size;
          p = n;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504842uL);
          do {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504843uL);
            m = (head[--p] & 0xffff);
            head[p] = (short)((m >= w_size) ? (m - w_size) : 0);
          } while (--n != 0);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504844uL);
          n = w_size;
          p = n;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504845uL);
          do {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504846uL);
            m = (prev[--p] & 0xffff);
            prev[p] = (short)((m >= w_size) ? (m - w_size) : 0);
          } while (--n != 0);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504847uL);
          more += w_size;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504849uL);
        if (_codec.AvailableBytesIn == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504850uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504851uL);
        n = _codec.read_buf(window, strstart + lookahead, more);
        lookahead += n;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504852uL);
        if (lookahead >= MIN_MATCH) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504853uL);
          ins_h = window[strstart] & 0xff;
          ins_h = (((ins_h) << hash_shift) ^ (window[strstart + 1] & 0xff)) & hash_mask;
        }
      } while (lookahead < MIN_LOOKAHEAD && _codec.AvailableBytesIn != 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504854uL);
    }
    internal BlockState DeflateFast(FlushType flush)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504855uL);
      int hash_head = 0;
      bool bflush;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504856uL);
      while (true) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504857uL);
        if (lookahead < MIN_LOOKAHEAD) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504858uL);
          _fillWindow();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504859uL);
          if (lookahead < MIN_LOOKAHEAD && flush == FlushType.None) {
            BlockState RNTRNTRNT_523 = BlockState.NeedMore;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504860uL);
            return RNTRNTRNT_523;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504861uL);
          if (lookahead == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504862uL);
            break;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504863uL);
        if (lookahead >= MIN_MATCH) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504864uL);
          ins_h = (((ins_h) << hash_shift) ^ (window[(strstart) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
          hash_head = (head[ins_h] & 0xffff);
          prev[strstart & w_mask] = head[ins_h];
          head[ins_h] = unchecked((short)strstart);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504865uL);
        if (hash_head != 0L && ((strstart - hash_head) & 0xffff) <= w_size - MIN_LOOKAHEAD) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504866uL);
          if (compressionStrategy != CompressionStrategy.HuffmanOnly) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504867uL);
            match_length = longest_match(hash_head);
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504868uL);
        if (match_length >= MIN_MATCH) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504869uL);
          bflush = _tr_tally(strstart - match_start, match_length - MIN_MATCH);
          lookahead -= match_length;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504870uL);
          if (match_length <= config.MaxLazy && lookahead >= MIN_MATCH) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504871uL);
            match_length--;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504872uL);
            do {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504873uL);
              strstart++;
              ins_h = ((ins_h << hash_shift) ^ (window[(strstart) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
              hash_head = (head[ins_h] & 0xffff);
              prev[strstart & w_mask] = head[ins_h];
              head[ins_h] = unchecked((short)strstart);
            } while (--match_length != 0);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504874uL);
            strstart++;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504875uL);
            strstart += match_length;
            match_length = 0;
            ins_h = window[strstart] & 0xff;
            ins_h = (((ins_h) << hash_shift) ^ (window[strstart + 1] & 0xff)) & hash_mask;
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504876uL);
          bflush = _tr_tally(0, window[strstart] & 0xff);
          lookahead--;
          strstart++;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504877uL);
        if (bflush) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504878uL);
          flush_block_only(false);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504879uL);
          if (_codec.AvailableBytesOut == 0) {
            BlockState RNTRNTRNT_524 = BlockState.NeedMore;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504880uL);
            return RNTRNTRNT_524;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504881uL);
      flush_block_only(flush == FlushType.Finish);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504882uL);
      if (_codec.AvailableBytesOut == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504883uL);
        if (flush == FlushType.Finish) {
          BlockState RNTRNTRNT_525 = BlockState.FinishStarted;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504884uL);
          return RNTRNTRNT_525;
        } else {
          BlockState RNTRNTRNT_526 = BlockState.NeedMore;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504885uL);
          return RNTRNTRNT_526;
        }
      }
      BlockState RNTRNTRNT_527 = flush == FlushType.Finish ? BlockState.FinishDone : BlockState.BlockDone;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504886uL);
      return RNTRNTRNT_527;
    }
    internal BlockState DeflateSlow(FlushType flush)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504887uL);
      int hash_head = 0;
      bool bflush;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504888uL);
      while (true) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504889uL);
        if (lookahead < MIN_LOOKAHEAD) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504890uL);
          _fillWindow();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504891uL);
          if (lookahead < MIN_LOOKAHEAD && flush == FlushType.None) {
            BlockState RNTRNTRNT_528 = BlockState.NeedMore;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504892uL);
            return RNTRNTRNT_528;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504893uL);
          if (lookahead == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504894uL);
            break;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504895uL);
        if (lookahead >= MIN_MATCH) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504896uL);
          ins_h = (((ins_h) << hash_shift) ^ (window[(strstart) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
          hash_head = (head[ins_h] & 0xffff);
          prev[strstart & w_mask] = head[ins_h];
          head[ins_h] = unchecked((short)strstart);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504897uL);
        prev_length = match_length;
        prev_match = match_start;
        match_length = MIN_MATCH - 1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504898uL);
        if (hash_head != 0 && prev_length < config.MaxLazy && ((strstart - hash_head) & 0xffff) <= w_size - MIN_LOOKAHEAD) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504899uL);
          if (compressionStrategy != CompressionStrategy.HuffmanOnly) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504900uL);
            match_length = longest_match(hash_head);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504901uL);
          if (match_length <= 5 && (compressionStrategy == CompressionStrategy.Filtered || (match_length == MIN_MATCH && strstart - match_start > 4096))) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504902uL);
            match_length = MIN_MATCH - 1;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504903uL);
        if (prev_length >= MIN_MATCH && match_length <= prev_length) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504904uL);
          int max_insert = strstart + lookahead - MIN_MATCH;
          bflush = _tr_tally(strstart - 1 - prev_match, prev_length - MIN_MATCH);
          lookahead -= (prev_length - 1);
          prev_length -= 2;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504905uL);
          do {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504906uL);
            if (++strstart <= max_insert) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504907uL);
              ins_h = (((ins_h) << hash_shift) ^ (window[(strstart) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
              hash_head = (head[ins_h] & 0xffff);
              prev[strstart & w_mask] = head[ins_h];
              head[ins_h] = unchecked((short)strstart);
            }
          } while (--prev_length != 0);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504908uL);
          match_available = 0;
          match_length = MIN_MATCH - 1;
          strstart++;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504909uL);
          if (bflush) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504910uL);
            flush_block_only(false);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504911uL);
            if (_codec.AvailableBytesOut == 0) {
              BlockState RNTRNTRNT_529 = BlockState.NeedMore;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504912uL);
              return RNTRNTRNT_529;
            }
          }
        } else if (match_available != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504914uL);
          bflush = _tr_tally(0, window[strstart - 1] & 0xff);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504915uL);
          if (bflush) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504916uL);
            flush_block_only(false);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504917uL);
          strstart++;
          lookahead--;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504918uL);
          if (_codec.AvailableBytesOut == 0) {
            BlockState RNTRNTRNT_530 = BlockState.NeedMore;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504919uL);
            return RNTRNTRNT_530;
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504913uL);
          match_available = 1;
          strstart++;
          lookahead--;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504920uL);
      if (match_available != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504921uL);
        bflush = _tr_tally(0, window[strstart - 1] & 0xff);
        match_available = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504922uL);
      flush_block_only(flush == FlushType.Finish);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504923uL);
      if (_codec.AvailableBytesOut == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504924uL);
        if (flush == FlushType.Finish) {
          BlockState RNTRNTRNT_531 = BlockState.FinishStarted;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504925uL);
          return RNTRNTRNT_531;
        } else {
          BlockState RNTRNTRNT_532 = BlockState.NeedMore;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504926uL);
          return RNTRNTRNT_532;
        }
      }
      BlockState RNTRNTRNT_533 = flush == FlushType.Finish ? BlockState.FinishDone : BlockState.BlockDone;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504927uL);
      return RNTRNTRNT_533;
    }
    internal int longest_match(int cur_match)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504928uL);
      int chain_length = config.MaxChainLength;
      int scan = strstart;
      int match;
      int len;
      int best_len = prev_length;
      int limit = strstart > (w_size - MIN_LOOKAHEAD) ? strstart - (w_size - MIN_LOOKAHEAD) : 0;
      int niceLength = config.NiceLength;
      int wmask = w_mask;
      int strend = strstart + MAX_MATCH;
      byte scan_end1 = window[scan + best_len - 1];
      byte scan_end = window[scan + best_len];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504929uL);
      if (prev_length >= config.GoodLength) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504930uL);
        chain_length >>= 2;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504931uL);
      if (niceLength > lookahead) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504932uL);
        niceLength = lookahead;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504933uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504934uL);
        match = cur_match;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504935uL);
        if (window[match + best_len] != scan_end || window[match + best_len - 1] != scan_end1 || window[match] != window[scan] || window[++match] != window[scan + 1]) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504936uL);
          continue;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504937uL);
        scan += 2;
        match++;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504938uL);
        do {
        } while (window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && scan < strend);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504939uL);
        len = MAX_MATCH - (int)(strend - scan);
        scan = strend - MAX_MATCH;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504940uL);
        if (len > best_len) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504941uL);
          match_start = cur_match;
          best_len = len;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504942uL);
          if (len >= niceLength) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504943uL);
            break;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504944uL);
          scan_end1 = window[scan + best_len - 1];
          scan_end = window[scan + best_len];
        }
      } while ((cur_match = (prev[cur_match & wmask] & 0xffff)) > limit && --chain_length != 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504945uL);
      if (best_len <= lookahead) {
        System.Int32 RNTRNTRNT_534 = best_len;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504946uL);
        return RNTRNTRNT_534;
      }
      System.Int32 RNTRNTRNT_535 = lookahead;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504947uL);
      return RNTRNTRNT_535;
    }
    private bool Rfc1950BytesEmitted = false;
    private bool _WantRfc1950HeaderBytes = true;
    internal bool WantRfc1950HeaderBytes {
      get {
        System.Boolean RNTRNTRNT_536 = _WantRfc1950HeaderBytes;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504948uL);
        return RNTRNTRNT_536;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504949uL);
        _WantRfc1950HeaderBytes = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504950uL);
      }
    }
    internal int Initialize(ZlibCodec codec, CompressionLevel level)
    {
      System.Int32 RNTRNTRNT_537 = Initialize(codec, level, ZlibConstants.WindowBitsMax);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504951uL);
      return RNTRNTRNT_537;
    }
    internal int Initialize(ZlibCodec codec, CompressionLevel level, int bits)
    {
      System.Int32 RNTRNTRNT_538 = Initialize(codec, level, bits, MEM_LEVEL_DEFAULT, CompressionStrategy.Default);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504952uL);
      return RNTRNTRNT_538;
    }
    internal int Initialize(ZlibCodec codec, CompressionLevel level, int bits, CompressionStrategy compressionStrategy)
    {
      System.Int32 RNTRNTRNT_539 = Initialize(codec, level, bits, MEM_LEVEL_DEFAULT, compressionStrategy);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504953uL);
      return RNTRNTRNT_539;
    }
    internal int Initialize(ZlibCodec codec, CompressionLevel level, int windowBits, int memLevel, CompressionStrategy strategy)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504954uL);
      _codec = codec;
      _codec.Message = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504955uL);
      if (windowBits < 9 || windowBits > 15) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504956uL);
        throw new ZlibException("windowBits must be in the range 9..15.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504957uL);
      if (memLevel < 1 || memLevel > MEM_LEVEL_MAX) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504958uL);
        throw new ZlibException(String.Format("memLevel must be in the range 1.. {0}", MEM_LEVEL_MAX));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504959uL);
      _codec.dstate = this;
      w_bits = windowBits;
      w_size = 1 << w_bits;
      w_mask = w_size - 1;
      hash_bits = memLevel + 7;
      hash_size = 1 << hash_bits;
      hash_mask = hash_size - 1;
      hash_shift = ((hash_bits + MIN_MATCH - 1) / MIN_MATCH);
      window = new byte[w_size * 2];
      prev = new short[w_size];
      head = new short[hash_size];
      lit_bufsize = 1 << (memLevel + 6);
      pending = new byte[lit_bufsize * 4];
      _distanceOffset = lit_bufsize;
      _lengthOffset = (1 + 2) * lit_bufsize;
      this.compressionLevel = level;
      this.compressionStrategy = strategy;
      Reset();
      System.Int32 RNTRNTRNT_540 = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504960uL);
      return RNTRNTRNT_540;
    }
    internal void Reset()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504961uL);
      _codec.TotalBytesIn = _codec.TotalBytesOut = 0;
      _codec.Message = null;
      pendingCount = 0;
      nextPending = 0;
      Rfc1950BytesEmitted = false;
      status = (WantRfc1950HeaderBytes) ? INIT_STATE : BUSY_STATE;
      _codec._Adler32 = Adler.Adler32(0, null, 0, 0);
      last_flush = (int)FlushType.None;
      _InitializeTreeData();
      _InitializeLazyMatch();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504962uL);
    }
    internal int End()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504963uL);
      if (status != INIT_STATE && status != BUSY_STATE && status != FINISH_STATE) {
        System.Int32 RNTRNTRNT_541 = ZlibConstants.Z_STREAM_ERROR;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504964uL);
        return RNTRNTRNT_541;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504965uL);
      pending = null;
      head = null;
      prev = null;
      window = null;
      System.Int32 RNTRNTRNT_542 = status == BUSY_STATE ? ZlibConstants.Z_DATA_ERROR : ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504966uL);
      return RNTRNTRNT_542;
    }
    private void SetDeflater()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504967uL);
      switch (config.Flavor) {
        case DeflateFlavor.Store:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504970uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504968uL);
            DeflateFunction = DeflateNone;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504969uL);
            break;
          }

        case DeflateFlavor.Fast:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504973uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504971uL);
            DeflateFunction = DeflateFast;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504972uL);
            break;
          }

        case DeflateFlavor.Slow:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504976uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504974uL);
            DeflateFunction = DeflateSlow;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504975uL);
            break;
          }

      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504977uL);
    }
    internal int SetParams(CompressionLevel level, CompressionStrategy strategy)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504978uL);
      int result = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504979uL);
      if (compressionLevel != level) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504980uL);
        Config newConfig = Config.Lookup(level);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504981uL);
        if (newConfig.Flavor != config.Flavor && _codec.TotalBytesIn != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504982uL);
          result = _codec.Deflate(FlushType.Partial);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504983uL);
        compressionLevel = level;
        config = newConfig;
        SetDeflater();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504984uL);
      compressionStrategy = strategy;
      System.Int32 RNTRNTRNT_543 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504985uL);
      return RNTRNTRNT_543;
    }
    internal int SetDictionary(byte[] dictionary)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504986uL);
      int length = dictionary.Length;
      int index = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504987uL);
      if (dictionary == null || status != INIT_STATE) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504988uL);
        throw new ZlibException("Stream error.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504989uL);
      _codec._Adler32 = Adler.Adler32(_codec._Adler32, dictionary, 0, dictionary.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504990uL);
      if (length < MIN_MATCH) {
        System.Int32 RNTRNTRNT_544 = ZlibConstants.Z_OK;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504991uL);
        return RNTRNTRNT_544;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504992uL);
      if (length > w_size - MIN_LOOKAHEAD) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504993uL);
        length = w_size - MIN_LOOKAHEAD;
        index = dictionary.Length - length;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504994uL);
      Array.Copy(dictionary, index, window, 0, length);
      strstart = length;
      block_start = length;
      ins_h = window[0] & 0xff;
      ins_h = (((ins_h) << hash_shift) ^ (window[1] & 0xff)) & hash_mask;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504995uL);
      for (int n = 0; n <= length - MIN_MATCH; n++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504996uL);
        ins_h = (((ins_h) << hash_shift) ^ (window[(n) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
        prev[n & w_mask] = head[ins_h];
        head[ins_h] = (short)n;
      }
      System.Int32 RNTRNTRNT_545 = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504997uL);
      return RNTRNTRNT_545;
    }
    internal int Deflate(FlushType flush)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504998uL);
      int old_flush;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(504999uL);
      if (_codec.OutputBuffer == null || (_codec.InputBuffer == null && _codec.AvailableBytesIn != 0) || (status == FINISH_STATE && flush != FlushType.Finish)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505000uL);
        _codec.Message = _ErrorMessage[ZlibConstants.Z_NEED_DICT - (ZlibConstants.Z_STREAM_ERROR)];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505001uL);
        throw new ZlibException(String.Format("Something is fishy. [{0}]", _codec.Message));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505002uL);
      if (_codec.AvailableBytesOut == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505003uL);
        _codec.Message = _ErrorMessage[ZlibConstants.Z_NEED_DICT - (ZlibConstants.Z_BUF_ERROR)];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505004uL);
        throw new ZlibException("OutputBuffer is full (AvailableBytesOut == 0)");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505005uL);
      old_flush = last_flush;
      last_flush = (int)flush;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505006uL);
      if (status == INIT_STATE) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505007uL);
        int header = (Z_DEFLATED + ((w_bits - 8) << 4)) << 8;
        int level_flags = (((int)compressionLevel - 1) & 0xff) >> 1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505008uL);
        if (level_flags > 3) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505009uL);
          level_flags = 3;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505010uL);
        header |= (level_flags << 6);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505011uL);
        if (strstart != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505012uL);
          header |= PRESET_DICT;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505013uL);
        header += 31 - (header % 31);
        status = BUSY_STATE;
        unchecked {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505014uL);
          pending[pendingCount++] = (byte)(header >> 8);
          pending[pendingCount++] = (byte)header;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505015uL);
        if (strstart != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505016uL);
          pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff000000u) >> 24);
          pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff0000) >> 16);
          pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff00) >> 8);
          pending[pendingCount++] = (byte)(_codec._Adler32 & 0xff);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505017uL);
        _codec._Adler32 = Adler.Adler32(0, null, 0, 0);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505018uL);
      if (pendingCount != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505019uL);
        _codec.flush_pending();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505020uL);
        if (_codec.AvailableBytesOut == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505021uL);
          last_flush = -1;
          System.Int32 RNTRNTRNT_546 = ZlibConstants.Z_OK;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505022uL);
          return RNTRNTRNT_546;
        }
      } else if (_codec.AvailableBytesIn == 0 && (int)flush <= old_flush && flush != FlushType.Finish) {
        System.Int32 RNTRNTRNT_547 = ZlibConstants.Z_OK;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505023uL);
        return RNTRNTRNT_547;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505024uL);
      if (status == FINISH_STATE && _codec.AvailableBytesIn != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505025uL);
        _codec.Message = _ErrorMessage[ZlibConstants.Z_NEED_DICT - (ZlibConstants.Z_BUF_ERROR)];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505026uL);
        throw new ZlibException("status == FINISH_STATE && _codec.AvailableBytesIn != 0");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505027uL);
      if (_codec.AvailableBytesIn != 0 || lookahead != 0 || (flush != FlushType.None && status != FINISH_STATE)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505028uL);
        BlockState bstate = DeflateFunction(flush);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505029uL);
        if (bstate == BlockState.FinishStarted || bstate == BlockState.FinishDone) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505030uL);
          status = FINISH_STATE;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505031uL);
        if (bstate == BlockState.NeedMore || bstate == BlockState.FinishStarted) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505032uL);
          if (_codec.AvailableBytesOut == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505033uL);
            last_flush = -1;
          }
          System.Int32 RNTRNTRNT_548 = ZlibConstants.Z_OK;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505034uL);
          return RNTRNTRNT_548;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505035uL);
        if (bstate == BlockState.BlockDone) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505036uL);
          if (flush == FlushType.Partial) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505037uL);
            _tr_align();
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505038uL);
            _tr_stored_block(0, 0, false);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505039uL);
            if (flush == FlushType.Full) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505040uL);
              for (int i = 0; i < hash_size; i++) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505041uL);
                head[i] = 0;
              }
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505042uL);
          _codec.flush_pending();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505043uL);
          if (_codec.AvailableBytesOut == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505044uL);
            last_flush = -1;
            System.Int32 RNTRNTRNT_549 = ZlibConstants.Z_OK;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505045uL);
            return RNTRNTRNT_549;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505046uL);
      if (flush != FlushType.Finish) {
        System.Int32 RNTRNTRNT_550 = ZlibConstants.Z_OK;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505047uL);
        return RNTRNTRNT_550;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505048uL);
      if (!WantRfc1950HeaderBytes || Rfc1950BytesEmitted) {
        System.Int32 RNTRNTRNT_551 = ZlibConstants.Z_STREAM_END;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505049uL);
        return RNTRNTRNT_551;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505050uL);
      pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff000000u) >> 24);
      pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff0000) >> 16);
      pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff00) >> 8);
      pending[pendingCount++] = (byte)(_codec._Adler32 & 0xff);
      _codec.flush_pending();
      Rfc1950BytesEmitted = true;
      System.Int32 RNTRNTRNT_552 = pendingCount != 0 ? ZlibConstants.Z_OK : ZlibConstants.Z_STREAM_END;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505051uL);
      return RNTRNTRNT_552;
    }
  }
}
