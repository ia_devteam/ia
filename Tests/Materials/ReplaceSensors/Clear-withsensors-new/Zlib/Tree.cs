using System;
namespace Ionic.Zlib
{
  sealed class Tree
  {
    private static readonly int HEAP_SIZE = (2 * InternalConstants.L_CODES + 1);
    static internal readonly int[] ExtraLengthBits = new int[] {
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      1,
      1,
      1,
      1,
      2,
      2,
      2,
      2,
      3,
      3,
      3,
      3,
      4,
      4,
      4,
      4,
      5,
      5,
      5,
      5,
      0
    };
    static internal readonly int[] ExtraDistanceBits = new int[] {
      0,
      0,
      0,
      0,
      1,
      1,
      2,
      2,
      3,
      3,
      4,
      4,
      5,
      5,
      6,
      6,
      7,
      7,
      8,
      8,
      9,
      9,
      10,
      10,
      11,
      11,
      12,
      12,
      13,
      13
    };
    static internal readonly int[] extra_blbits = new int[] {
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      2,
      3,
      7
    };
    static internal readonly sbyte[] bl_order = new sbyte[] {
      16,
      17,
      18,
      0,
      8,
      7,
      9,
      6,
      10,
      5,
      11,
      4,
      12,
      3,
      13,
      2,
      14,
      1,
      15
    };
    internal const int Buf_size = 8 * 2;
    private static readonly sbyte[] _dist_code = new sbyte[] {
      0,
      1,
      2,
      3,
      4,
      4,
      5,
      5,
      6,
      6,
      6,
      6,
      7,
      7,
      7,
      7,
      8,
      8,
      8,
      8,
      8,
      8,
      8,
      8,
      9,
      9,
      9,
      9,
      9,
      9,
      9,
      9,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      0,
      0,
      16,
      17,
      18,
      18,
      19,
      19,
      20,
      20,
      20,
      20,
      21,
      21,
      21,
      21,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29
    };
    static internal readonly sbyte[] LengthCode = new sbyte[] {
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      8,
      9,
      9,
      10,
      10,
      11,
      11,
      12,
      12,
      12,
      12,
      13,
      13,
      13,
      13,
      14,
      14,
      14,
      14,
      15,
      15,
      15,
      15,
      16,
      16,
      16,
      16,
      16,
      16,
      16,
      16,
      17,
      17,
      17,
      17,
      17,
      17,
      17,
      17,
      18,
      18,
      18,
      18,
      18,
      18,
      18,
      18,
      19,
      19,
      19,
      19,
      19,
      19,
      19,
      19,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      28
    };
    static internal readonly int[] LengthBase = new int[] {
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      10,
      12,
      14,
      16,
      20,
      24,
      28,
      32,
      40,
      48,
      56,
      64,
      80,
      96,
      112,
      128,
      160,
      192,
      224,
      0
    };
    static internal readonly int[] DistanceBase = new int[] {
      0,
      1,
      2,
      3,
      4,
      6,
      8,
      12,
      16,
      24,
      32,
      48,
      64,
      96,
      128,
      192,
      256,
      384,
      512,
      768,
      1024,
      1536,
      2048,
      3072,
      4096,
      6144,
      8192,
      12288,
      16384,
      24576
    };
    static internal int DistanceCode(int dist)
    {
      System.Int32 RNTRNTRNT_672 = (dist < 256) ? _dist_code[dist] : _dist_code[256 + SharedUtils.URShift(dist, 7)];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506012uL);
      return RNTRNTRNT_672;
    }
    internal short[] dyn_tree;
    internal int max_code;
    internal StaticTree staticTree;
    internal void gen_bitlen(DeflateManager s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506013uL);
      short[] tree = dyn_tree;
      short[] stree = staticTree.treeCodes;
      int[] extra = staticTree.extraBits;
      int base_Renamed = staticTree.extraBase;
      int max_length = staticTree.maxLength;
      int h;
      int n, m;
      int bits;
      int xbits;
      short f;
      int overflow = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506014uL);
      for (bits = 0; bits <= InternalConstants.MAX_BITS; bits++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506015uL);
        s.bl_count[bits] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506016uL);
      tree[s.heap[s.heap_max] * 2 + 1] = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506017uL);
      for (h = s.heap_max + 1; h < HEAP_SIZE; h++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506018uL);
        n = s.heap[h];
        bits = tree[tree[n * 2 + 1] * 2 + 1] + 1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506019uL);
        if (bits > max_length) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506020uL);
          bits = max_length;
          overflow++;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506021uL);
        tree[n * 2 + 1] = (short)bits;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506022uL);
        if (n > max_code) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506023uL);
          continue;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506024uL);
        s.bl_count[bits]++;
        xbits = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506025uL);
        if (n >= base_Renamed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506026uL);
          xbits = extra[n - base_Renamed];
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506027uL);
        f = tree[n * 2];
        s.opt_len += f * (bits + xbits);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506028uL);
        if (stree != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506029uL);
          s.static_len += f * (stree[n * 2 + 1] + xbits);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506030uL);
      if (overflow == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506031uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506032uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506033uL);
        bits = max_length - 1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506034uL);
        while (s.bl_count[bits] == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506035uL);
          bits--;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506036uL);
        s.bl_count[bits]--;
        s.bl_count[bits + 1] = (short)(s.bl_count[bits + 1] + 2);
        s.bl_count[max_length]--;
        overflow -= 2;
      } while (overflow > 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506037uL);
      for (bits = max_length; bits != 0; bits--) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506038uL);
        n = s.bl_count[bits];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506039uL);
        while (n != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506040uL);
          m = s.heap[--h];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506041uL);
          if (m > max_code) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506042uL);
            continue;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506043uL);
          if (tree[m * 2 + 1] != bits) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506044uL);
            s.opt_len = (int)(s.opt_len + ((long)bits - (long)tree[m * 2 + 1]) * (long)tree[m * 2]);
            tree[m * 2 + 1] = (short)bits;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506045uL);
          n--;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506046uL);
    }
    internal void build_tree(DeflateManager s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506047uL);
      short[] tree = dyn_tree;
      short[] stree = staticTree.treeCodes;
      int elems = staticTree.elems;
      int n, m;
      int max_code = -1;
      int node;
      s.heap_len = 0;
      s.heap_max = HEAP_SIZE;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506048uL);
      for (n = 0; n < elems; n++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506049uL);
        if (tree[n * 2] != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506050uL);
          s.heap[++s.heap_len] = max_code = n;
          s.depth[n] = 0;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506051uL);
          tree[n * 2 + 1] = 0;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506052uL);
      while (s.heap_len < 2) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506053uL);
        node = s.heap[++s.heap_len] = (max_code < 2 ? ++max_code : 0);
        tree[node * 2] = 1;
        s.depth[node] = 0;
        s.opt_len--;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506054uL);
        if (stree != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506055uL);
          s.static_len -= stree[node * 2 + 1];
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506056uL);
      this.max_code = max_code;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506057uL);
      for (n = s.heap_len / 2; n >= 1; n--) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506058uL);
        s.pqdownheap(tree, n);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506059uL);
      node = elems;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506060uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506061uL);
        n = s.heap[1];
        s.heap[1] = s.heap[s.heap_len--];
        s.pqdownheap(tree, 1);
        m = s.heap[1];
        s.heap[--s.heap_max] = n;
        s.heap[--s.heap_max] = m;
        tree[node * 2] = unchecked((short)(tree[n * 2] + tree[m * 2]));
        s.depth[node] = (sbyte)(System.Math.Max((byte)s.depth[n], (byte)s.depth[m]) + 1);
        tree[n * 2 + 1] = tree[m * 2 + 1] = (short)node;
        s.heap[1] = node++;
        s.pqdownheap(tree, 1);
      } while (s.heap_len >= 2);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506062uL);
      s.heap[--s.heap_max] = s.heap[1];
      gen_bitlen(s);
      gen_codes(tree, max_code, s.bl_count);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506063uL);
    }
    static internal void gen_codes(short[] tree, int max_code, short[] bl_count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506064uL);
      short[] next_code = new short[InternalConstants.MAX_BITS + 1];
      short code = 0;
      int bits;
      int n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506065uL);
      for (bits = 1; bits <= InternalConstants.MAX_BITS; bits++) {
        unchecked {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506066uL);
          next_code[bits] = code = (short)((code + bl_count[bits - 1]) << 1);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506067uL);
      for (n = 0; n <= max_code; n++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506068uL);
        int len = tree[n * 2 + 1];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506069uL);
        if (len == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506070uL);
          continue;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506071uL);
        tree[n * 2] = unchecked((short)(bi_reverse(next_code[len]++, len)));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506072uL);
    }
    static internal int bi_reverse(int code, int len)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506073uL);
      int res = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506074uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506075uL);
        res |= code & 1;
        code >>= 1;
        res <<= 1;
      } while (--len > 0);
      System.Int32 RNTRNTRNT_673 = res >> 1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506076uL);
      return RNTRNTRNT_673;
    }
  }
}
