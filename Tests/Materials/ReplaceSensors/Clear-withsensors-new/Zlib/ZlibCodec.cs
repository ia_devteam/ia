using System;
using Interop = System.Runtime.InteropServices;
namespace Ionic.Zlib
{
  [Interop.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d0000D")]
  [Interop.ComVisible(true)]
  [Interop.ClassInterface(Interop.ClassInterfaceType.AutoDispatch)]
  public sealed class ZlibCodec
  {
    public byte[] InputBuffer;
    public int NextIn;
    public int AvailableBytesIn;
    public long TotalBytesIn;
    public byte[] OutputBuffer;
    public int NextOut;
    public int AvailableBytesOut;
    public long TotalBytesOut;
    public System.String Message;
    internal DeflateManager dstate;
    internal InflateManager istate;
    internal uint _Adler32;
    public CompressionLevel CompressLevel = CompressionLevel.Default;
    public int WindowBits = ZlibConstants.WindowBitsDefault;
    public CompressionStrategy Strategy = CompressionStrategy.Default;
    public int Adler32 {
      get {
        System.Int32 RNTRNTRNT_701 = (int)_Adler32;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506301uL);
        return RNTRNTRNT_701;
      }
    }
    public ZlibCodec()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506302uL);
    }
    public ZlibCodec(CompressionMode mode)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506303uL);
      if (mode == CompressionMode.Compress) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506304uL);
        int rc = InitializeDeflate();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506305uL);
        if (rc != ZlibConstants.Z_OK) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506306uL);
          throw new ZlibException("Cannot initialize for deflate.");
        }
      } else if (mode == CompressionMode.Decompress) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506308uL);
        int rc = InitializeInflate();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506309uL);
        if (rc != ZlibConstants.Z_OK) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506310uL);
          throw new ZlibException("Cannot initialize for inflate.");
        }
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506307uL);
        throw new ZlibException("Invalid ZlibStreamFlavor.");
      }
    }
    public int InitializeInflate()
    {
      System.Int32 RNTRNTRNT_702 = InitializeInflate(this.WindowBits);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506311uL);
      return RNTRNTRNT_702;
    }
    public int InitializeInflate(bool expectRfc1950Header)
    {
      System.Int32 RNTRNTRNT_703 = InitializeInflate(this.WindowBits, expectRfc1950Header);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506312uL);
      return RNTRNTRNT_703;
    }
    public int InitializeInflate(int windowBits)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506313uL);
      this.WindowBits = windowBits;
      System.Int32 RNTRNTRNT_704 = InitializeInflate(windowBits, true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506314uL);
      return RNTRNTRNT_704;
    }
    public int InitializeInflate(int windowBits, bool expectRfc1950Header)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506315uL);
      this.WindowBits = windowBits;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506316uL);
      if (dstate != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506317uL);
        throw new ZlibException("You may not call InitializeInflate() after calling InitializeDeflate().");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506318uL);
      istate = new InflateManager(expectRfc1950Header);
      System.Int32 RNTRNTRNT_705 = istate.Initialize(this, windowBits);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506319uL);
      return RNTRNTRNT_705;
    }
    public int Inflate(FlushType flush)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506320uL);
      if (istate == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506321uL);
        throw new ZlibException("No Inflate State!");
      }
      System.Int32 RNTRNTRNT_706 = istate.Inflate(flush);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506322uL);
      return RNTRNTRNT_706;
    }
    public int EndInflate()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506323uL);
      if (istate == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506324uL);
        throw new ZlibException("No Inflate State!");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506325uL);
      int ret = istate.End();
      istate = null;
      System.Int32 RNTRNTRNT_707 = ret;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506326uL);
      return RNTRNTRNT_707;
    }
    public int SyncInflate()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506327uL);
      if (istate == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506328uL);
        throw new ZlibException("No Inflate State!");
      }
      System.Int32 RNTRNTRNT_708 = istate.Sync();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506329uL);
      return RNTRNTRNT_708;
    }
    public int InitializeDeflate()
    {
      System.Int32 RNTRNTRNT_709 = _InternalInitializeDeflate(true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506330uL);
      return RNTRNTRNT_709;
    }
    public int InitializeDeflate(CompressionLevel level)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506331uL);
      this.CompressLevel = level;
      System.Int32 RNTRNTRNT_710 = _InternalInitializeDeflate(true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506332uL);
      return RNTRNTRNT_710;
    }
    public int InitializeDeflate(CompressionLevel level, bool wantRfc1950Header)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506333uL);
      this.CompressLevel = level;
      System.Int32 RNTRNTRNT_711 = _InternalInitializeDeflate(wantRfc1950Header);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506334uL);
      return RNTRNTRNT_711;
    }
    public int InitializeDeflate(CompressionLevel level, int bits)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506335uL);
      this.CompressLevel = level;
      this.WindowBits = bits;
      System.Int32 RNTRNTRNT_712 = _InternalInitializeDeflate(true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506336uL);
      return RNTRNTRNT_712;
    }
    public int InitializeDeflate(CompressionLevel level, int bits, bool wantRfc1950Header)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506337uL);
      this.CompressLevel = level;
      this.WindowBits = bits;
      System.Int32 RNTRNTRNT_713 = _InternalInitializeDeflate(wantRfc1950Header);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506338uL);
      return RNTRNTRNT_713;
    }
    private int _InternalInitializeDeflate(bool wantRfc1950Header)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506339uL);
      if (istate != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506340uL);
        throw new ZlibException("You may not call InitializeDeflate() after calling InitializeInflate().");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506341uL);
      dstate = new DeflateManager();
      dstate.WantRfc1950HeaderBytes = wantRfc1950Header;
      System.Int32 RNTRNTRNT_714 = dstate.Initialize(this, this.CompressLevel, this.WindowBits, this.Strategy);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506342uL);
      return RNTRNTRNT_714;
    }
    public int Deflate(FlushType flush)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506343uL);
      if (dstate == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506344uL);
        throw new ZlibException("No Deflate State!");
      }
      System.Int32 RNTRNTRNT_715 = dstate.Deflate(flush);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506345uL);
      return RNTRNTRNT_715;
    }
    public int EndDeflate()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506346uL);
      if (dstate == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506347uL);
        throw new ZlibException("No Deflate State!");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506348uL);
      dstate = null;
      System.Int32 RNTRNTRNT_716 = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506349uL);
      return RNTRNTRNT_716;
    }
    public void ResetDeflate()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506350uL);
      if (dstate == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506351uL);
        throw new ZlibException("No Deflate State!");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506352uL);
      dstate.Reset();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506353uL);
    }
    public int SetDeflateParams(CompressionLevel level, CompressionStrategy strategy)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506354uL);
      if (dstate == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506355uL);
        throw new ZlibException("No Deflate State!");
      }
      System.Int32 RNTRNTRNT_717 = dstate.SetParams(level, strategy);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506356uL);
      return RNTRNTRNT_717;
    }
    public int SetDictionary(byte[] dictionary)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506357uL);
      if (istate != null) {
        System.Int32 RNTRNTRNT_718 = istate.SetDictionary(dictionary);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506358uL);
        return RNTRNTRNT_718;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506359uL);
      if (dstate != null) {
        System.Int32 RNTRNTRNT_719 = dstate.SetDictionary(dictionary);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506360uL);
        return RNTRNTRNT_719;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506361uL);
      throw new ZlibException("No Inflate or Deflate state!");
    }
    internal void flush_pending()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506362uL);
      int len = dstate.pendingCount;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506363uL);
      if (len > AvailableBytesOut) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506364uL);
        len = AvailableBytesOut;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506365uL);
      if (len == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506366uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506367uL);
      if (dstate.pending.Length <= dstate.nextPending || OutputBuffer.Length <= NextOut || dstate.pending.Length < (dstate.nextPending + len) || OutputBuffer.Length < (NextOut + len)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506368uL);
        throw new ZlibException(String.Format("Invalid State. (pending.Length={0}, pendingCount={1})", dstate.pending.Length, dstate.pendingCount));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506369uL);
      Array.Copy(dstate.pending, dstate.nextPending, OutputBuffer, NextOut, len);
      NextOut += len;
      dstate.nextPending += len;
      TotalBytesOut += len;
      AvailableBytesOut -= len;
      dstate.pendingCount -= len;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506370uL);
      if (dstate.pendingCount == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506371uL);
        dstate.nextPending = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506372uL);
    }
    internal int read_buf(byte[] buf, int start, int size)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506373uL);
      int len = AvailableBytesIn;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506374uL);
      if (len > size) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506375uL);
        len = size;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506376uL);
      if (len == 0) {
        System.Int32 RNTRNTRNT_720 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506377uL);
        return RNTRNTRNT_720;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506378uL);
      AvailableBytesIn -= len;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506379uL);
      if (dstate.WantRfc1950HeaderBytes) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506380uL);
        _Adler32 = Adler.Adler32(_Adler32, InputBuffer, NextIn, len);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506381uL);
      Array.Copy(InputBuffer, NextIn, buf, start, len);
      NextIn += len;
      TotalBytesIn += len;
      System.Int32 RNTRNTRNT_721 = len;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506382uL);
      return RNTRNTRNT_721;
    }
  }
}
