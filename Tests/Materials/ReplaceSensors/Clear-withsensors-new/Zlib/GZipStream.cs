using System;
using System.IO;
namespace Ionic.Zlib
{
  public class GZipStream : System.IO.Stream
  {
    public String Comment {
      get {
        String RNTRNTRNT_569 = _Comment;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505124uL);
        return RNTRNTRNT_569;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505125uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505126uL);
          throw new ObjectDisposedException("GZipStream");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505127uL);
        _Comment = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505128uL);
      }
    }
    public String FileName {
      get {
        String RNTRNTRNT_570 = _FileName;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505129uL);
        return RNTRNTRNT_570;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505130uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505131uL);
          throw new ObjectDisposedException("GZipStream");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505132uL);
        _FileName = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505133uL);
        if (_FileName == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505134uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505135uL);
        if (_FileName.IndexOf("/") != -1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505136uL);
          _FileName = _FileName.Replace("/", "\\");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505137uL);
        if (_FileName.EndsWith("\\")) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505138uL);
          throw new Exception("Illegal filename");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505139uL);
        if (_FileName.IndexOf("\\") != -1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505140uL);
          _FileName = Path.GetFileName(_FileName);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505141uL);
      }
    }
    public DateTime? LastModified;
    public int Crc32 {
      get {
        System.Int32 RNTRNTRNT_571 = _Crc32;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505142uL);
        return RNTRNTRNT_571;
      }
    }
    private int _headerByteCount;
    internal ZlibBaseStream _baseStream;
    bool _disposed;
    bool _firstReadDone;
    string _FileName;
    string _Comment;
    int _Crc32;
    public GZipStream(Stream stream, CompressionMode mode) : this(stream, mode, CompressionLevel.Default, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505143uL);
    }
    public GZipStream(Stream stream, CompressionMode mode, CompressionLevel level) : this(stream, mode, level, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505144uL);
    }
    public GZipStream(Stream stream, CompressionMode mode, bool leaveOpen) : this(stream, mode, CompressionLevel.Default, leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505145uL);
    }
    public GZipStream(Stream stream, CompressionMode mode, CompressionLevel level, bool leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505146uL);
      _baseStream = new ZlibBaseStream(stream, mode, level, ZlibStreamFlavor.GZIP, leaveOpen);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505147uL);
    }
    public virtual FlushType FlushMode {
      get {
        FlushType RNTRNTRNT_572 = (this._baseStream._flushMode);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505148uL);
        return RNTRNTRNT_572;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505149uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505150uL);
          throw new ObjectDisposedException("GZipStream");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505151uL);
        this._baseStream._flushMode = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505152uL);
      }
    }
    public int BufferSize {
      get {
        System.Int32 RNTRNTRNT_573 = this._baseStream._bufferSize;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505153uL);
        return RNTRNTRNT_573;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505154uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505155uL);
          throw new ObjectDisposedException("GZipStream");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505156uL);
        if (this._baseStream._workingBuffer != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505157uL);
          throw new ZlibException("The working buffer is already set.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505158uL);
        if (value < ZlibConstants.WorkingBufferSizeMin) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505159uL);
          throw new ZlibException(String.Format("Don't be silly. {0} bytes?? Use a bigger buffer, at least {1}.", value, ZlibConstants.WorkingBufferSizeMin));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505160uL);
        this._baseStream._bufferSize = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505161uL);
      }
    }
    public virtual long TotalIn {
      get {
        System.Int64 RNTRNTRNT_574 = this._baseStream._z.TotalBytesIn;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505162uL);
        return RNTRNTRNT_574;
      }
    }
    public virtual long TotalOut {
      get {
        System.Int64 RNTRNTRNT_575 = this._baseStream._z.TotalBytesOut;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505163uL);
        return RNTRNTRNT_575;
      }
    }
    protected override void Dispose(bool disposing)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505164uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505165uL);
        if (!_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505166uL);
          if (disposing && (this._baseStream != null)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505167uL);
            this._baseStream.Close();
            this._Crc32 = _baseStream.Crc32;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505168uL);
          _disposed = true;
        }
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505169uL);
        base.Dispose(disposing);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505170uL);
    }
    public override bool CanRead {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505171uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505172uL);
          throw new ObjectDisposedException("GZipStream");
        }
        System.Boolean RNTRNTRNT_576 = _baseStream._stream.CanRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505173uL);
        return RNTRNTRNT_576;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_577 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505174uL);
        return RNTRNTRNT_577;
      }
    }
    public override bool CanWrite {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505175uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505176uL);
          throw new ObjectDisposedException("GZipStream");
        }
        System.Boolean RNTRNTRNT_578 = _baseStream._stream.CanWrite;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505177uL);
        return RNTRNTRNT_578;
      }
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505178uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505179uL);
        throw new ObjectDisposedException("GZipStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505180uL);
      _baseStream.Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505181uL);
    }
    public override long Length {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505182uL);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505183uL);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Writer) {
          System.Int64 RNTRNTRNT_579 = this._baseStream._z.TotalBytesOut + _headerByteCount;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505184uL);
          return RNTRNTRNT_579;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505185uL);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Reader) {
          System.Int64 RNTRNTRNT_580 = this._baseStream._z.TotalBytesIn + this._baseStream._gzipHeaderByteCount;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505186uL);
          return RNTRNTRNT_580;
        }
        System.Int64 RNTRNTRNT_581 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505187uL);
        return RNTRNTRNT_581;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505188uL);
        throw new NotImplementedException();
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505189uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505190uL);
        throw new ObjectDisposedException("GZipStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505191uL);
      int n = _baseStream.Read(buffer, offset, count);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505192uL);
      if (!_firstReadDone) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505193uL);
        _firstReadDone = true;
        FileName = _baseStream._GzipFileName;
        Comment = _baseStream._GzipComment;
      }
      System.Int32 RNTRNTRNT_582 = n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505194uL);
      return RNTRNTRNT_582;
    }
    public override long Seek(long offset, SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505195uL);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505196uL);
      throw new NotImplementedException();
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505197uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505198uL);
        throw new ObjectDisposedException("GZipStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505199uL);
      if (_baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Undefined) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505200uL);
        if (_baseStream._wantCompress) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505201uL);
          _headerByteCount = EmitHeader();
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505202uL);
          throw new InvalidOperationException();
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505203uL);
      _baseStream.Write(buffer, offset, count);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505204uL);
    }
    static internal readonly System.DateTime _unixEpoch = new System.DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    static internal readonly System.Text.Encoding iso8859dash1 = System.Text.Encoding.GetEncoding("iso-8859-1");
    private int EmitHeader()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505205uL);
      byte[] commentBytes = (Comment == null) ? null : iso8859dash1.GetBytes(Comment);
      byte[] filenameBytes = (FileName == null) ? null : iso8859dash1.GetBytes(FileName);
      int cbLength = (Comment == null) ? 0 : commentBytes.Length + 1;
      int fnLength = (FileName == null) ? 0 : filenameBytes.Length + 1;
      int bufferLength = 10 + cbLength + fnLength;
      byte[] header = new byte[bufferLength];
      int i = 0;
      header[i++] = 0x1f;
      header[i++] = 0x8b;
      header[i++] = 8;
      byte flag = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505206uL);
      if (Comment != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505207uL);
        flag ^= 0x10;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505208uL);
      if (FileName != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505209uL);
        flag ^= 0x8;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505210uL);
      header[i++] = flag;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505211uL);
      if (!LastModified.HasValue) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505212uL);
        LastModified = DateTime.Now;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505213uL);
      System.TimeSpan delta = LastModified.Value - _unixEpoch;
      Int32 timet = (Int32)delta.TotalSeconds;
      Array.Copy(BitConverter.GetBytes(timet), 0, header, i, 4);
      i += 4;
      header[i++] = 0;
      header[i++] = 0xff;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505214uL);
      if (fnLength != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505215uL);
        Array.Copy(filenameBytes, 0, header, i, fnLength - 1);
        i += fnLength - 1;
        header[i++] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505216uL);
      if (cbLength != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505217uL);
        Array.Copy(commentBytes, 0, header, i, cbLength - 1);
        i += cbLength - 1;
        header[i++] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505218uL);
      _baseStream._stream.Write(header, 0, header.Length);
      System.Int32 RNTRNTRNT_583 = header.Length;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505219uL);
      return RNTRNTRNT_583;
    }
    public static byte[] CompressString(String s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505220uL);
      using (var ms = new MemoryStream()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505221uL);
        System.IO.Stream compressor = new GZipStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressString(s, compressor);
        System.Byte[] RNTRNTRNT_584 = ms.ToArray();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505222uL);
        return RNTRNTRNT_584;
      }
    }
    public static byte[] CompressBuffer(byte[] b)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505223uL);
      using (var ms = new MemoryStream()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505224uL);
        System.IO.Stream compressor = new GZipStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressBuffer(b, compressor);
        System.Byte[] RNTRNTRNT_585 = ms.ToArray();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505225uL);
        return RNTRNTRNT_585;
      }
    }
    public static String UncompressString(byte[] compressed)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505226uL);
      using (var input = new MemoryStream(compressed)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505227uL);
        Stream decompressor = new GZipStream(input, CompressionMode.Decompress);
        String RNTRNTRNT_586 = ZlibBaseStream.UncompressString(compressed, decompressor);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505228uL);
        return RNTRNTRNT_586;
      }
    }
    public static byte[] UncompressBuffer(byte[] compressed)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505229uL);
      using (var input = new System.IO.MemoryStream(compressed)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505230uL);
        System.IO.Stream decompressor = new GZipStream(input, CompressionMode.Decompress);
        System.Byte[] RNTRNTRNT_587 = ZlibBaseStream.UncompressBuffer(compressed, decompressor);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505231uL);
        return RNTRNTRNT_587;
      }
    }
  }
}
