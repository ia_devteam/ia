using System;
using System.Collections.Generic;
using System.Threading;
using Ionic.Zlib;
using System.IO;
namespace Ionic.Zlib
{
  internal class WorkItem
  {
    public byte[] buffer;
    public byte[] compressed;
    public int crc;
    public int index;
    public int ordinal;
    public int inputBytesAvailable;
    public int compressedBytesAvailable;
    public ZlibCodec compressor;
    public WorkItem(int size, Ionic.Zlib.CompressionLevel compressLevel, CompressionStrategy strategy, int ix)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505853uL);
      this.buffer = new byte[size];
      int n = size + ((size / 32768) + 1) * 5 * 2;
      this.compressed = new byte[n];
      this.compressor = new ZlibCodec();
      this.compressor.InitializeDeflate(compressLevel, false);
      this.compressor.OutputBuffer = this.compressed;
      this.compressor.InputBuffer = this.buffer;
      this.index = ix;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505854uL);
    }
  }
  public class ParallelDeflateOutputStream : System.IO.Stream
  {
    private static readonly int IO_BUFFER_SIZE_DEFAULT = 64 * 1024;
    private static readonly int BufferPairsPerCore = 4;
    private System.Collections.Generic.List<WorkItem> _pool;
    private bool _leaveOpen;
    private bool emitting;
    private System.IO.Stream _outStream;
    private int _maxBufferPairs;
    private int _bufferSize = IO_BUFFER_SIZE_DEFAULT;
    private AutoResetEvent _newlyCompressedBlob;
    private object _outputLock = new object();
    private bool _isClosed;
    private bool _firstWriteDone;
    private int _currentlyFilling;
    private int _lastFilled;
    private int _lastWritten;
    private int _latestCompressed;
    private int _Crc32;
    private Ionic.Crc.CRC32 _runningCrc;
    private object _latestLock = new object();
    private System.Collections.Generic.Queue<int> _toWrite;
    private System.Collections.Generic.Queue<int> _toFill;
    private Int64 _totalBytesProcessed;
    private Ionic.Zlib.CompressionLevel _compressLevel;
    private volatile Exception _pendingException;
    private bool _handlingException;
    private object _eLock = new Object();
    private TraceBits _DesiredTrace = TraceBits.Session | TraceBits.Compress | TraceBits.WriteTake | TraceBits.WriteEnter | TraceBits.EmitEnter | TraceBits.EmitDone | TraceBits.EmitLock | TraceBits.EmitSkip | TraceBits.EmitBegin;
    public ParallelDeflateOutputStream(System.IO.Stream stream) : this(stream, CompressionLevel.Default, CompressionStrategy.Default, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505855uL);
    }
    public ParallelDeflateOutputStream(System.IO.Stream stream, CompressionLevel level) : this(stream, level, CompressionStrategy.Default, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505856uL);
    }
    public ParallelDeflateOutputStream(System.IO.Stream stream, bool leaveOpen) : this(stream, CompressionLevel.Default, CompressionStrategy.Default, leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505857uL);
    }
    public ParallelDeflateOutputStream(System.IO.Stream stream, CompressionLevel level, bool leaveOpen) : this(stream, CompressionLevel.Default, CompressionStrategy.Default, leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505858uL);
    }
    public ParallelDeflateOutputStream(System.IO.Stream stream, CompressionLevel level, CompressionStrategy strategy, bool leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505859uL);
      TraceOutput(TraceBits.Lifecycle | TraceBits.Session, "-------------------------------------------------------");
      TraceOutput(TraceBits.Lifecycle | TraceBits.Session, "Create {0:X8}", this.GetHashCode());
      _outStream = stream;
      _compressLevel = level;
      Strategy = strategy;
      _leaveOpen = leaveOpen;
      this.MaxBufferPairs = 16;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505860uL);
    }
    public CompressionStrategy Strategy { get; private set; }
    public int MaxBufferPairs {
      get {
        System.Int32 RNTRNTRNT_663 = _maxBufferPairs;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505861uL);
        return RNTRNTRNT_663;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505862uL);
        if (value < 4) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505863uL);
          throw new ArgumentException("MaxBufferPairs", "Value must be 4 or greater.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505864uL);
        _maxBufferPairs = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505865uL);
      }
    }
    public int BufferSize {
      get {
        System.Int32 RNTRNTRNT_664 = _bufferSize;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505866uL);
        return RNTRNTRNT_664;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505867uL);
        if (value < 1024) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505868uL);
          throw new ArgumentOutOfRangeException("BufferSize", "BufferSize must be greater than 1024 bytes");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505869uL);
        _bufferSize = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505870uL);
      }
    }
    public int Crc32 {
      get {
        System.Int32 RNTRNTRNT_665 = _Crc32;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505871uL);
        return RNTRNTRNT_665;
      }
    }
    public Int64 BytesProcessed {
      get {
        Int64 RNTRNTRNT_666 = _totalBytesProcessed;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505872uL);
        return RNTRNTRNT_666;
      }
    }
    private void _InitializePoolOfWorkItems()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505873uL);
      _toWrite = new Queue<int>();
      _toFill = new Queue<int>();
      _pool = new System.Collections.Generic.List<WorkItem>();
      int nTasks = BufferPairsPerCore * Environment.ProcessorCount;
      nTasks = Math.Min(nTasks, _maxBufferPairs);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505874uL);
      for (int i = 0; i < nTasks; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505875uL);
        _pool.Add(new WorkItem(_bufferSize, _compressLevel, Strategy, i));
        _toFill.Enqueue(i);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505876uL);
      _newlyCompressedBlob = new AutoResetEvent(false);
      _runningCrc = new Ionic.Crc.CRC32();
      _currentlyFilling = -1;
      _lastFilled = -1;
      _lastWritten = -1;
      _latestCompressed = -1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505877uL);
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505878uL);
      bool mustWait = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505879uL);
      if (_isClosed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505880uL);
        throw new InvalidOperationException();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505881uL);
      if (_pendingException != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505882uL);
        _handlingException = true;
        var pe = _pendingException;
        _pendingException = null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505883uL);
        throw pe;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505884uL);
      if (count == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505885uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505886uL);
      if (!_firstWriteDone) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505887uL);
        _InitializePoolOfWorkItems();
        _firstWriteDone = true;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505888uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505889uL);
        EmitPendingBuffers(false, mustWait);
        mustWait = false;
        int ix = -1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505890uL);
        if (_currentlyFilling >= 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505891uL);
          ix = _currentlyFilling;
          TraceOutput(TraceBits.WriteTake, "Write    notake   wi({0}) lf({1})", ix, _lastFilled);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505892uL);
          TraceOutput(TraceBits.WriteTake, "Write    take?");
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505893uL);
          if (_toFill.Count == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505894uL);
            mustWait = true;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505895uL);
            continue;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505896uL);
          ix = _toFill.Dequeue();
          TraceOutput(TraceBits.WriteTake, "Write    take     wi({0}) lf({1})", ix, _lastFilled);
          ++_lastFilled;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505897uL);
        WorkItem workitem = _pool[ix];
        int limit = ((workitem.buffer.Length - workitem.inputBytesAvailable) > count) ? count : (workitem.buffer.Length - workitem.inputBytesAvailable);
        workitem.ordinal = _lastFilled;
        TraceOutput(TraceBits.Write, "Write    lock     wi({0}) ord({1}) iba({2})", workitem.index, workitem.ordinal, workitem.inputBytesAvailable);
        Buffer.BlockCopy(buffer, offset, workitem.buffer, workitem.inputBytesAvailable, limit);
        count -= limit;
        offset += limit;
        workitem.inputBytesAvailable += limit;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505898uL);
        if (workitem.inputBytesAvailable == workitem.buffer.Length) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505899uL);
          TraceOutput(TraceBits.Write, "Write    QUWI     wi({0}) ord({1}) iba({2}) nf({3})", workitem.index, workitem.ordinal, workitem.inputBytesAvailable);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505900uL);
          if (!ThreadPool.QueueUserWorkItem(_DeflateOne, workitem)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505901uL);
            throw new Exception("Cannot enqueue workitem");
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505902uL);
          _currentlyFilling = -1;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505903uL);
          _currentlyFilling = ix;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505904uL);
        if (count > 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505905uL);
          TraceOutput(TraceBits.WriteEnter, "Write    more");
        }
      } while (count > 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505906uL);
      TraceOutput(TraceBits.WriteEnter, "Write    exit");
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505907uL);
      return;
    }
    private void _FlushFinish()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505908uL);
      byte[] buffer = new byte[128];
      var compressor = new ZlibCodec();
      int rc = compressor.InitializeDeflate(_compressLevel, false);
      compressor.InputBuffer = null;
      compressor.NextIn = 0;
      compressor.AvailableBytesIn = 0;
      compressor.OutputBuffer = buffer;
      compressor.NextOut = 0;
      compressor.AvailableBytesOut = buffer.Length;
      rc = compressor.Deflate(FlushType.Finish);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505909uL);
      if (rc != ZlibConstants.Z_STREAM_END && rc != ZlibConstants.Z_OK) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505910uL);
        throw new Exception("deflating: " + compressor.Message);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505911uL);
      if (buffer.Length - compressor.AvailableBytesOut > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505912uL);
        TraceOutput(TraceBits.EmitBegin, "Emit     begin    flush bytes({0})", buffer.Length - compressor.AvailableBytesOut);
        _outStream.Write(buffer, 0, buffer.Length - compressor.AvailableBytesOut);
        TraceOutput(TraceBits.EmitDone, "Emit     done     flush");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505913uL);
      compressor.EndDeflate();
      _Crc32 = _runningCrc.Crc32Result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505914uL);
    }
    private void _Flush(bool lastInput)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505915uL);
      if (_isClosed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505916uL);
        throw new InvalidOperationException();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505917uL);
      if (emitting) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505918uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505919uL);
      if (_currentlyFilling >= 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505920uL);
        WorkItem workitem = _pool[_currentlyFilling];
        _DeflateOne(workitem);
        _currentlyFilling = -1;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505921uL);
      if (lastInput) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505922uL);
        EmitPendingBuffers(true, false);
        _FlushFinish();
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505923uL);
        EmitPendingBuffers(false, false);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505924uL);
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505925uL);
      if (_pendingException != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505926uL);
        _handlingException = true;
        var pe = _pendingException;
        _pendingException = null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505927uL);
        throw pe;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505928uL);
      if (_handlingException) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505929uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505930uL);
      _Flush(false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505931uL);
    }
    public override void Close()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505932uL);
      TraceOutput(TraceBits.Session, "Close {0:X8}", this.GetHashCode());
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505933uL);
      if (_pendingException != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505934uL);
        _handlingException = true;
        var pe = _pendingException;
        _pendingException = null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505935uL);
        throw pe;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505936uL);
      if (_handlingException) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505937uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505938uL);
      if (_isClosed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505939uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505940uL);
      _Flush(true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505941uL);
      if (!_leaveOpen) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505942uL);
        _outStream.Close();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505943uL);
      _isClosed = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505944uL);
    }
    public new void Dispose()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505945uL);
      TraceOutput(TraceBits.Lifecycle, "Dispose  {0:X8}", this.GetHashCode());
      Close();
      _pool = null;
      Dispose(true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505946uL);
    }
    protected override void Dispose(bool disposing)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505947uL);
      base.Dispose(disposing);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505948uL);
    }
    public void Reset(Stream stream)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505949uL);
      TraceOutput(TraceBits.Session, "-------------------------------------------------------");
      TraceOutput(TraceBits.Session, "Reset {0:X8} firstDone({1})", this.GetHashCode(), _firstWriteDone);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505950uL);
      if (!_firstWriteDone) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505951uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505952uL);
      _toWrite.Clear();
      _toFill.Clear();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505953uL);
      foreach (var workitem in _pool) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505954uL);
        _toFill.Enqueue(workitem.index);
        workitem.ordinal = -1;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505955uL);
      _firstWriteDone = false;
      _totalBytesProcessed = 0L;
      _runningCrc = new Ionic.Crc.CRC32();
      _isClosed = false;
      _currentlyFilling = -1;
      _lastFilled = -1;
      _lastWritten = -1;
      _latestCompressed = -1;
      _outStream = stream;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505956uL);
    }
    private void EmitPendingBuffers(bool doAll, bool mustWait)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505957uL);
      if (emitting) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505958uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505959uL);
      emitting = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505960uL);
      if (doAll || mustWait) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505961uL);
        _newlyCompressedBlob.WaitOne();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505962uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505963uL);
        int firstSkip = -1;
        int millisecondsToWait = doAll ? 200 : (mustWait ? -1 : 0);
        int nextToWrite = -1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505964uL);
        do {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505965uL);
          if (Monitor.TryEnter(_toWrite, millisecondsToWait)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505966uL);
            nextToWrite = -1;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505967uL);
            try {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505968uL);
              if (_toWrite.Count > 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505969uL);
                nextToWrite = _toWrite.Dequeue();
              }
            } finally {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505970uL);
              Monitor.Exit(_toWrite);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505971uL);
            if (nextToWrite >= 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505972uL);
              WorkItem workitem = _pool[nextToWrite];
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505973uL);
              if (workitem.ordinal != _lastWritten + 1) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505974uL);
                TraceOutput(TraceBits.EmitSkip, "Emit     skip     wi({0}) ord({1}) lw({2}) fs({3})", workitem.index, workitem.ordinal, _lastWritten, firstSkip);
                lock (_toWrite) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505975uL);
                  _toWrite.Enqueue(nextToWrite);
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505976uL);
                if (firstSkip == nextToWrite) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505977uL);
                  _newlyCompressedBlob.WaitOne();
                  firstSkip = -1;
                } else if (firstSkip == -1) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505978uL);
                  firstSkip = nextToWrite;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505979uL);
                continue;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505980uL);
              firstSkip = -1;
              TraceOutput(TraceBits.EmitBegin, "Emit     begin    wi({0}) ord({1})              cba({2})", workitem.index, workitem.ordinal, workitem.compressedBytesAvailable);
              _outStream.Write(workitem.compressed, 0, workitem.compressedBytesAvailable);
              _runningCrc.Combine(workitem.crc, workitem.inputBytesAvailable);
              _totalBytesProcessed += workitem.inputBytesAvailable;
              workitem.inputBytesAvailable = 0;
              TraceOutput(TraceBits.EmitDone, "Emit     done     wi({0}) ord({1})              cba({2}) mtw({3})", workitem.index, workitem.ordinal, workitem.compressedBytesAvailable, millisecondsToWait);
              _lastWritten = workitem.ordinal;
              _toFill.Enqueue(workitem.index);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505981uL);
              if (millisecondsToWait == -1) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505982uL);
                millisecondsToWait = 0;
              }
            }
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505983uL);
            nextToWrite = -1;
          }
        } while (nextToWrite >= 0);
      } while (doAll && (_lastWritten != _latestCompressed));
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505984uL);
      emitting = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505985uL);
    }
    private void _DeflateOne(Object wi)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505986uL);
      WorkItem workitem = (WorkItem)wi;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505987uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505988uL);
        int myItem = workitem.index;
        Ionic.Crc.CRC32 crc = new Ionic.Crc.CRC32();
        crc.SlurpBlock(workitem.buffer, 0, workitem.inputBytesAvailable);
        DeflateOneSegment(workitem);
        workitem.crc = crc.Crc32Result;
        TraceOutput(TraceBits.Compress, "Compress          wi({0}) ord({1}) len({2})", workitem.index, workitem.ordinal, workitem.compressedBytesAvailable);
        lock (_latestLock) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505989uL);
          if (workitem.ordinal > _latestCompressed) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505990uL);
            _latestCompressed = workitem.ordinal;
          }
        }
        lock (_toWrite) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505991uL);
          _toWrite.Enqueue(workitem.index);
        }
        _newlyCompressedBlob.Set();
      } catch (System.Exception exc1) {
        lock (_eLock) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505992uL);
          if (_pendingException != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505993uL);
            _pendingException = exc1;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505994uL);
    }
    private bool DeflateOneSegment(WorkItem workitem)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505995uL);
      ZlibCodec compressor = workitem.compressor;
      int rc = 0;
      compressor.ResetDeflate();
      compressor.NextIn = 0;
      compressor.AvailableBytesIn = workitem.inputBytesAvailable;
      compressor.NextOut = 0;
      compressor.AvailableBytesOut = workitem.compressed.Length;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505996uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505997uL);
        compressor.Deflate(FlushType.None);
      } while (compressor.AvailableBytesIn > 0 || compressor.AvailableBytesOut == 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505998uL);
      rc = compressor.Deflate(FlushType.Sync);
      workitem.compressedBytesAvailable = (int)compressor.TotalBytesOut;
      System.Boolean RNTRNTRNT_667 = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505999uL);
      return RNTRNTRNT_667;
    }
    [System.Diagnostics.ConditionalAttribute("Trace")]
    private void TraceOutput(TraceBits bits, string format, params object[] varParams)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506000uL);
      if ((bits & _DesiredTrace) != 0) {
        lock (_outputLock) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506001uL);
          int tid = Thread.CurrentThread.GetHashCode();
          Console.ForegroundColor = (ConsoleColor)(tid % 8 + 8);
          Console.Write("{0:000} PDOS ", tid);
          Console.WriteLine(format, varParams);
          Console.ResetColor();
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506002uL);
    }
    [Flags()]
    enum TraceBits : uint
    {
      None = 0,
      NotUsed1 = 1,
      EmitLock = 2,
      EmitEnter = 4,
      EmitBegin = 8,
      EmitDone = 16,
      EmitSkip = 32,
      EmitAll = 58,
      Flush = 64,
      Lifecycle = 128,
      Session = 256,
      Synch = 512,
      Instance = 1024,
      Compress = 2048,
      Write = 4096,
      WriteEnter = 8192,
      WriteTake = 16384,
      All = 0xffffffffu
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_668 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506003uL);
        return RNTRNTRNT_668;
      }
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_669 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506004uL);
        return RNTRNTRNT_669;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_670 = _outStream.CanWrite;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506005uL);
        return RNTRNTRNT_670;
      }
    }
    public override long Length {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506006uL);
        throw new NotSupportedException();
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_671 = _outStream.Position;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506007uL);
        return RNTRNTRNT_671;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506008uL);
        throw new NotSupportedException();
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506009uL);
      throw new NotSupportedException();
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506010uL);
      throw new NotSupportedException();
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(506011uL);
      throw new NotSupportedException();
    }
  }
}
