using System;
namespace Ionic.Zlib
{
  sealed class InflateBlocks
  {
    private const int MANY = 1440;
    static internal readonly int[] border = new int[] {
      16,
      17,
      18,
      0,
      8,
      7,
      9,
      6,
      10,
      5,
      11,
      4,
      12,
      3,
      13,
      2,
      14,
      1,
      15
    };
    private enum InflateBlockMode
    {
      TYPE = 0,
      LENS = 1,
      STORED = 2,
      TABLE = 3,
      BTREE = 4,
      DTREE = 5,
      CODES = 6,
      DRY = 7,
      DONE = 8,
      BAD = 9
    }
    private InflateBlockMode mode;
    internal int left;
    internal int table;
    internal int index;
    internal int[] blens;
    internal int[] bb = new int[1];
    internal int[] tb = new int[1];
    internal InflateCodes codes = new InflateCodes();
    internal int last;
    internal ZlibCodec _codec;
    internal int bitk;
    internal int bitb;
    internal int[] hufts;
    internal byte[] window;
    internal int end;
    internal int readAt;
    internal int writeAt;
    internal System.Object checkfn;
    internal uint check;
    internal InfTree inftree = new InfTree();
    internal InflateBlocks(ZlibCodec codec, System.Object checkfn, int w)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505232uL);
      _codec = codec;
      hufts = new int[MANY * 3];
      window = new byte[w];
      end = w;
      this.checkfn = checkfn;
      mode = InflateBlockMode.TYPE;
      Reset();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505233uL);
    }
    internal uint Reset()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505234uL);
      uint oldCheck = check;
      mode = InflateBlockMode.TYPE;
      bitk = 0;
      bitb = 0;
      readAt = writeAt = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505235uL);
      if (checkfn != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505236uL);
        _codec._Adler32 = check = Adler.Adler32(0, null, 0, 0);
      }
      System.UInt32 RNTRNTRNT_588 = oldCheck;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505237uL);
      return RNTRNTRNT_588;
    }
    internal int Process(int r)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505238uL);
      int t;
      int b;
      int k;
      int p;
      int n;
      int q;
      int m;
      p = _codec.NextIn;
      n = _codec.AvailableBytesIn;
      b = bitb;
      k = bitk;
      q = writeAt;
      m = (int)(q < readAt ? readAt - q - 1 : end - q);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505239uL);
      while (true) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505240uL);
        switch (mode) {
          case InflateBlockMode.TYPE:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505262uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505241uL);
              while (k < (3)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505242uL);
                if (n != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505243uL);
                  r = ZlibConstants.Z_OK;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505244uL);
                  bitb = b;
                  bitk = k;
                  _codec.AvailableBytesIn = n;
                  _codec.TotalBytesIn += p - _codec.NextIn;
                  _codec.NextIn = p;
                  writeAt = q;
                  System.Int32 RNTRNTRNT_589 = Flush(r);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505245uL);
                  return RNTRNTRNT_589;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505246uL);
                n--;
                b |= (_codec.InputBuffer[p++] & 0xff) << k;
                k += 8;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505247uL);
              t = (int)(b & 7);
              last = t & 1;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505248uL);
              switch ((uint)t >> 1) {
                case 0:
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505251uL);
                  
                  {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505249uL);
                    b >>= 3;
                    k -= (3);
                    t = k & 7;
                    b >>= t;
                    k -= t;
                    mode = InflateBlockMode.LENS;
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505250uL);
                    break;
                  }

                case 1:
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505254uL);
                  
                  {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505252uL);
                    int[] bl = new int[1];
                    int[] bd = new int[1];
                    int[][] tl = new int[1][];
                    int[][] td = new int[1][];
                    InfTree.inflate_trees_fixed(bl, bd, tl, td, _codec);
                    codes.Init(bl[0], bd[0], tl[0], 0, td[0], 0);
                    b >>= 3;
                    k -= 3;
                    mode = InflateBlockMode.CODES;
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505253uL);
                    break;
                  }

                case 2:
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505257uL);
                  
                  {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505255uL);
                    b >>= 3;
                    k -= 3;
                    mode = InflateBlockMode.TABLE;
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505256uL);
                    break;
                  }

                case 3:
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505260uL);
                  
                  {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505258uL);
                    b >>= 3;
                    k -= 3;
                    mode = InflateBlockMode.BAD;
                    _codec.Message = "invalid block type";
                    r = ZlibConstants.Z_DATA_ERROR;
                    bitb = b;
                    bitk = k;
                    _codec.AvailableBytesIn = n;
                    _codec.TotalBytesIn += p - _codec.NextIn;
                    _codec.NextIn = p;
                    writeAt = q;
                    System.Int32 RNTRNTRNT_590 = Flush(r);
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505259uL);
                    return RNTRNTRNT_590;
                  }

              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505261uL);
              break;
            }

          case InflateBlockMode.LENS:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505273uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505263uL);
              while (k < (32)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505264uL);
                if (n != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505265uL);
                  r = ZlibConstants.Z_OK;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505266uL);
                  bitb = b;
                  bitk = k;
                  _codec.AvailableBytesIn = n;
                  _codec.TotalBytesIn += p - _codec.NextIn;
                  _codec.NextIn = p;
                  writeAt = q;
                  System.Int32 RNTRNTRNT_591 = Flush(r);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505267uL);
                  return RNTRNTRNT_591;
                }
                ;
                n--;
                b |= (_codec.InputBuffer[p++] & 0xff) << k;
                k += 8;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505268uL);
              if ((((~b) >> 16) & 0xffff) != (b & 0xffff)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505269uL);
                mode = InflateBlockMode.BAD;
                _codec.Message = "invalid stored block lengths";
                r = ZlibConstants.Z_DATA_ERROR;
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_592 = Flush(r);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505270uL);
                return RNTRNTRNT_592;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505271uL);
              left = (b & 0xffff);
              b = k = 0;
              mode = left != 0 ? InflateBlockMode.STORED : (last != 0 ? InflateBlockMode.DRY : InflateBlockMode.TYPE);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505272uL);
              break;
            }

          case InflateBlockMode.STORED:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505297uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505274uL);
              if (n == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505275uL);
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_593 = Flush(r);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505276uL);
                return RNTRNTRNT_593;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505277uL);
              if (m == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505278uL);
                if (q == end && readAt != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505279uL);
                  q = 0;
                  m = (int)(q < readAt ? readAt - q - 1 : end - q);
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505280uL);
                if (m == 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505281uL);
                  writeAt = q;
                  r = Flush(r);
                  q = writeAt;
                  m = (int)(q < readAt ? readAt - q - 1 : end - q);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505282uL);
                  if (q == end && readAt != 0) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505283uL);
                    q = 0;
                    m = (int)(q < readAt ? readAt - q - 1 : end - q);
                  }
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505284uL);
                  if (m == 0) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505285uL);
                    bitb = b;
                    bitk = k;
                    _codec.AvailableBytesIn = n;
                    _codec.TotalBytesIn += p - _codec.NextIn;
                    _codec.NextIn = p;
                    writeAt = q;
                    System.Int32 RNTRNTRNT_594 = Flush(r);
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505286uL);
                    return RNTRNTRNT_594;
                  }
                }
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505287uL);
              r = ZlibConstants.Z_OK;
              t = left;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505288uL);
              if (t > n) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505289uL);
                t = n;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505290uL);
              if (t > m) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505291uL);
                t = m;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505292uL);
              Array.Copy(_codec.InputBuffer, p, window, q, t);
              p += t;
              n -= t;
              q += t;
              m -= t;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505293uL);
              if ((left -= t) != 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505294uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505295uL);
              mode = last != 0 ? InflateBlockMode.DRY : InflateBlockMode.TYPE;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505296uL);
              break;
            }

          case InflateBlockMode.TABLE:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505314uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505298uL);
              while (k < (14)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505299uL);
                if (n != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505300uL);
                  r = ZlibConstants.Z_OK;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505301uL);
                  bitb = b;
                  bitk = k;
                  _codec.AvailableBytesIn = n;
                  _codec.TotalBytesIn += p - _codec.NextIn;
                  _codec.NextIn = p;
                  writeAt = q;
                  System.Int32 RNTRNTRNT_595 = Flush(r);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505302uL);
                  return RNTRNTRNT_595;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505303uL);
                n--;
                b |= (_codec.InputBuffer[p++] & 0xff) << k;
                k += 8;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505304uL);
              table = t = (b & 0x3fff);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505305uL);
              if ((t & 0x1f) > 29 || ((t >> 5) & 0x1f) > 29) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505306uL);
                mode = InflateBlockMode.BAD;
                _codec.Message = "too many length or distance symbols";
                r = ZlibConstants.Z_DATA_ERROR;
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_596 = Flush(r);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505307uL);
                return RNTRNTRNT_596;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505308uL);
              t = 258 + (t & 0x1f) + ((t >> 5) & 0x1f);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505309uL);
              if (blens == null || blens.Length < t) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505310uL);
                blens = new int[t];
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505311uL);
                Array.Clear(blens, 0, t);
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505312uL);
              b >>= 14;
              k -= 14;
              index = 0;
              mode = InflateBlockMode.BTREE;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505313uL);
              goto case InflateBlockMode.BTREE;
            }

          case InflateBlockMode.BTREE:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505334uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505315uL);
              while (index < 4 + (table >> 10)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505316uL);
                while (k < (3)) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505317uL);
                  if (n != 0) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505318uL);
                    r = ZlibConstants.Z_OK;
                  } else {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505319uL);
                    bitb = b;
                    bitk = k;
                    _codec.AvailableBytesIn = n;
                    _codec.TotalBytesIn += p - _codec.NextIn;
                    _codec.NextIn = p;
                    writeAt = q;
                    System.Int32 RNTRNTRNT_597 = Flush(r);
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505320uL);
                    return RNTRNTRNT_597;
                  }
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505321uL);
                  n--;
                  b |= (_codec.InputBuffer[p++] & 0xff) << k;
                  k += 8;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505322uL);
                blens[border[index++]] = b & 7;
                b >>= 3;
                k -= 3;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505323uL);
              while (index < 19) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505324uL);
                blens[border[index++]] = 0;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505325uL);
              bb[0] = 7;
              t = inftree.inflate_trees_bits(blens, bb, tb, hufts, _codec);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505326uL);
              if (t != ZlibConstants.Z_OK) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505327uL);
                r = t;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505328uL);
                if (r == ZlibConstants.Z_DATA_ERROR) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505329uL);
                  blens = null;
                  mode = InflateBlockMode.BAD;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505330uL);
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_598 = Flush(r);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505331uL);
                return RNTRNTRNT_598;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505332uL);
              index = 0;
              mode = InflateBlockMode.DTREE;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505333uL);
              goto case InflateBlockMode.DTREE;
            }

          case InflateBlockMode.DTREE:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505365uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505335uL);
              while (true) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505336uL);
                t = table;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505337uL);
                if (!(index < 258 + (t & 0x1f) + ((t >> 5) & 0x1f))) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505338uL);
                  break;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505339uL);
                int i, j, c;
                t = bb[0];
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505340uL);
                while (k < t) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505341uL);
                  if (n != 0) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505342uL);
                    r = ZlibConstants.Z_OK;
                  } else {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505343uL);
                    bitb = b;
                    bitk = k;
                    _codec.AvailableBytesIn = n;
                    _codec.TotalBytesIn += p - _codec.NextIn;
                    _codec.NextIn = p;
                    writeAt = q;
                    System.Int32 RNTRNTRNT_599 = Flush(r);
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505344uL);
                    return RNTRNTRNT_599;
                  }
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505345uL);
                  n--;
                  b |= (_codec.InputBuffer[p++] & 0xff) << k;
                  k += 8;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505346uL);
                t = hufts[(tb[0] + (b & InternalInflateConstants.InflateMask[t])) * 3 + 1];
                c = hufts[(tb[0] + (b & InternalInflateConstants.InflateMask[t])) * 3 + 2];
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505347uL);
                if (c < 16) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505348uL);
                  b >>= t;
                  k -= t;
                  blens[index++] = c;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505349uL);
                  i = c == 18 ? 7 : c - 14;
                  j = c == 18 ? 11 : 3;
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505350uL);
                  while (k < (t + i)) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505351uL);
                    if (n != 0) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505352uL);
                      r = ZlibConstants.Z_OK;
                    } else {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505353uL);
                      bitb = b;
                      bitk = k;
                      _codec.AvailableBytesIn = n;
                      _codec.TotalBytesIn += p - _codec.NextIn;
                      _codec.NextIn = p;
                      writeAt = q;
                      System.Int32 RNTRNTRNT_600 = Flush(r);
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505354uL);
                      return RNTRNTRNT_600;
                    }
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505355uL);
                    n--;
                    b |= (_codec.InputBuffer[p++] & 0xff) << k;
                    k += 8;
                  }
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505356uL);
                  b >>= t;
                  k -= t;
                  j += (b & InternalInflateConstants.InflateMask[i]);
                  b >>= i;
                  k -= i;
                  i = index;
                  t = table;
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505357uL);
                  if (i + j > 258 + (t & 0x1f) + ((t >> 5) & 0x1f) || (c == 16 && i < 1)) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505358uL);
                    blens = null;
                    mode = InflateBlockMode.BAD;
                    _codec.Message = "invalid bit length repeat";
                    r = ZlibConstants.Z_DATA_ERROR;
                    bitb = b;
                    bitk = k;
                    _codec.AvailableBytesIn = n;
                    _codec.TotalBytesIn += p - _codec.NextIn;
                    _codec.NextIn = p;
                    writeAt = q;
                    System.Int32 RNTRNTRNT_601 = Flush(r);
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505359uL);
                    return RNTRNTRNT_601;
                  }
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505360uL);
                  c = (c == 16) ? blens[i - 1] : 0;
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505361uL);
                  do {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505362uL);
                    blens[i++] = c;
                  } while (--j != 0);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505363uL);
                  index = i;
                }
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505364uL);
              tb[0] = -1;
            }

            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505373uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505366uL);
              int[] bl = new int[] { 9 };
              int[] bd = new int[] { 6 };
              int[] tl = new int[1];
              int[] td = new int[1];
              t = table;
              t = inftree.inflate_trees_dynamic(257 + (t & 0x1f), 1 + ((t >> 5) & 0x1f), blens, bl, bd, tl, td, hufts, _codec);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505367uL);
              if (t != ZlibConstants.Z_OK) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505368uL);
                if (t == ZlibConstants.Z_DATA_ERROR) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505369uL);
                  blens = null;
                  mode = InflateBlockMode.BAD;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505370uL);
                r = t;
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_602 = Flush(r);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505371uL);
                return RNTRNTRNT_602;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505372uL);
              codes.Init(bl[0], bd[0], hufts, tl[0], hufts, td[0]);
            }

            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505376uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505374uL);
              mode = InflateBlockMode.CODES;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505375uL);
              goto case InflateBlockMode.CODES;
            }

          case InflateBlockMode.CODES:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505386uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505377uL);
              bitb = b;
              bitk = k;
              _codec.AvailableBytesIn = n;
              _codec.TotalBytesIn += p - _codec.NextIn;
              _codec.NextIn = p;
              writeAt = q;
              r = codes.Process(this, r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505378uL);
              if (r != ZlibConstants.Z_STREAM_END) {
                System.Int32 RNTRNTRNT_603 = Flush(r);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505379uL);
                return RNTRNTRNT_603;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505380uL);
              r = ZlibConstants.Z_OK;
              p = _codec.NextIn;
              n = _codec.AvailableBytesIn;
              b = bitb;
              k = bitk;
              q = writeAt;
              m = (int)(q < readAt ? readAt - q - 1 : end - q);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505381uL);
              if (last == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505382uL);
                mode = InflateBlockMode.TYPE;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505383uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505384uL);
              mode = InflateBlockMode.DRY;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505385uL);
              goto case InflateBlockMode.DRY;
            }

          case InflateBlockMode.DRY:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505393uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505387uL);
              writeAt = q;
              r = Flush(r);
              q = writeAt;
              m = (int)(q < readAt ? readAt - q - 1 : end - q);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505388uL);
              if (readAt != writeAt) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505389uL);
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_604 = Flush(r);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505390uL);
                return RNTRNTRNT_604;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505391uL);
              mode = InflateBlockMode.DONE;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505392uL);
              goto case InflateBlockMode.DONE;
            }

          case InflateBlockMode.DONE:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505396uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505394uL);
              r = ZlibConstants.Z_STREAM_END;
              bitb = b;
              bitk = k;
              _codec.AvailableBytesIn = n;
              _codec.TotalBytesIn += p - _codec.NextIn;
              _codec.NextIn = p;
              writeAt = q;
              System.Int32 RNTRNTRNT_605 = Flush(r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505395uL);
              return RNTRNTRNT_605;
            }

          case InflateBlockMode.BAD:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505399uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505397uL);
              r = ZlibConstants.Z_DATA_ERROR;
              bitb = b;
              bitk = k;
              _codec.AvailableBytesIn = n;
              _codec.TotalBytesIn += p - _codec.NextIn;
              _codec.NextIn = p;
              writeAt = q;
              System.Int32 RNTRNTRNT_606 = Flush(r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505398uL);
              return RNTRNTRNT_606;
            }

          default:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505402uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505400uL);
              r = ZlibConstants.Z_STREAM_ERROR;
              bitb = b;
              bitk = k;
              _codec.AvailableBytesIn = n;
              _codec.TotalBytesIn += p - _codec.NextIn;
              _codec.NextIn = p;
              writeAt = q;
              System.Int32 RNTRNTRNT_607 = Flush(r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505401uL);
              return RNTRNTRNT_607;
            }

        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505403uL);
    }
    internal void Free()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505404uL);
      Reset();
      window = null;
      hufts = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505405uL);
    }
    internal void SetDictionary(byte[] d, int start, int n)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505406uL);
      Array.Copy(d, start, window, 0, n);
      readAt = writeAt = n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505407uL);
    }
    internal int SyncPoint()
    {
      System.Int32 RNTRNTRNT_608 = mode == InflateBlockMode.LENS ? 1 : 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505408uL);
      return RNTRNTRNT_608;
    }
    internal int Flush(int r)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505409uL);
      int nBytes;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505410uL);
      for (int pass = 0; pass < 2; pass++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505411uL);
        if (pass == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505412uL);
          nBytes = (int)((readAt <= writeAt ? writeAt : end) - readAt);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505413uL);
          nBytes = writeAt - readAt;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505414uL);
        if (nBytes == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505415uL);
          if (r == ZlibConstants.Z_BUF_ERROR) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505416uL);
            r = ZlibConstants.Z_OK;
          }
          System.Int32 RNTRNTRNT_609 = r;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505417uL);
          return RNTRNTRNT_609;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505418uL);
        if (nBytes > _codec.AvailableBytesOut) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505419uL);
          nBytes = _codec.AvailableBytesOut;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505420uL);
        if (nBytes != 0 && r == ZlibConstants.Z_BUF_ERROR) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505421uL);
          r = ZlibConstants.Z_OK;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505422uL);
        _codec.AvailableBytesOut -= nBytes;
        _codec.TotalBytesOut += nBytes;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505423uL);
        if (checkfn != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505424uL);
          _codec._Adler32 = check = Adler.Adler32(check, window, readAt, nBytes);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505425uL);
        Array.Copy(window, readAt, _codec.OutputBuffer, _codec.NextOut, nBytes);
        _codec.NextOut += nBytes;
        readAt += nBytes;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505426uL);
        if (readAt == end && pass == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505427uL);
          readAt = 0;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505428uL);
          if (writeAt == end) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505429uL);
            writeAt = 0;
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505430uL);
          pass++;
        }
      }
      System.Int32 RNTRNTRNT_610 = r;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505431uL);
      return RNTRNTRNT_610;
    }
  }
  static internal class InternalInflateConstants
  {
    static internal readonly int[] InflateMask = new int[] {
      0x0,
      0x1,
      0x3,
      0x7,
      0xf,
      0x1f,
      0x3f,
      0x7f,
      0xff,
      0x1ff,
      0x3ff,
      0x7ff,
      0xfff,
      0x1fff,
      0x3fff,
      0x7fff,
      0xffff
    };
  }
  sealed class InflateCodes
  {
    private const int START = 0;
    private const int LEN = 1;
    private const int LENEXT = 2;
    private const int DIST = 3;
    private const int DISTEXT = 4;
    private const int COPY = 5;
    private const int LIT = 6;
    private const int WASH = 7;
    private const int END = 8;
    private const int BADCODE = 9;
    internal int mode;
    internal int len;
    internal int[] tree;
    internal int tree_index = 0;
    internal int need;
    internal int lit;
    internal int bitsToGet;
    internal int dist;
    internal byte lbits;
    internal byte dbits;
    internal int[] ltree;
    internal int ltree_index;
    internal int[] dtree;
    internal int dtree_index;
    internal InflateCodes()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505432uL);
    }
    internal void Init(int bl, int bd, int[] tl, int tl_index, int[] td, int td_index)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505433uL);
      mode = START;
      lbits = (byte)bl;
      dbits = (byte)bd;
      ltree = tl;
      ltree_index = tl_index;
      dtree = td;
      dtree_index = td_index;
      tree = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505434uL);
    }
    internal int Process(InflateBlocks blocks, int r)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505435uL);
      int j;
      int tindex;
      int e;
      int b = 0;
      int k = 0;
      int p = 0;
      int n;
      int q;
      int m;
      int f;
      ZlibCodec z = blocks._codec;
      p = z.NextIn;
      n = z.AvailableBytesIn;
      b = blocks.bitb;
      k = blocks.bitk;
      q = blocks.writeAt;
      m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505436uL);
      while (true) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505437uL);
        switch (mode) {
          case START:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505445uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505438uL);
              if (m >= 258 && n >= 10) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505439uL);
                blocks.bitb = b;
                blocks.bitk = k;
                z.AvailableBytesIn = n;
                z.TotalBytesIn += p - z.NextIn;
                z.NextIn = p;
                blocks.writeAt = q;
                r = InflateFast(lbits, dbits, ltree, ltree_index, dtree, dtree_index, blocks, z);
                p = z.NextIn;
                n = z.AvailableBytesIn;
                b = blocks.bitb;
                k = blocks.bitk;
                q = blocks.writeAt;
                m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505440uL);
                if (r != ZlibConstants.Z_OK) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505441uL);
                  mode = (r == ZlibConstants.Z_STREAM_END) ? WASH : BADCODE;
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505442uL);
                  break;
                }
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505443uL);
              need = lbits;
              tree = ltree;
              tree_index = ltree_index;
              mode = LEN;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505444uL);
              goto case LEN;
            }

          case LEN:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505468uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505446uL);
              j = need;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505447uL);
              while (k < j) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505448uL);
                if (n != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505449uL);
                  r = ZlibConstants.Z_OK;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505450uL);
                  blocks.bitb = b;
                  blocks.bitk = k;
                  z.AvailableBytesIn = n;
                  z.TotalBytesIn += p - z.NextIn;
                  z.NextIn = p;
                  blocks.writeAt = q;
                  System.Int32 RNTRNTRNT_611 = blocks.Flush(r);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505451uL);
                  return RNTRNTRNT_611;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505452uL);
                n--;
                b |= (z.InputBuffer[p++] & 0xff) << k;
                k += 8;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505453uL);
              tindex = (tree_index + (b & InternalInflateConstants.InflateMask[j])) * 3;
              b >>= (tree[tindex + 1]);
              k -= (tree[tindex + 1]);
              e = tree[tindex];
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505454uL);
              if (e == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505455uL);
                lit = tree[tindex + 2];
                mode = LIT;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505456uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505457uL);
              if ((e & 16) != 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505458uL);
                bitsToGet = e & 15;
                len = tree[tindex + 2];
                mode = LENEXT;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505459uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505460uL);
              if ((e & 64) == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505461uL);
                need = e;
                tree_index = tindex / 3 + tree[tindex + 2];
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505462uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505463uL);
              if ((e & 32) != 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505464uL);
                mode = WASH;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505465uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505466uL);
              mode = BADCODE;
              z.Message = "invalid literal/length code";
              r = ZlibConstants.Z_DATA_ERROR;
              blocks.bitb = b;
              blocks.bitk = k;
              z.AvailableBytesIn = n;
              z.TotalBytesIn += p - z.NextIn;
              z.NextIn = p;
              blocks.writeAt = q;
              System.Int32 RNTRNTRNT_612 = blocks.Flush(r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505467uL);
              return RNTRNTRNT_612;
            }

          case LENEXT:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505478uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505469uL);
              j = bitsToGet;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505470uL);
              while (k < j) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505471uL);
                if (n != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505472uL);
                  r = ZlibConstants.Z_OK;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505473uL);
                  blocks.bitb = b;
                  blocks.bitk = k;
                  z.AvailableBytesIn = n;
                  z.TotalBytesIn += p - z.NextIn;
                  z.NextIn = p;
                  blocks.writeAt = q;
                  System.Int32 RNTRNTRNT_613 = blocks.Flush(r);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505474uL);
                  return RNTRNTRNT_613;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505475uL);
                n--;
                b |= (z.InputBuffer[p++] & 0xff) << k;
                k += 8;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505476uL);
              len += (b & InternalInflateConstants.InflateMask[j]);
              b >>= j;
              k -= j;
              need = dbits;
              tree = dtree;
              tree_index = dtree_index;
              mode = DIST;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505477uL);
              goto case DIST;
            }

          case DIST:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505495uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505479uL);
              j = need;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505480uL);
              while (k < j) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505481uL);
                if (n != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505482uL);
                  r = ZlibConstants.Z_OK;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505483uL);
                  blocks.bitb = b;
                  blocks.bitk = k;
                  z.AvailableBytesIn = n;
                  z.TotalBytesIn += p - z.NextIn;
                  z.NextIn = p;
                  blocks.writeAt = q;
                  System.Int32 RNTRNTRNT_614 = blocks.Flush(r);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505484uL);
                  return RNTRNTRNT_614;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505485uL);
                n--;
                b |= (z.InputBuffer[p++] & 0xff) << k;
                k += 8;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505486uL);
              tindex = (tree_index + (b & InternalInflateConstants.InflateMask[j])) * 3;
              b >>= tree[tindex + 1];
              k -= tree[tindex + 1];
              e = (tree[tindex]);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505487uL);
              if ((e & 0x10) != 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505488uL);
                bitsToGet = e & 15;
                dist = tree[tindex + 2];
                mode = DISTEXT;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505489uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505490uL);
              if ((e & 64) == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505491uL);
                need = e;
                tree_index = tindex / 3 + tree[tindex + 2];
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505492uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505493uL);
              mode = BADCODE;
              z.Message = "invalid distance code";
              r = ZlibConstants.Z_DATA_ERROR;
              blocks.bitb = b;
              blocks.bitk = k;
              z.AvailableBytesIn = n;
              z.TotalBytesIn += p - z.NextIn;
              z.NextIn = p;
              blocks.writeAt = q;
              System.Int32 RNTRNTRNT_615 = blocks.Flush(r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505494uL);
              return RNTRNTRNT_615;
            }

          case DISTEXT:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505505uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505496uL);
              j = bitsToGet;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505497uL);
              while (k < j) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505498uL);
                if (n != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505499uL);
                  r = ZlibConstants.Z_OK;
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505500uL);
                  blocks.bitb = b;
                  blocks.bitk = k;
                  z.AvailableBytesIn = n;
                  z.TotalBytesIn += p - z.NextIn;
                  z.NextIn = p;
                  blocks.writeAt = q;
                  System.Int32 RNTRNTRNT_616 = blocks.Flush(r);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505501uL);
                  return RNTRNTRNT_616;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505502uL);
                n--;
                b |= (z.InputBuffer[p++] & 0xff) << k;
                k += 8;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505503uL);
              dist += (b & InternalInflateConstants.InflateMask[j]);
              b >>= j;
              k -= j;
              mode = COPY;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505504uL);
              goto case COPY;
            }

          case COPY:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505526uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505506uL);
              f = q - dist;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505507uL);
              while (f < 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505508uL);
                f += blocks.end;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505509uL);
              while (len != 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505510uL);
                if (m == 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505511uL);
                  if (q == blocks.end && blocks.readAt != 0) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505512uL);
                    q = 0;
                    m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                  }
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505513uL);
                  if (m == 0) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505514uL);
                    blocks.writeAt = q;
                    r = blocks.Flush(r);
                    q = blocks.writeAt;
                    m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505515uL);
                    if (q == blocks.end && blocks.readAt != 0) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505516uL);
                      q = 0;
                      m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                    }
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505517uL);
                    if (m == 0) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505518uL);
                      blocks.bitb = b;
                      blocks.bitk = k;
                      z.AvailableBytesIn = n;
                      z.TotalBytesIn += p - z.NextIn;
                      z.NextIn = p;
                      blocks.writeAt = q;
                      System.Int32 RNTRNTRNT_617 = blocks.Flush(r);
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505519uL);
                      return RNTRNTRNT_617;
                    }
                  }
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505520uL);
                blocks.window[q++] = blocks.window[f++];
                m--;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505521uL);
                if (f == blocks.end) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505522uL);
                  f = 0;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505523uL);
                len--;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505524uL);
              mode = START;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505525uL);
              break;
            }

          case LIT:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505539uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505527uL);
              if (m == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505528uL);
                if (q == blocks.end && blocks.readAt != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505529uL);
                  q = 0;
                  m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505530uL);
                if (m == 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505531uL);
                  blocks.writeAt = q;
                  r = blocks.Flush(r);
                  q = blocks.writeAt;
                  m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505532uL);
                  if (q == blocks.end && blocks.readAt != 0) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505533uL);
                    q = 0;
                    m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                  }
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505534uL);
                  if (m == 0) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505535uL);
                    blocks.bitb = b;
                    blocks.bitk = k;
                    z.AvailableBytesIn = n;
                    z.TotalBytesIn += p - z.NextIn;
                    z.NextIn = p;
                    blocks.writeAt = q;
                    System.Int32 RNTRNTRNT_618 = blocks.Flush(r);
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505536uL);
                    return RNTRNTRNT_618;
                  }
                }
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505537uL);
              r = ZlibConstants.Z_OK;
              blocks.window[q++] = (byte)lit;
              m--;
              mode = START;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505538uL);
              break;
            }

          case WASH:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505548uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505540uL);
              if (k > 7) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505541uL);
                k -= 8;
                n++;
                p--;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505542uL);
              blocks.writeAt = q;
              r = blocks.Flush(r);
              q = blocks.writeAt;
              m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505543uL);
              if (blocks.readAt != blocks.writeAt) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505544uL);
                blocks.bitb = b;
                blocks.bitk = k;
                z.AvailableBytesIn = n;
                z.TotalBytesIn += p - z.NextIn;
                z.NextIn = p;
                blocks.writeAt = q;
                System.Int32 RNTRNTRNT_619 = blocks.Flush(r);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505545uL);
                return RNTRNTRNT_619;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505546uL);
              mode = END;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505547uL);
              goto case END;
            }

          case END:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505551uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505549uL);
              r = ZlibConstants.Z_STREAM_END;
              blocks.bitb = b;
              blocks.bitk = k;
              z.AvailableBytesIn = n;
              z.TotalBytesIn += p - z.NextIn;
              z.NextIn = p;
              blocks.writeAt = q;
              System.Int32 RNTRNTRNT_620 = blocks.Flush(r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505550uL);
              return RNTRNTRNT_620;
            }

          case BADCODE:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505554uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505552uL);
              r = ZlibConstants.Z_DATA_ERROR;
              blocks.bitb = b;
              blocks.bitk = k;
              z.AvailableBytesIn = n;
              z.TotalBytesIn += p - z.NextIn;
              z.NextIn = p;
              blocks.writeAt = q;
              System.Int32 RNTRNTRNT_621 = blocks.Flush(r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505553uL);
              return RNTRNTRNT_621;
            }

          default:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505557uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505555uL);
              r = ZlibConstants.Z_STREAM_ERROR;
              blocks.bitb = b;
              blocks.bitk = k;
              z.AvailableBytesIn = n;
              z.TotalBytesIn += p - z.NextIn;
              z.NextIn = p;
              blocks.writeAt = q;
              System.Int32 RNTRNTRNT_622 = blocks.Flush(r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505556uL);
              return RNTRNTRNT_622;
            }

        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505558uL);
    }
    internal int InflateFast(int bl, int bd, int[] tl, int tl_index, int[] td, int td_index, InflateBlocks s, ZlibCodec z)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505559uL);
      int t;
      int[] tp;
      int tp_index;
      int e;
      int b;
      int k;
      int p;
      int n;
      int q;
      int m;
      int ml;
      int md;
      int c;
      int d;
      int r;
      int tp_index_t_3;
      p = z.NextIn;
      n = z.AvailableBytesIn;
      b = s.bitb;
      k = s.bitk;
      q = s.writeAt;
      m = q < s.readAt ? s.readAt - q - 1 : s.end - q;
      ml = InternalInflateConstants.InflateMask[bl];
      md = InternalInflateConstants.InflateMask[bd];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505560uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505561uL);
        while (k < (20)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505562uL);
          n--;
          b |= (z.InputBuffer[p++] & 0xff) << k;
          k += 8;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505563uL);
        t = b & ml;
        tp = tl;
        tp_index = tl_index;
        tp_index_t_3 = (tp_index + t) * 3;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505564uL);
        if ((e = tp[tp_index_t_3]) == 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505565uL);
          b >>= (tp[tp_index_t_3 + 1]);
          k -= (tp[tp_index_t_3 + 1]);
          s.window[q++] = (byte)tp[tp_index_t_3 + 2];
          m--;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505566uL);
          continue;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505567uL);
        do {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505568uL);
          b >>= (tp[tp_index_t_3 + 1]);
          k -= (tp[tp_index_t_3 + 1]);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505569uL);
          if ((e & 16) != 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505570uL);
            e &= 15;
            c = tp[tp_index_t_3 + 2] + ((int)b & InternalInflateConstants.InflateMask[e]);
            b >>= e;
            k -= e;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505571uL);
            while (k < 15) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505572uL);
              n--;
              b |= (z.InputBuffer[p++] & 0xff) << k;
              k += 8;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505573uL);
            t = b & md;
            tp = td;
            tp_index = td_index;
            tp_index_t_3 = (tp_index + t) * 3;
            e = tp[tp_index_t_3];
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505574uL);
            do {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505575uL);
              b >>= (tp[tp_index_t_3 + 1]);
              k -= (tp[tp_index_t_3 + 1]);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505576uL);
              if ((e & 16) != 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505577uL);
                e &= 15;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505578uL);
                while (k < e) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505579uL);
                  n--;
                  b |= (z.InputBuffer[p++] & 0xff) << k;
                  k += 8;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505580uL);
                d = tp[tp_index_t_3 + 2] + (b & InternalInflateConstants.InflateMask[e]);
                b >>= e;
                k -= e;
                m -= c;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505581uL);
                if (q >= d) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505582uL);
                  r = q - d;
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505583uL);
                  if (q - r > 0 && 2 > (q - r)) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505584uL);
                    s.window[q++] = s.window[r++];
                    s.window[q++] = s.window[r++];
                    c -= 2;
                  } else {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505585uL);
                    Array.Copy(s.window, r, s.window, q, 2);
                    q += 2;
                    r += 2;
                    c -= 2;
                  }
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505586uL);
                  r = q - d;
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505587uL);
                  do {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505588uL);
                    r += s.end;
                  } while (r < 0);
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505589uL);
                  e = s.end - r;
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505590uL);
                  if (c > e) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505591uL);
                    c -= e;
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505592uL);
                    if (q - r > 0 && e > (q - r)) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505593uL);
                      do {
                        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505594uL);
                        s.window[q++] = s.window[r++];
                      } while (--e != 0);
                    } else {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505595uL);
                      Array.Copy(s.window, r, s.window, q, e);
                      q += e;
                      r += e;
                      e = 0;
                    }
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505596uL);
                    r = 0;
                  }
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505597uL);
                if (q - r > 0 && c > (q - r)) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505598uL);
                  do {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505599uL);
                    s.window[q++] = s.window[r++];
                  } while (--c != 0);
                } else {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505600uL);
                  Array.Copy(s.window, r, s.window, q, c);
                  q += c;
                  r += c;
                  c = 0;
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505601uL);
                break;
              } else if ((e & 64) == 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505604uL);
                t += tp[tp_index_t_3 + 2];
                t += (b & InternalInflateConstants.InflateMask[e]);
                tp_index_t_3 = (tp_index + t) * 3;
                e = tp[tp_index_t_3];
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505602uL);
                z.Message = "invalid distance code";
                c = z.AvailableBytesIn - n;
                c = (k >> 3) < c ? k >> 3 : c;
                n += c;
                p -= c;
                k -= (c << 3);
                s.bitb = b;
                s.bitk = k;
                z.AvailableBytesIn = n;
                z.TotalBytesIn += p - z.NextIn;
                z.NextIn = p;
                s.writeAt = q;
                System.Int32 RNTRNTRNT_623 = ZlibConstants.Z_DATA_ERROR;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505603uL);
                return RNTRNTRNT_623;
              }
            } while (true);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505605uL);
            break;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505606uL);
          if ((e & 64) == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505607uL);
            t += tp[tp_index_t_3 + 2];
            t += (b & InternalInflateConstants.InflateMask[e]);
            tp_index_t_3 = (tp_index + t) * 3;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505608uL);
            if ((e = tp[tp_index_t_3]) == 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505609uL);
              b >>= (tp[tp_index_t_3 + 1]);
              k -= (tp[tp_index_t_3 + 1]);
              s.window[q++] = (byte)tp[tp_index_t_3 + 2];
              m--;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505610uL);
              break;
            }
          } else if ((e & 32) != 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505613uL);
            c = z.AvailableBytesIn - n;
            c = (k >> 3) < c ? k >> 3 : c;
            n += c;
            p -= c;
            k -= (c << 3);
            s.bitb = b;
            s.bitk = k;
            z.AvailableBytesIn = n;
            z.TotalBytesIn += p - z.NextIn;
            z.NextIn = p;
            s.writeAt = q;
            System.Int32 RNTRNTRNT_625 = ZlibConstants.Z_STREAM_END;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505614uL);
            return RNTRNTRNT_625;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505611uL);
            z.Message = "invalid literal/length code";
            c = z.AvailableBytesIn - n;
            c = (k >> 3) < c ? k >> 3 : c;
            n += c;
            p -= c;
            k -= (c << 3);
            s.bitb = b;
            s.bitk = k;
            z.AvailableBytesIn = n;
            z.TotalBytesIn += p - z.NextIn;
            z.NextIn = p;
            s.writeAt = q;
            System.Int32 RNTRNTRNT_624 = ZlibConstants.Z_DATA_ERROR;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505612uL);
            return RNTRNTRNT_624;
          }
        } while (true);
      } while (m >= 258 && n >= 10);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505615uL);
      c = z.AvailableBytesIn - n;
      c = (k >> 3) < c ? k >> 3 : c;
      n += c;
      p -= c;
      k -= (c << 3);
      s.bitb = b;
      s.bitk = k;
      z.AvailableBytesIn = n;
      z.TotalBytesIn += p - z.NextIn;
      z.NextIn = p;
      s.writeAt = q;
      System.Int32 RNTRNTRNT_626 = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505616uL);
      return RNTRNTRNT_626;
    }
  }
  internal sealed class InflateManager
  {
    private const int PRESET_DICT = 0x20;
    private const int Z_DEFLATED = 8;
    private enum InflateManagerMode
    {
      METHOD = 0,
      FLAG = 1,
      DICT4 = 2,
      DICT3 = 3,
      DICT2 = 4,
      DICT1 = 5,
      DICT0 = 6,
      BLOCKS = 7,
      CHECK4 = 8,
      CHECK3 = 9,
      CHECK2 = 10,
      CHECK1 = 11,
      DONE = 12,
      BAD = 13
    }
    private InflateManagerMode mode;
    internal ZlibCodec _codec;
    internal int method;
    internal uint computedCheck;
    internal uint expectedCheck;
    internal int marker;
    private bool _handleRfc1950HeaderBytes = true;
    internal bool HandleRfc1950HeaderBytes {
      get {
        System.Boolean RNTRNTRNT_627 = _handleRfc1950HeaderBytes;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505617uL);
        return RNTRNTRNT_627;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505618uL);
        _handleRfc1950HeaderBytes = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505619uL);
      }
    }
    internal int wbits;
    internal InflateBlocks blocks;
    public InflateManager()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505620uL);
    }
    public InflateManager(bool expectRfc1950HeaderBytes)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505621uL);
      _handleRfc1950HeaderBytes = expectRfc1950HeaderBytes;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505622uL);
    }
    internal int Reset()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505623uL);
      _codec.TotalBytesIn = _codec.TotalBytesOut = 0;
      _codec.Message = null;
      mode = HandleRfc1950HeaderBytes ? InflateManagerMode.METHOD : InflateManagerMode.BLOCKS;
      blocks.Reset();
      System.Int32 RNTRNTRNT_628 = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505624uL);
      return RNTRNTRNT_628;
    }
    internal int End()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505625uL);
      if (blocks != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505626uL);
        blocks.Free();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505627uL);
      blocks = null;
      System.Int32 RNTRNTRNT_629 = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505628uL);
      return RNTRNTRNT_629;
    }
    internal int Initialize(ZlibCodec codec, int w)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505629uL);
      _codec = codec;
      _codec.Message = null;
      blocks = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505630uL);
      if (w < 8 || w > 15) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505631uL);
        End();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505632uL);
        throw new ZlibException("Bad window size.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505633uL);
      wbits = w;
      blocks = new InflateBlocks(codec, HandleRfc1950HeaderBytes ? this : null, 1 << w);
      Reset();
      System.Int32 RNTRNTRNT_630 = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505634uL);
      return RNTRNTRNT_630;
    }
    internal int Inflate(FlushType flush)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505635uL);
      int b;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505636uL);
      if (_codec.InputBuffer == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505637uL);
        throw new ZlibException("InputBuffer is null. ");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505638uL);
      int f = ZlibConstants.Z_OK;
      int r = ZlibConstants.Z_BUF_ERROR;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505639uL);
      while (true) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505640uL);
        switch (mode) {
          case InflateManagerMode.METHOD:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505652uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505641uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_631 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505642uL);
                return RNTRNTRNT_631;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505643uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505644uL);
              if (((method = _codec.InputBuffer[_codec.NextIn++]) & 0xf) != Z_DEFLATED) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505645uL);
                mode = InflateManagerMode.BAD;
                _codec.Message = String.Format("unknown compression method (0x{0:X2})", method);
                marker = 5;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505646uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505647uL);
              if ((method >> 4) + 8 > wbits) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505648uL);
                mode = InflateManagerMode.BAD;
                _codec.Message = String.Format("invalid window size ({0})", (method >> 4) + 8);
                marker = 5;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505649uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505650uL);
              mode = InflateManagerMode.FLAG;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505651uL);
              break;
            }

          case InflateManagerMode.FLAG:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505661uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505653uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_632 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505654uL);
                return RNTRNTRNT_632;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505655uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              b = (_codec.InputBuffer[_codec.NextIn++]) & 0xff;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505656uL);
              if ((((method << 8) + b) % 31) != 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505657uL);
                mode = InflateManagerMode.BAD;
                _codec.Message = "incorrect header check";
                marker = 5;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505658uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505659uL);
              mode = ((b & PRESET_DICT) == 0) ? InflateManagerMode.BLOCKS : InflateManagerMode.DICT4;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505660uL);
              break;
            }

          case InflateManagerMode.DICT4:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505666uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505662uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_633 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505663uL);
                return RNTRNTRNT_633;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505664uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              expectedCheck = (uint)((_codec.InputBuffer[_codec.NextIn++] << 24) & 0xff000000u);
              mode = InflateManagerMode.DICT3;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505665uL);
              break;
            }

          case InflateManagerMode.DICT3:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505671uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505667uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_634 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505668uL);
                return RNTRNTRNT_634;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505669uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              expectedCheck += (uint)((_codec.InputBuffer[_codec.NextIn++] << 16) & 0xff0000);
              mode = InflateManagerMode.DICT2;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505670uL);
              break;
            }

          case InflateManagerMode.DICT2:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505676uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505672uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_635 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505673uL);
                return RNTRNTRNT_635;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505674uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              expectedCheck += (uint)((_codec.InputBuffer[_codec.NextIn++] << 8) & 0xff00);
              mode = InflateManagerMode.DICT1;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505675uL);
              break;
            }

          case InflateManagerMode.DICT1:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505681uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505677uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_636 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505678uL);
                return RNTRNTRNT_636;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505679uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              expectedCheck += (uint)(_codec.InputBuffer[_codec.NextIn++] & 0xff);
              _codec._Adler32 = expectedCheck;
              mode = InflateManagerMode.DICT0;
              System.Int32 RNTRNTRNT_637 = ZlibConstants.Z_NEED_DICT;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505680uL);
              return RNTRNTRNT_637;
            }

          case InflateManagerMode.DICT0:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505684uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505682uL);
              mode = InflateManagerMode.BAD;
              _codec.Message = "need dictionary";
              marker = 0;
              System.Int32 RNTRNTRNT_638 = ZlibConstants.Z_STREAM_ERROR;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505683uL);
              return RNTRNTRNT_638;
            }

          case InflateManagerMode.BLOCKS:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505699uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505685uL);
              r = blocks.Process(r);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505686uL);
              if (r == ZlibConstants.Z_DATA_ERROR) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505687uL);
                mode = InflateManagerMode.BAD;
                marker = 0;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505688uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505689uL);
              if (r == ZlibConstants.Z_OK) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505690uL);
                r = f;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505691uL);
              if (r != ZlibConstants.Z_STREAM_END) {
                System.Int32 RNTRNTRNT_639 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505692uL);
                return RNTRNTRNT_639;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505693uL);
              r = f;
              computedCheck = blocks.Reset();
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505694uL);
              if (!HandleRfc1950HeaderBytes) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505695uL);
                mode = InflateManagerMode.DONE;
                System.Int32 RNTRNTRNT_640 = ZlibConstants.Z_STREAM_END;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505696uL);
                return RNTRNTRNT_640;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505697uL);
              mode = InflateManagerMode.CHECK4;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505698uL);
              break;
            }

          case InflateManagerMode.CHECK4:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505704uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505700uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_641 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505701uL);
                return RNTRNTRNT_641;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505702uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              expectedCheck = (uint)((_codec.InputBuffer[_codec.NextIn++] << 24) & 0xff000000u);
              mode = InflateManagerMode.CHECK3;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505703uL);
              break;
            }

          case InflateManagerMode.CHECK3:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505709uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505705uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_642 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505706uL);
                return RNTRNTRNT_642;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505707uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              expectedCheck += (uint)((_codec.InputBuffer[_codec.NextIn++] << 16) & 0xff0000);
              mode = InflateManagerMode.CHECK2;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505708uL);
              break;
            }

          case InflateManagerMode.CHECK2:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505714uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505710uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_643 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505711uL);
                return RNTRNTRNT_643;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505712uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              expectedCheck += (uint)((_codec.InputBuffer[_codec.NextIn++] << 8) & 0xff00);
              mode = InflateManagerMode.CHECK1;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505713uL);
              break;
            }

          case InflateManagerMode.CHECK1:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505723uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505715uL);
              if (_codec.AvailableBytesIn == 0) {
                System.Int32 RNTRNTRNT_644 = r;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505716uL);
                return RNTRNTRNT_644;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505717uL);
              r = f;
              _codec.AvailableBytesIn--;
              _codec.TotalBytesIn++;
              expectedCheck += (uint)(_codec.InputBuffer[_codec.NextIn++] & 0xff);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505718uL);
              if (computedCheck != expectedCheck) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505719uL);
                mode = InflateManagerMode.BAD;
                _codec.Message = "incorrect data check";
                marker = 5;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505720uL);
                break;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505721uL);
              mode = InflateManagerMode.DONE;
              System.Int32 RNTRNTRNT_645 = ZlibConstants.Z_STREAM_END;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505722uL);
              return RNTRNTRNT_645;
            }

          case InflateManagerMode.DONE:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505725uL);
            
            {
              System.Int32 RNTRNTRNT_646 = ZlibConstants.Z_STREAM_END;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505724uL);
              return RNTRNTRNT_646;
            }

          case InflateManagerMode.BAD:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505727uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505726uL);
              throw new ZlibException(String.Format("Bad state ({0})", _codec.Message));
            }

          default:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505729uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505728uL);
              throw new ZlibException("Stream error.");
            }

        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505730uL);
    }
    internal int SetDictionary(byte[] dictionary)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505731uL);
      int index = 0;
      int length = dictionary.Length;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505732uL);
      if (mode != InflateManagerMode.DICT0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505733uL);
        throw new ZlibException("Stream error.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505734uL);
      if (Adler.Adler32(1, dictionary, 0, dictionary.Length) != _codec._Adler32) {
        System.Int32 RNTRNTRNT_647 = ZlibConstants.Z_DATA_ERROR;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505735uL);
        return RNTRNTRNT_647;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505736uL);
      _codec._Adler32 = Adler.Adler32(0, null, 0, 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505737uL);
      if (length >= (1 << wbits)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505738uL);
        length = (1 << wbits) - 1;
        index = dictionary.Length - length;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505739uL);
      blocks.SetDictionary(dictionary, index, length);
      mode = InflateManagerMode.BLOCKS;
      System.Int32 RNTRNTRNT_648 = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505740uL);
      return RNTRNTRNT_648;
    }
    private static readonly byte[] mark = new byte[] {
      0,
      0,
      0xff,
      0xff
    };
    internal int Sync()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505741uL);
      int n;
      int p;
      int m;
      long r, w;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505742uL);
      if (mode != InflateManagerMode.BAD) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505743uL);
        mode = InflateManagerMode.BAD;
        marker = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505744uL);
      if ((n = _codec.AvailableBytesIn) == 0) {
        System.Int32 RNTRNTRNT_649 = ZlibConstants.Z_BUF_ERROR;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505745uL);
        return RNTRNTRNT_649;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505746uL);
      p = _codec.NextIn;
      m = marker;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505747uL);
      while (n != 0 && m < 4) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505748uL);
        if (_codec.InputBuffer[p] == mark[m]) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505749uL);
          m++;
        } else if (_codec.InputBuffer[p] != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505751uL);
          m = 0;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505750uL);
          m = 4 - m;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505752uL);
        p++;
        n--;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505753uL);
      _codec.TotalBytesIn += p - _codec.NextIn;
      _codec.NextIn = p;
      _codec.AvailableBytesIn = n;
      marker = m;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505754uL);
      if (m != 4) {
        System.Int32 RNTRNTRNT_650 = ZlibConstants.Z_DATA_ERROR;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505755uL);
        return RNTRNTRNT_650;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505756uL);
      r = _codec.TotalBytesIn;
      w = _codec.TotalBytesOut;
      Reset();
      _codec.TotalBytesIn = r;
      _codec.TotalBytesOut = w;
      mode = InflateManagerMode.BLOCKS;
      System.Int32 RNTRNTRNT_651 = ZlibConstants.Z_OK;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505757uL);
      return RNTRNTRNT_651;
    }
    internal int SyncPoint(ZlibCodec z)
    {
      System.Int32 RNTRNTRNT_652 = blocks.SyncPoint();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(505758uL);
      return RNTRNTRNT_652;
    }
  }
}
