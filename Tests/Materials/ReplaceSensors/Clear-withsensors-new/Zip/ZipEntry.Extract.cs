using System;
using System.IO;
namespace Ionic.Zip
{
  public partial class ZipEntry
  {
    public void Extract()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501533uL);
      InternalExtract(".", null, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501534uL);
    }
    public void Extract(ExtractExistingFileAction extractExistingFile)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501535uL);
      ExtractExistingFile = extractExistingFile;
      InternalExtract(".", null, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501536uL);
    }
    public void Extract(Stream stream)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501537uL);
      InternalExtract(null, stream, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501538uL);
    }
    public void Extract(string baseDirectory)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501539uL);
      InternalExtract(baseDirectory, null, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501540uL);
    }
    public void Extract(string baseDirectory, ExtractExistingFileAction extractExistingFile)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501541uL);
      ExtractExistingFile = extractExistingFile;
      InternalExtract(baseDirectory, null, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501542uL);
    }
    public void ExtractWithPassword(string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501543uL);
      InternalExtract(".", null, password);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501544uL);
    }
    public void ExtractWithPassword(string baseDirectory, string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501545uL);
      InternalExtract(baseDirectory, null, password);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501546uL);
    }
    public void ExtractWithPassword(ExtractExistingFileAction extractExistingFile, string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501547uL);
      ExtractExistingFile = extractExistingFile;
      InternalExtract(".", null, password);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501548uL);
    }
    public void ExtractWithPassword(string baseDirectory, ExtractExistingFileAction extractExistingFile, string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501549uL);
      ExtractExistingFile = extractExistingFile;
      InternalExtract(baseDirectory, null, password);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501550uL);
    }
    public void ExtractWithPassword(Stream stream, string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501551uL);
      InternalExtract(null, stream, password);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501552uL);
    }
    public Ionic.Crc.CrcCalculatorStream OpenReader()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501553uL);
      if (_container.ZipFile == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501554uL);
        throw new InvalidOperationException("Use OpenReader() only with ZipFile.");
      }
      Ionic.Crc.CrcCalculatorStream RNTRNTRNT_279 = InternalOpenReader(this._Password ?? this._container.Password);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501555uL);
      return RNTRNTRNT_279;
    }
    public Ionic.Crc.CrcCalculatorStream OpenReader(string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501556uL);
      if (_container.ZipFile == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501557uL);
        throw new InvalidOperationException("Use OpenReader() only with ZipFile.");
      }
      Ionic.Crc.CrcCalculatorStream RNTRNTRNT_280 = InternalOpenReader(password);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501558uL);
      return RNTRNTRNT_280;
    }
    internal Ionic.Crc.CrcCalculatorStream InternalOpenReader(string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501559uL);
      ValidateCompression();
      ValidateEncryption();
      SetupCryptoForExtract(password);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501560uL);
      if (this._Source != ZipEntrySource.ZipFile) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501561uL);
        throw new BadStateException("You must call ZipFile.Save before calling OpenReader");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501562uL);
      Int64 LeftToRead = (_CompressionMethod_FromZipFile == (short)CompressionMethod.None) ? this._CompressedFileDataSize : this.UncompressedSize;
      Stream input = this.ArchiveStream;
      this.ArchiveStream.Seek(this.FileDataPosition, SeekOrigin.Begin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      _inputDecryptorStream = GetExtractDecryptor(input);
      Stream input3 = GetExtractDecompressor(_inputDecryptorStream);
      Ionic.Crc.CrcCalculatorStream RNTRNTRNT_281 = new Ionic.Crc.CrcCalculatorStream(input3, LeftToRead);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501563uL);
      return RNTRNTRNT_281;
    }
    private void OnExtractProgress(Int64 bytesWritten, Int64 totalBytesToWrite)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501564uL);
      if (_container.ZipFile != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501565uL);
        _ioOperationCanceled = _container.ZipFile.OnExtractBlock(this, bytesWritten, totalBytesToWrite);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501566uL);
    }
    private void OnBeforeExtract(string path)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501567uL);
      if (_container.ZipFile != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501568uL);
        if (!_container.ZipFile._inExtractAll) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501569uL);
          _ioOperationCanceled = _container.ZipFile.OnSingleEntryExtract(this, path, true);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501570uL);
    }
    private void OnAfterExtract(string path)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501571uL);
      if (_container.ZipFile != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501572uL);
        if (!_container.ZipFile._inExtractAll) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501573uL);
          _container.ZipFile.OnSingleEntryExtract(this, path, false);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501574uL);
    }
    private void OnExtractExisting(string path)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501575uL);
      if (_container.ZipFile != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501576uL);
        _ioOperationCanceled = _container.ZipFile.OnExtractExisting(this, path);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501577uL);
    }
    private static void ReallyDelete(string fileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501578uL);
      if ((File.GetAttributes(fileName) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501579uL);
        File.SetAttributes(fileName, FileAttributes.Normal);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501580uL);
      File.Delete(fileName);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501581uL);
    }
    private void WriteStatus(string format, params Object[] args)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501582uL);
      if (_container.ZipFile != null && _container.ZipFile.Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501583uL);
        _container.ZipFile.StatusMessageTextWriter.WriteLine(format, args);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501584uL);
    }
    private void InternalExtract(string baseDir, Stream outstream, string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501585uL);
      if (_container == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501586uL);
        throw new BadStateException("This entry is an orphan");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501587uL);
      if (_container.ZipFile == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501588uL);
        throw new InvalidOperationException("Use Extract() only with ZipFile.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501589uL);
      _container.ZipFile.Reset(false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501590uL);
      if (this._Source != ZipEntrySource.ZipFile) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501591uL);
        throw new BadStateException("You must call ZipFile.Save before calling any Extract method");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501592uL);
      OnBeforeExtract(baseDir);
      _ioOperationCanceled = false;
      string targetFileName = null;
      Stream output = null;
      bool fileExistsBeforeExtraction = false;
      bool checkLaterForResetDirTimes = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501593uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501594uL);
        ValidateCompression();
        ValidateEncryption();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501595uL);
        if (ValidateOutput(baseDir, outstream, out targetFileName)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501596uL);
          WriteStatus("extract dir {0}...", targetFileName);
          OnAfterExtract(baseDir);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501597uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501598uL);
        if (targetFileName != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501599uL);
          if (File.Exists(targetFileName)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501600uL);
            fileExistsBeforeExtraction = true;
            int rc = CheckExtractExistingFile(baseDir, targetFileName);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501601uL);
            if (rc == 2) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501602uL);
              goto ExitTry;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501603uL);
            if (rc == 1) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501604uL);
              return;
            }
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501605uL);
        string p = password ?? this._Password ?? this._container.Password;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501606uL);
        if (_Encryption_FromZipFile != EncryptionAlgorithm.None) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501607uL);
          if (p == null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501608uL);
            throw new BadPasswordException();
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501609uL);
          SetupCryptoForExtract(p);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501610uL);
        if (targetFileName != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501611uL);
          WriteStatus("extract file {0}...", targetFileName);
          targetFileName += ".tmp";
          var dirName = Path.GetDirectoryName(targetFileName);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501612uL);
          if (!Directory.Exists(dirName)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501613uL);
            Directory.CreateDirectory(dirName);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501614uL);
            if (_container.ZipFile != null) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501615uL);
              checkLaterForResetDirTimes = _container.ZipFile._inExtractAll;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501616uL);
          output = new FileStream(targetFileName, FileMode.CreateNew);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501617uL);
          WriteStatus("extract entry {0} to stream...", FileName);
          output = outstream;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501618uL);
        if (_ioOperationCanceled) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501619uL);
          goto ExitTry;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501620uL);
        Int32 ActualCrc32 = ExtractOne(output);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501621uL);
        if (_ioOperationCanceled) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501622uL);
          goto ExitTry;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501623uL);
        VerifyCrcAfterExtract(ActualCrc32);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501624uL);
        if (targetFileName != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501625uL);
          output.Close();
          output = null;
          string tmpName = targetFileName;
          string zombie = null;
          targetFileName = tmpName.Substring(0, tmpName.Length - 4);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501626uL);
          if (fileExistsBeforeExtraction) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501627uL);
            zombie = targetFileName + ".PendingOverwrite";
            File.Move(targetFileName, zombie);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501628uL);
          File.Move(tmpName, targetFileName);
          _SetTimes(targetFileName, true);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501629uL);
          if (zombie != null && File.Exists(zombie)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501630uL);
            ReallyDelete(zombie);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501631uL);
          if (checkLaterForResetDirTimes) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501632uL);
            if (this.FileName.IndexOf('/') != -1) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501633uL);
              string dirname = Path.GetDirectoryName(this.FileName);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501634uL);
              if (this._container.ZipFile[dirname] == null) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501635uL);
                _SetTimes(Path.GetDirectoryName(targetFileName), false);
              }
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501636uL);
          if ((_VersionMadeBy & 0xff00) == 0xa00 || (_VersionMadeBy & 0xff00) == 0x0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501637uL);
            File.SetAttributes(targetFileName, (FileAttributes)_ExternalFileAttrs);
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501638uL);
        OnAfterExtract(baseDir);
        ExitTry:
        ;
      } catch (Exception) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501639uL);
        _ioOperationCanceled = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501640uL);
        throw;
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501641uL);
        if (_ioOperationCanceled) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501642uL);
          if (targetFileName != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501643uL);
            try {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501644uL);
              if (output != null) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501645uL);
                output.Close();
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501646uL);
              if (File.Exists(targetFileName) && !fileExistsBeforeExtraction) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501647uL);
                File.Delete(targetFileName);
              }
            } finally {
            }
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501648uL);
    }
    internal void VerifyCrcAfterExtract(Int32 actualCrc32)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501649uL);
      if (actualCrc32 != _Crc32) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501650uL);
        if ((Encryption != EncryptionAlgorithm.WinZipAes128 && Encryption != EncryptionAlgorithm.WinZipAes256) || _WinZipAesMethod != 0x2) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501651uL);
          throw new BadCrcException("CRC error: the file being extracted appears to be corrupted. " + String.Format("Expected 0x{0:X8}, Actual 0x{1:X8}", _Crc32, actualCrc32));
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501652uL);
      if (this.UncompressedSize == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501653uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501654uL);
      if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501655uL);
        WinZipAesCipherStream wzs = _inputDecryptorStream as WinZipAesCipherStream;
        _aesCrypto_forExtract.CalculatedMac = wzs.FinalAuthentication;
        _aesCrypto_forExtract.ReadAndVerifyMac(this.ArchiveStream);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501656uL);
    }
    private int CheckExtractExistingFile(string baseDir, string targetFileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501657uL);
      int loop = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501658uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501659uL);
        switch (ExtractExistingFile) {
          case ExtractExistingFileAction.OverwriteSilently:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501662uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501660uL);
              WriteStatus("the file {0} exists; will overwrite it...", targetFileName);
              System.Int32 RNTRNTRNT_282 = 0;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501661uL);
              return RNTRNTRNT_282;
            }

          case ExtractExistingFileAction.DoNotOverwrite:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501665uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501663uL);
              WriteStatus("the file {0} exists; not extracting entry...", FileName);
              OnAfterExtract(baseDir);
              System.Int32 RNTRNTRNT_283 = 1;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501664uL);
              return RNTRNTRNT_283;
            }

          case ExtractExistingFileAction.InvokeExtractProgressEvent:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501672uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501666uL);
              if (loop > 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501667uL);
                throw new ZipException(String.Format("The file {0} already exists.", targetFileName));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501668uL);
              OnExtractExisting(baseDir);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501669uL);
              if (_ioOperationCanceled) {
                System.Int32 RNTRNTRNT_284 = 2;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501670uL);
                return RNTRNTRNT_284;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501671uL);
              break;
            }

          case ExtractExistingFileAction.Throw:
          default:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501674uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501673uL);
              throw new ZipException(String.Format("The file {0} already exists.", targetFileName));
            }

        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501675uL);
        loop++;
      } while (true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501676uL);
    }
    private void _CheckRead(int nbytes)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501677uL);
      if (nbytes == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501678uL);
        throw new BadReadException(String.Format("bad read of entry {0} from compressed archive.", this.FileName));
      }
    }
    private Stream _inputDecryptorStream;
    private Int32 ExtractOne(Stream output)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501679uL);
      Int32 CrcResult = 0;
      Stream input = this.ArchiveStream;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501680uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501681uL);
        input.Seek(this.FileDataPosition, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(input);
        byte[] bytes = new byte[BufferSize];
        Int64 LeftToRead = (_CompressionMethod_FromZipFile != (short)CompressionMethod.None) ? this.UncompressedSize : this._CompressedFileDataSize;
        _inputDecryptorStream = GetExtractDecryptor(input);
        Stream input3 = GetExtractDecompressor(_inputDecryptorStream);
        Int64 bytesWritten = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501682uL);
        using (var s1 = new Ionic.Crc.CrcCalculatorStream(input3)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501683uL);
          while (LeftToRead > 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501684uL);
            int len = (LeftToRead > bytes.Length) ? bytes.Length : (int)LeftToRead;
            int n = s1.Read(bytes, 0, len);
            _CheckRead(n);
            output.Write(bytes, 0, n);
            LeftToRead -= n;
            bytesWritten += n;
            OnExtractProgress(bytesWritten, UncompressedSize);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501685uL);
            if (_ioOperationCanceled) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501686uL);
              break;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501687uL);
          CrcResult = s1.Crc;
        }
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501688uL);
        var zss = input as ZipSegmentedStream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501689uL);
        if (zss != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501690uL);
          zss.Dispose();
          _archiveStream = null;
        }
      }
      Int32 RNTRNTRNT_285 = CrcResult;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501691uL);
      return RNTRNTRNT_285;
    }
    internal Stream GetExtractDecompressor(Stream input2)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501692uL);
      switch (_CompressionMethod_FromZipFile) {
        case (short)CompressionMethod.None:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501694uL);
          
          {
            Stream RNTRNTRNT_286 = input2;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501693uL);
            return RNTRNTRNT_286;
          }

        case (short)CompressionMethod.Deflate:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501696uL);
          
          {
            Stream RNTRNTRNT_287 = new Ionic.Zlib.DeflateStream(input2, Ionic.Zlib.CompressionMode.Decompress, true);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501695uL);
            return RNTRNTRNT_287;
          }

        case (short)CompressionMethod.BZip2:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501698uL);
          
          {
            Stream RNTRNTRNT_288 = new Ionic.BZip2.BZip2InputStream(input2, true);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501697uL);
            return RNTRNTRNT_288;
          }

      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501699uL);
      return null;
    }
    internal Stream GetExtractDecryptor(Stream input)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501700uL);
      Stream input2 = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501701uL);
      if (_Encryption_FromZipFile == EncryptionAlgorithm.PkzipWeak) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501702uL);
        input2 = new ZipCipherStream(input, _zipCrypto_forExtract, CryptoMode.Decrypt);
      } else if (_Encryption_FromZipFile == EncryptionAlgorithm.WinZipAes128 || _Encryption_FromZipFile == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501704uL);
        input2 = new WinZipAesCipherStream(input, _aesCrypto_forExtract, _CompressedFileDataSize, CryptoMode.Decrypt);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501703uL);
        input2 = input;
      }
      Stream RNTRNTRNT_289 = input2;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501705uL);
      return RNTRNTRNT_289;
    }
    internal void _SetTimes(string fileOrDirectory, bool isFile)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501706uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501707uL);
        if (_ntfsTimesAreSet) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501708uL);
          if (isFile) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501709uL);
            if (File.Exists(fileOrDirectory)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501710uL);
              File.SetCreationTimeUtc(fileOrDirectory, _Ctime);
              File.SetLastAccessTimeUtc(fileOrDirectory, _Atime);
              File.SetLastWriteTimeUtc(fileOrDirectory, _Mtime);
            }
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501711uL);
            if (Directory.Exists(fileOrDirectory)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501712uL);
              Directory.SetCreationTimeUtc(fileOrDirectory, _Ctime);
              Directory.SetLastAccessTimeUtc(fileOrDirectory, _Atime);
              Directory.SetLastWriteTimeUtc(fileOrDirectory, _Mtime);
            }
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501713uL);
          DateTime AdjustedLastModified = Ionic.Zip.SharedUtilities.AdjustTime_Reverse(LastModified);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501714uL);
          if (isFile) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501715uL);
            File.SetLastWriteTime(fileOrDirectory, AdjustedLastModified);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501716uL);
            Directory.SetLastWriteTime(fileOrDirectory, AdjustedLastModified);
          }
        }
      } catch (System.IO.IOException ioexc1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501717uL);
        WriteStatus("failed to set time on {0}: {1}", fileOrDirectory, ioexc1.Message);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501718uL);
    }
    private string UnsupportedAlgorithm {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501719uL);
        string alg = String.Empty;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501720uL);
        switch (_UnsupportedAlgorithmId) {
          case 0:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501723uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501721uL);
              alg = "--";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501722uL);
              break;
            }

          case 0x6601:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501726uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501724uL);
              alg = "DES";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501725uL);
              break;
            }

          case 0x6602:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501729uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501727uL);
              alg = "RC2";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501728uL);
              break;
            }

          case 0x6603:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501732uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501730uL);
              alg = "3DES-168";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501731uL);
              break;
            }

          case 0x6609:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501735uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501733uL);
              alg = "3DES-112";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501734uL);
              break;
            }

          case 0x660e:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501738uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501736uL);
              alg = "PKWare AES128";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501737uL);
              break;
            }

          case 0x660f:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501741uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501739uL);
              alg = "PKWare AES192";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501740uL);
              break;
            }

          case 0x6610:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501744uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501742uL);
              alg = "PKWare AES256";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501743uL);
              break;
            }

          case 0x6702:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501747uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501745uL);
              alg = "RC2";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501746uL);
              break;
            }

          case 0x6720:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501750uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501748uL);
              alg = "Blowfish";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501749uL);
              break;
            }

          case 0x6721:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501753uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501751uL);
              alg = "Twofish";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501752uL);
              break;
            }

          case 0x6801:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501756uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501754uL);
              alg = "RC4";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501755uL);
              break;
            }

          case 0xffff:
          default:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501759uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501757uL);
              alg = String.Format("Unknown (0x{0:X4})", _UnsupportedAlgorithmId);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501758uL);
              break;
            }

        }
        System.String RNTRNTRNT_290 = alg;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501760uL);
        return RNTRNTRNT_290;
      }
    }
    private string UnsupportedCompressionMethod {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501761uL);
        string meth = String.Empty;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501762uL);
        switch ((int)_CompressionMethod) {
          case 0:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501765uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501763uL);
              meth = "Store";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501764uL);
              break;
            }

          case 1:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501768uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501766uL);
              meth = "Shrink";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501767uL);
              break;
            }

          case 8:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501771uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501769uL);
              meth = "DEFLATE";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501770uL);
              break;
            }

          case 9:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501774uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501772uL);
              meth = "Deflate64";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501773uL);
              break;
            }

          case 12:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501777uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501775uL);
              meth = "BZIP2";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501776uL);
              break;
            }

          case 14:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501780uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501778uL);
              meth = "LZMA";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501779uL);
              break;
            }

          case 19:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501783uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501781uL);
              meth = "LZ77";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501782uL);
              break;
            }

          case 98:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501786uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501784uL);
              meth = "PPMd";
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501785uL);
              break;
            }

          default:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501789uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501787uL);
              meth = String.Format("Unknown (0x{0:X4})", _CompressionMethod);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501788uL);
              break;
            }

        }
        System.String RNTRNTRNT_291 = meth;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501790uL);
        return RNTRNTRNT_291;
      }
    }
    internal void ValidateEncryption()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501791uL);
      if (Encryption != EncryptionAlgorithm.PkzipWeak && Encryption != EncryptionAlgorithm.WinZipAes128 && Encryption != EncryptionAlgorithm.WinZipAes256 && Encryption != EncryptionAlgorithm.None) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501792uL);
        if (_UnsupportedAlgorithmId != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501793uL);
          throw new ZipException(String.Format("Cannot extract: Entry {0} is encrypted with an algorithm not supported by DotNetZip: {1}", FileName, UnsupportedAlgorithm));
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501794uL);
          throw new ZipException(String.Format("Cannot extract: Entry {0} uses an unsupported encryption algorithm ({1:X2})", FileName, (int)Encryption));
        }
      }
    }
    private void ValidateCompression()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501795uL);
      if ((_CompressionMethod_FromZipFile != (short)CompressionMethod.None) && (_CompressionMethod_FromZipFile != (short)CompressionMethod.Deflate) && (_CompressionMethod_FromZipFile != (short)CompressionMethod.BZip2)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501796uL);
        throw new ZipException(String.Format("Entry {0} uses an unsupported compression method (0x{1:X2}, {2})", FileName, _CompressionMethod_FromZipFile, UnsupportedCompressionMethod));
      }
    }
    private void SetupCryptoForExtract(string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501797uL);
      if (_Encryption_FromZipFile == EncryptionAlgorithm.None) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501798uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501799uL);
      if (_Encryption_FromZipFile == EncryptionAlgorithm.PkzipWeak) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501800uL);
        if (password == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501801uL);
          throw new ZipException("Missing password.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501802uL);
        this.ArchiveStream.Seek(this.FileDataPosition - 12, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
        _zipCrypto_forExtract = ZipCrypto.ForRead(password, this);
      } else if (_Encryption_FromZipFile == EncryptionAlgorithm.WinZipAes128 || _Encryption_FromZipFile == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501803uL);
        if (password == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501804uL);
          throw new ZipException("Missing password.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501805uL);
        if (_aesCrypto_forExtract != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501806uL);
          _aesCrypto_forExtract.Password = password;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501807uL);
          int sizeOfSaltAndPv = GetLengthOfCryptoHeaderBytes(_Encryption_FromZipFile);
          this.ArchiveStream.Seek(this.FileDataPosition - sizeOfSaltAndPv, SeekOrigin.Begin);
          Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
          int keystrength = GetKeyStrengthInBits(_Encryption_FromZipFile);
          _aesCrypto_forExtract = WinZipAesCrypto.ReadFromStream(password, keystrength, this.ArchiveStream);
        }
      }
    }
    private bool ValidateOutput(string basedir, Stream outstream, out string outFileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501808uL);
      if (basedir != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501809uL);
        string f = this.FileName.Replace("\\", "/");
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501810uL);
        if (f.IndexOf(':') == 1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501811uL);
          f = f.Substring(2);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501812uL);
        if (f.StartsWith("/")) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501813uL);
          f = f.Substring(1);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501814uL);
        if (_container.ZipFile.FlattenFoldersOnExtract) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501815uL);
          outFileName = Path.Combine(basedir, (f.IndexOf('/') != -1) ? Path.GetFileName(f) : f);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501816uL);
          outFileName = Path.Combine(basedir, f);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501817uL);
        outFileName = outFileName.Replace("/", "\\");
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501818uL);
        if ((IsDirectory) || (FileName.EndsWith("/"))) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501819uL);
          if (!Directory.Exists(outFileName)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501820uL);
            Directory.CreateDirectory(outFileName);
            _SetTimes(outFileName, false);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501821uL);
            if (ExtractExistingFile == ExtractExistingFileAction.OverwriteSilently) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501822uL);
              _SetTimes(outFileName, false);
            }
          }
          System.Boolean RNTRNTRNT_292 = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501823uL);
          return RNTRNTRNT_292;
        }
        System.Boolean RNTRNTRNT_293 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501824uL);
        return RNTRNTRNT_293;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501825uL);
      if (outstream != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501826uL);
        outFileName = null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501827uL);
        if ((IsDirectory) || (FileName.EndsWith("/"))) {
          System.Boolean RNTRNTRNT_294 = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501828uL);
          return RNTRNTRNT_294;
        }
        System.Boolean RNTRNTRNT_295 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501829uL);
        return RNTRNTRNT_295;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501830uL);
      throw new ArgumentNullException("outstream");
    }
  }
}
