using System;
using System.IO;
using System.Collections.Generic;
using System.Security.Cryptography;
namespace Ionic.Zip
{
  internal class WinZipAesCrypto
  {
    internal byte[] _Salt;
    internal byte[] _providedPv;
    internal byte[] _generatedPv;
    internal int _KeyStrengthInBits;
    private byte[] _MacInitializationVector;
    private byte[] _StoredMac;
    private byte[] _keyBytes;
    private Int16 PasswordVerificationStored;
    private Int16 PasswordVerificationGenerated;
    private int Rfc2898KeygenIterations = 1000;
    private string _Password;
    private bool _cryptoGenerated;
    private WinZipAesCrypto(string password, int KeyStrengthInBits)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500769uL);
      _Password = password;
      _KeyStrengthInBits = KeyStrengthInBits;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500770uL);
    }
    public static WinZipAesCrypto Generate(string password, int KeyStrengthInBits)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500771uL);
      WinZipAesCrypto c = new WinZipAesCrypto(password, KeyStrengthInBits);
      int saltSizeInBytes = c._KeyStrengthInBytes / 2;
      c._Salt = new byte[saltSizeInBytes];
      Random rnd = new Random();
      rnd.NextBytes(c._Salt);
      WinZipAesCrypto RNTRNTRNT_133 = c;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500772uL);
      return RNTRNTRNT_133;
    }
    public static WinZipAesCrypto ReadFromStream(string password, int KeyStrengthInBits, Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500773uL);
      WinZipAesCrypto c = new WinZipAesCrypto(password, KeyStrengthInBits);
      int saltSizeInBytes = c._KeyStrengthInBytes / 2;
      c._Salt = new byte[saltSizeInBytes];
      c._providedPv = new byte[2];
      s.Read(c._Salt, 0, c._Salt.Length);
      s.Read(c._providedPv, 0, c._providedPv.Length);
      c.PasswordVerificationStored = (Int16)(c._providedPv[0] + c._providedPv[1] * 256);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500774uL);
      if (password != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500775uL);
        c.PasswordVerificationGenerated = (Int16)(c.GeneratedPV[0] + c.GeneratedPV[1] * 256);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500776uL);
        if (c.PasswordVerificationGenerated != c.PasswordVerificationStored) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500777uL);
          throw new BadPasswordException("bad password");
        }
      }
      WinZipAesCrypto RNTRNTRNT_134 = c;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500778uL);
      return RNTRNTRNT_134;
    }
    public byte[] GeneratedPV {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500779uL);
        if (!_cryptoGenerated) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500780uL);
          _GenerateCryptoBytes();
        }
        System.Byte[] RNTRNTRNT_135 = _generatedPv;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500781uL);
        return RNTRNTRNT_135;
      }
    }
    public byte[] Salt {
      get {
        System.Byte[] RNTRNTRNT_136 = _Salt;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500782uL);
        return RNTRNTRNT_136;
      }
    }
    private int _KeyStrengthInBytes {
      get {
        System.Int32 RNTRNTRNT_137 = _KeyStrengthInBits / 8;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500783uL);
        return RNTRNTRNT_137;
      }
    }
    public int SizeOfEncryptionMetadata {
      get {
        System.Int32 RNTRNTRNT_138 = _KeyStrengthInBytes / 2 + 10 + 2;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500784uL);
        return RNTRNTRNT_138;
      }
    }
    public string Password {
      private get {
        System.String RNTRNTRNT_139 = _Password;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500785uL);
        return RNTRNTRNT_139;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500786uL);
        _Password = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500787uL);
        if (_Password != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500788uL);
          PasswordVerificationGenerated = (Int16)(GeneratedPV[0] + GeneratedPV[1] * 256);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500789uL);
          if (PasswordVerificationGenerated != PasswordVerificationStored) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500790uL);
            throw new Ionic.Zip.BadPasswordException();
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500791uL);
      }
    }
    private void _GenerateCryptoBytes()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500792uL);
      System.Security.Cryptography.Rfc2898DeriveBytes rfc2898 = new System.Security.Cryptography.Rfc2898DeriveBytes(_Password, Salt, Rfc2898KeygenIterations);
      _keyBytes = rfc2898.GetBytes(_KeyStrengthInBytes);
      _MacInitializationVector = rfc2898.GetBytes(_KeyStrengthInBytes);
      _generatedPv = rfc2898.GetBytes(2);
      _cryptoGenerated = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500793uL);
    }
    public byte[] KeyBytes {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500794uL);
        if (!_cryptoGenerated) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500795uL);
          _GenerateCryptoBytes();
        }
        System.Byte[] RNTRNTRNT_140 = _keyBytes;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500796uL);
        return RNTRNTRNT_140;
      }
    }
    public byte[] MacIv {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500797uL);
        if (!_cryptoGenerated) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500798uL);
          _GenerateCryptoBytes();
        }
        System.Byte[] RNTRNTRNT_141 = _MacInitializationVector;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500799uL);
        return RNTRNTRNT_141;
      }
    }
    public byte[] CalculatedMac;
    public void ReadAndVerifyMac(System.IO.Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500800uL);
      bool invalid = false;
      _StoredMac = new byte[10];
      s.Read(_StoredMac, 0, _StoredMac.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500801uL);
      if (_StoredMac.Length != CalculatedMac.Length) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500802uL);
        invalid = true;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500803uL);
      if (!invalid) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500804uL);
        for (int i = 0; i < _StoredMac.Length; i++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500805uL);
          if (_StoredMac[i] != CalculatedMac[i]) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500806uL);
            invalid = true;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500807uL);
      if (invalid) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500808uL);
        throw new Ionic.Zip.BadStateException("The MAC does not match.");
      }
    }
  }
  internal class WinZipAesCipherStream : Stream
  {
    private WinZipAesCrypto _params;
    private System.IO.Stream _s;
    private CryptoMode _mode;
    private int _nonce;
    private bool _finalBlock;
    internal HMACSHA1 _mac;
    internal RijndaelManaged _aesCipher;
    internal ICryptoTransform _xform;
    private const int BLOCK_SIZE_IN_BYTES = 16;
    private byte[] counter = new byte[BLOCK_SIZE_IN_BYTES];
    private byte[] counterOut = new byte[BLOCK_SIZE_IN_BYTES];
    private long _length;
    private long _totalBytesXferred;
    private byte[] _PendingWriteBlock;
    private int _pendingCount;
    private byte[] _iobuf;
    internal WinZipAesCipherStream(System.IO.Stream s, WinZipAesCrypto cryptoParams, long length, CryptoMode mode) : this(s, cryptoParams, mode)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500809uL);
      _length = length;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500810uL);
    }
    internal WinZipAesCipherStream(System.IO.Stream s, WinZipAesCrypto cryptoParams, CryptoMode mode) : base()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500811uL);
      TraceOutput("-------------------------------------------------------");
      TraceOutput("Create {0:X8}", this.GetHashCode());
      _params = cryptoParams;
      _s = s;
      _mode = mode;
      _nonce = 1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500812uL);
      if (_params == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500813uL);
        throw new BadPasswordException("Supply a password to use AES encryption.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500814uL);
      int keySizeInBits = _params.KeyBytes.Length * 8;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500815uL);
      if (keySizeInBits != 256 && keySizeInBits != 128 && keySizeInBits != 192) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500816uL);
        throw new ArgumentOutOfRangeException("keysize", "size of key must be 128, 192, or 256");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500817uL);
      _mac = new HMACSHA1(_params.MacIv);
      _aesCipher = new System.Security.Cryptography.RijndaelManaged();
      _aesCipher.BlockSize = 128;
      _aesCipher.KeySize = keySizeInBits;
      _aesCipher.Mode = CipherMode.ECB;
      _aesCipher.Padding = PaddingMode.None;
      byte[] iv = new byte[BLOCK_SIZE_IN_BYTES];
      _xform = _aesCipher.CreateEncryptor(_params.KeyBytes, iv);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500818uL);
      if (_mode == CryptoMode.Encrypt) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500819uL);
        _iobuf = new byte[2048];
        _PendingWriteBlock = new byte[BLOCK_SIZE_IN_BYTES];
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500820uL);
    }
    private void XorInPlace(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500821uL);
      for (int i = 0; i < count; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500822uL);
        buffer[offset + i] = (byte)(counterOut[i] ^ buffer[offset + i]);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500823uL);
    }
    private void WriteTransformOneBlock(byte[] buffer, int offset)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500824uL);
      System.Array.Copy(BitConverter.GetBytes(_nonce++), 0, counter, 0, 4);
      _xform.TransformBlock(counter, 0, BLOCK_SIZE_IN_BYTES, counterOut, 0);
      XorInPlace(buffer, offset, BLOCK_SIZE_IN_BYTES);
      _mac.TransformBlock(buffer, offset, BLOCK_SIZE_IN_BYTES, null, 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500825uL);
    }
    private void WriteTransformBlocks(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500826uL);
      int posn = offset;
      int last = count + offset;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500827uL);
      while (posn < buffer.Length && posn < last) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500828uL);
        WriteTransformOneBlock(buffer, posn);
        posn += BLOCK_SIZE_IN_BYTES;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500829uL);
    }
    private void WriteTransformFinalBlock()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500830uL);
      if (_pendingCount == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500831uL);
        throw new InvalidOperationException("No bytes available.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500832uL);
      if (_finalBlock) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500833uL);
        throw new InvalidOperationException("The final block has already been transformed.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500834uL);
      System.Array.Copy(BitConverter.GetBytes(_nonce++), 0, counter, 0, 4);
      counterOut = _xform.TransformFinalBlock(counter, 0, BLOCK_SIZE_IN_BYTES);
      XorInPlace(_PendingWriteBlock, 0, _pendingCount);
      _mac.TransformFinalBlock(_PendingWriteBlock, 0, _pendingCount);
      _finalBlock = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500835uL);
    }
    private int ReadTransformOneBlock(byte[] buffer, int offset, int last)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500836uL);
      if (_finalBlock) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500837uL);
        throw new NotSupportedException();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500838uL);
      int bytesRemaining = last - offset;
      int bytesToRead = (bytesRemaining > BLOCK_SIZE_IN_BYTES) ? BLOCK_SIZE_IN_BYTES : bytesRemaining;
      System.Array.Copy(BitConverter.GetBytes(_nonce++), 0, counter, 0, 4);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500839uL);
      if ((bytesToRead == bytesRemaining) && (_length > 0) && (_totalBytesXferred + last == _length)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500840uL);
        _mac.TransformFinalBlock(buffer, offset, bytesToRead);
        counterOut = _xform.TransformFinalBlock(counter, 0, BLOCK_SIZE_IN_BYTES);
        _finalBlock = true;
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500841uL);
        _mac.TransformBlock(buffer, offset, bytesToRead, null, 0);
        _xform.TransformBlock(counter, 0, BLOCK_SIZE_IN_BYTES, counterOut, 0);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500842uL);
      XorInPlace(buffer, offset, bytesToRead);
      System.Int32 RNTRNTRNT_142 = bytesToRead;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500843uL);
      return RNTRNTRNT_142;
    }
    private void ReadTransformBlocks(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500844uL);
      int posn = offset;
      int last = count + offset;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500845uL);
      while (posn < buffer.Length && posn < last) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500846uL);
        int n = ReadTransformOneBlock(buffer, posn, last);
        posn += n;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500847uL);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500848uL);
      if (_mode == CryptoMode.Encrypt) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500849uL);
        throw new NotSupportedException();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500850uL);
      if (buffer == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500851uL);
        throw new ArgumentNullException("buffer");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500852uL);
      if (offset < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500853uL);
        throw new ArgumentOutOfRangeException("offset", "Must not be less than zero.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500854uL);
      if (count < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500855uL);
        throw new ArgumentOutOfRangeException("count", "Must not be less than zero.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500856uL);
      if (buffer.Length < offset + count) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500857uL);
        throw new ArgumentException("The buffer is too small");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500858uL);
      int bytesToRead = count;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500859uL);
      if (_totalBytesXferred >= _length) {
        System.Int32 RNTRNTRNT_143 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500860uL);
        return RNTRNTRNT_143;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500861uL);
      long bytesRemaining = _length - _totalBytesXferred;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500862uL);
      if (bytesRemaining < count) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500863uL);
        bytesToRead = (int)bytesRemaining;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500864uL);
      int n = _s.Read(buffer, offset, bytesToRead);
      ReadTransformBlocks(buffer, offset, bytesToRead);
      _totalBytesXferred += n;
      System.Int32 RNTRNTRNT_144 = n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500865uL);
      return RNTRNTRNT_144;
    }
    public byte[] FinalAuthentication {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500866uL);
        if (!_finalBlock) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500867uL);
          if (_totalBytesXferred != 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500868uL);
            throw new BadStateException("The final hash has not been computed.");
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500869uL);
          byte[] b = {
            
          };
          _mac.ComputeHash(b);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500870uL);
        byte[] macBytes10 = new byte[10];
        System.Array.Copy(_mac.Hash, 0, macBytes10, 0, 10);
        System.Byte[] RNTRNTRNT_145 = macBytes10;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500871uL);
        return RNTRNTRNT_145;
      }
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500872uL);
      if (_finalBlock) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500873uL);
        throw new InvalidOperationException("The final block has already been transformed.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500874uL);
      if (_mode == CryptoMode.Decrypt) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500875uL);
        throw new NotSupportedException();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500876uL);
      if (buffer == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500877uL);
        throw new ArgumentNullException("buffer");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500878uL);
      if (offset < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500879uL);
        throw new ArgumentOutOfRangeException("offset", "Must not be less than zero.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500880uL);
      if (count < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500881uL);
        throw new ArgumentOutOfRangeException("count", "Must not be less than zero.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500882uL);
      if (buffer.Length < offset + count) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500883uL);
        throw new ArgumentException("The offset and count are too large");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500884uL);
      if (count == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500885uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500886uL);
      TraceOutput("Write off({0}) count({1})", offset, count);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500887uL);
      if (count + _pendingCount <= BLOCK_SIZE_IN_BYTES) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500888uL);
        Buffer.BlockCopy(buffer, offset, _PendingWriteBlock, _pendingCount, count);
        _pendingCount += count;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500889uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500890uL);
      int bytesRemaining = count;
      int curOffset = offset;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500891uL);
      if (_pendingCount != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500892uL);
        int fillCount = BLOCK_SIZE_IN_BYTES - _pendingCount;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500893uL);
        if (fillCount > 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500894uL);
          Buffer.BlockCopy(buffer, offset, _PendingWriteBlock, _pendingCount, fillCount);
          bytesRemaining -= fillCount;
          curOffset += fillCount;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500895uL);
        WriteTransformOneBlock(_PendingWriteBlock, 0);
        _s.Write(_PendingWriteBlock, 0, BLOCK_SIZE_IN_BYTES);
        _totalBytesXferred += BLOCK_SIZE_IN_BYTES;
        _pendingCount = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500896uL);
      int blocksToXform = (bytesRemaining - 1) / BLOCK_SIZE_IN_BYTES;
      _pendingCount = bytesRemaining - (blocksToXform * BLOCK_SIZE_IN_BYTES);
      Buffer.BlockCopy(buffer, curOffset + bytesRemaining - _pendingCount, _PendingWriteBlock, 0, _pendingCount);
      bytesRemaining -= _pendingCount;
      _totalBytesXferred += bytesRemaining;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500897uL);
      if (blocksToXform > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500898uL);
        do {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500899uL);
          int c = _iobuf.Length;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500900uL);
          if (c > bytesRemaining) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500901uL);
            c = bytesRemaining;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500902uL);
          Buffer.BlockCopy(buffer, curOffset, _iobuf, 0, c);
          WriteTransformBlocks(_iobuf, 0, c);
          _s.Write(_iobuf, 0, c);
          bytesRemaining -= c;
          curOffset += c;
        } while (bytesRemaining > 0);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500903uL);
    }
    public override void Close()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500904uL);
      TraceOutput("Close {0:X8}", this.GetHashCode());
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500905uL);
      if (_pendingCount > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500906uL);
        WriteTransformFinalBlock();
        _s.Write(_PendingWriteBlock, 0, _pendingCount);
        _totalBytesXferred += _pendingCount;
        _pendingCount = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500907uL);
      _s.Close();
      TraceOutput("-------------------------------------------------------");
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500908uL);
    }
    public override bool CanRead {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500909uL);
        if (_mode != CryptoMode.Decrypt) {
          System.Boolean RNTRNTRNT_146 = false;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500910uL);
          return RNTRNTRNT_146;
        }
        System.Boolean RNTRNTRNT_147 = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500911uL);
        return RNTRNTRNT_147;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_148 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500912uL);
        return RNTRNTRNT_148;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_149 = (_mode == CryptoMode.Encrypt);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500913uL);
        return RNTRNTRNT_149;
      }
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500914uL);
      _s.Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500915uL);
    }
    public override long Length {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500916uL);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500917uL);
        throw new NotImplementedException();
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500918uL);
        throw new NotImplementedException();
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500919uL);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500920uL);
      throw new NotImplementedException();
    }
    [System.Diagnostics.ConditionalAttribute("Trace")]
    private void TraceOutput(string format, params object[] varParams)
    {
      lock (_outputLock) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500921uL);
        int tid = System.Threading.Thread.CurrentThread.GetHashCode();
        Console.ForegroundColor = (ConsoleColor)(tid % 8 + 8);
        Console.Write("{0:000} WZACS ", tid);
        Console.WriteLine(format, varParams);
        Console.ResetColor();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500922uL);
    }
    private object _outputLock = new Object();
  }
}
