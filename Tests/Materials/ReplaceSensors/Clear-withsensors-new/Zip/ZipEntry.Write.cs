using System;
using System.IO;
using RE = System.Text.RegularExpressions;
namespace Ionic.Zip
{
  public partial class ZipEntry
  {
    internal void WriteCentralDirectoryEntry(Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502003uL);
      byte[] bytes = new byte[4096];
      int i = 0;
      bytes[i++] = (byte)(ZipConstants.ZipDirEntrySignature & 0xff);
      bytes[i++] = (byte)((ZipConstants.ZipDirEntrySignature & 0xff00) >> 8);
      bytes[i++] = (byte)((ZipConstants.ZipDirEntrySignature & 0xff0000) >> 16);
      bytes[i++] = (byte)((ZipConstants.ZipDirEntrySignature & 0xff000000u) >> 24);
      bytes[i++] = (byte)(_VersionMadeBy & 0xff);
      bytes[i++] = (byte)((_VersionMadeBy & 0xff00) >> 8);
      Int16 vNeeded = (Int16)(VersionNeeded != 0 ? VersionNeeded : 20);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502004uL);
      if (_OutputUsesZip64 == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502005uL);
        _OutputUsesZip64 = new Nullable<bool>(_container.Zip64 == Zip64Option.Always);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502006uL);
      Int16 versionNeededToExtract = (Int16)(_OutputUsesZip64.Value ? 45 : vNeeded);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502007uL);
      if (this.CompressionMethod == Ionic.Zip.CompressionMethod.BZip2) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502008uL);
        versionNeededToExtract = 46;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502009uL);
      bytes[i++] = (byte)(versionNeededToExtract & 0xff);
      bytes[i++] = (byte)((versionNeededToExtract & 0xff00) >> 8);
      bytes[i++] = (byte)(_BitField & 0xff);
      bytes[i++] = (byte)((_BitField & 0xff00) >> 8);
      bytes[i++] = (byte)(_CompressionMethod & 0xff);
      bytes[i++] = (byte)((_CompressionMethod & 0xff00) >> 8);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502010uL);
      if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502011uL);
        i -= 2;
        bytes[i++] = 0x63;
        bytes[i++] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502012uL);
      bytes[i++] = (byte)(_TimeBlob & 0xff);
      bytes[i++] = (byte)((_TimeBlob & 0xff00) >> 8);
      bytes[i++] = (byte)((_TimeBlob & 0xff0000) >> 16);
      bytes[i++] = (byte)((_TimeBlob & 0xff000000u) >> 24);
      bytes[i++] = (byte)(_Crc32 & 0xff);
      bytes[i++] = (byte)((_Crc32 & 0xff00) >> 8);
      bytes[i++] = (byte)((_Crc32 & 0xff0000) >> 16);
      bytes[i++] = (byte)((_Crc32 & 0xff000000u) >> 24);
      int j = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502013uL);
      if (_OutputUsesZip64.Value) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502014uL);
        for (j = 0; j < 8; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502015uL);
          bytes[i++] = 0xff;
        }
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502016uL);
        bytes[i++] = (byte)(_CompressedSize & 0xff);
        bytes[i++] = (byte)((_CompressedSize & 0xff00) >> 8);
        bytes[i++] = (byte)((_CompressedSize & 0xff0000) >> 16);
        bytes[i++] = (byte)((_CompressedSize & 0xff000000u) >> 24);
        bytes[i++] = (byte)(_UncompressedSize & 0xff);
        bytes[i++] = (byte)((_UncompressedSize & 0xff00) >> 8);
        bytes[i++] = (byte)((_UncompressedSize & 0xff0000) >> 16);
        bytes[i++] = (byte)((_UncompressedSize & 0xff000000u) >> 24);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502017uL);
      byte[] fileNameBytes = GetEncodedFileNameBytes();
      Int16 filenameLength = (Int16)fileNameBytes.Length;
      bytes[i++] = (byte)(filenameLength & 0xff);
      bytes[i++] = (byte)((filenameLength & 0xff00) >> 8);
      _presumeZip64 = _OutputUsesZip64.Value;
      _Extra = ConstructExtraField(true);
      Int16 extraFieldLength = (Int16)((_Extra == null) ? 0 : _Extra.Length);
      bytes[i++] = (byte)(extraFieldLength & 0xff);
      bytes[i++] = (byte)((extraFieldLength & 0xff00) >> 8);
      int commentLength = (_CommentBytes == null) ? 0 : _CommentBytes.Length;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502018uL);
      if (commentLength + i > bytes.Length) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502019uL);
        commentLength = bytes.Length - i;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502020uL);
      bytes[i++] = (byte)(commentLength & 0xff);
      bytes[i++] = (byte)((commentLength & 0xff00) >> 8);
      bool segmented = (this._container.ZipFile != null) && (this._container.ZipFile.MaxOutputSegmentSize != 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502021uL);
      if (segmented) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502022uL);
        bytes[i++] = (byte)(_diskNumber & 0xff);
        bytes[i++] = (byte)((_diskNumber & 0xff00) >> 8);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502023uL);
        bytes[i++] = 0;
        bytes[i++] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502024uL);
      bytes[i++] = (byte)((_IsText) ? 1 : 0);
      bytes[i++] = 0;
      bytes[i++] = (byte)(_ExternalFileAttrs & 0xff);
      bytes[i++] = (byte)((_ExternalFileAttrs & 0xff00) >> 8);
      bytes[i++] = (byte)((_ExternalFileAttrs & 0xff0000) >> 16);
      bytes[i++] = (byte)((_ExternalFileAttrs & 0xff000000u) >> 24);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502025uL);
      if (_RelativeOffsetOfLocalHeader > 0xffffffffL) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502026uL);
        bytes[i++] = 0xff;
        bytes[i++] = 0xff;
        bytes[i++] = 0xff;
        bytes[i++] = 0xff;
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502027uL);
        bytes[i++] = (byte)(_RelativeOffsetOfLocalHeader & 0xff);
        bytes[i++] = (byte)((_RelativeOffsetOfLocalHeader & 0xff00) >> 8);
        bytes[i++] = (byte)((_RelativeOffsetOfLocalHeader & 0xff0000) >> 16);
        bytes[i++] = (byte)((_RelativeOffsetOfLocalHeader & 0xff000000u) >> 24);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502028uL);
      Buffer.BlockCopy(fileNameBytes, 0, bytes, i, filenameLength);
      i += filenameLength;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502029uL);
      if (_Extra != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502030uL);
        byte[] h = _Extra;
        int offx = 0;
        Buffer.BlockCopy(h, offx, bytes, i, extraFieldLength);
        i += extraFieldLength;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502031uL);
      if (commentLength != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502032uL);
        Buffer.BlockCopy(_CommentBytes, 0, bytes, i, commentLength);
        i += commentLength;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502033uL);
      s.Write(bytes, 0, i);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502034uL);
    }
    private byte[] ConstructExtraField(bool forCentralDirectory)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502035uL);
      var listOfBlocks = new System.Collections.Generic.List<byte[]>();
      byte[] block;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502036uL);
      if (_container.Zip64 == Zip64Option.Always || (_container.Zip64 == Zip64Option.AsNecessary && (!forCentralDirectory || _entryRequiresZip64.Value))) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502037uL);
        int sz = 4 + (forCentralDirectory ? 28 : 16);
        block = new byte[sz];
        int i = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502038uL);
        if (_presumeZip64 || forCentralDirectory) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502039uL);
          block[i++] = 0x1;
          block[i++] = 0x0;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502040uL);
          block[i++] = 0x99;
          block[i++] = 0x99;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502041uL);
        block[i++] = (byte)(sz - 4);
        block[i++] = 0x0;
        Array.Copy(BitConverter.GetBytes(_UncompressedSize), 0, block, i, 8);
        i += 8;
        Array.Copy(BitConverter.GetBytes(_CompressedSize), 0, block, i, 8);
        i += 8;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502042uL);
        if (forCentralDirectory) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502043uL);
          Array.Copy(BitConverter.GetBytes(_RelativeOffsetOfLocalHeader), 0, block, i, 8);
          i += 8;
          Array.Copy(BitConverter.GetBytes(0), 0, block, i, 4);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502044uL);
        listOfBlocks.Add(block);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502045uL);
      if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502046uL);
        block = new byte[4 + 7];
        int i = 0;
        block[i++] = 0x1;
        block[i++] = 0x99;
        block[i++] = 0x7;
        block[i++] = 0x0;
        block[i++] = 0x1;
        block[i++] = 0x0;
        block[i++] = 0x41;
        block[i++] = 0x45;
        int keystrength = GetKeyStrengthInBits(Encryption);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502047uL);
        if (keystrength == 128) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502048uL);
          block[i] = 1;
        } else if (keystrength == 256) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502050uL);
          block[i] = 3;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502049uL);
          block[i] = 0xff;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502051uL);
        i++;
        block[i++] = (byte)(_CompressionMethod & 0xff);
        block[i++] = (byte)(_CompressionMethod & 0xff00);
        listOfBlocks.Add(block);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502052uL);
      if (_ntfsTimesAreSet && _emitNtfsTimes) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502053uL);
        block = new byte[32 + 4];
        int i = 0;
        block[i++] = 0xa;
        block[i++] = 0x0;
        block[i++] = 32;
        block[i++] = 0;
        i += 4;
        block[i++] = 0x1;
        block[i++] = 0x0;
        block[i++] = 24;
        block[i++] = 0;
        Int64 z = _Mtime.ToFileTime();
        Array.Copy(BitConverter.GetBytes(z), 0, block, i, 8);
        i += 8;
        z = _Atime.ToFileTime();
        Array.Copy(BitConverter.GetBytes(z), 0, block, i, 8);
        i += 8;
        z = _Ctime.ToFileTime();
        Array.Copy(BitConverter.GetBytes(z), 0, block, i, 8);
        i += 8;
        listOfBlocks.Add(block);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502054uL);
      if (_ntfsTimesAreSet && _emitUnixTimes) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502055uL);
        int len = 5 + 4;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502056uL);
        if (!forCentralDirectory) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502057uL);
          len += 8;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502058uL);
        block = new byte[len];
        int i = 0;
        block[i++] = 0x55;
        block[i++] = 0x54;
        block[i++] = unchecked((byte)(len - 4));
        block[i++] = 0;
        block[i++] = 0x7;
        Int32 z = unchecked((int)((_Mtime - _unixEpoch).TotalSeconds));
        Array.Copy(BitConverter.GetBytes(z), 0, block, i, 4);
        i += 4;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502059uL);
        if (!forCentralDirectory) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502060uL);
          z = unchecked((int)((_Atime - _unixEpoch).TotalSeconds));
          Array.Copy(BitConverter.GetBytes(z), 0, block, i, 4);
          i += 4;
          z = unchecked((int)((_Ctime - _unixEpoch).TotalSeconds));
          Array.Copy(BitConverter.GetBytes(z), 0, block, i, 4);
          i += 4;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502061uL);
        listOfBlocks.Add(block);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502062uL);
      byte[] aggregateBlock = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502063uL);
      if (listOfBlocks.Count > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502064uL);
        int totalLength = 0;
        int i, current = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502065uL);
        for (i = 0; i < listOfBlocks.Count; i++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502066uL);
          totalLength += listOfBlocks[i].Length;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502067uL);
        aggregateBlock = new byte[totalLength];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502068uL);
        for (i = 0; i < listOfBlocks.Count; i++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502069uL);
          System.Array.Copy(listOfBlocks[i], 0, aggregateBlock, current, listOfBlocks[i].Length);
          current += listOfBlocks[i].Length;
        }
      }
      System.Byte[] RNTRNTRNT_314 = aggregateBlock;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502070uL);
      return RNTRNTRNT_314;
    }
    private string NormalizeFileName()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502071uL);
      string SlashFixed = FileName.Replace("\\", "/");
      string s1 = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502072uL);
      if ((_TrimVolumeFromFullyQualifiedPaths) && (FileName.Length >= 3) && (FileName[1] == ':') && (SlashFixed[2] == '/')) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502073uL);
        s1 = SlashFixed.Substring(3);
      } else if ((FileName.Length >= 4) && ((SlashFixed[0] == '/') && (SlashFixed[1] == '/'))) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502076uL);
        int n = SlashFixed.IndexOf('/', 2);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502077uL);
        if (n == -1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502078uL);
          throw new ArgumentException("The path for that entry appears to be badly formatted");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502079uL);
        s1 = SlashFixed.Substring(n + 1);
      } else if ((FileName.Length >= 3) && ((SlashFixed[0] == '.') && (SlashFixed[1] == '/'))) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502075uL);
        s1 = SlashFixed.Substring(2);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502074uL);
        s1 = SlashFixed;
      }
      System.String RNTRNTRNT_315 = s1;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502080uL);
      return RNTRNTRNT_315;
    }
    private byte[] GetEncodedFileNameBytes()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502081uL);
      var s1 = NormalizeFileName();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502082uL);
      switch (AlternateEncodingUsage) {
        case ZipOption.Always:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502087uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502083uL);
            if (!(_Comment == null || _Comment.Length == 0)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502084uL);
              _CommentBytes = AlternateEncoding.GetBytes(_Comment);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502085uL);
            _actualEncoding = AlternateEncoding;
            System.Byte[] RNTRNTRNT_316 = AlternateEncoding.GetBytes(s1);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502086uL);
            return RNTRNTRNT_316;
          }

        case ZipOption.Never:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502092uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502088uL);
            if (!(_Comment == null || _Comment.Length == 0)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502089uL);
              _CommentBytes = ibm437.GetBytes(_Comment);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502090uL);
            _actualEncoding = ibm437;
            System.Byte[] RNTRNTRNT_317 = ibm437.GetBytes(s1);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502091uL);
            return RNTRNTRNT_317;
          }

      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502093uL);
      byte[] result = ibm437.GetBytes(s1);
      string s2 = ibm437.GetString(result, 0, result.Length);
      _CommentBytes = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502094uL);
      if (s2 != s1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502095uL);
        result = AlternateEncoding.GetBytes(s1);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502096uL);
        if (_Comment != null && _Comment.Length != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502097uL);
          _CommentBytes = AlternateEncoding.GetBytes(_Comment);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502098uL);
        _actualEncoding = AlternateEncoding;
        System.Byte[] RNTRNTRNT_318 = result;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502099uL);
        return RNTRNTRNT_318;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502100uL);
      _actualEncoding = ibm437;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502101uL);
      if (_Comment == null || _Comment.Length == 0) {
        System.Byte[] RNTRNTRNT_319 = result;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502102uL);
        return RNTRNTRNT_319;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502103uL);
      byte[] cbytes = ibm437.GetBytes(_Comment);
      string c2 = ibm437.GetString(cbytes, 0, cbytes.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502104uL);
      if (c2 != Comment) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502105uL);
        result = AlternateEncoding.GetBytes(s1);
        _CommentBytes = AlternateEncoding.GetBytes(_Comment);
        _actualEncoding = AlternateEncoding;
        System.Byte[] RNTRNTRNT_320 = result;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502106uL);
        return RNTRNTRNT_320;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502107uL);
      _CommentBytes = cbytes;
      System.Byte[] RNTRNTRNT_321 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502108uL);
      return RNTRNTRNT_321;
    }
    private bool WantReadAgain()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502109uL);
      if (_UncompressedSize < 0x10) {
        System.Boolean RNTRNTRNT_322 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502110uL);
        return RNTRNTRNT_322;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502111uL);
      if (_CompressionMethod == 0x0) {
        System.Boolean RNTRNTRNT_323 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502112uL);
        return RNTRNTRNT_323;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502113uL);
      if (CompressionLevel == Ionic.Zlib.CompressionLevel.None) {
        System.Boolean RNTRNTRNT_324 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502114uL);
        return RNTRNTRNT_324;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502115uL);
      if (_CompressedSize < _UncompressedSize) {
        System.Boolean RNTRNTRNT_325 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502116uL);
        return RNTRNTRNT_325;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502117uL);
      if (this._Source == ZipEntrySource.Stream && !this._sourceStream.CanSeek) {
        System.Boolean RNTRNTRNT_326 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502118uL);
        return RNTRNTRNT_326;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502119uL);
      if (_aesCrypto_forWrite != null && (CompressedSize - _aesCrypto_forWrite.SizeOfEncryptionMetadata) <= UncompressedSize + 0x10) {
        System.Boolean RNTRNTRNT_327 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502120uL);
        return RNTRNTRNT_327;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502121uL);
      if (_zipCrypto_forWrite != null && (CompressedSize - 12) <= UncompressedSize) {
        System.Boolean RNTRNTRNT_328 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502122uL);
        return RNTRNTRNT_328;
      }
      System.Boolean RNTRNTRNT_329 = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502123uL);
      return RNTRNTRNT_329;
    }
    private void MaybeUnsetCompressionMethodForWriting(int cycle)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502124uL);
      if (cycle > 1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502125uL);
        _CompressionMethod = 0x0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502126uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502127uL);
      if (IsDirectory) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502128uL);
        _CompressionMethod = 0x0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502129uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502130uL);
      if (this._Source == ZipEntrySource.ZipFile) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502131uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502132uL);
      if (this._Source == ZipEntrySource.Stream) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502133uL);
        if (_sourceStream != null && _sourceStream.CanSeek) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502134uL);
          long fileLength = _sourceStream.Length;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502135uL);
          if (fileLength == 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502136uL);
            _CompressionMethod = 0x0;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502137uL);
            return;
          }
        }
      } else if ((this._Source == ZipEntrySource.FileSystem) && (SharedUtilities.GetFileLength(LocalFileName) == 0L)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502138uL);
        _CompressionMethod = 0x0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502139uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502140uL);
      if (SetCompression != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502141uL);
        CompressionLevel = SetCompression(LocalFileName, _FileNameInArchive);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502142uL);
      if (CompressionLevel == (short)Ionic.Zlib.CompressionLevel.None && CompressionMethod == Ionic.Zip.CompressionMethod.Deflate) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502143uL);
        _CompressionMethod = 0x0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502144uL);
      return;
    }
    internal void WriteHeader(Stream s, int cycle)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502145uL);
      var counter = s as CountingStream;
      _future_ROLH = (counter != null) ? counter.ComputedPosition : s.Position;
      int j = 0, i = 0;
      byte[] block = new byte[30];
      block[i++] = (byte)(ZipConstants.ZipEntrySignature & 0xff);
      block[i++] = (byte)((ZipConstants.ZipEntrySignature & 0xff00) >> 8);
      block[i++] = (byte)((ZipConstants.ZipEntrySignature & 0xff0000) >> 16);
      block[i++] = (byte)((ZipConstants.ZipEntrySignature & 0xff000000u) >> 24);
      _presumeZip64 = (_container.Zip64 == Zip64Option.Always || (_container.Zip64 == Zip64Option.AsNecessary && !s.CanSeek));
      Int16 VersionNeededToExtract = (Int16)(_presumeZip64 ? 45 : 20);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502146uL);
      if (this.CompressionMethod == Ionic.Zip.CompressionMethod.BZip2) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502147uL);
        VersionNeededToExtract = 46;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502148uL);
      block[i++] = (byte)(VersionNeededToExtract & 0xff);
      block[i++] = (byte)((VersionNeededToExtract & 0xff00) >> 8);
      byte[] fileNameBytes = GetEncodedFileNameBytes();
      Int16 filenameLength = (Int16)fileNameBytes.Length;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502149uL);
      if (_Encryption == EncryptionAlgorithm.None) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502150uL);
        _BitField &= ~1;
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502151uL);
        _BitField |= 1;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502152uL);
      if (_actualEncoding.CodePage == System.Text.Encoding.UTF8.CodePage) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502153uL);
        _BitField |= 0x800;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502154uL);
      if (IsDirectory || cycle == 99) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502155uL);
        _BitField &= ~0x8;
        _BitField &= ~0x1;
        Encryption = EncryptionAlgorithm.None;
        Password = null;
      } else if (!s.CanSeek) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502156uL);
        _BitField |= 0x8;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502157uL);
      block[i++] = (byte)(_BitField & 0xff);
      block[i++] = (byte)((_BitField & 0xff00) >> 8);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502158uL);
      if (this.__FileDataPosition == -1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502159uL);
        _CompressedSize = 0;
        _crcCalculated = false;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502160uL);
      MaybeUnsetCompressionMethodForWriting(cycle);
      block[i++] = (byte)(_CompressionMethod & 0xff);
      block[i++] = (byte)((_CompressionMethod & 0xff00) >> 8);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502161uL);
      if (cycle == 99) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502162uL);
        SetZip64Flags();
      } else if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502163uL);
        i -= 2;
        block[i++] = 0x63;
        block[i++] = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502164uL);
      _TimeBlob = Ionic.Zip.SharedUtilities.DateTimeToPacked(LastModified);
      block[i++] = (byte)(_TimeBlob & 0xff);
      block[i++] = (byte)((_TimeBlob & 0xff00) >> 8);
      block[i++] = (byte)((_TimeBlob & 0xff0000) >> 16);
      block[i++] = (byte)((_TimeBlob & 0xff000000u) >> 24);
      block[i++] = (byte)(_Crc32 & 0xff);
      block[i++] = (byte)((_Crc32 & 0xff00) >> 8);
      block[i++] = (byte)((_Crc32 & 0xff0000) >> 16);
      block[i++] = (byte)((_Crc32 & 0xff000000u) >> 24);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502165uL);
      if (_presumeZip64) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502166uL);
        for (j = 0; j < 8; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502167uL);
          block[i++] = 0xff;
        }
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502168uL);
        block[i++] = (byte)(_CompressedSize & 0xff);
        block[i++] = (byte)((_CompressedSize & 0xff00) >> 8);
        block[i++] = (byte)((_CompressedSize & 0xff0000) >> 16);
        block[i++] = (byte)((_CompressedSize & 0xff000000u) >> 24);
        block[i++] = (byte)(_UncompressedSize & 0xff);
        block[i++] = (byte)((_UncompressedSize & 0xff00) >> 8);
        block[i++] = (byte)((_UncompressedSize & 0xff0000) >> 16);
        block[i++] = (byte)((_UncompressedSize & 0xff000000u) >> 24);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502169uL);
      block[i++] = (byte)(filenameLength & 0xff);
      block[i++] = (byte)((filenameLength & 0xff00) >> 8);
      _Extra = ConstructExtraField(false);
      Int16 extraFieldLength = (Int16)((_Extra == null) ? 0 : _Extra.Length);
      block[i++] = (byte)(extraFieldLength & 0xff);
      block[i++] = (byte)((extraFieldLength & 0xff00) >> 8);
      byte[] bytes = new byte[i + filenameLength + extraFieldLength];
      Buffer.BlockCopy(block, 0, bytes, 0, i);
      Buffer.BlockCopy(fileNameBytes, 0, bytes, i, fileNameBytes.Length);
      i += fileNameBytes.Length;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502170uL);
      if (_Extra != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502171uL);
        Buffer.BlockCopy(_Extra, 0, bytes, i, _Extra.Length);
        i += _Extra.Length;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502172uL);
      _LengthOfHeader = i;
      var zss = s as ZipSegmentedStream;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502173uL);
      if (zss != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502174uL);
        zss.ContiguousWrite = true;
        UInt32 requiredSegment = zss.ComputeSegment(i);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502175uL);
        if (requiredSegment != zss.CurrentSegment) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502176uL);
          _future_ROLH = 0;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502177uL);
          _future_ROLH = zss.Position;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502178uL);
        _diskNumber = requiredSegment;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502179uL);
      if (_container.Zip64 == Zip64Option.Never && (uint)_RelativeOffsetOfLocalHeader >= 0xffffffffu) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502180uL);
        throw new ZipException("Offset within the zip archive exceeds 0xFFFFFFFF. Consider setting the UseZip64WhenSaving property on the ZipFile instance.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502181uL);
      s.Write(bytes, 0, i);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502182uL);
      if (zss != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502183uL);
        zss.ContiguousWrite = false;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502184uL);
      _EntryHeader = bytes;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502185uL);
    }
    private Int32 FigureCrc32()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502186uL);
      if (_crcCalculated == false) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502187uL);
        Stream input = null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502188uL);
        if (this._Source == ZipEntrySource.WriteDelegate) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502189uL);
          var output = new Ionic.Crc.CrcCalculatorStream(Stream.Null);
          this._WriteDelegate(this.FileName, output);
          _Crc32 = output.Crc;
        } else if (this._Source == ZipEntrySource.ZipFile) {
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502190uL);
          if (this._Source == ZipEntrySource.Stream) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502191uL);
            PrepSourceStream();
            input = this._sourceStream;
          } else if (this._Source == ZipEntrySource.JitStream) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502193uL);
            if (this._sourceStream == null) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502194uL);
              _sourceStream = this._OpenDelegate(this.FileName);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502195uL);
            PrepSourceStream();
            input = this._sourceStream;
          } else if (this._Source == ZipEntrySource.ZipOutputStream) {
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502192uL);
            input = File.Open(LocalFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502196uL);
          var crc32 = new Ionic.Crc.CRC32();
          _Crc32 = crc32.GetCrc32(input);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502197uL);
          if (_sourceStream == null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502198uL);
            input.Dispose();
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502199uL);
        _crcCalculated = true;
      }
      Int32 RNTRNTRNT_330 = _Crc32;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502200uL);
      return RNTRNTRNT_330;
    }
    private void PrepSourceStream()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502201uL);
      if (_sourceStream == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502202uL);
        throw new ZipException(String.Format("The input stream is null for entry '{0}'.", FileName));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502203uL);
      if (this._sourceStreamOriginalPosition != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502204uL);
        this._sourceStream.Position = this._sourceStreamOriginalPosition.Value;
      } else if (this._sourceStream.CanSeek) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502207uL);
        this._sourceStreamOriginalPosition = new Nullable<Int64>(this._sourceStream.Position);
      } else if (this.Encryption == EncryptionAlgorithm.PkzipWeak) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502205uL);
        if (this._Source != ZipEntrySource.ZipFile && ((this._BitField & 0x8) != 0x8)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502206uL);
          throw new ZipException("It is not possible to use PKZIP encryption on a non-seekable input stream");
        }
      }
    }
    internal void CopyMetaData(ZipEntry source)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502208uL);
      this.__FileDataPosition = source.__FileDataPosition;
      this.CompressionMethod = source.CompressionMethod;
      this._CompressionMethod_FromZipFile = source._CompressionMethod_FromZipFile;
      this._CompressedFileDataSize = source._CompressedFileDataSize;
      this._UncompressedSize = source._UncompressedSize;
      this._BitField = source._BitField;
      this._Source = source._Source;
      this._LastModified = source._LastModified;
      this._Mtime = source._Mtime;
      this._Atime = source._Atime;
      this._Ctime = source._Ctime;
      this._ntfsTimesAreSet = source._ntfsTimesAreSet;
      this._emitUnixTimes = source._emitUnixTimes;
      this._emitNtfsTimes = source._emitNtfsTimes;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502209uL);
    }
    private void OnWriteBlock(Int64 bytesXferred, Int64 totalBytesToXfer)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502210uL);
      if (_container.ZipFile != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502211uL);
        _ioOperationCanceled = _container.ZipFile.OnSaveBlock(this, bytesXferred, totalBytesToXfer);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502212uL);
    }
    private void _WriteEntryData(Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502213uL);
      Stream input = null;
      long fdp = -1L;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502214uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502215uL);
        fdp = s.Position;
      } catch (Exception) {
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502216uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502217uL);
        long fileLength = SetInputAndFigureFileLength(ref input);
        CountingStream entryCounter = new CountingStream(s);
        Stream encryptor;
        Stream compressor;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502218uL);
        if (fileLength != 0L) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502219uL);
          encryptor = MaybeApplyEncryption(entryCounter);
          compressor = MaybeApplyCompression(encryptor, fileLength);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502220uL);
          encryptor = compressor = entryCounter;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502221uL);
        var output = new Ionic.Crc.CrcCalculatorStream(compressor, true);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502222uL);
        if (this._Source == ZipEntrySource.WriteDelegate) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502223uL);
          this._WriteDelegate(this.FileName, output);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502224uL);
          byte[] buffer = new byte[BufferSize];
          int n;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502225uL);
          while ((n = SharedUtilities.ReadWithRetry(input, buffer, 0, buffer.Length, FileName)) != 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502226uL);
            output.Write(buffer, 0, n);
            OnWriteBlock(output.TotalBytesSlurped, fileLength);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502227uL);
            if (_ioOperationCanceled) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502228uL);
              break;
            }
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502229uL);
        FinishOutputStream(s, entryCounter, encryptor, compressor, output);
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502230uL);
        if (this._Source == ZipEntrySource.JitStream) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502231uL);
          if (this._CloseDelegate != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502232uL);
            this._CloseDelegate(this.FileName, input);
          }
        } else if ((input as FileStream) != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502233uL);
          input.Dispose();
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502234uL);
      if (_ioOperationCanceled) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502235uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502236uL);
      this.__FileDataPosition = fdp;
      PostProcessOutput(s);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502237uL);
    }
    private long SetInputAndFigureFileLength(ref Stream input)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502238uL);
      long fileLength = -1L;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502239uL);
      if (this._Source == ZipEntrySource.Stream) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502240uL);
        PrepSourceStream();
        input = this._sourceStream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502241uL);
        try {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502242uL);
          fileLength = this._sourceStream.Length;
        } catch (NotSupportedException) {
        }
      } else if (this._Source == ZipEntrySource.ZipFile) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502249uL);
        string pwd = (_Encryption_FromZipFile == EncryptionAlgorithm.None) ? null : (this._Password ?? this._container.Password);
        this._sourceStream = InternalOpenReader(pwd);
        PrepSourceStream();
        input = this._sourceStream;
        fileLength = this._sourceStream.Length;
      } else if (this._Source == ZipEntrySource.JitStream) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502244uL);
        if (this._sourceStream == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502245uL);
          _sourceStream = this._OpenDelegate(this.FileName);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502246uL);
        PrepSourceStream();
        input = this._sourceStream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502247uL);
        try {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502248uL);
          fileLength = this._sourceStream.Length;
        } catch (NotSupportedException) {
        }
      } else if (this._Source == ZipEntrySource.FileSystem) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502243uL);
        FileShare fs = FileShare.ReadWrite;
        fs |= FileShare.Delete;
        input = File.Open(LocalFileName, FileMode.Open, FileAccess.Read, fs);
        fileLength = input.Length;
      }
      System.Int64 RNTRNTRNT_331 = fileLength;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502250uL);
      return RNTRNTRNT_331;
    }
    internal void FinishOutputStream(Stream s, CountingStream entryCounter, Stream encryptor, Stream compressor, Ionic.Crc.CrcCalculatorStream output)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502251uL);
      if (output == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502252uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502253uL);
      output.Close();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502254uL);
      if ((compressor as Ionic.Zlib.DeflateStream) != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502255uL);
        compressor.Close();
      } else if ((compressor as Ionic.BZip2.BZip2OutputStream) != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502258uL);
        compressor.Close();
      } else if ((compressor as Ionic.BZip2.ParallelBZip2OutputStream) != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502257uL);
        compressor.Close();
      } else if ((compressor as Ionic.Zlib.ParallelDeflateOutputStream) != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502256uL);
        compressor.Close();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502259uL);
      encryptor.Flush();
      encryptor.Close();
      _LengthOfTrailer = 0;
      _UncompressedSize = output.TotalBytesSlurped;
      WinZipAesCipherStream wzacs = encryptor as WinZipAesCipherStream;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502260uL);
      if (wzacs != null && _UncompressedSize > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502261uL);
        s.Write(wzacs.FinalAuthentication, 0, 10);
        _LengthOfTrailer += 10;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502262uL);
      _CompressedFileDataSize = entryCounter.BytesWritten;
      _CompressedSize = _CompressedFileDataSize;
      _Crc32 = output.Crc;
      StoreRelativeOffset();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502263uL);
    }
    internal void PostProcessOutput(Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502264uL);
      var s1 = s as CountingStream;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502265uL);
      if (_UncompressedSize == 0 && _CompressedSize == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502266uL);
        if (this._Source == ZipEntrySource.ZipOutputStream) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502267uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502268uL);
        if (_Password != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502269uL);
          int headerBytesToRetract = 0;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502270uL);
          if (Encryption == EncryptionAlgorithm.PkzipWeak) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502271uL);
            headerBytesToRetract = 12;
          } else if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502272uL);
            headerBytesToRetract = _aesCrypto_forWrite._Salt.Length + _aesCrypto_forWrite.GeneratedPV.Length;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502273uL);
          if (this._Source == ZipEntrySource.ZipOutputStream && !s.CanSeek) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502274uL);
            throw new ZipException("Zero bytes written, encryption in use, and non-seekable output.");
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502275uL);
          if (Encryption != EncryptionAlgorithm.None) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502276uL);
            s.Seek(-1 * headerBytesToRetract, SeekOrigin.Current);
            s.SetLength(s.Position);
            Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502277uL);
            if (s1 != null) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502278uL);
              s1.Adjust(headerBytesToRetract);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502279uL);
            _LengthOfHeader -= headerBytesToRetract;
            __FileDataPosition -= headerBytesToRetract;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502280uL);
          _Password = null;
          _BitField &= ~(0x1);
          int j = 6;
          _EntryHeader[j++] = (byte)(_BitField & 0xff);
          _EntryHeader[j++] = (byte)((_BitField & 0xff00) >> 8);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502281uL);
          if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502282uL);
            Int16 fnLength = (short)(_EntryHeader[26] + _EntryHeader[27] * 256);
            int offx = 30 + fnLength;
            int aesIndex = FindExtraFieldSegment(_EntryHeader, offx, 0x9901);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502283uL);
            if (aesIndex >= 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502284uL);
              _EntryHeader[aesIndex++] = 0x99;
              _EntryHeader[aesIndex++] = 0x99;
            }
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502285uL);
        CompressionMethod = 0;
        Encryption = EncryptionAlgorithm.None;
      } else if (_zipCrypto_forWrite != null || _aesCrypto_forWrite != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502286uL);
        if (Encryption == EncryptionAlgorithm.PkzipWeak) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502287uL);
          _CompressedSize += 12;
        } else if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502288uL);
          _CompressedSize += _aesCrypto_forWrite.SizeOfEncryptionMetadata;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502289uL);
      int i = 8;
      _EntryHeader[i++] = (byte)(_CompressionMethod & 0xff);
      _EntryHeader[i++] = (byte)((_CompressionMethod & 0xff00) >> 8);
      i = 14;
      _EntryHeader[i++] = (byte)(_Crc32 & 0xff);
      _EntryHeader[i++] = (byte)((_Crc32 & 0xff00) >> 8);
      _EntryHeader[i++] = (byte)((_Crc32 & 0xff0000) >> 16);
      _EntryHeader[i++] = (byte)((_Crc32 & 0xff000000u) >> 24);
      SetZip64Flags();
      Int16 filenameLength = (short)(_EntryHeader[26] + _EntryHeader[27] * 256);
      Int16 extraFieldLength = (short)(_EntryHeader[28] + _EntryHeader[29] * 256);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502290uL);
      if (_OutputUsesZip64.Value) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502291uL);
        _EntryHeader[4] = (byte)(45 & 0xff);
        _EntryHeader[5] = 0x0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502292uL);
        for (int j = 0; j < 8; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502293uL);
          _EntryHeader[i++] = 0xff;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502294uL);
        i = 30 + filenameLength;
        _EntryHeader[i++] = 0x1;
        _EntryHeader[i++] = 0x0;
        i += 2;
        Array.Copy(BitConverter.GetBytes(_UncompressedSize), 0, _EntryHeader, i, 8);
        i += 8;
        Array.Copy(BitConverter.GetBytes(_CompressedSize), 0, _EntryHeader, i, 8);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502295uL);
        _EntryHeader[4] = (byte)(20 & 0xff);
        _EntryHeader[5] = 0x0;
        i = 18;
        _EntryHeader[i++] = (byte)(_CompressedSize & 0xff);
        _EntryHeader[i++] = (byte)((_CompressedSize & 0xff00) >> 8);
        _EntryHeader[i++] = (byte)((_CompressedSize & 0xff0000) >> 16);
        _EntryHeader[i++] = (byte)((_CompressedSize & 0xff000000u) >> 24);
        _EntryHeader[i++] = (byte)(_UncompressedSize & 0xff);
        _EntryHeader[i++] = (byte)((_UncompressedSize & 0xff00) >> 8);
        _EntryHeader[i++] = (byte)((_UncompressedSize & 0xff0000) >> 16);
        _EntryHeader[i++] = (byte)((_UncompressedSize & 0xff000000u) >> 24);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502296uL);
        if (extraFieldLength != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502297uL);
          i = 30 + filenameLength;
          Int16 DataSize = (short)(_EntryHeader[i + 2] + _EntryHeader[i + 3] * 256);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502298uL);
          if (DataSize == 16) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502299uL);
            _EntryHeader[i++] = 0x99;
            _EntryHeader[i++] = 0x99;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502300uL);
      if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502301uL);
        i = 8;
        _EntryHeader[i++] = 0x63;
        _EntryHeader[i++] = 0;
        i = 30 + filenameLength;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502302uL);
        do {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502303uL);
          UInt16 HeaderId = (UInt16)(_EntryHeader[i] + _EntryHeader[i + 1] * 256);
          Int16 DataSize = (short)(_EntryHeader[i + 2] + _EntryHeader[i + 3] * 256);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502304uL);
          if (HeaderId != 0x9901) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502305uL);
            i += DataSize + 4;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502306uL);
            i += 9;
            _EntryHeader[i++] = (byte)(_CompressionMethod & 0xff);
            _EntryHeader[i++] = (byte)(_CompressionMethod & 0xff00);
          }
        } while (i < (extraFieldLength - 30 - filenameLength));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502307uL);
      if ((_BitField & 0x8) != 0x8 || (this._Source == ZipEntrySource.ZipOutputStream && s.CanSeek)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502308uL);
        var zss = s as ZipSegmentedStream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502309uL);
        if (zss != null && _diskNumber != zss.CurrentSegment) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502310uL);
          using (Stream hseg = ZipSegmentedStream.ForUpdate(this._container.ZipFile.Name, _diskNumber)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502311uL);
            hseg.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
            hseg.Write(_EntryHeader, 0, _EntryHeader.Length);
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502312uL);
          s.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
          s.Write(_EntryHeader, 0, _EntryHeader.Length);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502313uL);
          if (s1 != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502314uL);
            s1.Adjust(_EntryHeader.Length);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502315uL);
          s.Seek(_CompressedSize, SeekOrigin.Current);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502316uL);
      if (((_BitField & 0x8) == 0x8) && !IsDirectory) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502317uL);
        byte[] Descriptor = new byte[16 + (_OutputUsesZip64.Value ? 8 : 0)];
        i = 0;
        Array.Copy(BitConverter.GetBytes(ZipConstants.ZipEntryDataDescriptorSignature), 0, Descriptor, i, 4);
        i += 4;
        Array.Copy(BitConverter.GetBytes(_Crc32), 0, Descriptor, i, 4);
        i += 4;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502318uL);
        if (_OutputUsesZip64.Value) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502319uL);
          Array.Copy(BitConverter.GetBytes(_CompressedSize), 0, Descriptor, i, 8);
          i += 8;
          Array.Copy(BitConverter.GetBytes(_UncompressedSize), 0, Descriptor, i, 8);
          i += 8;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502320uL);
          Descriptor[i++] = (byte)(_CompressedSize & 0xff);
          Descriptor[i++] = (byte)((_CompressedSize & 0xff00) >> 8);
          Descriptor[i++] = (byte)((_CompressedSize & 0xff0000) >> 16);
          Descriptor[i++] = (byte)((_CompressedSize & 0xff000000u) >> 24);
          Descriptor[i++] = (byte)(_UncompressedSize & 0xff);
          Descriptor[i++] = (byte)((_UncompressedSize & 0xff00) >> 8);
          Descriptor[i++] = (byte)((_UncompressedSize & 0xff0000) >> 16);
          Descriptor[i++] = (byte)((_UncompressedSize & 0xff000000u) >> 24);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502321uL);
        s.Write(Descriptor, 0, Descriptor.Length);
        _LengthOfTrailer += Descriptor.Length;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502322uL);
    }
    private void SetZip64Flags()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502323uL);
      _entryRequiresZip64 = new Nullable<bool>(_CompressedSize >= 0xffffffffu || _UncompressedSize >= 0xffffffffu || _RelativeOffsetOfLocalHeader >= 0xffffffffu);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502324uL);
      if (_container.Zip64 == Zip64Option.Never && _entryRequiresZip64.Value) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502325uL);
        throw new ZipException("Compressed or Uncompressed size, or offset exceeds the maximum value. Consider setting the UseZip64WhenSaving property on the ZipFile instance.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502326uL);
      _OutputUsesZip64 = new Nullable<bool>(_container.Zip64 == Zip64Option.Always || _entryRequiresZip64.Value);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502327uL);
    }
    internal void PrepOutputStream(Stream s, long streamLength, out CountingStream outputCounter, out Stream encryptor, out Stream compressor, out Ionic.Crc.CrcCalculatorStream output)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502328uL);
      TraceWriteLine("PrepOutputStream: e({0}) comp({1}) crypto({2}) zf({3})", FileName, CompressionLevel, Encryption, _container.Name);
      outputCounter = new CountingStream(s);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502329uL);
      if (streamLength != 0L) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502330uL);
        encryptor = MaybeApplyEncryption(outputCounter);
        compressor = MaybeApplyCompression(encryptor, streamLength);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502331uL);
        encryptor = compressor = outputCounter;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502332uL);
      output = new Ionic.Crc.CrcCalculatorStream(compressor, true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502333uL);
    }
    private Stream MaybeApplyCompression(Stream s, long streamLength)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502334uL);
      if (_CompressionMethod == 0x8 && CompressionLevel != Ionic.Zlib.CompressionLevel.None) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502335uL);
        if (_container.ParallelDeflateThreshold == 0L || (streamLength > _container.ParallelDeflateThreshold && _container.ParallelDeflateThreshold > 0L)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502336uL);
          if (_container.ParallelDeflater == null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502337uL);
            _container.ParallelDeflater = new Ionic.Zlib.ParallelDeflateOutputStream(s, CompressionLevel, _container.Strategy, true);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502338uL);
            if (_container.CodecBufferSize > 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502339uL);
              _container.ParallelDeflater.BufferSize = _container.CodecBufferSize;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502340uL);
            if (_container.ParallelDeflateMaxBufferPairs > 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502341uL);
              _container.ParallelDeflater.MaxBufferPairs = _container.ParallelDeflateMaxBufferPairs;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502342uL);
          Ionic.Zlib.ParallelDeflateOutputStream o1 = _container.ParallelDeflater;
          o1.Reset(s);
          Stream RNTRNTRNT_332 = o1;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502343uL);
          return RNTRNTRNT_332;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502344uL);
        var o = new Ionic.Zlib.DeflateStream(s, Ionic.Zlib.CompressionMode.Compress, CompressionLevel, true);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502345uL);
        if (_container.CodecBufferSize > 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502346uL);
          o.BufferSize = _container.CodecBufferSize;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502347uL);
        o.Strategy = _container.Strategy;
        Stream RNTRNTRNT_333 = o;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502348uL);
        return RNTRNTRNT_333;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502349uL);
      if (_CompressionMethod == 0xc) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502350uL);
        if (_container.ParallelDeflateThreshold == 0L || (streamLength > _container.ParallelDeflateThreshold && _container.ParallelDeflateThreshold > 0L)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502351uL);
          var o1 = new Ionic.BZip2.ParallelBZip2OutputStream(s, true);
          Stream RNTRNTRNT_334 = o1;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502352uL);
          return RNTRNTRNT_334;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502353uL);
        var o = new Ionic.BZip2.BZip2OutputStream(s, true);
        Stream RNTRNTRNT_335 = o;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502354uL);
        return RNTRNTRNT_335;
      }
      Stream RNTRNTRNT_336 = s;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502355uL);
      return RNTRNTRNT_336;
    }
    private Stream MaybeApplyEncryption(Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502356uL);
      if (Encryption == EncryptionAlgorithm.PkzipWeak) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502357uL);
        TraceWriteLine("MaybeApplyEncryption: e({0}) PKZIP", FileName);
        Stream RNTRNTRNT_337 = new ZipCipherStream(s, _zipCrypto_forWrite, CryptoMode.Encrypt);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502358uL);
        return RNTRNTRNT_337;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502359uL);
      if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502360uL);
        TraceWriteLine("MaybeApplyEncryption: e({0}) AES", FileName);
        Stream RNTRNTRNT_338 = new WinZipAesCipherStream(s, _aesCrypto_forWrite, CryptoMode.Encrypt);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502361uL);
        return RNTRNTRNT_338;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502362uL);
      TraceWriteLine("MaybeApplyEncryption: e({0}) None", FileName);
      Stream RNTRNTRNT_339 = s;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502363uL);
      return RNTRNTRNT_339;
    }
    private void OnZipErrorWhileSaving(Exception e)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502364uL);
      if (_container.ZipFile != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502365uL);
        _ioOperationCanceled = _container.ZipFile.OnZipErrorSaving(this, e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502366uL);
    }
    internal void Write(Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502367uL);
      var cs1 = s as CountingStream;
      var zss1 = s as ZipSegmentedStream;
      bool done = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502368uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502369uL);
        try {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502370uL);
          if (_Source == ZipEntrySource.ZipFile && !_restreamRequiredOnSave) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502371uL);
            CopyThroughOneEntry(s);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502372uL);
            return;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502373uL);
          if (IsDirectory) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502374uL);
            WriteHeader(s, 1);
            StoreRelativeOffset();
            _entryRequiresZip64 = new Nullable<bool>(_RelativeOffsetOfLocalHeader >= 0xffffffffu);
            _OutputUsesZip64 = new Nullable<bool>(_container.Zip64 == Zip64Option.Always || _entryRequiresZip64.Value);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502375uL);
            if (zss1 != null) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502376uL);
              _diskNumber = zss1.CurrentSegment;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502377uL);
            return;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502378uL);
          bool readAgain = true;
          int nCycles = 0;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502379uL);
          do {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502380uL);
            nCycles++;
            WriteHeader(s, nCycles);
            WriteSecurityMetadata(s);
            _WriteEntryData(s);
            _TotalEntrySize = _LengthOfHeader + _CompressedFileDataSize + _LengthOfTrailer;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502381uL);
            if (nCycles > 1) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502382uL);
              readAgain = false;
            } else if (!s.CanSeek) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502384uL);
              readAgain = false;
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502383uL);
              readAgain = WantReadAgain();
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502385uL);
            if (readAgain) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502386uL);
              if (zss1 != null) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502387uL);
                zss1.TruncateBackward(_diskNumber, _RelativeOffsetOfLocalHeader);
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502388uL);
                s.Seek(_RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502389uL);
              s.SetLength(s.Position);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502390uL);
              if (cs1 != null) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502391uL);
                cs1.Adjust(_TotalEntrySize);
              }
            }
          } while (readAgain);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502392uL);
          _skippedDuringSave = false;
          done = true;
        } catch (System.Exception exc1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502393uL);
          ZipErrorAction orig = this.ZipErrorAction;
          int loop = 0;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502394uL);
          do {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502395uL);
            if (ZipErrorAction == ZipErrorAction.Throw) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502396uL);
              throw;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502397uL);
            if (ZipErrorAction == ZipErrorAction.Skip || ZipErrorAction == ZipErrorAction.Retry) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502398uL);
              long p1 = (cs1 != null) ? cs1.ComputedPosition : s.Position;
              long delta = p1 - _future_ROLH;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502399uL);
              if (delta > 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502400uL);
                s.Seek(delta, SeekOrigin.Current);
                long p2 = s.Position;
                s.SetLength(s.Position);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502401uL);
                if (cs1 != null) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502402uL);
                  cs1.Adjust(p1 - p2);
                }
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502403uL);
              if (ZipErrorAction == ZipErrorAction.Skip) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502404uL);
                WriteStatus("Skipping file {0} (exception: {1})", LocalFileName, exc1.ToString());
                _skippedDuringSave = true;
                done = true;
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502405uL);
                this.ZipErrorAction = orig;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502406uL);
              break;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502407uL);
            if (loop > 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502408uL);
              throw;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502409uL);
            if (ZipErrorAction == ZipErrorAction.InvokeErrorEvent) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502410uL);
              OnZipErrorWhileSaving(exc1);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502411uL);
              if (_ioOperationCanceled) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502412uL);
                done = true;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502413uL);
                break;
              }
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502414uL);
            loop++;
          } while (true);
        }
      } while (!done);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502415uL);
    }
    internal void StoreRelativeOffset()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502416uL);
      _RelativeOffsetOfLocalHeader = _future_ROLH;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502417uL);
    }
    internal void NotifySaveComplete()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502418uL);
      _Encryption_FromZipFile = _Encryption;
      _CompressionMethod_FromZipFile = _CompressionMethod;
      _restreamRequiredOnSave = false;
      _metadataChanged = false;
      _Source = ZipEntrySource.ZipFile;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502419uL);
    }
    internal void WriteSecurityMetadata(Stream outstream)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502420uL);
      if (Encryption == EncryptionAlgorithm.None) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502421uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502422uL);
      string pwd = this._Password;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502423uL);
      if (this._Source == ZipEntrySource.ZipFile && pwd == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502424uL);
        pwd = this._container.Password;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502425uL);
      if (pwd == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502426uL);
        _zipCrypto_forWrite = null;
        _aesCrypto_forWrite = null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502427uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502428uL);
      TraceWriteLine("WriteSecurityMetadata: e({0}) crypto({1}) pw({2})", FileName, Encryption.ToString(), pwd);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502429uL);
      if (Encryption == EncryptionAlgorithm.PkzipWeak) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502430uL);
        _zipCrypto_forWrite = ZipCrypto.ForWrite(pwd);
        var rnd = new System.Random();
        byte[] encryptionHeader = new byte[12];
        rnd.NextBytes(encryptionHeader);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502431uL);
        if ((this._BitField & 0x8) == 0x8) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502432uL);
          _TimeBlob = Ionic.Zip.SharedUtilities.DateTimeToPacked(LastModified);
          encryptionHeader[11] = (byte)((this._TimeBlob >> 8) & 0xff);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502433uL);
          FigureCrc32();
          encryptionHeader[11] = (byte)((this._Crc32 >> 24) & 0xff);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502434uL);
        byte[] cipherText = _zipCrypto_forWrite.EncryptMessage(encryptionHeader, encryptionHeader.Length);
        outstream.Write(cipherText, 0, cipherText.Length);
        _LengthOfHeader += cipherText.Length;
      } else if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502435uL);
        int keystrength = GetKeyStrengthInBits(Encryption);
        _aesCrypto_forWrite = WinZipAesCrypto.Generate(pwd, keystrength);
        outstream.Write(_aesCrypto_forWrite.Salt, 0, _aesCrypto_forWrite._Salt.Length);
        outstream.Write(_aesCrypto_forWrite.GeneratedPV, 0, _aesCrypto_forWrite.GeneratedPV.Length);
        _LengthOfHeader += _aesCrypto_forWrite._Salt.Length + _aesCrypto_forWrite.GeneratedPV.Length;
        TraceWriteLine("WriteSecurityMetadata: AES e({0}) keybits({1}) _LOH({2})", FileName, keystrength, _LengthOfHeader);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502436uL);
    }
    private void CopyThroughOneEntry(Stream outStream)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502437uL);
      if (this.LengthOfHeader == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502438uL);
        throw new BadStateException("Bad header length.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502439uL);
      bool needRecompute = _metadataChanged || (this.ArchiveStream is ZipSegmentedStream) || (outStream is ZipSegmentedStream) || (_InputUsesZip64 && _container.UseZip64WhenSaving == Zip64Option.Never) || (!_InputUsesZip64 && _container.UseZip64WhenSaving == Zip64Option.Always);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502440uL);
      if (needRecompute) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502441uL);
        CopyThroughWithRecompute(outStream);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502442uL);
        CopyThroughWithNoChange(outStream);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502443uL);
      _entryRequiresZip64 = new Nullable<bool>(_CompressedSize >= 0xffffffffu || _UncompressedSize >= 0xffffffffu || _RelativeOffsetOfLocalHeader >= 0xffffffffu);
      _OutputUsesZip64 = new Nullable<bool>(_container.Zip64 == Zip64Option.Always || _entryRequiresZip64.Value);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502444uL);
    }
    private void CopyThroughWithRecompute(Stream outstream)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502445uL);
      int n;
      byte[] bytes = new byte[BufferSize];
      var input = new CountingStream(this.ArchiveStream);
      long origRelativeOffsetOfHeader = _RelativeOffsetOfLocalHeader;
      int origLengthOfHeader = LengthOfHeader;
      WriteHeader(outstream, 0);
      StoreRelativeOffset();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502446uL);
      if (!this.FileName.EndsWith("/")) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502447uL);
        long pos = origRelativeOffsetOfHeader + origLengthOfHeader;
        int len = GetLengthOfCryptoHeaderBytes(_Encryption_FromZipFile);
        pos -= len;
        _LengthOfHeader += len;
        input.Seek(pos, SeekOrigin.Begin);
        long remaining = this._CompressedSize;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502448uL);
        while (remaining > 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502449uL);
          len = (remaining > bytes.Length) ? bytes.Length : (int)remaining;
          n = input.Read(bytes, 0, len);
          outstream.Write(bytes, 0, n);
          remaining -= n;
          OnWriteBlock(input.BytesRead, this._CompressedSize);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502450uL);
          if (_ioOperationCanceled) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502451uL);
            break;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502452uL);
        if ((this._BitField & 0x8) == 0x8) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502453uL);
          int size = 16;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502454uL);
          if (_InputUsesZip64) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502455uL);
            size += 8;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502456uL);
          byte[] Descriptor = new byte[size];
          input.Read(Descriptor, 0, size);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502457uL);
          if (_InputUsesZip64 && _container.UseZip64WhenSaving == Zip64Option.Never) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502458uL);
            outstream.Write(Descriptor, 0, 8);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502459uL);
            if (_CompressedSize > 0xffffffffu) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502460uL);
              throw new InvalidOperationException("ZIP64 is required");
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502461uL);
            outstream.Write(Descriptor, 8, 4);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502462uL);
            if (_UncompressedSize > 0xffffffffu) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502463uL);
              throw new InvalidOperationException("ZIP64 is required");
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502464uL);
            outstream.Write(Descriptor, 16, 4);
            _LengthOfTrailer -= 8;
          } else if (!_InputUsesZip64 && _container.UseZip64WhenSaving == Zip64Option.Always) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502466uL);
            byte[] pad = new byte[4];
            outstream.Write(Descriptor, 0, 8);
            outstream.Write(Descriptor, 8, 4);
            outstream.Write(pad, 0, 4);
            outstream.Write(Descriptor, 12, 4);
            outstream.Write(pad, 0, 4);
            _LengthOfTrailer += 8;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502465uL);
            outstream.Write(Descriptor, 0, size);
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502467uL);
      _TotalEntrySize = _LengthOfHeader + _CompressedFileDataSize + _LengthOfTrailer;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502468uL);
    }
    private void CopyThroughWithNoChange(Stream outstream)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502469uL);
      int n;
      byte[] bytes = new byte[BufferSize];
      var input = new CountingStream(this.ArchiveStream);
      input.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502470uL);
      if (this._TotalEntrySize == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502471uL);
        this._TotalEntrySize = this._LengthOfHeader + this._CompressedFileDataSize + _LengthOfTrailer;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502472uL);
      var counter = outstream as CountingStream;
      _RelativeOffsetOfLocalHeader = (counter != null) ? counter.ComputedPosition : outstream.Position;
      long remaining = this._TotalEntrySize;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502473uL);
      while (remaining > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502474uL);
        int len = (remaining > bytes.Length) ? bytes.Length : (int)remaining;
        n = input.Read(bytes, 0, len);
        outstream.Write(bytes, 0, n);
        remaining -= n;
        OnWriteBlock(input.BytesRead, this._TotalEntrySize);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502475uL);
        if (_ioOperationCanceled) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502476uL);
          break;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502477uL);
    }
    [System.Diagnostics.ConditionalAttribute("Trace")]
    private void TraceWriteLine(string format, params object[] varParams)
    {
      lock (_outputLock) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502478uL);
        int tid = System.Threading.Thread.CurrentThread.GetHashCode();
        Console.ForegroundColor = (ConsoleColor)(tid % 8 + 8);
        Console.Write("{0:000} ZipEntry.Write ", tid);
        Console.WriteLine(format, varParams);
        Console.ResetColor();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502479uL);
    }
    private object _outputLock = new Object();
  }
}
