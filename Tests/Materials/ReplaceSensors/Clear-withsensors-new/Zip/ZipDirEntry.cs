using System;
using System.Collections.Generic;
namespace Ionic.Zip
{
  partial class ZipEntry
  {
    internal bool AttributesIndicateDirectory {
      get {
        System.Boolean RNTRNTRNT_222 = ((_InternalFileAttrs == 0) && ((_ExternalFileAttrs & 0x10) == 0x10));
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501269uL);
        return RNTRNTRNT_222;
      }
    }
    internal void ResetDirEntry()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501270uL);
      this.__FileDataPosition = -1;
      this._LengthOfHeader = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501271uL);
    }
    public string Info {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501272uL);
        var builder = new System.Text.StringBuilder();
        builder.Append(string.Format("          ZipEntry: {0}\n", this.FileName)).Append(string.Format("   Version Made By: {0}\n", this._VersionMadeBy)).Append(string.Format(" Needed to extract: {0}\n", this.VersionNeeded));
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501273uL);
        if (this._IsDirectory) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501274uL);
          builder.Append("        Entry type: directory\n");
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501275uL);
          builder.Append(string.Format("         File type: {0}\n", this._IsText ? "text" : "binary")).Append(string.Format("       Compression: {0}\n", this.CompressionMethod)).Append(string.Format("        Compressed: 0x{0:X}\n", this.CompressedSize)).Append(string.Format("      Uncompressed: 0x{0:X}\n", this.UncompressedSize)).Append(string.Format("             CRC32: 0x{0:X8}\n", this._Crc32));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501276uL);
        builder.Append(string.Format("       Disk Number: {0}\n", this._diskNumber));
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501277uL);
        if (this._RelativeOffsetOfLocalHeader > 0xffffffffu) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501278uL);
          builder.Append(string.Format("   Relative Offset: 0x{0:X16}\n", this._RelativeOffsetOfLocalHeader));
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501279uL);
          builder.Append(string.Format("   Relative Offset: 0x{0:X8}\n", this._RelativeOffsetOfLocalHeader));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501280uL);
        builder.Append(string.Format("         Bit Field: 0x{0:X4}\n", this._BitField)).Append(string.Format("        Encrypted?: {0}\n", this._sourceIsEncrypted)).Append(string.Format("          Timeblob: 0x{0:X8}\n", this._TimeBlob)).Append(string.Format("              Time: {0}\n", Ionic.Zip.SharedUtilities.PackedToDateTime(this._TimeBlob)));
        builder.Append(string.Format("         Is Zip64?: {0}\n", this._InputUsesZip64));
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501281uL);
        if (!string.IsNullOrEmpty(this._Comment)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501282uL);
          builder.Append(string.Format("           Comment: {0}\n", this._Comment));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501283uL);
        builder.Append("\n");
        System.String RNTRNTRNT_223 = builder.ToString();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501284uL);
        return RNTRNTRNT_223;
      }
    }
    private class CopyHelper
    {
      private static System.Text.RegularExpressions.Regex re = new System.Text.RegularExpressions.Regex(" \\(copy (\\d+)\\)$");
      private static int callCount = 0;
      static internal string AppendCopyToFileName(string f)
      {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501285uL);
        callCount++;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501286uL);
        if (callCount > 25) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501287uL);
          throw new OverflowException("overflow while creating filename");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501288uL);
        int n = 1;
        int r = f.LastIndexOf(".");
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501289uL);
        if (r == -1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501290uL);
          System.Text.RegularExpressions.Match m = re.Match(f);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501291uL);
          if (m.Success) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501292uL);
            n = Int32.Parse(m.Groups[1].Value) + 1;
            string copy = String.Format(" (copy {0})", n);
            f = f.Substring(0, m.Index) + copy;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501293uL);
            string copy = String.Format(" (copy {0})", n);
            f = f + copy;
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501294uL);
          System.Text.RegularExpressions.Match m = re.Match(f.Substring(0, r));
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501295uL);
          if (m.Success) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501296uL);
            n = Int32.Parse(m.Groups[1].Value) + 1;
            string copy = String.Format(" (copy {0})", n);
            f = f.Substring(0, m.Index) + copy + f.Substring(r);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501297uL);
            string copy = String.Format(" (copy {0})", n);
            f = f.Substring(0, r) + copy + f.Substring(r);
          }
        }
        System.String RNTRNTRNT_224 = f;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501298uL);
        return RNTRNTRNT_224;
      }
    }
    static internal ZipEntry ReadDirEntry(ZipFile zf, Dictionary<String, Object> previouslySeen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501299uL);
      System.IO.Stream s = zf.ReadStream;
      System.Text.Encoding expectedEncoding = (zf.AlternateEncodingUsage == ZipOption.Always) ? zf.AlternateEncoding : ZipFile.DefaultEncoding;
      int signature = Ionic.Zip.SharedUtilities.ReadSignature(s);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501300uL);
      if (IsNotValidZipDirEntrySig(signature)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501301uL);
        s.Seek(-4, System.IO.SeekOrigin.Current);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501302uL);
        if (signature != ZipConstants.EndOfCentralDirectorySignature && signature != ZipConstants.Zip64EndOfCentralDirectoryRecordSignature && signature != ZipConstants.ZipEntrySignature) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501303uL);
          throw new BadReadException(String.Format("  Bad signature (0x{0:X8}) at position 0x{1:X8}", signature, s.Position));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501304uL);
        return null;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501305uL);
      int bytesRead = 42 + 4;
      byte[] block = new byte[42];
      int n = s.Read(block, 0, block.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501306uL);
      if (n != block.Length) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501307uL);
        return null;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501308uL);
      int i = 0;
      ZipEntry zde = new ZipEntry();
      zde.AlternateEncoding = expectedEncoding;
      zde._Source = ZipEntrySource.ZipFile;
      zde._container = new ZipContainer(zf);
      unchecked {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501309uL);
        zde._VersionMadeBy = (short)(block[i++] + block[i++] * 256);
        zde._VersionNeeded = (short)(block[i++] + block[i++] * 256);
        zde._BitField = (short)(block[i++] + block[i++] * 256);
        zde._CompressionMethod = (Int16)(block[i++] + block[i++] * 256);
        zde._TimeBlob = block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256;
        zde._LastModified = Ionic.Zip.SharedUtilities.PackedToDateTime(zde._TimeBlob);
        zde._timestamp |= ZipEntryTimestamp.DOS;
        zde._Crc32 = block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256;
        zde._CompressedSize = (uint)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
        zde._UncompressedSize = (uint)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
      }
      zde._CompressionMethod_FromZipFile = zde._CompressionMethod;
      zde._filenameLength = (short)(block[i++] + block[i++] * 256);
      zde._extraFieldLength = (short)(block[i++] + block[i++] * 256);
      zde._commentLength = (short)(block[i++] + block[i++] * 256);
      zde._diskNumber = (UInt32)(block[i++] + block[i++] * 256);
      zde._InternalFileAttrs = (short)(block[i++] + block[i++] * 256);
      zde._ExternalFileAttrs = block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256;
      zde._RelativeOffsetOfLocalHeader = (uint)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
      zde.IsText = ((zde._InternalFileAttrs & 0x1) == 0x1);
      block = new byte[zde._filenameLength];
      n = s.Read(block, 0, block.Length);
      bytesRead += n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501310uL);
      if ((zde._BitField & 0x800) == 0x800) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501311uL);
        zde._FileNameInArchive = Ionic.Zip.SharedUtilities.Utf8StringFromBuffer(block);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501312uL);
        zde._FileNameInArchive = Ionic.Zip.SharedUtilities.StringFromBuffer(block, expectedEncoding);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501313uL);
      while (previouslySeen.ContainsKey(zde._FileNameInArchive)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501314uL);
        zde._FileNameInArchive = CopyHelper.AppendCopyToFileName(zde._FileNameInArchive);
        zde._metadataChanged = true;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501315uL);
      if (zde.AttributesIndicateDirectory) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501316uL);
        zde.MarkAsDirectory();
      } else if (zde._FileNameInArchive.EndsWith("/")) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501317uL);
        zde.MarkAsDirectory();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501318uL);
      zde._CompressedFileDataSize = zde._CompressedSize;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501319uL);
      if ((zde._BitField & 0x1) == 0x1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501320uL);
        zde._Encryption_FromZipFile = zde._Encryption = EncryptionAlgorithm.PkzipWeak;
        zde._sourceIsEncrypted = true;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501321uL);
      if (zde._extraFieldLength > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501322uL);
        zde._InputUsesZip64 = (zde._CompressedSize == 0xffffffffu || zde._UncompressedSize == 0xffffffffu || zde._RelativeOffsetOfLocalHeader == 0xffffffffu);
        bytesRead += zde.ProcessExtraField(s, zde._extraFieldLength);
        zde._CompressedFileDataSize = zde._CompressedSize;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501323uL);
      if (zde._Encryption == EncryptionAlgorithm.PkzipWeak) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501324uL);
        zde._CompressedFileDataSize -= 12;
      } else if (zde.Encryption == EncryptionAlgorithm.WinZipAes128 || zde.Encryption == EncryptionAlgorithm.WinZipAes256) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501325uL);
        zde._CompressedFileDataSize = zde.CompressedSize - (ZipEntry.GetLengthOfCryptoHeaderBytes(zde.Encryption) + 10);
        zde._LengthOfTrailer = 10;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501326uL);
      if ((zde._BitField & 0x8) == 0x8) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501327uL);
        if (zde._InputUsesZip64) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501328uL);
          zde._LengthOfTrailer += 24;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501329uL);
          zde._LengthOfTrailer += 16;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501330uL);
      zde.AlternateEncoding = ((zde._BitField & 0x800) == 0x800) ? System.Text.Encoding.UTF8 : expectedEncoding;
      zde.AlternateEncodingUsage = ZipOption.Always;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501331uL);
      if (zde._commentLength > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501332uL);
        block = new byte[zde._commentLength];
        n = s.Read(block, 0, block.Length);
        bytesRead += n;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501333uL);
        if ((zde._BitField & 0x800) == 0x800) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501334uL);
          zde._Comment = Ionic.Zip.SharedUtilities.Utf8StringFromBuffer(block);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501335uL);
          zde._Comment = Ionic.Zip.SharedUtilities.StringFromBuffer(block, expectedEncoding);
        }
      }
      ZipEntry RNTRNTRNT_225 = zde;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501336uL);
      return RNTRNTRNT_225;
    }
    static internal bool IsNotValidZipDirEntrySig(int signature)
    {
      System.Boolean RNTRNTRNT_226 = (signature != ZipConstants.ZipDirEntrySignature);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501337uL);
      return RNTRNTRNT_226;
    }
    private Int16 _VersionMadeBy;
    private Int16 _InternalFileAttrs;
    private Int32 _ExternalFileAttrs;
    private Int16 _filenameLength;
    private Int16 _extraFieldLength;
    private Int16 _commentLength;
  }
}
