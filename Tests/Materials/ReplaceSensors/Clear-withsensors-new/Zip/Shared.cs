using System;
using System.IO;
using System.Security.Permissions;
namespace Ionic.Zip
{
  static internal class SharedUtilities
  {
    public static Int64 GetFileLength(string fileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500945uL);
      if (!File.Exists(fileName)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500946uL);
        throw new System.IO.FileNotFoundException(fileName);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500947uL);
      long fileLength = 0L;
      FileShare fs = FileShare.ReadWrite;
      fs |= FileShare.Delete;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500948uL);
      using (var s = File.Open(fileName, FileMode.Open, FileAccess.Read, fs)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500949uL);
        fileLength = s.Length;
      }
      Int64 RNTRNTRNT_150 = fileLength;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500950uL);
      return RNTRNTRNT_150;
    }
    [System.Diagnostics.Conditional("NETCF")]
    public static void Workaround_Ladybug318918(Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500951uL);
      s.Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500952uL);
    }
    private static System.Text.RegularExpressions.Regex doubleDotRegex1 = new System.Text.RegularExpressions.Regex("^(.*/)?([^/\\\\.]+/\\\\.\\\\./)(.+)$");
    private static string SimplifyFwdSlashPath(string path)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500953uL);
      if (path.StartsWith("./")) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500954uL);
        path = path.Substring(2);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500955uL);
      path = path.Replace("/./", "/");
      path = doubleDotRegex1.Replace(path, "$1$3");
      System.String RNTRNTRNT_151 = path;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500956uL);
      return RNTRNTRNT_151;
    }
    public static string NormalizePathForUseInZipFile(string pathName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500957uL);
      if (String.IsNullOrEmpty(pathName)) {
        System.String RNTRNTRNT_152 = pathName;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500958uL);
        return RNTRNTRNT_152;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500959uL);
      if ((pathName.Length >= 2) && ((pathName[1] == ':') && (pathName[2] == '\\'))) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500960uL);
        pathName = pathName.Substring(3);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500961uL);
      pathName = pathName.Replace('\\', '/');
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500962uL);
      while (pathName.StartsWith("/")) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500963uL);
        pathName = pathName.Substring(1);
      }
      System.String RNTRNTRNT_153 = SimplifyFwdSlashPath(pathName);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500964uL);
      return RNTRNTRNT_153;
    }
    static System.Text.Encoding ibm437 = System.Text.Encoding.GetEncoding("IBM437");
    static System.Text.Encoding utf8 = System.Text.Encoding.GetEncoding("UTF-8");
    static internal byte[] StringToByteArray(string value, System.Text.Encoding encoding)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500965uL);
      byte[] a = encoding.GetBytes(value);
      System.Byte[] RNTRNTRNT_154 = a;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500966uL);
      return RNTRNTRNT_154;
    }
    static internal byte[] StringToByteArray(string value)
    {
      System.Byte[] RNTRNTRNT_155 = StringToByteArray(value, ibm437);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500967uL);
      return RNTRNTRNT_155;
    }
    static internal string Utf8StringFromBuffer(byte[] buf)
    {
      System.String RNTRNTRNT_156 = StringFromBuffer(buf, utf8);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500968uL);
      return RNTRNTRNT_156;
    }
    static internal string StringFromBuffer(byte[] buf, System.Text.Encoding encoding)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500969uL);
      string s = encoding.GetString(buf, 0, buf.Length);
      System.String RNTRNTRNT_157 = s;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500970uL);
      return RNTRNTRNT_157;
    }
    static internal int ReadSignature(System.IO.Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500971uL);
      int x = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500972uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500973uL);
        x = _ReadFourBytes(s, "n/a");
      } catch (BadReadException) {
      }
      System.Int32 RNTRNTRNT_158 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500974uL);
      return RNTRNTRNT_158;
    }
    static internal int ReadEntrySignature(System.IO.Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500975uL);
      int x = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500976uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500977uL);
        x = _ReadFourBytes(s, "n/a");
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500978uL);
        if (x == ZipConstants.ZipEntryDataDescriptorSignature) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500979uL);
          s.Seek(12, SeekOrigin.Current);
          Workaround_Ladybug318918(s);
          x = _ReadFourBytes(s, "n/a");
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500980uL);
          if (x != ZipConstants.ZipEntrySignature) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500981uL);
            s.Seek(8, SeekOrigin.Current);
            Workaround_Ladybug318918(s);
            x = _ReadFourBytes(s, "n/a");
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500982uL);
            if (x != ZipConstants.ZipEntrySignature) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500983uL);
              s.Seek(-24, SeekOrigin.Current);
              Workaround_Ladybug318918(s);
              x = _ReadFourBytes(s, "n/a");
            }
          }
        }
      } catch (BadReadException) {
      }
      System.Int32 RNTRNTRNT_159 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500984uL);
      return RNTRNTRNT_159;
    }
    static internal int ReadInt(System.IO.Stream s)
    {
      System.Int32 RNTRNTRNT_160 = _ReadFourBytes(s, "Could not read block - no data!  (position 0x{0:X8})");
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500985uL);
      return RNTRNTRNT_160;
    }
    private static int _ReadFourBytes(System.IO.Stream s, string message)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500986uL);
      int n = 0;
      byte[] block = new byte[4];
      n = s.Read(block, 0, block.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500987uL);
      if (n != block.Length) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500988uL);
        throw new BadReadException(String.Format(message, s.Position));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500989uL);
      int data = unchecked((((block[3] * 256 + block[2]) * 256) + block[1]) * 256 + block[0]);
      System.Int32 RNTRNTRNT_161 = data;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500990uL);
      return RNTRNTRNT_161;
    }
    static internal long FindSignature(System.IO.Stream stream, int SignatureToFind)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500991uL);
      long startingPosition = stream.Position;
      int BATCH_SIZE = 65536;
      byte[] targetBytes = new byte[4];
      targetBytes[0] = (byte)(SignatureToFind >> 24);
      targetBytes[1] = (byte)((SignatureToFind & 0xff0000) >> 16);
      targetBytes[2] = (byte)((SignatureToFind & 0xff00) >> 8);
      targetBytes[3] = (byte)(SignatureToFind & 0xff);
      byte[] batch = new byte[BATCH_SIZE];
      int n = 0;
      bool success = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500992uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500993uL);
        n = stream.Read(batch, 0, batch.Length);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500994uL);
        if (n != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500995uL);
          for (int i = 0; i < n; i++) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500996uL);
            if (batch[i] == targetBytes[3]) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500997uL);
              long curPosition = stream.Position;
              stream.Seek(i - n, System.IO.SeekOrigin.Current);
              Workaround_Ladybug318918(stream);
              int sig = ReadSignature(stream);
              success = (sig == SignatureToFind);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500998uL);
              if (!success) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500999uL);
                stream.Seek(curPosition, System.IO.SeekOrigin.Begin);
                Workaround_Ladybug318918(stream);
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501000uL);
                break;
              }
            }
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501001uL);
          break;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501002uL);
        if (success) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501003uL);
          break;
        }
      } while (true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501004uL);
      if (!success) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501005uL);
        stream.Seek(startingPosition, System.IO.SeekOrigin.Begin);
        Workaround_Ladybug318918(stream);
        System.Int64 RNTRNTRNT_162 = -1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501006uL);
        return RNTRNTRNT_162;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501007uL);
      long bytesRead = (stream.Position - startingPosition) - 4;
      System.Int64 RNTRNTRNT_163 = bytesRead;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501008uL);
      return RNTRNTRNT_163;
    }
    static internal DateTime AdjustTime_Reverse(DateTime time)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501009uL);
      if (time.Kind == DateTimeKind.Utc) {
        DateTime RNTRNTRNT_164 = time;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501010uL);
        return RNTRNTRNT_164;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501011uL);
      DateTime adjusted = time;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501012uL);
      if (DateTime.Now.IsDaylightSavingTime() && !time.IsDaylightSavingTime()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501013uL);
        adjusted = time - new System.TimeSpan(1, 0, 0);
      } else if (!DateTime.Now.IsDaylightSavingTime() && time.IsDaylightSavingTime()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501014uL);
        adjusted = time + new System.TimeSpan(1, 0, 0);
      }
      DateTime RNTRNTRNT_165 = adjusted;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501015uL);
      return RNTRNTRNT_165;
    }
    static internal DateTime PackedToDateTime(Int32 packedDateTime)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501016uL);
      if (packedDateTime == 0xffff || packedDateTime == 0) {
        DateTime RNTRNTRNT_166 = new System.DateTime(1995, 1, 1, 0, 0, 0, 0);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501017uL);
        return RNTRNTRNT_166;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501018uL);
      Int16 packedTime = unchecked((Int16)(packedDateTime & 0xffff));
      Int16 packedDate = unchecked((Int16)((packedDateTime & 0xffff0000u) >> 16));
      int year = 1980 + ((packedDate & 0xfe00) >> 9);
      int month = (packedDate & 0x1e0) >> 5;
      int day = packedDate & 0x1f;
      int hour = (packedTime & 0xf800) >> 11;
      int minute = (packedTime & 0x7e0) >> 5;
      int second = (packedTime & 0x1f) * 2;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501019uL);
      if (second >= 60) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501020uL);
        minute++;
        second = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501021uL);
      if (minute >= 60) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501022uL);
        hour++;
        minute = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501023uL);
      if (hour >= 24) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501024uL);
        day++;
        hour = 0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501025uL);
      DateTime d = System.DateTime.Now;
      bool success = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501026uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501027uL);
        d = new System.DateTime(year, month, day, hour, minute, second, 0);
        success = true;
      } catch (System.ArgumentOutOfRangeException) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501028uL);
        if (year == 1980 && (month == 0 || day == 0)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501029uL);
          try {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501030uL);
            d = new System.DateTime(1980, 1, 1, hour, minute, second, 0);
            success = true;
          } catch (System.ArgumentOutOfRangeException) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501031uL);
            try {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501032uL);
              d = new System.DateTime(1980, 1, 1, 0, 0, 0, 0);
              success = true;
            } catch (System.ArgumentOutOfRangeException) {
            }
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501033uL);
          try {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501034uL);
            while (year < 1980) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501035uL);
              year++;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501036uL);
            while (year > 2030) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501037uL);
              year--;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501038uL);
            while (month < 1) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501039uL);
              month++;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501040uL);
            while (month > 12) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501041uL);
              month--;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501042uL);
            while (day < 1) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501043uL);
              day++;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501044uL);
            while (day > 28) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501045uL);
              day--;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501046uL);
            while (minute < 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501047uL);
              minute++;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501048uL);
            while (minute > 59) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501049uL);
              minute--;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501050uL);
            while (second < 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501051uL);
              second++;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501052uL);
            while (second > 59) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501053uL);
              second--;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501054uL);
            d = new System.DateTime(year, month, day, hour, minute, second, 0);
            success = true;
          } catch (System.ArgumentOutOfRangeException) {
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501055uL);
      if (!success) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501056uL);
        string msg = String.Format("y({0}) m({1}) d({2}) h({3}) m({4}) s({5})", year, month, day, hour, minute, second);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501057uL);
        throw new ZipException(String.Format("Bad date/time format in the zip file. ({0})", msg));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501058uL);
      d = DateTime.SpecifyKind(d, DateTimeKind.Local);
      DateTime RNTRNTRNT_167 = d;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501059uL);
      return RNTRNTRNT_167;
    }
    static internal Int32 DateTimeToPacked(DateTime time)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501060uL);
      time = time.ToLocalTime();
      UInt16 packedDate = (UInt16)((time.Day & 0x1f) | ((time.Month << 5) & 0x1e0) | (((time.Year - 1980) << 9) & 0xfe00));
      UInt16 packedTime = (UInt16)((time.Second / 2 & 0x1f) | ((time.Minute << 5) & 0x7e0) | ((time.Hour << 11) & 0xf800));
      Int32 result = (Int32)(((UInt32)(packedDate << 16)) | packedTime);
      Int32 RNTRNTRNT_168 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501061uL);
      return RNTRNTRNT_168;
    }
    public static void CreateAndOpenUniqueTempFile(string dir, out Stream fs, out string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501062uL);
      for (int i = 0; i < 3; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501063uL);
        try {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501064uL);
          filename = Path.Combine(dir, InternalGetTempFileName());
          fs = new FileStream(filename, FileMode.CreateNew);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501065uL);
          return;
        } catch (IOException) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501066uL);
          if (i == 2) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501067uL);
            throw;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501068uL);
      throw new IOException();
    }
    public static string InternalGetTempFileName()
    {
      System.String RNTRNTRNT_169 = "DotNetZip-" + Path.GetRandomFileName().Substring(0, 8) + ".tmp";
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501069uL);
      return RNTRNTRNT_169;
    }
    static internal int ReadWithRetry(System.IO.Stream s, byte[] buffer, int offset, int count, string FileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501070uL);
      int n = 0;
      bool done = false;
      int retries = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501071uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501072uL);
        try {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501073uL);
          n = s.Read(buffer, offset, count);
          done = true;
        } catch (System.IO.IOException ioexc1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501074uL);
          var p = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501075uL);
          if (p.IsUnrestricted()) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501076uL);
            uint hresult = _HRForException(ioexc1);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501077uL);
            if (hresult != 0x80070021u) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501078uL);
              throw new System.IO.IOException(String.Format("Cannot read file {0}", FileName), ioexc1);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501079uL);
            retries++;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501080uL);
            if (retries > 10) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501081uL);
              throw new System.IO.IOException(String.Format("Cannot read file {0}, at offset 0x{1:X8} after 10 retries", FileName, offset), ioexc1);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501082uL);
            System.Threading.Thread.Sleep(250 + retries * 550);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501083uL);
            throw;
          }
        }
      } while (!done);
      System.Int32 RNTRNTRNT_170 = n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501084uL);
      return RNTRNTRNT_170;
    }
    private static uint _HRForException(System.Exception ex1)
    {
      System.UInt32 RNTRNTRNT_171 = unchecked((uint)System.Runtime.InteropServices.Marshal.GetHRForException(ex1));
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501085uL);
      return RNTRNTRNT_171;
    }
  }
  public class CountingStream : System.IO.Stream
  {
    private System.IO.Stream _s;
    private Int64 _bytesWritten;
    private Int64 _bytesRead;
    private Int64 _initialOffset;
    public CountingStream(System.IO.Stream stream) : base()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501086uL);
      _s = stream;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501087uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501088uL);
        _initialOffset = _s.Position;
      } catch {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501089uL);
        _initialOffset = 0L;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501090uL);
    }
    public Stream WrappedStream {
      get {
        Stream RNTRNTRNT_172 = _s;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501091uL);
        return RNTRNTRNT_172;
      }
    }
    public Int64 BytesWritten {
      get {
        Int64 RNTRNTRNT_173 = _bytesWritten;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501092uL);
        return RNTRNTRNT_173;
      }
    }
    public Int64 BytesRead {
      get {
        Int64 RNTRNTRNT_174 = _bytesRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501093uL);
        return RNTRNTRNT_174;
      }
    }
    public void Adjust(Int64 delta)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501094uL);
      _bytesWritten -= delta;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501095uL);
      if (_bytesWritten < 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501096uL);
        throw new InvalidOperationException();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501097uL);
      if (_s as CountingStream != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501098uL);
        ((CountingStream)_s).Adjust(delta);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501099uL);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501100uL);
      int n = _s.Read(buffer, offset, count);
      _bytesRead += n;
      System.Int32 RNTRNTRNT_175 = n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501101uL);
      return RNTRNTRNT_175;
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501102uL);
      if (count == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501103uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501104uL);
      _s.Write(buffer, offset, count);
      _bytesWritten += count;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501105uL);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_176 = _s.CanRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501106uL);
        return RNTRNTRNT_176;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_177 = _s.CanSeek;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501107uL);
        return RNTRNTRNT_177;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_178 = _s.CanWrite;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501108uL);
        return RNTRNTRNT_178;
      }
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501109uL);
      _s.Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501110uL);
    }
    public override long Length {
      get {
        System.Int64 RNTRNTRNT_179 = _s.Length;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501111uL);
        return RNTRNTRNT_179;
      }
    }
    public long ComputedPosition {
      get {
        System.Int64 RNTRNTRNT_180 = _initialOffset + _bytesWritten;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501112uL);
        return RNTRNTRNT_180;
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_181 = _s.Position;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501113uL);
        return RNTRNTRNT_181;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501114uL);
        _s.Seek(value, System.IO.SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_s);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501115uL);
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      System.Int64 RNTRNTRNT_182 = _s.Seek(offset, origin);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501116uL);
      return RNTRNTRNT_182;
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501117uL);
      _s.SetLength(value);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501118uL);
    }
  }
}
