using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public partial class ZipFile
  {
    private void DeleteFileWithRetry(string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503191uL);
      bool done = false;
      int nRetries = 3;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503192uL);
      for (int i = 0; i < nRetries && !done; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503193uL);
        try {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503194uL);
          File.Delete(filename);
          done = true;
        } catch (System.UnauthorizedAccessException) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503195uL);
          Console.WriteLine("************************************************** Retry delete.");
          System.Threading.Thread.Sleep(200 + i * 200);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503196uL);
    }
    public void Save()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503197uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503198uL);
        bool thisSaveUsedZip64 = false;
        _saveOperationCanceled = false;
        _numberOfSegmentsForMostRecentSave = 0;
        OnSaveStarted();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503199uL);
        if (WriteStream == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503200uL);
          throw new BadStateException("You haven't specified where to save the zip.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503201uL);
        if (_name != null && _name.EndsWith(".exe") && !_SavingSfx) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503202uL);
          throw new BadStateException("You specified an EXE for a plain zip file.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503203uL);
        if (!_contentsChanged) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503204uL);
          OnSaveCompleted();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503205uL);
          if (Verbose) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503206uL);
            StatusMessageTextWriter.WriteLine("No save is necessary....");
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503207uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503208uL);
        Reset(true);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503209uL);
        if (Verbose) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503210uL);
          StatusMessageTextWriter.WriteLine("saving....");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503211uL);
        if (_entries.Count >= 0xffff && _zip64 == Zip64Option.Never) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503212uL);
          throw new ZipException("The number of entries is 65535 or greater. Consider setting the UseZip64WhenSaving property on the ZipFile instance.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503213uL);
        int n = 0;
        ICollection<ZipEntry> c = (SortEntriesBeforeSaving) ? EntriesSorted : Entries;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503214uL);
        foreach (ZipEntry e in c) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503215uL);
          OnSaveEntry(n, e, true);
          e.Write(WriteStream);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503216uL);
          if (_saveOperationCanceled) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503217uL);
            break;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503218uL);
          n++;
          OnSaveEntry(n, e, false);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503219uL);
          if (_saveOperationCanceled) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503220uL);
            break;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503221uL);
          if (e.IncludedInMostRecentSave) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503222uL);
            thisSaveUsedZip64 |= e.OutputUsedZip64.Value;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503223uL);
        if (_saveOperationCanceled) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503224uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503225uL);
        var zss = WriteStream as ZipSegmentedStream;
        _numberOfSegmentsForMostRecentSave = (zss != null) ? zss.CurrentSegment : 1;
        bool directoryNeededZip64 = ZipOutput.WriteCentralDirectoryStructure(WriteStream, c, _numberOfSegmentsForMostRecentSave, _zip64, Comment, new ZipContainer(this));
        OnSaveEvent(ZipProgressEventType.Saving_AfterSaveTempArchive);
        _hasBeenSaved = true;
        _contentsChanged = false;
        thisSaveUsedZip64 |= directoryNeededZip64;
        _OutputUsesZip64 = new Nullable<bool>(thisSaveUsedZip64);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503226uL);
        if (_name != null && (_temporaryFileName != null || zss != null)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503227uL);
          WriteStream.Dispose();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503228uL);
          if (_saveOperationCanceled) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503229uL);
            return;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503230uL);
          if (_fileAlreadyExists && this._readstream != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503231uL);
            this._readstream.Close();
            this._readstream = null;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503232uL);
            foreach (var e in c) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503233uL);
              var zss1 = e._archiveStream as ZipSegmentedStream;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503234uL);
              if (zss1 != null) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503235uL);
                zss1.Dispose();
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503236uL);
              e._archiveStream = null;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503237uL);
          string tmpName = null;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503238uL);
          if (File.Exists(_name)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503239uL);
            tmpName = _name + "." + Path.GetRandomFileName();
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503240uL);
            if (File.Exists(tmpName)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503241uL);
              DeleteFileWithRetry(tmpName);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503242uL);
            File.Move(_name, tmpName);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503243uL);
          OnSaveEvent(ZipProgressEventType.Saving_BeforeRenameTempArchive);
          File.Move((zss != null) ? zss.CurrentTempName : _temporaryFileName, _name);
          OnSaveEvent(ZipProgressEventType.Saving_AfterRenameTempArchive);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503244uL);
          if (tmpName != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503245uL);
            try {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503246uL);
              if (File.Exists(tmpName)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503247uL);
                File.Delete(tmpName);
              }
            } catch {
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503248uL);
          _fileAlreadyExists = true;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503249uL);
        NotifyEntriesSaveComplete(c);
        OnSaveCompleted();
        _JustSaved = true;
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503250uL);
        CleanupAfterSaveOperation();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503251uL);
      return;
    }
    private static void NotifyEntriesSaveComplete(ICollection<ZipEntry> c)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503252uL);
      foreach (ZipEntry e in c) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503253uL);
        e.NotifySaveComplete();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503254uL);
    }
    private void RemoveTempFile()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503255uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503256uL);
        if (File.Exists(_temporaryFileName)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503257uL);
          File.Delete(_temporaryFileName);
        }
      } catch (IOException ex1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503258uL);
        if (Verbose) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503259uL);
          StatusMessageTextWriter.WriteLine("ZipFile::Save: could not delete temp file: {0}.", ex1.Message);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503260uL);
    }
    private void CleanupAfterSaveOperation()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503261uL);
      if (_name != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503262uL);
        if (_writestream != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503263uL);
          try {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503264uL);
            _writestream.Dispose();
          } catch (System.IO.IOException) {
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503265uL);
        _writestream = null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503266uL);
        if (_temporaryFileName != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503267uL);
          RemoveTempFile();
          _temporaryFileName = null;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503268uL);
    }
    public void Save(String fileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503269uL);
      if (_name == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503270uL);
        _writestream = null;
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503271uL);
        _readName = _name;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503272uL);
      _name = fileName;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503273uL);
      if (Directory.Exists(_name)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503274uL);
        throw new ZipException("Bad Directory", new System.ArgumentException("That name specifies an existing directory. Please specify a filename.", "fileName"));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503275uL);
      _contentsChanged = true;
      _fileAlreadyExists = File.Exists(_name);
      Save();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503276uL);
    }
    public void Save(Stream outputStream)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503277uL);
      if (outputStream == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503278uL);
        throw new ArgumentNullException("outputStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503279uL);
      if (!outputStream.CanWrite) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503280uL);
        throw new ArgumentException("Must be a writable stream.", "outputStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503281uL);
      _name = null;
      _writestream = new CountingStream(outputStream);
      _contentsChanged = true;
      _fileAlreadyExists = false;
      Save();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503282uL);
    }
  }
  static internal class ZipOutput
  {
    public static bool WriteCentralDirectoryStructure(Stream s, ICollection<ZipEntry> entries, uint numSegments, Zip64Option zip64, String comment, ZipContainer container)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503283uL);
      var zss = s as ZipSegmentedStream;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503284uL);
      if (zss != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503285uL);
        zss.ContiguousWrite = true;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503286uL);
      Int64 aLength = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503287uL);
      using (var ms = new MemoryStream()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503288uL);
        foreach (ZipEntry e in entries) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503289uL);
          if (e.IncludedInMostRecentSave) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503290uL);
            e.WriteCentralDirectoryEntry(ms);
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503291uL);
        var a = ms.ToArray();
        s.Write(a, 0, a.Length);
        aLength = a.Length;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503292uL);
      var output = s as CountingStream;
      long Finish = (output != null) ? output.ComputedPosition : s.Position;
      long Start = Finish - aLength;
      UInt32 startSegment = (zss != null) ? zss.CurrentSegment : 0;
      Int64 SizeOfCentralDirectory = Finish - Start;
      int countOfEntries = CountEntries(entries);
      bool needZip64CentralDirectory = zip64 == Zip64Option.Always || countOfEntries >= 0xffff || SizeOfCentralDirectory > 0xffffffffu || Start > 0xffffffffu;
      byte[] a2 = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503293uL);
      if (needZip64CentralDirectory) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503294uL);
        if (zip64 == Zip64Option.Never) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503295uL);
          System.Diagnostics.StackFrame sf = new System.Diagnostics.StackFrame(1);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503296uL);
          if (sf.GetMethod().DeclaringType == typeof(ZipFile)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503297uL);
            throw new ZipException("The archive requires a ZIP64 Central Directory. Consider setting the ZipFile.UseZip64WhenSaving property.");
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503298uL);
            throw new ZipException("The archive requires a ZIP64 Central Directory. Consider setting the ZipOutputStream.EnableZip64 property.");
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503299uL);
        var a = GenZip64EndOfCentralDirectory(Start, Finish, countOfEntries, numSegments);
        a2 = GenCentralDirectoryFooter(Start, Finish, zip64, countOfEntries, comment, container);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503300uL);
        if (startSegment != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503301uL);
          UInt32 thisSegment = zss.ComputeSegment(a.Length + a2.Length);
          int i = 16;
          Array.Copy(BitConverter.GetBytes(thisSegment), 0, a, i, 4);
          i += 4;
          Array.Copy(BitConverter.GetBytes(thisSegment), 0, a, i, 4);
          i = 60;
          Array.Copy(BitConverter.GetBytes(thisSegment), 0, a, i, 4);
          i += 4;
          i += 8;
          Array.Copy(BitConverter.GetBytes(thisSegment), 0, a, i, 4);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503302uL);
        s.Write(a, 0, a.Length);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503303uL);
        a2 = GenCentralDirectoryFooter(Start, Finish, zip64, countOfEntries, comment, container);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503304uL);
      if (startSegment != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503305uL);
        UInt16 thisSegment = (UInt16)zss.ComputeSegment(a2.Length);
        int i = 4;
        Array.Copy(BitConverter.GetBytes(thisSegment), 0, a2, i, 2);
        i += 2;
        Array.Copy(BitConverter.GetBytes(thisSegment), 0, a2, i, 2);
        i += 2;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503306uL);
      s.Write(a2, 0, a2.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503307uL);
      if (zss != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503308uL);
        zss.ContiguousWrite = false;
      }
      System.Boolean RNTRNTRNT_439 = needZip64CentralDirectory;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503309uL);
      return RNTRNTRNT_439;
    }
    private static System.Text.Encoding GetEncoding(ZipContainer container, string t)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503310uL);
      switch (container.AlternateEncodingUsage) {
        case ZipOption.Always:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503312uL);
          
          {
            System.Text.Encoding RNTRNTRNT_440 = container.AlternateEncoding;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503311uL);
            return RNTRNTRNT_440;
          }

        case ZipOption.Never:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503314uL);
          
          {
            System.Text.Encoding RNTRNTRNT_441 = container.DefaultEncoding;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503313uL);
            return RNTRNTRNT_441;
          }

      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503315uL);
      var e = container.DefaultEncoding;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503316uL);
      if (t == null) {
        System.Text.Encoding RNTRNTRNT_442 = e;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503317uL);
        return RNTRNTRNT_442;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503318uL);
      var bytes = e.GetBytes(t);
      var t2 = e.GetString(bytes, 0, bytes.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503319uL);
      if (t2.Equals(t)) {
        System.Text.Encoding RNTRNTRNT_443 = e;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503320uL);
        return RNTRNTRNT_443;
      }
      System.Text.Encoding RNTRNTRNT_444 = container.AlternateEncoding;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503321uL);
      return RNTRNTRNT_444;
    }
    private static byte[] GenCentralDirectoryFooter(long StartOfCentralDirectory, long EndOfCentralDirectory, Zip64Option zip64, int entryCount, string comment, ZipContainer container)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503322uL);
      System.Text.Encoding encoding = GetEncoding(container, comment);
      int j = 0;
      int bufferLength = 22;
      byte[] block = null;
      Int16 commentLength = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503323uL);
      if ((comment != null) && (comment.Length != 0)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503324uL);
        block = encoding.GetBytes(comment);
        commentLength = (Int16)block.Length;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503325uL);
      bufferLength += commentLength;
      byte[] bytes = new byte[bufferLength];
      int i = 0;
      byte[] sig = BitConverter.GetBytes(ZipConstants.EndOfCentralDirectorySignature);
      Array.Copy(sig, 0, bytes, i, 4);
      i += 4;
      bytes[i++] = 0;
      bytes[i++] = 0;
      bytes[i++] = 0;
      bytes[i++] = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503326uL);
      if (entryCount >= 0xffff || zip64 == Zip64Option.Always) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503327uL);
        for (j = 0; j < 4; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503328uL);
          bytes[i++] = 0xff;
        }
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503329uL);
        bytes[i++] = (byte)(entryCount & 0xff);
        bytes[i++] = (byte)((entryCount & 0xff00) >> 8);
        bytes[i++] = (byte)(entryCount & 0xff);
        bytes[i++] = (byte)((entryCount & 0xff00) >> 8);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503330uL);
      Int64 SizeOfCentralDirectory = EndOfCentralDirectory - StartOfCentralDirectory;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503331uL);
      if (SizeOfCentralDirectory >= 0xffffffffu || StartOfCentralDirectory >= 0xffffffffu) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503332uL);
        for (j = 0; j < 8; j++) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503333uL);
          bytes[i++] = 0xff;
        }
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503334uL);
        bytes[i++] = (byte)(SizeOfCentralDirectory & 0xff);
        bytes[i++] = (byte)((SizeOfCentralDirectory & 0xff00) >> 8);
        bytes[i++] = (byte)((SizeOfCentralDirectory & 0xff0000) >> 16);
        bytes[i++] = (byte)((SizeOfCentralDirectory & 0xff000000u) >> 24);
        bytes[i++] = (byte)(StartOfCentralDirectory & 0xff);
        bytes[i++] = (byte)((StartOfCentralDirectory & 0xff00) >> 8);
        bytes[i++] = (byte)((StartOfCentralDirectory & 0xff0000) >> 16);
        bytes[i++] = (byte)((StartOfCentralDirectory & 0xff000000u) >> 24);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503335uL);
      if ((comment == null) || (comment.Length == 0)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503336uL);
        bytes[i++] = (byte)0;
        bytes[i++] = (byte)0;
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503337uL);
        if (commentLength + i + 2 > bytes.Length) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503338uL);
          commentLength = (Int16)(bytes.Length - i - 2);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503339uL);
        bytes[i++] = (byte)(commentLength & 0xff);
        bytes[i++] = (byte)((commentLength & 0xff00) >> 8);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503340uL);
        if (commentLength != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503341uL);
          for (j = 0; (j < commentLength) && (i + j < bytes.Length); j++) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503342uL);
            bytes[i + j] = block[j];
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503343uL);
          i += j;
        }
      }
      System.Byte[] RNTRNTRNT_445 = bytes;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503344uL);
      return RNTRNTRNT_445;
    }
    private static byte[] GenZip64EndOfCentralDirectory(long StartOfCentralDirectory, long EndOfCentralDirectory, int entryCount, uint numSegments)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503345uL);
      const int bufferLength = 12 + 44 + 20;
      byte[] bytes = new byte[bufferLength];
      int i = 0;
      byte[] sig = BitConverter.GetBytes(ZipConstants.Zip64EndOfCentralDirectoryRecordSignature);
      Array.Copy(sig, 0, bytes, i, 4);
      i += 4;
      long DataSize = 44;
      Array.Copy(BitConverter.GetBytes(DataSize), 0, bytes, i, 8);
      i += 8;
      bytes[i++] = 45;
      bytes[i++] = 0x0;
      bytes[i++] = 45;
      bytes[i++] = 0x0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503346uL);
      for (int j = 0; j < 8; j++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503347uL);
        bytes[i++] = 0x0;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503348uL);
      long numberOfEntries = entryCount;
      Array.Copy(BitConverter.GetBytes(numberOfEntries), 0, bytes, i, 8);
      i += 8;
      Array.Copy(BitConverter.GetBytes(numberOfEntries), 0, bytes, i, 8);
      i += 8;
      Int64 SizeofCentraldirectory = EndOfCentralDirectory - StartOfCentralDirectory;
      Array.Copy(BitConverter.GetBytes(SizeofCentraldirectory), 0, bytes, i, 8);
      i += 8;
      Array.Copy(BitConverter.GetBytes(StartOfCentralDirectory), 0, bytes, i, 8);
      i += 8;
      sig = BitConverter.GetBytes(ZipConstants.Zip64EndOfCentralDirectoryLocatorSignature);
      Array.Copy(sig, 0, bytes, i, 4);
      i += 4;
      uint x2 = (numSegments == 0) ? 0 : (uint)(numSegments - 1);
      Array.Copy(BitConverter.GetBytes(x2), 0, bytes, i, 4);
      i += 4;
      Array.Copy(BitConverter.GetBytes(EndOfCentralDirectory), 0, bytes, i, 8);
      i += 8;
      Array.Copy(BitConverter.GetBytes(numSegments), 0, bytes, i, 4);
      i += 4;
      System.Byte[] RNTRNTRNT_446 = bytes;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503349uL);
      return RNTRNTRNT_446;
    }
    private static int CountEntries(ICollection<ZipEntry> _entries)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503350uL);
      int count = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503351uL);
      foreach (var entry in _entries) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503352uL);
        if (entry.IncludedInMostRecentSave) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503353uL);
          count++;
        }
      }
      System.Int32 RNTRNTRNT_447 = count;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503354uL);
      return RNTRNTRNT_447;
    }
  }
}
