namespace Ionic.Zip
{
  public partial class ZipFile
  {
    public System.Collections.Generic.IEnumerator<ZipEntry> GetEnumerator()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503605uL);
      foreach (ZipEntry e in _entries.Values) {
        System.Collections.Generic.IEnumerator<ZipEntry> RNTRNTRNT_466 = e;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503606uL);
        yield return RNTRNTRNT_466;
      }
    }
    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      System.Collections.IEnumerator RNTRNTRNT_467 = GetEnumerator();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503607uL);
      return RNTRNTRNT_467;
    }
    [System.Runtime.InteropServices.DispId(-4)]
    public System.Collections.IEnumerator GetNewEnum()
    {
      System.Collections.IEnumerator RNTRNTRNT_468 = GetEnumerator();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503608uL);
      return RNTRNTRNT_468;
    }
  }
}
