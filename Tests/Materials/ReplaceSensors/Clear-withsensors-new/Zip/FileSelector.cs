using System;
using System.IO;
using System.Text;
using System.Reflection;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Collections.Generic;
namespace Ionic
{
  internal enum LogicalConjunction
  {
    NONE,
    AND,
    OR,
    XOR
  }
  internal enum WhichTime
  {
    atime,
    mtime,
    ctime
  }
  internal enum ComparisonOperator
  {
    [Description(">")]
    GreaterThan,
    [Description(">=")]
    GreaterThanOrEqualTo,
    [Description("<")]
    LesserThan,
    [Description("<=")]
    LesserThanOrEqualTo,
    [Description("=")]
    EqualTo,
    [Description("!=")]
    NotEqualTo
  }
  internal abstract partial class SelectionCriterion
  {
    internal virtual bool Verbose { get; set; }
    internal abstract bool Evaluate(string filename);
    [System.Diagnostics.Conditional("SelectorTrace")]
    protected static void CriterionTrace(string format, params object[] args)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500420uL);
    }
  }
  internal partial class SizeCriterion : SelectionCriterion
  {
    internal ComparisonOperator Operator;
    internal Int64 Size;
    public override String ToString()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500421uL);
      StringBuilder sb = new StringBuilder();
      sb.Append("size ").Append(EnumUtil.GetDescription(Operator)).Append(" ").Append(Size.ToString());
      String RNTRNTRNT_100 = sb.ToString();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500422uL);
      return RNTRNTRNT_100;
    }
    internal override bool Evaluate(string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500423uL);
      System.IO.FileInfo fi = new System.IO.FileInfo(filename);
      CriterionTrace("SizeCriterion::Evaluate('{0}' [{1}])", filename, this.ToString());
      System.Boolean RNTRNTRNT_101 = _Evaluate(fi.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500424uL);
      return RNTRNTRNT_101;
    }
    private bool _Evaluate(Int64 Length)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500425uL);
      bool result = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500426uL);
      switch (Operator) {
        case ComparisonOperator.GreaterThanOrEqualTo:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500429uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500427uL);
            result = Length >= Size;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500428uL);
            break;
          }

        case ComparisonOperator.GreaterThan:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500432uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500430uL);
            result = Length > Size;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500431uL);
            break;
          }

        case ComparisonOperator.LesserThanOrEqualTo:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500435uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500433uL);
            result = Length <= Size;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500434uL);
            break;
          }

        case ComparisonOperator.LesserThan:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500438uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500436uL);
            result = Length < Size;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500437uL);
            break;
          }

        case ComparisonOperator.EqualTo:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500441uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500439uL);
            result = Length == Size;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500440uL);
            break;
          }

        case ComparisonOperator.NotEqualTo:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500444uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500442uL);
            result = Length != Size;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500443uL);
            break;
          }

        default:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500446uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500445uL);
            throw new ArgumentException("Operator");
          }

      }
      System.Boolean RNTRNTRNT_102 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500447uL);
      return RNTRNTRNT_102;
    }
  }
  internal partial class TimeCriterion : SelectionCriterion
  {
    internal ComparisonOperator Operator;
    internal WhichTime Which;
    internal DateTime Time;
    public override String ToString()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500448uL);
      StringBuilder sb = new StringBuilder();
      sb.Append(Which.ToString()).Append(" ").Append(EnumUtil.GetDescription(Operator)).Append(" ").Append(Time.ToString("yyyy-MM-dd-HH:mm:ss"));
      String RNTRNTRNT_103 = sb.ToString();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500449uL);
      return RNTRNTRNT_103;
    }
    internal override bool Evaluate(string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500450uL);
      DateTime x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500451uL);
      switch (Which) {
        case WhichTime.atime:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500454uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500452uL);
            x = System.IO.File.GetLastAccessTime(filename).ToUniversalTime();
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500453uL);
            break;
          }

        case WhichTime.mtime:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500457uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500455uL);
            x = System.IO.File.GetLastWriteTime(filename).ToUniversalTime();
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500456uL);
            break;
          }

        case WhichTime.ctime:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500460uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500458uL);
            x = System.IO.File.GetCreationTime(filename).ToUniversalTime();
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500459uL);
            break;
          }

        default:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500462uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500461uL);
            throw new ArgumentException("Operator");
          }

      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500463uL);
      CriterionTrace("TimeCriterion({0},{1})= {2}", filename, Which.ToString(), x);
      System.Boolean RNTRNTRNT_104 = _Evaluate(x);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500464uL);
      return RNTRNTRNT_104;
    }
    private bool _Evaluate(DateTime x)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500465uL);
      bool result = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500466uL);
      switch (Operator) {
        case ComparisonOperator.GreaterThanOrEqualTo:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500469uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500467uL);
            result = (x >= Time);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500468uL);
            break;
          }

        case ComparisonOperator.GreaterThan:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500472uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500470uL);
            result = (x > Time);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500471uL);
            break;
          }

        case ComparisonOperator.LesserThanOrEqualTo:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500475uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500473uL);
            result = (x <= Time);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500474uL);
            break;
          }

        case ComparisonOperator.LesserThan:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500478uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500476uL);
            result = (x < Time);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500477uL);
            break;
          }

        case ComparisonOperator.EqualTo:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500481uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500479uL);
            result = (x == Time);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500480uL);
            break;
          }

        case ComparisonOperator.NotEqualTo:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500484uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500482uL);
            result = (x != Time);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500483uL);
            break;
          }

        default:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500486uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500485uL);
            throw new ArgumentException("Operator");
          }

      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500487uL);
      CriterionTrace("TimeCriterion: {0}", result);
      System.Boolean RNTRNTRNT_105 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500488uL);
      return RNTRNTRNT_105;
    }
  }
  internal partial class NameCriterion : SelectionCriterion
  {
    private Regex _re;
    private String _regexString;
    internal ComparisonOperator Operator;
    private string _MatchingFileSpec;
    internal virtual string MatchingFileSpec {
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500489uL);
        if (Directory.Exists(value)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500490uL);
          _MatchingFileSpec = ".\\" + value + "\\*.*";
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500491uL);
          _MatchingFileSpec = value;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500492uL);
        _regexString = "^" + Regex.Escape(_MatchingFileSpec).Replace("\\\\\\*\\.\\*", "\\\\([^\\.]+|.*\\.[^\\\\\\.]*)").Replace("\\.\\*", "\\.[^\\\\\\.]*").Replace("\\*", ".*").Replace("\\?", "[^\\\\\\.]") + "$";
        CriterionTrace("NameCriterion regexString({0})", _regexString);
        _re = new Regex(_regexString, RegexOptions.IgnoreCase);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500493uL);
      }
    }
    public override String ToString()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500494uL);
      StringBuilder sb = new StringBuilder();
      sb.Append("name ").Append(EnumUtil.GetDescription(Operator)).Append(" '").Append(_MatchingFileSpec).Append("'");
      String RNTRNTRNT_106 = sb.ToString();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500495uL);
      return RNTRNTRNT_106;
    }
    internal override bool Evaluate(string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500496uL);
      CriterionTrace("NameCriterion::Evaluate('{0}' pattern[{1}])", filename, _MatchingFileSpec);
      System.Boolean RNTRNTRNT_107 = _Evaluate(filename);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500497uL);
      return RNTRNTRNT_107;
    }
    private bool _Evaluate(string fullpath)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500498uL);
      CriterionTrace("NameCriterion::Evaluate({0})", fullpath);
      String f = (_MatchingFileSpec.IndexOf('\\') == -1) ? System.IO.Path.GetFileName(fullpath) : fullpath;
      bool result = _re.IsMatch(f);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500499uL);
      if (Operator != ComparisonOperator.EqualTo) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500500uL);
        result = !result;
      }
      System.Boolean RNTRNTRNT_108 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500501uL);
      return RNTRNTRNT_108;
    }
  }
  internal partial class TypeCriterion : SelectionCriterion
  {
    private char ObjectType;
    internal ComparisonOperator Operator;
    internal string AttributeString {
      get {
        System.String RNTRNTRNT_109 = ObjectType.ToString();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500502uL);
        return RNTRNTRNT_109;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500503uL);
        if (value.Length != 1 || (value[0] != 'D' && value[0] != 'F')) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500504uL);
          throw new ArgumentException("Specify a single character: either D or F");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500505uL);
        ObjectType = value[0];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500506uL);
      }
    }
    public override String ToString()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500507uL);
      StringBuilder sb = new StringBuilder();
      sb.Append("type ").Append(EnumUtil.GetDescription(Operator)).Append(" ").Append(AttributeString);
      String RNTRNTRNT_110 = sb.ToString();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500508uL);
      return RNTRNTRNT_110;
    }
    internal override bool Evaluate(string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500509uL);
      CriterionTrace("TypeCriterion::Evaluate({0})", filename);
      bool result = (ObjectType == 'D') ? Directory.Exists(filename) : File.Exists(filename);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500510uL);
      if (Operator != ComparisonOperator.EqualTo) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500511uL);
        result = !result;
      }
      System.Boolean RNTRNTRNT_111 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500512uL);
      return RNTRNTRNT_111;
    }
  }
  internal partial class AttributesCriterion : SelectionCriterion
  {
    private FileAttributes _Attributes;
    internal ComparisonOperator Operator;
    internal string AttributeString {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500513uL);
        string result = "";
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500514uL);
        if ((_Attributes & FileAttributes.Hidden) != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500515uL);
          result += "H";
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500516uL);
        if ((_Attributes & FileAttributes.System) != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500517uL);
          result += "S";
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500518uL);
        if ((_Attributes & FileAttributes.ReadOnly) != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500519uL);
          result += "R";
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500520uL);
        if ((_Attributes & FileAttributes.Archive) != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500521uL);
          result += "A";
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500522uL);
        if ((_Attributes & FileAttributes.ReparsePoint) != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500523uL);
          result += "L";
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500524uL);
        if ((_Attributes & FileAttributes.NotContentIndexed) != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500525uL);
          result += "I";
        }
        System.String RNTRNTRNT_112 = result;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500526uL);
        return RNTRNTRNT_112;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500527uL);
        _Attributes = FileAttributes.Normal;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500528uL);
        foreach (char c in value.ToUpper()) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500529uL);
          switch (c) {
            case 'H':
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500534uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500530uL);
                if ((_Attributes & FileAttributes.Hidden) != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500531uL);
                  throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500532uL);
                _Attributes |= FileAttributes.Hidden;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500533uL);
                break;
              }

            case 'R':
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500539uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500535uL);
                if ((_Attributes & FileAttributes.ReadOnly) != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500536uL);
                  throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500537uL);
                _Attributes |= FileAttributes.ReadOnly;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500538uL);
                break;
              }

            case 'S':
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500544uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500540uL);
                if ((_Attributes & FileAttributes.System) != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500541uL);
                  throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500542uL);
                _Attributes |= FileAttributes.System;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500543uL);
                break;
              }

            case 'A':
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500549uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500545uL);
                if ((_Attributes & FileAttributes.Archive) != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500546uL);
                  throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500547uL);
                _Attributes |= FileAttributes.Archive;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500548uL);
                break;
              }

            case 'I':
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500554uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500550uL);
                if ((_Attributes & FileAttributes.NotContentIndexed) != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500551uL);
                  throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500552uL);
                _Attributes |= FileAttributes.NotContentIndexed;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500553uL);
                break;
              }

            case 'L':
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500559uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500555uL);
                if ((_Attributes & FileAttributes.ReparsePoint) != 0) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500556uL);
                  throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500557uL);
                _Attributes |= FileAttributes.ReparsePoint;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500558uL);
                break;
              }

            default:
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500561uL);
              
              {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500560uL);
                throw new ArgumentException(value);
              }

          }
        }
      }
    }
    public override String ToString()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500562uL);
      StringBuilder sb = new StringBuilder();
      sb.Append("attributes ").Append(EnumUtil.GetDescription(Operator)).Append(" ").Append(AttributeString);
      String RNTRNTRNT_113 = sb.ToString();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500563uL);
      return RNTRNTRNT_113;
    }
    private bool _EvaluateOne(FileAttributes fileAttrs, FileAttributes criterionAttrs)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500564uL);
      bool result = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500565uL);
      if ((_Attributes & criterionAttrs) == criterionAttrs) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500566uL);
        result = ((fileAttrs & criterionAttrs) == criterionAttrs);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500567uL);
        result = true;
      }
      System.Boolean RNTRNTRNT_114 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500568uL);
      return RNTRNTRNT_114;
    }
    internal override bool Evaluate(string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500569uL);
      if (Directory.Exists(filename)) {
        System.Boolean RNTRNTRNT_115 = (Operator != ComparisonOperator.EqualTo);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500570uL);
        return RNTRNTRNT_115;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500571uL);
      FileAttributes fileAttrs = System.IO.File.GetAttributes(filename);
      System.Boolean RNTRNTRNT_116 = _Evaluate(fileAttrs);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500572uL);
      return RNTRNTRNT_116;
    }
    private bool _Evaluate(FileAttributes fileAttrs)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500573uL);
      bool result = _EvaluateOne(fileAttrs, FileAttributes.Hidden);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500574uL);
      if (result) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500575uL);
        result = _EvaluateOne(fileAttrs, FileAttributes.System);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500576uL);
      if (result) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500577uL);
        result = _EvaluateOne(fileAttrs, FileAttributes.ReadOnly);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500578uL);
      if (result) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500579uL);
        result = _EvaluateOne(fileAttrs, FileAttributes.Archive);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500580uL);
      if (result) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500581uL);
        result = _EvaluateOne(fileAttrs, FileAttributes.NotContentIndexed);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500582uL);
      if (result) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500583uL);
        result = _EvaluateOne(fileAttrs, FileAttributes.ReparsePoint);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500584uL);
      if (Operator != ComparisonOperator.EqualTo) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500585uL);
        result = !result;
      }
      System.Boolean RNTRNTRNT_117 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500586uL);
      return RNTRNTRNT_117;
    }
  }
  internal partial class CompoundCriterion : SelectionCriterion
  {
    internal LogicalConjunction Conjunction;
    internal SelectionCriterion Left;
    private SelectionCriterion _Right;
    internal SelectionCriterion Right {
      get {
        SelectionCriterion RNTRNTRNT_118 = _Right;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500587uL);
        return RNTRNTRNT_118;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500588uL);
        _Right = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500589uL);
        if (value == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500590uL);
          Conjunction = LogicalConjunction.NONE;
        } else if (Conjunction == LogicalConjunction.NONE) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500591uL);
          Conjunction = LogicalConjunction.AND;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500592uL);
      }
    }
    internal override bool Evaluate(string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500593uL);
      bool result = Left.Evaluate(filename);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500594uL);
      switch (Conjunction) {
        case LogicalConjunction.AND:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500598uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500595uL);
            if (result) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500596uL);
              result = Right.Evaluate(filename);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500597uL);
            break;
          }

        case LogicalConjunction.OR:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500602uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500599uL);
            if (!result) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500600uL);
              result = Right.Evaluate(filename);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500601uL);
            break;
          }

        case LogicalConjunction.XOR:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500605uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500603uL);
            result ^= Right.Evaluate(filename);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500604uL);
            break;
          }

        default:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500607uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500606uL);
            throw new ArgumentException("Conjunction");
          }

      }
      System.Boolean RNTRNTRNT_119 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500608uL);
      return RNTRNTRNT_119;
    }
    public override String ToString()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500609uL);
      StringBuilder sb = new StringBuilder();
      sb.Append("(").Append((Left != null) ? Left.ToString() : "null").Append(" ").Append(Conjunction.ToString()).Append(" ").Append((Right != null) ? Right.ToString() : "null").Append(")");
      String RNTRNTRNT_120 = sb.ToString();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500610uL);
      return RNTRNTRNT_120;
    }
  }
  public partial class FileSelector
  {
    internal SelectionCriterion _Criterion;
    public FileSelector(String selectionCriteria) : this(selectionCriteria, true)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500611uL);
    }
    public FileSelector(String selectionCriteria, bool traverseDirectoryReparsePoints)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500612uL);
      if (!String.IsNullOrEmpty(selectionCriteria)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500613uL);
        _Criterion = _ParseCriterion(selectionCriteria);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500614uL);
      TraverseReparsePoints = traverseDirectoryReparsePoints;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500615uL);
    }
    public String SelectionCriteria {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500616uL);
        if (_Criterion == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500617uL);
          return null;
        }
        String RNTRNTRNT_121 = _Criterion.ToString();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500618uL);
        return RNTRNTRNT_121;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500619uL);
        if (value == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500620uL);
          _Criterion = null;
        } else if (value.Trim() == "") {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500622uL);
          _Criterion = null;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500621uL);
          _Criterion = _ParseCriterion(value);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500623uL);
      }
    }
    public bool TraverseReparsePoints { get; set; }
    private enum ParseState
    {
      Start,
      OpenParen,
      CriterionDone,
      ConjunctionPending,
      Whitespace
    }
    private static class RegexAssertions
    {
      public static readonly String PrecededByOddNumberOfSingleQuotes = "(?<=(?:[^']*'[^']*')*'[^']*)";
      public static readonly String FollowedByOddNumberOfSingleQuotesAndLineEnd = "(?=[^']*'(?:[^']*'[^']*')*[^']*$)";
      public static readonly String PrecededByEvenNumberOfSingleQuotes = "(?<=(?:[^']*'[^']*')*[^']*)";
      public static readonly String FollowedByEvenNumberOfSingleQuotesAndLineEnd = "(?=(?:[^']*'[^']*')*[^']*$)";
    }
    private static string NormalizeCriteriaExpression(string source)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500624uL);
      string[][] prPairs = {
        new string[] {
          "([^']*)\\(\\(([^']+)",
          "$1( ($2"
        },
        new string[] {
          "(.)\\)\\)",
          "$1) )"
        },
        new string[] {
          "\\((\\S)",
          "( $1"
        },
        new string[] {
          "(\\S)\\)",
          "$1 )"
        },
        new string[] {
          "^\\)",
          " )"
        },
        new string[] {
          "(\\S)\\(",
          "$1 ("
        },
        new string[] {
          "\\)(\\S)",
          ") $1"
        },
        new string[] {
          "(=)('[^']*')",
          "$1 $2"
        },
        new string[] {
          "([^ !><])(>|<|!=|=)",
          "$1 $2"
        },
        new string[] {
          "(>|<|!=|=)([^ =])",
          "$1 $2"
        },
        new string[] {
          "/",
          "\\"
        }
      };
      string interim = source;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500625uL);
      for (int i = 0; i < prPairs.Length; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500626uL);
        string pattern = RegexAssertions.PrecededByEvenNumberOfSingleQuotes + prPairs[i][0] + RegexAssertions.FollowedByEvenNumberOfSingleQuotesAndLineEnd;
        interim = Regex.Replace(interim, pattern, prPairs[i][1]);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500627uL);
      var regexPattern = "/" + RegexAssertions.FollowedByOddNumberOfSingleQuotesAndLineEnd;
      interim = Regex.Replace(interim, regexPattern, "\\");
      regexPattern = " " + RegexAssertions.FollowedByOddNumberOfSingleQuotesAndLineEnd;
      System.String RNTRNTRNT_122 = Regex.Replace(interim, regexPattern, "\u0006");
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500628uL);
      return RNTRNTRNT_122;
    }
    private static SelectionCriterion _ParseCriterion(String s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500629uL);
      if (s == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500630uL);
        return null;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500631uL);
      s = NormalizeCriteriaExpression(s);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500632uL);
      if (s.IndexOf(" ") == -1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500633uL);
        s = "name = " + s;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500634uL);
      string[] tokens = s.Trim().Split(' ', '\t');
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500635uL);
      if (tokens.Length < 3) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500636uL);
        throw new ArgumentException(s);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500637uL);
      SelectionCriterion current = null;
      LogicalConjunction pendingConjunction = LogicalConjunction.NONE;
      ParseState state;
      var stateStack = new System.Collections.Generic.Stack<ParseState>();
      var critStack = new System.Collections.Generic.Stack<SelectionCriterion>();
      stateStack.Push(ParseState.Start);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500638uL);
      for (int i = 0; i < tokens.Length; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500639uL);
        string tok1 = tokens[i].ToLower();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500640uL);
        switch (tok1) {
          case "and":
          case "xor":
          case "or":
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500648uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500641uL);
              state = stateStack.Peek();
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500642uL);
              if (state != ParseState.CriterionDone) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500643uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500644uL);
              if (tokens.Length <= i + 3) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500645uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500646uL);
              pendingConjunction = (LogicalConjunction)Enum.Parse(typeof(LogicalConjunction), tokens[i].ToUpper(), true);
              current = new CompoundCriterion {
                Left = current,
                Right = null,
                Conjunction = pendingConjunction
              };
              stateStack.Push(state);
              stateStack.Push(ParseState.ConjunctionPending);
              critStack.Push(current);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500647uL);
              break;
            }

          case "(":
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500656uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500649uL);
              state = stateStack.Peek();
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500650uL);
              if (state != ParseState.Start && state != ParseState.ConjunctionPending && state != ParseState.OpenParen) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500651uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500652uL);
              if (tokens.Length <= i + 4) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500653uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500654uL);
              stateStack.Push(ParseState.OpenParen);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500655uL);
              break;
            }

          case ")":
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500662uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500657uL);
              state = stateStack.Pop();
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500658uL);
              if (stateStack.Peek() != ParseState.OpenParen) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500659uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500660uL);
              stateStack.Pop();
              stateStack.Push(ParseState.CriterionDone);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500661uL);
              break;
            }

          case "atime":
          case "ctime":
          case "mtime":
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500677uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500663uL);
              if (tokens.Length <= i + 2) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500664uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500665uL);
              DateTime t;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500666uL);
              try {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500667uL);
                t = DateTime.ParseExact(tokens[i + 2], "yyyy-MM-dd-HH:mm:ss", null);
              } catch (FormatException) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500668uL);
                try {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500669uL);
                  t = DateTime.ParseExact(tokens[i + 2], "yyyy/MM/dd-HH:mm:ss", null);
                } catch (FormatException) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500670uL);
                  try {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500671uL);
                    t = DateTime.ParseExact(tokens[i + 2], "yyyy/MM/dd", null);
                  } catch (FormatException) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500672uL);
                    try {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500673uL);
                      t = DateTime.ParseExact(tokens[i + 2], "MM/dd/yyyy", null);
                    } catch (FormatException) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500674uL);
                      t = DateTime.ParseExact(tokens[i + 2], "yyyy-MM-dd", null);
                    }
                  }
                }
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500675uL);
              t = DateTime.SpecifyKind(t, DateTimeKind.Local).ToUniversalTime();
              current = new TimeCriterion {
                Which = (WhichTime)Enum.Parse(typeof(WhichTime), tokens[i], true),
                Operator = (ComparisonOperator)EnumUtil.Parse(typeof(ComparisonOperator), tokens[i + 1]),
                Time = t
              };
              i += 2;
              stateStack.Push(ParseState.CriterionDone);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500676uL);
              break;
            }

          case "length":
          case "size":
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500691uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500678uL);
              if (tokens.Length <= i + 2) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500679uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500680uL);
              Int64 sz = 0;
              string v = tokens[i + 2];
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500681uL);
              if (v.ToUpper().EndsWith("K")) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500682uL);
                sz = Int64.Parse(v.Substring(0, v.Length - 1)) * 1024;
              } else if (v.ToUpper().EndsWith("KB")) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500688uL);
                sz = Int64.Parse(v.Substring(0, v.Length - 2)) * 1024;
              } else if (v.ToUpper().EndsWith("M")) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500687uL);
                sz = Int64.Parse(v.Substring(0, v.Length - 1)) * 1024 * 1024;
              } else if (v.ToUpper().EndsWith("MB")) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500686uL);
                sz = Int64.Parse(v.Substring(0, v.Length - 2)) * 1024 * 1024;
              } else if (v.ToUpper().EndsWith("G")) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500685uL);
                sz = Int64.Parse(v.Substring(0, v.Length - 1)) * 1024 * 1024 * 1024;
              } else if (v.ToUpper().EndsWith("GB")) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500684uL);
                sz = Int64.Parse(v.Substring(0, v.Length - 2)) * 1024 * 1024 * 1024;
              } else {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500683uL);
                sz = Int64.Parse(tokens[i + 2]);
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500689uL);
              current = new SizeCriterion {
                Size = sz,
                Operator = (ComparisonOperator)EnumUtil.Parse(typeof(ComparisonOperator), tokens[i + 1])
              };
              i += 2;
              stateStack.Push(ParseState.CriterionDone);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500690uL);
              break;
            }

          case "filename":
          case "name":
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500701uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500692uL);
              if (tokens.Length <= i + 2) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500693uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500694uL);
              ComparisonOperator c = (ComparisonOperator)EnumUtil.Parse(typeof(ComparisonOperator), tokens[i + 1]);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500695uL);
              if (c != ComparisonOperator.NotEqualTo && c != ComparisonOperator.EqualTo) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500696uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500697uL);
              string m = tokens[i + 2];
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500698uL);
              if (m.StartsWith("'") && m.EndsWith("'")) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500699uL);
                m = m.Substring(1, m.Length - 2).Replace("\u0006", " ");
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500700uL);
              current = new NameCriterion {
                MatchingFileSpec = m,
                Operator = c
              };
              i += 2;
              stateStack.Push(ParseState.CriterionDone);
            }

            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500703uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500702uL);
              break;
            }

          case "attrs":
          case "attributes":
          case "type":
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500710uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500704uL);
              if (tokens.Length <= i + 2) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500705uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500706uL);
              ComparisonOperator c = (ComparisonOperator)EnumUtil.Parse(typeof(ComparisonOperator), tokens[i + 1]);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500707uL);
              if (c != ComparisonOperator.NotEqualTo && c != ComparisonOperator.EqualTo) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500708uL);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500709uL);
              current = (tok1 == "type") ? (SelectionCriterion)new TypeCriterion {
                AttributeString = tokens[i + 2],
                Operator = c
              } : (SelectionCriterion)new AttributesCriterion {
                AttributeString = tokens[i + 2],
                Operator = c
              };
              i += 2;
              stateStack.Push(ParseState.CriterionDone);
            }

            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500712uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500711uL);
              break;
            }

          case "":
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500715uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500713uL);
              stateStack.Push(ParseState.Whitespace);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500714uL);
              break;
            }

          default:
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500717uL);
            
            {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500716uL);
              throw new ArgumentException("'" + tokens[i] + "'");
            }

        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500718uL);
        state = stateStack.Peek();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500719uL);
        if (state == ParseState.CriterionDone) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500720uL);
          stateStack.Pop();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500721uL);
          if (stateStack.Peek() == ParseState.ConjunctionPending) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500722uL);
            while (stateStack.Peek() == ParseState.ConjunctionPending) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500723uL);
              var cc = critStack.Pop() as CompoundCriterion;
              cc.Right = current;
              current = cc;
              stateStack.Pop();
              state = stateStack.Pop();
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500724uL);
              if (state != ParseState.CriterionDone) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500725uL);
                throw new ArgumentException("??");
              }
            }
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500726uL);
            stateStack.Push(ParseState.CriterionDone);
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500727uL);
        if (state == ParseState.Whitespace) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500728uL);
          stateStack.Pop();
        }
      }
      SelectionCriterion RNTRNTRNT_123 = current;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500729uL);
      return RNTRNTRNT_123;
    }
    public override String ToString()
    {
      String RNTRNTRNT_124 = "FileSelector(" + _Criterion.ToString() + ")";
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500730uL);
      return RNTRNTRNT_124;
    }
    private bool Evaluate(string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500731uL);
      SelectorTrace("Evaluate({0})", filename);
      bool result = _Criterion.Evaluate(filename);
      System.Boolean RNTRNTRNT_125 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500732uL);
      return RNTRNTRNT_125;
    }
    [System.Diagnostics.Conditional("SelectorTrace")]
    private void SelectorTrace(string format, params object[] args)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500733uL);
      if (_Criterion != null && _Criterion.Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500734uL);
        System.Console.WriteLine(format, args);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500735uL);
    }
    public System.Collections.Generic.ICollection<String> SelectFiles(String directory)
    {
      System.Collections.Generic.ICollection<String> RNTRNTRNT_126 = SelectFiles(directory, false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500736uL);
      return RNTRNTRNT_126;
    }
    public System.Collections.ObjectModel.ReadOnlyCollection<String> SelectFiles(String directory, bool recurseDirectories)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500737uL);
      if (_Criterion == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500738uL);
        throw new ArgumentException("SelectionCriteria has not been set");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500739uL);
      var list = new List<String>();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500740uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500741uL);
        if (Directory.Exists(directory)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500742uL);
          String[] filenames = Directory.GetFiles(directory);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500743uL);
          foreach (String filename in filenames) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500744uL);
            if (Evaluate(filename)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500745uL);
              list.Add(filename);
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500746uL);
          if (recurseDirectories) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500747uL);
            String[] dirnames = Directory.GetDirectories(directory);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500748uL);
            foreach (String dir in dirnames) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500749uL);
              if (this.TraverseReparsePoints || ((File.GetAttributes(dir) & FileAttributes.ReparsePoint) == 0)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500750uL);
                if (Evaluate(dir)) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500751uL);
                  list.Add(dir);
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500752uL);
                list.AddRange(this.SelectFiles(dir, recurseDirectories));
              }
            }
          }
        }
      } catch (System.UnauthorizedAccessException) {
      } catch (System.IO.IOException) {
      }
      System.Collections.ObjectModel.ReadOnlyCollection<String> RNTRNTRNT_127 = list.AsReadOnly();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500753uL);
      return RNTRNTRNT_127;
    }
  }
  internal sealed class EnumUtil
  {
    private EnumUtil()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500754uL);
    }
    static internal string GetDescription(System.Enum value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500755uL);
      FieldInfo fi = value.GetType().GetField(value.ToString());
      var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500756uL);
      if (attributes.Length > 0) {
        System.String RNTRNTRNT_128 = attributes[0].Description;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500757uL);
        return RNTRNTRNT_128;
      } else {
        System.String RNTRNTRNT_129 = value.ToString();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500758uL);
        return RNTRNTRNT_129;
      }
    }
    static internal object Parse(Type enumType, string stringRepresentation)
    {
      System.Object RNTRNTRNT_130 = Parse(enumType, stringRepresentation, false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500759uL);
      return RNTRNTRNT_130;
    }
    static internal object Parse(Type enumType, string stringRepresentation, bool ignoreCase)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500760uL);
      if (ignoreCase) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500761uL);
        stringRepresentation = stringRepresentation.ToLower();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500762uL);
      foreach (System.Enum enumVal in System.Enum.GetValues(enumType)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500763uL);
        string description = GetDescription(enumVal);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500764uL);
        if (ignoreCase) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500765uL);
          description = description.ToLower();
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500766uL);
        if (description == stringRepresentation) {
          System.Object RNTRNTRNT_131 = enumVal;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500767uL);
          return RNTRNTRNT_131;
        }
      }
      System.Object RNTRNTRNT_132 = System.Enum.Parse(enumType, stringRepresentation, ignoreCase);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500768uL);
      return RNTRNTRNT_132;
    }
  }
}
