using System;
using System.Collections.Generic;
using System.IO;
namespace Ionic.Zip
{
  internal class ZipSegmentedStream : System.IO.Stream
  {
    enum RwMode
    {
      None = 0,
      ReadOnly = 1,
      Write = 2
    }
    private RwMode rwMode;
    private bool _exceptionPending;
    private string _baseName;
    private string _baseDir;
    private string _currentName;
    private string _currentTempName;
    private uint _currentDiskNumber;
    private uint _maxDiskNumber;
    private int _maxSegmentSize;
    private System.IO.Stream _innerStream;
    private ZipSegmentedStream() : base()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500279uL);
      _exceptionPending = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500280uL);
    }
    public static ZipSegmentedStream ForReading(string name, uint initialDiskNumber, uint maxDiskNumber)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500281uL);
      ZipSegmentedStream zss = new ZipSegmentedStream {
        rwMode = RwMode.ReadOnly,
        CurrentSegment = initialDiskNumber,
        _maxDiskNumber = maxDiskNumber,
        _baseName = name
      };
      zss._SetReadStream();
      ZipSegmentedStream RNTRNTRNT_68 = zss;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500282uL);
      return RNTRNTRNT_68;
    }
    public static ZipSegmentedStream ForWriting(string name, int maxSegmentSize)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500283uL);
      ZipSegmentedStream zss = new ZipSegmentedStream {
        rwMode = RwMode.Write,
        CurrentSegment = 0,
        _baseName = name,
        _maxSegmentSize = maxSegmentSize,
        _baseDir = Path.GetDirectoryName(name)
      };
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500284uL);
      if (zss._baseDir == "") {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500285uL);
        zss._baseDir = ".";
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500286uL);
      zss._SetWriteStream(0);
      ZipSegmentedStream RNTRNTRNT_69 = zss;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500287uL);
      return RNTRNTRNT_69;
    }
    public static Stream ForUpdate(string name, uint diskNumber)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500288uL);
      if (diskNumber >= 99) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500289uL);
        throw new ArgumentOutOfRangeException("diskNumber");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500290uL);
      string fname = String.Format("{0}.z{1:D2}", Path.Combine(Path.GetDirectoryName(name), Path.GetFileNameWithoutExtension(name)), diskNumber + 1);
      Stream RNTRNTRNT_70 = File.Open(fname, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500291uL);
      return RNTRNTRNT_70;
    }
    public bool ContiguousWrite { get; set; }
    public UInt32 CurrentSegment {
      get {
        UInt32 RNTRNTRNT_71 = _currentDiskNumber;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500292uL);
        return RNTRNTRNT_71;
      }
      private set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500293uL);
        _currentDiskNumber = value;
        _currentName = null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500294uL);
      }
    }
    public String CurrentName {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500295uL);
        if (_currentName == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500296uL);
          _currentName = _NameForSegment(CurrentSegment);
        }
        String RNTRNTRNT_72 = _currentName;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500297uL);
        return RNTRNTRNT_72;
      }
    }
    public String CurrentTempName {
      get {
        String RNTRNTRNT_73 = _currentTempName;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500298uL);
        return RNTRNTRNT_73;
      }
    }
    private string _NameForSegment(uint diskNumber)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500299uL);
      if (diskNumber >= 99) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500300uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500301uL);
        throw new OverflowException("The number of zip segments would exceed 99.");
      }
      System.String RNTRNTRNT_74 = String.Format("{0}.z{1:D2}", Path.Combine(Path.GetDirectoryName(_baseName), Path.GetFileNameWithoutExtension(_baseName)), diskNumber + 1);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500302uL);
      return RNTRNTRNT_74;
    }
    public UInt32 ComputeSegment(int length)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500303uL);
      if (_innerStream.Position + length > _maxSegmentSize) {
        UInt32 RNTRNTRNT_75 = CurrentSegment + 1;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500304uL);
        return RNTRNTRNT_75;
      }
      UInt32 RNTRNTRNT_76 = CurrentSegment;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500305uL);
      return RNTRNTRNT_76;
    }
    public override String ToString()
    {
      String RNTRNTRNT_77 = String.Format("{0}[{1}][{2}], pos=0x{3:X})", "ZipSegmentedStream", CurrentName, rwMode.ToString(), this.Position);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500306uL);
      return RNTRNTRNT_77;
    }
    private void _SetReadStream()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500307uL);
      if (_innerStream != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500308uL);
        _innerStream.Dispose();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500309uL);
      if (CurrentSegment + 1 == _maxDiskNumber) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500310uL);
        _currentName = _baseName;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500311uL);
      _innerStream = File.OpenRead(CurrentName);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500312uL);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500313uL);
      if (rwMode != RwMode.ReadOnly) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500314uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500315uL);
        throw new InvalidOperationException("Stream Error: Cannot Read.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500316uL);
      int r = _innerStream.Read(buffer, offset, count);
      int r1 = r;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500317uL);
      while (r1 != count) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500318uL);
        if (_innerStream.Position != _innerStream.Length) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500319uL);
          _exceptionPending = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500320uL);
          throw new ZipException(String.Format("Read error in file {0}", CurrentName));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500321uL);
        if (CurrentSegment + 1 == _maxDiskNumber) {
          System.Int32 RNTRNTRNT_78 = r;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500322uL);
          return RNTRNTRNT_78;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500323uL);
        CurrentSegment++;
        _SetReadStream();
        offset += r1;
        count -= r1;
        r1 = _innerStream.Read(buffer, offset, count);
        r += r1;
      }
      System.Int32 RNTRNTRNT_79 = r;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500324uL);
      return RNTRNTRNT_79;
    }
    private void _SetWriteStream(uint increment)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500325uL);
      if (_innerStream != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500326uL);
        _innerStream.Dispose();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500327uL);
        if (File.Exists(CurrentName)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500328uL);
          File.Delete(CurrentName);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500329uL);
        File.Move(_currentTempName, CurrentName);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500330uL);
      if (increment > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500331uL);
        CurrentSegment += increment;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500332uL);
      SharedUtilities.CreateAndOpenUniqueTempFile(_baseDir, out _innerStream, out _currentTempName);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500333uL);
      if (CurrentSegment == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500334uL);
        _innerStream.Write(BitConverter.GetBytes(ZipConstants.SplitArchiveSignature), 0, 4);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500335uL);
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500336uL);
      if (rwMode != RwMode.Write) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500337uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500338uL);
        throw new InvalidOperationException("Stream Error: Cannot Write.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500339uL);
      if (ContiguousWrite) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500340uL);
        if (_innerStream.Position + count > _maxSegmentSize) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500341uL);
          _SetWriteStream(1);
        }
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500342uL);
        while (_innerStream.Position + count > _maxSegmentSize) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500343uL);
          int c = unchecked(_maxSegmentSize - (int)_innerStream.Position);
          _innerStream.Write(buffer, offset, c);
          _SetWriteStream(1);
          count -= c;
          offset += c;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500344uL);
      _innerStream.Write(buffer, offset, count);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500345uL);
    }
    public long TruncateBackward(uint diskNumber, long offset)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500346uL);
      if (diskNumber >= 99) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500347uL);
        throw new ArgumentOutOfRangeException("diskNumber");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500348uL);
      if (rwMode != RwMode.Write) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500349uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500350uL);
        throw new ZipException("bad state.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500351uL);
      if (diskNumber == CurrentSegment) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500352uL);
        var x = _innerStream.Seek(offset, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_innerStream);
        System.Int64 RNTRNTRNT_80 = x;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500353uL);
        return RNTRNTRNT_80;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500354uL);
      if (_innerStream != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500355uL);
        _innerStream.Dispose();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500356uL);
        if (File.Exists(_currentTempName)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500357uL);
          File.Delete(_currentTempName);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500358uL);
      for (uint j = CurrentSegment - 1; j > diskNumber; j--) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500359uL);
        string s = _NameForSegment(j);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500360uL);
        if (File.Exists(s)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500361uL);
          File.Delete(s);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500362uL);
      CurrentSegment = diskNumber;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500363uL);
      for (int i = 0; i < 3; i++) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500364uL);
        try {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500365uL);
          _currentTempName = SharedUtilities.InternalGetTempFileName();
          File.Move(CurrentName, _currentTempName);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500366uL);
          break;
        } catch (IOException) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500367uL);
          if (i == 2) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500368uL);
            throw;
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500369uL);
      _innerStream = new FileStream(_currentTempName, FileMode.Open);
      var r = _innerStream.Seek(offset, SeekOrigin.Begin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_innerStream);
      System.Int64 RNTRNTRNT_81 = r;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500370uL);
      return RNTRNTRNT_81;
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_82 = (rwMode == RwMode.ReadOnly && (_innerStream != null) && _innerStream.CanRead);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500371uL);
        return RNTRNTRNT_82;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_83 = (_innerStream != null) && _innerStream.CanSeek;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500372uL);
        return RNTRNTRNT_83;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_84 = (rwMode == RwMode.Write) && (_innerStream != null) && _innerStream.CanWrite;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500373uL);
        return RNTRNTRNT_84;
      }
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500374uL);
      _innerStream.Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500375uL);
    }
    public override long Length {
      get {
        System.Int64 RNTRNTRNT_85 = _innerStream.Length;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500376uL);
        return RNTRNTRNT_85;
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_86 = _innerStream.Position;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500377uL);
        return RNTRNTRNT_86;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500378uL);
        _innerStream.Position = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500379uL);
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500380uL);
      var x = _innerStream.Seek(offset, origin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_innerStream);
      System.Int64 RNTRNTRNT_87 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500381uL);
      return RNTRNTRNT_87;
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500382uL);
      if (rwMode != RwMode.Write) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500383uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500384uL);
        throw new InvalidOperationException();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500385uL);
      _innerStream.SetLength(value);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500386uL);
    }
    protected override void Dispose(bool disposing)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500387uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500388uL);
        if (_innerStream != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500389uL);
          _innerStream.Dispose();
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500390uL);
          if (rwMode == RwMode.Write) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500391uL);
            if (_exceptionPending) {
            } else {
            }
          }
        }
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500392uL);
        base.Dispose(disposing);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500393uL);
    }
  }
}
