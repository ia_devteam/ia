using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public partial class ZipFile
  {
    public ZipEntry AddItem(string fileOrDirectoryName)
    {
      ZipEntry RNTRNTRNT_340 = AddItem(fileOrDirectoryName, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502480uL);
      return RNTRNTRNT_340;
    }
    public ZipEntry AddItem(String fileOrDirectoryName, String directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502481uL);
      if (File.Exists(fileOrDirectoryName)) {
        ZipEntry RNTRNTRNT_341 = AddFile(fileOrDirectoryName, directoryPathInArchive);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502482uL);
        return RNTRNTRNT_341;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502483uL);
      if (Directory.Exists(fileOrDirectoryName)) {
        ZipEntry RNTRNTRNT_342 = AddDirectory(fileOrDirectoryName, directoryPathInArchive);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502484uL);
        return RNTRNTRNT_342;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502485uL);
      throw new FileNotFoundException(String.Format("That file or directory ({0}) does not exist!", fileOrDirectoryName));
    }
    public ZipEntry AddFile(string fileName)
    {
      ZipEntry RNTRNTRNT_343 = AddFile(fileName, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502486uL);
      return RNTRNTRNT_343;
    }
    public ZipEntry AddFile(string fileName, String directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502487uL);
      string nameInArchive = ZipEntry.NameInArchive(fileName, directoryPathInArchive);
      ZipEntry ze = ZipEntry.CreateFromFile(fileName, nameInArchive);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502488uL);
      if (Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502489uL);
        StatusMessageTextWriter.WriteLine("adding {0}...", fileName);
      }
      ZipEntry RNTRNTRNT_344 = _InternalAddEntry(ze);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502490uL);
      return RNTRNTRNT_344;
    }
    public void RemoveEntries(System.Collections.Generic.ICollection<ZipEntry> entriesToRemove)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502491uL);
      if (entriesToRemove == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502492uL);
        throw new ArgumentNullException("entriesToRemove");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502493uL);
      foreach (ZipEntry e in entriesToRemove) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502494uL);
        this.RemoveEntry(e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502495uL);
    }
    public void RemoveEntries(System.Collections.Generic.ICollection<String> entriesToRemove)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502496uL);
      if (entriesToRemove == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502497uL);
        throw new ArgumentNullException("entriesToRemove");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502498uL);
      foreach (String e in entriesToRemove) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502499uL);
        this.RemoveEntry(e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502500uL);
    }
    public void AddFiles(System.Collections.Generic.IEnumerable<String> fileNames)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502501uL);
      this.AddFiles(fileNames, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502502uL);
    }
    public void UpdateFiles(System.Collections.Generic.IEnumerable<String> fileNames)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502503uL);
      this.UpdateFiles(fileNames, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502504uL);
    }
    public void AddFiles(System.Collections.Generic.IEnumerable<String> fileNames, String directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502505uL);
      AddFiles(fileNames, false, directoryPathInArchive);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502506uL);
    }
    public void AddFiles(System.Collections.Generic.IEnumerable<String> fileNames, bool preserveDirHierarchy, String directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502507uL);
      if (fileNames == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502508uL);
        throw new ArgumentNullException("fileNames");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502509uL);
      _addOperationCanceled = false;
      OnAddStarted();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502510uL);
      if (preserveDirHierarchy) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502511uL);
        foreach (var f in fileNames) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502512uL);
          if (_addOperationCanceled) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502513uL);
            break;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502514uL);
          if (directoryPathInArchive != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502515uL);
            string s = Path.GetFullPath(Path.Combine(directoryPathInArchive, Path.GetDirectoryName(f)));
            this.AddFile(f, s);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502516uL);
            this.AddFile(f, null);
          }
        }
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502517uL);
        foreach (var f in fileNames) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502518uL);
          if (_addOperationCanceled) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502519uL);
            break;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502520uL);
          this.AddFile(f, directoryPathInArchive);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502521uL);
      if (!_addOperationCanceled) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502522uL);
        OnAddCompleted();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502523uL);
    }
    public void UpdateFiles(System.Collections.Generic.IEnumerable<String> fileNames, String directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502524uL);
      if (fileNames == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502525uL);
        throw new ArgumentNullException("fileNames");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502526uL);
      OnAddStarted();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502527uL);
      foreach (var f in fileNames) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502528uL);
        this.UpdateFile(f, directoryPathInArchive);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502529uL);
      OnAddCompleted();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502530uL);
    }
    public ZipEntry UpdateFile(string fileName)
    {
      ZipEntry RNTRNTRNT_345 = UpdateFile(fileName, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502531uL);
      return RNTRNTRNT_345;
    }
    public ZipEntry UpdateFile(string fileName, String directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502532uL);
      var key = ZipEntry.NameInArchive(fileName, directoryPathInArchive);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502533uL);
      if (this[key] != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502534uL);
        this.RemoveEntry(key);
      }
      ZipEntry RNTRNTRNT_346 = this.AddFile(fileName, directoryPathInArchive);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502535uL);
      return RNTRNTRNT_346;
    }
    public ZipEntry UpdateDirectory(string directoryName)
    {
      ZipEntry RNTRNTRNT_347 = UpdateDirectory(directoryName, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502536uL);
      return RNTRNTRNT_347;
    }
    public ZipEntry UpdateDirectory(string directoryName, String directoryPathInArchive)
    {
      ZipEntry RNTRNTRNT_348 = this.AddOrUpdateDirectoryImpl(directoryName, directoryPathInArchive, AddOrUpdateAction.AddOrUpdate);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502537uL);
      return RNTRNTRNT_348;
    }
    public void UpdateItem(string itemName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502538uL);
      UpdateItem(itemName, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502539uL);
    }
    public void UpdateItem(string itemName, string directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502540uL);
      if (File.Exists(itemName)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502541uL);
        UpdateFile(itemName, directoryPathInArchive);
      } else if (Directory.Exists(itemName)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502543uL);
        UpdateDirectory(itemName, directoryPathInArchive);
      } else {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502542uL);
        throw new FileNotFoundException(String.Format("That file or directory ({0}) does not exist!", itemName));
      }
    }
    public ZipEntry AddEntry(string entryName, string content)
    {
      ZipEntry RNTRNTRNT_349 = AddEntry(entryName, content, System.Text.Encoding.Default);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502544uL);
      return RNTRNTRNT_349;
    }
    public ZipEntry AddEntry(string entryName, string content, System.Text.Encoding encoding)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502545uL);
      var ms = new MemoryStream();
      var sw = new StreamWriter(ms, encoding);
      sw.Write(content);
      sw.Flush();
      ms.Seek(0, SeekOrigin.Begin);
      ZipEntry RNTRNTRNT_350 = AddEntry(entryName, ms);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502546uL);
      return RNTRNTRNT_350;
    }
    public ZipEntry AddEntry(string entryName, Stream stream)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502547uL);
      ZipEntry ze = ZipEntry.CreateForStream(entryName, stream);
      ze.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502548uL);
      if (Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502549uL);
        StatusMessageTextWriter.WriteLine("adding {0}...", entryName);
      }
      ZipEntry RNTRNTRNT_351 = _InternalAddEntry(ze);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502550uL);
      return RNTRNTRNT_351;
    }
    public ZipEntry AddEntry(string entryName, WriteDelegate writer)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502551uL);
      ZipEntry ze = ZipEntry.CreateForWriter(entryName, writer);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502552uL);
      if (Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502553uL);
        StatusMessageTextWriter.WriteLine("adding {0}...", entryName);
      }
      ZipEntry RNTRNTRNT_352 = _InternalAddEntry(ze);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502554uL);
      return RNTRNTRNT_352;
    }
    public ZipEntry AddEntry(string entryName, OpenDelegate opener, CloseDelegate closer)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502555uL);
      ZipEntry ze = ZipEntry.CreateForJitStreamProvider(entryName, opener, closer);
      ze.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502556uL);
      if (Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502557uL);
        StatusMessageTextWriter.WriteLine("adding {0}...", entryName);
      }
      ZipEntry RNTRNTRNT_353 = _InternalAddEntry(ze);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502558uL);
      return RNTRNTRNT_353;
    }
    private ZipEntry _InternalAddEntry(ZipEntry ze)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502559uL);
      ze._container = new ZipContainer(this);
      ze.CompressionMethod = this.CompressionMethod;
      ze.CompressionLevel = this.CompressionLevel;
      ze.ExtractExistingFile = this.ExtractExistingFile;
      ze.ZipErrorAction = this.ZipErrorAction;
      ze.SetCompression = this.SetCompression;
      ze.AlternateEncoding = this.AlternateEncoding;
      ze.AlternateEncodingUsage = this.AlternateEncodingUsage;
      ze.Password = this._Password;
      ze.Encryption = this.Encryption;
      ze.EmitTimesInWindowsFormatWhenSaving = this._emitNtfsTimes;
      ze.EmitTimesInUnixFormatWhenSaving = this._emitUnixTimes;
      InternalAddEntry(ze.FileName, ze);
      AfterAddEntry(ze);
      ZipEntry RNTRNTRNT_354 = ze;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502560uL);
      return RNTRNTRNT_354;
    }
    public ZipEntry UpdateEntry(string entryName, string content)
    {
      ZipEntry RNTRNTRNT_355 = UpdateEntry(entryName, content, System.Text.Encoding.Default);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502561uL);
      return RNTRNTRNT_355;
    }
    public ZipEntry UpdateEntry(string entryName, string content, System.Text.Encoding encoding)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502562uL);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_356 = AddEntry(entryName, content, encoding);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502563uL);
      return RNTRNTRNT_356;
    }
    public ZipEntry UpdateEntry(string entryName, WriteDelegate writer)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502564uL);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_357 = AddEntry(entryName, writer);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502565uL);
      return RNTRNTRNT_357;
    }
    public ZipEntry UpdateEntry(string entryName, OpenDelegate opener, CloseDelegate closer)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502566uL);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_358 = AddEntry(entryName, opener, closer);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502567uL);
      return RNTRNTRNT_358;
    }
    public ZipEntry UpdateEntry(string entryName, Stream stream)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502568uL);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_359 = AddEntry(entryName, stream);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502569uL);
      return RNTRNTRNT_359;
    }
    private void RemoveEntryForUpdate(string entryName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502570uL);
      if (String.IsNullOrEmpty(entryName)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502571uL);
        throw new ArgumentNullException("entryName");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502572uL);
      string directoryPathInArchive = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502573uL);
      if (entryName.IndexOf('\\') != -1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502574uL);
        directoryPathInArchive = Path.GetDirectoryName(entryName);
        entryName = Path.GetFileName(entryName);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502575uL);
      var key = ZipEntry.NameInArchive(entryName, directoryPathInArchive);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502576uL);
      if (this[key] != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502577uL);
        this.RemoveEntry(key);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502578uL);
    }
    public ZipEntry AddEntry(string entryName, byte[] byteContent)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502579uL);
      if (byteContent == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502580uL);
        throw new ArgumentException("bad argument", "byteContent");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502581uL);
      var ms = new MemoryStream(byteContent);
      ZipEntry RNTRNTRNT_360 = AddEntry(entryName, ms);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502582uL);
      return RNTRNTRNT_360;
    }
    public ZipEntry UpdateEntry(string entryName, byte[] byteContent)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502583uL);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_361 = AddEntry(entryName, byteContent);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502584uL);
      return RNTRNTRNT_361;
    }
    public ZipEntry AddDirectory(string directoryName)
    {
      ZipEntry RNTRNTRNT_362 = AddDirectory(directoryName, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502585uL);
      return RNTRNTRNT_362;
    }
    public ZipEntry AddDirectory(string directoryName, string directoryPathInArchive)
    {
      ZipEntry RNTRNTRNT_363 = AddOrUpdateDirectoryImpl(directoryName, directoryPathInArchive, AddOrUpdateAction.AddOnly);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502586uL);
      return RNTRNTRNT_363;
    }
    public ZipEntry AddDirectoryByName(string directoryNameInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502587uL);
      ZipEntry dir = ZipEntry.CreateFromNothing(directoryNameInArchive);
      dir._container = new ZipContainer(this);
      dir.MarkAsDirectory();
      dir.AlternateEncoding = this.AlternateEncoding;
      dir.AlternateEncodingUsage = this.AlternateEncodingUsage;
      dir.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
      dir.EmitTimesInWindowsFormatWhenSaving = _emitNtfsTimes;
      dir.EmitTimesInUnixFormatWhenSaving = _emitUnixTimes;
      dir._Source = ZipEntrySource.Stream;
      InternalAddEntry(dir.FileName, dir);
      AfterAddEntry(dir);
      ZipEntry RNTRNTRNT_364 = dir;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502588uL);
      return RNTRNTRNT_364;
    }
    private ZipEntry AddOrUpdateDirectoryImpl(string directoryName, string rootDirectoryPathInArchive, AddOrUpdateAction action)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502589uL);
      if (rootDirectoryPathInArchive == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502590uL);
        rootDirectoryPathInArchive = "";
      }
      ZipEntry RNTRNTRNT_365 = AddOrUpdateDirectoryImpl(directoryName, rootDirectoryPathInArchive, action, true, 0);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502591uL);
      return RNTRNTRNT_365;
    }
    internal void InternalAddEntry(String name, ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502592uL);
      _entries.Add(name, entry);
      _zipEntriesAsList = null;
      _contentsChanged = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502593uL);
    }
    private ZipEntry AddOrUpdateDirectoryImpl(string directoryName, string rootDirectoryPathInArchive, AddOrUpdateAction action, bool recurse, int level)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502594uL);
      if (Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502595uL);
        StatusMessageTextWriter.WriteLine("{0} {1}...", (action == AddOrUpdateAction.AddOnly) ? "adding" : "Adding or updating", directoryName);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502596uL);
      if (level == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502597uL);
        _addOperationCanceled = false;
        OnAddStarted();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502598uL);
      if (_addOperationCanceled) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502599uL);
        return null;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502600uL);
      string dirForEntries = rootDirectoryPathInArchive;
      ZipEntry baseDir = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502601uL);
      if (level > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502602uL);
        int f = directoryName.Length;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502603uL);
        for (int i = level; i > 0; i--) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502604uL);
          f = directoryName.LastIndexOfAny("/\\".ToCharArray(), f - 1, f - 1);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502605uL);
        dirForEntries = directoryName.Substring(f + 1);
        dirForEntries = Path.Combine(rootDirectoryPathInArchive, dirForEntries);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502606uL);
      if (level > 0 || rootDirectoryPathInArchive != "") {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502607uL);
        baseDir = ZipEntry.CreateFromFile(directoryName, dirForEntries);
        baseDir._container = new ZipContainer(this);
        baseDir.AlternateEncoding = this.AlternateEncoding;
        baseDir.AlternateEncodingUsage = this.AlternateEncodingUsage;
        baseDir.MarkAsDirectory();
        baseDir.EmitTimesInWindowsFormatWhenSaving = _emitNtfsTimes;
        baseDir.EmitTimesInUnixFormatWhenSaving = _emitUnixTimes;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502608uL);
        if (!_entries.ContainsKey(baseDir.FileName)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502609uL);
          InternalAddEntry(baseDir.FileName, baseDir);
          AfterAddEntry(baseDir);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502610uL);
        dirForEntries = baseDir.FileName;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502611uL);
      if (!_addOperationCanceled) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502612uL);
        String[] filenames = Directory.GetFiles(directoryName);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502613uL);
        if (recurse) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502614uL);
          foreach (String filename in filenames) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502615uL);
            if (_addOperationCanceled) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502616uL);
              break;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502617uL);
            if (action == AddOrUpdateAction.AddOnly) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502618uL);
              AddFile(filename, dirForEntries);
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502619uL);
              UpdateFile(filename, dirForEntries);
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502620uL);
          if (!_addOperationCanceled) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502621uL);
            String[] dirnames = Directory.GetDirectories(directoryName);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502622uL);
            foreach (String dir in dirnames) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502623uL);
              FileAttributes fileAttrs = System.IO.File.GetAttributes(dir);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502624uL);
              if (this.AddDirectoryWillTraverseReparsePoints || ((fileAttrs & FileAttributes.ReparsePoint) == 0)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502625uL);
                AddOrUpdateDirectoryImpl(dir, rootDirectoryPathInArchive, action, recurse, level + 1);
              }
            }
          }
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502626uL);
      if (level == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502627uL);
        OnAddCompleted();
      }
      ZipEntry RNTRNTRNT_366 = baseDir;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502628uL);
      return RNTRNTRNT_366;
    }
  }
}
