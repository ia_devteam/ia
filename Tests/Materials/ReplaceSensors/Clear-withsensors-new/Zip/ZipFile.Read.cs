using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public class ReadOptions
  {
    public EventHandler<ReadProgressEventArgs> ReadProgress { get; set; }
    public TextWriter StatusMessageWriter { get; set; }
    public System.Text.Encoding Encoding { get; set; }
  }
  public partial class ZipFile
  {
    public static ZipFile Read(string fileName)
    {
      ZipFile RNTRNTRNT_427 = ZipFile.Read(fileName, null, null, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503035uL);
      return RNTRNTRNT_427;
    }
    public static ZipFile Read(string fileName, ReadOptions options)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503036uL);
      if (options == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503037uL);
        throw new ArgumentNullException("options");
      }
      ZipFile RNTRNTRNT_428 = Read(fileName, options.StatusMessageWriter, options.Encoding, options.ReadProgress);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503038uL);
      return RNTRNTRNT_428;
    }
    private static ZipFile Read(string fileName, TextWriter statusMessageWriter, System.Text.Encoding encoding, EventHandler<ReadProgressEventArgs> readProgress)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503039uL);
      ZipFile zf = new ZipFile();
      zf.AlternateEncoding = encoding ?? DefaultEncoding;
      zf.AlternateEncodingUsage = ZipOption.Always;
      zf._StatusMessageTextWriter = statusMessageWriter;
      zf._name = fileName;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503040uL);
      if (readProgress != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503041uL);
        zf.ReadProgress = readProgress;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503042uL);
      if (zf.Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503043uL);
        zf._StatusMessageTextWriter.WriteLine("reading from {0}...", fileName);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503044uL);
      ReadIntoInstance(zf);
      zf._fileAlreadyExists = true;
      ZipFile RNTRNTRNT_429 = zf;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503045uL);
      return RNTRNTRNT_429;
    }
    public static ZipFile Read(Stream zipStream)
    {
      ZipFile RNTRNTRNT_430 = Read(zipStream, null, null, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503046uL);
      return RNTRNTRNT_430;
    }
    public static ZipFile Read(Stream zipStream, ReadOptions options)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503047uL);
      if (options == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503048uL);
        throw new ArgumentNullException("options");
      }
      ZipFile RNTRNTRNT_431 = Read(zipStream, options.StatusMessageWriter, options.Encoding, options.ReadProgress);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503049uL);
      return RNTRNTRNT_431;
    }
    private static ZipFile Read(Stream zipStream, TextWriter statusMessageWriter, System.Text.Encoding encoding, EventHandler<ReadProgressEventArgs> readProgress)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503050uL);
      if (zipStream == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503051uL);
        throw new ArgumentNullException("zipStream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503052uL);
      ZipFile zf = new ZipFile();
      zf._StatusMessageTextWriter = statusMessageWriter;
      zf._alternateEncoding = encoding ?? ZipFile.DefaultEncoding;
      zf._alternateEncodingUsage = ZipOption.Always;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503053uL);
      if (readProgress != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503054uL);
        zf.ReadProgress += readProgress;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503055uL);
      zf._readstream = (zipStream.Position == 0L) ? zipStream : new OffsetStream(zipStream);
      zf._ReadStreamIsOurs = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503056uL);
      if (zf.Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503057uL);
        zf._StatusMessageTextWriter.WriteLine("reading from stream...");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503058uL);
      ReadIntoInstance(zf);
      ZipFile RNTRNTRNT_432 = zf;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503059uL);
      return RNTRNTRNT_432;
    }
    private static void ReadIntoInstance(ZipFile zf)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503060uL);
      Stream s = zf.ReadStream;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503061uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503062uL);
        zf._readName = zf._name;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503063uL);
        if (!s.CanSeek) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503064uL);
          ReadIntoInstance_Orig(zf);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503065uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503066uL);
        zf.OnReadStarted();
        uint datum = ReadFirstFourBytes(s);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503067uL);
        if (datum == ZipConstants.EndOfCentralDirectorySignature) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503068uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503069uL);
        int nTries = 0;
        bool success = false;
        long posn = s.Length - 64;
        long maxSeekback = Math.Max(s.Length - 0x4000, 10);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503070uL);
        do {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503071uL);
          if (posn < 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503072uL);
            posn = 0;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503073uL);
          s.Seek(posn, SeekOrigin.Begin);
          long bytesRead = SharedUtilities.FindSignature(s, (int)ZipConstants.EndOfCentralDirectorySignature);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503074uL);
          if (bytesRead != -1) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503075uL);
            success = true;
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503076uL);
            if (posn == 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503077uL);
              break;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503078uL);
            nTries++;
            posn -= (32 * (nTries + 1) * nTries);
          }
        } while (!success && posn > maxSeekback);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503079uL);
        if (success) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503080uL);
          zf._locEndOfCDS = s.Position - 4;
          byte[] block = new byte[16];
          s.Read(block, 0, block.Length);
          zf._diskNumberWithCd = BitConverter.ToUInt16(block, 2);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503081uL);
          if (zf._diskNumberWithCd == 0xffff) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503082uL);
            throw new ZipException("Spanned archives with more than 65534 segments are not supported at this time.");
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503083uL);
          zf._diskNumberWithCd++;
          int i = 12;
          uint offset32 = (uint)BitConverter.ToUInt32(block, i);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503084uL);
          if (offset32 == 0xffffffffu) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503085uL);
            Zip64SeekToCentralDirectory(zf);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503086uL);
            zf._OffsetOfCentralDirectory = offset32;
            s.Seek(offset32, SeekOrigin.Begin);
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503087uL);
          ReadCentralDirectory(zf);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503088uL);
          s.Seek(0L, SeekOrigin.Begin);
          ReadIntoInstance_Orig(zf);
        }
      } catch (Exception ex1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503089uL);
        if (zf._ReadStreamIsOurs && zf._readstream != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503090uL);
          try {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503091uL);
            zf._readstream.Dispose();
            zf._readstream = null;
          } finally {
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503092uL);
        throw new ZipException("Cannot read that as a ZipFile", ex1);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503093uL);
      zf._contentsChanged = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503094uL);
    }
    private static void Zip64SeekToCentralDirectory(ZipFile zf)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503095uL);
      Stream s = zf.ReadStream;
      byte[] block = new byte[16];
      s.Seek(-40, SeekOrigin.Current);
      s.Read(block, 0, 16);
      Int64 offset64 = BitConverter.ToInt64(block, 8);
      zf._OffsetOfCentralDirectory = 0xffffffffu;
      zf._OffsetOfCentralDirectory64 = offset64;
      s.Seek(offset64, SeekOrigin.Begin);
      uint datum = (uint)Ionic.Zip.SharedUtilities.ReadInt(s);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503096uL);
      if (datum != ZipConstants.Zip64EndOfCentralDirectoryRecordSignature) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503097uL);
        throw new BadReadException(String.Format("  Bad signature (0x{0:X8}) looking for ZIP64 EoCD Record at position 0x{1:X8}", datum, s.Position));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503098uL);
      s.Read(block, 0, 8);
      Int64 Size = BitConverter.ToInt64(block, 0);
      block = new byte[Size];
      s.Read(block, 0, block.Length);
      offset64 = BitConverter.ToInt64(block, 36);
      s.Seek(offset64, SeekOrigin.Begin);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503099uL);
    }
    private static uint ReadFirstFourBytes(Stream s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503100uL);
      uint datum = (uint)Ionic.Zip.SharedUtilities.ReadInt(s);
      System.UInt32 RNTRNTRNT_433 = datum;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503101uL);
      return RNTRNTRNT_433;
    }
    private static void ReadCentralDirectory(ZipFile zf)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503102uL);
      bool inputUsesZip64 = false;
      ZipEntry de;
      var previouslySeen = new Dictionary<String, object>();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503103uL);
      while ((de = ZipEntry.ReadDirEntry(zf, previouslySeen)) != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503104uL);
        de.ResetDirEntry();
        zf.OnReadEntry(true, null);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503105uL);
        if (zf.Verbose) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503106uL);
          zf.StatusMessageTextWriter.WriteLine("entry {0}", de.FileName);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503107uL);
        zf._entries.Add(de.FileName, de);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503108uL);
        if (de._InputUsesZip64) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503109uL);
          inputUsesZip64 = true;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503110uL);
        previouslySeen.Add(de.FileName, null);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503111uL);
      if (inputUsesZip64) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503112uL);
        zf.UseZip64WhenSaving = Zip64Option.Always;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503113uL);
      if (zf._locEndOfCDS > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503114uL);
        zf.ReadStream.Seek(zf._locEndOfCDS, SeekOrigin.Begin);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503115uL);
      ReadCentralDirectoryFooter(zf);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503116uL);
      if (zf.Verbose && !String.IsNullOrEmpty(zf.Comment)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503117uL);
        zf.StatusMessageTextWriter.WriteLine("Zip file Comment: {0}", zf.Comment);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503118uL);
      if (zf.Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503119uL);
        zf.StatusMessageTextWriter.WriteLine("read in {0} entries.", zf._entries.Count);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503120uL);
      zf.OnReadCompleted();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503121uL);
    }
    private static void ReadIntoInstance_Orig(ZipFile zf)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503122uL);
      zf.OnReadStarted();
      zf._entries = new System.Collections.Generic.Dictionary<String, ZipEntry>();
      ZipEntry e;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503123uL);
      if (zf.Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503124uL);
        if (zf.Name == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503125uL);
          zf.StatusMessageTextWriter.WriteLine("Reading zip from stream...");
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503126uL);
          zf.StatusMessageTextWriter.WriteLine("Reading zip {0}...", zf.Name);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503127uL);
      bool firstEntry = true;
      ZipContainer zc = new ZipContainer(zf);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503128uL);
      while ((e = ZipEntry.ReadEntry(zc, firstEntry)) != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503129uL);
        if (zf.Verbose) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503130uL);
          zf.StatusMessageTextWriter.WriteLine("  {0}", e.FileName);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503131uL);
        zf._entries.Add(e.FileName, e);
        firstEntry = false;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503132uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503133uL);
        ZipEntry de;
        var previouslySeen = new Dictionary<String, Object>();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503134uL);
        while ((de = ZipEntry.ReadDirEntry(zf, previouslySeen)) != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503135uL);
          ZipEntry e1 = zf._entries[de.FileName];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503136uL);
          if (e1 != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503137uL);
            e1._Comment = de.Comment;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503138uL);
            if (de.IsDirectory) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503139uL);
              e1.MarkAsDirectory();
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503140uL);
          previouslySeen.Add(de.FileName, null);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503141uL);
        if (zf._locEndOfCDS > 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503142uL);
          zf.ReadStream.Seek(zf._locEndOfCDS, SeekOrigin.Begin);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503143uL);
        ReadCentralDirectoryFooter(zf);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503144uL);
        if (zf.Verbose && !String.IsNullOrEmpty(zf.Comment)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503145uL);
          zf.StatusMessageTextWriter.WriteLine("Zip file Comment: {0}", zf.Comment);
        }
      } catch (ZipException) {
      } catch (IOException) {
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503146uL);
      zf.OnReadCompleted();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503147uL);
    }
    private static void ReadCentralDirectoryFooter(ZipFile zf)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503148uL);
      Stream s = zf.ReadStream;
      int signature = Ionic.Zip.SharedUtilities.ReadSignature(s);
      byte[] block = null;
      int j = 0;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503149uL);
      if (signature == ZipConstants.Zip64EndOfCentralDirectoryRecordSignature) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503150uL);
        block = new byte[8 + 44];
        s.Read(block, 0, block.Length);
        Int64 DataSize = BitConverter.ToInt64(block, 0);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503151uL);
        if (DataSize < 44) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503152uL);
          throw new ZipException("Bad size in the ZIP64 Central Directory.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503153uL);
        zf._versionMadeBy = BitConverter.ToUInt16(block, j);
        j += 2;
        zf._versionNeededToExtract = BitConverter.ToUInt16(block, j);
        j += 2;
        zf._diskNumberWithCd = BitConverter.ToUInt32(block, j);
        j += 2;
        block = new byte[DataSize - 44];
        s.Read(block, 0, block.Length);
        signature = Ionic.Zip.SharedUtilities.ReadSignature(s);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503154uL);
        if (signature != ZipConstants.Zip64EndOfCentralDirectoryLocatorSignature) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503155uL);
          throw new ZipException("Inconsistent metadata in the ZIP64 Central Directory.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503156uL);
        block = new byte[16];
        s.Read(block, 0, block.Length);
        signature = Ionic.Zip.SharedUtilities.ReadSignature(s);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503157uL);
      if (signature != ZipConstants.EndOfCentralDirectorySignature) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503158uL);
        s.Seek(-4, SeekOrigin.Current);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503159uL);
        throw new BadReadException(String.Format("Bad signature ({0:X8}) at position 0x{1:X8}", signature, s.Position));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503160uL);
      block = new byte[16];
      zf.ReadStream.Read(block, 0, block.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503161uL);
      if (zf._diskNumberWithCd == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503162uL);
        zf._diskNumberWithCd = BitConverter.ToUInt16(block, 2);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503163uL);
      ReadZipFileComment(zf);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503164uL);
    }
    private static void ReadZipFileComment(ZipFile zf)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503165uL);
      byte[] block = new byte[2];
      zf.ReadStream.Read(block, 0, block.Length);
      Int16 commentLength = (short)(block[0] + block[1] * 256);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503166uL);
      if (commentLength > 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503167uL);
        block = new byte[commentLength];
        zf.ReadStream.Read(block, 0, block.Length);
        string s1 = zf.AlternateEncoding.GetString(block, 0, block.Length);
        zf.Comment = s1;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503168uL);
    }
    public static bool IsZipFile(string fileName)
    {
      System.Boolean RNTRNTRNT_434 = IsZipFile(fileName, false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503169uL);
      return RNTRNTRNT_434;
    }
    public static bool IsZipFile(string fileName, bool testExtract)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503170uL);
      bool result = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503171uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503172uL);
        if (!File.Exists(fileName)) {
          System.Boolean RNTRNTRNT_435 = false;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503173uL);
          return RNTRNTRNT_435;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503174uL);
        using (var s = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503175uL);
          result = IsZipFile(s, testExtract);
        }
      } catch (IOException) {
      } catch (ZipException) {
      }
      System.Boolean RNTRNTRNT_436 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503176uL);
      return RNTRNTRNT_436;
    }
    public static bool IsZipFile(Stream stream, bool testExtract)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503177uL);
      if (stream == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503178uL);
        throw new ArgumentNullException("stream");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503179uL);
      bool result = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503180uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503181uL);
        if (!stream.CanRead) {
          System.Boolean RNTRNTRNT_437 = false;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503182uL);
          return RNTRNTRNT_437;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503183uL);
        var bitBucket = Stream.Null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503184uL);
        using (ZipFile zip1 = ZipFile.Read(stream, null, null, null)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503185uL);
          if (testExtract) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503186uL);
            foreach (var e in zip1) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503187uL);
              if (!e.IsDirectory) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503188uL);
                e.Extract(bitBucket);
              }
            }
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503189uL);
        result = true;
      } catch (IOException) {
      } catch (ZipException) {
      }
      System.Boolean RNTRNTRNT_438 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503190uL);
      return RNTRNTRNT_438;
    }
  }
}
