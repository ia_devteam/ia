using System;
using System.Threading;
using System.Collections.Generic;
using System.IO;
using Ionic.Zip;
namespace Ionic.Zip
{
  public class ZipInputStream : Stream
  {
    public ZipInputStream(Stream stream) : this(stream, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500217uL);
    }
    public ZipInputStream(String fileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500218uL);
      Stream stream = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
      _Init(stream, false, fileName);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500219uL);
    }
    public ZipInputStream(Stream stream, bool leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500220uL);
      _Init(stream, leaveOpen, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500221uL);
    }
    private void _Init(Stream stream, bool leaveOpen, string name)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500222uL);
      _inputStream = stream;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500223uL);
      if (!_inputStream.CanRead) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500224uL);
        throw new ZipException("The stream must be readable.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500225uL);
      _container = new ZipContainer(this);
      _provisionalAlternateEncoding = System.Text.Encoding.GetEncoding("IBM437");
      _leaveUnderlyingStreamOpen = leaveOpen;
      _findRequired = true;
      _name = name ?? "(stream)";
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500226uL);
    }
    public override String ToString()
    {
      String RNTRNTRNT_56 = String.Format("ZipInputStream::{0}(leaveOpen({1})))", _name, _leaveUnderlyingStreamOpen);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500227uL);
      return RNTRNTRNT_56;
    }
    public System.Text.Encoding ProvisionalAlternateEncoding {
      get {
        System.Text.Encoding RNTRNTRNT_57 = _provisionalAlternateEncoding;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500228uL);
        return RNTRNTRNT_57;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500229uL);
        _provisionalAlternateEncoding = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500230uL);
      }
    }
    public int CodecBufferSize { get; set; }
    public String Password {
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500231uL);
        if (_closed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500232uL);
          _exceptionPending = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500233uL);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500234uL);
        _Password = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500235uL);
      }
    }
    private void SetupStream()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500236uL);
      _crcStream = _currentEntry.InternalOpenReader(_Password);
      _LeftToRead = _crcStream.Length;
      _needSetup = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500237uL);
    }
    internal Stream ReadStream {
      get {
        Stream RNTRNTRNT_58 = _inputStream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500238uL);
        return RNTRNTRNT_58;
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500239uL);
      if (_closed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500240uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500241uL);
        throw new System.InvalidOperationException("The stream has been closed.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500242uL);
      if (_needSetup) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500243uL);
        SetupStream();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500244uL);
      if (_LeftToRead == 0) {
        System.Int32 RNTRNTRNT_59 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500245uL);
        return RNTRNTRNT_59;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500246uL);
      int len = (_LeftToRead > count) ? count : (int)_LeftToRead;
      int n = _crcStream.Read(buffer, offset, len);
      _LeftToRead -= n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500247uL);
      if (_LeftToRead == 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500248uL);
        int CrcResult = _crcStream.Crc;
        _currentEntry.VerifyCrcAfterExtract(CrcResult);
        _inputStream.Seek(_endOfEntry, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_inputStream);
      }
      System.Int32 RNTRNTRNT_60 = n;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500249uL);
      return RNTRNTRNT_60;
    }
    public ZipEntry GetNextEntry()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500250uL);
      if (_findRequired) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500251uL);
        long d = SharedUtilities.FindSignature(_inputStream, ZipConstants.ZipEntrySignature);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500252uL);
        if (d == -1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500253uL);
          return null;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500254uL);
        _inputStream.Seek(-4, SeekOrigin.Current);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_inputStream);
      } else if (_firstEntry) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500255uL);
        _inputStream.Seek(_endOfEntry, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_inputStream);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500256uL);
      _currentEntry = ZipEntry.ReadEntry(_container, !_firstEntry);
      _endOfEntry = _inputStream.Position;
      _firstEntry = true;
      _needSetup = true;
      _findRequired = false;
      ZipEntry RNTRNTRNT_61 = _currentEntry;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500257uL);
      return RNTRNTRNT_61;
    }
    protected override void Dispose(bool disposing)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500258uL);
      if (_closed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500259uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500260uL);
      if (disposing) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500261uL);
        if (_exceptionPending) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500262uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500263uL);
        if (!_leaveUnderlyingStreamOpen) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500264uL);
          _inputStream.Dispose();
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500265uL);
      _closed = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500266uL);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_62 = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500267uL);
        return RNTRNTRNT_62;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_63 = _inputStream.CanSeek;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500268uL);
        return RNTRNTRNT_63;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_64 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500269uL);
        return RNTRNTRNT_64;
      }
    }
    public override long Length {
      get {
        System.Int64 RNTRNTRNT_65 = _inputStream.Length;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500270uL);
        return RNTRNTRNT_65;
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_66 = _inputStream.Position;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500271uL);
        return RNTRNTRNT_66;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500272uL);
        Seek(value, SeekOrigin.Begin);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500273uL);
      }
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500274uL);
      throw new NotSupportedException("Flush");
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500275uL);
      throw new NotSupportedException("Write");
    }
    public override long Seek(long offset, SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500276uL);
      _findRequired = true;
      var x = _inputStream.Seek(offset, origin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_inputStream);
      System.Int64 RNTRNTRNT_67 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500277uL);
      return RNTRNTRNT_67;
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500278uL);
      throw new NotSupportedException();
    }
    private Stream _inputStream;
    private System.Text.Encoding _provisionalAlternateEncoding;
    private ZipEntry _currentEntry;
    private bool _firstEntry;
    private bool _needSetup;
    private ZipContainer _container;
    private Ionic.Crc.CrcCalculatorStream _crcStream;
    private Int64 _LeftToRead;
    internal String _Password;
    private Int64 _endOfEntry;
    private string _name;
    private bool _leaveUnderlyingStreamOpen;
    private bool _closed;
    private bool _findRequired;
    private bool _exceptionPending;
  }
}
