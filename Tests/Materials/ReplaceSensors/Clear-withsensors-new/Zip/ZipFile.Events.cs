using System;
using System.IO;
namespace Ionic.Zip
{
  public partial class ZipFile
  {
    private string ArchiveNameForEvent {
      get {
        System.String RNTRNTRNT_420 = (_name != null) ? _name : "(stream)";
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502907uL);
        return RNTRNTRNT_420;
      }
    }
    public event EventHandler<SaveProgressEventArgs> SaveProgress;
    internal bool OnSaveBlock(ZipEntry entry, Int64 bytesXferred, Int64 totalBytesToXfer)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502908uL);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502909uL);
      if (sp != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502910uL);
        var e = SaveProgressEventArgs.ByteUpdate(ArchiveNameForEvent, entry, bytesXferred, totalBytesToXfer);
        sp(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502911uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502912uL);
          _saveOperationCanceled = true;
        }
      }
      System.Boolean RNTRNTRNT_421 = _saveOperationCanceled;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502913uL);
      return RNTRNTRNT_421;
    }
    private void OnSaveEntry(int current, ZipEntry entry, bool before)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502914uL);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502915uL);
      if (sp != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502916uL);
        var e = new SaveProgressEventArgs(ArchiveNameForEvent, before, _entries.Count, current, entry);
        sp(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502917uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502918uL);
          _saveOperationCanceled = true;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502919uL);
    }
    private void OnSaveEvent(ZipProgressEventType eventFlavor)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502920uL);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502921uL);
      if (sp != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502922uL);
        var e = new SaveProgressEventArgs(ArchiveNameForEvent, eventFlavor);
        sp(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502923uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502924uL);
          _saveOperationCanceled = true;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502925uL);
    }
    private void OnSaveStarted()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502926uL);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502927uL);
      if (sp != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502928uL);
        var e = SaveProgressEventArgs.Started(ArchiveNameForEvent);
        sp(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502929uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502930uL);
          _saveOperationCanceled = true;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502931uL);
    }
    private void OnSaveCompleted()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502932uL);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502933uL);
      if (sp != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502934uL);
        var e = SaveProgressEventArgs.Completed(ArchiveNameForEvent);
        sp(this, e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502935uL);
    }
    public event EventHandler<ReadProgressEventArgs> ReadProgress;
    private void OnReadStarted()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502936uL);
      EventHandler<ReadProgressEventArgs> rp = ReadProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502937uL);
      if (rp != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502938uL);
        var e = ReadProgressEventArgs.Started(ArchiveNameForEvent);
        rp(this, e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502939uL);
    }
    private void OnReadCompleted()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502940uL);
      EventHandler<ReadProgressEventArgs> rp = ReadProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502941uL);
      if (rp != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502942uL);
        var e = ReadProgressEventArgs.Completed(ArchiveNameForEvent);
        rp(this, e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502943uL);
    }
    internal void OnReadBytes(ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502944uL);
      EventHandler<ReadProgressEventArgs> rp = ReadProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502945uL);
      if (rp != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502946uL);
        var e = ReadProgressEventArgs.ByteUpdate(ArchiveNameForEvent, entry, ReadStream.Position, LengthOfReadStream);
        rp(this, e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502947uL);
    }
    internal void OnReadEntry(bool before, ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502948uL);
      EventHandler<ReadProgressEventArgs> rp = ReadProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502949uL);
      if (rp != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502950uL);
        ReadProgressEventArgs e = (before) ? ReadProgressEventArgs.Before(ArchiveNameForEvent, _entries.Count) : ReadProgressEventArgs.After(ArchiveNameForEvent, entry, _entries.Count);
        rp(this, e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502951uL);
    }
    private Int64 _lengthOfReadStream = -99;
    private Int64 LengthOfReadStream {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502952uL);
        if (_lengthOfReadStream == -99) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502953uL);
          _lengthOfReadStream = (_ReadStreamIsOurs) ? SharedUtilities.GetFileLength(_name) : -1L;
        }
        Int64 RNTRNTRNT_422 = _lengthOfReadStream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502954uL);
        return RNTRNTRNT_422;
      }
    }
    public event EventHandler<ExtractProgressEventArgs> ExtractProgress;
    private void OnExtractEntry(int current, bool before, ZipEntry currentEntry, string path)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502955uL);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502956uL);
      if (ep != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502957uL);
        var e = new ExtractProgressEventArgs(ArchiveNameForEvent, before, _entries.Count, current, currentEntry, path);
        ep(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502958uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502959uL);
          _extractOperationCanceled = true;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502960uL);
    }
    internal bool OnExtractBlock(ZipEntry entry, Int64 bytesWritten, Int64 totalBytesToWrite)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502961uL);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502962uL);
      if (ep != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502963uL);
        var e = ExtractProgressEventArgs.ByteUpdate(ArchiveNameForEvent, entry, bytesWritten, totalBytesToWrite);
        ep(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502964uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502965uL);
          _extractOperationCanceled = true;
        }
      }
      System.Boolean RNTRNTRNT_423 = _extractOperationCanceled;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502966uL);
      return RNTRNTRNT_423;
    }
    internal bool OnSingleEntryExtract(ZipEntry entry, string path, bool before)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502967uL);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502968uL);
      if (ep != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502969uL);
        var e = (before) ? ExtractProgressEventArgs.BeforeExtractEntry(ArchiveNameForEvent, entry, path) : ExtractProgressEventArgs.AfterExtractEntry(ArchiveNameForEvent, entry, path);
        ep(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502970uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502971uL);
          _extractOperationCanceled = true;
        }
      }
      System.Boolean RNTRNTRNT_424 = _extractOperationCanceled;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502972uL);
      return RNTRNTRNT_424;
    }
    internal bool OnExtractExisting(ZipEntry entry, string path)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502973uL);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502974uL);
      if (ep != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502975uL);
        var e = ExtractProgressEventArgs.ExtractExisting(ArchiveNameForEvent, entry, path);
        ep(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502976uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502977uL);
          _extractOperationCanceled = true;
        }
      }
      System.Boolean RNTRNTRNT_425 = _extractOperationCanceled;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502978uL);
      return RNTRNTRNT_425;
    }
    private void OnExtractAllCompleted(string path)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502979uL);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502980uL);
      if (ep != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502981uL);
        var e = ExtractProgressEventArgs.ExtractAllCompleted(ArchiveNameForEvent, path);
        ep(this, e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502982uL);
    }
    private void OnExtractAllStarted(string path)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502983uL);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502984uL);
      if (ep != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502985uL);
        var e = ExtractProgressEventArgs.ExtractAllStarted(ArchiveNameForEvent, path);
        ep(this, e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502986uL);
    }
    public event EventHandler<AddProgressEventArgs> AddProgress;
    private void OnAddStarted()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502987uL);
      EventHandler<AddProgressEventArgs> ap = AddProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502988uL);
      if (ap != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502989uL);
        var e = AddProgressEventArgs.Started(ArchiveNameForEvent);
        ap(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502990uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502991uL);
          _addOperationCanceled = true;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502992uL);
    }
    private void OnAddCompleted()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502993uL);
      EventHandler<AddProgressEventArgs> ap = AddProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502994uL);
      if (ap != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502995uL);
        var e = AddProgressEventArgs.Completed(ArchiveNameForEvent);
        ap(this, e);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502996uL);
    }
    internal void AfterAddEntry(ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502997uL);
      EventHandler<AddProgressEventArgs> ap = AddProgress;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502998uL);
      if (ap != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502999uL);
        var e = AddProgressEventArgs.AfterEntry(ArchiveNameForEvent, entry, _entries.Count);
        ap(this, e);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503000uL);
        if (e.Cancel) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503001uL);
          _addOperationCanceled = true;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503002uL);
    }
    public event EventHandler<ZipErrorEventArgs> ZipError;
    internal bool OnZipErrorSaving(ZipEntry entry, Exception exc)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503003uL);
      if (ZipError != null) {
        lock (LOCK) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503004uL);
          var e = ZipErrorEventArgs.Saving(this.Name, entry, exc);
          ZipError(this, e);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503005uL);
          if (e.Cancel) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503006uL);
            _saveOperationCanceled = true;
          }
        }
      }
      System.Boolean RNTRNTRNT_426 = _saveOperationCanceled;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503007uL);
      return RNTRNTRNT_426;
    }
  }
}
