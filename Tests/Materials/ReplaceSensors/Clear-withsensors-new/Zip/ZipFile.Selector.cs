using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  partial class ZipFile
  {
    public void AddSelectedFiles(String selectionCriteria)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503355uL);
      this.AddSelectedFiles(selectionCriteria, ".", null, false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503356uL);
    }
    public void AddSelectedFiles(String selectionCriteria, bool recurseDirectories)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503357uL);
      this.AddSelectedFiles(selectionCriteria, ".", null, recurseDirectories);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503358uL);
    }
    public void AddSelectedFiles(String selectionCriteria, String directoryOnDisk)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503359uL);
      this.AddSelectedFiles(selectionCriteria, directoryOnDisk, null, false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503360uL);
    }
    public void AddSelectedFiles(String selectionCriteria, String directoryOnDisk, bool recurseDirectories)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503361uL);
      this.AddSelectedFiles(selectionCriteria, directoryOnDisk, null, recurseDirectories);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503362uL);
    }
    public void AddSelectedFiles(String selectionCriteria, String directoryOnDisk, String directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503363uL);
      this.AddSelectedFiles(selectionCriteria, directoryOnDisk, directoryPathInArchive, false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503364uL);
    }
    public void AddSelectedFiles(String selectionCriteria, String directoryOnDisk, String directoryPathInArchive, bool recurseDirectories)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503365uL);
      _AddOrUpdateSelectedFiles(selectionCriteria, directoryOnDisk, directoryPathInArchive, recurseDirectories, false);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503366uL);
    }
    public void UpdateSelectedFiles(String selectionCriteria, String directoryOnDisk, String directoryPathInArchive, bool recurseDirectories)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503367uL);
      _AddOrUpdateSelectedFiles(selectionCriteria, directoryOnDisk, directoryPathInArchive, recurseDirectories, true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503368uL);
    }
    private string EnsureendInSlash(string s)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503369uL);
      if (s.EndsWith("\\")) {
        System.String RNTRNTRNT_448 = s;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503370uL);
        return RNTRNTRNT_448;
      }
      System.String RNTRNTRNT_449 = s + "\\";
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503371uL);
      return RNTRNTRNT_449;
    }
    private void _AddOrUpdateSelectedFiles(String selectionCriteria, String directoryOnDisk, String directoryPathInArchive, bool recurseDirectories, bool wantUpdate)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503372uL);
      if (directoryOnDisk == null && (Directory.Exists(selectionCriteria))) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503373uL);
        directoryOnDisk = selectionCriteria;
        selectionCriteria = "*.*";
      } else if (String.IsNullOrEmpty(directoryOnDisk)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503374uL);
        directoryOnDisk = ".";
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503375uL);
      while (directoryOnDisk.EndsWith("\\")) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503376uL);
        directoryOnDisk = directoryOnDisk.Substring(0, directoryOnDisk.Length - 1);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503377uL);
      if (Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503378uL);
        StatusMessageTextWriter.WriteLine("adding selection '{0}' from dir '{1}'...", selectionCriteria, directoryOnDisk);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503379uL);
      Ionic.FileSelector ff = new Ionic.FileSelector(selectionCriteria, AddDirectoryWillTraverseReparsePoints);
      var itemsToAdd = ff.SelectFiles(directoryOnDisk, recurseDirectories);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503380uL);
      if (Verbose) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503381uL);
        StatusMessageTextWriter.WriteLine("found {0} files...", itemsToAdd.Count);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503382uL);
      OnAddStarted();
      AddOrUpdateAction action = (wantUpdate) ? AddOrUpdateAction.AddOrUpdate : AddOrUpdateAction.AddOnly;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503383uL);
      foreach (var item in itemsToAdd) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503384uL);
        string dirInArchive = (directoryPathInArchive == null) ? null : ReplaceLeadingDirectory(Path.GetDirectoryName(item), directoryOnDisk, directoryPathInArchive);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503385uL);
        if (File.Exists(item)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503386uL);
          if (wantUpdate) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503387uL);
            this.UpdateFile(item, dirInArchive);
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503388uL);
            this.AddFile(item, dirInArchive);
          }
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503389uL);
          AddOrUpdateDirectoryImpl(item, dirInArchive, action, false, 0);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503390uL);
      OnAddCompleted();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503391uL);
    }
    private static string ReplaceLeadingDirectory(string original, string pattern, string replacement)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503392uL);
      string upperString = original.ToUpper();
      string upperPattern = pattern.ToUpper();
      int p1 = upperString.IndexOf(upperPattern);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503393uL);
      if (p1 != 0) {
        System.String RNTRNTRNT_450 = original;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503394uL);
        return RNTRNTRNT_450;
      }
      System.String RNTRNTRNT_451 = replacement + original.Substring(upperPattern.Length);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503395uL);
      return RNTRNTRNT_451;
    }
    public ICollection<ZipEntry> SelectEntries(String selectionCriteria)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503396uL);
      Ionic.FileSelector ff = new Ionic.FileSelector(selectionCriteria, AddDirectoryWillTraverseReparsePoints);
      ICollection<ZipEntry> RNTRNTRNT_452 = ff.SelectEntries(this);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503397uL);
      return RNTRNTRNT_452;
    }
    public ICollection<ZipEntry> SelectEntries(String selectionCriteria, string directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503398uL);
      Ionic.FileSelector ff = new Ionic.FileSelector(selectionCriteria, AddDirectoryWillTraverseReparsePoints);
      ICollection<ZipEntry> RNTRNTRNT_453 = ff.SelectEntries(this, directoryPathInArchive);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503399uL);
      return RNTRNTRNT_453;
    }
    public int RemoveSelectedEntries(String selectionCriteria)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503400uL);
      var selection = this.SelectEntries(selectionCriteria);
      this.RemoveEntries(selection);
      System.Int32 RNTRNTRNT_454 = selection.Count;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503401uL);
      return RNTRNTRNT_454;
    }
    public int RemoveSelectedEntries(String selectionCriteria, string directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503402uL);
      var selection = this.SelectEntries(selectionCriteria, directoryPathInArchive);
      this.RemoveEntries(selection);
      System.Int32 RNTRNTRNT_455 = selection.Count;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503403uL);
      return RNTRNTRNT_455;
    }
    public void ExtractSelectedEntries(String selectionCriteria)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503404uL);
      foreach (ZipEntry e in SelectEntries(selectionCriteria)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503405uL);
        e.Password = _Password;
        e.Extract();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503406uL);
    }
    public void ExtractSelectedEntries(String selectionCriteria, ExtractExistingFileAction extractExistingFile)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503407uL);
      foreach (ZipEntry e in SelectEntries(selectionCriteria)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503408uL);
        e.Password = _Password;
        e.Extract(extractExistingFile);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503409uL);
    }
    public void ExtractSelectedEntries(String selectionCriteria, String directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503410uL);
      foreach (ZipEntry e in SelectEntries(selectionCriteria, directoryPathInArchive)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503411uL);
        e.Password = _Password;
        e.Extract();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503412uL);
    }
    public void ExtractSelectedEntries(String selectionCriteria, string directoryInArchive, string extractDirectory)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503413uL);
      foreach (ZipEntry e in SelectEntries(selectionCriteria, directoryInArchive)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503414uL);
        e.Password = _Password;
        e.Extract(extractDirectory);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503415uL);
    }
    public void ExtractSelectedEntries(String selectionCriteria, string directoryPathInArchive, string extractDirectory, ExtractExistingFileAction extractExistingFile)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503416uL);
      foreach (ZipEntry e in SelectEntries(selectionCriteria, directoryPathInArchive)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503417uL);
        e.Password = _Password;
        e.Extract(extractDirectory, extractExistingFile);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503418uL);
    }
  }
}
namespace Ionic
{
  internal abstract partial class SelectionCriterion
  {
    internal abstract bool Evaluate(Ionic.Zip.ZipEntry entry);
  }
  internal partial class NameCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503419uL);
      string transformedFileName = entry.FileName.Replace("/", "\\");
      System.Boolean RNTRNTRNT_456 = _Evaluate(transformedFileName);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503420uL);
      return RNTRNTRNT_456;
    }
  }
  internal partial class SizeCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      System.Boolean RNTRNTRNT_457 = _Evaluate(entry.UncompressedSize);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503421uL);
      return RNTRNTRNT_457;
    }
  }
  internal partial class TimeCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503422uL);
      DateTime x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503423uL);
      switch (Which) {
        case WhichTime.atime:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503426uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503424uL);
            x = entry.AccessedTime;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503425uL);
            break;
          }

        case WhichTime.mtime:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503429uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503427uL);
            x = entry.ModifiedTime;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503428uL);
            break;
          }

        case WhichTime.ctime:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503432uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503430uL);
            x = entry.CreationTime;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503431uL);
            break;
          }

        default:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503434uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503433uL);
            throw new ArgumentException("??time");
          }

      }
      System.Boolean RNTRNTRNT_458 = _Evaluate(x);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503435uL);
      return RNTRNTRNT_458;
    }
  }
  internal partial class TypeCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503436uL);
      bool result = (ObjectType == 'D') ? entry.IsDirectory : !entry.IsDirectory;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503437uL);
      if (Operator != ComparisonOperator.EqualTo) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503438uL);
        result = !result;
      }
      System.Boolean RNTRNTRNT_459 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503439uL);
      return RNTRNTRNT_459;
    }
  }
  internal partial class AttributesCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503440uL);
      FileAttributes fileAttrs = entry.Attributes;
      System.Boolean RNTRNTRNT_460 = _Evaluate(fileAttrs);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503441uL);
      return RNTRNTRNT_460;
    }
  }
  internal partial class CompoundCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503442uL);
      bool result = Left.Evaluate(entry);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503443uL);
      switch (Conjunction) {
        case LogicalConjunction.AND:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503447uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503444uL);
            if (result) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503445uL);
              result = Right.Evaluate(entry);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503446uL);
            break;
          }

        case LogicalConjunction.OR:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503451uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503448uL);
            if (!result) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503449uL);
              result = Right.Evaluate(entry);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503450uL);
            break;
          }

        case LogicalConjunction.XOR:
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503454uL);
          
          {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503452uL);
            result ^= Right.Evaluate(entry);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503453uL);
            break;
          }

      }
      System.Boolean RNTRNTRNT_461 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503455uL);
      return RNTRNTRNT_461;
    }
  }
  public partial class FileSelector
  {
    private bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503456uL);
      bool result = _Criterion.Evaluate(entry);
      System.Boolean RNTRNTRNT_462 = result;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503457uL);
      return RNTRNTRNT_462;
    }
    public ICollection<Ionic.Zip.ZipEntry> SelectEntries(Ionic.Zip.ZipFile zip)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503458uL);
      if (zip == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503459uL);
        throw new ArgumentNullException("zip");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503460uL);
      var list = new List<Ionic.Zip.ZipEntry>();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503461uL);
      foreach (Ionic.Zip.ZipEntry e in zip) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503462uL);
        if (this.Evaluate(e)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503463uL);
          list.Add(e);
        }
      }
      ICollection<Ionic.Zip.ZipEntry> RNTRNTRNT_463 = list;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503464uL);
      return RNTRNTRNT_463;
    }
    public ICollection<Ionic.Zip.ZipEntry> SelectEntries(Ionic.Zip.ZipFile zip, string directoryPathInArchive)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503465uL);
      if (zip == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503466uL);
        throw new ArgumentNullException("zip");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503467uL);
      var list = new List<Ionic.Zip.ZipEntry>();
      string slashSwapped = (directoryPathInArchive == null) ? null : directoryPathInArchive.Replace("/", "\\");
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503468uL);
      if (slashSwapped != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503469uL);
        while (slashSwapped.EndsWith("\\")) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503470uL);
          slashSwapped = slashSwapped.Substring(0, slashSwapped.Length - 1);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503471uL);
      foreach (Ionic.Zip.ZipEntry e in zip) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503472uL);
        if (directoryPathInArchive == null || (Path.GetDirectoryName(e.FileName) == directoryPathInArchive) || (Path.GetDirectoryName(e.FileName) == slashSwapped)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503473uL);
          if (this.Evaluate(e)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503474uL);
            list.Add(e);
          }
        }
      }
      ICollection<Ionic.Zip.ZipEntry> RNTRNTRNT_464 = list;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503475uL);
      return RNTRNTRNT_464;
    }
  }
}
