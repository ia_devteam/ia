using System;
using System.IO;
namespace Ionic.Zip
{
  internal class OffsetStream : System.IO.Stream, System.IDisposable
  {
    private Int64 _originalPosition;
    private Stream _innerStream;
    public OffsetStream(Stream s) : base()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500401uL);
      _originalPosition = s.Position;
      _innerStream = s;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500402uL);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      System.Int32 RNTRNTRNT_93 = _innerStream.Read(buffer, offset, count);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500403uL);
      return RNTRNTRNT_93;
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500404uL);
      throw new NotImplementedException();
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_94 = _innerStream.CanRead;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500405uL);
        return RNTRNTRNT_94;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_95 = _innerStream.CanSeek;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500406uL);
        return RNTRNTRNT_95;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_96 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500407uL);
        return RNTRNTRNT_96;
      }
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500408uL);
      _innerStream.Flush();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500409uL);
    }
    public override long Length {
      get {
        System.Int64 RNTRNTRNT_97 = _innerStream.Length;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500410uL);
        return RNTRNTRNT_97;
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_98 = _innerStream.Position - _originalPosition;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500411uL);
        return RNTRNTRNT_98;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500412uL);
        _innerStream.Position = _originalPosition + value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500413uL);
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      System.Int64 RNTRNTRNT_99 = _innerStream.Seek(_originalPosition + offset, origin) - _originalPosition;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500414uL);
      return RNTRNTRNT_99;
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500415uL);
      throw new NotImplementedException();
    }
    void IDisposable.Dispose()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500416uL);
      Close();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500417uL);
    }
    public override void Close()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500418uL);
      base.Close();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500419uL);
    }
  }
}
