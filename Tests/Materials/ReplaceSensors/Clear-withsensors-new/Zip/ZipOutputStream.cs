using System;
using System.Threading;
using System.Collections.Generic;
using System.IO;
using Ionic.Zip;
namespace Ionic.Zip
{
  public class ZipOutputStream : Stream
  {
    public ZipOutputStream(Stream stream) : this(stream, false)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500001uL);
    }
    public ZipOutputStream(String fileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500002uL);
      Stream stream = File.Open(fileName, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
      _Init(stream, false, fileName);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500003uL);
    }
    public ZipOutputStream(Stream stream, bool leaveOpen)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500004uL);
      _Init(stream, leaveOpen, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500005uL);
    }
    private void _Init(Stream stream, bool leaveOpen, string name)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500006uL);
      _outputStream = stream.CanRead ? stream : new CountingStream(stream);
      CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
      CompressionMethod = Ionic.Zip.CompressionMethod.Deflate;
      _encryption = EncryptionAlgorithm.None;
      _entriesWritten = new Dictionary<String, ZipEntry>(StringComparer.Ordinal);
      _zip64 = Zip64Option.Never;
      _leaveUnderlyingStreamOpen = leaveOpen;
      Strategy = Ionic.Zlib.CompressionStrategy.Default;
      _name = name ?? "(stream)";
      ParallelDeflateThreshold = -1L;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500007uL);
    }
    public override String ToString()
    {
      String RNTRNTRNT_1 = String.Format("ZipOutputStream::{0}(leaveOpen({1})))", _name, _leaveUnderlyingStreamOpen);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500008uL);
      return RNTRNTRNT_1;
    }
    public String Password {
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500009uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500010uL);
          _exceptionPending = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500011uL);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500012uL);
        _password = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500013uL);
        if (_password == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500014uL);
          _encryption = EncryptionAlgorithm.None;
        } else if (_encryption == EncryptionAlgorithm.None) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500015uL);
          _encryption = EncryptionAlgorithm.PkzipWeak;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500016uL);
      }
    }
    public EncryptionAlgorithm Encryption {
      get {
        EncryptionAlgorithm RNTRNTRNT_2 = _encryption;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500017uL);
        return RNTRNTRNT_2;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500018uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500019uL);
          _exceptionPending = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500020uL);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500021uL);
        if (value == EncryptionAlgorithm.Unsupported) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500022uL);
          _exceptionPending = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500023uL);
          throw new InvalidOperationException("You may not set Encryption to that value.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500024uL);
        _encryption = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500025uL);
      }
    }
    public int CodecBufferSize { get; set; }
    public Ionic.Zlib.CompressionStrategy Strategy { get; set; }
    public ZipEntryTimestamp Timestamp {
      get {
        ZipEntryTimestamp RNTRNTRNT_3 = _timestamp;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500026uL);
        return RNTRNTRNT_3;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500027uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500028uL);
          _exceptionPending = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500029uL);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500030uL);
        _timestamp = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500031uL);
      }
    }
    public Ionic.Zlib.CompressionLevel CompressionLevel { get; set; }
    public Ionic.Zip.CompressionMethod CompressionMethod { get; set; }
    public string Comment {
      get {
        System.String RNTRNTRNT_4 = _comment;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500032uL);
        return RNTRNTRNT_4;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500033uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500034uL);
          _exceptionPending = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500035uL);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500036uL);
        _comment = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500037uL);
      }
    }
    public Zip64Option EnableZip64 {
      get {
        Zip64Option RNTRNTRNT_5 = _zip64;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500038uL);
        return RNTRNTRNT_5;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500039uL);
        if (_disposed) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500040uL);
          _exceptionPending = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500041uL);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500042uL);
        _zip64 = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500043uL);
      }
    }
    public bool OutputUsedZip64 {
      get {
        System.Boolean RNTRNTRNT_6 = _anyEntriesUsedZip64 || _directoryNeededZip64;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500044uL);
        return RNTRNTRNT_6;
      }
    }
    public bool IgnoreCase {
      get {
        System.Boolean RNTRNTRNT_7 = !_DontIgnoreCase;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500045uL);
        return RNTRNTRNT_7;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500046uL);
        _DontIgnoreCase = !value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500047uL);
      }
    }
    [Obsolete("Beginning with v1.9.1.6 of DotNetZip, this property is obsolete. It will be removed in a future version of the library. Use AlternateEncoding and AlternateEncodingUsage instead.")]
    public bool UseUnicodeAsNecessary {
      get {
        System.Boolean RNTRNTRNT_8 = (_alternateEncoding == System.Text.Encoding.UTF8) && (AlternateEncodingUsage == ZipOption.AsNecessary);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500048uL);
        return RNTRNTRNT_8;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500049uL);
        if (value) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500050uL);
          _alternateEncoding = System.Text.Encoding.UTF8;
          _alternateEncodingUsage = ZipOption.AsNecessary;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500051uL);
          _alternateEncoding = Ionic.Zip.ZipOutputStream.DefaultEncoding;
          _alternateEncodingUsage = ZipOption.Never;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500052uL);
      }
    }
    [Obsolete("use AlternateEncoding and AlternateEncodingUsage instead.")]
    public System.Text.Encoding ProvisionalAlternateEncoding {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500053uL);
        if (_alternateEncodingUsage == ZipOption.AsNecessary) {
          System.Text.Encoding RNTRNTRNT_9 = _alternateEncoding;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500054uL);
          return RNTRNTRNT_9;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500055uL);
        return null;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500056uL);
        _alternateEncoding = value;
        _alternateEncodingUsage = ZipOption.AsNecessary;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500057uL);
      }
    }
    public System.Text.Encoding AlternateEncoding {
      get {
        System.Text.Encoding RNTRNTRNT_10 = _alternateEncoding;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500058uL);
        return RNTRNTRNT_10;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500059uL);
        _alternateEncoding = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500060uL);
      }
    }
    public ZipOption AlternateEncodingUsage {
      get {
        ZipOption RNTRNTRNT_11 = _alternateEncodingUsage;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500061uL);
        return RNTRNTRNT_11;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500062uL);
        _alternateEncodingUsage = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500063uL);
      }
    }
    public static System.Text.Encoding DefaultEncoding {
      get {
        System.Text.Encoding RNTRNTRNT_12 = System.Text.Encoding.GetEncoding("IBM437");
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500064uL);
        return RNTRNTRNT_12;
      }
    }
    public long ParallelDeflateThreshold {
      get {
        System.Int64 RNTRNTRNT_13 = _ParallelDeflateThreshold;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500065uL);
        return RNTRNTRNT_13;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500066uL);
        if ((value != 0) && (value != -1) && (value < 64 * 1024)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500067uL);
          throw new ArgumentOutOfRangeException("value must be greater than 64k, or 0, or -1");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500068uL);
        _ParallelDeflateThreshold = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500069uL);
      }
    }
    public int ParallelDeflateMaxBufferPairs {
      get {
        System.Int32 RNTRNTRNT_14 = _maxBufferPairs;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500070uL);
        return RNTRNTRNT_14;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500071uL);
        if (value < 4) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500072uL);
          throw new ArgumentOutOfRangeException("ParallelDeflateMaxBufferPairs", "Value must be 4 or greater.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500073uL);
        _maxBufferPairs = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500074uL);
      }
    }
    private void InsureUniqueEntry(ZipEntry ze1)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500075uL);
      if (_entriesWritten.ContainsKey(ze1.FileName)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500076uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500077uL);
        throw new ArgumentException(String.Format("The entry '{0}' already exists in the zip archive.", ze1.FileName));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500078uL);
    }
    internal Stream OutputStream {
      get {
        Stream RNTRNTRNT_15 = _outputStream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500079uL);
        return RNTRNTRNT_15;
      }
    }
    internal String Name {
      get {
        String RNTRNTRNT_16 = _name;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500080uL);
        return RNTRNTRNT_16;
      }
    }
    public bool ContainsEntry(string name)
    {
      System.Boolean RNTRNTRNT_17 = _entriesWritten.ContainsKey(SharedUtilities.NormalizePathForUseInZipFile(name));
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500081uL);
      return RNTRNTRNT_17;
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500082uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500083uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500084uL);
        throw new System.InvalidOperationException("The stream has been closed.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500085uL);
      if (buffer == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500086uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500087uL);
        throw new System.ArgumentNullException("buffer");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500088uL);
      if (_currentEntry == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500089uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500090uL);
        throw new System.InvalidOperationException("You must call PutNextEntry() before calling Write().");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500091uL);
      if (_currentEntry.IsDirectory) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500092uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500093uL);
        throw new System.InvalidOperationException("You cannot Write() data for an entry that is a directory.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500094uL);
      if (_needToWriteEntryHeader) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500095uL);
        _InitiateCurrentEntry(false);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500096uL);
      if (count != 0) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500097uL);
        _entryOutputStream.Write(buffer, offset, count);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500098uL);
    }
    public ZipEntry PutNextEntry(String entryName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500099uL);
      if (String.IsNullOrEmpty(entryName)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500100uL);
        throw new ArgumentNullException("entryName");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500101uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500102uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500103uL);
        throw new System.InvalidOperationException("The stream has been closed.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500104uL);
      _FinishCurrentEntry();
      _currentEntry = ZipEntry.CreateForZipOutputStream(entryName);
      _currentEntry._container = new ZipContainer(this);
      _currentEntry._BitField |= 0x8;
      _currentEntry.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
      _currentEntry.CompressionLevel = this.CompressionLevel;
      _currentEntry.CompressionMethod = this.CompressionMethod;
      _currentEntry.Password = _password;
      _currentEntry.Encryption = this.Encryption;
      _currentEntry.AlternateEncoding = this.AlternateEncoding;
      _currentEntry.AlternateEncodingUsage = this.AlternateEncodingUsage;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500105uL);
      if (entryName.EndsWith("/")) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500106uL);
        _currentEntry.MarkAsDirectory();
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500107uL);
      _currentEntry.EmitTimesInWindowsFormatWhenSaving = ((_timestamp & ZipEntryTimestamp.Windows) != 0);
      _currentEntry.EmitTimesInUnixFormatWhenSaving = ((_timestamp & ZipEntryTimestamp.Unix) != 0);
      InsureUniqueEntry(_currentEntry);
      _needToWriteEntryHeader = true;
      ZipEntry RNTRNTRNT_18 = _currentEntry;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500108uL);
      return RNTRNTRNT_18;
    }
    private void _InitiateCurrentEntry(bool finishing)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500109uL);
      _entriesWritten.Add(_currentEntry.FileName, _currentEntry);
      _entryCount++;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500110uL);
      if (_entryCount > 65534 && _zip64 == Zip64Option.Never) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500111uL);
        _exceptionPending = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500112uL);
        throw new System.InvalidOperationException("Too many entries. Consider setting ZipOutputStream.EnableZip64.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500113uL);
      _currentEntry.WriteHeader(_outputStream, finishing ? 99 : 0);
      _currentEntry.StoreRelativeOffset();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500114uL);
      if (!_currentEntry.IsDirectory) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500115uL);
        _currentEntry.WriteSecurityMetadata(_outputStream);
        _currentEntry.PrepOutputStream(_outputStream, finishing ? 0 : -1, out _outputCounter, out _encryptor, out _deflater, out _entryOutputStream);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500116uL);
      _needToWriteEntryHeader = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500117uL);
    }
    private void _FinishCurrentEntry()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500118uL);
      if (_currentEntry != null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500119uL);
        if (_needToWriteEntryHeader) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500120uL);
          _InitiateCurrentEntry(true);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500121uL);
        _currentEntry.FinishOutputStream(_outputStream, _outputCounter, _encryptor, _deflater, _entryOutputStream);
        _currentEntry.PostProcessOutput(_outputStream);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500122uL);
        if (_currentEntry.OutputUsedZip64 != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500123uL);
          _anyEntriesUsedZip64 |= _currentEntry.OutputUsedZip64.Value;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500124uL);
        _outputCounter = null;
        _encryptor = _deflater = null;
        _entryOutputStream = null;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500125uL);
    }
    protected override void Dispose(bool disposing)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500126uL);
      if (_disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500127uL);
        return;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500128uL);
      if (disposing) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500129uL);
        if (!_exceptionPending) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500130uL);
          _FinishCurrentEntry();
          _directoryNeededZip64 = ZipOutput.WriteCentralDirectoryStructure(_outputStream, _entriesWritten.Values, 1, _zip64, Comment, new ZipContainer(this));
          Stream wrappedStream = null;
          CountingStream cs = _outputStream as CountingStream;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500131uL);
          if (cs != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500132uL);
            wrappedStream = cs.WrappedStream;
            cs.Dispose();
          } else {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500133uL);
            wrappedStream = _outputStream;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500134uL);
          if (!_leaveUnderlyingStreamOpen) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500135uL);
            wrappedStream.Dispose();
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500136uL);
          _outputStream = null;
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500137uL);
      _disposed = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500138uL);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_19 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500139uL);
        return RNTRNTRNT_19;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_20 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500140uL);
        return RNTRNTRNT_20;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_21 = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500141uL);
        return RNTRNTRNT_21;
      }
    }
    public override long Length {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500142uL);
        throw new NotSupportedException();
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_22 = _outputStream.Position;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500143uL);
        return RNTRNTRNT_22;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500144uL);
        throw new NotSupportedException();
      }
    }
    public override void Flush()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500145uL);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500146uL);
      throw new NotSupportedException("Read");
    }
    public override long Seek(long offset, SeekOrigin origin)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500147uL);
      throw new NotSupportedException("Seek");
    }
    public override void SetLength(long value)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500148uL);
      throw new NotSupportedException();
    }
    private EncryptionAlgorithm _encryption;
    private ZipEntryTimestamp _timestamp;
    internal String _password;
    private String _comment;
    private Stream _outputStream;
    private ZipEntry _currentEntry;
    internal Zip64Option _zip64;
    private Dictionary<String, ZipEntry> _entriesWritten;
    private int _entryCount;
    private ZipOption _alternateEncodingUsage = ZipOption.Never;
    private System.Text.Encoding _alternateEncoding = System.Text.Encoding.GetEncoding("IBM437");
    private bool _leaveUnderlyingStreamOpen;
    private bool _disposed;
    private bool _exceptionPending;
    private bool _anyEntriesUsedZip64, _directoryNeededZip64;
    private CountingStream _outputCounter;
    private Stream _encryptor;
    private Stream _deflater;
    private Ionic.Crc.CrcCalculatorStream _entryOutputStream;
    private bool _needToWriteEntryHeader;
    private string _name;
    private bool _DontIgnoreCase;
    internal Ionic.Zlib.ParallelDeflateOutputStream ParallelDeflater;
    private long _ParallelDeflateThreshold;
    private int _maxBufferPairs = 16;
  }
  internal class ZipContainer
  {
    private ZipFile _zf;
    private ZipOutputStream _zos;
    private ZipInputStream _zis;
    public ZipContainer(Object o)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500149uL);
      _zf = (o as ZipFile);
      _zos = (o as ZipOutputStream);
      _zis = (o as ZipInputStream);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500150uL);
    }
    public ZipFile ZipFile {
      get {
        ZipFile RNTRNTRNT_23 = _zf;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500151uL);
        return RNTRNTRNT_23;
      }
    }
    public ZipOutputStream ZipOutputStream {
      get {
        ZipOutputStream RNTRNTRNT_24 = _zos;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500152uL);
        return RNTRNTRNT_24;
      }
    }
    public string Name {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500153uL);
        if (_zf != null) {
          System.String RNTRNTRNT_25 = _zf.Name;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500154uL);
          return RNTRNTRNT_25;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500155uL);
        if (_zis != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500156uL);
          throw new NotSupportedException();
        }
        System.String RNTRNTRNT_26 = _zos.Name;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500157uL);
        return RNTRNTRNT_26;
      }
    }
    public string Password {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500158uL);
        if (_zf != null) {
          System.String RNTRNTRNT_27 = _zf._Password;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500159uL);
          return RNTRNTRNT_27;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500160uL);
        if (_zis != null) {
          System.String RNTRNTRNT_28 = _zis._Password;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500161uL);
          return RNTRNTRNT_28;
        }
        System.String RNTRNTRNT_29 = _zos._password;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500162uL);
        return RNTRNTRNT_29;
      }
    }
    public Zip64Option Zip64 {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500163uL);
        if (_zf != null) {
          Zip64Option RNTRNTRNT_30 = _zf._zip64;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500164uL);
          return RNTRNTRNT_30;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500165uL);
        if (_zis != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500166uL);
          throw new NotSupportedException();
        }
        Zip64Option RNTRNTRNT_31 = _zos._zip64;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500167uL);
        return RNTRNTRNT_31;
      }
    }
    public int BufferSize {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500168uL);
        if (_zf != null) {
          System.Int32 RNTRNTRNT_32 = _zf.BufferSize;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500169uL);
          return RNTRNTRNT_32;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500170uL);
        if (_zis != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500171uL);
          throw new NotSupportedException();
        }
        System.Int32 RNTRNTRNT_33 = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500172uL);
        return RNTRNTRNT_33;
      }
    }
    public Ionic.Zlib.ParallelDeflateOutputStream ParallelDeflater {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500173uL);
        if (_zf != null) {
          Ionic.Zlib.ParallelDeflateOutputStream RNTRNTRNT_34 = _zf.ParallelDeflater;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500174uL);
          return RNTRNTRNT_34;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500175uL);
        if (_zis != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500176uL);
          return null;
        }
        Ionic.Zlib.ParallelDeflateOutputStream RNTRNTRNT_35 = _zos.ParallelDeflater;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500177uL);
        return RNTRNTRNT_35;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500178uL);
        if (_zf != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500179uL);
          _zf.ParallelDeflater = value;
        } else if (_zos != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500180uL);
          _zos.ParallelDeflater = value;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500181uL);
      }
    }
    public long ParallelDeflateThreshold {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500182uL);
        if (_zf != null) {
          System.Int64 RNTRNTRNT_36 = _zf.ParallelDeflateThreshold;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500183uL);
          return RNTRNTRNT_36;
        }
        System.Int64 RNTRNTRNT_37 = _zos.ParallelDeflateThreshold;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500184uL);
        return RNTRNTRNT_37;
      }
    }
    public int ParallelDeflateMaxBufferPairs {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500185uL);
        if (_zf != null) {
          System.Int32 RNTRNTRNT_38 = _zf.ParallelDeflateMaxBufferPairs;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500186uL);
          return RNTRNTRNT_38;
        }
        System.Int32 RNTRNTRNT_39 = _zos.ParallelDeflateMaxBufferPairs;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500187uL);
        return RNTRNTRNT_39;
      }
    }
    public int CodecBufferSize {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500188uL);
        if (_zf != null) {
          System.Int32 RNTRNTRNT_40 = _zf.CodecBufferSize;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500189uL);
          return RNTRNTRNT_40;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500190uL);
        if (_zis != null) {
          System.Int32 RNTRNTRNT_41 = _zis.CodecBufferSize;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500191uL);
          return RNTRNTRNT_41;
        }
        System.Int32 RNTRNTRNT_42 = _zos.CodecBufferSize;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500192uL);
        return RNTRNTRNT_42;
      }
    }
    public Ionic.Zlib.CompressionStrategy Strategy {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500193uL);
        if (_zf != null) {
          Ionic.Zlib.CompressionStrategy RNTRNTRNT_43 = _zf.Strategy;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500194uL);
          return RNTRNTRNT_43;
        }
        Ionic.Zlib.CompressionStrategy RNTRNTRNT_44 = _zos.Strategy;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500195uL);
        return RNTRNTRNT_44;
      }
    }
    public Zip64Option UseZip64WhenSaving {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500196uL);
        if (_zf != null) {
          Zip64Option RNTRNTRNT_45 = _zf.UseZip64WhenSaving;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500197uL);
          return RNTRNTRNT_45;
        }
        Zip64Option RNTRNTRNT_46 = _zos.EnableZip64;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500198uL);
        return RNTRNTRNT_46;
      }
    }
    public System.Text.Encoding AlternateEncoding {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500199uL);
        if (_zf != null) {
          System.Text.Encoding RNTRNTRNT_47 = _zf.AlternateEncoding;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500200uL);
          return RNTRNTRNT_47;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500201uL);
        if (_zos != null) {
          System.Text.Encoding RNTRNTRNT_48 = _zos.AlternateEncoding;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500202uL);
          return RNTRNTRNT_48;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500203uL);
        return null;
      }
    }
    public System.Text.Encoding DefaultEncoding {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500204uL);
        if (_zf != null) {
          System.Text.Encoding RNTRNTRNT_49 = ZipFile.DefaultEncoding;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500205uL);
          return RNTRNTRNT_49;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500206uL);
        if (_zos != null) {
          System.Text.Encoding RNTRNTRNT_50 = ZipOutputStream.DefaultEncoding;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500207uL);
          return RNTRNTRNT_50;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500208uL);
        return null;
      }
    }
    public ZipOption AlternateEncodingUsage {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500209uL);
        if (_zf != null) {
          ZipOption RNTRNTRNT_51 = _zf.AlternateEncodingUsage;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500210uL);
          return RNTRNTRNT_51;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500211uL);
        if (_zos != null) {
          ZipOption RNTRNTRNT_52 = _zos.AlternateEncodingUsage;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500212uL);
          return RNTRNTRNT_52;
        }
        ZipOption RNTRNTRNT_53 = ZipOption.Never;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500213uL);
        return RNTRNTRNT_53;
      }
    }
    public Stream ReadStream {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500214uL);
        if (_zf != null) {
          Stream RNTRNTRNT_54 = _zf.ReadStream;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500215uL);
          return RNTRNTRNT_54;
        }
        Stream RNTRNTRNT_55 = _zis.ReadStream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500216uL);
        return RNTRNTRNT_55;
      }
    }
  }
}
