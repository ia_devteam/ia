using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public partial class ZipFile
  {
    public static bool CheckZip(string zipFileName)
    {
      System.Boolean RNTRNTRNT_367 = CheckZip(zipFileName, false, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502629uL);
      return RNTRNTRNT_367;
    }
    public static bool CheckZip(string zipFileName, bool fixIfNecessary, TextWriter writer)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502630uL);
      ZipFile zip1 = null, zip2 = null;
      bool isOk = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502631uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502632uL);
        zip1 = new ZipFile();
        zip1.FullScan = true;
        zip1.Initialize(zipFileName);
        zip2 = ZipFile.Read(zipFileName);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502633uL);
        foreach (var e1 in zip1) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502634uL);
          foreach (var e2 in zip2) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502635uL);
            if (e1.FileName == e2.FileName) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502636uL);
              if (e1._RelativeOffsetOfLocalHeader != e2._RelativeOffsetOfLocalHeader) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502637uL);
                isOk = false;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502638uL);
                if (writer != null) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502639uL);
                  writer.WriteLine("{0}: mismatch in RelativeOffsetOfLocalHeader  (0x{1:X16} != 0x{2:X16})", e1.FileName, e1._RelativeOffsetOfLocalHeader, e2._RelativeOffsetOfLocalHeader);
                }
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502640uL);
              if (e1._CompressedSize != e2._CompressedSize) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502641uL);
                isOk = false;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502642uL);
                if (writer != null) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502643uL);
                  writer.WriteLine("{0}: mismatch in CompressedSize  (0x{1:X16} != 0x{2:X16})", e1.FileName, e1._CompressedSize, e2._CompressedSize);
                }
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502644uL);
              if (e1._UncompressedSize != e2._UncompressedSize) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502645uL);
                isOk = false;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502646uL);
                if (writer != null) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502647uL);
                  writer.WriteLine("{0}: mismatch in UncompressedSize  (0x{1:X16} != 0x{2:X16})", e1.FileName, e1._UncompressedSize, e2._UncompressedSize);
                }
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502648uL);
              if (e1.CompressionMethod != e2.CompressionMethod) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502649uL);
                isOk = false;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502650uL);
                if (writer != null) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502651uL);
                  writer.WriteLine("{0}: mismatch in CompressionMethod  (0x{1:X4} != 0x{2:X4})", e1.FileName, e1.CompressionMethod, e2.CompressionMethod);
                }
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502652uL);
              if (e1.Crc != e2.Crc) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502653uL);
                isOk = false;
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502654uL);
                if (writer != null) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502655uL);
                  writer.WriteLine("{0}: mismatch in Crc32  (0x{1:X4} != 0x{2:X4})", e1.FileName, e1.Crc, e2.Crc);
                }
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502656uL);
              break;
            }
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502657uL);
        zip2.Dispose();
        zip2 = null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502658uL);
        if (!isOk && fixIfNecessary) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502659uL);
          string newFileName = Path.GetFileNameWithoutExtension(zipFileName);
          newFileName = System.String.Format("{0}_fixed.zip", newFileName);
          zip1.Save(newFileName);
        }
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502660uL);
        if (zip1 != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502661uL);
          zip1.Dispose();
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502662uL);
        if (zip2 != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502663uL);
          zip2.Dispose();
        }
      }
      System.Boolean RNTRNTRNT_368 = isOk;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502664uL);
      return RNTRNTRNT_368;
    }
    public static void FixZipDirectory(string zipFileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502665uL);
      using (var zip = new ZipFile()) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502666uL);
        zip.FullScan = true;
        zip.Initialize(zipFileName);
        zip.Save(zipFileName);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502667uL);
    }
    public static bool CheckZipPassword(string zipFileName, string password)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502668uL);
      bool success = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502669uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502670uL);
        using (ZipFile zip1 = ZipFile.Read(zipFileName)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502671uL);
          foreach (var e in zip1) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502672uL);
            if (!e.IsDirectory && e.UsesEncryption) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502673uL);
              e.ExtractWithPassword(System.IO.Stream.Null, password);
            }
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502674uL);
        success = true;
      } catch (Ionic.Zip.BadPasswordException) {
      }
      System.Boolean RNTRNTRNT_369 = success;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502675uL);
      return RNTRNTRNT_369;
    }
    public string Info {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502676uL);
        var builder = new System.Text.StringBuilder();
        builder.Append(string.Format("          ZipFile: {0}\n", this.Name));
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502677uL);
        if (!string.IsNullOrEmpty(this._Comment)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502678uL);
          builder.Append(string.Format("          Comment: {0}\n", this._Comment));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502679uL);
        if (this._versionMadeBy != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502680uL);
          builder.Append(string.Format("  version made by: 0x{0:X4}\n", this._versionMadeBy));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502681uL);
        if (this._versionNeededToExtract != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502682uL);
          builder.Append(string.Format("needed to extract: 0x{0:X4}\n", this._versionNeededToExtract));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502683uL);
        builder.Append(string.Format("       uses ZIP64: {0}\n", this.InputUsesZip64));
        builder.Append(string.Format("     disk with CD: {0}\n", this._diskNumberWithCd));
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502684uL);
        if (this._OffsetOfCentralDirectory == 0xffffffffu) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502685uL);
          builder.Append(string.Format("      CD64 offset: 0x{0:X16}\n", this._OffsetOfCentralDirectory64));
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502686uL);
          builder.Append(string.Format("        CD offset: 0x{0:X8}\n", this._OffsetOfCentralDirectory));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502687uL);
        builder.Append("\n");
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502688uL);
        foreach (ZipEntry entry in this._entries.Values) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502689uL);
          builder.Append(entry.Info);
        }
        System.String RNTRNTRNT_370 = builder.ToString();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502690uL);
        return RNTRNTRNT_370;
      }
    }
  }
}
