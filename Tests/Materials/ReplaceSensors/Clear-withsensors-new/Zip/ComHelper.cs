using Interop = System.Runtime.InteropServices;
namespace Ionic.Zip
{
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d0000F")]
  [System.Runtime.InteropServices.ComVisible(true)]
  [System.Runtime.InteropServices.ClassInterface(System.Runtime.InteropServices.ClassInterfaceType.AutoDispatch)]
  public class ComHelper
  {
    public bool IsZipFile(string filename)
    {
      System.Boolean RNTRNTRNT_88 = ZipFile.IsZipFile(filename);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500394uL);
      return RNTRNTRNT_88;
    }
    public bool IsZipFileWithExtract(string filename)
    {
      System.Boolean RNTRNTRNT_89 = ZipFile.IsZipFile(filename, true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500395uL);
      return RNTRNTRNT_89;
    }
    public bool CheckZip(string filename)
    {
      System.Boolean RNTRNTRNT_90 = ZipFile.CheckZip(filename);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500396uL);
      return RNTRNTRNT_90;
    }
    public bool CheckZipPassword(string filename, string password)
    {
      System.Boolean RNTRNTRNT_91 = ZipFile.CheckZipPassword(filename, password);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500397uL);
      return RNTRNTRNT_91;
    }
    public void FixZipDirectory(string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500398uL);
      ZipFile.FixZipDirectory(filename);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500399uL);
    }
    public string GetZipLibraryVersion()
    {
      System.String RNTRNTRNT_92 = ZipFile.LibraryVersion.ToString();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(500400uL);
      return RNTRNTRNT_92;
    }
  }
}
