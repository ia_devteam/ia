using System;
using System.Collections.Generic;
using System.Text;
namespace Ionic.Zip
{
  public delegate void WriteDelegate(string entryName, System.IO.Stream stream);
  public delegate System.IO.Stream OpenDelegate(string entryName);
  public delegate void CloseDelegate(string entryName, System.IO.Stream stream);
  public delegate Ionic.Zlib.CompressionLevel SetCompressionCallback(string localFileName, string fileNameInArchive);
  public enum ZipProgressEventType
  {
    Adding_Started,
    Adding_AfterAddEntry,
    Adding_Completed,
    Reading_Started,
    Reading_BeforeReadEntry,
    Reading_AfterReadEntry,
    Reading_Completed,
    Reading_ArchiveBytesRead,
    Saving_Started,
    Saving_BeforeWriteEntry,
    Saving_AfterWriteEntry,
    Saving_Completed,
    Saving_AfterSaveTempArchive,
    Saving_BeforeRenameTempArchive,
    Saving_AfterRenameTempArchive,
    Saving_AfterCompileSelfExtractor,
    Saving_EntryBytesRead,
    Extracting_BeforeExtractEntry,
    Extracting_AfterExtractEntry,
    Extracting_ExtractEntryWouldOverwrite,
    Extracting_EntryBytesWritten,
    Extracting_BeforeExtractAll,
    Extracting_AfterExtractAll,
    Error_Saving
  }
  public class ZipProgressEventArgs : EventArgs
  {
    private int _entriesTotal;
    private bool _cancel;
    private ZipEntry _latestEntry;
    private ZipProgressEventType _flavor;
    private String _archiveName;
    private Int64 _bytesTransferred;
    private Int64 _totalBytesToTransfer;
    internal ZipProgressEventArgs()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501191uL);
    }
    internal ZipProgressEventArgs(string archiveName, ZipProgressEventType flavor)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501192uL);
      this._archiveName = archiveName;
      this._flavor = flavor;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501193uL);
    }
    public int EntriesTotal {
      get {
        System.Int32 RNTRNTRNT_192 = _entriesTotal;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501194uL);
        return RNTRNTRNT_192;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501195uL);
        _entriesTotal = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501196uL);
      }
    }
    public ZipEntry CurrentEntry {
      get {
        ZipEntry RNTRNTRNT_193 = _latestEntry;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501197uL);
        return RNTRNTRNT_193;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501198uL);
        _latestEntry = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501199uL);
      }
    }
    public bool Cancel {
      get {
        System.Boolean RNTRNTRNT_194 = _cancel;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501200uL);
        return RNTRNTRNT_194;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501201uL);
        _cancel = _cancel || value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501202uL);
      }
    }
    public ZipProgressEventType EventType {
      get {
        ZipProgressEventType RNTRNTRNT_195 = _flavor;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501203uL);
        return RNTRNTRNT_195;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501204uL);
        _flavor = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501205uL);
      }
    }
    public String ArchiveName {
      get {
        String RNTRNTRNT_196 = _archiveName;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501206uL);
        return RNTRNTRNT_196;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501207uL);
        _archiveName = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501208uL);
      }
    }
    public Int64 BytesTransferred {
      get {
        Int64 RNTRNTRNT_197 = _bytesTransferred;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501209uL);
        return RNTRNTRNT_197;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501210uL);
        _bytesTransferred = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501211uL);
      }
    }
    public Int64 TotalBytesToTransfer {
      get {
        Int64 RNTRNTRNT_198 = _totalBytesToTransfer;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501212uL);
        return RNTRNTRNT_198;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501213uL);
        _totalBytesToTransfer = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501214uL);
      }
    }
  }
  public class ReadProgressEventArgs : ZipProgressEventArgs
  {
    internal ReadProgressEventArgs()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501215uL);
    }
    private ReadProgressEventArgs(string archiveName, ZipProgressEventType flavor) : base(archiveName, flavor)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501216uL);
    }
    static internal ReadProgressEventArgs Before(string archiveName, int entriesTotal)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501217uL);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_BeforeReadEntry);
      x.EntriesTotal = entriesTotal;
      ReadProgressEventArgs RNTRNTRNT_199 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501218uL);
      return RNTRNTRNT_199;
    }
    static internal ReadProgressEventArgs After(string archiveName, ZipEntry entry, int entriesTotal)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501219uL);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_AfterReadEntry);
      x.EntriesTotal = entriesTotal;
      x.CurrentEntry = entry;
      ReadProgressEventArgs RNTRNTRNT_200 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501220uL);
      return RNTRNTRNT_200;
    }
    static internal ReadProgressEventArgs Started(string archiveName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501221uL);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_Started);
      ReadProgressEventArgs RNTRNTRNT_201 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501222uL);
      return RNTRNTRNT_201;
    }
    static internal ReadProgressEventArgs ByteUpdate(string archiveName, ZipEntry entry, Int64 bytesXferred, Int64 totalBytes)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501223uL);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_ArchiveBytesRead);
      x.CurrentEntry = entry;
      x.BytesTransferred = bytesXferred;
      x.TotalBytesToTransfer = totalBytes;
      ReadProgressEventArgs RNTRNTRNT_202 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501224uL);
      return RNTRNTRNT_202;
    }
    static internal ReadProgressEventArgs Completed(string archiveName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501225uL);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_Completed);
      ReadProgressEventArgs RNTRNTRNT_203 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501226uL);
      return RNTRNTRNT_203;
    }
  }
  public class AddProgressEventArgs : ZipProgressEventArgs
  {
    internal AddProgressEventArgs()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501227uL);
    }
    private AddProgressEventArgs(string archiveName, ZipProgressEventType flavor) : base(archiveName, flavor)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501228uL);
    }
    static internal AddProgressEventArgs AfterEntry(string archiveName, ZipEntry entry, int entriesTotal)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501229uL);
      var x = new AddProgressEventArgs(archiveName, ZipProgressEventType.Adding_AfterAddEntry);
      x.EntriesTotal = entriesTotal;
      x.CurrentEntry = entry;
      AddProgressEventArgs RNTRNTRNT_204 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501230uL);
      return RNTRNTRNT_204;
    }
    static internal AddProgressEventArgs Started(string archiveName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501231uL);
      var x = new AddProgressEventArgs(archiveName, ZipProgressEventType.Adding_Started);
      AddProgressEventArgs RNTRNTRNT_205 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501232uL);
      return RNTRNTRNT_205;
    }
    static internal AddProgressEventArgs Completed(string archiveName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501233uL);
      var x = new AddProgressEventArgs(archiveName, ZipProgressEventType.Adding_Completed);
      AddProgressEventArgs RNTRNTRNT_206 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501234uL);
      return RNTRNTRNT_206;
    }
  }
  public class SaveProgressEventArgs : ZipProgressEventArgs
  {
    private int _entriesSaved;
    internal SaveProgressEventArgs(string archiveName, bool before, int entriesTotal, int entriesSaved, ZipEntry entry) : base(archiveName, (before) ? ZipProgressEventType.Saving_BeforeWriteEntry : ZipProgressEventType.Saving_AfterWriteEntry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501235uL);
      this.EntriesTotal = entriesTotal;
      this.CurrentEntry = entry;
      this._entriesSaved = entriesSaved;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501236uL);
    }
    internal SaveProgressEventArgs()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501237uL);
    }
    internal SaveProgressEventArgs(string archiveName, ZipProgressEventType flavor) : base(archiveName, flavor)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501238uL);
    }
    static internal SaveProgressEventArgs ByteUpdate(string archiveName, ZipEntry entry, Int64 bytesXferred, Int64 totalBytes)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501239uL);
      var x = new SaveProgressEventArgs(archiveName, ZipProgressEventType.Saving_EntryBytesRead);
      x.ArchiveName = archiveName;
      x.CurrentEntry = entry;
      x.BytesTransferred = bytesXferred;
      x.TotalBytesToTransfer = totalBytes;
      SaveProgressEventArgs RNTRNTRNT_207 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501240uL);
      return RNTRNTRNT_207;
    }
    static internal SaveProgressEventArgs Started(string archiveName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501241uL);
      var x = new SaveProgressEventArgs(archiveName, ZipProgressEventType.Saving_Started);
      SaveProgressEventArgs RNTRNTRNT_208 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501242uL);
      return RNTRNTRNT_208;
    }
    static internal SaveProgressEventArgs Completed(string archiveName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501243uL);
      var x = new SaveProgressEventArgs(archiveName, ZipProgressEventType.Saving_Completed);
      SaveProgressEventArgs RNTRNTRNT_209 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501244uL);
      return RNTRNTRNT_209;
    }
    public int EntriesSaved {
      get {
        System.Int32 RNTRNTRNT_210 = _entriesSaved;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501245uL);
        return RNTRNTRNT_210;
      }
    }
  }
  public class ExtractProgressEventArgs : ZipProgressEventArgs
  {
    private int _entriesExtracted;
    private string _target;
    internal ExtractProgressEventArgs(string archiveName, bool before, int entriesTotal, int entriesExtracted, ZipEntry entry, string extractLocation) : base(archiveName, (before) ? ZipProgressEventType.Extracting_BeforeExtractEntry : ZipProgressEventType.Extracting_AfterExtractEntry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501246uL);
      this.EntriesTotal = entriesTotal;
      this.CurrentEntry = entry;
      this._entriesExtracted = entriesExtracted;
      this._target = extractLocation;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501247uL);
    }
    internal ExtractProgressEventArgs(string archiveName, ZipProgressEventType flavor) : base(archiveName, flavor)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501248uL);
    }
    internal ExtractProgressEventArgs()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501249uL);
    }
    static internal ExtractProgressEventArgs BeforeExtractEntry(string archiveName, ZipEntry entry, string extractLocation)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501250uL);
      var x = new ExtractProgressEventArgs {
        ArchiveName = archiveName,
        EventType = ZipProgressEventType.Extracting_BeforeExtractEntry,
        CurrentEntry = entry,
        _target = extractLocation
      };
      ExtractProgressEventArgs RNTRNTRNT_211 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501251uL);
      return RNTRNTRNT_211;
    }
    static internal ExtractProgressEventArgs ExtractExisting(string archiveName, ZipEntry entry, string extractLocation)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501252uL);
      var x = new ExtractProgressEventArgs {
        ArchiveName = archiveName,
        EventType = ZipProgressEventType.Extracting_ExtractEntryWouldOverwrite,
        CurrentEntry = entry,
        _target = extractLocation
      };
      ExtractProgressEventArgs RNTRNTRNT_212 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501253uL);
      return RNTRNTRNT_212;
    }
    static internal ExtractProgressEventArgs AfterExtractEntry(string archiveName, ZipEntry entry, string extractLocation)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501254uL);
      var x = new ExtractProgressEventArgs {
        ArchiveName = archiveName,
        EventType = ZipProgressEventType.Extracting_AfterExtractEntry,
        CurrentEntry = entry,
        _target = extractLocation
      };
      ExtractProgressEventArgs RNTRNTRNT_213 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501255uL);
      return RNTRNTRNT_213;
    }
    static internal ExtractProgressEventArgs ExtractAllStarted(string archiveName, string extractLocation)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501256uL);
      var x = new ExtractProgressEventArgs(archiveName, ZipProgressEventType.Extracting_BeforeExtractAll);
      x._target = extractLocation;
      ExtractProgressEventArgs RNTRNTRNT_214 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501257uL);
      return RNTRNTRNT_214;
    }
    static internal ExtractProgressEventArgs ExtractAllCompleted(string archiveName, string extractLocation)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501258uL);
      var x = new ExtractProgressEventArgs(archiveName, ZipProgressEventType.Extracting_AfterExtractAll);
      x._target = extractLocation;
      ExtractProgressEventArgs RNTRNTRNT_215 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501259uL);
      return RNTRNTRNT_215;
    }
    static internal ExtractProgressEventArgs ByteUpdate(string archiveName, ZipEntry entry, Int64 bytesWritten, Int64 totalBytes)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501260uL);
      var x = new ExtractProgressEventArgs(archiveName, ZipProgressEventType.Extracting_EntryBytesWritten);
      x.ArchiveName = archiveName;
      x.CurrentEntry = entry;
      x.BytesTransferred = bytesWritten;
      x.TotalBytesToTransfer = totalBytes;
      ExtractProgressEventArgs RNTRNTRNT_216 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501261uL);
      return RNTRNTRNT_216;
    }
    public int EntriesExtracted {
      get {
        System.Int32 RNTRNTRNT_217 = _entriesExtracted;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501262uL);
        return RNTRNTRNT_217;
      }
    }
    public String ExtractLocation {
      get {
        String RNTRNTRNT_218 = _target;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501263uL);
        return RNTRNTRNT_218;
      }
    }
  }
  public class ZipErrorEventArgs : ZipProgressEventArgs
  {
    private Exception _exc;
    private ZipErrorEventArgs()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501264uL);
    }
    static internal ZipErrorEventArgs Saving(string archiveName, ZipEntry entry, Exception exception)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501265uL);
      var x = new ZipErrorEventArgs {
        EventType = ZipProgressEventType.Error_Saving,
        ArchiveName = archiveName,
        CurrentEntry = entry,
        _exc = exception
      };
      ZipErrorEventArgs RNTRNTRNT_219 = x;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501266uL);
      return RNTRNTRNT_219;
    }
    public Exception Exception {
      get {
        Exception RNTRNTRNT_220 = _exc;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501267uL);
        return RNTRNTRNT_220;
      }
    }
    public String FileName {
      get {
        String RNTRNTRNT_221 = CurrentEntry.LocalFileName;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(501268uL);
        return RNTRNTRNT_221;
      }
    }
  }
}
