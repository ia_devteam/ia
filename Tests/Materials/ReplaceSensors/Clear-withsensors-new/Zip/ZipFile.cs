using System;
using System.IO;
using System.Collections.Generic;
using Interop = System.Runtime.InteropServices;
namespace Ionic.Zip
{
  [Interop.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d00005")]
  [Interop.ComVisible(true)]
  [Interop.ClassInterface(Interop.ClassInterfaceType.AutoDispatch)]
  public partial class ZipFile : System.Collections.IEnumerable, System.Collections.Generic.IEnumerable<ZipEntry>, IDisposable
  {
    public bool FullScan { get; set; }
    public bool SortEntriesBeforeSaving { get; set; }
    public bool AddDirectoryWillTraverseReparsePoints { get; set; }
    public int BufferSize {
      get {
        System.Int32 RNTRNTRNT_371 = _BufferSize;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502691uL);
        return RNTRNTRNT_371;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502692uL);
        _BufferSize = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502693uL);
      }
    }
    public int CodecBufferSize { get; set; }
    public bool FlattenFoldersOnExtract { get; set; }
    public Ionic.Zlib.CompressionStrategy Strategy {
      get {
        Ionic.Zlib.CompressionStrategy RNTRNTRNT_372 = _Strategy;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502694uL);
        return RNTRNTRNT_372;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502695uL);
        _Strategy = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502696uL);
      }
    }
    public string Name {
      get {
        System.String RNTRNTRNT_373 = _name;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502697uL);
        return RNTRNTRNT_373;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502698uL);
        _name = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502699uL);
      }
    }
    public Ionic.Zlib.CompressionLevel CompressionLevel { get; set; }
    public Ionic.Zip.CompressionMethod CompressionMethod {
      get {
        Ionic.Zip.CompressionMethod RNTRNTRNT_374 = _compressionMethod;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502700uL);
        return RNTRNTRNT_374;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502701uL);
        _compressionMethod = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502702uL);
      }
    }
    public string Comment {
      get {
        System.String RNTRNTRNT_375 = _Comment;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502703uL);
        return RNTRNTRNT_375;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502704uL);
        _Comment = value;
        _contentsChanged = true;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502705uL);
      }
    }
    public bool EmitTimesInWindowsFormatWhenSaving {
      get {
        System.Boolean RNTRNTRNT_376 = _emitNtfsTimes;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502706uL);
        return RNTRNTRNT_376;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502707uL);
        _emitNtfsTimes = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502708uL);
      }
    }
    public bool EmitTimesInUnixFormatWhenSaving {
      get {
        System.Boolean RNTRNTRNT_377 = _emitUnixTimes;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502709uL);
        return RNTRNTRNT_377;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502710uL);
        _emitUnixTimes = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502711uL);
      }
    }
    internal bool Verbose {
      get {
        System.Boolean RNTRNTRNT_378 = (_StatusMessageTextWriter != null);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502712uL);
        return RNTRNTRNT_378;
      }
    }
    public bool ContainsEntry(string name)
    {
      System.Boolean RNTRNTRNT_379 = _entries.ContainsKey(SharedUtilities.NormalizePathForUseInZipFile(name));
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502713uL);
      return RNTRNTRNT_379;
    }
    public bool CaseSensitiveRetrieval {
      get {
        System.Boolean RNTRNTRNT_380 = _CaseSensitiveRetrieval;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502714uL);
        return RNTRNTRNT_380;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502715uL);
        if (value != _CaseSensitiveRetrieval) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502716uL);
          _CaseSensitiveRetrieval = value;
          _initEntriesDictionary();
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502717uL);
      }
    }
    [Obsolete("Beginning with v1.9.1.6 of DotNetZip, this property is obsolete.  It will be removed in a future version of the library. Your applications should  use AlternateEncoding and AlternateEncodingUsage instead.")]
    public bool UseUnicodeAsNecessary {
      get {
        System.Boolean RNTRNTRNT_381 = (_alternateEncoding == System.Text.Encoding.GetEncoding("UTF-8")) && (_alternateEncodingUsage == ZipOption.AsNecessary);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502718uL);
        return RNTRNTRNT_381;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502719uL);
        if (value) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502720uL);
          _alternateEncoding = System.Text.Encoding.GetEncoding("UTF-8");
          _alternateEncodingUsage = ZipOption.AsNecessary;
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502721uL);
          _alternateEncoding = Ionic.Zip.ZipFile.DefaultEncoding;
          _alternateEncodingUsage = ZipOption.Never;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502722uL);
      }
    }
    public Zip64Option UseZip64WhenSaving {
      get {
        Zip64Option RNTRNTRNT_382 = _zip64;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502723uL);
        return RNTRNTRNT_382;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502724uL);
        _zip64 = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502725uL);
      }
    }
    public Nullable<bool> RequiresZip64 {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502726uL);
        if (_entries.Count > 65534) {
          Nullable<System.Boolean> RNTRNTRNT_383 = new Nullable<bool>(true);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502727uL);
          return RNTRNTRNT_383;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502728uL);
        if (!_hasBeenSaved || _contentsChanged) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502729uL);
          return null;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502730uL);
        foreach (ZipEntry e in _entries.Values) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502731uL);
          if (e.RequiresZip64.Value) {
            Nullable<System.Boolean> RNTRNTRNT_384 = new Nullable<bool>(true);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502732uL);
            return RNTRNTRNT_384;
          }
        }
        Nullable<System.Boolean> RNTRNTRNT_385 = new Nullable<bool>(false);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502733uL);
        return RNTRNTRNT_385;
      }
    }
    public Nullable<bool> OutputUsedZip64 {
      get {
        Nullable<System.Boolean> RNTRNTRNT_386 = _OutputUsesZip64;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502734uL);
        return RNTRNTRNT_386;
      }
    }
    public Nullable<bool> InputUsesZip64 {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502735uL);
        if (_entries.Count > 65534) {
          Nullable<System.Boolean> RNTRNTRNT_387 = true;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502736uL);
          return RNTRNTRNT_387;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502737uL);
        foreach (ZipEntry e in this) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502738uL);
          if (e.Source != ZipEntrySource.ZipFile) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502739uL);
            return null;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502740uL);
          if (e._InputUsesZip64) {
            Nullable<System.Boolean> RNTRNTRNT_388 = true;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502741uL);
            return RNTRNTRNT_388;
          }
        }
        Nullable<System.Boolean> RNTRNTRNT_389 = false;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502742uL);
        return RNTRNTRNT_389;
      }
    }
    [Obsolete("use AlternateEncoding instead.")]
    public System.Text.Encoding ProvisionalAlternateEncoding {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502743uL);
        if (_alternateEncodingUsage == ZipOption.AsNecessary) {
          System.Text.Encoding RNTRNTRNT_390 = _alternateEncoding;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502744uL);
          return RNTRNTRNT_390;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502745uL);
        return null;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502746uL);
        _alternateEncoding = value;
        _alternateEncodingUsage = ZipOption.AsNecessary;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502747uL);
      }
    }
    public System.Text.Encoding AlternateEncoding {
      get {
        System.Text.Encoding RNTRNTRNT_391 = _alternateEncoding;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502748uL);
        return RNTRNTRNT_391;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502749uL);
        _alternateEncoding = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502750uL);
      }
    }
    public ZipOption AlternateEncodingUsage {
      get {
        ZipOption RNTRNTRNT_392 = _alternateEncodingUsage;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502751uL);
        return RNTRNTRNT_392;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502752uL);
        _alternateEncodingUsage = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502753uL);
      }
    }
    public static System.Text.Encoding DefaultEncoding {
      get {
        System.Text.Encoding RNTRNTRNT_393 = _defaultEncoding;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502754uL);
        return RNTRNTRNT_393;
      }
    }
    public TextWriter StatusMessageTextWriter {
      get {
        TextWriter RNTRNTRNT_394 = _StatusMessageTextWriter;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502755uL);
        return RNTRNTRNT_394;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502756uL);
        _StatusMessageTextWriter = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502757uL);
      }
    }
    public String TempFileFolder {
      get {
        String RNTRNTRNT_395 = _TempFileFolder;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502758uL);
        return RNTRNTRNT_395;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502759uL);
        _TempFileFolder = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502760uL);
        if (value == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502761uL);
          return;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502762uL);
        if (!Directory.Exists(value)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502763uL);
          throw new FileNotFoundException(String.Format("That directory ({0}) does not exist.", value));
        }
      }
    }
    public String Password {
      private get {
        String RNTRNTRNT_396 = _Password;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502764uL);
        return RNTRNTRNT_396;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502765uL);
        _Password = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502766uL);
        if (_Password == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502767uL);
          Encryption = EncryptionAlgorithm.None;
        } else if (Encryption == EncryptionAlgorithm.None) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502768uL);
          Encryption = EncryptionAlgorithm.PkzipWeak;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502769uL);
      }
    }
    public ExtractExistingFileAction ExtractExistingFile { get; set; }
    public ZipErrorAction ZipErrorAction {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502770uL);
        if (ZipError != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502771uL);
          _zipErrorAction = ZipErrorAction.InvokeErrorEvent;
        }
        ZipErrorAction RNTRNTRNT_397 = _zipErrorAction;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502772uL);
        return RNTRNTRNT_397;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502773uL);
        _zipErrorAction = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502774uL);
        if (_zipErrorAction != ZipErrorAction.InvokeErrorEvent && ZipError != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502775uL);
          ZipError = null;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502776uL);
      }
    }
    public EncryptionAlgorithm Encryption {
      get {
        EncryptionAlgorithm RNTRNTRNT_398 = _Encryption;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502777uL);
        return RNTRNTRNT_398;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502778uL);
        if (value == EncryptionAlgorithm.Unsupported) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502779uL);
          throw new InvalidOperationException("You may not set Encryption to that value.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502780uL);
        _Encryption = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502781uL);
      }
    }
    public SetCompressionCallback SetCompression { get; set; }
    public Int32 MaxOutputSegmentSize {
      get {
        Int32 RNTRNTRNT_399 = _maxOutputSegmentSize;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502782uL);
        return RNTRNTRNT_399;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502783uL);
        if (value < 65536 && value != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502784uL);
          throw new ZipException("The minimum acceptable segment size is 65536.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502785uL);
        _maxOutputSegmentSize = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502786uL);
      }
    }
    public Int32 NumberOfSegmentsForMostRecentSave {
      get {
        Int32 RNTRNTRNT_400 = unchecked((Int32)_numberOfSegmentsForMostRecentSave + 1);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502787uL);
        return RNTRNTRNT_400;
      }
    }
    public long ParallelDeflateThreshold {
      get {
        System.Int64 RNTRNTRNT_401 = _ParallelDeflateThreshold;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502788uL);
        return RNTRNTRNT_401;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502789uL);
        if ((value != 0) && (value != -1) && (value < 64 * 1024)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502790uL);
          throw new ArgumentOutOfRangeException("ParallelDeflateThreshold should be -1, 0, or > 65536");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502791uL);
        _ParallelDeflateThreshold = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502792uL);
      }
    }
    public int ParallelDeflateMaxBufferPairs {
      get {
        System.Int32 RNTRNTRNT_402 = _maxBufferPairs;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502793uL);
        return RNTRNTRNT_402;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502794uL);
        if (value < 4) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502795uL);
          throw new ArgumentOutOfRangeException("ParallelDeflateMaxBufferPairs", "Value must be 4 or greater.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502796uL);
        _maxBufferPairs = value;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502797uL);
      }
    }
    public override String ToString()
    {
      String RNTRNTRNT_403 = String.Format("ZipFile::{0}", Name);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502798uL);
      return RNTRNTRNT_403;
    }
    public static System.Version LibraryVersion {
      get {
        System.Version RNTRNTRNT_404 = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502799uL);
        return RNTRNTRNT_404;
      }
    }
    internal void NotifyEntryChanged()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502800uL);
      _contentsChanged = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502801uL);
    }
    internal Stream StreamForDiskNumber(uint diskNumber)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502802uL);
      if (diskNumber + 1 == this._diskNumberWithCd || (diskNumber == 0 && this._diskNumberWithCd == 0)) {
        Stream RNTRNTRNT_405 = this.ReadStream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502803uL);
        return RNTRNTRNT_405;
      }
      Stream RNTRNTRNT_406 = ZipSegmentedStream.ForReading(this._readName ?? this._name, diskNumber, _diskNumberWithCd);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502804uL);
      return RNTRNTRNT_406;
    }
    internal void Reset(bool whileSaving)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502805uL);
      if (_JustSaved) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502806uL);
        using (ZipFile x = new ZipFile()) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502807uL);
          x._readName = x._name = whileSaving ? (this._readName ?? this._name) : this._name;
          x.AlternateEncoding = this.AlternateEncoding;
          x.AlternateEncodingUsage = this.AlternateEncodingUsage;
          ReadIntoInstance(x);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502808uL);
          foreach (ZipEntry e1 in x) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502809uL);
            foreach (ZipEntry e2 in this) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502810uL);
              if (e1.FileName == e2.FileName) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502811uL);
                e2.CopyMetaData(e1);
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502812uL);
                break;
              }
            }
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502813uL);
        _JustSaved = false;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502814uL);
    }
    public ZipFile(string fileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502815uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502816uL);
        _InitInstance(fileName, null);
      } catch (Exception e1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502817uL);
        throw new ZipException(String.Format("Could not read {0} as a zip file", fileName), e1);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502818uL);
    }
    public ZipFile(string fileName, System.Text.Encoding encoding)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502819uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502820uL);
        AlternateEncoding = encoding;
        AlternateEncodingUsage = ZipOption.Always;
        _InitInstance(fileName, null);
      } catch (Exception e1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502821uL);
        throw new ZipException(String.Format("{0} is not a valid zip file", fileName), e1);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502822uL);
    }
    public ZipFile()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502823uL);
      _InitInstance(null, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502824uL);
    }
    public ZipFile(System.Text.Encoding encoding)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502825uL);
      AlternateEncoding = encoding;
      AlternateEncodingUsage = ZipOption.Always;
      _InitInstance(null, null);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502826uL);
    }
    public ZipFile(string fileName, TextWriter statusMessageWriter)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502827uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502828uL);
        _InitInstance(fileName, statusMessageWriter);
      } catch (Exception e1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502829uL);
        throw new ZipException(String.Format("{0} is not a valid zip file", fileName), e1);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502830uL);
    }
    public ZipFile(string fileName, TextWriter statusMessageWriter, System.Text.Encoding encoding)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502831uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502832uL);
        AlternateEncoding = encoding;
        AlternateEncodingUsage = ZipOption.Always;
        _InitInstance(fileName, statusMessageWriter);
      } catch (Exception e1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502833uL);
        throw new ZipException(String.Format("{0} is not a valid zip file", fileName), e1);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502834uL);
    }
    public void Initialize(string fileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502835uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502836uL);
        _InitInstance(fileName, null);
      } catch (Exception e1) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502837uL);
        throw new ZipException(String.Format("{0} is not a valid zip file", fileName), e1);
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502838uL);
    }
    private void _initEntriesDictionary()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502839uL);
      StringComparer sc = (CaseSensitiveRetrieval) ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase;
      _entries = (_entries == null) ? new Dictionary<String, ZipEntry>(sc) : new Dictionary<String, ZipEntry>(_entries, sc);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502840uL);
    }
    private void _InitInstance(string zipFileName, TextWriter statusMessageWriter)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502841uL);
      _name = zipFileName;
      _StatusMessageTextWriter = statusMessageWriter;
      _contentsChanged = true;
      AddDirectoryWillTraverseReparsePoints = true;
      CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
      ParallelDeflateThreshold = 512 * 1024;
      _initEntriesDictionary();
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502842uL);
      if (File.Exists(_name)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502843uL);
        if (FullScan) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502844uL);
          ReadIntoInstance_Orig(this);
        } else {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502845uL);
          ReadIntoInstance(this);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502846uL);
        this._fileAlreadyExists = true;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502847uL);
      return;
    }
    private List<ZipEntry> ZipEntriesAsList {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502848uL);
        if (_zipEntriesAsList == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502849uL);
          _zipEntriesAsList = new List<ZipEntry>(_entries.Values);
        }
        List<ZipEntry> RNTRNTRNT_407 = _zipEntriesAsList;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502850uL);
        return RNTRNTRNT_407;
      }
    }
    public ZipEntry this[int ix] {
      get {
        ZipEntry RNTRNTRNT_408 = ZipEntriesAsList[ix];
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502851uL);
        return RNTRNTRNT_408;
      }
    }
    public ZipEntry this[String fileName] {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502852uL);
        var key = SharedUtilities.NormalizePathForUseInZipFile(fileName);
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502853uL);
        if (_entries.ContainsKey(key)) {
          ZipEntry RNTRNTRNT_409 = _entries[key];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502854uL);
          return RNTRNTRNT_409;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502855uL);
        key = key.Replace("/", "\\");
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502856uL);
        if (_entries.ContainsKey(key)) {
          ZipEntry RNTRNTRNT_410 = _entries[key];
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502857uL);
          return RNTRNTRNT_410;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502858uL);
        return null;
      }
    }
    public System.Collections.Generic.ICollection<String> EntryFileNames {
      get {
        System.Collections.Generic.ICollection<String> RNTRNTRNT_411 = _entries.Keys;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502859uL);
        return RNTRNTRNT_411;
      }
    }
    public System.Collections.Generic.ICollection<ZipEntry> Entries {
      get {
        System.Collections.Generic.ICollection<ZipEntry> RNTRNTRNT_412 = _entries.Values;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502860uL);
        return RNTRNTRNT_412;
      }
    }
    public System.Collections.Generic.ICollection<ZipEntry> EntriesSorted {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502861uL);
        var coll = new System.Collections.Generic.List<ZipEntry>();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502862uL);
        foreach (var e in this.Entries) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502863uL);
          coll.Add(e);
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502864uL);
        StringComparison sc = (CaseSensitiveRetrieval) ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase;
        coll.Sort((x, y) => { return String.Compare(x.FileName, y.FileName, sc); });
        System.Collections.Generic.ICollection<ZipEntry> RNTRNTRNT_413 = coll.AsReadOnly();
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502865uL);
        return RNTRNTRNT_413;
      }
    }
    public int Count {
      get {
        System.Int32 RNTRNTRNT_414 = _entries.Count;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502866uL);
        return RNTRNTRNT_414;
      }
    }
    public void RemoveEntry(ZipEntry entry)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502867uL);
      if (entry == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502868uL);
        throw new ArgumentNullException("entry");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502869uL);
      _entries.Remove(SharedUtilities.NormalizePathForUseInZipFile(entry.FileName));
      _zipEntriesAsList = null;
      _contentsChanged = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502870uL);
    }
    public void RemoveEntry(String fileName)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502871uL);
      string modifiedName = ZipEntry.NameInArchive(fileName, null);
      ZipEntry e = this[modifiedName];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502872uL);
      if (e == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502873uL);
        throw new ArgumentException("The entry you specified was not found in the zip archive.");
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502874uL);
      RemoveEntry(e);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502875uL);
    }
    public void Dispose()
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502876uL);
      Dispose(true);
      GC.SuppressFinalize(this);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502877uL);
    }
    protected virtual void Dispose(bool disposeManagedResources)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502878uL);
      if (!this._disposed) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502879uL);
        if (disposeManagedResources) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502880uL);
          if (_ReadStreamIsOurs) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502881uL);
            if (_readstream != null) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502882uL);
              _readstream.Dispose();
              _readstream = null;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502883uL);
          if ((_temporaryFileName != null) && (_name != null)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502884uL);
            if (_writestream != null) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502885uL);
              _writestream.Dispose();
              _writestream = null;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502886uL);
          if (this.ParallelDeflater != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502887uL);
            this.ParallelDeflater.Dispose();
            this.ParallelDeflater = null;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502888uL);
        this._disposed = true;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502889uL);
    }
    internal Stream ReadStream {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502890uL);
        if (_readstream == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502891uL);
          if (_readName != null || _name != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502892uL);
            _readstream = File.Open(_readName ?? _name, FileMode.Open, FileAccess.Read, FileShare.Read | FileShare.Write);
            _ReadStreamIsOurs = true;
          }
        }
        Stream RNTRNTRNT_415 = _readstream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502893uL);
        return RNTRNTRNT_415;
      }
    }
    private Stream WriteStream {
      get {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502894uL);
        if (_writestream != null) {
          Stream RNTRNTRNT_416 = _writestream;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502895uL);
          return RNTRNTRNT_416;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502896uL);
        if (_name == null) {
          Stream RNTRNTRNT_417 = _writestream;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502897uL);
          return RNTRNTRNT_417;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502898uL);
        if (_maxOutputSegmentSize != 0) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502899uL);
          _writestream = ZipSegmentedStream.ForWriting(this._name, _maxOutputSegmentSize);
          Stream RNTRNTRNT_418 = _writestream;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502900uL);
          return RNTRNTRNT_418;
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502901uL);
        SharedUtilities.CreateAndOpenUniqueTempFile(TempFileFolder ?? Path.GetDirectoryName(_name), out _writestream, out _temporaryFileName);
        Stream RNTRNTRNT_419 = _writestream;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502902uL);
        return RNTRNTRNT_419;
      }
      set {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502903uL);
        if (value != null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502904uL);
          throw new ZipException("Cannot set the stream to a non-null value.");
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502905uL);
        _writestream = null;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(502906uL);
      }
    }
    private TextWriter _StatusMessageTextWriter;
    private bool _CaseSensitiveRetrieval;
    private Stream _readstream;
    private Stream _writestream;
    private UInt16 _versionMadeBy;
    private UInt16 _versionNeededToExtract;
    private UInt32 _diskNumberWithCd;
    private Int32 _maxOutputSegmentSize;
    private UInt32 _numberOfSegmentsForMostRecentSave;
    private ZipErrorAction _zipErrorAction;
    private bool _disposed;
    private System.Collections.Generic.Dictionary<String, ZipEntry> _entries;
    private List<ZipEntry> _zipEntriesAsList;
    private string _name;
    private string _readName;
    private string _Comment;
    internal string _Password;
    private bool _emitNtfsTimes = true;
    private bool _emitUnixTimes;
    private Ionic.Zlib.CompressionStrategy _Strategy = Ionic.Zlib.CompressionStrategy.Default;
    private Ionic.Zip.CompressionMethod _compressionMethod = Ionic.Zip.CompressionMethod.Deflate;
    private bool _fileAlreadyExists;
    private string _temporaryFileName;
    private bool _contentsChanged;
    private bool _hasBeenSaved;
    private String _TempFileFolder;
    private bool _ReadStreamIsOurs = true;
    private object LOCK = new object();
    private bool _saveOperationCanceled;
    private bool _extractOperationCanceled;
    private bool _addOperationCanceled;
    private EncryptionAlgorithm _Encryption;
    private bool _JustSaved;
    private long _locEndOfCDS = -1;
    private uint _OffsetOfCentralDirectory;
    private Int64 _OffsetOfCentralDirectory64;
    private Nullable<bool> _OutputUsesZip64;
    internal bool _inExtractAll;
    private System.Text.Encoding _alternateEncoding = System.Text.Encoding.GetEncoding("IBM437");
    private ZipOption _alternateEncodingUsage = ZipOption.Never;
    private static System.Text.Encoding _defaultEncoding = System.Text.Encoding.GetEncoding("IBM437");
    private int _BufferSize = BufferSizeDefault;
    internal Ionic.Zlib.ParallelDeflateOutputStream ParallelDeflater;
    private long _ParallelDeflateThreshold;
    private int _maxBufferPairs = 16;
    internal Zip64Option _zip64 = Zip64Option.Default;
    private bool _SavingSfx;
    public static readonly int BufferSizeDefault = 32768;
  }
  public enum Zip64Option
  {
    Default = 0,
    Never = 0,
    AsNecessary = 1,
    Always
  }
  public enum ZipOption
  {
    Default = 0,
    Never = 0,
    AsNecessary = 1,
    Always
  }
  enum AddOrUpdateAction
  {
    AddOnly = 0,
    AddOrUpdate
  }
}
