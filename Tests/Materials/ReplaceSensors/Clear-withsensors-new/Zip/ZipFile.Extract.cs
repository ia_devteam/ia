using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public partial class ZipFile
  {
    public void ExtractAll(string path)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503008uL);
      _InternalExtractAll(path, true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503009uL);
    }
    public void ExtractAll(string path, ExtractExistingFileAction extractExistingFile)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503010uL);
      ExtractExistingFile = extractExistingFile;
      _InternalExtractAll(path, true);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503011uL);
    }
    private void _InternalExtractAll(string path, bool overrideExtractExistingProperty)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503012uL);
      bool header = Verbose;
      _inExtractAll = true;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503013uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503014uL);
        OnExtractAllStarted(path);
        int n = 0;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503015uL);
        foreach (ZipEntry e in _entries.Values) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503016uL);
          if (header) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503017uL);
            StatusMessageTextWriter.WriteLine("\n{1,-22} {2,-8} {3,4}   {4,-8}  {0}", "Name", "Modified", "Size", "Ratio", "Packed");
            StatusMessageTextWriter.WriteLine(new System.String('-', 72));
            header = false;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503018uL);
          if (Verbose) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503019uL);
            StatusMessageTextWriter.WriteLine("{1,-22} {2,-8} {3,4:F0}%   {4,-8} {0}", e.FileName, e.LastModified.ToString("yyyy-MM-dd HH:mm:ss"), e.UncompressedSize, e.CompressionRatio, e.CompressedSize);
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503020uL);
            if (!String.IsNullOrEmpty(e.Comment)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503021uL);
              StatusMessageTextWriter.WriteLine("  Comment: {0}", e.Comment);
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503022uL);
          e.Password = _Password;
          OnExtractEntry(n, true, e, path);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503023uL);
          if (overrideExtractExistingProperty) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503024uL);
            e.ExtractExistingFile = this.ExtractExistingFile;
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503025uL);
          e.Extract(path);
          n++;
          OnExtractEntry(n, false, e, path);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503026uL);
          if (_extractOperationCanceled) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503027uL);
            break;
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503028uL);
        if (!_extractOperationCanceled) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503029uL);
          foreach (ZipEntry e in _entries.Values) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503030uL);
            if ((e.IsDirectory) || (e.FileName.EndsWith("/"))) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503031uL);
              string outputFile = (e.FileName.StartsWith("/")) ? Path.Combine(path, e.FileName.Substring(1)) : Path.Combine(path, e.FileName);
              e._SetTimes(outputFile, false);
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503032uL);
          OnExtractAllCompleted(path);
        }
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503033uL);
        _inExtractAll = false;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503034uL);
    }
  }
}
