using System;
using System.Reflection;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public enum SelfExtractorFlavor
  {
    ConsoleApplication = 0,
    WinFormsApplication
  }
  public class SelfExtractorSaveOptions
  {
    public SelfExtractorFlavor Flavor { get; set; }
    public String PostExtractCommandLine { get; set; }
    public String DefaultExtractDirectory { get; set; }
    public string IconFile { get; set; }
    public bool Quiet { get; set; }
    public Ionic.Zip.ExtractExistingFileAction ExtractExistingFile { get; set; }
    public bool RemoveUnpackedFilesAfterExecute { get; set; }
    public Version FileVersion { get; set; }
    public String ProductVersion { get; set; }
    public String Copyright { get; set; }
    public String Description { get; set; }
    public String ProductName { get; set; }
    public String SfxExeWindowTitle { get; set; }
    public string AdditionalCompilerSwitches { get; set; }
  }
  partial class ZipFile
  {
    class ExtractorSettings
    {
      public SelfExtractorFlavor Flavor;
      public List<string> ReferencedAssemblies;
      public List<string> CopyThroughResources;
      public List<string> ResourcesToCompile;
    }
    private static ExtractorSettings[] SettingsList = {
      new ExtractorSettings {
        Flavor = SelfExtractorFlavor.WinFormsApplication,
        ReferencedAssemblies = new List<string> {
          "System.dll",
          "System.Windows.Forms.dll",
          "System.Drawing.dll"
        },
        CopyThroughResources = new List<string> {
          "Ionic.Zip.WinFormsSelfExtractorStub.resources",
          "Ionic.Zip.Forms.PasswordDialog.resources",
          "Ionic.Zip.Forms.ZipContentsDialog.resources"
        },
        ResourcesToCompile = new List<string> {
          "WinFormsSelfExtractorStub.cs",
          "WinFormsSelfExtractorStub.Designer.cs",
          "PasswordDialog.cs",
          "PasswordDialog.Designer.cs",
          "ZipContentsDialog.cs",
          "ZipContentsDialog.Designer.cs",
          "FolderBrowserDialogEx.cs"
        }
      },
      new ExtractorSettings {
        Flavor = SelfExtractorFlavor.ConsoleApplication,
        ReferencedAssemblies = new List<string> { "System.dll" },
        CopyThroughResources = null,
        ResourcesToCompile = new List<string> { "CommandLineSelfExtractorStub.cs" }
      }
    };
    public void SaveSelfExtractor(string exeToGenerate, SelfExtractorFlavor flavor)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503476uL);
      SelfExtractorSaveOptions options = new SelfExtractorSaveOptions();
      options.Flavor = flavor;
      SaveSelfExtractor(exeToGenerate, options);
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503477uL);
    }
    public void SaveSelfExtractor(string exeToGenerate, SelfExtractorSaveOptions options)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503478uL);
      if (_name == null) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503479uL);
        _writestream = null;
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503480uL);
      _SavingSfx = true;
      _name = exeToGenerate;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503481uL);
      if (Directory.Exists(_name)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503482uL);
        throw new ZipException("Bad Directory", new System.ArgumentException("That name specifies an existing directory. Please specify a filename.", "exeToGenerate"));
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503483uL);
      _contentsChanged = true;
      _fileAlreadyExists = File.Exists(_name);
      _SaveSfxStub(exeToGenerate, options);
      Save();
      _SavingSfx = false;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503484uL);
    }
    private static void ExtractResourceToFile(Assembly a, string resourceName, string filename)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503485uL);
      int n = 0;
      byte[] bytes = new byte[1024];
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503486uL);
      using (Stream instream = a.GetManifestResourceStream(resourceName)) {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503487uL);
        if (instream == null) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503488uL);
          throw new ZipException(String.Format("missing resource '{0}'", resourceName));
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503489uL);
        using (FileStream outstream = File.OpenWrite(filename)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503490uL);
          do {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503491uL);
            n = instream.Read(bytes, 0, bytes.Length);
            outstream.Write(bytes, 0, n);
          } while (n > 0);
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503492uL);
    }
    private void _SaveSfxStub(string exeToGenerate, SelfExtractorSaveOptions options)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503493uL);
      string nameOfIconFile = null;
      string stubExe = null;
      string unpackedResourceDir = null;
      string tmpDir = null;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503494uL);
      try {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503495uL);
        if (File.Exists(exeToGenerate)) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503496uL);
          if (Verbose) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503497uL);
            StatusMessageTextWriter.WriteLine("The existing file ({0}) will be overwritten.", exeToGenerate);
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503498uL);
        if (!exeToGenerate.EndsWith(".exe")) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503499uL);
          if (Verbose) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503500uL);
            StatusMessageTextWriter.WriteLine("Warning: The generated self-extracting file will not have an .exe extension.");
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503501uL);
        tmpDir = TempFileFolder ?? Path.GetDirectoryName(exeToGenerate);
        stubExe = GenerateTempPathname(tmpDir, "exe");
        Assembly a1 = typeof(ZipFile).Assembly;
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503502uL);
        using (var csharp = new Microsoft.CSharp.CSharpCodeProvider()) {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503503uL);
          ExtractorSettings settings = null;
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503504uL);
          foreach (var x in SettingsList) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503505uL);
            if (x.Flavor == options.Flavor) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503506uL);
              settings = x;
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503507uL);
              break;
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503508uL);
          if (settings == null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503509uL);
            throw new BadStateException(String.Format("While saving a Self-Extracting Zip, Cannot find that flavor ({0})?", options.Flavor));
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503510uL);
          var cp = new System.CodeDom.Compiler.CompilerParameters();
          cp.ReferencedAssemblies.Add(a1.Location);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503511uL);
          if (settings.ReferencedAssemblies != null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503512uL);
            foreach (string ra in settings.ReferencedAssemblies) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503513uL);
              cp.ReferencedAssemblies.Add(ra);
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503514uL);
          cp.GenerateInMemory = false;
          cp.GenerateExecutable = true;
          cp.IncludeDebugInformation = false;
          cp.CompilerOptions = "";
          Assembly a2 = Assembly.GetExecutingAssembly();
          var sb = new System.Text.StringBuilder();
          string sourceFile = GenerateTempPathname(tmpDir, "cs");
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503515uL);
          using (ZipFile zip = ZipFile.Read(a2.GetManifestResourceStream("Ionic.Zip.Resources.ZippedResources.zip"))) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503516uL);
            unpackedResourceDir = GenerateTempPathname(tmpDir, "tmp");
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503517uL);
            if (String.IsNullOrEmpty(options.IconFile)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503518uL);
              System.IO.Directory.CreateDirectory(unpackedResourceDir);
              ZipEntry e = zip["zippedFile.ico"];
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503519uL);
              if ((e.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503520uL);
                e.Attributes ^= FileAttributes.ReadOnly;
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503521uL);
              e.Extract(unpackedResourceDir);
              nameOfIconFile = Path.Combine(unpackedResourceDir, "zippedFile.ico");
              cp.CompilerOptions += String.Format("/win32icon:\"{0}\"", nameOfIconFile);
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503522uL);
              cp.CompilerOptions += String.Format("/win32icon:\"{0}\"", options.IconFile);
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503523uL);
            cp.OutputAssembly = stubExe;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503524uL);
            if (options.Flavor == SelfExtractorFlavor.WinFormsApplication) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503525uL);
              cp.CompilerOptions += " /target:winexe";
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503526uL);
            if (!String.IsNullOrEmpty(options.AdditionalCompilerSwitches)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503527uL);
              cp.CompilerOptions += " " + options.AdditionalCompilerSwitches;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503528uL);
            if (String.IsNullOrEmpty(cp.CompilerOptions)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503529uL);
              cp.CompilerOptions = null;
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503530uL);
            if ((settings.CopyThroughResources != null) && (settings.CopyThroughResources.Count != 0)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503531uL);
              if (!Directory.Exists(unpackedResourceDir)) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503532uL);
                System.IO.Directory.CreateDirectory(unpackedResourceDir);
              }
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503533uL);
              foreach (string re in settings.CopyThroughResources) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503534uL);
                string filename = Path.Combine(unpackedResourceDir, re);
                ExtractResourceToFile(a2, re, filename);
                cp.EmbeddedResources.Add(filename);
              }
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503535uL);
            cp.EmbeddedResources.Add(a1.Location);
            sb.Append("// " + Path.GetFileName(sourceFile) + "\n").Append("// --------------------------------------------\n//\n").Append("// This SFX source file was generated by DotNetZip ").Append(ZipFile.LibraryVersion.ToString()).Append("\n//         at ").Append(System.DateTime.Now.ToString("yyyy MMMM dd  HH:mm:ss")).Append("\n//\n// --------------------------------------------\n\n\n");
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503536uL);
            if (!String.IsNullOrEmpty(options.Description)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503537uL);
              sb.Append("[assembly: System.Reflection.AssemblyTitle(\"" + options.Description.Replace("\"", "") + "\")]\n");
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503538uL);
              sb.Append("[assembly: System.Reflection.AssemblyTitle(\"DotNetZip SFX Archive\")]\n");
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503539uL);
            if (!String.IsNullOrEmpty(options.ProductVersion)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503540uL);
              sb.Append("[assembly: System.Reflection.AssemblyInformationalVersion(\"" + options.ProductVersion.Replace("\"", "") + "\")]\n");
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503541uL);
            string copyright = (String.IsNullOrEmpty(options.Copyright)) ? "Extractor: Copyright � Dino Chiesa 2008-2011" : options.Copyright.Replace("\"", "");
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503542uL);
            if (!String.IsNullOrEmpty(options.ProductName)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503543uL);
              sb.Append("[assembly: System.Reflection.AssemblyProduct(\"").Append(options.ProductName.Replace("\"", "")).Append("\")]\n");
            } else {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503544uL);
              sb.Append("[assembly: System.Reflection.AssemblyProduct(\"DotNetZip\")]\n");
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503545uL);
            sb.Append("[assembly: System.Reflection.AssemblyCopyright(\"" + copyright + "\")]\n").Append(String.Format("[assembly: System.Reflection.AssemblyVersion(\"{0}\")]\n", ZipFile.LibraryVersion.ToString()));
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503546uL);
            if (options.FileVersion != null) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503547uL);
              sb.Append(String.Format("[assembly: System.Reflection.AssemblyFileVersion(\"{0}\")]\n", options.FileVersion.ToString()));
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503548uL);
            sb.Append("\n\n\n");
            string extractLoc = options.DefaultExtractDirectory;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503549uL);
            if (extractLoc != null) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503550uL);
              extractLoc = extractLoc.Replace("\"", "").Replace("\\", "\\\\");
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503551uL);
            string postExCmdLine = options.PostExtractCommandLine;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503552uL);
            if (postExCmdLine != null) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503553uL);
              postExCmdLine = postExCmdLine.Replace("\\", "\\\\");
              postExCmdLine = postExCmdLine.Replace("\"", "\\\"");
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503554uL);
            foreach (string rc in settings.ResourcesToCompile) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503555uL);
              using (Stream s = zip[rc].OpenReader()) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503556uL);
                if (s == null) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503557uL);
                  throw new ZipException(String.Format("missing resource '{0}'", rc));
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503558uL);
                using (StreamReader sr = new StreamReader(s)) {
                  RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503559uL);
                  while (sr.Peek() >= 0) {
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503560uL);
                    string line = sr.ReadLine();
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503561uL);
                    if (extractLoc != null) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503562uL);
                      line = line.Replace("@@EXTRACTLOCATION", extractLoc);
                    }
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503563uL);
                    line = line.Replace("@@REMOVE_AFTER_EXECUTE", options.RemoveUnpackedFilesAfterExecute.ToString());
                    line = line.Replace("@@QUIET", options.Quiet.ToString());
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503564uL);
                    if (!String.IsNullOrEmpty(options.SfxExeWindowTitle)) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503565uL);
                      line = line.Replace("@@SFX_EXE_WINDOW_TITLE", options.SfxExeWindowTitle);
                    }
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503566uL);
                    line = line.Replace("@@EXTRACT_EXISTING_FILE", ((int)options.ExtractExistingFile).ToString());
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503567uL);
                    if (postExCmdLine != null) {
                      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503568uL);
                      line = line.Replace("@@POST_UNPACK_CMD_LINE", postExCmdLine);
                    }
                    RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503569uL);
                    sb.Append(line).Append("\n");
                  }
                }
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503570uL);
                sb.Append("\n\n");
              }
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503571uL);
          string LiteralSource = sb.ToString();
          var cr = csharp.CompileAssemblyFromSource(cp, LiteralSource);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503572uL);
          if (cr == null) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503573uL);
            throw new SfxGenerationException("Cannot compile the extraction logic!");
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503574uL);
          if (Verbose) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503575uL);
            foreach (string output in cr.Output) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503576uL);
              StatusMessageTextWriter.WriteLine(output);
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503577uL);
          if (cr.Errors.Count != 0) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503578uL);
            using (TextWriter tw = new StreamWriter(sourceFile)) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503579uL);
              tw.Write(LiteralSource);
              tw.Write("\n\n\n// ------------------------------------------------------------------\n");
              tw.Write("// Errors during compilation: \n//\n");
              string p = Path.GetFileName(sourceFile);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503580uL);
              foreach (System.CodeDom.Compiler.CompilerError error in cr.Errors) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503581uL);
                tw.Write(String.Format("//   {0}({1},{2}): {3} {4}: {5}\n//\n", p, error.Line, error.Column, error.IsWarning ? "Warning" : "error", error.ErrorNumber, error.ErrorText));
              }
            }
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503582uL);
            throw new SfxGenerationException(String.Format("Errors compiling the extraction logic!  {0}", sourceFile));
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503583uL);
          OnSaveEvent(ZipProgressEventType.Saving_AfterCompileSelfExtractor);
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503584uL);
          using (System.IO.Stream input = System.IO.File.OpenRead(stubExe)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503585uL);
            byte[] buffer = new byte[4000];
            int n = 1;
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503586uL);
            while (n != 0) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503587uL);
              n = input.Read(buffer, 0, buffer.Length);
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503588uL);
              if (n != 0) {
                RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503589uL);
                WriteStream.Write(buffer, 0, n);
              }
            }
          }
        }
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503590uL);
        OnSaveEvent(ZipProgressEventType.Saving_AfterSaveTempArchive);
      } finally {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503591uL);
        try {
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503592uL);
          if (Directory.Exists(unpackedResourceDir)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503593uL);
            try {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503594uL);
              Directory.Delete(unpackedResourceDir, true);
            } catch (System.IO.IOException exc1) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503595uL);
              StatusMessageTextWriter.WriteLine("Warning: Exception: {0}", exc1);
            }
          }
          RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503596uL);
          if (File.Exists(stubExe)) {
            RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503597uL);
            try {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503598uL);
              File.Delete(stubExe);
            } catch (System.IO.IOException exc1) {
              RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503599uL);
              StatusMessageTextWriter.WriteLine("Warning: Exception: {0}", exc1);
            }
          }
        } catch (System.IO.IOException) {
        }
      }
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503600uL);
      return;
    }
    static internal string GenerateTempPathname(string dir, string extension)
    {
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503601uL);
      string candidate = null;
      String AppName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503602uL);
      do {
        RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503603uL);
        string uuid = System.Guid.NewGuid().ToString();
        string Name = String.Format("{0}-{1}-{2}.{3}", AppName, System.DateTime.Now.ToString("yyyyMMMdd-HHmmss"), uuid, extension);
        candidate = System.IO.Path.Combine(dir, Name);
      } while (System.IO.File.Exists(candidate) || System.IO.Directory.Exists(candidate));
      System.String RNTRNTRNT_465 = candidate;
      RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(503604uL);
      return RNTRNTRNT_465;
    }
  }
}
