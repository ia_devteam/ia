Sensor();
(function (global, factory) {
    Sensor();
    if (typeof module === 'object' && typeof module.exports === 'object') {
        module.exports = global.document ? factory(global, true) : function (w) {
            Sensor();
            if (!w.document) {
                throw new Error('jQuery requires a window with a document');
            }
            var IARETREPLACE = factory(w);
            Sensor();
            return IARETREPLACE;
        };
    } else {
        factory(global);
    }
    Sensor();
}(typeof window !== 'undefined' ? window : this, function (window, noGlobal) {
    Sensor();
    var deletedIds = [];
    var slice = deletedIds.slice;
    var concat = deletedIds.concat;
    var push = deletedIds.push;
    var indexOf = deletedIds.indexOf;
    var class2type = {};
    var toString = class2type.toString;
    var hasOwn = class2type.hasOwnProperty;
    var trim = ''.trim;
    var support = {};
    var version = '1.11.0', jQuery = function (selector, context) {
            var IARETREPLACE = new jQuery.fn.init(selector, context);
            Sensor();
            return IARETREPLACE;
        }, rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, rmsPrefix = /^-ms-/, rdashAlpha = /-([\da-z])/gi, fcamelCase = function (all, letter) {
            var IARETREPLACE = letter.toUpperCase();
            Sensor();
            return IARETREPLACE;
        };
    jQuery.fn = jQuery.prototype = {
        jquery: version,
        constructor: jQuery,
        selector: '',
        length: 0,
        toArray: function () {
            var IARETREPLACE = slice.call(this);
            Sensor();
            return IARETREPLACE;
        },
        get: function (num) {
            var IARETREPLACE = num != null ? num < 0 ? this[num + this.length] : this[num] : slice.call(this);
            Sensor();
            return IARETREPLACE;
        },
        pushStack: function (elems) {
            Sensor();
            var ret = jQuery.merge(this.constructor(), elems);
            ret.prevObject = this;
            ret.context = this.context;
            var IARETREPLACE = ret;
            Sensor();
            return IARETREPLACE;
        },
        each: function (callback, args) {
            var IARETREPLACE = jQuery.each(this, callback, args);
            Sensor();
            return IARETREPLACE;
        },
        map: function (callback) {
            var IARETREPLACE = this.pushStack(jQuery.map(this, function (elem, i) {
                var IARETREPLACE = callback.call(elem, i, elem);
                Sensor();
                return IARETREPLACE;
            }));
            Sensor();
            return IARETREPLACE;
        },
        slice: function () {
            var IARETREPLACE = this.pushStack(slice.apply(this, arguments));
            Sensor();
            return IARETREPLACE;
        },
        first: function () {
            var IARETREPLACE = this.eq(0);
            Sensor();
            return IARETREPLACE;
        },
        last: function () {
            var IARETREPLACE = this.eq(-1);
            Sensor();
            return IARETREPLACE;
        },
        eq: function (i) {
            Sensor();
            var len = this.length, j = +i + (i < 0 ? len : 0);
            var IARETREPLACE = this.pushStack(j >= 0 && j < len ? [this[j]] : []);
            Sensor();
            return IARETREPLACE;
        },
        end: function () {
            var IARETREPLACE = this.prevObject || this.constructor(null);
            Sensor();
            return IARETREPLACE;
        },
        push: push,
        sort: deletedIds.sort,
        splice: deletedIds.splice
    };
    jQuery.extend = jQuery.fn.extend = function () {
        Sensor();
        var src, copyIsArray, copy, name, options, clone, target = arguments[0] || {}, i = 1, length = arguments.length, deep = false;
        if (typeof target === 'boolean') {
            deep = target;
            target = arguments[i] || {};
            i++;
        }
        if (typeof target !== 'object' && !jQuery.isFunction(target)) {
            target = {};
        }
        if (i === length) {
            target = this;
            i--;
        }
        for (; i < length; i++) {
            if ((options = arguments[i]) != null) {
                for (name in options) {
                    src = target[name];
                    copy = options[name];
                    if (target === copy) {
                        continue;
                    }
                    if (deep && copy && (jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy)))) {
                        if (copyIsArray) {
                            copyIsArray = false;
                            clone = src && jQuery.isArray(src) ? src : [];
                        } else {
                            clone = src && jQuery.isPlainObject(src) ? src : {};
                        }
                        target[name] = jQuery.extend(deep, clone, copy);
                    } else {
                        if (copy !== undefined) {
                            target[name] = copy;
                        }
                    }
                }
            }
        }
        var IARETREPLACE = target;
        Sensor();
        return IARETREPLACE;
    };
    jQuery.extend({
        expando: 'jQuery' + (version + Math.random()).replace(/\D/g, ''),
        isReady: true,
        error: function (msg) {
            Sensor();
            throw new Error(msg);
        },
        noop: function () {
            Sensor();
        },
        isFunction: function (obj) {
            var IARETREPLACE = jQuery.type(obj) === 'function';
            Sensor();
            return IARETREPLACE;
        },
        isArray: Array.isArray || function (obj) {
            var IARETREPLACE = jQuery.type(obj) === 'array';
            Sensor();
            return IARETREPLACE;
        },
        isWindow: function (obj) {
            var IARETREPLACE = obj != null && obj == obj.window;
            Sensor();
            return IARETREPLACE;
        },
        isNumeric: function (obj) {
            var IARETREPLACE = obj - parseFloat(obj) >= 0;
            Sensor();
            return IARETREPLACE;
        },
        isEmptyObject: function (obj) {
            Sensor();
            var name;
            for (name in obj) {
                var IARETREPLACE = false;
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = true;
            Sensor();
            return IARETREPLACE;
        },
        isPlainObject: function (obj) {
            Sensor();
            var key;
            if (!obj || jQuery.type(obj) !== 'object' || obj.nodeType || jQuery.isWindow(obj)) {
                var IARETREPLACE = false;
                Sensor();
                return IARETREPLACE;
            }
            try {
                if (obj.constructor && !hasOwn.call(obj, 'constructor') && !hasOwn.call(obj.constructor.prototype, 'isPrototypeOf')) {
                    var IARETREPLACE = false;
                    Sensor();
                    return IARETREPLACE;
                }
            } catch (e) {
                var IARETREPLACE = false;
                Sensor();
                return IARETREPLACE;
            }
            if (support.ownLast) {
                for (key in obj) {
                    var IARETREPLACE = hasOwn.call(obj, key);
                    Sensor();
                    return IARETREPLACE;
                }
            }
            for (key in obj) {
            }
            var IARETREPLACE = key === undefined || hasOwn.call(obj, key);
            Sensor();
            return IARETREPLACE;
        },
        type: function (obj) {
            Sensor();
            if (obj == null) {
                var IARETREPLACE = obj + '';
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = typeof obj === 'object' || typeof obj === 'function' ? class2type[toString.call(obj)] || 'object' : typeof obj;
            Sensor();
            return IARETREPLACE;
        },
        globalEval: function (data) {
            Sensor();
            if (data && jQuery.trim(data)) {
                (window.execScript || function (data) {
                    Sensor();
                    window['eval'].call(window, data);
                    Sensor();
                })(data);
            }
            Sensor();
        },
        camelCase: function (string) {
            var IARETREPLACE = string.replace(rmsPrefix, 'ms-').replace(rdashAlpha, fcamelCase);
            Sensor();
            return IARETREPLACE;
        },
        nodeName: function (elem, name) {
            var IARETREPLACE = elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
            Sensor();
            return IARETREPLACE;
        },
        each: function (obj, callback, args) {
            Sensor();
            var value, i = 0, length = obj.length, isArray = isArraylike(obj);
            if (args) {
                if (isArray) {
                    for (; i < length; i++) {
                        value = callback.apply(obj[i], args);
                        if (value === false) {
                            break;
                        }
                    }
                } else {
                    for (i in obj) {
                        value = callback.apply(obj[i], args);
                        if (value === false) {
                            break;
                        }
                    }
                }
            } else {
                if (isArray) {
                    for (; i < length; i++) {
                        value = callback.call(obj[i], i, obj[i]);
                        if (value === false) {
                            break;
                        }
                    }
                } else {
                    for (i in obj) {
                        value = callback.call(obj[i], i, obj[i]);
                        if (value === false) {
                            break;
                        }
                    }
                }
            }
            var IARETREPLACE = obj;
            Sensor();
            return IARETREPLACE;
        },
        trim: trim && !trim.call('\uFEFF\xA0') ? function (text) {
            var IARETREPLACE = text == null ? '' : trim.call(text);
            Sensor();
            return IARETREPLACE;
        } : function (text) {
            var IARETREPLACE = text == null ? '' : (text + '').replace(rtrim, '');
            Sensor();
            return IARETREPLACE;
        },
        makeArray: function (arr, results) {
            Sensor();
            var ret = results || [];
            if (arr != null) {
                if (isArraylike(Object(arr))) {
                    jQuery.merge(ret, typeof arr === 'string' ? [arr] : arr);
                } else {
                    push.call(ret, arr);
                }
            }
            var IARETREPLACE = ret;
            Sensor();
            return IARETREPLACE;
        },
        inArray: function (elem, arr, i) {
            Sensor();
            var len;
            if (arr) {
                if (indexOf) {
                    var IARETREPLACE = indexOf.call(arr, elem, i);
                    Sensor();
                    return IARETREPLACE;
                }
                len = arr.length;
                i = i ? i < 0 ? Math.max(0, len + i) : i : 0;
                for (; i < len; i++) {
                    if (i in arr && arr[i] === elem) {
                        var IARETREPLACE = i;
                        Sensor();
                        return IARETREPLACE;
                    }
                }
            }
            var IARETREPLACE = -1;
            Sensor();
            return IARETREPLACE;
        },
        merge: function (first, second) {
            Sensor();
            var len = +second.length, j = 0, i = first.length;
            while (j < len) {
                first[i++] = second[j++];
            }
            if (len !== len) {
                while (second[j] !== undefined) {
                    first[i++] = second[j++];
                }
            }
            first.length = i;
            var IARETREPLACE = first;
            Sensor();
            return IARETREPLACE;
        },
        grep: function (elems, callback, invert) {
            Sensor();
            var callbackInverse, matches = [], i = 0, length = elems.length, callbackExpect = !invert;
            for (; i < length; i++) {
                callbackInverse = !callback(elems[i], i);
                if (callbackInverse !== callbackExpect) {
                    matches.push(elems[i]);
                }
            }
            var IARETREPLACE = matches;
            Sensor();
            return IARETREPLACE;
        },
        map: function (elems, callback, arg) {
            Sensor();
            var value, i = 0, length = elems.length, isArray = isArraylike(elems), ret = [];
            if (isArray) {
                for (; i < length; i++) {
                    value = callback(elems[i], i, arg);
                    if (value != null) {
                        ret.push(value);
                    }
                }
            } else {
                for (i in elems) {
                    value = callback(elems[i], i, arg);
                    if (value != null) {
                        ret.push(value);
                    }
                }
            }
            var IARETREPLACE = concat.apply([], ret);
            Sensor();
            return IARETREPLACE;
        },
        guid: 1,
        proxy: function (fn, context) {
            Sensor();
            var args, proxy, tmp;
            if (typeof context === 'string') {
                tmp = fn[context];
                context = fn;
                fn = tmp;
            }
            if (!jQuery.isFunction(fn)) {
                var IARETREPLACE = undefined;
                Sensor();
                return IARETREPLACE;
            }
            args = slice.call(arguments, 2);
            proxy = function () {
                var IARETREPLACE = fn.apply(context || this, args.concat(slice.call(arguments)));
                Sensor();
                return IARETREPLACE;
            };
            proxy.guid = fn.guid = fn.guid || jQuery.guid++;
            var IARETREPLACE = proxy;
            Sensor();
            return IARETREPLACE;
        },
        now: function () {
            var IARETREPLACE = +new Date();
            Sensor();
            return IARETREPLACE;
        },
        support: support
    });
    jQuery.each('Boolean Number String Function Array Date RegExp Object Error'.split(' '), function (i, name) {
        Sensor();
        class2type['[object ' + name + ']'] = name.toLowerCase();
        Sensor();
    });
    function isArraylike(obj) {
        Sensor();
        var length = obj.length, type = jQuery.type(obj);
        if (type === 'function' || jQuery.isWindow(obj)) {
            var IARETREPLACE = false;
            Sensor();
            return IARETREPLACE;
        }
        if (obj.nodeType === 1 && length) {
            var IARETREPLACE = true;
            Sensor();
            return IARETREPLACE;
        }
        var IARETREPLACE = type === 'array' || length === 0 || typeof length === 'number' && length > 0 && length - 1 in obj;
        Sensor();
        return IARETREPLACE;
    }
    var Sizzle = function (window) {
        Sensor();
        var i, support, Expr, getText, isXML, compile, outermostContext, sortInput, hasDuplicate, setDocument, document, docElem, documentIsHTML, rbuggyQSA, rbuggyMatches, matches, contains, expando = 'sizzle' + -new Date(), preferredDoc = window.document, dirruns = 0, done = 0, classCache = createCache(), tokenCache = createCache(), compilerCache = createCache(), sortOrder = function (a, b) {
                Sensor();
                if (a === b) {
                    hasDuplicate = true;
                }
                var IARETREPLACE = 0;
                Sensor();
                return IARETREPLACE;
            }, strundefined = typeof undefined, MAX_NEGATIVE = 1 << 31, hasOwn = {}.hasOwnProperty, arr = [], pop = arr.pop, push_native = arr.push, push = arr.push, slice = arr.slice, indexOf = arr.indexOf || function (elem) {
                Sensor();
                var i = 0, len = this.length;
                for (; i < len; i++) {
                    if (this[i] === elem) {
                        var IARETREPLACE = i;
                        Sensor();
                        return IARETREPLACE;
                    }
                }
                var IARETREPLACE = -1;
                Sensor();
                return IARETREPLACE;
            }, booleans = 'checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped', whitespace = '[\\x20\\t\\r\\n\\f]', characterEncoding = '(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+', identifier = characterEncoding.replace('w', 'w#'), attributes = '\\[' + whitespace + '*(' + characterEncoding + ')' + whitespace + '*(?:([*^$|!~]?=)' + whitespace + '*(?:([\'"])((?:\\\\.|[^\\\\])*?)\\3|(' + identifier + ')|)|)' + whitespace + '*\\]', pseudos = ':(' + characterEncoding + ')(?:\\((([\'"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|' + attributes.replace(3, 8) + ')*)|.*)\\)|)', rtrim = new RegExp('^' + whitespace + '+|((?:^|[^\\\\])(?:\\\\.)*)' + whitespace + '+$', 'g'), rcomma = new RegExp('^' + whitespace + '*,' + whitespace + '*'), rcombinators = new RegExp('^' + whitespace + '*([>+~]|' + whitespace + ')' + whitespace + '*'), rattributeQuotes = new RegExp('=' + whitespace + '*([^\\]\'"]*?)' + whitespace + '*\\]', 'g'), rpseudo = new RegExp(pseudos), ridentifier = new RegExp('^' + identifier + '$'), matchExpr = {
                'ID': new RegExp('^#(' + characterEncoding + ')'),
                'CLASS': new RegExp('^\\.(' + characterEncoding + ')'),
                'TAG': new RegExp('^(' + characterEncoding.replace('w', 'w*') + ')'),
                'ATTR': new RegExp('^' + attributes),
                'PSEUDO': new RegExp('^' + pseudos),
                'CHILD': new RegExp('^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(' + whitespace + '*(even|odd|(([+-]|)(\\d*)n|)' + whitespace + '*(?:([+-]|)' + whitespace + '*(\\d+)|))' + whitespace + '*\\)|)', 'i'),
                'bool': new RegExp('^(?:' + booleans + ')$', 'i'),
                'needsContext': new RegExp('^' + whitespace + '*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(' + whitespace + '*((?:-\\d)?\\d*)' + whitespace + '*\\)|)(?=[^-]|$)', 'i')
            }, rinputs = /^(?:input|select|textarea|button)$/i, rheader = /^h\d$/i, rnative = /^[^{]+\{\s*\[native \w/, rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, rsibling = /[+~]/, rescape = /'|\\/g, runescape = new RegExp('\\\\([\\da-f]{1,6}' + whitespace + '?|(' + whitespace + ')|.)', 'ig'), funescape = function (_, escaped, escapedWhitespace) {
                Sensor();
                var high = '0x' + escaped - 65536;
                var IARETREPLACE = high !== high || escapedWhitespace ? escaped : high < 0 ? String.fromCharCode(high + 65536) : String.fromCharCode(high >> 10 | 55296, high & 1023 | 56320);
                Sensor();
                return IARETREPLACE;
            };
        try {
            push.apply(arr = slice.call(preferredDoc.childNodes), preferredDoc.childNodes);
            arr[preferredDoc.childNodes.length].nodeType;
        } catch (e) {
            push = {
                apply: arr.length ? function (target, els) {
                    Sensor();
                    push_native.apply(target, slice.call(els));
                    Sensor();
                } : function (target, els) {
                    Sensor();
                    var j = target.length, i = 0;
                    while (target[j++] = els[i++]) {
                    }
                    target.length = j - 1;
                    Sensor();
                }
            };
        }
        function Sizzle(selector, context, results, seed) {
            Sensor();
            var match, elem, m, nodeType, i, groups, old, nid, newContext, newSelector;
            if ((context ? context.ownerDocument || context : preferredDoc) !== document) {
                setDocument(context);
            }
            context = context || document;
            results = results || [];
            if (!selector || typeof selector !== 'string') {
                var IARETREPLACE = results;
                Sensor();
                return IARETREPLACE;
            }
            if ((nodeType = context.nodeType) !== 1 && nodeType !== 9) {
                var IARETREPLACE = [];
                Sensor();
                return IARETREPLACE;
            }
            if (documentIsHTML && !seed) {
                if (match = rquickExpr.exec(selector)) {
                    if (m = match[1]) {
                        if (nodeType === 9) {
                            elem = context.getElementById(m);
                            if (elem && elem.parentNode) {
                                if (elem.id === m) {
                                    results.push(elem);
                                    var IARETREPLACE = results;
                                    Sensor();
                                    return IARETREPLACE;
                                }
                            } else {
                                var IARETREPLACE = results;
                                Sensor();
                                return IARETREPLACE;
                            }
                        } else {
                            if (context.ownerDocument && (elem = context.ownerDocument.getElementById(m)) && contains(context, elem) && elem.id === m) {
                                results.push(elem);
                                var IARETREPLACE = results;
                                Sensor();
                                return IARETREPLACE;
                            }
                        }
                    } else {
                        if (match[2]) {
                            push.apply(results, context.getElementsByTagName(selector));
                            var IARETREPLACE = results;
                            Sensor();
                            return IARETREPLACE;
                        } else {
                            if ((m = match[3]) && support.getElementsByClassName && context.getElementsByClassName) {
                                push.apply(results, context.getElementsByClassName(m));
                                var IARETREPLACE = results;
                                Sensor();
                                return IARETREPLACE;
                            }
                        }
                    }
                }
                if (support.qsa && (!rbuggyQSA || !rbuggyQSA.test(selector))) {
                    nid = old = expando;
                    newContext = context;
                    newSelector = nodeType === 9 && selector;
                    if (nodeType === 1 && context.nodeName.toLowerCase() !== 'object') {
                        groups = tokenize(selector);
                        if (old = context.getAttribute('id')) {
                            nid = old.replace(rescape, '\\$&');
                        } else {
                            context.setAttribute('id', nid);
                        }
                        nid = '[id=\'' + nid + '\'] ';
                        i = groups.length;
                        while (i--) {
                            groups[i] = nid + toSelector(groups[i]);
                        }
                        newContext = rsibling.test(selector) && testContext(context.parentNode) || context;
                        newSelector = groups.join(',');
                    }
                    if (newSelector) {
                        try {
                            push.apply(results, newContext.querySelectorAll(newSelector));
                            var IARETREPLACE = results;
                            Sensor();
                            return IARETREPLACE;
                        } catch (qsaError) {
                        } finally {
                            if (!old) {
                                context.removeAttribute('id');
                            }
                        }
                    }
                }
            }
            var IARETREPLACE = select(selector.replace(rtrim, '$1'), context, results, seed);
            Sensor();
            return IARETREPLACE;
        }
        function createCache() {
            Sensor();
            var keys = [];
            function cache(key, value) {
                Sensor();
                if (keys.push(key + ' ') > Expr.cacheLength) {
                    delete cache[keys.shift()];
                }
                var IARETREPLACE = cache[key + ' '] = value;
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = cache;
            Sensor();
            return IARETREPLACE;
        }
        function markFunction(fn) {
            Sensor();
            fn[expando] = true;
            var IARETREPLACE = fn;
            Sensor();
            return IARETREPLACE;
        }
        function assert(fn) {
            Sensor();
            var div = document.createElement('div');
            try {
                var IARETREPLACE = !!fn(div);
                Sensor();
                return IARETREPLACE;
            } catch (e) {
                var IARETREPLACE = false;
                Sensor();
                return IARETREPLACE;
            } finally {
                if (div.parentNode) {
                    div.parentNode.removeChild(div);
                }
                div = null;
            }
            Sensor();
        }
        function addHandle(attrs, handler) {
            Sensor();
            var arr = attrs.split('|'), i = attrs.length;
            while (i--) {
                Expr.attrHandle[arr[i]] = handler;
            }
            Sensor();
        }
        function siblingCheck(a, b) {
            Sensor();
            var cur = b && a, diff = cur && a.nodeType === 1 && b.nodeType === 1 && (~b.sourceIndex || MAX_NEGATIVE) - (~a.sourceIndex || MAX_NEGATIVE);
            if (diff) {
                var IARETREPLACE = diff;
                Sensor();
                return IARETREPLACE;
            }
            if (cur) {
                while (cur = cur.nextSibling) {
                    if (cur === b) {
                        var IARETREPLACE = -1;
                        Sensor();
                        return IARETREPLACE;
                    }
                }
            }
            var IARETREPLACE = a ? 1 : -1;
            Sensor();
            return IARETREPLACE;
        }
        function createInputPseudo(type) {
            var IARETREPLACE = function (elem) {
                Sensor();
                var name = elem.nodeName.toLowerCase();
                var IARETREPLACE = name === 'input' && elem.type === type;
                Sensor();
                return IARETREPLACE;
            };
            Sensor();
            return IARETREPLACE;
        }
        function createButtonPseudo(type) {
            var IARETREPLACE = function (elem) {
                Sensor();
                var name = elem.nodeName.toLowerCase();
                var IARETREPLACE = (name === 'input' || name === 'button') && elem.type === type;
                Sensor();
                return IARETREPLACE;
            };
            Sensor();
            return IARETREPLACE;
        }
        function createPositionalPseudo(fn) {
            var IARETREPLACE = markFunction(function (argument) {
                Sensor();
                argument = +argument;
                var IARETREPLACE = markFunction(function (seed, matches) {
                    Sensor();
                    var j, matchIndexes = fn([], seed.length, argument), i = matchIndexes.length;
                    while (i--) {
                        if (seed[j = matchIndexes[i]]) {
                            seed[j] = !(matches[j] = seed[j]);
                        }
                    }
                    Sensor();
                });
                Sensor();
                return IARETREPLACE;
            });
            Sensor();
            return IARETREPLACE;
        }
        function testContext(context) {
            var IARETREPLACE = context && typeof context.getElementsByTagName !== strundefined && context;
            Sensor();
            return IARETREPLACE;
        }
        support = Sizzle.support = {};
        isXML = Sizzle.isXML = function (elem) {
            Sensor();
            var documentElement = elem && (elem.ownerDocument || elem).documentElement;
            var IARETREPLACE = documentElement ? documentElement.nodeName !== 'HTML' : false;
            Sensor();
            return IARETREPLACE;
        };
        setDocument = Sizzle.setDocument = function (node) {
            Sensor();
            var hasCompare, doc = node ? node.ownerDocument || node : preferredDoc, parent = doc.defaultView;
            if (doc === document || doc.nodeType !== 9 || !doc.documentElement) {
                var IARETREPLACE = document;
                Sensor();
                return IARETREPLACE;
            }
            document = doc;
            docElem = doc.documentElement;
            documentIsHTML = !isXML(doc);
            if (parent && parent !== parent.top) {
                if (parent.addEventListener) {
                    parent.addEventListener('unload', function () {
                        Sensor();
                        setDocument();
                        Sensor();
                    }, false);
                } else {
                    if (parent.attachEvent) {
                        parent.attachEvent('onunload', function () {
                            Sensor();
                            setDocument();
                            Sensor();
                        });
                    }
                }
            }
            support.attributes = assert(function (div) {
                Sensor();
                div.className = 'i';
                var IARETREPLACE = !div.getAttribute('className');
                Sensor();
                return IARETREPLACE;
            });
            support.getElementsByTagName = assert(function (div) {
                Sensor();
                div.appendChild(doc.createComment(''));
                var IARETREPLACE = !div.getElementsByTagName('*').length;
                Sensor();
                return IARETREPLACE;
            });
            support.getElementsByClassName = rnative.test(doc.getElementsByClassName) && assert(function (div) {
                Sensor();
                div.innerHTML = '<div class=\'a\'></div><div class=\'a i\'></div>';
                div.firstChild.className = 'i';
                var IARETREPLACE = div.getElementsByClassName('i').length === 2;
                Sensor();
                return IARETREPLACE;
            });
            support.getById = assert(function (div) {
                Sensor();
                docElem.appendChild(div).id = expando;
                var IARETREPLACE = !doc.getElementsByName || !doc.getElementsByName(expando).length;
                Sensor();
                return IARETREPLACE;
            });
            if (support.getById) {
                Expr.find['ID'] = function (id, context) {
                    Sensor();
                    if (typeof context.getElementById !== strundefined && documentIsHTML) {
                        var m = context.getElementById(id);
                        var IARETREPLACE = m && m.parentNode ? [m] : [];
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                };
                Expr.filter['ID'] = function (id) {
                    Sensor();
                    var attrId = id.replace(runescape, funescape);
                    var IARETREPLACE = function (elem) {
                        var IARETREPLACE = elem.getAttribute('id') === attrId;
                        Sensor();
                        return IARETREPLACE;
                    };
                    Sensor();
                    return IARETREPLACE;
                };
            } else {
                delete Expr.find['ID'];
                Expr.filter['ID'] = function (id) {
                    Sensor();
                    var attrId = id.replace(runescape, funescape);
                    var IARETREPLACE = function (elem) {
                        Sensor();
                        var node = typeof elem.getAttributeNode !== strundefined && elem.getAttributeNode('id');
                        var IARETREPLACE = node && node.value === attrId;
                        Sensor();
                        return IARETREPLACE;
                    };
                    Sensor();
                    return IARETREPLACE;
                };
            }
            Expr.find['TAG'] = support.getElementsByTagName ? function (tag, context) {
                Sensor();
                if (typeof context.getElementsByTagName !== strundefined) {
                    var IARETREPLACE = context.getElementsByTagName(tag);
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            } : function (tag, context) {
                Sensor();
                var elem, tmp = [], i = 0, results = context.getElementsByTagName(tag);
                if (tag === '*') {
                    while (elem = results[i++]) {
                        if (elem.nodeType === 1) {
                            tmp.push(elem);
                        }
                    }
                    var IARETREPLACE = tmp;
                    Sensor();
                    return IARETREPLACE;
                }
                var IARETREPLACE = results;
                Sensor();
                return IARETREPLACE;
            };
            Expr.find['CLASS'] = support.getElementsByClassName && function (className, context) {
                Sensor();
                if (typeof context.getElementsByClassName !== strundefined && documentIsHTML) {
                    var IARETREPLACE = context.getElementsByClassName(className);
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            };
            rbuggyMatches = [];
            rbuggyQSA = [];
            if (support.qsa = rnative.test(doc.querySelectorAll)) {
                assert(function (div) {
                    Sensor();
                    div.innerHTML = '<select t=\'\'><option selected=\'\'></option></select>';
                    if (div.querySelectorAll('[t^=\'\']').length) {
                        rbuggyQSA.push('[*^$]=' + whitespace + '*(?:\'\'|"")');
                    }
                    if (!div.querySelectorAll('[selected]').length) {
                        rbuggyQSA.push('\\[' + whitespace + '*(?:value|' + booleans + ')');
                    }
                    if (!div.querySelectorAll(':checked').length) {
                        rbuggyQSA.push(':checked');
                    }
                    Sensor();
                });
                assert(function (div) {
                    Sensor();
                    var input = doc.createElement('input');
                    input.setAttribute('type', 'hidden');
                    div.appendChild(input).setAttribute('name', 'D');
                    if (div.querySelectorAll('[name=d]').length) {
                        rbuggyQSA.push('name' + whitespace + '*[*^$|!~]?=');
                    }
                    if (!div.querySelectorAll(':enabled').length) {
                        rbuggyQSA.push(':enabled', ':disabled');
                    }
                    div.querySelectorAll('*,:x');
                    rbuggyQSA.push(',.*:');
                    Sensor();
                });
            }
            if (support.matchesSelector = rnative.test(matches = docElem.webkitMatchesSelector || docElem.mozMatchesSelector || docElem.oMatchesSelector || docElem.msMatchesSelector)) {
                assert(function (div) {
                    Sensor();
                    support.disconnectedMatch = matches.call(div, 'div');
                    matches.call(div, '[s!=\'\']:x');
                    rbuggyMatches.push('!=', pseudos);
                    Sensor();
                });
            }
            rbuggyQSA = rbuggyQSA.length && new RegExp(rbuggyQSA.join('|'));
            rbuggyMatches = rbuggyMatches.length && new RegExp(rbuggyMatches.join('|'));
            hasCompare = rnative.test(docElem.compareDocumentPosition);
            contains = hasCompare || rnative.test(docElem.contains) ? function (a, b) {
                Sensor();
                var adown = a.nodeType === 9 ? a.documentElement : a, bup = b && b.parentNode;
                var IARETREPLACE = a === bup || !!(bup && bup.nodeType === 1 && (adown.contains ? adown.contains(bup) : a.compareDocumentPosition && a.compareDocumentPosition(bup) & 16));
                Sensor();
                return IARETREPLACE;
            } : function (a, b) {
                Sensor();
                if (b) {
                    while (b = b.parentNode) {
                        if (b === a) {
                            var IARETREPLACE = true;
                            Sensor();
                            return IARETREPLACE;
                        }
                    }
                }
                var IARETREPLACE = false;
                Sensor();
                return IARETREPLACE;
            };
            sortOrder = hasCompare ? function (a, b) {
                Sensor();
                if (a === b) {
                    hasDuplicate = true;
                    var IARETREPLACE = 0;
                    Sensor();
                    return IARETREPLACE;
                }
                var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
                if (compare) {
                    var IARETREPLACE = compare;
                    Sensor();
                    return IARETREPLACE;
                }
                compare = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1;
                if (compare & 1 || !support.sortDetached && b.compareDocumentPosition(a) === compare) {
                    if (a === doc || a.ownerDocument === preferredDoc && contains(preferredDoc, a)) {
                        var IARETREPLACE = -1;
                        Sensor();
                        return IARETREPLACE;
                    }
                    if (b === doc || b.ownerDocument === preferredDoc && contains(preferredDoc, b)) {
                        var IARETREPLACE = 1;
                        Sensor();
                        return IARETREPLACE;
                    }
                    var IARETREPLACE = sortInput ? indexOf.call(sortInput, a) - indexOf.call(sortInput, b) : 0;
                    Sensor();
                    return IARETREPLACE;
                }
                var IARETREPLACE = compare & 4 ? -1 : 1;
                Sensor();
                return IARETREPLACE;
            } : function (a, b) {
                Sensor();
                if (a === b) {
                    hasDuplicate = true;
                    var IARETREPLACE = 0;
                    Sensor();
                    return IARETREPLACE;
                }
                var cur, i = 0, aup = a.parentNode, bup = b.parentNode, ap = [a], bp = [b];
                if (!aup || !bup) {
                    var IARETREPLACE = a === doc ? -1 : b === doc ? 1 : aup ? -1 : bup ? 1 : sortInput ? indexOf.call(sortInput, a) - indexOf.call(sortInput, b) : 0;
                    Sensor();
                    return IARETREPLACE;
                } else {
                    if (aup === bup) {
                        var IARETREPLACE = siblingCheck(a, b);
                        Sensor();
                        return IARETREPLACE;
                    }
                }
                cur = a;
                while (cur = cur.parentNode) {
                    ap.unshift(cur);
                }
                cur = b;
                while (cur = cur.parentNode) {
                    bp.unshift(cur);
                }
                while (ap[i] === bp[i]) {
                    i++;
                }
                var IARETREPLACE = i ? siblingCheck(ap[i], bp[i]) : ap[i] === preferredDoc ? -1 : bp[i] === preferredDoc ? 1 : 0;
                Sensor();
                return IARETREPLACE;
            };
            var IARETREPLACE = doc;
            Sensor();
            return IARETREPLACE;
        };
        Sizzle.matches = function (expr, elements) {
            var IARETREPLACE = Sizzle(expr, null, null, elements);
            Sensor();
            return IARETREPLACE;
        };
        Sizzle.matchesSelector = function (elem, expr) {
            Sensor();
            if ((elem.ownerDocument || elem) !== document) {
                setDocument(elem);
            }
            expr = expr.replace(rattributeQuotes, '=\'$1\']');
            if (support.matchesSelector && documentIsHTML && (!rbuggyMatches || !rbuggyMatches.test(expr)) && (!rbuggyQSA || !rbuggyQSA.test(expr))) {
                try {
                    var ret = matches.call(elem, expr);
                    if (ret || support.disconnectedMatch || elem.document && elem.document.nodeType !== 11) {
                        var IARETREPLACE = ret;
                        Sensor();
                        return IARETREPLACE;
                    }
                } catch (e) {
                }
            }
            var IARETREPLACE = Sizzle(expr, document, null, [elem]).length > 0;
            Sensor();
            return IARETREPLACE;
        };
        Sizzle.contains = function (context, elem) {
            Sensor();
            if ((context.ownerDocument || context) !== document) {
                setDocument(context);
            }
            var IARETREPLACE = contains(context, elem);
            Sensor();
            return IARETREPLACE;
        };
        Sizzle.attr = function (elem, name) {
            Sensor();
            if ((elem.ownerDocument || elem) !== document) {
                setDocument(elem);
            }
            var fn = Expr.attrHandle[name.toLowerCase()], val = fn && hasOwn.call(Expr.attrHandle, name.toLowerCase()) ? fn(elem, name, !documentIsHTML) : undefined;
            var IARETREPLACE = val !== undefined ? val : support.attributes || !documentIsHTML ? elem.getAttribute(name) : (val = elem.getAttributeNode(name)) && val.specified ? val.value : null;
            Sensor();
            return IARETREPLACE;
        };
        Sizzle.error = function (msg) {
            Sensor();
            throw new Error('Syntax error, unrecognized expression: ' + msg);
        };
        Sizzle.uniqueSort = function (results) {
            Sensor();
            var elem, duplicates = [], j = 0, i = 0;
            hasDuplicate = !support.detectDuplicates;
            sortInput = !support.sortStable && results.slice(0);
            results.sort(sortOrder);
            if (hasDuplicate) {
                while (elem = results[i++]) {
                    if (elem === results[i]) {
                        j = duplicates.push(i);
                    }
                }
                while (j--) {
                    results.splice(duplicates[j], 1);
                }
            }
            sortInput = null;
            var IARETREPLACE = results;
            Sensor();
            return IARETREPLACE;
        };
        getText = Sizzle.getText = function (elem) {
            Sensor();
            var node, ret = '', i = 0, nodeType = elem.nodeType;
            if (!nodeType) {
                while (node = elem[i++]) {
                    ret += getText(node);
                }
            } else {
                if (nodeType === 1 || nodeType === 9 || nodeType === 11) {
                    if (typeof elem.textContent === 'string') {
                        var IARETREPLACE = elem.textContent;
                        Sensor();
                        return IARETREPLACE;
                    } else {
                        for (elem = elem.firstChild; elem; elem = elem.nextSibling) {
                            ret += getText(elem);
                        }
                    }
                } else {
                    if (nodeType === 3 || nodeType === 4) {
                        var IARETREPLACE = elem.nodeValue;
                        Sensor();
                        return IARETREPLACE;
                    }
                }
            }
            var IARETREPLACE = ret;
            Sensor();
            return IARETREPLACE;
        };
        Expr = Sizzle.selectors = {
            cacheLength: 50,
            createPseudo: markFunction,
            match: matchExpr,
            attrHandle: {},
            find: {},
            relative: {
                '>': {
                    dir: 'parentNode',
                    first: true
                },
                ' ': { dir: 'parentNode' },
                '+': {
                    dir: 'previousSibling',
                    first: true
                },
                '~': { dir: 'previousSibling' }
            },
            preFilter: {
                'ATTR': function (match) {
                    Sensor();
                    match[1] = match[1].replace(runescape, funescape);
                    match[3] = (match[4] || match[5] || '').replace(runescape, funescape);
                    if (match[2] === '~=') {
                        match[3] = ' ' + match[3] + ' ';
                    }
                    var IARETREPLACE = match.slice(0, 4);
                    Sensor();
                    return IARETREPLACE;
                },
                'CHILD': function (match) {
                    Sensor();
                    match[1] = match[1].toLowerCase();
                    if (match[1].slice(0, 3) === 'nth') {
                        if (!match[3]) {
                            Sizzle.error(match[0]);
                        }
                        match[4] = +(match[4] ? match[5] + (match[6] || 1) : 2 * (match[3] === 'even' || match[3] === 'odd'));
                        match[5] = +(match[7] + match[8] || match[3] === 'odd');
                    } else {
                        if (match[3]) {
                            Sizzle.error(match[0]);
                        }
                    }
                    var IARETREPLACE = match;
                    Sensor();
                    return IARETREPLACE;
                },
                'PSEUDO': function (match) {
                    Sensor();
                    var excess, unquoted = !match[5] && match[2];
                    if (matchExpr['CHILD'].test(match[0])) {
                        var IARETREPLACE = null;
                        Sensor();
                        return IARETREPLACE;
                    }
                    if (match[3] && match[4] !== undefined) {
                        match[2] = match[4];
                    } else {
                        if (unquoted && rpseudo.test(unquoted) && (excess = tokenize(unquoted, true)) && (excess = unquoted.indexOf(')', unquoted.length - excess) - unquoted.length)) {
                            match[0] = match[0].slice(0, excess);
                            match[2] = unquoted.slice(0, excess);
                        }
                    }
                    var IARETREPLACE = match.slice(0, 3);
                    Sensor();
                    return IARETREPLACE;
                }
            },
            filter: {
                'TAG': function (nodeNameSelector) {
                    Sensor();
                    var nodeName = nodeNameSelector.replace(runescape, funescape).toLowerCase();
                    var IARETREPLACE = nodeNameSelector === '*' ? function () {
                        var IARETREPLACE = true;
                        Sensor();
                        return IARETREPLACE;
                    } : function (elem) {
                        var IARETREPLACE = elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
                        Sensor();
                        return IARETREPLACE;
                    };
                    Sensor();
                    return IARETREPLACE;
                },
                'CLASS': function (className) {
                    Sensor();
                    var pattern = classCache[className + ' '];
                    var IARETREPLACE = pattern || (pattern = new RegExp('(^|' + whitespace + ')' + className + '(' + whitespace + '|$)')) && classCache(className, function (elem) {
                        var IARETREPLACE = pattern.test(typeof elem.className === 'string' && elem.className || typeof elem.getAttribute !== strundefined && elem.getAttribute('class') || '');
                        Sensor();
                        return IARETREPLACE;
                    });
                    Sensor();
                    return IARETREPLACE;
                },
                'ATTR': function (name, operator, check) {
                    var IARETREPLACE = function (elem) {
                        Sensor();
                        var result = Sizzle.attr(elem, name);
                        if (result == null) {
                            var IARETREPLACE = operator === '!=';
                            Sensor();
                            return IARETREPLACE;
                        }
                        if (!operator) {
                            var IARETREPLACE = true;
                            Sensor();
                            return IARETREPLACE;
                        }
                        result += '';
                        var IARETREPLACE = operator === '=' ? result === check : operator === '!=' ? result !== check : operator === '^=' ? check && result.indexOf(check) === 0 : operator === '*=' ? check && result.indexOf(check) > -1 : operator === '$=' ? check && result.slice(-check.length) === check : operator === '~=' ? (' ' + result + ' ').indexOf(check) > -1 : operator === '|=' ? result === check || result.slice(0, check.length + 1) === check + '-' : false;
                        Sensor();
                        return IARETREPLACE;
                    };
                    Sensor();
                    return IARETREPLACE;
                },
                'CHILD': function (type, what, argument, first, last) {
                    Sensor();
                    var simple = type.slice(0, 3) !== 'nth', forward = type.slice(-4) !== 'last', ofType = what === 'of-type';
                    var IARETREPLACE = first === 1 && last === 0 ? function (elem) {
                        var IARETREPLACE = !!elem.parentNode;
                        Sensor();
                        return IARETREPLACE;
                    } : function (elem, context, xml) {
                        Sensor();
                        var cache, outerCache, node, diff, nodeIndex, start, dir = simple !== forward ? 'nextSibling' : 'previousSibling', parent = elem.parentNode, name = ofType && elem.nodeName.toLowerCase(), useCache = !xml && !ofType;
                        if (parent) {
                            if (simple) {
                                while (dir) {
                                    node = elem;
                                    while (node = node[dir]) {
                                        if (ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1) {
                                            var IARETREPLACE = false;
                                            Sensor();
                                            return IARETREPLACE;
                                        }
                                    }
                                    start = dir = type === 'only' && !start && 'nextSibling';
                                }
                                var IARETREPLACE = true;
                                Sensor();
                                return IARETREPLACE;
                            }
                            start = [forward ? parent.firstChild : parent.lastChild];
                            if (forward && useCache) {
                                outerCache = parent[expando] || (parent[expando] = {});
                                cache = outerCache[type] || [];
                                nodeIndex = cache[0] === dirruns && cache[1];
                                diff = cache[0] === dirruns && cache[2];
                                node = nodeIndex && parent.childNodes[nodeIndex];
                                while (node = ++nodeIndex && node && node[dir] || (diff = nodeIndex = 0) || start.pop()) {
                                    if (node.nodeType === 1 && ++diff && node === elem) {
                                        outerCache[type] = [
                                            dirruns,
                                            nodeIndex,
                                            diff
                                        ];
                                        break;
                                    }
                                }
                            } else {
                                if (useCache && (cache = (elem[expando] || (elem[expando] = {}))[type]) && cache[0] === dirruns) {
                                    diff = cache[1];
                                } else {
                                    while (node = ++nodeIndex && node && node[dir] || (diff = nodeIndex = 0) || start.pop()) {
                                        if ((ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1) && ++diff) {
                                            if (useCache) {
                                                (node[expando] || (node[expando] = {}))[type] = [
                                                    dirruns,
                                                    diff
                                                ];
                                            }
                                            if (node === elem) {
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            diff -= last;
                            var IARETREPLACE = diff === first || diff % first === 0 && diff / first >= 0;
                            Sensor();
                            return IARETREPLACE;
                        }
                        Sensor();
                    };
                    Sensor();
                    return IARETREPLACE;
                },
                'PSEUDO': function (pseudo, argument) {
                    Sensor();
                    var args, fn = Expr.pseudos[pseudo] || Expr.setFilters[pseudo.toLowerCase()] || Sizzle.error('unsupported pseudo: ' + pseudo);
                    if (fn[expando]) {
                        var IARETREPLACE = fn(argument);
                        Sensor();
                        return IARETREPLACE;
                    }
                    if (fn.length > 1) {
                        args = [
                            pseudo,
                            pseudo,
                            '',
                            argument
                        ];
                        var IARETREPLACE = Expr.setFilters.hasOwnProperty(pseudo.toLowerCase()) ? markFunction(function (seed, matches) {
                            Sensor();
                            var idx, matched = fn(seed, argument), i = matched.length;
                            while (i--) {
                                idx = indexOf.call(seed, matched[i]);
                                seed[idx] = !(matches[idx] = matched[i]);
                            }
                            Sensor();
                        }) : function (elem) {
                            var IARETREPLACE = fn(elem, 0, args);
                            Sensor();
                            return IARETREPLACE;
                        };
                        Sensor();
                        return IARETREPLACE;
                    }
                    var IARETREPLACE = fn;
                    Sensor();
                    return IARETREPLACE;
                }
            },
            pseudos: {
                'not': markFunction(function (selector) {
                    Sensor();
                    var input = [], results = [], matcher = compile(selector.replace(rtrim, '$1'));
                    var IARETREPLACE = matcher[expando] ? markFunction(function (seed, matches, context, xml) {
                        Sensor();
                        var elem, unmatched = matcher(seed, null, xml, []), i = seed.length;
                        while (i--) {
                            if (elem = unmatched[i]) {
                                seed[i] = !(matches[i] = elem);
                            }
                        }
                        Sensor();
                    }) : function (elem, context, xml) {
                        Sensor();
                        input[0] = elem;
                        matcher(input, null, xml, results);
                        var IARETREPLACE = !results.pop();
                        Sensor();
                        return IARETREPLACE;
                    };
                    Sensor();
                    return IARETREPLACE;
                }),
                'has': markFunction(function (selector) {
                    var IARETREPLACE = function (elem) {
                        var IARETREPLACE = Sizzle(selector, elem).length > 0;
                        Sensor();
                        return IARETREPLACE;
                    };
                    Sensor();
                    return IARETREPLACE;
                }),
                'contains': markFunction(function (text) {
                    var IARETREPLACE = function (elem) {
                        var IARETREPLACE = (elem.textContent || elem.innerText || getText(elem)).indexOf(text) > -1;
                        Sensor();
                        return IARETREPLACE;
                    };
                    Sensor();
                    return IARETREPLACE;
                }),
                'lang': markFunction(function (lang) {
                    Sensor();
                    if (!ridentifier.test(lang || '')) {
                        Sizzle.error('unsupported lang: ' + lang);
                    }
                    lang = lang.replace(runescape, funescape).toLowerCase();
                    var IARETREPLACE = function (elem) {
                        Sensor();
                        var elemLang;
                        do {
                            if (elemLang = documentIsHTML ? elem.lang : elem.getAttribute('xml:lang') || elem.getAttribute('lang')) {
                                elemLang = elemLang.toLowerCase();
                                var IARETREPLACE = elemLang === lang || elemLang.indexOf(lang + '-') === 0;
                                Sensor();
                                return IARETREPLACE;
                            }
                        } while ((elem = elem.parentNode) && elem.nodeType === 1);
                        var IARETREPLACE = false;
                        Sensor();
                        return IARETREPLACE;
                    };
                    Sensor();
                    return IARETREPLACE;
                }),
                'target': function (elem) {
                    Sensor();
                    var hash = window.location && window.location.hash;
                    var IARETREPLACE = hash && hash.slice(1) === elem.id;
                    Sensor();
                    return IARETREPLACE;
                },
                'root': function (elem) {
                    var IARETREPLACE = elem === docElem;
                    Sensor();
                    return IARETREPLACE;
                },
                'focus': function (elem) {
                    var IARETREPLACE = elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
                    Sensor();
                    return IARETREPLACE;
                },
                'enabled': function (elem) {
                    var IARETREPLACE = elem.disabled === false;
                    Sensor();
                    return IARETREPLACE;
                },
                'disabled': function (elem) {
                    var IARETREPLACE = elem.disabled === true;
                    Sensor();
                    return IARETREPLACE;
                },
                'checked': function (elem) {
                    Sensor();
                    var nodeName = elem.nodeName.toLowerCase();
                    var IARETREPLACE = nodeName === 'input' && !!elem.checked || nodeName === 'option' && !!elem.selected;
                    Sensor();
                    return IARETREPLACE;
                },
                'selected': function (elem) {
                    Sensor();
                    if (elem.parentNode) {
                        elem.parentNode.selectedIndex;
                    }
                    var IARETREPLACE = elem.selected === true;
                    Sensor();
                    return IARETREPLACE;
                },
                'empty': function (elem) {
                    Sensor();
                    for (elem = elem.firstChild; elem; elem = elem.nextSibling) {
                        if (elem.nodeType < 6) {
                            var IARETREPLACE = false;
                            Sensor();
                            return IARETREPLACE;
                        }
                    }
                    var IARETREPLACE = true;
                    Sensor();
                    return IARETREPLACE;
                },
                'parent': function (elem) {
                    var IARETREPLACE = !Expr.pseudos['empty'](elem);
                    Sensor();
                    return IARETREPLACE;
                },
                'header': function (elem) {
                    var IARETREPLACE = rheader.test(elem.nodeName);
                    Sensor();
                    return IARETREPLACE;
                },
                'input': function (elem) {
                    var IARETREPLACE = rinputs.test(elem.nodeName);
                    Sensor();
                    return IARETREPLACE;
                },
                'button': function (elem) {
                    Sensor();
                    var name = elem.nodeName.toLowerCase();
                    var IARETREPLACE = name === 'input' && elem.type === 'button' || name === 'button';
                    Sensor();
                    return IARETREPLACE;
                },
                'text': function (elem) {
                    Sensor();
                    var attr;
                    var IARETREPLACE = elem.nodeName.toLowerCase() === 'input' && elem.type === 'text' && ((attr = elem.getAttribute('type')) == null || attr.toLowerCase() === 'text');
                    Sensor();
                    return IARETREPLACE;
                },
                'first': createPositionalPseudo(function () {
                    var IARETREPLACE = [0];
                    Sensor();
                    return IARETREPLACE;
                }),
                'last': createPositionalPseudo(function (matchIndexes, length) {
                    var IARETREPLACE = [length - 1];
                    Sensor();
                    return IARETREPLACE;
                }),
                'eq': createPositionalPseudo(function (matchIndexes, length, argument) {
                    var IARETREPLACE = [argument < 0 ? argument + length : argument];
                    Sensor();
                    return IARETREPLACE;
                }),
                'even': createPositionalPseudo(function (matchIndexes, length) {
                    Sensor();
                    var i = 0;
                    for (; i < length; i += 2) {
                        matchIndexes.push(i);
                    }
                    var IARETREPLACE = matchIndexes;
                    Sensor();
                    return IARETREPLACE;
                }),
                'odd': createPositionalPseudo(function (matchIndexes, length) {
                    Sensor();
                    var i = 1;
                    for (; i < length; i += 2) {
                        matchIndexes.push(i);
                    }
                    var IARETREPLACE = matchIndexes;
                    Sensor();
                    return IARETREPLACE;
                }),
                'lt': createPositionalPseudo(function (matchIndexes, length, argument) {
                    Sensor();
                    var i = argument < 0 ? argument + length : argument;
                    for (; --i >= 0;) {
                        matchIndexes.push(i);
                    }
                    var IARETREPLACE = matchIndexes;
                    Sensor();
                    return IARETREPLACE;
                }),
                'gt': createPositionalPseudo(function (matchIndexes, length, argument) {
                    Sensor();
                    var i = argument < 0 ? argument + length : argument;
                    for (; ++i < length;) {
                        matchIndexes.push(i);
                    }
                    var IARETREPLACE = matchIndexes;
                    Sensor();
                    return IARETREPLACE;
                })
            }
        };
        Expr.pseudos['nth'] = Expr.pseudos['eq'];
        for (i in {
                radio: true,
                checkbox: true,
                file: true,
                password: true,
                image: true
            }) {
            Expr.pseudos[i] = createInputPseudo(i);
        }
        for (i in {
                submit: true,
                reset: true
            }) {
            Expr.pseudos[i] = createButtonPseudo(i);
        }
        function setFilters() {
            Sensor();
        }
        setFilters.prototype = Expr.filters = Expr.pseudos;
        Expr.setFilters = new setFilters();
        function tokenize(selector, parseOnly) {
            Sensor();
            var matched, match, tokens, type, soFar, groups, preFilters, cached = tokenCache[selector + ' '];
            if (cached) {
                var IARETREPLACE = parseOnly ? 0 : cached.slice(0);
                Sensor();
                return IARETREPLACE;
            }
            soFar = selector;
            groups = [];
            preFilters = Expr.preFilter;
            while (soFar) {
                if (!matched || (match = rcomma.exec(soFar))) {
                    if (match) {
                        soFar = soFar.slice(match[0].length) || soFar;
                    }
                    groups.push(tokens = []);
                }
                matched = false;
                if (match = rcombinators.exec(soFar)) {
                    matched = match.shift();
                    tokens.push({
                        value: matched,
                        type: match[0].replace(rtrim, ' ')
                    });
                    soFar = soFar.slice(matched.length);
                }
                for (type in Expr.filter) {
                    if ((match = matchExpr[type].exec(soFar)) && (!preFilters[type] || (match = preFilters[type](match)))) {
                        matched = match.shift();
                        tokens.push({
                            value: matched,
                            type: type,
                            matches: match
                        });
                        soFar = soFar.slice(matched.length);
                    }
                }
                if (!matched) {
                    break;
                }
            }
            var IARETREPLACE = parseOnly ? soFar.length : soFar ? Sizzle.error(selector) : tokenCache(selector, groups).slice(0);
            Sensor();
            return IARETREPLACE;
        }
        function toSelector(tokens) {
            Sensor();
            var i = 0, len = tokens.length, selector = '';
            for (; i < len; i++) {
                selector += tokens[i].value;
            }
            var IARETREPLACE = selector;
            Sensor();
            return IARETREPLACE;
        }
        function addCombinator(matcher, combinator, base) {
            Sensor();
            var dir = combinator.dir, checkNonElements = base && dir === 'parentNode', doneName = done++;
            var IARETREPLACE = combinator.first ? function (elem, context, xml) {
                Sensor();
                while (elem = elem[dir]) {
                    if (elem.nodeType === 1 || checkNonElements) {
                        var IARETREPLACE = matcher(elem, context, xml);
                        Sensor();
                        return IARETREPLACE;
                    }
                }
                Sensor();
            } : function (elem, context, xml) {
                Sensor();
                var oldCache, outerCache, newCache = [
                        dirruns,
                        doneName
                    ];
                if (xml) {
                    while (elem = elem[dir]) {
                        if (elem.nodeType === 1 || checkNonElements) {
                            if (matcher(elem, context, xml)) {
                                var IARETREPLACE = true;
                                Sensor();
                                return IARETREPLACE;
                            }
                        }
                    }
                } else {
                    while (elem = elem[dir]) {
                        if (elem.nodeType === 1 || checkNonElements) {
                            outerCache = elem[expando] || (elem[expando] = {});
                            if ((oldCache = outerCache[dir]) && oldCache[0] === dirruns && oldCache[1] === doneName) {
                                var IARETREPLACE = newCache[2] = oldCache[2];
                                Sensor();
                                return IARETREPLACE;
                            } else {
                                outerCache[dir] = newCache;
                                if (newCache[2] = matcher(elem, context, xml)) {
                                    var IARETREPLACE = true;
                                    Sensor();
                                    return IARETREPLACE;
                                }
                            }
                        }
                    }
                }
                Sensor();
            };
            Sensor();
            return IARETREPLACE;
        }
        function elementMatcher(matchers) {
            var IARETREPLACE = matchers.length > 1 ? function (elem, context, xml) {
                Sensor();
                var i = matchers.length;
                while (i--) {
                    if (!matchers[i](elem, context, xml)) {
                        var IARETREPLACE = false;
                        Sensor();
                        return IARETREPLACE;
                    }
                }
                var IARETREPLACE = true;
                Sensor();
                return IARETREPLACE;
            } : matchers[0];
            Sensor();
            return IARETREPLACE;
        }
        function condense(unmatched, map, filter, context, xml) {
            Sensor();
            var elem, newUnmatched = [], i = 0, len = unmatched.length, mapped = map != null;
            for (; i < len; i++) {
                if (elem = unmatched[i]) {
                    if (!filter || filter(elem, context, xml)) {
                        newUnmatched.push(elem);
                        if (mapped) {
                            map.push(i);
                        }
                    }
                }
            }
            var IARETREPLACE = newUnmatched;
            Sensor();
            return IARETREPLACE;
        }
        function setMatcher(preFilter, selector, matcher, postFilter, postFinder, postSelector) {
            Sensor();
            if (postFilter && !postFilter[expando]) {
                postFilter = setMatcher(postFilter);
            }
            if (postFinder && !postFinder[expando]) {
                postFinder = setMatcher(postFinder, postSelector);
            }
            var IARETREPLACE = markFunction(function (seed, results, context, xml) {
                Sensor();
                var temp, i, elem, preMap = [], postMap = [], preexisting = results.length, elems = seed || multipleContexts(selector || '*', context.nodeType ? [context] : context, []), matcherIn = preFilter && (seed || !selector) ? condense(elems, preMap, preFilter, context, xml) : elems, matcherOut = matcher ? postFinder || (seed ? preFilter : preexisting || postFilter) ? [] : results : matcherIn;
                if (matcher) {
                    matcher(matcherIn, matcherOut, context, xml);
                }
                if (postFilter) {
                    temp = condense(matcherOut, postMap);
                    postFilter(temp, [], context, xml);
                    i = temp.length;
                    while (i--) {
                        if (elem = temp[i]) {
                            matcherOut[postMap[i]] = !(matcherIn[postMap[i]] = elem);
                        }
                    }
                }
                if (seed) {
                    if (postFinder || preFilter) {
                        if (postFinder) {
                            temp = [];
                            i = matcherOut.length;
                            while (i--) {
                                if (elem = matcherOut[i]) {
                                    temp.push(matcherIn[i] = elem);
                                }
                            }
                            postFinder(null, matcherOut = [], temp, xml);
                        }
                        i = matcherOut.length;
                        while (i--) {
                            if ((elem = matcherOut[i]) && (temp = postFinder ? indexOf.call(seed, elem) : preMap[i]) > -1) {
                                seed[temp] = !(results[temp] = elem);
                            }
                        }
                    }
                } else {
                    matcherOut = condense(matcherOut === results ? matcherOut.splice(preexisting, matcherOut.length) : matcherOut);
                    if (postFinder) {
                        postFinder(null, results, matcherOut, xml);
                    } else {
                        push.apply(results, matcherOut);
                    }
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        }
        function matcherFromTokens(tokens) {
            Sensor();
            var checkContext, matcher, j, len = tokens.length, leadingRelative = Expr.relative[tokens[0].type], implicitRelative = leadingRelative || Expr.relative[' '], i = leadingRelative ? 1 : 0, matchContext = addCombinator(function (elem) {
                    var IARETREPLACE = elem === checkContext;
                    Sensor();
                    return IARETREPLACE;
                }, implicitRelative, true), matchAnyContext = addCombinator(function (elem) {
                    var IARETREPLACE = indexOf.call(checkContext, elem) > -1;
                    Sensor();
                    return IARETREPLACE;
                }, implicitRelative, true), matchers = [function (elem, context, xml) {
                        var IARETREPLACE = !leadingRelative && (xml || context !== outermostContext) || ((checkContext = context).nodeType ? matchContext(elem, context, xml) : matchAnyContext(elem, context, xml));
                        Sensor();
                        return IARETREPLACE;
                    }];
            for (; i < len; i++) {
                if (matcher = Expr.relative[tokens[i].type]) {
                    matchers = [addCombinator(elementMatcher(matchers), matcher)];
                } else {
                    matcher = Expr.filter[tokens[i].type].apply(null, tokens[i].matches);
                    if (matcher[expando]) {
                        j = ++i;
                        for (; j < len; j++) {
                            if (Expr.relative[tokens[j].type]) {
                                break;
                            }
                        }
                        var IARETREPLACE = setMatcher(i > 1 && elementMatcher(matchers), i > 1 && toSelector(tokens.slice(0, i - 1).concat({ value: tokens[i - 2].type === ' ' ? '*' : '' })).replace(rtrim, '$1'), matcher, i < j && matcherFromTokens(tokens.slice(i, j)), j < len && matcherFromTokens(tokens = tokens.slice(j)), j < len && toSelector(tokens));
                        Sensor();
                        return IARETREPLACE;
                    }
                    matchers.push(matcher);
                }
            }
            var IARETREPLACE = elementMatcher(matchers);
            Sensor();
            return IARETREPLACE;
        }
        function matcherFromGroupMatchers(elementMatchers, setMatchers) {
            Sensor();
            var bySet = setMatchers.length > 0, byElement = elementMatchers.length > 0, superMatcher = function (seed, context, xml, results, outermost) {
                    Sensor();
                    var elem, j, matcher, matchedCount = 0, i = '0', unmatched = seed && [], setMatched = [], contextBackup = outermostContext, elems = seed || byElement && Expr.find['TAG']('*', outermost), dirrunsUnique = dirruns += contextBackup == null ? 1 : Math.random() || 0.1, len = elems.length;
                    if (outermost) {
                        outermostContext = context !== document && context;
                    }
                    for (; i !== len && (elem = elems[i]) != null; i++) {
                        if (byElement && elem) {
                            j = 0;
                            while (matcher = elementMatchers[j++]) {
                                if (matcher(elem, context, xml)) {
                                    results.push(elem);
                                    break;
                                }
                            }
                            if (outermost) {
                                dirruns = dirrunsUnique;
                            }
                        }
                        if (bySet) {
                            if (elem = !matcher && elem) {
                                matchedCount--;
                            }
                            if (seed) {
                                unmatched.push(elem);
                            }
                        }
                    }
                    matchedCount += i;
                    if (bySet && i !== matchedCount) {
                        j = 0;
                        while (matcher = setMatchers[j++]) {
                            matcher(unmatched, setMatched, context, xml);
                        }
                        if (seed) {
                            if (matchedCount > 0) {
                                while (i--) {
                                    if (!(unmatched[i] || setMatched[i])) {
                                        setMatched[i] = pop.call(results);
                                    }
                                }
                            }
                            setMatched = condense(setMatched);
                        }
                        push.apply(results, setMatched);
                        if (outermost && !seed && setMatched.length > 0 && matchedCount + setMatchers.length > 1) {
                            Sizzle.uniqueSort(results);
                        }
                    }
                    if (outermost) {
                        dirruns = dirrunsUnique;
                        outermostContext = contextBackup;
                    }
                    var IARETREPLACE = unmatched;
                    Sensor();
                    return IARETREPLACE;
                };
            var IARETREPLACE = bySet ? markFunction(superMatcher) : superMatcher;
            Sensor();
            return IARETREPLACE;
        }
        compile = Sizzle.compile = function (selector, group) {
            Sensor();
            var i, setMatchers = [], elementMatchers = [], cached = compilerCache[selector + ' '];
            if (!cached) {
                if (!group) {
                    group = tokenize(selector);
                }
                i = group.length;
                while (i--) {
                    cached = matcherFromTokens(group[i]);
                    if (cached[expando]) {
                        setMatchers.push(cached);
                    } else {
                        elementMatchers.push(cached);
                    }
                }
                cached = compilerCache(selector, matcherFromGroupMatchers(elementMatchers, setMatchers));
            }
            var IARETREPLACE = cached;
            Sensor();
            return IARETREPLACE;
        };
        function multipleContexts(selector, contexts, results) {
            Sensor();
            var i = 0, len = contexts.length;
            for (; i < len; i++) {
                Sizzle(selector, contexts[i], results);
            }
            var IARETREPLACE = results;
            Sensor();
            return IARETREPLACE;
        }
        function select(selector, context, results, seed) {
            Sensor();
            var i, tokens, token, type, find, match = tokenize(selector);
            if (!seed) {
                if (match.length === 1) {
                    tokens = match[0] = match[0].slice(0);
                    if (tokens.length > 2 && (token = tokens[0]).type === 'ID' && support.getById && context.nodeType === 9 && documentIsHTML && Expr.relative[tokens[1].type]) {
                        context = (Expr.find['ID'](token.matches[0].replace(runescape, funescape), context) || [])[0];
                        if (!context) {
                            var IARETREPLACE = results;
                            Sensor();
                            return IARETREPLACE;
                        }
                        selector = selector.slice(tokens.shift().value.length);
                    }
                    i = matchExpr['needsContext'].test(selector) ? 0 : tokens.length;
                    while (i--) {
                        token = tokens[i];
                        if (Expr.relative[type = token.type]) {
                            break;
                        }
                        if (find = Expr.find[type]) {
                            if (seed = find(token.matches[0].replace(runescape, funescape), rsibling.test(tokens[0].type) && testContext(context.parentNode) || context)) {
                                tokens.splice(i, 1);
                                selector = seed.length && toSelector(tokens);
                                if (!selector) {
                                    push.apply(results, seed);
                                    var IARETREPLACE = results;
                                    Sensor();
                                    return IARETREPLACE;
                                }
                                break;
                            }
                        }
                    }
                }
            }
            compile(selector, match)(seed, context, !documentIsHTML, results, rsibling.test(selector) && testContext(context.parentNode) || context);
            var IARETREPLACE = results;
            Sensor();
            return IARETREPLACE;
        }
        support.sortStable = expando.split('').sort(sortOrder).join('') === expando;
        support.detectDuplicates = !!hasDuplicate;
        setDocument();
        support.sortDetached = assert(function (div1) {
            var IARETREPLACE = div1.compareDocumentPosition(document.createElement('div')) & 1;
            Sensor();
            return IARETREPLACE;
        });
        if (!assert(function (div) {
                Sensor();
                div.innerHTML = '<a href=\'#\'></a>';
                var IARETREPLACE = div.firstChild.getAttribute('href') === '#';
                Sensor();
                return IARETREPLACE;
            })) {
            addHandle('type|href|height|width', function (elem, name, isXML) {
                Sensor();
                if (!isXML) {
                    var IARETREPLACE = elem.getAttribute(name, name.toLowerCase() === 'type' ? 1 : 2);
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            });
        }
        if (!support.attributes || !assert(function (div) {
                Sensor();
                div.innerHTML = '<input/>';
                div.firstChild.setAttribute('value', '');
                var IARETREPLACE = div.firstChild.getAttribute('value') === '';
                Sensor();
                return IARETREPLACE;
            })) {
            addHandle('value', function (elem, name, isXML) {
                Sensor();
                if (!isXML && elem.nodeName.toLowerCase() === 'input') {
                    var IARETREPLACE = elem.defaultValue;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            });
        }
        if (!assert(function (div) {
                var IARETREPLACE = div.getAttribute('disabled') == null;
                Sensor();
                return IARETREPLACE;
            })) {
            addHandle(booleans, function (elem, name, isXML) {
                Sensor();
                var val;
                if (!isXML) {
                    var IARETREPLACE = elem[name] === true ? name.toLowerCase() : (val = elem.getAttributeNode(name)) && val.specified ? val.value : null;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            });
        }
        var IARETREPLACE = Sizzle;
        Sensor();
        return IARETREPLACE;
    }(window);
    jQuery.find = Sizzle;
    jQuery.expr = Sizzle.selectors;
    jQuery.expr[':'] = jQuery.expr.pseudos;
    jQuery.unique = Sizzle.uniqueSort;
    jQuery.text = Sizzle.getText;
    jQuery.isXMLDoc = Sizzle.isXML;
    jQuery.contains = Sizzle.contains;
    var rneedsContext = jQuery.expr.match.needsContext;
    var rsingleTag = /^<(\w+)\s*\/?>(?:<\/\1>|)$/;
    var risSimple = /^.[^:#\[\.,]*$/;
    function winnow(elements, qualifier, not) {
        Sensor();
        if (jQuery.isFunction(qualifier)) {
            var IARETREPLACE = jQuery.grep(elements, function (elem, i) {
                var IARETREPLACE = !!qualifier.call(elem, i, elem) !== not;
                Sensor();
                return IARETREPLACE;
            });
            Sensor();
            return IARETREPLACE;
        }
        if (qualifier.nodeType) {
            var IARETREPLACE = jQuery.grep(elements, function (elem) {
                var IARETREPLACE = elem === qualifier !== not;
                Sensor();
                return IARETREPLACE;
            });
            Sensor();
            return IARETREPLACE;
        }
        if (typeof qualifier === 'string') {
            if (risSimple.test(qualifier)) {
                var IARETREPLACE = jQuery.filter(qualifier, elements, not);
                Sensor();
                return IARETREPLACE;
            }
            qualifier = jQuery.filter(qualifier, elements);
        }
        var IARETREPLACE = jQuery.grep(elements, function (elem) {
            var IARETREPLACE = jQuery.inArray(elem, qualifier) >= 0 !== not;
            Sensor();
            return IARETREPLACE;
        });
        Sensor();
        return IARETREPLACE;
    }
    jQuery.filter = function (expr, elems, not) {
        Sensor();
        var elem = elems[0];
        if (not) {
            expr = ':not(' + expr + ')';
        }
        var IARETREPLACE = elems.length === 1 && elem.nodeType === 1 ? jQuery.find.matchesSelector(elem, expr) ? [elem] : [] : jQuery.find.matches(expr, jQuery.grep(elems, function (elem) {
            var IARETREPLACE = elem.nodeType === 1;
            Sensor();
            return IARETREPLACE;
        }));
        Sensor();
        return IARETREPLACE;
    };
    jQuery.fn.extend({
        find: function (selector) {
            Sensor();
            var i, ret = [], self = this, len = self.length;
            if (typeof selector !== 'string') {
                var IARETREPLACE = this.pushStack(jQuery(selector).filter(function () {
                    Sensor();
                    for (i = 0; i < len; i++) {
                        if (jQuery.contains(self[i], this)) {
                            var IARETREPLACE = true;
                            Sensor();
                            return IARETREPLACE;
                        }
                    }
                    Sensor();
                }));
                Sensor();
                return IARETREPLACE;
            }
            for (i = 0; i < len; i++) {
                jQuery.find(selector, self[i], ret);
            }
            ret = this.pushStack(len > 1 ? jQuery.unique(ret) : ret);
            ret.selector = this.selector ? this.selector + ' ' + selector : selector;
            var IARETREPLACE = ret;
            Sensor();
            return IARETREPLACE;
        },
        filter: function (selector) {
            var IARETREPLACE = this.pushStack(winnow(this, selector || [], false));
            Sensor();
            return IARETREPLACE;
        },
        not: function (selector) {
            var IARETREPLACE = this.pushStack(winnow(this, selector || [], true));
            Sensor();
            return IARETREPLACE;
        },
        is: function (selector) {
            var IARETREPLACE = !!winnow(this, typeof selector === 'string' && rneedsContext.test(selector) ? jQuery(selector) : selector || [], false).length;
            Sensor();
            return IARETREPLACE;
        }
    });
    var rootjQuery, document = window.document, rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/, init = jQuery.fn.init = function (selector, context) {
            Sensor();
            var match, elem;
            if (!selector) {
                var IARETREPLACE = this;
                Sensor();
                return IARETREPLACE;
            }
            if (typeof selector === 'string') {
                if (selector.charAt(0) === '<' && selector.charAt(selector.length - 1) === '>' && selector.length >= 3) {
                    match = [
                        null,
                        selector,
                        null
                    ];
                } else {
                    match = rquickExpr.exec(selector);
                }
                if (match && (match[1] || !context)) {
                    if (match[1]) {
                        context = context instanceof jQuery ? context[0] : context;
                        jQuery.merge(this, jQuery.parseHTML(match[1], context && context.nodeType ? context.ownerDocument || context : document, true));
                        if (rsingleTag.test(match[1]) && jQuery.isPlainObject(context)) {
                            for (match in context) {
                                if (jQuery.isFunction(this[match])) {
                                    this[match](context[match]);
                                } else {
                                    this.attr(match, context[match]);
                                }
                            }
                        }
                        var IARETREPLACE = this;
                        Sensor();
                        return IARETREPLACE;
                    } else {
                        elem = document.getElementById(match[2]);
                        if (elem && elem.parentNode) {
                            if (elem.id !== match[2]) {
                                var IARETREPLACE = rootjQuery.find(selector);
                                Sensor();
                                return IARETREPLACE;
                            }
                            this.length = 1;
                            this[0] = elem;
                        }
                        this.context = document;
                        this.selector = selector;
                        var IARETREPLACE = this;
                        Sensor();
                        return IARETREPLACE;
                    }
                } else {
                    if (!context || context.jquery) {
                        var IARETREPLACE = (context || rootjQuery).find(selector);
                        Sensor();
                        return IARETREPLACE;
                    } else {
                        var IARETREPLACE = this.constructor(context).find(selector);
                        Sensor();
                        return IARETREPLACE;
                    }
                }
            } else {
                if (selector.nodeType) {
                    this.context = this[0] = selector;
                    this.length = 1;
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                } else {
                    if (jQuery.isFunction(selector)) {
                        var IARETREPLACE = typeof rootjQuery.ready !== 'undefined' ? rootjQuery.ready(selector) : selector(jQuery);
                        Sensor();
                        return IARETREPLACE;
                    }
                }
            }
            if (selector.selector !== undefined) {
                this.selector = selector.selector;
                this.context = selector.context;
            }
            var IARETREPLACE = jQuery.makeArray(selector, this);
            Sensor();
            return IARETREPLACE;
        };
    init.prototype = jQuery.fn;
    rootjQuery = jQuery(document);
    var rparentsprev = /^(?:parents|prev(?:Until|All))/, guaranteedUnique = {
            children: true,
            contents: true,
            next: true,
            prev: true
        };
    jQuery.extend({
        dir: function (elem, dir, until) {
            Sensor();
            var matched = [], cur = elem[dir];
            while (cur && cur.nodeType !== 9 && (until === undefined || cur.nodeType !== 1 || !jQuery(cur).is(until))) {
                if (cur.nodeType === 1) {
                    matched.push(cur);
                }
                cur = cur[dir];
            }
            var IARETREPLACE = matched;
            Sensor();
            return IARETREPLACE;
        },
        sibling: function (n, elem) {
            Sensor();
            var r = [];
            for (; n; n = n.nextSibling) {
                if (n.nodeType === 1 && n !== elem) {
                    r.push(n);
                }
            }
            var IARETREPLACE = r;
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.fn.extend({
        has: function (target) {
            Sensor();
            var i, targets = jQuery(target, this), len = targets.length;
            var IARETREPLACE = this.filter(function () {
                Sensor();
                for (i = 0; i < len; i++) {
                    if (jQuery.contains(this, targets[i])) {
                        var IARETREPLACE = true;
                        Sensor();
                        return IARETREPLACE;
                    }
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        closest: function (selectors, context) {
            Sensor();
            var cur, i = 0, l = this.length, matched = [], pos = rneedsContext.test(selectors) || typeof selectors !== 'string' ? jQuery(selectors, context || this.context) : 0;
            for (; i < l; i++) {
                for (cur = this[i]; cur && cur !== context; cur = cur.parentNode) {
                    if (cur.nodeType < 11 && (pos ? pos.index(cur) > -1 : cur.nodeType === 1 && jQuery.find.matchesSelector(cur, selectors))) {
                        matched.push(cur);
                        break;
                    }
                }
            }
            var IARETREPLACE = this.pushStack(matched.length > 1 ? jQuery.unique(matched) : matched);
            Sensor();
            return IARETREPLACE;
        },
        index: function (elem) {
            Sensor();
            if (!elem) {
                var IARETREPLACE = this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
                Sensor();
                return IARETREPLACE;
            }
            if (typeof elem === 'string') {
                var IARETREPLACE = jQuery.inArray(this[0], jQuery(elem));
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = jQuery.inArray(elem.jquery ? elem[0] : elem, this);
            Sensor();
            return IARETREPLACE;
        },
        add: function (selector, context) {
            var IARETREPLACE = this.pushStack(jQuery.unique(jQuery.merge(this.get(), jQuery(selector, context))));
            Sensor();
            return IARETREPLACE;
        },
        addBack: function (selector) {
            var IARETREPLACE = this.add(selector == null ? this.prevObject : this.prevObject.filter(selector));
            Sensor();
            return IARETREPLACE;
        }
    });
    function sibling(cur, dir) {
        Sensor();
        do {
            cur = cur[dir];
        } while (cur && cur.nodeType !== 1);
        var IARETREPLACE = cur;
        Sensor();
        return IARETREPLACE;
    }
    jQuery.each({
        parent: function (elem) {
            Sensor();
            var parent = elem.parentNode;
            var IARETREPLACE = parent && parent.nodeType !== 11 ? parent : null;
            Sensor();
            return IARETREPLACE;
        },
        parents: function (elem) {
            var IARETREPLACE = jQuery.dir(elem, 'parentNode');
            Sensor();
            return IARETREPLACE;
        },
        parentsUntil: function (elem, i, until) {
            var IARETREPLACE = jQuery.dir(elem, 'parentNode', until);
            Sensor();
            return IARETREPLACE;
        },
        next: function (elem) {
            var IARETREPLACE = sibling(elem, 'nextSibling');
            Sensor();
            return IARETREPLACE;
        },
        prev: function (elem) {
            var IARETREPLACE = sibling(elem, 'previousSibling');
            Sensor();
            return IARETREPLACE;
        },
        nextAll: function (elem) {
            var IARETREPLACE = jQuery.dir(elem, 'nextSibling');
            Sensor();
            return IARETREPLACE;
        },
        prevAll: function (elem) {
            var IARETREPLACE = jQuery.dir(elem, 'previousSibling');
            Sensor();
            return IARETREPLACE;
        },
        nextUntil: function (elem, i, until) {
            var IARETREPLACE = jQuery.dir(elem, 'nextSibling', until);
            Sensor();
            return IARETREPLACE;
        },
        prevUntil: function (elem, i, until) {
            var IARETREPLACE = jQuery.dir(elem, 'previousSibling', until);
            Sensor();
            return IARETREPLACE;
        },
        siblings: function (elem) {
            var IARETREPLACE = jQuery.sibling((elem.parentNode || {}).firstChild, elem);
            Sensor();
            return IARETREPLACE;
        },
        children: function (elem) {
            var IARETREPLACE = jQuery.sibling(elem.firstChild);
            Sensor();
            return IARETREPLACE;
        },
        contents: function (elem) {
            var IARETREPLACE = jQuery.nodeName(elem, 'iframe') ? elem.contentDocument || elem.contentWindow.document : jQuery.merge([], elem.childNodes);
            Sensor();
            return IARETREPLACE;
        }
    }, function (name, fn) {
        Sensor();
        jQuery.fn[name] = function (until, selector) {
            Sensor();
            var ret = jQuery.map(this, fn, until);
            if (name.slice(-5) !== 'Until') {
                selector = until;
            }
            if (selector && typeof selector === 'string') {
                ret = jQuery.filter(selector, ret);
            }
            if (this.length > 1) {
                if (!guaranteedUnique[name]) {
                    ret = jQuery.unique(ret);
                }
                if (rparentsprev.test(name)) {
                    ret = ret.reverse();
                }
            }
            var IARETREPLACE = this.pushStack(ret);
            Sensor();
            return IARETREPLACE;
        };
        Sensor();
    });
    var rnotwhite = /\S+/g;
    var optionsCache = {};
    function createOptions(options) {
        Sensor();
        var object = optionsCache[options] = {};
        jQuery.each(options.match(rnotwhite) || [], function (_, flag) {
            Sensor();
            object[flag] = true;
            Sensor();
        });
        var IARETREPLACE = object;
        Sensor();
        return IARETREPLACE;
    }
    jQuery.Callbacks = function (options) {
        Sensor();
        options = typeof options === 'string' ? optionsCache[options] || createOptions(options) : jQuery.extend({}, options);
        var firing, memory, fired, firingLength, firingIndex, firingStart, list = [], stack = !options.once && [], fire = function (data) {
                Sensor();
                memory = options.memory && data;
                fired = true;
                firingIndex = firingStart || 0;
                firingStart = 0;
                firingLength = list.length;
                firing = true;
                for (; list && firingIndex < firingLength; firingIndex++) {
                    if (list[firingIndex].apply(data[0], data[1]) === false && options.stopOnFalse) {
                        memory = false;
                        break;
                    }
                }
                firing = false;
                if (list) {
                    if (stack) {
                        if (stack.length) {
                            fire(stack.shift());
                        }
                    } else {
                        if (memory) {
                            list = [];
                        } else {
                            self.disable();
                        }
                    }
                }
                Sensor();
            }, self = {
                add: function () {
                    Sensor();
                    if (list) {
                        var start = list.length;
                        (function add(args) {
                            Sensor();
                            jQuery.each(args, function (_, arg) {
                                Sensor();
                                var type = jQuery.type(arg);
                                if (type === 'function') {
                                    if (!options.unique || !self.has(arg)) {
                                        list.push(arg);
                                    }
                                } else {
                                    if (arg && arg.length && type !== 'string') {
                                        add(arg);
                                    }
                                }
                                Sensor();
                            });
                            Sensor();
                        }(arguments));
                        if (firing) {
                            firingLength = list.length;
                        } else {
                            if (memory) {
                                firingStart = start;
                                fire(memory);
                            }
                        }
                    }
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                },
                remove: function () {
                    Sensor();
                    if (list) {
                        jQuery.each(arguments, function (_, arg) {
                            Sensor();
                            var index;
                            while ((index = jQuery.inArray(arg, list, index)) > -1) {
                                list.splice(index, 1);
                                if (firing) {
                                    if (index <= firingLength) {
                                        firingLength--;
                                    }
                                    if (index <= firingIndex) {
                                        firingIndex--;
                                    }
                                }
                            }
                            Sensor();
                        });
                    }
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                },
                has: function (fn) {
                    var IARETREPLACE = fn ? jQuery.inArray(fn, list) > -1 : !!(list && list.length);
                    Sensor();
                    return IARETREPLACE;
                },
                empty: function () {
                    Sensor();
                    list = [];
                    firingLength = 0;
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                },
                disable: function () {
                    Sensor();
                    list = stack = memory = undefined;
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                },
                disabled: function () {
                    var IARETREPLACE = !list;
                    Sensor();
                    return IARETREPLACE;
                },
                lock: function () {
                    Sensor();
                    stack = undefined;
                    if (!memory) {
                        self.disable();
                    }
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                },
                locked: function () {
                    var IARETREPLACE = !stack;
                    Sensor();
                    return IARETREPLACE;
                },
                fireWith: function (context, args) {
                    Sensor();
                    if (list && (!fired || stack)) {
                        args = args || [];
                        args = [
                            context,
                            args.slice ? args.slice() : args
                        ];
                        if (firing) {
                            stack.push(args);
                        } else {
                            fire(args);
                        }
                    }
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                },
                fire: function () {
                    Sensor();
                    self.fireWith(this, arguments);
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                },
                fired: function () {
                    var IARETREPLACE = !!fired;
                    Sensor();
                    return IARETREPLACE;
                }
            };
        var IARETREPLACE = self;
        Sensor();
        return IARETREPLACE;
    };
    jQuery.extend({
        Deferred: function (func) {
            Sensor();
            var tuples = [
                    [
                        'resolve',
                        'done',
                        jQuery.Callbacks('once memory'),
                        'resolved'
                    ],
                    [
                        'reject',
                        'fail',
                        jQuery.Callbacks('once memory'),
                        'rejected'
                    ],
                    [
                        'notify',
                        'progress',
                        jQuery.Callbacks('memory')
                    ]
                ], state = 'pending', promise = {
                    state: function () {
                        var IARETREPLACE = state;
                        Sensor();
                        return IARETREPLACE;
                    },
                    always: function () {
                        Sensor();
                        deferred.done(arguments).fail(arguments);
                        var IARETREPLACE = this;
                        Sensor();
                        return IARETREPLACE;
                    },
                    then: function () {
                        Sensor();
                        var fns = arguments;
                        var IARETREPLACE = jQuery.Deferred(function (newDefer) {
                            Sensor();
                            jQuery.each(tuples, function (i, tuple) {
                                Sensor();
                                var fn = jQuery.isFunction(fns[i]) && fns[i];
                                deferred[tuple[1]](function () {
                                    Sensor();
                                    var returned = fn && fn.apply(this, arguments);
                                    if (returned && jQuery.isFunction(returned.promise)) {
                                        returned.promise().done(newDefer.resolve).fail(newDefer.reject).progress(newDefer.notify);
                                    } else {
                                        newDefer[tuple[0] + 'With'](this === promise ? newDefer.promise() : this, fn ? [returned] : arguments);
                                    }
                                    Sensor();
                                });
                                Sensor();
                            });
                            fns = null;
                            Sensor();
                        }).promise();
                        Sensor();
                        return IARETREPLACE;
                    },
                    promise: function (obj) {
                        var IARETREPLACE = obj != null ? jQuery.extend(obj, promise) : promise;
                        Sensor();
                        return IARETREPLACE;
                    }
                }, deferred = {};
            promise.pipe = promise.then;
            jQuery.each(tuples, function (i, tuple) {
                Sensor();
                var list = tuple[2], stateString = tuple[3];
                promise[tuple[1]] = list.add;
                if (stateString) {
                    list.add(function () {
                        Sensor();
                        state = stateString;
                        Sensor();
                    }, tuples[i ^ 1][2].disable, tuples[2][2].lock);
                }
                deferred[tuple[0]] = function () {
                    Sensor();
                    deferred[tuple[0] + 'With'](this === deferred ? promise : this, arguments);
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                };
                deferred[tuple[0] + 'With'] = list.fireWith;
                Sensor();
            });
            promise.promise(deferred);
            if (func) {
                func.call(deferred, deferred);
            }
            var IARETREPLACE = deferred;
            Sensor();
            return IARETREPLACE;
        },
        when: function (subordinate) {
            Sensor();
            var i = 0, resolveValues = slice.call(arguments), length = resolveValues.length, remaining = length !== 1 || subordinate && jQuery.isFunction(subordinate.promise) ? length : 0, deferred = remaining === 1 ? subordinate : jQuery.Deferred(), updateFunc = function (i, contexts, values) {
                    var IARETREPLACE = function (value) {
                        Sensor();
                        contexts[i] = this;
                        values[i] = arguments.length > 1 ? slice.call(arguments) : value;
                        if (values === progressValues) {
                            deferred.notifyWith(contexts, values);
                        } else {
                            if (!--remaining) {
                                deferred.resolveWith(contexts, values);
                            }
                        }
                        Sensor();
                    };
                    Sensor();
                    return IARETREPLACE;
                }, progressValues, progressContexts, resolveContexts;
            if (length > 1) {
                progressValues = new Array(length);
                progressContexts = new Array(length);
                resolveContexts = new Array(length);
                for (; i < length; i++) {
                    if (resolveValues[i] && jQuery.isFunction(resolveValues[i].promise)) {
                        resolveValues[i].promise().done(updateFunc(i, resolveContexts, resolveValues)).fail(deferred.reject).progress(updateFunc(i, progressContexts, progressValues));
                    } else {
                        --remaining;
                    }
                }
            }
            if (!remaining) {
                deferred.resolveWith(resolveContexts, resolveValues);
            }
            var IARETREPLACE = deferred.promise();
            Sensor();
            return IARETREPLACE;
        }
    });
    var readyList;
    jQuery.fn.ready = function (fn) {
        Sensor();
        jQuery.ready.promise().done(fn);
        var IARETREPLACE = this;
        Sensor();
        return IARETREPLACE;
    };
    jQuery.extend({
        isReady: false,
        readyWait: 1,
        holdReady: function (hold) {
            Sensor();
            if (hold) {
                jQuery.readyWait++;
            } else {
                jQuery.ready(true);
            }
            Sensor();
        },
        ready: function (wait) {
            Sensor();
            if (wait === true ? --jQuery.readyWait : jQuery.isReady) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            if (!document.body) {
                var IARETREPLACE = setTimeout(jQuery.ready);
                Sensor();
                return IARETREPLACE;
            }
            jQuery.isReady = true;
            if (wait !== true && --jQuery.readyWait > 0) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            readyList.resolveWith(document, [jQuery]);
            if (jQuery.fn.trigger) {
                jQuery(document).trigger('ready').off('ready');
            }
            Sensor();
        }
    });
    function detach() {
        Sensor();
        if (document.addEventListener) {
            document.removeEventListener('DOMContentLoaded', completed, false);
            window.removeEventListener('load', completed, false);
        } else {
            document.detachEvent('onreadystatechange', completed);
            window.detachEvent('onload', completed);
        }
        Sensor();
    }
    function completed() {
        Sensor();
        if (document.addEventListener || event.type === 'load' || document.readyState === 'complete') {
            detach();
            jQuery.ready();
        }
        Sensor();
    }
    jQuery.ready.promise = function (obj) {
        Sensor();
        if (!readyList) {
            readyList = jQuery.Deferred();
            if (document.readyState === 'complete') {
                setTimeout(jQuery.ready);
            } else {
                if (document.addEventListener) {
                    document.addEventListener('DOMContentLoaded', completed, false);
                    window.addEventListener('load', completed, false);
                } else {
                    document.attachEvent('onreadystatechange', completed);
                    window.attachEvent('onload', completed);
                    var top = false;
                    try {
                        top = window.frameElement == null && document.documentElement;
                    } catch (e) {
                    }
                    if (top && top.doScroll) {
                        (function doScrollCheck() {
                            Sensor();
                            if (!jQuery.isReady) {
                                try {
                                    top.doScroll('left');
                                } catch (e) {
                                    var IARETREPLACE = setTimeout(doScrollCheck, 50);
                                    Sensor();
                                    return IARETREPLACE;
                                }
                                detach();
                                jQuery.ready();
                            }
                            Sensor();
                        }());
                    }
                }
            }
        }
        var IARETREPLACE = readyList.promise(obj);
        Sensor();
        return IARETREPLACE;
    };
    var strundefined = typeof undefined;
    var i;
    for (i in jQuery(support)) {
        break;
    }
    support.ownLast = i !== '0';
    support.inlineBlockNeedsLayout = false;
    jQuery(function () {
        Sensor();
        var container, div, body = document.getElementsByTagName('body')[0];
        if (!body) {
            var IARETREPLACE;
            Sensor();
            return IARETREPLACE;
        }
        container = document.createElement('div');
        container.style.cssText = 'border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px';
        div = document.createElement('div');
        body.appendChild(container).appendChild(div);
        if (typeof div.style.zoom !== strundefined) {
            div.style.cssText = 'border:0;margin:0;width:1px;padding:1px;display:inline;zoom:1';
            if (support.inlineBlockNeedsLayout = div.offsetWidth === 3) {
                body.style.zoom = 1;
            }
        }
        body.removeChild(container);
        container = div = null;
        Sensor();
    });
    (function () {
        Sensor();
        var div = document.createElement('div');
        if (support.deleteExpando == null) {
            support.deleteExpando = true;
            try {
                delete div.test;
            } catch (e) {
                support.deleteExpando = false;
            }
        }
        div = null;
        Sensor();
    }());
    jQuery.acceptData = function (elem) {
        Sensor();
        var noData = jQuery.noData[(elem.nodeName + ' ').toLowerCase()], nodeType = +elem.nodeType || 1;
        var IARETREPLACE = nodeType !== 1 && nodeType !== 9 ? false : !noData || noData !== true && elem.getAttribute('classid') === noData;
        Sensor();
        return IARETREPLACE;
    };
    var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, rmultiDash = /([A-Z])/g;
    function dataAttr(elem, key, data) {
        Sensor();
        if (data === undefined && elem.nodeType === 1) {
            var name = 'data-' + key.replace(rmultiDash, '-$1').toLowerCase();
            data = elem.getAttribute(name);
            if (typeof data === 'string') {
                try {
                    data = data === 'true' ? true : data === 'false' ? false : data === 'null' ? null : +data + '' === data ? +data : rbrace.test(data) ? jQuery.parseJSON(data) : data;
                } catch (e) {
                }
                jQuery.data(elem, key, data);
            } else {
                data = undefined;
            }
        }
        var IARETREPLACE = data;
        Sensor();
        return IARETREPLACE;
    }
    function isEmptyDataObject(obj) {
        Sensor();
        var name;
        for (name in obj) {
            if (name === 'data' && jQuery.isEmptyObject(obj[name])) {
                continue;
            }
            if (name !== 'toJSON') {
                var IARETREPLACE = false;
                Sensor();
                return IARETREPLACE;
            }
        }
        var IARETREPLACE = true;
        Sensor();
        return IARETREPLACE;
    }
    function internalData(elem, name, data, pvt) {
        Sensor();
        if (!jQuery.acceptData(elem)) {
            var IARETREPLACE;
            Sensor();
            return IARETREPLACE;
        }
        var ret, thisCache, internalKey = jQuery.expando, isNode = elem.nodeType, cache = isNode ? jQuery.cache : elem, id = isNode ? elem[internalKey] : elem[internalKey] && internalKey;
        if ((!id || !cache[id] || !pvt && !cache[id].data) && data === undefined && typeof name === 'string') {
            var IARETREPLACE;
            Sensor();
            return IARETREPLACE;
        }
        if (!id) {
            if (isNode) {
                id = elem[internalKey] = deletedIds.pop() || jQuery.guid++;
            } else {
                id = internalKey;
            }
        }
        if (!cache[id]) {
            cache[id] = isNode ? {} : { toJSON: jQuery.noop };
        }
        if (typeof name === 'object' || typeof name === 'function') {
            if (pvt) {
                cache[id] = jQuery.extend(cache[id], name);
            } else {
                cache[id].data = jQuery.extend(cache[id].data, name);
            }
        }
        thisCache = cache[id];
        if (!pvt) {
            if (!thisCache.data) {
                thisCache.data = {};
            }
            thisCache = thisCache.data;
        }
        if (data !== undefined) {
            thisCache[jQuery.camelCase(name)] = data;
        }
        if (typeof name === 'string') {
            ret = thisCache[name];
            if (ret == null) {
                ret = thisCache[jQuery.camelCase(name)];
            }
        } else {
            ret = thisCache;
        }
        var IARETREPLACE = ret;
        Sensor();
        return IARETREPLACE;
    }
    function internalRemoveData(elem, name, pvt) {
        Sensor();
        if (!jQuery.acceptData(elem)) {
            var IARETREPLACE;
            Sensor();
            return IARETREPLACE;
        }
        var thisCache, i, isNode = elem.nodeType, cache = isNode ? jQuery.cache : elem, id = isNode ? elem[jQuery.expando] : jQuery.expando;
        if (!cache[id]) {
            var IARETREPLACE;
            Sensor();
            return IARETREPLACE;
        }
        if (name) {
            thisCache = pvt ? cache[id] : cache[id].data;
            if (thisCache) {
                if (!jQuery.isArray(name)) {
                    if (name in thisCache) {
                        name = [name];
                    } else {
                        name = jQuery.camelCase(name);
                        if (name in thisCache) {
                            name = [name];
                        } else {
                            name = name.split(' ');
                        }
                    }
                } else {
                    name = name.concat(jQuery.map(name, jQuery.camelCase));
                }
                i = name.length;
                while (i--) {
                    delete thisCache[name[i]];
                }
                if (pvt ? !isEmptyDataObject(thisCache) : !jQuery.isEmptyObject(thisCache)) {
                    var IARETREPLACE;
                    Sensor();
                    return IARETREPLACE;
                }
            }
        }
        if (!pvt) {
            delete cache[id].data;
            if (!isEmptyDataObject(cache[id])) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
        }
        if (isNode) {
            jQuery.cleanData([elem], true);
        } else {
            if (support.deleteExpando || cache != cache.window) {
                delete cache[id];
            } else {
                cache[id] = null;
            }
        }
        Sensor();
    }
    jQuery.extend({
        cache: {},
        noData: {
            'applet ': true,
            'embed ': true,
            'object ': 'clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        },
        hasData: function (elem) {
            Sensor();
            elem = elem.nodeType ? jQuery.cache[elem[jQuery.expando]] : elem[jQuery.expando];
            var IARETREPLACE = !!elem && !isEmptyDataObject(elem);
            Sensor();
            return IARETREPLACE;
        },
        data: function (elem, name, data) {
            var IARETREPLACE = internalData(elem, name, data);
            Sensor();
            return IARETREPLACE;
        },
        removeData: function (elem, name) {
            var IARETREPLACE = internalRemoveData(elem, name);
            Sensor();
            return IARETREPLACE;
        },
        _data: function (elem, name, data) {
            var IARETREPLACE = internalData(elem, name, data, true);
            Sensor();
            return IARETREPLACE;
        },
        _removeData: function (elem, name) {
            var IARETREPLACE = internalRemoveData(elem, name, true);
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.fn.extend({
        data: function (key, value) {
            Sensor();
            var i, name, data, elem = this[0], attrs = elem && elem.attributes;
            if (key === undefined) {
                if (this.length) {
                    data = jQuery.data(elem);
                    if (elem.nodeType === 1 && !jQuery._data(elem, 'parsedAttrs')) {
                        i = attrs.length;
                        while (i--) {
                            name = attrs[i].name;
                            if (name.indexOf('data-') === 0) {
                                name = jQuery.camelCase(name.slice(5));
                                dataAttr(elem, name, data[name]);
                            }
                        }
                        jQuery._data(elem, 'parsedAttrs', true);
                    }
                }
                var IARETREPLACE = data;
                Sensor();
                return IARETREPLACE;
            }
            if (typeof key === 'object') {
                var IARETREPLACE = this.each(function () {
                    Sensor();
                    jQuery.data(this, key);
                    Sensor();
                });
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = arguments.length > 1 ? this.each(function () {
                Sensor();
                jQuery.data(this, key, value);
                Sensor();
            }) : elem ? dataAttr(elem, key, jQuery.data(elem, key)) : undefined;
            Sensor();
            return IARETREPLACE;
        },
        removeData: function (key) {
            var IARETREPLACE = this.each(function () {
                Sensor();
                jQuery.removeData(this, key);
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.extend({
        queue: function (elem, type, data) {
            Sensor();
            var queue;
            if (elem) {
                type = (type || 'fx') + 'queue';
                queue = jQuery._data(elem, type);
                if (data) {
                    if (!queue || jQuery.isArray(data)) {
                        queue = jQuery._data(elem, type, jQuery.makeArray(data));
                    } else {
                        queue.push(data);
                    }
                }
                var IARETREPLACE = queue || [];
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
        },
        dequeue: function (elem, type) {
            Sensor();
            type = type || 'fx';
            var queue = jQuery.queue(elem, type), startLength = queue.length, fn = queue.shift(), hooks = jQuery._queueHooks(elem, type), next = function () {
                    Sensor();
                    jQuery.dequeue(elem, type);
                    Sensor();
                };
            if (fn === 'inprogress') {
                fn = queue.shift();
                startLength--;
            }
            if (fn) {
                if (type === 'fx') {
                    queue.unshift('inprogress');
                }
                delete hooks.stop;
                fn.call(elem, next, hooks);
            }
            if (!startLength && hooks) {
                hooks.empty.fire();
            }
            Sensor();
        },
        _queueHooks: function (elem, type) {
            Sensor();
            var key = type + 'queueHooks';
            var IARETREPLACE = jQuery._data(elem, key) || jQuery._data(elem, key, {
                empty: jQuery.Callbacks('once memory').add(function () {
                    Sensor();
                    jQuery._removeData(elem, type + 'queue');
                    jQuery._removeData(elem, key);
                    Sensor();
                })
            });
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.fn.extend({
        queue: function (type, data) {
            Sensor();
            var setter = 2;
            if (typeof type !== 'string') {
                data = type;
                type = 'fx';
                setter--;
            }
            if (arguments.length < setter) {
                var IARETREPLACE = jQuery.queue(this[0], type);
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = data === undefined ? this : this.each(function () {
                Sensor();
                var queue = jQuery.queue(this, type, data);
                jQuery._queueHooks(this, type);
                if (type === 'fx' && queue[0] !== 'inprogress') {
                    jQuery.dequeue(this, type);
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        dequeue: function (type) {
            var IARETREPLACE = this.each(function () {
                Sensor();
                jQuery.dequeue(this, type);
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        clearQueue: function (type) {
            var IARETREPLACE = this.queue(type || 'fx', []);
            Sensor();
            return IARETREPLACE;
        },
        promise: function (type, obj) {
            Sensor();
            var tmp, count = 1, defer = jQuery.Deferred(), elements = this, i = this.length, resolve = function () {
                    Sensor();
                    if (!--count) {
                        defer.resolveWith(elements, [elements]);
                    }
                    Sensor();
                };
            if (typeof type !== 'string') {
                obj = type;
                type = undefined;
            }
            type = type || 'fx';
            while (i--) {
                tmp = jQuery._data(elements[i], type + 'queueHooks');
                if (tmp && tmp.empty) {
                    count++;
                    tmp.empty.add(resolve);
                }
            }
            resolve();
            var IARETREPLACE = defer.promise(obj);
            Sensor();
            return IARETREPLACE;
        }
    });
    var pnum = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source;
    var cssExpand = [
        'Top',
        'Right',
        'Bottom',
        'Left'
    ];
    var isHidden = function (elem, el) {
        Sensor();
        elem = el || elem;
        var IARETREPLACE = jQuery.css(elem, 'display') === 'none' || !jQuery.contains(elem.ownerDocument, elem);
        Sensor();
        return IARETREPLACE;
    };
    var access = jQuery.access = function (elems, fn, key, value, chainable, emptyGet, raw) {
        Sensor();
        var i = 0, length = elems.length, bulk = key == null;
        if (jQuery.type(key) === 'object') {
            chainable = true;
            for (i in key) {
                jQuery.access(elems, fn, i, key[i], true, emptyGet, raw);
            }
        } else {
            if (value !== undefined) {
                chainable = true;
                if (!jQuery.isFunction(value)) {
                    raw = true;
                }
                if (bulk) {
                    if (raw) {
                        fn.call(elems, value);
                        fn = null;
                    } else {
                        bulk = fn;
                        fn = function (elem, key, value) {
                            var IARETREPLACE = bulk.call(jQuery(elem), value);
                            Sensor();
                            return IARETREPLACE;
                        };
                    }
                }
                if (fn) {
                    for (; i < length; i++) {
                        fn(elems[i], key, raw ? value : value.call(elems[i], i, fn(elems[i], key)));
                    }
                }
            }
        }
        var IARETREPLACE = chainable ? elems : bulk ? fn.call(elems) : length ? fn(elems[0], key) : emptyGet;
        Sensor();
        return IARETREPLACE;
    };
    var rcheckableType = /^(?:checkbox|radio)$/i;
    (function () {
        Sensor();
        var fragment = document.createDocumentFragment(), div = document.createElement('div'), input = document.createElement('input');
        div.setAttribute('className', 't');
        div.innerHTML = '  <link/><table></table><a href=\'/a\'>a</a>';
        support.leadingWhitespace = div.firstChild.nodeType === 3;
        support.tbody = !div.getElementsByTagName('tbody').length;
        support.htmlSerialize = !!div.getElementsByTagName('link').length;
        support.html5Clone = document.createElement('nav').cloneNode(true).outerHTML !== '<:nav></:nav>';
        input.type = 'checkbox';
        input.checked = true;
        fragment.appendChild(input);
        support.appendChecked = input.checked;
        div.innerHTML = '<textarea>x</textarea>';
        support.noCloneChecked = !!div.cloneNode(true).lastChild.defaultValue;
        fragment.appendChild(div);
        div.innerHTML = '<input type=\'radio\' checked=\'checked\' name=\'t\'/>';
        support.checkClone = div.cloneNode(true).cloneNode(true).lastChild.checked;
        support.noCloneEvent = true;
        if (div.attachEvent) {
            div.attachEvent('onclick', function () {
                Sensor();
                support.noCloneEvent = false;
                Sensor();
            });
            div.cloneNode(true).click();
        }
        if (support.deleteExpando == null) {
            support.deleteExpando = true;
            try {
                delete div.test;
            } catch (e) {
                support.deleteExpando = false;
            }
        }
        fragment = div = input = null;
        Sensor();
    }());
    (function () {
        Sensor();
        var i, eventName, div = document.createElement('div');
        for (i in {
                submit: true,
                change: true,
                focusin: true
            }) {
            eventName = 'on' + i;
            if (!(support[i + 'Bubbles'] = eventName in window)) {
                div.setAttribute(eventName, 't');
                support[i + 'Bubbles'] = div.attributes[eventName].expando === false;
            }
        }
        div = null;
        Sensor();
    }());
    var rformElems = /^(?:input|select|textarea)$/i, rkeyEvent = /^key/, rmouseEvent = /^(?:mouse|contextmenu)|click/, rfocusMorph = /^(?:focusinfocus|focusoutblur)$/, rtypenamespace = /^([^.]*)(?:\.(.+)|)$/;
    function returnTrue() {
        var IARETREPLACE = true;
        Sensor();
        return IARETREPLACE;
    }
    function returnFalse() {
        var IARETREPLACE = false;
        Sensor();
        return IARETREPLACE;
    }
    function safeActiveElement() {
        Sensor();
        try {
            var IARETREPLACE = document.activeElement;
            Sensor();
            return IARETREPLACE;
        } catch (err) {
        }
        Sensor();
    }
    jQuery.event = {
        global: {},
        add: function (elem, types, handler, data, selector) {
            Sensor();
            var tmp, events, t, handleObjIn, special, eventHandle, handleObj, handlers, type, namespaces, origType, elemData = jQuery._data(elem);
            if (!elemData) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            if (handler.handler) {
                handleObjIn = handler;
                handler = handleObjIn.handler;
                selector = handleObjIn.selector;
            }
            if (!handler.guid) {
                handler.guid = jQuery.guid++;
            }
            if (!(events = elemData.events)) {
                events = elemData.events = {};
            }
            if (!(eventHandle = elemData.handle)) {
                eventHandle = elemData.handle = function (e) {
                    var IARETREPLACE = typeof jQuery !== strundefined && (!e || jQuery.event.triggered !== e.type) ? jQuery.event.dispatch.apply(eventHandle.elem, arguments) : undefined;
                    Sensor();
                    return IARETREPLACE;
                };
                eventHandle.elem = elem;
            }
            types = (types || '').match(rnotwhite) || [''];
            t = types.length;
            while (t--) {
                tmp = rtypenamespace.exec(types[t]) || [];
                type = origType = tmp[1];
                namespaces = (tmp[2] || '').split('.').sort();
                if (!type) {
                    continue;
                }
                special = jQuery.event.special[type] || {};
                type = (selector ? special.delegateType : special.bindType) || type;
                special = jQuery.event.special[type] || {};
                handleObj = jQuery.extend({
                    type: type,
                    origType: origType,
                    data: data,
                    handler: handler,
                    guid: handler.guid,
                    selector: selector,
                    needsContext: selector && jQuery.expr.match.needsContext.test(selector),
                    namespace: namespaces.join('.')
                }, handleObjIn);
                if (!(handlers = events[type])) {
                    handlers = events[type] = [];
                    handlers.delegateCount = 0;
                    if (!special.setup || special.setup.call(elem, data, namespaces, eventHandle) === false) {
                        if (elem.addEventListener) {
                            elem.addEventListener(type, eventHandle, false);
                        } else {
                            if (elem.attachEvent) {
                                elem.attachEvent('on' + type, eventHandle);
                            }
                        }
                    }
                }
                if (special.add) {
                    special.add.call(elem, handleObj);
                    if (!handleObj.handler.guid) {
                        handleObj.handler.guid = handler.guid;
                    }
                }
                if (selector) {
                    handlers.splice(handlers.delegateCount++, 0, handleObj);
                } else {
                    handlers.push(handleObj);
                }
                jQuery.event.global[type] = true;
            }
            elem = null;
            Sensor();
        },
        remove: function (elem, types, handler, selector, mappedTypes) {
            Sensor();
            var j, handleObj, tmp, origCount, t, events, special, handlers, type, namespaces, origType, elemData = jQuery.hasData(elem) && jQuery._data(elem);
            if (!elemData || !(events = elemData.events)) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            types = (types || '').match(rnotwhite) || [''];
            t = types.length;
            while (t--) {
                tmp = rtypenamespace.exec(types[t]) || [];
                type = origType = tmp[1];
                namespaces = (tmp[2] || '').split('.').sort();
                if (!type) {
                    for (type in events) {
                        jQuery.event.remove(elem, type + types[t], handler, selector, true);
                    }
                    continue;
                }
                special = jQuery.event.special[type] || {};
                type = (selector ? special.delegateType : special.bindType) || type;
                handlers = events[type] || [];
                tmp = tmp[2] && new RegExp('(^|\\.)' + namespaces.join('\\.(?:.*\\.|)') + '(\\.|$)');
                origCount = j = handlers.length;
                while (j--) {
                    handleObj = handlers[j];
                    if ((mappedTypes || origType === handleObj.origType) && (!handler || handler.guid === handleObj.guid) && (!tmp || tmp.test(handleObj.namespace)) && (!selector || selector === handleObj.selector || selector === '**' && handleObj.selector)) {
                        handlers.splice(j, 1);
                        if (handleObj.selector) {
                            handlers.delegateCount--;
                        }
                        if (special.remove) {
                            special.remove.call(elem, handleObj);
                        }
                    }
                }
                if (origCount && !handlers.length) {
                    if (!special.teardown || special.teardown.call(elem, namespaces, elemData.handle) === false) {
                        jQuery.removeEvent(elem, type, elemData.handle);
                    }
                    delete events[type];
                }
            }
            if (jQuery.isEmptyObject(events)) {
                delete elemData.handle;
                jQuery._removeData(elem, 'events');
            }
            Sensor();
        },
        trigger: function (event, data, elem, onlyHandlers) {
            Sensor();
            var handle, ontype, cur, bubbleType, special, tmp, i, eventPath = [elem || document], type = hasOwn.call(event, 'type') ? event.type : event, namespaces = hasOwn.call(event, 'namespace') ? event.namespace.split('.') : [];
            cur = tmp = elem = elem || document;
            if (elem.nodeType === 3 || elem.nodeType === 8) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            if (rfocusMorph.test(type + jQuery.event.triggered)) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            if (type.indexOf('.') >= 0) {
                namespaces = type.split('.');
                type = namespaces.shift();
                namespaces.sort();
            }
            ontype = type.indexOf(':') < 0 && 'on' + type;
            event = event[jQuery.expando] ? event : new jQuery.Event(type, typeof event === 'object' && event);
            event.isTrigger = onlyHandlers ? 2 : 3;
            event.namespace = namespaces.join('.');
            event.namespace_re = event.namespace ? new RegExp('(^|\\.)' + namespaces.join('\\.(?:.*\\.|)') + '(\\.|$)') : null;
            event.result = undefined;
            if (!event.target) {
                event.target = elem;
            }
            data = data == null ? [event] : jQuery.makeArray(data, [event]);
            special = jQuery.event.special[type] || {};
            if (!onlyHandlers && special.trigger && special.trigger.apply(elem, data) === false) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            if (!onlyHandlers && !special.noBubble && !jQuery.isWindow(elem)) {
                bubbleType = special.delegateType || type;
                if (!rfocusMorph.test(bubbleType + type)) {
                    cur = cur.parentNode;
                }
                for (; cur; cur = cur.parentNode) {
                    eventPath.push(cur);
                    tmp = cur;
                }
                if (tmp === (elem.ownerDocument || document)) {
                    eventPath.push(tmp.defaultView || tmp.parentWindow || window);
                }
            }
            i = 0;
            while ((cur = eventPath[i++]) && !event.isPropagationStopped()) {
                event.type = i > 1 ? bubbleType : special.bindType || type;
                handle = (jQuery._data(cur, 'events') || {})[event.type] && jQuery._data(cur, 'handle');
                if (handle) {
                    handle.apply(cur, data);
                }
                handle = ontype && cur[ontype];
                if (handle && handle.apply && jQuery.acceptData(cur)) {
                    event.result = handle.apply(cur, data);
                    if (event.result === false) {
                        event.preventDefault();
                    }
                }
            }
            event.type = type;
            if (!onlyHandlers && !event.isDefaultPrevented()) {
                if ((!special._default || special._default.apply(eventPath.pop(), data) === false) && jQuery.acceptData(elem)) {
                    if (ontype && elem[type] && !jQuery.isWindow(elem)) {
                        tmp = elem[ontype];
                        if (tmp) {
                            elem[ontype] = null;
                        }
                        jQuery.event.triggered = type;
                        try {
                            elem[type]();
                        } catch (e) {
                        }
                        jQuery.event.triggered = undefined;
                        if (tmp) {
                            elem[ontype] = tmp;
                        }
                    }
                }
            }
            var IARETREPLACE = event.result;
            Sensor();
            return IARETREPLACE;
        },
        dispatch: function (event) {
            Sensor();
            event = jQuery.event.fix(event);
            var i, ret, handleObj, matched, j, handlerQueue = [], args = slice.call(arguments), handlers = (jQuery._data(this, 'events') || {})[event.type] || [], special = jQuery.event.special[event.type] || {};
            args[0] = event;
            event.delegateTarget = this;
            if (special.preDispatch && special.preDispatch.call(this, event) === false) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            handlerQueue = jQuery.event.handlers.call(this, event, handlers);
            i = 0;
            while ((matched = handlerQueue[i++]) && !event.isPropagationStopped()) {
                event.currentTarget = matched.elem;
                j = 0;
                while ((handleObj = matched.handlers[j++]) && !event.isImmediatePropagationStopped()) {
                    if (!event.namespace_re || event.namespace_re.test(handleObj.namespace)) {
                        event.handleObj = handleObj;
                        event.data = handleObj.data;
                        ret = ((jQuery.event.special[handleObj.origType] || {}).handle || handleObj.handler).apply(matched.elem, args);
                        if (ret !== undefined) {
                            if ((event.result = ret) === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                        }
                    }
                }
            }
            if (special.postDispatch) {
                special.postDispatch.call(this, event);
            }
            var IARETREPLACE = event.result;
            Sensor();
            return IARETREPLACE;
        },
        handlers: function (event, handlers) {
            Sensor();
            var sel, handleObj, matches, i, handlerQueue = [], delegateCount = handlers.delegateCount, cur = event.target;
            if (delegateCount && cur.nodeType && (!event.button || event.type !== 'click')) {
                for (; cur != this; cur = cur.parentNode || this) {
                    if (cur.nodeType === 1 && (cur.disabled !== true || event.type !== 'click')) {
                        matches = [];
                        for (i = 0; i < delegateCount; i++) {
                            handleObj = handlers[i];
                            sel = handleObj.selector + ' ';
                            if (matches[sel] === undefined) {
                                matches[sel] = handleObj.needsContext ? jQuery(sel, this).index(cur) >= 0 : jQuery.find(sel, this, null, [cur]).length;
                            }
                            if (matches[sel]) {
                                matches.push(handleObj);
                            }
                        }
                        if (matches.length) {
                            handlerQueue.push({
                                elem: cur,
                                handlers: matches
                            });
                        }
                    }
                }
            }
            if (delegateCount < handlers.length) {
                handlerQueue.push({
                    elem: this,
                    handlers: handlers.slice(delegateCount)
                });
            }
            var IARETREPLACE = handlerQueue;
            Sensor();
            return IARETREPLACE;
        },
        fix: function (event) {
            Sensor();
            if (event[jQuery.expando]) {
                var IARETREPLACE = event;
                Sensor();
                return IARETREPLACE;
            }
            var i, prop, copy, type = event.type, originalEvent = event, fixHook = this.fixHooks[type];
            if (!fixHook) {
                this.fixHooks[type] = fixHook = rmouseEvent.test(type) ? this.mouseHooks : rkeyEvent.test(type) ? this.keyHooks : {};
            }
            copy = fixHook.props ? this.props.concat(fixHook.props) : this.props;
            event = new jQuery.Event(originalEvent);
            i = copy.length;
            while (i--) {
                prop = copy[i];
                event[prop] = originalEvent[prop];
            }
            if (!event.target) {
                event.target = originalEvent.srcElement || document;
            }
            if (event.target.nodeType === 3) {
                event.target = event.target.parentNode;
            }
            event.metaKey = !!event.metaKey;
            var IARETREPLACE = fixHook.filter ? fixHook.filter(event, originalEvent) : event;
            Sensor();
            return IARETREPLACE;
        },
        props: 'altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which'.split(' '),
        fixHooks: {},
        keyHooks: {
            props: 'char charCode key keyCode'.split(' '),
            filter: function (event, original) {
                Sensor();
                if (event.which == null) {
                    event.which = original.charCode != null ? original.charCode : original.keyCode;
                }
                var IARETREPLACE = event;
                Sensor();
                return IARETREPLACE;
            }
        },
        mouseHooks: {
            props: 'button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement'.split(' '),
            filter: function (event, original) {
                Sensor();
                var body, eventDoc, doc, button = original.button, fromElement = original.fromElement;
                if (event.pageX == null && original.clientX != null) {
                    eventDoc = event.target.ownerDocument || document;
                    doc = eventDoc.documentElement;
                    body = eventDoc.body;
                    event.pageX = original.clientX + (doc && doc.scrollLeft || body && body.scrollLeft || 0) - (doc && doc.clientLeft || body && body.clientLeft || 0);
                    event.pageY = original.clientY + (doc && doc.scrollTop || body && body.scrollTop || 0) - (doc && doc.clientTop || body && body.clientTop || 0);
                }
                if (!event.relatedTarget && fromElement) {
                    event.relatedTarget = fromElement === event.target ? original.toElement : fromElement;
                }
                if (!event.which && button !== undefined) {
                    event.which = button & 1 ? 1 : button & 2 ? 3 : button & 4 ? 2 : 0;
                }
                var IARETREPLACE = event;
                Sensor();
                return IARETREPLACE;
            }
        },
        special: {
            load: { noBubble: true },
            focus: {
                trigger: function () {
                    Sensor();
                    if (this !== safeActiveElement() && this.focus) {
                        try {
                            this.focus();
                            var IARETREPLACE = false;
                            Sensor();
                            return IARETREPLACE;
                        } catch (e) {
                        }
                    }
                    Sensor();
                },
                delegateType: 'focusin'
            },
            blur: {
                trigger: function () {
                    Sensor();
                    if (this === safeActiveElement() && this.blur) {
                        this.blur();
                        var IARETREPLACE = false;
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                },
                delegateType: 'focusout'
            },
            click: {
                trigger: function () {
                    Sensor();
                    if (jQuery.nodeName(this, 'input') && this.type === 'checkbox' && this.click) {
                        this.click();
                        var IARETREPLACE = false;
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                },
                _default: function (event) {
                    var IARETREPLACE = jQuery.nodeName(event.target, 'a');
                    Sensor();
                    return IARETREPLACE;
                }
            },
            beforeunload: {
                postDispatch: function (event) {
                    Sensor();
                    if (event.result !== undefined) {
                        event.originalEvent.returnValue = event.result;
                    }
                    Sensor();
                }
            }
        },
        simulate: function (type, elem, event, bubble) {
            Sensor();
            var e = jQuery.extend(new jQuery.Event(), event, {
                type: type,
                isSimulated: true,
                originalEvent: {}
            });
            if (bubble) {
                jQuery.event.trigger(e, null, elem);
            } else {
                jQuery.event.dispatch.call(elem, e);
            }
            if (e.isDefaultPrevented()) {
                event.preventDefault();
            }
            Sensor();
        }
    };
    jQuery.removeEvent = document.removeEventListener ? function (elem, type, handle) {
        Sensor();
        if (elem.removeEventListener) {
            elem.removeEventListener(type, handle, false);
        }
        Sensor();
    } : function (elem, type, handle) {
        Sensor();
        var name = 'on' + type;
        if (elem.detachEvent) {
            if (typeof elem[name] === strundefined) {
                elem[name] = null;
            }
            elem.detachEvent(name, handle);
        }
        Sensor();
    };
    jQuery.Event = function (src, props) {
        Sensor();
        if (!(this instanceof jQuery.Event)) {
            var IARETREPLACE = new jQuery.Event(src, props);
            Sensor();
            return IARETREPLACE;
        }
        if (src && src.type) {
            this.originalEvent = src;
            this.type = src.type;
            this.isDefaultPrevented = src.defaultPrevented || src.defaultPrevented === undefined && (src.returnValue === false || src.getPreventDefault && src.getPreventDefault()) ? returnTrue : returnFalse;
        } else {
            this.type = src;
        }
        if (props) {
            jQuery.extend(this, props);
        }
        this.timeStamp = src && src.timeStamp || jQuery.now();
        this[jQuery.expando] = true;
        Sensor();
    };
    jQuery.Event.prototype = {
        isDefaultPrevented: returnFalse,
        isPropagationStopped: returnFalse,
        isImmediatePropagationStopped: returnFalse,
        preventDefault: function () {
            Sensor();
            var e = this.originalEvent;
            this.isDefaultPrevented = returnTrue;
            if (!e) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            if (e.preventDefault) {
                e.preventDefault();
            } else {
                e.returnValue = false;
            }
            Sensor();
        },
        stopPropagation: function () {
            Sensor();
            var e = this.originalEvent;
            this.isPropagationStopped = returnTrue;
            if (!e) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            if (e.stopPropagation) {
                e.stopPropagation();
            }
            e.cancelBubble = true;
            Sensor();
        },
        stopImmediatePropagation: function () {
            Sensor();
            this.isImmediatePropagationStopped = returnTrue;
            this.stopPropagation();
            Sensor();
        }
    };
    jQuery.each({
        mouseenter: 'mouseover',
        mouseleave: 'mouseout'
    }, function (orig, fix) {
        Sensor();
        jQuery.event.special[orig] = {
            delegateType: fix,
            bindType: fix,
            handle: function (event) {
                Sensor();
                var ret, target = this, related = event.relatedTarget, handleObj = event.handleObj;
                if (!related || related !== target && !jQuery.contains(target, related)) {
                    event.type = handleObj.origType;
                    ret = handleObj.handler.apply(this, arguments);
                    event.type = fix;
                }
                var IARETREPLACE = ret;
                Sensor();
                return IARETREPLACE;
            }
        };
        Sensor();
    });
    if (!support.submitBubbles) {
        jQuery.event.special.submit = {
            setup: function () {
                Sensor();
                if (jQuery.nodeName(this, 'form')) {
                    var IARETREPLACE = false;
                    Sensor();
                    return IARETREPLACE;
                }
                jQuery.event.add(this, 'click._submit keypress._submit', function (e) {
                    Sensor();
                    var elem = e.target, form = jQuery.nodeName(elem, 'input') || jQuery.nodeName(elem, 'button') ? elem.form : undefined;
                    if (form && !jQuery._data(form, 'submitBubbles')) {
                        jQuery.event.add(form, 'submit._submit', function (event) {
                            Sensor();
                            event._submit_bubble = true;
                            Sensor();
                        });
                        jQuery._data(form, 'submitBubbles', true);
                    }
                    Sensor();
                });
                Sensor();
            },
            postDispatch: function (event) {
                Sensor();
                if (event._submit_bubble) {
                    delete event._submit_bubble;
                    if (this.parentNode && !event.isTrigger) {
                        jQuery.event.simulate('submit', this.parentNode, event, true);
                    }
                }
                Sensor();
            },
            teardown: function () {
                Sensor();
                if (jQuery.nodeName(this, 'form')) {
                    var IARETREPLACE = false;
                    Sensor();
                    return IARETREPLACE;
                }
                jQuery.event.remove(this, '._submit');
                Sensor();
            }
        };
    }
    if (!support.changeBubbles) {
        jQuery.event.special.change = {
            setup: function () {
                Sensor();
                if (rformElems.test(this.nodeName)) {
                    if (this.type === 'checkbox' || this.type === 'radio') {
                        jQuery.event.add(this, 'propertychange._change', function (event) {
                            Sensor();
                            if (event.originalEvent.propertyName === 'checked') {
                                this._just_changed = true;
                            }
                            Sensor();
                        });
                        jQuery.event.add(this, 'click._change', function (event) {
                            Sensor();
                            if (this._just_changed && !event.isTrigger) {
                                this._just_changed = false;
                            }
                            jQuery.event.simulate('change', this, event, true);
                            Sensor();
                        });
                    }
                    var IARETREPLACE = false;
                    Sensor();
                    return IARETREPLACE;
                }
                jQuery.event.add(this, 'beforeactivate._change', function (e) {
                    Sensor();
                    var elem = e.target;
                    if (rformElems.test(elem.nodeName) && !jQuery._data(elem, 'changeBubbles')) {
                        jQuery.event.add(elem, 'change._change', function (event) {
                            Sensor();
                            if (this.parentNode && !event.isSimulated && !event.isTrigger) {
                                jQuery.event.simulate('change', this.parentNode, event, true);
                            }
                            Sensor();
                        });
                        jQuery._data(elem, 'changeBubbles', true);
                    }
                    Sensor();
                });
                Sensor();
            },
            handle: function (event) {
                Sensor();
                var elem = event.target;
                if (this !== elem || event.isSimulated || event.isTrigger || elem.type !== 'radio' && elem.type !== 'checkbox') {
                    var IARETREPLACE = event.handleObj.handler.apply(this, arguments);
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            },
            teardown: function () {
                Sensor();
                jQuery.event.remove(this, '._change');
                var IARETREPLACE = !rformElems.test(this.nodeName);
                Sensor();
                return IARETREPLACE;
            }
        };
    }
    if (!support.focusinBubbles) {
        jQuery.each({
            focus: 'focusin',
            blur: 'focusout'
        }, function (orig, fix) {
            Sensor();
            var handler = function (event) {
                Sensor();
                jQuery.event.simulate(fix, event.target, jQuery.event.fix(event), true);
                Sensor();
            };
            jQuery.event.special[fix] = {
                setup: function () {
                    Sensor();
                    var doc = this.ownerDocument || this, attaches = jQuery._data(doc, fix);
                    if (!attaches) {
                        doc.addEventListener(orig, handler, true);
                    }
                    jQuery._data(doc, fix, (attaches || 0) + 1);
                    Sensor();
                },
                teardown: function () {
                    Sensor();
                    var doc = this.ownerDocument || this, attaches = jQuery._data(doc, fix) - 1;
                    if (!attaches) {
                        doc.removeEventListener(orig, handler, true);
                        jQuery._removeData(doc, fix);
                    } else {
                        jQuery._data(doc, fix, attaches);
                    }
                    Sensor();
                }
            };
            Sensor();
        });
    }
    jQuery.fn.extend({
        on: function (types, selector, data, fn, one) {
            Sensor();
            var type, origFn;
            if (typeof types === 'object') {
                if (typeof selector !== 'string') {
                    data = data || selector;
                    selector = undefined;
                }
                for (type in types) {
                    this.on(type, selector, data, types[type], one);
                }
                var IARETREPLACE = this;
                Sensor();
                return IARETREPLACE;
            }
            if (data == null && fn == null) {
                fn = selector;
                data = selector = undefined;
            } else {
                if (fn == null) {
                    if (typeof selector === 'string') {
                        fn = data;
                        data = undefined;
                    } else {
                        fn = data;
                        data = selector;
                        selector = undefined;
                    }
                }
            }
            if (fn === false) {
                fn = returnFalse;
            } else {
                if (!fn) {
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                }
            }
            if (one === 1) {
                origFn = fn;
                fn = function (event) {
                    Sensor();
                    jQuery().off(event);
                    var IARETREPLACE = origFn.apply(this, arguments);
                    Sensor();
                    return IARETREPLACE;
                };
                fn.guid = origFn.guid || (origFn.guid = jQuery.guid++);
            }
            var IARETREPLACE = this.each(function () {
                Sensor();
                jQuery.event.add(this, types, fn, data, selector);
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        one: function (types, selector, data, fn) {
            var IARETREPLACE = this.on(types, selector, data, fn, 1);
            Sensor();
            return IARETREPLACE;
        },
        off: function (types, selector, fn) {
            Sensor();
            var handleObj, type;
            if (types && types.preventDefault && types.handleObj) {
                handleObj = types.handleObj;
                jQuery(types.delegateTarget).off(handleObj.namespace ? handleObj.origType + '.' + handleObj.namespace : handleObj.origType, handleObj.selector, handleObj.handler);
                var IARETREPLACE = this;
                Sensor();
                return IARETREPLACE;
            }
            if (typeof types === 'object') {
                for (type in types) {
                    this.off(type, selector, types[type]);
                }
                var IARETREPLACE = this;
                Sensor();
                return IARETREPLACE;
            }
            if (selector === false || typeof selector === 'function') {
                fn = selector;
                selector = undefined;
            }
            if (fn === false) {
                fn = returnFalse;
            }
            var IARETREPLACE = this.each(function () {
                Sensor();
                jQuery.event.remove(this, types, fn, selector);
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        trigger: function (type, data) {
            var IARETREPLACE = this.each(function () {
                Sensor();
                jQuery.event.trigger(type, data, this);
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        triggerHandler: function (type, data) {
            Sensor();
            var elem = this[0];
            if (elem) {
                var IARETREPLACE = jQuery.event.trigger(type, data, elem, true);
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
        }
    });
    function createSafeFragment(document) {
        Sensor();
        var list = nodeNames.split('|'), safeFrag = document.createDocumentFragment();
        if (safeFrag.createElement) {
            while (list.length) {
                safeFrag.createElement(list.pop());
            }
        }
        var IARETREPLACE = safeFrag;
        Sensor();
        return IARETREPLACE;
    }
    var nodeNames = 'abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|' + 'header|hgroup|mark|meter|nav|output|progress|section|summary|time|video', rinlinejQuery = / jQuery\d+="(?:null|\d+)"/g, rnoshimcache = new RegExp('<(?:' + nodeNames + ')[\\s/>]', 'i'), rleadingWhitespace = /^\s+/, rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, rtagName = /<([\w:]+)/, rtbody = /<tbody/i, rhtml = /<|&#?\w+;/, rnoInnerhtml = /<(?:script|style|link)/i, rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i, rscriptType = /^$|\/(?:java|ecma)script/i, rscriptTypeMasked = /^true\/(.*)/, rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, wrapMap = {
            option: [
                1,
                '<select multiple=\'multiple\'>',
                '</select>'
            ],
            legend: [
                1,
                '<fieldset>',
                '</fieldset>'
            ],
            area: [
                1,
                '<map>',
                '</map>'
            ],
            param: [
                1,
                '<object>',
                '</object>'
            ],
            thead: [
                1,
                '<table>',
                '</table>'
            ],
            tr: [
                2,
                '<table><tbody>',
                '</tbody></table>'
            ],
            col: [
                2,
                '<table><tbody></tbody><colgroup>',
                '</colgroup></table>'
            ],
            td: [
                3,
                '<table><tbody><tr>',
                '</tr></tbody></table>'
            ],
            _default: support.htmlSerialize ? [
                0,
                '',
                ''
            ] : [
                1,
                'X<div>',
                '</div>'
            ]
        }, safeFragment = createSafeFragment(document), fragmentDiv = safeFragment.appendChild(document.createElement('div'));
    wrapMap.optgroup = wrapMap.option;
    wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
    wrapMap.th = wrapMap.td;
    function getAll(context, tag) {
        Sensor();
        var elems, elem, i = 0, found = typeof context.getElementsByTagName !== strundefined ? context.getElementsByTagName(tag || '*') : typeof context.querySelectorAll !== strundefined ? context.querySelectorAll(tag || '*') : undefined;
        if (!found) {
            for (found = [], elems = context.childNodes || context; (elem = elems[i]) != null; i++) {
                if (!tag || jQuery.nodeName(elem, tag)) {
                    found.push(elem);
                } else {
                    jQuery.merge(found, getAll(elem, tag));
                }
            }
        }
        var IARETREPLACE = tag === undefined || tag && jQuery.nodeName(context, tag) ? jQuery.merge([context], found) : found;
        Sensor();
        return IARETREPLACE;
    }
    function fixDefaultChecked(elem) {
        Sensor();
        if (rcheckableType.test(elem.type)) {
            elem.defaultChecked = elem.checked;
        }
        Sensor();
    }
    function manipulationTarget(elem, content) {
        var IARETREPLACE = jQuery.nodeName(elem, 'table') && jQuery.nodeName(content.nodeType !== 11 ? content : content.firstChild, 'tr') ? elem.getElementsByTagName('tbody')[0] || elem.appendChild(elem.ownerDocument.createElement('tbody')) : elem;
        Sensor();
        return IARETREPLACE;
    }
    function disableScript(elem) {
        Sensor();
        elem.type = (jQuery.find.attr(elem, 'type') !== null) + '/' + elem.type;
        var IARETREPLACE = elem;
        Sensor();
        return IARETREPLACE;
    }
    function restoreScript(elem) {
        Sensor();
        var match = rscriptTypeMasked.exec(elem.type);
        if (match) {
            elem.type = match[1];
        } else {
            elem.removeAttribute('type');
        }
        var IARETREPLACE = elem;
        Sensor();
        return IARETREPLACE;
    }
    function setGlobalEval(elems, refElements) {
        Sensor();
        var elem, i = 0;
        for (; (elem = elems[i]) != null; i++) {
            jQuery._data(elem, 'globalEval', !refElements || jQuery._data(refElements[i], 'globalEval'));
        }
        Sensor();
    }
    function cloneCopyEvent(src, dest) {
        Sensor();
        if (dest.nodeType !== 1 || !jQuery.hasData(src)) {
            var IARETREPLACE;
            Sensor();
            return IARETREPLACE;
        }
        var type, i, l, oldData = jQuery._data(src), curData = jQuery._data(dest, oldData), events = oldData.events;
        if (events) {
            delete curData.handle;
            curData.events = {};
            for (type in events) {
                for (i = 0, l = events[type].length; i < l; i++) {
                    jQuery.event.add(dest, type, events[type][i]);
                }
            }
        }
        if (curData.data) {
            curData.data = jQuery.extend({}, curData.data);
        }
        Sensor();
    }
    function fixCloneNodeIssues(src, dest) {
        Sensor();
        var nodeName, e, data;
        if (dest.nodeType !== 1) {
            var IARETREPLACE;
            Sensor();
            return IARETREPLACE;
        }
        nodeName = dest.nodeName.toLowerCase();
        if (!support.noCloneEvent && dest[jQuery.expando]) {
            data = jQuery._data(dest);
            for (e in data.events) {
                jQuery.removeEvent(dest, e, data.handle);
            }
            dest.removeAttribute(jQuery.expando);
        }
        if (nodeName === 'script' && dest.text !== src.text) {
            disableScript(dest).text = src.text;
            restoreScript(dest);
        } else {
            if (nodeName === 'object') {
                if (dest.parentNode) {
                    dest.outerHTML = src.outerHTML;
                }
                if (support.html5Clone && (src.innerHTML && !jQuery.trim(dest.innerHTML))) {
                    dest.innerHTML = src.innerHTML;
                }
            } else {
                if (nodeName === 'input' && rcheckableType.test(src.type)) {
                    dest.defaultChecked = dest.checked = src.checked;
                    if (dest.value !== src.value) {
                        dest.value = src.value;
                    }
                } else {
                    if (nodeName === 'option') {
                        dest.defaultSelected = dest.selected = src.defaultSelected;
                    } else {
                        if (nodeName === 'input' || nodeName === 'textarea') {
                            dest.defaultValue = src.defaultValue;
                        }
                    }
                }
            }
        }
        Sensor();
    }
    jQuery.extend({
        clone: function (elem, dataAndEvents, deepDataAndEvents) {
            Sensor();
            var destElements, node, clone, i, srcElements, inPage = jQuery.contains(elem.ownerDocument, elem);
            if (support.html5Clone || jQuery.isXMLDoc(elem) || !rnoshimcache.test('<' + elem.nodeName + '>')) {
                clone = elem.cloneNode(true);
            } else {
                fragmentDiv.innerHTML = elem.outerHTML;
                fragmentDiv.removeChild(clone = fragmentDiv.firstChild);
            }
            if ((!support.noCloneEvent || !support.noCloneChecked) && (elem.nodeType === 1 || elem.nodeType === 11) && !jQuery.isXMLDoc(elem)) {
                destElements = getAll(clone);
                srcElements = getAll(elem);
                for (i = 0; (node = srcElements[i]) != null; ++i) {
                    if (destElements[i]) {
                        fixCloneNodeIssues(node, destElements[i]);
                    }
                }
            }
            if (dataAndEvents) {
                if (deepDataAndEvents) {
                    srcElements = srcElements || getAll(elem);
                    destElements = destElements || getAll(clone);
                    for (i = 0; (node = srcElements[i]) != null; i++) {
                        cloneCopyEvent(node, destElements[i]);
                    }
                } else {
                    cloneCopyEvent(elem, clone);
                }
            }
            destElements = getAll(clone, 'script');
            if (destElements.length > 0) {
                setGlobalEval(destElements, !inPage && getAll(elem, 'script'));
            }
            destElements = srcElements = node = null;
            var IARETREPLACE = clone;
            Sensor();
            return IARETREPLACE;
        },
        buildFragment: function (elems, context, scripts, selection) {
            Sensor();
            var j, elem, contains, tmp, tag, tbody, wrap, l = elems.length, safe = createSafeFragment(context), nodes = [], i = 0;
            for (; i < l; i++) {
                elem = elems[i];
                if (elem || elem === 0) {
                    if (jQuery.type(elem) === 'object') {
                        jQuery.merge(nodes, elem.nodeType ? [elem] : elem);
                    } else {
                        if (!rhtml.test(elem)) {
                            nodes.push(context.createTextNode(elem));
                        } else {
                            tmp = tmp || safe.appendChild(context.createElement('div'));
                            tag = (rtagName.exec(elem) || [
                                '',
                                ''
                            ])[1].toLowerCase();
                            wrap = wrapMap[tag] || wrapMap._default;
                            tmp.innerHTML = wrap[1] + elem.replace(rxhtmlTag, '<$1></$2>') + wrap[2];
                            j = wrap[0];
                            while (j--) {
                                tmp = tmp.lastChild;
                            }
                            if (!support.leadingWhitespace && rleadingWhitespace.test(elem)) {
                                nodes.push(context.createTextNode(rleadingWhitespace.exec(elem)[0]));
                            }
                            if (!support.tbody) {
                                elem = tag === 'table' && !rtbody.test(elem) ? tmp.firstChild : wrap[1] === '<table>' && !rtbody.test(elem) ? tmp : 0;
                                j = elem && elem.childNodes.length;
                                while (j--) {
                                    if (jQuery.nodeName(tbody = elem.childNodes[j], 'tbody') && !tbody.childNodes.length) {
                                        elem.removeChild(tbody);
                                    }
                                }
                            }
                            jQuery.merge(nodes, tmp.childNodes);
                            tmp.textContent = '';
                            while (tmp.firstChild) {
                                tmp.removeChild(tmp.firstChild);
                            }
                            tmp = safe.lastChild;
                        }
                    }
                }
            }
            if (tmp) {
                safe.removeChild(tmp);
            }
            if (!support.appendChecked) {
                jQuery.grep(getAll(nodes, 'input'), fixDefaultChecked);
            }
            i = 0;
            while (elem = nodes[i++]) {
                if (selection && jQuery.inArray(elem, selection) !== -1) {
                    continue;
                }
                contains = jQuery.contains(elem.ownerDocument, elem);
                tmp = getAll(safe.appendChild(elem), 'script');
                if (contains) {
                    setGlobalEval(tmp);
                }
                if (scripts) {
                    j = 0;
                    while (elem = tmp[j++]) {
                        if (rscriptType.test(elem.type || '')) {
                            scripts.push(elem);
                        }
                    }
                }
            }
            tmp = null;
            var IARETREPLACE = safe;
            Sensor();
            return IARETREPLACE;
        },
        cleanData: function (elems, acceptData) {
            Sensor();
            var elem, type, id, data, i = 0, internalKey = jQuery.expando, cache = jQuery.cache, deleteExpando = support.deleteExpando, special = jQuery.event.special;
            for (; (elem = elems[i]) != null; i++) {
                if (acceptData || jQuery.acceptData(elem)) {
                    id = elem[internalKey];
                    data = id && cache[id];
                    if (data) {
                        if (data.events) {
                            for (type in data.events) {
                                if (special[type]) {
                                    jQuery.event.remove(elem, type);
                                } else {
                                    jQuery.removeEvent(elem, type, data.handle);
                                }
                            }
                        }
                        if (cache[id]) {
                            delete cache[id];
                            if (deleteExpando) {
                                delete elem[internalKey];
                            } else {
                                if (typeof elem.removeAttribute !== strundefined) {
                                    elem.removeAttribute(internalKey);
                                } else {
                                    elem[internalKey] = null;
                                }
                            }
                            deletedIds.push(id);
                        }
                    }
                }
            }
            Sensor();
        }
    });
    jQuery.fn.extend({
        text: function (value) {
            var IARETREPLACE = access(this, function (value) {
                var IARETREPLACE = value === undefined ? jQuery.text(this) : this.empty().append((this[0] && this[0].ownerDocument || document).createTextNode(value));
                Sensor();
                return IARETREPLACE;
            }, null, value, arguments.length);
            Sensor();
            return IARETREPLACE;
        },
        append: function () {
            var IARETREPLACE = this.domManip(arguments, function (elem) {
                Sensor();
                if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
                    var target = manipulationTarget(this, elem);
                    target.appendChild(elem);
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        prepend: function () {
            var IARETREPLACE = this.domManip(arguments, function (elem) {
                Sensor();
                if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
                    var target = manipulationTarget(this, elem);
                    target.insertBefore(elem, target.firstChild);
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        before: function () {
            var IARETREPLACE = this.domManip(arguments, function (elem) {
                Sensor();
                if (this.parentNode) {
                    this.parentNode.insertBefore(elem, this);
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        after: function () {
            var IARETREPLACE = this.domManip(arguments, function (elem) {
                Sensor();
                if (this.parentNode) {
                    this.parentNode.insertBefore(elem, this.nextSibling);
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        remove: function (selector, keepData) {
            Sensor();
            var elem, elems = selector ? jQuery.filter(selector, this) : this, i = 0;
            for (; (elem = elems[i]) != null; i++) {
                if (!keepData && elem.nodeType === 1) {
                    jQuery.cleanData(getAll(elem));
                }
                if (elem.parentNode) {
                    if (keepData && jQuery.contains(elem.ownerDocument, elem)) {
                        setGlobalEval(getAll(elem, 'script'));
                    }
                    elem.parentNode.removeChild(elem);
                }
            }
            var IARETREPLACE = this;
            Sensor();
            return IARETREPLACE;
        },
        empty: function () {
            Sensor();
            var elem, i = 0;
            for (; (elem = this[i]) != null; i++) {
                if (elem.nodeType === 1) {
                    jQuery.cleanData(getAll(elem, false));
                }
                while (elem.firstChild) {
                    elem.removeChild(elem.firstChild);
                }
                if (elem.options && jQuery.nodeName(elem, 'select')) {
                    elem.options.length = 0;
                }
            }
            var IARETREPLACE = this;
            Sensor();
            return IARETREPLACE;
        },
        clone: function (dataAndEvents, deepDataAndEvents) {
            Sensor();
            dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
            deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;
            var IARETREPLACE = this.map(function () {
                var IARETREPLACE = jQuery.clone(this, dataAndEvents, deepDataAndEvents);
                Sensor();
                return IARETREPLACE;
            });
            Sensor();
            return IARETREPLACE;
        },
        html: function (value) {
            var IARETREPLACE = access(this, function (value) {
                Sensor();
                var elem = this[0] || {}, i = 0, l = this.length;
                if (value === undefined) {
                    var IARETREPLACE = elem.nodeType === 1 ? elem.innerHTML.replace(rinlinejQuery, '') : undefined;
                    Sensor();
                    return IARETREPLACE;
                }
                if (typeof value === 'string' && !rnoInnerhtml.test(value) && (support.htmlSerialize || !rnoshimcache.test(value)) && (support.leadingWhitespace || !rleadingWhitespace.test(value)) && !wrapMap[(rtagName.exec(value) || [
                        '',
                        ''
                    ])[1].toLowerCase()]) {
                    value = value.replace(rxhtmlTag, '<$1></$2>');
                    try {
                        for (; i < l; i++) {
                            elem = this[i] || {};
                            if (elem.nodeType === 1) {
                                jQuery.cleanData(getAll(elem, false));
                                elem.innerHTML = value;
                            }
                        }
                        elem = 0;
                    } catch (e) {
                    }
                }
                if (elem) {
                    this.empty().append(value);
                }
                Sensor();
            }, null, value, arguments.length);
            Sensor();
            return IARETREPLACE;
        },
        replaceWith: function () {
            Sensor();
            var arg = arguments[0];
            this.domManip(arguments, function (elem) {
                Sensor();
                arg = this.parentNode;
                jQuery.cleanData(getAll(this));
                if (arg) {
                    arg.replaceChild(elem, this);
                }
                Sensor();
            });
            var IARETREPLACE = arg && (arg.length || arg.nodeType) ? this : this.remove();
            Sensor();
            return IARETREPLACE;
        },
        detach: function (selector) {
            var IARETREPLACE = this.remove(selector, true);
            Sensor();
            return IARETREPLACE;
        },
        domManip: function (args, callback) {
            Sensor();
            args = concat.apply([], args);
            var first, node, hasScripts, scripts, doc, fragment, i = 0, l = this.length, set = this, iNoClone = l - 1, value = args[0], isFunction = jQuery.isFunction(value);
            if (isFunction || l > 1 && typeof value === 'string' && !support.checkClone && rchecked.test(value)) {
                var IARETREPLACE = this.each(function (index) {
                    Sensor();
                    var self = set.eq(index);
                    if (isFunction) {
                        args[0] = value.call(this, index, self.html());
                    }
                    self.domManip(args, callback);
                    Sensor();
                });
                Sensor();
                return IARETREPLACE;
            }
            if (l) {
                fragment = jQuery.buildFragment(args, this[0].ownerDocument, false, this);
                first = fragment.firstChild;
                if (fragment.childNodes.length === 1) {
                    fragment = first;
                }
                if (first) {
                    scripts = jQuery.map(getAll(fragment, 'script'), disableScript);
                    hasScripts = scripts.length;
                    for (; i < l; i++) {
                        node = fragment;
                        if (i !== iNoClone) {
                            node = jQuery.clone(node, true, true);
                            if (hasScripts) {
                                jQuery.merge(scripts, getAll(node, 'script'));
                            }
                        }
                        callback.call(this[i], node, i);
                    }
                    if (hasScripts) {
                        doc = scripts[scripts.length - 1].ownerDocument;
                        jQuery.map(scripts, restoreScript);
                        for (i = 0; i < hasScripts; i++) {
                            node = scripts[i];
                            if (rscriptType.test(node.type || '') && !jQuery._data(node, 'globalEval') && jQuery.contains(doc, node)) {
                                if (node.src) {
                                    if (jQuery._evalUrl) {
                                        jQuery._evalUrl(node.src);
                                    }
                                } else {
                                    jQuery.globalEval((node.text || node.textContent || node.innerHTML || '').replace(rcleanScript, ''));
                                }
                            }
                        }
                    }
                    fragment = first = null;
                }
            }
            var IARETREPLACE = this;
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.each({
        appendTo: 'append',
        prependTo: 'prepend',
        insertBefore: 'before',
        insertAfter: 'after',
        replaceAll: 'replaceWith'
    }, function (name, original) {
        Sensor();
        jQuery.fn[name] = function (selector) {
            Sensor();
            var elems, i = 0, ret = [], insert = jQuery(selector), last = insert.length - 1;
            for (; i <= last; i++) {
                elems = i === last ? this : this.clone(true);
                jQuery(insert[i])[original](elems);
                push.apply(ret, elems.get());
            }
            var IARETREPLACE = this.pushStack(ret);
            Sensor();
            return IARETREPLACE;
        };
        Sensor();
    });
    var iframe, elemdisplay = {};
    function actualDisplay(name, doc) {
        Sensor();
        var elem = jQuery(doc.createElement(name)).appendTo(doc.body), display = window.getDefaultComputedStyle ? window.getDefaultComputedStyle(elem[0]).display : jQuery.css(elem[0], 'display');
        elem.detach();
        var IARETREPLACE = display;
        Sensor();
        return IARETREPLACE;
    }
    function defaultDisplay(nodeName) {
        Sensor();
        var doc = document, display = elemdisplay[nodeName];
        if (!display) {
            display = actualDisplay(nodeName, doc);
            if (display === 'none' || !display) {
                iframe = (iframe || jQuery('<iframe frameborder=\'0\' width=\'0\' height=\'0\'/>')).appendTo(doc.documentElement);
                doc = (iframe[0].contentWindow || iframe[0].contentDocument).document;
                doc.write();
                doc.close();
                display = actualDisplay(nodeName, doc);
                iframe.detach();
            }
            elemdisplay[nodeName] = display;
        }
        var IARETREPLACE = display;
        Sensor();
        return IARETREPLACE;
    }
    (function () {
        Sensor();
        var a, shrinkWrapBlocksVal, div = document.createElement('div'), divReset = '-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;' + 'display:block;padding:0;margin:0;border:0';
        div.innerHTML = '  <link/><table></table><a href=\'/a\'>a</a><input type=\'checkbox\'/>';
        a = div.getElementsByTagName('a')[0];
        a.style.cssText = 'float:left;opacity:.5';
        support.opacity = /^0.5/.test(a.style.opacity);
        support.cssFloat = !!a.style.cssFloat;
        div.style.backgroundClip = 'content-box';
        div.cloneNode(true).style.backgroundClip = '';
        support.clearCloneStyle = div.style.backgroundClip === 'content-box';
        a = div = null;
        support.shrinkWrapBlocks = function () {
            Sensor();
            var body, container, div, containerStyles;
            if (shrinkWrapBlocksVal == null) {
                body = document.getElementsByTagName('body')[0];
                if (!body) {
                    var IARETREPLACE;
                    Sensor();
                    return IARETREPLACE;
                }
                containerStyles = 'border:0;width:0;height:0;position:absolute;top:0;left:-9999px';
                container = document.createElement('div');
                div = document.createElement('div');
                body.appendChild(container).appendChild(div);
                shrinkWrapBlocksVal = false;
                if (typeof div.style.zoom !== strundefined) {
                    div.style.cssText = divReset + ';width:1px;padding:1px;zoom:1';
                    div.innerHTML = '<div></div>';
                    div.firstChild.style.width = '5px';
                    shrinkWrapBlocksVal = div.offsetWidth !== 3;
                }
                body.removeChild(container);
                body = container = div = null;
            }
            var IARETREPLACE = shrinkWrapBlocksVal;
            Sensor();
            return IARETREPLACE;
        };
        Sensor();
    }());
    var rmargin = /^margin/;
    var rnumnonpx = new RegExp('^(' + pnum + ')(?!px)[a-z%]+$', 'i');
    var getStyles, curCSS, rposition = /^(top|right|bottom|left)$/;
    if (window.getComputedStyle) {
        getStyles = function (elem) {
            var IARETREPLACE = elem.ownerDocument.defaultView.getComputedStyle(elem, null);
            Sensor();
            return IARETREPLACE;
        };
        curCSS = function (elem, name, computed) {
            Sensor();
            var width, minWidth, maxWidth, ret, style = elem.style;
            computed = computed || getStyles(elem);
            ret = computed ? computed.getPropertyValue(name) || computed[name] : undefined;
            if (computed) {
                if (ret === '' && !jQuery.contains(elem.ownerDocument, elem)) {
                    ret = jQuery.style(elem, name);
                }
                if (rnumnonpx.test(ret) && rmargin.test(name)) {
                    width = style.width;
                    minWidth = style.minWidth;
                    maxWidth = style.maxWidth;
                    style.minWidth = style.maxWidth = style.width = ret;
                    ret = computed.width;
                    style.width = width;
                    style.minWidth = minWidth;
                    style.maxWidth = maxWidth;
                }
            }
            var IARETREPLACE = ret === undefined ? ret : ret + '';
            Sensor();
            return IARETREPLACE;
        };
    } else {
        if (document.documentElement.currentStyle) {
            getStyles = function (elem) {
                var IARETREPLACE = elem.currentStyle;
                Sensor();
                return IARETREPLACE;
            };
            curCSS = function (elem, name, computed) {
                Sensor();
                var left, rs, rsLeft, ret, style = elem.style;
                computed = computed || getStyles(elem);
                ret = computed ? computed[name] : undefined;
                if (ret == null && style && style[name]) {
                    ret = style[name];
                }
                if (rnumnonpx.test(ret) && !rposition.test(name)) {
                    left = style.left;
                    rs = elem.runtimeStyle;
                    rsLeft = rs && rs.left;
                    if (rsLeft) {
                        rs.left = elem.currentStyle.left;
                    }
                    style.left = name === 'fontSize' ? '1em' : ret;
                    ret = style.pixelLeft + 'px';
                    style.left = left;
                    if (rsLeft) {
                        rs.left = rsLeft;
                    }
                }
                var IARETREPLACE = ret === undefined ? ret : ret + '' || 'auto';
                Sensor();
                return IARETREPLACE;
            };
        }
    }
    function addGetHookIf(conditionFn, hookFn) {
        var IARETREPLACE = {
            get: function () {
                Sensor();
                var condition = conditionFn();
                if (condition == null) {
                    var IARETREPLACE;
                    Sensor();
                    return IARETREPLACE;
                }
                if (condition) {
                    delete this.get;
                    var IARETREPLACE;
                    Sensor();
                    return IARETREPLACE;
                }
                var IARETREPLACE = (this.get = hookFn).apply(this, arguments);
                Sensor();
                return IARETREPLACE;
            }
        };
        Sensor();
        return IARETREPLACE;
    }
    (function () {
        Sensor();
        var a, reliableHiddenOffsetsVal, boxSizingVal, boxSizingReliableVal, pixelPositionVal, reliableMarginRightVal, div = document.createElement('div'), containerStyles = 'border:0;width:0;height:0;position:absolute;top:0;left:-9999px', divReset = '-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;' + 'display:block;padding:0;margin:0;border:0';
        div.innerHTML = '  <link/><table></table><a href=\'/a\'>a</a><input type=\'checkbox\'/>';
        a = div.getElementsByTagName('a')[0];
        a.style.cssText = 'float:left;opacity:.5';
        support.opacity = /^0.5/.test(a.style.opacity);
        support.cssFloat = !!a.style.cssFloat;
        div.style.backgroundClip = 'content-box';
        div.cloneNode(true).style.backgroundClip = '';
        support.clearCloneStyle = div.style.backgroundClip === 'content-box';
        a = div = null;
        jQuery.extend(support, {
            reliableHiddenOffsets: function () {
                Sensor();
                if (reliableHiddenOffsetsVal != null) {
                    var IARETREPLACE = reliableHiddenOffsetsVal;
                    Sensor();
                    return IARETREPLACE;
                }
                var container, tds, isSupported, div = document.createElement('div'), body = document.getElementsByTagName('body')[0];
                if (!body) {
                    var IARETREPLACE;
                    Sensor();
                    return IARETREPLACE;
                }
                div.setAttribute('className', 't');
                div.innerHTML = '  <link/><table></table><a href=\'/a\'>a</a><input type=\'checkbox\'/>';
                container = document.createElement('div');
                container.style.cssText = containerStyles;
                body.appendChild(container).appendChild(div);
                div.innerHTML = '<table><tr><td></td><td>t</td></tr></table>';
                tds = div.getElementsByTagName('td');
                tds[0].style.cssText = 'padding:0;margin:0;border:0;display:none';
                isSupported = tds[0].offsetHeight === 0;
                tds[0].style.display = '';
                tds[1].style.display = 'none';
                reliableHiddenOffsetsVal = isSupported && tds[0].offsetHeight === 0;
                body.removeChild(container);
                div = body = null;
                var IARETREPLACE = reliableHiddenOffsetsVal;
                Sensor();
                return IARETREPLACE;
            },
            boxSizing: function () {
                Sensor();
                if (boxSizingVal == null) {
                    computeStyleTests();
                }
                var IARETREPLACE = boxSizingVal;
                Sensor();
                return IARETREPLACE;
            },
            boxSizingReliable: function () {
                Sensor();
                if (boxSizingReliableVal == null) {
                    computeStyleTests();
                }
                var IARETREPLACE = boxSizingReliableVal;
                Sensor();
                return IARETREPLACE;
            },
            pixelPosition: function () {
                Sensor();
                if (pixelPositionVal == null) {
                    computeStyleTests();
                }
                var IARETREPLACE = pixelPositionVal;
                Sensor();
                return IARETREPLACE;
            },
            reliableMarginRight: function () {
                Sensor();
                var body, container, div, marginDiv;
                if (reliableMarginRightVal == null && window.getComputedStyle) {
                    body = document.getElementsByTagName('body')[0];
                    if (!body) {
                        var IARETREPLACE;
                        Sensor();
                        return IARETREPLACE;
                    }
                    container = document.createElement('div');
                    div = document.createElement('div');
                    container.style.cssText = containerStyles;
                    body.appendChild(container).appendChild(div);
                    marginDiv = div.appendChild(document.createElement('div'));
                    marginDiv.style.cssText = div.style.cssText = divReset;
                    marginDiv.style.marginRight = marginDiv.style.width = '0';
                    div.style.width = '1px';
                    reliableMarginRightVal = !parseFloat((window.getComputedStyle(marginDiv, null) || {}).marginRight);
                    body.removeChild(container);
                }
                var IARETREPLACE = reliableMarginRightVal;
                Sensor();
                return IARETREPLACE;
            }
        });
        function computeStyleTests() {
            Sensor();
            var container, div, body = document.getElementsByTagName('body')[0];
            if (!body) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            container = document.createElement('div');
            div = document.createElement('div');
            container.style.cssText = containerStyles;
            body.appendChild(container).appendChild(div);
            div.style.cssText = '-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;' + 'position:absolute;display:block;padding:1px;border:1px;width:4px;' + 'margin-top:1%;top:1%';
            jQuery.swap(body, body.style.zoom != null ? { zoom: 1 } : {}, function () {
                Sensor();
                boxSizingVal = div.offsetWidth === 4;
                Sensor();
            });
            boxSizingReliableVal = true;
            pixelPositionVal = false;
            reliableMarginRightVal = true;
            if (window.getComputedStyle) {
                pixelPositionVal = (window.getComputedStyle(div, null) || {}).top !== '1%';
                boxSizingReliableVal = (window.getComputedStyle(div, null) || { width: '4px' }).width === '4px';
            }
            body.removeChild(container);
            div = body = null;
            Sensor();
        }
        Sensor();
    }());
    jQuery.swap = function (elem, options, callback, args) {
        Sensor();
        var ret, name, old = {};
        for (name in options) {
            old[name] = elem.style[name];
            elem.style[name] = options[name];
        }
        ret = callback.apply(elem, args || []);
        for (name in options) {
            elem.style[name] = old[name];
        }
        var IARETREPLACE = ret;
        Sensor();
        return IARETREPLACE;
    };
    var ralpha = /alpha\([^)]*\)/i, ropacity = /opacity\s*=\s*([^)]*)/, rdisplayswap = /^(none|table(?!-c[ea]).+)/, rnumsplit = new RegExp('^(' + pnum + ')(.*)$', 'i'), rrelNum = new RegExp('^([+-])=(' + pnum + ')', 'i'), cssShow = {
            position: 'absolute',
            visibility: 'hidden',
            display: 'block'
        }, cssNormalTransform = {
            letterSpacing: 0,
            fontWeight: 400
        }, cssPrefixes = [
            'Webkit',
            'O',
            'Moz',
            'ms'
        ];
    function vendorPropName(style, name) {
        Sensor();
        if (name in style) {
            var IARETREPLACE = name;
            Sensor();
            return IARETREPLACE;
        }
        var capName = name.charAt(0).toUpperCase() + name.slice(1), origName = name, i = cssPrefixes.length;
        while (i--) {
            name = cssPrefixes[i] + capName;
            if (name in style) {
                var IARETREPLACE = name;
                Sensor();
                return IARETREPLACE;
            }
        }
        var IARETREPLACE = origName;
        Sensor();
        return IARETREPLACE;
    }
    function showHide(elements, show) {
        Sensor();
        var display, elem, hidden, values = [], index = 0, length = elements.length;
        for (; index < length; index++) {
            elem = elements[index];
            if (!elem.style) {
                continue;
            }
            values[index] = jQuery._data(elem, 'olddisplay');
            display = elem.style.display;
            if (show) {
                if (!values[index] && display === 'none') {
                    elem.style.display = '';
                }
                if (elem.style.display === '' && isHidden(elem)) {
                    values[index] = jQuery._data(elem, 'olddisplay', defaultDisplay(elem.nodeName));
                }
            } else {
                if (!values[index]) {
                    hidden = isHidden(elem);
                    if (display && display !== 'none' || !hidden) {
                        jQuery._data(elem, 'olddisplay', hidden ? display : jQuery.css(elem, 'display'));
                    }
                }
            }
        }
        for (index = 0; index < length; index++) {
            elem = elements[index];
            if (!elem.style) {
                continue;
            }
            if (!show || elem.style.display === 'none' || elem.style.display === '') {
                elem.style.display = show ? values[index] || '' : 'none';
            }
        }
        var IARETREPLACE = elements;
        Sensor();
        return IARETREPLACE;
    }
    function setPositiveNumber(elem, value, subtract) {
        Sensor();
        var matches = rnumsplit.exec(value);
        var IARETREPLACE = matches ? Math.max(0, matches[1] - (subtract || 0)) + (matches[2] || 'px') : value;
        Sensor();
        return IARETREPLACE;
    }
    function augmentWidthOrHeight(elem, name, extra, isBorderBox, styles) {
        Sensor();
        var i = extra === (isBorderBox ? 'border' : 'content') ? 4 : name === 'width' ? 1 : 0, val = 0;
        for (; i < 4; i += 2) {
            if (extra === 'margin') {
                val += jQuery.css(elem, extra + cssExpand[i], true, styles);
            }
            if (isBorderBox) {
                if (extra === 'content') {
                    val -= jQuery.css(elem, 'padding' + cssExpand[i], true, styles);
                }
                if (extra !== 'margin') {
                    val -= jQuery.css(elem, 'border' + cssExpand[i] + 'Width', true, styles);
                }
            } else {
                val += jQuery.css(elem, 'padding' + cssExpand[i], true, styles);
                if (extra !== 'padding') {
                    val += jQuery.css(elem, 'border' + cssExpand[i] + 'Width', true, styles);
                }
            }
        }
        var IARETREPLACE = val;
        Sensor();
        return IARETREPLACE;
    }
    function getWidthOrHeight(elem, name, extra) {
        Sensor();
        var valueIsBorderBox = true, val = name === 'width' ? elem.offsetWidth : elem.offsetHeight, styles = getStyles(elem), isBorderBox = support.boxSizing() && jQuery.css(elem, 'boxSizing', false, styles) === 'border-box';
        if (val <= 0 || val == null) {
            val = curCSS(elem, name, styles);
            if (val < 0 || val == null) {
                val = elem.style[name];
            }
            if (rnumnonpx.test(val)) {
                var IARETREPLACE = val;
                Sensor();
                return IARETREPLACE;
            }
            valueIsBorderBox = isBorderBox && (support.boxSizingReliable() || val === elem.style[name]);
            val = parseFloat(val) || 0;
        }
        var IARETREPLACE = val + augmentWidthOrHeight(elem, name, extra || (isBorderBox ? 'border' : 'content'), valueIsBorderBox, styles) + 'px';
        Sensor();
        return IARETREPLACE;
    }
    jQuery.extend({
        cssHooks: {
            opacity: {
                get: function (elem, computed) {
                    Sensor();
                    if (computed) {
                        var ret = curCSS(elem, 'opacity');
                        var IARETREPLACE = ret === '' ? '1' : ret;
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                }
            }
        },
        cssNumber: {
            'columnCount': true,
            'fillOpacity': true,
            'fontWeight': true,
            'lineHeight': true,
            'opacity': true,
            'order': true,
            'orphans': true,
            'widows': true,
            'zIndex': true,
            'zoom': true
        },
        cssProps: { 'float': support.cssFloat ? 'cssFloat' : 'styleFloat' },
        style: function (elem, name, value, extra) {
            Sensor();
            if (!elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            var ret, type, hooks, origName = jQuery.camelCase(name), style = elem.style;
            name = jQuery.cssProps[origName] || (jQuery.cssProps[origName] = vendorPropName(style, origName));
            hooks = jQuery.cssHooks[name] || jQuery.cssHooks[origName];
            if (value !== undefined) {
                type = typeof value;
                if (type === 'string' && (ret = rrelNum.exec(value))) {
                    value = (ret[1] + 1) * ret[2] + parseFloat(jQuery.css(elem, name));
                    type = 'number';
                }
                if (value == null || value !== value) {
                    var IARETREPLACE;
                    Sensor();
                    return IARETREPLACE;
                }
                if (type === 'number' && !jQuery.cssNumber[origName]) {
                    value += 'px';
                }
                if (!support.clearCloneStyle && value === '' && name.indexOf('background') === 0) {
                    style[name] = 'inherit';
                }
                if (!hooks || !('set' in hooks) || (value = hooks.set(elem, value, extra)) !== undefined) {
                    try {
                        style[name] = '';
                        style[name] = value;
                    } catch (e) {
                    }
                }
            } else {
                if (hooks && 'get' in hooks && (ret = hooks.get(elem, false, extra)) !== undefined) {
                    var IARETREPLACE = ret;
                    Sensor();
                    return IARETREPLACE;
                }
                var IARETREPLACE = style[name];
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
        },
        css: function (elem, name, extra, styles) {
            Sensor();
            var num, val, hooks, origName = jQuery.camelCase(name);
            name = jQuery.cssProps[origName] || (jQuery.cssProps[origName] = vendorPropName(elem.style, origName));
            hooks = jQuery.cssHooks[name] || jQuery.cssHooks[origName];
            if (hooks && 'get' in hooks) {
                val = hooks.get(elem, true, extra);
            }
            if (val === undefined) {
                val = curCSS(elem, name, styles);
            }
            if (val === 'normal' && name in cssNormalTransform) {
                val = cssNormalTransform[name];
            }
            if (extra === '' || extra) {
                num = parseFloat(val);
                var IARETREPLACE = extra === true || jQuery.isNumeric(num) ? num || 0 : val;
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = val;
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.each([
        'height',
        'width'
    ], function (i, name) {
        Sensor();
        jQuery.cssHooks[name] = {
            get: function (elem, computed, extra) {
                Sensor();
                if (computed) {
                    var IARETREPLACE = elem.offsetWidth === 0 && rdisplayswap.test(jQuery.css(elem, 'display')) ? jQuery.swap(elem, cssShow, function () {
                        var IARETREPLACE = getWidthOrHeight(elem, name, extra);
                        Sensor();
                        return IARETREPLACE;
                    }) : getWidthOrHeight(elem, name, extra);
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            },
            set: function (elem, value, extra) {
                Sensor();
                var styles = extra && getStyles(elem);
                var IARETREPLACE = setPositiveNumber(elem, value, extra ? augmentWidthOrHeight(elem, name, extra, support.boxSizing() && jQuery.css(elem, 'boxSizing', false, styles) === 'border-box', styles) : 0);
                Sensor();
                return IARETREPLACE;
            }
        };
        Sensor();
    });
    if (!support.opacity) {
        jQuery.cssHooks.opacity = {
            get: function (elem, computed) {
                var IARETREPLACE = ropacity.test((computed && elem.currentStyle ? elem.currentStyle.filter : elem.style.filter) || '') ? 0.01 * parseFloat(RegExp.$1) + '' : computed ? '1' : '';
                Sensor();
                return IARETREPLACE;
            },
            set: function (elem, value) {
                Sensor();
                var style = elem.style, currentStyle = elem.currentStyle, opacity = jQuery.isNumeric(value) ? 'alpha(opacity=' + value * 100 + ')' : '', filter = currentStyle && currentStyle.filter || style.filter || '';
                style.zoom = 1;
                if ((value >= 1 || value === '') && jQuery.trim(filter.replace(ralpha, '')) === '' && style.removeAttribute) {
                    style.removeAttribute('filter');
                    if (value === '' || currentStyle && !currentStyle.filter) {
                        var IARETREPLACE;
                        Sensor();
                        return IARETREPLACE;
                    }
                }
                style.filter = ralpha.test(filter) ? filter.replace(ralpha, opacity) : filter + ' ' + opacity;
                Sensor();
            }
        };
    }
    jQuery.cssHooks.marginRight = addGetHookIf(support.reliableMarginRight, function (elem, computed) {
        Sensor();
        if (computed) {
            var IARETREPLACE = jQuery.swap(elem, { 'display': 'inline-block' }, curCSS, [
                elem,
                'marginRight'
            ]);
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
    });
    jQuery.each({
        margin: '',
        padding: '',
        border: 'Width'
    }, function (prefix, suffix) {
        Sensor();
        jQuery.cssHooks[prefix + suffix] = {
            expand: function (value) {
                Sensor();
                var i = 0, expanded = {}, parts = typeof value === 'string' ? value.split(' ') : [value];
                for (; i < 4; i++) {
                    expanded[prefix + cssExpand[i] + suffix] = parts[i] || parts[i - 2] || parts[0];
                }
                var IARETREPLACE = expanded;
                Sensor();
                return IARETREPLACE;
            }
        };
        if (!rmargin.test(prefix)) {
            jQuery.cssHooks[prefix + suffix].set = setPositiveNumber;
        }
        Sensor();
    });
    jQuery.fn.extend({
        css: function (name, value) {
            var IARETREPLACE = access(this, function (elem, name, value) {
                Sensor();
                var styles, len, map = {}, i = 0;
                if (jQuery.isArray(name)) {
                    styles = getStyles(elem);
                    len = name.length;
                    for (; i < len; i++) {
                        map[name[i]] = jQuery.css(elem, name[i], false, styles);
                    }
                    var IARETREPLACE = map;
                    Sensor();
                    return IARETREPLACE;
                }
                var IARETREPLACE = value !== undefined ? jQuery.style(elem, name, value) : jQuery.css(elem, name);
                Sensor();
                return IARETREPLACE;
            }, name, value, arguments.length > 1);
            Sensor();
            return IARETREPLACE;
        },
        show: function () {
            var IARETREPLACE = showHide(this, true);
            Sensor();
            return IARETREPLACE;
        },
        hide: function () {
            var IARETREPLACE = showHide(this);
            Sensor();
            return IARETREPLACE;
        },
        toggle: function (state) {
            Sensor();
            if (typeof state === 'boolean') {
                var IARETREPLACE = state ? this.show() : this.hide();
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = this.each(function () {
                Sensor();
                if (isHidden(this)) {
                    jQuery(this).show();
                } else {
                    jQuery(this).hide();
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        }
    });
    function Tween(elem, options, prop, end, easing) {
        var IARETREPLACE = new Tween.prototype.init(elem, options, prop, end, easing);
        Sensor();
        return IARETREPLACE;
    }
    jQuery.Tween = Tween;
    Tween.prototype = {
        constructor: Tween,
        init: function (elem, options, prop, end, easing, unit) {
            Sensor();
            this.elem = elem;
            this.prop = prop;
            this.easing = easing || 'swing';
            this.options = options;
            this.start = this.now = this.cur();
            this.end = end;
            this.unit = unit || (jQuery.cssNumber[prop] ? '' : 'px');
            Sensor();
        },
        cur: function () {
            Sensor();
            var hooks = Tween.propHooks[this.prop];
            var IARETREPLACE = hooks && hooks.get ? hooks.get(this) : Tween.propHooks._default.get(this);
            Sensor();
            return IARETREPLACE;
        },
        run: function (percent) {
            Sensor();
            var eased, hooks = Tween.propHooks[this.prop];
            if (this.options.duration) {
                this.pos = eased = jQuery.easing[this.easing](percent, this.options.duration * percent, 0, 1, this.options.duration);
            } else {
                this.pos = eased = percent;
            }
            this.now = (this.end - this.start) * eased + this.start;
            if (this.options.step) {
                this.options.step.call(this.elem, this.now, this);
            }
            if (hooks && hooks.set) {
                hooks.set(this);
            } else {
                Tween.propHooks._default.set(this);
            }
            var IARETREPLACE = this;
            Sensor();
            return IARETREPLACE;
        }
    };
    Tween.prototype.init.prototype = Tween.prototype;
    Tween.propHooks = {
        _default: {
            get: function (tween) {
                Sensor();
                var result;
                if (tween.elem[tween.prop] != null && (!tween.elem.style || tween.elem.style[tween.prop] == null)) {
                    var IARETREPLACE = tween.elem[tween.prop];
                    Sensor();
                    return IARETREPLACE;
                }
                result = jQuery.css(tween.elem, tween.prop, '');
                var IARETREPLACE = !result || result === 'auto' ? 0 : result;
                Sensor();
                return IARETREPLACE;
            },
            set: function (tween) {
                Sensor();
                if (jQuery.fx.step[tween.prop]) {
                    jQuery.fx.step[tween.prop](tween);
                } else {
                    if (tween.elem.style && (tween.elem.style[jQuery.cssProps[tween.prop]] != null || jQuery.cssHooks[tween.prop])) {
                        jQuery.style(tween.elem, tween.prop, tween.now + tween.unit);
                    } else {
                        tween.elem[tween.prop] = tween.now;
                    }
                }
                Sensor();
            }
        }
    };
    Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
        set: function (tween) {
            Sensor();
            if (tween.elem.nodeType && tween.elem.parentNode) {
                tween.elem[tween.prop] = tween.now;
            }
            Sensor();
        }
    };
    jQuery.easing = {
        linear: function (p) {
            var IARETREPLACE = p;
            Sensor();
            return IARETREPLACE;
        },
        swing: function (p) {
            var IARETREPLACE = 0.5 - Math.cos(p * Math.PI) / 2;
            Sensor();
            return IARETREPLACE;
        }
    };
    jQuery.fx = Tween.prototype.init;
    jQuery.fx.step = {};
    var fxNow, timerId, rfxtypes = /^(?:toggle|show|hide)$/, rfxnum = new RegExp('^(?:([+-])=|)(' + pnum + ')([a-z%]*)$', 'i'), rrun = /queueHooks$/, animationPrefilters = [defaultPrefilter], tweeners = {
            '*': [function (prop, value) {
                    Sensor();
                    var tween = this.createTween(prop, value), target = tween.cur(), parts = rfxnum.exec(value), unit = parts && parts[3] || (jQuery.cssNumber[prop] ? '' : 'px'), start = (jQuery.cssNumber[prop] || unit !== 'px' && +target) && rfxnum.exec(jQuery.css(tween.elem, prop)), scale = 1, maxIterations = 20;
                    if (start && start[3] !== unit) {
                        unit = unit || start[3];
                        parts = parts || [];
                        start = +target || 1;
                        do {
                            scale = scale || '.5';
                            start = start / scale;
                            jQuery.style(tween.elem, prop, start + unit);
                        } while (scale !== (scale = tween.cur() / target) && scale !== 1 && --maxIterations);
                    }
                    if (parts) {
                        start = tween.start = +start || +target || 0;
                        tween.unit = unit;
                        tween.end = parts[1] ? start + (parts[1] + 1) * parts[2] : +parts[2];
                    }
                    var IARETREPLACE = tween;
                    Sensor();
                    return IARETREPLACE;
                }]
        };
    function createFxNow() {
        Sensor();
        setTimeout(function () {
            Sensor();
            fxNow = undefined;
            Sensor();
        });
        var IARETREPLACE = fxNow = jQuery.now();
        Sensor();
        return IARETREPLACE;
    }
    function genFx(type, includeWidth) {
        Sensor();
        var which, attrs = { height: type }, i = 0;
        includeWidth = includeWidth ? 1 : 0;
        for (; i < 4; i += 2 - includeWidth) {
            which = cssExpand[i];
            attrs['margin' + which] = attrs['padding' + which] = type;
        }
        if (includeWidth) {
            attrs.opacity = attrs.width = type;
        }
        var IARETREPLACE = attrs;
        Sensor();
        return IARETREPLACE;
    }
    function createTween(value, prop, animation) {
        Sensor();
        var tween, collection = (tweeners[prop] || []).concat(tweeners['*']), index = 0, length = collection.length;
        for (; index < length; index++) {
            if (tween = collection[index].call(animation, prop, value)) {
                var IARETREPLACE = tween;
                Sensor();
                return IARETREPLACE;
            }
        }
        Sensor();
    }
    function defaultPrefilter(elem, props, opts) {
        Sensor();
        var prop, value, toggle, tween, hooks, oldfire, display, dDisplay, anim = this, orig = {}, style = elem.style, hidden = elem.nodeType && isHidden(elem), dataShow = jQuery._data(elem, 'fxshow');
        if (!opts.queue) {
            hooks = jQuery._queueHooks(elem, 'fx');
            if (hooks.unqueued == null) {
                hooks.unqueued = 0;
                oldfire = hooks.empty.fire;
                hooks.empty.fire = function () {
                    Sensor();
                    if (!hooks.unqueued) {
                        oldfire();
                    }
                    Sensor();
                };
            }
            hooks.unqueued++;
            anim.always(function () {
                Sensor();
                anim.always(function () {
                    Sensor();
                    hooks.unqueued--;
                    if (!jQuery.queue(elem, 'fx').length) {
                        hooks.empty.fire();
                    }
                    Sensor();
                });
                Sensor();
            });
        }
        if (elem.nodeType === 1 && ('height' in props || 'width' in props)) {
            opts.overflow = [
                style.overflow,
                style.overflowX,
                style.overflowY
            ];
            display = jQuery.css(elem, 'display');
            dDisplay = defaultDisplay(elem.nodeName);
            if (display === 'none') {
                display = dDisplay;
            }
            if (display === 'inline' && jQuery.css(elem, 'float') === 'none') {
                if (!support.inlineBlockNeedsLayout || dDisplay === 'inline') {
                    style.display = 'inline-block';
                } else {
                    style.zoom = 1;
                }
            }
        }
        if (opts.overflow) {
            style.overflow = 'hidden';
            if (!support.shrinkWrapBlocks()) {
                anim.always(function () {
                    Sensor();
                    style.overflow = opts.overflow[0];
                    style.overflowX = opts.overflow[1];
                    style.overflowY = opts.overflow[2];
                    Sensor();
                });
            }
        }
        for (prop in props) {
            value = props[prop];
            if (rfxtypes.exec(value)) {
                delete props[prop];
                toggle = toggle || value === 'toggle';
                if (value === (hidden ? 'hide' : 'show')) {
                    if (value === 'show' && dataShow && dataShow[prop] !== undefined) {
                        hidden = true;
                    } else {
                        continue;
                    }
                }
                orig[prop] = dataShow && dataShow[prop] || jQuery.style(elem, prop);
            }
        }
        if (!jQuery.isEmptyObject(orig)) {
            if (dataShow) {
                if ('hidden' in dataShow) {
                    hidden = dataShow.hidden;
                }
            } else {
                dataShow = jQuery._data(elem, 'fxshow', {});
            }
            if (toggle) {
                dataShow.hidden = !hidden;
            }
            if (hidden) {
                jQuery(elem).show();
            } else {
                anim.done(function () {
                    Sensor();
                    jQuery(elem).hide();
                    Sensor();
                });
            }
            anim.done(function () {
                Sensor();
                var prop;
                jQuery._removeData(elem, 'fxshow');
                for (prop in orig) {
                    jQuery.style(elem, prop, orig[prop]);
                }
                Sensor();
            });
            for (prop in orig) {
                tween = createTween(hidden ? dataShow[prop] : 0, prop, anim);
                if (!(prop in dataShow)) {
                    dataShow[prop] = tween.start;
                    if (hidden) {
                        tween.end = tween.start;
                        tween.start = prop === 'width' || prop === 'height' ? 1 : 0;
                    }
                }
            }
        }
        Sensor();
    }
    function propFilter(props, specialEasing) {
        Sensor();
        var index, name, easing, value, hooks;
        for (index in props) {
            name = jQuery.camelCase(index);
            easing = specialEasing[name];
            value = props[index];
            if (jQuery.isArray(value)) {
                easing = value[1];
                value = props[index] = value[0];
            }
            if (index !== name) {
                props[name] = value;
                delete props[index];
            }
            hooks = jQuery.cssHooks[name];
            if (hooks && 'expand' in hooks) {
                value = hooks.expand(value);
                delete props[name];
                for (index in value) {
                    if (!(index in props)) {
                        props[index] = value[index];
                        specialEasing[index] = easing;
                    }
                }
            } else {
                specialEasing[name] = easing;
            }
        }
        Sensor();
    }
    function Animation(elem, properties, options) {
        Sensor();
        var result, stopped, index = 0, length = animationPrefilters.length, deferred = jQuery.Deferred().always(function () {
                Sensor();
                delete tick.elem;
                Sensor();
            }), tick = function () {
                Sensor();
                if (stopped) {
                    var IARETREPLACE = false;
                    Sensor();
                    return IARETREPLACE;
                }
                var currentTime = fxNow || createFxNow(), remaining = Math.max(0, animation.startTime + animation.duration - currentTime), temp = remaining / animation.duration || 0, percent = 1 - temp, index = 0, length = animation.tweens.length;
                for (; index < length; index++) {
                    animation.tweens[index].run(percent);
                }
                deferred.notifyWith(elem, [
                    animation,
                    percent,
                    remaining
                ]);
                if (percent < 1 && length) {
                    var IARETREPLACE = remaining;
                    Sensor();
                    return IARETREPLACE;
                } else {
                    deferred.resolveWith(elem, [animation]);
                    var IARETREPLACE = false;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            }, animation = deferred.promise({
                elem: elem,
                props: jQuery.extend({}, properties),
                opts: jQuery.extend(true, { specialEasing: {} }, options),
                originalProperties: properties,
                originalOptions: options,
                startTime: fxNow || createFxNow(),
                duration: options.duration,
                tweens: [],
                createTween: function (prop, end) {
                    Sensor();
                    var tween = jQuery.Tween(elem, animation.opts, prop, end, animation.opts.specialEasing[prop] || animation.opts.easing);
                    animation.tweens.push(tween);
                    var IARETREPLACE = tween;
                    Sensor();
                    return IARETREPLACE;
                },
                stop: function (gotoEnd) {
                    Sensor();
                    var index = 0, length = gotoEnd ? animation.tweens.length : 0;
                    if (stopped) {
                        var IARETREPLACE = this;
                        Sensor();
                        return IARETREPLACE;
                    }
                    stopped = true;
                    for (; index < length; index++) {
                        animation.tweens[index].run(1);
                    }
                    if (gotoEnd) {
                        deferred.resolveWith(elem, [
                            animation,
                            gotoEnd
                        ]);
                    } else {
                        deferred.rejectWith(elem, [
                            animation,
                            gotoEnd
                        ]);
                    }
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                }
            }), props = animation.props;
        propFilter(props, animation.opts.specialEasing);
        for (; index < length; index++) {
            result = animationPrefilters[index].call(animation, elem, props, animation.opts);
            if (result) {
                var IARETREPLACE = result;
                Sensor();
                return IARETREPLACE;
            }
        }
        jQuery.map(props, createTween, animation);
        if (jQuery.isFunction(animation.opts.start)) {
            animation.opts.start.call(elem, animation);
        }
        jQuery.fx.timer(jQuery.extend(tick, {
            elem: elem,
            anim: animation,
            queue: animation.opts.queue
        }));
        var IARETREPLACE = animation.progress(animation.opts.progress).done(animation.opts.done, animation.opts.complete).fail(animation.opts.fail).always(animation.opts.always);
        Sensor();
        return IARETREPLACE;
    }
    jQuery.Animation = jQuery.extend(Animation, {
        tweener: function (props, callback) {
            Sensor();
            if (jQuery.isFunction(props)) {
                callback = props;
                props = ['*'];
            } else {
                props = props.split(' ');
            }
            var prop, index = 0, length = props.length;
            for (; index < length; index++) {
                prop = props[index];
                tweeners[prop] = tweeners[prop] || [];
                tweeners[prop].unshift(callback);
            }
            Sensor();
        },
        prefilter: function (callback, prepend) {
            Sensor();
            if (prepend) {
                animationPrefilters.unshift(callback);
            } else {
                animationPrefilters.push(callback);
            }
            Sensor();
        }
    });
    jQuery.speed = function (speed, easing, fn) {
        Sensor();
        var opt = speed && typeof speed === 'object' ? jQuery.extend({}, speed) : {
            complete: fn || !fn && easing || jQuery.isFunction(speed) && speed,
            duration: speed,
            easing: fn && easing || easing && !jQuery.isFunction(easing) && easing
        };
        opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === 'number' ? opt.duration : opt.duration in jQuery.fx.speeds ? jQuery.fx.speeds[opt.duration] : jQuery.fx.speeds._default;
        if (opt.queue == null || opt.queue === true) {
            opt.queue = 'fx';
        }
        opt.old = opt.complete;
        opt.complete = function () {
            Sensor();
            if (jQuery.isFunction(opt.old)) {
                opt.old.call(this);
            }
            if (opt.queue) {
                jQuery.dequeue(this, opt.queue);
            }
            Sensor();
        };
        var IARETREPLACE = opt;
        Sensor();
        return IARETREPLACE;
    };
    jQuery.fn.extend({
        fadeTo: function (speed, to, easing, callback) {
            var IARETREPLACE = this.filter(isHidden).css('opacity', 0).show().end().animate({ opacity: to }, speed, easing, callback);
            Sensor();
            return IARETREPLACE;
        },
        animate: function (prop, speed, easing, callback) {
            Sensor();
            var empty = jQuery.isEmptyObject(prop), optall = jQuery.speed(speed, easing, callback), doAnimation = function () {
                    Sensor();
                    var anim = Animation(this, jQuery.extend({}, prop), optall);
                    if (empty || jQuery._data(this, 'finish')) {
                        anim.stop(true);
                    }
                    Sensor();
                };
            doAnimation.finish = doAnimation;
            var IARETREPLACE = empty || optall.queue === false ? this.each(doAnimation) : this.queue(optall.queue, doAnimation);
            Sensor();
            return IARETREPLACE;
        },
        stop: function (type, clearQueue, gotoEnd) {
            Sensor();
            var stopQueue = function (hooks) {
                Sensor();
                var stop = hooks.stop;
                delete hooks.stop;
                stop(gotoEnd);
                Sensor();
            };
            if (typeof type !== 'string') {
                gotoEnd = clearQueue;
                clearQueue = type;
                type = undefined;
            }
            if (clearQueue && type !== false) {
                this.queue(type || 'fx', []);
            }
            var IARETREPLACE = this.each(function () {
                Sensor();
                var dequeue = true, index = type != null && type + 'queueHooks', timers = jQuery.timers, data = jQuery._data(this);
                if (index) {
                    if (data[index] && data[index].stop) {
                        stopQueue(data[index]);
                    }
                } else {
                    for (index in data) {
                        if (data[index] && data[index].stop && rrun.test(index)) {
                            stopQueue(data[index]);
                        }
                    }
                }
                for (index = timers.length; index--;) {
                    if (timers[index].elem === this && (type == null || timers[index].queue === type)) {
                        timers[index].anim.stop(gotoEnd);
                        dequeue = false;
                        timers.splice(index, 1);
                    }
                }
                if (dequeue || !gotoEnd) {
                    jQuery.dequeue(this, type);
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        finish: function (type) {
            Sensor();
            if (type !== false) {
                type = type || 'fx';
            }
            var IARETREPLACE = this.each(function () {
                Sensor();
                var index, data = jQuery._data(this), queue = data[type + 'queue'], hooks = data[type + 'queueHooks'], timers = jQuery.timers, length = queue ? queue.length : 0;
                data.finish = true;
                jQuery.queue(this, type, []);
                if (hooks && hooks.stop) {
                    hooks.stop.call(this, true);
                }
                for (index = timers.length; index--;) {
                    if (timers[index].elem === this && timers[index].queue === type) {
                        timers[index].anim.stop(true);
                        timers.splice(index, 1);
                    }
                }
                for (index = 0; index < length; index++) {
                    if (queue[index] && queue[index].finish) {
                        queue[index].finish.call(this);
                    }
                }
                delete data.finish;
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.each([
        'toggle',
        'show',
        'hide'
    ], function (i, name) {
        Sensor();
        var cssFn = jQuery.fn[name];
        jQuery.fn[name] = function (speed, easing, callback) {
            var IARETREPLACE = speed == null || typeof speed === 'boolean' ? cssFn.apply(this, arguments) : this.animate(genFx(name, true), speed, easing, callback);
            Sensor();
            return IARETREPLACE;
        };
        Sensor();
    });
    jQuery.each({
        slideDown: genFx('show'),
        slideUp: genFx('hide'),
        slideToggle: genFx('toggle'),
        fadeIn: { opacity: 'show' },
        fadeOut: { opacity: 'hide' },
        fadeToggle: { opacity: 'toggle' }
    }, function (name, props) {
        Sensor();
        jQuery.fn[name] = function (speed, easing, callback) {
            var IARETREPLACE = this.animate(props, speed, easing, callback);
            Sensor();
            return IARETREPLACE;
        };
        Sensor();
    });
    jQuery.timers = [];
    jQuery.fx.tick = function () {
        Sensor();
        var timer, timers = jQuery.timers, i = 0;
        fxNow = jQuery.now();
        for (; i < timers.length; i++) {
            timer = timers[i];
            if (!timer() && timers[i] === timer) {
                timers.splice(i--, 1);
            }
        }
        if (!timers.length) {
            jQuery.fx.stop();
        }
        fxNow = undefined;
        Sensor();
    };
    jQuery.fx.timer = function (timer) {
        Sensor();
        jQuery.timers.push(timer);
        if (timer()) {
            jQuery.fx.start();
        } else {
            jQuery.timers.pop();
        }
        Sensor();
    };
    jQuery.fx.interval = 13;
    jQuery.fx.start = function () {
        Sensor();
        if (!timerId) {
            timerId = setInterval(jQuery.fx.tick, jQuery.fx.interval);
        }
        Sensor();
    };
    jQuery.fx.stop = function () {
        Sensor();
        clearInterval(timerId);
        timerId = null;
        Sensor();
    };
    jQuery.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    };
    jQuery.fn.delay = function (time, type) {
        Sensor();
        time = jQuery.fx ? jQuery.fx.speeds[time] || time : time;
        type = type || 'fx';
        var IARETREPLACE = this.queue(type, function (next, hooks) {
            Sensor();
            var timeout = setTimeout(next, time);
            hooks.stop = function () {
                Sensor();
                clearTimeout(timeout);
                Sensor();
            };
            Sensor();
        });
        Sensor();
        return IARETREPLACE;
    };
    (function () {
        Sensor();
        var a, input, select, opt, div = document.createElement('div');
        div.setAttribute('className', 't');
        div.innerHTML = '  <link/><table></table><a href=\'/a\'>a</a><input type=\'checkbox\'/>';
        a = div.getElementsByTagName('a')[0];
        select = document.createElement('select');
        opt = select.appendChild(document.createElement('option'));
        input = div.getElementsByTagName('input')[0];
        a.style.cssText = 'top:1px';
        support.getSetAttribute = div.className !== 't';
        support.style = /top/.test(a.getAttribute('style'));
        support.hrefNormalized = a.getAttribute('href') === '/a';
        support.checkOn = !!input.value;
        support.optSelected = opt.selected;
        support.enctype = !!document.createElement('form').enctype;
        select.disabled = true;
        support.optDisabled = !opt.disabled;
        input = document.createElement('input');
        input.setAttribute('value', '');
        support.input = input.getAttribute('value') === '';
        input.value = 't';
        input.setAttribute('type', 'radio');
        support.radioValue = input.value === 't';
        a = input = select = opt = div = null;
        Sensor();
    }());
    var rreturn = /\r/g;
    jQuery.fn.extend({
        val: function (value) {
            Sensor();
            var hooks, ret, isFunction, elem = this[0];
            if (!arguments.length) {
                if (elem) {
                    hooks = jQuery.valHooks[elem.type] || jQuery.valHooks[elem.nodeName.toLowerCase()];
                    if (hooks && 'get' in hooks && (ret = hooks.get(elem, 'value')) !== undefined) {
                        var IARETREPLACE = ret;
                        Sensor();
                        return IARETREPLACE;
                    }
                    ret = elem.value;
                    var IARETREPLACE = typeof ret === 'string' ? ret.replace(rreturn, '') : ret == null ? '' : ret;
                    Sensor();
                    return IARETREPLACE;
                }
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            isFunction = jQuery.isFunction(value);
            var IARETREPLACE = this.each(function (i) {
                Sensor();
                var val;
                if (this.nodeType !== 1) {
                    var IARETREPLACE;
                    Sensor();
                    return IARETREPLACE;
                }
                if (isFunction) {
                    val = value.call(this, i, jQuery(this).val());
                } else {
                    val = value;
                }
                if (val == null) {
                    val = '';
                } else {
                    if (typeof val === 'number') {
                        val += '';
                    } else {
                        if (jQuery.isArray(val)) {
                            val = jQuery.map(val, function (value) {
                                var IARETREPLACE = value == null ? '' : value + '';
                                Sensor();
                                return IARETREPLACE;
                            });
                        }
                    }
                }
                hooks = jQuery.valHooks[this.type] || jQuery.valHooks[this.nodeName.toLowerCase()];
                if (!hooks || !('set' in hooks) || hooks.set(this, val, 'value') === undefined) {
                    this.value = val;
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.extend({
        valHooks: {
            option: {
                get: function (elem) {
                    Sensor();
                    var val = jQuery.find.attr(elem, 'value');
                    var IARETREPLACE = val != null ? val : jQuery.text(elem);
                    Sensor();
                    return IARETREPLACE;
                }
            },
            select: {
                get: function (elem) {
                    Sensor();
                    var value, option, options = elem.options, index = elem.selectedIndex, one = elem.type === 'select-one' || index < 0, values = one ? null : [], max = one ? index + 1 : options.length, i = index < 0 ? max : one ? index : 0;
                    for (; i < max; i++) {
                        option = options[i];
                        if ((option.selected || i === index) && (support.optDisabled ? !option.disabled : option.getAttribute('disabled') === null) && (!option.parentNode.disabled || !jQuery.nodeName(option.parentNode, 'optgroup'))) {
                            value = jQuery(option).val();
                            if (one) {
                                var IARETREPLACE = value;
                                Sensor();
                                return IARETREPLACE;
                            }
                            values.push(value);
                        }
                    }
                    var IARETREPLACE = values;
                    Sensor();
                    return IARETREPLACE;
                },
                set: function (elem, value) {
                    Sensor();
                    var optionSet, option, options = elem.options, values = jQuery.makeArray(value), i = options.length;
                    while (i--) {
                        option = options[i];
                        if (jQuery.inArray(jQuery.valHooks.option.get(option), values) >= 0) {
                            try {
                                option.selected = optionSet = true;
                            } catch (_) {
                                option.scrollHeight;
                            }
                        } else {
                            option.selected = false;
                        }
                    }
                    if (!optionSet) {
                        elem.selectedIndex = -1;
                    }
                    var IARETREPLACE = options;
                    Sensor();
                    return IARETREPLACE;
                }
            }
        }
    });
    jQuery.each([
        'radio',
        'checkbox'
    ], function () {
        Sensor();
        jQuery.valHooks[this] = {
            set: function (elem, value) {
                Sensor();
                if (jQuery.isArray(value)) {
                    var IARETREPLACE = elem.checked = jQuery.inArray(jQuery(elem).val(), value) >= 0;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            }
        };
        if (!support.checkOn) {
            jQuery.valHooks[this].get = function (elem) {
                var IARETREPLACE = elem.getAttribute('value') === null ? 'on' : elem.value;
                Sensor();
                return IARETREPLACE;
            };
        }
        Sensor();
    });
    var nodeHook, boolHook, attrHandle = jQuery.expr.attrHandle, ruseDefault = /^(?:checked|selected)$/i, getSetAttribute = support.getSetAttribute, getSetInput = support.input;
    jQuery.fn.extend({
        attr: function (name, value) {
            var IARETREPLACE = access(this, jQuery.attr, name, value, arguments.length > 1);
            Sensor();
            return IARETREPLACE;
        },
        removeAttr: function (name) {
            var IARETREPLACE = this.each(function () {
                Sensor();
                jQuery.removeAttr(this, name);
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.extend({
        attr: function (elem, name, value) {
            Sensor();
            var hooks, ret, nType = elem.nodeType;
            if (!elem || nType === 3 || nType === 8 || nType === 2) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            if (typeof elem.getAttribute === strundefined) {
                var IARETREPLACE = jQuery.prop(elem, name, value);
                Sensor();
                return IARETREPLACE;
            }
            if (nType !== 1 || !jQuery.isXMLDoc(elem)) {
                name = name.toLowerCase();
                hooks = jQuery.attrHooks[name] || (jQuery.expr.match.bool.test(name) ? boolHook : nodeHook);
            }
            if (value !== undefined) {
                if (value === null) {
                    jQuery.removeAttr(elem, name);
                } else {
                    if (hooks && 'set' in hooks && (ret = hooks.set(elem, value, name)) !== undefined) {
                        var IARETREPLACE = ret;
                        Sensor();
                        return IARETREPLACE;
                    } else {
                        elem.setAttribute(name, value + '');
                        var IARETREPLACE = value;
                        Sensor();
                        return IARETREPLACE;
                    }
                }
            } else {
                if (hooks && 'get' in hooks && (ret = hooks.get(elem, name)) !== null) {
                    var IARETREPLACE = ret;
                    Sensor();
                    return IARETREPLACE;
                } else {
                    ret = jQuery.find.attr(elem, name);
                    var IARETREPLACE = ret == null ? undefined : ret;
                    Sensor();
                    return IARETREPLACE;
                }
            }
            Sensor();
        },
        removeAttr: function (elem, value) {
            Sensor();
            var name, propName, i = 0, attrNames = value && value.match(rnotwhite);
            if (attrNames && elem.nodeType === 1) {
                while (name = attrNames[i++]) {
                    propName = jQuery.propFix[name] || name;
                    if (jQuery.expr.match.bool.test(name)) {
                        if (getSetInput && getSetAttribute || !ruseDefault.test(name)) {
                            elem[propName] = false;
                        } else {
                            elem[jQuery.camelCase('default-' + name)] = elem[propName] = false;
                        }
                    } else {
                        jQuery.attr(elem, name, '');
                    }
                    elem.removeAttribute(getSetAttribute ? name : propName);
                }
            }
            Sensor();
        },
        attrHooks: {
            type: {
                set: function (elem, value) {
                    Sensor();
                    if (!support.radioValue && value === 'radio' && jQuery.nodeName(elem, 'input')) {
                        var val = elem.value;
                        elem.setAttribute('type', value);
                        if (val) {
                            elem.value = val;
                        }
                        var IARETREPLACE = value;
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                }
            }
        }
    });
    boolHook = {
        set: function (elem, value, name) {
            Sensor();
            if (value === false) {
                jQuery.removeAttr(elem, name);
            } else {
                if (getSetInput && getSetAttribute || !ruseDefault.test(name)) {
                    elem.setAttribute(!getSetAttribute && jQuery.propFix[name] || name, name);
                } else {
                    elem[jQuery.camelCase('default-' + name)] = elem[name] = true;
                }
            }
            var IARETREPLACE = name;
            Sensor();
            return IARETREPLACE;
        }
    };
    jQuery.each(jQuery.expr.match.bool.source.match(/\w+/g), function (i, name) {
        Sensor();
        var getter = attrHandle[name] || jQuery.find.attr;
        attrHandle[name] = getSetInput && getSetAttribute || !ruseDefault.test(name) ? function (elem, name, isXML) {
            Sensor();
            var ret, handle;
            if (!isXML) {
                handle = attrHandle[name];
                attrHandle[name] = ret;
                ret = getter(elem, name, isXML) != null ? name.toLowerCase() : null;
                attrHandle[name] = handle;
            }
            var IARETREPLACE = ret;
            Sensor();
            return IARETREPLACE;
        } : function (elem, name, isXML) {
            Sensor();
            if (!isXML) {
                var IARETREPLACE = elem[jQuery.camelCase('default-' + name)] ? name.toLowerCase() : null;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
        };
        Sensor();
    });
    if (!getSetInput || !getSetAttribute) {
        jQuery.attrHooks.value = {
            set: function (elem, value, name) {
                Sensor();
                if (jQuery.nodeName(elem, 'input')) {
                    elem.defaultValue = value;
                } else {
                    var IARETREPLACE = nodeHook && nodeHook.set(elem, value, name);
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            }
        };
    }
    if (!getSetAttribute) {
        nodeHook = {
            set: function (elem, value, name) {
                Sensor();
                var ret = elem.getAttributeNode(name);
                if (!ret) {
                    elem.setAttributeNode(ret = elem.ownerDocument.createAttribute(name));
                }
                ret.value = value += '';
                if (name === 'value' || value === elem.getAttribute(name)) {
                    var IARETREPLACE = value;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            }
        };
        attrHandle.id = attrHandle.name = attrHandle.coords = function (elem, name, isXML) {
            Sensor();
            var ret;
            if (!isXML) {
                var IARETREPLACE = (ret = elem.getAttributeNode(name)) && ret.value !== '' ? ret.value : null;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
        };
        jQuery.valHooks.button = {
            get: function (elem, name) {
                Sensor();
                var ret = elem.getAttributeNode(name);
                if (ret && ret.specified) {
                    var IARETREPLACE = ret.value;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            },
            set: nodeHook.set
        };
        jQuery.attrHooks.contenteditable = {
            set: function (elem, value, name) {
                Sensor();
                nodeHook.set(elem, value === '' ? false : value, name);
                Sensor();
            }
        };
        jQuery.each([
            'width',
            'height'
        ], function (i, name) {
            Sensor();
            jQuery.attrHooks[name] = {
                set: function (elem, value) {
                    Sensor();
                    if (value === '') {
                        elem.setAttribute(name, 'auto');
                        var IARETREPLACE = value;
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                }
            };
            Sensor();
        });
    }
    if (!support.style) {
        jQuery.attrHooks.style = {
            get: function (elem) {
                var IARETREPLACE = elem.style.cssText || undefined;
                Sensor();
                return IARETREPLACE;
            },
            set: function (elem, value) {
                var IARETREPLACE = elem.style.cssText = value + '';
                Sensor();
                return IARETREPLACE;
            }
        };
    }
    var rfocusable = /^(?:input|select|textarea|button|object)$/i, rclickable = /^(?:a|area)$/i;
    jQuery.fn.extend({
        prop: function (name, value) {
            var IARETREPLACE = access(this, jQuery.prop, name, value, arguments.length > 1);
            Sensor();
            return IARETREPLACE;
        },
        removeProp: function (name) {
            Sensor();
            name = jQuery.propFix[name] || name;
            var IARETREPLACE = this.each(function () {
                Sensor();
                try {
                    this[name] = undefined;
                    delete this[name];
                } catch (e) {
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.extend({
        propFix: {
            'for': 'htmlFor',
            'class': 'className'
        },
        prop: function (elem, name, value) {
            Sensor();
            var ret, hooks, notxml, nType = elem.nodeType;
            if (!elem || nType === 3 || nType === 8 || nType === 2) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            notxml = nType !== 1 || !jQuery.isXMLDoc(elem);
            if (notxml) {
                name = jQuery.propFix[name] || name;
                hooks = jQuery.propHooks[name];
            }
            if (value !== undefined) {
                var IARETREPLACE = hooks && 'set' in hooks && (ret = hooks.set(elem, value, name)) !== undefined ? ret : elem[name] = value;
                Sensor();
                return IARETREPLACE;
            } else {
                var IARETREPLACE = hooks && 'get' in hooks && (ret = hooks.get(elem, name)) !== null ? ret : elem[name];
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
        },
        propHooks: {
            tabIndex: {
                get: function (elem) {
                    Sensor();
                    var tabindex = jQuery.find.attr(elem, 'tabindex');
                    var IARETREPLACE = tabindex ? parseInt(tabindex, 10) : rfocusable.test(elem.nodeName) || rclickable.test(elem.nodeName) && elem.href ? 0 : -1;
                    Sensor();
                    return IARETREPLACE;
                }
            }
        }
    });
    if (!support.hrefNormalized) {
        jQuery.each([
            'href',
            'src'
        ], function (i, name) {
            Sensor();
            jQuery.propHooks[name] = {
                get: function (elem) {
                    var IARETREPLACE = elem.getAttribute(name, 4);
                    Sensor();
                    return IARETREPLACE;
                }
            };
            Sensor();
        });
    }
    if (!support.optSelected) {
        jQuery.propHooks.selected = {
            get: function (elem) {
                Sensor();
                var parent = elem.parentNode;
                if (parent) {
                    parent.selectedIndex;
                    if (parent.parentNode) {
                        parent.parentNode.selectedIndex;
                    }
                }
                var IARETREPLACE = null;
                Sensor();
                return IARETREPLACE;
            }
        };
    }
    jQuery.each([
        'tabIndex',
        'readOnly',
        'maxLength',
        'cellSpacing',
        'cellPadding',
        'rowSpan',
        'colSpan',
        'useMap',
        'frameBorder',
        'contentEditable'
    ], function () {
        Sensor();
        jQuery.propFix[this.toLowerCase()] = this;
        Sensor();
    });
    if (!support.enctype) {
        jQuery.propFix.enctype = 'encoding';
    }
    var rclass = /[\t\r\n\f]/g;
    jQuery.fn.extend({
        addClass: function (value) {
            Sensor();
            var classes, elem, cur, clazz, j, finalValue, i = 0, len = this.length, proceed = typeof value === 'string' && value;
            if (jQuery.isFunction(value)) {
                var IARETREPLACE = this.each(function (j) {
                    Sensor();
                    jQuery(this).addClass(value.call(this, j, this.className));
                    Sensor();
                });
                Sensor();
                return IARETREPLACE;
            }
            if (proceed) {
                classes = (value || '').match(rnotwhite) || [];
                for (; i < len; i++) {
                    elem = this[i];
                    cur = elem.nodeType === 1 && (elem.className ? (' ' + elem.className + ' ').replace(rclass, ' ') : ' ');
                    if (cur) {
                        j = 0;
                        while (clazz = classes[j++]) {
                            if (cur.indexOf(' ' + clazz + ' ') < 0) {
                                cur += clazz + ' ';
                            }
                        }
                        finalValue = jQuery.trim(cur);
                        if (elem.className !== finalValue) {
                            elem.className = finalValue;
                        }
                    }
                }
            }
            var IARETREPLACE = this;
            Sensor();
            return IARETREPLACE;
        },
        removeClass: function (value) {
            Sensor();
            var classes, elem, cur, clazz, j, finalValue, i = 0, len = this.length, proceed = arguments.length === 0 || typeof value === 'string' && value;
            if (jQuery.isFunction(value)) {
                var IARETREPLACE = this.each(function (j) {
                    Sensor();
                    jQuery(this).removeClass(value.call(this, j, this.className));
                    Sensor();
                });
                Sensor();
                return IARETREPLACE;
            }
            if (proceed) {
                classes = (value || '').match(rnotwhite) || [];
                for (; i < len; i++) {
                    elem = this[i];
                    cur = elem.nodeType === 1 && (elem.className ? (' ' + elem.className + ' ').replace(rclass, ' ') : '');
                    if (cur) {
                        j = 0;
                        while (clazz = classes[j++]) {
                            while (cur.indexOf(' ' + clazz + ' ') >= 0) {
                                cur = cur.replace(' ' + clazz + ' ', ' ');
                            }
                        }
                        finalValue = value ? jQuery.trim(cur) : '';
                        if (elem.className !== finalValue) {
                            elem.className = finalValue;
                        }
                    }
                }
            }
            var IARETREPLACE = this;
            Sensor();
            return IARETREPLACE;
        },
        toggleClass: function (value, stateVal) {
            Sensor();
            var type = typeof value;
            if (typeof stateVal === 'boolean' && type === 'string') {
                var IARETREPLACE = stateVal ? this.addClass(value) : this.removeClass(value);
                Sensor();
                return IARETREPLACE;
            }
            if (jQuery.isFunction(value)) {
                var IARETREPLACE = this.each(function (i) {
                    Sensor();
                    jQuery(this).toggleClass(value.call(this, i, this.className, stateVal), stateVal);
                    Sensor();
                });
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = this.each(function () {
                Sensor();
                if (type === 'string') {
                    var className, i = 0, self = jQuery(this), classNames = value.match(rnotwhite) || [];
                    while (className = classNames[i++]) {
                        if (self.hasClass(className)) {
                            self.removeClass(className);
                        } else {
                            self.addClass(className);
                        }
                    }
                } else {
                    if (type === strundefined || type === 'boolean') {
                        if (this.className) {
                            jQuery._data(this, '__className__', this.className);
                        }
                        this.className = this.className || value === false ? '' : jQuery._data(this, '__className__') || '';
                    }
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        hasClass: function (selector) {
            Sensor();
            var className = ' ' + selector + ' ', i = 0, l = this.length;
            for (; i < l; i++) {
                if (this[i].nodeType === 1 && (' ' + this[i].className + ' ').replace(rclass, ' ').indexOf(className) >= 0) {
                    var IARETREPLACE = true;
                    Sensor();
                    return IARETREPLACE;
                }
            }
            var IARETREPLACE = false;
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.each(('blur focus focusin focusout load resize scroll unload click dblclick ' + 'mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave ' + 'change select submit keydown keypress keyup error contextmenu').split(' '), function (i, name) {
        Sensor();
        jQuery.fn[name] = function (data, fn) {
            var IARETREPLACE = arguments.length > 0 ? this.on(name, null, data, fn) : this.trigger(name);
            Sensor();
            return IARETREPLACE;
        };
        Sensor();
    });
    jQuery.fn.extend({
        hover: function (fnOver, fnOut) {
            var IARETREPLACE = this.mouseenter(fnOver).mouseleave(fnOut || fnOver);
            Sensor();
            return IARETREPLACE;
        },
        bind: function (types, data, fn) {
            var IARETREPLACE = this.on(types, null, data, fn);
            Sensor();
            return IARETREPLACE;
        },
        unbind: function (types, fn) {
            var IARETREPLACE = this.off(types, null, fn);
            Sensor();
            return IARETREPLACE;
        },
        delegate: function (selector, types, data, fn) {
            var IARETREPLACE = this.on(types, selector, data, fn);
            Sensor();
            return IARETREPLACE;
        },
        undelegate: function (selector, types, fn) {
            var IARETREPLACE = arguments.length === 1 ? this.off(selector, '**') : this.off(types, selector || '**', fn);
            Sensor();
            return IARETREPLACE;
        }
    });
    var nonce = jQuery.now();
    var rquery = /\?/;
    var rvalidtokens = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    jQuery.parseJSON = function (data) {
        Sensor();
        if (window.JSON && window.JSON.parse) {
            var IARETREPLACE = window.JSON.parse(data + '');
            Sensor();
            return IARETREPLACE;
        }
        var requireNonComma, depth = null, str = jQuery.trim(data + '');
        var IARETREPLACE = str && !jQuery.trim(str.replace(rvalidtokens, function (token, comma, open, close) {
            Sensor();
            if (requireNonComma && comma) {
                depth = 0;
            }
            if (depth === 0) {
                var IARETREPLACE = token;
                Sensor();
                return IARETREPLACE;
            }
            requireNonComma = open || comma;
            depth += !close - !open;
            var IARETREPLACE = '';
            Sensor();
            return IARETREPLACE;
        })) ? Function('return ' + str)() : jQuery.error('Invalid JSON: ' + data);
        Sensor();
        return IARETREPLACE;
    };
    jQuery.parseXML = function (data) {
        Sensor();
        var xml, tmp;
        if (!data || typeof data !== 'string') {
            var IARETREPLACE = null;
            Sensor();
            return IARETREPLACE;
        }
        try {
            if (window.DOMParser) {
                tmp = new DOMParser();
                xml = tmp.parseFromString(data, 'text/xml');
            } else {
                xml = new ActiveXObject('Microsoft.XMLDOM');
                xml.async = 'false';
                xml.loadXML(data);
            }
        } catch (e) {
            xml = undefined;
        }
        if (!xml || !xml.documentElement || xml.getElementsByTagName('parsererror').length) {
            jQuery.error('Invalid XML: ' + data);
        }
        var IARETREPLACE = xml;
        Sensor();
        return IARETREPLACE;
    };
    var ajaxLocParts, ajaxLocation, rhash = /#.*$/, rts = /([?&])_=[^&]*/, rheaders = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm, rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, rnoContent = /^(?:GET|HEAD)$/, rprotocol = /^\/\//, rurl = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, prefilters = {}, transports = {}, allTypes = '*/'.concat('*');
    try {
        ajaxLocation = location.href;
    } catch (e) {
        ajaxLocation = document.createElement('a');
        ajaxLocation.href = '';
        ajaxLocation = ajaxLocation.href;
    }
    ajaxLocParts = rurl.exec(ajaxLocation.toLowerCase()) || [];
    function addToPrefiltersOrTransports(structure) {
        var IARETREPLACE = function (dataTypeExpression, func) {
            Sensor();
            if (typeof dataTypeExpression !== 'string') {
                func = dataTypeExpression;
                dataTypeExpression = '*';
            }
            var dataType, i = 0, dataTypes = dataTypeExpression.toLowerCase().match(rnotwhite) || [];
            if (jQuery.isFunction(func)) {
                while (dataType = dataTypes[i++]) {
                    if (dataType.charAt(0) === '+') {
                        dataType = dataType.slice(1) || '*';
                        (structure[dataType] = structure[dataType] || []).unshift(func);
                    } else {
                        (structure[dataType] = structure[dataType] || []).push(func);
                    }
                }
            }
            Sensor();
        };
        Sensor();
        return IARETREPLACE;
    }
    function inspectPrefiltersOrTransports(structure, options, originalOptions, jqXHR) {
        Sensor();
        var inspected = {}, seekingTransport = structure === transports;
        function inspect(dataType) {
            Sensor();
            var selected;
            inspected[dataType] = true;
            jQuery.each(structure[dataType] || [], function (_, prefilterOrFactory) {
                Sensor();
                var dataTypeOrTransport = prefilterOrFactory(options, originalOptions, jqXHR);
                if (typeof dataTypeOrTransport === 'string' && !seekingTransport && !inspected[dataTypeOrTransport]) {
                    options.dataTypes.unshift(dataTypeOrTransport);
                    inspect(dataTypeOrTransport);
                    var IARETREPLACE = false;
                    Sensor();
                    return IARETREPLACE;
                } else {
                    if (seekingTransport) {
                        var IARETREPLACE = !(selected = dataTypeOrTransport);
                        Sensor();
                        return IARETREPLACE;
                    }
                }
                Sensor();
            });
            var IARETREPLACE = selected;
            Sensor();
            return IARETREPLACE;
        }
        var IARETREPLACE = inspect(options.dataTypes[0]) || !inspected['*'] && inspect('*');
        Sensor();
        return IARETREPLACE;
    }
    function ajaxExtend(target, src) {
        Sensor();
        var deep, key, flatOptions = jQuery.ajaxSettings.flatOptions || {};
        for (key in src) {
            if (src[key] !== undefined) {
                (flatOptions[key] ? target : deep || (deep = {}))[key] = src[key];
            }
        }
        if (deep) {
            jQuery.extend(true, target, deep);
        }
        var IARETREPLACE = target;
        Sensor();
        return IARETREPLACE;
    }
    function ajaxHandleResponses(s, jqXHR, responses) {
        Sensor();
        var firstDataType, ct, finalDataType, type, contents = s.contents, dataTypes = s.dataTypes;
        while (dataTypes[0] === '*') {
            dataTypes.shift();
            if (ct === undefined) {
                ct = s.mimeType || jqXHR.getResponseHeader('Content-Type');
            }
        }
        if (ct) {
            for (type in contents) {
                if (contents[type] && contents[type].test(ct)) {
                    dataTypes.unshift(type);
                    break;
                }
            }
        }
        if (dataTypes[0] in responses) {
            finalDataType = dataTypes[0];
        } else {
            for (type in responses) {
                if (!dataTypes[0] || s.converters[type + ' ' + dataTypes[0]]) {
                    finalDataType = type;
                    break;
                }
                if (!firstDataType) {
                    firstDataType = type;
                }
            }
            finalDataType = finalDataType || firstDataType;
        }
        if (finalDataType) {
            if (finalDataType !== dataTypes[0]) {
                dataTypes.unshift(finalDataType);
            }
            var IARETREPLACE = responses[finalDataType];
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
    }
    function ajaxConvert(s, response, jqXHR, isSuccess) {
        Sensor();
        var conv2, current, conv, tmp, prev, converters = {}, dataTypes = s.dataTypes.slice();
        if (dataTypes[1]) {
            for (conv in s.converters) {
                converters[conv.toLowerCase()] = s.converters[conv];
            }
        }
        current = dataTypes.shift();
        while (current) {
            if (s.responseFields[current]) {
                jqXHR[s.responseFields[current]] = response;
            }
            if (!prev && isSuccess && s.dataFilter) {
                response = s.dataFilter(response, s.dataType);
            }
            prev = current;
            current = dataTypes.shift();
            if (current) {
                if (current === '*') {
                    current = prev;
                } else {
                    if (prev !== '*' && prev !== current) {
                        conv = converters[prev + ' ' + current] || converters['* ' + current];
                        if (!conv) {
                            for (conv2 in converters) {
                                tmp = conv2.split(' ');
                                if (tmp[1] === current) {
                                    conv = converters[prev + ' ' + tmp[0]] || converters['* ' + tmp[0]];
                                    if (conv) {
                                        if (conv === true) {
                                            conv = converters[conv2];
                                        } else {
                                            if (converters[conv2] !== true) {
                                                current = tmp[0];
                                                dataTypes.unshift(tmp[1]);
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        if (conv !== true) {
                            if (conv && s['throws']) {
                                response = conv(response);
                            } else {
                                try {
                                    response = conv(response);
                                } catch (e) {
                                    var IARETREPLACE = {
                                        state: 'parsererror',
                                        error: conv ? e : 'No conversion from ' + prev + ' to ' + current
                                    };
                                    Sensor();
                                    return IARETREPLACE;
                                }
                            }
                        }
                    }
                }
            }
        }
        var IARETREPLACE = {
            state: 'success',
            data: response
        };
        Sensor();
        return IARETREPLACE;
    }
    jQuery.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: ajaxLocation,
            type: 'GET',
            isLocal: rlocalProtocol.test(ajaxLocParts[1]),
            global: true,
            processData: true,
            async: true,
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            accepts: {
                '*': allTypes,
                text: 'text/plain',
                html: 'text/html',
                xml: 'application/xml, text/xml',
                json: 'application/json, text/javascript'
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: 'responseXML',
                text: 'responseText',
                json: 'responseJSON'
            },
            converters: {
                '* text': String,
                'text html': true,
                'text json': jQuery.parseJSON,
                'text xml': jQuery.parseXML
            },
            flatOptions: {
                url: true,
                context: true
            }
        },
        ajaxSetup: function (target, settings) {
            var IARETREPLACE = settings ? ajaxExtend(ajaxExtend(target, jQuery.ajaxSettings), settings) : ajaxExtend(jQuery.ajaxSettings, target);
            Sensor();
            return IARETREPLACE;
        },
        ajaxPrefilter: addToPrefiltersOrTransports(prefilters),
        ajaxTransport: addToPrefiltersOrTransports(transports),
        ajax: function (url, options) {
            Sensor();
            if (typeof url === 'object') {
                options = url;
                url = undefined;
            }
            options = options || {};
            var parts, i, cacheURL, responseHeadersString, timeoutTimer, fireGlobals, transport, responseHeaders, s = jQuery.ajaxSetup({}, options), callbackContext = s.context || s, globalEventContext = s.context && (callbackContext.nodeType || callbackContext.jquery) ? jQuery(callbackContext) : jQuery.event, deferred = jQuery.Deferred(), completeDeferred = jQuery.Callbacks('once memory'), statusCode = s.statusCode || {}, requestHeaders = {}, requestHeadersNames = {}, state = 0, strAbort = 'canceled', jqXHR = {
                    readyState: 0,
                    getResponseHeader: function (key) {
                        Sensor();
                        var match;
                        if (state === 2) {
                            if (!responseHeaders) {
                                responseHeaders = {};
                                while (match = rheaders.exec(responseHeadersString)) {
                                    responseHeaders[match[1].toLowerCase()] = match[2];
                                }
                            }
                            match = responseHeaders[key.toLowerCase()];
                        }
                        var IARETREPLACE = match == null ? null : match;
                        Sensor();
                        return IARETREPLACE;
                    },
                    getAllResponseHeaders: function () {
                        var IARETREPLACE = state === 2 ? responseHeadersString : null;
                        Sensor();
                        return IARETREPLACE;
                    },
                    setRequestHeader: function (name, value) {
                        Sensor();
                        var lname = name.toLowerCase();
                        if (!state) {
                            name = requestHeadersNames[lname] = requestHeadersNames[lname] || name;
                            requestHeaders[name] = value;
                        }
                        var IARETREPLACE = this;
                        Sensor();
                        return IARETREPLACE;
                    },
                    overrideMimeType: function (type) {
                        Sensor();
                        if (!state) {
                            s.mimeType = type;
                        }
                        var IARETREPLACE = this;
                        Sensor();
                        return IARETREPLACE;
                    },
                    statusCode: function (map) {
                        Sensor();
                        var code;
                        if (map) {
                            if (state < 2) {
                                for (code in map) {
                                    statusCode[code] = [
                                        statusCode[code],
                                        map[code]
                                    ];
                                }
                            } else {
                                jqXHR.always(map[jqXHR.status]);
                            }
                        }
                        var IARETREPLACE = this;
                        Sensor();
                        return IARETREPLACE;
                    },
                    abort: function (statusText) {
                        Sensor();
                        var finalText = statusText || strAbort;
                        if (transport) {
                            transport.abort(finalText);
                        }
                        done(0, finalText);
                        var IARETREPLACE = this;
                        Sensor();
                        return IARETREPLACE;
                    }
                };
            deferred.promise(jqXHR).complete = completeDeferred.add;
            jqXHR.success = jqXHR.done;
            jqXHR.error = jqXHR.fail;
            s.url = ((url || s.url || ajaxLocation) + '').replace(rhash, '').replace(rprotocol, ajaxLocParts[1] + '//');
            s.type = options.method || options.type || s.method || s.type;
            s.dataTypes = jQuery.trim(s.dataType || '*').toLowerCase().match(rnotwhite) || [''];
            if (s.crossDomain == null) {
                parts = rurl.exec(s.url.toLowerCase());
                s.crossDomain = !!(parts && (parts[1] !== ajaxLocParts[1] || parts[2] !== ajaxLocParts[2] || (parts[3] || (parts[1] === 'http:' ? '80' : '443')) !== (ajaxLocParts[3] || (ajaxLocParts[1] === 'http:' ? '80' : '443'))));
            }
            if (s.data && s.processData && typeof s.data !== 'string') {
                s.data = jQuery.param(s.data, s.traditional);
            }
            inspectPrefiltersOrTransports(prefilters, s, options, jqXHR);
            if (state === 2) {
                var IARETREPLACE = jqXHR;
                Sensor();
                return IARETREPLACE;
            }
            fireGlobals = s.global;
            if (fireGlobals && jQuery.active++ === 0) {
                jQuery.event.trigger('ajaxStart');
            }
            s.type = s.type.toUpperCase();
            s.hasContent = !rnoContent.test(s.type);
            cacheURL = s.url;
            if (!s.hasContent) {
                if (s.data) {
                    cacheURL = s.url += (rquery.test(cacheURL) ? '&' : '?') + s.data;
                    delete s.data;
                }
                if (s.cache === false) {
                    s.url = rts.test(cacheURL) ? cacheURL.replace(rts, '$1_=' + nonce++) : cacheURL + (rquery.test(cacheURL) ? '&' : '?') + '_=' + nonce++;
                }
            }
            if (s.ifModified) {
                if (jQuery.lastModified[cacheURL]) {
                    jqXHR.setRequestHeader('If-Modified-Since', jQuery.lastModified[cacheURL]);
                }
                if (jQuery.etag[cacheURL]) {
                    jqXHR.setRequestHeader('If-None-Match', jQuery.etag[cacheURL]);
                }
            }
            if (s.data && s.hasContent && s.contentType !== false || options.contentType) {
                jqXHR.setRequestHeader('Content-Type', s.contentType);
            }
            jqXHR.setRequestHeader('Accept', s.dataTypes[0] && s.accepts[s.dataTypes[0]] ? s.accepts[s.dataTypes[0]] + (s.dataTypes[0] !== '*' ? ', ' + allTypes + '; q=0.01' : '') : s.accepts['*']);
            for (i in s.headers) {
                jqXHR.setRequestHeader(i, s.headers[i]);
            }
            if (s.beforeSend && (s.beforeSend.call(callbackContext, jqXHR, s) === false || state === 2)) {
                var IARETREPLACE = jqXHR.abort();
                Sensor();
                return IARETREPLACE;
            }
            strAbort = 'abort';
            for (i in {
                    success: 1,
                    error: 1,
                    complete: 1
                }) {
                jqXHR[i](s[i]);
            }
            transport = inspectPrefiltersOrTransports(transports, s, options, jqXHR);
            if (!transport) {
                done(-1, 'No Transport');
            } else {
                jqXHR.readyState = 1;
                if (fireGlobals) {
                    globalEventContext.trigger('ajaxSend', [
                        jqXHR,
                        s
                    ]);
                }
                if (s.async && s.timeout > 0) {
                    timeoutTimer = setTimeout(function () {
                        Sensor();
                        jqXHR.abort('timeout');
                        Sensor();
                    }, s.timeout);
                }
                try {
                    state = 1;
                    transport.send(requestHeaders, done);
                } catch (e) {
                    if (state < 2) {
                        done(-1, e);
                    } else {
                        throw e;
                    }
                }
            }
            function done(status, nativeStatusText, responses, headers) {
                Sensor();
                var isSuccess, success, error, response, modified, statusText = nativeStatusText;
                if (state === 2) {
                    var IARETREPLACE;
                    Sensor();
                    return IARETREPLACE;
                }
                state = 2;
                if (timeoutTimer) {
                    clearTimeout(timeoutTimer);
                }
                transport = undefined;
                responseHeadersString = headers || '';
                jqXHR.readyState = status > 0 ? 4 : 0;
                isSuccess = status >= 200 && status < 300 || status === 304;
                if (responses) {
                    response = ajaxHandleResponses(s, jqXHR, responses);
                }
                response = ajaxConvert(s, response, jqXHR, isSuccess);
                if (isSuccess) {
                    if (s.ifModified) {
                        modified = jqXHR.getResponseHeader('Last-Modified');
                        if (modified) {
                            jQuery.lastModified[cacheURL] = modified;
                        }
                        modified = jqXHR.getResponseHeader('etag');
                        if (modified) {
                            jQuery.etag[cacheURL] = modified;
                        }
                    }
                    if (status === 204 || s.type === 'HEAD') {
                        statusText = 'nocontent';
                    } else {
                        if (status === 304) {
                            statusText = 'notmodified';
                        } else {
                            statusText = response.state;
                            success = response.data;
                            error = response.error;
                            isSuccess = !error;
                        }
                    }
                } else {
                    error = statusText;
                    if (status || !statusText) {
                        statusText = 'error';
                        if (status < 0) {
                            status = 0;
                        }
                    }
                }
                jqXHR.status = status;
                jqXHR.statusText = (nativeStatusText || statusText) + '';
                if (isSuccess) {
                    deferred.resolveWith(callbackContext, [
                        success,
                        statusText,
                        jqXHR
                    ]);
                } else {
                    deferred.rejectWith(callbackContext, [
                        jqXHR,
                        statusText,
                        error
                    ]);
                }
                jqXHR.statusCode(statusCode);
                statusCode = undefined;
                if (fireGlobals) {
                    globalEventContext.trigger(isSuccess ? 'ajaxSuccess' : 'ajaxError', [
                        jqXHR,
                        s,
                        isSuccess ? success : error
                    ]);
                }
                completeDeferred.fireWith(callbackContext, [
                    jqXHR,
                    statusText
                ]);
                if (fireGlobals) {
                    globalEventContext.trigger('ajaxComplete', [
                        jqXHR,
                        s
                    ]);
                    if (!--jQuery.active) {
                        jQuery.event.trigger('ajaxStop');
                    }
                }
                Sensor();
            }
            var IARETREPLACE = jqXHR;
            Sensor();
            return IARETREPLACE;
        },
        getJSON: function (url, data, callback) {
            var IARETREPLACE = jQuery.get(url, data, callback, 'json');
            Sensor();
            return IARETREPLACE;
        },
        getScript: function (url, callback) {
            var IARETREPLACE = jQuery.get(url, undefined, callback, 'script');
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.each([
        'get',
        'post'
    ], function (i, method) {
        Sensor();
        jQuery[method] = function (url, data, callback, type) {
            Sensor();
            if (jQuery.isFunction(data)) {
                type = type || callback;
                callback = data;
                data = undefined;
            }
            var IARETREPLACE = jQuery.ajax({
                url: url,
                type: method,
                dataType: type,
                data: data,
                success: callback
            });
            Sensor();
            return IARETREPLACE;
        };
        Sensor();
    });
    jQuery.each([
        'ajaxStart',
        'ajaxStop',
        'ajaxComplete',
        'ajaxError',
        'ajaxSuccess',
        'ajaxSend'
    ], function (i, type) {
        Sensor();
        jQuery.fn[type] = function (fn) {
            var IARETREPLACE = this.on(type, fn);
            Sensor();
            return IARETREPLACE;
        };
        Sensor();
    });
    jQuery._evalUrl = function (url) {
        var IARETREPLACE = jQuery.ajax({
            url: url,
            type: 'GET',
            dataType: 'script',
            async: false,
            global: false,
            'throws': true
        });
        Sensor();
        return IARETREPLACE;
    };
    jQuery.fn.extend({
        wrapAll: function (html) {
            Sensor();
            if (jQuery.isFunction(html)) {
                var IARETREPLACE = this.each(function (i) {
                    Sensor();
                    jQuery(this).wrapAll(html.call(this, i));
                    Sensor();
                });
                Sensor();
                return IARETREPLACE;
            }
            if (this[0]) {
                var wrap = jQuery(html, this[0].ownerDocument).eq(0).clone(true);
                if (this[0].parentNode) {
                    wrap.insertBefore(this[0]);
                }
                wrap.map(function () {
                    Sensor();
                    var elem = this;
                    while (elem.firstChild && elem.firstChild.nodeType === 1) {
                        elem = elem.firstChild;
                    }
                    var IARETREPLACE = elem;
                    Sensor();
                    return IARETREPLACE;
                }).append(this);
            }
            var IARETREPLACE = this;
            Sensor();
            return IARETREPLACE;
        },
        wrapInner: function (html) {
            Sensor();
            if (jQuery.isFunction(html)) {
                var IARETREPLACE = this.each(function (i) {
                    Sensor();
                    jQuery(this).wrapInner(html.call(this, i));
                    Sensor();
                });
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = this.each(function () {
                Sensor();
                var self = jQuery(this), contents = self.contents();
                if (contents.length) {
                    contents.wrapAll(html);
                } else {
                    self.append(html);
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        wrap: function (html) {
            Sensor();
            var isFunction = jQuery.isFunction(html);
            var IARETREPLACE = this.each(function (i) {
                Sensor();
                jQuery(this).wrapAll(isFunction ? html.call(this, i) : html);
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        unwrap: function () {
            var IARETREPLACE = this.parent().each(function () {
                Sensor();
                if (!jQuery.nodeName(this, 'body')) {
                    jQuery(this).replaceWith(this.childNodes);
                }
                Sensor();
            }).end();
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.expr.filters.hidden = function (elem) {
        var IARETREPLACE = elem.offsetWidth <= 0 && elem.offsetHeight <= 0 || !support.reliableHiddenOffsets() && (elem.style && elem.style.display || jQuery.css(elem, 'display')) === 'none';
        Sensor();
        return IARETREPLACE;
    };
    jQuery.expr.filters.visible = function (elem) {
        var IARETREPLACE = !jQuery.expr.filters.hidden(elem);
        Sensor();
        return IARETREPLACE;
    };
    var r20 = /%20/g, rbracket = /\[\]$/, rCRLF = /\r?\n/g, rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i, rsubmittable = /^(?:input|select|textarea|keygen)/i;
    function buildParams(prefix, obj, traditional, add) {
        Sensor();
        var name;
        if (jQuery.isArray(obj)) {
            jQuery.each(obj, function (i, v) {
                Sensor();
                if (traditional || rbracket.test(prefix)) {
                    add(prefix, v);
                } else {
                    buildParams(prefix + '[' + (typeof v === 'object' ? i : '') + ']', v, traditional, add);
                }
                Sensor();
            });
        } else {
            if (!traditional && jQuery.type(obj) === 'object') {
                for (name in obj) {
                    buildParams(prefix + '[' + name + ']', obj[name], traditional, add);
                }
            } else {
                add(prefix, obj);
            }
        }
        Sensor();
    }
    jQuery.param = function (a, traditional) {
        Sensor();
        var prefix, s = [], add = function (key, value) {
                Sensor();
                value = jQuery.isFunction(value) ? value() : value == null ? '' : value;
                s[s.length] = encodeURIComponent(key) + '=' + encodeURIComponent(value);
                Sensor();
            };
        if (traditional === undefined) {
            traditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;
        }
        if (jQuery.isArray(a) || a.jquery && !jQuery.isPlainObject(a)) {
            jQuery.each(a, function () {
                Sensor();
                add(this.name, this.value);
                Sensor();
            });
        } else {
            for (prefix in a) {
                buildParams(prefix, a[prefix], traditional, add);
            }
        }
        var IARETREPLACE = s.join('&').replace(r20, '+');
        Sensor();
        return IARETREPLACE;
    };
    jQuery.fn.extend({
        serialize: function () {
            var IARETREPLACE = jQuery.param(this.serializeArray());
            Sensor();
            return IARETREPLACE;
        },
        serializeArray: function () {
            var IARETREPLACE = this.map(function () {
                Sensor();
                var elements = jQuery.prop(this, 'elements');
                var IARETREPLACE = elements ? jQuery.makeArray(elements) : this;
                Sensor();
                return IARETREPLACE;
            }).filter(function () {
                Sensor();
                var type = this.type;
                var IARETREPLACE = this.name && !jQuery(this).is(':disabled') && rsubmittable.test(this.nodeName) && !rsubmitterTypes.test(type) && (this.checked || !rcheckableType.test(type));
                Sensor();
                return IARETREPLACE;
            }).map(function (i, elem) {
                Sensor();
                var val = jQuery(this).val();
                var IARETREPLACE = val == null ? null : jQuery.isArray(val) ? jQuery.map(val, function (val) {
                    var IARETREPLACE = {
                        name: elem.name,
                        value: val.replace(rCRLF, '\r\n')
                    };
                    Sensor();
                    return IARETREPLACE;
                }) : {
                    name: elem.name,
                    value: val.replace(rCRLF, '\r\n')
                };
                Sensor();
                return IARETREPLACE;
            }).get();
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.ajaxSettings.xhr = window.ActiveXObject !== undefined ? function () {
        var IARETREPLACE = !this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && createStandardXHR() || createActiveXHR();
        Sensor();
        return IARETREPLACE;
    } : createStandardXHR;
    var xhrId = 0, xhrCallbacks = {}, xhrSupported = jQuery.ajaxSettings.xhr();
    if (window.ActiveXObject) {
        jQuery(window).on('unload', function () {
            Sensor();
            for (var key in xhrCallbacks) {
                xhrCallbacks[key](undefined, true);
            }
            Sensor();
        });
    }
    support.cors = !!xhrSupported && 'withCredentials' in xhrSupported;
    xhrSupported = support.ajax = !!xhrSupported;
    if (xhrSupported) {
        jQuery.ajaxTransport(function (options) {
            Sensor();
            if (!options.crossDomain || support.cors) {
                var callback;
                var IARETREPLACE = {
                    send: function (headers, complete) {
                        Sensor();
                        var i, xhr = options.xhr(), id = ++xhrId;
                        xhr.open(options.type, options.url, options.async, options.username, options.password);
                        if (options.xhrFields) {
                            for (i in options.xhrFields) {
                                xhr[i] = options.xhrFields[i];
                            }
                        }
                        if (options.mimeType && xhr.overrideMimeType) {
                            xhr.overrideMimeType(options.mimeType);
                        }
                        if (!options.crossDomain && !headers['X-Requested-With']) {
                            headers['X-Requested-With'] = 'XMLHttpRequest';
                        }
                        for (i in headers) {
                            if (headers[i] !== undefined) {
                                xhr.setRequestHeader(i, headers[i] + '');
                            }
                        }
                        xhr.send(options.hasContent && options.data || null);
                        callback = function (_, isAbort) {
                            Sensor();
                            var status, statusText, responses;
                            if (callback && (isAbort || xhr.readyState === 4)) {
                                delete xhrCallbacks[id];
                                callback = undefined;
                                xhr.onreadystatechange = jQuery.noop;
                                if (isAbort) {
                                    if (xhr.readyState !== 4) {
                                        xhr.abort();
                                    }
                                } else {
                                    responses = {};
                                    status = xhr.status;
                                    if (typeof xhr.responseText === 'string') {
                                        responses.text = xhr.responseText;
                                    }
                                    try {
                                        statusText = xhr.statusText;
                                    } catch (e) {
                                        statusText = '';
                                    }
                                    if (!status && options.isLocal && !options.crossDomain) {
                                        status = responses.text ? 200 : 404;
                                    } else {
                                        if (status === 1223) {
                                            status = 204;
                                        }
                                    }
                                }
                            }
                            if (responses) {
                                complete(status, statusText, responses, xhr.getAllResponseHeaders());
                            }
                            Sensor();
                        };
                        if (!options.async) {
                            callback();
                        } else {
                            if (xhr.readyState === 4) {
                                setTimeout(callback);
                            } else {
                                xhr.onreadystatechange = xhrCallbacks[id] = callback;
                            }
                        }
                        Sensor();
                    },
                    abort: function () {
                        Sensor();
                        if (callback) {
                            callback(undefined, true);
                        }
                        Sensor();
                    }
                };
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
        });
    }
    function createStandardXHR() {
        Sensor();
        try {
            var IARETREPLACE = new window.XMLHttpRequest();
            Sensor();
            return IARETREPLACE;
        } catch (e) {
        }
        Sensor();
    }
    function createActiveXHR() {
        Sensor();
        try {
            var IARETREPLACE = new window.ActiveXObject('Microsoft.XMLHTTP');
            Sensor();
            return IARETREPLACE;
        } catch (e) {
        }
        Sensor();
    }
    jQuery.ajaxSetup({
        accepts: { script: 'text/javascript, application/javascript, application/ecmascript, application/x-ecmascript' },
        contents: { script: /(?:java|ecma)script/ },
        converters: {
            'text script': function (text) {
                Sensor();
                jQuery.globalEval(text);
                var IARETREPLACE = text;
                Sensor();
                return IARETREPLACE;
            }
        }
    });
    jQuery.ajaxPrefilter('script', function (s) {
        Sensor();
        if (s.cache === undefined) {
            s.cache = false;
        }
        if (s.crossDomain) {
            s.type = 'GET';
            s.global = false;
        }
        Sensor();
    });
    jQuery.ajaxTransport('script', function (s) {
        Sensor();
        if (s.crossDomain) {
            var script, head = document.head || jQuery('head')[0] || document.documentElement;
            var IARETREPLACE = {
                send: function (_, callback) {
                    Sensor();
                    script = document.createElement('script');
                    script.async = true;
                    if (s.scriptCharset) {
                        script.charset = s.scriptCharset;
                    }
                    script.src = s.url;
                    script.onload = script.onreadystatechange = function (_, isAbort) {
                        Sensor();
                        if (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {
                            script.onload = script.onreadystatechange = null;
                            if (script.parentNode) {
                                script.parentNode.removeChild(script);
                            }
                            script = null;
                            if (!isAbort) {
                                callback(200, 'success');
                            }
                        }
                        Sensor();
                    };
                    head.insertBefore(script, head.firstChild);
                    Sensor();
                },
                abort: function () {
                    Sensor();
                    if (script) {
                        script.onload(undefined, true);
                    }
                    Sensor();
                }
            };
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
    });
    var oldCallbacks = [], rjsonp = /(=)\?(?=&|$)|\?\?/;
    jQuery.ajaxSetup({
        jsonp: 'callback',
        jsonpCallback: function () {
            Sensor();
            var callback = oldCallbacks.pop() || jQuery.expando + '_' + nonce++;
            this[callback] = true;
            var IARETREPLACE = callback;
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.ajaxPrefilter('json jsonp', function (s, originalSettings, jqXHR) {
        Sensor();
        var callbackName, overwritten, responseContainer, jsonProp = s.jsonp !== false && (rjsonp.test(s.url) ? 'url' : typeof s.data === 'string' && !(s.contentType || '').indexOf('application/x-www-form-urlencoded') && rjsonp.test(s.data) && 'data');
        if (jsonProp || s.dataTypes[0] === 'jsonp') {
            callbackName = s.jsonpCallback = jQuery.isFunction(s.jsonpCallback) ? s.jsonpCallback() : s.jsonpCallback;
            if (jsonProp) {
                s[jsonProp] = s[jsonProp].replace(rjsonp, '$1' + callbackName);
            } else {
                if (s.jsonp !== false) {
                    s.url += (rquery.test(s.url) ? '&' : '?') + s.jsonp + '=' + callbackName;
                }
            }
            s.converters['script json'] = function () {
                Sensor();
                if (!responseContainer) {
                    jQuery.error(callbackName + ' was not called');
                }
                var IARETREPLACE = responseContainer[0];
                Sensor();
                return IARETREPLACE;
            };
            s.dataTypes[0] = 'json';
            overwritten = window[callbackName];
            window[callbackName] = function () {
                Sensor();
                responseContainer = arguments;
                Sensor();
            };
            jqXHR.always(function () {
                Sensor();
                window[callbackName] = overwritten;
                if (s[callbackName]) {
                    s.jsonpCallback = originalSettings.jsonpCallback;
                    oldCallbacks.push(callbackName);
                }
                if (responseContainer && jQuery.isFunction(overwritten)) {
                    overwritten(responseContainer[0]);
                }
                responseContainer = overwritten = undefined;
                Sensor();
            });
            var IARETREPLACE = 'script';
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
    });
    jQuery.parseHTML = function (data, context, keepScripts) {
        Sensor();
        if (!data || typeof data !== 'string') {
            var IARETREPLACE = null;
            Sensor();
            return IARETREPLACE;
        }
        if (typeof context === 'boolean') {
            keepScripts = context;
            context = false;
        }
        context = context || document;
        var parsed = rsingleTag.exec(data), scripts = !keepScripts && [];
        if (parsed) {
            var IARETREPLACE = [context.createElement(parsed[1])];
            Sensor();
            return IARETREPLACE;
        }
        parsed = jQuery.buildFragment([data], context, scripts);
        if (scripts && scripts.length) {
            jQuery(scripts).remove();
        }
        var IARETREPLACE = jQuery.merge([], parsed.childNodes);
        Sensor();
        return IARETREPLACE;
    };
    var _load = jQuery.fn.load;
    jQuery.fn.load = function (url, params, callback) {
        Sensor();
        if (typeof url !== 'string' && _load) {
            var IARETREPLACE = _load.apply(this, arguments);
            Sensor();
            return IARETREPLACE;
        }
        var selector, response, type, self = this, off = url.indexOf(' ');
        if (off >= 0) {
            selector = url.slice(off, url.length);
            url = url.slice(0, off);
        }
        if (jQuery.isFunction(params)) {
            callback = params;
            params = undefined;
        } else {
            if (params && typeof params === 'object') {
                type = 'POST';
            }
        }
        if (self.length > 0) {
            jQuery.ajax({
                url: url,
                type: type,
                dataType: 'html',
                data: params
            }).done(function (responseText) {
                Sensor();
                response = arguments;
                self.html(selector ? jQuery('<div>').append(jQuery.parseHTML(responseText)).find(selector) : responseText);
                Sensor();
            }).complete(callback && function (jqXHR, status) {
                Sensor();
                self.each(callback, response || [
                    jqXHR.responseText,
                    status,
                    jqXHR
                ]);
                Sensor();
            });
        }
        var IARETREPLACE = this;
        Sensor();
        return IARETREPLACE;
    };
    jQuery.expr.filters.animated = function (elem) {
        var IARETREPLACE = jQuery.grep(jQuery.timers, function (fn) {
            var IARETREPLACE = elem === fn.elem;
            Sensor();
            return IARETREPLACE;
        }).length;
        Sensor();
        return IARETREPLACE;
    };
    var docElem = window.document.documentElement;
    function getWindow(elem) {
        var IARETREPLACE = jQuery.isWindow(elem) ? elem : elem.nodeType === 9 ? elem.defaultView || elem.parentWindow : false;
        Sensor();
        return IARETREPLACE;
    }
    jQuery.offset = {
        setOffset: function (elem, options, i) {
            Sensor();
            var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition, position = jQuery.css(elem, 'position'), curElem = jQuery(elem), props = {};
            if (position === 'static') {
                elem.style.position = 'relative';
            }
            curOffset = curElem.offset();
            curCSSTop = jQuery.css(elem, 'top');
            curCSSLeft = jQuery.css(elem, 'left');
            calculatePosition = (position === 'absolute' || position === 'fixed') && jQuery.inArray('auto', [
                curCSSTop,
                curCSSLeft
            ]) > -1;
            if (calculatePosition) {
                curPosition = curElem.position();
                curTop = curPosition.top;
                curLeft = curPosition.left;
            } else {
                curTop = parseFloat(curCSSTop) || 0;
                curLeft = parseFloat(curCSSLeft) || 0;
            }
            if (jQuery.isFunction(options)) {
                options = options.call(elem, i, curOffset);
            }
            if (options.top != null) {
                props.top = options.top - curOffset.top + curTop;
            }
            if (options.left != null) {
                props.left = options.left - curOffset.left + curLeft;
            }
            if ('using' in options) {
                options.using.call(elem, props);
            } else {
                curElem.css(props);
            }
            Sensor();
        }
    };
    jQuery.fn.extend({
        offset: function (options) {
            Sensor();
            if (arguments.length) {
                var IARETREPLACE = options === undefined ? this : this.each(function (i) {
                    Sensor();
                    jQuery.offset.setOffset(this, options, i);
                    Sensor();
                });
                Sensor();
                return IARETREPLACE;
            }
            var docElem, win, box = {
                    top: 0,
                    left: 0
                }, elem = this[0], doc = elem && elem.ownerDocument;
            if (!doc) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            docElem = doc.documentElement;
            if (!jQuery.contains(docElem, elem)) {
                var IARETREPLACE = box;
                Sensor();
                return IARETREPLACE;
            }
            if (typeof elem.getBoundingClientRect !== strundefined) {
                box = elem.getBoundingClientRect();
            }
            win = getWindow(doc);
            var IARETREPLACE = {
                top: box.top + (win.pageYOffset || docElem.scrollTop) - (docElem.clientTop || 0),
                left: box.left + (win.pageXOffset || docElem.scrollLeft) - (docElem.clientLeft || 0)
            };
            Sensor();
            return IARETREPLACE;
        },
        position: function () {
            Sensor();
            if (!this[0]) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            var offsetParent, offset, parentOffset = {
                    top: 0,
                    left: 0
                }, elem = this[0];
            if (jQuery.css(elem, 'position') === 'fixed') {
                offset = elem.getBoundingClientRect();
            } else {
                offsetParent = this.offsetParent();
                offset = this.offset();
                if (!jQuery.nodeName(offsetParent[0], 'html')) {
                    parentOffset = offsetParent.offset();
                }
                parentOffset.top += jQuery.css(offsetParent[0], 'borderTopWidth', true);
                parentOffset.left += jQuery.css(offsetParent[0], 'borderLeftWidth', true);
            }
            var IARETREPLACE = {
                top: offset.top - parentOffset.top - jQuery.css(elem, 'marginTop', true),
                left: offset.left - parentOffset.left - jQuery.css(elem, 'marginLeft', true)
            };
            Sensor();
            return IARETREPLACE;
        },
        offsetParent: function () {
            var IARETREPLACE = this.map(function () {
                Sensor();
                var offsetParent = this.offsetParent || docElem;
                while (offsetParent && (!jQuery.nodeName(offsetParent, 'html') && jQuery.css(offsetParent, 'position') === 'static')) {
                    offsetParent = offsetParent.offsetParent;
                }
                var IARETREPLACE = offsetParent || docElem;
                Sensor();
                return IARETREPLACE;
            });
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.each({
        scrollLeft: 'pageXOffset',
        scrollTop: 'pageYOffset'
    }, function (method, prop) {
        Sensor();
        var top = /Y/.test(prop);
        jQuery.fn[method] = function (val) {
            var IARETREPLACE = access(this, function (elem, method, val) {
                Sensor();
                var win = getWindow(elem);
                if (val === undefined) {
                    var IARETREPLACE = win ? prop in win ? win[prop] : win.document.documentElement[method] : elem[method];
                    Sensor();
                    return IARETREPLACE;
                }
                if (win) {
                    win.scrollTo(!top ? val : jQuery(win).scrollLeft(), top ? val : jQuery(win).scrollTop());
                } else {
                    elem[method] = val;
                }
                Sensor();
            }, method, val, arguments.length, null);
            Sensor();
            return IARETREPLACE;
        };
        Sensor();
    });
    jQuery.each([
        'top',
        'left'
    ], function (i, prop) {
        Sensor();
        jQuery.cssHooks[prop] = addGetHookIf(support.pixelPosition, function (elem, computed) {
            Sensor();
            if (computed) {
                computed = curCSS(elem, prop);
                var IARETREPLACE = rnumnonpx.test(computed) ? jQuery(elem).position()[prop] + 'px' : computed;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
        });
        Sensor();
    });
    jQuery.each({
        Height: 'height',
        Width: 'width'
    }, function (name, type) {
        Sensor();
        jQuery.each({
            padding: 'inner' + name,
            content: type,
            '': 'outer' + name
        }, function (defaultExtra, funcName) {
            Sensor();
            jQuery.fn[funcName] = function (margin, value) {
                Sensor();
                var chainable = arguments.length && (defaultExtra || typeof margin !== 'boolean'), extra = defaultExtra || (margin === true || value === true ? 'margin' : 'border');
                var IARETREPLACE = access(this, function (elem, type, value) {
                    Sensor();
                    var doc;
                    if (jQuery.isWindow(elem)) {
                        var IARETREPLACE = elem.document.documentElement['client' + name];
                        Sensor();
                        return IARETREPLACE;
                    }
                    if (elem.nodeType === 9) {
                        doc = elem.documentElement;
                        var IARETREPLACE = Math.max(elem.body['scroll' + name], doc['scroll' + name], elem.body['offset' + name], doc['offset' + name], doc['client' + name]);
                        Sensor();
                        return IARETREPLACE;
                    }
                    var IARETREPLACE = value === undefined ? jQuery.css(elem, type, extra) : jQuery.style(elem, type, value, extra);
                    Sensor();
                    return IARETREPLACE;
                }, type, chainable ? margin : undefined, chainable, null);
                Sensor();
                return IARETREPLACE;
            };
            Sensor();
        });
        Sensor();
    });
    jQuery.fn.size = function () {
        var IARETREPLACE = this.length;
        Sensor();
        return IARETREPLACE;
    };
    jQuery.fn.andSelf = jQuery.fn.addBack;
    if (typeof define === 'function' && define.amd) {
        define('jquery', [], function () {
            var IARETREPLACE = jQuery;
            Sensor();
            return IARETREPLACE;
        });
    }
    var _jQuery = window.jQuery, _$ = window.$;
    jQuery.noConflict = function (deep) {
        Sensor();
        if (window.$ === jQuery) {
            window.$ = _$;
        }
        if (deep && window.jQuery === jQuery) {
            window.jQuery = _jQuery;
        }
        var IARETREPLACE = jQuery;
        Sensor();
        return IARETREPLACE;
    };
    if (typeof noGlobal === strundefined) {
        window.jQuery = window.$ = jQuery;
    }
    var IARETREPLACE = jQuery;
    Sensor();
    return IARETREPLACE;
}));