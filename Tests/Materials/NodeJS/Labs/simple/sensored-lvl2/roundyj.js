Sensor();
var imagedir = 'images/roundy/';
function preload() {
    Sensor();
    this.length = preload.arguments.length;
    Sensor();
    for (var i = 0; i < this.length; i++) {
        Sensor();
        this[i] = new Image();
        this[i].src = imagedir + preload.arguments[i];
    }
    Sensor();
}
var pics = new preload('rndy1.gif', 'rndr1.gif', 'rndb1.gif', 'rndy2.gif', 'rndr2.gif', 'rndb2.gif', 'rndy3.gif', 'rndr3.gif', 'rndb3.gif', 'rndy4.gif', 'rndr4.gif', 'rndb4.gif', '../buttons/blank.gif', '../buttons/edit.gif', '../buttons/edit2.gif', '../buttons/solve.gif', '../buttons/solve2.gif', '../buttons/play.gif', '../buttons/pause.gif');
var posit = new Array();
var pairs = new Array(4, 1, 6, 3, 0, 9, 2, 11, 8, 5, 10, 7);
function initbrd() {
    Sensor();
    posit = new Array(0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2);
    Sensor();
}
initbrd();
document.write('<table cellpadding=0 cellspacing=0 border=0 background=\'images/roundy/rndback.gif\'><tr><td><table cellpadding=0 cellspacing=0 border=0 background=\'images/buttons/blank.gif\'>');
Sensor();
for (var i = 0; i < 2; i++) {
    Sensor();
    document.writeln('<tr>');
    Sensor();
    for (var j = 0; j < 3; j++) {
        Sensor();
        if (j) {
            Sensor();
            document.write('<td><img src=\'images/buttons/blank.gif\' width=11 height=' + (50 - i) + '></td>');
        }
        Sensor();
        for (var k = 0; k < 2; k++) {
            Sensor();
            document.write('<td><a href=\'javascript:clicked(' + j + ',' + (i + i + k) + ');\'><img src=\'images/buttons/blank.gif\' width=' + (50 - k) + ' height=' + (50 - i) + ' name=\'pc' + (j * 4 + i + i + k) + '\' border=0></a></td>');
        }
    }
    Sensor();
    document.write('</tr>');
}
Sensor();
document.write('</table></td></tr></table>');
document.write('<p><a href=\'javascript:mix();\'><img src=\'images/buttons/mix.gif\' height=16 width=48 border=0></a>');
document.write('<a href=\'javascript:reset();\'><img src=\'images/buttons/reset.gif\' height=16 width=48 border=0></a>');
document.write('<a href=\'javascript:edit();\'><img src=\'images/buttons/edit.gif\' height=16 width=48 border=0 name=\'edit\'></a>');
document.writeln('<a href=\'javascript:help();\'><img src=\'images/buttons/help.gif\' height=16 width=48 border=0></a><br>');
document.write('<a href=\'javascript:solve();\'><img src=\'images/buttons/solve.gif\' height=16 width=48 border=0 name=\'solve\'></a>');
document.writeln('<a href=\'javascript:solplay();\'><img src=\'images/buttons/play.gif\' height=16 width=48 border=0 name=\'play\'></a>');
var mode = 0;
var seq = new Array();
var edt;
function display() {
    Sensor();
    displaybrd();
    displaybut();
    Sensor();
    if (mode == 1 && solved()) {
        Sensor();
        alert('You solved it!\nYou don\'t get a prize for this though!');
        mode = 0;
    }
    Sensor();
}
function displaybut() {
    Sensor();
    if (mode == 2) {
        Sensor();
        document.images['edit'].src = pics[14].src;
    } else {
        Sensor();
        document.images['edit'].src = pics[13].src;
    }
    Sensor();
    if (mode >= 3) {
        Sensor();
        document.images['solve'].src = pics[16].src;
    } else {
        Sensor();
        document.images['solve'].src = pics[15].src;
    }
    Sensor();
    if (mode == 4) {
        Sensor();
        document.images['play'].src = pics[18].src;
    } else {
        Sensor();
        document.images['play'].src = pics[17].src;
    }
    Sensor();
}
function displaybrd() {
    Sensor();
    var c = 0;
    Sensor();
    for (var i = 0; i < 3; i++) {
        Sensor();
        for (var j = 0; j < 4; j++) {
            Sensor();
            if (posit[c] > 2) {
                Sensor();
                document.images['pc' + c].src = pics[12].src;
            } else {
                Sensor();
                document.images['pc' + c].src = pics[3 * j + posit[c]].src;
            }
            Sensor();
            c++;
        }
    }
    Sensor();
}
display();
function solved() {
    Sensor();
    for (var i = 0; i < 3; i++) {
        Sensor();
        for (var j = 0; j < 4; j++) {
            Sensor();
            if (posit[i * 4 + j] != i) {
                var IARETREPLACE = false;
                Sensor();
                return IARETREPLACE;
            }
        }
    }
    var IARETREPLACE = true;
    Sensor();
    return IARETREPLACE;
}
function mix() {
    Sensor();
    var i, j, c;
    initbrd();
    Sensor();
    for (i = 0; i < 5; i++) {
        Sensor();
        k = Math.floor(Math.random() * (6 - i));
        Sensor();
        if (Math.random() < 0.5) {
            Sensor();
            c = posit[pairs[k + k]];
            posit[pairs[k + k]] = posit[pairs[i + i]];
            posit[pairs[i + i]] = c;
            c = posit[pairs[k + k + 1]];
            posit[pairs[k + k + 1]] = posit[pairs[i + i + 1]];
            posit[pairs[i + i + 1]] = c;
        } else {
            Sensor();
            c = posit[pairs[k + k]];
            posit[pairs[k + k]] = posit[pairs[i + i + 1]];
            posit[pairs[i + i + 1]] = c;
            c = posit[pairs[k + k + 1]];
            posit[pairs[k + k + 1]] = posit[pairs[i + i]];
            posit[pairs[i + i]] = c;
        }
    }
    Sensor();
    mode = 1;
    display();
    Sensor();
}
function reset() {
    Sensor();
    initbrd();
    mode = 0;
    display();
    Sensor();
}
function clearbrd() {
    Sensor();
    for (i = 0; i < 12; i++) {
        Sensor();
        posit[i] = 3;
    }
    Sensor();
}
function edit() {
    Sensor();
    var i;
    Sensor();
    if (mode != 2) {
        Sensor();
        mode = 2;
    }
    Sensor();
    clearbrd();
    edt = 0;
    display();
    Sensor();
}
function clicked(f, p) {
    Sensor();
    focus();
    var x = f * 4 + p;
    Sensor();
    if (mode == 2) {
        Sensor();
        if (posit[x] == 3) {
            Sensor();
            for (var i = 0; i < 12; i++) {
                Sensor();
                if (x == pairs[i]) {
                    Sensor();
                    break;
                }
            }
            Sensor();
            i = pairs[i ^ 1];
            Sensor();
            if (edt < 2) {
                Sensor();
                posit[x] = 0;
                posit[i] = 1;
            } else {
                Sensor();
                if (edt < 4) {
                    Sensor();
                    posit[x] = 0;
                    posit[i] = 2;
                } else {
                    Sensor();
                    posit[x] = 1;
                    posit[i] = 2;
                }
            }
            Sensor();
            edt++;
            Sensor();
            if (edt >= 5) {
                Sensor();
                var c = 0;
                Sensor();
                for (i = 0; i < 12; i += 2) {
                    Sensor();
                    if (posit[pairs[i]] > posit[pairs[i + 1]]) {
                        Sensor();
                        c++;
                    }
                }
                Sensor();
                for (i = 0; i < 12; i += 2) {
                    Sensor();
                    if (posit[pairs[i]] == 3) {
                        Sensor();
                        break;
                    }
                }
                Sensor();
                if (c & 1) {
                    Sensor();
                    posit[pairs[i]] = 2;
                    posit[pairs[i + 1]] = 1;
                } else {
                    Sensor();
                    posit[pairs[i]] = 1;
                    posit[pairs[i + 1]] = 2;
                }
                Sensor();
                mode = 1;
            }
            Sensor();
            display();
        }
    } else {
        Sensor();
        if (mode != 4) {
            Sensor();
            if (p == 0 || p == 2) {
                Sensor();
                domove(f + 1);
            } else {
                Sensor();
                domove(-f - 1);
            }
            Sensor();
            if (mode == 3) {
                Sensor();
                mode = 0;
            }
            Sensor();
            display();
        }
    }
    Sensor();
}
function domove(m) {
    Sensor();
    var c, i = 0;
    Sensor();
    if (m < 0) {
        Sensor();
        i = 3;
        m = -m;
    } else {
        Sensor();
        i = 1;
    }
    Sensor();
    while (i--) {
        Sensor();
        if (m == 1) {
            Sensor();
            c = posit[0];
            posit[0] = posit[1];
            posit[1] = posit[3];
            posit[3] = posit[2];
            posit[2] = c;
            c = posit[9];
            posit[9] = posit[4];
            posit[4] = posit[6];
            posit[6] = posit[11];
            posit[11] = c;
        } else {
            Sensor();
            if (m == 2) {
                Sensor();
                c = posit[4];
                posit[4] = posit[5];
                posit[5] = posit[7];
                posit[7] = posit[6];
                posit[6] = c;
                c = posit[1];
                posit[1] = posit[8];
                posit[8] = posit[10];
                posit[10] = posit[3];
                posit[3] = c;
            } else {
                Sensor();
                if (m == 3) {
                    Sensor();
                    c = posit[8];
                    posit[8] = posit[9];
                    posit[9] = posit[11];
                    posit[11] = posit[10];
                    posit[10] = c;
                    c = posit[5];
                    posit[5] = posit[0];
                    posit[0] = posit[2];
                    posit[2] = posit[7];
                    posit[7] = c;
                }
            }
        }
    }
    Sensor();
}
var soltimer;
function solplay() {
    Sensor();
    if (mode == 4) {
        Sensor();
        clearTimeout(soltimer);
        mode = 3;
        displaybut();
    } else {
        Sensor();
        if (mode != 2) {
            Sensor();
            solve();
            Sensor();
            if (mode == 3) {
                Sensor();
                mode = 4;
                soltimer = setTimeout('playstep()', 400);
                displaybut();
            }
        }
    }
    Sensor();
}
function playstep() {
    Sensor();
    if (mode >= 3) {
        Sensor();
        mode = 4;
        solve();
        Sensor();
        if (mode >= 3) {
            Sensor();
            soltimer = setTimeout('playstep()', 400);
        }
    } else {
        Sensor();
        displaybut();
    }
    Sensor();
}
function solve() {
    Sensor();
    if (mode == 0 || mode == 1) {
        Sensor();
        mode = 3;
        seq.length = 0;
        Sensor();
        if (perm.length == 0) {
            Sensor();
            alert('I will have to calculate through all positions.\n' + 'This will allow the shortest solution to be\n' + 'found in every case. This may take a little\n' + 'while, but only needs to be done once. \n');
            calcperm();
        }
        Sensor();
        var back = new Array();
        var i;
        Sensor();
        for (i = 0; i < 12; i++) {
            Sensor();
            back[i] = posit[i];
        }
        Sensor();
        var q = 0, c = 1;
        Sensor();
        for (i = 0; i < 5; i++) {
            Sensor();
            if (posit[pairs[i + i]] < posit[pairs[i + i + 1]]) {
                Sensor();
                q += c;
            }
            Sensor();
            c += c;
        }
        Sensor();
        var pos = new Array();
        Sensor();
        for (i = 0; i < 6; i++) {
            Sensor();
            pos[i] = posit[pairs[i + i]] + posit[pairs[i + i + 1]] - 1;
        }
        Sensor();
        a = 0;
        Sensor();
        for (i = 0; i < 5; i++) {
            Sensor();
            if (pos[i] == 1) {
                Sensor();
                break;
            }
            Sensor();
            if (pos[i] != 0) {
                Sensor();
                a++;
            }
        }
        Sensor();
        c = 0;
        Sensor();
        for (j = i + 1; j < 5; j++) {
            Sensor();
            if (pos[j] == 1) {
                Sensor();
                break;
            }
            Sensor();
            if (pos[j] != 0) {
                Sensor();
                c++;
            }
        }
        Sensor();
        q *= 6;
        Sensor();
        if (a == 0) {
            Sensor();
            q += c;
        } else {
            Sensor();
            if (a == 1) {
                Sensor();
                q += c + 3;
            } else {
                Sensor();
                if (a == 2) {
                    Sensor();
                    q += 5;
                }
            }
        }
        Sensor();
        for (i = 0; i < 5; i++) {
            Sensor();
            if (pos[i] == 0) {
                Sensor();
                break;
            }
        }
        Sensor();
        for (j = i + 1; j < 5; j++) {
            Sensor();
            if (pos[j] == 0) {
                Sensor();
                break;
            }
        }
        Sensor();
        q = q * 15 + j - i - 1;
        Sensor();
        if (i == 1) {
            Sensor();
            q += 5;
        } else {
            Sensor();
            if (i == 2) {
                Sensor();
                q += 9;
            } else {
                Sensor();
                if (i == 3) {
                    Sensor();
                    q += 12;
                } else {
                    Sensor();
                    if (i == 4) {
                        Sensor();
                        q += 14;
                    }
                }
            }
        }
        Sensor();
        do {
            Sensor();
            var n, l = perm[q];
            Sensor();
            for (var m = 0; m < 3; m++) {
                Sensor();
                n = 1;
                q = permmv[q][m];
                Sensor();
                if (perm[q] < l) {
                    Sensor();
                    break;
                }
                Sensor();
                n++;
                q = permmv[q][m];
                n++;
                q = permmv[q][m];
                Sensor();
                if (perm[q] < l) {
                    Sensor();
                    break;
                }
                Sensor();
                n = 0;
                q = permmv[q][m];
            }
            Sensor();
            while (n) {
                Sensor();
                push(-(m + 1));
                n--;
            }
        } while (m < 3);
        Sensor();
        for (i = 0; i < 12; i++) {
            Sensor();
            posit[i] = back[i];
        }
    }
    Sensor();
    if (mode >= 3) {
        Sensor();
        if (seq.length == 0) {
            Sensor();
            mode = 0;
        } else {
            Sensor();
            var c = pop();
            Sensor();
            if (seq.length == 0) {
                Sensor();
                mode = 0;
            }
            Sensor();
            domove(c);
        }
        Sensor();
        display();
    }
    Sensor();
}
function push() {
    Sensor();
    var i, l, m;
    Sensor();
    for (i = 0; i < push.arguments.length; i++) {
        Sensor();
        m = push.arguments[i];
        domove(m);
        Sensor();
        if (seq.length) {
            Sensor();
            l = seq[seq.length - 1];
            Sensor();
            if (l + m == 0) {
                Sensor();
                seq.length--;
            } else {
                Sensor();
                if (seq.length >= 2 && l == m && seq[seq.length - 2] == m) {
                    Sensor();
                    seq.length--;
                    seq[seq.length - 1] = -m;
                } else {
                    Sensor();
                    seq[seq.length] = m;
                }
            }
        } else {
            Sensor();
            seq[0] = m;
        }
    }
    Sensor();
}
function pop() {
    Sensor();
    var c = seq[0];
    Sensor();
    for (var i = 1; i < seq.length; i++) {
        Sensor();
        seq[i - 1] = seq[i];
    }
    Sensor();
    seq.length--;
    var IARETREPLACE = c;
    Sensor();
    return IARETREPLACE;
}
function help() {
    Sensor();
    alert('The Roundy puzzle\n\n' + 'The aim is to arrange the pieces so that each\n' + 'face is a single colour. Clicking on the left or\n' + 'right half of a face will rotate it to the left\n' + 'or right.\n' + '\nFurther controls:\n' + 'Mix:     This button randomly mixes the puzzle up.\n' + 'Reset: Resets the puzzle to the initial position.\n' + 'Edit:    Allows you to set up any position. The pieces\n' + '            are cleared, and then you can click where the\n' + '            yellow parts should be (the yellow/red pieces\n' + '            then the yellow/blue) and then a red piece. The\n' + '            last piece is filled in automatically\n' + 'Solve: Solves the puzzle. Each time you click this\n' + '            button, one move is performed, until it is solved.\n' + 'Play:   This solves the puzzle, playing through the whole\n' + '            solution automatically. Press it again to pause.\n' + 'Help:  Shows this help screen.\n');
    Sensor();
}
function getprmmv(p, m) {
    Sensor();
    var a, c, s, r, i, j;
    var pos = new Array(2, 2, 2, 2, 2, 2);
    var ori = new Array(0, 0, 0, 0, 0, 0);
    var q = p;
    r = Math.floor(q / 15);
    c = q - 15 * r;
    q = r;
    Sensor();
    if (c < 5) {
        Sensor();
        pos[0] = 0;
        pos[c + 1] = 0;
    } else {
        Sensor();
        if (c < 9) {
            Sensor();
            pos[1] = 0;
            pos[c - 3] = 0;
        } else {
            Sensor();
            if (c < 12) {
                Sensor();
                pos[2] = 0;
                pos[c - 6] = 0;
            } else {
                Sensor();
                if (c < 14) {
                    Sensor();
                    pos[3] = 0;
                    pos[c - 8] = 0;
                } else {
                    Sensor();
                    if (c < 15) {
                        Sensor();
                        pos[4] = 0;
                        pos[5] = 0;
                    }
                }
            }
        }
    }
    Sensor();
    r = Math.floor(q / 6);
    c = q - 6 * r;
    q = r;
    Sensor();
    if (c == 0) {
        Sensor();
        a = 0;
        c = 0;
    } else {
        Sensor();
        if (c == 1) {
            Sensor();
            a = 0;
            c = 1;
        } else {
            Sensor();
            if (c == 2) {
                Sensor();
                a = 0;
                c = 2;
            } else {
                Sensor();
                if (c == 3) {
                    Sensor();
                    a = 1;
                    c = 0;
                } else {
                    Sensor();
                    if (c == 4) {
                        Sensor();
                        a = 1;
                        c = 1;
                    } else {
                        Sensor();
                        if (c == 5) {
                            Sensor();
                            a = 2;
                            c = 0;
                        }
                    }
                }
            }
        }
    }
    Sensor();
    i = 0;
    Sensor();
    while (pos[i] == 0) {
        Sensor();
        i++;
    }
    Sensor();
    while (a) {
        Sensor();
        do {
            Sensor();
            i++;
        } while (pos[i] == 0);
        Sensor();
        a--;
    }
    Sensor();
    pos[i] = 1;
    Sensor();
    do {
        Sensor();
        do {
            Sensor();
            i++;
        } while (pos[i] == 0);
        Sensor();
        c--;
    } while (c >= 0);
    Sensor();
    pos[i] = 1;
    Sensor();
    if (q & 1) {
        Sensor();
        ori[0] = 1;
    }
    Sensor();
    if (q & 2) {
        Sensor();
        ori[1] = 1;
    }
    Sensor();
    if (q & 4) {
        Sensor();
        ori[2] = 1;
    }
    Sensor();
    if (q & 8) {
        Sensor();
        ori[3] = 1;
    }
    Sensor();
    if (q & 16) {
        Sensor();
        ori[4] = 1;
    }
    Sensor();
    ori[5] = ori[0] + ori[1] + ori[2] + ori[3] + ori[4] & 1;
    Sensor();
    if (m == 0) {
        Sensor();
        c = pos[2];
        pos[2] = pos[3];
        pos[3] = pos[1];
        pos[1] = pos[0];
        pos[0] = c;
        c = ori[2];
        ori[2] = ori[3];
        ori[3] = ori[1];
        ori[1] = ori[0];
        ori[0] = c;
        ori[3] ^= 1;
        ori[0] ^= 1;
    } else {
        Sensor();
        if (m == 1) {
            Sensor();
            c = pos[0];
            pos[0] = pos[1];
            pos[1] = pos[5];
            pos[5] = pos[4];
            pos[4] = c;
            c = ori[0];
            ori[0] = ori[1];
            ori[1] = ori[5];
            ori[5] = ori[4];
            ori[4] = c;
            ori[1] ^= 1;
            ori[4] ^= 1;
        } else {
            Sensor();
            if (m == 2) {
                Sensor();
                c = pos[4];
                pos[4] = pos[5];
                pos[5] = pos[3];
                pos[3] = pos[2];
                pos[2] = c;
                c = ori[4];
                ori[4] = ori[5];
                ori[5] = ori[3];
                ori[3] = ori[2];
                ori[2] = c;
                ori[5] ^= 1;
                ori[2] ^= 1;
            }
        }
    }
    Sensor();
    q = ori[0] + ori[1] * 2 + ori[2] * 4 + ori[3] * 8 + ori[4] * 16;
    a = 0;
    Sensor();
    for (i = 0; i < 5; i++) {
        Sensor();
        if (pos[i] == 1) {
            Sensor();
            break;
        }
        Sensor();
        if (pos[i] != 0) {
            Sensor();
            a++;
        }
    }
    Sensor();
    c = 0;
    Sensor();
    for (j = i + 1; j < 5; j++) {
        Sensor();
        if (pos[j] == 1) {
            Sensor();
            break;
        }
        Sensor();
        if (pos[j] != 0) {
            Sensor();
            c++;
        }
    }
    Sensor();
    q *= 6;
    Sensor();
    if (a == 0) {
        Sensor();
        q += c;
    } else {
        Sensor();
        if (a == 1) {
            Sensor();
            q += c + 3;
        } else {
            Sensor();
            if (a == 2) {
                Sensor();
                q += 5;
            }
        }
    }
    Sensor();
    for (i = 0; i < 5; i++) {
        Sensor();
        if (pos[i] == 0) {
            Sensor();
            break;
        }
    }
    Sensor();
    for (j = i + 1; j < 5; j++) {
        Sensor();
        if (pos[j] == 0) {
            Sensor();
            break;
        }
    }
    Sensor();
    q = q * 15 + j - i - 1;
    Sensor();
    if (i == 1) {
        Sensor();
        q += 5;
    } else {
        Sensor();
        if (i == 2) {
            Sensor();
            q += 9;
        } else {
            Sensor();
            if (i == 3) {
                Sensor();
                q += 12;
            } else {
                Sensor();
                if (i == 4) {
                    Sensor();
                    q += 14;
                }
            }
        }
    }
    var IARETREPLACE = q;
    Sensor();
    return IARETREPLACE;
}
perm = new Array();
permmv = new Array();
function calcperm() {
    Sensor();
    var p, q, m, n, l;
    Sensor();
    for (p = 0; p < 2880; p++) {
        Sensor();
        perm[p] = -1;
        permmv[p] = new Array();
        Sensor();
        for (m = 0; m < 3; m++) {
            Sensor();
            permmv[p][m] = getprmmv(p, m);
        }
    }
    Sensor();
    perm[12 * 15 * 6] = 0;
    Sensor();
    for (l = 0; l < 9; l++) {
        Sensor();
        n = 0;
        Sensor();
        for (p = 0; p < 2880; p++) {
            Sensor();
            if (perm[p] == l) {
                Sensor();
                for (m = 0; m < 3; m++) {
                    Sensor();
                    q = permmv[p][m];
                    Sensor();
                    if (perm[q] == -1) {
                        Sensor();
                        perm[q] = l + 1;
                        n++;
                    }
                    Sensor();
                    q = permmv[q][m];
                    q = permmv[q][m];
                    Sensor();
                    if (perm[q] == -1) {
                        Sensor();
                        perm[q] = l + 1;
                        n++;
                    }
                }
            }
        }
    }
    Sensor();
}