Sensor();
var netscape = document.layers ? 1 : 0;
var goodIE = document.all ? 1 : 0;
var mozilla = goodIE == 0 && document.getElementById ? 1 : 0;
Sensor();
if (netscape) {
    Sensor();
    window.captureEvents(Event.MOUSEDOWN);
    window.onMouseDown = mouseclick;
} else {
    Sensor();
    if (goodIE || mozilla) {
        Sensor();
        document.onmousedown = mouseclick;
    }
}
Sensor();
var imagedir = 'images/dinocube/';
function preload() {
    Sensor();
    this.length = preload.arguments.length;
    Sensor();
    for (var i = 0; i < this.length; i++) {
        Sensor();
        this[i] = new Image();
        this[i].src = imagedir + preload.arguments[i];
    }
    Sensor();
}
var pics = new preload('orangeu.gif', 'redu.gif', 'blueu.gif', 'greenu.gif', 'whiteu.gif', 'yellowu.gif', 'greyu.gif', 'oranger.gif', 'redr.gif', 'bluer.gif', 'greenr.gif', 'whiter.gif', 'yellowr.gif', 'greyr.gif', 'oranged.gif', 'redd.gif', 'blued.gif', 'greend.gif', 'whited.gif', 'yellowd.gif', 'greyd.gif', 'orangel.gif', 'redl.gif', 'bluel.gif', 'greenl.gif', 'whitel.gif', 'yellowl.gif', 'greyl.gif', '../buttons/edit.gif', '../buttons/edit2.gif', '../buttons/solve.gif', '../buttons/solve2.gif', '../buttons/play.gif', '../buttons/pause.gif', '../buttons/mode1.gif', '../buttons/mode2.gif');
var posit = new Array();
var mode = 0;
var type = 0;
var edt;
var pcperm = new Array();
var sol = new Array();
var soltimer;
function changeimage(lay, im, nw) {
    Sensor();
    if (netscape) {
        Sensor();
        document.layers[lay].document.images[im].src = nw;
    } else {
        Sensor();
        eval('document.images.' + im + '.src=nw');
    }
    Sensor();
}
function showhide(lay, vis) {
    Sensor();
    if (netscape) {
        Sensor();
        document.layers[lay].visibility = vis ? 'visible' : 'hidden';
    } else {
        Sensor();
        if (goodIE) {
            Sensor();
            eval('document.all.' + lay + '.style.visibility=\'' + (vis ? 'visible' : 'hidden') + '\'');
        } else {
            Sensor();
            document.getElementById(lay).style.visibility = vis ? 'visible' : 'hidden';
        }
    }
    Sensor();
}
var edges = new Array(0, 1, 2, 21, 19, 18, 12, 13, 14, 9, 7, 6, 5, 4, 3, 8, 10, 11, 17, 16, 15, 20, 22, 23);
var colours = new Array();
colours[0] = new Array(0, 2, 2, 2, 2, 1, 0, 4, 4, 4, 4, 1, 0, 3, 3, 3, 3, 1, 0, 5, 5, 5, 5, 1);
colours[1] = new Array(4, 4, 4, 2, 2, 2, 1, 1, 2, 1, 2, 2, 1, 1, 1, 3, 3, 3, 4, 4, 3, 4, 3, 3);
var shapgif = new Array(2, 0, 3, 1, 2, 0);
function display() {
    Sensor();
    var a = 0;
    Sensor();
    for (var i = 0; i < 4; i++) {
        Sensor();
        for (var j = 0; j < 6; j++) {
            Sensor();
            changeimage('l' + a, 'i' + a, pics[shapgif[j] * 7 + posit[a]].src);
            a++;
        }
    }
    Sensor();
    if (mode == 2) {
        Sensor();
        showhide('prev1', 1);
        changeimage('prev1', 'p1', pics[14 + colours[type][edges[edt * 2]]].src);
        showhide('prev2', 1);
        changeimage('prev2', 'p2', pics[colours[type][edges[edt * 2 + 1]]].src);
    } else {
        Sensor();
        showhide('prev1', 0);
        showhide('prev2', 0);
    }
    Sensor();
    if (mode == 2) {
        Sensor();
        changeimage('buttons', 'edit', pics[29].src);
    } else {
        Sensor();
        changeimage('buttons', 'edit', pics[28].src);
    }
    Sensor();
    if (mode >= 3) {
        Sensor();
        changeimage('buttons', 'solve', pics[31].src);
    } else {
        Sensor();
        changeimage('buttons', 'solve', pics[30].src);
    }
    Sensor();
    if (mode == 4) {
        Sensor();
        changeimage('buttons', 'play', pics[33].src);
    } else {
        Sensor();
        changeimage('buttons', 'play', pics[32].src);
    }
    Sensor();
    changeimage('buttons', 'type', pics[34 + type].src);
    Sensor();
    if (mode == 1 && solved()) {
        Sensor();
        alert('You solved it!\nYou don\'t get a prize for this though!');
        mode = 0;
    }
    Sensor();
}
function changegame() {
    Sensor();
    type = 1 - type;
    reset();
    display();
    Sensor();
}
function initbrd() {
    Sensor();
    if (mode == 4) {
        Sensor();
        clearTimeout(soltimer);
    }
    Sensor();
    for (i = 0; i < 24; i++) {
        Sensor();
        posit[i] = colours[type][i];
    }
    Sensor();
    mode = 0;
    sol.length = 0;
    Sensor();
}
initbrd();
function solved() {
    Sensor();
    var i;
    Sensor();
    if (type == 0) {
        Sensor();
        for (i = 0; i < 24; i += 6) {
            Sensor();
            if (posit[i + 1] != posit[i + 2]) {
                var IARETREPLACE = false;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (posit[i + 1] != posit[i + 3]) {
                var IARETREPLACE = false;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (posit[i + 1] != posit[i + 4]) {
                var IARETREPLACE = false;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (i < 18 && (posit[i] != posit[i + 6] || posit[i + 5] != posit[i + 11])) {
                var IARETREPLACE = false;
                Sensor();
                return IARETREPLACE;
            }
        }
        var IARETREPLACE = true;
        Sensor();
        return IARETREPLACE;
    } else {
        Sensor();
        if (posit[1] == posit[2]) {
            Sensor();
            if (posit[1] == posit[19] && posit[7] == posit[14] && posit[13] == posit[14] && posit[4] == posit[3] && posit[3] == posit[10]) {
                var IARETREPLACE = true;
                Sensor();
                return IARETREPLACE;
            }
        } else {
            Sensor();
            if (posit[1] == posit[3]) {
                Sensor();
                if (posit[7] == posit[1] && posit[13] == posit[20] && posit[19] == posit[20] && posit[10] == posit[9] && posit[9] == posit[16]) {
                    var IARETREPLACE = true;
                    Sensor();
                    return IARETREPLACE;
                }
            }
        }
        var IARETREPLACE = false;
        Sensor();
        return IARETREPLACE;
    }
    Sensor();
}
function edit() {
    Sensor();
    initbrd();
    Sensor();
    for (var i = 0; i < 24; i++) {
        Sensor();
        posit[i] = 6;
    }
    Sensor();
    mode = 2;
    edt = 0;
    display();
    Sensor();
}
function getmove(e) {
    Sensor();
    var clickX, clickY;
    Sensor();
    if (netscape || mozilla) {
        Sensor();
        clickX = e.pageX;
        clickY = e.pageY;
    } else {
        Sensor();
        clickX = window.event.clientX - 2;
        clickY = window.event.clientY - 2;
    }
    Sensor();
    if (clickY > 107) {
        var IARETREPLACE = -1;
        Sensor();
        return IARETREPLACE;
    }
    Sensor();
    clickX -= 5;
    clickX = Math.floor(clickX / 23.5);
    clickX++;
    clickX &= 7;
    Sensor();
    if (clickY > 53) {
        Sensor();
        clickX += 8;
    }
    var IARETREPLACE = clickX;
    Sensor();
    return IARETREPLACE;
}
function getfacelet(e) {
    Sensor();
    var clickX, clickY;
    Sensor();
    if (netscape || mozilla) {
        Sensor();
        clickX = e.pageX;
        clickY = e.pageY;
    } else {
        Sensor();
        clickX = window.event.clientX - 2;
        clickY = window.event.clientY - 2;
    }
    Sensor();
    clickX -= 6;
    clickY -= 5;
    Sensor();
    if (clickY < 0 || clickX < 0) {
        var IARETREPLACE = -1;
        Sensor();
        return IARETREPLACE;
    }
    Sensor();
    var f = Math.floor(clickX / 47);
    Sensor();
    if (f > 5) {
        var IARETREPLACE = -1;
        Sensor();
        return IARETREPLACE;
    }
    Sensor();
    clickX -= 47 * f;
    f *= 6;
    Sensor();
    if (clickY < 26) {
        var IARETREPLACE = clickX + clickY > 23 && clickX - clickY < 23 ? f : -1;
        Sensor();
        return IARETREPLACE;
    }
    Sensor();
    clickY -= 26;
    Sensor();
    if (clickY >= 47) {
        Sensor();
        clickY -= 47;
        var IARETREPLACE = clickX + clickY < 46 && clickX > clickY + 2 ? f + 5 : -1;
        Sensor();
        return IARETREPLACE;
    }
    Sensor();
    if (clickX <= clickY) {
        var IARETREPLACE = clickX + clickY < 46 ? f + 2 : f + 4;
        Sensor();
        return IARETREPLACE;
    }
    var IARETREPLACE = clickX + clickY < 46 ? f + 1 : f + 3;
    Sensor();
    return IARETREPLACE;
}
function mouseclick(e) {
    Sensor();
    var f;
    Sensor();
    if (mode == 2) {
        Sensor();
        f = getfacelet(e);
        Sensor();
        if (f >= 0) {
            Sensor();
            clicked(f);
        }
    } else {
        Sensor();
        if (mode < 2) {
            Sensor();
            f = getmove(e);
            Sensor();
            if (f >= 0) {
                Sensor();
                domove(f);
                display();
            }
        }
    }
    Sensor();
}
function reset() {
    Sensor();
    initbrd();
    display();
    Sensor();
}
function mix() {
    Sensor();
    var tmp = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
    var i, j, c;
    Sensor();
    for (i = 0; i < 10; i++) {
        Sensor();
        j = i + Math.floor(Math.random() * (12 - i));
        Sensor();
        if (j > i) {
            Sensor();
            c = tmp[j];
            tmp[j] = tmp[i];
            tmp[i] = c;
            c = tmp[11];
            tmp[11] = tmp[10];
            tmp[10] = c;
        }
    }
    Sensor();
    for (i = 0; i < 12; i++) {
        Sensor();
        posit[edges[i + i]] = colours[type][edges[tmp[i] * 2]];
        posit[edges[i + i + 1]] = colours[type][edges[tmp[i] * 2 + 1]];
    }
    Sensor();
    mode = 1;
    display();
    Sensor();
}
function placePiece(ps, pc) {
    Sensor();
    posit[edges[ps * 2]] = colours[type][edges[pc * 2]];
    posit[edges[ps * 2 + 1]] = colours[type][edges[pc * 2 + 1]];
    Sensor();
}
function getPieceAt(ps) {
    Sensor();
    var c1 = posit[edges[ps * 2]];
    var c2 = posit[edges[ps * 2 + 1]];
    Sensor();
    if (c1 == 6 && c2 == 6) {
        var IARETREPLACE = -1;
        Sensor();
        return IARETREPLACE;
    }
    Sensor();
    for (var pc = 0; pc < 12; pc++) {
        Sensor();
        if (colours[type][edges[pc * 2]] == c1 && colours[type][edges[pc * 2 + 1]] == c2) {
            Sensor();
            break;
        }
    }
    var IARETREPLACE = pc;
    Sensor();
    return IARETREPLACE;
}
function clicked(f) {
    Sensor();
    var i, j, k;
    Sensor();
    if (posit[f] == 6) {
        Sensor();
        for (i = 0; i < 12 && edges[i + i] != f && edges[i + i + 1] != f; i++) {
            ;
            Sensor();
        }
        Sensor();
        placePiece(i, edt);
        Sensor();
        edt++;
        Sensor();
        if (type != 0 && edt == 9) {
            Sensor();
            for (i = 0; i < 12; i++) {
                Sensor();
                if (getPieceAt(i) < 0) {
                    Sensor();
                    placePiece(i, edt++);
                }
            }
            Sensor();
            mode = 1;
        } else {
            Sensor();
            if (type == 0 && edt == 10) {
                Sensor();
                for (i = 0; i < 12; i++) {
                    Sensor();
                    pcperm[i] = getPieceAt(i);
                    Sensor();
                    if (pcperm[i] < 0) {
                        Sensor();
                        pcperm[i] = edt++;
                    }
                }
                Sensor();
                k = 0;
                Sensor();
                for (i = 0; i < 12; i++) {
                    Sensor();
                    for (j = i + 1; j < 12; j++) {
                        Sensor();
                        if (pcperm[i] > pcperm[j]) {
                            Sensor();
                            k++;
                        }
                    }
                }
                Sensor();
                k &= 1;
                Sensor();
                for (i = 0; i < 12; i++) {
                    Sensor();
                    if (pcperm[i] >= 10) {
                        Sensor();
                        placePiece(i, pcperm[i] ^ k);
                    }
                }
                Sensor();
                mode = 1;
            }
        }
        Sensor();
        display();
    }
    Sensor();
}
var movelist = new Array();
movelist[0] = new Array(0, 2, 19, 1, 21, 18);
movelist[1] = new Array(6, 8, 1, 7, 3, 0);
movelist[2] = new Array(12, 14, 7, 13, 9, 6);
movelist[3] = new Array(18, 20, 13, 19, 15, 12);
movelist[4] = new Array(23, 21, 4, 22, 2, 5);
movelist[5] = new Array(5, 3, 10, 4, 8, 11);
movelist[6] = new Array(11, 9, 16, 10, 14, 17);
movelist[7] = new Array(17, 15, 22, 16, 20, 23);
function domove(m) {
    Sensor();
    var c, i, k = Math.floor(m / 2);
    var l = 1 + (m & 1);
    Sensor();
    while (l > 0) {
        Sensor();
        for (i = 0; i < movelist[k].length; i += 3) {
            Sensor();
            c = posit[movelist[k][i]];
            posit[movelist[k][i]] = posit[movelist[k][i + 2]];
            posit[movelist[k][i + 2]] = posit[movelist[k][i + 1]];
            posit[movelist[k][i + 1]] = c;
        }
        Sensor();
        l--;
    }
    Sensor();
}
function solve(m) {
    Sensor();
    var a;
    Sensor();
    if (mode < 2) {
        Sensor();
        mode = m ? 4 : 3;
        dosolve();
    } else {
        Sensor();
        if (mode > 2) {
            Sensor();
            if (m) {
                Sensor();
                if (mode == 4) {
                    Sensor();
                    mode = 3;
                    clearTimeout(soltimer);
                    display();
                    var IARETREPLACE = void 0;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
                mode = 4;
            }
            Sensor();
            if (sol.length) {
                Sensor();
                domove(sol[0]);
                Sensor();
                for (a = 1; a < sol.length; a++) {
                    Sensor();
                    sol[a - 1] = sol[a];
                }
                Sensor();
                sol.length--;
            }
            Sensor();
            if (sol.length == 0) {
                Sensor();
                mode = 0;
            }
            Sensor();
            display();
            Sensor();
            if (mode == 4) {
                Sensor();
                soltimer = setTimeout('solve(0);', 200);
            }
        }
    }
    Sensor();
}
function find(prm, pc) {
    Sensor();
    var i;
    Sensor();
    for (i = 0; i < 12 && prm[i] != pc; i++) {
        ;
        Sensor();
    }
    var IARETREPLACE = i;
    Sensor();
    return IARETREPLACE;
}
function find2(prm, pc) {
    Sensor();
    var i;
    Sensor();
    for (i = 2; i < find2.arguments.length; i++) {
        Sensor();
        if (prm[find2.arguments[i]] == pc) {
            var IARETREPLACE = find2.arguments[i];
            Sensor();
            return IARETREPLACE;
        }
    }
    var IARETREPLACE = find(prm, pc);
    Sensor();
    return IARETREPLACE;
}
var moveprm = new Array();
moveprm[0] = new Array(0, 2, 1);
moveprm[1] = new Array(0, 1, 2);
moveprm[2] = new Array(5, 0, 7);
moveprm[3] = new Array(5, 7, 0);
moveprm[4] = new Array(3, 5, 4);
moveprm[5] = new Array(3, 4, 5);
moveprm[6] = new Array(2, 3, 10);
moveprm[7] = new Array(2, 10, 3);
moveprm[8] = new Array(11, 6, 1);
moveprm[9] = new Array(11, 1, 6);
moveprm[10] = new Array(6, 8, 7);
moveprm[11] = new Array(6, 7, 8);
moveprm[12] = new Array(8, 9, 4);
moveprm[13] = new Array(8, 4, 9);
moveprm[14] = new Array(9, 11, 10);
moveprm[15] = new Array(9, 10, 11);
function push() {
    Sensor();
    var i, l, m;
    Sensor();
    for (i = 0; i < push.arguments.length; i++) {
        Sensor();
        m = push.arguments[i];
        l = pcperm[moveprm[m][0]];
        pcperm[moveprm[m][0]] = pcperm[moveprm[m][1]];
        pcperm[moveprm[m][1]] = pcperm[moveprm[m][2]];
        pcperm[moveprm[m][2]] = l;
        l = sol.length;
        sol[l] = m;
        Sensor();
        if (l > 0) {
            Sensor();
            if (sol[l - 1] == (m ^ 1)) {
                Sensor();
                sol.length -= 2;
            }
            Sensor();
            if (sol[l - 1] == m) {
                Sensor();
                sol[l - 1] ^= 1;
                sol.length--;
            }
        }
    }
    Sensor();
}
function dosolve() {
    Sensor();
    var i, j, c;
    sol.length = 0;
    Sensor();
    for (i = 0; i < 12; i++) {
        Sensor();
        pcperm[i] = getPieceAt(i);
    }
    Sensor();
    if (type) {
        Sensor();
        i = find2(pcperm, 0, 0, 1, 2, 5, 7, 3, 4, 6, 8, 10, 11, 9);
        Sensor();
        while (i != 0) {
            Sensor();
            switch (i) {
            case 1:
                Sensor();
                push(1);
                Sensor();
                break;
            case 2:
                Sensor();
                push(0);
                Sensor();
                break;
            case 5:
                Sensor();
                push(3);
                Sensor();
                break;
            case 7:
                Sensor();
                push(2);
                Sensor();
                break;
            case 3:
                Sensor();
                push(5);
                Sensor();
                break;
            case 4:
                Sensor();
                push(4);
                Sensor();
                break;
            case 6:
                Sensor();
                push(10);
                Sensor();
                break;
            case 8:
                Sensor();
                push(11);
                Sensor();
                break;
            case 10:
                Sensor();
                push(7);
                Sensor();
                break;
            case 11:
                Sensor();
                push(8);
                Sensor();
                break;
            case 9:
                Sensor();
                push(12);
                Sensor();
                break;
            }
            Sensor();
            i = find2(pcperm, 0, 0, 1, 2, 5, 7, 3, 4, 6, 8, 10, 11, 9);
        }
        Sensor();
        i = find2(pcperm, 0, 1, 2, 11, 6, 3, 7, 8, 9, 10, 5, 4);
        Sensor();
        while (i != 1) {
            Sensor();
            switch (i) {
            case 2:
                Sensor();
                push(0);
                Sensor();
                break;
            case 11:
                Sensor();
                push(8);
                Sensor();
                break;
            case 6:
                Sensor();
                push(9);
                Sensor();
                break;
            case 3:
                Sensor();
                push(6);
                Sensor();
                break;
            case 7:
                Sensor();
                push(11);
                Sensor();
                break;
            case 8:
                Sensor();
                push(10);
                Sensor();
                break;
            case 9:
                Sensor();
                push(15);
                Sensor();
                break;
            case 10:
                Sensor();
                push(14);
                Sensor();
                break;
            case 5:
                Sensor();
                push(0, 3);
                Sensor();
                break;
            case 4:
                Sensor();
                push(12);
                Sensor();
                break;
            }
            Sensor();
            i = find2(pcperm, 0, 1, 2, 11, 6, 3, 7, 8, 9, 10, 5, 4);
        }
        Sensor();
        i = find2(pcperm, 0, 2, 3, 10, 4, 5, 9, 11, 6, 7, 8);
        Sensor();
        while (i != 2) {
            Sensor();
            switch (i) {
            case 3:
                Sensor();
                push(6);
                Sensor();
                break;
            case 10:
                Sensor();
                push(7);
                Sensor();
                break;
            case 4:
                Sensor();
                push(5);
                Sensor();
                break;
            case 5:
                Sensor();
                push(4);
                Sensor();
                break;
            case 9:
                Sensor();
                push(14);
                Sensor();
                break;
            case 11:
                Sensor();
                push(15);
                Sensor();
                break;
            case 6:
                Sensor();
                push(1, 9);
                Sensor();
                break;
            case 7:
                Sensor();
                push(2);
                Sensor();
                break;
            case 8:
                Sensor();
                push(10);
                Sensor();
                break;
            }
            Sensor();
            i = find2(pcperm, 0, 2, 3, 10, 4, 5, 9, 11, 6, 7, 8);
        }
        Sensor();
        i = find2(pcperm, 3, 3, 4, 5, 8, 9, 6, 7, 10, 11);
        Sensor();
        while (i != 3) {
            Sensor();
            switch (i) {
            case 4:
                Sensor();
                push(5);
                Sensor();
                break;
            case 5:
                Sensor();
                push(4);
                Sensor();
                break;
            case 8:
                Sensor();
                push(12);
                Sensor();
                break;
            case 9:
                Sensor();
                push(13);
                Sensor();
                break;
            case 10:
                Sensor();
                push(15);
                Sensor();
                break;
            case 11:
                Sensor();
                push(14);
                Sensor();
                break;
            case 6:
                Sensor();
                push(11);
                Sensor();
                break;
            case 7:
                Sensor();
                push(10);
                Sensor();
                break;
            }
            Sensor();
            i = find2(pcperm, 3, 3, 4, 5, 8, 9, 6, 7, 10, 11);
        }
        Sensor();
        i = find2(pcperm, 3, 5, 4, 8, 9, 6, 7, 10, 11);
        Sensor();
        while (i != 5) {
            Sensor();
            switch (i) {
            case 4:
                Sensor();
                push(5);
                Sensor();
                break;
            case 8:
                Sensor();
                push(12);
                Sensor();
                break;
            case 9:
                Sensor();
                push(13);
                Sensor();
                break;
            case 10:
                Sensor();
                push(15);
                Sensor();
                break;
            case 11:
                Sensor();
                push(14);
                Sensor();
                break;
            case 6:
                Sensor();
                push(11);
                Sensor();
                break;
            case 7:
                Sensor();
                push(10);
                Sensor();
                break;
            }
            Sensor();
            i = find2(pcperm, 3, 5, 4, 8, 9, 6, 7, 10, 11);
        }
        Sensor();
        i = find2(pcperm, 6, 6, 7, 8, 4, 9, 10, 11);
        Sensor();
        while (i != 6) {
            Sensor();
            switch (i) {
            case 7:
                Sensor();
                push(11);
                Sensor();
                break;
            case 8:
                Sensor();
                push(10);
                Sensor();
                break;
            case 4:
                Sensor();
                push(13);
                Sensor();
                break;
            case 9:
                Sensor();
                push(12);
                Sensor();
                break;
            case 10:
                Sensor();
                push(15);
                Sensor();
                break;
            case 11:
                Sensor();
                push(14);
                Sensor();
                break;
            }
            Sensor();
            i = find2(pcperm, 6, 6, 7, 8, 4, 9, 10, 11);
        }
        Sensor();
        i = find2(pcperm, 6, 7, 8, 4, 9, 10, 11);
        Sensor();
        while (i != 7) {
            Sensor();
            switch (i) {
            case 8:
                Sensor();
                push(10);
                Sensor();
                break;
            case 4:
                Sensor();
                push(13);
                Sensor();
                break;
            case 9:
                Sensor();
                push(12);
                Sensor();
                break;
            case 10:
                Sensor();
                push(15);
                Sensor();
                break;
            case 11:
                Sensor();
                push(14);
                Sensor();
                break;
            }
            Sensor();
            i = find2(pcperm, 6, 7, 8, 4, 9, 10, 11);
        }
        Sensor();
        i = find2(pcperm, 6, 4, 8, 9, 10, 11);
        Sensor();
        while (i != 4) {
            Sensor();
            switch (i) {
            case 8:
                Sensor();
                push(12);
                Sensor();
                break;
            case 9:
                Sensor();
                push(13);
                Sensor();
                break;
            case 10:
                Sensor();
                push(15);
                Sensor();
                break;
            case 11:
                Sensor();
                push(14);
                Sensor();
                break;
            }
            Sensor();
            i = find2(pcperm, 6, 4, 8, 9, 10, 11);
        }
        Sensor();
        i = find2(pcperm, 3, 8, 9, 10, 11);
        Sensor();
        if (i == 8) {
            Sensor();
            push(12, 14, 13, 15, 12);
            i = 4;
        }
        Sensor();
        while (i != 4) {
            Sensor();
            switch (i) {
            case 9:
                Sensor();
                push(13);
                Sensor();
                break;
            case 10:
                Sensor();
                push(15);
                Sensor();
                break;
            case 11:
                Sensor();
                push(14);
                Sensor();
                break;
            }
            Sensor();
            i = find2(pcperm, 3, 4, 9, 10, 11);
        }
    } else {
        Sensor();
        i = find(pcperm, 0);
        Sensor();
        while (i != 0) {
            Sensor();
            switch (i) {
            case 1:
                Sensor();
                push(1);
                Sensor();
                break;
            case 2:
                Sensor();
                push(0);
                Sensor();
                break;
            case 3:
                Sensor();
                push(5);
                Sensor();
                break;
            case 4:
                Sensor();
                push(4);
                Sensor();
                break;
            case 5:
                Sensor();
                push(3);
                Sensor();
                break;
            case 6:
                Sensor();
                push(10);
                Sensor();
                break;
            case 7:
                Sensor();
                push(2);
                Sensor();
                break;
            case 8:
                Sensor();
                push(11);
                Sensor();
                break;
            case 9:
                Sensor();
                push(12);
                Sensor();
                break;
            case 10:
                Sensor();
                push(7);
                Sensor();
                break;
            case 11:
                Sensor();
                push(8);
                Sensor();
                break;
            }
            Sensor();
            i = find(pcperm, 0);
        }
        Sensor();
        i = find(pcperm, 1);
        Sensor();
        while (i != 1) {
            Sensor();
            switch (i) {
            case 11:
                Sensor();
                push(8);
                Sensor();
                break;
            case 6:
                Sensor();
                push(9);
                Sensor();
                break;
            case 7:
                Sensor();
                push(11);
                Sensor();
                break;
            case 8:
                Sensor();
                push(10);
                Sensor();
                break;
            case 9:
                Sensor();
                push(15);
                Sensor();
                break;
            case 10:
                Sensor();
                push(14);
                Sensor();
                break;
            case 2:
                Sensor();
                push(6);
                Sensor();
                break;
            case 3:
                Sensor();
                push(5);
                Sensor();
                break;
            case 4:
                Sensor();
                push(12);
                Sensor();
                break;
            case 5:
                Sensor();
                push(1, 3, 0);
                Sensor();
                break;
            }
            Sensor();
            i = find(pcperm, 1);
        }
        Sensor();
        i = find(pcperm, 2);
        Sensor();
        while (i != 2) {
            Sensor();
            switch (i) {
            case 3:
                Sensor();
                push(6);
                Sensor();
                break;
            case 10:
                Sensor();
                push(7);
                Sensor();
                break;
            case 9:
                Sensor();
                push(14);
                Sensor();
                break;
            case 11:
                Sensor();
                push(15);
                Sensor();
                break;
            case 4:
                Sensor();
                push(5);
                Sensor();
                break;
            case 5:
                Sensor();
                push(4);
                Sensor();
                break;
            case 6:
                Sensor();
                push(1, 9, 0);
                Sensor();
                break;
            case 7:
                Sensor();
                push(0, 2, 1);
                Sensor();
                break;
            case 8:
                Sensor();
                push(10);
                Sensor();
                break;
            }
            Sensor();
            i = find(pcperm, 2);
        }
        Sensor();
        i = find(pcperm, 5);
        Sensor();
        while (i != 3) {
            Sensor();
            switch (i) {
            case 4:
                Sensor();
                push(5);
                Sensor();
                break;
            case 5:
                Sensor();
                push(4);
                Sensor();
                break;
            case 8:
                Sensor();
                push(12);
                Sensor();
                break;
            case 9:
                Sensor();
                push(13);
                Sensor();
                break;
            case 10:
                Sensor();
                push(15);
                Sensor();
                break;
            case 11:
                Sensor();
                push(14);
                Sensor();
                break;
            case 6:
                Sensor();
                push(11);
                Sensor();
                break;
            case 7:
                Sensor();
                push(10);
                Sensor();
                break;
            }
            Sensor();
            i = find(pcperm, 5);
        }
        Sensor();
        i = find(pcperm, 3);
        Sensor();
        if (i == 5) {
            Sensor();
            push(3, 4, 2, 4);
            i = 3;
        }
        Sensor();
        while (i != 3) {
            Sensor();
            switch (i) {
            case 4:
                Sensor();
                push(5);
                Sensor();
                break;
            case 8:
                Sensor();
                push(12);
                Sensor();
                break;
            case 9:
                Sensor();
                push(13);
                Sensor();
                break;
            case 6:
                Sensor();
                push(11);
                Sensor();
                break;
            case 7:
                Sensor();
                push(10);
                Sensor();
                break;
            case 10:
                Sensor();
                push(15);
                Sensor();
                break;
            case 11:
                Sensor();
                push(14);
                Sensor();
                break;
            }
            Sensor();
            i = find(pcperm, 3);
        }
        Sensor();
        i = find(pcperm, 6);
        Sensor();
        while (i != 7) {
            Sensor();
            switch (i) {
            case 6:
                Sensor();
                push(10);
                Sensor();
                break;
            case 8:
                Sensor();
                push(11);
                Sensor();
                break;
            case 4:
                Sensor();
                push(13);
                Sensor();
                break;
            case 9:
                Sensor();
                push(12);
                Sensor();
                break;
            case 10:
                Sensor();
                push(15);
                Sensor();
                break;
            case 11:
                Sensor();
                push(14);
                Sensor();
                break;
            }
            Sensor();
            i = find(pcperm, 6);
        }
        Sensor();
        i = find(pcperm, 7);
        Sensor();
        if (i == 6) {
            Sensor();
            push(10, 12, 11, 13, 10);
            i = 7;
        }
        Sensor();
        while (i != 7) {
            Sensor();
            switch (i) {
            case 8:
                Sensor();
                push(11);
                Sensor();
                break;
            case 4:
                Sensor();
                push(13);
                Sensor();
                break;
            case 9:
                Sensor();
                push(12);
                Sensor();
                break;
            case 10:
                Sensor();
                push(15);
                Sensor();
                break;
            case 11:
                Sensor();
                push(14);
                Sensor();
                break;
            }
            Sensor();
            i = find(pcperm, 7);
        }
        Sensor();
        i = find(pcperm, 8);
        Sensor();
        while (i != 4) {
            Sensor();
            switch (i) {
            case 8:
                Sensor();
                push(12);
                Sensor();
                break;
            case 9:
                Sensor();
                push(13);
                Sensor();
                break;
            case 10:
                Sensor();
                push(15);
                Sensor();
                break;
            case 11:
                Sensor();
                push(14);
                Sensor();
                break;
            }
            Sensor();
            i = find(pcperm, 8);
        }
        Sensor();
        i = find(pcperm, 4);
        Sensor();
        if (i == 8) {
            Sensor();
            push(12, 14, 13, 15, 12);
            i = 4;
        }
        Sensor();
        while (i != 4) {
            Sensor();
            switch (i) {
            case 9:
                Sensor();
                push(13);
                Sensor();
                break;
            case 10:
                Sensor();
                push(15);
                Sensor();
                break;
            case 11:
                Sensor();
                push(14);
                Sensor();
                break;
            }
            Sensor();
            i = find(pcperm, 4);
        }
        Sensor();
        i = find(pcperm, 9);
        Sensor();
        switch (i) {
        case 10:
            Sensor();
            push(15);
            Sensor();
            break;
        case 11:
            Sensor();
            push(14);
            Sensor();
            break;
        }
    }
    Sensor();
    solve(0);
    Sensor();
}
function help() {
    Sensor();
    alert('Dinocube\n\n' + 'This is the Dinocube puzzle. Click to the\n' + 'left or right of a corner to turn it in\n' + 'either direction. The aim is of course to\n' + 'rearrange it so that each face has only one\n' + 'colour.\n\n' + 'Buttons:\n' + 'Mix:   Mixes up the cube.\n' + 'Reset: Resets the cube to solved position.\n' + 'Solve: Calculates the shortest solution. Each\n' + '      time you click this button, one more move\n' + '      of the solution is performed.\n' + 'Play:  Calculates the shortest solution, and\n' + '      plays it back automatically.\n' + 'Edit:  Edit the puzzle. The pieces are cleared\n' + '      and then you can click where each of them\n' + '      (shown in miniature) is supposed to be.\n' + 'Mode:  Switch between the 6 colour and the 4\n' + '      colour version of the dino cube.\n' + 'Help:  Shows this help screen.');
    Sensor();
}
function dostyle() {
    Sensor();
    var s = '<STYLE TYPE="text/css">';
    Sensor();
    for (var i = 0; i < 4; i++) {
        Sensor();
        s += '#l' + i * 6 + '{position:absolute; left:' + (i * 47 + 5) + '; top:5; width:1;}';
        s += '#l' + (i * 6 + 1) + '{position:absolute; left:' + (i * 47 + 5) + '; top:29; width:1;}';
        s += '#l' + (i * 6 + 2) + '{position:absolute; left:' + (i * 47 + 5) + '; top:29; width:1;}';
        s += '#l' + (i * 6 + 3) + '{position:absolute; left:' + (i * 47 + 28) + '; top:29; width:1;}';
        s += '#l' + (i * 6 + 4) + '{position:absolute; left:' + (i * 47 + 5) + '; top:52; width:1;}';
        s += '#l' + (i * 6 + 5) + '{position:absolute; left:' + (i * 47 + 5) + '; top:76; width:1;}';
    }
    Sensor();
    s += '#prev1{position:absolute; left:91; top:5; width:1; visibility:hidden;}';
    s += '#prev2{position:absolute; left:91; top:14; width:1; visibility:hidden;}';
    s += '#buttons{position:absolute; left:004; top:110; width:192;}';
    s += '</STYLE>';
    var IARETREPLACE = s;
    Sensor();
    return IARETREPLACE;
}
document.write(dostyle());