Sensor();
var netscape = document.layers ? 1 : 0;
var goodIE = document.all ? 1 : 0;
var mozilla = goodIE == 0 && document.getElementById ? 1 : 0;
Sensor();
if (netscape) {
    Sensor();
    window.captureEvents(Event.MOUSEDOWN);
    window.onMouseDown = mouseclick;
} else {
    Sensor();
    if (goodIE || mozilla) {
        Sensor();
        document.onmousedown = mouseclick;
    }
}
Sensor();
var imagedir = 'images/pyraminx/';
function preload() {
    Sensor();
    this.length = preload.arguments.length;
    Sensor();
    for (var i = 0; i < this.length; i++) {
        Sensor();
        this[i] = new Image();
        this[i].src = imagedir + preload.arguments[i];
    }
    Sensor();
}
var pics = new preload('pyrf1b.gif', 'pyrf1g.gif', 'pyrf1r.gif', 'pyrf1y.gif', 'pyrf1x.gif', 'pyrf2b.gif', 'pyrf2g.gif', 'pyrf2r.gif', 'pyrf2y.gif', 'pyrf2x.gif', 'pyrr1b.gif', 'pyrr1g.gif', 'pyrr1r.gif', 'pyrr1y.gif', 'pyrr1x.gif', 'pyrr2b.gif', 'pyrr2g.gif', 'pyrr2r.gif', 'pyrr2y.gif', 'pyrr2x.gif', 'pyrl1b.gif', 'pyrl1g.gif', 'pyrl1r.gif', 'pyrl1y.gif', 'pyrl1x.gif', 'pyrl2b.gif', 'pyrl2g.gif', 'pyrl2r.gif', 'pyrl2y.gif', 'pyrl2x.gif', '../buttons/edit.gif', '../buttons/edit2.gif', '../buttons/solve.gif', '../buttons/solve2.gif', '../buttons/play.gif', '../buttons/pause.gif', 'pyre1.gif', 'pyre2.gif', 'pyre3.gif');
var fletx = new Array(66, 34, 98, 2, 66, 130, 66, 34, 98, 98, 98, 130, 98, 130, 162, 98, 98, 130, 66, 66, 34, 66, 34, 2, 66, 66, 34, 66, 34, 98, 2, 66, 130, 66, 34, 98);
var flety = new Array(122, 138, 138, 154, 154, 154, 138, 154, 154, 82, 42, 98, 2, 58, 114, 82, 42, 98, 82, 42, 98, 2, 58, 114, 82, 42, 98, 206, 190, 190, 174, 174, 174, 190, 174, 174);
var shapgif = new Array(0, 0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 4, 4, 4, 4, 4, 4, 5, 5, 5, 1, 1, 1, 1, 1, 1, 0, 0, 0);
var moves = new Array(-1, -1, -1, 5, -1, 14, -1, 1, 10, 4, -1, -1, 15, -1, 6, 0, 11, 2, 12, -1, -1, 7, -1, 13, 8, 3, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1);
var posit = new Array();
var mode = 0;
var edt;
var perm = new Array();
var twst = new Array();
var permmv = new Array();
var twstmv = new Array();
var sol = new Array();
var pcperm = new Array();
var pcori = new Array();
var soltimer;
function changeimage(lay, im, nw) {
    Sensor();
    if (netscape) {
        Sensor();
        document.layers[lay].document.images[im].src = nw;
    } else {
        Sensor();
        eval('document.images.' + im + '.src=nw');
    }
    Sensor();
}
function showhide(lay, vis) {
    Sensor();
    if (netscape) {
        Sensor();
        document.layers[lay].visibility = vis ? 'visible' : 'hidden';
    } else {
        Sensor();
        if (goodIE) {
            Sensor();
            eval('document.all.' + lay + '.style.visibility=\'' + (vis ? 'visible' : 'hidden') + '\'');
        } else {
            Sensor();
            document.getElementById(lay).style.visibility = vis ? 'visible' : 'hidden';
        }
    }
    Sensor();
}
function signoff() {
    Sensor();
    showhide('wait', 0);
    Sensor();
}
function signon() {
    Sensor();
    showhide('wait', 1);
    Sensor();
}
function display() {
    Sensor();
    var a;
    Sensor();
    for (a = 0; a < 36; a++) {
        Sensor();
        changeimage('l' + a, 'i' + a, pics[shapgif[a] * 5 + posit[a]].src);
    }
    Sensor();
    if (mode == 2) {
        Sensor();
        changeimage('but4', 'edit', pics[31].src);
    } else {
        Sensor();
        changeimage('but4', 'edit', pics[30].src);
    }
    Sensor();
    if (mode >= 3) {
        Sensor();
        changeimage('but3', 'solve', pics[33].src);
    } else {
        Sensor();
        changeimage('but3', 'solve', pics[32].src);
    }
    Sensor();
    if (mode == 4) {
        Sensor();
        changeimage('but6', 'play', pics[35].src);
    } else {
        Sensor();
        changeimage('but6', 'play', pics[34].src);
    }
    Sensor();
    if (mode == 2) {
        Sensor();
        showhide('editv', 1);
        changeimage('editv', 'editp', pics[36 + Math.floor(edt / 3)].src);
    } else {
        Sensor();
        showhide('editv', 0);
    }
    Sensor();
    if (mode == 1 && solved()) {
        Sensor();
        alert('You solved it!\nYou don\'t get a prize for this though!');
        mode = 0;
    }
    Sensor();
}
function initbrd() {
    Sensor();
    if (mode == 4) {
        Sensor();
        clearTimeout(soltimer);
    }
    Sensor();
    posit = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3);
    mode = 0;
    sol.length = 0;
    Sensor();
}
initbrd();
function solved() {
    Sensor();
    for (var i = 1; i < 9; i++) {
        Sensor();
        if (posit[i] != posit[0]) {
            var IARETREPLACE = false;
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        if (posit[i + 9] != posit[9]) {
            var IARETREPLACE = false;
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        if (posit[i + 18] != posit[18]) {
            var IARETREPLACE = false;
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        if (posit[i + 27] != posit[27]) {
            var IARETREPLACE = false;
            Sensor();
            return IARETREPLACE;
        }
    }
    var IARETREPLACE = true;
    Sensor();
    return IARETREPLACE;
}
function edit() {
    Sensor();
    initbrd();
    Sensor();
    for (var i = 0; i < 36; i += 9) {
        Sensor();
        posit[i + 1] = 4;
        posit[i + 2] = 4;
        posit[i + 4] = 4;
    }
    Sensor();
    mode = 2;
    edt = 0;
    display();
    Sensor();
}
function onResize() {
    Sensor();
    document.location.reload();
    Sensor();
}
function getfacelet(e) {
    Sensor();
    var clickX, clickY;
    Sensor();
    if (netscape || mozilla) {
        Sensor();
        clickX = e.pageX;
        clickY = e.pageY;
    } else {
        Sensor();
        clickX = window.event.clientX - 2;
        clickY = window.event.clientY - 2;
    }
    Sensor();
    for (var i = 0; i < 36; i++) {
        Sensor();
        var x = clickX - fletx[i];
        var y = clickY - flety[i];
        Sensor();
        switch (shapgif[i]) {
        case 0:
            Sensor();
            if (y < 16 && x + 2 * y >= 36 && x - 2 * y <= 29) {
                var IARETREPLACE = i;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            break;
        case 1:
            Sensor();
            if (y > 1 && x - 2 * y >= 2 && x + 2 * y <= 63) {
                var IARETREPLACE = i;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            break;
        case 2:
            Sensor();
            if (x > 1 && 7 * x - 4 * y <= -8 && x - 2 * y >= -78) {
                var IARETREPLACE = i;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            break;
        case 3:
            Sensor();
            if (x < 32 && x - 2 * y <= -3 && 7 * x - 4 * y >= 7) {
                var IARETREPLACE = i;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            break;
        case 4:
            Sensor();
            if (x < 32 && 7 * x + 4 * y >= 239 && x + 2 * y <= 111) {
                var IARETREPLACE = i;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            break;
        case 5:
            Sensor();
            if (x > 1 && x + 2 * y >= 36 && 7 * x + 4 * y <= 224) {
                var IARETREPLACE = i;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            break;
        }
    }
    var IARETREPLACE = -1;
    Sensor();
    return IARETREPLACE;
}
function mouseclick(e) {
    Sensor();
    var f = getfacelet(e);
    Sensor();
    if (f >= 0) {
        Sensor();
        clicked(f);
    }
    Sensor();
}
function reset() {
    Sensor();
    initbrd();
    display();
    Sensor();
}
function mix() {
    Sensor();
    var i, f, lm = -1;
    initbrd();
    Sensor();
    for (i = 0; i < 25; i++) {
        Sensor();
        do {
            Sensor();
            f = Math.floor(Math.random() * 8);
        } while ((f & 3) == lm);
        Sensor();
        lm = f & 3;
        domove(lm);
        Sensor();
        if (f >= 4) {
            Sensor();
            domove(lm);
        }
    }
    Sensor();
    for (i = 4; i <= 7; i++) {
        Sensor();
        f = Math.floor(Math.random() * 3);
        Sensor();
        while (f--) {
            Sensor();
            domove(i);
        }
    }
    Sensor();
    mode = 1;
    display();
    Sensor();
}
var edges = new Array(2, 11, 1, 20, 4, 31, 10, 19, 13, 29, 22, 28);
function clicked(f) {
    Sensor();
    var i, j, k, m;
    Sensor();
    if (mode == 2) {
        Sensor();
        if (moves[f] >= 0) {
            Sensor();
            if (moves[f] < 4) {
                Sensor();
                domove(moves[f] + 8);
            } else {
                Sensor();
                if (moves[f] < 8) {
                    Sensor();
                    domove(moves[f]);
                } else {
                    Sensor();
                    if (moves[f] < 12) {
                        Sensor();
                        domove(moves[f]);
                        domove(moves[f]);
                    } else {
                        Sensor();
                        domove(moves[f] - 8);
                        domove(moves[f] - 8);
                    }
                }
            }
        } else {
            Sensor();
            if (posit[f] == 4) {
                Sensor();
                j = Math.floor(edt / 3);
                Sensor();
                for (i = 0; i < 12 && edges[i] != f; i++) {
                    ;
                    Sensor();
                }
                Sensor();
                m = posit[edges[i ^ 1]];
                k = 0;
                Sensor();
                for (i = 0; i < 12; i += 2) {
                    Sensor();
                    if (posit[edges[i]] + posit[edges[i + 1]] == 1) {
                        Sensor();
                        k++;
                    }
                }
                Sensor();
                if (m != j && (k == 0 || m != 0 || edt >= 6) && (edt != 5 || m != 2)) {
                    Sensor();
                    posit[f] = j;
                    edt++;
                    Sensor();
                    if (edt == 6 || edt == 5) {
                        Sensor();
                        j = -1;
                        k = 0;
                        Sensor();
                        for (i = 0; i < 12; i += 2) {
                            Sensor();
                            if (posit[edges[i]] == 4 && posit[edges[i + 1]] == 4) {
                                Sensor();
                                if (j >= 0) {
                                    Sensor();
                                    j = -2;
                                } else {
                                    Sensor();
                                    j = i;
                                }
                            }
                            Sensor();
                            if (posit[edges[i + 1]] == 0 || posit[edges[i + 1]] == 1 && posit[edges[i]] != 0) {
                                Sensor();
                                k ^= 1;
                            }
                        }
                        Sensor();
                        if (j >= 0) {
                            Sensor();
                            posit[edges[j + k]] = 2;
                        }
                    }
                    Sensor();
                    if (edt >= 7) {
                        Sensor();
                        j = 0;
                        Sensor();
                        for (i = 0; i < 12; i++) {
                            Sensor();
                            if (posit[edges[i]] == 4) {
                                Sensor();
                                if (posit[edges[i ^ 1]] == 1 - m) {
                                    Sensor();
                                    posit[edges[i]] = j ? 2 : 3;
                                    j++;
                                } else {
                                    Sensor();
                                    posit[edges[i]] = 3;
                                }
                            }
                        }
                        Sensor();
                        convertpos();
                        k = 0;
                        Sensor();
                        for (i = 0; i < 6; i++) {
                            Sensor();
                            for (j = i + 1; j < 6; j++) {
                                Sensor();
                                if (pcperm[i] > pcperm[j]) {
                                    Sensor();
                                    k++;
                                }
                            }
                        }
                        Sensor();
                        if (k & 1) {
                            Sensor();
                            for (i = 0; i < 12; i++) {
                                Sensor();
                                if (posit[edges[i]] == 1 - m && posit[edges[i ^ 1]] == 3) {
                                    Sensor();
                                    break;
                                }
                            }
                            Sensor();
                            for (j = 0; j < 12; j++) {
                                Sensor();
                                if (posit[edges[j]] == 1 - m && posit[edges[j ^ 1]] == 2) {
                                    Sensor();
                                    break;
                                }
                            }
                            Sensor();
                            if (i < 12 && j < 12) {
                                Sensor();
                                posit[edges[i ^ 1]] = 2;
                                posit[edges[j ^ 1]] = 3;
                            }
                        }
                        Sensor();
                        mode = 1;
                    }
                }
            }
        }
        Sensor();
        display();
    } else {
        Sensor();
        if (mode < 2) {
            Sensor();
            if (moves[f] >= 0) {
                Sensor();
                if (moves[f] <= 7) {
                    Sensor();
                    domove(moves[f]);
                } else {
                    Sensor();
                    domove(moves[f] - 8);
                    domove(moves[f] - 8);
                }
                Sensor();
                display();
            }
        }
    }
    Sensor();
}
var movelist = new Array();
movelist[0] = new Array(0, 18, 9, 6, 24, 15, 1, 19, 11, 2, 20, 10);
movelist[1] = new Array(23, 3, 30, 26, 7, 34, 22, 1, 31, 20, 4, 28);
movelist[2] = new Array(5, 14, 32, 8, 17, 35, 4, 11, 29, 2, 13, 31);
movelist[3] = new Array(12, 21, 27, 16, 25, 33, 13, 19, 28, 10, 22, 29);
movelist[4] = new Array(0, 18, 9);
movelist[5] = new Array(23, 3, 30);
movelist[6] = new Array(5, 14, 32);
movelist[7] = new Array(12, 21, 27);
movelist[8] = new Array(6, 24, 15);
movelist[9] = new Array(26, 7, 34);
movelist[10] = new Array(8, 17, 35);
movelist[11] = new Array(16, 25, 33);
function domove(m) {
    Sensor();
    for (var i = 0; i < movelist[m].length; i += 3) {
        Sensor();
        var c = posit[movelist[m][i]];
        posit[movelist[m][i]] = posit[movelist[m][i + 2]];
        posit[movelist[m][i + 2]] = posit[movelist[m][i + 1]];
        posit[movelist[m][i + 1]] = c;
    }
    Sensor();
}
var blocksolve = 0;
function solve(m) {
    Sensor();
    var a;
    Sensor();
    if (blocksolve == 0) {
        Sensor();
        if (mode < 2) {
            Sensor();
            blocksolve = 1;
            mode = m ? 4 : 3;
            signon();
            setTimeout('dosolve();', 0);
        } else {
            Sensor();
            if (mode > 2) {
                Sensor();
                if (m) {
                    Sensor();
                    if (mode == 4) {
                        Sensor();
                        mode = 3;
                        clearTimeout(soltimer);
                        display();
                        var IARETREPLACE = void 0;
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                    mode = 4;
                }
                Sensor();
                if (sol.length) {
                    Sensor();
                    domove(sol[0] & 7);
                    Sensor();
                    if (sol[0] & 8) {
                        Sensor();
                        domove(sol[0] & 7);
                    }
                    Sensor();
                    for (a = 1; a < sol.length; a++) {
                        Sensor();
                        sol[a - 1] = sol[a];
                    }
                    Sensor();
                    sol.length--;
                }
                Sensor();
                if (sol.length == 0) {
                    Sensor();
                    mode = 0;
                }
                Sensor();
                display();
                Sensor();
                if (mode == 4) {
                    Sensor();
                    soltimer = setTimeout('solve(0);', 200);
                }
            }
        }
    }
    Sensor();
}
function convertpos() {
    Sensor();
    var i, j, k, l;
    Sensor();
    for (i = 0; i < 6; i++) {
        Sensor();
        j = posit[edges[i + i]];
        k = posit[edges[i + i + 1]];
        Sensor();
        if (j > k) {
            Sensor();
            l = j;
            j = k;
            k = l;
            pcori[i] = 1;
        } else {
            Sensor();
            pcori[i] = 0;
        }
        Sensor();
        if (j == 0) {
            Sensor();
            pcperm[i] = k - 1;
        } else {
            Sensor();
            if (j == 1) {
                Sensor();
                pcperm[i] = k + 1;
            } else {
                Sensor();
                pcperm[i] = 5;
            }
        }
    }
    Sensor();
    pcori[6] = posit[6];
    pcori[7] = posit[7] == 3 ? 2 : posit[7] ? 1 : 0;
    pcori[8] = posit[8] == 1 ? 2 : posit[8] ? 1 : 0;
    pcori[9] = posit[16] == 2 ? 2 : posit[16] == 3 ? 1 : 0;
    Sensor();
}
function dosolve() {
    Sensor();
    var a, b, c, l, t = 0, q = 0;
    sol.length = 0;
    Sensor();
    if (posit[0] == posit[24]) {
        Sensor();
        sol[sol.length] = 4;
    } else {
        Sensor();
        if (posit[0] == posit[15]) {
            Sensor();
            sol[sol.length] = 8 + 4;
        }
    }
    Sensor();
    if (posit[23] == posit[7]) {
        Sensor();
        sol[sol.length] = 5;
    } else {
        Sensor();
        if (posit[23] == posit[34]) {
            Sensor();
            sol[sol.length] = 8 + 5;
        }
    }
    Sensor();
    if (posit[5] == posit[17]) {
        Sensor();
        sol[sol.length] = 6;
    } else {
        Sensor();
        if (posit[5] == posit[35]) {
            Sensor();
            sol[sol.length] = 8 + 6;
        }
    }
    Sensor();
    if (posit[12] == posit[25]) {
        Sensor();
        sol[sol.length] = 7;
    } else {
        Sensor();
        if (posit[12] == posit[33]) {
            Sensor();
            sol[sol.length] = 8 + 7;
        }
    }
    Sensor();
    convertpos();
    Sensor();
    for (a = 0; a < 6; a++) {
        Sensor();
        b = 0;
        Sensor();
        for (c = 0; c < 6; c++) {
            Sensor();
            if (pcperm[c] == a) {
                Sensor();
                break;
            }
            Sensor();
            if (pcperm[c] > a) {
                Sensor();
                b++;
            }
        }
        Sensor();
        q = q * (6 - a) + b;
    }
    Sensor();
    for (a = 9; a >= 6; a--) {
        Sensor();
        t = t * 3 + pcori[a];
    }
    Sensor();
    for (a = 4; a >= 0; a--) {
        Sensor();
        t = t * 2 + pcori[a];
    }
    Sensor();
    if (q != 0 || t != 0) {
        Sensor();
        for (l = 0; l < 12; l++) {
            Sensor();
            if (search(q, t, l, -1)) {
                Sensor();
                break;
            }
        }
    }
    Sensor();
    signoff();
    blocksolve = 0;
    setTimeout('solve(0);', 0);
    Sensor();
}
function search(q, t, l, lm) {
    Sensor();
    if (l == 0) {
        Sensor();
        if (q == 0 && t == 0) {
            var IARETREPLACE = true;
            Sensor();
            return IARETREPLACE;
        }
    } else {
        Sensor();
        if (perm[q] > l || twst[t] > l) {
            var IARETREPLACE = false;
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        var p, s, a, m;
        Sensor();
        for (m = 0; m < 4; m++) {
            Sensor();
            if (m != lm) {
                Sensor();
                p = q;
                s = t;
                Sensor();
                for (a = 0; a < 2; a++) {
                    Sensor();
                    p = permmv[p][m];
                    s = twstmv[s][m];
                    sol[sol.length] = m + 8 * a;
                    Sensor();
                    if (search(p, s, l - 1, m)) {
                        var IARETREPLACE = true;
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                    sol.length--;
                }
            }
        }
    }
    var IARETREPLACE = false;
    Sensor();
    return IARETREPLACE;
}
function calcperm() {
    Sensor();
    var c, p, q, l, m, n;
    Sensor();
    for (p = 0; p < 720; p++) {
        Sensor();
        perm[p] = -1;
        permmv[p] = new Array();
        Sensor();
        for (m = 0; m < 4; m++) {
            Sensor();
            permmv[p][m] = getprmmv(p, m);
        }
    }
    Sensor();
    perm[0] = 0;
    Sensor();
    for (l = 0; l <= 6; l++) {
        Sensor();
        n = 0;
        Sensor();
        for (p = 0; p < 720; p++) {
            Sensor();
            if (perm[p] == l) {
                Sensor();
                for (m = 0; m < 4; m++) {
                    Sensor();
                    q = p;
                    Sensor();
                    for (c = 0; c < 2; c++) {
                        Sensor();
                        q = permmv[q][m];
                        Sensor();
                        if (perm[q] == -1) {
                            Sensor();
                            perm[q] = l + 1;
                            n++;
                        }
                    }
                }
            }
        }
    }
    Sensor();
    for (p = 0; p < 2592; p++) {
        Sensor();
        twst[p] = -1;
        twstmv[p] = new Array();
        Sensor();
        for (m = 0; m < 4; m++) {
            Sensor();
            twstmv[p][m] = gettwsmv(p, m);
        }
    }
    Sensor();
    twst[0] = 0;
    Sensor();
    for (l = 0; l <= 5; l++) {
        Sensor();
        n = 0;
        Sensor();
        for (p = 0; p < 2592; p++) {
            Sensor();
            if (twst[p] == l) {
                Sensor();
                for (m = 0; m < 4; m++) {
                    Sensor();
                    q = p;
                    Sensor();
                    for (c = 0; c < 2; c++) {
                        Sensor();
                        q = twstmv[q][m];
                        Sensor();
                        if (twst[q] == -1) {
                            Sensor();
                            twst[q] = l + 1;
                            n++;
                        }
                    }
                }
            }
        }
    }
    Sensor();
    signoff();
    Sensor();
}
function getprmmv(p, m) {
    Sensor();
    var a, b, c;
    var ps = new Array();
    var q = p;
    Sensor();
    for (a = 1; a <= 6; a++) {
        Sensor();
        c = Math.floor(q / a);
        b = q - a * c;
        q = c;
        Sensor();
        for (c = a - 1; c >= b; c--) {
            Sensor();
            ps[c + 1] = ps[c];
        }
        Sensor();
        ps[b] = 6 - a;
    }
    Sensor();
    if (m == 0) {
        Sensor();
        c = ps[0];
        ps[0] = ps[3];
        ps[3] = ps[1];
        ps[1] = c;
    } else {
        Sensor();
        if (m == 1) {
            Sensor();
            c = ps[1];
            ps[1] = ps[5];
            ps[5] = ps[2];
            ps[2] = c;
        } else {
            Sensor();
            if (m == 2) {
                Sensor();
                c = ps[0];
                ps[0] = ps[2];
                ps[2] = ps[4];
                ps[4] = c;
            } else {
                Sensor();
                if (m == 3) {
                    Sensor();
                    c = ps[3];
                    ps[3] = ps[4];
                    ps[4] = ps[5];
                    ps[5] = c;
                }
            }
        }
    }
    Sensor();
    q = 0;
    Sensor();
    for (a = 0; a < 6; a++) {
        Sensor();
        b = 0;
        Sensor();
        for (c = 0; c < 6; c++) {
            Sensor();
            if (ps[c] == a) {
                Sensor();
                break;
            }
            Sensor();
            if (ps[c] > a) {
                Sensor();
                b++;
            }
        }
        Sensor();
        q = q * (6 - a) + b;
    }
    var IARETREPLACE = q;
    Sensor();
    return IARETREPLACE;
}
function gettwsmv(p, m) {
    Sensor();
    var a, b, c, d = 0;
    var ps = new Array();
    var q = p;
    Sensor();
    for (a = 0; a <= 4; a++) {
        Sensor();
        ps[a] = q & 1;
        q >>= 1;
        d ^= ps[a];
    }
    Sensor();
    ps[5] = d;
    Sensor();
    for (a = 6; a <= 9; a++) {
        Sensor();
        c = Math.floor(q / 3);
        b = q - 3 * c;
        q = c;
        ps[a] = b;
    }
    Sensor();
    if (m == 0) {
        Sensor();
        ps[6]++;
        Sensor();
        if (ps[6] == 3) {
            Sensor();
            ps[6] = 0;
        }
        Sensor();
        c = ps[0];
        ps[0] = ps[3];
        ps[3] = ps[1];
        ps[1] = c;
        ps[1] ^= 1;
        ps[3] ^= 1;
    } else {
        Sensor();
        if (m == 1) {
            Sensor();
            ps[7]++;
            Sensor();
            if (ps[7] == 3) {
                Sensor();
                ps[7] = 0;
            }
            Sensor();
            c = ps[1];
            ps[1] = ps[5];
            ps[5] = ps[2];
            ps[2] = c;
            ps[2] ^= 1;
            ps[5] ^= 1;
        } else {
            Sensor();
            if (m == 2) {
                Sensor();
                ps[8]++;
                Sensor();
                if (ps[8] == 3) {
                    Sensor();
                    ps[8] = 0;
                }
                Sensor();
                c = ps[0];
                ps[0] = ps[2];
                ps[2] = ps[4];
                ps[4] = c;
                ps[0] ^= 1;
                ps[2] ^= 1;
            } else {
                Sensor();
                if (m == 3) {
                    Sensor();
                    ps[9]++;
                    Sensor();
                    if (ps[9] == 3) {
                        Sensor();
                        ps[9] = 0;
                    }
                    Sensor();
                    c = ps[3];
                    ps[3] = ps[4];
                    ps[4] = ps[5];
                    ps[5] = c;
                    ps[3] ^= 1;
                    ps[4] ^= 1;
                }
            }
        }
    }
    Sensor();
    q = 0;
    Sensor();
    for (a = 9; a >= 6; a--) {
        Sensor();
        q = q * 3 + ps[a];
    }
    Sensor();
    for (a = 4; a >= 0; a--) {
        Sensor();
        q = q * 2 + ps[a];
    }
    var IARETREPLACE = q;
    Sensor();
    return IARETREPLACE;
}
function help() {
    Sensor();
    alert('Pyraminx\n\n' + 'This is the Pyraminx puzzle. To turn a\n' + 'tip or vertex piece, simply click on it and\n' + 'it will move in that direction. The aim is\n' + 'of course to rearrange it so that each face\n' + 'has only one colour.\n\n' + 'Buttons:\n' + 'Mix:   Mixes up the cube.\n' + 'Reset: Resets the cube to solved position.\n' + 'Solve: Calculates the shortest solution. Each\n' + '      time you click this button, one more move\n' + '      of the solution is performed.\n' + 'Play:  Calculates the shortest solution, and\n' + '      plays it back automatically.\n' + 'Edit:  Edit the puzzle. The edges are cleared\n' + '      and then you can click the facelets to\n' + '      fill them with each colour in turn. Click\n' + '      a tip or vertex piece to twist it.\n' + 'Help:  Shows this help screen.');
    Sensor();
}
function dostyle() {
    Sensor();
    var s = '<STYLE TYPE="text/css">';
    Sensor();
    for (var i = 0; i < 36; i++) {
        Sensor();
        s += '#l' + i + '{position:absolute; left:' + fletx[i] + '; top:' + flety[i] + '; width:1;}';
    }
    Sensor();
    s += '#editv{position:absolute; left:010; top:30; width:1; visibility:hidden;}';
    s += '#but1{position:absolute; left:003; top:230; width:1;}';
    s += '#but2{position:absolute; left:051; top:230; width:1;}';
    s += '#but4{position:absolute; left:099; top:230; width:1;}';
    s += '#but5{position:absolute; left:147; top:230; width:1;}';
    s += '#but3{position:absolute; left:051; top:246; width:1;}';
    s += '#but6{position:absolute; left:099; top:246; width:1;}';
    s += '#wait{position:absolute; left:0;  top:100; width:200;}';
    s += '</STYLE>';
    var IARETREPLACE = s;
    Sensor();
    return IARETREPLACE;
}
document.write(dostyle());