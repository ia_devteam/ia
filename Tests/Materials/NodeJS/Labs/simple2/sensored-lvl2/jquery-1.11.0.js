Sensor();
(function (global, factory) {
    Sensor();
    if (typeof module === 'object' && typeof module.exports === 'object') {
        Sensor();
        module.exports = global.document ? factory(global, true) : function (w) {
            Sensor();
            if (!w.document) {
                Sensor();
                throw new Error('jQuery requires a window with a document');
            }
            var IARETREPLACE = factory(w);
            Sensor();
            return IARETREPLACE;
        };
    } else {
        Sensor();
        factory(global);
    }
    Sensor();
}(typeof window !== 'undefined' ? window : this, function (window, noGlobal) {
    Sensor();
    var deletedIds = [];
    var slice = deletedIds.slice;
    var concat = deletedIds.concat;
    var push = deletedIds.push;
    var indexOf = deletedIds.indexOf;
    var class2type = {};
    var toString = class2type.toString;
    var hasOwn = class2type.hasOwnProperty;
    var trim = ''.trim;
    var support = {};
    var version = '1.11.0', jQuery = function (selector, context) {
            var IARETREPLACE = new jQuery.fn.init(selector, context);
            Sensor();
            return IARETREPLACE;
        }, rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, rmsPrefix = /^-ms-/, rdashAlpha = /-([\da-z])/gi, fcamelCase = function (all, letter) {
            var IARETREPLACE = letter.toUpperCase();
            Sensor();
            return IARETREPLACE;
        };
    jQuery.fn = jQuery.prototype = {
        jquery: version,
        constructor: jQuery,
        selector: '',
        length: 0,
        toArray: function () {
            var IARETREPLACE = slice.call(this);
            Sensor();
            return IARETREPLACE;
        },
        get: function (num) {
            var IARETREPLACE = num != null ? num < 0 ? this[num + this.length] : this[num] : slice.call(this);
            Sensor();
            return IARETREPLACE;
        },
        pushStack: function (elems) {
            Sensor();
            var ret = jQuery.merge(this.constructor(), elems);
            ret.prevObject = this;
            ret.context = this.context;
            var IARETREPLACE = ret;
            Sensor();
            return IARETREPLACE;
        },
        each: function (callback, args) {
            var IARETREPLACE = jQuery.each(this, callback, args);
            Sensor();
            return IARETREPLACE;
        },
        map: function (callback) {
            var IARETREPLACE = this.pushStack(jQuery.map(this, function (elem, i) {
                var IARETREPLACE = callback.call(elem, i, elem);
                Sensor();
                return IARETREPLACE;
            }));
            Sensor();
            return IARETREPLACE;
        },
        slice: function () {
            var IARETREPLACE = this.pushStack(slice.apply(this, arguments));
            Sensor();
            return IARETREPLACE;
        },
        first: function () {
            var IARETREPLACE = this.eq(0);
            Sensor();
            return IARETREPLACE;
        },
        last: function () {
            var IARETREPLACE = this.eq(-1);
            Sensor();
            return IARETREPLACE;
        },
        eq: function (i) {
            Sensor();
            var len = this.length, j = +i + (i < 0 ? len : 0);
            var IARETREPLACE = this.pushStack(j >= 0 && j < len ? [this[j]] : []);
            Sensor();
            return IARETREPLACE;
        },
        end: function () {
            var IARETREPLACE = this.prevObject || this.constructor(null);
            Sensor();
            return IARETREPLACE;
        },
        push: push,
        sort: deletedIds.sort,
        splice: deletedIds.splice
    };
    jQuery.extend = jQuery.fn.extend = function () {
        Sensor();
        var src, copyIsArray, copy, name, options, clone, target = arguments[0] || {}, i = 1, length = arguments.length, deep = false;
        Sensor();
        if (typeof target === 'boolean') {
            Sensor();
            deep = target;
            target = arguments[i] || {};
            i++;
        }
        Sensor();
        if (typeof target !== 'object' && !jQuery.isFunction(target)) {
            Sensor();
            target = {};
        }
        Sensor();
        if (i === length) {
            Sensor();
            target = this;
            i--;
        }
        Sensor();
        for (; i < length; i++) {
            Sensor();
            if ((options = arguments[i]) != null) {
                Sensor();
                for (name in options) {
                    Sensor();
                    src = target[name];
                    copy = options[name];
                    Sensor();
                    if (target === copy) {
                        Sensor();
                        continue;
                    }
                    Sensor();
                    if (deep && copy && (jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy)))) {
                        Sensor();
                        if (copyIsArray) {
                            Sensor();
                            copyIsArray = false;
                            clone = src && jQuery.isArray(src) ? src : [];
                        } else {
                            Sensor();
                            clone = src && jQuery.isPlainObject(src) ? src : {};
                        }
                        Sensor();
                        target[name] = jQuery.extend(deep, clone, copy);
                    } else {
                        Sensor();
                        if (copy !== undefined) {
                            Sensor();
                            target[name] = copy;
                        }
                    }
                }
            }
        }
        var IARETREPLACE = target;
        Sensor();
        return IARETREPLACE;
    };
    jQuery.extend({
        expando: 'jQuery' + (version + Math.random()).replace(/\D/g, ''),
        isReady: true,
        error: function (msg) {
            Sensor();
            throw new Error(msg);
        },
        noop: function () {
            Sensor();
        },
        isFunction: function (obj) {
            var IARETREPLACE = jQuery.type(obj) === 'function';
            Sensor();
            return IARETREPLACE;
        },
        isArray: Array.isArray || function (obj) {
            var IARETREPLACE = jQuery.type(obj) === 'array';
            Sensor();
            return IARETREPLACE;
        },
        isWindow: function (obj) {
            var IARETREPLACE = obj != null && obj == obj.window;
            Sensor();
            return IARETREPLACE;
        },
        isNumeric: function (obj) {
            var IARETREPLACE = obj - parseFloat(obj) >= 0;
            Sensor();
            return IARETREPLACE;
        },
        isEmptyObject: function (obj) {
            Sensor();
            var name;
            Sensor();
            for (name in obj) {
                var IARETREPLACE = false;
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = true;
            Sensor();
            return IARETREPLACE;
        },
        isPlainObject: function (obj) {
            Sensor();
            var key;
            Sensor();
            if (!obj || jQuery.type(obj) !== 'object' || obj.nodeType || jQuery.isWindow(obj)) {
                var IARETREPLACE = false;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            try {
                Sensor();
                if (obj.constructor && !hasOwn.call(obj, 'constructor') && !hasOwn.call(obj.constructor.prototype, 'isPrototypeOf')) {
                    var IARETREPLACE = false;
                    Sensor();
                    return IARETREPLACE;
                }
            } catch (e) {
                var IARETREPLACE = false;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (support.ownLast) {
                Sensor();
                for (key in obj) {
                    var IARETREPLACE = hasOwn.call(obj, key);
                    Sensor();
                    return IARETREPLACE;
                }
            }
            Sensor();
            for (key in obj) {
                Sensor();
            }
            var IARETREPLACE = key === undefined || hasOwn.call(obj, key);
            Sensor();
            return IARETREPLACE;
        },
        type: function (obj) {
            Sensor();
            if (obj == null) {
                var IARETREPLACE = obj + '';
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = typeof obj === 'object' || typeof obj === 'function' ? class2type[toString.call(obj)] || 'object' : typeof obj;
            Sensor();
            return IARETREPLACE;
        },
        globalEval: function (data) {
            Sensor();
            if (data && jQuery.trim(data)) {
                Sensor();
                (window.execScript || function (data) {
                    Sensor();
                    window['eval'].call(window, data);
                    Sensor();
                })(data);
            }
            Sensor();
        },
        camelCase: function (string) {
            var IARETREPLACE = string.replace(rmsPrefix, 'ms-').replace(rdashAlpha, fcamelCase);
            Sensor();
            return IARETREPLACE;
        },
        nodeName: function (elem, name) {
            var IARETREPLACE = elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
            Sensor();
            return IARETREPLACE;
        },
        each: function (obj, callback, args) {
            Sensor();
            var value, i = 0, length = obj.length, isArray = isArraylike(obj);
            Sensor();
            if (args) {
                Sensor();
                if (isArray) {
                    Sensor();
                    for (; i < length; i++) {
                        Sensor();
                        value = callback.apply(obj[i], args);
                        Sensor();
                        if (value === false) {
                            Sensor();
                            break;
                        }
                    }
                } else {
                    Sensor();
                    for (i in obj) {
                        Sensor();
                        value = callback.apply(obj[i], args);
                        Sensor();
                        if (value === false) {
                            Sensor();
                            break;
                        }
                    }
                }
            } else {
                Sensor();
                if (isArray) {
                    Sensor();
                    for (; i < length; i++) {
                        Sensor();
                        value = callback.call(obj[i], i, obj[i]);
                        Sensor();
                        if (value === false) {
                            Sensor();
                            break;
                        }
                    }
                } else {
                    Sensor();
                    for (i in obj) {
                        Sensor();
                        value = callback.call(obj[i], i, obj[i]);
                        Sensor();
                        if (value === false) {
                            Sensor();
                            break;
                        }
                    }
                }
            }
            var IARETREPLACE = obj;
            Sensor();
            return IARETREPLACE;
        },
        trim: trim && !trim.call('\uFEFF\xA0') ? function (text) {
            var IARETREPLACE = text == null ? '' : trim.call(text);
            Sensor();
            return IARETREPLACE;
        } : function (text) {
            var IARETREPLACE = text == null ? '' : (text + '').replace(rtrim, '');
            Sensor();
            return IARETREPLACE;
        },
        makeArray: function (arr, results) {
            Sensor();
            var ret = results || [];
            Sensor();
            if (arr != null) {
                Sensor();
                if (isArraylike(Object(arr))) {
                    Sensor();
                    jQuery.merge(ret, typeof arr === 'string' ? [arr] : arr);
                } else {
                    Sensor();
                    push.call(ret, arr);
                }
            }
            var IARETREPLACE = ret;
            Sensor();
            return IARETREPLACE;
        },
        inArray: function (elem, arr, i) {
            Sensor();
            var len;
            Sensor();
            if (arr) {
                Sensor();
                if (indexOf) {
                    var IARETREPLACE = indexOf.call(arr, elem, i);
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
                len = arr.length;
                i = i ? i < 0 ? Math.max(0, len + i) : i : 0;
                Sensor();
                for (; i < len; i++) {
                    Sensor();
                    if (i in arr && arr[i] === elem) {
                        var IARETREPLACE = i;
                        Sensor();
                        return IARETREPLACE;
                    }
                }
            }
            var IARETREPLACE = -1;
            Sensor();
            return IARETREPLACE;
        },
        merge: function (first, second) {
            Sensor();
            var len = +second.length, j = 0, i = first.length;
            Sensor();
            while (j < len) {
                Sensor();
                first[i++] = second[j++];
            }
            Sensor();
            if (len !== len) {
                Sensor();
                while (second[j] !== undefined) {
                    Sensor();
                    first[i++] = second[j++];
                }
            }
            Sensor();
            first.length = i;
            var IARETREPLACE = first;
            Sensor();
            return IARETREPLACE;
        },
        grep: function (elems, callback, invert) {
            Sensor();
            var callbackInverse, matches = [], i = 0, length = elems.length, callbackExpect = !invert;
            Sensor();
            for (; i < length; i++) {
                Sensor();
                callbackInverse = !callback(elems[i], i);
                Sensor();
                if (callbackInverse !== callbackExpect) {
                    Sensor();
                    matches.push(elems[i]);
                }
            }
            var IARETREPLACE = matches;
            Sensor();
            return IARETREPLACE;
        },
        map: function (elems, callback, arg) {
            Sensor();
            var value, i = 0, length = elems.length, isArray = isArraylike(elems), ret = [];
            Sensor();
            if (isArray) {
                Sensor();
                for (; i < length; i++) {
                    Sensor();
                    value = callback(elems[i], i, arg);
                    Sensor();
                    if (value != null) {
                        Sensor();
                        ret.push(value);
                    }
                }
            } else {
                Sensor();
                for (i in elems) {
                    Sensor();
                    value = callback(elems[i], i, arg);
                    Sensor();
                    if (value != null) {
                        Sensor();
                        ret.push(value);
                    }
                }
            }
            var IARETREPLACE = concat.apply([], ret);
            Sensor();
            return IARETREPLACE;
        },
        guid: 1,
        proxy: function (fn, context) {
            Sensor();
            var args, proxy, tmp;
            Sensor();
            if (typeof context === 'string') {
                Sensor();
                tmp = fn[context];
                context = fn;
                fn = tmp;
            }
            Sensor();
            if (!jQuery.isFunction(fn)) {
                var IARETREPLACE = undefined;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            args = slice.call(arguments, 2);
            proxy = function () {
                var IARETREPLACE = fn.apply(context || this, args.concat(slice.call(arguments)));
                Sensor();
                return IARETREPLACE;
            };
            proxy.guid = fn.guid = fn.guid || jQuery.guid++;
            var IARETREPLACE = proxy;
            Sensor();
            return IARETREPLACE;
        },
        now: function () {
            var IARETREPLACE = +new Date();
            Sensor();
            return IARETREPLACE;
        },
        support: support
    });
    jQuery.each('Boolean Number String Function Array Date RegExp Object Error'.split(' '), function (i, name) {
        Sensor();
        class2type['[object ' + name + ']'] = name.toLowerCase();
        Sensor();
    });
    function isArraylike(obj) {
        Sensor();
        var length = obj.length, type = jQuery.type(obj);
        Sensor();
        if (type === 'function' || jQuery.isWindow(obj)) {
            var IARETREPLACE = false;
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        if (obj.nodeType === 1 && length) {
            var IARETREPLACE = true;
            Sensor();
            return IARETREPLACE;
        }
        var IARETREPLACE = type === 'array' || length === 0 || typeof length === 'number' && length > 0 && length - 1 in obj;
        Sensor();
        return IARETREPLACE;
    }
    var Sizzle = function (window) {
        Sensor();
        var i, support, Expr, getText, isXML, compile, outermostContext, sortInput, hasDuplicate, setDocument, document, docElem, documentIsHTML, rbuggyQSA, rbuggyMatches, matches, contains, expando = 'sizzle' + -new Date(), preferredDoc = window.document, dirruns = 0, done = 0, classCache = createCache(), tokenCache = createCache(), compilerCache = createCache(), sortOrder = function (a, b) {
                Sensor();
                if (a === b) {
                    Sensor();
                    hasDuplicate = true;
                }
                var IARETREPLACE = 0;
                Sensor();
                return IARETREPLACE;
            }, strundefined = typeof undefined, MAX_NEGATIVE = 1 << 31, hasOwn = {}.hasOwnProperty, arr = [], pop = arr.pop, push_native = arr.push, push = arr.push, slice = arr.slice, indexOf = arr.indexOf || function (elem) {
                Sensor();
                var i = 0, len = this.length;
                Sensor();
                for (; i < len; i++) {
                    Sensor();
                    if (this[i] === elem) {
                        var IARETREPLACE = i;
                        Sensor();
                        return IARETREPLACE;
                    }
                }
                var IARETREPLACE = -1;
                Sensor();
                return IARETREPLACE;
            }, booleans = 'checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped', whitespace = '[\\x20\\t\\r\\n\\f]', characterEncoding = '(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+', identifier = characterEncoding.replace('w', 'w#'), attributes = '\\[' + whitespace + '*(' + characterEncoding + ')' + whitespace + '*(?:([*^$|!~]?=)' + whitespace + '*(?:([\'"])((?:\\\\.|[^\\\\])*?)\\3|(' + identifier + ')|)|)' + whitespace + '*\\]', pseudos = ':(' + characterEncoding + ')(?:\\((([\'"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|' + attributes.replace(3, 8) + ')*)|.*)\\)|)', rtrim = new RegExp('^' + whitespace + '+|((?:^|[^\\\\])(?:\\\\.)*)' + whitespace + '+$', 'g'), rcomma = new RegExp('^' + whitespace + '*,' + whitespace + '*'), rcombinators = new RegExp('^' + whitespace + '*([>+~]|' + whitespace + ')' + whitespace + '*'), rattributeQuotes = new RegExp('=' + whitespace + '*([^\\]\'"]*?)' + whitespace + '*\\]', 'g'), rpseudo = new RegExp(pseudos), ridentifier = new RegExp('^' + identifier + '$'), matchExpr = {
                'ID': new RegExp('^#(' + characterEncoding + ')'),
                'CLASS': new RegExp('^\\.(' + characterEncoding + ')'),
                'TAG': new RegExp('^(' + characterEncoding.replace('w', 'w*') + ')'),
                'ATTR': new RegExp('^' + attributes),
                'PSEUDO': new RegExp('^' + pseudos),
                'CHILD': new RegExp('^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(' + whitespace + '*(even|odd|(([+-]|)(\\d*)n|)' + whitespace + '*(?:([+-]|)' + whitespace + '*(\\d+)|))' + whitespace + '*\\)|)', 'i'),
                'bool': new RegExp('^(?:' + booleans + ')$', 'i'),
                'needsContext': new RegExp('^' + whitespace + '*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(' + whitespace + '*((?:-\\d)?\\d*)' + whitespace + '*\\)|)(?=[^-]|$)', 'i')
            }, rinputs = /^(?:input|select|textarea|button)$/i, rheader = /^h\d$/i, rnative = /^[^{]+\{\s*\[native \w/, rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, rsibling = /[+~]/, rescape = /'|\\/g, runescape = new RegExp('\\\\([\\da-f]{1,6}' + whitespace + '?|(' + whitespace + ')|.)', 'ig'), funescape = function (_, escaped, escapedWhitespace) {
                Sensor();
                var high = '0x' + escaped - 65536;
                var IARETREPLACE = high !== high || escapedWhitespace ? escaped : high < 0 ? String.fromCharCode(high + 65536) : String.fromCharCode(high >> 10 | 55296, high & 1023 | 56320);
                Sensor();
                return IARETREPLACE;
            };
        Sensor();
        try {
            Sensor();
            push.apply(arr = slice.call(preferredDoc.childNodes), preferredDoc.childNodes);
            arr[preferredDoc.childNodes.length].nodeType;
        } catch (e) {
            Sensor();
            push = {
                apply: arr.length ? function (target, els) {
                    Sensor();
                    push_native.apply(target, slice.call(els));
                    Sensor();
                } : function (target, els) {
                    Sensor();
                    var j = target.length, i = 0;
                    Sensor();
                    while (target[j++] = els[i++]) {
                        Sensor();
                    }
                    Sensor();
                    target.length = j - 1;
                    Sensor();
                }
            };
        }
        function Sizzle(selector, context, results, seed) {
            Sensor();
            var match, elem, m, nodeType, i, groups, old, nid, newContext, newSelector;
            Sensor();
            if ((context ? context.ownerDocument || context : preferredDoc) !== document) {
                Sensor();
                setDocument(context);
            }
            Sensor();
            context = context || document;
            Sensor();
            results = results || [];
            Sensor();
            if (!selector || typeof selector !== 'string') {
                var IARETREPLACE = results;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if ((nodeType = context.nodeType) !== 1 && nodeType !== 9) {
                var IARETREPLACE = [];
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (documentIsHTML && !seed) {
                Sensor();
                if (match = rquickExpr.exec(selector)) {
                    Sensor();
                    if (m = match[1]) {
                        Sensor();
                        if (nodeType === 9) {
                            Sensor();
                            elem = context.getElementById(m);
                            Sensor();
                            if (elem && elem.parentNode) {
                                Sensor();
                                if (elem.id === m) {
                                    Sensor();
                                    results.push(elem);
                                    var IARETREPLACE = results;
                                    Sensor();
                                    return IARETREPLACE;
                                }
                            } else {
                                var IARETREPLACE = results;
                                Sensor();
                                return IARETREPLACE;
                            }
                        } else {
                            Sensor();
                            if (context.ownerDocument && (elem = context.ownerDocument.getElementById(m)) && contains(context, elem) && elem.id === m) {
                                Sensor();
                                results.push(elem);
                                var IARETREPLACE = results;
                                Sensor();
                                return IARETREPLACE;
                            }
                        }
                    } else {
                        Sensor();
                        if (match[2]) {
                            Sensor();
                            push.apply(results, context.getElementsByTagName(selector));
                            var IARETREPLACE = results;
                            Sensor();
                            return IARETREPLACE;
                        } else {
                            Sensor();
                            if ((m = match[3]) && support.getElementsByClassName && context.getElementsByClassName) {
                                Sensor();
                                push.apply(results, context.getElementsByClassName(m));
                                var IARETREPLACE = results;
                                Sensor();
                                return IARETREPLACE;
                            }
                        }
                    }
                }
                Sensor();
                if (support.qsa && (!rbuggyQSA || !rbuggyQSA.test(selector))) {
                    Sensor();
                    nid = old = expando;
                    newContext = context;
                    newSelector = nodeType === 9 && selector;
                    Sensor();
                    if (nodeType === 1 && context.nodeName.toLowerCase() !== 'object') {
                        Sensor();
                        groups = tokenize(selector);
                        Sensor();
                        if (old = context.getAttribute('id')) {
                            Sensor();
                            nid = old.replace(rescape, '\\$&');
                        } else {
                            Sensor();
                            context.setAttribute('id', nid);
                        }
                        Sensor();
                        nid = '[id=\'' + nid + '\'] ';
                        i = groups.length;
                        Sensor();
                        while (i--) {
                            Sensor();
                            groups[i] = nid + toSelector(groups[i]);
                        }
                        Sensor();
                        newContext = rsibling.test(selector) && testContext(context.parentNode) || context;
                        newSelector = groups.join(',');
                    }
                    Sensor();
                    if (newSelector) {
                        Sensor();
                        try {
                            Sensor();
                            push.apply(results, newContext.querySelectorAll(newSelector));
                            var IARETREPLACE = results;
                            Sensor();
                            return IARETREPLACE;
                        } catch (qsaError) {
                            Sensor();
                        } finally {
                            Sensor();
                            if (!old) {
                                Sensor();
                                context.removeAttribute('id');
                            }
                        }
                    }
                }
            }
            var IARETREPLACE = select(selector.replace(rtrim, '$1'), context, results, seed);
            Sensor();
            return IARETREPLACE;
        }
        function createCache() {
            Sensor();
            var keys = [];
            function cache(key, value) {
                Sensor();
                if (keys.push(key + ' ') > Expr.cacheLength) {
                    Sensor();
                    delete cache[keys.shift()];
                }
                var IARETREPLACE = cache[key + ' '] = value;
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = cache;
            Sensor();
            return IARETREPLACE;
        }
        function markFunction(fn) {
            Sensor();
            fn[expando] = true;
            var IARETREPLACE = fn;
            Sensor();
            return IARETREPLACE;
        }
        function assert(fn) {
            Sensor();
            var div = document.createElement('div');
            Sensor();
            try {
                var IARETREPLACE = !!fn(div);
                Sensor();
                return IARETREPLACE;
            } catch (e) {
                var IARETREPLACE = false;
                Sensor();
                return IARETREPLACE;
            } finally {
                Sensor();
                if (div.parentNode) {
                    Sensor();
                    div.parentNode.removeChild(div);
                }
                Sensor();
                div = null;
            }
            Sensor();
        }
        function addHandle(attrs, handler) {
            Sensor();
            var arr = attrs.split('|'), i = attrs.length;
            Sensor();
            while (i--) {
                Sensor();
                Expr.attrHandle[arr[i]] = handler;
            }
            Sensor();
        }
        function siblingCheck(a, b) {
            Sensor();
            var cur = b && a, diff = cur && a.nodeType === 1 && b.nodeType === 1 && (~b.sourceIndex || MAX_NEGATIVE) - (~a.sourceIndex || MAX_NEGATIVE);
            Sensor();
            if (diff) {
                var IARETREPLACE = diff;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (cur) {
                Sensor();
                while (cur = cur.nextSibling) {
                    Sensor();
                    if (cur === b) {
                        var IARETREPLACE = -1;
                        Sensor();
                        return IARETREPLACE;
                    }
                }
            }
            var IARETREPLACE = a ? 1 : -1;
            Sensor();
            return IARETREPLACE;
        }
        function createInputPseudo(type) {
            var IARETREPLACE = function (elem) {
                Sensor();
                var name = elem.nodeName.toLowerCase();
                var IARETREPLACE = name === 'input' && elem.type === type;
                Sensor();
                return IARETREPLACE;
            };
            Sensor();
            return IARETREPLACE;
        }
        function createButtonPseudo(type) {
            var IARETREPLACE = function (elem) {
                Sensor();
                var name = elem.nodeName.toLowerCase();
                var IARETREPLACE = (name === 'input' || name === 'button') && elem.type === type;
                Sensor();
                return IARETREPLACE;
            };
            Sensor();
            return IARETREPLACE;
        }
        function createPositionalPseudo(fn) {
            var IARETREPLACE = markFunction(function (argument) {
                Sensor();
                argument = +argument;
                var IARETREPLACE = markFunction(function (seed, matches) {
                    Sensor();
                    var j, matchIndexes = fn([], seed.length, argument), i = matchIndexes.length;
                    Sensor();
                    while (i--) {
                        Sensor();
                        if (seed[j = matchIndexes[i]]) {
                            Sensor();
                            seed[j] = !(matches[j] = seed[j]);
                        }
                    }
                    Sensor();
                });
                Sensor();
                return IARETREPLACE;
            });
            Sensor();
            return IARETREPLACE;
        }
        function testContext(context) {
            var IARETREPLACE = context && typeof context.getElementsByTagName !== strundefined && context;
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        support = Sizzle.support = {};
        isXML = Sizzle.isXML = function (elem) {
            Sensor();
            var documentElement = elem && (elem.ownerDocument || elem).documentElement;
            var IARETREPLACE = documentElement ? documentElement.nodeName !== 'HTML' : false;
            Sensor();
            return IARETREPLACE;
        };
        setDocument = Sizzle.setDocument = function (node) {
            Sensor();
            var hasCompare, doc = node ? node.ownerDocument || node : preferredDoc, parent = doc.defaultView;
            Sensor();
            if (doc === document || doc.nodeType !== 9 || !doc.documentElement) {
                var IARETREPLACE = document;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            document = doc;
            Sensor();
            docElem = doc.documentElement;
            Sensor();
            documentIsHTML = !isXML(doc);
            Sensor();
            if (parent && parent !== parent.top) {
                Sensor();
                if (parent.addEventListener) {
                    Sensor();
                    parent.addEventListener('unload', function () {
                        Sensor();
                        setDocument();
                        Sensor();
                    }, false);
                } else {
                    Sensor();
                    if (parent.attachEvent) {
                        Sensor();
                        parent.attachEvent('onunload', function () {
                            Sensor();
                            setDocument();
                            Sensor();
                        });
                    }
                }
            }
            Sensor();
            support.attributes = assert(function (div) {
                Sensor();
                div.className = 'i';
                var IARETREPLACE = !div.getAttribute('className');
                Sensor();
                return IARETREPLACE;
            });
            support.getElementsByTagName = assert(function (div) {
                Sensor();
                div.appendChild(doc.createComment(''));
                var IARETREPLACE = !div.getElementsByTagName('*').length;
                Sensor();
                return IARETREPLACE;
            });
            support.getElementsByClassName = rnative.test(doc.getElementsByClassName) && assert(function (div) {
                Sensor();
                div.innerHTML = '<div class=\'a\'></div><div class=\'a i\'></div>';
                div.firstChild.className = 'i';
                var IARETREPLACE = div.getElementsByClassName('i').length === 2;
                Sensor();
                return IARETREPLACE;
            });
            support.getById = assert(function (div) {
                Sensor();
                docElem.appendChild(div).id = expando;
                var IARETREPLACE = !doc.getElementsByName || !doc.getElementsByName(expando).length;
                Sensor();
                return IARETREPLACE;
            });
            Sensor();
            if (support.getById) {
                Sensor();
                Expr.find['ID'] = function (id, context) {
                    Sensor();
                    if (typeof context.getElementById !== strundefined && documentIsHTML) {
                        Sensor();
                        var m = context.getElementById(id);
                        var IARETREPLACE = m && m.parentNode ? [m] : [];
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                };
                Expr.filter['ID'] = function (id) {
                    Sensor();
                    var attrId = id.replace(runescape, funescape);
                    var IARETREPLACE = function (elem) {
                        var IARETREPLACE = elem.getAttribute('id') === attrId;
                        Sensor();
                        return IARETREPLACE;
                    };
                    Sensor();
                    return IARETREPLACE;
                };
            } else {
                Sensor();
                delete Expr.find['ID'];
                Expr.filter['ID'] = function (id) {
                    Sensor();
                    var attrId = id.replace(runescape, funescape);
                    var IARETREPLACE = function (elem) {
                        Sensor();
                        var node = typeof elem.getAttributeNode !== strundefined && elem.getAttributeNode('id');
                        var IARETREPLACE = node && node.value === attrId;
                        Sensor();
                        return IARETREPLACE;
                    };
                    Sensor();
                    return IARETREPLACE;
                };
            }
            Sensor();
            Expr.find['TAG'] = support.getElementsByTagName ? function (tag, context) {
                Sensor();
                if (typeof context.getElementsByTagName !== strundefined) {
                    var IARETREPLACE = context.getElementsByTagName(tag);
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            } : function (tag, context) {
                Sensor();
                var elem, tmp = [], i = 0, results = context.getElementsByTagName(tag);
                Sensor();
                if (tag === '*') {
                    Sensor();
                    while (elem = results[i++]) {
                        Sensor();
                        if (elem.nodeType === 1) {
                            Sensor();
                            tmp.push(elem);
                        }
                    }
                    var IARETREPLACE = tmp;
                    Sensor();
                    return IARETREPLACE;
                }
                var IARETREPLACE = results;
                Sensor();
                return IARETREPLACE;
            };
            Expr.find['CLASS'] = support.getElementsByClassName && function (className, context) {
                Sensor();
                if (typeof context.getElementsByClassName !== strundefined && documentIsHTML) {
                    var IARETREPLACE = context.getElementsByClassName(className);
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            };
            rbuggyMatches = [];
            rbuggyQSA = [];
            Sensor();
            if (support.qsa = rnative.test(doc.querySelectorAll)) {
                Sensor();
                assert(function (div) {
                    Sensor();
                    div.innerHTML = '<select t=\'\'><option selected=\'\'></option></select>';
                    Sensor();
                    if (div.querySelectorAll('[t^=\'\']').length) {
                        Sensor();
                        rbuggyQSA.push('[*^$]=' + whitespace + '*(?:\'\'|"")');
                    }
                    Sensor();
                    if (!div.querySelectorAll('[selected]').length) {
                        Sensor();
                        rbuggyQSA.push('\\[' + whitespace + '*(?:value|' + booleans + ')');
                    }
                    Sensor();
                    if (!div.querySelectorAll(':checked').length) {
                        Sensor();
                        rbuggyQSA.push(':checked');
                    }
                    Sensor();
                });
                assert(function (div) {
                    Sensor();
                    var input = doc.createElement('input');
                    input.setAttribute('type', 'hidden');
                    div.appendChild(input).setAttribute('name', 'D');
                    Sensor();
                    if (div.querySelectorAll('[name=d]').length) {
                        Sensor();
                        rbuggyQSA.push('name' + whitespace + '*[*^$|!~]?=');
                    }
                    Sensor();
                    if (!div.querySelectorAll(':enabled').length) {
                        Sensor();
                        rbuggyQSA.push(':enabled', ':disabled');
                    }
                    Sensor();
                    div.querySelectorAll('*,:x');
                    rbuggyQSA.push(',.*:');
                    Sensor();
                });
            }
            Sensor();
            if (support.matchesSelector = rnative.test(matches = docElem.webkitMatchesSelector || docElem.mozMatchesSelector || docElem.oMatchesSelector || docElem.msMatchesSelector)) {
                Sensor();
                assert(function (div) {
                    Sensor();
                    support.disconnectedMatch = matches.call(div, 'div');
                    matches.call(div, '[s!=\'\']:x');
                    rbuggyMatches.push('!=', pseudos);
                    Sensor();
                });
            }
            Sensor();
            rbuggyQSA = rbuggyQSA.length && new RegExp(rbuggyQSA.join('|'));
            Sensor();
            rbuggyMatches = rbuggyMatches.length && new RegExp(rbuggyMatches.join('|'));
            Sensor();
            hasCompare = rnative.test(docElem.compareDocumentPosition);
            Sensor();
            contains = hasCompare || rnative.test(docElem.contains) ? function (a, b) {
                Sensor();
                var adown = a.nodeType === 9 ? a.documentElement : a, bup = b && b.parentNode;
                var IARETREPLACE = a === bup || !!(bup && bup.nodeType === 1 && (adown.contains ? adown.contains(bup) : a.compareDocumentPosition && a.compareDocumentPosition(bup) & 16));
                Sensor();
                return IARETREPLACE;
            } : function (a, b) {
                Sensor();
                if (b) {
                    Sensor();
                    while (b = b.parentNode) {
                        Sensor();
                        if (b === a) {
                            var IARETREPLACE = true;
                            Sensor();
                            return IARETREPLACE;
                        }
                    }
                }
                var IARETREPLACE = false;
                Sensor();
                return IARETREPLACE;
            };
            Sensor();
            sortOrder = hasCompare ? function (a, b) {
                Sensor();
                if (a === b) {
                    Sensor();
                    hasDuplicate = true;
                    var IARETREPLACE = 0;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
                var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
                Sensor();
                if (compare) {
                    var IARETREPLACE = compare;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
                compare = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1;
                Sensor();
                if (compare & 1 || !support.sortDetached && b.compareDocumentPosition(a) === compare) {
                    Sensor();
                    if (a === doc || a.ownerDocument === preferredDoc && contains(preferredDoc, a)) {
                        var IARETREPLACE = -1;
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                    if (b === doc || b.ownerDocument === preferredDoc && contains(preferredDoc, b)) {
                        var IARETREPLACE = 1;
                        Sensor();
                        return IARETREPLACE;
                    }
                    var IARETREPLACE = sortInput ? indexOf.call(sortInput, a) - indexOf.call(sortInput, b) : 0;
                    Sensor();
                    return IARETREPLACE;
                }
                var IARETREPLACE = compare & 4 ? -1 : 1;
                Sensor();
                return IARETREPLACE;
            } : function (a, b) {
                Sensor();
                if (a === b) {
                    Sensor();
                    hasDuplicate = true;
                    var IARETREPLACE = 0;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
                var cur, i = 0, aup = a.parentNode, bup = b.parentNode, ap = [a], bp = [b];
                Sensor();
                if (!aup || !bup) {
                    var IARETREPLACE = a === doc ? -1 : b === doc ? 1 : aup ? -1 : bup ? 1 : sortInput ? indexOf.call(sortInput, a) - indexOf.call(sortInput, b) : 0;
                    Sensor();
                    return IARETREPLACE;
                } else {
                    Sensor();
                    if (aup === bup) {
                        var IARETREPLACE = siblingCheck(a, b);
                        Sensor();
                        return IARETREPLACE;
                    }
                }
                Sensor();
                cur = a;
                Sensor();
                while (cur = cur.parentNode) {
                    Sensor();
                    ap.unshift(cur);
                }
                Sensor();
                cur = b;
                Sensor();
                while (cur = cur.parentNode) {
                    Sensor();
                    bp.unshift(cur);
                }
                Sensor();
                while (ap[i] === bp[i]) {
                    Sensor();
                    i++;
                }
                var IARETREPLACE = i ? siblingCheck(ap[i], bp[i]) : ap[i] === preferredDoc ? -1 : bp[i] === preferredDoc ? 1 : 0;
                Sensor();
                return IARETREPLACE;
            };
            var IARETREPLACE = doc;
            Sensor();
            return IARETREPLACE;
        };
        Sizzle.matches = function (expr, elements) {
            var IARETREPLACE = Sizzle(expr, null, null, elements);
            Sensor();
            return IARETREPLACE;
        };
        Sizzle.matchesSelector = function (elem, expr) {
            Sensor();
            if ((elem.ownerDocument || elem) !== document) {
                Sensor();
                setDocument(elem);
            }
            Sensor();
            expr = expr.replace(rattributeQuotes, '=\'$1\']');
            Sensor();
            if (support.matchesSelector && documentIsHTML && (!rbuggyMatches || !rbuggyMatches.test(expr)) && (!rbuggyQSA || !rbuggyQSA.test(expr))) {
                Sensor();
                try {
                    Sensor();
                    var ret = matches.call(elem, expr);
                    Sensor();
                    if (ret || support.disconnectedMatch || elem.document && elem.document.nodeType !== 11) {
                        var IARETREPLACE = ret;
                        Sensor();
                        return IARETREPLACE;
                    }
                } catch (e) {
                    Sensor();
                }
            }
            var IARETREPLACE = Sizzle(expr, document, null, [elem]).length > 0;
            Sensor();
            return IARETREPLACE;
        };
        Sizzle.contains = function (context, elem) {
            Sensor();
            if ((context.ownerDocument || context) !== document) {
                Sensor();
                setDocument(context);
            }
            var IARETREPLACE = contains(context, elem);
            Sensor();
            return IARETREPLACE;
        };
        Sizzle.attr = function (elem, name) {
            Sensor();
            if ((elem.ownerDocument || elem) !== document) {
                Sensor();
                setDocument(elem);
            }
            Sensor();
            var fn = Expr.attrHandle[name.toLowerCase()], val = fn && hasOwn.call(Expr.attrHandle, name.toLowerCase()) ? fn(elem, name, !documentIsHTML) : undefined;
            var IARETREPLACE = val !== undefined ? val : support.attributes || !documentIsHTML ? elem.getAttribute(name) : (val = elem.getAttributeNode(name)) && val.specified ? val.value : null;
            Sensor();
            return IARETREPLACE;
        };
        Sizzle.error = function (msg) {
            Sensor();
            throw new Error('Syntax error, unrecognized expression: ' + msg);
        };
        Sizzle.uniqueSort = function (results) {
            Sensor();
            var elem, duplicates = [], j = 0, i = 0;
            hasDuplicate = !support.detectDuplicates;
            sortInput = !support.sortStable && results.slice(0);
            results.sort(sortOrder);
            Sensor();
            if (hasDuplicate) {
                Sensor();
                while (elem = results[i++]) {
                    Sensor();
                    if (elem === results[i]) {
                        Sensor();
                        j = duplicates.push(i);
                    }
                }
                Sensor();
                while (j--) {
                    Sensor();
                    results.splice(duplicates[j], 1);
                }
            }
            Sensor();
            sortInput = null;
            var IARETREPLACE = results;
            Sensor();
            return IARETREPLACE;
        };
        getText = Sizzle.getText = function (elem) {
            Sensor();
            var node, ret = '', i = 0, nodeType = elem.nodeType;
            Sensor();
            if (!nodeType) {
                Sensor();
                while (node = elem[i++]) {
                    Sensor();
                    ret += getText(node);
                }
            } else {
                Sensor();
                if (nodeType === 1 || nodeType === 9 || nodeType === 11) {
                    Sensor();
                    if (typeof elem.textContent === 'string') {
                        var IARETREPLACE = elem.textContent;
                        Sensor();
                        return IARETREPLACE;
                    } else {
                        Sensor();
                        for (elem = elem.firstChild; elem; elem = elem.nextSibling) {
                            Sensor();
                            ret += getText(elem);
                        }
                    }
                } else {
                    Sensor();
                    if (nodeType === 3 || nodeType === 4) {
                        var IARETREPLACE = elem.nodeValue;
                        Sensor();
                        return IARETREPLACE;
                    }
                }
            }
            var IARETREPLACE = ret;
            Sensor();
            return IARETREPLACE;
        };
        Expr = Sizzle.selectors = {
            cacheLength: 50,
            createPseudo: markFunction,
            match: matchExpr,
            attrHandle: {},
            find: {},
            relative: {
                '>': {
                    dir: 'parentNode',
                    first: true
                },
                ' ': { dir: 'parentNode' },
                '+': {
                    dir: 'previousSibling',
                    first: true
                },
                '~': { dir: 'previousSibling' }
            },
            preFilter: {
                'ATTR': function (match) {
                    Sensor();
                    match[1] = match[1].replace(runescape, funescape);
                    match[3] = (match[4] || match[5] || '').replace(runescape, funescape);
                    Sensor();
                    if (match[2] === '~=') {
                        Sensor();
                        match[3] = ' ' + match[3] + ' ';
                    }
                    var IARETREPLACE = match.slice(0, 4);
                    Sensor();
                    return IARETREPLACE;
                },
                'CHILD': function (match) {
                    Sensor();
                    match[1] = match[1].toLowerCase();
                    Sensor();
                    if (match[1].slice(0, 3) === 'nth') {
                        Sensor();
                        if (!match[3]) {
                            Sensor();
                            Sizzle.error(match[0]);
                        }
                        Sensor();
                        match[4] = +(match[4] ? match[5] + (match[6] || 1) : 2 * (match[3] === 'even' || match[3] === 'odd'));
                        match[5] = +(match[7] + match[8] || match[3] === 'odd');
                    } else {
                        Sensor();
                        if (match[3]) {
                            Sensor();
                            Sizzle.error(match[0]);
                        }
                    }
                    var IARETREPLACE = match;
                    Sensor();
                    return IARETREPLACE;
                },
                'PSEUDO': function (match) {
                    Sensor();
                    var excess, unquoted = !match[5] && match[2];
                    Sensor();
                    if (matchExpr['CHILD'].test(match[0])) {
                        var IARETREPLACE = null;
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                    if (match[3] && match[4] !== undefined) {
                        Sensor();
                        match[2] = match[4];
                    } else {
                        Sensor();
                        if (unquoted && rpseudo.test(unquoted) && (excess = tokenize(unquoted, true)) && (excess = unquoted.indexOf(')', unquoted.length - excess) - unquoted.length)) {
                            Sensor();
                            match[0] = match[0].slice(0, excess);
                            match[2] = unquoted.slice(0, excess);
                        }
                    }
                    var IARETREPLACE = match.slice(0, 3);
                    Sensor();
                    return IARETREPLACE;
                }
            },
            filter: {
                'TAG': function (nodeNameSelector) {
                    Sensor();
                    var nodeName = nodeNameSelector.replace(runescape, funescape).toLowerCase();
                    var IARETREPLACE = nodeNameSelector === '*' ? function () {
                        var IARETREPLACE = true;
                        Sensor();
                        return IARETREPLACE;
                    } : function (elem) {
                        var IARETREPLACE = elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
                        Sensor();
                        return IARETREPLACE;
                    };
                    Sensor();
                    return IARETREPLACE;
                },
                'CLASS': function (className) {
                    Sensor();
                    var pattern = classCache[className + ' '];
                    var IARETREPLACE = pattern || (pattern = new RegExp('(^|' + whitespace + ')' + className + '(' + whitespace + '|$)')) && classCache(className, function (elem) {
                        var IARETREPLACE = pattern.test(typeof elem.className === 'string' && elem.className || typeof elem.getAttribute !== strundefined && elem.getAttribute('class') || '');
                        Sensor();
                        return IARETREPLACE;
                    });
                    Sensor();
                    return IARETREPLACE;
                },
                'ATTR': function (name, operator, check) {
                    var IARETREPLACE = function (elem) {
                        Sensor();
                        var result = Sizzle.attr(elem, name);
                        Sensor();
                        if (result == null) {
                            var IARETREPLACE = operator === '!=';
                            Sensor();
                            return IARETREPLACE;
                        }
                        Sensor();
                        if (!operator) {
                            var IARETREPLACE = true;
                            Sensor();
                            return IARETREPLACE;
                        }
                        Sensor();
                        result += '';
                        var IARETREPLACE = operator === '=' ? result === check : operator === '!=' ? result !== check : operator === '^=' ? check && result.indexOf(check) === 0 : operator === '*=' ? check && result.indexOf(check) > -1 : operator === '$=' ? check && result.slice(-check.length) === check : operator === '~=' ? (' ' + result + ' ').indexOf(check) > -1 : operator === '|=' ? result === check || result.slice(0, check.length + 1) === check + '-' : false;
                        Sensor();
                        return IARETREPLACE;
                    };
                    Sensor();
                    return IARETREPLACE;
                },
                'CHILD': function (type, what, argument, first, last) {
                    Sensor();
                    var simple = type.slice(0, 3) !== 'nth', forward = type.slice(-4) !== 'last', ofType = what === 'of-type';
                    var IARETREPLACE = first === 1 && last === 0 ? function (elem) {
                        var IARETREPLACE = !!elem.parentNode;
                        Sensor();
                        return IARETREPLACE;
                    } : function (elem, context, xml) {
                        Sensor();
                        var cache, outerCache, node, diff, nodeIndex, start, dir = simple !== forward ? 'nextSibling' : 'previousSibling', parent = elem.parentNode, name = ofType && elem.nodeName.toLowerCase(), useCache = !xml && !ofType;
                        Sensor();
                        if (parent) {
                            Sensor();
                            if (simple) {
                                Sensor();
                                while (dir) {
                                    Sensor();
                                    node = elem;
                                    Sensor();
                                    while (node = node[dir]) {
                                        Sensor();
                                        if (ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1) {
                                            var IARETREPLACE = false;
                                            Sensor();
                                            return IARETREPLACE;
                                        }
                                    }
                                    Sensor();
                                    start = dir = type === 'only' && !start && 'nextSibling';
                                }
                                var IARETREPLACE = true;
                                Sensor();
                                return IARETREPLACE;
                            }
                            Sensor();
                            start = [forward ? parent.firstChild : parent.lastChild];
                            Sensor();
                            if (forward && useCache) {
                                Sensor();
                                outerCache = parent[expando] || (parent[expando] = {});
                                cache = outerCache[type] || [];
                                nodeIndex = cache[0] === dirruns && cache[1];
                                diff = cache[0] === dirruns && cache[2];
                                node = nodeIndex && parent.childNodes[nodeIndex];
                                Sensor();
                                while (node = ++nodeIndex && node && node[dir] || (diff = nodeIndex = 0) || start.pop()) {
                                    Sensor();
                                    if (node.nodeType === 1 && ++diff && node === elem) {
                                        Sensor();
                                        outerCache[type] = [
                                            dirruns,
                                            nodeIndex,
                                            diff
                                        ];
                                        Sensor();
                                        break;
                                    }
                                }
                            } else {
                                Sensor();
                                if (useCache && (cache = (elem[expando] || (elem[expando] = {}))[type]) && cache[0] === dirruns) {
                                    Sensor();
                                    diff = cache[1];
                                } else {
                                    Sensor();
                                    while (node = ++nodeIndex && node && node[dir] || (diff = nodeIndex = 0) || start.pop()) {
                                        Sensor();
                                        if ((ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1) && ++diff) {
                                            Sensor();
                                            if (useCache) {
                                                Sensor();
                                                (node[expando] || (node[expando] = {}))[type] = [
                                                    dirruns,
                                                    diff
                                                ];
                                            }
                                            Sensor();
                                            if (node === elem) {
                                                Sensor();
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            Sensor();
                            diff -= last;
                            var IARETREPLACE = diff === first || diff % first === 0 && diff / first >= 0;
                            Sensor();
                            return IARETREPLACE;
                        }
                        Sensor();
                    };
                    Sensor();
                    return IARETREPLACE;
                },
                'PSEUDO': function (pseudo, argument) {
                    Sensor();
                    var args, fn = Expr.pseudos[pseudo] || Expr.setFilters[pseudo.toLowerCase()] || Sizzle.error('unsupported pseudo: ' + pseudo);
                    Sensor();
                    if (fn[expando]) {
                        var IARETREPLACE = fn(argument);
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                    if (fn.length > 1) {
                        Sensor();
                        args = [
                            pseudo,
                            pseudo,
                            '',
                            argument
                        ];
                        var IARETREPLACE = Expr.setFilters.hasOwnProperty(pseudo.toLowerCase()) ? markFunction(function (seed, matches) {
                            Sensor();
                            var idx, matched = fn(seed, argument), i = matched.length;
                            Sensor();
                            while (i--) {
                                Sensor();
                                idx = indexOf.call(seed, matched[i]);
                                seed[idx] = !(matches[idx] = matched[i]);
                            }
                            Sensor();
                        }) : function (elem) {
                            var IARETREPLACE = fn(elem, 0, args);
                            Sensor();
                            return IARETREPLACE;
                        };
                        Sensor();
                        return IARETREPLACE;
                    }
                    var IARETREPLACE = fn;
                    Sensor();
                    return IARETREPLACE;
                }
            },
            pseudos: {
                'not': markFunction(function (selector) {
                    Sensor();
                    var input = [], results = [], matcher = compile(selector.replace(rtrim, '$1'));
                    var IARETREPLACE = matcher[expando] ? markFunction(function (seed, matches, context, xml) {
                        Sensor();
                        var elem, unmatched = matcher(seed, null, xml, []), i = seed.length;
                        Sensor();
                        while (i--) {
                            Sensor();
                            if (elem = unmatched[i]) {
                                Sensor();
                                seed[i] = !(matches[i] = elem);
                            }
                        }
                        Sensor();
                    }) : function (elem, context, xml) {
                        Sensor();
                        input[0] = elem;
                        matcher(input, null, xml, results);
                        var IARETREPLACE = !results.pop();
                        Sensor();
                        return IARETREPLACE;
                    };
                    Sensor();
                    return IARETREPLACE;
                }),
                'has': markFunction(function (selector) {
                    var IARETREPLACE = function (elem) {
                        var IARETREPLACE = Sizzle(selector, elem).length > 0;
                        Sensor();
                        return IARETREPLACE;
                    };
                    Sensor();
                    return IARETREPLACE;
                }),
                'contains': markFunction(function (text) {
                    var IARETREPLACE = function (elem) {
                        var IARETREPLACE = (elem.textContent || elem.innerText || getText(elem)).indexOf(text) > -1;
                        Sensor();
                        return IARETREPLACE;
                    };
                    Sensor();
                    return IARETREPLACE;
                }),
                'lang': markFunction(function (lang) {
                    Sensor();
                    if (!ridentifier.test(lang || '')) {
                        Sensor();
                        Sizzle.error('unsupported lang: ' + lang);
                    }
                    Sensor();
                    lang = lang.replace(runescape, funescape).toLowerCase();
                    var IARETREPLACE = function (elem) {
                        Sensor();
                        var elemLang;
                        Sensor();
                        do {
                            Sensor();
                            if (elemLang = documentIsHTML ? elem.lang : elem.getAttribute('xml:lang') || elem.getAttribute('lang')) {
                                Sensor();
                                elemLang = elemLang.toLowerCase();
                                var IARETREPLACE = elemLang === lang || elemLang.indexOf(lang + '-') === 0;
                                Sensor();
                                return IARETREPLACE;
                            }
                        } while ((elem = elem.parentNode) && elem.nodeType === 1);
                        var IARETREPLACE = false;
                        Sensor();
                        return IARETREPLACE;
                    };
                    Sensor();
                    return IARETREPLACE;
                }),
                'target': function (elem) {
                    Sensor();
                    var hash = window.location && window.location.hash;
                    var IARETREPLACE = hash && hash.slice(1) === elem.id;
                    Sensor();
                    return IARETREPLACE;
                },
                'root': function (elem) {
                    var IARETREPLACE = elem === docElem;
                    Sensor();
                    return IARETREPLACE;
                },
                'focus': function (elem) {
                    var IARETREPLACE = elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
                    Sensor();
                    return IARETREPLACE;
                },
                'enabled': function (elem) {
                    var IARETREPLACE = elem.disabled === false;
                    Sensor();
                    return IARETREPLACE;
                },
                'disabled': function (elem) {
                    var IARETREPLACE = elem.disabled === true;
                    Sensor();
                    return IARETREPLACE;
                },
                'checked': function (elem) {
                    Sensor();
                    var nodeName = elem.nodeName.toLowerCase();
                    var IARETREPLACE = nodeName === 'input' && !!elem.checked || nodeName === 'option' && !!elem.selected;
                    Sensor();
                    return IARETREPLACE;
                },
                'selected': function (elem) {
                    Sensor();
                    if (elem.parentNode) {
                        Sensor();
                        elem.parentNode.selectedIndex;
                    }
                    var IARETREPLACE = elem.selected === true;
                    Sensor();
                    return IARETREPLACE;
                },
                'empty': function (elem) {
                    Sensor();
                    for (elem = elem.firstChild; elem; elem = elem.nextSibling) {
                        Sensor();
                        if (elem.nodeType < 6) {
                            var IARETREPLACE = false;
                            Sensor();
                            return IARETREPLACE;
                        }
                    }
                    var IARETREPLACE = true;
                    Sensor();
                    return IARETREPLACE;
                },
                'parent': function (elem) {
                    var IARETREPLACE = !Expr.pseudos['empty'](elem);
                    Sensor();
                    return IARETREPLACE;
                },
                'header': function (elem) {
                    var IARETREPLACE = rheader.test(elem.nodeName);
                    Sensor();
                    return IARETREPLACE;
                },
                'input': function (elem) {
                    var IARETREPLACE = rinputs.test(elem.nodeName);
                    Sensor();
                    return IARETREPLACE;
                },
                'button': function (elem) {
                    Sensor();
                    var name = elem.nodeName.toLowerCase();
                    var IARETREPLACE = name === 'input' && elem.type === 'button' || name === 'button';
                    Sensor();
                    return IARETREPLACE;
                },
                'text': function (elem) {
                    Sensor();
                    var attr;
                    var IARETREPLACE = elem.nodeName.toLowerCase() === 'input' && elem.type === 'text' && ((attr = elem.getAttribute('type')) == null || attr.toLowerCase() === 'text');
                    Sensor();
                    return IARETREPLACE;
                },
                'first': createPositionalPseudo(function () {
                    var IARETREPLACE = [0];
                    Sensor();
                    return IARETREPLACE;
                }),
                'last': createPositionalPseudo(function (matchIndexes, length) {
                    var IARETREPLACE = [length - 1];
                    Sensor();
                    return IARETREPLACE;
                }),
                'eq': createPositionalPseudo(function (matchIndexes, length, argument) {
                    var IARETREPLACE = [argument < 0 ? argument + length : argument];
                    Sensor();
                    return IARETREPLACE;
                }),
                'even': createPositionalPseudo(function (matchIndexes, length) {
                    Sensor();
                    var i = 0;
                    Sensor();
                    for (; i < length; i += 2) {
                        Sensor();
                        matchIndexes.push(i);
                    }
                    var IARETREPLACE = matchIndexes;
                    Sensor();
                    return IARETREPLACE;
                }),
                'odd': createPositionalPseudo(function (matchIndexes, length) {
                    Sensor();
                    var i = 1;
                    Sensor();
                    for (; i < length; i += 2) {
                        Sensor();
                        matchIndexes.push(i);
                    }
                    var IARETREPLACE = matchIndexes;
                    Sensor();
                    return IARETREPLACE;
                }),
                'lt': createPositionalPseudo(function (matchIndexes, length, argument) {
                    Sensor();
                    var i = argument < 0 ? argument + length : argument;
                    Sensor();
                    for (; --i >= 0;) {
                        Sensor();
                        matchIndexes.push(i);
                    }
                    var IARETREPLACE = matchIndexes;
                    Sensor();
                    return IARETREPLACE;
                }),
                'gt': createPositionalPseudo(function (matchIndexes, length, argument) {
                    Sensor();
                    var i = argument < 0 ? argument + length : argument;
                    Sensor();
                    for (; ++i < length;) {
                        Sensor();
                        matchIndexes.push(i);
                    }
                    var IARETREPLACE = matchIndexes;
                    Sensor();
                    return IARETREPLACE;
                })
            }
        };
        Expr.pseudos['nth'] = Expr.pseudos['eq'];
        Sensor();
        for (i in {
                radio: true,
                checkbox: true,
                file: true,
                password: true,
                image: true
            }) {
            Sensor();
            Expr.pseudos[i] = createInputPseudo(i);
        }
        Sensor();
        for (i in {
                submit: true,
                reset: true
            }) {
            Sensor();
            Expr.pseudos[i] = createButtonPseudo(i);
        }
        function setFilters() {
            Sensor();
        }
        Sensor();
        setFilters.prototype = Expr.filters = Expr.pseudos;
        Expr.setFilters = new setFilters();
        function tokenize(selector, parseOnly) {
            Sensor();
            var matched, match, tokens, type, soFar, groups, preFilters, cached = tokenCache[selector + ' '];
            Sensor();
            if (cached) {
                var IARETREPLACE = parseOnly ? 0 : cached.slice(0);
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            soFar = selector;
            groups = [];
            preFilters = Expr.preFilter;
            Sensor();
            while (soFar) {
                Sensor();
                if (!matched || (match = rcomma.exec(soFar))) {
                    Sensor();
                    if (match) {
                        Sensor();
                        soFar = soFar.slice(match[0].length) || soFar;
                    }
                    Sensor();
                    groups.push(tokens = []);
                }
                Sensor();
                matched = false;
                Sensor();
                if (match = rcombinators.exec(soFar)) {
                    Sensor();
                    matched = match.shift();
                    tokens.push({
                        value: matched,
                        type: match[0].replace(rtrim, ' ')
                    });
                    soFar = soFar.slice(matched.length);
                }
                Sensor();
                for (type in Expr.filter) {
                    Sensor();
                    if ((match = matchExpr[type].exec(soFar)) && (!preFilters[type] || (match = preFilters[type](match)))) {
                        Sensor();
                        matched = match.shift();
                        tokens.push({
                            value: matched,
                            type: type,
                            matches: match
                        });
                        soFar = soFar.slice(matched.length);
                    }
                }
                Sensor();
                if (!matched) {
                    Sensor();
                    break;
                }
            }
            var IARETREPLACE = parseOnly ? soFar.length : soFar ? Sizzle.error(selector) : tokenCache(selector, groups).slice(0);
            Sensor();
            return IARETREPLACE;
        }
        function toSelector(tokens) {
            Sensor();
            var i = 0, len = tokens.length, selector = '';
            Sensor();
            for (; i < len; i++) {
                Sensor();
                selector += tokens[i].value;
            }
            var IARETREPLACE = selector;
            Sensor();
            return IARETREPLACE;
        }
        function addCombinator(matcher, combinator, base) {
            Sensor();
            var dir = combinator.dir, checkNonElements = base && dir === 'parentNode', doneName = done++;
            var IARETREPLACE = combinator.first ? function (elem, context, xml) {
                Sensor();
                while (elem = elem[dir]) {
                    Sensor();
                    if (elem.nodeType === 1 || checkNonElements) {
                        var IARETREPLACE = matcher(elem, context, xml);
                        Sensor();
                        return IARETREPLACE;
                    }
                }
                Sensor();
            } : function (elem, context, xml) {
                Sensor();
                var oldCache, outerCache, newCache = [
                        dirruns,
                        doneName
                    ];
                Sensor();
                if (xml) {
                    Sensor();
                    while (elem = elem[dir]) {
                        Sensor();
                        if (elem.nodeType === 1 || checkNonElements) {
                            Sensor();
                            if (matcher(elem, context, xml)) {
                                var IARETREPLACE = true;
                                Sensor();
                                return IARETREPLACE;
                            }
                        }
                    }
                } else {
                    Sensor();
                    while (elem = elem[dir]) {
                        Sensor();
                        if (elem.nodeType === 1 || checkNonElements) {
                            Sensor();
                            outerCache = elem[expando] || (elem[expando] = {});
                            Sensor();
                            if ((oldCache = outerCache[dir]) && oldCache[0] === dirruns && oldCache[1] === doneName) {
                                var IARETREPLACE = newCache[2] = oldCache[2];
                                Sensor();
                                return IARETREPLACE;
                            } else {
                                Sensor();
                                outerCache[dir] = newCache;
                                Sensor();
                                if (newCache[2] = matcher(elem, context, xml)) {
                                    var IARETREPLACE = true;
                                    Sensor();
                                    return IARETREPLACE;
                                }
                            }
                        }
                    }
                }
                Sensor();
            };
            Sensor();
            return IARETREPLACE;
        }
        function elementMatcher(matchers) {
            var IARETREPLACE = matchers.length > 1 ? function (elem, context, xml) {
                Sensor();
                var i = matchers.length;
                Sensor();
                while (i--) {
                    Sensor();
                    if (!matchers[i](elem, context, xml)) {
                        var IARETREPLACE = false;
                        Sensor();
                        return IARETREPLACE;
                    }
                }
                var IARETREPLACE = true;
                Sensor();
                return IARETREPLACE;
            } : matchers[0];
            Sensor();
            return IARETREPLACE;
        }
        function condense(unmatched, map, filter, context, xml) {
            Sensor();
            var elem, newUnmatched = [], i = 0, len = unmatched.length, mapped = map != null;
            Sensor();
            for (; i < len; i++) {
                Sensor();
                if (elem = unmatched[i]) {
                    Sensor();
                    if (!filter || filter(elem, context, xml)) {
                        Sensor();
                        newUnmatched.push(elem);
                        Sensor();
                        if (mapped) {
                            Sensor();
                            map.push(i);
                        }
                    }
                }
            }
            var IARETREPLACE = newUnmatched;
            Sensor();
            return IARETREPLACE;
        }
        function setMatcher(preFilter, selector, matcher, postFilter, postFinder, postSelector) {
            Sensor();
            if (postFilter && !postFilter[expando]) {
                Sensor();
                postFilter = setMatcher(postFilter);
            }
            Sensor();
            if (postFinder && !postFinder[expando]) {
                Sensor();
                postFinder = setMatcher(postFinder, postSelector);
            }
            var IARETREPLACE = markFunction(function (seed, results, context, xml) {
                Sensor();
                var temp, i, elem, preMap = [], postMap = [], preexisting = results.length, elems = seed || multipleContexts(selector || '*', context.nodeType ? [context] : context, []), matcherIn = preFilter && (seed || !selector) ? condense(elems, preMap, preFilter, context, xml) : elems, matcherOut = matcher ? postFinder || (seed ? preFilter : preexisting || postFilter) ? [] : results : matcherIn;
                Sensor();
                if (matcher) {
                    Sensor();
                    matcher(matcherIn, matcherOut, context, xml);
                }
                Sensor();
                if (postFilter) {
                    Sensor();
                    temp = condense(matcherOut, postMap);
                    postFilter(temp, [], context, xml);
                    i = temp.length;
                    Sensor();
                    while (i--) {
                        Sensor();
                        if (elem = temp[i]) {
                            Sensor();
                            matcherOut[postMap[i]] = !(matcherIn[postMap[i]] = elem);
                        }
                    }
                }
                Sensor();
                if (seed) {
                    Sensor();
                    if (postFinder || preFilter) {
                        Sensor();
                        if (postFinder) {
                            Sensor();
                            temp = [];
                            i = matcherOut.length;
                            Sensor();
                            while (i--) {
                                Sensor();
                                if (elem = matcherOut[i]) {
                                    Sensor();
                                    temp.push(matcherIn[i] = elem);
                                }
                            }
                            Sensor();
                            postFinder(null, matcherOut = [], temp, xml);
                        }
                        Sensor();
                        i = matcherOut.length;
                        Sensor();
                        while (i--) {
                            Sensor();
                            if ((elem = matcherOut[i]) && (temp = postFinder ? indexOf.call(seed, elem) : preMap[i]) > -1) {
                                Sensor();
                                seed[temp] = !(results[temp] = elem);
                            }
                        }
                    }
                } else {
                    Sensor();
                    matcherOut = condense(matcherOut === results ? matcherOut.splice(preexisting, matcherOut.length) : matcherOut);
                    Sensor();
                    if (postFinder) {
                        Sensor();
                        postFinder(null, results, matcherOut, xml);
                    } else {
                        Sensor();
                        push.apply(results, matcherOut);
                    }
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        }
        function matcherFromTokens(tokens) {
            Sensor();
            var checkContext, matcher, j, len = tokens.length, leadingRelative = Expr.relative[tokens[0].type], implicitRelative = leadingRelative || Expr.relative[' '], i = leadingRelative ? 1 : 0, matchContext = addCombinator(function (elem) {
                    var IARETREPLACE = elem === checkContext;
                    Sensor();
                    return IARETREPLACE;
                }, implicitRelative, true), matchAnyContext = addCombinator(function (elem) {
                    var IARETREPLACE = indexOf.call(checkContext, elem) > -1;
                    Sensor();
                    return IARETREPLACE;
                }, implicitRelative, true), matchers = [function (elem, context, xml) {
                        var IARETREPLACE = !leadingRelative && (xml || context !== outermostContext) || ((checkContext = context).nodeType ? matchContext(elem, context, xml) : matchAnyContext(elem, context, xml));
                        Sensor();
                        return IARETREPLACE;
                    }];
            Sensor();
            for (; i < len; i++) {
                Sensor();
                if (matcher = Expr.relative[tokens[i].type]) {
                    Sensor();
                    matchers = [addCombinator(elementMatcher(matchers), matcher)];
                } else {
                    Sensor();
                    matcher = Expr.filter[tokens[i].type].apply(null, tokens[i].matches);
                    Sensor();
                    if (matcher[expando]) {
                        Sensor();
                        j = ++i;
                        Sensor();
                        for (; j < len; j++) {
                            Sensor();
                            if (Expr.relative[tokens[j].type]) {
                                Sensor();
                                break;
                            }
                        }
                        var IARETREPLACE = setMatcher(i > 1 && elementMatcher(matchers), i > 1 && toSelector(tokens.slice(0, i - 1).concat({ value: tokens[i - 2].type === ' ' ? '*' : '' })).replace(rtrim, '$1'), matcher, i < j && matcherFromTokens(tokens.slice(i, j)), j < len && matcherFromTokens(tokens = tokens.slice(j)), j < len && toSelector(tokens));
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                    matchers.push(matcher);
                }
            }
            var IARETREPLACE = elementMatcher(matchers);
            Sensor();
            return IARETREPLACE;
        }
        function matcherFromGroupMatchers(elementMatchers, setMatchers) {
            Sensor();
            var bySet = setMatchers.length > 0, byElement = elementMatchers.length > 0, superMatcher = function (seed, context, xml, results, outermost) {
                    Sensor();
                    var elem, j, matcher, matchedCount = 0, i = '0', unmatched = seed && [], setMatched = [], contextBackup = outermostContext, elems = seed || byElement && Expr.find['TAG']('*', outermost), dirrunsUnique = dirruns += contextBackup == null ? 1 : Math.random() || 0.1, len = elems.length;
                    Sensor();
                    if (outermost) {
                        Sensor();
                        outermostContext = context !== document && context;
                    }
                    Sensor();
                    for (; i !== len && (elem = elems[i]) != null; i++) {
                        Sensor();
                        if (byElement && elem) {
                            Sensor();
                            j = 0;
                            Sensor();
                            while (matcher = elementMatchers[j++]) {
                                Sensor();
                                if (matcher(elem, context, xml)) {
                                    Sensor();
                                    results.push(elem);
                                    Sensor();
                                    break;
                                }
                            }
                            Sensor();
                            if (outermost) {
                                Sensor();
                                dirruns = dirrunsUnique;
                            }
                        }
                        Sensor();
                        if (bySet) {
                            Sensor();
                            if (elem = !matcher && elem) {
                                Sensor();
                                matchedCount--;
                            }
                            Sensor();
                            if (seed) {
                                Sensor();
                                unmatched.push(elem);
                            }
                        }
                    }
                    Sensor();
                    matchedCount += i;
                    Sensor();
                    if (bySet && i !== matchedCount) {
                        Sensor();
                        j = 0;
                        Sensor();
                        while (matcher = setMatchers[j++]) {
                            Sensor();
                            matcher(unmatched, setMatched, context, xml);
                        }
                        Sensor();
                        if (seed) {
                            Sensor();
                            if (matchedCount > 0) {
                                Sensor();
                                while (i--) {
                                    Sensor();
                                    if (!(unmatched[i] || setMatched[i])) {
                                        Sensor();
                                        setMatched[i] = pop.call(results);
                                    }
                                }
                            }
                            Sensor();
                            setMatched = condense(setMatched);
                        }
                        Sensor();
                        push.apply(results, setMatched);
                        Sensor();
                        if (outermost && !seed && setMatched.length > 0 && matchedCount + setMatchers.length > 1) {
                            Sensor();
                            Sizzle.uniqueSort(results);
                        }
                    }
                    Sensor();
                    if (outermost) {
                        Sensor();
                        dirruns = dirrunsUnique;
                        outermostContext = contextBackup;
                    }
                    var IARETREPLACE = unmatched;
                    Sensor();
                    return IARETREPLACE;
                };
            var IARETREPLACE = bySet ? markFunction(superMatcher) : superMatcher;
            Sensor();
            return IARETREPLACE;
        }
        compile = Sizzle.compile = function (selector, group) {
            Sensor();
            var i, setMatchers = [], elementMatchers = [], cached = compilerCache[selector + ' '];
            Sensor();
            if (!cached) {
                Sensor();
                if (!group) {
                    Sensor();
                    group = tokenize(selector);
                }
                Sensor();
                i = group.length;
                Sensor();
                while (i--) {
                    Sensor();
                    cached = matcherFromTokens(group[i]);
                    Sensor();
                    if (cached[expando]) {
                        Sensor();
                        setMatchers.push(cached);
                    } else {
                        Sensor();
                        elementMatchers.push(cached);
                    }
                }
                Sensor();
                cached = compilerCache(selector, matcherFromGroupMatchers(elementMatchers, setMatchers));
            }
            var IARETREPLACE = cached;
            Sensor();
            return IARETREPLACE;
        };
        function multipleContexts(selector, contexts, results) {
            Sensor();
            var i = 0, len = contexts.length;
            Sensor();
            for (; i < len; i++) {
                Sensor();
                Sizzle(selector, contexts[i], results);
            }
            var IARETREPLACE = results;
            Sensor();
            return IARETREPLACE;
        }
        function select(selector, context, results, seed) {
            Sensor();
            var i, tokens, token, type, find, match = tokenize(selector);
            Sensor();
            if (!seed) {
                Sensor();
                if (match.length === 1) {
                    Sensor();
                    tokens = match[0] = match[0].slice(0);
                    Sensor();
                    if (tokens.length > 2 && (token = tokens[0]).type === 'ID' && support.getById && context.nodeType === 9 && documentIsHTML && Expr.relative[tokens[1].type]) {
                        Sensor();
                        context = (Expr.find['ID'](token.matches[0].replace(runescape, funescape), context) || [])[0];
                        Sensor();
                        if (!context) {
                            var IARETREPLACE = results;
                            Sensor();
                            return IARETREPLACE;
                        }
                        Sensor();
                        selector = selector.slice(tokens.shift().value.length);
                    }
                    Sensor();
                    i = matchExpr['needsContext'].test(selector) ? 0 : tokens.length;
                    Sensor();
                    while (i--) {
                        Sensor();
                        token = tokens[i];
                        Sensor();
                        if (Expr.relative[type = token.type]) {
                            Sensor();
                            break;
                        }
                        Sensor();
                        if (find = Expr.find[type]) {
                            Sensor();
                            if (seed = find(token.matches[0].replace(runescape, funescape), rsibling.test(tokens[0].type) && testContext(context.parentNode) || context)) {
                                Sensor();
                                tokens.splice(i, 1);
                                selector = seed.length && toSelector(tokens);
                                Sensor();
                                if (!selector) {
                                    Sensor();
                                    push.apply(results, seed);
                                    var IARETREPLACE = results;
                                    Sensor();
                                    return IARETREPLACE;
                                }
                                Sensor();
                                break;
                            }
                        }
                    }
                }
            }
            Sensor();
            compile(selector, match)(seed, context, !documentIsHTML, results, rsibling.test(selector) && testContext(context.parentNode) || context);
            var IARETREPLACE = results;
            Sensor();
            return IARETREPLACE;
        }
        support.sortStable = expando.split('').sort(sortOrder).join('') === expando;
        support.detectDuplicates = !!hasDuplicate;
        setDocument();
        support.sortDetached = assert(function (div1) {
            var IARETREPLACE = div1.compareDocumentPosition(document.createElement('div')) & 1;
            Sensor();
            return IARETREPLACE;
        });
        Sensor();
        if (!assert(function (div) {
                Sensor();
                div.innerHTML = '<a href=\'#\'></a>';
                var IARETREPLACE = div.firstChild.getAttribute('href') === '#';
                Sensor();
                return IARETREPLACE;
            })) {
            Sensor();
            addHandle('type|href|height|width', function (elem, name, isXML) {
                Sensor();
                if (!isXML) {
                    var IARETREPLACE = elem.getAttribute(name, name.toLowerCase() === 'type' ? 1 : 2);
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            });
        }
        Sensor();
        if (!support.attributes || !assert(function (div) {
                Sensor();
                div.innerHTML = '<input/>';
                div.firstChild.setAttribute('value', '');
                var IARETREPLACE = div.firstChild.getAttribute('value') === '';
                Sensor();
                return IARETREPLACE;
            })) {
            Sensor();
            addHandle('value', function (elem, name, isXML) {
                Sensor();
                if (!isXML && elem.nodeName.toLowerCase() === 'input') {
                    var IARETREPLACE = elem.defaultValue;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            });
        }
        Sensor();
        if (!assert(function (div) {
                var IARETREPLACE = div.getAttribute('disabled') == null;
                Sensor();
                return IARETREPLACE;
            })) {
            Sensor();
            addHandle(booleans, function (elem, name, isXML) {
                Sensor();
                var val;
                Sensor();
                if (!isXML) {
                    var IARETREPLACE = elem[name] === true ? name.toLowerCase() : (val = elem.getAttributeNode(name)) && val.specified ? val.value : null;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            });
        }
        var IARETREPLACE = Sizzle;
        Sensor();
        return IARETREPLACE;
    }(window);
    jQuery.find = Sizzle;
    jQuery.expr = Sizzle.selectors;
    jQuery.expr[':'] = jQuery.expr.pseudos;
    jQuery.unique = Sizzle.uniqueSort;
    jQuery.text = Sizzle.getText;
    jQuery.isXMLDoc = Sizzle.isXML;
    jQuery.contains = Sizzle.contains;
    var rneedsContext = jQuery.expr.match.needsContext;
    var rsingleTag = /^<(\w+)\s*\/?>(?:<\/\1>|)$/;
    var risSimple = /^.[^:#\[\.,]*$/;
    function winnow(elements, qualifier, not) {
        Sensor();
        if (jQuery.isFunction(qualifier)) {
            var IARETREPLACE = jQuery.grep(elements, function (elem, i) {
                var IARETREPLACE = !!qualifier.call(elem, i, elem) !== not;
                Sensor();
                return IARETREPLACE;
            });
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        if (qualifier.nodeType) {
            var IARETREPLACE = jQuery.grep(elements, function (elem) {
                var IARETREPLACE = elem === qualifier !== not;
                Sensor();
                return IARETREPLACE;
            });
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        if (typeof qualifier === 'string') {
            Sensor();
            if (risSimple.test(qualifier)) {
                var IARETREPLACE = jQuery.filter(qualifier, elements, not);
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            qualifier = jQuery.filter(qualifier, elements);
        }
        var IARETREPLACE = jQuery.grep(elements, function (elem) {
            var IARETREPLACE = jQuery.inArray(elem, qualifier) >= 0 !== not;
            Sensor();
            return IARETREPLACE;
        });
        Sensor();
        return IARETREPLACE;
    }
    jQuery.filter = function (expr, elems, not) {
        Sensor();
        var elem = elems[0];
        Sensor();
        if (not) {
            Sensor();
            expr = ':not(' + expr + ')';
        }
        var IARETREPLACE = elems.length === 1 && elem.nodeType === 1 ? jQuery.find.matchesSelector(elem, expr) ? [elem] : [] : jQuery.find.matches(expr, jQuery.grep(elems, function (elem) {
            var IARETREPLACE = elem.nodeType === 1;
            Sensor();
            return IARETREPLACE;
        }));
        Sensor();
        return IARETREPLACE;
    };
    jQuery.fn.extend({
        find: function (selector) {
            Sensor();
            var i, ret = [], self = this, len = self.length;
            Sensor();
            if (typeof selector !== 'string') {
                var IARETREPLACE = this.pushStack(jQuery(selector).filter(function () {
                    Sensor();
                    for (i = 0; i < len; i++) {
                        Sensor();
                        if (jQuery.contains(self[i], this)) {
                            var IARETREPLACE = true;
                            Sensor();
                            return IARETREPLACE;
                        }
                    }
                    Sensor();
                }));
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            for (i = 0; i < len; i++) {
                Sensor();
                jQuery.find(selector, self[i], ret);
            }
            Sensor();
            ret = this.pushStack(len > 1 ? jQuery.unique(ret) : ret);
            ret.selector = this.selector ? this.selector + ' ' + selector : selector;
            var IARETREPLACE = ret;
            Sensor();
            return IARETREPLACE;
        },
        filter: function (selector) {
            var IARETREPLACE = this.pushStack(winnow(this, selector || [], false));
            Sensor();
            return IARETREPLACE;
        },
        not: function (selector) {
            var IARETREPLACE = this.pushStack(winnow(this, selector || [], true));
            Sensor();
            return IARETREPLACE;
        },
        is: function (selector) {
            var IARETREPLACE = !!winnow(this, typeof selector === 'string' && rneedsContext.test(selector) ? jQuery(selector) : selector || [], false).length;
            Sensor();
            return IARETREPLACE;
        }
    });
    var rootjQuery, document = window.document, rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/, init = jQuery.fn.init = function (selector, context) {
            Sensor();
            var match, elem;
            Sensor();
            if (!selector) {
                var IARETREPLACE = this;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (typeof selector === 'string') {
                Sensor();
                if (selector.charAt(0) === '<' && selector.charAt(selector.length - 1) === '>' && selector.length >= 3) {
                    Sensor();
                    match = [
                        null,
                        selector,
                        null
                    ];
                } else {
                    Sensor();
                    match = rquickExpr.exec(selector);
                }
                Sensor();
                if (match && (match[1] || !context)) {
                    Sensor();
                    if (match[1]) {
                        Sensor();
                        context = context instanceof jQuery ? context[0] : context;
                        jQuery.merge(this, jQuery.parseHTML(match[1], context && context.nodeType ? context.ownerDocument || context : document, true));
                        Sensor();
                        if (rsingleTag.test(match[1]) && jQuery.isPlainObject(context)) {
                            Sensor();
                            for (match in context) {
                                Sensor();
                                if (jQuery.isFunction(this[match])) {
                                    Sensor();
                                    this[match](context[match]);
                                } else {
                                    Sensor();
                                    this.attr(match, context[match]);
                                }
                            }
                        }
                        var IARETREPLACE = this;
                        Sensor();
                        return IARETREPLACE;
                    } else {
                        Sensor();
                        elem = document.getElementById(match[2]);
                        Sensor();
                        if (elem && elem.parentNode) {
                            Sensor();
                            if (elem.id !== match[2]) {
                                var IARETREPLACE = rootjQuery.find(selector);
                                Sensor();
                                return IARETREPLACE;
                            }
                            Sensor();
                            this.length = 1;
                            this[0] = elem;
                        }
                        Sensor();
                        this.context = document;
                        this.selector = selector;
                        var IARETREPLACE = this;
                        Sensor();
                        return IARETREPLACE;
                    }
                } else {
                    Sensor();
                    if (!context || context.jquery) {
                        var IARETREPLACE = (context || rootjQuery).find(selector);
                        Sensor();
                        return IARETREPLACE;
                    } else {
                        var IARETREPLACE = this.constructor(context).find(selector);
                        Sensor();
                        return IARETREPLACE;
                    }
                }
            } else {
                Sensor();
                if (selector.nodeType) {
                    Sensor();
                    this.context = this[0] = selector;
                    this.length = 1;
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                } else {
                    Sensor();
                    if (jQuery.isFunction(selector)) {
                        var IARETREPLACE = typeof rootjQuery.ready !== 'undefined' ? rootjQuery.ready(selector) : selector(jQuery);
                        Sensor();
                        return IARETREPLACE;
                    }
                }
            }
            Sensor();
            if (selector.selector !== undefined) {
                Sensor();
                this.selector = selector.selector;
                this.context = selector.context;
            }
            var IARETREPLACE = jQuery.makeArray(selector, this);
            Sensor();
            return IARETREPLACE;
        };
    init.prototype = jQuery.fn;
    rootjQuery = jQuery(document);
    var rparentsprev = /^(?:parents|prev(?:Until|All))/, guaranteedUnique = {
            children: true,
            contents: true,
            next: true,
            prev: true
        };
    jQuery.extend({
        dir: function (elem, dir, until) {
            Sensor();
            var matched = [], cur = elem[dir];
            Sensor();
            while (cur && cur.nodeType !== 9 && (until === undefined || cur.nodeType !== 1 || !jQuery(cur).is(until))) {
                Sensor();
                if (cur.nodeType === 1) {
                    Sensor();
                    matched.push(cur);
                }
                Sensor();
                cur = cur[dir];
            }
            var IARETREPLACE = matched;
            Sensor();
            return IARETREPLACE;
        },
        sibling: function (n, elem) {
            Sensor();
            var r = [];
            Sensor();
            for (; n; n = n.nextSibling) {
                Sensor();
                if (n.nodeType === 1 && n !== elem) {
                    Sensor();
                    r.push(n);
                }
            }
            var IARETREPLACE = r;
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.fn.extend({
        has: function (target) {
            Sensor();
            var i, targets = jQuery(target, this), len = targets.length;
            var IARETREPLACE = this.filter(function () {
                Sensor();
                for (i = 0; i < len; i++) {
                    Sensor();
                    if (jQuery.contains(this, targets[i])) {
                        var IARETREPLACE = true;
                        Sensor();
                        return IARETREPLACE;
                    }
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        closest: function (selectors, context) {
            Sensor();
            var cur, i = 0, l = this.length, matched = [], pos = rneedsContext.test(selectors) || typeof selectors !== 'string' ? jQuery(selectors, context || this.context) : 0;
            Sensor();
            for (; i < l; i++) {
                Sensor();
                for (cur = this[i]; cur && cur !== context; cur = cur.parentNode) {
                    Sensor();
                    if (cur.nodeType < 11 && (pos ? pos.index(cur) > -1 : cur.nodeType === 1 && jQuery.find.matchesSelector(cur, selectors))) {
                        Sensor();
                        matched.push(cur);
                        Sensor();
                        break;
                    }
                }
            }
            var IARETREPLACE = this.pushStack(matched.length > 1 ? jQuery.unique(matched) : matched);
            Sensor();
            return IARETREPLACE;
        },
        index: function (elem) {
            Sensor();
            if (!elem) {
                var IARETREPLACE = this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (typeof elem === 'string') {
                var IARETREPLACE = jQuery.inArray(this[0], jQuery(elem));
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = jQuery.inArray(elem.jquery ? elem[0] : elem, this);
            Sensor();
            return IARETREPLACE;
        },
        add: function (selector, context) {
            var IARETREPLACE = this.pushStack(jQuery.unique(jQuery.merge(this.get(), jQuery(selector, context))));
            Sensor();
            return IARETREPLACE;
        },
        addBack: function (selector) {
            var IARETREPLACE = this.add(selector == null ? this.prevObject : this.prevObject.filter(selector));
            Sensor();
            return IARETREPLACE;
        }
    });
    function sibling(cur, dir) {
        Sensor();
        do {
            Sensor();
            cur = cur[dir];
        } while (cur && cur.nodeType !== 1);
        var IARETREPLACE = cur;
        Sensor();
        return IARETREPLACE;
    }
    jQuery.each({
        parent: function (elem) {
            Sensor();
            var parent = elem.parentNode;
            var IARETREPLACE = parent && parent.nodeType !== 11 ? parent : null;
            Sensor();
            return IARETREPLACE;
        },
        parents: function (elem) {
            var IARETREPLACE = jQuery.dir(elem, 'parentNode');
            Sensor();
            return IARETREPLACE;
        },
        parentsUntil: function (elem, i, until) {
            var IARETREPLACE = jQuery.dir(elem, 'parentNode', until);
            Sensor();
            return IARETREPLACE;
        },
        next: function (elem) {
            var IARETREPLACE = sibling(elem, 'nextSibling');
            Sensor();
            return IARETREPLACE;
        },
        prev: function (elem) {
            var IARETREPLACE = sibling(elem, 'previousSibling');
            Sensor();
            return IARETREPLACE;
        },
        nextAll: function (elem) {
            var IARETREPLACE = jQuery.dir(elem, 'nextSibling');
            Sensor();
            return IARETREPLACE;
        },
        prevAll: function (elem) {
            var IARETREPLACE = jQuery.dir(elem, 'previousSibling');
            Sensor();
            return IARETREPLACE;
        },
        nextUntil: function (elem, i, until) {
            var IARETREPLACE = jQuery.dir(elem, 'nextSibling', until);
            Sensor();
            return IARETREPLACE;
        },
        prevUntil: function (elem, i, until) {
            var IARETREPLACE = jQuery.dir(elem, 'previousSibling', until);
            Sensor();
            return IARETREPLACE;
        },
        siblings: function (elem) {
            var IARETREPLACE = jQuery.sibling((elem.parentNode || {}).firstChild, elem);
            Sensor();
            return IARETREPLACE;
        },
        children: function (elem) {
            var IARETREPLACE = jQuery.sibling(elem.firstChild);
            Sensor();
            return IARETREPLACE;
        },
        contents: function (elem) {
            var IARETREPLACE = jQuery.nodeName(elem, 'iframe') ? elem.contentDocument || elem.contentWindow.document : jQuery.merge([], elem.childNodes);
            Sensor();
            return IARETREPLACE;
        }
    }, function (name, fn) {
        Sensor();
        jQuery.fn[name] = function (until, selector) {
            Sensor();
            var ret = jQuery.map(this, fn, until);
            Sensor();
            if (name.slice(-5) !== 'Until') {
                Sensor();
                selector = until;
            }
            Sensor();
            if (selector && typeof selector === 'string') {
                Sensor();
                ret = jQuery.filter(selector, ret);
            }
            Sensor();
            if (this.length > 1) {
                Sensor();
                if (!guaranteedUnique[name]) {
                    Sensor();
                    ret = jQuery.unique(ret);
                }
                Sensor();
                if (rparentsprev.test(name)) {
                    Sensor();
                    ret = ret.reverse();
                }
            }
            var IARETREPLACE = this.pushStack(ret);
            Sensor();
            return IARETREPLACE;
        };
        Sensor();
    });
    var rnotwhite = /\S+/g;
    var optionsCache = {};
    function createOptions(options) {
        Sensor();
        var object = optionsCache[options] = {};
        jQuery.each(options.match(rnotwhite) || [], function (_, flag) {
            Sensor();
            object[flag] = true;
            Sensor();
        });
        var IARETREPLACE = object;
        Sensor();
        return IARETREPLACE;
    }
    jQuery.Callbacks = function (options) {
        Sensor();
        options = typeof options === 'string' ? optionsCache[options] || createOptions(options) : jQuery.extend({}, options);
        var firing, memory, fired, firingLength, firingIndex, firingStart, list = [], stack = !options.once && [], fire = function (data) {
                Sensor();
                memory = options.memory && data;
                fired = true;
                firingIndex = firingStart || 0;
                firingStart = 0;
                firingLength = list.length;
                firing = true;
                Sensor();
                for (; list && firingIndex < firingLength; firingIndex++) {
                    Sensor();
                    if (list[firingIndex].apply(data[0], data[1]) === false && options.stopOnFalse) {
                        Sensor();
                        memory = false;
                        Sensor();
                        break;
                    }
                }
                Sensor();
                firing = false;
                Sensor();
                if (list) {
                    Sensor();
                    if (stack) {
                        Sensor();
                        if (stack.length) {
                            Sensor();
                            fire(stack.shift());
                        }
                    } else {
                        Sensor();
                        if (memory) {
                            Sensor();
                            list = [];
                        } else {
                            Sensor();
                            self.disable();
                        }
                    }
                }
                Sensor();
            }, self = {
                add: function () {
                    Sensor();
                    if (list) {
                        Sensor();
                        var start = list.length;
                        (function add(args) {
                            Sensor();
                            jQuery.each(args, function (_, arg) {
                                Sensor();
                                var type = jQuery.type(arg);
                                Sensor();
                                if (type === 'function') {
                                    Sensor();
                                    if (!options.unique || !self.has(arg)) {
                                        Sensor();
                                        list.push(arg);
                                    }
                                } else {
                                    Sensor();
                                    if (arg && arg.length && type !== 'string') {
                                        Sensor();
                                        add(arg);
                                    }
                                }
                                Sensor();
                            });
                            Sensor();
                        }(arguments));
                        Sensor();
                        if (firing) {
                            Sensor();
                            firingLength = list.length;
                        } else {
                            Sensor();
                            if (memory) {
                                Sensor();
                                firingStart = start;
                                fire(memory);
                            }
                        }
                    }
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                },
                remove: function () {
                    Sensor();
                    if (list) {
                        Sensor();
                        jQuery.each(arguments, function (_, arg) {
                            Sensor();
                            var index;
                            Sensor();
                            while ((index = jQuery.inArray(arg, list, index)) > -1) {
                                Sensor();
                                list.splice(index, 1);
                                Sensor();
                                if (firing) {
                                    Sensor();
                                    if (index <= firingLength) {
                                        Sensor();
                                        firingLength--;
                                    }
                                    Sensor();
                                    if (index <= firingIndex) {
                                        Sensor();
                                        firingIndex--;
                                    }
                                }
                            }
                            Sensor();
                        });
                    }
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                },
                has: function (fn) {
                    var IARETREPLACE = fn ? jQuery.inArray(fn, list) > -1 : !!(list && list.length);
                    Sensor();
                    return IARETREPLACE;
                },
                empty: function () {
                    Sensor();
                    list = [];
                    firingLength = 0;
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                },
                disable: function () {
                    Sensor();
                    list = stack = memory = undefined;
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                },
                disabled: function () {
                    var IARETREPLACE = !list;
                    Sensor();
                    return IARETREPLACE;
                },
                lock: function () {
                    Sensor();
                    stack = undefined;
                    Sensor();
                    if (!memory) {
                        Sensor();
                        self.disable();
                    }
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                },
                locked: function () {
                    var IARETREPLACE = !stack;
                    Sensor();
                    return IARETREPLACE;
                },
                fireWith: function (context, args) {
                    Sensor();
                    if (list && (!fired || stack)) {
                        Sensor();
                        args = args || [];
                        args = [
                            context,
                            args.slice ? args.slice() : args
                        ];
                        Sensor();
                        if (firing) {
                            Sensor();
                            stack.push(args);
                        } else {
                            Sensor();
                            fire(args);
                        }
                    }
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                },
                fire: function () {
                    Sensor();
                    self.fireWith(this, arguments);
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                },
                fired: function () {
                    var IARETREPLACE = !!fired;
                    Sensor();
                    return IARETREPLACE;
                }
            };
        var IARETREPLACE = self;
        Sensor();
        return IARETREPLACE;
    };
    jQuery.extend({
        Deferred: function (func) {
            Sensor();
            var tuples = [
                    [
                        'resolve',
                        'done',
                        jQuery.Callbacks('once memory'),
                        'resolved'
                    ],
                    [
                        'reject',
                        'fail',
                        jQuery.Callbacks('once memory'),
                        'rejected'
                    ],
                    [
                        'notify',
                        'progress',
                        jQuery.Callbacks('memory')
                    ]
                ], state = 'pending', promise = {
                    state: function () {
                        var IARETREPLACE = state;
                        Sensor();
                        return IARETREPLACE;
                    },
                    always: function () {
                        Sensor();
                        deferred.done(arguments).fail(arguments);
                        var IARETREPLACE = this;
                        Sensor();
                        return IARETREPLACE;
                    },
                    then: function () {
                        Sensor();
                        var fns = arguments;
                        var IARETREPLACE = jQuery.Deferred(function (newDefer) {
                            Sensor();
                            jQuery.each(tuples, function (i, tuple) {
                                Sensor();
                                var fn = jQuery.isFunction(fns[i]) && fns[i];
                                deferred[tuple[1]](function () {
                                    Sensor();
                                    var returned = fn && fn.apply(this, arguments);
                                    Sensor();
                                    if (returned && jQuery.isFunction(returned.promise)) {
                                        Sensor();
                                        returned.promise().done(newDefer.resolve).fail(newDefer.reject).progress(newDefer.notify);
                                    } else {
                                        Sensor();
                                        newDefer[tuple[0] + 'With'](this === promise ? newDefer.promise() : this, fn ? [returned] : arguments);
                                    }
                                    Sensor();
                                });
                                Sensor();
                            });
                            fns = null;
                            Sensor();
                        }).promise();
                        Sensor();
                        return IARETREPLACE;
                    },
                    promise: function (obj) {
                        var IARETREPLACE = obj != null ? jQuery.extend(obj, promise) : promise;
                        Sensor();
                        return IARETREPLACE;
                    }
                }, deferred = {};
            promise.pipe = promise.then;
            jQuery.each(tuples, function (i, tuple) {
                Sensor();
                var list = tuple[2], stateString = tuple[3];
                promise[tuple[1]] = list.add;
                Sensor();
                if (stateString) {
                    Sensor();
                    list.add(function () {
                        Sensor();
                        state = stateString;
                        Sensor();
                    }, tuples[i ^ 1][2].disable, tuples[2][2].lock);
                }
                Sensor();
                deferred[tuple[0]] = function () {
                    Sensor();
                    deferred[tuple[0] + 'With'](this === deferred ? promise : this, arguments);
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                };
                deferred[tuple[0] + 'With'] = list.fireWith;
                Sensor();
            });
            promise.promise(deferred);
            Sensor();
            if (func) {
                Sensor();
                func.call(deferred, deferred);
            }
            var IARETREPLACE = deferred;
            Sensor();
            return IARETREPLACE;
        },
        when: function (subordinate) {
            Sensor();
            var i = 0, resolveValues = slice.call(arguments), length = resolveValues.length, remaining = length !== 1 || subordinate && jQuery.isFunction(subordinate.promise) ? length : 0, deferred = remaining === 1 ? subordinate : jQuery.Deferred(), updateFunc = function (i, contexts, values) {
                    var IARETREPLACE = function (value) {
                        Sensor();
                        contexts[i] = this;
                        values[i] = arguments.length > 1 ? slice.call(arguments) : value;
                        Sensor();
                        if (values === progressValues) {
                            Sensor();
                            deferred.notifyWith(contexts, values);
                        } else {
                            Sensor();
                            if (!--remaining) {
                                Sensor();
                                deferred.resolveWith(contexts, values);
                            }
                        }
                        Sensor();
                    };
                    Sensor();
                    return IARETREPLACE;
                }, progressValues, progressContexts, resolveContexts;
            Sensor();
            if (length > 1) {
                Sensor();
                progressValues = new Array(length);
                progressContexts = new Array(length);
                resolveContexts = new Array(length);
                Sensor();
                for (; i < length; i++) {
                    Sensor();
                    if (resolveValues[i] && jQuery.isFunction(resolveValues[i].promise)) {
                        Sensor();
                        resolveValues[i].promise().done(updateFunc(i, resolveContexts, resolveValues)).fail(deferred.reject).progress(updateFunc(i, progressContexts, progressValues));
                    } else {
                        Sensor();
                        --remaining;
                    }
                }
            }
            Sensor();
            if (!remaining) {
                Sensor();
                deferred.resolveWith(resolveContexts, resolveValues);
            }
            var IARETREPLACE = deferred.promise();
            Sensor();
            return IARETREPLACE;
        }
    });
    var readyList;
    jQuery.fn.ready = function (fn) {
        Sensor();
        jQuery.ready.promise().done(fn);
        var IARETREPLACE = this;
        Sensor();
        return IARETREPLACE;
    };
    jQuery.extend({
        isReady: false,
        readyWait: 1,
        holdReady: function (hold) {
            Sensor();
            if (hold) {
                Sensor();
                jQuery.readyWait++;
            } else {
                Sensor();
                jQuery.ready(true);
            }
            Sensor();
        },
        ready: function (wait) {
            Sensor();
            if (wait === true ? --jQuery.readyWait : jQuery.isReady) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (!document.body) {
                var IARETREPLACE = setTimeout(jQuery.ready);
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            jQuery.isReady = true;
            Sensor();
            if (wait !== true && --jQuery.readyWait > 0) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            readyList.resolveWith(document, [jQuery]);
            Sensor();
            if (jQuery.fn.trigger) {
                Sensor();
                jQuery(document).trigger('ready').off('ready');
            }
            Sensor();
        }
    });
    function detach() {
        Sensor();
        if (document.addEventListener) {
            Sensor();
            document.removeEventListener('DOMContentLoaded', completed, false);
            window.removeEventListener('load', completed, false);
        } else {
            Sensor();
            document.detachEvent('onreadystatechange', completed);
            window.detachEvent('onload', completed);
        }
        Sensor();
    }
    function completed() {
        Sensor();
        if (document.addEventListener || event.type === 'load' || document.readyState === 'complete') {
            Sensor();
            detach();
            jQuery.ready();
        }
        Sensor();
    }
    jQuery.ready.promise = function (obj) {
        Sensor();
        if (!readyList) {
            Sensor();
            readyList = jQuery.Deferred();
            Sensor();
            if (document.readyState === 'complete') {
                Sensor();
                setTimeout(jQuery.ready);
            } else {
                Sensor();
                if (document.addEventListener) {
                    Sensor();
                    document.addEventListener('DOMContentLoaded', completed, false);
                    window.addEventListener('load', completed, false);
                } else {
                    Sensor();
                    document.attachEvent('onreadystatechange', completed);
                    window.attachEvent('onload', completed);
                    var top = false;
                    Sensor();
                    try {
                        Sensor();
                        top = window.frameElement == null && document.documentElement;
                    } catch (e) {
                        Sensor();
                    }
                    Sensor();
                    if (top && top.doScroll) {
                        Sensor();
                        (function doScrollCheck() {
                            Sensor();
                            if (!jQuery.isReady) {
                                Sensor();
                                try {
                                    Sensor();
                                    top.doScroll('left');
                                } catch (e) {
                                    var IARETREPLACE = setTimeout(doScrollCheck, 50);
                                    Sensor();
                                    return IARETREPLACE;
                                }
                                Sensor();
                                detach();
                                jQuery.ready();
                            }
                            Sensor();
                        }());
                    }
                }
            }
        }
        var IARETREPLACE = readyList.promise(obj);
        Sensor();
        return IARETREPLACE;
    };
    var strundefined = typeof undefined;
    var i;
    Sensor();
    for (i in jQuery(support)) {
        Sensor();
        break;
    }
    Sensor();
    support.ownLast = i !== '0';
    support.inlineBlockNeedsLayout = false;
    jQuery(function () {
        Sensor();
        var container, div, body = document.getElementsByTagName('body')[0];
        Sensor();
        if (!body) {
            var IARETREPLACE;
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        container = document.createElement('div');
        container.style.cssText = 'border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px';
        div = document.createElement('div');
        body.appendChild(container).appendChild(div);
        Sensor();
        if (typeof div.style.zoom !== strundefined) {
            Sensor();
            div.style.cssText = 'border:0;margin:0;width:1px;padding:1px;display:inline;zoom:1';
            Sensor();
            if (support.inlineBlockNeedsLayout = div.offsetWidth === 3) {
                Sensor();
                body.style.zoom = 1;
            }
        }
        Sensor();
        body.removeChild(container);
        container = div = null;
        Sensor();
    });
    (function () {
        Sensor();
        var div = document.createElement('div');
        Sensor();
        if (support.deleteExpando == null) {
            Sensor();
            support.deleteExpando = true;
            Sensor();
            try {
                Sensor();
                delete div.test;
            } catch (e) {
                Sensor();
                support.deleteExpando = false;
            }
        }
        Sensor();
        div = null;
        Sensor();
    }());
    jQuery.acceptData = function (elem) {
        Sensor();
        var noData = jQuery.noData[(elem.nodeName + ' ').toLowerCase()], nodeType = +elem.nodeType || 1;
        var IARETREPLACE = nodeType !== 1 && nodeType !== 9 ? false : !noData || noData !== true && elem.getAttribute('classid') === noData;
        Sensor();
        return IARETREPLACE;
    };
    var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, rmultiDash = /([A-Z])/g;
    function dataAttr(elem, key, data) {
        Sensor();
        if (data === undefined && elem.nodeType === 1) {
            Sensor();
            var name = 'data-' + key.replace(rmultiDash, '-$1').toLowerCase();
            data = elem.getAttribute(name);
            Sensor();
            if (typeof data === 'string') {
                Sensor();
                try {
                    Sensor();
                    data = data === 'true' ? true : data === 'false' ? false : data === 'null' ? null : +data + '' === data ? +data : rbrace.test(data) ? jQuery.parseJSON(data) : data;
                } catch (e) {
                    Sensor();
                }
                Sensor();
                jQuery.data(elem, key, data);
            } else {
                Sensor();
                data = undefined;
            }
        }
        var IARETREPLACE = data;
        Sensor();
        return IARETREPLACE;
    }
    function isEmptyDataObject(obj) {
        Sensor();
        var name;
        Sensor();
        for (name in obj) {
            Sensor();
            if (name === 'data' && jQuery.isEmptyObject(obj[name])) {
                Sensor();
                continue;
            }
            Sensor();
            if (name !== 'toJSON') {
                var IARETREPLACE = false;
                Sensor();
                return IARETREPLACE;
            }
        }
        var IARETREPLACE = true;
        Sensor();
        return IARETREPLACE;
    }
    function internalData(elem, name, data, pvt) {
        Sensor();
        if (!jQuery.acceptData(elem)) {
            var IARETREPLACE;
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        var ret, thisCache, internalKey = jQuery.expando, isNode = elem.nodeType, cache = isNode ? jQuery.cache : elem, id = isNode ? elem[internalKey] : elem[internalKey] && internalKey;
        Sensor();
        if ((!id || !cache[id] || !pvt && !cache[id].data) && data === undefined && typeof name === 'string') {
            var IARETREPLACE;
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        if (!id) {
            Sensor();
            if (isNode) {
                Sensor();
                id = elem[internalKey] = deletedIds.pop() || jQuery.guid++;
            } else {
                Sensor();
                id = internalKey;
            }
        }
        Sensor();
        if (!cache[id]) {
            Sensor();
            cache[id] = isNode ? {} : { toJSON: jQuery.noop };
        }
        Sensor();
        if (typeof name === 'object' || typeof name === 'function') {
            Sensor();
            if (pvt) {
                Sensor();
                cache[id] = jQuery.extend(cache[id], name);
            } else {
                Sensor();
                cache[id].data = jQuery.extend(cache[id].data, name);
            }
        }
        Sensor();
        thisCache = cache[id];
        Sensor();
        if (!pvt) {
            Sensor();
            if (!thisCache.data) {
                Sensor();
                thisCache.data = {};
            }
            Sensor();
            thisCache = thisCache.data;
        }
        Sensor();
        if (data !== undefined) {
            Sensor();
            thisCache[jQuery.camelCase(name)] = data;
        }
        Sensor();
        if (typeof name === 'string') {
            Sensor();
            ret = thisCache[name];
            Sensor();
            if (ret == null) {
                Sensor();
                ret = thisCache[jQuery.camelCase(name)];
            }
        } else {
            Sensor();
            ret = thisCache;
        }
        var IARETREPLACE = ret;
        Sensor();
        return IARETREPLACE;
    }
    function internalRemoveData(elem, name, pvt) {
        Sensor();
        if (!jQuery.acceptData(elem)) {
            var IARETREPLACE;
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        var thisCache, i, isNode = elem.nodeType, cache = isNode ? jQuery.cache : elem, id = isNode ? elem[jQuery.expando] : jQuery.expando;
        Sensor();
        if (!cache[id]) {
            var IARETREPLACE;
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        if (name) {
            Sensor();
            thisCache = pvt ? cache[id] : cache[id].data;
            Sensor();
            if (thisCache) {
                Sensor();
                if (!jQuery.isArray(name)) {
                    Sensor();
                    if (name in thisCache) {
                        Sensor();
                        name = [name];
                    } else {
                        Sensor();
                        name = jQuery.camelCase(name);
                        Sensor();
                        if (name in thisCache) {
                            Sensor();
                            name = [name];
                        } else {
                            Sensor();
                            name = name.split(' ');
                        }
                    }
                } else {
                    Sensor();
                    name = name.concat(jQuery.map(name, jQuery.camelCase));
                }
                Sensor();
                i = name.length;
                Sensor();
                while (i--) {
                    Sensor();
                    delete thisCache[name[i]];
                }
                Sensor();
                if (pvt ? !isEmptyDataObject(thisCache) : !jQuery.isEmptyObject(thisCache)) {
                    var IARETREPLACE;
                    Sensor();
                    return IARETREPLACE;
                }
            }
        }
        Sensor();
        if (!pvt) {
            Sensor();
            delete cache[id].data;
            Sensor();
            if (!isEmptyDataObject(cache[id])) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
        }
        Sensor();
        if (isNode) {
            Sensor();
            jQuery.cleanData([elem], true);
        } else {
            Sensor();
            if (support.deleteExpando || cache != cache.window) {
                Sensor();
                delete cache[id];
            } else {
                Sensor();
                cache[id] = null;
            }
        }
        Sensor();
    }
    jQuery.extend({
        cache: {},
        noData: {
            'applet ': true,
            'embed ': true,
            'object ': 'clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        },
        hasData: function (elem) {
            Sensor();
            elem = elem.nodeType ? jQuery.cache[elem[jQuery.expando]] : elem[jQuery.expando];
            var IARETREPLACE = !!elem && !isEmptyDataObject(elem);
            Sensor();
            return IARETREPLACE;
        },
        data: function (elem, name, data) {
            var IARETREPLACE = internalData(elem, name, data);
            Sensor();
            return IARETREPLACE;
        },
        removeData: function (elem, name) {
            var IARETREPLACE = internalRemoveData(elem, name);
            Sensor();
            return IARETREPLACE;
        },
        _data: function (elem, name, data) {
            var IARETREPLACE = internalData(elem, name, data, true);
            Sensor();
            return IARETREPLACE;
        },
        _removeData: function (elem, name) {
            var IARETREPLACE = internalRemoveData(elem, name, true);
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.fn.extend({
        data: function (key, value) {
            Sensor();
            var i, name, data, elem = this[0], attrs = elem && elem.attributes;
            Sensor();
            if (key === undefined) {
                Sensor();
                if (this.length) {
                    Sensor();
                    data = jQuery.data(elem);
                    Sensor();
                    if (elem.nodeType === 1 && !jQuery._data(elem, 'parsedAttrs')) {
                        Sensor();
                        i = attrs.length;
                        Sensor();
                        while (i--) {
                            Sensor();
                            name = attrs[i].name;
                            Sensor();
                            if (name.indexOf('data-') === 0) {
                                Sensor();
                                name = jQuery.camelCase(name.slice(5));
                                dataAttr(elem, name, data[name]);
                            }
                        }
                        Sensor();
                        jQuery._data(elem, 'parsedAttrs', true);
                    }
                }
                var IARETREPLACE = data;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (typeof key === 'object') {
                var IARETREPLACE = this.each(function () {
                    Sensor();
                    jQuery.data(this, key);
                    Sensor();
                });
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = arguments.length > 1 ? this.each(function () {
                Sensor();
                jQuery.data(this, key, value);
                Sensor();
            }) : elem ? dataAttr(elem, key, jQuery.data(elem, key)) : undefined;
            Sensor();
            return IARETREPLACE;
        },
        removeData: function (key) {
            var IARETREPLACE = this.each(function () {
                Sensor();
                jQuery.removeData(this, key);
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.extend({
        queue: function (elem, type, data) {
            Sensor();
            var queue;
            Sensor();
            if (elem) {
                Sensor();
                type = (type || 'fx') + 'queue';
                queue = jQuery._data(elem, type);
                Sensor();
                if (data) {
                    Sensor();
                    if (!queue || jQuery.isArray(data)) {
                        Sensor();
                        queue = jQuery._data(elem, type, jQuery.makeArray(data));
                    } else {
                        Sensor();
                        queue.push(data);
                    }
                }
                var IARETREPLACE = queue || [];
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
        },
        dequeue: function (elem, type) {
            Sensor();
            type = type || 'fx';
            var queue = jQuery.queue(elem, type), startLength = queue.length, fn = queue.shift(), hooks = jQuery._queueHooks(elem, type), next = function () {
                    Sensor();
                    jQuery.dequeue(elem, type);
                    Sensor();
                };
            Sensor();
            if (fn === 'inprogress') {
                Sensor();
                fn = queue.shift();
                startLength--;
            }
            Sensor();
            if (fn) {
                Sensor();
                if (type === 'fx') {
                    Sensor();
                    queue.unshift('inprogress');
                }
                Sensor();
                delete hooks.stop;
                fn.call(elem, next, hooks);
            }
            Sensor();
            if (!startLength && hooks) {
                Sensor();
                hooks.empty.fire();
            }
            Sensor();
        },
        _queueHooks: function (elem, type) {
            Sensor();
            var key = type + 'queueHooks';
            var IARETREPLACE = jQuery._data(elem, key) || jQuery._data(elem, key, {
                empty: jQuery.Callbacks('once memory').add(function () {
                    Sensor();
                    jQuery._removeData(elem, type + 'queue');
                    jQuery._removeData(elem, key);
                    Sensor();
                })
            });
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.fn.extend({
        queue: function (type, data) {
            Sensor();
            var setter = 2;
            Sensor();
            if (typeof type !== 'string') {
                Sensor();
                data = type;
                type = 'fx';
                setter--;
            }
            Sensor();
            if (arguments.length < setter) {
                var IARETREPLACE = jQuery.queue(this[0], type);
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = data === undefined ? this : this.each(function () {
                Sensor();
                var queue = jQuery.queue(this, type, data);
                jQuery._queueHooks(this, type);
                Sensor();
                if (type === 'fx' && queue[0] !== 'inprogress') {
                    Sensor();
                    jQuery.dequeue(this, type);
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        dequeue: function (type) {
            var IARETREPLACE = this.each(function () {
                Sensor();
                jQuery.dequeue(this, type);
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        clearQueue: function (type) {
            var IARETREPLACE = this.queue(type || 'fx', []);
            Sensor();
            return IARETREPLACE;
        },
        promise: function (type, obj) {
            Sensor();
            var tmp, count = 1, defer = jQuery.Deferred(), elements = this, i = this.length, resolve = function () {
                    Sensor();
                    if (!--count) {
                        Sensor();
                        defer.resolveWith(elements, [elements]);
                    }
                    Sensor();
                };
            Sensor();
            if (typeof type !== 'string') {
                Sensor();
                obj = type;
                type = undefined;
            }
            Sensor();
            type = type || 'fx';
            Sensor();
            while (i--) {
                Sensor();
                tmp = jQuery._data(elements[i], type + 'queueHooks');
                Sensor();
                if (tmp && tmp.empty) {
                    Sensor();
                    count++;
                    tmp.empty.add(resolve);
                }
            }
            Sensor();
            resolve();
            var IARETREPLACE = defer.promise(obj);
            Sensor();
            return IARETREPLACE;
        }
    });
    var pnum = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source;
    var cssExpand = [
        'Top',
        'Right',
        'Bottom',
        'Left'
    ];
    var isHidden = function (elem, el) {
        Sensor();
        elem = el || elem;
        var IARETREPLACE = jQuery.css(elem, 'display') === 'none' || !jQuery.contains(elem.ownerDocument, elem);
        Sensor();
        return IARETREPLACE;
    };
    var access = jQuery.access = function (elems, fn, key, value, chainable, emptyGet, raw) {
        Sensor();
        var i = 0, length = elems.length, bulk = key == null;
        Sensor();
        if (jQuery.type(key) === 'object') {
            Sensor();
            chainable = true;
            Sensor();
            for (i in key) {
                Sensor();
                jQuery.access(elems, fn, i, key[i], true, emptyGet, raw);
            }
        } else {
            Sensor();
            if (value !== undefined) {
                Sensor();
                chainable = true;
                Sensor();
                if (!jQuery.isFunction(value)) {
                    Sensor();
                    raw = true;
                }
                Sensor();
                if (bulk) {
                    Sensor();
                    if (raw) {
                        Sensor();
                        fn.call(elems, value);
                        fn = null;
                    } else {
                        Sensor();
                        bulk = fn;
                        fn = function (elem, key, value) {
                            var IARETREPLACE = bulk.call(jQuery(elem), value);
                            Sensor();
                            return IARETREPLACE;
                        };
                    }
                }
                Sensor();
                if (fn) {
                    Sensor();
                    for (; i < length; i++) {
                        Sensor();
                        fn(elems[i], key, raw ? value : value.call(elems[i], i, fn(elems[i], key)));
                    }
                }
            }
        }
        var IARETREPLACE = chainable ? elems : bulk ? fn.call(elems) : length ? fn(elems[0], key) : emptyGet;
        Sensor();
        return IARETREPLACE;
    };
    var rcheckableType = /^(?:checkbox|radio)$/i;
    (function () {
        Sensor();
        var fragment = document.createDocumentFragment(), div = document.createElement('div'), input = document.createElement('input');
        div.setAttribute('className', 't');
        div.innerHTML = '  <link/><table></table><a href=\'/a\'>a</a>';
        support.leadingWhitespace = div.firstChild.nodeType === 3;
        support.tbody = !div.getElementsByTagName('tbody').length;
        support.htmlSerialize = !!div.getElementsByTagName('link').length;
        support.html5Clone = document.createElement('nav').cloneNode(true).outerHTML !== '<:nav></:nav>';
        input.type = 'checkbox';
        input.checked = true;
        fragment.appendChild(input);
        support.appendChecked = input.checked;
        div.innerHTML = '<textarea>x</textarea>';
        support.noCloneChecked = !!div.cloneNode(true).lastChild.defaultValue;
        fragment.appendChild(div);
        div.innerHTML = '<input type=\'radio\' checked=\'checked\' name=\'t\'/>';
        support.checkClone = div.cloneNode(true).cloneNode(true).lastChild.checked;
        support.noCloneEvent = true;
        Sensor();
        if (div.attachEvent) {
            Sensor();
            div.attachEvent('onclick', function () {
                Sensor();
                support.noCloneEvent = false;
                Sensor();
            });
            div.cloneNode(true).click();
        }
        Sensor();
        if (support.deleteExpando == null) {
            Sensor();
            support.deleteExpando = true;
            Sensor();
            try {
                Sensor();
                delete div.test;
            } catch (e) {
                Sensor();
                support.deleteExpando = false;
            }
        }
        Sensor();
        fragment = div = input = null;
        Sensor();
    }());
    (function () {
        Sensor();
        var i, eventName, div = document.createElement('div');
        Sensor();
        for (i in {
                submit: true,
                change: true,
                focusin: true
            }) {
            Sensor();
            eventName = 'on' + i;
            Sensor();
            if (!(support[i + 'Bubbles'] = eventName in window)) {
                Sensor();
                div.setAttribute(eventName, 't');
                support[i + 'Bubbles'] = div.attributes[eventName].expando === false;
            }
        }
        Sensor();
        div = null;
        Sensor();
    }());
    var rformElems = /^(?:input|select|textarea)$/i, rkeyEvent = /^key/, rmouseEvent = /^(?:mouse|contextmenu)|click/, rfocusMorph = /^(?:focusinfocus|focusoutblur)$/, rtypenamespace = /^([^.]*)(?:\.(.+)|)$/;
    function returnTrue() {
        var IARETREPLACE = true;
        Sensor();
        return IARETREPLACE;
    }
    function returnFalse() {
        var IARETREPLACE = false;
        Sensor();
        return IARETREPLACE;
    }
    function safeActiveElement() {
        Sensor();
        try {
            var IARETREPLACE = document.activeElement;
            Sensor();
            return IARETREPLACE;
        } catch (err) {
            Sensor();
        }
        Sensor();
    }
    jQuery.event = {
        global: {},
        add: function (elem, types, handler, data, selector) {
            Sensor();
            var tmp, events, t, handleObjIn, special, eventHandle, handleObj, handlers, type, namespaces, origType, elemData = jQuery._data(elem);
            Sensor();
            if (!elemData) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (handler.handler) {
                Sensor();
                handleObjIn = handler;
                handler = handleObjIn.handler;
                selector = handleObjIn.selector;
            }
            Sensor();
            if (!handler.guid) {
                Sensor();
                handler.guid = jQuery.guid++;
            }
            Sensor();
            if (!(events = elemData.events)) {
                Sensor();
                events = elemData.events = {};
            }
            Sensor();
            if (!(eventHandle = elemData.handle)) {
                Sensor();
                eventHandle = elemData.handle = function (e) {
                    var IARETREPLACE = typeof jQuery !== strundefined && (!e || jQuery.event.triggered !== e.type) ? jQuery.event.dispatch.apply(eventHandle.elem, arguments) : undefined;
                    Sensor();
                    return IARETREPLACE;
                };
                eventHandle.elem = elem;
            }
            Sensor();
            types = (types || '').match(rnotwhite) || [''];
            t = types.length;
            Sensor();
            while (t--) {
                Sensor();
                tmp = rtypenamespace.exec(types[t]) || [];
                type = origType = tmp[1];
                namespaces = (tmp[2] || '').split('.').sort();
                Sensor();
                if (!type) {
                    Sensor();
                    continue;
                }
                Sensor();
                special = jQuery.event.special[type] || {};
                type = (selector ? special.delegateType : special.bindType) || type;
                special = jQuery.event.special[type] || {};
                handleObj = jQuery.extend({
                    type: type,
                    origType: origType,
                    data: data,
                    handler: handler,
                    guid: handler.guid,
                    selector: selector,
                    needsContext: selector && jQuery.expr.match.needsContext.test(selector),
                    namespace: namespaces.join('.')
                }, handleObjIn);
                Sensor();
                if (!(handlers = events[type])) {
                    Sensor();
                    handlers = events[type] = [];
                    handlers.delegateCount = 0;
                    Sensor();
                    if (!special.setup || special.setup.call(elem, data, namespaces, eventHandle) === false) {
                        Sensor();
                        if (elem.addEventListener) {
                            Sensor();
                            elem.addEventListener(type, eventHandle, false);
                        } else {
                            Sensor();
                            if (elem.attachEvent) {
                                Sensor();
                                elem.attachEvent('on' + type, eventHandle);
                            }
                        }
                    }
                }
                Sensor();
                if (special.add) {
                    Sensor();
                    special.add.call(elem, handleObj);
                    Sensor();
                    if (!handleObj.handler.guid) {
                        Sensor();
                        handleObj.handler.guid = handler.guid;
                    }
                }
                Sensor();
                if (selector) {
                    Sensor();
                    handlers.splice(handlers.delegateCount++, 0, handleObj);
                } else {
                    Sensor();
                    handlers.push(handleObj);
                }
                Sensor();
                jQuery.event.global[type] = true;
            }
            Sensor();
            elem = null;
            Sensor();
        },
        remove: function (elem, types, handler, selector, mappedTypes) {
            Sensor();
            var j, handleObj, tmp, origCount, t, events, special, handlers, type, namespaces, origType, elemData = jQuery.hasData(elem) && jQuery._data(elem);
            Sensor();
            if (!elemData || !(events = elemData.events)) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            types = (types || '').match(rnotwhite) || [''];
            t = types.length;
            Sensor();
            while (t--) {
                Sensor();
                tmp = rtypenamespace.exec(types[t]) || [];
                type = origType = tmp[1];
                namespaces = (tmp[2] || '').split('.').sort();
                Sensor();
                if (!type) {
                    Sensor();
                    for (type in events) {
                        Sensor();
                        jQuery.event.remove(elem, type + types[t], handler, selector, true);
                    }
                    Sensor();
                    continue;
                }
                Sensor();
                special = jQuery.event.special[type] || {};
                type = (selector ? special.delegateType : special.bindType) || type;
                handlers = events[type] || [];
                tmp = tmp[2] && new RegExp('(^|\\.)' + namespaces.join('\\.(?:.*\\.|)') + '(\\.|$)');
                origCount = j = handlers.length;
                Sensor();
                while (j--) {
                    Sensor();
                    handleObj = handlers[j];
                    Sensor();
                    if ((mappedTypes || origType === handleObj.origType) && (!handler || handler.guid === handleObj.guid) && (!tmp || tmp.test(handleObj.namespace)) && (!selector || selector === handleObj.selector || selector === '**' && handleObj.selector)) {
                        Sensor();
                        handlers.splice(j, 1);
                        Sensor();
                        if (handleObj.selector) {
                            Sensor();
                            handlers.delegateCount--;
                        }
                        Sensor();
                        if (special.remove) {
                            Sensor();
                            special.remove.call(elem, handleObj);
                        }
                    }
                }
                Sensor();
                if (origCount && !handlers.length) {
                    Sensor();
                    if (!special.teardown || special.teardown.call(elem, namespaces, elemData.handle) === false) {
                        Sensor();
                        jQuery.removeEvent(elem, type, elemData.handle);
                    }
                    Sensor();
                    delete events[type];
                }
            }
            Sensor();
            if (jQuery.isEmptyObject(events)) {
                Sensor();
                delete elemData.handle;
                jQuery._removeData(elem, 'events');
            }
            Sensor();
        },
        trigger: function (event, data, elem, onlyHandlers) {
            Sensor();
            var handle, ontype, cur, bubbleType, special, tmp, i, eventPath = [elem || document], type = hasOwn.call(event, 'type') ? event.type : event, namespaces = hasOwn.call(event, 'namespace') ? event.namespace.split('.') : [];
            cur = tmp = elem = elem || document;
            Sensor();
            if (elem.nodeType === 3 || elem.nodeType === 8) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (rfocusMorph.test(type + jQuery.event.triggered)) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (type.indexOf('.') >= 0) {
                Sensor();
                namespaces = type.split('.');
                type = namespaces.shift();
                namespaces.sort();
            }
            Sensor();
            ontype = type.indexOf(':') < 0 && 'on' + type;
            event = event[jQuery.expando] ? event : new jQuery.Event(type, typeof event === 'object' && event);
            event.isTrigger = onlyHandlers ? 2 : 3;
            event.namespace = namespaces.join('.');
            event.namespace_re = event.namespace ? new RegExp('(^|\\.)' + namespaces.join('\\.(?:.*\\.|)') + '(\\.|$)') : null;
            event.result = undefined;
            Sensor();
            if (!event.target) {
                Sensor();
                event.target = elem;
            }
            Sensor();
            data = data == null ? [event] : jQuery.makeArray(data, [event]);
            special = jQuery.event.special[type] || {};
            Sensor();
            if (!onlyHandlers && special.trigger && special.trigger.apply(elem, data) === false) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (!onlyHandlers && !special.noBubble && !jQuery.isWindow(elem)) {
                Sensor();
                bubbleType = special.delegateType || type;
                Sensor();
                if (!rfocusMorph.test(bubbleType + type)) {
                    Sensor();
                    cur = cur.parentNode;
                }
                Sensor();
                for (; cur; cur = cur.parentNode) {
                    Sensor();
                    eventPath.push(cur);
                    tmp = cur;
                }
                Sensor();
                if (tmp === (elem.ownerDocument || document)) {
                    Sensor();
                    eventPath.push(tmp.defaultView || tmp.parentWindow || window);
                }
            }
            Sensor();
            i = 0;
            Sensor();
            while ((cur = eventPath[i++]) && !event.isPropagationStopped()) {
                Sensor();
                event.type = i > 1 ? bubbleType : special.bindType || type;
                handle = (jQuery._data(cur, 'events') || {})[event.type] && jQuery._data(cur, 'handle');
                Sensor();
                if (handle) {
                    Sensor();
                    handle.apply(cur, data);
                }
                Sensor();
                handle = ontype && cur[ontype];
                Sensor();
                if (handle && handle.apply && jQuery.acceptData(cur)) {
                    Sensor();
                    event.result = handle.apply(cur, data);
                    Sensor();
                    if (event.result === false) {
                        Sensor();
                        event.preventDefault();
                    }
                }
            }
            Sensor();
            event.type = type;
            Sensor();
            if (!onlyHandlers && !event.isDefaultPrevented()) {
                Sensor();
                if ((!special._default || special._default.apply(eventPath.pop(), data) === false) && jQuery.acceptData(elem)) {
                    Sensor();
                    if (ontype && elem[type] && !jQuery.isWindow(elem)) {
                        Sensor();
                        tmp = elem[ontype];
                        Sensor();
                        if (tmp) {
                            Sensor();
                            elem[ontype] = null;
                        }
                        Sensor();
                        jQuery.event.triggered = type;
                        Sensor();
                        try {
                            Sensor();
                            elem[type]();
                        } catch (e) {
                            Sensor();
                        }
                        Sensor();
                        jQuery.event.triggered = undefined;
                        Sensor();
                        if (tmp) {
                            Sensor();
                            elem[ontype] = tmp;
                        }
                    }
                }
            }
            var IARETREPLACE = event.result;
            Sensor();
            return IARETREPLACE;
        },
        dispatch: function (event) {
            Sensor();
            event = jQuery.event.fix(event);
            var i, ret, handleObj, matched, j, handlerQueue = [], args = slice.call(arguments), handlers = (jQuery._data(this, 'events') || {})[event.type] || [], special = jQuery.event.special[event.type] || {};
            args[0] = event;
            event.delegateTarget = this;
            Sensor();
            if (special.preDispatch && special.preDispatch.call(this, event) === false) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            handlerQueue = jQuery.event.handlers.call(this, event, handlers);
            i = 0;
            Sensor();
            while ((matched = handlerQueue[i++]) && !event.isPropagationStopped()) {
                Sensor();
                event.currentTarget = matched.elem;
                j = 0;
                Sensor();
                while ((handleObj = matched.handlers[j++]) && !event.isImmediatePropagationStopped()) {
                    Sensor();
                    if (!event.namespace_re || event.namespace_re.test(handleObj.namespace)) {
                        Sensor();
                        event.handleObj = handleObj;
                        event.data = handleObj.data;
                        ret = ((jQuery.event.special[handleObj.origType] || {}).handle || handleObj.handler).apply(matched.elem, args);
                        Sensor();
                        if (ret !== undefined) {
                            Sensor();
                            if ((event.result = ret) === false) {
                                Sensor();
                                event.preventDefault();
                                event.stopPropagation();
                            }
                        }
                    }
                }
            }
            Sensor();
            if (special.postDispatch) {
                Sensor();
                special.postDispatch.call(this, event);
            }
            var IARETREPLACE = event.result;
            Sensor();
            return IARETREPLACE;
        },
        handlers: function (event, handlers) {
            Sensor();
            var sel, handleObj, matches, i, handlerQueue = [], delegateCount = handlers.delegateCount, cur = event.target;
            Sensor();
            if (delegateCount && cur.nodeType && (!event.button || event.type !== 'click')) {
                Sensor();
                for (; cur != this; cur = cur.parentNode || this) {
                    Sensor();
                    if (cur.nodeType === 1 && (cur.disabled !== true || event.type !== 'click')) {
                        Sensor();
                        matches = [];
                        Sensor();
                        for (i = 0; i < delegateCount; i++) {
                            Sensor();
                            handleObj = handlers[i];
                            sel = handleObj.selector + ' ';
                            Sensor();
                            if (matches[sel] === undefined) {
                                Sensor();
                                matches[sel] = handleObj.needsContext ? jQuery(sel, this).index(cur) >= 0 : jQuery.find(sel, this, null, [cur]).length;
                            }
                            Sensor();
                            if (matches[sel]) {
                                Sensor();
                                matches.push(handleObj);
                            }
                        }
                        Sensor();
                        if (matches.length) {
                            Sensor();
                            handlerQueue.push({
                                elem: cur,
                                handlers: matches
                            });
                        }
                    }
                }
            }
            Sensor();
            if (delegateCount < handlers.length) {
                Sensor();
                handlerQueue.push({
                    elem: this,
                    handlers: handlers.slice(delegateCount)
                });
            }
            var IARETREPLACE = handlerQueue;
            Sensor();
            return IARETREPLACE;
        },
        fix: function (event) {
            Sensor();
            if (event[jQuery.expando]) {
                var IARETREPLACE = event;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            var i, prop, copy, type = event.type, originalEvent = event, fixHook = this.fixHooks[type];
            Sensor();
            if (!fixHook) {
                Sensor();
                this.fixHooks[type] = fixHook = rmouseEvent.test(type) ? this.mouseHooks : rkeyEvent.test(type) ? this.keyHooks : {};
            }
            Sensor();
            copy = fixHook.props ? this.props.concat(fixHook.props) : this.props;
            event = new jQuery.Event(originalEvent);
            i = copy.length;
            Sensor();
            while (i--) {
                Sensor();
                prop = copy[i];
                event[prop] = originalEvent[prop];
            }
            Sensor();
            if (!event.target) {
                Sensor();
                event.target = originalEvent.srcElement || document;
            }
            Sensor();
            if (event.target.nodeType === 3) {
                Sensor();
                event.target = event.target.parentNode;
            }
            Sensor();
            event.metaKey = !!event.metaKey;
            var IARETREPLACE = fixHook.filter ? fixHook.filter(event, originalEvent) : event;
            Sensor();
            return IARETREPLACE;
        },
        props: 'altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which'.split(' '),
        fixHooks: {},
        keyHooks: {
            props: 'char charCode key keyCode'.split(' '),
            filter: function (event, original) {
                Sensor();
                if (event.which == null) {
                    Sensor();
                    event.which = original.charCode != null ? original.charCode : original.keyCode;
                }
                var IARETREPLACE = event;
                Sensor();
                return IARETREPLACE;
            }
        },
        mouseHooks: {
            props: 'button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement'.split(' '),
            filter: function (event, original) {
                Sensor();
                var body, eventDoc, doc, button = original.button, fromElement = original.fromElement;
                Sensor();
                if (event.pageX == null && original.clientX != null) {
                    Sensor();
                    eventDoc = event.target.ownerDocument || document;
                    doc = eventDoc.documentElement;
                    body = eventDoc.body;
                    event.pageX = original.clientX + (doc && doc.scrollLeft || body && body.scrollLeft || 0) - (doc && doc.clientLeft || body && body.clientLeft || 0);
                    event.pageY = original.clientY + (doc && doc.scrollTop || body && body.scrollTop || 0) - (doc && doc.clientTop || body && body.clientTop || 0);
                }
                Sensor();
                if (!event.relatedTarget && fromElement) {
                    Sensor();
                    event.relatedTarget = fromElement === event.target ? original.toElement : fromElement;
                }
                Sensor();
                if (!event.which && button !== undefined) {
                    Sensor();
                    event.which = button & 1 ? 1 : button & 2 ? 3 : button & 4 ? 2 : 0;
                }
                var IARETREPLACE = event;
                Sensor();
                return IARETREPLACE;
            }
        },
        special: {
            load: { noBubble: true },
            focus: {
                trigger: function () {
                    Sensor();
                    if (this !== safeActiveElement() && this.focus) {
                        Sensor();
                        try {
                            Sensor();
                            this.focus();
                            var IARETREPLACE = false;
                            Sensor();
                            return IARETREPLACE;
                        } catch (e) {
                            Sensor();
                        }
                    }
                    Sensor();
                },
                delegateType: 'focusin'
            },
            blur: {
                trigger: function () {
                    Sensor();
                    if (this === safeActiveElement() && this.blur) {
                        Sensor();
                        this.blur();
                        var IARETREPLACE = false;
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                },
                delegateType: 'focusout'
            },
            click: {
                trigger: function () {
                    Sensor();
                    if (jQuery.nodeName(this, 'input') && this.type === 'checkbox' && this.click) {
                        Sensor();
                        this.click();
                        var IARETREPLACE = false;
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                },
                _default: function (event) {
                    var IARETREPLACE = jQuery.nodeName(event.target, 'a');
                    Sensor();
                    return IARETREPLACE;
                }
            },
            beforeunload: {
                postDispatch: function (event) {
                    Sensor();
                    if (event.result !== undefined) {
                        Sensor();
                        event.originalEvent.returnValue = event.result;
                    }
                    Sensor();
                }
            }
        },
        simulate: function (type, elem, event, bubble) {
            Sensor();
            var e = jQuery.extend(new jQuery.Event(), event, {
                type: type,
                isSimulated: true,
                originalEvent: {}
            });
            Sensor();
            if (bubble) {
                Sensor();
                jQuery.event.trigger(e, null, elem);
            } else {
                Sensor();
                jQuery.event.dispatch.call(elem, e);
            }
            Sensor();
            if (e.isDefaultPrevented()) {
                Sensor();
                event.preventDefault();
            }
            Sensor();
        }
    };
    jQuery.removeEvent = document.removeEventListener ? function (elem, type, handle) {
        Sensor();
        if (elem.removeEventListener) {
            Sensor();
            elem.removeEventListener(type, handle, false);
        }
        Sensor();
    } : function (elem, type, handle) {
        Sensor();
        var name = 'on' + type;
        Sensor();
        if (elem.detachEvent) {
            Sensor();
            if (typeof elem[name] === strundefined) {
                Sensor();
                elem[name] = null;
            }
            Sensor();
            elem.detachEvent(name, handle);
        }
        Sensor();
    };
    jQuery.Event = function (src, props) {
        Sensor();
        if (!(this instanceof jQuery.Event)) {
            var IARETREPLACE = new jQuery.Event(src, props);
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        if (src && src.type) {
            Sensor();
            this.originalEvent = src;
            this.type = src.type;
            this.isDefaultPrevented = src.defaultPrevented || src.defaultPrevented === undefined && (src.returnValue === false || src.getPreventDefault && src.getPreventDefault()) ? returnTrue : returnFalse;
        } else {
            Sensor();
            this.type = src;
        }
        Sensor();
        if (props) {
            Sensor();
            jQuery.extend(this, props);
        }
        Sensor();
        this.timeStamp = src && src.timeStamp || jQuery.now();
        this[jQuery.expando] = true;
        Sensor();
    };
    jQuery.Event.prototype = {
        isDefaultPrevented: returnFalse,
        isPropagationStopped: returnFalse,
        isImmediatePropagationStopped: returnFalse,
        preventDefault: function () {
            Sensor();
            var e = this.originalEvent;
            this.isDefaultPrevented = returnTrue;
            Sensor();
            if (!e) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (e.preventDefault) {
                Sensor();
                e.preventDefault();
            } else {
                Sensor();
                e.returnValue = false;
            }
            Sensor();
        },
        stopPropagation: function () {
            Sensor();
            var e = this.originalEvent;
            this.isPropagationStopped = returnTrue;
            Sensor();
            if (!e) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (e.stopPropagation) {
                Sensor();
                e.stopPropagation();
            }
            Sensor();
            e.cancelBubble = true;
            Sensor();
        },
        stopImmediatePropagation: function () {
            Sensor();
            this.isImmediatePropagationStopped = returnTrue;
            this.stopPropagation();
            Sensor();
        }
    };
    jQuery.each({
        mouseenter: 'mouseover',
        mouseleave: 'mouseout'
    }, function (orig, fix) {
        Sensor();
        jQuery.event.special[orig] = {
            delegateType: fix,
            bindType: fix,
            handle: function (event) {
                Sensor();
                var ret, target = this, related = event.relatedTarget, handleObj = event.handleObj;
                Sensor();
                if (!related || related !== target && !jQuery.contains(target, related)) {
                    Sensor();
                    event.type = handleObj.origType;
                    ret = handleObj.handler.apply(this, arguments);
                    event.type = fix;
                }
                var IARETREPLACE = ret;
                Sensor();
                return IARETREPLACE;
            }
        };
        Sensor();
    });
    Sensor();
    if (!support.submitBubbles) {
        Sensor();
        jQuery.event.special.submit = {
            setup: function () {
                Sensor();
                if (jQuery.nodeName(this, 'form')) {
                    var IARETREPLACE = false;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
                jQuery.event.add(this, 'click._submit keypress._submit', function (e) {
                    Sensor();
                    var elem = e.target, form = jQuery.nodeName(elem, 'input') || jQuery.nodeName(elem, 'button') ? elem.form : undefined;
                    Sensor();
                    if (form && !jQuery._data(form, 'submitBubbles')) {
                        Sensor();
                        jQuery.event.add(form, 'submit._submit', function (event) {
                            Sensor();
                            event._submit_bubble = true;
                            Sensor();
                        });
                        jQuery._data(form, 'submitBubbles', true);
                    }
                    Sensor();
                });
                Sensor();
            },
            postDispatch: function (event) {
                Sensor();
                if (event._submit_bubble) {
                    Sensor();
                    delete event._submit_bubble;
                    Sensor();
                    if (this.parentNode && !event.isTrigger) {
                        Sensor();
                        jQuery.event.simulate('submit', this.parentNode, event, true);
                    }
                }
                Sensor();
            },
            teardown: function () {
                Sensor();
                if (jQuery.nodeName(this, 'form')) {
                    var IARETREPLACE = false;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
                jQuery.event.remove(this, '._submit');
                Sensor();
            }
        };
    }
    Sensor();
    if (!support.changeBubbles) {
        Sensor();
        jQuery.event.special.change = {
            setup: function () {
                Sensor();
                if (rformElems.test(this.nodeName)) {
                    Sensor();
                    if (this.type === 'checkbox' || this.type === 'radio') {
                        Sensor();
                        jQuery.event.add(this, 'propertychange._change', function (event) {
                            Sensor();
                            if (event.originalEvent.propertyName === 'checked') {
                                Sensor();
                                this._just_changed = true;
                            }
                            Sensor();
                        });
                        jQuery.event.add(this, 'click._change', function (event) {
                            Sensor();
                            if (this._just_changed && !event.isTrigger) {
                                Sensor();
                                this._just_changed = false;
                            }
                            Sensor();
                            jQuery.event.simulate('change', this, event, true);
                            Sensor();
                        });
                    }
                    var IARETREPLACE = false;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
                jQuery.event.add(this, 'beforeactivate._change', function (e) {
                    Sensor();
                    var elem = e.target;
                    Sensor();
                    if (rformElems.test(elem.nodeName) && !jQuery._data(elem, 'changeBubbles')) {
                        Sensor();
                        jQuery.event.add(elem, 'change._change', function (event) {
                            Sensor();
                            if (this.parentNode && !event.isSimulated && !event.isTrigger) {
                                Sensor();
                                jQuery.event.simulate('change', this.parentNode, event, true);
                            }
                            Sensor();
                        });
                        jQuery._data(elem, 'changeBubbles', true);
                    }
                    Sensor();
                });
                Sensor();
            },
            handle: function (event) {
                Sensor();
                var elem = event.target;
                Sensor();
                if (this !== elem || event.isSimulated || event.isTrigger || elem.type !== 'radio' && elem.type !== 'checkbox') {
                    var IARETREPLACE = event.handleObj.handler.apply(this, arguments);
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            },
            teardown: function () {
                Sensor();
                jQuery.event.remove(this, '._change');
                var IARETREPLACE = !rformElems.test(this.nodeName);
                Sensor();
                return IARETREPLACE;
            }
        };
    }
    Sensor();
    if (!support.focusinBubbles) {
        Sensor();
        jQuery.each({
            focus: 'focusin',
            blur: 'focusout'
        }, function (orig, fix) {
            Sensor();
            var handler = function (event) {
                Sensor();
                jQuery.event.simulate(fix, event.target, jQuery.event.fix(event), true);
                Sensor();
            };
            jQuery.event.special[fix] = {
                setup: function () {
                    Sensor();
                    var doc = this.ownerDocument || this, attaches = jQuery._data(doc, fix);
                    Sensor();
                    if (!attaches) {
                        Sensor();
                        doc.addEventListener(orig, handler, true);
                    }
                    Sensor();
                    jQuery._data(doc, fix, (attaches || 0) + 1);
                    Sensor();
                },
                teardown: function () {
                    Sensor();
                    var doc = this.ownerDocument || this, attaches = jQuery._data(doc, fix) - 1;
                    Sensor();
                    if (!attaches) {
                        Sensor();
                        doc.removeEventListener(orig, handler, true);
                        jQuery._removeData(doc, fix);
                    } else {
                        Sensor();
                        jQuery._data(doc, fix, attaches);
                    }
                    Sensor();
                }
            };
            Sensor();
        });
    }
    Sensor();
    jQuery.fn.extend({
        on: function (types, selector, data, fn, one) {
            Sensor();
            var type, origFn;
            Sensor();
            if (typeof types === 'object') {
                Sensor();
                if (typeof selector !== 'string') {
                    Sensor();
                    data = data || selector;
                    selector = undefined;
                }
                Sensor();
                for (type in types) {
                    Sensor();
                    this.on(type, selector, data, types[type], one);
                }
                var IARETREPLACE = this;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (data == null && fn == null) {
                Sensor();
                fn = selector;
                data = selector = undefined;
            } else {
                Sensor();
                if (fn == null) {
                    Sensor();
                    if (typeof selector === 'string') {
                        Sensor();
                        fn = data;
                        data = undefined;
                    } else {
                        Sensor();
                        fn = data;
                        data = selector;
                        selector = undefined;
                    }
                }
            }
            Sensor();
            if (fn === false) {
                Sensor();
                fn = returnFalse;
            } else {
                Sensor();
                if (!fn) {
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                }
            }
            Sensor();
            if (one === 1) {
                Sensor();
                origFn = fn;
                fn = function (event) {
                    Sensor();
                    jQuery().off(event);
                    var IARETREPLACE = origFn.apply(this, arguments);
                    Sensor();
                    return IARETREPLACE;
                };
                fn.guid = origFn.guid || (origFn.guid = jQuery.guid++);
            }
            var IARETREPLACE = this.each(function () {
                Sensor();
                jQuery.event.add(this, types, fn, data, selector);
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        one: function (types, selector, data, fn) {
            var IARETREPLACE = this.on(types, selector, data, fn, 1);
            Sensor();
            return IARETREPLACE;
        },
        off: function (types, selector, fn) {
            Sensor();
            var handleObj, type;
            Sensor();
            if (types && types.preventDefault && types.handleObj) {
                Sensor();
                handleObj = types.handleObj;
                jQuery(types.delegateTarget).off(handleObj.namespace ? handleObj.origType + '.' + handleObj.namespace : handleObj.origType, handleObj.selector, handleObj.handler);
                var IARETREPLACE = this;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (typeof types === 'object') {
                Sensor();
                for (type in types) {
                    Sensor();
                    this.off(type, selector, types[type]);
                }
                var IARETREPLACE = this;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (selector === false || typeof selector === 'function') {
                Sensor();
                fn = selector;
                selector = undefined;
            }
            Sensor();
            if (fn === false) {
                Sensor();
                fn = returnFalse;
            }
            var IARETREPLACE = this.each(function () {
                Sensor();
                jQuery.event.remove(this, types, fn, selector);
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        trigger: function (type, data) {
            var IARETREPLACE = this.each(function () {
                Sensor();
                jQuery.event.trigger(type, data, this);
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        triggerHandler: function (type, data) {
            Sensor();
            var elem = this[0];
            Sensor();
            if (elem) {
                var IARETREPLACE = jQuery.event.trigger(type, data, elem, true);
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
        }
    });
    function createSafeFragment(document) {
        Sensor();
        var list = nodeNames.split('|'), safeFrag = document.createDocumentFragment();
        Sensor();
        if (safeFrag.createElement) {
            Sensor();
            while (list.length) {
                Sensor();
                safeFrag.createElement(list.pop());
            }
        }
        var IARETREPLACE = safeFrag;
        Sensor();
        return IARETREPLACE;
    }
    var nodeNames = 'abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|' + 'header|hgroup|mark|meter|nav|output|progress|section|summary|time|video', rinlinejQuery = / jQuery\d+="(?:null|\d+)"/g, rnoshimcache = new RegExp('<(?:' + nodeNames + ')[\\s/>]', 'i'), rleadingWhitespace = /^\s+/, rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, rtagName = /<([\w:]+)/, rtbody = /<tbody/i, rhtml = /<|&#?\w+;/, rnoInnerhtml = /<(?:script|style|link)/i, rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i, rscriptType = /^$|\/(?:java|ecma)script/i, rscriptTypeMasked = /^true\/(.*)/, rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, wrapMap = {
            option: [
                1,
                '<select multiple=\'multiple\'>',
                '</select>'
            ],
            legend: [
                1,
                '<fieldset>',
                '</fieldset>'
            ],
            area: [
                1,
                '<map>',
                '</map>'
            ],
            param: [
                1,
                '<object>',
                '</object>'
            ],
            thead: [
                1,
                '<table>',
                '</table>'
            ],
            tr: [
                2,
                '<table><tbody>',
                '</tbody></table>'
            ],
            col: [
                2,
                '<table><tbody></tbody><colgroup>',
                '</colgroup></table>'
            ],
            td: [
                3,
                '<table><tbody><tr>',
                '</tr></tbody></table>'
            ],
            _default: support.htmlSerialize ? [
                0,
                '',
                ''
            ] : [
                1,
                'X<div>',
                '</div>'
            ]
        }, safeFragment = createSafeFragment(document), fragmentDiv = safeFragment.appendChild(document.createElement('div'));
    wrapMap.optgroup = wrapMap.option;
    wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
    wrapMap.th = wrapMap.td;
    function getAll(context, tag) {
        Sensor();
        var elems, elem, i = 0, found = typeof context.getElementsByTagName !== strundefined ? context.getElementsByTagName(tag || '*') : typeof context.querySelectorAll !== strundefined ? context.querySelectorAll(tag || '*') : undefined;
        Sensor();
        if (!found) {
            Sensor();
            for (found = [], elems = context.childNodes || context; (elem = elems[i]) != null; i++) {
                Sensor();
                if (!tag || jQuery.nodeName(elem, tag)) {
                    Sensor();
                    found.push(elem);
                } else {
                    Sensor();
                    jQuery.merge(found, getAll(elem, tag));
                }
            }
        }
        var IARETREPLACE = tag === undefined || tag && jQuery.nodeName(context, tag) ? jQuery.merge([context], found) : found;
        Sensor();
        return IARETREPLACE;
    }
    function fixDefaultChecked(elem) {
        Sensor();
        if (rcheckableType.test(elem.type)) {
            Sensor();
            elem.defaultChecked = elem.checked;
        }
        Sensor();
    }
    function manipulationTarget(elem, content) {
        var IARETREPLACE = jQuery.nodeName(elem, 'table') && jQuery.nodeName(content.nodeType !== 11 ? content : content.firstChild, 'tr') ? elem.getElementsByTagName('tbody')[0] || elem.appendChild(elem.ownerDocument.createElement('tbody')) : elem;
        Sensor();
        return IARETREPLACE;
    }
    function disableScript(elem) {
        Sensor();
        elem.type = (jQuery.find.attr(elem, 'type') !== null) + '/' + elem.type;
        var IARETREPLACE = elem;
        Sensor();
        return IARETREPLACE;
    }
    function restoreScript(elem) {
        Sensor();
        var match = rscriptTypeMasked.exec(elem.type);
        Sensor();
        if (match) {
            Sensor();
            elem.type = match[1];
        } else {
            Sensor();
            elem.removeAttribute('type');
        }
        var IARETREPLACE = elem;
        Sensor();
        return IARETREPLACE;
    }
    function setGlobalEval(elems, refElements) {
        Sensor();
        var elem, i = 0;
        Sensor();
        for (; (elem = elems[i]) != null; i++) {
            Sensor();
            jQuery._data(elem, 'globalEval', !refElements || jQuery._data(refElements[i], 'globalEval'));
        }
        Sensor();
    }
    function cloneCopyEvent(src, dest) {
        Sensor();
        if (dest.nodeType !== 1 || !jQuery.hasData(src)) {
            var IARETREPLACE;
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        var type, i, l, oldData = jQuery._data(src), curData = jQuery._data(dest, oldData), events = oldData.events;
        Sensor();
        if (events) {
            Sensor();
            delete curData.handle;
            curData.events = {};
            Sensor();
            for (type in events) {
                Sensor();
                for (i = 0, l = events[type].length; i < l; i++) {
                    Sensor();
                    jQuery.event.add(dest, type, events[type][i]);
                }
            }
        }
        Sensor();
        if (curData.data) {
            Sensor();
            curData.data = jQuery.extend({}, curData.data);
        }
        Sensor();
    }
    function fixCloneNodeIssues(src, dest) {
        Sensor();
        var nodeName, e, data;
        Sensor();
        if (dest.nodeType !== 1) {
            var IARETREPLACE;
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        nodeName = dest.nodeName.toLowerCase();
        Sensor();
        if (!support.noCloneEvent && dest[jQuery.expando]) {
            Sensor();
            data = jQuery._data(dest);
            Sensor();
            for (e in data.events) {
                Sensor();
                jQuery.removeEvent(dest, e, data.handle);
            }
            Sensor();
            dest.removeAttribute(jQuery.expando);
        }
        Sensor();
        if (nodeName === 'script' && dest.text !== src.text) {
            Sensor();
            disableScript(dest).text = src.text;
            restoreScript(dest);
        } else {
            Sensor();
            if (nodeName === 'object') {
                Sensor();
                if (dest.parentNode) {
                    Sensor();
                    dest.outerHTML = src.outerHTML;
                }
                Sensor();
                if (support.html5Clone && (src.innerHTML && !jQuery.trim(dest.innerHTML))) {
                    Sensor();
                    dest.innerHTML = src.innerHTML;
                }
            } else {
                Sensor();
                if (nodeName === 'input' && rcheckableType.test(src.type)) {
                    Sensor();
                    dest.defaultChecked = dest.checked = src.checked;
                    Sensor();
                    if (dest.value !== src.value) {
                        Sensor();
                        dest.value = src.value;
                    }
                } else {
                    Sensor();
                    if (nodeName === 'option') {
                        Sensor();
                        dest.defaultSelected = dest.selected = src.defaultSelected;
                    } else {
                        Sensor();
                        if (nodeName === 'input' || nodeName === 'textarea') {
                            Sensor();
                            dest.defaultValue = src.defaultValue;
                        }
                    }
                }
            }
        }
        Sensor();
    }
    jQuery.extend({
        clone: function (elem, dataAndEvents, deepDataAndEvents) {
            Sensor();
            var destElements, node, clone, i, srcElements, inPage = jQuery.contains(elem.ownerDocument, elem);
            Sensor();
            if (support.html5Clone || jQuery.isXMLDoc(elem) || !rnoshimcache.test('<' + elem.nodeName + '>')) {
                Sensor();
                clone = elem.cloneNode(true);
            } else {
                Sensor();
                fragmentDiv.innerHTML = elem.outerHTML;
                fragmentDiv.removeChild(clone = fragmentDiv.firstChild);
            }
            Sensor();
            if ((!support.noCloneEvent || !support.noCloneChecked) && (elem.nodeType === 1 || elem.nodeType === 11) && !jQuery.isXMLDoc(elem)) {
                Sensor();
                destElements = getAll(clone);
                srcElements = getAll(elem);
                Sensor();
                for (i = 0; (node = srcElements[i]) != null; ++i) {
                    Sensor();
                    if (destElements[i]) {
                        Sensor();
                        fixCloneNodeIssues(node, destElements[i]);
                    }
                }
            }
            Sensor();
            if (dataAndEvents) {
                Sensor();
                if (deepDataAndEvents) {
                    Sensor();
                    srcElements = srcElements || getAll(elem);
                    destElements = destElements || getAll(clone);
                    Sensor();
                    for (i = 0; (node = srcElements[i]) != null; i++) {
                        Sensor();
                        cloneCopyEvent(node, destElements[i]);
                    }
                } else {
                    Sensor();
                    cloneCopyEvent(elem, clone);
                }
            }
            Sensor();
            destElements = getAll(clone, 'script');
            Sensor();
            if (destElements.length > 0) {
                Sensor();
                setGlobalEval(destElements, !inPage && getAll(elem, 'script'));
            }
            Sensor();
            destElements = srcElements = node = null;
            var IARETREPLACE = clone;
            Sensor();
            return IARETREPLACE;
        },
        buildFragment: function (elems, context, scripts, selection) {
            Sensor();
            var j, elem, contains, tmp, tag, tbody, wrap, l = elems.length, safe = createSafeFragment(context), nodes = [], i = 0;
            Sensor();
            for (; i < l; i++) {
                Sensor();
                elem = elems[i];
                Sensor();
                if (elem || elem === 0) {
                    Sensor();
                    if (jQuery.type(elem) === 'object') {
                        Sensor();
                        jQuery.merge(nodes, elem.nodeType ? [elem] : elem);
                    } else {
                        Sensor();
                        if (!rhtml.test(elem)) {
                            Sensor();
                            nodes.push(context.createTextNode(elem));
                        } else {
                            Sensor();
                            tmp = tmp || safe.appendChild(context.createElement('div'));
                            tag = (rtagName.exec(elem) || [
                                '',
                                ''
                            ])[1].toLowerCase();
                            wrap = wrapMap[tag] || wrapMap._default;
                            tmp.innerHTML = wrap[1] + elem.replace(rxhtmlTag, '<$1></$2>') + wrap[2];
                            j = wrap[0];
                            Sensor();
                            while (j--) {
                                Sensor();
                                tmp = tmp.lastChild;
                            }
                            Sensor();
                            if (!support.leadingWhitespace && rleadingWhitespace.test(elem)) {
                                Sensor();
                                nodes.push(context.createTextNode(rleadingWhitespace.exec(elem)[0]));
                            }
                            Sensor();
                            if (!support.tbody) {
                                Sensor();
                                elem = tag === 'table' && !rtbody.test(elem) ? tmp.firstChild : wrap[1] === '<table>' && !rtbody.test(elem) ? tmp : 0;
                                j = elem && elem.childNodes.length;
                                Sensor();
                                while (j--) {
                                    Sensor();
                                    if (jQuery.nodeName(tbody = elem.childNodes[j], 'tbody') && !tbody.childNodes.length) {
                                        Sensor();
                                        elem.removeChild(tbody);
                                    }
                                }
                            }
                            Sensor();
                            jQuery.merge(nodes, tmp.childNodes);
                            tmp.textContent = '';
                            Sensor();
                            while (tmp.firstChild) {
                                Sensor();
                                tmp.removeChild(tmp.firstChild);
                            }
                            Sensor();
                            tmp = safe.lastChild;
                        }
                    }
                }
            }
            Sensor();
            if (tmp) {
                Sensor();
                safe.removeChild(tmp);
            }
            Sensor();
            if (!support.appendChecked) {
                Sensor();
                jQuery.grep(getAll(nodes, 'input'), fixDefaultChecked);
            }
            Sensor();
            i = 0;
            Sensor();
            while (elem = nodes[i++]) {
                Sensor();
                if (selection && jQuery.inArray(elem, selection) !== -1) {
                    Sensor();
                    continue;
                }
                Sensor();
                contains = jQuery.contains(elem.ownerDocument, elem);
                tmp = getAll(safe.appendChild(elem), 'script');
                Sensor();
                if (contains) {
                    Sensor();
                    setGlobalEval(tmp);
                }
                Sensor();
                if (scripts) {
                    Sensor();
                    j = 0;
                    Sensor();
                    while (elem = tmp[j++]) {
                        Sensor();
                        if (rscriptType.test(elem.type || '')) {
                            Sensor();
                            scripts.push(elem);
                        }
                    }
                }
            }
            Sensor();
            tmp = null;
            var IARETREPLACE = safe;
            Sensor();
            return IARETREPLACE;
        },
        cleanData: function (elems, acceptData) {
            Sensor();
            var elem, type, id, data, i = 0, internalKey = jQuery.expando, cache = jQuery.cache, deleteExpando = support.deleteExpando, special = jQuery.event.special;
            Sensor();
            for (; (elem = elems[i]) != null; i++) {
                Sensor();
                if (acceptData || jQuery.acceptData(elem)) {
                    Sensor();
                    id = elem[internalKey];
                    data = id && cache[id];
                    Sensor();
                    if (data) {
                        Sensor();
                        if (data.events) {
                            Sensor();
                            for (type in data.events) {
                                Sensor();
                                if (special[type]) {
                                    Sensor();
                                    jQuery.event.remove(elem, type);
                                } else {
                                    Sensor();
                                    jQuery.removeEvent(elem, type, data.handle);
                                }
                            }
                        }
                        Sensor();
                        if (cache[id]) {
                            Sensor();
                            delete cache[id];
                            Sensor();
                            if (deleteExpando) {
                                Sensor();
                                delete elem[internalKey];
                            } else {
                                Sensor();
                                if (typeof elem.removeAttribute !== strundefined) {
                                    Sensor();
                                    elem.removeAttribute(internalKey);
                                } else {
                                    Sensor();
                                    elem[internalKey] = null;
                                }
                            }
                            Sensor();
                            deletedIds.push(id);
                        }
                    }
                }
            }
            Sensor();
        }
    });
    jQuery.fn.extend({
        text: function (value) {
            var IARETREPLACE = access(this, function (value) {
                var IARETREPLACE = value === undefined ? jQuery.text(this) : this.empty().append((this[0] && this[0].ownerDocument || document).createTextNode(value));
                Sensor();
                return IARETREPLACE;
            }, null, value, arguments.length);
            Sensor();
            return IARETREPLACE;
        },
        append: function () {
            var IARETREPLACE = this.domManip(arguments, function (elem) {
                Sensor();
                if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
                    Sensor();
                    var target = manipulationTarget(this, elem);
                    target.appendChild(elem);
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        prepend: function () {
            var IARETREPLACE = this.domManip(arguments, function (elem) {
                Sensor();
                if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
                    Sensor();
                    var target = manipulationTarget(this, elem);
                    target.insertBefore(elem, target.firstChild);
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        before: function () {
            var IARETREPLACE = this.domManip(arguments, function (elem) {
                Sensor();
                if (this.parentNode) {
                    Sensor();
                    this.parentNode.insertBefore(elem, this);
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        after: function () {
            var IARETREPLACE = this.domManip(arguments, function (elem) {
                Sensor();
                if (this.parentNode) {
                    Sensor();
                    this.parentNode.insertBefore(elem, this.nextSibling);
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        remove: function (selector, keepData) {
            Sensor();
            var elem, elems = selector ? jQuery.filter(selector, this) : this, i = 0;
            Sensor();
            for (; (elem = elems[i]) != null; i++) {
                Sensor();
                if (!keepData && elem.nodeType === 1) {
                    Sensor();
                    jQuery.cleanData(getAll(elem));
                }
                Sensor();
                if (elem.parentNode) {
                    Sensor();
                    if (keepData && jQuery.contains(elem.ownerDocument, elem)) {
                        Sensor();
                        setGlobalEval(getAll(elem, 'script'));
                    }
                    Sensor();
                    elem.parentNode.removeChild(elem);
                }
            }
            var IARETREPLACE = this;
            Sensor();
            return IARETREPLACE;
        },
        empty: function () {
            Sensor();
            var elem, i = 0;
            Sensor();
            for (; (elem = this[i]) != null; i++) {
                Sensor();
                if (elem.nodeType === 1) {
                    Sensor();
                    jQuery.cleanData(getAll(elem, false));
                }
                Sensor();
                while (elem.firstChild) {
                    Sensor();
                    elem.removeChild(elem.firstChild);
                }
                Sensor();
                if (elem.options && jQuery.nodeName(elem, 'select')) {
                    Sensor();
                    elem.options.length = 0;
                }
            }
            var IARETREPLACE = this;
            Sensor();
            return IARETREPLACE;
        },
        clone: function (dataAndEvents, deepDataAndEvents) {
            Sensor();
            dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
            deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;
            var IARETREPLACE = this.map(function () {
                var IARETREPLACE = jQuery.clone(this, dataAndEvents, deepDataAndEvents);
                Sensor();
                return IARETREPLACE;
            });
            Sensor();
            return IARETREPLACE;
        },
        html: function (value) {
            var IARETREPLACE = access(this, function (value) {
                Sensor();
                var elem = this[0] || {}, i = 0, l = this.length;
                Sensor();
                if (value === undefined) {
                    var IARETREPLACE = elem.nodeType === 1 ? elem.innerHTML.replace(rinlinejQuery, '') : undefined;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
                if (typeof value === 'string' && !rnoInnerhtml.test(value) && (support.htmlSerialize || !rnoshimcache.test(value)) && (support.leadingWhitespace || !rleadingWhitespace.test(value)) && !wrapMap[(rtagName.exec(value) || [
                        '',
                        ''
                    ])[1].toLowerCase()]) {
                    Sensor();
                    value = value.replace(rxhtmlTag, '<$1></$2>');
                    Sensor();
                    try {
                        Sensor();
                        for (; i < l; i++) {
                            Sensor();
                            elem = this[i] || {};
                            Sensor();
                            if (elem.nodeType === 1) {
                                Sensor();
                                jQuery.cleanData(getAll(elem, false));
                                elem.innerHTML = value;
                            }
                        }
                        Sensor();
                        elem = 0;
                    } catch (e) {
                        Sensor();
                    }
                }
                Sensor();
                if (elem) {
                    Sensor();
                    this.empty().append(value);
                }
                Sensor();
            }, null, value, arguments.length);
            Sensor();
            return IARETREPLACE;
        },
        replaceWith: function () {
            Sensor();
            var arg = arguments[0];
            this.domManip(arguments, function (elem) {
                Sensor();
                arg = this.parentNode;
                jQuery.cleanData(getAll(this));
                Sensor();
                if (arg) {
                    Sensor();
                    arg.replaceChild(elem, this);
                }
                Sensor();
            });
            var IARETREPLACE = arg && (arg.length || arg.nodeType) ? this : this.remove();
            Sensor();
            return IARETREPLACE;
        },
        detach: function (selector) {
            var IARETREPLACE = this.remove(selector, true);
            Sensor();
            return IARETREPLACE;
        },
        domManip: function (args, callback) {
            Sensor();
            args = concat.apply([], args);
            var first, node, hasScripts, scripts, doc, fragment, i = 0, l = this.length, set = this, iNoClone = l - 1, value = args[0], isFunction = jQuery.isFunction(value);
            Sensor();
            if (isFunction || l > 1 && typeof value === 'string' && !support.checkClone && rchecked.test(value)) {
                var IARETREPLACE = this.each(function (index) {
                    Sensor();
                    var self = set.eq(index);
                    Sensor();
                    if (isFunction) {
                        Sensor();
                        args[0] = value.call(this, index, self.html());
                    }
                    Sensor();
                    self.domManip(args, callback);
                    Sensor();
                });
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (l) {
                Sensor();
                fragment = jQuery.buildFragment(args, this[0].ownerDocument, false, this);
                first = fragment.firstChild;
                Sensor();
                if (fragment.childNodes.length === 1) {
                    Sensor();
                    fragment = first;
                }
                Sensor();
                if (first) {
                    Sensor();
                    scripts = jQuery.map(getAll(fragment, 'script'), disableScript);
                    hasScripts = scripts.length;
                    Sensor();
                    for (; i < l; i++) {
                        Sensor();
                        node = fragment;
                        Sensor();
                        if (i !== iNoClone) {
                            Sensor();
                            node = jQuery.clone(node, true, true);
                            Sensor();
                            if (hasScripts) {
                                Sensor();
                                jQuery.merge(scripts, getAll(node, 'script'));
                            }
                        }
                        Sensor();
                        callback.call(this[i], node, i);
                    }
                    Sensor();
                    if (hasScripts) {
                        Sensor();
                        doc = scripts[scripts.length - 1].ownerDocument;
                        jQuery.map(scripts, restoreScript);
                        Sensor();
                        for (i = 0; i < hasScripts; i++) {
                            Sensor();
                            node = scripts[i];
                            Sensor();
                            if (rscriptType.test(node.type || '') && !jQuery._data(node, 'globalEval') && jQuery.contains(doc, node)) {
                                Sensor();
                                if (node.src) {
                                    Sensor();
                                    if (jQuery._evalUrl) {
                                        Sensor();
                                        jQuery._evalUrl(node.src);
                                    }
                                } else {
                                    Sensor();
                                    jQuery.globalEval((node.text || node.textContent || node.innerHTML || '').replace(rcleanScript, ''));
                                }
                            }
                        }
                    }
                    Sensor();
                    fragment = first = null;
                }
            }
            var IARETREPLACE = this;
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.each({
        appendTo: 'append',
        prependTo: 'prepend',
        insertBefore: 'before',
        insertAfter: 'after',
        replaceAll: 'replaceWith'
    }, function (name, original) {
        Sensor();
        jQuery.fn[name] = function (selector) {
            Sensor();
            var elems, i = 0, ret = [], insert = jQuery(selector), last = insert.length - 1;
            Sensor();
            for (; i <= last; i++) {
                Sensor();
                elems = i === last ? this : this.clone(true);
                jQuery(insert[i])[original](elems);
                push.apply(ret, elems.get());
            }
            var IARETREPLACE = this.pushStack(ret);
            Sensor();
            return IARETREPLACE;
        };
        Sensor();
    });
    var iframe, elemdisplay = {};
    function actualDisplay(name, doc) {
        Sensor();
        var elem = jQuery(doc.createElement(name)).appendTo(doc.body), display = window.getDefaultComputedStyle ? window.getDefaultComputedStyle(elem[0]).display : jQuery.css(elem[0], 'display');
        elem.detach();
        var IARETREPLACE = display;
        Sensor();
        return IARETREPLACE;
    }
    function defaultDisplay(nodeName) {
        Sensor();
        var doc = document, display = elemdisplay[nodeName];
        Sensor();
        if (!display) {
            Sensor();
            display = actualDisplay(nodeName, doc);
            Sensor();
            if (display === 'none' || !display) {
                Sensor();
                iframe = (iframe || jQuery('<iframe frameborder=\'0\' width=\'0\' height=\'0\'/>')).appendTo(doc.documentElement);
                doc = (iframe[0].contentWindow || iframe[0].contentDocument).document;
                doc.write();
                doc.close();
                display = actualDisplay(nodeName, doc);
                iframe.detach();
            }
            Sensor();
            elemdisplay[nodeName] = display;
        }
        var IARETREPLACE = display;
        Sensor();
        return IARETREPLACE;
    }
    (function () {
        Sensor();
        var a, shrinkWrapBlocksVal, div = document.createElement('div'), divReset = '-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;' + 'display:block;padding:0;margin:0;border:0';
        div.innerHTML = '  <link/><table></table><a href=\'/a\'>a</a><input type=\'checkbox\'/>';
        a = div.getElementsByTagName('a')[0];
        a.style.cssText = 'float:left;opacity:.5';
        support.opacity = /^0.5/.test(a.style.opacity);
        support.cssFloat = !!a.style.cssFloat;
        div.style.backgroundClip = 'content-box';
        div.cloneNode(true).style.backgroundClip = '';
        support.clearCloneStyle = div.style.backgroundClip === 'content-box';
        a = div = null;
        support.shrinkWrapBlocks = function () {
            Sensor();
            var body, container, div, containerStyles;
            Sensor();
            if (shrinkWrapBlocksVal == null) {
                Sensor();
                body = document.getElementsByTagName('body')[0];
                Sensor();
                if (!body) {
                    var IARETREPLACE;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
                containerStyles = 'border:0;width:0;height:0;position:absolute;top:0;left:-9999px';
                container = document.createElement('div');
                div = document.createElement('div');
                body.appendChild(container).appendChild(div);
                shrinkWrapBlocksVal = false;
                Sensor();
                if (typeof div.style.zoom !== strundefined) {
                    Sensor();
                    div.style.cssText = divReset + ';width:1px;padding:1px;zoom:1';
                    div.innerHTML = '<div></div>';
                    div.firstChild.style.width = '5px';
                    shrinkWrapBlocksVal = div.offsetWidth !== 3;
                }
                Sensor();
                body.removeChild(container);
                body = container = div = null;
            }
            var IARETREPLACE = shrinkWrapBlocksVal;
            Sensor();
            return IARETREPLACE;
        };
        Sensor();
    }());
    var rmargin = /^margin/;
    var rnumnonpx = new RegExp('^(' + pnum + ')(?!px)[a-z%]+$', 'i');
    var getStyles, curCSS, rposition = /^(top|right|bottom|left)$/;
    Sensor();
    if (window.getComputedStyle) {
        Sensor();
        getStyles = function (elem) {
            var IARETREPLACE = elem.ownerDocument.defaultView.getComputedStyle(elem, null);
            Sensor();
            return IARETREPLACE;
        };
        curCSS = function (elem, name, computed) {
            Sensor();
            var width, minWidth, maxWidth, ret, style = elem.style;
            computed = computed || getStyles(elem);
            ret = computed ? computed.getPropertyValue(name) || computed[name] : undefined;
            Sensor();
            if (computed) {
                Sensor();
                if (ret === '' && !jQuery.contains(elem.ownerDocument, elem)) {
                    Sensor();
                    ret = jQuery.style(elem, name);
                }
                Sensor();
                if (rnumnonpx.test(ret) && rmargin.test(name)) {
                    Sensor();
                    width = style.width;
                    minWidth = style.minWidth;
                    maxWidth = style.maxWidth;
                    style.minWidth = style.maxWidth = style.width = ret;
                    ret = computed.width;
                    style.width = width;
                    style.minWidth = minWidth;
                    style.maxWidth = maxWidth;
                }
            }
            var IARETREPLACE = ret === undefined ? ret : ret + '';
            Sensor();
            return IARETREPLACE;
        };
    } else {
        Sensor();
        if (document.documentElement.currentStyle) {
            Sensor();
            getStyles = function (elem) {
                var IARETREPLACE = elem.currentStyle;
                Sensor();
                return IARETREPLACE;
            };
            curCSS = function (elem, name, computed) {
                Sensor();
                var left, rs, rsLeft, ret, style = elem.style;
                computed = computed || getStyles(elem);
                ret = computed ? computed[name] : undefined;
                Sensor();
                if (ret == null && style && style[name]) {
                    Sensor();
                    ret = style[name];
                }
                Sensor();
                if (rnumnonpx.test(ret) && !rposition.test(name)) {
                    Sensor();
                    left = style.left;
                    rs = elem.runtimeStyle;
                    rsLeft = rs && rs.left;
                    Sensor();
                    if (rsLeft) {
                        Sensor();
                        rs.left = elem.currentStyle.left;
                    }
                    Sensor();
                    style.left = name === 'fontSize' ? '1em' : ret;
                    ret = style.pixelLeft + 'px';
                    style.left = left;
                    Sensor();
                    if (rsLeft) {
                        Sensor();
                        rs.left = rsLeft;
                    }
                }
                var IARETREPLACE = ret === undefined ? ret : ret + '' || 'auto';
                Sensor();
                return IARETREPLACE;
            };
        }
    }
    function addGetHookIf(conditionFn, hookFn) {
        var IARETREPLACE = {
            get: function () {
                Sensor();
                var condition = conditionFn();
                Sensor();
                if (condition == null) {
                    var IARETREPLACE;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
                if (condition) {
                    Sensor();
                    delete this.get;
                    var IARETREPLACE;
                    Sensor();
                    return IARETREPLACE;
                }
                var IARETREPLACE = (this.get = hookFn).apply(this, arguments);
                Sensor();
                return IARETREPLACE;
            }
        };
        Sensor();
        return IARETREPLACE;
    }
    Sensor();
    (function () {
        Sensor();
        var a, reliableHiddenOffsetsVal, boxSizingVal, boxSizingReliableVal, pixelPositionVal, reliableMarginRightVal, div = document.createElement('div'), containerStyles = 'border:0;width:0;height:0;position:absolute;top:0;left:-9999px', divReset = '-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;' + 'display:block;padding:0;margin:0;border:0';
        div.innerHTML = '  <link/><table></table><a href=\'/a\'>a</a><input type=\'checkbox\'/>';
        a = div.getElementsByTagName('a')[0];
        a.style.cssText = 'float:left;opacity:.5';
        support.opacity = /^0.5/.test(a.style.opacity);
        support.cssFloat = !!a.style.cssFloat;
        div.style.backgroundClip = 'content-box';
        div.cloneNode(true).style.backgroundClip = '';
        support.clearCloneStyle = div.style.backgroundClip === 'content-box';
        a = div = null;
        jQuery.extend(support, {
            reliableHiddenOffsets: function () {
                Sensor();
                if (reliableHiddenOffsetsVal != null) {
                    var IARETREPLACE = reliableHiddenOffsetsVal;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
                var container, tds, isSupported, div = document.createElement('div'), body = document.getElementsByTagName('body')[0];
                Sensor();
                if (!body) {
                    var IARETREPLACE;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
                div.setAttribute('className', 't');
                div.innerHTML = '  <link/><table></table><a href=\'/a\'>a</a><input type=\'checkbox\'/>';
                container = document.createElement('div');
                container.style.cssText = containerStyles;
                body.appendChild(container).appendChild(div);
                div.innerHTML = '<table><tr><td></td><td>t</td></tr></table>';
                tds = div.getElementsByTagName('td');
                tds[0].style.cssText = 'padding:0;margin:0;border:0;display:none';
                isSupported = tds[0].offsetHeight === 0;
                tds[0].style.display = '';
                tds[1].style.display = 'none';
                reliableHiddenOffsetsVal = isSupported && tds[0].offsetHeight === 0;
                body.removeChild(container);
                div = body = null;
                var IARETREPLACE = reliableHiddenOffsetsVal;
                Sensor();
                return IARETREPLACE;
            },
            boxSizing: function () {
                Sensor();
                if (boxSizingVal == null) {
                    Sensor();
                    computeStyleTests();
                }
                var IARETREPLACE = boxSizingVal;
                Sensor();
                return IARETREPLACE;
            },
            boxSizingReliable: function () {
                Sensor();
                if (boxSizingReliableVal == null) {
                    Sensor();
                    computeStyleTests();
                }
                var IARETREPLACE = boxSizingReliableVal;
                Sensor();
                return IARETREPLACE;
            },
            pixelPosition: function () {
                Sensor();
                if (pixelPositionVal == null) {
                    Sensor();
                    computeStyleTests();
                }
                var IARETREPLACE = pixelPositionVal;
                Sensor();
                return IARETREPLACE;
            },
            reliableMarginRight: function () {
                Sensor();
                var body, container, div, marginDiv;
                Sensor();
                if (reliableMarginRightVal == null && window.getComputedStyle) {
                    Sensor();
                    body = document.getElementsByTagName('body')[0];
                    Sensor();
                    if (!body) {
                        var IARETREPLACE;
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                    container = document.createElement('div');
                    div = document.createElement('div');
                    container.style.cssText = containerStyles;
                    body.appendChild(container).appendChild(div);
                    marginDiv = div.appendChild(document.createElement('div'));
                    marginDiv.style.cssText = div.style.cssText = divReset;
                    marginDiv.style.marginRight = marginDiv.style.width = '0';
                    div.style.width = '1px';
                    reliableMarginRightVal = !parseFloat((window.getComputedStyle(marginDiv, null) || {}).marginRight);
                    body.removeChild(container);
                }
                var IARETREPLACE = reliableMarginRightVal;
                Sensor();
                return IARETREPLACE;
            }
        });
        function computeStyleTests() {
            Sensor();
            var container, div, body = document.getElementsByTagName('body')[0];
            Sensor();
            if (!body) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            container = document.createElement('div');
            div = document.createElement('div');
            container.style.cssText = containerStyles;
            body.appendChild(container).appendChild(div);
            div.style.cssText = '-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;' + 'position:absolute;display:block;padding:1px;border:1px;width:4px;' + 'margin-top:1%;top:1%';
            jQuery.swap(body, body.style.zoom != null ? { zoom: 1 } : {}, function () {
                Sensor();
                boxSizingVal = div.offsetWidth === 4;
                Sensor();
            });
            boxSizingReliableVal = true;
            pixelPositionVal = false;
            reliableMarginRightVal = true;
            Sensor();
            if (window.getComputedStyle) {
                Sensor();
                pixelPositionVal = (window.getComputedStyle(div, null) || {}).top !== '1%';
                boxSizingReliableVal = (window.getComputedStyle(div, null) || { width: '4px' }).width === '4px';
            }
            Sensor();
            body.removeChild(container);
            div = body = null;
            Sensor();
        }
        Sensor();
    }());
    jQuery.swap = function (elem, options, callback, args) {
        Sensor();
        var ret, name, old = {};
        Sensor();
        for (name in options) {
            Sensor();
            old[name] = elem.style[name];
            elem.style[name] = options[name];
        }
        Sensor();
        ret = callback.apply(elem, args || []);
        Sensor();
        for (name in options) {
            Sensor();
            elem.style[name] = old[name];
        }
        var IARETREPLACE = ret;
        Sensor();
        return IARETREPLACE;
    };
    var ralpha = /alpha\([^)]*\)/i, ropacity = /opacity\s*=\s*([^)]*)/, rdisplayswap = /^(none|table(?!-c[ea]).+)/, rnumsplit = new RegExp('^(' + pnum + ')(.*)$', 'i'), rrelNum = new RegExp('^([+-])=(' + pnum + ')', 'i'), cssShow = {
            position: 'absolute',
            visibility: 'hidden',
            display: 'block'
        }, cssNormalTransform = {
            letterSpacing: 0,
            fontWeight: 400
        }, cssPrefixes = [
            'Webkit',
            'O',
            'Moz',
            'ms'
        ];
    function vendorPropName(style, name) {
        Sensor();
        if (name in style) {
            var IARETREPLACE = name;
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        var capName = name.charAt(0).toUpperCase() + name.slice(1), origName = name, i = cssPrefixes.length;
        Sensor();
        while (i--) {
            Sensor();
            name = cssPrefixes[i] + capName;
            Sensor();
            if (name in style) {
                var IARETREPLACE = name;
                Sensor();
                return IARETREPLACE;
            }
        }
        var IARETREPLACE = origName;
        Sensor();
        return IARETREPLACE;
    }
    function showHide(elements, show) {
        Sensor();
        var display, elem, hidden, values = [], index = 0, length = elements.length;
        Sensor();
        for (; index < length; index++) {
            Sensor();
            elem = elements[index];
            Sensor();
            if (!elem.style) {
                Sensor();
                continue;
            }
            Sensor();
            values[index] = jQuery._data(elem, 'olddisplay');
            display = elem.style.display;
            Sensor();
            if (show) {
                Sensor();
                if (!values[index] && display === 'none') {
                    Sensor();
                    elem.style.display = '';
                }
                Sensor();
                if (elem.style.display === '' && isHidden(elem)) {
                    Sensor();
                    values[index] = jQuery._data(elem, 'olddisplay', defaultDisplay(elem.nodeName));
                }
            } else {
                Sensor();
                if (!values[index]) {
                    Sensor();
                    hidden = isHidden(elem);
                    Sensor();
                    if (display && display !== 'none' || !hidden) {
                        Sensor();
                        jQuery._data(elem, 'olddisplay', hidden ? display : jQuery.css(elem, 'display'));
                    }
                }
            }
        }
        Sensor();
        for (index = 0; index < length; index++) {
            Sensor();
            elem = elements[index];
            Sensor();
            if (!elem.style) {
                Sensor();
                continue;
            }
            Sensor();
            if (!show || elem.style.display === 'none' || elem.style.display === '') {
                Sensor();
                elem.style.display = show ? values[index] || '' : 'none';
            }
        }
        var IARETREPLACE = elements;
        Sensor();
        return IARETREPLACE;
    }
    function setPositiveNumber(elem, value, subtract) {
        Sensor();
        var matches = rnumsplit.exec(value);
        var IARETREPLACE = matches ? Math.max(0, matches[1] - (subtract || 0)) + (matches[2] || 'px') : value;
        Sensor();
        return IARETREPLACE;
    }
    function augmentWidthOrHeight(elem, name, extra, isBorderBox, styles) {
        Sensor();
        var i = extra === (isBorderBox ? 'border' : 'content') ? 4 : name === 'width' ? 1 : 0, val = 0;
        Sensor();
        for (; i < 4; i += 2) {
            Sensor();
            if (extra === 'margin') {
                Sensor();
                val += jQuery.css(elem, extra + cssExpand[i], true, styles);
            }
            Sensor();
            if (isBorderBox) {
                Sensor();
                if (extra === 'content') {
                    Sensor();
                    val -= jQuery.css(elem, 'padding' + cssExpand[i], true, styles);
                }
                Sensor();
                if (extra !== 'margin') {
                    Sensor();
                    val -= jQuery.css(elem, 'border' + cssExpand[i] + 'Width', true, styles);
                }
            } else {
                Sensor();
                val += jQuery.css(elem, 'padding' + cssExpand[i], true, styles);
                Sensor();
                if (extra !== 'padding') {
                    Sensor();
                    val += jQuery.css(elem, 'border' + cssExpand[i] + 'Width', true, styles);
                }
            }
        }
        var IARETREPLACE = val;
        Sensor();
        return IARETREPLACE;
    }
    function getWidthOrHeight(elem, name, extra) {
        Sensor();
        var valueIsBorderBox = true, val = name === 'width' ? elem.offsetWidth : elem.offsetHeight, styles = getStyles(elem), isBorderBox = support.boxSizing() && jQuery.css(elem, 'boxSizing', false, styles) === 'border-box';
        Sensor();
        if (val <= 0 || val == null) {
            Sensor();
            val = curCSS(elem, name, styles);
            Sensor();
            if (val < 0 || val == null) {
                Sensor();
                val = elem.style[name];
            }
            Sensor();
            if (rnumnonpx.test(val)) {
                var IARETREPLACE = val;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            valueIsBorderBox = isBorderBox && (support.boxSizingReliable() || val === elem.style[name]);
            val = parseFloat(val) || 0;
        }
        var IARETREPLACE = val + augmentWidthOrHeight(elem, name, extra || (isBorderBox ? 'border' : 'content'), valueIsBorderBox, styles) + 'px';
        Sensor();
        return IARETREPLACE;
    }
    jQuery.extend({
        cssHooks: {
            opacity: {
                get: function (elem, computed) {
                    Sensor();
                    if (computed) {
                        Sensor();
                        var ret = curCSS(elem, 'opacity');
                        var IARETREPLACE = ret === '' ? '1' : ret;
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                }
            }
        },
        cssNumber: {
            'columnCount': true,
            'fillOpacity': true,
            'fontWeight': true,
            'lineHeight': true,
            'opacity': true,
            'order': true,
            'orphans': true,
            'widows': true,
            'zIndex': true,
            'zoom': true
        },
        cssProps: { 'float': support.cssFloat ? 'cssFloat' : 'styleFloat' },
        style: function (elem, name, value, extra) {
            Sensor();
            if (!elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            var ret, type, hooks, origName = jQuery.camelCase(name), style = elem.style;
            Sensor();
            name = jQuery.cssProps[origName] || (jQuery.cssProps[origName] = vendorPropName(style, origName));
            Sensor();
            hooks = jQuery.cssHooks[name] || jQuery.cssHooks[origName];
            Sensor();
            if (value !== undefined) {
                Sensor();
                type = typeof value;
                Sensor();
                if (type === 'string' && (ret = rrelNum.exec(value))) {
                    Sensor();
                    value = (ret[1] + 1) * ret[2] + parseFloat(jQuery.css(elem, name));
                    type = 'number';
                }
                Sensor();
                if (value == null || value !== value) {
                    var IARETREPLACE;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
                if (type === 'number' && !jQuery.cssNumber[origName]) {
                    Sensor();
                    value += 'px';
                }
                Sensor();
                if (!support.clearCloneStyle && value === '' && name.indexOf('background') === 0) {
                    Sensor();
                    style[name] = 'inherit';
                }
                Sensor();
                if (!hooks || !('set' in hooks) || (value = hooks.set(elem, value, extra)) !== undefined) {
                    Sensor();
                    try {
                        Sensor();
                        style[name] = '';
                        style[name] = value;
                    } catch (e) {
                        Sensor();
                    }
                }
            } else {
                Sensor();
                if (hooks && 'get' in hooks && (ret = hooks.get(elem, false, extra)) !== undefined) {
                    var IARETREPLACE = ret;
                    Sensor();
                    return IARETREPLACE;
                }
                var IARETREPLACE = style[name];
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
        },
        css: function (elem, name, extra, styles) {
            Sensor();
            var num, val, hooks, origName = jQuery.camelCase(name);
            name = jQuery.cssProps[origName] || (jQuery.cssProps[origName] = vendorPropName(elem.style, origName));
            hooks = jQuery.cssHooks[name] || jQuery.cssHooks[origName];
            Sensor();
            if (hooks && 'get' in hooks) {
                Sensor();
                val = hooks.get(elem, true, extra);
            }
            Sensor();
            if (val === undefined) {
                Sensor();
                val = curCSS(elem, name, styles);
            }
            Sensor();
            if (val === 'normal' && name in cssNormalTransform) {
                Sensor();
                val = cssNormalTransform[name];
            }
            Sensor();
            if (extra === '' || extra) {
                Sensor();
                num = parseFloat(val);
                var IARETREPLACE = extra === true || jQuery.isNumeric(num) ? num || 0 : val;
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = val;
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.each([
        'height',
        'width'
    ], function (i, name) {
        Sensor();
        jQuery.cssHooks[name] = {
            get: function (elem, computed, extra) {
                Sensor();
                if (computed) {
                    var IARETREPLACE = elem.offsetWidth === 0 && rdisplayswap.test(jQuery.css(elem, 'display')) ? jQuery.swap(elem, cssShow, function () {
                        var IARETREPLACE = getWidthOrHeight(elem, name, extra);
                        Sensor();
                        return IARETREPLACE;
                    }) : getWidthOrHeight(elem, name, extra);
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            },
            set: function (elem, value, extra) {
                Sensor();
                var styles = extra && getStyles(elem);
                var IARETREPLACE = setPositiveNumber(elem, value, extra ? augmentWidthOrHeight(elem, name, extra, support.boxSizing() && jQuery.css(elem, 'boxSizing', false, styles) === 'border-box', styles) : 0);
                Sensor();
                return IARETREPLACE;
            }
        };
        Sensor();
    });
    Sensor();
    if (!support.opacity) {
        Sensor();
        jQuery.cssHooks.opacity = {
            get: function (elem, computed) {
                var IARETREPLACE = ropacity.test((computed && elem.currentStyle ? elem.currentStyle.filter : elem.style.filter) || '') ? 0.01 * parseFloat(RegExp.$1) + '' : computed ? '1' : '';
                Sensor();
                return IARETREPLACE;
            },
            set: function (elem, value) {
                Sensor();
                var style = elem.style, currentStyle = elem.currentStyle, opacity = jQuery.isNumeric(value) ? 'alpha(opacity=' + value * 100 + ')' : '', filter = currentStyle && currentStyle.filter || style.filter || '';
                style.zoom = 1;
                Sensor();
                if ((value >= 1 || value === '') && jQuery.trim(filter.replace(ralpha, '')) === '' && style.removeAttribute) {
                    Sensor();
                    style.removeAttribute('filter');
                    Sensor();
                    if (value === '' || currentStyle && !currentStyle.filter) {
                        var IARETREPLACE;
                        Sensor();
                        return IARETREPLACE;
                    }
                }
                Sensor();
                style.filter = ralpha.test(filter) ? filter.replace(ralpha, opacity) : filter + ' ' + opacity;
                Sensor();
            }
        };
    }
    Sensor();
    jQuery.cssHooks.marginRight = addGetHookIf(support.reliableMarginRight, function (elem, computed) {
        Sensor();
        if (computed) {
            var IARETREPLACE = jQuery.swap(elem, { 'display': 'inline-block' }, curCSS, [
                elem,
                'marginRight'
            ]);
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
    });
    jQuery.each({
        margin: '',
        padding: '',
        border: 'Width'
    }, function (prefix, suffix) {
        Sensor();
        jQuery.cssHooks[prefix + suffix] = {
            expand: function (value) {
                Sensor();
                var i = 0, expanded = {}, parts = typeof value === 'string' ? value.split(' ') : [value];
                Sensor();
                for (; i < 4; i++) {
                    Sensor();
                    expanded[prefix + cssExpand[i] + suffix] = parts[i] || parts[i - 2] || parts[0];
                }
                var IARETREPLACE = expanded;
                Sensor();
                return IARETREPLACE;
            }
        };
        Sensor();
        if (!rmargin.test(prefix)) {
            Sensor();
            jQuery.cssHooks[prefix + suffix].set = setPositiveNumber;
        }
        Sensor();
    });
    jQuery.fn.extend({
        css: function (name, value) {
            var IARETREPLACE = access(this, function (elem, name, value) {
                Sensor();
                var styles, len, map = {}, i = 0;
                Sensor();
                if (jQuery.isArray(name)) {
                    Sensor();
                    styles = getStyles(elem);
                    len = name.length;
                    Sensor();
                    for (; i < len; i++) {
                        Sensor();
                        map[name[i]] = jQuery.css(elem, name[i], false, styles);
                    }
                    var IARETREPLACE = map;
                    Sensor();
                    return IARETREPLACE;
                }
                var IARETREPLACE = value !== undefined ? jQuery.style(elem, name, value) : jQuery.css(elem, name);
                Sensor();
                return IARETREPLACE;
            }, name, value, arguments.length > 1);
            Sensor();
            return IARETREPLACE;
        },
        show: function () {
            var IARETREPLACE = showHide(this, true);
            Sensor();
            return IARETREPLACE;
        },
        hide: function () {
            var IARETREPLACE = showHide(this);
            Sensor();
            return IARETREPLACE;
        },
        toggle: function (state) {
            Sensor();
            if (typeof state === 'boolean') {
                var IARETREPLACE = state ? this.show() : this.hide();
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = this.each(function () {
                Sensor();
                if (isHidden(this)) {
                    Sensor();
                    jQuery(this).show();
                } else {
                    Sensor();
                    jQuery(this).hide();
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        }
    });
    function Tween(elem, options, prop, end, easing) {
        var IARETREPLACE = new Tween.prototype.init(elem, options, prop, end, easing);
        Sensor();
        return IARETREPLACE;
    }
    jQuery.Tween = Tween;
    Tween.prototype = {
        constructor: Tween,
        init: function (elem, options, prop, end, easing, unit) {
            Sensor();
            this.elem = elem;
            this.prop = prop;
            this.easing = easing || 'swing';
            this.options = options;
            this.start = this.now = this.cur();
            this.end = end;
            this.unit = unit || (jQuery.cssNumber[prop] ? '' : 'px');
            Sensor();
        },
        cur: function () {
            Sensor();
            var hooks = Tween.propHooks[this.prop];
            var IARETREPLACE = hooks && hooks.get ? hooks.get(this) : Tween.propHooks._default.get(this);
            Sensor();
            return IARETREPLACE;
        },
        run: function (percent) {
            Sensor();
            var eased, hooks = Tween.propHooks[this.prop];
            Sensor();
            if (this.options.duration) {
                Sensor();
                this.pos = eased = jQuery.easing[this.easing](percent, this.options.duration * percent, 0, 1, this.options.duration);
            } else {
                Sensor();
                this.pos = eased = percent;
            }
            Sensor();
            this.now = (this.end - this.start) * eased + this.start;
            Sensor();
            if (this.options.step) {
                Sensor();
                this.options.step.call(this.elem, this.now, this);
            }
            Sensor();
            if (hooks && hooks.set) {
                Sensor();
                hooks.set(this);
            } else {
                Sensor();
                Tween.propHooks._default.set(this);
            }
            var IARETREPLACE = this;
            Sensor();
            return IARETREPLACE;
        }
    };
    Tween.prototype.init.prototype = Tween.prototype;
    Tween.propHooks = {
        _default: {
            get: function (tween) {
                Sensor();
                var result;
                Sensor();
                if (tween.elem[tween.prop] != null && (!tween.elem.style || tween.elem.style[tween.prop] == null)) {
                    var IARETREPLACE = tween.elem[tween.prop];
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
                result = jQuery.css(tween.elem, tween.prop, '');
                var IARETREPLACE = !result || result === 'auto' ? 0 : result;
                Sensor();
                return IARETREPLACE;
            },
            set: function (tween) {
                Sensor();
                if (jQuery.fx.step[tween.prop]) {
                    Sensor();
                    jQuery.fx.step[tween.prop](tween);
                } else {
                    Sensor();
                    if (tween.elem.style && (tween.elem.style[jQuery.cssProps[tween.prop]] != null || jQuery.cssHooks[tween.prop])) {
                        Sensor();
                        jQuery.style(tween.elem, tween.prop, tween.now + tween.unit);
                    } else {
                        Sensor();
                        tween.elem[tween.prop] = tween.now;
                    }
                }
                Sensor();
            }
        }
    };
    Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
        set: function (tween) {
            Sensor();
            if (tween.elem.nodeType && tween.elem.parentNode) {
                Sensor();
                tween.elem[tween.prop] = tween.now;
            }
            Sensor();
        }
    };
    jQuery.easing = {
        linear: function (p) {
            var IARETREPLACE = p;
            Sensor();
            return IARETREPLACE;
        },
        swing: function (p) {
            var IARETREPLACE = 0.5 - Math.cos(p * Math.PI) / 2;
            Sensor();
            return IARETREPLACE;
        }
    };
    jQuery.fx = Tween.prototype.init;
    jQuery.fx.step = {};
    var fxNow, timerId, rfxtypes = /^(?:toggle|show|hide)$/, rfxnum = new RegExp('^(?:([+-])=|)(' + pnum + ')([a-z%]*)$', 'i'), rrun = /queueHooks$/, animationPrefilters = [defaultPrefilter], tweeners = {
            '*': [function (prop, value) {
                    Sensor();
                    var tween = this.createTween(prop, value), target = tween.cur(), parts = rfxnum.exec(value), unit = parts && parts[3] || (jQuery.cssNumber[prop] ? '' : 'px'), start = (jQuery.cssNumber[prop] || unit !== 'px' && +target) && rfxnum.exec(jQuery.css(tween.elem, prop)), scale = 1, maxIterations = 20;
                    Sensor();
                    if (start && start[3] !== unit) {
                        Sensor();
                        unit = unit || start[3];
                        parts = parts || [];
                        start = +target || 1;
                        Sensor();
                        do {
                            Sensor();
                            scale = scale || '.5';
                            start = start / scale;
                            jQuery.style(tween.elem, prop, start + unit);
                        } while (scale !== (scale = tween.cur() / target) && scale !== 1 && --maxIterations);
                    }
                    Sensor();
                    if (parts) {
                        Sensor();
                        start = tween.start = +start || +target || 0;
                        tween.unit = unit;
                        tween.end = parts[1] ? start + (parts[1] + 1) * parts[2] : +parts[2];
                    }
                    var IARETREPLACE = tween;
                    Sensor();
                    return IARETREPLACE;
                }]
        };
    function createFxNow() {
        Sensor();
        setTimeout(function () {
            Sensor();
            fxNow = undefined;
            Sensor();
        });
        var IARETREPLACE = fxNow = jQuery.now();
        Sensor();
        return IARETREPLACE;
    }
    function genFx(type, includeWidth) {
        Sensor();
        var which, attrs = { height: type }, i = 0;
        includeWidth = includeWidth ? 1 : 0;
        Sensor();
        for (; i < 4; i += 2 - includeWidth) {
            Sensor();
            which = cssExpand[i];
            attrs['margin' + which] = attrs['padding' + which] = type;
        }
        Sensor();
        if (includeWidth) {
            Sensor();
            attrs.opacity = attrs.width = type;
        }
        var IARETREPLACE = attrs;
        Sensor();
        return IARETREPLACE;
    }
    function createTween(value, prop, animation) {
        Sensor();
        var tween, collection = (tweeners[prop] || []).concat(tweeners['*']), index = 0, length = collection.length;
        Sensor();
        for (; index < length; index++) {
            Sensor();
            if (tween = collection[index].call(animation, prop, value)) {
                var IARETREPLACE = tween;
                Sensor();
                return IARETREPLACE;
            }
        }
        Sensor();
    }
    function defaultPrefilter(elem, props, opts) {
        Sensor();
        var prop, value, toggle, tween, hooks, oldfire, display, dDisplay, anim = this, orig = {}, style = elem.style, hidden = elem.nodeType && isHidden(elem), dataShow = jQuery._data(elem, 'fxshow');
        Sensor();
        if (!opts.queue) {
            Sensor();
            hooks = jQuery._queueHooks(elem, 'fx');
            Sensor();
            if (hooks.unqueued == null) {
                Sensor();
                hooks.unqueued = 0;
                oldfire = hooks.empty.fire;
                hooks.empty.fire = function () {
                    Sensor();
                    if (!hooks.unqueued) {
                        Sensor();
                        oldfire();
                    }
                    Sensor();
                };
            }
            Sensor();
            hooks.unqueued++;
            anim.always(function () {
                Sensor();
                anim.always(function () {
                    Sensor();
                    hooks.unqueued--;
                    Sensor();
                    if (!jQuery.queue(elem, 'fx').length) {
                        Sensor();
                        hooks.empty.fire();
                    }
                    Sensor();
                });
                Sensor();
            });
        }
        Sensor();
        if (elem.nodeType === 1 && ('height' in props || 'width' in props)) {
            Sensor();
            opts.overflow = [
                style.overflow,
                style.overflowX,
                style.overflowY
            ];
            display = jQuery.css(elem, 'display');
            dDisplay = defaultDisplay(elem.nodeName);
            Sensor();
            if (display === 'none') {
                Sensor();
                display = dDisplay;
            }
            Sensor();
            if (display === 'inline' && jQuery.css(elem, 'float') === 'none') {
                Sensor();
                if (!support.inlineBlockNeedsLayout || dDisplay === 'inline') {
                    Sensor();
                    style.display = 'inline-block';
                } else {
                    Sensor();
                    style.zoom = 1;
                }
            }
        }
        Sensor();
        if (opts.overflow) {
            Sensor();
            style.overflow = 'hidden';
            Sensor();
            if (!support.shrinkWrapBlocks()) {
                Sensor();
                anim.always(function () {
                    Sensor();
                    style.overflow = opts.overflow[0];
                    style.overflowX = opts.overflow[1];
                    style.overflowY = opts.overflow[2];
                    Sensor();
                });
            }
        }
        Sensor();
        for (prop in props) {
            Sensor();
            value = props[prop];
            Sensor();
            if (rfxtypes.exec(value)) {
                Sensor();
                delete props[prop];
                toggle = toggle || value === 'toggle';
                Sensor();
                if (value === (hidden ? 'hide' : 'show')) {
                    Sensor();
                    if (value === 'show' && dataShow && dataShow[prop] !== undefined) {
                        Sensor();
                        hidden = true;
                    } else {
                        Sensor();
                        continue;
                    }
                }
                Sensor();
                orig[prop] = dataShow && dataShow[prop] || jQuery.style(elem, prop);
            }
        }
        Sensor();
        if (!jQuery.isEmptyObject(orig)) {
            Sensor();
            if (dataShow) {
                Sensor();
                if ('hidden' in dataShow) {
                    Sensor();
                    hidden = dataShow.hidden;
                }
            } else {
                Sensor();
                dataShow = jQuery._data(elem, 'fxshow', {});
            }
            Sensor();
            if (toggle) {
                Sensor();
                dataShow.hidden = !hidden;
            }
            Sensor();
            if (hidden) {
                Sensor();
                jQuery(elem).show();
            } else {
                Sensor();
                anim.done(function () {
                    Sensor();
                    jQuery(elem).hide();
                    Sensor();
                });
            }
            Sensor();
            anim.done(function () {
                Sensor();
                var prop;
                jQuery._removeData(elem, 'fxshow');
                Sensor();
                for (prop in orig) {
                    Sensor();
                    jQuery.style(elem, prop, orig[prop]);
                }
                Sensor();
            });
            Sensor();
            for (prop in orig) {
                Sensor();
                tween = createTween(hidden ? dataShow[prop] : 0, prop, anim);
                Sensor();
                if (!(prop in dataShow)) {
                    Sensor();
                    dataShow[prop] = tween.start;
                    Sensor();
                    if (hidden) {
                        Sensor();
                        tween.end = tween.start;
                        tween.start = prop === 'width' || prop === 'height' ? 1 : 0;
                    }
                }
            }
        }
        Sensor();
    }
    function propFilter(props, specialEasing) {
        Sensor();
        var index, name, easing, value, hooks;
        Sensor();
        for (index in props) {
            Sensor();
            name = jQuery.camelCase(index);
            easing = specialEasing[name];
            value = props[index];
            Sensor();
            if (jQuery.isArray(value)) {
                Sensor();
                easing = value[1];
                value = props[index] = value[0];
            }
            Sensor();
            if (index !== name) {
                Sensor();
                props[name] = value;
                delete props[index];
            }
            Sensor();
            hooks = jQuery.cssHooks[name];
            Sensor();
            if (hooks && 'expand' in hooks) {
                Sensor();
                value = hooks.expand(value);
                delete props[name];
                Sensor();
                for (index in value) {
                    Sensor();
                    if (!(index in props)) {
                        Sensor();
                        props[index] = value[index];
                        specialEasing[index] = easing;
                    }
                }
            } else {
                Sensor();
                specialEasing[name] = easing;
            }
        }
        Sensor();
    }
    function Animation(elem, properties, options) {
        Sensor();
        var result, stopped, index = 0, length = animationPrefilters.length, deferred = jQuery.Deferred().always(function () {
                Sensor();
                delete tick.elem;
                Sensor();
            }), tick = function () {
                Sensor();
                if (stopped) {
                    var IARETREPLACE = false;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
                var currentTime = fxNow || createFxNow(), remaining = Math.max(0, animation.startTime + animation.duration - currentTime), temp = remaining / animation.duration || 0, percent = 1 - temp, index = 0, length = animation.tweens.length;
                Sensor();
                for (; index < length; index++) {
                    Sensor();
                    animation.tweens[index].run(percent);
                }
                Sensor();
                deferred.notifyWith(elem, [
                    animation,
                    percent,
                    remaining
                ]);
                Sensor();
                if (percent < 1 && length) {
                    var IARETREPLACE = remaining;
                    Sensor();
                    return IARETREPLACE;
                } else {
                    Sensor();
                    deferred.resolveWith(elem, [animation]);
                    var IARETREPLACE = false;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            }, animation = deferred.promise({
                elem: elem,
                props: jQuery.extend({}, properties),
                opts: jQuery.extend(true, { specialEasing: {} }, options),
                originalProperties: properties,
                originalOptions: options,
                startTime: fxNow || createFxNow(),
                duration: options.duration,
                tweens: [],
                createTween: function (prop, end) {
                    Sensor();
                    var tween = jQuery.Tween(elem, animation.opts, prop, end, animation.opts.specialEasing[prop] || animation.opts.easing);
                    animation.tweens.push(tween);
                    var IARETREPLACE = tween;
                    Sensor();
                    return IARETREPLACE;
                },
                stop: function (gotoEnd) {
                    Sensor();
                    var index = 0, length = gotoEnd ? animation.tweens.length : 0;
                    Sensor();
                    if (stopped) {
                        var IARETREPLACE = this;
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                    stopped = true;
                    Sensor();
                    for (; index < length; index++) {
                        Sensor();
                        animation.tweens[index].run(1);
                    }
                    Sensor();
                    if (gotoEnd) {
                        Sensor();
                        deferred.resolveWith(elem, [
                            animation,
                            gotoEnd
                        ]);
                    } else {
                        Sensor();
                        deferred.rejectWith(elem, [
                            animation,
                            gotoEnd
                        ]);
                    }
                    var IARETREPLACE = this;
                    Sensor();
                    return IARETREPLACE;
                }
            }), props = animation.props;
        propFilter(props, animation.opts.specialEasing);
        Sensor();
        for (; index < length; index++) {
            Sensor();
            result = animationPrefilters[index].call(animation, elem, props, animation.opts);
            Sensor();
            if (result) {
                var IARETREPLACE = result;
                Sensor();
                return IARETREPLACE;
            }
        }
        Sensor();
        jQuery.map(props, createTween, animation);
        Sensor();
        if (jQuery.isFunction(animation.opts.start)) {
            Sensor();
            animation.opts.start.call(elem, animation);
        }
        Sensor();
        jQuery.fx.timer(jQuery.extend(tick, {
            elem: elem,
            anim: animation,
            queue: animation.opts.queue
        }));
        var IARETREPLACE = animation.progress(animation.opts.progress).done(animation.opts.done, animation.opts.complete).fail(animation.opts.fail).always(animation.opts.always);
        Sensor();
        return IARETREPLACE;
    }
    jQuery.Animation = jQuery.extend(Animation, {
        tweener: function (props, callback) {
            Sensor();
            if (jQuery.isFunction(props)) {
                Sensor();
                callback = props;
                props = ['*'];
            } else {
                Sensor();
                props = props.split(' ');
            }
            Sensor();
            var prop, index = 0, length = props.length;
            Sensor();
            for (; index < length; index++) {
                Sensor();
                prop = props[index];
                tweeners[prop] = tweeners[prop] || [];
                tweeners[prop].unshift(callback);
            }
            Sensor();
        },
        prefilter: function (callback, prepend) {
            Sensor();
            if (prepend) {
                Sensor();
                animationPrefilters.unshift(callback);
            } else {
                Sensor();
                animationPrefilters.push(callback);
            }
            Sensor();
        }
    });
    jQuery.speed = function (speed, easing, fn) {
        Sensor();
        var opt = speed && typeof speed === 'object' ? jQuery.extend({}, speed) : {
            complete: fn || !fn && easing || jQuery.isFunction(speed) && speed,
            duration: speed,
            easing: fn && easing || easing && !jQuery.isFunction(easing) && easing
        };
        opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === 'number' ? opt.duration : opt.duration in jQuery.fx.speeds ? jQuery.fx.speeds[opt.duration] : jQuery.fx.speeds._default;
        Sensor();
        if (opt.queue == null || opt.queue === true) {
            Sensor();
            opt.queue = 'fx';
        }
        Sensor();
        opt.old = opt.complete;
        opt.complete = function () {
            Sensor();
            if (jQuery.isFunction(opt.old)) {
                Sensor();
                opt.old.call(this);
            }
            Sensor();
            if (opt.queue) {
                Sensor();
                jQuery.dequeue(this, opt.queue);
            }
            Sensor();
        };
        var IARETREPLACE = opt;
        Sensor();
        return IARETREPLACE;
    };
    jQuery.fn.extend({
        fadeTo: function (speed, to, easing, callback) {
            var IARETREPLACE = this.filter(isHidden).css('opacity', 0).show().end().animate({ opacity: to }, speed, easing, callback);
            Sensor();
            return IARETREPLACE;
        },
        animate: function (prop, speed, easing, callback) {
            Sensor();
            var empty = jQuery.isEmptyObject(prop), optall = jQuery.speed(speed, easing, callback), doAnimation = function () {
                    Sensor();
                    var anim = Animation(this, jQuery.extend({}, prop), optall);
                    Sensor();
                    if (empty || jQuery._data(this, 'finish')) {
                        Sensor();
                        anim.stop(true);
                    }
                    Sensor();
                };
            doAnimation.finish = doAnimation;
            var IARETREPLACE = empty || optall.queue === false ? this.each(doAnimation) : this.queue(optall.queue, doAnimation);
            Sensor();
            return IARETREPLACE;
        },
        stop: function (type, clearQueue, gotoEnd) {
            Sensor();
            var stopQueue = function (hooks) {
                Sensor();
                var stop = hooks.stop;
                delete hooks.stop;
                stop(gotoEnd);
                Sensor();
            };
            Sensor();
            if (typeof type !== 'string') {
                Sensor();
                gotoEnd = clearQueue;
                clearQueue = type;
                type = undefined;
            }
            Sensor();
            if (clearQueue && type !== false) {
                Sensor();
                this.queue(type || 'fx', []);
            }
            var IARETREPLACE = this.each(function () {
                Sensor();
                var dequeue = true, index = type != null && type + 'queueHooks', timers = jQuery.timers, data = jQuery._data(this);
                Sensor();
                if (index) {
                    Sensor();
                    if (data[index] && data[index].stop) {
                        Sensor();
                        stopQueue(data[index]);
                    }
                } else {
                    Sensor();
                    for (index in data) {
                        Sensor();
                        if (data[index] && data[index].stop && rrun.test(index)) {
                            Sensor();
                            stopQueue(data[index]);
                        }
                    }
                }
                Sensor();
                for (index = timers.length; index--;) {
                    Sensor();
                    if (timers[index].elem === this && (type == null || timers[index].queue === type)) {
                        Sensor();
                        timers[index].anim.stop(gotoEnd);
                        dequeue = false;
                        timers.splice(index, 1);
                    }
                }
                Sensor();
                if (dequeue || !gotoEnd) {
                    Sensor();
                    jQuery.dequeue(this, type);
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        finish: function (type) {
            Sensor();
            if (type !== false) {
                Sensor();
                type = type || 'fx';
            }
            var IARETREPLACE = this.each(function () {
                Sensor();
                var index, data = jQuery._data(this), queue = data[type + 'queue'], hooks = data[type + 'queueHooks'], timers = jQuery.timers, length = queue ? queue.length : 0;
                data.finish = true;
                jQuery.queue(this, type, []);
                Sensor();
                if (hooks && hooks.stop) {
                    Sensor();
                    hooks.stop.call(this, true);
                }
                Sensor();
                for (index = timers.length; index--;) {
                    Sensor();
                    if (timers[index].elem === this && timers[index].queue === type) {
                        Sensor();
                        timers[index].anim.stop(true);
                        timers.splice(index, 1);
                    }
                }
                Sensor();
                for (index = 0; index < length; index++) {
                    Sensor();
                    if (queue[index] && queue[index].finish) {
                        Sensor();
                        queue[index].finish.call(this);
                    }
                }
                Sensor();
                delete data.finish;
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.each([
        'toggle',
        'show',
        'hide'
    ], function (i, name) {
        Sensor();
        var cssFn = jQuery.fn[name];
        jQuery.fn[name] = function (speed, easing, callback) {
            var IARETREPLACE = speed == null || typeof speed === 'boolean' ? cssFn.apply(this, arguments) : this.animate(genFx(name, true), speed, easing, callback);
            Sensor();
            return IARETREPLACE;
        };
        Sensor();
    });
    jQuery.each({
        slideDown: genFx('show'),
        slideUp: genFx('hide'),
        slideToggle: genFx('toggle'),
        fadeIn: { opacity: 'show' },
        fadeOut: { opacity: 'hide' },
        fadeToggle: { opacity: 'toggle' }
    }, function (name, props) {
        Sensor();
        jQuery.fn[name] = function (speed, easing, callback) {
            var IARETREPLACE = this.animate(props, speed, easing, callback);
            Sensor();
            return IARETREPLACE;
        };
        Sensor();
    });
    jQuery.timers = [];
    jQuery.fx.tick = function () {
        Sensor();
        var timer, timers = jQuery.timers, i = 0;
        fxNow = jQuery.now();
        Sensor();
        for (; i < timers.length; i++) {
            Sensor();
            timer = timers[i];
            Sensor();
            if (!timer() && timers[i] === timer) {
                Sensor();
                timers.splice(i--, 1);
            }
        }
        Sensor();
        if (!timers.length) {
            Sensor();
            jQuery.fx.stop();
        }
        Sensor();
        fxNow = undefined;
        Sensor();
    };
    jQuery.fx.timer = function (timer) {
        Sensor();
        jQuery.timers.push(timer);
        Sensor();
        if (timer()) {
            Sensor();
            jQuery.fx.start();
        } else {
            Sensor();
            jQuery.timers.pop();
        }
        Sensor();
    };
    jQuery.fx.interval = 13;
    jQuery.fx.start = function () {
        Sensor();
        if (!timerId) {
            Sensor();
            timerId = setInterval(jQuery.fx.tick, jQuery.fx.interval);
        }
        Sensor();
    };
    jQuery.fx.stop = function () {
        Sensor();
        clearInterval(timerId);
        timerId = null;
        Sensor();
    };
    jQuery.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    };
    jQuery.fn.delay = function (time, type) {
        Sensor();
        time = jQuery.fx ? jQuery.fx.speeds[time] || time : time;
        type = type || 'fx';
        var IARETREPLACE = this.queue(type, function (next, hooks) {
            Sensor();
            var timeout = setTimeout(next, time);
            hooks.stop = function () {
                Sensor();
                clearTimeout(timeout);
                Sensor();
            };
            Sensor();
        });
        Sensor();
        return IARETREPLACE;
    };
    (function () {
        Sensor();
        var a, input, select, opt, div = document.createElement('div');
        div.setAttribute('className', 't');
        div.innerHTML = '  <link/><table></table><a href=\'/a\'>a</a><input type=\'checkbox\'/>';
        a = div.getElementsByTagName('a')[0];
        select = document.createElement('select');
        opt = select.appendChild(document.createElement('option'));
        input = div.getElementsByTagName('input')[0];
        a.style.cssText = 'top:1px';
        support.getSetAttribute = div.className !== 't';
        support.style = /top/.test(a.getAttribute('style'));
        support.hrefNormalized = a.getAttribute('href') === '/a';
        support.checkOn = !!input.value;
        support.optSelected = opt.selected;
        support.enctype = !!document.createElement('form').enctype;
        select.disabled = true;
        support.optDisabled = !opt.disabled;
        input = document.createElement('input');
        input.setAttribute('value', '');
        support.input = input.getAttribute('value') === '';
        input.value = 't';
        input.setAttribute('type', 'radio');
        support.radioValue = input.value === 't';
        a = input = select = opt = div = null;
        Sensor();
    }());
    var rreturn = /\r/g;
    jQuery.fn.extend({
        val: function (value) {
            Sensor();
            var hooks, ret, isFunction, elem = this[0];
            Sensor();
            if (!arguments.length) {
                Sensor();
                if (elem) {
                    Sensor();
                    hooks = jQuery.valHooks[elem.type] || jQuery.valHooks[elem.nodeName.toLowerCase()];
                    Sensor();
                    if (hooks && 'get' in hooks && (ret = hooks.get(elem, 'value')) !== undefined) {
                        var IARETREPLACE = ret;
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                    ret = elem.value;
                    var IARETREPLACE = typeof ret === 'string' ? ret.replace(rreturn, '') : ret == null ? '' : ret;
                    Sensor();
                    return IARETREPLACE;
                }
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            isFunction = jQuery.isFunction(value);
            var IARETREPLACE = this.each(function (i) {
                Sensor();
                var val;
                Sensor();
                if (this.nodeType !== 1) {
                    var IARETREPLACE;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
                if (isFunction) {
                    Sensor();
                    val = value.call(this, i, jQuery(this).val());
                } else {
                    Sensor();
                    val = value;
                }
                Sensor();
                if (val == null) {
                    Sensor();
                    val = '';
                } else {
                    Sensor();
                    if (typeof val === 'number') {
                        Sensor();
                        val += '';
                    } else {
                        Sensor();
                        if (jQuery.isArray(val)) {
                            Sensor();
                            val = jQuery.map(val, function (value) {
                                var IARETREPLACE = value == null ? '' : value + '';
                                Sensor();
                                return IARETREPLACE;
                            });
                        }
                    }
                }
                Sensor();
                hooks = jQuery.valHooks[this.type] || jQuery.valHooks[this.nodeName.toLowerCase()];
                Sensor();
                if (!hooks || !('set' in hooks) || hooks.set(this, val, 'value') === undefined) {
                    Sensor();
                    this.value = val;
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.extend({
        valHooks: {
            option: {
                get: function (elem) {
                    Sensor();
                    var val = jQuery.find.attr(elem, 'value');
                    var IARETREPLACE = val != null ? val : jQuery.text(elem);
                    Sensor();
                    return IARETREPLACE;
                }
            },
            select: {
                get: function (elem) {
                    Sensor();
                    var value, option, options = elem.options, index = elem.selectedIndex, one = elem.type === 'select-one' || index < 0, values = one ? null : [], max = one ? index + 1 : options.length, i = index < 0 ? max : one ? index : 0;
                    Sensor();
                    for (; i < max; i++) {
                        Sensor();
                        option = options[i];
                        Sensor();
                        if ((option.selected || i === index) && (support.optDisabled ? !option.disabled : option.getAttribute('disabled') === null) && (!option.parentNode.disabled || !jQuery.nodeName(option.parentNode, 'optgroup'))) {
                            Sensor();
                            value = jQuery(option).val();
                            Sensor();
                            if (one) {
                                var IARETREPLACE = value;
                                Sensor();
                                return IARETREPLACE;
                            }
                            Sensor();
                            values.push(value);
                        }
                    }
                    var IARETREPLACE = values;
                    Sensor();
                    return IARETREPLACE;
                },
                set: function (elem, value) {
                    Sensor();
                    var optionSet, option, options = elem.options, values = jQuery.makeArray(value), i = options.length;
                    Sensor();
                    while (i--) {
                        Sensor();
                        option = options[i];
                        Sensor();
                        if (jQuery.inArray(jQuery.valHooks.option.get(option), values) >= 0) {
                            Sensor();
                            try {
                                Sensor();
                                option.selected = optionSet = true;
                            } catch (_) {
                                Sensor();
                                option.scrollHeight;
                            }
                        } else {
                            Sensor();
                            option.selected = false;
                        }
                    }
                    Sensor();
                    if (!optionSet) {
                        Sensor();
                        elem.selectedIndex = -1;
                    }
                    var IARETREPLACE = options;
                    Sensor();
                    return IARETREPLACE;
                }
            }
        }
    });
    jQuery.each([
        'radio',
        'checkbox'
    ], function () {
        Sensor();
        jQuery.valHooks[this] = {
            set: function (elem, value) {
                Sensor();
                if (jQuery.isArray(value)) {
                    var IARETREPLACE = elem.checked = jQuery.inArray(jQuery(elem).val(), value) >= 0;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            }
        };
        Sensor();
        if (!support.checkOn) {
            Sensor();
            jQuery.valHooks[this].get = function (elem) {
                var IARETREPLACE = elem.getAttribute('value') === null ? 'on' : elem.value;
                Sensor();
                return IARETREPLACE;
            };
        }
        Sensor();
    });
    var nodeHook, boolHook, attrHandle = jQuery.expr.attrHandle, ruseDefault = /^(?:checked|selected)$/i, getSetAttribute = support.getSetAttribute, getSetInput = support.input;
    jQuery.fn.extend({
        attr: function (name, value) {
            var IARETREPLACE = access(this, jQuery.attr, name, value, arguments.length > 1);
            Sensor();
            return IARETREPLACE;
        },
        removeAttr: function (name) {
            var IARETREPLACE = this.each(function () {
                Sensor();
                jQuery.removeAttr(this, name);
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.extend({
        attr: function (elem, name, value) {
            Sensor();
            var hooks, ret, nType = elem.nodeType;
            Sensor();
            if (!elem || nType === 3 || nType === 8 || nType === 2) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (typeof elem.getAttribute === strundefined) {
                var IARETREPLACE = jQuery.prop(elem, name, value);
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (nType !== 1 || !jQuery.isXMLDoc(elem)) {
                Sensor();
                name = name.toLowerCase();
                hooks = jQuery.attrHooks[name] || (jQuery.expr.match.bool.test(name) ? boolHook : nodeHook);
            }
            Sensor();
            if (value !== undefined) {
                Sensor();
                if (value === null) {
                    Sensor();
                    jQuery.removeAttr(elem, name);
                } else {
                    Sensor();
                    if (hooks && 'set' in hooks && (ret = hooks.set(elem, value, name)) !== undefined) {
                        var IARETREPLACE = ret;
                        Sensor();
                        return IARETREPLACE;
                    } else {
                        Sensor();
                        elem.setAttribute(name, value + '');
                        var IARETREPLACE = value;
                        Sensor();
                        return IARETREPLACE;
                    }
                }
            } else {
                Sensor();
                if (hooks && 'get' in hooks && (ret = hooks.get(elem, name)) !== null) {
                    var IARETREPLACE = ret;
                    Sensor();
                    return IARETREPLACE;
                } else {
                    Sensor();
                    ret = jQuery.find.attr(elem, name);
                    var IARETREPLACE = ret == null ? undefined : ret;
                    Sensor();
                    return IARETREPLACE;
                }
            }
            Sensor();
        },
        removeAttr: function (elem, value) {
            Sensor();
            var name, propName, i = 0, attrNames = value && value.match(rnotwhite);
            Sensor();
            if (attrNames && elem.nodeType === 1) {
                Sensor();
                while (name = attrNames[i++]) {
                    Sensor();
                    propName = jQuery.propFix[name] || name;
                    Sensor();
                    if (jQuery.expr.match.bool.test(name)) {
                        Sensor();
                        if (getSetInput && getSetAttribute || !ruseDefault.test(name)) {
                            Sensor();
                            elem[propName] = false;
                        } else {
                            Sensor();
                            elem[jQuery.camelCase('default-' + name)] = elem[propName] = false;
                        }
                    } else {
                        Sensor();
                        jQuery.attr(elem, name, '');
                    }
                    Sensor();
                    elem.removeAttribute(getSetAttribute ? name : propName);
                }
            }
            Sensor();
        },
        attrHooks: {
            type: {
                set: function (elem, value) {
                    Sensor();
                    if (!support.radioValue && value === 'radio' && jQuery.nodeName(elem, 'input')) {
                        Sensor();
                        var val = elem.value;
                        elem.setAttribute('type', value);
                        Sensor();
                        if (val) {
                            Sensor();
                            elem.value = val;
                        }
                        var IARETREPLACE = value;
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                }
            }
        }
    });
    boolHook = {
        set: function (elem, value, name) {
            Sensor();
            if (value === false) {
                Sensor();
                jQuery.removeAttr(elem, name);
            } else {
                Sensor();
                if (getSetInput && getSetAttribute || !ruseDefault.test(name)) {
                    Sensor();
                    elem.setAttribute(!getSetAttribute && jQuery.propFix[name] || name, name);
                } else {
                    Sensor();
                    elem[jQuery.camelCase('default-' + name)] = elem[name] = true;
                }
            }
            var IARETREPLACE = name;
            Sensor();
            return IARETREPLACE;
        }
    };
    jQuery.each(jQuery.expr.match.bool.source.match(/\w+/g), function (i, name) {
        Sensor();
        var getter = attrHandle[name] || jQuery.find.attr;
        attrHandle[name] = getSetInput && getSetAttribute || !ruseDefault.test(name) ? function (elem, name, isXML) {
            Sensor();
            var ret, handle;
            Sensor();
            if (!isXML) {
                Sensor();
                handle = attrHandle[name];
                attrHandle[name] = ret;
                ret = getter(elem, name, isXML) != null ? name.toLowerCase() : null;
                attrHandle[name] = handle;
            }
            var IARETREPLACE = ret;
            Sensor();
            return IARETREPLACE;
        } : function (elem, name, isXML) {
            Sensor();
            if (!isXML) {
                var IARETREPLACE = elem[jQuery.camelCase('default-' + name)] ? name.toLowerCase() : null;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
        };
        Sensor();
    });
    Sensor();
    if (!getSetInput || !getSetAttribute) {
        Sensor();
        jQuery.attrHooks.value = {
            set: function (elem, value, name) {
                Sensor();
                if (jQuery.nodeName(elem, 'input')) {
                    Sensor();
                    elem.defaultValue = value;
                } else {
                    var IARETREPLACE = nodeHook && nodeHook.set(elem, value, name);
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            }
        };
    }
    Sensor();
    if (!getSetAttribute) {
        Sensor();
        nodeHook = {
            set: function (elem, value, name) {
                Sensor();
                var ret = elem.getAttributeNode(name);
                Sensor();
                if (!ret) {
                    Sensor();
                    elem.setAttributeNode(ret = elem.ownerDocument.createAttribute(name));
                }
                Sensor();
                ret.value = value += '';
                Sensor();
                if (name === 'value' || value === elem.getAttribute(name)) {
                    var IARETREPLACE = value;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            }
        };
        attrHandle.id = attrHandle.name = attrHandle.coords = function (elem, name, isXML) {
            Sensor();
            var ret;
            Sensor();
            if (!isXML) {
                var IARETREPLACE = (ret = elem.getAttributeNode(name)) && ret.value !== '' ? ret.value : null;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
        };
        jQuery.valHooks.button = {
            get: function (elem, name) {
                Sensor();
                var ret = elem.getAttributeNode(name);
                Sensor();
                if (ret && ret.specified) {
                    var IARETREPLACE = ret.value;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
            },
            set: nodeHook.set
        };
        jQuery.attrHooks.contenteditable = {
            set: function (elem, value, name) {
                Sensor();
                nodeHook.set(elem, value === '' ? false : value, name);
                Sensor();
            }
        };
        jQuery.each([
            'width',
            'height'
        ], function (i, name) {
            Sensor();
            jQuery.attrHooks[name] = {
                set: function (elem, value) {
                    Sensor();
                    if (value === '') {
                        Sensor();
                        elem.setAttribute(name, 'auto');
                        var IARETREPLACE = value;
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                }
            };
            Sensor();
        });
    }
    Sensor();
    if (!support.style) {
        Sensor();
        jQuery.attrHooks.style = {
            get: function (elem) {
                var IARETREPLACE = elem.style.cssText || undefined;
                Sensor();
                return IARETREPLACE;
            },
            set: function (elem, value) {
                var IARETREPLACE = elem.style.cssText = value + '';
                Sensor();
                return IARETREPLACE;
            }
        };
    }
    Sensor();
    var rfocusable = /^(?:input|select|textarea|button|object)$/i, rclickable = /^(?:a|area)$/i;
    jQuery.fn.extend({
        prop: function (name, value) {
            var IARETREPLACE = access(this, jQuery.prop, name, value, arguments.length > 1);
            Sensor();
            return IARETREPLACE;
        },
        removeProp: function (name) {
            Sensor();
            name = jQuery.propFix[name] || name;
            var IARETREPLACE = this.each(function () {
                Sensor();
                try {
                    Sensor();
                    this[name] = undefined;
                    delete this[name];
                } catch (e) {
                    Sensor();
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.extend({
        propFix: {
            'for': 'htmlFor',
            'class': 'className'
        },
        prop: function (elem, name, value) {
            Sensor();
            var ret, hooks, notxml, nType = elem.nodeType;
            Sensor();
            if (!elem || nType === 3 || nType === 8 || nType === 2) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            notxml = nType !== 1 || !jQuery.isXMLDoc(elem);
            Sensor();
            if (notxml) {
                Sensor();
                name = jQuery.propFix[name] || name;
                hooks = jQuery.propHooks[name];
            }
            Sensor();
            if (value !== undefined) {
                var IARETREPLACE = hooks && 'set' in hooks && (ret = hooks.set(elem, value, name)) !== undefined ? ret : elem[name] = value;
                Sensor();
                return IARETREPLACE;
            } else {
                var IARETREPLACE = hooks && 'get' in hooks && (ret = hooks.get(elem, name)) !== null ? ret : elem[name];
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
        },
        propHooks: {
            tabIndex: {
                get: function (elem) {
                    Sensor();
                    var tabindex = jQuery.find.attr(elem, 'tabindex');
                    var IARETREPLACE = tabindex ? parseInt(tabindex, 10) : rfocusable.test(elem.nodeName) || rclickable.test(elem.nodeName) && elem.href ? 0 : -1;
                    Sensor();
                    return IARETREPLACE;
                }
            }
        }
    });
    Sensor();
    if (!support.hrefNormalized) {
        Sensor();
        jQuery.each([
            'href',
            'src'
        ], function (i, name) {
            Sensor();
            jQuery.propHooks[name] = {
                get: function (elem) {
                    var IARETREPLACE = elem.getAttribute(name, 4);
                    Sensor();
                    return IARETREPLACE;
                }
            };
            Sensor();
        });
    }
    Sensor();
    if (!support.optSelected) {
        Sensor();
        jQuery.propHooks.selected = {
            get: function (elem) {
                Sensor();
                var parent = elem.parentNode;
                Sensor();
                if (parent) {
                    Sensor();
                    parent.selectedIndex;
                    Sensor();
                    if (parent.parentNode) {
                        Sensor();
                        parent.parentNode.selectedIndex;
                    }
                }
                var IARETREPLACE = null;
                Sensor();
                return IARETREPLACE;
            }
        };
    }
    Sensor();
    jQuery.each([
        'tabIndex',
        'readOnly',
        'maxLength',
        'cellSpacing',
        'cellPadding',
        'rowSpan',
        'colSpan',
        'useMap',
        'frameBorder',
        'contentEditable'
    ], function () {
        Sensor();
        jQuery.propFix[this.toLowerCase()] = this;
        Sensor();
    });
    Sensor();
    if (!support.enctype) {
        Sensor();
        jQuery.propFix.enctype = 'encoding';
    }
    Sensor();
    var rclass = /[\t\r\n\f]/g;
    jQuery.fn.extend({
        addClass: function (value) {
            Sensor();
            var classes, elem, cur, clazz, j, finalValue, i = 0, len = this.length, proceed = typeof value === 'string' && value;
            Sensor();
            if (jQuery.isFunction(value)) {
                var IARETREPLACE = this.each(function (j) {
                    Sensor();
                    jQuery(this).addClass(value.call(this, j, this.className));
                    Sensor();
                });
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (proceed) {
                Sensor();
                classes = (value || '').match(rnotwhite) || [];
                Sensor();
                for (; i < len; i++) {
                    Sensor();
                    elem = this[i];
                    cur = elem.nodeType === 1 && (elem.className ? (' ' + elem.className + ' ').replace(rclass, ' ') : ' ');
                    Sensor();
                    if (cur) {
                        Sensor();
                        j = 0;
                        Sensor();
                        while (clazz = classes[j++]) {
                            Sensor();
                            if (cur.indexOf(' ' + clazz + ' ') < 0) {
                                Sensor();
                                cur += clazz + ' ';
                            }
                        }
                        Sensor();
                        finalValue = jQuery.trim(cur);
                        Sensor();
                        if (elem.className !== finalValue) {
                            Sensor();
                            elem.className = finalValue;
                        }
                    }
                }
            }
            var IARETREPLACE = this;
            Sensor();
            return IARETREPLACE;
        },
        removeClass: function (value) {
            Sensor();
            var classes, elem, cur, clazz, j, finalValue, i = 0, len = this.length, proceed = arguments.length === 0 || typeof value === 'string' && value;
            Sensor();
            if (jQuery.isFunction(value)) {
                var IARETREPLACE = this.each(function (j) {
                    Sensor();
                    jQuery(this).removeClass(value.call(this, j, this.className));
                    Sensor();
                });
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (proceed) {
                Sensor();
                classes = (value || '').match(rnotwhite) || [];
                Sensor();
                for (; i < len; i++) {
                    Sensor();
                    elem = this[i];
                    cur = elem.nodeType === 1 && (elem.className ? (' ' + elem.className + ' ').replace(rclass, ' ') : '');
                    Sensor();
                    if (cur) {
                        Sensor();
                        j = 0;
                        Sensor();
                        while (clazz = classes[j++]) {
                            Sensor();
                            while (cur.indexOf(' ' + clazz + ' ') >= 0) {
                                Sensor();
                                cur = cur.replace(' ' + clazz + ' ', ' ');
                            }
                        }
                        Sensor();
                        finalValue = value ? jQuery.trim(cur) : '';
                        Sensor();
                        if (elem.className !== finalValue) {
                            Sensor();
                            elem.className = finalValue;
                        }
                    }
                }
            }
            var IARETREPLACE = this;
            Sensor();
            return IARETREPLACE;
        },
        toggleClass: function (value, stateVal) {
            Sensor();
            var type = typeof value;
            Sensor();
            if (typeof stateVal === 'boolean' && type === 'string') {
                var IARETREPLACE = stateVal ? this.addClass(value) : this.removeClass(value);
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (jQuery.isFunction(value)) {
                var IARETREPLACE = this.each(function (i) {
                    Sensor();
                    jQuery(this).toggleClass(value.call(this, i, this.className, stateVal), stateVal);
                    Sensor();
                });
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = this.each(function () {
                Sensor();
                if (type === 'string') {
                    Sensor();
                    var className, i = 0, self = jQuery(this), classNames = value.match(rnotwhite) || [];
                    Sensor();
                    while (className = classNames[i++]) {
                        Sensor();
                        if (self.hasClass(className)) {
                            Sensor();
                            self.removeClass(className);
                        } else {
                            Sensor();
                            self.addClass(className);
                        }
                    }
                } else {
                    Sensor();
                    if (type === strundefined || type === 'boolean') {
                        Sensor();
                        if (this.className) {
                            Sensor();
                            jQuery._data(this, '__className__', this.className);
                        }
                        Sensor();
                        this.className = this.className || value === false ? '' : jQuery._data(this, '__className__') || '';
                    }
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        hasClass: function (selector) {
            Sensor();
            var className = ' ' + selector + ' ', i = 0, l = this.length;
            Sensor();
            for (; i < l; i++) {
                Sensor();
                if (this[i].nodeType === 1 && (' ' + this[i].className + ' ').replace(rclass, ' ').indexOf(className) >= 0) {
                    var IARETREPLACE = true;
                    Sensor();
                    return IARETREPLACE;
                }
            }
            var IARETREPLACE = false;
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.each(('blur focus focusin focusout load resize scroll unload click dblclick ' + 'mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave ' + 'change select submit keydown keypress keyup error contextmenu').split(' '), function (i, name) {
        Sensor();
        jQuery.fn[name] = function (data, fn) {
            var IARETREPLACE = arguments.length > 0 ? this.on(name, null, data, fn) : this.trigger(name);
            Sensor();
            return IARETREPLACE;
        };
        Sensor();
    });
    jQuery.fn.extend({
        hover: function (fnOver, fnOut) {
            var IARETREPLACE = this.mouseenter(fnOver).mouseleave(fnOut || fnOver);
            Sensor();
            return IARETREPLACE;
        },
        bind: function (types, data, fn) {
            var IARETREPLACE = this.on(types, null, data, fn);
            Sensor();
            return IARETREPLACE;
        },
        unbind: function (types, fn) {
            var IARETREPLACE = this.off(types, null, fn);
            Sensor();
            return IARETREPLACE;
        },
        delegate: function (selector, types, data, fn) {
            var IARETREPLACE = this.on(types, selector, data, fn);
            Sensor();
            return IARETREPLACE;
        },
        undelegate: function (selector, types, fn) {
            var IARETREPLACE = arguments.length === 1 ? this.off(selector, '**') : this.off(types, selector || '**', fn);
            Sensor();
            return IARETREPLACE;
        }
    });
    var nonce = jQuery.now();
    var rquery = /\?/;
    var rvalidtokens = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    jQuery.parseJSON = function (data) {
        Sensor();
        if (window.JSON && window.JSON.parse) {
            var IARETREPLACE = window.JSON.parse(data + '');
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        var requireNonComma, depth = null, str = jQuery.trim(data + '');
        var IARETREPLACE = str && !jQuery.trim(str.replace(rvalidtokens, function (token, comma, open, close) {
            Sensor();
            if (requireNonComma && comma) {
                Sensor();
                depth = 0;
            }
            Sensor();
            if (depth === 0) {
                var IARETREPLACE = token;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            requireNonComma = open || comma;
            depth += !close - !open;
            var IARETREPLACE = '';
            Sensor();
            return IARETREPLACE;
        })) ? Function('return ' + str)() : jQuery.error('Invalid JSON: ' + data);
        Sensor();
        return IARETREPLACE;
    };
    jQuery.parseXML = function (data) {
        Sensor();
        var xml, tmp;
        Sensor();
        if (!data || typeof data !== 'string') {
            var IARETREPLACE = null;
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        try {
            Sensor();
            if (window.DOMParser) {
                Sensor();
                tmp = new DOMParser();
                xml = tmp.parseFromString(data, 'text/xml');
            } else {
                Sensor();
                xml = new ActiveXObject('Microsoft.XMLDOM');
                xml.async = 'false';
                xml.loadXML(data);
            }
        } catch (e) {
            Sensor();
            xml = undefined;
        }
        Sensor();
        if (!xml || !xml.documentElement || xml.getElementsByTagName('parsererror').length) {
            Sensor();
            jQuery.error('Invalid XML: ' + data);
        }
        var IARETREPLACE = xml;
        Sensor();
        return IARETREPLACE;
    };
    var ajaxLocParts, ajaxLocation, rhash = /#.*$/, rts = /([?&])_=[^&]*/, rheaders = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm, rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, rnoContent = /^(?:GET|HEAD)$/, rprotocol = /^\/\//, rurl = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, prefilters = {}, transports = {}, allTypes = '*/'.concat('*');
    Sensor();
    try {
        Sensor();
        ajaxLocation = location.href;
    } catch (e) {
        Sensor();
        ajaxLocation = document.createElement('a');
        ajaxLocation.href = '';
        ajaxLocation = ajaxLocation.href;
    }
    Sensor();
    ajaxLocParts = rurl.exec(ajaxLocation.toLowerCase()) || [];
    function addToPrefiltersOrTransports(structure) {
        var IARETREPLACE = function (dataTypeExpression, func) {
            Sensor();
            if (typeof dataTypeExpression !== 'string') {
                Sensor();
                func = dataTypeExpression;
                dataTypeExpression = '*';
            }
            Sensor();
            var dataType, i = 0, dataTypes = dataTypeExpression.toLowerCase().match(rnotwhite) || [];
            Sensor();
            if (jQuery.isFunction(func)) {
                Sensor();
                while (dataType = dataTypes[i++]) {
                    Sensor();
                    if (dataType.charAt(0) === '+') {
                        Sensor();
                        dataType = dataType.slice(1) || '*';
                        (structure[dataType] = structure[dataType] || []).unshift(func);
                    } else {
                        Sensor();
                        (structure[dataType] = structure[dataType] || []).push(func);
                    }
                }
            }
            Sensor();
        };
        Sensor();
        return IARETREPLACE;
    }
    function inspectPrefiltersOrTransports(structure, options, originalOptions, jqXHR) {
        Sensor();
        var inspected = {}, seekingTransport = structure === transports;
        function inspect(dataType) {
            Sensor();
            var selected;
            inspected[dataType] = true;
            jQuery.each(structure[dataType] || [], function (_, prefilterOrFactory) {
                Sensor();
                var dataTypeOrTransport = prefilterOrFactory(options, originalOptions, jqXHR);
                Sensor();
                if (typeof dataTypeOrTransport === 'string' && !seekingTransport && !inspected[dataTypeOrTransport]) {
                    Sensor();
                    options.dataTypes.unshift(dataTypeOrTransport);
                    inspect(dataTypeOrTransport);
                    var IARETREPLACE = false;
                    Sensor();
                    return IARETREPLACE;
                } else {
                    Sensor();
                    if (seekingTransport) {
                        var IARETREPLACE = !(selected = dataTypeOrTransport);
                        Sensor();
                        return IARETREPLACE;
                    }
                }
                Sensor();
            });
            var IARETREPLACE = selected;
            Sensor();
            return IARETREPLACE;
        }
        var IARETREPLACE = inspect(options.dataTypes[0]) || !inspected['*'] && inspect('*');
        Sensor();
        return IARETREPLACE;
    }
    function ajaxExtend(target, src) {
        Sensor();
        var deep, key, flatOptions = jQuery.ajaxSettings.flatOptions || {};
        Sensor();
        for (key in src) {
            Sensor();
            if (src[key] !== undefined) {
                Sensor();
                (flatOptions[key] ? target : deep || (deep = {}))[key] = src[key];
            }
        }
        Sensor();
        if (deep) {
            Sensor();
            jQuery.extend(true, target, deep);
        }
        var IARETREPLACE = target;
        Sensor();
        return IARETREPLACE;
    }
    function ajaxHandleResponses(s, jqXHR, responses) {
        Sensor();
        var firstDataType, ct, finalDataType, type, contents = s.contents, dataTypes = s.dataTypes;
        Sensor();
        while (dataTypes[0] === '*') {
            Sensor();
            dataTypes.shift();
            Sensor();
            if (ct === undefined) {
                Sensor();
                ct = s.mimeType || jqXHR.getResponseHeader('Content-Type');
            }
        }
        Sensor();
        if (ct) {
            Sensor();
            for (type in contents) {
                Sensor();
                if (contents[type] && contents[type].test(ct)) {
                    Sensor();
                    dataTypes.unshift(type);
                    Sensor();
                    break;
                }
            }
        }
        Sensor();
        if (dataTypes[0] in responses) {
            Sensor();
            finalDataType = dataTypes[0];
        } else {
            Sensor();
            for (type in responses) {
                Sensor();
                if (!dataTypes[0] || s.converters[type + ' ' + dataTypes[0]]) {
                    Sensor();
                    finalDataType = type;
                    Sensor();
                    break;
                }
                Sensor();
                if (!firstDataType) {
                    Sensor();
                    firstDataType = type;
                }
            }
            Sensor();
            finalDataType = finalDataType || firstDataType;
        }
        Sensor();
        if (finalDataType) {
            Sensor();
            if (finalDataType !== dataTypes[0]) {
                Sensor();
                dataTypes.unshift(finalDataType);
            }
            var IARETREPLACE = responses[finalDataType];
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
    }
    function ajaxConvert(s, response, jqXHR, isSuccess) {
        Sensor();
        var conv2, current, conv, tmp, prev, converters = {}, dataTypes = s.dataTypes.slice();
        Sensor();
        if (dataTypes[1]) {
            Sensor();
            for (conv in s.converters) {
                Sensor();
                converters[conv.toLowerCase()] = s.converters[conv];
            }
        }
        Sensor();
        current = dataTypes.shift();
        Sensor();
        while (current) {
            Sensor();
            if (s.responseFields[current]) {
                Sensor();
                jqXHR[s.responseFields[current]] = response;
            }
            Sensor();
            if (!prev && isSuccess && s.dataFilter) {
                Sensor();
                response = s.dataFilter(response, s.dataType);
            }
            Sensor();
            prev = current;
            Sensor();
            current = dataTypes.shift();
            Sensor();
            if (current) {
                Sensor();
                if (current === '*') {
                    Sensor();
                    current = prev;
                } else {
                    Sensor();
                    if (prev !== '*' && prev !== current) {
                        Sensor();
                        conv = converters[prev + ' ' + current] || converters['* ' + current];
                        Sensor();
                        if (!conv) {
                            Sensor();
                            for (conv2 in converters) {
                                Sensor();
                                tmp = conv2.split(' ');
                                Sensor();
                                if (tmp[1] === current) {
                                    Sensor();
                                    conv = converters[prev + ' ' + tmp[0]] || converters['* ' + tmp[0]];
                                    Sensor();
                                    if (conv) {
                                        Sensor();
                                        if (conv === true) {
                                            Sensor();
                                            conv = converters[conv2];
                                        } else {
                                            Sensor();
                                            if (converters[conv2] !== true) {
                                                Sensor();
                                                current = tmp[0];
                                                dataTypes.unshift(tmp[1]);
                                            }
                                        }
                                        Sensor();
                                        break;
                                    }
                                }
                            }
                        }
                        Sensor();
                        if (conv !== true) {
                            Sensor();
                            if (conv && s['throws']) {
                                Sensor();
                                response = conv(response);
                            } else {
                                Sensor();
                                try {
                                    Sensor();
                                    response = conv(response);
                                } catch (e) {
                                    var IARETREPLACE = {
                                        state: 'parsererror',
                                        error: conv ? e : 'No conversion from ' + prev + ' to ' + current
                                    };
                                    Sensor();
                                    return IARETREPLACE;
                                }
                            }
                        }
                    }
                }
            }
        }
        var IARETREPLACE = {
            state: 'success',
            data: response
        };
        Sensor();
        return IARETREPLACE;
    }
    jQuery.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: ajaxLocation,
            type: 'GET',
            isLocal: rlocalProtocol.test(ajaxLocParts[1]),
            global: true,
            processData: true,
            async: true,
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            accepts: {
                '*': allTypes,
                text: 'text/plain',
                html: 'text/html',
                xml: 'application/xml, text/xml',
                json: 'application/json, text/javascript'
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: 'responseXML',
                text: 'responseText',
                json: 'responseJSON'
            },
            converters: {
                '* text': String,
                'text html': true,
                'text json': jQuery.parseJSON,
                'text xml': jQuery.parseXML
            },
            flatOptions: {
                url: true,
                context: true
            }
        },
        ajaxSetup: function (target, settings) {
            var IARETREPLACE = settings ? ajaxExtend(ajaxExtend(target, jQuery.ajaxSettings), settings) : ajaxExtend(jQuery.ajaxSettings, target);
            Sensor();
            return IARETREPLACE;
        },
        ajaxPrefilter: addToPrefiltersOrTransports(prefilters),
        ajaxTransport: addToPrefiltersOrTransports(transports),
        ajax: function (url, options) {
            Sensor();
            if (typeof url === 'object') {
                Sensor();
                options = url;
                url = undefined;
            }
            Sensor();
            options = options || {};
            var parts, i, cacheURL, responseHeadersString, timeoutTimer, fireGlobals, transport, responseHeaders, s = jQuery.ajaxSetup({}, options), callbackContext = s.context || s, globalEventContext = s.context && (callbackContext.nodeType || callbackContext.jquery) ? jQuery(callbackContext) : jQuery.event, deferred = jQuery.Deferred(), completeDeferred = jQuery.Callbacks('once memory'), statusCode = s.statusCode || {}, requestHeaders = {}, requestHeadersNames = {}, state = 0, strAbort = 'canceled', jqXHR = {
                    readyState: 0,
                    getResponseHeader: function (key) {
                        Sensor();
                        var match;
                        Sensor();
                        if (state === 2) {
                            Sensor();
                            if (!responseHeaders) {
                                Sensor();
                                responseHeaders = {};
                                Sensor();
                                while (match = rheaders.exec(responseHeadersString)) {
                                    Sensor();
                                    responseHeaders[match[1].toLowerCase()] = match[2];
                                }
                            }
                            Sensor();
                            match = responseHeaders[key.toLowerCase()];
                        }
                        var IARETREPLACE = match == null ? null : match;
                        Sensor();
                        return IARETREPLACE;
                    },
                    getAllResponseHeaders: function () {
                        var IARETREPLACE = state === 2 ? responseHeadersString : null;
                        Sensor();
                        return IARETREPLACE;
                    },
                    setRequestHeader: function (name, value) {
                        Sensor();
                        var lname = name.toLowerCase();
                        Sensor();
                        if (!state) {
                            Sensor();
                            name = requestHeadersNames[lname] = requestHeadersNames[lname] || name;
                            requestHeaders[name] = value;
                        }
                        var IARETREPLACE = this;
                        Sensor();
                        return IARETREPLACE;
                    },
                    overrideMimeType: function (type) {
                        Sensor();
                        if (!state) {
                            Sensor();
                            s.mimeType = type;
                        }
                        var IARETREPLACE = this;
                        Sensor();
                        return IARETREPLACE;
                    },
                    statusCode: function (map) {
                        Sensor();
                        var code;
                        Sensor();
                        if (map) {
                            Sensor();
                            if (state < 2) {
                                Sensor();
                                for (code in map) {
                                    Sensor();
                                    statusCode[code] = [
                                        statusCode[code],
                                        map[code]
                                    ];
                                }
                            } else {
                                Sensor();
                                jqXHR.always(map[jqXHR.status]);
                            }
                        }
                        var IARETREPLACE = this;
                        Sensor();
                        return IARETREPLACE;
                    },
                    abort: function (statusText) {
                        Sensor();
                        var finalText = statusText || strAbort;
                        Sensor();
                        if (transport) {
                            Sensor();
                            transport.abort(finalText);
                        }
                        Sensor();
                        done(0, finalText);
                        var IARETREPLACE = this;
                        Sensor();
                        return IARETREPLACE;
                    }
                };
            deferred.promise(jqXHR).complete = completeDeferred.add;
            jqXHR.success = jqXHR.done;
            jqXHR.error = jqXHR.fail;
            s.url = ((url || s.url || ajaxLocation) + '').replace(rhash, '').replace(rprotocol, ajaxLocParts[1] + '//');
            s.type = options.method || options.type || s.method || s.type;
            s.dataTypes = jQuery.trim(s.dataType || '*').toLowerCase().match(rnotwhite) || [''];
            Sensor();
            if (s.crossDomain == null) {
                Sensor();
                parts = rurl.exec(s.url.toLowerCase());
                s.crossDomain = !!(parts && (parts[1] !== ajaxLocParts[1] || parts[2] !== ajaxLocParts[2] || (parts[3] || (parts[1] === 'http:' ? '80' : '443')) !== (ajaxLocParts[3] || (ajaxLocParts[1] === 'http:' ? '80' : '443'))));
            }
            Sensor();
            if (s.data && s.processData && typeof s.data !== 'string') {
                Sensor();
                s.data = jQuery.param(s.data, s.traditional);
            }
            Sensor();
            inspectPrefiltersOrTransports(prefilters, s, options, jqXHR);
            Sensor();
            if (state === 2) {
                var IARETREPLACE = jqXHR;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            fireGlobals = s.global;
            Sensor();
            if (fireGlobals && jQuery.active++ === 0) {
                Sensor();
                jQuery.event.trigger('ajaxStart');
            }
            Sensor();
            s.type = s.type.toUpperCase();
            s.hasContent = !rnoContent.test(s.type);
            cacheURL = s.url;
            Sensor();
            if (!s.hasContent) {
                Sensor();
                if (s.data) {
                    Sensor();
                    cacheURL = s.url += (rquery.test(cacheURL) ? '&' : '?') + s.data;
                    delete s.data;
                }
                Sensor();
                if (s.cache === false) {
                    Sensor();
                    s.url = rts.test(cacheURL) ? cacheURL.replace(rts, '$1_=' + nonce++) : cacheURL + (rquery.test(cacheURL) ? '&' : '?') + '_=' + nonce++;
                }
            }
            Sensor();
            if (s.ifModified) {
                Sensor();
                if (jQuery.lastModified[cacheURL]) {
                    Sensor();
                    jqXHR.setRequestHeader('If-Modified-Since', jQuery.lastModified[cacheURL]);
                }
                Sensor();
                if (jQuery.etag[cacheURL]) {
                    Sensor();
                    jqXHR.setRequestHeader('If-None-Match', jQuery.etag[cacheURL]);
                }
            }
            Sensor();
            if (s.data && s.hasContent && s.contentType !== false || options.contentType) {
                Sensor();
                jqXHR.setRequestHeader('Content-Type', s.contentType);
            }
            Sensor();
            jqXHR.setRequestHeader('Accept', s.dataTypes[0] && s.accepts[s.dataTypes[0]] ? s.accepts[s.dataTypes[0]] + (s.dataTypes[0] !== '*' ? ', ' + allTypes + '; q=0.01' : '') : s.accepts['*']);
            Sensor();
            for (i in s.headers) {
                Sensor();
                jqXHR.setRequestHeader(i, s.headers[i]);
            }
            Sensor();
            if (s.beforeSend && (s.beforeSend.call(callbackContext, jqXHR, s) === false || state === 2)) {
                var IARETREPLACE = jqXHR.abort();
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            strAbort = 'abort';
            Sensor();
            for (i in {
                    success: 1,
                    error: 1,
                    complete: 1
                }) {
                Sensor();
                jqXHR[i](s[i]);
            }
            Sensor();
            transport = inspectPrefiltersOrTransports(transports, s, options, jqXHR);
            Sensor();
            if (!transport) {
                Sensor();
                done(-1, 'No Transport');
            } else {
                Sensor();
                jqXHR.readyState = 1;
                Sensor();
                if (fireGlobals) {
                    Sensor();
                    globalEventContext.trigger('ajaxSend', [
                        jqXHR,
                        s
                    ]);
                }
                Sensor();
                if (s.async && s.timeout > 0) {
                    Sensor();
                    timeoutTimer = setTimeout(function () {
                        Sensor();
                        jqXHR.abort('timeout');
                        Sensor();
                    }, s.timeout);
                }
                Sensor();
                try {
                    Sensor();
                    state = 1;
                    transport.send(requestHeaders, done);
                } catch (e) {
                    Sensor();
                    if (state < 2) {
                        Sensor();
                        done(-1, e);
                    } else {
                        Sensor();
                        throw e;
                    }
                }
            }
            function done(status, nativeStatusText, responses, headers) {
                Sensor();
                var isSuccess, success, error, response, modified, statusText = nativeStatusText;
                Sensor();
                if (state === 2) {
                    var IARETREPLACE;
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
                state = 2;
                Sensor();
                if (timeoutTimer) {
                    Sensor();
                    clearTimeout(timeoutTimer);
                }
                Sensor();
                transport = undefined;
                responseHeadersString = headers || '';
                jqXHR.readyState = status > 0 ? 4 : 0;
                isSuccess = status >= 200 && status < 300 || status === 304;
                Sensor();
                if (responses) {
                    Sensor();
                    response = ajaxHandleResponses(s, jqXHR, responses);
                }
                Sensor();
                response = ajaxConvert(s, response, jqXHR, isSuccess);
                Sensor();
                if (isSuccess) {
                    Sensor();
                    if (s.ifModified) {
                        Sensor();
                        modified = jqXHR.getResponseHeader('Last-Modified');
                        Sensor();
                        if (modified) {
                            Sensor();
                            jQuery.lastModified[cacheURL] = modified;
                        }
                        Sensor();
                        modified = jqXHR.getResponseHeader('etag');
                        Sensor();
                        if (modified) {
                            Sensor();
                            jQuery.etag[cacheURL] = modified;
                        }
                    }
                    Sensor();
                    if (status === 204 || s.type === 'HEAD') {
                        Sensor();
                        statusText = 'nocontent';
                    } else {
                        Sensor();
                        if (status === 304) {
                            Sensor();
                            statusText = 'notmodified';
                        } else {
                            Sensor();
                            statusText = response.state;
                            success = response.data;
                            error = response.error;
                            isSuccess = !error;
                        }
                    }
                } else {
                    Sensor();
                    error = statusText;
                    Sensor();
                    if (status || !statusText) {
                        Sensor();
                        statusText = 'error';
                        Sensor();
                        if (status < 0) {
                            Sensor();
                            status = 0;
                        }
                    }
                }
                Sensor();
                jqXHR.status = status;
                jqXHR.statusText = (nativeStatusText || statusText) + '';
                Sensor();
                if (isSuccess) {
                    Sensor();
                    deferred.resolveWith(callbackContext, [
                        success,
                        statusText,
                        jqXHR
                    ]);
                } else {
                    Sensor();
                    deferred.rejectWith(callbackContext, [
                        jqXHR,
                        statusText,
                        error
                    ]);
                }
                Sensor();
                jqXHR.statusCode(statusCode);
                statusCode = undefined;
                Sensor();
                if (fireGlobals) {
                    Sensor();
                    globalEventContext.trigger(isSuccess ? 'ajaxSuccess' : 'ajaxError', [
                        jqXHR,
                        s,
                        isSuccess ? success : error
                    ]);
                }
                Sensor();
                completeDeferred.fireWith(callbackContext, [
                    jqXHR,
                    statusText
                ]);
                Sensor();
                if (fireGlobals) {
                    Sensor();
                    globalEventContext.trigger('ajaxComplete', [
                        jqXHR,
                        s
                    ]);
                    Sensor();
                    if (!--jQuery.active) {
                        Sensor();
                        jQuery.event.trigger('ajaxStop');
                    }
                }
                Sensor();
            }
            var IARETREPLACE = jqXHR;
            Sensor();
            return IARETREPLACE;
        },
        getJSON: function (url, data, callback) {
            var IARETREPLACE = jQuery.get(url, data, callback, 'json');
            Sensor();
            return IARETREPLACE;
        },
        getScript: function (url, callback) {
            var IARETREPLACE = jQuery.get(url, undefined, callback, 'script');
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.each([
        'get',
        'post'
    ], function (i, method) {
        Sensor();
        jQuery[method] = function (url, data, callback, type) {
            Sensor();
            if (jQuery.isFunction(data)) {
                Sensor();
                type = type || callback;
                callback = data;
                data = undefined;
            }
            var IARETREPLACE = jQuery.ajax({
                url: url,
                type: method,
                dataType: type,
                data: data,
                success: callback
            });
            Sensor();
            return IARETREPLACE;
        };
        Sensor();
    });
    jQuery.each([
        'ajaxStart',
        'ajaxStop',
        'ajaxComplete',
        'ajaxError',
        'ajaxSuccess',
        'ajaxSend'
    ], function (i, type) {
        Sensor();
        jQuery.fn[type] = function (fn) {
            var IARETREPLACE = this.on(type, fn);
            Sensor();
            return IARETREPLACE;
        };
        Sensor();
    });
    jQuery._evalUrl = function (url) {
        var IARETREPLACE = jQuery.ajax({
            url: url,
            type: 'GET',
            dataType: 'script',
            async: false,
            global: false,
            'throws': true
        });
        Sensor();
        return IARETREPLACE;
    };
    jQuery.fn.extend({
        wrapAll: function (html) {
            Sensor();
            if (jQuery.isFunction(html)) {
                var IARETREPLACE = this.each(function (i) {
                    Sensor();
                    jQuery(this).wrapAll(html.call(this, i));
                    Sensor();
                });
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (this[0]) {
                Sensor();
                var wrap = jQuery(html, this[0].ownerDocument).eq(0).clone(true);
                Sensor();
                if (this[0].parentNode) {
                    Sensor();
                    wrap.insertBefore(this[0]);
                }
                Sensor();
                wrap.map(function () {
                    Sensor();
                    var elem = this;
                    Sensor();
                    while (elem.firstChild && elem.firstChild.nodeType === 1) {
                        Sensor();
                        elem = elem.firstChild;
                    }
                    var IARETREPLACE = elem;
                    Sensor();
                    return IARETREPLACE;
                }).append(this);
            }
            var IARETREPLACE = this;
            Sensor();
            return IARETREPLACE;
        },
        wrapInner: function (html) {
            Sensor();
            if (jQuery.isFunction(html)) {
                var IARETREPLACE = this.each(function (i) {
                    Sensor();
                    jQuery(this).wrapInner(html.call(this, i));
                    Sensor();
                });
                Sensor();
                return IARETREPLACE;
            }
            var IARETREPLACE = this.each(function () {
                Sensor();
                var self = jQuery(this), contents = self.contents();
                Sensor();
                if (contents.length) {
                    Sensor();
                    contents.wrapAll(html);
                } else {
                    Sensor();
                    self.append(html);
                }
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        wrap: function (html) {
            Sensor();
            var isFunction = jQuery.isFunction(html);
            var IARETREPLACE = this.each(function (i) {
                Sensor();
                jQuery(this).wrapAll(isFunction ? html.call(this, i) : html);
                Sensor();
            });
            Sensor();
            return IARETREPLACE;
        },
        unwrap: function () {
            var IARETREPLACE = this.parent().each(function () {
                Sensor();
                if (!jQuery.nodeName(this, 'body')) {
                    Sensor();
                    jQuery(this).replaceWith(this.childNodes);
                }
                Sensor();
            }).end();
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.expr.filters.hidden = function (elem) {
        var IARETREPLACE = elem.offsetWidth <= 0 && elem.offsetHeight <= 0 || !support.reliableHiddenOffsets() && (elem.style && elem.style.display || jQuery.css(elem, 'display')) === 'none';
        Sensor();
        return IARETREPLACE;
    };
    jQuery.expr.filters.visible = function (elem) {
        var IARETREPLACE = !jQuery.expr.filters.hidden(elem);
        Sensor();
        return IARETREPLACE;
    };
    var r20 = /%20/g, rbracket = /\[\]$/, rCRLF = /\r?\n/g, rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i, rsubmittable = /^(?:input|select|textarea|keygen)/i;
    function buildParams(prefix, obj, traditional, add) {
        Sensor();
        var name;
        Sensor();
        if (jQuery.isArray(obj)) {
            Sensor();
            jQuery.each(obj, function (i, v) {
                Sensor();
                if (traditional || rbracket.test(prefix)) {
                    Sensor();
                    add(prefix, v);
                } else {
                    Sensor();
                    buildParams(prefix + '[' + (typeof v === 'object' ? i : '') + ']', v, traditional, add);
                }
                Sensor();
            });
        } else {
            Sensor();
            if (!traditional && jQuery.type(obj) === 'object') {
                Sensor();
                for (name in obj) {
                    Sensor();
                    buildParams(prefix + '[' + name + ']', obj[name], traditional, add);
                }
            } else {
                Sensor();
                add(prefix, obj);
            }
        }
        Sensor();
    }
    jQuery.param = function (a, traditional) {
        Sensor();
        var prefix, s = [], add = function (key, value) {
                Sensor();
                value = jQuery.isFunction(value) ? value() : value == null ? '' : value;
                s[s.length] = encodeURIComponent(key) + '=' + encodeURIComponent(value);
                Sensor();
            };
        Sensor();
        if (traditional === undefined) {
            Sensor();
            traditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;
        }
        Sensor();
        if (jQuery.isArray(a) || a.jquery && !jQuery.isPlainObject(a)) {
            Sensor();
            jQuery.each(a, function () {
                Sensor();
                add(this.name, this.value);
                Sensor();
            });
        } else {
            Sensor();
            for (prefix in a) {
                Sensor();
                buildParams(prefix, a[prefix], traditional, add);
            }
        }
        var IARETREPLACE = s.join('&').replace(r20, '+');
        Sensor();
        return IARETREPLACE;
    };
    jQuery.fn.extend({
        serialize: function () {
            var IARETREPLACE = jQuery.param(this.serializeArray());
            Sensor();
            return IARETREPLACE;
        },
        serializeArray: function () {
            var IARETREPLACE = this.map(function () {
                Sensor();
                var elements = jQuery.prop(this, 'elements');
                var IARETREPLACE = elements ? jQuery.makeArray(elements) : this;
                Sensor();
                return IARETREPLACE;
            }).filter(function () {
                Sensor();
                var type = this.type;
                var IARETREPLACE = this.name && !jQuery(this).is(':disabled') && rsubmittable.test(this.nodeName) && !rsubmitterTypes.test(type) && (this.checked || !rcheckableType.test(type));
                Sensor();
                return IARETREPLACE;
            }).map(function (i, elem) {
                Sensor();
                var val = jQuery(this).val();
                var IARETREPLACE = val == null ? null : jQuery.isArray(val) ? jQuery.map(val, function (val) {
                    var IARETREPLACE = {
                        name: elem.name,
                        value: val.replace(rCRLF, '\r\n')
                    };
                    Sensor();
                    return IARETREPLACE;
                }) : {
                    name: elem.name,
                    value: val.replace(rCRLF, '\r\n')
                };
                Sensor();
                return IARETREPLACE;
            }).get();
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.ajaxSettings.xhr = window.ActiveXObject !== undefined ? function () {
        var IARETREPLACE = !this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && createStandardXHR() || createActiveXHR();
        Sensor();
        return IARETREPLACE;
    } : createStandardXHR;
    var xhrId = 0, xhrCallbacks = {}, xhrSupported = jQuery.ajaxSettings.xhr();
    Sensor();
    if (window.ActiveXObject) {
        Sensor();
        jQuery(window).on('unload', function () {
            Sensor();
            for (var key in xhrCallbacks) {
                Sensor();
                xhrCallbacks[key](undefined, true);
            }
            Sensor();
        });
    }
    Sensor();
    support.cors = !!xhrSupported && 'withCredentials' in xhrSupported;
    xhrSupported = support.ajax = !!xhrSupported;
    Sensor();
    if (xhrSupported) {
        Sensor();
        jQuery.ajaxTransport(function (options) {
            Sensor();
            if (!options.crossDomain || support.cors) {
                Sensor();
                var callback;
                var IARETREPLACE = {
                    send: function (headers, complete) {
                        Sensor();
                        var i, xhr = options.xhr(), id = ++xhrId;
                        xhr.open(options.type, options.url, options.async, options.username, options.password);
                        Sensor();
                        if (options.xhrFields) {
                            Sensor();
                            for (i in options.xhrFields) {
                                Sensor();
                                xhr[i] = options.xhrFields[i];
                            }
                        }
                        Sensor();
                        if (options.mimeType && xhr.overrideMimeType) {
                            Sensor();
                            xhr.overrideMimeType(options.mimeType);
                        }
                        Sensor();
                        if (!options.crossDomain && !headers['X-Requested-With']) {
                            Sensor();
                            headers['X-Requested-With'] = 'XMLHttpRequest';
                        }
                        Sensor();
                        for (i in headers) {
                            Sensor();
                            if (headers[i] !== undefined) {
                                Sensor();
                                xhr.setRequestHeader(i, headers[i] + '');
                            }
                        }
                        Sensor();
                        xhr.send(options.hasContent && options.data || null);
                        callback = function (_, isAbort) {
                            Sensor();
                            var status, statusText, responses;
                            Sensor();
                            if (callback && (isAbort || xhr.readyState === 4)) {
                                Sensor();
                                delete xhrCallbacks[id];
                                callback = undefined;
                                xhr.onreadystatechange = jQuery.noop;
                                Sensor();
                                if (isAbort) {
                                    Sensor();
                                    if (xhr.readyState !== 4) {
                                        Sensor();
                                        xhr.abort();
                                    }
                                } else {
                                    Sensor();
                                    responses = {};
                                    status = xhr.status;
                                    Sensor();
                                    if (typeof xhr.responseText === 'string') {
                                        Sensor();
                                        responses.text = xhr.responseText;
                                    }
                                    Sensor();
                                    try {
                                        Sensor();
                                        statusText = xhr.statusText;
                                    } catch (e) {
                                        Sensor();
                                        statusText = '';
                                    }
                                    Sensor();
                                    if (!status && options.isLocal && !options.crossDomain) {
                                        Sensor();
                                        status = responses.text ? 200 : 404;
                                    } else {
                                        Sensor();
                                        if (status === 1223) {
                                            Sensor();
                                            status = 204;
                                        }
                                    }
                                }
                            }
                            Sensor();
                            if (responses) {
                                Sensor();
                                complete(status, statusText, responses, xhr.getAllResponseHeaders());
                            }
                            Sensor();
                        };
                        Sensor();
                        if (!options.async) {
                            Sensor();
                            callback();
                        } else {
                            Sensor();
                            if (xhr.readyState === 4) {
                                Sensor();
                                setTimeout(callback);
                            } else {
                                Sensor();
                                xhr.onreadystatechange = xhrCallbacks[id] = callback;
                            }
                        }
                        Sensor();
                    },
                    abort: function () {
                        Sensor();
                        if (callback) {
                            Sensor();
                            callback(undefined, true);
                        }
                        Sensor();
                    }
                };
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
        });
    }
    function createStandardXHR() {
        Sensor();
        try {
            var IARETREPLACE = new window.XMLHttpRequest();
            Sensor();
            return IARETREPLACE;
        } catch (e) {
            Sensor();
        }
        Sensor();
    }
    function createActiveXHR() {
        Sensor();
        try {
            var IARETREPLACE = new window.ActiveXObject('Microsoft.XMLHTTP');
            Sensor();
            return IARETREPLACE;
        } catch (e) {
            Sensor();
        }
        Sensor();
    }
    Sensor();
    jQuery.ajaxSetup({
        accepts: { script: 'text/javascript, application/javascript, application/ecmascript, application/x-ecmascript' },
        contents: { script: /(?:java|ecma)script/ },
        converters: {
            'text script': function (text) {
                Sensor();
                jQuery.globalEval(text);
                var IARETREPLACE = text;
                Sensor();
                return IARETREPLACE;
            }
        }
    });
    jQuery.ajaxPrefilter('script', function (s) {
        Sensor();
        if (s.cache === undefined) {
            Sensor();
            s.cache = false;
        }
        Sensor();
        if (s.crossDomain) {
            Sensor();
            s.type = 'GET';
            s.global = false;
        }
        Sensor();
    });
    jQuery.ajaxTransport('script', function (s) {
        Sensor();
        if (s.crossDomain) {
            Sensor();
            var script, head = document.head || jQuery('head')[0] || document.documentElement;
            var IARETREPLACE = {
                send: function (_, callback) {
                    Sensor();
                    script = document.createElement('script');
                    script.async = true;
                    Sensor();
                    if (s.scriptCharset) {
                        Sensor();
                        script.charset = s.scriptCharset;
                    }
                    Sensor();
                    script.src = s.url;
                    script.onload = script.onreadystatechange = function (_, isAbort) {
                        Sensor();
                        if (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {
                            Sensor();
                            script.onload = script.onreadystatechange = null;
                            Sensor();
                            if (script.parentNode) {
                                Sensor();
                                script.parentNode.removeChild(script);
                            }
                            Sensor();
                            script = null;
                            Sensor();
                            if (!isAbort) {
                                Sensor();
                                callback(200, 'success');
                            }
                        }
                        Sensor();
                    };
                    head.insertBefore(script, head.firstChild);
                    Sensor();
                },
                abort: function () {
                    Sensor();
                    if (script) {
                        Sensor();
                        script.onload(undefined, true);
                    }
                    Sensor();
                }
            };
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
    });
    var oldCallbacks = [], rjsonp = /(=)\?(?=&|$)|\?\?/;
    jQuery.ajaxSetup({
        jsonp: 'callback',
        jsonpCallback: function () {
            Sensor();
            var callback = oldCallbacks.pop() || jQuery.expando + '_' + nonce++;
            this[callback] = true;
            var IARETREPLACE = callback;
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.ajaxPrefilter('json jsonp', function (s, originalSettings, jqXHR) {
        Sensor();
        var callbackName, overwritten, responseContainer, jsonProp = s.jsonp !== false && (rjsonp.test(s.url) ? 'url' : typeof s.data === 'string' && !(s.contentType || '').indexOf('application/x-www-form-urlencoded') && rjsonp.test(s.data) && 'data');
        Sensor();
        if (jsonProp || s.dataTypes[0] === 'jsonp') {
            Sensor();
            callbackName = s.jsonpCallback = jQuery.isFunction(s.jsonpCallback) ? s.jsonpCallback() : s.jsonpCallback;
            Sensor();
            if (jsonProp) {
                Sensor();
                s[jsonProp] = s[jsonProp].replace(rjsonp, '$1' + callbackName);
            } else {
                Sensor();
                if (s.jsonp !== false) {
                    Sensor();
                    s.url += (rquery.test(s.url) ? '&' : '?') + s.jsonp + '=' + callbackName;
                }
            }
            Sensor();
            s.converters['script json'] = function () {
                Sensor();
                if (!responseContainer) {
                    Sensor();
                    jQuery.error(callbackName + ' was not called');
                }
                var IARETREPLACE = responseContainer[0];
                Sensor();
                return IARETREPLACE;
            };
            s.dataTypes[0] = 'json';
            overwritten = window[callbackName];
            window[callbackName] = function () {
                Sensor();
                responseContainer = arguments;
                Sensor();
            };
            jqXHR.always(function () {
                Sensor();
                window[callbackName] = overwritten;
                Sensor();
                if (s[callbackName]) {
                    Sensor();
                    s.jsonpCallback = originalSettings.jsonpCallback;
                    oldCallbacks.push(callbackName);
                }
                Sensor();
                if (responseContainer && jQuery.isFunction(overwritten)) {
                    Sensor();
                    overwritten(responseContainer[0]);
                }
                Sensor();
                responseContainer = overwritten = undefined;
                Sensor();
            });
            var IARETREPLACE = 'script';
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
    });
    jQuery.parseHTML = function (data, context, keepScripts) {
        Sensor();
        if (!data || typeof data !== 'string') {
            var IARETREPLACE = null;
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        if (typeof context === 'boolean') {
            Sensor();
            keepScripts = context;
            context = false;
        }
        Sensor();
        context = context || document;
        var parsed = rsingleTag.exec(data), scripts = !keepScripts && [];
        Sensor();
        if (parsed) {
            var IARETREPLACE = [context.createElement(parsed[1])];
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        parsed = jQuery.buildFragment([data], context, scripts);
        Sensor();
        if (scripts && scripts.length) {
            Sensor();
            jQuery(scripts).remove();
        }
        var IARETREPLACE = jQuery.merge([], parsed.childNodes);
        Sensor();
        return IARETREPLACE;
    };
    var _load = jQuery.fn.load;
    jQuery.fn.load = function (url, params, callback) {
        Sensor();
        if (typeof url !== 'string' && _load) {
            var IARETREPLACE = _load.apply(this, arguments);
            Sensor();
            return IARETREPLACE;
        }
        Sensor();
        var selector, response, type, self = this, off = url.indexOf(' ');
        Sensor();
        if (off >= 0) {
            Sensor();
            selector = url.slice(off, url.length);
            url = url.slice(0, off);
        }
        Sensor();
        if (jQuery.isFunction(params)) {
            Sensor();
            callback = params;
            params = undefined;
        } else {
            Sensor();
            if (params && typeof params === 'object') {
                Sensor();
                type = 'POST';
            }
        }
        Sensor();
        if (self.length > 0) {
            Sensor();
            jQuery.ajax({
                url: url,
                type: type,
                dataType: 'html',
                data: params
            }).done(function (responseText) {
                Sensor();
                response = arguments;
                self.html(selector ? jQuery('<div>').append(jQuery.parseHTML(responseText)).find(selector) : responseText);
                Sensor();
            }).complete(callback && function (jqXHR, status) {
                Sensor();
                self.each(callback, response || [
                    jqXHR.responseText,
                    status,
                    jqXHR
                ]);
                Sensor();
            });
        }
        var IARETREPLACE = this;
        Sensor();
        return IARETREPLACE;
    };
    jQuery.expr.filters.animated = function (elem) {
        var IARETREPLACE = jQuery.grep(jQuery.timers, function (fn) {
            var IARETREPLACE = elem === fn.elem;
            Sensor();
            return IARETREPLACE;
        }).length;
        Sensor();
        return IARETREPLACE;
    };
    var docElem = window.document.documentElement;
    function getWindow(elem) {
        var IARETREPLACE = jQuery.isWindow(elem) ? elem : elem.nodeType === 9 ? elem.defaultView || elem.parentWindow : false;
        Sensor();
        return IARETREPLACE;
    }
    jQuery.offset = {
        setOffset: function (elem, options, i) {
            Sensor();
            var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition, position = jQuery.css(elem, 'position'), curElem = jQuery(elem), props = {};
            Sensor();
            if (position === 'static') {
                Sensor();
                elem.style.position = 'relative';
            }
            Sensor();
            curOffset = curElem.offset();
            curCSSTop = jQuery.css(elem, 'top');
            curCSSLeft = jQuery.css(elem, 'left');
            calculatePosition = (position === 'absolute' || position === 'fixed') && jQuery.inArray('auto', [
                curCSSTop,
                curCSSLeft
            ]) > -1;
            Sensor();
            if (calculatePosition) {
                Sensor();
                curPosition = curElem.position();
                curTop = curPosition.top;
                curLeft = curPosition.left;
            } else {
                Sensor();
                curTop = parseFloat(curCSSTop) || 0;
                curLeft = parseFloat(curCSSLeft) || 0;
            }
            Sensor();
            if (jQuery.isFunction(options)) {
                Sensor();
                options = options.call(elem, i, curOffset);
            }
            Sensor();
            if (options.top != null) {
                Sensor();
                props.top = options.top - curOffset.top + curTop;
            }
            Sensor();
            if (options.left != null) {
                Sensor();
                props.left = options.left - curOffset.left + curLeft;
            }
            Sensor();
            if ('using' in options) {
                Sensor();
                options.using.call(elem, props);
            } else {
                Sensor();
                curElem.css(props);
            }
            Sensor();
        }
    };
    jQuery.fn.extend({
        offset: function (options) {
            Sensor();
            if (arguments.length) {
                var IARETREPLACE = options === undefined ? this : this.each(function (i) {
                    Sensor();
                    jQuery.offset.setOffset(this, options, i);
                    Sensor();
                });
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            var docElem, win, box = {
                    top: 0,
                    left: 0
                }, elem = this[0], doc = elem && elem.ownerDocument;
            Sensor();
            if (!doc) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            docElem = doc.documentElement;
            Sensor();
            if (!jQuery.contains(docElem, elem)) {
                var IARETREPLACE = box;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            if (typeof elem.getBoundingClientRect !== strundefined) {
                Sensor();
                box = elem.getBoundingClientRect();
            }
            Sensor();
            win = getWindow(doc);
            var IARETREPLACE = {
                top: box.top + (win.pageYOffset || docElem.scrollTop) - (docElem.clientTop || 0),
                left: box.left + (win.pageXOffset || docElem.scrollLeft) - (docElem.clientLeft || 0)
            };
            Sensor();
            return IARETREPLACE;
        },
        position: function () {
            Sensor();
            if (!this[0]) {
                var IARETREPLACE;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
            var offsetParent, offset, parentOffset = {
                    top: 0,
                    left: 0
                }, elem = this[0];
            Sensor();
            if (jQuery.css(elem, 'position') === 'fixed') {
                Sensor();
                offset = elem.getBoundingClientRect();
            } else {
                Sensor();
                offsetParent = this.offsetParent();
                offset = this.offset();
                Sensor();
                if (!jQuery.nodeName(offsetParent[0], 'html')) {
                    Sensor();
                    parentOffset = offsetParent.offset();
                }
                Sensor();
                parentOffset.top += jQuery.css(offsetParent[0], 'borderTopWidth', true);
                parentOffset.left += jQuery.css(offsetParent[0], 'borderLeftWidth', true);
            }
            var IARETREPLACE = {
                top: offset.top - parentOffset.top - jQuery.css(elem, 'marginTop', true),
                left: offset.left - parentOffset.left - jQuery.css(elem, 'marginLeft', true)
            };
            Sensor();
            return IARETREPLACE;
        },
        offsetParent: function () {
            var IARETREPLACE = this.map(function () {
                Sensor();
                var offsetParent = this.offsetParent || docElem;
                Sensor();
                while (offsetParent && (!jQuery.nodeName(offsetParent, 'html') && jQuery.css(offsetParent, 'position') === 'static')) {
                    Sensor();
                    offsetParent = offsetParent.offsetParent;
                }
                var IARETREPLACE = offsetParent || docElem;
                Sensor();
                return IARETREPLACE;
            });
            Sensor();
            return IARETREPLACE;
        }
    });
    jQuery.each({
        scrollLeft: 'pageXOffset',
        scrollTop: 'pageYOffset'
    }, function (method, prop) {
        Sensor();
        var top = /Y/.test(prop);
        jQuery.fn[method] = function (val) {
            var IARETREPLACE = access(this, function (elem, method, val) {
                Sensor();
                var win = getWindow(elem);
                Sensor();
                if (val === undefined) {
                    var IARETREPLACE = win ? prop in win ? win[prop] : win.document.documentElement[method] : elem[method];
                    Sensor();
                    return IARETREPLACE;
                }
                Sensor();
                if (win) {
                    Sensor();
                    win.scrollTo(!top ? val : jQuery(win).scrollLeft(), top ? val : jQuery(win).scrollTop());
                } else {
                    Sensor();
                    elem[method] = val;
                }
                Sensor();
            }, method, val, arguments.length, null);
            Sensor();
            return IARETREPLACE;
        };
        Sensor();
    });
    jQuery.each([
        'top',
        'left'
    ], function (i, prop) {
        Sensor();
        jQuery.cssHooks[prop] = addGetHookIf(support.pixelPosition, function (elem, computed) {
            Sensor();
            if (computed) {
                Sensor();
                computed = curCSS(elem, prop);
                var IARETREPLACE = rnumnonpx.test(computed) ? jQuery(elem).position()[prop] + 'px' : computed;
                Sensor();
                return IARETREPLACE;
            }
            Sensor();
        });
        Sensor();
    });
    jQuery.each({
        Height: 'height',
        Width: 'width'
    }, function (name, type) {
        Sensor();
        jQuery.each({
            padding: 'inner' + name,
            content: type,
            '': 'outer' + name
        }, function (defaultExtra, funcName) {
            Sensor();
            jQuery.fn[funcName] = function (margin, value) {
                Sensor();
                var chainable = arguments.length && (defaultExtra || typeof margin !== 'boolean'), extra = defaultExtra || (margin === true || value === true ? 'margin' : 'border');
                var IARETREPLACE = access(this, function (elem, type, value) {
                    Sensor();
                    var doc;
                    Sensor();
                    if (jQuery.isWindow(elem)) {
                        var IARETREPLACE = elem.document.documentElement['client' + name];
                        Sensor();
                        return IARETREPLACE;
                    }
                    Sensor();
                    if (elem.nodeType === 9) {
                        Sensor();
                        doc = elem.documentElement;
                        var IARETREPLACE = Math.max(elem.body['scroll' + name], doc['scroll' + name], elem.body['offset' + name], doc['offset' + name], doc['client' + name]);
                        Sensor();
                        return IARETREPLACE;
                    }
                    var IARETREPLACE = value === undefined ? jQuery.css(elem, type, extra) : jQuery.style(elem, type, value, extra);
                    Sensor();
                    return IARETREPLACE;
                }, type, chainable ? margin : undefined, chainable, null);
                Sensor();
                return IARETREPLACE;
            };
            Sensor();
        });
        Sensor();
    });
    jQuery.fn.size = function () {
        var IARETREPLACE = this.length;
        Sensor();
        return IARETREPLACE;
    };
    jQuery.fn.andSelf = jQuery.fn.addBack;
    Sensor();
    if (typeof define === 'function' && define.amd) {
        Sensor();
        define('jquery', [], function () {
            var IARETREPLACE = jQuery;
            Sensor();
            return IARETREPLACE;
        });
    }
    Sensor();
    var _jQuery = window.jQuery, _$ = window.$;
    jQuery.noConflict = function (deep) {
        Sensor();
        if (window.$ === jQuery) {
            Sensor();
            window.$ = _$;
        }
        Sensor();
        if (deep && window.jQuery === jQuery) {
            Sensor();
            window.jQuery = _jQuery;
        }
        var IARETREPLACE = jQuery;
        Sensor();
        return IARETREPLACE;
    };
    Sensor();
    if (typeof noGlobal === strundefined) {
        Sensor();
        window.jQuery = window.$ = jQuery;
    }
    var IARETREPLACE = jQuery;
    Sensor();
    return IARETREPLACE;
}));