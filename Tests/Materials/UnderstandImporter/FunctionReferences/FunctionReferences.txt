Add
AddExportFunction
AddExternalModule
AddFunction
AddImplementedFunction
AddImportFunction
AddImportModule
AddModule
AddRange
Any
Append
CanonizePath
Clear
Close
Collect
Combine
ComputeHash
ContainsKey
CopyOf
Count
Create
DeleteDB
Description
Dispose
EnumerateEveryone
EnumerateFunctionsDefinedInFile
EnumerateModules
EnumerateWithoutDuplicates
Export
ExportsFunctions
ExtLibs
Find
FindModule
FindModuleByName
FirstOrDefault
FromString
GenerateExternalAssemblies
GenerateExternalFunctions
GenerateSimple
Get
GetBool
GetByte
GetDataFolder
GetEnumerator
GetExtension
GetFileName
GetFileNameWithoutExtension
GetFilePath
GetFullPath
GetFunction
GetMarker
GetName
GetObjectBytes
GetString
GetSubDirectory
GetSubDirectoryPath
GetTableIDDuplicate
GetTableIDNoDuplicate
GetTableIndexedDuplicates
GetTableIndexedNoDuplicates
GetUInt64
GetValueByKey
get
Plugin.cs 13 119 3324
	Count[Plugin.cs, 3502]
	Where[Plugin.cs, 3478]
(anon_method_1)
Plugin.cs 1 122 3447
get
Plugin.cs 13 182 5397
Demangle
Plugin.cs 34 56 1558
GenerateReports
Plugin.cs 21 330 11095
	Description[Plugin.cs, 11598]
	GenerateExternalAssemblies[Plugin.cs, 11467]
	GenerateExternalFunctions[Plugin.cs, 11679]
	GenerateSimple[Plugin.cs, 11890]
	ReviewReports[Plugin.cs, 12090]
	Update[Plugin.cs, 11524]
get
Plugin.cs 13 97 2809
Initialize
Plugin.cs 21 134 3889
	LoadFromStorage[Plugin.cs, 4054]
IsSettingsCorrect
Plugin.cs 21 223 6598
LoadFromStorage
Plugin.cs 14 365 12483
	Add[Plugin.cs, 13654]
	Clear[Plugin.cs, 12536]
	EnumerateModules[Plugin.cs, 12603]
	ExportsFunctions[Plugin.cs, 13395]
	ImportsFunctions[Plugin.cs, 12981]
	get[Plugin.cs, 12548]
LoadSettings
Plugin.cs 21 146 4312
get
Plugin.cs 13 108 3051
get
Plugin.cs 13 204 5891
Run
Plugin.cs 21 261 7743
	Add[Plugin.cs, 10377]
	AddRange[Plugin.cs, 9398]
	Clear[Plugin.cs, 7784]
	Count[Plugin.cs, 8017]
	Description[Plugin.cs, 7901]
	Export[Plugin.cs, 8701]
	ExtLibs[Plugin.cs, 9427]
	Import[Plugin.cs, 8947]
	IsElfFile[Plugin.cs, 8264]
	LastIndexOf[Plugin.cs, 8617]
	Select[Plugin.cs, 8736]
	EnumerateFiles[Plugin.cs, 7975]
	get[Plugin.cs, 7795]
	Substring[Plugin.cs, 8583]
	ToList[Plugin.cs, 8754]
	Update[Plugin.cs, 7822]
(anon_method_1)
Plugin.cs 1 279 8642
(anon_method_2)
Plugin.cs 1 284 8895
	Demangle[Plugin.cs, 9165]
	ToString[Plugin.cs, 9242]
(anon_method_3)
Plugin.cs 1 293 9357
(anon_method_4)
Plugin.cs 1 305 10020
get
Plugin.cs 13 193 5641
SaveResults
Plugin.cs 21 320 10726
	SaveToStorage[Plugin.cs, 10827]
SaveSettings
Plugin.cs 21 214 6218
SaveToStorage
Plugin.cs 14 397 13837
	AddExportFunction[Plugin.cs, 14831]
	AddExternalModule[Plugin.cs, 15515]
	AddFunction[Plugin.cs, 14667]
	AddImplementedFunction[Plugin.cs, 14769]
	AddImportFunction[Plugin.cs, 16461]
	AddImportModule[Plugin.cs, 15611]
	AddModule[Plugin.cs, 14392]
	ExportsFunctions[Plugin.cs, 15750]
	FindModule[Plugin.cs, 14348]
	FindModuleByName[Plugin.cs, 15443]
	FirstOrDefault[Plugin.cs, 15487]
	get[Plugin.cs, 14048]
	get[Plugin.cs, 13939]
SetSimpleSettings
Plugin.cs 21 154 4615
get
Plugin.cs 13 171 5131
StopRunning
Plugin.cs 21 354 12248
get
Plugin.cs 13 234 6895
	Description[Plugin.cs, 7104]
get
Plugin.cs 13 249 7353
	Description[Plugin.cs, 7557]
Import
ImportsFunctions
IndexOf
InformLoaded
IsDBExists
IsElfFile
IsEmpty
IsExists
IsNullOrEmpty
IsNullOrWhiteSpace
LastIndexOf
Lock
MoveNext
Open
Parse
PutWithoutRepetition
ReadBlock
ReadLine
Remove
ReviewReports
Save
Select
SequenceEqual
SetFileListPath
SetNameWithCanonize
ShortTypeDescription
StartsWith
AcqireLabels
File.cs 22 421 14492
	FileLabels[File.cs, 14581]
get
File.cs 13 341 11863
	Any[File.cs, 11919]
	Close[File.cs, 12441]
	ComputeHash[File.cs, 12337]
	Create[File.cs, 12277]
	Open[File.cs, 12046]
	get[File.cs, 11984]
	get[File.cs, 11882]
	set[File.cs, 12377]
(anon_method_1)
File.cs 1 343 11883
ConvertLineColumnToOffset
File.cs 23 400 13940
	Get[File.cs, 14057]
	get[File.cs, 14005]
CopyOf
File.cs 22 358 12529
	get[File.cs, 12549]
get
File.cs 13 267 9872
	GetExtension[File.cs, 9936]
	IsNullOrEmpty[File.cs, 10006]
	get[File.cs, 9891]
	Substring[File.cs, 10058]
File_Impl
File.cs 18 188 7253
get
File.cs 13 309 10926
	AcqireLabels[File.cs, 10962]
	get[File.cs, 10978]
set
File.cs 13 314 11052
	AcqireLabels[File.cs, 11088]
	set[File.cs, 11104]
get
File.cs 13 323 11249
	AcqireLabels[File.cs, 11285]
	get[File.cs, 11301]
set
File.cs 13 328 11375
	AcqireLabels[File.cs, 11411]
	get[File.cs, 11569]
	get[File.cs, 11429]
	set[File.cs, 11732]
get
File.cs 13 254 9592
	get[File.cs, 9612]
get
File.cs 13 295 10613
	AcqireLabels[File.cs, 10649]
	get[File.cs, 10665]
set
File.cs 13 300 10734
	AcqireLabels[File.cs, 10770]
	set[File.cs, 10786]
get
File.cs 13 233 9063
get
File.cs 13 198 7435
	Combine[File.cs, 7837]
	GetName[File.cs, 7509]
	get[File.cs, 7537]
	get[File.cs, 7690]
set
File.cs 13 208 7945
	GetName[File.cs, 8026]
	SetNameWithCanonize[File.cs, 8919]
	StartsWith[File.cs, 8542]
	get[File.cs, 8244]
	CanonizeFileName[File.cs, 8518]
	get[File.cs, 8385]
	Substring[File.cs, 8600]
FunctionsDefinedInTheFile
File.cs 39 387 13358
	EnumerateFunctionsDefinedInFile[File.cs, 13505]
	GetFunction[File.cs, 13584]
	get[File.cs, 13397]
GetCopies
File.cs 35 363 12632
	GetValueByKey[File.cs, 12744]
	Select[File.cs, 12762]
	CheckCopiesFound[File.cs, 12677]
(anon_method_1)
File.cs 1 367 12700
getMarkerFileType
File.cs 78 437 14903
	AcqireLabels[File.cs, 14947]
getMarkerNaming
File.cs 25 432 14755
get
File.cs 13 409 14195
	AcqireLabels[File.cs, 14231]
	get[File.cs, 14247]
set
File.cs 13 414 14332
	AcqireLabels[File.cs, 14368]
	set[File.cs, 14384]
isPlacedInSources
File.cs 21 395 13810
	get[File.cs, 13841]
get
File.cs 13 279 10219
	GetFileName[File.cs, 10277]
	get[File.cs, 10238]
get
File.cs 13 287 10413
	GetFileNameWithoutExtension[File.cs, 10471]
	get[File.cs, 10432]
get
File.cs 13 246 9404
	get[File.cs, 9423]
	CanonizeFileName[File.cs, 9453]
get
File.cs 13 238 9211
	GetName[File.cs, 9274]
RemoveMe
File.cs 21 372 12834
	Remove[File.cs, 12893]
(anon_method_1)
File.cs 1 374 12857
(anon_method_2)
File.cs 1 375 12956
ToString
File.cs 32 427 14655
	get[File.cs, 14677]
get
FileLabels.cs 13 185 8801
set
FileLabels.cs 13 189 8886
	Save[FileLabels.cs, 8960]
get
FileLabels.cs 13 198 9049
	CheckCopiesFound[FileLabels.cs, 9094]
	GetFile[FileLabels.cs, 9271]
set
FileLabels.cs 13 207 9319
	Save[FileLabels.cs, 9394]
get
FileLabels.cs 13 152 8063
set
FileLabels.cs 13 153 8107
	Save[FileLabels.cs, 8186]
get
FileLabels.cs 13 162 8292
set
FileLabels.cs 13 163 8338
	Save[FileLabels.cs, 8417]
FileLabels
FileLabels.cs 18 129 7475
	GetMarker[FileLabels.cs, 7696]
	Load[FileLabels.cs, 7722]
get
FileLabels.cs 13 142 7842
set
FileLabels.cs 13 143 7883
	Save[FileLabels.cs, 7957]
GetLoadErrorString
FileLabels.cs 35 246 10721
get
FileLabels.cs 13 172 8523
set
FileLabels.cs 13 176 8621
	Save[FileLabels.cs, 8709]
Load
FileLabels.cs 33 234 10178
	FromString[FileLabels.cs, 10253]
	GetBool[FileLabels.cs, 10430]
	GetByte[FileLabels.cs, 10341]
	GetObjectBytes[FileLabels.cs, 10549]
	GetString[FileLabels.cs, 10271]
	GetUInt64[FileLabels.cs, 10502]
	InformLoaded[FileLabels.cs, 10601]
(anon_method_1)
FileLabels.cs 1 243 10582
SaveIndexes
FileLabels.cs 33 218 9518
	UpdateIndex[FileLabels.cs, 9580]
(anon_method_1)
FileLabels.cs 1 220 9544
(anon_method_2)
FileLabels.cs 1 221 9660
SaveToBuffer
FileLabels.cs 33 224 9789
	Add[FileLabels.cs, 9867]
	ShortTypeDescription[FileLabels.cs, 9883]
Add
Files.cs 22 144 6210
	Add[Files.cs, 6286]
	resetCheckCopiesFoundWorked[Files.cs, 6613]
Equals
Files.cs 25 324 13711
	SequenceEqual[Files.cs, 13922]
GetHashCode
Files.cs 24 332 13983
	Sum[Files.cs, 14145]
(anon_method_1)
Files.cs 1 336 14118
CanonizeFileName
Files.cs 30 125 5557
	CanonizePath[Files.cs, 5655]
CheckCopiesFound
Files.cs 21 343 14344
	Clear[Files.cs, 14490]
	ContainsKey[Files.cs, 14747]
	PutWithoutRepetition[Files.cs, 14970]
	set[Files.cs, 14862]
	EnumerateFiles[Files.cs, 14649]
	get[Files.cs, 14374]
Clear
Files.cs 21 163 7082
	Clear[Files.cs, 7125]
	resetCheckCopiesFoundWorked[Files.cs, 7291]
Count
Files.cs 23 177 7514
	Count[Files.cs, 7566]
Dump
Files.cs 21 303 12778
	Combine[Files.cs, 12899]
	EnumerateFiles[Files.cs, 13034]
	ToString[Files.cs, 13250]
	WriteLine[Files.cs, 12965]
EnumerateFiles
Files.cs 35 186 7771
	EnumerateEveryone[Files.cs, 7867]
	get[Files.cs, 7888]
EnumerateIndex_tableNames
Files.cs 106 88 3629
	Add[Files.cs, 3814]
	EnumerateFiles[Files.cs, 3742]
EnumerateItemsForFileTypeIndex
Files.cs 106 106 4411
	Add[Files.cs, 4601]
	ShortTypeDescription[Files.cs, 4619]
	getMarkerFileType[Files.cs, 4824]
	EnumerateFiles[Files.cs, 4529]
EnumerateUniqFiles
Files.cs 35 198 8518
	EnumerateFiles[Files.cs, 8585]
	Where[Files.cs, 8606]
(anon_method_1)
Files.cs 1 200 8566
Files
Files.cs 18 74 2622
(anon_method_1)
Files.cs 1 79 2784
Find
Files.cs 22 226 9644
	Find[Files.cs, 10208]
	Get[Files.cs, 11797]
	GetEnumerator[Files.cs, 10227]
	GetFullPath[Files.cs, 11242]
	IsNullOrWhiteSpace[Files.cs, 11125]
	MoveNext[Files.cs, 10267]
	StartsWith[Files.cs, 9973]
	CanonizeFileName[Files.cs, 9777]
	get[Files.cs, 9831]
	Substring[Files.cs, 10069]
FindOrGenerate
Files.cs 22 286 12325
	Add[Files.cs, 12508]
	Find[Files.cs, 12420]
GetFile
Files.cs 22 135 5901
	Get[Files.cs, 5970]
resetCheckCopiesFoundWorked
Files.cs 22 369 15483
	get[Files.cs, 15524]
UsedFileTypes
Files.cs 27 207 8888
	Add[Files.cs, 9198]
	EnumerateWithoutDuplicates[Files.cs, 9062]
	GetString[Files.cs, 9162]
	ToArray[Files.cs, 9251]
get
File.cs 11 134 4982
ConvertLineColumnToOffset
File.cs 16 175 6910
CopyOf
File.cs 15 143 5552
get
File.cs 13 89 3557
get
File.cs 13 117 4430
set
File.cs 13 118 4448
get
File.cs 13 126 4826
set
File.cs 13 127 4844
get
File.cs 13 54 2432
get
File.cs 13 108 4149
set
File.cs 13 109 4167
get
File.cs 13 43 1972
get
File.cs 13 26 1065
set
File.cs 13 27 1083
FunctionsDefinedInTheFile
File.cs 32 161 6259
GetCopies
File.cs 28 150 5916
isPlacedInSources
File.cs 14 168 6673
get
File.cs 35 95 3719
get
File.cs 39 100 3876
get
File.cs 13 72 2922
get
File.cs 13 63 2680
RemoveMe
File.cs 14 155 6049
ToString
File.cs 16 79 3245
get
Storage.cs 13 554 21106
get
Storage.cs 13 351 14186
Close
Storage.cs 21 201 8972
	Close[Storage.cs, 9406]
	Collect[Storage.cs, 9144]
	Dispose[Storage.cs, 9559]
	NullEverything[Storage.cs, 9585]
	set[Storage.cs, 9039]
	Unlock[Storage.cs, 9677]
	WaitForPendingFinalizers[Storage.cs, 9191]
DeleteDB
Storage.cs 23 679 25385
	Close[Storage.cs, 25515]
	DeleteDB[Storage.cs, 25540]
DeleteDB
Storage.cs 23 692 25933
	Close[Storage.cs, 26110]
	Collect[Storage.cs, 26064]
	DeleteDB[Storage.cs, 26135]
DeleteDB
Storage.cs 23 706 26637
	Close[Storage.cs, 26871]
	Collect[Storage.cs, 26821]
	DeleteDB[Storage.cs, 26900]
DeleteDB
Storage.cs 23 724 27448
	Close[Storage.cs, 27690]
	Collect[Storage.cs, 27640]
	DeleteDB[Storage.cs, 27719]
Dispose
Storage.cs 21 258 10590
	Close[Storage.cs, 10624]
	SuppressFinalize[Storage.cs, 10649]
get
Storage.cs 13 319 13230
	Files[Storage.cs, 13312]
get
Storage.cs 13 335 13703
get
Storage.cs 13 367 14668
Get_DB
Storage.cs 32 669 25127
GetSharedFilePath
Storage.cs 23 291 12147
	GetFilePath[Storage.cs, 12252]
GetTableIDDuplicate
Storage.cs 35 772 29189
	GetTableIDDuplicate[Storage.cs, 29274]
GetTableIDNoDuplicate
Storage.cs 37 746 28165
	GetTableIDNoDuplicate[Storage.cs, 28254]
GetTableIndexDuplicate
Storage.cs 41 798 30211
	GetTableIndexedDuplicates[Storage.cs, 30305]
GetTableIndexNoDuplicate
Storage.cs 43 825 31391
	GetTableIndexedNoDuplicates[Storage.cs, 31489]
GetTempDirectory
Storage.cs 30 277 11432
	Add[Storage.cs, 11742]
	GetDataFolder[Storage.cs, 11673]
	get[Storage.cs, 11605]
IsStorageExists
Storage.cs 28 301 12547
	Combine[Storage.cs, 12736]
	GetSubDirectory[Storage.cs, 12758]
	IsExists[Storage.cs, 12712]
	get[Storage.cs, 12679]
locations
Storage.cs 26 383 15262
	Add[Storage.cs, 15646]
	TryGetValue[Storage.cs, 15509]
get
Storage.cs 13 403 15987
logs
Storage.cs 20 452 17502
	Add[Storage.cs, 17816]
	TryGetValue[Storage.cs, 17684]
get
Storage.cs 13 420 16487
get
Storage.cs 13 601 22743
NullEverything
Storage.cs 22 235 9876
Open
Storage.cs 21 95 3452
	Open[Storage.cs, 3527]
	set[Storage.cs, 3509]
Open
Storage.cs 21 105 3867
	Append[Storage.cs, 5920]
	Combine[Storage.cs, 7410]
	Create[Storage.cs, 4517]
	GetSubDirectory[Storage.cs, 4282]
	GetSubDirectoryPath[Storage.cs, 7951]
	IsEmpty[Storage.cs, 7924]
	Lock[Storage.cs, 7507]
	Open[Storage.cs, 7213]
	ReadBlock[Storage.cs, 5742]
	ReadLine[Storage.cs, 6244]
	SetFileListPath[Storage.cs, 7887]
	get[Storage.cs, 7858]
	get[Storage.cs, 4229]
	set[Storage.cs, 4126]
	ToString[Storage.cs, 6563]
	ToUInt32[Storage.cs, 6231]
	WriteLine[Storage.cs, 4874]
get
Storage.cs 13 586 22394
get
Storage.cs 13 505 19656
get
Storage.cs 13 489 19013
get
Storage.cs 13 474 18356
get
Storage.cs 13 522 20141
get
Storage.cs 13 570 21760
Storage
Storage.cs 16 75 2399
get
Storage.cs 13 538 20607
TryOpenTableIDDuplicates
Storage.cs 35 785 29718
	IsDBExists[Storage.cs, 29787]
	GetTableIDDuplicate[Storage.cs, 29829]
TryOpenTableIDNoDuplicate
Storage.cs 37 759 28704
	IsDBExists[Storage.cs, 28774]
	GetTableIDNoDuplicate[Storage.cs, 28816]
TryOpenTableIndexedDuplicates
Storage.cs 41 811 30767
	IsDBExists[Storage.cs, 30841]
	GetTableIndexDuplicate[Storage.cs, 30883]
TryOpenTableIndexedNoDuplicates
Storage.cs 43 839 32076
	IsDBExists[Storage.cs, 32152]
	GetTableIndexNoDuplicate[Storage.cs, 32194]
get
Storage.cs 13 436 16968
get
Storage.cs 46 70 2242
set
Storage.cs 59 70 2255
~Storage
Storage.cs 9 83 2760
Substring
Sum
SuppressFinalize
ToArray
ToList
ToString
ToUInt32
TryGetValue
Unlock
Update
UpdateIndex
WaitForPendingFinalizers
Where
WriteLine
