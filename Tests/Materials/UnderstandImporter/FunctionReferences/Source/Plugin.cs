using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;

using Store;
using Store.Table;

using IA.Extensions;
using System.Runtime.InteropServices;

namespace IA.Plugins.Analyses.BinariesReader
{
    /// <summary>
    /// Статический класс плагина
    /// </summary>
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Чтение разделов импорта-экспорта бинарных файлов";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.BINARIES_READER;

        /// <summary>
        /// Наименования задач плагина при выполнении
        /// </summary>
        internal enum Tasks
        {
            [Description("Обработка списков импорта/экспорта")]
            IMPORT_EXPORT_LIST_PROCESSING
        };

        /// <summary>
        /// Наименования задач плагина при генерации отчётов
        /// </summary>
        internal enum ReportTasks
        {
            [Description("Формирование отчётов")]
            REPORTS_GENERATION
        };
    }

    /// <summary>
    /// Основной класс плагина
    /// </summary>
    public class BinariesReader : IA.Plugin.Interface
    {
        [DllImport("ElfDemangle.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        extern static public int Demangle(string mangled, int options, StringBuilder sb, int len);
        
        /// <summary>
        /// Флаги декорирования имен функций в библиотеках
        /// </summary>
        public enum enOptions : int
        {
            //            DMGL_NO_OPTS = 0,
            DMGL_PARAMS = (1 << 0),
            DMGL_ANSI = (1 << 1),
            DMGL_JAVA = (1 << 2),
            DMGL_VERBOSE = (1 << 3),
            DMGL_TYPES = (1 << 4),
            DMGL_RET_POSTFIX = (1 << 5),
            DMGL_AUTO = (1 << 8),
            DMGL_GNU = (1 << 9),
            DMGL_LUCID = (1 << 10),
            DMGL_ARM = (1 << 11),
            DMGL_HP = (1 << 12),
            DMGL_EDG = (1 << 13),
            DMGL_GNU_V3 = (1 << 14),
            DMGL_GNAT = (1 << 15),
        }

        /// <summary>
        /// Текущее хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Список библиотек
        /// </summary>
        internal List<DllInfo> allDllInfo = new List<DllInfo>();
               
        #region PluginInterface Members

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                IA.Plugin.Capabilities res = Plugin.Capabilities.RERUN | Plugin.Capabilities.REPORTS;
                if (allDllInfo.Where(dll => dll.IsElf).Count() != 0)
                {
                    res |= Plugin.Capabilities.RESULT_WINDOW;
                }
                return res;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;
            
            //Получаем информацию о библиотеках из Хранилища
            LoadFromStorage();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return new ResultWindow(allDllInfo);
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = String.Empty;
            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.IMPORT_EXPORT_LIST_PROCESSING.Description())
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.ReportTasks.REPORTS_GENERATION.Description(), 4)
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            allDllInfo.Clear();

            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.IMPORT_EXPORT_LIST_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)storage.files.EnumerateFiles(enFileKind.fileWithPrefix).Count());
            
            ulong counter = 0;
            foreach (Store.IFile file in storage.files.EnumerateFiles(enFileKind.fileWithPrefix))
            {
                DllInfo dllInfo;

                if (ElfFormat.ElfFormat.IsElfFile(file.FullFileName_Original))
                {
                    dllInfo = new DllInfo()
                    {
                        FileName = file.FileNameForReports,
                        FullFileName = file.FullFileName_Original,
                        AssemblyName = file.FileNameForReports.Substring(file.FileNameForReports.LastIndexOf("\\") + 1),
                        ExpFunctions = ElfFormat.ElfFormat.Export(file.FullFileName_Original).Select(i => i[1]).ToList(),
                        IsExternal = false,
                        IsElf = true,
                    };
                    
                    dllInfo.ImpFunctions = new List<ImportedFunction>(ElfFormat.ElfFormat.Import(file.FullFileName_Original).Select(i =>
                        {
                            StringBuilder sb = new StringBuilder(8000);
                            if (Demangle(i[1], 0, sb, 8000) == 0)
                                i[1] = sb.ToString();

                            return new ImportedFunction("", i[1]);
                        }));

                    dllInfo.ImpFunctions.AddRange(ElfFormat.ElfFormat.ExtLibs(file.FullFileName_Original).Select(i => new ImportedFunction(i, "")));
                    
                    
                }
                else
                {
                    dllInfo = new DllInfo()
                    {
                        FileName = file.FileNameForReports,
                        FullFileName = file.FullFileName_Original,
                        AssemblyName = file.FileNameForReports.Substring(file.FileNameForReports.LastIndexOf("\\") + 1),
                        ExpFunctions = ImpExp.ImpExp.Export(file.FullFileName_Original),
                        ImpFunctions = ImpExp.ImpExp.Import(file.FullFileName_Original).Select(l => new ImportedFunction(l.Substring(0, l.IndexOf("\t")), l.Substring(l.IndexOf("\t") + 1))).ToList(),
                        IsExternal = false,
                        IsElf = false,
                    };
                }

                allDllInfo.Add(dllInfo);

                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.IMPORT_EXPORT_LIST_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
            //Сохраняем информацию о бибилиотеках в Хранилище
            SaveToStorage();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            //Проверка на разумность
            if (this.storage == null)
                throw new Exception("Хранилище не определено.");

            //Инициализируем генератор отчётов
            ReportsGenerator generator = new ReportsGenerator(reportsDirectoryPath, allDllInfo);

            generator.GenerateExternalAssemblies();
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.ReportTasks.REPORTS_GENERATION.Description(), Monitor.Tasks.Task.UpdateType.STAGE, 1);

            generator.GenerateExternalFunctions();
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.ReportTasks.REPORTS_GENERATION.Description(), Monitor.Tasks.Task.UpdateType.STAGE, 2);

            generator.GenerateSimple();
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.ReportTasks.REPORTS_GENERATION.Description(), Monitor.Tasks.Task.UpdateType.STAGE, 4);

            generator.ReviewReports();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }
                
        #endregion

        #region Additional Methods

        /// <summary>
        /// Загрузка информации о библиотеках из Хранилища
        /// </summary>
        void LoadFromStorage()
        {
            allDllInfo.Clear(); 

            foreach (Module module in storage.modules.EnumerateModules())
            {
                DllInfo dllInfo = new DllInfo()
                {
                    AssemblyName = module.Name,
                    FullFileName = module.File.FullFileName_Original,
                    FileName = module.File.FileNameForReports,
                };   

                foreach (ImportFunction importFunction in module.ImportsFunctions())
                    if (importFunction.function.Name != String.Empty)
                        dllInfo.ImpFunctions.Add(new ImportedFunction(importFunction.function.ImplementedInModule.AssemblyStrongName + "\t" + 
                            importFunction.function.ImplementedInModule.Name, importFunction.function.Name));

                foreach (ExportFunction exportFunction in module.ExportsFunctions())
                    dllInfo.ExpFunctions.Add(exportFunction.function.Name);

                if (module.Kind != enModuleKinds.EXTERNAL)
                {
                    dllInfo.IsExternal = false;
                    allDllInfo.Add(dllInfo);
                }
            }
        }

        /// <summary>
        /// Сохранение информации о библиотеках в Хранилище
        /// </summary>
        void SaveToStorage()
        {
            //получаем существующие модули и интерфейс для управления ими
            Modules modules = storage.modules;

            //получаем интерфейс для управления функциями
            Functions functions = storage.functions;

            //строим списки экспорта
            foreach (DllInfo dllInfo in allDllInfo)
            {
                Module module;

                //добавляем новый модуль, если такового еще не было 
                module = modules.FindModule(dllInfo.FullFileName) ?? modules.AddModule(dllInfo.FullFileName, enFileKindForAdd.fileWithPrefix);

                //выписываем все экспортируемые функции
                foreach (string exportedFunctionName in dllInfo.ExpFunctions)
                {
                    IFunction function = functions.AddFunction();
                    function.Name = exportedFunctionName;
                    module.AddImplementedFunction(function);
                    module.AddExportFunction(function, 0);
                }
            }

            //строим списки импорта
            foreach (DllInfo dllInfo in allDllInfo)
            {
                Module module;
                bool found = false;

                //добавляем новый модуль, если такового еще не было 
                module = modules.FindModule(dllInfo.FullFileName) ?? modules.AddModule(dllInfo.FullFileName, enFileKindForAdd.fileWithPrefix);

                foreach (ImportedFunction importedFunction in dllInfo.ImpFunctions)
                {
                    Module extModule = modules.FindModuleByName(importedFunction.Assembly).FirstOrDefault() ?? modules.AddExternalModule(importedFunction.Assembly);
                    
                    module.AddImportModule(extModule);

                    found = false;
                    foreach (ExportFunction exportFunction in extModule.ExportsFunctions())
                        if (exportFunction.function.Name == importedFunction.Name)
                        {
                            module.AddImportFunction(exportFunction.function);
                            found = true;
                            break;
                        }

                    if (!found)
                    {
                        IFunction function = functions.AddFunction();
                        function.Name = importedFunction.Name;

                        extModule.AddImplementedFunction(function);
                        extModule.AddExportFunction(function, Store.Const.CONSTS.WrongUInt32);

                        module.AddImportFunction(function);
                    }
                }
            }
        }

        #endregion
    }
}
