﻿using System;
using System.Collections.Generic;
using System.Text;
using Store.BaseDatatypes;
using System.Linq;

namespace Store
{
    /// <summary>
    /// Раздел Хранилища "Файлы"
    /// Подробнее см. <a href="fc4dfd8c-999c-44fb-96f0-6316992c8871.htm">Раздел Хранилища "Файлы"</a>.
    /// </summary>
    public interface IFile : IDataObjectLeader
    {
        /// <summary>
        /// Полное имя файла, включая путь и расширение.
        /// Имя берется как есть, как оно записано в файловой системе. Оригинальные полные имена могут быть различными 
        /// для разных файлов. Например, файловая система не зависит от регистра символов, но такая информация 
        /// в именах присутствует.
        /// 
        /// Присвоив этому полю значение можно задать имя файла. Изменять ранее присвоенное имя файла запрещено.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        string FullFileName_Original
        {
            get;
            set;
        }

        /// <summary>
        /// Полное имя файла, включая путь и расширение.
        /// Поле возвращает какнонизированный вариант полного имени. Такое имя уникально для каждого файла.
        /// 
        /// <remarks>
        /// Пути к файлам в Windows являются независимыми от регистра символов. Более того, полный путь к одному т тому же файлу может быть записан по-разному. 
        /// Данное свойство гарантирует возврат унифицированного имени файла вне зависимости от того, в каком виде оно пришло в Хранилище. 
        /// Для изменения значения следует изменять <see cref="Store.IFile.FullFileName_Original"/>.
        /// </remarks>
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        string FullFileName_Canonized
        {
            get;
        }

        /// <summary>
        /// Путь к файлу, который необходимо выводить в отчётах. Чаще всего этот путь является укороченным, однако однозначно указывающим на файл для использующего анализатор эксперта. 
        /// <remarks>
        /// Для изменения значения следует изменять <see cref="Store.IFile.FullFileName_Original"/>.
        /// </remarks>
        /// </summary>
        string FileNameForReports
        {
            get;

        }

        /// <summary>
        /// Относительный путь к файлу в "оригинальном" форматировании. Корень - папка, указанная в FillFileList.
        /// </summary>
        string RelativeFileName_Original
        {
            get;
        }


        /// <summary>
        /// Относительный путь к файлу в канонизированном виде. Корень - папка, указанная в FillFileList.
        /// </summary>
        string RelativeFileName_Canonized
        { 
            get;
        }

        /// <summary>
        /// Возвращает значение <see cref="Store.IFile.FileNameForReports"/>. Используется для отображения имени файла в списках в графическом интерфейсе. Использовать для реализации логики не рекомендуется.
        /// </summary>
        /// <returns></returns>
        string ToString();

        /// <summary>
        /// Возвращает расширение файла без точки.
        /// <remarks>
        /// Для изменения значения следует изменять <see cref="Store.IFile.FullFileName_Original"/>.
        /// </remarks>
        /// </summary>
        string Extension
        {
            get;
        }

        /// <summary>
        /// Возвращает имя файла с расширением, но без пути.
        /// </summary>
        string NameAndExtension { get; }

        /// <summary>
        /// Возвращает имя файла, без расширения и без пути.
        /// </summary>
        string NameWithoutExtension { get; }

        /// <summary>
        /// Тип файла
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "file")]
        FileType fileType
        {
            get;
            set;
        }

        /// <summary>
        /// Результат анализа избыточности по файлам (если такой анализ был выполнен). Подробнее <see cref="Store.enFileEssential"/>.
        /// </summary>
        enFileEssential FileEssential
        {
            get;
            set;
        }

        /// <summary>
        /// Флаг присутствия файла в комплекте исходных текстов, к которому был применен плагин FillFileList. Также может содержать информацию о том, изменяется ли файл при компиляции исходных текстов. Подробнее <see cref="Store.enFileExistence"/>.
        /// </summary>
        enFileExistence FileExistence
        {
            get;
            set;
        }

        /// <summary>
        /// Контрольная сумма файла
        /// </summary>
        byte[] CheckSum
        { get; }

        /// <summary>
        /// Все файлы разделяются на классы эквивалентности.Файлы принадлежат одному классу эквивалентности, если у них совпадают контрольные суммы. 
        /// В классе эксвивалентности ровно один файл считается оригиналом (выбирается случайно), а все остальные называются его копиями.
        /// 
        /// Реализация функции использует индекс, который ПОЛНОСТЬЮ перестраивается каждый раз при изменении состава файлов, что требует значительного времени. 
        /// </summary>
        /// <returns></returns>
        IFile CopyOf();

        /// <summary>
        /// Если данный файл для CopyOf выдаёт null и, одновременно, в его классе эквивалентности есть файлы, то данный метод выдаёт их список. 
        /// Иными словами данный метод выдаёт список всех копий в данном классе эквивалентности.
        /// </summary>
        /// <returns></returns>
        IEnumerable<IFile> GetCopies();

        /// <summary>
        /// Удаляет запись об этом файле из Хранилища. 
        /// </summary>
        void RemoveMe();

        /// <summary>
        /// Перечисляет все функции, определения которых находятся в данном файле.
        /// </summary>
        /// <returns></returns>
        IEnumerable<IFunction> FunctionsDefinedInTheFile();

        /// <summary>
        /// Возвращает true, если файл находится в папке, в которой помещаются исходные тексты (которая была указана пользователем в FillFileList)
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "is")]
        bool isPlacedInSources();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="firstSymbolLine"></param>
        /// <param name="firstSymbolColumn"></param>
        /// <returns></returns>
        UInt64 ConvertLineColumnToOffset(UInt64 line, UInt64 column);
    }

    internal class File_Impl : Store.BaseDatatypes.DataObjectLeader, IFile
    {
        Files theFiles;
        internal FileLabels labels;

        /// <summary>
        /// Маркер загрузки имени файла
        /// </summary>
        Naming naming;

        internal File_Impl (Files files)
        {
            theFiles = files;
        }

        #region File Members


        public string FullFileName_Original
        {
            get
            {
                string fileName = theFiles.tableNames.GetName(ref naming, theID);
                if (FileExistence != enFileExistence.EXTERNAL) //Если файл не является внешним, то он не хранит префикса. Добавляем.
                {
                    string prefix = theFiles.storage.appliedSettings.FileListPath ?? String.Empty;
                    fileName = System.IO.Path.Combine(prefix, fileName);
                }
                return fileName;
            }
            set
            {
                if(!string.IsNullOrEmpty(theFiles.tableNames.GetName(ref naming, theID))) //Изменять имена файлов в хранилище запрещено!
                    throw new Exception("Попытка сменить имя файла в Хранилище");
                    
                string Name = null;
                if (FileExistence != enFileExistence.EXTERNAL) //Если файл не является внешним, то надо отпилить префикс
                {
                    string prefix = theFiles.storage.appliedSettings.FileListPath ?? String.Empty;

                    if (Files.CanonizeFileName(value).StartsWith(prefix))
                        Name = value.Substring(prefix.Length);
                    else
                        throw new Store.Const.StorageException("От имени файла невозможно отрезать префикс: " + value);
                }
                else
                    Name = value;

                //Заносим имя
                theFiles.tableNames.SetNameWithCanonize(ref naming, theID, Name);
            }
        }

        public string FullFileName_Canonized
        {
            get { return Files.CanonizeFileName(FullFileName_Original); }
        }

        public string RelativeFileName_Original
        {
            get
            {
                return theFiles.tableNames.GetName(ref naming, theID);
            }
        }

        public string RelativeFileName_Canonized
        {
            get
            {
                return Files.CanonizeFileName(RelativeFileName_Original);
            }
        }

        public string FileNameForReports
        {
            get 
            {
                string res = RelativeFileName_Original;

                if (res == "")
                    res = "<имя файла не указано>";

                return res;
            }
        }

        public string Extension
        {
            get
            {
                string res = System.IO.Path.GetExtension(RelativeFileName_Original);
                if (!String.IsNullOrEmpty(res))
                    return res.Substring(1);
                else
                    return res;
            }
        }

        public string NameAndExtension
        {
            get
            {
                return System.IO.Path.GetFileName(RelativeFileName_Original);
            }
        }

        public string NameWithoutExtension
        {
            get
            {
                return System.IO.Path.GetFileNameWithoutExtension(RelativeFileName_Original);
            }
        }

        public FileType fileType
        {
            get
            {
                AcqireLabels();
                return labels.fileType;
            }
            set
            {
                AcqireLabels();
                labels.fileType = value;
            }
        }

        public enFileEssential FileEssential
        {
            get
            {
                AcqireLabels();
                return labels.FileEssential;
            }
            set
            {
                AcqireLabels();
                labels.FileEssential = value;
            }
        }

        public enFileExistence FileExistence
        {
            get
            {
                AcqireLabels();
                return labels.FileExistence;
            }
            set
            {
                AcqireLabels();

                if (labels.FileExistence == enFileExistence.EXTERNAL && value != enFileExistence.EXTERNAL) //Проверка бредовости изменения
                    throw new Store.Const.StorageException("Запрещено менять FileExistence для внешнего файла. Файл " + FullFileName_Original);
                
                labels.FileExistence = value;
            }
        }

        public byte[] CheckSum
        {
            get
            {
                if (labels.CheckSum.Any(a => a != 0))
                    return labels.CheckSum;

                System.IO.FileStream stream = System.IO.File.Open(FullFileName_Original, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);
                byte[] res;
                using(System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
                {
                    res = md5.ComputeHash(stream);
                }
                labels.CheckSum = res;
                stream.Close();
                return res;
            }
        }

        public IFile CopyOf()
        {
            return labels.CopyOf;
        }

        public IEnumerable<IFile> GetCopies()
        {
            theFiles.CheckCopiesFound();

            return theFiles.tableDublicates.GetValueByKey(Id).Select(a => theFiles.GetFile(a));
        }



        public void RemoveMe()
        {
            theFiles.tableFileTypes.Remove(buffer => buffer.Add(fileType.ShortTypeDescription()),
                                  buffer => buffer.Add(theID));

            theFiles.tableLabels.Remove(theID);
            theFiles.tableNames.Remove(theID);
            theFiles.tableGeneral.Remove(theID);

            theID = Store.Const.CONSTS.WrongID;
            theFiles = null;
            labels = null;
            naming = null;
        }

        public IEnumerable<IFunction> FunctionsDefinedInTheFile()
        {
            Functions functions = theFiles.storage.functions;
            foreach (UInt64 id in functions.EnumerateFunctionsDefinedInFile(this))
                yield return functions.GetFunction(id);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)")]
        public bool isPlacedInSources()
        {
            return FileExistence != enFileExistence.EXTERNAL;
        }

        public UInt64 ConvertLineColumnToOffset(UInt64 line, UInt64 column)
        {
            return theFiles.storage.locationsIndex.Get(this.theID, line, column);
        }

        #endregion

        internal bool isFileIndexedByLocations
        {
            get
            {
                AcqireLabels();
                return labels.isFileIndexedByLocations;
            }
            set
            {
                AcqireLabels();
                labels.isFileIndexedByLocations = value;
            }
        }

        private void AcqireLabels()
        {
            if (labels == null)
                labels = new FileLabels(theFiles, theID);
        }

        public override string ToString()
        {
            return FileNameForReports;
        }

        internal Naming getMarkerNaming()
        {
            return naming;
        }

        internal Marker_Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates getMarkerFileType()
        {
            AcqireLabels();
            return labels.marker;
        }


    }
}
