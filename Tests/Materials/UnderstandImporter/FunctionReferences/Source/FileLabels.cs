﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Store.BaseDatatypes;
using Store.Table;

namespace Store
{
    /// <summary>
    /// Описывает необходимость файла для компиляции.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "en"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1008:EnumsShouldHaveZeroValue")]
    public enum enFileEssential
    {
        /// <summary>
        /// Файл используется в процессе компиляции.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ESSENTIAL")]
        ESSENTIAL = 1,

        /// <summary>
        /// Файл не используется в процессе компиляции.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "SUPERFLUOUS")]
        SUPERFLUOUS = 2,

        /// <summary>
        /// Содержимое файла, бывшее в нем до начала компиляции, было уничтожено без прочтения и перезаписано. Т.е. был выполнен CreateFile до
        /// того, как этот файл был открыт.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "RESET")]
        RESET = 3,

        /// <summary>
        /// Распознавание не было выполнено.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "UNKNOWN")]
        UNKNOWN = 4,

        /// <summary>
        /// Файл может быть востребован для компиляции.
        /// Окончательное распознавание проведено не было.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "SUGGEST")]
        SUGGEST = 5
    }

    /// <summary>
    /// Флаги существования файла
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "en")]
    public enum enFileExistence
    {
        /// <summary>
        /// Файл существовал до начала компиляции в процессе компиляции не изменялся.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "UNCHANGED")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "EXISTS")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "AND")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        EXISTS_AND_UNCHANGED,

        /// <summary>
        /// Файл существовал до начала компиляции, но был изменен в процессе компиляции.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "EXISTS")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CHANGED")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "AND")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        EXISTS_AND_CHANGED,

        /// <summary>
        /// Файл существовал до начала компиляции, и не распознавалось, был ли файл изменен.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "UNKNOWN")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "EXISTS")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "AND")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        EXISTS_AND_UNKNOWN,

        /// <summary>
        /// Файл был создан в процессе компиляции и после компиляции удален не был.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "UNDELETED")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CREATED")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "AND")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        CREATED_AND_UNDELETED,

        /// <summary>
        /// Файл был создан в процессе компиляции и не распознано, был ли файл удален.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "UNKNOWN")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CREATED")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "AND")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        CREATED_AND_UNKNOWN,

        /// <summary>
        /// Файл был создан и удален в процессе компиляции.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "DELETED")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CREATED")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "AND")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        CREATED_AND_DELETED,

        /// <summary>
        /// Файл является внешним по отношению к исходным текстам. К примеру, файлы Microsoft .Net
        /// </summary>
        EXTERNAL
    }


    internal class FileLabels : AdditionalDataObject
    {
        FileType theFileType;
        internal Marker_Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates marker;

        enFileEssential theFileEssential = enFileEssential.UNKNOWN;
        enFileExistence theFileExistence = enFileExistence.EXISTS_AND_UNKNOWN;
        bool theFileIndexedByLocations = false;
        byte[] theCheckSum = {0,0,0,0,0,0,0,0,   0,0,0,0,0,0,0,0};
        UInt64 theCopyOf = Store.Const.CONSTS.WrongID;                                        
        
        Files theFiles;

        internal FileLabels(Files files, UInt64 Id)
            : base(files.tableLabels, Id)
        {
            this.theFiles = files;
            theFileType = SpecialFileTypes.UNKNOWN;
            marker = files.tableFileTypes.GetMarker();
            Load();
        }


        #region Interface realization
        public FileType fileType
        {
            get { return theFileType; }
            set
            {
                theFileType = value;
                Save();
            }
        }

        public enFileEssential FileEssential
        {
            get {return theFileEssential;}
            set
            {
                theFileEssential = value;
                Save();
            }
        }

        public enFileExistence FileExistence
        {
            get { return theFileExistence; }
            set
            {
                theFileExistence = value;
                Save();
            }
        }

        public bool isFileIndexedByLocations
        {
            get
            {
                return theFileIndexedByLocations;
            }
            set
            {
                theFileIndexedByLocations = value;
                Save();
            }
        }

        public byte[] CheckSum
        {
            get 
            {
                return theCheckSum;
            }
            set
            {
                theCheckSum = value;
                Save();
            }
        }

        public IFile CopyOf
        {
            get
            {
                theFiles.CheckCopiesFound();

                if (theCopyOf == Store.Const.CONSTS.WrongID)
                    return null;
                else
                    return theFiles.GetFile(theCopyOf);
            }
            set
            {
                theCopyOf = value.Id;
                Save();
            }
        }

        #endregion

        #region Save and Load

        protected override void SaveIndexes()
        {
            theFiles.tableFileTypes.UpdateIndex(marker, buffer => buffer.Add(theFileType.ShortTypeDescription()), 
                                                     buffer => buffer.Add(theId));
        }

        protected override void SaveToBuffer(Store.Table.IBufferWriter buffer)
        {
            buffer.Add(theFileType.ShortTypeDescription());
            buffer.Add((byte)theFileEssential);
            buffer.Add((byte)theFileExistence);
            buffer.Add(theFileIndexedByLocations);
            buffer.Add(theCopyOf);
            buffer.Add(theCheckSum);
        }

        protected override void Load(Store.Table.IBufferReader reader)
        {
            theFileType.FromString(reader.GetString());
            theFileEssential = (enFileEssential)reader.GetByte();
			theFileExistence = (enFileExistence)reader.GetByte();
            reader.GetBool(ref theFileIndexedByLocations);
            theCopyOf = reader.GetUInt64();
            theCheckSum = reader.GetObjectBytes();
            
            marker.InformLoaded(buffer => buffer.Add(theFileType.ShortTypeDescription()));
        }

        protected override string GetLoadErrorString()
        {
            return "Попытка загрузить FileLables с идентификатором %s. Такого в базе не существует";
        }
        #endregion

    }


}
