﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Store;
using Store.BaseDatatypes;
using Store.Table;
using System.Linq;

namespace Store
{
    /// <summary>
    /// Перечисление используется в параметрах функций для задания вида файлов
    /// </summary>
    public enum enFileKind
    {
        /// <summary>
        /// все файлы
        /// </summary>
        anyFile,  
        /// <summary>
        /// только файлы, находящиеся в папке с исходными текстами. Включая файлы, появляющиеся в процессе компиляции.
        /// </summary>
        fileWithPrefix, 
        /// <summary>
        /// //только внешние (виртуальные) файлы. то есть файлы, путь к которым не соотносится с префиксом.
        /// </summary>
        externalFile 
    }

    /// <summary>
    /// Перечисление используется в параметрах функций для задания вида файлов. Используется при создании новых файлов
    /// </summary>
    public enum enFileKindForAdd
    {
        fileWithPrefix, //только файлы, находящиеся в папке с исходными текстами. То есть находящиеся по пути, куда указывает префикс.
        externalFile //только внешние (виртуальные) файлы. то есть файлы, не присутствующие по пути, куда указывает префикс.
    }


    /// <summary>
    /// Реализует раздел Хранилища "Файлы".
    /// </summary>
    public sealed class Files
    {
        //Имена таблиц
        const string TableName_fileGeneral = "FileGeneral";
        const string TableName_fileNames = "FileNames";
        const string TableName_fileLabels = "FileLables";
        const string TableName_fileDublicates = "FileDublicates";
        const string TableName_fileTypes = "FileTypesIndex";
        const string TableName_fileNamesIndex = "FileNamesIndex";

        //Основные таблицы
        internal TableLeader<File_Impl> tableGeneral;
        internal TableObjectNamingWithIndex tableNames;
        internal Table_KeyId_ValueSaver_Nodublicate tableLabels;
        internal Table_KeyId_ValueId_Dublicates tableDublicates;

        //Индексные таблицы
        /// <summary>
        /// Перечень использованных типов
        /// ключ - идентификатор типа
        /// значение - идентификатор файла.
        /// </summary>
        internal Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates tableFileTypes;

        internal Storage storage;

        #region Generate Files
        /// <summary>
        /// Создавать экземпляры Files самостоятельно запрещено.
        /// </summary>
        /// <param name="storage"></param>
        internal Files(Storage storage) 
        {
            this.storage = storage;

            tableGeneral = new TableLeader<File_Impl>(storage, TableName_fileGeneral,
                                                            (kind) => { return new File_Impl(this); });
            tableNames = new TableObjectNamingWithIndex(storage, TableName_fileNames, TableName_fileNamesIndex,
                                                            EnumerateIndex_tableNames);
            tableLabels = new Table_KeyId_ValueSaver_Nodublicate(storage, TableName_fileLabels);
            tableDublicates = new Table_KeyId_ValueId_Dublicates(storage, TableName_fileDublicates);
            tableFileTypes = new Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates(storage, TableName_fileTypes, 
                                                                    EnumerateItemsForFileTypeIndex);
        }

        private IEnumerable<Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult> EnumerateIndex_tableNames(IBufferWriter key, IBufferWriter value)
        {
            foreach (IFile file in EnumerateFiles(enFileKind.anyFile))
            {
                key.Add(file.RelativeFileName_Canonized);
                value.Add(file.Id);

                yield return new Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult(key, value, null);
            }
        }


        /// <summary>
        /// Перечисление типов файлов. Используется при построении индекса по типам файлов.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private IEnumerable<Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult> EnumerateItemsForFileTypeIndex(IBufferWriter key, IBufferWriter value)
        {
            foreach (IFile file in EnumerateFiles(enFileKind.anyFile))
            {
                key.Add(file.fileType.ShortTypeDescription()); 
                value.Add(file.Id);
                yield return new Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult(key, value, (file as File_Impl).getMarkerFileType());
            }
        }

        #endregion

        #region Interface
        /// <summary>
        /// Иногда требуется выполнить унификацию имени файла не создавая объект в хранилище. Данный статический метод позволяет унифицировать имя файла независимо от свойства FullFileName_Canonized, но по тому же алгоритму.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0")]
        public static string CanonizeFileName(string name)
        {
            return RNT.IOController.DirectoryController.CanonizePath(name);
        }

        /// <summary>
        /// Получить описание имени файла по его идентификатору.
        /// </summary>
        /// <param name="objectId"></param>
        /// <returns></returns>
        public IFile GetFile(UInt64 objectId)
        {
            return tableGeneral.Get(objectId);
        }

        /// <summary>
        /// Добавить новый описатель файла в Хранилище. После добавления необходимо задать полное имя файла.
        /// </summary>
        /// <returns></returns>
        public IFile Add(enFileKindForAdd kind)
        {
            IFile res = tableGeneral.Add();
            
            if(kind == enFileKindForAdd.externalFile) //Если надо, задаём файл как внешний. В противном случае ничего делать не надо - по умолчанию файлы внутренние.
                res.FileExistence = enFileExistence.EXTERNAL;

            if(kind == enFileKindForAdd.fileWithPrefix)
                resetCheckCopiesFoundWorked();

            return res;
        }

        /// <summary>
        /// Удаляет из Хранилища все записи о файлах. 
        /// <remarks> 
        /// Использовать данную функцию имеет смысл только при перезапуске всех анализов. В остальных случаях весьма вероятна потеря целостности Хранилища из-за того, что другие разделы Хранилища ссылаются на раздел «Файлы».
        /// </remarks>
        /// </summary>
        public void Clear()
        {
            tableNames.Clear();
            tableLabels.Clear();
            tableFileTypes.Clear();
            tableDublicates.Clear();
            tableGeneral.Clear();
            resetCheckCopiesFoundWorked();
        }

        /// <summary>
        /// Возвращает количество записей о файлах, имеющееся в Хранилище.
        /// </summary>
        /// <returns></returns>
        public UInt64 Count()
        {
            return tableGeneral.Count();
        }

        /// <summary>
        /// Перечислить все имеющиеся в Хранилище описатели файлов
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IFile> EnumerateFiles(enFileKind kind)
        {
            foreach (File_Impl impl in tableGeneral.EnumerateEveryone())
                if(kind == enFileKind.anyFile || (kind == enFileKind.externalFile && impl.FileExistence == enFileExistence.EXTERNAL) || (kind == enFileKind.fileWithPrefix && impl.FileExistence != enFileExistence.EXTERNAL))
                    yield return impl;
        }

        /// <summary>
        /// Перечисляет все уникальные файлы. Файл является копией другого файла, если у них одинаковое имя и содержимое. Среди класса эквивалентности копий файлов ровно один является уникальным.
        /// </summary>
        /// <param name="kind"></param>
        /// <returns></returns>
        public IEnumerable<IFile> EnumerateUniqFiles(enFileKind kind)
        {
            return EnumerateFiles(kind).Where(a => a.CopyOf() == null);
        }

        /// <summary>
        /// Перечисляет все типы файлов, которые присвоены хотя бы одному файлу в Хранилище.
        /// При первом обращении перестраивается индексная таблица.
        /// </summary>
        public FileType[] UsedFileTypes()
        {
            List<FileType> list = new List<FileType>();

            foreach (KeyValuePair<IBufferReader, IBufferReader> pair in tableFileTypes.EnumerateWithoutDuplicates())
            {
                FileType type = new FileType(pair.Key.GetString());
                list.Add(type);
            }

            return list.ToArray();
        }


        /// <summary>
        /// Возвращает объект, описывающий файл с именем fileName. Если такого файла в БД не обнаружено, возвращает null.
        /// </summary>
        /// <param name="fileName">Имя файла. Может быть полным, может быть относительным. При поиске приводится к нижнему регистру.</param>
        /// <returns></returns>
        public IFile Find(string fileName)
        {
//RUSAKOV 11.02.2014. Fix: Не находятся файлы, присутствующие в хранилище 
            fileName = CanonizeFileName(fileName); //bugfix for Unix slash

            string prefix = storage.appliedSettings.FileListPath ?? String.Empty;
            string relativeName;
            if(fileName.StartsWith(prefix,StringComparison.OrdinalIgnoreCase))
                relativeName = fileName.Substring(prefix.Length);
            else
                relativeName = fileName;

            IEnumerator<UInt64> iter = tableNames.Find(relativeName).GetEnumerator();
            if (!iter.MoveNext())
            {
                //Есть 2 случая 
                //- 1 - если на вход было подано Глобальное окружение и оно было порезано длиной префикса
                //- 2 - если на вход было подано имя, содержащее точки в пути - например C:\1\..\file.cs
                //Пытаемся получить абсолютный путь. Обращаю внимание, что System.IO.Path.GetFullPath корректно работает даже в том случае, если файла нет на диске

//      RUS 15.07.2014 Если у нас имя глобального окружения оказалось в нижнем регистре, то мы сами дураки. Тем более не корректно, что потом выполняется привод в нижний регистр. Полностью не удаляю на всякий случай.
//                if (fullFileName == "глобальное окружение") //почти бесплатная обработка первого случая
//                    relativeName = "Глобальное окружение";

                if (String.IsNullOrWhiteSpace(relativeName))
                    return null;

                relativeName = System.IO.Path.GetFullPath(relativeName);

                //Ищем по новому имени файла
                iter = tableNames.Find(relativeName).GetEnumerator();

                if (!iter.MoveNext()) //Если все-таки ничего найти не удалось, возвращаем null
                    return null;
            }

//END RUSAKOV

            UInt64 Id = iter.Current;

            if (!iter.MoveNext())
            {
                if (Id == Store.Const.CONSTS.WrongID)
                    return null;
                else
                    return tableGeneral.Get(Id);
            }
            else
            {
                throw new Exception("Нарушение контроля целостности. В Хранилище находится два файла с одинаковым полным путем!");
                return null;
            }
        }

        /// <summary>
        /// Найти файл с указанным полным именем. Если записи о таком файле в Хранилище нет, то он будет добавлен.
        /// 
        /// </summary>
        /// <param name="fullFileName"></param>
        /// <returns></returns>
        public IFile FindOrGenerate(string fullFileName, enFileKindForAdd kind)
        {
            IFile res = Find(fullFileName);
            if (res == null)
            {
                res = Add(kind);
                res.FullFileName_Original = fullFileName;
            }

            return res;
        }

        #endregion
        /// <summary>
        /// 
        /// </summary>
        /// <param name="folder"></param>
        public void Dump(string folder)
        {
            using (System.IO.StreamWriter wr = new System.IO.StreamWriter(System.IO.Path.Combine(folder, "files.log")))
            {
                wr.WriteLine("Дамп файлов");

                foreach (IFile named in EnumerateFiles(enFileKind.anyFile))
                    wr.WriteLine(named.Id + "\t" +
                                    named.FileNameForReports + "\t" +
                                    named.FileEssential.ToString() + "\t" +
                                    named.FileExistence.ToString() + "\t" +
                                    named.fileType.ToString());
            }
        }


        /// <summary>
        /// Вспомогательный класс для последующей функции. Позвляет использовать хэш в качестве ключа для Dictonary
        /// </summary>
        private class ByteArrayComparer : IEqualityComparer<byte[]>
        {
            public bool Equals(byte[] left, byte[] right)
            {
                if (left == null || right == null)
                {
                    return left == right;
                }
                return left.SequenceEqual(right);
            }
            public int GetHashCode(byte[] key)
            {
                if (key == null)
                    throw new ArgumentNullException("key");
                return key.Sum(b => b);
            }
        }

        /// <summary>
        /// Проверяет, выполнялся ли поиск дубликатов. Если не выполнялся, то выполняет.
        /// </summary>
        public void CheckCopiesFound()
        {
            if (!storage.appliedSettings.isCheckCopiesFoundWorked)
            {
                tableDublicates.Clear();
                Dictionary<byte[], IFile> primaries = new Dictionary<byte[], IFile>(new ByteArrayComparer());
                foreach(IFile file in EnumerateFiles( enFileKind.fileWithPrefix))
                {
                    if (primaries.ContainsKey(file.CheckSum))
                    {
                        IFile orig = primaries[file.CheckSum];
                        (file as File_Impl).labels.CopyOf = orig;
                        tableDublicates.PutWithoutRepetition(orig.Id, file.Id);
                    }
                    else
                        primaries[file.CheckSum] = file;
                }

                storage.appliedSettings.isCheckCopiesFoundWorked = true;
            }
        }

        /// <summary>
        /// Данную функцию надо вызвать, если поиск дублей файлов надо повторять. То есть функция CheckCopiesFound будет запущена повторно.
        /// </summary>
        /// <returns></returns>
        private void resetCheckCopiesFoundWorked()
        {
            if (storage.appliedSettings.isCheckCopiesFoundWorked)
                storage.appliedSettings.isCheckCopiesFoundWorked = false;
        }
    }


}
