using System;
using System.Collections.Generic;
using Store.Table;

using IA.Extensions;
using RNT.IOController;

namespace Store
{

    internal delegate void CloseHandle();

    /// <summary>
    /// Головной класс хранилища. При любом сценарии использования библиотеки, работа начинается с создания экземпляра данного класса.
    /// 
    /// Данный класс выполняет открытие и закрытие базы данных Хранилища (функции <see cref="Store.Storage.Open(System.String)">Open</see>, 
    /// <see cref="Store.Storage.Create(System.String)">Create</see>, <see cref="Store.Storage.Close">Close</see>).
    /// </summary>
    public sealed class Storage : IDisposable
    {
        /// <summary>
        /// Режимы, в которых может открываться Хранилище
        /// </summary>
        public enum OpenMode
        {
            /// <summary>
            /// Создать Хранилище
            /// </summary>
            CREATE,
            /// <summary>
            /// Открыть Хранилище
            /// </summary>
            OPEN
        }

        public static readonly string objectName = "Хранилище";

        #region Открытие и закрытие

        private const UInt32 Version = 7;
        private const string VersionFileName = "version.version";
        private const string VersionTestString = "Версия хранилища ";
        private readonly UInt32 revision = UInt32.Parse(new AssemblyInfo().Version);

        #region Блокировка Хранилища
        /// <summary>
        /// Поток работы с файлом блокировки Хранилища
        /// </summary>
        private System.IO.FileStream lockFile = null;

        /// <summary>
        /// Имя файла блокировки Хранилища
        /// </summary>
        private const string LockFileName = "storage.lock";
        #endregion

        /// <summary>
        /// Состояние Хранилища: закрыто/открыто
        /// </summary>
        public bool isClosed = true;

        /// <summary>
        /// Низкоуровневый объект для работы с базой данных.
        /// </summary>
        internal DB_Operations DB = new DB_Operations();

        /// <summary>
        /// Рабочая директория
        /// </summary>
        public WorkDirectory WorkDirectory { get; private set; }

        /// <summary>
        /// Конструктор используется для создания экземпляра Хранилища
        /// </summary>
        public Storage()
        {
        }

        /// <summary>
        /// Деструктор хранилища
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1821:RemoveEmptyFinalizers"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations")]
        ~Storage()
        {
            if (!isClosed) //Выдать исключение, если Хранилище пытаются уничтожить, не закрыв его. В этом случае невозможно корректно сохранить 
                           //состояние объектов в базу данных.
                throw new Store.Const.StorageException("Перед завершением выполнения необходимо вызвать метод Storage.Close(). Данный метод не был вызван!");
        }

        /// <summary>
        /// Открыть Хранилище в рабочей директории по указанному пути.
        /// </summary>
        /// <param name="workDirectoryPath">Путь до рабочей директории.</param>
        /// <param name="mode">Режим открытия Хранилища.</param>
        public void Open(string workDirectoryPath, OpenMode mode)
        {
            this.Open(this.WorkDirectory = new WorkDirectory(workDirectoryPath), mode);
        }

        /// <summary>
        /// Открыть Хранилище в рабочей директории.
        /// </summary>
        /// <param name="workDirectory">Рабочая директория.</param>
        /// <param name="mode">Режим открытия Хранилища.</param>
        public void Open(WorkDirectory workDirectory, OpenMode mode)
        {
            //Проверка на разумность
            if (workDirectory == null)
                throw new Exception("Рабочая директория не определена.");

            //Запоминаем рабочую директори
            this.WorkDirectory = workDirectory;

            //Получаем путь к директории Хранилища
            string storageDirectory = workDirectory.GetSubDirectory(WorkDirectory.enSubDirectories.STORAGE);

            switch(mode)
            {
                case OpenMode.CREATE:
                    {
                        //Создаем Хранилище
                        DB.Create(this.WorkDirectory.GetSubDirectory(WorkDirectory.enSubDirectories.STORAGE));

                        //Запоминаем версию формата Хранилища
                        using (System.IO.StreamWriter ver = new System.IO.StreamWriter(System.IO.Path.Combine(storageDirectory, VersionFileName)))
                        {
                            ver.WriteLine(VersionTestString + Convert.ToString(Version, System.Globalization.CultureInfo.InvariantCulture));
                            ver.WriteLine("Ревизия IA " + Convert.ToString(revision, System.Globalization.CultureInfo.InvariantCulture));
                        }

                        break;
                    }
                case OpenMode.OPEN:
                    {
                        //Проверяем версию формата Хранилища
                        try
                        {
                            UInt32 storageVersion;

                            using (System.IO.StreamReader ver = new System.IO.StreamReader(System.IO.Path.Combine(storageDirectory, VersionFileName)))
                            {
                                char[] block = new char[VersionTestString.Length];
                                ver.ReadBlock(block, 0, VersionTestString.Length);
                                System.Text.StringBuilder b = new System.Text.StringBuilder();
                                b.Append(block);
                                string text = b.ToString();
                                if (text != VersionTestString)
                                    throw new Store.Const.StorageException("Формат хранилища не корректен ");

                                storageVersion = Convert.ToUInt32(ver.ReadLine(), System.Globalization.CultureInfo.InvariantCulture);
                            }

                            if (storageVersion != Version)
                                throw new Store.Const.StorageException("Попытка открыть Хранилище версии "
                                    + storageVersion.ToString(System.Globalization.CultureInfo.InvariantCulture)
                                    + " исполняемым файлом для версии "
                                    + Version.ToString(System.Globalization.CultureInfo.InvariantCulture));
                        }
                        catch (System.IO.FileNotFoundException)
                        {
                            throw new Store.Const.StorageException("Попытка открыть Хранилище, в котором отсутствует файл " + VersionFileName + ". Формат данного Хранилища устарел.");
                        }

                        //Открываем Хранилище
                        DB.Open(storageDirectory);

                        break;
                    }
            }

            //Блокируем Хранилище
            lockFile = new System.IO.FileStream(System.IO.Path.Combine(storageDirectory, LockFileName), System.IO.FileMode.OpenOrCreate);
            lockFile.Lock(0, 0);

            //Помечаем Хранилище как открытое
            isClosed = false;

            //Задаём список временных директорий для автоматического освобождения при закрытии
            tempDirectoriesForThisStorage = new List<TempDirectory>();

            //Прописываем префикс пути до директории с исходными текстами для анализа
            appliedSettings.SetFileListPath((DirectoryController.IsEmpty(this.WorkDirectory.GetSubDirectoryPath(Store.WorkDirectory.enSubDirectories.SOURCES_CLEAR)) ?
                                                       Store.WorkDirectory.enSubDirectories.SOURCES_ORIGINAL :
                                                       Store.WorkDirectory.enSubDirectories.SOURCES_CLEAR));
        }

        /// <summary>
        /// Это событие срабатывает при закрытие Хранилища. Позволяет сохранять несохраненные данные.
        /// </summary>
        internal event CloseHandle CloseEvent;

        /// <summary>
        /// Это событие срабатывает при закрытие Хранилища. Позволяет закрыть все открытые таблицы.
        /// </summary>
        event CloseHandle CloseEvent_System;

        /// <summary>
        /// Закрывает Хранилище. При этом сохраняются состояния всех открытых объектов.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2001:AvoidCallingProblematicMethods", MessageId = "System.GC.Collect")]
        public void Close()
        {
            //Рабочая директория не определена
            this.WorkDirectory = null;

            if (!isClosed)
            {
                GC.Collect(GC.MaxGeneration);
                GC.WaitForPendingFinalizers();

                if(CloseEvent != null)
                    CloseEvent();
                if (CloseEvent_System != null)
                    CloseEvent_System();
                DB.Close();
                isClosed = true;
            }

            foreach (var tempDir in tempDirectoriesForThisStorage)
                tempDir.Dispose();

            NullEverything();



            //Снимаем блокировку с Хранилища
            lockFile.Unlock(0, 0);
            lockFile.Close();
            lockFile = null;

            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();
        }

        private void NullEverything()
        {
            theClasses = null;
            theFiles = null;
            theFilesIndex = null;
            theFunctions = null;
            theLocations = null;
            theModules = null;
            theVariables = null;
            theLogs = null;
            thePluginSettings = null;
            thePluginData = null;
            theSensors = null;
            theTraces = null;
            theAppliedSettings = null;
            theOperations = null;
            theStatements = null;
            theNamespaces = null;
            tempDirectoriesForThisStorage = null;
        }
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Close();
            GC.SuppressFinalize(this);
        }

        #endregion

        /// <summary>
        /// Список временных директорий. Наполняется по мере вызова GetTempDirectory. При закрытии хранилища для каждого объекта в списке вызывается Dispose.
        /// </summary>
        List<TempDirectory> tempDirectoriesForThisStorage;

        /// <summary>
        /// Создание временного каталога в хранилище для заданного плагина. Существует пока хранилище открыто, при закрытии хранилища автоматически очищается.
        /// </summary>
        /// <param name="pluginId">Идентификатор плагина.</param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public TempDirectory GetTempDirectory(UInt64 pluginId)
        {
            if (pluginId == 0)
                throw new Exception("GetTempDirectory: неверно задан идентификатор плагина.");
            TempDirectory last = new TempDirectory(this.pluginData.GetDataFolder(pluginId));
            tempDirectoriesForThisStorage.Add(last);
            return last;
        }

        /// <summary>
        /// Полуить путь к файлу, который может использоваться несколькими плагинами.
        /// </summary>
        /// <param name="file">Хранилище должно знать обо всех файлах во избежание конфликта имён. На вход функция получает уникальный идентификатор файла.</param>
        /// <returns></returns>
        public string GetSharedFilePath(SharedFiles.enSharedFiles file)
        {
            return SharedFiles.SharedFiles.GetFilePath(this, file);
        }

        /// <summary>
        /// Проверить, существует ли Хранилище в указанной рабочей директории.
        /// </summary>
        /// <param name="workDirectory">Рабочая директория.</param>
        /// <returns></returns>
        public static bool IsStorageExists(WorkDirectory workDirectory)
        {            
            //Если в директории Хранилища нет файла блокировки
            if (!FileController.IsExists(System.IO.Path.Combine(workDirectory.GetSubDirectory(WorkDirectory.enSubDirectories.STORAGE), LockFileName)))
                return false;

            return true;
        }

        #region Раздача ссылок на разделы

        Files theFiles;
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "files")]
        public Files files
        {
            get
            {
                if (theFiles == null)
                    theFiles = new Files(this);

                return theFiles;
            }
        }

        FilesIndex theFilesIndex;
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "files")]
        public FilesIndex filesIndex
        {
            get
            {
                if (theFilesIndex == null)
                    theFilesIndex = new FilesIndex(this);

                return theFilesIndex;
            }
        }

        Classes theClasses;
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "classes")]
        public Classes classes
        {
            get 
            {
                if (theClasses == null)
                    theClasses = new Classes(this);

                return theClasses;
            }
        }

        Functions theFunctions;
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "functions")]
        public Functions functions
        {
            get
            {
                if (theFunctions == null)
                    theFunctions = new Functions(this);

                return theFunctions;
            }
        }

        Dictionary<string, Locations> theLocations = new Dictionary<string, Locations>(10);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "locations")]
        public Locations locations(string tableName)
        {
            if (theLocations == null)
            {
                theLocations = new Dictionary<string, Locations>(10);
            }
            Locations value = null;
            if (!theLocations.TryGetValue(tableName, out value))
            {
                value = new Locations(this, tableName);
                theLocations.Add(tableName, value);
            }

            return value;
        }

        LocationIndex theLocationsIndex;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "locations")]
        internal LocationIndex locationsIndex
        {
            get
            {
                if (theLocationsIndex == null)
                    theLocationsIndex = new LocationIndex(this);

                return theLocationsIndex;
            }

        }

        Modules theModules;
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "modules")]
        public Modules modules
        {
            get
            {
                if (theModules == null)
                    theModules = new Modules(this);

                return theModules;
            }
        }

        Variables theVariables;
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "variables")]
        public Variables variables
        {
            get
            {
                if (theVariables == null)
                    theVariables = new Variables(this);

                return theVariables;
            }
        }

        Dictionary<string, Log> theLogs;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="logFileName"></param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "logs")]
        public Log logs(string logFileName)
        {
            if (theLogs == null)
                theLogs = new Dictionary<string, Log>(5);

            Log result;
            if (!theLogs.TryGetValue(logFileName, out result))
            {
                result = new Log(this, logFileName);
                theLogs.Add(logFileName, result);
            }

            return result;
        }

        PluginSettings thePluginSettings;
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "plugin"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "plugin")]
        public PluginSettings pluginSettings
        {
            get
            {
                if (thePluginSettings == null)
                    thePluginSettings = new PluginSettings(this);

                return thePluginSettings;
            }
        }
        PluginResults thePluginResults;
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "plugin"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "plugin")]
        public PluginResults pluginResults
        {
            get
            {
                if (thePluginResults == null)
                    thePluginResults = new PluginResults(this);

                return thePluginResults;
            }
        }

        PluginData thePluginData;
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "plugin"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "plugin")]
        public PluginData pluginData
        {
            get
            {
                if (thePluginData == null)
                    thePluginData = new PluginData(this);

                return thePluginData;
            }
        }


        Sensors theSensors;
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "sensors")]
        public Sensors sensors
        {
            get
            {
                if (theSensors == null)
                    theSensors = new Sensors(this);

                return theSensors;
            }
        }

        Traces theTraces;
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "traces")]
        public Traces traces
        {
            get
            {
                if (theTraces == null)
                    theTraces = new Traces(this);

                return theTraces;
            }
        }

        AppliedSettings theAppliedSettings;
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "applied")]
        public AppliedSettings appliedSettings
        {
            get
            {
                if (theAppliedSettings == null)
                    theAppliedSettings = new AppliedSettings(this);

                return theAppliedSettings;
            }
        }

        Statements theStatements;
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "statements"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "sensors")]
        public Statements statements
        {
            get
            {
                if (theStatements == null)
                    theStatements = new Statements(this);

                return theStatements;
            }
        }

        Operations theOperations;
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "operations"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "sensors")]
        public Operations operations
        {
            get
            {
                if (theOperations == null)
                    theOperations = new Operations(this);

                return theOperations;
            }
        }

        Namespaces theNamespaces;
        /// <summary>
        /// 
        /// </summary>
        public Namespaces namespaces
        {
            get
            {
                if (theNamespaces == null)
                    theNamespaces = new Namespaces(this);

                return theNamespaces;
            }
        }

        #region Работа с серверными соединениями
        /*
        SQLTransitor.SQLTransitor theFileTypesSQLConnection;

        /// <summary>
        /// Возвращает соединение с серверной базой, содержащей типы файлов.
        /// </summary>
        public SQLTransitor.SQLTransitor FileTypesSqlConnection
        {
            get
            {
                try
                {
                    if (theFileTypesSQLConnection == null || SQLTransitor.SQLTransitor.GetConnStr(SQLTransitor.enDB.SignatureDB) != SQLTransitor.SQLTransitor.ConnectionString)
                        theFileTypesSQLConnection = new SQLTransitor.SQLTransitor(SQLTransitor.enDB.SignatureDB);
                }
                catch (Exception ex)
                {
                    IA.Monitor.Log.Warning(ex.
         * );
                }


                return theFileTypesSQLConnection;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public SQLTransitor.SQLTransitor DllSqlConnection
        {
            get
            {
                try
                {
                    if (theFileTypesSQLConnection == null || SQLTransitor.SQLTransitor.GetConnStr(SQLTransitor.enDB.DLLKnowledgeBase) != SQLTransitor.SQLTransitor.ConnectionString)
                        theFileTypesSQLConnection = new SQLTransitor.SQLTransitor(SQLTransitor.enDB.DLLKnowledgeBase);
                }
                catch (Exception ex)
                {
                    IA.Monitor.Log.Warning(ex.Message);
                }


                return theFileTypesSQLConnection;
            }
        }
        */
        #endregion

        #endregion

        #region Работа с таблицами на низком уровне
        /// <summary>
        /// Возвращает объект для низкойуровневой работы с БД.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        internal DB_Operations Get_DB()
        {
            return DB;
        }

        /// <summary>
        /// Удаляет таблицу из Хранилища.
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="table"></param>
        internal void DeleteDB(string TableName, TableIDNoDuplicate table)
        {
            CloseEvent_System -= table.Close;
            table.Close();
            DB.DeleteDB(TableName);
        }

        /// <summary>
        /// Удаляет таблицу из Хранилища.
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="table"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2001:AvoidCallingProblematicMethods", MessageId = "System.GC.Collect")]
        internal void DeleteDB(string TableName, TableIndexedDuplicates table)
        {
            CloseEvent_System -= table.Close;
            GC.Collect(GC.MaxGeneration);
            table.Close();
            DB.DeleteDB(TableName);
        }

        /// <summary>
        /// Удаляет таблицу из Хранилища.
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="table"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2001:AvoidCallingProblematicMethods", MessageId = "System.GC.Collect"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal void DeleteDB(string TableName, ref TableIDDuplicate table)
        {
            if (table != null)
            {
                CloseEvent_System -= table.Close;
                GC.Collect(GC.MaxGeneration);
                table.Close();
                DB.DeleteDB(TableName);
                table = null;
            }
        }

        /// <summary>
        /// Удаляет таблицу из Хранилища.
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="table"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2001:AvoidCallingProblematicMethods", MessageId = "System.GC.Collect"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal void DeleteDB(string TableName, ref TableIndexedNoDuplicates table)
        {
            if (table != null)
            {
                CloseEvent_System -= table.Close;
                GC.Collect(GC.MaxGeneration);
                table.Close();
                DB.DeleteDB(TableName);
                table = null;
            }
        }


        #endregion

        #region Работа с таблицами на высоком уровне

        /// <summary>
        /// Открывает таблицу типа TableIDNoDuplicate с указанным именем. Если такая таблица не существует, она будет создана.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal TableIDNoDuplicate GetTableIDNoDuplicate(string name)
        {
            TableIDNoDuplicate table = DB.GetTableIDNoDuplicate(name);
            CloseEvent_System += table.Close;
            return table;
        }

        /// <summary>
        /// Открывает таблицу типа TableIDNoDuplicate с указанным именем. 
        /// Если такая таблица не существует, она НЕ будет создана, результатом работы будет null.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal TableIDNoDuplicate TryOpenTableIDNoDuplicate(string name)
        {
            if (DB.IsDBExists(name))
                return GetTableIDNoDuplicate(name);
            else
                return null;
        }

        /// <summary>
        /// Открывает таблицу типа TableIDDuplicate с указанным именем. Если такая таблица не существует, она будет создана.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal TableIDDuplicate GetTableIDDuplicate(string name)
        {
            TableIDDuplicate table = DB.GetTableIDDuplicate(name);
            CloseEvent_System += table.Close;
            return table;
        }

        /// <summary>
        /// Открывает таблицу типа TableIDDuplicate с указанным именем. 
        /// Если такая таблица не существует, она НЕ будет создана, результатом работы будет null.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal TableIDDuplicate TryOpenTableIDDuplicates(string name)
        {
            if (DB.IsDBExists(name))
                return GetTableIDDuplicate(name);
            else
                return null;
        }

        /// <summary>
        /// Открывает таблицу типа TableIndexedDuplicates с указанным именем. Если такая таблица не существует, она будет создана.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal TableIndexedDuplicates GetTableIndexDuplicate(string name)
        {
            TableIndexedDuplicates table = DB.GetTableIndexedDuplicates(name);
            CloseEvent_System += table.Close;
            return table;
        }

        /// <summary>
        /// Открывает таблицу типа TableIndexedDuplicates с указанным именем. 
        /// Если такая таблица не существует, она НЕ будет создана, результатом работы будет null.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal TableIndexedDuplicates TryOpenTableIndexedDuplicates(string name)
        {
            if (DB.IsDBExists(name))
                return GetTableIndexDuplicate(name);
            else
                return null;
        }

        /// <summary>
        /// Открывает таблицу типа TableIndexedNoDuplicates с указанным именем. Если такая таблица не существует, она будет создана.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal TableIndexedNoDuplicates GetTableIndexNoDuplicate(string name)
        {
            TableIndexedNoDuplicates table = DB.GetTableIndexedNoDuplicates(name);
            CloseEvent_System += table.Close;
            return table;
        }

        /// <summary>
        /// Открывает таблицу типа TableIndexedNoDuplicates с указанным именем. 
        /// Если такая таблица не существует, она НЕ будет создана, результатом работы будет null.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal TableIndexedNoDuplicates TryOpenTableIndexedNoDuplicates(string name)
        {
            if (DB.IsDBExists(name))
                return GetTableIndexNoDuplicate(name);
            else
                return null;
        }

        #endregion


    }

}
