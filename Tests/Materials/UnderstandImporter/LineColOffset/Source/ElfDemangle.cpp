// ElfDemangle.cpp: ���������� ���������������� ������� ��� ���������� DLL.
//

#include "stdafx.h"
#include "demangle.h"

//

void StringCopy(char* dest, char* src, int len)
{
	int i=0;
	if(src==NULL)
	{
		dest[i]=0;
		return;
	}
	while(len>0)
	{
		dest[i]=src[i];
		if(src[i]==0)
			break;
		i++;
		len--;
	}
	if(dest[i]!=0)
		dest[i+1]=0;
}
int StringLen(char* s)
{
	int len=0;
	while(s[len]!=0) len++;
	return len;
}
void StringCat(char* dest, char* src, int len)
{
	int i=StringLen(dest);
	int j=0;
	while(len>0)
	{
		dest[i]=src[j];
		if(src[j]==0)
			break;
		i++;
		j++;
		len--;
	}
	if(dest[i]!=0)
		dest[i+1]=0;
}


extern "C" __declspec(dllexport) int Demangle(char* mangled, INT32 option, char* output,INT32 len)
    {
		char* demangled=cplus_demangle(mangled,option);
		if(demangled!=NULL)
		{
			StringCopy(output, demangled,len);
			return 0;
		}
		else
			return 1;
    }



