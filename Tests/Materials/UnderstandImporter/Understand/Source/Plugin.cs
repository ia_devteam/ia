using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.ComponentModel;

using Store;
using Store.Table;
using IA.Extensions;

namespace IA.Plugins.Importers.AistImporter
{
    using AistSubsystem;
    internal static class PluginSettings
    {
        /// <summary>
        /// ������������ �������
        /// </summary>
        internal const string Name = "������ ����������� ������ ����-C";

        /// <summary>
        /// ������������� �������
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.AIST_IMPORTER;

        #region Settings
        /// <summary>
        /// ���� � �������� ����� � ������� �������
        /// </summary>
        internal static string AistReportsPath = Properties.Settings.Default.AistReportsPath;

        /// <summary>
        /// ���� � ����������, �� ������� ���� ������������� ������
        /// </summary>
        internal static string OldSourcesPath = Properties.Settings.Default.OldSourcesPath;

        /// <summary>
        /// ������ ����� �������
        /// </summary>
        internal enum Tasks
        {
            [Description("��������� ������ ������ ����-�")]
            PARSING_BUNCHES
        };
        #endregion
    }

    /// <summary>
    /// ������ ������� ������� ����-� � ���������.
    /// </summary>
    public class AistImporter : IA.Plugin.Interface
    {
        internal Storage storage;

        AistReportReaderSettingsControl settings;
        Importer importer;

        #region PluginInterface Members

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.SIMPLE_SETTINGS_WINDOW | 
                    Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW | 
                    Plugin.Capabilities.RERUN;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;
            importer = new Importer(storage);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            if (settingsBuffer == null)
                return;
            PluginSettings.AistReportsPath = settingsBuffer.GetString();
            PluginSettings.OldSourcesPath = settingsBuffer.GetString();
            if (settings != null)
            {
                settings.PathToAistReportDirectory = PluginSettings.AistReportsPath;
                settings.PathToOldSourcesDirectory = PluginSettings.OldSourcesPath;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //FIXME - ������ ������ ���� �� ������������ � ������� ������
            //if (String.IsNullOrEmpty(settingsString) || !settingsString.Contains(";"))
            //    return;
            //string[] splitted = settingsString.Split(';');
            //if (splitted.Length < 2)
            //    return;
            //PluginSettings.AistReportsPath = splitted[0];
            //PluginSettings.OldSourcesPath = splitted[1];
            //if (settings != null)
            //{
            //    settings.PathToAistReportDirectory = splitted[0];
            //    settings.PathToOldSourcesDirectory = splitted[1];
            //}

            //����� ���������, ���������� ����� ������
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            if (storage == null)
            {
                Monitor.Log.Error(this.Name, "��������� ������������.");
                return;
            }

            AistReportsReader reader = new AistReportsReader(
                PluginSettings.AistReportsPath, 
                true,  
                oldFilePrefix: PluginSettings.OldSourcesPath,
                newFilePrefix: storage.appliedSettings.FileListPath);

            Monitor.Tasks.Update(this.Name, PluginSettings.Tasks.PARSING_BUNCHES.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)reader.BunchesCount);

            ulong count = 0;
            foreach (var bunch in reader.EnumerateBunches())
            {
                Monitor.Tasks.Update(this.Name, PluginSettings.Tasks.PARSING_BUNCHES.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++count);
                importer.ImportFunctions(bunch);

                if (bunch.Variables != null)
                    foreach (var variable in bunch.Variables)
                        if (!importer.ImportVariable(variable))
                            Monitor.Log.Warning(this.Name, String.Format("���������� �� ���������. ���: {0}, ������: {1}.", variable.Name, variable.AistLineNumber));
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //��������� ��������� � XML-���� �������� ����������
            Properties.Settings.Default.AistReportsPath = PluginSettings.AistReportsPath;
            Properties.Settings.Default.OldSourcesPath = PluginSettings.OldSourcesPath;
            Properties.Settings.Default.Save();

            //��������� ��������� � ����� ��������
            settingsBuffer.Add(PluginSettings.AistReportsPath);
            settingsBuffer.Add(PluginSettings.OldSourcesPath);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return (settings = new AistReportReaderSettingsControl());
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return this.CustomSettingsWindow;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (settings != null)
            {
                PluginSettings.AistReportsPath = settings.PathToAistReportDirectory;
                PluginSettings.OldSourcesPath = settings.PathToOldSourcesDirectory;
            }

            if (!Directory.Exists(PluginSettings.AistReportsPath))
            {
                message = "������� � ������� ������� �� ������.";
                return false;
            }

            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>() { PluginSettings.Tasks.PARSING_BUNCHES.ToMonitorTask(Name)};
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

        {
            get
            {
                return null;
            }
        }
        #endregion
    }
}
