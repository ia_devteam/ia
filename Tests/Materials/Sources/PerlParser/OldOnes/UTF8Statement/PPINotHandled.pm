#!./perl

# Tests for labels in UTF-8

ＬＡＢＥＬ: {
    pass("Sanity check, UTF-8 labels don't throw a syntax error.");
}
