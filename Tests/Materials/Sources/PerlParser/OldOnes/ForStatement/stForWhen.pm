#!/usr/bin/perl

for (<$afmfile>) {
    when (m/\A\s* C         \s/xms) { push @{$afm{metrics}},    $_ }
    when (m/\A\s* KPX       \s/xms) { push @{$afm{kernpairs}},  $_ }
    when (m/\A\s* TrackKern \s/xms) { push @{$afm{trackkerns}}, $_ }
    when (m/\A\s* CC        \s/xms) { push @{$afm{composites}}, $_ }
    default {push @{$afm{composites}}, $_}
}
