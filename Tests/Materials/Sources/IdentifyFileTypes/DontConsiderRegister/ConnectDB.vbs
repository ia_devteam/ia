'����������� ���� ������ � SQL-�������
'
'������ ����������� � ����������� 
'������ �������  - c:\ConnectDB.vbs srv_name db_name path_to_db mdf_name ldf_name

On Error resume Next
Set objFSO=WScript.CreateObject("Scripting.FileSystemObject")	
Set objWshShell=WScript.CreateObject("wscript.shell")

'������ ��������� ��������� ������
Set objArgs = WScript.Arguments
srv_name=objArgs.Item(0)			'��� �������
db_name=objArgs.Item(1)				'��� ���� ������
path_to_db=objArgs.Item(2)			'���� � �������� � ������� ���� ������ ������ \Database\
mdf_name=objArgs.Item(3)			'��� mdf-�����
ldf_name=objArgs.Item(4)			'��� ldf-�����

'��������� ������������
msgbox_title="����������� ��� ������ � SQL-������� " + srv_name

'������� ������� Scripts � �������� � ��
objFolder=objFSO.CreateFolder(path_to_db + "\scripts")
If(Err.Number<>0)then 
	If not (objFSO.FolderExists(path_to_db + "\scripts"))then '���� ������� �� �������,������� ������ � �������� � ��
		MsgBox "�� ������� ������� ����� ��� �������� " +path_to_db + "\scripts", msgbox_title
		path_to_script_dir = path_to_db
	Else '������� \Scripts ��� ����������
		path_to_script_dir=path_to_db + "\Scripts" 
	end if 
Else
	path_to_script_dir=path_to_db + "\Scripts" 
End If	

'������� ���� sql-�������
path_to_sql_file=path_to_script_dir + "\attachDB_" + db_name + ".sql"
set objFile = objFSO.OpenTextFile(path_to_sql_file,2,True)
if(Err.Number<>0)then 
	If not objFSO.FileExists(path_to_sql_file) Then 
		MsgBox "�� ������� ������� ���� sql-������" , msgbox_title , vbCritical '��� ������� �������� ����� sql-������� ������� �� vbs-�������
		WScript.Quit 1
	End If
End If	

'����� ������ ����������� �� � sql-������
objFile.writeline("EXEC sp_attach_db @dbname = N'" + db_name + "', ")
objFile.WriteLine(" @filename1 = N'" + path_to_db + "\" + mdf_name + "',")
objFile.WriteLine(" @filename2 = N'" + path_to_db + "\" + ldf_name + "'")
objFile.close

'��������� ������� osql.exe
ret_code=objWshShell.run ("osql.exe -S " + srv_name + " -E -i  " + chr(34) + path_to_sql_file + Chr(34) + " -o " + chr(34) + path_to_sql_file + Chr(34) + ".log",2,True)
If ret_code<>0 Then '��� �������� �� 0 - ������� osql.exe �� ��������� !!!
	MsgBox "������� osql �� ���������.���� ������ " + db_name + " �� ������������.",msgbox_title,vbInformation
	WScript.Quit 2
Else	
	'������ ���� � ������������ ������ ������� osql.exe (���������� .log)
	Set objFile = objFSO.GetFile(path_to_sql_file + ".log")
	If objFile.Size>0 Then
		path_to_log_file=path_to_sql_file + ".log"
		Set log_file=objFSO.OpenTextFile(path_to_log_file,1 )
		strContents = log_file.ReadAll
		log_file.Close
		'Wscript.Echo "The file is not empty."
	Else
		log_size=0
	    'Wscript.Echo "The file is empty."
	End If
	'���� log-���� �� ������, ���� � ��� ����� "unable" � "error",
	'���� ����� ����,�� ���������� pos_... ����� ������� �� ����
	If(log_size<>0)then
		pos_error=InStr(strContents,"error")				
		pos_unable=InStr(strContents,"unable")	
		pos_big_error=InStr(strContents,"Error")
		pos_big_unable=InStr(strContents,"Unable")	
	End If
	
	'���� log-���� �� ������ � � ��� ��� ����� "unable" � "error",�� �������� �� ������������ ���������
	If (log_size=0 Or pos_error<>0 Or pos_unable<>0 Or pos_big_error<>0 Or pos_big_unable<>0) Then
		MsgBox "������� osql ���������,�� �������� ���� ������ " + db_name + " �� ������������!",msgbox_title,vbCritical
		WScript.Quit 3	
	Else
		MsgBox "������� osql ���������.��������� ������������� ���� ������ " + db_name ,msgbox_title,vbInformation
		WScript.Quit 0		
	End If

End If
