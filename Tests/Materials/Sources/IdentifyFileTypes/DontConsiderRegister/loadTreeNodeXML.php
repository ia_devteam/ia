<?php
header("Cache-Control: no-cache");
header("Content-Type: text/xml");
if(!isset($_SERVER['DOCUMENT_ROOT'])){
    switch (true) {
        case preg_match('/(^.*)spo_modules/',$_SERVER['SCRIPT_FILENAME'], $laParts):
                      $lsDocumentRoot = $laParts[1];
                    break;
        case preg_match('/(^.*)resource/',$_SERVER['SCRIPT_FILENAME'], $laParts):
                     $lsDocumentRoot = $laParts[1];
                    break;
        default:
                    $lsDocumentRoot = dirname($_SERVER['SCRIPT_FILENAME']);
    }
    require_once( $lsDocumentRoot . '/spo_modules/config/document_root.php' );
}
require_once( $_SERVER['DOCUMENT_ROOT'] . '/spo_modules/config/config.php' );
require_once( CLASS_QUERY );
include_once('functions.php');

global $SQL, $user;

//---------------------  ---------------------//
if(isset($_GET['id'])){
	$root = $_GET['id'];
	$_GET['kod_omenu'] = '';
}
else{
	if(!isset($_GET['kod_omenu'])){
		$_GET['id'] = 0;
		$root = 0;
	}
	else{  
		$root = 0;
	}
}

$res = getMenuItems($_GET);

//---------------------  ---------------------//

/*Mandatory parameters for this tag are:
text - label of the node
id - id of the node
Optional parameters for this tag are:
tooltip - tooltip for the node
im0 - image for node without children (tree will get images from the path specified in setImagePath(url) method)
im1 - image for opened node with children
im2 - image for closed node with children
aCol - colour of not selected item
sCol - colour of selected item
select - select node on load (any value)
style - node text style
open - show node opened (any value)
call - call function on select(any value)
checked - check checkbox if exists(any value)
child - spec. if node has children (1) or not (0)

imheight - height of the icon
imwidth - width of the icon
topoffset - offset of the item from the node above
radio - if non empty, then children of this node will have radiobuttons*/
//'0' => '�����', '1' => '�������� �����', '2' => '�������� ��������', '3' => '�������� �� ����������', '4' => '���������� �����'

echo "<tree id='".$root."'>";
foreach($res as $key => $value){
	
	if( trim($value['icon_src']) == '' ){
		$ln_isAlwaysIcon = 0;
		
		switch($value['pr_inf']){
			case 1:
					$icon = 'iconWrite1.png';
					break;
			case 2:
					$icon = 'iconText.png';
					break;
			case 3:
					$icon = 'iconWrite2.png';
					break;
			case 4:
					$icon = 'iconText.png';
					break;
			default:
					$icon = 'folderClosed.gif';
					break;
		}
	}
	else{
		$ln_isAlwaysIcon = 1;
		$ls_IconSrc = $value['icon_src']{0} == '/' ? substr($value['icon_src'], 1) : $value['icon_src'];
		$icon = '../../../../../' . $ls_IconSrc;
	}
	
	
	if($ln_isAlwaysIcon != 1){
		if($icon != '')
			$ln_strIcon = "im0='".$icon."'";
		else
			$ln_strIcon = "";
	}
	else{
		if($icon != '')
			$ln_strIcon = "im0='".$icon."' im1='".$icon."' im2='".$icon."'";
		else
			$ln_strIcon = "";
	}
	
	switch ( $_GET['vid_menu'] )
	{
		case 'MainMenu' :
            if ( $value['kod_menu'] > 0 ){
                $l_aUrlOnClick = getCompiledMenuLink($value['link_menu'].($value['param'] != '0'?"&".$value['param']:""));
				$l_aUrlOnClick .= (strpos($l_aUrlOnClick, "?")!=false?"&":"?").($value['pr_inf'] != 0?"pr_inf=".$value['pr_inf']:"")."&pkod_omenu=".$value['kod_omenu'];
            }
			else
                $l_aUrlOnClick = getCompiledMenuLink("/spo_modules/lib/php/report.php?name_tpl=../../menu/tpl/sys_menu_comment_report&sreport=1&kod_omenu=".$value['kod_omenu']."&kodv_omenu=".$value['kodv_omenu']."&rubrika_omenu=".$value['rubrika_omenu'] );
			break;
		case 'ObjMenu'  : $l_aUrlOnClick = getCompiledMenuLink("../../lib/php/frame_edit.php?name_tpl=../../menu/tpl/sys_menu_edit&rezhim=view&action=undo&domElem=1&kod_omenu=".$value['kod_omenu']);
			break;
		case 'BaseMenu'  : $l_aUrlOnClick = getCompiledMenuLink("../../lib/php/frame_edit.php?name_tpl=../../menu/tpl/spo_menu_base_edit&rezhim=view&action=undo&domElem=1&kod_bmenu=".$value['kod_omenu']);
			break;
	}

	echo "<item id='".$value['kod_omenu']."' text='".iconv('cp1251', 'utf-8', $value['naimp_omenu'])."' child='".$value['haschilds'].
	"' tooltip='".iconv('cp1251', 'utf-8', $value['prim_menu'])."' ".$ln_strIcon."
	".($value['kod_omenu'] == $_GET['selectId']?"select='yes'":'').">
	<userdata name='link'><![CDATA[".iconv('cp1251', 'utf-8', $l_aUrlOnClick)."]]></userdata>
	<userdata name='is_newwin'>".$value['is_newwin']."</userdata>
	<userdata name='target_window'>target_window_".$value['kod_omenu']."</userdata>
	</item>\n";
}
echo "</tree>";
?>