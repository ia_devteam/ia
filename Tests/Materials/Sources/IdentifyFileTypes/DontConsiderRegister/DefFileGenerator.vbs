dim DefFileName
dim ObjectName

if WScript.Arguments.Count <> 2 Then
  Call PrintHelp
  Wscript.Quit 1
End If
DefFileName = WScript.Arguments.Item(0)
ObjectName = WScript.Arguments.Item(1)



Set objFSO = CreateObject("Scripting.FileSystemObject")
dim DefFile
Set DefFile = objFSO.CreateTextFile(DefFileName, True)
DefFile.WriteLine "LIBRARY"
DefFile.WriteLine "EXPORTS"

symbolsTmp = "symbols.txt"
dim shell
set shell=createobject("wscript.shell")
shellString = "%EXTERN_UTILS%\Other\genSymbol.bat " & """" & ObjectName & """ " & symbolsTmp
rem Wscript.Echo "Debug: Shell execute string """ & shellString & """"
ret = shell.Run(shellString , 0, TRUE)
If ret > 0 Then
 Wscript.Echo "Error dump symbols from file " & ObjectName
 Wscript.Echo "Error code " & ret
 Wscript.Quit 1
End if
rem reading symbols file
Set symbolFile = objFSO.OpenTextFile(symbolsTmp, 1, False)
Set fileMap = CreateObject("Scripting.Dictionary")
i = 0
Do Until symbolFile.AtEndOfStream
strNextLine = symbolFile.Readline
If len(strNextLine)  > 0 Then
fileMap.Add i, strNextLine
i = i + 1
End If
Loop
symbolFile.Close
objFSO.DeleteFile symbolsTmp, TRUE
rem extract symbols
Dim re1
Set re1 = new regexp
re1.Pattern = "^[^ ]+ +[^ ]+ +SECT[^ ]+ +[^ ]+ +\(\) +External +\| +(\?[^ ]*) (.*)"
re1.IgnoreCase = true
re1.Global = false

Dim re2
Set re2 = new regexp
re2.Pattern = "^[^ ]+ +[^ ]+ +SECT[^ ]+ +[^ ]+ +External +\| +(\?[^?][^ ]*)(.*)"
re2.IgnoreCase = true
re2.Global = false

Dim ex
Set ex = new regexp
ex.Pattern = "deleting destructor[^(]+\(unsigned int\)|anonymous namespace"
ex.IgnoreCase = true
ex.Global = false


Set symbolMap = CreateObject("Scripting.Dictionary")
i = 0
Do Until i =  fileMap.Count
set match = re1.Execute( fileMap.Item(i) )
if ( match.Count = 0 ) then
   set match = re2.Execute( fileMap.Item(i) )
End if
if ( match.Count = 1 ) then
    symbol = match.Item(0).SubMatches(0)
	args = match.Item(0).SubMatches(1)
	set matchEx = ex.Execute(args)
	if ( matchEx.Count = 0 ) Then 
	  symbolMap.Add j, symbol
	  j = j + 1
	End if
End if
i = i + 1
Loop

rem dump symbols to DEF file
i = 0
Do Until i =  symbolMap.Count
DefFile.WriteLine  symbolMap.Item(i)
i = i + 1
Loop
Wscript.Quit 0

Sub PrintHelp()
  Wscript.Echo "Usage: DefFileGenerator.vbs DefinitionFileName ObjectName"
End Sub