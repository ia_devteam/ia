<?php
header("Cache-Control: no-cache");
header("Content-Type: text/xml");
if(!isset($_SERVER['DOCUMENT_ROOT'])){
    switch (true) {
        case preg_match('/(^.*)spo_modules/',$_SERVER['SCRIPT_FILENAME'], $laParts):
                      $lsDocumentRoot = $laParts[1];
                    break;
        case preg_match('/(^.*)resource/',$_SERVER['SCRIPT_FILENAME'], $laParts):
                     $lsDocumentRoot = $laParts[1];
                    break;
        default:
                    $lsDocumentRoot = dirname($_SERVER['SCRIPT_FILENAME']);
    }
    require_once( $lsDocumentRoot . '/spo_modules/config/document_root.php' );
}
require_once( $_SERVER['DOCUMENT_ROOT'] . '/spo_modules/config/config.php' );
require_once( CLASS_QUERY );

global $SQL;

//---------------------  ---------------------//
if(!isset($_GET['id']))
	$_GET['id'] = 0;
	
$params = $_GET;
//f_print_r($params);
$res = $SQL->sql_query_mod("SELECT DISTINCT
								mv.kod_bmenu
							,	mv.kodv_bmenu
							,	mv.naimp_bmenu
							,	mv.rubrika_bmenu
							,	CAST(mv.prim_menu as varchar(5000)) as prim_menu
							,	mv.pr_inf
							,	COALESCE(mv.icon_src,'') as icon_src
							,	mv.haschilds
							FROM
								(
									SELECT 
										ss.kod_bmenu, rtrim(tt.rubrika_bmenu) as rubrika_bmenu
									FROM 
										spo.tspo_menu_base ss
											INNER JOIN
												(
													SELECT 
														prj.kod_bmenu 	 			as kod_bmenu
														, rtrim(prj.rubrika_bmenu) 	as rubrika_bmenu
													FROM 
														(SELECT kod_bmenu, rubrika_bmenu FROM spo.tspo_menubase_rubrika WHERE kod_project = (SELECT kod_project FROM spo.tspo_project WHERE kods_project = @kods_project@)) prj
												) tt ON ss.kod_bmenu = tt.kod_bmenu
									WHERE 1 = 1
										AND ss.kods_project & @kods_project@ > 0
								) t
								JOIN 
									(
										SELECT 
											ss.kod_bmenu
											, tt.kodv_bmenu
											, tt.naimp_bmenu
											, rtrim(tt.rubrika_bmenu) as rubrika_bmenu
											, COALESCE(bmenu_prim.prim_menu, tt.naimp_bmenu) as prim_menu
											, m.pr_inf
											, ss.icon_src
											, CASE WHEN have_child.child = 0 OR have_child.child IS NULL THEN 1 ELSE 0 END as haschilds
										FROM
											spo.tspo_menu_base ss
											LEFT JOIN spo.tspo_menu m ON ss.kod_menu = m.kod_menu
											LEFT JOIN
															(
																SELECT 
																	kod_bmenu,
																	prim_menu
																FROM 
																	spo.tspo_menubase_rubrika 
																WHERE 
																	kod_project IN (SELECT kod_project FROM spo.tspo_project WHERE kods_project = @kods_project@)
															) bmenu_prim ON ss.kod_bmenu = bmenu_prim.kod_bmenu
											INNER JOIN
														(
															SELECT 
																	kod_bmenu,
																	kodv_bmenu,
																	naimp_bmenu,
																	rubrika_bmenu,
																	kod_project
																FROM 
																	spo.tspo_menubase_rubrika
																WHERE 
																	kod_project IN (SELECT kod_project FROM spo.tspo_project WHERE kods_project = @kods_project@)
														) tt ON ss.kod_bmenu = tt.kod_bmenu
											LEFT JOIN  (
															SELECT DISTINCT kod_bmenu, 1 as child 
															FROM spo.tspo_menubase_rubrika v
																WHERE 1 = 1 AND NOT EXISTS
																(SELECT kod_bmenu FROM spo.tspo_menubase_rubrika n WHERE n.kodv_bmenu = v.kod_bmenu)
													) have_child ON ss.kod_bmenu = have_child.kod_bmenu
										WHERE 1 = 1
									) mv ON t.rubrika_bmenu LIKE rtrim(mv.rubrika_bmenu) || '%'
							WHERE
								1 = 1
								AND mv.kod_bmenu != 0
								#mv.kodv_bmenu = @id@
								#LOWER(mv.naimp_bmenu) like LOWER('%@naimp_bmenu@%')
								#mv.kod_bmenu IN (@kod_bmenu@)
								#mv.kod_bmenu IN (@kod_omenu@)
					#ORDER BY mv.rubrika_bmenu", $params);

//f_print_r($res);
//---------------------  ---------------------//

$str_link = "../../lib/php/frame_edit.php?name_tpl=../../menu/tpl/spo_menu_base_edit&rezhim=view&action=undo&domElem=1&kod_bmenu=";

echo "<tree id='".$_GET['id']."'>";

foreach($res as $key => $value)
{
	if( $value['icon_src'] == '' ){
		$ln_isAlwaysIcon = 0;
		
		switch($value['pr_inf']){
			case 1:
					$icon = 'iconWrite1.png';
					break;
			case 2:
					$icon = 'iconText.png';
					break;
			case 3:
					$icon = 'iconWrite2.png';
					break;
			case 4:
					$icon = 'iconText.png';
					break;
			default:
					$icon = '';
					break;
		}
	}
	else{
		$ls_IconSrc = $value['icon_src']{0} == '/' ? substr($value['icon_src'], 1) : $value['icon_src'];
		$icon = '../../../../../' . $ls_IconSrc;
	}
	
	if($ln_isAlwaysIcon != 1){
		if($icon != '')
			$ln_strIcon = "im0='".$icon."'";
		else
			$ln_strIcon = "";
	}
	else{
		if($icon != '')
			$ln_strIcon = "im0='".$icon."' im1='".$icon."' im2='".$icon."'";
		else
			$ln_strIcon = "";
	}


	echo "<item id='".$value['kod_bmenu']."' text='".iconv('cp1251', 'utf-8', $value['naimp_bmenu'])."' child='".$value['haschilds'].
	"' tooltip='".iconv('cp1251', 'utf-8', $value['prim_menu'])."' ".$ln_strIcon.">
	<userdata name='link'><![CDATA[".getCompiledMenuLink($str_link.$value['kod_bmenu'])."]]></userdata>
	</item>\n";
}
echo "</tree>";

?>