'==========================================================================
' NAME: GetSqlServiceName
'
' AUTHOR: ������� �. 
' DATE  : 03.06.2010
'
' �������� ����� ����� SQL �������,���������� ���� ����� ����� ����� ���������� ������������� ����������� SQL �������,
' �� ����� ������ � ����� ��������� ��������� ������ ��� SQL �������.
' ���������� ������ ���������� � ���� sql.ini � ������� temp ���������� �������� (������ c:\windows)
'
' ����� ������
' �������� ��� �������,������� ���������� �� "MSSQL"
' ���� � ���� ������� ����� �� ��� �����:������ ���� � exe-����� � ��������� ��������� ������
' �� ���� � ����� �������� ���������� ����,���� ��� sqlservr.exe, �� ��� ������ ��� ������
' ������ �������� ��� SQL ������� - ��� ��� ���������� + "\" + �����  �������� ������ ����� "MSSQL" (� ����� "$" ���� �� ����)
'==========================================================================
Dim SQL_service_name(16)												'������ ���� �����
Dim SQL_name(16)														'������ ���� SQL ��������

Set objNet=WScript.CreateObject("wscript.network")
com_name=objNet.ComputerName
'��������� ������ ��� ��������� ��������� ��������
srv_count=0
strComputer = "."
Set objWMIService = GetObject("winmgmts:" &  "{impersonationLevel=Impersonate}!\\" & strComputer & "\root\cimv2")


Set colServices = objWMIService.ExecQuery ("Select * From Win32_Service")' ��������� ��������
For Each objService in colServices
    leftname = Left( objService.name,5)									' ������ 5 ���� � �������� ������
    If (leftname="MSSQL") Then
    	rightname=Right(objService.name,Len(objService.name)-5)
    	pos_second_chr34=InStr(2,objService.pathname,chr(34))			' ������� ������ "
    	file_path=Mid(objService.pathname,2,pos_second_chr34-2)			' ���� ����� ������
    	pos_last_slash=InStrRev( file_path,"\")							' ������� ���������� / � ����
		file=Right(file_path,Len(file_path)-pos_last_slash)				' ������ ��� ���� ������
    	
    	If(file="sqlservr.exe")then
    		srv_count=srv_count+1
		   	'WScript.Echo objService.name				'��� ������
   			'WScript.Echo objService.pathname			'���� �������
   			If Left(rightname,1)="$" Then
    			rightname=Right(rightname,Len(rightname)-1)
    			'WScript.Echo rightname
    		end If
    		SQL_name(srv_count-1)=com_name + "\" + rightname
   			SQL_service_name(srv_count-1) = objService.name
   			'WScript.Echo SQL_service_name(count-1)
   		End if   		  		
    end If	    
Next
If (srv_count>1)then
	str="�� ���������� ����������� ����� ������ ���������� SQL Server. (" + CStr(srv_count) + ")"
	'MsgBox str	  
End If 	

Set objWshShell = CreateObject("WScript.Shell")

sWinDir = objWshShell.ExpandEnvironmentStrings("%WinDir%")
sWinTempFolder = sWinDir & "\Temp"
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objTextFile = objFSO.OpenTextFile (sWinTempFolder + "\sql.ini", 2, True)
objTextFile.WriteLine("[common]")
objTextFile.WriteLine("servers count=" + cstr(srv_count))
For i=1 To srv_count 
'	If count>1 then
		objTextFile.WriteLine("[server" + CStr(i) + "]")
'	Else	
'		objTextFile.WriteLine("[server]")
'	End if
	objTextFile.WriteLine("SRVname=" + SQL_name(i-1))
	objTextFile.WriteLine("servicename="  + SQL_service_name(i-1))
Next
objTextFile.Close
	
WScript.Quit srv_count 										'������� ���������� ��������� �����
