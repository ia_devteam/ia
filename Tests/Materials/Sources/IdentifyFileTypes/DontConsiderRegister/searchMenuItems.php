<?php
if(!isset($_SERVER['DOCUMENT_ROOT'])){
    switch (true) {
        case preg_match('/(^.*)spo_modules/',$_SERVER['SCRIPT_FILENAME'], $laParts):
                      $lsDocumentRoot = $laParts[1];
                    break;
        case preg_match('/(^.*)resource/',$_SERVER['SCRIPT_FILENAME'], $laParts):
                     $lsDocumentRoot = $laParts[1];
                    break;
        default:
                    $lsDocumentRoot = dirname($_SERVER['SCRIPT_FILENAME']);
    }
    require_once( $lsDocumentRoot . '/spo_modules/config/document_root.php' );
}
require_once( $_SERVER['DOCUMENT_ROOT'] . '/spo_modules/config/config.php' );
require_once( CLASS_QUERY );
include_once('functions.php');

global $SQL;

$res = getMenuItems($_GET);
//---------------------  ---------------------//

unset($_GET['naimp_omenu']);
unset($_GET['naimp_bmenu']);

echo "<select id='tmpResult'>";
foreach($res as $key => $value){
	$optValue = $value['kod_omenu'];
	$tmp = getMenuItems(array_merge ($_GET, array ("kod_omenu" => $value['kodv_omenu'])));
	$optValue = ($tmp[0]['kod_omenu'] > 0 ? $tmp[0]['kod_omenu'].',' : '').$optValue;
	$i = 0;
	while($tmp[0]['kodv_omenu'] > 0 && $i < 50){
		$tmp = getMenuItems(array_merge ($_GET, array ("kod_omenu" => $tmp[0]['kodv_omenu'])));
		$optValue = $tmp[0]['kod_omenu'].','.$optValue;
		$i++;
	}
	echo "<option value='".$optValue."'>".$value['naimp_omenu']."</option>\n";
}
echo "</select>";
?>
<!--script src="/spo_modules/lib/js/DOMInspector.js"></script-->
<script>
	var tmpObj = document.getElementById('tmpResult');
	var resObj = parent.document.getElementById('searchResult');
	
	/*classObj = resObj.className;
	styleObj = resObj.style;
	parentObj = resObj.parentNode;
	
	tmpObj.id = 'searchResult';
	//tmpObj.style = styleObj;
	tmpObj.className = classObj;
	
	parentObj.removeChild(resObj);
	parentObj.appendChild(tmpObj);*/
	resObj.options.length = 1;
	if(tmpObj){
		for(i = 0; i < tmpObj.options.length; i++){
			//var Node = tmpObj.options[i].cloneNode(true);
			//resObj.appendChild(Node);
			var newOpt = document.createElement('OPTION');
			newOpt.text = tmpObj.options[i].text;
			newOpt.value = tmpObj.options[i].value;
			resObj.options.add(newOpt);
		}
		tmpObj.parentNode.removeChild(tmpObj);
	}
	
</script>