unit UfrmDrive;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, JvExStdCtrls, JvCombobox, JvDriveCtrls;

type
  TfrmDrive = class(TForm)
    Label1: TLabel;
    drive_copy: TJvDriveCombo;
    Button1: TButton;
    Button2: TButton;
    procedure drive_copyChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDrive: TfrmDrive;

implementation

{$R *.dfm}

uses
  UfrmCopy;

procedure TfrmDrive.Button1Click(Sender: TObject);
begin
 UfrmCopy.FTask_Stop:=true;
 frmDrive.Close;
end;

procedure TfrmDrive.Button2Click(Sender: TObject);
begin
  FDriveCopy:=drive_copy.Drive+ ':';
  frmDrive.Close;
end;

procedure TfrmDrive.drive_copyChange(Sender: TObject);

begin
  FDriveCopy:=drive_copy.Drive+ ':';

end;

procedure TfrmDrive.FormShow(Sender: TObject);
begin
   FDriveCopy:=drive_copy.Drive+ ':';
end;

end.
