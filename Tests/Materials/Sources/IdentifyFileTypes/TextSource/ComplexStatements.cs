﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComplexStatements
{
    public class ComplexStatements
    {
        public ComplexStatements()
        {
            List<int> list = new List<int>();

            for (int i = 0; i < 10; i++)
            {
                list.Add(i);
            }

            int j = 0;
            while (list.Count != 0)
            {
                if (list[j]%2 == 0)
                {
                    list.Remove(j);
                    j++;
                }
                else
                {
                    try
                    {
                        foreach (int key in list)
                        {
                            switch (key)
                            {
                                case 2:
                                case 4:
                                case 6:
                                    j++;
                                    break;
                                case 0:
                                case 8:
                                    j--;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    catch
                    {
                        throw new Exception("Что-то пошло не так!");
                    }
                }
            }
        }
    }
}
