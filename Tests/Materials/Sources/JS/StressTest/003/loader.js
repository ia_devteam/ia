
angular.module('loader',[]).directive('loader',function(){

    return {
        restrict: 'E',
        replace: true,
        template: '<canvas></canvas>',
        link: function($scope,$element,$attr){

                var ctx = $element[0].getContext('2d'),

                    $parent = $element.parent(),

                    rotation = 0,

                    running = false,

                    options = {
                        radius : 3,
                        segments : 9,
                        color : 'black',
                        opacity : 0.7,
                        speed : 500,
                        width: 35,
                        height:35
                    },

                    render = function(){

                        var centerX = options.width/2,
                            centerY = options.height/2,
                            angle = 2*Math.PI/options.segments,
                            x =  centerX - options.radius,
                            y =  centerY - options.radius,
                            i;

                        rotation += angle;

                        ctx.clearRect(0,0,options.width,options.height);
                        ctx.fillStyle = options.color;

                        for(i = 0; i < options.segments; i++){
                            ctx.beginPath();
                            ctx.globalAlpha = (options.opacity - options.opacity/options.segments * i);
                            ctx.arc(centerX + Math.sin(angle * i - rotation)*x, centerY + Math.cos(angle * i - rotation)*y, options.radius, 0, 2 * Math.PI, false);
                            ctx.fill();
                            ctx.closePath();
                        }
                    },

                    startAnimation = function(){

                        var timeLast = new Date(),

                            animation = function(){
                                if( new Date() - timeLast > options.speed/options.segments  ){
                                    render();
                                    timeLast = new Date();
                                }

                                if(running){
                                    window.requestAnimationFrame(animation);
                                } 
                            };

                            running = true;

                            animation();
                    },

                    stopAnimation = function(){
                        running = false;
                    },
                    center = function(){
                        $element.css({
                            top: '50%',
                            left:'50%',
                            position: 'absolute',
                            marginTop: - options.height / 2,
                            marginLeft: - options.width / 2
                        }).attr({
                            width:options.width,
                            height:options.height
                        });           
                    },

                    animation;

                startAnimation();

                $scope.$watchCollection($attr.options,function(attrOptions){
                    angular.extend(options, attrOptions);
                    center();
                },true);

                $scope.$on('$destroy',function(){
                    stopAnimation();
                });
        }
    };
}); 
