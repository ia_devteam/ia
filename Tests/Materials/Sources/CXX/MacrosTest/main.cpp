#include "include.h"

#define OURMACRO called();

int main()
{
	return -1;
}

void first()
{
	int a;
	if(a == 1)
		SIMPLEMACRO
}

void second()
{
	int a;
	if (SIMPLETEST)
		SIMPLEMACRO
}

int third()
{
	int a;
	if (SIMPLETEST)
		SIMPLEMACRO called();
    return 0;
}

int fourth()
{
	int a;
	if (SIMPLETEST)
		SIMPLEMACRO return 0;
}

void first1()
{
	int a;
	if(a == 1)
		OURMACRO
}

void second1()
{
	int a;
	if (SIMPLETEST)
		OURMACRO
}

int third1()
{
	int a;
	if (SIMPLETEST)
		OURMACRO called();
    return 0;
}

int fourth1()
{
	int a;
	if (SIMPLETEST)
		OURMACRO return 0;
}