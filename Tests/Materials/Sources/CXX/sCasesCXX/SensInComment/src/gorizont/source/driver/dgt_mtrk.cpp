//---------------------------------------------------------------------------
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "dgt_osid.h"
#include "dgt_base.h"
#include "dgt_mtrk.h"
//---------------------------------------------------------------------------
TDgtMetricEditor::TDgtMetricEditor()
{
	dim        = NULL;
	part       = NULL;
	type       = NULL;
	metric     = NULL;
	isx_metric = NULL;
//
	nodeSize     = 4;
	dFactor      = 4.0;
	mode         = vmMetricNone;
	setxymode    = meXYFloat;
	activecontur = -1;
	activenode   = -1;
	derivenode   = 0;
	curr_xy.x    = 0.0;
	curr_xy.y    = 0.0;
	x_fixed      = 0;
	y_fixed      = 0;
	snap_xy.x    = 0.0;
	snap_xy.y    = 0.0;
	snapmode     = 0;
	snapped      = 0;
	memset(&snap_object,0,sizeof snap_object);
}
//---------------------------------------------------------------------------
TDgtMetricEditor::~TDgtMetricEditor()
{
	delete[] isx_metric;
	delete[] metric;
//
	delete[] dim;
	delete[] part;
	delete[] type;
}
//---------------------------------------------------------------
void TDgtMetricEditor::setxy(DIM_NODE &_node, double _x, double _y)
{
	if((setxymode == meXYShort) || (setxymode == meXYLong))
	{
		if(_x > 0.0)
			_x = (int)(_x + 0.5);
		else if(_x < 0.0)
			_x = (int)(_x - 0.5);
		if(_y > 0.0)
			_y = (int)(_y + 0.5);
		else if(_y < 0.0)
			_y = (int)(_y - 0.5);
	}
	if(setxymode == meXYShort)
	{
		_x = min2(max2(-32768, _x), 32767);
		_y = min2(max2(-32768, _y), 32767);
	}
//
	_node.x = _x;
	_node.y = _y;
}
//---------------------------------------------------------------
void TDgtMetricEditor::pix2dim1(double &_x, double &_y)
{
	pix2dim(_x, _y);
//
	DIM_NODE node;
	memset(&node, 0, sizeof(node));
	setxy(node, _x, _y);
	_x = node.x;
	_y = node.y;
}
//---------------------------------------------------------------
int TDgtMetricEditor::under(double _x, double _y)
{
	DIM_NODE *node = metricNodeA(activenode);
	if(node)
	{
		double x = node->x,
		       y = node->y;
		dim2pix(x, y);
		if((fabs(x - _x) < nodeSize + 1) &&
		   (fabs(y - _y) < nodeSize + 1))
			return activenode;
	}
	activenode = -1;
//
	int n = NDim__NodesCount(metric);
	node = NDim__Nodes(metric);

	double dx,
	       dy;
	for(int a = 0; a < n; a++, node++)
	{
		double x = node->x,
		       y = node->y;
		dim2pix(x, y);
		if((fabs(x -= _x) < nodeSize + 1) &&
		   (fabs(y -= _y) < nodeSize + 1))
			if(activenode == -1)
			{
				activenode = a;
				dx = x;
				dy = y;
			}
			else if(dx*dx + dy*dy > x*x + y*y)
			{
				activenode = a;
				dx = x;
				dy = y;
			}
	}
//
	return activenode;
}
//---------------------------------------------------------------
int TDgtMetricEditor::activenodeA()
{
	double x = 0,
	       y = 0;
	mouseposget(x, y);
//
	return under(x, y);
}
//---------------------------------------------------------------
void TDgtMetricEditor::mouserectset(int _mode)
{
	switch(_mode)
	{
		case vmClipNone:
		case vmClipControl:
			break;
		case vmClipXControl:
		case vmClipYControl:
			break;
	}
}
//---------------------------------------------------------------
DIM_NODE *TDgtMetricEditor::metricNodeA(int _node)
{
	if(_node < 0)
		return NULL;
	if(_node >= NDim__NodesCount(metric))
		return NULL;
//
	return NDim__Nodes(metric) + _node;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricNodeB(int _contur, int _node)
{
	if((_contur < 0) || (_contur >= metric->ContursCount))
		return -1;
	DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
	if((_node < 0) || (_node >= contur[_contur].NodesCount))
		return -1;
//
	for(int a = 0; a < _contur; a++, contur++)
		_node += contur->NodesCount;
//
	return _node;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricNodeInsertA(double _x, double _y)
{
DIM_CONTUR *conturs;
DIM_NODE   *nodes;
double     radius=_dgt_max_double;
int       contur,node;

	if(metric->ContursCount < 1)
		return metricNodeInsertB(0, 0, _x, _y);

	if(under(_x, _y) != -1)
		{
// EE - 15-Aug-03  17:18:58 - razreshim vstavky parnoy vershini
		conturs = (DIM_CONTUR*)(metric + 1);
		node    = activenode;
		for (int b = 0; b < metric->ContursCount; b++)
			{
			if (node < conturs->NodesCount)
				return metricNodeInsertB(b, node, _x, _y);
			node -= conturs->NodesCount;
			conturs++;
			}
// EE - end
		return activenode;
		}
//
	double x = _x,
	       y = _y;
	pix2dim(x, y);
//
	conturs = (DIM_CONTUR*)(metric + 1);
	nodes   = NDim__Nodes(metric);
	contur  = -1;
	node    = -1;
	for(int b = 0; b < metric->ContursCount; b++)
	{
		if((radius == _dgt_max_double) &&
		   (contur == -1) &&
		   (conturs->NodesCount == 0))
		{
			contur = b;
			node = 0;
		}
		else if(conturs->NodesCount == 1)
		{
			double r1 = hypot(x - nodes->x, y - nodes->y);
			if(r1 < radius)
			{
				contur = b;
				node = 1;
				radius = r1;
			}
		}
		else if(ItClosed(nodes, conturs->NodesCount))
		{
			if(conturs->NodesCount == 2)
			{
				double r1 = hypot(x - nodes->x, y - nodes->y);
				if(r1 < radius)
				{
					contur = b;
					node = 1;
					radius = r1;
				}
			}
			else
			{
				int n = conturs->NodesCount - 1;
				for(int a = 0; a < n; a++)
				{
					XYD n0 = {x, y},
					    n1 = {nodes[(a + n + 2)%n].x, nodes[(a + n + 2)%n].y},
					    n2 = {nodes[(a + n + 1)%n].x, nodes[(a + n + 1)%n].y},
					    n3 = {nodes[(a + n    )%n].x, nodes[(a + n    )%n].y},
					    n4 = {nodes[(a + n - 1)%n].x, nodes[(a + n - 1)%n].y},
					   *nn1 = &n1,
					   *nn4 = &n4;
//					    nnnn;
// EE - 14-Aug-03  19:31:33 - vstavka vershini okolo parnix vershin
						if ((n1.x == n2.x) && (n1.y == n2.y))
							nn1 = NULL;
						if ((n3.x == n4.x) && (n3.y == n4.y))
							nn4 = NULL;
// EE - end
					double r1 = NAlg__LineDistance(
						n0, (XYD*)NULL, nn1, n2, n3, nn4);
//						n0,      &nnnn, nn1, n2, n3, nn4);
					if(r1 < radius)
					{
//dim2pix(nnnn.x, nnnn.y);
//_x = nnnn.x;
//_y = nnnn.y;
						contur = b;
						node = a + 1;
						radius = r1;
					}
				}
			}
		}
		else
		{
			int nn = conturs->NodesCount - 1,
			     n = conturs->NodesCount;
			for(int a = 0; a < nn; a++)
			{
				XYD n0 = {x, y},
				    n1 = {nodes[(a + n + 2)%n].x, nodes[(a + n + 2)%n].y},
				    n2 = {nodes[(a + n + 1)%n].x, nodes[(a + n + 1)%n].y},
				    n3 = {nodes[(a + n    )%n].x, nodes[(a + n    )%n].y},
				    n4 = {nodes[(a + n - 1)%n].x, nodes[(a + n - 1)%n].y},
				   *nn1 = &n1,
				   *nn4 = &n4;
				if(a == 0)
					nn4 = NULL;
				if(a == (nn - 1))
					nn1 = NULL;
// EE - 14-Aug-03  19:31:33 - vstavka vershini okolo parnix vershin
				if ((n1.x == n2.x) && (n1.y == n2.y))
					nn1 = NULL;
				if ((n3.x == n4.x) && (n3.y == n4.y))
					nn4 = NULL;
// EE - end
				double r1 = NAlg__LineDistance(
					n0, (XYD*)NULL, nn1, n2, n3, nn4);
				if((r1 == _dgt_max_double) && (a == 0))
				{
					r1 = hypot(x - n3.x, y - n3.y);
					if(r1 < radius)
					{
						contur = b;
						node = 0;
						radius = r1;
					}
					if(n == 2)
					{
						r1 = hypot(x - n2.x, y - n2.y);
						if(r1 < radius)
						{
							contur = b;
							node = n;
							radius = r1;
						}
					}
				}
				else if((r1 == _dgt_max_double) && (a == (nn - 1)))
				{
					r1 = hypot(x - n2.x, y - n2.y);
					if(r1 < radius)
					{
						contur = b;
						node = n;
						radius = r1;
					}
				}
				else if(r1 < radius)
				{
					contur = b;
					node = a + 1;
					radius = r1;
				}
			}
		}
		nodes += conturs->NodesCount;
		conturs++;
	}
//
	return metricNodeInsertB(contur, node, _x, _y);
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricNodeInsertB(int _contur, int _node, double _x, double _y)
{
	activenode = -1;
	if((_node < 0) || (_contur < 0))
		return activenode;
//
	if(_contur > metric->ContursCount)
		_contur = metric->ContursCount;
	if(metric->ContursCount > _contur)
	{
		if(_node > ((DIM_CONTUR*)(metric + 1) + _contur)->NodesCount)
			_node = ((DIM_CONTUR*)(metric + 1) + _contur)->NodesCount;
	}
	else
		_node = 0;
//
	int n1 = NDim__NodesCount(metric),
	     n = 0;
	DIM_CONTUR *dc = (DIM_CONTUR*)(metric + 1);
	for(int a = 0; a < metric->ContursCount; a++, dc++)
		if(_contur == a)
		{
			n += _node;
			break;
		}
		else
			n += dc->NodesCount;
//
	int len = NDim__MetricLen(metric);
	len += sizeof(DIM_NODE);
	if(metric->ContursCount == _contur)
		len += sizeof(DIM_CONTUR);
	DIM_METRIC *m1 = (DIM_METRIC*)new char[len];
	memset(m1, 0, len);
	memcpy(m1, metric, sizeof(DIM_METRIC) + sizeof(DIM_CONTUR)*metric->ContursCount);
	if(metric->ContursCount == _contur)
		m1->ContursCount++;
	dc = (DIM_CONTUR*)(m1 + 1) + _contur;
	dc->NodesCount++;
//
	memcpy((DIM_CONTUR*)(m1 + 1) + m1->ContursCount,
	    (DIM_CONTUR*)(metric + 1) + metric->ContursCount,
	    n*sizeof(DIM_NODE));
	memcpy((DIM_NODE*)((DIM_CONTUR*)(m1 + 1) + m1->ContursCount) + n + 1,
	    (DIM_NODE*)((DIM_CONTUR*)(metric + 1) + metric->ContursCount) + n,
	    (n1 - n)*sizeof(DIM_NODE));
//
	DIM_NODE *dn = (DIM_NODE*)((DIM_CONTUR*)(m1 + 1) + m1->ContursCount);
	if(dc->NodesCount > 1)
	{
		if(dc->NodesCount - 1 == _node)
			dn[n].Flags |= dn[n - 1].Flags & DIM_NODE::dnPenUp;
		else
			dn[n].Flags |= dn[n + 1].Flags & DIM_NODE::dnPenUp;
	}
	pix2dim(_x, _y);
//
	setxy(dn[n], _x, _y);
//
	if((_node > 0) && (_node < dc->NodesCount - 1))
	{
		if ((dn[n-1].dXOut != 0.0) || (dn[n-1].dYOut != 0.0) ||
		    (dn[n+1].dXIn  != 0.0) || (dn[n+1].dYIn  != 0.0))
		{
			double dx = dn[n+1].x - dn[n-1].x;
			double dy = dn[n+1].y - dn[n-1].y;
			double h  = hypot(dx,dy),
				   h_in, h_out;
			if (h != 0.0)
			{
				h_in  = hypot(dn[n].x-dn[n-1].x,dn[n].y-dn[n-1].y);
				h_out = hypot(dn[n+1].x-dn[n].x,dn[n+1].y-dn[n].y);
				dn[n].dXIn  = dx * h_in  / h;
				dn[n].dYIn  = dy * h_in  / h;
				dn[n].dXOut = dx * h_out / h;
				dn[n].dYOut = dy * h_out / h;
			}
			if(_node > 1)
			{
				n--;
				dx = dn[n+1].x - dn[n-1].x;
				dy = dn[n+1].y - dn[n-1].y;
				h  = hypot(dx,dy);
				if (h != 0.0)
				{
					h_out = hypot(dn[n+1].x-dn[n].x,dn[n+1].y-dn[n].y);
					dn[n].dXOut = dx * h_out / h;
					dn[n].dYOut = dy * h_out / h;
				}
				n++;
			}
			if(_node < dc->NodesCount - 2)
			{
				n++;
				dx = dn[n+1].x - dn[n-1].x;
				dy = dn[n+1].y - dn[n-1].y;
				h  = hypot(dx,dy);
				if (h != 0.0)
				{
					h_in  = hypot(dn[n].x-dn[n-1].x,dn[n].y-dn[n-1].y);
					dn[n].dXIn  = dx * h_in  / h;
					dn[n].dYIn  = dy * h_in  / h;
				}
				n--;
			}
		}
	}
//
	delete[] metric;
	metric = m1;
//
	return activenode = n;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricNodeInsertC(double _x, double _y)
{
int n;

	if((activecontur < 0) || (activecontur > metric->ContursCount))
		return -1;
//
	if((metric->ContursCount > 0) && (activecontur < metric->ContursCount)) // EE - 30-Jan-07  19:42:49 "&& (activecontur..."
		{
		n = ((DIM_CONTUR*)(metric + 1) + activecontur)->NodesCount; // EE - 19-Mar-04  13:06:09 " + activecontur"
		if (n > 2)
			if (activenode < n - 1) // EE - 19-Mar-04  13:34:00
				metricConturOpen(activecontur);
		}
//
	if(activecontur == metric->ContursCount)
	{
		metricNodeInsertB(activecontur, 0, _x, _y);
		metricNodeInsertB(activecontur, 1, _x, _y);
	}
	else
	{
		n = ((DIM_CONTUR*)(metric + 1) + activecontur)->NodesCount;
		if(n < 1)
		{
			metricNodeInsertB(activecontur, 0, _x, _y);
			metricNodeInsertB(activecontur, 1, _x, _y);
		}
		else if(n == 1)
		{
			metricNodeInsertB(activecontur, 0, _x, _y);
			DIM_NODE *n0 = metricNodeA(activenode),
			         *n1 = n0 + 1;
			n1->x = n0->x;
			n1->y = n0->y;
			activenode++;
		}
		else
		{
			n = max2(0, n - 1);
			metricNodeInsertB(activecontur, n, _x, _y);
			DIM_NODE *n0 = metricNodeA(activenode),
			         *n1 = n0 + 1;
			n1->x = n0->x;
			n1->y = n0->y;
			activenode++;
		}
	}
//
	return activenode;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricNodeDeleteA(int _node)
{
	activenode = -1;
//
	return metricNodeDeleteB(
		metricContur(_node),
		metricConturNode(_node));
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricNodeDeleteB(int _contur, int _node)
{
	activenode = metricNodeB(_contur, _node);
	if(activenode < 0)
		return activenode;
//
	if(mode == vmMetricNewLine)
	{
		DIM_CONTUR *c0 = (DIM_CONTUR*)(metric + 1) + _contur;
		if(c0->NodesCount > 1)
		{
			DIM_NODE *n0 = NDim__Nodes(metric) + activenode;
			int nn = NDim__NodesCount(metric);
			memmove(n0, n0 + 1, (nn - activenode - 1)*sizeof(*n0));
			c0->NodesCount--;
// EE - 22-Apr-10  18:55:04
			nn --;
			if (activenode >= nn)
				activenode = nn - 1;
// EE - end
		}
		return activenode;
	}
//
	int len = NDim__MetricLen(metric),
	     empty = 0,
	     count = 0;
	DIM_METRIC *m1 = (DIM_METRIC*)new char[len];
	memset(m1, 0, len);
	memcpy(m1, metric, sizeof(*m1) + metric->ContursCount*sizeof(DIM_CONTUR));
	DIM_CONTUR *c0 = (DIM_CONTUR*)(metric + 1),
	           *c1 = (DIM_CONTUR*)(m1 + 1);
	DIM_NODE   *n0 = NDim__Nodes(metric),
	           *n1 = NDim__Nodes(m1),
	           *n1t = n1,
	           *node = metricNodeA(activenode);
	for(int a = 0; a < metric->ContursCount; a++)
	{
		if(c1->NodesCount > 0)
		{
			if(node->Flags & DIM_NODE::dnSelected)
			{
				int n = 0;
				if(ItClosed(n0, c0->NodesCount))
				{
					if(_node == c0->NodesCount - 1)
						_node = 0;
					int nn = _node;
					for(int b = 0; b < c0->NodesCount - 1; b++)
						if(!(n0[b].Flags & DIM_NODE::dnSelected))
							memcpy(n1 + n++, n0 + b, sizeof(*n1));
						else
							if(nn > b)
								_node--;
					if(n > 0)
						memcpy(n1 + n++, n1, sizeof(*n1));
				}
				else
				{
					int nn = _node;
					for(int b = 0; b < c0->NodesCount; b++)
						if(!(n0[b].Flags & DIM_NODE::dnSelected))
							memcpy(n1 + n++, n0 + b, sizeof(*n1));
						else
							if(nn > b)
								_node--;
				}
				c1->NodesCount = n;
				n1 += c1->NodesCount;
				n0 += c0->NodesCount;
			}
			else
			{
				if(_contur == a)
				{
					if(ItClosed(n0, c0->NodesCount) &&
						((_node == 0) || (_node == c0->NodesCount - 1)))
					{
						if(_node ==  c0->NodesCount - 1)
							_node = 0;
						if(c0->NodesCount < 3)
							c1->NodesCount = 1;
						else
							for(int b = 1; b < c0->NodesCount; b++)
								if(b == c0->NodesCount - 1)
									memcpy(n1 + (b - 1), n0 + 1, sizeof(*n1));
								else
									memcpy(n1 + (b - 1), n0 + b, sizeof(*n1));
					}
					else
					{
						for(int b = 0, t = 0; b < c0->NodesCount; b++)
							if(b != _node)
								memcpy(n1 + t++, n0 + b, sizeof(*n1));
						if(_node ==  c0->NodesCount - 1)
						{
							_node--;
							_node = max2(0, _node);
						}
					}
					c1->NodesCount--;
				}
				else
					for(int b = 0; b < c0->NodesCount; b++)
						memcpy(n1 + b, n0 + b, sizeof(*n1));
				n1 += c1->NodesCount;
				n0 += c0->NodesCount;
			}
		}
		if(c1->NodesCount < 1)
			empty++;
		else
			count += c1->NodesCount;
		c1++;
		c0++;
	}
	if(empty)
	{
		c1 = (DIM_CONTUR*)(m1 + 1);
		int n = 0;
		for(int a = 0; a < m1->ContursCount; a++)
			if(c1[a].NodesCount > 0)
			{
				if(_contur == a)
					_contur = n;
				if(a != n)
					memcpy(c1 + n, c1 + a, sizeof(*c1));
				n++;
			}
			else
				if(_contur == a)
					_contur = -1;
		m1->ContursCount = n;
		if(m1->ContursCount > 0)
		{
			n1 = NDim__Nodes(m1);
			memmove(n1, n1t, count*sizeof(DIM_NODE));
		}
	}
	delete[] metric;
	metric = m1;
//
	metricGoNode(activenode = metricNodeB(_contur, _node));
//
	return activenode;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricConturDivide(int _contur, int _node)
{
	if((_contur < 0) || (_contur >= metric->ContursCount))
		return activenode;

	DIM_CONTUR *dc,*dc1;
	dc = (DIM_CONTUR*)(metric + 1);
	if((_node <= 0) || (_node >= dc[_contur].NodesCount))
		return activenode;

	int n   = NDim__NodesCount(metric),
	     len = NDim__MetricLen(metric);
	len += sizeof(DIM_CONTUR);
	DIM_METRIC *m1 = (DIM_METRIC*)new char[len];
	memset(m1, 0, len);
	memcpy(m1, metric, sizeof(DIM_METRIC));
	m1->ContursCount++;
	dc1 = (DIM_CONTUR*)(m1 + 1);
	memcpy(dc1,dc,(_contur+1)*sizeof(DIM_CONTUR));
	memcpy(dc1+_contur+1,dc+_contur,(metric->ContursCount-_contur)*sizeof(DIM_CONTUR));
	dc1[_contur  ].NodesCount  = _node;
	dc1[_contur+1].NodesCount -= _node;
	memcpy(dc1+m1->ContursCount,dc+metric->ContursCount,n*sizeof(DIM_NODE));

	delete[] metric;
	metric = m1;

	if (activecontur == _contur)
		activecontur = _contur + 1;

	return activenode;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricConturMerge(int _contur)
{
	if((_contur < 1) || (_contur >= metric->ContursCount))
		return activenode;

	int n = NDim__NodesCount(metric);
	DIM_CONTUR *dc = (DIM_CONTUR*)(metric + 1);
	metric->ContursCount--;
	dc[_contur-1].NodesCount += dc[_contur].NodesCount;
	memmove(dc+_contur,dc+_contur+1,(metric->ContursCount-_contur)*sizeof(DIM_CONTUR));
	memmove(dc+metric->ContursCount,dc+metric->ContursCount+1,n*sizeof(DIM_NODE));

	if (activecontur == _contur)
		activecontur = _contur - 1;

	return activenode;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricGoNearest(double _x, double _y)
{
	if(under(_x, _y) != -1)
		return activenode;
//
	int n = NDim__NodesCount(metric),
	     n1 = -1;
	DIM_NODE *node = NDim__Nodes(metric);
	double r = 0;
	for(int a = 0; a < n; a++, node++)
	{
		double x = node->x,
		       y = node->y;
		dim2pix(x, y);
		if(!insidescreen(x, y))
			continue;
		if(n1 == -1)
		{
			n1 = a;
			r = hypot(x - _x, y - _y);
		}
		else if(hypot(x - _x, y - _y) < r)
		{
			n1 = a;
			r = hypot(x - _x, y - _y);
		}
	}
//
	return metricGoNode(n1);
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricGoNode(int _node)
{
	DIM_NODE *node = metricNodeA(_node);
	if(!node)
		return activenode;
//
	double x = node->x,
	       y = node->y;
	dim2pix(x, y);
	if(insidescreen(x, y))
	{
		mouseposset(x, y);
		activenode = _node;
	}
//
	return activenode;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricNodeKeyMove(int _node, int _mode)
{
	if(!metric)
		return 1;
//
	switch(mode)
	{
		case vmMetricNone:
		{
			double x = 0,
			       y = 0;
			mouseposget(x, y);
			under(x, y);
			if(activenode == -1)
			{
				metricGoNearest(x, y);
				break;
			}
//
			double x0,
			       y0,
			       x1,
			       y1,
			       dx = 0.0,
			       dy = 0.0,
			       dx0 = 0.0,
			       dy0 = 0.0;
			switch(_mode)
			{
				case meMoveUp:
					dy0 = 1.0;
					break;
				case meMoveDown:
					dy0 = -1.0;
					break;
				case meMoveLeft:
					dx0 = -1.0;
					break;
				case meMoveRight:
					dx0 = 1.0;
					break;
				default:
					break;
			}
			if(!dx0 && !dy0)
				break;
//
			hide();
//
			DIM_NODE *n0 = metricNodeA(activenode),
			          n1;
			xy0.x = x;
			xy0.y = y;
			do
			{
				memcpy(&n1, n0, sizeof(n1));
//
				dx += dx0;
				dy += dy0;
//
				x0 = xy0.x,
				y0 = xy0.y,
				x1 = x0 + dx,
				y1 = y0 + dy;
//
				pix2dim(x0, y0);
				pix2dim(x1, y1);
//
				dxy.x = x1 - x0;
				dxy.y = y1 - y0;
//
				setxy(n1, n1.x + dxy.x, n1.y + dxy.y);
			}
			while((n1.x == n0->x) && (n1.y == n0->y));
//
			mode = vmMetricMove;
			metricCorrect(metric);
			mode = vmMetricNone;
//
			metricGoNode(activenode);
//
			draw();
			break;
		}
		case vmMetricNewLine:
		case vmMetricRotate:
		case vmMetricScale:
			break;
	}
//
	showstatus();
	return 0;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricContur(int _node)
{
	int n1 = NDim__NodesCount(metric);
	if((_node < 0) || (_node >= n1))
		return -1;
//
	DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
	int        a = 0;
	for(; a < metric->ContursCount; a++, contur++)
		if(_node >= contur->NodesCount)
			_node -= contur->NodesCount;
		else
			break;
//
	return a;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricConturNode(int _node)
{
	int n1 = NDim__NodesCount(metric);
	if((_node < 0) || (_node >= n1))
		return -1;
//
	DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
	for(int a = 0; a < metric->ContursCount; a++, contur++)
		if(_node >= contur->NodesCount)
			_node -= contur->NodesCount;
		else
			break;
//
	return _node;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricConturOpen(int _contur)
{
	if(!metric)
		return 1;
	if(metric->ContursCount < 1)
		return 0;
	if(_contur >= metric->ContursCount)
		return 2;
//
	DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
	DIM_NODE   *nodes = NDim__Nodes(metric);
	int        count = NDim__NodesCount(metric);
	for(int a = 0; a < metric->ContursCount; a++, contur++)
	{
		int n = contur->NodesCount;
		if((_contur < 0) || (a == _contur))
			while(ItClosed(nodes, n))
				n--;
		if(contur->NodesCount > n)
			memmove(
				nodes + n,
				nodes + contur->NodesCount,
				sizeof(*nodes)*(count - contur->NodesCount));
		nodes += n;
		count -= contur->NodesCount;
		contur->NodesCount = n;
	}
//
	return 0;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricConturClose(int _contur)
{
	if(!metric)
		return 1;
	if(metric->ContursCount < 1)
		return 0;
	if(_contur >= metric->ContursCount)
		return 2;
//
	int len = NDim__MetricLen(metric);
	len += sizeof(DIM_NODE)*metric->ContursCount;
	DIM_METRIC *m1 = (DIM_METRIC*)new char[len];
	memset(m1, 0, len);
	memcpy(m1, metric, sizeof(*m1));
//
	DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1),
	           *c1 = (DIM_CONTUR*)(m1 + 1);
	DIM_NODE   *nodes = (DIM_NODE*)(contur + metric->ContursCount),
	           *n1 = (DIM_NODE*)(c1 + m1->ContursCount);
//
	for(int a = 0; a < metric->ContursCount; a++, contur++, c1++)
	{
		memcpy(c1, contur, sizeof(*c1));
		memcpy(n1, nodes, c1->NodesCount*sizeof(*n1));
		if((_contur < 0) || (_contur == a))
			if(!ItClosed(n1, c1->NodesCount))
				memcpy(n1 + c1->NodesCount++, n1, sizeof(*n1));
		nodes += contur->NodesCount;
		n1 += c1->NodesCount;
	}
//
	delete[] metric;
	metric = m1;
//
	return 0;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricConturInvert(int _contur)
{
	if(!metric)
		return 1;
	if(metric->ContursCount < 1)
		return 0;
	if(_contur >= metric->ContursCount)
		return 2;
//
	int len = NDim__MetricLen(metric);
	DIM_METRIC *m1 = (DIM_METRIC*)new char[len];
	memcpy(m1, metric, len);
	DIM_CONTUR *c1 = (DIM_CONTUR*)(m1 + 1);
	DIM_NODE   *nodes = NDim__Nodes(metric),
	           *n1 = NDim__Nodes(m1);
	for(int b = 0; b < m1->ContursCount; b++)
	{
		int n = c1->NodesCount;
		if(ItClosed(n1, n))
			memcpy(n1 + (n - 1), n1, sizeof(DIM_NODE));
		if((_contur < 0) || (_contur == b))
			for(int a = 0, a1 = n - 1; a < n; a++, a1--)
			{
				memcpy(nodes + a, n1 + a1, sizeof(DIM_NODE));
				if (a)
					{
					if(n1[a1+1].Flags & DIM_NODE::dnPenUp) // EE - 09-Sep-04  10:48:47 - a1+1
						nodes[a].Flags |=  DIM_NODE::dnPenUp;
					else
						nodes[a].Flags &= ~DIM_NODE::dnPenUp;
					}
				nodes[a].dXIn  = -n1[a1].dXOut;
				nodes[a].dXOut = -n1[a1].dXIn;
				nodes[a].dYIn  = -n1[a1].dYOut;
				nodes[a].dYOut = -n1[a1].dYIn;
				nodes[a].dZIn  = -n1[a1].dZOut;
				nodes[a].dZOut = -n1[a1].dZIn;
			}
		nodes += n;
		n1 += n;
		c1++;
	}
	delete[] m1;
//
	return 0;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricGetFlag(int *_flag)
{
int rc=1;
double x=0,y=0;
int uc,an;
DIM_NODE *node;

if (!metric)
	return(rc);

switch(mode)
	{
	case vmMetricNone :
		mouseposget(x, y);
		under(x, y);
		if (activenode >= 0)
			{
			node = metricNodeA(activenode);
			*_flag = node->Flags;
			rc = 0;
			}
		break;
	case vmMetricNewLine :
		uc = ((DIM_CONTUR*)(metric + 1) + activecontur)->NodesCount - 1;
		an = metricNodeB(activecontur,max2(0,uc));
		if (an >= 0)
			{
			node = metricNodeA(an);
			*_flag = node->Flags;
			rc = 0;
			}
		break;
	}
return(rc);
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricSetFlag(int _flag,bool _state)
{
double x=0,y=0;
int i;
int uc,an;
DIM_NODE *node;

if (!metric)
	return(1);

switch(mode)
	{
	case vmMetricNone :
		mouseposget(x, y);
		under(x, y);
		if (activenode == -1)
			metricGoNearest(x, y);
		else
			{
			hide();
			node = metricNodeA(activenode);
			if (node->Flags & DIM_NODE::dnSelected)
				{
				node = NDim__Nodes(metric);
				uc   = NDim__NodesCount(metric);
				for (i=0; i<uc; i++)
					{
					if (node[i].Flags & DIM_NODE::dnSelected)
						{
						if (_state)
							node[i].Flags |=  _flag;
						else
							node[i].Flags &= ~_flag;
						}
					}
				}
			else
				{
				if (_state)
					node->Flags |=  _flag;
				else
					node->Flags &= ~_flag;
				}
			draw();
			}
		break;
	case vmMetricNewLine :
		uc = ((DIM_CONTUR*)(metric + 1) + activecontur)->NodesCount - 1;
		an = metricNodeB(activecontur,max2(0,uc));
		if (an == -1)
			;
		else
			{
			hide();
			node = metricNodeA(an);
			if (_state)
				node->Flags |=  _flag;
			else
				node->Flags &= ~_flag;
			draw();
			}
		break;

	}
return(0);
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricInputContinue(int _contur)
{
	metricPassivate();
//
	hide();
//
	if((_contur < 0) || (_contur >= metric->ContursCount))
	{
		activecontur = metric->ContursCount;
		activenode = -1;
	}
	else
	{
		metricConturOpen(_contur);
//
		activecontur = _contur;
		activenode = ((DIM_CONTUR*)(metric + 1) + activecontur)->NodesCount;
		if(activenode > 0)
		{
			double x = 0,
			       y = 0;
			mouseposget(x, y);
			metricNodeInsertB(activecontur, activenode, x, y);
		}
	}
//
	mode = vmMetricNewLine;
//
	draw();
//
	return 0;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricMovingStart(double _x, double _y)
{
	activecontur = -1;
	mode = vmMetricMove;
	xy0.x = _x;
	xy0.y = _y;
	dxy.x = 0;
	dxy.y = 0;
//
	return 0;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricMovingContinue(double _x, double _y)
{
	if(mode != vmMetricMove)
		return 1;
//
	hide();
//
	double x0 = xy0.x,
	       y0 = xy0.y,
	       x1 = _x,
	       y1 = _y;
	pix2dim(x0, y0);
	pix2dim(x1, y1);
//
	dxy.x = x1 - x0;
	dxy.y = y1 - y0;
//
	draw();
//
	return 0;
}
//---------------------------------------------------------------
int TDgtMetricEditor::deriveMovingStart(double _x, double _y)
{
	mode = vmMetricDerive;
	xy0.x = _x;
	xy0.y = _y;
//
	return 0;
}
//---------------------------------------------------------------
int TDgtMetricEditor::deriveMovingContinue(double _x, double _y)
{
double x,y,dx,dy;
int dn,is,ie;

	if(mode != vmMetricDerive)
		return 1;
//
	hide();
//
	DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
	DIM_NODE *nodes = NDim__Nodes(metric);
	dn = abs(derivenode) - 1;
	x = _x;
	y = _y;
	pix2dim(x,y);
	if (derivenode < 0)
		{
		dx = (nodes[dn].x - x) * dFactor;
		dy = (nodes[dn].y - y) * dFactor;
		nodes[dn].dXIn = dx;
		nodes[dn].dYIn = dy;
		}
	else
		{
		dx = (x - nodes[dn].x) * dFactor;
		dy = (y - nodes[dn].y) * dFactor;
		nodes[dn].dXOut = dx;
		nodes[dn].dYOut = dy;
		}

	is = 0;
	for (int b = 0; b < metric->ContursCount; b++)
		{
		ie = is + contur->NodesCount - 1;
		if ((dn >= is) && (dn <= ie))
			{
			if (ItClosed(nodes+is, contur->NodesCount))
				{
				if (dn == is)
					{
					nodes[ie].dXIn  = nodes[is].dXIn;
					nodes[ie].dYIn  = nodes[is].dYIn;
					nodes[ie].dXOut = nodes[is].dXOut;
					nodes[ie].dYOut = nodes[is].dYOut;
					}
				else
					{
					if (dn == ie)
						{
						nodes[is].dXIn  = nodes[ie].dXIn;
						nodes[is].dYIn  = nodes[ie].dYIn;
						nodes[is].dXOut = nodes[ie].dXOut;
						nodes[is].dYOut = nodes[ie].dYOut;
						}
					}
				}
			break;
			}
		is += contur->NodesCount;
		contur ++;
		}
//
	draw();
//
	return 0;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricSelectionStart(double _x, double _y)
{
	activecontur = -1;
	activenode = -1;
	mode = vmMetricSelect;
	xy0.x = _x;
	xy0.y = _y;
	xy1.x = _x;
	xy1.y = _y;
//
	drawrect();
//
	return 0;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricSelectionContinue(double _x, double _y)
{
	hiderect();
//
	xy1.x = _x;
	xy1.y = _y;
//
	drawrect();
//
	return 0;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricRotateStart(double _x, double _y)
{
	activecontur = -1;
	activenode = -1;
	rotangle = 0.0;
	mode = vmMetricRotate;
	xy0.x = _x;
	xy0.y = _y;
	xy1.x = _x;
	xy1.y = _y;
//
	return 0;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricRotateContinue(double _x, double _y)
{
	if(mode != vmMetricRotate)
		return 1;
//
	hide();
//
	xy1.x = _x;
	xy1.y = _y;
//
	double cx = rotcenter.x;
	double cy = rotcenter.y;
	dim2pix(cx, cy);
	rotangle = 0.0;
	dxy.x = xy1.x - cx;
	dxy.y = xy1.y - cy;
	if ((dxy.x != 0.0) || (dxy.y != 0.0))
		rotangle = atan2(dxy.y, dxy.x);
	dxy.x = xy0.x - cx;
	dxy.y = xy0.y - cy;
	if ((dxy.x != 0.0) || (dxy.y != 0.0))
		rotangle -= atan2(dxy.y, dxy.x);
//
	draw();
//
	return 0;
}
//---------------------------------------------------------------
int TDgtMetricEditor::metricPassivate()
{
	if(mode == vmMetricNone)
		return 0;
//
	hide();
//
	if((mode == vmMetricNewLine) && !(activenode < 0))
	{
		int contur = metricContur(activenode);
		DIM_CONTUR *c0 = (DIM_CONTUR*)(metric + 1) + contur;
		if(c0->NodesCount < 2)
		{
			if(c0->NodesCount > 0)
			{
				DIM_NODE *n0 = NDim__Nodes(metric) + activenode;
				int nn = NDim__NodesCount(metric);
				memmove(n0, n0 + 1, (nn - activenode - 1)*sizeof(*n0));
				c0->NodesCount = 0;
			}
			int lc = (metric->ContursCount - contur - 1)*sizeof(DIM_CONTUR),
			     ln = NDim__NodesCount(metric)*sizeof(DIM_NODE);
			memmove(c0, c0 + 1, lc + ln);
			metric->ContursCount--;
			activenode = -1;
		}
	}
//
	metricCorrect(metric);
	if(mode == vmMetricSelect)
	{
		hiderect();
	}
//
	activecontur = -1;
	mode = vmMetricNone;
//
	draw();
//
	return 0;
}
//---------------------------------------------------------------
void TDgtMetricEditor::hide()
{
	draw();
}
//---------------------------------------------------------------
void TDgtMetricEditor::draw()
{
	if(dim)
	{
		DIM_OBJECT *dim1 = AllocDim();
		draw(dim1);
		delete[] dim1;
	}
	else if(part || type)
	{
		DIM_OBJECT *dim1 = NDim__DimNew();
		DIM_METRIC *m1 = NDim__MetricCopy(metric);
		metricCorrect(m1);
		NDim__MetricSet(dim1, m1);
		delete[] m1;
		draw(dim1);
		delete[] dim1;
	}
//	else if(part)
//	{
//		RULE_PART *rp = AllocPart();
//		draw(rp);
//		delete[] rp;
//	}
//	else if(type)
//	{
//		RULE_PART_TYPE *rt = AllocType();
//		draw(rt);
//		delete[] rt;
//	}
}
//---------------------------------------------------------------
void TDgtMetricEditor::hiderect()
{
	drawrect();
}
//---------------------------------------------------------------
void TDgtMetricEditor::drawrect()
{
	drawrect(xy0.x, xy0.y, xy1.x, xy1.y);
}
//---------------------------------------------------------------
int TDgtMetricEditor::InitDim(DIM_OBJECT *_dim, DIM_HEAD *_dh)
{
	setxymode = meXYFloat;
//
	hide();
//
	delete[] isx_metric;
	isx_metric = NULL;
	delete[] metric;
	metric = NULL;
//
	delete[] dim;
	dim = NULL;
	delete[] part;
	part = NULL;
	delete[] type;
	type = NULL;
//
	mode = vmMetricNone;
	activecontur = -1;
	activenode   = -1;
	derivenode   = 0;
	curr_xy.x    = 0.0;
	curr_xy.y    = 0.0;
	x_fixed      = 0;
	y_fixed      = 0;
	snap_xy.x    = 0.0;
	snap_xy.y    = 0.0;
	snapmode     = 0;
	snapped      = 0;
//
	if(!_dim)
		return 1;
/* EE - 27-Oct-06  17:17:48 - A zachem Long a ne Float ? Pust' ostanetsa Float
	if(_dh)
		if(!(_dh->Flags & DIM_HEAD::dhBL))
			setxymode = meXYLong;
*/
	dim = NDim__DimCopy(_dim);
	NDim__2DIM_NODE(dim);
//
	DIM_METRIC *metric1 = NDim__Metric(dim);
	if(!metric1)
	{
		metric = (DIM_METRIC*)new char[sizeof(DIM_METRIC)];
		memset(metric, 0, sizeof(DIM_METRIC));
		metric->Flags = DIM_METRIC::xyDIMNode;
	}
	else
	{
		metric = NDim__MetricCopy(metric1);
		if(setxymode == meXYLong)
		{
			int n = NDim__NodesCount(metric);
			DIM_NODE *nn = NDim__Nodes(metric);
			for(int a = 0; a < n; a++)
				setxy(nn[a], nn[a].x, nn[a].y);
		}
	}
	isx_metric = NDim__MetricCopy(metric);
//
	draw();
//
	return 0;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::InitPart(RULE_PART *_rp)
{
	setxymode = meXYFloat;
//
	hide();
//
	delete[] isx_metric;
	isx_metric = NULL;
	delete[] metric;
	metric = NULL;
//
	delete[] dim;
	dim = NULL;
	delete[] part;
	part = NULL;
	delete[] type;
	type = NULL;
//
	mode = vmMetricNone;
	activecontur = -1;
	activenode = -1;
//
	if(!_rp)
		return 1;
//
	setxymode = meXYShort;
	int len = NRul__PartLen(*_rp);
	part = (RULE_PART*)new char[len];
	memcpy(part, _rp, len);
//
	int n = NAno__NodesCount((RULE_PART_CONTUR*)(part + 1), part->PartType.ContursCount),
	     ml = sizeof(DIM_METRIC) + sizeof(DIM_CONTUR)*part->PartType.ContursCount + sizeof(DIM_NODE)*n;
	metric = (DIM_METRIC*)new char[ml];
	memset(metric, 0, ml);
	metric->Flags = DIM_METRIC::xyDIMNode;
	metric->ContursCount = part->PartType.ContursCount;
	DIM_CONTUR *dc = (DIM_CONTUR*)(metric + 1);
	RULE_PART_CONTUR *rc = (RULE_PART_CONTUR*)(part + 1);
	for(int a = 0; a < metric->ContursCount; a++)
		dc[a].NodesCount = rc[a].NodesCount;
	XY *xy = (XY*)(rc + metric->ContursCount);
	DIM_NODE *node = (DIM_NODE*)(dc + metric->ContursCount);
	for(int a = 0; a < n; a++)
	{
		node[a].x = xy[a].x;
		node[a].y = xy[a].y;
	}
	isx_metric = NDim__MetricCopy(metric);
//
	draw();
//
	return 0;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::InitType(RULE_PART_TYPE *_rt)
{
	setxymode = meXYFloat;
//
	hide();
//
	delete[] metric;
	metric = NULL;
//
	delete[] dim;
	dim = NULL;
	delete[] part;
	part = NULL;
	delete[] type;
	type = NULL;
//
	mode = vmMetricNone;
	activecontur = -1;
	activenode = -1;
//
	if(!_rt)
		return 1;
//
	setxymode = meXYShort;
	int len = NRul__TypeLen(*_rt);
	type = (RULE_PART_TYPE*)new char[len];
	memcpy(type, _rt, len);
//
	int n = NAno__NodesCount((RULE_PART_CONTUR*)(type + 1), type->ContursCount),
	     ml = sizeof(DIM_METRIC) + sizeof(DIM_CONTUR)*type->ContursCount + sizeof(DIM_NODE)*n;
	metric = (DIM_METRIC*)new char[ml];
	memset(metric, 0, ml);
	metric->Flags = DIM_METRIC::xyDIMNode;
	metric->ContursCount = type->ContursCount;
	DIM_CONTUR *dc = (DIM_CONTUR*)(metric + 1);
	RULE_PART_CONTUR *rc = (RULE_PART_CONTUR*)(type + 1);
	for(int a = 0; a < metric->ContursCount; a++)
		dc[a].NodesCount = rc[a].NodesCount;
	XY *xy = (XY*)(rc + metric->ContursCount);
	DIM_NODE *node = (DIM_NODE*)(dc + metric->ContursCount);
	for(int a = 0; a < n; a++)
	{
		node[a].x = xy[a].x;
		node[a].y = xy[a].y;
	}
//
	draw();
//
	return 0;
}
//---------------------------------------------------------------------------
DIM_OBJECT *TDgtMetricEditor::AllocDim()
{
	if(!metric || !dim)
		return NULL;
//
	DIM_OBJECT *dim1 = NDim__DimCopy(dim);
//
	DIM_METRIC *m1 = NDim__MetricCopy(metric);
	metricCorrect(m1);
	NDim__MetricSet(dim1, m1);
	delete[] m1;
//
	return dim1;
}
//---------------------------------------------------------------------------
RULE_PART *TDgtMetricEditor::AllocPart()
{
	if(!metric || !part)
		return NULL;
//
	int len = NRul__PartLen(*part);
	RULE_PART *rp = (RULE_PART*)new char[len];
	memcpy(rp, part, len);
//
	DIM_METRIC *m1 = NDim__MetricCopy(metric);
	metricCorrect(m1);
//
	int n = NAno__NodesCount((DIM_CONTUR*)(m1 + 1), m1->ContursCount);
	int *conturs = new int[m1->ContursCount];
	for(int a = 0; a < m1->ContursCount; a++)
		conturs[a] = ((DIM_CONTUR*)(m1 + 1) + a)->NodesCount;
	XY *xy = new XY[n];
	DIM_NODE *node = (DIM_NODE*)((DIM_CONTUR*)(m1 + 1) + m1->ContursCount);
	for(int a = 0; a < n; a++)
	{
		xy[a].x = node[a].x;
		xy[a].y = node[a].y;
	}
//
	int rs = NRul__PartMetricSet(rp, xy, conturs, m1->ContursCount);
//
	delete[] conturs;
	delete[] xy;
//
	delete[] m1;
//
	if(rs)
	{
		delete[] rp;
		return NULL;
	}
//
	return rp;
}
//---------------------------------------------------------------------------
RULE_PART_TYPE *TDgtMetricEditor::AllocType()
{
	if(!metric || ! type)
		return NULL;
//
	int len = NRul__TypeLen(*type);
	RULE_PART_TYPE *rt = (RULE_PART_TYPE*)new char[len];
	memcpy(rt, type, len);
//
	DIM_METRIC *m1 = NDim__MetricCopy(metric);
	metricCorrect(m1);
//
	int n = NAno__NodesCount((DIM_CONTUR*)(m1 + 1), m1->ContursCount);
	int *conturs = new int[m1->ContursCount];
	for(int a = 0; a < m1->ContursCount; a++)
		conturs[a] = ((DIM_CONTUR*)(m1 + 1) + a)->NodesCount;
	XY *xy = new XY[n];
	DIM_NODE *node = (DIM_NODE*)((DIM_CONTUR*)(m1 + 1) + m1->ContursCount);
	for(int a = 0; a < n; a++)
	{
		xy[a].x = node[a].x;
		xy[a].y = node[a].y;
	}
//
	int rs = NRul__TypeMetricSet(rt, xy, conturs, m1->ContursCount);
//
	delete[] conturs;
	delete[] xy;
//
	delete[] m1;
//
	if(rs)
	{
		delete[] rt;
		return NULL;
	}
//
	return rt;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::MouseDown(double _x, double _y, int _shift)
{
	if(!metric)
		return 1;

	switch(mode)
		{
		case vmMetricMove:
		case vmMetricDerive:
		case vmMetricNewLine:
			if (x_fixed)
				_x = curr_xy.x;
			if (y_fixed)
				_y = curr_xy.y;
			break;
		}
	curr_xy.x = _x;
	curr_xy.y = _y;

	mouserectset(vmClipControl);
	int snap = 0;
	switch(mode)
	{
		case vmMetricNone:
		{
			if(metric->ContursCount == 0)
			{
				metricInputContinue(0);
				ID_OBJECT snapobj = {0, 0};
				if (snapmode)
					snap = snappos(_x, _y, snapobj);
				hide();
				if (snapmode)
				{
					snapped = 0; //snap;
					memcpy(&snap_object,&snapobj,sizeof(ID_OBJECT));
					snap_xy.x = _x;
					snap_xy.y = _y;
					pix2dim(snap_xy.x, snap_xy.y);
				}
				metricNodeInsertC(_x, _y);
				draw();
				break;
			}
			int uc;
			uc = under_derive(_x, _y);
			if (uc)
				deriveMovingStart(_x, _y);
			else
				{
				uc = under(_x, _y);
				if(uc == -1)
					metricSelectionStart(_x, _y);
				else
					{
					if (_shift & vmViewShift)
						{
						int is=-1,ie=-1;
						int n = NDim__NodesCount(metric);
						DIM_NODE *node = NDim__Nodes(metric);
						hide();
						for(int a = 0; a < n; a++)
							{
							if (node[a].Flags & DIM_NODE::dnSelected)
								{
								node[a].Flags &= ~DIM_NODE::dnSelected;
								if (is < 0)
									is = a;
								ie = a;
								}
							}
						if (is < 0)
							{
							ie = is = activenode;
							}
						else
							{
							if (activenode >= is)
								ie = activenode;
							else
								{
								ie = is;
								is = activenode;
								}
							}
						for(int a = is; a <= ie; a++)
							node[a].Flags |= DIM_NODE::dnSelected;
						draw();
						}
					else
						{
						DIM_NODE *node = metricNodeA(activenode);
						if (_shift & vmViewCtrl)
							{
							hide();
							if (node->Flags & DIM_NODE::dnSelected)
								node->Flags &= ~DIM_NODE::dnSelected;
							else
								node->Flags |=  DIM_NODE::dnSelected;
							draw();
							}
						else
							{
// EE - 22-Dec-09  11:59:34
							_x = node->x;
							_y = node->y;
							dim2pix(_x, _y);
// EE - end
							metricMovingStart(_x, _y);
							}
						}
					}
				}
			break;
		}
		case vmMetricNewLine:
		{
			ID_OBJECT snapobj = {0, 0};
			if (snapmode)
				snap = snappos(_x, _y, snapobj);
			hide();
			if (snapmode)
			{
				snapped = 0; //snap;
				memcpy(&snap_object,&snapobj,sizeof(ID_OBJECT));
				snap_xy.x = _x;
				snap_xy.y = _y;
				pix2dim(snap_xy.x, snap_xy.y);
			}
			metricNodeInsertC(_x, _y);
			draw();
			break;
		}
		case vmMetricRotate | vmViewDown:
			metricRotateStart(_x, _y);
			break;
		case vmMetricScale:
			break;
	}
//
	showstatus();
	return 0;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::MouseMove(double _x, double _y)
{
	if(!metric)
		return 1;

	switch(mode)
		{
		case vmMetricMove:
		case vmMetricDerive:
		case vmMetricNewLine:
			if (x_fixed)
				_x = curr_xy.x;
			if (y_fixed)
				_y = curr_xy.y;
			break;
		}
	curr_xy.x = _x;
	curr_xy.y = _y;

	XYD xyd = {_x, _y};
	int snap = 0;
	switch(mode)
	{
		case vmMetricNone:
			if(metric->ContursCount == 0)
			{
				if (snapmode)
				{
					ID_OBJECT snapobj = {0, 0};
					snap = snappos(xyd.x, xyd.y, snapobj);
					hide();
					snapped = snap;
					memcpy(&snap_object,&snapobj,sizeof(ID_OBJECT));
					snap_xy.x = xyd.x;
					snap_xy.y = xyd.y;
					pix2dim(snap_xy.x, snap_xy.y);
					draw();
				}
			}
			break;
		case vmMetricMove:
			if (snapmode)
			{
				ID_OBJECT snapobj = {0, 0};
				snap = snappos(xyd.x, xyd.y, snapobj);
				hide();
				snapped = snap;
				memcpy(&snap_object,&snapobj,sizeof(ID_OBJECT));
				snap_xy.x = xyd.x;
				snap_xy.y = xyd.y;
				pix2dim(snap_xy.x, snap_xy.y);
				draw();
			}
			metricMovingContinue(_x, _y);
			break;
		case vmMetricSelect:
			metricSelectionContinue(_x, _y);
			break;
		case vmMetricDerive:
			deriveMovingContinue(_x, _y);
			break;
		case vmMetricNewLine:
		{
			ID_OBJECT snapobj = {0, 0};
			if (snapmode)
				snap = snappos(xyd.x, xyd.y, snapobj);
			hide();
			if (snapmode)
			{
				snapped = snap;
				memcpy(&snap_object,&snapobj,sizeof(ID_OBJECT));
				snap_xy.x = xyd.x;
				snap_xy.y = xyd.y;
				pix2dim(snap_xy.x, snap_xy.y);
			}
			DIM_NODE *node = metricNodeA(activenode);
			if(node)
			{
				pix2dim1(_x, _y);
				node->x = _x;
				node->y = _y;
			}
			draw();
			break;
		}
		case vmMetricRotate:
			metricRotateContinue(_x, _y);
			break;
		case vmMetricScale:
			break;
	}
//
	showstatus();
	return 0;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::MouseUp(double _x, double _y)
{
	if(!metric)
		return 1;

	switch(mode)
		{
		case vmMetricMove:
		case vmMetricDerive:
		case vmMetricNewLine:
			if (x_fixed)
				_x = curr_xy.x;
			if (y_fixed)
				_y = curr_xy.y;
			break;
		}
	curr_xy.x = _x;
	curr_xy.y = _y;

	mouserectset(vmClipNone);
	int snap = 0;
	switch(mode)
	{
		case vmMetricMove:
			if (snapmode)
			{
				ID_OBJECT snapobj = {0, 0};
				snap = snappos(_x, _y, snapobj);
				hide();
				snapped = 0; //snap;
				memcpy(&snap_object,&snapobj,sizeof(ID_OBJECT));
				snap_xy.x = _x;
				snap_xy.y = _y;
				pix2dim(snap_xy.x, snap_xy.y);
				draw();
			}
			metricMovingContinue(_x, _y);
			metricPassivate();
			break;
		case vmMetricSelect:
			metricSelectionContinue(_x, _y);
			metricPassivate();
			break;
		case vmMetricDerive:
			deriveMovingContinue(_x, _y);
			metricPassivate();
			break;
		case vmMetricRotate:
			metricRotateContinue(_x, _y);
			metricPassivate();
			break;
		case vmMetricScale:
			break;
	}
//
	showstatus();
	return 0;
}
//---------------------------------------------------------------
int TDgtMetricEditor::NodeFlag(int *_flag)
{
int rs;

rs = metricGetFlag(_flag);
return rs;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyPenUp(bool _state)
{
int rs;

rs = metricSetFlag(DIM_NODE::dnPenUp,_state);
return rs;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyNoMarker(bool _state)
{
int rs;

rs = metricSetFlag(DIM_NODE::dnNoMarker,_state);
return rs;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyOrient(bool _state)
{
int rs;

rs = metricSetFlag(DIM_NODE::dnOrient,_state);
return rs;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::MetricAsLine()
{
	hide();
	int rs = MetricDerivesClear(metric);
	draw();
//
	return rs;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::MetricAsErmit()
{
	hide();
	int rs = MetricDerivesAuto(metric);
	draw();
//
	return rs;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyIns()
{
	if(!metric)
		return 1;
//
	switch(mode)
	{
		case vmMetricNone:
		{
			double x = 0,
			       y = 0;
			mouseposget(x, y);
			if(metric->ContursCount == 0)
			{
				metricInputContinue(0);
				hide();
				metricNodeInsertC(x, y);
				draw();
				break;
			}
			if(1) //under(x, y) == -1) // EE - 15-Aug-03  17:18:58 - razreshim vstavky parnoy vershini
			{
				hide();
				metricNodeInsertA(x, y);
				draw();
//				metricGoNode(activenode); // EE - 01-Oct-04  13:54:52 - ostavim cursor v pokoe
			}
			break;
		}
		case vmMetricNewLine:
		{
			double x = 0,
			       y = 0;
			mouseposget(x, y);
			hide();
			metricNodeInsertC(x, y);
			draw();
			break;
		}
		case vmMetricRotate:
		case vmMetricScale:
			break;
	}
//
	showstatus();
	return 0;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyDel()
{
	if(!metric)
		return 1;
//
	switch(mode)
	{
		case vmMetricNone:
		{
			double x = 0,
			       y = 0;
			mouseposget(x, y);
			under(x, y);
			if(activenode == -1)
				metricGoNearest(x, y);
			else
			{
				hide();
				metricNodeDeleteA(activenode);
				draw();
				metricGoNode(activenode);
			}
			break;
		}
		case vmMetricNewLine:
		{
			int uc = ((DIM_CONTUR*)(metric + 1) + activecontur)->NodesCount - 2;
			hide();
			metricNodeDeleteB(activecontur, max2(0, uc));
			draw();
			break;
		}
		case vmMetricRotate:
		case vmMetricScale:
			break;
	}
//
	showstatus();
	return 0;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyUp()
{
	return metricNodeKeyMove(activenode, meMoveUp);
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyDown()
{
	return metricNodeKeyMove(activenode, meMoveDown);
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyLeft()
{
	return metricNodeKeyMove(activenode, meMoveLeft);
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyRight()
{
	return metricNodeKeyMove(activenode, meMoveRight);
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyToNearest()
{
	if(!metric)
		return 1;
//
	switch(mode)
	{
		case vmMetricNone:
		{
			double x = 0,
			       y = 0;
			mouseposget(x, y);
			under(x, y);
			if(activenode == -1)
				metricGoNearest(x, y);
			break;
		}
		case vmMetricNewLine:
		case vmMetricRotate:
		case vmMetricScale:
			break;
	}
//
	showstatus();
	return 0;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyToNext()
{
	if(!metric)
		return 1;
//
	switch(mode)
	{
		case vmMetricNone:
		{
			double x = 0,
			       y = 0;
			mouseposget(x, y);
			under(x, y);
			if(activenode == -1)
				metricGoNearest(x, y);
			else
			{
				DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
				DIM_NODE *n0 = NDim__Nodes(metric),
				         *n1 = n0 + activenode;
				for(int a = 0; a < metric->ContursCount; a++, contur++)
				{
					if(contur->NodesCount > (n1 - n0))
						break;
					n0 += contur->NodesCount;
				}
				if(ItClosed(n0, contur->NodesCount))
				{
					if((n1 - n0) == (contur->NodesCount - 1))//last
						n1 = n0 + 1;
					else if((n1 - n0) == (contur->NodesCount - 2))//last - 1
						n1++; //n1 = n0; // EE - 29-Mar-04  14:23:11
					else
						n1++;
				}
				else
				{
					if((n1 - n0) == (contur->NodesCount - 1))//last
						n1 = n0;
					else
						n1++;
				}
				n0 = NDim__Nodes(metric);
//				metricGoNode(n1 - n0);  // EE - 10-Jun-08  12:01:04 - walk through current contour
				metricGoNode((activenode < NDim__NodesCount(metric) - 1) ? activenode + 1 : 0); // EE - 10-Jun-08  12:01:04 - walk through all contours
			}
			break;
		}
		case vmMetricNewLine:
		case vmMetricRotate:
		case vmMetricScale:
			break;
	}
//
	showstatus();
	return 0;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyToPrev()
{
	if(!metric)
		return 1;
//
	switch(mode)
	{
		case vmMetricNone:
		{
			double x = 0,
			       y = 0;
			mouseposget(x, y);
			under(x, y);
			if(activenode == -1)
				metricGoNearest(x, y);
			else
			{
				DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
				DIM_NODE *n0 = (DIM_NODE*)(contur + metric->ContursCount),
				         *n1 = n0 + activenode;
				for(int a = 0; a < metric->ContursCount; a++, contur++)
				{
					if(contur->NodesCount > (n1 - n0))
						break;
					n0 += contur->NodesCount;
				}
				if(ItClosed(n0, contur->NodesCount))
				{
					if((n1 - n0) == 0)
						n1 = n0 + contur->NodesCount - 2;
					else
						n1--;
				}
				else
				{
					if((n1 - n0) == 0)
						n1 = n0 + contur->NodesCount - 1;
					else
						n1--;
				}
				n0 = (DIM_NODE*)((DIM_CONTUR*)(metric + 1) + metric->ContursCount);
//				metricGoNode(n1 - n0); // EE - 10-Jun-08  12:01:04 - walk through current contour
				metricGoNode((activenode > 0) ? activenode - 1 : NDim__NodesCount(metric) - 1); // EE - 10-Jun-08  12:01:04 - walk through all contours
			}
			break;
		}
		case vmMetricNewLine:
		case vmMetricRotate:
		case vmMetricScale:
			break;
	}
//
	showstatus();
	return 0;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyToFirst()
{
	if(mode != vmMetricNone)
		return 1;
//
	int contur = metricContur(activenodeA());
	if(contur < 0)
		metricGoNode(0);
	else
		metricGoNode(metricNodeB(contur, 0));
//
	return 0;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyToLast()
{
	if(mode != vmMetricNone)
		return 1;
//
	int contur = metricContur(activenodeA()),
	     n = NDim__NodesCount(metric);
	if(n > 0)
	{
		if(contur < 0)
			metricGoNode(n - 1);
		else
			metricGoNode(metricNodeB(contur,
				((DIM_CONTUR*)(metric + 1) + contur)->NodesCount - 1));
	}
//
	return 0;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeySelectAll()
{
	if(mode != vmMetricNone)
		return 1;
//
	int n = NDim__NodesCount(metric);
	DIM_NODE *node = NDim__Nodes(metric);
	hide();
	for(int a = 0; a < n; a++)
		node[a].Flags |= DIM_NODE::dnSelected;
	draw();
//
	return 0;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyMakeFirstNode()
{
	if(!metric)
		return 1;
//
	switch(mode)
	{
		case vmMetricNone:
		{
			double x = 0,
			       y = 0;
			mouseposget(x, y);
			under(x, y);
			if(activenode == -1)
				metricGoNearest(x, y);
			else
				{
				hide();
				DIM_NODE *nodes,*nodes1;
				nodes = metricNodeA(activenode);
				if (nodes != NULL)
					{
					DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
					int nc = metricContur(activenode);
					int nn = metricConturNode(activenode);
					contur += nc;
					nodes  -= nn;
					if ((nn > 0) && ItClosed(nodes,contur->NodesCount))
						{
						nodes1 = new DIM_NODE[contur->NodesCount];
						memcpy(nodes1,nodes,contur->NodesCount*sizeof(DIM_NODE));
						memcpy(nodes,nodes1+nn,(contur->NodesCount-nn)*sizeof(DIM_NODE));
						memcpy(nodes+(contur->NodesCount-nn-1),nodes1,(nn+1)*sizeof(DIM_NODE));
						nodes[0].Flags &= ~DIM_NODE::dnPenUp;
						if (nodes1[contur->NodesCount-1].Flags & DIM_NODE::dnPenUp)
							nodes[contur->NodesCount-nn-1].Flags |=  DIM_NODE::dnPenUp;
						else
							nodes[contur->NodesCount-nn-1].Flags &= ~DIM_NODE::dnPenUp;
						for(int a = 0; a < contur->NodesCount; a++)
							nodes[a].Flags &= ~DIM_NODE::dnSelected;
						delete[] nodes1;

						activenode = metricNodeB(nc,0);
						}
					}
				draw();
				}
			break;
		}
		case vmMetricNewLine:
		case vmMetricRotate:
		case vmMetricScale:
			break;
	}
//
	showstatus();
	return 0;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyConturOpen()
{
	int rs = 1;
	if(!metric)
		return rs;
//
	hide();
	rs = metricConturOpen(metricContur(activenodeA()));
	draw();
//
	showstatus();
	return rs;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyConturClose()
{
	int rs = 1;
	if(!metric)
		return rs;
//
	hide();
	rs = metricConturClose(metricContur(activenodeA()));
	draw();
//
	showstatus();
	return rs;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyConturInvert()
{
	int rs = 1;
	if(!metric)
		return rs;
//
	hide();
	rs = metricConturInvert(metricContur(activenodeA()));
	draw();
//
	showstatus();
	return rs;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyConturDivide()
{
	int rs = 1;
	if(!metric)
		return rs;
//
	hide();
	int a = activenodeA();
	rs = metricConturDivide(metricContur(a),metricConturNode(a));
	draw();
//
	showstatus();
	return rs;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyConturMerge()
{
	int rs = 1;
	if(!metric)
		return rs;
//
	hide();
	int a = activenodeA();
	rs = metricConturMerge(metricContur(a));
	draw();
//
	showstatus();
	return rs;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeySnapContur()
{
	int rs = 1;
	if(!metric)
		return rs;

	int a = activenodeA();
	if (mode == vmMetricNewLine)
		a --;
	if (a < 0)
		return rs;
	if (metricContur(a - 1) != metricContur(a))
		return rs;

	XYD xyd[2];
	DIM_NODE *dn,*nodes=NULL;
	dn = metricNodeA(a - 1);
	xyd[0].x = dn[0].x;
	xyd[0].y = dn[0].y;
	xyd[1].x = dn[1].x;
	xyd[1].y = dn[1].y;
	int n = snapconturdim(xyd[0],xyd[1],nodes);
	if (nodes && (n > 1))
	{
		hide();

		int i,j,cntn;
		cntn = NDim__NodesCount(metric);
		dn = NDim__Nodes(metric);
		for (i=0; i<cntn; i++,dn++)
			dn->Flags &= ~DIM_NODE::dnSelected;
		i = a;
		int con  = metricContur(i);
		int node = metricConturNode(i);
		node --;
		for (j=1; j<n; j++)
			{
			dim2pix(nodes[j].x,nodes[j].y);
			i = metricNodeInsertB(con,node+j,nodes[j].x,nodes[j].y);
			dn = NDim__Nodes(metric);
			if (i == -1)
				break;
			if ((j == 1) && (i > 0))
				{
				dn[i-1].dXOut = nodes[j-1].dXOut;
				dn[i-1].dYOut = nodes[j-1].dYOut;
				}
			dn[i].dXIn = nodes[j].dXIn;
			dn[i].dYIn = nodes[j].dYIn;
			if (j < n - 1)
				{
				dn[i].dXOut = nodes[j].dXOut;
				dn[i].dYOut = nodes[j].dYOut;
				}
//			if (mode == vmMetricNone)
//				dn[i].Flags |= DIM_NODE::dnSelected;
			i ++;
			}
		metricNodeDeleteA(i);
		if (mode == vmMetricNewLine)
			activenode = i;

		rs = 0;
		draw();
	}
	if (nodes)
		delete[] nodes;

	showstatus();
	return rs;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::ModeMain()
{
	if(!metric)
		return 1;
//
	int rs = metricPassivate();
//
	showstatus();
	return rs;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::ModeInputNew()
{
	if(!metric)
		return 1;
//
	int rs = metricInputContinue(metric->ContursCount);
//
	showstatus();
	return rs;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::ModeInputContinue()
{
	if(!metric)
		return 1;
//
	int rs = metricInputContinue(metricContur(activenodeA()));
//
	showstatus();
	return rs;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::ModeRotate(double _cx, double _cy)
{
	int rs = 0;

	if ((mode & vmViewModes) == vmMetricRotate)
	{
		if (mode & vmViewDown)
			mode = vmMetricNone;
		else
			rs = metricPassivate();
	}
	else
	{
		rs = metricPassivate();
		rotcenter.x = _cx;
		rotcenter.y = _cy;
		rotangle = 0.0;
		mode = vmMetricRotate | vmViewDown;
	}

	showstatus();
	return rs;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::FixX(bool _fix)
{
	if (_fix)
	{
		DIM_NODE *node = metricNodeA(activenode - 1);
		if(node)
		{
			double x = node->x,
			       y = node->y;
			dim2pix(x, y);
			curr_xy.x = x;
		}
	}

	x_fixed = _fix;
	return x_fixed;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::FixY(bool _fix)
{
	if (_fix)
	{
		DIM_NODE *node = metricNodeA(activenode - 1);
		if(node)
		{
			double x = node->x,
			       y = node->y;
			dim2pix(x, y);
			curr_xy.y = y;
		}
	}

	y_fixed = _fix;
	return y_fixed;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::XFixed()
{
	return x_fixed;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::YFixed()
{
	return y_fixed;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::IsActive()
{
	if(metric)
		return 1;
	return 0;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::IsChanged()
{
int changed=0,isx_len,len;

isx_len = NDim__MetricLen(isx_metric);
len     = NDim__MetricLen(metric);
if ((isx_metric != NULL) || (metric != NULL))
	{
	if ((isx_metric != NULL) && (metric != NULL))
		{
		if (isx_len == len)
			{
			if (len)
				changed = memcmp(isx_metric,metric,len);
			}
		else
			changed = -1;
		}
	else
		changed = -1;
	}
/*
if(metric)
	changed = -1;
*/
return(changed);
}
//---------------------------------------------------------------
int TDgtMetricEditor::ItClosed(DIM_NODE *_nodes, int _count)
{
//	if(mode == vmMetricNewLine)
//		return 0;
//
	if((_count < 2) || !_nodes)
		return 0;
	if((_nodes->x != _nodes[_count - 1].x) ||
	   (_nodes->y != _nodes[_count - 1].y))
//	if(memcmp(nodes, nodes + _count - 1, sizeof(*nodes)))
		return 0;
//
	return 1;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::metricCorrect(DIM_METRIC *&_metric)
{
double xx,yy;

	if(!_metric)
		return 1;
//
	if((mode == vmMetricMove) && (activenode != -1))
	{
		DIM_NODE *n0 = (DIM_NODE*)((DIM_CONTUR*)(_metric + 1) + _metric->ContursCount),
		         *n1 = n0 + activenode;
		if(n1->Flags & DIM_NODE::dnSelected)
		{
			DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
			for(int a = 0; a < _metric->ContursCount; a++, contur++)
			{
				int n = contur->NodesCount - 1;
				if(ItClosed(n0, contur->NodesCount))
				{
					if((n0[0].Flags & DIM_NODE::dnSelected) ||
					   (n0[n].Flags & DIM_NODE::dnSelected))
					{
						setxy(n0[0], n0[0].x + dxy.x, n0[0].y + dxy.y);
						setxy(n0[n], n0[n].x + dxy.x, n0[n].y + dxy.y);
					}
					n0++;
					for(int a = 1; a < n; a++, n0++)
						if(n0->Flags & DIM_NODE::dnSelected)
							setxy(*n0, n0->x + dxy.x, n0->y + dxy.y);
					n0++;
				}
				else
					for(int a = 0; a < contur->NodesCount; a++, n0++)
						if(n0->Flags & DIM_NODE::dnSelected)
							setxy(*n0, n0->x + dxy.x, n0->y + dxy.y);
			}
		}
		else
		{
			DIM_CONTUR *contur = (DIM_CONTUR*)(metric + 1);
			for(int a = 0; a < _metric->ContursCount; a++, contur++)
			{
				if(contur->NodesCount > (n1 - n0))
					break;
				n0 += contur->NodesCount;
			}
			int n = contur->NodesCount - 1;
			if(ItClosed(n0, contur->NodesCount))
			{
				if((n1 == n0) || (n1 == (n0 + n)))
				{
					setxy(n0[0], n0[0].x + dxy.x, n0[0].y + dxy.y);
					setxy(n0[n], n0[n].x + dxy.x, n0[n].y + dxy.y);
				}
				else
					setxy(*n1, n1->x + dxy.x, n1->y + dxy.y);
			}
			else
				setxy(*n1, n1->x + dxy.x, n1->y + dxy.y);
		}
	}
//
	if((mode == vmMetricSelect) && (_metric == metric))
	{
		int n = NDim__NodesCount(_metric);
		DIM_NODE *n0 = NDim__Nodes(_metric);
		double x0 = min2(xy0.x, xy1.x),
			   y0 = min2(xy0.y, xy1.y),
			   x1 = max2(xy0.x, xy1.x),
		       y1 = max2(xy0.y, xy1.y);
/* EE - 09-Jun-03  17:37:22 - � �������� �������� �����������. ���� ���������� �������� dim2pix (��.����)
		pix2dim(x0, y0);
		pix2dim(x1, y1);
*/
		for(int a = 0; a < n; a++, n0++)
			{
			xx = n0->x;
			yy = n0->y;
			dim2pix(xx,yy); // EE - 09-Jun-03  17:37:22
			if((xx >= x0) && (xx <= x1) && (yy >= y0) && (yy <= y1))
				n0->Flags |=  DIM_NODE::dnSelected;
			else
				n0->Flags &= ~DIM_NODE::dnSelected;
			}
	}
//
	if((mode == vmMetricNewLine) && (_metric == metric))
	{
		metricNodeDeleteA(activenode);
	}
//
	if(mode == vmMetricRotate)
	{
		double cx = rotcenter.x;
		double cy = rotcenter.y;
		dim2pix(cx, cy);
		dxy.x = cos(rotangle);
		dxy.y = sin(rotangle);
		int n = NDim__NodesCount(_metric);
		DIM_NODE *nodes = NDim__Nodes(_metric);
		for (int a = 0; a < n; a++)
		{
			XYD xyd[3];
			xyd[0].x = nodes[a].x;
			xyd[0].y = nodes[a].y;
			xyd[1].x = nodes[a].x + nodes[a].dXIn;
			xyd[1].y = nodes[a].y + nodes[a].dYIn;
			xyd[2].x = nodes[a].x + nodes[a].dXOut;
			xyd[2].y = nodes[a].y + nodes[a].dYOut;
			for (int i = 0; i < 3; i++)
			{
				dim2pix(xyd[i].x, xyd[i].y);
				xyd[i].x -= cx;
				xyd[i].y -= cy;
				double x = (xyd[i].x * dxy.x - xyd[i].y * dxy.y) + cx;
				double y = (xyd[i].x * dxy.y + xyd[i].y * dxy.x) + cy;
				pix2dim(x, y);
				xyd[i].x = x;
				xyd[i].y = y;
			}
			setxy(nodes[a], xyd[0].x, xyd[0].y);
			nodes[a].dXIn  = xyd[1].x - xyd[0].x;
			nodes[a].dYIn  = xyd[1].y - xyd[0].y;
			nodes[a].dXOut = xyd[2].x - xyd[0].x;
			nodes[a].dYOut = xyd[2].y - xyd[0].y;
		}
	}
//
	return 0;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::MetricDerivesClear(DIM_METRIC *_metric)
{
	if(!_metric)
		return 1;
//
	DIM_NODE *nodes = NDim__Nodes(_metric);
	int      count = NDim__NodesCount(_metric);
	for(int a = 0; a < count; a++)
	{
		nodes[a].dXIn  = 0.0;
		nodes[a].dYIn  = 0.0;
		nodes[a].dXOut = 0.0;
		nodes[a].dYOut = 0.0;
//		nodes[a].dZIn  = 0.0;
//		nodes[a].dZOut = 0.0;
	}
//	_metric->Flags &= ~DIM_METRIC::exDerives;
//
	return 0;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::MetricDerivesAuto(DIM_METRIC *_metric)
{
	if(MetricDerivesClear(_metric))
		return 1;
	if(_metric->ContursCount < 1)
		return 0;
//
	DIM_CONTUR *contur = (DIM_CONTUR*)(_metric + 1);
	DIM_NODE *nodes = NDim__Nodes(_metric);
	for(int a = 0; a < _metric->ContursCount; a++)
	{
		int n = contur->NodesCount - 1;
		for(int b = 1; b < n; b++)
		{
			double dx = nodes[b+1].x - nodes[b-1].x;
			double dy = nodes[b+1].y - nodes[b-1].y;
			double h  = hypot(dx,dy);
			if (h != 0.0)
				{
				double h_in  = hypot(nodes[b].x-nodes[b-1].x,nodes[b].y-nodes[b-1].y);
				double h_out = hypot(nodes[b+1].x-nodes[b].x,nodes[b+1].y-nodes[b].y);
				nodes[b].dXIn  = dx * h_in  / h;
				nodes[b].dYIn  = dy * h_in  / h;
				nodes[b].dXOut = dx * h_out / h;
				nodes[b].dYOut = dy * h_out / h;
				}

/* EE - 02-Apr-09  16:17:34 - see above
			nodes[b].dXIn = nodes[b].dXOut = (nodes[b + 1].x - nodes[b - 1].x) / 2.0;
			nodes[b].dYIn = nodes[b].dYOut = (nodes[b + 1].y - nodes[b - 1].y) / 2.0;
*/
		}
		if(ItClosed(nodes, contur->NodesCount))
		{
			double dx = nodes[1].x - nodes[n-1].x;
			double dy = nodes[1].y - nodes[n-1].y;
			double h  = hypot(dx,dy);
			if (h != 0.0)
				{
				double h_in  = hypot(nodes[0].x-nodes[n-1].x,nodes[0].y-nodes[n-1].y);
				double h_out = hypot(nodes[1].x-nodes[0].x,nodes[1].y-nodes[0].y);
				nodes[0].dXIn  = dx * h_in  / h;
				nodes[0].dYIn  = dy * h_in  / h;
				nodes[0].dXOut = dx * h_out / h;
				nodes[0].dYOut = dy * h_out / h;
				}

/* EE - 02-Apr-09  16:17:34 - see above
			nodes[0].dXIn = nodes[0].dXOut = (nodes[1].x - nodes[n - 1].x)/2.0;
			nodes[0].dYIn = nodes[0].dYOut = (nodes[1].y - nodes[n - 1].y)/2.0;
*/
			nodes[n].dXIn  = nodes[0].dXIn;
			nodes[n].dYIn  = nodes[0].dYIn;
			nodes[n].dXOut = nodes[0].dXOut;
			nodes[n].dYOut = nodes[0].dYOut;
		}
		nodes += contur->NodesCount;
		contur++;
	}
//	_metric->Flags |= DIM_METRIC::exDerives;
//
	return 0;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::under_derive(double _x, double _y)
{

derivenode = 0;

if (metric)
	{
	DIM_NODE *nodes = NDim__Nodes(metric);
	int      count = NDim__NodesCount(metric);
	for(int a = 0; a < count; a++)
		{
		if (nodes[a].Flags & DIM_NODE::dnSelected)
			{
			double x,y;
			if ((nodes[a].dXIn != 0.0) || (nodes[a].dYIn != 0.0))
				{
				x = nodes[a].x - nodes[a].dXIn / dFactor;
				y = nodes[a].y - nodes[a].dYIn / dFactor;
				dim2pix(x,y);
				if ((fabs(x-_x) < nodeSize) && (fabs(y-_y) < nodeSize))
					{
					derivenode = -(a + 1);
					break;
					}
				}
			if ((nodes[a].dXOut != 0.0) || (nodes[a].dYOut != 0.0))
				{
				x = nodes[a].x + nodes[a].dXOut / dFactor;
				y = nodes[a].y + nodes[a].dYOut / dFactor;
				dim2pix(x,y);
				if ((fabs(x-_x) < nodeSize) && (fabs(y-_y) < nodeSize))
					{
					derivenode = a + 1;
					break;
					}
				}
			}
		}
	}

return(derivenode);
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::SetSnapMode(bool _snapmode)
{
	hide();
	snapmode = _snapmode;
	snapped = 0;
	draw();
	return snapmode;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::SnapMode()
{
	return snapmode;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::snappos(double &_x, double &_y, ID_OBJECT &_obj)
{
	int snap = 0;
	return snap;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::snapconturdim(XYD _xyd0, XYD _xyd1, DIM_NODE *&_nodes)
{
	_nodes = NULL;
	return 0;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::ostoclipboard()
{
	return(1);
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::osfromclipboard()
{
	return(1);
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyToClipboard()
{
	int rs = 1;

	rs = ostoclipboard();
	return rs;
}
//---------------------------------------------------------------------------
int TDgtMetricEditor::KeyFromClipboard()
{
	int rs = 1;
	if(!metric)
		return rs;
//
	hide();
	rs = osfromclipboard();
	draw();
	if (!rs)
		KeyToNext();
//
	showstatus();
	return rs;
}
//---------------------------------------------------------------------------
