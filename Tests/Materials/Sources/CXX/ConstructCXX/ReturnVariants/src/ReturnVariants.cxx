// ReturnVariants.cxx - различные виды return; для отладки ReplaceReturn#

class OGRLineString
{
	private : OGRLineString * const pc_OGRLineString = this;  //- const pointer to OGRLineString;

	public : OGRLineString *getPointIterator() const;
			 const char *getGeometryName() const;
			 void veryVoidMethod() const;
			 static void failsToSupply() 
			 {
				 return;
			 }
};

const char * OGRLineString::getGeometryName() const
{
	return "LINESTRING"; 
}

OGRLineString *OGRLineString::getPointIterator() const
{	
	return pc_OGRLineString;
}

void OGRLineString::veryVoidMethod() const
{
	return;
}

int main(int argc, char* argv[]) 
{
	OGRLineString OGRLine;
	OGRLineString * const pOGRLine = OGRLine.getPointIterator();
	OGRLine.getGeometryName();

	OGRLine.veryVoidMethod();
	//
	return 0;
}