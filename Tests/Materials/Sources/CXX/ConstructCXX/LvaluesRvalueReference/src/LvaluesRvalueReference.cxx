//#include <iostream>
//#include <string>

void lvalue(int &p_int, int value)
{
	p_int = value;
}

int rvalue(int p_int)
{
	int r_int = p_int * p_int;
	return r_int;
}

int *reference_call(int p_int)
{
	return &p_int;
}

int main()
{
	int p_int = 0;
	int n_int = 2;

	lvalue(p_int, n_int);
	n_int = rvalue(p_int);

	//std::cout << "p_int is lvalue : " << p_int << '\n';
	//std::cout << "p_int is rvalue : " << n_int << '\n';

	int *pp_int = reference_call(p_int);
	int v_int = *pp_int;

	//std::cout << "v_int value is equal to p_int : " << v_int << '\n';

	//return 0;
}
