#include "stdio.h"

namespace D {
	int d1;
	void f(char a_char)
	{
		printf("%c\n", a_char);
	}
}
using namespace D; // introduces D::d1, D::f, D::d2, D::f,
				   //  E::e, and E::f into global namespace!

int d1; // OK: no conflict with D::d1 when declaring
namespace E {
	int e;
	void f(int a_int)
	{
		printf("%d\n", a_int);
	}
}
namespace D { // namespace extension
	int d2;
	using namespace E; // transitive using-directive
	void f(int a_int)
	{
		printf("%d\n", a_int);
	}
}

int main() {
	//d1++; // error: ambiguous ::d1 or D::d1?
	::d1++; // OK
	D::d1++; // OK
	d2++; // OK, d2 is D::d2
	e++; // OK: e is E::e due to transitive using
	E::f(1); // error: ambiguous: D::f(int) or E::f(int)?
	f('a'); // OK: the only f(char) is D::f(char)
	return 0;
}
