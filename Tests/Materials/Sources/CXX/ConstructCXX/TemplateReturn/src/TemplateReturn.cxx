// TemplateReturn.cpp

template <typename T>
T minimum(const T& lhs, const T& rhs)
{
	return lhs < rhs ? lhs : rhs;
}

int main()
{
	int i_min = -1;
	int i_max = 10;
	int result = minimum(i_min, i_max);
}