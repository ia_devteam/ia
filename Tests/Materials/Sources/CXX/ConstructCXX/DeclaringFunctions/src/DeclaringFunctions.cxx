#include <iostream>
#include <string>
#include <stdlib.h>
 
// declaration in namespace(file) scope
// (the definition is provided later)
int f1();
 
// simple function with a default argument, returning nothing
void f0(const std::string& arg = "world")
{
    std::cout << "Hello, " << arg << '\n';
}
 
// function returning a pointer to f0
//auto fp11() -> void(*)(const std::string&)
//{
//    return f0;
//}
 
// function returning a pointer to f0, pre-C++11 style
void (*fp03())(const std::string&)
{
    return f0;
}

// factorial calculator
long factorial (long a)
{
  if (a > 1)
   return (a * factorial (a-1));
  else
   return 1;
}

int main()
{
    f0();
    //fp11()("test");
    fp03()("again");
    int f2(std::string); // declaration in function scope
    std::cout << f2("bad12") << '\n';
	long number = 9;
  	std::cout << number << "! = " << factorial (number) << std::endl;
	return 0;
}
 
// simple non-member function returning int
int f1()
{
    return 42;
}
 
// function with an exception specification and a function try block
int f2(std::string str)
{ 
    return atoi(str.c_str());
}
