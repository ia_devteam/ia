#include <iostream>
#include <vector>
#include <algorithm>

struct Printer { // generic functor
	std::ostream& os;
	Printer(std::ostream& os) : os(os) {}
	template<typename T>
	void operator()(const T& obj) { os << obj << ' '; } // member template
};

int main()
{
	std::vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	std::for_each(v.begin(), v.end(), Printer(std::cout));
	std::string s = "abc";
	std::for_each(s.begin(), s.end(), Printer(std::cout));
}