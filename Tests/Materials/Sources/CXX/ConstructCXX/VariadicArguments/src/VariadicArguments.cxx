#include <stdio.h>   
#include <stdarg.h> 
#define EMPTY  

#define CHECK1(x, ...) if (!(x)) { printf(__VA_ARGS__); }  
#define CHECK2(x, ...) if ((x)) { printf(__VA_ARGS__); }  
#define CHECK3(...) { printf(__VA_ARGS__); }  
//#define MACRO(s, ...) printf(s, __VA_ARGS__)  

//  Declaration, but not definition, of ShowVar.  
void ShowVar(char *szTypes, ...);

int main() {
	CHECK1(0, "here %s %s %s", "are", "some", "varargs1(1)\n");
	CHECK1(1, "here %s %s %s", "are", "some", "varargs1(2)\n");   // won't print  

	CHECK2(0, "here %s %s %s", "are", "some", "varargs2(3)\n");   // won't print  
	CHECK2(1, "here %s %s %s", "are", "some", "varargs2(4)\n");

	// always invokes printf in the macro  
	CHECK3("here %s %s %s", "are", "some", "varargs3(5)\n");

	//MACRO("hello, world\n");

	printf("\n\nVariadic args in function\n\n");
	ShowVar("fcsi", 32.4f, 'a', "Test string", 4);

	//MACRO("error\n", EMPTY); // would cause error C2059, except VC++   
							 // suppresses the trailing comma  
}

//  ShowVar takes a format string of the form  
//   "ifcs", where each character specifies the  
//   type of the argument in that position.  
//  
//  i = int  
//  f = float  
//  c = char  
//  s = string (char *)  
//  
//  Following the format specification is a variable   
//  list of arguments. Each argument corresponds to   
//  a format character in the format string to which   
// the szTypes parameter points   
void ShowVar(char *szTypes, ...) {
	va_list vl;
	int i;

	//  szTypes is the last argument specified; you must access   
	//  all others using the variable-argument macros.  
	va_start(vl, szTypes);

	// Step through the list.  
	for (i = 0; szTypes[i] != '\0'; ++i) {
		union Printable_t {
			int     i;
			float   f;
			char    c;
			char   *s;
		} Printable;

		switch (szTypes[i]) {   // Type to expect.  
		case 'i':
			Printable.i = va_arg(vl, int);
			printf("%i\n", Printable.i);
			break;

		case 'f':
			Printable.f = va_arg(vl, double);
			printf("%f\n", Printable.f);
			break;

		case 'c':
			Printable.c = va_arg(vl, char);
			printf("%c\n", Printable.c);
			break;

		case 's':
			Printable.s = va_arg(vl, char *);
			printf("%s\n", Printable.s);
			break;

		default:
			break;
		}
	}
	va_end(vl);
}
