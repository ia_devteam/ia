int my_int_func(int x)
{
	//printf("%d\n", x);
	if (x == 0)
		return 0;
	else if (x < 0)
		return -1;
	else
		return 1;
}


int main()
{
	int(*foo)(int);
	foo = &my_int_func;

	/* call my_int_func (note that you do not need to write (*foo)(2) ) */
	int v1 = foo(-2);
	/* but if you want to, you may */
	//int v1 = (*foo)(2);

	return (*foo)(2);
}