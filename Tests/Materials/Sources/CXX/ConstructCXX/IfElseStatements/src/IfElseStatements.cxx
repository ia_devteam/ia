// IfElseStatements.cxx 
#include <stdio.h>  
  
int main()   
{  
   int x = 0;  
   if (x == 0)  
   {  
      printf("x is 0!\n");  
   }  
   else  
   {  
      printf("x is not 0!\n"); // this statement will not be executed  
   }  
  
   x = 2;  
   if (x == 0)  
   {  
      printf("x is 0!\n"); // this statement will not be executed  
   }  
   else if (x == 1) 
   {  
      printf("x is 1!\n");  
   }
   else
	   printf("x is greater than 1!\n");
  
   return 0;  
}  