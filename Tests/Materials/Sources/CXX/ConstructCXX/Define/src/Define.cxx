// Define.cxx

// Macro to define cursor lines   
#define CURSOR(top, bottom) (((top) << 8) | (bottom))
#define IS_ODD(value)(value % 2)

bool isALPHA(char ch);
bool isDIGIT(char ch);

#define USERFUN 287
#define VAR 293
#define SNARFWORD \
	d = tokenbuf; \
	while (isALPHA(*s) || isDIGIT(*s) || *s == '_') \
	    *d++ = *s++; \
	*d = '\0'; \
	d = tokenbuf; \
	if (*s == '(') \
	    idtype = USERFUN; \
	else \
	    idtype = VAR;

//
#define MAX_RES 5
long mCURSORS[MAX_RES];

int main()
{
	int c_top = 40;
	int c_bottom = 0;
	for (int iT = 0; iT < MAX_RES; iT++)
	{
		c_top = c_top * 2;
		c_bottom = int((c_top / 4) * 3);
		mCURSORS[iT] = CURSOR(c_top, c_bottom);
	}
	//
	int odd_val = IS_ODD(8);
	int noodd_val = IS_ODD(31);

	// one source code
	char perl_src[] = "#!/ usr / bin / perl\nprint \"Hello, world!\n\";";
	char *tokenbuf = &perl_src[0];
	char *s = &perl_src[0];
	char *d;
	int idtype = 0;
	SNARFWORD;

	// another source code
	char perl_src2[] = "use Mojolicious::Lite;\nget '/' = > {text = > 'Hello World!'}\n;app->start;";
	tokenbuf = &perl_src2[0];
	s = &perl_src2[0];
	idtype = 0;
	SNARFWORD;

	return 0;
}

bool isALPHA(char ch)
{
	unsigned int uint_ch = (unsigned int)(ch);
	if ( uint_ch >= 65 && uint_ch <= 90 )
		return true;
	if ( uint_ch >= 97 && uint_ch <= 122 )
		return true;
	return false;
}
bool isDIGIT(char ch)
{
	unsigned int uint_ch = (unsigned int)(ch);
	if (uint_ch >= 48 && uint_ch <= 57)
		return true; 
	return false;
}