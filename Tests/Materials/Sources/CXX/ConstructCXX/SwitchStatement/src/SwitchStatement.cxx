// switch_statement1.cpp  
//#include <stdio.h>  

//using namespace std;

int main(int argc, char* argv[]) {
	char *buffer = "Any character stream";
	int capa, lettera, nota;
	char c;
	capa = lettera = nota = 0;

	while (c = *buffer++)   // Walks buffer until NULL  
	{
		switch (c)
		{
		case 'A':
			capa++;
			break;
		case 'a':
			lettera++;
			break;
		default:
			nota++;
		}
	}
	//printf("\nUppercase a: %d\nLowercase a: %d\nTotal: %d\n",
	//	capa, lettera, (capa + lettera + nota));


	switch (*argv[1])
	{
		// Error. Unreachable declaration.  
		//char szChEntered[] = "Character entered was: ";

	case 'a':
	{
		// Declaration of szChEntered OK. Local scope.  
		char szChEntered[] = "Character entered was: ";
		//cout << szChEntered << "a\n";
	}
	break;

	case 'b':
		// Value of szChEntered undefined.  
		//cout << szChEntered << "b\n";
		break;

	default:
		// Value of szChEntered undefined.  
		//cout << szChEntered << "neither a nor b\n";
		break;
	}

}