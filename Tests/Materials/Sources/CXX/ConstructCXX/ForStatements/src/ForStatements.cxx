#include <stdio.h>  
#include <string>
#include <iostream>
#include <vector>
using namespace std;

int main() {
	
	// classic C-style for statement
	char* line = "H e  \tl\tlo World\0";
	int space = 0;
	int tab = 0;
	int i;
	int max = string(line).length();
	for (i = 0; i < max; i++)
	{
		if (line[i] == ' ')
		{
			space++;
		}
		if (line[i] == '\t')
		{
			tab++;
		}
	}

	cout << "For statement #1" << endl;
	cout << "Number of spaces: " << space << endl;
	cout << "Number of tabs: " << tab << endl;
	
	space = 0;
	tab = 0;
	for (int iT = 0; iT < max; iT++)
	{
		if (line[iT] == ' ')
		{
			space++;
		}
		if (line[iT] == '\t')
		{
			tab++;
		}
	}

	cout << "For statement #2" << endl;
	cout << "Number of spaces: " << space << endl;
	cout << "Number of tabs: " << tab << endl;

	// 'for each' C++ statement 
	//string MyString = string("abcd");
	//for each (char c in MyString)
	//	cout << c;
	//cout << endl;

	//range-based 'for' statement
	// Basic 10-element integer array.  
	int x[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

	// Range-based for loop to iterate through the array.  
	for (int y : x) { // Access by value using a copy declared as a specific type.   
					  // Not preferred.  
		cout << y << " ";
	}
	cout << endl;

	// The auto keyword causes type inference to be used. Preferred.  

	for (auto y : x) { // Copy of 'x', almost always undesirable  
		cout << y << " ";
	}
	cout << endl;

	for (auto &y : x) { // Type inference by reference.  
						// Observes and/or modifies in-place. Preferred when modify is needed.  
		cout << y << " ";
	}
	cout << endl;

	for (const auto &y : x) { // Type inference by reference.  
							  // Observes in-place. Preferred when no modify is needed.  
		cout << y << " ";
	}
	cout << endl;
	cout << "end of integer array test" << endl;
	cout << endl;

	// Create a vector object that contains 10 elements.  
	vector<double> v;
	for (int i = 0; i < 10; ++i) {
		v.push_back(i + 0.14159);
	}

	// Range-based for loop to iterate through the vector, observing in-place.  
	for (const auto &j : v) {
		cout << j << " ";
	}
	cout << endl;
	cout << "end of vector test" << endl;

	return 0;
}
