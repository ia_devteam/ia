14/12/2009 : 2.28.3

This is another bug fixing release:
#603871 -  [Audio project] Song order inverted when moving songs up in the tracklist
#590648 -  Text in cover editor does not show up automatically
#603605 -  .checksum.md5 ("file integrity check" plugin) broken when using "Increase compatibility with Windows"
#602872 -  User is unable to fast-forward neither rewing sound preview
#601872 -  Audio project : Modifying added silence length fails
#538060 -  Brasero fails to read cue file when PATH contains a non fully qualified path
#601871 -  Wrong remaining space when audio CD of size greater than 70 min
#601496 -  Select disc image selector allows to selecd pictures
#601109 -  burning disks with burn:// doesn't set the disk label
#600293 -  Fails to build error: cannot convert to a pointer type - Carlos Garcia Campos
#599620 -  stray underline in button label - Matthias Clasen
#602566 -  libbrasero-burn has broken headers
#599655 -  brasero crashed with SIGSEGV in g_main_context_dispatch()
#588323 -  crash in Disc Copier: Dragging a track from Ba...
#598055 -  Brasero says "Audio CD successfully burnt" after creating a video DVD
#600007 -  Memory leaks in brasero - Eric Sesterhenn

and many other smaller bug fixes.

Translations:
Nils-Christoph Fiedler <fiedler@medienkompanie.de>: new Low German translation
Joan Duran <jodufi@gmail.com>: Updated Catalan translation
Petr Kovar <pknbe@volny.cz>: Updated Czech translation by Adrian Gunis
Aleksander Łukasiewicz <aleksander@lukasiewicz.org>: Updated Polish translation
Claude Paroz <claude@2xlibre.net>: Updated French translation
Milo Casagrande <milo@ubuntu.com>: Updated Italian translation
Jorge González <jorgegonz@svn.gnome.org>: Updated Spanish translation
Gabor Kelemen <kelemeng@gnome.hu>: Updated Hungarian translation
Mads Lundby <lundbymads@gmail.com>: Updated Danish translation
Matej Urbančič <mateju@svn.gnome.org>: Updated Slovenian translation
Mario Blättermann <mariobl@gnome.org>: Updated German translation
Leonid Kanter <leon@asplinux.ru>: Updated Russian translation
Rodrigo L. M. Flores <rlmflores@src.gnome.org>: Updated Brazilian Portuguese Translation
Daniel Nylander <po@danielnylander.se>: Updated Swedish translation
Jorge González <jorgegonz@svn.gnome.org>: Updated Spanish translation

Homepage: http://www.gnome.org/projects/brasero

Please report bugs to: http://bugzilla.gnome.org/browse.cgi?product=brasero

Mailing List for User and Developer discussion: brasero-list@gnome.org

GIT Repository: http://git.gnome.org/cgit/brasero/

Thanks to all the people who contributed to this release through patches, translation, advices, artwork, bug reports.


19/10/2009 : 2.28.2

This is another bug release:

This should fix for good the remaining ejection problems and various (also remaining) drive/medium management problems after the migration from HAL to GIO.

Other bug fixes:
#598908 -  data project: multiple file rename do not work
#598149 -  Remember volume title
#597808 -  Crash when launching with -e option
#598306 -  brasero crashed with SIGSEGV in brasero_burn_session_set_image_output_full()
#597584 -  make burning audio cds with brasero work (Bastien Nocera)


Translations:

Milo Casagrande <milo@ubuntu.com>: Updated Italian translation
Lucian Adrian Grijincu <lucian.grijincu@gmail.com>: Updated Romanian translation
Leonid Kanter <leon@asplinux.ru>: Updated Russian translation

Homepage: http://www.gnome.org/projects/brasero

Please report bugs to: http://bugzilla.gnome.org/browse.cgi?product=brasero

Mailing List for User and Developer discussion: brasero-list@gnome.org

GIT Repository: http://git.gnome.org/cgit/brasero/

Thanks to all the people who contributed to this release through patches, translation, advices, artwork, bug reports.



5/10/2009 : 2.28.1

This is a bug fix release that fixes important bugs.

Highlights:
- fix problems with drive/medium management (file descriptor leak preventing ejection, improved threading safety)
- fix problems with libburn/libisofs (rare case data corruption, workaround TAO+dummy problem)
- some crashes fixed (596625 among others)
- memory leaks

Updated translations:
Italian by Milo Casagrande <milo@ubuntu.com>
Catalan by Joan Duran <jodufi@gmail.com>
Slovenian by Matej Urbančič <mateju@svn.gnome.org>

New translations:
Simplified Chinese by 苏运强 <wzssyqa@gmail.com>

Homepage: http://www.gnome.org/projects/brasero

Please report bugs to: http://bugzilla.gnome.org/browse.cgi?product=brasero

Mailing List for User and Developer discussion: brasero-list@gnome.org

GIT Repository: http://git.gnome.org/cgit/brasero/

Thanks to all the people who contributed to this release through patches, translation, advices, artwork, bug reports.


