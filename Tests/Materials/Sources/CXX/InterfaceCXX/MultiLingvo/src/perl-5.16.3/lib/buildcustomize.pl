#!perl

# We are miniperl, building extensions
# Reset @INC completely, adding the directories we need, and removing the
# installed directories (which we don't need to read, and may confuse us)
@INC = (q /root/rpmbuild/BUILD/perl-5.16.3/cpan/AutoLoader/lib ,
        q /root/rpmbuild/BUILD/perl-5.16.3/dist/Carp/lib ,
        q /root/rpmbuild/BUILD/perl-5.16.3/dist/Cwd ,
        q /root/rpmbuild/BUILD/perl-5.16.3/dist/Cwd/lib ,
        q /root/rpmbuild/BUILD/perl-5.16.3/dist/ExtUtils-Command/lib ,
        q /root/rpmbuild/BUILD/perl-5.16.3/dist/ExtUtils-Install/lib ,
        q /root/rpmbuild/BUILD/perl-5.16.3/cpan/ExtUtils-MakeMaker/lib ,
        q /root/rpmbuild/BUILD/perl-5.16.3/dist/ExtUtils-Manifest/lib ,
        q /root/rpmbuild/BUILD/perl-5.16.3/cpan/File-Path/lib ,
        q /root/rpmbuild/BUILD/perl-5.16.3/ext/re ,
        q /root/rpmbuild/BUILD/perl-5.16.3/dist/Term-ReadLine/lib ,
        q /root/rpmbuild/BUILD/perl-5.16.3/lib ,
        q . );
