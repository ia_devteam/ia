// custom countdown using while
#include <iostream>
using namespace std;
int n;
int main()
{
	n = 10;

	while (n>0) {
		cout << n << ", ";
		--n;
	}

	cout << "liftoff!\n";
}

int whileloop() 
{
	n = 10;

	while (n>0) {
		cout << n << ", ";
		--n;
	}

	cout << "liftoff!\n";
	return 0;
}

void ifcases()
{
	int x = 5;
	if (x == 100)
		cout << "x is 100";
	if (x == 100)
	{
		cout << "x is ";
		cout << x;
	}
	if (x == 100) { cout << "x is "; cout << x; }
	if (x == 100)
		cout << "x is 100";
	else
		cout << "x is not 100";
	if (x > 0)
		cout << "x is positive";
	else if (x < 0)
		cout << "x is negative";
	else
		cout << "x is 0";
}

void dowhile()
{
	string str;
	do {
		cout << "Enter text: ";
		//getline(cin, str);
		//cout << str << '\n';
	} while (strcmp(str.c_str(),"goodbye"));
}

void forloop()
{
	for (int n = 10; n>0; n--) {
		cout << n << ", ";
	}
	cout << "liftoff!\n";
	for (int n = 10; n>0; n--) cout << n << ", "; 
	string str{ "Hello!" };
	for (char c : str)
	{
		cout << "[" << c << "]";
	}
	cout << '\n';
	for (auto c : str)
		cout << "[" << c << "]";
}

void breakst()
{
	for (int n = 10; n>0; n--)
	{
		cout << n << ", ";
		if (n == 3)
		{
			cout << "countdown aborted!";
			break;
		}
	}
}

void continuest()
{
	for (n = 10; n>0; n--) {
		if (n == 5) continue;
		cout << n << ", ";
	}
	cout << "liftoff!\n";
}

void gotost()
{
	int n = 10;
mylabel:
	cout << n << ", ";
	n--;
	if (n>0) goto mylabel;
	cout << "liftoff!\n";
}

int switchst()
{
	int x = 1;
	switch (x) {
	case 0:
	case 1:
		cout << "x is 1";
		break;
	case 2:
	{
			  cout << "x is 2";
			  break;
	}
	default:
		cout << "value of x unknown";
	}

	return main();
}

template <class SomeType>
SomeType sum(SomeType a, SomeType b)
{
	return a + b;
}

int maintest() {
	int i = 5, j = 6, k;
	double f = 2.0, g = 0.5, h;
	k = sum<int>(i, j);
	h = sum<double>(f, g);
	cout << k << '\n';
	cout << h << '\n';
	return 0;
}

template <class T, int N>
T fixed_multiply(T val)
{
	return val * N;
}

int mainnontype() {
	std::cout << fixed_multiply<int, 2>(10) << '\n';
	std::cout << fixed_multiply<int, 3>(10) << '\n';
	return - 100;
}

class Rectangle {
	int width, height;
public:
	void set_values(int, int);
	int area() { return width*height; }
	Rectangle::Rectangle(int a, int b) {
		width = a;
		height = b;
	}
};

class ChildRect : public Rectangle {
protected:
	int a;
private:
	int b;
public:
	int c;
	ChildRect():Rectangle(1,1){
		b = 2;
	}
};

void Rectangle::set_values(int x, int y) {
	width = x;
	height = y;
}

void tryCatch()
{
	try
	{
		throw 20;
	}
	catch (int e)
	{
		cout << "An exception occurred. Exception Nr. " << e << '\n';
		throw;
	}
	ChildRect test;
	test.area();
}


#define OBRACE {
#define CBRACE }

void test() OBRACE
int a;
CBRACE
