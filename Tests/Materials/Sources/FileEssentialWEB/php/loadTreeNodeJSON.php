<?php
//Header("Cache-Control: no-cache");

require_once( $_SERVER['DOCUMENT_ROOT'] . '/spo_modules/config/config.php' );
require_once( CLASS_QUERY );

global $SQL;

//---------------------  ---------------------//
if(!isset($_GET['id']))
	$_GET['id'] = 0;
	
$params = $_GET;
//f_print_r($params);
$res = $SQL->sql_query_mod("SELECT DISTINCT
							mv.kod_omenu
						,	mv.kodv_omenu
						,	mv.kod_menu
						,	mv.naimp_omenu
						,	mv.rubrika_omenu
						,	mv.param
						,	m.link_menu
						,	m.prim_menu
						,	m.pr_inf
						,	m.is_newwin
						,	CASE WHEN (mv.kod_menu = 0) THEN 1 ELSE 0 END	AS	haschilds
						FROM
						(
						     SELECT ss.rubrika_omenu 
						     FROM sys.tsys_menu ss
							       INNER JOIN spo.tspo_menu m ON ss.kod_menu = m.kod_menu
							       INNER JOIN spo.tspo_menu_group mg ON m.kod_menu = mg.kod_menu
							       INNER JOIN sys.tsys_group_otv go ON mg.kod_group = go.kod_group
						     WHERE
						     	TRUE
						     #ss.kods_project & @kods_project@ > 0#
							 AND go.kod_otv = 640
						     ORDER BY ss.rubrika_omenu
						) ttt
							INNER JOIN sys.tsys_menu mv ON ttt.rubrika_omenu LIKE rtrim(mv.rubrika_omenu) || '%'
							LEFT JOIN spo.tspo_menu m ON mv.kod_menu = m.kod_menu
						WHERE
							TRUE
						AND	kod_omenu != 0
						#mv.kods_project & @kods_project@ > 0
						#mv.kodv_omenu = @id@
						ORDER BY mv.rubrika_omenu", $params);
//f_print_r($res);
//---------------------  ---------------------//
/*Mandatory parameters for this tag are:
text - label of the node
id - id of the node
Optional parameters for this tag are:
tooltip - tooltip for the node
im0 - image for node without children (tree will get images from the path specified in setImagePath(url) method)
im1 - image for opened node with children
im2 - image for closed node with children
aCol - colour of not selected item
sCol - colour of selected item
select - select node on load (any value)
style - node text style
open - show node opened (any value)
call - call function on select(any value)
checked - check checkbox if exists(any value)
child - spec. if node has children (1) or not (0)

imheight - height of the icon
imwidth - width of the icon
topoffset - offset of the item from the node above
radio - if non empty, then children of this node will have radiobuttons*/

echo "{id:'".$_GET['id']."', item:[\n";
foreach($res as $key => $value){
	if ($key != 0) echo "," ;
	echo "{id:'".$value['kod_omenu']."',  text:'".iconv('cp1251', 'utf-8', $value['naimp_omenu'])."', child:'".$value['haschilds'].
	"', tooltip: '".($value['kod_menu']!=0?iconv('cp1251', 'utf-8', $value['prim_menu']):'')."', link: '".getCompiledMenuLink($value['link_menu']).
	"', is_newwin: '".$value['is_newwin']."'}\n";
}
echo "]}\n";
?>