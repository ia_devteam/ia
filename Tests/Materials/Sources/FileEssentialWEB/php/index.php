<?php
Header("Cache-Control: no-cache");
header("Content-Type: text/html; charset=windows-1251");
if(!isset($_SERVER['DOCUMENT_ROOT'])){
    switch (true) {
        case preg_match('/(^.*)spo_modules/',$_SERVER['SCRIPT_FILENAME'], $laParts):
                      $lsDocumentRoot = $laParts[1];
                    break;
        case preg_match('/(^.*)resource/',$_SERVER['SCRIPT_FILENAME'], $laParts):
                     $lsDocumentRoot = $laParts[1];
                    break;
        default:
                    $lsDocumentRoot = dirname($_SERVER['SCRIPT_FILENAME']);
    }
    require_once( $lsDocumentRoot . '/spo_modules/config/document_root.php' );
}
require_once( $_SERVER['DOCUMENT_ROOT'] . '/spo_modules/config/config.php' );
require_once( CLASS_QUERY );
require_once( PHP_LIB . 'cookies.php' );
require_once( $_SERVER['DOCUMENT_ROOT'] . PROJECT_RESOURCE_DIR . '/GNU_source/DHTMLX_Tree/php/functions.php' );
include($_SERVER['DOCUMENT_ROOT']."/spo_modules/lib/php/theme_switcher.php");

global $SQL, $user, $ga_KodsPrj, $vid_menu;

//$lbCeckFiles = 1; // ����� ������  $user['kod'] - sporoot ������ �� ��

switch( $vid_menu ){
	case 'ObjMenu'  : $lsPrjTitle = '���� �������'; break;
	case 'BaseMenu' : $lsPrjTitle = '������� ����'; break;
	default			: $lsPrjTitle = '����'; break;
}
$la_mCookies = GetInfoCookies();
/**
 * ��� ��������� ������������ ������������ ���� ���������� ������������ ����� ������
 */
$theme_name = ThemeSwitcher::getTheme(); 
?>
<head>
<title><?php echo $lsPrjTitle;?></title>
<meta http-equiv="X-UA-Compatible" content="IE=8"/>
<link rel='stylesheet' type='text/css' href='/resource/CSS/Styles.css.php'>

<?php if ( !(defined('USE_THEMES_IN_PROJECT') && USE_THEMES_IN_PROJECT>0) || !file_exists($_SERVER['DOCUMENT_ROOT'] . "/resource/CSS/themes/".$theme_name."/dhtmlxtree.css") ) {?>
<link rel="STYLESHEET" type="text/css" href="../css/dhtmlxtree.css">
<?php } else {?>
<link rel="STYLESHEET" type="text/css" href="/resource/CSS/themes/<?php echo $theme_name;?>/dhtmlxtree.css">
<?php }?>
<script src="/spo_modules/lib/js/DOMInspector.js"></script>
<script src="/spo_modules/lib/js/main.js"></script>
<script src="/spo_modules/lib/js/cookies.js"></script>
<script src="/resource/GNU_source/DHTMLX_Tree/js/dhtmlxcommon.js"></script>
<script src="/resource/GNU_source/DHTMLX_Tree/js/dhtmlxtree.js"></script>
<script src="/resource/GNU_source/DHTMLX_Tree/js/dhtmlxtree_json.js"></script>
<script src="/resource/GNU_source/DHTMLX_Tree/js/dhtmlxtree_xw.js"></script>
<script src="/resource/GNU_source/DHTMLX_Tree/js/dhtmlxtree_kn.js"></script>
<script src="/resource/GNU_source/DHTMLX_Tree/js/dhtmlxtree_lf.js"></script>
</head>
<body onload="loadTitle();testReload();">
<?php

$arr_prj 	 = getProjects();
//print_r( $arr_prj );
$kol_prj	 = count($arr_prj);

if( !isset($_GET['kods_project']) )
{
	//--- ��������� � ����� ---//
	//������� �������� �� ������������ ���� ��� $ga_KodsPrj
	if( is_array( $ga_KodsPrj ) && intval($la_mCookies['g_nLastPrj']) && in_array($la_mCookies['g_nLastPrj'], $ga_KodsPrj) )
	$current_kods_project = $la_mCookies['g_nLastPrj'];
	else
	$current_kods_project = $arr_prj[0]['kods_project'];

	SetInfoCookie( 'g_nLastPrj', $current_kods_project );
}
else
{
	//--- ���������� � ���� ---//

	if( in_array($_GET['kods_project'], $ga_KodsPrj) && $_GET['kods_project'] != $la_mCookies['g_nLastPrj'] )
	{
		SetInfoCookie( 'g_nLastPrj', $_GET['kods_project'] );
	}

	$current_kods_project = $_GET['kods_project'];
}
if(!$current_kods_project)
die();

if( $lbCeckFiles )
{
	require_once('unused_files.php');
}

if(	$vid_menu == 'ObjMenu' || $vid_menu == 'BaseMenu' ) {
	$la_TmpKod_project = $SQL->sql_query("SELECT kod_project FROM spo.tspo_project WHERE kods_project = ". $current_kods_project);

	switch( $vid_menu ) {
		case 'ObjMenu': if( count($SQL->sql_query("SELECT * FROM spo.tspo_project_user WHERE pr_obj_menu = 1 AND kod_otv = ". $user['kod'])) == 1 )
		$SQL->sql_query("UPDATE spo.tspo_project_user SET kod_project = ". $la_TmpKod_project[0]['kod_project'] ." WHERE pr_obj_menu = 1 AND kod_otv = ". $user['kod']);
		else
		$SQL->sql_query("INSERT INTO spo.tspo_project_user ( kod_project, pr_obj_menu, kod_otv) VALUES (". $la_TmpKod_project[0]['kod_project'] .", 1,". $user['kod'] .");");
		break;
			
		case 'BaseMenu':if( count($SQL->sql_query("SELECT * FROM spo.tspo_project_user WHERE pr_obj_menu = 0 AND kod_otv = ". $user['kod'])) == 1 )
		$SQL->sql_query("UPDATE spo.tspo_project_user SET kod_project = ". $la_TmpKod_project[0]['kod_project'] ." WHERE pr_obj_menu = 0 AND kod_otv = ". $user['kod']);
		else
		$SQL->sql_query("INSERT INTO spo.tspo_project_user ( kod_project, pr_obj_menu, kod_otv) VALUES (". $la_TmpKod_project[0]['kod_project'] .", 0,". $user['kod'] .");");
		break;
	}
}

if ($kol_prj > 1) {
	?>
<div class="treeProjects blueBg" id="treeProjects"><img
	class="CSS_MiniFilter_img" src="/resource/icons/search_16.png"
	onclick="show_hide('treeSearch');"> <select id="ProjectsList"
	class="CarlingSelect" onchange="ProjectsList_change(this);">
	<?php
	foreach($arr_prj as $p_key => $p_value){
		echo "<option value='".$p_value['kods_project']."' ".($current_kods_project == $p_value['kods_project'] ? "selected" : '').">".$p_value['naim_project']."</option>";
	}
	?>
</select></div>
<div class="treeSearch blueBg" id="treeSearch" style="visibility:hidden;"><img
	src='/resource/icons/close_16.png' class="CSS_MiniFilter_img"
	onclick="show_hide('treeProjects');"> <?php 
} else {
	?>
<div class="treeSearch blueBg" id="treeSearch" ><img
	class="CSS_MiniFilter_img" src="/resource/icons/search_16.png"> <?php 
}
?> <input class="CarlingSelect" onkeypress='searchContext(this, event);'
	id='searchText' title='������������ ������(������ enter)'> <select
	class="CarlingSelect" onchange='findInTree(this);' id='searchResult'
	title='������� �� ����������� ������ ������������ ����� ����'>
	<option value='0'>�� ����������</option>
</select></div>
<table class='treeTable'>
	<tr>
		<td colspan=3>
		<div id='mainMenu'></div>
		</td>
	</tr>
</table>
<?php
	// ���������� � ����������� ���� ������ ���� ����� �������� � ����, � ������� kod_omenu ��� kod_reload, � kodv_reload ������� �����
	if( $_GET['kod_reload'] && !$_GET['kodv_reload'] ){
		$lrKodvReload = getMenuItems( array( 'kods_project'=>$current_kods_project, 'kod_omenu'=>$_GET['kod_reload'] ) );

		if( count( $lrKodvReload) )
			foreach( $lrKodvReload AS $value )
			{
				$_GET['kodv_reload'] = $value['kodv_omenu'];break;
			}
	}
?>
<script>
var kods_project = '<?php echo $current_kods_project ?>';
var kod_omenu 	 = '<?php echo ($_GET['kod_omenu']>0?$_GET['kod_omenu']:"undefined") ?>';
var vid_menu 	 = '<?php echo $_GET['vid_menu']; ?>';
var doOpen 		 = [];
var kod_reload   = '<?echo $_GET['kod_reload'];?>';
var kodv_reload  = '<?echo $_GET['kodv_reload'];?>';
//var selectValue
var ctrlDown     = false;


/* ������� ��������� �������� ������ � ����� ����/������� */
function detectKeys(e) {
	if(e.ctrlKey) {
		ctrlDown = true;
	}
	else {
		ctrlDown = false;
	}
}
if( document.all )
    window.attachEvent("onclick" , detectKeys);
else
    window.addEventListener("click", detectKeys, true);

function testReload()
{
	if( kod_reload && kodv_reload )
//		findInTree( null , kod_reload, kodv_reload );
		setTimeout( 'findInTree(null,'+kod_reload+','+kodv_reload+')', 1000 )
}

function loadTitle()
{
	var projectsList = document.getElementById('ProjectsList');
        if(!projectsList) return;
	var titleFrame = parent.document.getElementById('titul');
	
	//--- ������ ������� �������� ������ � ����� ---//
	if( titleFrame )
		titleFrame.src = '/spo_modules/config/titul.php?projectName=' + projectsList.options[projectsList.selectedIndex].text;
	if( projectsList )	
		parent.document.title = projectsList.options[projectsList.selectedIndex].text;	
}

function show_hide(t){
	var Obj = document.getElementById(t);
	Obj.style.visibility = 'visible';

	if(t == 'treeProjects'){
		var pObj = document.getElementById('treeSearch');
	}
	if(t == 'treeSearch'){
		var pObj = document.getElementById('treeProjects');
	}
	pObj.style.visibility = 'hidden';
}

function openItemsDynamicEnd(){
	if(tree.getUserData(0, 'selectIdAfterLoad') != ''){
		var lbMode = Boolean(tree.getUserData(0, 'lbMode'));
		tree.selectItem(tree.getUserData(0, 'selectIdAfterLoad'), lbMode, false);
		tree.setUserData(0, 'selectIdAfterLoad', '');
		tree.setUserData(0, 'lbMode', false);
		//tree.detachEvent("onOpenDynamicEnd");
		//tree.removeEvent("onOpenDynamicEnd");
	}
}

function checkResult(result, lpMode){
	//alert(doOpen[0]+"; "+doOpen[1]+"; "+doOpen[2]+"; "+doOpen[3]+"; "+doOpen[4]+"; ");
	if(result != ''){
		tree.setUserData(0, 'selectIdAfterLoad', result.replace(/(.*,)?([0-9]*)$/g, '$2'));
		tree.setUserData(0, 'lbMode', Boolean(lpMode));
		tree.attachEvent("onOpenDynamicEnd", openItemsDynamicEnd);
		tree.openItemsDynamic(result, false);
	}
	/*if(result.length > 0){
		if(tree.getLevel(result[0]['kod_omenu']) != 0){
			doOpenIds = '';
			for(i = doOpen.length - 1; i >= 0; i--){
				doOpenIds += doOpen[i] + ',';
			}
			doOpenIds += ',' + itemId;
			tree.openItemsDynamic(doOpenIds, false);
		}
		else{
			doOpen[doOpen.length] = result[0]['kodv_omenu'];
			loadParentsNode(result[0]['kodv_omenu']);
		}
	}*/
}

/*function loadParentsNode(id){
	if (id == '')
		return;
	top.loadIFrame = document.getElementById('IFrame_getParentMenuItems_' + id);
	if (undefined == top.loadIFrame)
	{
		top.loadIFrame = document.createElement('IFRAME');
		top.loadIFrame.id = 'IFrame_getParentMenuItems_' + id ;
		top.loadIFrame.style.display = 'none';
		top.loadIFrame.style.width = '100%';
		top.loadIFrame.style.height = '100%';
		document.body.appendChild(top.loadIFrame);
	}

	var l_aSqlExecUrl = './loadTreeNode.php?kod_omenu=' + id + '&kods_project=' + kods_project + "&js_function=parent.checkResult(result);&vid_menu=" + vid_menu;
	top.loadIFrame.src = l_aSqlExecUrl;
}*/

function findInTree(rObj, kod, kodv, lpMode)
{
	var lbMode = Boolean(lpMode);
	if( rObj )
	{
		items = rObj.options[rObj.selectedIndex].value;
		//var RExp = /([0-9]*)-([0-9]*)/gi
		itemId = items.replace(/(.*,)?([0-9]*)$/g, '$2');
		itemParentId = items.replace(/(.*,)?([0-9]*,)([0-9]*)$/g, '$2');
		tree.openItemsDynamic(rObj.options[rObj.selectedIndex].value, true);
	}
	else
	{
		itemId = kod;
		itemParentId = kodv;
		
		if(tree.getLevel(itemId) == 0)
		{
			doOpen.length = 0;
			doOpen[0] = itemParentId;
			/////////////
			top.loadIFrame = document.getElementById('IFrame_loadExpandTreeNodesString_' + itemId);
			if (undefined == top.loadIFrame)
			{
				top.loadIFrame = document.createElement('IFRAME');
				top.loadIFrame.id = 'IFrame_loadExpandTreeNodesString_' + itemId ;
				top.loadIFrame.style.display = 'none';
				top.loadIFrame.style.width = '100%';
				top.loadIFrame.style.height = '100%';
				document.body.appendChild(top.loadIFrame);
			}

			var l_aSqlExecUrl = './loadExpandTreeNodesString.php?kod_omenu=' + itemId + '&kods_project=' + kods_project + "&js_function=parent.checkResult(result, " + lbMode.toString() + ");&vid_menu=" + vid_menu;
			top.loadIFrame.src = l_aSqlExecUrl;
			////////////
			//loadParentsNode(itemParentId);
		}
		else{
			setTimeout('tree.selectItem(' + itemId + ', ' + lbMode.toString() + ', false);', 0);
			/*if(typeof(jQuery) != 'undefined' && (Number(jQuery.browser.version) < 8 && jQuery.browser.msie))
				setTimeout('tree.selectItem('+itemId+', '+lbMode.toString()+', false);', 100);
			else
				tree.selectItem(itemId, lbMode, false);
			*/
		}
	}
	
}
function searchContext(sObj, sEvent){
	if (!sEvent || sEvent.keyCode == 13 || sEvent.type == 'change')
	{
		var l_aDopUrl = '';
		
		var ifr = CheckAndCreateIFR('SelectFrame_searchMenuItems', 0);
		if( vid_menu == 'BaseMenu' )
			var url = './searchMenuItems.php?kods_project=' + kods_project + '&naimp_bmenu=' + sObj.value + '&vid_menu=' + vid_menu;
		else
			var url = './searchMenuItems.php?kods_project=' + kods_project + '&naimp_omenu=' + sObj.value + '&vid_menu=' + vid_menu;
		ifr.src = url;
	}
}
function ProjectsList_change(Obj){
	list_frame 		= parent.frames[1];//document.getElementById('list');
	list_frame = this; // ����������� ��� ���� ,- �.�. ��� ������ �������� ������ �������� ... //rdv
	var RExp 		= /&kods_project=[0-9]*/gi
//	inspectDOM(list_frame);return;
	URL 			= list_frame.frameElement.src;
	URL 			= URL.replace(RExp, '');
	list_frame.frameElement.src 	= URL + '&kods_project=' + Obj.value;
	
	if( typeof(parent.frames[0].frameElement.contentDocument) != 'undefined' && parent.frames[0].frameElement.contentDocument.getElementById('kods_project') )
		parent.frames[0].frameElement.contentDocument.getElementById('kods_project').value = Obj.value;
}
/* ����� � ������� 1.3 � ����� �����������*/
function openWinTab(url, target, attrs)
{
	if(!ctrlDown) {
		View_obj(url, target, 900, 900);
		return;
	}
	if(url.indexOf("javascript:void") != -1 ) {
		return ;		
	}
	var	server_host = document.location.href.replace(/^(http[s]*:\/\/[^\/]*).*/, '$1')
	try
	{
		netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
	}
	catch(e)
	{
			window.open(url);
			return;
	}
	var domWindow = GetNavigatorWindow();
	var gBrowser = domWindow.gBrowser;
	url = server_host + url.replace( /^\.\.\/\.\./, _DOCUMENT_ROOT )

	var source = Components.classes["@mozilla.org/network/standard-url;1"].createInstance(Components.interfaces.nsIURI);
	source.spec = document.location.href;
	for(var i=0; i < gBrowser.mTabContainer.childNodes.length; i++)
		if ( gBrowser.mTabContainer.childNodes[i].id && gBrowser.mTabContainer.childNodes[i].id == target )
		{
			gBrowser.mTabContainer.selectedIndex = i;
			var doc = gBrowser.mTabContainer.childNodes[i].ownerDocument.getElementById('content').contentWindow.document
			if ( doc.location.href != url )
				doc.location.href = url;
			return;
		}
	var win = gBrowser.addTab( url, source );
	win.id = target
	win.tabbed = 1
	gBrowser.selectedTab = win;
	return null;
}

function doOnClick(nodeId,evt){
	
	switch( vid_menu )
	{
		case 'MainMenu' :

			var Url = tree.getUserData(nodeId, "link");
			var is_newwin = tree.getUserData(nodeId, "is_newwin");
			var target_window = tree.getUserData(nodeId, "target_window");
			
			//--- ���� ������ ������� ---//
			if( typeof(_DESKTOP_PRJ) != 'undefined' && _DESKTOP_PRJ > 0 ){
				top.customClass.addWindow( {
					'_url' : Url,
					'text': this.label
				} );
				return true;			
			}
			//------------------------------//
			
			if(is_newwin == 1 || ctrlDown) {
			    //--- ����������� ���� ---//
				openWinTab(Url, target_window);
			}
			else{
				var frm = parent.document.getElementsByName('main');
				//--- ���� ��� �������� ������ ����� ---//
				if(frm[0].src == Url)
				{
					var RExp = /&msec=[0-9]*/gi
					Url = Url.replace(RExp, '');
					var now = new Date();
					var msec = now.getTime();
					frm[0].src = Url + ( Url.indexOf("void(0)") !=-1 ? '' : '&msec=' + msec );
				}
				else
					frm[0].src = Url;
			}
			break;
		case 'ObjMenu'  :
		case 'BaseMenu' :
			var Url = tree.getUserData(nodeId, "link");
			
			var l_aMainWinName = ( vid_menu == 'BaseMenu' ? 'spo_menu_base_main' : 'sys_menu_main' );
			var frm 	= parent.document.getElementsByName( l_aMainWinName );
			frm[0].src 	= Url+ '&kods_project=' + kods_project;
			break;
	}
/* �������� - �������� ����� �� ����� �� �������� �����*/
	var lbOpenState = tree.getOpenState(nodeId); 
	//alert(lbOpenState);
	if( lbOpenState == 0 || lbOpenState == -1) {
		tree.openItem(nodeId);
	}
	else {
		tree.closeItem(nodeId);
	}
}


	function tondrag(id,id2){
		var result;
		
		if( confirm("��������� ����� ���� "+tree.getItemText(id)+" � "+tree.getItemText(id2)+"?") == true )
			result = true;
		else
			result = false;
		
		
		if( result == true ){
			var ifr = CheckAndCreateIFR( 'Select_UPDATE_menu_', 0 );

			if( ifr != null ){
				ifr.src = "/spo_modules/menu/php/Update_menu_sql_exec.php?kod_menu=" + id + "&kodv_menu=" + id2 + "&kods_project=" + kods_project + "&vid_menu=" + vid_menu + "&naimp_menu=" + tree.getItemText(id);
			}
		}
		return result;
	}
    var gsThemeCss   = '<?php echo $theme_name;?>';
    var lsPathImages = "/resource/GNU_source/DHTMLX_Tree/images/";
    
    if( gsThemeCss == 'green')
        lsPathImages += "csh_citron/";
    else{
        lsPathImages += "chs_inic_gpv/";
    }
	tree=new dhtmlXTreeObject("mainMenu","auto","100%",0);
	tree.setImagePath( lsPathImages );
	tree.enableHighlighting(true);
	tree.enableActiveImages(true);
	tree.enableSmartXMLParsing(true);
	tree.enableKeyboardNavigation(true);
	tree.enableLoadingItem("��������...");
	tree.enableFixedMode(true);
	if( typeof(_DESKTOP_PRJ) != 'undefined' && _DESKTOP_PRJ > 0 ){
		tree.setOnRightClickHandler(function(nodeId){
			var curNode = this._globalIdStorageFind(nodeId);
			top.customClass.addShortcut({_shortcut : {
				id 		: curNode.id,
				href	: tree.getUserData(nodeId, "link"),
				title 	: curNode.label
			}});	
		});
	}
	
	tree.setIconSize(17,17);
	
	if( vid_menu != 'MainMenu' ){
		tree.enableDragAndDrop(1);
		tree.setDragHandler(tondrag);
	}
	
	tree.setOnClickHandler(doOnClick);
	tree.setXMLAutoLoading("/resource/GNU_source/DHTMLX_Tree/php/loadTreeNodeXML.php?&kods_project="+kods_project+'&vid_menu='+vid_menu);
	if(kod_omenu != 'undefined')
		tree.loadXML("/resource/GNU_source/DHTMLX_Tree/php/loadTreeNodeXML.php?kods_project="+kods_project+"&kod_omenu="+kod_omenu+'&vid_menu='+vid_menu);
	else
		tree.loadXML("/resource/GNU_source/DHTMLX_Tree/php/loadTreeNodeXML.php?id=0&kods_project="+kods_project+'&vid_menu='+vid_menu);

	/*function enableEllipses( pnLimit )
	{
		pnLimit--;
		var lnTOset = setTimeout(function(){if(!tree.allTreeElemTrace()&&pnLimit>0)enableEllipses(pnLimit);},500 );
	}*/

	tree.enableEllipses( 10 );
	
	//  @anchor ���������� � ����� �������� ������ ����������.
	document.body.setAttribute('onResize', "tree.allTreeElemTrace();" );

</script>

</body>
