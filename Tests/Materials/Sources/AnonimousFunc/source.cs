namespace IA
{
    internal static class Plugin
    {
        public Plugin.Capabilities Capabilities
        {
            get
            {
                IA.Plugin.Capabilities res = Plugin.Capabilities.RERUN | Plugin.Capabilities.REPORTS;
                if (allDllInfo.Where(dll => dll.IsElf).Count() != 0)
                {
                    res |= Plugin.Capabilities.RESULT_WINDOW;
                }
                return res;
            }
        }
    }
}