<?php
	function getFibonacci()
	{
    		$i = 0;
    		$k = 1; //first fibonacci value
    		yield $k;
    		while(true)
    		{
        		$k = $i + $k;
        		$i = $k - $i;
        		yield $k;        
    		}
	}
?>