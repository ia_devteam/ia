﻿using System.Collections.Generic;

namespace YieldReturnStatement
{
    public class YieldReturnStatement
    {
        IEnumerable<int> StatementYieldReturn()
        {
            int a = 0;
            yield return a;
            a++;
            yield return a;
            a++;
            yield return a;
        }
    }
}
