﻿namespace ForeachStatement
{
    public class ForeachStatement
    {
        ForeachStatement()
        {
            int[] arr = { 0, 1, 2 };
            int[] a = new int[arr.Length];

            foreach (int i in arr)
            {
                a[i] = arr[i];
            }
        }
    }
}
