﻿using System;

namespace TryCatchFinallyStatement
{
    public class TryCatchFinallyStatement
    {
        TryCatchFinallyStatement()
        {
            int a = 0;
            try
            {
                a = 1;
            }
            catch (NullReferenceException)
            {
                a = 2;
            }
            catch (OverflowException)
            {
                a = 3;
            }
            finally
            {
                a = 4;
            }
        }
    }
    public class TryCatchStatement
    {
        TryCatchStatement()
        {
            int a = 0;
            try
            {
                if (a == 0)
                {
                    throw new Exception("");
                }
                a++;
            }
            catch (Exception)
            {
                a++;
            }
        }
    }
}
