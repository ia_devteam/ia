﻿using System;

namespace SimpleCalls
{
    class Program
    {
        static int a;
        static void A()
        {
            B();
        }

        static void B()
        {
            while (a < 2)
            {
                a++;
                A();
            }
        }

        static void Main(string[] args)
        {
            if (args.Length > 1)
                a = 1;
            else
                a = 2;
            A();
        }
    }
}
