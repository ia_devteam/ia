﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject
{
    /// <summary>
    /// Реализация интерфейса автомобиля для доставки
    /// </summary>
    public class Car : ICar
    {
        #region Private Fields
        /// <summary>
        /// <see cref="TestProject.ICar.StartLat"/>
        /// </summary>
        private double _startLat;

        /// <summary>
        /// <see cref="TestProject.ICar.StartLon"/>
        /// </summary>
        private double _startLon;

        /// <summary>
        /// <see cref="TestProject.ICar.MaxWeight"/>
        /// </summary>
        private double _maxWeight;
        #endregion Private Fields

        #region Public Properties
        /// <summary>
        /// <see cref="TestProject.ICar.StartLat"/>
        /// </summary>
        public double StartLat
        {
            get { return _startLat; }
        }

        /// <summary>
        /// <see cref="TestProject.ICar.StartLon"/>
        /// </summary>
        public double StartLon
        {
            get { return _startLon; }
        }

        /// <summary>
        /// <see cref="TestProject.ICar.MaxWeight"/>
        /// </summary>
        public double MaxWeight
        {
            get { return _maxWeight; }
        }

        /// <summary>
        /// Цвет автомобиля
        /// </summary>
        public string Color { get; private set; }

        /// <summary>
        /// Маршрут автомобиля
        /// </summary>
        public Route Route { get; private set; }
        #endregion Public Properties
        
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="startLat">Начальная широта</param>
        /// <param name="startLon">Начальная долгота</param>
        /// <param name="maxWeight">Максимальный вес</param>
        public Car(double startLat, double startLon, double maxWeight, string color)
        {
            _startLat = startLat;
            _startLon = startLon;
            _maxWeight = maxWeight;
            Color = color;
        }

        /// <summary>
        /// <see cref="TestProject.ICar.CreateRoute"/>
        /// </summary>
        /// <returns></returns>
        public IRoute CreateRoute()
        {
            Route = new Route(this);
            return Route;
        }
    }
}
