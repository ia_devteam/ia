﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject
{
    /// <summary>
    /// Реализация интерфейса документа доставки
    /// </summary>
    public class Document : IDoc
    {
        /// <summary>
        /// <see cref="TestProject.IDoc.Lat"/>
        /// </summary>
        private double _lat;
        /// <summary>
        /// <see cref="TestProject.IDoc.Lon"/>
        /// </summary>
        private double _lon;
        /// <summary>
        /// <see cref="TestProject.IDoc.Weight"/>
        /// </summary>
        private double _weight;

        /// <summary>
        /// <see cref="TestProject.IDoc.Lat"/>
        /// </summary>
        public double Lat
        {
            get { return _lat; }
        }

        /// <summary>
        /// <see cref="TestProject.IDoc.Lon"/>
        /// </summary>
        public double Lon
        {
            get { return _lon; }
        }

        /// <summary>
        /// <see cref="TestProject.IDoc.Weight"/>
        /// </summary>
        public double Weight
        {
            get { return _weight; }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public Document(double lat, double lon, double weight)
        {
            _lat = lat;
            _lon = lon;
            _weight = weight;
        }
    }
}
