﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {

        private unsafe struct PDS_DATA
        {
            public fixed char fixedBuffer[128];
        }

        private static int PDS_BLOCK_SIZE
        {
            get
            {
                unsafe
                {
                    return sizeof(PDS_DATA);
                }
            }
        }
        
        static void Main(string[] args)
        {
            Console.WriteLine(PDS_BLOCK_SIZE.ToString());
        }
    }
}
