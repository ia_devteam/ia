﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FixUtilits
{
    public partial class FixUtilsMain : Form
    {
        OpenFileDialog ofd;

        public FixUtilsMain()
        {
            InitializeComponent();
            ofd = new OpenFileDialog();
            ofd.InitialDirectory = Environment.CurrentDirectory;
            ofd.Filter = "HTML-файлы отчета Fix'а|*.html";
            ofd.Title = "Выберете файл отчета Fix'а...";
        }

        private void FixUtilsMain_Load(object sender, EventArgs e)
        {
            dataTables1.Enabled = false;
            dataTables1.HideTabControl();
        }

        private void загрузкаОтчетаToolStripMenuItem_Click(object sender, EventArgs e)
        {            
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                this.Text = "Файл: " + ofd.FileName;
                dataTables1.setFixFile = ofd.FileName;
                dataTables1.Run();
                dataTables1.Enabled = true;
                закрытьToolStripMenuItem.Enabled = true;
                загрузкаОтчетаToolStripMenuItem.Enabled = false;
            }
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void закрытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            закрытьToolStripMenuItem.Enabled = false;
            загрузкаОтчетаToolStripMenuItem.Enabled = true;
            dataTables1.Enabled = false;
            dataTables1.ClearAllTableLists();
            dataTables1.HideTabControl();
            this.Text = "Утилита работы с фиксом";
        }

        private void FixUtilsMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            dataTables1.Closing();
        }
    }
}
