﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace FixUtilits
{
    public partial class DataTables : UserControl
    {
        // Класс данных основной таблицы
        class HtmlFirstTable
        {           
            // Каталог с файлами
            public string path;
            // Список файлов
            public List<HtmlFirstTableTag> tagList;
        }

        // Класс данных файла основной таблицы
        class HtmlFirstTableTag
        {
            // Номер файла
            public int num;
            // Имя файла
            public string filename;
            // Дата создания файла
            public string date;
            // Длина байт
            public int bytelength;
            // Количество строк в файле
            public int rowcount;
            // Контрольная сумма
            public string hex;
        }

        // Класс данных таблицы с расширениями
        class HtmlSecondTable
        {
            // Идентификатор расширения
            public int num;
            // Расширение файла
            public string ext;
            // Общее количество файлов 
            public int filescount;
            // Общая длина файлов в байтах
            public int bytelength; 
            // Общее количество строк в файлах
            public int rowlength;
        }

        // Класс данных таблиц с одинаковыми контрольными суммами
        // и одинаковыми именами фалов
        class HtmlThirdTable
        {
            // Идентификатор группы одинаковых значений
            public int groupnum;
            // Номер файла в главной таблице
            public int num;
            // Длина файла в байтах
            public int bytelength;
            // Колическтво строк в файле
            public int rowlength;
            // Имя файла
            public string filename;
            // Контрольная сумма
            public string hex;
            // Каталог с данным файлом
            public string path;
        }

        // Путь до редактируемого файла отчета
        private string pathToHTML;

        // Словарь данных главной таблицы
        Dictionary<int, HtmlFirstTable> firstTable = new Dictionary<int, HtmlFirstTable>();
        // Список данных таблицы с раширениями
        List<HtmlSecondTable> secondTable = new List<HtmlSecondTable>();
        // Список данных таблицы с одинаковыми контрольными суммами
        List<HtmlThirdTable> thirdTable = new List<HtmlThirdTable>();
        // Список данных таблицы с одинаковыми именами
        List<HtmlThirdTable> fourthTable = new List<HtmlThirdTable>();

        // Шаблон пути к каталогу с файлами
        string patternNameSubTable = "<td class=\"ktlg\" COLSPAN=6 ALIGN=CENTER>(.*?)</td>";
        // Шаблон разделителя таблиц в отчете
        string patternStartTableString = "<meta http-equiv=\"Content-Type\"content=\"text/html;charset=windows-1251\" >";
        // Шаблон тэгов с данными о файле для главной таблицы
        string[] patterns1 = {"<td ALIGN=CENTER>(.*?)</td>","<td class=\"ks\" >(.*?)</td>","<td  ALIGN=CENTER>(.*?)</td>",
                                "<td  ALIGN=RIGHT>(.*?)</td>","<td  ALIGN=RIGHT>(.*?)</td>","<td class=\"ks\" ALIGN=CENTER>(.*?)</td>"};
        // Шаблон тэгов с данными о расширении
        string[] patterns2 = {"<td ALIGN=CENTER>(.*?)</td>","<td class=\"ext\" ALIGN=CENTER><B>(.*?)</B></td>",
                                "<td ALIGN=CENTER>(.*?)</td>","<td ALIGN=RIGHT>(.*?)</td>", "<td ALIGN=CENTER>(.*?)</td>"};
        // Шаблон тэгов с данными для таблицы с одинаковыми контрольными суммами
        string[] patterns3 = {"<td ALIGN=CENTER>(.*?)</td>","<td class=\"ext\" ALIGN=LEFT>(.*?)</td>","<td ALIGN=RIGHT>(.*?)</td>",
                                "<td ALIGN=RIGHT>(.*?)</td>","<td class=\"ext\" ALIGN=CENTER>(.*?)</td>", "<td ALIGN=LEFT>(.*?)</td>"};
        // Шаблон тэга о значении группы одинаковых значений
        string pattern3 = "<td ALIGN=CENTER ROWSPAN=(.*?)>(.*?)</td>";
        // Шаблон тэгов с данными для таблицы с одинаковыми именами
        string[] patterns4 = {"<td ALIGN=CENTER>(.*?)</td>","<td class=\"ext\" ALIGN=LEFT>(.*?)</td>","<td ALIGN=RIGHT>(.*?)</td>",
                                "<td ALIGN=RIGHT>(.*?)</td>","<td class=\"ext\" ALIGN=CENTER>(.*?)</td>","<td ALIGN=LEFT>(.*?)</td>"};
        // Текущая выбранная вкладка
        int tabSelectedIndex;
        
        public DataTables()
        {
            InitializeComponent();
        }

        public string setFixFile
        {
            set { pathToHTML = value; }
        }

        private void DataTables_Load(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
            tabControl1.SelectedIndex = 0;
        }

        public void HideTabControl()
        {
            tabControl1.Hide();
        }

        public void Run()
        {
            DisableButtons();
            tabControl1.Hide();
            textBox1.Enabled = false;
            ClearAllTableLists();
            FindAllTables(File.ReadAllText(pathToHTML, Encoding.Default));
            tabControl1.SelectTab(0);
            tabSelectedIndex = tabControl1.SelectedIndex;
            textBox1.Enabled = true;
            button1.Enabled = true;
            button2.Enabled = true;
            button4.Enabled = true;
            tabControl1.Show();
        }

        private void FindAllTables(string p)
        {
            Regex regex = new Regex(patternStartTableString);
            MatchCollection mCol= regex.Matches(p);
            if (mCol.Count != 0)
            {
                string[] ColSplit = regex.Split(p);
                SaveFirstTable(ColSplit[1]);
                SaveSecondTable(ColSplit[2]);
                SaveThirdTable(ColSplit[3]);
                SaveFourthTable(ColSplit[4]);
            }
            richTextBox1.AppendText("Чтение завершено...OK\n");
            richTextBox1.SelectionStart = richTextBox1.TextLength;
            richTextBox1.ScrollToCaret();
        }

        private void SaveFourthTable(string p)
        {
            richTextBox1.AppendText("Чтение таблицы с файлами с одинаковыми именами...\n");
            richTextBox1.SelectionStart = richTextBox1.TextLength;
            richTextBox1.ScrollToCaret();
            Regex rowSpRegex = new Regex(pattern3);
            string[] rSSp = rowSpRegex.Split(p);
            bool b = true;
            int i = 1, k = 0;
            Regex[] regex = new Regex[6];
            Dictionary<int, string[]> d;
            while (b)
            {
                try
                {
                    for (int rs = 0; rs < int.Parse(rSSp[i]); rs++)
                    {
                        d = new Dictionary<int, string[]>();
                        foreach (string str in patterns3)
                        {
                            regex[k] = new Regex(str);
                            d.Add(k, regex[k].Split(p));
                            p = p.Substring(regex[k].Match(p).Index + 1);
                            k++;
                        }
                        k = 0;

                        HtmlThirdTable table = new HtmlThirdTable();
                        table.groupnum = int.Parse(rSSp[i + 1]);
                        table.num = int.Parse(d[0][1]);
                        table.filename = d[1][1];
                        table.bytelength = FromDashToZero(d[2][1]);
                        table.rowlength = FromDashToZero(d[3][1]);
                        table.hex = HexFromRusToEng(d[4][1]);
                        table.path = d[5][1];

                        dataGridView4.Rows.Add(table.groupnum, table.num, table.filename, table.bytelength, table.rowlength, table.hex, table.path);

                        fourthTable.Add(table);
                    }
                    i += 3;
                }
                catch
                {
                    b = false;
                }
            }
        }

        private void SaveThirdTable(string p)
        {
            richTextBox1.AppendText("Чтение таблицы с файлами с одинаковыми КС...\n");
            richTextBox1.SelectionStart = richTextBox1.TextLength;
            richTextBox1.ScrollToCaret();
            Regex rowSpRegex = new Regex(pattern3);
            string[] rSSp = rowSpRegex.Split(p);
            bool b = true;
            int i = 1, k = 0;
            Regex[] regex = new Regex[6];
            Dictionary<int, string[]> d;
            while (b)
            {
                try
                {
                    for (int rs = 0; rs < int.Parse(rSSp[i]); rs++)
                    {
                        d = new Dictionary<int, string[]>();
                        foreach (string str in patterns3)
                        {
                            regex[k] = new Regex(str);
                            d.Add(k, regex[k].Split(p));
                            p = p.Substring(regex[k].Match(p).Index + 1);
                            k++;
                        }
                        k = 0;

                        HtmlThirdTable table = new HtmlThirdTable();
                        table.groupnum = int.Parse(rSSp[i + 1]);
                        table.num = int.Parse(d[0][1]);
                        table.filename = d[1][1];
                        table.bytelength = FromDashToZero(d[2][1]);
                        table.rowlength = FromDashToZero(d[3][1]);
                        table.hex = HexFromRusToEng(d[4][1]);
                        table.path = d[5][1];

                        dataGridView3.Rows.Add(table.groupnum, table.num, table.filename, table.bytelength, table.rowlength, table.hex, table.path);

                        thirdTable.Add(table);
                    }
                    i += 3;
                }
                catch
                {
                    b = false;
                }
            }
        }

        private void SaveSecondTable(string p)
        {
            richTextBox1.AppendText("Чтение таблицы с расширениями...\n");
            richTextBox1.SelectionStart = richTextBox1.TextLength;
            richTextBox1.ScrollToCaret();
            int i = 0;
            bool b = true;
            Regex[] regex = new Regex[5];
            Dictionary<int, string[]> d;
            while (b)
            {
                try
                {
                    d = new Dictionary<int, string[]>();
                    foreach (string str in patterns2)
                    {
                        regex[i] = new Regex(str);
                        d.Add(i, regex[i].Split(p));
                        p = p.Substring(regex[i].Match(p).Index + 1);
                        i++;
                    }
                    i = 0;

                    HtmlSecondTable table = new HtmlSecondTable();
                    table.num = int.Parse(d[0][1]);
                    table.ext = d[1][1];
                    table.filescount = FromDashToZero(d[2][1]);
                    table.bytelength = FromDashToZero(d[3][1]);
                    table.rowlength = FromDashToZero(d[4][1]);

                    dataGridView2.Rows.Add(table.num, table.ext, table.filescount, table.bytelength, table.rowlength);
                 
                    secondTable.Add(table);
                }
                catch
                {
                    b = false;
                }
            }

        }

        private void SaveFirstTable(string p)
        {
            richTextBox1.AppendText("Чтение общей таблицы...\n");
            richTextBox1.SelectionStart = richTextBox1.TextLength; 
            richTextBox1.ScrollToCaret();
            Regex regex = new Regex(patternNameSubTable);
            MatchCollection mCol = regex.Matches(p);
            if (mCol.Count != 0)
            {
                string[] ColSplit = regex.Split(p);
                int k = 0;
                for (int i = 1; i < ColSplit.Length; i += 2)
                    if (ColSplit[i + 1] != null)
                    {
                        SaveFirstTableTags(k, ColSplit[i], ColSplit[i + 1]);
                        k++;
                    }
            }
        }

        private void SaveFirstTableTags(int k, string p, string p_2)
        {
            richTextBox1.AppendText("\t" + p + "\n");
            richTextBox1.SelectionStart = richTextBox1.TextLength;
            richTextBox1.ScrollToCaret();
            HtmlFirstTable fTable = new HtmlFirstTable();
            List<HtmlFirstTableTag> lTable = new List<HtmlFirstTableTag>();
            int i = 0;
            bool b = true;
            Regex[] regex = new Regex[6];
            Dictionary<int, string[]> d;            
            while (b)
            {
                try
                {
                    d = new Dictionary<int, string[]>();
                    foreach (string str in patterns1)
                    {
                        regex[i] = new Regex(str);
                        d.Add(i, regex[i].Split(p_2));
                        p_2 = p_2.Substring(regex[i].Match(p_2).Index + 1);
                        i++;
                    }
                    i = 0;

                    HtmlFirstTableTag table = new HtmlFirstTableTag();
                    table.num = int.Parse(d[0][1]);
                    table.filename = d[1][1];
                    table.date = d[2][1];
                    table.bytelength = FromDashToZero(d[3][1]);
                    table.rowcount = FromDashToZero(d[4][1]);
                    table.hex = HexFromRusToEng(d[5][1]);

                    dataGridView1.Rows.Add(table.num, table.filename, table.date, table.bytelength, table.rowcount, table.hex);

                    lTable.Add(table);
                }
                catch
                {
                    b = false;
                }
            }
            fTable.path = p;
            fTable.tagList = lTable;
            firstTable.Add(k, fTable);
        }

        private int FromDashToZero(string p)
        {
            if (p == "-")
                return 0;
            else return int.Parse(p);
        }

        public void ClearAllTableLists()
        {
            firstTable.Clear();
            secondTable.Clear();
            thirdTable.Clear();
            fourthTable.Clear();
            dataGridView1.Rows.Clear();
            dataGridView2.Rows.Clear();
            dataGridView3.Rows.Clear();
            dataGridView4.Rows.Clear();
        }

        private void DisableButtons()
        {
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
            button4.Enabled = false;
        }

        private void GenerateHTML()
        {
            richTextBox1.AppendText("Создание отчета...\n");
            richTextBox1.SelectionStart = richTextBox1.TextLength;
            richTextBox1.ScrollToCaret();
            StreamWriter sw = new StreamWriter("Test_Html.html", false, Encoding.Default);
            HTMLtags Tags = new HTMLtags();

            foreach (string str in Tags.StartHat)
                sw.WriteLine(str);
            AddFirstTableTags(sw);
            foreach (string str in Tags.SecondHat)
                sw.WriteLine(str);
            AddSecondTableTags(sw);
            foreach (string str in Tags.ThirdHat)
                sw.WriteLine(str);
            AddLastTableTags(sw, thirdTable);
            foreach (string str in Tags.FourthHat)
                sw.WriteLine(str);
            AddLastTableTags(sw, fourthTable);
            foreach (string str in Tags.FifthHat)
                sw.WriteLine(str);
            sw.Close();
            richTextBox1.AppendText("Создание отчета завершено...OK\n");
            richTextBox1.SelectionStart = richTextBox1.TextLength;
            richTextBox1.ScrollToCaret();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(textBox1.Text))
            {
                switch (tabControl1.SelectedIndex)
                {
                    case 0:
                        for (int i = 0; i < dataGridView1.RowCount; i++)
                            if (textBox1.Text == ".")
                            {
                                if (!dataGridView1[1, i].FormattedValue.ToString().Contains(textBox1.Text.Trim()))
                                {
                                    dataGridView1.CurrentCell = dataGridView1[0, i];
                                    break;
                                }
                            }
                            else
                            {
                                if (dataGridView1[1, i].FormattedValue.ToString().Contains(textBox1.Text.Trim()))
                                {
                                    dataGridView1.CurrentCell = dataGridView1[0, i];
                                    break;
                                }
                            }
                        break;
                    case 1:
                        for (int i = 0; i < dataGridView2.RowCount; i++)
                            if (dataGridView2[1, i].FormattedValue.ToString().Contains(textBox1.Text.Trim()))
                            {
                                dataGridView2.CurrentCell = dataGridView2[0, i];
                                break;
                            }  
                        break;
                    case 2:
                        for (int i = 0; i < dataGridView3.RowCount; i++)
                            if (dataGridView3[2, i].FormattedValue.ToString().Contains(textBox1.Text.Trim()))
                            {
                                dataGridView3.CurrentCell = dataGridView3[0, i];
                                break;
                            }
                        break;
                    case 3:
                        for (int i = 0; i < dataGridView4.RowCount; i++)
                            if (dataGridView4[2, i].FormattedValue.ToString().Contains(textBox1.Text.Trim()))
                            {
                                dataGridView4.CurrentCell = dataGridView4[0, i];
                                break;
                            }
                        break;
                }
                button2.Enabled = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Удалить строку?", "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                if (dataGridView1.SelectedRows.Count != 0)
                {
                    switch (tabControl1.SelectedIndex)
                    {
                        case 1:
                            try
                            {
                                string ext = dataGridView2[1, dataGridView2.SelectedRows[0].Index].FormattedValue.ToString();
                                for (int i = 0; i < int.Parse(dataGridView2[2, dataGridView2.SelectedRows[0].Index].FormattedValue.ToString()); i++)
                                    DecrimentFirstTable(ext);
                                DecrimentSecondTable(ext);
                                DecrimentThirdTable(ext);
                                DecrimentFourthTable(ext);
                                button3.Enabled = true;
                                button4.Enabled = false;
                            }
                            catch
                            {
                                MessageBox.Show("Не удалось удалить строку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            break;
                    }
                    textBox1.Enabled = false;
                    button1.Enabled = false;
                    button2.Enabled = false;
                }
            }
        }

        private void DecrimentFourthTable(string p)
        {
            fourthTable = fourthTable.Where(x => !x.filename.Substring(x.filename.Length - p.Length).Equals(p)).ToList();
        }

        private void DecrimentThirdTable(string p)
        {
            thirdTable = thirdTable.Where(x => !x.filename.Substring(x.filename.Length - p.Length).Equals(p)).ToList(); 
        }
        
        private void DecrimentSecondTable(string p)
        {
            secondTable = secondTable.Where(x => !x.ext.Equals(p)).ToList();
        }

        private void DecrimentFirstTable(string p)
        {
            foreach (int k in firstTable.Keys)
            {
                foreach (HtmlFirstTableTag ftg in firstTable[k].tagList)
                {
                    if (ftg.filename.Substring(ftg.filename.Length - p.Length).Equals(p))
                    {
                        firstTable[k].tagList.Remove(ftg);
                        return;
                    }
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ReWriteDataGridView();
            ReBuildFirstTable();
            ReBuildSecondTable();
            ReBuildThirdTable();
            ReBuildFourthTable();
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = false;
            button4.Enabled = true;
            textBox1.Enabled = true;
        }

        private void ReWriteDataGridView()
        {
            dataGridView1.Rows.Clear();
            dataGridView2.Rows.Clear();
            dataGridView3.Rows.Clear();
            dataGridView4.Rows.Clear();
        }

        private void ReBuildFourthTable()
        {
            int group, count = 1;
            HtmlThirdTable elem;
            List<HtmlThirdTable> ls = new List<HtmlThirdTable>();
            if (fourthTable.Count() > 0)
            {
                elem = fourthTable[0];
                group = fourthTable[0].groupnum;
                for (int i = 1; i < fourthTable.Count(); i++)
                {
                    if (fourthTable[i].groupnum == group)
                        count++;
                    else
                        if (count == 1)
                            ls.Add(elem);
                        else
                        {
                            elem = fourthTable[i];
                            count = 1;
                            group = fourthTable[i].groupnum;
                        }
                }
            }
            fourthTable = fourthTable.Where(x => !ls.Contains(x)).ToList();
            foreach (HtmlThirdTable table in fourthTable)
                dataGridView4.Rows.Add(table.groupnum, table.num, table.filename, table.bytelength, table.rowlength, table.hex, table.path);
        }

        private void ReBuildThirdTable()
        {
            int group, count = 1;
            HtmlThirdTable elem;
            List<HtmlThirdTable> ls = new List<HtmlThirdTable>();
            if (thirdTable.Count() > 0)
            {
                elem = thirdTable[0];
                group = thirdTable[0].groupnum;
                for (int i = 1; i < thirdTable.Count(); i++)
                {
                    if (thirdTable[i].groupnum == group)
                        count++;
                    else
                        if (count == 1)
                            ls.Add(elem);
                        else
                        {
                            elem = thirdTable[i];
                            count = 1;
                            group = thirdTable[i].groupnum;
                        }
                }
            }
            thirdTable = thirdTable.Where(x => !ls.Contains(x)).ToList();
            foreach (HtmlThirdTable table in thirdTable)
                dataGridView3.Rows.Add(table.groupnum, table.num, table.filename, table.bytelength, table.rowlength, table.hex, table.path);
        }

        private void ReBuildSecondTable()
        {
            int i = 1;
            foreach (HtmlSecondTable sec in secondTable)
            {
                sec.num = i;
                i++;
                dataGridView2.Rows.Add(sec.num, sec.ext, sec.filescount, sec.bytelength, sec.rowlength);
            }
        }

        private void ReBuildFirstTable()
        {
            int i = 1;
            foreach (int k in firstTable.Keys)
            {
                //if (firstTable[k].tagList.Count == 0)
                //    firstTable.Remove(k);
                //else
                //{
                    foreach (HtmlFirstTableTag ftg in firstTable[k].tagList)
                    {
                        ftg.num = i;
                        i++;
                        dataGridView1.Rows.Add(ftg.num, ftg.filename, ftg.date, ftg.bytelength, ftg.rowcount, ftg.hex);
                    }
                //}
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            GenerateHTML();
        }

        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            tabSelectedIndex = tabControl1.SelectedIndex;
        }

        /// <summary>
        /// Добавление в отчет основных данных из главной таблицы
        /// </summary>
        /// <param name="sw"></param>
        private void AddFirstTableTags(StreamWriter sw)
        {
            int FullRowCount = 0;
            int FullByteLength = 0;
            string HEX = "";
            int Files = 0;
            int RowCount = 0;
            int ByteLength = 0;
            string HexSum = "";
            foreach (int k in firstTable.Keys)
            {
                RowCount = 0;
                ByteLength = 0; 
                HexSum = "";
                sw.WriteLine("<tr>");
                sw.WriteLine("<td class=\"ktlg\" COLSPAN=6 ALIGN=CENTER>{0}</td>", firstTable[k].path);
                sw.WriteLine("</tr>");
                foreach (HtmlFirstTableTag ftg in firstTable[k].tagList)
                {                    
                    sw.WriteLine("<tr>");
                    sw.WriteLine("<td ALIGN=CENTER>{0}</td>", ftg.num);
                    sw.WriteLine("<td class=\"ks\" >{0}</td>", ftg.filename);
                    sw.WriteLine("<td  ALIGN=CENTER>{0}</td>", ftg.date);
                    sw.WriteLine("<td  ALIGN=RIGHT>{0}</td>", ftg.bytelength);
                    sw.WriteLine("<td  ALIGN=RIGHT>{0}</td>", FromZeroToDash(ftg.rowcount));
                    sw.WriteLine("<td class=\"ks\" ALIGN=CENTER>{0}</td>", FromEngToRus(ftg.hex));
                    sw.WriteLine("</tr>");
                    RowCount += ftg.rowcount;
                    ByteLength += ftg.bytelength;
                    //if (string.IsNullOrWhiteSpace(ftg.hex))
                    //{
                    //    // write to bug trace
                    //}
                    HexSum = SumHexes(HexSum, ftg.hex);
                }
                sw.WriteLine("<tr>");
                sw.WriteLine("<td class=\"itog1\" ALIGN=CENTER COLSPAN=3 >итого: файлов - {0}</td>", firstTable[k].tagList.Count);
                sw.WriteLine("<td class=\"itog1\" ALIGN=RIGHT>{0}</td>", ByteLength);
                sw.WriteLine("<td class=\"itog1\" ALIGN=RIGHT>{0}</td>", RowCount);
                sw.WriteLine("<td class=\"itog_ks1\" ALIGN=CENTER>{0}</td>", FromEngToRus(HexSum));
                sw.WriteLine("</tr>");
                FullRowCount += RowCount;
                FullByteLength += ByteLength;
                HEX = SumHexes(HEX, HexSum);
                Files += firstTable[k].tagList.Count;
            }
            sw.WriteLine("<tr>");
            sw.WriteLine("<td class=\"itog\" COLSPAN=3 ALIGN=CENTER>ВСЕГО: файлов - {0}</td>", Files);
            sw.WriteLine("<td class=\"itog\" ALIGN=RIGHT>{0}</td>", FullByteLength);
            sw.WriteLine("<td class=\"itog\" ALIGN=RIGHT>{0}</td>", FullRowCount);
            sw.WriteLine("<td class=\"itog_ks\" ALIGN=CENTER>{0}</td>", FromEngToRus(HEX));
            sw.WriteLine("</tr>");
        }

        /// <summary>
        /// Добавление в отчет данных из таблицы с одинаковыми именами файлов
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="TabList"></param>
        private void AddLastTableTags(StreamWriter sw, List<HtmlThirdTable> TabList)
        {
            if (TabList != null)
            {
                int grnum = TabList[0].groupnum;
                int RowSpan = 0;
                foreach (HtmlThirdTable ttl in TabList)
                {
                    sw.WriteLine("<tr>");
                    if (grnum != ttl.groupnum)
                    {
                        grnum = ttl.groupnum;
                        RowSpan = TabList.Count(x => x.groupnum == grnum);
                        sw.WriteLine("<td ALIGN=CENTER ROWSPAN={0}>{1}{2}</td>", RowSpan, WriteProbel(grnum), grnum);
                    }
                    else
                    {
                        if (RowSpan == 0)
                        {
                            RowSpan = TabList.Count(x => x.groupnum == grnum);
                            sw.WriteLine("<td ALIGN=CENTER ROWSPAN={0}>{1}{2}</td>", RowSpan, WriteProbel(grnum), grnum);
                        }
                    }
                    sw.WriteLine("<td ALIGN=CENTER>{0}</td>", ttl.num);
                    sw.WriteLine("<td class=\"ext\" ALIGN=LEFT>{0}</td>", ttl.filename);
                    sw.WriteLine("<td ALIGN=RIGHT>{0}</td>", FromZeroToDash(ttl.bytelength));
                    sw.WriteLine("<td ALIGN=RIGHT>{0}</td>", FromZeroToDash(ttl.rowlength));
                    sw.WriteLine("<td class=\"ext\" ALIGN=CENTER>{0}</td>", ttl.hex);
                    sw.WriteLine("<td ALIGN=LEFT>{0}</td>", ttl.path);
                    sw.WriteLine("</tr>");
                }
            }
        }

        private string WriteProbel(int RowSpan)
        {            
            int length = RowSpan.ToString().Length;
            if (length == 1)
                return "    ";
            else if (length == 2)
                return "   ";
            else if (length == 3)
                return "  ";
            else if (length == 4)
                return " ";
            else return "";
        }

        /// <summary>
        /// Добавление в отчет данных из таблицы расширений
        /// </summary>
        /// <param name="sw">Стрим файла-отчета данных</param>
        private void AddSecondTableTags(StreamWriter sw)
        {
            int Length = 0, Bytes = 0, Rows = 0;
            foreach (HtmlSecondTable stl in secondTable)
            {
                sw.WriteLine("<tr>");
                sw.WriteLine("<td ALIGN=CENTER>{0}</td>", stl.num);
                sw.WriteLine("<td class=\"ext\" ALIGN=CENTER><B>{0}</B></td>", stl.ext);
                sw.WriteLine("<td ALIGN=CENTER>{0}</td>", stl.filescount);
                sw.WriteLine("<td ALIGN=RIGHT>{0}</td>", stl.bytelength);
                sw.WriteLine("<td ALIGN=CENTER>{0}</td>", stl.rowlength);
                sw.WriteLine("</tr>");
                Length += stl.filescount;
                Bytes += stl.bytelength;
                Rows += stl.rowlength;
            }
            sw.WriteLine("<tr>");
            sw.WriteLine("<tr>");
            sw.WriteLine("<td class=\"itog\" ALIGN=CENTER  COLSPAN=2>Итого:</td>");
            sw.WriteLine("<td class=\"itog\" ALIGN=CENTER>{0}</td>", Length);
            sw.WriteLine("<td class=\"itog\" ALIGN=CENTER>{0}</td>", Bytes);
            sw.WriteLine("<td class=\"itog\" ALIGN=CENTER>{0}</td>", Rows);
            sw.WriteLine("</tr>");
        }

        private string FromZeroToDash(int p)
        {
            if (p == 0)
                return "-";
            else return p.ToString();
        }

        private string FromEngToRus(string p)
        {
            string tmp = "";
            for (int i = 0; i < p.Length; i++)
                if (p[i] == 'e')
                    tmp += 'е';
                else tmp += p[i];
            return tmp;
        }

        private string HexFromRusToEng(string p)
        {
            string tmp = "";
            for (int i = 0; i < p.Length; i++)
                if (p[i] == 1077)
                    tmp += 'e';
                else tmp += p[i];
            return tmp;
        }

        private string SumHexes(string HexSum, string hex)
        {
            if (HexSum == "")
                return hex;
            if (hex == "")
                return HexSum;
            int[] a = { Convert.ToInt32(HexSum.Substring(0,2),16),
                          Convert.ToInt32(HexSum.Substring(2,2),16),
                          Convert.ToInt32(HexSum.Substring(4,2),16),
                          Convert.ToInt32(HexSum.Substring(6,2),16)};
            int[] b = { Convert.ToInt32(hex.Substring(0,2),16),
                          Convert.ToInt32(hex.Substring(2,2),16),
                          Convert.ToInt32(hex.Substring(4,2),16),
                          Convert.ToInt32(hex.Substring(6,2),16)};
            int[] c = new int[4];
            string[] wHex = new string[4];
            int[] razr = new int[4];
            for (int i = 0; i < 4; i++)
            {
                c[i] = a[i] + b[i];
                if (Convert.ToString(c[i], 16).Length > 2)
                {
                    razr[i] = 1;
                    wHex[i] = Convert.ToString(c[i], 16).Substring(1);
                }
                else
                {
                    razr[i] = 0;
                    wHex[i] = c[i].ToString("x2");
                }
            }
            return SumHexes(wHex, razr);
        }

        private string SumHexes(string[] wHex, int[] razr)
        {
            int[] razr_tmp = { 0, 0, 0, 0 };
            if (razr[0] == 1)
            {
                wHex[1] = Convert.ToString(Convert.ToInt32(wHex[1], 16) + 1, 16);
                if (wHex[1].Length > 2)
                    wHex[1] = wHex[1].Substring(1);
                else wHex[1] = (Convert.ToInt32(wHex[1], 16)).ToString("x2");
                if (wHex[1] == "00")
                    razr_tmp[1] = 1;
            }
            if (razr[1] == 1)
            {
                wHex[2] = Convert.ToString(Convert.ToInt32(wHex[2], 16) + 1, 16);
                if (wHex[2].Length > 2)
                    wHex[2] = wHex[2].Substring(1);
                else wHex[2] = (Convert.ToInt32(wHex[2], 16)).ToString("x2");
                if (wHex[2] == "00")
                    razr_tmp[2] = 1;
            }
            if (razr[2] == 1)
            {
                wHex[3] = Convert.ToString(Convert.ToInt32(wHex[3], 16) + 1, 16);
                if (wHex[3].Length > 2)
                    wHex[3] = wHex[3].Substring(1);
                else wHex[3] = (Convert.ToInt32(wHex[3], 16)).ToString("x2");
            }
            if (razr_tmp[1] == 1 || razr_tmp[2] == 1)
                return SumHexes(wHex, razr_tmp);
            return wHex[0] + wHex[1] + wHex[2] + wHex[3];
        }
        
        public void Closing()
        {
            firstTable.Clear();
            secondTable.Clear();
            thirdTable.Clear();
            fourthTable.Clear();
        }
    }
}
