﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDotNetZip1
{
    class ZipProgressEventType
    {
        public static object Saving_AfterCompileSelfExtractor { get; set; }

        public static object Saving_AfterSaveTempArchive { get; set; }
    }
}
