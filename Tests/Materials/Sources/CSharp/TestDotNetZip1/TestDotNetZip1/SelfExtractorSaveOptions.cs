﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestDotNetZip1
{
    public enum SelfExtractorFlavor
    {
        ConsoleApplication = 0,
        WinFormsApplication
    }
    public class SelfExtractorSaveOptions
    {
        public SelfExtractorFlavor Flavor { get; set; }
        public String PostExtractCommandLine { get; set; }
        public String DefaultExtractDirectory { get; set; }
        public string IconFile { get; set; }
        public bool Quiet { get; set; }
        public bool RemoveUnpackedFilesAfterExecute { get; set; }
        public Version FileVersion { get; set; }
        public String ProductVersion { get; set; }
        public String Copyright { get; set; }
        public String Description { get; set; }
        public String ProductName { get; set; }
        public String SfxExeWindowTitle { get; set; }
        public string AdditionalCompilerSwitches { get; set; }

        public int ExtractExistingFile { get; set; }
    }
}
