var netscape = (document.layers) ? 1:0;
var goodIE = (document.all) ? 1:0;
var mozilla = (goodIE==0 && document.getElementById) ? 1: 0;

if (netscape) {
	window.captureEvents(Event.MOUSEDOWN);
	window.onMouseDown = mouseclick;
}else if (goodIE||mozilla) {
	document.onmousedown=mouseclick;
}

var imagedir="images/dinocube/";
function preload(){
	this.length=preload.arguments.length;
	for (var i=0;i<this.length;i++){
		this[i]=new Image();
		this[i].src=imagedir+preload.arguments[i];
	}
}
var pics=new preload(
			"orangeu.gif","redu.gif","blueu.gif","greenu.gif","whiteu.gif","yellowu.gif","greyu.gif",
			"oranger.gif","redr.gif","bluer.gif","greenr.gif","whiter.gif","yellowr.gif","greyr.gif",
			"oranged.gif","redd.gif","blued.gif","greend.gif","whited.gif","yellowd.gif","greyd.gif",
			"orangel.gif","redl.gif","bluel.gif","greenl.gif","whitel.gif","yellowl.gif","greyl.gif",
			"../buttons/edit.gif", "../buttons/edit2.gif",
			"../buttons/solve.gif","../buttons/solve2.gif",
			"../buttons/play.gif","../buttons/pause.gif",
			"../buttons/mode1.gif","../buttons/mode2.gif"
			);

var posit = new Array ();
var mode = 0;   //0=normal  1=solving scrambled  2=edit  3=solve  4=solving
var type = 0;
var edt;
var pcperm = new Array();
var sol=new Array();
var soltimer;

function changeimage(lay,im,nw){
	if(netscape){
		document.layers[lay].document.images[im].src=nw;
	}else{
		eval("document.images."+im+".src=nw");
	}
}
function showhide(lay,vis){
	if(netscape){
		document.layers[lay].visibility= (vis?"visible":"hidden");
	}else if(goodIE){
		eval("document.all."+lay+".style.visibility='"+(vis?"visible":"hidden")+"'");
	}else{
		document.getElementById(lay).style.visibility= (vis?"visible":"hidden");
	}
}

var edges = new Array(
	 0, 1,  2,21, 19,18,
	12,13, 14, 9,  7, 6,
	 5, 4,  3, 8, 10,11,
	17,16, 15,20, 22,23
);
var colours = new Array();
colours[0]= new Array(
	0,2,2,2,2,1,
	0,4,4,4,4,1,
	0,3,3,3,3,1,
	0,5,5,5,5,1
);
colours[1]= new Array(
	4,4,4,2,2,2,
	1,1,2,1,2,2,
	1,1,1,3,3,3,
	4,4,3,4,3,3
);
var shapgif=new Array(2,0,3,1,2,0);
// 0 5 3  2
//1 7 4 10 1
// 6 8 9 11

function display(){
	var a=0;
	for(var i=0;i<4;i++){
		for(var j=0;j<6;j++){
			changeimage("l"+a,"i"+a, pics[shapgif[j]*7+posit[a]].src);
			a++;
		}
	}

	if(mode==2){
		showhide("prev1",1);
		changeimage("prev1","p1", pics[14+colours[type][edges[edt*2  ]]].src);
		showhide("prev2",1);
		changeimage("prev2","p2", pics[   colours[type][edges[edt*2+1]]].src);
	}else{
		showhide("prev1",0);
		showhide("prev2",0);
	}

	if(mode==2) changeimage("buttons","edit",pics[29].src);
	else changeimage("buttons","edit",pics[28].src);
	if(mode>=3) changeimage("buttons","solve",pics[31].src);
	else changeimage("buttons","solve",pics[30].src);
	if(mode==4) changeimage("buttons","play",pics[33].src);
	else changeimage("buttons","play",pics[32].src);

	changeimage("buttons","type",pics[34+type].src);

	if(mode==1 && solved()){
		alert("You solved it!\nYou don't get a prize for this though!");
		mode=0;
	}
}

function changegame(){
	type=1-type;
	reset();
	display();
}

function initbrd(){
	if( mode==4 ) clearTimeout(soltimer);
	for(i=0; i<24; i++) posit[i]=colours[type][i];
	mode=0;
	sol.length=0;
}
initbrd();

function solved(){
	var i;
	if(type==0){
		for (i=0;i<24; i+=6){
			if( posit[i+1]!=posit[i+2] ) return false;
			if( posit[i+1]!=posit[i+3] ) return false;
			if( posit[i+1]!=posit[i+4] ) return false;
			if( i<18 && ( posit[i]!=posit[i+6] || posit[i+5]!=posit[i+11] ) ) return false;
		}
		return true;
	}else{
		// 0 5 3  2
		//1 7 4 10 1
		// 6 8 9 11
		if( posit[1]==posit[2] ) {
			if( posit[1]==posit[19] &&
				posit[7]==posit[14] && posit[13]==posit[14] &&
				posit[4]==posit[3] && posit[3]==posit[10] ) return true;
		}else if( posit[1]==posit[3] ) {
			if( posit[7]==posit[1] &&
				posit[13]==posit[20] && posit[19]==posit[20] &&
				posit[10]==posit[9] && posit[9]==posit[16] ) return true;
		}
		return false;
	}
}

function edit(){
	initbrd();
	for(var i=0;i<24;i++) posit[i]=6;
	mode=2;
	edt=0;
	display();
}

function getmove(e){
	var clickX, clickY;
	if(netscape||mozilla){
		clickX = e.pageX;
		clickY = e.pageY;
	}else{
		clickX =window.event.clientX-2;
		clickY =window.event.clientY-2;
	}
	if(clickY>107) return -1;
	clickX-=5; //skip left border
	clickX=Math.floor(clickX/23.5); // get section number
	clickX++; clickX&=7; // get move number of top corners
	if(clickY>53)clickX+=8;	//adjust if bottom corner
	return clickX;
}
function getfacelet(e){
	var clickX, clickY;
	if(netscape||mozilla){
		clickX = e.pageX;
		clickY = e.pageY;
	}else{
		clickX =window.event.clientX-2;
		clickY =window.event.clientY-2;
	}
	clickX-=6; //skip left border
	clickY-=5; //skip top border
	if(clickY<0 || clickX<0) return -1;
	var f=Math.floor(clickX/47);	// get face number
	if(f>5) return -1;
	clickX-=47*f; f*=6;
	if(clickY<26){
		return (clickX+clickY>23 && clickX-clickY<23) ? f : -1;
	}
	clickY-=26;
	if(clickY>=47){
		clickY-=47;
		return (clickX+clickY<46 && clickX>clickY+2 ) ? f+5 : -1;
	}
	if(clickX<=clickY){
		return (clickX+clickY<46)? f+2:f+4;
	}
	return (clickX+clickY<46)? f+1:f+3;
}

function mouseclick(e){
	var f;
	if( mode==2 ){
		f=getfacelet(e);
		if(f>=0)clicked(f);
	}else if(mode<2){
		f=getmove(e);
		if(f>=0){
			domove(f);
			display();
		}
	}
}


function reset(){
	initbrd();
	display();
}

function mix(){
	var tmp = new Array(0,1,2,3,4,5,6,7,8,9,10,11);
	var i,j,c;
	for(i=0; i<10; i++){
		j=i+Math.floor(Math.random()*(12-i));
		if(j>i){
			c=tmp[j];tmp[j]=tmp[i]; tmp[i]=c;
			c=tmp[11];tmp[11]=tmp[10]; tmp[10]=c;
		}
	}
	for(i=0; i<12;i++){
		// at position i place piece tmp[i]
		posit[edges[i+i  ]]=colours[type][edges[tmp[i]*2  ]];
		posit[edges[i+i+1]]=colours[type][edges[tmp[i]*2+1]];
	}
	mode=1;
	display();
}

function placePiece(ps,pc){
	posit[edges[ps*2  ]] = colours[type][edges[pc*2  ]];
	posit[edges[ps*2+1]] = colours[type][edges[pc*2+1]];
}
function getPieceAt(ps){
	var c1 = posit[edges[ps*2  ]];
	var c2 = posit[edges[ps*2+1]];
	if( c1==6 && c2==6 ) return(-1);
	for( var pc=0; pc<12; pc++)
		if( colours[type][edges[pc*2  ]]==c1 && colours[type][edges[pc*2+1]]==c2 ) break;
	return pc;
}

function clicked(f){
	var i,j,k;
	if(posit[f]==6){
		// find it in edge facelet array
		for( i=0; i<12 && edges[i+i]!=f && edges[i+i+1]!=f; i++);
		//place edge piece
		placePiece(i,edt);
		edt++;

		// fill in last pieces which are forced
		if(type!=0 && edt==9){
			for(i=0; i<12; i++){
				if(getPieceAt(i)<0) placePiece(i,edt++);
			}
			mode=1;
		}else if(type==0 && edt==10){
			// first get piece array
			for(i=0; i<12; i++){
				pcperm[i]=getPieceAt(i);
				if(pcperm[i]<0) pcperm[i]=edt++;
			}
			// get permutation parity
			k=0;
			for(i=0; i<12; i++){
				for(j=i+1; j<12; j++){
					if( pcperm[i]>pcperm[j] ) k++;
				}
			}
			k&=1;

			// place last two pieces correctly
			for(i=0; i<12; i++)
				if(pcperm[i]>=10)
					placePiece(i,pcperm[i]^k);
			mode=1;
		}
		display();
	}
}


var movelist=new Array();
movelist[0]=new Array( 0, 2,19,  1,21,18);
movelist[1]=new Array( 6, 8, 1,  7, 3, 0);
movelist[2]=new Array(12,14, 7, 13, 9, 6);
movelist[3]=new Array(18,20,13, 19,15,12);

movelist[4]=new Array(23,21, 4, 22, 2, 5);
movelist[5]=new Array( 5, 3,10,  4, 8,11);
movelist[6]=new Array(11, 9,16, 10,14,17);
movelist[7]=new Array(17,15,22, 16,20,23);

function domove(m){
	var c, i, k=Math.floor(m/2);
	var l=1+(m&1);
	while(l>0){
		for(i=0;i<movelist[k].length; i+=3){
			c=posit[movelist[k][i]];
			posit[movelist[k][i  ]]=posit[movelist[k][i+2]];
			posit[movelist[k][i+2]]=posit[movelist[k][i+1]];
			posit[movelist[k][i+1]]=c;
		}
		l--;
	}
}


// Solve/play button routine
function solve(m){
	var a;
	if( mode<2 ){
		mode=m?4:3;
		dosolve();
	}else if(mode>2){
		if( m ) {	// pause/play
			if(mode==4){
				mode=3;
				clearTimeout(soltimer);
				display();
				return(void(0));
			}
			mode=4;
		}
		// do single move of solution
		if( sol.length ){
			domove( sol[0] );
			for( a=1; a<sol.length; a++) sol[a-1]=sol[a];
			sol.length--;
		}
		if( sol.length==0 ) mode=0;
		display();
		// set repeat move
		if( mode==4 ){
			soltimer=setTimeout("solve(0);", 200);
		}
	}
}

function find(prm,pc){
	var i;
	for(i=0; i<12 && prm[i]!=pc; i++);
	return i;
}
function find2(prm,pc){
	var i;
	for(i=2; i<find2.arguments.length; i++){
		if( prm[find2.arguments[i]]==pc ) return find2.arguments[i];
	}
	return find(prm,pc);
}

var moveprm=new Array();
moveprm[0]=new Array(0,2,1);
moveprm[1]=new Array(0,1,2);
moveprm[2]=new Array(5,0,7);
moveprm[3]=new Array(5,7,0);
moveprm[4]=new Array(3,5,4);
moveprm[5]=new Array(3,4,5);
moveprm[6]=new Array(2,3,10);
moveprm[7]=new Array(2,10,3);
moveprm[8]=new Array(11,6,1);
moveprm[9]=new Array(11,1,6);
moveprm[10]=new Array(6,8,7);
moveprm[11]=new Array(6,7,8);
moveprm[12]=new Array(8,9,4);
moveprm[13]=new Array(8,4,9);
moveprm[14]=new Array(9,11,10);
moveprm[15]=new Array(9,10,11);

function push(){
	//adds move sequence to movelist, and performs moves as well
	var i,l,m;
	for (i=0;i<push.arguments.length;i++){
		m=push.arguments[i];
		// do move
		l=pcperm[moveprm[m][0]];
		pcperm[moveprm[m][0]]=pcperm[moveprm[m][1]];
		pcperm[moveprm[m][1]]=pcperm[moveprm[m][2]];
		pcperm[moveprm[m][2]]=l;
		//put move on stack
		l=sol.length;
		sol[l]=m;
		if(l>0){
			if( sol[l-1]==(m^1) ) sol.length-=2;
			if( sol[l-1]==m ) { sol[l-1]^=1; sol.length--; }
		}
	}
}

function dosolve(){
	// create solution
	var i,j,c;
	sol.length=0;

	//get piece permutation
	for(i=0; i<12; i++) pcperm[i]=getPieceAt(i);

	if( type ){

	// solve piece 0
	i=find2(pcperm,0, 0, 1,2,5,7, 3,4,6,8,10,11, 9);
	while(i!=0){
		switch(i){
		case 1: push(1); break;
		case 2: push(0); break;
		case 5: push(3); break;
		case 7: push(2); break;

		case 3: push(5); break;
		case 4: push(4); break;
		case 6: push(10); break;
		case 8: push(11); break;
		case 10: push(7); break;
		case 11: push(8); break;

		case 9: push(12); break;
		}
		i=find2(pcperm,0, 0, 1,2,5,7, 3,4,6,8,10,11, 9);
	}

	// solve piece 1
	i=find2(pcperm,0, 1, 2,11,6, 3,7,8,9,10,5, 4);
	while(i!=1){
		switch(i){
		case 2: push(0); break;
		case 11: push(8); break;
		case 6: push(9); break;

		case 3: push(6); break;
		case 7: push(11); break;
		case 8: push(10); break;
		case 9: push(15); break;
		case 10: push(14); break;
		case 5: push(0,3); break;

		case 4: push(12); break;
		}
		i=find2(pcperm,0, 1, 2,11,6, 3,7,8,9,10,5, 4);
	}

	// solve piece 2
	i=find2(pcperm,0, 2, 3,10, 4,5,9,11,6,7, 8);
	while(i!=2){
		switch(i){
		case 3: push(6); break;
		case 10: push(7); break;

		case 4: push(5); break;
		case 5: push(4); break;
		case 9: push(14); break;
		case 11: push(15); break;
		case 6: push(1,9); break;
		case 7: push(2); break;

		case 8: push(10); break;
		}
		i=find2(pcperm,0, 2, 3,10, 4,5,9,11,6,7, 8);
	}

	// solve piece 3
	i=find2(pcperm,3, 3, 4,5, 8,9, 6,7,10,11);
	while(i!=3){
		switch(i){
		case 4: push(5); break;
		case 5: push(4); break;

		case 8: push(12); break;
		case 9: push(13); break;

		case 10: push(15); break;
		case 11: push(14); break;
		case 6: push(11); break;
		case 7: push(10); break;
		}
		i=find2(pcperm,3, 3, 4,5, 8,9, 6,7,10,11);
	}

	// solve piece 5
	i=find2(pcperm,3, 5, 4, 8,9, 6,7,10,11);
	while(i!=5){
		switch(i){
		case 4: push(5); break;

		case 8: push(12); break;
		case 9: push(13); break;

		case 10: push(15); break;
		case 11: push(14); break;
		case 6: push(11); break;
		case 7: push(10); break;
		}
		i=find2(pcperm,3, 5, 4, 8,9, 6,7,10,11);
	}

	// solve piece 6
	i=find2(pcperm,6, 6, 7,8, 4,9, 10,11);
	while(i!=6){
		switch(i){
		case 7: push(11); break;
		case 8: push(10); break;

		case 4: push(13); break;
		case 9: push(12); break;

		case 10: push(15); break;
		case 11: push(14); break;
		}
		i=find2(pcperm,6, 6, 7,8, 4,9, 10,11);
	}

	// solve piece 7
	i=find2(pcperm,6, 7, 8, 4,9, 10,11);
	while(i!=7){
		switch(i){
		case 8: push(10); break;

		case 4: push(13); break;
		case 9: push(12); break;

		case 10: push(15); break;
		case 11: push(14); break;
		}
		i=find2(pcperm,6, 7, 8, 4,9, 10,11);
	}

	// solve piece 8 on position 4
	i=find2(pcperm,6, 4, 8,9, 10,11);
	while(i!=4){
		switch(i){
		case 8: push(12); break;
		case 9: push(13); break;
		case 10: push(15); break;
		case 11: push(14); break;
		}
		i=find2(pcperm,6, 4, 8,9, 10,11);
	}


	// solve piece 4 on position 4 (pushing 8 correct too)
	i=find2(pcperm,3, 8, 9, 10,11);
	if(i==8) { push(12,14,13,15,12); i=4;}
	while(i!=4){
		switch(i){
		case 9: push(13); break;
		case 10: push(15); break;
		case 11: push(14); break;
		}
		i=find2(pcperm,3, 4, 9, 10,11);
	}

// 0 5 3  2 0 5 3  2
//1 7 4 10 1 7 4 10 1
// 6 8 9 11 6 8 9 11
	//alert(pcperm);

	}else{ //6-colour game mode

	// solve piece 0
	i=find(pcperm,0);
	while(i!=0){
		switch(i){
		case 1: push(1); break;
		case 2: push(0); break;
		case 3: push(5); break;
		case 4: push(4); break;
		case 5: push(3); break;
		case 6: push(10); break;
		case 7: push(2); break;
		case 8: push(11); break;
		case 9: push(12); break;
		case 10: push(7); break;
		case 11: push(8); break;
		}
		i=find(pcperm,0);
	}

	// solve piece 1
	i=find(pcperm,1);
	while(i!=1){
		switch(i){
		case 11: push(8); break;
		case 6: push(9); break;

		case 7: push(11); break;
		case 8: push(10); break;
		case 9: push(15); break;
		case 10: push(14); break;

		case 2: push(6); break;
		case 3: push(5); break;
		case 4: push(12); break;

		case 5: push(1,3,0); break;
		}
		i=find(pcperm,1);
	}

	// solve piece 2
	i=find(pcperm,2);
	while(i!=2){
		switch(i){
		case 3: push(6); break;
		case 10: push(7); break;

		case 9: push(14); break;
		case 11: push(15); break;

		case 4: push(5); break;
		case 5: push(4); break;

		case 6: push(1,9,0); break;
		case 7: push(0,2,1); break;

		case 8: push(10); break;
		}
		i=find(pcperm,2);
	}

	// solve piece 5 on position 3
	i=find(pcperm,5);
	while(i!=3){
		switch(i){
		case 4: push(5); break;
		case 5: push(4); break;

		case 8: push(12); break;
		case 9: push(13); break;

		case 10: push(15); break;
		case 11: push(14); break;
		case 6: push(11); break;
		case 7: push(10); break;
		}
		i=find(pcperm,5);
	}

	// solve piece 3 on position 3 (pushing 5 correct too)
	i=find(pcperm,3);
	if(i==5) { push(3,4,2,4); i=3;}
	while(i!=3){
		switch(i){
		case 4: push(5); break;
		case 8: push(12); break;
		case 9: push(13); break;
		case 6: push(11); break;
		case 7: push(10); break;
		case 10: push(15); break;
		case 11: push(14); break;
		}
		i=find(pcperm,3);
	}

	// solve piece 6 on position 7
	i=find(pcperm,6);
	while(i!=7){
		switch(i){
		case 6: push(10); break;
		case 8: push(11); break;
		case 4: push(13); break;
		case 9: push(12); break;
		case 10: push(15); break;
		case 11: push(14); break;
		}
		i=find(pcperm,6);
	}

	// solve piece 7 on position 7 (pushing 6 correct too)
	i=find(pcperm,7);
	if(i==6) { push(10,12,11,13,10); i=7;}
	while(i!=7){
		switch(i){
		case 8: push(11); break;
		case 4: push(13); break;
		case 9: push(12); break;
		case 10: push(15); break;
		case 11: push(14); break;
		}
		i=find(pcperm,7);
	}

	// solve piece 8 on position 4
	i=find(pcperm,8);
	while(i!=4){
		switch(i){
		case 8: push(12); break;
		case 9: push(13); break;
		case 10: push(15); break;
		case 11: push(14); break;
		}
		i=find(pcperm,8);
	}

	// solve piece 4 on position 4 (pushing 8 correct too)
	i=find(pcperm,4);
	if(i==8) { push(12,14,13,15,12); i=4;}
	while(i!=4){
		switch(i){
		case 9: push(13); break;
		case 10: push(15); break;
		case 11: push(14); break;
		}
		i=find(pcperm,4);
	}

	// solve piece 9-11
	i=find(pcperm,9);
	switch(i){
	case 10: push(15); break;
	case 11: push(14); break;
	}

	}// end of type==0 solution

	solve(0);
}

function help(){
	alert(
		"Dinocube\n\n"+
		"This is the Dinocube puzzle. Click to the\n"+
		"left or right of a corner to turn it in\n"+
		"either direction. The aim is of course to\n"+
		"rearrange it so that each face has only one\n"+
		"colour.\n\n"+
		"Buttons:\n"+
		"Mix:   Mixes up the cube.\n"+
		"Reset: Resets the cube to solved position.\n"+
		"Solve: Calculates the shortest solution. Each\n"+
		"      time you click this button, one more move\n"+
		"      of the solution is performed.\n"+
		"Play:  Calculates the shortest solution, and\n"+
		"      plays it back automatically.\n"+
		"Edit:  Edit the puzzle. The pieces are cleared\n"+
		"      and then you can click where each of them\n"+
		"      (shown in miniature) is supposed to be.\n"+
		"Mode:  Switch between the 6 colour and the 4\n"+
		"      colour version of the dino cube.\n"+
		"Help:  Shows this help screen.");
}

function dostyle(){
	var s='<STYLE TYPE="text/css">';
	for(var i=0;i<4;i++){
		s+="#l"+(i*6  )+"{position:absolute; left:"+(i*47+ 5)+"; top:5; width:1;}";
		s+="#l"+(i*6+1)+"{position:absolute; left:"+(i*47+ 5)+"; top:29; width:1;}";
		s+="#l"+(i*6+2)+"{position:absolute; left:"+(i*47+ 5)+"; top:29; width:1;}";
		s+="#l"+(i*6+3)+"{position:absolute; left:"+(i*47+28)+"; top:29; width:1;}";
		s+="#l"+(i*6+4)+"{position:absolute; left:"+(i*47+ 5)+"; top:52; width:1;}";
		s+="#l"+(i*6+5)+"{position:absolute; left:"+(i*47+ 5)+"; top:76; width:1;}";
	}
	s+='#prev1{position:absolute; left:91; top:5; width:1; visibility:hidden;}';
	s+='#prev2{position:absolute; left:91; top:14; width:1; visibility:hidden;}';
	s+='#buttons{position:absolute; left:004; top:110; width:192;}';
	s+='<\/STYLE>';
	return s;
}
document.write(dostyle());