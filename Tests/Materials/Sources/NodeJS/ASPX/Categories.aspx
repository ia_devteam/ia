﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/TechnicalMeansAccounting/Views/Shared/TechnicalMeansAccounting.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Учет
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" language="javascript">
        Ext.onReady(function () {

            function createAddWindow() {

                var frame;

                var panel = new WebMap.form.FormPanel({
                    url: '<%: Url.Action("Add", "Categories") %>',
                    labelWidth: 8,
                    frame: true,
                    border: false,
                    layout: {
                        type: 'table',
                        columns: 2
                    },
                    defaults: { msgTarget: 'side', blankText: 'Поле не должно быть пустым' },
                    items: [
                        {
                            xtype: 'clabelwindow',
                            text: 'Название:'
                        }, {
                            xtype: 'ctextfield',
                            id: 'name-field-id',
                            width: 300,
                            name: 'name',
                            allowBlank: false
                        }
                    ],
                    buttonAlign: 'center',
                    buttons: [
			            {
			                text: 'Сохранить',
			                handler: function () {
			                    panel.getForm().submit({
			                        success: function (a, b) {
			                            store.load();
			                            frame.close();
			                        }
			                    });
			                }
			            }, {
			                text: 'Отмена',
			                handler: function () {
			                    frame.close();
			                }
			            }
                    ]
                });

                frame = new WebMap.Window({
                    title: 'Новая категория',
                    width: 425,
                    height: 125,
                    layout: 'fit',
                    resizable: true,
                    items: [panel]
                });

                return frame;
            }

            function createAddWindow2() {

                var frame;

                var panel = new WebMap.form.FormPanel({
                    url: '<%: Url.Action("Add", "Subcategories") %>',
                    labelWidth: 8,
                    frame: true,
                    border: false,
                    layout: {
                        type: 'table',
                        columns: 2
                    },
                    defaults: { msgTarget: 'side', blankText: 'Поле не должно быть пустым' },
                    items: [
                        {
                            xtype: 'clabelwindow',
                            text: 'Название:'
                        }, {
                            xtype: 'ctextfield',
                            id: 'name-field-id',
                            width: 300,
                            name: 'name',
                            allowBlank: false
                        }
                    ],
                    buttonAlign: 'center',
                    buttons: [
			            {
			                text: 'Сохранить',
			                handler: function () {
			                    panel.getForm().submit({
			                        success: function (a, b) {
			                            store2.load();
			                            frame.close();
			                        }
			                    });
			                }
			            }, {
			                text: 'Отмена',
			                handler: function () {
			                    frame.close();
			                }
			            }
                    ]
                });

                frame = new WebMap.Window({
                    title: 'Новая подкатегория',
                    width: 425,
                    height: 125,
                    layout: 'fit',
                    resizable: true,
                    items: [panel]
                });

                return frame;
            }

            function createUpdateWindow() {

                var frame;

                var panel = new WebMap.form.FormPanel({
                    url: '<%: Url.Action("Update", "Categories") %>',
                    labelWidth: 8,
                    frame: true,
                    border: false,
                    layout: {
                        type: 'table',
                        columns: 2
                    },
                    defaults: { msgTarget: 'side', blankText: 'Поле не должно быть пустым' },
                    items: [
                        {
                            xtype: 'clabelwindow',
                            text: 'Название:'
                        }, {
                            xtype: 'ctextfield',
                            id: 'name-field-id',
                            width: 300,
                            name: 'name',
                            allowBlank: false
                        }
                    ],
                    buttonAlign: 'center',
                    buttons: [
			            {
			                text: 'Сохранить',
			                handler: function () {
			                    panel.getForm().submit({
			                        success: function (a, b) {
			                            store.load();
			                            frame.close();
			                        }
			                    });
			                }
			            }, {
			                text: 'Отмена',
			                handler: function () {
			                    frame.close();
			                }
			            }
                    ]
                });

                Ext.Ajax.request({
                    url: '<%: Url.Action("GetById","Categories") %>',
                    method: 'GET',
                    scope: this,
                    success: function (response, options) {
                        var obj = Ext.decode(response.responseText);
                        Ext.getCmp('name-field-id').setValue(obj.data.Name);
                    }
                });

                frame = new WebMap.Window({
                    title: 'Изменить категорию',
                    width: 425,
                    height: 125,
                    layout: 'fit',
                    resizable: true,
                    items: [panel]
                });

                return frame;
            }

            function createUpdateWindow2() {

                var frame;

                var panel = new WebMap.form.FormPanel({
                    url: '<%: Url.Action("Update", "Subcategories") %>',
                    labelWidth: 8,
                    frame: true,
                    border: false,
                    layout: {
                        type: 'table',
                        columns: 2
                    },
                    defaults: { msgTarget: 'side', blankText: 'Поле не должно быть пустым' },
                    items: [
                        {
                            xtype: 'clabelwindow',
                            text: 'Название:'
                        }, {
                            xtype: 'ctextfield',
                            id: 'name-field-id',
                            width: 300,
                            name: 'name',
                            allowBlank: false
                        }
                    ],
                    buttonAlign: 'center',
                    buttons: [
			            {
			                text: 'Сохранить',
			                handler: function () {
			                    panel.getForm().submit({
			                        success: function (a, b) {
			                            store2.load();
			                            frame.close();
			                        }
			                    });
			                }
			            }, {
			                text: 'Отмена',
			                handler: function () {
			                    frame.close();
			                }
			            }
                    ]
                });

                Ext.Ajax.request({
                    url: '<%: Url.Action("GetById","Subcategories") %>',
                    method: 'GET',
                    scope: this,
                    success: function (response, options) {
                        var obj = Ext.decode(response.responseText);
                        Ext.getCmp('name-field-id').setValue(obj.data.Name);
                    }
                });

                frame = new WebMap.Window({
                    title: 'Изменить подкатегорию',
                    width: 425,
                    height: 125,
                    layout: 'fit',
                    resizable: true,
                    items: [panel]
                });

                return frame;
            }

            var store = new Ext.data.JsonStore({
                root: 'data',
                fields: [
                        { type: 'string', name: 'Id' },
                        { type: 'string', name: 'Name' }
                ],
                proxy: new Ext.data.HttpProxy({
                    url: '<%: Url.Action("GetAll","Categories") %>',
                    method: 'GET'
                })
            });
            store.load();

            var columns = [
                {
                    header: 'Категории',
                    sortable: true,
                    dataIndex: 'Name'
                }
            ];

            var store2 = new Ext.data.JsonStore({
                root: 'data',
                fields: [
                        { type: 'string', name: 'Id' },
                        { type: 'string', name: 'Name' }
                ],
                proxy: new Ext.data.HttpProxy({
                    url: '<%: Url.Action("GetByCategoryId","Subcategories") %>',
                    method: 'GET'
                })
            });

            var columns2 = [
                {
                    header: 'Подкатегории',
                    sortable: true,
                    dataIndex: 'Name'
                }
            ];

            var grid = new WebMap.grid.GridPanel({
                singleSelect: true,
                viewConfig: {
                    markDirty: false,
                    forceFit: true
                },
                sm: new Ext.grid.RowSelectionModel({
                    singleSelect: true,
                    listeners: {
                        rowselect: function (selModel, rowIndex, r) {
                            var record = grid.store.getAt(rowIndex);

                            Ext.Ajax.request({
                                url: '<%: Url.Action("SetSelected", "Categories") %>',
                                method: 'POST',
                                scope: this,
                                params: {
                                    "id": record.get('Id'),
                                    "name": record.get('Name'),
                                    "rowIndex": rowIndex
                                },
                                success: function (response, options) {
                                    store2.removeAll();
                                    store2.load();
                                }
                            });

                        }
                    }
                }),
                store: store,
                columns: columns,
                listeners: {
                    render: function () {
                        grid.store.on('load', function () {
                            Ext.Ajax.request({
                                url: '<%: Url.Action("GetSelectedRowIndex", "Categories") %>',
                                method: 'GET',
                                scope: this,
                                success: function (response, options) {
                                    var selectedRowIndex = response.responseText;
                                    grid.getSelectionModel().selectRow(selectedRowIndex);
                                }
                            });
                        });
                    }
                }
            });

            var grid2 = new WebMap.grid.GridPanel({
                singleSelect: true,
                viewConfig: {
                    markDirty: false,
                    forceFit: true
                },
                sm: new Ext.grid.RowSelectionModel({
                    singleSelect: true,
                    listeners: {
                        rowselect: function (selModel, rowIndex, r) {
                            var record = grid2.store.getAt(rowIndex);

                            Ext.Ajax.request({
                                url: '<%: Url.Action("SetSelected", "Subcategories") %>',
                                method: 'POST',
                                scope: this,
                                params: {
                                    "id": record.get('Id'),
                                    "name": record.get('Name')
                                }
                            });

                        }
                    }
                }),
                listeners: {
                    dblclick: function () {
                        var selectedItem = grid2.getSelectionModel().getSelected();
                        if (selectedItem == undefined) {
                            Ext.Msg.alert('', 'Не выбрана ни одна подкатегория.');
                        } else {
                            window.location = '<%: Url.Action("Types", "Main") %>';
                        }
                    }
                },
                store: store2,
                columns: columns2
            });

            var myPanel = new WebMap.Panel({
                title: 'Перечень категорий и подкатегорий',
                height: 700,
                layout: {
                    type: 'hbox',
                    pack: 'start',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'cpanel',
                        margins: '0 10 0 0',
                        flex: 1,
                        layout: 'fit',
                        items: grid,
                        buttonAlign: 'left',
                        buttons: [
                            {
                                text: 'Добавить',
                                width: 100,
                                margins: '0 5 0 0',
                                handler: function () {
                                    var form = createAddWindow();
                                    form.show();
                                }
                            }, {
                                text: 'Изменить',
                                width: 100,
                                margins: '0 5 0 0',
                                handler: function () {
                                    var form = createUpdateWindow();
                                    form.show();
                                }
                            }, {
                                text: 'Удалить',
                                width: 100,
                                margins: '0 5 0 0',
                                handler: function () {
                                    var message = Ext.Msg;
                                    message.buttonText.yes = "Да";
                                    message.buttonText.no = "Нет";
                                    message.confirm('', 'Удалить выбранную запись?', function (btn) {
                                        if (btn == 'yes') {
                                            Ext.Ajax.request({
                                                url: '<%: Url.Action("Delete", "Categories") %>',
                                                method: 'POST',
                                                scope: this,
                                                success: function (response, options) {
                                                    store.load();
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        ]
                    }, {
                        xtype: 'cpanel',
                        flex: 1,
                        layout: 'fit',
                        items: grid2,
                        buttonAlign: 'left',
                        buttons: [
                            {
                                text: 'Добавить',
                                width: 100,
                                margins: '0 5 0 0',
                                handler: function () {
                                    var form = createAddWindow2();
                                    form.show();
                                }
                            }, {
                                text: 'Изменить',
                                width: 100,
                                margins: '0 5 0 0',
                                handler: function () {
                                    var form = createUpdateWindow2();
                                    form.show();
                                }
                            }, {
                                text: 'Удалить',
                                width: 100,
                                margins: '0 5 0 0',
                                handler: function () {
                                    var message = Ext.Msg;
                                    message.buttonText.yes = "Да";
                                    message.buttonText.no = "Нет";
                                    message.confirm('', 'Удалить выбранную запись?', function (btn) {
                                        if (btn == 'yes') {
                                            Ext.Ajax.request({
                                                url: '<%: Url.Action("Delete", "Subcategories") %>',
                                                method: 'POST',
                                                scope: this,
                                                success: function (response, options) {
                                                    store2.load();
                                                }
                                            });
                                        }
                                    });
                                }
                            }, {
                                text: 'Перейти',
                                width: 100,
                                margins: '0 5 0 0',
                                handler: function () {
                                    var selectedItem = grid2.getSelectionModel().getSelected();
                                    if (selectedItem == undefined) {
                                        Ext.Msg.alert('', 'Не выбрана ни одна подкатегория.');
                                    } else {
                                        window.location = '<%: Url.Action("Types", "Main") %>';
                                    }
                                }
                            }
                        ]
                    }
                ]
            });

            var globalPanel = new WebMap.Panel({
                layout: 'anchor',
                width: '100%',
                defaultType: 'cpanel',
                renderTo: Ext.getBody(),
                items: [{
                    border: true,
                    anchor: '100%',
                    frame: true,
                    contentEl: 'header'
                }, {
                    border: false,
                    items: myPanel,
                    frame: false,
                    anchor: '100%'
                }]
            });


        })
    </script>
</asp:Content>
