//1
/*
SELECT agg_area_model_id, area_model_id, atlas_info, step, region_nw_corner_x, 
       region_nw_corner_y, row_count, column_count, hascellmatrix, surname, 
       name, owner_user_id
  FROM agg_spatial_models_view;
*/
/**	��������� ��� ������ �� ������� � ��������������� �������� ���������
 *	@retval	0	������� ������� ���������
 *	@retval 1	��������� ������
 */
 function loadAggregateTerrainModelsView( atlas_info ) 
{
   if (arguments.length != 1)
   {
      toolBox.errorMessage ('������','�������� ������������ ���������� [loadAggregateTerrainModelsView(...)]');
      return 1;
   }
   ;
   sql.setQuery(" SELECT                        " + 
 				"	 agg_area_model_id,			" +  
				"	 area_model_id,			 	" + 
				"	 atlas_info,    			" +
				"	 step_b,  	 				" +				
				"	 step_l,  	 				" +				
				"    region_nw_corner_b,        " +	
				"    region_nw_corner_l,        " +	
				"    row_count,              	" +
				"    column_count,              " +	
				"	 hascellmatrix,				" +
				"    surname,              		" +
				"    name,	             		" +		
				"    region_name             		" +		
                " FROM                          " + 
                "    agg_spatial_models_view	" +
		" WHERE agg_spatial_models_view.atlas_info = :atlas_info" );
    sql.bindValueByPosition(0, ':atlas_info');
    return 0;
}

function loadAggregateTerrainModelViewById(agg_area_model_id)
{
	if(arguments.length != 1)
	{
		toolBox.errorMessage('');
		return 1;
	}
   sql.setQuery(" SELECT                        " + 
 				"	 agg_area_model_id,			" +  
				"	 area_model_id,			 	" + 
				"	 atlas_info,    			" +
				"	 step_b,  	 				" +				
				"	 step_l,  	 				" +				
				"    region_nw_corner_b,        " +	
				"    region_nw_corner_l,        " +	
				"    row_count,              	" +
				"    column_count,              " +	
				"	 hascellmatrix,				" +
				"    surname,              		" +
				"    name	              		" +		
                " FROM                          " + 
                "    agg_spatial_models_view	" +
		" WHERE " +
		" agg_area_model_id = :agg_area_model_id" );
	sql.bindValueByPosition(0, ':agg_area_model_id');
	return 0;
}



//2
/*
SELECT row_num, col_num, raster_properties, abs_height
  FROM agg_spatial_model_properties_cells
  WHERE agg_area_model_id = '20f6a0a2-26c7-11e3-930b-b388e69ec94e';
*/
/**	�������� ������� ������ ������ ������ �� �������������� �������������� ������
 *	@retval	0	������� ������� ���������
 *	@retval 1	��������� ������
 */
  function loadPropertiesCellsAggModel( agg_area_model_id ) 
{
   if (arguments.length != 1)
   {
      toolBox.errorMessage ('������','�������� ������������ ���������� [loadPropertiesCellsAggModel(...)]');
      return 1;
   }
   ;
   sql.setQuery(" SELECT                        " + 
 				"	 row_num,					" +  
				"	 col_num,			 		" + 
				"	 raster_properties,    		" +
				"	 abs_height    	 			" +							
                " FROM                          " + 
                "    agg_spatial_model_properties_cells		" +
				" WHERE										" +
				" agg_area_model_id = :agg_area_model_id	" );
	sql.bindValueByPosition(0, ':agg_area_model_id');
    return 0;
}

//3
/*
INSERT INTO agg_spatial_models(
            agg_area_model_id, area_model_id, atlas_info, step, region_nw_corner_x, 
            region_nw_corner_y, row_count, column_count, owner_user_id)
    VALUES (?, ?, ?, ?, ?, 
            ?, ?, ?, ?);
*/
/**	���������� �����/���������� �������������� ������ ��������� (����� ����������)
 *  @param[in]	 agg_area_model_id ������������� �������������� ������ ���������
 *  @param[in]	 area_model_id ������������� ������ ���������
 *  @param[in]	 atlas_info �����
 *  @param[in]	 step ��� �������������� ������
 *  @param[in]	 region_nw_corner_x ���������� ���������� � (��)
 *  @param[in]	 region_nw_corner_y ���������� ���������� � (��)
 *  @param[in]	 row_count ����� ����� ������ 
 *  @param[in]	 column_count ����� �������� ������ 
 *  @param[in]	 owner_user_id ������������� ������������, ���������� ������ 
 *  @param[in]	 isInsert ������ �� ����������/���������
 
 *	@retval	0	������� ������� ���������
 *	@retval 1	��������� ������
 */
 function insertAggAreaModelInfo (agg_area_model_id, area_model_id, atlas_info, step_b, step_l, region_nw_corner_x, region_nw_corner_y, row_count, column_count, owner_user_id, region_name, has_matrix, isInsert)
{
 if (arguments.length != 13)
	{
		toolBox.errorMessage ('������','�������� ������������ ���������� [insertAggAreaModelInfo(...)]');
		return 1;
	};
	if (isInsert == true)
	{
		sql.setQuery (" INSERT INTO     	" +
					"   agg_spatial_models  " +
					"   (               	" +
					"   agg_area_model_id,	" +
					"   area_model_id, 		" +
					"   atlas_inform,    	" +  
					"   step_b,				" +
					"   step_l,				" +
					"   region_nw_corner_b, " +
					"   region_nw_corner_l, " +
					"   row_count,			" +	
					"   column_count,		" +	
					"   owner_user_id,		" +					
					"   region_name,     " +	
					"   has_matrix     "	+			
					"     )             " +
					" VALUES            " +
					"     (             " +
					"   :agg_area_model_id,		" +
					"   :area_model_id, 		" +
					"   :atlas_info,    		" +
					"   :step_b,  				" +
					"   :step_l,  				" +
					"   :region_nw_corner_x,    " +
					"   :region_nw_corner_y,    " +
					"   :row_count,				" +	
					"   :column_count,    		" +
					"   :owner_user_id,    		" +				
					"   :region_name,    		" +
					"   :has_matrix	 "           +			
					"      )  ;         		" );	
	}
	else
	{
		sql.setQuery (" UPDATE                                  " +
                  "      agg_spatial_models                     " +
                  "    SET                                      " +
                  "      area_model_id = :area_model_id,         	" +
                  "      atlas_inform = :atlas_info, 				" +
				  "      step_b = :step_b, 							" +	
				  "      step_l = :step_l, 							" +	
				  "      region_nw_corner_b = :region_nw_corner_x, 	" +
                  "      region_nw_corner_l  = :region_nw_corner_y, " +
                  "      row_count  = :row_count, 					" +				  
                  "      column_count  = :column_count, 			" +				  
                  "      owner_user_id  = :owner_user_id, 			" +
                  "      region_name  = :region_name, 			" +
	          "      has_matrix = :has_matrix "  +
                  "   WHERE                                     	" +
                  "     agg_area_model_id = :agg_area_model_id     	" );
	}
				
	sql.bindValueByPosition (0, ':agg_area_model_id');
	sql.bindValueByPosition (1, ':area_model_id');	
	sql.bindValueByPosition (2, ':atlas_info');
	sql.bindValueByPosition (3, ':step_b');
	sql.bindValueByPosition (4, ':step_l');
	sql.bindValueByPosition (5, ':region_nw_corner_x');	
	sql.bindValueByPosition (6, ':region_nw_corner_y');
	sql.bindValueByPosition (7, ':row_count');	
	sql.bindValueByPosition (8, ':column_count');
	sql.bindValueByPosition (9, ':owner_user_id');	
	sql.bindValueByPosition (10, ':region_name');
	sql.bindValueByPosition (11, ':has_matrix');
	return 0;
}

function ereseAggModelCellBlocks ( agg_area_model_id )
{
	if( arguments.length != 1)
		{
			toolBox.errorMessage(" error in ereseAggModelCellBlocks ");
			return 1;
		}

 	sql.setQuery(" DELETE FROM                    			" +
                "   agg_spatial_model_generalization_cell_blocks     " +
                " WHERE                                     " +
                "   agg_area_model_id = :agg_area_model_id  " );
   sql.bindValueByPosition(0, ':agg_area_model_id');
   return 0;
}

//4
/*
INSERT INTO agg_spatial_model_properties_cells(
            agg_area_model_id, row_num, col_num, raster_properties, abs_height)
    VALUES (?, ?, ?, ?, ?)
*/
/**	���������� ������ �������� � ������� �� ���������� ������ �������������� ������ ���������
 *  @param[in]	 agg_area_model_id ����� ������, � ������� ������ ���������
 *  @param[in]	 row_num ����� ������, � ������� ������ ���������
 *  @param[in]	 col_num ����� �������, � ������� ������ ���������
 *  @param[in]	 raster_properties ������ ������� ��� ������
 *  @param[in]	 abs_height ����������� ������ ���������, ������������� � ������
 
 *	@retval	0	������� ������� ���������
 *	@retval 1	��������� ������
 */
 function insertAggAreaModelCellProperties (agg_area_model_id, row_num, col_num, raster_properties, abs_height)
{
	if (arguments.length != 5)
	{
		toolBox.errorMessage ('������','�������� ������������ ���������� [insertAggAreaModelCellProperties(...)]');
		return 1;
	};
	sql.setQuery (" INSERT INTO     						" +
					"   agg_spatial_model_properties_cells  " +
					"   (               	" +
					"   agg_area_model_id,	" +
					"   row_num,			" +
					"   col_num, 			" +
					"   raster_properties,  " +
					"   abs_height			" +		
					"     )             	" +
					" VALUES            	" +
					"     (             	" +
					"   :agg_area_model_id,	" +
					"   :row_num, 			" +
					"   :col_num,    		" +
					"   :raster_properties, " +
					"   :abs_height    		" +	
					"      )  ;         	" );
					
	sql.bindValueByPosition (0, ':agg_area_model_id');
	sql.bindValueByPosition (1, ':row_num');
	sql.bindValueByPosition (2, ':col_num');	
	sql.bindValueByPosition (3, ':raster_properties');
	sql.bindValueByPosition (4, ':abs_height');
	return 0;
}

//5
/*
INSERT INTO agg_spatial_model_geograph_coordinates_cells(
            agg_area_model_id, row_num, col_num, cell_bl_x, cell_bl_y)
    VALUES (?, ?, ?, ?, ?);
*/
/**	���������� ����� �������������� ���������� � ������� 
 *  @param[in]	 agg_area_model_id ����� ������, � ������� ������ ���������
 *  @param[in]	 row_num ����� ������, � ������� ������ ���������
 *  @param[in]	 col_num ����� �������, � ������� ������ ���������
 *  @param[in]	 cell_bl_x ������
 *  @param[in]	 cell_bl_y �������
 
 *	@retval	0	������� ������� ���������
 *	@retval 1	��������� ������
 */
  function insertAggAreaModelCellGeoCoordinates (agg_area_model_id, row_num, col_num, cell_bl_x, cell_bl_y)
{
	if (arguments.length != 5)
	{
		toolBox.errorMessage ('������','�������� ������������ ���������� [insertAggAreaModelCellGeoCoordinates(...)]');
		return 1;
	};
	sql.setQuery (" INSERT INTO     									" +
					"   agg_spatial_model_xy_coordinates_cells  	" +
					"   (               	" +
					"   agg_area_model_id,	" +
					"   row_num,			" +
					"   col_num, 			" +
					"   cell_xy_x,  		" +
					"   cell_xy_y			" +		
					"     )             	" +
					" VALUES            	" +
					"     (             	" +
					"   :agg_area_model_id,	" +
					"   :row_num, 			" +
					"   :col_num,    		" +
					"   :cell_bl_x,			" +
					"   :cell_bl_y    		" +	
					"      )  ;         	" );
					
	sql.bindValueByPosition (0, ':agg_area_model_id');
	sql.bindValueByPosition (1, ':row_num');
	sql.bindValueByPosition (2, ':col_num');	
	sql.bindValueByPosition (3, ':cell_bl_x');
	sql.bindValueByPosition (4, ':cell_bl_y');
	return 0;
}

//6
/*
SELECT agg_area_model_id, area_model_id, atlas_inform, step, region_nw_corner_x, 
       region_nw_corner_y, row_count, column_count, owner_user_id
  FROM agg_spatial_models
  WHERE agg_area_model_id = '20f6a0a2-26c7-11e3-930b-b388e69ec94e'
*/
/**	�������� ������ �� ������� � ���������� �������������� ������ ��������� �� � ��������������
 *	@retval	0	������� ������� ���������
 *	@retval 1	��������� ������
 */
 function loadAggregateTerrainModels( agg_area_model_id ) 
{
   if (arguments.length != 1)
   {
      toolBox.errorMessage ('������','�������� ������������ ���������� [loadAggregateTerrainModels(...)]');
      return 1;
   }
   ;
   sql.setQuery(" SELECT                        " + 
				"    agg_area_model_id,         " +
				"	 area_model_id,			 	" + 
				"	 atlas_inform,    			" +
				"	 step_b,    	 				" +				
				"	 step_l,    	 				" +				
				"    region_nw_corner_b,        " +	
				"    region_nw_corner_l,        " +	
				"    row_count,              	" +
				"    column_count,              " +		
				"    owner_user_id,    		" +					
				"    region_name                " +
                " FROM                          " + 
                "    agg_spatial_models      	" +
				"  WHERE                        " +
                "    agg_area_model_id = :agg_area_model_id  " );
				
	sql.bindValueByPosition (0, ':agg_area_model_id');		
    return 0;
}

//7
/*
DELETE FROM agg_spatial_models
 WHERE agg_area_model_id = '';
*/
/** �������� ������ �� ������� � ��������������� �������� ���������
 *  @param[in]	 agg_area_model_id ������������� �������������� ������ ���������
 *	@retval	0	������� ������� ���������
 *	@retval 1	��������� ������
 */
 function deleteAggAreaModelInfo ( agg_area_model_id )
{
   if (arguments.length != 1)
   {
      toolBox.errorMessage ('������', '�������� ������������ ���������� [deleteAggAreaModelInfo(...)]');
      return 1;
   }
   ;
   sql.setQuery(" DELETE FROM                    			" +
                "   agg_spatial_models                      " +
                " WHERE                                     " +
                "   agg_area_model_id = :agg_area_model_id  " );
   sql.bindValueByPosition(0, ':agg_area_model_id');
   return 0;
}

//8
/*
DELETE FROM agg_spatial_model_properties_cells
 WHERE <condition>;
*/
/** �������� ������� ������ � �������������� ������ ���������
 *  @param[in]	 agg_area_model_id ������������� �������������� ������ ���������
 *	@retval	0	������� ������� ���������
 *	@retval 1	��������� ������
 */
 function deleteAggAreaModelPropertiesCells ( agg_area_model_id )
{
   if (arguments.length != 1)
   {
      toolBox.errorMessage ('������', '�������� ������������ ���������� [deleteAggAreaModelPropertiesCells(...)]');
      return 1;
   }
   ;
   sql.setQuery(" DELETE FROM                    			" +
                "   agg_spatial_model_properties_cells      " +
                " WHERE                                     " +
                "   agg_area_model_id = :agg_area_model_id  " );
   sql.bindValueByPosition(0, ':agg_area_model_id');
   return 0;
}

//9
/*
DELETE FROM agg_spatial_model_geograph_coordinates_cells
 WHERE <condition>;
*/
/** �������� �������������� ��������� �������������� ������ ���������
 *  @param[in]	 agg_area_model_id ������������� �������������� ������ ���������
 *	@retval	0	������� ������� ���������
 *	@retval 1	��������� ������
 *
 */
 function deleteAggAreaModelGeoCoordinateCells ( agg_area_model_id )
{
   if (arguments.length != 1)
   {
      toolBox.errorMessage ('������', '�������� ������������ ���������� [deleteAggAreaModelGeoCoordinateCells(...)]');
      return 1;
   }
   ;
   sql.setQuery(" DELETE FROM                    					" +
                "   agg_spatial_model_xy_coordinates_cells    " +
                " WHERE                                     		" +
                "   agg_area_model_id = :agg_area_model_id  		" );
   sql.bindValueByPosition(0, ':agg_area_model_id');
   return 0;
}

//10
/*
SELECT hascellmatrix
  FROM agg_spatial_models_view
  WHERE agg_area_model_id = '';
*/
/**�������� �� ������� ������� ������� � �������������� ������ ���������
 *	@retval	0	������� ������� ���������
 *	@retval 1	��������� ������
 */
 function loadExistCellMatrix( agg_area_model_id ) 
{
   if (arguments.length != 1)
   {
      toolBox.errorMessage ('������','�������� ������������ ���������� [loadExistCellMatrix(...)]');
      return 1;
   };
   sql.setQuery(" SELECT hascellmatrix FROM agg_spatial_models_view	" +
                " WHERE agg_area_model_id = :agg_area_model_id  	" );
   sql.bindValueByPosition(0, ':agg_area_model_id');
   return 0;
}

//11
/*
SELECT row_num, col_num, cell_bl_x, cell_bl_y
  FROM agg_spatial_model_geograph_coordinates_cells
  WHERE agg_area_model_id = '';
*/
/**�������� �������������� ��������� �������������� ������ ��������� �� � ��������������
 *	@retval	0	������� ������� ���������
 *	@retval 1	��������� ������
 */
 function loadAggAreaGeoCoordinates( agg_area_model_id ) 
{
   if (arguments.length != 1)
   {
      toolBox.errorMessage ('������','�������� ������������ ���������� [loadAggAreaGeoCoordinates(...)]');
      return 1;
   };
   sql.setQuery(" SELECT row_num, col_num, cell_bl_x, cell_bl_y 	" +
				" FROM agg_spatial_model_xy_coordinates_cells	" +
                " WHERE agg_area_model_id = :agg_area_model_id  	" );
   sql.bindValueByPosition(0, ':agg_area_model_id');
   return 0;
}

function checkAggAreaModelDependes( agg_area_model_id )
{
	if(arguments.length != 1)
	{
		toolBox.errorMessage (  'error in [checkAggAreaModelDependes(...)]');
		return 1;
	}
	sql.setQuery( "SELECT agg_object_model_id FROM agg_object_models " +
			" WHERE agg_area_model_id = :agg_area_model_id " );
	sql.bindValueByPosition(0, ':agg_area_model_id');
	return 0;

}


function setAxisMerid( axis_merid , agg_area_model_id  )
{
     if( arguments.length != 2)
     {
	toolBox.errorMessage (  'error in [setAxisMerid(...)]');
	return 1;
     }

     sql.setQuery( "UPDATE agg_spatial_models " +
		   " SET axis_merid = :axis_merid " +
		   " WHERE agg_area_model_id = :agg_area_model_id " );
     sql.bindValueByPosition(0, ':axis_merid');
     sql.bindValueByPosition(1, ':agg_area_model_id');
     return 0;
}

