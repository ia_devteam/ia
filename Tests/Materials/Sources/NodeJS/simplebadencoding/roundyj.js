var imagedir="images/roundy/";
function preload(){
    this.length=preload.arguments.length;
    for (var i=0;i<this.length;i++){
        this[i]=new Image();
        this[i].src=imagedir+preload.arguments[i];
    }
}

var pics=new preload(
        "rndy1.gif","rndr1.gif","rndb1.gif",
        "rndy2.gif","rndr2.gif","rndb2.gif",
        "rndy3.gif","rndr3.gif","rndb3.gif",
        "rndy4.gif","rndr4.gif","rndb4.gif",
        "../buttons/blank.gif",
        "../buttons/edit.gif","../buttons/edit2.gif",
        "../buttons/solve.gif","../buttons/solve2.gif",
        "../buttons/play.gif","../buttons/pause.gif");

var posit = new Array ();
var pairs = new Array(4,1,6,3,  0,9,2,11,  8,5,10,7);
function initbrd(){
    posit = new Array(0,0,0,0,1,1,1,1,2,2,2,2);
}
initbrd();

document.write("<table cellpadding=0 cellspacing=0 border=0 background='images/roundy/rndback.gif'><tr><td><table cellpadding=0 cellspacing=0 border=0 background='images/buttons/blank.gif'>");
for(var i=0; i<2; i++){ //rows
    document.writeln("<tr>");
    for(var j=0; j<3; j++){ //faces
        if(j)
            document.write("<td><img src='images/buttons/blank.gif' width=11 height="+(50-i)+"><\/td>");
        for(var k=0; k<2; k++){ //pieces
            document.write("<td><a href='javascript:clicked("+j+","+(i+i+k)+
                            ");'><img src='images/buttons/blank.gif' width="+(50-k)+
                            " height="+(50-i)+" name='pc"+(j*4+i+i+k)+"' border=0><\/a><\/td>");
        }
    }
    document.write("<\/tr>");
}
document.write("<\/table><\/td><\/tr><\/table>");

document.write("<p><a href='javascript:mix();'><img src='images/buttons/mix.gif' height=16 width=48 border=0><\/a>");
document.write("<a href='javascript:reset();'><img src='images/buttons/reset.gif' height=16 width=48 border=0><\/a>");
document.write("<a href='javascript:edit();'><img src='images/buttons/edit.gif' height=16 width=48 border=0 name='edit'><\/a>");
document.writeln("<a href='javascript:help();'><img src='images/buttons/help.gif' height=16 width=48 border=0><\/a><br>");
document.write("<a href='javascript:solve();'><img src='images/buttons/solve.gif' height=16 width=48 border=0 name='solve'><\/a>");
document.writeln("<a href='javascript:solplay();'><img src='images/buttons/play.gif' height=16 width=48 border=0 name='play'><\/a>");

var mode = 0;   //0=normal  1=solving scrambled  2=edit  3=solving
var seq = new Array();
var edt;

function display(){
    displaybrd();
    displaybut();
    if(mode==1 && solved()){
        alert("You solved it!\nYou don't get a prize for this though!");
        mode=0;
    }
}
function displaybut(){
    if(mode==2) document.images["edit"].src=pics[14].src;
    else document.images["edit"].src=pics[13].src;
    if(mode>=3) document.images["solve"].src=pics[16].src;
    else document.images["solve"].src=pics[15].src;
    if(mode==4) document.images["play"].src=pics[18].src;
    else document.images["play"].src=pics[17].src;
}
function displaybrd(){
    var c=0;
    for (var i=0;i<3;i++){
        for (var j=0;j<4;j++){
            if(posit[c]>2){
                document.images["pc"+c].src = pics[12].src;
            }else{
                document.images["pc"+c].src = pics[3*j+posit[c]].src;
            }
            c++;
        }
    }
}

display();

function solved(){
    for (var i=0;i<3;i++){
        for (var j=0;j<4;j++){
            if( posit[i*4+j]!=i ) return(false);
        }
    }
    return(true);
}


function mix(){
    var i,j,c;
    initbrd();
    for( i=0; i<5; i++){
       k=Math.floor(Math.random()*(6-i));
       if( Math.random()<.5 ){
         c=posit[pairs[k+k]]; posit[pairs[k+k]]=posit[pairs[i+i]]; posit[pairs[i+i]]=c;
         c=posit[pairs[k+k+1]]; posit[pairs[k+k+1]]=posit[pairs[i+i+1]]; posit[pairs[i+i+1]]=c;
       }else{
         c=posit[pairs[k+k]]; posit[pairs[k+k]]=posit[pairs[i+i+1]]; posit[pairs[i+i+1]]=c;
         c=posit[pairs[k+k+1]]; posit[pairs[k+k+1]]=posit[pairs[i+i]]; posit[pairs[i+i]]=c;
       }
    }
    mode=1;
    display();
}

function reset(){
    initbrd();
    mode=0;
    display();
}

function clearbrd(){
    //clear pieces
    for(i=0;i<12;i++) posit[i]=3;
}

function edit(){
    var i;
    if(mode!=2) mode=2;
    clearbrd();
    edt=0;   //first piece to be placed
    display();
}

function clicked(f,p){
    focus();
    var x=f*4+p;
    if(mode==2){  //editing
        if(posit[x]==3){
            for(var i=0; i<12;i++)
              if( x==pairs[i] ) break;
            i = pairs[i^1];
            if( edt<2 ){
              posit[x]=0;
              posit[i]=1;
            }else if( edt<4 ){
              posit[x]=0;
              posit[i]=2;
            }else {
              posit[x]=1;
              posit[i]=2;
            }
            edt++;
            if(edt>=5){
                // get flip parity
                var c=0;
                for(i=0; i<12; i+=2){
                    if(posit[pairs[i]] > posit[pairs[i+1]] ) c++;
                }
                // find remaining blank
                for(i=0; i<12; i+=2){
                    if(posit[pairs[i]] ==3 ) break;
                }
                if(c&1){
                    posit[pairs[i]]=2;
                    posit[pairs[i+1]]=1;
                }else{
                    posit[pairs[i]]=1;
                    posit[pairs[i+1]]=2;
                }
                mode=1;
            }
            display();
        }
    }else if(mode!=4){
        if( p==0 || p==2 ) domove(f+1);
        else { domove(-f-1); }
        if( mode==3 ) mode=0;
        display();
    }
}


function domove(m){
    var c,i=0;
    if(m<0){ i=3; m=-m; }
    else {i=1;}
    while(i--){
       if(m==1){
           c=posit[0];posit[0]=posit[1];posit[1]=posit[3];posit[3]=posit[2 ];posit[2 ]=c;
           c=posit[9];posit[9]=posit[4];posit[4]=posit[6];posit[6]=posit[11];posit[11]=c;
       }else if(m==2){
           c=posit[4];posit[4]=posit[5];posit[5]=posit[7 ];posit[7 ]=posit[6 ];posit[6 ]=c;
           c=posit[1];posit[1]=posit[8];posit[8]=posit[10];posit[10]=posit[3 ];posit[3 ]=c;
       }else if(m==3){
           c=posit[8];posit[8]=posit[9];posit[9]=posit[11];posit[11]=posit[10];posit[10]=c;
           c=posit[5];posit[5]=posit[0];posit[0]=posit[2 ];posit[2 ]=posit[7 ];posit[7 ]=c;
       }
    }
}

//Play back solution
var soltimer;
function solplay(){
    if(mode==4){
        // stop the play in progress
        clearTimeout(soltimer);
        mode=3;
        displaybut();
    }else if(mode!=2){
        // start play
        solve();
        if(mode==3){
            mode=4;
            soltimer=setTimeout("playstep()", 400);
            displaybut();
        }
    }
}
function playstep(){
    if(mode>=3){
        mode=4;
        solve();
        if(mode>=3) soltimer=setTimeout("playstep()", 400);
    }else{
        displaybut();
    }
}


function solve(){
    if(mode==0||mode==1){
        mode=3;
        seq.length=0;

        // If arrays not yet prepared, then do that now.
        if( perm.length==0 ){
            alert("I will have to calculate through all positions.\n"
                    +"This will allow the shortest solution to be\n"
                    +"found in every case. This may take a little\n"
                    +"while, but only needs to be done once. \n"
            );
            calcperm();
        }

        //no solution set up yet. Construct it!
        //save pieces;
        var back=new Array();
        var i;
        for(i=0;i<12;i++) back[i]=posit[i];



        // convert to position number
        // first do orientation
        var q=0, c=1;

        for(i=0; i<5; i++){
            if( posit[pairs[i+i]]<posit[pairs[i+i+1]] ) q+=c;
            c+=c;
        }
        // copy perm into easier array
        var pos=new Array();
        for(i=0; i<6; i++)
             pos[i] = posit[pairs[i+i]] + posit[pairs[i+i+1]] -1;

        // find the 1 pieces, while ignoring the 0 pieces
        a=0;
        for(i=0;   i<5; i++){
            if(pos[i]==1) break;
            if(pos[i]!=0) a++;
        }
        c=0;
        for(j=i+1; j<5; j++){
            if(pos[j]==1) break;
            if(pos[j]!=0) c++;
        }
        q*=6;
        if(a==0) q+=c;
        else if(a==1) q+=c+3;
        else if(a==2) q+=5;

        // find the 0 pieces
        for(i=0;   i<5; i++) if(pos[i]==0) break;
        for(j=i+1; j<5; j++) if(pos[j]==0) break;
        q=q*15+j-i-1;
        if(i==1) q+=5;
        else if(i==2) q+=9;
        else if(i==3) q+=12;
        else if(i==4) q+=14;

        // repeatedly find any good move
        do{
            var n, l=perm[q];
            for(var m=0; m<3; m++){
                n=1; q = permmv[q][m]; if( perm[q]<l ) break;
                n++; q = permmv[q][m];  //skip half turn - working in quarter turn metric
                n++; q = permmv[q][m]; if( perm[q]<l ) break;
                n=0; q = permmv[q][m];
            }
            while(n) { push(-(m+1)); n--; }
        }while(m<3);

        //restore pieces;
        for(i=0;i<12;i++) posit[i]=back[i];

    }
    if(mode>=3){
        //do next move of prerecorded sequence
        if(seq.length==0) mode=0;
        else{
            var c=pop();
            if(seq.length==0) mode=0;
            domove(c);
        }
        display();
    }
}

function push(){
    //adds sequence to movelist, and performs moves as well
    // moves pushed are 1,2,3 for clockwise moves, -1,-2,-3 for anticlockwise moves
    var i,l,m;
    for(i=0;i<push.arguments.length;i++){
        m=push.arguments[i];
        domove(m);
        if(seq.length){
            l=seq[seq.length-1];
            if( l+m==0 ){
                 seq.length--;  //moves cancel
            }else if(seq.length>=2 && l==m && seq[seq.length-2]==m ){
                 seq.length--;
                 seq[seq.length-1]=-m;  // three moves equal one in other dir
            }else{
                 seq[seq.length]=m;
            }
        }else{
            seq[0]= m;
        }
    }
}
function pop(){
    var c=seq[0];
    for(var i=1;i<seq.length;i++) seq[i-1]=seq[i];
    seq.length--;
    return(c);
}


function help(){

    alert(
        "The Roundy puzzle\n\n"+
        "The aim is to arrange the pieces so that each\n"+
        "face is a single colour. Clicking on the left or\n"+
        "right half of a face will rotate it to the left\n"+
        "or right.\n"+
        "\nFurther controls:\n"+
        "Mix:     This button randomly mixes the puzzle up.\n"+
        "Reset: Resets the puzzle to the initial position.\n"+
        "Edit:    Allows you to set up any position. The pieces\n"+
        "            are cleared, and then you can click where the\n"+
        "            yellow parts should be (the yellow/red pieces\n"+
        "            then the yellow/blue) and then a red piece. The\n"+
        "            last piece is filled in automatically\n"+
        "Solve: Solves the puzzle. Each time you click this\n"+
        "            button, one move is performed, until it is solved.\n"+
        "Play:   This solves the puzzle, playing through the whole\n"+
        "            solution automatically. Press it again to pause.\n"+
        "Help:  Shows this help screen.\n"
    );
}



function getprmmv(p,m){
    //given position p<2880 and move m<3, return new position number
    var a,c,s,r,i,j;
    //convert number into array
    var pos=new Array(2,2,2,2,2,2)
    var ori=new Array(0,0,0,0,0,0)
    var q=p;

    //decode piece pair 0
    r= Math.floor(q / 15);
    c= q-15*r;
    q= r;
    if( c<5 )     { pos[0]=0;pos[c+1]=0; }
    else if( c<9 ){ pos[1]=0;pos[c-3]=0; }
    else if( c<12){ pos[2]=0;pos[c-6]=0; }
    else if( c<14){ pos[3]=0;pos[c-8]=0; }
    else if( c<15){ pos[4]=0;pos[5  ]=0; }

    //decode piece pair 1
    r= Math.floor(q / 6);
    c= q-6*r;
    q= r;
    if     (c==0) {a=0; c=0;}
    else if(c==1) {a=0; c=1;}
    else if(c==2) {a=0; c=2;}
    else if(c==3) {a=1; c=0;}
    else if(c==4) {a=1; c=1;}
    else if(c==5) {a=2; c=0;}

    i=0;
    while( pos[i]==0 ) i++;
    while(a){
        do{ i++; }while( pos[i]==0 );
        a--;
    }
    pos[i]=1;
    do{
        do{ i++; }while( pos[i]==0 );
        c--;
    }while(c>=0);
    pos[i]=1;
    if( q&1 ) ori[0]=1;
    if( q&2 ) ori[1]=1;
    if( q&4 ) ori[2]=1;
    if( q&8 ) ori[3]=1;
    if( q&16) ori[4]=1;
    ori[5]= ( ori[0]+ori[1]+ori[2]+ori[3]+ori[4] )&1;

// 2_0_4_2
// 3 1 5 3
    //perform move on array
    if(m==0){
        c=pos[2];pos[2]=pos[3];pos[3]=pos[1];pos[1]=pos[0];pos[0]=c;
        c=ori[2];ori[2]=ori[3];ori[3]=ori[1];ori[1]=ori[0];ori[0]=c;
        ori[3]^=1; ori[0]^=1;
    }else if(m==1){
        c=pos[0];pos[0]=pos[1];pos[1]=pos[5];pos[5]=pos[4];pos[4]=c;
        c=ori[0];ori[0]=ori[1];ori[1]=ori[5];ori[5]=ori[4];ori[4]=c;
        ori[1]^=1; ori[4]^=1;
    }else if(m==2){
        c=pos[4];pos[4]=pos[5];pos[5]=pos[3];pos[3]=pos[2];pos[2]=c;
        c=ori[4];ori[4]=ori[5];ori[5]=ori[3];ori[3]=ori[2];ori[2]=c;
        ori[5]^=1; ori[2]^=1;
    }

    //convert array back to number
    q= ori[0] +ori[1]*2 +ori[2]*4 +ori[3]*8 +ori[4]*16;

    a=0;
    for(i=0;   i<5; i++){
        if(pos[i]==1) break;
        if(pos[i]!=0) a++;
    }
    c=0;
    for(j=i+1; j<5; j++){
        if(pos[j]==1) break;
        if(pos[j]!=0) c++;
    }
    q*=6;
    if(a==0) q+=c;
    else if(a==1) q+=c+3;
    else if(a==2) q+=5;

    for(i=0;   i<5; i++) if(pos[i]==0) break;
    for(j=i+1; j<5; j++) if(pos[j]==0) break;
    q=q*15+j-i-1;
    if(i==1) q+=5;
    else if(i==2) q+=9;
    else if(i==3) q+=12;
    else if(i==4) q+=14;

    return(q)
}


perm=new Array();
permmv=new Array()
function calcperm(){
    //calculate solving arrays
    var p,q,m,n,l;


    //first permutation
    for(p=0;p<2880;p++){
        perm[p]=-1;
        permmv[p]=new Array();
        for(m=0;m<3;m++){
            permmv[p][m]=getprmmv(p,m);
        }
    }
    perm[12*15*6]=0;
    for(l=0;l<9;l++){
        n=0;
        for(p=0;p<2880;p++){
            if(perm[p]==l){
                for(m=0;m<3;m++){
                    q=permmv[p][m];
                    if(perm[q]==-1) { perm[q]=l+1; n++; }
                    q=permmv[q][m];
                    q=permmv[q][m];
                    if(perm[q]==-1) { perm[q]=l+1; n++; }
                }
            }
        }
        //alert("l="+(l+1)+"  n="+n);
        //9, 54, 261, 919, 1246, 378, 12          htm
        //6, 27, 114, 350, 879, 1069,398, 35, 1   qtm
    }
}