VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Form1 
   BackColor       =   &H00FFFFFF&
   Caption         =   "Battle Tanks - STI"
   ClientHeight    =   7185
   ClientLeft      =   60
   ClientTop       =   420
   ClientWidth     =   12480
   LinkTopic       =   "Form1"
   ScaleHeight     =   7185
   ScaleWidth      =   12480
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Tank2 Control"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   8400
      TabIndex        =   23
      Top             =   5280
      Width           =   3615
      Begin VB.Label Label12 
         BackColor       =   &H00FFFFFF&
         Caption         =   "I - ATTACK"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   480
         TabIndex        =   26
         Top             =   480
         Width           =   1575
      End
      Begin VB.Label Label11 
         BackColor       =   &H00FFFFFF&
         Caption         =   "O - SPECIAL"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   480
         TabIndex        =   25
         Top             =   840
         Width           =   1575
      End
      Begin VB.Label Label10 
         BackColor       =   &H00FFFFFF&
         Caption         =   "P - DEFENSE"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   480
         TabIndex        =   24
         Top             =   1200
         Width           =   1575
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Tank1 Control"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   480
      TabIndex        =   19
      Top             =   5280
      Width           =   3615
      Begin VB.Label Label9 
         BackColor       =   &H00FFFFFF&
         Caption         =   "D - DEFENSE"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   480
         TabIndex        =   22
         Top             =   1200
         Width           =   1575
      End
      Begin VB.Label Label8 
         BackColor       =   &H00FFFFFF&
         Caption         =   "S - SPECIAL"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   480
         TabIndex        =   21
         Top             =   840
         Width           =   1575
      End
      Begin VB.Label Label7 
         BackColor       =   &H00FFFFFF&
         Caption         =   "A - ATTACK"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   480
         TabIndex        =   20
         Top             =   480
         Width           =   1575
      End
   End
   Begin VB.Timer Timer4 
      Interval        =   1
      Left            =   5160
      Top             =   7560
   End
   Begin VB.Timer Timer3 
      Interval        =   1
      Left            =   4560
      Top             =   7560
   End
   Begin VB.Timer Timer2 
      Interval        =   1
      Left            =   3960
      Top             =   7560
   End
   Begin VB.Timer Timer1 
      Interval        =   1
      Left            =   3360
      Top             =   7560
   End
   Begin VB.CommandButton cmdDefense2 
      Caption         =   "Defense"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   10320
      TabIndex        =   7
      Top             =   8880
      Width           =   1935
   End
   Begin VB.CommandButton cmdSpecial2 
      Caption         =   "Special"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   10320
      TabIndex        =   6
      Top             =   8280
      Width           =   1935
   End
   Begin VB.CommandButton cmdDefense1 
      Caption         =   "Defense"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   720
      TabIndex        =   5
      Top             =   8760
      Width           =   1935
   End
   Begin VB.CommandButton cmdSpecial1 
      Caption         =   "Special"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   720
      TabIndex        =   4
      Top             =   8160
      Width           =   1935
   End
   Begin VB.CommandButton cmdAttack2 
      Caption         =   "Attack"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   10320
      TabIndex        =   3
      Top             =   7680
      Width           =   1935
   End
   Begin VB.CommandButton cmdAttack1 
      Caption         =   "Attack"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   720
      TabIndex        =   2
      Top             =   7560
      Width           =   1935
   End
   Begin MSComctlLib.ProgressBar pgbTank1 
      Height          =   375
      Left            =   840
      TabIndex        =   0
      Top             =   480
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   0
      Max             =   10
      Scrolling       =   1
   End
   Begin MSComctlLib.ProgressBar pgbTank2 
      Height          =   375
      Left            =   8520
      TabIndex        =   1
      Top             =   480
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   0
      Max             =   10
      Scrolling       =   1
   End
   Begin MSComctlLib.ProgressBar pStank1 
      Height          =   375
      Left            =   840
      TabIndex        =   9
      Top             =   960
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   0
      Max             =   10
      Scrolling       =   1
   End
   Begin MSComctlLib.ProgressBar pDtank1 
      Height          =   375
      Left            =   840
      TabIndex        =   11
      Top             =   1440
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   0
      Max             =   10
      Scrolling       =   1
   End
   Begin MSComctlLib.ProgressBar pStank2 
      Height          =   375
      Left            =   8520
      TabIndex        =   14
      Top             =   960
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   0
      Max             =   10
      Scrolling       =   1
   End
   Begin MSComctlLib.ProgressBar pDtank2 
      Height          =   375
      Left            =   8520
      TabIndex        =   16
      Top             =   1440
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   0
      Max             =   10
      Scrolling       =   1
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   1920
      TabIndex        =   18
      Top             =   7680
      Width           =   375
   End
   Begin VB.Image Image6 
      Height          =   855
      Left            =   6840
      Picture         =   "Form1.frx":0000
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   1335
   End
   Begin VB.Image Image5 
      Height          =   855
      Left            =   3120
      Picture         =   "Form1.frx":3386
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   1215
   End
   Begin VB.Image Image4 
      Height          =   255
      Left            =   3120
      Picture         =   "Form1.frx":49A5
      Stretch         =   -1  'True
      Top             =   3360
      Width           =   615
   End
   Begin VB.Image Image3 
      Height          =   255
      Left            =   7440
      Picture         =   "Form1.frx":5FC4
      Stretch         =   -1  'True
      Top             =   3360
      Width           =   615
   End
   Begin VB.Label Label6 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Defense"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7800
      TabIndex        =   17
      Top             =   1440
      Width           =   735
   End
   Begin VB.Label Label5 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Special"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7800
      TabIndex        =   15
      Top             =   960
      Width           =   615
   End
   Begin VB.Label Label4 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Life"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7800
      TabIndex        =   13
      Top             =   480
      Width           =   615
   End
   Begin VB.Label Label3 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Defense"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   1440
      Width           =   735
   End
   Begin VB.Label Label2 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Special"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   960
      Width           =   615
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Life"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   480
      Width           =   615
   End
   Begin VB.Image Image2 
      Height          =   1455
      Left            =   8040
      Picture         =   "Form1.frx":934A
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   4455
   End
   Begin VB.Image Image1 
      Height          =   1455
      Left            =   120
      Picture         =   "Form1.frx":D110
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   4455
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim oTank1 As New CTank
Dim otank2 As New CTank
Const HiLyt = "{HOME}+{END}"
Private Sub cmdAttack1_Click()

otank2.DefenseStatus = False
Timer2.Enabled = True
End Sub

Private Sub cmdAttack2_Click()
    
    oTank1.DefenseStatus = False
    Timer1.Enabled = True
End Sub

Private Sub cmdDefense1_Click()
oTank1.DefenseStatus = True
Call oTank1.defense(oTank1)
If oTank1.Defensa <= 0 Then
    Me.cmdDefense1.Enabled = False
End If
Me.pDtank1.Value = oTank1.Defensa
End Sub

Private Sub cmdDefense2_Click()
On Error Resume Next
otank2.DefenseStatus = True
Call otank2.defense(otank2)
If otank2.Defensa <= 0 Then
    Me.cmdDefense2.Enabled = False
End If
Me.pDtank2.Value = otank2.Defensa
End Sub

Private Sub Command5_Click()

End Sub

Private Sub cmdSpecial1_Click()
    Timer4.Enabled = True
'    Call otank2.AttackSpecial(otank2)
'    Call oTank1.defense(oTank1)
   ' Me.pStank1.Value = oTank1.Defensa
   On Error Resume Next
    Me.pgbTank2.Value = otank2.Life
    otank2.DefenseStatus = False
     Me.pStank1.Value = oTank1.Special
End Sub

Private Sub cmdSpecial2_Click()
    Timer3.Enabled = True
   ' Call oTank1.AttackSpecial(oTank1)
    'Call otank2.defense(otank2)
    'Me.pStank2.Value = otank2.Defensa
    On Error Resume Next
    Me.pgbTank1.Value = oTank1.Life
    oTank1.DefenseStatus = False
    Me.pStank2.Value = otank2.Special
End Sub

Private Sub Form_Activate()
Text1.SetFocus
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
If KeyAscii = 97 Or KeyAscii = 65 Then
    cmdAttack1_Click
End If
End Sub

Private Sub Form_Load()
oTank1.Life = 10
Me.pgbTank1.Value = oTank1.Life
oTank1.Special = 5
Me.pStank1.Value = oTank1.Special
oTank1.Defensa = 5
Me.pDtank1.Value = oTank1.Defensa


otank2.Life = 10
Me.pgbTank2.Value = otank2.Life
otank2.Special = 5
Me.pStank2.Value = otank2.Special
otank2.Defensa = 5
Me.pDtank2.Value = otank2.Defensa

Image3.Visible = False
Image4.Visible = False
Image5.Visible = False
Image6.Visible = False

Timer1.Enabled = False
Timer2.Enabled = False
Timer3.Enabled = False
Timer4.Enabled = False
End Sub

Private Sub Text1_Change()
SendKeys HiLyt
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
If KeyAscii = 97 Or KeyAscii = 65 Then
    cmdAttack1_Click
End If
If KeyAscii = 115 Or KeyAscii = 83 Then
    If Me.pStank1.Value > 0 Then
        cmdSpecial1_Click
    End If
End If
If KeyAscii = 100 Or KeyAscii = 68 Then
    If Me.pDtank1.Value > 0 Then
        cmdDefense1_Click
    End If
End If

If KeyAscii = 105 Or KeyAscii = 73 Then
    cmdAttack2_Click
End If
If KeyAscii = 111 Or KeyAscii = 79 Then
    If Me.pStank2.Value > 0 Then
        cmdSpecial2_Click
    End If
End If
If KeyAscii = 112 Or KeyAscii = 80 Then
    If Me.pDtank2.Value > 0 Then
        cmdDefense2_Click
    End If
End If
End Sub

Private Sub Timer1_Timer()
On Error Resume Next
Image3.Visible = True
Image3.Left = Image3.Left - 50
If Image3.Left <= 3120 Then
    Call oTank1.Attack(oTank1)
    Me.pgbTank1.Value = oTank1.Life
    Image3.Visible = False
    Timer1.Enabled = False
    Image3.Left = 7440
    If oTank1.Life <= 0 Then
        MsgBox "Tank2 WIN."
    End If
End If
End Sub

Private Sub Timer2_Timer()
On Error Resume Next
Image4.Visible = True
Image4.Left = Image4.Left + 50
If Image4.Left >= 7440 Then
    Call otank2.Attack(otank2)
    Me.pgbTank2.Value = otank2.Life
    Image4.Visible = False
    Timer2.Enabled = False
    Image4.Left = 3120
    If otank2.Life <= 0 Then
        MsgBox "Tank1 WIN."
    End If
End If
End Sub

Private Sub Timer3_Timer()
On Error Resume Next
Image6.Visible = True
Image6.Left = Image6.Left - 50
If Image6.Left <= 3120 Then
    Call oTank1.AttackSpecial(oTank1, otank2)
    Me.pgbTank1.Value = oTank1.Life
    If otank2.Special <= 0 Then
        Me.cmdSpecial2.Enabled = False
    End If
    Me.pStank2.Value = otank2.Special
    Image6.Visible = False
    Timer3.Enabled = False
    Image6.Left = 7440
    If oTank1.Life <= 0 Then
        MsgBox "Tank2 WIN."
    End If
End If
End Sub

Private Sub Timer4_Timer()
Image5.Visible = True
Image5.Left = Image5.Left + 50
If Image5.Left >= 7440 Then
    Call otank2.AttackSpecial(otank2, oTank1)
    On Error Resume Next
    Me.pgbTank2.Value = otank2.Life
    If oTank1.Special <= 0 Then
        Me.cmdSpecial1.Enabled = False
    End If
    Me.pStank1.Value = oTank1.Special
    Image5.Visible = False
    Timer4.Enabled = False
    Image5.Left = 3120
    If otank2.Life <= 0 Then
        MsgBox "Tank1 WIN."
    End If
End If
End Sub
