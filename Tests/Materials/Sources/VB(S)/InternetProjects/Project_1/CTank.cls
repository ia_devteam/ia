VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTank"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mDefense As Integer
Private mSpecial As Integer
Private mLife As Integer
Private mDefenseStatus As Boolean
Private mSpecialStatus As Boolean
Private mAttackStatus As Boolean


Public Property Get AttackStatus() As Boolean
    AttackStatus = mAttackStatus
End Property
Public Property Let AttackStatus(ByVal dAttack As Boolean)
    mAttackStatusStatus = dAttack
End Property


Public Property Get SpecialStatus() As Boolean
    SpecialStatus = mSpecialStatus
End Property
Public Property Let SpecialStatus(ByVal dSpecialStatus As Boolean)
    mSpecialStatus = dSpecialStatus
End Property


Public Property Get DefenseStatus() As Boolean
    DefenseStatus = mDefenseStatus
End Property
Public Property Let DefenseStatus(ByVal dDefenseStatus As Boolean)
    mDefenseStatus = dDefenseStatus
End Property


Public Property Get Life() As Integer
    Life = mLife
End Property
Public Property Let Life(ByVal dLife As Integer)
    mLife = dLife
End Property


Public Property Get Special() As Integer
    Special = mSpecial
End Property
Public Property Let Special(ByVal dSpecial As Integer)
    mSpecial = dSpecial
End Property


Public Property Get Defensa() As Integer
    Defensa = mDefense
End Property
Public Property Let Defensa(ByVal dDefensa As Integer)
    mDefense = dDefensa
End Property

Public Function Attack(otank As CTank)
    Call hit(otank)
End Function

Public Function hit(otank As CTank)
    If otank.DefenseStatus = True Then
        otank.Life = otank.Life
    Else
        otank.Life = otank.Life - 1
    End If
End Function

Public Function defense(otank As CTank)
    otank.Defensa = otank.Defensa - 1
End Function

Public Function AttackSpecial(otank As CTank, ptank As CTank)
    
    Call hitSpecial(otank, ptank)
End Function

Public Function hitSpecial(otank As CTank, ptank As CTank)
    ptank.Special = ptank.Special - 1
    If otank.DefenseStatus = True Then
        otank.Life = otank.Life - 1
    Else
        otank.Life = otank.Life - 2
    End If
End Function
