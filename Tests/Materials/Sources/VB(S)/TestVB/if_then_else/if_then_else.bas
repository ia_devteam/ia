Public Function If_Then_Else_Fun()
If number < 10 Then
    digits = 1
Else
    digits = 3
End If
End Function


Public Function If_Then_Else_OneLine_Fun()
If digits = 1 Then myString = "One" Else myString = "More than one"
End Function


Public Function If_Then_ElseIF_Else_Fun()
If number < 10 Then
    digits = 1
ElseIf number < 100 Then
    digits = 2
ElseIf number < 1000 Then
    digits = 3
ElseIf number < 10000 Then
    digits = 4
ElseIf number < 100000 Then
    digits = 5
ElseIf number < 1000000 Then
    digits = 6
Else
    digits = 100500
End If
End Function
