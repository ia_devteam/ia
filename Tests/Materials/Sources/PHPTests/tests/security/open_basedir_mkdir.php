
<?php
if (substr(PHP_OS, 0, 3) != 'WIN') {
    die('skip Windows only variation');
}
?>

<?php
require_once "open_basedir.inc";
$initdir = getcwd();
test_open_basedir_before("mkdir");

var_dump(mkdir("../bad/blah"));
var_dump(mkdir("../blah"));
var_dump(mkdir("../bad/./blah"));
var_dump(mkdir("./.././blah"));

var_dump(mkdir($initdir."/test/ok/blah"));
var_dump(rmdir($initdir."/test/ok/blah"));
test_open_basedir_after("mkdir");
?>
--CLEAN--
<?php
require_once "open_basedir.inc";
delete_directories();
?>
