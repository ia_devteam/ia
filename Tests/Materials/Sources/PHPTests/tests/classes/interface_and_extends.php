
<?php if (version_compare(zend_version(), '2.0.0-dev', '<')) die('skip ZendEngine 2 needed'); ?>

<?php

interface Test
{
	function show();
}

class Tester extends Test
{
	function show() {
		echo __METHOD__ . "\n";
	}
}

$o = new Tester;
$o->show();

?>
