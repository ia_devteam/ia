
<?php if (version_compare(zend_version(), '2.0.0-dev', '<')) die('skip ZendEngine 2 needed'); ?>

<?php

class pass {
	final function show() {
		echo "Call to function pass::show()\n";
	}
}

$t = new pass();

class fail extends pass {
	function show() {
		echo "Call to function fail::show()\n";
	}
}

echo "Done\n"; // Shouldn't be displayed
?>

