
<?php if (version_compare(zend_version(), '2.0.0-dev', '<')) die('skip ZendEngine 2 needed'); ?>

<?php

class Base {
	private function __destruct() {
    	echo __METHOD__ . "\n";
	}
}

class Derived extends Base {
}

$obj = new Derived;

unset($obj);

?>

