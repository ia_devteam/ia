
<?php if (version_compare(zend_version(), '2.0.0-dev', '<')) die('skip ZendEngine 2 needed'); ?>
--FILE--
<?php

class Caller {
	public $x = array(1, 2, 3);
	
	function __call($m, $a) {
		echo "Method $m called:\n";
		var_dump($a);
		return $this->x;
	}
}

$foo = new Caller();
$a = $foo->test(1, '2', 3.4, true);
var_dump($a);

?>


