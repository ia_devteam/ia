
<?php if (version_compare(zend_version(), '2.0.0-dev', '<')) die('skip ZendEngine 2 needed'); ?>

<?php

final class base {
	function show() {
		echo "base\n";
	}
}

$t = new base();

class derived extends base {
}

echo "Done\n"; // shouldn't be displayed
?>
