﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject
{
    /// <summary>
    /// Реализация интерфейса маршрута автомобиля
    /// </summary>
    public class Route : IRoute
    {
        /// <summary>
        /// <see cref="TestProject.IRoute.Docs"/>
        /// </summary>
        List<IDoc> _docs;

        /// <summary>
        /// <see cref="TestProject.IRoute.Docs"/>
        /// </summary>
        public List<IDoc> Docs
        {
            get
            {
                if (_docs == null)
                    _docs = new List<IDoc>();

                return _docs;
            }
            set
            {
                if (_docs == null)
                    _docs = new List<IDoc>();

                foreach (IDoc doc in value)
                    _docs.Add(doc);
            }
        }

        /// <summary>
        /// Автомобиль
        /// </summary>
        public Car Car { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="car"></param>
        public Route (Car car)
        {
            this.Car = car;
        }
    }
}
