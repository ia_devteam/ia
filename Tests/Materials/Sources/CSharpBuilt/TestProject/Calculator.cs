﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

using System.Device.Location;

namespace TestProject
{
    /*
     * Задача: реализовать метод CalcRoutes в классе Calculator.
     * Метод должен распределять документы по машинам с учётом
     * максимальной грузоподъёмности.
     * В маршрут необходимо добавлять точки, наиболее близкие к 
     * последней точке маршрута (по прямой).
     * Исходная точка - StartLat и StartLon автомобиля.
     * Допускается остаток неразвозимых документов (все машины заполены).
     * Порядок выбора автомобилей не важен.
     * Изменять интерфейсы не допускается.
     * Изменения в классе Calculator не ограничены (кроме сигнатуры метода CalcRoutes),
     *   но работать он должен только с указанными интерфейсами, не с их реализацией.
     * Реализация интерфейсов с загрузкой из любых источников приветствуется.
     * Реализация GUI приветствуется. 
     * Допускается использование стандартных компонентов и компонентов DevExpress.
     */

    /// <summary>
    /// Расчётчик маршрутов
    /// </summary>
    public class Calculator
    {
        /// <summary>
        /// Документы, не попавшие в маршруты автомобилей
        /// </summary>
        private List<IDoc> _unreachedDocs;

        /// <summary>
        /// Конструктор
        /// </summary>
        public Calculator ()
        {
            _unreachedDocs = new List<IDoc>();
        }

        /// <summary>
        /// Получить нераспределенные документы
        /// </summary>
        /// <returns></returns>
        public List<IDoc> GetUnreachedDocs()
        {
            return _unreachedDocs;
        }
        
        /// <summary>
        /// Получить ближайший документ с учетом доступной грузоподъемностью
        /// </summary>
        /// <param name="docs">Документы в наличие</param>
        /// <param name="car">Текущая машина</param>
        /// <param name="route">Текущий маршрут машины</param>
        /// <returns>Возвращает ближайший документ</returns>
        private IDoc GetNearestDoc(List<IDoc> docs, ICar car, IRoute route)
        {
            if (docs == null || docs.Count == 0)
                return null;

            //Общий вес документов
            double routeTotalWeight = 0;
            //Последняя точка
            IDoc lastDoc = null;

            ////Начало списка - точка старта автомобиля. Её считать не нужно. 
            ////Поэтому сохраняем.
            //IDoc carStart = route.Docs[0];
            ////И удаляем. В конце восстановим
            //route.Docs.Remove(carStart);

            //Если есть документы в маршруте
            if (route.Docs.Count != 0)
            {
                routeTotalWeight = route.Docs.Sum(item => item.Weight);
                lastDoc = route.Docs.LastOrDefault();
            }

            //Выбираем только те документы, которые еще можем доставить
            List<IDoc> acceptedDocs = docs.Where(d => d.Weight <= car.MaxWeight - routeTotalWeight).ToList();
            //Не нашли документы - выходим
            if (acceptedDocs == null || acceptedDocs.Count == 0)
            {
                //Восстанавливаем точку старта
                //route.Docs.Insert(0, carStart);
                return null;
            }
            if (acceptedDocs.Count == 1)
                return acceptedDocs[0];

            //Расчет расстояния по прямой до ближайшей точки
            IDoc result = acceptedDocs
                .Aggregate((d1, d2) =>
                {
                    double distanceToD1, distanceToD2;
                    if (lastDoc == null)
                    {
                        distanceToD1 = Math.Sqrt(Math.Pow(car.StartLat - d1.Lat, 2) + Math.Pow(car.StartLon - d1.Lon, 2));
                        distanceToD2 = Math.Sqrt(Math.Pow(car.StartLat - d2.Lat, 2) + Math.Pow(car.StartLon - d2.Lon, 2));
                    }
                    else 
                    {
                        distanceToD1 = Math.Sqrt(Math.Pow(lastDoc.Lat - d1.Lat, 2) + Math.Pow(lastDoc.Lon - d1.Lon, 2));
                        distanceToD2 = Math.Sqrt(Math.Pow(lastDoc.Lat - d2.Lat, 2) + Math.Pow(lastDoc.Lon - d2.Lon, 2));
                    }
                    return distanceToD1 < distanceToD2 ? d1 : d2;
                });

            //Рассчет расстояний с учетом георасположения координат
            //IDoc result = acceptedDocs
            //    .Aggregate((d1, d2) =>
            //    {
            //        var carCoordinate = (lastDoc == null) ? new GeoCoordinate(car.StartLat, car.StartLon) : new GeoCoordinate(lastDoc.Lat, lastDoc.Lon);
            //        var oldDocCoordinate = new GeoCoordinate(d1.Lat, d1.Lon);
            //        var newDocCoordiante = new GeoCoordinate(d2.Lat, d2.Lon);
            //        return carCoordinate.GetDistanceTo(oldDocCoordinate) < carCoordinate.GetDistanceTo(newDocCoordiante) ? d1 : d2;
            //    });

            //Восстанавливаем точку старта
            //route.Docs.Insert(0, carStart);

            return result;
        }

        /// <summary>
        /// Рассчитать маршруты
        /// </summary>
        /// <param name="docs">Список документов</param>
        /// <param name="cars">Список доступных машин</param>
        /// <returns>Список сформированных маршрутов</returns>
        public List<IRoute> CalcRoutes(List<IDoc> docs, List<ICar> cars)
        {
            List<IRoute> result = new List<IRoute>();

            //По всем машинам по-порядку
            using (IEnumerator<ICar> carEnum = cars.GetEnumerator())
            {
                //Пока не закончатся машины или документы
                while (carEnum.MoveNext() && docs.Count != 0)
                {
                    ICar currCar = carEnum.Current;
                    IRoute newRoute = currCar.CreateRoute();
                    IDoc currDoc;
                    //Создаем возможный маршрут
                    while ((currDoc = GetNearestDoc(docs, currCar, newRoute)) != null)
                    {
                        newRoute.Docs.Add(currDoc);
                        docs.Remove(currDoc);
                    }

                    result.Add(newRoute);
                }
            }

            //Если остались не доставленные документы - сохраняем.
            if (docs.Count != 0)
                _unreachedDocs.AddRange(docs);

            return result;
        }
    }

    /// <summary>
    /// Документ для доставки
    /// </summary>
    public interface IDoc
    {
        /// <summary>
        /// Географическая широта
        /// </summary>
        double Lat { get; }
        /// <summary>
        /// Географическая долгота
        /// </summary>
        double Lon { get; }
        /// <summary>
        /// Вес товара
        /// </summary>
        double Weight { get; }
    }

    /// <summary>
    /// Маршрут автомобиля
    /// </summary>
    public interface IRoute
    {
        /// <summary>
        /// Список документов в маршруте
        /// </summary>
        List<IDoc> Docs { get; set; }
    }

    /// <summary>
    /// Автомобиль для доставки
    /// </summary>
    public interface ICar
    {
        /// <summary>
        /// Начальная широта
        /// </summary>
        double StartLat { get; }
        /// <summary>
        /// Начальная долгота
        /// </summary>
        double StartLon { get; }
        /// <summary>
        /// Максимальный вес
        /// </summary>
        double MaxWeight { get; }
        /// <summary>
        /// Создать маршрут с этим автомобилем
        /// </summary>
        /// <returns></returns>
        IRoute CreateRoute();
    }
}
