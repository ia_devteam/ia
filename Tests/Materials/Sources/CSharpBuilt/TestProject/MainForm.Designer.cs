﻿namespace TestProject
{
    partial class main_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cars_groupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.cars_dataGridView = new System.Windows.Forms.DataGridView();
            this.carLat_Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.carLon_Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.carMaxWeight_Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.carLineColor_Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.docs_groupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.docs_dataGridView = new System.Windows.Forms.DataGridView();
            this.docLat_dataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.docLon_dataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weight_dataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.calculate_toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadCars_toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadCarCSV_toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadCarTXT_toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadDocs_toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadDocCSV_toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadDocTXT_toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.export_toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToTXT_toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1.SuspendLayout();
            this.cars_groupBox.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cars_dataGridView)).BeginInit();
            this.docs_groupBox.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.docs_dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.83688F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.16312F));
            this.tableLayoutPanel1.Controls.Add(this.cars_groupBox, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.docs_groupBox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 24);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(705, 443);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // cars_groupBox
            // 
            this.cars_groupBox.Controls.Add(this.tableLayoutPanel2);
            this.cars_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cars_groupBox.Location = new System.Drawing.Point(3, 3);
            this.cars_groupBox.Name = "cars_groupBox";
            this.cars_groupBox.Size = new System.Drawing.Size(296, 215);
            this.cars_groupBox.TabIndex = 2;
            this.cars_groupBox.TabStop = false;
            this.cars_groupBox.Text = "Автомобили";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.cars_dataGridView, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(290, 196);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // cars_dataGridView
            // 
            this.cars_dataGridView.AllowUserToResizeRows = false;
            this.cars_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cars_dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.carLat_Column,
            this.carLon_Column,
            this.carMaxWeight_Column,
            this.carLineColor_Column});
            this.cars_dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cars_dataGridView.Location = new System.Drawing.Point(3, 3);
            this.cars_dataGridView.Name = "cars_dataGridView";
            this.cars_dataGridView.RowHeadersVisible = false;
            this.cars_dataGridView.Size = new System.Drawing.Size(284, 190);
            this.cars_dataGridView.TabIndex = 1;
            this.cars_dataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.cars_dataGridView_CellBeginEdit);
            this.cars_dataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.cars_dataGridView_CellEndEdit);
            this.cars_dataGridView.DefaultValuesNeeded += new System.Windows.Forms.DataGridViewRowEventHandler(this.cars_dataGridView_DefaultValuesNeeded);
            // 
            // carLat_Column
            // 
            this.carLat_Column.HeaderText = "Широта";
            this.carLat_Column.Name = "carLat_Column";
            this.carLat_Column.Width = 50;
            // 
            // carLon_Column
            // 
            this.carLon_Column.HeaderText = "Долгота";
            this.carLon_Column.Name = "carLon_Column";
            this.carLon_Column.Width = 50;
            // 
            // carMaxWeight_Column
            // 
            this.carMaxWeight_Column.HeaderText = "Макс. вес";
            this.carMaxWeight_Column.Name = "carMaxWeight_Column";
            this.carMaxWeight_Column.Width = 50;
            // 
            // carLineColor_Column
            // 
            this.carLineColor_Column.HeaderText = "Цвет линии (hex)";
            this.carLineColor_Column.Name = "carLineColor_Column";
            // 
            // docs_groupBox
            // 
            this.docs_groupBox.Controls.Add(this.tableLayoutPanel3);
            this.docs_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.docs_groupBox.Location = new System.Drawing.Point(3, 224);
            this.docs_groupBox.Name = "docs_groupBox";
            this.docs_groupBox.Size = new System.Drawing.Size(296, 216);
            this.docs_groupBox.TabIndex = 3;
            this.docs_groupBox.TabStop = false;
            this.docs_groupBox.Text = "Документы";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.docs_dataGridView, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(290, 197);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // docs_dataGridView
            // 
            this.docs_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.docs_dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.docLat_dataGridViewTextBoxColumn,
            this.docLon_dataGridViewTextBoxColumn,
            this.weight_dataGridViewTextBoxColumn});
            this.docs_dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.docs_dataGridView.Location = new System.Drawing.Point(3, 3);
            this.docs_dataGridView.Name = "docs_dataGridView";
            this.docs_dataGridView.RowHeadersVisible = false;
            this.docs_dataGridView.Size = new System.Drawing.Size(284, 191);
            this.docs_dataGridView.TabIndex = 2;
            this.docs_dataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.docs_dataGridView_CellEndEdit);
            // 
            // docLat_dataGridViewTextBoxColumn
            // 
            this.docLat_dataGridViewTextBoxColumn.HeaderText = "Широта";
            this.docLat_dataGridViewTextBoxColumn.Name = "docLat_dataGridViewTextBoxColumn";
            // 
            // docLon_dataGridViewTextBoxColumn
            // 
            this.docLon_dataGridViewTextBoxColumn.HeaderText = "Долгота";
            this.docLon_dataGridViewTextBoxColumn.Name = "docLon_dataGridViewTextBoxColumn";
            // 
            // weight_dataGridViewTextBoxColumn
            // 
            this.weight_dataGridViewTextBoxColumn.HeaderText = "Вес";
            this.weight_dataGridViewTextBoxColumn.Name = "weight_dataGridViewTextBoxColumn";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(305, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.tableLayoutPanel1.SetRowSpan(this.pictureBox1, 2);
            this.pictureBox1.Size = new System.Drawing.Size(397, 437);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calculate_toolStripMenuItem,
            this.loadCars_toolStripMenuItem,
            this.loadDocs_toolStripMenuItem,
            this.export_toolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(705, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // calculate_toolStripMenuItem
            // 
            this.calculate_toolStripMenuItem.Name = "calculate_toolStripMenuItem";
            this.calculate_toolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.calculate_toolStripMenuItem.Text = "Расчитать";
            this.calculate_toolStripMenuItem.Click += new System.EventHandler(this.calculate_toolStripMenuItem_Click);
            // 
            // loadCars_toolStripMenuItem
            // 
            this.loadCars_toolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadCarCSV_toolStripMenuItem,
            this.loadCarTXT_toolStripMenuItem});
            this.loadCars_toolStripMenuItem.Name = "loadCars_toolStripMenuItem";
            this.loadCars_toolStripMenuItem.Size = new System.Drawing.Size(144, 20);
            this.loadCars_toolStripMenuItem.Text = "Загрузить автомобили";
            // 
            // loadCarCSV_toolStripMenuItem
            // 
            this.loadCarCSV_toolStripMenuItem.Name = "loadCarCSV_toolStripMenuItem";
            this.loadCarCSV_toolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.loadCarCSV_toolStripMenuItem.Text = "Загрузка из CSV";
            this.loadCarCSV_toolStripMenuItem.Click += new System.EventHandler(this.loadCarsCSV_toolStripMenuItem_Click);
            // 
            // loadCarTXT_toolStripMenuItem
            // 
            this.loadCarTXT_toolStripMenuItem.Name = "loadCarTXT_toolStripMenuItem";
            this.loadCarTXT_toolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.loadCarTXT_toolStripMenuItem.Text = "Загрузка из TXT";
            this.loadCarTXT_toolStripMenuItem.Click += new System.EventHandler(this.loadCarsTXT_toolStripMenuItem_Click);
            // 
            // loadDocs_toolStripMenuItem
            // 
            this.loadDocs_toolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadDocCSV_toolStripMenuItem,
            this.loadDocTXT_toolStripMenuItem});
            this.loadDocs_toolStripMenuItem.Name = "loadDocs_toolStripMenuItem";
            this.loadDocs_toolStripMenuItem.Size = new System.Drawing.Size(137, 20);
            this.loadDocs_toolStripMenuItem.Text = "Загрузить документы";
            // 
            // loadDocCSV_toolStripMenuItem
            // 
            this.loadDocCSV_toolStripMenuItem.Name = "loadDocCSV_toolStripMenuItem";
            this.loadDocCSV_toolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.loadDocCSV_toolStripMenuItem.Text = "Загрузить из CSV";
            this.loadDocCSV_toolStripMenuItem.Click += new System.EventHandler(this.loadDocCSV_toolStripMenuItem_Click);
            // 
            // loadDocTXT_toolStripMenuItem
            // 
            this.loadDocTXT_toolStripMenuItem.Name = "loadDocTXT_toolStripMenuItem";
            this.loadDocTXT_toolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.loadDocTXT_toolStripMenuItem.Text = "Загрузить из TXT";
            this.loadDocTXT_toolStripMenuItem.Click += new System.EventHandler(this.loadDocTXT_toolStripMenuItem_Click);
            // 
            // export_toolStripMenuItem
            // 
            this.export_toolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportToTXT_toolStripMenuItem});
            this.export_toolStripMenuItem.Name = "export_toolStripMenuItem";
            this.export_toolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.export_toolStripMenuItem.Text = "Выгрузить";
            // 
            // exportToTXT_toolStripMenuItem
            // 
            this.exportToTXT_toolStripMenuItem.Name = "exportToTXT_toolStripMenuItem";
            this.exportToTXT_toolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.exportToTXT_toolStripMenuItem.Text = "Выгрузить в TXT";
            this.exportToTXT_toolStripMenuItem.Click += new System.EventHandler(this.exportToTXT_toolStripMenuItem_Click);
            // 
            // main_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(705, 467);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "main_Form";
            this.Text = "Route Calculator";
            this.Load += new System.EventHandler(this.main_Form_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.cars_groupBox.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cars_dataGridView)).EndInit();
            this.docs_groupBox.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.docs_dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox cars_groupBox;
        private System.Windows.Forms.GroupBox docs_groupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView cars_dataGridView;
        private System.Windows.Forms.DataGridView docs_dataGridView;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem calculate_toolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn docLat_dataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn docLon_dataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn weight_dataGridViewTextBoxColumn;
        private System.Windows.Forms.ToolStripMenuItem loadCars_toolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadCarCSV_toolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadCarTXT_toolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadDocs_toolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn carLat_Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn carLon_Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn carMaxWeight_Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn carLineColor_Column;
        private System.Windows.Forms.ToolStripMenuItem loadDocCSV_toolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadDocTXT_toolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem export_toolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToTXT_toolStripMenuItem;
    }
}

