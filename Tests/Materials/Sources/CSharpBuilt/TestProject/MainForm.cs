﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TestProject
{
    public partial class main_Form : Form
    {
        /// <summary>
        /// Картинка, на которой рисуются маршруты
        /// </summary>
        private Bitmap bitmap;

        //Центр картинки
        private float centerX, centerY;
        
        /// <summary>
        /// Список автомобилей
        /// </summary>
        private List<Car> cars;

        /// <summary>
        /// Список документов
        /// </summary>
        private List<Document> documents;

        /// <summary>
        /// Конструктор
        /// </summary>
        public main_Form()
        {
            InitializeComponent();

            centerX = pictureBox1.ClientRectangle.Width / 2;
            centerY = pictureBox1.ClientRectangle.Height / 2;

            foreach (DataGridViewColumn clm in cars_dataGridView.Columns)
            {
                clm.Width = (cars_dataGridView.Width - 5) / cars_dataGridView.Columns.Count;
                clm.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }

            foreach (DataGridViewColumn clm in docs_dataGridView.Columns)
            {
                clm.Width = (cars_dataGridView.Width - 5) / cars_dataGridView.Columns.Count;
                clm.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }

            bitmap = new Bitmap(pictureBox1.Size.Width, pictureBox1.Size.Height);
            pictureBox1.Image = bitmap;
        }

        /// <summary>
        /// Обработчик события - нажали на кнопку "Подсчет маршрутов"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void calculate_toolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClearImage();
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                SaveData();

                Calculator calc = new Calculator(); ;
                List<Route> routes = calc.CalcRoutes(documents.Cast<IDoc>().ToList(), cars.Cast<ICar>().ToList()).Cast<Route>().ToList();                

                using (StreamWriter writer = new StreamWriter("Routs.txt", false, Encoding.Default))
                {
                    foreach (Route route in routes)
                    {
                        Color carColor = ColorTranslator.FromHtml(route.Car.Color);
                        
                        writer.WriteLine("Car route:");
                        writer.Write(string.Format(@"({0}, {1})[{2}]", route.Car.StartLat, route.Car.StartLon, route.Car.MaxWeight));

                        //Рисуем стартовую позицию
                        PointF p1 = new PointF(centerX + (float)route.Car.StartLon * centerX, centerY - (float)route.Car.StartLat * centerY);
                        g.FillEllipse(new SolidBrush(carColor), p1.X - 4, p1.Y - 4, 8, 8);
                        pictureBox1.Image = bitmap;

                        IEnumerator<Document> docEnum = route.Docs.Cast<Document>().GetEnumerator();

                        while (docEnum.MoveNext())
                        {
                            //Рисуем точки доставки и соединяющую линию
                            PointF p2 = new PointF(centerX + (float)docEnum.Current.Lon * centerX, centerY - (float)docEnum.Current.Lat * centerY);
                            g.DrawLine(new Pen(carColor), p1, p2);
                            g.FillRectangle(new SolidBrush(carColor), p2.X - 1, p2.Y - 1, 2, 2);
                            pictureBox1.Image = bitmap;

                            //Перемещаемся на эту точку
                            p1 = p2;

                            writer.Write(string.Format(@" -> ({0}, {1})[{2}]", docEnum.Current.Lat, docEnum.Current.Lon, docEnum.Current.Weight));
                        }
                        writer.WriteLine();
                    }

                    foreach (Document unreachable in calc.GetUnreachedDocs())
                    {
                        PointF p = new PointF(centerX + (float)unreachable.Lon * centerX, centerY - (float)unreachable.Lat * centerY);
                        g.FillRectangle(new SolidBrush(Color.Black), p.X - 2, p.Y - 2, 4, 4);
                        pictureBox1.Image = bitmap;
                    }
                }
            }
        }

        /// <summary>
        /// Сохраняем данные из таблиц
        /// </summary>
        private void SaveData()
        {
            cars = new List<Car>();
            foreach(DataGridViewRow dtgr in cars_dataGridView.Rows)
            {
                try
                {
                    double lat = Convert.ToDouble(dtgr.Cells[0].Value) / 90;
                    double lon = Convert.ToDouble(dtgr.Cells[1].Value) / 180;
                    double maxWeigth = Convert.ToDouble(dtgr.Cells[2].Value);
                    if (maxWeigth == 0)
                        continue;
                    cars.Add(new Car(lat, lon, maxWeigth, dtgr.Cells[3].Value.ToString()));
                }
                catch{}
            }
            documents = new List<Document>();
            foreach (DataGridViewRow dtgr in docs_dataGridView.Rows)
            {
                try
                {
                    double lat = Convert.ToDouble(dtgr.Cells[0].Value) / 90;
                    double lon = Convert.ToDouble(dtgr.Cells[1].Value) / 180;
                    double weigth = Convert.ToDouble(dtgr.Cells[2].Value);
                    if (weigth == 0)
                        continue;
                    documents.Add(new Document(lat, lon, weigth));
                }
                catch { }
            }
        }

        /// <summary>
        /// Обработчик события - отрисовка половин Земли
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void main_Form_Load(object sender, EventArgs e)
        {
            ClearImage();
        }

        /// <summary>
        /// Очистка картинки
        /// </summary>
        private void ClearImage()
        {
            //Рисуем овалы половин Земли
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                g.Clear(Color.White);

                RectangleF rect = new RectangleF(0, 0, pictureBox1.ClientRectangle.Width, pictureBox1.ClientRectangle.Height);
                g.FillRectangle(new SolidBrush(Color.FromArgb(25, Color.Blue)), rect);
            }
        }

        /// <summary>
        /// Обработчик - изменение данных ячеек
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cars_dataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            CheckCells(sender, e);
            if (e.ColumnIndex == 3)
            {
                //Проверяем цвет
                string colorHEX = (string)cars_dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                Color color;
                try
                {
                    color = ColorTranslator.FromHtml(colorHEX);
                }
                catch
                {
                    colorHEX = "#000000";
                    color = ColorTranslator.FromHtml(colorHEX);
                    cars_dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = colorHEX;
                }
                cars_dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = color;
            }
        }

        /// <summary>
        /// Проверка ячеек
        /// </summary>
        /// <param name="sender">Таблица</param>
        /// <param name="e">Данные события</param>
        private static void CheckCells(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                try
                {
                    //Проверяем широту
                    double value = Convert.ToDouble(((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
                    ((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].Value = value;
                    if (value > 90)
                        ((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 90;
                    if (value < -90)
                        ((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].Value = -90;
                }
                catch
                {
                    ((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
                }
            }
            if (e.ColumnIndex == 1)
            {
                try
                {
                    //Проверяем долготу
                    double value = Convert.ToDouble(((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
                    ((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].Value = value;
                    if (value > 180)
                        ((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 180;
                    if (value < -180)
                        ((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].Value = -180;

                }
                catch
                {
                    ((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
                }
            }
            if (e.ColumnIndex == 2)
            {
                try
                {
                    //Проверяем, что введенное число - это беззнаковое число
                    UInt32 value = Convert.ToUInt32(((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
                    ((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].Value = value;
                }
                catch
                {
                    ((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
                }
            }
        }

        /// <summary>
        /// Обработчик - начало редактирования цвета линии
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cars_dataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                cars_dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.White;
            }
        }

        /// <summary>
        /// Обработчик - загрузка данных из CSV
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadCarsCSV_toolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadFromCSV(cars_dataGridView);
        }

        /// <summary>
        /// Обработчик - загрузка данных автомобилей из TXT
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadCarsTXT_toolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadFromTXT(cars_dataGridView);
        }

        /// <summary>
        /// Обработчик - загрузка данных документов из CSV
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadDocCSV_toolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadFromCSV(docs_dataGridView);
        }

        /// <summary>
        /// Обработчик - загрузка данных документов из TXT
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadDocTXT_toolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadFromTXT(docs_dataGridView);
        }

        /// <summary>
        /// Загрузка данных из CSV
        /// </summary>
        /// <param name="dtgv">Таблица</param>
        private void LoadFromCSV(DataGridView dtgv)
        {
            OpenFileDialog ofd = new OpenFileDialog()
            {
                InitialDirectory = Environment.CurrentDirectory,
                Filter = "Файл с разделителем данных(*.csv)|*.csv",
                Multiselect = false
            };

            if (ofd.ShowDialog() != DialogResult.OK)
                return;

            dtgv.Rows.Clear();

            if (ofd.FileName.EndsWith(".csv"))
            {
                using (StreamReader csvReader = new StreamReader(ofd.FileName, Encoding.Default))
                {
                    string line;
                    while ((line = csvReader.ReadLine()) != null)
                    {
                        string[] values = line.Split(';');
                        if (values.Length != 3)
                            continue;

                        try
                        {
                            int lat = int.Parse(values[0]);
                            if (lat < -90 || lat > 90)
                                continue;

                            int lon = int.Parse(values[1]);
                            if (lon < -180 || lon > 180)
                                continue;

                            uint weight = uint.Parse(values[2]);

                            if (dtgv.Equals(cars_dataGridView))
                                dtgv.Rows.Add(lat, lon, weight, "#000000");
                            else
                                dtgv.Rows.Add(lat, lon, weight);
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Загрузка данных из TXT
        /// </summary>
        /// <param name="dtgv"></param>
        private void LoadFromTXT(DataGridView dtgv)
        {
            OpenFileDialog ofd = new OpenFileDialog()
            {
                InitialDirectory = Environment.CurrentDirectory,
                Filter = "Текстовые файлы(*.txt)|*.txt",
                Multiselect = false
            };

            if (ofd.ShowDialog() != DialogResult.OK)
                return;

            dtgv.Rows.Clear();

            if (ofd.FileName.EndsWith(".txt"))
            {
                using (StreamReader csvReader = new StreamReader(ofd.FileName, Encoding.Default))
                {
                    string line;
                    while ((line = csvReader.ReadLine()) != null)
                    {
                        string[] values = line.Split(new char[] { '\t', ' ' });
                        if (values.Length != 3)
                            continue;

                        try
                        {
                            int lat = int.Parse(values[0]);
                            if (lat < -90 || lat > 90)
                                continue;

                            int lon = int.Parse(values[1]);
                            if (lon < -180 || lon > 180)
                                continue;

                            uint weight = uint.Parse(values[2]);
                            if (dtgv.Equals(cars_dataGridView))
                                dtgv.Rows.Add(lat, lon, weight, "#000000");
                            else
                                dtgv.Rows.Add(lat, lon, weight);
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Обработчик - изменение данных ячеек
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void docs_dataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            CheckCells(sender, e);
        }

        /// <summary>
        /// Обработчик - выгрузка в TXT
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exportToTXT_toolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (StreamWriter writer = new StreamWriter("docs.txt", false, Encoding.Default))
            {
                foreach (Document doc in documents)
                {
                    writer.WriteLine("{0}\t{1}\t{2}", doc.Lat * 90, doc.Lon * 180, doc.Weight);
                }
            }

            using (StreamWriter writer = new StreamWriter("cars.txt", false, Encoding.Default))
            {
                foreach (Car car in cars)
                {
                    writer.WriteLine("{0}\t{1}\t{2}", car.StartLat * 90, car.StartLon * 180, car.MaxWeight);
                }
            }
        }

        /// <summary>
        /// Обработчик - при ручном вводе - значение цвета по умолчанию
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cars_dataGridView_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e)
        {
            e.Row.Cells[3].Value = "#000000";
        }
    }
}