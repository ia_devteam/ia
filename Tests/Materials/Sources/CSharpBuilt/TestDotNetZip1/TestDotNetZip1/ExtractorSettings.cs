﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestDotNetZip1
{
    class ExtractorSettings
    {
        public List<string> ReferencedAssemblies { get; set; }

        public List<string> CopyThroughResources { get; set; }
        
        public List<string> ResourcesToCompile { get; set; }

        public SelfExtractorFlavor Flavor { get; set; }
    }
}
