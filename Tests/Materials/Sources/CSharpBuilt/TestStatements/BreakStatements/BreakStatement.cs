﻿namespace TestStatements
{
    public class BreakStatement
    {
        BreakStatement()
        {
            int[] arr = { 0, 1, 2 };

            foreach(int i in arr)
            {
                int a = arr[i];
                break;
            }
        }
    }
}
