﻿namespace GotoStatement
{
    public class GotoStatement
    {
        GotoStatement()
        {
            int a = 0;
            goto label2;
        label1:
            a++;
            if (a < 2)
                goto label3;
            a--;
        label2:
            a++;
            if (a < 2)
                goto label1;
            a--;
        label3:
            a++;
        }
    }
}
