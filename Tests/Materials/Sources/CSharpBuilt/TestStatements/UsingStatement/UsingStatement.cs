﻿using System.IO;

namespace UsingStatement
{
    public class UsingStatement
    {
        UsingStatement()
        {
            Stream stream = FileStream.Null;
            using (StreamReader reader = new StreamReader(stream))
            {
                stream = reader.BaseStream;
            }
        }
    }
}
