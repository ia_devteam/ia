﻿namespace DoWhileStatement
{
    public class DoWhileStatement
    {
        DoWhileStatement()
        {
            int a = 0;
            do
            {
                a++;
            }
            while (a < 10);
        }
    }
}
