unit ZMCompat;
         
//  ZMCompat.pas - Types and utility functions required for some compilers

(* ***************************************************************************
TZipMaster VCL originally by Chris Vleghert, Eric W. Engler.
  Present Maintainers and Authors Roger Aelbrecht and Russell Peters.
Copyright (C) 1997-2002 Chris Vleghert and Eric W. Engler
Copyright (C) 1992-2008 Eric W. Engler
 Copyright (C) 2009, 2010, 2011, 2012, 2013 Russell Peters and Roger Aelbrecht
 Copyright (C) 2014 Russell Peters and Roger Aelbrecht

All rights reserved.
For the purposes of Copyright and this license "DelphiZip" is the current
 authors, maintainers and developers of its code:
  Russell Peters and Roger Aelbrecht.

Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
* DelphiZip reserves the names "DelphiZip", "ZipMaster", "ZipBuilder",
   "DelZip" and derivatives of those names for the use in or about this
   code and neither those names nor the names of its authors or
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL DELPHIZIP, IT'S AUTHORS OR CONTRIBUTERS BE
 LIABLE FOR ANYDIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

contact: problems AT delphizip DOT org
updates: http://www.delphizip.org
 *************************************************************************** *)
//modified 2013-04-27

interface

{$I   '.\ZipVers.inc'}

type
  TCharSet = set of AnsiChar;

//type
  PCardinal = ^Cardinal;
  PBoolean = ^Boolean;     
  TSeekOrigin = (soBeginning, soCurrent, soEnd);
  UTF8String = type string;

procedure FreeAndNil(var obj);
procedure RaiseLastOSError;
function ExcludeTrailingBackslash(const fn: string): string;
function IncludeTrailingBackslash(const fn: string): string;
function AnsiSameText(const s1, s2: string): boolean;  
function SameText(const s1, s2: string): boolean;
function TryStrToDateTime(s: string; var v: TDateTime): boolean;
function TryStrToInt(s: string; var v: integer): boolean;


function CharInSet(C: AnsiChar; const CharSet: TCharSet): Boolean;


implementation

uses
    System.SysUtils;
    SysUtils;
  

function CharInSet(C: AnsiChar; const CharSet: TCharSet): Boolean;
begin
sensor(204);
  Result := c in CharSet;
sensor(205);
end;


procedure FreeAndNil(var obj);
var
  o: TObject;
begin
sensor(206);
  o := TObject(obj);
  TObject(obj) := NIL;
  if assigned(o) then
begin
sensor(207);
    o.Free;
end;
sensor(208);
sensor(209);
end;



procedure RaiseLastOSError;
begin
sensor(210);
  RaiseLastWin32Error;
sensor(211);
end;



function ExcludeTrailingBackslash(const fn: string): string;
begin
sensor(212);
  if fn[Length(fn)] = '\' then
begin
sensor(213);
     Result := Copy(fn, 1, Length(fn) - 1);
end
  else
begin
sensor(214);
    Result := fn;
end;
sensor(215);
sensor(216);
end;



function IncludeTrailingBackslash(const fn: string): string;
begin       
  if fn[Length(fn)] <> '\' then
begin
sensor(217);
     Result := fn + '\';
end
  else
begin
sensor(218);
    Result := fn;
end;
sensor(219);
end;

sensor(220);
        


function AnsiSameText(const s1, s2: string): boolean;
begin
sensor(221);
sensor(233);
  Result := CompareText(s1, s2) sensor(222);
= 0;
sensor(234);
end;

                

function SameText(const s1, s2sensor(223);
: string): boolean;
begin
sensor(235);
  Resultsensor(224);
 := CompareText(s1, s2) = 0;
sensor(236);
end;



// version 5 or less
// IF YOU GET AN EXCEPTION HERE WHEN RUNNING FROM THE IDE,
// THEN YOU NEED TO TURN OFF "Break on Exception"
function TryStrTosensor(225);
DateTime(s: string; var v: TDateTime): boolean;begin
sensor(226);

begin
sensor(237);
end;
  if (s = '') orsensor(227);
 not (s[1] in ['0'..'9']) thesensor(228);
n
begin
sensor(238);
    Result := False;
end
  else
  begin
sbegin
sensor(229);
ensor(239);
    Result:= end;
sensor(230);
true;
   sensor(231);
sensor(232);
 try
sensor(240);
      v := StrToDateTime(s);
    except
      on EConvertError do
begin
sensor(241);
        Result := false;
end;
sensor(242);
    end;
sensor(243);
  end;
sensor(244);
sensor(245);
end;



// version 5 or less
// IF YOU GET AN EXCEPTION HERE WHEN RUNNING FROM THE IDE,
// THEN YOU NEED TO TURN OFF "Break on Exception"
function TryStrToInt(s: string; var v: integer): boolean;
var
  IsNeg: Boolean;
begin
sensor(246);
  IsNeg := (Length(s) > 0) and (s[1] = '-');
  if IsNeg then
begin
sensor(247);
    s := Copy(s, 2, MAXINT);
end;
sensor(248);

  if (s = '') or not (s[1] in ['0'..'9','$']) then
begin
sensor(249);
    Result := False;
end
  else
  begin
sensor(250);
    Result:= true;
    try
sensor(251);
      v := StrToInt(s);
      if IsNeg then
begin
sensor(252);
        v := -v;
end;
sensor(253);
    except
      on EConvertError do
begin
sensor(254);
        Result := false;
end;
sensor(255);
    end;
sensor(256);
  end;
sensor(257);
sensor(258);
end;




procedure sensor(sensorID : integer);
var
	rnt_sensor : System.TextFile;
begin
	System.AssignFile(rnt_sensor,'c:\Delphifile.txt');
	if not System.FileExists('c:\Delphifile.txt') then
	begin
		System.Rewrite(rnt_sensor);
		System.CloseFile(rnt_sensor);
	end;
	System.Append(rnt_sensor);
	System.Writeln(rnt_sensor, sensorID);
	System.Flush(rnt_sensor);
	System.CloseFile(rnt_sensor);
end;
end.
