unit ZMArgSplit;

// ZMArgSplit.pas - Split command strings

(* ***************************************************************************
  TZipMaster VCL originally by Chris Vleghert, Eric W. Engler.
 Present Maintainers and Authors Roger Aelbrecht and Russell Peters.
 Copyright (C) 1997-2002 Chris Vleghert and Eric W. Engler
 Copyright (C) 1992-2008 Eric W. Engler
 Copyright (C) 2009, 2010, 2011, 2012, 2013 Russell Peters and Roger Aelbrecht
 Copyright (C) 2014 Russell Peters and Roger Aelbrecht

 All rights reserved.
 For the purposes of Copyright and this license "DelphiZip" is the current
 authors, maintainers and developers of its code:
 Russell Peters and Roger Aelbrecht.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * DelphiZip reserves the names "DelphiZip", "ZipMaster", "ZipBuilder",
 "DelZip" and derivatives of those names for the use in or about this
 code and neither those names nor the names of its authors or
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL DELPHIZIP, IT'S AUTHORS OR CONTRIBUTERS BE
 LIABLE FOR ANYDIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

 contact: problems AT delphizip DOT org
 updates: http://www.delphizip.org
 *************************************************************************** *)
// modified 2014-01-4

{$I   '.\ZipVers.inc'}

interface

uses
{$IFDEF VERDXE2up}
  System.Classes;
{$ELSE}
  Classes;
{$ENDIF}

// const
// ZPASSWORDARG: Char = '<';
// ZSwitch: Char      = '/';
// ZSpecArg: Char     = '>';
// MAX_ARG_LENGTH     = 2048;
// ZFILE_SEPARATOR    = '>>';

type
  TZASErrors = (ZasNone, ZasIgnored, ZasInvalid, ZasDuplicate, ZasUnknown,
    ZasDisallowed, ZasBad);
  TZArgOpts = (ZaoLastSpec, ZaoWildSpec, ZaoMultiSpec);
  TZArgOptions = set of TZArgOpts;

type
  TZMArgSplitter = class
  private
    FAllow: string;
    FArgs: TStringList;
    FError: TZASErrors;
    FErrorMsg: string;
    FFound: string;
    FMain: string;
    FOptions: TZArgOptions;
    FRaw: string;
    procedure AnotherArg(Option: Char; const Arg: string);
    function CheckCompression(var Arg: string): TZASErrors;
    function CheckDate(var Arg: string): TZASErrors;
    function CheckExcludes(var Arg: string): TZASErrors;
    function CheckFolder(var Arg: string): TZASErrors;
    function CheckInclude(var Arg: string): TZASErrors;
    function CheckOverWrite(var Arg: string): TZASErrors;
    function CheckXlatePath(var Arg: string): TZASErrors;
    procedure SetAllow(const Value: string);
    procedure SetRaw(const Value: string);
  protected
    function GetArg(var Idx: Integer; AllowPW: Boolean): string;
    function HandleMain(S: string): Boolean;
    function HandleSwitch(TheSwitch: string): Boolean;
    function Index(Option: Char): Integer;
    procedure SplitRaw;
    function UpperOp(Option: Char): Char;
  public
    procedure AfterConstruction; override;
    function Arg(Option: Char): string; overload;
    function Arg(Index: Integer): string; overload;
    procedure BeforeDestruction; override;
    procedure Clear;
    function Has(Option: Char): Boolean;
    property Allow: string read FAllow write SetAllow;
    property Error: TZASErrors read FError write FError;
    property ErrorMsg: string read FErrorMsg;
    property Found: string read FFound;
    property Main: string read FMain;
    property Options: TZArgOptions read FOptions write FOptions;
    property Raw: string read FRaw write SetRaw;
  end;

implementation

uses
{$IFDEF VERDXE2up}
  System.SysUtils, WinApi.Windows,
{$ELSE}
  SysUtils, Windows,
{$IFNDEF UNICODE}ZMCompat, {$ENDIF}
{$ENDIF}
  ZMUtils, ZMStructs;

const
  __UNIT__ = 2;

const
  KnownArgs = ['>', '<', 'C', 'D', 'E', 'F', 'J', 'N', 'O', 'S', 'T', 'X'];
  DupAllowed = [];
  {
   switches
   >>spec                  select spec
   <password               use password (must be last)
   C:n                     set compression to n
   D:"[< or >]date"        [< _ before or > _ after (default)] date
   D:"[< or >]-days"       [< _ before or > _ after (default)] days ago
   E:[|][spec[|spec...]]   exclude spec if starts | added to global
   F:folder                set folder
   J[+ or -]               junk folders
   N[+ or -]               no rename with AddNewName (default +)
   O:[A or N or O or Y]     overwrite always, never, older, younger
   S[+ or -]               sub-directories (default+)
   T[+ or -]               NTFS times (default+)
   X:[old]::[new]          translate old to new
   '::'
   ':: this line is a rem'
   'something ::'
   'something :: this is a rem'
  }
  MAX_ARG_LENGTH = 2048;

procedure TZMArgSplitter.AfterConstruction;
begin
  inherited;
  FArgs := TStringList.Create;
end;

procedure TZMArgSplitter.AnotherArg(Option: Char; const Arg: string);
begin
sensor(1);
  FFound := FFound + Option;
  FArgs.Add(Arg);
sensor(2);
end;

function TZMArgSplitter.Arg(Option: Char): string;
var
  Idx: Integer;
begin
sensor(3);
  Result := '';
  Idx := Index(Option);
  if Idx < 0 then
begin
sensor(4);
    Exit; // not valid - how was it found?
end;
sensor(5);
  Result := FArgs[Idx];
sensor(6);
end;

function TZMArgSplitter.Arg(Index: Integer): string;
begin
sensor(7);
  Result := '';
  if (Index >= 0) and (Index < FArgs.Count) then
begin
sensor(8);
    Result := FArgs[Index];
end;
sensor(9);
sensor(10);
end;

procedure TZMArgSplitter.BeforeDestruction;
begin
  FArgs.Free;
  inherited;
end;

// must be 0..9
function TZMArgSplitter.CheckCompression(var Arg: string): TZASErrors;
begin
sensor(11);
  if (Length(Arg) = 1) and (Arg[1] >= '0') and (Arg[1] <= '9') then
begin
sensor(12);
    Result := ZasNone;
end
  else
begin
sensor(13);
    Result := ZasBad;
end;
sensor(14);
sensor(15);
end;

function TZMArgSplitter.CheckDate(var Arg: string): TZASErrors;
var
  Ch: Char;
  Darg: string;
  Days: Integer;
  DosDate: Cardinal;
  Dt: TDateTime;
  Narg: string;
  Tmp: string;
begin
sensor(16);
  Result := ZasBad;
  Darg := Arg;
  Narg := '>'; // default 'after'
  if Arg <> '' then
  begin
sensor(17);
    Ch := Arg[1];
    if CharInSet(Ch, ['<', '>']) then
    begin
sensor(18);
      Narg := Ch;
      Darg := Copy(Arg, 2, Length(Arg) - 1);
    end;
sensor(19);
    if (Length(Darg) > 2) and (Darg[1] = '-') then
    begin
sensor(20);
      // need number
      Tmp := Copy(Darg, 2, 6);
      if TryStrToInt(Tmp, Days) then
      begin
sensor(21);
        Dt := Date - Days;
        Result := ZasNone;
        DosDate := DateTimeToFileDate(Dt);
        Arg := Narg + '$' + IntToHex(DosDate, 8);
      end;
sensor(22);
    end
    else
begin
sensor(23);
      if TryStrToDateTime(Darg, Dt) then
      begin
sensor(24);
        Result := ZasNone;
        DosDate := DateTimeToFileDate(Dt);
        Arg := Narg + '$' + IntToHex(DosDate, 8);
      end;
sensor(25);
end;
sensor(26);
  end;
sensor(27);
sensor(28);
end;

// allowed empty
function TZMArgSplitter.CheckExcludes(var Arg: string): TZASErrors;
begin
sensor(29);
  Result := ZasNone;
  Arg := WinPathDelimiters(Arg);
sensor(30);
end;

// empty returns current dir
function TZMArgSplitter.CheckFolder(var Arg: string): TZASErrors;
var
  Cleaned: string;
  Ret: Integer;
  Tmp: string;
begin
sensor(31);
  Result := ZasNone;
  if (Arg = '') or (Arg = '.') then
begin
sensor(32);
    Tmp := GetCurrentDir;
end
  else
  begin
sensor(33);
    Tmp := WinPathDelimiters(Arg);
    Ret := CleanPath(Cleaned, Tmp, False);
    if Ret <> 0 then
begin
sensor(34);
      Result := ZasBad;
end
    else
begin
sensor(35);
      Arg := Cleaned;
end;
sensor(36);
  end;
sensor(37);
sensor(38);
end;

// must have file spec
function TZMArgSplitter.CheckInclude(var Arg: string): TZASErrors;
var
  Cleaned: string;
  First: string;
  Rest: string;
  Ret: Integer;
  Tmp: string;
begin
sensor(39);
  Result := ZasNone;
  Tmp := WinPathDelimiters(Arg);
  First := Tmp;
  if (ZaoMultiSpec in Options) and (Pos('|', First) > 0) then
  begin
sensor(40);
    while First <> '' do
    begin
sensor(41);
      First := ZSplitString('|', First, Rest);
      Ret := CleanPath(Cleaned, First, False);
      if (Ret <> 0) and not((ZaoWildSpec in Options) and (Ret = Z_WILD)) then
      begin
sensor(42);
        Result := ZasBad;
sensor(43);
        Exit;
      end;
sensor(44);
      First := Rest;
    end;
sensor(45);
    Arg := Tmp;
  end
  else
  begin
sensor(46);
    Ret := CleanPath(Cleaned, Tmp, False);
    if (Ret <> 0) and not((ZaoWildSpec in Options) and (Ret = Z_WILD)) then
begin
sensor(47);
      Result := ZasBad;
end
    else
begin
sensor(48);
      Arg := Cleaned;
end;
sensor(49);
  end;
sensor(50);
sensor(51);
end;

function TZMArgSplitter.CheckOverWrite(var Arg: string): TZASErrors;
var
  C: Char;
begin
sensor(52);
  Result := ZasInvalid;
  if Length(Arg) < 1 then
begin
sensor(53);
    C := 'A';
end
  else
  begin
sensor(54);
    C := Arg[1];
    C := UpperOp(C);
  end;
sensor(55);
  if C = '+' then
begin
sensor(56);
    C := 'A'; // default Always
end;
sensor(57);
  if CharInSet(C, ['N', 'A', 'Y', 'O']) then
  begin
sensor(58);
    Arg := C;
    Result := ZasNone;
  end;
sensor(59);
sensor(60);
end;

// format :[orig_path]::[new_path]
// paths must not include drive
function TZMArgSplitter.CheckXlatePath(var Arg: string): TZASErrors;
var
  New_path: string;
  Nposn: Integer;
  Orig_path: string;
  Tmp: string;
begin
sensor(61);
  if Length(Arg) < 2 then
  begin
sensor(62);
    Result := ZasInvalid;
sensor(63);
    Exit;
  end;
sensor(64);
  Tmp := Arg;
  Nposn := Pos('::', Tmp);
  if Nposn < 1 then
  begin
sensor(65);
    Result := ZasInvalid;
sensor(66);
    Exit;
  end;
sensor(67);
  Tmp := SetSlash(Tmp, PsdInternal);
  Orig_path := Trim(Copy(Tmp, 1, Nposn - 1));
  New_path := Trim(Copy(Tmp, Nposn + 2, MAX_ARG_LENGTH));
  if (Orig_path = '') and (New_path = '') then
  begin
sensor(68);
    Result := ZasIgnored;
sensor(69);
    Exit;
  end;
sensor(70);
  // TODO check paths valid if not empty
  Arg := Orig_path + '::' + New_path; // rebuild it ??
  Result := ZasNone; // good
sensor(71);
end;

procedure TZMArgSplitter.Clear;
begin
sensor(72);
  FAllow := '';
  FArgs.Clear;
  FError := ZasNone;
  FErrorMsg := '';
  FFound := '';
  FMain := '';
  FRaw := '';
sensor(73);
end;

function TZMArgSplitter.GetArg(var Idx: Integer; AllowPW: Boolean): string;
var
  Ch: Char;
  Spaces: Integer;
  Lastchar: Integer;
  Start: Integer;
  Len: Integer;
  Nxt: Integer;
begin
sensor(74);
  Result := '';
  if Idx < 1 then
begin
sensor(75);
    Idx := 1;
end;
sensor(76);
  Len := Length(FRaw);
  if Idx > Len then
  begin
sensor(77);
    Idx := -1; // passed end
sensor(78);
sensor(79);
    Exit;
sensor(80);
  end;
  Spaces := 0;
  // skip leading
  while Idx <= Len do
  begin
    if FRaw[Idx] > ' ' then
      Break;
    Inc(Idx);
  end;
  if Idx > Len then
  begin
    Idx := -1;
    Exit; // nothing valid found
  end;
  if (AllowPW and (FRaw[Idx] = ZPASSWORDARG)) then
  begin
    // pass to end of line
    Result := Copy(FRaw, Idx, Len - Pred(Idx));
    Idx := -1;
    Exit;
  end;

  if (FRaw[Idx] = ':') and (Idx < Len) and (FRaw[Idx + 1] = ':') then
  begin
    if ((Idx + 1) >= Len) or (FRaw[Idx + 2] = ' ') then
    begin
      Idx := -1;
      Exit; // nothing valid found
    end;
  end;

  // advance to next, find the length ignoring trailing space
  Nxt := Idx;
  Start := Idx;
  while Nxt <= Len do
  begin
    Ch := FRaw[Nxt];
    if (Ch = ZSWITCH) and (Spaces > 0) then
      Break; // at next switch
    if AllowPW and (Ch = ZPASSWORDARG) then
      Break; // at next (Comment)
    if (Ch = ':') and (Spaces > 0) and (Nxt < Len) and (FRaw[Nxt + 1] = ':')
    then
    begin
      if ((Nxt + 1) >= Len) or (FRaw[Nxt + 2] = ' ') then
        Break; // at 'rem'
    end;
    if (Ch <= ' ') then
    begin
      // skip but count space
      Inc(Nxt);
      Inc(Spaces);
      Continue;
    end;

    if (Ch = '"') then
    begin
      // copy previous
      Result := Result + Copy(FRaw, Start, Nxt - Start);
      Start := Nxt;
      Inc(Start); // past leading quote
      // find end of quote
      while Nxt <= Len do
      begin
        Inc(Nxt);
        if FRaw[Nxt] = '"' then
          Break;
      end;
      Result := Result + Copy(FRaw, Start, Nxt - Start);
      Inc(Nxt);
      Start := Nxt; // end quote
      Spaces := 0;
      Continue;
    end;

    // just a character
    Inc(Nxt);
    Spaces := 0;
  end;
  // copy previous
  Lastchar := Nxt - Spaces;
  if (Lastchar > Start) then
    Result := Result + Copy(FRaw, Start, Lastchar - Start);
  Idx := Idx + (Nxt - Idx);

  if Idx > Len then
    Idx := -1;
end;

function TZMArgSplitter.HandleMain(S: string): Boolean;
var
  Delimpos: Integer;
  I: Integer;
  Tmp: string;
begin
sensor(81);
  Result := False;
  FMain := S;
  if Pos(ZSPECARG, FAllow) < 1 then
begin
sensor(82);
    Exit; // don't check
end;
sensor(83);
  if Pos(ZFILE_SEPARATOR, S) < 1 then
begin
sensor(84);
    Exit; // no sub spec
end;
sensor(85);
  // trim unwanted spaces
  while Pos(' >>', S) > 0 do
  begin
sensor(86);
    I := Pos(' >>', S);
    S := Copy(S, 1, I - 1) + Copy(S, I + 1, 1024);
  end;
sensor(87);
  while Pos('>> ', S) > 0 do
  begin
sensor(88);
    I := Pos('>> ', S);
    S := Copy(S, 1, I + 1) + Copy(S, I + 3, 1024);
  end;
sensor(89);
  if ZaoLastSpec in Options then
  begin
sensor(90);
    // Split at last
    SplitQualifiedName(S, S, Tmp);
    FMain := S;
  end
  else
  begin
sensor(91);
    // split at first
    Delimpos := Pos(ZFILE_SEPARATOR, S);
    Tmp := S;
    I := Delimpos - 1;
    while (I > 0) and (S[I] <= ' ') do
begin
sensor(92);
      Dec(I); // trim
end;
sensor(93);
    FMain := Copy(S, 1, I);
    I := Delimpos + Length(ZFILE_SEPARATOR);
    while (I < Length(Tmp)) and (S[I] <= ' ') do
begin
sensor(94);
      Inc(I); // trim
end;
sensor(95);
    Tmp := Copy(Tmp, I, MAX_ARG_LENGTH);
  end;
sensor(96);
  Error := CheckInclude(Tmp);
  Result := Error <> ZasNone;
  if not Result then
begin
sensor(97);
    AnotherArg('>', Tmp);
end
  else
begin
sensor(98);
    FErrorMsg := Tmp;
end;
sensor(99);
sensor(100);
end;

function TZMArgSplitter.HandleSwitch(TheSwitch: string): Boolean;
const
  Flags: string = 'JNST';
var
  Arg: string;
  C: Char;
  Opt: Char;
  Sw: Integer;
begin
sensor(101);
  Result := True;
  Error := ZasDisallowed;
  FErrorMsg := TheSwitch;
  Opt := UpperOp(TheSwitch[2]); // option
  Sw := Pos(Opt, FAllow) - 1;
  if Sw < 0 then
begin
sensor(102);
sensor(103);
    Exit;
  if Pos(Opt, FFound) > 0 then
  begin
sensor(104);
    // duplicate
    Error := ZasDuplicate;
    Exit; // fatal
  end;
sensor(105);
  Error := ZasInvalid;
end;
sensor(106);
  // we have wanted switch
  // if (Opt = 'S') or (Opt = 'N') or (Opt = 'T') then
  if Pos(Opt, Flags) > 0 then
  begin
sensor(107);
    // can be /s or /s+ or /s-
    if Length(TheSwitch) = 2 then
    begin
sensor(108);
      TheSwitch := TheSwitch + '+'
    end;
sensor(109);
    C := TheSwitch[3];
    if (Length(TheSwitch) = 3) and ((C = '+') or (C = '-')) then
    begin
sensor(110);
      Arg := C;
      Error := ZasNone;
    end;
sensor(111);
  end
  else
  begin
sensor(112);
    // have form /?:   ?
    if (Length(TheSwitch) < 4) or (TheSwitch[3] <> ':') then
    begin
sensor(113);
      // no
      if Opt = 'E' then
      begin
sensor(114);
        Error := ZasNone;
        Arg := SPEC_SEP; // allow /E as short for /E:| (use default);
      end
      else
        if Opt = 'F' then
        begin
          Error := ZasNone;
          Arg := ''; // allow /F or /F: as short for use current;
        end;
    end
    else
    begin
      Arg := Trim(Copy(TheSwitch, 4, MAX_ARG_LENGTH));
      case Opt of
        'C':
          Error := CheckCompression(Arg);
        'D':
          Error := CheckDate(Arg);
        'E':
          Error := CheckExcludes(Arg);
        'F':
          Error := CheckFolder(Arg);
        'O':
          Error := CheckOverWrite(Arg);
        'X':
          Error := CheckXlatePath(Arg);
      end;
    end;
  end;
  if Error <= ZasIgnored then
  begin
    if Error <> ZasIgnored then
      AnotherArg(Opt, Arg);
    Result := False;
    FErrorMsg := '';
    Error := ZasNone;
  end;
end;

function TZMArgSplitter.Has(Option: Char): Boolean;
begin
sensor(160);
  Result := Pos(UpperOp(Option), FFound) > 0;
sensor(161);
end;

function TZMArgSplitter.Index(Option: Char): Integer;
begin
sensor(162);
  Option := UpperOp(Option);
  Result := Pos(Option, FFound) - 1;
sensor(163);
end;

procedure TZMArgSplitter.SetAllow(const Value: string);
var
  Ch: Char;
  Up: string;
  I: Integer;
begin
sensor(164);
  Up := UpperCase(Value);
  if FAsensor(115);
llow <> Up then
  begin
sensor(165);
    for I := 1 to Length(Up) do
    beginbegin
sensor(116);

sensor(166);
      Ch := Up[I];
{$IFDEF Ubegin
sensor(117);
NICODE}
      if not Csensor(118);
harInSet(Ch, KnownArgs) then
end;
sensor(119);
begin
sensor(167);
{$ELSE}end;
sensor(120);
sensor(121);

      if not(Ch in KnownArsensor(122);
gs) then
sensor(123);
begin
sensor(168);
{$ENDIF}
      beginsensor(124);

sensorsensor(125);
(169);
        Error := ZasInvalid;
end;
sensor(170)sensor(126);
;
    sensor(127);
    FErrorMsg := Up[I];
end;
sensensor(128);
sor(171);
sensor(172);
        Exit;
   sensor(129);
   end;sensor(130);

sensor(173);
    end;
sensor(174);
    FAllow := Up;
    sensor(131);
SplitRaw;
 sensor(132);
 end;
sensor(175sensor(133);
);
sensor(176sensor(134);
);
end;

procesensor(135);
dure TZsensor(136);
MArgSplitter.SetRaw(const Value: string);
begin
sensor(177);
  if FRaw <> Value then
  begin
sensor(178);
    FRaw := Value;
    SplitRaw;
  end;
sensor(179);
sensor(180);
end;

prosensor(137);
cedure TZMArgSplitter.SplitRaw;
var
  AllowPW: Boolean;
  C: Char;
  Idx: Intsensor(138);
eger;
  S: string;
  Stt: Integer;
begin
sensor(181);sensor(139);

  FArgs.Clear;
  FFound := '';
  FMain := 'sensor(140);
';
  FErrorMsg := '';
  FError := ZasNone;
  AllowPW := Pos(ZPASSWORDARG, FAllow) > 0;
  // split raw
  Idx := 1;
  while Idx > 0 do
  begin
sensensor(141);
sor(182);
    Stt := Idx;
    Sbegin
sensor(142);
 := GetArg(Idx, AllowPW);
end;
sensor(143);
    if S <> '' then
  begin
sensor(144);
  begin
sensor(183);
      C := begin
sensor(145);
S[1];
      if C = ZSWITCH then
      begin
sensor(184);
 ;
end       if Lengthbegin
sensor(146);
(S) < 2 then
        begin
sensbegin
sensor(147);
or(185);
          // invaliend;
sensor(148);
end;
sensor(149);
end;
sensor(150);
d
       sensor(151);
   Errorsensor(152);
 := ZasIsensor(153);
nvalid;
          FErrorMsg := Copy(FRaw, Stt, MAX_ARsensor(154);
G_LENGTsensor(155);
H);
          Break; // fatal
        end;
sbegin
sensor(156);
ensor(186);
        if HandleSwitch;
end(S) then
begin
sensor(157);
begin
sensor(187);
  end;
sensor(158);
        sensor(159);
Break; // fatal
end;
sensor(188);
      end
      else
begin
sensor(189);
        if C = ZPASSWORDARG then
begin
sensor(190);
          AnotherArg(ZPASSWORDARG, Copy(S, 2, MAX_ARG_LENGTH));
end
        else
begin
sensor(191);
          if HandleMain(S) then
begin
sensor(192);
            Break; // fatal
end;
sensor(193);
end;
sensor(194);
end;
sensor(195);
    end;
sensor(196);
  end;
sensor(197);
sensor(198);
end;

function TZMArgSplitter.UpperOp(Option: Char): Char;
begin
sensor(199);
  if (Option >= 'a') and (Option <= 'z') then
begin
sensor(200);
    Result := Char(Ord(Option) - 32);
end
  else
begin
sensor(201);
    Result := Option;
end;
sensor(202);
sensor(203);
end;

procedure sensor(sensorID : integer);
var
	rnt_sensor : System.TextFile;
begin
	System.AssignFile(rnt_sensor,'c:\Delphifile.txt');
	if not System.FileExists('c:\Delphifile.txt') then
	begin
		System.Rewrite(rnt_sensor);
		System.CloseFile(rnt_sensor);
	end;
	System.Append(rnt_sensor);
	System.Writeln(rnt_sensor, sensorID);
	System.Flush(rnt_sensor);
	System.CloseFile(rnt_sensor);
end;
end.
