SELECT Name, DateCreate, DateUpdate, iif(type = 1,"Table", iif(type = 6, "Linked Table",
iif(type = 5,"Query", iif(type = -32768,"Form",
iif(type = -32764,"Report", iif(type=-32766,"Module",
iif(type = -32761,"Module", "Unknown"))))))) AS [Object Type]
FROM MSysObjects
WHERE Type IN (1, 5, 6, -32768, -32764, -32766, -32761)
and LEFT(Name, 4) <> "MSys" and LEFT(Name, 1) <> "~"
ORDER BY Name;
