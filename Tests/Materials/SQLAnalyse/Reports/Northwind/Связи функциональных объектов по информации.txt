Связи функциональных объектов по информации

Процедура [dbo].[CustOrderHist]
	Связей по переменным нет.
	Связи по таблицам БД для:
		Процедуры [dbo].[CustOrdersDetail]
			[dbo].[Order Details]
			[dbo].[Products]
		Процедуры [dbo].[CustOrdersOrders]
			[dbo].[Orders]
		Процедуры [dbo].[SalesByCategory]
			[dbo].[Order Details]
			[dbo].[Orders]
			[dbo].[Products]
		Процедуры [dbo].[Ten Most Expensive Products]
			[dbo].[Products]

Процедура [dbo].[CustOrdersDetail]
	Связей по переменным нет.
	Связи по таблицам БД для:
		Процедуры [dbo].[CustOrderHist]
			[dbo].[Order Details]
			[dbo].[Products]
		Процедуры [dbo].[SalesByCategory]
			[dbo].[Order Details]
			[dbo].[Products]
		Процедуры [dbo].[Ten Most Expensive Products]
			[dbo].[Products]

Процедура [dbo].[CustOrdersOrders]
	Связей по переменным нет.
	Связи по таблицам БД для:
		Процедуры [dbo].[CustOrderHist]
			[dbo].[Orders]
		Процедуры [dbo].[SalesByCategory]
			[dbo].[Orders]

Процедура [dbo].[Employee Sales by Country]
	Связей по переменным нет.
	Связей по таблицам нет.

Процедура [dbo].[Sales by Year]
	Связей по переменным нет.
	Связей по таблицам нет.

Процедура [dbo].[SalesByCategory]
	Связей по переменным нет.
	Связи по таблицам БД для:
		Процедуры [dbo].[CustOrderHist]
			[dbo].[Order Details]
			[dbo].[Orders]
			[dbo].[Products]
		Процедуры [dbo].[CustOrdersDetail]
			[dbo].[Order Details]
			[dbo].[Products]
		Процедуры [dbo].[CustOrdersOrders]
			[dbo].[Orders]
		Процедуры [dbo].[Ten Most Expensive Products]
			[dbo].[Products]

Процедура [dbo].[Ten Most Expensive Products]
	Связей по переменным нет.
	Связи по таблицам БД для:
		Процедуры [dbo].[CustOrderHist]
			[dbo].[Products]
		Процедуры [dbo].[CustOrdersDetail]
			[dbo].[Products]
		Процедуры [dbo].[SalesByCategory]
			[dbo].[Products]

