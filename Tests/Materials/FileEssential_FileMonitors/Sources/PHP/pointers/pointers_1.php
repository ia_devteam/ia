<?php

    class a {
        public $a = 1;
    }
    
    class b {
        static $var;
        
        public static function & gvar($i) {
            if(!(isset(b::$var[$i]))) {
                b::$var[$i] = new a;
            }
            
            return b::$var[$i];
        }
    }
    
    $b = new b;
    $a = b::gvar(1);
    $a->a = 14;
    
    $c = b::gvar(1);
    echo $c->a; // 14
?>