<?php
function slashit(&$aray, $db_link)
{
  foreach ($aray as $key => &$value)
   if(is_array($value)) slashit($value, $link);
   else $aray[$key] = mysql_real_escape_string($value, $db_link);
}

// Test:
$fruits = array (
    "fruits"  => array("a" => "or'ange", "b" => "ban'ana", "c" => "apple'"),
    "numbers" => array(1, 2, 3, 4, 5, 6),
    "holes"   => array("fir'st", 5 => "sec'ond", "thir'd"),
    "odma"    => "jugo'slavija"
);

// You have to make link to the database or can use addslashes instead of mysql_real_escape_string and remove $link from function definition

slashit($fruits, $dbLink);
echo "<pre>"; print_r($fruits); echo "</pre>";
?>