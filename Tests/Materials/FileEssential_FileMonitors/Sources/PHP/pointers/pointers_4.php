<?php
 function foo() {
    echo "In foo()<br />\n";
 }
 
 function bar($arg = '')
 {
    echo "In bar(); argument was '$arg'.<br />\n";
 }
 
 // �������-������� ��� echo
 function echoit($string)
 {
    echo $string;
 }

 $func = 'foo';
 $func();        // �������� ������� foo()
 
 $func = 'bar';
 $func('test');  // �������� ������� bar()
 
 $func = 'echoit';
 $func('test');  // �������� ������� echoit()
 
 $func = 'omg_wtf';
 $funct(); // �������� ���???
 ?>