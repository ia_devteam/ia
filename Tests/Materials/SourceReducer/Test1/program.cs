﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.IO;

namespace FindStringsInFiles
{
    class Program
    {
        class LinePosition
        {
            internal string File;
            internal List<ulong> Line = new List<ulong>();
        }

        [STAThreadAttribute]
        static void Main(string[] args)
        {
            OpenFileDialog ofd = new OpenFileDialog() { InitialDirectory = @"d:\123\Work\" };
            while (ofd.ShowDialog()!= DialogResult.OK)
            {
                Console.WriteLine("\nНе выбран файл. Выйти? (y|n)");
                if (Console.ReadKey().KeyChar == 'y')
                    return;
            }

            FolderBrowserDialog fbd = new FolderBrowserDialog() { SelectedPath = @"d:\123\Work\" };
            while (fbd.ShowDialog() != DialogResult.OK)
            {
                Console.WriteLine("\nНе выбрана директория. Выйти? (y|n)");
                if (Console.ReadKey().KeyChar == 'y')
                    return;
            }
            ulong count = (ulong)Directory.EnumerateFiles(fbd.SelectedPath,"*.*", SearchOption.AllDirectories).Count();

            Console.WriteLine("\nПоиск...");

            Dictionary<string, List<LinePosition>> dict = new Dictionary<string,List<LinePosition>>();

            string reportPath = Path.Combine(Path.GetDirectoryName(ofd.FileName), "Reports");
            if (!Directory.Exists(reportPath))
                Directory.CreateDirectory(reportPath);
            else
            {
                DirectoryInfo di = new DirectoryInfo(reportPath);

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
            }

            using (StreamReader reader = new StreamReader(ofd.FileName))
            {
                string signature;
                while ((signature = reader.ReadLine()) != null)
                {
                    Console.WriteLine("\n{0}...", signature);
                    List<LinePosition> curSig = new List<LinePosition>();

                    ulong curCount = 0;
                    foreach (string file in Directory.GetFiles(fbd.SelectedPath, "*.*", SearchOption.AllDirectories))
                    {
                        curCount++;
                        Console.Write(String.Format("\r{0,1:P3}", curCount / (double)count));
                        LinePosition curFile = new LinePosition();
                        curFile.File = file;
                        using (StreamReader currentFile = new StreamReader(file))
                        {
                            ulong counter = 0;
                            string line;
                            while ((line = currentFile.ReadLine()) != null)
                            {
                                counter++;
                                if (line.IndexOf(signature) != -1)
                                {
                                    curFile.Line.Add(counter);
                                }
                            }
                        }
                        curSig.Add(curFile);
                    }
                    dict.Add(signature, curSig);

                    WriteReport(reportPath, signature, curSig);
                }
            }
        }
//
//        private static void WriteReport(string reportPath, string signature, List<LinePosition> curSig)
//        {
//            using (StreamWriter writer = new StreamWriter(Path.Combine(reportPath, signature + ".txt")))
//            {
//                foreach(LinePosition lp in curSig)
//                {
//                    foreach (ulong line in lp.Line)
//                    {
//                        writer.WriteLine(signature + "\t" + lp.File + "\t" + line);
//                    }
//                }
//            }
//        }
    }
}
