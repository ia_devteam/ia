using System;
using System.Data;
using System.Data.Linq;
using System.Reflection;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.ComponentModel;
using ITable = DbLinq.Data.Linq.ITable;
using DbLinq;
using DbLinq.Data.Linq.Implementation;
using DbLinq.Data.Linq.Sugar;
namespace DbLinq.Data.Linq
{
  public sealed partial class Table<TEntity> : ITable, IQueryProvider, IListSource, IEnumerable<TEntity>, IEnumerable, IQueryable<TEntity>, IQueryable where TEntity : class
  {
    IQueryable<S> IQueryProvider.CreateQuery<S>(Expression expr)
    {
      IQueryable<S> RNTRNTRNT_1 = _queryProvider.CreateQuery<S>(expr);
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      return RNTRNTRNT_1;
    }
    S IQueryProvider.Execute<S>(Expression expression)
    {
      S RNTRNTRNT_2 = _queryProvider.Execute<S>(expression);
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      return RNTRNTRNT_2;
    }
  }
}
