using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Resources;
using System.Text;
namespace Npgsql
{
  static internal class PGUtil
  {
    private static readonly String CLASSNAME = MethodBase.GetCurrentMethod().DeclaringType.Name;
    static internal readonly ResourceManager resman = new ResourceManager(MethodBase.GetCurrentMethod().DeclaringType);
    private const int THRASH_CAN_SIZE = 4096;
    private static readonly byte[] THRASH_CAN = new byte[THRASH_CAN_SIZE];
    private static readonly Encoding ENCODING_UTF8 = Encoding.UTF8;
    private static readonly string NULL_TERMINATOR_STRING = '\0'.ToString();
    public static Int32 ConvertProtocolVersion(ProtocolVersion Ver)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      switch (Ver) {
        case ProtocolVersion.Version2:
          Int32 RNTRNTRNT_1 = (int)ServerVersionCode.ProtocolVersion2;
          IACSharpSensor.IACSharpSensor.SensorReached(2);
          return RNTRNTRNT_1;
        case ProtocolVersion.Version3:
          Int32 RNTRNTRNT_2 = (int)ServerVersionCode.ProtocolVersion3;
          IACSharpSensor.IACSharpSensor.SensorReached(3);
          return RNTRNTRNT_2;
      }
      throw new ArgumentOutOfRangeException();
    }
    public static string ExtractServerVersion(string VersionString)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4);
      Int32 Start = 0, End = 0;
      for (; Start < VersionString.Length && !char.IsDigit(VersionString[Start]); Start++) {
        ;
      }
      End = Start;
      for (; End < VersionString.Length && !char.IsWhiteSpace(VersionString[End]); End++) {
        ;
      }
      VersionString = VersionString.Substring(Start, End - Start + 1);
      for (int idx = 0; idx != VersionString.Length; ++idx) {
        char c = VersionString[idx];
        if (!char.IsDigit(c) && c != '.') {
          VersionString = VersionString.Substring(0, idx);
          break;
        }
      }
      System.String RNTRNTRNT_3 = VersionString;
      IACSharpSensor.IACSharpSensor.SensorReached(5);
      return RNTRNTRNT_3;
    }
    public static String ReadString(Stream network_stream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6);
      NpgsqlEventLog.LogMethodEnter(LogLevel.Debug, CLASSNAME, "ReadString");
      List<byte> buffer = new List<byte>();
      for (int bRead = network_stream.ReadByte(); bRead != 0; bRead = network_stream.ReadByte()) {
        if (bRead == -1) {
          throw new IOException();
        } else {
          buffer.Add((byte)bRead);
        }
      }
      if (NpgsqlEventLog.Level >= LogLevel.Debug) {
        NpgsqlEventLog.LogMsg(resman, "Log_StringRead", LogLevel.Debug, ENCODING_UTF8.GetString(buffer.ToArray()));
      }
      String RNTRNTRNT_4 = ENCODING_UTF8.GetString(buffer.ToArray());
      IACSharpSensor.IACSharpSensor.SensorReached(7);
      return RNTRNTRNT_4;
    }
    public static char ReadChar(Stream stream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(8);
      byte[] buffer = new byte[4];
      for (int i = 0; i != 4; ++i) {
        int byteRead = stream.ReadByte();
        if (byteRead == -1) {
          throw new EndOfStreamException();
        }
        buffer[i] = (byte)byteRead;
        if (ValidUTF8Ending(buffer, 0, i + 1)) {
          System.Char RNTRNTRNT_5 = ENCODING_UTF8.GetChars(buffer)[0];
          IACSharpSensor.IACSharpSensor.SensorReached(9);
          return RNTRNTRNT_5;
        }
      }
      throw new InvalidDataException();
    }
    public static int ReadChars(Stream stream, char[] output, int maxChars, ref int maxRead, int outputIdx)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(10);
      if (maxChars == 0 || maxRead == 0) {
        System.Int32 RNTRNTRNT_6 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(11);
        return RNTRNTRNT_6;
      }
      byte[] buffer = new byte[Math.Min(maxRead, maxChars * 4)];
      int bytesSoFar = 0;
      int charsSoFar = 0;
      do {
        int toRead = Math.Min(maxRead, maxChars - charsSoFar);
        CheckedStreamRead(stream, buffer, bytesSoFar, toRead);
        maxRead -= toRead;
        bytesSoFar += toRead;
      } while (maxRead > 0 && (charsSoFar = PessimisticGetCharCount(buffer, 0, bytesSoFar)) < maxChars);
      System.Int32 RNTRNTRNT_7 = ENCODING_UTF8.GetDecoder().GetChars(buffer, 0, bytesSoFar, output, outputIdx, false);
      IACSharpSensor.IACSharpSensor.SensorReached(12);
      return RNTRNTRNT_7;
    }
    public static int SkipChars(Stream stream, int maxChars, ref int maxRead)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(13);
      if (maxChars == 0 || maxRead == 0) {
        System.Int32 RNTRNTRNT_8 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(14);
        return RNTRNTRNT_8;
      }
      byte[] buffer = new byte[Math.Min(maxRead, ENCODING_UTF8.GetMaxByteCount(maxChars))];
      int bytesSoFar = 0;
      int charsSoFar = 0;
      do {
        int toRead = Math.Min(maxRead, maxChars - charsSoFar);
        CheckedStreamRead(stream, buffer, bytesSoFar, toRead);
        maxRead -= toRead;
        bytesSoFar += toRead;
      } while (maxRead > 0 && (charsSoFar = PessimisticGetCharCount(buffer, 0, bytesSoFar)) < maxChars);
      System.Int32 RNTRNTRNT_9 = charsSoFar;
      IACSharpSensor.IACSharpSensor.SensorReached(15);
      return RNTRNTRNT_9;
    }
    public static bool ValidUTF8Ending(byte[] buffer, int index, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(16);
      if (count <= 0) {
        System.Boolean RNTRNTRNT_10 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(17);
        return RNTRNTRNT_10;
      }
      byte examine = buffer[index + count - 1];
      if ((examine & 0x80) == 0) {
        System.Boolean RNTRNTRNT_11 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(18);
        return RNTRNTRNT_11;
      }
      if ((examine & 0xc0) != 0x80) {
        System.Boolean RNTRNTRNT_12 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(19);
        return RNTRNTRNT_12;
      }
      byte[] masks = new byte[] {
        0xe0,
        0xf0,
        0xf8
      };
      byte[] matches = new byte[] {
        0xc0,
        0xe0,
        0xf0
      };
      for (int i = 0; i + 2 <= count; ++i) {
        examine = buffer[index + count - 2 - i];
        if ((examine & masks[i]) == matches[i]) {
          System.Boolean RNTRNTRNT_13 = true;
          IACSharpSensor.IACSharpSensor.SensorReached(20);
          return RNTRNTRNT_13;
        }
        if ((examine & 0xc0) != 0x80) {
          System.Boolean RNTRNTRNT_14 = false;
          IACSharpSensor.IACSharpSensor.SensorReached(21);
          return RNTRNTRNT_14;
        }
      }
      System.Boolean RNTRNTRNT_15 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(22);
      return RNTRNTRNT_15;
    }
    public static int ReadBytes(Stream stream, byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(23);
      int end = offset + count;
      int got = 0;
      int need = count;
      do {
        got = stream.Read(buffer, offset, need);
        offset += got;
        need -= got;
      } while (offset < end && got != 0);
      System.Int32 RNTRNTRNT_16 = count - (end - offset);
      IACSharpSensor.IACSharpSensor.SensorReached(24);
      return RNTRNTRNT_16;
    }
    public static int PessimisticGetCharCount(byte[] buffer, int index, int count)
    {
      System.Int32 RNTRNTRNT_17 = ENCODING_UTF8.GetCharCount(buffer, index, count) - (ValidUTF8Ending(buffer, index, count) ? 0 : 1);
      IACSharpSensor.IACSharpSensor.SensorReached(25);
      return RNTRNTRNT_17;
    }
    public static void WriteString(String the_string, Stream network_stream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(26);
      NpgsqlEventLog.LogMethodEnter(LogLevel.Debug, CLASSNAME, "WriteString");
      NpgsqlEventLog.LogMsg(resman, "Log_StringWritten", LogLevel.Debug, the_string);
      byte[] bytes = ENCODING_UTF8.GetBytes(the_string + NULL_TERMINATOR_STRING);
      network_stream.Write(bytes, 0, bytes.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(27);
    }
    public static void WriteBytes(byte[] the_bytes, Stream network_stream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(28);
      NpgsqlEventLog.LogMethodEnter(LogLevel.Debug, CLASSNAME, "WriteBytes");
      NpgsqlEventLog.LogMsg(resman, "Log_BytesWritten", LogLevel.Debug, the_bytes);
      network_stream.Write(the_bytes, 0, the_bytes.Length);
      network_stream.Write(new byte[1], 0, 1);
      IACSharpSensor.IACSharpSensor.SensorReached(29);
    }
    public static void WriteLimString(String the_string, Int32 n, Stream network_stream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(30);
      NpgsqlEventLog.LogMethodEnter(LogLevel.Debug, CLASSNAME, "WriteLimString");
      byte[] bytes = ENCODING_UTF8.GetBytes(the_string);
      if (bytes.Length > n) {
        throw new ArgumentOutOfRangeException("the_string", the_string, string.Format(resman.GetString("LimStringWriteTooLarge"), the_string, n));
      }
      network_stream.Write(bytes, 0, bytes.Length);
      if (bytes.Length < n) {
        bytes = new byte[n - bytes.Length];
        network_stream.Write(bytes, 0, bytes.Length);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(31);
    }
    public static void CheckedStreamRead(Stream stream, Byte[] buffer, Int32 offset, Int32 size)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(32);
      Int32 bytes_from_stream = 0;
      Int32 total_bytes_read = 0;
      int maxReadChunkSize = 8192;
      while (size > 0) {
        int readSize = (size > maxReadChunkSize) ? maxReadChunkSize : size;
        bytes_from_stream = stream.Read(buffer, offset + total_bytes_read, readSize);
        total_bytes_read += bytes_from_stream;
        size -= bytes_from_stream;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(33);
    }
    public static void EatStreamBytes(Stream stream, int size)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(34);
      while (size > 0) {
        size -= stream.Read(THRASH_CAN, 0, size < THRASH_CAN_SIZE ? size : THRASH_CAN_SIZE);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(35);
    }
    public static int ReadEscapedBytes(Stream stream, byte[] output, int maxBytes, ref int maxRead, int outputIdx)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(36);
      maxBytes = maxBytes > output.Length - outputIdx ? output.Length - outputIdx : maxBytes;
      int i;
      for (i = 0; i != maxBytes && maxRead > 0; ++i) {
        char c = ReadChar(stream);
        --maxRead;
        if (c == '\\') {
          --maxRead;
          switch (c = ReadChar(stream)) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
              maxRead -= 2;
              output[outputIdx++] = (byte)((int.Parse(c.ToString()) << 6) | (int.Parse(ReadChar(stream).ToString()) << 3) | int.Parse(ReadChar(stream).ToString()));
              break;
            default:
              output[outputIdx++] = (byte)c;
              break;
          }
        } else {
          output[outputIdx++] = (byte)c;
        }
      }
      System.Int32 RNTRNTRNT_18 = i;
      IACSharpSensor.IACSharpSensor.SensorReached(37);
      return RNTRNTRNT_18;
    }
    public static int SkipEscapedBytes(Stream stream, int maxBytes, ref int maxRead)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(38);
      int i;
      for (i = 0; i != maxBytes && maxRead > 0; ++i) {
        --maxRead;
        if (ReadChar(stream) == '\\') {
          --maxRead;
          switch (ReadChar(stream)) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
              maxRead -= 2;
              EatStreamBytes(stream, 2);
              break;
          }
        }
      }
      System.Int32 RNTRNTRNT_19 = i;
      IACSharpSensor.IACSharpSensor.SensorReached(39);
      return RNTRNTRNT_19;
    }
    public static void WriteInt32(Stream stream, Int32 value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(40);
      stream.Write(BitConverter.GetBytes(IPAddress.HostToNetworkOrder(value)), 0, 4);
      IACSharpSensor.IACSharpSensor.SensorReached(41);
    }
    public static Int32 ReadInt32(Stream stream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(42);
      byte[] buffer = new byte[4];
      CheckedStreamRead(stream, buffer, 0, 4);
      Int32 RNTRNTRNT_20 = IPAddress.NetworkToHostOrder(BitConverter.ToInt32(buffer, 0));
      IACSharpSensor.IACSharpSensor.SensorReached(43);
      return RNTRNTRNT_20;
    }
    public static void WriteInt16(Stream stream, Int16 value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(44);
      stream.Write(BitConverter.GetBytes(IPAddress.HostToNetworkOrder(value)), 0, 2);
      IACSharpSensor.IACSharpSensor.SensorReached(45);
    }
    public static Int16 ReadInt16(Stream stream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(46);
      byte[] buffer = new byte[2];
      CheckedStreamRead(stream, buffer, 0, 2);
      Int16 RNTRNTRNT_21 = IPAddress.NetworkToHostOrder(BitConverter.ToInt16(buffer, 0));
      IACSharpSensor.IACSharpSensor.SensorReached(47);
      return RNTRNTRNT_21;
    }
    public static int RotateShift(int val, int shift)
    {
      System.Int32 RNTRNTRNT_22 = (val << shift) | (val >> (sizeof(int) - shift));
      IACSharpSensor.IACSharpSensor.SensorReached(48);
      return RNTRNTRNT_22;
    }
    public static StringBuilder TrimStringBuilder(StringBuilder sb)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(49);
      while (sb.Length != 0 && char.IsWhiteSpace(sb[0])) {
        sb.Remove(0, 1);
      }
      while (sb.Length != 0 && char.IsWhiteSpace(sb[sb.Length - 1])) {
        sb.Remove(sb.Length - 1, 1);
      }
      StringBuilder RNTRNTRNT_23 = sb;
      IACSharpSensor.IACSharpSensor.SensorReached(50);
      return RNTRNTRNT_23;
    }
    static internal void LogStringWritten(string theString)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(51);
      NpgsqlEventLog.LogMsg(resman, "Log_StringWritten", LogLevel.Debug, theString);
      IACSharpSensor.IACSharpSensor.SensorReached(52);
    }
  }
  public enum ProtocolVersion
  {
    Unknown,
    Version2,
    Version3
  }
  public enum ServerVersionCode
  {
    ProtocolVersion2 = 2 << 16,
    ProtocolVersion3 = 3 << 16
  }
  [Obsolete("Use System.Version")]
  public sealed class ServerVersion : IEquatable<ServerVersion>, IComparable<ServerVersion>, IComparable, ICloneable
  {
    [Obsolete("Use ServerVersionCode.ProtocolVersion2")]
    public static readonly Int32 ProtocolVersion2 = 2 << 16;
    [Obsolete("Use ServerVersionCode.ProtocolVersion3")]
    public static readonly Int32 ProtocolVersion3 = 3 << 16;
    private readonly Version _version;
    private ServerVersion(Version ver)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(53);
      _version = ver;
      IACSharpSensor.IACSharpSensor.SensorReached(54);
    }
    public Int32 Major {
      get {
        Int32 RNTRNTRNT_24 = _version.Major;
        IACSharpSensor.IACSharpSensor.SensorReached(55);
        return RNTRNTRNT_24;
      }
    }
    public Int32 Minor {
      get {
        Int32 RNTRNTRNT_25 = _version.Minor;
        IACSharpSensor.IACSharpSensor.SensorReached(56);
        return RNTRNTRNT_25;
      }
    }
    public Int32 Patch {
      get {
        Int32 RNTRNTRNT_26 = _version.Build;
        IACSharpSensor.IACSharpSensor.SensorReached(57);
        return RNTRNTRNT_26;
      }
    }
    public static implicit operator Version(ServerVersion sv)
    {
      Version RNTRNTRNT_27 = (object)sv == null ? null : sv._version;
      IACSharpSensor.IACSharpSensor.SensorReached(58);
      return RNTRNTRNT_27;
    }
    public static implicit operator ServerVersion(Version ver)
    {
      ServerVersion RNTRNTRNT_28 = (object)ver == null ? null : new ServerVersion(ver.Clone() as Version);
      IACSharpSensor.IACSharpSensor.SensorReached(59);
      return RNTRNTRNT_28;
    }
    public static bool operator ==(ServerVersion One, ServerVersion TheOther)
    {
      System.Boolean RNTRNTRNT_29 = ((Version)One) == ((Version)TheOther);
      IACSharpSensor.IACSharpSensor.SensorReached(60);
      return RNTRNTRNT_29;
    }
    public static bool operator !=(ServerVersion One, ServerVersion TheOther)
    {
      System.Boolean RNTRNTRNT_30 = (Version)One != (Version)TheOther;
      IACSharpSensor.IACSharpSensor.SensorReached(61);
      return RNTRNTRNT_30;
    }
    public static bool operator >(ServerVersion One, ServerVersion TheOther)
    {
      System.Boolean RNTRNTRNT_31 = (Version)One > (Version)TheOther;
      IACSharpSensor.IACSharpSensor.SensorReached(62);
      return RNTRNTRNT_31;
    }
    public static bool operator >=(ServerVersion One, ServerVersion TheOther)
    {
      System.Boolean RNTRNTRNT_32 = (Version)One >= (Version)TheOther;
      IACSharpSensor.IACSharpSensor.SensorReached(63);
      return RNTRNTRNT_32;
    }
    public static bool operator <(ServerVersion One, ServerVersion TheOther)
    {
      System.Boolean RNTRNTRNT_33 = (Version)One < (Version)TheOther;
      IACSharpSensor.IACSharpSensor.SensorReached(64);
      return RNTRNTRNT_33;
    }
    public static bool operator <=(ServerVersion One, ServerVersion TheOther)
    {
      System.Boolean RNTRNTRNT_34 = (Version)One <= (Version)TheOther;
      IACSharpSensor.IACSharpSensor.SensorReached(65);
      return RNTRNTRNT_34;
    }
    public bool Equals(ServerVersion other)
    {
      System.Boolean RNTRNTRNT_35 = other != null && this == other;
      IACSharpSensor.IACSharpSensor.SensorReached(66);
      return RNTRNTRNT_35;
    }
    public int CompareTo(ServerVersion other)
    {
      System.Int32 RNTRNTRNT_36 = _version.CompareTo(other);
      IACSharpSensor.IACSharpSensor.SensorReached(67);
      return RNTRNTRNT_36;
    }
    public int CompareTo(object obj)
    {
      System.Int32 RNTRNTRNT_37 = CompareTo(obj as ServerVersion);
      IACSharpSensor.IACSharpSensor.SensorReached(68);
      return RNTRNTRNT_37;
    }
    public object Clone()
    {
      System.Object RNTRNTRNT_38 = new ServerVersion(_version.Clone() as Version);
      IACSharpSensor.IACSharpSensor.SensorReached(69);
      return RNTRNTRNT_38;
    }
    public override bool Equals(object O)
    {
      System.Boolean RNTRNTRNT_39 = Equals(O as ServerVersion);
      IACSharpSensor.IACSharpSensor.SensorReached(70);
      return RNTRNTRNT_39;
    }
    public override int GetHashCode()
    {
      System.Int32 RNTRNTRNT_40 = _version.GetHashCode();
      IACSharpSensor.IACSharpSensor.SensorReached(71);
      return RNTRNTRNT_40;
    }
    public override String ToString()
    {
      String RNTRNTRNT_41 = _version.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(72);
      return RNTRNTRNT_41;
    }
  }
  internal enum FormatCode : short
  {
    Text = 0,
    Binary = 1
  }
  internal class ReadOnlyDictionary<TKey, TValue> : IDictionary<TKey, TValue>
  {
    private readonly Dictionary<TKey, TValue> _inner;
    public ReadOnlyDictionary(Dictionary<TKey, TValue> inner)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(73);
      _inner = inner;
      IACSharpSensor.IACSharpSensor.SensorReached(74);
    }
    private static void StopWrite()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(75);
      throw new NotSupportedException(PGUtil.resman.GetString("Read_Only_Write_Error"));
    }
    public TValue this[TKey key] {
      get {
        TValue RNTRNTRNT_42 = _inner[key];
        IACSharpSensor.IACSharpSensor.SensorReached(76);
        return RNTRNTRNT_42;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(77);
        StopWrite();
        IACSharpSensor.IACSharpSensor.SensorReached(78);
      }
    }
    public ICollection<TKey> Keys {
      get {
        ICollection<TKey> RNTRNTRNT_43 = _inner.Keys;
        IACSharpSensor.IACSharpSensor.SensorReached(79);
        return RNTRNTRNT_43;
      }
    }
    public ICollection<TValue> Values {
      get {
        ICollection<TValue> RNTRNTRNT_44 = _inner.Values;
        IACSharpSensor.IACSharpSensor.SensorReached(80);
        return RNTRNTRNT_44;
      }
    }
    public int Count {
      get {
        System.Int32 RNTRNTRNT_45 = _inner.Count;
        IACSharpSensor.IACSharpSensor.SensorReached(81);
        return RNTRNTRNT_45;
      }
    }
    public bool IsReadOnly {
      get {
        System.Boolean RNTRNTRNT_46 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(82);
        return RNTRNTRNT_46;
      }
    }
    public bool ContainsKey(TKey key)
    {
      System.Boolean RNTRNTRNT_47 = _inner.ContainsKey(key);
      IACSharpSensor.IACSharpSensor.SensorReached(83);
      return RNTRNTRNT_47;
    }
    public void Add(TKey key, TValue value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(84);
      StopWrite();
      IACSharpSensor.IACSharpSensor.SensorReached(85);
    }
    public bool Remove(TKey key)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(86);
      StopWrite();
      System.Boolean RNTRNTRNT_48 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(87);
      return RNTRNTRNT_48;
    }
    public bool TryGetValue(TKey key, out TValue value)
    {
      System.Boolean RNTRNTRNT_49 = _inner.TryGetValue(key, out value);
      IACSharpSensor.IACSharpSensor.SensorReached(88);
      return RNTRNTRNT_49;
    }
    public void Add(KeyValuePair<TKey, TValue> item)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(89);
      StopWrite();
      IACSharpSensor.IACSharpSensor.SensorReached(90);
    }
    public void Clear()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(91);
      StopWrite();
      IACSharpSensor.IACSharpSensor.SensorReached(92);
    }
    public bool Contains(KeyValuePair<TKey, TValue> item)
    {
      System.Boolean RNTRNTRNT_50 = ((IDictionary<TKey, TValue>)_inner).Contains(item);
      IACSharpSensor.IACSharpSensor.SensorReached(93);
      return RNTRNTRNT_50;
    }
    public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(94);
      ((IDictionary<TKey, TValue>)_inner).CopyTo(array, arrayIndex);
      IACSharpSensor.IACSharpSensor.SensorReached(95);
    }
    public bool Remove(KeyValuePair<TKey, TValue> item)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(96);
      StopWrite();
      System.Boolean RNTRNTRNT_51 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(97);
      return RNTRNTRNT_51;
    }
    public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(98);
      foreach (KeyValuePair<TKey, TValue> kvp in _inner) {
        KeyValuePair<TKey,TValue> RNTRNTRNT_52 = new KeyValuePair<TKey, TValue>(kvp.Key, kvp.Value);
        IACSharpSensor.IACSharpSensor.SensorReached(99);
        yield return RNTRNTRNT_52;
        IACSharpSensor.IACSharpSensor.SensorReached(100);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(101);
    }
    IEnumerator IEnumerable.GetEnumerator()
    {
      IEnumerator RNTRNTRNT_53 = _inner.GetEnumerator();
      IACSharpSensor.IACSharpSensor.SensorReached(102);
      return RNTRNTRNT_53;
    }
  }
}
