using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
namespace Newtonsoft.Json.Utilities
{
  static internal class JavaScriptUtils
  {
    static internal readonly bool[] SingleQuoteCharEscapeFlags = new bool[128];
    static internal readonly bool[] DoubleQuoteCharEscapeFlags = new bool[128];
    static internal readonly bool[] HtmlCharEscapeFlags = new bool[128];
    static JavaScriptUtils()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      IList<char> escapeChars = new List<char> {
        '\n',
        '\r',
        '\t',
        '\\',
        '\f',
        '\b'
      };
      for (int i = 0; i < ' '; i++) {
        escapeChars.Add((char)i);
      }
      foreach (var escapeChar in escapeChars.Union(new[] { '\'' })) {
        SingleQuoteCharEscapeFlags[escapeChar] = true;
      }
      foreach (var escapeChar in escapeChars.Union(new[] { '"' })) {
        DoubleQuoteCharEscapeFlags[escapeChar] = true;
      }
      foreach (var escapeChar in escapeChars.Union(new[] {
        '"',
        '\'',
        '<',
        '>',
        '&'
      })) {
        HtmlCharEscapeFlags[escapeChar] = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2);
    }
    private const string EscapedUnicodeText = "!";
    public static void WriteEscapedJavaScriptString(TextWriter writer, string s, char delimiter, bool appendDelimiters, bool[] charEscapeFlags, StringEscapeHandling stringEscapeHandling, ref char[] writeBuffer)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3);
      if (appendDelimiters) {
        writer.Write(delimiter);
      }
      if (s != null) {
        int lastWritePosition = 0;
        for (int i = 0; i < s.Length; i++) {
          var c = s[i];
          if (c < charEscapeFlags.Length && !charEscapeFlags[c]) {
            continue;
          }
          string escapedValue;
          switch (c) {
            case '\t':
              escapedValue = "\\t";
              break;
            case '\n':
              escapedValue = "\\n";
              break;
            case '\r':
              escapedValue = "\\r";
              break;
            case '\f':
              escapedValue = "\\f";
              break;
            case '\b':
              escapedValue = "\\b";
              break;
            case '\\':
              escapedValue = "\\\\";
              break;
            case '\u0085':
              escapedValue = "\\u0085";
              break;
            case '\u2028':
              escapedValue = "\\u2028";
              break;
            case '\u2029':
              escapedValue = "\\u2029";
              break;
            default:
              if (c < charEscapeFlags.Length || stringEscapeHandling == StringEscapeHandling.EscapeNonAscii) {
                if (c == '\'' && stringEscapeHandling != StringEscapeHandling.EscapeHtml) {
                  escapedValue = "\\'";
                } else if (c == '"' && stringEscapeHandling != StringEscapeHandling.EscapeHtml) {
                  escapedValue = "\\\"";
                } else {
                  if (writeBuffer == null) {
                    writeBuffer = new char[6];
                  }
                  StringUtils.ToCharAsUnicode(c, writeBuffer);
                  escapedValue = EscapedUnicodeText;
                }
              } else {
                escapedValue = null;
              }
              break;
          }
          if (escapedValue == null) {
            continue;
          }
          bool isEscapedUnicodeText = string.Equals(escapedValue, EscapedUnicodeText);
          if (i > lastWritePosition) {
            int length = i - lastWritePosition + ((isEscapedUnicodeText) ? 6 : 0);
            int start = (isEscapedUnicodeText) ? 6 : 0;
            if (writeBuffer == null || writeBuffer.Length < length) {
              char[] newBuffer = new char[length];
              if (isEscapedUnicodeText) {
                Array.Copy(writeBuffer, newBuffer, 6);
              }
              writeBuffer = newBuffer;
            }
            s.CopyTo(lastWritePosition, writeBuffer, start, length - start);
            writer.Write(writeBuffer, start, length - start);
          }
          lastWritePosition = i + 1;
          if (!isEscapedUnicodeText) {
            writer.Write(escapedValue);
          } else {
            writer.Write(writeBuffer, 0, 6);
          }
        }
        if (lastWritePosition == 0) {
          writer.Write(s);
        } else {
          int length = s.Length - lastWritePosition;
          if (writeBuffer == null || writeBuffer.Length < length) {
            writeBuffer = new char[length];
          }
          s.CopyTo(lastWritePosition, writeBuffer, 0, length);
          writer.Write(writeBuffer, 0, length);
        }
      }
      if (appendDelimiters) {
        writer.Write(delimiter);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4);
    }
    public static string ToEscapedJavaScriptString(string value, char delimiter, bool appendDelimiters)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5);
      using (StringWriter w = StringUtils.CreateStringWriter(StringUtils.GetLength(value) ?? 16)) {
        char[] buffer = null;
        WriteEscapedJavaScriptString(w, value, delimiter, appendDelimiters, (delimiter == '"') ? DoubleQuoteCharEscapeFlags : SingleQuoteCharEscapeFlags, StringEscapeHandling.Default, ref buffer);
        System.String RNTRNTRNT_1 = w.ToString();
        IACSharpSensor.IACSharpSensor.SensorReached(6);
        return RNTRNTRNT_1;
      }
    }
    private char ToTitleCase(char c)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(7);
      switch (c) {
        case '\u01c4':
        case '\u01c5':
        case '\u01c6':
          System.Char RNTRNTRNT_2 = '\u01c5';
          IACSharpSensor.IACSharpSensor.SensorReached(8);
          return RNTRNTRNT_2;
      }
      if ('\u2170' <= c && c <= '\u217f' || '\u24d0' <= c && c <= '\u24e9') {
        System.Char RNTRNTRNT_3 = c;
        IACSharpSensor.IACSharpSensor.SensorReached(9);
        return RNTRNTRNT_3;
      }
      System.Char RNTRNTRNT_4 = ToUpper(c);
      IACSharpSensor.IACSharpSensor.SensorReached(10);
      return RNTRNTRNT_4;
    }
  }
}
