using System.Linq.Expressions;
namespace DbLinq.Data.Linq.Sugar.Expressions
{
  public class OrderByExpression : MutableExpression
  {
    public override System.Collections.Generic.IEnumerable<Expression> Operands {
      get {
        Expression RNTRNTRNT_1 = ColumnExpression;
        IACSharpSensor.IACSharpSensor.SensorReached(1);
        yield return RNTRNTRNT_1;
        IACSharpSensor.IACSharpSensor.SensorReached(2);
        IACSharpSensor.IACSharpSensor.SensorReached(3);
      }
    }
  }
}
