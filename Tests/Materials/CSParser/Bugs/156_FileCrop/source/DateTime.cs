//
// System.DateTime.cs
//
// Authors:
//   Marcel Narings (marcel@narings.nl)
//   Martin Baulig (martin@gnome.org)
//   Atsushi Enomoto (atsushi@ximian.com)
//   Marek Safar (marek.safar@gmail.com)
//
//   (C) 2001 Marcel Narings
// Copyright (C) 2004-2006 Novell, Inc (http://www.novell.com)
// Copyright (C) 2012 Xamarin Inc (http://www.xamarin.com)
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>
	/// The DateTime structure represents dates and time ranging from
	/// 1-1-0001 12:00:00 AM to 31-12-9999 23:59:00 Common Era.
	/// </summary>
	/// 
	[Serializable]
	[StructLayout (LayoutKind.Auto)]
	public struct DateTime : IFormattable, IConvertible, IComparable, ISerializable, IComparable<DateTime>, IEquatable <DateTime>
	{
		//
		// Encodes the DateTime in 64 bits, top two bits contain the DateTimeKind,
		// the rest contains the 62 bit value for the ticks.   This reduces the
		// memory usage from 16 to 8 bytes, see bug: 592221.   This also fixes the
		// 622127 issue and simplifies the code in reflection.c to encode DateTimes
		//
		long encoded;
		const long TicksMask = 0x3fffffffffffffff;
		const long KindMask = unchecked ((long) 0xc000000000000000);
		const int KindShift = 62;

		private const int dp400 = 146097;
		private const int dp100 = 36524;
		private const int dp4 = 1461;

		// w32 file time starts counting from 1/1/1601 00:00 GMT
		// which is the constant ticks from the .NET epoch
		private const long w32file_epoch = 504911232000000000L;

		//private const long MAX_VALUE_TICKS = 3155378975400000000L;
		// -- Microsoft .NET has this value.
		private const long MAX_VALUE_TICKS = 3155378975999999999L;

		//
		// The UnixEpoch, it begins on Jan 1, 1970 at 0:0:0, expressed
		// in Ticks
		//
		internal const long UnixEpoch = 621355968000000000L;

		// for OLE Automation dates
		private const long ticks18991230 = 599264352000000000L;
		private const double OAMinValue = -657435.0d;
		private const double OAMaxValue = 2958466.0d;

		public static readonly DateTime MaxValue = new DateTime (3155378975999999999);
		public static readonly DateTime MinValue = new DateTime (0);

		// DateTime.Parse patterns
		// Patterns are divided to date and time patterns. The algorithm will
		// try combinations of these patterns. The algorithm also looks for
		// day of the week, AM/PM GMT and Z independently of the patterns.
		private static readonly string[] ParseTimeFormats = new string [] {
			"H:m:s.fff zzz",
			"H:m:s.fffffffzzz",
			"H:m:s.fffffff",
			"H:m:s.ffffff",
			"H:m:s.ffffffzzz",
			"H:m:s.fffff",
			"H:m:s.ffff",
			"H:m:s.fff",
			"H:m:s.ff",
			"H:m:s.f",
			"H:m:s tt zzz",
			"H:m:szzz",
			"H:m:s",
			"H:mzzz",
			"H:m",
			"H tt", // Specifies AM to disallow '8'.
			"H'\u6642'm'\u5206's'\u79D2'",
		};

		// DateTime.Parse date patterns extend ParseExact patterns as follows:
		//   MMM - month short name or month full name
		//   MMMM - month number or short name or month full name

		// Parse behaves differently according to the ShorDatePattern of the
		// DateTimeFormatInfo. The following define the date patterns for
		// different orders of day, month and year in ShorDatePattern.
		// Note that the year cannot go between the day and the month.
		private static readonly string[] ParseYearDayMonthFormats = new string [] {
			"yyyy/M/dT",
			"M/yyyy/dT",
			"yyyy'\u5E74'M'\u6708'd'\u65E5",


			"yyyy/d/MMMM",
			"yyyy/MMM/d",
			"d/MMMM/yyyy",
			"MMM/d/yyyy",
			"d/yyyy/MMMM",
			"MMM/yyyy/d",

			"yy/d/M",
		};
	}
}
		