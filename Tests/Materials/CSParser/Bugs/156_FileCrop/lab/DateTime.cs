using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Runtime.Serialization;
namespace System
{
  [Serializable()]
  [StructLayout(LayoutKind.Auto)]
  public struct DateTime : IFormattable, IConvertible, IComparable, ISerializable, IComparable<DateTime>, IEquatable<DateTime>
  {
    long encoded;
    const long TicksMask = 0x3fffffffffffffffL;
    const long KindMask = unchecked((long)0xc000000000000000uL);
    const int KindShift = 62;
    private const int dp400 = 146097;
    private const int dp100 = 36524;
    private const int dp4 = 1461;
    private const long w32file_epoch = 504911232000000000L;
    private const long MAX_VALUE_TICKS = 3155378975999999999L;
    internal const long UnixEpoch = 621355968000000000L;
    private const long ticks18991230 = 599264352000000000L;
    private const double OAMinValue = -657435.0d;
    private const double OAMaxValue = 2958466.0d;
    public static readonly DateTime MaxValue = new DateTime(3155378975999999999L);
    public static readonly DateTime MinValue = new DateTime(0);
    private static readonly string[] ParseTimeFormats = new string[] {
      "H:m:s.fff zzz",
      "H:m:s.fffffffzzz",
      "H:m:s.fffffff",
      "H:m:s.ffffff",
      "H:m:s.ffffffzzz",
      "H:m:s.fffff",
      "H:m:s.ffff",
      "H:m:s.fff",
      "H:m:s.ff",
      "H:m:s.f",
      "H:m:s tt zzz",
      "H:m:szzz",
      "H:m:s",
      "H:mzzz",
      "H:m",
      "H tt",
      "H'\u6642'm'\u5206's'\u79D2'"
    };
    private static readonly string[] ParseYearDayMonthFormats = new string[] {
      "yyyy/M/dT",
      "M/yyyy/dT",
      "yyyy'\u5E74'M'\u6708'd'\u65E5",
      "yyyy/d/MMMM",
      "yyyy/MMM/d",
      "d/MMMM/yyyy",
      "MMM/d/yyyy",
      "d/yyyy/MMMM",
      "MMM/yyyy/d",
      "yy/d/M"
    };
  }
}
