namespace System
{
  class Test
  {
    [CLSCompliant(false)]
    [ReliabilityContractAttribute(Consistency.WillNotCorruptState, Cer.Success)]
    unsafe public void* ToPointer()
    {
      void * RNTRNTRNT_1 = m_value;
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      return RNTRNTRNT_1;
    }
  }
}
