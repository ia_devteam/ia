namespace System
{
	class Test{
	[CLSCompliant (false)]
		[ReliabilityContractAttribute (Consistency.WillNotCorruptState, Cer.Success)]
		unsafe public void *ToPointer ()
		{
			return m_value;
		}
}
}