using System;
using Interop = System.Runtime.InteropServices;
namespace Ionic.Crc
{
  [Interop.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d0000C")]
  [Interop.ComVisible(true)]
  [Interop.ClassInterface(Interop.ClassInterfaceType.AutoDispatch)]
  public class CRC32
  {
    public Int64 TotalBytesRead {
      get {
        Int64 RNTRNTRNT_737 = _TotalBytesRead;
        IACSharpSensor.IACSharpSensor.SensorReached(6362);
        return RNTRNTRNT_737;
      }
    }
    public Int32 Crc32Result {
      get {
        Int32 RNTRNTRNT_738 = unchecked((Int32)(~_register));
        IACSharpSensor.IACSharpSensor.SensorReached(6363);
        return RNTRNTRNT_738;
      }
    }
    public Int32 GetCrc32(System.IO.Stream input)
    {
      Int32 RNTRNTRNT_739 = GetCrc32AndCopy(input, null);
      IACSharpSensor.IACSharpSensor.SensorReached(6364);
      return RNTRNTRNT_739;
    }
    public Int32 GetCrc32AndCopy(System.IO.Stream input, System.IO.Stream output)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6365);
      if (input == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6366);
        throw new Exception("The input stream must not be null.");
      }
      unchecked {
        IACSharpSensor.IACSharpSensor.SensorReached(6367);
        byte[] buffer = new byte[BUFFER_SIZE];
        int readSize = BUFFER_SIZE;
        _TotalBytesRead = 0;
        int count = input.Read(buffer, 0, readSize);
        IACSharpSensor.IACSharpSensor.SensorReached(6368);
        if (output != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(6369);
          output.Write(buffer, 0, count);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6370);
        _TotalBytesRead += count;
        IACSharpSensor.IACSharpSensor.SensorReached(6371);
        while (count > 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(6372);
          SlurpBlock(buffer, 0, count);
          count = input.Read(buffer, 0, readSize);
          IACSharpSensor.IACSharpSensor.SensorReached(6373);
          if (output != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(6374);
            output.Write(buffer, 0, count);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(6375);
          _TotalBytesRead += count;
        }
        Int32 RNTRNTRNT_740 = (Int32)(~_register);
        IACSharpSensor.IACSharpSensor.SensorReached(6376);
        return RNTRNTRNT_740;
      }
    }
    public Int32 ComputeCrc32(Int32 W, byte B)
    {
      Int32 RNTRNTRNT_741 = _InternalComputeCrc32((UInt32)W, B);
      IACSharpSensor.IACSharpSensor.SensorReached(6377);
      return RNTRNTRNT_741;
    }
    internal Int32 _InternalComputeCrc32(UInt32 W, byte B)
    {
      Int32 RNTRNTRNT_742 = (Int32)(crc32Table[(W ^ B) & 0xff] ^ (W >> 8));
      IACSharpSensor.IACSharpSensor.SensorReached(6378);
      return RNTRNTRNT_742;
    }
    public void SlurpBlock(byte[] block, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6379);
      if (block == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6380);
        throw new Exception("The data buffer must not be null.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6381);
      for (int i = 0; i < count; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(6382);
        int x = offset + i;
        byte b = block[x];
        IACSharpSensor.IACSharpSensor.SensorReached(6383);
        if (this.reverseBits) {
          IACSharpSensor.IACSharpSensor.SensorReached(6384);
          UInt32 temp = (_register >> 24) ^ b;
          _register = (_register << 8) ^ crc32Table[temp];
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(6385);
          UInt32 temp = (_register & 0xff) ^ b;
          _register = (_register >> 8) ^ crc32Table[temp];
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6386);
      _TotalBytesRead += count;
      IACSharpSensor.IACSharpSensor.SensorReached(6387);
    }
    public void UpdateCRC(byte b)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6388);
      if (this.reverseBits) {
        IACSharpSensor.IACSharpSensor.SensorReached(6389);
        UInt32 temp = (_register >> 24) ^ b;
        _register = (_register << 8) ^ crc32Table[temp];
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(6390);
        UInt32 temp = (_register & 0xff) ^ b;
        _register = (_register >> 8) ^ crc32Table[temp];
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6391);
    }
    public void UpdateCRC(byte b, int n)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6392);
      while (n-- > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6393);
        if (this.reverseBits) {
          IACSharpSensor.IACSharpSensor.SensorReached(6394);
          uint temp = (_register >> 24) ^ b;
          _register = (_register << 8) ^ crc32Table[(temp >= 0) ? temp : (temp + 256)];
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(6395);
          UInt32 temp = (_register & 0xff) ^ b;
          _register = (_register >> 8) ^ crc32Table[(temp >= 0) ? temp : (temp + 256)];
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6396);
    }
    private static uint ReverseBits(uint data)
    {
      unchecked {
        IACSharpSensor.IACSharpSensor.SensorReached(6397);
        uint ret = data;
        ret = (ret & 0x55555555) << 1 | (ret >> 1) & 0x55555555;
        ret = (ret & 0x33333333) << 2 | (ret >> 2) & 0x33333333;
        ret = (ret & 0xf0f0f0f) << 4 | (ret >> 4) & 0xf0f0f0f;
        ret = (ret << 24) | ((ret & 0xff00) << 8) | ((ret >> 8) & 0xff00) | (ret >> 24);
        System.UInt32 RNTRNTRNT_743 = ret;
        IACSharpSensor.IACSharpSensor.SensorReached(6398);
        return RNTRNTRNT_743;
      }
    }
    private static byte ReverseBits(byte data)
    {
      unchecked {
        IACSharpSensor.IACSharpSensor.SensorReached(6399);
        uint u = (uint)data * 0x20202;
        uint m = 0x1044010;
        uint s = u & m;
        uint t = (u << 2) & (m << 1);
        System.Byte RNTRNTRNT_744 = (byte)((0x1001001 * (s + t)) >> 24);
        IACSharpSensor.IACSharpSensor.SensorReached(6400);
        return RNTRNTRNT_744;
      }
    }
    private void GenerateLookupTable()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6401);
      crc32Table = new UInt32[256];
      unchecked {
        IACSharpSensor.IACSharpSensor.SensorReached(6402);
        UInt32 dwCrc;
        byte i = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(6403);
        do {
          IACSharpSensor.IACSharpSensor.SensorReached(6404);
          dwCrc = i;
          IACSharpSensor.IACSharpSensor.SensorReached(6405);
          for (byte j = 8; j > 0; j--) {
            IACSharpSensor.IACSharpSensor.SensorReached(6406);
            if ((dwCrc & 1) == 1) {
              IACSharpSensor.IACSharpSensor.SensorReached(6407);
              dwCrc = (dwCrc >> 1) ^ dwPolynomial;
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(6408);
              dwCrc >>= 1;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(6409);
          if (reverseBits) {
            IACSharpSensor.IACSharpSensor.SensorReached(6410);
            crc32Table[ReverseBits(i)] = ReverseBits(dwCrc);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(6411);
            crc32Table[i] = dwCrc;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(6412);
          i++;
        } while (i != 0);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6413);
    }
    private uint gf2_matrix_times(uint[] matrix, uint vec)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6414);
      uint sum = 0;
      int i = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(6415);
      while (vec != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6416);
        if ((vec & 0x1) == 0x1) {
          IACSharpSensor.IACSharpSensor.SensorReached(6417);
          sum ^= matrix[i];
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6418);
        vec >>= 1;
        i++;
      }
      System.UInt32 RNTRNTRNT_745 = sum;
      IACSharpSensor.IACSharpSensor.SensorReached(6419);
      return RNTRNTRNT_745;
    }
    private void gf2_matrix_square(uint[] square, uint[] mat)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6420);
      for (int i = 0; i < 32; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(6421);
        square[i] = gf2_matrix_times(mat, mat[i]);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6422);
    }
    public void Combine(int crc, int length)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6423);
      uint[] even = new uint[32];
      uint[] odd = new uint[32];
      IACSharpSensor.IACSharpSensor.SensorReached(6424);
      if (length == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6425);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6426);
      uint crc1 = ~_register;
      uint crc2 = (uint)crc;
      odd[0] = this.dwPolynomial;
      uint row = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(6427);
      for (int i = 1; i < 32; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(6428);
        odd[i] = row;
        row <<= 1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6429);
      gf2_matrix_square(even, odd);
      gf2_matrix_square(odd, even);
      uint len2 = (uint)length;
      IACSharpSensor.IACSharpSensor.SensorReached(6430);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(6431);
        gf2_matrix_square(even, odd);
        IACSharpSensor.IACSharpSensor.SensorReached(6432);
        if ((len2 & 1) == 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(6433);
          crc1 = gf2_matrix_times(even, crc1);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6434);
        len2 >>= 1;
        IACSharpSensor.IACSharpSensor.SensorReached(6435);
        if (len2 == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(6436);
          break;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6437);
        gf2_matrix_square(odd, even);
        IACSharpSensor.IACSharpSensor.SensorReached(6438);
        if ((len2 & 1) == 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(6439);
          crc1 = gf2_matrix_times(odd, crc1);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6440);
        len2 >>= 1;
      } while (len2 != 0);
      IACSharpSensor.IACSharpSensor.SensorReached(6441);
      crc1 ^= crc2;
      _register = ~crc1;
      IACSharpSensor.IACSharpSensor.SensorReached(6442);
      return;
    }
    public CRC32() : this(false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6443);
    }
    public CRC32(bool reverseBits) : this(unchecked((int)0xedb88320u), reverseBits)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6444);
    }
    public CRC32(int polynomial, bool reverseBits)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6445);
      this.reverseBits = reverseBits;
      this.dwPolynomial = (uint)polynomial;
      this.GenerateLookupTable();
      IACSharpSensor.IACSharpSensor.SensorReached(6446);
    }
    public void Reset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6447);
      _register = 0xffffffffu;
      IACSharpSensor.IACSharpSensor.SensorReached(6448);
    }
    private UInt32 dwPolynomial;
    private Int64 _TotalBytesRead;
    private bool reverseBits;
    private UInt32[] crc32Table;
    private const int BUFFER_SIZE = 8192;
    private UInt32 _register = 0xffffffffu;
  }
  public class CrcCalculatorStream : System.IO.Stream, System.IDisposable
  {
    private static readonly Int64 UnsetLengthLimit = -99;
    internal System.IO.Stream _innerStream;
    private CRC32 _Crc32;
    private Int64 _lengthLimit = -99;
    private bool _leaveOpen;
    public CrcCalculatorStream(System.IO.Stream stream) : this(true, CrcCalculatorStream.UnsetLengthLimit, stream, null)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6449);
    }
    public CrcCalculatorStream(System.IO.Stream stream, bool leaveOpen) : this(leaveOpen, CrcCalculatorStream.UnsetLengthLimit, stream, null)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6450);
    }
    public CrcCalculatorStream(System.IO.Stream stream, Int64 length) : this(true, length, stream, null)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6451);
      if (length < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6452);
        throw new ArgumentException("length");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6453);
    }
    public CrcCalculatorStream(System.IO.Stream stream, Int64 length, bool leaveOpen) : this(leaveOpen, length, stream, null)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6454);
      if (length < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6455);
        throw new ArgumentException("length");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6456);
    }
    public CrcCalculatorStream(System.IO.Stream stream, Int64 length, bool leaveOpen, CRC32 crc32) : this(leaveOpen, length, stream, crc32)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6457);
      if (length < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6458);
        throw new ArgumentException("length");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6459);
    }
    private CrcCalculatorStream(bool leaveOpen, Int64 length, System.IO.Stream stream, CRC32 crc32) : base()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6460);
      _innerStream = stream;
      _Crc32 = crc32 ?? new CRC32();
      _lengthLimit = length;
      _leaveOpen = leaveOpen;
      IACSharpSensor.IACSharpSensor.SensorReached(6461);
    }
    public Int64 TotalBytesSlurped {
      get {
        Int64 RNTRNTRNT_746 = _Crc32.TotalBytesRead;
        IACSharpSensor.IACSharpSensor.SensorReached(6462);
        return RNTRNTRNT_746;
      }
    }
    public Int32 Crc {
      get {
        Int32 RNTRNTRNT_747 = _Crc32.Crc32Result;
        IACSharpSensor.IACSharpSensor.SensorReached(6463);
        return RNTRNTRNT_747;
      }
    }
    public bool LeaveOpen {
      get {
        System.Boolean RNTRNTRNT_748 = _leaveOpen;
        IACSharpSensor.IACSharpSensor.SensorReached(6464);
        return RNTRNTRNT_748;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(6465);
        _leaveOpen = value;
        IACSharpSensor.IACSharpSensor.SensorReached(6466);
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6467);
      int bytesToRead = count;
      IACSharpSensor.IACSharpSensor.SensorReached(6468);
      if (_lengthLimit != CrcCalculatorStream.UnsetLengthLimit) {
        IACSharpSensor.IACSharpSensor.SensorReached(6469);
        if (_Crc32.TotalBytesRead >= _lengthLimit) {
          System.Int32 RNTRNTRNT_749 = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(6470);
          return RNTRNTRNT_749;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6471);
        Int64 bytesRemaining = _lengthLimit - _Crc32.TotalBytesRead;
        IACSharpSensor.IACSharpSensor.SensorReached(6472);
        if (bytesRemaining < count) {
          IACSharpSensor.IACSharpSensor.SensorReached(6473);
          bytesToRead = (int)bytesRemaining;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6474);
      int n = _innerStream.Read(buffer, offset, bytesToRead);
      IACSharpSensor.IACSharpSensor.SensorReached(6475);
      if (n > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6476);
        _Crc32.SlurpBlock(buffer, offset, n);
      }
      System.Int32 RNTRNTRNT_750 = n;
      IACSharpSensor.IACSharpSensor.SensorReached(6477);
      return RNTRNTRNT_750;
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6478);
      if (count > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6479);
        _Crc32.SlurpBlock(buffer, offset, count);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6480);
      _innerStream.Write(buffer, offset, count);
      IACSharpSensor.IACSharpSensor.SensorReached(6481);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_751 = _innerStream.CanRead;
        IACSharpSensor.IACSharpSensor.SensorReached(6482);
        return RNTRNTRNT_751;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_752 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(6483);
        return RNTRNTRNT_752;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_753 = _innerStream.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(6484);
        return RNTRNTRNT_753;
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6485);
      _innerStream.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(6486);
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(6487);
        if (_lengthLimit == CrcCalculatorStream.UnsetLengthLimit) {
          System.Int64 RNTRNTRNT_754 = _innerStream.Length;
          IACSharpSensor.IACSharpSensor.SensorReached(6488);
          return RNTRNTRNT_754;
        } else {
          System.Int64 RNTRNTRNT_755 = _lengthLimit;
          IACSharpSensor.IACSharpSensor.SensorReached(6489);
          return RNTRNTRNT_755;
        }
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_756 = _Crc32.TotalBytesRead;
        IACSharpSensor.IACSharpSensor.SensorReached(6490);
        return RNTRNTRNT_756;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(6491);
        throw new NotSupportedException();
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6492);
      throw new NotSupportedException();
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6493);
      throw new NotSupportedException();
    }
    void IDisposable.Dispose()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6494);
      Close();
      IACSharpSensor.IACSharpSensor.SensorReached(6495);
    }
    public override void Close()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6496);
      base.Close();
      IACSharpSensor.IACSharpSensor.SensorReached(6497);
      if (!_leaveOpen) {
        IACSharpSensor.IACSharpSensor.SensorReached(6498);
        _innerStream.Close();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6499);
    }
  }
}
